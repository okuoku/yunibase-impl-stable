/*===========================================================================*/
/*   (Ieee/number.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/number.scm -indent -o objs/obj_s/Ieee/number.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_NUMBERS_6_5_TYPE_DEFINITIONS
#define BGL___R4_NUMBERS_6_5_TYPE_DEFINITIONS
#endif // BGL___R4_NUMBERS_6_5_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL int64_t BGl_bignumzd2ze3int64z31zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62logz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_sinz00zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_log2z00zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62flonumzd2ze3elongz53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62llongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_flonumzd2ze3bignumz31zz__r4_numbers_6_5z00(double);
BGL_EXPORTED_DECL double BGl_acosz00zz__r4_numbers_6_5z00(obj_t);
extern obj_t the_failure(obj_t, obj_t, obj_t);
static obj_t BGl_z62numberzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62uint64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL int64_t BGl_flonumzd2ze3int64z31zz__r4_numbers_6_5z00(double);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00 = BUNSPEC;
static obj_t BGl_z62int64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, obj_t);
BGL_EXPORTED_DECL bool_t BGl_zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_rationalzf3zf3zz__r4_numbers_6_5z00(obj_t);
extern long BGl_exptfxz00zz__r4_numbers_6_5_fixnumz00(long, long);
extern obj_t BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(obj_t, long);
static obj_t BGl_z62tanz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_inexactzf3zf3zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL double BGl_int32zd2ze3flonumz31zz__r4_numbers_6_5z00(int32_t);
static obj_t BGl_z62flonumzd2ze3int32z53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62positivezf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_bignum_neg(obj_t);
static obj_t BGl_z62fixnumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62flonumzd2ze3fixnumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_symbol3802z00zz__r4_numbers_6_5z00 = BUNSPEC;
static obj_t BGl_z62bignumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_truncatez00zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_symbol3808z00zz__r4_numbers_6_5z00 = BUNSPEC;
static obj_t BGl_z62atanz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_expz00zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62roundz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3zd3z72zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_bignum_div(obj_t, obj_t);
static obj_t BGl_z62bignumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62maxz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__r4_numbers_6_5z00(void);
static obj_t BGl_z62numberzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
static obj_t BGl_z622zc3zd3z72zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_absz00zz__r4_numbers_6_5z00(obj_t);
extern obj_t BGl_stringzd2ze3integerzd2objze3zz__r4_numbers_6_5_fixnumz00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_ceilingz00zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62ceilingz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_int64zd2ze3bignumz31zz__r4_numbers_6_5z00(int64_t);
BGL_EXPORTED_DECL double BGl_atanz00zz__r4_numbers_6_5z00(obj_t, obj_t);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
static obj_t BGl_z62sqrtz62zz__r4_numbers_6_5z00(obj_t, obj_t);
static double BGl_za2minintflza2z00zz__r4_numbers_6_5z00;
static obj_t BGl_z62flonumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_numbers_6_5z00(void);
extern double BGl_roundflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62complexzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BGL_LONGLONG_T, obj_t);
static obj_t BGl_z62ze3zd3z52zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL double BGl_cosz00zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62uint32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62flonumzd2ze3uint32z53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5z00(void);
static obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5z00(void);
static obj_t BGl_z62int32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_positivezf3zf3zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z622ze3zd3z52zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z622maxz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
extern double bgl_bignum_to_flonum(obj_t);
BGL_EXPORTED_DECL double BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL double BGl_uint64zd2ze3flonumz31zz__r4_numbers_6_5z00(uint64_t);
static obj_t BGl_z62exptz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
extern obj_t bgl_bignum_mul(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_sqrtz00zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_logz00zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62sinz62zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62inexactzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_elongzd2ze3flonumz31zz__r4_numbers_6_5z00(long);
static obj_t BGl_z62exactzd2ze3inexactz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_fixnumzd2ze3flonumz31zz__r4_numbers_6_5z00(long);
BGL_EXPORTED_DECL long BGl_flonumzd2ze3fixnumz31zz__r4_numbers_6_5z00(double);
BGL_EXPORTED_DECL obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_flonumzd2ze3llongz31zz__r4_numbers_6_5z00(double);
static obj_t BGl_z62exactzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62bignumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62uint64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_bignumzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_exptz00zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z622za2zc0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z622zb2zd0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z622zd2zb0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z622zf2z90zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62int64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_bignum_add(obj_t, obj_t);
static obj_t BGl_z62truncatez62zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62flonumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62asinz62zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62inexactzd2ze3exactz53zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z622zc3za1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z622zd3zb1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z622ze3z81zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static double BGl_za2maxintflza2z00zz__r4_numbers_6_5z00;
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL double BGl_tanz00zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL double BGl_log10z00zz__r4_numbers_6_5z00(obj_t);
extern int64_t bgl_bignum_to_int64(obj_t);
extern obj_t bgl_long_to_bignum(long);
static obj_t BGl__stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl__numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62za2zc0zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62zb2zd0zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62zd2zb0zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zf2z90zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_za7erozf3z54zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62zc3za1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_floorz00zz__r4_numbers_6_5z00(obj_t);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zd3zb1zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62ze3z81zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62flonumzd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_asinz00zz__r4_numbers_6_5z00(obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_z62expz62zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62negativezf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_bignum_expt(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_uint32zd2ze3flonumz31zz__r4_numbers_6_5z00(uint32_t);
BGL_EXPORTED_DECL uint32_t BGl_flonumzd2ze3uint32z31zz__r4_numbers_6_5z00(double);
static bool_t BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62minz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_flonumzd2ze3elongz31zz__r4_numbers_6_5z00(double);
static obj_t BGl_z62elongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_llongzd2ze3flonumz31zz__r4_numbers_6_5z00(BGL_LONGLONG_T);
extern uint64_t bgl_bignum_to_uint64(obj_t);
extern obj_t bgl_uint64_to_bignum(uint64_t);
static obj_t BGl_z62absz62zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62flonumzd2ze3llongz53zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_bignum_sub(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_int64zd2ze3flonumz31zz__r4_numbers_6_5z00(int64_t);
extern int bgl_bignum_cmp(obj_t, obj_t);
extern obj_t bgl_bignum_abs(obj_t);
static obj_t BGl_z62rationalzf3z91zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_real_to_string(double);
extern obj_t bgl_long_to_bignum(long);
static obj_t BGl_z62log10z62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_za2za2zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62cosz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL obj_t bgl_exact_to_inexact(obj_t);
BGL_EXPORTED_DECL int32_t BGl_flonumzd2ze3int32z31zz__r4_numbers_6_5z00(double);
BGL_EXPORTED_DECL obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_llong_to_bignum(BGL_LONGLONG_T);
BGL_EXPORTED_DECL obj_t BGl_zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_exactzf3zf3zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_uint64zd2ze3bignumz31zz__r4_numbers_6_5z00(uint64_t);
BGL_EXPORTED_DECL uint64_t BGl_bignumzd2ze3uint64z31zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_z62log2z62zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z622minz62zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t, obj_t);
extern obj_t bgl_flonum_to_bignum(double);
static obj_t BGl_z62za7erozf3z36zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_flonumzd2ze3uint64z31zz__r4_numbers_6_5z00(double);
extern obj_t bgl_int64_to_bignum(uint64_t);
static obj_t BGl_z62floorz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_roundz00zz__r4_numbers_6_5z00(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62acosz62zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_inexact_to_exact(obj_t);
BGL_EXPORTED_DECL bool_t BGl_negativezf3zf3zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_complexzf3zf3zz__r4_numbers_6_5z00(obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string3810z00zz__r4_numbers_6_5z00, BgL_bgl_string3810za700za7za7_3822za7, "_string->number", 15 );
DEFINE_STRING( BGl_string3811z00zz__r4_numbers_6_5z00, BgL_bgl_string3811za700za7za7_3823za7, "string-ref", 10 );
DEFINE_STRING( BGl_string3730z00zz__r4_numbers_6_5z00, BgL_bgl_string3730za700za7za7_3824za7, "&flonum->bignum", 15 );
DEFINE_STRING( BGl_string3812z00zz__r4_numbers_6_5z00, BgL_bgl_string3812za700za7za7_3825za7, "loop", 4 );
DEFINE_STRING( BGl_string3731z00zz__r4_numbers_6_5z00, BgL_bgl_string3731za700za7za7_3826za7, "&bignum->flonum", 15 );
DEFINE_STRING( BGl_string3813z00zz__r4_numbers_6_5z00, BgL_bgl_string3813za700za7za7_3827za7, "+nan.0", 6 );
DEFINE_STRING( BGl_string3732z00zz__r4_numbers_6_5z00, BgL_bgl_string3732za700za7za7_3828za7, "bignum", 6 );
DEFINE_STRING( BGl_string3733z00zz__r4_numbers_6_5z00, BgL_bgl_string3733za700za7za7_3829za7, "&int64->bignum", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_int64zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762int64za7d2za7e3fl3830za7, BGl_z62int64zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3815z00zz__r4_numbers_6_5z00, BgL_bgl_string3815za700za7za7_3831za7, "+inf.0", 6 );
DEFINE_STRING( BGl_string3734z00zz__r4_numbers_6_5z00, BgL_bgl_string3734za700za7za7_3832za7, "bint64", 6 );
DEFINE_STRING( BGl_string3735z00zz__r4_numbers_6_5z00, BgL_bgl_string3735za700za7za7_3833za7, "&bignum->int64", 14 );
DEFINE_STRING( BGl_string3817z00zz__r4_numbers_6_5z00, BgL_bgl_string3817za700za7za7_3834za7, "-inf.0", 6 );
DEFINE_STRING( BGl_string3736z00zz__r4_numbers_6_5z00, BgL_bgl_string3736za700za7za7_3835za7, "&uint64->bignum", 15 );
DEFINE_STRING( BGl_string3737z00zz__r4_numbers_6_5z00, BgL_bgl_string3737za700za7za7_3836za7, "buint64", 7 );
DEFINE_STRING( BGl_string3738z00zz__r4_numbers_6_5z00, BgL_bgl_string3738za700za7za7_3837za7, "&bignum->uint64", 15 );
DEFINE_STRING( BGl_string3739z00zz__r4_numbers_6_5z00, BgL_bgl_string3739za700za7za7_3838za7, "&flonum->int32", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_int32zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762int32za7d2za7e3fl3839za7, BGl_z62int32zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zd2zd2envz00zz__r4_numbers_6_5z00, BgL_bgl_za762za7d2za7b0za7za7__r4_3840za7, va_generic_entry, BGl_z62zd2zb0zz__r4_numbers_6_5z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2ze3zd3zd2envze2zz__r4_numbers_6_5z00, BgL_bgl_za7622za7e3za7d3za752za7za7_3841z00, BGl_z622ze3zd3z52zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3820z00zz__r4_numbers_6_5z00, BgL_bgl_string3820za700za7za7_3842za7, "Only radix `10' is legal for floating point number", 50 );
DEFINE_STRING( BGl_string3821z00zz__r4_numbers_6_5z00, BgL_bgl_string3821za700za7za7_3843za7, "__r4_numbers_6_5", 16 );
DEFINE_STRING( BGl_string3740z00zz__r4_numbers_6_5z00, BgL_bgl_string3740za700za7za7_3844za7, "&int32->flonum", 14 );
DEFINE_STRING( BGl_string3741z00zz__r4_numbers_6_5z00, BgL_bgl_string3741za700za7za7_3845za7, "bint32", 6 );
DEFINE_STRING( BGl_string3742z00zz__r4_numbers_6_5z00, BgL_bgl_string3742za700za7za7_3846za7, "&flonum->uint32", 15 );
DEFINE_STRING( BGl_string3743z00zz__r4_numbers_6_5z00, BgL_bgl_string3743za700za7za7_3847za7, "&uint32->flonum", 15 );
DEFINE_STRING( BGl_string3744z00zz__r4_numbers_6_5z00, BgL_bgl_string3744za700za7za7_3848za7, "buint32", 7 );
DEFINE_STRING( BGl_string3745z00zz__r4_numbers_6_5z00, BgL_bgl_string3745za700za7za7_3849za7, "&flonum->int64", 14 );
DEFINE_STRING( BGl_string3746z00zz__r4_numbers_6_5z00, BgL_bgl_string3746za700za7za7_3850za7, "&int64->flonum", 14 );
DEFINE_STRING( BGl_string3747z00zz__r4_numbers_6_5z00, BgL_bgl_string3747za700za7za7_3851za7, "&flonum->uint64", 15 );
DEFINE_STRING( BGl_string3748z00zz__r4_numbers_6_5z00, BgL_bgl_string3748za700za7za7_3852za7, "&uint64->flonum", 15 );
DEFINE_STRING( BGl_string3749z00zz__r4_numbers_6_5z00, BgL_bgl_string3749za700za7za7_3853za7, "2=", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zd3zd2envz01zz__r4_numbers_6_5z00, BgL_bgl_za762za7d3za7b1za7za7__r4_3854za7, va_generic_entry, BGl_z62zd3zb1zz__r4_numbers_6_5z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2zd2zd2envz00zz__r4_numbers_6_5z00, BgL_bgl_za7622za7d2za7b0za7za7__r43855za7, BGl_z622zd2zb0zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_floorzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762floorza762za7za7__r3856z00, BGl_z62floorz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2zd3zd2envz01zz__r4_numbers_6_5z00, BgL_bgl_za7622za7d3za7b1za7za7__r43857za7, BGl_z622zd3zb1zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3750z00zz__r4_numbers_6_5z00, BgL_bgl_string3750za700za7za7_3858za7, "=", 1 );
DEFINE_STRING( BGl_string3751z00zz__r4_numbers_6_5z00, BgL_bgl_string3751za700za7za7_3859za7, "not a number", 12 );
DEFINE_STRING( BGl_string3752z00zz__r4_numbers_6_5z00, BgL_bgl_string3752za700za7za7_3860za7, "not a number5", 13 );
DEFINE_STRING( BGl_string3753z00zz__r4_numbers_6_5z00, BgL_bgl_string3753za700za7za7_3861za7, "=-list", 6 );
DEFINE_STRING( BGl_string3754z00zz__r4_numbers_6_5z00, BgL_bgl_string3754za700za7za7_3862za7, "pair", 4 );
DEFINE_STRING( BGl_string3755z00zz__r4_numbers_6_5z00, BgL_bgl_string3755za700za7za7_3863za7, "2<", 2 );
DEFINE_STRING( BGl_string3756z00zz__r4_numbers_6_5z00, BgL_bgl_string3756za700za7za7_3864za7, "<", 1 );
DEFINE_STRING( BGl_string3757z00zz__r4_numbers_6_5z00, BgL_bgl_string3757za700za7za7_3865za7, "<-list", 6 );
DEFINE_STRING( BGl_string3758z00zz__r4_numbers_6_5z00, BgL_bgl_string3758za700za7za7_3866za7, "2>", 2 );
DEFINE_STRING( BGl_string3759z00zz__r4_numbers_6_5z00, BgL_bgl_string3759za700za7za7_3867za7, ">", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_minzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762minza762za7za7__r4_3868z00, va_generic_entry, BGl_z62minz62zz__r4_numbers_6_5z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_maxzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762maxza762za7za7__r4_3869z00, va_generic_entry, BGl_z62maxz62zz__r4_numbers_6_5z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_numberzf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762numberza7f3za791za73870z00, BGl_z62numberzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3760z00zz__r4_numbers_6_5z00, BgL_bgl_string3760za700za7za7_3871za7, ">-list", 6 );
DEFINE_STRING( BGl_string3761z00zz__r4_numbers_6_5z00, BgL_bgl_string3761za700za7za7_3872za7, "2<=", 3 );
DEFINE_STRING( BGl_string3762z00zz__r4_numbers_6_5z00, BgL_bgl_string3762za700za7za7_3873za7, "<=", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_fixnumzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762fixnumza7d2za7e3f3874za7, BGl_z62fixnumzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3fixnumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3f3875za7, BGl_z62flonumzd2ze3fixnumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3763z00zz__r4_numbers_6_5z00, BgL_bgl_string3763za700za7za7_3876za7, "<=-list", 7 );
DEFINE_STRING( BGl_string3764z00zz__r4_numbers_6_5z00, BgL_bgl_string3764za700za7za7_3877za7, "2>=", 3 );
DEFINE_STRING( BGl_string3765z00zz__r4_numbers_6_5z00, BgL_bgl_string3765za700za7za7_3878za7, ">=", 2 );
DEFINE_STRING( BGl_string3766z00zz__r4_numbers_6_5z00, BgL_bgl_string3766za700za7za7_3879za7, ">=-list", 7 );
DEFINE_STRING( BGl_string3767z00zz__r4_numbers_6_5z00, BgL_bgl_string3767za700za7za7_3880za7, "zero", 4 );
DEFINE_STRING( BGl_string3768z00zz__r4_numbers_6_5z00, BgL_bgl_string3768za700za7za7_3881za7, "positive", 8 );
DEFINE_STRING( BGl_string3769z00zz__r4_numbers_6_5z00, BgL_bgl_string3769za700za7za7_3882za7, "negative", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_atanzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762atanza762za7za7__r43883z00, va_generic_entry, BGl_z62atanz62zz__r4_numbers_6_5z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3int64zd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3i3884za7, BGl_z62flonumzd2ze3int64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3770z00zz__r4_numbers_6_5z00, BgL_bgl_string3770za700za7za7_3885za7, "2max", 4 );
DEFINE_STRING( BGl_string3771z00zz__r4_numbers_6_5z00, BgL_bgl_string3771za700za7za7_3886za7, "2min", 4 );
DEFINE_STRING( BGl_string3772z00zz__r4_numbers_6_5z00, BgL_bgl_string3772za700za7za7_3887za7, "2+", 2 );
DEFINE_STRING( BGl_string3773z00zz__r4_numbers_6_5z00, BgL_bgl_string3773za700za7za7_3888za7, "+", 1 );
DEFINE_STRING( BGl_string3774z00zz__r4_numbers_6_5z00, BgL_bgl_string3774za700za7za7_3889za7, "2*", 2 );
DEFINE_STRING( BGl_string3775z00zz__r4_numbers_6_5z00, BgL_bgl_string3775za700za7za7_3890za7, "*", 1 );
DEFINE_STRING( BGl_string3776z00zz__r4_numbers_6_5z00, BgL_bgl_string3776za700za7za7_3891za7, "2-", 2 );
DEFINE_STRING( BGl_string3777z00zz__r4_numbers_6_5z00, BgL_bgl_string3777za700za7za7_3892za7, "-", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zc3zd3zd2envzc2zz__r4_numbers_6_5z00, BgL_bgl_za762za7c3za7d3za772za7za7__3893z00, va_generic_entry, BGl_z62zc3zd3z72zz__r4_numbers_6_5z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_rationalzf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762rationalza7f3za793894za7, BGl_z62rationalzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3778z00zz__r4_numbers_6_5z00, BgL_bgl_string3778za700za7za7_3895za7, "2/", 2 );
DEFINE_STRING( BGl_string3779z00zz__r4_numbers_6_5z00, BgL_bgl_string3779za700za7za7_3896za7, "/", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tanzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762tanza762za7za7__r4_3897z00, BGl_z62tanz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_log10zd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762log10za762za7za7__r3898z00, BGl_z62log10z62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_REAL( BGl_real3797z00zz__r4_numbers_6_5z00, BgL_bgl_real3797za700za7za7__r3899za7, 0.0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_za2zd2envz70zz__r4_numbers_6_5z00, BgL_bgl_za762za7a2za7c0za7za7__r4_3900za7, va_generic_entry, BGl_z62za2zc0zz__r4_numbers_6_5z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string3780z00zz__r4_numbers_6_5z00, BgL_bgl_string3780za700za7za7_3901za7, "abs", 3 );
DEFINE_STRING( BGl_string3781z00zz__r4_numbers_6_5z00, BgL_bgl_string3781za700za7za7_3902za7, "floor", 5 );
DEFINE_STRING( BGl_string3782z00zz__r4_numbers_6_5z00, BgL_bgl_string3782za700za7za7_3903za7, "ceiling", 7 );
DEFINE_STRING( BGl_string3783z00zz__r4_numbers_6_5z00, BgL_bgl_string3783za700za7za7_3904za7, "truncate", 8 );
DEFINE_STRING( BGl_string3784z00zz__r4_numbers_6_5z00, BgL_bgl_string3784za700za7za7_3905za7, "round", 5 );
DEFINE_STRING( BGl_string3785z00zz__r4_numbers_6_5z00, BgL_bgl_string3785za700za7za7_3906za7, "exp", 3 );
DEFINE_STRING( BGl_string3786z00zz__r4_numbers_6_5z00, BgL_bgl_string3786za700za7za7_3907za7, "log", 3 );
DEFINE_STRING( BGl_string3787z00zz__r4_numbers_6_5z00, BgL_bgl_string3787za700za7za7_3908za7, "log2", 4 );
DEFINE_STRING( BGl_string3788z00zz__r4_numbers_6_5z00, BgL_bgl_string3788za700za7za7_3909za7, "log10", 5 );
DEFINE_STRING( BGl_string3789z00zz__r4_numbers_6_5z00, BgL_bgl_string3789za700za7za7_3910za7, "sin", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2za2zd2envz70zz__r4_numbers_6_5z00, BgL_bgl_za7622za7a2za7c0za7za7__r43911za7, BGl_z622za2zc0zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ze3zd2envz31zz__r4_numbers_6_5z00, BgL_bgl_za762za7e3za781za7za7__r4_3912za7, va_generic_entry, BGl_z62ze3z81zz__r4_numbers_6_5z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_za7erozf3zd2envz86zz__r4_numbers_6_5z00, BgL_bgl_za762za7a7eroza7f3za736za73913za7, BGl_z62za7erozf3z36zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3uint64zd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3u3914za7, BGl_z62flonumzd2ze3uint64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_asinzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762asinza762za7za7__r43915z00, BGl_z62asinz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_expzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762expza762za7za7__r4_3916z00, BGl_z62expz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2ze3zd2envz31zz__r4_numbers_6_5z00, BgL_bgl_za7622za7e3za781za7za7__r43917za7, BGl_z622ze3z81zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_negativezf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762negativeza7f3za793918za7, BGl_z62negativezf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3790z00zz__r4_numbers_6_5z00, BgL_bgl_string3790za700za7za7_3919za7, "cos", 3 );
DEFINE_STRING( BGl_string3791z00zz__r4_numbers_6_5z00, BgL_bgl_string3791za700za7za7_3920za7, "tan", 3 );
DEFINE_STRING( BGl_string3792z00zz__r4_numbers_6_5z00, BgL_bgl_string3792za700za7za7_3921za7, "asin", 4 );
DEFINE_STRING( BGl_string3793z00zz__r4_numbers_6_5z00, BgL_bgl_string3793za700za7za7_3922za7, "acos", 4 );
DEFINE_STRING( BGl_string3794z00zz__r4_numbers_6_5z00, BgL_bgl_string3794za700za7za7_3923za7, "atan", 4 );
DEFINE_STRING( BGl_string3795z00zz__r4_numbers_6_5z00, BgL_bgl_string3795za700za7za7_3924za7, "atanfl", 6 );
DEFINE_STRING( BGl_string3796z00zz__r4_numbers_6_5z00, BgL_bgl_string3796za700za7za7_3925za7, "Domain error", 12 );
DEFINE_STRING( BGl_string3798z00zz__r4_numbers_6_5z00, BgL_bgl_string3798za700za7za7_3926za7, "sqrtfl", 6 );
DEFINE_STRING( BGl_string3799z00zz__r4_numbers_6_5z00, BgL_bgl_string3799za700za7za7_3927za7, "sqrt", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bignumzd2ze3int64zd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762bignumza7d2za7e3i3928za7, BGl_z62bignumzd2ze3int64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sinzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762sinza762za7za7__r4_3929z00, BGl_z62sinz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2zc3zd3zd2envzc2zz__r4_numbers_6_5z00, BgL_bgl_za7622za7c3za7d3za772za7za7_3930z00, BGl_z622zc3zd3z72zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2minzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za7622minza762za7za7__r43931z00, BGl_z622minz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2maxzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za7622maxza762za7za7__r43932z00, BGl_z622maxz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3llongzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3l3933za7, BGl_z62flonumzd2ze3llongz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3bignumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3b3934za7, BGl_z62flonumzd2ze3bignumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_acoszd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762acosza762za7za7__r43935z00, BGl_z62acosz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_coszd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762cosza762za7za7__r4_3936z00, BGl_z62cosz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zb2zd2envz60zz__r4_numbers_6_5z00, BgL_bgl_za762za7b2za7d0za7za7__r4_3937za7, va_generic_entry, BGl_z62zb2zd0zz__r4_numbers_6_5z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zf2zd2envz20zz__r4_numbers_6_5z00, BgL_bgl_za762za7f2za790za7za7__r4_3938za7, va_generic_entry, BGl_z62zf2z90zz__r4_numbers_6_5z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bignumzd2ze3uint64zd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762bignumza7d2za7e3u3939za7, BGl_z62bignumzd2ze3uint64z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_uint64zd2ze3bignumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762uint64za7d2za7e3b3940za7, BGl_z62uint64zd2ze3bignumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2zb2zd2envz60zz__r4_numbers_6_5z00, BgL_bgl_za7622za7b2za7d0za7za7__r43941za7, BGl_z622zb2zd0zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2zf2zd2envz20zz__r4_numbers_6_5z00, BgL_bgl_za7622za7f2za790za7za7__r43942za7, BGl_z622zf2z90zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_numberzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762numberza7d2za7e3f3943za7, BGl_z62numberzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_truncatezd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762truncateza762za7za73944z00, BGl_z62truncatez62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_complexzf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762complexza7f3za7913945za7, BGl_z62complexzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3numberzd2envze3zz__r4_numbers_6_5z00, BgL_bgl__stringza7d2za7e3num3946z00, opt_generic_entry, BGl__stringzd2ze3numberz31zz__r4_numbers_6_5z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_numberzd2ze3stringzd2envze3zz__r4_numbers_6_5z00, BgL_bgl__numberza7d2za7e3str3947z00, opt_generic_entry, BGl__numberzd2ze3stringz31zz__r4_numbers_6_5z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_elongzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762elongza7d2za7e3fl3948za7, BGl_z62elongzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_abszd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762absza762za7za7__r4_3949z00, BGl_z62absz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_llongzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762llongza7d2za7e3fl3950za7, BGl_z62llongzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exptzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762exptza762za7za7__r43951z00, BGl_z62exptz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_int64zd2ze3bignumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762int64za7d2za7e3bi3952za7, BGl_z62int64zd2ze3bignumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inexactzf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762inexactza7f3za7913953za7, BGl_z62inexactzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exactzd2ze3inexactzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762exactza7d2za7e3in3954za7, BGl_z62exactzd2ze3inexactz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3int32zd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3i3955za7, BGl_z62flonumzd2ze3int32z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_positivezf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762positiveza7f3za793956za7, BGl_z62positivezf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exactzf3zd2envz21zz__r4_numbers_6_5z00, BgL_bgl_za762exactza7f3za791za7za73957za7, BGl_z62exactzf3z91zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_REAL( BGl_real3800z00zz__r4_numbers_6_5z00, BgL_bgl_real3800za700za7za7__r3958za7, 1.0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zc3zd2envz11zz__r4_numbers_6_5z00, BgL_bgl_za762za7c3za7a1za7za7__r4_3959za7, va_generic_entry, BGl_z62zc3za1zz__r4_numbers_6_5z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_uint64zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762uint64za7d2za7e3f3960za7, BGl_z62uint64zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_roundzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762roundza762za7za7__r3961z00, BGl_z62roundz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sqrtzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762sqrtza762za7za7__r43962z00, BGl_z62sqrtz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inexactzd2ze3exactzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762inexactza7d2za7e33963za7, BGl_z62inexactzd2ze3exactz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_2zc3zd2envz11zz__r4_numbers_6_5z00, BgL_bgl_za7622za7c3za7a1za7za7__r43964za7, BGl_z622zc3za1zz__r4_numbers_6_5z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3717z00zz__r4_numbers_6_5z00, BgL_bgl_string3717za700za7za7_3965za7, "/tmp/bigloo/runtime/Ieee/number.scm", 35 );
DEFINE_STRING( BGl_string3718z00zz__r4_numbers_6_5z00, BgL_bgl_string3718za700za7za7_3966za7, "number->flonum", 14 );
DEFINE_STRING( BGl_string3719z00zz__r4_numbers_6_5z00, BgL_bgl_string3719za700za7za7_3967za7, "belong", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ze3zd3zd2envze2zz__r4_numbers_6_5z00, BgL_bgl_za762za7e3za7d3za752za7za7__3968z00, va_generic_entry, BGl_z62ze3zd3z52zz__r4_numbers_6_5z00, BUNSPEC, -3 );
#define BGl_real3814z00zz__r4_numbers_6_5z00 bigloo_nan
#define BGl_real3816z00zz__r4_numbers_6_5z00 bigloo_infinity
DEFINE_EXPORT_BGL_PROCEDURE( BGl_uint32zd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762uint32za7d2za7e3f3969za7, BGl_z62uint32zd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3uint32zd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3u3970za7, BGl_z62flonumzd2ze3uint32z53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
#define BGl_real3818z00zz__r4_numbers_6_5z00 bigloo_minfinity
DEFINE_EXPORT_BGL_PROCEDURE( BGl_logzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762logza762za7za7__r4_3971z00, BGl_z62logz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
#define BGl_real3819z00zz__r4_numbers_6_5z00 bigloo_nan
DEFINE_STRING( BGl_string3801z00zz__r4_numbers_6_5z00, BgL_bgl_string3801za700za7za7_3972za7, "expt", 4 );
DEFINE_STRING( BGl_string3720z00zz__r4_numbers_6_5z00, BgL_bgl_string3720za700za7za7_3973za7, "number", 6 );
DEFINE_STRING( BGl_string3721z00zz__r4_numbers_6_5z00, BgL_bgl_string3721za700za7za7_3974za7, "real", 4 );
DEFINE_STRING( BGl_string3803z00zz__r4_numbers_6_5z00, BgL_bgl_string3803za700za7za7_3975za7, "number->string", 14 );
DEFINE_STRING( BGl_string3722z00zz__r4_numbers_6_5z00, BgL_bgl_string3722za700za7za7_3976za7, "&flonum->fixnum", 15 );
DEFINE_STRING( BGl_string3804z00zz__r4_numbers_6_5z00, BgL_bgl_string3804za700za7za7_3977za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string3723z00zz__r4_numbers_6_5z00, BgL_bgl_string3723za700za7za7_3978za7, "&fixnum->flonum", 15 );
DEFINE_STRING( BGl_string3805z00zz__r4_numbers_6_5z00, BgL_bgl_string3805za700za7za7_3979za7, "Argument not a number", 21 );
DEFINE_STRING( BGl_string3724z00zz__r4_numbers_6_5z00, BgL_bgl_string3724za700za7za7_3980za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzd2ze3elongzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762flonumza7d2za7e3e3981za7, BGl_z62flonumzd2ze3elongz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3806z00zz__r4_numbers_6_5z00, BgL_bgl_string3806za700za7za7_3982za7, "bstring", 7 );
DEFINE_STRING( BGl_string3725z00zz__r4_numbers_6_5z00, BgL_bgl_string3725za700za7za7_3983za7, "&flonum->elong", 14 );
DEFINE_STRING( BGl_string3807z00zz__r4_numbers_6_5z00, BgL_bgl_string3807za700za7za7_3984za7, "Illegal radix", 13 );
DEFINE_STRING( BGl_string3726z00zz__r4_numbers_6_5z00, BgL_bgl_string3726za700za7za7_3985za7, "&elong->flonum", 14 );
DEFINE_STRING( BGl_string3727z00zz__r4_numbers_6_5z00, BgL_bgl_string3727za700za7za7_3986za7, "&flonum->llong", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_log2zd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762log2za762za7za7__r43987z00, BGl_z62log2z62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3809z00zz__r4_numbers_6_5z00, BgL_bgl_string3809za700za7za7_3988za7, "string->number", 14 );
DEFINE_STRING( BGl_string3728z00zz__r4_numbers_6_5z00, BgL_bgl_string3728za700za7za7_3989za7, "&llong->flonum", 14 );
DEFINE_STRING( BGl_string3729z00zz__r4_numbers_6_5z00, BgL_bgl_string3729za700za7za7_3990za7, "bllong", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ceilingzd2envzd2zz__r4_numbers_6_5z00, BgL_bgl_za762ceilingza762za7za7_3991z00, BGl_z62ceilingz62zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bignumzd2ze3flonumzd2envze3zz__r4_numbers_6_5z00, BgL_bgl_za762bignumza7d2za7e3f3992za7, BGl_z62bignumzd2ze3flonumz53zz__r4_numbers_6_5z00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00) );
ADD_ROOT( (void *)(&BGl_symbol3802z00zz__r4_numbers_6_5z00) );
ADD_ROOT( (void *)(&BGl_symbol3808z00zz__r4_numbers_6_5z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long BgL_checksumz00_5551, char * BgL_fromz00_5552)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_numbers_6_5z00(); 
BGl_cnstzd2initzd2zz__r4_numbers_6_5z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5z00(); 
return 
BGl_toplevelzd2initzd2zz__r4_numbers_6_5z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_numbers_6_5z00(void)
{
{ /* Ieee/number.scm 18 */
BGl_symbol3802z00zz__r4_numbers_6_5z00 = 
bstring_to_symbol(BGl_string3803z00zz__r4_numbers_6_5z00); 
return ( 
BGl_symbol3808z00zz__r4_numbers_6_5z00 = 
bstring_to_symbol(BGl_string3809z00zz__r4_numbers_6_5z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5z00(void)
{
{ /* Ieee/number.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r4_numbers_6_5z00(void)
{
{ /* Ieee/number.scm 18 */
BGl_za2maxintflza2z00zz__r4_numbers_6_5z00 = 
(double)(BGL_LONG_MAX); 
return ( 
BGl_za2minintflza2z00zz__r4_numbers_6_5z00 = 
(double)(BGL_LONG_MIN), BUNSPEC) ;} 

}



/* number? */
BGL_EXPORTED_DEF bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t BgL_objz00_3)
{
{ /* Ieee/number.scm 247 */
{ /* Ieee/number.scm 248 */
 bool_t BgL__ortest_1012z00_871;
BgL__ortest_1012z00_871 = 
INTEGERP(BgL_objz00_3); 
if(BgL__ortest_1012z00_871)
{ /* Ieee/number.scm 248 */
return BgL__ortest_1012z00_871;}  else 
{ /* Ieee/number.scm 249 */
 bool_t BgL__ortest_1013z00_872;
BgL__ortest_1013z00_872 = 
REALP(BgL_objz00_3); 
if(BgL__ortest_1013z00_872)
{ /* Ieee/number.scm 249 */
return BgL__ortest_1013z00_872;}  else 
{ /* Ieee/number.scm 250 */
 bool_t BgL__ortest_1014z00_873;
BgL__ortest_1014z00_873 = 
ELONGP(BgL_objz00_3); 
if(BgL__ortest_1014z00_873)
{ /* Ieee/number.scm 250 */
return BgL__ortest_1014z00_873;}  else 
{ /* Ieee/number.scm 251 */
 bool_t BgL__ortest_1015z00_874;
BgL__ortest_1015z00_874 = 
LLONGP(BgL_objz00_3); 
if(BgL__ortest_1015z00_874)
{ /* Ieee/number.scm 251 */
return BgL__ortest_1015z00_874;}  else 
{ /* Ieee/number.scm 252 */
 bool_t BgL__ortest_1016z00_875;
BgL__ortest_1016z00_875 = 
BGL_INT8P(BgL_objz00_3); 
if(BgL__ortest_1016z00_875)
{ /* Ieee/number.scm 252 */
return BgL__ortest_1016z00_875;}  else 
{ /* Ieee/number.scm 253 */
 bool_t BgL__ortest_1017z00_876;
BgL__ortest_1017z00_876 = 
BGL_UINT8P(BgL_objz00_3); 
if(BgL__ortest_1017z00_876)
{ /* Ieee/number.scm 253 */
return BgL__ortest_1017z00_876;}  else 
{ /* Ieee/number.scm 254 */
 bool_t BgL__ortest_1018z00_877;
BgL__ortest_1018z00_877 = 
BGL_INT16P(BgL_objz00_3); 
if(BgL__ortest_1018z00_877)
{ /* Ieee/number.scm 254 */
return BgL__ortest_1018z00_877;}  else 
{ /* Ieee/number.scm 255 */
 bool_t BgL__ortest_1019z00_878;
BgL__ortest_1019z00_878 = 
BGL_UINT16P(BgL_objz00_3); 
if(BgL__ortest_1019z00_878)
{ /* Ieee/number.scm 255 */
return BgL__ortest_1019z00_878;}  else 
{ /* Ieee/number.scm 256 */
 bool_t BgL__ortest_1020z00_879;
BgL__ortest_1020z00_879 = 
BGL_INT32P(BgL_objz00_3); 
if(BgL__ortest_1020z00_879)
{ /* Ieee/number.scm 256 */
return BgL__ortest_1020z00_879;}  else 
{ /* Ieee/number.scm 257 */
 bool_t BgL__ortest_1021z00_880;
BgL__ortest_1021z00_880 = 
BGL_UINT32P(BgL_objz00_3); 
if(BgL__ortest_1021z00_880)
{ /* Ieee/number.scm 257 */
return BgL__ortest_1021z00_880;}  else 
{ /* Ieee/number.scm 258 */
 bool_t BgL__ortest_1022z00_881;
BgL__ortest_1022z00_881 = 
BGL_INT64P(BgL_objz00_3); 
if(BgL__ortest_1022z00_881)
{ /* Ieee/number.scm 258 */
return BgL__ortest_1022z00_881;}  else 
{ /* Ieee/number.scm 259 */
 bool_t BgL__ortest_1023z00_882;
BgL__ortest_1023z00_882 = 
BGL_UINT64P(BgL_objz00_3); 
if(BgL__ortest_1023z00_882)
{ /* Ieee/number.scm 259 */
return BgL__ortest_1023z00_882;}  else 
{ /* Ieee/number.scm 259 */
return 
BIGNUMP(BgL_objz00_3);} } } } } } } } } } } } } } 

}



/* &number? */
obj_t BGl_z62numberzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4907, obj_t BgL_objz00_4908)
{
{ /* Ieee/number.scm 247 */
return 
BBOOL(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_objz00_4908));} 

}



/* exact? */
BGL_EXPORTED_DEF bool_t BGl_exactzf3zf3zz__r4_numbers_6_5z00(obj_t BgL_za7za7_4)
{
{ /* Ieee/number.scm 265 */
{ /* Ieee/number.scm 266 */
 bool_t BgL__ortest_1024z00_5540;
BgL__ortest_1024z00_5540 = 
INTEGERP(BgL_za7za7_4); 
if(BgL__ortest_1024z00_5540)
{ /* Ieee/number.scm 266 */
return BgL__ortest_1024z00_5540;}  else 
{ /* Ieee/number.scm 267 */
 bool_t BgL__ortest_1025z00_5541;
BgL__ortest_1025z00_5541 = 
ELONGP(BgL_za7za7_4); 
if(BgL__ortest_1025z00_5541)
{ /* Ieee/number.scm 267 */
return BgL__ortest_1025z00_5541;}  else 
{ /* Ieee/number.scm 268 */
 bool_t BgL__ortest_1026z00_5542;
BgL__ortest_1026z00_5542 = 
LLONGP(BgL_za7za7_4); 
if(BgL__ortest_1026z00_5542)
{ /* Ieee/number.scm 268 */
return BgL__ortest_1026z00_5542;}  else 
{ /* Ieee/number.scm 269 */
 bool_t BgL__ortest_1027z00_5543;
BgL__ortest_1027z00_5543 = 
BGL_INT8P(BgL_za7za7_4); 
if(BgL__ortest_1027z00_5543)
{ /* Ieee/number.scm 269 */
return BgL__ortest_1027z00_5543;}  else 
{ /* Ieee/number.scm 270 */
 bool_t BgL__ortest_1028z00_5544;
BgL__ortest_1028z00_5544 = 
BGL_UINT8P(BgL_za7za7_4); 
if(BgL__ortest_1028z00_5544)
{ /* Ieee/number.scm 270 */
return BgL__ortest_1028z00_5544;}  else 
{ /* Ieee/number.scm 271 */
 bool_t BgL__ortest_1029z00_5545;
BgL__ortest_1029z00_5545 = 
BGL_INT16P(BgL_za7za7_4); 
if(BgL__ortest_1029z00_5545)
{ /* Ieee/number.scm 271 */
return BgL__ortest_1029z00_5545;}  else 
{ /* Ieee/number.scm 272 */
 bool_t BgL__ortest_1030z00_5546;
BgL__ortest_1030z00_5546 = 
BGL_UINT16P(BgL_za7za7_4); 
if(BgL__ortest_1030z00_5546)
{ /* Ieee/number.scm 272 */
return BgL__ortest_1030z00_5546;}  else 
{ /* Ieee/number.scm 273 */
 bool_t BgL__ortest_1031z00_5547;
BgL__ortest_1031z00_5547 = 
BGL_INT32P(BgL_za7za7_4); 
if(BgL__ortest_1031z00_5547)
{ /* Ieee/number.scm 273 */
return BgL__ortest_1031z00_5547;}  else 
{ /* Ieee/number.scm 274 */
 bool_t BgL__ortest_1032z00_5548;
BgL__ortest_1032z00_5548 = 
BGL_UINT32P(BgL_za7za7_4); 
if(BgL__ortest_1032z00_5548)
{ /* Ieee/number.scm 274 */
return BgL__ortest_1032z00_5548;}  else 
{ /* Ieee/number.scm 275 */
 bool_t BgL__ortest_1033z00_5549;
BgL__ortest_1033z00_5549 = 
BGL_INT64P(BgL_za7za7_4); 
if(BgL__ortest_1033z00_5549)
{ /* Ieee/number.scm 275 */
return BgL__ortest_1033z00_5549;}  else 
{ /* Ieee/number.scm 276 */
 bool_t BgL__ortest_1034z00_5550;
BgL__ortest_1034z00_5550 = 
BGL_UINT64P(BgL_za7za7_4); 
if(BgL__ortest_1034z00_5550)
{ /* Ieee/number.scm 276 */
return BgL__ortest_1034z00_5550;}  else 
{ /* Ieee/number.scm 276 */
return 
BIGNUMP(BgL_za7za7_4);} } } } } } } } } } } } } 

}



/* &exact? */
obj_t BGl_z62exactzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4909, obj_t BgL_za7za7_4910)
{
{ /* Ieee/number.scm 265 */
return 
BBOOL(
BGl_exactzf3zf3zz__r4_numbers_6_5z00(BgL_za7za7_4910));} 

}



/* inexact? */
BGL_EXPORTED_DEF bool_t BGl_inexactzf3zf3zz__r4_numbers_6_5z00(obj_t BgL_za7za7_5)
{
{ /* Ieee/number.scm 282 */
return 
REALP(BgL_za7za7_5);} 

}



/* &inexact? */
obj_t BGl_z62inexactzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4911, obj_t BgL_za7za7_4912)
{
{ /* Ieee/number.scm 282 */
return 
BBOOL(
BGl_inexactzf3zf3zz__r4_numbers_6_5z00(BgL_za7za7_4912));} 

}



/* complex? */
BGL_EXPORTED_DEF bool_t BGl_complexzf3zf3zz__r4_numbers_6_5z00(obj_t BgL_xz00_6)
{
{ /* Ieee/number.scm 288 */
BGL_TAIL return 
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_6);} 

}



/* &complex? */
obj_t BGl_z62complexzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4913, obj_t BgL_xz00_4914)
{
{ /* Ieee/number.scm 288 */
return 
BBOOL(
BGl_complexzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_4914));} 

}



/* rational? */
BGL_EXPORTED_DEF bool_t BGl_rationalzf3zf3zz__r4_numbers_6_5z00(obj_t BgL_xz00_7)
{
{ /* Ieee/number.scm 294 */
if(
INTEGERP(BgL_xz00_7))
{ /* Ieee/number.scm 295 */
return ((bool_t)1);}  else 
{ /* Ieee/number.scm 295 */
return 
REALP(BgL_xz00_7);} } 

}



/* &rational? */
obj_t BGl_z62rationalzf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4915, obj_t BgL_xz00_4916)
{
{ /* Ieee/number.scm 294 */
return 
BBOOL(
BGl_rationalzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_4916));} 

}



/* number->flonum */
BGL_EXPORTED_DEF double BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t BgL_xz00_8)
{
{ /* Ieee/number.scm 300 */
if(
INTEGERP(BgL_xz00_8))
{ /* Ieee/number.scm 302 */
return 
(double)(
(long)CINT(BgL_xz00_8));}  else 
{ /* Ieee/number.scm 302 */
if(
BIGNUMP(BgL_xz00_8))
{ /* Ieee/number.scm 303 */
return 
bgl_bignum_to_flonum(BgL_xz00_8);}  else 
{ /* Ieee/number.scm 303 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_8))
{ /* Ieee/number.scm 304 */
 obj_t BgL_arg1122z00_897;
BgL_arg1122z00_897 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_8); 
{ /* Ieee/number.scm 304 */
 long BgL_xz00_2835;
{ /* Ieee/number.scm 304 */
 obj_t BgL_tmpz00_5638;
if(
ELONGP(BgL_arg1122z00_897))
{ /* Ieee/number.scm 304 */
BgL_tmpz00_5638 = BgL_arg1122z00_897
; }  else 
{ 
 obj_t BgL_auxz00_5641;
BgL_auxz00_5641 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(11770L), BGl_string3718z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1122z00_897); 
FAILURE(BgL_auxz00_5641,BFALSE,BFALSE);} 
BgL_xz00_2835 = 
BELONG_TO_LONG(BgL_tmpz00_5638); } 
return 
(double)(BgL_xz00_2835);} }  else 
{ /* Ieee/number.scm 304 */
if(
LLONGP(BgL_xz00_8))
{ /* Ieee/number.scm 305 */
return 
(double)(
BLLONG_TO_LLONG(BgL_xz00_8));}  else 
{ /* Ieee/number.scm 305 */
if(
REALP(BgL_xz00_8))
{ /* Ieee/number.scm 306 */
return 
REAL_TO_DOUBLE(BgL_xz00_8);}  else 
{ /* Ieee/number.scm 307 */
 obj_t BgL_tmpz00_5654;
{ /* Ieee/number.scm 307 */
 obj_t BgL_aux3331z00_5152;
BgL_aux3331z00_5152 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3718z00zz__r4_numbers_6_5z00, BGl_string3720z00zz__r4_numbers_6_5z00, BgL_xz00_8); 
if(
REALP(BgL_aux3331z00_5152))
{ /* Ieee/number.scm 307 */
BgL_tmpz00_5654 = BgL_aux3331z00_5152
; }  else 
{ 
 obj_t BgL_auxz00_5658;
BgL_auxz00_5658 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(11868L), BGl_string3718z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3331z00_5152); 
FAILURE(BgL_auxz00_5658,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_5654);} } } } } } 

}



/* &number->flonum */
obj_t BGl_z62numberzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4917, obj_t BgL_xz00_4918)
{
{ /* Ieee/number.scm 300 */
return 
DOUBLE_TO_REAL(
BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_xz00_4918));} 

}



/* flonum->fixnum */
BGL_EXPORTED_DEF long BGl_flonumzd2ze3fixnumz31zz__r4_numbers_6_5z00(double BgL_xz00_9)
{
{ /* Ieee/number.scm 312 */
return 
(long)(BgL_xz00_9);} 

}



/* &flonum->fixnum */
obj_t BGl_z62flonumzd2ze3fixnumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4919, obj_t BgL_xz00_4920)
{
{ /* Ieee/number.scm 312 */
{ /* Ieee/number.scm 312 */
 long BgL_tmpz00_5666;
{ /* Ieee/number.scm 312 */
 double BgL_auxz00_5667;
{ /* Ieee/number.scm 312 */
 obj_t BgL_tmpz00_5668;
if(
REALP(BgL_xz00_4920))
{ /* Ieee/number.scm 312 */
BgL_tmpz00_5668 = BgL_xz00_4920
; }  else 
{ 
 obj_t BgL_auxz00_5671;
BgL_auxz00_5671 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(12176L), BGl_string3722z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4920); 
FAILURE(BgL_auxz00_5671,BFALSE,BFALSE);} 
BgL_auxz00_5667 = 
REAL_TO_DOUBLE(BgL_tmpz00_5668); } 
BgL_tmpz00_5666 = 
BGl_flonumzd2ze3fixnumz31zz__r4_numbers_6_5z00(BgL_auxz00_5667); } 
return 
BINT(BgL_tmpz00_5666);} } 

}



/* fixnum->flonum */
BGL_EXPORTED_DEF double BGl_fixnumzd2ze3flonumz31zz__r4_numbers_6_5z00(long BgL_xz00_10)
{
{ /* Ieee/number.scm 313 */
return 
(double)(BgL_xz00_10);} 

}



/* &fixnum->flonum */
obj_t BGl_z62fixnumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4921, obj_t BgL_xz00_4922)
{
{ /* Ieee/number.scm 313 */
{ /* Ieee/number.scm 313 */
 double BgL_tmpz00_5679;
{ /* Ieee/number.scm 313 */
 long BgL_auxz00_5680;
{ /* Ieee/number.scm 313 */
 obj_t BgL_tmpz00_5681;
if(
INTEGERP(BgL_xz00_4922))
{ /* Ieee/number.scm 313 */
BgL_tmpz00_5681 = BgL_xz00_4922
; }  else 
{ 
 obj_t BgL_auxz00_5684;
BgL_auxz00_5684 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(12231L), BGl_string3723z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_xz00_4922); 
FAILURE(BgL_auxz00_5684,BFALSE,BFALSE);} 
BgL_auxz00_5680 = 
(long)CINT(BgL_tmpz00_5681); } 
BgL_tmpz00_5679 = 
BGl_fixnumzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5680); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5679);} } 

}



/* flonum->elong */
BGL_EXPORTED_DEF long BGl_flonumzd2ze3elongz31zz__r4_numbers_6_5z00(double BgL_xz00_11)
{
{ /* Ieee/number.scm 318 */
{ /* Ieee/number.scm 318 */
 long BgL_tmpz00_5691;
BgL_tmpz00_5691 = 
(long)(BgL_xz00_11); 
return 
(long)(BgL_tmpz00_5691);} } 

}



/* &flonum->elong */
obj_t BGl_z62flonumzd2ze3elongz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4923, obj_t BgL_xz00_4924)
{
{ /* Ieee/number.scm 318 */
{ /* Ieee/number.scm 318 */
 long BgL_tmpz00_5694;
{ /* Ieee/number.scm 318 */
 double BgL_auxz00_5695;
{ /* Ieee/number.scm 318 */
 obj_t BgL_tmpz00_5696;
if(
REALP(BgL_xz00_4924))
{ /* Ieee/number.scm 318 */
BgL_tmpz00_5696 = BgL_xz00_4924
; }  else 
{ 
 obj_t BgL_auxz00_5699;
BgL_auxz00_5699 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(12517L), BGl_string3725z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4924); 
FAILURE(BgL_auxz00_5699,BFALSE,BFALSE);} 
BgL_auxz00_5695 = 
REAL_TO_DOUBLE(BgL_tmpz00_5696); } 
BgL_tmpz00_5694 = 
BGl_flonumzd2ze3elongz31zz__r4_numbers_6_5z00(BgL_auxz00_5695); } 
return 
make_belong(BgL_tmpz00_5694);} } 

}



/* elong->flonum */
BGL_EXPORTED_DEF double BGl_elongzd2ze3flonumz31zz__r4_numbers_6_5z00(long BgL_xz00_12)
{
{ /* Ieee/number.scm 319 */
return 
(double)(BgL_xz00_12);} 

}



/* &elong->flonum */
obj_t BGl_z62elongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4925, obj_t BgL_xz00_4926)
{
{ /* Ieee/number.scm 319 */
{ /* Ieee/number.scm 319 */
 double BgL_tmpz00_5707;
{ /* Ieee/number.scm 319 */
 long BgL_auxz00_5708;
{ /* Ieee/number.scm 319 */
 obj_t BgL_tmpz00_5709;
if(
ELONGP(BgL_xz00_4926))
{ /* Ieee/number.scm 319 */
BgL_tmpz00_5709 = BgL_xz00_4926
; }  else 
{ 
 obj_t BgL_auxz00_5712;
BgL_auxz00_5712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(12570L), BGl_string3726z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_xz00_4926); 
FAILURE(BgL_auxz00_5712,BFALSE,BFALSE);} 
BgL_auxz00_5708 = 
BELONG_TO_LONG(BgL_tmpz00_5709); } 
BgL_tmpz00_5707 = 
BGl_elongzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5708); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5707);} } 

}



/* flonum->llong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_flonumzd2ze3llongz31zz__r4_numbers_6_5z00(double BgL_xz00_13)
{
{ /* Ieee/number.scm 324 */
return 
(BGL_LONGLONG_T)(BgL_xz00_13);} 

}



/* &flonum->llong */
obj_t BGl_z62flonumzd2ze3llongz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4927, obj_t BgL_xz00_4928)
{
{ /* Ieee/number.scm 324 */
{ /* Ieee/number.scm 324 */
 BGL_LONGLONG_T BgL_tmpz00_5720;
{ /* Ieee/number.scm 324 */
 double BgL_auxz00_5721;
{ /* Ieee/number.scm 324 */
 obj_t BgL_tmpz00_5722;
if(
REALP(BgL_xz00_4928))
{ /* Ieee/number.scm 324 */
BgL_tmpz00_5722 = BgL_xz00_4928
; }  else 
{ 
 obj_t BgL_auxz00_5725;
BgL_auxz00_5725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(12855L), BGl_string3727z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4928); 
FAILURE(BgL_auxz00_5725,BFALSE,BFALSE);} 
BgL_auxz00_5721 = 
REAL_TO_DOUBLE(BgL_tmpz00_5722); } 
BgL_tmpz00_5720 = 
BGl_flonumzd2ze3llongz31zz__r4_numbers_6_5z00(BgL_auxz00_5721); } 
return 
make_bllong(BgL_tmpz00_5720);} } 

}



/* llong->flonum */
BGL_EXPORTED_DEF double BGl_llongzd2ze3flonumz31zz__r4_numbers_6_5z00(BGL_LONGLONG_T BgL_xz00_14)
{
{ /* Ieee/number.scm 325 */
return 
(double)(BgL_xz00_14);} 

}



/* &llong->flonum */
obj_t BGl_z62llongzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4929, obj_t BgL_xz00_4930)
{
{ /* Ieee/number.scm 325 */
{ /* Ieee/number.scm 325 */
 double BgL_tmpz00_5733;
{ /* Ieee/number.scm 325 */
 BGL_LONGLONG_T BgL_auxz00_5734;
{ /* Ieee/number.scm 325 */
 obj_t BgL_tmpz00_5735;
if(
LLONGP(BgL_xz00_4930))
{ /* Ieee/number.scm 325 */
BgL_tmpz00_5735 = BgL_xz00_4930
; }  else 
{ 
 obj_t BgL_auxz00_5738;
BgL_auxz00_5738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(12908L), BGl_string3728z00zz__r4_numbers_6_5z00, BGl_string3729z00zz__r4_numbers_6_5z00, BgL_xz00_4930); 
FAILURE(BgL_auxz00_5738,BFALSE,BFALSE);} 
BgL_auxz00_5734 = 
BLLONG_TO_LLONG(BgL_tmpz00_5735); } 
BgL_tmpz00_5733 = 
BGl_llongzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5734); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5733);} } 

}



/* flonum->bignum */
BGL_EXPORTED_DEF obj_t BGl_flonumzd2ze3bignumz31zz__r4_numbers_6_5z00(double BgL_xz00_15)
{
{ /* Ieee/number.scm 330 */
BGL_TAIL return 
bgl_flonum_to_bignum(BgL_xz00_15);} 

}



/* &flonum->bignum */
obj_t BGl_z62flonumzd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4931, obj_t BgL_xz00_4932)
{
{ /* Ieee/number.scm 330 */
{ /* Ieee/number.scm 330 */
 double BgL_auxz00_5746;
{ /* Ieee/number.scm 330 */
 obj_t BgL_tmpz00_5747;
if(
REALP(BgL_xz00_4932))
{ /* Ieee/number.scm 330 */
BgL_tmpz00_5747 = BgL_xz00_4932
; }  else 
{ 
 obj_t BgL_auxz00_5750;
BgL_auxz00_5750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13194L), BGl_string3730z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4932); 
FAILURE(BgL_auxz00_5750,BFALSE,BFALSE);} 
BgL_auxz00_5746 = 
REAL_TO_DOUBLE(BgL_tmpz00_5747); } 
return 
BGl_flonumzd2ze3bignumz31zz__r4_numbers_6_5z00(BgL_auxz00_5746);} } 

}



/* bignum->flonum */
BGL_EXPORTED_DEF double BGl_bignumzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t BgL_xz00_16)
{
{ /* Ieee/number.scm 331 */
BGL_TAIL return 
bgl_bignum_to_flonum(BgL_xz00_16);} 

}



/* &bignum->flonum */
obj_t BGl_z62bignumzd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4933, obj_t BgL_xz00_4934)
{
{ /* Ieee/number.scm 331 */
{ /* Ieee/number.scm 331 */
 double BgL_tmpz00_5757;
{ /* Ieee/number.scm 331 */
 obj_t BgL_auxz00_5758;
if(
BIGNUMP(BgL_xz00_4934))
{ /* Ieee/number.scm 331 */
BgL_auxz00_5758 = BgL_xz00_4934
; }  else 
{ 
 obj_t BgL_auxz00_5761;
BgL_auxz00_5761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13249L), BGl_string3731z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_xz00_4934); 
FAILURE(BgL_auxz00_5761,BFALSE,BFALSE);} 
BgL_tmpz00_5757 = 
BGl_bignumzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5758); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5757);} } 

}



/* int64->bignum */
BGL_EXPORTED_DEF obj_t BGl_int64zd2ze3bignumz31zz__r4_numbers_6_5z00(int64_t BgL_xz00_17)
{
{ /* Ieee/number.scm 336 */
return 
bgl_int64_to_bignum(
(uint64_t)(BgL_xz00_17));} 

}



/* &int64->bignum */
obj_t BGl_z62int64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4935, obj_t BgL_xz00_4936)
{
{ /* Ieee/number.scm 336 */
{ /* Ieee/number.scm 336 */
 int64_t BgL_auxz00_5769;
{ /* Ieee/number.scm 336 */
 obj_t BgL_tmpz00_5770;
if(
BGL_INT64P(BgL_xz00_4936))
{ /* Ieee/number.scm 336 */
BgL_tmpz00_5770 = BgL_xz00_4936
; }  else 
{ 
 obj_t BgL_auxz00_5773;
BgL_auxz00_5773 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13526L), BGl_string3733z00zz__r4_numbers_6_5z00, BGl_string3734z00zz__r4_numbers_6_5z00, BgL_xz00_4936); 
FAILURE(BgL_auxz00_5773,BFALSE,BFALSE);} 
BgL_auxz00_5769 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5770); } 
return 
BGl_int64zd2ze3bignumz31zz__r4_numbers_6_5z00(BgL_auxz00_5769);} } 

}



/* bignum->int64 */
BGL_EXPORTED_DEF int64_t BGl_bignumzd2ze3int64z31zz__r4_numbers_6_5z00(obj_t BgL_xz00_18)
{
{ /* Ieee/number.scm 337 */
BGL_TAIL return 
bgl_bignum_to_int64(BgL_xz00_18);} 

}



/* &bignum->int64 */
obj_t BGl_z62bignumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4937, obj_t BgL_xz00_4938)
{
{ /* Ieee/number.scm 337 */
{ /* Ieee/number.scm 337 */
 int64_t BgL_tmpz00_5780;
{ /* Ieee/number.scm 337 */
 obj_t BgL_auxz00_5781;
if(
BIGNUMP(BgL_xz00_4938))
{ /* Ieee/number.scm 337 */
BgL_auxz00_5781 = BgL_xz00_4938
; }  else 
{ 
 obj_t BgL_auxz00_5784;
BgL_auxz00_5784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13579L), BGl_string3735z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_xz00_4938); 
FAILURE(BgL_auxz00_5784,BFALSE,BFALSE);} 
BgL_tmpz00_5780 = 
BGl_bignumzd2ze3int64z31zz__r4_numbers_6_5z00(BgL_auxz00_5781); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_5780);} } 

}



/* uint64->bignum */
BGL_EXPORTED_DEF obj_t BGl_uint64zd2ze3bignumz31zz__r4_numbers_6_5z00(uint64_t BgL_xz00_19)
{
{ /* Ieee/number.scm 339 */
BGL_TAIL return 
bgl_uint64_to_bignum(BgL_xz00_19);} 

}



/* &uint64->bignum */
obj_t BGl_z62uint64zd2ze3bignumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4939, obj_t BgL_xz00_4940)
{
{ /* Ieee/number.scm 339 */
{ /* Ieee/number.scm 339 */
 uint64_t BgL_auxz00_5791;
{ /* Ieee/number.scm 339 */
 obj_t BgL_tmpz00_5792;
if(
BGL_UINT64P(BgL_xz00_4940))
{ /* Ieee/number.scm 339 */
BgL_tmpz00_5792 = BgL_xz00_4940
; }  else 
{ 
 obj_t BgL_auxz00_5795;
BgL_auxz00_5795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13634L), BGl_string3736z00zz__r4_numbers_6_5z00, BGl_string3737z00zz__r4_numbers_6_5z00, BgL_xz00_4940); 
FAILURE(BgL_auxz00_5795,BFALSE,BFALSE);} 
BgL_auxz00_5791 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5792); } 
return 
BGl_uint64zd2ze3bignumz31zz__r4_numbers_6_5z00(BgL_auxz00_5791);} } 

}



/* bignum->uint64 */
BGL_EXPORTED_DEF uint64_t BGl_bignumzd2ze3uint64z31zz__r4_numbers_6_5z00(obj_t BgL_xz00_20)
{
{ /* Ieee/number.scm 340 */
BGL_TAIL return 
bgl_bignum_to_uint64(BgL_xz00_20);} 

}



/* &bignum->uint64 */
obj_t BGl_z62bignumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4941, obj_t BgL_xz00_4942)
{
{ /* Ieee/number.scm 340 */
{ /* Ieee/number.scm 340 */
 uint64_t BgL_tmpz00_5802;
{ /* Ieee/number.scm 340 */
 obj_t BgL_auxz00_5803;
if(
BIGNUMP(BgL_xz00_4942))
{ /* Ieee/number.scm 340 */
BgL_auxz00_5803 = BgL_xz00_4942
; }  else 
{ 
 obj_t BgL_auxz00_5806;
BgL_auxz00_5806 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13689L), BGl_string3738z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_xz00_4942); 
FAILURE(BgL_auxz00_5806,BFALSE,BFALSE);} 
BgL_tmpz00_5802 = 
BGl_bignumzd2ze3uint64z31zz__r4_numbers_6_5z00(BgL_auxz00_5803); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_5802);} } 

}



/* flonum->int32 */
BGL_EXPORTED_DEF int32_t BGl_flonumzd2ze3int32z31zz__r4_numbers_6_5z00(double BgL_xz00_21)
{
{ /* Ieee/number.scm 345 */
return 
(int32_t)(BgL_xz00_21);} 

}



/* &flonum->int32 */
obj_t BGl_z62flonumzd2ze3int32z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4943, obj_t BgL_xz00_4944)
{
{ /* Ieee/number.scm 345 */
{ /* Ieee/number.scm 345 */
 int32_t BgL_tmpz00_5813;
{ /* Ieee/number.scm 345 */
 double BgL_auxz00_5814;
{ /* Ieee/number.scm 345 */
 obj_t BgL_tmpz00_5815;
if(
REALP(BgL_xz00_4944))
{ /* Ieee/number.scm 345 */
BgL_tmpz00_5815 = BgL_xz00_4944
; }  else 
{ 
 obj_t BgL_auxz00_5818;
BgL_auxz00_5818 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(13966L), BGl_string3739z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4944); 
FAILURE(BgL_auxz00_5818,BFALSE,BFALSE);} 
BgL_auxz00_5814 = 
REAL_TO_DOUBLE(BgL_tmpz00_5815); } 
BgL_tmpz00_5813 = 
BGl_flonumzd2ze3int32z31zz__r4_numbers_6_5z00(BgL_auxz00_5814); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_5813);} } 

}



/* int32->flonum */
BGL_EXPORTED_DEF double BGl_int32zd2ze3flonumz31zz__r4_numbers_6_5z00(int32_t BgL_xz00_22)
{
{ /* Ieee/number.scm 346 */
return 
(double)(BgL_xz00_22);} 

}



/* &int32->flonum */
obj_t BGl_z62int32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4945, obj_t BgL_xz00_4946)
{
{ /* Ieee/number.scm 346 */
{ /* Ieee/number.scm 346 */
 double BgL_tmpz00_5826;
{ /* Ieee/number.scm 346 */
 int32_t BgL_auxz00_5827;
{ /* Ieee/number.scm 346 */
 obj_t BgL_tmpz00_5828;
if(
BGL_INT32P(BgL_xz00_4946))
{ /* Ieee/number.scm 346 */
BgL_tmpz00_5828 = BgL_xz00_4946
; }  else 
{ 
 obj_t BgL_auxz00_5831;
BgL_auxz00_5831 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14019L), BGl_string3740z00zz__r4_numbers_6_5z00, BGl_string3741z00zz__r4_numbers_6_5z00, BgL_xz00_4946); 
FAILURE(BgL_auxz00_5831,BFALSE,BFALSE);} 
BgL_auxz00_5827 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_5828); } 
BgL_tmpz00_5826 = 
BGl_int32zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5827); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5826);} } 

}



/* flonum->uint32 */
BGL_EXPORTED_DEF uint32_t BGl_flonumzd2ze3uint32z31zz__r4_numbers_6_5z00(double BgL_xz00_23)
{
{ /* Ieee/number.scm 348 */
return 
(uint32_t)(BgL_xz00_23);} 

}



/* &flonum->uint32 */
obj_t BGl_z62flonumzd2ze3uint32z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4947, obj_t BgL_xz00_4948)
{
{ /* Ieee/number.scm 348 */
{ /* Ieee/number.scm 348 */
 uint32_t BgL_tmpz00_5839;
{ /* Ieee/number.scm 348 */
 double BgL_auxz00_5840;
{ /* Ieee/number.scm 348 */
 obj_t BgL_tmpz00_5841;
if(
REALP(BgL_xz00_4948))
{ /* Ieee/number.scm 348 */
BgL_tmpz00_5841 = BgL_xz00_4948
; }  else 
{ 
 obj_t BgL_auxz00_5844;
BgL_auxz00_5844 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14074L), BGl_string3742z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4948); 
FAILURE(BgL_auxz00_5844,BFALSE,BFALSE);} 
BgL_auxz00_5840 = 
REAL_TO_DOUBLE(BgL_tmpz00_5841); } 
BgL_tmpz00_5839 = 
BGl_flonumzd2ze3uint32z31zz__r4_numbers_6_5z00(BgL_auxz00_5840); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_5839);} } 

}



/* uint32->flonum */
BGL_EXPORTED_DEF double BGl_uint32zd2ze3flonumz31zz__r4_numbers_6_5z00(uint32_t BgL_xz00_24)
{
{ /* Ieee/number.scm 349 */
return 
(double)(BgL_xz00_24);} 

}



/* &uint32->flonum */
obj_t BGl_z62uint32zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4949, obj_t BgL_xz00_4950)
{
{ /* Ieee/number.scm 349 */
{ /* Ieee/number.scm 349 */
 double BgL_tmpz00_5852;
{ /* Ieee/number.scm 349 */
 uint32_t BgL_auxz00_5853;
{ /* Ieee/number.scm 349 */
 obj_t BgL_tmpz00_5854;
if(
BGL_UINT32P(BgL_xz00_4950))
{ /* Ieee/number.scm 349 */
BgL_tmpz00_5854 = BgL_xz00_4950
; }  else 
{ 
 obj_t BgL_auxz00_5857;
BgL_auxz00_5857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14129L), BGl_string3743z00zz__r4_numbers_6_5z00, BGl_string3744z00zz__r4_numbers_6_5z00, BgL_xz00_4950); 
FAILURE(BgL_auxz00_5857,BFALSE,BFALSE);} 
BgL_auxz00_5853 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_5854); } 
BgL_tmpz00_5852 = 
BGl_uint32zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5853); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5852);} } 

}



/* flonum->int64 */
BGL_EXPORTED_DEF int64_t BGl_flonumzd2ze3int64z31zz__r4_numbers_6_5z00(double BgL_xz00_25)
{
{ /* Ieee/number.scm 354 */
return 
(int64_t)(BgL_xz00_25);} 

}



/* &flonum->int64 */
obj_t BGl_z62flonumzd2ze3int64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4951, obj_t BgL_xz00_4952)
{
{ /* Ieee/number.scm 354 */
{ /* Ieee/number.scm 354 */
 int64_t BgL_tmpz00_5865;
{ /* Ieee/number.scm 354 */
 double BgL_auxz00_5866;
{ /* Ieee/number.scm 354 */
 obj_t BgL_tmpz00_5867;
if(
REALP(BgL_xz00_4952))
{ /* Ieee/number.scm 354 */
BgL_tmpz00_5867 = BgL_xz00_4952
; }  else 
{ 
 obj_t BgL_auxz00_5870;
BgL_auxz00_5870 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14406L), BGl_string3745z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4952); 
FAILURE(BgL_auxz00_5870,BFALSE,BFALSE);} 
BgL_auxz00_5866 = 
REAL_TO_DOUBLE(BgL_tmpz00_5867); } 
BgL_tmpz00_5865 = 
BGl_flonumzd2ze3int64z31zz__r4_numbers_6_5z00(BgL_auxz00_5866); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_5865);} } 

}



/* int64->flonum */
BGL_EXPORTED_DEF double BGl_int64zd2ze3flonumz31zz__r4_numbers_6_5z00(int64_t BgL_xz00_26)
{
{ /* Ieee/number.scm 355 */
return 
(double)(BgL_xz00_26);} 

}



/* &int64->flonum */
obj_t BGl_z62int64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4953, obj_t BgL_xz00_4954)
{
{ /* Ieee/number.scm 355 */
{ /* Ieee/number.scm 355 */
 double BgL_tmpz00_5878;
{ /* Ieee/number.scm 355 */
 int64_t BgL_auxz00_5879;
{ /* Ieee/number.scm 355 */
 obj_t BgL_tmpz00_5880;
if(
BGL_INT64P(BgL_xz00_4954))
{ /* Ieee/number.scm 355 */
BgL_tmpz00_5880 = BgL_xz00_4954
; }  else 
{ 
 obj_t BgL_auxz00_5883;
BgL_auxz00_5883 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14459L), BGl_string3746z00zz__r4_numbers_6_5z00, BGl_string3734z00zz__r4_numbers_6_5z00, BgL_xz00_4954); 
FAILURE(BgL_auxz00_5883,BFALSE,BFALSE);} 
BgL_auxz00_5879 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5880); } 
BgL_tmpz00_5878 = 
BGl_int64zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5879); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5878);} } 

}



/* flonum->uint64 */
BGL_EXPORTED_DEF uint64_t BGl_flonumzd2ze3uint64z31zz__r4_numbers_6_5z00(double BgL_xz00_27)
{
{ /* Ieee/number.scm 357 */
return 
(uint64_t)(BgL_xz00_27);} 

}



/* &flonum->uint64 */
obj_t BGl_z62flonumzd2ze3uint64z53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4955, obj_t BgL_xz00_4956)
{
{ /* Ieee/number.scm 357 */
{ /* Ieee/number.scm 357 */
 uint64_t BgL_tmpz00_5891;
{ /* Ieee/number.scm 357 */
 double BgL_auxz00_5892;
{ /* Ieee/number.scm 357 */
 obj_t BgL_tmpz00_5893;
if(
REALP(BgL_xz00_4956))
{ /* Ieee/number.scm 357 */
BgL_tmpz00_5893 = BgL_xz00_4956
; }  else 
{ 
 obj_t BgL_auxz00_5896;
BgL_auxz00_5896 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14514L), BGl_string3747z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_xz00_4956); 
FAILURE(BgL_auxz00_5896,BFALSE,BFALSE);} 
BgL_auxz00_5892 = 
REAL_TO_DOUBLE(BgL_tmpz00_5893); } 
BgL_tmpz00_5891 = 
BGl_flonumzd2ze3uint64z31zz__r4_numbers_6_5z00(BgL_auxz00_5892); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_5891);} } 

}



/* uint64->flonum */
BGL_EXPORTED_DEF double BGl_uint64zd2ze3flonumz31zz__r4_numbers_6_5z00(uint64_t BgL_xz00_28)
{
{ /* Ieee/number.scm 358 */
return 
(double)(BgL_xz00_28);} 

}



/* &uint64->flonum */
obj_t BGl_z62uint64zd2ze3flonumz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_4957, obj_t BgL_xz00_4958)
{
{ /* Ieee/number.scm 358 */
{ /* Ieee/number.scm 358 */
 double BgL_tmpz00_5904;
{ /* Ieee/number.scm 358 */
 uint64_t BgL_auxz00_5905;
{ /* Ieee/number.scm 358 */
 obj_t BgL_tmpz00_5906;
if(
BGL_UINT64P(BgL_xz00_4958))
{ /* Ieee/number.scm 358 */
BgL_tmpz00_5906 = BgL_xz00_4958
; }  else 
{ 
 obj_t BgL_auxz00_5909;
BgL_auxz00_5909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(14569L), BGl_string3748z00zz__r4_numbers_6_5z00, BGl_string3737z00zz__r4_numbers_6_5z00, BgL_xz00_4958); 
FAILURE(BgL_auxz00_5909,BFALSE,BFALSE);} 
BgL_auxz00_5905 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5906); } 
BgL_tmpz00_5904 = 
BGl_uint64zd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_auxz00_5905); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5904);} } 

}



/* $subelong->elong */
obj_t BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(obj_t BgL_xz00_29)
{
{ /* Ieee/number.scm 363 */
if(
ELONGP(BgL_xz00_29))
{ /* Ieee/number.scm 367 */
return BgL_xz00_29;}  else 
{ /* Ieee/number.scm 367 */
if(
BGL_INT8P(BgL_xz00_29))
{ /* Ieee/number.scm 368 */
 long BgL_arg1129z00_903;
{ /* Ieee/number.scm 368 */
 int8_t BgL_xz00_2837;
BgL_xz00_2837 = 
BGL_BINT8_TO_INT8(BgL_xz00_29); 
{ /* Ieee/number.scm 368 */
 long BgL_arg3252z00_2838;
BgL_arg3252z00_2838 = 
(long)(BgL_xz00_2837); 
BgL_arg1129z00_903 = 
(long)(BgL_arg3252z00_2838); } } 
{ /* Ieee/number.scm 368 */
 long BgL_tmpz00_5923;
BgL_tmpz00_5923 = 
(long)(BgL_arg1129z00_903); 
return 
make_belong(BgL_tmpz00_5923);} }  else 
{ /* Ieee/number.scm 368 */
if(
BGL_UINT8P(BgL_xz00_29))
{ /* Ieee/number.scm 369 */
 long BgL_arg1131z00_905;
{ /* Ieee/number.scm 369 */
 uint8_t BgL_xz00_2841;
BgL_xz00_2841 = 
BGL_BUINT8_TO_UINT8(BgL_xz00_29); 
{ /* Ieee/number.scm 369 */
 long BgL_arg3251z00_2842;
BgL_arg3251z00_2842 = 
(long)(BgL_xz00_2841); 
BgL_arg1131z00_905 = 
(long)(BgL_arg3251z00_2842); } } 
{ /* Ieee/number.scm 369 */
 long BgL_tmpz00_5931;
BgL_tmpz00_5931 = 
(long)(BgL_arg1131z00_905); 
return 
make_belong(BgL_tmpz00_5931);} }  else 
{ /* Ieee/number.scm 369 */
if(
BGL_INT16P(BgL_xz00_29))
{ /* Ieee/number.scm 370 */
 long BgL_arg1137z00_907;
{ /* Ieee/number.scm 370 */
 int16_t BgL_xz00_2845;
BgL_xz00_2845 = 
BGL_BINT16_TO_INT16(BgL_xz00_29); 
{ /* Ieee/number.scm 370 */
 long BgL_arg3250z00_2846;
BgL_arg3250z00_2846 = 
(long)(BgL_xz00_2845); 
BgL_arg1137z00_907 = 
(long)(BgL_arg3250z00_2846); } } 
{ /* Ieee/number.scm 370 */
 long BgL_tmpz00_5939;
BgL_tmpz00_5939 = 
(long)(BgL_arg1137z00_907); 
return 
make_belong(BgL_tmpz00_5939);} }  else 
{ /* Ieee/number.scm 370 */
if(
BGL_UINT16P(BgL_xz00_29))
{ /* Ieee/number.scm 371 */
 long BgL_arg1140z00_909;
{ /* Ieee/number.scm 371 */
 uint16_t BgL_xz00_2849;
BgL_xz00_2849 = 
BGL_BUINT16_TO_UINT16(BgL_xz00_29); 
{ /* Ieee/number.scm 371 */
 long BgL_arg3249z00_2850;
BgL_arg3249z00_2850 = 
(long)(BgL_xz00_2849); 
BgL_arg1140z00_909 = 
(long)(BgL_arg3249z00_2850); } } 
{ /* Ieee/number.scm 371 */
 long BgL_tmpz00_5947;
BgL_tmpz00_5947 = 
(long)(BgL_arg1140z00_909); 
return 
make_belong(BgL_tmpz00_5947);} }  else 
{ /* Ieee/number.scm 371 */
if(
BGL_INT32P(BgL_xz00_29))
{ /* Ieee/number.scm 372 */
 long BgL_arg1142z00_911;
{ /* Ieee/number.scm 372 */
 int32_t BgL_xz00_2853;
BgL_xz00_2853 = 
BGL_BINT32_TO_INT32(BgL_xz00_29); 
{ /* Ieee/number.scm 372 */
 long BgL_arg3247z00_2854;
BgL_arg3247z00_2854 = 
(long)(BgL_xz00_2853); 
BgL_arg1142z00_911 = 
(long)(BgL_arg3247z00_2854); } } 
{ /* Ieee/number.scm 372 */
 long BgL_tmpz00_5955;
BgL_tmpz00_5955 = 
(long)(BgL_arg1142z00_911); 
return 
make_belong(BgL_tmpz00_5955);} }  else 
{ /* Ieee/number.scm 372 */
if(
BGL_UINT32P(BgL_xz00_29))
{ /* Ieee/number.scm 373 */
 long BgL_arg1145z00_913;
{ /* Ieee/number.scm 373 */
 uint32_t BgL_xz00_2857;
BgL_xz00_2857 = 
BGL_BUINT32_TO_UINT32(BgL_xz00_29); 
BgL_arg1145z00_913 = 
(long)(BgL_xz00_2857); } 
{ /* Ieee/number.scm 373 */
 long BgL_tmpz00_5962;
BgL_tmpz00_5962 = 
(long)(BgL_arg1145z00_913); 
return 
make_belong(BgL_tmpz00_5962);} }  else 
{ /* Ieee/number.scm 373 */
if(
BGL_INT64P(BgL_xz00_29))
{ /* Ieee/number.scm 374 */
 long BgL_arg1148z00_915;
{ /* Ieee/number.scm 374 */
 int64_t BgL_xz00_2859;
BgL_xz00_2859 = 
BGL_BINT64_TO_INT64(BgL_xz00_29); 
{ /* Ieee/number.scm 374 */
 long BgL_arg3246z00_2860;
BgL_arg3246z00_2860 = 
(long)(BgL_xz00_2859); 
BgL_arg1148z00_915 = 
(long)(BgL_arg3246z00_2860); } } 
{ /* Ieee/number.scm 374 */
 long BgL_tmpz00_5970;
BgL_tmpz00_5970 = 
(long)(BgL_arg1148z00_915); 
return 
make_belong(BgL_tmpz00_5970);} }  else 
{ /* Ieee/number.scm 374 */
return BFALSE;} } } } } } } } } 

}



/* $subelong? */
bool_t BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(obj_t BgL_xz00_31)
{
{ /* Ieee/number.scm 394 */
{ /* Ieee/number.scm 397 */
 bool_t BgL__ortest_1035z00_916;
BgL__ortest_1035z00_916 = 
ELONGP(BgL_xz00_31); 
if(BgL__ortest_1035z00_916)
{ /* Ieee/number.scm 397 */
return BgL__ortest_1035z00_916;}  else 
{ /* Ieee/number.scm 397 */
 bool_t BgL__ortest_1036z00_917;
BgL__ortest_1036z00_917 = 
BGL_INT8P(BgL_xz00_31); 
if(BgL__ortest_1036z00_917)
{ /* Ieee/number.scm 397 */
return BgL__ortest_1036z00_917;}  else 
{ /* Ieee/number.scm 397 */
 bool_t BgL__ortest_1037z00_918;
BgL__ortest_1037z00_918 = 
BGL_UINT8P(BgL_xz00_31); 
if(BgL__ortest_1037z00_918)
{ /* Ieee/number.scm 397 */
return BgL__ortest_1037z00_918;}  else 
{ /* Ieee/number.scm 397 */
 bool_t BgL__ortest_1038z00_919;
BgL__ortest_1038z00_919 = 
BGL_INT16P(BgL_xz00_31); 
if(BgL__ortest_1038z00_919)
{ /* Ieee/number.scm 397 */
return BgL__ortest_1038z00_919;}  else 
{ /* Ieee/number.scm 397 */
 bool_t BgL__ortest_1039z00_920;
BgL__ortest_1039z00_920 = 
BGL_UINT16P(BgL_xz00_31); 
if(BgL__ortest_1039z00_920)
{ /* Ieee/number.scm 397 */
return BgL__ortest_1039z00_920;}  else 
{ /* Ieee/number.scm 397 */
 bool_t BgL__ortest_1040z00_921;
BgL__ortest_1040z00_921 = 
BGL_INT32P(BgL_xz00_31); 
if(BgL__ortest_1040z00_921)
{ /* Ieee/number.scm 397 */
return BgL__ortest_1040z00_921;}  else 
{ /* Ieee/number.scm 398 */
 bool_t BgL__ortest_1041z00_922;
BgL__ortest_1041z00_922 = 
BGL_UINT32P(BgL_xz00_31); 
if(BgL__ortest_1041z00_922)
{ /* Ieee/number.scm 398 */
return BgL__ortest_1041z00_922;}  else 
{ /* Ieee/number.scm 398 */
return 
BGL_INT64P(BgL_xz00_31);} } } } } } } } } 

}



/* 2= */
BGL_EXPORTED_DEF bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t BgL_xz00_33, obj_t BgL_yz00_34)
{
{ /* Ieee/number.scm 529 */
if(
INTEGERP(BgL_xz00_33))
{ /* Ieee/number.scm 530 */
if(
INTEGERP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(long)CINT(BgL_xz00_33)==
(long)CINT(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(double)(
(long)CINT(BgL_xz00_33))==
REAL_TO_DOUBLE(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 long BgL_arg1154z00_928; obj_t BgL_arg1157z00_929;
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6003;
BgL_tmpz00_6003 = 
(long)CINT(BgL_xz00_33); 
BgL_arg1154z00_928 = 
(long)(BgL_tmpz00_6003); } 
BgL_arg1157z00_929 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_34); 
{ /* Ieee/number.scm 530 */
 long BgL_n2z00_2869;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6007;
if(
ELONGP(BgL_arg1157z00_929))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6007 = BgL_arg1157z00_929
; }  else 
{ 
 obj_t BgL_auxz00_6010;
BgL_auxz00_6010 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1157z00_929); 
FAILURE(BgL_auxz00_6010,BFALSE,BFALSE);} 
BgL_n2z00_2869 = 
BELONG_TO_LONG(BgL_tmpz00_6007); } 
return 
(BgL_arg1154z00_928==BgL_n2z00_2869);} }  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1162z00_931;
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6018;
BgL_tmpz00_6018 = 
(long)CINT(BgL_xz00_33); 
BgL_arg1162z00_931 = 
LONG_TO_LLONG(BgL_tmpz00_6018); } 
return 
(BgL_arg1162z00_931==
BLLONG_TO_LLONG(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_arg1166z00_934;
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1171z00_935;
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6025;
BgL_tmpz00_6025 = 
(long)CINT(BgL_xz00_33); 
BgL_arg1171z00_935 = 
LONG_TO_LLONG(BgL_tmpz00_6025); } 
BgL_arg1166z00_934 = 
(uint64_t)(BgL_arg1171z00_935); } 
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n2z00_2876;
BgL_n2z00_2876 = 
BGL_BINT64_TO_INT64(BgL_yz00_34); 
return 
(BgL_arg1166z00_934==BgL_n2z00_2876);} }  else 
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_33)), BgL_yz00_34))==0L);}  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_34));} } } } } } }  else 
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_xz00_33))
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_33)==
REAL_TO_DOUBLE(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
INTEGERP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_33)==
(double)(
(long)CINT(BgL_yz00_34)));}  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 double BgL_arg1189z00_943;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1190z00_944;
BgL_arg1190z00_944 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_34); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6056;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6057;
if(
ELONGP(BgL_arg1190z00_944))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6057 = BgL_arg1190z00_944
; }  else 
{ 
 obj_t BgL_auxz00_6060;
BgL_auxz00_6060 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1190z00_944); 
FAILURE(BgL_auxz00_6060,BFALSE,BFALSE);} 
BgL_tmpz00_6056 = 
BELONG_TO_LONG(BgL_tmpz00_6057); } 
BgL_arg1189z00_943 = 
(double)(BgL_tmpz00_6056); } } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_33)==BgL_arg1189z00_943);}  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_33)==
(double)(
BLLONG_TO_LLONG(BgL_yz00_34)));}  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 double BgL_arg1196z00_949;
{ /* Ieee/number.scm 530 */
 uint64_t BgL_xz00_2890;
BgL_xz00_2890 = 
BGL_BINT64_TO_INT64(BgL_yz00_34); 
BgL_arg1196z00_949 = 
(double)(BgL_xz00_2890); } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_33)==BgL_arg1196z00_949);}  else 
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_33)==
bgl_bignum_to_flonum(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_34));} } } } } } }  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_33))
{ /* Ieee/number.scm 530 */
if(
INTEGERP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1201z00_954; long BgL_arg1202z00_955;
BgL_arg1201z00_954 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_33); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6092;
BgL_tmpz00_6092 = 
(long)CINT(BgL_yz00_34); 
BgL_arg1202z00_955 = 
(long)(BgL_tmpz00_6092); } 
{ /* Ieee/number.scm 530 */
 long BgL_n1z00_2897;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6095;
if(
ELONGP(BgL_arg1201z00_954))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6095 = BgL_arg1201z00_954
; }  else 
{ 
 obj_t BgL_auxz00_6098;
BgL_auxz00_6098 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1201z00_954); 
FAILURE(BgL_auxz00_6098,BFALSE,BFALSE);} 
BgL_n1z00_2897 = 
BELONG_TO_LONG(BgL_tmpz00_6095); } 
return 
(BgL_n1z00_2897==BgL_arg1202z00_955);} }  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1206z00_957; obj_t BgL_arg1208z00_958;
BgL_arg1206z00_957 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_33); 
BgL_arg1208z00_958 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_34); 
{ /* Ieee/number.scm 530 */
 long BgL_n1z00_2899; long BgL_n2z00_2900;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6108;
if(
ELONGP(BgL_arg1206z00_957))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6108 = BgL_arg1206z00_957
; }  else 
{ 
 obj_t BgL_auxz00_6111;
BgL_auxz00_6111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1206z00_957); 
FAILURE(BgL_auxz00_6111,BFALSE,BFALSE);} 
BgL_n1z00_2899 = 
BELONG_TO_LONG(BgL_tmpz00_6108); } 
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6116;
if(
ELONGP(BgL_arg1208z00_958))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6116 = BgL_arg1208z00_958
; }  else 
{ 
 obj_t BgL_auxz00_6119;
BgL_auxz00_6119 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1208z00_958); 
FAILURE(BgL_auxz00_6119,BFALSE,BFALSE);} 
BgL_n2z00_2900 = 
BELONG_TO_LONG(BgL_tmpz00_6116); } 
return 
(BgL_n1z00_2899==BgL_n2z00_2900);} }  else 
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 double BgL_arg1210z00_960;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1212z00_961;
BgL_arg1212z00_961 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_33); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6128;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6129;
if(
ELONGP(BgL_arg1212z00_961))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6129 = BgL_arg1212z00_961
; }  else 
{ 
 obj_t BgL_auxz00_6132;
BgL_auxz00_6132 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1212z00_961); 
FAILURE(BgL_auxz00_6132,BFALSE,BFALSE);} 
BgL_tmpz00_6128 = 
BELONG_TO_LONG(BgL_tmpz00_6129); } 
BgL_arg1210z00_960 = 
(double)(BgL_tmpz00_6128); } } 
return 
(BgL_arg1210z00_960==
REAL_TO_DOUBLE(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1215z00_963;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1218z00_965;
BgL_arg1218z00_965 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_33); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6143;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6144;
if(
ELONGP(BgL_arg1218z00_965))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6144 = BgL_arg1218z00_965
; }  else 
{ 
 obj_t BgL_auxz00_6147;
BgL_auxz00_6147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1218z00_965); 
FAILURE(BgL_auxz00_6147,BFALSE,BFALSE);} 
BgL_tmpz00_6143 = 
BELONG_TO_LONG(BgL_tmpz00_6144); } 
BgL_arg1215z00_963 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6143); } } 
return 
(BgL_arg1215z00_963==
BLLONG_TO_LLONG(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_arg1220z00_967;
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1221z00_968;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1223z00_969;
BgL_arg1223z00_969 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_33); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6158;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6159;
if(
ELONGP(BgL_arg1223z00_969))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6159 = BgL_arg1223z00_969
; }  else 
{ 
 obj_t BgL_auxz00_6162;
BgL_auxz00_6162 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1223z00_969); 
FAILURE(BgL_auxz00_6162,BFALSE,BFALSE);} 
BgL_tmpz00_6158 = 
BELONG_TO_LONG(BgL_tmpz00_6159); } 
BgL_arg1221z00_968 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6158); } } 
BgL_arg1220z00_967 = 
(uint64_t)(BgL_arg1221z00_968); } 
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n2z00_2907;
BgL_n2z00_2907 = 
BGL_BINT64_TO_INT64(BgL_yz00_34); 
return 
(BgL_arg1220z00_967==BgL_n2z00_2907);} }  else 
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1225z00_971;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1226z00_972;
BgL_arg1226z00_972 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_33); 
{ /* Ieee/number.scm 530 */
 long BgL_xz00_2908;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6174;
if(
ELONGP(BgL_arg1226z00_972))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6174 = BgL_arg1226z00_972
; }  else 
{ 
 obj_t BgL_auxz00_6177;
BgL_auxz00_6177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1226z00_972); 
FAILURE(BgL_auxz00_6177,BFALSE,BFALSE);} 
BgL_xz00_2908 = 
BELONG_TO_LONG(BgL_tmpz00_6174); } 
BgL_arg1225z00_971 = 
bgl_long_to_bignum(BgL_xz00_2908); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_arg1225z00_971, BgL_yz00_34))==0L);}  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_34));} } } } } } }  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_xz00_33))
{ /* Ieee/number.scm 530 */
if(
INTEGERP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1230z00_976;
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6192;
BgL_tmpz00_6192 = 
(long)CINT(BgL_yz00_34); 
BgL_arg1230z00_976 = 
LONG_TO_LLONG(BgL_tmpz00_6192); } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_33)==BgL_arg1230z00_976);}  else 
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_33))==
REAL_TO_DOUBLE(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_33)==
BLLONG_TO_LLONG(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1239z00_984;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1242z00_985;
BgL_arg1242z00_985 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_34); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6211;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6212;
if(
ELONGP(BgL_arg1242z00_985))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6212 = BgL_arg1242z00_985
; }  else 
{ 
 obj_t BgL_auxz00_6215;
BgL_auxz00_6215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1242z00_985); 
FAILURE(BgL_auxz00_6215,BFALSE,BFALSE);} 
BgL_tmpz00_6211 = 
BELONG_TO_LONG(BgL_tmpz00_6212); } 
BgL_arg1239z00_984 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6211); } } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_33)==BgL_arg1239z00_984);}  else 
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_33)), BgL_yz00_34))==0L);}  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_arg1252z00_990;
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_tmpz00_6232;
BgL_tmpz00_6232 = 
BLLONG_TO_LLONG(BgL_xz00_33); 
BgL_arg1252z00_990 = 
(uint64_t)(BgL_tmpz00_6232); } 
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n2z00_2929;
BgL_n2z00_2929 = 
BGL_BINT64_TO_INT64(BgL_yz00_34); 
return 
(BgL_arg1252z00_990==BgL_n2z00_2929);} }  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_34));} } } } } } }  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_xz00_33))
{ /* Ieee/number.scm 530 */
if(
INTEGERP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_arg1268z00_993;
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6243;
BgL_tmpz00_6243 = 
(long)CINT(BgL_yz00_34); 
BgL_arg1268z00_993 = 
(uint64_t)(BgL_tmpz00_6243); } 
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n1z00_2931;
BgL_n1z00_2931 = 
BGL_BINT64_TO_INT64(BgL_xz00_33); 
return 
(BgL_n1z00_2931==BgL_arg1268z00_993);} }  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n1z00_2933; uint64_t BgL_n2z00_2934;
BgL_n1z00_2933 = 
BGL_BINT64_TO_INT64(BgL_xz00_33); 
BgL_n2z00_2934 = 
BGL_BINT64_TO_INT64(BgL_yz00_34); 
return 
(BgL_n1z00_2933==BgL_n2z00_2934);}  else 
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 double BgL_arg1272z00_996;
{ /* Ieee/number.scm 530 */
 uint64_t BgL_tmpz00_6255;
BgL_tmpz00_6255 = 
BGL_BINT64_TO_INT64(BgL_xz00_33); 
BgL_arg1272z00_996 = 
(double)(BgL_tmpz00_6255); } 
return 
(BgL_arg1272z00_996==
REAL_TO_DOUBLE(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_arg1284z00_998;
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_tmpz00_6262;
BgL_tmpz00_6262 = 
BLLONG_TO_LLONG(BgL_yz00_34); 
BgL_arg1284z00_998 = 
(uint64_t)(BgL_tmpz00_6262); } 
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n1z00_2938;
BgL_n1z00_2938 = 
BGL_BINT64_TO_INT64(BgL_xz00_33); 
return 
(BgL_n1z00_2938==BgL_arg1284z00_998);} }  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 uint64_t BgL_arg1306z00_1001;
{ /* Ieee/number.scm 530 */
 BGL_LONGLONG_T BgL_arg1307z00_1002;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1308z00_1003;
BgL_arg1308z00_1003 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_34); 
{ /* Ieee/number.scm 530 */
 long BgL_tmpz00_6270;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6271;
if(
ELONGP(BgL_arg1308z00_1003))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6271 = BgL_arg1308z00_1003
; }  else 
{ 
 obj_t BgL_auxz00_6274;
BgL_auxz00_6274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1308z00_1003); 
FAILURE(BgL_auxz00_6274,BFALSE,BFALSE);} 
BgL_tmpz00_6270 = 
BELONG_TO_LONG(BgL_tmpz00_6271); } 
BgL_arg1307z00_1002 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6270); } } 
BgL_arg1306z00_1001 = 
(uint64_t)(BgL_arg1307z00_1002); } 
{ /* Ieee/number.scm 530 */
 uint64_t BgL_n1z00_2941;
BgL_n1z00_2941 = 
BGL_BINT64_TO_INT64(BgL_xz00_33); 
return 
(BgL_n1z00_2941==BgL_arg1306z00_1001);} }  else 
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 long BgL_n1z00_2946;
BgL_n1z00_2946 = 
(long)(
bgl_bignum_cmp(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_33)), BgL_yz00_34)); 
return 
(BgL_n1z00_2946==0L);}  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_34));} } } } } } }  else 
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_xz00_33))
{ /* Ieee/number.scm 530 */
if(
BIGNUMP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_33, BgL_yz00_34))==0L);}  else 
{ /* Ieee/number.scm 530 */
if(
INTEGERP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_33, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_34))))==0L);}  else 
{ /* Ieee/number.scm 530 */
if(
REALP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
bgl_bignum_to_flonum(BgL_xz00_33)==
REAL_TO_DOUBLE(BgL_yz00_34));}  else 
{ /* Ieee/number.scm 530 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1318z00_1013;
{ /* Ieee/number.scm 530 */
 obj_t BgL_arg1319z00_1014;
BgL_arg1319z00_1014 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_34); 
{ /* Ieee/number.scm 530 */
 long BgL_xz00_2959;
{ /* Ieee/number.scm 530 */
 obj_t BgL_tmpz00_6314;
if(
ELONGP(BgL_arg1319z00_1014))
{ /* Ieee/number.scm 530 */
BgL_tmpz00_6314 = BgL_arg1319z00_1014
; }  else 
{ 
 obj_t BgL_auxz00_6317;
BgL_auxz00_6317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20419L), BGl_string3749z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1319z00_1014); 
FAILURE(BgL_auxz00_6317,BFALSE,BFALSE);} 
BgL_xz00_2959 = 
BELONG_TO_LONG(BgL_tmpz00_6314); } 
BgL_arg1318z00_1013 = 
bgl_long_to_bignum(BgL_xz00_2959); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_33, BgL_arg1318z00_1013))==0L);}  else 
{ /* Ieee/number.scm 530 */
if(
LLONGP(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_33, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_34))))==0L);}  else 
{ /* Ieee/number.scm 530 */
if(
BGL_UINT64P(BgL_yz00_34))
{ /* Ieee/number.scm 530 */
 long BgL_n1z00_2972;
BgL_n1z00_2972 = 
(long)(
bgl_bignum_cmp(BgL_xz00_33, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_34)))); 
return 
(BgL_n1z00_2972==0L);}  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_34));} } } } } } }  else 
{ /* Ieee/number.scm 530 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3750z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_33));} } } } } } } 

}



/* &2= */
obj_t BGl_z622zd3zb1zz__r4_numbers_6_5z00(obj_t BgL_envz00_4959, obj_t BgL_xz00_4960, obj_t BgL_yz00_4961)
{
{ /* Ieee/number.scm 529 */
return 
BBOOL(
BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_4960, BgL_yz00_4961));} 

}



/* = */
BGL_EXPORTED_DEF bool_t BGl_zd3zd3zz__r4_numbers_6_5z00(obj_t BgL_xz00_35, obj_t BgL_yz00_36, obj_t BgL_za7za7_37)
{
{ /* Ieee/number.scm 535 */
{ 
 obj_t BgL_xz00_1022; obj_t BgL_za7za7_1023;
if(
BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_35, BgL_yz00_36))
{ /* Ieee/number.scm 541 */
BgL_xz00_1022 = BgL_yz00_36; 
BgL_za7za7_1023 = BgL_za7za7_37; 
BgL_zc3z04anonymousza31326ze3z87_1024:
if(
NULLP(BgL_za7za7_1023))
{ /* Ieee/number.scm 538 */
return ((bool_t)1);}  else 
{ /* Ieee/number.scm 539 */
 bool_t BgL_test4116z00_6350;
{ /* Ieee/number.scm 539 */
 obj_t BgL_arg1332z00_1029;
{ /* Ieee/number.scm 539 */
 obj_t BgL_pairz00_2973;
if(
PAIRP(BgL_za7za7_1023))
{ /* Ieee/number.scm 539 */
BgL_pairz00_2973 = BgL_za7za7_1023; }  else 
{ 
 obj_t BgL_auxz00_6353;
BgL_auxz00_6353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20742L), BGl_string3753z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1023); 
FAILURE(BgL_auxz00_6353,BFALSE,BFALSE);} 
BgL_arg1332z00_1029 = 
CAR(BgL_pairz00_2973); } 
BgL_test4116z00_6350 = 
BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_1022, BgL_arg1332z00_1029); } 
if(BgL_test4116z00_6350)
{ 
 obj_t BgL_za7za7_6359;
{ /* Ieee/number.scm 539 */
 obj_t BgL_pairz00_2974;
if(
PAIRP(BgL_za7za7_1023))
{ /* Ieee/number.scm 539 */
BgL_pairz00_2974 = BgL_za7za7_1023; }  else 
{ 
 obj_t BgL_auxz00_6362;
BgL_auxz00_6362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(20761L), BGl_string3753z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1023); 
FAILURE(BgL_auxz00_6362,BFALSE,BFALSE);} 
BgL_za7za7_6359 = 
CDR(BgL_pairz00_2974); } 
BgL_za7za7_1023 = BgL_za7za7_6359; 
goto BgL_zc3z04anonymousza31326ze3z87_1024;}  else 
{ /* Ieee/number.scm 539 */
return ((bool_t)0);} } }  else 
{ /* Ieee/number.scm 541 */
return ((bool_t)0);} } } 

}



/* &= */
obj_t BGl_z62zd3zb1zz__r4_numbers_6_5z00(obj_t BgL_envz00_4962, obj_t BgL_xz00_4963, obj_t BgL_yz00_4964, obj_t BgL_za7za7_4965)
{
{ /* Ieee/number.scm 535 */
return 
BBOOL(
BGl_zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_4963, BgL_yz00_4964, BgL_za7za7_4965));} 

}



/* 2< */
BGL_EXPORTED_DEF bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t BgL_xz00_38, obj_t BgL_yz00_39)
{
{ /* Ieee/number.scm 546 */
if(
INTEGERP(BgL_xz00_38))
{ /* Ieee/number.scm 547 */
if(
INTEGERP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(long)CINT(BgL_xz00_38)<
(long)CINT(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(double)(
(long)CINT(BgL_xz00_38))<
REAL_TO_DOUBLE(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 long BgL_arg1338z00_1036; obj_t BgL_arg1339z00_1037;
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6384;
BgL_tmpz00_6384 = 
(long)CINT(BgL_xz00_38); 
BgL_arg1338z00_1036 = 
(long)(BgL_tmpz00_6384); } 
BgL_arg1339z00_1037 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_39); 
{ /* Ieee/number.scm 547 */
 long BgL_n2z00_2981;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6388;
if(
ELONGP(BgL_arg1339z00_1037))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6388 = BgL_arg1339z00_1037
; }  else 
{ 
 obj_t BgL_auxz00_6391;
BgL_auxz00_6391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1339z00_1037); 
FAILURE(BgL_auxz00_6391,BFALSE,BFALSE);} 
BgL_n2z00_2981 = 
BELONG_TO_LONG(BgL_tmpz00_6388); } 
return 
(BgL_arg1338z00_1036<BgL_n2z00_2981);} }  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1341z00_1039;
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6399;
BgL_tmpz00_6399 = 
(long)CINT(BgL_xz00_38); 
BgL_arg1341z00_1039 = 
LONG_TO_LLONG(BgL_tmpz00_6399); } 
return 
(BgL_arg1341z00_1039<
BLLONG_TO_LLONG(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_arg1344z00_1042;
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1346z00_1043;
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6406;
BgL_tmpz00_6406 = 
(long)CINT(BgL_xz00_38); 
BgL_arg1346z00_1043 = 
LONG_TO_LLONG(BgL_tmpz00_6406); } 
BgL_arg1344z00_1042 = 
(uint64_t)(BgL_arg1346z00_1043); } 
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n2z00_2988;
BgL_n2z00_2988 = 
BGL_BINT64_TO_INT64(BgL_yz00_39); 
return 
(BgL_arg1344z00_1042<BgL_n2z00_2988);} }  else 
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_38)), BgL_yz00_39))<0L);}  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_39));} } } } } } }  else 
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_xz00_38))
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_38)<
REAL_TO_DOUBLE(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
INTEGERP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_38)<
(double)(
(long)CINT(BgL_yz00_39)));}  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 double BgL_arg1354z00_1051;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1356z00_1052;
BgL_arg1356z00_1052 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_39); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6437;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6438;
if(
ELONGP(BgL_arg1356z00_1052))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6438 = BgL_arg1356z00_1052
; }  else 
{ 
 obj_t BgL_auxz00_6441;
BgL_auxz00_6441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1356z00_1052); 
FAILURE(BgL_auxz00_6441,BFALSE,BFALSE);} 
BgL_tmpz00_6437 = 
BELONG_TO_LONG(BgL_tmpz00_6438); } 
BgL_arg1354z00_1051 = 
(double)(BgL_tmpz00_6437); } } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_38)<BgL_arg1354z00_1051);}  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_38)<
(double)(
BLLONG_TO_LLONG(BgL_yz00_39)));}  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 double BgL_arg1361z00_1057;
{ /* Ieee/number.scm 547 */
 uint64_t BgL_xz00_3002;
BgL_xz00_3002 = 
BGL_BINT64_TO_INT64(BgL_yz00_39); 
BgL_arg1361z00_1057 = 
(double)(BgL_xz00_3002); } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_38)<BgL_arg1361z00_1057);}  else 
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_38)<
bgl_bignum_to_flonum(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_39));} } } } } } }  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_38))
{ /* Ieee/number.scm 547 */
if(
INTEGERP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1366z00_1062; long BgL_arg1367z00_1063;
BgL_arg1366z00_1062 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_38); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6473;
BgL_tmpz00_6473 = 
(long)CINT(BgL_yz00_39); 
BgL_arg1367z00_1063 = 
(long)(BgL_tmpz00_6473); } 
{ /* Ieee/number.scm 547 */
 long BgL_n1z00_3009;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6476;
if(
ELONGP(BgL_arg1366z00_1062))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6476 = BgL_arg1366z00_1062
; }  else 
{ 
 obj_t BgL_auxz00_6479;
BgL_auxz00_6479 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1366z00_1062); 
FAILURE(BgL_auxz00_6479,BFALSE,BFALSE);} 
BgL_n1z00_3009 = 
BELONG_TO_LONG(BgL_tmpz00_6476); } 
return 
(BgL_n1z00_3009<BgL_arg1367z00_1063);} }  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1369z00_1065; obj_t BgL_arg1370z00_1066;
BgL_arg1369z00_1065 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_38); 
BgL_arg1370z00_1066 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_39); 
{ /* Ieee/number.scm 547 */
 long BgL_n1z00_3011; long BgL_n2z00_3012;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6489;
if(
ELONGP(BgL_arg1369z00_1065))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6489 = BgL_arg1369z00_1065
; }  else 
{ 
 obj_t BgL_auxz00_6492;
BgL_auxz00_6492 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1369z00_1065); 
FAILURE(BgL_auxz00_6492,BFALSE,BFALSE);} 
BgL_n1z00_3011 = 
BELONG_TO_LONG(BgL_tmpz00_6489); } 
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6497;
if(
ELONGP(BgL_arg1370z00_1066))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6497 = BgL_arg1370z00_1066
; }  else 
{ 
 obj_t BgL_auxz00_6500;
BgL_auxz00_6500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1370z00_1066); 
FAILURE(BgL_auxz00_6500,BFALSE,BFALSE);} 
BgL_n2z00_3012 = 
BELONG_TO_LONG(BgL_tmpz00_6497); } 
return 
(BgL_n1z00_3011<BgL_n2z00_3012);} }  else 
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 double BgL_arg1372z00_1068;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1373z00_1069;
BgL_arg1373z00_1069 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_38); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6509;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6510;
if(
ELONGP(BgL_arg1373z00_1069))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6510 = BgL_arg1373z00_1069
; }  else 
{ 
 obj_t BgL_auxz00_6513;
BgL_auxz00_6513 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1373z00_1069); 
FAILURE(BgL_auxz00_6513,BFALSE,BFALSE);} 
BgL_tmpz00_6509 = 
BELONG_TO_LONG(BgL_tmpz00_6510); } 
BgL_arg1372z00_1068 = 
(double)(BgL_tmpz00_6509); } } 
return 
(BgL_arg1372z00_1068<
REAL_TO_DOUBLE(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1375z00_1071;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1377z00_1073;
BgL_arg1377z00_1073 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_38); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6524;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6525;
if(
ELONGP(BgL_arg1377z00_1073))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6525 = BgL_arg1377z00_1073
; }  else 
{ 
 obj_t BgL_auxz00_6528;
BgL_auxz00_6528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1377z00_1073); 
FAILURE(BgL_auxz00_6528,BFALSE,BFALSE);} 
BgL_tmpz00_6524 = 
BELONG_TO_LONG(BgL_tmpz00_6525); } 
BgL_arg1375z00_1071 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6524); } } 
return 
(BgL_arg1375z00_1071<
BLLONG_TO_LLONG(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_arg1379z00_1075;
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1380z00_1076;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1382z00_1077;
BgL_arg1382z00_1077 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_38); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6539;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6540;
if(
ELONGP(BgL_arg1382z00_1077))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6540 = BgL_arg1382z00_1077
; }  else 
{ 
 obj_t BgL_auxz00_6543;
BgL_auxz00_6543 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1382z00_1077); 
FAILURE(BgL_auxz00_6543,BFALSE,BFALSE);} 
BgL_tmpz00_6539 = 
BELONG_TO_LONG(BgL_tmpz00_6540); } 
BgL_arg1380z00_1076 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6539); } } 
BgL_arg1379z00_1075 = 
(uint64_t)(BgL_arg1380z00_1076); } 
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n2z00_3019;
BgL_n2z00_3019 = 
BGL_BINT64_TO_INT64(BgL_yz00_39); 
return 
(BgL_arg1379z00_1075<BgL_n2z00_3019);} }  else 
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1384z00_1079;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1387z00_1080;
BgL_arg1387z00_1080 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_38); 
{ /* Ieee/number.scm 547 */
 long BgL_xz00_3020;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6555;
if(
ELONGP(BgL_arg1387z00_1080))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6555 = BgL_arg1387z00_1080
; }  else 
{ 
 obj_t BgL_auxz00_6558;
BgL_auxz00_6558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1387z00_1080); 
FAILURE(BgL_auxz00_6558,BFALSE,BFALSE);} 
BgL_xz00_3020 = 
BELONG_TO_LONG(BgL_tmpz00_6555); } 
BgL_arg1384z00_1079 = 
bgl_long_to_bignum(BgL_xz00_3020); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_arg1384z00_1079, BgL_yz00_39))<0L);}  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_39));} } } } } } }  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_xz00_38))
{ /* Ieee/number.scm 547 */
if(
INTEGERP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1391z00_1084;
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6573;
BgL_tmpz00_6573 = 
(long)CINT(BgL_yz00_39); 
BgL_arg1391z00_1084 = 
LONG_TO_LLONG(BgL_tmpz00_6573); } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_38)<BgL_arg1391z00_1084);}  else 
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_38))<
REAL_TO_DOUBLE(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_38)<
BLLONG_TO_LLONG(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1400z00_1092;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1401z00_1093;
BgL_arg1401z00_1093 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_39); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6592;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6593;
if(
ELONGP(BgL_arg1401z00_1093))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6593 = BgL_arg1401z00_1093
; }  else 
{ 
 obj_t BgL_auxz00_6596;
BgL_auxz00_6596 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1401z00_1093); 
FAILURE(BgL_auxz00_6596,BFALSE,BFALSE);} 
BgL_tmpz00_6592 = 
BELONG_TO_LONG(BgL_tmpz00_6593); } 
BgL_arg1400z00_1092 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6592); } } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_38)<BgL_arg1400z00_1092);}  else 
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_38)), BgL_yz00_39))<0L);}  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_arg1406z00_1098;
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_tmpz00_6613;
BgL_tmpz00_6613 = 
BLLONG_TO_LLONG(BgL_xz00_38); 
BgL_arg1406z00_1098 = 
(uint64_t)(BgL_tmpz00_6613); } 
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n2z00_3041;
BgL_n2z00_3041 = 
BGL_BINT64_TO_INT64(BgL_yz00_39); 
return 
(BgL_arg1406z00_1098<BgL_n2z00_3041);} }  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_39));} } } } } } }  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_xz00_38))
{ /* Ieee/number.scm 547 */
if(
INTEGERP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_arg1410z00_1101;
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6624;
BgL_tmpz00_6624 = 
(long)CINT(BgL_yz00_39); 
BgL_arg1410z00_1101 = 
(uint64_t)(BgL_tmpz00_6624); } 
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n1z00_3043;
BgL_n1z00_3043 = 
BGL_BINT64_TO_INT64(BgL_xz00_38); 
return 
(BgL_n1z00_3043<BgL_arg1410z00_1101);} }  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n1z00_3045; uint64_t BgL_n2z00_3046;
BgL_n1z00_3045 = 
BGL_BINT64_TO_INT64(BgL_xz00_38); 
BgL_n2z00_3046 = 
BGL_BINT64_TO_INT64(BgL_yz00_39); 
return 
(BgL_n1z00_3045<BgL_n2z00_3046);}  else 
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 double BgL_arg1413z00_1104;
{ /* Ieee/number.scm 547 */
 uint64_t BgL_tmpz00_6636;
BgL_tmpz00_6636 = 
BGL_BINT64_TO_INT64(BgL_xz00_38); 
BgL_arg1413z00_1104 = 
(double)(BgL_tmpz00_6636); } 
return 
(BgL_arg1413z00_1104<
REAL_TO_DOUBLE(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_arg1415z00_1106;
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_tmpz00_6643;
BgL_tmpz00_6643 = 
BLLONG_TO_LLONG(BgL_yz00_39); 
BgL_arg1415z00_1106 = 
(uint64_t)(BgL_tmpz00_6643); } 
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n1z00_3050;
BgL_n1z00_3050 = 
BGL_BINT64_TO_INT64(BgL_xz00_38); 
return 
(BgL_n1z00_3050<BgL_arg1415z00_1106);} }  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 uint64_t BgL_arg1418z00_1109;
{ /* Ieee/number.scm 547 */
 BGL_LONGLONG_T BgL_arg1419z00_1110;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1420z00_1111;
BgL_arg1420z00_1111 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_39); 
{ /* Ieee/number.scm 547 */
 long BgL_tmpz00_6651;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6652;
if(
ELONGP(BgL_arg1420z00_1111))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6652 = BgL_arg1420z00_1111
; }  else 
{ 
 obj_t BgL_auxz00_6655;
BgL_auxz00_6655 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1420z00_1111); 
FAILURE(BgL_auxz00_6655,BFALSE,BFALSE);} 
BgL_tmpz00_6651 = 
BELONG_TO_LONG(BgL_tmpz00_6652); } 
BgL_arg1419z00_1110 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6651); } } 
BgL_arg1418z00_1109 = 
(uint64_t)(BgL_arg1419z00_1110); } 
{ /* Ieee/number.scm 547 */
 uint64_t BgL_n1z00_3053;
BgL_n1z00_3053 = 
BGL_BINT64_TO_INT64(BgL_xz00_38); 
return 
(BgL_n1z00_3053<BgL_arg1418z00_1109);} }  else 
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 long BgL_n1z00_3058;
BgL_n1z00_3058 = 
(long)(
bgl_bignum_cmp(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_38)), BgL_yz00_39)); 
return 
(BgL_n1z00_3058<0L);}  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_39));} } } } } } }  else 
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_xz00_38))
{ /* Ieee/number.scm 547 */
if(
BIGNUMP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_38, BgL_yz00_39))<0L);}  else 
{ /* Ieee/number.scm 547 */
if(
INTEGERP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_38, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_39))))<0L);}  else 
{ /* Ieee/number.scm 547 */
if(
REALP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
bgl_bignum_to_flonum(BgL_xz00_38)<
REAL_TO_DOUBLE(BgL_yz00_39));}  else 
{ /* Ieee/number.scm 547 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1430z00_1121;
{ /* Ieee/number.scm 547 */
 obj_t BgL_arg1431z00_1122;
BgL_arg1431z00_1122 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_39); 
{ /* Ieee/number.scm 547 */
 long BgL_xz00_3071;
{ /* Ieee/number.scm 547 */
 obj_t BgL_tmpz00_6695;
if(
ELONGP(BgL_arg1431z00_1122))
{ /* Ieee/number.scm 547 */
BgL_tmpz00_6695 = BgL_arg1431z00_1122
; }  else 
{ 
 obj_t BgL_auxz00_6698;
BgL_auxz00_6698 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21056L), BGl_string3755z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1431z00_1122); 
FAILURE(BgL_auxz00_6698,BFALSE,BFALSE);} 
BgL_xz00_3071 = 
BELONG_TO_LONG(BgL_tmpz00_6695); } 
BgL_arg1430z00_1121 = 
bgl_long_to_bignum(BgL_xz00_3071); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_38, BgL_arg1430z00_1121))<0L);}  else 
{ /* Ieee/number.scm 547 */
if(
LLONGP(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_38, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_39))))<0L);}  else 
{ /* Ieee/number.scm 547 */
if(
BGL_UINT64P(BgL_yz00_39))
{ /* Ieee/number.scm 547 */
 long BgL_n1z00_3084;
BgL_n1z00_3084 = 
(long)(
bgl_bignum_cmp(BgL_xz00_38, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_39)))); 
return 
(BgL_n1z00_3084<0L);}  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_39));} } } } } } }  else 
{ /* Ieee/number.scm 547 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3756z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_38));} } } } } } } 

}



/* &2< */
obj_t BGl_z622zc3za1zz__r4_numbers_6_5z00(obj_t BgL_envz00_4966, obj_t BgL_xz00_4967, obj_t BgL_yz00_4968)
{
{ /* Ieee/number.scm 546 */
return 
BBOOL(
BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_4967, BgL_yz00_4968));} 

}



/* < */
BGL_EXPORTED_DEF bool_t BGl_zc3zc3zz__r4_numbers_6_5z00(obj_t BgL_xz00_40, obj_t BgL_yz00_41, obj_t BgL_za7za7_42)
{
{ /* Ieee/number.scm 552 */
{ 
 obj_t BgL_xz00_1130; obj_t BgL_za7za7_1131;
if(
BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_40, BgL_yz00_41))
{ /* Ieee/number.scm 558 */
BgL_xz00_1130 = BgL_yz00_41; 
BgL_za7za7_1131 = BgL_za7za7_42; 
BgL_zc3z04anonymousza31438ze3z87_1132:
if(
NULLP(BgL_za7za7_1131))
{ /* Ieee/number.scm 555 */
return ((bool_t)1);}  else 
{ /* Ieee/number.scm 556 */
 bool_t BgL_test4175z00_6731;
{ /* Ieee/number.scm 556 */
 obj_t BgL_arg1444z00_1138;
{ /* Ieee/number.scm 556 */
 obj_t BgL_pairz00_3085;
if(
PAIRP(BgL_za7za7_1131))
{ /* Ieee/number.scm 556 */
BgL_pairz00_3085 = BgL_za7za7_1131; }  else 
{ 
 obj_t BgL_auxz00_6734;
BgL_auxz00_6734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21379L), BGl_string3757z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1131); 
FAILURE(BgL_auxz00_6734,BFALSE,BFALSE);} 
BgL_arg1444z00_1138 = 
CAR(BgL_pairz00_3085); } 
BgL_test4175z00_6731 = 
BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_1130, BgL_arg1444z00_1138); } 
if(BgL_test4175z00_6731)
{ 
 obj_t BgL_za7za7_6748; obj_t BgL_xz00_6740;
{ /* Ieee/number.scm 556 */
 obj_t BgL_pairz00_3086;
if(
PAIRP(BgL_za7za7_1131))
{ /* Ieee/number.scm 556 */
BgL_pairz00_3086 = BgL_za7za7_1131; }  else 
{ 
 obj_t BgL_auxz00_6743;
BgL_auxz00_6743 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21396L), BGl_string3757z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1131); 
FAILURE(BgL_auxz00_6743,BFALSE,BFALSE);} 
BgL_xz00_6740 = 
CAR(BgL_pairz00_3086); } 
{ /* Ieee/number.scm 556 */
 obj_t BgL_pairz00_3087;
if(
PAIRP(BgL_za7za7_1131))
{ /* Ieee/number.scm 556 */
BgL_pairz00_3087 = BgL_za7za7_1131; }  else 
{ 
 obj_t BgL_auxz00_6751;
BgL_auxz00_6751 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21404L), BGl_string3757z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1131); 
FAILURE(BgL_auxz00_6751,BFALSE,BFALSE);} 
BgL_za7za7_6748 = 
CDR(BgL_pairz00_3087); } 
BgL_za7za7_1131 = BgL_za7za7_6748; 
BgL_xz00_1130 = BgL_xz00_6740; 
goto BgL_zc3z04anonymousza31438ze3z87_1132;}  else 
{ /* Ieee/number.scm 556 */
return ((bool_t)0);} } }  else 
{ /* Ieee/number.scm 558 */
return ((bool_t)0);} } } 

}



/* &< */
obj_t BGl_z62zc3za1zz__r4_numbers_6_5z00(obj_t BgL_envz00_4969, obj_t BgL_xz00_4970, obj_t BgL_yz00_4971, obj_t BgL_za7za7_4972)
{
{ /* Ieee/number.scm 552 */
return 
BBOOL(
BGl_zc3zc3zz__r4_numbers_6_5z00(BgL_xz00_4970, BgL_yz00_4971, BgL_za7za7_4972));} 

}



/* 2> */
BGL_EXPORTED_DEF bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t BgL_xz00_43, obj_t BgL_yz00_44)
{
{ /* Ieee/number.scm 563 */
if(
INTEGERP(BgL_xz00_43))
{ /* Ieee/number.scm 564 */
if(
INTEGERP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(long)CINT(BgL_xz00_43)>
(long)CINT(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(double)(
(long)CINT(BgL_xz00_43))>
REAL_TO_DOUBLE(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 long BgL_arg1450z00_1145; obj_t BgL_arg1451z00_1146;
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6773;
BgL_tmpz00_6773 = 
(long)CINT(BgL_xz00_43); 
BgL_arg1450z00_1145 = 
(long)(BgL_tmpz00_6773); } 
BgL_arg1451z00_1146 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_44); 
{ /* Ieee/number.scm 564 */
 long BgL_n2z00_3094;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6777;
if(
ELONGP(BgL_arg1451z00_1146))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6777 = BgL_arg1451z00_1146
; }  else 
{ 
 obj_t BgL_auxz00_6780;
BgL_auxz00_6780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1451z00_1146); 
FAILURE(BgL_auxz00_6780,BFALSE,BFALSE);} 
BgL_n2z00_3094 = 
BELONG_TO_LONG(BgL_tmpz00_6777); } 
return 
(BgL_arg1450z00_1145>BgL_n2z00_3094);} }  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1453z00_1148;
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6788;
BgL_tmpz00_6788 = 
(long)CINT(BgL_xz00_43); 
BgL_arg1453z00_1148 = 
LONG_TO_LLONG(BgL_tmpz00_6788); } 
return 
(BgL_arg1453z00_1148>
BLLONG_TO_LLONG(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_arg1456z00_1151;
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1457z00_1152;
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6795;
BgL_tmpz00_6795 = 
(long)CINT(BgL_xz00_43); 
BgL_arg1457z00_1152 = 
LONG_TO_LLONG(BgL_tmpz00_6795); } 
BgL_arg1456z00_1151 = 
(uint64_t)(BgL_arg1457z00_1152); } 
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n2z00_3101;
BgL_n2z00_3101 = 
BGL_BINT64_TO_INT64(BgL_yz00_44); 
return 
(BgL_arg1456z00_1151>BgL_n2z00_3101);} }  else 
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_43)), BgL_yz00_44))>0L);}  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_44));} } } } } } }  else 
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_xz00_43))
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_43)>
REAL_TO_DOUBLE(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
INTEGERP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_43)>
(double)(
(long)CINT(BgL_yz00_44)));}  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 double BgL_arg1465z00_1160;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1466z00_1161;
BgL_arg1466z00_1161 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_44); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6826;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6827;
if(
ELONGP(BgL_arg1466z00_1161))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6827 = BgL_arg1466z00_1161
; }  else 
{ 
 obj_t BgL_auxz00_6830;
BgL_auxz00_6830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1466z00_1161); 
FAILURE(BgL_auxz00_6830,BFALSE,BFALSE);} 
BgL_tmpz00_6826 = 
BELONG_TO_LONG(BgL_tmpz00_6827); } 
BgL_arg1465z00_1160 = 
(double)(BgL_tmpz00_6826); } } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_43)>BgL_arg1465z00_1160);}  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_43)>
(double)(
BLLONG_TO_LLONG(BgL_yz00_44)));}  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 double BgL_arg1472z00_1166;
{ /* Ieee/number.scm 564 */
 uint64_t BgL_xz00_3115;
BgL_xz00_3115 = 
BGL_BINT64_TO_INT64(BgL_yz00_44); 
BgL_arg1472z00_1166 = 
(double)(BgL_xz00_3115); } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_43)>BgL_arg1472z00_1166);}  else 
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_43)>
bgl_bignum_to_flonum(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_44));} } } } } } }  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_43))
{ /* Ieee/number.scm 564 */
if(
INTEGERP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1477z00_1171; long BgL_arg1478z00_1172;
BgL_arg1477z00_1171 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_43); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6862;
BgL_tmpz00_6862 = 
(long)CINT(BgL_yz00_44); 
BgL_arg1478z00_1172 = 
(long)(BgL_tmpz00_6862); } 
{ /* Ieee/number.scm 564 */
 long BgL_n1z00_3122;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6865;
if(
ELONGP(BgL_arg1477z00_1171))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6865 = BgL_arg1477z00_1171
; }  else 
{ 
 obj_t BgL_auxz00_6868;
BgL_auxz00_6868 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1477z00_1171); 
FAILURE(BgL_auxz00_6868,BFALSE,BFALSE);} 
BgL_n1z00_3122 = 
BELONG_TO_LONG(BgL_tmpz00_6865); } 
return 
(BgL_n1z00_3122>BgL_arg1478z00_1172);} }  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1480z00_1174; obj_t BgL_arg1481z00_1175;
BgL_arg1480z00_1174 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_43); 
BgL_arg1481z00_1175 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_44); 
{ /* Ieee/number.scm 564 */
 long BgL_n1z00_3124; long BgL_n2z00_3125;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6878;
if(
ELONGP(BgL_arg1480z00_1174))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6878 = BgL_arg1480z00_1174
; }  else 
{ 
 obj_t BgL_auxz00_6881;
BgL_auxz00_6881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1480z00_1174); 
FAILURE(BgL_auxz00_6881,BFALSE,BFALSE);} 
BgL_n1z00_3124 = 
BELONG_TO_LONG(BgL_tmpz00_6878); } 
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6886;
if(
ELONGP(BgL_arg1481z00_1175))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6886 = BgL_arg1481z00_1175
; }  else 
{ 
 obj_t BgL_auxz00_6889;
BgL_auxz00_6889 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1481z00_1175); 
FAILURE(BgL_auxz00_6889,BFALSE,BFALSE);} 
BgL_n2z00_3125 = 
BELONG_TO_LONG(BgL_tmpz00_6886); } 
return 
(BgL_n1z00_3124>BgL_n2z00_3125);} }  else 
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 double BgL_arg1483z00_1177;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1484z00_1178;
BgL_arg1484z00_1178 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_43); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6898;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6899;
if(
ELONGP(BgL_arg1484z00_1178))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6899 = BgL_arg1484z00_1178
; }  else 
{ 
 obj_t BgL_auxz00_6902;
BgL_auxz00_6902 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1484z00_1178); 
FAILURE(BgL_auxz00_6902,BFALSE,BFALSE);} 
BgL_tmpz00_6898 = 
BELONG_TO_LONG(BgL_tmpz00_6899); } 
BgL_arg1483z00_1177 = 
(double)(BgL_tmpz00_6898); } } 
return 
(BgL_arg1483z00_1177>
REAL_TO_DOUBLE(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1486z00_1180;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1488z00_1182;
BgL_arg1488z00_1182 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_43); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6913;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6914;
if(
ELONGP(BgL_arg1488z00_1182))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6914 = BgL_arg1488z00_1182
; }  else 
{ 
 obj_t BgL_auxz00_6917;
BgL_auxz00_6917 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1488z00_1182); 
FAILURE(BgL_auxz00_6917,BFALSE,BFALSE);} 
BgL_tmpz00_6913 = 
BELONG_TO_LONG(BgL_tmpz00_6914); } 
BgL_arg1486z00_1180 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6913); } } 
return 
(BgL_arg1486z00_1180>
BLLONG_TO_LLONG(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_arg1490z00_1184;
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1492z00_1185;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1494z00_1186;
BgL_arg1494z00_1186 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_43); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6928;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6929;
if(
ELONGP(BgL_arg1494z00_1186))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6929 = BgL_arg1494z00_1186
; }  else 
{ 
 obj_t BgL_auxz00_6932;
BgL_auxz00_6932 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1494z00_1186); 
FAILURE(BgL_auxz00_6932,BFALSE,BFALSE);} 
BgL_tmpz00_6928 = 
BELONG_TO_LONG(BgL_tmpz00_6929); } 
BgL_arg1492z00_1185 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6928); } } 
BgL_arg1490z00_1184 = 
(uint64_t)(BgL_arg1492z00_1185); } 
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n2z00_3132;
BgL_n2z00_3132 = 
BGL_BINT64_TO_INT64(BgL_yz00_44); 
return 
(BgL_arg1490z00_1184>BgL_n2z00_3132);} }  else 
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1497z00_1188;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1498z00_1189;
BgL_arg1498z00_1189 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_43); 
{ /* Ieee/number.scm 564 */
 long BgL_xz00_3133;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6944;
if(
ELONGP(BgL_arg1498z00_1189))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6944 = BgL_arg1498z00_1189
; }  else 
{ 
 obj_t BgL_auxz00_6947;
BgL_auxz00_6947 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1498z00_1189); 
FAILURE(BgL_auxz00_6947,BFALSE,BFALSE);} 
BgL_xz00_3133 = 
BELONG_TO_LONG(BgL_tmpz00_6944); } 
BgL_arg1497z00_1188 = 
bgl_long_to_bignum(BgL_xz00_3133); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_arg1497z00_1188, BgL_yz00_44))>0L);}  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_44));} } } } } } }  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_xz00_43))
{ /* Ieee/number.scm 564 */
if(
INTEGERP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1502z00_1193;
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6962;
BgL_tmpz00_6962 = 
(long)CINT(BgL_yz00_44); 
BgL_arg1502z00_1193 = 
LONG_TO_LLONG(BgL_tmpz00_6962); } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_43)>BgL_arg1502z00_1193);}  else 
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_43))>
REAL_TO_DOUBLE(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_43)>
BLLONG_TO_LLONG(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1510z00_1201;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1511z00_1202;
BgL_arg1511z00_1202 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_44); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_6981;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_6982;
if(
ELONGP(BgL_arg1511z00_1202))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_6982 = BgL_arg1511z00_1202
; }  else 
{ 
 obj_t BgL_auxz00_6985;
BgL_auxz00_6985 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1511z00_1202); 
FAILURE(BgL_auxz00_6985,BFALSE,BFALSE);} 
BgL_tmpz00_6981 = 
BELONG_TO_LONG(BgL_tmpz00_6982); } 
BgL_arg1510z00_1201 = 
(BGL_LONGLONG_T)(BgL_tmpz00_6981); } } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_43)>BgL_arg1510z00_1201);}  else 
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_43)), BgL_yz00_44))>0L);}  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_arg1516z00_1207;
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_tmpz00_7002;
BgL_tmpz00_7002 = 
BLLONG_TO_LLONG(BgL_xz00_43); 
BgL_arg1516z00_1207 = 
(uint64_t)(BgL_tmpz00_7002); } 
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n2z00_3154;
BgL_n2z00_3154 = 
BGL_BINT64_TO_INT64(BgL_yz00_44); 
return 
(BgL_arg1516z00_1207>BgL_n2z00_3154);} }  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_44));} } } } } } }  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_xz00_43))
{ /* Ieee/number.scm 564 */
if(
INTEGERP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_arg1521z00_1210;
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_7013;
BgL_tmpz00_7013 = 
(long)CINT(BgL_yz00_44); 
BgL_arg1521z00_1210 = 
(uint64_t)(BgL_tmpz00_7013); } 
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n1z00_3156;
BgL_n1z00_3156 = 
BGL_BINT64_TO_INT64(BgL_xz00_43); 
return 
(BgL_n1z00_3156>BgL_arg1521z00_1210);} }  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n1z00_3158; uint64_t BgL_n2z00_3159;
BgL_n1z00_3158 = 
BGL_BINT64_TO_INT64(BgL_xz00_43); 
BgL_n2z00_3159 = 
BGL_BINT64_TO_INT64(BgL_yz00_44); 
return 
(BgL_n1z00_3158>BgL_n2z00_3159);}  else 
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 double BgL_arg1524z00_1213;
{ /* Ieee/number.scm 564 */
 uint64_t BgL_tmpz00_7025;
BgL_tmpz00_7025 = 
BGL_BINT64_TO_INT64(BgL_xz00_43); 
BgL_arg1524z00_1213 = 
(double)(BgL_tmpz00_7025); } 
return 
(BgL_arg1524z00_1213>
REAL_TO_DOUBLE(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_arg1526z00_1215;
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_tmpz00_7032;
BgL_tmpz00_7032 = 
BLLONG_TO_LLONG(BgL_yz00_44); 
BgL_arg1526z00_1215 = 
(uint64_t)(BgL_tmpz00_7032); } 
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n1z00_3163;
BgL_n1z00_3163 = 
BGL_BINT64_TO_INT64(BgL_xz00_43); 
return 
(BgL_n1z00_3163>BgL_arg1526z00_1215);} }  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 uint64_t BgL_arg1529z00_1218;
{ /* Ieee/number.scm 564 */
 BGL_LONGLONG_T BgL_arg1530z00_1219;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1531z00_1220;
BgL_arg1531z00_1220 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_44); 
{ /* Ieee/number.scm 564 */
 long BgL_tmpz00_7040;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_7041;
if(
ELONGP(BgL_arg1531z00_1220))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_7041 = BgL_arg1531z00_1220
; }  else 
{ 
 obj_t BgL_auxz00_7044;
BgL_auxz00_7044 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1531z00_1220); 
FAILURE(BgL_auxz00_7044,BFALSE,BFALSE);} 
BgL_tmpz00_7040 = 
BELONG_TO_LONG(BgL_tmpz00_7041); } 
BgL_arg1530z00_1219 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7040); } } 
BgL_arg1529z00_1218 = 
(uint64_t)(BgL_arg1530z00_1219); } 
{ /* Ieee/number.scm 564 */
 uint64_t BgL_n1z00_3166;
BgL_n1z00_3166 = 
BGL_BINT64_TO_INT64(BgL_xz00_43); 
return 
(BgL_n1z00_3166>BgL_arg1529z00_1218);} }  else 
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 long BgL_n1z00_3171;
BgL_n1z00_3171 = 
(long)(
bgl_bignum_cmp(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_43)), BgL_yz00_44)); 
return 
(BgL_n1z00_3171>0L);}  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_44));} } } } } } }  else 
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_xz00_43))
{ /* Ieee/number.scm 564 */
if(
BIGNUMP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_43, BgL_yz00_44))>0L);}  else 
{ /* Ieee/number.scm 564 */
if(
INTEGERP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_43, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_44))))>0L);}  else 
{ /* Ieee/number.scm 564 */
if(
REALP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
bgl_bignum_to_flonum(BgL_xz00_43)>
REAL_TO_DOUBLE(BgL_yz00_44));}  else 
{ /* Ieee/number.scm 564 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1546z00_1230;
{ /* Ieee/number.scm 564 */
 obj_t BgL_arg1547z00_1231;
BgL_arg1547z00_1231 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_44); 
{ /* Ieee/number.scm 564 */
 long BgL_xz00_3184;
{ /* Ieee/number.scm 564 */
 obj_t BgL_tmpz00_7084;
if(
ELONGP(BgL_arg1547z00_1231))
{ /* Ieee/number.scm 564 */
BgL_tmpz00_7084 = BgL_arg1547z00_1231
; }  else 
{ 
 obj_t BgL_auxz00_7087;
BgL_auxz00_7087 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(21701L), BGl_string3758z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1547z00_1231); 
FAILURE(BgL_auxz00_7087,BFALSE,BFALSE);} 
BgL_xz00_3184 = 
BELONG_TO_LONG(BgL_tmpz00_7084); } 
BgL_arg1546z00_1230 = 
bgl_long_to_bignum(BgL_xz00_3184); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_43, BgL_arg1546z00_1230))>0L);}  else 
{ /* Ieee/number.scm 564 */
if(
LLONGP(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_43, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_44))))>0L);}  else 
{ /* Ieee/number.scm 564 */
if(
BGL_UINT64P(BgL_yz00_44))
{ /* Ieee/number.scm 564 */
 long BgL_n1z00_3197;
BgL_n1z00_3197 = 
(long)(
bgl_bignum_cmp(BgL_xz00_43, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_44)))); 
return 
(BgL_n1z00_3197>0L);}  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_44));} } } } } } }  else 
{ /* Ieee/number.scm 564 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3759z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_43));} } } } } } } 

}



/* &2> */
obj_t BGl_z622ze3z81zz__r4_numbers_6_5z00(obj_t BgL_envz00_4973, obj_t BgL_xz00_4974, obj_t BgL_yz00_4975)
{
{ /* Ieee/number.scm 563 */
return 
BBOOL(
BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_4974, BgL_yz00_4975));} 

}



/* > */
BGL_EXPORTED_DEF bool_t BGl_ze3ze3zz__r4_numbers_6_5z00(obj_t BgL_xz00_45, obj_t BgL_yz00_46, obj_t BgL_za7za7_47)
{
{ /* Ieee/number.scm 569 */
{ 
 obj_t BgL_xz00_1239; obj_t BgL_za7za7_1240;
if(
BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_45, BgL_yz00_46))
{ /* Ieee/number.scm 575 */
BgL_xz00_1239 = BgL_yz00_46; 
BgL_za7za7_1240 = BgL_za7za7_47; 
BgL_zc3z04anonymousza31555ze3z87_1241:
if(
NULLP(BgL_za7za7_1240))
{ /* Ieee/number.scm 572 */
return ((bool_t)1);}  else 
{ /* Ieee/number.scm 573 */
 bool_t BgL_test4235z00_7120;
{ /* Ieee/number.scm 573 */
 obj_t BgL_arg1562z00_1247;
{ /* Ieee/number.scm 573 */
 obj_t BgL_pairz00_3198;
if(
PAIRP(BgL_za7za7_1240))
{ /* Ieee/number.scm 573 */
BgL_pairz00_3198 = BgL_za7za7_1240; }  else 
{ 
 obj_t BgL_auxz00_7123;
BgL_auxz00_7123 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22024L), BGl_string3760z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1240); 
FAILURE(BgL_auxz00_7123,BFALSE,BFALSE);} 
BgL_arg1562z00_1247 = 
CAR(BgL_pairz00_3198); } 
BgL_test4235z00_7120 = 
BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_1239, BgL_arg1562z00_1247); } 
if(BgL_test4235z00_7120)
{ 
 obj_t BgL_za7za7_7137; obj_t BgL_xz00_7129;
{ /* Ieee/number.scm 573 */
 obj_t BgL_pairz00_3199;
if(
PAIRP(BgL_za7za7_1240))
{ /* Ieee/number.scm 573 */
BgL_pairz00_3199 = BgL_za7za7_1240; }  else 
{ 
 obj_t BgL_auxz00_7132;
BgL_auxz00_7132 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22041L), BGl_string3760z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1240); 
FAILURE(BgL_auxz00_7132,BFALSE,BFALSE);} 
BgL_xz00_7129 = 
CAR(BgL_pairz00_3199); } 
{ /* Ieee/number.scm 573 */
 obj_t BgL_pairz00_3200;
if(
PAIRP(BgL_za7za7_1240))
{ /* Ieee/number.scm 573 */
BgL_pairz00_3200 = BgL_za7za7_1240; }  else 
{ 
 obj_t BgL_auxz00_7140;
BgL_auxz00_7140 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22049L), BGl_string3760z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1240); 
FAILURE(BgL_auxz00_7140,BFALSE,BFALSE);} 
BgL_za7za7_7137 = 
CDR(BgL_pairz00_3200); } 
BgL_za7za7_1240 = BgL_za7za7_7137; 
BgL_xz00_1239 = BgL_xz00_7129; 
goto BgL_zc3z04anonymousza31555ze3z87_1241;}  else 
{ /* Ieee/number.scm 573 */
return ((bool_t)0);} } }  else 
{ /* Ieee/number.scm 575 */
return ((bool_t)0);} } } 

}



/* &> */
obj_t BGl_z62ze3z81zz__r4_numbers_6_5z00(obj_t BgL_envz00_4976, obj_t BgL_xz00_4977, obj_t BgL_yz00_4978, obj_t BgL_za7za7_4979)
{
{ /* Ieee/number.scm 569 */
return 
BBOOL(
BGl_ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_4977, BgL_yz00_4978, BgL_za7za7_4979));} 

}



/* 2<= */
BGL_EXPORTED_DEF bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t BgL_xz00_48, obj_t BgL_yz00_49)
{
{ /* Ieee/number.scm 580 */
if(
INTEGERP(BgL_xz00_48))
{ /* Ieee/number.scm 581 */
if(
INTEGERP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(long)CINT(BgL_xz00_48)<=
(long)CINT(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(double)(
(long)CINT(BgL_xz00_48))<=
REAL_TO_DOUBLE(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 long BgL_arg1571z00_1254; obj_t BgL_arg1573z00_1255;
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7162;
BgL_tmpz00_7162 = 
(long)CINT(BgL_xz00_48); 
BgL_arg1571z00_1254 = 
(long)(BgL_tmpz00_7162); } 
BgL_arg1573z00_1255 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_49); 
{ /* Ieee/number.scm 581 */
 long BgL_n2z00_3207;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7166;
if(
ELONGP(BgL_arg1573z00_1255))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7166 = BgL_arg1573z00_1255
; }  else 
{ 
 obj_t BgL_auxz00_7169;
BgL_auxz00_7169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1573z00_1255); 
FAILURE(BgL_auxz00_7169,BFALSE,BFALSE);} 
BgL_n2z00_3207 = 
BELONG_TO_LONG(BgL_tmpz00_7166); } 
return 
(BgL_arg1571z00_1254<=BgL_n2z00_3207);} }  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1575z00_1257;
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7177;
BgL_tmpz00_7177 = 
(long)CINT(BgL_xz00_48); 
BgL_arg1575z00_1257 = 
LONG_TO_LLONG(BgL_tmpz00_7177); } 
return 
(BgL_arg1575z00_1257<=
BLLONG_TO_LLONG(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_arg1578z00_1260;
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1579z00_1261;
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7184;
BgL_tmpz00_7184 = 
(long)CINT(BgL_xz00_48); 
BgL_arg1579z00_1261 = 
LONG_TO_LLONG(BgL_tmpz00_7184); } 
BgL_arg1578z00_1260 = 
(uint64_t)(BgL_arg1579z00_1261); } 
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n2z00_3214;
BgL_n2z00_3214 = 
BGL_BINT64_TO_INT64(BgL_yz00_49); 
return 
(BgL_arg1578z00_1260<=BgL_n2z00_3214);} }  else 
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_48)), BgL_yz00_49))<=0L);}  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_49));} } } } } } }  else 
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_xz00_48))
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_48)<=
REAL_TO_DOUBLE(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
INTEGERP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_48)<=
(double)(
(long)CINT(BgL_yz00_49)));}  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 double BgL_arg1589z00_1269;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1591z00_1270;
BgL_arg1591z00_1270 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_49); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7215;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7216;
if(
ELONGP(BgL_arg1591z00_1270))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7216 = BgL_arg1591z00_1270
; }  else 
{ 
 obj_t BgL_auxz00_7219;
BgL_auxz00_7219 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1591z00_1270); 
FAILURE(BgL_auxz00_7219,BFALSE,BFALSE);} 
BgL_tmpz00_7215 = 
BELONG_TO_LONG(BgL_tmpz00_7216); } 
BgL_arg1589z00_1269 = 
(double)(BgL_tmpz00_7215); } } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_48)<=BgL_arg1589z00_1269);}  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_48)<=
(double)(
BLLONG_TO_LLONG(BgL_yz00_49)));}  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 double BgL_arg1598z00_1275;
{ /* Ieee/number.scm 581 */
 uint64_t BgL_xz00_3228;
BgL_xz00_3228 = 
BGL_BINT64_TO_INT64(BgL_yz00_49); 
BgL_arg1598z00_1275 = 
(double)(BgL_xz00_3228); } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_48)<=BgL_arg1598z00_1275);}  else 
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_48)<=
bgl_bignum_to_flonum(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_49));} } } } } } }  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_48))
{ /* Ieee/number.scm 581 */
if(
INTEGERP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1605z00_1280; long BgL_arg1606z00_1281;
BgL_arg1605z00_1280 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_48); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7251;
BgL_tmpz00_7251 = 
(long)CINT(BgL_yz00_49); 
BgL_arg1606z00_1281 = 
(long)(BgL_tmpz00_7251); } 
{ /* Ieee/number.scm 581 */
 long BgL_n1z00_3235;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7254;
if(
ELONGP(BgL_arg1605z00_1280))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7254 = BgL_arg1605z00_1280
; }  else 
{ 
 obj_t BgL_auxz00_7257;
BgL_auxz00_7257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1605z00_1280); 
FAILURE(BgL_auxz00_7257,BFALSE,BFALSE);} 
BgL_n1z00_3235 = 
BELONG_TO_LONG(BgL_tmpz00_7254); } 
return 
(BgL_n1z00_3235<=BgL_arg1606z00_1281);} }  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1608z00_1283; obj_t BgL_arg1609z00_1284;
BgL_arg1608z00_1283 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_48); 
BgL_arg1609z00_1284 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_49); 
{ /* Ieee/number.scm 581 */
 long BgL_n1z00_3237; long BgL_n2z00_3238;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7267;
if(
ELONGP(BgL_arg1608z00_1283))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7267 = BgL_arg1608z00_1283
; }  else 
{ 
 obj_t BgL_auxz00_7270;
BgL_auxz00_7270 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1608z00_1283); 
FAILURE(BgL_auxz00_7270,BFALSE,BFALSE);} 
BgL_n1z00_3237 = 
BELONG_TO_LONG(BgL_tmpz00_7267); } 
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7275;
if(
ELONGP(BgL_arg1609z00_1284))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7275 = BgL_arg1609z00_1284
; }  else 
{ 
 obj_t BgL_auxz00_7278;
BgL_auxz00_7278 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1609z00_1284); 
FAILURE(BgL_auxz00_7278,BFALSE,BFALSE);} 
BgL_n2z00_3238 = 
BELONG_TO_LONG(BgL_tmpz00_7275); } 
return 
(BgL_n1z00_3237<=BgL_n2z00_3238);} }  else 
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 double BgL_arg1611z00_1286;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1612z00_1287;
BgL_arg1612z00_1287 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_48); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7287;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7288;
if(
ELONGP(BgL_arg1612z00_1287))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7288 = BgL_arg1612z00_1287
; }  else 
{ 
 obj_t BgL_auxz00_7291;
BgL_auxz00_7291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1612z00_1287); 
FAILURE(BgL_auxz00_7291,BFALSE,BFALSE);} 
BgL_tmpz00_7287 = 
BELONG_TO_LONG(BgL_tmpz00_7288); } 
BgL_arg1611z00_1286 = 
(double)(BgL_tmpz00_7287); } } 
return 
(BgL_arg1611z00_1286<=
REAL_TO_DOUBLE(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1615z00_1289;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1617z00_1291;
BgL_arg1617z00_1291 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_48); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7302;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7303;
if(
ELONGP(BgL_arg1617z00_1291))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7303 = BgL_arg1617z00_1291
; }  else 
{ 
 obj_t BgL_auxz00_7306;
BgL_auxz00_7306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1617z00_1291); 
FAILURE(BgL_auxz00_7306,BFALSE,BFALSE);} 
BgL_tmpz00_7302 = 
BELONG_TO_LONG(BgL_tmpz00_7303); } 
BgL_arg1615z00_1289 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7302); } } 
return 
(BgL_arg1615z00_1289<=
BLLONG_TO_LLONG(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_arg1619z00_1293;
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1620z00_1294;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1621z00_1295;
BgL_arg1621z00_1295 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_48); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7317;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7318;
if(
ELONGP(BgL_arg1621z00_1295))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7318 = BgL_arg1621z00_1295
; }  else 
{ 
 obj_t BgL_auxz00_7321;
BgL_auxz00_7321 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1621z00_1295); 
FAILURE(BgL_auxz00_7321,BFALSE,BFALSE);} 
BgL_tmpz00_7317 = 
BELONG_TO_LONG(BgL_tmpz00_7318); } 
BgL_arg1620z00_1294 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7317); } } 
BgL_arg1619z00_1293 = 
(uint64_t)(BgL_arg1620z00_1294); } 
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n2z00_3245;
BgL_n2z00_3245 = 
BGL_BINT64_TO_INT64(BgL_yz00_49); 
return 
(BgL_arg1619z00_1293<=BgL_n2z00_3245);} }  else 
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1623z00_1297;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1624z00_1298;
BgL_arg1624z00_1298 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_48); 
{ /* Ieee/number.scm 581 */
 long BgL_xz00_3246;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7333;
if(
ELONGP(BgL_arg1624z00_1298))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7333 = BgL_arg1624z00_1298
; }  else 
{ 
 obj_t BgL_auxz00_7336;
BgL_auxz00_7336 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1624z00_1298); 
FAILURE(BgL_auxz00_7336,BFALSE,BFALSE);} 
BgL_xz00_3246 = 
BELONG_TO_LONG(BgL_tmpz00_7333); } 
BgL_arg1623z00_1297 = 
bgl_long_to_bignum(BgL_xz00_3246); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_arg1623z00_1297, BgL_yz00_49))<=0L);}  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_49));} } } } } } }  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_xz00_48))
{ /* Ieee/number.scm 581 */
if(
INTEGERP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1628z00_1302;
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7351;
BgL_tmpz00_7351 = 
(long)CINT(BgL_yz00_49); 
BgL_arg1628z00_1302 = 
LONG_TO_LLONG(BgL_tmpz00_7351); } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_48)<=BgL_arg1628z00_1302);}  else 
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_48))<=
REAL_TO_DOUBLE(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_48)<=
BLLONG_TO_LLONG(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1639z00_1310;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1640z00_1311;
BgL_arg1640z00_1311 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_49); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7370;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7371;
if(
ELONGP(BgL_arg1640z00_1311))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7371 = BgL_arg1640z00_1311
; }  else 
{ 
 obj_t BgL_auxz00_7374;
BgL_auxz00_7374 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1640z00_1311); 
FAILURE(BgL_auxz00_7374,BFALSE,BFALSE);} 
BgL_tmpz00_7370 = 
BELONG_TO_LONG(BgL_tmpz00_7371); } 
BgL_arg1639z00_1310 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7370); } } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_48)<=BgL_arg1639z00_1310);}  else 
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_48)), BgL_yz00_49))<=0L);}  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_arg1645z00_1316;
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_tmpz00_7391;
BgL_tmpz00_7391 = 
BLLONG_TO_LLONG(BgL_xz00_48); 
BgL_arg1645z00_1316 = 
(uint64_t)(BgL_tmpz00_7391); } 
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n2z00_3267;
BgL_n2z00_3267 = 
BGL_BINT64_TO_INT64(BgL_yz00_49); 
return 
(BgL_arg1645z00_1316<=BgL_n2z00_3267);} }  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_49));} } } } } } }  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_xz00_48))
{ /* Ieee/number.scm 581 */
if(
INTEGERP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_arg1648z00_1319;
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7402;
BgL_tmpz00_7402 = 
(long)CINT(BgL_yz00_49); 
BgL_arg1648z00_1319 = 
(uint64_t)(BgL_tmpz00_7402); } 
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n1z00_3269;
BgL_n1z00_3269 = 
BGL_BINT64_TO_INT64(BgL_xz00_48); 
return 
(BgL_n1z00_3269<=BgL_arg1648z00_1319);} }  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n1z00_3271; uint64_t BgL_n2z00_3272;
BgL_n1z00_3271 = 
BGL_BINT64_TO_INT64(BgL_xz00_48); 
BgL_n2z00_3272 = 
BGL_BINT64_TO_INT64(BgL_yz00_49); 
return 
(BgL_n1z00_3271<=BgL_n2z00_3272);}  else 
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 double BgL_arg1651z00_1322;
{ /* Ieee/number.scm 581 */
 uint64_t BgL_tmpz00_7414;
BgL_tmpz00_7414 = 
BGL_BINT64_TO_INT64(BgL_xz00_48); 
BgL_arg1651z00_1322 = 
(double)(BgL_tmpz00_7414); } 
return 
(BgL_arg1651z00_1322<=
REAL_TO_DOUBLE(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_arg1653z00_1324;
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_tmpz00_7421;
BgL_tmpz00_7421 = 
BLLONG_TO_LLONG(BgL_yz00_49); 
BgL_arg1653z00_1324 = 
(uint64_t)(BgL_tmpz00_7421); } 
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n1z00_3276;
BgL_n1z00_3276 = 
BGL_BINT64_TO_INT64(BgL_xz00_48); 
return 
(BgL_n1z00_3276<=BgL_arg1653z00_1324);} }  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 uint64_t BgL_arg1656z00_1327;
{ /* Ieee/number.scm 581 */
 BGL_LONGLONG_T BgL_arg1657z00_1328;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1658z00_1329;
BgL_arg1658z00_1329 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_49); 
{ /* Ieee/number.scm 581 */
 long BgL_tmpz00_7429;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7430;
if(
ELONGP(BgL_arg1658z00_1329))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7430 = BgL_arg1658z00_1329
; }  else 
{ 
 obj_t BgL_auxz00_7433;
BgL_auxz00_7433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1658z00_1329); 
FAILURE(BgL_auxz00_7433,BFALSE,BFALSE);} 
BgL_tmpz00_7429 = 
BELONG_TO_LONG(BgL_tmpz00_7430); } 
BgL_arg1657z00_1328 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7429); } } 
BgL_arg1656z00_1327 = 
(uint64_t)(BgL_arg1657z00_1328); } 
{ /* Ieee/number.scm 581 */
 uint64_t BgL_n1z00_3279;
BgL_n1z00_3279 = 
BGL_BINT64_TO_INT64(BgL_xz00_48); 
return 
(BgL_n1z00_3279<=BgL_arg1656z00_1327);} }  else 
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 long BgL_n1z00_3284;
BgL_n1z00_3284 = 
(long)(
bgl_bignum_cmp(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_48)), BgL_yz00_49)); 
return 
(BgL_n1z00_3284<=0L);}  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_49));} } } } } } }  else 
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_xz00_48))
{ /* Ieee/number.scm 581 */
if(
BIGNUMP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_48, BgL_yz00_49))<=0L);}  else 
{ /* Ieee/number.scm 581 */
if(
INTEGERP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_48, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_49))))<=0L);}  else 
{ /* Ieee/number.scm 581 */
if(
REALP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
bgl_bignum_to_flonum(BgL_xz00_48)<=
REAL_TO_DOUBLE(BgL_yz00_49));}  else 
{ /* Ieee/number.scm 581 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1675z00_1339;
{ /* Ieee/number.scm 581 */
 obj_t BgL_arg1676z00_1340;
BgL_arg1676z00_1340 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_49); 
{ /* Ieee/number.scm 581 */
 long BgL_xz00_3297;
{ /* Ieee/number.scm 581 */
 obj_t BgL_tmpz00_7473;
if(
ELONGP(BgL_arg1676z00_1340))
{ /* Ieee/number.scm 581 */
BgL_tmpz00_7473 = BgL_arg1676z00_1340
; }  else 
{ 
 obj_t BgL_auxz00_7476;
BgL_auxz00_7476 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22345L), BGl_string3761z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1676z00_1340); 
FAILURE(BgL_auxz00_7476,BFALSE,BFALSE);} 
BgL_xz00_3297 = 
BELONG_TO_LONG(BgL_tmpz00_7473); } 
BgL_arg1675z00_1339 = 
bgl_long_to_bignum(BgL_xz00_3297); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_48, BgL_arg1675z00_1339))<=0L);}  else 
{ /* Ieee/number.scm 581 */
if(
LLONGP(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_48, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_49))))<=0L);}  else 
{ /* Ieee/number.scm 581 */
if(
BGL_UINT64P(BgL_yz00_49))
{ /* Ieee/number.scm 581 */
 long BgL_n1z00_3310;
BgL_n1z00_3310 = 
(long)(
bgl_bignum_cmp(BgL_xz00_48, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_49)))); 
return 
(BgL_n1z00_3310<=0L);}  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_49));} } } } } } }  else 
{ /* Ieee/number.scm 581 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3762z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_48));} } } } } } } 

}



/* &2<= */
obj_t BGl_z622zc3zd3z72zz__r4_numbers_6_5z00(obj_t BgL_envz00_4980, obj_t BgL_xz00_4981, obj_t BgL_yz00_4982)
{
{ /* Ieee/number.scm 580 */
return 
BBOOL(
BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_4981, BgL_yz00_4982));} 

}



/* <= */
BGL_EXPORTED_DEF bool_t BGl_zc3zd3z10zz__r4_numbers_6_5z00(obj_t BgL_xz00_50, obj_t BgL_yz00_51, obj_t BgL_za7za7_52)
{
{ /* Ieee/number.scm 586 */
{ 
 obj_t BgL_xz00_1348; obj_t BgL_za7za7_1349;
if(
BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_50, BgL_yz00_51))
{ /* Ieee/number.scm 592 */
BgL_xz00_1348 = BgL_yz00_51; 
BgL_za7za7_1349 = BgL_za7za7_52; 
BgL_zc3z04anonymousza31685ze3z87_1350:
if(
NULLP(BgL_za7za7_1349))
{ /* Ieee/number.scm 589 */
return ((bool_t)1);}  else 
{ /* Ieee/number.scm 590 */
 bool_t BgL_test4295z00_7509;
{ /* Ieee/number.scm 590 */
 obj_t BgL_arg1699z00_1356;
{ /* Ieee/number.scm 590 */
 obj_t BgL_pairz00_3311;
if(
PAIRP(BgL_za7za7_1349))
{ /* Ieee/number.scm 590 */
BgL_pairz00_3311 = BgL_za7za7_1349; }  else 
{ 
 obj_t BgL_auxz00_7512;
BgL_auxz00_7512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22672L), BGl_string3763z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1349); 
FAILURE(BgL_auxz00_7512,BFALSE,BFALSE);} 
BgL_arg1699z00_1356 = 
CAR(BgL_pairz00_3311); } 
BgL_test4295z00_7509 = 
BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_1348, BgL_arg1699z00_1356); } 
if(BgL_test4295z00_7509)
{ 
 obj_t BgL_za7za7_7526; obj_t BgL_xz00_7518;
{ /* Ieee/number.scm 590 */
 obj_t BgL_pairz00_3312;
if(
PAIRP(BgL_za7za7_1349))
{ /* Ieee/number.scm 590 */
BgL_pairz00_3312 = BgL_za7za7_1349; }  else 
{ 
 obj_t BgL_auxz00_7521;
BgL_auxz00_7521 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22690L), BGl_string3763z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1349); 
FAILURE(BgL_auxz00_7521,BFALSE,BFALSE);} 
BgL_xz00_7518 = 
CAR(BgL_pairz00_3312); } 
{ /* Ieee/number.scm 590 */
 obj_t BgL_pairz00_3313;
if(
PAIRP(BgL_za7za7_1349))
{ /* Ieee/number.scm 590 */
BgL_pairz00_3313 = BgL_za7za7_1349; }  else 
{ 
 obj_t BgL_auxz00_7529;
BgL_auxz00_7529 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22698L), BGl_string3763z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1349); 
FAILURE(BgL_auxz00_7529,BFALSE,BFALSE);} 
BgL_za7za7_7526 = 
CDR(BgL_pairz00_3313); } 
BgL_za7za7_1349 = BgL_za7za7_7526; 
BgL_xz00_1348 = BgL_xz00_7518; 
goto BgL_zc3z04anonymousza31685ze3z87_1350;}  else 
{ /* Ieee/number.scm 590 */
return ((bool_t)0);} } }  else 
{ /* Ieee/number.scm 592 */
return ((bool_t)0);} } } 

}



/* &<= */
obj_t BGl_z62zc3zd3z72zz__r4_numbers_6_5z00(obj_t BgL_envz00_4983, obj_t BgL_xz00_4984, obj_t BgL_yz00_4985, obj_t BgL_za7za7_4986)
{
{ /* Ieee/number.scm 586 */
return 
BBOOL(
BGl_zc3zd3z10zz__r4_numbers_6_5z00(BgL_xz00_4984, BgL_yz00_4985, BgL_za7za7_4986));} 

}



/* 2>= */
BGL_EXPORTED_DEF bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t BgL_xz00_53, obj_t BgL_yz00_54)
{
{ /* Ieee/number.scm 597 */
if(
INTEGERP(BgL_xz00_53))
{ /* Ieee/number.scm 598 */
if(
INTEGERP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(long)CINT(BgL_xz00_53)>=
(long)CINT(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(double)(
(long)CINT(BgL_xz00_53))>=
REAL_TO_DOUBLE(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 long BgL_arg1705z00_1363; obj_t BgL_arg1706z00_1364;
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7551;
BgL_tmpz00_7551 = 
(long)CINT(BgL_xz00_53); 
BgL_arg1705z00_1363 = 
(long)(BgL_tmpz00_7551); } 
BgL_arg1706z00_1364 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_54); 
{ /* Ieee/number.scm 598 */
 long BgL_n2z00_3320;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7555;
if(
ELONGP(BgL_arg1706z00_1364))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7555 = BgL_arg1706z00_1364
; }  else 
{ 
 obj_t BgL_auxz00_7558;
BgL_auxz00_7558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1706z00_1364); 
FAILURE(BgL_auxz00_7558,BFALSE,BFALSE);} 
BgL_n2z00_3320 = 
BELONG_TO_LONG(BgL_tmpz00_7555); } 
return 
(BgL_arg1705z00_1363>=BgL_n2z00_3320);} }  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1708z00_1366;
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7566;
BgL_tmpz00_7566 = 
(long)CINT(BgL_xz00_53); 
BgL_arg1708z00_1366 = 
LONG_TO_LLONG(BgL_tmpz00_7566); } 
return 
(BgL_arg1708z00_1366>=
BLLONG_TO_LLONG(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_arg1711z00_1369;
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1714z00_1370;
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7573;
BgL_tmpz00_7573 = 
(long)CINT(BgL_xz00_53); 
BgL_arg1714z00_1370 = 
LONG_TO_LLONG(BgL_tmpz00_7573); } 
BgL_arg1711z00_1369 = 
(uint64_t)(BgL_arg1714z00_1370); } 
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n2z00_3327;
BgL_n2z00_3327 = 
BGL_BINT64_TO_INT64(BgL_yz00_54); 
return 
(BgL_arg1711z00_1369>=BgL_n2z00_3327);} }  else 
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_53)), BgL_yz00_54))>=0L);}  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_54));} } } } } } }  else 
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_xz00_53))
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_53)>=
REAL_TO_DOUBLE(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
INTEGERP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_53)>=
(double)(
(long)CINT(BgL_yz00_54)));}  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 double BgL_arg1724z00_1378;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1725z00_1379;
BgL_arg1725z00_1379 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_54); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7604;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7605;
if(
ELONGP(BgL_arg1725z00_1379))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7605 = BgL_arg1725z00_1379
; }  else 
{ 
 obj_t BgL_auxz00_7608;
BgL_auxz00_7608 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1725z00_1379); 
FAILURE(BgL_auxz00_7608,BFALSE,BFALSE);} 
BgL_tmpz00_7604 = 
BELONG_TO_LONG(BgL_tmpz00_7605); } 
BgL_arg1724z00_1378 = 
(double)(BgL_tmpz00_7604); } } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_53)>=BgL_arg1724z00_1378);}  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_53)>=
(double)(
BLLONG_TO_LLONG(BgL_yz00_54)));}  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 double BgL_arg1730z00_1384;
{ /* Ieee/number.scm 598 */
 uint64_t BgL_xz00_3341;
BgL_xz00_3341 = 
BGL_BINT64_TO_INT64(BgL_yz00_54); 
BgL_arg1730z00_1384 = 
(double)(BgL_xz00_3341); } 
return 
(
REAL_TO_DOUBLE(BgL_xz00_53)>=BgL_arg1730z00_1384);}  else 
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_53)>=
bgl_bignum_to_flonum(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_54));} } } } } } }  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_53))
{ /* Ieee/number.scm 598 */
if(
INTEGERP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1736z00_1389; long BgL_arg1737z00_1390;
BgL_arg1736z00_1389 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_53); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7640;
BgL_tmpz00_7640 = 
(long)CINT(BgL_yz00_54); 
BgL_arg1737z00_1390 = 
(long)(BgL_tmpz00_7640); } 
{ /* Ieee/number.scm 598 */
 long BgL_n1z00_3348;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7643;
if(
ELONGP(BgL_arg1736z00_1389))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7643 = BgL_arg1736z00_1389
; }  else 
{ 
 obj_t BgL_auxz00_7646;
BgL_auxz00_7646 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1736z00_1389); 
FAILURE(BgL_auxz00_7646,BFALSE,BFALSE);} 
BgL_n1z00_3348 = 
BELONG_TO_LONG(BgL_tmpz00_7643); } 
return 
(BgL_n1z00_3348>=BgL_arg1737z00_1390);} }  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1739z00_1392; obj_t BgL_arg1740z00_1393;
BgL_arg1739z00_1392 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_53); 
BgL_arg1740z00_1393 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_54); 
{ /* Ieee/number.scm 598 */
 long BgL_n1z00_3350; long BgL_n2z00_3351;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7656;
if(
ELONGP(BgL_arg1739z00_1392))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7656 = BgL_arg1739z00_1392
; }  else 
{ 
 obj_t BgL_auxz00_7659;
BgL_auxz00_7659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1739z00_1392); 
FAILURE(BgL_auxz00_7659,BFALSE,BFALSE);} 
BgL_n1z00_3350 = 
BELONG_TO_LONG(BgL_tmpz00_7656); } 
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7664;
if(
ELONGP(BgL_arg1740z00_1393))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7664 = BgL_arg1740z00_1393
; }  else 
{ 
 obj_t BgL_auxz00_7667;
BgL_auxz00_7667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1740z00_1393); 
FAILURE(BgL_auxz00_7667,BFALSE,BFALSE);} 
BgL_n2z00_3351 = 
BELONG_TO_LONG(BgL_tmpz00_7664); } 
return 
(BgL_n1z00_3350>=BgL_n2z00_3351);} }  else 
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 double BgL_arg1743z00_1395;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1744z00_1396;
BgL_arg1744z00_1396 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_53); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7676;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7677;
if(
ELONGP(BgL_arg1744z00_1396))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7677 = BgL_arg1744z00_1396
; }  else 
{ 
 obj_t BgL_auxz00_7680;
BgL_auxz00_7680 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1744z00_1396); 
FAILURE(BgL_auxz00_7680,BFALSE,BFALSE);} 
BgL_tmpz00_7676 = 
BELONG_TO_LONG(BgL_tmpz00_7677); } 
BgL_arg1743z00_1395 = 
(double)(BgL_tmpz00_7676); } } 
return 
(BgL_arg1743z00_1395>=
REAL_TO_DOUBLE(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1746z00_1398;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1748z00_1400;
BgL_arg1748z00_1400 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_53); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7691;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7692;
if(
ELONGP(BgL_arg1748z00_1400))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7692 = BgL_arg1748z00_1400
; }  else 
{ 
 obj_t BgL_auxz00_7695;
BgL_auxz00_7695 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1748z00_1400); 
FAILURE(BgL_auxz00_7695,BFALSE,BFALSE);} 
BgL_tmpz00_7691 = 
BELONG_TO_LONG(BgL_tmpz00_7692); } 
BgL_arg1746z00_1398 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7691); } } 
return 
(BgL_arg1746z00_1398>=
BLLONG_TO_LLONG(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_arg1750z00_1402;
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1751z00_1403;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1752z00_1404;
BgL_arg1752z00_1404 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_53); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7706;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7707;
if(
ELONGP(BgL_arg1752z00_1404))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7707 = BgL_arg1752z00_1404
; }  else 
{ 
 obj_t BgL_auxz00_7710;
BgL_auxz00_7710 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1752z00_1404); 
FAILURE(BgL_auxz00_7710,BFALSE,BFALSE);} 
BgL_tmpz00_7706 = 
BELONG_TO_LONG(BgL_tmpz00_7707); } 
BgL_arg1751z00_1403 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7706); } } 
BgL_arg1750z00_1402 = 
(uint64_t)(BgL_arg1751z00_1403); } 
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n2z00_3358;
BgL_n2z00_3358 = 
BGL_BINT64_TO_INT64(BgL_yz00_54); 
return 
(BgL_arg1750z00_1402>=BgL_n2z00_3358);} }  else 
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1754z00_1406;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1755z00_1407;
BgL_arg1755z00_1407 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_53); 
{ /* Ieee/number.scm 598 */
 long BgL_xz00_3359;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7722;
if(
ELONGP(BgL_arg1755z00_1407))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7722 = BgL_arg1755z00_1407
; }  else 
{ 
 obj_t BgL_auxz00_7725;
BgL_auxz00_7725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1755z00_1407); 
FAILURE(BgL_auxz00_7725,BFALSE,BFALSE);} 
BgL_xz00_3359 = 
BELONG_TO_LONG(BgL_tmpz00_7722); } 
BgL_arg1754z00_1406 = 
bgl_long_to_bignum(BgL_xz00_3359); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_arg1754z00_1406, BgL_yz00_54))>=0L);}  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_54));} } } } } } }  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_xz00_53))
{ /* Ieee/number.scm 598 */
if(
INTEGERP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1759z00_1411;
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7740;
BgL_tmpz00_7740 = 
(long)CINT(BgL_yz00_54); 
BgL_arg1759z00_1411 = 
LONG_TO_LLONG(BgL_tmpz00_7740); } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_53)>=BgL_arg1759z00_1411);}  else 
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_53))>=
REAL_TO_DOUBLE(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_53)>=
BLLONG_TO_LLONG(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1767z00_1419;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1768z00_1420;
BgL_arg1768z00_1420 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_54); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7759;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7760;
if(
ELONGP(BgL_arg1768z00_1420))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7760 = BgL_arg1768z00_1420
; }  else 
{ 
 obj_t BgL_auxz00_7763;
BgL_auxz00_7763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1768z00_1420); 
FAILURE(BgL_auxz00_7763,BFALSE,BFALSE);} 
BgL_tmpz00_7759 = 
BELONG_TO_LONG(BgL_tmpz00_7760); } 
BgL_arg1767z00_1419 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7759); } } 
return 
(
BLLONG_TO_LLONG(BgL_xz00_53)>=BgL_arg1767z00_1419);}  else 
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(long)(
bgl_bignum_cmp(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_53)), BgL_yz00_54))>=0L);}  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_arg1773z00_1425;
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_tmpz00_7780;
BgL_tmpz00_7780 = 
BLLONG_TO_LLONG(BgL_xz00_53); 
BgL_arg1773z00_1425 = 
(uint64_t)(BgL_tmpz00_7780); } 
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n2z00_3380;
BgL_n2z00_3380 = 
BGL_BINT64_TO_INT64(BgL_yz00_54); 
return 
(BgL_arg1773z00_1425>=BgL_n2z00_3380);} }  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_54));} } } } } } }  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_xz00_53))
{ /* Ieee/number.scm 598 */
if(
INTEGERP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_arg1777z00_1428;
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7791;
BgL_tmpz00_7791 = 
(long)CINT(BgL_yz00_54); 
BgL_arg1777z00_1428 = 
(uint64_t)(BgL_tmpz00_7791); } 
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n1z00_3382;
BgL_n1z00_3382 = 
BGL_BINT64_TO_INT64(BgL_xz00_53); 
return 
(BgL_n1z00_3382>=BgL_arg1777z00_1428);} }  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n1z00_3384; uint64_t BgL_n2z00_3385;
BgL_n1z00_3384 = 
BGL_BINT64_TO_INT64(BgL_xz00_53); 
BgL_n2z00_3385 = 
BGL_BINT64_TO_INT64(BgL_yz00_54); 
return 
(BgL_n1z00_3384>=BgL_n2z00_3385);}  else 
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 double BgL_arg1781z00_1431;
{ /* Ieee/number.scm 598 */
 uint64_t BgL_tmpz00_7803;
BgL_tmpz00_7803 = 
BGL_BINT64_TO_INT64(BgL_xz00_53); 
BgL_arg1781z00_1431 = 
(double)(BgL_tmpz00_7803); } 
return 
(BgL_arg1781z00_1431>=
REAL_TO_DOUBLE(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_arg1783z00_1433;
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_tmpz00_7810;
BgL_tmpz00_7810 = 
BLLONG_TO_LLONG(BgL_yz00_54); 
BgL_arg1783z00_1433 = 
(uint64_t)(BgL_tmpz00_7810); } 
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n1z00_3389;
BgL_n1z00_3389 = 
BGL_BINT64_TO_INT64(BgL_xz00_53); 
return 
(BgL_n1z00_3389>=BgL_arg1783z00_1433);} }  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 uint64_t BgL_arg1787z00_1436;
{ /* Ieee/number.scm 598 */
 BGL_LONGLONG_T BgL_arg1788z00_1437;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1789z00_1438;
BgL_arg1789z00_1438 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_54); 
{ /* Ieee/number.scm 598 */
 long BgL_tmpz00_7818;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7819;
if(
ELONGP(BgL_arg1789z00_1438))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7819 = BgL_arg1789z00_1438
; }  else 
{ 
 obj_t BgL_auxz00_7822;
BgL_auxz00_7822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1789z00_1438); 
FAILURE(BgL_auxz00_7822,BFALSE,BFALSE);} 
BgL_tmpz00_7818 = 
BELONG_TO_LONG(BgL_tmpz00_7819); } 
BgL_arg1788z00_1437 = 
(BGL_LONGLONG_T)(BgL_tmpz00_7818); } } 
BgL_arg1787z00_1436 = 
(uint64_t)(BgL_arg1788z00_1437); } 
{ /* Ieee/number.scm 598 */
 uint64_t BgL_n1z00_3392;
BgL_n1z00_3392 = 
BGL_BINT64_TO_INT64(BgL_xz00_53); 
return 
(BgL_n1z00_3392>=BgL_arg1787z00_1436);} }  else 
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 long BgL_n1z00_3397;
BgL_n1z00_3397 = 
(long)(
bgl_bignum_cmp(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_53)), BgL_yz00_54)); 
return 
(BgL_n1z00_3397>=0L);}  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_54));} } } } } } }  else 
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_xz00_53))
{ /* Ieee/number.scm 598 */
if(
BIGNUMP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_53, BgL_yz00_54))>=0L);}  else 
{ /* Ieee/number.scm 598 */
if(
INTEGERP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_53, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_54))))>=0L);}  else 
{ /* Ieee/number.scm 598 */
if(
REALP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
bgl_bignum_to_flonum(BgL_xz00_53)>=
REAL_TO_DOUBLE(BgL_yz00_54));}  else 
{ /* Ieee/number.scm 598 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1799z00_1448;
{ /* Ieee/number.scm 598 */
 obj_t BgL_arg1800z00_1449;
BgL_arg1800z00_1449 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_54); 
{ /* Ieee/number.scm 598 */
 long BgL_xz00_3410;
{ /* Ieee/number.scm 598 */
 obj_t BgL_tmpz00_7862;
if(
ELONGP(BgL_arg1800z00_1449))
{ /* Ieee/number.scm 598 */
BgL_tmpz00_7862 = BgL_arg1800z00_1449
; }  else 
{ 
 obj_t BgL_auxz00_7865;
BgL_auxz00_7865 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(22995L), BGl_string3764z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1800z00_1449); 
FAILURE(BgL_auxz00_7865,BFALSE,BFALSE);} 
BgL_xz00_3410 = 
BELONG_TO_LONG(BgL_tmpz00_7862); } 
BgL_arg1799z00_1448 = 
bgl_long_to_bignum(BgL_xz00_3410); } } 
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_53, BgL_arg1799z00_1448))>=0L);}  else 
{ /* Ieee/number.scm 598 */
if(
LLONGP(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
return 
(
(long)(
bgl_bignum_cmp(BgL_xz00_53, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_54))))>=0L);}  else 
{ /* Ieee/number.scm 598 */
if(
BGL_UINT64P(BgL_yz00_54))
{ /* Ieee/number.scm 598 */
 long BgL_n1z00_3423;
BgL_n1z00_3423 = 
(long)(
bgl_bignum_cmp(BgL_xz00_53, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_54)))); 
return 
(BgL_n1z00_3423>=0L);}  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_54));} } } } } } }  else 
{ /* Ieee/number.scm 598 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3765z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_53));} } } } } } } 

}



/* &2>= */
obj_t BGl_z622ze3zd3z52zz__r4_numbers_6_5z00(obj_t BgL_envz00_4987, obj_t BgL_xz00_4988, obj_t BgL_yz00_4989)
{
{ /* Ieee/number.scm 597 */
return 
BBOOL(
BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_4988, BgL_yz00_4989));} 

}



/* >= */
BGL_EXPORTED_DEF bool_t BGl_ze3zd3z30zz__r4_numbers_6_5z00(obj_t BgL_xz00_55, obj_t BgL_yz00_56, obj_t BgL_za7za7_57)
{
{ /* Ieee/number.scm 603 */
{ 
 obj_t BgL_xz00_1457; obj_t BgL_za7za7_1458;
if(
BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_55, BgL_yz00_56))
{ /* Ieee/number.scm 609 */
BgL_xz00_1457 = BgL_yz00_56; 
BgL_za7za7_1458 = BgL_za7za7_57; 
BgL_zc3z04anonymousza31806ze3z87_1459:
if(
NULLP(BgL_za7za7_1458))
{ /* Ieee/number.scm 606 */
return ((bool_t)1);}  else 
{ /* Ieee/number.scm 607 */
 bool_t BgL_test4355z00_7898;
{ /* Ieee/number.scm 607 */
 obj_t BgL_arg1812z00_1465;
{ /* Ieee/number.scm 607 */
 obj_t BgL_pairz00_3424;
if(
PAIRP(BgL_za7za7_1458))
{ /* Ieee/number.scm 607 */
BgL_pairz00_3424 = BgL_za7za7_1458; }  else 
{ 
 obj_t BgL_auxz00_7901;
BgL_auxz00_7901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(23322L), BGl_string3766z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1458); 
FAILURE(BgL_auxz00_7901,BFALSE,BFALSE);} 
BgL_arg1812z00_1465 = 
CAR(BgL_pairz00_3424); } 
BgL_test4355z00_7898 = 
BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_1457, BgL_arg1812z00_1465); } 
if(BgL_test4355z00_7898)
{ 
 obj_t BgL_za7za7_7915; obj_t BgL_xz00_7907;
{ /* Ieee/number.scm 607 */
 obj_t BgL_pairz00_3425;
if(
PAIRP(BgL_za7za7_1458))
{ /* Ieee/number.scm 607 */
BgL_pairz00_3425 = BgL_za7za7_1458; }  else 
{ 
 obj_t BgL_auxz00_7910;
BgL_auxz00_7910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(23340L), BGl_string3766z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1458); 
FAILURE(BgL_auxz00_7910,BFALSE,BFALSE);} 
BgL_xz00_7907 = 
CAR(BgL_pairz00_3425); } 
{ /* Ieee/number.scm 607 */
 obj_t BgL_pairz00_3426;
if(
PAIRP(BgL_za7za7_1458))
{ /* Ieee/number.scm 607 */
BgL_pairz00_3426 = BgL_za7za7_1458; }  else 
{ 
 obj_t BgL_auxz00_7918;
BgL_auxz00_7918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(23348L), BGl_string3766z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_za7za7_1458); 
FAILURE(BgL_auxz00_7918,BFALSE,BFALSE);} 
BgL_za7za7_7915 = 
CDR(BgL_pairz00_3426); } 
BgL_za7za7_1458 = BgL_za7za7_7915; 
BgL_xz00_1457 = BgL_xz00_7907; 
goto BgL_zc3z04anonymousza31806ze3z87_1459;}  else 
{ /* Ieee/number.scm 607 */
return ((bool_t)0);} } }  else 
{ /* Ieee/number.scm 609 */
return ((bool_t)0);} } } 

}



/* &>= */
obj_t BGl_z62ze3zd3z52zz__r4_numbers_6_5z00(obj_t BgL_envz00_4990, obj_t BgL_xz00_4991, obj_t BgL_yz00_4992, obj_t BgL_za7za7_4993)
{
{ /* Ieee/number.scm 603 */
return 
BBOOL(
BGl_ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_4991, BgL_yz00_4992, BgL_za7za7_4993));} 

}



/* zero? */
BGL_EXPORTED_DEF bool_t BGl_za7erozf3z54zz__r4_numbers_6_5z00(obj_t BgL_xz00_58)
{
{ /* Ieee/number.scm 614 */
if(
INTEGERP(BgL_xz00_58))
{ /* Ieee/number.scm 616 */
return 
(
(long)CINT(BgL_xz00_58)==0L);}  else 
{ /* Ieee/number.scm 616 */
if(
REALP(BgL_xz00_58))
{ /* Ieee/number.scm 617 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_58)==((double)0.0));}  else 
{ /* Ieee/number.scm 617 */
if(
ELONGP(BgL_xz00_58))
{ /* Ieee/number.scm 618 */
 long BgL_n1z00_3432;
BgL_n1z00_3432 = 
BELONG_TO_LONG(BgL_xz00_58); 
return 
(BgL_n1z00_3432==((long)0));}  else 
{ /* Ieee/number.scm 618 */
if(
LLONGP(BgL_xz00_58))
{ /* Ieee/number.scm 619 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_58)==((BGL_LONGLONG_T)0));}  else 
{ /* Ieee/number.scm 619 */
if(
BIGNUMP(BgL_xz00_58))
{ /* Ieee/number.scm 620 */
return 
BXZERO(BgL_xz00_58);}  else 
{ /* Ieee/number.scm 620 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3767z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_58));} } } } } } 

}



/* &zero? */
obj_t BGl_z62za7erozf3z36zz__r4_numbers_6_5z00(obj_t BgL_envz00_4994, obj_t BgL_xz00_4995)
{
{ /* Ieee/number.scm 614 */
return 
BBOOL(
BGl_za7erozf3z54zz__r4_numbers_6_5z00(BgL_xz00_4995));} 

}



/* positive? */
BGL_EXPORTED_DEF bool_t BGl_positivezf3zf3zz__r4_numbers_6_5z00(obj_t BgL_xz00_59)
{
{ /* Ieee/number.scm 626 */
if(
INTEGERP(BgL_xz00_59))
{ /* Ieee/number.scm 628 */
return 
(
(long)CINT(BgL_xz00_59)>0L);}  else 
{ /* Ieee/number.scm 628 */
if(
REALP(BgL_xz00_59))
{ /* Ieee/number.scm 629 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_59)>((double)0.0));}  else 
{ /* Ieee/number.scm 629 */
if(
ELONGP(BgL_xz00_59))
{ /* Ieee/number.scm 630 */
 long BgL_n1z00_3442;
BgL_n1z00_3442 = 
BELONG_TO_LONG(BgL_xz00_59); 
return 
(BgL_n1z00_3442>((long)0));}  else 
{ /* Ieee/number.scm 630 */
if(
LLONGP(BgL_xz00_59))
{ /* Ieee/number.scm 631 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_59)>((BGL_LONGLONG_T)0));}  else 
{ /* Ieee/number.scm 631 */
if(
BIGNUMP(BgL_xz00_59))
{ /* Ieee/number.scm 632 */
return 
BXPOSITIVE(BgL_xz00_59);}  else 
{ /* Ieee/number.scm 632 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3768z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_59));} } } } } } 

}



/* &positive? */
obj_t BGl_z62positivezf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4996, obj_t BgL_xz00_4997)
{
{ /* Ieee/number.scm 626 */
return 
BBOOL(
BGl_positivezf3zf3zz__r4_numbers_6_5z00(BgL_xz00_4997));} 

}



/* negative? */
BGL_EXPORTED_DEF bool_t BGl_negativezf3zf3zz__r4_numbers_6_5z00(obj_t BgL_xz00_60)
{
{ /* Ieee/number.scm 638 */
if(
INTEGERP(BgL_xz00_60))
{ /* Ieee/number.scm 640 */
return 
(
(long)CINT(BgL_xz00_60)<0L);}  else 
{ /* Ieee/number.scm 640 */
if(
REALP(BgL_xz00_60))
{ /* Ieee/number.scm 641 */
return 
(
REAL_TO_DOUBLE(BgL_xz00_60)<((double)0.0));}  else 
{ /* Ieee/number.scm 641 */
if(
ELONGP(BgL_xz00_60))
{ /* Ieee/number.scm 642 */
 long BgL_n1z00_3452;
BgL_n1z00_3452 = 
BELONG_TO_LONG(BgL_xz00_60); 
return 
(BgL_n1z00_3452<((long)0));}  else 
{ /* Ieee/number.scm 642 */
if(
LLONGP(BgL_xz00_60))
{ /* Ieee/number.scm 643 */
return 
(
BLLONG_TO_LLONG(BgL_xz00_60)<((BGL_LONGLONG_T)0));}  else 
{ /* Ieee/number.scm 643 */
if(
BIGNUMP(BgL_xz00_60))
{ /* Ieee/number.scm 644 */
return 
BXNEGATIVE(BgL_xz00_60);}  else 
{ /* Ieee/number.scm 644 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string3769z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_60));} } } } } } 

}



/* &negative? */
obj_t BGl_z62negativezf3z91zz__r4_numbers_6_5z00(obj_t BgL_envz00_4998, obj_t BgL_xz00_4999)
{
{ /* Ieee/number.scm 638 */
return 
BBOOL(
BGl_negativezf3zf3zz__r4_numbers_6_5z00(BgL_xz00_4999));} 

}



/* 2max */
BGL_EXPORTED_DEF obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_73, obj_t BgL_yz00_74)
{
{ /* Ieee/number.scm 666 */
if(
INTEGERP(BgL_xz00_73))
{ /* Ieee/number.scm 667 */
if(
INTEGERP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
if(
(
(long)CINT(BgL_xz00_73)>
(long)CINT(BgL_yz00_74)))
{ /* Ieee/number.scm 651 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 651 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1837z00_1491;
BgL_arg1837z00_1491 = 
(double)(
(long)CINT(BgL_xz00_73)); 
if(
(BgL_arg1837z00_1491>
REAL_TO_DOUBLE(BgL_yz00_74)))
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1837z00_1491);}  else 
{ /* Ieee/number.scm 653 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 long BgL_arg1839z00_1493; obj_t BgL_arg1840z00_1494;
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8012;
BgL_tmpz00_8012 = 
(long)CINT(BgL_xz00_73); 
BgL_arg1839z00_1493 = 
(long)(BgL_tmpz00_8012); } 
BgL_arg1840z00_1494 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_74); 
{ /* Ieee/number.scm 655 */
 bool_t BgL_test4380z00_8016;
{ /* Ieee/number.scm 655 */
 long BgL_n2z00_3486;
{ /* Ieee/number.scm 655 */
 obj_t BgL_tmpz00_8017;
if(
ELONGP(BgL_arg1840z00_1494))
{ /* Ieee/number.scm 655 */
BgL_tmpz00_8017 = BgL_arg1840z00_1494
; }  else 
{ 
 obj_t BgL_auxz00_8020;
BgL_auxz00_8020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25197L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1840z00_1494); 
FAILURE(BgL_auxz00_8020,BFALSE,BFALSE);} 
BgL_n2z00_3486 = 
BELONG_TO_LONG(BgL_tmpz00_8017); } 
BgL_test4380z00_8016 = 
(BgL_arg1839z00_1493>BgL_n2z00_3486); } 
if(BgL_test4380z00_8016)
{ /* Ieee/number.scm 655 */
return 
make_belong(BgL_arg1839z00_1493);}  else 
{ /* Ieee/number.scm 655 */
return BgL_arg1840z00_1494;} } }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1842z00_1496;
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8029;
BgL_tmpz00_8029 = 
(long)CINT(BgL_xz00_73); 
BgL_arg1842z00_1496 = 
LONG_TO_LLONG(BgL_tmpz00_8029); } 
if(
(BgL_arg1842z00_1496>
BLLONG_TO_LLONG(BgL_yz00_74)))
{ /* Ieee/number.scm 657 */
return 
make_bllong(BgL_arg1842z00_1496);}  else 
{ /* Ieee/number.scm 657 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 uint64_t BgL_arg1845z00_1499;
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1846z00_1500;
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8038;
BgL_tmpz00_8038 = 
(long)CINT(BgL_xz00_73); 
BgL_arg1846z00_1500 = 
LONG_TO_LLONG(BgL_tmpz00_8038); } 
BgL_arg1845z00_1499 = 
(uint64_t)(BgL_arg1846z00_1500); } 
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4385z00_8042;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n2z00_3495;
BgL_n2z00_3495 = 
BGL_BINT64_TO_INT64(BgL_yz00_74); 
BgL_test4385z00_8042 = 
(BgL_arg1845z00_1499>BgL_n2z00_3495); } 
if(BgL_test4385z00_8042)
{ /* Ieee/number.scm 661 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1845z00_1499);}  else 
{ /* Ieee/number.scm 661 */
return BgL_yz00_74;} } }  else 
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1848z00_1502;
BgL_arg1848z00_1502 = 
bgl_long_to_bignum(
(long)CINT(BgL_xz00_73)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg1848z00_1502, BgL_yz00_74))>0L))
{ /* Ieee/number.scm 659 */
return BgL_arg1848z00_1502;}  else 
{ /* Ieee/number.scm 659 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_74);} } } } } } }  else 
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_xz00_73))
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
if(
(
REAL_TO_DOUBLE(BgL_xz00_73)>
REAL_TO_DOUBLE(BgL_yz00_74)))
{ /* Ieee/number.scm 653 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 653 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
INTEGERP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1852z00_1506;
BgL_arg1852z00_1506 = 
(double)(
(long)CINT(BgL_yz00_74)); 
if(
(
REAL_TO_DOUBLE(BgL_xz00_73)>BgL_arg1852z00_1506))
{ /* Ieee/number.scm 653 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1852z00_1506);} }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1854z00_1508;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1856z00_1509;
BgL_arg1856z00_1509 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_74); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8074;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8075;
if(
ELONGP(BgL_arg1856z00_1509))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8075 = BgL_arg1856z00_1509
; }  else 
{ 
 obj_t BgL_auxz00_8078;
BgL_auxz00_8078 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1856z00_1509); 
FAILURE(BgL_auxz00_8078,BFALSE,BFALSE);} 
BgL_tmpz00_8074 = 
BELONG_TO_LONG(BgL_tmpz00_8075); } 
BgL_arg1854z00_1508 = 
(double)(BgL_tmpz00_8074); } } 
if(
(
REAL_TO_DOUBLE(BgL_xz00_73)>BgL_arg1854z00_1508))
{ /* Ieee/number.scm 653 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1854z00_1508);} }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1858z00_1511;
BgL_arg1858z00_1511 = 
(double)(
BLLONG_TO_LLONG(BgL_yz00_74)); 
if(
(
REAL_TO_DOUBLE(BgL_xz00_73)>BgL_arg1858z00_1511))
{ /* Ieee/number.scm 653 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1858z00_1511);} }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1862z00_1514;
{ /* Ieee/number.scm 667 */
 uint64_t BgL_xz00_3514;
BgL_xz00_3514 = 
BGL_BINT64_TO_INT64(BgL_yz00_74); 
BgL_arg1862z00_1514 = 
(double)(BgL_xz00_3514); } 
if(
(
REAL_TO_DOUBLE(BgL_xz00_73)>BgL_arg1862z00_1514))
{ /* Ieee/number.scm 653 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1862z00_1514);} }  else 
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1864z00_1516;
BgL_arg1864z00_1516 = 
bgl_bignum_to_flonum(BgL_yz00_74); 
if(
(
REAL_TO_DOUBLE(BgL_xz00_73)>BgL_arg1864z00_1516))
{ /* Ieee/number.scm 653 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1864z00_1516);} }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_74);} } } } } } }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_73))
{ /* Ieee/number.scm 667 */
if(
INTEGERP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1868z00_1519; long BgL_arg1869z00_1520;
BgL_arg1868z00_1519 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_73); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8117;
BgL_tmpz00_8117 = 
(long)CINT(BgL_yz00_74); 
BgL_arg1869z00_1520 = 
(long)(BgL_tmpz00_8117); } 
{ /* Ieee/number.scm 655 */
 bool_t BgL_test4404z00_8120;
{ /* Ieee/number.scm 655 */
 long BgL_n1z00_3524;
{ /* Ieee/number.scm 655 */
 obj_t BgL_tmpz00_8121;
if(
ELONGP(BgL_arg1868z00_1519))
{ /* Ieee/number.scm 655 */
BgL_tmpz00_8121 = BgL_arg1868z00_1519
; }  else 
{ 
 obj_t BgL_auxz00_8124;
BgL_auxz00_8124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25195L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1868z00_1519); 
FAILURE(BgL_auxz00_8124,BFALSE,BFALSE);} 
BgL_n1z00_3524 = 
BELONG_TO_LONG(BgL_tmpz00_8121); } 
BgL_test4404z00_8120 = 
(BgL_n1z00_3524>BgL_arg1869z00_1520); } 
if(BgL_test4404z00_8120)
{ /* Ieee/number.scm 655 */
return BgL_arg1868z00_1519;}  else 
{ /* Ieee/number.scm 655 */
return 
make_belong(BgL_arg1869z00_1520);} } }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1872z00_1522; obj_t BgL_arg1873z00_1523;
BgL_arg1872z00_1522 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_73); 
BgL_arg1873z00_1523 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_74); 
{ /* Ieee/number.scm 655 */
 bool_t BgL_test4407z00_8135;
{ /* Ieee/number.scm 655 */
 long BgL_n1z00_3527; long BgL_n2z00_3528;
{ /* Ieee/number.scm 655 */
 obj_t BgL_tmpz00_8136;
if(
ELONGP(BgL_arg1872z00_1522))
{ /* Ieee/number.scm 655 */
BgL_tmpz00_8136 = BgL_arg1872z00_1522
; }  else 
{ 
 obj_t BgL_auxz00_8139;
BgL_auxz00_8139 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25195L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1872z00_1522); 
FAILURE(BgL_auxz00_8139,BFALSE,BFALSE);} 
BgL_n1z00_3527 = 
BELONG_TO_LONG(BgL_tmpz00_8136); } 
{ /* Ieee/number.scm 655 */
 obj_t BgL_tmpz00_8144;
if(
ELONGP(BgL_arg1873z00_1523))
{ /* Ieee/number.scm 655 */
BgL_tmpz00_8144 = BgL_arg1873z00_1523
; }  else 
{ 
 obj_t BgL_auxz00_8147;
BgL_auxz00_8147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25197L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1873z00_1523); 
FAILURE(BgL_auxz00_8147,BFALSE,BFALSE);} 
BgL_n2z00_3528 = 
BELONG_TO_LONG(BgL_tmpz00_8144); } 
BgL_test4407z00_8135 = 
(BgL_n1z00_3527>BgL_n2z00_3528); } 
if(BgL_test4407z00_8135)
{ /* Ieee/number.scm 655 */
return BgL_arg1872z00_1522;}  else 
{ /* Ieee/number.scm 655 */
return BgL_arg1873z00_1523;} } }  else 
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1875z00_1525;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1876z00_1526;
BgL_arg1876z00_1526 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_73); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8156;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8157;
if(
ELONGP(BgL_arg1876z00_1526))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8157 = BgL_arg1876z00_1526
; }  else 
{ 
 obj_t BgL_auxz00_8160;
BgL_auxz00_8160 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1876z00_1526); 
FAILURE(BgL_auxz00_8160,BFALSE,BFALSE);} 
BgL_tmpz00_8156 = 
BELONG_TO_LONG(BgL_tmpz00_8157); } 
BgL_arg1875z00_1525 = 
(double)(BgL_tmpz00_8156); } } 
if(
(BgL_arg1875z00_1525>
REAL_TO_DOUBLE(BgL_yz00_74)))
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1875z00_1525);}  else 
{ /* Ieee/number.scm 653 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1878z00_1528;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1880z00_1530;
BgL_arg1880z00_1530 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_73); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8173;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8174;
if(
ELONGP(BgL_arg1880z00_1530))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8174 = BgL_arg1880z00_1530
; }  else 
{ 
 obj_t BgL_auxz00_8177;
BgL_auxz00_8177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1880z00_1530); 
FAILURE(BgL_auxz00_8177,BFALSE,BFALSE);} 
BgL_tmpz00_8173 = 
BELONG_TO_LONG(BgL_tmpz00_8174); } 
BgL_arg1878z00_1528 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8173); } } 
if(
(BgL_arg1878z00_1528>
BLLONG_TO_LLONG(BgL_yz00_74)))
{ /* Ieee/number.scm 657 */
return 
make_bllong(BgL_arg1878z00_1528);}  else 
{ /* Ieee/number.scm 657 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 uint64_t BgL_arg1882z00_1532;
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1883z00_1533;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1884z00_1534;
BgL_arg1884z00_1534 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_73); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8190;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8191;
if(
ELONGP(BgL_arg1884z00_1534))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8191 = BgL_arg1884z00_1534
; }  else 
{ 
 obj_t BgL_auxz00_8194;
BgL_auxz00_8194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1884z00_1534); 
FAILURE(BgL_auxz00_8194,BFALSE,BFALSE);} 
BgL_tmpz00_8190 = 
BELONG_TO_LONG(BgL_tmpz00_8191); } 
BgL_arg1883z00_1533 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8190); } } 
BgL_arg1882z00_1532 = 
(uint64_t)(BgL_arg1883z00_1533); } 
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4418z00_8201;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n2z00_3538;
BgL_n2z00_3538 = 
BGL_BINT64_TO_INT64(BgL_yz00_74); 
BgL_test4418z00_8201 = 
(BgL_arg1882z00_1532>BgL_n2z00_3538); } 
if(BgL_test4418z00_8201)
{ /* Ieee/number.scm 661 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1882z00_1532);}  else 
{ /* Ieee/number.scm 661 */
return BgL_yz00_74;} } }  else 
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1887z00_1536;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1888z00_1537;
BgL_arg1888z00_1537 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_73); 
{ /* Ieee/number.scm 667 */
 long BgL_xz00_3539;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8208;
if(
ELONGP(BgL_arg1888z00_1537))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8208 = BgL_arg1888z00_1537
; }  else 
{ 
 obj_t BgL_auxz00_8211;
BgL_auxz00_8211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1888z00_1537); 
FAILURE(BgL_auxz00_8211,BFALSE,BFALSE);} 
BgL_xz00_3539 = 
BELONG_TO_LONG(BgL_tmpz00_8208); } 
BgL_arg1887z00_1536 = 
bgl_long_to_bignum(BgL_xz00_3539); } } 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg1887z00_1536, BgL_yz00_74))>0L))
{ /* Ieee/number.scm 659 */
return BgL_arg1887z00_1536;}  else 
{ /* Ieee/number.scm 659 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_74);} } } } } } }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_xz00_73))
{ /* Ieee/number.scm 667 */
if(
INTEGERP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1892z00_1541;
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8226;
BgL_tmpz00_8226 = 
(long)CINT(BgL_yz00_74); 
BgL_arg1892z00_1541 = 
LONG_TO_LLONG(BgL_tmpz00_8226); } 
if(
(
BLLONG_TO_LLONG(BgL_xz00_73)>BgL_arg1892z00_1541))
{ /* Ieee/number.scm 657 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 657 */
return 
make_bllong(BgL_arg1892z00_1541);} }  else 
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1894z00_1543;
BgL_arg1894z00_1543 = 
(double)(
BLLONG_TO_LLONG(BgL_xz00_73)); 
if(
(BgL_arg1894z00_1543>
REAL_TO_DOUBLE(BgL_yz00_74)))
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1894z00_1543);}  else 
{ /* Ieee/number.scm 653 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
if(
(
BLLONG_TO_LLONG(BgL_xz00_73)>
BLLONG_TO_LLONG(BgL_yz00_74)))
{ /* Ieee/number.scm 657 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 657 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1901z00_1549;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1902z00_1550;
BgL_arg1902z00_1550 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_74); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8250;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8251;
if(
ELONGP(BgL_arg1902z00_1550))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8251 = BgL_arg1902z00_1550
; }  else 
{ 
 obj_t BgL_auxz00_8254;
BgL_auxz00_8254 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1902z00_1550); 
FAILURE(BgL_auxz00_8254,BFALSE,BFALSE);} 
BgL_tmpz00_8250 = 
BELONG_TO_LONG(BgL_tmpz00_8251); } 
BgL_arg1901z00_1549 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8250); } } 
if(
(
BLLONG_TO_LLONG(BgL_xz00_73)>BgL_arg1901z00_1549))
{ /* Ieee/number.scm 657 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 657 */
return 
make_bllong(BgL_arg1901z00_1549);} }  else 
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1904z00_1552;
BgL_arg1904z00_1552 = 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_73)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg1904z00_1552, BgL_yz00_74))>0L))
{ /* Ieee/number.scm 659 */
return BgL_arg1904z00_1552;}  else 
{ /* Ieee/number.scm 659 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 uint64_t BgL_arg1910z00_1555;
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_tmpz00_8274;
BgL_tmpz00_8274 = 
BLLONG_TO_LLONG(BgL_xz00_73); 
BgL_arg1910z00_1555 = 
(uint64_t)(BgL_tmpz00_8274); } 
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4435z00_8277;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n2z00_3567;
BgL_n2z00_3567 = 
BGL_BINT64_TO_INT64(BgL_yz00_74); 
BgL_test4435z00_8277 = 
(BgL_arg1910z00_1555>BgL_n2z00_3567); } 
if(BgL_test4435z00_8277)
{ /* Ieee/number.scm 661 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1910z00_1555);}  else 
{ /* Ieee/number.scm 661 */
return BgL_yz00_74;} } }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_74);} } } } } } }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_xz00_73))
{ /* Ieee/number.scm 667 */
if(
INTEGERP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 uint64_t BgL_arg1913z00_1558;
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8286;
BgL_tmpz00_8286 = 
(long)CINT(BgL_yz00_74); 
BgL_arg1913z00_1558 = 
(uint64_t)(BgL_tmpz00_8286); } 
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4438z00_8289;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n1z00_3570;
BgL_n1z00_3570 = 
BGL_BINT64_TO_INT64(BgL_xz00_73); 
BgL_test4438z00_8289 = 
(BgL_n1z00_3570>BgL_arg1913z00_1558); } 
if(BgL_test4438z00_8289)
{ /* Ieee/number.scm 661 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 661 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1913z00_1558);} } }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_yz00_74))
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4440z00_8295;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n1z00_3573; uint64_t BgL_n2z00_3574;
BgL_n1z00_3573 = 
BGL_BINT64_TO_INT64(BgL_xz00_73); 
BgL_n2z00_3574 = 
BGL_BINT64_TO_INT64(BgL_yz00_74); 
BgL_test4440z00_8295 = 
(BgL_n1z00_3573>BgL_n2z00_3574); } 
if(BgL_test4440z00_8295)
{ /* Ieee/number.scm 661 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 661 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1916z00_1561;
{ /* Ieee/number.scm 667 */
 uint64_t BgL_tmpz00_8301;
BgL_tmpz00_8301 = 
BGL_BINT64_TO_INT64(BgL_xz00_73); 
BgL_arg1916z00_1561 = 
(double)(BgL_tmpz00_8301); } 
if(
(BgL_arg1916z00_1561>
REAL_TO_DOUBLE(BgL_yz00_74)))
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1916z00_1561);}  else 
{ /* Ieee/number.scm 653 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 uint64_t BgL_arg1918z00_1563;
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_tmpz00_8310;
BgL_tmpz00_8310 = 
BLLONG_TO_LLONG(BgL_yz00_74); 
BgL_arg1918z00_1563 = 
(uint64_t)(BgL_tmpz00_8310); } 
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4444z00_8313;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n1z00_3580;
BgL_n1z00_3580 = 
BGL_BINT64_TO_INT64(BgL_xz00_73); 
BgL_test4444z00_8313 = 
(BgL_n1z00_3580>BgL_arg1918z00_1563); } 
if(BgL_test4444z00_8313)
{ /* Ieee/number.scm 661 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 661 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1918z00_1563);} } }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 uint64_t BgL_arg1923z00_1566;
{ /* Ieee/number.scm 667 */
 BGL_LONGLONG_T BgL_arg1924z00_1567;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1925z00_1568;
BgL_arg1925z00_1568 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_74); 
{ /* Ieee/number.scm 667 */
 long BgL_tmpz00_8320;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8321;
if(
ELONGP(BgL_arg1925z00_1568))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8321 = BgL_arg1925z00_1568
; }  else 
{ 
 obj_t BgL_auxz00_8324;
BgL_auxz00_8324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1925z00_1568); 
FAILURE(BgL_auxz00_8324,BFALSE,BFALSE);} 
BgL_tmpz00_8320 = 
BELONG_TO_LONG(BgL_tmpz00_8321); } 
BgL_arg1924z00_1567 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8320); } } 
BgL_arg1923z00_1566 = 
(uint64_t)(BgL_arg1924z00_1567); } 
{ /* Ieee/number.scm 661 */
 bool_t BgL_test4447z00_8331;
{ /* Ieee/number.scm 661 */
 uint64_t BgL_n1z00_3584;
BgL_n1z00_3584 = 
BGL_BINT64_TO_INT64(BgL_xz00_73); 
BgL_test4447z00_8331 = 
(BgL_n1z00_3584>BgL_arg1923z00_1566); } 
if(BgL_test4447z00_8331)
{ /* Ieee/number.scm 661 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 661 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1923z00_1566);} } }  else 
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1927z00_1570;
BgL_arg1927z00_1570 = 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_73)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg1927z00_1570, BgL_yz00_74))>0L))
{ /* Ieee/number.scm 659 */
return BgL_arg1927z00_1570;}  else 
{ /* Ieee/number.scm 659 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_74);} } } } } } }  else 
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_xz00_73))
{ /* Ieee/number.scm 667 */
if(
BIGNUMP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_73, BgL_yz00_74))>0L))
{ /* Ieee/number.scm 659 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 659 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
INTEGERP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1931z00_1574;
BgL_arg1931z00_1574 = 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_74)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_73, BgL_arg1931z00_1574))>0L))
{ /* Ieee/number.scm 659 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 659 */
return BgL_arg1931z00_1574;} }  else 
{ /* Ieee/number.scm 667 */
if(
REALP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 double BgL_arg1933z00_1576;
BgL_arg1933z00_1576 = 
bgl_bignum_to_flonum(BgL_xz00_73); 
if(
(BgL_arg1933z00_1576>
REAL_TO_DOUBLE(BgL_yz00_74)))
{ /* Ieee/number.scm 653 */
return 
DOUBLE_TO_REAL(BgL_arg1933z00_1576);}  else 
{ /* Ieee/number.scm 653 */
return BgL_yz00_74;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1935z00_1578;
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1936z00_1579;
BgL_arg1936z00_1579 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_74); 
{ /* Ieee/number.scm 667 */
 long BgL_xz00_3606;
{ /* Ieee/number.scm 667 */
 obj_t BgL_tmpz00_8370;
if(
ELONGP(BgL_arg1936z00_1579))
{ /* Ieee/number.scm 667 */
BgL_tmpz00_8370 = BgL_arg1936z00_1579
; }  else 
{ 
 obj_t BgL_auxz00_8373;
BgL_auxz00_8373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(25612L), BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1936z00_1579); 
FAILURE(BgL_auxz00_8373,BFALSE,BFALSE);} 
BgL_xz00_3606 = 
BELONG_TO_LONG(BgL_tmpz00_8370); } 
BgL_arg1935z00_1578 = 
bgl_long_to_bignum(BgL_xz00_3606); } } 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_73, BgL_arg1935z00_1578))>0L))
{ /* Ieee/number.scm 659 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 659 */
return BgL_arg1935z00_1578;} }  else 
{ /* Ieee/number.scm 667 */
if(
LLONGP(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1938z00_1581;
BgL_arg1938z00_1581 = 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_74)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_73, BgL_arg1938z00_1581))>0L))
{ /* Ieee/number.scm 659 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 659 */
return BgL_arg1938z00_1581;} }  else 
{ /* Ieee/number.scm 667 */
if(
BGL_UINT64P(BgL_yz00_74))
{ /* Ieee/number.scm 667 */
 obj_t BgL_arg1941z00_1584;
BgL_arg1941z00_1584 = 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_74)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_73, BgL_arg1941z00_1584))>0L))
{ /* Ieee/number.scm 659 */
return BgL_xz00_73;}  else 
{ /* Ieee/number.scm 659 */
return BgL_arg1941z00_1584;} }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_74);} } } } } } }  else 
{ /* Ieee/number.scm 667 */
return 
BGl_errorz00zz__errorz00(BGl_string3770z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_73);} } } } } } } 

}



/* &2max */
obj_t BGl_z622maxz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5000, obj_t BgL_xz00_5001, obj_t BgL_yz00_5002)
{
{ /* Ieee/number.scm 666 */
return 
BGl_2maxz00zz__r4_numbers_6_5z00(BgL_xz00_5001, BgL_yz00_5002);} 

}



/* max */
BGL_EXPORTED_DEF obj_t BGl_maxz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_75, obj_t BgL_yz00_76)
{
{ /* Ieee/number.scm 672 */
{ 
 obj_t BgL_xz00_3636; obj_t BgL_yz00_3637;
BgL_xz00_3636 = BgL_xz00_75; 
BgL_yz00_3637 = BgL_yz00_76; 
BgL_loopz00_3635:
if(
PAIRP(BgL_yz00_3637))
{ 
 obj_t BgL_yz00_8407; obj_t BgL_xz00_8404;
BgL_xz00_8404 = 
BGl_2maxz00zz__r4_numbers_6_5z00(BgL_xz00_3636, 
CAR(BgL_yz00_3637)); 
BgL_yz00_8407 = 
CDR(BgL_yz00_3637); 
BgL_yz00_3637 = BgL_yz00_8407; 
BgL_xz00_3636 = BgL_xz00_8404; 
goto BgL_loopz00_3635;}  else 
{ /* Ieee/number.scm 675 */
return BgL_xz00_3636;} } } 

}



/* &max */
obj_t BGl_z62maxz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5003, obj_t BgL_xz00_5004, obj_t BgL_yz00_5005)
{
{ /* Ieee/number.scm 672 */
return 
BGl_maxz00zz__r4_numbers_6_5z00(BgL_xz00_5004, BgL_yz00_5005);} 

}



/* 2min */
BGL_EXPORTED_DEF obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_89, obj_t BgL_yz00_90)
{
{ /* Ieee/number.scm 698 */
if(
INTEGERP(BgL_xz00_89))
{ /* Ieee/number.scm 699 */
if(
INTEGERP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
if(
(
(long)CINT(BgL_xz00_89)>
(long)CINT(BgL_yz00_90)))
{ /* Ieee/number.scm 683 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 683 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1956z00_1603;
BgL_arg1956z00_1603 = 
(double)(
(long)CINT(BgL_xz00_89)); 
if(
(BgL_arg1956z00_1603>
REAL_TO_DOUBLE(BgL_yz00_90)))
{ /* Ieee/number.scm 685 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1956z00_1603);} }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 long BgL_arg1958z00_1605; obj_t BgL_arg1959z00_1606;
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8428;
BgL_tmpz00_8428 = 
(long)CINT(BgL_xz00_89); 
BgL_arg1958z00_1605 = 
(long)(BgL_tmpz00_8428); } 
BgL_arg1959z00_1606 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_90); 
{ /* Ieee/number.scm 687 */
 bool_t BgL_test4471z00_8432;
{ /* Ieee/number.scm 687 */
 long BgL_n2z00_3677;
{ /* Ieee/number.scm 687 */
 obj_t BgL_tmpz00_8433;
if(
ELONGP(BgL_arg1959z00_1606))
{ /* Ieee/number.scm 687 */
BgL_tmpz00_8433 = BgL_arg1959z00_1606
; }  else 
{ 
 obj_t BgL_auxz00_8436;
BgL_auxz00_8436 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26341L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1959z00_1606); 
FAILURE(BgL_auxz00_8436,BFALSE,BFALSE);} 
BgL_n2z00_3677 = 
BELONG_TO_LONG(BgL_tmpz00_8433); } 
BgL_test4471z00_8432 = 
(BgL_arg1958z00_1605>BgL_n2z00_3677); } 
if(BgL_test4471z00_8432)
{ /* Ieee/number.scm 687 */
return BgL_arg1959z00_1606;}  else 
{ /* Ieee/number.scm 687 */
return 
make_belong(BgL_arg1958z00_1605);} } }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg1961z00_1608;
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8445;
BgL_tmpz00_8445 = 
(long)CINT(BgL_xz00_89); 
BgL_arg1961z00_1608 = 
LONG_TO_LLONG(BgL_tmpz00_8445); } 
if(
(BgL_arg1961z00_1608>
BLLONG_TO_LLONG(BgL_yz00_90)))
{ /* Ieee/number.scm 689 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 689 */
return 
make_bllong(BgL_arg1961z00_1608);} }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 uint64_t BgL_arg1964z00_1611;
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg1965z00_1612;
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8454;
BgL_tmpz00_8454 = 
(long)CINT(BgL_xz00_89); 
BgL_arg1965z00_1612 = 
LONG_TO_LLONG(BgL_tmpz00_8454); } 
BgL_arg1964z00_1611 = 
(uint64_t)(BgL_arg1965z00_1612); } 
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4476z00_8458;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n2z00_3686;
BgL_n2z00_3686 = 
BGL_BINT64_TO_INT64(BgL_yz00_90); 
BgL_test4476z00_8458 = 
(BgL_arg1964z00_1611>BgL_n2z00_3686); } 
if(BgL_test4476z00_8458)
{ /* Ieee/number.scm 693 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 693 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1964z00_1611);} } }  else 
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1967z00_1614;
BgL_arg1967z00_1614 = 
bgl_long_to_bignum(
(long)CINT(BgL_xz00_89)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg1967z00_1614, BgL_yz00_90))>0L))
{ /* Ieee/number.scm 691 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 691 */
return BgL_arg1967z00_1614;} }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_90);} } } } } } }  else 
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_xz00_89))
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
if(
(
REAL_TO_DOUBLE(BgL_xz00_89)>
REAL_TO_DOUBLE(BgL_yz00_90)))
{ /* Ieee/number.scm 685 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 685 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
INTEGERP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1971z00_1618;
BgL_arg1971z00_1618 = 
(double)(
(long)CINT(BgL_yz00_90)); 
if(
(
REAL_TO_DOUBLE(BgL_xz00_89)>BgL_arg1971z00_1618))
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1971z00_1618);}  else 
{ /* Ieee/number.scm 685 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1973z00_1620;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1974z00_1621;
BgL_arg1974z00_1621 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_90); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8490;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8491;
if(
ELONGP(BgL_arg1974z00_1621))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8491 = BgL_arg1974z00_1621
; }  else 
{ 
 obj_t BgL_auxz00_8494;
BgL_auxz00_8494 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1974z00_1621); 
FAILURE(BgL_auxz00_8494,BFALSE,BFALSE);} 
BgL_tmpz00_8490 = 
BELONG_TO_LONG(BgL_tmpz00_8491); } 
BgL_arg1973z00_1620 = 
(double)(BgL_tmpz00_8490); } } 
if(
(
REAL_TO_DOUBLE(BgL_xz00_89)>BgL_arg1973z00_1620))
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1973z00_1620);}  else 
{ /* Ieee/number.scm 685 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1976z00_1623;
BgL_arg1976z00_1623 = 
(double)(
BLLONG_TO_LLONG(BgL_yz00_90)); 
if(
(
REAL_TO_DOUBLE(BgL_xz00_89)>BgL_arg1976z00_1623))
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1976z00_1623);}  else 
{ /* Ieee/number.scm 685 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1979z00_1626;
{ /* Ieee/number.scm 699 */
 uint64_t BgL_xz00_3705;
BgL_xz00_3705 = 
BGL_BINT64_TO_INT64(BgL_yz00_90); 
BgL_arg1979z00_1626 = 
(double)(BgL_xz00_3705); } 
if(
(
REAL_TO_DOUBLE(BgL_xz00_89)>BgL_arg1979z00_1626))
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1979z00_1626);}  else 
{ /* Ieee/number.scm 685 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1981z00_1628;
BgL_arg1981z00_1628 = 
bgl_bignum_to_flonum(BgL_yz00_90); 
if(
(
REAL_TO_DOUBLE(BgL_xz00_89)>BgL_arg1981z00_1628))
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1981z00_1628);}  else 
{ /* Ieee/number.scm 685 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_90);} } } } } } }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_89))
{ /* Ieee/number.scm 699 */
if(
INTEGERP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1984z00_1631; long BgL_arg1985z00_1632;
BgL_arg1984z00_1631 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_89); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8533;
BgL_tmpz00_8533 = 
(long)CINT(BgL_yz00_90); 
BgL_arg1985z00_1632 = 
(long)(BgL_tmpz00_8533); } 
{ /* Ieee/number.scm 687 */
 bool_t BgL_test4495z00_8536;
{ /* Ieee/number.scm 687 */
 long BgL_n1z00_3715;
{ /* Ieee/number.scm 687 */
 obj_t BgL_tmpz00_8537;
if(
ELONGP(BgL_arg1984z00_1631))
{ /* Ieee/number.scm 687 */
BgL_tmpz00_8537 = BgL_arg1984z00_1631
; }  else 
{ 
 obj_t BgL_auxz00_8540;
BgL_auxz00_8540 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26339L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1984z00_1631); 
FAILURE(BgL_auxz00_8540,BFALSE,BFALSE);} 
BgL_n1z00_3715 = 
BELONG_TO_LONG(BgL_tmpz00_8537); } 
BgL_test4495z00_8536 = 
(BgL_n1z00_3715>BgL_arg1985z00_1632); } 
if(BgL_test4495z00_8536)
{ /* Ieee/number.scm 687 */
return 
make_belong(BgL_arg1985z00_1632);}  else 
{ /* Ieee/number.scm 687 */
return BgL_arg1984z00_1631;} } }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1987z00_1634; obj_t BgL_arg1988z00_1635;
BgL_arg1987z00_1634 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_89); 
BgL_arg1988z00_1635 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_90); 
{ /* Ieee/number.scm 687 */
 bool_t BgL_test4498z00_8551;
{ /* Ieee/number.scm 687 */
 long BgL_n1z00_3718; long BgL_n2z00_3719;
{ /* Ieee/number.scm 687 */
 obj_t BgL_tmpz00_8552;
if(
ELONGP(BgL_arg1987z00_1634))
{ /* Ieee/number.scm 687 */
BgL_tmpz00_8552 = BgL_arg1987z00_1634
; }  else 
{ 
 obj_t BgL_auxz00_8555;
BgL_auxz00_8555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26339L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1987z00_1634); 
FAILURE(BgL_auxz00_8555,BFALSE,BFALSE);} 
BgL_n1z00_3718 = 
BELONG_TO_LONG(BgL_tmpz00_8552); } 
{ /* Ieee/number.scm 687 */
 obj_t BgL_tmpz00_8560;
if(
ELONGP(BgL_arg1988z00_1635))
{ /* Ieee/number.scm 687 */
BgL_tmpz00_8560 = BgL_arg1988z00_1635
; }  else 
{ 
 obj_t BgL_auxz00_8563;
BgL_auxz00_8563 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26341L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1988z00_1635); 
FAILURE(BgL_auxz00_8563,BFALSE,BFALSE);} 
BgL_n2z00_3719 = 
BELONG_TO_LONG(BgL_tmpz00_8560); } 
BgL_test4498z00_8551 = 
(BgL_n1z00_3718>BgL_n2z00_3719); } 
if(BgL_test4498z00_8551)
{ /* Ieee/number.scm 687 */
return BgL_arg1988z00_1635;}  else 
{ /* Ieee/number.scm 687 */
return BgL_arg1987z00_1634;} } }  else 
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg1990z00_1637;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1991z00_1638;
BgL_arg1991z00_1638 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_89); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8572;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8573;
if(
ELONGP(BgL_arg1991z00_1638))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8573 = BgL_arg1991z00_1638
; }  else 
{ 
 obj_t BgL_auxz00_8576;
BgL_auxz00_8576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1991z00_1638); 
FAILURE(BgL_auxz00_8576,BFALSE,BFALSE);} 
BgL_tmpz00_8572 = 
BELONG_TO_LONG(BgL_tmpz00_8573); } 
BgL_arg1990z00_1637 = 
(double)(BgL_tmpz00_8572); } } 
if(
(BgL_arg1990z00_1637>
REAL_TO_DOUBLE(BgL_yz00_90)))
{ /* Ieee/number.scm 685 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg1990z00_1637);} }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg1993z00_1640;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1995z00_1642;
BgL_arg1995z00_1642 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_89); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8589;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8590;
if(
ELONGP(BgL_arg1995z00_1642))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8590 = BgL_arg1995z00_1642
; }  else 
{ 
 obj_t BgL_auxz00_8593;
BgL_auxz00_8593 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1995z00_1642); 
FAILURE(BgL_auxz00_8593,BFALSE,BFALSE);} 
BgL_tmpz00_8589 = 
BELONG_TO_LONG(BgL_tmpz00_8590); } 
BgL_arg1993z00_1640 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8589); } } 
if(
(BgL_arg1993z00_1640>
BLLONG_TO_LLONG(BgL_yz00_90)))
{ /* Ieee/number.scm 689 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 689 */
return 
make_bllong(BgL_arg1993z00_1640);} }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 uint64_t BgL_arg1997z00_1644;
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg1998z00_1645;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg1999z00_1646;
BgL_arg1999z00_1646 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_89); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8606;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8607;
if(
ELONGP(BgL_arg1999z00_1646))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8607 = BgL_arg1999z00_1646
; }  else 
{ 
 obj_t BgL_auxz00_8610;
BgL_auxz00_8610 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg1999z00_1646); 
FAILURE(BgL_auxz00_8610,BFALSE,BFALSE);} 
BgL_tmpz00_8606 = 
BELONG_TO_LONG(BgL_tmpz00_8607); } 
BgL_arg1998z00_1645 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8606); } } 
BgL_arg1997z00_1644 = 
(uint64_t)(BgL_arg1998z00_1645); } 
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4509z00_8617;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n2z00_3729;
BgL_n2z00_3729 = 
BGL_BINT64_TO_INT64(BgL_yz00_90); 
BgL_test4509z00_8617 = 
(BgL_arg1997z00_1644>BgL_n2z00_3729); } 
if(BgL_test4509z00_8617)
{ /* Ieee/number.scm 693 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 693 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg1997z00_1644);} } }  else 
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2001z00_1648;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2002z00_1649;
BgL_arg2002z00_1649 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_89); 
{ /* Ieee/number.scm 699 */
 long BgL_xz00_3730;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8624;
if(
ELONGP(BgL_arg2002z00_1649))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8624 = BgL_arg2002z00_1649
; }  else 
{ 
 obj_t BgL_auxz00_8627;
BgL_auxz00_8627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2002z00_1649); 
FAILURE(BgL_auxz00_8627,BFALSE,BFALSE);} 
BgL_xz00_3730 = 
BELONG_TO_LONG(BgL_tmpz00_8624); } 
BgL_arg2001z00_1648 = 
bgl_long_to_bignum(BgL_xz00_3730); } } 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg2001z00_1648, BgL_yz00_90))>0L))
{ /* Ieee/number.scm 691 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 691 */
return BgL_arg2001z00_1648;} }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_90);} } } } } } }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_xz00_89))
{ /* Ieee/number.scm 699 */
if(
INTEGERP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg2007z00_1653;
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8642;
BgL_tmpz00_8642 = 
(long)CINT(BgL_yz00_90); 
BgL_arg2007z00_1653 = 
LONG_TO_LLONG(BgL_tmpz00_8642); } 
if(
(
BLLONG_TO_LLONG(BgL_xz00_89)>BgL_arg2007z00_1653))
{ /* Ieee/number.scm 689 */
return 
make_bllong(BgL_arg2007z00_1653);}  else 
{ /* Ieee/number.scm 689 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg2009z00_1655;
BgL_arg2009z00_1655 = 
(double)(
BLLONG_TO_LLONG(BgL_xz00_89)); 
if(
(BgL_arg2009z00_1655>
REAL_TO_DOUBLE(BgL_yz00_90)))
{ /* Ieee/number.scm 685 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg2009z00_1655);} }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
if(
(
BLLONG_TO_LLONG(BgL_xz00_89)>
BLLONG_TO_LLONG(BgL_yz00_90)))
{ /* Ieee/number.scm 689 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 689 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg2015z00_1661;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2016z00_1662;
BgL_arg2016z00_1662 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_90); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8666;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8667;
if(
ELONGP(BgL_arg2016z00_1662))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8667 = BgL_arg2016z00_1662
; }  else 
{ 
 obj_t BgL_auxz00_8670;
BgL_auxz00_8670 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2016z00_1662); 
FAILURE(BgL_auxz00_8670,BFALSE,BFALSE);} 
BgL_tmpz00_8666 = 
BELONG_TO_LONG(BgL_tmpz00_8667); } 
BgL_arg2015z00_1661 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8666); } } 
if(
(
BLLONG_TO_LLONG(BgL_xz00_89)>BgL_arg2015z00_1661))
{ /* Ieee/number.scm 689 */
return 
make_bllong(BgL_arg2015z00_1661);}  else 
{ /* Ieee/number.scm 689 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2018z00_1664;
BgL_arg2018z00_1664 = 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_89)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg2018z00_1664, BgL_yz00_90))>0L))
{ /* Ieee/number.scm 691 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 691 */
return BgL_arg2018z00_1664;} }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 uint64_t BgL_arg2021z00_1667;
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_tmpz00_8690;
BgL_tmpz00_8690 = 
BLLONG_TO_LLONG(BgL_xz00_89); 
BgL_arg2021z00_1667 = 
(uint64_t)(BgL_tmpz00_8690); } 
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4526z00_8693;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n2z00_3758;
BgL_n2z00_3758 = 
BGL_BINT64_TO_INT64(BgL_yz00_90); 
BgL_test4526z00_8693 = 
(BgL_arg2021z00_1667>BgL_n2z00_3758); } 
if(BgL_test4526z00_8693)
{ /* Ieee/number.scm 693 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 693 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg2021z00_1667);} } }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_90);} } } } } } }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_xz00_89))
{ /* Ieee/number.scm 699 */
if(
INTEGERP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 uint64_t BgL_arg2024z00_1670;
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8702;
BgL_tmpz00_8702 = 
(long)CINT(BgL_yz00_90); 
BgL_arg2024z00_1670 = 
(uint64_t)(BgL_tmpz00_8702); } 
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4529z00_8705;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n1z00_3761;
BgL_n1z00_3761 = 
BGL_BINT64_TO_INT64(BgL_xz00_89); 
BgL_test4529z00_8705 = 
(BgL_n1z00_3761>BgL_arg2024z00_1670); } 
if(BgL_test4529z00_8705)
{ /* Ieee/number.scm 693 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg2024z00_1670);}  else 
{ /* Ieee/number.scm 693 */
return BgL_xz00_89;} } }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_yz00_90))
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4531z00_8711;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n1z00_3764; uint64_t BgL_n2z00_3765;
BgL_n1z00_3764 = 
BGL_BINT64_TO_INT64(BgL_xz00_89); 
BgL_n2z00_3765 = 
BGL_BINT64_TO_INT64(BgL_yz00_90); 
BgL_test4531z00_8711 = 
(BgL_n1z00_3764>BgL_n2z00_3765); } 
if(BgL_test4531z00_8711)
{ /* Ieee/number.scm 693 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 693 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg2027z00_1673;
{ /* Ieee/number.scm 699 */
 uint64_t BgL_tmpz00_8717;
BgL_tmpz00_8717 = 
BGL_BINT64_TO_INT64(BgL_xz00_89); 
BgL_arg2027z00_1673 = 
(double)(BgL_tmpz00_8717); } 
if(
(BgL_arg2027z00_1673>
REAL_TO_DOUBLE(BgL_yz00_90)))
{ /* Ieee/number.scm 685 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg2027z00_1673);} }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 uint64_t BgL_arg2029z00_1675;
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_tmpz00_8726;
BgL_tmpz00_8726 = 
BLLONG_TO_LLONG(BgL_yz00_90); 
BgL_arg2029z00_1675 = 
(uint64_t)(BgL_tmpz00_8726); } 
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4535z00_8729;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n1z00_3771;
BgL_n1z00_3771 = 
BGL_BINT64_TO_INT64(BgL_xz00_89); 
BgL_test4535z00_8729 = 
(BgL_n1z00_3771>BgL_arg2029z00_1675); } 
if(BgL_test4535z00_8729)
{ /* Ieee/number.scm 693 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg2029z00_1675);}  else 
{ /* Ieee/number.scm 693 */
return BgL_xz00_89;} } }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 uint64_t BgL_arg2033z00_1678;
{ /* Ieee/number.scm 699 */
 BGL_LONGLONG_T BgL_arg2034z00_1679;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2036z00_1680;
BgL_arg2036z00_1680 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_90); 
{ /* Ieee/number.scm 699 */
 long BgL_tmpz00_8736;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8737;
if(
ELONGP(BgL_arg2036z00_1680))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8737 = BgL_arg2036z00_1680
; }  else 
{ 
 obj_t BgL_auxz00_8740;
BgL_auxz00_8740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2036z00_1680); 
FAILURE(BgL_auxz00_8740,BFALSE,BFALSE);} 
BgL_tmpz00_8736 = 
BELONG_TO_LONG(BgL_tmpz00_8737); } 
BgL_arg2034z00_1679 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8736); } } 
BgL_arg2033z00_1678 = 
(uint64_t)(BgL_arg2034z00_1679); } 
{ /* Ieee/number.scm 693 */
 bool_t BgL_test4538z00_8747;
{ /* Ieee/number.scm 693 */
 uint64_t BgL_n1z00_3775;
BgL_n1z00_3775 = 
BGL_BINT64_TO_INT64(BgL_xz00_89); 
BgL_test4538z00_8747 = 
(BgL_n1z00_3775>BgL_arg2033z00_1678); } 
if(BgL_test4538z00_8747)
{ /* Ieee/number.scm 693 */
return 
BGL_UINT64_TO_BUINT64(BgL_arg2033z00_1678);}  else 
{ /* Ieee/number.scm 693 */
return BgL_xz00_89;} } }  else 
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2038z00_1682;
BgL_arg2038z00_1682 = 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_89)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_arg2038z00_1682, BgL_yz00_90))>0L))
{ /* Ieee/number.scm 691 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 691 */
return BgL_arg2038z00_1682;} }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_90);} } } } } } }  else 
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_xz00_89))
{ /* Ieee/number.scm 699 */
if(
BIGNUMP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_89, BgL_yz00_90))>0L))
{ /* Ieee/number.scm 691 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 691 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
INTEGERP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2042z00_1686;
BgL_arg2042z00_1686 = 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_90)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_89, BgL_arg2042z00_1686))>0L))
{ /* Ieee/number.scm 691 */
return BgL_arg2042z00_1686;}  else 
{ /* Ieee/number.scm 691 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
REALP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 double BgL_arg2044z00_1688;
BgL_arg2044z00_1688 = 
bgl_bignum_to_flonum(BgL_xz00_89); 
if(
(BgL_arg2044z00_1688>
REAL_TO_DOUBLE(BgL_yz00_90)))
{ /* Ieee/number.scm 685 */
return BgL_yz00_90;}  else 
{ /* Ieee/number.scm 685 */
return 
DOUBLE_TO_REAL(BgL_arg2044z00_1688);} }  else 
{ /* Ieee/number.scm 699 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2046z00_1690;
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2047z00_1691;
BgL_arg2047z00_1691 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_90); 
{ /* Ieee/number.scm 699 */
 long BgL_xz00_3797;
{ /* Ieee/number.scm 699 */
 obj_t BgL_tmpz00_8786;
if(
ELONGP(BgL_arg2047z00_1691))
{ /* Ieee/number.scm 699 */
BgL_tmpz00_8786 = BgL_arg2047z00_1691
; }  else 
{ 
 obj_t BgL_auxz00_8789;
BgL_auxz00_8789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(26756L), BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2047z00_1691); 
FAILURE(BgL_auxz00_8789,BFALSE,BFALSE);} 
BgL_xz00_3797 = 
BELONG_TO_LONG(BgL_tmpz00_8786); } 
BgL_arg2046z00_1690 = 
bgl_long_to_bignum(BgL_xz00_3797); } } 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_89, BgL_arg2046z00_1690))>0L))
{ /* Ieee/number.scm 691 */
return BgL_arg2046z00_1690;}  else 
{ /* Ieee/number.scm 691 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
LLONGP(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2049z00_1693;
BgL_arg2049z00_1693 = 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_90)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_89, BgL_arg2049z00_1693))>0L))
{ /* Ieee/number.scm 691 */
return BgL_arg2049z00_1693;}  else 
{ /* Ieee/number.scm 691 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
if(
BGL_UINT64P(BgL_yz00_90))
{ /* Ieee/number.scm 699 */
 obj_t BgL_arg2052z00_1696;
BgL_arg2052z00_1696 = 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_90)); 
if(
(
(long)(
bgl_bignum_cmp(BgL_xz00_89, BgL_arg2052z00_1696))>0L))
{ /* Ieee/number.scm 691 */
return BgL_arg2052z00_1696;}  else 
{ /* Ieee/number.scm 691 */
return BgL_xz00_89;} }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_90);} } } } } } }  else 
{ /* Ieee/number.scm 699 */
return 
BGl_errorz00zz__errorz00(BGl_string3771z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_89);} } } } } } } 

}



/* &2min */
obj_t BGl_z622minz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5006, obj_t BgL_xz00_5007, obj_t BgL_yz00_5008)
{
{ /* Ieee/number.scm 698 */
return 
BGl_2minz00zz__r4_numbers_6_5z00(BgL_xz00_5007, BgL_yz00_5008);} 

}



/* min */
BGL_EXPORTED_DEF obj_t BGl_minz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_91, obj_t BgL_yz00_92)
{
{ /* Ieee/number.scm 704 */
{ 
 obj_t BgL_xz00_3827; obj_t BgL_yz00_3828;
BgL_xz00_3827 = BgL_xz00_91; 
BgL_yz00_3828 = BgL_yz00_92; 
BgL_loopz00_3826:
if(
PAIRP(BgL_yz00_3828))
{ 
 obj_t BgL_yz00_8823; obj_t BgL_xz00_8820;
BgL_xz00_8820 = 
BGl_2minz00zz__r4_numbers_6_5z00(BgL_xz00_3827, 
CAR(BgL_yz00_3828)); 
BgL_yz00_8823 = 
CDR(BgL_yz00_3828); 
BgL_yz00_3828 = BgL_yz00_8823; 
BgL_xz00_3827 = BgL_xz00_8820; 
goto BgL_loopz00_3826;}  else 
{ /* Ieee/number.scm 707 */
return BgL_xz00_3827;} } } 

}



/* &min */
obj_t BGl_z62minz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5009, obj_t BgL_xz00_5010, obj_t BgL_yz00_5011)
{
{ /* Ieee/number.scm 704 */
return 
BGl_minz00zz__r4_numbers_6_5z00(BgL_xz00_5010, BgL_yz00_5011);} 

}



/* 2+ */
BGL_EXPORTED_DEF obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t BgL_xz00_93, obj_t BgL_yz00_94)
{
{ /* Ieee/number.scm 714 */
if(
INTEGERP(BgL_xz00_93))
{ /* Ieee/number.scm 715 */
if(
INTEGERP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 long BgL_auxz00_8832; long BgL_tmpz00_8830;
BgL_auxz00_8832 = 
(long)CINT(BgL_yz00_94); 
BgL_tmpz00_8830 = 
(long)CINT(BgL_xz00_93); 
return 
BGL_SAFE_PLUS_FX(BgL_tmpz00_8830, BgL_auxz00_8832);}  else 
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_93))+
REAL_TO_DOUBLE(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 long BgL_arg2063z00_1711; obj_t BgL_arg2064z00_1712;
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8844;
BgL_tmpz00_8844 = 
(long)CINT(BgL_xz00_93); 
BgL_arg2063z00_1711 = 
(long)(BgL_tmpz00_8844); } 
BgL_arg2064z00_1712 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8848;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8849;
if(
ELONGP(BgL_arg2064z00_1712))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8849 = BgL_arg2064z00_1712
; }  else 
{ 
 obj_t BgL_auxz00_8852;
BgL_auxz00_8852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2064z00_1712); 
FAILURE(BgL_auxz00_8852,BFALSE,BFALSE);} 
BgL_tmpz00_8848 = 
BELONG_TO_LONG(BgL_tmpz00_8849); } 
return 
BGL_SAFE_PLUS_ELONG(BgL_arg2063z00_1711, BgL_tmpz00_8848);} }  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2067z00_1714;
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8860;
BgL_tmpz00_8860 = 
(long)CINT(BgL_xz00_93); 
BgL_arg2067z00_1714 = 
LONG_TO_LLONG(BgL_tmpz00_8860); } 
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_tmpz00_8863;
BgL_tmpz00_8863 = 
BLLONG_TO_LLONG(BgL_yz00_94); 
return 
BGL_SAFE_PLUS_LLONG(BgL_arg2067z00_1714, BgL_tmpz00_8863);} }  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_arg2070z00_1717;
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2072z00_1718;
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8868;
BgL_tmpz00_8868 = 
(long)CINT(BgL_xz00_93); 
BgL_arg2072z00_1718 = 
LONG_TO_LLONG(BgL_tmpz00_8868); } 
BgL_arg2070z00_1717 = 
(uint64_t)(BgL_arg2072z00_1718); } 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za72za7_3846;
BgL_za72za7_3846 = 
BGL_BINT64_TO_INT64(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_8873;
BgL_tmpz00_8873 = 
(BgL_arg2070z00_1717+BgL_za72za7_3846); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_8873);} } }  else 
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8878;
BgL_tmpz00_8878 = 
bgl_bignum_add(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_93)), BgL_yz00_94); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_8878);}  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_94);} } } } } } }  else 
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_xz00_93))
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_93)+
REAL_TO_DOUBLE(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
INTEGERP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_93)+
(double)(
(long)CINT(BgL_yz00_94))));}  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 double BgL_arg2081z00_1727;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2082z00_1728;
BgL_arg2082z00_1728 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8902;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8903;
if(
ELONGP(BgL_arg2082z00_1728))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8903 = BgL_arg2082z00_1728
; }  else 
{ 
 obj_t BgL_auxz00_8906;
BgL_auxz00_8906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2082z00_1728); 
FAILURE(BgL_auxz00_8906,BFALSE,BFALSE);} 
BgL_tmpz00_8902 = 
BELONG_TO_LONG(BgL_tmpz00_8903); } 
BgL_arg2081z00_1727 = 
(double)(BgL_tmpz00_8902); } } 
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_93)+BgL_arg2081z00_1727));}  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_93)+
(double)(
BLLONG_TO_LLONG(BgL_yz00_94))));}  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 double BgL_arg2088z00_1733;
{ /* Ieee/number.scm 715 */
 uint64_t BgL_xz00_3858;
BgL_xz00_3858 = 
BGL_BINT64_TO_INT64(BgL_yz00_94); 
BgL_arg2088z00_1733 = 
(double)(BgL_xz00_3858); } 
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_93)+BgL_arg2088z00_1733));}  else 
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_93)+
bgl_bignum_to_flonum(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_94);} } } } } } }  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_93))
{ /* Ieee/number.scm 715 */
if(
INTEGERP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2093z00_1738; long BgL_arg2094z00_1739;
BgL_arg2093z00_1738 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8941;
BgL_tmpz00_8941 = 
(long)CINT(BgL_yz00_94); 
BgL_arg2094z00_1739 = 
(long)(BgL_tmpz00_8941); } 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8944;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8945;
if(
ELONGP(BgL_arg2093z00_1738))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8945 = BgL_arg2093z00_1738
; }  else 
{ 
 obj_t BgL_auxz00_8948;
BgL_auxz00_8948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2093z00_1738); 
FAILURE(BgL_auxz00_8948,BFALSE,BFALSE);} 
BgL_tmpz00_8944 = 
BELONG_TO_LONG(BgL_tmpz00_8945); } 
return 
BGL_SAFE_PLUS_ELONG(BgL_tmpz00_8944, BgL_arg2094z00_1739);} }  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2096z00_1741; obj_t BgL_arg2097z00_1742;
BgL_arg2096z00_1741 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_93); 
BgL_arg2097z00_1742 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 long BgL_auxz00_8967; long BgL_tmpz00_8958;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8968;
if(
ELONGP(BgL_arg2097z00_1742))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8968 = BgL_arg2097z00_1742
; }  else 
{ 
 obj_t BgL_auxz00_8971;
BgL_auxz00_8971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2097z00_1742); 
FAILURE(BgL_auxz00_8971,BFALSE,BFALSE);} 
BgL_auxz00_8967 = 
BELONG_TO_LONG(BgL_tmpz00_8968); } 
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8959;
if(
ELONGP(BgL_arg2096z00_1741))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8959 = BgL_arg2096z00_1741
; }  else 
{ 
 obj_t BgL_auxz00_8962;
BgL_auxz00_8962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2096z00_1741); 
FAILURE(BgL_auxz00_8962,BFALSE,BFALSE);} 
BgL_tmpz00_8958 = 
BELONG_TO_LONG(BgL_tmpz00_8959); } 
return 
BGL_SAFE_PLUS_ELONG(BgL_tmpz00_8958, BgL_auxz00_8967);} }  else 
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 double BgL_arg2099z00_1744;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2100z00_1745;
BgL_arg2100z00_1745 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8980;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8981;
if(
ELONGP(BgL_arg2100z00_1745))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8981 = BgL_arg2100z00_1745
; }  else 
{ 
 obj_t BgL_auxz00_8984;
BgL_auxz00_8984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2100z00_1745); 
FAILURE(BgL_auxz00_8984,BFALSE,BFALSE);} 
BgL_tmpz00_8980 = 
BELONG_TO_LONG(BgL_tmpz00_8981); } 
BgL_arg2099z00_1744 = 
(double)(BgL_tmpz00_8980); } } 
return 
DOUBLE_TO_REAL(
(BgL_arg2099z00_1744+
REAL_TO_DOUBLE(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2102z00_1747;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2104z00_1749;
BgL_arg2104z00_1749 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_8996;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_8997;
if(
ELONGP(BgL_arg2104z00_1749))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_8997 = BgL_arg2104z00_1749
; }  else 
{ 
 obj_t BgL_auxz00_9000;
BgL_auxz00_9000 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2104z00_1749); 
FAILURE(BgL_auxz00_9000,BFALSE,BFALSE);} 
BgL_tmpz00_8996 = 
BELONG_TO_LONG(BgL_tmpz00_8997); } 
BgL_arg2102z00_1747 = 
(BGL_LONGLONG_T)(BgL_tmpz00_8996); } } 
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_tmpz00_9006;
BgL_tmpz00_9006 = 
BLLONG_TO_LLONG(BgL_yz00_94); 
return 
BGL_SAFE_PLUS_LLONG(BgL_arg2102z00_1747, BgL_tmpz00_9006);} }  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_arg2106z00_1751;
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2107z00_1752;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2108z00_1753;
BgL_arg2108z00_1753 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_9012;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9013;
if(
ELONGP(BgL_arg2108z00_1753))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_9013 = BgL_arg2108z00_1753
; }  else 
{ 
 obj_t BgL_auxz00_9016;
BgL_auxz00_9016 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2108z00_1753); 
FAILURE(BgL_auxz00_9016,BFALSE,BFALSE);} 
BgL_tmpz00_9012 = 
BELONG_TO_LONG(BgL_tmpz00_9013); } 
BgL_arg2107z00_1752 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9012); } } 
BgL_arg2106z00_1751 = 
(uint64_t)(BgL_arg2107z00_1752); } 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za72za7_3869;
BgL_za72za7_3869 = 
BGL_BINT64_TO_INT64(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9024;
BgL_tmpz00_9024 = 
(BgL_arg2106z00_1751+BgL_za72za7_3869); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9024);} } }  else 
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2110z00_1755;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2111z00_1756;
BgL_arg2111z00_1756 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 long BgL_xz00_3870;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9030;
if(
ELONGP(BgL_arg2111z00_1756))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_9030 = BgL_arg2111z00_1756
; }  else 
{ 
 obj_t BgL_auxz00_9033;
BgL_auxz00_9033 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2111z00_1756); 
FAILURE(BgL_auxz00_9033,BFALSE,BFALSE);} 
BgL_xz00_3870 = 
BELONG_TO_LONG(BgL_tmpz00_9030); } 
BgL_arg2110z00_1755 = 
bgl_long_to_bignum(BgL_xz00_3870); } } 
return 
bgl_bignum_add(BgL_arg2110z00_1755, BgL_yz00_94);}  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_94);} } } } } } }  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_xz00_93))
{ /* Ieee/number.scm 715 */
if(
INTEGERP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2115z00_1760;
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_9045;
BgL_tmpz00_9045 = 
(long)CINT(BgL_yz00_94); 
BgL_arg2115z00_1760 = 
LONG_TO_LLONG(BgL_tmpz00_9045); } 
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_tmpz00_9048;
BgL_tmpz00_9048 = 
BLLONG_TO_LLONG(BgL_xz00_93); 
return 
BGL_SAFE_PLUS_LLONG(BgL_tmpz00_9048, BgL_arg2115z00_1760);} }  else 
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_93))+
REAL_TO_DOUBLE(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_auxz00_9062; BGL_LONGLONG_T BgL_tmpz00_9060;
BgL_auxz00_9062 = 
BLLONG_TO_LLONG(BgL_yz00_94); 
BgL_tmpz00_9060 = 
BLLONG_TO_LLONG(BgL_xz00_93); 
return 
BGL_SAFE_PLUS_LLONG(BgL_tmpz00_9060, BgL_auxz00_9062);}  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2123z00_1768;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2124z00_1769;
BgL_arg2124z00_1769 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_9068;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9069;
if(
ELONGP(BgL_arg2124z00_1769))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_9069 = BgL_arg2124z00_1769
; }  else 
{ 
 obj_t BgL_auxz00_9072;
BgL_auxz00_9072 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2124z00_1769); 
FAILURE(BgL_auxz00_9072,BFALSE,BFALSE);} 
BgL_tmpz00_9068 = 
BELONG_TO_LONG(BgL_tmpz00_9069); } 
BgL_arg2123z00_1768 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9068); } } 
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_tmpz00_9078;
BgL_tmpz00_9078 = 
BLLONG_TO_LLONG(BgL_xz00_93); 
return 
BGL_SAFE_PLUS_LLONG(BgL_tmpz00_9078, BgL_arg2123z00_1768);} }  else 
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
bgl_bignum_add(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_93)), BgL_yz00_94);}  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_arg2129z00_1774;
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_tmpz00_9088;
BgL_tmpz00_9088 = 
BLLONG_TO_LLONG(BgL_xz00_93); 
BgL_arg2129z00_1774 = 
(uint64_t)(BgL_tmpz00_9088); } 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za72za7_3881;
BgL_za72za7_3881 = 
BGL_BINT64_TO_INT64(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9092;
BgL_tmpz00_9092 = 
(BgL_arg2129z00_1774+BgL_za72za7_3881); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9092);} } }  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_94);} } } } } } }  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_xz00_93))
{ /* Ieee/number.scm 715 */
if(
INTEGERP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_arg2132z00_1777;
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_9100;
BgL_tmpz00_9100 = 
(long)CINT(BgL_yz00_94); 
BgL_arg2132z00_1777 = 
(uint64_t)(BgL_tmpz00_9100); } 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za71za7_3883;
BgL_za71za7_3883 = 
BGL_BINT64_TO_INT64(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9104;
BgL_tmpz00_9104 = 
(BgL_za71za7_3883+BgL_arg2132z00_1777); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9104);} } }  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za71za7_3885; uint64_t BgL_za72za7_3886;
BgL_za71za7_3885 = 
BGL_BINT64_TO_INT64(BgL_xz00_93); 
BgL_za72za7_3886 = 
BGL_BINT64_TO_INT64(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9111;
BgL_tmpz00_9111 = 
(BgL_za71za7_3885+BgL_za72za7_3886); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9111);} }  else 
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 double BgL_arg2135z00_1780;
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9116;
BgL_tmpz00_9116 = 
BGL_BINT64_TO_INT64(BgL_xz00_93); 
BgL_arg2135z00_1780 = 
(double)(BgL_tmpz00_9116); } 
return 
DOUBLE_TO_REAL(
(BgL_arg2135z00_1780+
REAL_TO_DOUBLE(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_arg2137z00_1782;
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_tmpz00_9124;
BgL_tmpz00_9124 = 
BLLONG_TO_LLONG(BgL_yz00_94); 
BgL_arg2137z00_1782 = 
(uint64_t)(BgL_tmpz00_9124); } 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za71za7_3890;
BgL_za71za7_3890 = 
BGL_BINT64_TO_INT64(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9128;
BgL_tmpz00_9128 = 
(BgL_za71za7_3890+BgL_arg2137z00_1782); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9128);} } }  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 uint64_t BgL_arg2141z00_1785;
{ /* Ieee/number.scm 715 */
 BGL_LONGLONG_T BgL_arg2142z00_1786;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2143z00_1787;
BgL_arg2143z00_1787 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 long BgL_tmpz00_9134;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9135;
if(
ELONGP(BgL_arg2143z00_1787))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_9135 = BgL_arg2143z00_1787
; }  else 
{ 
 obj_t BgL_auxz00_9138;
BgL_auxz00_9138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2143z00_1787); 
FAILURE(BgL_auxz00_9138,BFALSE,BFALSE);} 
BgL_tmpz00_9134 = 
BELONG_TO_LONG(BgL_tmpz00_9135); } 
BgL_arg2142z00_1786 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9134); } } 
BgL_arg2141z00_1785 = 
(uint64_t)(BgL_arg2142z00_1786); } 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_za71za7_3893;
BgL_za71za7_3893 = 
BGL_BINT64_TO_INT64(BgL_xz00_93); 
{ /* Ieee/number.scm 715 */
 uint64_t BgL_tmpz00_9146;
BgL_tmpz00_9146 = 
(BgL_za71za7_3893+BgL_arg2141z00_1785); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9146);} } }  else 
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
bgl_bignum_add(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_93)), BgL_yz00_94);}  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_94);} } } } } } }  else 
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_xz00_93))
{ /* Ieee/number.scm 715 */
if(
BIGNUMP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9159;
BgL_tmpz00_9159 = 
bgl_bignum_add(BgL_xz00_93, BgL_yz00_94); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9159);}  else 
{ /* Ieee/number.scm 715 */
if(
INTEGERP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9164;
BgL_tmpz00_9164 = 
bgl_bignum_add(BgL_xz00_93, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_94))); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9164);}  else 
{ /* Ieee/number.scm 715 */
if(
REALP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_93)+
REAL_TO_DOUBLE(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2156z00_1799;
{ /* Ieee/number.scm 715 */
 obj_t BgL_arg2157z00_1800;
BgL_arg2157z00_1800 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_94); 
{ /* Ieee/number.scm 715 */
 long BgL_xz00_3905;
{ /* Ieee/number.scm 715 */
 obj_t BgL_tmpz00_9178;
if(
ELONGP(BgL_arg2157z00_1800))
{ /* Ieee/number.scm 715 */
BgL_tmpz00_9178 = BgL_arg2157z00_1800
; }  else 
{ 
 obj_t BgL_auxz00_9181;
BgL_auxz00_9181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27355L), BGl_string3772z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2157z00_1800); 
FAILURE(BgL_auxz00_9181,BFALSE,BFALSE);} 
BgL_xz00_3905 = 
BELONG_TO_LONG(BgL_tmpz00_9178); } 
BgL_arg2156z00_1799 = 
bgl_long_to_bignum(BgL_xz00_3905); } } 
return 
bgl_bignum_add(BgL_xz00_93, BgL_arg2156z00_1799);}  else 
{ /* Ieee/number.scm 715 */
if(
LLONGP(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
bgl_bignum_add(BgL_xz00_93, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
if(
BGL_UINT64P(BgL_yz00_94))
{ /* Ieee/number.scm 715 */
return 
bgl_bignum_add(BgL_xz00_93, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_94)));}  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_94);} } } } } } }  else 
{ /* Ieee/number.scm 715 */
return 
BGl_errorz00zz__errorz00(BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_93);} } } } } } } 

}



/* &2+ */
obj_t BGl_z622zb2zd0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5012, obj_t BgL_xz00_5013, obj_t BgL_yz00_5014)
{
{ /* Ieee/number.scm 714 */
return 
BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_xz00_5013, BgL_yz00_5014);} 

}



/* + */
BGL_EXPORTED_DEF obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t BgL_xz00_95)
{
{ /* Ieee/number.scm 720 */
if(
NULLP(BgL_xz00_95))
{ /* Ieee/number.scm 723 */
return 
BINT(0L);}  else 
{ /* Ieee/number.scm 725 */
 obj_t BgL_g1050z00_1807; obj_t BgL_g1051z00_1808;
{ /* Ieee/number.scm 725 */
 obj_t BgL_pairz00_3913;
if(
PAIRP(BgL_xz00_95))
{ /* Ieee/number.scm 725 */
BgL_pairz00_3913 = BgL_xz00_95; }  else 
{ 
 obj_t BgL_auxz00_9206;
BgL_auxz00_9206 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27779L), BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_xz00_95); 
FAILURE(BgL_auxz00_9206,BFALSE,BFALSE);} 
BgL_g1050z00_1807 = 
CAR(BgL_pairz00_3913); } 
{ /* Ieee/number.scm 726 */
 obj_t BgL_pairz00_3914;
if(
PAIRP(BgL_xz00_95))
{ /* Ieee/number.scm 726 */
BgL_pairz00_3914 = BgL_xz00_95; }  else 
{ 
 obj_t BgL_auxz00_9213;
BgL_auxz00_9213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(27795L), BGl_string3773z00zz__r4_numbers_6_5z00, BGl_string3754z00zz__r4_numbers_6_5z00, BgL_xz00_95); 
FAILURE(BgL_auxz00_9213,BFALSE,BFALSE);} 
BgL_g1051z00_1808 = 
CDR(BgL_pairz00_3914); } 
{ 
 obj_t BgL_sumz00_3928; obj_t BgL_xz00_3929;
BgL_sumz00_3928 = BgL_g1050z00_1807; 
BgL_xz00_3929 = BgL_g1051z00_1808; 
BgL_loopz00_3927:
if(
PAIRP(BgL_xz00_3929))
{ 
 obj_t BgL_xz00_9223; obj_t BgL_sumz00_9220;
BgL_sumz00_9220 = 
BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_sumz00_3928, 
CAR(BgL_xz00_3929)); 
BgL_xz00_9223 = 
CDR(BgL_xz00_3929); 
BgL_xz00_3929 = BgL_xz00_9223; 
BgL_sumz00_3928 = BgL_sumz00_9220; 
goto BgL_loopz00_3927;}  else 
{ /* Ieee/number.scm 727 */
return BgL_sumz00_3928;} } } } 

}



/* &+ */
obj_t BGl_z62zb2zd0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5015, obj_t BgL_xz00_5016)
{
{ /* Ieee/number.scm 720 */
return 
BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_xz00_5016);} 

}



/* 2* */
BGL_EXPORTED_DEF obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t BgL_xz00_96, obj_t BgL_yz00_97)
{
{ /* Ieee/number.scm 734 */
if(
INTEGERP(BgL_xz00_96))
{ /* Ieee/number.scm 735 */
if(
INTEGERP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 long BgL_auxz00_9232; long BgL_tmpz00_9230;
BgL_auxz00_9232 = 
(long)CINT(BgL_yz00_97); 
BgL_tmpz00_9230 = 
(long)CINT(BgL_xz00_96); 
return 
BGL_SAFE_MUL_FX(BgL_tmpz00_9230, BgL_auxz00_9232);}  else 
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_96))*
REAL_TO_DOUBLE(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 long BgL_arg2174z00_1823; obj_t BgL_arg2175z00_1824;
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9244;
BgL_tmpz00_9244 = 
(long)CINT(BgL_xz00_96); 
BgL_arg2174z00_1823 = 
(long)(BgL_tmpz00_9244); } 
BgL_arg2175z00_1824 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9248;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9249;
if(
ELONGP(BgL_arg2175z00_1824))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9249 = BgL_arg2175z00_1824
; }  else 
{ 
 obj_t BgL_auxz00_9252;
BgL_auxz00_9252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2175z00_1824); 
FAILURE(BgL_auxz00_9252,BFALSE,BFALSE);} 
BgL_tmpz00_9248 = 
BELONG_TO_LONG(BgL_tmpz00_9249); } 
return 
BGL_SAFE_MUL_ELONG(BgL_arg2174z00_1823, BgL_tmpz00_9248);} }  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2177z00_1826;
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9260;
BgL_tmpz00_9260 = 
(long)CINT(BgL_xz00_96); 
BgL_arg2177z00_1826 = 
LONG_TO_LLONG(BgL_tmpz00_9260); } 
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_tmpz00_9263;
BgL_tmpz00_9263 = 
BLLONG_TO_LLONG(BgL_yz00_97); 
return 
BGL_SAFE_MUL_LLONG(BgL_arg2177z00_1826, BgL_tmpz00_9263);} }  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_arg2180z00_1829;
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2181z00_1830;
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9268;
BgL_tmpz00_9268 = 
(long)CINT(BgL_xz00_96); 
BgL_arg2181z00_1830 = 
LONG_TO_LLONG(BgL_tmpz00_9268); } 
BgL_arg2180z00_1829 = 
(uint64_t)(BgL_arg2181z00_1830); } 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za72za7_3947;
BgL_za72za7_3947 = 
BGL_BINT64_TO_INT64(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9273;
BgL_tmpz00_9273 = 
(BgL_arg2180z00_1829*BgL_za72za7_3947); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9273);} } }  else 
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9278;
BgL_tmpz00_9278 = 
bgl_bignum_mul(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_96)), BgL_yz00_97); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9278);}  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_97);} } } } } } }  else 
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_xz00_96))
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_96)*
REAL_TO_DOUBLE(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
INTEGERP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_96)*
(double)(
(long)CINT(BgL_yz00_97))));}  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 double BgL_arg2190z00_1839;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2191z00_1840;
BgL_arg2191z00_1840 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9302;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9303;
if(
ELONGP(BgL_arg2191z00_1840))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9303 = BgL_arg2191z00_1840
; }  else 
{ 
 obj_t BgL_auxz00_9306;
BgL_auxz00_9306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2191z00_1840); 
FAILURE(BgL_auxz00_9306,BFALSE,BFALSE);} 
BgL_tmpz00_9302 = 
BELONG_TO_LONG(BgL_tmpz00_9303); } 
BgL_arg2190z00_1839 = 
(double)(BgL_tmpz00_9302); } } 
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_96)*BgL_arg2190z00_1839));}  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_96)*
(double)(
BLLONG_TO_LLONG(BgL_yz00_97))));}  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 double BgL_arg2196z00_1845;
{ /* Ieee/number.scm 735 */
 uint64_t BgL_xz00_3959;
BgL_xz00_3959 = 
BGL_BINT64_TO_INT64(BgL_yz00_97); 
BgL_arg2196z00_1845 = 
(double)(BgL_xz00_3959); } 
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_96)*BgL_arg2196z00_1845));}  else 
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_96)*
bgl_bignum_to_flonum(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_97);} } } } } } }  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_96))
{ /* Ieee/number.scm 735 */
if(
INTEGERP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2201z00_1850; long BgL_arg2202z00_1851;
BgL_arg2201z00_1850 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9341;
BgL_tmpz00_9341 = 
(long)CINT(BgL_yz00_97); 
BgL_arg2202z00_1851 = 
(long)(BgL_tmpz00_9341); } 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9344;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9345;
if(
ELONGP(BgL_arg2201z00_1850))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9345 = BgL_arg2201z00_1850
; }  else 
{ 
 obj_t BgL_auxz00_9348;
BgL_auxz00_9348 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2201z00_1850); 
FAILURE(BgL_auxz00_9348,BFALSE,BFALSE);} 
BgL_tmpz00_9344 = 
BELONG_TO_LONG(BgL_tmpz00_9345); } 
return 
BGL_SAFE_MUL_ELONG(BgL_tmpz00_9344, BgL_arg2202z00_1851);} }  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2204z00_1853; obj_t BgL_arg2205z00_1854;
BgL_arg2204z00_1853 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_96); 
BgL_arg2205z00_1854 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 long BgL_auxz00_9367; long BgL_tmpz00_9358;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9368;
if(
ELONGP(BgL_arg2205z00_1854))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9368 = BgL_arg2205z00_1854
; }  else 
{ 
 obj_t BgL_auxz00_9371;
BgL_auxz00_9371 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2205z00_1854); 
FAILURE(BgL_auxz00_9371,BFALSE,BFALSE);} 
BgL_auxz00_9367 = 
BELONG_TO_LONG(BgL_tmpz00_9368); } 
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9359;
if(
ELONGP(BgL_arg2204z00_1853))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9359 = BgL_arg2204z00_1853
; }  else 
{ 
 obj_t BgL_auxz00_9362;
BgL_auxz00_9362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2204z00_1853); 
FAILURE(BgL_auxz00_9362,BFALSE,BFALSE);} 
BgL_tmpz00_9358 = 
BELONG_TO_LONG(BgL_tmpz00_9359); } 
return 
BGL_SAFE_MUL_ELONG(BgL_tmpz00_9358, BgL_auxz00_9367);} }  else 
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 double BgL_arg2207z00_1856;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2208z00_1857;
BgL_arg2208z00_1857 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9380;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9381;
if(
ELONGP(BgL_arg2208z00_1857))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9381 = BgL_arg2208z00_1857
; }  else 
{ 
 obj_t BgL_auxz00_9384;
BgL_auxz00_9384 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2208z00_1857); 
FAILURE(BgL_auxz00_9384,BFALSE,BFALSE);} 
BgL_tmpz00_9380 = 
BELONG_TO_LONG(BgL_tmpz00_9381); } 
BgL_arg2207z00_1856 = 
(double)(BgL_tmpz00_9380); } } 
return 
DOUBLE_TO_REAL(
(BgL_arg2207z00_1856*
REAL_TO_DOUBLE(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2210z00_1859;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2212z00_1861;
BgL_arg2212z00_1861 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9396;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9397;
if(
ELONGP(BgL_arg2212z00_1861))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9397 = BgL_arg2212z00_1861
; }  else 
{ 
 obj_t BgL_auxz00_9400;
BgL_auxz00_9400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2212z00_1861); 
FAILURE(BgL_auxz00_9400,BFALSE,BFALSE);} 
BgL_tmpz00_9396 = 
BELONG_TO_LONG(BgL_tmpz00_9397); } 
BgL_arg2210z00_1859 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9396); } } 
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_tmpz00_9406;
BgL_tmpz00_9406 = 
BLLONG_TO_LLONG(BgL_yz00_97); 
return 
BGL_SAFE_MUL_LLONG(BgL_arg2210z00_1859, BgL_tmpz00_9406);} }  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_arg2214z00_1863;
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2215z00_1864;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2216z00_1865;
BgL_arg2216z00_1865 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9412;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9413;
if(
ELONGP(BgL_arg2216z00_1865))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9413 = BgL_arg2216z00_1865
; }  else 
{ 
 obj_t BgL_auxz00_9416;
BgL_auxz00_9416 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2216z00_1865); 
FAILURE(BgL_auxz00_9416,BFALSE,BFALSE);} 
BgL_tmpz00_9412 = 
BELONG_TO_LONG(BgL_tmpz00_9413); } 
BgL_arg2215z00_1864 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9412); } } 
BgL_arg2214z00_1863 = 
(uint64_t)(BgL_arg2215z00_1864); } 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za72za7_3970;
BgL_za72za7_3970 = 
BGL_BINT64_TO_INT64(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9424;
BgL_tmpz00_9424 = 
(BgL_arg2214z00_1863*BgL_za72za7_3970); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9424);} } }  else 
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2218z00_1867;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2219z00_1868;
BgL_arg2219z00_1868 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 long BgL_xz00_3971;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9430;
if(
ELONGP(BgL_arg2219z00_1868))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9430 = BgL_arg2219z00_1868
; }  else 
{ 
 obj_t BgL_auxz00_9433;
BgL_auxz00_9433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2219z00_1868); 
FAILURE(BgL_auxz00_9433,BFALSE,BFALSE);} 
BgL_xz00_3971 = 
BELONG_TO_LONG(BgL_tmpz00_9430); } 
BgL_arg2218z00_1867 = 
bgl_long_to_bignum(BgL_xz00_3971); } } 
return 
bgl_bignum_mul(BgL_arg2218z00_1867, BgL_yz00_97);}  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_97);} } } } } } }  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_xz00_96))
{ /* Ieee/number.scm 735 */
if(
INTEGERP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2223z00_1872;
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9445;
BgL_tmpz00_9445 = 
(long)CINT(BgL_yz00_97); 
BgL_arg2223z00_1872 = 
LONG_TO_LLONG(BgL_tmpz00_9445); } 
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_tmpz00_9448;
BgL_tmpz00_9448 = 
BLLONG_TO_LLONG(BgL_xz00_96); 
return 
BGL_SAFE_MUL_LLONG(BgL_tmpz00_9448, BgL_arg2223z00_1872);} }  else 
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_96))*
REAL_TO_DOUBLE(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_auxz00_9462; BGL_LONGLONG_T BgL_tmpz00_9460;
BgL_auxz00_9462 = 
BLLONG_TO_LLONG(BgL_yz00_97); 
BgL_tmpz00_9460 = 
BLLONG_TO_LLONG(BgL_xz00_96); 
return 
BGL_SAFE_MUL_LLONG(BgL_tmpz00_9460, BgL_auxz00_9462);}  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2231z00_1880;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2232z00_1881;
BgL_arg2232z00_1881 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9468;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9469;
if(
ELONGP(BgL_arg2232z00_1881))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9469 = BgL_arg2232z00_1881
; }  else 
{ 
 obj_t BgL_auxz00_9472;
BgL_auxz00_9472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2232z00_1881); 
FAILURE(BgL_auxz00_9472,BFALSE,BFALSE);} 
BgL_tmpz00_9468 = 
BELONG_TO_LONG(BgL_tmpz00_9469); } 
BgL_arg2231z00_1880 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9468); } } 
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_tmpz00_9478;
BgL_tmpz00_9478 = 
BLLONG_TO_LLONG(BgL_xz00_96); 
return 
BGL_SAFE_MUL_LLONG(BgL_tmpz00_9478, BgL_arg2231z00_1880);} }  else 
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
bgl_bignum_mul(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_96)), BgL_yz00_97);}  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_arg2237z00_1886;
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_tmpz00_9488;
BgL_tmpz00_9488 = 
BLLONG_TO_LLONG(BgL_xz00_96); 
BgL_arg2237z00_1886 = 
(uint64_t)(BgL_tmpz00_9488); } 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za72za7_3982;
BgL_za72za7_3982 = 
BGL_BINT64_TO_INT64(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9492;
BgL_tmpz00_9492 = 
(BgL_arg2237z00_1886*BgL_za72za7_3982); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9492);} } }  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_97);} } } } } } }  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_xz00_96))
{ /* Ieee/number.scm 735 */
if(
INTEGERP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_arg2240z00_1889;
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9500;
BgL_tmpz00_9500 = 
(long)CINT(BgL_yz00_97); 
BgL_arg2240z00_1889 = 
(uint64_t)(BgL_tmpz00_9500); } 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za71za7_3984;
BgL_za71za7_3984 = 
BGL_BINT64_TO_INT64(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9504;
BgL_tmpz00_9504 = 
(BgL_za71za7_3984*BgL_arg2240z00_1889); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9504);} } }  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za71za7_3986; uint64_t BgL_za72za7_3987;
BgL_za71za7_3986 = 
BGL_BINT64_TO_INT64(BgL_xz00_96); 
BgL_za72za7_3987 = 
BGL_BINT64_TO_INT64(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9511;
BgL_tmpz00_9511 = 
(BgL_za71za7_3986*BgL_za72za7_3987); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9511);} }  else 
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 double BgL_arg2243z00_1892;
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9516;
BgL_tmpz00_9516 = 
BGL_BINT64_TO_INT64(BgL_xz00_96); 
BgL_arg2243z00_1892 = 
(double)(BgL_tmpz00_9516); } 
return 
DOUBLE_TO_REAL(
(BgL_arg2243z00_1892*
REAL_TO_DOUBLE(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_arg2245z00_1894;
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_tmpz00_9524;
BgL_tmpz00_9524 = 
BLLONG_TO_LLONG(BgL_yz00_97); 
BgL_arg2245z00_1894 = 
(uint64_t)(BgL_tmpz00_9524); } 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za71za7_3991;
BgL_za71za7_3991 = 
BGL_BINT64_TO_INT64(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9528;
BgL_tmpz00_9528 = 
(BgL_za71za7_3991*BgL_arg2245z00_1894); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9528);} } }  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 uint64_t BgL_arg2248z00_1897;
{ /* Ieee/number.scm 735 */
 BGL_LONGLONG_T BgL_arg2249z00_1898;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2250z00_1899;
BgL_arg2250z00_1899 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 long BgL_tmpz00_9534;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9535;
if(
ELONGP(BgL_arg2250z00_1899))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9535 = BgL_arg2250z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_9538;
BgL_auxz00_9538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2250z00_1899); 
FAILURE(BgL_auxz00_9538,BFALSE,BFALSE);} 
BgL_tmpz00_9534 = 
BELONG_TO_LONG(BgL_tmpz00_9535); } 
BgL_arg2249z00_1898 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9534); } } 
BgL_arg2248z00_1897 = 
(uint64_t)(BgL_arg2249z00_1898); } 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_za71za7_3994;
BgL_za71za7_3994 = 
BGL_BINT64_TO_INT64(BgL_xz00_96); 
{ /* Ieee/number.scm 735 */
 uint64_t BgL_tmpz00_9546;
BgL_tmpz00_9546 = 
(BgL_za71za7_3994*BgL_arg2248z00_1897); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9546);} } }  else 
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
bgl_bignum_mul(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_96)), BgL_yz00_97);}  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_97);} } } } } } }  else 
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_xz00_96))
{ /* Ieee/number.scm 735 */
if(
BIGNUMP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9559;
BgL_tmpz00_9559 = 
bgl_bignum_mul(BgL_xz00_96, BgL_yz00_97); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9559);}  else 
{ /* Ieee/number.scm 735 */
if(
INTEGERP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9564;
BgL_tmpz00_9564 = 
bgl_bignum_mul(BgL_xz00_96, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_97))); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9564);}  else 
{ /* Ieee/number.scm 735 */
if(
REALP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_96)*
REAL_TO_DOUBLE(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2262z00_1911;
{ /* Ieee/number.scm 735 */
 obj_t BgL_arg2263z00_1912;
BgL_arg2263z00_1912 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_97); 
{ /* Ieee/number.scm 735 */
 long BgL_xz00_4006;
{ /* Ieee/number.scm 735 */
 obj_t BgL_tmpz00_9578;
if(
ELONGP(BgL_arg2263z00_1912))
{ /* Ieee/number.scm 735 */
BgL_tmpz00_9578 = BgL_arg2263z00_1912
; }  else 
{ 
 obj_t BgL_auxz00_9581;
BgL_auxz00_9581 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28114L), BGl_string3774z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2263z00_1912); 
FAILURE(BgL_auxz00_9581,BFALSE,BFALSE);} 
BgL_xz00_4006 = 
BELONG_TO_LONG(BgL_tmpz00_9578); } 
BgL_arg2262z00_1911 = 
bgl_long_to_bignum(BgL_xz00_4006); } } 
return 
bgl_bignum_mul(BgL_xz00_96, BgL_arg2262z00_1911);}  else 
{ /* Ieee/number.scm 735 */
if(
LLONGP(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
bgl_bignum_mul(BgL_xz00_96, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
if(
BGL_UINT64P(BgL_yz00_97))
{ /* Ieee/number.scm 735 */
return 
bgl_bignum_mul(BgL_xz00_96, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_97)));}  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_97);} } } } } } }  else 
{ /* Ieee/number.scm 735 */
return 
BGl_errorz00zz__errorz00(BGl_string3775z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_96);} } } } } } } 

}



/* &2* */
obj_t BGl_z622za2zc0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5017, obj_t BgL_xz00_5018, obj_t BgL_yz00_5019)
{
{ /* Ieee/number.scm 734 */
return 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_xz00_5018, BgL_yz00_5019);} 

}



/* * */
BGL_EXPORTED_DEF obj_t BGl_za2za2zz__r4_numbers_6_5z00(obj_t BgL_xz00_98)
{
{ /* Ieee/number.scm 740 */
{ 
 obj_t BgL_productz00_4027; obj_t BgL_xz00_4028;
BgL_productz00_4027 = 
BINT(1L); 
BgL_xz00_4028 = BgL_xz00_98; 
BgL_loopz00_4026:
if(
PAIRP(BgL_xz00_4028))
{ 
 obj_t BgL_xz00_9606; obj_t BgL_productz00_9603;
BgL_productz00_9603 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_productz00_4027, 
CAR(BgL_xz00_4028)); 
BgL_xz00_9606 = 
CDR(BgL_xz00_4028); 
BgL_xz00_4028 = BgL_xz00_9606; 
BgL_productz00_4027 = BgL_productz00_9603; 
goto BgL_loopz00_4026;}  else 
{ /* Ieee/number.scm 743 */
return BgL_productz00_4027;} } } 

}



/* &* */
obj_t BGl_z62za2zc0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5020, obj_t BgL_xz00_5021)
{
{ /* Ieee/number.scm 740 */
return 
BGl_za2za2zz__r4_numbers_6_5z00(BgL_xz00_5021);} 

}



/* 2- */
BGL_EXPORTED_DEF obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t BgL_xz00_99, obj_t BgL_yz00_100)
{
{ /* Ieee/number.scm 750 */
if(
INTEGERP(BgL_xz00_99))
{ /* Ieee/number.scm 751 */
if(
INTEGERP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 long BgL_auxz00_9616; long BgL_tmpz00_9614;
BgL_auxz00_9616 = 
(long)CINT(BgL_yz00_100); 
BgL_tmpz00_9614 = 
(long)CINT(BgL_xz00_99); 
return 
BGL_SAFE_MINUS_FX(BgL_tmpz00_9614, BgL_auxz00_9616);}  else 
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_99))-
REAL_TO_DOUBLE(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 long BgL_arg2279z00_1932; obj_t BgL_arg2280z00_1933;
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9628;
BgL_tmpz00_9628 = 
(long)CINT(BgL_xz00_99); 
BgL_arg2279z00_1932 = 
(long)(BgL_tmpz00_9628); } 
BgL_arg2280z00_1933 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9632;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9633;
if(
ELONGP(BgL_arg2280z00_1933))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9633 = BgL_arg2280z00_1933
; }  else 
{ 
 obj_t BgL_auxz00_9636;
BgL_auxz00_9636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2280z00_1933); 
FAILURE(BgL_auxz00_9636,BFALSE,BFALSE);} 
BgL_tmpz00_9632 = 
BELONG_TO_LONG(BgL_tmpz00_9633); } 
return 
BGL_SAFE_MINUS_ELONG(BgL_arg2279z00_1932, BgL_tmpz00_9632);} }  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2282z00_1935;
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9644;
BgL_tmpz00_9644 = 
(long)CINT(BgL_xz00_99); 
BgL_arg2282z00_1935 = 
LONG_TO_LLONG(BgL_tmpz00_9644); } 
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_tmpz00_9647;
BgL_tmpz00_9647 = 
BLLONG_TO_LLONG(BgL_yz00_100); 
return 
BGL_SAFE_MINUS_LLONG(BgL_arg2282z00_1935, BgL_tmpz00_9647);} }  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_arg2286z00_1938;
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2287z00_1939;
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9652;
BgL_tmpz00_9652 = 
(long)CINT(BgL_xz00_99); 
BgL_arg2287z00_1939 = 
LONG_TO_LLONG(BgL_tmpz00_9652); } 
BgL_arg2286z00_1938 = 
(uint64_t)(BgL_arg2287z00_1939); } 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za72za7_4046;
BgL_za72za7_4046 = 
BGL_BINT64_TO_INT64(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9657;
BgL_tmpz00_9657 = 
(BgL_arg2286z00_1938-BgL_za72za7_4046); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9657);} } }  else 
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9662;
BgL_tmpz00_9662 = 
bgl_bignum_sub(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_99)), BgL_yz00_100); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9662);}  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_100);} } } } } } }  else 
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_xz00_99))
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_99)-
REAL_TO_DOUBLE(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
INTEGERP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_99)-
(double)(
(long)CINT(BgL_yz00_100))));}  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 double BgL_arg2296z00_1948;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2297z00_1949;
BgL_arg2297z00_1949 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9686;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9687;
if(
ELONGP(BgL_arg2297z00_1949))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9687 = BgL_arg2297z00_1949
; }  else 
{ 
 obj_t BgL_auxz00_9690;
BgL_auxz00_9690 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2297z00_1949); 
FAILURE(BgL_auxz00_9690,BFALSE,BFALSE);} 
BgL_tmpz00_9686 = 
BELONG_TO_LONG(BgL_tmpz00_9687); } 
BgL_arg2296z00_1948 = 
(double)(BgL_tmpz00_9686); } } 
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_99)-BgL_arg2296z00_1948));}  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_99)-
(double)(
BLLONG_TO_LLONG(BgL_yz00_100))));}  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 double BgL_arg2304z00_1954;
{ /* Ieee/number.scm 751 */
 uint64_t BgL_xz00_4058;
BgL_xz00_4058 = 
BGL_BINT64_TO_INT64(BgL_yz00_100); 
BgL_arg2304z00_1954 = 
(double)(BgL_xz00_4058); } 
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_99)-BgL_arg2304z00_1954));}  else 
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_99)-
bgl_bignum_to_flonum(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_100);} } } } } } }  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_xz00_99))
{ /* Ieee/number.scm 751 */
if(
INTEGERP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2309z00_1959; long BgL_arg2310z00_1960;
BgL_arg2309z00_1959 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9725;
BgL_tmpz00_9725 = 
(long)CINT(BgL_yz00_100); 
BgL_arg2310z00_1960 = 
(long)(BgL_tmpz00_9725); } 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9728;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9729;
if(
ELONGP(BgL_arg2309z00_1959))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9729 = BgL_arg2309z00_1959
; }  else 
{ 
 obj_t BgL_auxz00_9732;
BgL_auxz00_9732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2309z00_1959); 
FAILURE(BgL_auxz00_9732,BFALSE,BFALSE);} 
BgL_tmpz00_9728 = 
BELONG_TO_LONG(BgL_tmpz00_9729); } 
return 
BGL_SAFE_MINUS_ELONG(BgL_tmpz00_9728, BgL_arg2310z00_1960);} }  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2312z00_1962; obj_t BgL_arg2313z00_1963;
BgL_arg2312z00_1962 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_99); 
BgL_arg2313z00_1963 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 long BgL_auxz00_9751; long BgL_tmpz00_9742;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9752;
if(
ELONGP(BgL_arg2313z00_1963))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9752 = BgL_arg2313z00_1963
; }  else 
{ 
 obj_t BgL_auxz00_9755;
BgL_auxz00_9755 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2313z00_1963); 
FAILURE(BgL_auxz00_9755,BFALSE,BFALSE);} 
BgL_auxz00_9751 = 
BELONG_TO_LONG(BgL_tmpz00_9752); } 
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9743;
if(
ELONGP(BgL_arg2312z00_1962))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9743 = BgL_arg2312z00_1962
; }  else 
{ 
 obj_t BgL_auxz00_9746;
BgL_auxz00_9746 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2312z00_1962); 
FAILURE(BgL_auxz00_9746,BFALSE,BFALSE);} 
BgL_tmpz00_9742 = 
BELONG_TO_LONG(BgL_tmpz00_9743); } 
return 
BGL_SAFE_MINUS_ELONG(BgL_tmpz00_9742, BgL_auxz00_9751);} }  else 
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 double BgL_arg2315z00_1965;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2316z00_1966;
BgL_arg2316z00_1966 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9764;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9765;
if(
ELONGP(BgL_arg2316z00_1966))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9765 = BgL_arg2316z00_1966
; }  else 
{ 
 obj_t BgL_auxz00_9768;
BgL_auxz00_9768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2316z00_1966); 
FAILURE(BgL_auxz00_9768,BFALSE,BFALSE);} 
BgL_tmpz00_9764 = 
BELONG_TO_LONG(BgL_tmpz00_9765); } 
BgL_arg2315z00_1965 = 
(double)(BgL_tmpz00_9764); } } 
return 
DOUBLE_TO_REAL(
(BgL_arg2315z00_1965-
REAL_TO_DOUBLE(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2318z00_1968;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2320z00_1970;
BgL_arg2320z00_1970 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9780;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9781;
if(
ELONGP(BgL_arg2320z00_1970))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9781 = BgL_arg2320z00_1970
; }  else 
{ 
 obj_t BgL_auxz00_9784;
BgL_auxz00_9784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2320z00_1970); 
FAILURE(BgL_auxz00_9784,BFALSE,BFALSE);} 
BgL_tmpz00_9780 = 
BELONG_TO_LONG(BgL_tmpz00_9781); } 
BgL_arg2318z00_1968 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9780); } } 
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_tmpz00_9790;
BgL_tmpz00_9790 = 
BLLONG_TO_LLONG(BgL_yz00_100); 
return 
BGL_SAFE_MINUS_LLONG(BgL_arg2318z00_1968, BgL_tmpz00_9790);} }  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_arg2323z00_1972;
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2324z00_1973;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2325z00_1974;
BgL_arg2325z00_1974 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9796;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9797;
if(
ELONGP(BgL_arg2325z00_1974))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9797 = BgL_arg2325z00_1974
; }  else 
{ 
 obj_t BgL_auxz00_9800;
BgL_auxz00_9800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2325z00_1974); 
FAILURE(BgL_auxz00_9800,BFALSE,BFALSE);} 
BgL_tmpz00_9796 = 
BELONG_TO_LONG(BgL_tmpz00_9797); } 
BgL_arg2324z00_1973 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9796); } } 
BgL_arg2323z00_1972 = 
(uint64_t)(BgL_arg2324z00_1973); } 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za72za7_4069;
BgL_za72za7_4069 = 
BGL_BINT64_TO_INT64(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9808;
BgL_tmpz00_9808 = 
(BgL_arg2323z00_1972-BgL_za72za7_4069); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9808);} } }  else 
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2327z00_1976;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2328z00_1977;
BgL_arg2328z00_1977 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 long BgL_xz00_4070;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9814;
if(
ELONGP(BgL_arg2328z00_1977))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9814 = BgL_arg2328z00_1977
; }  else 
{ 
 obj_t BgL_auxz00_9817;
BgL_auxz00_9817 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2328z00_1977); 
FAILURE(BgL_auxz00_9817,BFALSE,BFALSE);} 
BgL_xz00_4070 = 
BELONG_TO_LONG(BgL_tmpz00_9814); } 
BgL_arg2327z00_1976 = 
bgl_long_to_bignum(BgL_xz00_4070); } } 
return 
bgl_bignum_sub(BgL_arg2327z00_1976, BgL_yz00_100);}  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_100);} } } } } } }  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_xz00_99))
{ /* Ieee/number.scm 751 */
if(
INTEGERP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2333z00_1981;
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9829;
BgL_tmpz00_9829 = 
(long)CINT(BgL_yz00_100); 
BgL_arg2333z00_1981 = 
LONG_TO_LLONG(BgL_tmpz00_9829); } 
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_tmpz00_9832;
BgL_tmpz00_9832 = 
BLLONG_TO_LLONG(BgL_xz00_99); 
return 
BGL_SAFE_MINUS_LLONG(BgL_tmpz00_9832, BgL_arg2333z00_1981);} }  else 
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_99))-
REAL_TO_DOUBLE(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_auxz00_9846; BGL_LONGLONG_T BgL_tmpz00_9844;
BgL_auxz00_9846 = 
BLLONG_TO_LLONG(BgL_yz00_100); 
BgL_tmpz00_9844 = 
BLLONG_TO_LLONG(BgL_xz00_99); 
return 
BGL_SAFE_MINUS_LLONG(BgL_tmpz00_9844, BgL_auxz00_9846);}  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2341z00_1989;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2342z00_1990;
BgL_arg2342z00_1990 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9852;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9853;
if(
ELONGP(BgL_arg2342z00_1990))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9853 = BgL_arg2342z00_1990
; }  else 
{ 
 obj_t BgL_auxz00_9856;
BgL_auxz00_9856 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2342z00_1990); 
FAILURE(BgL_auxz00_9856,BFALSE,BFALSE);} 
BgL_tmpz00_9852 = 
BELONG_TO_LONG(BgL_tmpz00_9853); } 
BgL_arg2341z00_1989 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9852); } } 
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_tmpz00_9862;
BgL_tmpz00_9862 = 
BLLONG_TO_LLONG(BgL_xz00_99); 
return 
BGL_SAFE_MINUS_LLONG(BgL_tmpz00_9862, BgL_arg2341z00_1989);} }  else 
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
bgl_bignum_sub(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_99)), BgL_yz00_100);}  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_arg2348z00_1995;
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_tmpz00_9872;
BgL_tmpz00_9872 = 
BLLONG_TO_LLONG(BgL_xz00_99); 
BgL_arg2348z00_1995 = 
(uint64_t)(BgL_tmpz00_9872); } 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za72za7_4081;
BgL_za72za7_4081 = 
BGL_BINT64_TO_INT64(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9876;
BgL_tmpz00_9876 = 
(BgL_arg2348z00_1995-BgL_za72za7_4081); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9876);} } }  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_100);} } } } } } }  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_xz00_99))
{ /* Ieee/number.scm 751 */
if(
INTEGERP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_arg2351z00_1998;
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9884;
BgL_tmpz00_9884 = 
(long)CINT(BgL_yz00_100); 
BgL_arg2351z00_1998 = 
(uint64_t)(BgL_tmpz00_9884); } 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za71za7_4083;
BgL_za71za7_4083 = 
BGL_BINT64_TO_INT64(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9888;
BgL_tmpz00_9888 = 
(BgL_za71za7_4083-BgL_arg2351z00_1998); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9888);} } }  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za71za7_4085; uint64_t BgL_za72za7_4086;
BgL_za71za7_4085 = 
BGL_BINT64_TO_INT64(BgL_xz00_99); 
BgL_za72za7_4086 = 
BGL_BINT64_TO_INT64(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9895;
BgL_tmpz00_9895 = 
(BgL_za71za7_4085-BgL_za72za7_4086); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9895);} }  else 
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 double BgL_arg2354z00_2001;
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9900;
BgL_tmpz00_9900 = 
BGL_BINT64_TO_INT64(BgL_xz00_99); 
BgL_arg2354z00_2001 = 
(double)(BgL_tmpz00_9900); } 
return 
DOUBLE_TO_REAL(
(BgL_arg2354z00_2001-
REAL_TO_DOUBLE(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_arg2356z00_2003;
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_tmpz00_9908;
BgL_tmpz00_9908 = 
BLLONG_TO_LLONG(BgL_yz00_100); 
BgL_arg2356z00_2003 = 
(uint64_t)(BgL_tmpz00_9908); } 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za71za7_4090;
BgL_za71za7_4090 = 
BGL_BINT64_TO_INT64(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9912;
BgL_tmpz00_9912 = 
(BgL_za71za7_4090-BgL_arg2356z00_2003); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9912);} } }  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 uint64_t BgL_arg2361z00_2006;
{ /* Ieee/number.scm 751 */
 BGL_LONGLONG_T BgL_arg2363z00_2007;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2364z00_2008;
BgL_arg2364z00_2008 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 long BgL_tmpz00_9918;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9919;
if(
ELONGP(BgL_arg2364z00_2008))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9919 = BgL_arg2364z00_2008
; }  else 
{ 
 obj_t BgL_auxz00_9922;
BgL_auxz00_9922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2364z00_2008); 
FAILURE(BgL_auxz00_9922,BFALSE,BFALSE);} 
BgL_tmpz00_9918 = 
BELONG_TO_LONG(BgL_tmpz00_9919); } 
BgL_arg2363z00_2007 = 
(BGL_LONGLONG_T)(BgL_tmpz00_9918); } } 
BgL_arg2361z00_2006 = 
(uint64_t)(BgL_arg2363z00_2007); } 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_za71za7_4093;
BgL_za71za7_4093 = 
BGL_BINT64_TO_INT64(BgL_xz00_99); 
{ /* Ieee/number.scm 751 */
 uint64_t BgL_tmpz00_9930;
BgL_tmpz00_9930 = 
(BgL_za71za7_4093-BgL_arg2361z00_2006); 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_9930);} } }  else 
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
bgl_bignum_sub(
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_xz00_99)), BgL_yz00_100);}  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3752z00zz__r4_numbers_6_5z00, BgL_yz00_100);} } } } } } }  else 
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_xz00_99))
{ /* Ieee/number.scm 751 */
if(
BIGNUMP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9943;
BgL_tmpz00_9943 = 
bgl_bignum_sub(BgL_xz00_99, BgL_yz00_100); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9943);}  else 
{ /* Ieee/number.scm 751 */
if(
INTEGERP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9948;
BgL_tmpz00_9948 = 
bgl_bignum_sub(BgL_xz00_99, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_100))); 
return 
BGL_SAFE_BX_TO_FX(BgL_tmpz00_9948);}  else 
{ /* Ieee/number.scm 751 */
if(
REALP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_99)-
REAL_TO_DOUBLE(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
BGl_z42subelongzf3zb1zz__r4_numbers_6_5z00(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2377z00_2020;
{ /* Ieee/number.scm 751 */
 obj_t BgL_arg2378z00_2021;
BgL_arg2378z00_2021 = 
BGl_z42subelongzd2ze3elongz73zz__r4_numbers_6_5z00(BgL_yz00_100); 
{ /* Ieee/number.scm 751 */
 long BgL_xz00_4105;
{ /* Ieee/number.scm 751 */
 obj_t BgL_tmpz00_9962;
if(
ELONGP(BgL_arg2378z00_2021))
{ /* Ieee/number.scm 751 */
BgL_tmpz00_9962 = BgL_arg2378z00_2021
; }  else 
{ 
 obj_t BgL_auxz00_9965;
BgL_auxz00_9965 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(28723L), BGl_string3776z00zz__r4_numbers_6_5z00, BGl_string3719z00zz__r4_numbers_6_5z00, BgL_arg2378z00_2021); 
FAILURE(BgL_auxz00_9965,BFALSE,BFALSE);} 
BgL_xz00_4105 = 
BELONG_TO_LONG(BgL_tmpz00_9962); } 
BgL_arg2377z00_2020 = 
bgl_long_to_bignum(BgL_xz00_4105); } } 
return 
bgl_bignum_sub(BgL_xz00_99, BgL_arg2377z00_2020);}  else 
{ /* Ieee/number.scm 751 */
if(
LLONGP(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
bgl_bignum_sub(BgL_xz00_99, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
if(
BGL_UINT64P(BgL_yz00_100))
{ /* Ieee/number.scm 751 */
return 
bgl_bignum_sub(BgL_xz00_99, 
bgl_uint64_to_bignum(
BGL_BINT64_TO_INT64(BgL_yz00_100)));}  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_100);} } } } } } }  else 
{ /* Ieee/number.scm 751 */
return 
BGl_errorz00zz__errorz00(BGl_string3777z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_99);} } } } } } } 

}



/* &2- */
obj_t BGl_z622zd2zb0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5022, obj_t BgL_xz00_5023, obj_t BgL_yz00_5024)
{
{ /* Ieee/number.scm 750 */
return 
BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_xz00_5023, BgL_yz00_5024);} 

}



/* - */
BGL_EXPORTED_DEF obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t BgL_xz00_101, obj_t BgL_yz00_102)
{
{ /* Ieee/number.scm 756 */
if(
PAIRP(BgL_yz00_102))
{ /* Ieee/number.scm 758 */
 obj_t BgL_g1052z00_2028; obj_t BgL_g1053z00_2029;
BgL_g1052z00_2028 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_xz00_101, 
CAR(BgL_yz00_102)); 
BgL_g1053z00_2029 = 
CDR(BgL_yz00_102); 
{ 
 obj_t BgL_resultz00_4128; obj_t BgL_argsz00_4129;
BgL_resultz00_4128 = BgL_g1052z00_2028; 
BgL_argsz00_4129 = BgL_g1053z00_2029; 
BgL_loopz00_4127:
if(
PAIRP(BgL_argsz00_4129))
{ 
 obj_t BgL_argsz00_9995; obj_t BgL_resultz00_9992;
BgL_resultz00_9992 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_resultz00_4128, 
CAR(BgL_argsz00_4129)); 
BgL_argsz00_9995 = 
CDR(BgL_argsz00_4129); 
BgL_argsz00_4129 = BgL_argsz00_9995; 
BgL_resultz00_4128 = BgL_resultz00_9992; 
goto BgL_loopz00_4127;}  else 
{ /* Ieee/number.scm 760 */
return BgL_resultz00_4128;} } }  else 
{ /* Ieee/number.scm 757 */
BGL_TAIL return 
BGl_2zd2zd2zz__r4_numbers_6_5z00(
BINT(0L), BgL_xz00_101);} } 

}



/* &- */
obj_t BGl_z62zd2zb0zz__r4_numbers_6_5z00(obj_t BgL_envz00_5025, obj_t BgL_xz00_5026, obj_t BgL_yz00_5027)
{
{ /* Ieee/number.scm 756 */
return 
BGl_zd2zd2zz__r4_numbers_6_5z00(BgL_xz00_5026, BgL_yz00_5027);} 

}



/* 2/ */
BGL_EXPORTED_DEF obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t BgL_xz00_103, obj_t BgL_yz00_104)
{
{ /* Ieee/number.scm 768 */
if(
INTEGERP(BgL_xz00_103))
{ /* Ieee/number.scm 770 */
if(
INTEGERP(BgL_yz00_104))
{ /* Ieee/number.scm 773 */
 bool_t BgL_test4728z00_10004;
{ /* Ieee/number.scm 773 */
 long BgL_arg2397z00_2046;
{ /* Ieee/number.scm 773 */
 long BgL_n1z00_4140; long BgL_n2z00_4141;
BgL_n1z00_4140 = 
(long)CINT(BgL_xz00_103); 
BgL_n2z00_4141 = 
(long)CINT(BgL_yz00_104); 
{ /* Ieee/number.scm 773 */
 bool_t BgL_test4729z00_10007;
{ /* Ieee/number.scm 773 */
 long BgL_arg3088z00_4143;
BgL_arg3088z00_4143 = 
(((BgL_n1z00_4140) | (BgL_n2z00_4141)) & -2147483648); 
BgL_test4729z00_10007 = 
(BgL_arg3088z00_4143==0L); } 
if(BgL_test4729z00_10007)
{ /* Ieee/number.scm 773 */
 int32_t BgL_arg3085z00_4144;
{ /* Ieee/number.scm 773 */
 int32_t BgL_arg3086z00_4145; int32_t BgL_arg3087z00_4146;
BgL_arg3086z00_4145 = 
(int32_t)(BgL_n1z00_4140); 
BgL_arg3087z00_4146 = 
(int32_t)(BgL_n2z00_4141); 
BgL_arg3085z00_4144 = 
(BgL_arg3086z00_4145%BgL_arg3087z00_4146); } 
{ /* Ieee/number.scm 773 */
 long BgL_arg3247z00_4151;
BgL_arg3247z00_4151 = 
(long)(BgL_arg3085z00_4144); 
BgL_arg2397z00_2046 = 
(long)(BgL_arg3247z00_4151); } }  else 
{ /* Ieee/number.scm 773 */
BgL_arg2397z00_2046 = 
(BgL_n1z00_4140%BgL_n2z00_4141); } } } 
BgL_test4728z00_10004 = 
(BgL_arg2397z00_2046==0L); } 
if(BgL_test4728z00_10004)
{ /* Ieee/number.scm 773 */
return 
BINT(
(
(long)CINT(BgL_xz00_103)/
(long)CINT(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 773 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_103))/
(double)(
(long)CINT(BgL_yz00_104))));} }  else 
{ /* Ieee/number.scm 772 */
if(
REALP(BgL_yz00_104))
{ /* Ieee/number.scm 776 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_103))/
REAL_TO_DOUBLE(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 776 */
if(
ELONGP(BgL_yz00_104))
{ /* Ieee/number.scm 779 */
 long BgL_exz00_2050;
{ /* Ieee/number.scm 779 */
 long BgL_tmpz00_10036;
BgL_tmpz00_10036 = 
(long)CINT(BgL_xz00_103); 
BgL_exz00_2050 = 
(long)(BgL_tmpz00_10036); } 
{ /* Ieee/number.scm 780 */
 bool_t BgL_test4732z00_10039;
{ /* Ieee/number.scm 780 */
 long BgL_arg2405z00_2055;
{ /* Ieee/number.scm 780 */
 long BgL_n2z00_4162;
BgL_n2z00_4162 = 
BELONG_TO_LONG(BgL_yz00_104); 
BgL_arg2405z00_2055 = 
(BgL_exz00_2050%BgL_n2z00_4162); } 
BgL_test4732z00_10039 = 
(BgL_arg2405z00_2055==((long)0)); } 
if(BgL_test4732z00_10039)
{ /* Ieee/number.scm 781 */
 long BgL_za72za7_4166;
BgL_za72za7_4166 = 
BELONG_TO_LONG(BgL_yz00_104); 
{ /* Ieee/number.scm 781 */
 long BgL_tmpz00_10044;
BgL_tmpz00_10044 = 
(BgL_exz00_2050/BgL_za72za7_4166); 
return 
make_belong(BgL_tmpz00_10044);} }  else 
{ /* Ieee/number.scm 780 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_103))/
(double)(
BELONG_TO_LONG(BgL_yz00_104))));} } }  else 
{ /* Ieee/number.scm 778 */
if(
LLONGP(BgL_yz00_104))
{ /* Ieee/number.scm 784 */
 BGL_LONGLONG_T BgL_lxz00_2057;
{ /* Ieee/number.scm 784 */
 long BgL_tmpz00_10055;
BgL_tmpz00_10055 = 
(long)CINT(BgL_xz00_103); 
BgL_lxz00_2057 = 
LONG_TO_LLONG(BgL_tmpz00_10055); } 
{ /* Ieee/number.scm 785 */
 bool_t BgL_test4734z00_10058;
{ /* Ieee/number.scm 785 */
 BGL_LONGLONG_T BgL_arg2412z00_2062;
{ /* Ieee/number.scm 785 */
 BGL_LONGLONG_T BgL_tmpz00_10059;
BgL_tmpz00_10059 = 
BLLONG_TO_LLONG(BgL_yz00_104); 
BgL_arg2412z00_2062 = 
(BgL_lxz00_2057%BgL_tmpz00_10059); } 
BgL_test4734z00_10058 = 
(BgL_arg2412z00_2062==((BGL_LONGLONG_T)0)); } 
if(BgL_test4734z00_10058)
{ /* Ieee/number.scm 786 */
 BGL_LONGLONG_T BgL_za72za7_4175;
BgL_za72za7_4175 = 
BLLONG_TO_LLONG(BgL_yz00_104); 
return 
make_bllong(
(BgL_lxz00_2057/BgL_za72za7_4175));}  else 
{ /* Ieee/number.scm 785 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_103))/
(double)(
BLLONG_TO_LLONG(BgL_yz00_104))));} } }  else 
{ /* Ieee/number.scm 783 */
if(
BIGNUMP(BgL_yz00_104))
{ /* Ieee/number.scm 789 */
 obj_t BgL_qz00_2064;
BgL_qz00_2064 = 
bgl_bignum_div(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_103)), BgL_yz00_104); 
{ /* Ieee/number.scm 790 */
 obj_t BgL_rz00_2065;
{ /* Ieee/number.scm 791 */
 obj_t BgL_tmpz00_4179;
{ /* Ieee/number.scm 791 */
 int BgL_tmpz00_10077;
BgL_tmpz00_10077 = 
(int)(1L); 
BgL_tmpz00_4179 = 
BGL_MVALUES_VAL(BgL_tmpz00_10077); } 
{ /* Ieee/number.scm 791 */
 int BgL_tmpz00_10080;
BgL_tmpz00_10080 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10080, BUNSPEC); } 
BgL_rz00_2065 = BgL_tmpz00_4179; } 
{ /* Ieee/number.scm 791 */
 bool_t BgL_test4736z00_10083;
{ /* Ieee/number.scm 791 */
 obj_t BgL_nz00_4180;
if(
BIGNUMP(BgL_rz00_2065))
{ /* Ieee/number.scm 791 */
BgL_nz00_4180 = BgL_rz00_2065; }  else 
{ 
 obj_t BgL_auxz00_10086;
BgL_auxz00_10086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(30042L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2065); 
FAILURE(BgL_auxz00_10086,BFALSE,BFALSE);} 
BgL_test4736z00_10083 = 
BXZERO(BgL_nz00_4180); } 
if(BgL_test4736z00_10083)
{ /* Ieee/number.scm 791 */
return BgL_qz00_2064;}  else 
{ /* Ieee/number.scm 791 */
return 
DOUBLE_TO_REAL(
(
(double)(
(long)CINT(BgL_xz00_103))/
bgl_bignum_to_flonum(BgL_yz00_104)));} } } }  else 
{ /* Ieee/number.scm 788 */
return 
BGl_errorz00zz__errorz00(BGl_string3779z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_104);} } } } } }  else 
{ /* Ieee/number.scm 770 */
if(
REALP(BgL_xz00_103))
{ /* Ieee/number.scm 796 */
if(
REALP(BgL_yz00_104))
{ /* Ieee/number.scm 798 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_103)/
REAL_TO_DOUBLE(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 798 */
if(
INTEGERP(BgL_yz00_104))
{ /* Ieee/number.scm 800 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_103)/
(double)(
(long)CINT(BgL_yz00_104))));}  else 
{ /* Ieee/number.scm 800 */
if(
ELONGP(BgL_yz00_104))
{ /* Ieee/number.scm 802 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_103)/
(double)(
BELONG_TO_LONG(BgL_yz00_104))));}  else 
{ /* Ieee/number.scm 802 */
if(
LLONGP(BgL_yz00_104))
{ /* Ieee/number.scm 804 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_103)/
(double)(
BLLONG_TO_LLONG(BgL_yz00_104))));}  else 
{ /* Ieee/number.scm 804 */
if(
BIGNUMP(BgL_yz00_104))
{ /* Ieee/number.scm 806 */
return 
DOUBLE_TO_REAL(
(
REAL_TO_DOUBLE(BgL_xz00_103)/
bgl_bignum_to_flonum(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 806 */
return 
BGl_errorz00zz__errorz00(BGl_string3779z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_104);} } } } } }  else 
{ /* Ieee/number.scm 796 */
if(
ELONGP(BgL_xz00_103))
{ /* Ieee/number.scm 810 */
if(
INTEGERP(BgL_yz00_104))
{ /* Ieee/number.scm 813 */
 long BgL_eyz00_2082;
{ /* Ieee/number.scm 813 */
 long BgL_tmpz00_10137;
BgL_tmpz00_10137 = 
(long)CINT(BgL_yz00_104); 
BgL_eyz00_2082 = 
(long)(BgL_tmpz00_10137); } 
{ /* Ieee/number.scm 814 */
 bool_t BgL_test4746z00_10140;
{ /* Ieee/number.scm 814 */
 long BgL_arg2439z00_2087;
{ /* Ieee/number.scm 814 */
 long BgL_n1z00_4196;
BgL_n1z00_4196 = 
BELONG_TO_LONG(BgL_xz00_103); 
BgL_arg2439z00_2087 = 
(BgL_n1z00_4196%BgL_eyz00_2082); } 
BgL_test4746z00_10140 = 
(BgL_arg2439z00_2087==((long)0)); } 
if(BgL_test4746z00_10140)
{ /* Ieee/number.scm 815 */
 long BgL_za71za7_4200;
BgL_za71za7_4200 = 
BELONG_TO_LONG(BgL_xz00_103); 
{ /* Ieee/number.scm 815 */
 long BgL_tmpz00_10145;
BgL_tmpz00_10145 = 
(BgL_za71za7_4200/BgL_eyz00_2082); 
return 
make_belong(BgL_tmpz00_10145);} }  else 
{ /* Ieee/number.scm 814 */
return 
DOUBLE_TO_REAL(
(
(double)(
BELONG_TO_LONG(BgL_xz00_103))/
(double)(
(long)CINT(BgL_yz00_104))));} } }  else 
{ /* Ieee/number.scm 812 */
if(
REALP(BgL_yz00_104))
{ /* Ieee/number.scm 817 */
return 
DOUBLE_TO_REAL(
(
(double)(
BELONG_TO_LONG(BgL_xz00_103))/
REAL_TO_DOUBLE(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 817 */
if(
ELONGP(BgL_yz00_104))
{ /* Ieee/number.scm 820 */
 bool_t BgL_test4749z00_10163;
{ /* Ieee/number.scm 820 */
 long BgL_arg2449z00_2095;
{ /* Ieee/number.scm 820 */
 long BgL_n1z00_4206; long BgL_n2z00_4207;
BgL_n1z00_4206 = 
BELONG_TO_LONG(BgL_xz00_103); 
BgL_n2z00_4207 = 
BELONG_TO_LONG(BgL_yz00_104); 
BgL_arg2449z00_2095 = 
(BgL_n1z00_4206%BgL_n2z00_4207); } 
BgL_test4749z00_10163 = 
(BgL_arg2449z00_2095==((long)0)); } 
if(BgL_test4749z00_10163)
{ /* Ieee/number.scm 821 */
 long BgL_za71za7_4210; long BgL_za72za7_4211;
BgL_za71za7_4210 = 
BELONG_TO_LONG(BgL_xz00_103); 
BgL_za72za7_4211 = 
BELONG_TO_LONG(BgL_yz00_104); 
{ /* Ieee/number.scm 821 */
 long BgL_tmpz00_10170;
BgL_tmpz00_10170 = 
(BgL_za71za7_4210/BgL_za72za7_4211); 
return 
make_belong(BgL_tmpz00_10170);} }  else 
{ /* Ieee/number.scm 820 */
return 
DOUBLE_TO_REAL(
(
(double)(
BELONG_TO_LONG(BgL_xz00_103))/
(double)(
BELONG_TO_LONG(BgL_yz00_104))));} }  else 
{ /* Ieee/number.scm 819 */
if(
LLONGP(BgL_yz00_104))
{ /* Ieee/number.scm 824 */
 double BgL_fxz00_2097;
BgL_fxz00_2097 = 
(double)(
BELONG_TO_LONG(BgL_xz00_103)); 
{ /* Ieee/number.scm 824 */
 BGL_LONGLONG_T BgL_lxz00_2098;
BgL_lxz00_2098 = 
(BGL_LONGLONG_T)(BgL_fxz00_2097); 
{ /* Ieee/number.scm 825 */

{ /* Ieee/number.scm 826 */
 bool_t BgL_test4751z00_10184;
{ /* Ieee/number.scm 826 */
 BGL_LONGLONG_T BgL_arg2455z00_2102;
{ /* Ieee/number.scm 826 */
 BGL_LONGLONG_T BgL_tmpz00_10185;
BgL_tmpz00_10185 = 
BLLONG_TO_LLONG(BgL_yz00_104); 
BgL_arg2455z00_2102 = 
(BgL_lxz00_2098%BgL_tmpz00_10185); } 
BgL_test4751z00_10184 = 
(BgL_arg2455z00_2102==((BGL_LONGLONG_T)0)); } 
if(BgL_test4751z00_10184)
{ /* Ieee/number.scm 827 */
 BGL_LONGLONG_T BgL_za72za7_4219;
BgL_za72za7_4219 = 
BLLONG_TO_LLONG(BgL_yz00_104); 
return 
make_bllong(
(BgL_lxz00_2098/BgL_za72za7_4219));}  else 
{ /* Ieee/number.scm 826 */
return 
DOUBLE_TO_REAL(
(BgL_fxz00_2097/
(double)(
BLLONG_TO_LLONG(BgL_yz00_104))));} } } } }  else 
{ /* Ieee/number.scm 823 */
if(
BIGNUMP(BgL_yz00_104))
{ /* Ieee/number.scm 830 */
 obj_t BgL_qz00_2104;
{ /* Ieee/number.scm 831 */
 obj_t BgL_arg2460z00_2109;
{ /* Ieee/number.scm 831 */
 long BgL_xz00_4222;
BgL_xz00_4222 = 
BELONG_TO_LONG(BgL_xz00_103); 
BgL_arg2460z00_2109 = 
bgl_long_to_bignum(BgL_xz00_4222); } 
BgL_qz00_2104 = 
bgl_bignum_div(BgL_arg2460z00_2109, BgL_yz00_104); } 
{ /* Ieee/number.scm 831 */
 obj_t BgL_rz00_2105;
{ /* Ieee/number.scm 832 */
 obj_t BgL_tmpz00_4223;
{ /* Ieee/number.scm 832 */
 int BgL_tmpz00_10201;
BgL_tmpz00_10201 = 
(int)(1L); 
BgL_tmpz00_4223 = 
BGL_MVALUES_VAL(BgL_tmpz00_10201); } 
{ /* Ieee/number.scm 832 */
 int BgL_tmpz00_10204;
BgL_tmpz00_10204 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10204, BUNSPEC); } 
BgL_rz00_2105 = BgL_tmpz00_4223; } 
{ /* Ieee/number.scm 832 */
 bool_t BgL_test4753z00_10207;
{ /* Ieee/number.scm 832 */
 obj_t BgL_nz00_4224;
if(
BIGNUMP(BgL_rz00_2105))
{ /* Ieee/number.scm 832 */
BgL_nz00_4224 = BgL_rz00_2105; }  else 
{ 
 obj_t BgL_auxz00_10210;
BgL_auxz00_10210 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(31109L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2105); 
FAILURE(BgL_auxz00_10210,BFALSE,BFALSE);} 
BgL_test4753z00_10207 = 
BXZERO(BgL_nz00_4224); } 
if(BgL_test4753z00_10207)
{ /* Ieee/number.scm 832 */
return BgL_qz00_2104;}  else 
{ /* Ieee/number.scm 832 */
return 
DOUBLE_TO_REAL(
(
(double)(
BELONG_TO_LONG(BgL_xz00_103))/
bgl_bignum_to_flonum(BgL_yz00_104)));} } } }  else 
{ /* Ieee/number.scm 829 */
return 
BGl_errorz00zz__errorz00(BGl_string3779z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_104);} } } } } }  else 
{ /* Ieee/number.scm 810 */
if(
LLONGP(BgL_xz00_103))
{ /* Ieee/number.scm 837 */
if(
INTEGERP(BgL_yz00_104))
{ /* Ieee/number.scm 840 */
 BGL_LONGLONG_T BgL_lyz00_2112;
{ /* Ieee/number.scm 840 */
 long BgL_tmpz00_10225;
BgL_tmpz00_10225 = 
(long)CINT(BgL_yz00_104); 
BgL_lyz00_2112 = 
LONG_TO_LLONG(BgL_tmpz00_10225); } 
{ /* Ieee/number.scm 841 */
 bool_t BgL_test4757z00_10228;
{ /* Ieee/number.scm 841 */
 BGL_LONGLONG_T BgL_arg2469z00_2117;
{ /* Ieee/number.scm 841 */
 BGL_LONGLONG_T BgL_tmpz00_10229;
BgL_tmpz00_10229 = 
BLLONG_TO_LLONG(BgL_xz00_103); 
BgL_arg2469z00_2117 = 
(BgL_tmpz00_10229%BgL_lyz00_2112); } 
BgL_test4757z00_10228 = 
(BgL_arg2469z00_2117==((BGL_LONGLONG_T)0)); } 
if(BgL_test4757z00_10228)
{ /* Ieee/number.scm 842 */
 BGL_LONGLONG_T BgL_za71za7_4233;
BgL_za71za7_4233 = 
BLLONG_TO_LLONG(BgL_xz00_103); 
return 
make_bllong(
(BgL_za71za7_4233/BgL_lyz00_2112));}  else 
{ /* Ieee/number.scm 841 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_103))/
(double)(
(long)CINT(BgL_yz00_104))));} } }  else 
{ /* Ieee/number.scm 839 */
if(
REALP(BgL_yz00_104))
{ /* Ieee/number.scm 844 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_103))/
REAL_TO_DOUBLE(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 844 */
if(
ELONGP(BgL_yz00_104))
{ /* Ieee/number.scm 847 */
 double BgL_fyz00_2121;
BgL_fyz00_2121 = 
(double)(
BELONG_TO_LONG(BgL_yz00_104)); 
{ /* Ieee/number.scm 847 */
 BGL_LONGLONG_T BgL_lyz00_2122;
BgL_lyz00_2122 = 
(BGL_LONGLONG_T)(BgL_fyz00_2121); 
{ /* Ieee/number.scm 848 */

{ /* Ieee/number.scm 849 */
 bool_t BgL_test4760z00_10254;
{ /* Ieee/number.scm 849 */
 BGL_LONGLONG_T BgL_arg2476z00_2126;
{ /* Ieee/number.scm 849 */
 BGL_LONGLONG_T BgL_tmpz00_10255;
BgL_tmpz00_10255 = 
BLLONG_TO_LLONG(BgL_xz00_103); 
BgL_arg2476z00_2126 = 
(BgL_tmpz00_10255%BgL_lyz00_2122); } 
BgL_test4760z00_10254 = 
(BgL_arg2476z00_2126==((BGL_LONGLONG_T)0)); } 
if(BgL_test4760z00_10254)
{ /* Ieee/number.scm 850 */
 BGL_LONGLONG_T BgL_za71za7_4243;
BgL_za71za7_4243 = 
BLLONG_TO_LLONG(BgL_xz00_103); 
return 
make_bllong(
(BgL_za71za7_4243/BgL_lyz00_2122));}  else 
{ /* Ieee/number.scm 849 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_103))/BgL_fyz00_2121));} } } } }  else 
{ /* Ieee/number.scm 846 */
if(
LLONGP(BgL_yz00_104))
{ /* Ieee/number.scm 853 */
 bool_t BgL_test4762z00_10268;
{ /* Ieee/number.scm 853 */
 BGL_LONGLONG_T BgL_arg2483z00_2132;
{ /* Ieee/number.scm 853 */
 BGL_LONGLONG_T BgL_auxz00_10271; BGL_LONGLONG_T BgL_tmpz00_10269;
BgL_auxz00_10271 = 
BLLONG_TO_LLONG(BgL_yz00_104); 
BgL_tmpz00_10269 = 
BLLONG_TO_LLONG(BgL_xz00_103); 
BgL_arg2483z00_2132 = 
(BgL_tmpz00_10269%BgL_auxz00_10271); } 
BgL_test4762z00_10268 = 
(BgL_arg2483z00_2132==((BGL_LONGLONG_T)0)); } 
if(BgL_test4762z00_10268)
{ /* Ieee/number.scm 854 */
 BGL_LONGLONG_T BgL_za71za7_4251; BGL_LONGLONG_T BgL_za72za7_4252;
BgL_za71za7_4251 = 
BLLONG_TO_LLONG(BgL_xz00_103); 
BgL_za72za7_4252 = 
BLLONG_TO_LLONG(BgL_yz00_104); 
return 
make_bllong(
(BgL_za71za7_4251/BgL_za72za7_4252));}  else 
{ /* Ieee/number.scm 853 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_103))/
(double)(
BLLONG_TO_LLONG(BgL_yz00_104))));} }  else 
{ /* Ieee/number.scm 852 */
if(
BIGNUMP(BgL_yz00_104))
{ /* Ieee/number.scm 857 */
 obj_t BgL_qz00_2134;
BgL_qz00_2134 = 
bgl_bignum_div(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_103)), BgL_yz00_104); 
{ /* Ieee/number.scm 858 */
 obj_t BgL_rz00_2135;
{ /* Ieee/number.scm 859 */
 obj_t BgL_tmpz00_4256;
{ /* Ieee/number.scm 859 */
 int BgL_tmpz00_10290;
BgL_tmpz00_10290 = 
(int)(1L); 
BgL_tmpz00_4256 = 
BGL_MVALUES_VAL(BgL_tmpz00_10290); } 
{ /* Ieee/number.scm 859 */
 int BgL_tmpz00_10293;
BgL_tmpz00_10293 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10293, BUNSPEC); } 
BgL_rz00_2135 = BgL_tmpz00_4256; } 
{ /* Ieee/number.scm 859 */
 bool_t BgL_test4764z00_10296;
{ /* Ieee/number.scm 859 */
 obj_t BgL_nz00_4257;
if(
BIGNUMP(BgL_rz00_2135))
{ /* Ieee/number.scm 859 */
BgL_nz00_4257 = BgL_rz00_2135; }  else 
{ 
 obj_t BgL_auxz00_10299;
BgL_auxz00_10299 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(31876L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2135); 
FAILURE(BgL_auxz00_10299,BFALSE,BFALSE);} 
BgL_test4764z00_10296 = 
BXZERO(BgL_nz00_4257); } 
if(BgL_test4764z00_10296)
{ /* Ieee/number.scm 859 */
return BgL_qz00_2134;}  else 
{ /* Ieee/number.scm 859 */
return 
DOUBLE_TO_REAL(
(
(double)(
BLLONG_TO_LLONG(BgL_xz00_103))/
bgl_bignum_to_flonum(BgL_yz00_104)));} } } }  else 
{ /* Ieee/number.scm 856 */
return 
BGl_errorz00zz__errorz00(BGl_string3779z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_104);} } } } } }  else 
{ /* Ieee/number.scm 837 */
if(
BIGNUMP(BgL_xz00_103))
{ /* Ieee/number.scm 864 */
if(
INTEGERP(BgL_yz00_104))
{ /* Ieee/number.scm 867 */
 obj_t BgL_qz00_2142;
BgL_qz00_2142 = 
bgl_bignum_div(BgL_xz00_103, 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_104))); 
{ /* Ieee/number.scm 868 */
 obj_t BgL_rz00_2143;
{ /* Ieee/number.scm 869 */
 obj_t BgL_tmpz00_4262;
{ /* Ieee/number.scm 869 */
 int BgL_tmpz00_10317;
BgL_tmpz00_10317 = 
(int)(1L); 
BgL_tmpz00_4262 = 
BGL_MVALUES_VAL(BgL_tmpz00_10317); } 
{ /* Ieee/number.scm 869 */
 int BgL_tmpz00_10320;
BgL_tmpz00_10320 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10320, BUNSPEC); } 
BgL_rz00_2143 = BgL_tmpz00_4262; } 
{ /* Ieee/number.scm 869 */
 bool_t BgL_test4768z00_10323;
{ /* Ieee/number.scm 869 */
 obj_t BgL_nz00_4263;
if(
BIGNUMP(BgL_rz00_2143))
{ /* Ieee/number.scm 869 */
BgL_nz00_4263 = BgL_rz00_2143; }  else 
{ 
 obj_t BgL_auxz00_10326;
BgL_auxz00_10326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(32122L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2143); 
FAILURE(BgL_auxz00_10326,BFALSE,BFALSE);} 
BgL_test4768z00_10323 = 
BXZERO(BgL_nz00_4263); } 
if(BgL_test4768z00_10323)
{ /* Ieee/number.scm 869 */
return BgL_qz00_2142;}  else 
{ /* Ieee/number.scm 869 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_103)/
(double)(
(long)CINT(BgL_yz00_104))));} } } }  else 
{ /* Ieee/number.scm 866 */
if(
REALP(BgL_yz00_104))
{ /* Ieee/number.scm 872 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_103)/
REAL_TO_DOUBLE(BgL_yz00_104)));}  else 
{ /* Ieee/number.scm 872 */
if(
ELONGP(BgL_yz00_104))
{ /* Ieee/number.scm 875 */
 obj_t BgL_qz00_2151;
{ /* Ieee/number.scm 876 */
 obj_t BgL_arg2502z00_2156;
{ /* Ieee/number.scm 876 */
 long BgL_xz00_4270;
BgL_xz00_4270 = 
BELONG_TO_LONG(BgL_yz00_104); 
BgL_arg2502z00_2156 = 
bgl_long_to_bignum(BgL_xz00_4270); } 
BgL_qz00_2151 = 
bgl_bignum_div(BgL_xz00_103, BgL_arg2502z00_2156); } 
{ /* Ieee/number.scm 876 */
 obj_t BgL_rz00_2152;
{ /* Ieee/number.scm 877 */
 obj_t BgL_tmpz00_4271;
{ /* Ieee/number.scm 877 */
 int BgL_tmpz00_10347;
BgL_tmpz00_10347 = 
(int)(1L); 
BgL_tmpz00_4271 = 
BGL_MVALUES_VAL(BgL_tmpz00_10347); } 
{ /* Ieee/number.scm 877 */
 int BgL_tmpz00_10350;
BgL_tmpz00_10350 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10350, BUNSPEC); } 
BgL_rz00_2152 = BgL_tmpz00_4271; } 
{ /* Ieee/number.scm 877 */
 bool_t BgL_test4772z00_10353;
{ /* Ieee/number.scm 877 */
 obj_t BgL_nz00_4272;
if(
BIGNUMP(BgL_rz00_2152))
{ /* Ieee/number.scm 877 */
BgL_nz00_4272 = BgL_rz00_2152; }  else 
{ 
 obj_t BgL_auxz00_10356;
BgL_auxz00_10356 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(32342L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2152); 
FAILURE(BgL_auxz00_10356,BFALSE,BFALSE);} 
BgL_test4772z00_10353 = 
BXZERO(BgL_nz00_4272); } 
if(BgL_test4772z00_10353)
{ /* Ieee/number.scm 877 */
return BgL_qz00_2151;}  else 
{ /* Ieee/number.scm 877 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_103)/
(double)(
BELONG_TO_LONG(BgL_yz00_104))));} } } }  else 
{ /* Ieee/number.scm 874 */
if(
LLONGP(BgL_yz00_104))
{ /* Ieee/number.scm 881 */
 obj_t BgL_qz00_2158;
BgL_qz00_2158 = 
bgl_bignum_div(BgL_xz00_103, 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_104))); 
{ /* Ieee/number.scm 882 */
 obj_t BgL_rz00_2159;
{ /* Ieee/number.scm 883 */
 obj_t BgL_tmpz00_4277;
{ /* Ieee/number.scm 883 */
 int BgL_tmpz00_10371;
BgL_tmpz00_10371 = 
(int)(1L); 
BgL_tmpz00_4277 = 
BGL_MVALUES_VAL(BgL_tmpz00_10371); } 
{ /* Ieee/number.scm 883 */
 int BgL_tmpz00_10374;
BgL_tmpz00_10374 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10374, BUNSPEC); } 
BgL_rz00_2159 = BgL_tmpz00_4277; } 
{ /* Ieee/number.scm 883 */
 bool_t BgL_test4775z00_10377;
{ /* Ieee/number.scm 883 */
 obj_t BgL_nz00_4278;
if(
BIGNUMP(BgL_rz00_2159))
{ /* Ieee/number.scm 883 */
BgL_nz00_4278 = BgL_rz00_2159; }  else 
{ 
 obj_t BgL_auxz00_10380;
BgL_auxz00_10380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(32511L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2159); 
FAILURE(BgL_auxz00_10380,BFALSE,BFALSE);} 
BgL_test4775z00_10377 = 
BXZERO(BgL_nz00_4278); } 
if(BgL_test4775z00_10377)
{ /* Ieee/number.scm 883 */
return BgL_qz00_2158;}  else 
{ /* Ieee/number.scm 883 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_103)/
(double)(
BLLONG_TO_LLONG(BgL_yz00_104))));} } } }  else 
{ /* Ieee/number.scm 880 */
if(
BIGNUMP(BgL_yz00_104))
{ /* Ieee/number.scm 887 */
 obj_t BgL_qz00_2165;
BgL_qz00_2165 = 
bgl_bignum_div(BgL_xz00_103, BgL_yz00_104); 
{ /* Ieee/number.scm 888 */
 obj_t BgL_rz00_2166;
{ /* Ieee/number.scm 889 */
 obj_t BgL_tmpz00_4282;
{ /* Ieee/number.scm 889 */
 int BgL_tmpz00_10393;
BgL_tmpz00_10393 = 
(int)(1L); 
BgL_tmpz00_4282 = 
BGL_MVALUES_VAL(BgL_tmpz00_10393); } 
{ /* Ieee/number.scm 889 */
 int BgL_tmpz00_10396;
BgL_tmpz00_10396 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_10396, BUNSPEC); } 
BgL_rz00_2166 = BgL_tmpz00_4282; } 
{ /* Ieee/number.scm 889 */
 bool_t BgL_test4778z00_10399;
{ /* Ieee/number.scm 889 */
 obj_t BgL_nz00_4283;
if(
BIGNUMP(BgL_rz00_2166))
{ /* Ieee/number.scm 889 */
BgL_nz00_4283 = BgL_rz00_2166; }  else 
{ 
 obj_t BgL_auxz00_10402;
BgL_auxz00_10402 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(32665L), BGl_string3778z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_rz00_2166); 
FAILURE(BgL_auxz00_10402,BFALSE,BFALSE);} 
BgL_test4778z00_10399 = 
BXZERO(BgL_nz00_4283); } 
if(BgL_test4778z00_10399)
{ /* Ieee/number.scm 889 */
return BgL_qz00_2165;}  else 
{ /* Ieee/number.scm 889 */
return 
DOUBLE_TO_REAL(
(
bgl_bignum_to_flonum(BgL_xz00_103)/
bgl_bignum_to_flonum(BgL_yz00_104)));} } } }  else 
{ /* Ieee/number.scm 886 */
return 
BGl_errorz00zz__errorz00(BGl_string3779z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_104);} } } } } }  else 
{ /* Ieee/number.scm 864 */
return 
BGl_errorz00zz__errorz00(BGl_string3779z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_103);} } } } } } 

}



/* &2/ */
obj_t BGl_z622zf2z90zz__r4_numbers_6_5z00(obj_t BgL_envz00_5028, obj_t BgL_xz00_5029, obj_t BgL_yz00_5030)
{
{ /* Ieee/number.scm 768 */
return 
BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_xz00_5029, BgL_yz00_5030);} 

}



/* / */
BGL_EXPORTED_DEF obj_t BGl_zf2zf2zz__r4_numbers_6_5z00(obj_t BgL_xz00_105, obj_t BgL_yz00_106)
{
{ /* Ieee/number.scm 900 */
if(
PAIRP(BgL_yz00_106))
{ /* Ieee/number.scm 902 */
 obj_t BgL_g1054z00_2171; obj_t BgL_g1055z00_2172;
BgL_g1054z00_2171 = 
BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_xz00_105, 
CAR(BgL_yz00_106)); 
BgL_g1055z00_2172 = 
CDR(BgL_yz00_106); 
{ 
 obj_t BgL_resultz00_4303; obj_t BgL_za7za7_4304;
BgL_resultz00_4303 = BgL_g1054z00_2171; 
BgL_za7za7_4304 = BgL_g1055z00_2172; 
BgL_loopz00_4302:
if(
PAIRP(BgL_za7za7_4304))
{ 
 obj_t BgL_za7za7_10424; obj_t BgL_resultz00_10421;
BgL_resultz00_10421 = 
BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_resultz00_4303, 
CAR(BgL_za7za7_4304)); 
BgL_za7za7_10424 = 
CDR(BgL_za7za7_4304); 
BgL_za7za7_4304 = BgL_za7za7_10424; 
BgL_resultz00_4303 = BgL_resultz00_10421; 
goto BgL_loopz00_4302;}  else 
{ /* Ieee/number.scm 904 */
return BgL_resultz00_4303;} } }  else 
{ /* Ieee/number.scm 901 */
BGL_TAIL return 
BGl_2zf2zf2zz__r4_numbers_6_5z00(
BINT(1L), BgL_xz00_105);} } 

}



/* &/ */
obj_t BGl_z62zf2z90zz__r4_numbers_6_5z00(obj_t BgL_envz00_5031, obj_t BgL_xz00_5032, obj_t BgL_yz00_5033)
{
{ /* Ieee/number.scm 900 */
return 
BGl_zf2zf2zz__r4_numbers_6_5z00(BgL_xz00_5032, BgL_yz00_5033);} 

}



/* abs */
BGL_EXPORTED_DEF obj_t BGl_absz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_107)
{
{ /* Ieee/number.scm 913 */
if(
INTEGERP(BgL_xz00_107))
{ /* Ieee/number.scm 915 */
if(
(
(long)CINT(BgL_xz00_107)==BGL_LONG_MIN))
{ /* Ieee/number.scm 916 */
return 
bgl_bignum_neg(
bgl_long_to_bignum(
(long)CINT(BgL_xz00_107)));}  else 
{ /* Ieee/number.scm 918 */
 long BgL_nz00_4319;
BgL_nz00_4319 = 
(long)CINT(BgL_xz00_107); 
if(
(BgL_nz00_4319<0L))
{ /* Ieee/number.scm 918 */
return 
BINT(
NEG(BgL_nz00_4319));}  else 
{ /* Ieee/number.scm 918 */
return 
BINT(BgL_nz00_4319);} } }  else 
{ /* Ieee/number.scm 915 */
if(
REALP(BgL_xz00_107))
{ /* Ieee/number.scm 919 */
return 
DOUBLE_TO_REAL(
fabs(
REAL_TO_DOUBLE(BgL_xz00_107)));}  else 
{ /* Ieee/number.scm 919 */
if(
ELONGP(BgL_xz00_107))
{ /* Ieee/number.scm 922 */
 bool_t BgL_test4787z00_10450;
{ /* Ieee/number.scm 922 */
 long BgL_n1z00_4324;
BgL_n1z00_4324 = 
BELONG_TO_LONG(BgL_xz00_107); 
BgL_test4787z00_10450 = 
(BgL_n1z00_4324==LONG_MIN); } 
if(BgL_test4787z00_10450)
{ /* Ieee/number.scm 923 */
 obj_t BgL_tmpz00_10453;
{ /* Ieee/number.scm 923 */
 long BgL_xz00_4326;
BgL_xz00_4326 = 
BELONG_TO_LONG(BgL_xz00_107); 
BgL_tmpz00_10453 = 
bgl_long_to_bignum(BgL_xz00_4326); } 
return 
bgl_bignum_neg(BgL_tmpz00_10453);}  else 
{ /* Ieee/number.scm 924 */
 long BgL_nz00_4328;
BgL_nz00_4328 = 
BELONG_TO_LONG(BgL_xz00_107); 
if(
(BgL_nz00_4328<((long)0)))
{ /* Ieee/number.scm 924 */
 long BgL_tmpz00_10460;
BgL_tmpz00_10460 = 
NEG(BgL_nz00_4328); 
return 
make_belong(BgL_tmpz00_10460);}  else 
{ /* Ieee/number.scm 924 */
return 
make_belong(BgL_nz00_4328);} } }  else 
{ /* Ieee/number.scm 921 */
if(
LLONGP(BgL_xz00_107))
{ /* Ieee/number.scm 925 */
if(
(
BLLONG_TO_LLONG(BgL_xz00_107)==BGL_LONGLONG_MIN))
{ /* Ieee/number.scm 926 */
return 
bgl_bignum_neg(
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_xz00_107)));}  else 
{ /* Ieee/number.scm 928 */
 BGL_LONGLONG_T BgL_nz00_4337;
BgL_nz00_4337 = 
BLLONG_TO_LLONG(BgL_xz00_107); 
if(
(BgL_nz00_4337<((BGL_LONGLONG_T)0)))
{ /* Ieee/number.scm 928 */
return 
make_bllong(
NEG(BgL_nz00_4337));}  else 
{ /* Ieee/number.scm 928 */
return 
make_bllong(BgL_nz00_4337);} } }  else 
{ /* Ieee/number.scm 925 */
if(
BIGNUMP(BgL_xz00_107))
{ /* Ieee/number.scm 929 */
return 
bgl_bignum_abs(BgL_xz00_107);}  else 
{ /* Ieee/number.scm 929 */
return 
BGl_errorz00zz__errorz00(BGl_string3780z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_107);} } } } } } 

}



/* &abs */
obj_t BGl_z62absz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5034, obj_t BgL_xz00_5035)
{
{ /* Ieee/number.scm 913 */
return 
BGl_absz00zz__r4_numbers_6_5z00(BgL_xz00_5035);} 

}



/* floor */
BGL_EXPORTED_DEF obj_t BGl_floorz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_108)
{
{ /* Ieee/number.scm 937 */
if(
INTEGERP(BgL_xz00_108))
{ /* Ieee/number.scm 939 */
return BgL_xz00_108;}  else 
{ /* Ieee/number.scm 939 */
if(
REALP(BgL_xz00_108))
{ /* Ieee/number.scm 940 */
 double BgL_rz00_4343;
BgL_rz00_4343 = 
REAL_TO_DOUBLE(BgL_xz00_108); 
return 
DOUBLE_TO_REAL(
floor(BgL_rz00_4343));}  else 
{ /* Ieee/number.scm 940 */
if(
ELONGP(BgL_xz00_108))
{ /* Ieee/number.scm 941 */
return BgL_xz00_108;}  else 
{ /* Ieee/number.scm 941 */
if(
LLONGP(BgL_xz00_108))
{ /* Ieee/number.scm 942 */
return BgL_xz00_108;}  else 
{ /* Ieee/number.scm 942 */
if(
BIGNUMP(BgL_xz00_108))
{ /* Ieee/number.scm 943 */
return BgL_xz00_108;}  else 
{ /* Ieee/number.scm 943 */
return 
BGl_errorz00zz__errorz00(BGl_string3781z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_108);} } } } } } 

}



/* &floor */
obj_t BGl_z62floorz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5036, obj_t BgL_xz00_5037)
{
{ /* Ieee/number.scm 937 */
return 
BGl_floorz00zz__r4_numbers_6_5z00(BgL_xz00_5037);} 

}



/* ceiling */
BGL_EXPORTED_DEF obj_t BGl_ceilingz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_109)
{
{ /* Ieee/number.scm 949 */
if(
INTEGERP(BgL_xz00_109))
{ /* Ieee/number.scm 951 */
return BgL_xz00_109;}  else 
{ /* Ieee/number.scm 951 */
if(
REALP(BgL_xz00_109))
{ /* Ieee/number.scm 952 */
 double BgL_rz00_4344;
BgL_rz00_4344 = 
REAL_TO_DOUBLE(BgL_xz00_109); 
return 
DOUBLE_TO_REAL(
ceil(BgL_rz00_4344));}  else 
{ /* Ieee/number.scm 952 */
if(
ELONGP(BgL_xz00_109))
{ /* Ieee/number.scm 953 */
return BgL_xz00_109;}  else 
{ /* Ieee/number.scm 953 */
if(
LLONGP(BgL_xz00_109))
{ /* Ieee/number.scm 954 */
return BgL_xz00_109;}  else 
{ /* Ieee/number.scm 954 */
if(
BIGNUMP(BgL_xz00_109))
{ /* Ieee/number.scm 955 */
return BgL_xz00_109;}  else 
{ /* Ieee/number.scm 955 */
return 
BGl_errorz00zz__errorz00(BGl_string3782z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_109);} } } } } } 

}



/* &ceiling */
obj_t BGl_z62ceilingz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5038, obj_t BgL_xz00_5039)
{
{ /* Ieee/number.scm 949 */
return 
BGl_ceilingz00zz__r4_numbers_6_5z00(BgL_xz00_5039);} 

}



/* truncate */
BGL_EXPORTED_DEF obj_t BGl_truncatez00zz__r4_numbers_6_5z00(obj_t BgL_xz00_110)
{
{ /* Ieee/number.scm 961 */
if(
INTEGERP(BgL_xz00_110))
{ /* Ieee/number.scm 963 */
return BgL_xz00_110;}  else 
{ /* Ieee/number.scm 963 */
if(
REALP(BgL_xz00_110))
{ /* Ieee/number.scm 964 */
 double BgL_rz00_4345;
BgL_rz00_4345 = 
REAL_TO_DOUBLE(BgL_xz00_110); 
if(
(BgL_rz00_4345<((double)0.0)))
{ /* Ieee/number.scm 964 */
return 
DOUBLE_TO_REAL(
ceil(BgL_rz00_4345));}  else 
{ /* Ieee/number.scm 964 */
return 
DOUBLE_TO_REAL(
floor(BgL_rz00_4345));} }  else 
{ /* Ieee/number.scm 964 */
if(
ELONGP(BgL_xz00_110))
{ /* Ieee/number.scm 965 */
return BgL_xz00_110;}  else 
{ /* Ieee/number.scm 965 */
if(
LLONGP(BgL_xz00_110))
{ /* Ieee/number.scm 966 */
return BgL_xz00_110;}  else 
{ /* Ieee/number.scm 966 */
return 
BGl_errorz00zz__errorz00(BGl_string3783z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_110);} } } } } 

}



/* &truncate */
obj_t BGl_z62truncatez62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5040, obj_t BgL_xz00_5041)
{
{ /* Ieee/number.scm 961 */
return 
BGl_truncatez00zz__r4_numbers_6_5z00(BgL_xz00_5041);} 

}



/* round */
BGL_EXPORTED_DEF obj_t BGl_roundz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_111)
{
{ /* Ieee/number.scm 972 */
if(
INTEGERP(BgL_xz00_111))
{ /* Ieee/number.scm 974 */
return BgL_xz00_111;}  else 
{ /* Ieee/number.scm 974 */
if(
REALP(BgL_xz00_111))
{ /* Ieee/number.scm 975 */
return 
DOUBLE_TO_REAL(
BGl_roundflz00zz__r4_numbers_6_5_flonumz00(
REAL_TO_DOUBLE(BgL_xz00_111)));}  else 
{ /* Ieee/number.scm 975 */
if(
ELONGP(BgL_xz00_111))
{ /* Ieee/number.scm 976 */
return BgL_xz00_111;}  else 
{ /* Ieee/number.scm 976 */
if(
LLONGP(BgL_xz00_111))
{ /* Ieee/number.scm 977 */
return BgL_xz00_111;}  else 
{ /* Ieee/number.scm 977 */
if(
BIGNUMP(BgL_xz00_111))
{ /* Ieee/number.scm 978 */
return BgL_xz00_111;}  else 
{ /* Ieee/number.scm 978 */
return 
BGl_errorz00zz__errorz00(BGl_string3784z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_111);} } } } } } 

}



/* &round */
obj_t BGl_z62roundz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5042, obj_t BgL_xz00_5043)
{
{ /* Ieee/number.scm 972 */
return 
BGl_roundz00zz__r4_numbers_6_5z00(BgL_xz00_5043);} 

}



/* exp */
BGL_EXPORTED_DEF double BGl_expz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_112)
{
{ /* Ieee/number.scm 984 */
if(
REALP(BgL_xz00_112))
{ /* Ieee/number.scm 986 */
return 
exp(
REAL_TO_DOUBLE(BgL_xz00_112));}  else 
{ /* Ieee/number.scm 986 */
if(
INTEGERP(BgL_xz00_112))
{ /* Ieee/number.scm 987 */
return 
exp(
(double)(
(long)CINT(BgL_xz00_112)));}  else 
{ /* Ieee/number.scm 987 */
if(
ELONGP(BgL_xz00_112))
{ /* Ieee/number.scm 988 */
return 
exp(
(double)(
BELONG_TO_LONG(BgL_xz00_112)));}  else 
{ /* Ieee/number.scm 988 */
if(
LLONGP(BgL_xz00_112))
{ /* Ieee/number.scm 989 */
return 
exp(
(double)(
BLLONG_TO_LLONG(BgL_xz00_112)));}  else 
{ /* Ieee/number.scm 989 */
if(
BIGNUMP(BgL_xz00_112))
{ /* Ieee/number.scm 990 */
return 
exp(
bgl_bignum_to_flonum(BgL_xz00_112));}  else 
{ /* Ieee/number.scm 991 */
 obj_t BgL_tmpz00_10568;
{ /* Ieee/number.scm 991 */
 obj_t BgL_aux3658z00_5479;
BgL_aux3658z00_5479 = 
BGl_errorz00zz__errorz00(BGl_string3785z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_112); 
if(
REALP(BgL_aux3658z00_5479))
{ /* Ieee/number.scm 991 */
BgL_tmpz00_10568 = BgL_aux3658z00_5479
; }  else 
{ 
 obj_t BgL_auxz00_10572;
BgL_auxz00_10572 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(36042L), BGl_string3785z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3658z00_5479); 
FAILURE(BgL_auxz00_10572,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10568);} } } } } } 

}



/* &exp */
obj_t BGl_z62expz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5044, obj_t BgL_xz00_5045)
{
{ /* Ieee/number.scm 984 */
return 
DOUBLE_TO_REAL(
BGl_expz00zz__r4_numbers_6_5z00(BgL_xz00_5045));} 

}



/* log */
BGL_EXPORTED_DEF double BGl_logz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_113)
{
{ /* Ieee/number.scm 996 */
if(
REALP(BgL_xz00_113))
{ /* Ieee/number.scm 998 */
return 
log(
REAL_TO_DOUBLE(BgL_xz00_113));}  else 
{ /* Ieee/number.scm 998 */
if(
INTEGERP(BgL_xz00_113))
{ /* Ieee/number.scm 999 */
return 
log(
(double)(
(long)CINT(BgL_xz00_113)));}  else 
{ /* Ieee/number.scm 999 */
if(
ELONGP(BgL_xz00_113))
{ /* Ieee/number.scm 1000 */
return 
log(
(double)(
BELONG_TO_LONG(BgL_xz00_113)));}  else 
{ /* Ieee/number.scm 1000 */
if(
LLONGP(BgL_xz00_113))
{ /* Ieee/number.scm 1001 */
return 
log(
(double)(
BLLONG_TO_LLONG(BgL_xz00_113)));}  else 
{ /* Ieee/number.scm 1001 */
if(
BIGNUMP(BgL_xz00_113))
{ /* Ieee/number.scm 1002 */
return 
log(
bgl_bignum_to_flonum(BgL_xz00_113));}  else 
{ /* Ieee/number.scm 1003 */
 obj_t BgL_tmpz00_10602;
{ /* Ieee/number.scm 1003 */
 obj_t BgL_aux3660z00_5481;
BgL_aux3660z00_5481 = 
BGl_errorz00zz__errorz00(BGl_string3786z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_113); 
if(
REALP(BgL_aux3660z00_5481))
{ /* Ieee/number.scm 1003 */
BgL_tmpz00_10602 = BgL_aux3660z00_5481
; }  else 
{ 
 obj_t BgL_auxz00_10606;
BgL_auxz00_10606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(36554L), BGl_string3786z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3660z00_5481); 
FAILURE(BgL_auxz00_10606,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10602);} } } } } } 

}



/* &log */
obj_t BGl_z62logz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5046, obj_t BgL_xz00_5047)
{
{ /* Ieee/number.scm 996 */
return 
DOUBLE_TO_REAL(
BGl_logz00zz__r4_numbers_6_5z00(BgL_xz00_5047));} 

}



/* log2 */
BGL_EXPORTED_DEF double BGl_log2z00zz__r4_numbers_6_5z00(obj_t BgL_xz00_114)
{
{ /* Ieee/number.scm 1008 */
if(
REALP(BgL_xz00_114))
{ /* Ieee/number.scm 1010 */
return 
log2(
REAL_TO_DOUBLE(BgL_xz00_114));}  else 
{ /* Ieee/number.scm 1010 */
if(
INTEGERP(BgL_xz00_114))
{ /* Ieee/number.scm 1011 */
return 
log2(
(double)(
(long)CINT(BgL_xz00_114)));}  else 
{ /* Ieee/number.scm 1011 */
if(
ELONGP(BgL_xz00_114))
{ /* Ieee/number.scm 1012 */
return 
log2(
(double)(
BELONG_TO_LONG(BgL_xz00_114)));}  else 
{ /* Ieee/number.scm 1012 */
if(
LLONGP(BgL_xz00_114))
{ /* Ieee/number.scm 1013 */
return 
log2(
(double)(
BLLONG_TO_LLONG(BgL_xz00_114)));}  else 
{ /* Ieee/number.scm 1013 */
if(
BIGNUMP(BgL_xz00_114))
{ /* Ieee/number.scm 1014 */
return 
log2(
bgl_bignum_to_flonum(BgL_xz00_114));}  else 
{ /* Ieee/number.scm 1015 */
 obj_t BgL_tmpz00_10636;
{ /* Ieee/number.scm 1015 */
 obj_t BgL_aux3662z00_5483;
BgL_aux3662z00_5483 = 
BGl_errorz00zz__errorz00(BGl_string3787z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_114); 
if(
REALP(BgL_aux3662z00_5483))
{ /* Ieee/number.scm 1015 */
BgL_tmpz00_10636 = BgL_aux3662z00_5483
; }  else 
{ 
 obj_t BgL_auxz00_10640;
BgL_auxz00_10640 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(37072L), BGl_string3787z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3662z00_5483); 
FAILURE(BgL_auxz00_10640,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10636);} } } } } } 

}



/* &log2 */
obj_t BGl_z62log2z62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5048, obj_t BgL_xz00_5049)
{
{ /* Ieee/number.scm 1008 */
return 
DOUBLE_TO_REAL(
BGl_log2z00zz__r4_numbers_6_5z00(BgL_xz00_5049));} 

}



/* log10 */
BGL_EXPORTED_DEF double BGl_log10z00zz__r4_numbers_6_5z00(obj_t BgL_xz00_115)
{
{ /* Ieee/number.scm 1020 */
if(
REALP(BgL_xz00_115))
{ /* Ieee/number.scm 1022 */
return 
log10(
REAL_TO_DOUBLE(BgL_xz00_115));}  else 
{ /* Ieee/number.scm 1022 */
if(
INTEGERP(BgL_xz00_115))
{ /* Ieee/number.scm 1023 */
return 
log10(
(double)(
(long)CINT(BgL_xz00_115)));}  else 
{ /* Ieee/number.scm 1023 */
if(
ELONGP(BgL_xz00_115))
{ /* Ieee/number.scm 1024 */
return 
log10(
(double)(
BELONG_TO_LONG(BgL_xz00_115)));}  else 
{ /* Ieee/number.scm 1024 */
if(
LLONGP(BgL_xz00_115))
{ /* Ieee/number.scm 1025 */
return 
log10(
(double)(
BLLONG_TO_LLONG(BgL_xz00_115)));}  else 
{ /* Ieee/number.scm 1025 */
if(
BIGNUMP(BgL_xz00_115))
{ /* Ieee/number.scm 1026 */
return 
log10(
bgl_bignum_to_flonum(BgL_xz00_115));}  else 
{ /* Ieee/number.scm 1027 */
 obj_t BgL_tmpz00_10670;
{ /* Ieee/number.scm 1027 */
 obj_t BgL_aux3664z00_5485;
BgL_aux3664z00_5485 = 
BGl_errorz00zz__errorz00(BGl_string3788z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_115); 
if(
REALP(BgL_aux3664z00_5485))
{ /* Ieee/number.scm 1027 */
BgL_tmpz00_10670 = BgL_aux3664z00_5485
; }  else 
{ 
 obj_t BgL_auxz00_10674;
BgL_auxz00_10674 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(37597L), BGl_string3788z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3664z00_5485); 
FAILURE(BgL_auxz00_10674,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10670);} } } } } } 

}



/* &log10 */
obj_t BGl_z62log10z62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5050, obj_t BgL_xz00_5051)
{
{ /* Ieee/number.scm 1020 */
return 
DOUBLE_TO_REAL(
BGl_log10z00zz__r4_numbers_6_5z00(BgL_xz00_5051));} 

}



/* sin */
BGL_EXPORTED_DEF double BGl_sinz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_116)
{
{ /* Ieee/number.scm 1032 */
if(
REALP(BgL_xz00_116))
{ /* Ieee/number.scm 1034 */
return 
sin(
REAL_TO_DOUBLE(BgL_xz00_116));}  else 
{ /* Ieee/number.scm 1034 */
if(
INTEGERP(BgL_xz00_116))
{ /* Ieee/number.scm 1035 */
return 
sin(
(double)(
(long)CINT(BgL_xz00_116)));}  else 
{ /* Ieee/number.scm 1035 */
if(
ELONGP(BgL_xz00_116))
{ /* Ieee/number.scm 1036 */
return 
sin(
(double)(
BELONG_TO_LONG(BgL_xz00_116)));}  else 
{ /* Ieee/number.scm 1036 */
if(
LLONGP(BgL_xz00_116))
{ /* Ieee/number.scm 1037 */
return 
sin(
(double)(
BLLONG_TO_LLONG(BgL_xz00_116)));}  else 
{ /* Ieee/number.scm 1037 */
if(
BIGNUMP(BgL_xz00_116))
{ /* Ieee/number.scm 1038 */
return 
sin(
bgl_bignum_to_flonum(BgL_xz00_116));}  else 
{ /* Ieee/number.scm 1039 */
 obj_t BgL_tmpz00_10704;
{ /* Ieee/number.scm 1039 */
 obj_t BgL_aux3666z00_5487;
BgL_aux3666z00_5487 = 
BGl_errorz00zz__errorz00(BGl_string3789z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_116); 
if(
REALP(BgL_aux3666z00_5487))
{ /* Ieee/number.scm 1039 */
BgL_tmpz00_10704 = BgL_aux3666z00_5487
; }  else 
{ 
 obj_t BgL_auxz00_10708;
BgL_auxz00_10708 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(38110L), BGl_string3789z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3666z00_5487); 
FAILURE(BgL_auxz00_10708,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10704);} } } } } } 

}



/* &sin */
obj_t BGl_z62sinz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5052, obj_t BgL_xz00_5053)
{
{ /* Ieee/number.scm 1032 */
return 
DOUBLE_TO_REAL(
BGl_sinz00zz__r4_numbers_6_5z00(BgL_xz00_5053));} 

}



/* cos */
BGL_EXPORTED_DEF double BGl_cosz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_117)
{
{ /* Ieee/number.scm 1044 */
if(
REALP(BgL_xz00_117))
{ /* Ieee/number.scm 1046 */
return 
cos(
REAL_TO_DOUBLE(BgL_xz00_117));}  else 
{ /* Ieee/number.scm 1046 */
if(
INTEGERP(BgL_xz00_117))
{ /* Ieee/number.scm 1047 */
return 
cos(
(double)(
(long)CINT(BgL_xz00_117)));}  else 
{ /* Ieee/number.scm 1047 */
if(
ELONGP(BgL_xz00_117))
{ /* Ieee/number.scm 1048 */
return 
cos(
(double)(
BELONG_TO_LONG(BgL_xz00_117)));}  else 
{ /* Ieee/number.scm 1048 */
if(
LLONGP(BgL_xz00_117))
{ /* Ieee/number.scm 1049 */
return 
cos(
(double)(
BLLONG_TO_LLONG(BgL_xz00_117)));}  else 
{ /* Ieee/number.scm 1049 */
if(
BIGNUMP(BgL_xz00_117))
{ /* Ieee/number.scm 1050 */
return 
cos(
bgl_bignum_to_flonum(BgL_xz00_117));}  else 
{ /* Ieee/number.scm 1051 */
 obj_t BgL_tmpz00_10738;
{ /* Ieee/number.scm 1051 */
 obj_t BgL_aux3668z00_5489;
BgL_aux3668z00_5489 = 
BGl_errorz00zz__errorz00(BGl_string3790z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_117); 
if(
REALP(BgL_aux3668z00_5489))
{ /* Ieee/number.scm 1051 */
BgL_tmpz00_10738 = BgL_aux3668z00_5489
; }  else 
{ 
 obj_t BgL_auxz00_10742;
BgL_auxz00_10742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(38621L), BGl_string3790z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3668z00_5489); 
FAILURE(BgL_auxz00_10742,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10738);} } } } } } 

}



/* &cos */
obj_t BGl_z62cosz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5054, obj_t BgL_xz00_5055)
{
{ /* Ieee/number.scm 1044 */
return 
DOUBLE_TO_REAL(
BGl_cosz00zz__r4_numbers_6_5z00(BgL_xz00_5055));} 

}



/* tan */
BGL_EXPORTED_DEF double BGl_tanz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_118)
{
{ /* Ieee/number.scm 1056 */
if(
REALP(BgL_xz00_118))
{ /* Ieee/number.scm 1058 */
return 
tan(
REAL_TO_DOUBLE(BgL_xz00_118));}  else 
{ /* Ieee/number.scm 1058 */
if(
INTEGERP(BgL_xz00_118))
{ /* Ieee/number.scm 1059 */
return 
tan(
(double)(
(long)CINT(BgL_xz00_118)));}  else 
{ /* Ieee/number.scm 1059 */
if(
ELONGP(BgL_xz00_118))
{ /* Ieee/number.scm 1060 */
return 
tan(
(double)(
BELONG_TO_LONG(BgL_xz00_118)));}  else 
{ /* Ieee/number.scm 1060 */
if(
LLONGP(BgL_xz00_118))
{ /* Ieee/number.scm 1061 */
return 
tan(
(double)(
BLLONG_TO_LLONG(BgL_xz00_118)));}  else 
{ /* Ieee/number.scm 1061 */
if(
BIGNUMP(BgL_xz00_118))
{ /* Ieee/number.scm 1062 */
return 
tan(
bgl_bignum_to_flonum(BgL_xz00_118));}  else 
{ /* Ieee/number.scm 1063 */
 obj_t BgL_tmpz00_10772;
{ /* Ieee/number.scm 1063 */
 obj_t BgL_aux3670z00_5491;
BgL_aux3670z00_5491 = 
BGl_errorz00zz__errorz00(BGl_string3791z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_118); 
if(
REALP(BgL_aux3670z00_5491))
{ /* Ieee/number.scm 1063 */
BgL_tmpz00_10772 = BgL_aux3670z00_5491
; }  else 
{ 
 obj_t BgL_auxz00_10776;
BgL_auxz00_10776 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(39132L), BGl_string3791z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3670z00_5491); 
FAILURE(BgL_auxz00_10776,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10772);} } } } } } 

}



/* &tan */
obj_t BGl_z62tanz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5056, obj_t BgL_xz00_5057)
{
{ /* Ieee/number.scm 1056 */
return 
DOUBLE_TO_REAL(
BGl_tanz00zz__r4_numbers_6_5z00(BgL_xz00_5057));} 

}



/* asin */
BGL_EXPORTED_DEF double BGl_asinz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_119)
{
{ /* Ieee/number.scm 1068 */
if(
REALP(BgL_xz00_119))
{ /* Ieee/number.scm 1070 */
return 
asin(
REAL_TO_DOUBLE(BgL_xz00_119));}  else 
{ /* Ieee/number.scm 1070 */
if(
INTEGERP(BgL_xz00_119))
{ /* Ieee/number.scm 1071 */
return 
asin(
(double)(
(long)CINT(BgL_xz00_119)));}  else 
{ /* Ieee/number.scm 1071 */
if(
ELONGP(BgL_xz00_119))
{ /* Ieee/number.scm 1072 */
return 
asin(
(double)(
BELONG_TO_LONG(BgL_xz00_119)));}  else 
{ /* Ieee/number.scm 1072 */
if(
LLONGP(BgL_xz00_119))
{ /* Ieee/number.scm 1073 */
return 
asin(
(double)(
BLLONG_TO_LLONG(BgL_xz00_119)));}  else 
{ /* Ieee/number.scm 1073 */
if(
BIGNUMP(BgL_xz00_119))
{ /* Ieee/number.scm 1074 */
return 
asin(
bgl_bignum_to_flonum(BgL_xz00_119));}  else 
{ /* Ieee/number.scm 1075 */
 obj_t BgL_tmpz00_10806;
{ /* Ieee/number.scm 1075 */
 obj_t BgL_aux3672z00_5493;
BgL_aux3672z00_5493 = 
BGl_errorz00zz__errorz00(BGl_string3792z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_119); 
if(
REALP(BgL_aux3672z00_5493))
{ /* Ieee/number.scm 1075 */
BgL_tmpz00_10806 = BgL_aux3672z00_5493
; }  else 
{ 
 obj_t BgL_auxz00_10810;
BgL_auxz00_10810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(39649L), BGl_string3792z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3672z00_5493); 
FAILURE(BgL_auxz00_10810,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10806);} } } } } } 

}



/* &asin */
obj_t BGl_z62asinz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5058, obj_t BgL_xz00_5059)
{
{ /* Ieee/number.scm 1068 */
return 
DOUBLE_TO_REAL(
BGl_asinz00zz__r4_numbers_6_5z00(BgL_xz00_5059));} 

}



/* acos */
BGL_EXPORTED_DEF double BGl_acosz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_120)
{
{ /* Ieee/number.scm 1080 */
if(
REALP(BgL_xz00_120))
{ /* Ieee/number.scm 1082 */
return 
acos(
REAL_TO_DOUBLE(BgL_xz00_120));}  else 
{ /* Ieee/number.scm 1082 */
if(
INTEGERP(BgL_xz00_120))
{ /* Ieee/number.scm 1083 */
return 
acos(
(double)(
(long)CINT(BgL_xz00_120)));}  else 
{ /* Ieee/number.scm 1083 */
if(
ELONGP(BgL_xz00_120))
{ /* Ieee/number.scm 1084 */
return 
acos(
(double)(
BELONG_TO_LONG(BgL_xz00_120)));}  else 
{ /* Ieee/number.scm 1084 */
if(
LLONGP(BgL_xz00_120))
{ /* Ieee/number.scm 1085 */
return 
acos(
(double)(
BLLONG_TO_LLONG(BgL_xz00_120)));}  else 
{ /* Ieee/number.scm 1085 */
if(
BIGNUMP(BgL_xz00_120))
{ /* Ieee/number.scm 1086 */
return 
acos(
bgl_bignum_to_flonum(BgL_xz00_120));}  else 
{ /* Ieee/number.scm 1087 */
 obj_t BgL_tmpz00_10840;
{ /* Ieee/number.scm 1087 */
 obj_t BgL_aux3674z00_5495;
BgL_aux3674z00_5495 = 
BGl_errorz00zz__errorz00(BGl_string3793z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_120); 
if(
REALP(BgL_aux3674z00_5495))
{ /* Ieee/number.scm 1087 */
BgL_tmpz00_10840 = BgL_aux3674z00_5495
; }  else 
{ 
 obj_t BgL_auxz00_10844;
BgL_auxz00_10844 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40167L), BGl_string3793z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3674z00_5495); 
FAILURE(BgL_auxz00_10844,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10840);} } } } } } 

}



/* &acos */
obj_t BGl_z62acosz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5060, obj_t BgL_xz00_5061)
{
{ /* Ieee/number.scm 1080 */
return 
DOUBLE_TO_REAL(
BGl_acosz00zz__r4_numbers_6_5z00(BgL_xz00_5061));} 

}



/* atan */
BGL_EXPORTED_DEF double BGl_atanz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_121, obj_t BgL_yz00_122)
{
{ /* Ieee/number.scm 1092 */
{ /* Ieee/number.scm 1093 */
 obj_t BgL_yz00_2300;
if(
PAIRP(BgL_yz00_122))
{ /* Ieee/number.scm 1094 */
 obj_t BgL_yz00_2316;
BgL_yz00_2316 = 
CAR(BgL_yz00_122); 
if(
INTEGERP(BgL_yz00_2316))
{ /* Ieee/number.scm 1096 */
BgL_yz00_2300 = 
DOUBLE_TO_REAL(
(double)(
(long)CINT(BgL_yz00_2316))); }  else 
{ /* Ieee/number.scm 1096 */
if(
REALP(BgL_yz00_2316))
{ /* Ieee/number.scm 1097 */
BgL_yz00_2300 = BgL_yz00_2316; }  else 
{ /* Ieee/number.scm 1097 */
BgL_yz00_2300 = 
BGl_errorz00zz__errorz00(BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_2316); } } }  else 
{ /* Ieee/number.scm 1093 */
BgL_yz00_2300 = BFALSE; } 
if(
REALP(BgL_xz00_121))
{ /* Ieee/number.scm 1105 */
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
 double BgL_xz00_4418; double BgL_yz00_4419;
BgL_xz00_4418 = 
REAL_TO_DOUBLE(BgL_xz00_121); 
{ /* Ieee/number.scm 1102 */
 obj_t BgL_tmpz00_10867;
if(
REALP(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
BgL_tmpz00_10867 = BgL_yz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_10870;
BgL_auxz00_10870 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40680L), BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_yz00_2300); 
FAILURE(BgL_auxz00_10870,BFALSE,BFALSE);} 
BgL_yz00_4419 = 
REAL_TO_DOUBLE(BgL_tmpz00_10867); } 
{ /* Ieee/number.scm 1102 */
 bool_t BgL_test4873z00_10875;
if(
(BgL_xz00_4418==((double)0.0)))
{ /* Ieee/number.scm 1102 */
BgL_test4873z00_10875 = 
(BgL_yz00_4419==((double)0.0))
; }  else 
{ /* Ieee/number.scm 1102 */
BgL_test4873z00_10875 = ((bool_t)0)
; } 
if(BgL_test4873z00_10875)
{ /* Ieee/number.scm 1102 */
 obj_t BgL_procz00_4422; obj_t BgL_msgz00_4423; obj_t BgL_objz00_4424;
BgL_procz00_4422 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3795z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4423 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4424 = BGL_REAL_CNST( BGl_real3797z00zz__r4_numbers_6_5z00); 
the_failure(BgL_procz00_4422, BgL_msgz00_4423, BgL_objz00_4424); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1102 */
return 
atan2(BgL_xz00_4418, BgL_yz00_4419);} } }  else 
{ /* Ieee/number.scm 1101 */
return 
atan(
REAL_TO_DOUBLE(BgL_xz00_121));} }  else 
{ /* Ieee/number.scm 1105 */
if(
INTEGERP(BgL_xz00_121))
{ /* Ieee/number.scm 1106 */
 double BgL_arg2664z00_2304;
BgL_arg2664z00_2304 = 
(double)(
(long)CINT(BgL_xz00_121)); 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
 double BgL_yz00_4432;
{ /* Ieee/number.scm 1102 */
 obj_t BgL_tmpz00_10893;
if(
REALP(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
BgL_tmpz00_10893 = BgL_yz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_10896;
BgL_auxz00_10896 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40680L), BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_yz00_2300); 
FAILURE(BgL_auxz00_10896,BFALSE,BFALSE);} 
BgL_yz00_4432 = 
REAL_TO_DOUBLE(BgL_tmpz00_10893); } 
{ /* Ieee/number.scm 1102 */
 bool_t BgL_test4878z00_10901;
if(
(BgL_arg2664z00_2304==((double)0.0)))
{ /* Ieee/number.scm 1102 */
BgL_test4878z00_10901 = 
(BgL_yz00_4432==((double)0.0))
; }  else 
{ /* Ieee/number.scm 1102 */
BgL_test4878z00_10901 = ((bool_t)0)
; } 
if(BgL_test4878z00_10901)
{ /* Ieee/number.scm 1102 */
 obj_t BgL_procz00_4435; obj_t BgL_msgz00_4436; obj_t BgL_objz00_4437;
BgL_procz00_4435 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3795z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4436 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4437 = BGL_REAL_CNST( BGl_real3797z00zz__r4_numbers_6_5z00); 
the_failure(BgL_procz00_4435, BgL_msgz00_4436, BgL_objz00_4437); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1102 */
return 
atan2(BgL_arg2664z00_2304, BgL_yz00_4432);} } }  else 
{ /* Ieee/number.scm 1101 */
return 
atan(BgL_arg2664z00_2304);} }  else 
{ /* Ieee/number.scm 1106 */
if(
ELONGP(BgL_xz00_121))
{ /* Ieee/number.scm 1107 */
 double BgL_arg2666z00_2306;
BgL_arg2666z00_2306 = 
(double)(
BELONG_TO_LONG(BgL_xz00_121)); 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
 double BgL_yz00_4445;
{ /* Ieee/number.scm 1102 */
 obj_t BgL_tmpz00_10918;
if(
REALP(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
BgL_tmpz00_10918 = BgL_yz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_10921;
BgL_auxz00_10921 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40680L), BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_yz00_2300); 
FAILURE(BgL_auxz00_10921,BFALSE,BFALSE);} 
BgL_yz00_4445 = 
REAL_TO_DOUBLE(BgL_tmpz00_10918); } 
{ /* Ieee/number.scm 1102 */
 bool_t BgL_test4883z00_10926;
if(
(BgL_arg2666z00_2306==((double)0.0)))
{ /* Ieee/number.scm 1102 */
BgL_test4883z00_10926 = 
(BgL_yz00_4445==((double)0.0))
; }  else 
{ /* Ieee/number.scm 1102 */
BgL_test4883z00_10926 = ((bool_t)0)
; } 
if(BgL_test4883z00_10926)
{ /* Ieee/number.scm 1102 */
 obj_t BgL_procz00_4448; obj_t BgL_msgz00_4449; obj_t BgL_objz00_4450;
BgL_procz00_4448 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3795z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4449 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4450 = BGL_REAL_CNST( BGl_real3797z00zz__r4_numbers_6_5z00); 
the_failure(BgL_procz00_4448, BgL_msgz00_4449, BgL_objz00_4450); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1102 */
return 
atan2(BgL_arg2666z00_2306, BgL_yz00_4445);} } }  else 
{ /* Ieee/number.scm 1101 */
return 
atan(BgL_arg2666z00_2306);} }  else 
{ /* Ieee/number.scm 1107 */
if(
LLONGP(BgL_xz00_121))
{ /* Ieee/number.scm 1108 */
 double BgL_arg2669z00_2308;
BgL_arg2669z00_2308 = 
(double)(
BLLONG_TO_LLONG(BgL_xz00_121)); 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
 double BgL_yz00_4458;
{ /* Ieee/number.scm 1102 */
 obj_t BgL_tmpz00_10943;
if(
REALP(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
BgL_tmpz00_10943 = BgL_yz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_10946;
BgL_auxz00_10946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40680L), BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_yz00_2300); 
FAILURE(BgL_auxz00_10946,BFALSE,BFALSE);} 
BgL_yz00_4458 = 
REAL_TO_DOUBLE(BgL_tmpz00_10943); } 
{ /* Ieee/number.scm 1102 */
 bool_t BgL_test4890z00_10951;
if(
(BgL_arg2669z00_2308==((double)0.0)))
{ /* Ieee/number.scm 1102 */
BgL_test4890z00_10951 = 
(BgL_yz00_4458==((double)0.0))
; }  else 
{ /* Ieee/number.scm 1102 */
BgL_test4890z00_10951 = ((bool_t)0)
; } 
if(BgL_test4890z00_10951)
{ /* Ieee/number.scm 1102 */
 obj_t BgL_procz00_4461; obj_t BgL_msgz00_4462; obj_t BgL_objz00_4463;
BgL_procz00_4461 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3795z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4462 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4463 = BGL_REAL_CNST( BGl_real3797z00zz__r4_numbers_6_5z00); 
the_failure(BgL_procz00_4461, BgL_msgz00_4462, BgL_objz00_4463); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1102 */
return 
atan2(BgL_arg2669z00_2308, BgL_yz00_4458);} } }  else 
{ /* Ieee/number.scm 1101 */
return 
atan(BgL_arg2669z00_2308);} }  else 
{ /* Ieee/number.scm 1108 */
if(
BIGNUMP(BgL_xz00_121))
{ /* Ieee/number.scm 1109 */
 double BgL_arg2672z00_2310;
BgL_arg2672z00_2310 = 
bgl_bignum_to_flonum(BgL_xz00_121); 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
 double BgL_yz00_4472;
{ /* Ieee/number.scm 1102 */
 obj_t BgL_tmpz00_10967;
if(
REALP(BgL_yz00_2300))
{ /* Ieee/number.scm 1102 */
BgL_tmpz00_10967 = BgL_yz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_10970;
BgL_auxz00_10970 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40680L), BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_yz00_2300); 
FAILURE(BgL_auxz00_10970,BFALSE,BFALSE);} 
BgL_yz00_4472 = 
REAL_TO_DOUBLE(BgL_tmpz00_10967); } 
{ /* Ieee/number.scm 1102 */
 bool_t BgL_test4896z00_10975;
if(
(BgL_arg2672z00_2310==((double)0.0)))
{ /* Ieee/number.scm 1102 */
BgL_test4896z00_10975 = 
(BgL_yz00_4472==((double)0.0))
; }  else 
{ /* Ieee/number.scm 1102 */
BgL_test4896z00_10975 = ((bool_t)0)
; } 
if(BgL_test4896z00_10975)
{ /* Ieee/number.scm 1102 */
 obj_t BgL_procz00_4475; obj_t BgL_msgz00_4476; obj_t BgL_objz00_4477;
BgL_procz00_4475 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3795z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4476 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4477 = BGL_REAL_CNST( BGl_real3797z00zz__r4_numbers_6_5z00); 
the_failure(BgL_procz00_4475, BgL_msgz00_4476, BgL_objz00_4477); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1102 */
return 
atan2(BgL_arg2672z00_2310, BgL_yz00_4472);} } }  else 
{ /* Ieee/number.scm 1101 */
return 
atan(BgL_arg2672z00_2310);} }  else 
{ /* Ieee/number.scm 1110 */
 obj_t BgL_tmpz00_10986;
{ /* Ieee/number.scm 1110 */
 obj_t BgL_aux3686z00_5507;
BgL_aux3686z00_5507 = 
BGl_errorz00zz__errorz00(BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_121); 
if(
REALP(BgL_aux3686z00_5507))
{ /* Ieee/number.scm 1110 */
BgL_tmpz00_10986 = BgL_aux3686z00_5507
; }  else 
{ 
 obj_t BgL_auxz00_10990;
BgL_auxz00_10990 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(40949L), BGl_string3794z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3686z00_5507); 
FAILURE(BgL_auxz00_10990,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_10986);} } } } } } } 

}



/* &atan */
obj_t BGl_z62atanz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5062, obj_t BgL_xz00_5063, obj_t BgL_yz00_5064)
{
{ /* Ieee/number.scm 1092 */
return 
DOUBLE_TO_REAL(
BGl_atanz00zz__r4_numbers_6_5z00(BgL_xz00_5063, BgL_yz00_5064));} 

}



/* sqrt */
BGL_EXPORTED_DEF double BGl_sqrtz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_123)
{
{ /* Ieee/number.scm 1115 */
if(
INTEGERP(BgL_xz00_123))
{ /* Ieee/number.scm 1117 */
 double BgL_arg2680z00_2320;
BgL_arg2680z00_2320 = 
(double)(
(long)CINT(BgL_xz00_123)); 
if(
(BgL_arg2680z00_2320<((double)0.0)))
{ /* Ieee/number.scm 1117 */
 obj_t BgL_procz00_4485; obj_t BgL_msgz00_4486; obj_t BgL_objz00_4487;
BgL_procz00_4485 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3798z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4486 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4487 = 
DOUBLE_TO_REAL(BgL_arg2680z00_2320); 
BGl_errorz00zz__errorz00(BgL_procz00_4485, BgL_msgz00_4486, BgL_objz00_4487); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1117 */
return 
sqrt(BgL_arg2680z00_2320);} }  else 
{ /* Ieee/number.scm 1117 */
if(
REALP(BgL_xz00_123))
{ /* Ieee/number.scm 1118 */
 double BgL_rz00_4490;
BgL_rz00_4490 = 
REAL_TO_DOUBLE(BgL_xz00_123); 
if(
(BgL_rz00_4490<((double)0.0)))
{ /* Ieee/number.scm 1118 */
 obj_t BgL_procz00_4492; obj_t BgL_msgz00_4493; obj_t BgL_objz00_4494;
BgL_procz00_4492 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3798z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4493 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4494 = 
DOUBLE_TO_REAL(BgL_rz00_4490); 
BGl_errorz00zz__errorz00(BgL_procz00_4492, BgL_msgz00_4493, BgL_objz00_4494); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1118 */
return 
sqrt(BgL_rz00_4490);} }  else 
{ /* Ieee/number.scm 1118 */
if(
ELONGP(BgL_xz00_123))
{ /* Ieee/number.scm 1119 */
 double BgL_arg2684z00_2323;
BgL_arg2684z00_2323 = 
(double)(
BELONG_TO_LONG(BgL_xz00_123)); 
if(
(BgL_arg2684z00_2323<((double)0.0)))
{ /* Ieee/number.scm 1119 */
 obj_t BgL_procz00_4499; obj_t BgL_msgz00_4500; obj_t BgL_objz00_4501;
BgL_procz00_4499 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3798z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4500 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4501 = 
DOUBLE_TO_REAL(BgL_arg2684z00_2323); 
BGl_errorz00zz__errorz00(BgL_procz00_4499, BgL_msgz00_4500, BgL_objz00_4501); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1119 */
return 
sqrt(BgL_arg2684z00_2323);} }  else 
{ /* Ieee/number.scm 1119 */
if(
LLONGP(BgL_xz00_123))
{ /* Ieee/number.scm 1120 */
 double BgL_arg2686z00_2325;
BgL_arg2686z00_2325 = 
(double)(
BLLONG_TO_LLONG(BgL_xz00_123)); 
if(
(BgL_arg2686z00_2325<((double)0.0)))
{ /* Ieee/number.scm 1120 */
 obj_t BgL_procz00_4506; obj_t BgL_msgz00_4507; obj_t BgL_objz00_4508;
BgL_procz00_4506 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3798z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4507 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4508 = 
DOUBLE_TO_REAL(BgL_arg2686z00_2325); 
BGl_errorz00zz__errorz00(BgL_procz00_4506, BgL_msgz00_4507, BgL_objz00_4508); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1120 */
return 
sqrt(BgL_arg2686z00_2325);} }  else 
{ /* Ieee/number.scm 1120 */
if(
BIGNUMP(BgL_xz00_123))
{ /* Ieee/number.scm 1121 */
 double BgL_arg2688z00_2327;
BgL_arg2688z00_2327 = 
bgl_bignum_to_flonum(BgL_xz00_123); 
if(
(BgL_arg2688z00_2327<((double)0.0)))
{ /* Ieee/number.scm 1121 */
 obj_t BgL_procz00_4514; obj_t BgL_msgz00_4515; obj_t BgL_objz00_4516;
BgL_procz00_4514 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3798z00zz__r4_numbers_6_5z00)); 
BgL_msgz00_4515 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string3796z00zz__r4_numbers_6_5z00)); 
BgL_objz00_4516 = 
DOUBLE_TO_REAL(BgL_arg2688z00_2327); 
BGl_errorz00zz__errorz00(BgL_procz00_4514, BgL_msgz00_4515, BgL_objz00_4516); 
return ((double)0.0);}  else 
{ /* Ieee/number.scm 1121 */
return 
sqrt(BgL_arg2688z00_2327);} }  else 
{ /* Ieee/number.scm 1122 */
 obj_t BgL_tmpz00_11060;
{ /* Ieee/number.scm 1122 */
 obj_t BgL_aux3688z00_5509;
BgL_aux3688z00_5509 = 
BGl_errorz00zz__errorz00(BGl_string3799z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_123); 
if(
REALP(BgL_aux3688z00_5509))
{ /* Ieee/number.scm 1122 */
BgL_tmpz00_11060 = BgL_aux3688z00_5509
; }  else 
{ 
 obj_t BgL_auxz00_11064;
BgL_auxz00_11064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(41468L), BGl_string3799z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_aux3688z00_5509); 
FAILURE(BgL_auxz00_11064,BFALSE,BFALSE);} } 
return 
REAL_TO_DOUBLE(BgL_tmpz00_11060);} } } } } } 

}



/* &sqrt */
obj_t BGl_z62sqrtz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5065, obj_t BgL_xz00_5066)
{
{ /* Ieee/number.scm 1115 */
return 
DOUBLE_TO_REAL(
BGl_sqrtz00zz__r4_numbers_6_5z00(BgL_xz00_5066));} 

}



/* expt */
BGL_EXPORTED_DEF obj_t BGl_exptz00zz__r4_numbers_6_5z00(obj_t BgL_xz00_124, obj_t BgL_yz00_125)
{
{ /* Ieee/number.scm 1127 */
{ /* Ieee/number.scm 1129 */
 bool_t BgL_test4910z00_11071;
if(
REALP(BgL_xz00_124))
{ /* Ieee/number.scm 1129 */
if(
REALP(BgL_yz00_125))
{ /* Ieee/number.scm 1129 */
if(
(
REAL_TO_DOUBLE(BgL_xz00_124)==((double)0.0)))
{ /* Ieee/number.scm 1129 */
BgL_test4910z00_11071 = 
(
REAL_TO_DOUBLE(BgL_yz00_125)==((double)0.0))
; }  else 
{ /* Ieee/number.scm 1129 */
BgL_test4910z00_11071 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1129 */
BgL_test4910z00_11071 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1129 */
BgL_test4910z00_11071 = ((bool_t)0)
; } 
if(BgL_test4910z00_11071)
{ /* Ieee/number.scm 1129 */
return BGL_REAL_CNST( BGl_real3800z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1131 */
 bool_t BgL_test4914z00_11081;
if(
INTEGERP(BgL_xz00_124))
{ /* Ieee/number.scm 1131 */
if(
INTEGERP(BgL_yz00_125))
{ /* Ieee/number.scm 1131 */
BgL_test4914z00_11081 = 
(
(long)CINT(BgL_yz00_125)>=0L)
; }  else 
{ /* Ieee/number.scm 1131 */
BgL_test4914z00_11081 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1131 */
BgL_test4914z00_11081 = ((bool_t)0)
; } 
if(BgL_test4914z00_11081)
{ /* Ieee/number.scm 1131 */
return 
BINT(
BGl_exptfxz00zz__r4_numbers_6_5_fixnumz00(
(long)CINT(BgL_xz00_124), 
(long)CINT(BgL_yz00_125)));}  else 
{ /* Ieee/number.scm 1133 */
 bool_t BgL_test4917z00_11092;
if(
BIGNUMP(BgL_xz00_124))
{ /* Ieee/number.scm 1133 */
if(
BIGNUMP(BgL_yz00_125))
{ /* Ieee/number.scm 1133 */
BgL_test4917z00_11092 = 
BXPOSITIVE(BgL_yz00_125)
; }  else 
{ /* Ieee/number.scm 1133 */
BgL_test4917z00_11092 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1133 */
BgL_test4917z00_11092 = ((bool_t)0)
; } 
if(BgL_test4917z00_11092)
{ /* Ieee/number.scm 1133 */
return 
bgl_bignum_expt(BgL_xz00_124, BgL_yz00_125);}  else 
{ /* Ieee/number.scm 1133 */
if(
BIGNUMP(BgL_xz00_124))
{ /* Ieee/number.scm 1136 */
 obj_t BgL_y1z00_2339;
if(
REALP(BgL_yz00_125))
{ /* Ieee/number.scm 1137 */
BgL_y1z00_2339 = 
bgl_long_to_bignum(
(long)(
REAL_TO_DOUBLE(BgL_yz00_125))); }  else 
{ /* Ieee/number.scm 1137 */
if(
INTEGERP(BgL_yz00_125))
{ /* Ieee/number.scm 1138 */
BgL_y1z00_2339 = 
bgl_long_to_bignum(
(long)CINT(BgL_yz00_125)); }  else 
{ /* Ieee/number.scm 1138 */
if(
ELONGP(BgL_yz00_125))
{ /* Ieee/number.scm 1139 */
BgL_y1z00_2339 = 
bgl_long_to_bignum(
BELONG_TO_LONG(BgL_yz00_125)); }  else 
{ /* Ieee/number.scm 1139 */
if(
LLONGP(BgL_yz00_125))
{ /* Ieee/number.scm 1140 */
BgL_y1z00_2339 = 
bgl_llong_to_bignum(
BLLONG_TO_LLONG(BgL_yz00_125)); }  else 
{ /* Ieee/number.scm 1140 */
if(
BIGNUMP(BgL_yz00_125))
{ /* Ieee/number.scm 1141 */
BgL_y1z00_2339 = BgL_yz00_125; }  else 
{ /* Ieee/number.scm 1141 */
BgL_y1z00_2339 = 
BGl_errorz00zz__errorz00(BGl_string3801z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_125); } } } } } 
{ /* Ieee/number.scm 1143 */
 obj_t BgL_yz00_4528;
if(
BIGNUMP(BgL_y1z00_2339))
{ /* Ieee/number.scm 1143 */
BgL_yz00_4528 = BgL_y1z00_2339; }  else 
{ 
 obj_t BgL_auxz00_11123;
BgL_auxz00_11123 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(42269L), BGl_string3801z00zz__r4_numbers_6_5z00, BGl_string3732z00zz__r4_numbers_6_5z00, BgL_y1z00_2339); 
FAILURE(BgL_auxz00_11123,BFALSE,BFALSE);} 
return 
bgl_bignum_expt(BgL_xz00_124, BgL_yz00_4528);} }  else 
{ /* Ieee/number.scm 1145 */
 obj_t BgL_x1z00_2346; obj_t BgL_y1z00_2347;
if(
REALP(BgL_xz00_124))
{ /* Ieee/number.scm 1146 */
BgL_x1z00_2346 = BgL_xz00_124; }  else 
{ /* Ieee/number.scm 1146 */
if(
INTEGERP(BgL_xz00_124))
{ /* Ieee/number.scm 1147 */
BgL_x1z00_2346 = 
DOUBLE_TO_REAL(
(double)(
(long)CINT(BgL_xz00_124))); }  else 
{ /* Ieee/number.scm 1147 */
if(
ELONGP(BgL_xz00_124))
{ /* Ieee/number.scm 1148 */
BgL_x1z00_2346 = 
DOUBLE_TO_REAL(
(double)(
BELONG_TO_LONG(BgL_xz00_124))); }  else 
{ /* Ieee/number.scm 1148 */
if(
LLONGP(BgL_xz00_124))
{ /* Ieee/number.scm 1149 */
BgL_x1z00_2346 = 
DOUBLE_TO_REAL(
(double)(
BLLONG_TO_LLONG(BgL_xz00_124))); }  else 
{ /* Ieee/number.scm 1149 */
if(
BIGNUMP(BgL_xz00_124))
{ /* Ieee/number.scm 1150 */
BgL_x1z00_2346 = 
DOUBLE_TO_REAL(
bgl_bignum_to_flonum(BgL_xz00_124)); }  else 
{ /* Ieee/number.scm 1150 */
BgL_x1z00_2346 = 
BGl_errorz00zz__errorz00(BGl_string3801z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_xz00_124); } } } } } 
if(
REALP(BgL_yz00_125))
{ /* Ieee/number.scm 1153 */
BgL_y1z00_2347 = BgL_yz00_125; }  else 
{ /* Ieee/number.scm 1153 */
if(
INTEGERP(BgL_yz00_125))
{ /* Ieee/number.scm 1154 */
BgL_y1z00_2347 = 
DOUBLE_TO_REAL(
(double)(
(long)CINT(BgL_yz00_125))); }  else 
{ /* Ieee/number.scm 1154 */
if(
ELONGP(BgL_yz00_125))
{ /* Ieee/number.scm 1155 */
BgL_y1z00_2347 = 
DOUBLE_TO_REAL(
(double)(
BELONG_TO_LONG(BgL_yz00_125))); }  else 
{ /* Ieee/number.scm 1155 */
if(
LLONGP(BgL_yz00_125))
{ /* Ieee/number.scm 1156 */
BgL_y1z00_2347 = 
DOUBLE_TO_REAL(
(double)(
BLLONG_TO_LLONG(BgL_yz00_125))); }  else 
{ /* Ieee/number.scm 1156 */
if(
BIGNUMP(BgL_yz00_125))
{ /* Ieee/number.scm 1157 */
BgL_y1z00_2347 = 
DOUBLE_TO_REAL(
bgl_bignum_to_flonum(BgL_yz00_125)); }  else 
{ /* Ieee/number.scm 1157 */
BgL_y1z00_2347 = 
BGl_errorz00zz__errorz00(BGl_string3801z00zz__r4_numbers_6_5z00, BGl_string3751z00zz__r4_numbers_6_5z00, BgL_yz00_125); } } } } } 
{ /* Ieee/number.scm 1159 */
 double BgL_r1z00_4531; double BgL_r2z00_4532;
{ /* Ieee/number.scm 1159 */
 obj_t BgL_tmpz00_11172;
if(
REALP(BgL_x1z00_2346))
{ /* Ieee/number.scm 1159 */
BgL_tmpz00_11172 = BgL_x1z00_2346
; }  else 
{ 
 obj_t BgL_auxz00_11175;
BgL_auxz00_11175 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(42786L), BGl_string3801z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_x1z00_2346); 
FAILURE(BgL_auxz00_11175,BFALSE,BFALSE);} 
BgL_r1z00_4531 = 
REAL_TO_DOUBLE(BgL_tmpz00_11172); } 
{ /* Ieee/number.scm 1159 */
 obj_t BgL_tmpz00_11180;
if(
REALP(BgL_y1z00_2347))
{ /* Ieee/number.scm 1159 */
BgL_tmpz00_11180 = BgL_y1z00_2347
; }  else 
{ 
 obj_t BgL_auxz00_11183;
BgL_auxz00_11183 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(42789L), BGl_string3801z00zz__r4_numbers_6_5z00, BGl_string3721z00zz__r4_numbers_6_5z00, BgL_y1z00_2347); 
FAILURE(BgL_auxz00_11183,BFALSE,BFALSE);} 
BgL_r2z00_4532 = 
REAL_TO_DOUBLE(BgL_tmpz00_11180); } 
return 
DOUBLE_TO_REAL(
pow(BgL_r1z00_4531, BgL_r2z00_4532));} } } } } } } 

}



/* &expt */
obj_t BGl_z62exptz62zz__r4_numbers_6_5z00(obj_t BgL_envz00_5067, obj_t BgL_xz00_5068, obj_t BgL_yz00_5069)
{
{ /* Ieee/number.scm 1127 */
return 
BGl_exptz00zz__r4_numbers_6_5z00(BgL_xz00_5068, BgL_yz00_5069);} 

}



/* exact->inexact */
BGL_EXPORTED_DEF obj_t bgl_exact_to_inexact(obj_t BgL_za7za7_126)
{
{ /* Ieee/number.scm 1164 */
if(
INTEGERP(BgL_za7za7_126))
{ /* Ieee/number.scm 1166 */
return 
DOUBLE_TO_REAL(
(double)(
(long)CINT(BgL_za7za7_126)));}  else 
{ /* Ieee/number.scm 1166 */
if(
REALP(BgL_za7za7_126))
{ /* Ieee/number.scm 1167 */
return BgL_za7za7_126;}  else 
{ /* Ieee/number.scm 1167 */
if(
ELONGP(BgL_za7za7_126))
{ /* Ieee/number.scm 1168 */
return 
DOUBLE_TO_REAL(
(double)(
BELONG_TO_LONG(BgL_za7za7_126)));}  else 
{ /* Ieee/number.scm 1168 */
if(
LLONGP(BgL_za7za7_126))
{ /* Ieee/number.scm 1169 */
return 
DOUBLE_TO_REAL(
(double)(
BLLONG_TO_LLONG(BgL_za7za7_126)));}  else 
{ /* Ieee/number.scm 1169 */
if(
BIGNUMP(BgL_za7za7_126))
{ /* Ieee/number.scm 1170 */
return 
DOUBLE_TO_REAL(
bgl_bignum_to_flonum(BgL_za7za7_126));}  else 
{ /* Ieee/number.scm 1170 */
return BgL_za7za7_126;} } } } } } 

}



/* &exact->inexact */
obj_t BGl_z62exactzd2ze3inexactz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5070, obj_t BgL_za7za7_5071)
{
{ /* Ieee/number.scm 1164 */
return 
bgl_exact_to_inexact(BgL_za7za7_5071);} 

}



/* inexact->exact */
BGL_EXPORTED_DEF obj_t bgl_inexact_to_exact(obj_t BgL_za7za7_127)
{
{ /* Ieee/number.scm 1182 */
if(
REALP(BgL_za7za7_127))
{ /* Ieee/number.scm 1184 */
 bool_t BgL_test4945z00_11215;
{ /* Ieee/number.scm 1184 */
 bool_t BgL_test4946z00_11216;
{ /* Ieee/number.scm 1184 */
 double BgL_r2z00_4535;
BgL_r2z00_4535 = BGl_za2minintflza2z00zz__r4_numbers_6_5z00; 
BgL_test4946z00_11216 = 
(
REAL_TO_DOUBLE(BgL_za7za7_127)>=BgL_r2z00_4535); } 
if(BgL_test4946z00_11216)
{ /* Ieee/number.scm 1184 */
 double BgL_r2z00_4537;
BgL_r2z00_4537 = BGl_za2maxintflza2z00zz__r4_numbers_6_5z00; 
BgL_test4945z00_11215 = 
(
REAL_TO_DOUBLE(BgL_za7za7_127)<=BgL_r2z00_4537); }  else 
{ /* Ieee/number.scm 1184 */
BgL_test4945z00_11215 = ((bool_t)0)
; } } 
if(BgL_test4945z00_11215)
{ /* Ieee/number.scm 1184 */
return 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_za7za7_127)));}  else 
{ /* Ieee/number.scm 1184 */
return 
bgl_flonum_to_bignum(
REAL_TO_DOUBLE(BgL_za7za7_127));} }  else 
{ /* Ieee/number.scm 1183 */
return BgL_za7za7_127;} } 

}



/* &inexact->exact */
obj_t BGl_z62inexactzd2ze3exactz53zz__r4_numbers_6_5z00(obj_t BgL_envz00_5072, obj_t BgL_za7za7_5073)
{
{ /* Ieee/number.scm 1182 */
return 
bgl_inexact_to_exact(BgL_za7za7_5073);} 

}



/* _number->string */
obj_t BGl__numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t BgL_env1080z00_132, obj_t BgL_opt1079z00_131)
{
{ /* Ieee/number.scm 1197 */
{ /* Ieee/number.scm 1197 */
 obj_t BgL_xz00_2375;
BgL_xz00_2375 = 
VECTOR_REF(BgL_opt1079z00_131,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1079z00_131)) { case 1L : 

{ /* Ieee/number.scm 1197 */

return 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_xz00_2375, 
BINT(10L));} break;case 2L : 

{ /* Ieee/number.scm 1197 */
 obj_t BgL_radixz00_2379;
BgL_radixz00_2379 = 
VECTOR_REF(BgL_opt1079z00_131,1L); 
{ /* Ieee/number.scm 1197 */

return 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_xz00_2375, BgL_radixz00_2379);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3802z00zz__r4_numbers_6_5z00, BGl_string3804z00zz__r4_numbers_6_5z00, 
BINT(
VECTOR_LENGTH(BgL_opt1079z00_131)));} } } } 

}



/* number->string */
BGL_EXPORTED_DEF obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t BgL_xz00_129, obj_t BgL_radixz00_130)
{
{ /* Ieee/number.scm 1197 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_radixz00_130))
{ /* Ieee/number.scm 1199 */
if(
INTEGERP(BgL_xz00_129))
{ /* Ieee/number.scm 1200 */
 long BgL_auxz00_11241;
{ /* Ieee/number.scm 1200 */
 obj_t BgL_tmpz00_11243;
if(
INTEGERP(BgL_radixz00_130))
{ /* Ieee/number.scm 1200 */
BgL_tmpz00_11243 = BgL_radixz00_130
; }  else 
{ 
 obj_t BgL_auxz00_11246;
BgL_auxz00_11246 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44441L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
FAILURE(BgL_auxz00_11246,BFALSE,BFALSE);} 
BgL_auxz00_11241 = 
(long)CINT(BgL_tmpz00_11243); } 
return 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(long)CINT(BgL_xz00_129), BgL_auxz00_11241);}  else 
{ /* Ieee/number.scm 1200 */
if(
REALP(BgL_xz00_129))
{ /* Ieee/number.scm 1201 */
return 
bgl_real_to_string(
REAL_TO_DOUBLE(BgL_xz00_129));}  else 
{ /* Ieee/number.scm 1201 */
if(
ELONGP(BgL_xz00_129))
{ /* Ieee/number.scm 1202 */
 obj_t BgL_list2730z00_2385;
BgL_list2730z00_2385 = 
MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL); 
return 
BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
BELONG_TO_LONG(BgL_xz00_129), BgL_list2730z00_2385);}  else 
{ /* Ieee/number.scm 1202 */
if(
LLONGP(BgL_xz00_129))
{ /* Ieee/number.scm 1203 */
 obj_t BgL_list2732z00_2387;
BgL_list2732z00_2387 = 
MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL); 
return 
BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
BLLONG_TO_LLONG(BgL_xz00_129), BgL_list2732z00_2387);}  else 
{ /* Ieee/number.scm 1203 */
if(
BIGNUMP(BgL_xz00_129))
{ /* Ieee/number.scm 1204 */
 long BgL_auxz00_11268;
{ /* Ieee/number.scm 1204 */
 obj_t BgL_tmpz00_11269;
if(
INTEGERP(BgL_radixz00_130))
{ /* Ieee/number.scm 1204 */
BgL_tmpz00_11269 = BgL_radixz00_130
; }  else 
{ 
 obj_t BgL_auxz00_11272;
BgL_auxz00_11272 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44609L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
FAILURE(BgL_auxz00_11272,BFALSE,BFALSE);} 
BgL_auxz00_11268 = 
(long)CINT(BgL_tmpz00_11269); } 
return 
BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_xz00_129, BgL_auxz00_11268);}  else 
{ /* Ieee/number.scm 1204 */
if(
BGL_INT8P(BgL_xz00_129))
{ /* Ieee/number.scm 1205 */
 long BgL_arg2736z00_2390;
{ /* Ieee/number.scm 1205 */
 int8_t BgL_xz00_4538;
BgL_xz00_4538 = 
BGL_BINT8_TO_INT8(BgL_xz00_129); 
{ /* Ieee/number.scm 1205 */
 long BgL_arg3252z00_4539;
BgL_arg3252z00_4539 = 
(long)(BgL_xz00_4538); 
BgL_arg2736z00_2390 = 
(long)(BgL_arg3252z00_4539); } } 
{ /* Ieee/number.scm 1205 */
 long BgL_auxz00_11283;
{ /* Ieee/number.scm 1205 */
 obj_t BgL_tmpz00_11284;
if(
INTEGERP(BgL_radixz00_130))
{ /* Ieee/number.scm 1205 */
BgL_tmpz00_11284 = BgL_radixz00_130
; }  else 
{ 
 obj_t BgL_auxz00_11287;
BgL_auxz00_11287 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44668L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
FAILURE(BgL_auxz00_11287,BFALSE,BFALSE);} 
BgL_auxz00_11283 = 
(long)CINT(BgL_tmpz00_11284); } 
return 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2736z00_2390, BgL_auxz00_11283);} }  else 
{ /* Ieee/number.scm 1205 */
if(
BGL_UINT8P(BgL_xz00_129))
{ /* Ieee/number.scm 1206 */
 long BgL_arg2738z00_2392;
{ /* Ieee/number.scm 1206 */
 uint8_t BgL_xz00_4541;
BgL_xz00_4541 = 
BGL_BUINT8_TO_UINT8(BgL_xz00_129); 
{ /* Ieee/number.scm 1206 */
 long BgL_arg3251z00_4542;
BgL_arg3251z00_4542 = 
(long)(BgL_xz00_4541); 
BgL_arg2738z00_2392 = 
(long)(BgL_arg3251z00_4542); } } 
{ /* Ieee/number.scm 1206 */
 long BgL_auxz00_11298;
{ /* Ieee/number.scm 1206 */
 obj_t BgL_tmpz00_11299;
if(
INTEGERP(BgL_radixz00_130))
{ /* Ieee/number.scm 1206 */
BgL_tmpz00_11299 = BgL_radixz00_130
; }  else 
{ 
 obj_t BgL_auxz00_11302;
BgL_auxz00_11302 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44729L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
FAILURE(BgL_auxz00_11302,BFALSE,BFALSE);} 
BgL_auxz00_11298 = 
(long)CINT(BgL_tmpz00_11299); } 
return 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2738z00_2392, BgL_auxz00_11298);} }  else 
{ /* Ieee/number.scm 1206 */
if(
BGL_INT16P(BgL_xz00_129))
{ /* Ieee/number.scm 1207 */
 long BgL_arg2740z00_2394;
{ /* Ieee/number.scm 1207 */
 int16_t BgL_xz00_4544;
BgL_xz00_4544 = 
BGL_BINT16_TO_INT16(BgL_xz00_129); 
{ /* Ieee/number.scm 1207 */
 long BgL_arg3250z00_4545;
BgL_arg3250z00_4545 = 
(long)(BgL_xz00_4544); 
BgL_arg2740z00_2394 = 
(long)(BgL_arg3250z00_4545); } } 
{ /* Ieee/number.scm 1207 */
 long BgL_auxz00_11313;
{ /* Ieee/number.scm 1207 */
 obj_t BgL_tmpz00_11314;
if(
INTEGERP(BgL_radixz00_130))
{ /* Ieee/number.scm 1207 */
BgL_tmpz00_11314 = BgL_radixz00_130
; }  else 
{ 
 obj_t BgL_auxz00_11317;
BgL_auxz00_11317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44790L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
FAILURE(BgL_auxz00_11317,BFALSE,BFALSE);} 
BgL_auxz00_11313 = 
(long)CINT(BgL_tmpz00_11314); } 
return 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2740z00_2394, BgL_auxz00_11313);} }  else 
{ /* Ieee/number.scm 1207 */
if(
BGL_UINT16P(BgL_xz00_129))
{ /* Ieee/number.scm 1208 */
 long BgL_arg2742z00_2396;
{ /* Ieee/number.scm 1208 */
 uint16_t BgL_xz00_4547;
BgL_xz00_4547 = 
BGL_BUINT16_TO_UINT16(BgL_xz00_129); 
{ /* Ieee/number.scm 1208 */
 long BgL_arg3249z00_4548;
BgL_arg3249z00_4548 = 
(long)(BgL_xz00_4547); 
BgL_arg2742z00_2396 = 
(long)(BgL_arg3249z00_4548); } } 
{ /* Ieee/number.scm 1208 */
 long BgL_auxz00_11328;
{ /* Ieee/number.scm 1208 */
 obj_t BgL_tmpz00_11329;
if(
INTEGERP(BgL_radixz00_130))
{ /* Ieee/number.scm 1208 */
BgL_tmpz00_11329 = BgL_radixz00_130
; }  else 
{ 
 obj_t BgL_auxz00_11332;
BgL_auxz00_11332 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44853L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
FAILURE(BgL_auxz00_11332,BFALSE,BFALSE);} 
BgL_auxz00_11328 = 
(long)CINT(BgL_tmpz00_11329); } 
return 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2742z00_2396, BgL_auxz00_11328);} }  else 
{ /* Ieee/number.scm 1208 */
if(
BGL_INT32P(BgL_xz00_129))
{ /* Ieee/number.scm 1209 */
 BGL_LONGLONG_T BgL_arg2744z00_2398;
{ /* Ieee/number.scm 1209 */
 int32_t BgL_nz00_4550;
BgL_nz00_4550 = 
BGL_BINT32_TO_INT32(BgL_xz00_129); 
BgL_arg2744z00_2398 = 
(BGL_LONGLONG_T)(BgL_nz00_4550); } 
{ /* Ieee/number.scm 1209 */
 obj_t BgL_list2745z00_2399;
BgL_list2745z00_2399 = 
MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL); 
return 
BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2744z00_2398, BgL_list2745z00_2399);} }  else 
{ /* Ieee/number.scm 1209 */
if(
BGL_UINT32P(BgL_xz00_129))
{ /* Ieee/number.scm 1210 */
 BGL_LONGLONG_T BgL_arg2747z00_2401;
{ /* Ieee/number.scm 1210 */
 uint32_t BgL_nz00_4551;
BgL_nz00_4551 = 
BGL_BUINT32_TO_UINT32(BgL_xz00_129); 
BgL_arg2747z00_2401 = 
(BGL_LONGLONG_T)(BgL_nz00_4551); } 
{ /* Ieee/number.scm 1210 */
 obj_t BgL_list2748z00_2402;
BgL_list2748z00_2402 = 
MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL); 
return 
BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2747z00_2401, BgL_list2748z00_2402);} }  else 
{ /* Ieee/number.scm 1210 */
if(
BGL_INT64P(BgL_xz00_129))
{ /* Ieee/number.scm 1211 */
 BGL_LONGLONG_T BgL_arg2750z00_2404;
{ /* Ieee/number.scm 1211 */
 int64_t BgL_nz00_4552;
BgL_nz00_4552 = 
BGL_BINT64_TO_INT64(BgL_xz00_129); 
BgL_arg2750z00_2404 = 
(BGL_LONGLONG_T)(BgL_nz00_4552); } 
{ /* Ieee/number.scm 1211 */
 obj_t BgL_list2751z00_2405;
BgL_list2751z00_2405 = 
MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL); 
return 
BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2750z00_2404, BgL_list2751z00_2405);} }  else 
{ /* Ieee/number.scm 1211 */
if(
BGL_UINT64P(BgL_xz00_129))
{ /* Ieee/number.scm 1212 */
 BGL_LONGLONG_T BgL_arg2753z00_2407;
{ /* Ieee/number.scm 1212 */
 uint64_t BgL_nz00_4553;
BgL_nz00_4553 = 
BGL_BINT64_TO_INT64(BgL_xz00_129); 
BgL_arg2753z00_2407 = 
(BGL_LONGLONG_T)(BgL_nz00_4553); } 
{ /* Ieee/number.scm 1212 */
 obj_t BgL_list2754z00_2408;
BgL_list2754z00_2408 = 
MAKE_YOUNG_PAIR(BgL_radixz00_130, BNIL); 
return 
BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg2753z00_2407, BgL_list2754z00_2408);} }  else 
{ /* Ieee/number.scm 1213 */
 obj_t BgL_aux3702z00_5523;
BgL_aux3702z00_5523 = 
BGl_errorz00zz__errorz00(BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3805z00zz__r4_numbers_6_5z00, BgL_xz00_129); 
if(
STRINGP(BgL_aux3702z00_5523))
{ /* Ieee/number.scm 1213 */
return BgL_aux3702z00_5523;}  else 
{ 
 obj_t BgL_auxz00_11365;
BgL_auxz00_11365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45109L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3806z00zz__r4_numbers_6_5z00, BgL_aux3702z00_5523); 
FAILURE(BgL_auxz00_11365,BFALSE,BFALSE);} } } } } } } } } } } } } } }  else 
{ /* Ieee/number.scm 1199 */
 obj_t BgL_aux3704z00_5525;
BgL_aux3704z00_5525 = 
BGl_errorz00zz__errorz00(BGl_symbol3802z00zz__r4_numbers_6_5z00, BGl_string3807z00zz__r4_numbers_6_5z00, BgL_radixz00_130); 
if(
STRINGP(BgL_aux3704z00_5525))
{ /* Ieee/number.scm 1199 */
return BgL_aux3704z00_5525;}  else 
{ 
 obj_t BgL_auxz00_11372;
BgL_auxz00_11372 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(44356L), BGl_string3803z00zz__r4_numbers_6_5z00, BGl_string3806z00zz__r4_numbers_6_5z00, BgL_aux3704z00_5525); 
FAILURE(BgL_auxz00_11372,BFALSE,BFALSE);} } } 

}



/* _string->number */
obj_t BGl__stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t BgL_env1084z00_136, obj_t BgL_opt1083z00_135)
{
{ /* Ieee/number.scm 1218 */
{ /* Ieee/number.scm 1218 */
 obj_t BgL_g1085z00_2409;
BgL_g1085z00_2409 = 
VECTOR_REF(BgL_opt1083z00_135,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1083z00_135)) { case 1L : 

{ /* Ieee/number.scm 1218 */

{ /* Ieee/number.scm 1218 */
 obj_t BgL_auxz00_11377;
if(
STRINGP(BgL_g1085z00_2409))
{ /* Ieee/number.scm 1218 */
BgL_auxz00_11377 = BgL_g1085z00_2409
; }  else 
{ 
 obj_t BgL_auxz00_11380;
BgL_auxz00_11380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45386L), BGl_string3810z00zz__r4_numbers_6_5z00, BGl_string3806z00zz__r4_numbers_6_5z00, BgL_g1085z00_2409); 
FAILURE(BgL_auxz00_11380,BFALSE,BFALSE);} 
return 
BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(BgL_auxz00_11377, 
BINT(10L));} } break;case 2L : 

{ /* Ieee/number.scm 1218 */
 obj_t BgL_radixz00_2413;
BgL_radixz00_2413 = 
VECTOR_REF(BgL_opt1083z00_135,1L); 
{ /* Ieee/number.scm 1218 */

{ /* Ieee/number.scm 1218 */
 obj_t BgL_auxz00_11387;
if(
STRINGP(BgL_g1085z00_2409))
{ /* Ieee/number.scm 1218 */
BgL_auxz00_11387 = BgL_g1085z00_2409
; }  else 
{ 
 obj_t BgL_auxz00_11390;
BgL_auxz00_11390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45386L), BGl_string3810z00zz__r4_numbers_6_5z00, BGl_string3806z00zz__r4_numbers_6_5z00, BgL_g1085z00_2409); 
FAILURE(BgL_auxz00_11390,BFALSE,BFALSE);} 
return 
BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(BgL_auxz00_11387, BgL_radixz00_2413);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3808z00zz__r4_numbers_6_5z00, BGl_string3804z00zz__r4_numbers_6_5z00, 
BINT(
VECTOR_LENGTH(BgL_opt1083z00_135)));} } } } 

}



/* string->number */
BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t BgL_xz00_133, obj_t BgL_radixz00_134)
{
{ /* Ieee/number.scm 1218 */
{ 
 obj_t BgL_xz00_2506; obj_t BgL_xz00_2427; obj_t BgL_rz00_2428;
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_radixz00_134))
{ /* Ieee/number.scm 1287 */
if(
(
STRING_LENGTH(BgL_xz00_133)==0L))
{ /* Ieee/number.scm 1289 */
return BFALSE;}  else 
{ /* Ieee/number.scm 1291 */
 bool_t BgL_test4973z00_11405;
BgL_xz00_2427 = BgL_xz00_133; 
BgL_rz00_2428 = BgL_radixz00_134; 
{ /* Ieee/number.scm 1221 */
 long BgL_lenz00_2430;
BgL_lenz00_2430 = 
STRING_LENGTH(BgL_xz00_2427); 
{ 
 long BgL_iz00_2433;
BgL_iz00_2433 = 
(BgL_lenz00_2430-1L); 
BgL_zc3z04anonymousza32767ze3z87_2434:
if(
(-1L==BgL_iz00_2433))
{ /* Ieee/number.scm 1223 */
BgL_test4973z00_11405 = ((bool_t)1)
; }  else 
{ /* Ieee/number.scm 1225 */
 bool_t BgL_test4975z00_11409;
{ /* Ieee/number.scm 1225 */
 bool_t BgL_test4976z00_11410;
{ /* Ieee/number.scm 1225 */
 unsigned char BgL_tmpz00_11411;
{ /* Ieee/number.scm 1225 */
 long BgL_l3255z00_5076;
BgL_l3255z00_5076 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3255z00_5076))
{ /* Ieee/number.scm 1225 */
BgL_tmpz00_11411 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11416;
BgL_auxz00_11416 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45590L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3255z00_5076), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11416,BFALSE,BFALSE);} } 
BgL_test4976z00_11410 = 
(BgL_tmpz00_11411>=((unsigned char)'0')); } 
if(BgL_test4976z00_11410)
{ /* Ieee/number.scm 1226 */
 bool_t BgL_test4978z00_11423;
{ /* Ieee/number.scm 1226 */
 unsigned char BgL_tmpz00_11424;
{ /* Ieee/number.scm 1226 */
 long BgL_l3259z00_5080;
BgL_l3259z00_5080 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3259z00_5080))
{ /* Ieee/number.scm 1226 */
BgL_tmpz00_11424 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11429;
BgL_auxz00_11429 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45624L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3259z00_5080), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11429,BFALSE,BFALSE);} } 
BgL_test4978z00_11423 = 
(BgL_tmpz00_11424<=((unsigned char)'1')); } 
if(BgL_test4978z00_11423)
{ /* Ieee/number.scm 1227 */
 long BgL_n1z00_4565;
{ /* Ieee/number.scm 1227 */
 obj_t BgL_tmpz00_11436;
if(
INTEGERP(BgL_rz00_2428))
{ /* Ieee/number.scm 1227 */
BgL_tmpz00_11436 = BgL_rz00_2428
; }  else 
{ 
 obj_t BgL_auxz00_11439;
BgL_auxz00_11439 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45655L), BGl_string3812z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_rz00_2428); 
FAILURE(BgL_auxz00_11439,BFALSE,BFALSE);} 
BgL_n1z00_4565 = 
(long)CINT(BgL_tmpz00_11436); } 
BgL_test4975z00_11409 = 
(BgL_n1z00_4565>=2L); }  else 
{ /* Ieee/number.scm 1226 */
BgL_test4975z00_11409 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1225 */
BgL_test4975z00_11409 = ((bool_t)0)
; } } 
if(BgL_test4975z00_11409)
{ 
 long BgL_iz00_11445;
BgL_iz00_11445 = 
(BgL_iz00_2433-1L); 
BgL_iz00_2433 = BgL_iz00_11445; 
goto BgL_zc3z04anonymousza32767ze3z87_2434;}  else 
{ /* Ieee/number.scm 1229 */
 bool_t BgL_test4981z00_11447;
{ /* Ieee/number.scm 1229 */
 bool_t BgL_test4982z00_11448;
{ /* Ieee/number.scm 1229 */
 unsigned char BgL_tmpz00_11449;
{ /* Ieee/number.scm 1229 */
 long BgL_l3263z00_5084;
BgL_l3263z00_5084 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3263z00_5084))
{ /* Ieee/number.scm 1229 */
BgL_tmpz00_11449 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11454;
BgL_auxz00_11454 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45703L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3263z00_5084), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11454,BFALSE,BFALSE);} } 
BgL_test4982z00_11448 = 
(BgL_tmpz00_11449>=((unsigned char)'2')); } 
if(BgL_test4982z00_11448)
{ /* Ieee/number.scm 1230 */
 bool_t BgL_test4984z00_11461;
{ /* Ieee/number.scm 1230 */
 unsigned char BgL_tmpz00_11462;
{ /* Ieee/number.scm 1230 */
 long BgL_l3267z00_5088;
BgL_l3267z00_5088 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3267z00_5088))
{ /* Ieee/number.scm 1230 */
BgL_tmpz00_11462 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11467;
BgL_auxz00_11467 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45737L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3267z00_5088), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11467,BFALSE,BFALSE);} } 
BgL_test4984z00_11461 = 
(BgL_tmpz00_11462<=((unsigned char)'7')); } 
if(BgL_test4984z00_11461)
{ /* Ieee/number.scm 1231 */
 long BgL_n1z00_4575;
{ /* Ieee/number.scm 1231 */
 obj_t BgL_tmpz00_11474;
if(
INTEGERP(BgL_rz00_2428))
{ /* Ieee/number.scm 1231 */
BgL_tmpz00_11474 = BgL_rz00_2428
; }  else 
{ 
 obj_t BgL_auxz00_11477;
BgL_auxz00_11477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45768L), BGl_string3812z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_rz00_2428); 
FAILURE(BgL_auxz00_11477,BFALSE,BFALSE);} 
BgL_n1z00_4575 = 
(long)CINT(BgL_tmpz00_11474); } 
BgL_test4981z00_11447 = 
(BgL_n1z00_4575>=8L); }  else 
{ /* Ieee/number.scm 1230 */
BgL_test4981z00_11447 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1229 */
BgL_test4981z00_11447 = ((bool_t)0)
; } } 
if(BgL_test4981z00_11447)
{ 
 long BgL_iz00_11483;
BgL_iz00_11483 = 
(BgL_iz00_2433-1L); 
BgL_iz00_2433 = BgL_iz00_11483; 
goto BgL_zc3z04anonymousza32767ze3z87_2434;}  else 
{ /* Ieee/number.scm 1233 */
 bool_t BgL_test4987z00_11485;
{ /* Ieee/number.scm 1233 */
 bool_t BgL_test4988z00_11486;
{ /* Ieee/number.scm 1233 */
 unsigned char BgL_tmpz00_11487;
{ /* Ieee/number.scm 1233 */
 long BgL_l3271z00_5092;
BgL_l3271z00_5092 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3271z00_5092))
{ /* Ieee/number.scm 1233 */
BgL_tmpz00_11487 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11492;
BgL_auxz00_11492 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45816L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3271z00_5092), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11492,BFALSE,BFALSE);} } 
BgL_test4988z00_11486 = 
(BgL_tmpz00_11487>=((unsigned char)'8')); } 
if(BgL_test4988z00_11486)
{ /* Ieee/number.scm 1234 */
 bool_t BgL_test4990z00_11499;
{ /* Ieee/number.scm 1234 */
 unsigned char BgL_tmpz00_11500;
{ /* Ieee/number.scm 1234 */
 long BgL_l3275z00_5096;
BgL_l3275z00_5096 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3275z00_5096))
{ /* Ieee/number.scm 1234 */
BgL_tmpz00_11500 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11505;
BgL_auxz00_11505 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45850L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3275z00_5096), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11505,BFALSE,BFALSE);} } 
BgL_test4990z00_11499 = 
(BgL_tmpz00_11500<=((unsigned char)'9')); } 
if(BgL_test4990z00_11499)
{ /* Ieee/number.scm 1235 */
 long BgL_n1z00_4585;
{ /* Ieee/number.scm 1235 */
 obj_t BgL_tmpz00_11512;
if(
INTEGERP(BgL_rz00_2428))
{ /* Ieee/number.scm 1235 */
BgL_tmpz00_11512 = BgL_rz00_2428
; }  else 
{ 
 obj_t BgL_auxz00_11515;
BgL_auxz00_11515 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45881L), BGl_string3812z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_rz00_2428); 
FAILURE(BgL_auxz00_11515,BFALSE,BFALSE);} 
BgL_n1z00_4585 = 
(long)CINT(BgL_tmpz00_11512); } 
BgL_test4987z00_11485 = 
(BgL_n1z00_4585>=10L); }  else 
{ /* Ieee/number.scm 1234 */
BgL_test4987z00_11485 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1233 */
BgL_test4987z00_11485 = ((bool_t)0)
; } } 
if(BgL_test4987z00_11485)
{ 
 long BgL_iz00_11521;
BgL_iz00_11521 = 
(BgL_iz00_2433-1L); 
BgL_iz00_2433 = BgL_iz00_11521; 
goto BgL_zc3z04anonymousza32767ze3z87_2434;}  else 
{ /* Ieee/number.scm 1237 */
 bool_t BgL_test4993z00_11523;
{ /* Ieee/number.scm 1237 */
 bool_t BgL_test4994z00_11524;
{ /* Ieee/number.scm 1237 */
 unsigned char BgL_tmpz00_11525;
{ /* Ieee/number.scm 1237 */
 long BgL_l3279z00_5100;
BgL_l3279z00_5100 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3279z00_5100))
{ /* Ieee/number.scm 1237 */
BgL_tmpz00_11525 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11530;
BgL_auxz00_11530 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45930L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3279z00_5100), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11530,BFALSE,BFALSE);} } 
BgL_test4994z00_11524 = 
(BgL_tmpz00_11525>=((unsigned char)'a')); } 
if(BgL_test4994z00_11524)
{ /* Ieee/number.scm 1238 */
 bool_t BgL_test4996z00_11537;
{ /* Ieee/number.scm 1238 */
 unsigned char BgL_tmpz00_11538;
{ /* Ieee/number.scm 1238 */
 long BgL_l3283z00_5104;
BgL_l3283z00_5104 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3283z00_5104))
{ /* Ieee/number.scm 1238 */
BgL_tmpz00_11538 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11543;
BgL_auxz00_11543 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45964L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3283z00_5104), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11543,BFALSE,BFALSE);} } 
BgL_test4996z00_11537 = 
(BgL_tmpz00_11538<=((unsigned char)'f')); } 
if(BgL_test4996z00_11537)
{ /* Ieee/number.scm 1239 */
 long BgL_n1z00_4595;
{ /* Ieee/number.scm 1239 */
 obj_t BgL_tmpz00_11550;
if(
INTEGERP(BgL_rz00_2428))
{ /* Ieee/number.scm 1239 */
BgL_tmpz00_11550 = BgL_rz00_2428
; }  else 
{ 
 obj_t BgL_auxz00_11553;
BgL_auxz00_11553 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(45994L), BGl_string3812z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_rz00_2428); 
FAILURE(BgL_auxz00_11553,BFALSE,BFALSE);} 
BgL_n1z00_4595 = 
(long)CINT(BgL_tmpz00_11550); } 
BgL_test4993z00_11523 = 
(BgL_n1z00_4595==16L); }  else 
{ /* Ieee/number.scm 1238 */
BgL_test4993z00_11523 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1237 */
BgL_test4993z00_11523 = ((bool_t)0)
; } } 
if(BgL_test4993z00_11523)
{ 
 long BgL_iz00_11559;
BgL_iz00_11559 = 
(BgL_iz00_2433-1L); 
BgL_iz00_2433 = BgL_iz00_11559; 
goto BgL_zc3z04anonymousza32767ze3z87_2434;}  else 
{ /* Ieee/number.scm 1241 */
 bool_t BgL_test4999z00_11561;
{ /* Ieee/number.scm 1241 */
 bool_t BgL_test5000z00_11562;
{ /* Ieee/number.scm 1241 */
 unsigned char BgL_tmpz00_11563;
{ /* Ieee/number.scm 1241 */
 long BgL_l3287z00_5108;
BgL_l3287z00_5108 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3287z00_5108))
{ /* Ieee/number.scm 1241 */
BgL_tmpz00_11563 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11568;
BgL_auxz00_11568 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46043L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3287z00_5108), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11568,BFALSE,BFALSE);} } 
BgL_test5000z00_11562 = 
(BgL_tmpz00_11563>=((unsigned char)'A')); } 
if(BgL_test5000z00_11562)
{ /* Ieee/number.scm 1242 */
 bool_t BgL_test5002z00_11575;
{ /* Ieee/number.scm 1242 */
 unsigned char BgL_tmpz00_11576;
{ /* Ieee/number.scm 1242 */
 long BgL_l3291z00_5112;
BgL_l3291z00_5112 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3291z00_5112))
{ /* Ieee/number.scm 1242 */
BgL_tmpz00_11576 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11581;
BgL_auxz00_11581 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46077L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3291z00_5112), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11581,BFALSE,BFALSE);} } 
BgL_test5002z00_11575 = 
(BgL_tmpz00_11576<=((unsigned char)'F')); } 
if(BgL_test5002z00_11575)
{ /* Ieee/number.scm 1243 */
 long BgL_n1z00_4605;
{ /* Ieee/number.scm 1243 */
 obj_t BgL_tmpz00_11588;
if(
INTEGERP(BgL_rz00_2428))
{ /* Ieee/number.scm 1243 */
BgL_tmpz00_11588 = BgL_rz00_2428
; }  else 
{ 
 obj_t BgL_auxz00_11591;
BgL_auxz00_11591 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46107L), BGl_string3812z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_rz00_2428); 
FAILURE(BgL_auxz00_11591,BFALSE,BFALSE);} 
BgL_n1z00_4605 = 
(long)CINT(BgL_tmpz00_11588); } 
BgL_test4999z00_11561 = 
(BgL_n1z00_4605==16L); }  else 
{ /* Ieee/number.scm 1242 */
BgL_test4999z00_11561 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1241 */
BgL_test4999z00_11561 = ((bool_t)0)
; } } 
if(BgL_test4999z00_11561)
{ 
 long BgL_iz00_11597;
BgL_iz00_11597 = 
(BgL_iz00_2433-1L); 
BgL_iz00_2433 = BgL_iz00_11597; 
goto BgL_zc3z04anonymousza32767ze3z87_2434;}  else 
{ /* Ieee/number.scm 1245 */
 bool_t BgL_test5005z00_11599;
{ /* Ieee/number.scm 1245 */
 bool_t BgL_test5006z00_11600;
{ /* Ieee/number.scm 1245 */
 unsigned char BgL_tmpz00_11601;
{ /* Ieee/number.scm 1245 */
 long BgL_l3295z00_5116;
BgL_l3295z00_5116 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3295z00_5116))
{ /* Ieee/number.scm 1245 */
BgL_tmpz00_11601 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11606;
BgL_auxz00_11606 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46154L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3295z00_5116), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11606,BFALSE,BFALSE);} } 
BgL_test5006z00_11600 = 
(BgL_tmpz00_11601==((unsigned char)'-')); } 
if(BgL_test5006z00_11600)
{ /* Ieee/number.scm 1245 */
BgL_test5005z00_11599 = ((bool_t)1)
; }  else 
{ /* Ieee/number.scm 1246 */
 unsigned char BgL_tmpz00_11613;
{ /* Ieee/number.scm 1246 */
 long BgL_l3299z00_5120;
BgL_l3299z00_5120 = 
STRING_LENGTH(BgL_xz00_2427); 
if(
BOUND_CHECK(BgL_iz00_2433, BgL_l3299z00_5120))
{ /* Ieee/number.scm 1246 */
BgL_tmpz00_11613 = 
STRING_REF(BgL_xz00_2427, BgL_iz00_2433)
; }  else 
{ 
 obj_t BgL_auxz00_11618;
BgL_auxz00_11618 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46193L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2427, 
(int)(BgL_l3299z00_5120), 
(int)(BgL_iz00_2433)); 
FAILURE(BgL_auxz00_11618,BFALSE,BFALSE);} } 
BgL_test5005z00_11599 = 
(BgL_tmpz00_11613==((unsigned char)'+')); } } 
if(BgL_test5005z00_11599)
{ /* Ieee/number.scm 1245 */
if(
(BgL_iz00_2433==0L))
{ /* Ieee/number.scm 1247 */
BgL_test4973z00_11405 = 
(BgL_lenz00_2430>1L)
; }  else 
{ /* Ieee/number.scm 1247 */
BgL_test4973z00_11405 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1245 */
BgL_test4973z00_11405 = ((bool_t)0)
; } } } } } } } } } 
if(BgL_test4973z00_11405)
{ /* Ieee/number.scm 1292 */
 long BgL_auxz00_11629;
{ /* Ieee/number.scm 1292 */
 obj_t BgL_tmpz00_11630;
if(
INTEGERP(BgL_radixz00_134))
{ /* Ieee/number.scm 1292 */
BgL_tmpz00_11630 = BgL_radixz00_134
; }  else 
{ 
 obj_t BgL_auxz00_11633;
BgL_auxz00_11633 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(47194L), BGl_string3809z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_134); 
FAILURE(BgL_auxz00_11633,BFALSE,BFALSE);} 
BgL_auxz00_11629 = 
(long)CINT(BgL_tmpz00_11630); } 
return 
BGl_stringzd2ze3integerzd2objze3zz__r4_numbers_6_5_fixnumz00(BgL_xz00_133, BgL_auxz00_11629);}  else 
{ /* Ieee/number.scm 1293 */
 bool_t BgL_test5011z00_11639;
{ /* Ieee/number.scm 1293 */
 long BgL_l1z00_4660;
BgL_l1z00_4660 = 
STRING_LENGTH(BgL_xz00_133); 
if(
(BgL_l1z00_4660==6L))
{ /* Ieee/number.scm 1293 */
 int BgL_arg2919z00_4663;
{ /* Ieee/number.scm 1293 */
 char * BgL_auxz00_11645; char * BgL_tmpz00_11643;
BgL_auxz00_11645 = 
BSTRING_TO_STRING(BGl_string3813z00zz__r4_numbers_6_5z00); 
BgL_tmpz00_11643 = 
BSTRING_TO_STRING(BgL_xz00_133); 
BgL_arg2919z00_4663 = 
memcmp(BgL_tmpz00_11643, BgL_auxz00_11645, BgL_l1z00_4660); } 
BgL_test5011z00_11639 = 
(
(long)(BgL_arg2919z00_4663)==0L); }  else 
{ /* Ieee/number.scm 1293 */
BgL_test5011z00_11639 = ((bool_t)0)
; } } 
if(BgL_test5011z00_11639)
{ /* Ieee/number.scm 1293 */
return BGL_REAL_CNST( BGl_real3814z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1295 */
 bool_t BgL_test5013z00_11650;
{ /* Ieee/number.scm 1295 */
 long BgL_l1z00_4671;
BgL_l1z00_4671 = 
STRING_LENGTH(BgL_xz00_133); 
if(
(BgL_l1z00_4671==6L))
{ /* Ieee/number.scm 1295 */
 int BgL_arg2919z00_4674;
{ /* Ieee/number.scm 1295 */
 char * BgL_auxz00_11656; char * BgL_tmpz00_11654;
BgL_auxz00_11656 = 
BSTRING_TO_STRING(BGl_string3815z00zz__r4_numbers_6_5z00); 
BgL_tmpz00_11654 = 
BSTRING_TO_STRING(BgL_xz00_133); 
BgL_arg2919z00_4674 = 
memcmp(BgL_tmpz00_11654, BgL_auxz00_11656, BgL_l1z00_4671); } 
BgL_test5013z00_11650 = 
(
(long)(BgL_arg2919z00_4674)==0L); }  else 
{ /* Ieee/number.scm 1295 */
BgL_test5013z00_11650 = ((bool_t)0)
; } } 
if(BgL_test5013z00_11650)
{ /* Ieee/number.scm 1295 */
return BGL_REAL_CNST( BGl_real3816z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1297 */
 bool_t BgL_test5015z00_11661;
{ /* Ieee/number.scm 1297 */
 long BgL_l1z00_4682;
BgL_l1z00_4682 = 
STRING_LENGTH(BgL_xz00_133); 
if(
(BgL_l1z00_4682==6L))
{ /* Ieee/number.scm 1297 */
 int BgL_arg2919z00_4685;
{ /* Ieee/number.scm 1297 */
 char * BgL_auxz00_11667; char * BgL_tmpz00_11665;
BgL_auxz00_11667 = 
BSTRING_TO_STRING(BGl_string3817z00zz__r4_numbers_6_5z00); 
BgL_tmpz00_11665 = 
BSTRING_TO_STRING(BgL_xz00_133); 
BgL_arg2919z00_4685 = 
memcmp(BgL_tmpz00_11665, BgL_auxz00_11667, BgL_l1z00_4682); } 
BgL_test5015z00_11661 = 
(
(long)(BgL_arg2919z00_4685)==0L); }  else 
{ /* Ieee/number.scm 1297 */
BgL_test5015z00_11661 = ((bool_t)0)
; } } 
if(BgL_test5015z00_11661)
{ /* Ieee/number.scm 1297 */
return BGL_REAL_CNST( BGl_real3818z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1299 */
 bool_t BgL_test5017z00_11672;
BgL_xz00_2506 = BgL_xz00_133; 
{ /* Ieee/number.scm 1251 */
 long BgL_lenz00_2508;
BgL_lenz00_2508 = 
STRING_LENGTH(BgL_xz00_2506); 
{ 
 long BgL_iz00_2510; bool_t BgL_ez00_2511; long BgL_pz00_2512; bool_t BgL_dz00_2513;
BgL_iz00_2510 = 0L; 
BgL_ez00_2511 = ((bool_t)0); 
BgL_pz00_2512 = 0L; 
BgL_dz00_2513 = ((bool_t)0); 
BgL_zc3z04anonymousza32861ze3z87_2514:
if(
(BgL_iz00_2510==BgL_lenz00_2508))
{ /* Ieee/number.scm 1256 */
BgL_test5017z00_11672 = BgL_dz00_2513
; }  else 
{ /* Ieee/number.scm 1258 */
 bool_t BgL_test5019z00_11676;
{ /* Ieee/number.scm 1258 */
 bool_t BgL_test5020z00_11677;
{ /* Ieee/number.scm 1258 */
 unsigned char BgL_tmpz00_11678;
{ /* Ieee/number.scm 1258 */
 long BgL_l3303z00_5124;
BgL_l3303z00_5124 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3303z00_5124))
{ /* Ieee/number.scm 1258 */
BgL_tmpz00_11678 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11683;
BgL_auxz00_11683 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46446L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3303z00_5124), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11683,BFALSE,BFALSE);} } 
BgL_test5020z00_11677 = 
(BgL_tmpz00_11678>=((unsigned char)'0')); } 
if(BgL_test5020z00_11677)
{ /* Ieee/number.scm 1259 */
 unsigned char BgL_tmpz00_11690;
{ /* Ieee/number.scm 1259 */
 long BgL_l3307z00_5128;
BgL_l3307z00_5128 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3307z00_5128))
{ /* Ieee/number.scm 1259 */
BgL_tmpz00_11690 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11695;
BgL_auxz00_11695 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46480L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3307z00_5128), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11695,BFALSE,BFALSE);} } 
BgL_test5019z00_11676 = 
(BgL_tmpz00_11690<=((unsigned char)'9')); }  else 
{ /* Ieee/number.scm 1258 */
BgL_test5019z00_11676 = ((bool_t)0)
; } } 
if(BgL_test5019z00_11676)
{ 
 bool_t BgL_dz00_11705; long BgL_pz00_11704; long BgL_iz00_11702;
BgL_iz00_11702 = 
(BgL_iz00_2510+1L); 
BgL_pz00_11704 = 0L; 
BgL_dz00_11705 = ((bool_t)1); 
BgL_dz00_2513 = BgL_dz00_11705; 
BgL_pz00_2512 = BgL_pz00_11704; 
BgL_iz00_2510 = BgL_iz00_11702; 
goto BgL_zc3z04anonymousza32861ze3z87_2514;}  else 
{ /* Ieee/number.scm 1264 */
 bool_t BgL_test5023z00_11706;
{ /* Ieee/number.scm 1264 */
 unsigned char BgL_tmpz00_11707;
{ /* Ieee/number.scm 1264 */
 long BgL_l3311z00_5132;
BgL_l3311z00_5132 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3311z00_5132))
{ /* Ieee/number.scm 1264 */
BgL_tmpz00_11707 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11712;
BgL_auxz00_11712 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46558L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3311z00_5132), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11712,BFALSE,BFALSE);} } 
BgL_test5023z00_11706 = 
(BgL_tmpz00_11707==((unsigned char)'.')); } 
if(BgL_test5023z00_11706)
{ 
 long BgL_pz00_11721; long BgL_iz00_11719;
BgL_iz00_11719 = 
(BgL_iz00_2510+1L); 
BgL_pz00_11721 = 0L; 
BgL_pz00_2512 = BgL_pz00_11721; 
BgL_iz00_2510 = BgL_iz00_11719; 
goto BgL_zc3z04anonymousza32861ze3z87_2514;}  else 
{ /* Ieee/number.scm 1269 */
 bool_t BgL_test5025z00_11722;
{ /* Ieee/number.scm 1269 */
 bool_t BgL_test5026z00_11723;
{ /* Ieee/number.scm 1269 */
 unsigned char BgL_tmpz00_11724;
{ /* Ieee/number.scm 1269 */
 long BgL_l3315z00_5136;
BgL_l3315z00_5136 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3315z00_5136))
{ /* Ieee/number.scm 1269 */
BgL_tmpz00_11724 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11729;
BgL_auxz00_11729 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46638L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3315z00_5136), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11729,BFALSE,BFALSE);} } 
BgL_test5026z00_11723 = 
(BgL_tmpz00_11724==((unsigned char)'e')); } 
if(BgL_test5026z00_11723)
{ /* Ieee/number.scm 1269 */
BgL_test5025z00_11722 = ((bool_t)1)
; }  else 
{ /* Ieee/number.scm 1270 */
 unsigned char BgL_tmpz00_11736;
{ /* Ieee/number.scm 1270 */
 long BgL_l3319z00_5140;
BgL_l3319z00_5140 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3319z00_5140))
{ /* Ieee/number.scm 1270 */
BgL_tmpz00_11736 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11741;
BgL_auxz00_11741 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46677L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3319z00_5140), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11741,BFALSE,BFALSE);} } 
BgL_test5025z00_11722 = 
(BgL_tmpz00_11736==((unsigned char)'E')); } } 
if(BgL_test5025z00_11722)
{ /* Ieee/number.scm 1271 */
 bool_t BgL_test5029z00_11748;
if(BgL_ez00_2511)
{ /* Ieee/number.scm 1271 */
BgL_test5029z00_11748 = ((bool_t)1)
; }  else 
{ /* Ieee/number.scm 1271 */
if(BgL_dz00_2513)
{ /* Ieee/number.scm 1271 */
BgL_test5029z00_11748 = ((bool_t)0)
; }  else 
{ /* Ieee/number.scm 1271 */
BgL_test5029z00_11748 = ((bool_t)1)
; } } 
if(BgL_test5029z00_11748)
{ /* Ieee/number.scm 1271 */
BgL_test5017z00_11672 = ((bool_t)0)
; }  else 
{ 
 long BgL_pz00_11754; bool_t BgL_ez00_11753; long BgL_iz00_11751;
BgL_iz00_11751 = 
(BgL_iz00_2510+1L); 
BgL_ez00_11753 = ((bool_t)1); 
BgL_pz00_11754 = 
(BgL_iz00_2510+1L); 
BgL_pz00_2512 = BgL_pz00_11754; 
BgL_ez00_2511 = BgL_ez00_11753; 
BgL_iz00_2510 = BgL_iz00_11751; 
goto BgL_zc3z04anonymousza32861ze3z87_2514;} }  else 
{ /* Ieee/number.scm 1277 */
 bool_t BgL_test5032z00_11756;
{ /* Ieee/number.scm 1277 */
 bool_t BgL_test5033z00_11757;
{ /* Ieee/number.scm 1277 */
 unsigned char BgL_tmpz00_11758;
{ /* Ieee/number.scm 1277 */
 long BgL_l3323z00_5144;
BgL_l3323z00_5144 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3323z00_5144))
{ /* Ieee/number.scm 1277 */
BgL_tmpz00_11758 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11763;
BgL_auxz00_11763 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46820L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3323z00_5144), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11763,BFALSE,BFALSE);} } 
BgL_test5033z00_11757 = 
(BgL_tmpz00_11758==((unsigned char)'-')); } 
if(BgL_test5033z00_11757)
{ /* Ieee/number.scm 1277 */
BgL_test5032z00_11756 = ((bool_t)1)
; }  else 
{ /* Ieee/number.scm 1278 */
 unsigned char BgL_tmpz00_11770;
{ /* Ieee/number.scm 1278 */
 long BgL_l3327z00_5148;
BgL_l3327z00_5148 = 
STRING_LENGTH(BgL_xz00_2506); 
if(
BOUND_CHECK(BgL_iz00_2510, BgL_l3327z00_5148))
{ /* Ieee/number.scm 1278 */
BgL_tmpz00_11770 = 
STRING_REF(BgL_xz00_2506, BgL_iz00_2510)
; }  else 
{ 
 obj_t BgL_auxz00_11775;
BgL_auxz00_11775 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(46859L), BGl_string3811z00zz__r4_numbers_6_5z00, BgL_xz00_2506, 
(int)(BgL_l3327z00_5148), 
(int)(BgL_iz00_2510)); 
FAILURE(BgL_auxz00_11775,BFALSE,BFALSE);} } 
BgL_test5032z00_11756 = 
(BgL_tmpz00_11770==((unsigned char)'+')); } } 
if(BgL_test5032z00_11756)
{ /* Ieee/number.scm 1279 */
 bool_t BgL_test5036z00_11782;
{ /* Ieee/number.scm 1279 */
 bool_t BgL__ortest_1061z00_2540;
BgL__ortest_1061z00_2540 = 
(BgL_iz00_2510==0L); 
if(BgL__ortest_1061z00_2540)
{ /* Ieee/number.scm 1279 */
BgL_test5036z00_11782 = BgL__ortest_1061z00_2540
; }  else 
{ /* Ieee/number.scm 1279 */
BgL_test5036z00_11782 = 
(BgL_iz00_2510==BgL_pz00_2512)
; } } 
if(BgL_test5036z00_11782)
{ 
 long BgL_pz00_11788; long BgL_iz00_11786;
BgL_iz00_11786 = 
(BgL_iz00_2510+1L); 
BgL_pz00_11788 = 0L; 
BgL_pz00_2512 = BgL_pz00_11788; 
BgL_iz00_2510 = BgL_iz00_11786; 
goto BgL_zc3z04anonymousza32861ze3z87_2514;}  else 
{ /* Ieee/number.scm 1279 */
BgL_test5017z00_11672 = ((bool_t)0)
; } }  else 
{ /* Ieee/number.scm 1277 */
BgL_test5017z00_11672 = ((bool_t)0)
; } } } } } } } 
if(BgL_test5017z00_11672)
{ /* Ieee/number.scm 1300 */
 bool_t BgL_test5038z00_11789;
{ /* Ieee/number.scm 1300 */
 long BgL_n1z00_4691;
{ /* Ieee/number.scm 1300 */
 obj_t BgL_tmpz00_11790;
if(
INTEGERP(BgL_radixz00_134))
{ /* Ieee/number.scm 1300 */
BgL_tmpz00_11790 = BgL_radixz00_134
; }  else 
{ 
 obj_t BgL_auxz00_11793;
BgL_auxz00_11793 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3717z00zz__r4_numbers_6_5z00, 
BINT(47374L), BGl_string3809z00zz__r4_numbers_6_5z00, BGl_string3724z00zz__r4_numbers_6_5z00, BgL_radixz00_134); 
FAILURE(BgL_auxz00_11793,BFALSE,BFALSE);} 
BgL_n1z00_4691 = 
(long)CINT(BgL_tmpz00_11790); } 
BgL_test5038z00_11789 = 
(BgL_n1z00_4691==10L); } 
if(BgL_test5038z00_11789)
{ /* Ieee/number.scm 1301 */
 char * BgL_stringz00_4692;
BgL_stringz00_4692 = 
BSTRING_TO_STRING(BgL_xz00_133); 
{ /* Ieee/number.scm 1301 */
 bool_t BgL_test5040z00_11800;
{ /* Ieee/number.scm 1301 */
 obj_t BgL_string1z00_4696;
BgL_string1z00_4696 = 
string_to_bstring(BgL_stringz00_4692); 
{ /* Ieee/number.scm 1301 */
 long BgL_l1z00_4698;
BgL_l1z00_4698 = 
STRING_LENGTH(BgL_string1z00_4696); 
if(
(BgL_l1z00_4698==6L))
{ /* Ieee/number.scm 1301 */
 int BgL_arg2919z00_4701;
{ /* Ieee/number.scm 1301 */
 char * BgL_auxz00_11807; char * BgL_tmpz00_11805;
BgL_auxz00_11807 = 
BSTRING_TO_STRING(BGl_string3813z00zz__r4_numbers_6_5z00); 
BgL_tmpz00_11805 = 
BSTRING_TO_STRING(BgL_string1z00_4696); 
BgL_arg2919z00_4701 = 
memcmp(BgL_tmpz00_11805, BgL_auxz00_11807, BgL_l1z00_4698); } 
BgL_test5040z00_11800 = 
(
(long)(BgL_arg2919z00_4701)==0L); }  else 
{ /* Ieee/number.scm 1301 */
BgL_test5040z00_11800 = ((bool_t)0)
; } } } 
if(BgL_test5040z00_11800)
{ /* Ieee/number.scm 1301 */
return BGL_REAL_CNST( BGl_real3819z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1301 */
 bool_t BgL_test5042z00_11812;
{ /* Ieee/number.scm 1301 */
 obj_t BgL_string1z00_4707;
BgL_string1z00_4707 = 
string_to_bstring(BgL_stringz00_4692); 
{ /* Ieee/number.scm 1301 */
 long BgL_l1z00_4709;
BgL_l1z00_4709 = 
STRING_LENGTH(BgL_string1z00_4707); 
if(
(BgL_l1z00_4709==6L))
{ /* Ieee/number.scm 1301 */
 int BgL_arg2919z00_4712;
{ /* Ieee/number.scm 1301 */
 char * BgL_auxz00_11819; char * BgL_tmpz00_11817;
BgL_auxz00_11819 = 
BSTRING_TO_STRING(BGl_string3815z00zz__r4_numbers_6_5z00); 
BgL_tmpz00_11817 = 
BSTRING_TO_STRING(BgL_string1z00_4707); 
BgL_arg2919z00_4712 = 
memcmp(BgL_tmpz00_11817, BgL_auxz00_11819, BgL_l1z00_4709); } 
BgL_test5042z00_11812 = 
(
(long)(BgL_arg2919z00_4712)==0L); }  else 
{ /* Ieee/number.scm 1301 */
BgL_test5042z00_11812 = ((bool_t)0)
; } } } 
if(BgL_test5042z00_11812)
{ /* Ieee/number.scm 1301 */
return BGL_REAL_CNST( BGl_real3816z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1301 */
 bool_t BgL_test5044z00_11824;
{ /* Ieee/number.scm 1301 */
 obj_t BgL_string1z00_4718;
BgL_string1z00_4718 = 
string_to_bstring(BgL_stringz00_4692); 
{ /* Ieee/number.scm 1301 */
 long BgL_l1z00_4720;
BgL_l1z00_4720 = 
STRING_LENGTH(BgL_string1z00_4718); 
if(
(BgL_l1z00_4720==6L))
{ /* Ieee/number.scm 1301 */
 int BgL_arg2919z00_4723;
{ /* Ieee/number.scm 1301 */
 char * BgL_auxz00_11831; char * BgL_tmpz00_11829;
BgL_auxz00_11831 = 
BSTRING_TO_STRING(BGl_string3817z00zz__r4_numbers_6_5z00); 
BgL_tmpz00_11829 = 
BSTRING_TO_STRING(BgL_string1z00_4718); 
BgL_arg2919z00_4723 = 
memcmp(BgL_tmpz00_11829, BgL_auxz00_11831, BgL_l1z00_4720); } 
BgL_test5044z00_11824 = 
(
(long)(BgL_arg2919z00_4723)==0L); }  else 
{ /* Ieee/number.scm 1301 */
BgL_test5044z00_11824 = ((bool_t)0)
; } } } 
if(BgL_test5044z00_11824)
{ /* Ieee/number.scm 1301 */
return BGL_REAL_CNST( BGl_real3818z00zz__r4_numbers_6_5z00);}  else 
{ /* Ieee/number.scm 1301 */
return 
DOUBLE_TO_REAL(
STRTOD(BgL_stringz00_4692));} } } } }  else 
{ /* Ieee/number.scm 1300 */
return 
BGl_errorz00zz__errorz00(BGl_string3809z00zz__r4_numbers_6_5z00, BGl_string3820z00zz__r4_numbers_6_5z00, BgL_radixz00_134);} }  else 
{ /* Ieee/number.scm 1299 */
return BFALSE;} } } } } } }  else 
{ /* Ieee/number.scm 1287 */
return 
BGl_errorz00zz__errorz00(BGl_symbol3802z00zz__r4_numbers_6_5z00, BGl_string3807z00zz__r4_numbers_6_5z00, BgL_radixz00_134);} } } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5z00(void)
{
{ /* Ieee/number.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string3821z00zz__r4_numbers_6_5z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string3821z00zz__r4_numbers_6_5z00));} 

}

#ifdef __cplusplus
}
#endif
