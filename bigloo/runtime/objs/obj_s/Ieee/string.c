/*===========================================================================*/
/*   (Ieee/string.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/string.scm -indent -o objs/obj_s/Ieee/string.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_STRINGS_6_7_TYPE_DEFINITIONS
#define BGL___R4_STRINGS_6_7_TYPE_DEFINITIONS
#endif // BGL___R4_STRINGS_6_7_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62stringzd2shrinkz12za2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stringze3zf3z72zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62substringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62escapezd2schemezd2stringz62zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t bigloo_strncmp_ci_at(obj_t, obj_t, long, long);
static obj_t BGl__stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3530z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl__stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
static obj_t BGl_symbol3533z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3455z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3374z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t, unsigned char, obj_t, obj_t);
static obj_t BGl_symbol3537z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3458z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3379z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_z62stringzd2cizc3zd3zf3z53zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_emptyzd2stringzf3z21zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2forzd2readz00zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2deletezd2zz__r4_strings_6_7z00(obj_t, obj_t, int, long);
extern bool_t bigloo_string_cile(obj_t, obj_t);
static obj_t BGl_z62stringzd2forzd2readz62zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t, unsigned char, unsigned char);
static obj_t BGl__stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3463z00zz__r4_strings_6_7z00 = BUNSPEC;
extern bool_t bigloo_string_cilt(obj_t, obj_t);
static obj_t BGl_symbol3546z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3467z00zz__r4_strings_6_7z00 = BUNSPEC;
extern bool_t bigloo_string_le(obj_t, obj_t);
static obj_t BGl_list3439z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_escapezd2schemezd2stringz00zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
static obj_t BGl__substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t bigloo_string_lt(obj_t, obj_t);
static obj_t BGl__stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3471z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3473z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3392z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_stringzd2cizc3zd3zf3z31zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t bigloo_strcicmp(obj_t, obj_t);
static obj_t BGl_z62stringzd2upcasezb0zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzd2cizc3zf3ze2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3479z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3399z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_stringze3zd3zf3zc3zz__r4_strings_6_7z00(obj_t, obj_t);
extern obj_t string_for_read(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62stringzd2downcasez12za2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2nullzf3z43zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3481z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_stringzd2lengthzd2zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL int BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3485z00zz__r4_strings_6_7z00 = BUNSPEC;
static bool_t BGl_delimzf3zf3zz__r4_strings_6_7z00(obj_t, unsigned char);
static obj_t BGl_z62stringzd2upcasez12za2zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2setz12zc0zz__r4_strings_6_7z00(obj_t, long, unsigned char);
static obj_t BGl__stringzd2skipzd2zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t bigloo_strncmp_ci(obj_t, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL int BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3496z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_stringzd2cize3zf3zc2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3499z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_z62stringzd2cize3zd3zf3z73zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL unsigned char BGl_stringzd2refzd2urz00zz__r4_strings_6_7z00(obj_t, long);
static obj_t BGl_z62emptyzd2stringzf3z43zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t, obj_t, int);
BGL_EXPORTED_DECL bool_t BGl_stringzd2nullzf3z21zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62substringzd3zf3z42zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62listzd2ze3stringz53zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2aszd2readz62zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_list3470z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl__stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t bigloo_strcmp_ci_at(obj_t, obj_t, long);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
static obj_t BGl_z62stringzd2capitaliza7ez17zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_list3478z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_stringzd2shrinkz12zc0zz__r4_strings_6_7z00(obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_substringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t, obj_t, long);
extern unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
static obj_t BGl_cnstzd2initzd2zz__r4_strings_6_7z00(void);
BGL_EXPORTED_DECL obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
extern long bgl_list_length(obj_t);
BGL_EXPORTED_DECL obj_t BGl_blitzd2stringzd2urz12z12zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(obj_t);
static obj_t BGl__stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62blitzd2stringzd2urz12z70zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzd2cize3zd3zf3z11zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_strings_6_7z00(void);
static obj_t BGl_z62stringzd2hexzd2internz62zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__r4_strings_6_7z00(void);
static obj_t BGl__stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2splitzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2skipzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(obj_t, unsigned char, unsigned char);
static obj_t BGl__stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2replacez12za2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2lengthzb0zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzc3zf3z30zz__r4_strings_6_7z00(obj_t, obj_t);
extern bool_t bigloo_strcmp_at(obj_t, obj_t, long);
extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_stringzd2compare3zd2ciz00zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62substringzd2urzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
extern obj_t bgl_escape_scheme_string(char *, long, long);
extern obj_t bgl_reverse_bang(obj_t);
static obj_t BGl__stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t, obj_t);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
static obj_t BGl__makezd2stringzd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2appendzb0zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t, int);
BGL_EXPORTED_DECL obj_t BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzd2setzd2urz12z70zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzd2hexzd2internz12z70zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_stringzd3zf3z20zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2refzd2urz62zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzd2ze3listz53zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_escapezd2Czd2stringz00zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2upcasez12zc0zz__r4_strings_6_7z00(obj_t);
extern bool_t bigloo_strncmp_at(obj_t, obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_stringzd2capitaliza7ez75zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62escapezd2Czd2stringz62zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringze3zf3z10zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_substringzd2urzd2zz__r4_strings_6_7z00(obj_t, long, long);
BGL_EXPORTED_DECL bool_t BGl_substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t, obj_t, long, obj_t);
static obj_t BGl_z62stringzd2compare3zd2ciz62zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stringz62zz__r4_strings_6_7z00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL long BGl_stringzd2compare3zd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_comparezd2leftzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzd2charzd2indexzd2urzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2compare3zb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t);
extern obj_t c_substring(obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_stringzd2hexzd2internz00zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62stringzd2cizc3zf3z80zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzc3zd3zf3z81zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_symbol3402z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_stringzd2aszd2readz00zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL unsigned char BGl_stringzd2refzd2zz__r4_strings_6_7z00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_makezd2stringzd2zz__r4_strings_6_7z00(long, obj_t);
static obj_t BGl_z62stringzd2replacezb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
extern obj_t bgl_escape_C_string(char *, long, long);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_z62stringzd2capitaliza7ez12z05zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_comparezd2rightzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
extern bool_t bigloo_string_cige(obj_t, obj_t);
extern obj_t blit_string(obj_t, long, obj_t, long, long);
static obj_t BGl__stringzd2deletezd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2setz12za2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl__stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2charzd2indexzd2urzd2zz__r4_strings_6_7z00(obj_t, unsigned char, long, long);
extern bool_t bigloo_string_cigt(obj_t, obj_t);
extern bool_t bigloo_string_ge(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzd2cize3zf3za0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
extern bool_t bigloo_string_gt(obj_t, obj_t);
static obj_t BGl_z62blitzd2stringz12za2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2downcasez12zc0zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_strnatcmpz00zz__r4_strings_6_7z00(obj_t, obj_t, bool_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_substringzd3zf3z20zz__r4_strings_6_7z00(obj_t, obj_t, long);
static obj_t BGl_symbol3501z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3504z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3506z00zz__r4_strings_6_7z00 = BUNSPEC;
extern obj_t make_string(long, unsigned char);
static obj_t BGl_symbol3509z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_z62stringzd2refzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2cutzd2zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_z62stringzd2fillz12za2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_stringzf3zf3zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62stringzc3zf3z52zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzf3z91zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3511z00zz__r4_strings_6_7z00 = BUNSPEC;
extern obj_t string_append(obj_t, obj_t);
static obj_t BGl_symbol3514z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3434z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3516z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_z62stringzd2copyzb0zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2setzd2urz12z12zz__r4_strings_6_7z00(obj_t, long, unsigned char);
static obj_t BGl__stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3519z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3357z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t, int, long);
BGL_EXPORTED_DECL bool_t BGl_stringzc3zd3zf3ze3zz__r4_strings_6_7z00(obj_t, obj_t);
extern obj_t make_string_sans_fill(long);
BGL_EXPORTED_DECL obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
static obj_t BGl_z62stringze3zd3zf3za1zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62stringzd3zf3z42zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stringzd2downcasezb0zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl__substringz00zz__r4_strings_6_7z00(obj_t, obj_t);
static obj_t BGl_symbol3521z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3440z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern bool_t bigloo_strncmp(obj_t, obj_t, long);
BGL_EXPORTED_DECL int BGl_stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t, obj_t, long, obj_t);
static obj_t BGl_symbol3442z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3524z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3444z00zz__r4_strings_6_7z00 = BUNSPEC;
static obj_t BGl_symbol3526z00zz__r4_strings_6_7z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(obj_t, unsigned char);
static obj_t BGl_z62stringzd2cutzb0zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringz00zz__r4_strings_6_7z00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2upcasez12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2upca3553z00, BGl_z62stringzd2upcasez12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2capitaliza7ezd2envza7zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2capi3554z00, BGl_z62stringzd2capitaliza7ez17zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3490z00zz__r4_strings_6_7z00, BgL_bgl_string3490za700za7za7_3555za7, "Too large end index `", 21 );
DEFINE_STRING( BGl_string3491z00zz__r4_strings_6_7z00, BgL_bgl_string3491za700za7za7_3556za7, "end2", 4 );
DEFINE_STRING( BGl_string3492z00zz__r4_strings_6_7z00, BgL_bgl_string3492za700za7za7_3557za7, "Index negative start index `", 28 );
DEFINE_STRING( BGl_string3493z00zz__r4_strings_6_7z00, BgL_bgl_string3493za700za7za7_3558za7, "start1", 6 );
DEFINE_STRING( BGl_string3494z00zz__r4_strings_6_7z00, BgL_bgl_string3494za700za7za7_3559za7, "Too large start index `", 23 );
DEFINE_STRING( BGl_string3495z00zz__r4_strings_6_7z00, BgL_bgl_string3495za700za7za7_3560za7, "start2", 6 );
DEFINE_STRING( BGl_string3497z00zz__r4_strings_6_7z00, BgL_bgl_string3497za700za7za7_3561za7, "string-prefix-length-ci::int", 28 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2suffixzf3zd2envzf3zz__r4_strings_6_7z00, BgL_bgl__stringza7d2suffix3562za7, opt_generic_entry, BGl__stringzd2suffixzf3z21zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3498z00zz__r4_strings_6_7z00, BgL_bgl_string3498za700za7za7_3563za7, "_string-prefix-length-ci", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2compare3zd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2comp3564z00, BGl_z62stringzd2compare3zb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2copyzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2copy3565z00, BGl_z62stringzd2copyzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2upcasezd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2upca3566z00, BGl_z62stringzd2upcasezb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2setzd2urz12zd2envzc0zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2setza73567za7, BGl_z62stringzd2setzd2urz12z70zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hexzd2internzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2hexza73568za7, BGl_z62stringzd2hexzd2internz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hexzd2internz12zd2envzc0zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2hexza73569za7, BGl_z62stringzd2hexzd2internz12z70zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2cizc3zd3zf3zd2envze3zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2ciza7c3570za7, BGl_z62stringzd2cizc3zd3zf3z53zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2prefixzd2lengthzd2cizd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2prefix3571za7, opt_generic_entry, BGl__stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2skipzd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2skipza7d3572z00, opt_generic_entry, BGl__stringzd2skipzd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3listzd2envze3zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2za7e3l3573za7, BGl_z62stringzd2ze3listz53zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2suffixzd2lengthzd2cizd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2suffix3574za7, opt_generic_entry, BGl__stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2stringzd2envz00zz__r4_strings_6_7z00, BgL_bgl__makeza7d2stringza7d3575z00, opt_generic_entry, BGl__makezd2stringzd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2suffixzd2lengthzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2suffix3576za7, opt_generic_entry, BGl__stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2cizd3zf3zd2envz20zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2ciza7d3577za7, BGl_z62stringzd2cizd3zf3z90zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2compare3zd2cizd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2comp3578z00, BGl_z62stringzd2compare3zd2ciz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2appendzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2appe3579z00, va_generic_entry, BGl_z62stringzd2appendzb0zz__r4_strings_6_7z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_substringzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__substringza700za7za7_3580za7, opt_generic_entry, BGl__substringz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2containszd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2contai3581za7, opt_generic_entry, BGl__stringzd2containszd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2indexzd2rightzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2indexza73582z00, opt_generic_entry, BGl__stringzd2indexzd2rightz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd3zf3zd2envzf2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d3za7f3za73583z00, BGl_z62stringzd3zf3z42zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2naturalzd2compare3zd2cizd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2natura3584za7, opt_generic_entry, BGl__stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2containszd2cizd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2contai3585za7, opt_generic_entry, BGl__stringzd2containszd2ciz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2capitaliza7ez12zd2envzb5zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2capi3586z00, BGl_z62stringzd2capitaliza7ez12z05zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2shrinkz12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2shri3587z00, BGl_z62stringzd2shrinkz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzf3zd2envz21zz__r4_strings_6_7z00, BgL_bgl_za762stringza7f3za791za73588z00, BGl_z62stringzf3z91zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_substringzd2cizd2atzf3zd2envz21zz__r4_strings_6_7z00, BgL_bgl__substringza7d2ciza73589z00, opt_generic_entry, BGl__substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2indexzd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2indexza73590z00, opt_generic_entry, BGl__stringzd2indexzd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_blitzd2stringzd2urz12zd2envzc0zz__r4_strings_6_7z00, BgL_bgl_za762blitza7d2string3591z00, BGl_z62blitzd2stringzd2urz12z70zz__r4_strings_6_7z00, 0L, BUNSPEC, 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringze3zd3zf3zd2envz11zz__r4_strings_6_7z00, BgL_bgl_za762stringza7e3za7d3za73592z00, BGl_z62stringze3zd3zf3za1zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2naturalzd2compare3zd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2natura3593za7, opt_generic_entry, BGl__stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_substringzd2atzf3zd2envzf3zz__r4_strings_6_7z00, BgL_bgl__substringza7d2atza73594z00, opt_generic_entry, BGl__substringzd2atzf3z21zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2prefixzd2cizf3zd2envz21zz__r4_strings_6_7z00, BgL_bgl__stringza7d2prefix3595za7, opt_generic_entry, BGl__stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2setz12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2setza73596za7, BGl_z62stringzd2setz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2charzd2indexzd2urzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2char3597z00, BGl_z62stringzd2charzd2indexzd2urzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2prefixzd2lengthzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2prefix3598za7, opt_generic_entry, BGl__stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3400z00zz__r4_strings_6_7z00, BgL_bgl_string3400za700za7za7_3599za7, "string-contains", 15 );
DEFINE_STRING( BGl_string3401z00zz__r4_strings_6_7z00, BgL_bgl_string3401za700za7za7_3600za7, "_string-contains", 16 );
DEFINE_STRING( BGl_string3403z00zz__r4_strings_6_7z00, BgL_bgl_string3403za700za7za7_3601za7, "string-contains-ci", 18 );
DEFINE_STRING( BGl_string3404z00zz__r4_strings_6_7z00, BgL_bgl_string3404za700za7za7_3602za7, "_string-contains-ci", 19 );
DEFINE_STRING( BGl_string3405z00zz__r4_strings_6_7z00, BgL_bgl_string3405za700za7za7_3603za7, "&string-compare3", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2cize3zf3zd2envz10zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2ciza7e3604za7, BGl_z62stringzd2cize3zf3za0zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3406z00zz__r4_strings_6_7z00, BgL_bgl_string3406za700za7za7_3605za7, "&string-compare3-ci", 19 );
DEFINE_STRING( BGl_string3407z00zz__r4_strings_6_7z00, BgL_bgl_string3407za700za7za7_3606za7, "", 0 );
DEFINE_STRING( BGl_string3408z00zz__r4_strings_6_7z00, BgL_bgl_string3408za700za7za7_3607za7, "string-append", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2splitzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2spli3608z00, va_generic_entry, BGl_z62stringzd2splitzb0zz__r4_strings_6_7z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string3409z00zz__r4_strings_6_7z00, BgL_bgl_string3409za700za7za7_3609za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_emptyzd2stringzf3zd2envzf3zz__r4_strings_6_7z00, BgL_bgl_za762emptyza7d2strin3610z00, BGl_z62emptyzd2stringzf3z43zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2refzd2urzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2refza73611za7, BGl_z62stringzd2refzd2urz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3410z00zz__r4_strings_6_7z00, BgL_bgl_string3410za700za7za7_3612za7, "loop", 4 );
DEFINE_STRING( BGl_string3411z00zz__r4_strings_6_7z00, BgL_bgl_string3411za700za7za7_3613za7, "&list->string", 13 );
DEFINE_STRING( BGl_string3412z00zz__r4_strings_6_7z00, BgL_bgl_string3412za700za7za7_3614za7, "&string->list", 13 );
DEFINE_STRING( BGl_string3413z00zz__r4_strings_6_7z00, BgL_bgl_string3413za700za7za7_3615za7, "&string-copy", 12 );
DEFINE_STRING( BGl_string3414z00zz__r4_strings_6_7z00, BgL_bgl_string3414za700za7za7_3616za7, "&string-fill!", 13 );
DEFINE_STRING( BGl_string3415z00zz__r4_strings_6_7z00, BgL_bgl_string3415za700za7za7_3617za7, "&string-upcase", 14 );
DEFINE_STRING( BGl_string3416z00zz__r4_strings_6_7z00, BgL_bgl_string3416za700za7za7_3618za7, "&string-downcase", 16 );
DEFINE_STRING( BGl_string3417z00zz__r4_strings_6_7z00, BgL_bgl_string3417za700za7za7_3619za7, "&string-upcase!", 15 );
DEFINE_STRING( BGl_string3418z00zz__r4_strings_6_7z00, BgL_bgl_string3418za700za7za7_3620za7, "&string-downcase!", 17 );
DEFINE_STRING( BGl_string3419z00zz__r4_strings_6_7z00, BgL_bgl_string3419za700za7za7_3621za7, "&string-capitalize!", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringze3zf3zd2envzc2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7e3za7f3za73622z00, BGl_z62stringze3zf3z72zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3500z00zz__r4_strings_6_7z00, BgL_bgl_string3500za700za7za7_3623za7, "string-prefix-length-ci", 23 );
DEFINE_STRING( BGl_string3420z00zz__r4_strings_6_7z00, BgL_bgl_string3420za700za7za7_3624za7, "&string-capitalize", 18 );
DEFINE_STRING( BGl_string3502z00zz__r4_strings_6_7z00, BgL_bgl_string3502za700za7za7_3625za7, "string-suffix-length::int", 25 );
DEFINE_STRING( BGl_string3421z00zz__r4_strings_6_7z00, BgL_bgl_string3421za700za7za7_3626za7, "&string-for-read", 16 );
DEFINE_STRING( BGl_string3503z00zz__r4_strings_6_7z00, BgL_bgl_string3503za700za7za7_3627za7, "_string-suffix-length", 21 );
DEFINE_STRING( BGl_string3422z00zz__r4_strings_6_7z00, BgL_bgl_string3422za700za7za7_3628za7, "&escape-C-string", 16 );
DEFINE_STRING( BGl_string3423z00zz__r4_strings_6_7z00, BgL_bgl_string3423za700za7za7_3629za7, "&escape-scheme-string", 21 );
DEFINE_STRING( BGl_string3505z00zz__r4_strings_6_7z00, BgL_bgl_string3505za700za7za7_3630za7, "string-suffix-length", 20 );
DEFINE_STRING( BGl_string3424z00zz__r4_strings_6_7z00, BgL_bgl_string3424za700za7za7_3631za7, "&string-as-read", 15 );
DEFINE_STRING( BGl_string3425z00zz__r4_strings_6_7z00, BgL_bgl_string3425za700za7za7_3632za7, "&blit-string-ur!", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzc3zd3zf3zd2envz31zz__r4_strings_6_7z00, BgL_bgl_za762stringza7c3za7d3za73633z00, BGl_z62stringzc3zd3zf3z81zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3507z00zz__r4_strings_6_7z00, BgL_bgl_string3507za700za7za7_3634za7, "string-suffix-length-ci::int", 28 );
DEFINE_STRING( BGl_string3426z00zz__r4_strings_6_7z00, BgL_bgl_string3426za700za7za7_3635za7, "]", 1 );
DEFINE_STRING( BGl_string3508z00zz__r4_strings_6_7z00, BgL_bgl_string3508za700za7za7_3636za7, "_string-suffix-length-ci", 24 );
DEFINE_STRING( BGl_string3427z00zz__r4_strings_6_7z00, BgL_bgl_string3427za700za7za7_3637za7, "] [dest:", 8 );
DEFINE_STRING( BGl_string3428z00zz__r4_strings_6_7z00, BgL_bgl_string3428za700za7za7_3638za7, "[src:", 5 );
DEFINE_STRING( BGl_string3429z00zz__r4_strings_6_7z00, BgL_bgl_string3429za700za7za7_3639za7, "blit-string!:Index and length out of range", 42 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2replacez12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2repl3640z00, BGl_z62stringzd2replacez12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2skipzd2rightzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2skipza7d3641z00, opt_generic_entry, BGl__stringzd2skipzd2rightz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2lengthzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2leng3642z00, BGl_z62stringzd2lengthzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3510z00zz__r4_strings_6_7z00, BgL_bgl_string3510za700za7za7_3643za7, "string-suffix-length-ci", 23 );
DEFINE_STRING( BGl_string3430z00zz__r4_strings_6_7z00, BgL_bgl_string3430za700za7za7_3644za7, "&blit-string!", 13 );
DEFINE_STRING( BGl_string3512z00zz__r4_strings_6_7z00, BgL_bgl_string3512za700za7za7_3645za7, "string-prefix?::bool", 20 );
DEFINE_STRING( BGl_string3431z00zz__r4_strings_6_7z00, BgL_bgl_string3431za700za7za7_3646za7, "&string-shrink!", 15 );
DEFINE_STRING( BGl_string3513z00zz__r4_strings_6_7z00, BgL_bgl_string3513za700za7za7_3647za7, "_string-prefix?", 15 );
DEFINE_STRING( BGl_string3432z00zz__r4_strings_6_7z00, BgL_bgl_string3432za700za7za7_3648za7, "&string-replace", 15 );
DEFINE_STRING( BGl_string3433z00zz__r4_strings_6_7z00, BgL_bgl_string3433za700za7za7_3649za7, "&string-replace!", 16 );
DEFINE_STRING( BGl_string3515z00zz__r4_strings_6_7z00, BgL_bgl_string3515za700za7za7_3650za7, "string-prefix?", 14 );
DEFINE_STRING( BGl_string3435z00zz__r4_strings_6_7z00, BgL_bgl_string3435za700za7za7_3651za7, "string-delete", 13 );
DEFINE_STRING( BGl_string3354z00zz__r4_strings_6_7z00, BgL_bgl_string3354za700za7za7_3652za7, "/tmp/bigloo/runtime/Ieee/string.scm", 35 );
DEFINE_STRING( BGl_string3517z00zz__r4_strings_6_7z00, BgL_bgl_string3517za700za7za7_3653za7, "string-prefix-ci?::bool", 23 );
DEFINE_STRING( BGl_string3436z00zz__r4_strings_6_7z00, BgL_bgl_string3436za700za7za7_3654za7, "wrong number of arguments: [2..4] expected, provided", 52 );
DEFINE_STRING( BGl_string3355z00zz__r4_strings_6_7z00, BgL_bgl_string3355za700za7za7_3655za7, "&string-null?", 13 );
DEFINE_STRING( BGl_string3518z00zz__r4_strings_6_7z00, BgL_bgl_string3518za700za7za7_3656za7, "_string-prefix-ci?", 18 );
DEFINE_STRING( BGl_string3437z00zz__r4_strings_6_7z00, BgL_bgl_string3437za700za7za7_3657za7, "_string-delete", 14 );
DEFINE_STRING( BGl_string3356z00zz__r4_strings_6_7z00, BgL_bgl_string3356za700za7za7_3658za7, "bstring", 7 );
DEFINE_STRING( BGl_string3438z00zz__r4_strings_6_7z00, BgL_bgl_string3438za700za7za7_3659za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string3358z00zz__r4_strings_6_7z00, BgL_bgl_string3358za700za7za7_3660za7, "make-string", 11 );
DEFINE_STRING( BGl_string3359z00zz__r4_strings_6_7z00, BgL_bgl_string3359za700za7za7_3661za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762stringza762za7za7__3662z00, va_generic_entry, BGl_z62stringz62zz__r4_strings_6_7z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2charzd2indexzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2charza7d3663z00, opt_generic_entry, BGl__stringzd2charzd2indexz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3520z00zz__r4_strings_6_7z00, BgL_bgl_string3520za700za7za7_3664za7, "string-prefix-ci?", 17 );
DEFINE_STRING( BGl_string3522z00zz__r4_strings_6_7z00, BgL_bgl_string3522za700za7za7_3665za7, "string-suffix?::bool", 20 );
DEFINE_STRING( BGl_string3441z00zz__r4_strings_6_7z00, BgL_bgl_string3441za700za7za7_3666za7, "funcall", 7 );
DEFINE_STRING( BGl_string3360z00zz__r4_strings_6_7z00, BgL_bgl_string3360za700za7za7_3667za7, "_make-string", 12 );
DEFINE_STRING( BGl_string3523z00zz__r4_strings_6_7z00, BgL_bgl_string3523za700za7za7_3668za7, "_string-suffix?", 15 );
DEFINE_STRING( BGl_string3361z00zz__r4_strings_6_7z00, BgL_bgl_string3361za700za7za7_3669za7, "bint", 4 );
DEFINE_STRING( BGl_string3443z00zz__r4_strings_6_7z00, BgL_bgl_string3443za700za7za7_3670za7, "pred", 4 );
DEFINE_STRING( BGl_string3362z00zz__r4_strings_6_7z00, BgL_bgl_string3362za700za7za7_3671za7, "bchar", 5 );
DEFINE_STRING( BGl_string3525z00zz__r4_strings_6_7z00, BgL_bgl_string3525za700za7za7_3672za7, "string-suffix?", 14 );
DEFINE_STRING( BGl_string3363z00zz__r4_strings_6_7z00, BgL_bgl_string3363za700za7za7_3673za7, "string", 6 );
DEFINE_STRING( BGl_string3445z00zz__r4_strings_6_7z00, BgL_bgl_string3445za700za7za7_3674za7, "cc", 2 );
DEFINE_STRING( BGl_string3364z00zz__r4_strings_6_7z00, BgL_bgl_string3364za700za7za7_3675za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string3527z00zz__r4_strings_6_7z00, BgL_bgl_string3527za700za7za7_3676za7, "string-suffix-ci?::bool", 23 );
DEFINE_STRING( BGl_string3446z00zz__r4_strings_6_7z00, BgL_bgl_string3446za700za7za7_3677za7, "start index out of range", 24 );
DEFINE_STRING( BGl_string3365z00zz__r4_strings_6_7z00, BgL_bgl_string3365za700za7za7_3678za7, "&string-length", 14 );
DEFINE_STRING( BGl_string3528z00zz__r4_strings_6_7z00, BgL_bgl_string3528za700za7za7_3679za7, "_string-suffix-ci?", 18 );
DEFINE_STRING( BGl_string3447z00zz__r4_strings_6_7z00, BgL_bgl_string3447za700za7za7_3680za7, "end index out of range", 22 );
DEFINE_STRING( BGl_string3366z00zz__r4_strings_6_7z00, BgL_bgl_string3366za700za7za7_3681za7, "string-ref", 10 );
DEFINE_STRING( BGl_string3529z00zz__r4_strings_6_7z00, BgL_bgl_string3529za700za7za7_3682za7, "string-suffix-ci?", 17 );
DEFINE_STRING( BGl_string3448z00zz__r4_strings_6_7z00, BgL_bgl_string3448za700za7za7_3683za7, "Illegal indices", 15 );
DEFINE_STRING( BGl_string3367z00zz__r4_strings_6_7z00, BgL_bgl_string3367za700za7za7_3684za7, "&string-ref", 11 );
DEFINE_STRING( BGl_string3449z00zz__r4_strings_6_7z00, BgL_bgl_string3449za700za7za7_3685za7, "Illegal char/charset/predicate", 30 );
DEFINE_STRING( BGl_string3368z00zz__r4_strings_6_7z00, BgL_bgl_string3368za700za7za7_3686za7, "string-set!", 11 );
DEFINE_STRING( BGl_string3369z00zz__r4_strings_6_7z00, BgL_bgl_string3369za700za7za7_3687za7, "&string-set!", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2deletezd2envz00zz__r4_strings_6_7z00, BgL_bgl__stringza7d2delete3688za7, opt_generic_entry, BGl__stringzd2deletezd2zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2nullzf3zd2envzf3zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2null3689z00, BGl_z62stringzd2nullzf3z43zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_escapezd2Czd2stringzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762escapeza7d2cza7d23690za7, BGl_z62escapezd2Czd2stringz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3531z00zz__r4_strings_6_7z00, BgL_bgl_string3531za700za7za7_3691za7, "string-natural-compare3", 23 );
DEFINE_STRING( BGl_string3450z00zz__r4_strings_6_7z00, BgL_bgl_string3450za700za7za7_3692za7, "delim?", 6 );
DEFINE_STRING( BGl_string3532z00zz__r4_strings_6_7z00, BgL_bgl_string3532za700za7za7_3693za7, "_string-natural-compare3", 24 );
DEFINE_STRING( BGl_string3451z00zz__r4_strings_6_7z00, BgL_bgl_string3451za700za7za7_3694za7, " \t\n", 3 );
DEFINE_STRING( BGl_string3370z00zz__r4_strings_6_7z00, BgL_bgl_string3370za700za7za7_3695za7, "&string-ref-ur", 14 );
DEFINE_STRING( BGl_string3452z00zz__r4_strings_6_7z00, BgL_bgl_string3452za700za7za7_3696za7, "&string-split", 13 );
DEFINE_STRING( BGl_string3371z00zz__r4_strings_6_7z00, BgL_bgl_string3371za700za7za7_3697za7, "&string-set-ur!", 15 );
DEFINE_STRING( BGl_string3534z00zz__r4_strings_6_7z00, BgL_bgl_string3534za700za7za7_3698za7, "string-natural-compare3-ci", 26 );
DEFINE_STRING( BGl_string3453z00zz__r4_strings_6_7z00, BgL_bgl_string3453za700za7za7_3699za7, "&string-cut", 11 );
DEFINE_STRING( BGl_string3372z00zz__r4_strings_6_7z00, BgL_bgl_string3372za700za7za7_3700za7, "&string=?", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2cutzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2cutza73701za7, va_generic_entry, BGl_z62stringzd2cutzb0zz__r4_strings_6_7z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string3535z00zz__r4_strings_6_7z00, BgL_bgl_string3535za700za7za7_3702za7, "_string-natural-compare3-ci", 27 );
DEFINE_STRING( BGl_string3454z00zz__r4_strings_6_7z00, BgL_bgl_string3454za700za7za7_3703za7, "&string-char-index-ur", 21 );
DEFINE_STRING( BGl_string3373z00zz__r4_strings_6_7z00, BgL_bgl_string3373za700za7za7_3704za7, "&substring=?", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2suffixzd2cizf3zd2envz21zz__r4_strings_6_7z00, BgL_bgl__stringza7d2suffix3705za7, opt_generic_entry, BGl__stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3536z00zz__r4_strings_6_7z00, BgL_bgl_string3536za700za7za7_3706za7, "while", 5 );
DEFINE_STRING( BGl_string3456z00zz__r4_strings_6_7z00, BgL_bgl_string3456za700za7za7_3707za7, "string-char-index", 17 );
DEFINE_STRING( BGl_string3375z00zz__r4_strings_6_7z00, BgL_bgl_string3375za700za7za7_3708za7, "substring-at?", 13 );
DEFINE_STRING( BGl_string3538z00zz__r4_strings_6_7z00, BgL_bgl_string3538za700za7za7_3709za7, "an-awful-hack", 13 );
DEFINE_STRING( BGl_string3457z00zz__r4_strings_6_7z00, BgL_bgl_string3457za700za7za7_3710za7, "_string-char-index", 18 );
DEFINE_STRING( BGl_string3376z00zz__r4_strings_6_7z00, BgL_bgl_string3376za700za7za7_3711za7, "wrong number of arguments: [3..4] expected, provided", 52 );
DEFINE_STRING( BGl_string3539z00zz__r4_strings_6_7z00, BgL_bgl_string3539za700za7za7_3712za7, "hex-string->string", 18 );
DEFINE_STRING( BGl_string3377z00zz__r4_strings_6_7z00, BgL_bgl_string3377za700za7za7_3713za7, "_substring-at?", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_substringzd2urzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762substringza7d2u3714z00, BGl_z62substringzd2urzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3459z00zz__r4_strings_6_7z00, BgL_bgl_string3459za700za7za7_3715za7, "string-index", 12 );
DEFINE_STRING( BGl_string3378z00zz__r4_strings_6_7z00, BgL_bgl_string3378za700za7za7_3716za7, "&substring-ci=?", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_substringzd2cizd3zf3zd2envz20zz__r4_strings_6_7z00, BgL_bgl_za762substringza7d2c3717z00, BGl_z62substringzd2cizd3zf3z90zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_escapezd2schemezd2stringzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762escapeza7d2sche3718z00, BGl_z62escapezd2schemezd2stringz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2cize3zd3zf3zd2envzc3zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2ciza7e3719za7, BGl_z62stringzd2cize3zd3zf3z73zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2cizc3zf3zd2envz30zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2ciza7c3720za7, BGl_z62stringzd2cizc3zf3z80zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hexzd2externzd2envzd2zz__r4_strings_6_7z00, BgL_bgl__stringza7d2hexza7d23721z00, opt_generic_entry, BGl__stringzd2hexzd2externz00zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3540z00zz__r4_strings_6_7z00, BgL_bgl_string3540za700za7za7_3722za7, "Illegal string (illegal character)", 34 );
DEFINE_STRING( BGl_string3541z00zz__r4_strings_6_7z00, BgL_bgl_string3541za700za7za7_3723za7, "string-hex-intern", 17 );
DEFINE_STRING( BGl_string3460z00zz__r4_strings_6_7z00, BgL_bgl_string3460za700za7za7_3724za7, "_string-index", 13 );
DEFINE_STRING( BGl_string3542z00zz__r4_strings_6_7z00, BgL_bgl_string3542za700za7za7_3725za7, "Illegal string (length is odd)", 30 );
DEFINE_STRING( BGl_string3461z00zz__r4_strings_6_7z00, BgL_bgl_string3461za700za7za7_3726za7, "liip", 4 );
DEFINE_STRING( BGl_string3380z00zz__r4_strings_6_7z00, BgL_bgl_string3380za700za7za7_3727za7, "substring-ci-at?", 16 );
DEFINE_STRING( BGl_string3543z00zz__r4_strings_6_7z00, BgL_bgl_string3543za700za7za7_3728za7, "&string-hex-intern", 18 );
DEFINE_STRING( BGl_string3462z00zz__r4_strings_6_7z00, BgL_bgl_string3462za700za7za7_3729za7, "Illegal regset", 14 );
DEFINE_STRING( BGl_string3381z00zz__r4_strings_6_7z00, BgL_bgl_string3381za700za7za7_3730za7, "_substring-ci-at?", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_blitzd2stringz12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762blitza7d2string3731z00, BGl_z62blitzd2stringz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 5 );
DEFINE_STRING( BGl_string3544z00zz__r4_strings_6_7z00, BgL_bgl_string3544za700za7za7_3732za7, "string-hex-intern!", 18 );
DEFINE_STRING( BGl_string3382z00zz__r4_strings_6_7z00, BgL_bgl_string3382za700za7za7_3733za7, "&empty-string?", 14 );
DEFINE_STRING( BGl_string3545z00zz__r4_strings_6_7z00, BgL_bgl_string3545za700za7za7_3734za7, "&string-hex-intern!", 19 );
DEFINE_STRING( BGl_string3464z00zz__r4_strings_6_7z00, BgL_bgl_string3464za700za7za7_3735za7, "string-index-right", 18 );
DEFINE_STRING( BGl_string3383z00zz__r4_strings_6_7z00, BgL_bgl_string3383za700za7za7_3736za7, "&string-ci=?", 12 );
DEFINE_STRING( BGl_string3465z00zz__r4_strings_6_7z00, BgL_bgl_string3465za700za7za7_3737za7, "_string-index-right", 19 );
DEFINE_STRING( BGl_string3384z00zz__r4_strings_6_7z00, BgL_bgl_string3384za700za7za7_3738za7, "&string<?", 9 );
DEFINE_STRING( BGl_string3547z00zz__r4_strings_6_7z00, BgL_bgl_string3547za700za7za7_3739za7, "string-hex-extern::bstring", 26 );
DEFINE_STRING( BGl_string3466z00zz__r4_strings_6_7z00, BgL_bgl_string3466za700za7za7_3740za7, "index out of bound", 18 );
DEFINE_STRING( BGl_string3385z00zz__r4_strings_6_7z00, BgL_bgl_string3385za700za7za7_3741za7, "&string>?", 9 );
DEFINE_STRING( BGl_string3548z00zz__r4_strings_6_7z00, BgL_bgl_string3548za700za7za7_3742za7, "wrong number of arguments: [1..3] expected, provided", 52 );
DEFINE_STRING( BGl_string3386z00zz__r4_strings_6_7z00, BgL_bgl_string3386za700za7za7_3743za7, "&string<=?", 10 );
DEFINE_STRING( BGl_string3549z00zz__r4_strings_6_7z00, BgL_bgl_string3549za700za7za7_3744za7, "_string-hex-extern", 18 );
DEFINE_STRING( BGl_string3468z00zz__r4_strings_6_7z00, BgL_bgl_string3468za700za7za7_3745za7, "string-skip", 11 );
DEFINE_STRING( BGl_string3387z00zz__r4_strings_6_7z00, BgL_bgl_string3387za700za7za7_3746za7, "&string>=?", 10 );
DEFINE_STRING( BGl_string3469z00zz__r4_strings_6_7z00, BgL_bgl_string3469za700za7za7_3747za7, "_string-skip", 12 );
DEFINE_STRING( BGl_string3388z00zz__r4_strings_6_7z00, BgL_bgl_string3388za700za7za7_3748za7, "&string-ci<?", 12 );
DEFINE_STRING( BGl_string3389z00zz__r4_strings_6_7z00, BgL_bgl_string3389za700za7za7_3749za7, "&string-ci>?", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2downcasezd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2down3750z00, BGl_z62stringzd2downcasezb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3stringzd2envze3zz__r4_strings_6_7z00, BgL_bgl_za762listza7d2za7e3str3751za7, BGl_z62listzd2ze3stringz53zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2aszd2readzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2asza7d3752za7, BGl_z62stringzd2aszd2readz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2refzd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2refza73753za7, BGl_z62stringzd2refzb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2fillz12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2fill3754z00, BGl_z62stringzd2fillz12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3550z00zz__r4_strings_6_7z00, BgL_bgl_string3550za700za7za7_3755za7, "string-hex-extern", 17 );
DEFINE_STRING( BGl_string3551z00zz__r4_strings_6_7z00, BgL_bgl_string3551za700za7za7_3756za7, "0123456789abcdef", 16 );
DEFINE_STRING( BGl_string3552z00zz__r4_strings_6_7z00, BgL_bgl_string3552za700za7za7_3757za7, "__r4_strings_6_7", 16 );
DEFINE_STRING( BGl_string3390z00zz__r4_strings_6_7z00, BgL_bgl_string3390za700za7za7_3758za7, "&string-ci<=?", 13 );
DEFINE_STRING( BGl_string3472z00zz__r4_strings_6_7z00, BgL_bgl_string3472za700za7za7_3759za7, "arg1702", 7 );
DEFINE_STRING( BGl_string3391z00zz__r4_strings_6_7z00, BgL_bgl_string3391za700za7za7_3760za7, "&string-ci>=?", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzc3zf3zd2envze2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7c3za7f3za73761z00, BGl_z62stringzc3zf3z52zz__r4_strings_6_7z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3474z00zz__r4_strings_6_7z00, BgL_bgl_string3474za700za7za7_3762za7, "string-skip-right", 17 );
DEFINE_STRING( BGl_string3393z00zz__r4_strings_6_7z00, BgL_bgl_string3393za700za7za7_3763za7, "substring", 9 );
DEFINE_STRING( BGl_string3475z00zz__r4_strings_6_7z00, BgL_bgl_string3475za700za7za7_3764za7, "_string-skip-right", 18 );
DEFINE_STRING( BGl_string3394z00zz__r4_strings_6_7z00, BgL_bgl_string3394za700za7za7_3765za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_STRING( BGl_string3476z00zz__r4_strings_6_7z00, BgL_bgl_string3476za700za7za7_3766za7, "string-char-skip", 16 );
DEFINE_STRING( BGl_string3395z00zz__r4_strings_6_7z00, BgL_bgl_string3395za700za7za7_3767za7, "_substring", 10 );
DEFINE_STRING( BGl_string3477z00zz__r4_strings_6_7z00, BgL_bgl_string3477za700za7za7_3768za7, "string-pred-skip", 16 );
DEFINE_STRING( BGl_string3396z00zz__r4_strings_6_7z00, BgL_bgl_string3396za700za7za7_3769za7, "Illegal start index ", 20 );
DEFINE_STRING( BGl_string3397z00zz__r4_strings_6_7z00, BgL_bgl_string3397za700za7za7_3770za7, "Illegal end index ", 18 );
DEFINE_STRING( BGl_string3398z00zz__r4_strings_6_7z00, BgL_bgl_string3398za700za7za7_3771za7, "&substring-ur", 13 );
DEFINE_STRING( BGl_string3480z00zz__r4_strings_6_7z00, BgL_bgl_string3480za700za7za7_3772za7, "arg1758", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2prefixzf3zd2envzf3zz__r4_strings_6_7z00, BgL_bgl__stringza7d2prefix3773za7, opt_generic_entry, BGl__stringzd2prefixzf3z21zz__r4_strings_6_7z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3482z00zz__r4_strings_6_7z00, BgL_bgl_string3482za700za7za7_3774za7, "string-prefix-length::int", 25 );
DEFINE_STRING( BGl_string3483z00zz__r4_strings_6_7z00, BgL_bgl_string3483za700za7za7_3775za7, "wrong number of arguments: [2..6] expected, provided", 52 );
DEFINE_STRING( BGl_string3484z00zz__r4_strings_6_7z00, BgL_bgl_string3484za700za7za7_3776za7, "_string-prefix-length", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2forzd2readzd2envzd2zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2forza73777za7, BGl_z62stringzd2forzd2readz62zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3486z00zz__r4_strings_6_7z00, BgL_bgl_string3486za700za7za7_3778za7, "string-prefix-length", 20 );
DEFINE_STRING( BGl_string3487z00zz__r4_strings_6_7z00, BgL_bgl_string3487za700za7za7_3779za7, "Index negative end index `", 26 );
DEFINE_STRING( BGl_string3488z00zz__r4_strings_6_7z00, BgL_bgl_string3488za700za7za7_3780za7, "end1", 4 );
DEFINE_STRING( BGl_string3489z00zz__r4_strings_6_7z00, BgL_bgl_string3489za700za7za7_3781za7, "'", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2downcasez12zd2envz12zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2down3782z00, BGl_z62stringzd2downcasez12za2zz__r4_strings_6_7z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_substringzd3zf3zd2envzf2zz__r4_strings_6_7z00, BgL_bgl_za762substringza7d3za73783za7, BGl_z62substringzd3zf3z42zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2replacezd2envz00zz__r4_strings_6_7z00, BgL_bgl_za762stringza7d2repl3784z00, BGl_z62stringzd2replacezb0zz__r4_strings_6_7z00, 0L, BUNSPEC, 3 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol3530z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3533z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3455z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3374z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3537z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3458z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3379z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3463z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3546z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3467z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_list3439z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3471z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3473z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3392z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3479z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3399z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3481z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3485z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3496z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3499z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_list3470z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_list3478z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3402z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3501z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3504z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3506z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3509z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3511z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3514z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3434z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3516z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3519z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3357z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3521z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3440z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3442z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3524z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3444z00zz__r4_strings_6_7z00) );
ADD_ROOT( (void *)(&BGl_symbol3526z00zz__r4_strings_6_7z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long BgL_checksumz00_5391, char * BgL_fromz00_5392)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_strings_6_7z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_strings_6_7z00(); 
BGl_cnstzd2initzd2zz__r4_strings_6_7z00(); 
return 
BGl_importedzd2moduleszd2initz00zz__r4_strings_6_7z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_strings_6_7z00(void)
{
{ /* Ieee/string.scm 18 */
BGl_symbol3357z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3358z00zz__r4_strings_6_7z00); 
BGl_symbol3374z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3375z00zz__r4_strings_6_7z00); 
BGl_symbol3379z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3380z00zz__r4_strings_6_7z00); 
BGl_symbol3392z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3393z00zz__r4_strings_6_7z00); 
BGl_symbol3399z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3400z00zz__r4_strings_6_7z00); 
BGl_symbol3402z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3403z00zz__r4_strings_6_7z00); 
BGl_symbol3434z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3435z00zz__r4_strings_6_7z00); 
BGl_symbol3440z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3441z00zz__r4_strings_6_7z00); 
BGl_symbol3442z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3443z00zz__r4_strings_6_7z00); 
BGl_symbol3444z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3445z00zz__r4_strings_6_7z00); 
BGl_list3439z00zz__r4_strings_6_7z00 = 
MAKE_YOUNG_PAIR(BGl_symbol3440z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3442z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3442z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3444z00zz__r4_strings_6_7z00, BNIL)))); 
BGl_symbol3455z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3456z00zz__r4_strings_6_7z00); 
BGl_symbol3458z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3459z00zz__r4_strings_6_7z00); 
BGl_symbol3463z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3464z00zz__r4_strings_6_7z00); 
BGl_symbol3467z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3468z00zz__r4_strings_6_7z00); 
BGl_symbol3471z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3472z00zz__r4_strings_6_7z00); 
BGl_list3470z00zz__r4_strings_6_7z00 = 
MAKE_YOUNG_PAIR(BGl_symbol3440z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3442z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3442z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3471z00zz__r4_strings_6_7z00, BNIL)))); 
BGl_symbol3473z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3474z00zz__r4_strings_6_7z00); 
BGl_symbol3479z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3480z00zz__r4_strings_6_7z00); 
BGl_list3478z00zz__r4_strings_6_7z00 = 
MAKE_YOUNG_PAIR(BGl_symbol3440z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3442z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3442z00zz__r4_strings_6_7z00, 
MAKE_YOUNG_PAIR(BGl_symbol3479z00zz__r4_strings_6_7z00, BNIL)))); 
BGl_symbol3481z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3482z00zz__r4_strings_6_7z00); 
BGl_symbol3485z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3486z00zz__r4_strings_6_7z00); 
BGl_symbol3496z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3497z00zz__r4_strings_6_7z00); 
BGl_symbol3499z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3500z00zz__r4_strings_6_7z00); 
BGl_symbol3501z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3502z00zz__r4_strings_6_7z00); 
BGl_symbol3504z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3505z00zz__r4_strings_6_7z00); 
BGl_symbol3506z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3507z00zz__r4_strings_6_7z00); 
BGl_symbol3509z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3510z00zz__r4_strings_6_7z00); 
BGl_symbol3511z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3512z00zz__r4_strings_6_7z00); 
BGl_symbol3514z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3515z00zz__r4_strings_6_7z00); 
BGl_symbol3516z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3517z00zz__r4_strings_6_7z00); 
BGl_symbol3519z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3520z00zz__r4_strings_6_7z00); 
BGl_symbol3521z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3522z00zz__r4_strings_6_7z00); 
BGl_symbol3524z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3525z00zz__r4_strings_6_7z00); 
BGl_symbol3526z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3527z00zz__r4_strings_6_7z00); 
BGl_symbol3530z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3531z00zz__r4_strings_6_7z00); 
BGl_symbol3533z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3534z00zz__r4_strings_6_7z00); 
BGl_symbol3537z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3538z00zz__r4_strings_6_7z00); 
return ( 
BGl_symbol3546z00zz__r4_strings_6_7z00 = 
bstring_to_symbol(BGl_string3547z00zz__r4_strings_6_7z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_strings_6_7z00(void)
{
{ /* Ieee/string.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* string? */
BGL_EXPORTED_DEF bool_t BGl_stringzf3zf3zz__r4_strings_6_7z00(obj_t BgL_objz00_3)
{
{ /* Ieee/string.scm 300 */
return 
STRINGP(BgL_objz00_3);} 

}



/* &string? */
obj_t BGl_z62stringzf3z91zz__r4_strings_6_7z00(obj_t BgL_envz00_4036, obj_t BgL_objz00_4037)
{
{ /* Ieee/string.scm 300 */
return 
BBOOL(
BGl_stringzf3zf3zz__r4_strings_6_7z00(BgL_objz00_4037));} 

}



/* string-null? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2nullzf3z21zz__r4_strings_6_7z00(obj_t BgL_strz00_4)
{
{ /* Ieee/string.scm 306 */
return 
(
STRING_LENGTH(BgL_strz00_4)==0L);} 

}



/* &string-null? */
obj_t BGl_z62stringzd2nullzf3z43zz__r4_strings_6_7z00(obj_t BgL_envz00_4038, obj_t BgL_strz00_4039)
{
{ /* Ieee/string.scm 306 */
{ /* Ieee/string.scm 307 */
 bool_t BgL_tmpz00_5453;
{ /* Ieee/string.scm 307 */
 obj_t BgL_auxz00_5454;
if(
STRINGP(BgL_strz00_4039))
{ /* Ieee/string.scm 307 */
BgL_auxz00_5454 = BgL_strz00_4039
; }  else 
{ 
 obj_t BgL_auxz00_5457;
BgL_auxz00_5457 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15062L), BGl_string3355z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4039); 
FAILURE(BgL_auxz00_5457,BFALSE,BFALSE);} 
BgL_tmpz00_5453 = 
BGl_stringzd2nullzf3z21zz__r4_strings_6_7z00(BgL_auxz00_5454); } 
return 
BBOOL(BgL_tmpz00_5453);} } 

}



/* _make-string */
obj_t BGl__makezd2stringzd2zz__r4_strings_6_7z00(obj_t BgL_env1093z00_8, obj_t BgL_opt1092z00_7)
{
{ /* Ieee/string.scm 312 */
{ /* Ieee/string.scm 312 */
 obj_t BgL_g1094z00_5351;
BgL_g1094z00_5351 = 
VECTOR_REF(BgL_opt1092z00_7,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1092z00_7)) { case 1L : 

{ /* Ieee/string.scm 312 */

{ /* Ieee/string.scm 312 */
 long BgL_kz00_5353;
{ /* Ieee/string.scm 312 */
 obj_t BgL_tmpz00_5464;
if(
INTEGERP(BgL_g1094z00_5351))
{ /* Ieee/string.scm 312 */
BgL_tmpz00_5464 = BgL_g1094z00_5351
; }  else 
{ 
 obj_t BgL_auxz00_5467;
BgL_auxz00_5467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15309L), BGl_string3360z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1094z00_5351); 
FAILURE(BgL_auxz00_5467,BFALSE,BFALSE);} 
BgL_kz00_5353 = 
(long)CINT(BgL_tmpz00_5464); } 
return 
make_string(BgL_kz00_5353, ((unsigned char)' '));} } break;case 2L : 

{ /* Ieee/string.scm 312 */
 obj_t BgL_charz00_5354;
BgL_charz00_5354 = 
VECTOR_REF(BgL_opt1092z00_7,1L); 
{ /* Ieee/string.scm 312 */

{ /* Ieee/string.scm 312 */
 long BgL_kz00_5355;
{ /* Ieee/string.scm 312 */
 obj_t BgL_tmpz00_5474;
if(
INTEGERP(BgL_g1094z00_5351))
{ /* Ieee/string.scm 312 */
BgL_tmpz00_5474 = BgL_g1094z00_5351
; }  else 
{ 
 obj_t BgL_auxz00_5477;
BgL_auxz00_5477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15309L), BGl_string3360z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1094z00_5351); 
FAILURE(BgL_auxz00_5477,BFALSE,BFALSE);} 
BgL_kz00_5355 = 
(long)CINT(BgL_tmpz00_5474); } 
{ /* Ieee/string.scm 313 */
 unsigned char BgL_tmpz00_5482;
{ /* Ieee/string.scm 313 */
 obj_t BgL_tmpz00_5483;
if(
CHARP(BgL_charz00_5354))
{ /* Ieee/string.scm 313 */
BgL_tmpz00_5483 = BgL_charz00_5354
; }  else 
{ 
 obj_t BgL_auxz00_5486;
BgL_auxz00_5486 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15385L), BGl_string3360z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_charz00_5354); 
FAILURE(BgL_auxz00_5486,BFALSE,BFALSE);} 
BgL_tmpz00_5482 = 
CCHAR(BgL_tmpz00_5483); } 
return 
make_string(BgL_kz00_5355, BgL_tmpz00_5482);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3357z00zz__r4_strings_6_7z00, BGl_string3359z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1092z00_7)));} } } } 

}



/* make-string */
BGL_EXPORTED_DEF obj_t BGl_makezd2stringzd2zz__r4_strings_6_7z00(long BgL_kz00_5, obj_t BgL_charz00_6)
{
{ /* Ieee/string.scm 312 */
{ /* Ieee/string.scm 313 */
 unsigned char BgL_tmpz00_5497;
{ /* Ieee/string.scm 313 */
 obj_t BgL_tmpz00_5498;
if(
CHARP(BgL_charz00_6))
{ /* Ieee/string.scm 313 */
BgL_tmpz00_5498 = BgL_charz00_6
; }  else 
{ 
 obj_t BgL_auxz00_5501;
BgL_auxz00_5501 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15385L), BGl_string3358z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_charz00_6); 
FAILURE(BgL_auxz00_5501,BFALSE,BFALSE);} 
BgL_tmpz00_5497 = 
CCHAR(BgL_tmpz00_5498); } 
return 
make_string(BgL_kz00_5, BgL_tmpz00_5497);} } 

}



/* string */
BGL_EXPORTED_DEF obj_t BGl_stringz00zz__r4_strings_6_7z00(obj_t BgL_charsz00_9)
{
{ /* Ieee/string.scm 318 */
{ /* Ieee/string.scm 319 */
 obj_t BgL_auxz00_5507;
{ /* Ieee/string.scm 319 */
 bool_t BgL_test3791z00_5508;
if(
PAIRP(BgL_charsz00_9))
{ /* Ieee/string.scm 319 */
BgL_test3791z00_5508 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 319 */
BgL_test3791z00_5508 = 
NULLP(BgL_charsz00_9)
; } 
if(BgL_test3791z00_5508)
{ /* Ieee/string.scm 319 */
BgL_auxz00_5507 = BgL_charsz00_9
; }  else 
{ 
 obj_t BgL_auxz00_5512;
BgL_auxz00_5512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15665L), BGl_string3363z00zz__r4_strings_6_7z00, BGl_string3364z00zz__r4_strings_6_7z00, BgL_charsz00_9); 
FAILURE(BgL_auxz00_5512,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_auxz00_5507);} } 

}



/* &string */
obj_t BGl_z62stringz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4040, obj_t BgL_charsz00_4041)
{
{ /* Ieee/string.scm 318 */
return 
BGl_stringz00zz__r4_strings_6_7z00(BgL_charsz00_4041);} 

}



/* string-length */
BGL_EXPORTED_DEF long BGl_stringzd2lengthzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_10)
{
{ /* Ieee/string.scm 324 */
return 
STRING_LENGTH(BgL_stringz00_10);} 

}



/* &string-length */
obj_t BGl_z62stringzd2lengthzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4042, obj_t BgL_stringz00_4043)
{
{ /* Ieee/string.scm 324 */
{ /* Ieee/string.scm 325 */
 long BgL_tmpz00_5519;
{ /* Ieee/string.scm 325 */
 obj_t BgL_auxz00_5520;
if(
STRINGP(BgL_stringz00_4043))
{ /* Ieee/string.scm 325 */
BgL_auxz00_5520 = BgL_stringz00_4043
; }  else 
{ 
 obj_t BgL_auxz00_5523;
BgL_auxz00_5523 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(15937L), BGl_string3365z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4043); 
FAILURE(BgL_auxz00_5523,BFALSE,BFALSE);} 
BgL_tmpz00_5519 = 
BGl_stringzd2lengthzd2zz__r4_strings_6_7z00(BgL_auxz00_5520); } 
return 
BINT(BgL_tmpz00_5519);} } 

}



/* string-ref */
BGL_EXPORTED_DEF unsigned char BGl_stringzd2refzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_11, long BgL_kz00_12)
{
{ /* Ieee/string.scm 330 */
{ /* Ieee/string.scm 331 */
 long BgL_l2221z00_5357;
BgL_l2221z00_5357 = 
STRING_LENGTH(BgL_stringz00_11); 
if(
BOUND_CHECK(BgL_kz00_12, BgL_l2221z00_5357))
{ /* Ieee/string.scm 331 */
return 
STRING_REF(BgL_stringz00_11, BgL_kz00_12);}  else 
{ 
 obj_t BgL_auxz00_5533;
BgL_auxz00_5533 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_11, 
(int)(BgL_l2221z00_5357), 
(int)(BgL_kz00_12)); 
FAILURE(BgL_auxz00_5533,BFALSE,BFALSE);} } } 

}



/* &string-ref */
obj_t BGl_z62stringzd2refzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4044, obj_t BgL_stringz00_4045, obj_t BgL_kz00_4046)
{
{ /* Ieee/string.scm 330 */
{ /* Ieee/string.scm 331 */
 unsigned char BgL_tmpz00_5539;
{ /* Ieee/string.scm 331 */
 long BgL_auxz00_5547; obj_t BgL_auxz00_5540;
{ /* Ieee/string.scm 331 */
 obj_t BgL_tmpz00_5548;
if(
INTEGERP(BgL_kz00_4046))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_5548 = BgL_kz00_4046
; }  else 
{ 
 obj_t BgL_auxz00_5551;
BgL_auxz00_5551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3367z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_kz00_4046); 
FAILURE(BgL_auxz00_5551,BFALSE,BFALSE);} 
BgL_auxz00_5547 = 
(long)CINT(BgL_tmpz00_5548); } 
if(
STRINGP(BgL_stringz00_4045))
{ /* Ieee/string.scm 331 */
BgL_auxz00_5540 = BgL_stringz00_4045
; }  else 
{ 
 obj_t BgL_auxz00_5543;
BgL_auxz00_5543 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3367z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4045); 
FAILURE(BgL_auxz00_5543,BFALSE,BFALSE);} 
BgL_tmpz00_5539 = 
BGl_stringzd2refzd2zz__r4_strings_6_7z00(BgL_auxz00_5540, BgL_auxz00_5547); } 
return 
BCHAR(BgL_tmpz00_5539);} } 

}



/* string-set! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2setz12zc0zz__r4_strings_6_7z00(obj_t BgL_stringz00_13, long BgL_kz00_14, unsigned char BgL_charz00_15)
{
{ /* Ieee/string.scm 336 */
{ /* Ieee/string.scm 337 */
 long BgL_l2225z00_5358;
BgL_l2225z00_5358 = 
STRING_LENGTH(BgL_stringz00_13); 
if(
BOUND_CHECK(BgL_kz00_14, BgL_l2225z00_5358))
{ /* Ieee/string.scm 337 */
return 
STRING_SET(BgL_stringz00_13, BgL_kz00_14, BgL_charz00_15);}  else 
{ 
 obj_t BgL_auxz00_5562;
BgL_auxz00_5562 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_13, 
(int)(BgL_l2225z00_5358), 
(int)(BgL_kz00_14)); 
FAILURE(BgL_auxz00_5562,BFALSE,BFALSE);} } } 

}



/* &string-set! */
obj_t BGl_z62stringzd2setz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4047, obj_t BgL_stringz00_4048, obj_t BgL_kz00_4049, obj_t BgL_charz00_4050)
{
{ /* Ieee/string.scm 336 */
{ /* Ieee/string.scm 337 */
 unsigned char BgL_auxz00_5584; long BgL_auxz00_5575; obj_t BgL_auxz00_5568;
{ /* Ieee/string.scm 337 */
 obj_t BgL_tmpz00_5585;
if(
CHARP(BgL_charz00_4050))
{ /* Ieee/string.scm 337 */
BgL_tmpz00_5585 = BgL_charz00_4050
; }  else 
{ 
 obj_t BgL_auxz00_5588;
BgL_auxz00_5588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3369z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_charz00_4050); 
FAILURE(BgL_auxz00_5588,BFALSE,BFALSE);} 
BgL_auxz00_5584 = 
CCHAR(BgL_tmpz00_5585); } 
{ /* Ieee/string.scm 337 */
 obj_t BgL_tmpz00_5576;
if(
INTEGERP(BgL_kz00_4049))
{ /* Ieee/string.scm 337 */
BgL_tmpz00_5576 = BgL_kz00_4049
; }  else 
{ 
 obj_t BgL_auxz00_5579;
BgL_auxz00_5579 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3369z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_kz00_4049); 
FAILURE(BgL_auxz00_5579,BFALSE,BFALSE);} 
BgL_auxz00_5575 = 
(long)CINT(BgL_tmpz00_5576); } 
if(
STRINGP(BgL_stringz00_4048))
{ /* Ieee/string.scm 337 */
BgL_auxz00_5568 = BgL_stringz00_4048
; }  else 
{ 
 obj_t BgL_auxz00_5571;
BgL_auxz00_5571 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3369z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4048); 
FAILURE(BgL_auxz00_5571,BFALSE,BFALSE);} 
return 
BGl_stringzd2setz12zc0zz__r4_strings_6_7z00(BgL_auxz00_5568, BgL_auxz00_5575, BgL_auxz00_5584);} } 

}



/* string-ref-ur */
BGL_EXPORTED_DEF unsigned char BGl_stringzd2refzd2urz00zz__r4_strings_6_7z00(obj_t BgL_stringz00_16, long BgL_kz00_17)
{
{ /* Ieee/string.scm 342 */
{ /* Ieee/string.scm 343 */
 long BgL_l2229z00_5359;
BgL_l2229z00_5359 = 
STRING_LENGTH(BgL_stringz00_16); 
if(
BOUND_CHECK(BgL_kz00_17, BgL_l2229z00_5359))
{ /* Ieee/string.scm 343 */
return 
STRING_REF(BgL_stringz00_16, BgL_kz00_17);}  else 
{ 
 obj_t BgL_auxz00_5598;
BgL_auxz00_5598 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_16, 
(int)(BgL_l2229z00_5359), 
(int)(BgL_kz00_17)); 
FAILURE(BgL_auxz00_5598,BFALSE,BFALSE);} } } 

}



/* &string-ref-ur */
obj_t BGl_z62stringzd2refzd2urz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4051, obj_t BgL_stringz00_4052, obj_t BgL_kz00_4053)
{
{ /* Ieee/string.scm 342 */
{ /* Ieee/string.scm 343 */
 unsigned char BgL_tmpz00_5604;
{ /* Ieee/string.scm 343 */
 long BgL_auxz00_5612; obj_t BgL_auxz00_5605;
{ /* Ieee/string.scm 343 */
 obj_t BgL_tmpz00_5613;
if(
INTEGERP(BgL_kz00_4053))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_5613 = BgL_kz00_4053
; }  else 
{ 
 obj_t BgL_auxz00_5616;
BgL_auxz00_5616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3370z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_kz00_4053); 
FAILURE(BgL_auxz00_5616,BFALSE,BFALSE);} 
BgL_auxz00_5612 = 
(long)CINT(BgL_tmpz00_5613); } 
if(
STRINGP(BgL_stringz00_4052))
{ /* Ieee/string.scm 343 */
BgL_auxz00_5605 = BgL_stringz00_4052
; }  else 
{ 
 obj_t BgL_auxz00_5608;
BgL_auxz00_5608 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3370z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4052); 
FAILURE(BgL_auxz00_5608,BFALSE,BFALSE);} 
BgL_tmpz00_5604 = 
BGl_stringzd2refzd2urz00zz__r4_strings_6_7z00(BgL_auxz00_5605, BgL_auxz00_5612); } 
return 
BCHAR(BgL_tmpz00_5604);} } 

}



/* string-set-ur! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2setzd2urz12z12zz__r4_strings_6_7z00(obj_t BgL_stringz00_18, long BgL_kz00_19, unsigned char BgL_charz00_20)
{
{ /* Ieee/string.scm 348 */
{ /* Ieee/string.scm 349 */
 long BgL_l2233z00_5360;
BgL_l2233z00_5360 = 
STRING_LENGTH(BgL_stringz00_18); 
if(
BOUND_CHECK(BgL_kz00_19, BgL_l2233z00_5360))
{ /* Ieee/string.scm 349 */
return 
STRING_SET(BgL_stringz00_18, BgL_kz00_19, BgL_charz00_20);}  else 
{ 
 obj_t BgL_auxz00_5627;
BgL_auxz00_5627 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_18, 
(int)(BgL_l2233z00_5360), 
(int)(BgL_kz00_19)); 
FAILURE(BgL_auxz00_5627,BFALSE,BFALSE);} } } 

}



/* &string-set-ur! */
obj_t BGl_z62stringzd2setzd2urz12z70zz__r4_strings_6_7z00(obj_t BgL_envz00_4054, obj_t BgL_stringz00_4055, obj_t BgL_kz00_4056, obj_t BgL_charz00_4057)
{
{ /* Ieee/string.scm 348 */
{ /* Ieee/string.scm 349 */
 unsigned char BgL_auxz00_5649; long BgL_auxz00_5640; obj_t BgL_auxz00_5633;
{ /* Ieee/string.scm 349 */
 obj_t BgL_tmpz00_5650;
if(
CHARP(BgL_charz00_4057))
{ /* Ieee/string.scm 349 */
BgL_tmpz00_5650 = BgL_charz00_4057
; }  else 
{ 
 obj_t BgL_auxz00_5653;
BgL_auxz00_5653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3371z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_charz00_4057); 
FAILURE(BgL_auxz00_5653,BFALSE,BFALSE);} 
BgL_auxz00_5649 = 
CCHAR(BgL_tmpz00_5650); } 
{ /* Ieee/string.scm 349 */
 obj_t BgL_tmpz00_5641;
if(
INTEGERP(BgL_kz00_4056))
{ /* Ieee/string.scm 349 */
BgL_tmpz00_5641 = BgL_kz00_4056
; }  else 
{ 
 obj_t BgL_auxz00_5644;
BgL_auxz00_5644 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3371z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_kz00_4056); 
FAILURE(BgL_auxz00_5644,BFALSE,BFALSE);} 
BgL_auxz00_5640 = 
(long)CINT(BgL_tmpz00_5641); } 
if(
STRINGP(BgL_stringz00_4055))
{ /* Ieee/string.scm 349 */
BgL_auxz00_5633 = BgL_stringz00_4055
; }  else 
{ 
 obj_t BgL_auxz00_5636;
BgL_auxz00_5636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3371z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4055); 
FAILURE(BgL_auxz00_5636,BFALSE,BFALSE);} 
return 
BGl_stringzd2setzd2urz12z12zz__r4_strings_6_7z00(BgL_auxz00_5633, BgL_auxz00_5640, BgL_auxz00_5649);} } 

}



/* string=? */
BGL_EXPORTED_DEF bool_t BGl_stringzd3zf3z20zz__r4_strings_6_7z00(obj_t BgL_string1z00_21, obj_t BgL_string2z00_22)
{
{ /* Ieee/string.scm 354 */
{ /* Ieee/string.scm 357 */
 long BgL_l1z00_5361;
BgL_l1z00_5361 = 
STRING_LENGTH(BgL_string1z00_21); 
if(
(BgL_l1z00_5361==
STRING_LENGTH(BgL_string2z00_22)))
{ /* Ieee/string.scm 359 */
 int BgL_arg1223z00_5362;
{ /* Ieee/string.scm 359 */
 char * BgL_auxz00_5665; char * BgL_tmpz00_5663;
BgL_auxz00_5665 = 
BSTRING_TO_STRING(BgL_string2z00_22); 
BgL_tmpz00_5663 = 
BSTRING_TO_STRING(BgL_string1z00_21); 
BgL_arg1223z00_5362 = 
memcmp(BgL_tmpz00_5663, BgL_auxz00_5665, BgL_l1z00_5361); } 
return 
(
(long)(BgL_arg1223z00_5362)==0L);}  else 
{ /* Ieee/string.scm 358 */
return ((bool_t)0);} } } 

}



/* &string=? */
obj_t BGl_z62stringzd3zf3z42zz__r4_strings_6_7z00(obj_t BgL_envz00_4058, obj_t BgL_string1z00_4059, obj_t BgL_string2z00_4060)
{
{ /* Ieee/string.scm 354 */
{ /* Ieee/string.scm 357 */
 bool_t BgL_tmpz00_5670;
{ /* Ieee/string.scm 357 */
 obj_t BgL_auxz00_5678; obj_t BgL_auxz00_5671;
if(
STRINGP(BgL_string2z00_4060))
{ /* Ieee/string.scm 357 */
BgL_auxz00_5678 = BgL_string2z00_4060
; }  else 
{ 
 obj_t BgL_auxz00_5681;
BgL_auxz00_5681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17446L), BGl_string3372z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4060); 
FAILURE(BgL_auxz00_5681,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4059))
{ /* Ieee/string.scm 357 */
BgL_auxz00_5671 = BgL_string1z00_4059
; }  else 
{ 
 obj_t BgL_auxz00_5674;
BgL_auxz00_5674 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17446L), BGl_string3372z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4059); 
FAILURE(BgL_auxz00_5674,BFALSE,BFALSE);} 
BgL_tmpz00_5670 = 
BGl_stringzd3zf3z20zz__r4_strings_6_7z00(BgL_auxz00_5671, BgL_auxz00_5678); } 
return 
BBOOL(BgL_tmpz00_5670);} } 

}



/* substring=? */
BGL_EXPORTED_DEF bool_t BGl_substringzd3zf3z20zz__r4_strings_6_7z00(obj_t BgL_string1z00_23, obj_t BgL_string2z00_24, long BgL_lenz00_25)
{
{ /* Ieee/string.scm 368 */
BGL_TAIL return 
bigloo_strncmp(BgL_string1z00_23, BgL_string2z00_24, BgL_lenz00_25);} 

}



/* &substring=? */
obj_t BGl_z62substringzd3zf3z42zz__r4_strings_6_7z00(obj_t BgL_envz00_4061, obj_t BgL_string1z00_4062, obj_t BgL_string2z00_4063, obj_t BgL_lenz00_4064)
{
{ /* Ieee/string.scm 368 */
{ /* Ieee/string.scm 369 */
 bool_t BgL_tmpz00_5688;
{ /* Ieee/string.scm 369 */
 long BgL_auxz00_5703; obj_t BgL_auxz00_5696; obj_t BgL_auxz00_5689;
{ /* Ieee/string.scm 369 */
 obj_t BgL_tmpz00_5704;
if(
INTEGERP(BgL_lenz00_4064))
{ /* Ieee/string.scm 369 */
BgL_tmpz00_5704 = BgL_lenz00_4064
; }  else 
{ 
 obj_t BgL_auxz00_5707;
BgL_auxz00_5707 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17949L), BGl_string3373z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_4064); 
FAILURE(BgL_auxz00_5707,BFALSE,BFALSE);} 
BgL_auxz00_5703 = 
(long)CINT(BgL_tmpz00_5704); } 
if(
STRINGP(BgL_string2z00_4063))
{ /* Ieee/string.scm 369 */
BgL_auxz00_5696 = BgL_string2z00_4063
; }  else 
{ 
 obj_t BgL_auxz00_5699;
BgL_auxz00_5699 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17949L), BGl_string3373z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4063); 
FAILURE(BgL_auxz00_5699,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4062))
{ /* Ieee/string.scm 369 */
BgL_auxz00_5689 = BgL_string1z00_4062
; }  else 
{ 
 obj_t BgL_auxz00_5692;
BgL_auxz00_5692 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17949L), BGl_string3373z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4062); 
FAILURE(BgL_auxz00_5692,BFALSE,BFALSE);} 
BgL_tmpz00_5688 = 
BGl_substringzd3zf3z20zz__r4_strings_6_7z00(BgL_auxz00_5689, BgL_auxz00_5696, BgL_auxz00_5703); } 
return 
BBOOL(BgL_tmpz00_5688);} } 

}



/* _substring-at? */
obj_t BGl__substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t BgL_env1098z00_31, obj_t BgL_opt1097z00_30)
{
{ /* Ieee/string.scm 374 */
{ /* Ieee/string.scm 374 */
 obj_t BgL_g1099z00_5363; obj_t BgL_g1100z00_5364; obj_t BgL_g1101z00_5365;
BgL_g1099z00_5363 = 
VECTOR_REF(BgL_opt1097z00_30,0L); 
BgL_g1100z00_5364 = 
VECTOR_REF(BgL_opt1097z00_30,1L); 
BgL_g1101z00_5365 = 
VECTOR_REF(BgL_opt1097z00_30,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1097z00_30)) { case 3L : 

{ /* Ieee/string.scm 374 */

{ /* Ieee/string.scm 374 */
 obj_t BgL_string1z00_5367; obj_t BgL_string2z00_5368; long BgL_offz00_5369;
if(
STRINGP(BgL_g1099z00_5363))
{ /* Ieee/string.scm 374 */
BgL_string1z00_5367 = BgL_g1099z00_5363; }  else 
{ 
 obj_t BgL_auxz00_5719;
BgL_auxz00_5719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18208L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1099z00_5363); 
FAILURE(BgL_auxz00_5719,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1100z00_5364))
{ /* Ieee/string.scm 374 */
BgL_string2z00_5368 = BgL_g1100z00_5364; }  else 
{ 
 obj_t BgL_auxz00_5725;
BgL_auxz00_5725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18208L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1100z00_5364); 
FAILURE(BgL_auxz00_5725,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 374 */
 obj_t BgL_tmpz00_5729;
if(
INTEGERP(BgL_g1101z00_5365))
{ /* Ieee/string.scm 374 */
BgL_tmpz00_5729 = BgL_g1101z00_5365
; }  else 
{ 
 obj_t BgL_auxz00_5732;
BgL_auxz00_5732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18208L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1101z00_5365); 
FAILURE(BgL_auxz00_5732,BFALSE,BFALSE);} 
BgL_offz00_5369 = 
(long)CINT(BgL_tmpz00_5729); } 
return 
BBOOL(
bigloo_strcmp_at(BgL_string1z00_5367, BgL_string2z00_5368, BgL_offz00_5369));} } break;case 4L : 

{ /* Ieee/string.scm 374 */
 obj_t BgL_lenz00_5370;
BgL_lenz00_5370 = 
VECTOR_REF(BgL_opt1097z00_30,3L); 
{ /* Ieee/string.scm 374 */

{ /* Ieee/string.scm 374 */
 obj_t BgL_string1z00_5371; obj_t BgL_string2z00_5372; long BgL_offz00_5373;
if(
STRINGP(BgL_g1099z00_5363))
{ /* Ieee/string.scm 374 */
BgL_string1z00_5371 = BgL_g1099z00_5363; }  else 
{ 
 obj_t BgL_auxz00_5742;
BgL_auxz00_5742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18208L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1099z00_5363); 
FAILURE(BgL_auxz00_5742,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1100z00_5364))
{ /* Ieee/string.scm 374 */
BgL_string2z00_5372 = BgL_g1100z00_5364; }  else 
{ 
 obj_t BgL_auxz00_5748;
BgL_auxz00_5748 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18208L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1100z00_5364); 
FAILURE(BgL_auxz00_5748,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 374 */
 obj_t BgL_tmpz00_5752;
if(
INTEGERP(BgL_g1101z00_5365))
{ /* Ieee/string.scm 374 */
BgL_tmpz00_5752 = BgL_g1101z00_5365
; }  else 
{ 
 obj_t BgL_auxz00_5755;
BgL_auxz00_5755 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18208L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1101z00_5365); 
FAILURE(BgL_auxz00_5755,BFALSE,BFALSE);} 
BgL_offz00_5373 = 
(long)CINT(BgL_tmpz00_5752); } 
{ /* Ieee/string.scm 375 */
 bool_t BgL_test3820z00_5760;
{ /* Ieee/string.scm 375 */
 long BgL_n1z00_5374;
{ /* Ieee/string.scm 375 */
 obj_t BgL_tmpz00_5761;
if(
INTEGERP(BgL_lenz00_5370))
{ /* Ieee/string.scm 375 */
BgL_tmpz00_5761 = BgL_lenz00_5370
; }  else 
{ 
 obj_t BgL_auxz00_5764;
BgL_auxz00_5764 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18291L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_5370); 
FAILURE(BgL_auxz00_5764,BFALSE,BFALSE);} 
BgL_n1z00_5374 = 
(long)CINT(BgL_tmpz00_5761); } 
BgL_test3820z00_5760 = 
(BgL_n1z00_5374==-1L); } 
if(BgL_test3820z00_5760)
{ /* Ieee/string.scm 375 */
return 
BBOOL(
bigloo_strcmp_at(BgL_string1z00_5371, BgL_string2z00_5372, BgL_offz00_5373));}  else 
{ /* Ieee/string.scm 377 */
 bool_t BgL_tmpz00_5772;
{ /* Ieee/string.scm 377 */
 long BgL_tmpz00_5773;
{ /* Ieee/string.scm 377 */
 obj_t BgL_tmpz00_5774;
if(
INTEGERP(BgL_lenz00_5370))
{ /* Ieee/string.scm 377 */
BgL_tmpz00_5774 = BgL_lenz00_5370
; }  else 
{ 
 obj_t BgL_auxz00_5777;
BgL_auxz00_5777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18383L), BGl_string3377z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_5370); 
FAILURE(BgL_auxz00_5777,BFALSE,BFALSE);} 
BgL_tmpz00_5773 = 
(long)CINT(BgL_tmpz00_5774); } 
BgL_tmpz00_5772 = 
bigloo_strncmp_at(BgL_string1z00_5371, BgL_string2z00_5372, BgL_offz00_5373, BgL_tmpz00_5773); } 
return 
BBOOL(BgL_tmpz00_5772);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3374z00zz__r4_strings_6_7z00, BGl_string3376z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1097z00_30)));} } } } 

}



/* substring-at? */
BGL_EXPORTED_DEF bool_t BGl_substringzd2atzf3z21zz__r4_strings_6_7z00(obj_t BgL_string1z00_26, obj_t BgL_string2z00_27, long BgL_offz00_28, obj_t BgL_lenz00_29)
{
{ /* Ieee/string.scm 374 */
{ /* Ieee/string.scm 375 */
 bool_t BgL_test3823z00_5789;
{ /* Ieee/string.scm 375 */
 long BgL_n1z00_5375;
{ /* Ieee/string.scm 375 */
 obj_t BgL_tmpz00_5790;
if(
INTEGERP(BgL_lenz00_29))
{ /* Ieee/string.scm 375 */
BgL_tmpz00_5790 = BgL_lenz00_29
; }  else 
{ 
 obj_t BgL_auxz00_5793;
BgL_auxz00_5793 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18291L), BGl_string3375z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_29); 
FAILURE(BgL_auxz00_5793,BFALSE,BFALSE);} 
BgL_n1z00_5375 = 
(long)CINT(BgL_tmpz00_5790); } 
BgL_test3823z00_5789 = 
(BgL_n1z00_5375==-1L); } 
if(BgL_test3823z00_5789)
{ /* Ieee/string.scm 375 */
return 
bigloo_strcmp_at(BgL_string1z00_26, BgL_string2z00_27, BgL_offz00_28);}  else 
{ /* Ieee/string.scm 377 */
 long BgL_tmpz00_5800;
{ /* Ieee/string.scm 377 */
 obj_t BgL_tmpz00_5801;
if(
INTEGERP(BgL_lenz00_29))
{ /* Ieee/string.scm 377 */
BgL_tmpz00_5801 = BgL_lenz00_29
; }  else 
{ 
 obj_t BgL_auxz00_5804;
BgL_auxz00_5804 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18383L), BGl_string3375z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_29); 
FAILURE(BgL_auxz00_5804,BFALSE,BFALSE);} 
BgL_tmpz00_5800 = 
(long)CINT(BgL_tmpz00_5801); } 
return 
bigloo_strncmp_at(BgL_string1z00_26, BgL_string2z00_27, BgL_offz00_28, BgL_tmpz00_5800);} } } 

}



/* substring-ci=? */
BGL_EXPORTED_DEF bool_t BGl_substringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t BgL_string1z00_32, obj_t BgL_string2z00_33, long BgL_lenz00_34)
{
{ /* Ieee/string.scm 382 */
BGL_TAIL return 
bigloo_strncmp_ci(BgL_string1z00_32, BgL_string2z00_33, BgL_lenz00_34);} 

}



/* &substring-ci=? */
obj_t BGl_z62substringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t BgL_envz00_4065, obj_t BgL_string1z00_4066, obj_t BgL_string2z00_4067, obj_t BgL_lenz00_4068)
{
{ /* Ieee/string.scm 382 */
{ /* Ieee/string.scm 383 */
 bool_t BgL_tmpz00_5811;
{ /* Ieee/string.scm 383 */
 long BgL_auxz00_5826; obj_t BgL_auxz00_5819; obj_t BgL_auxz00_5812;
{ /* Ieee/string.scm 383 */
 obj_t BgL_tmpz00_5827;
if(
INTEGERP(BgL_lenz00_4068))
{ /* Ieee/string.scm 383 */
BgL_tmpz00_5827 = BgL_lenz00_4068
; }  else 
{ 
 obj_t BgL_auxz00_5830;
BgL_auxz00_5830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18668L), BGl_string3378z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_4068); 
FAILURE(BgL_auxz00_5830,BFALSE,BFALSE);} 
BgL_auxz00_5826 = 
(long)CINT(BgL_tmpz00_5827); } 
if(
STRINGP(BgL_string2z00_4067))
{ /* Ieee/string.scm 383 */
BgL_auxz00_5819 = BgL_string2z00_4067
; }  else 
{ 
 obj_t BgL_auxz00_5822;
BgL_auxz00_5822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18668L), BGl_string3378z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4067); 
FAILURE(BgL_auxz00_5822,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4066))
{ /* Ieee/string.scm 383 */
BgL_auxz00_5812 = BgL_string1z00_4066
; }  else 
{ 
 obj_t BgL_auxz00_5815;
BgL_auxz00_5815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18668L), BGl_string3378z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4066); 
FAILURE(BgL_auxz00_5815,BFALSE,BFALSE);} 
BgL_tmpz00_5811 = 
BGl_substringzd2cizd3zf3zf2zz__r4_strings_6_7z00(BgL_auxz00_5812, BgL_auxz00_5819, BgL_auxz00_5826); } 
return 
BBOOL(BgL_tmpz00_5811);} } 

}



/* _substring-ci-at? */
obj_t BGl__substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t BgL_env1105z00_40, obj_t BgL_opt1104z00_39)
{
{ /* Ieee/string.scm 388 */
{ /* Ieee/string.scm 388 */
 obj_t BgL_g1106z00_5376; obj_t BgL_g1107z00_5377; obj_t BgL_g1108z00_5378;
BgL_g1106z00_5376 = 
VECTOR_REF(BgL_opt1104z00_39,0L); 
BgL_g1107z00_5377 = 
VECTOR_REF(BgL_opt1104z00_39,1L); 
BgL_g1108z00_5378 = 
VECTOR_REF(BgL_opt1104z00_39,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1104z00_39)) { case 3L : 

{ /* Ieee/string.scm 388 */

{ /* Ieee/string.scm 388 */
 obj_t BgL_string1z00_5380; obj_t BgL_string2z00_5381; long BgL_offz00_5382;
if(
STRINGP(BgL_g1106z00_5376))
{ /* Ieee/string.scm 388 */
BgL_string1z00_5380 = BgL_g1106z00_5376; }  else 
{ 
 obj_t BgL_auxz00_5842;
BgL_auxz00_5842 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18930L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1106z00_5376); 
FAILURE(BgL_auxz00_5842,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1107z00_5377))
{ /* Ieee/string.scm 388 */
BgL_string2z00_5381 = BgL_g1107z00_5377; }  else 
{ 
 obj_t BgL_auxz00_5848;
BgL_auxz00_5848 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18930L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1107z00_5377); 
FAILURE(BgL_auxz00_5848,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 388 */
 obj_t BgL_tmpz00_5852;
if(
INTEGERP(BgL_g1108z00_5378))
{ /* Ieee/string.scm 388 */
BgL_tmpz00_5852 = BgL_g1108z00_5378
; }  else 
{ 
 obj_t BgL_auxz00_5855;
BgL_auxz00_5855 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18930L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1108z00_5378); 
FAILURE(BgL_auxz00_5855,BFALSE,BFALSE);} 
BgL_offz00_5382 = 
(long)CINT(BgL_tmpz00_5852); } 
return 
BBOOL(
bigloo_strcmp_ci_at(BgL_string1z00_5380, BgL_string2z00_5381, BgL_offz00_5382));} } break;case 4L : 

{ /* Ieee/string.scm 388 */
 obj_t BgL_lenz00_5383;
BgL_lenz00_5383 = 
VECTOR_REF(BgL_opt1104z00_39,3L); 
{ /* Ieee/string.scm 388 */

{ /* Ieee/string.scm 388 */
 obj_t BgL_string1z00_5384; obj_t BgL_string2z00_5385; long BgL_offz00_5386;
if(
STRINGP(BgL_g1106z00_5376))
{ /* Ieee/string.scm 388 */
BgL_string1z00_5384 = BgL_g1106z00_5376; }  else 
{ 
 obj_t BgL_auxz00_5865;
BgL_auxz00_5865 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18930L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1106z00_5376); 
FAILURE(BgL_auxz00_5865,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1107z00_5377))
{ /* Ieee/string.scm 388 */
BgL_string2z00_5385 = BgL_g1107z00_5377; }  else 
{ 
 obj_t BgL_auxz00_5871;
BgL_auxz00_5871 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18930L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1107z00_5377); 
FAILURE(BgL_auxz00_5871,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 388 */
 obj_t BgL_tmpz00_5875;
if(
INTEGERP(BgL_g1108z00_5378))
{ /* Ieee/string.scm 388 */
BgL_tmpz00_5875 = BgL_g1108z00_5378
; }  else 
{ 
 obj_t BgL_auxz00_5878;
BgL_auxz00_5878 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(18930L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1108z00_5378); 
FAILURE(BgL_auxz00_5878,BFALSE,BFALSE);} 
BgL_offz00_5386 = 
(long)CINT(BgL_tmpz00_5875); } 
{ /* Ieee/string.scm 389 */
 bool_t BgL_test3835z00_5883;
{ /* Ieee/string.scm 389 */
 long BgL_n1z00_5387;
{ /* Ieee/string.scm 389 */
 obj_t BgL_tmpz00_5884;
if(
INTEGERP(BgL_lenz00_5383))
{ /* Ieee/string.scm 389 */
BgL_tmpz00_5884 = BgL_lenz00_5383
; }  else 
{ 
 obj_t BgL_auxz00_5887;
BgL_auxz00_5887 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19016L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_5383); 
FAILURE(BgL_auxz00_5887,BFALSE,BFALSE);} 
BgL_n1z00_5387 = 
(long)CINT(BgL_tmpz00_5884); } 
BgL_test3835z00_5883 = 
(BgL_n1z00_5387==-1L); } 
if(BgL_test3835z00_5883)
{ /* Ieee/string.scm 389 */
return 
BBOOL(
bigloo_strcmp_ci_at(BgL_string1z00_5384, BgL_string2z00_5385, BgL_offz00_5386));}  else 
{ /* Ieee/string.scm 391 */
 bool_t BgL_tmpz00_5895;
{ /* Ieee/string.scm 391 */
 long BgL_tmpz00_5896;
{ /* Ieee/string.scm 391 */
 obj_t BgL_tmpz00_5897;
if(
INTEGERP(BgL_lenz00_5383))
{ /* Ieee/string.scm 391 */
BgL_tmpz00_5897 = BgL_lenz00_5383
; }  else 
{ 
 obj_t BgL_auxz00_5900;
BgL_auxz00_5900 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19108L), BGl_string3381z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_5383); 
FAILURE(BgL_auxz00_5900,BFALSE,BFALSE);} 
BgL_tmpz00_5896 = 
(long)CINT(BgL_tmpz00_5897); } 
BgL_tmpz00_5895 = 
bigloo_strncmp_ci_at(BgL_string1z00_5384, BgL_string2z00_5385, BgL_offz00_5386, BgL_tmpz00_5896); } 
return 
BBOOL(BgL_tmpz00_5895);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3379z00zz__r4_strings_6_7z00, BGl_string3376z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1104z00_39)));} } } } 

}



/* substring-ci-at? */
BGL_EXPORTED_DEF bool_t BGl_substringzd2cizd2atzf3zf3zz__r4_strings_6_7z00(obj_t BgL_string1z00_35, obj_t BgL_string2z00_36, long BgL_offz00_37, obj_t BgL_lenz00_38)
{
{ /* Ieee/string.scm 388 */
{ /* Ieee/string.scm 389 */
 bool_t BgL_test3838z00_5912;
{ /* Ieee/string.scm 389 */
 long BgL_n1z00_5388;
{ /* Ieee/string.scm 389 */
 obj_t BgL_tmpz00_5913;
if(
INTEGERP(BgL_lenz00_38))
{ /* Ieee/string.scm 389 */
BgL_tmpz00_5913 = BgL_lenz00_38
; }  else 
{ 
 obj_t BgL_auxz00_5916;
BgL_auxz00_5916 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19016L), BGl_string3380z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_38); 
FAILURE(BgL_auxz00_5916,BFALSE,BFALSE);} 
BgL_n1z00_5388 = 
(long)CINT(BgL_tmpz00_5913); } 
BgL_test3838z00_5912 = 
(BgL_n1z00_5388==-1L); } 
if(BgL_test3838z00_5912)
{ /* Ieee/string.scm 389 */
return 
bigloo_strcmp_ci_at(BgL_string1z00_35, BgL_string2z00_36, BgL_offz00_37);}  else 
{ /* Ieee/string.scm 391 */
 long BgL_tmpz00_5923;
{ /* Ieee/string.scm 391 */
 obj_t BgL_tmpz00_5924;
if(
INTEGERP(BgL_lenz00_38))
{ /* Ieee/string.scm 391 */
BgL_tmpz00_5924 = BgL_lenz00_38
; }  else 
{ 
 obj_t BgL_auxz00_5927;
BgL_auxz00_5927 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19108L), BGl_string3380z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lenz00_38); 
FAILURE(BgL_auxz00_5927,BFALSE,BFALSE);} 
BgL_tmpz00_5923 = 
(long)CINT(BgL_tmpz00_5924); } 
return 
bigloo_strncmp_ci_at(BgL_string1z00_35, BgL_string2z00_36, BgL_offz00_37, BgL_tmpz00_5923);} } } 

}



/* empty-string? */
BGL_EXPORTED_DEF bool_t BGl_emptyzd2stringzf3z21zz__r4_strings_6_7z00(obj_t BgL_stringz00_41)
{
{ /* Ieee/string.scm 396 */
return 
(
STRING_LENGTH(BgL_stringz00_41)==0L);} 

}



/* &empty-string? */
obj_t BGl_z62emptyzd2stringzf3z43zz__r4_strings_6_7z00(obj_t BgL_envz00_4069, obj_t BgL_stringz00_4070)
{
{ /* Ieee/string.scm 396 */
{ /* Ieee/string.scm 397 */
 bool_t BgL_tmpz00_5935;
{ /* Ieee/string.scm 397 */
 obj_t BgL_auxz00_5936;
if(
STRINGP(BgL_stringz00_4070))
{ /* Ieee/string.scm 397 */
BgL_auxz00_5936 = BgL_stringz00_4070
; }  else 
{ 
 obj_t BgL_auxz00_5939;
BgL_auxz00_5939 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19384L), BGl_string3382z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4070); 
FAILURE(BgL_auxz00_5939,BFALSE,BFALSE);} 
BgL_tmpz00_5935 = 
BGl_emptyzd2stringzf3z21zz__r4_strings_6_7z00(BgL_auxz00_5936); } 
return 
BBOOL(BgL_tmpz00_5935);} } 

}



/* string-ci=? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2cizd3zf3zf2zz__r4_strings_6_7z00(obj_t BgL_string1z00_42, obj_t BgL_string2z00_43)
{
{ /* Ieee/string.scm 402 */
BGL_TAIL return 
bigloo_strcicmp(BgL_string1z00_42, BgL_string2z00_43);} 

}



/* &string-ci=? */
obj_t BGl_z62stringzd2cizd3zf3z90zz__r4_strings_6_7z00(obj_t BgL_envz00_4071, obj_t BgL_string1z00_4072, obj_t BgL_string2z00_4073)
{
{ /* Ieee/string.scm 402 */
{ /* Ieee/string.scm 403 */
 bool_t BgL_tmpz00_5946;
{ /* Ieee/string.scm 403 */
 obj_t BgL_auxz00_5954; obj_t BgL_auxz00_5947;
if(
STRINGP(BgL_string2z00_4073))
{ /* Ieee/string.scm 403 */
BgL_auxz00_5954 = BgL_string2z00_4073
; }  else 
{ 
 obj_t BgL_auxz00_5957;
BgL_auxz00_5957 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19682L), BGl_string3383z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4073); 
FAILURE(BgL_auxz00_5957,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4072))
{ /* Ieee/string.scm 403 */
BgL_auxz00_5947 = BgL_string1z00_4072
; }  else 
{ 
 obj_t BgL_auxz00_5950;
BgL_auxz00_5950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19682L), BGl_string3383z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4072); 
FAILURE(BgL_auxz00_5950,BFALSE,BFALSE);} 
BgL_tmpz00_5946 = 
BGl_stringzd2cizd3zf3zf2zz__r4_strings_6_7z00(BgL_auxz00_5947, BgL_auxz00_5954); } 
return 
BBOOL(BgL_tmpz00_5946);} } 

}



/* string<? */
BGL_EXPORTED_DEF bool_t BGl_stringzc3zf3z30zz__r4_strings_6_7z00(obj_t BgL_string1z00_44, obj_t BgL_string2z00_45)
{
{ /* Ieee/string.scm 408 */
BGL_TAIL return 
bigloo_string_lt(BgL_string1z00_44, BgL_string2z00_45);} 

}



/* &string<? */
obj_t BGl_z62stringzc3zf3z52zz__r4_strings_6_7z00(obj_t BgL_envz00_4074, obj_t BgL_string1z00_4075, obj_t BgL_string2z00_4076)
{
{ /* Ieee/string.scm 408 */
{ /* Ieee/string.scm 409 */
 bool_t BgL_tmpz00_5964;
{ /* Ieee/string.scm 409 */
 obj_t BgL_auxz00_5972; obj_t BgL_auxz00_5965;
if(
STRINGP(BgL_string2z00_4076))
{ /* Ieee/string.scm 409 */
BgL_auxz00_5972 = BgL_string2z00_4076
; }  else 
{ 
 obj_t BgL_auxz00_5975;
BgL_auxz00_5975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19979L), BGl_string3384z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4076); 
FAILURE(BgL_auxz00_5975,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4075))
{ /* Ieee/string.scm 409 */
BgL_auxz00_5965 = BgL_string1z00_4075
; }  else 
{ 
 obj_t BgL_auxz00_5968;
BgL_auxz00_5968 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(19979L), BGl_string3384z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4075); 
FAILURE(BgL_auxz00_5968,BFALSE,BFALSE);} 
BgL_tmpz00_5964 = 
BGl_stringzc3zf3z30zz__r4_strings_6_7z00(BgL_auxz00_5965, BgL_auxz00_5972); } 
return 
BBOOL(BgL_tmpz00_5964);} } 

}



/* string>? */
BGL_EXPORTED_DEF bool_t BGl_stringze3zf3z10zz__r4_strings_6_7z00(obj_t BgL_string1z00_46, obj_t BgL_string2z00_47)
{
{ /* Ieee/string.scm 414 */
BGL_TAIL return 
bigloo_string_gt(BgL_string1z00_46, BgL_string2z00_47);} 

}



/* &string>? */
obj_t BGl_z62stringze3zf3z72zz__r4_strings_6_7z00(obj_t BgL_envz00_4077, obj_t BgL_string1z00_4078, obj_t BgL_string2z00_4079)
{
{ /* Ieee/string.scm 414 */
{ /* Ieee/string.scm 415 */
 bool_t BgL_tmpz00_5982;
{ /* Ieee/string.scm 415 */
 obj_t BgL_auxz00_5990; obj_t BgL_auxz00_5983;
if(
STRINGP(BgL_string2z00_4079))
{ /* Ieee/string.scm 415 */
BgL_auxz00_5990 = BgL_string2z00_4079
; }  else 
{ 
 obj_t BgL_auxz00_5993;
BgL_auxz00_5993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(20277L), BGl_string3385z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4079); 
FAILURE(BgL_auxz00_5993,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4078))
{ /* Ieee/string.scm 415 */
BgL_auxz00_5983 = BgL_string1z00_4078
; }  else 
{ 
 obj_t BgL_auxz00_5986;
BgL_auxz00_5986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(20277L), BGl_string3385z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4078); 
FAILURE(BgL_auxz00_5986,BFALSE,BFALSE);} 
BgL_tmpz00_5982 = 
BGl_stringze3zf3z10zz__r4_strings_6_7z00(BgL_auxz00_5983, BgL_auxz00_5990); } 
return 
BBOOL(BgL_tmpz00_5982);} } 

}



/* string<=? */
BGL_EXPORTED_DEF bool_t BGl_stringzc3zd3zf3ze3zz__r4_strings_6_7z00(obj_t BgL_string1z00_48, obj_t BgL_string2z00_49)
{
{ /* Ieee/string.scm 420 */
BGL_TAIL return 
bigloo_string_le(BgL_string1z00_48, BgL_string2z00_49);} 

}



/* &string<=? */
obj_t BGl_z62stringzc3zd3zf3z81zz__r4_strings_6_7z00(obj_t BgL_envz00_4080, obj_t BgL_string1z00_4081, obj_t BgL_string2z00_4082)
{
{ /* Ieee/string.scm 420 */
{ /* Ieee/string.scm 421 */
 bool_t BgL_tmpz00_6000;
{ /* Ieee/string.scm 421 */
 obj_t BgL_auxz00_6008; obj_t BgL_auxz00_6001;
if(
STRINGP(BgL_string2z00_4082))
{ /* Ieee/string.scm 421 */
BgL_auxz00_6008 = BgL_string2z00_4082
; }  else 
{ 
 obj_t BgL_auxz00_6011;
BgL_auxz00_6011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(20576L), BGl_string3386z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4082); 
FAILURE(BgL_auxz00_6011,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4081))
{ /* Ieee/string.scm 421 */
BgL_auxz00_6001 = BgL_string1z00_4081
; }  else 
{ 
 obj_t BgL_auxz00_6004;
BgL_auxz00_6004 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(20576L), BGl_string3386z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4081); 
FAILURE(BgL_auxz00_6004,BFALSE,BFALSE);} 
BgL_tmpz00_6000 = 
BGl_stringzc3zd3zf3ze3zz__r4_strings_6_7z00(BgL_auxz00_6001, BgL_auxz00_6008); } 
return 
BBOOL(BgL_tmpz00_6000);} } 

}



/* string>=? */
BGL_EXPORTED_DEF bool_t BGl_stringze3zd3zf3zc3zz__r4_strings_6_7z00(obj_t BgL_string1z00_50, obj_t BgL_string2z00_51)
{
{ /* Ieee/string.scm 426 */
BGL_TAIL return 
bigloo_string_ge(BgL_string1z00_50, BgL_string2z00_51);} 

}



/* &string>=? */
obj_t BGl_z62stringze3zd3zf3za1zz__r4_strings_6_7z00(obj_t BgL_envz00_4083, obj_t BgL_string1z00_4084, obj_t BgL_string2z00_4085)
{
{ /* Ieee/string.scm 426 */
{ /* Ieee/string.scm 427 */
 bool_t BgL_tmpz00_6018;
{ /* Ieee/string.scm 427 */
 obj_t BgL_auxz00_6026; obj_t BgL_auxz00_6019;
if(
STRINGP(BgL_string2z00_4085))
{ /* Ieee/string.scm 427 */
BgL_auxz00_6026 = BgL_string2z00_4085
; }  else 
{ 
 obj_t BgL_auxz00_6029;
BgL_auxz00_6029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(20875L), BGl_string3387z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4085); 
FAILURE(BgL_auxz00_6029,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4084))
{ /* Ieee/string.scm 427 */
BgL_auxz00_6019 = BgL_string1z00_4084
; }  else 
{ 
 obj_t BgL_auxz00_6022;
BgL_auxz00_6022 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(20875L), BGl_string3387z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4084); 
FAILURE(BgL_auxz00_6022,BFALSE,BFALSE);} 
BgL_tmpz00_6018 = 
BGl_stringze3zd3zf3zc3zz__r4_strings_6_7z00(BgL_auxz00_6019, BgL_auxz00_6026); } 
return 
BBOOL(BgL_tmpz00_6018);} } 

}



/* string-ci<? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2cizc3zf3ze2zz__r4_strings_6_7z00(obj_t BgL_string1z00_52, obj_t BgL_string2z00_53)
{
{ /* Ieee/string.scm 432 */
BGL_TAIL return 
bigloo_string_cilt(BgL_string1z00_52, BgL_string2z00_53);} 

}



/* &string-ci<? */
obj_t BGl_z62stringzd2cizc3zf3z80zz__r4_strings_6_7z00(obj_t BgL_envz00_4086, obj_t BgL_string1z00_4087, obj_t BgL_string2z00_4088)
{
{ /* Ieee/string.scm 432 */
{ /* Ieee/string.scm 433 */
 bool_t BgL_tmpz00_6036;
{ /* Ieee/string.scm 433 */
 obj_t BgL_auxz00_6044; obj_t BgL_auxz00_6037;
if(
STRINGP(BgL_string2z00_4088))
{ /* Ieee/string.scm 433 */
BgL_auxz00_6044 = BgL_string2z00_4088
; }  else 
{ 
 obj_t BgL_auxz00_6047;
BgL_auxz00_6047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(21176L), BGl_string3388z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4088); 
FAILURE(BgL_auxz00_6047,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4087))
{ /* Ieee/string.scm 433 */
BgL_auxz00_6037 = BgL_string1z00_4087
; }  else 
{ 
 obj_t BgL_auxz00_6040;
BgL_auxz00_6040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(21176L), BGl_string3388z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4087); 
FAILURE(BgL_auxz00_6040,BFALSE,BFALSE);} 
BgL_tmpz00_6036 = 
BGl_stringzd2cizc3zf3ze2zz__r4_strings_6_7z00(BgL_auxz00_6037, BgL_auxz00_6044); } 
return 
BBOOL(BgL_tmpz00_6036);} } 

}



/* string-ci>? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2cize3zf3zc2zz__r4_strings_6_7z00(obj_t BgL_string1z00_54, obj_t BgL_string2z00_55)
{
{ /* Ieee/string.scm 438 */
BGL_TAIL return 
bigloo_string_cigt(BgL_string1z00_54, BgL_string2z00_55);} 

}



/* &string-ci>? */
obj_t BGl_z62stringzd2cize3zf3za0zz__r4_strings_6_7z00(obj_t BgL_envz00_4089, obj_t BgL_string1z00_4090, obj_t BgL_string2z00_4091)
{
{ /* Ieee/string.scm 438 */
{ /* Ieee/string.scm 439 */
 bool_t BgL_tmpz00_6054;
{ /* Ieee/string.scm 439 */
 obj_t BgL_auxz00_6062; obj_t BgL_auxz00_6055;
if(
STRINGP(BgL_string2z00_4091))
{ /* Ieee/string.scm 439 */
BgL_auxz00_6062 = BgL_string2z00_4091
; }  else 
{ 
 obj_t BgL_auxz00_6065;
BgL_auxz00_6065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(21479L), BGl_string3389z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4091); 
FAILURE(BgL_auxz00_6065,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4090))
{ /* Ieee/string.scm 439 */
BgL_auxz00_6055 = BgL_string1z00_4090
; }  else 
{ 
 obj_t BgL_auxz00_6058;
BgL_auxz00_6058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(21479L), BGl_string3389z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4090); 
FAILURE(BgL_auxz00_6058,BFALSE,BFALSE);} 
BgL_tmpz00_6054 = 
BGl_stringzd2cize3zf3zc2zz__r4_strings_6_7z00(BgL_auxz00_6055, BgL_auxz00_6062); } 
return 
BBOOL(BgL_tmpz00_6054);} } 

}



/* string-ci<=? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2cizc3zd3zf3z31zz__r4_strings_6_7z00(obj_t BgL_string1z00_56, obj_t BgL_string2z00_57)
{
{ /* Ieee/string.scm 444 */
BGL_TAIL return 
bigloo_string_cile(BgL_string1z00_56, BgL_string2z00_57);} 

}



/* &string-ci<=? */
obj_t BGl_z62stringzd2cizc3zd3zf3z53zz__r4_strings_6_7z00(obj_t BgL_envz00_4092, obj_t BgL_string1z00_4093, obj_t BgL_string2z00_4094)
{
{ /* Ieee/string.scm 444 */
{ /* Ieee/string.scm 445 */
 bool_t BgL_tmpz00_6072;
{ /* Ieee/string.scm 445 */
 obj_t BgL_auxz00_6080; obj_t BgL_auxz00_6073;
if(
STRINGP(BgL_string2z00_4094))
{ /* Ieee/string.scm 445 */
BgL_auxz00_6080 = BgL_string2z00_4094
; }  else 
{ 
 obj_t BgL_auxz00_6083;
BgL_auxz00_6083 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(21783L), BGl_string3390z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4094); 
FAILURE(BgL_auxz00_6083,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4093))
{ /* Ieee/string.scm 445 */
BgL_auxz00_6073 = BgL_string1z00_4093
; }  else 
{ 
 obj_t BgL_auxz00_6076;
BgL_auxz00_6076 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(21783L), BGl_string3390z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4093); 
FAILURE(BgL_auxz00_6076,BFALSE,BFALSE);} 
BgL_tmpz00_6072 = 
BGl_stringzd2cizc3zd3zf3z31zz__r4_strings_6_7z00(BgL_auxz00_6073, BgL_auxz00_6080); } 
return 
BBOOL(BgL_tmpz00_6072);} } 

}



/* string-ci>=? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2cize3zd3zf3z11zz__r4_strings_6_7z00(obj_t BgL_string1z00_58, obj_t BgL_string2z00_59)
{
{ /* Ieee/string.scm 450 */
BGL_TAIL return 
bigloo_string_cige(BgL_string1z00_58, BgL_string2z00_59);} 

}



/* &string-ci>=? */
obj_t BGl_z62stringzd2cize3zd3zf3z73zz__r4_strings_6_7z00(obj_t BgL_envz00_4095, obj_t BgL_string1z00_4096, obj_t BgL_string2z00_4097)
{
{ /* Ieee/string.scm 450 */
{ /* Ieee/string.scm 451 */
 bool_t BgL_tmpz00_6090;
{ /* Ieee/string.scm 451 */
 obj_t BgL_auxz00_6098; obj_t BgL_auxz00_6091;
if(
STRINGP(BgL_string2z00_4097))
{ /* Ieee/string.scm 451 */
BgL_auxz00_6098 = BgL_string2z00_4097
; }  else 
{ 
 obj_t BgL_auxz00_6101;
BgL_auxz00_6101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22087L), BGl_string3391z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string2z00_4097); 
FAILURE(BgL_auxz00_6101,BFALSE,BFALSE);} 
if(
STRINGP(BgL_string1z00_4096))
{ /* Ieee/string.scm 451 */
BgL_auxz00_6091 = BgL_string1z00_4096
; }  else 
{ 
 obj_t BgL_auxz00_6094;
BgL_auxz00_6094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22087L), BGl_string3391z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_string1z00_4096); 
FAILURE(BgL_auxz00_6094,BFALSE,BFALSE);} 
BgL_tmpz00_6090 = 
BGl_stringzd2cize3zd3zf3z11zz__r4_strings_6_7z00(BgL_auxz00_6091, BgL_auxz00_6098); } 
return 
BBOOL(BgL_tmpz00_6090);} } 

}



/* _substring */
obj_t BGl__substringz00zz__r4_strings_6_7z00(obj_t BgL_env1112z00_64, obj_t BgL_opt1111z00_63)
{
{ /* Ieee/string.scm 456 */
{ /* Ieee/string.scm 456 */
 obj_t BgL_stringz00_969; obj_t BgL_g1113z00_970;
BgL_stringz00_969 = 
VECTOR_REF(BgL_opt1111z00_63,0L); 
BgL_g1113z00_970 = 
VECTOR_REF(BgL_opt1111z00_63,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1111z00_63)) { case 2L : 

{ /* Ieee/string.scm 456 */
 long BgL_endz00_973;
{ /* Ieee/string.scm 456 */
 obj_t BgL_stringz00_2640;
if(
STRINGP(BgL_stringz00_969))
{ /* Ieee/string.scm 456 */
BgL_stringz00_2640 = BgL_stringz00_969; }  else 
{ 
 obj_t BgL_auxz00_6111;
BgL_auxz00_6111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22396L), BGl_string3395z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_969); 
FAILURE(BgL_auxz00_6111,BFALSE,BFALSE);} 
BgL_endz00_973 = 
STRING_LENGTH(BgL_stringz00_2640); } 
{ /* Ieee/string.scm 456 */

{ /* Ieee/string.scm 456 */
 long BgL_auxz00_6123; obj_t BgL_auxz00_6116;
{ /* Ieee/string.scm 456 */
 obj_t BgL_tmpz00_6124;
if(
INTEGERP(BgL_g1113z00_970))
{ /* Ieee/string.scm 456 */
BgL_tmpz00_6124 = BgL_g1113z00_970
; }  else 
{ 
 obj_t BgL_auxz00_6127;
BgL_auxz00_6127 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22342L), BGl_string3395z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1113z00_970); 
FAILURE(BgL_auxz00_6127,BFALSE,BFALSE);} 
BgL_auxz00_6123 = 
(long)CINT(BgL_tmpz00_6124); } 
if(
STRINGP(BgL_stringz00_969))
{ /* Ieee/string.scm 456 */
BgL_auxz00_6116 = BgL_stringz00_969
; }  else 
{ 
 obj_t BgL_auxz00_6119;
BgL_auxz00_6119 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22342L), BGl_string3395z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_969); 
FAILURE(BgL_auxz00_6119,BFALSE,BFALSE);} 
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_auxz00_6116, BgL_auxz00_6123, BgL_endz00_973);} } } break;case 3L : 

{ /* Ieee/string.scm 456 */
 obj_t BgL_endz00_974;
BgL_endz00_974 = 
VECTOR_REF(BgL_opt1111z00_63,2L); 
{ /* Ieee/string.scm 456 */

{ /* Ieee/string.scm 456 */
 long BgL_auxz00_6150; long BgL_auxz00_6141; obj_t BgL_auxz00_6134;
{ /* Ieee/string.scm 456 */
 obj_t BgL_tmpz00_6151;
if(
INTEGERP(BgL_endz00_974))
{ /* Ieee/string.scm 456 */
BgL_tmpz00_6151 = BgL_endz00_974
; }  else 
{ 
 obj_t BgL_auxz00_6154;
BgL_auxz00_6154 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22342L), BGl_string3395z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_endz00_974); 
FAILURE(BgL_auxz00_6154,BFALSE,BFALSE);} 
BgL_auxz00_6150 = 
(long)CINT(BgL_tmpz00_6151); } 
{ /* Ieee/string.scm 456 */
 obj_t BgL_tmpz00_6142;
if(
INTEGERP(BgL_g1113z00_970))
{ /* Ieee/string.scm 456 */
BgL_tmpz00_6142 = BgL_g1113z00_970
; }  else 
{ 
 obj_t BgL_auxz00_6145;
BgL_auxz00_6145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22342L), BGl_string3395z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_g1113z00_970); 
FAILURE(BgL_auxz00_6145,BFALSE,BFALSE);} 
BgL_auxz00_6141 = 
(long)CINT(BgL_tmpz00_6142); } 
if(
STRINGP(BgL_stringz00_969))
{ /* Ieee/string.scm 456 */
BgL_auxz00_6134 = BgL_stringz00_969
; }  else 
{ 
 obj_t BgL_auxz00_6137;
BgL_auxz00_6137 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22342L), BGl_string3395z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_969); 
FAILURE(BgL_auxz00_6137,BFALSE,BFALSE);} 
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_auxz00_6134, BgL_auxz00_6141, BgL_auxz00_6150);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3392z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1111z00_63)));} } } } 

}



/* substring */
BGL_EXPORTED_DEF obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t BgL_stringz00_60, long BgL_startz00_61, long BgL_endz00_62)
{
{ /* Ieee/string.scm 456 */
{ /* Ieee/string.scm 457 */
 long BgL_lenz00_976;
BgL_lenz00_976 = 
STRING_LENGTH(BgL_stringz00_60); 
{ /* Ieee/string.scm 459 */
 bool_t BgL_test3866z00_6166;
if(
(BgL_startz00_61<0L))
{ /* Ieee/string.scm 459 */
BgL_test3866z00_6166 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 459 */
BgL_test3866z00_6166 = 
(BgL_startz00_61>BgL_lenz00_976)
; } 
if(BgL_test3866z00_6166)
{ /* Ieee/string.scm 461 */
 obj_t BgL_arg1234z00_979; obj_t BgL_arg1236z00_980;
{ /* Ieee/string.scm 461 */
 obj_t BgL_arg1238z00_981;
{ /* Ieee/fixnum.scm 1002 */

BgL_arg1238z00_981 = 
BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_startz00_61, 10L); } 
BgL_arg1234z00_979 = 
string_append(BGl_string3396z00zz__r4_strings_6_7z00, BgL_arg1238z00_981); } 
{ /* Ieee/string.scm 462 */
 obj_t BgL_list1243z00_985;
{ /* Ieee/string.scm 462 */
 obj_t BgL_arg1244z00_986;
BgL_arg1244z00_986 = 
MAKE_YOUNG_PAIR(BgL_stringz00_60, BNIL); 
BgL_list1243z00_985 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lenz00_976), BgL_arg1244z00_986); } 
BgL_arg1236z00_980 = BgL_list1243z00_985; } 
{ /* Ieee/string.scm 460 */
 obj_t BgL_aux2760z00_4718;
BgL_aux2760z00_4718 = 
BGl_errorz00zz__errorz00(BGl_string3393z00zz__r4_strings_6_7z00, BgL_arg1234z00_979, BgL_arg1236z00_980); 
if(
STRINGP(BgL_aux2760z00_4718))
{ /* Ieee/string.scm 460 */
return BgL_aux2760z00_4718;}  else 
{ 
 obj_t BgL_auxz00_6178;
BgL_auxz00_6178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22513L), BGl_string3393z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2760z00_4718); 
FAILURE(BgL_auxz00_6178,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 463 */
 bool_t BgL_test3869z00_6182;
if(
(BgL_endz00_62<BgL_startz00_61))
{ /* Ieee/string.scm 463 */
BgL_test3869z00_6182 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 463 */
BgL_test3869z00_6182 = 
(BgL_endz00_62>BgL_lenz00_976)
; } 
if(BgL_test3869z00_6182)
{ /* Ieee/string.scm 465 */
 obj_t BgL_arg1248z00_989; obj_t BgL_arg1249z00_990;
{ /* Ieee/string.scm 465 */
 obj_t BgL_arg1252z00_991;
{ /* Ieee/fixnum.scm 1002 */

BgL_arg1252z00_991 = 
BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_endz00_62, 10L); } 
BgL_arg1248z00_989 = 
string_append(BGl_string3397z00zz__r4_strings_6_7z00, BgL_arg1252z00_991); } 
{ /* Ieee/string.scm 466 */
 obj_t BgL_list1269z00_995;
{ /* Ieee/string.scm 466 */
 obj_t BgL_arg1272z00_996;
BgL_arg1272z00_996 = 
MAKE_YOUNG_PAIR(BgL_stringz00_60, BNIL); 
BgL_list1269z00_995 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lenz00_976), BgL_arg1272z00_996); } 
BgL_arg1249z00_990 = BgL_list1269z00_995; } 
{ /* Ieee/string.scm 464 */
 obj_t BgL_aux2762z00_4720;
BgL_aux2762z00_4720 = 
BGl_errorz00zz__errorz00(BGl_string3393z00zz__r4_strings_6_7z00, BgL_arg1248z00_989, BgL_arg1249z00_990); 
if(
STRINGP(BgL_aux2762z00_4720))
{ /* Ieee/string.scm 464 */
return BgL_aux2762z00_4720;}  else 
{ 
 obj_t BgL_auxz00_6194;
BgL_auxz00_6194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(22680L), BGl_string3393z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2762z00_4720); 
FAILURE(BgL_auxz00_6194,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 463 */
BGL_TAIL return 
c_substring(BgL_stringz00_60, BgL_startz00_61, BgL_endz00_62);} } } } } 

}



/* substring-ur */
BGL_EXPORTED_DEF obj_t BGl_substringzd2urzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_65, long BgL_startz00_66, long BgL_endz00_67)
{
{ /* Ieee/string.scm 473 */
BGL_TAIL return 
c_substring(BgL_stringz00_65, BgL_startz00_66, BgL_endz00_67);} 

}



/* &substring-ur */
obj_t BGl_z62substringzd2urzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4098, obj_t BgL_stringz00_4099, obj_t BgL_startz00_4100, obj_t BgL_endz00_4101)
{
{ /* Ieee/string.scm 473 */
{ /* Ieee/string.scm 474 */
 long BgL_auxz00_6216; long BgL_auxz00_6207; obj_t BgL_auxz00_6200;
{ /* Ieee/string.scm 474 */
 obj_t BgL_tmpz00_6217;
if(
INTEGERP(BgL_endz00_4101))
{ /* Ieee/string.scm 474 */
BgL_tmpz00_6217 = BgL_endz00_4101
; }  else 
{ 
 obj_t BgL_auxz00_6220;
BgL_auxz00_6220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23120L), BGl_string3398z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_endz00_4101); 
FAILURE(BgL_auxz00_6220,BFALSE,BFALSE);} 
BgL_auxz00_6216 = 
(long)CINT(BgL_tmpz00_6217); } 
{ /* Ieee/string.scm 474 */
 obj_t BgL_tmpz00_6208;
if(
INTEGERP(BgL_startz00_4100))
{ /* Ieee/string.scm 474 */
BgL_tmpz00_6208 = BgL_startz00_4100
; }  else 
{ 
 obj_t BgL_auxz00_6211;
BgL_auxz00_6211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23120L), BGl_string3398z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_4100); 
FAILURE(BgL_auxz00_6211,BFALSE,BFALSE);} 
BgL_auxz00_6207 = 
(long)CINT(BgL_tmpz00_6208); } 
if(
STRINGP(BgL_stringz00_4099))
{ /* Ieee/string.scm 474 */
BgL_auxz00_6200 = BgL_stringz00_4099
; }  else 
{ 
 obj_t BgL_auxz00_6203;
BgL_auxz00_6203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23120L), BGl_string3398z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4099); 
FAILURE(BgL_auxz00_6203,BFALSE,BFALSE);} 
return 
BGl_substringzd2urzd2zz__r4_strings_6_7z00(BgL_auxz00_6200, BgL_auxz00_6207, BgL_auxz00_6216);} } 

}



/* _string-contains */
obj_t BGl__stringzd2containszd2zz__r4_strings_6_7z00(obj_t BgL_env1117z00_72, obj_t BgL_opt1116z00_71)
{
{ /* Ieee/string.scm 479 */
{ /* Ieee/string.scm 479 */
 obj_t BgL_g1118z00_999; obj_t BgL_g1119z00_1000;
BgL_g1118z00_999 = 
VECTOR_REF(BgL_opt1116z00_71,0L); 
BgL_g1119z00_1000 = 
VECTOR_REF(BgL_opt1116z00_71,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1116z00_71)) { case 2L : 

{ /* Ieee/string.scm 479 */

{ /* Ieee/string.scm 479 */
 obj_t BgL_auxz00_6235; obj_t BgL_auxz00_6228;
if(
STRINGP(BgL_g1119z00_1000))
{ /* Ieee/string.scm 479 */
BgL_auxz00_6235 = BgL_g1119z00_1000
; }  else 
{ 
 obj_t BgL_auxz00_6238;
BgL_auxz00_6238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23374L), BGl_string3401z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1119z00_1000); 
FAILURE(BgL_auxz00_6238,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1118z00_999))
{ /* Ieee/string.scm 479 */
BgL_auxz00_6228 = BgL_g1118z00_999
; }  else 
{ 
 obj_t BgL_auxz00_6231;
BgL_auxz00_6231 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23374L), BGl_string3401z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1118z00_999); 
FAILURE(BgL_auxz00_6231,BFALSE,BFALSE);} 
return 
BGl_stringzd2containszd2zz__r4_strings_6_7z00(BgL_auxz00_6228, BgL_auxz00_6235, 
(int)(0L));} } break;case 3L : 

{ /* Ieee/string.scm 479 */
 obj_t BgL_startz00_1004;
BgL_startz00_1004 = 
VECTOR_REF(BgL_opt1116z00_71,2L); 
{ /* Ieee/string.scm 479 */

{ /* Ieee/string.scm 479 */
 int BgL_auxz00_6259; obj_t BgL_auxz00_6252; obj_t BgL_auxz00_6245;
{ /* Ieee/string.scm 479 */
 obj_t BgL_tmpz00_6260;
if(
INTEGERP(BgL_startz00_1004))
{ /* Ieee/string.scm 479 */
BgL_tmpz00_6260 = BgL_startz00_1004
; }  else 
{ 
 obj_t BgL_auxz00_6263;
BgL_auxz00_6263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23374L), BGl_string3401z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_1004); 
FAILURE(BgL_auxz00_6263,BFALSE,BFALSE);} 
BgL_auxz00_6259 = 
CINT(BgL_tmpz00_6260); } 
if(
STRINGP(BgL_g1119z00_1000))
{ /* Ieee/string.scm 479 */
BgL_auxz00_6252 = BgL_g1119z00_1000
; }  else 
{ 
 obj_t BgL_auxz00_6255;
BgL_auxz00_6255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23374L), BGl_string3401z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1119z00_1000); 
FAILURE(BgL_auxz00_6255,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1118z00_999))
{ /* Ieee/string.scm 479 */
BgL_auxz00_6245 = BgL_g1118z00_999
; }  else 
{ 
 obj_t BgL_auxz00_6248;
BgL_auxz00_6248 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(23374L), BGl_string3401z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1118z00_999); 
FAILURE(BgL_auxz00_6248,BFALSE,BFALSE);} 
return 
BGl_stringzd2containszd2zz__r4_strings_6_7z00(BgL_auxz00_6245, BgL_auxz00_6252, BgL_auxz00_6259);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3399z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1116z00_71)));} } } } 

}



/* string-contains */
BGL_EXPORTED_DEF obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t BgL_s1z00_68, obj_t BgL_s2z00_69, int BgL_startz00_70)
{
{ /* Ieee/string.scm 479 */
{ /* Ieee/string.scm 480 */
 long BgL_l2z00_1006;
BgL_l2z00_1006 = 
STRING_LENGTH(BgL_s2z00_69); 
if(
(BgL_l2z00_1006==1L))
{ /* Ieee/string.scm 482 */
 unsigned char BgL_arg1304z00_1008;
if(
BOUND_CHECK(0L, BgL_l2z00_1006))
{ /* Ieee/string.scm 331 */
BgL_arg1304z00_1008 = 
STRING_REF(BgL_s2z00_69, 0L); }  else 
{ 
 obj_t BgL_auxz00_6280;
BgL_auxz00_6280 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_69, 
(int)(BgL_l2z00_1006), 
(int)(0L)); 
FAILURE(BgL_auxz00_6280,BFALSE,BFALSE);} 
return 
BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_s1z00_68, 
BCHAR(BgL_arg1304z00_1008), 
BINT(BgL_startz00_70));}  else 
{ /* Ieee/string.scm 483 */
 long BgL_l1z00_1009; long BgL_i0z00_1010;
BgL_l1z00_1009 = 
STRING_LENGTH(BgL_s1z00_68); 
if(
(
(long)(BgL_startz00_70)<0L))
{ /* Ieee/string.scm 484 */
BgL_i0z00_1010 = 0L; }  else 
{ /* Ieee/string.scm 484 */
BgL_i0z00_1010 = 
(long)(BgL_startz00_70); } 
if(
(BgL_l1z00_1009<
(BgL_i0z00_1010+BgL_l2z00_1006)))
{ /* Ieee/string.scm 485 */
return BFALSE;}  else 
{ /* Ieee/string.scm 487 */
 long BgL_stopz00_1013;
BgL_stopz00_1013 = 
(BgL_l1z00_1009-BgL_l2z00_1006); 
{ 
 long BgL_iz00_1015;
BgL_iz00_1015 = BgL_i0z00_1010; 
BgL_zc3z04anonymousza31307ze3z87_1016:
if(
bigloo_strcmp_at(BgL_s1z00_68, BgL_s2z00_69, BgL_iz00_1015))
{ /* Ieee/string.scm 490 */
return 
BINT(BgL_iz00_1015);}  else 
{ /* Ieee/string.scm 490 */
if(
(BgL_iz00_1015==BgL_stopz00_1013))
{ /* Ieee/string.scm 492 */
return BFALSE;}  else 
{ 
 long BgL_iz00_6303;
BgL_iz00_6303 = 
(BgL_iz00_1015+1L); 
BgL_iz00_1015 = BgL_iz00_6303; 
goto BgL_zc3z04anonymousza31307ze3z87_1016;} } } } } } } 

}



/* _string-contains-ci */
obj_t BGl__stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t BgL_env1123z00_77, obj_t BgL_opt1122z00_76)
{
{ /* Ieee/string.scm 500 */
{ /* Ieee/string.scm 500 */
 obj_t BgL_g1124z00_1023; obj_t BgL_g1125z00_1024;
BgL_g1124z00_1023 = 
VECTOR_REF(BgL_opt1122z00_76,0L); 
BgL_g1125z00_1024 = 
VECTOR_REF(BgL_opt1122z00_76,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1122z00_76)) { case 2L : 

{ /* Ieee/string.scm 500 */

{ /* Ieee/string.scm 500 */
 obj_t BgL_auxz00_6314; obj_t BgL_auxz00_6307;
if(
STRINGP(BgL_g1125z00_1024))
{ /* Ieee/string.scm 500 */
BgL_auxz00_6314 = BgL_g1125z00_1024
; }  else 
{ 
 obj_t BgL_auxz00_6317;
BgL_auxz00_6317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24040L), BGl_string3404z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1125z00_1024); 
FAILURE(BgL_auxz00_6317,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1124z00_1023))
{ /* Ieee/string.scm 500 */
BgL_auxz00_6307 = BgL_g1124z00_1023
; }  else 
{ 
 obj_t BgL_auxz00_6310;
BgL_auxz00_6310 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24040L), BGl_string3404z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1124z00_1023); 
FAILURE(BgL_auxz00_6310,BFALSE,BFALSE);} 
return 
BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(BgL_auxz00_6307, BgL_auxz00_6314, 
(int)(0L));} } break;case 3L : 

{ /* Ieee/string.scm 500 */
 obj_t BgL_startz00_1028;
BgL_startz00_1028 = 
VECTOR_REF(BgL_opt1122z00_76,2L); 
{ /* Ieee/string.scm 500 */

{ /* Ieee/string.scm 500 */
 int BgL_auxz00_6338; obj_t BgL_auxz00_6331; obj_t BgL_auxz00_6324;
{ /* Ieee/string.scm 500 */
 obj_t BgL_tmpz00_6339;
if(
INTEGERP(BgL_startz00_1028))
{ /* Ieee/string.scm 500 */
BgL_tmpz00_6339 = BgL_startz00_1028
; }  else 
{ 
 obj_t BgL_auxz00_6342;
BgL_auxz00_6342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24040L), BGl_string3404z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_1028); 
FAILURE(BgL_auxz00_6342,BFALSE,BFALSE);} 
BgL_auxz00_6338 = 
CINT(BgL_tmpz00_6339); } 
if(
STRINGP(BgL_g1125z00_1024))
{ /* Ieee/string.scm 500 */
BgL_auxz00_6331 = BgL_g1125z00_1024
; }  else 
{ 
 obj_t BgL_auxz00_6334;
BgL_auxz00_6334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24040L), BGl_string3404z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1125z00_1024); 
FAILURE(BgL_auxz00_6334,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1124z00_1023))
{ /* Ieee/string.scm 500 */
BgL_auxz00_6324 = BgL_g1124z00_1023
; }  else 
{ 
 obj_t BgL_auxz00_6327;
BgL_auxz00_6327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24040L), BGl_string3404z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1124z00_1023); 
FAILURE(BgL_auxz00_6327,BFALSE,BFALSE);} 
return 
BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(BgL_auxz00_6324, BgL_auxz00_6331, BgL_auxz00_6338);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3402z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1122z00_76)));} } } } 

}



/* string-contains-ci */
BGL_EXPORTED_DEF obj_t BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t BgL_s1z00_73, obj_t BgL_s2z00_74, int BgL_startz00_75)
{
{ /* Ieee/string.scm 500 */
{ /* Ieee/string.scm 501 */
 long BgL_l1z00_1030; long BgL_l2z00_1031; long BgL_i0z00_1032;
BgL_l1z00_1030 = 
STRING_LENGTH(BgL_s1z00_73); 
BgL_l2z00_1031 = 
STRING_LENGTH(BgL_s2z00_74); 
if(
(
(long)(BgL_startz00_75)<0L))
{ /* Ieee/string.scm 503 */
BgL_i0z00_1032 = 0L; }  else 
{ /* Ieee/string.scm 503 */
BgL_i0z00_1032 = 
(long)(BgL_startz00_75); } 
if(
(BgL_l1z00_1030<
(BgL_i0z00_1032+BgL_l2z00_1031)))
{ /* Ieee/string.scm 504 */
return BFALSE;}  else 
{ /* Ieee/string.scm 506 */
 long BgL_stopz00_1035;
BgL_stopz00_1035 = 
(BgL_l1z00_1030-BgL_l2z00_1031); 
{ 
 long BgL_iz00_1037;
BgL_iz00_1037 = BgL_i0z00_1032; 
BgL_zc3z04anonymousza31317ze3z87_1038:
if(
bigloo_strcmp_ci_at(BgL_s1z00_73, BgL_s2z00_74, BgL_iz00_1037))
{ /* Ieee/string.scm 509 */
return 
BINT(BgL_iz00_1037);}  else 
{ /* Ieee/string.scm 509 */
if(
(BgL_iz00_1037==BgL_stopz00_1035))
{ /* Ieee/string.scm 511 */
return BFALSE;}  else 
{ 
 long BgL_iz00_6368;
BgL_iz00_6368 = 
(BgL_iz00_1037+1L); 
BgL_iz00_1037 = BgL_iz00_6368; 
goto BgL_zc3z04anonymousza31317ze3z87_1038;} } } } } } 

}



/* string-compare3 */
BGL_EXPORTED_DEF long BGl_stringzd2compare3zd2zz__r4_strings_6_7z00(obj_t BgL_az00_78, obj_t BgL_bz00_79)
{
{ /* Ieee/string.scm 523 */
{ /* Ieee/string.scm 524 */
 long BgL_alz00_1045;
BgL_alz00_1045 = 
STRING_LENGTH(BgL_az00_78); 
{ /* Ieee/string.scm 524 */
 long BgL_blz00_1046;
BgL_blz00_1046 = 
STRING_LENGTH(BgL_bz00_79); 
{ /* Ieee/string.scm 525 */
 long BgL_lz00_1047;
if(
(BgL_alz00_1045<BgL_blz00_1046))
{ /* Ieee/string.scm 526 */
BgL_lz00_1047 = BgL_alz00_1045; }  else 
{ /* Ieee/string.scm 526 */
BgL_lz00_1047 = BgL_blz00_1046; } 
{ /* Ieee/string.scm 526 */

{ 
 long BgL_rz00_1049;
BgL_rz00_1049 = 0L; 
BgL_zc3z04anonymousza31323ze3z87_1050:
if(
(BgL_rz00_1049==BgL_lz00_1047))
{ /* Ieee/string.scm 528 */
return 
(BgL_alz00_1045-BgL_blz00_1046);}  else 
{ /* Ieee/string.scm 530 */
 long BgL_cz00_1052;
{ /* Ieee/string.scm 530 */
 long BgL_auxz00_6390; long BgL_tmpz00_6377;
{ /* Ieee/string.scm 531 */
 unsigned char BgL_tmpz00_6391;
{ /* Ieee/string.scm 343 */
 long BgL_l2245z00_4203;
BgL_l2245z00_4203 = 
STRING_LENGTH(BgL_bz00_79); 
if(
BOUND_CHECK(BgL_rz00_1049, BgL_l2245z00_4203))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6391 = 
STRING_REF(BgL_bz00_79, BgL_rz00_1049)
; }  else 
{ 
 obj_t BgL_auxz00_6396;
BgL_auxz00_6396 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_bz00_79, 
(int)(BgL_l2245z00_4203), 
(int)(BgL_rz00_1049)); 
FAILURE(BgL_auxz00_6396,BFALSE,BFALSE);} } 
BgL_auxz00_6390 = 
(BgL_tmpz00_6391); } 
{ /* Ieee/string.scm 530 */
 unsigned char BgL_tmpz00_6378;
{ /* Ieee/string.scm 343 */
 long BgL_l2241z00_4199;
BgL_l2241z00_4199 = 
STRING_LENGTH(BgL_az00_78); 
if(
BOUND_CHECK(BgL_rz00_1049, BgL_l2241z00_4199))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6378 = 
STRING_REF(BgL_az00_78, BgL_rz00_1049)
; }  else 
{ 
 obj_t BgL_auxz00_6383;
BgL_auxz00_6383 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_az00_78, 
(int)(BgL_l2241z00_4199), 
(int)(BgL_rz00_1049)); 
FAILURE(BgL_auxz00_6383,BFALSE,BFALSE);} } 
BgL_tmpz00_6377 = 
(BgL_tmpz00_6378); } 
BgL_cz00_1052 = 
(BgL_tmpz00_6377-BgL_auxz00_6390); } 
if(
(BgL_cz00_1052==0L))
{ 
 long BgL_rz00_6406;
BgL_rz00_6406 = 
(BgL_rz00_1049+1L); 
BgL_rz00_1049 = BgL_rz00_6406; 
goto BgL_zc3z04anonymousza31323ze3z87_1050;}  else 
{ /* Ieee/string.scm 532 */
return BgL_cz00_1052;} } } } } } } } 

}



/* &string-compare3 */
obj_t BGl_z62stringzd2compare3zb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4102, obj_t BgL_az00_4103, obj_t BgL_bz00_4104)
{
{ /* Ieee/string.scm 523 */
{ /* Ieee/string.scm 524 */
 long BgL_tmpz00_6408;
{ /* Ieee/string.scm 524 */
 obj_t BgL_auxz00_6416; obj_t BgL_auxz00_6409;
if(
STRINGP(BgL_bz00_4104))
{ /* Ieee/string.scm 524 */
BgL_auxz00_6416 = BgL_bz00_4104
; }  else 
{ 
 obj_t BgL_auxz00_6419;
BgL_auxz00_6419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24981L), BGl_string3405z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_bz00_4104); 
FAILURE(BgL_auxz00_6419,BFALSE,BFALSE);} 
if(
STRINGP(BgL_az00_4103))
{ /* Ieee/string.scm 524 */
BgL_auxz00_6409 = BgL_az00_4103
; }  else 
{ 
 obj_t BgL_auxz00_6412;
BgL_auxz00_6412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(24981L), BGl_string3405z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_az00_4103); 
FAILURE(BgL_auxz00_6412,BFALSE,BFALSE);} 
BgL_tmpz00_6408 = 
BGl_stringzd2compare3zd2zz__r4_strings_6_7z00(BgL_auxz00_6409, BgL_auxz00_6416); } 
return 
BINT(BgL_tmpz00_6408);} } 

}



/* string-compare3-ci */
BGL_EXPORTED_DEF long BGl_stringzd2compare3zd2ciz00zz__r4_strings_6_7z00(obj_t BgL_az00_80, obj_t BgL_bz00_81)
{
{ /* Ieee/string.scm 539 */
{ /* Ieee/string.scm 540 */
 long BgL_alz00_1063;
BgL_alz00_1063 = 
STRING_LENGTH(BgL_az00_80); 
{ /* Ieee/string.scm 540 */
 long BgL_blz00_1064;
BgL_blz00_1064 = 
STRING_LENGTH(BgL_bz00_81); 
{ /* Ieee/string.scm 541 */
 long BgL_lz00_1065;
if(
(BgL_alz00_1063<BgL_blz00_1064))
{ /* Ieee/string.scm 542 */
BgL_lz00_1065 = BgL_alz00_1063; }  else 
{ /* Ieee/string.scm 542 */
BgL_lz00_1065 = BgL_blz00_1064; } 
{ /* Ieee/string.scm 542 */

{ 
 long BgL_rz00_1067;
BgL_rz00_1067 = 0L; 
BgL_zc3z04anonymousza31333ze3z87_1068:
if(
(BgL_rz00_1067==BgL_lz00_1065))
{ /* Ieee/string.scm 544 */
return 
(BgL_alz00_1063-BgL_blz00_1064);}  else 
{ /* Ieee/string.scm 546 */
 long BgL_cz00_1070;
{ /* Ieee/string.scm 546 */
 long BgL_auxz00_6447; long BgL_tmpz00_6432;
{ /* Ieee/string.scm 547 */
 unsigned char BgL_tmpz00_6448;
{ /* Ieee/string.scm 547 */
 unsigned char BgL_tmpz00_6449;
{ /* Ieee/string.scm 343 */
 long BgL_l2253z00_4211;
BgL_l2253z00_4211 = 
STRING_LENGTH(BgL_bz00_81); 
if(
BOUND_CHECK(BgL_rz00_1067, BgL_l2253z00_4211))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6449 = 
STRING_REF(BgL_bz00_81, BgL_rz00_1067)
; }  else 
{ 
 obj_t BgL_auxz00_6454;
BgL_auxz00_6454 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_bz00_81, 
(int)(BgL_l2253z00_4211), 
(int)(BgL_rz00_1067)); 
FAILURE(BgL_auxz00_6454,BFALSE,BFALSE);} } 
BgL_tmpz00_6448 = 
tolower(BgL_tmpz00_6449); } 
BgL_auxz00_6447 = 
(BgL_tmpz00_6448); } 
{ /* Ieee/string.scm 546 */
 unsigned char BgL_tmpz00_6433;
{ /* Ieee/string.scm 546 */
 unsigned char BgL_tmpz00_6434;
{ /* Ieee/string.scm 343 */
 long BgL_l2249z00_4207;
BgL_l2249z00_4207 = 
STRING_LENGTH(BgL_az00_80); 
if(
BOUND_CHECK(BgL_rz00_1067, BgL_l2249z00_4207))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6434 = 
STRING_REF(BgL_az00_80, BgL_rz00_1067)
; }  else 
{ 
 obj_t BgL_auxz00_6439;
BgL_auxz00_6439 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_az00_80, 
(int)(BgL_l2249z00_4207), 
(int)(BgL_rz00_1067)); 
FAILURE(BgL_auxz00_6439,BFALSE,BFALSE);} } 
BgL_tmpz00_6433 = 
tolower(BgL_tmpz00_6434); } 
BgL_tmpz00_6432 = 
(BgL_tmpz00_6433); } 
BgL_cz00_1070 = 
(BgL_tmpz00_6432-BgL_auxz00_6447); } 
if(
(BgL_cz00_1070==0L))
{ 
 long BgL_rz00_6465;
BgL_rz00_6465 = 
(BgL_rz00_1067+1L); 
BgL_rz00_1067 = BgL_rz00_6465; 
goto BgL_zc3z04anonymousza31333ze3z87_1068;}  else 
{ /* Ieee/string.scm 548 */
return BgL_cz00_1070;} } } } } } } } 

}



/* &string-compare3-ci */
obj_t BGl_z62stringzd2compare3zd2ciz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4105, obj_t BgL_az00_4106, obj_t BgL_bz00_4107)
{
{ /* Ieee/string.scm 539 */
{ /* Ieee/string.scm 540 */
 long BgL_tmpz00_6467;
{ /* Ieee/string.scm 540 */
 obj_t BgL_auxz00_6475; obj_t BgL_auxz00_6468;
if(
STRINGP(BgL_bz00_4107))
{ /* Ieee/string.scm 540 */
BgL_auxz00_6475 = BgL_bz00_4107
; }  else 
{ 
 obj_t BgL_auxz00_6478;
BgL_auxz00_6478 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(25633L), BGl_string3406z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_bz00_4107); 
FAILURE(BgL_auxz00_6478,BFALSE,BFALSE);} 
if(
STRINGP(BgL_az00_4106))
{ /* Ieee/string.scm 540 */
BgL_auxz00_6468 = BgL_az00_4106
; }  else 
{ 
 obj_t BgL_auxz00_6471;
BgL_auxz00_6471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(25633L), BGl_string3406z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_az00_4106); 
FAILURE(BgL_auxz00_6471,BFALSE,BFALSE);} 
BgL_tmpz00_6467 = 
BGl_stringzd2compare3zd2ciz00zz__r4_strings_6_7z00(BgL_auxz00_6468, BgL_auxz00_6475); } 
return 
BINT(BgL_tmpz00_6467);} } 

}



/* string-append */
BGL_EXPORTED_DEF obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t BgL_listz00_82)
{
{ /* Ieee/string.scm 559 */
if(
NULLP(BgL_listz00_82))
{ /* Ieee/string.scm 560 */
return BGl_string3407z00zz__r4_strings_6_7z00;}  else 
{ /* Ieee/string.scm 562 */
 long BgL_lenz00_1084;
{ 
 obj_t BgL_listz00_2738; long BgL_resz00_2739;
BgL_listz00_2738 = BgL_listz00_82; 
BgL_resz00_2739 = 0L; 
BgL_stringzd2appendzd2_2737:
if(
NULLP(BgL_listz00_2738))
{ /* Ieee/string.scm 564 */
BgL_lenz00_1084 = BgL_resz00_2739; }  else 
{ 
 long BgL_resz00_6496; obj_t BgL_listz00_6488;
{ /* Ieee/string.scm 566 */
 obj_t BgL_pairz00_2750;
if(
PAIRP(BgL_listz00_2738))
{ /* Ieee/string.scm 566 */
BgL_pairz00_2750 = BgL_listz00_2738; }  else 
{ 
 obj_t BgL_auxz00_6491;
BgL_auxz00_6491 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(26746L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3409z00zz__r4_strings_6_7z00, BgL_listz00_2738); 
FAILURE(BgL_auxz00_6491,BFALSE,BFALSE);} 
BgL_listz00_6488 = 
CDR(BgL_pairz00_2750); } 
{ /* Ieee/string.scm 567 */
 long BgL_tmpz00_6497;
{ /* Ieee/string.scm 568 */
 obj_t BgL_stringz00_2752;
{ /* Ieee/string.scm 568 */
 obj_t BgL_pairz00_2751;
if(
PAIRP(BgL_listz00_2738))
{ /* Ieee/string.scm 568 */
BgL_pairz00_2751 = BgL_listz00_2738; }  else 
{ 
 obj_t BgL_auxz00_6500;
BgL_auxz00_6500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(26793L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3409z00zz__r4_strings_6_7z00, BgL_listz00_2738); 
FAILURE(BgL_auxz00_6500,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 568 */
 obj_t BgL_aux2798z00_4756;
BgL_aux2798z00_4756 = 
CAR(BgL_pairz00_2751); 
if(
STRINGP(BgL_aux2798z00_4756))
{ /* Ieee/string.scm 568 */
BgL_stringz00_2752 = BgL_aux2798z00_4756; }  else 
{ 
 obj_t BgL_auxz00_6507;
BgL_auxz00_6507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(26788L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2798z00_4756); 
FAILURE(BgL_auxz00_6507,BFALSE,BFALSE);} } } 
BgL_tmpz00_6497 = 
STRING_LENGTH(BgL_stringz00_2752); } 
BgL_resz00_6496 = 
(BgL_resz00_2739+BgL_tmpz00_6497); } 
BgL_resz00_2739 = BgL_resz00_6496; 
BgL_listz00_2738 = BgL_listz00_6488; 
goto BgL_stringzd2appendzd2_2737;} } 
{ /* Ieee/string.scm 562 */
 obj_t BgL_resz00_1085;
BgL_resz00_1085 = 
make_string_sans_fill(BgL_lenz00_1084); 
{ /* Ieee/string.scm 569 */

{ 
 obj_t BgL_listz00_2765; long BgL_wz00_2766;
BgL_listz00_2765 = BgL_listz00_82; 
BgL_wz00_2766 = 0L; 
BgL_stringzd2appendzd2_2764:
if(
NULLP(BgL_listz00_2765))
{ /* Ieee/string.scm 572 */
return BgL_resz00_1085;}  else 
{ /* Ieee/string.scm 574 */
 obj_t BgL_sz00_2768;
{ /* Ieee/string.scm 574 */
 obj_t BgL_pairz00_2769;
if(
PAIRP(BgL_listz00_2765))
{ /* Ieee/string.scm 574 */
BgL_pairz00_2769 = BgL_listz00_2765; }  else 
{ 
 obj_t BgL_auxz00_6518;
BgL_auxz00_6518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(26945L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3409z00zz__r4_strings_6_7z00, BgL_listz00_2765); 
FAILURE(BgL_auxz00_6518,BFALSE,BFALSE);} 
BgL_sz00_2768 = 
CAR(BgL_pairz00_2769); } 
{ /* Ieee/string.scm 574 */
 long BgL_lz00_2770;
{ /* Ieee/string.scm 575 */
 obj_t BgL_stringz00_2771;
if(
STRINGP(BgL_sz00_2768))
{ /* Ieee/string.scm 575 */
BgL_stringz00_2771 = BgL_sz00_2768; }  else 
{ 
 obj_t BgL_auxz00_6525;
BgL_auxz00_6525 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(26958L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_2768); 
FAILURE(BgL_auxz00_6525,BFALSE,BFALSE);} 
BgL_lz00_2770 = 
STRING_LENGTH(BgL_stringz00_2771); } 
{ /* Ieee/string.scm 575 */

{ /* Ieee/string.scm 576 */
 obj_t BgL_s1z00_2772;
if(
STRINGP(BgL_sz00_2768))
{ /* Ieee/string.scm 576 */
BgL_s1z00_2772 = BgL_sz00_2768; }  else 
{ 
 obj_t BgL_auxz00_6532;
BgL_auxz00_6532 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27001L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_2768); 
FAILURE(BgL_auxz00_6532,BFALSE,BFALSE);} 
blit_string(BgL_s1z00_2772, 0L, BgL_resz00_1085, BgL_wz00_2766, BgL_lz00_2770); } 
{ /* Ieee/string.scm 577 */
 obj_t BgL_arg1347z00_2776; long BgL_arg1348z00_2777;
{ /* Ieee/string.scm 577 */
 obj_t BgL_pairz00_2778;
if(
PAIRP(BgL_listz00_2765))
{ /* Ieee/string.scm 577 */
BgL_pairz00_2778 = BgL_listz00_2765; }  else 
{ 
 obj_t BgL_auxz00_6539;
BgL_auxz00_6539 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27040L), BGl_string3408z00zz__r4_strings_6_7z00, BGl_string3409z00zz__r4_strings_6_7z00, BgL_listz00_2765); 
FAILURE(BgL_auxz00_6539,BFALSE,BFALSE);} 
BgL_arg1347z00_2776 = 
CDR(BgL_pairz00_2778); } 
BgL_arg1348z00_2777 = 
(BgL_wz00_2766+BgL_lz00_2770); 
{ 
 long BgL_wz00_6546; obj_t BgL_listz00_6545;
BgL_listz00_6545 = BgL_arg1347z00_2776; 
BgL_wz00_6546 = BgL_arg1348z00_2777; 
BgL_wz00_2766 = BgL_wz00_6546; 
BgL_listz00_2765 = BgL_listz00_6545; 
goto BgL_stringzd2appendzd2_2764;} } } } } } } } } } 

}



/* &string-append */
obj_t BGl_z62stringzd2appendzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4108, obj_t BgL_listz00_4109)
{
{ /* Ieee/string.scm 559 */
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_listz00_4109);} 

}



/* list->string */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t BgL_listz00_83)
{
{ /* Ieee/string.scm 582 */
{ /* Ieee/string.scm 583 */
 long BgL_lenz00_1106;
BgL_lenz00_1106 = 
bgl_list_length(BgL_listz00_83); 
{ /* Ieee/string.scm 583 */
 obj_t BgL_stringz00_1107;
BgL_stringz00_1107 = 
make_string_sans_fill(BgL_lenz00_1106); 
{ /* Ieee/string.scm 584 */

{ 
 long BgL_iz00_2790; obj_t BgL_lz00_2791;
BgL_iz00_2790 = 0L; 
BgL_lz00_2791 = BgL_listz00_83; 
BgL_loopz00_2789:
if(
(BgL_iz00_2790==BgL_lenz00_1106))
{ /* Ieee/string.scm 587 */
return BgL_stringz00_1107;}  else 
{ /* Ieee/string.scm 587 */
{ /* Ieee/string.scm 590 */
 obj_t BgL_arg1359z00_2795;
{ /* Ieee/string.scm 590 */
 obj_t BgL_pairz00_2796;
if(
PAIRP(BgL_lz00_2791))
{ /* Ieee/string.scm 590 */
BgL_pairz00_2796 = BgL_lz00_2791; }  else 
{ 
 obj_t BgL_auxz00_6554;
BgL_auxz00_6554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27500L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3409z00zz__r4_strings_6_7z00, BgL_lz00_2791); 
FAILURE(BgL_auxz00_6554,BFALSE,BFALSE);} 
BgL_arg1359z00_2795 = 
CAR(BgL_pairz00_2796); } 
{ /* Ieee/string.scm 590 */
 unsigned char BgL_charz00_2799;
{ /* Ieee/string.scm 590 */
 obj_t BgL_tmpz00_6559;
if(
CHARP(BgL_arg1359z00_2795))
{ /* Ieee/string.scm 590 */
BgL_tmpz00_6559 = BgL_arg1359z00_2795
; }  else 
{ 
 obj_t BgL_auxz00_6562;
BgL_auxz00_6562 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27501L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_arg1359z00_2795); 
FAILURE(BgL_auxz00_6562,BFALSE,BFALSE);} 
BgL_charz00_2799 = 
CCHAR(BgL_tmpz00_6559); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2257z00_4215;
BgL_l2257z00_4215 = 
STRING_LENGTH(BgL_stringz00_1107); 
if(
BOUND_CHECK(BgL_iz00_2790, BgL_l2257z00_4215))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_stringz00_1107, BgL_iz00_2790, BgL_charz00_2799); }  else 
{ 
 obj_t BgL_auxz00_6571;
BgL_auxz00_6571 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_1107, 
(int)(BgL_l2257z00_4215), 
(int)(BgL_iz00_2790)); 
FAILURE(BgL_auxz00_6571,BFALSE,BFALSE);} } } } 
{ /* Ieee/string.scm 591 */
 long BgL_arg1360z00_2800; obj_t BgL_arg1361z00_2801;
BgL_arg1360z00_2800 = 
(BgL_iz00_2790+1L); 
{ /* Ieee/string.scm 591 */
 obj_t BgL_pairz00_2803;
if(
PAIRP(BgL_lz00_2791))
{ /* Ieee/string.scm 591 */
BgL_pairz00_2803 = BgL_lz00_2791; }  else 
{ 
 obj_t BgL_auxz00_6580;
BgL_auxz00_6580 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27527L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3409z00zz__r4_strings_6_7z00, BgL_lz00_2791); 
FAILURE(BgL_auxz00_6580,BFALSE,BFALSE);} 
BgL_arg1361z00_2801 = 
CDR(BgL_pairz00_2803); } 
{ 
 obj_t BgL_lz00_6586; long BgL_iz00_6585;
BgL_iz00_6585 = BgL_arg1360z00_2800; 
BgL_lz00_6586 = BgL_arg1361z00_2801; 
BgL_lz00_2791 = BgL_lz00_6586; 
BgL_iz00_2790 = BgL_iz00_6585; 
goto BgL_loopz00_2789;} } } } } } } } 

}



/* &list->string */
obj_t BGl_z62listzd2ze3stringz53zz__r4_strings_6_7z00(obj_t BgL_envz00_4110, obj_t BgL_listz00_4111)
{
{ /* Ieee/string.scm 582 */
{ /* Ieee/string.scm 583 */
 obj_t BgL_auxz00_6587;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_4111))
{ /* Ieee/string.scm 583 */
BgL_auxz00_6587 = BgL_listz00_4111
; }  else 
{ 
 obj_t BgL_auxz00_6590;
BgL_auxz00_6590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27318L), BGl_string3411z00zz__r4_strings_6_7z00, BGl_string3364z00zz__r4_strings_6_7z00, BgL_listz00_4111); 
FAILURE(BgL_auxz00_6590,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_auxz00_6587);} } 

}



/* string->list */
BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t BgL_stringz00_84)
{
{ /* Ieee/string.scm 596 */
{ /* Ieee/string.scm 597 */
 long BgL_lenz00_1117;
BgL_lenz00_1117 = 
STRING_LENGTH(BgL_stringz00_84); 
{ /* Ieee/string.scm 598 */
 long BgL_g1021z00_1118;
BgL_g1021z00_1118 = 
(BgL_lenz00_1117-1L); 
{ 
 long BgL_iz00_1121; obj_t BgL_resz00_1122;
BgL_iz00_1121 = BgL_g1021z00_1118; 
BgL_resz00_1122 = BNIL; 
BgL_zc3z04anonymousza31362ze3z87_1123:
if(
(BgL_iz00_1121==-1L))
{ /* Ieee/string.scm 600 */
return BgL_resz00_1122;}  else 
{ /* Ieee/string.scm 602 */
 long BgL_arg1364z00_1125; obj_t BgL_arg1365z00_1126;
BgL_arg1364z00_1125 = 
(BgL_iz00_1121-1L); 
{ /* Ieee/string.scm 603 */
 unsigned char BgL_arg1366z00_1127;
{ /* Ieee/string.scm 343 */
 long BgL_l2261z00_4219;
BgL_l2261z00_4219 = 
STRING_LENGTH(BgL_stringz00_84); 
if(
BOUND_CHECK(BgL_iz00_1121, BgL_l2261z00_4219))
{ /* Ieee/string.scm 343 */
BgL_arg1366z00_1127 = 
STRING_REF(BgL_stringz00_84, BgL_iz00_1121); }  else 
{ 
 obj_t BgL_auxz00_6604;
BgL_auxz00_6604 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_84, 
(int)(BgL_l2261z00_4219), 
(int)(BgL_iz00_1121)); 
FAILURE(BgL_auxz00_6604,BFALSE,BFALSE);} } 
BgL_arg1365z00_1126 = 
MAKE_YOUNG_PAIR(
BCHAR(BgL_arg1366z00_1127), BgL_resz00_1122); } 
{ 
 obj_t BgL_resz00_6613; long BgL_iz00_6612;
BgL_iz00_6612 = BgL_arg1364z00_1125; 
BgL_resz00_6613 = BgL_arg1365z00_1126; 
BgL_resz00_1122 = BgL_resz00_6613; 
BgL_iz00_1121 = BgL_iz00_6612; 
goto BgL_zc3z04anonymousza31362ze3z87_1123;} } } } } } 

}



/* &string->list */
obj_t BGl_z62stringzd2ze3listz53zz__r4_strings_6_7z00(obj_t BgL_envz00_4112, obj_t BgL_stringz00_4113)
{
{ /* Ieee/string.scm 596 */
{ /* Ieee/string.scm 597 */
 obj_t BgL_auxz00_6614;
if(
STRINGP(BgL_stringz00_4113))
{ /* Ieee/string.scm 597 */
BgL_auxz00_6614 = BgL_stringz00_4113
; }  else 
{ 
 obj_t BgL_auxz00_6617;
BgL_auxz00_6617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(27792L), BGl_string3412z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4113); 
FAILURE(BgL_auxz00_6617,BFALSE,BFALSE);} 
return 
BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_auxz00_6614);} } 

}



/* string-copy */
BGL_EXPORTED_DEF obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_85)
{
{ /* Ieee/string.scm 608 */
{ /* Ieee/string.scm 609 */
 long BgL_lenz00_1129;
BgL_lenz00_1129 = 
STRING_LENGTH(BgL_stringz00_85); 
{ /* Ieee/string.scm 609 */
 obj_t BgL_newz00_1130;
BgL_newz00_1130 = 
make_string_sans_fill(BgL_lenz00_1129); 
{ /* Ieee/string.scm 610 */

{ /* Ieee/string.scm 611 */
 long BgL_g1023z00_1131;
BgL_g1023z00_1131 = 
(BgL_lenz00_1129-1L); 
{ 
 long BgL_iz00_1133;
BgL_iz00_1133 = BgL_g1023z00_1131; 
BgL_zc3z04anonymousza31367ze3z87_1134:
if(
(BgL_iz00_1133==-1L))
{ /* Ieee/string.scm 612 */
return BgL_newz00_1130;}  else 
{ /* Ieee/string.scm 612 */
{ /* Ieee/string.scm 615 */
 unsigned char BgL_arg1369z00_1136;
{ /* Ieee/string.scm 343 */
 long BgL_l2265z00_4223;
BgL_l2265z00_4223 = 
STRING_LENGTH(BgL_stringz00_85); 
if(
BOUND_CHECK(BgL_iz00_1133, BgL_l2265z00_4223))
{ /* Ieee/string.scm 343 */
BgL_arg1369z00_1136 = 
STRING_REF(BgL_stringz00_85, BgL_iz00_1133); }  else 
{ 
 obj_t BgL_auxz00_6631;
BgL_auxz00_6631 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_85, 
(int)(BgL_l2265z00_4223), 
(int)(BgL_iz00_1133)); 
FAILURE(BgL_auxz00_6631,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 349 */
 long BgL_l2269z00_4227;
BgL_l2269z00_4227 = 
STRING_LENGTH(BgL_newz00_1130); 
if(
BOUND_CHECK(BgL_iz00_1133, BgL_l2269z00_4227))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_newz00_1130, BgL_iz00_1133, BgL_arg1369z00_1136); }  else 
{ 
 obj_t BgL_auxz00_6641;
BgL_auxz00_6641 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_newz00_1130, 
(int)(BgL_l2269z00_4227), 
(int)(BgL_iz00_1133)); 
FAILURE(BgL_auxz00_6641,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_6647;
BgL_iz00_6647 = 
(BgL_iz00_1133-1L); 
BgL_iz00_1133 = BgL_iz00_6647; 
goto BgL_zc3z04anonymousza31367ze3z87_1134;} } } } } } } } 

}



/* &string-copy */
obj_t BGl_z62stringzd2copyzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4114, obj_t BgL_stringz00_4115)
{
{ /* Ieee/string.scm 608 */
{ /* Ieee/string.scm 609 */
 obj_t BgL_auxz00_6649;
if(
STRINGP(BgL_stringz00_4115))
{ /* Ieee/string.scm 609 */
BgL_auxz00_6649 = BgL_stringz00_4115
; }  else 
{ 
 obj_t BgL_auxz00_6652;
BgL_auxz00_6652 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(28227L), BGl_string3413z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4115); 
FAILURE(BgL_auxz00_6652,BFALSE,BFALSE);} 
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_auxz00_6649);} } 

}



/* string-fill! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(obj_t BgL_stringz00_86, unsigned char BgL_charz00_87)
{
{ /* Ieee/string.scm 621 */
{ /* Ieee/string.scm 622 */
 long BgL_lenz00_1139;
BgL_lenz00_1139 = 
STRING_LENGTH(BgL_stringz00_86); 
{ 
 long BgL_iz00_2827;
BgL_iz00_2827 = 0L; 
BgL_loopz00_2826:
if(
(BgL_iz00_2827==BgL_lenz00_1139))
{ /* Ieee/string.scm 624 */
return BUNSPEC;}  else 
{ /* Ieee/string.scm 624 */
{ /* Ieee/string.scm 349 */
 long BgL_l2273z00_4231;
BgL_l2273z00_4231 = 
STRING_LENGTH(BgL_stringz00_86); 
if(
BOUND_CHECK(BgL_iz00_2827, BgL_l2273z00_4231))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_stringz00_86, BgL_iz00_2827, BgL_charz00_87); }  else 
{ 
 obj_t BgL_auxz00_6664;
BgL_auxz00_6664 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_86, 
(int)(BgL_l2273z00_4231), 
(int)(BgL_iz00_2827)); 
FAILURE(BgL_auxz00_6664,BFALSE,BFALSE);} } 
{ 
 long BgL_iz00_6670;
BgL_iz00_6670 = 
(BgL_iz00_2827+1L); 
BgL_iz00_2827 = BgL_iz00_6670; 
goto BgL_loopz00_2826;} } } } } 

}



/* &string-fill! */
obj_t BGl_z62stringzd2fillz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4116, obj_t BgL_stringz00_4117, obj_t BgL_charz00_4118)
{
{ /* Ieee/string.scm 621 */
{ /* Ieee/string.scm 622 */
 unsigned char BgL_auxz00_6679; obj_t BgL_auxz00_6672;
{ /* Ieee/string.scm 622 */
 obj_t BgL_tmpz00_6680;
if(
CHARP(BgL_charz00_4118))
{ /* Ieee/string.scm 622 */
BgL_tmpz00_6680 = BgL_charz00_4118
; }  else 
{ 
 obj_t BgL_auxz00_6683;
BgL_auxz00_6683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(28709L), BGl_string3414z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_charz00_4118); 
FAILURE(BgL_auxz00_6683,BFALSE,BFALSE);} 
BgL_auxz00_6679 = 
CCHAR(BgL_tmpz00_6680); } 
if(
STRINGP(BgL_stringz00_4117))
{ /* Ieee/string.scm 622 */
BgL_auxz00_6672 = BgL_stringz00_4117
; }  else 
{ 
 obj_t BgL_auxz00_6675;
BgL_auxz00_6675 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(28709L), BGl_string3414z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4117); 
FAILURE(BgL_auxz00_6675,BFALSE,BFALSE);} 
return 
BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(BgL_auxz00_6672, BgL_auxz00_6679);} } 

}



/* string-upcase */
BGL_EXPORTED_DEF obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_88)
{
{ /* Ieee/string.scm 633 */
{ /* Ieee/string.scm 634 */
 long BgL_lenz00_1146;
BgL_lenz00_1146 = 
STRING_LENGTH(BgL_stringz00_88); 
{ /* Ieee/string.scm 634 */
 obj_t BgL_resz00_1147;
BgL_resz00_1147 = 
make_string_sans_fill(BgL_lenz00_1146); 
{ /* Ieee/string.scm 635 */

{ 
 long BgL_iz00_1149;
BgL_iz00_1149 = 0L; 
BgL_zc3z04anonymousza31374ze3z87_1150:
if(
(BgL_iz00_1149==BgL_lenz00_1146))
{ /* Ieee/string.scm 637 */
return BgL_resz00_1147;}  else 
{ /* Ieee/string.scm 637 */
{ /* Ieee/string.scm 640 */
 unsigned char BgL_arg1376z00_1152;
{ /* Ieee/string.scm 640 */
 unsigned char BgL_tmpz00_6693;
{ /* Ieee/string.scm 343 */
 long BgL_l2277z00_4235;
BgL_l2277z00_4235 = 
STRING_LENGTH(BgL_stringz00_88); 
if(
BOUND_CHECK(BgL_iz00_1149, BgL_l2277z00_4235))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6693 = 
STRING_REF(BgL_stringz00_88, BgL_iz00_1149)
; }  else 
{ 
 obj_t BgL_auxz00_6698;
BgL_auxz00_6698 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_88, 
(int)(BgL_l2277z00_4235), 
(int)(BgL_iz00_1149)); 
FAILURE(BgL_auxz00_6698,BFALSE,BFALSE);} } 
BgL_arg1376z00_1152 = 
toupper(BgL_tmpz00_6693); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2281z00_4239;
BgL_l2281z00_4239 = 
STRING_LENGTH(BgL_resz00_1147); 
if(
BOUND_CHECK(BgL_iz00_1149, BgL_l2281z00_4239))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_resz00_1147, BgL_iz00_1149, BgL_arg1376z00_1152); }  else 
{ 
 obj_t BgL_auxz00_6709;
BgL_auxz00_6709 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_resz00_1147, 
(int)(BgL_l2281z00_4239), 
(int)(BgL_iz00_1149)); 
FAILURE(BgL_auxz00_6709,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_6715;
BgL_iz00_6715 = 
(BgL_iz00_1149+1L); 
BgL_iz00_1149 = BgL_iz00_6715; 
goto BgL_zc3z04anonymousza31374ze3z87_1150;} } } } } } } 

}



/* &string-upcase */
obj_t BGl_z62stringzd2upcasezb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4119, obj_t BgL_stringz00_4120)
{
{ /* Ieee/string.scm 633 */
{ /* Ieee/string.scm 634 */
 obj_t BgL_auxz00_6717;
if(
STRINGP(BgL_stringz00_4120))
{ /* Ieee/string.scm 634 */
BgL_auxz00_6717 = BgL_stringz00_4120
; }  else 
{ 
 obj_t BgL_auxz00_6720;
BgL_auxz00_6720 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(29133L), BGl_string3415z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4120); 
FAILURE(BgL_auxz00_6720,BFALSE,BFALSE);} 
return 
BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_auxz00_6717);} } 

}



/* string-downcase */
BGL_EXPORTED_DEF obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_89)
{
{ /* Ieee/string.scm 646 */
{ /* Ieee/string.scm 647 */
 long BgL_lenz00_1156;
BgL_lenz00_1156 = 
STRING_LENGTH(BgL_stringz00_89); 
{ /* Ieee/string.scm 647 */
 obj_t BgL_resz00_1157;
BgL_resz00_1157 = 
make_string_sans_fill(BgL_lenz00_1156); 
{ /* Ieee/string.scm 648 */

{ 
 long BgL_iz00_1159;
BgL_iz00_1159 = 0L; 
BgL_zc3z04anonymousza31379ze3z87_1160:
if(
(BgL_iz00_1159==BgL_lenz00_1156))
{ /* Ieee/string.scm 650 */
return BgL_resz00_1157;}  else 
{ /* Ieee/string.scm 650 */
{ /* Ieee/string.scm 653 */
 unsigned char BgL_arg1382z00_1162;
{ /* Ieee/string.scm 653 */
 unsigned char BgL_tmpz00_6729;
{ /* Ieee/string.scm 343 */
 long BgL_l2285z00_4243;
BgL_l2285z00_4243 = 
STRING_LENGTH(BgL_stringz00_89); 
if(
BOUND_CHECK(BgL_iz00_1159, BgL_l2285z00_4243))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6729 = 
STRING_REF(BgL_stringz00_89, BgL_iz00_1159)
; }  else 
{ 
 obj_t BgL_auxz00_6734;
BgL_auxz00_6734 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_89, 
(int)(BgL_l2285z00_4243), 
(int)(BgL_iz00_1159)); 
FAILURE(BgL_auxz00_6734,BFALSE,BFALSE);} } 
BgL_arg1382z00_1162 = 
tolower(BgL_tmpz00_6729); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2289z00_4247;
BgL_l2289z00_4247 = 
STRING_LENGTH(BgL_resz00_1157); 
if(
BOUND_CHECK(BgL_iz00_1159, BgL_l2289z00_4247))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_resz00_1157, BgL_iz00_1159, BgL_arg1382z00_1162); }  else 
{ 
 obj_t BgL_auxz00_6745;
BgL_auxz00_6745 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_resz00_1157, 
(int)(BgL_l2289z00_4247), 
(int)(BgL_iz00_1159)); 
FAILURE(BgL_auxz00_6745,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_6751;
BgL_iz00_6751 = 
(BgL_iz00_1159+1L); 
BgL_iz00_1159 = BgL_iz00_6751; 
goto BgL_zc3z04anonymousza31379ze3z87_1160;} } } } } } } 

}



/* &string-downcase */
obj_t BGl_z62stringzd2downcasezb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4121, obj_t BgL_stringz00_4122)
{
{ /* Ieee/string.scm 646 */
{ /* Ieee/string.scm 647 */
 obj_t BgL_auxz00_6753;
if(
STRINGP(BgL_stringz00_4122))
{ /* Ieee/string.scm 647 */
BgL_auxz00_6753 = BgL_stringz00_4122
; }  else 
{ 
 obj_t BgL_auxz00_6756;
BgL_auxz00_6756 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(29618L), BGl_string3416z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4122); 
FAILURE(BgL_auxz00_6756,BFALSE,BFALSE);} 
return 
BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_auxz00_6753);} } 

}



/* string-upcase! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2upcasez12zc0zz__r4_strings_6_7z00(obj_t BgL_stringz00_90)
{
{ /* Ieee/string.scm 659 */
{ /* Ieee/string.scm 660 */
 long BgL_lenz00_1166;
BgL_lenz00_1166 = 
STRING_LENGTH(BgL_stringz00_90); 
{ 
 long BgL_iz00_1168;
BgL_iz00_1168 = 0L; 
BgL_zc3z04anonymousza31385ze3z87_1169:
if(
(BgL_iz00_1168==BgL_lenz00_1166))
{ /* Ieee/string.scm 662 */
return BgL_stringz00_90;}  else 
{ /* Ieee/string.scm 662 */
{ /* Ieee/string.scm 665 */
 unsigned char BgL_arg1387z00_1171;
{ /* Ieee/string.scm 665 */
 unsigned char BgL_tmpz00_6764;
{ /* Ieee/string.scm 343 */
 long BgL_l2293z00_4251;
BgL_l2293z00_4251 = 
STRING_LENGTH(BgL_stringz00_90); 
if(
BOUND_CHECK(BgL_iz00_1168, BgL_l2293z00_4251))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6764 = 
STRING_REF(BgL_stringz00_90, BgL_iz00_1168)
; }  else 
{ 
 obj_t BgL_auxz00_6769;
BgL_auxz00_6769 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_90, 
(int)(BgL_l2293z00_4251), 
(int)(BgL_iz00_1168)); 
FAILURE(BgL_auxz00_6769,BFALSE,BFALSE);} } 
BgL_arg1387z00_1171 = 
toupper(BgL_tmpz00_6764); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2297z00_4255;
BgL_l2297z00_4255 = 
STRING_LENGTH(BgL_stringz00_90); 
if(
BOUND_CHECK(BgL_iz00_1168, BgL_l2297z00_4255))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_stringz00_90, BgL_iz00_1168, BgL_arg1387z00_1171); }  else 
{ 
 obj_t BgL_auxz00_6780;
BgL_auxz00_6780 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_90, 
(int)(BgL_l2297z00_4255), 
(int)(BgL_iz00_1168)); 
FAILURE(BgL_auxz00_6780,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_6786;
BgL_iz00_6786 = 
(BgL_iz00_1168+1L); 
BgL_iz00_1168 = BgL_iz00_6786; 
goto BgL_zc3z04anonymousza31385ze3z87_1169;} } } } } 

}



/* &string-upcase! */
obj_t BGl_z62stringzd2upcasez12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4123, obj_t BgL_stringz00_4124)
{
{ /* Ieee/string.scm 659 */
{ /* Ieee/string.scm 660 */
 obj_t BgL_auxz00_6788;
if(
STRINGP(BgL_stringz00_4124))
{ /* Ieee/string.scm 660 */
BgL_auxz00_6788 = BgL_stringz00_4124
; }  else 
{ 
 obj_t BgL_auxz00_6791;
BgL_auxz00_6791 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(30104L), BGl_string3417z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4124); 
FAILURE(BgL_auxz00_6791,BFALSE,BFALSE);} 
return 
BGl_stringzd2upcasez12zc0zz__r4_strings_6_7z00(BgL_auxz00_6788);} } 

}



/* string-downcase! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2downcasez12zc0zz__r4_strings_6_7z00(obj_t BgL_stringz00_91)
{
{ /* Ieee/string.scm 671 */
{ /* Ieee/string.scm 672 */
 long BgL_lenz00_1175;
BgL_lenz00_1175 = 
STRING_LENGTH(BgL_stringz00_91); 
{ 
 long BgL_iz00_1177;
BgL_iz00_1177 = 0L; 
BgL_zc3z04anonymousza31390ze3z87_1178:
if(
(BgL_iz00_1177==BgL_lenz00_1175))
{ /* Ieee/string.scm 674 */
return BgL_stringz00_91;}  else 
{ /* Ieee/string.scm 674 */
{ /* Ieee/string.scm 677 */
 unsigned char BgL_arg1392z00_1180;
{ /* Ieee/string.scm 677 */
 unsigned char BgL_tmpz00_6799;
{ /* Ieee/string.scm 343 */
 long BgL_l2301z00_4259;
BgL_l2301z00_4259 = 
STRING_LENGTH(BgL_stringz00_91); 
if(
BOUND_CHECK(BgL_iz00_1177, BgL_l2301z00_4259))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_6799 = 
STRING_REF(BgL_stringz00_91, BgL_iz00_1177)
; }  else 
{ 
 obj_t BgL_auxz00_6804;
BgL_auxz00_6804 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_91, 
(int)(BgL_l2301z00_4259), 
(int)(BgL_iz00_1177)); 
FAILURE(BgL_auxz00_6804,BFALSE,BFALSE);} } 
BgL_arg1392z00_1180 = 
tolower(BgL_tmpz00_6799); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2305z00_4263;
BgL_l2305z00_4263 = 
STRING_LENGTH(BgL_stringz00_91); 
if(
BOUND_CHECK(BgL_iz00_1177, BgL_l2305z00_4263))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_stringz00_91, BgL_iz00_1177, BgL_arg1392z00_1180); }  else 
{ 
 obj_t BgL_auxz00_6815;
BgL_auxz00_6815 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_91, 
(int)(BgL_l2305z00_4263), 
(int)(BgL_iz00_1177)); 
FAILURE(BgL_auxz00_6815,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_6821;
BgL_iz00_6821 = 
(BgL_iz00_1177+1L); 
BgL_iz00_1177 = BgL_iz00_6821; 
goto BgL_zc3z04anonymousza31390ze3z87_1178;} } } } } 

}



/* &string-downcase! */
obj_t BGl_z62stringzd2downcasez12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4125, obj_t BgL_stringz00_4126)
{
{ /* Ieee/string.scm 671 */
{ /* Ieee/string.scm 672 */
 obj_t BgL_auxz00_6823;
if(
STRINGP(BgL_stringz00_4126))
{ /* Ieee/string.scm 672 */
BgL_auxz00_6823 = BgL_stringz00_4126
; }  else 
{ 
 obj_t BgL_auxz00_6826;
BgL_auxz00_6826 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(30559L), BGl_string3418z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4126); 
FAILURE(BgL_auxz00_6826,BFALSE,BFALSE);} 
return 
BGl_stringzd2downcasez12zc0zz__r4_strings_6_7z00(BgL_auxz00_6823);} } 

}



/* string-capitalize! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(obj_t BgL_stringz00_92)
{
{ /* Ieee/string.scm 687 */
{ /* Ieee/string.scm 688 */
 bool_t BgL_nonzd2firstzd2alphaz00_1184; long BgL_stringzd2lenzd2_1185;
BgL_nonzd2firstzd2alphaz00_1184 = ((bool_t)0); 
BgL_stringzd2lenzd2_1185 = 
STRING_LENGTH(BgL_stringz00_92); 
{ 
 long BgL_iz00_1187;
BgL_iz00_1187 = 0L; 
BgL_zc3z04anonymousza31395ze3z87_1188:
if(
(BgL_iz00_1187==BgL_stringzd2lenzd2_1185))
{ /* Ieee/string.scm 690 */
return BgL_stringz00_92;}  else 
{ /* Ieee/string.scm 690 */
{ /* Ieee/string.scm 692 */
 unsigned char BgL_cz00_1190;
{ /* Ieee/string.scm 343 */
 long BgL_l2309z00_4267;
BgL_l2309z00_4267 = 
STRING_LENGTH(BgL_stringz00_92); 
if(
BOUND_CHECK(BgL_iz00_1187, BgL_l2309z00_4267))
{ /* Ieee/string.scm 343 */
BgL_cz00_1190 = 
STRING_REF(BgL_stringz00_92, BgL_iz00_1187); }  else 
{ 
 obj_t BgL_auxz00_6838;
BgL_auxz00_6838 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_92, 
(int)(BgL_l2309z00_4267), 
(int)(BgL_iz00_1187)); 
FAILURE(BgL_auxz00_6838,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 693 */
 bool_t BgL_test3954z00_6844;
if(
isalpha(BgL_cz00_1190))
{ /* Ieee/string.scm 693 */
BgL_test3954z00_6844 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 693 */
BgL_test3954z00_6844 = 
(
(BgL_cz00_1190)>127L)
; } 
if(BgL_test3954z00_6844)
{ /* Ieee/string.scm 693 */
if(BgL_nonzd2firstzd2alphaz00_1184)
{ /* Ieee/string.scm 695 */
 unsigned char BgL_arg1401z00_1194;
BgL_arg1401z00_1194 = 
tolower(BgL_cz00_1190); 
{ /* Ieee/string.scm 349 */
 long BgL_l2313z00_4271;
BgL_l2313z00_4271 = 
STRING_LENGTH(BgL_stringz00_92); 
if(
BOUND_CHECK(BgL_iz00_1187, BgL_l2313z00_4271))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_stringz00_92, BgL_iz00_1187, BgL_arg1401z00_1194); }  else 
{ 
 obj_t BgL_auxz00_6855;
BgL_auxz00_6855 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_92, 
(int)(BgL_l2313z00_4271), 
(int)(BgL_iz00_1187)); 
FAILURE(BgL_auxz00_6855,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 694 */
BgL_nonzd2firstzd2alphaz00_1184 = ((bool_t)1); 
{ /* Ieee/string.scm 698 */
 unsigned char BgL_arg1402z00_1195;
BgL_arg1402z00_1195 = 
toupper(BgL_cz00_1190); 
{ /* Ieee/string.scm 349 */
 long BgL_l2317z00_4275;
BgL_l2317z00_4275 = 
STRING_LENGTH(BgL_stringz00_92); 
if(
BOUND_CHECK(BgL_iz00_1187, BgL_l2317z00_4275))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_stringz00_92, BgL_iz00_1187, BgL_arg1402z00_1195); }  else 
{ 
 obj_t BgL_auxz00_6866;
BgL_auxz00_6866 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_stringz00_92, 
(int)(BgL_l2317z00_4275), 
(int)(BgL_iz00_1187)); 
FAILURE(BgL_auxz00_6866,BFALSE,BFALSE);} } } } }  else 
{ /* Ieee/string.scm 693 */
BgL_nonzd2firstzd2alphaz00_1184 = ((bool_t)0); } } } 
{ 
 long BgL_iz00_6872;
BgL_iz00_6872 = 
(BgL_iz00_1187+1L); 
BgL_iz00_1187 = BgL_iz00_6872; 
goto BgL_zc3z04anonymousza31395ze3z87_1188;} } } } } 

}



/* &string-capitalize! */
obj_t BGl_z62stringzd2capitaliza7ez12z05zz__r4_strings_6_7z00(obj_t BgL_envz00_4127, obj_t BgL_stringz00_4128)
{
{ /* Ieee/string.scm 687 */
{ /* Ieee/string.scm 688 */
 obj_t BgL_auxz00_6874;
if(
STRINGP(BgL_stringz00_4128))
{ /* Ieee/string.scm 688 */
BgL_auxz00_6874 = BgL_stringz00_4128
; }  else 
{ 
 obj_t BgL_auxz00_6877;
BgL_auxz00_6877 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(31314L), BGl_string3419z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4128); 
FAILURE(BgL_auxz00_6877,BFALSE,BFALSE);} 
return 
BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(BgL_auxz00_6874);} } 

}



/* string-capitalize */
BGL_EXPORTED_DEF obj_t BGl_stringzd2capitaliza7ez75zz__r4_strings_6_7z00(obj_t BgL_stringz00_93)
{
{ /* Ieee/string.scm 704 */
BGL_TAIL return 
BGl_stringzd2capitaliza7ez12z67zz__r4_strings_6_7z00(
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_stringz00_93));} 

}



/* &string-capitalize */
obj_t BGl_z62stringzd2capitaliza7ez17zz__r4_strings_6_7z00(obj_t BgL_envz00_4129, obj_t BgL_stringz00_4130)
{
{ /* Ieee/string.scm 704 */
{ /* Ieee/string.scm 705 */
 obj_t BgL_auxz00_6884;
if(
STRINGP(BgL_stringz00_4130))
{ /* Ieee/string.scm 705 */
BgL_auxz00_6884 = BgL_stringz00_4130
; }  else 
{ 
 obj_t BgL_auxz00_6887;
BgL_auxz00_6887 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(32038L), BGl_string3420z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4130); 
FAILURE(BgL_auxz00_6887,BFALSE,BFALSE);} 
return 
BGl_stringzd2capitaliza7ez75zz__r4_strings_6_7z00(BgL_auxz00_6884);} } 

}



/* string-for-read */
BGL_EXPORTED_DEF obj_t BGl_stringzd2forzd2readz00zz__r4_strings_6_7z00(obj_t BgL_stringz00_94)
{
{ /* Ieee/string.scm 710 */
BGL_TAIL return 
string_for_read(BgL_stringz00_94);} 

}



/* &string-for-read */
obj_t BGl_z62stringzd2forzd2readz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4131, obj_t BgL_stringz00_4132)
{
{ /* Ieee/string.scm 710 */
{ /* Ieee/string.scm 711 */
 obj_t BgL_auxz00_6893;
if(
STRINGP(BgL_stringz00_4132))
{ /* Ieee/string.scm 711 */
BgL_auxz00_6893 = BgL_stringz00_4132
; }  else 
{ 
 obj_t BgL_auxz00_6896;
BgL_auxz00_6896 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(32327L), BGl_string3421z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4132); 
FAILURE(BgL_auxz00_6896,BFALSE,BFALSE);} 
return 
BGl_stringzd2forzd2readz00zz__r4_strings_6_7z00(BgL_auxz00_6893);} } 

}



/* escape-C-string */
BGL_EXPORTED_DEF obj_t BGl_escapezd2Czd2stringz00zz__r4_strings_6_7z00(obj_t BgL_strz00_95)
{
{ /* Ieee/string.scm 718 */
{ /* Ieee/string.scm 719 */
 obj_t BgL_arg1406z00_5389;
{ /* Ieee/string.scm 194 */
 long BgL_endz00_5390;
BgL_endz00_5390 = 
STRING_LENGTH(BgL_strz00_95); 
{ /* Ieee/string.scm 194 */

BgL_arg1406z00_5389 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_95, 1L, BgL_endz00_5390); } } 
return 
bgl_escape_C_string(
BSTRING_TO_STRING(BgL_arg1406z00_5389), 0L, 
STRING_LENGTH(BgL_arg1406z00_5389));} } 

}



/* &escape-C-string */
obj_t BGl_z62escapezd2Czd2stringz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4133, obj_t BgL_strz00_4134)
{
{ /* Ieee/string.scm 718 */
{ /* Ieee/string.scm 719 */
 obj_t BgL_auxz00_6906;
if(
STRINGP(BgL_strz00_4134))
{ /* Ieee/string.scm 719 */
BgL_auxz00_6906 = BgL_strz00_4134
; }  else 
{ 
 obj_t BgL_auxz00_6909;
BgL_auxz00_6909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(32781L), BGl_string3422z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4134); 
FAILURE(BgL_auxz00_6909,BFALSE,BFALSE);} 
return 
BGl_escapezd2Czd2stringz00zz__r4_strings_6_7z00(BgL_auxz00_6906);} } 

}



/* escape-scheme-string */
BGL_EXPORTED_DEF obj_t BGl_escapezd2schemezd2stringz00zz__r4_strings_6_7z00(obj_t BgL_strz00_96)
{
{ /* Ieee/string.scm 726 */
return 
bgl_escape_scheme_string(
BSTRING_TO_STRING(BgL_strz00_96), 0L, 
STRING_LENGTH(BgL_strz00_96));} 

}



/* &escape-scheme-string */
obj_t BGl_z62escapezd2schemezd2stringz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4135, obj_t BgL_strz00_4136)
{
{ /* Ieee/string.scm 726 */
{ /* Ieee/string.scm 727 */
 obj_t BgL_auxz00_6917;
if(
STRINGP(BgL_strz00_4136))
{ /* Ieee/string.scm 727 */
BgL_auxz00_6917 = BgL_strz00_4136
; }  else 
{ 
 obj_t BgL_auxz00_6920;
BgL_auxz00_6920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33246L), BGl_string3423z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4136); 
FAILURE(BgL_auxz00_6920,BFALSE,BFALSE);} 
return 
BGl_escapezd2schemezd2stringz00zz__r4_strings_6_7z00(BgL_auxz00_6917);} } 

}



/* string-as-read */
BGL_EXPORTED_DEF obj_t BGl_stringzd2aszd2readz00zz__r4_strings_6_7z00(obj_t BgL_strz00_97)
{
{ /* Ieee/string.scm 732 */
return 
bgl_escape_C_string(
BSTRING_TO_STRING(BgL_strz00_97), 0L, 
STRING_LENGTH(BgL_strz00_97));} 

}



/* &string-as-read */
obj_t BGl_z62stringzd2aszd2readz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4137, obj_t BgL_strz00_4138)
{
{ /* Ieee/string.scm 732 */
{ /* Ieee/string.scm 733 */
 obj_t BgL_auxz00_6928;
if(
STRINGP(BgL_strz00_4138))
{ /* Ieee/string.scm 733 */
BgL_auxz00_6928 = BgL_strz00_4138
; }  else 
{ 
 obj_t BgL_auxz00_6931;
BgL_auxz00_6931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33554L), BGl_string3424z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4138); 
FAILURE(BgL_auxz00_6931,BFALSE,BFALSE);} 
return 
BGl_stringzd2aszd2readz00zz__r4_strings_6_7z00(BgL_auxz00_6928);} } 

}



/* blit-string-ur! */
BGL_EXPORTED_DEF obj_t BGl_blitzd2stringzd2urz12z12zz__r4_strings_6_7z00(obj_t BgL_s1z00_98, long BgL_o1z00_99, obj_t BgL_s2z00_100, long BgL_o2z00_101, long BgL_lz00_102)
{
{ /* Ieee/string.scm 738 */
BGL_TAIL return 
blit_string(BgL_s1z00_98, BgL_o1z00_99, BgL_s2z00_100, BgL_o2z00_101, BgL_lz00_102);} 

}



/* &blit-string-ur! */
obj_t BGl_z62blitzd2stringzd2urz12z70zz__r4_strings_6_7z00(obj_t BgL_envz00_4139, obj_t BgL_s1z00_4140, obj_t BgL_o1z00_4141, obj_t BgL_s2z00_4142, obj_t BgL_o2z00_4143, obj_t BgL_lz00_4144)
{
{ /* Ieee/string.scm 738 */
{ /* Ieee/string.scm 739 */
 long BgL_auxz00_6969; long BgL_auxz00_6960; obj_t BgL_auxz00_6953; long BgL_auxz00_6944; obj_t BgL_auxz00_6937;
{ /* Ieee/string.scm 739 */
 obj_t BgL_tmpz00_6970;
if(
INTEGERP(BgL_lz00_4144))
{ /* Ieee/string.scm 739 */
BgL_tmpz00_6970 = BgL_lz00_4144
; }  else 
{ 
 obj_t BgL_auxz00_6973;
BgL_auxz00_6973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33849L), BGl_string3425z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lz00_4144); 
FAILURE(BgL_auxz00_6973,BFALSE,BFALSE);} 
BgL_auxz00_6969 = 
(long)CINT(BgL_tmpz00_6970); } 
{ /* Ieee/string.scm 739 */
 obj_t BgL_tmpz00_6961;
if(
INTEGERP(BgL_o2z00_4143))
{ /* Ieee/string.scm 739 */
BgL_tmpz00_6961 = BgL_o2z00_4143
; }  else 
{ 
 obj_t BgL_auxz00_6964;
BgL_auxz00_6964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33849L), BGl_string3425z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_o2z00_4143); 
FAILURE(BgL_auxz00_6964,BFALSE,BFALSE);} 
BgL_auxz00_6960 = 
(long)CINT(BgL_tmpz00_6961); } 
if(
STRINGP(BgL_s2z00_4142))
{ /* Ieee/string.scm 739 */
BgL_auxz00_6953 = BgL_s2z00_4142
; }  else 
{ 
 obj_t BgL_auxz00_6956;
BgL_auxz00_6956 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33849L), BGl_string3425z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_4142); 
FAILURE(BgL_auxz00_6956,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 739 */
 obj_t BgL_tmpz00_6945;
if(
INTEGERP(BgL_o1z00_4141))
{ /* Ieee/string.scm 739 */
BgL_tmpz00_6945 = BgL_o1z00_4141
; }  else 
{ 
 obj_t BgL_auxz00_6948;
BgL_auxz00_6948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33849L), BGl_string3425z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_o1z00_4141); 
FAILURE(BgL_auxz00_6948,BFALSE,BFALSE);} 
BgL_auxz00_6944 = 
(long)CINT(BgL_tmpz00_6945); } 
if(
STRINGP(BgL_s1z00_4140))
{ /* Ieee/string.scm 739 */
BgL_auxz00_6937 = BgL_s1z00_4140
; }  else 
{ 
 obj_t BgL_auxz00_6940;
BgL_auxz00_6940 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(33849L), BGl_string3425z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_4140); 
FAILURE(BgL_auxz00_6940,BFALSE,BFALSE);} 
return 
BGl_blitzd2stringzd2urz12z12zz__r4_strings_6_7z00(BgL_auxz00_6937, BgL_auxz00_6944, BgL_auxz00_6953, BgL_auxz00_6960, BgL_auxz00_6969);} } 

}



/* blit-string! */
BGL_EXPORTED_DEF obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t BgL_s1z00_103, long BgL_o1z00_104, obj_t BgL_s2z00_105, long BgL_o2z00_106, long BgL_lz00_107)
{
{ /* Ieee/string.scm 744 */
{ /* Ieee/string.scm 745 */
 bool_t BgL_test3970z00_6979;
{ /* Ieee/string.scm 745 */
 bool_t BgL_test3971z00_6980;
{ /* Ieee/string.scm 745 */
 long BgL_auxz00_6983; long BgL_tmpz00_6981;
BgL_auxz00_6983 = 
(
STRING_LENGTH(BgL_s1z00_103)+1L); 
BgL_tmpz00_6981 = 
(BgL_lz00_107+BgL_o1z00_104); 
BgL_test3971z00_6980 = 
BOUND_CHECK(BgL_tmpz00_6981, BgL_auxz00_6983); } 
if(BgL_test3971z00_6980)
{ /* Ieee/string.scm 746 */
 long BgL_auxz00_6989; long BgL_tmpz00_6987;
BgL_auxz00_6989 = 
(
STRING_LENGTH(BgL_s2z00_105)+1L); 
BgL_tmpz00_6987 = 
(BgL_lz00_107+BgL_o2z00_106); 
BgL_test3970z00_6979 = 
BOUND_CHECK(BgL_tmpz00_6987, BgL_auxz00_6989); }  else 
{ /* Ieee/string.scm 745 */
BgL_test3970z00_6979 = ((bool_t)0)
; } } 
if(BgL_test3970z00_6979)
{ /* Ieee/string.scm 745 */
BGL_TAIL return 
blit_string(BgL_s1z00_103, BgL_o1z00_104, BgL_s2z00_105, BgL_o2z00_106, BgL_lz00_107);}  else 
{ /* Ieee/string.scm 749 */
 obj_t BgL_arg1421z00_1218; obj_t BgL_arg1422z00_1219;
{ /* Ieee/string.scm 749 */
 obj_t BgL_list1423z00_1220;
{ /* Ieee/string.scm 749 */
 obj_t BgL_arg1424z00_1221;
{ /* Ieee/string.scm 749 */
 obj_t BgL_arg1425z00_1222;
{ /* Ieee/string.scm 749 */
 obj_t BgL_arg1426z00_1223;
{ /* Ieee/string.scm 749 */
 obj_t BgL_arg1427z00_1224;
BgL_arg1427z00_1224 = 
MAKE_YOUNG_PAIR(BGl_string3426z00zz__r4_strings_6_7z00, BNIL); 
BgL_arg1426z00_1223 = 
MAKE_YOUNG_PAIR(BgL_s2z00_105, BgL_arg1427z00_1224); } 
BgL_arg1425z00_1222 = 
MAKE_YOUNG_PAIR(BGl_string3427z00zz__r4_strings_6_7z00, BgL_arg1426z00_1223); } 
BgL_arg1424z00_1221 = 
MAKE_YOUNG_PAIR(BgL_s1z00_103, BgL_arg1425z00_1222); } 
BgL_list1423z00_1220 = 
MAKE_YOUNG_PAIR(BGl_string3428z00zz__r4_strings_6_7z00, BgL_arg1424z00_1221); } 
BgL_arg1421z00_1218 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1423z00_1220); } 
{ /* Ieee/string.scm 750 */
 long BgL_arg1428z00_1225; long BgL_arg1429z00_1226;
BgL_arg1428z00_1225 = 
STRING_LENGTH(BgL_s1z00_103); 
BgL_arg1429z00_1226 = 
STRING_LENGTH(BgL_s2z00_105); 
{ /* Ieee/string.scm 750 */
 obj_t BgL_list1430z00_1227;
{ /* Ieee/string.scm 750 */
 obj_t BgL_arg1431z00_1228;
{ /* Ieee/string.scm 750 */
 obj_t BgL_arg1434z00_1229;
{ /* Ieee/string.scm 750 */
 obj_t BgL_arg1435z00_1230;
{ /* Ieee/string.scm 750 */
 obj_t BgL_arg1436z00_1231;
BgL_arg1436z00_1231 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lz00_107), BNIL); 
BgL_arg1435z00_1230 = 
MAKE_YOUNG_PAIR(
BINT(BgL_o2z00_106), BgL_arg1436z00_1231); } 
BgL_arg1434z00_1229 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1429z00_1226), BgL_arg1435z00_1230); } 
BgL_arg1431z00_1228 = 
MAKE_YOUNG_PAIR(
BINT(BgL_o1z00_104), BgL_arg1434z00_1229); } 
BgL_list1430z00_1227 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1428z00_1225), BgL_arg1431z00_1228); } 
BgL_arg1422z00_1219 = BgL_list1430z00_1227; } } 
return 
BGl_errorz00zz__errorz00(BGl_string3429z00zz__r4_strings_6_7z00, BgL_arg1421z00_1218, BgL_arg1422z00_1219);} } } 

}



/* &blit-string! */
obj_t BGl_z62blitzd2stringz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4145, obj_t BgL_s1z00_4146, obj_t BgL_o1z00_4147, obj_t BgL_s2z00_4148, obj_t BgL_o2z00_4149, obj_t BgL_lz00_4150)
{
{ /* Ieee/string.scm 744 */
{ /* Ieee/string.scm 745 */
 long BgL_auxz00_7045; long BgL_auxz00_7036; obj_t BgL_auxz00_7029; long BgL_auxz00_7020; obj_t BgL_auxz00_7013;
{ /* Ieee/string.scm 745 */
 obj_t BgL_tmpz00_7046;
if(
INTEGERP(BgL_lz00_4150))
{ /* Ieee/string.scm 745 */
BgL_tmpz00_7046 = BgL_lz00_4150
; }  else 
{ 
 obj_t BgL_auxz00_7049;
BgL_auxz00_7049 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34142L), BGl_string3430z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lz00_4150); 
FAILURE(BgL_auxz00_7049,BFALSE,BFALSE);} 
BgL_auxz00_7045 = 
(long)CINT(BgL_tmpz00_7046); } 
{ /* Ieee/string.scm 745 */
 obj_t BgL_tmpz00_7037;
if(
INTEGERP(BgL_o2z00_4149))
{ /* Ieee/string.scm 745 */
BgL_tmpz00_7037 = BgL_o2z00_4149
; }  else 
{ 
 obj_t BgL_auxz00_7040;
BgL_auxz00_7040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34142L), BGl_string3430z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_o2z00_4149); 
FAILURE(BgL_auxz00_7040,BFALSE,BFALSE);} 
BgL_auxz00_7036 = 
(long)CINT(BgL_tmpz00_7037); } 
if(
STRINGP(BgL_s2z00_4148))
{ /* Ieee/string.scm 745 */
BgL_auxz00_7029 = BgL_s2z00_4148
; }  else 
{ 
 obj_t BgL_auxz00_7032;
BgL_auxz00_7032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34142L), BGl_string3430z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_4148); 
FAILURE(BgL_auxz00_7032,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 745 */
 obj_t BgL_tmpz00_7021;
if(
INTEGERP(BgL_o1z00_4147))
{ /* Ieee/string.scm 745 */
BgL_tmpz00_7021 = BgL_o1z00_4147
; }  else 
{ 
 obj_t BgL_auxz00_7024;
BgL_auxz00_7024 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34142L), BGl_string3430z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_o1z00_4147); 
FAILURE(BgL_auxz00_7024,BFALSE,BFALSE);} 
BgL_auxz00_7020 = 
(long)CINT(BgL_tmpz00_7021); } 
if(
STRINGP(BgL_s1z00_4146))
{ /* Ieee/string.scm 745 */
BgL_auxz00_7013 = BgL_s1z00_4146
; }  else 
{ 
 obj_t BgL_auxz00_7016;
BgL_auxz00_7016 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34142L), BGl_string3430z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_4146); 
FAILURE(BgL_auxz00_7016,BFALSE,BFALSE);} 
return 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_auxz00_7013, BgL_auxz00_7020, BgL_auxz00_7029, BgL_auxz00_7036, BgL_auxz00_7045);} } 

}



/* string-shrink! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2shrinkz12zc0zz__r4_strings_6_7z00(obj_t BgL_sz00_108, long BgL_lz00_109)
{
{ /* Ieee/string.scm 755 */
return 
bgl_string_shrink(BgL_sz00_108, BgL_lz00_109);} 

}



/* &string-shrink! */
obj_t BGl_z62stringzd2shrinkz12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4151, obj_t BgL_sz00_4152, obj_t BgL_lz00_4153)
{
{ /* Ieee/string.scm 755 */
{ /* Ieee/string.scm 756 */
 long BgL_auxz00_7063; obj_t BgL_auxz00_7056;
{ /* Ieee/string.scm 756 */
 obj_t BgL_tmpz00_7064;
if(
INTEGERP(BgL_lz00_4153))
{ /* Ieee/string.scm 756 */
BgL_tmpz00_7064 = BgL_lz00_4153
; }  else 
{ 
 obj_t BgL_auxz00_7067;
BgL_auxz00_7067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34751L), BGl_string3431z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_lz00_4153); 
FAILURE(BgL_auxz00_7067,BFALSE,BFALSE);} 
BgL_auxz00_7063 = 
(long)CINT(BgL_tmpz00_7064); } 
if(
STRINGP(BgL_sz00_4152))
{ /* Ieee/string.scm 756 */
BgL_auxz00_7056 = BgL_sz00_4152
; }  else 
{ 
 obj_t BgL_auxz00_7059;
BgL_auxz00_7059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(34751L), BGl_string3431z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_4152); 
FAILURE(BgL_auxz00_7059,BFALSE,BFALSE);} 
return 
BGl_stringzd2shrinkz12zc0zz__r4_strings_6_7z00(BgL_auxz00_7056, BgL_auxz00_7063);} } 

}



/* string-replace */
BGL_EXPORTED_DEF obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t BgL_strz00_110, unsigned char BgL_c1z00_111, unsigned char BgL_c2z00_112)
{
{ /* Ieee/string.scm 761 */
{ /* Ieee/string.scm 762 */
 long BgL_lenz00_1239;
BgL_lenz00_1239 = 
STRING_LENGTH(BgL_strz00_110); 
{ /* Ieee/string.scm 762 */
 obj_t BgL_newz00_1240;
{ /* Ieee/string.scm 172 */

BgL_newz00_1240 = 
make_string(BgL_lenz00_1239, ((unsigned char)' ')); } 
{ /* Ieee/string.scm 763 */

{ 
 long BgL_iz00_1242;
BgL_iz00_1242 = 0L; 
BgL_zc3z04anonymousza31443ze3z87_1243:
if(
(BgL_iz00_1242==BgL_lenz00_1239))
{ /* Ieee/string.scm 765 */
return BgL_newz00_1240;}  else 
{ /* Ieee/string.scm 767 */
 unsigned char BgL_cz00_1245;
{ /* Ieee/string.scm 343 */
 long BgL_l2321z00_4279;
BgL_l2321z00_4279 = 
STRING_LENGTH(BgL_strz00_110); 
if(
BOUND_CHECK(BgL_iz00_1242, BgL_l2321z00_4279))
{ /* Ieee/string.scm 343 */
BgL_cz00_1245 = 
STRING_REF(BgL_strz00_110, BgL_iz00_1242); }  else 
{ 
 obj_t BgL_auxz00_7081;
BgL_auxz00_7081 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_strz00_110, 
(int)(BgL_l2321z00_4279), 
(int)(BgL_iz00_1242)); 
FAILURE(BgL_auxz00_7081,BFALSE,BFALSE);} } 
if(
(BgL_cz00_1245==
(unsigned char)(BgL_c1z00_111)))
{ /* Ieee/string.scm 769 */
 unsigned char BgL_charz00_2927;
BgL_charz00_2927 = 
(unsigned char)(BgL_c2z00_112); 
{ /* Ieee/string.scm 349 */
 long BgL_l2325z00_4283;
BgL_l2325z00_4283 = 
STRING_LENGTH(BgL_newz00_1240); 
if(
BOUND_CHECK(BgL_iz00_1242, BgL_l2325z00_4283))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_newz00_1240, BgL_iz00_1242, BgL_charz00_2927); }  else 
{ 
 obj_t BgL_auxz00_7095;
BgL_auxz00_7095 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_newz00_1240, 
(int)(BgL_l2325z00_4283), 
(int)(BgL_iz00_1242)); 
FAILURE(BgL_auxz00_7095,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 349 */
 long BgL_l2329z00_4287;
BgL_l2329z00_4287 = 
STRING_LENGTH(BgL_newz00_1240); 
if(
BOUND_CHECK(BgL_iz00_1242, BgL_l2329z00_4287))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_newz00_1240, BgL_iz00_1242, BgL_cz00_1245); }  else 
{ 
 obj_t BgL_auxz00_7105;
BgL_auxz00_7105 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_newz00_1240, 
(int)(BgL_l2329z00_4287), 
(int)(BgL_iz00_1242)); 
FAILURE(BgL_auxz00_7105,BFALSE,BFALSE);} } 
{ 
 long BgL_iz00_7111;
BgL_iz00_7111 = 
(BgL_iz00_1242+1L); 
BgL_iz00_1242 = BgL_iz00_7111; 
goto BgL_zc3z04anonymousza31443ze3z87_1243;} } } } } } } 

}



/* &string-replace */
obj_t BGl_z62stringzd2replacezb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4154, obj_t BgL_strz00_4155, obj_t BgL_c1z00_4156, obj_t BgL_c2z00_4157)
{
{ /* Ieee/string.scm 761 */
{ /* Ieee/string.scm 762 */
 unsigned char BgL_auxz00_7129; unsigned char BgL_auxz00_7120; obj_t BgL_auxz00_7113;
{ /* Ieee/string.scm 762 */
 obj_t BgL_tmpz00_7130;
if(
CHARP(BgL_c2z00_4157))
{ /* Ieee/string.scm 762 */
BgL_tmpz00_7130 = BgL_c2z00_4157
; }  else 
{ 
 obj_t BgL_auxz00_7133;
BgL_auxz00_7133 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(35035L), BGl_string3432z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_c2z00_4157); 
FAILURE(BgL_auxz00_7133,BFALSE,BFALSE);} 
BgL_auxz00_7129 = 
CCHAR(BgL_tmpz00_7130); } 
{ /* Ieee/string.scm 762 */
 obj_t BgL_tmpz00_7121;
if(
CHARP(BgL_c1z00_4156))
{ /* Ieee/string.scm 762 */
BgL_tmpz00_7121 = BgL_c1z00_4156
; }  else 
{ 
 obj_t BgL_auxz00_7124;
BgL_auxz00_7124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(35035L), BGl_string3432z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_c1z00_4156); 
FAILURE(BgL_auxz00_7124,BFALSE,BFALSE);} 
BgL_auxz00_7120 = 
CCHAR(BgL_tmpz00_7121); } 
if(
STRINGP(BgL_strz00_4155))
{ /* Ieee/string.scm 762 */
BgL_auxz00_7113 = BgL_strz00_4155
; }  else 
{ 
 obj_t BgL_auxz00_7116;
BgL_auxz00_7116 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(35035L), BGl_string3432z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4155); 
FAILURE(BgL_auxz00_7116,BFALSE,BFALSE);} 
return 
BGl_stringzd2replacezd2zz__r4_strings_6_7z00(BgL_auxz00_7113, BgL_auxz00_7120, BgL_auxz00_7129);} } 

}



/* string-replace! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(obj_t BgL_strz00_113, unsigned char BgL_c1z00_114, unsigned char BgL_c2z00_115)
{
{ /* Ieee/string.scm 776 */
{ /* Ieee/string.scm 777 */
 long BgL_lenz00_1251;
BgL_lenz00_1251 = 
STRING_LENGTH(BgL_strz00_113); 
{ 
 long BgL_iz00_1253;
BgL_iz00_1253 = 0L; 
BgL_zc3z04anonymousza31447ze3z87_1254:
if(
(BgL_iz00_1253==BgL_lenz00_1251))
{ /* Ieee/string.scm 780 */
return BgL_strz00_113;}  else 
{ /* Ieee/string.scm 782 */
 bool_t BgL_test3988z00_7142;
{ /* Ieee/string.scm 782 */
 unsigned char BgL_char2z00_2938;
BgL_char2z00_2938 = 
(unsigned char)(BgL_c1z00_114); 
{ /* Ieee/string.scm 782 */
 unsigned char BgL_tmpz00_7144;
{ /* Ieee/string.scm 343 */
 long BgL_l2333z00_4291;
BgL_l2333z00_4291 = 
STRING_LENGTH(BgL_strz00_113); 
if(
BOUND_CHECK(BgL_iz00_1253, BgL_l2333z00_4291))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_7144 = 
STRING_REF(BgL_strz00_113, BgL_iz00_1253)
; }  else 
{ 
 obj_t BgL_auxz00_7149;
BgL_auxz00_7149 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_strz00_113, 
(int)(BgL_l2333z00_4291), 
(int)(BgL_iz00_1253)); 
FAILURE(BgL_auxz00_7149,BFALSE,BFALSE);} } 
BgL_test3988z00_7142 = 
(BgL_tmpz00_7144==BgL_char2z00_2938); } } 
if(BgL_test3988z00_7142)
{ /* Ieee/string.scm 782 */
{ /* Ieee/string.scm 783 */
 unsigned char BgL_charz00_2941;
BgL_charz00_2941 = 
(unsigned char)(BgL_c2z00_115); 
{ /* Ieee/string.scm 349 */
 long BgL_l2337z00_4295;
BgL_l2337z00_4295 = 
STRING_LENGTH(BgL_strz00_113); 
if(
BOUND_CHECK(BgL_iz00_1253, BgL_l2337z00_4295))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_strz00_113, BgL_iz00_1253, BgL_charz00_2941); }  else 
{ 
 obj_t BgL_auxz00_7161;
BgL_auxz00_7161 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_strz00_113, 
(int)(BgL_l2337z00_4295), 
(int)(BgL_iz00_1253)); 
FAILURE(BgL_auxz00_7161,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_7167;
BgL_iz00_7167 = 
(BgL_iz00_1253+1L); 
BgL_iz00_1253 = BgL_iz00_7167; 
goto BgL_zc3z04anonymousza31447ze3z87_1254;} }  else 
{ 
 long BgL_iz00_7169;
BgL_iz00_7169 = 
(BgL_iz00_1253+1L); 
BgL_iz00_1253 = BgL_iz00_7169; 
goto BgL_zc3z04anonymousza31447ze3z87_1254;} } } } } 

}



/* &string-replace! */
obj_t BGl_z62stringzd2replacez12za2zz__r4_strings_6_7z00(obj_t BgL_envz00_4158, obj_t BgL_strz00_4159, obj_t BgL_c1z00_4160, obj_t BgL_c2z00_4161)
{
{ /* Ieee/string.scm 776 */
{ /* Ieee/string.scm 777 */
 unsigned char BgL_auxz00_7187; unsigned char BgL_auxz00_7178; obj_t BgL_auxz00_7171;
{ /* Ieee/string.scm 777 */
 obj_t BgL_tmpz00_7188;
if(
CHARP(BgL_c2z00_4161))
{ /* Ieee/string.scm 777 */
BgL_tmpz00_7188 = BgL_c2z00_4161
; }  else 
{ 
 obj_t BgL_auxz00_7191;
BgL_auxz00_7191 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(35557L), BGl_string3433z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_c2z00_4161); 
FAILURE(BgL_auxz00_7191,BFALSE,BFALSE);} 
BgL_auxz00_7187 = 
CCHAR(BgL_tmpz00_7188); } 
{ /* Ieee/string.scm 777 */
 obj_t BgL_tmpz00_7179;
if(
CHARP(BgL_c1z00_4160))
{ /* Ieee/string.scm 777 */
BgL_tmpz00_7179 = BgL_c1z00_4160
; }  else 
{ 
 obj_t BgL_auxz00_7182;
BgL_auxz00_7182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(35557L), BGl_string3433z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_c1z00_4160); 
FAILURE(BgL_auxz00_7182,BFALSE,BFALSE);} 
BgL_auxz00_7178 = 
CCHAR(BgL_tmpz00_7179); } 
if(
STRINGP(BgL_strz00_4159))
{ /* Ieee/string.scm 777 */
BgL_auxz00_7171 = BgL_strz00_4159
; }  else 
{ 
 obj_t BgL_auxz00_7174;
BgL_auxz00_7174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(35557L), BGl_string3433z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4159); 
FAILURE(BgL_auxz00_7174,BFALSE,BFALSE);} 
return 
BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(BgL_auxz00_7171, BgL_auxz00_7178, BgL_auxz00_7187);} } 

}



/* _string-delete */
obj_t BGl__stringzd2deletezd2zz__r4_strings_6_7z00(obj_t BgL_env1129z00_121, obj_t BgL_opt1128z00_120)
{
{ /* Ieee/string.scm 791 */
{ /* Ieee/string.scm 791 */
 obj_t BgL_stringz00_1262; obj_t BgL_g1130z00_1263;
BgL_stringz00_1262 = 
VECTOR_REF(BgL_opt1128z00_120,0L); 
BgL_g1130z00_1263 = 
VECTOR_REF(BgL_opt1128z00_120,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1128z00_120)) { case 2L : 

{ /* Ieee/string.scm 791 */
 long BgL_endz00_1267;
{ /* Ieee/string.scm 791 */
 obj_t BgL_stringz00_2944;
if(
STRINGP(BgL_stringz00_1262))
{ /* Ieee/string.scm 791 */
BgL_stringz00_2944 = BgL_stringz00_1262; }  else 
{ 
 obj_t BgL_auxz00_7201;
BgL_auxz00_7201 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36080L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_1262); 
FAILURE(BgL_auxz00_7201,BFALSE,BFALSE);} 
BgL_endz00_1267 = 
STRING_LENGTH(BgL_stringz00_2944); } 
{ /* Ieee/string.scm 791 */

{ /* Ieee/string.scm 791 */
 obj_t BgL_auxz00_7206;
if(
STRINGP(BgL_stringz00_1262))
{ /* Ieee/string.scm 791 */
BgL_auxz00_7206 = BgL_stringz00_1262
; }  else 
{ 
 obj_t BgL_auxz00_7209;
BgL_auxz00_7209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36009L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_1262); 
FAILURE(BgL_auxz00_7209,BFALSE,BFALSE);} 
return 
BGl_stringzd2deletezd2zz__r4_strings_6_7z00(BgL_auxz00_7206, BgL_g1130z00_1263, 
(int)(0L), BgL_endz00_1267);} } } break;case 3L : 

{ /* Ieee/string.scm 791 */
 obj_t BgL_startz00_1268;
BgL_startz00_1268 = 
VECTOR_REF(BgL_opt1128z00_120,2L); 
{ /* Ieee/string.scm 791 */
 long BgL_endz00_1269;
{ /* Ieee/string.scm 791 */
 obj_t BgL_stringz00_2945;
if(
STRINGP(BgL_stringz00_1262))
{ /* Ieee/string.scm 791 */
BgL_stringz00_2945 = BgL_stringz00_1262; }  else 
{ 
 obj_t BgL_auxz00_7218;
BgL_auxz00_7218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36080L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_1262); 
FAILURE(BgL_auxz00_7218,BFALSE,BFALSE);} 
BgL_endz00_1269 = 
STRING_LENGTH(BgL_stringz00_2945); } 
{ /* Ieee/string.scm 791 */

{ /* Ieee/string.scm 791 */
 int BgL_auxz00_7230; obj_t BgL_auxz00_7223;
{ /* Ieee/string.scm 791 */
 obj_t BgL_tmpz00_7231;
if(
INTEGERP(BgL_startz00_1268))
{ /* Ieee/string.scm 791 */
BgL_tmpz00_7231 = BgL_startz00_1268
; }  else 
{ 
 obj_t BgL_auxz00_7234;
BgL_auxz00_7234 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36009L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_1268); 
FAILURE(BgL_auxz00_7234,BFALSE,BFALSE);} 
BgL_auxz00_7230 = 
CINT(BgL_tmpz00_7231); } 
if(
STRINGP(BgL_stringz00_1262))
{ /* Ieee/string.scm 791 */
BgL_auxz00_7223 = BgL_stringz00_1262
; }  else 
{ 
 obj_t BgL_auxz00_7226;
BgL_auxz00_7226 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36009L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_1262); 
FAILURE(BgL_auxz00_7226,BFALSE,BFALSE);} 
return 
BGl_stringzd2deletezd2zz__r4_strings_6_7z00(BgL_auxz00_7223, BgL_g1130z00_1263, BgL_auxz00_7230, BgL_endz00_1269);} } } } break;case 4L : 

{ /* Ieee/string.scm 791 */
 obj_t BgL_startz00_1270;
BgL_startz00_1270 = 
VECTOR_REF(BgL_opt1128z00_120,2L); 
{ /* Ieee/string.scm 791 */
 obj_t BgL_endz00_1271;
BgL_endz00_1271 = 
VECTOR_REF(BgL_opt1128z00_120,3L); 
{ /* Ieee/string.scm 791 */

{ /* Ieee/string.scm 791 */
 long BgL_auxz00_7258; int BgL_auxz00_7249; obj_t BgL_auxz00_7242;
{ /* Ieee/string.scm 791 */
 obj_t BgL_tmpz00_7259;
if(
INTEGERP(BgL_endz00_1271))
{ /* Ieee/string.scm 791 */
BgL_tmpz00_7259 = BgL_endz00_1271
; }  else 
{ 
 obj_t BgL_auxz00_7262;
BgL_auxz00_7262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36009L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_endz00_1271); 
FAILURE(BgL_auxz00_7262,BFALSE,BFALSE);} 
BgL_auxz00_7258 = 
(long)CINT(BgL_tmpz00_7259); } 
{ /* Ieee/string.scm 791 */
 obj_t BgL_tmpz00_7250;
if(
INTEGERP(BgL_startz00_1270))
{ /* Ieee/string.scm 791 */
BgL_tmpz00_7250 = BgL_startz00_1270
; }  else 
{ 
 obj_t BgL_auxz00_7253;
BgL_auxz00_7253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36009L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_1270); 
FAILURE(BgL_auxz00_7253,BFALSE,BFALSE);} 
BgL_auxz00_7249 = 
CINT(BgL_tmpz00_7250); } 
if(
STRINGP(BgL_stringz00_1262))
{ /* Ieee/string.scm 791 */
BgL_auxz00_7242 = BgL_stringz00_1262
; }  else 
{ 
 obj_t BgL_auxz00_7245;
BgL_auxz00_7245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(36009L), BGl_string3437z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_1262); 
FAILURE(BgL_auxz00_7245,BFALSE,BFALSE);} 
return 
BGl_stringzd2deletezd2zz__r4_strings_6_7z00(BgL_auxz00_7242, BgL_g1130z00_1263, BgL_auxz00_7249, BgL_auxz00_7258);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3434z00zz__r4_strings_6_7z00, BGl_string3436z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1128z00_120)));} } } } 

}



/* string-delete */
BGL_EXPORTED_DEF obj_t BGl_stringzd2deletezd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_116, obj_t BgL_objz00_117, int BgL_startz00_118, long BgL_endz00_119)
{
{ /* Ieee/string.scm 791 */
{ 
 obj_t BgL_newz00_1320; obj_t BgL_predz00_1321; int BgL_startz00_1322; long BgL_endz00_1323; obj_t BgL_newz00_1304; obj_t BgL_lz00_1305; int BgL_startz00_1306; long BgL_endz00_1307; obj_t BgL_newz00_1288; obj_t BgL_cz00_1289; int BgL_startz00_1290; long BgL_endz00_1291;
if(
(
(long)(BgL_startz00_118)<0L))
{ /* Ieee/string.scm 831 */
 obj_t BgL_aux2887z00_4846;
BgL_aux2887z00_4846 = 
BGl_errorz00zz__errorz00(BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3446z00zz__r4_strings_6_7z00, 
BINT(BgL_startz00_118)); 
if(
STRINGP(BgL_aux2887z00_4846))
{ /* Ieee/string.scm 831 */
return BgL_aux2887z00_4846;}  else 
{ 
 obj_t BgL_auxz00_7280;
BgL_auxz00_7280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(37074L), BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2887z00_4846); 
FAILURE(BgL_auxz00_7280,BFALSE,BFALSE);} }  else 
{ /* Ieee/string.scm 830 */
if(
(BgL_endz00_119>
STRING_LENGTH(BgL_stringz00_116)))
{ /* Ieee/string.scm 833 */
 obj_t BgL_aux2889z00_4848;
BgL_aux2889z00_4848 = 
BGl_errorz00zz__errorz00(BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3447z00zz__r4_strings_6_7z00, 
BINT(BgL_endz00_119)); 
if(
STRINGP(BgL_aux2889z00_4848))
{ /* Ieee/string.scm 833 */
return BgL_aux2889z00_4848;}  else 
{ 
 obj_t BgL_auxz00_7291;
BgL_auxz00_7291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(37179L), BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2889z00_4848); 
FAILURE(BgL_auxz00_7291,BFALSE,BFALSE);} }  else 
{ /* Ieee/string.scm 832 */
if(
(BgL_endz00_119<
(long)(BgL_startz00_118)))
{ /* Ieee/string.scm 835 */
 obj_t BgL_arg1459z00_1280;
BgL_arg1459z00_1280 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_118), 
BINT(BgL_endz00_119)); 
{ /* Ieee/string.scm 835 */
 obj_t BgL_aux2891z00_4850;
BgL_aux2891z00_4850 = 
BGl_errorz00zz__errorz00(BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3448z00zz__r4_strings_6_7z00, BgL_arg1459z00_1280); 
if(
STRINGP(BgL_aux2891z00_4850))
{ /* Ieee/string.scm 835 */
return BgL_aux2891z00_4850;}  else 
{ 
 obj_t BgL_auxz00_7304;
BgL_auxz00_7304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(37263L), BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2891z00_4850); 
FAILURE(BgL_auxz00_7304,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 837 */
 obj_t BgL_newz00_1282;
BgL_newz00_1282 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_stringz00_116); 
{ /* Ieee/string.scm 838 */

if(
CHARP(BgL_objz00_117))
{ /* Ieee/string.scm 840 */
BgL_newz00_1288 = BgL_newz00_1282; 
BgL_cz00_1289 = BgL_objz00_117; 
BgL_startz00_1290 = BgL_startz00_118; 
BgL_endz00_1291 = BgL_endz00_119; 
{ 
 int BgL_iz00_1294; long BgL_jz00_1295;
BgL_iz00_1294 = BgL_startz00_1290; 
BgL_jz00_1295 = 0L; 
BgL_zc3z04anonymousza31466ze3z87_1296:
if(
(
(long)(BgL_iz00_1294)==BgL_endz00_1291))
{ /* Ieee/string.scm 796 */
return 
bgl_string_shrink(BgL_newz00_1288, BgL_jz00_1295);}  else 
{ /* Ieee/string.scm 798 */
 unsigned char BgL_ccz00_1298;
{ /* Ieee/string.scm 798 */
 long BgL_kz00_2951;
BgL_kz00_2951 = 
(long)(BgL_iz00_1294); 
{ /* Ieee/string.scm 331 */
 long BgL_l2341z00_4299;
BgL_l2341z00_4299 = 
STRING_LENGTH(BgL_stringz00_116); 
if(
BOUND_CHECK(BgL_kz00_2951, BgL_l2341z00_4299))
{ /* Ieee/string.scm 331 */
BgL_ccz00_1298 = 
STRING_REF(BgL_stringz00_116, BgL_kz00_2951); }  else 
{ 
 obj_t BgL_auxz00_7320;
BgL_auxz00_7320 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_116, 
(int)(BgL_l2341z00_4299), 
(int)(BgL_kz00_2951)); 
FAILURE(BgL_auxz00_7320,BFALSE,BFALSE);} } } 
if(
(BgL_ccz00_1298==
CCHAR(BgL_cz00_1289)))
{ /* Ieee/string.scm 800 */
 long BgL_arg1469z00_1300;
BgL_arg1469z00_1300 = 
(
(long)(BgL_iz00_1294)+1L); 
{ 
 int BgL_iz00_7331;
BgL_iz00_7331 = 
(int)(BgL_arg1469z00_1300); 
BgL_iz00_1294 = BgL_iz00_7331; 
goto BgL_zc3z04anonymousza31466ze3z87_1296;} }  else 
{ /* Ieee/string.scm 799 */
{ /* Ieee/string.scm 337 */
 long BgL_l2345z00_4303;
BgL_l2345z00_4303 = 
STRING_LENGTH(BgL_newz00_1288); 
if(
BOUND_CHECK(BgL_jz00_1295, BgL_l2345z00_4303))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_newz00_1288, BgL_jz00_1295, BgL_ccz00_1298); }  else 
{ 
 obj_t BgL_auxz00_7337;
BgL_auxz00_7337 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_newz00_1288, 
(int)(BgL_l2345z00_4303), 
(int)(BgL_jz00_1295)); 
FAILURE(BgL_auxz00_7337,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 803 */
 long BgL_arg1472z00_1301; long BgL_arg1473z00_1302;
BgL_arg1472z00_1301 = 
(
(long)(BgL_iz00_1294)+1L); 
BgL_arg1473z00_1302 = 
(BgL_jz00_1295+1L); 
{ 
 long BgL_jz00_7348; int BgL_iz00_7346;
BgL_iz00_7346 = 
(int)(BgL_arg1472z00_1301); 
BgL_jz00_7348 = BgL_arg1473z00_1302; 
BgL_jz00_1295 = BgL_jz00_7348; 
BgL_iz00_1294 = BgL_iz00_7346; 
goto BgL_zc3z04anonymousza31466ze3z87_1296;} } } } } }  else 
{ /* Ieee/string.scm 840 */
if(
STRINGP(BgL_objz00_117))
{ /* Ieee/string.scm 842 */
BgL_newz00_1304 = BgL_newz00_1282; 
BgL_lz00_1305 = 
BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_objz00_117); 
BgL_startz00_1306 = BgL_startz00_118; 
BgL_endz00_1307 = BgL_endz00_119; 
{ 
 int BgL_iz00_1310; long BgL_jz00_1311;
BgL_iz00_1310 = BgL_startz00_1306; 
BgL_jz00_1311 = 0L; 
BgL_zc3z04anonymousza31475ze3z87_1312:
if(
(
(long)(BgL_iz00_1310)==BgL_endz00_1307))
{ /* Ieee/string.scm 808 */
return 
bgl_string_shrink(BgL_newz00_1304, BgL_jz00_1311);}  else 
{ /* Ieee/string.scm 810 */
 unsigned char BgL_ccz00_1314;
{ /* Ieee/string.scm 810 */
 long BgL_kz00_2965;
BgL_kz00_2965 = 
(long)(BgL_iz00_1310); 
{ /* Ieee/string.scm 331 */
 long BgL_l2349z00_4307;
BgL_l2349z00_4307 = 
STRING_LENGTH(BgL_stringz00_116); 
if(
BOUND_CHECK(BgL_kz00_2965, BgL_l2349z00_4307))
{ /* Ieee/string.scm 331 */
BgL_ccz00_1314 = 
STRING_REF(BgL_stringz00_116, BgL_kz00_2965); }  else 
{ 
 obj_t BgL_auxz00_7360;
BgL_auxz00_7360 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_116, 
(int)(BgL_l2349z00_4307), 
(int)(BgL_kz00_2965)); 
FAILURE(BgL_auxz00_7360,BFALSE,BFALSE);} } } 
if(
CBOOL(
BGl_memvz00zz__r4_pairs_and_lists_6_3z00(
BCHAR(BgL_ccz00_1314), BgL_lz00_1305)))
{ /* Ieee/string.scm 812 */
 long BgL_arg1478z00_1316;
BgL_arg1478z00_1316 = 
(
(long)(BgL_iz00_1310)+1L); 
{ 
 int BgL_iz00_7372;
BgL_iz00_7372 = 
(int)(BgL_arg1478z00_1316); 
BgL_iz00_1310 = BgL_iz00_7372; 
goto BgL_zc3z04anonymousza31475ze3z87_1312;} }  else 
{ /* Ieee/string.scm 811 */
{ /* Ieee/string.scm 337 */
 long BgL_l2353z00_4311;
BgL_l2353z00_4311 = 
STRING_LENGTH(BgL_newz00_1304); 
if(
BOUND_CHECK(BgL_jz00_1311, BgL_l2353z00_4311))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_newz00_1304, BgL_jz00_1311, BgL_ccz00_1314); }  else 
{ 
 obj_t BgL_auxz00_7378;
BgL_auxz00_7378 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_newz00_1304, 
(int)(BgL_l2353z00_4311), 
(int)(BgL_jz00_1311)); 
FAILURE(BgL_auxz00_7378,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 815 */
 long BgL_arg1479z00_1317; long BgL_arg1480z00_1318;
BgL_arg1479z00_1317 = 
(
(long)(BgL_iz00_1310)+1L); 
BgL_arg1480z00_1318 = 
(BgL_jz00_1311+1L); 
{ 
 long BgL_jz00_7389; int BgL_iz00_7387;
BgL_iz00_7387 = 
(int)(BgL_arg1479z00_1317); 
BgL_jz00_7389 = BgL_arg1480z00_1318; 
BgL_jz00_1311 = BgL_jz00_7389; 
BgL_iz00_1310 = BgL_iz00_7387; 
goto BgL_zc3z04anonymousza31475ze3z87_1312;} } } } } }  else 
{ /* Ieee/string.scm 842 */
if(
PROCEDUREP(BgL_objz00_117))
{ /* Ieee/string.scm 844 */
BgL_newz00_1320 = BgL_newz00_1282; 
BgL_predz00_1321 = BgL_objz00_117; 
BgL_startz00_1322 = BgL_startz00_118; 
BgL_endz00_1323 = BgL_endz00_119; 
{ 
 int BgL_iz00_1326; long BgL_jz00_1327;
BgL_iz00_1326 = BgL_startz00_1322; 
BgL_jz00_1327 = 0L; 
BgL_zc3z04anonymousza31482ze3z87_1328:
if(
(
(long)(BgL_iz00_1326)==BgL_endz00_1323))
{ /* Ieee/string.scm 820 */
return 
bgl_string_shrink(BgL_newz00_1320, BgL_jz00_1327);}  else 
{ /* Ieee/string.scm 822 */
 unsigned char BgL_ccz00_1330;
{ /* Ieee/string.scm 822 */
 long BgL_kz00_2977;
BgL_kz00_2977 = 
(long)(BgL_iz00_1326); 
{ /* Ieee/string.scm 331 */
 long BgL_l2357z00_4315;
BgL_l2357z00_4315 = 
STRING_LENGTH(BgL_stringz00_116); 
if(
BOUND_CHECK(BgL_kz00_2977, BgL_l2357z00_4315))
{ /* Ieee/string.scm 331 */
BgL_ccz00_1330 = 
STRING_REF(BgL_stringz00_116, BgL_kz00_2977); }  else 
{ 
 obj_t BgL_auxz00_7402;
BgL_auxz00_7402 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_116, 
(int)(BgL_l2357z00_4315), 
(int)(BgL_kz00_2977)); 
FAILURE(BgL_auxz00_7402,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 823 */
 bool_t BgL_test4021z00_7408;
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_1321, 1))
{ /* Ieee/string.scm 823 */
BgL_test4021z00_7408 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_1321, 
BCHAR(BgL_ccz00_1330)))
; }  else 
{ /* Ieee/string.scm 823 */
FAILURE(BGl_string3438z00zz__r4_strings_6_7z00,BGl_list3439z00zz__r4_strings_6_7z00,BgL_predz00_1321);} 
if(BgL_test4021z00_7408)
{ /* Ieee/string.scm 824 */
 long BgL_arg1485z00_1332;
BgL_arg1485z00_1332 = 
(
(long)(BgL_iz00_1326)+1L); 
{ 
 int BgL_iz00_7420;
BgL_iz00_7420 = 
(int)(BgL_arg1485z00_1332); 
BgL_iz00_1326 = BgL_iz00_7420; 
goto BgL_zc3z04anonymousza31482ze3z87_1328;} }  else 
{ /* Ieee/string.scm 823 */
{ /* Ieee/string.scm 337 */
 long BgL_l2361z00_4319;
BgL_l2361z00_4319 = 
STRING_LENGTH(BgL_newz00_1320); 
if(
BOUND_CHECK(BgL_jz00_1327, BgL_l2361z00_4319))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_newz00_1320, BgL_jz00_1327, BgL_ccz00_1330); }  else 
{ 
 obj_t BgL_auxz00_7426;
BgL_auxz00_7426 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_newz00_1320, 
(int)(BgL_l2361z00_4319), 
(int)(BgL_jz00_1327)); 
FAILURE(BgL_auxz00_7426,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 827 */
 long BgL_arg1486z00_1333; long BgL_arg1487z00_1334;
BgL_arg1486z00_1333 = 
(
(long)(BgL_iz00_1326)+1L); 
BgL_arg1487z00_1334 = 
(BgL_jz00_1327+1L); 
{ 
 long BgL_jz00_7437; int BgL_iz00_7435;
BgL_iz00_7435 = 
(int)(BgL_arg1486z00_1333); 
BgL_jz00_7437 = BgL_arg1487z00_1334; 
BgL_jz00_1327 = BgL_jz00_7437; 
BgL_iz00_1326 = BgL_iz00_7435; 
goto BgL_zc3z04anonymousza31482ze3z87_1328;} } } } } } }  else 
{ /* Ieee/string.scm 847 */
 obj_t BgL_aux2893z00_4852;
BgL_aux2893z00_4852 = 
BGl_errorz00zz__errorz00(BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3449z00zz__r4_strings_6_7z00, BgL_objz00_117); 
if(
STRINGP(BgL_aux2893z00_4852))
{ /* Ieee/string.scm 847 */
return BgL_aux2893z00_4852;}  else 
{ 
 obj_t BgL_auxz00_7441;
BgL_auxz00_7441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(37661L), BGl_string3435z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux2893z00_4852); 
FAILURE(BgL_auxz00_7441,BFALSE,BFALSE);} } } } } } } } } } 

}



/* delim? */
bool_t BGl_delimzf3zf3zz__r4_strings_6_7z00(obj_t BgL_delimsz00_122, unsigned char BgL_charz00_123)
{
{ /* Ieee/string.scm 852 */
{ /* Ieee/string.scm 853 */
 long BgL_lenz00_1339;
{ /* Ieee/string.scm 853 */
 obj_t BgL_stringz00_2992;
if(
STRINGP(BgL_delimsz00_122))
{ /* Ieee/string.scm 853 */
BgL_stringz00_2992 = BgL_delimsz00_122; }  else 
{ 
 obj_t BgL_auxz00_7447;
BgL_auxz00_7447 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(37994L), BGl_string3450z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_delimsz00_122); 
FAILURE(BgL_auxz00_7447,BFALSE,BFALSE);} 
BgL_lenz00_1339 = 
STRING_LENGTH(BgL_stringz00_2992); } 
{ 
 long BgL_iz00_1341;
BgL_iz00_1341 = 0L; 
BgL_zc3z04anonymousza31488ze3z87_1342:
if(
(BgL_iz00_1341==BgL_lenz00_1339))
{ /* Ieee/string.scm 856 */
return ((bool_t)0);}  else 
{ /* Ieee/string.scm 858 */
 bool_t BgL_test4027z00_7454;
{ /* Ieee/string.scm 858 */
 unsigned char BgL_tmpz00_7455;
{ /* Ieee/string.scm 858 */
 obj_t BgL_stringz00_2995;
if(
STRINGP(BgL_delimsz00_122))
{ /* Ieee/string.scm 858 */
BgL_stringz00_2995 = BgL_delimsz00_122; }  else 
{ 
 obj_t BgL_auxz00_7458;
BgL_auxz00_7458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(38113L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_delimsz00_122); 
FAILURE(BgL_auxz00_7458,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 343 */
 long BgL_l2365z00_4323;
BgL_l2365z00_4323 = 
STRING_LENGTH(BgL_stringz00_2995); 
if(
BOUND_CHECK(BgL_iz00_1341, BgL_l2365z00_4323))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_7455 = 
STRING_REF(BgL_stringz00_2995, BgL_iz00_1341)
; }  else 
{ 
 obj_t BgL_auxz00_7466;
BgL_auxz00_7466 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_2995, 
(int)(BgL_l2365z00_4323), 
(int)(BgL_iz00_1341)); 
FAILURE(BgL_auxz00_7466,BFALSE,BFALSE);} } } 
BgL_test4027z00_7454 = 
(BgL_charz00_123==BgL_tmpz00_7455); } 
if(BgL_test4027z00_7454)
{ /* Ieee/string.scm 858 */
return ((bool_t)1);}  else 
{ 
 long BgL_iz00_7473;
BgL_iz00_7473 = 
(BgL_iz00_1341+1L); 
BgL_iz00_1341 = BgL_iz00_7473; 
goto BgL_zc3z04anonymousza31488ze3z87_1342;} } } } } 

}



/* string-split */
BGL_EXPORTED_DEF obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_132, obj_t BgL_delimitersz00_133)
{
{ /* Ieee/string.scm 890 */
{ /* Ieee/string.scm 891 */
 obj_t BgL_dz00_1359;
if(
PAIRP(BgL_delimitersz00_133))
{ /* Ieee/string.scm 891 */
BgL_dz00_1359 = 
CAR(BgL_delimitersz00_133); }  else 
{ /* Ieee/string.scm 891 */
BgL_dz00_1359 = BGl_string3451z00zz__r4_strings_6_7z00; } 
{ /* Ieee/string.scm 891 */
 long BgL_lenz00_1360;
BgL_lenz00_1360 = 
STRING_LENGTH(BgL_stringz00_132); 
{ /* Ieee/string.scm 894 */
 long BgL_iz00_1361;
{ 
 long BgL_iz00_3039;
BgL_iz00_3039 = 0L; 
BgL_skipzd2separatorzd2_3038:
if(
(BgL_iz00_3039==BgL_lenz00_1360))
{ /* Ieee/string.scm 868 */
BgL_iz00_1361 = BgL_lenz00_1360; }  else 
{ /* Ieee/string.scm 870 */
 bool_t BgL_test4032z00_7481;
{ /* Ieee/string.scm 870 */
 unsigned char BgL_auxz00_7482;
{ /* Ieee/string.scm 343 */
 long BgL_l2369z00_4327;
BgL_l2369z00_4327 = 
STRING_LENGTH(BgL_stringz00_132); 
if(
BOUND_CHECK(BgL_iz00_3039, BgL_l2369z00_4327))
{ /* Ieee/string.scm 343 */
BgL_auxz00_7482 = 
STRING_REF(BgL_stringz00_132, BgL_iz00_3039)
; }  else 
{ 
 obj_t BgL_auxz00_7487;
BgL_auxz00_7487 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_132, 
(int)(BgL_l2369z00_4327), 
(int)(BgL_iz00_3039)); 
FAILURE(BgL_auxz00_7487,BFALSE,BFALSE);} } 
BgL_test4032z00_7481 = 
BGl_delimzf3zf3zz__r4_strings_6_7z00(BgL_dz00_1359, BgL_auxz00_7482); } 
if(BgL_test4032z00_7481)
{ 
 long BgL_iz00_7494;
BgL_iz00_7494 = 
(BgL_iz00_3039+1L); 
BgL_iz00_3039 = BgL_iz00_7494; 
goto BgL_skipzd2separatorzd2_3038;}  else 
{ /* Ieee/string.scm 870 */
BgL_iz00_1361 = BgL_iz00_3039; } } } 
{ /* Ieee/string.scm 895 */

{ 
 long BgL_iz00_1364; obj_t BgL_resz00_1365;
BgL_iz00_1364 = BgL_iz00_1361; 
BgL_resz00_1365 = BNIL; 
BgL_zc3z04anonymousza31506ze3z87_1366:
if(
(BgL_iz00_1364==BgL_lenz00_1360))
{ /* Ieee/string.scm 898 */
return 
bgl_reverse_bang(BgL_resz00_1365);}  else 
{ /* Ieee/string.scm 900 */
 long BgL_ez00_1368;
{ /* Ieee/string.scm 900 */
 long BgL_arg1513z00_1374;
BgL_arg1513z00_1374 = 
(BgL_iz00_1364+1L); 
{ 
 long BgL_iz00_3053;
BgL_iz00_3053 = BgL_arg1513z00_1374; 
BgL_skipzd2nonzd2separatorz00_3052:
if(
(BgL_iz00_3053==BgL_lenz00_1360))
{ /* Ieee/string.scm 880 */
BgL_ez00_1368 = BgL_lenz00_1360; }  else 
{ /* Ieee/string.scm 882 */
 bool_t BgL_test4036z00_7502;
{ /* Ieee/string.scm 882 */
 unsigned char BgL_auxz00_7503;
{ /* Ieee/string.scm 343 */
 long BgL_l2373z00_4331;
BgL_l2373z00_4331 = 
STRING_LENGTH(BgL_stringz00_132); 
if(
BOUND_CHECK(BgL_iz00_3053, BgL_l2373z00_4331))
{ /* Ieee/string.scm 343 */
BgL_auxz00_7503 = 
STRING_REF(BgL_stringz00_132, BgL_iz00_3053)
; }  else 
{ 
 obj_t BgL_auxz00_7508;
BgL_auxz00_7508 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_132, 
(int)(BgL_l2373z00_4331), 
(int)(BgL_iz00_3053)); 
FAILURE(BgL_auxz00_7508,BFALSE,BFALSE);} } 
BgL_test4036z00_7502 = 
BGl_delimzf3zf3zz__r4_strings_6_7z00(BgL_dz00_1359, BgL_auxz00_7503); } 
if(BgL_test4036z00_7502)
{ /* Ieee/string.scm 882 */
BgL_ez00_1368 = BgL_iz00_3053; }  else 
{ 
 long BgL_iz00_7515;
BgL_iz00_7515 = 
(BgL_iz00_3053+1L); 
BgL_iz00_3053 = BgL_iz00_7515; 
goto BgL_skipzd2nonzd2separatorz00_3052;} } } } 
{ /* Ieee/string.scm 900 */
 obj_t BgL_nresz00_1369;
BgL_nresz00_1369 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_132, BgL_iz00_1364, BgL_ez00_1368), BgL_resz00_1365); 
{ /* Ieee/string.scm 901 */

if(
(BgL_ez00_1368==BgL_lenz00_1360))
{ /* Ieee/string.scm 902 */
return 
bgl_reverse_bang(BgL_nresz00_1369);}  else 
{ /* Ieee/string.scm 904 */
 long BgL_arg1509z00_1371;
{ /* Ieee/string.scm 904 */
 long BgL_arg1510z00_1372;
BgL_arg1510z00_1372 = 
(BgL_ez00_1368+1L); 
{ 
 long BgL_iz00_3067;
BgL_iz00_3067 = BgL_arg1510z00_1372; 
BgL_skipzd2separatorzd2_3066:
if(
(BgL_iz00_3067==BgL_lenz00_1360))
{ /* Ieee/string.scm 868 */
BgL_arg1509z00_1371 = BgL_lenz00_1360; }  else 
{ /* Ieee/string.scm 870 */
 bool_t BgL_test4040z00_7525;
{ /* Ieee/string.scm 870 */
 unsigned char BgL_auxz00_7526;
{ /* Ieee/string.scm 343 */
 long BgL_l2377z00_4335;
BgL_l2377z00_4335 = 
STRING_LENGTH(BgL_stringz00_132); 
if(
BOUND_CHECK(BgL_iz00_3067, BgL_l2377z00_4335))
{ /* Ieee/string.scm 343 */
BgL_auxz00_7526 = 
STRING_REF(BgL_stringz00_132, BgL_iz00_3067)
; }  else 
{ 
 obj_t BgL_auxz00_7531;
BgL_auxz00_7531 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_132, 
(int)(BgL_l2377z00_4335), 
(int)(BgL_iz00_3067)); 
FAILURE(BgL_auxz00_7531,BFALSE,BFALSE);} } 
BgL_test4040z00_7525 = 
BGl_delimzf3zf3zz__r4_strings_6_7z00(BgL_dz00_1359, BgL_auxz00_7526); } 
if(BgL_test4040z00_7525)
{ 
 long BgL_iz00_7538;
BgL_iz00_7538 = 
(BgL_iz00_3067+1L); 
BgL_iz00_3067 = BgL_iz00_7538; 
goto BgL_skipzd2separatorzd2_3066;}  else 
{ /* Ieee/string.scm 870 */
BgL_arg1509z00_1371 = BgL_iz00_3067; } } } } 
{ 
 obj_t BgL_resz00_7541; long BgL_iz00_7540;
BgL_iz00_7540 = BgL_arg1509z00_1371; 
BgL_resz00_7541 = BgL_nresz00_1369; 
BgL_resz00_1365 = BgL_resz00_7541; 
BgL_iz00_1364 = BgL_iz00_7540; 
goto BgL_zc3z04anonymousza31506ze3z87_1366;} } } } } } } } } } } 

}



/* &string-split */
obj_t BGl_z62stringzd2splitzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4162, obj_t BgL_stringz00_4163, obj_t BgL_delimitersz00_4164)
{
{ /* Ieee/string.scm 890 */
{ /* Ieee/string.scm 891 */
 obj_t BgL_auxz00_7542;
if(
STRINGP(BgL_stringz00_4163))
{ /* Ieee/string.scm 891 */
BgL_auxz00_7542 = BgL_stringz00_4163
; }  else 
{ 
 obj_t BgL_auxz00_7545;
BgL_auxz00_7545 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(39316L), BGl_string3452z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4163); 
FAILURE(BgL_auxz00_7545,BFALSE,BFALSE);} 
return 
BGl_stringzd2splitzd2zz__r4_strings_6_7z00(BgL_auxz00_7542, BgL_delimitersz00_4164);} } 

}



/* string-cut */
BGL_EXPORTED_DEF obj_t BGl_stringzd2cutzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_134, obj_t BgL_delimitersz00_135)
{
{ /* Ieee/string.scm 910 */
{ /* Ieee/string.scm 911 */
 obj_t BgL_dz00_1377;
if(
PAIRP(BgL_delimitersz00_135))
{ /* Ieee/string.scm 911 */
BgL_dz00_1377 = 
CAR(BgL_delimitersz00_135); }  else 
{ /* Ieee/string.scm 911 */
BgL_dz00_1377 = BGl_string3451z00zz__r4_strings_6_7z00; } 
{ /* Ieee/string.scm 911 */
 long BgL_lenz00_1378;
BgL_lenz00_1378 = 
STRING_LENGTH(BgL_stringz00_134); 
{ /* Ieee/string.scm 914 */

{ 
 long BgL_iz00_1381; obj_t BgL_resz00_1382;
BgL_iz00_1381 = 0L; 
BgL_resz00_1382 = BNIL; 
BgL_zc3z04anonymousza31515ze3z87_1383:
if(
(BgL_iz00_1381>=BgL_lenz00_1378))
{ /* Ieee/string.scm 918 */
 obj_t BgL_arg1517z00_1385;
BgL_arg1517z00_1385 = 
MAKE_YOUNG_PAIR(BGl_string3407z00zz__r4_strings_6_7z00, BgL_resz00_1382); 
return 
bgl_reverse_bang(BgL_arg1517z00_1385);}  else 
{ /* Ieee/string.scm 919 */
 long BgL_ez00_1386;
{ 
 long BgL_iz00_3082;
BgL_iz00_3082 = BgL_iz00_1381; 
BgL_skipzd2nonzd2separatorz00_3081:
if(
(BgL_iz00_3082==BgL_lenz00_1378))
{ /* Ieee/string.scm 880 */
BgL_ez00_1386 = BgL_lenz00_1378; }  else 
{ /* Ieee/string.scm 882 */
 bool_t BgL_test4046z00_7560;
{ /* Ieee/string.scm 882 */
 unsigned char BgL_auxz00_7561;
{ /* Ieee/string.scm 343 */
 long BgL_l2381z00_4339;
BgL_l2381z00_4339 = 
STRING_LENGTH(BgL_stringz00_134); 
if(
BOUND_CHECK(BgL_iz00_3082, BgL_l2381z00_4339))
{ /* Ieee/string.scm 343 */
BgL_auxz00_7561 = 
STRING_REF(BgL_stringz00_134, BgL_iz00_3082)
; }  else 
{ 
 obj_t BgL_auxz00_7566;
BgL_auxz00_7566 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_134, 
(int)(BgL_l2381z00_4339), 
(int)(BgL_iz00_3082)); 
FAILURE(BgL_auxz00_7566,BFALSE,BFALSE);} } 
BgL_test4046z00_7560 = 
BGl_delimzf3zf3zz__r4_strings_6_7z00(BgL_dz00_1377, BgL_auxz00_7561); } 
if(BgL_test4046z00_7560)
{ /* Ieee/string.scm 882 */
BgL_ez00_1386 = BgL_iz00_3082; }  else 
{ 
 long BgL_iz00_7573;
BgL_iz00_7573 = 
(BgL_iz00_3082+1L); 
BgL_iz00_3082 = BgL_iz00_7573; 
goto BgL_skipzd2nonzd2separatorz00_3081;} } } 
{ /* Ieee/string.scm 919 */
 obj_t BgL_sz00_1387;
BgL_sz00_1387 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_134, BgL_iz00_1381, BgL_ez00_1386); 
{ /* Ieee/string.scm 920 */
 obj_t BgL_nrz00_1388;
BgL_nrz00_1388 = 
MAKE_YOUNG_PAIR(BgL_sz00_1387, BgL_resz00_1382); 
{ /* Ieee/string.scm 921 */

if(
(BgL_ez00_1386==BgL_lenz00_1378))
{ /* Ieee/string.scm 922 */
return 
bgl_reverse_bang(BgL_nrz00_1388);}  else 
{ 
 obj_t BgL_resz00_7582; long BgL_iz00_7580;
BgL_iz00_7580 = 
(BgL_ez00_1386+1L); 
BgL_resz00_7582 = BgL_nrz00_1388; 
BgL_resz00_1382 = BgL_resz00_7582; 
BgL_iz00_1381 = BgL_iz00_7580; 
goto BgL_zc3z04anonymousza31515ze3z87_1383;} } } } } } } } } } 

}



/* &string-cut */
obj_t BGl_z62stringzd2cutzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4165, obj_t BgL_stringz00_4166, obj_t BgL_delimitersz00_4167)
{
{ /* Ieee/string.scm 910 */
{ /* Ieee/string.scm 911 */
 obj_t BgL_auxz00_7583;
if(
STRINGP(BgL_stringz00_4166))
{ /* Ieee/string.scm 911 */
BgL_auxz00_7583 = BgL_stringz00_4166
; }  else 
{ 
 obj_t BgL_auxz00_7586;
BgL_auxz00_7586 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(40015L), BGl_string3453z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4166); 
FAILURE(BgL_auxz00_7586,BFALSE,BFALSE);} 
return 
BGl_stringzd2cutzd2zz__r4_strings_6_7z00(BgL_auxz00_7583, BgL_delimitersz00_4167);} } 

}



/* string-char-index-ur */
BGL_EXPORTED_DEF obj_t BGl_stringzd2charzd2indexzd2urzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_136, unsigned char BgL_charz00_137, long BgL_startz00_138, long BgL_nz00_139)
{
{ /* Ieee/string.scm 929 */
{ /* Ieee/string.scm 932 */
 char * BgL_s0z00_3095;
BgL_s0z00_3095 = 
BSTRING_TO_STRING(BgL_stringz00_136); 
{ /* Ieee/string.scm 932 */
 char * BgL_s1z00_3096;
{ /* Ieee/string.scm 933 */
 int BgL_tmpz00_7592;
BgL_tmpz00_7592 = 
(int)(
(
(unsigned char)(BgL_charz00_137))); 
BgL_s1z00_3096 = 
BGL_MEMCHR(BgL_s0z00_3095, BgL_tmpz00_7592, BgL_nz00_139, BgL_startz00_138); } 
{ /* Ieee/string.scm 933 */

if(
BGL_MEMCHR_ZERO(BgL_s1z00_3096))
{ /* Ieee/string.scm 934 */
return BFALSE;}  else 
{ /* Ieee/string.scm 934 */
return 
BINT(
BGL_MEMCHR_DIFF(BgL_s1z00_3096, BgL_s0z00_3095));} } } } } 

}



/* &string-char-index-ur */
obj_t BGl_z62stringzd2charzd2indexzd2urzb0zz__r4_strings_6_7z00(obj_t BgL_envz00_4168, obj_t BgL_stringz00_4169, obj_t BgL_charz00_4170, obj_t BgL_startz00_4171, obj_t BgL_nz00_4172)
{
{ /* Ieee/string.scm 929 */
{ /* Ieee/string.scm 932 */
 long BgL_auxz00_7626; long BgL_auxz00_7617; unsigned char BgL_auxz00_7608; obj_t BgL_auxz00_7601;
{ /* Ieee/string.scm 932 */
 obj_t BgL_tmpz00_7627;
if(
INTEGERP(BgL_nz00_4172))
{ /* Ieee/string.scm 932 */
BgL_tmpz00_7627 = BgL_nz00_4172
; }  else 
{ 
 obj_t BgL_auxz00_7630;
BgL_auxz00_7630 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(40707L), BGl_string3454z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_nz00_4172); 
FAILURE(BgL_auxz00_7630,BFALSE,BFALSE);} 
BgL_auxz00_7626 = 
(long)CINT(BgL_tmpz00_7627); } 
{ /* Ieee/string.scm 932 */
 obj_t BgL_tmpz00_7618;
if(
INTEGERP(BgL_startz00_4171))
{ /* Ieee/string.scm 932 */
BgL_tmpz00_7618 = BgL_startz00_4171
; }  else 
{ 
 obj_t BgL_auxz00_7621;
BgL_auxz00_7621 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(40707L), BGl_string3454z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_4171); 
FAILURE(BgL_auxz00_7621,BFALSE,BFALSE);} 
BgL_auxz00_7617 = 
(long)CINT(BgL_tmpz00_7618); } 
{ /* Ieee/string.scm 932 */
 obj_t BgL_tmpz00_7609;
if(
CHARP(BgL_charz00_4170))
{ /* Ieee/string.scm 932 */
BgL_tmpz00_7609 = BgL_charz00_4170
; }  else 
{ 
 obj_t BgL_auxz00_7612;
BgL_auxz00_7612 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(40707L), BGl_string3454z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_charz00_4170); 
FAILURE(BgL_auxz00_7612,BFALSE,BFALSE);} 
BgL_auxz00_7608 = 
CCHAR(BgL_tmpz00_7609); } 
if(
STRINGP(BgL_stringz00_4169))
{ /* Ieee/string.scm 932 */
BgL_auxz00_7601 = BgL_stringz00_4169
; }  else 
{ 
 obj_t BgL_auxz00_7604;
BgL_auxz00_7604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(40707L), BGl_string3454z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_stringz00_4169); 
FAILURE(BgL_auxz00_7604,BFALSE,BFALSE);} 
return 
BGl_stringzd2charzd2indexzd2urzd2zz__r4_strings_6_7z00(BgL_auxz00_7601, BgL_auxz00_7608, BgL_auxz00_7617, BgL_auxz00_7626);} } 

}



/* _string-char-index */
obj_t BGl__stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t BgL_env1134z00_145, obj_t BgL_opt1133z00_144)
{
{ /* Ieee/string.scm 950 */
{ /* Ieee/string.scm 950 */
 obj_t BgL_g1135z00_1397; obj_t BgL_g1136z00_1398;
BgL_g1135z00_1397 = 
VECTOR_REF(BgL_opt1133z00_144,0L); 
BgL_g1136z00_1398 = 
VECTOR_REF(BgL_opt1133z00_144,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1133z00_144)) { case 2L : 

{ /* Ieee/string.scm 950 */

{ /* Ieee/string.scm 950 */
 unsigned char BgL_auxz00_7645; obj_t BgL_auxz00_7638;
{ /* Ieee/string.scm 950 */
 obj_t BgL_tmpz00_7646;
if(
CHARP(BgL_g1136z00_1398))
{ /* Ieee/string.scm 950 */
BgL_tmpz00_7646 = BgL_g1136z00_1398
; }  else 
{ 
 obj_t BgL_auxz00_7649;
BgL_auxz00_7649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41269L), BGl_string3457z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_g1136z00_1398); 
FAILURE(BgL_auxz00_7649,BFALSE,BFALSE);} 
BgL_auxz00_7645 = 
CCHAR(BgL_tmpz00_7646); } 
if(
STRINGP(BgL_g1135z00_1397))
{ /* Ieee/string.scm 950 */
BgL_auxz00_7638 = BgL_g1135z00_1397
; }  else 
{ 
 obj_t BgL_auxz00_7641;
BgL_auxz00_7641 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41269L), BGl_string3457z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1135z00_1397); 
FAILURE(BgL_auxz00_7641,BFALSE,BFALSE);} 
return 
BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(BgL_auxz00_7638, BgL_auxz00_7645, 
BINT(0L), 
BINT(-1L));} } break;case 3L : 

{ /* Ieee/string.scm 950 */
 obj_t BgL_startz00_1403;
BgL_startz00_1403 = 
VECTOR_REF(BgL_opt1133z00_144,2L); 
{ /* Ieee/string.scm 950 */

{ /* Ieee/string.scm 950 */
 unsigned char BgL_auxz00_7665; obj_t BgL_auxz00_7658;
{ /* Ieee/string.scm 950 */
 obj_t BgL_tmpz00_7666;
if(
CHARP(BgL_g1136z00_1398))
{ /* Ieee/string.scm 950 */
BgL_tmpz00_7666 = BgL_g1136z00_1398
; }  else 
{ 
 obj_t BgL_auxz00_7669;
BgL_auxz00_7669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41269L), BGl_string3457z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_g1136z00_1398); 
FAILURE(BgL_auxz00_7669,BFALSE,BFALSE);} 
BgL_auxz00_7665 = 
CCHAR(BgL_tmpz00_7666); } 
if(
STRINGP(BgL_g1135z00_1397))
{ /* Ieee/string.scm 950 */
BgL_auxz00_7658 = BgL_g1135z00_1397
; }  else 
{ 
 obj_t BgL_auxz00_7661;
BgL_auxz00_7661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41269L), BGl_string3457z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1135z00_1397); 
FAILURE(BgL_auxz00_7661,BFALSE,BFALSE);} 
return 
BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(BgL_auxz00_7658, BgL_auxz00_7665, BgL_startz00_1403, 
BINT(-1L));} } } break;case 4L : 

{ /* Ieee/string.scm 950 */
 obj_t BgL_startz00_1405;
BgL_startz00_1405 = 
VECTOR_REF(BgL_opt1133z00_144,2L); 
{ /* Ieee/string.scm 950 */
 obj_t BgL_countz00_1406;
BgL_countz00_1406 = 
VECTOR_REF(BgL_opt1133z00_144,3L); 
{ /* Ieee/string.scm 950 */

{ /* Ieee/string.scm 950 */
 unsigned char BgL_auxz00_7685; obj_t BgL_auxz00_7678;
{ /* Ieee/string.scm 950 */
 obj_t BgL_tmpz00_7686;
if(
CHARP(BgL_g1136z00_1398))
{ /* Ieee/string.scm 950 */
BgL_tmpz00_7686 = BgL_g1136z00_1398
; }  else 
{ 
 obj_t BgL_auxz00_7689;
BgL_auxz00_7689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41269L), BGl_string3457z00zz__r4_strings_6_7z00, BGl_string3362z00zz__r4_strings_6_7z00, BgL_g1136z00_1398); 
FAILURE(BgL_auxz00_7689,BFALSE,BFALSE);} 
BgL_auxz00_7685 = 
CCHAR(BgL_tmpz00_7686); } 
if(
STRINGP(BgL_g1135z00_1397))
{ /* Ieee/string.scm 950 */
BgL_auxz00_7678 = BgL_g1135z00_1397
; }  else 
{ 
 obj_t BgL_auxz00_7681;
BgL_auxz00_7681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41269L), BGl_string3457z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1135z00_1397); 
FAILURE(BgL_auxz00_7681,BFALSE,BFALSE);} 
return 
BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(BgL_auxz00_7678, BgL_auxz00_7685, BgL_startz00_1405, BgL_countz00_1406);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3455z00zz__r4_strings_6_7z00, BGl_string3436z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1133z00_144)));} } } } 

}



/* string-char-index */
BGL_EXPORTED_DEF obj_t BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(obj_t BgL_stringz00_140, unsigned char BgL_charz00_141, obj_t BgL_startz00_142, obj_t BgL_countz00_143)
{
{ /* Ieee/string.scm 950 */
{ /* Ieee/string.scm 951 */
 long BgL_lenz00_1408;
BgL_lenz00_1408 = 
STRING_LENGTH(BgL_stringz00_140); 
{ /* Ieee/string.scm 952 */

{ /* Ieee/string.scm 956 */
 bool_t BgL_test4061z00_7701;
{ /* Ieee/string.scm 956 */
 long BgL_n2z00_3109;
{ /* Ieee/string.scm 956 */
 obj_t BgL_tmpz00_7702;
if(
INTEGERP(BgL_startz00_142))
{ /* Ieee/string.scm 956 */
BgL_tmpz00_7702 = BgL_startz00_142
; }  else 
{ 
 obj_t BgL_auxz00_7705;
BgL_auxz00_7705 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41517L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_142); 
FAILURE(BgL_auxz00_7705,BFALSE,BFALSE);} 
BgL_n2z00_3109 = 
(long)CINT(BgL_tmpz00_7702); } 
BgL_test4061z00_7701 = 
(BgL_lenz00_1408>BgL_n2z00_3109); } 
if(BgL_test4061z00_7701)
{ /* Ieee/string.scm 957 */
 long BgL_startz00_3112; long BgL_nz00_3113;
{ /* Ieee/string.scm 957 */
 obj_t BgL_tmpz00_7711;
if(
INTEGERP(BgL_startz00_142))
{ /* Ieee/string.scm 957 */
BgL_tmpz00_7711 = BgL_startz00_142
; }  else 
{ 
 obj_t BgL_auxz00_7714;
BgL_auxz00_7714 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41560L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_142); 
FAILURE(BgL_auxz00_7714,BFALSE,BFALSE);} 
BgL_startz00_3112 = 
(long)CINT(BgL_tmpz00_7711); } 
{ /* Ieee/string.scm 952 */
 bool_t BgL_test4064z00_7719;
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_countz00_143))
{ /* Ieee/string.scm 953 */
 bool_t BgL_test4066z00_7722;
{ /* Ieee/string.scm 953 */
 long BgL_n1z00_3101;
{ /* Ieee/string.scm 953 */
 obj_t BgL_tmpz00_7723;
if(
INTEGERP(BgL_countz00_143))
{ /* Ieee/string.scm 953 */
BgL_tmpz00_7723 = BgL_countz00_143
; }  else 
{ 
 obj_t BgL_auxz00_7726;
BgL_auxz00_7726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41426L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_countz00_143); 
FAILURE(BgL_auxz00_7726,BFALSE,BFALSE);} 
BgL_n1z00_3101 = 
(long)CINT(BgL_tmpz00_7723); } 
BgL_test4066z00_7722 = 
(BgL_n1z00_3101>=0L); } 
if(BgL_test4066z00_7722)
{ /* Ieee/string.scm 953 */
 long BgL_n1z00_3104;
{ /* Ieee/string.scm 953 */
 obj_t BgL_tmpz00_7732;
if(
INTEGERP(BgL_countz00_143))
{ /* Ieee/string.scm 953 */
BgL_tmpz00_7732 = BgL_countz00_143
; }  else 
{ 
 obj_t BgL_auxz00_7735;
BgL_auxz00_7735 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41441L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_countz00_143); 
FAILURE(BgL_auxz00_7735,BFALSE,BFALSE);} 
BgL_n1z00_3104 = 
(long)CINT(BgL_tmpz00_7732); } 
{ /* Ieee/string.scm 953 */
 long BgL_tmpz00_7740;
{ /* Ieee/string.scm 953 */
 long BgL_za72za7_3103;
{ /* Ieee/string.scm 953 */
 obj_t BgL_tmpz00_7741;
if(
INTEGERP(BgL_startz00_142))
{ /* Ieee/string.scm 953 */
BgL_tmpz00_7741 = BgL_startz00_142
; }  else 
{ 
 obj_t BgL_auxz00_7744;
BgL_auxz00_7744 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41456L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_142); 
FAILURE(BgL_auxz00_7744,BFALSE,BFALSE);} 
BgL_za72za7_3103 = 
(long)CINT(BgL_tmpz00_7741); } 
BgL_tmpz00_7740 = 
(BgL_lenz00_1408-BgL_za72za7_3103); } 
BgL_test4064z00_7719 = 
(BgL_n1z00_3104<=BgL_tmpz00_7740); } }  else 
{ /* Ieee/string.scm 953 */
BgL_test4064z00_7719 = ((bool_t)0)
; } }  else 
{ /* Ieee/string.scm 952 */
BgL_test4064z00_7719 = ((bool_t)0)
; } 
if(BgL_test4064z00_7719)
{ /* Ieee/string.scm 954 */
 obj_t BgL_tmpz00_7751;
if(
INTEGERP(BgL_countz00_143))
{ /* Ieee/string.scm 954 */
BgL_tmpz00_7751 = BgL_countz00_143
; }  else 
{ 
 obj_t BgL_auxz00_7754;
BgL_auxz00_7754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41468L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_countz00_143); 
FAILURE(BgL_auxz00_7754,BFALSE,BFALSE);} 
BgL_nz00_3113 = 
(long)CINT(BgL_tmpz00_7751); }  else 
{ /* Ieee/string.scm 955 */
 long BgL_za72za7_3107;
{ /* Ieee/string.scm 955 */
 obj_t BgL_tmpz00_7759;
if(
INTEGERP(BgL_startz00_142))
{ /* Ieee/string.scm 955 */
BgL_tmpz00_7759 = BgL_startz00_142
; }  else 
{ 
 obj_t BgL_auxz00_7762;
BgL_auxz00_7762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41486L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_142); 
FAILURE(BgL_auxz00_7762,BFALSE,BFALSE);} 
BgL_za72za7_3107 = 
(long)CINT(BgL_tmpz00_7759); } 
BgL_nz00_3113 = 
(BgL_lenz00_1408-BgL_za72za7_3107); } } 
{ /* Ieee/string.scm 932 */
 char * BgL_s0z00_3114;
BgL_s0z00_3114 = 
BSTRING_TO_STRING(BgL_stringz00_140); 
{ /* Ieee/string.scm 932 */
 char * BgL_s1z00_3115;
{ /* Ieee/string.scm 933 */
 int BgL_tmpz00_7769;
BgL_tmpz00_7769 = 
(int)(
(
(unsigned char)(BgL_charz00_141))); 
BgL_s1z00_3115 = 
BGL_MEMCHR(BgL_s0z00_3114, BgL_tmpz00_7769, BgL_nz00_3113, BgL_startz00_3112); } 
{ /* Ieee/string.scm 933 */

if(
BGL_MEMCHR_ZERO(BgL_s1z00_3115))
{ /* Ieee/string.scm 934 */
return BFALSE;}  else 
{ /* Ieee/string.scm 934 */
return 
BINT(
BGL_MEMCHR_DIFF(BgL_s1z00_3115, BgL_s0z00_3114));} } } } }  else 
{ /* Ieee/string.scm 956 */
return BFALSE;} } } } } 

}



/* _string-index */
obj_t BGl__stringzd2indexzd2zz__r4_strings_6_7z00(obj_t BgL_env1140z00_150, obj_t BgL_opt1139z00_149)
{
{ /* Ieee/string.scm 962 */
{ /* Ieee/string.scm 962 */
 obj_t BgL_g1141z00_1418; obj_t BgL_g1142z00_1419;
BgL_g1141z00_1418 = 
VECTOR_REF(BgL_opt1139z00_149,0L); 
BgL_g1142z00_1419 = 
VECTOR_REF(BgL_opt1139z00_149,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1139z00_149)) { case 2L : 

{ /* Ieee/string.scm 962 */

{ /* Ieee/string.scm 962 */
 obj_t BgL_auxz00_7780;
if(
STRINGP(BgL_g1141z00_1418))
{ /* Ieee/string.scm 962 */
BgL_auxz00_7780 = BgL_g1141z00_1418
; }  else 
{ 
 obj_t BgL_auxz00_7783;
BgL_auxz00_7783 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41795L), BGl_string3460z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1141z00_1418); 
FAILURE(BgL_auxz00_7783,BFALSE,BFALSE);} 
return 
BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_auxz00_7780, BgL_g1142z00_1419, 
BINT(0L));} } break;case 3L : 

{ /* Ieee/string.scm 962 */
 obj_t BgL_startz00_1423;
BgL_startz00_1423 = 
VECTOR_REF(BgL_opt1139z00_149,2L); 
{ /* Ieee/string.scm 962 */

{ /* Ieee/string.scm 962 */
 obj_t BgL_auxz00_7790;
if(
STRINGP(BgL_g1141z00_1418))
{ /* Ieee/string.scm 962 */
BgL_auxz00_7790 = BgL_g1141z00_1418
; }  else 
{ 
 obj_t BgL_auxz00_7793;
BgL_auxz00_7793 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(41795L), BGl_string3460z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1141z00_1418); 
FAILURE(BgL_auxz00_7793,BFALSE,BFALSE);} 
return 
BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_auxz00_7790, BgL_g1142z00_1419, BgL_startz00_1423);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3458z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1139z00_149)));} } } } 

}



/* string-index */
BGL_EXPORTED_DEF obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_146, obj_t BgL_rsz00_147, obj_t BgL_startz00_148)
{
{ /* Ieee/string.scm 962 */
if(
CHARP(BgL_rsz00_147))
{ /* Ieee/string.scm 224 */

return 
BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(BgL_stringz00_146, 
CCHAR(BgL_rsz00_147), BgL_startz00_148, 
BINT(-1L));}  else 
{ /* Ieee/string.scm 964 */
if(
STRINGP(BgL_rsz00_147))
{ /* Ieee/string.scm 966 */
if(
(
STRING_LENGTH(BgL_rsz00_147)==1L))
{ /* Ieee/string.scm 224 */

{ /* Ieee/string.scm 224 */
 unsigned char BgL_auxz00_7813;
{ /* Ieee/string.scm 331 */
 long BgL_l2385z00_4343;
BgL_l2385z00_4343 = 
STRING_LENGTH(BgL_rsz00_147); 
if(
BOUND_CHECK(0L, BgL_l2385z00_4343))
{ /* Ieee/string.scm 331 */
BgL_auxz00_7813 = 
(char)(
STRING_REF(BgL_rsz00_147, 0L))
; }  else 
{ 
 obj_t BgL_auxz00_7819;
BgL_auxz00_7819 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_rsz00_147, 
(int)(BgL_l2385z00_4343), 
(int)(0L)); 
FAILURE(BgL_auxz00_7819,BFALSE,BFALSE);} } 
return 
BGl_stringzd2charzd2indexz00zz__r4_strings_6_7z00(BgL_stringz00_146, BgL_auxz00_7813, BgL_startz00_148, 
BINT(-1L));} }  else 
{ /* Ieee/string.scm 968 */
if(
(
STRING_LENGTH(BgL_rsz00_147)<=10L))
{ /* Ieee/string.scm 971 */
 long BgL_lenz00_1440; long BgL_lenjz00_1441;
BgL_lenz00_1440 = 
STRING_LENGTH(BgL_stringz00_146); 
BgL_lenjz00_1441 = 
STRING_LENGTH(BgL_rsz00_147); 
{ 
 obj_t BgL_iz00_1443;
BgL_iz00_1443 = BgL_startz00_148; 
BgL_zc3z04anonymousza31546ze3z87_1444:
{ /* Ieee/string.scm 974 */
 bool_t BgL_test4080z00_7832;
{ /* Ieee/string.scm 974 */
 long BgL_n1z00_3126;
{ /* Ieee/string.scm 974 */
 obj_t BgL_tmpz00_7833;
if(
INTEGERP(BgL_iz00_1443))
{ /* Ieee/string.scm 974 */
BgL_tmpz00_7833 = BgL_iz00_1443
; }  else 
{ 
 obj_t BgL_auxz00_7836;
BgL_auxz00_7836 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(42242L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1443); 
FAILURE(BgL_auxz00_7836,BFALSE,BFALSE);} 
BgL_n1z00_3126 = 
(long)CINT(BgL_tmpz00_7833); } 
BgL_test4080z00_7832 = 
(BgL_n1z00_3126>=BgL_lenz00_1440); } 
if(BgL_test4080z00_7832)
{ /* Ieee/string.scm 974 */
return BFALSE;}  else 
{ /* Ieee/string.scm 976 */
 unsigned char BgL_cz00_1446;
{ /* Ieee/string.scm 976 */
 long BgL_kz00_3129;
{ /* Ieee/string.scm 976 */
 obj_t BgL_tmpz00_7842;
if(
INTEGERP(BgL_iz00_1443))
{ /* Ieee/string.scm 976 */
BgL_tmpz00_7842 = BgL_iz00_1443
; }  else 
{ 
 obj_t BgL_auxz00_7845;
BgL_auxz00_7845 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(42286L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1443); 
FAILURE(BgL_auxz00_7845,BFALSE,BFALSE);} 
BgL_kz00_3129 = 
(long)CINT(BgL_tmpz00_7842); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2389z00_4347;
BgL_l2389z00_4347 = 
STRING_LENGTH(BgL_stringz00_146); 
if(
BOUND_CHECK(BgL_kz00_3129, BgL_l2389z00_4347))
{ /* Ieee/string.scm 331 */
BgL_cz00_1446 = 
STRING_REF(BgL_stringz00_146, BgL_kz00_3129); }  else 
{ 
 obj_t BgL_auxz00_7854;
BgL_auxz00_7854 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_146, 
(int)(BgL_l2389z00_4347), 
(int)(BgL_kz00_3129)); 
FAILURE(BgL_auxz00_7854,BFALSE,BFALSE);} } } 
{ 
 long BgL_jz00_1448;
BgL_jz00_1448 = 0L; 
BgL_zc3z04anonymousza31548ze3z87_1449:
if(
(BgL_jz00_1448==BgL_lenjz00_1441))
{ 
 obj_t BgL_iz00_7862;
{ 
 obj_t BgL_tmpz00_7863;
if(
INTEGERP(BgL_iz00_1443))
{ /* Ieee/string.scm 979 */
BgL_tmpz00_7863 = BgL_iz00_1443
; }  else 
{ 
 obj_t BgL_auxz00_7866;
BgL_auxz00_7866 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(42358L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1443); 
FAILURE(BgL_auxz00_7866,BFALSE,BFALSE);} 
BgL_iz00_7862 = 
ADDFX(BgL_tmpz00_7863, 
BINT(1L)); } 
BgL_iz00_1443 = BgL_iz00_7862; 
goto BgL_zc3z04anonymousza31546ze3z87_1444;}  else 
{ /* Ieee/string.scm 980 */
 bool_t BgL_test4086z00_7872;
{ /* Ieee/string.scm 980 */
 unsigned char BgL_tmpz00_7873;
{ /* Ieee/string.scm 343 */
 long BgL_l2393z00_4351;
BgL_l2393z00_4351 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_147)); 
if(
BOUND_CHECK(BgL_jz00_1448, BgL_l2393z00_4351))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_7873 = 
STRING_REF(
((obj_t)BgL_rsz00_147), BgL_jz00_1448)
; }  else 
{ 
 obj_t BgL_auxz00_7880;
BgL_auxz00_7880 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_147), 
(int)(BgL_l2393z00_4351), 
(int)(BgL_jz00_1448)); 
FAILURE(BgL_auxz00_7880,BFALSE,BFALSE);} } 
BgL_test4086z00_7872 = 
(BgL_cz00_1446==BgL_tmpz00_7873); } 
if(BgL_test4086z00_7872)
{ /* Ieee/string.scm 980 */
return BgL_iz00_1443;}  else 
{ 
 long BgL_jz00_7888;
BgL_jz00_7888 = 
(BgL_jz00_1448+1L); 
BgL_jz00_1448 = BgL_jz00_7888; 
goto BgL_zc3z04anonymousza31548ze3z87_1449;} } } } } } }  else 
{ /* Ieee/string.scm 984 */
 obj_t BgL_tz00_1458; long BgL_lenz00_1459;
BgL_tz00_1458 = 
make_string(256L, ((unsigned char)'n')); 
BgL_lenz00_1459 = 
STRING_LENGTH(BgL_stringz00_146); 
{ /* Ieee/string.scm 986 */
 long BgL_g1030z00_1460;
BgL_g1030z00_1460 = 
(
STRING_LENGTH(BgL_rsz00_147)-1L); 
{ 
 long BgL_iz00_1462;
BgL_iz00_1462 = BgL_g1030z00_1460; 
BgL_zc3z04anonymousza31557ze3z87_1463:
if(
(BgL_iz00_1462==-1L))
{ 
 obj_t BgL_iz00_1466;
BgL_iz00_1466 = BgL_startz00_148; 
BgL_zc3z04anonymousza31559ze3z87_1467:
{ /* Ieee/string.scm 990 */
 bool_t BgL_test4089z00_7896;
{ /* Ieee/string.scm 990 */
 long BgL_n1z00_3142;
{ /* Ieee/string.scm 990 */
 obj_t BgL_tmpz00_7897;
if(
INTEGERP(BgL_iz00_1466))
{ /* Ieee/string.scm 990 */
BgL_tmpz00_7897 = BgL_iz00_1466
; }  else 
{ 
 obj_t BgL_auxz00_7900;
BgL_auxz00_7900 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(42660L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1466); 
FAILURE(BgL_auxz00_7900,BFALSE,BFALSE);} 
BgL_n1z00_3142 = 
(long)CINT(BgL_tmpz00_7897); } 
BgL_test4089z00_7896 = 
(BgL_n1z00_3142>=BgL_lenz00_1459); } 
if(BgL_test4089z00_7896)
{ /* Ieee/string.scm 990 */
return BFALSE;}  else 
{ /* Ieee/string.scm 992 */
 bool_t BgL_test4091z00_7906;
{ /* Ieee/string.scm 992 */
 unsigned char BgL_tmpz00_7907;
{ /* Ieee/string.scm 993 */
 long BgL_i2400z00_4358;
{ /* Ieee/string.scm 993 */
 unsigned char BgL_tmpz00_7908;
{ /* Ieee/string.scm 993 */
 long BgL_kz00_3145;
{ /* Ieee/string.scm 993 */
 obj_t BgL_tmpz00_7909;
if(
INTEGERP(BgL_iz00_1466))
{ /* Ieee/string.scm 993 */
BgL_tmpz00_7909 = BgL_iz00_1466
; }  else 
{ 
 obj_t BgL_auxz00_7912;
BgL_auxz00_7912 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(42750L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1466); 
FAILURE(BgL_auxz00_7912,BFALSE,BFALSE);} 
BgL_kz00_3145 = 
(long)CINT(BgL_tmpz00_7909); } 
{ /* Ieee/string.scm 343 */
 long BgL_l2397z00_4355;
BgL_l2397z00_4355 = 
STRING_LENGTH(BgL_stringz00_146); 
if(
BOUND_CHECK(BgL_kz00_3145, BgL_l2397z00_4355))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_7908 = 
STRING_REF(BgL_stringz00_146, BgL_kz00_3145)
; }  else 
{ 
 obj_t BgL_auxz00_7921;
BgL_auxz00_7921 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_146, 
(int)(BgL_l2397z00_4355), 
(int)(BgL_kz00_3145)); 
FAILURE(BgL_auxz00_7921,BFALSE,BFALSE);} } } 
BgL_i2400z00_4358 = 
(BgL_tmpz00_7908); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2401z00_4359;
BgL_l2401z00_4359 = 
STRING_LENGTH(BgL_tz00_1458); 
if(
BOUND_CHECK(BgL_i2400z00_4358, BgL_l2401z00_4359))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_7907 = 
STRING_REF(BgL_tz00_1458, BgL_i2400z00_4358)
; }  else 
{ 
 obj_t BgL_auxz00_7932;
BgL_auxz00_7932 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_tz00_1458, 
(int)(BgL_l2401z00_4359), 
(int)(BgL_i2400z00_4358)); 
FAILURE(BgL_auxz00_7932,BFALSE,BFALSE);} } } 
BgL_test4091z00_7906 = 
(BgL_tmpz00_7907==((unsigned char)'y')); } 
if(BgL_test4091z00_7906)
{ /* Ieee/string.scm 992 */
return BgL_iz00_1466;}  else 
{ 
 obj_t BgL_iz00_7939;
{ 
 obj_t BgL_tmpz00_7940;
if(
INTEGERP(BgL_iz00_1466))
{ /* Ieee/string.scm 997 */
BgL_tmpz00_7940 = BgL_iz00_1466
; }  else 
{ 
 obj_t BgL_auxz00_7943;
BgL_auxz00_7943 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(42801L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1466); 
FAILURE(BgL_auxz00_7943,BFALSE,BFALSE);} 
BgL_iz00_7939 = 
ADDFX(BgL_tmpz00_7940, 
BINT(1L)); } 
BgL_iz00_1466 = BgL_iz00_7939; 
goto BgL_zc3z04anonymousza31559ze3z87_1467;} } } }  else 
{ /* Ieee/string.scm 987 */
{ /* Ieee/string.scm 999 */
 long BgL_arg1576z00_1478;
{ /* Ieee/string.scm 999 */
 unsigned char BgL_tmpz00_7949;
{ /* Ieee/string.scm 343 */
 long BgL_l2405z00_4363;
BgL_l2405z00_4363 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_147)); 
if(
BOUND_CHECK(BgL_iz00_1462, BgL_l2405z00_4363))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_7949 = 
STRING_REF(
((obj_t)BgL_rsz00_147), BgL_iz00_1462)
; }  else 
{ 
 obj_t BgL_auxz00_7956;
BgL_auxz00_7956 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_147), 
(int)(BgL_l2405z00_4363), 
(int)(BgL_iz00_1462)); 
FAILURE(BgL_auxz00_7956,BFALSE,BFALSE);} } 
BgL_arg1576z00_1478 = 
(BgL_tmpz00_7949); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2409z00_4367;
BgL_l2409z00_4367 = 
STRING_LENGTH(BgL_tz00_1458); 
if(
BOUND_CHECK(BgL_arg1576z00_1478, BgL_l2409z00_4367))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_tz00_1458, BgL_arg1576z00_1478, ((unsigned char)'y')); }  else 
{ 
 obj_t BgL_auxz00_7968;
BgL_auxz00_7968 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_tz00_1458, 
(int)(BgL_l2409z00_4367), 
(int)(BgL_arg1576z00_1478)); 
FAILURE(BgL_auxz00_7968,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_7974;
BgL_iz00_7974 = 
(BgL_iz00_1462-1L); 
BgL_iz00_1462 = BgL_iz00_7974; 
goto BgL_zc3z04anonymousza31557ze3z87_1463;} } } } } } }  else 
{ /* Ieee/string.scm 966 */
return 
BGl_errorz00zz__errorz00(BGl_string3459z00zz__r4_strings_6_7z00, BGl_string3462z00zz__r4_strings_6_7z00, BgL_rsz00_147);} } } 

}



/* _string-index-right */
obj_t BGl__stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t BgL_env1146z00_155, obj_t BgL_opt1145z00_154)
{
{ /* Ieee/string.scm 1005 */
{ /* Ieee/string.scm 1005 */
 obj_t BgL_sz00_1485; obj_t BgL_g1147z00_1486;
BgL_sz00_1485 = 
VECTOR_REF(BgL_opt1145z00_154,0L); 
BgL_g1147z00_1486 = 
VECTOR_REF(BgL_opt1145z00_154,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1145z00_154)) { case 2L : 

{ /* Ieee/string.scm 1005 */
 long BgL_startz00_1489;
{ /* Ieee/string.scm 1005 */
 obj_t BgL_stringz00_3159;
if(
STRINGP(BgL_sz00_1485))
{ /* Ieee/string.scm 1005 */
BgL_stringz00_3159 = BgL_sz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7981;
BgL_auxz00_7981 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(43190L), BGl_string3465z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_1485); 
FAILURE(BgL_auxz00_7981,BFALSE,BFALSE);} 
BgL_startz00_1489 = 
STRING_LENGTH(BgL_stringz00_3159); } 
{ /* Ieee/string.scm 1005 */

{ /* Ieee/string.scm 1005 */
 obj_t BgL_auxz00_7986;
if(
STRINGP(BgL_sz00_1485))
{ /* Ieee/string.scm 1005 */
BgL_auxz00_7986 = BgL_sz00_1485
; }  else 
{ 
 obj_t BgL_auxz00_7989;
BgL_auxz00_7989 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(43139L), BGl_string3465z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_1485); 
FAILURE(BgL_auxz00_7989,BFALSE,BFALSE);} 
return 
BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(BgL_auxz00_7986, BgL_g1147z00_1486, 
BINT(BgL_startz00_1489));} } } break;case 3L : 

{ /* Ieee/string.scm 1005 */
 obj_t BgL_startz00_1490;
BgL_startz00_1490 = 
VECTOR_REF(BgL_opt1145z00_154,2L); 
{ /* Ieee/string.scm 1005 */

{ /* Ieee/string.scm 1005 */
 obj_t BgL_auxz00_7996;
if(
STRINGP(BgL_sz00_1485))
{ /* Ieee/string.scm 1005 */
BgL_auxz00_7996 = BgL_sz00_1485
; }  else 
{ 
 obj_t BgL_auxz00_7999;
BgL_auxz00_7999 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(43139L), BGl_string3465z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_1485); 
FAILURE(BgL_auxz00_7999,BFALSE,BFALSE);} 
return 
BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(BgL_auxz00_7996, BgL_g1147z00_1486, BgL_startz00_1490);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3463z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1145z00_154)));} } } } 

}



/* string-index-right */
BGL_EXPORTED_DEF obj_t BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t BgL_sz00_151, obj_t BgL_rsz00_152, obj_t BgL_startz00_153)
{
{ /* Ieee/string.scm 1005 */
{ 
 obj_t BgL_sz00_1550; obj_t BgL_cz00_1551;
{ /* Ieee/string.scm 1016 */
 bool_t BgL_test4101z00_8009;
{ /* Ieee/string.scm 1016 */
 long BgL_n1z00_3168;
{ /* Ieee/string.scm 1016 */
 obj_t BgL_tmpz00_8010;
if(
INTEGERP(BgL_startz00_153))
{ /* Ieee/string.scm 1016 */
BgL_tmpz00_8010 = BgL_startz00_153
; }  else 
{ 
 obj_t BgL_auxz00_8013;
BgL_auxz00_8013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(43420L), BGl_string3464z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_153); 
FAILURE(BgL_auxz00_8013,BFALSE,BFALSE);} 
BgL_n1z00_3168 = 
(long)CINT(BgL_tmpz00_8010); } 
BgL_test4101z00_8009 = 
(BgL_n1z00_3168>
STRING_LENGTH(BgL_sz00_151)); } 
if(BgL_test4101z00_8009)
{ /* Ieee/string.scm 1016 */
return 
BGl_errorz00zz__errorz00(BGl_string3464z00zz__r4_strings_6_7z00, BGl_string3466z00zz__r4_strings_6_7z00, BgL_startz00_153);}  else 
{ /* Ieee/string.scm 1016 */
if(
CHARP(BgL_rsz00_152))
{ /* Ieee/string.scm 1018 */
BgL_sz00_1550 = BgL_sz00_151; 
BgL_cz00_1551 = BgL_rsz00_152; 
BgL_zc3z04anonymousza31626ze3z87_1552:
{ 
 long BgL_iz00_1555;
{ /* Ieee/string.scm 1007 */
 long BgL_za71za7_3160;
{ /* Ieee/string.scm 1007 */
 obj_t BgL_tmpz00_8042;
if(
INTEGERP(BgL_startz00_153))
{ /* Ieee/string.scm 1007 */
BgL_tmpz00_8042 = BgL_startz00_153
; }  else 
{ 
 obj_t BgL_auxz00_8045;
BgL_auxz00_8045 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(43270L), BGl_string3456z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_153); 
FAILURE(BgL_auxz00_8045,BFALSE,BFALSE);} 
BgL_za71za7_3160 = 
(long)CINT(BgL_tmpz00_8042); } 
BgL_iz00_1555 = 
(BgL_za71za7_3160-1L); } 
BgL_zc3z04anonymousza31627ze3z87_1556:
if(
(BgL_iz00_1555<0L))
{ /* Ieee/string.scm 1009 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1011 */
 bool_t BgL_test4105z00_8025;
{ /* Ieee/string.scm 1011 */
 unsigned char BgL_char2z00_3165;
BgL_char2z00_3165 = 
CCHAR(BgL_cz00_1551); 
{ /* Ieee/string.scm 1011 */
 unsigned char BgL_tmpz00_8027;
{ /* Ieee/string.scm 343 */
 long BgL_l2413z00_4371;
BgL_l2413z00_4371 = 
STRING_LENGTH(BgL_sz00_1550); 
if(
BOUND_CHECK(BgL_iz00_1555, BgL_l2413z00_4371))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8027 = 
STRING_REF(BgL_sz00_1550, BgL_iz00_1555)
; }  else 
{ 
 obj_t BgL_auxz00_8032;
BgL_auxz00_8032 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_1550, 
(int)(BgL_l2413z00_4371), 
(int)(BgL_iz00_1555)); 
FAILURE(BgL_auxz00_8032,BFALSE,BFALSE);} } 
BgL_test4105z00_8025 = 
(BgL_tmpz00_8027==BgL_char2z00_3165); } } 
if(BgL_test4105z00_8025)
{ /* Ieee/string.scm 1011 */
return 
BINT(BgL_iz00_1555);}  else 
{ 
 long BgL_iz00_8040;
BgL_iz00_8040 = 
(BgL_iz00_1555-1L); 
BgL_iz00_1555 = BgL_iz00_8040; 
goto BgL_zc3z04anonymousza31627ze3z87_1556;} } } }  else 
{ /* Ieee/string.scm 1018 */
if(
STRINGP(BgL_rsz00_152))
{ /* Ieee/string.scm 1020 */
if(
(
STRING_LENGTH(BgL_rsz00_152)==1L))
{ 
 obj_t BgL_cz00_8057; obj_t BgL_sz00_8056;
BgL_sz00_8056 = BgL_sz00_151; 
{ /* Ieee/string.scm 331 */
 long BgL_l2417z00_4375;
BgL_l2417z00_4375 = 
STRING_LENGTH(BgL_rsz00_152); 
if(
BOUND_CHECK(0L, BgL_l2417z00_4375))
{ /* Ieee/string.scm 331 */
BgL_cz00_8057 = 
BCHAR(
STRING_REF(BgL_rsz00_152, 0L)); }  else 
{ 
 obj_t BgL_auxz00_8063;
BgL_auxz00_8063 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_rsz00_152, 
(int)(BgL_l2417z00_4375), 
(int)(0L)); 
FAILURE(BgL_auxz00_8063,BFALSE,BFALSE);} } 
BgL_cz00_1551 = BgL_cz00_8057; 
BgL_sz00_1550 = BgL_sz00_8056; 
goto BgL_zc3z04anonymousza31626ze3z87_1552;}  else 
{ /* Ieee/string.scm 1022 */
if(
(
STRING_LENGTH(BgL_rsz00_152)<=10L))
{ /* Ieee/string.scm 1025 */
 long BgL_lenjz00_1503;
BgL_lenjz00_1503 = 
STRING_LENGTH(BgL_rsz00_152); 
{ 
 long BgL_iz00_1506;
{ /* Ieee/string.scm 1027 */
 long BgL_za71za7_3177;
{ /* Ieee/string.scm 1027 */
 obj_t BgL_tmpz00_8108;
if(
INTEGERP(BgL_startz00_153))
{ /* Ieee/string.scm 1027 */
BgL_tmpz00_8108 = BgL_startz00_153
; }  else 
{ 
 obj_t BgL_auxz00_8111;
BgL_auxz00_8111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(43853L), BGl_string3464z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_153); 
FAILURE(BgL_auxz00_8111,BFALSE,BFALSE);} 
BgL_za71za7_3177 = 
(long)CINT(BgL_tmpz00_8108); } 
BgL_iz00_1506 = 
(BgL_za71za7_3177-1L); } 
BgL_zc3z04anonymousza31596ze3z87_1507:
if(
(BgL_iz00_1506<0L))
{ /* Ieee/string.scm 1028 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1030 */
 unsigned char BgL_cz00_1509;
{ /* Ieee/string.scm 331 */
 long BgL_l2421z00_4379;
BgL_l2421z00_4379 = 
STRING_LENGTH(BgL_sz00_151); 
if(
BOUND_CHECK(BgL_iz00_1506, BgL_l2421z00_4379))
{ /* Ieee/string.scm 331 */
BgL_cz00_1509 = 
STRING_REF(BgL_sz00_151, BgL_iz00_1506); }  else 
{ 
 obj_t BgL_auxz00_8079;
BgL_auxz00_8079 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_151, 
(int)(BgL_l2421z00_4379), 
(int)(BgL_iz00_1506)); 
FAILURE(BgL_auxz00_8079,BFALSE,BFALSE);} } 
{ 
 long BgL_jz00_1511;
BgL_jz00_1511 = 0L; 
BgL_zc3z04anonymousza31598ze3z87_1512:
if(
(BgL_jz00_1511==BgL_lenjz00_1503))
{ 
 long BgL_iz00_8087;
BgL_iz00_8087 = 
(BgL_iz00_1506-1L); 
BgL_iz00_1506 = BgL_iz00_8087; 
goto BgL_zc3z04anonymousza31596ze3z87_1507;}  else 
{ /* Ieee/string.scm 1034 */
 bool_t BgL_test4115z00_8089;
{ /* Ieee/string.scm 1034 */
 unsigned char BgL_tmpz00_8090;
{ /* Ieee/string.scm 343 */
 long BgL_l2425z00_4383;
BgL_l2425z00_4383 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_152)); 
if(
BOUND_CHECK(BgL_jz00_1511, BgL_l2425z00_4383))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8090 = 
STRING_REF(
((obj_t)BgL_rsz00_152), BgL_jz00_1511)
; }  else 
{ 
 obj_t BgL_auxz00_8097;
BgL_auxz00_8097 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_152), 
(int)(BgL_l2425z00_4383), 
(int)(BgL_jz00_1511)); 
FAILURE(BgL_auxz00_8097,BFALSE,BFALSE);} } 
BgL_test4115z00_8089 = 
(BgL_cz00_1509==BgL_tmpz00_8090); } 
if(BgL_test4115z00_8089)
{ /* Ieee/string.scm 1034 */
return 
BINT(BgL_iz00_1506);}  else 
{ 
 long BgL_jz00_8106;
BgL_jz00_8106 = 
(BgL_jz00_1511+1L); 
BgL_jz00_1511 = BgL_jz00_8106; 
goto BgL_zc3z04anonymousza31598ze3z87_1512;} } } } } }  else 
{ /* Ieee/string.scm 1038 */
 obj_t BgL_tz00_1521;
BgL_tz00_1521 = 
make_string(256L, ((unsigned char)'n')); 
{ /* Ieee/string.scm 1040 */
 long BgL_g1034z00_1523;
BgL_g1034z00_1523 = 
(
STRING_LENGTH(BgL_rsz00_152)-1L); 
{ 
 long BgL_iz00_1525;
BgL_iz00_1525 = BgL_g1034z00_1523; 
BgL_zc3z04anonymousza31607ze3z87_1526:
if(
(BgL_iz00_1525==-1L))
{ 
 long BgL_iz00_1530;
{ /* Ieee/string.scm 1042 */
 long BgL_za71za7_3193;
{ /* Ieee/string.scm 1042 */
 obj_t BgL_tmpz00_8152;
if(
INTEGERP(BgL_startz00_153))
{ /* Ieee/string.scm 1042 */
BgL_tmpz00_8152 = BgL_startz00_153
; }  else 
{ 
 obj_t BgL_auxz00_8155;
BgL_auxz00_8155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(44254L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_153); 
FAILURE(BgL_auxz00_8155,BFALSE,BFALSE);} 
BgL_za71za7_3193 = 
(long)CINT(BgL_tmpz00_8152); } 
BgL_iz00_1530 = 
(BgL_za71za7_3193-1L); } 
BgL_zc3z04anonymousza31609ze3z87_1531:
if(
(BgL_iz00_1530<0L))
{ /* Ieee/string.scm 1044 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1046 */
 bool_t BgL_test4120z00_8124;
{ /* Ieee/string.scm 1046 */
 unsigned char BgL_tmpz00_8125;
{ /* Ieee/string.scm 1047 */
 long BgL_i2432z00_4390;
{ /* Ieee/string.scm 1047 */
 unsigned char BgL_tmpz00_8126;
{ /* Ieee/string.scm 343 */
 long BgL_l2429z00_4387;
BgL_l2429z00_4387 = 
STRING_LENGTH(BgL_sz00_151); 
if(
BOUND_CHECK(BgL_iz00_1530, BgL_l2429z00_4387))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8126 = 
STRING_REF(BgL_sz00_151, BgL_iz00_1530)
; }  else 
{ 
 obj_t BgL_auxz00_8131;
BgL_auxz00_8131 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_151, 
(int)(BgL_l2429z00_4387), 
(int)(BgL_iz00_1530)); 
FAILURE(BgL_auxz00_8131,BFALSE,BFALSE);} } 
BgL_i2432z00_4390 = 
(BgL_tmpz00_8126); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2433z00_4391;
BgL_l2433z00_4391 = 
STRING_LENGTH(BgL_tz00_1521); 
if(
BOUND_CHECK(BgL_i2432z00_4390, BgL_l2433z00_4391))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_8125 = 
STRING_REF(BgL_tz00_1521, BgL_i2432z00_4390)
; }  else 
{ 
 obj_t BgL_auxz00_8142;
BgL_auxz00_8142 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_tz00_1521, 
(int)(BgL_l2433z00_4391), 
(int)(BgL_i2432z00_4390)); 
FAILURE(BgL_auxz00_8142,BFALSE,BFALSE);} } } 
BgL_test4120z00_8124 = 
(BgL_tmpz00_8125==((unsigned char)'y')); } 
if(BgL_test4120z00_8124)
{ /* Ieee/string.scm 1046 */
return 
BINT(BgL_iz00_1530);}  else 
{ 
 long BgL_iz00_8150;
BgL_iz00_8150 = 
(BgL_iz00_1530-1L); 
BgL_iz00_1530 = BgL_iz00_8150; 
goto BgL_zc3z04anonymousza31609ze3z87_1531;} } }  else 
{ /* Ieee/string.scm 1041 */
{ /* Ieee/string.scm 1053 */
 long BgL_arg1619z00_1542;
{ /* Ieee/string.scm 1053 */
 unsigned char BgL_tmpz00_8161;
{ /* Ieee/string.scm 343 */
 long BgL_l2437z00_4395;
BgL_l2437z00_4395 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_152)); 
if(
BOUND_CHECK(BgL_iz00_1525, BgL_l2437z00_4395))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8161 = 
STRING_REF(
((obj_t)BgL_rsz00_152), BgL_iz00_1525)
; }  else 
{ 
 obj_t BgL_auxz00_8168;
BgL_auxz00_8168 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_152), 
(int)(BgL_l2437z00_4395), 
(int)(BgL_iz00_1525)); 
FAILURE(BgL_auxz00_8168,BFALSE,BFALSE);} } 
BgL_arg1619z00_1542 = 
(BgL_tmpz00_8161); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2441z00_4399;
BgL_l2441z00_4399 = 
STRING_LENGTH(BgL_tz00_1521); 
if(
BOUND_CHECK(BgL_arg1619z00_1542, BgL_l2441z00_4399))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_tz00_1521, BgL_arg1619z00_1542, ((unsigned char)'y')); }  else 
{ 
 obj_t BgL_auxz00_8180;
BgL_auxz00_8180 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_tz00_1521, 
(int)(BgL_l2441z00_4399), 
(int)(BgL_arg1619z00_1542)); 
FAILURE(BgL_auxz00_8180,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_8186;
BgL_iz00_8186 = 
(BgL_iz00_1525-1L); 
BgL_iz00_1525 = BgL_iz00_8186; 
goto BgL_zc3z04anonymousza31607ze3z87_1526;} } } } } } }  else 
{ /* Ieee/string.scm 1020 */
return 
BGl_errorz00zz__errorz00(BGl_string3464z00zz__r4_strings_6_7z00, BGl_string3462z00zz__r4_strings_6_7z00, BgL_rsz00_152);} } } } } } 

}



/* _string-skip */
obj_t BGl__stringzd2skipzd2zz__r4_strings_6_7z00(obj_t BgL_env1151z00_160, obj_t BgL_opt1150z00_159)
{
{ /* Ieee/string.scm 1059 */
{ /* Ieee/string.scm 1059 */
 obj_t BgL_g1152z00_1564; obj_t BgL_g1153z00_1565;
BgL_g1152z00_1564 = 
VECTOR_REF(BgL_opt1150z00_159,0L); 
BgL_g1153z00_1565 = 
VECTOR_REF(BgL_opt1150z00_159,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1150z00_159)) { case 2L : 

{ /* Ieee/string.scm 1059 */

{ /* Ieee/string.scm 1059 */
 obj_t BgL_auxz00_8191;
if(
STRINGP(BgL_g1152z00_1564))
{ /* Ieee/string.scm 1059 */
BgL_auxz00_8191 = BgL_g1152z00_1564
; }  else 
{ 
 obj_t BgL_auxz00_8194;
BgL_auxz00_8194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(44760L), BGl_string3469z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1152z00_1564); 
FAILURE(BgL_auxz00_8194,BFALSE,BFALSE);} 
return 
BGl_stringzd2skipzd2zz__r4_strings_6_7z00(BgL_auxz00_8191, BgL_g1153z00_1565, 
BINT(0L));} } break;case 3L : 

{ /* Ieee/string.scm 1059 */
 obj_t BgL_startz00_1569;
BgL_startz00_1569 = 
VECTOR_REF(BgL_opt1150z00_159,2L); 
{ /* Ieee/string.scm 1059 */

{ /* Ieee/string.scm 1059 */
 obj_t BgL_auxz00_8201;
if(
STRINGP(BgL_g1152z00_1564))
{ /* Ieee/string.scm 1059 */
BgL_auxz00_8201 = BgL_g1152z00_1564
; }  else 
{ 
 obj_t BgL_auxz00_8204;
BgL_auxz00_8204 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(44760L), BGl_string3469z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1152z00_1564); 
FAILURE(BgL_auxz00_8204,BFALSE,BFALSE);} 
return 
BGl_stringzd2skipzd2zz__r4_strings_6_7z00(BgL_auxz00_8201, BgL_g1153z00_1565, BgL_startz00_1569);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3467z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1150z00_159)));} } } } 

}



/* string-skip */
BGL_EXPORTED_DEF obj_t BGl_stringzd2skipzd2zz__r4_strings_6_7z00(obj_t BgL_stringz00_156, obj_t BgL_rsz00_157, obj_t BgL_startz00_158)
{
{ /* Ieee/string.scm 1059 */
{ 
 obj_t BgL_sz00_1639; obj_t BgL_predz00_1640; obj_t BgL_sz00_1626; obj_t BgL_cz00_1627;
if(
CHARP(BgL_rsz00_157))
{ /* Ieee/string.scm 1083 */
BgL_sz00_1626 = BgL_stringz00_156; 
BgL_cz00_1627 = BgL_rsz00_157; 
BgL_zc3z04anonymousza31686ze3z87_1628:
{ /* Ieee/string.scm 1062 */
 long BgL_lenz00_1629;
BgL_lenz00_1629 = 
STRING_LENGTH(BgL_sz00_1626); 
{ 
 obj_t BgL_iz00_1631;
BgL_iz00_1631 = BgL_startz00_158; 
BgL_zc3z04anonymousza31687ze3z87_1632:
{ /* Ieee/string.scm 1065 */
 bool_t BgL_test4129z00_8217;
{ /* Ieee/string.scm 1065 */
 long BgL_n1z00_3211;
{ /* Ieee/string.scm 1065 */
 obj_t BgL_tmpz00_8218;
if(
INTEGERP(BgL_iz00_1631))
{ /* Ieee/string.scm 1065 */
BgL_tmpz00_8218 = BgL_iz00_1631
; }  else 
{ 
 obj_t BgL_auxz00_8221;
BgL_auxz00_8221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(44938L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1631); 
FAILURE(BgL_auxz00_8221,BFALSE,BFALSE);} 
BgL_n1z00_3211 = 
(long)CINT(BgL_tmpz00_8218); } 
BgL_test4129z00_8217 = 
(BgL_n1z00_3211>=BgL_lenz00_1629); } 
if(BgL_test4129z00_8217)
{ /* Ieee/string.scm 1065 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1067 */
 bool_t BgL_test4131z00_8227;
{ /* Ieee/string.scm 1067 */
 unsigned char BgL_char2z00_3216;
BgL_char2z00_3216 = 
CCHAR(BgL_cz00_1627); 
{ /* Ieee/string.scm 1067 */
 unsigned char BgL_tmpz00_8229;
{ /* Ieee/string.scm 1067 */
 long BgL_kz00_3214;
{ /* Ieee/string.scm 1067 */
 obj_t BgL_tmpz00_8230;
if(
INTEGERP(BgL_iz00_1631))
{ /* Ieee/string.scm 1067 */
BgL_tmpz00_8230 = BgL_iz00_1631
; }  else 
{ 
 obj_t BgL_auxz00_8233;
BgL_auxz00_8233 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(44985L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1631); 
FAILURE(BgL_auxz00_8233,BFALSE,BFALSE);} 
BgL_kz00_3214 = 
(long)CINT(BgL_tmpz00_8230); } 
{ /* Ieee/string.scm 343 */
 long BgL_l2445z00_4403;
BgL_l2445z00_4403 = 
STRING_LENGTH(BgL_sz00_1626); 
if(
BOUND_CHECK(BgL_kz00_3214, BgL_l2445z00_4403))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8229 = 
STRING_REF(BgL_sz00_1626, BgL_kz00_3214)
; }  else 
{ 
 obj_t BgL_auxz00_8242;
BgL_auxz00_8242 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_1626, 
(int)(BgL_l2445z00_4403), 
(int)(BgL_kz00_3214)); 
FAILURE(BgL_auxz00_8242,BFALSE,BFALSE);} } } 
BgL_test4131z00_8227 = 
(BgL_tmpz00_8229==BgL_char2z00_3216); } } 
if(BgL_test4131z00_8227)
{ 
 obj_t BgL_iz00_8249;
{ 
 obj_t BgL_tmpz00_8250;
if(
INTEGERP(BgL_iz00_1631))
{ /* Ieee/string.scm 1068 */
BgL_tmpz00_8250 = BgL_iz00_1631
; }  else 
{ 
 obj_t BgL_auxz00_8253;
BgL_auxz00_8253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45004L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1631); 
FAILURE(BgL_auxz00_8253,BFALSE,BFALSE);} 
BgL_iz00_8249 = 
ADDFX(BgL_tmpz00_8250, 
BINT(1L)); } 
BgL_iz00_1631 = BgL_iz00_8249; 
goto BgL_zc3z04anonymousza31687ze3z87_1632;}  else 
{ /* Ieee/string.scm 1067 */
return BgL_iz00_1631;} } } } } }  else 
{ /* Ieee/string.scm 1083 */
if(
PROCEDUREP(BgL_rsz00_157))
{ /* Ieee/string.scm 1085 */
BgL_sz00_1639 = BgL_stringz00_156; 
BgL_predz00_1640 = BgL_rsz00_157; 
{ /* Ieee/string.scm 1072 */
 long BgL_lenz00_1642;
BgL_lenz00_1642 = 
STRING_LENGTH(BgL_sz00_1639); 
{ 
 obj_t BgL_iz00_1644;
BgL_iz00_1644 = BgL_startz00_158; 
BgL_zc3z04anonymousza31694ze3z87_1645:
{ /* Ieee/string.scm 1075 */
 bool_t BgL_test4136z00_8262;
{ /* Ieee/string.scm 1075 */
 long BgL_n1z00_3219;
{ /* Ieee/string.scm 1075 */
 obj_t BgL_tmpz00_8263;
if(
INTEGERP(BgL_iz00_1644))
{ /* Ieee/string.scm 1075 */
BgL_tmpz00_8263 = BgL_iz00_1644
; }  else 
{ 
 obj_t BgL_auxz00_8266;
BgL_auxz00_8266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45158L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1644); 
FAILURE(BgL_auxz00_8266,BFALSE,BFALSE);} 
BgL_n1z00_3219 = 
(long)CINT(BgL_tmpz00_8263); } 
BgL_test4136z00_8262 = 
(BgL_n1z00_3219>=BgL_lenz00_1642); } 
if(BgL_test4136z00_8262)
{ /* Ieee/string.scm 1075 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1077 */
 bool_t BgL_test4138z00_8272;
{ /* Ieee/string.scm 1077 */
 unsigned char BgL_arg1702z00_1650;
{ /* Ieee/string.scm 1077 */
 long BgL_kz00_3222;
{ /* Ieee/string.scm 1077 */
 obj_t BgL_tmpz00_8273;
if(
INTEGERP(BgL_iz00_1644))
{ /* Ieee/string.scm 1077 */
BgL_tmpz00_8273 = BgL_iz00_1644
; }  else 
{ 
 obj_t BgL_auxz00_8276;
BgL_auxz00_8276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45203L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1644); 
FAILURE(BgL_auxz00_8276,BFALSE,BFALSE);} 
BgL_kz00_3222 = 
(long)CINT(BgL_tmpz00_8273); } 
{ /* Ieee/string.scm 343 */
 long BgL_l2449z00_4407;
BgL_l2449z00_4407 = 
STRING_LENGTH(BgL_sz00_1639); 
if(
BOUND_CHECK(BgL_kz00_3222, BgL_l2449z00_4407))
{ /* Ieee/string.scm 343 */
BgL_arg1702z00_1650 = 
STRING_REF(BgL_sz00_1639, BgL_kz00_3222); }  else 
{ 
 obj_t BgL_auxz00_8285;
BgL_auxz00_8285 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_1639, 
(int)(BgL_l2449z00_4407), 
(int)(BgL_kz00_3222)); 
FAILURE(BgL_auxz00_8285,BFALSE,BFALSE);} } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_1640, 1))
{ /* Ieee/string.scm 1077 */
BgL_test4138z00_8272 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_1640, 
BCHAR(BgL_arg1702z00_1650)))
; }  else 
{ /* Ieee/string.scm 1077 */
FAILURE(BGl_string3438z00zz__r4_strings_6_7z00,BGl_list3470z00zz__r4_strings_6_7z00,BgL_predz00_1640);} } 
if(BgL_test4138z00_8272)
{ /* Ieee/string.scm 1078 */
 long BgL_arg1701z00_1649;
{ /* Ieee/string.scm 1078 */
 long BgL_za71za7_3223;
{ /* Ieee/string.scm 1078 */
 obj_t BgL_tmpz00_8300;
if(
INTEGERP(BgL_iz00_1644))
{ /* Ieee/string.scm 1078 */
BgL_tmpz00_8300 = BgL_iz00_1644
; }  else 
{ 
 obj_t BgL_auxz00_8303;
BgL_auxz00_8303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45220L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1644); 
FAILURE(BgL_auxz00_8303,BFALSE,BFALSE);} 
BgL_za71za7_3223 = 
(long)CINT(BgL_tmpz00_8300); } 
BgL_arg1701z00_1649 = 
(BgL_za71za7_3223+1L); } 
{ 
 obj_t BgL_iz00_8309;
BgL_iz00_8309 = 
BINT(BgL_arg1701z00_1649); 
BgL_iz00_1644 = BgL_iz00_8309; 
goto BgL_zc3z04anonymousza31694ze3z87_1645;} }  else 
{ /* Ieee/string.scm 1077 */
return BgL_iz00_1644;} } } } } }  else 
{ /* Ieee/string.scm 1085 */
if(
STRINGP(BgL_rsz00_157))
{ /* Ieee/string.scm 1087 */
if(
(
STRING_LENGTH(BgL_rsz00_157)==1L))
{ 
 obj_t BgL_cz00_8317; obj_t BgL_sz00_8316;
BgL_sz00_8316 = BgL_stringz00_156; 
{ /* Ieee/string.scm 331 */
 long BgL_l2453z00_4411;
BgL_l2453z00_4411 = 
STRING_LENGTH(BgL_rsz00_157); 
if(
BOUND_CHECK(0L, BgL_l2453z00_4411))
{ /* Ieee/string.scm 331 */
BgL_cz00_8317 = 
BCHAR(
STRING_REF(BgL_rsz00_157, 0L)); }  else 
{ 
 obj_t BgL_auxz00_8323;
BgL_auxz00_8323 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_rsz00_157, 
(int)(BgL_l2453z00_4411), 
(int)(0L)); 
FAILURE(BgL_auxz00_8323,BFALSE,BFALSE);} } 
BgL_cz00_1627 = BgL_cz00_8317; 
BgL_sz00_1626 = BgL_sz00_8316; 
goto BgL_zc3z04anonymousza31686ze3z87_1628;}  else 
{ /* Ieee/string.scm 1089 */
if(
(
STRING_LENGTH(BgL_rsz00_157)<=10L))
{ /* Ieee/string.scm 1092 */
 long BgL_lenz00_1581; long BgL_lenjz00_1582;
BgL_lenz00_1581 = 
STRING_LENGTH(BgL_stringz00_156); 
BgL_lenjz00_1582 = 
STRING_LENGTH(BgL_rsz00_157); 
{ 
 obj_t BgL_iz00_1584;
BgL_iz00_1584 = BgL_startz00_158; 
BgL_zc3z04anonymousza31645ze3z87_1585:
{ /* Ieee/string.scm 1095 */
 bool_t BgL_test4147z00_8334;
{ /* Ieee/string.scm 1095 */
 long BgL_n1z00_3231;
{ /* Ieee/string.scm 1095 */
 obj_t BgL_tmpz00_8335;
if(
INTEGERP(BgL_iz00_1584))
{ /* Ieee/string.scm 1095 */
BgL_tmpz00_8335 = BgL_iz00_1584
; }  else 
{ 
 obj_t BgL_auxz00_8338;
BgL_auxz00_8338 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45692L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1584); 
FAILURE(BgL_auxz00_8338,BFALSE,BFALSE);} 
BgL_n1z00_3231 = 
(long)CINT(BgL_tmpz00_8335); } 
BgL_test4147z00_8334 = 
(BgL_n1z00_3231>=BgL_lenz00_1581); } 
if(BgL_test4147z00_8334)
{ /* Ieee/string.scm 1095 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1097 */
 unsigned char BgL_cz00_1587;
{ /* Ieee/string.scm 1097 */
 long BgL_kz00_3234;
{ /* Ieee/string.scm 1097 */
 obj_t BgL_tmpz00_8344;
if(
INTEGERP(BgL_iz00_1584))
{ /* Ieee/string.scm 1097 */
BgL_tmpz00_8344 = BgL_iz00_1584
; }  else 
{ 
 obj_t BgL_auxz00_8347;
BgL_auxz00_8347 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45736L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1584); 
FAILURE(BgL_auxz00_8347,BFALSE,BFALSE);} 
BgL_kz00_3234 = 
(long)CINT(BgL_tmpz00_8344); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2457z00_4415;
BgL_l2457z00_4415 = 
STRING_LENGTH(BgL_stringz00_156); 
if(
BOUND_CHECK(BgL_kz00_3234, BgL_l2457z00_4415))
{ /* Ieee/string.scm 331 */
BgL_cz00_1587 = 
STRING_REF(BgL_stringz00_156, BgL_kz00_3234); }  else 
{ 
 obj_t BgL_auxz00_8356;
BgL_auxz00_8356 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_156, 
(int)(BgL_l2457z00_4415), 
(int)(BgL_kz00_3234)); 
FAILURE(BgL_auxz00_8356,BFALSE,BFALSE);} } } 
{ 
 long BgL_jz00_1589;
BgL_jz00_1589 = 0L; 
BgL_zc3z04anonymousza31647ze3z87_1590:
if(
(BgL_jz00_1589==BgL_lenjz00_1582))
{ /* Ieee/string.scm 1099 */
return BgL_iz00_1584;}  else 
{ /* Ieee/string.scm 1101 */
 bool_t BgL_test4152z00_8364;
{ /* Ieee/string.scm 1101 */
 unsigned char BgL_tmpz00_8365;
{ /* Ieee/string.scm 343 */
 long BgL_l2461z00_4419;
BgL_l2461z00_4419 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_157)); 
if(
BOUND_CHECK(BgL_jz00_1589, BgL_l2461z00_4419))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8365 = 
STRING_REF(
((obj_t)BgL_rsz00_157), BgL_jz00_1589)
; }  else 
{ 
 obj_t BgL_auxz00_8372;
BgL_auxz00_8372 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_157), 
(int)(BgL_l2461z00_4419), 
(int)(BgL_jz00_1589)); 
FAILURE(BgL_auxz00_8372,BFALSE,BFALSE);} } 
BgL_test4152z00_8364 = 
(BgL_cz00_1587==BgL_tmpz00_8365); } 
if(BgL_test4152z00_8364)
{ 
 obj_t BgL_iz00_8380;
{ 
 obj_t BgL_tmpz00_8381;
if(
INTEGERP(BgL_iz00_1584))
{ /* Ieee/string.scm 1102 */
BgL_tmpz00_8381 = BgL_iz00_1584
; }  else 
{ 
 obj_t BgL_auxz00_8384;
BgL_auxz00_8384 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(45862L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1584); 
FAILURE(BgL_auxz00_8384,BFALSE,BFALSE);} 
BgL_iz00_8380 = 
ADDFX(BgL_tmpz00_8381, 
BINT(1L)); } 
BgL_iz00_1584 = BgL_iz00_8380; 
goto BgL_zc3z04anonymousza31645ze3z87_1585;}  else 
{ 
 long BgL_jz00_8390;
BgL_jz00_8390 = 
(BgL_jz00_1589+1L); 
BgL_jz00_1589 = BgL_jz00_8390; 
goto BgL_zc3z04anonymousza31647ze3z87_1590;} } } } } } }  else 
{ /* Ieee/string.scm 1105 */
 obj_t BgL_tz00_1599; long BgL_lenz00_1600;
BgL_tz00_1599 = 
make_string(256L, ((unsigned char)'n')); 
BgL_lenz00_1600 = 
STRING_LENGTH(BgL_stringz00_156); 
{ /* Ieee/string.scm 1107 */
 long BgL_g1037z00_1601;
BgL_g1037z00_1601 = 
(
STRING_LENGTH(BgL_rsz00_157)-1L); 
{ 
 long BgL_iz00_1603;
BgL_iz00_1603 = BgL_g1037z00_1601; 
BgL_zc3z04anonymousza31654ze3z87_1604:
if(
(BgL_iz00_1603==-1L))
{ 
 obj_t BgL_iz00_1607;
BgL_iz00_1607 = BgL_startz00_158; 
BgL_zc3z04anonymousza31656ze3z87_1608:
{ /* Ieee/string.scm 1111 */
 bool_t BgL_test4156z00_8398;
{ /* Ieee/string.scm 1111 */
 long BgL_n1z00_3247;
{ /* Ieee/string.scm 1111 */
 obj_t BgL_tmpz00_8399;
if(
INTEGERP(BgL_iz00_1607))
{ /* Ieee/string.scm 1111 */
BgL_tmpz00_8399 = BgL_iz00_1607
; }  else 
{ 
 obj_t BgL_auxz00_8402;
BgL_auxz00_8402 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46110L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1607); 
FAILURE(BgL_auxz00_8402,BFALSE,BFALSE);} 
BgL_n1z00_3247 = 
(long)CINT(BgL_tmpz00_8399); } 
BgL_test4156z00_8398 = 
(BgL_n1z00_3247>=BgL_lenz00_1600); } 
if(BgL_test4156z00_8398)
{ /* Ieee/string.scm 1111 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1113 */
 bool_t BgL_test4158z00_8408;
{ /* Ieee/string.scm 1113 */
 unsigned char BgL_tmpz00_8409;
{ /* Ieee/string.scm 1114 */
 long BgL_i2468z00_4426;
{ /* Ieee/string.scm 1114 */
 unsigned char BgL_tmpz00_8410;
{ /* Ieee/string.scm 1114 */
 long BgL_kz00_3250;
{ /* Ieee/string.scm 1114 */
 obj_t BgL_tmpz00_8411;
if(
INTEGERP(BgL_iz00_1607))
{ /* Ieee/string.scm 1114 */
BgL_tmpz00_8411 = BgL_iz00_1607
; }  else 
{ 
 obj_t BgL_auxz00_8414;
BgL_auxz00_8414 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46198L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1607); 
FAILURE(BgL_auxz00_8414,BFALSE,BFALSE);} 
BgL_kz00_3250 = 
(long)CINT(BgL_tmpz00_8411); } 
{ /* Ieee/string.scm 343 */
 long BgL_l2465z00_4423;
BgL_l2465z00_4423 = 
STRING_LENGTH(BgL_stringz00_156); 
if(
BOUND_CHECK(BgL_kz00_3250, BgL_l2465z00_4423))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8410 = 
STRING_REF(BgL_stringz00_156, BgL_kz00_3250)
; }  else 
{ 
 obj_t BgL_auxz00_8423;
BgL_auxz00_8423 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_stringz00_156, 
(int)(BgL_l2465z00_4423), 
(int)(BgL_kz00_3250)); 
FAILURE(BgL_auxz00_8423,BFALSE,BFALSE);} } } 
BgL_i2468z00_4426 = 
(BgL_tmpz00_8410); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2469z00_4427;
BgL_l2469z00_4427 = 
STRING_LENGTH(BgL_tz00_1599); 
if(
BOUND_CHECK(BgL_i2468z00_4426, BgL_l2469z00_4427))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_8409 = 
STRING_REF(BgL_tz00_1599, BgL_i2468z00_4426)
; }  else 
{ 
 obj_t BgL_auxz00_8434;
BgL_auxz00_8434 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_tz00_1599, 
(int)(BgL_l2469z00_4427), 
(int)(BgL_i2468z00_4426)); 
FAILURE(BgL_auxz00_8434,BFALSE,BFALSE);} } } 
BgL_test4158z00_8408 = 
(BgL_tmpz00_8409==((unsigned char)'y')); } 
if(BgL_test4158z00_8408)
{ 
 obj_t BgL_iz00_8441;
{ 
 obj_t BgL_tmpz00_8442;
if(
INTEGERP(BgL_iz00_1607))
{ /* Ieee/string.scm 1116 */
BgL_tmpz00_8442 = BgL_iz00_1607
; }  else 
{ 
 obj_t BgL_auxz00_8445;
BgL_auxz00_8445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46226L), BGl_string3461z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_1607); 
FAILURE(BgL_auxz00_8445,BFALSE,BFALSE);} 
BgL_iz00_8441 = 
ADDFX(BgL_tmpz00_8442, 
BINT(1L)); } 
BgL_iz00_1607 = BgL_iz00_8441; 
goto BgL_zc3z04anonymousza31656ze3z87_1608;}  else 
{ /* Ieee/string.scm 1113 */
return BgL_iz00_1607;} } } }  else 
{ /* Ieee/string.scm 1108 */
{ /* Ieee/string.scm 1120 */
 long BgL_arg1675z00_1619;
{ /* Ieee/string.scm 1120 */
 unsigned char BgL_tmpz00_8451;
{ /* Ieee/string.scm 343 */
 long BgL_l2473z00_4431;
BgL_l2473z00_4431 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_157)); 
if(
BOUND_CHECK(BgL_iz00_1603, BgL_l2473z00_4431))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8451 = 
STRING_REF(
((obj_t)BgL_rsz00_157), BgL_iz00_1603)
; }  else 
{ 
 obj_t BgL_auxz00_8458;
BgL_auxz00_8458 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_157), 
(int)(BgL_l2473z00_4431), 
(int)(BgL_iz00_1603)); 
FAILURE(BgL_auxz00_8458,BFALSE,BFALSE);} } 
BgL_arg1675z00_1619 = 
(BgL_tmpz00_8451); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2477z00_4435;
BgL_l2477z00_4435 = 
STRING_LENGTH(BgL_tz00_1599); 
if(
BOUND_CHECK(BgL_arg1675z00_1619, BgL_l2477z00_4435))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_tz00_1599, BgL_arg1675z00_1619, ((unsigned char)'y')); }  else 
{ 
 obj_t BgL_auxz00_8470;
BgL_auxz00_8470 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_tz00_1599, 
(int)(BgL_l2477z00_4435), 
(int)(BgL_arg1675z00_1619)); 
FAILURE(BgL_auxz00_8470,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_8476;
BgL_iz00_8476 = 
(BgL_iz00_1603-1L); 
BgL_iz00_1603 = BgL_iz00_8476; 
goto BgL_zc3z04anonymousza31654ze3z87_1604;} } } } } } }  else 
{ /* Ieee/string.scm 1087 */
return 
BGl_errorz00zz__errorz00(BGl_string3468z00zz__r4_strings_6_7z00, BGl_string3462z00zz__r4_strings_6_7z00, BgL_rsz00_157);} } } } } 

}



/* _string-skip-right */
obj_t BGl__stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t BgL_env1157z00_165, obj_t BgL_opt1156z00_164)
{
{ /* Ieee/string.scm 1126 */
{ /* Ieee/string.scm 1126 */
 obj_t BgL_sz00_1654; obj_t BgL_g1158z00_1655;
BgL_sz00_1654 = 
VECTOR_REF(BgL_opt1156z00_164,0L); 
BgL_g1158z00_1655 = 
VECTOR_REF(BgL_opt1156z00_164,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1156z00_164)) { case 2L : 

{ /* Ieee/string.scm 1126 */
 long BgL_startz00_1658;
{ /* Ieee/string.scm 1126 */
 obj_t BgL_stringz00_3264;
if(
STRINGP(BgL_sz00_1654))
{ /* Ieee/string.scm 1126 */
BgL_stringz00_3264 = BgL_sz00_1654; }  else 
{ 
 obj_t BgL_auxz00_8483;
BgL_auxz00_8483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46635L), BGl_string3475z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_1654); 
FAILURE(BgL_auxz00_8483,BFALSE,BFALSE);} 
BgL_startz00_1658 = 
STRING_LENGTH(BgL_stringz00_3264); } 
{ /* Ieee/string.scm 1126 */

{ /* Ieee/string.scm 1126 */
 obj_t BgL_auxz00_8488;
if(
STRINGP(BgL_sz00_1654))
{ /* Ieee/string.scm 1126 */
BgL_auxz00_8488 = BgL_sz00_1654
; }  else 
{ 
 obj_t BgL_auxz00_8491;
BgL_auxz00_8491 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46585L), BGl_string3475z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_1654); 
FAILURE(BgL_auxz00_8491,BFALSE,BFALSE);} 
return 
BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00(BgL_auxz00_8488, BgL_g1158z00_1655, 
BINT(BgL_startz00_1658));} } } break;case 3L : 

{ /* Ieee/string.scm 1126 */
 obj_t BgL_startz00_1659;
BgL_startz00_1659 = 
VECTOR_REF(BgL_opt1156z00_164,2L); 
{ /* Ieee/string.scm 1126 */

{ /* Ieee/string.scm 1126 */
 obj_t BgL_auxz00_8498;
if(
STRINGP(BgL_sz00_1654))
{ /* Ieee/string.scm 1126 */
BgL_auxz00_8498 = BgL_sz00_1654
; }  else 
{ 
 obj_t BgL_auxz00_8501;
BgL_auxz00_8501 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46585L), BGl_string3475z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_sz00_1654); 
FAILURE(BgL_auxz00_8501,BFALSE,BFALSE);} 
return 
BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00(BgL_auxz00_8498, BgL_g1158z00_1655, BgL_startz00_1659);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3473z00zz__r4_strings_6_7z00, BGl_string3394z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1156z00_164)));} } } } 

}



/* string-skip-right */
BGL_EXPORTED_DEF obj_t BGl_stringzd2skipzd2rightz00zz__r4_strings_6_7z00(obj_t BgL_sz00_161, obj_t BgL_rsz00_162, obj_t BgL_startz00_163)
{
{ /* Ieee/string.scm 1126 */
{ 
 obj_t BgL_sz00_1734; obj_t BgL_predz00_1735; obj_t BgL_sz00_1721; obj_t BgL_cz00_1722;
{ /* Ieee/string.scm 1149 */
 bool_t BgL_test4168z00_8511;
{ /* Ieee/string.scm 1149 */
 long BgL_n1z00_3278;
{ /* Ieee/string.scm 1149 */
 obj_t BgL_tmpz00_8512;
if(
INTEGERP(BgL_startz00_163))
{ /* Ieee/string.scm 1149 */
BgL_tmpz00_8512 = BgL_startz00_163
; }  else 
{ 
 obj_t BgL_auxz00_8515;
BgL_auxz00_8515 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(47063L), BGl_string3474z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_163); 
FAILURE(BgL_auxz00_8515,BFALSE,BFALSE);} 
BgL_n1z00_3278 = 
(long)CINT(BgL_tmpz00_8512); } 
BgL_test4168z00_8511 = 
(BgL_n1z00_3278>
STRING_LENGTH(BgL_sz00_161)); } 
if(BgL_test4168z00_8511)
{ /* Ieee/string.scm 1149 */
return 
BGl_errorz00zz__errorz00(BGl_string3459z00zz__r4_strings_6_7z00, BGl_string3466z00zz__r4_strings_6_7z00, BgL_startz00_163);}  else 
{ /* Ieee/string.scm 1149 */
if(
CHARP(BgL_rsz00_162))
{ /* Ieee/string.scm 1151 */
BgL_sz00_1721 = BgL_sz00_161; 
BgL_cz00_1722 = BgL_rsz00_162; 
BgL_zc3z04anonymousza31745ze3z87_1723:
{ 
 long BgL_iz00_1726;
{ /* Ieee/string.scm 1129 */
 long BgL_za71za7_3265;
{ /* Ieee/string.scm 1129 */
 obj_t BgL_tmpz00_8544;
if(
INTEGERP(BgL_startz00_163))
{ /* Ieee/string.scm 1129 */
BgL_tmpz00_8544 = BgL_startz00_163
; }  else 
{ 
 obj_t BgL_auxz00_8547;
BgL_auxz00_8547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46718L), BGl_string3476z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_163); 
FAILURE(BgL_auxz00_8547,BFALSE,BFALSE);} 
BgL_za71za7_3265 = 
(long)CINT(BgL_tmpz00_8544); } 
BgL_iz00_1726 = 
(BgL_za71za7_3265-1L); } 
BgL_zc3z04anonymousza31746ze3z87_1727:
if(
(BgL_iz00_1726<0L))
{ /* Ieee/string.scm 1131 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1133 */
 bool_t BgL_test4172z00_8527;
{ /* Ieee/string.scm 1133 */
 unsigned char BgL_char2z00_3270;
BgL_char2z00_3270 = 
CCHAR(BgL_cz00_1722); 
{ /* Ieee/string.scm 1133 */
 unsigned char BgL_tmpz00_8529;
{ /* Ieee/string.scm 343 */
 long BgL_l2481z00_4439;
BgL_l2481z00_4439 = 
STRING_LENGTH(BgL_sz00_1721); 
if(
BOUND_CHECK(BgL_iz00_1726, BgL_l2481z00_4439))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8529 = 
STRING_REF(BgL_sz00_1721, BgL_iz00_1726)
; }  else 
{ 
 obj_t BgL_auxz00_8534;
BgL_auxz00_8534 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_1721, 
(int)(BgL_l2481z00_4439), 
(int)(BgL_iz00_1726)); 
FAILURE(BgL_auxz00_8534,BFALSE,BFALSE);} } 
BgL_test4172z00_8527 = 
(BgL_tmpz00_8529==BgL_char2z00_3270); } } 
if(BgL_test4172z00_8527)
{ 
 long BgL_iz00_8541;
BgL_iz00_8541 = 
(BgL_iz00_1726-1L); 
BgL_iz00_1726 = BgL_iz00_8541; 
goto BgL_zc3z04anonymousza31746ze3z87_1727;}  else 
{ /* Ieee/string.scm 1133 */
return 
BINT(BgL_iz00_1726);} } } }  else 
{ /* Ieee/string.scm 1151 */
if(
PROCEDUREP(BgL_rsz00_162))
{ /* Ieee/string.scm 1153 */
BgL_sz00_1734 = BgL_sz00_161; 
BgL_predz00_1735 = BgL_rsz00_162; 
{ /* Ieee/string.scm 1139 */
 long BgL_g1040z00_1737;
{ /* Ieee/string.scm 1139 */
 long BgL_za71za7_3272;
{ /* Ieee/string.scm 1139 */
 obj_t BgL_tmpz00_8555;
if(
INTEGERP(BgL_startz00_163))
{ /* Ieee/string.scm 1139 */
BgL_tmpz00_8555 = BgL_startz00_163
; }  else 
{ 
 obj_t BgL_auxz00_8558;
BgL_auxz00_8558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(46913L), BGl_string3477z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_163); 
FAILURE(BgL_auxz00_8558,BFALSE,BFALSE);} 
BgL_za71za7_3272 = 
(long)CINT(BgL_tmpz00_8555); } 
BgL_g1040z00_1737 = 
(BgL_za71za7_3272-1L); } 
{ 
 long BgL_iz00_1739;
BgL_iz00_1739 = BgL_g1040z00_1737; 
BgL_zc3z04anonymousza31753ze3z87_1740:
if(
(BgL_iz00_1739<0L))
{ /* Ieee/string.scm 1141 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1143 */
 bool_t BgL_test4178z00_8566;
{ /* Ieee/string.scm 1143 */
 unsigned char BgL_arg1758z00_1745;
{ /* Ieee/string.scm 343 */
 long BgL_l2485z00_4443;
BgL_l2485z00_4443 = 
STRING_LENGTH(BgL_sz00_1734); 
if(
BOUND_CHECK(BgL_iz00_1739, BgL_l2485z00_4443))
{ /* Ieee/string.scm 343 */
BgL_arg1758z00_1745 = 
STRING_REF(BgL_sz00_1734, BgL_iz00_1739); }  else 
{ 
 obj_t BgL_auxz00_8571;
BgL_auxz00_8571 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_1734, 
(int)(BgL_l2485z00_4443), 
(int)(BgL_iz00_1739)); 
FAILURE(BgL_auxz00_8571,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_1735, 1))
{ /* Ieee/string.scm 1143 */
BgL_test4178z00_8566 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_1735, 
BCHAR(BgL_arg1758z00_1745)))
; }  else 
{ /* Ieee/string.scm 1143 */
FAILURE(BGl_string3438z00zz__r4_strings_6_7z00,BGl_list3478z00zz__r4_strings_6_7z00,BgL_predz00_1735);} } 
if(BgL_test4178z00_8566)
{ 
 long BgL_iz00_8586;
BgL_iz00_8586 = 
(BgL_iz00_1739-1L); 
BgL_iz00_1739 = BgL_iz00_8586; 
goto BgL_zc3z04anonymousza31753ze3z87_1740;}  else 
{ /* Ieee/string.scm 1143 */
return 
BINT(BgL_iz00_1739);} } } } }  else 
{ /* Ieee/string.scm 1153 */
if(
STRINGP(BgL_rsz00_162))
{ /* Ieee/string.scm 1155 */
if(
(
STRING_LENGTH(BgL_rsz00_162)==1L))
{ 
 obj_t BgL_cz00_8595; obj_t BgL_sz00_8594;
BgL_sz00_8594 = BgL_sz00_161; 
{ /* Ieee/string.scm 331 */
 long BgL_l2489z00_4447;
BgL_l2489z00_4447 = 
STRING_LENGTH(BgL_rsz00_162); 
if(
BOUND_CHECK(0L, BgL_l2489z00_4447))
{ /* Ieee/string.scm 331 */
BgL_cz00_8595 = 
BCHAR(
STRING_REF(BgL_rsz00_162, 0L)); }  else 
{ 
 obj_t BgL_auxz00_8601;
BgL_auxz00_8601 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_rsz00_162, 
(int)(BgL_l2489z00_4447), 
(int)(0L)); 
FAILURE(BgL_auxz00_8601,BFALSE,BFALSE);} } 
BgL_cz00_1722 = BgL_cz00_8595; 
BgL_sz00_1721 = BgL_sz00_8594; 
goto BgL_zc3z04anonymousza31745ze3z87_1723;}  else 
{ /* Ieee/string.scm 1157 */
if(
(
STRING_LENGTH(BgL_rsz00_162)<=10L))
{ /* Ieee/string.scm 1160 */
 long BgL_lenjz00_1674;
BgL_lenjz00_1674 = 
STRING_LENGTH(BgL_rsz00_162); 
{ 
 long BgL_iz00_1677;
{ /* Ieee/string.scm 1162 */
 long BgL_za71za7_3287;
{ /* Ieee/string.scm 1162 */
 obj_t BgL_tmpz00_8646;
if(
INTEGERP(BgL_startz00_163))
{ /* Ieee/string.scm 1162 */
BgL_tmpz00_8646 = BgL_startz00_163
; }  else 
{ 
 obj_t BgL_auxz00_8649;
BgL_auxz00_8649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(47543L), BGl_string3474z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_163); 
FAILURE(BgL_auxz00_8649,BFALSE,BFALSE);} 
BgL_za71za7_3287 = 
(long)CINT(BgL_tmpz00_8646); } 
BgL_iz00_1677 = 
(BgL_za71za7_3287-1L); } 
BgL_zc3z04anonymousza31716ze3z87_1678:
if(
(BgL_iz00_1677<0L))
{ /* Ieee/string.scm 1163 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1165 */
 unsigned char BgL_cz00_1680;
{ /* Ieee/string.scm 331 */
 long BgL_l2493z00_4451;
BgL_l2493z00_4451 = 
STRING_LENGTH(BgL_sz00_161); 
if(
BOUND_CHECK(BgL_iz00_1677, BgL_l2493z00_4451))
{ /* Ieee/string.scm 331 */
BgL_cz00_1680 = 
STRING_REF(BgL_sz00_161, BgL_iz00_1677); }  else 
{ 
 obj_t BgL_auxz00_8617;
BgL_auxz00_8617 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_161, 
(int)(BgL_l2493z00_4451), 
(int)(BgL_iz00_1677)); 
FAILURE(BgL_auxz00_8617,BFALSE,BFALSE);} } 
{ 
 long BgL_jz00_1682;
BgL_jz00_1682 = 0L; 
BgL_zc3z04anonymousza31718ze3z87_1683:
if(
(BgL_jz00_1682==BgL_lenjz00_1674))
{ /* Ieee/string.scm 1167 */
return 
BINT(BgL_iz00_1677);}  else 
{ /* Ieee/string.scm 1169 */
 bool_t BgL_test4188z00_8626;
{ /* Ieee/string.scm 1169 */
 unsigned char BgL_tmpz00_8627;
{ /* Ieee/string.scm 343 */
 long BgL_l2497z00_4455;
BgL_l2497z00_4455 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_162)); 
if(
BOUND_CHECK(BgL_jz00_1682, BgL_l2497z00_4455))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8627 = 
STRING_REF(
((obj_t)BgL_rsz00_162), BgL_jz00_1682)
; }  else 
{ 
 obj_t BgL_auxz00_8634;
BgL_auxz00_8634 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_162), 
(int)(BgL_l2497z00_4455), 
(int)(BgL_jz00_1682)); 
FAILURE(BgL_auxz00_8634,BFALSE,BFALSE);} } 
BgL_test4188z00_8626 = 
(BgL_cz00_1680==BgL_tmpz00_8627); } 
if(BgL_test4188z00_8626)
{ 
 long BgL_iz00_8642;
BgL_iz00_8642 = 
(BgL_iz00_1677-1L); 
BgL_iz00_1677 = BgL_iz00_8642; 
goto BgL_zc3z04anonymousza31716ze3z87_1678;}  else 
{ 
 long BgL_jz00_8644;
BgL_jz00_8644 = 
(BgL_jz00_1682+1L); 
BgL_jz00_1682 = BgL_jz00_8644; 
goto BgL_zc3z04anonymousza31718ze3z87_1683;} } } } } }  else 
{ /* Ieee/string.scm 1173 */
 obj_t BgL_tz00_1692;
BgL_tz00_1692 = 
make_string(256L, ((unsigned char)'n')); 
{ /* Ieee/string.scm 1175 */
 long BgL_g1042z00_1694;
BgL_g1042z00_1694 = 
(
STRING_LENGTH(BgL_rsz00_162)-1L); 
{ 
 long BgL_iz00_1696;
BgL_iz00_1696 = BgL_g1042z00_1694; 
BgL_zc3z04anonymousza31725ze3z87_1697:
if(
(BgL_iz00_1696==-1L))
{ 
 long BgL_iz00_1701;
{ /* Ieee/string.scm 1177 */
 long BgL_za71za7_3303;
{ /* Ieee/string.scm 1177 */
 obj_t BgL_tmpz00_8690;
if(
INTEGERP(BgL_startz00_163))
{ /* Ieee/string.scm 1177 */
BgL_tmpz00_8690 = BgL_startz00_163
; }  else 
{ 
 obj_t BgL_auxz00_8693;
BgL_auxz00_8693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(47944L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_163); 
FAILURE(BgL_auxz00_8693,BFALSE,BFALSE);} 
BgL_za71za7_3303 = 
(long)CINT(BgL_tmpz00_8690); } 
BgL_iz00_1701 = 
(BgL_za71za7_3303-1L); } 
BgL_zc3z04anonymousza31727ze3z87_1702:
if(
(BgL_iz00_1701<0L))
{ /* Ieee/string.scm 1179 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1181 */
 bool_t BgL_test4193z00_8662;
{ /* Ieee/string.scm 1181 */
 unsigned char BgL_tmpz00_8663;
{ /* Ieee/string.scm 1182 */
 long BgL_i2504z00_4462;
{ /* Ieee/string.scm 1182 */
 unsigned char BgL_tmpz00_8664;
{ /* Ieee/string.scm 343 */
 long BgL_l2501z00_4459;
BgL_l2501z00_4459 = 
STRING_LENGTH(BgL_sz00_161); 
if(
BOUND_CHECK(BgL_iz00_1701, BgL_l2501z00_4459))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8664 = 
STRING_REF(BgL_sz00_161, BgL_iz00_1701)
; }  else 
{ 
 obj_t BgL_auxz00_8669;
BgL_auxz00_8669 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_sz00_161, 
(int)(BgL_l2501z00_4459), 
(int)(BgL_iz00_1701)); 
FAILURE(BgL_auxz00_8669,BFALSE,BFALSE);} } 
BgL_i2504z00_4462 = 
(BgL_tmpz00_8664); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2505z00_4463;
BgL_l2505z00_4463 = 
STRING_LENGTH(BgL_tz00_1692); 
if(
BOUND_CHECK(BgL_i2504z00_4462, BgL_l2505z00_4463))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_8663 = 
STRING_REF(BgL_tz00_1692, BgL_i2504z00_4462)
; }  else 
{ 
 obj_t BgL_auxz00_8680;
BgL_auxz00_8680 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_tz00_1692, 
(int)(BgL_l2505z00_4463), 
(int)(BgL_i2504z00_4462)); 
FAILURE(BgL_auxz00_8680,BFALSE,BFALSE);} } } 
BgL_test4193z00_8662 = 
(BgL_tmpz00_8663==((unsigned char)'y')); } 
if(BgL_test4193z00_8662)
{ 
 long BgL_iz00_8687;
BgL_iz00_8687 = 
(BgL_iz00_1701-1L); 
BgL_iz00_1701 = BgL_iz00_8687; 
goto BgL_zc3z04anonymousza31727ze3z87_1702;}  else 
{ /* Ieee/string.scm 1181 */
return 
BINT(BgL_iz00_1701);} } }  else 
{ /* Ieee/string.scm 1176 */
{ /* Ieee/string.scm 1188 */
 long BgL_arg1737z00_1713;
{ /* Ieee/string.scm 1188 */
 unsigned char BgL_tmpz00_8699;
{ /* Ieee/string.scm 343 */
 long BgL_l2509z00_4467;
BgL_l2509z00_4467 = 
STRING_LENGTH(
((obj_t)BgL_rsz00_162)); 
if(
BOUND_CHECK(BgL_iz00_1696, BgL_l2509z00_4467))
{ /* Ieee/string.scm 343 */
BgL_tmpz00_8699 = 
STRING_REF(
((obj_t)BgL_rsz00_162), BgL_iz00_1696)
; }  else 
{ 
 obj_t BgL_auxz00_8706;
BgL_auxz00_8706 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, 
((obj_t)BgL_rsz00_162), 
(int)(BgL_l2509z00_4467), 
(int)(BgL_iz00_1696)); 
FAILURE(BgL_auxz00_8706,BFALSE,BFALSE);} } 
BgL_arg1737z00_1713 = 
(BgL_tmpz00_8699); } 
{ /* Ieee/string.scm 349 */
 long BgL_l2513z00_4471;
BgL_l2513z00_4471 = 
STRING_LENGTH(BgL_tz00_1692); 
if(
BOUND_CHECK(BgL_arg1737z00_1713, BgL_l2513z00_4471))
{ /* Ieee/string.scm 349 */
STRING_SET(BgL_tz00_1692, BgL_arg1737z00_1713, ((unsigned char)'y')); }  else 
{ 
 obj_t BgL_auxz00_8718;
BgL_auxz00_8718 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(17112L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_tz00_1692, 
(int)(BgL_l2513z00_4471), 
(int)(BgL_arg1737z00_1713)); 
FAILURE(BgL_auxz00_8718,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_8724;
BgL_iz00_8724 = 
(BgL_iz00_1696-1L); 
BgL_iz00_1696 = BgL_iz00_8724; 
goto BgL_zc3z04anonymousza31725ze3z87_1697;} } } } } } }  else 
{ /* Ieee/string.scm 1155 */
return 
BGl_errorz00zz__errorz00(BGl_string3464z00zz__r4_strings_6_7z00, BGl_string3462z00zz__r4_strings_6_7z00, BgL_rsz00_162);} } } } } } } 

}



/* _string-prefix-length */
obj_t BGl__stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t BgL_env1162z00_183, obj_t BgL_opt1161z00_182)
{
{ /* Ieee/string.scm 1222 */
{ /* Ieee/string.scm 1222 */
 obj_t BgL_s1z00_1757; obj_t BgL_s2z00_1758;
BgL_s1z00_1757 = 
VECTOR_REF(BgL_opt1161z00_182,0L); 
BgL_s2z00_1758 = 
VECTOR_REF(BgL_opt1161z00_182,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1161z00_182)) { case 2L : 

{ /* Ieee/string.scm 1222 */

{ /* Ieee/string.scm 1222 */
 int BgL_tmpz00_8729;
{ /* Ieee/string.scm 1222 */
 obj_t BgL_auxz00_8737; obj_t BgL_auxz00_8730;
if(
STRINGP(BgL_s2z00_1758))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8737 = BgL_s2z00_1758
; }  else 
{ 
 obj_t BgL_auxz00_8740;
BgL_auxz00_8740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1758); 
FAILURE(BgL_auxz00_8740,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1757))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8730 = BgL_s1z00_1757
; }  else 
{ 
 obj_t BgL_auxz00_8733;
BgL_auxz00_8733 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1757); 
FAILURE(BgL_auxz00_8733,BFALSE,BFALSE);} 
BgL_tmpz00_8729 = 
BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_8730, BgL_auxz00_8737, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_8729);} } break;case 3L : 

{ /* Ieee/string.scm 1222 */
 obj_t BgL_start1z00_1765;
BgL_start1z00_1765 = 
VECTOR_REF(BgL_opt1161z00_182,2L); 
{ /* Ieee/string.scm 1222 */

{ /* Ieee/string.scm 1222 */
 int BgL_tmpz00_8747;
{ /* Ieee/string.scm 1222 */
 obj_t BgL_auxz00_8755; obj_t BgL_auxz00_8748;
if(
STRINGP(BgL_s2z00_1758))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8755 = BgL_s2z00_1758
; }  else 
{ 
 obj_t BgL_auxz00_8758;
BgL_auxz00_8758 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1758); 
FAILURE(BgL_auxz00_8758,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1757))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8748 = BgL_s1z00_1757
; }  else 
{ 
 obj_t BgL_auxz00_8751;
BgL_auxz00_8751 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1757); 
FAILURE(BgL_auxz00_8751,BFALSE,BFALSE);} 
BgL_tmpz00_8747 = 
BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_8748, BgL_auxz00_8755, BgL_start1z00_1765, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_8747);} } } break;case 4L : 

{ /* Ieee/string.scm 1222 */
 obj_t BgL_start1z00_1769;
BgL_start1z00_1769 = 
VECTOR_REF(BgL_opt1161z00_182,2L); 
{ /* Ieee/string.scm 1222 */
 obj_t BgL_end1z00_1770;
BgL_end1z00_1770 = 
VECTOR_REF(BgL_opt1161z00_182,3L); 
{ /* Ieee/string.scm 1222 */

{ /* Ieee/string.scm 1222 */
 int BgL_tmpz00_8766;
{ /* Ieee/string.scm 1222 */
 obj_t BgL_auxz00_8774; obj_t BgL_auxz00_8767;
if(
STRINGP(BgL_s2z00_1758))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8774 = BgL_s2z00_1758
; }  else 
{ 
 obj_t BgL_auxz00_8777;
BgL_auxz00_8777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1758); 
FAILURE(BgL_auxz00_8777,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1757))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8767 = BgL_s1z00_1757
; }  else 
{ 
 obj_t BgL_auxz00_8770;
BgL_auxz00_8770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1757); 
FAILURE(BgL_auxz00_8770,BFALSE,BFALSE);} 
BgL_tmpz00_8766 = 
BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_8767, BgL_auxz00_8774, BgL_start1z00_1769, BgL_end1z00_1770, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_8766);} } } } break;case 5L : 

{ /* Ieee/string.scm 1222 */
 obj_t BgL_start1z00_1773;
BgL_start1z00_1773 = 
VECTOR_REF(BgL_opt1161z00_182,2L); 
{ /* Ieee/string.scm 1222 */
 obj_t BgL_end1z00_1774;
BgL_end1z00_1774 = 
VECTOR_REF(BgL_opt1161z00_182,3L); 
{ /* Ieee/string.scm 1222 */
 obj_t BgL_start2z00_1775;
BgL_start2z00_1775 = 
VECTOR_REF(BgL_opt1161z00_182,4L); 
{ /* Ieee/string.scm 1222 */

{ /* Ieee/string.scm 1222 */
 int BgL_tmpz00_8786;
{ /* Ieee/string.scm 1222 */
 obj_t BgL_auxz00_8794; obj_t BgL_auxz00_8787;
if(
STRINGP(BgL_s2z00_1758))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8794 = BgL_s2z00_1758
; }  else 
{ 
 obj_t BgL_auxz00_8797;
BgL_auxz00_8797 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1758); 
FAILURE(BgL_auxz00_8797,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1757))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8787 = BgL_s1z00_1757
; }  else 
{ 
 obj_t BgL_auxz00_8790;
BgL_auxz00_8790 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1757); 
FAILURE(BgL_auxz00_8790,BFALSE,BFALSE);} 
BgL_tmpz00_8786 = 
BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_8787, BgL_auxz00_8794, BgL_start1z00_1773, BgL_end1z00_1774, BgL_start2z00_1775, BFALSE); } 
return 
BINT(BgL_tmpz00_8786);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1222 */
 obj_t BgL_start1z00_1777;
BgL_start1z00_1777 = 
VECTOR_REF(BgL_opt1161z00_182,2L); 
{ /* Ieee/string.scm 1222 */
 obj_t BgL_end1z00_1778;
BgL_end1z00_1778 = 
VECTOR_REF(BgL_opt1161z00_182,3L); 
{ /* Ieee/string.scm 1222 */
 obj_t BgL_start2z00_1779;
BgL_start2z00_1779 = 
VECTOR_REF(BgL_opt1161z00_182,4L); 
{ /* Ieee/string.scm 1222 */
 obj_t BgL_end2z00_1780;
BgL_end2z00_1780 = 
VECTOR_REF(BgL_opt1161z00_182,5L); 
{ /* Ieee/string.scm 1222 */

{ /* Ieee/string.scm 1222 */
 int BgL_tmpz00_8807;
{ /* Ieee/string.scm 1222 */
 obj_t BgL_auxz00_8815; obj_t BgL_auxz00_8808;
if(
STRINGP(BgL_s2z00_1758))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8815 = BgL_s2z00_1758
; }  else 
{ 
 obj_t BgL_auxz00_8818;
BgL_auxz00_8818 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1758); 
FAILURE(BgL_auxz00_8818,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1757))
{ /* Ieee/string.scm 1222 */
BgL_auxz00_8808 = BgL_s1z00_1757
; }  else 
{ 
 obj_t BgL_auxz00_8811;
BgL_auxz00_8811 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49488L), BGl_string3484z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1757); 
FAILURE(BgL_auxz00_8811,BFALSE,BFALSE);} 
BgL_tmpz00_8807 = 
BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_8808, BgL_auxz00_8815, BgL_start1z00_1777, BgL_end1z00_1778, BgL_start2z00_1779, BgL_end2z00_1780); } 
return 
BINT(BgL_tmpz00_8807);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3481z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1161z00_182)));} } } } 

}



/* string-prefix-length */
BGL_EXPORTED_DEF int BGl_stringzd2prefixzd2lengthz00zz__r4_strings_6_7z00(obj_t BgL_s1z00_176, obj_t BgL_s2z00_177, obj_t BgL_start1z00_178, obj_t BgL_end1z00_179, obj_t BgL_start2z00_180, obj_t BgL_end2z00_181)
{
{ /* Ieee/string.scm 1222 */
{ /* Ieee/string.scm 1224 */
 long BgL_l1z00_1782;
BgL_l1z00_1782 = 
STRING_LENGTH(BgL_s1z00_176); 
{ /* Ieee/string.scm 1224 */
 long BgL_l2z00_1783;
BgL_l2z00_1783 = 
STRING_LENGTH(BgL_s2z00_177); 
{ /* Ieee/string.scm 1225 */
 obj_t BgL_e1z00_1784;
{ /* Ieee/string.scm 1226 */
 obj_t BgL_procz00_3336;
BgL_procz00_3336 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_179))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4210z00_8833;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3341;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_8834;
if(
INTEGERP(BgL_end1z00_179))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_8834 = BgL_end1z00_179
; }  else 
{ 
 obj_t BgL_auxz00_8837;
BgL_auxz00_8837 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_179); 
FAILURE(BgL_auxz00_8837,BFALSE,BFALSE);} 
BgL_n1z00_3341 = 
(long)CINT(BgL_tmpz00_8834); } 
BgL_test4210z00_8833 = 
(BgL_n1z00_3341<=0L); } 
if(BgL_test4210z00_8833)
{ /* Ieee/string.scm 1212 */
BgL_e1z00_1784 = 
BGl_errorz00zz__errorz00(BgL_procz00_3336, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_179); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4212z00_8845;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3342;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_8846;
if(
INTEGERP(BgL_end1z00_179))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_8846 = BgL_end1z00_179
; }  else 
{ 
 obj_t BgL_auxz00_8849;
BgL_auxz00_8849 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_179); 
FAILURE(BgL_auxz00_8849,BFALSE,BFALSE);} 
BgL_n1z00_3342 = 
(long)CINT(BgL_tmpz00_8846); } 
BgL_test4212z00_8845 = 
(BgL_n1z00_3342>BgL_l1z00_1782); } 
if(BgL_test4212z00_8845)
{ /* Ieee/string.scm 1214 */
BgL_e1z00_1784 = 
BGl_errorz00zz__errorz00(BgL_procz00_3336, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_179); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e1z00_1784 = BgL_end1z00_179; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e1z00_1784 = 
BINT(BgL_l1z00_1782); } } 
{ /* Ieee/string.scm 1226 */
 obj_t BgL_e2z00_1785;
{ /* Ieee/string.scm 1227 */
 obj_t BgL_procz00_3344;
BgL_procz00_3344 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_181))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4215z00_8860;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3349;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_8861;
if(
INTEGERP(BgL_end2z00_181))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_8861 = BgL_end2z00_181
; }  else 
{ 
 obj_t BgL_auxz00_8864;
BgL_auxz00_8864 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_181); 
FAILURE(BgL_auxz00_8864,BFALSE,BFALSE);} 
BgL_n1z00_3349 = 
(long)CINT(BgL_tmpz00_8861); } 
BgL_test4215z00_8860 = 
(BgL_n1z00_3349<=0L); } 
if(BgL_test4215z00_8860)
{ /* Ieee/string.scm 1212 */
BgL_e2z00_1785 = 
BGl_errorz00zz__errorz00(BgL_procz00_3344, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_181); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4217z00_8872;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3350;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_8873;
if(
INTEGERP(BgL_end2z00_181))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_8873 = BgL_end2z00_181
; }  else 
{ 
 obj_t BgL_auxz00_8876;
BgL_auxz00_8876 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_181); 
FAILURE(BgL_auxz00_8876,BFALSE,BFALSE);} 
BgL_n1z00_3350 = 
(long)CINT(BgL_tmpz00_8873); } 
BgL_test4217z00_8872 = 
(BgL_n1z00_3350>BgL_l2z00_1783); } 
if(BgL_test4217z00_8872)
{ /* Ieee/string.scm 1214 */
BgL_e2z00_1785 = 
BGl_errorz00zz__errorz00(BgL_procz00_3344, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_181); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e2z00_1785 = BgL_end2z00_181; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e2z00_1785 = 
BINT(BgL_l2z00_1783); } } 
{ /* Ieee/string.scm 1227 */
 obj_t BgL_b1z00_1786;
{ /* Ieee/string.scm 1228 */
 obj_t BgL_procz00_3352;
BgL_procz00_3352 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_178))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4220z00_8887;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3357;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_8888;
if(
INTEGERP(BgL_start1z00_178))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_8888 = BgL_start1z00_178
; }  else 
{ 
 obj_t BgL_auxz00_8891;
BgL_auxz00_8891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_178); 
FAILURE(BgL_auxz00_8891,BFALSE,BFALSE);} 
BgL_n1z00_3357 = 
(long)CINT(BgL_tmpz00_8888); } 
BgL_test4220z00_8887 = 
(BgL_n1z00_3357<0L); } 
if(BgL_test4220z00_8887)
{ /* Ieee/string.scm 1198 */
BgL_b1z00_1786 = 
BGl_errorz00zz__errorz00(BgL_procz00_3352, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_178); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4222z00_8899;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3358;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_8900;
if(
INTEGERP(BgL_start1z00_178))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_8900 = BgL_start1z00_178
; }  else 
{ 
 obj_t BgL_auxz00_8903;
BgL_auxz00_8903 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_178); 
FAILURE(BgL_auxz00_8903,BFALSE,BFALSE);} 
BgL_n1z00_3358 = 
(long)CINT(BgL_tmpz00_8900); } 
BgL_test4222z00_8899 = 
(BgL_n1z00_3358>=BgL_l1z00_1782); } 
if(BgL_test4222z00_8899)
{ /* Ieee/string.scm 1200 */
BgL_b1z00_1786 = 
BGl_errorz00zz__errorz00(BgL_procz00_3352, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_178); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b1z00_1786 = BgL_start1z00_178; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b1z00_1786 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1228 */
 obj_t BgL_b2z00_1787;
{ /* Ieee/string.scm 1229 */
 obj_t BgL_procz00_3360;
BgL_procz00_3360 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_180))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4225z00_8914;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3365;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_8915;
if(
INTEGERP(BgL_start2z00_180))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_8915 = BgL_start2z00_180
; }  else 
{ 
 obj_t BgL_auxz00_8918;
BgL_auxz00_8918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_180); 
FAILURE(BgL_auxz00_8918,BFALSE,BFALSE);} 
BgL_n1z00_3365 = 
(long)CINT(BgL_tmpz00_8915); } 
BgL_test4225z00_8914 = 
(BgL_n1z00_3365<0L); } 
if(BgL_test4225z00_8914)
{ /* Ieee/string.scm 1198 */
BgL_b2z00_1787 = 
BGl_errorz00zz__errorz00(BgL_procz00_3360, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_180); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4227z00_8926;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3366;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_8927;
if(
INTEGERP(BgL_start2z00_180))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_8927 = BgL_start2z00_180
; }  else 
{ 
 obj_t BgL_auxz00_8930;
BgL_auxz00_8930 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3486z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_180); 
FAILURE(BgL_auxz00_8930,BFALSE,BFALSE);} 
BgL_n1z00_3366 = 
(long)CINT(BgL_tmpz00_8927); } 
BgL_test4227z00_8926 = 
(BgL_n1z00_3366>=BgL_l2z00_1783); } 
if(BgL_test4227z00_8926)
{ /* Ieee/string.scm 1200 */
BgL_b2z00_1787 = 
BGl_errorz00zz__errorz00(BgL_procz00_3360, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_180); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b2z00_1787 = BgL_start2z00_180; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b2z00_1787 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1229 */

{ 
 obj_t BgL_i1z00_1789; obj_t BgL_i2z00_1790;
{ /* Ieee/string.scm 1230 */
 long BgL_tmpz00_8939;
BgL_i1z00_1789 = BgL_b1z00_1786; 
BgL_i2z00_1790 = BgL_b2z00_1787; 
BgL_zc3z04anonymousza31768ze3z87_1791:
{ /* Ieee/string.scm 1233 */
 bool_t BgL_test4229z00_8940;
{ /* Ieee/string.scm 1233 */
 bool_t BgL_test4230z00_8941;
{ /* Ieee/string.scm 1233 */
 long BgL_n1z00_3368; long BgL_n2z00_3369;
{ /* Ieee/string.scm 1233 */
 obj_t BgL_tmpz00_8942;
if(
INTEGERP(BgL_i1z00_1789))
{ /* Ieee/string.scm 1233 */
BgL_tmpz00_8942 = BgL_i1z00_1789
; }  else 
{ 
 obj_t BgL_auxz00_8945;
BgL_auxz00_8945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49982L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1789); 
FAILURE(BgL_auxz00_8945,BFALSE,BFALSE);} 
BgL_n1z00_3368 = 
(long)CINT(BgL_tmpz00_8942); } 
{ /* Ieee/string.scm 1233 */
 obj_t BgL_tmpz00_8950;
if(
INTEGERP(BgL_e1z00_1784))
{ /* Ieee/string.scm 1233 */
BgL_tmpz00_8950 = BgL_e1z00_1784
; }  else 
{ 
 obj_t BgL_auxz00_8953;
BgL_auxz00_8953 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49985L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_1784); 
FAILURE(BgL_auxz00_8953,BFALSE,BFALSE);} 
BgL_n2z00_3369 = 
(long)CINT(BgL_tmpz00_8950); } 
BgL_test4230z00_8941 = 
(BgL_n1z00_3368==BgL_n2z00_3369); } 
if(BgL_test4230z00_8941)
{ /* Ieee/string.scm 1233 */
BgL_test4229z00_8940 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1233 */
 long BgL_n1z00_3370; long BgL_n2z00_3371;
{ /* Ieee/string.scm 1233 */
 obj_t BgL_tmpz00_8959;
if(
INTEGERP(BgL_i2z00_1790))
{ /* Ieee/string.scm 1233 */
BgL_tmpz00_8959 = BgL_i2z00_1790
; }  else 
{ 
 obj_t BgL_auxz00_8962;
BgL_auxz00_8962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49994L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1790); 
FAILURE(BgL_auxz00_8962,BFALSE,BFALSE);} 
BgL_n1z00_3370 = 
(long)CINT(BgL_tmpz00_8959); } 
{ /* Ieee/string.scm 1233 */
 obj_t BgL_tmpz00_8967;
if(
INTEGERP(BgL_e2z00_1785))
{ /* Ieee/string.scm 1233 */
BgL_tmpz00_8967 = BgL_e2z00_1785
; }  else 
{ 
 obj_t BgL_auxz00_8970;
BgL_auxz00_8970 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49997L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_1785); 
FAILURE(BgL_auxz00_8970,BFALSE,BFALSE);} 
BgL_n2z00_3371 = 
(long)CINT(BgL_tmpz00_8967); } 
BgL_test4229z00_8940 = 
(BgL_n1z00_3370==BgL_n2z00_3371); } } 
if(BgL_test4229z00_8940)
{ /* Ieee/string.scm 1234 */
 long BgL_za71za7_3372; long BgL_za72za7_3373;
{ /* Ieee/string.scm 1234 */
 obj_t BgL_tmpz00_8976;
if(
INTEGERP(BgL_i1z00_1789))
{ /* Ieee/string.scm 1234 */
BgL_tmpz00_8976 = BgL_i1z00_1789
; }  else 
{ 
 obj_t BgL_auxz00_8979;
BgL_auxz00_8979 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50013L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1789); 
FAILURE(BgL_auxz00_8979,BFALSE,BFALSE);} 
BgL_za71za7_3372 = 
(long)CINT(BgL_tmpz00_8976); } 
{ /* Ieee/string.scm 1234 */
 obj_t BgL_tmpz00_8984;
if(
INTEGERP(BgL_b1z00_1786))
{ /* Ieee/string.scm 1234 */
BgL_tmpz00_8984 = BgL_b1z00_1786
; }  else 
{ 
 obj_t BgL_auxz00_8987;
BgL_auxz00_8987 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50016L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1786); 
FAILURE(BgL_auxz00_8987,BFALSE,BFALSE);} 
BgL_za72za7_3373 = 
(long)CINT(BgL_tmpz00_8984); } 
BgL_tmpz00_8939 = 
(BgL_za71za7_3372-BgL_za72za7_3373); }  else 
{ /* Ieee/string.scm 1235 */
 bool_t BgL_test4237z00_8993;
{ /* Ieee/string.scm 1235 */
 unsigned char BgL_auxz00_9013; unsigned char BgL_tmpz00_8994;
{ /* Ieee/string.scm 1235 */
 long BgL_kz00_3377;
{ /* Ieee/string.scm 1235 */
 obj_t BgL_tmpz00_9014;
if(
INTEGERP(BgL_i2z00_1790))
{ /* Ieee/string.scm 1235 */
BgL_tmpz00_9014 = BgL_i2z00_1790
; }  else 
{ 
 obj_t BgL_auxz00_9017;
BgL_auxz00_9017 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50069L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1790); 
FAILURE(BgL_auxz00_9017,BFALSE,BFALSE);} 
BgL_kz00_3377 = 
(long)CINT(BgL_tmpz00_9014); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2521z00_4479;
BgL_l2521z00_4479 = 
STRING_LENGTH(BgL_s2z00_177); 
if(
BOUND_CHECK(BgL_kz00_3377, BgL_l2521z00_4479))
{ /* Ieee/string.scm 331 */
BgL_auxz00_9013 = 
STRING_REF(BgL_s2z00_177, BgL_kz00_3377)
; }  else 
{ 
 obj_t BgL_auxz00_9026;
BgL_auxz00_9026 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_177, 
(int)(BgL_l2521z00_4479), 
(int)(BgL_kz00_3377)); 
FAILURE(BgL_auxz00_9026,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1235 */
 long BgL_kz00_3375;
{ /* Ieee/string.scm 1235 */
 obj_t BgL_tmpz00_8995;
if(
INTEGERP(BgL_i1z00_1789))
{ /* Ieee/string.scm 1235 */
BgL_tmpz00_8995 = BgL_i1z00_1789
; }  else 
{ 
 obj_t BgL_auxz00_8998;
BgL_auxz00_8998 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50050L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1789); 
FAILURE(BgL_auxz00_8998,BFALSE,BFALSE);} 
BgL_kz00_3375 = 
(long)CINT(BgL_tmpz00_8995); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2517z00_4475;
BgL_l2517z00_4475 = 
STRING_LENGTH(BgL_s1z00_176); 
if(
BOUND_CHECK(BgL_kz00_3375, BgL_l2517z00_4475))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_8994 = 
STRING_REF(BgL_s1z00_176, BgL_kz00_3375)
; }  else 
{ 
 obj_t BgL_auxz00_9007;
BgL_auxz00_9007 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_176, 
(int)(BgL_l2517z00_4475), 
(int)(BgL_kz00_3375)); 
FAILURE(BgL_auxz00_9007,BFALSE,BFALSE);} } } 
BgL_test4237z00_8993 = 
(BgL_tmpz00_8994==BgL_auxz00_9013); } 
if(BgL_test4237z00_8993)
{ 
 obj_t BgL_i2z00_9043; obj_t BgL_i1z00_9033;
{ 
 obj_t BgL_tmpz00_9034;
if(
INTEGERP(BgL_i1z00_1789))
{ /* Ieee/string.scm 1236 */
BgL_tmpz00_9034 = BgL_i1z00_1789
; }  else 
{ 
 obj_t BgL_auxz00_9037;
BgL_auxz00_9037 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50091L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1789); 
FAILURE(BgL_auxz00_9037,BFALSE,BFALSE);} 
BgL_i1z00_9033 = 
ADDFX(BgL_tmpz00_9034, 
BINT(1L)); } 
{ 
 obj_t BgL_tmpz00_9044;
if(
INTEGERP(BgL_i2z00_1790))
{ /* Ieee/string.scm 1236 */
BgL_tmpz00_9044 = BgL_i2z00_1790
; }  else 
{ 
 obj_t BgL_auxz00_9047;
BgL_auxz00_9047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50102L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1790); 
FAILURE(BgL_auxz00_9047,BFALSE,BFALSE);} 
BgL_i2z00_9043 = 
ADDFX(BgL_tmpz00_9044, 
BINT(1L)); } 
BgL_i2z00_1790 = BgL_i2z00_9043; 
BgL_i1z00_1789 = BgL_i1z00_9033; 
goto BgL_zc3z04anonymousza31768ze3z87_1791;}  else 
{ /* Ieee/string.scm 1238 */
 long BgL_za71za7_3382; long BgL_za72za7_3383;
{ /* Ieee/string.scm 1238 */
 obj_t BgL_tmpz00_9053;
if(
INTEGERP(BgL_i1z00_1789))
{ /* Ieee/string.scm 1238 */
BgL_tmpz00_9053 = BgL_i1z00_1789
; }  else 
{ 
 obj_t BgL_auxz00_9056;
BgL_auxz00_9056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50132L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1789); 
FAILURE(BgL_auxz00_9056,BFALSE,BFALSE);} 
BgL_za71za7_3382 = 
(long)CINT(BgL_tmpz00_9053); } 
{ /* Ieee/string.scm 1238 */
 obj_t BgL_tmpz00_9061;
if(
INTEGERP(BgL_b1z00_1786))
{ /* Ieee/string.scm 1238 */
BgL_tmpz00_9061 = BgL_b1z00_1786
; }  else 
{ 
 obj_t BgL_auxz00_9064;
BgL_auxz00_9064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50135L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1786); 
FAILURE(BgL_auxz00_9064,BFALSE,BFALSE);} 
BgL_za72za7_3383 = 
(long)CINT(BgL_tmpz00_9061); } 
BgL_tmpz00_8939 = 
(BgL_za71za7_3382-BgL_za72za7_3383); } } } 
return 
(int)(BgL_tmpz00_8939);} } } } } } } } } } 

}



/* _string-prefix-length-ci */
obj_t BGl__stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t BgL_env1166z00_191, obj_t BgL_opt1165z00_190)
{
{ /* Ieee/string.scm 1243 */
{ /* Ieee/string.scm 1243 */
 obj_t BgL_s1z00_1803; obj_t BgL_s2z00_1804;
BgL_s1z00_1803 = 
VECTOR_REF(BgL_opt1165z00_190,0L); 
BgL_s2z00_1804 = 
VECTOR_REF(BgL_opt1165z00_190,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1165z00_190)) { case 2L : 

{ /* Ieee/string.scm 1243 */

{ /* Ieee/string.scm 1243 */
 int BgL_tmpz00_9073;
{ /* Ieee/string.scm 1243 */
 obj_t BgL_auxz00_9081; obj_t BgL_auxz00_9074;
if(
STRINGP(BgL_s2z00_1804))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9081 = BgL_s2z00_1804
; }  else 
{ 
 obj_t BgL_auxz00_9084;
BgL_auxz00_9084 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1804); 
FAILURE(BgL_auxz00_9084,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1803))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9074 = BgL_s1z00_1803
; }  else 
{ 
 obj_t BgL_auxz00_9077;
BgL_auxz00_9077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1803); 
FAILURE(BgL_auxz00_9077,BFALSE,BFALSE);} 
BgL_tmpz00_9073 = 
BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9074, BgL_auxz00_9081, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9073);} } break;case 3L : 

{ /* Ieee/string.scm 1243 */
 obj_t BgL_start1z00_1811;
BgL_start1z00_1811 = 
VECTOR_REF(BgL_opt1165z00_190,2L); 
{ /* Ieee/string.scm 1243 */

{ /* Ieee/string.scm 1243 */
 int BgL_tmpz00_9091;
{ /* Ieee/string.scm 1243 */
 obj_t BgL_auxz00_9099; obj_t BgL_auxz00_9092;
if(
STRINGP(BgL_s2z00_1804))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9099 = BgL_s2z00_1804
; }  else 
{ 
 obj_t BgL_auxz00_9102;
BgL_auxz00_9102 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1804); 
FAILURE(BgL_auxz00_9102,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1803))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9092 = BgL_s1z00_1803
; }  else 
{ 
 obj_t BgL_auxz00_9095;
BgL_auxz00_9095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1803); 
FAILURE(BgL_auxz00_9095,BFALSE,BFALSE);} 
BgL_tmpz00_9091 = 
BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9092, BgL_auxz00_9099, BgL_start1z00_1811, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9091);} } } break;case 4L : 

{ /* Ieee/string.scm 1243 */
 obj_t BgL_start1z00_1815;
BgL_start1z00_1815 = 
VECTOR_REF(BgL_opt1165z00_190,2L); 
{ /* Ieee/string.scm 1243 */
 obj_t BgL_end1z00_1816;
BgL_end1z00_1816 = 
VECTOR_REF(BgL_opt1165z00_190,3L); 
{ /* Ieee/string.scm 1243 */

{ /* Ieee/string.scm 1243 */
 int BgL_tmpz00_9110;
{ /* Ieee/string.scm 1243 */
 obj_t BgL_auxz00_9118; obj_t BgL_auxz00_9111;
if(
STRINGP(BgL_s2z00_1804))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9118 = BgL_s2z00_1804
; }  else 
{ 
 obj_t BgL_auxz00_9121;
BgL_auxz00_9121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1804); 
FAILURE(BgL_auxz00_9121,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1803))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9111 = BgL_s1z00_1803
; }  else 
{ 
 obj_t BgL_auxz00_9114;
BgL_auxz00_9114 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1803); 
FAILURE(BgL_auxz00_9114,BFALSE,BFALSE);} 
BgL_tmpz00_9110 = 
BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9111, BgL_auxz00_9118, BgL_start1z00_1815, BgL_end1z00_1816, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9110);} } } } break;case 5L : 

{ /* Ieee/string.scm 1243 */
 obj_t BgL_start1z00_1819;
BgL_start1z00_1819 = 
VECTOR_REF(BgL_opt1165z00_190,2L); 
{ /* Ieee/string.scm 1243 */
 obj_t BgL_end1z00_1820;
BgL_end1z00_1820 = 
VECTOR_REF(BgL_opt1165z00_190,3L); 
{ /* Ieee/string.scm 1243 */
 obj_t BgL_start2z00_1821;
BgL_start2z00_1821 = 
VECTOR_REF(BgL_opt1165z00_190,4L); 
{ /* Ieee/string.scm 1243 */

{ /* Ieee/string.scm 1243 */
 int BgL_tmpz00_9130;
{ /* Ieee/string.scm 1243 */
 obj_t BgL_auxz00_9138; obj_t BgL_auxz00_9131;
if(
STRINGP(BgL_s2z00_1804))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9138 = BgL_s2z00_1804
; }  else 
{ 
 obj_t BgL_auxz00_9141;
BgL_auxz00_9141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1804); 
FAILURE(BgL_auxz00_9141,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1803))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9131 = BgL_s1z00_1803
; }  else 
{ 
 obj_t BgL_auxz00_9134;
BgL_auxz00_9134 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1803); 
FAILURE(BgL_auxz00_9134,BFALSE,BFALSE);} 
BgL_tmpz00_9130 = 
BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9131, BgL_auxz00_9138, BgL_start1z00_1819, BgL_end1z00_1820, BgL_start2z00_1821, BFALSE); } 
return 
BINT(BgL_tmpz00_9130);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1243 */
 obj_t BgL_start1z00_1823;
BgL_start1z00_1823 = 
VECTOR_REF(BgL_opt1165z00_190,2L); 
{ /* Ieee/string.scm 1243 */
 obj_t BgL_end1z00_1824;
BgL_end1z00_1824 = 
VECTOR_REF(BgL_opt1165z00_190,3L); 
{ /* Ieee/string.scm 1243 */
 obj_t BgL_start2z00_1825;
BgL_start2z00_1825 = 
VECTOR_REF(BgL_opt1165z00_190,4L); 
{ /* Ieee/string.scm 1243 */
 obj_t BgL_end2z00_1826;
BgL_end2z00_1826 = 
VECTOR_REF(BgL_opt1165z00_190,5L); 
{ /* Ieee/string.scm 1243 */

{ /* Ieee/string.scm 1243 */
 int BgL_tmpz00_9151;
{ /* Ieee/string.scm 1243 */
 obj_t BgL_auxz00_9159; obj_t BgL_auxz00_9152;
if(
STRINGP(BgL_s2z00_1804))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9159 = BgL_s2z00_1804
; }  else 
{ 
 obj_t BgL_auxz00_9162;
BgL_auxz00_9162 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1804); 
FAILURE(BgL_auxz00_9162,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1803))
{ /* Ieee/string.scm 1243 */
BgL_auxz00_9152 = BgL_s1z00_1803
; }  else 
{ 
 obj_t BgL_auxz00_9155;
BgL_auxz00_9155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50369L), BGl_string3498z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1803); 
FAILURE(BgL_auxz00_9155,BFALSE,BFALSE);} 
BgL_tmpz00_9151 = 
BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9152, BgL_auxz00_9159, BgL_start1z00_1823, BgL_end1z00_1824, BgL_start2z00_1825, BgL_end2z00_1826); } 
return 
BINT(BgL_tmpz00_9151);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3496z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1165z00_190)));} } } } 

}



/* string-prefix-length-ci */
BGL_EXPORTED_DEF int BGl_stringzd2prefixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t BgL_s1z00_184, obj_t BgL_s2z00_185, obj_t BgL_start1z00_186, obj_t BgL_end1z00_187, obj_t BgL_start2z00_188, obj_t BgL_end2z00_189)
{
{ /* Ieee/string.scm 1243 */
{ /* Ieee/string.scm 1245 */
 long BgL_l1z00_1828;
BgL_l1z00_1828 = 
STRING_LENGTH(BgL_s1z00_184); 
{ /* Ieee/string.scm 1245 */
 long BgL_l2z00_1829;
BgL_l2z00_1829 = 
STRING_LENGTH(BgL_s2z00_185); 
{ /* Ieee/string.scm 1246 */
 obj_t BgL_e1z00_1830;
{ /* Ieee/string.scm 1247 */
 obj_t BgL_procz00_3386;
BgL_procz00_3386 = BGl_symbol3499z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_187))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4257z00_9177;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3391;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_9178;
if(
INTEGERP(BgL_end1z00_187))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_9178 = BgL_end1z00_187
; }  else 
{ 
 obj_t BgL_auxz00_9181;
BgL_auxz00_9181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_187); 
FAILURE(BgL_auxz00_9181,BFALSE,BFALSE);} 
BgL_n1z00_3391 = 
(long)CINT(BgL_tmpz00_9178); } 
BgL_test4257z00_9177 = 
(BgL_n1z00_3391<=0L); } 
if(BgL_test4257z00_9177)
{ /* Ieee/string.scm 1212 */
BgL_e1z00_1830 = 
BGl_errorz00zz__errorz00(BgL_procz00_3386, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_187); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4259z00_9189;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3392;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_9190;
if(
INTEGERP(BgL_end1z00_187))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_9190 = BgL_end1z00_187
; }  else 
{ 
 obj_t BgL_auxz00_9193;
BgL_auxz00_9193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_187); 
FAILURE(BgL_auxz00_9193,BFALSE,BFALSE);} 
BgL_n1z00_3392 = 
(long)CINT(BgL_tmpz00_9190); } 
BgL_test4259z00_9189 = 
(BgL_n1z00_3392>BgL_l1z00_1828); } 
if(BgL_test4259z00_9189)
{ /* Ieee/string.scm 1214 */
BgL_e1z00_1830 = 
BGl_errorz00zz__errorz00(BgL_procz00_3386, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_187); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e1z00_1830 = BgL_end1z00_187; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e1z00_1830 = 
BINT(BgL_l1z00_1828); } } 
{ /* Ieee/string.scm 1247 */
 obj_t BgL_e2z00_1831;
{ /* Ieee/string.scm 1248 */
 obj_t BgL_procz00_3394;
BgL_procz00_3394 = BGl_symbol3499z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_189))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4262z00_9204;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3399;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_9205;
if(
INTEGERP(BgL_end2z00_189))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_9205 = BgL_end2z00_189
; }  else 
{ 
 obj_t BgL_auxz00_9208;
BgL_auxz00_9208 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_189); 
FAILURE(BgL_auxz00_9208,BFALSE,BFALSE);} 
BgL_n1z00_3399 = 
(long)CINT(BgL_tmpz00_9205); } 
BgL_test4262z00_9204 = 
(BgL_n1z00_3399<=0L); } 
if(BgL_test4262z00_9204)
{ /* Ieee/string.scm 1212 */
BgL_e2z00_1831 = 
BGl_errorz00zz__errorz00(BgL_procz00_3394, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_189); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4264z00_9216;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3400;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_9217;
if(
INTEGERP(BgL_end2z00_189))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_9217 = BgL_end2z00_189
; }  else 
{ 
 obj_t BgL_auxz00_9220;
BgL_auxz00_9220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_189); 
FAILURE(BgL_auxz00_9220,BFALSE,BFALSE);} 
BgL_n1z00_3400 = 
(long)CINT(BgL_tmpz00_9217); } 
BgL_test4264z00_9216 = 
(BgL_n1z00_3400>BgL_l2z00_1829); } 
if(BgL_test4264z00_9216)
{ /* Ieee/string.scm 1214 */
BgL_e2z00_1831 = 
BGl_errorz00zz__errorz00(BgL_procz00_3394, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_189); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e2z00_1831 = BgL_end2z00_189; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e2z00_1831 = 
BINT(BgL_l2z00_1829); } } 
{ /* Ieee/string.scm 1248 */
 obj_t BgL_b1z00_1832;
{ /* Ieee/string.scm 1249 */
 obj_t BgL_procz00_3402;
BgL_procz00_3402 = BGl_symbol3499z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_186))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4267z00_9231;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3407;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_9232;
if(
INTEGERP(BgL_start1z00_186))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_9232 = BgL_start1z00_186
; }  else 
{ 
 obj_t BgL_auxz00_9235;
BgL_auxz00_9235 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_186); 
FAILURE(BgL_auxz00_9235,BFALSE,BFALSE);} 
BgL_n1z00_3407 = 
(long)CINT(BgL_tmpz00_9232); } 
BgL_test4267z00_9231 = 
(BgL_n1z00_3407<0L); } 
if(BgL_test4267z00_9231)
{ /* Ieee/string.scm 1198 */
BgL_b1z00_1832 = 
BGl_errorz00zz__errorz00(BgL_procz00_3402, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_186); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4269z00_9243;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3408;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_9244;
if(
INTEGERP(BgL_start1z00_186))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_9244 = BgL_start1z00_186
; }  else 
{ 
 obj_t BgL_auxz00_9247;
BgL_auxz00_9247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_186); 
FAILURE(BgL_auxz00_9247,BFALSE,BFALSE);} 
BgL_n1z00_3408 = 
(long)CINT(BgL_tmpz00_9244); } 
BgL_test4269z00_9243 = 
(BgL_n1z00_3408>=BgL_l1z00_1828); } 
if(BgL_test4269z00_9243)
{ /* Ieee/string.scm 1200 */
BgL_b1z00_1832 = 
BGl_errorz00zz__errorz00(BgL_procz00_3402, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_186); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b1z00_1832 = BgL_start1z00_186; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b1z00_1832 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1249 */
 obj_t BgL_b2z00_1833;
{ /* Ieee/string.scm 1250 */
 obj_t BgL_procz00_3410;
BgL_procz00_3410 = BGl_symbol3499z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_188))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4272z00_9258;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3415;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_9259;
if(
INTEGERP(BgL_start2z00_188))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_9259 = BgL_start2z00_188
; }  else 
{ 
 obj_t BgL_auxz00_9262;
BgL_auxz00_9262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_188); 
FAILURE(BgL_auxz00_9262,BFALSE,BFALSE);} 
BgL_n1z00_3415 = 
(long)CINT(BgL_tmpz00_9259); } 
BgL_test4272z00_9258 = 
(BgL_n1z00_3415<0L); } 
if(BgL_test4272z00_9258)
{ /* Ieee/string.scm 1198 */
BgL_b2z00_1833 = 
BGl_errorz00zz__errorz00(BgL_procz00_3410, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_188); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4274z00_9270;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3416;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_9271;
if(
INTEGERP(BgL_start2z00_188))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_9271 = BgL_start2z00_188
; }  else 
{ 
 obj_t BgL_auxz00_9274;
BgL_auxz00_9274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3500z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_188); 
FAILURE(BgL_auxz00_9274,BFALSE,BFALSE);} 
BgL_n1z00_3416 = 
(long)CINT(BgL_tmpz00_9271); } 
BgL_test4274z00_9270 = 
(BgL_n1z00_3416>=BgL_l2z00_1829); } 
if(BgL_test4274z00_9270)
{ /* Ieee/string.scm 1200 */
BgL_b2z00_1833 = 
BGl_errorz00zz__errorz00(BgL_procz00_3410, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_188); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b2z00_1833 = BgL_start2z00_188; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b2z00_1833 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1250 */

{ 
 obj_t BgL_i1z00_1835; obj_t BgL_i2z00_1836;
{ /* Ieee/string.scm 1251 */
 long BgL_tmpz00_9283;
BgL_i1z00_1835 = BgL_b1z00_1832; 
BgL_i2z00_1836 = BgL_b2z00_1833; 
BgL_zc3z04anonymousza31782ze3z87_1837:
{ /* Ieee/string.scm 1254 */
 bool_t BgL_test4276z00_9284;
{ /* Ieee/string.scm 1254 */
 bool_t BgL_test4277z00_9285;
{ /* Ieee/string.scm 1254 */
 long BgL_n1z00_3418; long BgL_n2z00_3419;
{ /* Ieee/string.scm 1254 */
 obj_t BgL_tmpz00_9286;
if(
INTEGERP(BgL_i1z00_1835))
{ /* Ieee/string.scm 1254 */
BgL_tmpz00_9286 = BgL_i1z00_1835
; }  else 
{ 
 obj_t BgL_auxz00_9289;
BgL_auxz00_9289 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50881L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1835); 
FAILURE(BgL_auxz00_9289,BFALSE,BFALSE);} 
BgL_n1z00_3418 = 
(long)CINT(BgL_tmpz00_9286); } 
{ /* Ieee/string.scm 1254 */
 obj_t BgL_tmpz00_9294;
if(
INTEGERP(BgL_e1z00_1830))
{ /* Ieee/string.scm 1254 */
BgL_tmpz00_9294 = BgL_e1z00_1830
; }  else 
{ 
 obj_t BgL_auxz00_9297;
BgL_auxz00_9297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50884L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_1830); 
FAILURE(BgL_auxz00_9297,BFALSE,BFALSE);} 
BgL_n2z00_3419 = 
(long)CINT(BgL_tmpz00_9294); } 
BgL_test4277z00_9285 = 
(BgL_n1z00_3418==BgL_n2z00_3419); } 
if(BgL_test4277z00_9285)
{ /* Ieee/string.scm 1254 */
BgL_test4276z00_9284 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1254 */
 long BgL_n1z00_3420; long BgL_n2z00_3421;
{ /* Ieee/string.scm 1254 */
 obj_t BgL_tmpz00_9303;
if(
INTEGERP(BgL_i2z00_1836))
{ /* Ieee/string.scm 1254 */
BgL_tmpz00_9303 = BgL_i2z00_1836
; }  else 
{ 
 obj_t BgL_auxz00_9306;
BgL_auxz00_9306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50893L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1836); 
FAILURE(BgL_auxz00_9306,BFALSE,BFALSE);} 
BgL_n1z00_3420 = 
(long)CINT(BgL_tmpz00_9303); } 
{ /* Ieee/string.scm 1254 */
 obj_t BgL_tmpz00_9311;
if(
INTEGERP(BgL_e2z00_1831))
{ /* Ieee/string.scm 1254 */
BgL_tmpz00_9311 = BgL_e2z00_1831
; }  else 
{ 
 obj_t BgL_auxz00_9314;
BgL_auxz00_9314 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50896L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_1831); 
FAILURE(BgL_auxz00_9314,BFALSE,BFALSE);} 
BgL_n2z00_3421 = 
(long)CINT(BgL_tmpz00_9311); } 
BgL_test4276z00_9284 = 
(BgL_n1z00_3420==BgL_n2z00_3421); } } 
if(BgL_test4276z00_9284)
{ /* Ieee/string.scm 1255 */
 long BgL_za71za7_3422; long BgL_za72za7_3423;
{ /* Ieee/string.scm 1255 */
 obj_t BgL_tmpz00_9320;
if(
INTEGERP(BgL_i1z00_1835))
{ /* Ieee/string.scm 1255 */
BgL_tmpz00_9320 = BgL_i1z00_1835
; }  else 
{ 
 obj_t BgL_auxz00_9323;
BgL_auxz00_9323 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50912L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1835); 
FAILURE(BgL_auxz00_9323,BFALSE,BFALSE);} 
BgL_za71za7_3422 = 
(long)CINT(BgL_tmpz00_9320); } 
{ /* Ieee/string.scm 1255 */
 obj_t BgL_tmpz00_9328;
if(
INTEGERP(BgL_b1z00_1832))
{ /* Ieee/string.scm 1255 */
BgL_tmpz00_9328 = BgL_b1z00_1832
; }  else 
{ 
 obj_t BgL_auxz00_9331;
BgL_auxz00_9331 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50915L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1832); 
FAILURE(BgL_auxz00_9331,BFALSE,BFALSE);} 
BgL_za72za7_3423 = 
(long)CINT(BgL_tmpz00_9328); } 
BgL_tmpz00_9283 = 
(BgL_za71za7_3422-BgL_za72za7_3423); }  else 
{ /* Ieee/string.scm 1256 */
 bool_t BgL_test4284z00_9337;
{ /* Ieee/string.scm 1256 */
 unsigned char BgL_auxz00_9359; unsigned char BgL_tmpz00_9338;
{ /* Ieee/string.scm 1256 */
 unsigned char BgL_tmpz00_9360;
{ /* Ieee/string.scm 1256 */
 long BgL_kz00_3427;
{ /* Ieee/string.scm 1256 */
 obj_t BgL_tmpz00_9361;
if(
INTEGERP(BgL_i2z00_1836))
{ /* Ieee/string.scm 1256 */
BgL_tmpz00_9361 = BgL_i2z00_1836
; }  else 
{ 
 obj_t BgL_auxz00_9364;
BgL_auxz00_9364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50971L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1836); 
FAILURE(BgL_auxz00_9364,BFALSE,BFALSE);} 
BgL_kz00_3427 = 
(long)CINT(BgL_tmpz00_9361); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2529z00_4487;
BgL_l2529z00_4487 = 
STRING_LENGTH(BgL_s2z00_185); 
if(
BOUND_CHECK(BgL_kz00_3427, BgL_l2529z00_4487))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_9360 = 
STRING_REF(BgL_s2z00_185, BgL_kz00_3427)
; }  else 
{ 
 obj_t BgL_auxz00_9373;
BgL_auxz00_9373 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_185, 
(int)(BgL_l2529z00_4487), 
(int)(BgL_kz00_3427)); 
FAILURE(BgL_auxz00_9373,BFALSE,BFALSE);} } } 
BgL_auxz00_9359 = 
toupper(BgL_tmpz00_9360); } 
{ /* Ieee/string.scm 1256 */
 unsigned char BgL_tmpz00_9339;
{ /* Ieee/string.scm 1256 */
 long BgL_kz00_3425;
{ /* Ieee/string.scm 1256 */
 obj_t BgL_tmpz00_9340;
if(
INTEGERP(BgL_i1z00_1835))
{ /* Ieee/string.scm 1256 */
BgL_tmpz00_9340 = BgL_i1z00_1835
; }  else 
{ 
 obj_t BgL_auxz00_9343;
BgL_auxz00_9343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50952L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1835); 
FAILURE(BgL_auxz00_9343,BFALSE,BFALSE);} 
BgL_kz00_3425 = 
(long)CINT(BgL_tmpz00_9340); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2525z00_4483;
BgL_l2525z00_4483 = 
STRING_LENGTH(BgL_s1z00_184); 
if(
BOUND_CHECK(BgL_kz00_3425, BgL_l2525z00_4483))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_9339 = 
STRING_REF(BgL_s1z00_184, BgL_kz00_3425)
; }  else 
{ 
 obj_t BgL_auxz00_9352;
BgL_auxz00_9352 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_184, 
(int)(BgL_l2525z00_4483), 
(int)(BgL_kz00_3425)); 
FAILURE(BgL_auxz00_9352,BFALSE,BFALSE);} } } 
BgL_tmpz00_9338 = 
toupper(BgL_tmpz00_9339); } 
BgL_test4284z00_9337 = 
(BgL_tmpz00_9338==BgL_auxz00_9359); } 
if(BgL_test4284z00_9337)
{ 
 obj_t BgL_i2z00_9391; obj_t BgL_i1z00_9381;
{ 
 obj_t BgL_tmpz00_9382;
if(
INTEGERP(BgL_i1z00_1835))
{ /* Ieee/string.scm 1257 */
BgL_tmpz00_9382 = BgL_i1z00_1835
; }  else 
{ 
 obj_t BgL_auxz00_9385;
BgL_auxz00_9385 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(50993L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1835); 
FAILURE(BgL_auxz00_9385,BFALSE,BFALSE);} 
BgL_i1z00_9381 = 
ADDFX(BgL_tmpz00_9382, 
BINT(1L)); } 
{ 
 obj_t BgL_tmpz00_9392;
if(
INTEGERP(BgL_i2z00_1836))
{ /* Ieee/string.scm 1257 */
BgL_tmpz00_9392 = BgL_i2z00_1836
; }  else 
{ 
 obj_t BgL_auxz00_9395;
BgL_auxz00_9395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51004L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1836); 
FAILURE(BgL_auxz00_9395,BFALSE,BFALSE);} 
BgL_i2z00_9391 = 
ADDFX(BgL_tmpz00_9392, 
BINT(1L)); } 
BgL_i2z00_1836 = BgL_i2z00_9391; 
BgL_i1z00_1835 = BgL_i1z00_9381; 
goto BgL_zc3z04anonymousza31782ze3z87_1837;}  else 
{ /* Ieee/string.scm 1259 */
 long BgL_za71za7_3438; long BgL_za72za7_3439;
{ /* Ieee/string.scm 1259 */
 obj_t BgL_tmpz00_9401;
if(
INTEGERP(BgL_i1z00_1835))
{ /* Ieee/string.scm 1259 */
BgL_tmpz00_9401 = BgL_i1z00_1835
; }  else 
{ 
 obj_t BgL_auxz00_9404;
BgL_auxz00_9404 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51034L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1835); 
FAILURE(BgL_auxz00_9404,BFALSE,BFALSE);} 
BgL_za71za7_3438 = 
(long)CINT(BgL_tmpz00_9401); } 
{ /* Ieee/string.scm 1259 */
 obj_t BgL_tmpz00_9409;
if(
INTEGERP(BgL_b1z00_1832))
{ /* Ieee/string.scm 1259 */
BgL_tmpz00_9409 = BgL_b1z00_1832
; }  else 
{ 
 obj_t BgL_auxz00_9412;
BgL_auxz00_9412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51037L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1832); 
FAILURE(BgL_auxz00_9412,BFALSE,BFALSE);} 
BgL_za72za7_3439 = 
(long)CINT(BgL_tmpz00_9409); } 
BgL_tmpz00_9283 = 
(BgL_za71za7_3438-BgL_za72za7_3439); } } } 
return 
(int)(BgL_tmpz00_9283);} } } } } } } } } } 

}



/* _string-suffix-length */
obj_t BGl__stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t BgL_env1170z00_199, obj_t BgL_opt1169z00_198)
{
{ /* Ieee/string.scm 1264 */
{ /* Ieee/string.scm 1264 */
 obj_t BgL_s1z00_1849; obj_t BgL_s2z00_1850;
BgL_s1z00_1849 = 
VECTOR_REF(BgL_opt1169z00_198,0L); 
BgL_s2z00_1850 = 
VECTOR_REF(BgL_opt1169z00_198,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1169z00_198)) { case 2L : 

{ /* Ieee/string.scm 1264 */

{ /* Ieee/string.scm 1264 */
 int BgL_tmpz00_9421;
{ /* Ieee/string.scm 1264 */
 obj_t BgL_auxz00_9429; obj_t BgL_auxz00_9422;
if(
STRINGP(BgL_s2z00_1850))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9429 = BgL_s2z00_1850
; }  else 
{ 
 obj_t BgL_auxz00_9432;
BgL_auxz00_9432 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1850); 
FAILURE(BgL_auxz00_9432,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1849))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9422 = BgL_s1z00_1849
; }  else 
{ 
 obj_t BgL_auxz00_9425;
BgL_auxz00_9425 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1849); 
FAILURE(BgL_auxz00_9425,BFALSE,BFALSE);} 
BgL_tmpz00_9421 = 
BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_9422, BgL_auxz00_9429, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9421);} } break;case 3L : 

{ /* Ieee/string.scm 1264 */
 obj_t BgL_start1z00_1857;
BgL_start1z00_1857 = 
VECTOR_REF(BgL_opt1169z00_198,2L); 
{ /* Ieee/string.scm 1264 */

{ /* Ieee/string.scm 1264 */
 int BgL_tmpz00_9439;
{ /* Ieee/string.scm 1264 */
 obj_t BgL_auxz00_9447; obj_t BgL_auxz00_9440;
if(
STRINGP(BgL_s2z00_1850))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9447 = BgL_s2z00_1850
; }  else 
{ 
 obj_t BgL_auxz00_9450;
BgL_auxz00_9450 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1850); 
FAILURE(BgL_auxz00_9450,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1849))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9440 = BgL_s1z00_1849
; }  else 
{ 
 obj_t BgL_auxz00_9443;
BgL_auxz00_9443 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1849); 
FAILURE(BgL_auxz00_9443,BFALSE,BFALSE);} 
BgL_tmpz00_9439 = 
BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_9440, BgL_auxz00_9447, BgL_start1z00_1857, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9439);} } } break;case 4L : 

{ /* Ieee/string.scm 1264 */
 obj_t BgL_start1z00_1861;
BgL_start1z00_1861 = 
VECTOR_REF(BgL_opt1169z00_198,2L); 
{ /* Ieee/string.scm 1264 */
 obj_t BgL_end1z00_1862;
BgL_end1z00_1862 = 
VECTOR_REF(BgL_opt1169z00_198,3L); 
{ /* Ieee/string.scm 1264 */

{ /* Ieee/string.scm 1264 */
 int BgL_tmpz00_9458;
{ /* Ieee/string.scm 1264 */
 obj_t BgL_auxz00_9466; obj_t BgL_auxz00_9459;
if(
STRINGP(BgL_s2z00_1850))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9466 = BgL_s2z00_1850
; }  else 
{ 
 obj_t BgL_auxz00_9469;
BgL_auxz00_9469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1850); 
FAILURE(BgL_auxz00_9469,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1849))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9459 = BgL_s1z00_1849
; }  else 
{ 
 obj_t BgL_auxz00_9462;
BgL_auxz00_9462 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1849); 
FAILURE(BgL_auxz00_9462,BFALSE,BFALSE);} 
BgL_tmpz00_9458 = 
BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_9459, BgL_auxz00_9466, BgL_start1z00_1861, BgL_end1z00_1862, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9458);} } } } break;case 5L : 

{ /* Ieee/string.scm 1264 */
 obj_t BgL_start1z00_1865;
BgL_start1z00_1865 = 
VECTOR_REF(BgL_opt1169z00_198,2L); 
{ /* Ieee/string.scm 1264 */
 obj_t BgL_end1z00_1866;
BgL_end1z00_1866 = 
VECTOR_REF(BgL_opt1169z00_198,3L); 
{ /* Ieee/string.scm 1264 */
 obj_t BgL_start2z00_1867;
BgL_start2z00_1867 = 
VECTOR_REF(BgL_opt1169z00_198,4L); 
{ /* Ieee/string.scm 1264 */

{ /* Ieee/string.scm 1264 */
 int BgL_tmpz00_9478;
{ /* Ieee/string.scm 1264 */
 obj_t BgL_auxz00_9486; obj_t BgL_auxz00_9479;
if(
STRINGP(BgL_s2z00_1850))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9486 = BgL_s2z00_1850
; }  else 
{ 
 obj_t BgL_auxz00_9489;
BgL_auxz00_9489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1850); 
FAILURE(BgL_auxz00_9489,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1849))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9479 = BgL_s1z00_1849
; }  else 
{ 
 obj_t BgL_auxz00_9482;
BgL_auxz00_9482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1849); 
FAILURE(BgL_auxz00_9482,BFALSE,BFALSE);} 
BgL_tmpz00_9478 = 
BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_9479, BgL_auxz00_9486, BgL_start1z00_1865, BgL_end1z00_1866, BgL_start2z00_1867, BFALSE); } 
return 
BINT(BgL_tmpz00_9478);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1264 */
 obj_t BgL_start1z00_1869;
BgL_start1z00_1869 = 
VECTOR_REF(BgL_opt1169z00_198,2L); 
{ /* Ieee/string.scm 1264 */
 obj_t BgL_end1z00_1870;
BgL_end1z00_1870 = 
VECTOR_REF(BgL_opt1169z00_198,3L); 
{ /* Ieee/string.scm 1264 */
 obj_t BgL_start2z00_1871;
BgL_start2z00_1871 = 
VECTOR_REF(BgL_opt1169z00_198,4L); 
{ /* Ieee/string.scm 1264 */
 obj_t BgL_end2z00_1872;
BgL_end2z00_1872 = 
VECTOR_REF(BgL_opt1169z00_198,5L); 
{ /* Ieee/string.scm 1264 */

{ /* Ieee/string.scm 1264 */
 int BgL_tmpz00_9499;
{ /* Ieee/string.scm 1264 */
 obj_t BgL_auxz00_9507; obj_t BgL_auxz00_9500;
if(
STRINGP(BgL_s2z00_1850))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9507 = BgL_s2z00_1850
; }  else 
{ 
 obj_t BgL_auxz00_9510;
BgL_auxz00_9510 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1850); 
FAILURE(BgL_auxz00_9510,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1849))
{ /* Ieee/string.scm 1264 */
BgL_auxz00_9500 = BgL_s1z00_1849
; }  else 
{ 
 obj_t BgL_auxz00_9503;
BgL_auxz00_9503 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51271L), BGl_string3503z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1849); 
FAILURE(BgL_auxz00_9503,BFALSE,BFALSE);} 
BgL_tmpz00_9499 = 
BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(BgL_auxz00_9500, BgL_auxz00_9507, BgL_start1z00_1869, BgL_end1z00_1870, BgL_start2z00_1871, BgL_end2z00_1872); } 
return 
BINT(BgL_tmpz00_9499);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3501z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1169z00_198)));} } } } 

}



/* string-suffix-length */
BGL_EXPORTED_DEF int BGl_stringzd2suffixzd2lengthz00zz__r4_strings_6_7z00(obj_t BgL_s1z00_192, obj_t BgL_s2z00_193, obj_t BgL_start1z00_194, obj_t BgL_end1z00_195, obj_t BgL_start2z00_196, obj_t BgL_end2z00_197)
{
{ /* Ieee/string.scm 1264 */
{ /* Ieee/string.scm 1266 */
 long BgL_l1z00_1874;
BgL_l1z00_1874 = 
STRING_LENGTH(BgL_s1z00_192); 
{ /* Ieee/string.scm 1266 */
 long BgL_l2z00_1875;
BgL_l2z00_1875 = 
STRING_LENGTH(BgL_s2z00_193); 
{ /* Ieee/string.scm 1267 */
 obj_t BgL_b1z00_1876;
{ /* Ieee/string.scm 1268 */
 obj_t BgL_procz00_3442;
BgL_procz00_3442 = BGl_symbol3504z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_195))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4304z00_9525;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3447;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_9526;
if(
INTEGERP(BgL_end1z00_195))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_9526 = BgL_end1z00_195
; }  else 
{ 
 obj_t BgL_auxz00_9529;
BgL_auxz00_9529 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_195); 
FAILURE(BgL_auxz00_9529,BFALSE,BFALSE);} 
BgL_n1z00_3447 = 
(long)CINT(BgL_tmpz00_9526); } 
BgL_test4304z00_9525 = 
(BgL_n1z00_3447<=0L); } 
if(BgL_test4304z00_9525)
{ /* Ieee/string.scm 1212 */
BgL_b1z00_1876 = 
BGl_errorz00zz__errorz00(BgL_procz00_3442, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_195); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4306z00_9537;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3448;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_9538;
if(
INTEGERP(BgL_end1z00_195))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_9538 = BgL_end1z00_195
; }  else 
{ 
 obj_t BgL_auxz00_9541;
BgL_auxz00_9541 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_195); 
FAILURE(BgL_auxz00_9541,BFALSE,BFALSE);} 
BgL_n1z00_3448 = 
(long)CINT(BgL_tmpz00_9538); } 
BgL_test4306z00_9537 = 
(BgL_n1z00_3448>BgL_l1z00_1874); } 
if(BgL_test4306z00_9537)
{ /* Ieee/string.scm 1214 */
BgL_b1z00_1876 = 
BGl_errorz00zz__errorz00(BgL_procz00_3442, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_195); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b1z00_1876 = BgL_end1z00_195; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b1z00_1876 = 
BINT(BgL_l1z00_1874); } } 
{ /* Ieee/string.scm 1268 */
 obj_t BgL_b2z00_1877;
{ /* Ieee/string.scm 1269 */
 obj_t BgL_procz00_3450;
BgL_procz00_3450 = BGl_symbol3504z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_197))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4309z00_9552;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3455;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_9553;
if(
INTEGERP(BgL_end2z00_197))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_9553 = BgL_end2z00_197
; }  else 
{ 
 obj_t BgL_auxz00_9556;
BgL_auxz00_9556 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_197); 
FAILURE(BgL_auxz00_9556,BFALSE,BFALSE);} 
BgL_n1z00_3455 = 
(long)CINT(BgL_tmpz00_9553); } 
BgL_test4309z00_9552 = 
(BgL_n1z00_3455<=0L); } 
if(BgL_test4309z00_9552)
{ /* Ieee/string.scm 1212 */
BgL_b2z00_1877 = 
BGl_errorz00zz__errorz00(BgL_procz00_3450, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_197); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4311z00_9564;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3456;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_9565;
if(
INTEGERP(BgL_end2z00_197))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_9565 = BgL_end2z00_197
; }  else 
{ 
 obj_t BgL_auxz00_9568;
BgL_auxz00_9568 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_197); 
FAILURE(BgL_auxz00_9568,BFALSE,BFALSE);} 
BgL_n1z00_3456 = 
(long)CINT(BgL_tmpz00_9565); } 
BgL_test4311z00_9564 = 
(BgL_n1z00_3456>BgL_l2z00_1875); } 
if(BgL_test4311z00_9564)
{ /* Ieee/string.scm 1214 */
BgL_b2z00_1877 = 
BGl_errorz00zz__errorz00(BgL_procz00_3450, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_197); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b2z00_1877 = BgL_end2z00_197; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b2z00_1877 = 
BINT(BgL_l2z00_1875); } } 
{ /* Ieee/string.scm 1269 */
 obj_t BgL_e1z00_1878;
{ /* Ieee/string.scm 1270 */
 obj_t BgL_procz00_3458;
BgL_procz00_3458 = BGl_symbol3504z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_194))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4314z00_9579;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3463;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_9580;
if(
INTEGERP(BgL_start1z00_194))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_9580 = BgL_start1z00_194
; }  else 
{ 
 obj_t BgL_auxz00_9583;
BgL_auxz00_9583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_194); 
FAILURE(BgL_auxz00_9583,BFALSE,BFALSE);} 
BgL_n1z00_3463 = 
(long)CINT(BgL_tmpz00_9580); } 
BgL_test4314z00_9579 = 
(BgL_n1z00_3463<0L); } 
if(BgL_test4314z00_9579)
{ /* Ieee/string.scm 1198 */
BgL_e1z00_1878 = 
BGl_errorz00zz__errorz00(BgL_procz00_3458, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_194); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4316z00_9591;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3464;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_9592;
if(
INTEGERP(BgL_start1z00_194))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_9592 = BgL_start1z00_194
; }  else 
{ 
 obj_t BgL_auxz00_9595;
BgL_auxz00_9595 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_194); 
FAILURE(BgL_auxz00_9595,BFALSE,BFALSE);} 
BgL_n1z00_3464 = 
(long)CINT(BgL_tmpz00_9592); } 
BgL_test4316z00_9591 = 
(BgL_n1z00_3464>=BgL_l1z00_1874); } 
if(BgL_test4316z00_9591)
{ /* Ieee/string.scm 1200 */
BgL_e1z00_1878 = 
BGl_errorz00zz__errorz00(BgL_procz00_3458, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_194); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e1z00_1878 = BgL_start1z00_194; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e1z00_1878 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1270 */
 obj_t BgL_e2z00_1879;
{ /* Ieee/string.scm 1271 */
 obj_t BgL_procz00_3466;
BgL_procz00_3466 = BGl_symbol3504z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_196))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4319z00_9606;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3471;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_9607;
if(
INTEGERP(BgL_start2z00_196))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_9607 = BgL_start2z00_196
; }  else 
{ 
 obj_t BgL_auxz00_9610;
BgL_auxz00_9610 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_196); 
FAILURE(BgL_auxz00_9610,BFALSE,BFALSE);} 
BgL_n1z00_3471 = 
(long)CINT(BgL_tmpz00_9607); } 
BgL_test4319z00_9606 = 
(BgL_n1z00_3471<0L); } 
if(BgL_test4319z00_9606)
{ /* Ieee/string.scm 1198 */
BgL_e2z00_1879 = 
BGl_errorz00zz__errorz00(BgL_procz00_3466, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_196); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4321z00_9618;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3472;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_9619;
if(
INTEGERP(BgL_start2z00_196))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_9619 = BgL_start2z00_196
; }  else 
{ 
 obj_t BgL_auxz00_9622;
BgL_auxz00_9622 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_196); 
FAILURE(BgL_auxz00_9622,BFALSE,BFALSE);} 
BgL_n1z00_3472 = 
(long)CINT(BgL_tmpz00_9619); } 
BgL_test4321z00_9618 = 
(BgL_n1z00_3472>=BgL_l2z00_1875); } 
if(BgL_test4321z00_9618)
{ /* Ieee/string.scm 1200 */
BgL_e2z00_1879 = 
BGl_errorz00zz__errorz00(BgL_procz00_3466, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_196); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e2z00_1879 = BgL_start2z00_196; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e2z00_1879 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1271 */

{ 
 long BgL_i1z00_1883; long BgL_i2z00_1884;
{ /* Ieee/string.scm 1272 */
 long BgL_tmpz00_9631;
{ /* Ieee/string.scm 1272 */
 long BgL_za71za7_3474;
{ /* Ieee/string.scm 1272 */
 obj_t BgL_tmpz00_9700;
if(
INTEGERP(BgL_b1z00_1876))
{ /* Ieee/string.scm 1272 */
BgL_tmpz00_9700 = BgL_b1z00_1876
; }  else 
{ 
 obj_t BgL_auxz00_9703;
BgL_auxz00_9703 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51731L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1876); 
FAILURE(BgL_auxz00_9703,BFALSE,BFALSE);} 
BgL_za71za7_3474 = 
(long)CINT(BgL_tmpz00_9700); } 
BgL_i1z00_1883 = 
(BgL_za71za7_3474-1L); } 
{ /* Ieee/string.scm 1273 */
 long BgL_za71za7_3475;
{ /* Ieee/string.scm 1273 */
 obj_t BgL_tmpz00_9709;
if(
INTEGERP(BgL_b2z00_1877))
{ /* Ieee/string.scm 1273 */
BgL_tmpz00_9709 = BgL_b2z00_1877
; }  else 
{ 
 obj_t BgL_auxz00_9712;
BgL_auxz00_9712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51750L), BGl_string3505z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b2z00_1877); 
FAILURE(BgL_auxz00_9712,BFALSE,BFALSE);} 
BgL_za71za7_3475 = 
(long)CINT(BgL_tmpz00_9709); } 
BgL_i2z00_1884 = 
(BgL_za71za7_3475-1L); } 
BgL_zc3z04anonymousza31793ze3z87_1885:
{ /* Ieee/string.scm 1275 */
 bool_t BgL_test4323z00_9632;
{ /* Ieee/string.scm 1275 */
 bool_t BgL_test4324z00_9633;
{ /* Ieee/string.scm 1275 */
 long BgL_n2z00_3477;
{ /* Ieee/string.scm 1275 */
 obj_t BgL_tmpz00_9634;
if(
INTEGERP(BgL_e1z00_1878))
{ /* Ieee/string.scm 1275 */
BgL_tmpz00_9634 = BgL_e1z00_1878
; }  else 
{ 
 obj_t BgL_auxz00_9637;
BgL_auxz00_9637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51784L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_1878); 
FAILURE(BgL_auxz00_9637,BFALSE,BFALSE);} 
BgL_n2z00_3477 = 
(long)CINT(BgL_tmpz00_9634); } 
BgL_test4324z00_9633 = 
(BgL_i1z00_1883<BgL_n2z00_3477); } 
if(BgL_test4324z00_9633)
{ /* Ieee/string.scm 1275 */
BgL_test4323z00_9632 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1275 */
 long BgL_n2z00_3479;
{ /* Ieee/string.scm 1275 */
 obj_t BgL_tmpz00_9643;
if(
INTEGERP(BgL_e2z00_1879))
{ /* Ieee/string.scm 1275 */
BgL_tmpz00_9643 = BgL_e2z00_1879
; }  else 
{ 
 obj_t BgL_auxz00_9646;
BgL_auxz00_9646 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51796L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_1879); 
FAILURE(BgL_auxz00_9646,BFALSE,BFALSE);} 
BgL_n2z00_3479 = 
(long)CINT(BgL_tmpz00_9643); } 
BgL_test4323z00_9632 = 
(BgL_i2z00_1884<BgL_n2z00_3479); } } 
if(BgL_test4323z00_9632)
{ /* Ieee/string.scm 1276 */
 long BgL_za71za7_3481;
{ /* Ieee/string.scm 1276 */
 obj_t BgL_tmpz00_9652;
if(
INTEGERP(BgL_b1z00_1876))
{ /* Ieee/string.scm 1276 */
BgL_tmpz00_9652 = BgL_b1z00_1876
; }  else 
{ 
 obj_t BgL_auxz00_9655;
BgL_auxz00_9655 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51812L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1876); 
FAILURE(BgL_auxz00_9655,BFALSE,BFALSE);} 
BgL_za71za7_3481 = 
(long)CINT(BgL_tmpz00_9652); } 
BgL_tmpz00_9631 = 
(BgL_za71za7_3481-
(BgL_i1z00_1883+1L)); }  else 
{ /* Ieee/string.scm 1277 */
 bool_t BgL_test4328z00_9662;
{ /* Ieee/string.scm 1277 */
 unsigned char BgL_auxz00_9674; unsigned char BgL_tmpz00_9663;
{ /* Ieee/string.scm 331 */
 long BgL_l2537z00_4495;
BgL_l2537z00_4495 = 
STRING_LENGTH(BgL_s2z00_193); 
if(
BOUND_CHECK(BgL_i2z00_1884, BgL_l2537z00_4495))
{ /* Ieee/string.scm 331 */
BgL_auxz00_9674 = 
STRING_REF(BgL_s2z00_193, BgL_i2z00_1884)
; }  else 
{ 
 obj_t BgL_auxz00_9679;
BgL_auxz00_9679 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_193, 
(int)(BgL_l2537z00_4495), 
(int)(BgL_i2z00_1884)); 
FAILURE(BgL_auxz00_9679,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 331 */
 long BgL_l2533z00_4491;
BgL_l2533z00_4491 = 
STRING_LENGTH(BgL_s1z00_192); 
if(
BOUND_CHECK(BgL_i1z00_1883, BgL_l2533z00_4491))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_9663 = 
STRING_REF(BgL_s1z00_192, BgL_i1z00_1883)
; }  else 
{ 
 obj_t BgL_auxz00_9668;
BgL_auxz00_9668 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_192, 
(int)(BgL_l2533z00_4491), 
(int)(BgL_i1z00_1883)); 
FAILURE(BgL_auxz00_9668,BFALSE,BFALSE);} } 
BgL_test4328z00_9662 = 
(BgL_tmpz00_9663==BgL_auxz00_9674); } 
if(BgL_test4328z00_9662)
{ 
 long BgL_i2z00_9688; long BgL_i1z00_9686;
BgL_i1z00_9686 = 
(BgL_i1z00_1883-1L); 
BgL_i2z00_9688 = 
(BgL_i2z00_1884-1L); 
BgL_i2z00_1884 = BgL_i2z00_9688; 
BgL_i1z00_1883 = BgL_i1z00_9686; 
goto BgL_zc3z04anonymousza31793ze3z87_1885;}  else 
{ /* Ieee/string.scm 1280 */
 long BgL_za71za7_3492;
{ /* Ieee/string.scm 1280 */
 obj_t BgL_tmpz00_9690;
if(
INTEGERP(BgL_b1z00_1876))
{ /* Ieee/string.scm 1280 */
BgL_tmpz00_9690 = BgL_b1z00_1876
; }  else 
{ 
 obj_t BgL_auxz00_9693;
BgL_auxz00_9693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(51939L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1876); 
FAILURE(BgL_auxz00_9693,BFALSE,BFALSE);} 
BgL_za71za7_3492 = 
(long)CINT(BgL_tmpz00_9690); } 
BgL_tmpz00_9631 = 
(BgL_za71za7_3492-
(BgL_i1z00_1883+1L)); } } } 
return 
(int)(BgL_tmpz00_9631);} } } } } } } } } } 

}



/* _string-suffix-length-ci */
obj_t BGl__stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t BgL_env1174z00_207, obj_t BgL_opt1173z00_206)
{
{ /* Ieee/string.scm 1285 */
{ /* Ieee/string.scm 1285 */
 obj_t BgL_s1z00_1899; obj_t BgL_s2z00_1900;
BgL_s1z00_1899 = 
VECTOR_REF(BgL_opt1173z00_206,0L); 
BgL_s2z00_1900 = 
VECTOR_REF(BgL_opt1173z00_206,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1173z00_206)) { case 2L : 

{ /* Ieee/string.scm 1285 */

{ /* Ieee/string.scm 1285 */
 int BgL_tmpz00_9721;
{ /* Ieee/string.scm 1285 */
 obj_t BgL_auxz00_9729; obj_t BgL_auxz00_9722;
if(
STRINGP(BgL_s2z00_1900))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9729 = BgL_s2z00_1900
; }  else 
{ 
 obj_t BgL_auxz00_9732;
BgL_auxz00_9732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1900); 
FAILURE(BgL_auxz00_9732,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1899))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9722 = BgL_s1z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_9725;
BgL_auxz00_9725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1899); 
FAILURE(BgL_auxz00_9725,BFALSE,BFALSE);} 
BgL_tmpz00_9721 = 
BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9722, BgL_auxz00_9729, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9721);} } break;case 3L : 

{ /* Ieee/string.scm 1285 */
 obj_t BgL_start1z00_1907;
BgL_start1z00_1907 = 
VECTOR_REF(BgL_opt1173z00_206,2L); 
{ /* Ieee/string.scm 1285 */

{ /* Ieee/string.scm 1285 */
 int BgL_tmpz00_9739;
{ /* Ieee/string.scm 1285 */
 obj_t BgL_auxz00_9747; obj_t BgL_auxz00_9740;
if(
STRINGP(BgL_s2z00_1900))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9747 = BgL_s2z00_1900
; }  else 
{ 
 obj_t BgL_auxz00_9750;
BgL_auxz00_9750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1900); 
FAILURE(BgL_auxz00_9750,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1899))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9740 = BgL_s1z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_9743;
BgL_auxz00_9743 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1899); 
FAILURE(BgL_auxz00_9743,BFALSE,BFALSE);} 
BgL_tmpz00_9739 = 
BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9740, BgL_auxz00_9747, BgL_start1z00_1907, BFALSE, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9739);} } } break;case 4L : 

{ /* Ieee/string.scm 1285 */
 obj_t BgL_start1z00_1911;
BgL_start1z00_1911 = 
VECTOR_REF(BgL_opt1173z00_206,2L); 
{ /* Ieee/string.scm 1285 */
 obj_t BgL_end1z00_1912;
BgL_end1z00_1912 = 
VECTOR_REF(BgL_opt1173z00_206,3L); 
{ /* Ieee/string.scm 1285 */

{ /* Ieee/string.scm 1285 */
 int BgL_tmpz00_9758;
{ /* Ieee/string.scm 1285 */
 obj_t BgL_auxz00_9766; obj_t BgL_auxz00_9759;
if(
STRINGP(BgL_s2z00_1900))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9766 = BgL_s2z00_1900
; }  else 
{ 
 obj_t BgL_auxz00_9769;
BgL_auxz00_9769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1900); 
FAILURE(BgL_auxz00_9769,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1899))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9759 = BgL_s1z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_9762;
BgL_auxz00_9762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1899); 
FAILURE(BgL_auxz00_9762,BFALSE,BFALSE);} 
BgL_tmpz00_9758 = 
BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9759, BgL_auxz00_9766, BgL_start1z00_1911, BgL_end1z00_1912, BFALSE, BFALSE); } 
return 
BINT(BgL_tmpz00_9758);} } } } break;case 5L : 

{ /* Ieee/string.scm 1285 */
 obj_t BgL_start1z00_1915;
BgL_start1z00_1915 = 
VECTOR_REF(BgL_opt1173z00_206,2L); 
{ /* Ieee/string.scm 1285 */
 obj_t BgL_end1z00_1916;
BgL_end1z00_1916 = 
VECTOR_REF(BgL_opt1173z00_206,3L); 
{ /* Ieee/string.scm 1285 */
 obj_t BgL_start2z00_1917;
BgL_start2z00_1917 = 
VECTOR_REF(BgL_opt1173z00_206,4L); 
{ /* Ieee/string.scm 1285 */

{ /* Ieee/string.scm 1285 */
 int BgL_tmpz00_9778;
{ /* Ieee/string.scm 1285 */
 obj_t BgL_auxz00_9786; obj_t BgL_auxz00_9779;
if(
STRINGP(BgL_s2z00_1900))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9786 = BgL_s2z00_1900
; }  else 
{ 
 obj_t BgL_auxz00_9789;
BgL_auxz00_9789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1900); 
FAILURE(BgL_auxz00_9789,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1899))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9779 = BgL_s1z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_9782;
BgL_auxz00_9782 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1899); 
FAILURE(BgL_auxz00_9782,BFALSE,BFALSE);} 
BgL_tmpz00_9778 = 
BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9779, BgL_auxz00_9786, BgL_start1z00_1915, BgL_end1z00_1916, BgL_start2z00_1917, BFALSE); } 
return 
BINT(BgL_tmpz00_9778);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1285 */
 obj_t BgL_start1z00_1919;
BgL_start1z00_1919 = 
VECTOR_REF(BgL_opt1173z00_206,2L); 
{ /* Ieee/string.scm 1285 */
 obj_t BgL_end1z00_1920;
BgL_end1z00_1920 = 
VECTOR_REF(BgL_opt1173z00_206,3L); 
{ /* Ieee/string.scm 1285 */
 obj_t BgL_start2z00_1921;
BgL_start2z00_1921 = 
VECTOR_REF(BgL_opt1173z00_206,4L); 
{ /* Ieee/string.scm 1285 */
 obj_t BgL_end2z00_1922;
BgL_end2z00_1922 = 
VECTOR_REF(BgL_opt1173z00_206,5L); 
{ /* Ieee/string.scm 1285 */

{ /* Ieee/string.scm 1285 */
 int BgL_tmpz00_9799;
{ /* Ieee/string.scm 1285 */
 obj_t BgL_auxz00_9807; obj_t BgL_auxz00_9800;
if(
STRINGP(BgL_s2z00_1900))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9807 = BgL_s2z00_1900
; }  else 
{ 
 obj_t BgL_auxz00_9810;
BgL_auxz00_9810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1900); 
FAILURE(BgL_auxz00_9810,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1899))
{ /* Ieee/string.scm 1285 */
BgL_auxz00_9800 = BgL_s1z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_9803;
BgL_auxz00_9803 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52182L), BGl_string3508z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1899); 
FAILURE(BgL_auxz00_9803,BFALSE,BFALSE);} 
BgL_tmpz00_9799 = 
BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(BgL_auxz00_9800, BgL_auxz00_9807, BgL_start1z00_1919, BgL_end1z00_1920, BgL_start2z00_1921, BgL_end2z00_1922); } 
return 
BINT(BgL_tmpz00_9799);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3506z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1173z00_206)));} } } } 

}



/* string-suffix-length-ci */
BGL_EXPORTED_DEF int BGl_stringzd2suffixzd2lengthzd2cizd2zz__r4_strings_6_7z00(obj_t BgL_s1z00_200, obj_t BgL_s2z00_201, obj_t BgL_start1z00_202, obj_t BgL_end1z00_203, obj_t BgL_start2z00_204, obj_t BgL_end2z00_205)
{
{ /* Ieee/string.scm 1285 */
{ /* Ieee/string.scm 1287 */
 long BgL_l1z00_1924;
BgL_l1z00_1924 = 
STRING_LENGTH(BgL_s1z00_200); 
{ /* Ieee/string.scm 1287 */
 long BgL_l2z00_1925;
BgL_l2z00_1925 = 
STRING_LENGTH(BgL_s2z00_201); 
{ /* Ieee/string.scm 1288 */
 obj_t BgL_b1z00_1926;
{ /* Ieee/string.scm 1289 */
 obj_t BgL_procz00_3496;
BgL_procz00_3496 = BGl_symbol3509z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_203))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4345z00_9825;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3501;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_9826;
if(
INTEGERP(BgL_end1z00_203))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_9826 = BgL_end1z00_203
; }  else 
{ 
 obj_t BgL_auxz00_9829;
BgL_auxz00_9829 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_203); 
FAILURE(BgL_auxz00_9829,BFALSE,BFALSE);} 
BgL_n1z00_3501 = 
(long)CINT(BgL_tmpz00_9826); } 
BgL_test4345z00_9825 = 
(BgL_n1z00_3501<=0L); } 
if(BgL_test4345z00_9825)
{ /* Ieee/string.scm 1212 */
BgL_b1z00_1926 = 
BGl_errorz00zz__errorz00(BgL_procz00_3496, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_203); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4347z00_9837;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3502;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_9838;
if(
INTEGERP(BgL_end1z00_203))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_9838 = BgL_end1z00_203
; }  else 
{ 
 obj_t BgL_auxz00_9841;
BgL_auxz00_9841 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_203); 
FAILURE(BgL_auxz00_9841,BFALSE,BFALSE);} 
BgL_n1z00_3502 = 
(long)CINT(BgL_tmpz00_9838); } 
BgL_test4347z00_9837 = 
(BgL_n1z00_3502>BgL_l1z00_1924); } 
if(BgL_test4347z00_9837)
{ /* Ieee/string.scm 1214 */
BgL_b1z00_1926 = 
BGl_errorz00zz__errorz00(BgL_procz00_3496, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_203); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b1z00_1926 = BgL_end1z00_203; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b1z00_1926 = 
BINT(BgL_l1z00_1924); } } 
{ /* Ieee/string.scm 1289 */
 obj_t BgL_b2z00_1927;
{ /* Ieee/string.scm 1290 */
 obj_t BgL_procz00_3504;
BgL_procz00_3504 = BGl_symbol3509z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_205))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4350z00_9852;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3509;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_9853;
if(
INTEGERP(BgL_end2z00_205))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_9853 = BgL_end2z00_205
; }  else 
{ 
 obj_t BgL_auxz00_9856;
BgL_auxz00_9856 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_205); 
FAILURE(BgL_auxz00_9856,BFALSE,BFALSE);} 
BgL_n1z00_3509 = 
(long)CINT(BgL_tmpz00_9853); } 
BgL_test4350z00_9852 = 
(BgL_n1z00_3509<=0L); } 
if(BgL_test4350z00_9852)
{ /* Ieee/string.scm 1212 */
BgL_b2z00_1927 = 
BGl_errorz00zz__errorz00(BgL_procz00_3504, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_205); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4352z00_9864;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3510;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_9865;
if(
INTEGERP(BgL_end2z00_205))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_9865 = BgL_end2z00_205
; }  else 
{ 
 obj_t BgL_auxz00_9868;
BgL_auxz00_9868 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_205); 
FAILURE(BgL_auxz00_9868,BFALSE,BFALSE);} 
BgL_n1z00_3510 = 
(long)CINT(BgL_tmpz00_9865); } 
BgL_test4352z00_9864 = 
(BgL_n1z00_3510>BgL_l2z00_1925); } 
if(BgL_test4352z00_9864)
{ /* Ieee/string.scm 1214 */
BgL_b2z00_1927 = 
BGl_errorz00zz__errorz00(BgL_procz00_3504, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_205); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b2z00_1927 = BgL_end2z00_205; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b2z00_1927 = 
BINT(BgL_l2z00_1925); } } 
{ /* Ieee/string.scm 1290 */
 obj_t BgL_e1z00_1928;
{ /* Ieee/string.scm 1291 */
 obj_t BgL_procz00_3512;
BgL_procz00_3512 = BGl_symbol3509z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_202))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4355z00_9879;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3517;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_9880;
if(
INTEGERP(BgL_start1z00_202))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_9880 = BgL_start1z00_202
; }  else 
{ 
 obj_t BgL_auxz00_9883;
BgL_auxz00_9883 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_202); 
FAILURE(BgL_auxz00_9883,BFALSE,BFALSE);} 
BgL_n1z00_3517 = 
(long)CINT(BgL_tmpz00_9880); } 
BgL_test4355z00_9879 = 
(BgL_n1z00_3517<0L); } 
if(BgL_test4355z00_9879)
{ /* Ieee/string.scm 1198 */
BgL_e1z00_1928 = 
BGl_errorz00zz__errorz00(BgL_procz00_3512, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_202); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4357z00_9891;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3518;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_9892;
if(
INTEGERP(BgL_start1z00_202))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_9892 = BgL_start1z00_202
; }  else 
{ 
 obj_t BgL_auxz00_9895;
BgL_auxz00_9895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_202); 
FAILURE(BgL_auxz00_9895,BFALSE,BFALSE);} 
BgL_n1z00_3518 = 
(long)CINT(BgL_tmpz00_9892); } 
BgL_test4357z00_9891 = 
(BgL_n1z00_3518>=BgL_l1z00_1924); } 
if(BgL_test4357z00_9891)
{ /* Ieee/string.scm 1200 */
BgL_e1z00_1928 = 
BGl_errorz00zz__errorz00(BgL_procz00_3512, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_202); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e1z00_1928 = BgL_start1z00_202; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e1z00_1928 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1291 */
 obj_t BgL_e2z00_1929;
{ /* Ieee/string.scm 1292 */
 obj_t BgL_procz00_3520;
BgL_procz00_3520 = BGl_symbol3509z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_204))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4360z00_9906;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3525;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_9907;
if(
INTEGERP(BgL_start2z00_204))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_9907 = BgL_start2z00_204
; }  else 
{ 
 obj_t BgL_auxz00_9910;
BgL_auxz00_9910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_204); 
FAILURE(BgL_auxz00_9910,BFALSE,BFALSE);} 
BgL_n1z00_3525 = 
(long)CINT(BgL_tmpz00_9907); } 
BgL_test4360z00_9906 = 
(BgL_n1z00_3525<0L); } 
if(BgL_test4360z00_9906)
{ /* Ieee/string.scm 1198 */
BgL_e2z00_1929 = 
BGl_errorz00zz__errorz00(BgL_procz00_3520, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_204); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4362z00_9918;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3526;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_9919;
if(
INTEGERP(BgL_start2z00_204))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_9919 = BgL_start2z00_204
; }  else 
{ 
 obj_t BgL_auxz00_9922;
BgL_auxz00_9922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_204); 
FAILURE(BgL_auxz00_9922,BFALSE,BFALSE);} 
BgL_n1z00_3526 = 
(long)CINT(BgL_tmpz00_9919); } 
BgL_test4362z00_9918 = 
(BgL_n1z00_3526>=BgL_l2z00_1925); } 
if(BgL_test4362z00_9918)
{ /* Ieee/string.scm 1200 */
BgL_e2z00_1929 = 
BGl_errorz00zz__errorz00(BgL_procz00_3520, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_204); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e2z00_1929 = BgL_start2z00_204; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e2z00_1929 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1292 */

{ 
 long BgL_i1z00_1933; long BgL_i2z00_1934;
{ /* Ieee/string.scm 1293 */
 long BgL_tmpz00_9931;
{ /* Ieee/string.scm 1293 */
 long BgL_za71za7_3528;
{ /* Ieee/string.scm 1293 */
 obj_t BgL_tmpz00_10004;
if(
INTEGERP(BgL_b1z00_1926))
{ /* Ieee/string.scm 1293 */
BgL_tmpz00_10004 = BgL_b1z00_1926
; }  else 
{ 
 obj_t BgL_auxz00_10007;
BgL_auxz00_10007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52660L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1926); 
FAILURE(BgL_auxz00_10007,BFALSE,BFALSE);} 
BgL_za71za7_3528 = 
(long)CINT(BgL_tmpz00_10004); } 
BgL_i1z00_1933 = 
(BgL_za71za7_3528-1L); } 
{ /* Ieee/string.scm 1294 */
 long BgL_za71za7_3529;
{ /* Ieee/string.scm 1294 */
 obj_t BgL_tmpz00_10013;
if(
INTEGERP(BgL_b2z00_1927))
{ /* Ieee/string.scm 1294 */
BgL_tmpz00_10013 = BgL_b2z00_1927
; }  else 
{ 
 obj_t BgL_auxz00_10016;
BgL_auxz00_10016 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52679L), BGl_string3510z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b2z00_1927); 
FAILURE(BgL_auxz00_10016,BFALSE,BFALSE);} 
BgL_za71za7_3529 = 
(long)CINT(BgL_tmpz00_10013); } 
BgL_i2z00_1934 = 
(BgL_za71za7_3529-1L); } 
BgL_zc3z04anonymousza31806ze3z87_1935:
{ /* Ieee/string.scm 1296 */
 bool_t BgL_test4364z00_9932;
{ /* Ieee/string.scm 1296 */
 bool_t BgL_test4365z00_9933;
{ /* Ieee/string.scm 1296 */
 long BgL_n2z00_3531;
{ /* Ieee/string.scm 1296 */
 obj_t BgL_tmpz00_9934;
if(
INTEGERP(BgL_e1z00_1928))
{ /* Ieee/string.scm 1296 */
BgL_tmpz00_9934 = BgL_e1z00_1928
; }  else 
{ 
 obj_t BgL_auxz00_9937;
BgL_auxz00_9937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52713L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_1928); 
FAILURE(BgL_auxz00_9937,BFALSE,BFALSE);} 
BgL_n2z00_3531 = 
(long)CINT(BgL_tmpz00_9934); } 
BgL_test4365z00_9933 = 
(BgL_i1z00_1933<BgL_n2z00_3531); } 
if(BgL_test4365z00_9933)
{ /* Ieee/string.scm 1296 */
BgL_test4364z00_9932 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1296 */
 long BgL_n2z00_3533;
{ /* Ieee/string.scm 1296 */
 obj_t BgL_tmpz00_9943;
if(
INTEGERP(BgL_e2z00_1929))
{ /* Ieee/string.scm 1296 */
BgL_tmpz00_9943 = BgL_e2z00_1929
; }  else 
{ 
 obj_t BgL_auxz00_9946;
BgL_auxz00_9946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52725L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_1929); 
FAILURE(BgL_auxz00_9946,BFALSE,BFALSE);} 
BgL_n2z00_3533 = 
(long)CINT(BgL_tmpz00_9943); } 
BgL_test4364z00_9932 = 
(BgL_i2z00_1934<BgL_n2z00_3533); } } 
if(BgL_test4364z00_9932)
{ /* Ieee/string.scm 1297 */
 long BgL_za71za7_3535;
{ /* Ieee/string.scm 1297 */
 obj_t BgL_tmpz00_9952;
if(
INTEGERP(BgL_b1z00_1926))
{ /* Ieee/string.scm 1297 */
BgL_tmpz00_9952 = BgL_b1z00_1926
; }  else 
{ 
 obj_t BgL_auxz00_9955;
BgL_auxz00_9955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52741L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1926); 
FAILURE(BgL_auxz00_9955,BFALSE,BFALSE);} 
BgL_za71za7_3535 = 
(long)CINT(BgL_tmpz00_9952); } 
BgL_tmpz00_9931 = 
(BgL_za71za7_3535-
(BgL_i1z00_1933+1L)); }  else 
{ /* Ieee/string.scm 1298 */
 bool_t BgL_test4369z00_9962;
{ /* Ieee/string.scm 1298 */
 unsigned char BgL_auxz00_9976; unsigned char BgL_tmpz00_9963;
{ /* Ieee/string.scm 1298 */
 unsigned char BgL_tmpz00_9977;
{ /* Ieee/string.scm 331 */
 long BgL_l2545z00_4503;
BgL_l2545z00_4503 = 
STRING_LENGTH(BgL_s2z00_201); 
if(
BOUND_CHECK(BgL_i2z00_1934, BgL_l2545z00_4503))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_9977 = 
STRING_REF(BgL_s2z00_201, BgL_i2z00_1934)
; }  else 
{ 
 obj_t BgL_auxz00_9982;
BgL_auxz00_9982 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_201, 
(int)(BgL_l2545z00_4503), 
(int)(BgL_i2z00_1934)); 
FAILURE(BgL_auxz00_9982,BFALSE,BFALSE);} } 
BgL_auxz00_9976 = 
toupper(BgL_tmpz00_9977); } 
{ /* Ieee/string.scm 1298 */
 unsigned char BgL_tmpz00_9964;
{ /* Ieee/string.scm 331 */
 long BgL_l2541z00_4499;
BgL_l2541z00_4499 = 
STRING_LENGTH(BgL_s1z00_200); 
if(
BOUND_CHECK(BgL_i1z00_1933, BgL_l2541z00_4499))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_9964 = 
STRING_REF(BgL_s1z00_200, BgL_i1z00_1933)
; }  else 
{ 
 obj_t BgL_auxz00_9969;
BgL_auxz00_9969 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_200, 
(int)(BgL_l2541z00_4499), 
(int)(BgL_i1z00_1933)); 
FAILURE(BgL_auxz00_9969,BFALSE,BFALSE);} } 
BgL_tmpz00_9963 = 
toupper(BgL_tmpz00_9964); } 
BgL_test4369z00_9962 = 
(BgL_tmpz00_9963==BgL_auxz00_9976); } 
if(BgL_test4369z00_9962)
{ 
 long BgL_i2z00_9992; long BgL_i1z00_9990;
BgL_i1z00_9990 = 
(BgL_i1z00_1933-1L); 
BgL_i2z00_9992 = 
(BgL_i2z00_1934-1L); 
BgL_i2z00_1934 = BgL_i2z00_9992; 
BgL_i1z00_1933 = BgL_i1z00_9990; 
goto BgL_zc3z04anonymousza31806ze3z87_1935;}  else 
{ /* Ieee/string.scm 1301 */
 long BgL_za71za7_3552;
{ /* Ieee/string.scm 1301 */
 obj_t BgL_tmpz00_9994;
if(
INTEGERP(BgL_b1z00_1926))
{ /* Ieee/string.scm 1301 */
BgL_tmpz00_9994 = BgL_b1z00_1926
; }  else 
{ 
 obj_t BgL_auxz00_9997;
BgL_auxz00_9997 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(52871L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_1926); 
FAILURE(BgL_auxz00_9997,BFALSE,BFALSE);} 
BgL_za71za7_3552 = 
(long)CINT(BgL_tmpz00_9994); } 
BgL_tmpz00_9931 = 
(BgL_za71za7_3552-
(BgL_i1z00_1933+1L)); } } } 
return 
(int)(BgL_tmpz00_9931);} } } } } } } } } } 

}



/* _string-prefix? */
obj_t BGl__stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t BgL_env1178z00_215, obj_t BgL_opt1177z00_214)
{
{ /* Ieee/string.scm 1306 */
{ /* Ieee/string.scm 1306 */
 obj_t BgL_s1z00_1949; obj_t BgL_s2z00_1950;
BgL_s1z00_1949 = 
VECTOR_REF(BgL_opt1177z00_214,0L); 
BgL_s2z00_1950 = 
VECTOR_REF(BgL_opt1177z00_214,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1177z00_214)) { case 2L : 

{ /* Ieee/string.scm 1306 */

{ /* Ieee/string.scm 1306 */
 bool_t BgL_tmpz00_10025;
{ /* Ieee/string.scm 1306 */
 obj_t BgL_auxz00_10033; obj_t BgL_auxz00_10026;
if(
STRINGP(BgL_s2z00_1950))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10033 = BgL_s2z00_1950
; }  else 
{ 
 obj_t BgL_auxz00_10036;
BgL_auxz00_10036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1950); 
FAILURE(BgL_auxz00_10036,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1949))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10026 = BgL_s1z00_1949
; }  else 
{ 
 obj_t BgL_auxz00_10029;
BgL_auxz00_10029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1949); 
FAILURE(BgL_auxz00_10029,BFALSE,BFALSE);} 
BgL_tmpz00_10025 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10026, BgL_auxz00_10033, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10025);} } break;case 3L : 

{ /* Ieee/string.scm 1306 */
 obj_t BgL_start1z00_1957;
BgL_start1z00_1957 = 
VECTOR_REF(BgL_opt1177z00_214,2L); 
{ /* Ieee/string.scm 1306 */

{ /* Ieee/string.scm 1306 */
 bool_t BgL_tmpz00_10043;
{ /* Ieee/string.scm 1306 */
 obj_t BgL_auxz00_10051; obj_t BgL_auxz00_10044;
if(
STRINGP(BgL_s2z00_1950))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10051 = BgL_s2z00_1950
; }  else 
{ 
 obj_t BgL_auxz00_10054;
BgL_auxz00_10054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1950); 
FAILURE(BgL_auxz00_10054,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1949))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10044 = BgL_s1z00_1949
; }  else 
{ 
 obj_t BgL_auxz00_10047;
BgL_auxz00_10047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1949); 
FAILURE(BgL_auxz00_10047,BFALSE,BFALSE);} 
BgL_tmpz00_10043 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10044, BgL_auxz00_10051, BgL_start1z00_1957, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10043);} } } break;case 4L : 

{ /* Ieee/string.scm 1306 */
 obj_t BgL_start1z00_1961;
BgL_start1z00_1961 = 
VECTOR_REF(BgL_opt1177z00_214,2L); 
{ /* Ieee/string.scm 1306 */
 obj_t BgL_end1z00_1962;
BgL_end1z00_1962 = 
VECTOR_REF(BgL_opt1177z00_214,3L); 
{ /* Ieee/string.scm 1306 */

{ /* Ieee/string.scm 1306 */
 bool_t BgL_tmpz00_10062;
{ /* Ieee/string.scm 1306 */
 obj_t BgL_auxz00_10070; obj_t BgL_auxz00_10063;
if(
STRINGP(BgL_s2z00_1950))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10070 = BgL_s2z00_1950
; }  else 
{ 
 obj_t BgL_auxz00_10073;
BgL_auxz00_10073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1950); 
FAILURE(BgL_auxz00_10073,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1949))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10063 = BgL_s1z00_1949
; }  else 
{ 
 obj_t BgL_auxz00_10066;
BgL_auxz00_10066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1949); 
FAILURE(BgL_auxz00_10066,BFALSE,BFALSE);} 
BgL_tmpz00_10062 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10063, BgL_auxz00_10070, BgL_start1z00_1961, BgL_end1z00_1962, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10062);} } } } break;case 5L : 

{ /* Ieee/string.scm 1306 */
 obj_t BgL_start1z00_1965;
BgL_start1z00_1965 = 
VECTOR_REF(BgL_opt1177z00_214,2L); 
{ /* Ieee/string.scm 1306 */
 obj_t BgL_end1z00_1966;
BgL_end1z00_1966 = 
VECTOR_REF(BgL_opt1177z00_214,3L); 
{ /* Ieee/string.scm 1306 */
 obj_t BgL_start2z00_1967;
BgL_start2z00_1967 = 
VECTOR_REF(BgL_opt1177z00_214,4L); 
{ /* Ieee/string.scm 1306 */

{ /* Ieee/string.scm 1306 */
 bool_t BgL_tmpz00_10082;
{ /* Ieee/string.scm 1306 */
 obj_t BgL_auxz00_10090; obj_t BgL_auxz00_10083;
if(
STRINGP(BgL_s2z00_1950))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10090 = BgL_s2z00_1950
; }  else 
{ 
 obj_t BgL_auxz00_10093;
BgL_auxz00_10093 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1950); 
FAILURE(BgL_auxz00_10093,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1949))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10083 = BgL_s1z00_1949
; }  else 
{ 
 obj_t BgL_auxz00_10086;
BgL_auxz00_10086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1949); 
FAILURE(BgL_auxz00_10086,BFALSE,BFALSE);} 
BgL_tmpz00_10082 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10083, BgL_auxz00_10090, BgL_start1z00_1965, BgL_end1z00_1966, BgL_start2z00_1967, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10082);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1306 */
 obj_t BgL_start1z00_1969;
BgL_start1z00_1969 = 
VECTOR_REF(BgL_opt1177z00_214,2L); 
{ /* Ieee/string.scm 1306 */
 obj_t BgL_end1z00_1970;
BgL_end1z00_1970 = 
VECTOR_REF(BgL_opt1177z00_214,3L); 
{ /* Ieee/string.scm 1306 */
 obj_t BgL_start2z00_1971;
BgL_start2z00_1971 = 
VECTOR_REF(BgL_opt1177z00_214,4L); 
{ /* Ieee/string.scm 1306 */
 obj_t BgL_end2z00_1972;
BgL_end2z00_1972 = 
VECTOR_REF(BgL_opt1177z00_214,5L); 
{ /* Ieee/string.scm 1306 */

{ /* Ieee/string.scm 1306 */
 bool_t BgL_tmpz00_10103;
{ /* Ieee/string.scm 1306 */
 obj_t BgL_auxz00_10111; obj_t BgL_auxz00_10104;
if(
STRINGP(BgL_s2z00_1950))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10111 = BgL_s2z00_1950
; }  else 
{ 
 obj_t BgL_auxz00_10114;
BgL_auxz00_10114 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1950); 
FAILURE(BgL_auxz00_10114,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1949))
{ /* Ieee/string.scm 1306 */
BgL_auxz00_10104 = BgL_s1z00_1949
; }  else 
{ 
 obj_t BgL_auxz00_10107;
BgL_auxz00_10107 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53114L), BGl_string3513z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1949); 
FAILURE(BgL_auxz00_10107,BFALSE,BFALSE);} 
BgL_tmpz00_10103 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10104, BgL_auxz00_10111, BgL_start1z00_1969, BgL_end1z00_1970, BgL_start2z00_1971, BgL_end2z00_1972); } 
return 
BBOOL(BgL_tmpz00_10103);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3511z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1177z00_214)));} } } } 

}



/* string-prefix? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t BgL_s1z00_208, obj_t BgL_s2z00_209, obj_t BgL_start1z00_210, obj_t BgL_end1z00_211, obj_t BgL_start2z00_212, obj_t BgL_end2z00_213)
{
{ /* Ieee/string.scm 1306 */
{ /* Ieee/string.scm 1308 */
 long BgL_l1z00_1974;
BgL_l1z00_1974 = 
STRING_LENGTH(BgL_s1z00_208); 
{ /* Ieee/string.scm 1308 */
 long BgL_l2z00_1975;
BgL_l2z00_1975 = 
STRING_LENGTH(BgL_s2z00_209); 
{ /* Ieee/string.scm 1309 */
 obj_t BgL_e1z00_1976;
{ /* Ieee/string.scm 1310 */
 obj_t BgL_procz00_3556;
BgL_procz00_3556 = BGl_symbol3514z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_211))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4386z00_10129;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3561;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_10130;
if(
INTEGERP(BgL_end1z00_211))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_10130 = BgL_end1z00_211
; }  else 
{ 
 obj_t BgL_auxz00_10133;
BgL_auxz00_10133 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_211); 
FAILURE(BgL_auxz00_10133,BFALSE,BFALSE);} 
BgL_n1z00_3561 = 
(long)CINT(BgL_tmpz00_10130); } 
BgL_test4386z00_10129 = 
(BgL_n1z00_3561<=0L); } 
if(BgL_test4386z00_10129)
{ /* Ieee/string.scm 1212 */
BgL_e1z00_1976 = 
BGl_errorz00zz__errorz00(BgL_procz00_3556, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_211); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4388z00_10141;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3562;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_10142;
if(
INTEGERP(BgL_end1z00_211))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_10142 = BgL_end1z00_211
; }  else 
{ 
 obj_t BgL_auxz00_10145;
BgL_auxz00_10145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_211); 
FAILURE(BgL_auxz00_10145,BFALSE,BFALSE);} 
BgL_n1z00_3562 = 
(long)CINT(BgL_tmpz00_10142); } 
BgL_test4388z00_10141 = 
(BgL_n1z00_3562>BgL_l1z00_1974); } 
if(BgL_test4388z00_10141)
{ /* Ieee/string.scm 1214 */
BgL_e1z00_1976 = 
BGl_errorz00zz__errorz00(BgL_procz00_3556, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_211); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e1z00_1976 = BgL_end1z00_211; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e1z00_1976 = 
BINT(BgL_l1z00_1974); } } 
{ /* Ieee/string.scm 1310 */
 obj_t BgL_e2z00_1977;
{ /* Ieee/string.scm 1311 */
 obj_t BgL_procz00_3564;
BgL_procz00_3564 = BGl_symbol3514z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_213))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4391z00_10156;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3569;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_10157;
if(
INTEGERP(BgL_end2z00_213))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_10157 = BgL_end2z00_213
; }  else 
{ 
 obj_t BgL_auxz00_10160;
BgL_auxz00_10160 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_213); 
FAILURE(BgL_auxz00_10160,BFALSE,BFALSE);} 
BgL_n1z00_3569 = 
(long)CINT(BgL_tmpz00_10157); } 
BgL_test4391z00_10156 = 
(BgL_n1z00_3569<=0L); } 
if(BgL_test4391z00_10156)
{ /* Ieee/string.scm 1212 */
BgL_e2z00_1977 = 
BGl_errorz00zz__errorz00(BgL_procz00_3564, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_213); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4393z00_10168;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3570;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_10169;
if(
INTEGERP(BgL_end2z00_213))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_10169 = BgL_end2z00_213
; }  else 
{ 
 obj_t BgL_auxz00_10172;
BgL_auxz00_10172 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_213); 
FAILURE(BgL_auxz00_10172,BFALSE,BFALSE);} 
BgL_n1z00_3570 = 
(long)CINT(BgL_tmpz00_10169); } 
BgL_test4393z00_10168 = 
(BgL_n1z00_3570>BgL_l2z00_1975); } 
if(BgL_test4393z00_10168)
{ /* Ieee/string.scm 1214 */
BgL_e2z00_1977 = 
BGl_errorz00zz__errorz00(BgL_procz00_3564, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_213); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e2z00_1977 = BgL_end2z00_213; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e2z00_1977 = 
BINT(BgL_l2z00_1975); } } 
{ /* Ieee/string.scm 1311 */
 obj_t BgL_b1z00_1978;
{ /* Ieee/string.scm 1312 */
 obj_t BgL_procz00_3572;
BgL_procz00_3572 = BGl_symbol3514z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_210))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4396z00_10183;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3577;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_10184;
if(
INTEGERP(BgL_start1z00_210))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_10184 = BgL_start1z00_210
; }  else 
{ 
 obj_t BgL_auxz00_10187;
BgL_auxz00_10187 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_210); 
FAILURE(BgL_auxz00_10187,BFALSE,BFALSE);} 
BgL_n1z00_3577 = 
(long)CINT(BgL_tmpz00_10184); } 
BgL_test4396z00_10183 = 
(BgL_n1z00_3577<0L); } 
if(BgL_test4396z00_10183)
{ /* Ieee/string.scm 1198 */
BgL_b1z00_1978 = 
BGl_errorz00zz__errorz00(BgL_procz00_3572, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_210); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4398z00_10195;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3578;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_10196;
if(
INTEGERP(BgL_start1z00_210))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_10196 = BgL_start1z00_210
; }  else 
{ 
 obj_t BgL_auxz00_10199;
BgL_auxz00_10199 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_210); 
FAILURE(BgL_auxz00_10199,BFALSE,BFALSE);} 
BgL_n1z00_3578 = 
(long)CINT(BgL_tmpz00_10196); } 
BgL_test4398z00_10195 = 
(BgL_n1z00_3578>=BgL_l1z00_1974); } 
if(BgL_test4398z00_10195)
{ /* Ieee/string.scm 1200 */
BgL_b1z00_1978 = 
BGl_errorz00zz__errorz00(BgL_procz00_3572, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_210); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b1z00_1978 = BgL_start1z00_210; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b1z00_1978 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1312 */
 obj_t BgL_b2z00_1979;
{ /* Ieee/string.scm 1313 */
 obj_t BgL_procz00_3580;
BgL_procz00_3580 = BGl_symbol3514z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_212))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4401z00_10210;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3585;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_10211;
if(
INTEGERP(BgL_start2z00_212))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_10211 = BgL_start2z00_212
; }  else 
{ 
 obj_t BgL_auxz00_10214;
BgL_auxz00_10214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_212); 
FAILURE(BgL_auxz00_10214,BFALSE,BFALSE);} 
BgL_n1z00_3585 = 
(long)CINT(BgL_tmpz00_10211); } 
BgL_test4401z00_10210 = 
(BgL_n1z00_3585<0L); } 
if(BgL_test4401z00_10210)
{ /* Ieee/string.scm 1198 */
BgL_b2z00_1979 = 
BGl_errorz00zz__errorz00(BgL_procz00_3580, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_212); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4403z00_10222;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3586;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_10223;
if(
INTEGERP(BgL_start2z00_212))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_10223 = BgL_start2z00_212
; }  else 
{ 
 obj_t BgL_auxz00_10226;
BgL_auxz00_10226 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3515z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_212); 
FAILURE(BgL_auxz00_10226,BFALSE,BFALSE);} 
BgL_n1z00_3586 = 
(long)CINT(BgL_tmpz00_10223); } 
BgL_test4403z00_10222 = 
(BgL_n1z00_3586>=BgL_l2z00_1975); } 
if(BgL_test4403z00_10222)
{ /* Ieee/string.scm 1200 */
BgL_b2z00_1979 = 
BGl_errorz00zz__errorz00(BgL_procz00_3580, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_212); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b2z00_1979 = BgL_start2z00_212; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b2z00_1979 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1313 */

{ 
 obj_t BgL_i1z00_1981; obj_t BgL_i2z00_1982;
BgL_i1z00_1981 = BgL_b1z00_1978; 
BgL_i2z00_1982 = BgL_b2z00_1979; 
BgL_zc3z04anonymousza31819ze3z87_1983:
{ /* Ieee/string.scm 1317 */
 bool_t BgL_test4405z00_10235;
{ /* Ieee/string.scm 1317 */
 long BgL_n1z00_3588; long BgL_n2z00_3589;
{ /* Ieee/string.scm 1317 */
 obj_t BgL_tmpz00_10236;
if(
INTEGERP(BgL_i1z00_1981))
{ /* Ieee/string.scm 1317 */
BgL_tmpz00_10236 = BgL_i1z00_1981
; }  else 
{ 
 obj_t BgL_auxz00_10239;
BgL_auxz00_10239 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53577L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1981); 
FAILURE(BgL_auxz00_10239,BFALSE,BFALSE);} 
BgL_n1z00_3588 = 
(long)CINT(BgL_tmpz00_10236); } 
{ /* Ieee/string.scm 1317 */
 obj_t BgL_tmpz00_10244;
if(
INTEGERP(BgL_e1z00_1976))
{ /* Ieee/string.scm 1317 */
BgL_tmpz00_10244 = BgL_e1z00_1976
; }  else 
{ 
 obj_t BgL_auxz00_10247;
BgL_auxz00_10247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53580L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_1976); 
FAILURE(BgL_auxz00_10247,BFALSE,BFALSE);} 
BgL_n2z00_3589 = 
(long)CINT(BgL_tmpz00_10244); } 
BgL_test4405z00_10235 = 
(BgL_n1z00_3588==BgL_n2z00_3589); } 
if(BgL_test4405z00_10235)
{ /* Ieee/string.scm 1317 */
return ((bool_t)1);}  else 
{ /* Ieee/string.scm 1319 */
 bool_t BgL_test4408z00_10253;
{ /* Ieee/string.scm 1319 */
 long BgL_n1z00_3590; long BgL_n2z00_3591;
{ /* Ieee/string.scm 1319 */
 obj_t BgL_tmpz00_10254;
if(
INTEGERP(BgL_i2z00_1982))
{ /* Ieee/string.scm 1319 */
BgL_tmpz00_10254 = BgL_i2z00_1982
; }  else 
{ 
 obj_t BgL_auxz00_10257;
BgL_auxz00_10257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53605L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1982); 
FAILURE(BgL_auxz00_10257,BFALSE,BFALSE);} 
BgL_n1z00_3590 = 
(long)CINT(BgL_tmpz00_10254); } 
{ /* Ieee/string.scm 1319 */
 obj_t BgL_tmpz00_10262;
if(
INTEGERP(BgL_e2z00_1977))
{ /* Ieee/string.scm 1319 */
BgL_tmpz00_10262 = BgL_e2z00_1977
; }  else 
{ 
 obj_t BgL_auxz00_10265;
BgL_auxz00_10265 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53608L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_1977); 
FAILURE(BgL_auxz00_10265,BFALSE,BFALSE);} 
BgL_n2z00_3591 = 
(long)CINT(BgL_tmpz00_10262); } 
BgL_test4408z00_10253 = 
(BgL_n1z00_3590==BgL_n2z00_3591); } 
if(BgL_test4408z00_10253)
{ /* Ieee/string.scm 1319 */
return ((bool_t)0);}  else 
{ /* Ieee/string.scm 1321 */
 bool_t BgL_test4411z00_10271;
{ /* Ieee/string.scm 1321 */
 unsigned char BgL_auxz00_10291; unsigned char BgL_tmpz00_10272;
{ /* Ieee/string.scm 1321 */
 long BgL_kz00_3595;
{ /* Ieee/string.scm 1321 */
 obj_t BgL_tmpz00_10292;
if(
INTEGERP(BgL_i2z00_1982))
{ /* Ieee/string.scm 1321 */
BgL_tmpz00_10292 = BgL_i2z00_1982
; }  else 
{ 
 obj_t BgL_auxz00_10295;
BgL_auxz00_10295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53670L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1982); 
FAILURE(BgL_auxz00_10295,BFALSE,BFALSE);} 
BgL_kz00_3595 = 
(long)CINT(BgL_tmpz00_10292); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2553z00_4511;
BgL_l2553z00_4511 = 
STRING_LENGTH(BgL_s2z00_209); 
if(
BOUND_CHECK(BgL_kz00_3595, BgL_l2553z00_4511))
{ /* Ieee/string.scm 331 */
BgL_auxz00_10291 = 
STRING_REF(BgL_s2z00_209, BgL_kz00_3595)
; }  else 
{ 
 obj_t BgL_auxz00_10304;
BgL_auxz00_10304 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_209, 
(int)(BgL_l2553z00_4511), 
(int)(BgL_kz00_3595)); 
FAILURE(BgL_auxz00_10304,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1321 */
 long BgL_kz00_3593;
{ /* Ieee/string.scm 1321 */
 obj_t BgL_tmpz00_10273;
if(
INTEGERP(BgL_i1z00_1981))
{ /* Ieee/string.scm 1321 */
BgL_tmpz00_10273 = BgL_i1z00_1981
; }  else 
{ 
 obj_t BgL_auxz00_10276;
BgL_auxz00_10276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53651L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1981); 
FAILURE(BgL_auxz00_10276,BFALSE,BFALSE);} 
BgL_kz00_3593 = 
(long)CINT(BgL_tmpz00_10273); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2549z00_4507;
BgL_l2549z00_4507 = 
STRING_LENGTH(BgL_s1z00_208); 
if(
BOUND_CHECK(BgL_kz00_3593, BgL_l2549z00_4507))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_10272 = 
STRING_REF(BgL_s1z00_208, BgL_kz00_3593)
; }  else 
{ 
 obj_t BgL_auxz00_10285;
BgL_auxz00_10285 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_208, 
(int)(BgL_l2549z00_4507), 
(int)(BgL_kz00_3593)); 
FAILURE(BgL_auxz00_10285,BFALSE,BFALSE);} } } 
BgL_test4411z00_10271 = 
(BgL_tmpz00_10272==BgL_auxz00_10291); } 
if(BgL_test4411z00_10271)
{ 
 obj_t BgL_i2z00_10321; obj_t BgL_i1z00_10311;
{ 
 obj_t BgL_tmpz00_10312;
if(
INTEGERP(BgL_i1z00_1981))
{ /* Ieee/string.scm 1322 */
BgL_tmpz00_10312 = BgL_i1z00_1981
; }  else 
{ 
 obj_t BgL_auxz00_10315;
BgL_auxz00_10315 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53692L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_1981); 
FAILURE(BgL_auxz00_10315,BFALSE,BFALSE);} 
BgL_i1z00_10311 = 
ADDFX(BgL_tmpz00_10312, 
BINT(1L)); } 
{ 
 obj_t BgL_tmpz00_10322;
if(
INTEGERP(BgL_i2z00_1982))
{ /* Ieee/string.scm 1322 */
BgL_tmpz00_10322 = BgL_i2z00_1982
; }  else 
{ 
 obj_t BgL_auxz00_10325;
BgL_auxz00_10325 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53703L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_1982); 
FAILURE(BgL_auxz00_10325,BFALSE,BFALSE);} 
BgL_i2z00_10321 = 
ADDFX(BgL_tmpz00_10322, 
BINT(1L)); } 
BgL_i2z00_1982 = BgL_i2z00_10321; 
BgL_i1z00_1981 = BgL_i1z00_10311; 
goto BgL_zc3z04anonymousza31819ze3z87_1983;}  else 
{ /* Ieee/string.scm 1321 */
return ((bool_t)0);} } } } } } } } } } } } } 

}



/* _string-prefix-ci? */
obj_t BGl__stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t BgL_env1182z00_223, obj_t BgL_opt1181z00_222)
{
{ /* Ieee/string.scm 1329 */
{ /* Ieee/string.scm 1329 */
 obj_t BgL_s1z00_1994; obj_t BgL_s2z00_1995;
BgL_s1z00_1994 = 
VECTOR_REF(BgL_opt1181z00_222,0L); 
BgL_s2z00_1995 = 
VECTOR_REF(BgL_opt1181z00_222,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1181z00_222)) { case 2L : 

{ /* Ieee/string.scm 1329 */

{ /* Ieee/string.scm 1329 */
 bool_t BgL_tmpz00_10333;
{ /* Ieee/string.scm 1329 */
 obj_t BgL_auxz00_10341; obj_t BgL_auxz00_10334;
if(
STRINGP(BgL_s2z00_1995))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10341 = BgL_s2z00_1995
; }  else 
{ 
 obj_t BgL_auxz00_10344;
BgL_auxz00_10344 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1995); 
FAILURE(BgL_auxz00_10344,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1994))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10334 = BgL_s1z00_1994
; }  else 
{ 
 obj_t BgL_auxz00_10337;
BgL_auxz00_10337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1994); 
FAILURE(BgL_auxz00_10337,BFALSE,BFALSE);} 
BgL_tmpz00_10333 = 
BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10334, BgL_auxz00_10341, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10333);} } break;case 3L : 

{ /* Ieee/string.scm 1329 */
 obj_t BgL_start1z00_2002;
BgL_start1z00_2002 = 
VECTOR_REF(BgL_opt1181z00_222,2L); 
{ /* Ieee/string.scm 1329 */

{ /* Ieee/string.scm 1329 */
 bool_t BgL_tmpz00_10351;
{ /* Ieee/string.scm 1329 */
 obj_t BgL_auxz00_10359; obj_t BgL_auxz00_10352;
if(
STRINGP(BgL_s2z00_1995))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10359 = BgL_s2z00_1995
; }  else 
{ 
 obj_t BgL_auxz00_10362;
BgL_auxz00_10362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1995); 
FAILURE(BgL_auxz00_10362,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1994))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10352 = BgL_s1z00_1994
; }  else 
{ 
 obj_t BgL_auxz00_10355;
BgL_auxz00_10355 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1994); 
FAILURE(BgL_auxz00_10355,BFALSE,BFALSE);} 
BgL_tmpz00_10351 = 
BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10352, BgL_auxz00_10359, BgL_start1z00_2002, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10351);} } } break;case 4L : 

{ /* Ieee/string.scm 1329 */
 obj_t BgL_start1z00_2006;
BgL_start1z00_2006 = 
VECTOR_REF(BgL_opt1181z00_222,2L); 
{ /* Ieee/string.scm 1329 */
 obj_t BgL_end1z00_2007;
BgL_end1z00_2007 = 
VECTOR_REF(BgL_opt1181z00_222,3L); 
{ /* Ieee/string.scm 1329 */

{ /* Ieee/string.scm 1329 */
 bool_t BgL_tmpz00_10370;
{ /* Ieee/string.scm 1329 */
 obj_t BgL_auxz00_10378; obj_t BgL_auxz00_10371;
if(
STRINGP(BgL_s2z00_1995))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10378 = BgL_s2z00_1995
; }  else 
{ 
 obj_t BgL_auxz00_10381;
BgL_auxz00_10381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1995); 
FAILURE(BgL_auxz00_10381,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1994))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10371 = BgL_s1z00_1994
; }  else 
{ 
 obj_t BgL_auxz00_10374;
BgL_auxz00_10374 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1994); 
FAILURE(BgL_auxz00_10374,BFALSE,BFALSE);} 
BgL_tmpz00_10370 = 
BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10371, BgL_auxz00_10378, BgL_start1z00_2006, BgL_end1z00_2007, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10370);} } } } break;case 5L : 

{ /* Ieee/string.scm 1329 */
 obj_t BgL_start1z00_2010;
BgL_start1z00_2010 = 
VECTOR_REF(BgL_opt1181z00_222,2L); 
{ /* Ieee/string.scm 1329 */
 obj_t BgL_end1z00_2011;
BgL_end1z00_2011 = 
VECTOR_REF(BgL_opt1181z00_222,3L); 
{ /* Ieee/string.scm 1329 */
 obj_t BgL_start2z00_2012;
BgL_start2z00_2012 = 
VECTOR_REF(BgL_opt1181z00_222,4L); 
{ /* Ieee/string.scm 1329 */

{ /* Ieee/string.scm 1329 */
 bool_t BgL_tmpz00_10390;
{ /* Ieee/string.scm 1329 */
 obj_t BgL_auxz00_10398; obj_t BgL_auxz00_10391;
if(
STRINGP(BgL_s2z00_1995))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10398 = BgL_s2z00_1995
; }  else 
{ 
 obj_t BgL_auxz00_10401;
BgL_auxz00_10401 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1995); 
FAILURE(BgL_auxz00_10401,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1994))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10391 = BgL_s1z00_1994
; }  else 
{ 
 obj_t BgL_auxz00_10394;
BgL_auxz00_10394 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1994); 
FAILURE(BgL_auxz00_10394,BFALSE,BFALSE);} 
BgL_tmpz00_10390 = 
BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10391, BgL_auxz00_10398, BgL_start1z00_2010, BgL_end1z00_2011, BgL_start2z00_2012, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10390);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1329 */
 obj_t BgL_start1z00_2014;
BgL_start1z00_2014 = 
VECTOR_REF(BgL_opt1181z00_222,2L); 
{ /* Ieee/string.scm 1329 */
 obj_t BgL_end1z00_2015;
BgL_end1z00_2015 = 
VECTOR_REF(BgL_opt1181z00_222,3L); 
{ /* Ieee/string.scm 1329 */
 obj_t BgL_start2z00_2016;
BgL_start2z00_2016 = 
VECTOR_REF(BgL_opt1181z00_222,4L); 
{ /* Ieee/string.scm 1329 */
 obj_t BgL_end2z00_2017;
BgL_end2z00_2017 = 
VECTOR_REF(BgL_opt1181z00_222,5L); 
{ /* Ieee/string.scm 1329 */

{ /* Ieee/string.scm 1329 */
 bool_t BgL_tmpz00_10411;
{ /* Ieee/string.scm 1329 */
 obj_t BgL_auxz00_10419; obj_t BgL_auxz00_10412;
if(
STRINGP(BgL_s2z00_1995))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10419 = BgL_s2z00_1995
; }  else 
{ 
 obj_t BgL_auxz00_10422;
BgL_auxz00_10422 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_1995); 
FAILURE(BgL_auxz00_10422,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_1994))
{ /* Ieee/string.scm 1329 */
BgL_auxz00_10412 = BgL_s1z00_1994
; }  else 
{ 
 obj_t BgL_auxz00_10415;
BgL_auxz00_10415 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(53959L), BGl_string3518z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_1994); 
FAILURE(BgL_auxz00_10415,BFALSE,BFALSE);} 
BgL_tmpz00_10411 = 
BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10412, BgL_auxz00_10419, BgL_start1z00_2014, BgL_end1z00_2015, BgL_start2z00_2016, BgL_end2z00_2017); } 
return 
BBOOL(BgL_tmpz00_10411);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3516z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1181z00_222)));} } } } 

}



/* string-prefix-ci? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2prefixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t BgL_s1z00_216, obj_t BgL_s2z00_217, obj_t BgL_start1z00_218, obj_t BgL_end1z00_219, obj_t BgL_start2z00_220, obj_t BgL_end2z00_221)
{
{ /* Ieee/string.scm 1329 */
{ /* Ieee/string.scm 1331 */
 long BgL_l1z00_2019;
BgL_l1z00_2019 = 
STRING_LENGTH(BgL_s1z00_216); 
{ /* Ieee/string.scm 1331 */
 long BgL_l2z00_2020;
BgL_l2z00_2020 = 
STRING_LENGTH(BgL_s2z00_217); 
{ /* Ieee/string.scm 1332 */
 obj_t BgL_e1z00_2021;
{ /* Ieee/string.scm 1333 */
 obj_t BgL_procz00_3602;
BgL_procz00_3602 = BGl_symbol3519z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_219))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4429z00_10437;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3607;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_10438;
if(
INTEGERP(BgL_end1z00_219))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_10438 = BgL_end1z00_219
; }  else 
{ 
 obj_t BgL_auxz00_10441;
BgL_auxz00_10441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_219); 
FAILURE(BgL_auxz00_10441,BFALSE,BFALSE);} 
BgL_n1z00_3607 = 
(long)CINT(BgL_tmpz00_10438); } 
BgL_test4429z00_10437 = 
(BgL_n1z00_3607<=0L); } 
if(BgL_test4429z00_10437)
{ /* Ieee/string.scm 1212 */
BgL_e1z00_2021 = 
BGl_errorz00zz__errorz00(BgL_procz00_3602, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_219); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4431z00_10449;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3608;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_10450;
if(
INTEGERP(BgL_end1z00_219))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_10450 = BgL_end1z00_219
; }  else 
{ 
 obj_t BgL_auxz00_10453;
BgL_auxz00_10453 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_219); 
FAILURE(BgL_auxz00_10453,BFALSE,BFALSE);} 
BgL_n1z00_3608 = 
(long)CINT(BgL_tmpz00_10450); } 
BgL_test4431z00_10449 = 
(BgL_n1z00_3608>BgL_l1z00_2019); } 
if(BgL_test4431z00_10449)
{ /* Ieee/string.scm 1214 */
BgL_e1z00_2021 = 
BGl_errorz00zz__errorz00(BgL_procz00_3602, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_219); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e1z00_2021 = BgL_end1z00_219; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e1z00_2021 = 
BINT(BgL_l1z00_2019); } } 
{ /* Ieee/string.scm 1333 */
 obj_t BgL_e2z00_2022;
{ /* Ieee/string.scm 1334 */
 obj_t BgL_procz00_3610;
BgL_procz00_3610 = BGl_symbol3519z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_221))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4434z00_10464;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3615;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_10465;
if(
INTEGERP(BgL_end2z00_221))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_10465 = BgL_end2z00_221
; }  else 
{ 
 obj_t BgL_auxz00_10468;
BgL_auxz00_10468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_221); 
FAILURE(BgL_auxz00_10468,BFALSE,BFALSE);} 
BgL_n1z00_3615 = 
(long)CINT(BgL_tmpz00_10465); } 
BgL_test4434z00_10464 = 
(BgL_n1z00_3615<=0L); } 
if(BgL_test4434z00_10464)
{ /* Ieee/string.scm 1212 */
BgL_e2z00_2022 = 
BGl_errorz00zz__errorz00(BgL_procz00_3610, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_221); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4436z00_10476;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3616;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_10477;
if(
INTEGERP(BgL_end2z00_221))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_10477 = BgL_end2z00_221
; }  else 
{ 
 obj_t BgL_auxz00_10480;
BgL_auxz00_10480 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_221); 
FAILURE(BgL_auxz00_10480,BFALSE,BFALSE);} 
BgL_n1z00_3616 = 
(long)CINT(BgL_tmpz00_10477); } 
BgL_test4436z00_10476 = 
(BgL_n1z00_3616>BgL_l2z00_2020); } 
if(BgL_test4436z00_10476)
{ /* Ieee/string.scm 1214 */
BgL_e2z00_2022 = 
BGl_errorz00zz__errorz00(BgL_procz00_3610, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_221); }  else 
{ /* Ieee/string.scm 1214 */
BgL_e2z00_2022 = BgL_end2z00_221; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_e2z00_2022 = 
BINT(BgL_l2z00_2020); } } 
{ /* Ieee/string.scm 1334 */
 obj_t BgL_b1z00_2023;
{ /* Ieee/string.scm 1335 */
 obj_t BgL_procz00_3618;
BgL_procz00_3618 = BGl_symbol3519z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_218))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4439z00_10491;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3623;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_10492;
if(
INTEGERP(BgL_start1z00_218))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_10492 = BgL_start1z00_218
; }  else 
{ 
 obj_t BgL_auxz00_10495;
BgL_auxz00_10495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_218); 
FAILURE(BgL_auxz00_10495,BFALSE,BFALSE);} 
BgL_n1z00_3623 = 
(long)CINT(BgL_tmpz00_10492); } 
BgL_test4439z00_10491 = 
(BgL_n1z00_3623<0L); } 
if(BgL_test4439z00_10491)
{ /* Ieee/string.scm 1198 */
BgL_b1z00_2023 = 
BGl_errorz00zz__errorz00(BgL_procz00_3618, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_218); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4441z00_10503;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3624;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_10504;
if(
INTEGERP(BgL_start1z00_218))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_10504 = BgL_start1z00_218
; }  else 
{ 
 obj_t BgL_auxz00_10507;
BgL_auxz00_10507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_218); 
FAILURE(BgL_auxz00_10507,BFALSE,BFALSE);} 
BgL_n1z00_3624 = 
(long)CINT(BgL_tmpz00_10504); } 
BgL_test4441z00_10503 = 
(BgL_n1z00_3624>=BgL_l1z00_2019); } 
if(BgL_test4441z00_10503)
{ /* Ieee/string.scm 1200 */
BgL_b1z00_2023 = 
BGl_errorz00zz__errorz00(BgL_procz00_3618, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_218); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b1z00_2023 = BgL_start1z00_218; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b1z00_2023 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1335 */
 obj_t BgL_b2z00_2024;
{ /* Ieee/string.scm 1336 */
 obj_t BgL_procz00_3626;
BgL_procz00_3626 = BGl_symbol3519z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_220))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4444z00_10518;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3631;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_10519;
if(
INTEGERP(BgL_start2z00_220))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_10519 = BgL_start2z00_220
; }  else 
{ 
 obj_t BgL_auxz00_10522;
BgL_auxz00_10522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_220); 
FAILURE(BgL_auxz00_10522,BFALSE,BFALSE);} 
BgL_n1z00_3631 = 
(long)CINT(BgL_tmpz00_10519); } 
BgL_test4444z00_10518 = 
(BgL_n1z00_3631<0L); } 
if(BgL_test4444z00_10518)
{ /* Ieee/string.scm 1198 */
BgL_b2z00_2024 = 
BGl_errorz00zz__errorz00(BgL_procz00_3626, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_220); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4446z00_10530;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3632;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_10531;
if(
INTEGERP(BgL_start2z00_220))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_10531 = BgL_start2z00_220
; }  else 
{ 
 obj_t BgL_auxz00_10534;
BgL_auxz00_10534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3520z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_220); 
FAILURE(BgL_auxz00_10534,BFALSE,BFALSE);} 
BgL_n1z00_3632 = 
(long)CINT(BgL_tmpz00_10531); } 
BgL_test4446z00_10530 = 
(BgL_n1z00_3632>=BgL_l2z00_2020); } 
if(BgL_test4446z00_10530)
{ /* Ieee/string.scm 1200 */
BgL_b2z00_2024 = 
BGl_errorz00zz__errorz00(BgL_procz00_3626, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_220); }  else 
{ /* Ieee/string.scm 1200 */
BgL_b2z00_2024 = BgL_start2z00_220; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_b2z00_2024 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1336 */

{ 
 obj_t BgL_i1z00_2026; obj_t BgL_i2z00_2027;
BgL_i1z00_2026 = BgL_b1z00_2023; 
BgL_i2z00_2027 = BgL_b2z00_2024; 
BgL_zc3z04anonymousza31832ze3z87_2028:
{ /* Ieee/string.scm 1340 */
 bool_t BgL_test4448z00_10543;
{ /* Ieee/string.scm 1340 */
 long BgL_n1z00_3634; long BgL_n2z00_3635;
{ /* Ieee/string.scm 1340 */
 obj_t BgL_tmpz00_10544;
if(
INTEGERP(BgL_i1z00_2026))
{ /* Ieee/string.scm 1340 */
BgL_tmpz00_10544 = BgL_i1z00_2026
; }  else 
{ 
 obj_t BgL_auxz00_10547;
BgL_auxz00_10547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54433L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_2026); 
FAILURE(BgL_auxz00_10547,BFALSE,BFALSE);} 
BgL_n1z00_3634 = 
(long)CINT(BgL_tmpz00_10544); } 
{ /* Ieee/string.scm 1340 */
 obj_t BgL_tmpz00_10552;
if(
INTEGERP(BgL_e1z00_2021))
{ /* Ieee/string.scm 1340 */
BgL_tmpz00_10552 = BgL_e1z00_2021
; }  else 
{ 
 obj_t BgL_auxz00_10555;
BgL_auxz00_10555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54436L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_2021); 
FAILURE(BgL_auxz00_10555,BFALSE,BFALSE);} 
BgL_n2z00_3635 = 
(long)CINT(BgL_tmpz00_10552); } 
BgL_test4448z00_10543 = 
(BgL_n1z00_3634==BgL_n2z00_3635); } 
if(BgL_test4448z00_10543)
{ /* Ieee/string.scm 1340 */
return ((bool_t)1);}  else 
{ /* Ieee/string.scm 1342 */
 bool_t BgL_test4451z00_10561;
{ /* Ieee/string.scm 1342 */
 long BgL_n1z00_3636; long BgL_n2z00_3637;
{ /* Ieee/string.scm 1342 */
 obj_t BgL_tmpz00_10562;
if(
INTEGERP(BgL_i2z00_2027))
{ /* Ieee/string.scm 1342 */
BgL_tmpz00_10562 = BgL_i2z00_2027
; }  else 
{ 
 obj_t BgL_auxz00_10565;
BgL_auxz00_10565 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54461L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_2027); 
FAILURE(BgL_auxz00_10565,BFALSE,BFALSE);} 
BgL_n1z00_3636 = 
(long)CINT(BgL_tmpz00_10562); } 
{ /* Ieee/string.scm 1342 */
 obj_t BgL_tmpz00_10570;
if(
INTEGERP(BgL_e2z00_2022))
{ /* Ieee/string.scm 1342 */
BgL_tmpz00_10570 = BgL_e2z00_2022
; }  else 
{ 
 obj_t BgL_auxz00_10573;
BgL_auxz00_10573 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54464L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_2022); 
FAILURE(BgL_auxz00_10573,BFALSE,BFALSE);} 
BgL_n2z00_3637 = 
(long)CINT(BgL_tmpz00_10570); } 
BgL_test4451z00_10561 = 
(BgL_n1z00_3636==BgL_n2z00_3637); } 
if(BgL_test4451z00_10561)
{ /* Ieee/string.scm 1342 */
return ((bool_t)0);}  else 
{ /* Ieee/string.scm 1344 */
 bool_t BgL_test4454z00_10579;
{ /* Ieee/string.scm 1344 */
 unsigned char BgL_auxz00_10601; unsigned char BgL_tmpz00_10580;
{ /* Ieee/string.scm 1344 */
 unsigned char BgL_tmpz00_10602;
{ /* Ieee/string.scm 1344 */
 long BgL_kz00_3641;
{ /* Ieee/string.scm 1344 */
 obj_t BgL_tmpz00_10603;
if(
INTEGERP(BgL_i2z00_2027))
{ /* Ieee/string.scm 1344 */
BgL_tmpz00_10603 = BgL_i2z00_2027
; }  else 
{ 
 obj_t BgL_auxz00_10606;
BgL_auxz00_10606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54529L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_2027); 
FAILURE(BgL_auxz00_10606,BFALSE,BFALSE);} 
BgL_kz00_3641 = 
(long)CINT(BgL_tmpz00_10603); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2561z00_4519;
BgL_l2561z00_4519 = 
STRING_LENGTH(BgL_s2z00_217); 
if(
BOUND_CHECK(BgL_kz00_3641, BgL_l2561z00_4519))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_10602 = 
STRING_REF(BgL_s2z00_217, BgL_kz00_3641)
; }  else 
{ 
 obj_t BgL_auxz00_10615;
BgL_auxz00_10615 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_217, 
(int)(BgL_l2561z00_4519), 
(int)(BgL_kz00_3641)); 
FAILURE(BgL_auxz00_10615,BFALSE,BFALSE);} } } 
BgL_auxz00_10601 = 
toupper(BgL_tmpz00_10602); } 
{ /* Ieee/string.scm 1344 */
 unsigned char BgL_tmpz00_10581;
{ /* Ieee/string.scm 1344 */
 long BgL_kz00_3639;
{ /* Ieee/string.scm 1344 */
 obj_t BgL_tmpz00_10582;
if(
INTEGERP(BgL_i1z00_2026))
{ /* Ieee/string.scm 1344 */
BgL_tmpz00_10582 = BgL_i1z00_2026
; }  else 
{ 
 obj_t BgL_auxz00_10585;
BgL_auxz00_10585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54510L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_2026); 
FAILURE(BgL_auxz00_10585,BFALSE,BFALSE);} 
BgL_kz00_3639 = 
(long)CINT(BgL_tmpz00_10582); } 
{ /* Ieee/string.scm 331 */
 long BgL_l2557z00_4515;
BgL_l2557z00_4515 = 
STRING_LENGTH(BgL_s1z00_216); 
if(
BOUND_CHECK(BgL_kz00_3639, BgL_l2557z00_4515))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_10581 = 
STRING_REF(BgL_s1z00_216, BgL_kz00_3639)
; }  else 
{ 
 obj_t BgL_auxz00_10594;
BgL_auxz00_10594 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_216, 
(int)(BgL_l2557z00_4515), 
(int)(BgL_kz00_3639)); 
FAILURE(BgL_auxz00_10594,BFALSE,BFALSE);} } } 
BgL_tmpz00_10580 = 
toupper(BgL_tmpz00_10581); } 
BgL_test4454z00_10579 = 
(BgL_tmpz00_10580==BgL_auxz00_10601); } 
if(BgL_test4454z00_10579)
{ 
 obj_t BgL_i2z00_10633; obj_t BgL_i1z00_10623;
{ 
 obj_t BgL_tmpz00_10624;
if(
INTEGERP(BgL_i1z00_2026))
{ /* Ieee/string.scm 1345 */
BgL_tmpz00_10624 = BgL_i1z00_2026
; }  else 
{ 
 obj_t BgL_auxz00_10627;
BgL_auxz00_10627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54551L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i1z00_2026); 
FAILURE(BgL_auxz00_10627,BFALSE,BFALSE);} 
BgL_i1z00_10623 = 
ADDFX(BgL_tmpz00_10624, 
BINT(1L)); } 
{ 
 obj_t BgL_tmpz00_10634;
if(
INTEGERP(BgL_i2z00_2027))
{ /* Ieee/string.scm 1345 */
BgL_tmpz00_10634 = BgL_i2z00_2027
; }  else 
{ 
 obj_t BgL_auxz00_10637;
BgL_auxz00_10637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54562L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_i2z00_2027); 
FAILURE(BgL_auxz00_10637,BFALSE,BFALSE);} 
BgL_i2z00_10633 = 
ADDFX(BgL_tmpz00_10634, 
BINT(1L)); } 
BgL_i2z00_2027 = BgL_i2z00_10633; 
BgL_i1z00_2026 = BgL_i1z00_10623; 
goto BgL_zc3z04anonymousza31832ze3z87_2028;}  else 
{ /* Ieee/string.scm 1344 */
return ((bool_t)0);} } } } } } } } } } } } } 

}



/* _string-suffix? */
obj_t BGl__stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t BgL_env1186z00_231, obj_t BgL_opt1185z00_230)
{
{ /* Ieee/string.scm 1352 */
{ /* Ieee/string.scm 1352 */
 obj_t BgL_s1z00_2039; obj_t BgL_s2z00_2040;
BgL_s1z00_2039 = 
VECTOR_REF(BgL_opt1185z00_230,0L); 
BgL_s2z00_2040 = 
VECTOR_REF(BgL_opt1185z00_230,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1185z00_230)) { case 2L : 

{ /* Ieee/string.scm 1352 */

{ /* Ieee/string.scm 1352 */
 bool_t BgL_tmpz00_10645;
{ /* Ieee/string.scm 1352 */
 obj_t BgL_auxz00_10653; obj_t BgL_auxz00_10646;
if(
STRINGP(BgL_s2z00_2040))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10653 = BgL_s2z00_2040
; }  else 
{ 
 obj_t BgL_auxz00_10656;
BgL_auxz00_10656 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2040); 
FAILURE(BgL_auxz00_10656,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2039))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10646 = BgL_s1z00_2039
; }  else 
{ 
 obj_t BgL_auxz00_10649;
BgL_auxz00_10649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2039); 
FAILURE(BgL_auxz00_10649,BFALSE,BFALSE);} 
BgL_tmpz00_10645 = 
BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10646, BgL_auxz00_10653, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10645);} } break;case 3L : 

{ /* Ieee/string.scm 1352 */
 obj_t BgL_start1z00_2047;
BgL_start1z00_2047 = 
VECTOR_REF(BgL_opt1185z00_230,2L); 
{ /* Ieee/string.scm 1352 */

{ /* Ieee/string.scm 1352 */
 bool_t BgL_tmpz00_10663;
{ /* Ieee/string.scm 1352 */
 obj_t BgL_auxz00_10671; obj_t BgL_auxz00_10664;
if(
STRINGP(BgL_s2z00_2040))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10671 = BgL_s2z00_2040
; }  else 
{ 
 obj_t BgL_auxz00_10674;
BgL_auxz00_10674 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2040); 
FAILURE(BgL_auxz00_10674,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2039))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10664 = BgL_s1z00_2039
; }  else 
{ 
 obj_t BgL_auxz00_10667;
BgL_auxz00_10667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2039); 
FAILURE(BgL_auxz00_10667,BFALSE,BFALSE);} 
BgL_tmpz00_10663 = 
BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10664, BgL_auxz00_10671, BgL_start1z00_2047, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10663);} } } break;case 4L : 

{ /* Ieee/string.scm 1352 */
 obj_t BgL_start1z00_2051;
BgL_start1z00_2051 = 
VECTOR_REF(BgL_opt1185z00_230,2L); 
{ /* Ieee/string.scm 1352 */
 obj_t BgL_end1z00_2052;
BgL_end1z00_2052 = 
VECTOR_REF(BgL_opt1185z00_230,3L); 
{ /* Ieee/string.scm 1352 */

{ /* Ieee/string.scm 1352 */
 bool_t BgL_tmpz00_10682;
{ /* Ieee/string.scm 1352 */
 obj_t BgL_auxz00_10690; obj_t BgL_auxz00_10683;
if(
STRINGP(BgL_s2z00_2040))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10690 = BgL_s2z00_2040
; }  else 
{ 
 obj_t BgL_auxz00_10693;
BgL_auxz00_10693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2040); 
FAILURE(BgL_auxz00_10693,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2039))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10683 = BgL_s1z00_2039
; }  else 
{ 
 obj_t BgL_auxz00_10686;
BgL_auxz00_10686 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2039); 
FAILURE(BgL_auxz00_10686,BFALSE,BFALSE);} 
BgL_tmpz00_10682 = 
BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10683, BgL_auxz00_10690, BgL_start1z00_2051, BgL_end1z00_2052, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10682);} } } } break;case 5L : 

{ /* Ieee/string.scm 1352 */
 obj_t BgL_start1z00_2055;
BgL_start1z00_2055 = 
VECTOR_REF(BgL_opt1185z00_230,2L); 
{ /* Ieee/string.scm 1352 */
 obj_t BgL_end1z00_2056;
BgL_end1z00_2056 = 
VECTOR_REF(BgL_opt1185z00_230,3L); 
{ /* Ieee/string.scm 1352 */
 obj_t BgL_start2z00_2057;
BgL_start2z00_2057 = 
VECTOR_REF(BgL_opt1185z00_230,4L); 
{ /* Ieee/string.scm 1352 */

{ /* Ieee/string.scm 1352 */
 bool_t BgL_tmpz00_10702;
{ /* Ieee/string.scm 1352 */
 obj_t BgL_auxz00_10710; obj_t BgL_auxz00_10703;
if(
STRINGP(BgL_s2z00_2040))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10710 = BgL_s2z00_2040
; }  else 
{ 
 obj_t BgL_auxz00_10713;
BgL_auxz00_10713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2040); 
FAILURE(BgL_auxz00_10713,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2039))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10703 = BgL_s1z00_2039
; }  else 
{ 
 obj_t BgL_auxz00_10706;
BgL_auxz00_10706 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2039); 
FAILURE(BgL_auxz00_10706,BFALSE,BFALSE);} 
BgL_tmpz00_10702 = 
BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10703, BgL_auxz00_10710, BgL_start1z00_2055, BgL_end1z00_2056, BgL_start2z00_2057, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10702);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1352 */
 obj_t BgL_start1z00_2059;
BgL_start1z00_2059 = 
VECTOR_REF(BgL_opt1185z00_230,2L); 
{ /* Ieee/string.scm 1352 */
 obj_t BgL_end1z00_2060;
BgL_end1z00_2060 = 
VECTOR_REF(BgL_opt1185z00_230,3L); 
{ /* Ieee/string.scm 1352 */
 obj_t BgL_start2z00_2061;
BgL_start2z00_2061 = 
VECTOR_REF(BgL_opt1185z00_230,4L); 
{ /* Ieee/string.scm 1352 */
 obj_t BgL_end2z00_2062;
BgL_end2z00_2062 = 
VECTOR_REF(BgL_opt1185z00_230,5L); 
{ /* Ieee/string.scm 1352 */

{ /* Ieee/string.scm 1352 */
 bool_t BgL_tmpz00_10723;
{ /* Ieee/string.scm 1352 */
 obj_t BgL_auxz00_10731; obj_t BgL_auxz00_10724;
if(
STRINGP(BgL_s2z00_2040))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10731 = BgL_s2z00_2040
; }  else 
{ 
 obj_t BgL_auxz00_10734;
BgL_auxz00_10734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2040); 
FAILURE(BgL_auxz00_10734,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2039))
{ /* Ieee/string.scm 1352 */
BgL_auxz00_10724 = BgL_s1z00_2039
; }  else 
{ 
 obj_t BgL_auxz00_10727;
BgL_auxz00_10727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(54818L), BGl_string3523z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2039); 
FAILURE(BgL_auxz00_10727,BFALSE,BFALSE);} 
BgL_tmpz00_10723 = 
BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(BgL_auxz00_10724, BgL_auxz00_10731, BgL_start1z00_2059, BgL_end1z00_2060, BgL_start2z00_2061, BgL_end2z00_2062); } 
return 
BBOOL(BgL_tmpz00_10723);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3521z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1185z00_230)));} } } } 

}



/* string-suffix? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t BgL_s1z00_224, obj_t BgL_s2z00_225, obj_t BgL_start1z00_226, obj_t BgL_end1z00_227, obj_t BgL_start2z00_228, obj_t BgL_end2z00_229)
{
{ /* Ieee/string.scm 1352 */
{ /* Ieee/string.scm 1354 */
 long BgL_l1z00_2064;
BgL_l1z00_2064 = 
STRING_LENGTH(BgL_s1z00_224); 
{ /* Ieee/string.scm 1354 */
 long BgL_l2z00_2065;
BgL_l2z00_2065 = 
STRING_LENGTH(BgL_s2z00_225); 
{ /* Ieee/string.scm 1355 */
 obj_t BgL_b1z00_2066;
{ /* Ieee/string.scm 1356 */
 obj_t BgL_procz00_3654;
BgL_procz00_3654 = BGl_symbol3524z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_227))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4472z00_10749;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3659;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_10750;
if(
INTEGERP(BgL_end1z00_227))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_10750 = BgL_end1z00_227
; }  else 
{ 
 obj_t BgL_auxz00_10753;
BgL_auxz00_10753 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_227); 
FAILURE(BgL_auxz00_10753,BFALSE,BFALSE);} 
BgL_n1z00_3659 = 
(long)CINT(BgL_tmpz00_10750); } 
BgL_test4472z00_10749 = 
(BgL_n1z00_3659<=0L); } 
if(BgL_test4472z00_10749)
{ /* Ieee/string.scm 1212 */
BgL_b1z00_2066 = 
BGl_errorz00zz__errorz00(BgL_procz00_3654, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_227); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4474z00_10761;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3660;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_10762;
if(
INTEGERP(BgL_end1z00_227))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_10762 = BgL_end1z00_227
; }  else 
{ 
 obj_t BgL_auxz00_10765;
BgL_auxz00_10765 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_227); 
FAILURE(BgL_auxz00_10765,BFALSE,BFALSE);} 
BgL_n1z00_3660 = 
(long)CINT(BgL_tmpz00_10762); } 
BgL_test4474z00_10761 = 
(BgL_n1z00_3660>BgL_l1z00_2064); } 
if(BgL_test4474z00_10761)
{ /* Ieee/string.scm 1214 */
BgL_b1z00_2066 = 
BGl_errorz00zz__errorz00(BgL_procz00_3654, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_227); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b1z00_2066 = BgL_end1z00_227; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b1z00_2066 = 
BINT(BgL_l1z00_2064); } } 
{ /* Ieee/string.scm 1356 */
 obj_t BgL_b2z00_2067;
{ /* Ieee/string.scm 1357 */
 obj_t BgL_procz00_3662;
BgL_procz00_3662 = BGl_symbol3524z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_229))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4477z00_10776;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3667;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_10777;
if(
INTEGERP(BgL_end2z00_229))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_10777 = BgL_end2z00_229
; }  else 
{ 
 obj_t BgL_auxz00_10780;
BgL_auxz00_10780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_229); 
FAILURE(BgL_auxz00_10780,BFALSE,BFALSE);} 
BgL_n1z00_3667 = 
(long)CINT(BgL_tmpz00_10777); } 
BgL_test4477z00_10776 = 
(BgL_n1z00_3667<=0L); } 
if(BgL_test4477z00_10776)
{ /* Ieee/string.scm 1212 */
BgL_b2z00_2067 = 
BGl_errorz00zz__errorz00(BgL_procz00_3662, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_229); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4479z00_10788;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3668;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_10789;
if(
INTEGERP(BgL_end2z00_229))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_10789 = BgL_end2z00_229
; }  else 
{ 
 obj_t BgL_auxz00_10792;
BgL_auxz00_10792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_229); 
FAILURE(BgL_auxz00_10792,BFALSE,BFALSE);} 
BgL_n1z00_3668 = 
(long)CINT(BgL_tmpz00_10789); } 
BgL_test4479z00_10788 = 
(BgL_n1z00_3668>BgL_l2z00_2065); } 
if(BgL_test4479z00_10788)
{ /* Ieee/string.scm 1214 */
BgL_b2z00_2067 = 
BGl_errorz00zz__errorz00(BgL_procz00_3662, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_229); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b2z00_2067 = BgL_end2z00_229; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b2z00_2067 = 
BINT(BgL_l2z00_2065); } } 
{ /* Ieee/string.scm 1357 */
 obj_t BgL_e1z00_2068;
{ /* Ieee/string.scm 1358 */
 obj_t BgL_procz00_3670;
BgL_procz00_3670 = BGl_symbol3524z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_226))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4482z00_10803;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3675;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_10804;
if(
INTEGERP(BgL_start1z00_226))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_10804 = BgL_start1z00_226
; }  else 
{ 
 obj_t BgL_auxz00_10807;
BgL_auxz00_10807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_226); 
FAILURE(BgL_auxz00_10807,BFALSE,BFALSE);} 
BgL_n1z00_3675 = 
(long)CINT(BgL_tmpz00_10804); } 
BgL_test4482z00_10803 = 
(BgL_n1z00_3675<0L); } 
if(BgL_test4482z00_10803)
{ /* Ieee/string.scm 1198 */
BgL_e1z00_2068 = 
BGl_errorz00zz__errorz00(BgL_procz00_3670, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_226); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4484z00_10815;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3676;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_10816;
if(
INTEGERP(BgL_start1z00_226))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_10816 = BgL_start1z00_226
; }  else 
{ 
 obj_t BgL_auxz00_10819;
BgL_auxz00_10819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_226); 
FAILURE(BgL_auxz00_10819,BFALSE,BFALSE);} 
BgL_n1z00_3676 = 
(long)CINT(BgL_tmpz00_10816); } 
BgL_test4484z00_10815 = 
(BgL_n1z00_3676>=BgL_l1z00_2064); } 
if(BgL_test4484z00_10815)
{ /* Ieee/string.scm 1200 */
BgL_e1z00_2068 = 
BGl_errorz00zz__errorz00(BgL_procz00_3670, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_226); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e1z00_2068 = BgL_start1z00_226; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e1z00_2068 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1358 */
 obj_t BgL_e2z00_2069;
{ /* Ieee/string.scm 1359 */
 obj_t BgL_procz00_3678;
BgL_procz00_3678 = BGl_symbol3524z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_228))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4487z00_10830;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3683;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_10831;
if(
INTEGERP(BgL_start2z00_228))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_10831 = BgL_start2z00_228
; }  else 
{ 
 obj_t BgL_auxz00_10834;
BgL_auxz00_10834 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_228); 
FAILURE(BgL_auxz00_10834,BFALSE,BFALSE);} 
BgL_n1z00_3683 = 
(long)CINT(BgL_tmpz00_10831); } 
BgL_test4487z00_10830 = 
(BgL_n1z00_3683<0L); } 
if(BgL_test4487z00_10830)
{ /* Ieee/string.scm 1198 */
BgL_e2z00_2069 = 
BGl_errorz00zz__errorz00(BgL_procz00_3678, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_228); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4489z00_10842;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3684;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_10843;
if(
INTEGERP(BgL_start2z00_228))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_10843 = BgL_start2z00_228
; }  else 
{ 
 obj_t BgL_auxz00_10846;
BgL_auxz00_10846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_228); 
FAILURE(BgL_auxz00_10846,BFALSE,BFALSE);} 
BgL_n1z00_3684 = 
(long)CINT(BgL_tmpz00_10843); } 
BgL_test4489z00_10842 = 
(BgL_n1z00_3684>=BgL_l2z00_2065); } 
if(BgL_test4489z00_10842)
{ /* Ieee/string.scm 1200 */
BgL_e2z00_2069 = 
BGl_errorz00zz__errorz00(BgL_procz00_3678, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_228); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e2z00_2069 = BgL_start2z00_228; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e2z00_2069 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1359 */

{ 
 long BgL_i1z00_2073; long BgL_i2z00_2074;
{ /* Ieee/string.scm 1360 */
 long BgL_za71za7_3686;
{ /* Ieee/string.scm 1360 */
 obj_t BgL_tmpz00_10903;
if(
INTEGERP(BgL_b1z00_2066))
{ /* Ieee/string.scm 1360 */
BgL_tmpz00_10903 = BgL_b1z00_2066
; }  else 
{ 
 obj_t BgL_auxz00_10906;
BgL_auxz00_10906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55251L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_2066); 
FAILURE(BgL_auxz00_10906,BFALSE,BFALSE);} 
BgL_za71za7_3686 = 
(long)CINT(BgL_tmpz00_10903); } 
BgL_i1z00_2073 = 
(BgL_za71za7_3686-1L); } 
{ /* Ieee/string.scm 1361 */
 long BgL_za71za7_3687;
{ /* Ieee/string.scm 1361 */
 obj_t BgL_tmpz00_10912;
if(
INTEGERP(BgL_b2z00_2067))
{ /* Ieee/string.scm 1361 */
BgL_tmpz00_10912 = BgL_b2z00_2067
; }  else 
{ 
 obj_t BgL_auxz00_10915;
BgL_auxz00_10915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55270L), BGl_string3525z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b2z00_2067); 
FAILURE(BgL_auxz00_10915,BFALSE,BFALSE);} 
BgL_za71za7_3687 = 
(long)CINT(BgL_tmpz00_10912); } 
BgL_i2z00_2074 = 
(BgL_za71za7_3687-1L); } 
BgL_zc3z04anonymousza31844ze3z87_2075:
{ /* Ieee/string.scm 1363 */
 bool_t BgL_test4491z00_10855;
{ /* Ieee/string.scm 1363 */
 long BgL_n2z00_3689;
{ /* Ieee/string.scm 1363 */
 obj_t BgL_tmpz00_10856;
if(
INTEGERP(BgL_e1z00_2068))
{ /* Ieee/string.scm 1363 */
BgL_tmpz00_10856 = BgL_e1z00_2068
; }  else 
{ 
 obj_t BgL_auxz00_10859;
BgL_auxz00_10859 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55300L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_2068); 
FAILURE(BgL_auxz00_10859,BFALSE,BFALSE);} 
BgL_n2z00_3689 = 
(long)CINT(BgL_tmpz00_10856); } 
BgL_test4491z00_10855 = 
(BgL_i1z00_2073<BgL_n2z00_3689); } 
if(BgL_test4491z00_10855)
{ /* Ieee/string.scm 1363 */
return ((bool_t)1);}  else 
{ /* Ieee/string.scm 1365 */
 bool_t BgL_test4493z00_10865;
{ /* Ieee/string.scm 1365 */
 long BgL_n2z00_3691;
{ /* Ieee/string.scm 1365 */
 obj_t BgL_tmpz00_10866;
if(
INTEGERP(BgL_e2z00_2069))
{ /* Ieee/string.scm 1365 */
BgL_tmpz00_10866 = BgL_e2z00_2069
; }  else 
{ 
 obj_t BgL_auxz00_10869;
BgL_auxz00_10869 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55328L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_2069); 
FAILURE(BgL_auxz00_10869,BFALSE,BFALSE);} 
BgL_n2z00_3691 = 
(long)CINT(BgL_tmpz00_10866); } 
BgL_test4493z00_10865 = 
(BgL_i2z00_2074<BgL_n2z00_3691); } 
if(BgL_test4493z00_10865)
{ /* Ieee/string.scm 1365 */
return ((bool_t)0);}  else 
{ /* Ieee/string.scm 1367 */
 bool_t BgL_test4495z00_10875;
{ /* Ieee/string.scm 1367 */
 unsigned char BgL_auxz00_10887; unsigned char BgL_tmpz00_10876;
{ /* Ieee/string.scm 331 */
 long BgL_l2569z00_4527;
BgL_l2569z00_4527 = 
STRING_LENGTH(BgL_s2z00_225); 
if(
BOUND_CHECK(BgL_i2z00_2074, BgL_l2569z00_4527))
{ /* Ieee/string.scm 331 */
BgL_auxz00_10887 = 
STRING_REF(BgL_s2z00_225, BgL_i2z00_2074)
; }  else 
{ 
 obj_t BgL_auxz00_10892;
BgL_auxz00_10892 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_225, 
(int)(BgL_l2569z00_4527), 
(int)(BgL_i2z00_2074)); 
FAILURE(BgL_auxz00_10892,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 331 */
 long BgL_l2565z00_4523;
BgL_l2565z00_4523 = 
STRING_LENGTH(BgL_s1z00_224); 
if(
BOUND_CHECK(BgL_i1z00_2073, BgL_l2565z00_4523))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_10876 = 
STRING_REF(BgL_s1z00_224, BgL_i1z00_2073)
; }  else 
{ 
 obj_t BgL_auxz00_10881;
BgL_auxz00_10881 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_224, 
(int)(BgL_l2565z00_4523), 
(int)(BgL_i1z00_2073)); 
FAILURE(BgL_auxz00_10881,BFALSE,BFALSE);} } 
BgL_test4495z00_10875 = 
(BgL_tmpz00_10876==BgL_auxz00_10887); } 
if(BgL_test4495z00_10875)
{ 
 long BgL_i2z00_10901; long BgL_i1z00_10899;
BgL_i1z00_10899 = 
(BgL_i1z00_2073-1L); 
BgL_i2z00_10901 = 
(BgL_i2z00_2074-1L); 
BgL_i2z00_2074 = BgL_i2z00_10901; 
BgL_i1z00_2073 = BgL_i1z00_10899; 
goto BgL_zc3z04anonymousza31844ze3z87_2075;}  else 
{ /* Ieee/string.scm 1367 */
return ((bool_t)0);} } } } } } } } } } } } } 

}



/* _string-suffix-ci? */
obj_t BGl__stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t BgL_env1190z00_239, obj_t BgL_opt1189z00_238)
{
{ /* Ieee/string.scm 1375 */
{ /* Ieee/string.scm 1375 */
 obj_t BgL_s1z00_2086; obj_t BgL_s2z00_2087;
BgL_s1z00_2086 = 
VECTOR_REF(BgL_opt1189z00_238,0L); 
BgL_s2z00_2087 = 
VECTOR_REF(BgL_opt1189z00_238,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1189z00_238)) { case 2L : 

{ /* Ieee/string.scm 1375 */

{ /* Ieee/string.scm 1375 */
 bool_t BgL_tmpz00_10923;
{ /* Ieee/string.scm 1375 */
 obj_t BgL_auxz00_10931; obj_t BgL_auxz00_10924;
if(
STRINGP(BgL_s2z00_2087))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10931 = BgL_s2z00_2087
; }  else 
{ 
 obj_t BgL_auxz00_10934;
BgL_auxz00_10934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2087); 
FAILURE(BgL_auxz00_10934,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2086))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10924 = BgL_s1z00_2086
; }  else 
{ 
 obj_t BgL_auxz00_10927;
BgL_auxz00_10927 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2086); 
FAILURE(BgL_auxz00_10927,BFALSE,BFALSE);} 
BgL_tmpz00_10923 = 
BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10924, BgL_auxz00_10931, BFALSE, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10923);} } break;case 3L : 

{ /* Ieee/string.scm 1375 */
 obj_t BgL_start1z00_2094;
BgL_start1z00_2094 = 
VECTOR_REF(BgL_opt1189z00_238,2L); 
{ /* Ieee/string.scm 1375 */

{ /* Ieee/string.scm 1375 */
 bool_t BgL_tmpz00_10941;
{ /* Ieee/string.scm 1375 */
 obj_t BgL_auxz00_10949; obj_t BgL_auxz00_10942;
if(
STRINGP(BgL_s2z00_2087))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10949 = BgL_s2z00_2087
; }  else 
{ 
 obj_t BgL_auxz00_10952;
BgL_auxz00_10952 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2087); 
FAILURE(BgL_auxz00_10952,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2086))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10942 = BgL_s1z00_2086
; }  else 
{ 
 obj_t BgL_auxz00_10945;
BgL_auxz00_10945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2086); 
FAILURE(BgL_auxz00_10945,BFALSE,BFALSE);} 
BgL_tmpz00_10941 = 
BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10942, BgL_auxz00_10949, BgL_start1z00_2094, BFALSE, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10941);} } } break;case 4L : 

{ /* Ieee/string.scm 1375 */
 obj_t BgL_start1z00_2098;
BgL_start1z00_2098 = 
VECTOR_REF(BgL_opt1189z00_238,2L); 
{ /* Ieee/string.scm 1375 */
 obj_t BgL_end1z00_2099;
BgL_end1z00_2099 = 
VECTOR_REF(BgL_opt1189z00_238,3L); 
{ /* Ieee/string.scm 1375 */

{ /* Ieee/string.scm 1375 */
 bool_t BgL_tmpz00_10960;
{ /* Ieee/string.scm 1375 */
 obj_t BgL_auxz00_10968; obj_t BgL_auxz00_10961;
if(
STRINGP(BgL_s2z00_2087))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10968 = BgL_s2z00_2087
; }  else 
{ 
 obj_t BgL_auxz00_10971;
BgL_auxz00_10971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2087); 
FAILURE(BgL_auxz00_10971,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2086))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10961 = BgL_s1z00_2086
; }  else 
{ 
 obj_t BgL_auxz00_10964;
BgL_auxz00_10964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2086); 
FAILURE(BgL_auxz00_10964,BFALSE,BFALSE);} 
BgL_tmpz00_10960 = 
BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10961, BgL_auxz00_10968, BgL_start1z00_2098, BgL_end1z00_2099, BFALSE, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10960);} } } } break;case 5L : 

{ /* Ieee/string.scm 1375 */
 obj_t BgL_start1z00_2102;
BgL_start1z00_2102 = 
VECTOR_REF(BgL_opt1189z00_238,2L); 
{ /* Ieee/string.scm 1375 */
 obj_t BgL_end1z00_2103;
BgL_end1z00_2103 = 
VECTOR_REF(BgL_opt1189z00_238,3L); 
{ /* Ieee/string.scm 1375 */
 obj_t BgL_start2z00_2104;
BgL_start2z00_2104 = 
VECTOR_REF(BgL_opt1189z00_238,4L); 
{ /* Ieee/string.scm 1375 */

{ /* Ieee/string.scm 1375 */
 bool_t BgL_tmpz00_10980;
{ /* Ieee/string.scm 1375 */
 obj_t BgL_auxz00_10988; obj_t BgL_auxz00_10981;
if(
STRINGP(BgL_s2z00_2087))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10988 = BgL_s2z00_2087
; }  else 
{ 
 obj_t BgL_auxz00_10991;
BgL_auxz00_10991 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2087); 
FAILURE(BgL_auxz00_10991,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2086))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_10981 = BgL_s1z00_2086
; }  else 
{ 
 obj_t BgL_auxz00_10984;
BgL_auxz00_10984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2086); 
FAILURE(BgL_auxz00_10984,BFALSE,BFALSE);} 
BgL_tmpz00_10980 = 
BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_10981, BgL_auxz00_10988, BgL_start1z00_2102, BgL_end1z00_2103, BgL_start2z00_2104, BFALSE); } 
return 
BBOOL(BgL_tmpz00_10980);} } } } } break;case 6L : 

{ /* Ieee/string.scm 1375 */
 obj_t BgL_start1z00_2106;
BgL_start1z00_2106 = 
VECTOR_REF(BgL_opt1189z00_238,2L); 
{ /* Ieee/string.scm 1375 */
 obj_t BgL_end1z00_2107;
BgL_end1z00_2107 = 
VECTOR_REF(BgL_opt1189z00_238,3L); 
{ /* Ieee/string.scm 1375 */
 obj_t BgL_start2z00_2108;
BgL_start2z00_2108 = 
VECTOR_REF(BgL_opt1189z00_238,4L); 
{ /* Ieee/string.scm 1375 */
 obj_t BgL_end2z00_2109;
BgL_end2z00_2109 = 
VECTOR_REF(BgL_opt1189z00_238,5L); 
{ /* Ieee/string.scm 1375 */

{ /* Ieee/string.scm 1375 */
 bool_t BgL_tmpz00_11001;
{ /* Ieee/string.scm 1375 */
 obj_t BgL_auxz00_11009; obj_t BgL_auxz00_11002;
if(
STRINGP(BgL_s2z00_2087))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_11009 = BgL_s2z00_2087
; }  else 
{ 
 obj_t BgL_auxz00_11012;
BgL_auxz00_11012 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s2z00_2087); 
FAILURE(BgL_auxz00_11012,BFALSE,BFALSE);} 
if(
STRINGP(BgL_s1z00_2086))
{ /* Ieee/string.scm 1375 */
BgL_auxz00_11002 = BgL_s1z00_2086
; }  else 
{ 
 obj_t BgL_auxz00_11005;
BgL_auxz00_11005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(55679L), BGl_string3528z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_s1z00_2086); 
FAILURE(BgL_auxz00_11005,BFALSE,BFALSE);} 
BgL_tmpz00_11001 = 
BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(BgL_auxz00_11002, BgL_auxz00_11009, BgL_start1z00_2106, BgL_end1z00_2107, BgL_start2z00_2108, BgL_end2z00_2109); } 
return 
BBOOL(BgL_tmpz00_11001);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3526z00zz__r4_strings_6_7z00, BGl_string3483z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1189z00_238)));} } } } 

}



/* string-suffix-ci? */
BGL_EXPORTED_DEF bool_t BGl_stringzd2suffixzd2cizf3zf3zz__r4_strings_6_7z00(obj_t BgL_s1z00_232, obj_t BgL_s2z00_233, obj_t BgL_start1z00_234, obj_t BgL_end1z00_235, obj_t BgL_start2z00_236, obj_t BgL_end2z00_237)
{
{ /* Ieee/string.scm 1375 */
{ /* Ieee/string.scm 1377 */
 long BgL_l1z00_2111;
BgL_l1z00_2111 = 
STRING_LENGTH(BgL_s1z00_232); 
{ /* Ieee/string.scm 1377 */
 long BgL_l2z00_2112;
BgL_l2z00_2112 = 
STRING_LENGTH(BgL_s2z00_233); 
{ /* Ieee/string.scm 1378 */
 obj_t BgL_b1z00_2113;
{ /* Ieee/string.scm 1379 */
 obj_t BgL_procz00_3702;
BgL_procz00_3702 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end1z00_235))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4511z00_11027;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3707;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_11028;
if(
INTEGERP(BgL_end1z00_235))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_11028 = BgL_end1z00_235
; }  else 
{ 
 obj_t BgL_auxz00_11031;
BgL_auxz00_11031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_235); 
FAILURE(BgL_auxz00_11031,BFALSE,BFALSE);} 
BgL_n1z00_3707 = 
(long)CINT(BgL_tmpz00_11028); } 
BgL_test4511z00_11027 = 
(BgL_n1z00_3707<=0L); } 
if(BgL_test4511z00_11027)
{ /* Ieee/string.scm 1212 */
BgL_b1z00_2113 = 
BGl_errorz00zz__errorz00(BgL_procz00_3702, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_235); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4513z00_11039;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3708;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_11040;
if(
INTEGERP(BgL_end1z00_235))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_11040 = BgL_end1z00_235
; }  else 
{ 
 obj_t BgL_auxz00_11043;
BgL_auxz00_11043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end1z00_235); 
FAILURE(BgL_auxz00_11043,BFALSE,BFALSE);} 
BgL_n1z00_3708 = 
(long)CINT(BgL_tmpz00_11040); } 
BgL_test4513z00_11039 = 
(BgL_n1z00_3708>BgL_l1z00_2111); } 
if(BgL_test4513z00_11039)
{ /* Ieee/string.scm 1214 */
BgL_b1z00_2113 = 
BGl_errorz00zz__errorz00(BgL_procz00_3702, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3488z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end1z00_235); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b1z00_2113 = BgL_end1z00_235; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b1z00_2113 = 
BINT(BgL_l1z00_2111); } } 
{ /* Ieee/string.scm 1379 */
 obj_t BgL_b2z00_2114;
{ /* Ieee/string.scm 1380 */
 obj_t BgL_procz00_3710;
BgL_procz00_3710 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_end2z00_237))
{ /* Ieee/string.scm 1212 */
 bool_t BgL_test4516z00_11054;
{ /* Ieee/string.scm 1212 */
 long BgL_n1z00_3715;
{ /* Ieee/string.scm 1212 */
 obj_t BgL_tmpz00_11055;
if(
INTEGERP(BgL_end2z00_237))
{ /* Ieee/string.scm 1212 */
BgL_tmpz00_11055 = BgL_end2z00_237
; }  else 
{ 
 obj_t BgL_auxz00_11058;
BgL_auxz00_11058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49072L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_237); 
FAILURE(BgL_auxz00_11058,BFALSE,BFALSE);} 
BgL_n1z00_3715 = 
(long)CINT(BgL_tmpz00_11055); } 
BgL_test4516z00_11054 = 
(BgL_n1z00_3715<=0L); } 
if(BgL_test4516z00_11054)
{ /* Ieee/string.scm 1212 */
BgL_b2z00_2114 = 
BGl_errorz00zz__errorz00(BgL_procz00_3710, 
string_append_3(BGl_string3487z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_237); }  else 
{ /* Ieee/string.scm 1214 */
 bool_t BgL_test4518z00_11066;
{ /* Ieee/string.scm 1214 */
 long BgL_n1z00_3716;
{ /* Ieee/string.scm 1214 */
 obj_t BgL_tmpz00_11067;
if(
INTEGERP(BgL_end2z00_237))
{ /* Ieee/string.scm 1214 */
BgL_tmpz00_11067 = BgL_end2z00_237
; }  else 
{ 
 obj_t BgL_auxz00_11070;
BgL_auxz00_11070 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(49164L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_end2z00_237); 
FAILURE(BgL_auxz00_11070,BFALSE,BFALSE);} 
BgL_n1z00_3716 = 
(long)CINT(BgL_tmpz00_11067); } 
BgL_test4518z00_11066 = 
(BgL_n1z00_3716>BgL_l2z00_2112); } 
if(BgL_test4518z00_11066)
{ /* Ieee/string.scm 1214 */
BgL_b2z00_2114 = 
BGl_errorz00zz__errorz00(BgL_procz00_3710, 
string_append_3(BGl_string3490z00zz__r4_strings_6_7z00, BGl_string3491z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_end2z00_237); }  else 
{ /* Ieee/string.scm 1214 */
BgL_b2z00_2114 = BgL_end2z00_237; } } }  else 
{ /* Ieee/string.scm 1210 */
BgL_b2z00_2114 = 
BINT(BgL_l2z00_2112); } } 
{ /* Ieee/string.scm 1380 */
 obj_t BgL_e1z00_2115;
{ /* Ieee/string.scm 1381 */
 obj_t BgL_procz00_3718;
BgL_procz00_3718 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start1z00_234))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4521z00_11081;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3723;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_11082;
if(
INTEGERP(BgL_start1z00_234))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_11082 = BgL_start1z00_234
; }  else 
{ 
 obj_t BgL_auxz00_11085;
BgL_auxz00_11085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_234); 
FAILURE(BgL_auxz00_11085,BFALSE,BFALSE);} 
BgL_n1z00_3723 = 
(long)CINT(BgL_tmpz00_11082); } 
BgL_test4521z00_11081 = 
(BgL_n1z00_3723<0L); } 
if(BgL_test4521z00_11081)
{ /* Ieee/string.scm 1198 */
BgL_e1z00_2115 = 
BGl_errorz00zz__errorz00(BgL_procz00_3718, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_234); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4523z00_11093;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3724;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_11094;
if(
INTEGERP(BgL_start1z00_234))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_11094 = BgL_start1z00_234
; }  else 
{ 
 obj_t BgL_auxz00_11097;
BgL_auxz00_11097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start1z00_234); 
FAILURE(BgL_auxz00_11097,BFALSE,BFALSE);} 
BgL_n1z00_3724 = 
(long)CINT(BgL_tmpz00_11094); } 
BgL_test4523z00_11093 = 
(BgL_n1z00_3724>=BgL_l1z00_2111); } 
if(BgL_test4523z00_11093)
{ /* Ieee/string.scm 1200 */
BgL_e1z00_2115 = 
BGl_errorz00zz__errorz00(BgL_procz00_3718, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3493z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start1z00_234); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e1z00_2115 = BgL_start1z00_234; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e1z00_2115 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1381 */
 obj_t BgL_e2z00_2116;
{ /* Ieee/string.scm 1382 */
 obj_t BgL_procz00_3726;
BgL_procz00_3726 = BGl_symbol3485z00zz__r4_strings_6_7z00; 
if(
CBOOL(BgL_start2z00_236))
{ /* Ieee/string.scm 1198 */
 bool_t BgL_test4526z00_11108;
{ /* Ieee/string.scm 1198 */
 long BgL_n1z00_3731;
{ /* Ieee/string.scm 1198 */
 obj_t BgL_tmpz00_11109;
if(
INTEGERP(BgL_start2z00_236))
{ /* Ieee/string.scm 1198 */
BgL_tmpz00_11109 = BgL_start2z00_236
; }  else 
{ 
 obj_t BgL_auxz00_11112;
BgL_auxz00_11112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48551L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_236); 
FAILURE(BgL_auxz00_11112,BFALSE,BFALSE);} 
BgL_n1z00_3731 = 
(long)CINT(BgL_tmpz00_11109); } 
BgL_test4526z00_11108 = 
(BgL_n1z00_3731<0L); } 
if(BgL_test4526z00_11108)
{ /* Ieee/string.scm 1198 */
BgL_e2z00_2116 = 
BGl_errorz00zz__errorz00(BgL_procz00_3726, 
string_append_3(BGl_string3492z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_236); }  else 
{ /* Ieee/string.scm 1200 */
 bool_t BgL_test4528z00_11120;
{ /* Ieee/string.scm 1200 */
 long BgL_n1z00_3732;
{ /* Ieee/string.scm 1200 */
 obj_t BgL_tmpz00_11121;
if(
INTEGERP(BgL_start2z00_236))
{ /* Ieee/string.scm 1200 */
BgL_tmpz00_11121 = BgL_start2z00_236
; }  else 
{ 
 obj_t BgL_auxz00_11124;
BgL_auxz00_11124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(48646L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_start2z00_236); 
FAILURE(BgL_auxz00_11124,BFALSE,BFALSE);} 
BgL_n1z00_3732 = 
(long)CINT(BgL_tmpz00_11121); } 
BgL_test4528z00_11120 = 
(BgL_n1z00_3732>=BgL_l2z00_2112); } 
if(BgL_test4528z00_11120)
{ /* Ieee/string.scm 1200 */
BgL_e2z00_2116 = 
BGl_errorz00zz__errorz00(BgL_procz00_3726, 
string_append_3(BGl_string3494z00zz__r4_strings_6_7z00, BGl_string3495z00zz__r4_strings_6_7z00, BGl_string3489z00zz__r4_strings_6_7z00), BgL_start2z00_236); }  else 
{ /* Ieee/string.scm 1200 */
BgL_e2z00_2116 = BgL_start2z00_236; } } }  else 
{ /* Ieee/string.scm 1196 */
BgL_e2z00_2116 = 
BINT(0L); } } 
{ /* Ieee/string.scm 1382 */

{ 
 long BgL_i1z00_2120; long BgL_i2z00_2121;
{ /* Ieee/string.scm 1383 */
 long BgL_za71za7_3734;
{ /* Ieee/string.scm 1383 */
 obj_t BgL_tmpz00_11185;
if(
INTEGERP(BgL_b1z00_2113))
{ /* Ieee/string.scm 1383 */
BgL_tmpz00_11185 = BgL_b1z00_2113
; }  else 
{ 
 obj_t BgL_auxz00_11188;
BgL_auxz00_11188 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56135L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b1z00_2113); 
FAILURE(BgL_auxz00_11188,BFALSE,BFALSE);} 
BgL_za71za7_3734 = 
(long)CINT(BgL_tmpz00_11185); } 
BgL_i1z00_2120 = 
(BgL_za71za7_3734-1L); } 
{ /* Ieee/string.scm 1384 */
 long BgL_za71za7_3735;
{ /* Ieee/string.scm 1384 */
 obj_t BgL_tmpz00_11194;
if(
INTEGERP(BgL_b2z00_2114))
{ /* Ieee/string.scm 1384 */
BgL_tmpz00_11194 = BgL_b2z00_2114
; }  else 
{ 
 obj_t BgL_auxz00_11197;
BgL_auxz00_11197 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56154L), BGl_string3529z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_b2z00_2114); 
FAILURE(BgL_auxz00_11197,BFALSE,BFALSE);} 
BgL_za71za7_3735 = 
(long)CINT(BgL_tmpz00_11194); } 
BgL_i2z00_2121 = 
(BgL_za71za7_3735-1L); } 
BgL_zc3z04anonymousza31855ze3z87_2122:
{ /* Ieee/string.scm 1386 */
 bool_t BgL_test4530z00_11133;
{ /* Ieee/string.scm 1386 */
 long BgL_n2z00_3737;
{ /* Ieee/string.scm 1386 */
 obj_t BgL_tmpz00_11134;
if(
INTEGERP(BgL_e1z00_2115))
{ /* Ieee/string.scm 1386 */
BgL_tmpz00_11134 = BgL_e1z00_2115
; }  else 
{ 
 obj_t BgL_auxz00_11137;
BgL_auxz00_11137 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56184L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e1z00_2115); 
FAILURE(BgL_auxz00_11137,BFALSE,BFALSE);} 
BgL_n2z00_3737 = 
(long)CINT(BgL_tmpz00_11134); } 
BgL_test4530z00_11133 = 
(BgL_i1z00_2120<BgL_n2z00_3737); } 
if(BgL_test4530z00_11133)
{ /* Ieee/string.scm 1386 */
return ((bool_t)1);}  else 
{ /* Ieee/string.scm 1388 */
 bool_t BgL_test4532z00_11143;
{ /* Ieee/string.scm 1388 */
 long BgL_n2z00_3739;
{ /* Ieee/string.scm 1388 */
 obj_t BgL_tmpz00_11144;
if(
INTEGERP(BgL_e2z00_2116))
{ /* Ieee/string.scm 1388 */
BgL_tmpz00_11144 = BgL_e2z00_2116
; }  else 
{ 
 obj_t BgL_auxz00_11147;
BgL_auxz00_11147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56212L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_e2z00_2116); 
FAILURE(BgL_auxz00_11147,BFALSE,BFALSE);} 
BgL_n2z00_3739 = 
(long)CINT(BgL_tmpz00_11144); } 
BgL_test4532z00_11143 = 
(BgL_i2z00_2121<BgL_n2z00_3739); } 
if(BgL_test4532z00_11143)
{ /* Ieee/string.scm 1388 */
return ((bool_t)0);}  else 
{ /* Ieee/string.scm 1390 */
 bool_t BgL_test4534z00_11153;
{ /* Ieee/string.scm 1390 */
 unsigned char BgL_auxz00_11167; unsigned char BgL_tmpz00_11154;
{ /* Ieee/string.scm 1390 */
 unsigned char BgL_tmpz00_11168;
{ /* Ieee/string.scm 331 */
 long BgL_l2577z00_4535;
BgL_l2577z00_4535 = 
STRING_LENGTH(BgL_s2z00_233); 
if(
BOUND_CHECK(BgL_i2z00_2121, BgL_l2577z00_4535))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_11168 = 
STRING_REF(BgL_s2z00_233, BgL_i2z00_2121)
; }  else 
{ 
 obj_t BgL_auxz00_11173;
BgL_auxz00_11173 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s2z00_233, 
(int)(BgL_l2577z00_4535), 
(int)(BgL_i2z00_2121)); 
FAILURE(BgL_auxz00_11173,BFALSE,BFALSE);} } 
BgL_auxz00_11167 = 
toupper(BgL_tmpz00_11168); } 
{ /* Ieee/string.scm 1390 */
 unsigned char BgL_tmpz00_11155;
{ /* Ieee/string.scm 331 */
 long BgL_l2573z00_4531;
BgL_l2573z00_4531 = 
STRING_LENGTH(BgL_s1z00_232); 
if(
BOUND_CHECK(BgL_i1z00_2120, BgL_l2573z00_4531))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_11155 = 
STRING_REF(BgL_s1z00_232, BgL_i1z00_2120)
; }  else 
{ 
 obj_t BgL_auxz00_11160;
BgL_auxz00_11160 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_s1z00_232, 
(int)(BgL_l2573z00_4531), 
(int)(BgL_i1z00_2120)); 
FAILURE(BgL_auxz00_11160,BFALSE,BFALSE);} } 
BgL_tmpz00_11154 = 
toupper(BgL_tmpz00_11155); } 
BgL_test4534z00_11153 = 
(BgL_tmpz00_11154==BgL_auxz00_11167); } 
if(BgL_test4534z00_11153)
{ 
 long BgL_i2z00_11183; long BgL_i1z00_11181;
BgL_i1z00_11181 = 
(BgL_i1z00_2120-1L); 
BgL_i2z00_11183 = 
(BgL_i2z00_2121-1L); 
BgL_i2z00_2121 = BgL_i2z00_11183; 
BgL_i1z00_2120 = BgL_i1z00_11181; 
goto BgL_zc3z04anonymousza31855ze3z87_2122;}  else 
{ /* Ieee/string.scm 1390 */
return ((bool_t)0);} } } } } } } } } } } } } 

}



/* _string-natural-compare3 */
obj_t BGl__stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t BgL_env1194z00_245, obj_t BgL_opt1193z00_244)
{
{ /* Ieee/string.scm 1398 */
{ /* Ieee/string.scm 1398 */
 obj_t BgL_g1195z00_2133; obj_t BgL_g1196z00_2134;
BgL_g1195z00_2133 = 
VECTOR_REF(BgL_opt1193z00_244,0L); 
BgL_g1196z00_2134 = 
VECTOR_REF(BgL_opt1193z00_244,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1193z00_244)) { case 2L : 

{ /* Ieee/string.scm 1398 */

{ /* Ieee/string.scm 1398 */
 int BgL_res2211z00_3756;
{ /* Ieee/string.scm 1398 */
 obj_t BgL_az00_3754; obj_t BgL_bz00_3755;
if(
STRINGP(BgL_g1195z00_2133))
{ /* Ieee/string.scm 1398 */
BgL_az00_3754 = BgL_g1195z00_2133; }  else 
{ 
 obj_t BgL_auxz00_11207;
BgL_auxz00_11207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56566L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1195z00_2133); 
FAILURE(BgL_auxz00_11207,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1196z00_2134))
{ /* Ieee/string.scm 1398 */
BgL_bz00_3755 = BgL_g1196z00_2134; }  else 
{ 
 obj_t BgL_auxz00_11213;
BgL_auxz00_11213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56566L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1196z00_2134); 
FAILURE(BgL_auxz00_11213,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 1399 */
 obj_t BgL_tmpz00_11217;
{ /* Ieee/string.scm 1399 */
 obj_t BgL_aux3281z00_5242;
BgL_aux3281z00_5242 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_3754, BgL_bz00_3755, ((bool_t)0), 
BINT(0L), 
BINT(0L)); 
if(
INTEGERP(BgL_aux3281z00_5242))
{ /* Ieee/string.scm 1399 */
BgL_tmpz00_11217 = BgL_aux3281z00_5242
; }  else 
{ 
 obj_t BgL_auxz00_11223;
BgL_auxz00_11223 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56640L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3281z00_5242); 
FAILURE(BgL_auxz00_11223,BFALSE,BFALSE);} } 
BgL_res2211z00_3756 = 
CINT(BgL_tmpz00_11217); } } 
return 
BINT(BgL_res2211z00_3756);} } break;case 3L : 

{ /* Ieee/string.scm 1398 */
 obj_t BgL_start1z00_2139;
BgL_start1z00_2139 = 
VECTOR_REF(BgL_opt1193z00_244,2L); 
{ /* Ieee/string.scm 1398 */

{ /* Ieee/string.scm 1398 */
 int BgL_res2212z00_3759;
{ /* Ieee/string.scm 1398 */
 obj_t BgL_az00_3757; obj_t BgL_bz00_3758;
if(
STRINGP(BgL_g1195z00_2133))
{ /* Ieee/string.scm 1398 */
BgL_az00_3757 = BgL_g1195z00_2133; }  else 
{ 
 obj_t BgL_auxz00_11232;
BgL_auxz00_11232 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56566L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1195z00_2133); 
FAILURE(BgL_auxz00_11232,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1196z00_2134))
{ /* Ieee/string.scm 1398 */
BgL_bz00_3758 = BgL_g1196z00_2134; }  else 
{ 
 obj_t BgL_auxz00_11238;
BgL_auxz00_11238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56566L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1196z00_2134); 
FAILURE(BgL_auxz00_11238,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 1399 */
 obj_t BgL_tmpz00_11242;
{ /* Ieee/string.scm 1399 */
 obj_t BgL_aux3286z00_5247;
BgL_aux3286z00_5247 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_3757, BgL_bz00_3758, ((bool_t)0), BgL_start1z00_2139, 
BINT(0L)); 
if(
INTEGERP(BgL_aux3286z00_5247))
{ /* Ieee/string.scm 1399 */
BgL_tmpz00_11242 = BgL_aux3286z00_5247
; }  else 
{ 
 obj_t BgL_auxz00_11247;
BgL_auxz00_11247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56640L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3286z00_5247); 
FAILURE(BgL_auxz00_11247,BFALSE,BFALSE);} } 
BgL_res2212z00_3759 = 
CINT(BgL_tmpz00_11242); } } 
return 
BINT(BgL_res2212z00_3759);} } } break;case 4L : 

{ /* Ieee/string.scm 1398 */
 obj_t BgL_start1z00_2141;
BgL_start1z00_2141 = 
VECTOR_REF(BgL_opt1193z00_244,2L); 
{ /* Ieee/string.scm 1398 */
 obj_t BgL_start2z00_2142;
BgL_start2z00_2142 = 
VECTOR_REF(BgL_opt1193z00_244,3L); 
{ /* Ieee/string.scm 1398 */

{ /* Ieee/string.scm 1398 */
 int BgL_res2213z00_3762;
{ /* Ieee/string.scm 1398 */
 obj_t BgL_az00_3760; obj_t BgL_bz00_3761;
if(
STRINGP(BgL_g1195z00_2133))
{ /* Ieee/string.scm 1398 */
BgL_az00_3760 = BgL_g1195z00_2133; }  else 
{ 
 obj_t BgL_auxz00_11257;
BgL_auxz00_11257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56566L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1195z00_2133); 
FAILURE(BgL_auxz00_11257,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1196z00_2134))
{ /* Ieee/string.scm 1398 */
BgL_bz00_3761 = BgL_g1196z00_2134; }  else 
{ 
 obj_t BgL_auxz00_11263;
BgL_auxz00_11263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56566L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1196z00_2134); 
FAILURE(BgL_auxz00_11263,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 1399 */
 obj_t BgL_tmpz00_11267;
{ /* Ieee/string.scm 1399 */
 obj_t BgL_aux3291z00_5252;
BgL_aux3291z00_5252 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_3760, BgL_bz00_3761, ((bool_t)0), BgL_start1z00_2141, BgL_start2z00_2142); 
if(
INTEGERP(BgL_aux3291z00_5252))
{ /* Ieee/string.scm 1399 */
BgL_tmpz00_11267 = BgL_aux3291z00_5252
; }  else 
{ 
 obj_t BgL_auxz00_11271;
BgL_auxz00_11271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56640L), BGl_string3532z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3291z00_5252); 
FAILURE(BgL_auxz00_11271,BFALSE,BFALSE);} } 
BgL_res2213z00_3762 = 
CINT(BgL_tmpz00_11267); } } 
return 
BINT(BgL_res2213z00_3762);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3530z00zz__r4_strings_6_7z00, BGl_string3436z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1193z00_244)));} } } } 

}



/* string-natural-compare3 */
BGL_EXPORTED_DEF int BGl_stringzd2naturalzd2compare3z00zz__r4_strings_6_7z00(obj_t BgL_az00_240, obj_t BgL_bz00_241, obj_t BgL_start1z00_242, obj_t BgL_start2z00_243)
{
{ /* Ieee/string.scm 1398 */
{ /* Ieee/string.scm 1399 */
 obj_t BgL_tmpz00_11282;
{ /* Ieee/string.scm 1399 */
 obj_t BgL_aux3292z00_5253;
BgL_aux3292z00_5253 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_240, BgL_bz00_241, ((bool_t)0), BgL_start1z00_242, BgL_start2z00_243); 
if(
INTEGERP(BgL_aux3292z00_5253))
{ /* Ieee/string.scm 1399 */
BgL_tmpz00_11282 = BgL_aux3292z00_5253
; }  else 
{ 
 obj_t BgL_auxz00_11286;
BgL_auxz00_11286 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56640L), BGl_string3531z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3292z00_5253); 
FAILURE(BgL_auxz00_11286,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_11282);} } 

}



/* _string-natural-compare3-ci */
obj_t BGl__stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t BgL_env1200z00_251, obj_t BgL_opt1199z00_250)
{
{ /* Ieee/string.scm 1404 */
{ /* Ieee/string.scm 1404 */
 obj_t BgL_g1201z00_2144; obj_t BgL_g1202z00_2145;
BgL_g1201z00_2144 = 
VECTOR_REF(BgL_opt1199z00_250,0L); 
BgL_g1202z00_2145 = 
VECTOR_REF(BgL_opt1199z00_250,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1199z00_250)) { case 2L : 

{ /* Ieee/string.scm 1404 */

{ /* Ieee/string.scm 1404 */
 int BgL_res2214z00_3765;
{ /* Ieee/string.scm 1404 */
 obj_t BgL_az00_3763; obj_t BgL_bz00_3764;
if(
STRINGP(BgL_g1201z00_2144))
{ /* Ieee/string.scm 1404 */
BgL_az00_3763 = BgL_g1201z00_2144; }  else 
{ 
 obj_t BgL_auxz00_11295;
BgL_auxz00_11295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56897L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1201z00_2144); 
FAILURE(BgL_auxz00_11295,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1202z00_2145))
{ /* Ieee/string.scm 1404 */
BgL_bz00_3764 = BgL_g1202z00_2145; }  else 
{ 
 obj_t BgL_auxz00_11301;
BgL_auxz00_11301 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56897L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1202z00_2145); 
FAILURE(BgL_auxz00_11301,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 1405 */
 obj_t BgL_tmpz00_11305;
{ /* Ieee/string.scm 1405 */
 obj_t BgL_aux3297z00_5258;
BgL_aux3297z00_5258 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_3763, BgL_bz00_3764, ((bool_t)1), 
BINT(0L), 
BINT(0L)); 
if(
INTEGERP(BgL_aux3297z00_5258))
{ /* Ieee/string.scm 1405 */
BgL_tmpz00_11305 = BgL_aux3297z00_5258
; }  else 
{ 
 obj_t BgL_auxz00_11311;
BgL_auxz00_11311 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56974L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3297z00_5258); 
FAILURE(BgL_auxz00_11311,BFALSE,BFALSE);} } 
BgL_res2214z00_3765 = 
CINT(BgL_tmpz00_11305); } } 
return 
BINT(BgL_res2214z00_3765);} } break;case 3L : 

{ /* Ieee/string.scm 1404 */
 obj_t BgL_start1z00_2150;
BgL_start1z00_2150 = 
VECTOR_REF(BgL_opt1199z00_250,2L); 
{ /* Ieee/string.scm 1404 */

{ /* Ieee/string.scm 1404 */
 int BgL_res2215z00_3768;
{ /* Ieee/string.scm 1404 */
 obj_t BgL_az00_3766; obj_t BgL_bz00_3767;
if(
STRINGP(BgL_g1201z00_2144))
{ /* Ieee/string.scm 1404 */
BgL_az00_3766 = BgL_g1201z00_2144; }  else 
{ 
 obj_t BgL_auxz00_11320;
BgL_auxz00_11320 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56897L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1201z00_2144); 
FAILURE(BgL_auxz00_11320,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1202z00_2145))
{ /* Ieee/string.scm 1404 */
BgL_bz00_3767 = BgL_g1202z00_2145; }  else 
{ 
 obj_t BgL_auxz00_11326;
BgL_auxz00_11326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56897L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1202z00_2145); 
FAILURE(BgL_auxz00_11326,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 1405 */
 obj_t BgL_tmpz00_11330;
{ /* Ieee/string.scm 1405 */
 obj_t BgL_aux3302z00_5263;
BgL_aux3302z00_5263 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_3766, BgL_bz00_3767, ((bool_t)1), BgL_start1z00_2150, 
BINT(0L)); 
if(
INTEGERP(BgL_aux3302z00_5263))
{ /* Ieee/string.scm 1405 */
BgL_tmpz00_11330 = BgL_aux3302z00_5263
; }  else 
{ 
 obj_t BgL_auxz00_11335;
BgL_auxz00_11335 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56974L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3302z00_5263); 
FAILURE(BgL_auxz00_11335,BFALSE,BFALSE);} } 
BgL_res2215z00_3768 = 
CINT(BgL_tmpz00_11330); } } 
return 
BINT(BgL_res2215z00_3768);} } } break;case 4L : 

{ /* Ieee/string.scm 1404 */
 obj_t BgL_start1z00_2152;
BgL_start1z00_2152 = 
VECTOR_REF(BgL_opt1199z00_250,2L); 
{ /* Ieee/string.scm 1404 */
 obj_t BgL_start2z00_2153;
BgL_start2z00_2153 = 
VECTOR_REF(BgL_opt1199z00_250,3L); 
{ /* Ieee/string.scm 1404 */

{ /* Ieee/string.scm 1404 */
 int BgL_res2216z00_3771;
{ /* Ieee/string.scm 1404 */
 obj_t BgL_az00_3769; obj_t BgL_bz00_3770;
if(
STRINGP(BgL_g1201z00_2144))
{ /* Ieee/string.scm 1404 */
BgL_az00_3769 = BgL_g1201z00_2144; }  else 
{ 
 obj_t BgL_auxz00_11345;
BgL_auxz00_11345 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56897L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1201z00_2144); 
FAILURE(BgL_auxz00_11345,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1202z00_2145))
{ /* Ieee/string.scm 1404 */
BgL_bz00_3770 = BgL_g1202z00_2145; }  else 
{ 
 obj_t BgL_auxz00_11351;
BgL_auxz00_11351 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56897L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_g1202z00_2145); 
FAILURE(BgL_auxz00_11351,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 1405 */
 obj_t BgL_tmpz00_11355;
{ /* Ieee/string.scm 1405 */
 obj_t BgL_aux3307z00_5268;
BgL_aux3307z00_5268 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_3769, BgL_bz00_3770, ((bool_t)1), BgL_start1z00_2152, BgL_start2z00_2153); 
if(
INTEGERP(BgL_aux3307z00_5268))
{ /* Ieee/string.scm 1405 */
BgL_tmpz00_11355 = BgL_aux3307z00_5268
; }  else 
{ 
 obj_t BgL_auxz00_11359;
BgL_auxz00_11359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56974L), BGl_string3535z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3307z00_5268); 
FAILURE(BgL_auxz00_11359,BFALSE,BFALSE);} } 
BgL_res2216z00_3771 = 
CINT(BgL_tmpz00_11355); } } 
return 
BINT(BgL_res2216z00_3771);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3533z00zz__r4_strings_6_7z00, BGl_string3436z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1199z00_250)));} } } } 

}



/* string-natural-compare3-ci */
BGL_EXPORTED_DEF int BGl_stringzd2naturalzd2compare3zd2cizd2zz__r4_strings_6_7z00(obj_t BgL_az00_246, obj_t BgL_bz00_247, obj_t BgL_start1z00_248, obj_t BgL_start2z00_249)
{
{ /* Ieee/string.scm 1404 */
{ /* Ieee/string.scm 1405 */
 obj_t BgL_tmpz00_11370;
{ /* Ieee/string.scm 1405 */
 obj_t BgL_aux3308z00_5269;
BgL_aux3308z00_5269 = 
BGl_strnatcmpz00zz__r4_strings_6_7z00(BgL_az00_246, BgL_bz00_247, ((bool_t)1), BgL_start1z00_248, BgL_start2z00_249); 
if(
INTEGERP(BgL_aux3308z00_5269))
{ /* Ieee/string.scm 1405 */
BgL_tmpz00_11370 = BgL_aux3308z00_5269
; }  else 
{ 
 obj_t BgL_auxz00_11374;
BgL_auxz00_11374 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(56974L), BGl_string3534z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3308z00_5269); 
FAILURE(BgL_auxz00_11374,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_11370);} } 

}



/* strnatcmp */
obj_t BGl_strnatcmpz00zz__r4_strings_6_7z00(obj_t BgL_az00_252, obj_t BgL_bz00_253, bool_t BgL_foldcasez00_254, obj_t BgL_start1z00_255, obj_t BgL_start2z00_256)
{
{ /* Ieee/string.scm 1410 */
{ 
 obj_t BgL_iaz00_2156; obj_t BgL_ibz00_2157;
BgL_iaz00_2156 = BgL_start1z00_255; 
BgL_ibz00_2157 = BgL_start2z00_256; 
BgL_zc3z04anonymousza31870ze3z87_2158:
{ /* Ieee/string.scm 1413 */
 unsigned char BgL_caz00_2159; unsigned char BgL_cbz00_2160;
{ /* Ieee/string.scm 1413 */
 obj_t BgL_iz00_3772;
BgL_iz00_3772 = BgL_iaz00_2156; 
{ /* Ieee/string.scm 1500 */
 bool_t BgL_test4559z00_11379;
{ /* Ieee/string.scm 1500 */
 long BgL_n1z00_3776;
{ /* Ieee/string.scm 1500 */
 obj_t BgL_tmpz00_11380;
if(
INTEGERP(BgL_iz00_3772))
{ /* Ieee/string.scm 1500 */
BgL_tmpz00_11380 = BgL_iz00_3772
; }  else 
{ 
 obj_t BgL_auxz00_11383;
BgL_auxz00_11383 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(60044L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_3772); 
FAILURE(BgL_auxz00_11383,BFALSE,BFALSE);} 
BgL_n1z00_3776 = 
(long)CINT(BgL_tmpz00_11380); } 
BgL_test4559z00_11379 = 
(BgL_n1z00_3776>=
STRING_LENGTH(BgL_az00_252)); } 
if(BgL_test4559z00_11379)
{ /* Ieee/string.scm 1500 */
BgL_caz00_2159 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 1502 */
 long BgL_kz00_3779;
{ /* Ieee/string.scm 1502 */
 obj_t BgL_tmpz00_11390;
if(
INTEGERP(BgL_iz00_3772))
{ /* Ieee/string.scm 1502 */
BgL_tmpz00_11390 = BgL_iz00_3772
; }  else 
{ 
 obj_t BgL_auxz00_11393;
BgL_auxz00_11393 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(60102L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_3772); 
FAILURE(BgL_auxz00_11393,BFALSE,BFALSE);} 
BgL_kz00_3779 = 
(long)CINT(BgL_tmpz00_11390); } 
{ /* Ieee/string.scm 343 */
 long BgL_l2581z00_4539;
BgL_l2581z00_4539 = 
STRING_LENGTH(BgL_az00_252); 
if(
BOUND_CHECK(BgL_kz00_3779, BgL_l2581z00_4539))
{ /* Ieee/string.scm 343 */
BgL_caz00_2159 = 
STRING_REF(BgL_az00_252, BgL_kz00_3779); }  else 
{ 
 obj_t BgL_auxz00_11402;
BgL_auxz00_11402 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_az00_252, 
(int)(BgL_l2581z00_4539), 
(int)(BgL_kz00_3779)); 
FAILURE(BgL_auxz00_11402,BFALSE,BFALSE);} } } } } 
{ /* Ieee/string.scm 1414 */
 obj_t BgL_iz00_3780;
BgL_iz00_3780 = BgL_ibz00_2157; 
{ /* Ieee/string.scm 1500 */
 bool_t BgL_test4564z00_11408;
{ /* Ieee/string.scm 1500 */
 long BgL_n1z00_3784;
{ /* Ieee/string.scm 1500 */
 obj_t BgL_tmpz00_11409;
if(
INTEGERP(BgL_iz00_3780))
{ /* Ieee/string.scm 1500 */
BgL_tmpz00_11409 = BgL_iz00_3780
; }  else 
{ 
 obj_t BgL_auxz00_11412;
BgL_auxz00_11412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(60044L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_3780); 
FAILURE(BgL_auxz00_11412,BFALSE,BFALSE);} 
BgL_n1z00_3784 = 
(long)CINT(BgL_tmpz00_11409); } 
BgL_test4564z00_11408 = 
(BgL_n1z00_3784>=
STRING_LENGTH(BgL_bz00_253)); } 
if(BgL_test4564z00_11408)
{ /* Ieee/string.scm 1500 */
BgL_cbz00_2160 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 1502 */
 long BgL_kz00_3787;
{ /* Ieee/string.scm 1502 */
 obj_t BgL_tmpz00_11419;
if(
INTEGERP(BgL_iz00_3780))
{ /* Ieee/string.scm 1502 */
BgL_tmpz00_11419 = BgL_iz00_3780
; }  else 
{ 
 obj_t BgL_auxz00_11422;
BgL_auxz00_11422 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(60102L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iz00_3780); 
FAILURE(BgL_auxz00_11422,BFALSE,BFALSE);} 
BgL_kz00_3787 = 
(long)CINT(BgL_tmpz00_11419); } 
{ /* Ieee/string.scm 343 */
 long BgL_l2585z00_4543;
BgL_l2585z00_4543 = 
STRING_LENGTH(BgL_bz00_253); 
if(
BOUND_CHECK(BgL_kz00_3787, BgL_l2585z00_4543))
{ /* Ieee/string.scm 343 */
BgL_cbz00_2160 = 
STRING_REF(BgL_bz00_253, BgL_kz00_3787); }  else 
{ 
 obj_t BgL_auxz00_11431;
BgL_auxz00_11431 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_bz00_253, 
(int)(BgL_l2585z00_4543), 
(int)(BgL_kz00_3787)); 
FAILURE(BgL_auxz00_11431,BFALSE,BFALSE);} } } } } 
{ 

if(
isspace(BgL_caz00_2159))
{ /* Ieee/string.scm 1416 */
{ 
 obj_t BgL_tmpz00_11439;
{ /* Ieee/string.scm 1417 */
 obj_t BgL_aux3313z00_5274;
BgL_aux3313z00_5274 = BgL_iaz00_2156; 
if(
INTEGERP(BgL_aux3313z00_5274))
{ /* Ieee/string.scm 1417 */
BgL_tmpz00_11439 = BgL_aux3313z00_5274
; }  else 
{ 
 obj_t BgL_auxz00_11442;
BgL_auxz00_11442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(57453L), BGl_string3536z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3313z00_5274); 
FAILURE(BgL_auxz00_11442,BFALSE,BFALSE);} } 
BgL_iaz00_2156 = 
ADDFX(BgL_tmpz00_11439, 
BINT(1L)); } 
{ /* Ieee/string.scm 1418 */
 long BgL_iz00_3790;
BgL_iz00_3790 = 
(long)CINT(BgL_iaz00_2156); 
if(
(BgL_iz00_3790>=
STRING_LENGTH(BgL_az00_252)))
{ /* Ieee/string.scm 1500 */
BgL_caz00_2159 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 343 */
 long BgL_l2589z00_4547;
BgL_l2589z00_4547 = 
STRING_LENGTH(BgL_az00_252); 
if(
BOUND_CHECK(BgL_iz00_3790, BgL_l2589z00_4547))
{ /* Ieee/string.scm 343 */
BgL_caz00_2159 = 
STRING_REF(BgL_az00_252, BgL_iz00_3790); }  else 
{ 
 obj_t BgL_auxz00_11456;
BgL_auxz00_11456 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_az00_252, 
(int)(BgL_l2589z00_4547), 
(int)(BgL_iz00_3790)); 
FAILURE(BgL_auxz00_11456,BFALSE,BFALSE);} } } }  else 
{ /* Ieee/string.scm 1416 */BFALSE; } } 
{ 

if(
isspace(BgL_cbz00_2160))
{ /* Ieee/string.scm 1420 */
{ 
 obj_t BgL_tmpz00_11464;
{ /* Ieee/string.scm 1421 */
 obj_t BgL_aux3314z00_5275;
BgL_aux3314z00_5275 = BgL_ibz00_2157; 
if(
INTEGERP(BgL_aux3314z00_5275))
{ /* Ieee/string.scm 1421 */
BgL_tmpz00_11464 = BgL_aux3314z00_5275
; }  else 
{ 
 obj_t BgL_auxz00_11467;
BgL_auxz00_11467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(57566L), BGl_string3536z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3314z00_5275); 
FAILURE(BgL_auxz00_11467,BFALSE,BFALSE);} } 
BgL_ibz00_2157 = 
ADDFX(BgL_tmpz00_11464, 
BINT(1L)); } 
{ /* Ieee/string.scm 1422 */
 long BgL_iz00_3800;
BgL_iz00_3800 = 
(long)CINT(BgL_ibz00_2157); 
if(
(BgL_iz00_3800>=
STRING_LENGTH(BgL_bz00_253)))
{ /* Ieee/string.scm 1500 */
BgL_cbz00_2160 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 343 */
 long BgL_l2593z00_4551;
BgL_l2593z00_4551 = 
STRING_LENGTH(BgL_bz00_253); 
if(
BOUND_CHECK(BgL_iz00_3800, BgL_l2593z00_4551))
{ /* Ieee/string.scm 343 */
BgL_cbz00_2160 = 
STRING_REF(BgL_bz00_253, BgL_iz00_3800); }  else 
{ 
 obj_t BgL_auxz00_11481;
BgL_auxz00_11481 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_bz00_253, 
(int)(BgL_l2593z00_4551), 
(int)(BgL_iz00_3800)); 
FAILURE(BgL_auxz00_11481,BFALSE,BFALSE);} } } }  else 
{ /* Ieee/string.scm 1420 */BFALSE; } } 
{ /* Ieee/string.scm 1424 */
 bool_t BgL_test4576z00_11487;
if(
isdigit(BgL_caz00_2159))
{ /* Ieee/string.scm 1424 */
BgL_test4576z00_11487 = 
isdigit(BgL_cbz00_2160)
; }  else 
{ /* Ieee/string.scm 1424 */
BgL_test4576z00_11487 = ((bool_t)0)
; } 
if(BgL_test4576z00_11487)
{ /* Ieee/string.scm 1425 */
 bool_t BgL_test4578z00_11491;
if(
(BgL_caz00_2159==((unsigned char)'0')))
{ /* Ieee/string.scm 1425 */
BgL_test4578z00_11491 = 
(BgL_cbz00_2160==((unsigned char)'0'))
; }  else 
{ /* Ieee/string.scm 1425 */
BgL_test4578z00_11491 = ((bool_t)0)
; } 
if(BgL_test4578z00_11491)
{ /* Ieee/string.scm 1426 */
 long BgL_arg1879z00_2173; long BgL_arg1880z00_2174;
{ /* Ieee/string.scm 1426 */
 long BgL_za71za7_3814;
{ /* Ieee/string.scm 1426 */
 obj_t BgL_tmpz00_11495;
{ /* Ieee/string.scm 1426 */
 obj_t BgL_aux3315z00_5276;
BgL_aux3315z00_5276 = BgL_iaz00_2156; 
if(
INTEGERP(BgL_aux3315z00_5276))
{ /* Ieee/string.scm 1426 */
BgL_tmpz00_11495 = BgL_aux3315z00_5276
; }  else 
{ 
 obj_t BgL_auxz00_11498;
BgL_auxz00_11498 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(57728L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3315z00_5276); 
FAILURE(BgL_auxz00_11498,BFALSE,BFALSE);} } 
BgL_za71za7_3814 = 
(long)CINT(BgL_tmpz00_11495); } 
BgL_arg1879z00_2173 = 
(BgL_za71za7_3814+1L); } 
{ /* Ieee/string.scm 1426 */
 long BgL_za71za7_3815;
{ /* Ieee/string.scm 1426 */
 obj_t BgL_tmpz00_11504;
{ /* Ieee/string.scm 1426 */
 obj_t BgL_aux3316z00_5277;
BgL_aux3316z00_5277 = BgL_ibz00_2157; 
if(
INTEGERP(BgL_aux3316z00_5277))
{ /* Ieee/string.scm 1426 */
BgL_tmpz00_11504 = BgL_aux3316z00_5277
; }  else 
{ 
 obj_t BgL_auxz00_11507;
BgL_auxz00_11507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(57739L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3316z00_5277); 
FAILURE(BgL_auxz00_11507,BFALSE,BFALSE);} } 
BgL_za71za7_3815 = 
(long)CINT(BgL_tmpz00_11504); } 
BgL_arg1880z00_2174 = 
(BgL_za71za7_3815+1L); } 
{ 
 obj_t BgL_ibz00_11515; obj_t BgL_iaz00_11513;
BgL_iaz00_11513 = 
BINT(BgL_arg1879z00_2173); 
BgL_ibz00_11515 = 
BINT(BgL_arg1880z00_2174); 
BgL_ibz00_2157 = BgL_ibz00_11515; 
BgL_iaz00_2156 = BgL_iaz00_11513; 
goto BgL_zc3z04anonymousza31870ze3z87_2158;} }  else 
{ /* Ieee/string.scm 1427 */
 obj_t BgL_resultz00_2175;
{ /* Ieee/string.scm 1427 */
 bool_t BgL_test4582z00_11517;
if(
(BgL_caz00_2159==((unsigned char)'0')))
{ /* Ieee/string.scm 1427 */
BgL_test4582z00_11517 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1427 */
BgL_test4582z00_11517 = 
(BgL_cbz00_2160==((unsigned char)'0'))
; } 
if(BgL_test4582z00_11517)
{ /* Ieee/string.scm 1427 */
BgL_resultz00_2175 = 
BGl_comparezd2leftzd2zz__r4_strings_6_7z00(BgL_az00_252, BgL_iaz00_2156, BgL_bz00_253, BgL_ibz00_2157); }  else 
{ /* Ieee/string.scm 1427 */
BgL_resultz00_2175 = 
BGl_comparezd2rightzd2zz__r4_strings_6_7z00(BgL_az00_252, BgL_iaz00_2156, BgL_bz00_253, BgL_ibz00_2157); } } 
if(
INTEGERP(BgL_resultz00_2175))
{ /* Ieee/string.scm 1432 */
 long BgL_arg1882z00_2177; long BgL_arg1883z00_2178;
{ /* Ieee/string.scm 1432 */
 long BgL_za71za7_3820; long BgL_za72za7_3821;
{ /* Ieee/string.scm 1432 */
 obj_t BgL_tmpz00_11525;
{ /* Ieee/string.scm 1432 */
 obj_t BgL_aux3317z00_5278;
BgL_aux3317z00_5278 = BgL_iaz00_2156; 
if(
INTEGERP(BgL_aux3317z00_5278))
{ /* Ieee/string.scm 1432 */
BgL_tmpz00_11525 = BgL_aux3317z00_5278
; }  else 
{ 
 obj_t BgL_auxz00_11528;
BgL_auxz00_11528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(57925L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3317z00_5278); 
FAILURE(BgL_auxz00_11528,BFALSE,BFALSE);} } 
BgL_za71za7_3820 = 
(long)CINT(BgL_tmpz00_11525); } 
BgL_za72za7_3821 = 
(long)CINT(BgL_resultz00_2175); 
BgL_arg1882z00_2177 = 
(BgL_za71za7_3820+BgL_za72za7_3821); } 
{ /* Ieee/string.scm 1432 */
 long BgL_za71za7_3822; long BgL_za72za7_3823;
{ /* Ieee/string.scm 1432 */
 obj_t BgL_tmpz00_11535;
{ /* Ieee/string.scm 1432 */
 obj_t BgL_aux3318z00_5279;
BgL_aux3318z00_5279 = BgL_ibz00_2157; 
if(
INTEGERP(BgL_aux3318z00_5279))
{ /* Ieee/string.scm 1432 */
BgL_tmpz00_11535 = BgL_aux3318z00_5279
; }  else 
{ 
 obj_t BgL_auxz00_11538;
BgL_auxz00_11538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(57941L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3318z00_5279); 
FAILURE(BgL_auxz00_11538,BFALSE,BFALSE);} } 
BgL_za71za7_3822 = 
(long)CINT(BgL_tmpz00_11535); } 
BgL_za72za7_3823 = 
(long)CINT(BgL_resultz00_2175); 
BgL_arg1883z00_2178 = 
(BgL_za71za7_3822+BgL_za72za7_3823); } 
{ 
 obj_t BgL_ibz00_11547; obj_t BgL_iaz00_11545;
BgL_iaz00_11545 = 
BINT(BgL_arg1882z00_2177); 
BgL_ibz00_11547 = 
BINT(BgL_arg1883z00_2178); 
BgL_ibz00_2157 = BgL_ibz00_11547; 
BgL_iaz00_2156 = BgL_iaz00_11545; 
goto BgL_zc3z04anonymousza31870ze3z87_2158;} }  else 
{ /* Ieee/string.scm 1431 */
if(
CBOOL(BgL_resultz00_2175))
{ /* Ieee/string.scm 1433 */
return 
BINT(1L);}  else 
{ /* Ieee/string.scm 1433 */
return 
BINT(-1L);} } } }  else 
{ /* Ieee/string.scm 1437 */
 bool_t BgL_test4588z00_11553;
if(
(BgL_caz00_2159==((unsigned char)'\000')))
{ /* Ieee/string.scm 1437 */
BgL_test4588z00_11553 = 
(BgL_cbz00_2160==((unsigned char)'\000'))
; }  else 
{ /* Ieee/string.scm 1437 */
BgL_test4588z00_11553 = ((bool_t)0)
; } 
if(BgL_test4588z00_11553)
{ /* Ieee/string.scm 1437 */
return 
BINT(0L);}  else 
{ /* Ieee/string.scm 1439 */
 bool_t BgL_test4590z00_11558;
if(BgL_foldcasez00_254)
{ /* Ieee/string.scm 1439 */
BgL_caz00_2159 = 
toupper(BgL_caz00_2159); 
BgL_cbz00_2160 = 
toupper(BgL_cbz00_2160); 
BgL_test4590z00_11558 = ((bool_t)0); }  else 
{ /* Ieee/string.scm 1439 */
BgL_test4590z00_11558 = ((bool_t)0)
; } 
if(BgL_test4590z00_11558)
{ /* Ieee/string.scm 1439 */
return BGl_symbol3537z00zz__r4_strings_6_7z00;}  else 
{ /* Ieee/string.scm 1439 */
if(
(BgL_caz00_2159<BgL_cbz00_2160))
{ /* Ieee/string.scm 1444 */
return 
BINT(-1L);}  else 
{ /* Ieee/string.scm 1444 */
if(
(BgL_caz00_2159>BgL_cbz00_2160))
{ /* Ieee/string.scm 1446 */
return 
BINT(1L);}  else 
{ /* Ieee/string.scm 1449 */
 long BgL_arg1891z00_2188; long BgL_arg1892z00_2189;
{ /* Ieee/string.scm 1449 */
 long BgL_za71za7_3834;
{ /* Ieee/string.scm 1449 */
 obj_t BgL_tmpz00_11568;
{ /* Ieee/string.scm 1449 */
 obj_t BgL_aux3319z00_5280;
BgL_aux3319z00_5280 = BgL_iaz00_2156; 
if(
INTEGERP(BgL_aux3319z00_5280))
{ /* Ieee/string.scm 1449 */
BgL_tmpz00_11568 = BgL_aux3319z00_5280
; }  else 
{ 
 obj_t BgL_auxz00_11571;
BgL_auxz00_11571 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(58271L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3319z00_5280); 
FAILURE(BgL_auxz00_11571,BFALSE,BFALSE);} } 
BgL_za71za7_3834 = 
(long)CINT(BgL_tmpz00_11568); } 
BgL_arg1891z00_2188 = 
(BgL_za71za7_3834+1L); } 
{ /* Ieee/string.scm 1449 */
 long BgL_za71za7_3835;
{ /* Ieee/string.scm 1449 */
 obj_t BgL_tmpz00_11577;
{ /* Ieee/string.scm 1449 */
 obj_t BgL_aux3320z00_5281;
BgL_aux3320z00_5281 = BgL_ibz00_2157; 
if(
INTEGERP(BgL_aux3320z00_5281))
{ /* Ieee/string.scm 1449 */
BgL_tmpz00_11577 = BgL_aux3320z00_5281
; }  else 
{ 
 obj_t BgL_auxz00_11580;
BgL_auxz00_11580 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(58282L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_aux3320z00_5281); 
FAILURE(BgL_auxz00_11580,BFALSE,BFALSE);} } 
BgL_za71za7_3835 = 
(long)CINT(BgL_tmpz00_11577); } 
BgL_arg1892z00_2189 = 
(BgL_za71za7_3835+1L); } 
{ 
 obj_t BgL_ibz00_11588; obj_t BgL_iaz00_11586;
BgL_iaz00_11586 = 
BINT(BgL_arg1891z00_2188); 
BgL_ibz00_11588 = 
BINT(BgL_arg1892z00_2189); 
BgL_ibz00_2157 = BgL_ibz00_11588; 
BgL_iaz00_2156 = BgL_iaz00_11586; 
goto BgL_zc3z04anonymousza31870ze3z87_2158;} } } } } } } } } } 

}



/* compare-right */
obj_t BGl_comparezd2rightzd2zz__r4_strings_6_7z00(obj_t BgL_az00_257, obj_t BgL_iaz00_258, obj_t BgL_bz00_259, obj_t BgL_ibz00_260)
{
{ /* Ieee/string.scm 1454 */
{ 
 obj_t BgL_biasz00_2194; long BgL_iz00_2195;
BgL_biasz00_2194 = BUNSPEC; 
BgL_iz00_2195 = 0L; 
BgL_zc3z04anonymousza31893ze3z87_2196:
{ /* Ieee/string.scm 1457 */
 unsigned char BgL_caz00_2197; unsigned char BgL_cbz00_2198;
{ /* Ieee/string.scm 1457 */
 long BgL_arg1911z00_2216;
{ /* Ieee/string.scm 1457 */
 long BgL_za72za7_3837;
{ /* Ieee/string.scm 1457 */
 obj_t BgL_tmpz00_11590;
if(
INTEGERP(BgL_iaz00_258))
{ /* Ieee/string.scm 1457 */
BgL_tmpz00_11590 = BgL_iaz00_258
; }  else 
{ 
 obj_t BgL_auxz00_11593;
BgL_auxz00_11593 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(58633L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iaz00_258); 
FAILURE(BgL_auxz00_11593,BFALSE,BFALSE);} 
BgL_za72za7_3837 = 
(long)CINT(BgL_tmpz00_11590); } 
BgL_arg1911z00_2216 = 
(BgL_iz00_2195+BgL_za72za7_3837); } 
if(
(BgL_arg1911z00_2216>=
STRING_LENGTH(BgL_az00_257)))
{ /* Ieee/string.scm 1500 */
BgL_caz00_2197 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 343 */
 long BgL_l2597z00_4555;
BgL_l2597z00_4555 = 
STRING_LENGTH(BgL_az00_257); 
if(
BOUND_CHECK(BgL_arg1911z00_2216, BgL_l2597z00_4555))
{ /* Ieee/string.scm 343 */
BgL_caz00_2197 = 
STRING_REF(BgL_az00_257, BgL_arg1911z00_2216); }  else 
{ 
 obj_t BgL_auxz00_11606;
BgL_auxz00_11606 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_az00_257, 
(int)(BgL_l2597z00_4555), 
(int)(BgL_arg1911z00_2216)); 
FAILURE(BgL_auxz00_11606,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1458 */
 long BgL_arg1912z00_2217;
{ /* Ieee/string.scm 1458 */
 long BgL_za72za7_3846;
{ /* Ieee/string.scm 1458 */
 obj_t BgL_tmpz00_11612;
if(
INTEGERP(BgL_ibz00_260))
{ /* Ieee/string.scm 1458 */
BgL_tmpz00_11612 = BgL_ibz00_260
; }  else 
{ 
 obj_t BgL_auxz00_11615;
BgL_auxz00_11615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(58666L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_ibz00_260); 
FAILURE(BgL_auxz00_11615,BFALSE,BFALSE);} 
BgL_za72za7_3846 = 
(long)CINT(BgL_tmpz00_11612); } 
BgL_arg1912z00_2217 = 
(BgL_iz00_2195+BgL_za72za7_3846); } 
if(
(BgL_arg1912z00_2217>=
STRING_LENGTH(BgL_bz00_259)))
{ /* Ieee/string.scm 1500 */
BgL_cbz00_2198 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 343 */
 long BgL_l2601z00_4559;
BgL_l2601z00_4559 = 
STRING_LENGTH(BgL_bz00_259); 
if(
BOUND_CHECK(BgL_arg1912z00_2217, BgL_l2601z00_4559))
{ /* Ieee/string.scm 343 */
BgL_cbz00_2198 = 
STRING_REF(BgL_bz00_259, BgL_arg1912z00_2217); }  else 
{ 
 obj_t BgL_auxz00_11628;
BgL_auxz00_11628 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_bz00_259, 
(int)(BgL_l2601z00_4559), 
(int)(BgL_arg1912z00_2217)); 
FAILURE(BgL_auxz00_11628,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1460 */
 bool_t BgL_test4602z00_11634;
if(
isdigit(BgL_caz00_2197))
{ /* Ieee/string.scm 1460 */
BgL_test4602z00_11634 = ((bool_t)0)
; }  else 
{ /* Ieee/string.scm 1460 */
if(
isdigit(BgL_cbz00_2198))
{ /* Ieee/string.scm 1460 */
BgL_test4602z00_11634 = ((bool_t)0)
; }  else 
{ /* Ieee/string.scm 1460 */
BgL_test4602z00_11634 = ((bool_t)1)
; } } 
if(BgL_test4602z00_11634)
{ /* Ieee/string.scm 1460 */
if(
(BgL_biasz00_2194==BUNSPEC))
{ /* Ieee/string.scm 1461 */
return 
BINT(BgL_iz00_2195);}  else 
{ /* Ieee/string.scm 1461 */
return BgL_biasz00_2194;} }  else 
{ /* Ieee/string.scm 1460 */
if(
isdigit(BgL_caz00_2197))
{ /* Ieee/string.scm 1462 */
if(
isdigit(BgL_cbz00_2198))
{ /* Ieee/string.scm 1464 */
if(
(BgL_caz00_2197<BgL_cbz00_2198))
{ 
 long BgL_iz00_11651; obj_t BgL_biasz00_11648;
if(
(BgL_biasz00_2194==BUNSPEC))
{ /* Ieee/string.scm 1467 */
BgL_biasz00_11648 = BFALSE; }  else 
{ /* Ieee/string.scm 1467 */
BgL_biasz00_11648 = BgL_biasz00_2194; } 
BgL_iz00_11651 = 
(BgL_iz00_2195+1L); 
BgL_iz00_2195 = BgL_iz00_11651; 
BgL_biasz00_2194 = BgL_biasz00_11648; 
goto BgL_zc3z04anonymousza31893ze3z87_2196;}  else 
{ /* Ieee/string.scm 1466 */
if(
(BgL_caz00_2197>BgL_cbz00_2198))
{ 
 long BgL_iz00_11658; obj_t BgL_biasz00_11655;
if(
(BgL_biasz00_2194==BUNSPEC))
{ /* Ieee/string.scm 1469 */
BgL_biasz00_11655 = BTRUE; }  else 
{ /* Ieee/string.scm 1469 */
BgL_biasz00_11655 = BgL_biasz00_2194; } 
BgL_iz00_11658 = 
(BgL_iz00_2195+1L); 
BgL_iz00_2195 = BgL_iz00_11658; 
BgL_biasz00_2194 = BgL_biasz00_11655; 
goto BgL_zc3z04anonymousza31893ze3z87_2196;}  else 
{ /* Ieee/string.scm 1470 */
 bool_t BgL_test4612z00_11660;
if(
(BgL_caz00_2197==((unsigned char)'\000')))
{ /* Ieee/string.scm 1470 */
BgL_test4612z00_11660 = 
(BgL_cbz00_2198==((unsigned char)'\000'))
; }  else 
{ /* Ieee/string.scm 1470 */
BgL_test4612z00_11660 = ((bool_t)0)
; } 
if(BgL_test4612z00_11660)
{ /* Ieee/string.scm 1470 */
if(
(BgL_biasz00_2194==BUNSPEC))
{ /* Ieee/string.scm 1471 */
return 
BINT(BgL_iz00_2195);}  else 
{ /* Ieee/string.scm 1471 */
return BgL_biasz00_2194;} }  else 
{ 
 long BgL_iz00_11667;
BgL_iz00_11667 = 
(BgL_iz00_2195+1L); 
BgL_iz00_2195 = BgL_iz00_11667; 
goto BgL_zc3z04anonymousza31893ze3z87_2196;} } } }  else 
{ /* Ieee/string.scm 1464 */
return BTRUE;} }  else 
{ /* Ieee/string.scm 1462 */
return BFALSE;} } } } } } 

}



/* compare-left */
obj_t BGl_comparezd2leftzd2zz__r4_strings_6_7z00(obj_t BgL_az00_261, obj_t BgL_iaz00_262, obj_t BgL_bz00_263, obj_t BgL_ibz00_264)
{
{ /* Ieee/string.scm 1478 */
{ 
 long BgL_iz00_2220;
BgL_iz00_2220 = 0L; 
BgL_zc3z04anonymousza31913ze3z87_2221:
{ /* Ieee/string.scm 1480 */
 unsigned char BgL_caz00_2222; unsigned char BgL_cbz00_2223;
{ /* Ieee/string.scm 1480 */
 long BgL_arg1924z00_2234;
{ /* Ieee/string.scm 1480 */
 long BgL_za71za7_3869;
{ /* Ieee/string.scm 1480 */
 obj_t BgL_tmpz00_11669;
if(
INTEGERP(BgL_iaz00_262))
{ /* Ieee/string.scm 1480 */
BgL_tmpz00_11669 = BgL_iaz00_262
; }  else 
{ 
 obj_t BgL_auxz00_11672;
BgL_auxz00_11672 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(59476L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_iaz00_262); 
FAILURE(BgL_auxz00_11672,BFALSE,BFALSE);} 
BgL_za71za7_3869 = 
(long)CINT(BgL_tmpz00_11669); } 
BgL_arg1924z00_2234 = 
(BgL_za71za7_3869+BgL_iz00_2220); } 
if(
(BgL_arg1924z00_2234>=
STRING_LENGTH(BgL_az00_261)))
{ /* Ieee/string.scm 1500 */
BgL_caz00_2222 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 343 */
 long BgL_l2605z00_4563;
BgL_l2605z00_4563 = 
STRING_LENGTH(BgL_az00_261); 
if(
BOUND_CHECK(BgL_arg1924z00_2234, BgL_l2605z00_4563))
{ /* Ieee/string.scm 343 */
BgL_caz00_2222 = 
STRING_REF(BgL_az00_261, BgL_arg1924z00_2234); }  else 
{ 
 obj_t BgL_auxz00_11685;
BgL_auxz00_11685 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_az00_261, 
(int)(BgL_l2605z00_4563), 
(int)(BgL_arg1924z00_2234)); 
FAILURE(BgL_auxz00_11685,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1481 */
 long BgL_arg1925z00_2235;
{ /* Ieee/string.scm 1481 */
 long BgL_za71za7_3878;
{ /* Ieee/string.scm 1481 */
 obj_t BgL_tmpz00_11691;
if(
INTEGERP(BgL_ibz00_264))
{ /* Ieee/string.scm 1481 */
BgL_tmpz00_11691 = BgL_ibz00_264
; }  else 
{ 
 obj_t BgL_auxz00_11694;
BgL_auxz00_11694 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(59509L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_ibz00_264); 
FAILURE(BgL_auxz00_11694,BFALSE,BFALSE);} 
BgL_za71za7_3878 = 
(long)CINT(BgL_tmpz00_11691); } 
BgL_arg1925z00_2235 = 
(BgL_za71za7_3878+BgL_iz00_2220); } 
if(
(BgL_arg1925z00_2235>=
STRING_LENGTH(BgL_bz00_263)))
{ /* Ieee/string.scm 1500 */
BgL_cbz00_2223 = ((unsigned char)'\000'); }  else 
{ /* Ieee/string.scm 343 */
 long BgL_l2609z00_4567;
BgL_l2609z00_4567 = 
STRING_LENGTH(BgL_bz00_263); 
if(
BOUND_CHECK(BgL_arg1925z00_2235, BgL_l2609z00_4567))
{ /* Ieee/string.scm 343 */
BgL_cbz00_2223 = 
STRING_REF(BgL_bz00_263, BgL_arg1925z00_2235); }  else 
{ 
 obj_t BgL_auxz00_11707;
BgL_auxz00_11707 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_bz00_263, 
(int)(BgL_l2609z00_4567), 
(int)(BgL_arg1925z00_2235)); 
FAILURE(BgL_auxz00_11707,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1483 */
 bool_t BgL_test4621z00_11713;
if(
isdigit(BgL_caz00_2222))
{ /* Ieee/string.scm 1483 */
BgL_test4621z00_11713 = ((bool_t)0)
; }  else 
{ /* Ieee/string.scm 1483 */
if(
isdigit(BgL_cbz00_2223))
{ /* Ieee/string.scm 1483 */
BgL_test4621z00_11713 = ((bool_t)0)
; }  else 
{ /* Ieee/string.scm 1483 */
BgL_test4621z00_11713 = ((bool_t)1)
; } } 
if(BgL_test4621z00_11713)
{ /* Ieee/string.scm 1483 */
return 
BINT(BgL_iz00_2220);}  else 
{ /* Ieee/string.scm 1483 */
if(
isdigit(BgL_caz00_2222))
{ /* Ieee/string.scm 1485 */
if(
isdigit(BgL_cbz00_2223))
{ /* Ieee/string.scm 1487 */
if(
(BgL_caz00_2222<BgL_cbz00_2223))
{ /* Ieee/string.scm 1489 */
return BFALSE;}  else 
{ /* Ieee/string.scm 1489 */
if(
(BgL_caz00_2222>BgL_cbz00_2223))
{ /* Ieee/string.scm 1491 */
return BTRUE;}  else 
{ 
 long BgL_iz00_11727;
BgL_iz00_11727 = 
(BgL_iz00_2220+1L); 
BgL_iz00_2220 = BgL_iz00_11727; 
goto BgL_zc3z04anonymousza31913ze3z87_2221;} } }  else 
{ /* Ieee/string.scm 1487 */
return BTRUE;} }  else 
{ /* Ieee/string.scm 1485 */
return BFALSE;} } } } } } 

}



/* hex-string-ref */
obj_t BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(obj_t BgL_strz00_267, long BgL_iz00_268)
{
{ /* Ieee/string.scm 1507 */
{ /* Ieee/string.scm 1508 */
 unsigned char BgL_nz00_2240;
{ /* Ieee/string.scm 343 */
 long BgL_l2613z00_4571;
BgL_l2613z00_4571 = 
STRING_LENGTH(BgL_strz00_267); 
if(
BOUND_CHECK(BgL_iz00_268, BgL_l2613z00_4571))
{ /* Ieee/string.scm 343 */
BgL_nz00_2240 = 
STRING_REF(BgL_strz00_267, BgL_iz00_268); }  else 
{ 
 obj_t BgL_auxz00_11733;
BgL_auxz00_11733 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16815L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_strz00_267, 
(int)(BgL_l2613z00_4571), 
(int)(BgL_iz00_268)); 
FAILURE(BgL_auxz00_11733,BFALSE,BFALSE);} } 
{ /* Ieee/string.scm 1510 */
 bool_t BgL_test4629z00_11739;
if(
(BgL_nz00_2240>=((unsigned char)'0')))
{ /* Ieee/string.scm 1510 */
BgL_test4629z00_11739 = 
(BgL_nz00_2240<=((unsigned char)'9'))
; }  else 
{ /* Ieee/string.scm 1510 */
BgL_test4629z00_11739 = ((bool_t)0)
; } 
if(BgL_test4629z00_11739)
{ /* Ieee/string.scm 1510 */
return 
BINT(
(
(BgL_nz00_2240)-48L));}  else 
{ /* Ieee/string.scm 1512 */
 bool_t BgL_test4631z00_11746;
if(
(BgL_nz00_2240>=((unsigned char)'a')))
{ /* Ieee/string.scm 1512 */
BgL_test4631z00_11746 = 
(BgL_nz00_2240<=((unsigned char)'f'))
; }  else 
{ /* Ieee/string.scm 1512 */
BgL_test4631z00_11746 = ((bool_t)0)
; } 
if(BgL_test4631z00_11746)
{ /* Ieee/string.scm 1512 */
return 
BINT(
(10L+
(
(BgL_nz00_2240)-97L)));}  else 
{ /* Ieee/string.scm 1514 */
 bool_t BgL_test4633z00_11754;
if(
(BgL_nz00_2240>=((unsigned char)'A')))
{ /* Ieee/string.scm 1514 */
BgL_test4633z00_11754 = 
(BgL_nz00_2240<=((unsigned char)'F'))
; }  else 
{ /* Ieee/string.scm 1514 */
BgL_test4633z00_11754 = ((bool_t)0)
; } 
if(BgL_test4633z00_11754)
{ /* Ieee/string.scm 1514 */
return 
BINT(
(10L+
(
(BgL_nz00_2240)-65L)));}  else 
{ /* Ieee/string.scm 1514 */
return 
BGl_errorz00zz__errorz00(BGl_string3539z00zz__r4_strings_6_7z00, BGl_string3540z00zz__r4_strings_6_7z00, BgL_strz00_267);} } } } } } 

}



/* string-hex-intern */
BGL_EXPORTED_DEF obj_t BGl_stringzd2hexzd2internz00zz__r4_strings_6_7z00(obj_t BgL_strz00_269)
{
{ /* Ieee/string.scm 1524 */
{ /* Ieee/string.scm 1525 */
 long BgL_lenz00_2255;
BgL_lenz00_2255 = 
STRING_LENGTH(BgL_strz00_269); 
if(
ODDP_FX(BgL_lenz00_2255))
{ /* Ieee/string.scm 1527 */
 obj_t BgL_aux3325z00_5286;
BgL_aux3325z00_5286 = 
BGl_errorz00zz__errorz00(BGl_string3541z00zz__r4_strings_6_7z00, BGl_string3542z00zz__r4_strings_6_7z00, BgL_strz00_269); 
if(
STRINGP(BgL_aux3325z00_5286))
{ /* Ieee/string.scm 1527 */
return BgL_aux3325z00_5286;}  else 
{ 
 obj_t BgL_auxz00_11769;
BgL_auxz00_11769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(61102L), BGl_string3541z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux3325z00_5286); 
FAILURE(BgL_auxz00_11769,BFALSE,BFALSE);} }  else 
{ /* Ieee/string.scm 1528 */
 obj_t BgL_resz00_2257;
{ /* Ieee/string.scm 1528 */
 long BgL_arg1948z00_2272;
BgL_arg1948z00_2272 = 
(BgL_lenz00_2255/2L); 
{ /* Ieee/string.scm 172 */

BgL_resz00_2257 = 
make_string(BgL_arg1948z00_2272, ((unsigned char)' ')); } } 
{ 
 long BgL_iz00_2259; long BgL_jz00_2260;
BgL_iz00_2259 = 0L; 
BgL_jz00_2260 = 0L; 
BgL_zc3z04anonymousza31941ze3z87_2261:
if(
(BgL_iz00_2259==BgL_lenz00_2255))
{ /* Ieee/string.scm 1531 */
return BgL_resz00_2257;}  else 
{ /* Ieee/string.scm 1533 */
 obj_t BgL_c1z00_2263;
BgL_c1z00_2263 = 
BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(BgL_strz00_269, BgL_iz00_2259); 
{ /* Ieee/string.scm 1533 */
 obj_t BgL_c2z00_2264;
BgL_c2z00_2264 = 
BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(BgL_strz00_269, 
(BgL_iz00_2259+1L)); 
{ /* Ieee/string.scm 1534 */
 long BgL_cz00_2265;
{ /* Ieee/string.scm 1535 */
 long BgL_za72za7_3934;
{ /* Ieee/string.scm 1535 */
 obj_t BgL_tmpz00_11780;
if(
INTEGERP(BgL_c2z00_2264))
{ /* Ieee/string.scm 1535 */
BgL_tmpz00_11780 = BgL_c2z00_2264
; }  else 
{ 
 obj_t BgL_auxz00_11783;
BgL_auxz00_11783 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(61379L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_c2z00_2264); 
FAILURE(BgL_auxz00_11783,BFALSE,BFALSE);} 
BgL_za72za7_3934 = 
(long)CINT(BgL_tmpz00_11780); } 
{ /* Ieee/string.scm 1535 */
 long BgL_tmpz00_11788;
{ /* Ieee/string.scm 1535 */
 long BgL_za71za7_3932;
{ /* Ieee/string.scm 1535 */
 obj_t BgL_tmpz00_11789;
if(
INTEGERP(BgL_c1z00_2263))
{ /* Ieee/string.scm 1535 */
BgL_tmpz00_11789 = BgL_c1z00_2263
; }  else 
{ 
 obj_t BgL_auxz00_11792;
BgL_auxz00_11792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(61372L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_c1z00_2263); 
FAILURE(BgL_auxz00_11792,BFALSE,BFALSE);} 
BgL_za71za7_3932 = 
(long)CINT(BgL_tmpz00_11789); } 
BgL_tmpz00_11788 = 
(BgL_za71za7_3932*16L); } 
BgL_cz00_2265 = 
(BgL_tmpz00_11788+BgL_za72za7_3934); } } 
{ /* Ieee/string.scm 1535 */

{ /* Ieee/string.scm 1536 */
 unsigned char BgL_arg1943z00_2266;
BgL_arg1943z00_2266 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cz00_2265); 
{ /* Ieee/string.scm 337 */
 long BgL_l2617z00_4575;
BgL_l2617z00_4575 = 
STRING_LENGTH(BgL_resz00_2257); 
if(
BOUND_CHECK(BgL_jz00_2260, BgL_l2617z00_4575))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_resz00_2257, BgL_jz00_2260, BgL_arg1943z00_2266); }  else 
{ 
 obj_t BgL_auxz00_11804;
BgL_auxz00_11804 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_resz00_2257, 
(int)(BgL_l2617z00_4575), 
(int)(BgL_jz00_2260)); 
FAILURE(BgL_auxz00_11804,BFALSE,BFALSE);} } } 
{ 
 long BgL_jz00_11812; long BgL_iz00_11810;
BgL_iz00_11810 = 
(BgL_iz00_2259+2L); 
BgL_jz00_11812 = 
(BgL_jz00_2260+1L); 
BgL_jz00_2260 = BgL_jz00_11812; 
BgL_iz00_2259 = BgL_iz00_11810; 
goto BgL_zc3z04anonymousza31941ze3z87_2261;} } } } } } } } } 

}



/* &string-hex-intern */
obj_t BGl_z62stringzd2hexzd2internz62zz__r4_strings_6_7z00(obj_t BgL_envz00_4173, obj_t BgL_strz00_4174)
{
{ /* Ieee/string.scm 1524 */
{ /* Ieee/string.scm 1525 */
 obj_t BgL_auxz00_11814;
if(
STRINGP(BgL_strz00_4174))
{ /* Ieee/string.scm 1525 */
BgL_auxz00_11814 = BgL_strz00_4174
; }  else 
{ 
 obj_t BgL_auxz00_11817;
BgL_auxz00_11817 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(61043L), BGl_string3543z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4174); 
FAILURE(BgL_auxz00_11817,BFALSE,BFALSE);} 
return 
BGl_stringzd2hexzd2internz00zz__r4_strings_6_7z00(BgL_auxz00_11814);} } 

}



/* string-hex-intern! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(obj_t BgL_strz00_270)
{
{ /* Ieee/string.scm 1542 */
{ /* Ieee/string.scm 1543 */
 long BgL_lenz00_2275;
BgL_lenz00_2275 = 
STRING_LENGTH(BgL_strz00_270); 
if(
ODDP_FX(BgL_lenz00_2275))
{ /* Ieee/string.scm 1545 */
 obj_t BgL_aux3331z00_5292;
BgL_aux3331z00_5292 = 
BGl_errorz00zz__errorz00(BGl_string3544z00zz__r4_strings_6_7z00, BGl_string3542z00zz__r4_strings_6_7z00, BgL_strz00_270); 
if(
STRINGP(BgL_aux3331z00_5292))
{ /* Ieee/string.scm 1545 */
return BgL_aux3331z00_5292;}  else 
{ 
 obj_t BgL_auxz00_11828;
BgL_auxz00_11828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(61793L), BGl_string3544z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux3331z00_5292); 
FAILURE(BgL_auxz00_11828,BFALSE,BFALSE);} }  else 
{ 
 long BgL_iz00_2278; long BgL_jz00_2279;
BgL_iz00_2278 = 0L; 
BgL_jz00_2279 = 0L; 
BgL_zc3z04anonymousza31950ze3z87_2280:
if(
(BgL_iz00_2278==BgL_lenz00_2275))
{ /* Ieee/string.scm 756 */
 long BgL_tmpz00_11834;
BgL_tmpz00_11834 = 
(BgL_lenz00_2275/2L); 
return 
bgl_string_shrink(BgL_strz00_270, BgL_tmpz00_11834);}  else 
{ /* Ieee/string.scm 1550 */
 obj_t BgL_c1z00_2283;
BgL_c1z00_2283 = 
BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(BgL_strz00_270, BgL_iz00_2278); 
{ /* Ieee/string.scm 1550 */
 obj_t BgL_c2z00_2284;
BgL_c2z00_2284 = 
BGl_hexzd2stringzd2refz00zz__r4_strings_6_7z00(BgL_strz00_270, 
(BgL_iz00_2278+1L)); 
{ /* Ieee/string.scm 1551 */
 long BgL_cz00_2285;
{ /* Ieee/string.scm 1552 */
 long BgL_za72za7_3950;
{ /* Ieee/string.scm 1552 */
 obj_t BgL_tmpz00_11840;
if(
INTEGERP(BgL_c2z00_2284))
{ /* Ieee/string.scm 1552 */
BgL_tmpz00_11840 = BgL_c2z00_2284
; }  else 
{ 
 obj_t BgL_auxz00_11843;
BgL_auxz00_11843 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62051L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_c2z00_2284); 
FAILURE(BgL_auxz00_11843,BFALSE,BFALSE);} 
BgL_za72za7_3950 = 
(long)CINT(BgL_tmpz00_11840); } 
{ /* Ieee/string.scm 1552 */
 long BgL_tmpz00_11848;
{ /* Ieee/string.scm 1552 */
 long BgL_za71za7_3948;
{ /* Ieee/string.scm 1552 */
 obj_t BgL_tmpz00_11849;
if(
INTEGERP(BgL_c1z00_2283))
{ /* Ieee/string.scm 1552 */
BgL_tmpz00_11849 = BgL_c1z00_2283
; }  else 
{ 
 obj_t BgL_auxz00_11852;
BgL_auxz00_11852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62044L), BGl_string3410z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_c1z00_2283); 
FAILURE(BgL_auxz00_11852,BFALSE,BFALSE);} 
BgL_za71za7_3948 = 
(long)CINT(BgL_tmpz00_11849); } 
BgL_tmpz00_11848 = 
(BgL_za71za7_3948*16L); } 
BgL_cz00_2285 = 
(BgL_tmpz00_11848+BgL_za72za7_3950); } } 
{ /* Ieee/string.scm 1552 */

{ /* Ieee/string.scm 1553 */
 unsigned char BgL_arg1953z00_2286;
BgL_arg1953z00_2286 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cz00_2285); 
{ /* Ieee/string.scm 337 */
 long BgL_l2621z00_4579;
BgL_l2621z00_4579 = 
STRING_LENGTH(BgL_strz00_270); 
if(
BOUND_CHECK(BgL_jz00_2279, BgL_l2621z00_4579))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_strz00_270, BgL_jz00_2279, BgL_arg1953z00_2286); }  else 
{ 
 obj_t BgL_auxz00_11864;
BgL_auxz00_11864 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_strz00_270, 
(int)(BgL_l2621z00_4579), 
(int)(BgL_jz00_2279)); 
FAILURE(BgL_auxz00_11864,BFALSE,BFALSE);} } } 
{ 
 long BgL_jz00_11872; long BgL_iz00_11870;
BgL_iz00_11870 = 
(BgL_iz00_2278+2L); 
BgL_jz00_11872 = 
(BgL_jz00_2279+1L); 
BgL_jz00_2279 = BgL_jz00_11872; 
BgL_iz00_2278 = BgL_iz00_11870; 
goto BgL_zc3z04anonymousza31950ze3z87_2280;} } } } } } } } 

}



/* &string-hex-intern! */
obj_t BGl_z62stringzd2hexzd2internz12z70zz__r4_strings_6_7z00(obj_t BgL_envz00_4175, obj_t BgL_strz00_4176)
{
{ /* Ieee/string.scm 1542 */
{ /* Ieee/string.scm 1543 */
 obj_t BgL_auxz00_11874;
if(
STRINGP(BgL_strz00_4176))
{ /* Ieee/string.scm 1543 */
BgL_auxz00_11874 = BgL_strz00_4176
; }  else 
{ 
 obj_t BgL_auxz00_11877;
BgL_auxz00_11877 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(61734L), BGl_string3545z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_4176); 
FAILURE(BgL_auxz00_11877,BFALSE,BFALSE);} 
return 
BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(BgL_auxz00_11874);} } 

}



/* _string-hex-extern */
obj_t BGl__stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t BgL_env1206z00_275, obj_t BgL_opt1205z00_274)
{
{ /* Ieee/string.scm 1559 */
{ /* Ieee/string.scm 1559 */
 obj_t BgL_strz00_2292;
BgL_strz00_2292 = 
VECTOR_REF(BgL_opt1205z00_274,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1205z00_274)) { case 1L : 

{ /* Ieee/string.scm 1560 */
 long BgL_endz00_2296;
{ /* Ieee/string.scm 1560 */
 obj_t BgL_stringz00_3956;
if(
STRINGP(BgL_strz00_2292))
{ /* Ieee/string.scm 1560 */
BgL_stringz00_3956 = BgL_strz00_2292; }  else 
{ 
 obj_t BgL_auxz00_11885;
BgL_auxz00_11885 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62453L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_2292); 
FAILURE(BgL_auxz00_11885,BFALSE,BFALSE);} 
BgL_endz00_2296 = 
STRING_LENGTH(BgL_stringz00_3956); } 
{ /* Ieee/string.scm 1559 */

{ /* Ieee/string.scm 1559 */
 obj_t BgL_auxz00_11890;
if(
STRINGP(BgL_strz00_2292))
{ /* Ieee/string.scm 1559 */
BgL_auxz00_11890 = BgL_strz00_2292
; }  else 
{ 
 obj_t BgL_auxz00_11893;
BgL_auxz00_11893 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62363L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_2292); 
FAILURE(BgL_auxz00_11893,BFALSE,BFALSE);} 
return 
BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(BgL_auxz00_11890, 
(int)(0L), BgL_endz00_2296);} } } break;case 2L : 

{ /* Ieee/string.scm 1559 */
 obj_t BgL_startz00_2297;
BgL_startz00_2297 = 
VECTOR_REF(BgL_opt1205z00_274,1L); 
{ /* Ieee/string.scm 1560 */
 long BgL_endz00_2298;
{ /* Ieee/string.scm 1560 */
 obj_t BgL_stringz00_3957;
if(
STRINGP(BgL_strz00_2292))
{ /* Ieee/string.scm 1560 */
BgL_stringz00_3957 = BgL_strz00_2292; }  else 
{ 
 obj_t BgL_auxz00_11902;
BgL_auxz00_11902 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62453L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_2292); 
FAILURE(BgL_auxz00_11902,BFALSE,BFALSE);} 
BgL_endz00_2298 = 
STRING_LENGTH(BgL_stringz00_3957); } 
{ /* Ieee/string.scm 1559 */

{ /* Ieee/string.scm 1559 */
 int BgL_auxz00_11914; obj_t BgL_auxz00_11907;
{ /* Ieee/string.scm 1559 */
 obj_t BgL_tmpz00_11915;
if(
INTEGERP(BgL_startz00_2297))
{ /* Ieee/string.scm 1559 */
BgL_tmpz00_11915 = BgL_startz00_2297
; }  else 
{ 
 obj_t BgL_auxz00_11918;
BgL_auxz00_11918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62363L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_2297); 
FAILURE(BgL_auxz00_11918,BFALSE,BFALSE);} 
BgL_auxz00_11914 = 
CINT(BgL_tmpz00_11915); } 
if(
STRINGP(BgL_strz00_2292))
{ /* Ieee/string.scm 1559 */
BgL_auxz00_11907 = BgL_strz00_2292
; }  else 
{ 
 obj_t BgL_auxz00_11910;
BgL_auxz00_11910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62363L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_2292); 
FAILURE(BgL_auxz00_11910,BFALSE,BFALSE);} 
return 
BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(BgL_auxz00_11907, BgL_auxz00_11914, BgL_endz00_2298);} } } } break;case 3L : 

{ /* Ieee/string.scm 1559 */
 obj_t BgL_startz00_2299;
BgL_startz00_2299 = 
VECTOR_REF(BgL_opt1205z00_274,1L); 
{ /* Ieee/string.scm 1559 */
 obj_t BgL_endz00_2300;
BgL_endz00_2300 = 
VECTOR_REF(BgL_opt1205z00_274,2L); 
{ /* Ieee/string.scm 1559 */

{ /* Ieee/string.scm 1559 */
 long BgL_auxz00_11942; int BgL_auxz00_11933; obj_t BgL_auxz00_11926;
{ /* Ieee/string.scm 1559 */
 obj_t BgL_tmpz00_11943;
if(
INTEGERP(BgL_endz00_2300))
{ /* Ieee/string.scm 1559 */
BgL_tmpz00_11943 = BgL_endz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_11946;
BgL_auxz00_11946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62363L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_endz00_2300); 
FAILURE(BgL_auxz00_11946,BFALSE,BFALSE);} 
BgL_auxz00_11942 = 
(long)CINT(BgL_tmpz00_11943); } 
{ /* Ieee/string.scm 1559 */
 obj_t BgL_tmpz00_11934;
if(
INTEGERP(BgL_startz00_2299))
{ /* Ieee/string.scm 1559 */
BgL_tmpz00_11934 = BgL_startz00_2299
; }  else 
{ 
 obj_t BgL_auxz00_11937;
BgL_auxz00_11937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62363L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3361z00zz__r4_strings_6_7z00, BgL_startz00_2299); 
FAILURE(BgL_auxz00_11937,BFALSE,BFALSE);} 
BgL_auxz00_11933 = 
CINT(BgL_tmpz00_11934); } 
if(
STRINGP(BgL_strz00_2292))
{ /* Ieee/string.scm 1559 */
BgL_auxz00_11926 = BgL_strz00_2292
; }  else 
{ 
 obj_t BgL_auxz00_11929;
BgL_auxz00_11929 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62363L), BGl_string3549z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_strz00_2292); 
FAILURE(BgL_auxz00_11929,BFALSE,BFALSE);} 
return 
BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(BgL_auxz00_11926, BgL_auxz00_11933, BgL_auxz00_11942);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3546z00zz__r4_strings_6_7z00, BGl_string3548z00zz__r4_strings_6_7z00, 
BINT(
VECTOR_LENGTH(BgL_opt1205z00_274)));} } } } 

}



/* string-hex-extern */
BGL_EXPORTED_DEF obj_t BGl_stringzd2hexzd2externz00zz__r4_strings_6_7z00(obj_t BgL_strz00_271, int BgL_startz00_272, long BgL_endz00_273)
{
{ /* Ieee/string.scm 1559 */
{ /* Ieee/string.scm 1564 */
 long BgL_lenz00_2303;
BgL_lenz00_2303 = 
STRING_LENGTH(BgL_strz00_271); 
{ /* Ieee/string.scm 1566 */
 bool_t BgL_test4657z00_11958;
if(
(
(long)(BgL_startz00_272)<0L))
{ /* Ieee/string.scm 1566 */
BgL_test4657z00_11958 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1566 */
BgL_test4657z00_11958 = 
(
(long)(BgL_startz00_272)>BgL_lenz00_2303)
; } 
if(BgL_test4657z00_11958)
{ /* Ieee/string.scm 1568 */
 obj_t BgL_arg1961z00_2306; obj_t BgL_arg1962z00_2307;
{ /* Ieee/string.scm 1568 */
 obj_t BgL_arg1963z00_2308;
{ /* Ieee/fixnum.scm 1002 */

BgL_arg1963z00_2308 = 
BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_startz00_272), 10L); } 
BgL_arg1961z00_2306 = 
string_append(BGl_string3396z00zz__r4_strings_6_7z00, BgL_arg1963z00_2308); } 
{ /* Ieee/string.scm 1569 */
 obj_t BgL_list1965z00_2312;
{ /* Ieee/string.scm 1569 */
 obj_t BgL_arg1966z00_2313;
BgL_arg1966z00_2313 = 
MAKE_YOUNG_PAIR(BgL_strz00_271, BNIL); 
BgL_list1965z00_2312 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lenz00_2303), BgL_arg1966z00_2313); } 
BgL_arg1962z00_2307 = BgL_list1965z00_2312; } 
{ /* Ieee/string.scm 1567 */
 obj_t BgL_aux3350z00_5311;
BgL_aux3350z00_5311 = 
BGl_errorz00zz__errorz00(BGl_string3550z00zz__r4_strings_6_7z00, BgL_arg1961z00_2306, BgL_arg1962z00_2307); 
if(
STRINGP(BgL_aux3350z00_5311))
{ /* Ieee/string.scm 1567 */
return BgL_aux3350z00_5311;}  else 
{ 
 obj_t BgL_auxz00_11973;
BgL_auxz00_11973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62635L), BGl_string3550z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux3350z00_5311); 
FAILURE(BgL_auxz00_11973,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 1570 */
 bool_t BgL_test4660z00_11977;
if(
(BgL_endz00_273<
(long)(BgL_startz00_272)))
{ /* Ieee/string.scm 1570 */
BgL_test4660z00_11977 = ((bool_t)1)
; }  else 
{ /* Ieee/string.scm 1570 */
BgL_test4660z00_11977 = 
(BgL_endz00_273>BgL_lenz00_2303)
; } 
if(BgL_test4660z00_11977)
{ /* Ieee/string.scm 1572 */
 obj_t BgL_arg1969z00_2316; obj_t BgL_arg1970z00_2317;
{ /* Ieee/string.scm 1572 */
 obj_t BgL_arg1971z00_2318;
{ /* Ieee/fixnum.scm 1002 */

BgL_arg1971z00_2318 = 
BGl_fixnumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_endz00_273, 10L); } 
BgL_arg1969z00_2316 = 
string_append(BGl_string3397z00zz__r4_strings_6_7z00, BgL_arg1971z00_2318); } 
{ /* Ieee/string.scm 1573 */
 obj_t BgL_list1973z00_2322;
{ /* Ieee/string.scm 1573 */
 obj_t BgL_arg1974z00_2323;
BgL_arg1974z00_2323 = 
MAKE_YOUNG_PAIR(BgL_strz00_271, BNIL); 
BgL_list1973z00_2322 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lenz00_2303), BgL_arg1974z00_2323); } 
BgL_arg1970z00_2317 = BgL_list1973z00_2322; } 
{ /* Ieee/string.scm 1571 */
 obj_t BgL_aux3352z00_5313;
BgL_aux3352z00_5313 = 
BGl_errorz00zz__errorz00(BGl_string3550z00zz__r4_strings_6_7z00, BgL_arg1969z00_2316, BgL_arg1970z00_2317); 
if(
STRINGP(BgL_aux3352z00_5313))
{ /* Ieee/string.scm 1571 */
return BgL_aux3352z00_5313;}  else 
{ 
 obj_t BgL_auxz00_11990;
BgL_auxz00_11990 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(62810L), BGl_string3550z00zz__r4_strings_6_7z00, BGl_string3356z00zz__r4_strings_6_7z00, BgL_aux3352z00_5313); 
FAILURE(BgL_auxz00_11990,BFALSE,BFALSE);} } }  else 
{ /* Ieee/string.scm 1575 */
 obj_t BgL_resz00_2324;
{ /* Ieee/string.scm 1575 */
 long BgL_arg1983z00_2340;
BgL_arg1983z00_2340 = 
(2L*
(BgL_endz00_273-
(long)(BgL_startz00_272))); 
{ /* Ieee/string.scm 172 */

BgL_resz00_2324 = 
make_string(BgL_arg1983z00_2340, ((unsigned char)' ')); } } 
{ 
 int BgL_iz00_2326; long BgL_jz00_2327;
BgL_iz00_2326 = BgL_startz00_272; 
BgL_jz00_2327 = 0L; 
BgL_zc3z04anonymousza31975ze3z87_2328:
if(
(
(long)(BgL_iz00_2326)==BgL_endz00_273))
{ /* Ieee/string.scm 1578 */
return BgL_resz00_2324;}  else 
{ /* Ieee/string.scm 1580 */
 long BgL_nz00_2330;
{ /* Ieee/string.scm 1580 */
 unsigned char BgL_tmpz00_12001;
{ /* Ieee/string.scm 1580 */
 long BgL_kz00_3979;
BgL_kz00_3979 = 
(long)(BgL_iz00_2326); 
{ /* Ieee/string.scm 331 */
 long BgL_l2625z00_4583;
BgL_l2625z00_4583 = 
STRING_LENGTH(BgL_strz00_271); 
if(
BOUND_CHECK(BgL_kz00_3979, BgL_l2625z00_4583))
{ /* Ieee/string.scm 331 */
BgL_tmpz00_12001 = 
STRING_REF(BgL_strz00_271, BgL_kz00_3979)
; }  else 
{ 
 obj_t BgL_auxz00_12007;
BgL_auxz00_12007 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BgL_strz00_271, 
(int)(BgL_l2625z00_4583), 
(int)(BgL_kz00_3979)); 
FAILURE(BgL_auxz00_12007,BFALSE,BFALSE);} } } 
BgL_nz00_2330 = 
(BgL_tmpz00_12001); } 
{ /* Ieee/string.scm 1580 */
 long BgL_d0z00_2331;
{ /* Ieee/string.scm 1581 */
 long BgL_n1z00_3981; long BgL_n2z00_3982;
BgL_n1z00_3981 = BgL_nz00_2330; 
BgL_n2z00_3982 = 16L; 
{ /* Ieee/string.scm 1581 */
 bool_t BgL_test4665z00_12014;
{ /* Ieee/string.scm 1581 */
 long BgL_arg2101z00_3984;
BgL_arg2101z00_3984 = 
(((BgL_n1z00_3981) | (BgL_n2z00_3982)) & -2147483648); 
BgL_test4665z00_12014 = 
(BgL_arg2101z00_3984==0L); } 
if(BgL_test4665z00_12014)
{ /* Ieee/string.scm 1581 */
 int32_t BgL_arg2098z00_3985;
{ /* Ieee/string.scm 1581 */
 int32_t BgL_arg2099z00_3986; int32_t BgL_arg2100z00_3987;
BgL_arg2099z00_3986 = 
(int32_t)(BgL_n1z00_3981); 
BgL_arg2100z00_3987 = 
(int32_t)(BgL_n2z00_3982); 
BgL_arg2098z00_3985 = 
(BgL_arg2099z00_3986%BgL_arg2100z00_3987); } 
{ /* Ieee/string.scm 1581 */
 long BgL_arg2203z00_3992;
BgL_arg2203z00_3992 = 
(long)(BgL_arg2098z00_3985); 
BgL_d0z00_2331 = 
(long)(BgL_arg2203z00_3992); } }  else 
{ /* Ieee/string.scm 1581 */
BgL_d0z00_2331 = 
(BgL_n1z00_3981%BgL_n2z00_3982); } } } 
{ /* Ieee/string.scm 1581 */
 long BgL_d1z00_2332;
BgL_d1z00_2332 = 
(BgL_nz00_2330/16L); 
{ /* Ieee/string.scm 1582 */

{ /* Ieee/string.scm 1583 */
 unsigned char BgL_arg1977z00_2333;
if(
BOUND_CHECK(BgL_d1z00_2332, 16L))
{ /* Ieee/string.scm 331 */
BgL_arg1977z00_2333 = 
STRING_REF(BGl_string3551z00zz__r4_strings_6_7z00, BgL_d1z00_2332); }  else 
{ 
 obj_t BgL_auxz00_12027;
BgL_auxz00_12027 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BGl_string3551z00zz__r4_strings_6_7z00, 
(int)(16L), 
(int)(BgL_d1z00_2332)); 
FAILURE(BgL_auxz00_12027,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 337 */
 long BgL_l2633z00_4591;
BgL_l2633z00_4591 = 
STRING_LENGTH(BgL_resz00_2324); 
if(
BOUND_CHECK(BgL_jz00_2327, BgL_l2633z00_4591))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_resz00_2324, BgL_jz00_2327, BgL_arg1977z00_2333); }  else 
{ 
 obj_t BgL_auxz00_12037;
BgL_auxz00_12037 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_resz00_2324, 
(int)(BgL_l2633z00_4591), 
(int)(BgL_jz00_2327)); 
FAILURE(BgL_auxz00_12037,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1584 */
 long BgL_arg1978z00_2334; unsigned char BgL_arg1979z00_2335;
BgL_arg1978z00_2334 = 
(BgL_jz00_2327+1L); 
if(
BOUND_CHECK(BgL_d0z00_2331, 16L))
{ /* Ieee/string.scm 331 */
BgL_arg1979z00_2335 = 
STRING_REF(BGl_string3551z00zz__r4_strings_6_7z00, BgL_d0z00_2331); }  else 
{ 
 obj_t BgL_auxz00_12047;
BgL_auxz00_12047 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16225L), BGl_string3366z00zz__r4_strings_6_7z00, BGl_string3551z00zz__r4_strings_6_7z00, 
(int)(16L), 
(int)(BgL_d0z00_2331)); 
FAILURE(BgL_auxz00_12047,BFALSE,BFALSE);} 
{ /* Ieee/string.scm 337 */
 long BgL_l2641z00_4599;
BgL_l2641z00_4599 = 
STRING_LENGTH(BgL_resz00_2324); 
if(
BOUND_CHECK(BgL_arg1978z00_2334, BgL_l2641z00_4599))
{ /* Ieee/string.scm 337 */
STRING_SET(BgL_resz00_2324, BgL_arg1978z00_2334, BgL_arg1979z00_2335); }  else 
{ 
 obj_t BgL_auxz00_12057;
BgL_auxz00_12057 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3354z00zz__r4_strings_6_7z00, 
BINT(16519L), BGl_string3368z00zz__r4_strings_6_7z00, BgL_resz00_2324, 
(int)(BgL_l2641z00_4599), 
(int)(BgL_arg1978z00_2334)); 
FAILURE(BgL_auxz00_12057,BFALSE,BFALSE);} } } 
{ /* Ieee/string.scm 1585 */
 long BgL_arg1980z00_2336; long BgL_arg1981z00_2337;
BgL_arg1980z00_2336 = 
(
(long)(BgL_iz00_2326)+1L); 
BgL_arg1981z00_2337 = 
(BgL_jz00_2327+2L); 
{ 
 long BgL_jz00_12068; int BgL_iz00_12066;
BgL_iz00_12066 = 
(int)(BgL_arg1980z00_2336); 
BgL_jz00_12068 = BgL_arg1981z00_2337; 
BgL_jz00_2327 = BgL_jz00_12068; 
BgL_iz00_2326 = BgL_iz00_12066; 
goto BgL_zc3z04anonymousza31975ze3z87_2328;} } } } } } } } } } } } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_strings_6_7z00(void)
{
{ /* Ieee/string.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string3552z00zz__r4_strings_6_7z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string3552z00zz__r4_strings_6_7z00));} 

}

#ifdef __cplusplus
}
#endif
