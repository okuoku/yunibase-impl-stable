/*===========================================================================*/
/*   (Ieee/vector.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/vector.scm -indent -o objs/obj_s/Ieee/vector.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_VECTORS_6_8_TYPE_DEFINITIONS
#define BGL___R4_VECTORS_6_8_TYPE_DEFINITIONS
#endif // BGL___R4_VECTORS_6_8_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t, long, obj_t, obj_t, obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol1832z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1915z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1835z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl__vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_symbol1839z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1922z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1844z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1845z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1928z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1848z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list1818z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62sortz62zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62listzd2ze3vectorz53zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_symbol1931z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1850z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1901z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1772z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1936z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1855z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1904z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1858z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_vectorzd2shrinkz12zc0zz__r4_vectors_6_8z00(obj_t, long);
static obj_t BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_symbol1942z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1863z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_list1831z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1914z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1866z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1834z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1918z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1837z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1919z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1838z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
BGL_EXPORTED_DECL long BGl_vectorzd2lengthzd2zz__r4_vectors_6_8z00(obj_t);
static obj_t BGl_symbol1952z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1920z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1791z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, long, long);
static obj_t BGl_list1921z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1874z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1841z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1842z00zz__r4_vectors_6_8z00 = BUNSPEC;
extern obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list1924z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1843z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1925z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1877z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1926z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1927z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1847z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_vectorzd2map2z12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62vectorzd2setz12za2zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2tagzd2setz12z12zz__r4_vectors_6_8z00(obj_t, int);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_z62vectorzd2tagzd2setz12z70zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl__makezd2vectorzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_symbol1880z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62vectorzd2setzd2urz12z70zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list1930z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1883z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1933z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1852z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1934z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1886z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1853z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1935z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1854z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1889z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1938z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1857z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_vectorzd2refzd2urz00zz__r4_vectors_6_8z00(obj_t, long);
static obj_t BGl_list1939z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_cnstzd2initzd2zz__r4_vectors_6_8z00(void);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62vectorzd2mapz12za2zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list1940z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1941z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1860z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_vectors_6_8z00(void);
static obj_t BGl_symbol1894z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1861z00zz__r4_vectors_6_8z00 = BUNSPEC;
extern obj_t sort_vector(obj_t, obj_t);
static obj_t BGl_list1862z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1944z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1945z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1897z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__r4_vectors_6_8z00(void);
static obj_t BGl_list1946z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1865z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1947z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1899z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1948z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_vectorzf3zf3zz__r4_vectors_6_8z00(obj_t);
static obj_t BGl_list1949z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1868z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62vectorzf3z91zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_z62vectorz62zz__r4_vectors_6_8z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t, long);
static obj_t BGl_z62vectorzd2copyzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_list1950z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1951z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1871z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1872z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1954z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1873z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1876z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1879z00zz__r4_vectors_6_8z00 = BUNSPEC;
static bool_t BGl_vectorzd2forzd2each2z00zz__r4_vectors_6_8z00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_vectorzd2tagzd2zz__r4_vectors_6_8z00(obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2refzd2zz__r4_vectors_6_8z00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_makezd2vectorzd2zz__r4_vectors_6_8z00(long, obj_t);
static obj_t BGl_z62vectorzd2lengthzb0zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_list1882z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1885z00zz__r4_vectors_6_8z00 = BUNSPEC;
static bool_t BGl_vectorzd2forzd2eachNz00zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_list1888z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62vectorzd2appendzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31306ze3ze5zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_list1891z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1892z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1893z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_list1896z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62vectorzd2refzd2urz62zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2mapzd2zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62vectorzd2tagzb0zz__r4_vectors_6_8z00(obj_t, obj_t);
extern obj_t make_vector(long, obj_t);
static obj_t BGl_z62vectorzd2refzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
extern obj_t bgl_fill_vector(obj_t, long, long, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2setzd2urz12z12zz__r4_vectors_6_8z00(obj_t, long, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl__vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_z62vectorzd2mapzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2forzd2eachz00zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62vectorzd2forzd2eachz62zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31333ze3ze5zz__r4_vectors_6_8z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2setz12zc0zz__r4_vectors_6_8z00(obj_t, long, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorz00zz__r4_vectors_6_8z00(obj_t);
static obj_t BGl_z62vectorzd2shrinkz12za2zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1807z00zz__r4_vectors_6_8z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_vectorzd2mapz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62vectorzd2ze3listz53zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_symbol1819z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31254ze3ze5zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_z62vectorzd2copy3zb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_vectorzd2copyzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
static obj_t BGl_z62copyzd2vectorzb0zz__r4_vectors_6_8z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1902z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1821z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1823z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1905z00zz__r4_vectors_6_8z00 = BUNSPEC;
static obj_t BGl_symbol1829z00zz__r4_vectors_6_8z00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string1870z00zz__r4_vectors_6_8z00, BgL_bgl_string1870za700za7za7_1960za7, "_", 1 );
DEFINE_STRING( BGl_string1790z00zz__r4_vectors_6_8z00, BgL_bgl_string1790za700za7za7_1961za7, "&list->vector", 13 );
DEFINE_STRING( BGl_string1953z00zz__r4_vectors_6_8z00, BgL_bgl_string1953za700za7za7_1962za7, "list1315", 8 );
DEFINE_STRING( BGl_string1792z00zz__r4_vectors_6_8z00, BgL_bgl_string1792za700za7za7_1963za7, "vector-fill!", 12 );
DEFINE_STRING( BGl_string1955z00zz__r4_vectors_6_8z00, BgL_bgl_string1955za700za7za7_1964za7, "vector-for-each", 15 );
DEFINE_STRING( BGl_string1793z00zz__r4_vectors_6_8z00, BgL_bgl_string1793za700za7za7_1965za7, "wrong number of arguments: [2..4] expected, provided", 52 );
DEFINE_STRING( BGl_string1956z00zz__r4_vectors_6_8z00, BgL_bgl_string1956za700za7za7_1966za7, "&vector-for-each", 16 );
DEFINE_STRING( BGl_string1875z00zz__r4_vectors_6_8z00, BgL_bgl_string1875za700za7za7_1967za7, "test1605", 8 );
DEFINE_STRING( BGl_string1794z00zz__r4_vectors_6_8z00, BgL_bgl_string1794za700za7za7_1968za7, "_vector-fill!", 13 );
DEFINE_STRING( BGl_string1957z00zz__r4_vectors_6_8z00, BgL_bgl_string1957za700za7za7_1969za7, "&<@anonymous:1333>", 18 );
DEFINE_STRING( BGl_string1795z00zz__r4_vectors_6_8z00, BgL_bgl_string1795za700za7za7_1970za7, "wrong negative start", 20 );
DEFINE_STRING( BGl_string1958z00zz__r4_vectors_6_8z00, BgL_bgl_string1958za700za7za7_1971za7, "&vector-shrink!", 15 );
DEFINE_STRING( BGl_string1796z00zz__r4_vectors_6_8z00, BgL_bgl_string1796za700za7za7_1972za7, "end index too large", 19 );
DEFINE_STRING( BGl_string1959z00zz__r4_vectors_6_8z00, BgL_bgl_string1959za700za7za7_1973za7, "__r4_vectors_6_8", 16 );
DEFINE_STRING( BGl_string1878z00zz__r4_vectors_6_8z00, BgL_bgl_string1878za700za7za7_1974za7, "$vector-bound-check?", 20 );
DEFINE_STRING( BGl_string1797z00zz__r4_vectors_6_8z00, BgL_bgl_string1797za700za7za7_1975za7, "start index larger than end", 27 );
DEFINE_STRING( BGl_string1798z00zz__r4_vectors_6_8z00, BgL_bgl_string1798za700za7za7_1976za7, "&vector-tag", 11 );
DEFINE_STRING( BGl_string1799z00zz__r4_vectors_6_8z00, BgL_bgl_string1799za700za7za7_1977za7, "&vector-tag-set!", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2mapzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2mapza71978za7, va_generic_entry, BGl_z62vectorzd2mapzb0zz__r4_vectors_6_8z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2shrinkz12zd2envz12zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2shri1979z00, BGl_z62vectorzd2shrinkz12za2zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1881z00zz__r4_vectors_6_8z00, BgL_bgl_string1881za700za7za7_1980za7, "if", 2 );
DEFINE_STRING( BGl_string1884z00zz__r4_vectors_6_8z00, BgL_bgl_string1884za700za7za7_1981za7, "vref", 4 );
DEFINE_STRING( BGl_string1887z00zz__r4_vectors_6_8z00, BgL_bgl_string1887za700za7za7_1982za7, "failure", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_copyzd2vectorzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762copyza7d2vector1983z00, BGl_z62copyzd2vectorzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sortzd2envzd2zz__r4_vectors_6_8z00, BgL_bgl_za762sortza762za7za7__r41984z00, BGl_z62sortz62zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1890z00zz__r4_vectors_6_8z00, BgL_bgl_string1890za700za7za7_1985za7, "index-out-of-bounds-error", 25 );
DEFINE_STRING( BGl_string1895z00zz__r4_vectors_6_8z00, BgL_bgl_string1895za700za7za7_1986za7, "list1232", 8 );
DEFINE_STRING( BGl_string1898z00zz__r4_vectors_6_8z00, BgL_bgl_string1898za700za7za7_1987za7, "$cons", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2fillz12zd2envz12zz__r4_vectors_6_8z00, BgL_bgl__vectorza7d2fillza711988z00, opt_generic_entry, BGl__vectorzd2fillz12zc0zz__r4_vectors_6_8z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2appendzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2appe1989z00, va_generic_entry, BGl_z62vectorzd2appendzb0zz__r4_vectors_6_8z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2ze3listzd2envze3zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2za7e3l1990za7, BGl_z62vectorzd2ze3listz53zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2tagzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2tagza71991za7, BGl_z62vectorzd2tagzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2copy3zd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2copy1992z00, BGl_z62vectorzd2copy3zb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2envzd2zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza762za7za7__1993z00, va_generic_entry, BGl_z62vectorz62zz__r4_vectors_6_8z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2refzd2urzd2envzd2zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2refza71994za7, BGl_z62vectorzd2refzd2urz62zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2mapz12zd2envz12zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2mapza71995za7, va_generic_entry, BGl_z62vectorzd2mapz12za2zz__r4_vectors_6_8z00, BUNSPEC, -3 );
DEFINE_STRING( BGl_string1800z00zz__r4_vectors_6_8z00, BgL_bgl_string1800za700za7za7_1996za7, "&copy-vector", 12 );
DEFINE_STRING( BGl_string1801z00zz__r4_vectors_6_8z00, BgL_bgl_string1801za700za7za7_1997za7, "vector-copy3", 12 );
DEFINE_STRING( BGl_string1802z00zz__r4_vectors_6_8z00, BgL_bgl_string1802za700za7za7_1998za7, "vector-copy", 11 );
DEFINE_STRING( BGl_string1803z00zz__r4_vectors_6_8z00, BgL_bgl_string1803za700za7za7_1999za7, "Illegal indexes", 15 );
DEFINE_STRING( BGl_string1804z00zz__r4_vectors_6_8z00, BgL_bgl_string1804za700za7za7_2000za7, "&vector-copy3", 13 );
DEFINE_STRING( BGl_string1805z00zz__r4_vectors_6_8z00, BgL_bgl_string1805za700za7za7_2001za7, "Illegal argument", 16 );
DEFINE_STRING( BGl_string1806z00zz__r4_vectors_6_8z00, BgL_bgl_string1806za700za7za7_2002za7, "&vector-copy", 12 );
DEFINE_STRING( BGl_string1808z00zz__r4_vectors_6_8z00, BgL_bgl_string1808za700za7za7_2003za7, "vector-copy!", 12 );
DEFINE_STRING( BGl_string1809z00zz__r4_vectors_6_8z00, BgL_bgl_string1809za700za7za7_2004za7, "wrong number of arguments: [3..5] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2copyzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2copy2005z00, va_generic_entry, BGl_z62vectorzd2copyzb0zz__r4_vectors_6_8z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string1810z00zz__r4_vectors_6_8z00, BgL_bgl_string1810za700za7za7_2006za7, "_vector-copy!", 13 );
DEFINE_STRING( BGl_string1811z00zz__r4_vectors_6_8z00, BgL_bgl_string1811za700za7za7_2007za7, "&vector-append", 14 );
DEFINE_STRING( BGl_string1812z00zz__r4_vectors_6_8z00, BgL_bgl_string1812za700za7za7_2008za7, "sort", 4 );
DEFINE_STRING( BGl_string1813z00zz__r4_vectors_6_8z00, BgL_bgl_string1813za700za7za7_2009za7, "procedure", 9 );
DEFINE_STRING( BGl_string1814z00zz__r4_vectors_6_8z00, BgL_bgl_string1814za700za7za7_2010za7, "Object must be a list or a vector", 33 );
DEFINE_STRING( BGl_string1815z00zz__r4_vectors_6_8z00, BgL_bgl_string1815za700za7za7_2011za7, "inner-sort", 10 );
DEFINE_STRING( BGl_string1816z00zz__r4_vectors_6_8z00, BgL_bgl_string1816za700za7za7_2012za7, "vector-map2!", 12 );
DEFINE_STRING( BGl_string1817z00zz__r4_vectors_6_8z00, BgL_bgl_string1817za700za7za7_2013za7, "loop:Wrong number of arguments", 30 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3vectorzd2envze3zz__r4_vectors_6_8z00, BgL_bgl_za762listza7d2za7e3vec2014za7, BGl_z62listzd2ze3vectorz53zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2refzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2refza72015za7, BGl_z62vectorzd2refzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2lengthzd2envz00zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2leng2016z00, BGl_z62vectorzd2lengthzb0zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1900z00zz__r4_vectors_6_8z00, BgL_bgl_string1900za700za7za7_2017za7, "args", 4 );
DEFINE_STRING( BGl_string1820z00zz__r4_vectors_6_8z00, BgL_bgl_string1820za700za7za7_2018za7, "funcall", 7 );
DEFINE_STRING( BGl_string1903z00zz__r4_vectors_6_8z00, BgL_bgl_string1903za700za7za7_2019za7, "quote", 5 );
DEFINE_STRING( BGl_string1822z00zz__r4_vectors_6_8z00, BgL_bgl_string1822za700za7za7_2020za7, "proc", 4 );
DEFINE_STRING( BGl_string1824z00zz__r4_vectors_6_8z00, BgL_bgl_string1824za700za7za7_2021za7, "arg1226", 7 );
DEFINE_STRING( BGl_string1906z00zz__r4_vectors_6_8z00, BgL_bgl_string1906za700za7za7_2022za7, "cons*", 5 );
DEFINE_STRING( BGl_string1825z00zz__r4_vectors_6_8z00, BgL_bgl_string1825za700za7za7_2023za7, "vector-mapN!", 12 );
DEFINE_STRING( BGl_string1907z00zz__r4_vectors_6_8z00, BgL_bgl_string1907za700za7za7_2024za7, "vector-map", 10 );
DEFINE_STRING( BGl_string1826z00zz__r4_vectors_6_8z00, BgL_bgl_string1826za700za7za7_2025za7, "<@anonymous:1234>", 17 );
DEFINE_STRING( BGl_string1908z00zz__r4_vectors_6_8z00, BgL_bgl_string1908za700za7za7_2026za7, "Illegal arguments", 17 );
DEFINE_STRING( BGl_string1827z00zz__r4_vectors_6_8z00, BgL_bgl_string1827za700za7za7_2027za7, "map", 3 );
DEFINE_STRING( BGl_string1909z00zz__r4_vectors_6_8z00, BgL_bgl_string1909za700za7za7_2028za7, "&vector-map", 11 );
DEFINE_STRING( BGl_string1828z00zz__r4_vectors_6_8z00, BgL_bgl_string1828za700za7za7_2029za7, "list", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2tagzd2setz12zd2envzc0zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2tagza72030za7, BGl_z62vectorzd2tagzd2setz12z70zz__r4_vectors_6_8z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1910z00zz__r4_vectors_6_8z00, BgL_bgl_string1910za700za7za7_2031za7, "&<@anonymous:1254>", 18 );
DEFINE_STRING( BGl_string1911z00zz__r4_vectors_6_8z00, BgL_bgl_string1911za700za7za7_2032za7, "vector-map!", 11 );
DEFINE_STRING( BGl_string1830z00zz__r4_vectors_6_8z00, BgL_bgl_string1830za700za7za7_2033za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string1912z00zz__r4_vectors_6_8z00, BgL_bgl_string1912za700za7za7_2034za7, "&vector-map!", 12 );
DEFINE_STRING( BGl_string1913z00zz__r4_vectors_6_8z00, BgL_bgl_string1913za700za7za7_2035za7, "&<@anonymous:1306>", 18 );
DEFINE_STRING( BGl_string1833z00zz__r4_vectors_6_8z00, BgL_bgl_string1833za700za7za7_2036za7, "apply", 5 );
DEFINE_STRING( BGl_string1916z00zz__r4_vectors_6_8z00, BgL_bgl_string1916za700za7za7_2037za7, "arg1310", 7 );
DEFINE_STRING( BGl_string1917z00zz__r4_vectors_6_8z00, BgL_bgl_string1917za700za7za7_2038za7, "<@anonymous:1318>", 17 );
DEFINE_STRING( BGl_string1836z00zz__r4_vectors_6_8z00, BgL_bgl_string1836za700za7za7_2039za7, "let", 3 );
DEFINE_STRING( BGl_string1840z00zz__r4_vectors_6_8z00, BgL_bgl_string1840za700za7za7_2040za7, "arg1231", 7 );
DEFINE_STRING( BGl_string1923z00zz__r4_vectors_6_8z00, BgL_bgl_string1923za700za7za7_2041za7, "arg1314", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzf3zd2envz21zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7f3za791za72042z00, BGl_z62vectorzf3z91zz__r4_vectors_6_8z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1846z00zz__r4_vectors_6_8z00, BgL_bgl_string1846za700za7za7_2043za7, "vsrc", 4 );
DEFINE_STRING( BGl_string1929z00zz__r4_vectors_6_8z00, BgL_bgl_string1929za700za7za7_2044za7, "v1610", 5 );
DEFINE_STRING( BGl_string1849z00zz__r4_vectors_6_8z00, BgL_bgl_string1849za700za7za7_2045za7, "k", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2setzd2urz12zd2envzc0zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2setza72046za7, BGl_z62vectorzd2setzd2urz12z70zz__r4_vectors_6_8z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string1932z00zz__r4_vectors_6_8z00, BgL_bgl_string1932za700za7za7_2047za7, "i1611", 5 );
DEFINE_STRING( BGl_string1851z00zz__r4_vectors_6_8z00, BgL_bgl_string1851za700za7za7_2048za7, "i", 1 );
DEFINE_STRING( BGl_string1773z00zz__r4_vectors_6_8z00, BgL_bgl_string1773za700za7za7_2049za7, "make-vector", 11 );
DEFINE_STRING( BGl_string1774z00zz__r4_vectors_6_8z00, BgL_bgl_string1774za700za7za7_2050za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string1937z00zz__r4_vectors_6_8z00, BgL_bgl_string1937za700za7za7_2051za7, "l1612", 5 );
DEFINE_STRING( BGl_string1856z00zz__r4_vectors_6_8z00, BgL_bgl_string1856za700za7za7_2052za7, "v1602", 5 );
DEFINE_STRING( BGl_string1775z00zz__r4_vectors_6_8z00, BgL_bgl_string1775za700za7za7_2053za7, "/tmp/bigloo/runtime/Ieee/vector.scm", 35 );
DEFINE_STRING( BGl_string1776z00zz__r4_vectors_6_8z00, BgL_bgl_string1776za700za7za7_2054za7, "_make-vector", 12 );
DEFINE_STRING( BGl_string1777z00zz__r4_vectors_6_8z00, BgL_bgl_string1777za700za7za7_2055za7, "bint", 4 );
DEFINE_STRING( BGl_string1859z00zz__r4_vectors_6_8z00, BgL_bgl_string1859za700za7za7_2056za7, "i1603", 5 );
DEFINE_STRING( BGl_string1778z00zz__r4_vectors_6_8z00, BgL_bgl_string1778za700za7za7_2057za7, "vector", 6 );
DEFINE_STRING( BGl_string1779z00zz__r4_vectors_6_8z00, BgL_bgl_string1779za700za7za7_2058za7, "pair-nil", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2copyz12zd2envz12zz__r4_vectors_6_8z00, BgL_bgl__vectorza7d2copyza712059z00, opt_generic_entry, BGl__vectorzd2copyz12zc0zz__r4_vectors_6_8z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2vectorzd2envz00zz__r4_vectors_6_8z00, BgL_bgl__makeza7d2vectorza7d2060z00, opt_generic_entry, BGl__makezd2vectorzd2zz__r4_vectors_6_8z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2forzd2eachzd2envzd2zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2forza72061za7, va_generic_entry, BGl_z62vectorzd2forzd2eachz62zz__r4_vectors_6_8z00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2setz12zd2envz12zz__r4_vectors_6_8z00, BgL_bgl_za762vectorza7d2setza72062za7, BGl_z62vectorzd2setz12za2zz__r4_vectors_6_8z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string1780z00zz__r4_vectors_6_8z00, BgL_bgl_string1780za700za7za7_2063za7, "&vector-length", 14 );
DEFINE_STRING( BGl_string1943z00zz__r4_vectors_6_8z00, BgL_bgl_string1943za700za7za7_2064za7, "test1613", 8 );
DEFINE_STRING( BGl_string1781z00zz__r4_vectors_6_8z00, BgL_bgl_string1781za700za7za7_2065za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string1782z00zz__r4_vectors_6_8z00, BgL_bgl_string1782za700za7za7_2066za7, "&vector-ref", 11 );
DEFINE_STRING( BGl_string1864z00zz__r4_vectors_6_8z00, BgL_bgl_string1864za700za7za7_2067za7, "l1604", 5 );
DEFINE_STRING( BGl_string1783z00zz__r4_vectors_6_8z00, BgL_bgl_string1783za700za7za7_2068za7, "vector-set!", 11 );
DEFINE_STRING( BGl_string1784z00zz__r4_vectors_6_8z00, BgL_bgl_string1784za700za7za7_2069za7, "&vector-set!", 12 );
DEFINE_STRING( BGl_string1785z00zz__r4_vectors_6_8z00, BgL_bgl_string1785za700za7za7_2070za7, "&vector-ref-ur", 14 );
DEFINE_STRING( BGl_string1867z00zz__r4_vectors_6_8z00, BgL_bgl_string1867za700za7za7_2071za7, "vlength", 7 );
DEFINE_STRING( BGl_string1786z00zz__r4_vectors_6_8z00, BgL_bgl_string1786za700za7za7_2072za7, "&vector-set-ur!", 15 );
DEFINE_STRING( BGl_string1787z00zz__r4_vectors_6_8z00, BgL_bgl_string1787za700za7za7_2073za7, "&vector->list", 13 );
DEFINE_STRING( BGl_string1869z00zz__r4_vectors_6_8z00, BgL_bgl_string1869za700za7za7_2074za7, "ftype:", 6 );
DEFINE_STRING( BGl_string1788z00zz__r4_vectors_6_8z00, BgL_bgl_string1788za700za7za7_2075za7, "loop", 4 );
DEFINE_STRING( BGl_string1789z00zz__r4_vectors_6_8z00, BgL_bgl_string1789za700za7za7_2076za7, "pair", 4 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1832z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1915z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1835z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1839z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1922z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1844z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1845z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1928z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1848z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1818z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1931z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1850z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1901z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1772z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1936z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1855z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1904z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1858z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1942z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1863z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1831z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1914z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1866z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1834z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1918z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1837z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1919z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1838z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1952z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1920z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1791z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1921z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1874z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1841z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1842z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1924z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1843z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1925z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1877z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1926z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1927z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1847z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1880z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1930z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1883z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1933z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1852z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1934z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1886z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1853z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1935z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1854z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1889z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1938z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1857z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1939z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1940z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1941z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1860z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1894z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1861z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1862z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1944z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1945z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1897z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1946z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1865z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1947z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1899z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1948z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1949z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1868z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1950z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1951z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1871z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1872z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1954z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1873z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1876z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1879z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1882z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1885z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1888z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1891z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1892z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1893z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_list1896z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1807z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1819z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1902z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1821z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1823z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1905z00zz__r4_vectors_6_8z00) );
ADD_ROOT( (void *)(&BGl_symbol1829z00zz__r4_vectors_6_8z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long BgL_checksumz00_1842, char * BgL_fromz00_1843)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_vectors_6_8z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_vectors_6_8z00(); 
BGl_cnstzd2initzd2zz__r4_vectors_6_8z00(); 
return 
BGl_importedzd2moduleszd2initz00zz__r4_vectors_6_8z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_vectors_6_8z00(void)
{
{ /* Ieee/vector.scm 18 */
BGl_symbol1772z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1773z00zz__r4_vectors_6_8z00); 
BGl_symbol1791z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1792z00zz__r4_vectors_6_8z00); 
BGl_symbol1807z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1808z00zz__r4_vectors_6_8z00); 
BGl_symbol1819z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1820z00zz__r4_vectors_6_8z00); 
BGl_symbol1821z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1822z00zz__r4_vectors_6_8z00); 
BGl_symbol1823z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1824z00zz__r4_vectors_6_8z00); 
BGl_list1818z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1819z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1823z00zz__r4_vectors_6_8z00, BNIL)))); 
BGl_symbol1829z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1788z00zz__r4_vectors_6_8z00); 
BGl_symbol1832z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1833z00zz__r4_vectors_6_8z00); 
BGl_symbol1835z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1836z00zz__r4_vectors_6_8z00); 
BGl_symbol1839z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1840z00zz__r4_vectors_6_8z00); 
BGl_symbol1844z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1778z00zz__r4_vectors_6_8z00); 
BGl_symbol1845z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1846z00zz__r4_vectors_6_8z00); 
BGl_list1843z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_symbol1848z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1849z00zz__r4_vectors_6_8z00); 
BGl_symbol1850z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1851z00zz__r4_vectors_6_8z00); 
BGl_list1847z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1848z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1850z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1842z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1843z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1847z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_symbol1855z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1856z00zz__r4_vectors_6_8z00); 
BGl_list1854z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1855z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_symbol1858z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1859z00zz__r4_vectors_6_8z00); 
BGl_list1857z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1848z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1853z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1854z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1857z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_symbol1863z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1864z00zz__r4_vectors_6_8z00); 
BGl_symbol1866z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1867z00zz__r4_vectors_6_8z00); 
BGl_list1868z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_string1869z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_string1870z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1865z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1866z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1868z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1855z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1862z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1863z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1865z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1861z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1862z00zz__r4_vectors_6_8z00, BNIL); 
BGl_symbol1874z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1875z00zz__r4_vectors_6_8z00); 
BGl_symbol1877z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1878z00zz__r4_vectors_6_8z00); 
BGl_list1876z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1877z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1863z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1873z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1874z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1876z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1872z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1873z00zz__r4_vectors_6_8z00, BNIL); 
BGl_symbol1880z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1881z00zz__r4_vectors_6_8z00); 
BGl_symbol1883z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1884z00zz__r4_vectors_6_8z00); 
BGl_list1882z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1883z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1855z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_symbol1886z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1887z00zz__r4_vectors_6_8z00); 
BGl_symbol1889z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1890z00zz__r4_vectors_6_8z00); 
BGl_list1888z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1889z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_string1775z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(
BINT(7562L), 
MAKE_YOUNG_PAIR(BGl_string1781z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1855z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1863z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r4_vectors_6_8z00, BNIL))))))); 
BGl_list1885z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1886z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1888z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(
BBOOL(((bool_t)0)), 
MAKE_YOUNG_PAIR(
BBOOL(((bool_t)0)), BNIL)))); 
BGl_list1879z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1880z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1874z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1882z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1885z00zz__r4_vectors_6_8z00, BNIL)))); 
BGl_list1871z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1872z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1879z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1860z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1861z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1871z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1852z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1853z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1860z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1841z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1842z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1852z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1838z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1839z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1841z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1837z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1838z00zz__r4_vectors_6_8z00, BNIL); 
BGl_symbol1894z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1895z00zz__r4_vectors_6_8z00); 
BGl_symbol1897z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1898z00zz__r4_vectors_6_8z00); 
BGl_symbol1899z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1900z00zz__r4_vectors_6_8z00); 
BGl_symbol1902z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1903z00zz__r4_vectors_6_8z00); 
BGl_list1901z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1902z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BNIL, BNIL)); 
BGl_list1896z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1897z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1899z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1901z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1893z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1894z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1896z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1892z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1893z00zz__r4_vectors_6_8z00, BNIL); 
BGl_symbol1905z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1906z00zz__r4_vectors_6_8z00); 
BGl_list1904z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1905z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1839z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1894z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1891z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1892z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1904z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1834z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1837z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1891z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1831z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1832z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1834z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_symbol1915z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1916z00zz__r4_vectors_6_8z00); 
BGl_list1914z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1819z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1915z00zz__r4_vectors_6_8z00, BNIL)))); 
BGl_symbol1922z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1923z00zz__r4_vectors_6_8z00); 
BGl_symbol1928z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1929z00zz__r4_vectors_6_8z00); 
BGl_list1927z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1928z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_symbol1931z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1932z00zz__r4_vectors_6_8z00); 
BGl_list1930z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1931z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1848z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1926z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1927z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1930z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_symbol1936z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1937z00zz__r4_vectors_6_8z00); 
BGl_list1938z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1866z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1868z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1928z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1935z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1936z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1938z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1934z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1935z00zz__r4_vectors_6_8z00, BNIL); 
BGl_symbol1942z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1943z00zz__r4_vectors_6_8z00); 
BGl_list1944z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1877z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1931z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1936z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1941z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1942z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1944z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1940z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1941z00zz__r4_vectors_6_8z00, BNIL); 
BGl_list1946z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1883z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1928z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1931z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1948z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1889z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_string1775z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(
BINT(7562L), 
MAKE_YOUNG_PAIR(BGl_string1781z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1928z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1936z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1931z00zz__r4_vectors_6_8z00, BNIL))))))); 
BGl_list1947z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1886z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1948z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(
BBOOL(((bool_t)0)), 
MAKE_YOUNG_PAIR(
BBOOL(((bool_t)0)), BNIL)))); 
BGl_list1945z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1880z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1942z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1946z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1947z00zz__r4_vectors_6_8z00, BNIL)))); 
BGl_list1939z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1940z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1945z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1933z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1934z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1939z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1925z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1926z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1933z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1924z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1842z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1925z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1921z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1922z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1924z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1920z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1921z00zz__r4_vectors_6_8z00, BNIL); 
BGl_symbol1952z00zz__r4_vectors_6_8z00 = 
bstring_to_symbol(BGl_string1953z00zz__r4_vectors_6_8z00); 
BGl_list1951z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1952z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1896z00zz__r4_vectors_6_8z00, BNIL)); 
BGl_list1950z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_list1951z00zz__r4_vectors_6_8z00, BNIL); 
BGl_list1954z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1905z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1922z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1952z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1949z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1950z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1954z00zz__r4_vectors_6_8z00, BNIL))); 
BGl_list1919z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1920z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1949z00zz__r4_vectors_6_8z00, BNIL))); 
return ( 
BGl_list1918z00zz__r4_vectors_6_8z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1832z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r4_vectors_6_8z00, 
MAKE_YOUNG_PAIR(BGl_list1919z00zz__r4_vectors_6_8z00, BNIL))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_vectors_6_8z00(void)
{
{ /* Ieee/vector.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* vector? */
BGL_EXPORTED_DEF bool_t BGl_vectorzf3zf3zz__r4_vectors_6_8z00(obj_t BgL_objz00_3)
{
{ /* Ieee/vector.scm 136 */
return 
VECTORP(BgL_objz00_3);} 

}



/* &vector? */
obj_t BGl_z62vectorzf3z91zz__r4_vectors_6_8z00(obj_t BgL_envz00_1537, obj_t BgL_objz00_1538)
{
{ /* Ieee/vector.scm 136 */
return 
BBOOL(
BGl_vectorzf3zf3zz__r4_vectors_6_8z00(BgL_objz00_1538));} 

}



/* _make-vector */
obj_t BGl__makezd2vectorzd2zz__r4_vectors_6_8z00(obj_t BgL_env1049z00_7, obj_t BgL_opt1048z00_6)
{
{ /* Ieee/vector.scm 142 */
{ /* Ieee/vector.scm 142 */
 obj_t BgL_g1050z00_1836;
BgL_g1050z00_1836 = 
VECTOR_REF(BgL_opt1048z00_6,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1048z00_6)) { case 1L : 

{ /* Ieee/vector.scm 142 */

{ /* Ieee/vector.scm 142 */
 long BgL_intz00_1838;
{ /* Ieee/vector.scm 142 */
 obj_t BgL_tmpz00_2050;
if(
INTEGERP(BgL_g1050z00_1836))
{ /* Ieee/vector.scm 142 */
BgL_tmpz00_2050 = BgL_g1050z00_1836
; }  else 
{ 
 obj_t BgL_auxz00_2053;
BgL_auxz00_2053 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(6640L), BGl_string1776z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_g1050z00_1836); 
FAILURE(BgL_auxz00_2053,BFALSE,BFALSE);} 
BgL_intz00_1838 = 
(long)CINT(BgL_tmpz00_2050); } 
return 
make_vector(BgL_intz00_1838, BUNSPEC);} } break;case 2L : 

{ /* Ieee/vector.scm 142 */
 obj_t BgL_fillz00_1839;
BgL_fillz00_1839 = 
VECTOR_REF(BgL_opt1048z00_6,1L); 
{ /* Ieee/vector.scm 142 */

{ /* Ieee/vector.scm 142 */
 long BgL_intz00_1840;
{ /* Ieee/vector.scm 142 */
 obj_t BgL_tmpz00_2060;
if(
INTEGERP(BgL_g1050z00_1836))
{ /* Ieee/vector.scm 142 */
BgL_tmpz00_2060 = BgL_g1050z00_1836
; }  else 
{ 
 obj_t BgL_auxz00_2063;
BgL_auxz00_2063 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(6640L), BGl_string1776z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_g1050z00_1836); 
FAILURE(BgL_auxz00_2063,BFALSE,BFALSE);} 
BgL_intz00_1840 = 
(long)CINT(BgL_tmpz00_2060); } 
return 
make_vector(BgL_intz00_1840, BgL_fillz00_1839);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol1772z00zz__r4_vectors_6_8z00, BGl_string1774z00zz__r4_vectors_6_8z00, 
BINT(
VECTOR_LENGTH(BgL_opt1048z00_6)));} } } } 

}



/* make-vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2vectorzd2zz__r4_vectors_6_8z00(long BgL_intz00_4, obj_t BgL_fillz00_5)
{
{ /* Ieee/vector.scm 142 */
BGL_TAIL return 
make_vector(BgL_intz00_4, BgL_fillz00_5);} 

}



/* vector */
BGL_EXPORTED_DEF obj_t BGl_vectorz00zz__r4_vectors_6_8z00(obj_t BgL_argsz00_8)
{
{ /* Ieee/vector.scm 148 */
{ /* Ieee/vector.scm 149 */
 obj_t BgL_auxz00_2075;
{ /* Ieee/vector.scm 149 */
 bool_t BgL_test2080z00_2076;
if(
PAIRP(BgL_argsz00_8))
{ /* Ieee/vector.scm 149 */
BgL_test2080z00_2076 = ((bool_t)1)
; }  else 
{ /* Ieee/vector.scm 149 */
BgL_test2080z00_2076 = 
NULLP(BgL_argsz00_8)
; } 
if(BgL_test2080z00_2076)
{ /* Ieee/vector.scm 149 */
BgL_auxz00_2075 = BgL_argsz00_8
; }  else 
{ 
 obj_t BgL_auxz00_2080;
BgL_auxz00_2080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7003L), BGl_string1778z00zz__r4_vectors_6_8z00, BGl_string1779z00zz__r4_vectors_6_8z00, BgL_argsz00_8); 
FAILURE(BgL_auxz00_2080,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_auxz00_2075);} } 

}



/* &vector */
obj_t BGl_z62vectorz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1539, obj_t BgL_argsz00_1540)
{
{ /* Ieee/vector.scm 148 */
return 
BGl_vectorz00zz__r4_vectors_6_8z00(BgL_argsz00_1540);} 

}



/* vector-length */
BGL_EXPORTED_DEF long BGl_vectorzd2lengthzd2zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_9)
{
{ /* Ieee/vector.scm 154 */
return 
VECTOR_LENGTH(BgL_vectorz00_9);} 

}



/* &vector-length */
obj_t BGl_z62vectorzd2lengthzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1541, obj_t BgL_vectorz00_1542)
{
{ /* Ieee/vector.scm 154 */
{ /* Ieee/vector.scm 155 */
 long BgL_tmpz00_2087;
{ /* Ieee/vector.scm 155 */
 obj_t BgL_auxz00_2088;
if(
VECTORP(BgL_vectorz00_1542))
{ /* Ieee/vector.scm 155 */
BgL_auxz00_2088 = BgL_vectorz00_1542
; }  else 
{ 
 obj_t BgL_auxz00_2091;
BgL_auxz00_2091 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7274L), BGl_string1780z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1542); 
FAILURE(BgL_auxz00_2091,BFALSE,BFALSE);} 
BgL_tmpz00_2087 = 
BGl_vectorzd2lengthzd2zz__r4_vectors_6_8z00(BgL_auxz00_2088); } 
return 
BINT(BgL_tmpz00_2087);} } 

}



/* vector-ref */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2refzd2zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_10, long BgL_kz00_11)
{
{ /* Ieee/vector.scm 160 */
{ /* Ieee/vector.scm 161 */
 bool_t BgL_test2083z00_2097;
{ /* Ieee/vector.scm 161 */
 long BgL_tmpz00_2098;
BgL_tmpz00_2098 = 
VECTOR_LENGTH(BgL_vectorz00_10); 
BgL_test2083z00_2097 = 
BOUND_CHECK(BgL_kz00_11, BgL_tmpz00_2098); } 
if(BgL_test2083z00_2097)
{ /* Ieee/vector.scm 161 */
return 
VECTOR_REF(BgL_vectorz00_10,BgL_kz00_11);}  else 
{ 
 obj_t BgL_auxz00_2102;
BgL_auxz00_2102 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1781z00zz__r4_vectors_6_8z00, BgL_vectorz00_10, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_10)), 
(int)(BgL_kz00_11)); 
FAILURE(BgL_auxz00_2102,BFALSE,BFALSE);} } } 

}



/* &vector-ref */
obj_t BGl_z62vectorzd2refzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1543, obj_t BgL_vectorz00_1544, obj_t BgL_kz00_1545)
{
{ /* Ieee/vector.scm 160 */
{ /* Ieee/vector.scm 161 */
 long BgL_auxz00_2116; obj_t BgL_auxz00_2109;
{ /* Ieee/vector.scm 161 */
 obj_t BgL_tmpz00_2117;
if(
INTEGERP(BgL_kz00_1545))
{ /* Ieee/vector.scm 161 */
BgL_tmpz00_2117 = BgL_kz00_1545
; }  else 
{ 
 obj_t BgL_auxz00_2120;
BgL_auxz00_2120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1782z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_kz00_1545); 
FAILURE(BgL_auxz00_2120,BFALSE,BFALSE);} 
BgL_auxz00_2116 = 
(long)CINT(BgL_tmpz00_2117); } 
if(
VECTORP(BgL_vectorz00_1544))
{ /* Ieee/vector.scm 161 */
BgL_auxz00_2109 = BgL_vectorz00_1544
; }  else 
{ 
 obj_t BgL_auxz00_2112;
BgL_auxz00_2112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1782z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1544); 
FAILURE(BgL_auxz00_2112,BFALSE,BFALSE);} 
return 
BGl_vectorzd2refzd2zz__r4_vectors_6_8z00(BgL_auxz00_2109, BgL_auxz00_2116);} } 

}



/* vector-set! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2setz12zc0zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_12, long BgL_kz00_13, obj_t BgL_objz00_14)
{
{ /* Ieee/vector.scm 166 */
{ /* Ieee/vector.scm 167 */
 bool_t BgL_test2086z00_2126;
{ /* Ieee/vector.scm 167 */
 long BgL_tmpz00_2127;
BgL_tmpz00_2127 = 
VECTOR_LENGTH(BgL_vectorz00_12); 
BgL_test2086z00_2126 = 
BOUND_CHECK(BgL_kz00_13, BgL_tmpz00_2127); } 
if(BgL_test2086z00_2126)
{ /* Ieee/vector.scm 167 */
return 
VECTOR_SET(BgL_vectorz00_12,BgL_kz00_13,BgL_objz00_14);}  else 
{ 
 obj_t BgL_auxz00_2131;
BgL_auxz00_2131 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7854L), BGl_string1783z00zz__r4_vectors_6_8z00, BgL_vectorz00_12, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_12)), 
(int)(BgL_kz00_13)); 
FAILURE(BgL_auxz00_2131,BFALSE,BFALSE);} } } 

}



/* &vector-set! */
obj_t BGl_z62vectorzd2setz12za2zz__r4_vectors_6_8z00(obj_t BgL_envz00_1546, obj_t BgL_vectorz00_1547, obj_t BgL_kz00_1548, obj_t BgL_objz00_1549)
{
{ /* Ieee/vector.scm 166 */
{ /* Ieee/vector.scm 167 */
 long BgL_auxz00_2145; obj_t BgL_auxz00_2138;
{ /* Ieee/vector.scm 167 */
 obj_t BgL_tmpz00_2146;
if(
INTEGERP(BgL_kz00_1548))
{ /* Ieee/vector.scm 167 */
BgL_tmpz00_2146 = BgL_kz00_1548
; }  else 
{ 
 obj_t BgL_auxz00_2149;
BgL_auxz00_2149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7854L), BGl_string1784z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_kz00_1548); 
FAILURE(BgL_auxz00_2149,BFALSE,BFALSE);} 
BgL_auxz00_2145 = 
(long)CINT(BgL_tmpz00_2146); } 
if(
VECTORP(BgL_vectorz00_1547))
{ /* Ieee/vector.scm 167 */
BgL_auxz00_2138 = BgL_vectorz00_1547
; }  else 
{ 
 obj_t BgL_auxz00_2141;
BgL_auxz00_2141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7854L), BGl_string1784z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1547); 
FAILURE(BgL_auxz00_2141,BFALSE,BFALSE);} 
return 
BGl_vectorzd2setz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2138, BgL_auxz00_2145, BgL_objz00_1549);} } 

}



/* vector-ref-ur */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2refzd2urz00zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_15, long BgL_kz00_16)
{
{ /* Ieee/vector.scm 172 */
return 
VECTOR_REF(BgL_vectorz00_15,BgL_kz00_16);} 

}



/* &vector-ref-ur */
obj_t BGl_z62vectorzd2refzd2urz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1550, obj_t BgL_vectorz00_1551, obj_t BgL_kz00_1552)
{
{ /* Ieee/vector.scm 172 */
{ /* Ieee/vector.scm 173 */
 long BgL_auxz00_2163; obj_t BgL_auxz00_2156;
{ /* Ieee/vector.scm 173 */
 obj_t BgL_tmpz00_2164;
if(
INTEGERP(BgL_kz00_1552))
{ /* Ieee/vector.scm 173 */
BgL_tmpz00_2164 = BgL_kz00_1552
; }  else 
{ 
 obj_t BgL_auxz00_2167;
BgL_auxz00_2167 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(8149L), BGl_string1785z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_kz00_1552); 
FAILURE(BgL_auxz00_2167,BFALSE,BFALSE);} 
BgL_auxz00_2163 = 
(long)CINT(BgL_tmpz00_2164); } 
if(
VECTORP(BgL_vectorz00_1551))
{ /* Ieee/vector.scm 173 */
BgL_auxz00_2156 = BgL_vectorz00_1551
; }  else 
{ 
 obj_t BgL_auxz00_2159;
BgL_auxz00_2159 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(8149L), BGl_string1785z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1551); 
FAILURE(BgL_auxz00_2159,BFALSE,BFALSE);} 
return 
BGl_vectorzd2refzd2urz00zz__r4_vectors_6_8z00(BgL_auxz00_2156, BgL_auxz00_2163);} } 

}



/* vector-set-ur! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2setzd2urz12z12zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_17, long BgL_kz00_18, obj_t BgL_objz00_19)
{
{ /* Ieee/vector.scm 178 */
return 
VECTOR_SET(BgL_vectorz00_17,BgL_kz00_18,BgL_objz00_19);} 

}



/* &vector-set-ur! */
obj_t BGl_z62vectorzd2setzd2urz12z70zz__r4_vectors_6_8z00(obj_t BgL_envz00_1553, obj_t BgL_vectorz00_1554, obj_t BgL_kz00_1555, obj_t BgL_objz00_1556)
{
{ /* Ieee/vector.scm 178 */
{ /* Ieee/vector.scm 179 */
 long BgL_auxz00_2181; obj_t BgL_auxz00_2174;
{ /* Ieee/vector.scm 179 */
 obj_t BgL_tmpz00_2182;
if(
INTEGERP(BgL_kz00_1555))
{ /* Ieee/vector.scm 179 */
BgL_tmpz00_2182 = BgL_kz00_1555
; }  else 
{ 
 obj_t BgL_auxz00_2185;
BgL_auxz00_2185 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(8447L), BGl_string1786z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_kz00_1555); 
FAILURE(BgL_auxz00_2185,BFALSE,BFALSE);} 
BgL_auxz00_2181 = 
(long)CINT(BgL_tmpz00_2182); } 
if(
VECTORP(BgL_vectorz00_1554))
{ /* Ieee/vector.scm 179 */
BgL_auxz00_2174 = BgL_vectorz00_1554
; }  else 
{ 
 obj_t BgL_auxz00_2177;
BgL_auxz00_2177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(8447L), BGl_string1786z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1554); 
FAILURE(BgL_auxz00_2177,BFALSE,BFALSE);} 
return 
BGl_vectorzd2setzd2urz12z12zz__r4_vectors_6_8z00(BgL_auxz00_2174, BgL_auxz00_2181, BgL_objz00_1556);} } 

}



/* vector->list */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_20)
{
{ /* Ieee/vector.scm 184 */
if(
(
VECTOR_LENGTH(BgL_vectorz00_20)==0L))
{ /* Ieee/vector.scm 186 */
return BNIL;}  else 
{ /* Ieee/vector.scm 188 */
 long BgL_g1014z00_758;
BgL_g1014z00_758 = 
(
VECTOR_LENGTH(BgL_vectorz00_20)-1L); 
{ 
 long BgL_iz00_761; obj_t BgL_accz00_762;
BgL_iz00_761 = BgL_g1014z00_758; 
BgL_accz00_762 = BNIL; 
BgL_zc3z04anonymousza31087ze3z87_763:
if(
(BgL_iz00_761==0L))
{ /* Ieee/vector.scm 190 */
return 
MAKE_YOUNG_PAIR(
VECTOR_REF(BgL_vectorz00_20,BgL_iz00_761), BgL_accz00_762);}  else 
{ /* Ieee/vector.scm 192 */
 long BgL_arg1092z00_766; obj_t BgL_arg1097z00_767;
BgL_arg1092z00_766 = 
(BgL_iz00_761-1L); 
BgL_arg1097z00_767 = 
MAKE_YOUNG_PAIR(
VECTOR_REF(BgL_vectorz00_20,BgL_iz00_761), BgL_accz00_762); 
{ 
 obj_t BgL_accz00_2204; long BgL_iz00_2203;
BgL_iz00_2203 = BgL_arg1092z00_766; 
BgL_accz00_2204 = BgL_arg1097z00_767; 
BgL_accz00_762 = BgL_accz00_2204; 
BgL_iz00_761 = BgL_iz00_2203; 
goto BgL_zc3z04anonymousza31087ze3z87_763;} } } } } 

}



/* &vector->list */
obj_t BGl_z62vectorzd2ze3listz53zz__r4_vectors_6_8z00(obj_t BgL_envz00_1557, obj_t BgL_vectorz00_1558)
{
{ /* Ieee/vector.scm 184 */
{ /* Ieee/vector.scm 185 */
 obj_t BgL_auxz00_2205;
if(
VECTORP(BgL_vectorz00_1558))
{ /* Ieee/vector.scm 185 */
BgL_auxz00_2205 = BgL_vectorz00_1558
; }  else 
{ 
 obj_t BgL_auxz00_2208;
BgL_auxz00_2208 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(8735L), BGl_string1787z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1558); 
FAILURE(BgL_auxz00_2208,BFALSE,BFALSE);} 
return 
BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_auxz00_2205);} } 

}



/* list->vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t BgL_listz00_21)
{
{ /* Ieee/vector.scm 197 */
{ /* Ieee/vector.scm 198 */
 long BgL_lenz00_770;
BgL_lenz00_770 = 
bgl_list_length(BgL_listz00_21); 
{ /* Ieee/vector.scm 198 */
 obj_t BgL_vecz00_771;
BgL_vecz00_771 = 
create_vector(BgL_lenz00_770); 
{ /* Ieee/vector.scm 199 */

{ 
 long BgL_iz00_1314; obj_t BgL_lz00_1315;
BgL_iz00_1314 = 0L; 
BgL_lz00_1315 = BgL_listz00_21; 
BgL_loopz00_1313:
if(
(BgL_iz00_1314==BgL_lenz00_770))
{ /* Ieee/vector.scm 202 */
return BgL_vecz00_771;}  else 
{ /* Ieee/vector.scm 202 */
{ /* Ieee/vector.scm 205 */
 obj_t BgL_arg1114z00_1319;
{ /* Ieee/vector.scm 205 */
 obj_t BgL_pairz00_1320;
if(
PAIRP(BgL_lz00_1315))
{ /* Ieee/vector.scm 205 */
BgL_pairz00_1320 = BgL_lz00_1315; }  else 
{ 
 obj_t BgL_auxz00_2219;
BgL_auxz00_2219 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9389L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_lz00_1315); 
FAILURE(BgL_auxz00_2219,BFALSE,BFALSE);} 
BgL_arg1114z00_1319 = 
CAR(BgL_pairz00_1320); } 
VECTOR_SET(BgL_vecz00_771,BgL_iz00_1314,BgL_arg1114z00_1319); } 
{ /* Ieee/vector.scm 206 */
 long BgL_arg1115z00_1323; obj_t BgL_arg1122z00_1324;
BgL_arg1115z00_1323 = 
(BgL_iz00_1314+1L); 
{ /* Ieee/vector.scm 206 */
 obj_t BgL_pairz00_1326;
if(
PAIRP(BgL_lz00_1315))
{ /* Ieee/vector.scm 206 */
BgL_pairz00_1326 = BgL_lz00_1315; }  else 
{ 
 obj_t BgL_auxz00_2228;
BgL_auxz00_2228 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9416L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_lz00_1315); 
FAILURE(BgL_auxz00_2228,BFALSE,BFALSE);} 
BgL_arg1122z00_1324 = 
CDR(BgL_pairz00_1326); } 
{ 
 obj_t BgL_lz00_2234; long BgL_iz00_2233;
BgL_iz00_2233 = BgL_arg1115z00_1323; 
BgL_lz00_2234 = BgL_arg1122z00_1324; 
BgL_lz00_1315 = BgL_lz00_2234; 
BgL_iz00_1314 = BgL_iz00_2233; 
goto BgL_loopz00_1313;} } } } } } } } 

}



/* &list->vector */
obj_t BGl_z62listzd2ze3vectorz53zz__r4_vectors_6_8z00(obj_t BgL_envz00_1559, obj_t BgL_listz00_1560)
{
{ /* Ieee/vector.scm 197 */
{ /* Ieee/vector.scm 198 */
 obj_t BgL_auxz00_2235;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_1560))
{ /* Ieee/vector.scm 198 */
BgL_auxz00_2235 = BgL_listz00_1560
; }  else 
{ 
 obj_t BgL_auxz00_2238;
BgL_auxz00_2238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9225L), BGl_string1790z00zz__r4_vectors_6_8z00, BGl_string1779z00zz__r4_vectors_6_8z00, BgL_listz00_1560); 
FAILURE(BgL_auxz00_2238,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_auxz00_2235);} } 

}



/* _vector-fill! */
obj_t BGl__vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t BgL_env1054z00_27, obj_t BgL_opt1053z00_26)
{
{ /* Ieee/vector.scm 211 */
{ /* Ieee/vector.scm 211 */
 obj_t BgL_vecz00_781; obj_t BgL_fillz00_782;
BgL_vecz00_781 = 
VECTOR_REF(BgL_opt1053z00_26,0L); 
BgL_fillz00_782 = 
VECTOR_REF(BgL_opt1053z00_26,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1053z00_26)) { case 2L : 

{ /* Ieee/vector.scm 212 */
 long BgL_endz00_786;
{ /* Ieee/vector.scm 212 */
 obj_t BgL_vectorz00_1327;
if(
VECTORP(BgL_vecz00_781))
{ /* Ieee/vector.scm 212 */
BgL_vectorz00_1327 = BgL_vecz00_781; }  else 
{ 
 obj_t BgL_auxz00_2247;
BgL_auxz00_2247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9736L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_781); 
FAILURE(BgL_auxz00_2247,BFALSE,BFALSE);} 
BgL_endz00_786 = 
VECTOR_LENGTH(BgL_vectorz00_1327); } 
{ /* Ieee/vector.scm 211 */

{ /* Ieee/vector.scm 211 */
 obj_t BgL_auxz00_2252;
if(
VECTORP(BgL_vecz00_781))
{ /* Ieee/vector.scm 211 */
BgL_auxz00_2252 = BgL_vecz00_781
; }  else 
{ 
 obj_t BgL_auxz00_2255;
BgL_auxz00_2255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9648L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_781); 
FAILURE(BgL_auxz00_2255,BFALSE,BFALSE);} 
return 
BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2252, BgL_fillz00_782, 0L, BgL_endz00_786);} } } break;case 3L : 

{ /* Ieee/vector.scm 211 */
 obj_t BgL_startz00_787;
BgL_startz00_787 = 
VECTOR_REF(BgL_opt1053z00_26,2L); 
{ /* Ieee/vector.scm 212 */
 long BgL_endz00_788;
{ /* Ieee/vector.scm 212 */
 obj_t BgL_vectorz00_1328;
if(
VECTORP(BgL_vecz00_781))
{ /* Ieee/vector.scm 212 */
BgL_vectorz00_1328 = BgL_vecz00_781; }  else 
{ 
 obj_t BgL_auxz00_2263;
BgL_auxz00_2263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9736L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_781); 
FAILURE(BgL_auxz00_2263,BFALSE,BFALSE);} 
BgL_endz00_788 = 
VECTOR_LENGTH(BgL_vectorz00_1328); } 
{ /* Ieee/vector.scm 211 */

{ /* Ieee/vector.scm 211 */
 long BgL_auxz00_2275; obj_t BgL_auxz00_2268;
{ /* Ieee/vector.scm 211 */
 obj_t BgL_tmpz00_2276;
if(
INTEGERP(BgL_startz00_787))
{ /* Ieee/vector.scm 211 */
BgL_tmpz00_2276 = BgL_startz00_787
; }  else 
{ 
 obj_t BgL_auxz00_2279;
BgL_auxz00_2279 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9648L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_startz00_787); 
FAILURE(BgL_auxz00_2279,BFALSE,BFALSE);} 
BgL_auxz00_2275 = 
(long)CINT(BgL_tmpz00_2276); } 
if(
VECTORP(BgL_vecz00_781))
{ /* Ieee/vector.scm 211 */
BgL_auxz00_2268 = BgL_vecz00_781
; }  else 
{ 
 obj_t BgL_auxz00_2271;
BgL_auxz00_2271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9648L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_781); 
FAILURE(BgL_auxz00_2271,BFALSE,BFALSE);} 
return 
BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2268, BgL_fillz00_782, BgL_auxz00_2275, BgL_endz00_788);} } } } break;case 4L : 

{ /* Ieee/vector.scm 211 */
 obj_t BgL_startz00_789;
BgL_startz00_789 = 
VECTOR_REF(BgL_opt1053z00_26,2L); 
{ /* Ieee/vector.scm 211 */
 obj_t BgL_endz00_790;
BgL_endz00_790 = 
VECTOR_REF(BgL_opt1053z00_26,3L); 
{ /* Ieee/vector.scm 211 */

{ /* Ieee/vector.scm 211 */
 long BgL_auxz00_2303; long BgL_auxz00_2294; obj_t BgL_auxz00_2287;
{ /* Ieee/vector.scm 211 */
 obj_t BgL_tmpz00_2304;
if(
INTEGERP(BgL_endz00_790))
{ /* Ieee/vector.scm 211 */
BgL_tmpz00_2304 = BgL_endz00_790
; }  else 
{ 
 obj_t BgL_auxz00_2307;
BgL_auxz00_2307 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9648L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_endz00_790); 
FAILURE(BgL_auxz00_2307,BFALSE,BFALSE);} 
BgL_auxz00_2303 = 
(long)CINT(BgL_tmpz00_2304); } 
{ /* Ieee/vector.scm 211 */
 obj_t BgL_tmpz00_2295;
if(
INTEGERP(BgL_startz00_789))
{ /* Ieee/vector.scm 211 */
BgL_tmpz00_2295 = BgL_startz00_789
; }  else 
{ 
 obj_t BgL_auxz00_2298;
BgL_auxz00_2298 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9648L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_startz00_789); 
FAILURE(BgL_auxz00_2298,BFALSE,BFALSE);} 
BgL_auxz00_2294 = 
(long)CINT(BgL_tmpz00_2295); } 
if(
VECTORP(BgL_vecz00_781))
{ /* Ieee/vector.scm 211 */
BgL_auxz00_2287 = BgL_vecz00_781
; }  else 
{ 
 obj_t BgL_auxz00_2290;
BgL_auxz00_2290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(9648L), BGl_string1794z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_781); 
FAILURE(BgL_auxz00_2290,BFALSE,BFALSE);} 
return 
BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2287, BgL_fillz00_782, BgL_auxz00_2294, BgL_auxz00_2303);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol1791z00zz__r4_vectors_6_8z00, BGl_string1793z00zz__r4_vectors_6_8z00, 
BINT(
VECTOR_LENGTH(BgL_opt1053z00_26)));} } } } 

}



/* vector-fill! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t BgL_vecz00_22, obj_t BgL_fillz00_23, long BgL_startz00_24, long BgL_endz00_25)
{
{ /* Ieee/vector.scm 211 */
if(
(BgL_startz00_24<0L))
{ /* Ieee/vector.scm 214 */
return 
BGl_errorz00zz__errorz00(BGl_string1792z00zz__r4_vectors_6_8z00, BGl_string1795z00zz__r4_vectors_6_8z00, 
BINT(BgL_startz00_24));}  else 
{ /* Ieee/vector.scm 214 */
if(
(BgL_endz00_25>
VECTOR_LENGTH(BgL_vecz00_22)))
{ /* Ieee/vector.scm 216 */
return 
BGl_errorz00zz__errorz00(BGl_string1792z00zz__r4_vectors_6_8z00, BGl_string1796z00zz__r4_vectors_6_8z00, 
BINT(BgL_endz00_25));}  else 
{ /* Ieee/vector.scm 216 */
if(
(BgL_startz00_24>=BgL_endz00_25))
{ /* Ieee/vector.scm 219 */
 bool_t BgL_test2111z00_2329;
if(
(BgL_startz00_24==BgL_endz00_25))
{ /* Ieee/vector.scm 219 */
BgL_test2111z00_2329 = 
(BgL_startz00_24==0L)
; }  else 
{ /* Ieee/vector.scm 219 */
BgL_test2111z00_2329 = ((bool_t)0)
; } 
if(BgL_test2111z00_2329)
{ /* Ieee/vector.scm 219 */
return BUNSPEC;}  else 
{ /* Ieee/vector.scm 222 */
 obj_t BgL_arg1131z00_798;
BgL_arg1131z00_798 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_24), 
BINT(BgL_endz00_25)); 
return 
BGl_errorz00zz__errorz00(BGl_string1792z00zz__r4_vectors_6_8z00, BGl_string1797z00zz__r4_vectors_6_8z00, BgL_arg1131z00_798);} }  else 
{ /* Ieee/vector.scm 218 */
return 
bgl_fill_vector(BgL_vecz00_22, BgL_startz00_24, BgL_endz00_25, BgL_fillz00_23);} } } } 

}



/* vector-tag */
BGL_EXPORTED_DEF int BGl_vectorzd2tagzd2zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_28)
{
{ /* Ieee/vector.scm 229 */
return 
VECTOR_TAG(BgL_vectorz00_28);} 

}



/* &vector-tag */
obj_t BGl_z62vectorzd2tagzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1561, obj_t BgL_vectorz00_1562)
{
{ /* Ieee/vector.scm 229 */
{ /* Ieee/vector.scm 230 */
 int BgL_tmpz00_2339;
{ /* Ieee/vector.scm 230 */
 obj_t BgL_auxz00_2340;
if(
VECTORP(BgL_vectorz00_1562))
{ /* Ieee/vector.scm 230 */
BgL_auxz00_2340 = BgL_vectorz00_1562
; }  else 
{ 
 obj_t BgL_auxz00_2343;
BgL_auxz00_2343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(10416L), BGl_string1798z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1562); 
FAILURE(BgL_auxz00_2343,BFALSE,BFALSE);} 
BgL_tmpz00_2339 = 
BGl_vectorzd2tagzd2zz__r4_vectors_6_8z00(BgL_auxz00_2340); } 
return 
BINT(BgL_tmpz00_2339);} } 

}



/* vector-tag-set! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2tagzd2setz12z12zz__r4_vectors_6_8z00(obj_t BgL_vectorz00_29, int BgL_tagz00_30)
{
{ /* Ieee/vector.scm 235 */
return 
VECTOR_TAG_SET(BgL_vectorz00_29, BgL_tagz00_30);} 

}



/* &vector-tag-set! */
obj_t BGl_z62vectorzd2tagzd2setz12z70zz__r4_vectors_6_8z00(obj_t BgL_envz00_1563, obj_t BgL_vectorz00_1564, obj_t BgL_tagz00_1565)
{
{ /* Ieee/vector.scm 235 */
{ /* Ieee/vector.scm 236 */
 int BgL_auxz00_2357; obj_t BgL_auxz00_2350;
{ /* Ieee/vector.scm 236 */
 obj_t BgL_tmpz00_2358;
if(
INTEGERP(BgL_tagz00_1565))
{ /* Ieee/vector.scm 236 */
BgL_tmpz00_2358 = BgL_tagz00_1565
; }  else 
{ 
 obj_t BgL_auxz00_2361;
BgL_auxz00_2361 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(10708L), BGl_string1799z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_tagz00_1565); 
FAILURE(BgL_auxz00_2361,BFALSE,BFALSE);} 
BgL_auxz00_2357 = 
CINT(BgL_tmpz00_2358); } 
if(
VECTORP(BgL_vectorz00_1564))
{ /* Ieee/vector.scm 236 */
BgL_auxz00_2350 = BgL_vectorz00_1564
; }  else 
{ 
 obj_t BgL_auxz00_2353;
BgL_auxz00_2353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(10708L), BGl_string1799z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vectorz00_1564); 
FAILURE(BgL_auxz00_2353,BFALSE,BFALSE);} 
return 
BGl_vectorzd2tagzd2setz12z12zz__r4_vectors_6_8z00(BgL_auxz00_2350, BgL_auxz00_2357);} } 

}



/* copy-vector */
BGL_EXPORTED_DEF obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t BgL_oldzd2veczd2_31, long BgL_newzd2lenzd2_32)
{
{ /* Ieee/vector.scm 241 */
{ /* Ieee/vector.scm 242 */
 obj_t BgL_newzd2veczd2_802;
BgL_newzd2veczd2_802 = 
make_vector(BgL_newzd2lenzd2_32, BUNSPEC); 
{ /* Ieee/vector.scm 243 */
 long BgL_minz00_803;
if(
(BgL_newzd2lenzd2_32<
VECTOR_LENGTH(BgL_oldzd2veczd2_31)))
{ /* Ieee/vector.scm 244 */
BgL_minz00_803 = BgL_newzd2lenzd2_32; }  else 
{ /* Ieee/vector.scm 244 */
BgL_minz00_803 = 
VECTOR_LENGTH(BgL_oldzd2veczd2_31); } 
{ /* Ieee/vector.scm 244 */

return 
BGL_VECTOR_BLIT_NO_OVERLAP(BgL_newzd2veczd2_802, BgL_oldzd2veczd2_31, 0L, 0L, BgL_minz00_803);} } } } 

}



/* &copy-vector */
obj_t BGl_z62copyzd2vectorzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1566, obj_t BgL_oldzd2veczd2_1567, obj_t BgL_newzd2lenzd2_1568)
{
{ /* Ieee/vector.scm 241 */
{ /* Ieee/vector.scm 242 */
 long BgL_auxz00_2380; obj_t BgL_auxz00_2373;
{ /* Ieee/vector.scm 242 */
 obj_t BgL_tmpz00_2381;
if(
INTEGERP(BgL_newzd2lenzd2_1568))
{ /* Ieee/vector.scm 242 */
BgL_tmpz00_2381 = BgL_newzd2lenzd2_1568
; }  else 
{ 
 obj_t BgL_auxz00_2384;
BgL_auxz00_2384 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11025L), BGl_string1800z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_newzd2lenzd2_1568); 
FAILURE(BgL_auxz00_2384,BFALSE,BFALSE);} 
BgL_auxz00_2380 = 
(long)CINT(BgL_tmpz00_2381); } 
if(
VECTORP(BgL_oldzd2veczd2_1567))
{ /* Ieee/vector.scm 242 */
BgL_auxz00_2373 = BgL_oldzd2veczd2_1567
; }  else 
{ 
 obj_t BgL_auxz00_2376;
BgL_auxz00_2376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11025L), BGl_string1800z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_oldzd2veczd2_1567); 
FAILURE(BgL_auxz00_2376,BFALSE,BFALSE);} 
return 
BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(BgL_auxz00_2373, BgL_auxz00_2380);} } 

}



/* vector-copy3 */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(obj_t BgL_oldzd2veczd2_33, obj_t BgL_startz00_34, obj_t BgL_stopz00_35)
{
{ /* Ieee/vector.scm 261 */
{ /* Ieee/vector.scm 262 */
 long BgL_newzd2lenzd2_807;
{ /* Ieee/vector.scm 263 */
 long BgL_za71za7_1343; long BgL_za72za7_1344;
{ /* Ieee/vector.scm 263 */
 obj_t BgL_tmpz00_2390;
if(
INTEGERP(BgL_stopz00_35))
{ /* Ieee/vector.scm 263 */
BgL_tmpz00_2390 = BgL_stopz00_35
; }  else 
{ 
 obj_t BgL_auxz00_2393;
BgL_auxz00_2393 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11765L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_stopz00_35); 
FAILURE(BgL_auxz00_2393,BFALSE,BFALSE);} 
BgL_za71za7_1343 = 
(long)CINT(BgL_tmpz00_2390); } 
{ /* Ieee/vector.scm 263 */
 obj_t BgL_tmpz00_2398;
if(
INTEGERP(BgL_startz00_34))
{ /* Ieee/vector.scm 263 */
BgL_tmpz00_2398 = BgL_startz00_34
; }  else 
{ 
 obj_t BgL_auxz00_2401;
BgL_auxz00_2401 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11770L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_startz00_34); 
FAILURE(BgL_auxz00_2401,BFALSE,BFALSE);} 
BgL_za72za7_1344 = 
(long)CINT(BgL_tmpz00_2398); } 
BgL_newzd2lenzd2_807 = 
(BgL_za71za7_1343-BgL_za72za7_1344); } 
{ /* Ieee/vector.scm 263 */
 obj_t BgL_newzd2veczd2_808;
BgL_newzd2veczd2_808 = 
make_vector(BgL_newzd2lenzd2_807, BUNSPEC); 
{ /* Ieee/vector.scm 265 */

{ /* Ieee/vector.scm 268 */
 bool_t BgL_test2121z00_2408;
if(
(BgL_newzd2lenzd2_807<0L))
{ /* Ieee/vector.scm 268 */
BgL_test2121z00_2408 = ((bool_t)1)
; }  else 
{ /* Ieee/vector.scm 268 */
 bool_t BgL_test2123z00_2411;
{ /* Ieee/vector.scm 268 */
 long BgL_n1z00_1349;
{ /* Ieee/vector.scm 268 */
 obj_t BgL_tmpz00_2412;
if(
INTEGERP(BgL_startz00_34))
{ /* Ieee/vector.scm 268 */
BgL_tmpz00_2412 = BgL_startz00_34
; }  else 
{ 
 obj_t BgL_auxz00_2415;
BgL_auxz00_2415 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11911L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_startz00_34); 
FAILURE(BgL_auxz00_2415,BFALSE,BFALSE);} 
BgL_n1z00_1349 = 
(long)CINT(BgL_tmpz00_2412); } 
BgL_test2123z00_2411 = 
(BgL_n1z00_1349>
VECTOR_LENGTH(BgL_oldzd2veczd2_33)); } 
if(BgL_test2123z00_2411)
{ /* Ieee/vector.scm 268 */
BgL_test2121z00_2408 = ((bool_t)1)
; }  else 
{ /* Ieee/vector.scm 268 */
 long BgL_n1z00_1351;
{ /* Ieee/vector.scm 268 */
 obj_t BgL_tmpz00_2422;
if(
INTEGERP(BgL_stopz00_35))
{ /* Ieee/vector.scm 268 */
BgL_tmpz00_2422 = BgL_stopz00_35
; }  else 
{ 
 obj_t BgL_auxz00_2425;
BgL_auxz00_2425 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11931L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_stopz00_35); 
FAILURE(BgL_auxz00_2425,BFALSE,BFALSE);} 
BgL_n1z00_1351 = 
(long)CINT(BgL_tmpz00_2422); } 
BgL_test2121z00_2408 = 
(BgL_n1z00_1351>
VECTOR_LENGTH(BgL_oldzd2veczd2_33)); } } 
if(BgL_test2121z00_2408)
{ /* Ieee/vector.scm 269 */
 obj_t BgL_arg1141z00_813;
BgL_arg1141z00_813 = 
MAKE_YOUNG_PAIR(BgL_startz00_34, BgL_stopz00_35); 
{ /* Ieee/vector.scm 269 */
 obj_t BgL_aux1665z00_1686;
BgL_aux1665z00_1686 = 
BGl_errorz00zz__errorz00(BGl_string1802z00zz__r4_vectors_6_8z00, BGl_string1803z00zz__r4_vectors_6_8z00, BgL_arg1141z00_813); 
if(
VECTORP(BgL_aux1665z00_1686))
{ /* Ieee/vector.scm 269 */
return BgL_aux1665z00_1686;}  else 
{ 
 obj_t BgL_auxz00_2436;
BgL_auxz00_2436 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11949L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1665z00_1686); 
FAILURE(BgL_auxz00_2436,BFALSE,BFALSE);} } }  else 
{ /* Ieee/vector.scm 272 */
 long BgL_arg1142z00_814;
{ /* Ieee/vector.scm 272 */
 long BgL_za71za7_1353; long BgL_za72za7_1354;
{ /* Ieee/vector.scm 272 */
 obj_t BgL_tmpz00_2440;
if(
INTEGERP(BgL_stopz00_35))
{ /* Ieee/vector.scm 272 */
BgL_tmpz00_2440 = BgL_stopz00_35
; }  else 
{ 
 obj_t BgL_auxz00_2443;
BgL_auxz00_2443 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12101L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_stopz00_35); 
FAILURE(BgL_auxz00_2443,BFALSE,BFALSE);} 
BgL_za71za7_1353 = 
(long)CINT(BgL_tmpz00_2440); } 
{ /* Ieee/vector.scm 272 */
 obj_t BgL_tmpz00_2448;
if(
INTEGERP(BgL_startz00_34))
{ /* Ieee/vector.scm 272 */
BgL_tmpz00_2448 = BgL_startz00_34
; }  else 
{ 
 obj_t BgL_auxz00_2451;
BgL_auxz00_2451 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12106L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_startz00_34); 
FAILURE(BgL_auxz00_2451,BFALSE,BFALSE);} 
BgL_za72za7_1354 = 
(long)CINT(BgL_tmpz00_2448); } 
BgL_arg1142z00_814 = 
(BgL_za71za7_1353-BgL_za72za7_1354); } 
{ /* Ieee/vector.scm 272 */
 long BgL_tmpz00_2457;
{ /* Ieee/vector.scm 272 */
 obj_t BgL_tmpz00_2458;
if(
INTEGERP(BgL_startz00_34))
{ /* Ieee/vector.scm 272 */
BgL_tmpz00_2458 = BgL_startz00_34
; }  else 
{ 
 obj_t BgL_auxz00_2461;
BgL_auxz00_2461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12090L), BGl_string1801z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_startz00_34); 
FAILURE(BgL_auxz00_2461,BFALSE,BFALSE);} 
BgL_tmpz00_2457 = 
(long)CINT(BgL_tmpz00_2458); } 
return 
BGL_VECTOR_BLIT_NO_OVERLAP(BgL_newzd2veczd2_808, BgL_oldzd2veczd2_33, 0L, BgL_tmpz00_2457, BgL_arg1142z00_814);} } } } } } } 

}



/* &vector-copy3 */
obj_t BGl_z62vectorzd2copy3zb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1569, obj_t BgL_oldzd2veczd2_1570, obj_t BgL_startz00_1571, obj_t BgL_stopz00_1572)
{
{ /* Ieee/vector.scm 261 */
{ /* Ieee/vector.scm 262 */
 obj_t BgL_auxz00_2467;
if(
VECTORP(BgL_oldzd2veczd2_1570))
{ /* Ieee/vector.scm 262 */
BgL_auxz00_2467 = BgL_oldzd2veczd2_1570
; }  else 
{ 
 obj_t BgL_auxz00_2470;
BgL_auxz00_2470 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(11707L), BGl_string1804z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_oldzd2veczd2_1570); 
FAILURE(BgL_auxz00_2470,BFALSE,BFALSE);} 
return 
BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(BgL_auxz00_2467, BgL_startz00_1571, BgL_stopz00_1572);} } 

}



/* vector-copy */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2copyzd2zz__r4_vectors_6_8z00(obj_t BgL_oldzd2veczd2_36, obj_t BgL_argsz00_37)
{
{ /* Ieee/vector.scm 285 */
{ /* Ieee/vector.scm 286 */
 obj_t BgL_startz00_820;
if(
PAIRP(BgL_argsz00_37))
{ /* Ieee/vector.scm 287 */
if(
INTEGERP(
CAR(BgL_argsz00_37)))
{ /* Ieee/vector.scm 288 */
BgL_startz00_820 = 
CAR(BgL_argsz00_37); }  else 
{ /* Ieee/vector.scm 288 */
BgL_startz00_820 = 
BGl_errorz00zz__errorz00(BGl_string1802z00zz__r4_vectors_6_8z00, BGl_string1805z00zz__r4_vectors_6_8z00, 
CAR(BgL_argsz00_37)); } }  else 
{ /* Ieee/vector.scm 287 */
BgL_startz00_820 = 
BINT(0L); } 
{ /* Ieee/vector.scm 287 */
 obj_t BgL_stopz00_821;
{ /* Ieee/vector.scm 292 */
 bool_t BgL_test2133z00_2484;
if(
PAIRP(BgL_argsz00_37))
{ /* Ieee/vector.scm 292 */
 obj_t BgL_tmpz00_2487;
BgL_tmpz00_2487 = 
CDR(BgL_argsz00_37); 
BgL_test2133z00_2484 = 
PAIRP(BgL_tmpz00_2487); }  else 
{ /* Ieee/vector.scm 292 */
BgL_test2133z00_2484 = ((bool_t)0)
; } 
if(BgL_test2133z00_2484)
{ /* Ieee/vector.scm 293 */
 bool_t BgL_test2135z00_2490;
{ /* Ieee/vector.scm 293 */
 bool_t BgL_test2136z00_2491;
{ /* Ieee/vector.scm 293 */
 obj_t BgL_tmpz00_2492;
{ /* Ieee/vector.scm 293 */
 obj_t BgL_pairz00_1363;
{ /* Ieee/vector.scm 293 */
 obj_t BgL_aux1672z00_1693;
BgL_aux1672z00_1693 = 
CDR(BgL_argsz00_37); 
if(
PAIRP(BgL_aux1672z00_1693))
{ /* Ieee/vector.scm 293 */
BgL_pairz00_1363 = BgL_aux1672z00_1693; }  else 
{ 
 obj_t BgL_auxz00_2496;
BgL_auxz00_2496 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12843L), BGl_string1802z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_aux1672z00_1693); 
FAILURE(BgL_auxz00_2496,BFALSE,BFALSE);} } 
BgL_tmpz00_2492 = 
CDR(BgL_pairz00_1363); } 
BgL_test2136z00_2491 = 
PAIRP(BgL_tmpz00_2492); } 
if(BgL_test2136z00_2491)
{ /* Ieee/vector.scm 293 */
BgL_test2135z00_2490 = ((bool_t)1)
; }  else 
{ /* Ieee/vector.scm 294 */
 bool_t BgL_test2138z00_2502;
{ /* Ieee/vector.scm 294 */
 obj_t BgL_tmpz00_2503;
{ /* Ieee/vector.scm 294 */
 obj_t BgL_pairz00_1367;
{ /* Ieee/vector.scm 294 */
 obj_t BgL_aux1674z00_1695;
BgL_aux1674z00_1695 = 
CDR(BgL_argsz00_37); 
if(
PAIRP(BgL_aux1674z00_1695))
{ /* Ieee/vector.scm 294 */
BgL_pairz00_1367 = BgL_aux1674z00_1695; }  else 
{ 
 obj_t BgL_auxz00_2507;
BgL_auxz00_2507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12877L), BGl_string1802z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_aux1674z00_1695); 
FAILURE(BgL_auxz00_2507,BFALSE,BFALSE);} } 
BgL_tmpz00_2503 = 
CAR(BgL_pairz00_1367); } 
BgL_test2138z00_2502 = 
INTEGERP(BgL_tmpz00_2503); } 
if(BgL_test2138z00_2502)
{ /* Ieee/vector.scm 294 */
BgL_test2135z00_2490 = ((bool_t)0)
; }  else 
{ /* Ieee/vector.scm 294 */
BgL_test2135z00_2490 = ((bool_t)1)
; } } } 
if(BgL_test2135z00_2490)
{ /* Ieee/vector.scm 293 */
BgL_stopz00_821 = 
BGl_errorz00zz__errorz00(BGl_string1802z00zz__r4_vectors_6_8z00, BGl_string1805z00zz__r4_vectors_6_8z00, 
CDR(BgL_argsz00_37)); }  else 
{ /* Ieee/vector.scm 296 */
 obj_t BgL_pairz00_1372;
{ /* Ieee/vector.scm 296 */
 obj_t BgL_aux1676z00_1697;
BgL_aux1676z00_1697 = 
CDR(BgL_argsz00_37); 
if(
PAIRP(BgL_aux1676z00_1697))
{ /* Ieee/vector.scm 296 */
BgL_pairz00_1372 = BgL_aux1676z00_1697; }  else 
{ 
 obj_t BgL_auxz00_2518;
BgL_auxz00_2518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12950L), BGl_string1802z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_aux1676z00_1697); 
FAILURE(BgL_auxz00_2518,BFALSE,BFALSE);} } 
BgL_stopz00_821 = 
CAR(BgL_pairz00_1372); } }  else 
{ /* Ieee/vector.scm 292 */
BgL_stopz00_821 = 
BINT(
VECTOR_LENGTH(BgL_oldzd2veczd2_36)); } } 
{ /* Ieee/vector.scm 292 */

return 
BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(BgL_oldzd2veczd2_36, BgL_startz00_820, BgL_stopz00_821);} } } } 

}



/* &vector-copy */
obj_t BGl_z62vectorzd2copyzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1573, obj_t BgL_oldzd2veczd2_1574, obj_t BgL_argsz00_1575)
{
{ /* Ieee/vector.scm 285 */
{ /* Ieee/vector.scm 286 */
 obj_t BgL_auxz00_2526;
if(
VECTORP(BgL_oldzd2veczd2_1574))
{ /* Ieee/vector.scm 286 */
BgL_auxz00_2526 = BgL_oldzd2veczd2_1574
; }  else 
{ 
 obj_t BgL_auxz00_2529;
BgL_auxz00_2529 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(12588L), BGl_string1806z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_oldzd2veczd2_1574); 
FAILURE(BgL_auxz00_2529,BFALSE,BFALSE);} 
return 
BGl_vectorzd2copyzd2zz__r4_vectors_6_8z00(BgL_auxz00_2526, BgL_argsz00_1575);} } 

}



/* _vector-copy! */
obj_t BGl__vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t BgL_env1058z00_44, obj_t BgL_opt1057z00_43)
{
{ /* Ieee/vector.scm 303 */
{ /* Ieee/vector.scm 303 */
 obj_t BgL_g1059z00_844; obj_t BgL_g1060z00_845; obj_t BgL_sourcez00_846;
BgL_g1059z00_844 = 
VECTOR_REF(BgL_opt1057z00_43,0L); 
BgL_g1060z00_845 = 
VECTOR_REF(BgL_opt1057z00_43,1L); 
BgL_sourcez00_846 = 
VECTOR_REF(BgL_opt1057z00_43,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1057z00_43)) { case 3L : 

{ /* Ieee/vector.scm 304 */
 long BgL_sendz00_850;
{ /* Ieee/vector.scm 304 */
 obj_t BgL_vectorz00_1373;
if(
VECTORP(BgL_sourcez00_846))
{ /* Ieee/vector.scm 304 */
BgL_vectorz00_1373 = BgL_sourcez00_846; }  else 
{ 
 obj_t BgL_auxz00_2539;
BgL_auxz00_2539 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13335L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_sourcez00_846); 
FAILURE(BgL_auxz00_2539,BFALSE,BFALSE);} 
BgL_sendz00_850 = 
VECTOR_LENGTH(BgL_vectorz00_1373); } 
{ /* Ieee/vector.scm 303 */

{ /* Ieee/vector.scm 303 */
 long BgL_auxz00_2551; obj_t BgL_auxz00_2544;
{ /* Ieee/vector.scm 303 */
 obj_t BgL_tmpz00_2552;
if(
INTEGERP(BgL_g1060z00_845))
{ /* Ieee/vector.scm 303 */
BgL_tmpz00_2552 = BgL_g1060z00_845
; }  else 
{ 
 obj_t BgL_auxz00_2555;
BgL_auxz00_2555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13245L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_g1060z00_845); 
FAILURE(BgL_auxz00_2555,BFALSE,BFALSE);} 
BgL_auxz00_2551 = 
(long)CINT(BgL_tmpz00_2552); } 
if(
VECTORP(BgL_g1059z00_844))
{ /* Ieee/vector.scm 303 */
BgL_auxz00_2544 = BgL_g1059z00_844
; }  else 
{ 
 obj_t BgL_auxz00_2547;
BgL_auxz00_2547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13245L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_g1059z00_844); 
FAILURE(BgL_auxz00_2547,BFALSE,BFALSE);} 
return 
BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2544, BgL_auxz00_2551, BgL_sourcez00_846, 
BINT(0L), 
BINT(BgL_sendz00_850));} } } break;case 4L : 

{ /* Ieee/vector.scm 303 */
 obj_t BgL_sstartz00_851;
BgL_sstartz00_851 = 
VECTOR_REF(BgL_opt1057z00_43,3L); 
{ /* Ieee/vector.scm 304 */
 long BgL_sendz00_852;
{ /* Ieee/vector.scm 304 */
 obj_t BgL_vectorz00_1374;
if(
VECTORP(BgL_sourcez00_846))
{ /* Ieee/vector.scm 304 */
BgL_vectorz00_1374 = BgL_sourcez00_846; }  else 
{ 
 obj_t BgL_auxz00_2566;
BgL_auxz00_2566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13335L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_sourcez00_846); 
FAILURE(BgL_auxz00_2566,BFALSE,BFALSE);} 
BgL_sendz00_852 = 
VECTOR_LENGTH(BgL_vectorz00_1374); } 
{ /* Ieee/vector.scm 303 */

{ /* Ieee/vector.scm 303 */
 long BgL_auxz00_2578; obj_t BgL_auxz00_2571;
{ /* Ieee/vector.scm 303 */
 obj_t BgL_tmpz00_2579;
if(
INTEGERP(BgL_g1060z00_845))
{ /* Ieee/vector.scm 303 */
BgL_tmpz00_2579 = BgL_g1060z00_845
; }  else 
{ 
 obj_t BgL_auxz00_2582;
BgL_auxz00_2582 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13245L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_g1060z00_845); 
FAILURE(BgL_auxz00_2582,BFALSE,BFALSE);} 
BgL_auxz00_2578 = 
(long)CINT(BgL_tmpz00_2579); } 
if(
VECTORP(BgL_g1059z00_844))
{ /* Ieee/vector.scm 303 */
BgL_auxz00_2571 = BgL_g1059z00_844
; }  else 
{ 
 obj_t BgL_auxz00_2574;
BgL_auxz00_2574 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13245L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_g1059z00_844); 
FAILURE(BgL_auxz00_2574,BFALSE,BFALSE);} 
return 
BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2571, BgL_auxz00_2578, BgL_sourcez00_846, BgL_sstartz00_851, 
BINT(BgL_sendz00_852));} } } } break;case 5L : 

{ /* Ieee/vector.scm 303 */
 obj_t BgL_sstartz00_853;
BgL_sstartz00_853 = 
VECTOR_REF(BgL_opt1057z00_43,3L); 
{ /* Ieee/vector.scm 303 */
 obj_t BgL_sendz00_854;
BgL_sendz00_854 = 
VECTOR_REF(BgL_opt1057z00_43,4L); 
{ /* Ieee/vector.scm 303 */

{ /* Ieee/vector.scm 303 */
 long BgL_auxz00_2598; obj_t BgL_auxz00_2591;
{ /* Ieee/vector.scm 303 */
 obj_t BgL_tmpz00_2599;
if(
INTEGERP(BgL_g1060z00_845))
{ /* Ieee/vector.scm 303 */
BgL_tmpz00_2599 = BgL_g1060z00_845
; }  else 
{ 
 obj_t BgL_auxz00_2602;
BgL_auxz00_2602 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13245L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_g1060z00_845); 
FAILURE(BgL_auxz00_2602,BFALSE,BFALSE);} 
BgL_auxz00_2598 = 
(long)CINT(BgL_tmpz00_2599); } 
if(
VECTORP(BgL_g1059z00_844))
{ /* Ieee/vector.scm 303 */
BgL_auxz00_2591 = BgL_g1059z00_844
; }  else 
{ 
 obj_t BgL_auxz00_2594;
BgL_auxz00_2594 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13245L), BGl_string1810z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_g1059z00_844); 
FAILURE(BgL_auxz00_2594,BFALSE,BFALSE);} 
return 
BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_2591, BgL_auxz00_2598, BgL_sourcez00_846, BgL_sstartz00_853, BgL_sendz00_854);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol1807z00zz__r4_vectors_6_8z00, BGl_string1809z00zz__r4_vectors_6_8z00, 
BINT(
VECTOR_LENGTH(BgL_opt1057z00_43)));} } } } 

}



/* vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t BgL_targetz00_38, long BgL_tstartz00_39, obj_t BgL_sourcez00_40, obj_t BgL_sstartz00_41, obj_t BgL_sendz00_42)
{
{ /* Ieee/vector.scm 303 */
{ /* Ieee/vector.scm 305 */
 obj_t BgL_endz00_856;
{ /* Ieee/vector.scm 305 */
 long BgL_bz00_865;
{ /* Ieee/vector.scm 305 */
 obj_t BgL_vectorz00_1375;
if(
VECTORP(BgL_sourcez00_40))
{ /* Ieee/vector.scm 305 */
BgL_vectorz00_1375 = BgL_sourcez00_40; }  else 
{ 
 obj_t BgL_auxz00_2615;
BgL_auxz00_2615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13387L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_sourcez00_40); 
FAILURE(BgL_auxz00_2615,BFALSE,BFALSE);} 
BgL_bz00_865 = 
VECTOR_LENGTH(BgL_vectorz00_1375); } 
{ /* Ieee/vector.scm 305 */
 bool_t BgL_test2151z00_2620;
{ /* Ieee/vector.scm 305 */
 long BgL_n1z00_1376;
{ /* Ieee/vector.scm 305 */
 obj_t BgL_tmpz00_2621;
if(
INTEGERP(BgL_sendz00_42))
{ /* Ieee/vector.scm 305 */
BgL_tmpz00_2621 = BgL_sendz00_42
; }  else 
{ 
 obj_t BgL_auxz00_2624;
BgL_auxz00_2624 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13372L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_sendz00_42); 
FAILURE(BgL_auxz00_2624,BFALSE,BFALSE);} 
BgL_n1z00_1376 = 
(long)CINT(BgL_tmpz00_2621); } 
BgL_test2151z00_2620 = 
(BgL_n1z00_1376<BgL_bz00_865); } 
if(BgL_test2151z00_2620)
{ /* Ieee/vector.scm 305 */
BgL_endz00_856 = BgL_sendz00_42; }  else 
{ /* Ieee/vector.scm 305 */
BgL_endz00_856 = 
BINT(BgL_bz00_865); } } } 
{ /* Ieee/vector.scm 305 */
 long BgL_countz00_857;
{ /* Ieee/vector.scm 306 */
 long BgL_za71za7_1378; long BgL_za72za7_1379;
{ /* Ieee/vector.scm 306 */
 obj_t BgL_tmpz00_2631;
if(
INTEGERP(BgL_endz00_856))
{ /* Ieee/vector.scm 306 */
BgL_tmpz00_2631 = BgL_endz00_856
; }  else 
{ 
 obj_t BgL_auxz00_2634;
BgL_auxz00_2634 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13419L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_endz00_856); 
FAILURE(BgL_auxz00_2634,BFALSE,BFALSE);} 
BgL_za71za7_1378 = 
(long)CINT(BgL_tmpz00_2631); } 
{ /* Ieee/vector.scm 306 */
 obj_t BgL_tmpz00_2639;
if(
INTEGERP(BgL_sstartz00_41))
{ /* Ieee/vector.scm 306 */
BgL_tmpz00_2639 = BgL_sstartz00_41
; }  else 
{ 
 obj_t BgL_auxz00_2642;
BgL_auxz00_2642 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13423L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_sstartz00_41); 
FAILURE(BgL_auxz00_2642,BFALSE,BFALSE);} 
BgL_za72za7_1379 = 
(long)CINT(BgL_tmpz00_2639); } 
BgL_countz00_857 = 
(BgL_za71za7_1378-BgL_za72za7_1379); } 
{ /* Ieee/vector.scm 306 */
 long BgL_tendz00_858;
{ /* Ieee/vector.scm 307 */
 long BgL_az00_861;
BgL_az00_861 = 
(BgL_tstartz00_39+BgL_countz00_857); 
if(
(BgL_az00_861<
VECTOR_LENGTH(BgL_targetz00_38)))
{ /* Ieee/vector.scm 307 */
BgL_tendz00_858 = BgL_az00_861; }  else 
{ /* Ieee/vector.scm 307 */
BgL_tendz00_858 = 
VECTOR_LENGTH(BgL_targetz00_38); } } 
{ /* Ieee/vector.scm 307 */

if(
(BgL_targetz00_38==BgL_sourcez00_40))
{ /* Ieee/vector.scm 311 */
 long BgL_arg1190z00_859;
BgL_arg1190z00_859 = 
(BgL_tendz00_858-BgL_tstartz00_39); 
{ /* Ieee/vector.scm 311 */
 long BgL_auxz00_2663; obj_t BgL_tmpz00_2656;
{ /* Ieee/vector.scm 311 */
 obj_t BgL_tmpz00_2664;
if(
INTEGERP(BgL_sstartz00_41))
{ /* Ieee/vector.scm 311 */
BgL_tmpz00_2664 = BgL_sstartz00_41
; }  else 
{ 
 obj_t BgL_auxz00_2667;
BgL_auxz00_2667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13609L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_sstartz00_41); 
FAILURE(BgL_auxz00_2667,BFALSE,BFALSE);} 
BgL_auxz00_2663 = 
(long)CINT(BgL_tmpz00_2664); } 
if(
VECTORP(BgL_sourcez00_40))
{ /* Ieee/vector.scm 311 */
BgL_tmpz00_2656 = BgL_sourcez00_40
; }  else 
{ 
 obj_t BgL_auxz00_2659;
BgL_auxz00_2659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13595L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_sourcez00_40); 
FAILURE(BgL_auxz00_2659,BFALSE,BFALSE);} 
return 
BGL_VECTOR_BLIT_OVERLAP(BgL_targetz00_38, BgL_tmpz00_2656, BgL_tstartz00_39, BgL_auxz00_2663, BgL_arg1190z00_859);} }  else 
{ /* Ieee/vector.scm 312 */
 long BgL_arg1191z00_860;
BgL_arg1191z00_860 = 
(BgL_tendz00_858-BgL_tstartz00_39); 
{ /* Ieee/vector.scm 312 */
 long BgL_auxz00_2681; obj_t BgL_tmpz00_2674;
{ /* Ieee/vector.scm 312 */
 obj_t BgL_tmpz00_2682;
if(
INTEGERP(BgL_sstartz00_41))
{ /* Ieee/vector.scm 312 */
BgL_tmpz00_2682 = BgL_sstartz00_41
; }  else 
{ 
 obj_t BgL_auxz00_2685;
BgL_auxz00_2685 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13689L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_sstartz00_41); 
FAILURE(BgL_auxz00_2685,BFALSE,BFALSE);} 
BgL_auxz00_2681 = 
(long)CINT(BgL_tmpz00_2682); } 
if(
VECTORP(BgL_sourcez00_40))
{ /* Ieee/vector.scm 312 */
BgL_tmpz00_2674 = BgL_sourcez00_40
; }  else 
{ 
 obj_t BgL_auxz00_2677;
BgL_auxz00_2677 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(13675L), BGl_string1808z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_sourcez00_40); 
FAILURE(BgL_auxz00_2677,BFALSE,BFALSE);} 
return 
BGL_VECTOR_BLIT_NO_OVERLAP(BgL_targetz00_38, BgL_tmpz00_2674, BgL_tstartz00_39, BgL_auxz00_2681, BgL_arg1191z00_860);} } } } } } } 

}



/* vector-append */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(obj_t BgL_vecz00_45, obj_t BgL_argsz00_46)
{
{ /* Ieee/vector.scm 331 */
{ 
 long BgL_lenz00_869; obj_t BgL_vectsz00_870;
BgL_lenz00_869 = 
VECTOR_LENGTH(BgL_vecz00_45); 
BgL_vectsz00_870 = BgL_argsz00_46; 
BgL_zc3z04anonymousza31194ze3z87_871:
if(
NULLP(BgL_vectsz00_870))
{ /* Ieee/vector.scm 335 */
 obj_t BgL_resz00_873;
BgL_resz00_873 = 
make_vector(BgL_lenz00_869, BUNSPEC); 
{ /* Ieee/vector.scm 112 */

BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(BgL_resz00_873, 0L, BgL_vecz00_45, 
BINT(0L), 
BINT(
VECTOR_LENGTH(BgL_vecz00_45))); } 
{ 
 long BgL_iz00_881; obj_t BgL_vectsz00_882;
BgL_iz00_881 = 
VECTOR_LENGTH(BgL_vecz00_45); 
BgL_vectsz00_882 = BgL_argsz00_46; 
BgL_zc3z04anonymousza31196ze3z87_883:
if(
NULLP(BgL_vectsz00_882))
{ /* Ieee/vector.scm 339 */
return BgL_resz00_873;}  else 
{ /* Ieee/vector.scm 341 */
 obj_t BgL_vecz00_885;
{ /* Ieee/vector.scm 341 */
 obj_t BgL_pairz00_1393;
if(
PAIRP(BgL_vectsz00_882))
{ /* Ieee/vector.scm 341 */
BgL_pairz00_1393 = BgL_vectsz00_882; }  else 
{ 
 obj_t BgL_auxz00_2702;
BgL_auxz00_2702 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14743L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_vectsz00_882); 
FAILURE(BgL_auxz00_2702,BFALSE,BFALSE);} 
BgL_vecz00_885 = 
CAR(BgL_pairz00_1393); } 
{ /* Ieee/vector.scm 112 */
 long BgL_sendz00_890;
{ /* Ieee/vector.scm 112 */
 obj_t BgL_vectorz00_1394;
if(
VECTORP(BgL_vecz00_885))
{ /* Ieee/vector.scm 112 */
BgL_vectorz00_1394 = BgL_vecz00_885; }  else 
{ 
 obj_t BgL_auxz00_2709;
BgL_auxz00_2709 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(5224L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_885); 
FAILURE(BgL_auxz00_2709,BFALSE,BFALSE);} 
BgL_sendz00_890 = 
VECTOR_LENGTH(BgL_vectorz00_1394); } 
{ /* Ieee/vector.scm 112 */

BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(BgL_resz00_873, BgL_iz00_881, BgL_vecz00_885, 
BINT(0L), 
BINT(BgL_sendz00_890)); } } 
{ /* Ieee/vector.scm 343 */
 long BgL_arg1198z00_891; obj_t BgL_arg1199z00_892;
{ /* Ieee/vector.scm 343 */
 long BgL_arg1200z00_893;
{ /* Ieee/vector.scm 343 */
 obj_t BgL_vectorz00_1395;
if(
VECTORP(BgL_vecz00_885))
{ /* Ieee/vector.scm 343 */
BgL_vectorz00_1395 = BgL_vecz00_885; }  else 
{ 
 obj_t BgL_auxz00_2719;
BgL_auxz00_2719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14823L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_885); 
FAILURE(BgL_auxz00_2719,BFALSE,BFALSE);} 
BgL_arg1200z00_893 = 
VECTOR_LENGTH(BgL_vectorz00_1395); } 
BgL_arg1198z00_891 = 
(BgL_iz00_881+BgL_arg1200z00_893); } 
{ /* Ieee/vector.scm 343 */
 obj_t BgL_pairz00_1398;
if(
PAIRP(BgL_vectsz00_882))
{ /* Ieee/vector.scm 343 */
BgL_pairz00_1398 = BgL_vectsz00_882; }  else 
{ 
 obj_t BgL_auxz00_2727;
BgL_auxz00_2727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14834L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_vectsz00_882); 
FAILURE(BgL_auxz00_2727,BFALSE,BFALSE);} 
BgL_arg1199z00_892 = 
CDR(BgL_pairz00_1398); } 
{ 
 obj_t BgL_vectsz00_2733; long BgL_iz00_2732;
BgL_iz00_2732 = BgL_arg1198z00_891; 
BgL_vectsz00_2733 = BgL_arg1199z00_892; 
BgL_vectsz00_882 = BgL_vectsz00_2733; 
BgL_iz00_881 = BgL_iz00_2732; 
goto BgL_zc3z04anonymousza31196ze3z87_883;} } } } }  else 
{ /* Ieee/vector.scm 344 */
 long BgL_arg1202z00_896; obj_t BgL_arg1203z00_897;
{ /* Ieee/vector.scm 344 */
 long BgL_arg1206z00_898;
{ /* Ieee/vector.scm 344 */
 obj_t BgL_arg1208z00_899;
{ /* Ieee/vector.scm 344 */
 obj_t BgL_pairz00_1399;
if(
PAIRP(BgL_vectsz00_870))
{ /* Ieee/vector.scm 344 */
BgL_pairz00_1399 = BgL_vectsz00_870; }  else 
{ 
 obj_t BgL_auxz00_2737;
BgL_auxz00_2737 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14880L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_vectsz00_870); 
FAILURE(BgL_auxz00_2737,BFALSE,BFALSE);} 
BgL_arg1208z00_899 = 
CAR(BgL_pairz00_1399); } 
{ /* Ieee/vector.scm 344 */
 obj_t BgL_vectorz00_1400;
if(
VECTORP(BgL_arg1208z00_899))
{ /* Ieee/vector.scm 344 */
BgL_vectorz00_1400 = BgL_arg1208z00_899; }  else 
{ 
 obj_t BgL_auxz00_2744;
BgL_auxz00_2744 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14885L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_arg1208z00_899); 
FAILURE(BgL_auxz00_2744,BFALSE,BFALSE);} 
BgL_arg1206z00_898 = 
VECTOR_LENGTH(BgL_vectorz00_1400); } } 
BgL_arg1202z00_896 = 
(BgL_arg1206z00_898+BgL_lenz00_869); } 
{ /* Ieee/vector.scm 344 */
 obj_t BgL_pairz00_1403;
if(
PAIRP(BgL_vectsz00_870))
{ /* Ieee/vector.scm 344 */
BgL_pairz00_1403 = BgL_vectsz00_870; }  else 
{ 
 obj_t BgL_auxz00_2752;
BgL_auxz00_2752 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14898L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1789z00zz__r4_vectors_6_8z00, BgL_vectsz00_870); 
FAILURE(BgL_auxz00_2752,BFALSE,BFALSE);} 
BgL_arg1203z00_897 = 
CDR(BgL_pairz00_1403); } 
{ 
 obj_t BgL_vectsz00_2758; long BgL_lenz00_2757;
BgL_lenz00_2757 = BgL_arg1202z00_896; 
BgL_vectsz00_2758 = BgL_arg1203z00_897; 
BgL_vectsz00_870 = BgL_vectsz00_2758; 
BgL_lenz00_869 = BgL_lenz00_2757; 
goto BgL_zc3z04anonymousza31194ze3z87_871;} } } } 

}



/* &vector-append */
obj_t BGl_z62vectorzd2appendzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1576, obj_t BgL_vecz00_1577, obj_t BgL_argsz00_1578)
{
{ /* Ieee/vector.scm 331 */
{ /* Ieee/vector.scm 332 */
 obj_t BgL_auxz00_2760;
if(
VECTORP(BgL_vecz00_1577))
{ /* Ieee/vector.scm 332 */
BgL_auxz00_2760 = BgL_vecz00_1577
; }  else 
{ 
 obj_t BgL_auxz00_2763;
BgL_auxz00_2763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(14486L), BGl_string1811z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_1577); 
FAILURE(BgL_auxz00_2763,BFALSE,BFALSE);} 
return 
BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(BgL_auxz00_2760, BgL_argsz00_1578);} } 

}



/* sort */
BGL_EXPORTED_DEF obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t BgL_o1z00_47, obj_t BgL_o2z00_48)
{
{ /* Ieee/vector.scm 349 */
if(
PROCEDUREP(BgL_o1z00_47))
{ /* Ieee/vector.scm 350 */
return 
BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(BgL_o2z00_48, BgL_o1z00_47);}  else 
{ /* Ieee/vector.scm 352 */
 obj_t BgL_auxz00_2771;
if(
PROCEDUREP(BgL_o2z00_48))
{ /* Ieee/vector.scm 352 */
BgL_auxz00_2771 = BgL_o2z00_48
; }  else 
{ 
 obj_t BgL_auxz00_2774;
BgL_auxz00_2774 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(15224L), BGl_string1812z00zz__r4_vectors_6_8z00, BGl_string1813z00zz__r4_vectors_6_8z00, BgL_o2z00_48); 
FAILURE(BgL_auxz00_2774,BFALSE,BFALSE);} 
return 
BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(BgL_o1z00_47, BgL_auxz00_2771);} } 

}



/* &sort */
obj_t BGl_z62sortz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1579, obj_t BgL_o1z00_1580, obj_t BgL_o2z00_1581)
{
{ /* Ieee/vector.scm 349 */
return 
BGl_sortz00zz__r4_vectors_6_8z00(BgL_o1z00_1580, BgL_o2z00_1581);} 

}



/* inner-sort */
obj_t BGl_innerzd2sortzd2zz__r4_vectors_6_8z00(obj_t BgL_objz00_49, obj_t BgL_procz00_50)
{
{ /* Ieee/vector.scm 357 */
if(
NULLP(BgL_objz00_49))
{ /* Ieee/vector.scm 359 */
return BgL_objz00_49;}  else 
{ /* Ieee/vector.scm 361 */
 bool_t BgL_test2174z00_2782;
if(
PAIRP(BgL_objz00_49))
{ /* Ieee/vector.scm 361 */
BgL_test2174z00_2782 = 
NULLP(
CDR(BgL_objz00_49))
; }  else 
{ /* Ieee/vector.scm 361 */
BgL_test2174z00_2782 = ((bool_t)0)
; } 
if(BgL_test2174z00_2782)
{ /* Ieee/vector.scm 361 */
return BgL_objz00_49;}  else 
{ /* Ieee/vector.scm 364 */
 obj_t BgL_vecz00_906;
if(
VECTORP(BgL_objz00_49))
{ /* Ieee/vector.scm 366 */
 obj_t BgL_newz00_911;
BgL_newz00_911 = 
create_vector(
VECTOR_LENGTH(BgL_objz00_49)); 
{ /* Ieee/vector.scm 367 */

{ 
 long BgL_iz00_913;
BgL_iz00_913 = 0L; 
BgL_zc3z04anonymousza31216ze3z87_914:
if(
(BgL_iz00_913<
VECTOR_LENGTH(BgL_objz00_49)))
{ /* Ieee/vector.scm 369 */
{ /* Ieee/vector.scm 371 */
 obj_t BgL_arg1218z00_916;
BgL_arg1218z00_916 = 
VECTOR_REF(
((obj_t)BgL_objz00_49),BgL_iz00_913); 
VECTOR_SET(BgL_newz00_911,BgL_iz00_913,BgL_arg1218z00_916); } 
{ 
 long BgL_iz00_2797;
BgL_iz00_2797 = 
(BgL_iz00_913+1L); 
BgL_iz00_913 = BgL_iz00_2797; 
goto BgL_zc3z04anonymousza31216ze3z87_914;} }  else 
{ /* Ieee/vector.scm 369 */((bool_t)0); } } 
BgL_vecz00_906 = BgL_newz00_911; } }  else 
{ /* Ieee/vector.scm 365 */
if(
PAIRP(BgL_objz00_49))
{ /* Ieee/vector.scm 374 */
BgL_vecz00_906 = 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_objz00_49); }  else 
{ /* Ieee/vector.scm 374 */
BgL_vecz00_906 = 
BGl_errorz00zz__errorz00(BGl_string1812z00zz__r4_vectors_6_8z00, BGl_string1814z00zz__r4_vectors_6_8z00, BgL_objz00_49); } } 
{ /* Ieee/vector.scm 380 */
 obj_t BgL_resz00_907;
{ /* Ieee/vector.scm 380 */
 obj_t BgL_tmpz00_2803;
if(
VECTORP(BgL_vecz00_906))
{ /* Ieee/vector.scm 380 */
BgL_tmpz00_2803 = BgL_vecz00_906
; }  else 
{ 
 obj_t BgL_auxz00_2806;
BgL_auxz00_2806 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(16041L), BGl_string1815z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_906); 
FAILURE(BgL_auxz00_2806,BFALSE,BFALSE);} 
BgL_resz00_907 = 
sort_vector(BgL_tmpz00_2803, BgL_procz00_50); } 
if(
PAIRP(BgL_objz00_49))
{ /* Ieee/vector.scm 381 */
return 
BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_resz00_907);}  else 
{ /* Ieee/vector.scm 381 */
return BgL_resz00_907;} } } } } 

}



/* vector-map2! */
obj_t BGl_vectorzd2map2z12zc0zz__r4_vectors_6_8z00(obj_t BgL_procz00_51, obj_t BgL_vdestz00_52, obj_t BgL_vsrcz00_53)
{
{ /* Ieee/vector.scm 388 */
{ /* Ieee/vector.scm 389 */
 long BgL_lenz00_922;
{ /* Ieee/vector.scm 389 */
 obj_t BgL_vectorz00_1414;
if(
VECTORP(BgL_vdestz00_52))
{ /* Ieee/vector.scm 389 */
BgL_vectorz00_1414 = BgL_vdestz00_52; }  else 
{ 
 obj_t BgL_auxz00_2816;
BgL_auxz00_2816 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(16401L), BGl_string1816z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vdestz00_52); 
FAILURE(BgL_auxz00_2816,BFALSE,BFALSE);} 
BgL_lenz00_922 = 
VECTOR_LENGTH(BgL_vectorz00_1414); } 
{ 
 long BgL_iz00_924;
BgL_iz00_924 = 0L; 
BgL_zc3z04anonymousza31222ze3z87_925:
if(
(BgL_iz00_924<BgL_lenz00_922))
{ /* Ieee/vector.scm 391 */
{ /* Ieee/vector.scm 393 */
 obj_t BgL_arg1225z00_927;
{ /* Ieee/vector.scm 393 */
 obj_t BgL_arg1226z00_928;
BgL_arg1226z00_928 = 
VECTOR_REF(BgL_vsrcz00_53,BgL_iz00_924); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_51, 1))
{ /* Ieee/vector.scm 393 */
BgL_arg1225z00_927 = 
BGL_PROCEDURE_CALL1(BgL_procz00_51, BgL_arg1226z00_928); }  else 
{ /* Ieee/vector.scm 393 */
FAILURE(BGl_string1817z00zz__r4_vectors_6_8z00,BGl_list1818z00zz__r4_vectors_6_8z00,BgL_procz00_51);} } 
{ /* Ieee/vector.scm 393 */
 obj_t BgL_vectorz00_1419;
if(
VECTORP(BgL_vdestz00_52))
{ /* Ieee/vector.scm 393 */
BgL_vectorz00_1419 = BgL_vdestz00_52; }  else 
{ 
 obj_t BgL_auxz00_2833;
BgL_auxz00_2833 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(16483L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vdestz00_52); 
FAILURE(BgL_auxz00_2833,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_1419,BgL_iz00_924,BgL_arg1225z00_927); } } 
{ 
 long BgL_iz00_2838;
BgL_iz00_2838 = 
(BgL_iz00_924+1L); 
BgL_iz00_924 = BgL_iz00_2838; 
goto BgL_zc3z04anonymousza31222ze3z87_925;} }  else 
{ /* Ieee/vector.scm 391 */
return BgL_vdestz00_52;} } } } 

}



/* vector-mapN! */
obj_t BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(obj_t BgL_procz00_54, obj_t BgL_vdestz00_55, obj_t BgL_vsrcz00_56, obj_t BgL_vrestz00_57)
{
{ /* Ieee/vector.scm 400 */
{ /* Ieee/vector.scm 401 */
 long BgL_lenz00_931;
{ /* Ieee/vector.scm 401 */
 obj_t BgL_vectorz00_1422;
if(
VECTORP(BgL_vdestz00_55))
{ /* Ieee/vector.scm 401 */
BgL_vectorz00_1422 = BgL_vdestz00_55; }  else 
{ 
 obj_t BgL_auxz00_2842;
BgL_auxz00_2842 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(16855L), BGl_string1825z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vdestz00_55); 
FAILURE(BgL_auxz00_2842,BFALSE,BFALSE);} 
BgL_lenz00_931 = 
VECTOR_LENGTH(BgL_vectorz00_1422); } 
{ 
 long BgL_iz00_933;
BgL_iz00_933 = 0L; 
BgL_zc3z04anonymousza31228ze3z87_934:
if(
(BgL_iz00_933<BgL_lenz00_931))
{ /* Ieee/vector.scm 404 */
 obj_t BgL_argsz00_936;
if(
NULLP(BgL_vrestz00_57))
{ /* Ieee/vector.scm 404 */
BgL_argsz00_936 = BNIL; }  else 
{ /* Ieee/vector.scm 404 */
 obj_t BgL_head1036z00_943;
BgL_head1036z00_943 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1034z00_945; obj_t BgL_tail1037z00_946;
BgL_l1034z00_945 = BgL_vrestz00_57; 
BgL_tail1037z00_946 = BgL_head1036z00_943; 
BgL_zc3z04anonymousza31234ze3z87_947:
if(
PAIRP(BgL_l1034z00_945))
{ /* Ieee/vector.scm 404 */
 obj_t BgL_newtail1038z00_949;
{ /* Ieee/vector.scm 404 */
 obj_t BgL_arg1238z00_951;
{ /* Ieee/vector.scm 404 */
 obj_t BgL_vz00_952;
BgL_vz00_952 = 
CAR(BgL_l1034z00_945); 
{ /* Ieee/vector.scm 404 */
 obj_t BgL_vectorz00_1426;
if(
VECTORP(BgL_vz00_952))
{ /* Ieee/vector.scm 404 */
BgL_vectorz00_1426 = BgL_vz00_952; }  else 
{ 
 obj_t BgL_auxz00_2857;
BgL_auxz00_2857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(16954L), BGl_string1826z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vz00_952); 
FAILURE(BgL_auxz00_2857,BFALSE,BFALSE);} 
{ /* Ieee/vector.scm 161 */
 bool_t BgL_test2190z00_2861;
{ /* Ieee/vector.scm 161 */
 long BgL_tmpz00_2862;
BgL_tmpz00_2862 = 
VECTOR_LENGTH(BgL_vectorz00_1426); 
BgL_test2190z00_2861 = 
BOUND_CHECK(BgL_iz00_933, BgL_tmpz00_2862); } 
if(BgL_test2190z00_2861)
{ /* Ieee/vector.scm 161 */
BgL_arg1238z00_951 = 
VECTOR_REF(BgL_vectorz00_1426,BgL_iz00_933); }  else 
{ 
 obj_t BgL_auxz00_2866;
BgL_auxz00_2866 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1781z00zz__r4_vectors_6_8z00, BgL_vectorz00_1426, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_1426)), 
(int)(BgL_iz00_933)); 
FAILURE(BgL_auxz00_2866,BFALSE,BFALSE);} } } } 
BgL_newtail1038z00_949 = 
MAKE_YOUNG_PAIR(BgL_arg1238z00_951, BNIL); } 
SET_CDR(BgL_tail1037z00_946, BgL_newtail1038z00_949); 
{ 
 obj_t BgL_tail1037z00_2877; obj_t BgL_l1034z00_2875;
BgL_l1034z00_2875 = 
CDR(BgL_l1034z00_945); 
BgL_tail1037z00_2877 = BgL_newtail1038z00_949; 
BgL_tail1037z00_946 = BgL_tail1037z00_2877; 
BgL_l1034z00_945 = BgL_l1034z00_2875; 
goto BgL_zc3z04anonymousza31234ze3z87_947;} }  else 
{ /* Ieee/vector.scm 404 */
if(
NULLP(BgL_l1034z00_945))
{ /* Ieee/vector.scm 404 */
BgL_argsz00_936 = 
CDR(BgL_head1036z00_943); }  else 
{ /* Ieee/vector.scm 404 */
BgL_argsz00_936 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1827z00zz__r4_vectors_6_8z00, BGl_string1828z00zz__r4_vectors_6_8z00, BgL_l1034z00_945, BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(16925L)); } } } } 
{ /* Ieee/vector.scm 404 */
 obj_t BgL_nvalz00_937;
{ /* Ieee/vector.scm 405 */
 obj_t BgL_valz00_1759;
{ /* Ieee/vector.scm 405 */
 obj_t BgL_arg1231z00_939;
{ /* Ieee/vector.scm 161 */
 bool_t BgL_test2192z00_2883;
{ /* Ieee/vector.scm 161 */
 long BgL_tmpz00_2884;
BgL_tmpz00_2884 = 
VECTOR_LENGTH(BgL_vsrcz00_56); 
BgL_test2192z00_2883 = 
BOUND_CHECK(BgL_iz00_933, BgL_tmpz00_2884); } 
if(BgL_test2192z00_2883)
{ /* Ieee/vector.scm 161 */
BgL_arg1231z00_939 = 
VECTOR_REF(BgL_vsrcz00_56,BgL_iz00_933); }  else 
{ 
 obj_t BgL_auxz00_2888;
BgL_auxz00_2888 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1781z00zz__r4_vectors_6_8z00, BgL_vsrcz00_56, 
(int)(
VECTOR_LENGTH(BgL_vsrcz00_56)), 
(int)(BgL_iz00_933)); 
FAILURE(BgL_auxz00_2888,BFALSE,BFALSE);} } 
{ /* Ieee/vector.scm 405 */
 obj_t BgL_list1232z00_940;
BgL_list1232z00_940 = 
MAKE_YOUNG_PAIR(BgL_argsz00_936, BNIL); 
BgL_valz00_1759 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_arg1231z00_939, BgL_list1232z00_940); } } 
{ /* Ieee/vector.scm 405 */
 int BgL_len1735z00_1760;
BgL_len1735z00_1760 = 
(int)(
bgl_list_length(BgL_valz00_1759)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_54, BgL_len1735z00_1760))
{ /* Ieee/vector.scm 405 */
BgL_nvalz00_937 = 
apply(BgL_procz00_54, BgL_valz00_1759); }  else 
{ /* Ieee/vector.scm 405 */
FAILURE(BGl_symbol1829z00zz__r4_vectors_6_8z00,BGl_string1830z00zz__r4_vectors_6_8z00,BGl_list1831z00zz__r4_vectors_6_8z00);} } } 
{ /* Ieee/vector.scm 405 */

{ /* Ieee/vector.scm 406 */
 obj_t BgL_vectorz00_1433;
if(
VECTORP(BgL_vdestz00_55))
{ /* Ieee/vector.scm 406 */
BgL_vectorz00_1433 = BgL_vdestz00_55; }  else 
{ 
 obj_t BgL_auxz00_2906;
BgL_auxz00_2906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17038L), BGl_string1788z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vdestz00_55); 
FAILURE(BgL_auxz00_2906,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_1433,BgL_iz00_933,BgL_nvalz00_937); } 
{ 
 long BgL_iz00_2911;
BgL_iz00_2911 = 
(BgL_iz00_933+1L); 
BgL_iz00_933 = BgL_iz00_2911; 
goto BgL_zc3z04anonymousza31228ze3z87_934;} } } }  else 
{ /* Ieee/vector.scm 403 */
return BgL_vdestz00_55;} } } } 

}



/* vector-map */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2mapzd2zz__r4_vectors_6_8z00(obj_t BgL_procz00_58, obj_t BgL_vz00_59, obj_t BgL_restz00_60)
{
{ /* Ieee/vector.scm 413 */
{ /* Ieee/vector.scm 414 */
 obj_t BgL_nvz00_957;
BgL_nvz00_957 = 
create_vector(
VECTOR_LENGTH(BgL_vz00_59)); 
{ /* Ieee/vector.scm 415 */

if(
NULLP(BgL_restz00_60))
{ /* Ieee/vector.scm 418 */
 obj_t BgL_aux1738z00_1763;
BgL_aux1738z00_1763 = 
BGl_vectorzd2map2z12zc0zz__r4_vectors_6_8z00(BgL_procz00_58, BgL_nvz00_957, BgL_vz00_59); 
if(
VECTORP(BgL_aux1738z00_1763))
{ /* Ieee/vector.scm 418 */
return BgL_aux1738z00_1763;}  else 
{ 
 obj_t BgL_auxz00_2920;
BgL_auxz00_2920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17441L), BGl_string1907z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1738z00_1763); 
FAILURE(BgL_auxz00_2920,BFALSE,BFALSE);} }  else 
{ /* Ieee/vector.scm 419 */
 bool_t BgL_test2197z00_2924;
{ /* Ieee/vector.scm 419 */
 obj_t BgL_zc3z04anonymousza31254ze3z87_1582;
BgL_zc3z04anonymousza31254ze3z87_1582 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31254ze3ze5zz__r4_vectors_6_8z00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31254ze3z87_1582, 
(int)(0L), 
BINT(
VECTOR_LENGTH(BgL_vz00_59))); 
BgL_test2197z00_2924 = 
CBOOL(
BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BgL_zc3z04anonymousza31254ze3z87_1582, BNIL)); } 
if(BgL_test2197z00_2924)
{ /* Ieee/vector.scm 420 */
 obj_t BgL_aux1740z00_1765;
BgL_aux1740z00_1765 = 
BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(BgL_procz00_58, BgL_nvz00_957, BgL_vz00_59, BgL_restz00_60); 
if(
VECTORP(BgL_aux1740z00_1765))
{ /* Ieee/vector.scm 420 */
return BgL_aux1740z00_1765;}  else 
{ 
 obj_t BgL_auxz00_2937;
BgL_auxz00_2937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17540L), BGl_string1907z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1740z00_1765); 
FAILURE(BgL_auxz00_2937,BFALSE,BFALSE);} }  else 
{ /* Ieee/vector.scm 422 */
 obj_t BgL_aux1742z00_1767;
BgL_aux1742z00_1767 = 
BGl_errorz00zz__errorz00(BGl_string1907z00zz__r4_vectors_6_8z00, BGl_string1908z00zz__r4_vectors_6_8z00, BgL_restz00_60); 
if(
VECTORP(BgL_aux1742z00_1767))
{ /* Ieee/vector.scm 422 */
return BgL_aux1742z00_1767;}  else 
{ 
 obj_t BgL_auxz00_2944;
BgL_auxz00_2944 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17582L), BGl_string1907z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1742z00_1767); 
FAILURE(BgL_auxz00_2944,BFALSE,BFALSE);} } } } } } 

}



/* &vector-map */
obj_t BGl_z62vectorzd2mapzb0zz__r4_vectors_6_8z00(obj_t BgL_envz00_1583, obj_t BgL_procz00_1584, obj_t BgL_vz00_1585, obj_t BgL_restz00_1586)
{
{ /* Ieee/vector.scm 413 */
{ /* Ieee/vector.scm 414 */
 obj_t BgL_auxz00_2955; obj_t BgL_auxz00_2948;
if(
VECTORP(BgL_vz00_1585))
{ /* Ieee/vector.scm 414 */
BgL_auxz00_2955 = BgL_vz00_1585
; }  else 
{ 
 obj_t BgL_auxz00_2958;
BgL_auxz00_2958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17349L), BGl_string1909z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vz00_1585); 
FAILURE(BgL_auxz00_2958,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_procz00_1584))
{ /* Ieee/vector.scm 414 */
BgL_auxz00_2948 = BgL_procz00_1584
; }  else 
{ 
 obj_t BgL_auxz00_2951;
BgL_auxz00_2951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17349L), BGl_string1909z00zz__r4_vectors_6_8z00, BGl_string1813z00zz__r4_vectors_6_8z00, BgL_procz00_1584); 
FAILURE(BgL_auxz00_2951,BFALSE,BFALSE);} 
return 
BGl_vectorzd2mapzd2zz__r4_vectors_6_8z00(BgL_auxz00_2948, BgL_auxz00_2955, BgL_restz00_1586);} } 

}



/* &<@anonymous:1254> */
obj_t BGl_z62zc3z04anonymousza31254ze3ze5zz__r4_vectors_6_8z00(obj_t BgL_envz00_1587, obj_t BgL_vz00_1589)
{
{ /* Ieee/vector.scm 419 */
{ /* Ieee/vector.scm 419 */
 long BgL_lenz00_1588;
{ /* Ieee/vector.scm 419 */
 obj_t BgL_tmpz00_2963;
{ /* Ieee/vector.scm 419 */
 obj_t BgL_aux1748z00_1773;
BgL_aux1748z00_1773 = 
PROCEDURE_REF(BgL_envz00_1587, 
(int)(0L)); 
if(
INTEGERP(BgL_aux1748z00_1773))
{ /* Ieee/vector.scm 419 */
BgL_tmpz00_2963 = BgL_aux1748z00_1773
; }  else 
{ 
 obj_t BgL_auxz00_2968;
BgL_auxz00_2968 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17494L), BGl_string1910z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_aux1748z00_1773); 
FAILURE(BgL_auxz00_2968,BFALSE,BFALSE);} } 
BgL_lenz00_1588 = 
(long)CINT(BgL_tmpz00_2963); } 
{ /* Ieee/vector.scm 419 */
 bool_t BgL_tmpz00_2973;
if(
VECTORP(BgL_vz00_1589))
{ /* Ieee/vector.scm 419 */
BgL_tmpz00_2973 = 
(
VECTOR_LENGTH(BgL_vz00_1589)==BgL_lenz00_1588)
; }  else 
{ /* Ieee/vector.scm 419 */
BgL_tmpz00_2973 = ((bool_t)0)
; } 
return 
BBOOL(BgL_tmpz00_2973);} } } 

}



/* vector-map! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2mapz12zc0zz__r4_vectors_6_8z00(obj_t BgL_procz00_61, obj_t BgL_vz00_62, obj_t BgL_restz00_63)
{
{ /* Ieee/vector.scm 427 */
if(
NULLP(BgL_restz00_63))
{ /* Ieee/vector.scm 431 */
 obj_t BgL_aux1749z00_1774;
BgL_aux1749z00_1774 = 
BGl_vectorzd2map2z12zc0zz__r4_vectors_6_8z00(BgL_procz00_61, BgL_vz00_62, BgL_vz00_62); 
if(
VECTORP(BgL_aux1749z00_1774))
{ /* Ieee/vector.scm 431 */
return BgL_aux1749z00_1774;}  else 
{ 
 obj_t BgL_auxz00_2984;
BgL_auxz00_2984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17956L), BGl_string1911z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1749z00_1774); 
FAILURE(BgL_auxz00_2984,BFALSE,BFALSE);} }  else 
{ /* Ieee/vector.scm 432 */
 bool_t BgL_test2206z00_2988;
{ /* Ieee/vector.scm 432 */
 obj_t BgL_zc3z04anonymousza31306ze3z87_1590;
BgL_zc3z04anonymousza31306ze3z87_1590 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31306ze3ze5zz__r4_vectors_6_8z00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31306ze3z87_1590, 
(int)(0L), 
BINT(
VECTOR_LENGTH(BgL_vz00_62))); 
BgL_test2206z00_2988 = 
CBOOL(
BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BgL_zc3z04anonymousza31306ze3z87_1590, BNIL)); } 
if(BgL_test2206z00_2988)
{ /* Ieee/vector.scm 433 */
 obj_t BgL_aux1751z00_1776;
BgL_aux1751z00_1776 = 
BGl_vectorzd2mapNz12zc0zz__r4_vectors_6_8z00(BgL_procz00_61, BgL_vz00_62, BgL_vz00_62, BgL_restz00_63); 
if(
VECTORP(BgL_aux1751z00_1776))
{ /* Ieee/vector.scm 433 */
return BgL_aux1751z00_1776;}  else 
{ 
 obj_t BgL_auxz00_3001;
BgL_auxz00_3001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(18054L), BGl_string1911z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1751z00_1776); 
FAILURE(BgL_auxz00_3001,BFALSE,BFALSE);} }  else 
{ /* Ieee/vector.scm 435 */
 obj_t BgL_aux1753z00_1778;
BgL_aux1753z00_1778 = 
BGl_errorz00zz__errorz00(BGl_string1911z00zz__r4_vectors_6_8z00, BGl_string1908z00zz__r4_vectors_6_8z00, BgL_restz00_63); 
if(
VECTORP(BgL_aux1753z00_1778))
{ /* Ieee/vector.scm 435 */
return BgL_aux1753z00_1778;}  else 
{ 
 obj_t BgL_auxz00_3008;
BgL_auxz00_3008 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(18095L), BGl_string1911z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_aux1753z00_1778); 
FAILURE(BgL_auxz00_3008,BFALSE,BFALSE);} } } } 

}



/* &vector-map! */
obj_t BGl_z62vectorzd2mapz12za2zz__r4_vectors_6_8z00(obj_t BgL_envz00_1591, obj_t BgL_procz00_1592, obj_t BgL_vz00_1593, obj_t BgL_restz00_1594)
{
{ /* Ieee/vector.scm 427 */
{ /* Ieee/vector.scm 428 */
 obj_t BgL_auxz00_3019; obj_t BgL_auxz00_3012;
if(
VECTORP(BgL_vz00_1593))
{ /* Ieee/vector.scm 428 */
BgL_auxz00_3019 = BgL_vz00_1593
; }  else 
{ 
 obj_t BgL_auxz00_3022;
BgL_auxz00_3022 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17894L), BGl_string1912z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vz00_1593); 
FAILURE(BgL_auxz00_3022,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_procz00_1592))
{ /* Ieee/vector.scm 428 */
BgL_auxz00_3012 = BgL_procz00_1592
; }  else 
{ 
 obj_t BgL_auxz00_3015;
BgL_auxz00_3015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(17894L), BGl_string1912z00zz__r4_vectors_6_8z00, BGl_string1813z00zz__r4_vectors_6_8z00, BgL_procz00_1592); 
FAILURE(BgL_auxz00_3015,BFALSE,BFALSE);} 
return 
BGl_vectorzd2mapz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_3012, BgL_auxz00_3019, BgL_restz00_1594);} } 

}



/* &<@anonymous:1306> */
obj_t BGl_z62zc3z04anonymousza31306ze3ze5zz__r4_vectors_6_8z00(obj_t BgL_envz00_1595, obj_t BgL_vz00_1597)
{
{ /* Ieee/vector.scm 432 */
{ /* Ieee/vector.scm 432 */
 long BgL_lenz00_1596;
{ /* Ieee/vector.scm 432 */
 obj_t BgL_tmpz00_3027;
{ /* Ieee/vector.scm 432 */
 obj_t BgL_aux1759z00_1784;
BgL_aux1759z00_1784 = 
PROCEDURE_REF(BgL_envz00_1595, 
(int)(0L)); 
if(
INTEGERP(BgL_aux1759z00_1784))
{ /* Ieee/vector.scm 432 */
BgL_tmpz00_3027 = BgL_aux1759z00_1784
; }  else 
{ 
 obj_t BgL_auxz00_3032;
BgL_auxz00_3032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(18008L), BGl_string1913z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_aux1759z00_1784); 
FAILURE(BgL_auxz00_3032,BFALSE,BFALSE);} } 
BgL_lenz00_1596 = 
(long)CINT(BgL_tmpz00_3027); } 
{ /* Ieee/vector.scm 432 */
 bool_t BgL_tmpz00_3037;
if(
VECTORP(BgL_vz00_1597))
{ /* Ieee/vector.scm 432 */
BgL_tmpz00_3037 = 
(
VECTOR_LENGTH(BgL_vz00_1597)==BgL_lenz00_1596)
; }  else 
{ /* Ieee/vector.scm 432 */
BgL_tmpz00_3037 = ((bool_t)0)
; } 
return 
BBOOL(BgL_tmpz00_3037);} } } 

}



/* vector-for-each2 */
bool_t BGl_vectorzd2forzd2each2z00zz__r4_vectors_6_8z00(obj_t BgL_procz00_64, obj_t BgL_vsrcz00_65)
{
{ /* Ieee/vector.scm 440 */
{ 
 long BgL_iz00_1455;
BgL_iz00_1455 = 0L; 
BgL_loopz00_1454:
if(
(BgL_iz00_1455<
VECTOR_LENGTH(BgL_vsrcz00_65)))
{ /* Ieee/vector.scm 443 */
{ /* Ieee/vector.scm 444 */
 obj_t BgL_arg1310z00_1459;
BgL_arg1310z00_1459 = 
VECTOR_REF(BgL_vsrcz00_65,BgL_iz00_1455); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_64, 1))
{ /* Ieee/vector.scm 444 */
BGL_PROCEDURE_CALL1(BgL_procz00_64, BgL_arg1310z00_1459); }  else 
{ /* Ieee/vector.scm 444 */
FAILURE(BGl_string1817z00zz__r4_vectors_6_8z00,BGl_list1914z00zz__r4_vectors_6_8z00,BgL_procz00_64);} } 
{ 
 long BgL_iz00_3054;
BgL_iz00_3054 = 
(BgL_iz00_1455+1L); 
BgL_iz00_1455 = BgL_iz00_3054; 
goto BgL_loopz00_1454;} }  else 
{ /* Ieee/vector.scm 443 */
return ((bool_t)0);} } } 

}



/* vector-for-eachN */
bool_t BGl_vectorzd2forzd2eachNz00zz__r4_vectors_6_8z00(obj_t BgL_procz00_66, obj_t BgL_vsrcz00_67, obj_t BgL_vrestz00_68)
{
{ /* Ieee/vector.scm 450 */
{ 
 long BgL_iz00_1001;
BgL_iz00_1001 = 0L; 
BgL_zc3z04anonymousza31312ze3z87_1002:
if(
(BgL_iz00_1001<
VECTOR_LENGTH(BgL_vsrcz00_67)))
{ /* Ieee/vector.scm 454 */
 obj_t BgL_argsz00_1004;
if(
NULLP(BgL_vrestz00_68))
{ /* Ieee/vector.scm 454 */
BgL_argsz00_1004 = BNIL; }  else 
{ /* Ieee/vector.scm 454 */
 obj_t BgL_head1041z00_1010;
BgL_head1041z00_1010 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1039z00_1012; obj_t BgL_tail1042z00_1013;
BgL_l1039z00_1012 = BgL_vrestz00_68; 
BgL_tail1042z00_1013 = BgL_head1041z00_1010; 
BgL_zc3z04anonymousza31318ze3z87_1014:
if(
PAIRP(BgL_l1039z00_1012))
{ /* Ieee/vector.scm 454 */
 obj_t BgL_newtail1043z00_1016;
{ /* Ieee/vector.scm 454 */
 obj_t BgL_arg1321z00_1018;
{ /* Ieee/vector.scm 454 */
 obj_t BgL_vz00_1019;
BgL_vz00_1019 = 
CAR(BgL_l1039z00_1012); 
{ /* Ieee/vector.scm 454 */
 obj_t BgL_vectorz00_1468;
if(
VECTORP(BgL_vz00_1019))
{ /* Ieee/vector.scm 454 */
BgL_vectorz00_1468 = BgL_vz00_1019; }  else 
{ 
 obj_t BgL_auxz00_3067;
BgL_auxz00_3067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(18941L), BGl_string1917z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vz00_1019); 
FAILURE(BgL_auxz00_3067,BFALSE,BFALSE);} 
{ /* Ieee/vector.scm 161 */
 bool_t BgL_test2219z00_3071;
{ /* Ieee/vector.scm 161 */
 long BgL_tmpz00_3072;
BgL_tmpz00_3072 = 
VECTOR_LENGTH(BgL_vectorz00_1468); 
BgL_test2219z00_3071 = 
BOUND_CHECK(BgL_iz00_1001, BgL_tmpz00_3072); } 
if(BgL_test2219z00_3071)
{ /* Ieee/vector.scm 161 */
BgL_arg1321z00_1018 = 
VECTOR_REF(BgL_vectorz00_1468,BgL_iz00_1001); }  else 
{ 
 obj_t BgL_auxz00_3076;
BgL_auxz00_3076 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1781z00zz__r4_vectors_6_8z00, BgL_vectorz00_1468, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_1468)), 
(int)(BgL_iz00_1001)); 
FAILURE(BgL_auxz00_3076,BFALSE,BFALSE);} } } } 
BgL_newtail1043z00_1016 = 
MAKE_YOUNG_PAIR(BgL_arg1321z00_1018, BNIL); } 
SET_CDR(BgL_tail1042z00_1013, BgL_newtail1043z00_1016); 
{ 
 obj_t BgL_tail1042z00_3087; obj_t BgL_l1039z00_3085;
BgL_l1039z00_3085 = 
CDR(BgL_l1039z00_1012); 
BgL_tail1042z00_3087 = BgL_newtail1043z00_1016; 
BgL_tail1042z00_1013 = BgL_tail1042z00_3087; 
BgL_l1039z00_1012 = BgL_l1039z00_3085; 
goto BgL_zc3z04anonymousza31318ze3z87_1014;} }  else 
{ /* Ieee/vector.scm 454 */
if(
NULLP(BgL_l1039z00_1012))
{ /* Ieee/vector.scm 454 */
BgL_argsz00_1004 = 
CDR(BgL_head1041z00_1010); }  else 
{ /* Ieee/vector.scm 454 */
BgL_argsz00_1004 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1827z00zz__r4_vectors_6_8z00, BGl_string1828z00zz__r4_vectors_6_8z00, BgL_l1039z00_1012, BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(18912L)); } } } } 
{ /* Ieee/vector.scm 455 */
 obj_t BgL_valz00_1790;
{ /* Ieee/vector.scm 455 */
 obj_t BgL_arg1314z00_1005;
{ /* Ieee/vector.scm 161 */
 bool_t BgL_test2221z00_3093;
{ /* Ieee/vector.scm 161 */
 long BgL_tmpz00_3094;
BgL_tmpz00_3094 = 
VECTOR_LENGTH(BgL_vsrcz00_67); 
BgL_test2221z00_3093 = 
BOUND_CHECK(BgL_iz00_1001, BgL_tmpz00_3094); } 
if(BgL_test2221z00_3093)
{ /* Ieee/vector.scm 161 */
BgL_arg1314z00_1005 = 
VECTOR_REF(BgL_vsrcz00_67,BgL_iz00_1001); }  else 
{ 
 obj_t BgL_auxz00_3098;
BgL_auxz00_3098 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(7562L), BGl_string1781z00zz__r4_vectors_6_8z00, BgL_vsrcz00_67, 
(int)(
VECTOR_LENGTH(BgL_vsrcz00_67)), 
(int)(BgL_iz00_1001)); 
FAILURE(BgL_auxz00_3098,BFALSE,BFALSE);} } 
{ /* Ieee/vector.scm 455 */
 obj_t BgL_list1315z00_1006;
BgL_list1315z00_1006 = 
MAKE_YOUNG_PAIR(BgL_argsz00_1004, BNIL); 
BgL_valz00_1790 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_arg1314z00_1005, BgL_list1315z00_1006); } } 
{ /* Ieee/vector.scm 455 */
 int BgL_len1763z00_1791;
BgL_len1763z00_1791 = 
(int)(
bgl_list_length(BgL_valz00_1790)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_66, BgL_len1763z00_1791))
{ /* Ieee/vector.scm 455 */
apply(BgL_procz00_66, BgL_valz00_1790); }  else 
{ /* Ieee/vector.scm 455 */
FAILURE(BGl_symbol1829z00zz__r4_vectors_6_8z00,BGl_string1830z00zz__r4_vectors_6_8z00,BGl_list1918z00zz__r4_vectors_6_8z00);} } } 
{ 
 long BgL_iz00_3114;
BgL_iz00_3114 = 
(BgL_iz00_1001+1L); 
BgL_iz00_1001 = BgL_iz00_3114; 
goto BgL_zc3z04anonymousza31312ze3z87_1002;} }  else 
{ /* Ieee/vector.scm 453 */
return ((bool_t)0);} } } 

}



/* vector-for-each */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2forzd2eachz00zz__r4_vectors_6_8z00(obj_t BgL_procz00_69, obj_t BgL_vz00_70, obj_t BgL_restz00_71)
{
{ /* Ieee/vector.scm 461 */
if(
NULLP(BgL_restz00_71))
{ /* Ieee/vector.scm 464 */
return 
BBOOL(
BGl_vectorzd2forzd2each2z00zz__r4_vectors_6_8z00(BgL_procz00_69, BgL_vz00_70));}  else 
{ /* Ieee/vector.scm 466 */
 bool_t BgL_test2224z00_3120;
{ /* Ieee/vector.scm 466 */
 obj_t BgL_zc3z04anonymousza31333ze3z87_1598;
BgL_zc3z04anonymousza31333ze3z87_1598 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31333ze3ze5zz__r4_vectors_6_8z00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31333ze3z87_1598, 
(int)(0L), 
BINT(
VECTOR_LENGTH(BgL_vz00_70))); 
BgL_test2224z00_3120 = 
CBOOL(
BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BgL_zc3z04anonymousza31333ze3z87_1598, BNIL)); } 
if(BgL_test2224z00_3120)
{ /* Ieee/vector.scm 466 */
return 
BBOOL(
BGl_vectorzd2forzd2eachNz00zz__r4_vectors_6_8z00(BgL_procz00_69, BgL_vz00_70, BgL_restz00_71));}  else 
{ /* Ieee/vector.scm 466 */
return 
BGl_errorz00zz__errorz00(BGl_string1955z00zz__r4_vectors_6_8z00, BGl_string1908z00zz__r4_vectors_6_8z00, BgL_restz00_71);} } } 

}



/* &vector-for-each */
obj_t BGl_z62vectorzd2forzd2eachz62zz__r4_vectors_6_8z00(obj_t BgL_envz00_1599, obj_t BgL_procz00_1600, obj_t BgL_vz00_1601, obj_t BgL_restz00_1602)
{
{ /* Ieee/vector.scm 461 */
{ /* Ieee/vector.scm 462 */
 obj_t BgL_auxz00_3140; obj_t BgL_auxz00_3133;
if(
VECTORP(BgL_vz00_1601))
{ /* Ieee/vector.scm 462 */
BgL_auxz00_3140 = BgL_vz00_1601
; }  else 
{ 
 obj_t BgL_auxz00_3143;
BgL_auxz00_3143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(19298L), BGl_string1956z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vz00_1601); 
FAILURE(BgL_auxz00_3143,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_procz00_1600))
{ /* Ieee/vector.scm 462 */
BgL_auxz00_3133 = BgL_procz00_1600
; }  else 
{ 
 obj_t BgL_auxz00_3136;
BgL_auxz00_3136 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(19298L), BGl_string1956z00zz__r4_vectors_6_8z00, BGl_string1813z00zz__r4_vectors_6_8z00, BgL_procz00_1600); 
FAILURE(BgL_auxz00_3136,BFALSE,BFALSE);} 
return 
BGl_vectorzd2forzd2eachz00zz__r4_vectors_6_8z00(BgL_auxz00_3133, BgL_auxz00_3140, BgL_restz00_1602);} } 

}



/* &<@anonymous:1333> */
obj_t BGl_z62zc3z04anonymousza31333ze3ze5zz__r4_vectors_6_8z00(obj_t BgL_envz00_1603, obj_t BgL_vz00_1605)
{
{ /* Ieee/vector.scm 466 */
{ /* Ieee/vector.scm 466 */
 long BgL_lenz00_1604;
{ /* Ieee/vector.scm 466 */
 obj_t BgL_tmpz00_3148;
{ /* Ieee/vector.scm 466 */
 obj_t BgL_aux1768z00_1796;
BgL_aux1768z00_1796 = 
PROCEDURE_REF(BgL_envz00_1603, 
(int)(0L)); 
if(
INTEGERP(BgL_aux1768z00_1796))
{ /* Ieee/vector.scm 466 */
BgL_tmpz00_3148 = BgL_aux1768z00_1796
; }  else 
{ 
 obj_t BgL_auxz00_3153;
BgL_auxz00_3153 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(19414L), BGl_string1957z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_aux1768z00_1796); 
FAILURE(BgL_auxz00_3153,BFALSE,BFALSE);} } 
BgL_lenz00_1604 = 
(long)CINT(BgL_tmpz00_3148); } 
{ /* Ieee/vector.scm 466 */
 bool_t BgL_tmpz00_3158;
if(
VECTORP(BgL_vz00_1605))
{ /* Ieee/vector.scm 466 */
BgL_tmpz00_3158 = 
(
VECTOR_LENGTH(BgL_vz00_1605)==BgL_lenz00_1604)
; }  else 
{ /* Ieee/vector.scm 466 */
BgL_tmpz00_3158 = ((bool_t)0)
; } 
return 
BBOOL(BgL_tmpz00_3158);} } } 

}



/* vector-shrink! */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2shrinkz12zc0zz__r4_vectors_6_8z00(obj_t BgL_vecz00_72, long BgL_nlenz00_73)
{
{ /* Ieee/vector.scm 474 */
return 
BGL_VECTOR_SHRINK(BgL_vecz00_72, BgL_nlenz00_73);} 

}



/* &vector-shrink! */
obj_t BGl_z62vectorzd2shrinkz12za2zz__r4_vectors_6_8z00(obj_t BgL_envz00_1606, obj_t BgL_vecz00_1607, obj_t BgL_nlenz00_1608)
{
{ /* Ieee/vector.scm 474 */
{ /* Ieee/vector.scm 475 */
 long BgL_auxz00_3172; obj_t BgL_auxz00_3165;
{ /* Ieee/vector.scm 475 */
 obj_t BgL_tmpz00_3173;
if(
INTEGERP(BgL_nlenz00_1608))
{ /* Ieee/vector.scm 475 */
BgL_tmpz00_3173 = BgL_nlenz00_1608
; }  else 
{ 
 obj_t BgL_auxz00_3176;
BgL_auxz00_3176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(19825L), BGl_string1958z00zz__r4_vectors_6_8z00, BGl_string1777z00zz__r4_vectors_6_8z00, BgL_nlenz00_1608); 
FAILURE(BgL_auxz00_3176,BFALSE,BFALSE);} 
BgL_auxz00_3172 = 
(long)CINT(BgL_tmpz00_3173); } 
if(
VECTORP(BgL_vecz00_1607))
{ /* Ieee/vector.scm 475 */
BgL_auxz00_3165 = BgL_vecz00_1607
; }  else 
{ 
 obj_t BgL_auxz00_3168;
BgL_auxz00_3168 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1775z00zz__r4_vectors_6_8z00, 
BINT(19825L), BGl_string1958z00zz__r4_vectors_6_8z00, BGl_string1778z00zz__r4_vectors_6_8z00, BgL_vecz00_1607); 
FAILURE(BgL_auxz00_3168,BFALSE,BFALSE);} 
return 
BGl_vectorzd2shrinkz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_3165, BgL_auxz00_3172);} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_vectors_6_8z00(void)
{
{ /* Ieee/vector.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1959z00zz__r4_vectors_6_8z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1959z00zz__r4_vectors_6_8z00));} 

}

#ifdef __cplusplus
}
#endif
