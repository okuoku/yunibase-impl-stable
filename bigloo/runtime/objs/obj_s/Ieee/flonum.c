/*===========================================================================*/
/*   (Ieee/flonum.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/flonum.scm -indent -o objs/obj_s/Ieee/flonum.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_NUMBERS_6_5_FLONUM_TYPE_DEFINITIONS
#define BGL___R4_NUMBERS_6_5_FLONUM_TYPE_DEFINITIONS
#endif // BGL___R4_NUMBERS_6_5_FLONUM_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_realzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(obj_t);
static obj_t BGl_z62stringzd2ze3realz53zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ze3zd3flz30zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_z62remainderflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62negflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL float BGl_ieeezd2stringzd2ze3floatze3zz__r4_numbers_6_5_flonumz00(obj_t);
static obj_t BGl_z62za2flzc0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
extern float bgl_ieee_string_to_float(obj_t);
BGL_EXPORTED_DECL bool_t BGl_infiniteflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
extern obj_t the_failure(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_ceilingflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62oddflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62atanzd22flzd2urz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ze3flze3zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00 = BUNSPEC;
BGL_EXPORTED_DECL double BGl_randomflz00zz__r4_numbers_6_5_flonumz00(void);
BGL_EXPORTED_DECL double BGl_exptflz00zz__r4_numbers_6_5_flonumz00(double, double);
BGL_EXPORTED_DECL double BGl_atanzd21flzd2zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_sqrtflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62asinflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_stringzd2ze3realz31zz__r4_numbers_6_5_flonumz00(char *);
static obj_t BGl_z62doublezd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_logflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_minzd22flzd2zz__r4_numbers_6_5_flonumz00(double, double);
BGL_EXPORTED_DECL double BGl_minflz00zz__r4_numbers_6_5_flonumz00(double, obj_t);
BGL_EXPORTED_DECL double BGl_maxzd22flzd2zz__r4_numbers_6_5_flonumz00(double, double);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62minzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62maxzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62maxflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_evenflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_doublezd2ze3llongzd2bitsze3zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_zf2flzf2zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_z62ieeezd2stringzd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62zb2flzd0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62evenflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_floatzd2ze3intzd2bitsze3zz__r4_numbers_6_5_flonumz00(float);
static obj_t BGl_z62floatzd2ze3intzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62ieeezd2stringzd2ze3realz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_za7eroflzf3z54zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_atanzd22flzd2zz__r4_numbers_6_5_flonumz00(double, double);
BGL_EXPORTED_DECL double BGl_atanflz00zz__r4_numbers_6_5_flonumz00(double, obj_t);
static obj_t BGl_z62za7eroflzf3z36zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_acosflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_expflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_ieeezd2stringzd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_nanflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62finiteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_realzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_positiveflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62doublezd2ze3llongzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62zc3flza1zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_floorflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_truncateflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62floorflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_roundflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62roundflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62negativeflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5_flonumz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5_flonumz00(void);
BGL_EXPORTED_DECL double BGl_tanflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_log10flz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_cosflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62logflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62log10flz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62minflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zd2flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62signbitflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_oddflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_absflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL obj_t BGl_ieeezd2stringzd2ze3realze3zz__r4_numbers_6_5_flonumz00(obj_t);
static obj_t BGl_z62zd3flzb1zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62randomflz62zz__r4_numbers_6_5_flonumz00(obj_t);
static obj_t BGl_z62atanzd21flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_asinflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62truncateflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62expflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_remainderflz00zz__r4_numbers_6_5_flonumz00(double, double);
BGL_EXPORTED_DECL bool_t BGl_finiteflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62zc3zd3flz72zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62sqrtflzd2urzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_za2flza2zz__r4_numbers_6_5_flonumz00(double, double);
extern double bgl_ieee_string_to_double(obj_t);
static obj_t BGl_z62realzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_atanzd22flzd2urz00zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_z62log2flz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62infiniteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62ze3flz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62atanzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_integerflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL obj_t BGl_doublezd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL bool_t BGl_zc3zd3flz10zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_z62tanflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_sinflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62cosflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_zb2flzb2zz__r4_numbers_6_5_flonumz00(double, double);
BGL_EXPORTED_DECL double BGl_llongzd2bitszd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(BGL_LONGLONG_T);
BGL_EXPORTED_DECL long BGl_signbitflz00zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62zf2flz90zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62absflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62exptflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62sqrtflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62integerflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62ieeezd2stringzd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL float BGl_intzd2bitszd2ze3floatze3zz__r4_numbers_6_5_flonumz00(int);
BGL_EXPORTED_DECL double BGl_negflz00zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL double BGl_sqrtflzd2urzd2zz__r4_numbers_6_5_flonumz00(double);
static obj_t BGl_z62intzd2bitszd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62realzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_floatzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(float);
extern obj_t bgl_float_to_ieee_string(float);
static obj_t BGl_z62llongzd2bitszd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62positiveflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_zc3flzc3zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_z62ceilingflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62atanflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62acosflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_negativeflzf3zf3zz__r4_numbers_6_5_flonumz00(double);
BGL_EXPORTED_DECL bool_t BGl_flonumzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t);
static obj_t BGl_z62ze3zd3flz52zz__r4_numbers_6_5_flonumz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62nanflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62flonumzf3z91zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_maxflz00zz__r4_numbers_6_5_flonumz00(double, obj_t);
BGL_EXPORTED_DECL double BGl_zd2flzd2zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t BGl_z62sinflz62zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
static obj_t BGl_z62floatzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t, obj_t);
BGL_EXPORTED_DECL double BGl_log2flz00zz__r4_numbers_6_5_flonumz00(double);
extern obj_t bgl_double_to_ieee_string(double);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_zd3flzd3zz__r4_numbers_6_5_flonumz00(double, double);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_ze3flzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7e3flza781za7za7__r1661za7, BGl_z62ze3flz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_atanzd21flzd2envz00zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762atanza7d21flza7b01662za7, BGl_z62atanzd21flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sqrtflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762sqrtflza762za7za7__1663z00, BGl_z62sqrtflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_doublezd2ze3ieeezd2stringzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762doubleza7d2za7e3i1664za7, BGl_z62doublezd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_logflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762logflza762za7za7__r1665z00, BGl_z62logflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cosflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762cosflza762za7za7__r1666z00, BGl_z62cosflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_signbitflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762signbitflza762za71667za7, BGl_z62signbitflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ieeezd2stringzd2ze3realzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762ieeeza7d2string1668z00, BGl_z62ieeezd2stringzd2ze3realz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_acosflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762acosflza762za7za7__1669z00, BGl_z62acosflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ze3zd3flzd2envze2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7e3za7d3flza752za7za71670z00, BGl_z62ze3zd3flz52zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ieeezd2stringzd2ze3floatzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762ieeeza7d2string1671z00, BGl_z62ieeezd2stringzd2ze3floatz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_positiveflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762positiveflza7f31672z00, BGl_z62positiveflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ceilingflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762ceilingflza762za71673za7, BGl_z62ceilingflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zc3flzd2envz11zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7c3flza7a1za7za7__r1674za7, BGl_z62zc3flza1zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_floorflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762floorflza762za7za7_1675z00, BGl_z62floorflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_truncateflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762truncateflza7621676z00, BGl_z62truncateflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_integerflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762integerflza7f3za71677za7, BGl_z62integerflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1600z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1600za700za7za7_1678za7, "&-fl", 4 );
DEFINE_STRING( BGl_string1601z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1601za700za7za7_1679za7, "&*fl", 4 );
DEFINE_STRING( BGl_string1602z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1602za700za7za7_1680za7, "&/fl", 4 );
DEFINE_STRING( BGl_string1603z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1603za700za7za7_1681za7, "&negfl", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zc3zd3flzd2envzc2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7c3za7d3flza772za7za71682z00, BGl_z62zc3zd3flz72zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1604za700za7za7_1683za7, "loop", 4 );
DEFINE_STRING( BGl_string1605z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1605za700za7za7_1684za7, "pair", 4 );
DEFINE_STRING( BGl_string1606z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1606za700za7za7_1685za7, "&maxfl", 6 );
DEFINE_STRING( BGl_string1607z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1607za700za7za7_1686za7, "&max-2fl", 8 );
DEFINE_STRING( BGl_string1608z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1608za700za7za7_1687za7, "&min-2fl", 8 );
DEFINE_STRING( BGl_string1609z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1609za700za7za7_1688za7, "&minfl", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_minzd22flzd2envz00zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762minza7d22flza7b0za71689z00, BGl_z62minzd22flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_REAL( BGl_real1627z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_real1627za700za7za7__r1690za7, 0.0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_doublezd2ze3llongzd2bitszd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762doubleza7d2za7e3l1691za7, BGl_z62doublezd2ze3llongzd2bitsz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zf2flzd2envz20zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7f2flza790za7za7__r1692za7, BGl_z62zf2flz90zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_log2flzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762log2flza762za7za7__1693z00, BGl_z62log2flz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oddflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762oddflza7f3za791za7za71694za7, BGl_z62oddflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_absflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762absflza762za7za7__r1695z00, BGl_z62absflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1610z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1610za700za7za7_1696za7, "&absfl", 6 );
DEFINE_STRING( BGl_string1611z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1611za700za7za7_1697za7, "&floorfl", 8 );
DEFINE_STRING( BGl_string1612z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1612za700za7za7_1698za7, "&ceilingfl", 10 );
DEFINE_STRING( BGl_string1613z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1613za700za7za7_1699za7, "&truncatefl", 11 );
DEFINE_STRING( BGl_string1614z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1614za700za7za7_1700za7, "&roundfl", 8 );
DEFINE_STRING( BGl_string1615z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1615za700za7za7_1701za7, "&remainderfl", 12 );
DEFINE_STRING( BGl_string1616z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1616za700za7za7_1702za7, "&expfl", 6 );
DEFINE_STRING( BGl_string1617z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1617za700za7za7_1703za7, "&logfl", 6 );
DEFINE_STRING( BGl_string1618z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1618za700za7za7_1704za7, "&log2fl", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_za7eroflzf3zd2envz86zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7a7eroflza7f3za731705z00, BGl_z62za7eroflzf3z36zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1619z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1619za700za7za7_1706za7, "&log10fl", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_atanzd22flzd2envz00zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762atanza7d22flza7b01707za7, BGl_z62atanzd22flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1620z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1620za700za7za7_1708za7, "&sinfl", 6 );
DEFINE_STRING( BGl_string1621z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1621za700za7za7_1709za7, "&cosfl", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_intzd2bitszd2ze3floatzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762intza7d2bitsza7d21710za7, BGl_z62intzd2bitszd2ze3floatz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1622z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1622za700za7za7_1711za7, "&tanfl", 6 );
DEFINE_STRING( BGl_string1623z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1623za700za7za7_1712za7, "&asinfl", 7 );
DEFINE_STRING( BGl_string1624z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1624za700za7za7_1713za7, "&acosfl", 7 );
DEFINE_STRING( BGl_string1625z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1625za700za7za7_1714za7, "atanfl", 6 );
DEFINE_STRING( BGl_string1626z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1626za700za7za7_1715za7, "Domain error", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_finiteflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762finiteflza7f3za791716za7, BGl_z62finiteflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1628z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1628za700za7za7_1717za7, "&atanfl", 7 );
DEFINE_STRING( BGl_string1629z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1629za700za7za7_1718za7, "&atan-1fl", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_za2flzd2envz70zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7a2flza7c0za7za7__r1719za7, BGl_z62za2flzc0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1630z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1630za700za7za7_1720za7, "&atan-2fl", 9 );
DEFINE_STRING( BGl_string1631z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1631za700za7za7_1721za7, "&atan-2fl-ur", 12 );
DEFINE_STRING( BGl_string1632z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1632za700za7za7_1722za7, "sqrtfl", 6 );
DEFINE_STRING( BGl_string1633z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1633za700za7za7_1723za7, "&sqrtfl", 7 );
DEFINE_STRING( BGl_string1634z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1634za700za7za7_1724za7, "&sqrtfl-ur", 10 );
DEFINE_STRING( BGl_string1635z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1635za700za7za7_1725za7, "&exptfl", 7 );
DEFINE_STRING( BGl_string1636z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1636za700za7za7_1726za7, "&signbitfl", 10 );
DEFINE_STRING( BGl_string1637z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1637za700za7za7_1727za7, "&integerfl?", 11 );
DEFINE_STRING( BGl_string1638z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1638za700za7za7_1728za7, "&evenfl?", 8 );
DEFINE_STRING( BGl_string1639z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1639za700za7za7_1729za7, "&oddfl?", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3realzd2envze3zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762stringza7d2za7e3r1730za7, BGl_z62stringzd2ze3realz53zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1640z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1640za700za7za7_1731za7, "&finitefl?", 10 );
DEFINE_STRING( BGl_string1641z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1641za700za7za7_1732za7, "&infinitefl?", 12 );
DEFINE_STRING( BGl_string1642z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1642za700za7za7_1733za7, "&nanfl?", 7 );
DEFINE_STRING( BGl_string1643z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1643za700za7za7_1734za7, "+nan.0", 6 );
DEFINE_STRING( BGl_string1644z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1644za700za7za7_1735za7, "+inf.0", 6 );
DEFINE_STRING( BGl_string1645z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1645za700za7za7_1736za7, "-inf.0", 6 );
DEFINE_STRING( BGl_string1646z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1646za700za7za7_1737za7, "&string->real", 13 );
DEFINE_STRING( BGl_string1647z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1647za700za7za7_1738za7, "bstring", 7 );
DEFINE_STRING( BGl_string1648z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1648za700za7za7_1739za7, "&ieee-string->real", 18 );
DEFINE_STRING( BGl_string1649z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1649za700za7za7_1740za7, "&real->ieee-string", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tanflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762tanflza762za7za7__r1741z00, BGl_z62tanflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_log10flzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762log10flza762za7za7_1742z00, BGl_z62log10flz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_minflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762minflza762za7za7__r1743z00, va_generic_entry, BGl_z62minflz62zz__r4_numbers_6_5_flonumz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sinflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762sinflza762za7za7__r1744z00, BGl_z62sinflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zd2flzd2envz00zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7d2flza7b0za7za7__r1745za7, BGl_z62zd2flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1650z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1650za700za7za7_1746za7, "&ieee-string->double", 20 );
DEFINE_STRING( BGl_string1651z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1651za700za7za7_1747za7, "&double->ieee-string", 20 );
DEFINE_STRING( BGl_string1652z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1652za700za7za7_1748za7, "&ieee-string->float", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7d3flza7b1za7za7__r1749za7, BGl_z62zd3flzb1zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1653z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1653za700za7za7_1750za7, "&float->ieee-string", 19 );
DEFINE_STRING( BGl_string1654z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1654za700za7za7_1751za7, "&double->llong-bits", 19 );
DEFINE_STRING( BGl_string1655z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1655za700za7za7_1752za7, "&llong-bits->double", 19 );
DEFINE_STRING( BGl_string1656z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1656za700za7za7_1753za7, "bllong", 6 );
DEFINE_STRING( BGl_string1657z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1657za700za7za7_1754za7, "&float->int-bits", 16 );
DEFINE_STRING( BGl_string1658z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1658za700za7za7_1755za7, "&int-bits->float", 16 );
DEFINE_STRING( BGl_string1659z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1659za700za7za7_1756za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_atanflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762atanflza762za7za7__1757z00, va_generic_entry, BGl_z62atanflz62zz__r4_numbers_6_5_flonumz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_asinflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762asinflza762za7za7__1758z00, BGl_z62asinflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ieeezd2stringzd2ze3doublezd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762ieeeza7d2string1759z00, BGl_z62ieeezd2stringzd2ze3doublez81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_remainderflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762remainderflza761760z00, BGl_z62remainderflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_realzd2ze3ieeezd2stringzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762realza7d2za7e3iee1761za7, BGl_z62realzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_negflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762negflza762za7za7__r1762z00, BGl_z62negflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1660z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1660za700za7za7_1763za7, "__r4_numbers_6_5_flonum", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_nanflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762nanflza7f3za791za7za71764za7, BGl_z62nanflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sqrtflzd2urzd2envz00zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762sqrtflza7d2urza7b1765za7, BGl_z62sqrtflzd2urzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1589z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1589za700za7za7_1766za7, "/tmp/bigloo/runtime/Ieee/flonum.scm", 35 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_infiniteflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762infiniteflza7f31767z00, BGl_z62infiniteflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_atanzd22flzd2urzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762atanza7d22flza7d21768za7, BGl_z62atanzd22flzd2urz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1590z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1590za700za7za7_1769za7, "&=fl", 4 );
DEFINE_STRING( BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1591za700za7za7_1770za7, "real", 4 );
DEFINE_STRING( BGl_string1592z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1592za700za7za7_1771za7, "&<fl", 4 );
DEFINE_STRING( BGl_string1593z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1593za700za7za7_1772za7, "&>fl", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_randomflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762randomflza762za7za71773z00, BGl_z62randomflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exptflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762exptflza762za7za7__1774z00, BGl_z62exptflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1594z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1594za700za7za7_1775za7, "&<=fl", 5 );
DEFINE_STRING( BGl_string1595z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1595za700za7za7_1776za7, "&>=fl", 5 );
DEFINE_STRING( BGl_string1596z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1596za700za7za7_1777za7, "&zerofl?", 8 );
DEFINE_STRING( BGl_string1597z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1597za700za7za7_1778za7, "&positivefl?", 12 );
DEFINE_STRING( BGl_string1598z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1598za700za7za7_1779za7, "&negativefl?", 12 );
DEFINE_STRING( BGl_string1599z00zz__r4_numbers_6_5_flonumz00, BgL_bgl_string1599za700za7za7_1780za7, "&+fl", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_roundflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762roundflza762za7za7_1781z00, BGl_z62roundflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_negativeflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762negativeflza7f31782z00, BGl_z62negativeflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flonumzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762flonumza7f3za791za71783z00, BGl_z62flonumzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_maxzd22flzd2envz00zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762maxza7d22flza7b0za71784z00, BGl_z62maxzd22flzb0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_maxflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762maxflza762za7za7__r1785z00, va_generic_entry, BGl_z62maxflz62zz__r4_numbers_6_5_flonumz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_zb2flzd2envz60zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762za7b2flza7d0za7za7__r1786za7, BGl_z62zb2flzd0zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_evenflzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762evenflza7f3za791za71787z00, BGl_z62evenflzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_llongzd2bitszd2ze3doublezd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762llongza7d2bitsza71788za7, BGl_z62llongzd2bitszd2ze3doublez81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_floatzd2ze3intzd2bitszd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762floatza7d2za7e3in1789za7, BGl_z62floatzd2ze3intzd2bitsz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_expflzd2envzd2zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762expflza762za7za7__r1790z00, BGl_z62expflz62zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_floatzd2ze3ieeezd2stringzd2envz31zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762floatza7d2za7e3ie1791za7, BGl_z62floatzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_realzf3zd2envz21zz__r4_numbers_6_5_flonumz00, BgL_bgl_za762realza7f3za791za7za7_1792za7, BGl_z62realzf3z91zz__r4_numbers_6_5_flonumz00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long BgL_checksumz00_1583, char * BgL_fromz00_1584)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_numbers_6_5_flonumz00(); 
return 
BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5_flonumz00();}  else 
{ 
return BUNSPEC;} } 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_numbers_6_5_flonumz00(void)
{
{ /* Ieee/flonum.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* real? */
BGL_EXPORTED_DEF bool_t BGl_realzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t BgL_objz00_3)
{
{ /* Ieee/flonum.scm 316 */
if(
INTEGERP(BgL_objz00_3))
{ /* Ieee/flonum.scm 317 */
return ((bool_t)1);}  else 
{ /* Ieee/flonum.scm 317 */
return 
REALP(BgL_objz00_3);} } 

}



/* &real? */
obj_t BGl_z62realzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1266, obj_t BgL_objz00_1267)
{
{ /* Ieee/flonum.scm 316 */
return 
BBOOL(
BGl_realzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_objz00_1267));} 

}



/* flonum? */
BGL_EXPORTED_DEF bool_t BGl_flonumzf3zf3zz__r4_numbers_6_5_flonumz00(obj_t BgL_objz00_4)
{
{ /* Ieee/flonum.scm 324 */
return 
REALP(BgL_objz00_4);} 

}



/* &flonum? */
obj_t BGl_z62flonumzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1268, obj_t BgL_objz00_1269)
{
{ /* Ieee/flonum.scm 324 */
return 
BBOOL(
BGl_flonumzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_objz00_1269));} 

}



/* =fl */
BGL_EXPORTED_DEF bool_t BGl_zd3flzd3zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_5, double BgL_r2z00_6)
{
{ /* Ieee/flonum.scm 330 */
return 
(BgL_r1z00_5==BgL_r2z00_6);} 

}



/* &=fl */
obj_t BGl_z62zd3flzb1zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1270, obj_t BgL_r1z00_1271, obj_t BgL_r2z00_1272)
{
{ /* Ieee/flonum.scm 330 */
{ /* Ieee/flonum.scm 331 */
 bool_t BgL_tmpz00_1600;
{ /* Ieee/flonum.scm 331 */
 double BgL_auxz00_1610; double BgL_auxz00_1601;
{ /* Ieee/flonum.scm 331 */
 obj_t BgL_tmpz00_1611;
if(
REALP(BgL_r2z00_1272))
{ /* Ieee/flonum.scm 331 */
BgL_tmpz00_1611 = BgL_r2z00_1272
; }  else 
{ 
 obj_t BgL_auxz00_1614;
BgL_auxz00_1614 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15207L), BGl_string1590z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1272); 
FAILURE(BgL_auxz00_1614,BFALSE,BFALSE);} 
BgL_auxz00_1610 = 
REAL_TO_DOUBLE(BgL_tmpz00_1611); } 
{ /* Ieee/flonum.scm 331 */
 obj_t BgL_tmpz00_1602;
if(
REALP(BgL_r1z00_1271))
{ /* Ieee/flonum.scm 331 */
BgL_tmpz00_1602 = BgL_r1z00_1271
; }  else 
{ 
 obj_t BgL_auxz00_1605;
BgL_auxz00_1605 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15207L), BGl_string1590z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1271); 
FAILURE(BgL_auxz00_1605,BFALSE,BFALSE);} 
BgL_auxz00_1601 = 
REAL_TO_DOUBLE(BgL_tmpz00_1602); } 
BgL_tmpz00_1600 = 
BGl_zd3flzd3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1601, BgL_auxz00_1610); } 
return 
BBOOL(BgL_tmpz00_1600);} } 

}



/* <fl */
BGL_EXPORTED_DEF bool_t BGl_zc3flzc3zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_7, double BgL_r2z00_8)
{
{ /* Ieee/flonum.scm 336 */
return 
(BgL_r1z00_7<BgL_r2z00_8);} 

}



/* &<fl */
obj_t BGl_z62zc3flza1zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1273, obj_t BgL_r1z00_1274, obj_t BgL_r2z00_1275)
{
{ /* Ieee/flonum.scm 336 */
{ /* Ieee/flonum.scm 337 */
 bool_t BgL_tmpz00_1622;
{ /* Ieee/flonum.scm 337 */
 double BgL_auxz00_1632; double BgL_auxz00_1623;
{ /* Ieee/flonum.scm 337 */
 obj_t BgL_tmpz00_1633;
if(
REALP(BgL_r2z00_1275))
{ /* Ieee/flonum.scm 337 */
BgL_tmpz00_1633 = BgL_r2z00_1275
; }  else 
{ 
 obj_t BgL_auxz00_1636;
BgL_auxz00_1636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15474L), BGl_string1592z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1275); 
FAILURE(BgL_auxz00_1636,BFALSE,BFALSE);} 
BgL_auxz00_1632 = 
REAL_TO_DOUBLE(BgL_tmpz00_1633); } 
{ /* Ieee/flonum.scm 337 */
 obj_t BgL_tmpz00_1624;
if(
REALP(BgL_r1z00_1274))
{ /* Ieee/flonum.scm 337 */
BgL_tmpz00_1624 = BgL_r1z00_1274
; }  else 
{ 
 obj_t BgL_auxz00_1627;
BgL_auxz00_1627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15474L), BGl_string1592z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1274); 
FAILURE(BgL_auxz00_1627,BFALSE,BFALSE);} 
BgL_auxz00_1623 = 
REAL_TO_DOUBLE(BgL_tmpz00_1624); } 
BgL_tmpz00_1622 = 
BGl_zc3flzc3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1623, BgL_auxz00_1632); } 
return 
BBOOL(BgL_tmpz00_1622);} } 

}



/* >fl */
BGL_EXPORTED_DEF bool_t BGl_ze3flze3zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_9, double BgL_r2z00_10)
{
{ /* Ieee/flonum.scm 342 */
return 
(BgL_r1z00_9>BgL_r2z00_10);} 

}



/* &>fl */
obj_t BGl_z62ze3flz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1276, obj_t BgL_r1z00_1277, obj_t BgL_r2z00_1278)
{
{ /* Ieee/flonum.scm 342 */
{ /* Ieee/flonum.scm 343 */
 bool_t BgL_tmpz00_1644;
{ /* Ieee/flonum.scm 343 */
 double BgL_auxz00_1654; double BgL_auxz00_1645;
{ /* Ieee/flonum.scm 343 */
 obj_t BgL_tmpz00_1655;
if(
REALP(BgL_r2z00_1278))
{ /* Ieee/flonum.scm 343 */
BgL_tmpz00_1655 = BgL_r2z00_1278
; }  else 
{ 
 obj_t BgL_auxz00_1658;
BgL_auxz00_1658 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15741L), BGl_string1593z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1278); 
FAILURE(BgL_auxz00_1658,BFALSE,BFALSE);} 
BgL_auxz00_1654 = 
REAL_TO_DOUBLE(BgL_tmpz00_1655); } 
{ /* Ieee/flonum.scm 343 */
 obj_t BgL_tmpz00_1646;
if(
REALP(BgL_r1z00_1277))
{ /* Ieee/flonum.scm 343 */
BgL_tmpz00_1646 = BgL_r1z00_1277
; }  else 
{ 
 obj_t BgL_auxz00_1649;
BgL_auxz00_1649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15741L), BGl_string1593z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1277); 
FAILURE(BgL_auxz00_1649,BFALSE,BFALSE);} 
BgL_auxz00_1645 = 
REAL_TO_DOUBLE(BgL_tmpz00_1646); } 
BgL_tmpz00_1644 = 
BGl_ze3flze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1645, BgL_auxz00_1654); } 
return 
BBOOL(BgL_tmpz00_1644);} } 

}



/* <=fl */
BGL_EXPORTED_DEF bool_t BGl_zc3zd3flz10zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_11, double BgL_r2z00_12)
{
{ /* Ieee/flonum.scm 348 */
return 
(BgL_r1z00_11<=BgL_r2z00_12);} 

}



/* &<=fl */
obj_t BGl_z62zc3zd3flz72zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1279, obj_t BgL_r1z00_1280, obj_t BgL_r2z00_1281)
{
{ /* Ieee/flonum.scm 348 */
{ /* Ieee/flonum.scm 349 */
 bool_t BgL_tmpz00_1666;
{ /* Ieee/flonum.scm 349 */
 double BgL_auxz00_1676; double BgL_auxz00_1667;
{ /* Ieee/flonum.scm 349 */
 obj_t BgL_tmpz00_1677;
if(
REALP(BgL_r2z00_1281))
{ /* Ieee/flonum.scm 349 */
BgL_tmpz00_1677 = BgL_r2z00_1281
; }  else 
{ 
 obj_t BgL_auxz00_1680;
BgL_auxz00_1680 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(16009L), BGl_string1594z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1281); 
FAILURE(BgL_auxz00_1680,BFALSE,BFALSE);} 
BgL_auxz00_1676 = 
REAL_TO_DOUBLE(BgL_tmpz00_1677); } 
{ /* Ieee/flonum.scm 349 */
 obj_t BgL_tmpz00_1668;
if(
REALP(BgL_r1z00_1280))
{ /* Ieee/flonum.scm 349 */
BgL_tmpz00_1668 = BgL_r1z00_1280
; }  else 
{ 
 obj_t BgL_auxz00_1671;
BgL_auxz00_1671 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(16009L), BGl_string1594z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1280); 
FAILURE(BgL_auxz00_1671,BFALSE,BFALSE);} 
BgL_auxz00_1667 = 
REAL_TO_DOUBLE(BgL_tmpz00_1668); } 
BgL_tmpz00_1666 = 
BGl_zc3zd3flz10zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1667, BgL_auxz00_1676); } 
return 
BBOOL(BgL_tmpz00_1666);} } 

}



/* >=fl */
BGL_EXPORTED_DEF bool_t BGl_ze3zd3flz30zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_13, double BgL_r2z00_14)
{
{ /* Ieee/flonum.scm 354 */
return 
(BgL_r1z00_13>=BgL_r2z00_14);} 

}



/* &>=fl */
obj_t BGl_z62ze3zd3flz52zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1282, obj_t BgL_r1z00_1283, obj_t BgL_r2z00_1284)
{
{ /* Ieee/flonum.scm 354 */
{ /* Ieee/flonum.scm 355 */
 bool_t BgL_tmpz00_1688;
{ /* Ieee/flonum.scm 355 */
 double BgL_auxz00_1698; double BgL_auxz00_1689;
{ /* Ieee/flonum.scm 355 */
 obj_t BgL_tmpz00_1699;
if(
REALP(BgL_r2z00_1284))
{ /* Ieee/flonum.scm 355 */
BgL_tmpz00_1699 = BgL_r2z00_1284
; }  else 
{ 
 obj_t BgL_auxz00_1702;
BgL_auxz00_1702 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(16278L), BGl_string1595z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1284); 
FAILURE(BgL_auxz00_1702,BFALSE,BFALSE);} 
BgL_auxz00_1698 = 
REAL_TO_DOUBLE(BgL_tmpz00_1699); } 
{ /* Ieee/flonum.scm 355 */
 obj_t BgL_tmpz00_1690;
if(
REALP(BgL_r1z00_1283))
{ /* Ieee/flonum.scm 355 */
BgL_tmpz00_1690 = BgL_r1z00_1283
; }  else 
{ 
 obj_t BgL_auxz00_1693;
BgL_auxz00_1693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(16278L), BGl_string1595z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1283); 
FAILURE(BgL_auxz00_1693,BFALSE,BFALSE);} 
BgL_auxz00_1689 = 
REAL_TO_DOUBLE(BgL_tmpz00_1690); } 
BgL_tmpz00_1688 = 
BGl_ze3zd3flz30zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1689, BgL_auxz00_1698); } 
return 
BBOOL(BgL_tmpz00_1688);} } 

}



/* zerofl? */
BGL_EXPORTED_DEF bool_t BGl_za7eroflzf3z54zz__r4_numbers_6_5_flonumz00(double BgL_rz00_15)
{
{ /* Ieee/flonum.scm 360 */
return 
(BgL_rz00_15==((double)0.0));} 

}



/* &zerofl? */
obj_t BGl_z62za7eroflzf3z36zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1285, obj_t BgL_rz00_1286)
{
{ /* Ieee/flonum.scm 360 */
{ /* Ieee/flonum.scm 331 */
 bool_t BgL_tmpz00_1710;
{ /* Ieee/flonum.scm 331 */
 double BgL_auxz00_1711;
{ /* Ieee/flonum.scm 331 */
 obj_t BgL_tmpz00_1712;
if(
REALP(BgL_rz00_1286))
{ /* Ieee/flonum.scm 331 */
BgL_tmpz00_1712 = BgL_rz00_1286
; }  else 
{ 
 obj_t BgL_auxz00_1715;
BgL_auxz00_1715 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15207L), BGl_string1596z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1286); 
FAILURE(BgL_auxz00_1715,BFALSE,BFALSE);} 
BgL_auxz00_1711 = 
REAL_TO_DOUBLE(BgL_tmpz00_1712); } 
BgL_tmpz00_1710 = 
BGl_za7eroflzf3z54zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1711); } 
return 
BBOOL(BgL_tmpz00_1710);} } 

}



/* positivefl? */
BGL_EXPORTED_DEF bool_t BGl_positiveflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_16)
{
{ /* Ieee/flonum.scm 366 */
return 
(BgL_rz00_16>((double)0.0));} 

}



/* &positivefl? */
obj_t BGl_z62positiveflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1287, obj_t BgL_rz00_1288)
{
{ /* Ieee/flonum.scm 366 */
{ /* Ieee/flonum.scm 343 */
 bool_t BgL_tmpz00_1723;
{ /* Ieee/flonum.scm 343 */
 double BgL_auxz00_1724;
{ /* Ieee/flonum.scm 343 */
 obj_t BgL_tmpz00_1725;
if(
REALP(BgL_rz00_1288))
{ /* Ieee/flonum.scm 343 */
BgL_tmpz00_1725 = BgL_rz00_1288
; }  else 
{ 
 obj_t BgL_auxz00_1728;
BgL_auxz00_1728 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15741L), BGl_string1597z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1288); 
FAILURE(BgL_auxz00_1728,BFALSE,BFALSE);} 
BgL_auxz00_1724 = 
REAL_TO_DOUBLE(BgL_tmpz00_1725); } 
BgL_tmpz00_1723 = 
BGl_positiveflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1724); } 
return 
BBOOL(BgL_tmpz00_1723);} } 

}



/* negativefl? */
BGL_EXPORTED_DEF bool_t BGl_negativeflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_17)
{
{ /* Ieee/flonum.scm 372 */
return 
(BgL_rz00_17<((double)0.0));} 

}



/* &negativefl? */
obj_t BGl_z62negativeflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1289, obj_t BgL_rz00_1290)
{
{ /* Ieee/flonum.scm 372 */
{ /* Ieee/flonum.scm 337 */
 bool_t BgL_tmpz00_1736;
{ /* Ieee/flonum.scm 337 */
 double BgL_auxz00_1737;
{ /* Ieee/flonum.scm 337 */
 obj_t BgL_tmpz00_1738;
if(
REALP(BgL_rz00_1290))
{ /* Ieee/flonum.scm 337 */
BgL_tmpz00_1738 = BgL_rz00_1290
; }  else 
{ 
 obj_t BgL_auxz00_1741;
BgL_auxz00_1741 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(15474L), BGl_string1598z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1290); 
FAILURE(BgL_auxz00_1741,BFALSE,BFALSE);} 
BgL_auxz00_1737 = 
REAL_TO_DOUBLE(BgL_tmpz00_1738); } 
BgL_tmpz00_1736 = 
BGl_negativeflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1737); } 
return 
BBOOL(BgL_tmpz00_1736);} } 

}



/* +fl */
BGL_EXPORTED_DEF double BGl_zb2flzb2zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_18, double BgL_r2z00_19)
{
{ /* Ieee/flonum.scm 378 */
return 
(BgL_r1z00_18+BgL_r2z00_19);} 

}



/* &+fl */
obj_t BGl_z62zb2flzd0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1291, obj_t BgL_r1z00_1292, obj_t BgL_r2z00_1293)
{
{ /* Ieee/flonum.scm 378 */
{ /* Ieee/flonum.scm 379 */
 double BgL_tmpz00_1749;
{ /* Ieee/flonum.scm 379 */
 double BgL_auxz00_1759; double BgL_auxz00_1750;
{ /* Ieee/flonum.scm 379 */
 obj_t BgL_tmpz00_1760;
if(
REALP(BgL_r2z00_1293))
{ /* Ieee/flonum.scm 379 */
BgL_tmpz00_1760 = BgL_r2z00_1293
; }  else 
{ 
 obj_t BgL_auxz00_1763;
BgL_auxz00_1763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17352L), BGl_string1599z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1293); 
FAILURE(BgL_auxz00_1763,BFALSE,BFALSE);} 
BgL_auxz00_1759 = 
REAL_TO_DOUBLE(BgL_tmpz00_1760); } 
{ /* Ieee/flonum.scm 379 */
 obj_t BgL_tmpz00_1751;
if(
REALP(BgL_r1z00_1292))
{ /* Ieee/flonum.scm 379 */
BgL_tmpz00_1751 = BgL_r1z00_1292
; }  else 
{ 
 obj_t BgL_auxz00_1754;
BgL_auxz00_1754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17352L), BGl_string1599z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1292); 
FAILURE(BgL_auxz00_1754,BFALSE,BFALSE);} 
BgL_auxz00_1750 = 
REAL_TO_DOUBLE(BgL_tmpz00_1751); } 
BgL_tmpz00_1749 = 
BGl_zb2flzb2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1750, BgL_auxz00_1759); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1749);} } 

}



/* -fl */
BGL_EXPORTED_DEF double BGl_zd2flzd2zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_20, double BgL_r2z00_21)
{
{ /* Ieee/flonum.scm 380 */
return 
(BgL_r1z00_20-BgL_r2z00_21);} 

}



/* &-fl */
obj_t BGl_z62zd2flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1294, obj_t BgL_r1z00_1295, obj_t BgL_r2z00_1296)
{
{ /* Ieee/flonum.scm 380 */
{ /* Ieee/flonum.scm 381 */
 double BgL_tmpz00_1771;
{ /* Ieee/flonum.scm 381 */
 double BgL_auxz00_1781; double BgL_auxz00_1772;
{ /* Ieee/flonum.scm 381 */
 obj_t BgL_tmpz00_1782;
if(
REALP(BgL_r2z00_1296))
{ /* Ieee/flonum.scm 381 */
BgL_tmpz00_1782 = BgL_r2z00_1296
; }  else 
{ 
 obj_t BgL_auxz00_1785;
BgL_auxz00_1785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17396L), BGl_string1600z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1296); 
FAILURE(BgL_auxz00_1785,BFALSE,BFALSE);} 
BgL_auxz00_1781 = 
REAL_TO_DOUBLE(BgL_tmpz00_1782); } 
{ /* Ieee/flonum.scm 381 */
 obj_t BgL_tmpz00_1773;
if(
REALP(BgL_r1z00_1295))
{ /* Ieee/flonum.scm 381 */
BgL_tmpz00_1773 = BgL_r1z00_1295
; }  else 
{ 
 obj_t BgL_auxz00_1776;
BgL_auxz00_1776 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17396L), BGl_string1600z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1295); 
FAILURE(BgL_auxz00_1776,BFALSE,BFALSE);} 
BgL_auxz00_1772 = 
REAL_TO_DOUBLE(BgL_tmpz00_1773); } 
BgL_tmpz00_1771 = 
BGl_zd2flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1772, BgL_auxz00_1781); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1771);} } 

}



/* *fl */
BGL_EXPORTED_DEF double BGl_za2flza2zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_22, double BgL_r2z00_23)
{
{ /* Ieee/flonum.scm 382 */
return 
(BgL_r1z00_22*BgL_r2z00_23);} 

}



/* &*fl */
obj_t BGl_z62za2flzc0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1297, obj_t BgL_r1z00_1298, obj_t BgL_r2z00_1299)
{
{ /* Ieee/flonum.scm 382 */
{ /* Ieee/flonum.scm 383 */
 double BgL_tmpz00_1793;
{ /* Ieee/flonum.scm 383 */
 double BgL_auxz00_1803; double BgL_auxz00_1794;
{ /* Ieee/flonum.scm 383 */
 obj_t BgL_tmpz00_1804;
if(
REALP(BgL_r2z00_1299))
{ /* Ieee/flonum.scm 383 */
BgL_tmpz00_1804 = BgL_r2z00_1299
; }  else 
{ 
 obj_t BgL_auxz00_1807;
BgL_auxz00_1807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17440L), BGl_string1601z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1299); 
FAILURE(BgL_auxz00_1807,BFALSE,BFALSE);} 
BgL_auxz00_1803 = 
REAL_TO_DOUBLE(BgL_tmpz00_1804); } 
{ /* Ieee/flonum.scm 383 */
 obj_t BgL_tmpz00_1795;
if(
REALP(BgL_r1z00_1298))
{ /* Ieee/flonum.scm 383 */
BgL_tmpz00_1795 = BgL_r1z00_1298
; }  else 
{ 
 obj_t BgL_auxz00_1798;
BgL_auxz00_1798 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17440L), BGl_string1601z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1298); 
FAILURE(BgL_auxz00_1798,BFALSE,BFALSE);} 
BgL_auxz00_1794 = 
REAL_TO_DOUBLE(BgL_tmpz00_1795); } 
BgL_tmpz00_1793 = 
BGl_za2flza2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1794, BgL_auxz00_1803); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1793);} } 

}



/* /fl */
BGL_EXPORTED_DEF double BGl_zf2flzf2zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_24, double BgL_r2z00_25)
{
{ /* Ieee/flonum.scm 384 */
return 
(BgL_r1z00_24/BgL_r2z00_25);} 

}



/* &/fl */
obj_t BGl_z62zf2flz90zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1300, obj_t BgL_r1z00_1301, obj_t BgL_r2z00_1302)
{
{ /* Ieee/flonum.scm 384 */
{ /* Ieee/flonum.scm 385 */
 double BgL_tmpz00_1815;
{ /* Ieee/flonum.scm 385 */
 double BgL_auxz00_1825; double BgL_auxz00_1816;
{ /* Ieee/flonum.scm 385 */
 obj_t BgL_tmpz00_1826;
if(
REALP(BgL_r2z00_1302))
{ /* Ieee/flonum.scm 385 */
BgL_tmpz00_1826 = BgL_r2z00_1302
; }  else 
{ 
 obj_t BgL_auxz00_1829;
BgL_auxz00_1829 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17484L), BGl_string1602z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1302); 
FAILURE(BgL_auxz00_1829,BFALSE,BFALSE);} 
BgL_auxz00_1825 = 
REAL_TO_DOUBLE(BgL_tmpz00_1826); } 
{ /* Ieee/flonum.scm 385 */
 obj_t BgL_tmpz00_1817;
if(
REALP(BgL_r1z00_1301))
{ /* Ieee/flonum.scm 385 */
BgL_tmpz00_1817 = BgL_r1z00_1301
; }  else 
{ 
 obj_t BgL_auxz00_1820;
BgL_auxz00_1820 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17484L), BGl_string1602z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1301); 
FAILURE(BgL_auxz00_1820,BFALSE,BFALSE);} 
BgL_auxz00_1816 = 
REAL_TO_DOUBLE(BgL_tmpz00_1817); } 
BgL_tmpz00_1815 = 
BGl_zf2flzf2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1816, BgL_auxz00_1825); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1815);} } 

}



/* negfl */
BGL_EXPORTED_DEF double BGl_negflz00zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_26)
{
{ /* Ieee/flonum.scm 390 */
return 
NEG(BgL_r1z00_26);} 

}



/* &negfl */
obj_t BGl_z62negflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1303, obj_t BgL_r1z00_1304)
{
{ /* Ieee/flonum.scm 390 */
{ /* Ieee/flonum.scm 391 */
 double BgL_tmpz00_1837;
{ /* Ieee/flonum.scm 391 */
 double BgL_auxz00_1838;
{ /* Ieee/flonum.scm 391 */
 obj_t BgL_tmpz00_1839;
if(
REALP(BgL_r1z00_1304))
{ /* Ieee/flonum.scm 391 */
BgL_tmpz00_1839 = BgL_r1z00_1304
; }  else 
{ 
 obj_t BgL_auxz00_1842;
BgL_auxz00_1842 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(17750L), BGl_string1603z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1304); 
FAILURE(BgL_auxz00_1842,BFALSE,BFALSE);} 
BgL_auxz00_1838 = 
REAL_TO_DOUBLE(BgL_tmpz00_1839); } 
BgL_tmpz00_1837 = 
BGl_negflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1838); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1837);} } 

}



/* maxfl */
BGL_EXPORTED_DEF double BGl_maxflz00zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_27, obj_t BgL_rnz00_28)
{
{ /* Ieee/flonum.scm 396 */
{ 
 double BgL_maxz00_1048; obj_t BgL_rnz00_1049;
BgL_maxz00_1048 = BgL_r1z00_27; 
BgL_rnz00_1049 = BgL_rnz00_28; 
BgL_loopz00_1047:
if(
NULLP(BgL_rnz00_1049))
{ /* Ieee/flonum.scm 399 */
return BgL_maxz00_1048;}  else 
{ 
 obj_t BgL_rnz00_1868; double BgL_maxz00_1851;
{ /* Ieee/flonum.scm 401 */
 double BgL_r1z00_1059;
{ /* Ieee/flonum.scm 401 */
 obj_t BgL_pairz00_1058;
if(
PAIRP(BgL_rnz00_1049))
{ /* Ieee/flonum.scm 401 */
BgL_pairz00_1058 = BgL_rnz00_1049; }  else 
{ 
 obj_t BgL_auxz00_1854;
BgL_auxz00_1854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18105L), BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BGl_string1605z00zz__r4_numbers_6_5_flonumz00, BgL_rnz00_1049); 
FAILURE(BgL_auxz00_1854,BFALSE,BFALSE);} 
{ /* Ieee/flonum.scm 401 */
 obj_t BgL_tmpz00_1858;
{ /* Ieee/flonum.scm 401 */
 obj_t BgL_aux1475z00_1449;
BgL_aux1475z00_1449 = 
CAR(BgL_pairz00_1058); 
if(
REALP(BgL_aux1475z00_1449))
{ /* Ieee/flonum.scm 401 */
BgL_tmpz00_1858 = BgL_aux1475z00_1449
; }  else 
{ 
 obj_t BgL_auxz00_1862;
BgL_auxz00_1862 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18100L), BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_aux1475z00_1449); 
FAILURE(BgL_auxz00_1862,BFALSE,BFALSE);} } 
BgL_r1z00_1059 = 
REAL_TO_DOUBLE(BgL_tmpz00_1858); } } 
BgL_maxz00_1851 = 
BGL_FL_MAX2(BgL_r1z00_1059, BgL_maxz00_1048); } 
{ /* Ieee/flonum.scm 401 */
 obj_t BgL_pairz00_1061;
if(
PAIRP(BgL_rnz00_1049))
{ /* Ieee/flonum.scm 401 */
BgL_pairz00_1061 = BgL_rnz00_1049; }  else 
{ 
 obj_t BgL_auxz00_1871;
BgL_auxz00_1871 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18119L), BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BGl_string1605z00zz__r4_numbers_6_5_flonumz00, BgL_rnz00_1049); 
FAILURE(BgL_auxz00_1871,BFALSE,BFALSE);} 
BgL_rnz00_1868 = 
CDR(BgL_pairz00_1061); } 
BgL_rnz00_1049 = BgL_rnz00_1868; 
BgL_maxz00_1048 = BgL_maxz00_1851; 
goto BgL_loopz00_1047;} } } 

}



/* &maxfl */
obj_t BGl_z62maxflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1305, obj_t BgL_r1z00_1306, obj_t BgL_rnz00_1307)
{
{ /* Ieee/flonum.scm 396 */
{ /* Ieee/flonum.scm 397 */
 double BgL_tmpz00_1876;
{ /* Ieee/flonum.scm 397 */
 double BgL_auxz00_1877;
{ /* Ieee/flonum.scm 397 */
 obj_t BgL_tmpz00_1878;
if(
REALP(BgL_r1z00_1306))
{ /* Ieee/flonum.scm 397 */
BgL_tmpz00_1878 = BgL_r1z00_1306
; }  else 
{ 
 obj_t BgL_auxz00_1881;
BgL_auxz00_1881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18017L), BGl_string1606z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1306); 
FAILURE(BgL_auxz00_1881,BFALSE,BFALSE);} 
BgL_auxz00_1877 = 
REAL_TO_DOUBLE(BgL_tmpz00_1878); } 
BgL_tmpz00_1876 = 
BGl_maxflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1877, BgL_rnz00_1307); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1876);} } 

}



/* max-2fl */
BGL_EXPORTED_DEF double BGl_maxzd22flzd2zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_29, double BgL_r2z00_30)
{
{ /* Ieee/flonum.scm 406 */
return 
BGL_FL_MAX2(BgL_r1z00_29, BgL_r2z00_30);} 

}



/* &max-2fl */
obj_t BGl_z62maxzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1308, obj_t BgL_r1z00_1309, obj_t BgL_r2z00_1310)
{
{ /* Ieee/flonum.scm 406 */
{ /* Ieee/flonum.scm 407 */
 double BgL_tmpz00_1889;
{ /* Ieee/flonum.scm 407 */
 double BgL_auxz00_1899; double BgL_auxz00_1890;
{ /* Ieee/flonum.scm 407 */
 obj_t BgL_tmpz00_1900;
if(
REALP(BgL_r2z00_1310))
{ /* Ieee/flonum.scm 407 */
BgL_tmpz00_1900 = BgL_r2z00_1310
; }  else 
{ 
 obj_t BgL_auxz00_1903;
BgL_auxz00_1903 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18384L), BGl_string1607z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1310); 
FAILURE(BgL_auxz00_1903,BFALSE,BFALSE);} 
BgL_auxz00_1899 = 
REAL_TO_DOUBLE(BgL_tmpz00_1900); } 
{ /* Ieee/flonum.scm 407 */
 obj_t BgL_tmpz00_1891;
if(
REALP(BgL_r1z00_1309))
{ /* Ieee/flonum.scm 407 */
BgL_tmpz00_1891 = BgL_r1z00_1309
; }  else 
{ 
 obj_t BgL_auxz00_1894;
BgL_auxz00_1894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18384L), BGl_string1607z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1309); 
FAILURE(BgL_auxz00_1894,BFALSE,BFALSE);} 
BgL_auxz00_1890 = 
REAL_TO_DOUBLE(BgL_tmpz00_1891); } 
BgL_tmpz00_1889 = 
BGl_maxzd22flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1890, BgL_auxz00_1899); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1889);} } 

}



/* min-2fl */
BGL_EXPORTED_DEF double BGl_minzd22flzd2zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_31, double BgL_r2z00_32)
{
{ /* Ieee/flonum.scm 412 */
return 
BGL_FL_MIN2(BgL_r1z00_31, BgL_r2z00_32);} 

}



/* &min-2fl */
obj_t BGl_z62minzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1311, obj_t BgL_r1z00_1312, obj_t BgL_r2z00_1313)
{
{ /* Ieee/flonum.scm 412 */
{ /* Ieee/flonum.scm 413 */
 double BgL_tmpz00_1911;
{ /* Ieee/flonum.scm 413 */
 double BgL_auxz00_1921; double BgL_auxz00_1912;
{ /* Ieee/flonum.scm 413 */
 obj_t BgL_tmpz00_1922;
if(
REALP(BgL_r2z00_1313))
{ /* Ieee/flonum.scm 413 */
BgL_tmpz00_1922 = BgL_r2z00_1313
; }  else 
{ 
 obj_t BgL_auxz00_1925;
BgL_auxz00_1925 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18657L), BGl_string1608z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1313); 
FAILURE(BgL_auxz00_1925,BFALSE,BFALSE);} 
BgL_auxz00_1921 = 
REAL_TO_DOUBLE(BgL_tmpz00_1922); } 
{ /* Ieee/flonum.scm 413 */
 obj_t BgL_tmpz00_1913;
if(
REALP(BgL_r1z00_1312))
{ /* Ieee/flonum.scm 413 */
BgL_tmpz00_1913 = BgL_r1z00_1312
; }  else 
{ 
 obj_t BgL_auxz00_1916;
BgL_auxz00_1916 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18657L), BGl_string1608z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1312); 
FAILURE(BgL_auxz00_1916,BFALSE,BFALSE);} 
BgL_auxz00_1912 = 
REAL_TO_DOUBLE(BgL_tmpz00_1913); } 
BgL_tmpz00_1911 = 
BGl_minzd22flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1912, BgL_auxz00_1921); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1911);} } 

}



/* minfl */
BGL_EXPORTED_DEF double BGl_minflz00zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_33, obj_t BgL_rnz00_34)
{
{ /* Ieee/flonum.scm 418 */
{ 
 double BgL_minz00_1079; obj_t BgL_rnz00_1080;
BgL_minz00_1079 = BgL_r1z00_33; 
BgL_rnz00_1080 = BgL_rnz00_34; 
BgL_loopz00_1078:
if(
NULLP(BgL_rnz00_1080))
{ /* Ieee/flonum.scm 421 */
return BgL_minz00_1079;}  else 
{ 
 obj_t BgL_rnz00_1951; double BgL_minz00_1934;
{ /* Ieee/flonum.scm 423 */
 double BgL_r1z00_1090;
{ /* Ieee/flonum.scm 423 */
 obj_t BgL_pairz00_1089;
if(
PAIRP(BgL_rnz00_1080))
{ /* Ieee/flonum.scm 423 */
BgL_pairz00_1089 = BgL_rnz00_1080; }  else 
{ 
 obj_t BgL_auxz00_1937;
BgL_auxz00_1937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(19014L), BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BGl_string1605z00zz__r4_numbers_6_5_flonumz00, BgL_rnz00_1080); 
FAILURE(BgL_auxz00_1937,BFALSE,BFALSE);} 
{ /* Ieee/flonum.scm 423 */
 obj_t BgL_tmpz00_1941;
{ /* Ieee/flonum.scm 423 */
 obj_t BgL_aux1491z00_1465;
BgL_aux1491z00_1465 = 
CAR(BgL_pairz00_1089); 
if(
REALP(BgL_aux1491z00_1465))
{ /* Ieee/flonum.scm 423 */
BgL_tmpz00_1941 = BgL_aux1491z00_1465
; }  else 
{ 
 obj_t BgL_auxz00_1945;
BgL_auxz00_1945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(19009L), BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_aux1491z00_1465); 
FAILURE(BgL_auxz00_1945,BFALSE,BFALSE);} } 
BgL_r1z00_1090 = 
REAL_TO_DOUBLE(BgL_tmpz00_1941); } } 
BgL_minz00_1934 = 
BGL_FL_MIN2(BgL_r1z00_1090, BgL_minz00_1079); } 
{ /* Ieee/flonum.scm 423 */
 obj_t BgL_pairz00_1092;
if(
PAIRP(BgL_rnz00_1080))
{ /* Ieee/flonum.scm 423 */
BgL_pairz00_1092 = BgL_rnz00_1080; }  else 
{ 
 obj_t BgL_auxz00_1954;
BgL_auxz00_1954 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(19028L), BGl_string1604z00zz__r4_numbers_6_5_flonumz00, BGl_string1605z00zz__r4_numbers_6_5_flonumz00, BgL_rnz00_1080); 
FAILURE(BgL_auxz00_1954,BFALSE,BFALSE);} 
BgL_rnz00_1951 = 
CDR(BgL_pairz00_1092); } 
BgL_rnz00_1080 = BgL_rnz00_1951; 
BgL_minz00_1079 = BgL_minz00_1934; 
goto BgL_loopz00_1078;} } } 

}



/* &minfl */
obj_t BGl_z62minflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1314, obj_t BgL_r1z00_1315, obj_t BgL_rnz00_1316)
{
{ /* Ieee/flonum.scm 418 */
{ /* Ieee/flonum.scm 419 */
 double BgL_tmpz00_1959;
{ /* Ieee/flonum.scm 419 */
 double BgL_auxz00_1960;
{ /* Ieee/flonum.scm 419 */
 obj_t BgL_tmpz00_1961;
if(
REALP(BgL_r1z00_1315))
{ /* Ieee/flonum.scm 419 */
BgL_tmpz00_1961 = BgL_r1z00_1315
; }  else 
{ 
 obj_t BgL_auxz00_1964;
BgL_auxz00_1964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(18926L), BGl_string1609z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1315); 
FAILURE(BgL_auxz00_1964,BFALSE,BFALSE);} 
BgL_auxz00_1960 = 
REAL_TO_DOUBLE(BgL_tmpz00_1961); } 
BgL_tmpz00_1959 = 
BGl_minflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1960, BgL_rnz00_1316); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1959);} } 

}



/* absfl */
BGL_EXPORTED_DEF double BGl_absflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_35)
{
{ /* Ieee/flonum.scm 428 */
return 
fabs(BgL_rz00_35);} 

}



/* &absfl */
obj_t BGl_z62absflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1317, obj_t BgL_rz00_1318)
{
{ /* Ieee/flonum.scm 428 */
{ /* Ieee/flonum.scm 429 */
 double BgL_tmpz00_1972;
{ /* Ieee/flonum.scm 429 */
 double BgL_auxz00_1973;
{ /* Ieee/flonum.scm 429 */
 obj_t BgL_tmpz00_1974;
if(
REALP(BgL_rz00_1318))
{ /* Ieee/flonum.scm 429 */
BgL_tmpz00_1974 = BgL_rz00_1318
; }  else 
{ 
 obj_t BgL_auxz00_1977;
BgL_auxz00_1977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(19290L), BGl_string1610z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1318); 
FAILURE(BgL_auxz00_1977,BFALSE,BFALSE);} 
BgL_auxz00_1973 = 
REAL_TO_DOUBLE(BgL_tmpz00_1974); } 
BgL_tmpz00_1972 = 
BGl_absflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1973); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1972);} } 

}



/* floorfl */
BGL_EXPORTED_DEF double BGl_floorflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_36)
{
{ /* Ieee/flonum.scm 434 */
return 
floor(BgL_rz00_36);} 

}



/* &floorfl */
obj_t BGl_z62floorflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1319, obj_t BgL_rz00_1320)
{
{ /* Ieee/flonum.scm 434 */
{ /* Ieee/flonum.scm 435 */
 double BgL_tmpz00_1985;
{ /* Ieee/flonum.scm 435 */
 double BgL_auxz00_1986;
{ /* Ieee/flonum.scm 435 */
 obj_t BgL_tmpz00_1987;
if(
REALP(BgL_rz00_1320))
{ /* Ieee/flonum.scm 435 */
BgL_tmpz00_1987 = BgL_rz00_1320
; }  else 
{ 
 obj_t BgL_auxz00_1990;
BgL_auxz00_1990 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(19555L), BGl_string1611z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1320); 
FAILURE(BgL_auxz00_1990,BFALSE,BFALSE);} 
BgL_auxz00_1986 = 
REAL_TO_DOUBLE(BgL_tmpz00_1987); } 
BgL_tmpz00_1985 = 
BGl_floorflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1986); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1985);} } 

}



/* ceilingfl */
BGL_EXPORTED_DEF double BGl_ceilingflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_37)
{
{ /* Ieee/flonum.scm 440 */
return 
ceil(BgL_rz00_37);} 

}



/* &ceilingfl */
obj_t BGl_z62ceilingflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1321, obj_t BgL_rz00_1322)
{
{ /* Ieee/flonum.scm 440 */
{ /* Ieee/flonum.scm 441 */
 double BgL_tmpz00_1998;
{ /* Ieee/flonum.scm 441 */
 double BgL_auxz00_1999;
{ /* Ieee/flonum.scm 441 */
 obj_t BgL_tmpz00_2000;
if(
REALP(BgL_rz00_1322))
{ /* Ieee/flonum.scm 441 */
BgL_tmpz00_2000 = BgL_rz00_1322
; }  else 
{ 
 obj_t BgL_auxz00_2003;
BgL_auxz00_2003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(19822L), BGl_string1612z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1322); 
FAILURE(BgL_auxz00_2003,BFALSE,BFALSE);} 
BgL_auxz00_1999 = 
REAL_TO_DOUBLE(BgL_tmpz00_2000); } 
BgL_tmpz00_1998 = 
BGl_ceilingflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_1999); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_1998);} } 

}



/* truncatefl */
BGL_EXPORTED_DEF double BGl_truncateflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_38)
{
{ /* Ieee/flonum.scm 446 */
if(
(BgL_rz00_38<((double)0.0)))
{ /* Ieee/flonum.scm 447 */
return 
ceil(BgL_rz00_38);}  else 
{ /* Ieee/flonum.scm 447 */
return 
floor(BgL_rz00_38);} } 

}



/* &truncatefl */
obj_t BGl_z62truncateflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1323, obj_t BgL_rz00_1324)
{
{ /* Ieee/flonum.scm 446 */
{ /* Ieee/flonum.scm 447 */
 double BgL_tmpz00_2014;
{ /* Ieee/flonum.scm 447 */
 double BgL_auxz00_2015;
{ /* Ieee/flonum.scm 447 */
 obj_t BgL_tmpz00_2016;
if(
REALP(BgL_rz00_1324))
{ /* Ieee/flonum.scm 447 */
BgL_tmpz00_2016 = BgL_rz00_1324
; }  else 
{ 
 obj_t BgL_auxz00_2019;
BgL_auxz00_2019 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(20092L), BGl_string1613z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1324); 
FAILURE(BgL_auxz00_2019,BFALSE,BFALSE);} 
BgL_auxz00_2015 = 
REAL_TO_DOUBLE(BgL_tmpz00_2016); } 
BgL_tmpz00_2014 = 
BGl_truncateflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2015); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2014);} } 

}



/* roundfl */
BGL_EXPORTED_DEF double BGl_roundflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_39)
{
{ /* Ieee/flonum.scm 454 */
return 
BGL_FL_ROUND(BgL_rz00_39);} 

}



/* &roundfl */
obj_t BGl_z62roundflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1325, obj_t BgL_rz00_1326)
{
{ /* Ieee/flonum.scm 454 */
{ /* Ieee/flonum.scm 455 */
 double BgL_tmpz00_2027;
{ /* Ieee/flonum.scm 455 */
 double BgL_auxz00_2028;
{ /* Ieee/flonum.scm 455 */
 obj_t BgL_tmpz00_2029;
if(
REALP(BgL_rz00_1326))
{ /* Ieee/flonum.scm 455 */
BgL_tmpz00_2029 = BgL_rz00_1326
; }  else 
{ 
 obj_t BgL_auxz00_2032;
BgL_auxz00_2032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(20400L), BGl_string1614z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1326); 
FAILURE(BgL_auxz00_2032,BFALSE,BFALSE);} 
BgL_auxz00_2028 = 
REAL_TO_DOUBLE(BgL_tmpz00_2029); } 
BgL_tmpz00_2027 = 
BGl_roundflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2028); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2027);} } 

}



/* remainderfl */
BGL_EXPORTED_DEF double BGl_remainderflz00zz__r4_numbers_6_5_flonumz00(double BgL_n1z00_40, double BgL_n2z00_41)
{
{ /* Ieee/flonum.scm 460 */
return 
fmod(BgL_n1z00_40, BgL_n2z00_41);} 

}



/* &remainderfl */
obj_t BGl_z62remainderflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1327, obj_t BgL_n1z00_1328, obj_t BgL_n2z00_1329)
{
{ /* Ieee/flonum.scm 460 */
{ /* Ieee/flonum.scm 461 */
 double BgL_tmpz00_2040;
{ /* Ieee/flonum.scm 461 */
 double BgL_auxz00_2050; double BgL_auxz00_2041;
{ /* Ieee/flonum.scm 461 */
 obj_t BgL_tmpz00_2051;
if(
REALP(BgL_n2z00_1329))
{ /* Ieee/flonum.scm 461 */
BgL_tmpz00_2051 = BgL_n2z00_1329
; }  else 
{ 
 obj_t BgL_auxz00_2054;
BgL_auxz00_2054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(20675L), BGl_string1615z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_n2z00_1329); 
FAILURE(BgL_auxz00_2054,BFALSE,BFALSE);} 
BgL_auxz00_2050 = 
REAL_TO_DOUBLE(BgL_tmpz00_2051); } 
{ /* Ieee/flonum.scm 461 */
 obj_t BgL_tmpz00_2042;
if(
REALP(BgL_n1z00_1328))
{ /* Ieee/flonum.scm 461 */
BgL_tmpz00_2042 = BgL_n1z00_1328
; }  else 
{ 
 obj_t BgL_auxz00_2045;
BgL_auxz00_2045 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(20675L), BGl_string1615z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_n1z00_1328); 
FAILURE(BgL_auxz00_2045,BFALSE,BFALSE);} 
BgL_auxz00_2041 = 
REAL_TO_DOUBLE(BgL_tmpz00_2042); } 
BgL_tmpz00_2040 = 
BGl_remainderflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2041, BgL_auxz00_2050); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2040);} } 

}



/* expfl */
BGL_EXPORTED_DEF double BGl_expflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_42)
{
{ /* Ieee/flonum.scm 466 */
return 
exp(BgL_xz00_42);} 

}



/* &expfl */
obj_t BGl_z62expflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1330, obj_t BgL_xz00_1331)
{
{ /* Ieee/flonum.scm 466 */
{ /* Ieee/flonum.scm 467 */
 double BgL_tmpz00_2062;
{ /* Ieee/flonum.scm 467 */
 double BgL_auxz00_2063;
{ /* Ieee/flonum.scm 467 */
 obj_t BgL_tmpz00_2064;
if(
REALP(BgL_xz00_1331))
{ /* Ieee/flonum.scm 467 */
BgL_tmpz00_2064 = BgL_xz00_1331
; }  else 
{ 
 obj_t BgL_auxz00_2067;
BgL_auxz00_2067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(20941L), BGl_string1616z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1331); 
FAILURE(BgL_auxz00_2067,BFALSE,BFALSE);} 
BgL_auxz00_2063 = 
REAL_TO_DOUBLE(BgL_tmpz00_2064); } 
BgL_tmpz00_2062 = 
BGl_expflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2063); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2062);} } 

}



/* logfl */
BGL_EXPORTED_DEF double BGl_logflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_43)
{
{ /* Ieee/flonum.scm 472 */
return 
log(BgL_xz00_43);} 

}



/* &logfl */
obj_t BGl_z62logflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1332, obj_t BgL_xz00_1333)
{
{ /* Ieee/flonum.scm 472 */
{ /* Ieee/flonum.scm 473 */
 double BgL_tmpz00_2075;
{ /* Ieee/flonum.scm 473 */
 double BgL_auxz00_2076;
{ /* Ieee/flonum.scm 473 */
 obj_t BgL_tmpz00_2077;
if(
REALP(BgL_xz00_1333))
{ /* Ieee/flonum.scm 473 */
BgL_tmpz00_2077 = BgL_xz00_1333
; }  else 
{ 
 obj_t BgL_auxz00_2080;
BgL_auxz00_2080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(21202L), BGl_string1617z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1333); 
FAILURE(BgL_auxz00_2080,BFALSE,BFALSE);} 
BgL_auxz00_2076 = 
REAL_TO_DOUBLE(BgL_tmpz00_2077); } 
BgL_tmpz00_2075 = 
BGl_logflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2076); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2075);} } 

}



/* log2fl */
BGL_EXPORTED_DEF double BGl_log2flz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_44)
{
{ /* Ieee/flonum.scm 478 */
return 
log2(BgL_xz00_44);} 

}



/* &log2fl */
obj_t BGl_z62log2flz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1334, obj_t BgL_xz00_1335)
{
{ /* Ieee/flonum.scm 478 */
{ /* Ieee/flonum.scm 479 */
 double BgL_tmpz00_2088;
{ /* Ieee/flonum.scm 479 */
 double BgL_auxz00_2089;
{ /* Ieee/flonum.scm 479 */
 obj_t BgL_tmpz00_2090;
if(
REALP(BgL_xz00_1335))
{ /* Ieee/flonum.scm 479 */
BgL_tmpz00_2090 = BgL_xz00_1335
; }  else 
{ 
 obj_t BgL_auxz00_2093;
BgL_auxz00_2093 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(21465L), BGl_string1618z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1335); 
FAILURE(BgL_auxz00_2093,BFALSE,BFALSE);} 
BgL_auxz00_2089 = 
REAL_TO_DOUBLE(BgL_tmpz00_2090); } 
BgL_tmpz00_2088 = 
BGl_log2flz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2089); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2088);} } 

}



/* log10fl */
BGL_EXPORTED_DEF double BGl_log10flz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_45)
{
{ /* Ieee/flonum.scm 484 */
return 
log10(BgL_xz00_45);} 

}



/* &log10fl */
obj_t BGl_z62log10flz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1336, obj_t BgL_xz00_1337)
{
{ /* Ieee/flonum.scm 484 */
{ /* Ieee/flonum.scm 485 */
 double BgL_tmpz00_2101;
{ /* Ieee/flonum.scm 485 */
 double BgL_auxz00_2102;
{ /* Ieee/flonum.scm 485 */
 obj_t BgL_tmpz00_2103;
if(
REALP(BgL_xz00_1337))
{ /* Ieee/flonum.scm 485 */
BgL_tmpz00_2103 = BgL_xz00_1337
; }  else 
{ 
 obj_t BgL_auxz00_2106;
BgL_auxz00_2106 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(21730L), BGl_string1619z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1337); 
FAILURE(BgL_auxz00_2106,BFALSE,BFALSE);} 
BgL_auxz00_2102 = 
REAL_TO_DOUBLE(BgL_tmpz00_2103); } 
BgL_tmpz00_2101 = 
BGl_log10flz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2102); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2101);} } 

}



/* sinfl */
BGL_EXPORTED_DEF double BGl_sinflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_46)
{
{ /* Ieee/flonum.scm 490 */
return 
sin(BgL_xz00_46);} 

}



/* &sinfl */
obj_t BGl_z62sinflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1338, obj_t BgL_xz00_1339)
{
{ /* Ieee/flonum.scm 490 */
{ /* Ieee/flonum.scm 491 */
 double BgL_tmpz00_2114;
{ /* Ieee/flonum.scm 491 */
 double BgL_auxz00_2115;
{ /* Ieee/flonum.scm 491 */
 obj_t BgL_tmpz00_2116;
if(
REALP(BgL_xz00_1339))
{ /* Ieee/flonum.scm 491 */
BgL_tmpz00_2116 = BgL_xz00_1339
; }  else 
{ 
 obj_t BgL_auxz00_2119;
BgL_auxz00_2119 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(21994L), BGl_string1620z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1339); 
FAILURE(BgL_auxz00_2119,BFALSE,BFALSE);} 
BgL_auxz00_2115 = 
REAL_TO_DOUBLE(BgL_tmpz00_2116); } 
BgL_tmpz00_2114 = 
BGl_sinflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2115); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2114);} } 

}



/* cosfl */
BGL_EXPORTED_DEF double BGl_cosflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_47)
{
{ /* Ieee/flonum.scm 496 */
return 
cos(BgL_xz00_47);} 

}



/* &cosfl */
obj_t BGl_z62cosflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1340, obj_t BgL_xz00_1341)
{
{ /* Ieee/flonum.scm 496 */
{ /* Ieee/flonum.scm 497 */
 double BgL_tmpz00_2127;
{ /* Ieee/flonum.scm 497 */
 double BgL_auxz00_2128;
{ /* Ieee/flonum.scm 497 */
 obj_t BgL_tmpz00_2129;
if(
REALP(BgL_xz00_1341))
{ /* Ieee/flonum.scm 497 */
BgL_tmpz00_2129 = BgL_xz00_1341
; }  else 
{ 
 obj_t BgL_auxz00_2132;
BgL_auxz00_2132 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(22255L), BGl_string1621z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1341); 
FAILURE(BgL_auxz00_2132,BFALSE,BFALSE);} 
BgL_auxz00_2128 = 
REAL_TO_DOUBLE(BgL_tmpz00_2129); } 
BgL_tmpz00_2127 = 
BGl_cosflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2128); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2127);} } 

}



/* tanfl */
BGL_EXPORTED_DEF double BGl_tanflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_48)
{
{ /* Ieee/flonum.scm 502 */
return 
tan(BgL_xz00_48);} 

}



/* &tanfl */
obj_t BGl_z62tanflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1342, obj_t BgL_xz00_1343)
{
{ /* Ieee/flonum.scm 502 */
{ /* Ieee/flonum.scm 503 */
 double BgL_tmpz00_2140;
{ /* Ieee/flonum.scm 503 */
 double BgL_auxz00_2141;
{ /* Ieee/flonum.scm 503 */
 obj_t BgL_tmpz00_2142;
if(
REALP(BgL_xz00_1343))
{ /* Ieee/flonum.scm 503 */
BgL_tmpz00_2142 = BgL_xz00_1343
; }  else 
{ 
 obj_t BgL_auxz00_2145;
BgL_auxz00_2145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(22516L), BGl_string1622z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1343); 
FAILURE(BgL_auxz00_2145,BFALSE,BFALSE);} 
BgL_auxz00_2141 = 
REAL_TO_DOUBLE(BgL_tmpz00_2142); } 
BgL_tmpz00_2140 = 
BGl_tanflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2141); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2140);} } 

}



/* asinfl */
BGL_EXPORTED_DEF double BGl_asinflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_49)
{
{ /* Ieee/flonum.scm 508 */
return 
asin(BgL_xz00_49);} 

}



/* &asinfl */
obj_t BGl_z62asinflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1344, obj_t BgL_xz00_1345)
{
{ /* Ieee/flonum.scm 508 */
{ /* Ieee/flonum.scm 509 */
 double BgL_tmpz00_2153;
{ /* Ieee/flonum.scm 509 */
 double BgL_auxz00_2154;
{ /* Ieee/flonum.scm 509 */
 obj_t BgL_tmpz00_2155;
if(
REALP(BgL_xz00_1345))
{ /* Ieee/flonum.scm 509 */
BgL_tmpz00_2155 = BgL_xz00_1345
; }  else 
{ 
 obj_t BgL_auxz00_2158;
BgL_auxz00_2158 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(22778L), BGl_string1623z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1345); 
FAILURE(BgL_auxz00_2158,BFALSE,BFALSE);} 
BgL_auxz00_2154 = 
REAL_TO_DOUBLE(BgL_tmpz00_2155); } 
BgL_tmpz00_2153 = 
BGl_asinflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2154); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2153);} } 

}



/* acosfl */
BGL_EXPORTED_DEF double BGl_acosflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_50)
{
{ /* Ieee/flonum.scm 514 */
return 
acos(BgL_xz00_50);} 

}



/* &acosfl */
obj_t BGl_z62acosflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1346, obj_t BgL_xz00_1347)
{
{ /* Ieee/flonum.scm 514 */
{ /* Ieee/flonum.scm 515 */
 double BgL_tmpz00_2166;
{ /* Ieee/flonum.scm 515 */
 double BgL_auxz00_2167;
{ /* Ieee/flonum.scm 515 */
 obj_t BgL_tmpz00_2168;
if(
REALP(BgL_xz00_1347))
{ /* Ieee/flonum.scm 515 */
BgL_tmpz00_2168 = BgL_xz00_1347
; }  else 
{ 
 obj_t BgL_auxz00_2171;
BgL_auxz00_2171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23041L), BGl_string1624z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1347); 
FAILURE(BgL_auxz00_2171,BFALSE,BFALSE);} 
BgL_auxz00_2167 = 
REAL_TO_DOUBLE(BgL_tmpz00_2168); } 
BgL_tmpz00_2166 = 
BGl_acosflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2167); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2166);} } 

}



/* atanfl */
BGL_EXPORTED_DEF double BGl_atanflz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_51, obj_t BgL_yz00_52)
{
{ /* Ieee/flonum.scm 520 */
if(
NULLP(BgL_yz00_52))
{ /* Ieee/flonum.scm 521 */
return 
atan(BgL_xz00_51);}  else 
{ /* Ieee/flonum.scm 523 */
 obj_t BgL_yz00_1100;
{ /* Ieee/flonum.scm 523 */
 obj_t BgL_pairz00_1101;
if(
PAIRP(BgL_yz00_52))
{ /* Ieee/flonum.scm 523 */
BgL_pairz00_1101 = BgL_yz00_52; }  else 
{ 
 obj_t BgL_auxz00_2183;
BgL_auxz00_2183 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23353L), BGl_string1625z00zz__r4_numbers_6_5_flonumz00, BGl_string1605z00zz__r4_numbers_6_5_flonumz00, BgL_yz00_52); 
FAILURE(BgL_auxz00_2183,BFALSE,BFALSE);} 
BgL_yz00_1100 = 
CAR(BgL_pairz00_1101); } 
{ /* Ieee/flonum.scm 524 */
 double BgL_yz00_1103;
{ /* Ieee/flonum.scm 524 */
 obj_t BgL_tmpz00_2188;
if(
REALP(BgL_yz00_1100))
{ /* Ieee/flonum.scm 524 */
BgL_tmpz00_2188 = BgL_yz00_1100
; }  else 
{ 
 obj_t BgL_auxz00_2191;
BgL_auxz00_2191 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23361L), BGl_string1625z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_yz00_1100); 
FAILURE(BgL_auxz00_2191,BFALSE,BFALSE);} 
BgL_yz00_1103 = 
REAL_TO_DOUBLE(BgL_tmpz00_2188); } 
{ /* Ieee/flonum.scm 539 */
 bool_t BgL_test1851z00_2196;
if(
(BgL_xz00_51==((double)0.0)))
{ /* Ieee/flonum.scm 536 */
BgL_test1851z00_2196 = 
(BgL_yz00_1103==((double)0.0))
; }  else 
{ /* Ieee/flonum.scm 536 */
BgL_test1851z00_2196 = ((bool_t)0)
; } 
if(BgL_test1851z00_2196)
{ /* Ieee/flonum.scm 540 */
 obj_t BgL_procz00_1106; obj_t BgL_msgz00_1107; obj_t BgL_objz00_1108;
BgL_procz00_1106 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string1625z00zz__r4_numbers_6_5_flonumz00)); 
BgL_msgz00_1107 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string1626z00zz__r4_numbers_6_5_flonumz00)); 
BgL_objz00_1108 = BGL_REAL_CNST( BGl_real1627z00zz__r4_numbers_6_5_flonumz00); 
the_failure(BgL_procz00_1106, BgL_msgz00_1107, BgL_objz00_1108); 
return ((double)0.0);}  else 
{ /* Ieee/flonum.scm 539 */
return 
atan2(BgL_xz00_51, BgL_yz00_1103);} } } } } 

}



/* &atanfl */
obj_t BGl_z62atanflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1348, obj_t BgL_xz00_1349, obj_t BgL_yz00_1350)
{
{ /* Ieee/flonum.scm 520 */
{ /* Ieee/flonum.scm 521 */
 double BgL_tmpz00_2206;
{ /* Ieee/flonum.scm 521 */
 double BgL_auxz00_2207;
{ /* Ieee/flonum.scm 521 */
 obj_t BgL_tmpz00_2208;
if(
REALP(BgL_xz00_1349))
{ /* Ieee/flonum.scm 521 */
BgL_tmpz00_2208 = BgL_xz00_1349
; }  else 
{ 
 obj_t BgL_auxz00_2211;
BgL_auxz00_2211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23301L), BGl_string1628z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1349); 
FAILURE(BgL_auxz00_2211,BFALSE,BFALSE);} 
BgL_auxz00_2207 = 
REAL_TO_DOUBLE(BgL_tmpz00_2208); } 
BgL_tmpz00_2206 = 
BGl_atanflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2207, BgL_yz00_1350); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2206);} } 

}



/* atan-1fl */
BGL_EXPORTED_DEF double BGl_atanzd21flzd2zz__r4_numbers_6_5_flonumz00(double BgL_xz00_53)
{
{ /* Ieee/flonum.scm 529 */
return 
atan(BgL_xz00_53);} 

}



/* &atan-1fl */
obj_t BGl_z62atanzd21flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1351, obj_t BgL_xz00_1352)
{
{ /* Ieee/flonum.scm 529 */
{ /* Ieee/flonum.scm 530 */
 double BgL_tmpz00_2219;
{ /* Ieee/flonum.scm 530 */
 double BgL_auxz00_2220;
{ /* Ieee/flonum.scm 530 */
 obj_t BgL_tmpz00_2221;
if(
REALP(BgL_xz00_1352))
{ /* Ieee/flonum.scm 530 */
BgL_tmpz00_2221 = BgL_xz00_1352
; }  else 
{ 
 obj_t BgL_auxz00_2224;
BgL_auxz00_2224 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23633L), BGl_string1629z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1352); 
FAILURE(BgL_auxz00_2224,BFALSE,BFALSE);} 
BgL_auxz00_2220 = 
REAL_TO_DOUBLE(BgL_tmpz00_2221); } 
BgL_tmpz00_2219 = 
BGl_atanzd21flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2220); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2219);} } 

}



/* atan-2fl */
BGL_EXPORTED_DEF double BGl_atanzd22flzd2zz__r4_numbers_6_5_flonumz00(double BgL_xz00_54, double BgL_yz00_55)
{
{ /* Ieee/flonum.scm 535 */
{ /* Ieee/flonum.scm 539 */
 bool_t BgL_test1855z00_2231;
if(
(BgL_xz00_54==((double)0.0)))
{ /* Ieee/flonum.scm 536 */
BgL_test1855z00_2231 = 
(BgL_yz00_55==((double)0.0))
; }  else 
{ /* Ieee/flonum.scm 536 */
BgL_test1855z00_2231 = ((bool_t)0)
; } 
if(BgL_test1855z00_2231)
{ /* Ieee/flonum.scm 540 */
 obj_t BgL_procz00_1562; obj_t BgL_msgz00_1563; obj_t BgL_objz00_1564;
BgL_procz00_1562 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string1625z00zz__r4_numbers_6_5_flonumz00)); 
BgL_msgz00_1563 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string1626z00zz__r4_numbers_6_5_flonumz00)); 
BgL_objz00_1564 = BGL_REAL_CNST( BGl_real1627z00zz__r4_numbers_6_5_flonumz00); 
the_failure(BgL_procz00_1562, BgL_msgz00_1563, BgL_objz00_1564); 
return ((double)0.0);}  else 
{ /* Ieee/flonum.scm 539 */
return 
atan2(BgL_xz00_54, BgL_yz00_55);} } } 

}



/* &atan-2fl */
obj_t BGl_z62atanzd22flzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1353, obj_t BgL_xz00_1354, obj_t BgL_yz00_1355)
{
{ /* Ieee/flonum.scm 535 */
{ /* Ieee/flonum.scm 536 */
 double BgL_tmpz00_2241;
{ /* Ieee/flonum.scm 536 */
 double BgL_auxz00_2251; double BgL_auxz00_2242;
{ /* Ieee/flonum.scm 536 */
 obj_t BgL_tmpz00_2252;
if(
REALP(BgL_yz00_1355))
{ /* Ieee/flonum.scm 536 */
BgL_tmpz00_2252 = BgL_yz00_1355
; }  else 
{ 
 obj_t BgL_auxz00_2255;
BgL_auxz00_2255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23900L), BGl_string1630z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_yz00_1355); 
FAILURE(BgL_auxz00_2255,BFALSE,BFALSE);} 
BgL_auxz00_2251 = 
REAL_TO_DOUBLE(BgL_tmpz00_2252); } 
{ /* Ieee/flonum.scm 536 */
 obj_t BgL_tmpz00_2243;
if(
REALP(BgL_xz00_1354))
{ /* Ieee/flonum.scm 536 */
BgL_tmpz00_2243 = BgL_xz00_1354
; }  else 
{ 
 obj_t BgL_auxz00_2246;
BgL_auxz00_2246 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(23900L), BGl_string1630z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1354); 
FAILURE(BgL_auxz00_2246,BFALSE,BFALSE);} 
BgL_auxz00_2242 = 
REAL_TO_DOUBLE(BgL_tmpz00_2243); } 
BgL_tmpz00_2241 = 
BGl_atanzd22flzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2242, BgL_auxz00_2251); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2241);} } 

}



/* atan-2fl-ur */
BGL_EXPORTED_DEF double BGl_atanzd22flzd2urz00zz__r4_numbers_6_5_flonumz00(double BgL_xz00_56, double BgL_yz00_57)
{
{ /* Ieee/flonum.scm 553 */
return 
atan2(BgL_xz00_56, BgL_yz00_57);} 

}



/* &atan-2fl-ur */
obj_t BGl_z62atanzd22flzd2urz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1356, obj_t BgL_xz00_1357, obj_t BgL_yz00_1358)
{
{ /* Ieee/flonum.scm 553 */
{ /* Ieee/flonum.scm 554 */
 double BgL_tmpz00_2263;
{ /* Ieee/flonum.scm 554 */
 double BgL_auxz00_2273; double BgL_auxz00_2264;
{ /* Ieee/flonum.scm 554 */
 obj_t BgL_tmpz00_2274;
if(
REALP(BgL_yz00_1358))
{ /* Ieee/flonum.scm 554 */
BgL_tmpz00_2274 = BgL_yz00_1358
; }  else 
{ 
 obj_t BgL_auxz00_2277;
BgL_auxz00_2277 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(24538L), BGl_string1631z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_yz00_1358); 
FAILURE(BgL_auxz00_2277,BFALSE,BFALSE);} 
BgL_auxz00_2273 = 
REAL_TO_DOUBLE(BgL_tmpz00_2274); } 
{ /* Ieee/flonum.scm 554 */
 obj_t BgL_tmpz00_2265;
if(
REALP(BgL_xz00_1357))
{ /* Ieee/flonum.scm 554 */
BgL_tmpz00_2265 = BgL_xz00_1357
; }  else 
{ 
 obj_t BgL_auxz00_2268;
BgL_auxz00_2268 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(24538L), BGl_string1631z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_xz00_1357); 
FAILURE(BgL_auxz00_2268,BFALSE,BFALSE);} 
BgL_auxz00_2264 = 
REAL_TO_DOUBLE(BgL_tmpz00_2265); } 
BgL_tmpz00_2263 = 
BGl_atanzd22flzd2urz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2264, BgL_auxz00_2273); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2263);} } 

}



/* sqrtfl */
BGL_EXPORTED_DEF double BGl_sqrtflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_58)
{
{ /* Ieee/flonum.scm 559 */
if(
(BgL_rz00_58<((double)0.0)))
{ /* Ieee/flonum.scm 561 */
 obj_t BgL_procz00_1565; obj_t BgL_msgz00_1566; obj_t BgL_objz00_1567;
BgL_procz00_1565 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string1632z00zz__r4_numbers_6_5_flonumz00)); 
BgL_msgz00_1566 = 
string_to_bstring(
BSTRING_TO_STRING(BGl_string1626z00zz__r4_numbers_6_5_flonumz00)); 
BgL_objz00_1567 = 
DOUBLE_TO_REAL(BgL_rz00_58); 
BGl_errorz00zz__errorz00(BgL_procz00_1565, BgL_msgz00_1566, BgL_objz00_1567); 
return ((double)0.0);}  else 
{ /* Ieee/flonum.scm 560 */
return 
sqrt(BgL_rz00_58);} } 

}



/* &sqrtfl */
obj_t BGl_z62sqrtflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1359, obj_t BgL_rz00_1360)
{
{ /* Ieee/flonum.scm 559 */
{ /* Ieee/flonum.scm 560 */
 double BgL_tmpz00_2293;
{ /* Ieee/flonum.scm 560 */
 double BgL_auxz00_2294;
{ /* Ieee/flonum.scm 560 */
 obj_t BgL_tmpz00_2295;
if(
REALP(BgL_rz00_1360))
{ /* Ieee/flonum.scm 560 */
BgL_tmpz00_2295 = BgL_rz00_1360
; }  else 
{ 
 obj_t BgL_auxz00_2298;
BgL_auxz00_2298 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(24804L), BGl_string1633z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1360); 
FAILURE(BgL_auxz00_2298,BFALSE,BFALSE);} 
BgL_auxz00_2294 = 
REAL_TO_DOUBLE(BgL_tmpz00_2295); } 
BgL_tmpz00_2293 = 
BGl_sqrtflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2294); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2293);} } 

}



/* sqrtfl-ur */
BGL_EXPORTED_DEF double BGl_sqrtflzd2urzd2zz__r4_numbers_6_5_flonumz00(double BgL_rz00_59)
{
{ /* Ieee/flonum.scm 572 */
return 
sqrt(BgL_rz00_59);} 

}



/* &sqrtfl-ur */
obj_t BGl_z62sqrtflzd2urzb0zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1361, obj_t BgL_rz00_1362)
{
{ /* Ieee/flonum.scm 572 */
{ /* Ieee/flonum.scm 573 */
 double BgL_tmpz00_2306;
{ /* Ieee/flonum.scm 573 */
 double BgL_auxz00_2307;
{ /* Ieee/flonum.scm 573 */
 obj_t BgL_tmpz00_2308;
if(
REALP(BgL_rz00_1362))
{ /* Ieee/flonum.scm 573 */
BgL_tmpz00_2308 = BgL_rz00_1362
; }  else 
{ 
 obj_t BgL_auxz00_2311;
BgL_auxz00_2311 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(25283L), BGl_string1634z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1362); 
FAILURE(BgL_auxz00_2311,BFALSE,BFALSE);} 
BgL_auxz00_2307 = 
REAL_TO_DOUBLE(BgL_tmpz00_2308); } 
BgL_tmpz00_2306 = 
BGl_sqrtflzd2urzd2zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2307); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2306);} } 

}



/* exptfl */
BGL_EXPORTED_DEF double BGl_exptflz00zz__r4_numbers_6_5_flonumz00(double BgL_r1z00_60, double BgL_r2z00_61)
{
{ /* Ieee/flonum.scm 578 */
return 
pow(BgL_r1z00_60, BgL_r2z00_61);} 

}



/* &exptfl */
obj_t BGl_z62exptflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1363, obj_t BgL_r1z00_1364, obj_t BgL_r2z00_1365)
{
{ /* Ieee/flonum.scm 578 */
{ /* Ieee/flonum.scm 579 */
 double BgL_tmpz00_2319;
{ /* Ieee/flonum.scm 579 */
 double BgL_auxz00_2329; double BgL_auxz00_2320;
{ /* Ieee/flonum.scm 579 */
 obj_t BgL_tmpz00_2330;
if(
REALP(BgL_r2z00_1365))
{ /* Ieee/flonum.scm 579 */
BgL_tmpz00_2330 = BgL_r2z00_1365
; }  else 
{ 
 obj_t BgL_auxz00_2333;
BgL_auxz00_2333 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(25550L), BGl_string1635z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r2z00_1365); 
FAILURE(BgL_auxz00_2333,BFALSE,BFALSE);} 
BgL_auxz00_2329 = 
REAL_TO_DOUBLE(BgL_tmpz00_2330); } 
{ /* Ieee/flonum.scm 579 */
 obj_t BgL_tmpz00_2321;
if(
REALP(BgL_r1z00_1364))
{ /* Ieee/flonum.scm 579 */
BgL_tmpz00_2321 = BgL_r1z00_1364
; }  else 
{ 
 obj_t BgL_auxz00_2324;
BgL_auxz00_2324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(25550L), BGl_string1635z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_r1z00_1364); 
FAILURE(BgL_auxz00_2324,BFALSE,BFALSE);} 
BgL_auxz00_2320 = 
REAL_TO_DOUBLE(BgL_tmpz00_2321); } 
BgL_tmpz00_2319 = 
BGl_exptflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2320, BgL_auxz00_2329); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2319);} } 

}



/* signbitfl */
BGL_EXPORTED_DEF long BGl_signbitflz00zz__r4_numbers_6_5_flonumz00(double BgL_rz00_62)
{
{ /* Ieee/flonum.scm 584 */
return 
BGL_SIGNBIT(BgL_rz00_62);} 

}



/* &signbitfl */
obj_t BGl_z62signbitflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1366, obj_t BgL_rz00_1367)
{
{ /* Ieee/flonum.scm 584 */
{ /* Ieee/flonum.scm 585 */
 long BgL_tmpz00_2341;
{ /* Ieee/flonum.scm 585 */
 double BgL_auxz00_2342;
{ /* Ieee/flonum.scm 585 */
 obj_t BgL_tmpz00_2343;
if(
REALP(BgL_rz00_1367))
{ /* Ieee/flonum.scm 585 */
BgL_tmpz00_2343 = BgL_rz00_1367
; }  else 
{ 
 obj_t BgL_auxz00_2346;
BgL_auxz00_2346 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(25819L), BGl_string1636z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1367); 
FAILURE(BgL_auxz00_2346,BFALSE,BFALSE);} 
BgL_auxz00_2342 = 
REAL_TO_DOUBLE(BgL_tmpz00_2343); } 
BgL_tmpz00_2341 = 
BGl_signbitflz00zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2342); } 
return 
BINT(BgL_tmpz00_2341);} } 

}



/* integerfl? */
BGL_EXPORTED_DEF bool_t BGl_integerflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_63)
{
{ /* Ieee/flonum.scm 590 */
if(
BGL_IS_FINITE(BgL_rz00_63))
{ /* Ieee/flonum.scm 591 */
 double BgL_arg1074z00_1568;
BgL_arg1074z00_1568 = 
floor(BgL_rz00_63); 
return 
(BgL_rz00_63==BgL_arg1074z00_1568);}  else 
{ /* Ieee/flonum.scm 591 */
return ((bool_t)0);} } 

}



/* &integerfl? */
obj_t BGl_z62integerflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1368, obj_t BgL_rz00_1369)
{
{ /* Ieee/flonum.scm 590 */
{ /* Ieee/flonum.scm 591 */
 bool_t BgL_tmpz00_2357;
{ /* Ieee/flonum.scm 591 */
 double BgL_auxz00_2358;
{ /* Ieee/flonum.scm 591 */
 obj_t BgL_tmpz00_2359;
if(
REALP(BgL_rz00_1369))
{ /* Ieee/flonum.scm 591 */
BgL_tmpz00_2359 = BgL_rz00_1369
; }  else 
{ 
 obj_t BgL_auxz00_2362;
BgL_auxz00_2362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(26094L), BGl_string1637z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1369); 
FAILURE(BgL_auxz00_2362,BFALSE,BFALSE);} 
BgL_auxz00_2358 = 
REAL_TO_DOUBLE(BgL_tmpz00_2359); } 
BgL_tmpz00_2357 = 
BGl_integerflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2358); } 
return 
BBOOL(BgL_tmpz00_2357);} } 

}



/* evenfl? */
BGL_EXPORTED_DEF bool_t BGl_evenflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_64)
{
{ /* Ieee/flonum.scm 596 */
{ /* Ieee/flonum.scm 597 */
 double BgL_arg1075z00_1569;
BgL_arg1075z00_1569 = 
(BgL_rz00_64/((double)2.0)); 
if(
BGL_IS_FINITE(BgL_arg1075z00_1569))
{ /* Ieee/flonum.scm 591 */
 double BgL_arg1074z00_1570;
BgL_arg1074z00_1570 = 
floor(BgL_arg1075z00_1569); 
return 
(BgL_arg1075z00_1569==BgL_arg1074z00_1570);}  else 
{ /* Ieee/flonum.scm 591 */
return ((bool_t)0);} } } 

}



/* &evenfl? */
obj_t BGl_z62evenflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1370, obj_t BgL_rz00_1371)
{
{ /* Ieee/flonum.scm 596 */
{ /* Ieee/flonum.scm 597 */
 bool_t BgL_tmpz00_2374;
{ /* Ieee/flonum.scm 597 */
 double BgL_auxz00_2375;
{ /* Ieee/flonum.scm 597 */
 obj_t BgL_tmpz00_2376;
if(
REALP(BgL_rz00_1371))
{ /* Ieee/flonum.scm 597 */
BgL_tmpz00_2376 = BgL_rz00_1371
; }  else 
{ 
 obj_t BgL_auxz00_2379;
BgL_auxz00_2379 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(26395L), BGl_string1638z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1371); 
FAILURE(BgL_auxz00_2379,BFALSE,BFALSE);} 
BgL_auxz00_2375 = 
REAL_TO_DOUBLE(BgL_tmpz00_2376); } 
BgL_tmpz00_2374 = 
BGl_evenflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2375); } 
return 
BBOOL(BgL_tmpz00_2374);} } 

}



/* oddfl? */
BGL_EXPORTED_DEF bool_t BGl_oddflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_65)
{
{ /* Ieee/flonum.scm 602 */
{ /* Ieee/flonum.scm 603 */
 bool_t BgL_test1871z00_2386;
if(
BGL_IS_FINITE(BgL_rz00_65))
{ /* Ieee/flonum.scm 591 */
 double BgL_arg1074z00_1571;
BgL_arg1074z00_1571 = 
floor(BgL_rz00_65); 
BgL_test1871z00_2386 = 
(BgL_rz00_65==BgL_arg1074z00_1571); }  else 
{ /* Ieee/flonum.scm 591 */
BgL_test1871z00_2386 = ((bool_t)0)
; } 
if(BgL_test1871z00_2386)
{ /* Ieee/flonum.scm 604 */
 bool_t BgL_test1873z00_2391;
{ /* Ieee/flonum.scm 597 */
 double BgL_arg1075z00_1572;
BgL_arg1075z00_1572 = 
(BgL_rz00_65/((double)2.0)); 
if(
BGL_IS_FINITE(BgL_arg1075z00_1572))
{ /* Ieee/flonum.scm 591 */
 double BgL_arg1074z00_1573;
BgL_arg1074z00_1573 = 
floor(BgL_arg1075z00_1572); 
BgL_test1873z00_2391 = 
(BgL_arg1075z00_1572==BgL_arg1074z00_1573); }  else 
{ /* Ieee/flonum.scm 591 */
BgL_test1873z00_2391 = ((bool_t)0)
; } } 
if(BgL_test1873z00_2391)
{ /* Ieee/flonum.scm 604 */
return ((bool_t)0);}  else 
{ /* Ieee/flonum.scm 604 */
return ((bool_t)1);} }  else 
{ /* Ieee/flonum.scm 603 */
return ((bool_t)0);} } } 

}



/* &oddfl? */
obj_t BGl_z62oddflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1372, obj_t BgL_rz00_1373)
{
{ /* Ieee/flonum.scm 602 */
{ /* Ieee/flonum.scm 603 */
 bool_t BgL_tmpz00_2397;
{ /* Ieee/flonum.scm 603 */
 double BgL_auxz00_2398;
{ /* Ieee/flonum.scm 603 */
 obj_t BgL_tmpz00_2399;
if(
REALP(BgL_rz00_1373))
{ /* Ieee/flonum.scm 603 */
BgL_tmpz00_2399 = BgL_rz00_1373
; }  else 
{ 
 obj_t BgL_auxz00_2402;
BgL_auxz00_2402 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(26666L), BGl_string1639z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1373); 
FAILURE(BgL_auxz00_2402,BFALSE,BFALSE);} 
BgL_auxz00_2398 = 
REAL_TO_DOUBLE(BgL_tmpz00_2399); } 
BgL_tmpz00_2397 = 
BGl_oddflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2398); } 
return 
BBOOL(BgL_tmpz00_2397);} } 

}



/* finitefl? */
BGL_EXPORTED_DEF bool_t BGl_finiteflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_66)
{
{ /* Ieee/flonum.scm 609 */
return 
BGL_IS_FINITE(BgL_rz00_66);} 

}



/* &finitefl? */
obj_t BGl_z62finiteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1374, obj_t BgL_rz00_1375)
{
{ /* Ieee/flonum.scm 609 */
{ /* Ieee/flonum.scm 610 */
 bool_t BgL_tmpz00_2410;
{ /* Ieee/flonum.scm 610 */
 double BgL_auxz00_2411;
{ /* Ieee/flonum.scm 610 */
 obj_t BgL_tmpz00_2412;
if(
REALP(BgL_rz00_1375))
{ /* Ieee/flonum.scm 610 */
BgL_tmpz00_2412 = BgL_rz00_1375
; }  else 
{ 
 obj_t BgL_auxz00_2415;
BgL_auxz00_2415 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(26957L), BGl_string1640z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1375); 
FAILURE(BgL_auxz00_2415,BFALSE,BFALSE);} 
BgL_auxz00_2411 = 
REAL_TO_DOUBLE(BgL_tmpz00_2412); } 
BgL_tmpz00_2410 = 
BGl_finiteflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2411); } 
return 
BBOOL(BgL_tmpz00_2410);} } 

}



/* infinitefl? */
BGL_EXPORTED_DEF bool_t BGl_infiniteflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_67)
{
{ /* Ieee/flonum.scm 615 */
return 
BGL_IS_INF(BgL_rz00_67);} 

}



/* &infinitefl? */
obj_t BGl_z62infiniteflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1376, obj_t BgL_rz00_1377)
{
{ /* Ieee/flonum.scm 615 */
{ /* Ieee/flonum.scm 616 */
 bool_t BgL_tmpz00_2423;
{ /* Ieee/flonum.scm 616 */
 double BgL_auxz00_2424;
{ /* Ieee/flonum.scm 616 */
 obj_t BgL_tmpz00_2425;
if(
REALP(BgL_rz00_1377))
{ /* Ieee/flonum.scm 616 */
BgL_tmpz00_2425 = BgL_rz00_1377
; }  else 
{ 
 obj_t BgL_auxz00_2428;
BgL_auxz00_2428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(27229L), BGl_string1641z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1377); 
FAILURE(BgL_auxz00_2428,BFALSE,BFALSE);} 
BgL_auxz00_2424 = 
REAL_TO_DOUBLE(BgL_tmpz00_2425); } 
BgL_tmpz00_2423 = 
BGl_infiniteflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2424); } 
return 
BBOOL(BgL_tmpz00_2423);} } 

}



/* nanfl? */
BGL_EXPORTED_DEF bool_t BGl_nanflzf3zf3zz__r4_numbers_6_5_flonumz00(double BgL_rz00_68)
{
{ /* Ieee/flonum.scm 621 */
return 
BGL_IS_NAN(BgL_rz00_68);} 

}



/* &nanfl? */
obj_t BGl_z62nanflzf3z91zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1378, obj_t BgL_rz00_1379)
{
{ /* Ieee/flonum.scm 621 */
{ /* Ieee/flonum.scm 622 */
 bool_t BgL_tmpz00_2436;
{ /* Ieee/flonum.scm 622 */
 double BgL_auxz00_2437;
{ /* Ieee/flonum.scm 622 */
 obj_t BgL_tmpz00_2438;
if(
REALP(BgL_rz00_1379))
{ /* Ieee/flonum.scm 622 */
BgL_tmpz00_2438 = BgL_rz00_1379
; }  else 
{ 
 obj_t BgL_auxz00_2441;
BgL_auxz00_2441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(27493L), BGl_string1642z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_rz00_1379); 
FAILURE(BgL_auxz00_2441,BFALSE,BFALSE);} 
BgL_auxz00_2437 = 
REAL_TO_DOUBLE(BgL_tmpz00_2438); } 
BgL_tmpz00_2436 = 
BGl_nanflzf3zf3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2437); } 
return 
BBOOL(BgL_tmpz00_2436);} } 

}



/* string->real */
BGL_EXPORTED_DEF double BGl_stringzd2ze3realz31zz__r4_numbers_6_5_flonumz00(char * BgL_stringz00_69)
{
{ /* Ieee/flonum.scm 627 */
{ /* Ieee/flonum.scm 629 */
 bool_t BgL_test1879z00_2448;
{ /* Ieee/flonum.scm 629 */
 obj_t BgL_string1z00_1574;
BgL_string1z00_1574 = 
string_to_bstring(BgL_stringz00_69); 
{ /* Ieee/flonum.scm 629 */
 long BgL_l1z00_1575;
BgL_l1z00_1575 = 
STRING_LENGTH(BgL_string1z00_1574); 
if(
(BgL_l1z00_1575==6L))
{ /* Ieee/flonum.scm 629 */
 int BgL_arg1424z00_1576;
{ /* Ieee/flonum.scm 629 */
 char * BgL_auxz00_2455; char * BgL_tmpz00_2453;
BgL_auxz00_2455 = 
BSTRING_TO_STRING(BGl_string1643z00zz__r4_numbers_6_5_flonumz00); 
BgL_tmpz00_2453 = 
BSTRING_TO_STRING(BgL_string1z00_1574); 
BgL_arg1424z00_1576 = 
memcmp(BgL_tmpz00_2453, BgL_auxz00_2455, BgL_l1z00_1575); } 
BgL_test1879z00_2448 = 
(
(long)(BgL_arg1424z00_1576)==0L); }  else 
{ /* Ieee/flonum.scm 629 */
BgL_test1879z00_2448 = ((bool_t)0)
; } } } 
if(BgL_test1879z00_2448)
{ /* Ieee/flonum.scm 629 */
return BGL_NAN;}  else 
{ /* Ieee/flonum.scm 631 */
 bool_t BgL_test1881z00_2460;
{ /* Ieee/flonum.scm 631 */
 obj_t BgL_string1z00_1577;
BgL_string1z00_1577 = 
string_to_bstring(BgL_stringz00_69); 
{ /* Ieee/flonum.scm 631 */
 long BgL_l1z00_1578;
BgL_l1z00_1578 = 
STRING_LENGTH(BgL_string1z00_1577); 
if(
(BgL_l1z00_1578==6L))
{ /* Ieee/flonum.scm 631 */
 int BgL_arg1424z00_1579;
{ /* Ieee/flonum.scm 631 */
 char * BgL_auxz00_2467; char * BgL_tmpz00_2465;
BgL_auxz00_2467 = 
BSTRING_TO_STRING(BGl_string1644z00zz__r4_numbers_6_5_flonumz00); 
BgL_tmpz00_2465 = 
BSTRING_TO_STRING(BgL_string1z00_1577); 
BgL_arg1424z00_1579 = 
memcmp(BgL_tmpz00_2465, BgL_auxz00_2467, BgL_l1z00_1578); } 
BgL_test1881z00_2460 = 
(
(long)(BgL_arg1424z00_1579)==0L); }  else 
{ /* Ieee/flonum.scm 631 */
BgL_test1881z00_2460 = ((bool_t)0)
; } } } 
if(BgL_test1881z00_2460)
{ /* Ieee/flonum.scm 631 */
return BGL_INFINITY;}  else 
{ /* Ieee/flonum.scm 633 */
 bool_t BgL_test1883z00_2472;
{ /* Ieee/flonum.scm 633 */
 obj_t BgL_string1z00_1580;
BgL_string1z00_1580 = 
string_to_bstring(BgL_stringz00_69); 
{ /* Ieee/flonum.scm 633 */
 long BgL_l1z00_1581;
BgL_l1z00_1581 = 
STRING_LENGTH(BgL_string1z00_1580); 
if(
(BgL_l1z00_1581==6L))
{ /* Ieee/flonum.scm 633 */
 int BgL_arg1424z00_1582;
{ /* Ieee/flonum.scm 633 */
 char * BgL_auxz00_2479; char * BgL_tmpz00_2477;
BgL_auxz00_2479 = 
BSTRING_TO_STRING(BGl_string1645z00zz__r4_numbers_6_5_flonumz00); 
BgL_tmpz00_2477 = 
BSTRING_TO_STRING(BgL_string1z00_1580); 
BgL_arg1424z00_1582 = 
memcmp(BgL_tmpz00_2477, BgL_auxz00_2479, BgL_l1z00_1581); } 
BgL_test1883z00_2472 = 
(
(long)(BgL_arg1424z00_1582)==0L); }  else 
{ /* Ieee/flonum.scm 633 */
BgL_test1883z00_2472 = ((bool_t)0)
; } } } 
if(BgL_test1883z00_2472)
{ /* Ieee/flonum.scm 633 */
return (-BGL_INFINITY);}  else 
{ /* Ieee/flonum.scm 633 */
return 
STRTOD(BgL_stringz00_69);} } } } } 

}



/* &string->real */
obj_t BGl_z62stringzd2ze3realz53zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1380, obj_t BgL_stringz00_1381)
{
{ /* Ieee/flonum.scm 627 */
{ /* Ieee/flonum.scm 629 */
 double BgL_tmpz00_2485;
{ /* Ieee/flonum.scm 629 */
 char * BgL_auxz00_2486;
{ /* Ieee/flonum.scm 629 */
 obj_t BgL_tmpz00_2487;
if(
STRINGP(BgL_stringz00_1381))
{ /* Ieee/flonum.scm 629 */
BgL_tmpz00_2487 = BgL_stringz00_1381
; }  else 
{ 
 obj_t BgL_auxz00_2490;
BgL_auxz00_2490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(27780L), BGl_string1646z00zz__r4_numbers_6_5_flonumz00, BGl_string1647z00zz__r4_numbers_6_5_flonumz00, BgL_stringz00_1381); 
FAILURE(BgL_auxz00_2490,BFALSE,BFALSE);} 
BgL_auxz00_2486 = 
BSTRING_TO_STRING(BgL_tmpz00_2487); } 
BgL_tmpz00_2485 = 
BGl_stringzd2ze3realz31zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2486); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2485);} } 

}



/* ieee-string->real */
BGL_EXPORTED_DEF obj_t BGl_ieeezd2stringzd2ze3realze3zz__r4_numbers_6_5_flonumz00(obj_t BgL_stringz00_70)
{
{ /* Ieee/flonum.scm 641 */
return 
DOUBLE_TO_REAL(
bgl_ieee_string_to_double(BgL_stringz00_70));} 

}



/* &ieee-string->real */
obj_t BGl_z62ieeezd2stringzd2ze3realz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1382, obj_t BgL_stringz00_1383)
{
{ /* Ieee/flonum.scm 641 */
{ /* Ieee/flonum.scm 642 */
 obj_t BgL_auxz00_2499;
if(
STRINGP(BgL_stringz00_1383))
{ /* Ieee/flonum.scm 642 */
BgL_auxz00_2499 = BgL_stringz00_1383
; }  else 
{ 
 obj_t BgL_auxz00_2502;
BgL_auxz00_2502 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(28228L), BGl_string1648z00zz__r4_numbers_6_5_flonumz00, BGl_string1647z00zz__r4_numbers_6_5_flonumz00, BgL_stringz00_1383); 
FAILURE(BgL_auxz00_2502,BFALSE,BFALSE);} 
return 
BGl_ieeezd2stringzd2ze3realze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2499);} } 

}



/* real->ieee-string */
BGL_EXPORTED_DEF obj_t BGl_realzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(obj_t BgL_realz00_71)
{
{ /* Ieee/flonum.scm 647 */
return 
bgl_double_to_ieee_string(
REAL_TO_DOUBLE(BgL_realz00_71));} 

}



/* &real->ieee-string */
obj_t BGl_z62realzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1384, obj_t BgL_realz00_1385)
{
{ /* Ieee/flonum.scm 647 */
{ /* Ieee/flonum.scm 648 */
 obj_t BgL_auxz00_2509;
if(
REALP(BgL_realz00_1385))
{ /* Ieee/flonum.scm 648 */
BgL_auxz00_2509 = BgL_realz00_1385
; }  else 
{ 
 obj_t BgL_auxz00_2512;
BgL_auxz00_2512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(28525L), BGl_string1649z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_realz00_1385); 
FAILURE(BgL_auxz00_2512,BFALSE,BFALSE);} 
return 
BGl_realzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2509);} } 

}



/* ieee-string->double */
BGL_EXPORTED_DEF double BGl_ieeezd2stringzd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(obj_t BgL_stringz00_72)
{
{ /* Ieee/flonum.scm 653 */
BGL_TAIL return 
bgl_ieee_string_to_double(BgL_stringz00_72);} 

}



/* &ieee-string->double */
obj_t BGl_z62ieeezd2stringzd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1386, obj_t BgL_stringz00_1387)
{
{ /* Ieee/flonum.scm 653 */
{ /* Ieee/flonum.scm 654 */
 double BgL_tmpz00_2518;
{ /* Ieee/flonum.scm 654 */
 obj_t BgL_auxz00_2519;
if(
STRINGP(BgL_stringz00_1387))
{ /* Ieee/flonum.scm 654 */
BgL_auxz00_2519 = BgL_stringz00_1387
; }  else 
{ 
 obj_t BgL_auxz00_2522;
BgL_auxz00_2522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(28824L), BGl_string1650z00zz__r4_numbers_6_5_flonumz00, BGl_string1647z00zz__r4_numbers_6_5_flonumz00, BgL_stringz00_1387); 
FAILURE(BgL_auxz00_2522,BFALSE,BFALSE);} 
BgL_tmpz00_2518 = 
BGl_ieeezd2stringzd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2519); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2518);} } 

}



/* double->ieee-string */
BGL_EXPORTED_DEF obj_t BGl_doublezd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(double BgL_doublez00_73)
{
{ /* Ieee/flonum.scm 659 */
BGL_TAIL return 
bgl_double_to_ieee_string(BgL_doublez00_73);} 

}



/* &double->ieee-string */
obj_t BGl_z62doublezd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1388, obj_t BgL_doublez00_1389)
{
{ /* Ieee/flonum.scm 659 */
{ /* Ieee/flonum.scm 660 */
 double BgL_auxz00_2529;
{ /* Ieee/flonum.scm 660 */
 obj_t BgL_tmpz00_2530;
if(
REALP(BgL_doublez00_1389))
{ /* Ieee/flonum.scm 660 */
BgL_tmpz00_2530 = BgL_doublez00_1389
; }  else 
{ 
 obj_t BgL_auxz00_2533;
BgL_auxz00_2533 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(29125L), BGl_string1651z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_doublez00_1389); 
FAILURE(BgL_auxz00_2533,BFALSE,BFALSE);} 
BgL_auxz00_2529 = 
REAL_TO_DOUBLE(BgL_tmpz00_2530); } 
return 
BGl_doublezd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2529);} } 

}



/* ieee-string->float */
BGL_EXPORTED_DEF float BGl_ieeezd2stringzd2ze3floatze3zz__r4_numbers_6_5_flonumz00(obj_t BgL_stringz00_74)
{
{ /* Ieee/flonum.scm 665 */
BGL_TAIL return 
bgl_ieee_string_to_float(BgL_stringz00_74);} 

}



/* &ieee-string->float */
obj_t BGl_z62ieeezd2stringzd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1390, obj_t BgL_stringz00_1391)
{
{ /* Ieee/flonum.scm 665 */
{ /* Ieee/flonum.scm 666 */
 float BgL_tmpz00_2540;
{ /* Ieee/flonum.scm 666 */
 obj_t BgL_auxz00_2541;
if(
STRINGP(BgL_stringz00_1391))
{ /* Ieee/flonum.scm 666 */
BgL_auxz00_2541 = BgL_stringz00_1391
; }  else 
{ 
 obj_t BgL_auxz00_2544;
BgL_auxz00_2544 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(29425L), BGl_string1652z00zz__r4_numbers_6_5_flonumz00, BGl_string1647z00zz__r4_numbers_6_5_flonumz00, BgL_stringz00_1391); 
FAILURE(BgL_auxz00_2544,BFALSE,BFALSE);} 
BgL_tmpz00_2540 = 
BGl_ieeezd2stringzd2ze3floatze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2541); } 
return 
FLOAT_TO_REAL(BgL_tmpz00_2540);} } 

}



/* float->ieee-string */
BGL_EXPORTED_DEF obj_t BGl_floatzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(float BgL_floatz00_75)
{
{ /* Ieee/flonum.scm 671 */
BGL_TAIL return 
bgl_float_to_ieee_string(BgL_floatz00_75);} 

}



/* &float->ieee-string */
obj_t BGl_z62floatzd2ze3ieeezd2stringz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1392, obj_t BgL_floatz00_1393)
{
{ /* Ieee/flonum.scm 671 */
{ /* Ieee/flonum.scm 672 */
 float BgL_auxz00_2551;
{ /* Ieee/flonum.scm 672 */
 obj_t BgL_tmpz00_2552;
if(
REALP(BgL_floatz00_1393))
{ /* Ieee/flonum.scm 672 */
BgL_tmpz00_2552 = BgL_floatz00_1393
; }  else 
{ 
 obj_t BgL_auxz00_2555;
BgL_auxz00_2555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(29723L), BGl_string1653z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_floatz00_1393); 
FAILURE(BgL_auxz00_2555,BFALSE,BFALSE);} 
BgL_auxz00_2551 = 
REAL_TO_FLOAT(BgL_tmpz00_2552); } 
return 
BGl_floatzd2ze3ieeezd2stringze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2551);} } 

}



/* double->llong-bits */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_doublezd2ze3llongzd2bitsze3zz__r4_numbers_6_5_flonumz00(double BgL_nz00_76)
{
{ /* Ieee/flonum.scm 677 */
return 
DOUBLE_TO_LLONG_BITS(BgL_nz00_76);} 

}



/* &double->llong-bits */
obj_t BGl_z62doublezd2ze3llongzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1394, obj_t BgL_nz00_1395)
{
{ /* Ieee/flonum.scm 677 */
{ /* Ieee/flonum.scm 678 */
 BGL_LONGLONG_T BgL_tmpz00_2562;
{ /* Ieee/flonum.scm 678 */
 double BgL_auxz00_2563;
{ /* Ieee/flonum.scm 678 */
 obj_t BgL_tmpz00_2564;
if(
REALP(BgL_nz00_1395))
{ /* Ieee/flonum.scm 678 */
BgL_tmpz00_2564 = BgL_nz00_1395
; }  else 
{ 
 obj_t BgL_auxz00_2567;
BgL_auxz00_2567 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(30031L), BGl_string1654z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1395); 
FAILURE(BgL_auxz00_2567,BFALSE,BFALSE);} 
BgL_auxz00_2563 = 
REAL_TO_DOUBLE(BgL_tmpz00_2564); } 
BgL_tmpz00_2562 = 
BGl_doublezd2ze3llongzd2bitsze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2563); } 
return 
make_bllong(BgL_tmpz00_2562);} } 

}



/* llong-bits->double */
BGL_EXPORTED_DEF double BGl_llongzd2bitszd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(BGL_LONGLONG_T BgL_nz00_77)
{
{ /* Ieee/flonum.scm 683 */
return 
LLONG_BITS_TO_DOUBLE(BgL_nz00_77);} 

}



/* &llong-bits->double */
obj_t BGl_z62llongzd2bitszd2ze3doublez81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1396, obj_t BgL_nz00_1397)
{
{ /* Ieee/flonum.scm 683 */
{ /* Ieee/flonum.scm 684 */
 double BgL_tmpz00_2575;
{ /* Ieee/flonum.scm 684 */
 BGL_LONGLONG_T BgL_auxz00_2576;
{ /* Ieee/flonum.scm 684 */
 obj_t BgL_tmpz00_2577;
if(
LLONGP(BgL_nz00_1397))
{ /* Ieee/flonum.scm 684 */
BgL_tmpz00_2577 = BgL_nz00_1397
; }  else 
{ 
 obj_t BgL_auxz00_2580;
BgL_auxz00_2580 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(30335L), BGl_string1655z00zz__r4_numbers_6_5_flonumz00, BGl_string1656z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1397); 
FAILURE(BgL_auxz00_2580,BFALSE,BFALSE);} 
BgL_auxz00_2576 = 
BLLONG_TO_LLONG(BgL_tmpz00_2577); } 
BgL_tmpz00_2575 = 
BGl_llongzd2bitszd2ze3doubleze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2576); } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_2575);} } 

}



/* float->int-bits */
BGL_EXPORTED_DEF int BGl_floatzd2ze3intzd2bitsze3zz__r4_numbers_6_5_flonumz00(float BgL_nz00_78)
{
{ /* Ieee/flonum.scm 689 */
return 
FLOAT_TO_INT_BITS(BgL_nz00_78);} 

}



/* &float->int-bits */
obj_t BGl_z62floatzd2ze3intzd2bitsz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1398, obj_t BgL_nz00_1399)
{
{ /* Ieee/flonum.scm 689 */
{ /* Ieee/flonum.scm 690 */
 int BgL_tmpz00_2588;
{ /* Ieee/flonum.scm 690 */
 float BgL_auxz00_2589;
{ /* Ieee/flonum.scm 690 */
 obj_t BgL_tmpz00_2590;
if(
REALP(BgL_nz00_1399))
{ /* Ieee/flonum.scm 690 */
BgL_tmpz00_2590 = BgL_nz00_1399
; }  else 
{ 
 obj_t BgL_auxz00_2593;
BgL_auxz00_2593 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(30633L), BGl_string1657z00zz__r4_numbers_6_5_flonumz00, BGl_string1591z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1399); 
FAILURE(BgL_auxz00_2593,BFALSE,BFALSE);} 
BgL_auxz00_2589 = 
REAL_TO_FLOAT(BgL_tmpz00_2590); } 
BgL_tmpz00_2588 = 
BGl_floatzd2ze3intzd2bitsze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2589); } 
return 
BINT(BgL_tmpz00_2588);} } 

}



/* int-bits->float */
BGL_EXPORTED_DEF float BGl_intzd2bitszd2ze3floatze3zz__r4_numbers_6_5_flonumz00(int BgL_nz00_79)
{
{ /* Ieee/flonum.scm 695 */
return 
INT_BITS_TO_FLOAT(BgL_nz00_79);} 

}



/* &int-bits->float */
obj_t BGl_z62intzd2bitszd2ze3floatz81zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1400, obj_t BgL_nz00_1401)
{
{ /* Ieee/flonum.scm 695 */
{ /* Ieee/flonum.scm 696 */
 float BgL_tmpz00_2601;
{ /* Ieee/flonum.scm 696 */
 int BgL_auxz00_2602;
{ /* Ieee/flonum.scm 696 */
 obj_t BgL_tmpz00_2603;
if(
INTEGERP(BgL_nz00_1401))
{ /* Ieee/flonum.scm 696 */
BgL_tmpz00_2603 = BgL_nz00_1401
; }  else 
{ 
 obj_t BgL_auxz00_2606;
BgL_auxz00_2606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1589z00zz__r4_numbers_6_5_flonumz00, 
BINT(30928L), BGl_string1658z00zz__r4_numbers_6_5_flonumz00, BGl_string1659z00zz__r4_numbers_6_5_flonumz00, BgL_nz00_1401); 
FAILURE(BgL_auxz00_2606,BFALSE,BFALSE);} 
BgL_auxz00_2602 = 
CINT(BgL_tmpz00_2603); } 
BgL_tmpz00_2601 = 
BGl_intzd2bitszd2ze3floatze3zz__r4_numbers_6_5_flonumz00(BgL_auxz00_2602); } 
return 
FLOAT_TO_REAL(BgL_tmpz00_2601);} } 

}



/* randomfl */
BGL_EXPORTED_DEF double BGl_randomflz00zz__r4_numbers_6_5_flonumz00(void)
{
{ /* Ieee/flonum.scm 701 */
return 
RANDOMFL();} 

}



/* &randomfl */
obj_t BGl_z62randomflz62zz__r4_numbers_6_5_flonumz00(obj_t BgL_envz00_1402)
{
{ /* Ieee/flonum.scm 701 */
return 
DOUBLE_TO_REAL(
BGl_randomflz00zz__r4_numbers_6_5_flonumz00());} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_numbers_6_5_flonumz00(void)
{
{ /* Ieee/flonum.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1660z00zz__r4_numbers_6_5_flonumz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1660z00zz__r4_numbers_6_5_flonumz00)); 
BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L, 
BSTRING_TO_STRING(BGl_string1660z00zz__r4_numbers_6_5_flonumz00)); 
return 
BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(268155843L, 
BSTRING_TO_STRING(BGl_string1660z00zz__r4_numbers_6_5_flonumz00));} 

}

#ifdef __cplusplus
}
#endif
