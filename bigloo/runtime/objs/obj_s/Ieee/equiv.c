/*===========================================================================*/
/*   (Ieee/equiv.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/equiv.scm -indent -o objs/obj_s/Ieee/equiv.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_EQUIVALENCE_6_2_TYPE_DEFINITIONS
#define BGL___R4_EQUIVALENCE_6_2_TYPE_DEFINITIONS

/* object type definitions */

#endif // BGL___R4_EQUIVALENCE_6_2_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_symbol1673z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1675z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1677z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1679z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1682z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1685z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1687z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_symbol1689z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62eqzf3z91zz__r4_equivalence_6_2z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_list1672z00zz__r4_equivalence_6_2z00 = BUNSPEC;
extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_list1681z00zz__r4_equivalence_6_2z00 = BUNSPEC;
static obj_t BGl_list1684z00zz__r4_equivalence_6_2z00 = BUNSPEC;
extern bool_t ucs2_strcmp(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_equivalence_6_2z00(void);
static obj_t BGl_genericzd2initzd2zz__r4_equivalence_6_2z00(void);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_equivalence_6_2z00(void);
static obj_t BGl_gczd2rootszd2initz00zz__r4_equivalence_6_2z00(void);
static obj_t BGl_objectzd2initzd2zz__r4_equivalence_6_2z00(void);
extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
extern bool_t BGl_objectzd2equalzf3z21zz__objectz00(BgL_objectz00_bglt, BgL_objectz00_bglt);
extern obj_t bgl_weakptr_data(obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL bool_t BGl_eqzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__r4_equivalence_6_2z00(void);
extern long bgl_date_to_seconds(obj_t);
static obj_t BGl_z62equalzf3z91zz__r4_equivalence_6_2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62eqvzf3z91zz__r4_equivalence_6_2z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_eqvzf3zd2envz21zz__r4_equivalence_6_2z00, BgL_bgl_za762eqvza7f3za791za7za7__1694za7, BGl_z62eqvzf3z91zz__r4_equivalence_6_2z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1668z00zz__r4_equivalence_6_2z00, BgL_bgl_string1668za700za7za7_1695za7, "/tmp/bigloo/runtime/Ieee/equiv.scm", 34 );
DEFINE_STRING( BGl_string1669z00zz__r4_equivalence_6_2z00, BgL_bgl_string1669za700za7za7_1696za7, "test", 4 );
DEFINE_STRING( BGl_string1670z00zz__r4_equivalence_6_2z00, BgL_bgl_string1670za700za7za7_1697za7, "procedure", 9 );
DEFINE_STRING( BGl_string1671z00zz__r4_equivalence_6_2z00, BgL_bgl_string1671za700za7za7_1698za7, "test:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string1674z00zz__r4_equivalence_6_2z00, BgL_bgl_string1674za700za7za7_1699za7, "funcall", 7 );
DEFINE_STRING( BGl_string1676z00zz__r4_equivalence_6_2z00, BgL_bgl_string1676za700za7za7_1700za7, "get", 3 );
DEFINE_STRING( BGl_string1678z00zz__r4_equivalence_6_2z00, BgL_bgl_string1678za700za7za7_1701za7, "obj1", 4 );
DEFINE_STRING( BGl_string1680z00zz__r4_equivalence_6_2z00, BgL_bgl_string1680za700za7za7_1702za7, "i", 1 );
DEFINE_STRING( BGl_string1683z00zz__r4_equivalence_6_2z00, BgL_bgl_string1683za700za7za7_1703za7, "obj2", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BgL_bgl_za762eqza7f3za791za7za7__r1704za7, BGl_z62eqzf3z91zz__r4_equivalence_6_2z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1686z00zz__r4_equivalence_6_2z00, BgL_bgl_string1686za700za7za7_1705za7, "cmp", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00, BgL_bgl_za762equalza7f3za791za7za71706za7, BGl_z62equalzf3z91zz__r4_equivalence_6_2z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1688z00zz__r4_equivalence_6_2z00, BgL_bgl_string1688za700za7za7_1707za7, "arg1194", 7 );
DEFINE_STRING( BGl_string1690z00zz__r4_equivalence_6_2z00, BgL_bgl_string1690za700za7za7_1708za7, "arg1196", 7 );
DEFINE_STRING( BGl_string1691z00zz__r4_equivalence_6_2z00, BgL_bgl_string1691za700za7za7_1709za7, "equal?", 6 );
DEFINE_STRING( BGl_string1692z00zz__r4_equivalence_6_2z00, BgL_bgl_string1692za700za7za7_1710za7, "custom", 6 );
DEFINE_STRING( BGl_string1693z00zz__r4_equivalence_6_2z00, BgL_bgl_string1693za700za7za7_1711za7, "__r4_equivalence_6_2", 20 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1673z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1675z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1677z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1679z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1682z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1685z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1687z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_symbol1689z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_list1672z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_list1681z00zz__r4_equivalence_6_2z00) );
ADD_ROOT( (void *)(&BGl_list1684z00zz__r4_equivalence_6_2z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long BgL_checksumz00_1967, char * BgL_fromz00_1968)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_equivalence_6_2z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_equivalence_6_2z00(); 
BGl_cnstzd2initzd2zz__r4_equivalence_6_2z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_equivalence_6_2z00(); 
return 
BGl_methodzd2initzd2zz__r4_equivalence_6_2z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_equivalence_6_2z00(void)
{
{ /* Ieee/equiv.scm 14 */
BGl_symbol1673z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1674z00zz__r4_equivalence_6_2z00); 
BGl_symbol1675z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1676z00zz__r4_equivalence_6_2z00); 
BGl_symbol1677z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1678z00zz__r4_equivalence_6_2z00); 
BGl_symbol1679z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1680z00zz__r4_equivalence_6_2z00); 
BGl_list1672z00zz__r4_equivalence_6_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1673z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1675z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1675z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1677z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1679z00zz__r4_equivalence_6_2z00, BNIL))))); 
BGl_symbol1682z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1683z00zz__r4_equivalence_6_2z00); 
BGl_list1681z00zz__r4_equivalence_6_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1673z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1675z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1675z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1682z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1679z00zz__r4_equivalence_6_2z00, BNIL))))); 
BGl_symbol1685z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1686z00zz__r4_equivalence_6_2z00); 
BGl_symbol1687z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1688z00zz__r4_equivalence_6_2z00); 
BGl_symbol1689z00zz__r4_equivalence_6_2z00 = 
bstring_to_symbol(BGl_string1690z00zz__r4_equivalence_6_2z00); 
return ( 
BGl_list1684z00zz__r4_equivalence_6_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1673z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1685z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1685z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1687z00zz__r4_equivalence_6_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol1689z00zz__r4_equivalence_6_2z00, BNIL))))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_equivalence_6_2z00(void)
{
{ /* Ieee/equiv.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* eq? */
BGL_EXPORTED_DEF bool_t BGl_eqzf3zf3zz__r4_equivalence_6_2z00(obj_t BgL_obj1z00_3, obj_t BgL_obj2z00_4)
{
{ /* Ieee/equiv.scm 67 */
return 
(BgL_obj1z00_3==BgL_obj2z00_4);} 

}



/* &eq? */
obj_t BGl_z62eqzf3z91zz__r4_equivalence_6_2z00(obj_t BgL_envz00_1926, obj_t BgL_obj1z00_1927, obj_t BgL_obj2z00_1928)
{
{ /* Ieee/equiv.scm 67 */
return 
BBOOL(
BGl_eqzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_1927, BgL_obj2z00_1928));} 

}



/* eqv? */
BGL_EXPORTED_DEF bool_t BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(obj_t BgL_obj1z00_5, obj_t BgL_obj2z00_6)
{
{ /* Ieee/equiv.scm 73 */
BGl_eqvzf3zf3zz__r4_equivalence_6_2z00:
if(
(BgL_obj1z00_5==BgL_obj2z00_6))
{ /* Ieee/equiv.scm 75 */
return ((bool_t)1);}  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL_test1714z00_2005;
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1076z00_1713;
BgL__ortest_1076z00_1713 = 
INTEGERP(BgL_obj1z00_5); 
if(BgL__ortest_1076z00_1713)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1076z00_1713
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1077z00_1714;
BgL__ortest_1077z00_1714 = 
ELONGP(BgL_obj1z00_5); 
if(BgL__ortest_1077z00_1714)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1077z00_1714
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1078z00_1715;
BgL__ortest_1078z00_1715 = 
LLONGP(BgL_obj1z00_5); 
if(BgL__ortest_1078z00_1715)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1078z00_1715
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1079z00_1716;
BgL__ortest_1079z00_1716 = 
BGL_INT8P(BgL_obj1z00_5); 
if(BgL__ortest_1079z00_1716)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1079z00_1716
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1080z00_1717;
BgL__ortest_1080z00_1717 = 
BGL_UINT8P(BgL_obj1z00_5); 
if(BgL__ortest_1080z00_1717)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1080z00_1717
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1081z00_1718;
BgL__ortest_1081z00_1718 = 
BGL_INT16P(BgL_obj1z00_5); 
if(BgL__ortest_1081z00_1718)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1081z00_1718
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1082z00_1719;
BgL__ortest_1082z00_1719 = 
BGL_UINT16P(BgL_obj1z00_5); 
if(BgL__ortest_1082z00_1719)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1082z00_1719
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1083z00_1720;
BgL__ortest_1083z00_1720 = 
BGL_INT32P(BgL_obj1z00_5); 
if(BgL__ortest_1083z00_1720)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1083z00_1720
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1084z00_1721;
BgL__ortest_1084z00_1721 = 
BGL_UINT32P(BgL_obj1z00_5); 
if(BgL__ortest_1084z00_1721)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1084z00_1721
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1085z00_1722;
BgL__ortest_1085z00_1722 = 
BGL_INT64P(BgL_obj1z00_5); 
if(BgL__ortest_1085z00_1722)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1085z00_1722
; }  else 
{ /* Ieee/equiv.scm 77 */
 bool_t BgL__ortest_1086z00_1723;
BgL__ortest_1086z00_1723 = 
BGL_UINT64P(BgL_obj1z00_5); 
if(BgL__ortest_1086z00_1723)
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = BgL__ortest_1086z00_1723
; }  else 
{ /* Ieee/equiv.scm 77 */
BgL_test1714z00_2005 = 
BIGNUMP(BgL_obj1z00_5)
; } } } } } } } } } } } } 
if(BgL_test1714z00_2005)
{ /* Ieee/equiv.scm 78 */
 bool_t BgL_test1726z00_2029;
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1076z00_1725;
BgL__ortest_1076z00_1725 = 
INTEGERP(BgL_obj2z00_6); 
if(BgL__ortest_1076z00_1725)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1076z00_1725
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1077z00_1726;
BgL__ortest_1077z00_1726 = 
ELONGP(BgL_obj2z00_6); 
if(BgL__ortest_1077z00_1726)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1077z00_1726
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1078z00_1727;
BgL__ortest_1078z00_1727 = 
LLONGP(BgL_obj2z00_6); 
if(BgL__ortest_1078z00_1727)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1078z00_1727
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1079z00_1728;
BgL__ortest_1079z00_1728 = 
BGL_INT8P(BgL_obj2z00_6); 
if(BgL__ortest_1079z00_1728)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1079z00_1728
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1080z00_1729;
BgL__ortest_1080z00_1729 = 
BGL_UINT8P(BgL_obj2z00_6); 
if(BgL__ortest_1080z00_1729)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1080z00_1729
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1081z00_1730;
BgL__ortest_1081z00_1730 = 
BGL_INT16P(BgL_obj2z00_6); 
if(BgL__ortest_1081z00_1730)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1081z00_1730
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1082z00_1731;
BgL__ortest_1082z00_1731 = 
BGL_UINT16P(BgL_obj2z00_6); 
if(BgL__ortest_1082z00_1731)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1082z00_1731
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1083z00_1732;
BgL__ortest_1083z00_1732 = 
BGL_INT32P(BgL_obj2z00_6); 
if(BgL__ortest_1083z00_1732)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1083z00_1732
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1084z00_1733;
BgL__ortest_1084z00_1733 = 
BGL_UINT32P(BgL_obj2z00_6); 
if(BgL__ortest_1084z00_1733)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1084z00_1733
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1085z00_1734;
BgL__ortest_1085z00_1734 = 
BGL_INT64P(BgL_obj2z00_6); 
if(BgL__ortest_1085z00_1734)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1085z00_1734
; }  else 
{ /* Ieee/equiv.scm 78 */
 bool_t BgL__ortest_1086z00_1735;
BgL__ortest_1086z00_1735 = 
BGL_UINT64P(BgL_obj2z00_6); 
if(BgL__ortest_1086z00_1735)
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = BgL__ortest_1086z00_1735
; }  else 
{ /* Ieee/equiv.scm 78 */
BgL_test1726z00_2029 = 
BIGNUMP(BgL_obj2z00_6)
; } } } } } } } } } } } } 
if(BgL_test1726z00_2029)
{ /* Ieee/equiv.scm 78 */
 bool_t BgL_test1738z00_2053;
if(
INTEGERP(BgL_obj1z00_5))
{ /* Ieee/equiv.scm 78 */
BgL_test1738z00_2053 = 
INTEGERP(BgL_obj2z00_6)
; }  else 
{ /* Ieee/equiv.scm 78 */
BgL_test1738z00_2053 = ((bool_t)0)
; } 
if(BgL_test1738z00_2053)
{ /* Ieee/equiv.scm 78 */
return 
(
(long)CINT(BgL_obj1z00_5)==
(long)CINT(BgL_obj2z00_6));}  else 
{ /* Ieee/equiv.scm 78 */
BGL_TAIL return 
BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_obj1z00_5, BgL_obj2z00_6);} }  else 
{ /* Ieee/equiv.scm 78 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 77 */
if(
REALP(BgL_obj1z00_5))
{ /* Ieee/equiv.scm 79 */
if(
REALP(BgL_obj2z00_6))
{ /* Ieee/equiv.scm 80 */
BGL_TAIL return 
BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_obj1z00_5, BgL_obj2z00_6);}  else 
{ /* Ieee/equiv.scm 80 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 79 */
if(
SYMBOLP(BgL_obj1z00_5))
{ /* Ieee/equiv.scm 81 */
if(
SYMBOLP(BgL_obj2z00_6))
{ /* Ieee/equiv.scm 83 */
 obj_t BgL_arg1148z00_1206; obj_t BgL_arg1149z00_1207;
BgL_arg1148z00_1206 = 
SYMBOL_TO_STRING(BgL_obj1z00_5); 
BgL_arg1149z00_1207 = 
SYMBOL_TO_STRING(BgL_obj2z00_6); 
{ /* Ieee/equiv.scm 83 */
 long BgL_l1z00_1745;
BgL_l1z00_1745 = 
STRING_LENGTH(BgL_arg1148z00_1206); 
if(
(BgL_l1z00_1745==
STRING_LENGTH(BgL_arg1149z00_1207)))
{ /* Ieee/equiv.scm 83 */
 int BgL_arg1238z00_1748;
{ /* Ieee/equiv.scm 83 */
 char * BgL_auxz00_2078; char * BgL_tmpz00_2076;
BgL_auxz00_2078 = 
BSTRING_TO_STRING(BgL_arg1149z00_1207); 
BgL_tmpz00_2076 = 
BSTRING_TO_STRING(BgL_arg1148z00_1206); 
BgL_arg1238z00_1748 = 
memcmp(BgL_tmpz00_2076, BgL_auxz00_2078, BgL_l1z00_1745); } 
return 
(
(long)(BgL_arg1238z00_1748)==0L);}  else 
{ /* Ieee/equiv.scm 83 */
return ((bool_t)0);} } }  else 
{ /* Ieee/equiv.scm 82 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 81 */
if(
FOREIGNP(BgL_obj1z00_5))
{ /* Ieee/equiv.scm 84 */
if(
FOREIGNP(BgL_obj2z00_6))
{ /* Ieee/equiv.scm 85 */
return 
FOREIGN_EQP(BgL_obj1z00_5, BgL_obj2z00_6);}  else 
{ /* Ieee/equiv.scm 85 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 84 */
if(
BGL_WEAKPTRP(BgL_obj1z00_5))
{ /* Ieee/equiv.scm 86 */
if(
BGL_WEAKPTRP(BgL_obj2z00_6))
{ 
 obj_t BgL_obj2z00_2094; obj_t BgL_obj1z00_2092;
BgL_obj1z00_2092 = 
bgl_weakptr_data(BgL_obj1z00_5); 
BgL_obj2z00_2094 = 
bgl_weakptr_data(BgL_obj2z00_6); 
BgL_obj2z00_6 = BgL_obj2z00_2094; 
BgL_obj1z00_5 = BgL_obj1z00_2092; 
goto BGl_eqvzf3zf3zz__r4_equivalence_6_2z00;}  else 
{ /* Ieee/equiv.scm 87 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 86 */
return ((bool_t)0);} } } } } } } 

}



/* &eqv? */
obj_t BGl_z62eqvzf3z91zz__r4_equivalence_6_2z00(obj_t BgL_envz00_1929, obj_t BgL_obj1z00_1930, obj_t BgL_obj2z00_1931)
{
{ /* Ieee/equiv.scm 73 */
return 
BBOOL(
BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_1930, BgL_obj2z00_1931));} 

}



/* equal? */
BGL_EXPORTED_DEF bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t BgL_obj1z00_7, obj_t BgL_obj2z00_8)
{
{ /* Ieee/equiv.scm 94 */
BGl_equalzf3zf3zz__r4_equivalence_6_2z00:
if(
(BgL_obj1z00_7==BgL_obj2z00_8))
{ /* Ieee/equiv.scm 96 */
return ((bool_t)1);}  else 
{ /* Ieee/equiv.scm 96 */
if(
STRINGP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 98 */
if(
STRINGP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 99 */
 long BgL_l1z00_1758;
BgL_l1z00_1758 = 
STRING_LENGTH(BgL_obj1z00_7); 
if(
(BgL_l1z00_1758==
STRING_LENGTH(BgL_obj2z00_8)))
{ /* Ieee/equiv.scm 99 */
 int BgL_arg1238z00_1761;
{ /* Ieee/equiv.scm 99 */
 char * BgL_auxz00_2110; char * BgL_tmpz00_2108;
BgL_auxz00_2110 = 
BSTRING_TO_STRING(BgL_obj2z00_8); 
BgL_tmpz00_2108 = 
BSTRING_TO_STRING(BgL_obj1z00_7); 
BgL_arg1238z00_1761 = 
memcmp(BgL_tmpz00_2108, BgL_auxz00_2110, BgL_l1z00_1758); } 
return 
(
(long)(BgL_arg1238z00_1761)==0L);}  else 
{ /* Ieee/equiv.scm 99 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 99 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 98 */
if(
SYMBOLP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 100 */
return ((bool_t)0);}  else 
{ /* Ieee/equiv.scm 100 */
if(
PAIRP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 102 */
if(
PAIRP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 103 */
if(
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(
CAR(BgL_obj1z00_7), 
CAR(BgL_obj2z00_8)))
{ 
 obj_t BgL_obj2z00_2127; obj_t BgL_obj1z00_2125;
BgL_obj1z00_2125 = 
CDR(BgL_obj1z00_7); 
BgL_obj2z00_2127 = 
CDR(BgL_obj2z00_8); 
BgL_obj2z00_8 = BgL_obj2z00_2127; 
BgL_obj1z00_7 = BgL_obj1z00_2125; 
goto BGl_equalzf3zf3zz__r4_equivalence_6_2z00;}  else 
{ /* Ieee/equiv.scm 104 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 103 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 102 */
if(
VECTORP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 106 */
if(
VECTORP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 108 */
if(
(
VECTOR_LENGTH(BgL_obj2z00_8)==
VECTOR_LENGTH(BgL_obj1z00_7)))
{ /* Ieee/equiv.scm 110 */
 bool_t BgL_test1760z00_2137;
{ /* Ieee/equiv.scm 110 */
 int BgL_arg1183z00_1238; int BgL_arg1187z00_1239;
BgL_arg1183z00_1238 = 
VECTOR_TAG(BgL_obj1z00_7); 
BgL_arg1187z00_1239 = 
VECTOR_TAG(BgL_obj2z00_8); 
BgL_test1760z00_2137 = 
(
(long)(BgL_arg1183z00_1238)==
(long)(BgL_arg1187z00_1239)); } 
if(BgL_test1760z00_2137)
{ 
 long BgL_iz00_1230;
BgL_iz00_1230 = 0L; 
BgL_zc3z04anonymousza31166ze3z87_1231:
{ /* Ieee/equiv.scm 112 */
 bool_t BgL__ortest_1047z00_1232;
BgL__ortest_1047z00_1232 = 
(BgL_iz00_1230==
VECTOR_LENGTH(BgL_obj1z00_7)); 
if(BgL__ortest_1047z00_1232)
{ /* Ieee/equiv.scm 112 */
return BgL__ortest_1047z00_1232;}  else 
{ /* Ieee/equiv.scm 113 */
 bool_t BgL_test1762z00_2146;
{ /* Ieee/equiv.scm 113 */
 obj_t BgL_arg1172z00_1235; obj_t BgL_arg1182z00_1236;
BgL_arg1172z00_1235 = 
VECTOR_REF(
((obj_t)BgL_obj1z00_7),BgL_iz00_1230); 
BgL_arg1182z00_1236 = 
VECTOR_REF(
((obj_t)BgL_obj2z00_8),BgL_iz00_1230); 
BgL_test1762z00_2146 = 
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_arg1172z00_1235, BgL_arg1182z00_1236); } 
if(BgL_test1762z00_2146)
{ 
 long BgL_iz00_2152;
BgL_iz00_2152 = 
(BgL_iz00_1230+1L); 
BgL_iz00_1230 = BgL_iz00_2152; 
goto BgL_zc3z04anonymousza31166ze3z87_1231;}  else 
{ /* Ieee/equiv.scm 113 */
return ((bool_t)0);} } } }  else 
{ /* Ieee/equiv.scm 110 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 109 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 108 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 106 */
if(
BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_7, BgL_obj2z00_8))
{ /* Ieee/equiv.scm 116 */
return ((bool_t)1);}  else 
{ /* Ieee/equiv.scm 116 */
if(
INTEGERP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 118 */
return ((bool_t)0);}  else 
{ /* Ieee/equiv.scm 118 */
if(
BGL_HVECTORP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 121 */
 long BgL_lobj1z00_1244;
BgL_lobj1z00_1244 = 
BGL_HVECTOR_LENGTH(BgL_obj1z00_7); 
if(
BGL_HVECTORP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 123 */
 bool_t BgL_test1767z00_2163;
{ /* Ieee/equiv.scm 123 */
 long BgL_arg1197z00_1267;
BgL_arg1197z00_1267 = 
BGL_HVECTOR_LENGTH(BgL_obj2z00_8); 
BgL_test1767z00_2163 = 
(BgL_arg1197z00_1267==BgL_lobj1z00_1244); } 
if(BgL_test1767z00_2163)
{ /* Ieee/equiv.scm 124 */
 obj_t BgL_tag1z00_1247;
BgL_tag1z00_1247 = 
BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_obj1z00_7); 
{ /* Ieee/equiv.scm 125 */
 obj_t BgL__z00_1248; obj_t BgL_getz00_1249; obj_t BgL__z00_1250; obj_t BgL_cmpz00_1251;
{ /* Ieee/equiv.scm 126 */
 obj_t BgL_tmpz00_1788;
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2167;
BgL_tmpz00_2167 = 
(int)(1L); 
BgL_tmpz00_1788 = 
BGL_MVALUES_VAL(BgL_tmpz00_2167); } 
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2170;
BgL_tmpz00_2170 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2170, BUNSPEC); } 
BgL__z00_1248 = BgL_tmpz00_1788; } 
{ /* Ieee/equiv.scm 126 */
 obj_t BgL_tmpz00_1789;
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2173;
BgL_tmpz00_2173 = 
(int)(2L); 
BgL_tmpz00_1789 = 
BGL_MVALUES_VAL(BgL_tmpz00_2173); } 
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2176;
BgL_tmpz00_2176 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2176, BUNSPEC); } 
BgL_getz00_1249 = BgL_tmpz00_1789; } 
{ /* Ieee/equiv.scm 126 */
 obj_t BgL_tmpz00_1790;
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2179;
BgL_tmpz00_2179 = 
(int)(3L); 
BgL_tmpz00_1790 = 
BGL_MVALUES_VAL(BgL_tmpz00_2179); } 
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2182;
BgL_tmpz00_2182 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2182, BUNSPEC); } 
BgL__z00_1250 = BgL_tmpz00_1790; } 
{ /* Ieee/equiv.scm 126 */
 obj_t BgL_tmpz00_1791;
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2185;
BgL_tmpz00_2185 = 
(int)(4L); 
BgL_tmpz00_1791 = 
BGL_MVALUES_VAL(BgL_tmpz00_2185); } 
{ /* Ieee/equiv.scm 126 */
 int BgL_tmpz00_2188;
BgL_tmpz00_2188 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2188, BUNSPEC); } 
BgL_cmpz00_1251 = BgL_tmpz00_1791; } 
{ /* Ieee/equiv.scm 126 */
 obj_t BgL_tag2z00_1252;
BgL_tag2z00_1252 = 
BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_obj2z00_8); 
{ /* Ieee/equiv.scm 127 */
 obj_t BgL__z00_1253; obj_t BgL__z00_1254; obj_t BgL__z00_1255; obj_t BgL__z00_1256;
{ /* Ieee/equiv.scm 128 */
 obj_t BgL_tmpz00_1792;
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2192;
BgL_tmpz00_2192 = 
(int)(1L); 
BgL_tmpz00_1792 = 
BGL_MVALUES_VAL(BgL_tmpz00_2192); } 
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2195;
BgL_tmpz00_2195 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2195, BUNSPEC); } 
BgL__z00_1253 = BgL_tmpz00_1792; } 
{ /* Ieee/equiv.scm 128 */
 obj_t BgL_tmpz00_1793;
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2198;
BgL_tmpz00_2198 = 
(int)(2L); 
BgL_tmpz00_1793 = 
BGL_MVALUES_VAL(BgL_tmpz00_2198); } 
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2201;
BgL_tmpz00_2201 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2201, BUNSPEC); } 
BgL__z00_1254 = BgL_tmpz00_1793; } 
{ /* Ieee/equiv.scm 128 */
 obj_t BgL_tmpz00_1794;
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2204;
BgL_tmpz00_2204 = 
(int)(3L); 
BgL_tmpz00_1794 = 
BGL_MVALUES_VAL(BgL_tmpz00_2204); } 
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2207;
BgL_tmpz00_2207 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2207, BUNSPEC); } 
BgL__z00_1255 = BgL_tmpz00_1794; } 
{ /* Ieee/equiv.scm 128 */
 obj_t BgL_tmpz00_1795;
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2210;
BgL_tmpz00_2210 = 
(int)(4L); 
BgL_tmpz00_1795 = 
BGL_MVALUES_VAL(BgL_tmpz00_2210); } 
{ /* Ieee/equiv.scm 128 */
 int BgL_tmpz00_2213;
BgL_tmpz00_2213 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2213, BUNSPEC); } 
BgL__z00_1256 = BgL_tmpz00_1795; } 
if(
(BgL_tag1z00_1247==BgL_tag2z00_1252))
{ 
 long BgL_iz00_1259;
BgL_iz00_1259 = 0L; 
BgL_zc3z04anonymousza31192ze3z87_1260:
{ /* Ieee/equiv.scm 130 */
 bool_t BgL__ortest_1052z00_1261;
BgL__ortest_1052z00_1261 = 
(BgL_iz00_1259==BgL_lobj1z00_1244); 
if(BgL__ortest_1052z00_1261)
{ /* Ieee/equiv.scm 130 */
return BgL__ortest_1052z00_1261;}  else 
{ /* Ieee/equiv.scm 131 */
 obj_t BgL__andtest_1053z00_1262;
{ /* Ieee/equiv.scm 131 */
 obj_t BgL_arg1194z00_1264; obj_t BgL_arg1196z00_1265;
{ /* Ieee/equiv.scm 131 */
 obj_t BgL_funz00_1941;
if(
PROCEDUREP(BgL_getz00_1249))
{ /* Ieee/equiv.scm 131 */
BgL_funz00_1941 = BgL_getz00_1249; }  else 
{ 
 obj_t BgL_auxz00_2222;
BgL_auxz00_2222 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1668z00zz__r4_equivalence_6_2z00, 
BINT(4369L), BGl_string1669z00zz__r4_equivalence_6_2z00, BGl_string1670z00zz__r4_equivalence_6_2z00, BgL_getz00_1249); 
FAILURE(BgL_auxz00_2222,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1941, 2))
{ /* Ieee/equiv.scm 131 */
BgL_arg1194z00_1264 = 
(VA_PROCEDUREP( BgL_funz00_1941 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1941))(BgL_getz00_1249, BgL_obj1z00_7, 
BINT(BgL_iz00_1259), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_1941))(BgL_getz00_1249, BgL_obj1z00_7, 
BINT(BgL_iz00_1259)) ); }  else 
{ /* Ieee/equiv.scm 131 */
FAILURE(BGl_string1671z00zz__r4_equivalence_6_2z00,BGl_list1672z00zz__r4_equivalence_6_2z00,BgL_funz00_1941);} } 
{ /* Ieee/equiv.scm 131 */
 obj_t BgL_funz00_1945;
if(
PROCEDUREP(BgL_getz00_1249))
{ /* Ieee/equiv.scm 131 */
BgL_funz00_1945 = BgL_getz00_1249; }  else 
{ 
 obj_t BgL_auxz00_2237;
BgL_auxz00_2237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1668z00zz__r4_equivalence_6_2z00, 
BINT(4382L), BGl_string1669z00zz__r4_equivalence_6_2z00, BGl_string1670z00zz__r4_equivalence_6_2z00, BgL_getz00_1249); 
FAILURE(BgL_auxz00_2237,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1945, 2))
{ /* Ieee/equiv.scm 131 */
BgL_arg1196z00_1265 = 
(VA_PROCEDUREP( BgL_funz00_1945 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1945))(BgL_getz00_1249, BgL_obj2z00_8, 
BINT(BgL_iz00_1259), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_1945))(BgL_getz00_1249, BgL_obj2z00_8, 
BINT(BgL_iz00_1259)) ); }  else 
{ /* Ieee/equiv.scm 131 */
FAILURE(BGl_string1671z00zz__r4_equivalence_6_2z00,BGl_list1681z00zz__r4_equivalence_6_2z00,BgL_funz00_1945);} } 
{ /* Ieee/equiv.scm 131 */
 obj_t BgL_funz00_1949;
if(
PROCEDUREP(BgL_cmpz00_1251))
{ /* Ieee/equiv.scm 131 */
BgL_funz00_1949 = BgL_cmpz00_1251; }  else 
{ 
 obj_t BgL_auxz00_2252;
BgL_auxz00_2252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1668z00zz__r4_equivalence_6_2z00, 
BINT(4364L), BGl_string1669z00zz__r4_equivalence_6_2z00, BGl_string1670z00zz__r4_equivalence_6_2z00, BgL_cmpz00_1251); 
FAILURE(BgL_auxz00_2252,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1949, 2))
{ /* Ieee/equiv.scm 131 */
BgL__andtest_1053z00_1262 = 
(VA_PROCEDUREP( BgL_funz00_1949 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1949))(BgL_cmpz00_1251, BgL_arg1194z00_1264, BgL_arg1196z00_1265, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_1949))(BgL_cmpz00_1251, BgL_arg1194z00_1264, BgL_arg1196z00_1265) ); }  else 
{ /* Ieee/equiv.scm 131 */
FAILURE(BGl_string1671z00zz__r4_equivalence_6_2z00,BGl_list1684z00zz__r4_equivalence_6_2z00,BgL_funz00_1949);} } } 
if(
CBOOL(BgL__andtest_1053z00_1262))
{ 
 long BgL_iz00_2266;
BgL_iz00_2266 = 
(BgL_iz00_1259+1L); 
BgL_iz00_1259 = BgL_iz00_2266; 
goto BgL_zc3z04anonymousza31192ze3z87_1260;}  else 
{ /* Ieee/equiv.scm 131 */
return ((bool_t)0);} } } }  else 
{ /* Ieee/equiv.scm 128 */
return ((bool_t)0);} } } } }  else 
{ /* Ieee/equiv.scm 123 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 122 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 120 */
if(
REALP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 133 */
return ((bool_t)0);}  else 
{ /* Ieee/equiv.scm 133 */
if(
STRUCTP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 136 */
 int BgL_lobj1z00_1270;
BgL_lobj1z00_1270 = 
STRUCT_LENGTH(BgL_obj1z00_7); 
if(
STRUCTP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 137 */
if(
(
(long)(
STRUCT_LENGTH(BgL_obj2z00_8))==
(long)(BgL_lobj1z00_1270)))
{ 
 long BgL_iz00_1274;
BgL_iz00_1274 = 0L; 
BgL_zc3z04anonymousza31200ze3z87_1275:
{ /* Ieee/equiv.scm 140 */
 bool_t BgL__ortest_1056z00_1276;
BgL__ortest_1056z00_1276 = 
(BgL_iz00_1274==
(long)(BgL_lobj1z00_1270)); 
if(BgL__ortest_1056z00_1276)
{ /* Ieee/equiv.scm 140 */
return BgL__ortest_1056z00_1276;}  else 
{ /* Ieee/equiv.scm 141 */
 bool_t BgL_test1782z00_2283;
{ /* Ieee/equiv.scm 141 */
 obj_t BgL_arg1202z00_1279; obj_t BgL_arg1203z00_1280;
BgL_arg1202z00_1279 = 
STRUCT_REF(
((obj_t)BgL_obj1z00_7), 
(int)(BgL_iz00_1274)); 
BgL_arg1203z00_1280 = 
STRUCT_REF(
((obj_t)BgL_obj2z00_8), 
(int)(BgL_iz00_1274)); 
BgL_test1782z00_2283 = 
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_arg1202z00_1279, BgL_arg1203z00_1280); } 
if(BgL_test1782z00_2283)
{ 
 long BgL_iz00_2291;
BgL_iz00_2291 = 
(BgL_iz00_1274+1L); 
BgL_iz00_1274 = BgL_iz00_2291; 
goto BgL_zc3z04anonymousza31200ze3z87_1275;}  else 
{ /* Ieee/equiv.scm 141 */
return ((bool_t)0);} } } }  else 
{ /* Ieee/equiv.scm 138 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 137 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 135 */
if(
CELLP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 143 */
if(
CELLP(BgL_obj2z00_8))
{ 
 obj_t BgL_obj2z00_2299; obj_t BgL_obj1z00_2297;
BgL_obj1z00_2297 = 
CELL_REF(BgL_obj1z00_7); 
BgL_obj2z00_2299 = 
CELL_REF(BgL_obj2z00_8); 
BgL_obj2z00_8 = BgL_obj2z00_2299; 
BgL_obj1z00_7 = BgL_obj1z00_2297; 
goto BGl_equalzf3zf3zz__r4_equivalence_6_2z00;}  else 
{ /* Ieee/equiv.scm 144 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 143 */
if(
BGL_OBJECTP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 145 */
if(
BGL_OBJECTP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 146 */
return 
BGl_objectzd2equalzf3z21zz__objectz00(
((BgL_objectz00_bglt)BgL_obj1z00_7), 
((BgL_objectz00_bglt)BgL_obj2z00_8));}  else 
{ /* Ieee/equiv.scm 146 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 145 */
if(
UCS2_STRINGP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 147 */
if(
UCS2_STRINGP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 148 */
return 
ucs2_strcmp(BgL_obj1z00_7, BgL_obj2z00_8);}  else 
{ /* Ieee/equiv.scm 148 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 147 */
if(
CUSTOMP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 149 */
if(
CUSTOMP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 150 */
 obj_t BgL_obj1z00_1814; obj_t BgL_obj2z00_1815;
if(
CUSTOMP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 150 */
BgL_obj1z00_1814 = BgL_obj1z00_7; }  else 
{ 
 obj_t BgL_auxz00_2319;
BgL_auxz00_2319 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1668z00zz__r4_equivalence_6_2z00, 
BINT(5051L), BGl_string1691z00zz__r4_equivalence_6_2z00, BGl_string1692z00zz__r4_equivalence_6_2z00, BgL_obj1z00_7); 
FAILURE(BgL_auxz00_2319,BFALSE,BFALSE);} 
if(
CUSTOMP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 150 */
BgL_obj2z00_1815 = BgL_obj2z00_8; }  else 
{ 
 obj_t BgL_auxz00_2325;
BgL_auxz00_2325 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1668z00zz__r4_equivalence_6_2z00, 
BINT(5056L), BGl_string1691z00zz__r4_equivalence_6_2z00, BGl_string1692z00zz__r4_equivalence_6_2z00, BgL_obj2z00_8); 
FAILURE(BgL_auxz00_2325,BFALSE,BFALSE);} 
return 
CUSTOM_CMP(BgL_obj1z00_1814, BgL_obj2z00_1815);}  else 
{ /* Ieee/equiv.scm 150 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 149 */
if(
UCS2P(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 151 */
if(
UCS2P(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 152 */
return 
(
CUCS2(BgL_obj1z00_7)==
CUCS2(BgL_obj2z00_8));}  else 
{ /* Ieee/equiv.scm 152 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 151 */
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 153 */
return ((bool_t)0);}  else 
{ /* Ieee/equiv.scm 153 */
if(
BGL_DATEP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 155 */
if(
BGL_DATEP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 156 */
return 
(
bgl_date_to_seconds(BgL_obj1z00_7)==
bgl_date_to_seconds(BgL_obj2z00_8));}  else 
{ /* Ieee/equiv.scm 156 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 155 */
if(
FOREIGNP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 157 */
if(
FOREIGNP(BgL_obj2z00_8))
{ /* Ieee/equiv.scm 158 */
return 
FOREIGN_EQP(BgL_obj1z00_7, BgL_obj2z00_8);}  else 
{ /* Ieee/equiv.scm 158 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 157 */
if(
BGL_WEAKPTRP(BgL_obj1z00_7))
{ /* Ieee/equiv.scm 159 */
if(
BGL_WEAKPTRP(BgL_obj2z00_8))
{ 
 obj_t BgL_obj2z00_2357; obj_t BgL_obj1z00_2355;
BgL_obj1z00_2355 = 
bgl_weakptr_data(BgL_obj1z00_7); 
BgL_obj2z00_2357 = 
bgl_weakptr_data(BgL_obj2z00_8); 
BgL_obj2z00_8 = BgL_obj2z00_2357; 
BgL_obj1z00_7 = BgL_obj1z00_2355; 
goto BGl_equalzf3zf3zz__r4_equivalence_6_2z00;}  else 
{ /* Ieee/equiv.scm 160 */
return ((bool_t)0);} }  else 
{ /* Ieee/equiv.scm 159 */
return ((bool_t)0);} } } } } } } } } } } } } } } } } } } } 

}



/* &equal? */
obj_t BGl_z62equalzf3z91zz__r4_equivalence_6_2z00(obj_t BgL_envz00_1932, obj_t BgL_obj1z00_1933, obj_t BgL_obj2z00_1934)
{
{ /* Ieee/equiv.scm 94 */
return 
BBOOL(
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_obj1z00_1933, BgL_obj2z00_1934));} 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__r4_equivalence_6_2z00(void)
{
{ /* Ieee/equiv.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__r4_equivalence_6_2z00(void)
{
{ /* Ieee/equiv.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__r4_equivalence_6_2z00(void)
{
{ /* Ieee/equiv.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_equivalence_6_2z00(void)
{
{ /* Ieee/equiv.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1693z00zz__r4_equivalence_6_2z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1693z00zz__r4_equivalence_6_2z00));} 

}

#ifdef __cplusplus
}
#endif
