/*===========================================================================*/
/*   (Ieee/pairlist.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/pairlist.scm -indent -o objs/obj_s/Ieee/pairlist.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_PAIRS_AND_LISTS_6_3_TYPE_DEFINITIONS
#define BGL___R4_PAIRS_AND_LISTS_6_3_TYPE_DEFINITIONS
#endif // BGL___R4_PAIRS_AND_LISTS_6_3_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62carz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2800z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62cdaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol2721z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2804z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62caddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2643z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_cddddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol2806z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2808z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62lengthz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2614z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62takez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_list2617z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t bgl_append2(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_reducez00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62appendz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_caddarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_ereversez00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62listz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2814z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2700z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cddaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2734z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2701z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2653z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2620z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2572z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2816z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2621z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cdaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_anyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2704z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2705z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2739z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2706z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2626z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2709z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2629z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t, long);
BGL_EXPORTED_DECL obj_t bgl_remq_bang(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_caaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cddarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62caadarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol2661z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
static obj_t BGl_list2710z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t bgl_list_ref(obj_t, long);
static obj_t BGl_list2711z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2826z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2712z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2713z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2665z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2632z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2584z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2586z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2716z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2668z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2635z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2636z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2588z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2637z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62epairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_caaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_listz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62caarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62cdrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2830z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cdadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2751z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2720z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2672z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2640z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2803z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2674z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2641z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2642z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2594z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2757z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2724z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2725z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2759z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2726z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2678z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2645z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2727z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2646z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2598z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2647z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_list2649z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_deletez00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62assqz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdadarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62cerz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cadddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62assvz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62pairzd2orzd2nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2730z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cddadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2731z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2683z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2650z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2813z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2732z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2651z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2766z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2733z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2652z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62makezd2listzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl__deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2736z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2688z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2655z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2737z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2738z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2658z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2659z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2579z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62iotaz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_setzd2cdrz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62setzd2carz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2splitz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, int, obj_t);
BGL_EXPORTED_DECL obj_t BGl_caarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_appendz12z12zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62remqz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_caaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62appendz12z70zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62cadaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62caaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2771z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2741z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2660z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2742z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2743z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2744z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2696z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2663z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2745z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2664z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2746z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2747z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2748z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2667z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2749z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl__deletez00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62memqz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_pairs_and_lists_6_3z00(void);
static obj_t BGl_z62memvz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_caadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62remqz12z70zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_reverse(obj_t);
BGL_EXPORTED_DECL obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62cadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62reversez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL long bgl_list_length(obj_t);
static obj_t BGl_z62cddarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2750z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2670z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_pairs_and_lists_6_3z00(void);
static obj_t BGl_list2671z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2786z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2753z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(int, obj_t);
static obj_t BGl_list2754z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2673z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2755z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2593z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__r4_pairs_and_lists_6_3z00(void);
static obj_t BGl_list2756z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2676z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2758z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2677z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2597z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_setzd2cerz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_remq(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62dropz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62appendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62caaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cddaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62ereversez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62cdddarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_pairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_list2761z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2680z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2762z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2681z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2763z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2682z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl__deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2764z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2765z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2685z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2686z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2768z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2687z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2769z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_carz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_caadarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_reverse_bang(obj_t);
static obj_t BGl_z62assocz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62cdaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62cadadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
static obj_t BGl_list2770z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62listzd2refzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_list2690z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2691z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2773z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2692z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2774z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2693z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_epairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_list2775z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2694z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62everyz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_list2776z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2695z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2777z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2778z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2779z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2698z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
static obj_t BGl_list2699z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_cadarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62memberz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62cdarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62findz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62cdddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2780z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2781z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62anyz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_list2782z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2783z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2784z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2785z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62lastzd2pairzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2788z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_listzd2splitzd2zz__r4_pairs_and_lists_6_3z00(obj_t, int, obj_t);
static obj_t BGl_list2789z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_listzd2tabulatezd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
static obj_t BGl_z62appendzd22z12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62listzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62caadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cddadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62cddddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_list2790z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2791z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2792z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2793z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2799z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_econsz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62setzd2cdrz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cadaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62reducez62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_caaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_findz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62cdaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62caddarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62pairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cerz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62listzd2splitzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62listzd2setz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_caddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62caaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62consza2zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62cddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2608z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62consz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62setzd2cerz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62listzd2splitz12za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_eappendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol2612z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cadarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62eappendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdddarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62eappendz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2615z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl__deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62listzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cddrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62treezd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cadadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol2702z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62listzd2tabulatezb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62reversez12z70zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2624z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cdadarz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2707z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62cadddrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62listzd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_setzd2carz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_consz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62findzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62econsz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_cdadrz00zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol2630z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_z62caaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_symbol2714z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2633z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_symbol2638z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
static obj_t BGl_list2605z00zz__r4_pairs_and_lists_6_3z00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern bool_t BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, long, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_anyzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762anyza762za7za7__r4_2835z00, va_generic_entry, BGl_z62anyz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_lastzd2pairzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762lastza7d2pairza7b2836za7, BGl_z62lastzd2pairzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2760z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2760za700za7za7_2837za7, "tail1087", 8 );
DEFINE_STRING( BGl_string2684z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2684za700za7za7_2838za7, "newtail1076", 11 );
DEFINE_STRING( BGl_string2767z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2767za700za7za7_2839za7, "newtail1088", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_treezd2copyzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762treeza7d2copyza7b2840za7, BGl_z62treezd2copyzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2689z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2689za700za7za7_2841za7, "arg1543", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7f3za791za7za7_2842za7, BGl_z62listzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caadrza762za7za7__r2843z00, BGl_z62caadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cddrza762za7za7__r42844z00, BGl_z62cddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cddadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cddadrza762za7za7__2845z00, BGl_z62cddadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2copyzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2copyza7b2846za7, BGl_z62listzd2copyzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2772z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2772za700za7za7_2847za7, "arg1580", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2tailzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762findza7d2tailza7b2848za7, BGl_z62findzd2tailzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caaarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caaarza762za7za7__r2849z00, BGl_z62caaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2697z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2697za700za7za7_2850za7, "begin", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdarza762za7za7__r42851z00, BGl_z62cdarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza762za7za7__r42852z00, va_generic_entry, BGl_z62listz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cddaarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cddaarza762za7za7__2853z00, BGl_z62cddaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762pairza7f3za791za7za7_2854za7, BGl_z62pairzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2787z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2787za700za7za7_2855za7, "arg1579", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_nullzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762nullza7f3za791za7za7_2856za7, BGl_z62nullzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cerzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cerza762za7za7__r4_2857z00, BGl_z62cerz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caddrza762za7za7__r2858z00, BGl_z62caddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_pairzd2orzd2nullzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762pairza7d2orza7d2n2859za7, BGl_z62pairzd2orzd2nullzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cddddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cddddrza762za7za7__2860z00, BGl_z62cddddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2listzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762makeza7d2listza7b2861za7, va_generic_entry, BGl_z62makezd2listzb0zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2794z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2794za700za7za7_2862za7, "<@anonymous:1563>", 17 );
DEFINE_STRING( BGl_string2795z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2795za700za7za7_2863za7, "&any", 4 );
DEFINE_STRING( BGl_string2796z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2796za700za7za7_2864za7, "find", 4 );
DEFINE_STRING( BGl_string2797z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2797za700za7za7_2865za7, "&find", 5 );
DEFINE_STRING( BGl_string2798z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2798za700za7za7_2866za7, "lp:Wrong number of arguments", 28 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cadarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cadarza762za7za7__r2867z00, BGl_z62cadarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caaadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caaadrza762za7za7__2868z00, BGl_z62caaadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ereversezd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762ereverseza762za7za72869z00, BGl_z62ereversez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdddarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdddarza762za7za7__2870z00, BGl_z62cdddarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_deletezd2duplicateszd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl__deleteza7d2duplic2871za7, opt_generic_entry, BGl__deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_assqzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762assqza762za7za7__r42872z00, BGl_z62assqz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caaaarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caaaarza762za7za7__2873z00, BGl_z62caaaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2tabulatezd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2tabula2874z00, BGl_z62listzd2tabulatezb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_reversezd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762reverseza762za7za7_2875z00, BGl_z62reversez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_takezd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762takeza762za7za7__r42876z00, BGl_z62takez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setzd2cerz12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762setza7d2cerza712za72877z00, BGl_z62setzd2cerz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762appendza762za7za7__2878z00, va_generic_entry, BGl_z62appendz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdadrza762za7za7__r2879z00, BGl_z62cdadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caaddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caaddrza762za7za7__2880z00, BGl_z62caaddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2500z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2500za700za7za7_2881za7, "&caaadr", 7 );
DEFINE_STRING( BGl_string2501z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2501za700za7za7_2882za7, "caadar", 6 );
DEFINE_STRING( BGl_string2502z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2502za700za7za7_2883za7, "&caadar", 7 );
DEFINE_STRING( BGl_string2503z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2503za700za7za7_2884za7, "cadaar", 6 );
DEFINE_STRING( BGl_string2504z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2504za700za7za7_2885za7, "&cadaar", 7 );
DEFINE_STRING( BGl_string2505z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2505za700za7za7_2886za7, "cdaaar", 6 );
DEFINE_STRING( BGl_string2506z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2506za700za7za7_2887za7, "&cdaaar", 7 );
DEFINE_STRING( BGl_string2507z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2507za700za7za7_2888za7, "caaddr", 6 );
DEFINE_STRING( BGl_string2508z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2508za700za7za7_2889za7, "&caaddr", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_memqzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762memqza762za7za7__r42890z00, BGl_z62memqz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2509z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2509za700za7za7_2891za7, "caddar", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2setz12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2setza7122892za7, BGl_z62listzd2setz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_assoczd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762assocza762za7za7__r2893z00, BGl_z62assocz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdaarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdaarza762za7za7__r2894z00, BGl_z62cdaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caadarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caadarza762za7za7__2895z00, BGl_z62caadarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2510z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2510za700za7za7_2896za7, "&caddar", 7 );
DEFINE_STRING( BGl_string2511z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2511za700za7za7_2897za7, "cadadr", 6 );
DEFINE_STRING( BGl_string2512z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2512za700za7za7_2898za7, "&cadadr", 7 );
DEFINE_STRING( BGl_string2513z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2513za700za7za7_2899za7, "cadddr", 6 );
DEFINE_STRING( BGl_string2514z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2514za700za7za7_2900za7, "&cadddr", 7 );
DEFINE_STRING( BGl_string2515z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2515za700za7za7_2901za7, "cdaadr", 6 );
DEFINE_STRING( BGl_string2516z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2516za700za7za7_2902za7, "&cdaadr", 7 );
DEFINE_STRING( BGl_string2517z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2517za700za7za7_2903za7, "cdaddr", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_epairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762epairza7f3za791za7za72904za7, BGl_z62epairzf3z91zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2518z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2518za700za7za7_2905za7, "&cdaddr", 7 );
DEFINE_STRING( BGl_string2519z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2519za700za7za7_2906za7, "cddaar", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendz12zd2envzc0zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762appendza712za770za72907z00, va_generic_entry, BGl_z62appendz12z70zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdddrza762za7za7__r2908z00, BGl_z62cdddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2600z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2600za700za7za7_2909za7, "cons*1~0", 8 );
DEFINE_STRING( BGl_string2601z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2601za700za7za7_2910za7, "nr", 2 );
DEFINE_STRING( BGl_string2520z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2520za700za7za7_2911za7, "&cddaar", 7 );
DEFINE_STRING( BGl_string2602z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2602za700za7za7_2912za7, "reverse!", 8 );
DEFINE_STRING( BGl_string2521z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2521za700za7za7_2913za7, "cddadr", 6 );
DEFINE_STRING( BGl_string2603z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2603za700za7za7_2914za7, "&reverse!", 9 );
DEFINE_STRING( BGl_string2522z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2522za700za7za7_2915za7, "&cddadr", 7 );
DEFINE_STRING( BGl_string2604z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2604za700za7za7_2916za7, "every", 5 );
DEFINE_STRING( BGl_string2523z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2523za700za7za7_2917za7, "cdadar", 6 );
DEFINE_STRING( BGl_string2524z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2524za700za7za7_2918za7, "&cdadar", 7 );
DEFINE_STRING( BGl_string2525z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2525za700za7za7_2919za7, "cdddar", 6 );
DEFINE_STRING( BGl_string2607z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2607za700za7za7_2920za7, "pred", 4 );
DEFINE_STRING( BGl_string2526z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2526za700za7za7_2921za7, "&cdddar", 7 );
DEFINE_STRING( BGl_string2527z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2527za700za7za7_2922za7, "cddddr", 6 );
DEFINE_STRING( BGl_string2609z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2609za700za7za7_2923za7, "arg1522", 7 );
DEFINE_STRING( BGl_string2528z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2528za700za7za7_2924za7, "&cddddr", 7 );
DEFINE_STRING( BGl_string2529z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2529za700za7za7_2925za7, "&set-car!", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_remqz12zd2envzc0zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762remqza712za770za7za7_2926za7, BGl_z62remqz12z70zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_assvzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762assvza762za7za7__r42927z00, BGl_z62assvz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cddarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cddarza762za7za7__r2928z00, BGl_z62cddarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cadadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cadadrza762za7za7__2929z00, BGl_z62cadadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2refzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2refza7b02930za7, BGl_z62listzd2refzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2610z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2610za700za7za7_2931za7, "<@anonymous:1538>", 17 );
DEFINE_STRING( BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2611za700za7za7_2932za7, "map", 3 );
DEFINE_STRING( BGl_string2530z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2530za700za7za7_2933za7, "&set-cdr!", 9 );
DEFINE_STRING( BGl_string2531z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2531za700za7za7_2934za7, "&set-cer!", 9 );
DEFINE_STRING( BGl_string2613z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2613za700za7za7_2935za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2532za700za7za7_2936za7, "list", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_iotazd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762iotaza762za7za7__r42937z00, va_generic_entry, BGl_z62iotaz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2533za700za7za7_2938za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2534z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2534za700za7za7_2939za7, "l2", 2 );
DEFINE_STRING( BGl_string2616z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2616za700za7za7_2940za7, "apply", 5 );
DEFINE_STRING( BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2535za700za7za7_2941za7, "loop", 4 );
DEFINE_STRING( BGl_string2536z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2536za700za7za7_2942za7, "&append-2", 9 );
DEFINE_STRING( BGl_string2537z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2537za700za7za7_2943za7, "eappend-2", 9 );
DEFINE_STRING( BGl_string2619z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2619za700za7za7_2944za7, "let", 3 );
DEFINE_STRING( BGl_string2538z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2538za700za7za7_2945za7, "&eappend-2", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762consza762za7za7__r42946z00, BGl_z62consz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2539za700za7za7_2947za7, "append-list~2", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_remqzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762remqza762za7za7__r42948z00, BGl_z62remqz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_econszd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762econsza762za7za7__r2949z00, BGl_z62econsz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cadaarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cadaarza762za7za7__2950z00, BGl_z62cadaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2540z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2540za700za7za7_2951za7, "case_else1012", 13 );
DEFINE_STRING( BGl_string2703z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2703za700za7za7_2952za7, "$set-cdr!", 9 );
DEFINE_STRING( BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2541za700za7za7_2953za7, "append-list~1", 13 );
DEFINE_STRING( BGl_string2623z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2623za700za7za7_2954za7, "l1072", 5 );
DEFINE_STRING( BGl_string2542z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2542za700za7za7_2955za7, "case_else1014", 13 );
DEFINE_STRING( BGl_string2543z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2543za700za7za7_2956za7, "append!", 7 );
DEFINE_STRING( BGl_string2625z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2625za700za7za7_2957za7, "l", 1 );
DEFINE_STRING( BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2544za700za7za7_2958za7, "append-list~0", 13 );
DEFINE_STRING( BGl_string2545z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2545za700za7za7_2959za7, "case_else1016", 13 );
DEFINE_STRING( BGl_string2708z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2708za700za7za7_2960za7, "arg1540", 7 );
DEFINE_STRING( BGl_string2546z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2546za700za7za7_2961za7, "do-loop--1018", 13 );
DEFINE_STRING( BGl_string2628z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2628za700za7za7_2962za7, "if", 2 );
DEFINE_STRING( BGl_string2547z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2547za700za7za7_2963za7, "&append-2!", 10 );
DEFINE_STRING( BGl_string2548z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2548za700za7za7_2964za7, "&length", 7 );
DEFINE_STRING( BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2467za700za7za7_2965za7, "/tmp/bigloo/runtime/Ieee/pairlist.scm", 37 );
DEFINE_STRING( BGl_string2549z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2549za700za7za7_2966za7, "&reverse", 8 );
DEFINE_STRING( BGl_string2468z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2468za700za7za7_2967za7, "&car", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_memvzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762memvza762za7za7__r42968z00, BGl_z62memvz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2469za700za7za7_2969za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_reversez12zd2envzc0zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762reverseza712za7702970za7, BGl_z62reversez12z70zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_consza2zd2envz70zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762consza7a2za7c0za7za7_2971za7, va_generic_entry, BGl_z62consza2zc0zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cadddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cadddrza762za7za7__2972z00, BGl_z62cadddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2631z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2631za700za7za7_2973za7, "$null?", 6 );
DEFINE_STRING( BGl_string2550z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2550za700za7za7_2974za7, "&ereverse", 9 );
DEFINE_STRING( BGl_string2551z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2551za700za7za7_2975za7, "&take", 5 );
DEFINE_STRING( BGl_string2470z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2470za700za7za7_2976za7, "&cdr", 4 );
DEFINE_STRING( BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2552za700za7za7_2977za7, "bint", 4 );
DEFINE_STRING( BGl_string2471z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2471za700za7za7_2978za7, "&cer", 4 );
DEFINE_STRING( BGl_string2715z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2715za700za7za7_2979za7, "bigloo-type-error/location", 26 );
DEFINE_STRING( BGl_string2634z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2634za700za7za7_2980za7, "quote", 5 );
DEFINE_STRING( BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2553za700za7za7_2981za7, "drop", 4 );
DEFINE_STRING( BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2472za700za7za7_2982za7, "epair", 5 );
DEFINE_STRING( BGl_string2554z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2554za700za7za7_2983za7, "&drop", 5 );
DEFINE_STRING( BGl_string2473z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2473za700za7za7_2984za7, "caar", 4 );
DEFINE_STRING( BGl_string2717z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2717za700za7za7_2985za7, "<@anonymous:1526>", 17 );
DEFINE_STRING( BGl_string2555z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2555za700za7za7_2986za7, "&list-tail", 10 );
DEFINE_STRING( BGl_string2474z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2474za700za7za7_2987za7, "&caar", 5 );
DEFINE_STRING( BGl_string2718z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2718za700za7za7_2988za7, "&every", 6 );
DEFINE_STRING( BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2556za700za7za7_2989za7, "list-ref", 8 );
DEFINE_STRING( BGl_string2475z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2475za700za7za7_2990za7, "cadr", 4 );
DEFINE_STRING( BGl_string2719z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2719za700za7za7_2991za7, "any", 3 );
DEFINE_STRING( BGl_string2557z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2557za700za7za7_2992za7, "&list-ref", 9 );
DEFINE_STRING( BGl_string2476z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2476za700za7za7_2993za7, "&cadr", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_deletez12zd2envzc0zz__r4_pairs_and_lists_6_3z00, BgL_bgl__deleteza712za712za7za7_2994z00, opt_generic_entry, BGl__deletez12z12zz__r4_pairs_and_lists_6_3z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2639z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2639za700za7za7_2995za7, "head1074", 8 );
DEFINE_STRING( BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2558za700za7za7_2996za7, "list-set!", 9 );
DEFINE_STRING( BGl_string2477z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2477za700za7za7_2997za7, "cdar", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dropzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762dropza762za7za7__r42998z00, BGl_z62dropz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2559z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2559za700za7za7_2999za7, "&list-set!", 10 );
DEFINE_STRING( BGl_string2478z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2478za700za7za7_3000za7, "&cdar", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2splitz12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2splitza73001za7, va_generic_entry, BGl_z62listzd2splitz12za2zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -3 );
DEFINE_STRING( BGl_string2479z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2479za700za7za7_3002za7, "cddr", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd22zd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762appendza7d22za7b03003za7, BGl_z62appendzd22zb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdaadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdaadrza762za7za7__3004z00, BGl_z62cdaadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caddarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caddarza762za7za7__3005z00, BGl_z62caddarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2801z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2801za700za7za7_3006za7, "arg1594", 7 );
DEFINE_STRING( BGl_string2802z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2802za700za7za7_3007za7, "&find-tail", 10 );
DEFINE_STRING( BGl_string2722z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2722za700za7za7_3008za7, "arg1558", 7 );
DEFINE_STRING( BGl_string2560z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2560za700za7za7_3009za7, "last-pair", 9 );
DEFINE_STRING( BGl_string2723z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2723za700za7za7_3010za7, "<@anonymous:1577>", 17 );
DEFINE_STRING( BGl_string2561z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2561za700za7za7_3011za7, "&last-pair", 10 );
DEFINE_STRING( BGl_string2480z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2480za700za7za7_3012za7, "&cddr", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_deletezd2duplicatesz12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl__deleteza7d2duplic3013za7, opt_generic_entry, BGl__deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2805z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2805za700za7za7_3014za7, "f", 1 );
DEFINE_STRING( BGl_string2562z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2562za700za7za7_3015za7, "&memq", 5 );
DEFINE_STRING( BGl_string2481z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2481za700za7za7_3016za7, "caaar", 5 );
DEFINE_STRING( BGl_string2644z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2644za700za7za7_3017za7, "arg1546", 7 );
DEFINE_STRING( BGl_string2563z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2563za700za7za7_3018za7, "&memv", 5 );
DEFINE_STRING( BGl_string2482z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2482za700za7za7_3019za7, "&caaar", 6 );
DEFINE_STRING( BGl_string2807z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2807za700za7za7_3020za7, "arg1602", 7 );
DEFINE_STRING( BGl_string2564z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2564za700za7za7_3021za7, "&member", 7 );
DEFINE_STRING( BGl_string2483z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2483za700za7za7_3022za7, "caadr", 5 );
DEFINE_STRING( BGl_string2565z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2565za700za7za7_3023za7, "&assq", 5 );
DEFINE_STRING( BGl_string2484z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2484za700za7za7_3024za7, "&caadr", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2tailzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2tailza7b3025za7, BGl_z62listzd2tailzb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2809z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2809za700za7za7_3026za7, "ans", 3 );
DEFINE_STRING( BGl_string2566z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2566za700za7za7_3027za7, "&assv", 5 );
DEFINE_STRING( BGl_string2485z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2485za700za7za7_3028za7, "cadar", 5 );
DEFINE_STRING( BGl_string2729z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2729za700za7za7_3029za7, "l1084", 5 );
DEFINE_STRING( BGl_string2567z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2567za700za7za7_3030za7, "&assoc", 6 );
DEFINE_STRING( BGl_string2486z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2486za700za7za7_3031za7, "&cadar", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2splitzd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762listza7d2splitza73032za7, va_generic_entry, BGl_z62listzd2splitzb0zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -3 );
DEFINE_STRING( BGl_string2568z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2568za700za7za7_3033za7, "remq", 4 );
DEFINE_STRING( BGl_string2487z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2487za700za7za7_3034za7, "caddr", 5 );
DEFINE_STRING( BGl_string2569z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2569za700za7za7_3035za7, "&remq", 5 );
DEFINE_STRING( BGl_string2488z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2488za700za7za7_3036za7, "&caddr", 6 );
DEFINE_STRING( BGl_string2489z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2489za700za7za7_3037za7, "cdaar", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd22z12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762appendza7d22za7123038za7, BGl_z62appendzd22z12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdaaarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdaaarza762za7za7__3039z00, BGl_z62cdaaarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_lengthzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762lengthza762za7za7__3040z00, BGl_z62lengthz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2810z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2810za700za7za7_3041za7, "&reduce", 7 );
DEFINE_STRING( BGl_string2811z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2811za700za7za7_3042za7, "&make-list", 10 );
DEFINE_STRING( BGl_string2812z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2812za700za7za7_3043za7, "walk:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string2570z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2570za700za7za7_3044za7, "remq!", 5 );
DEFINE_STRING( BGl_string2571z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2571za700za7za7_3045za7, "&remq!", 6 );
DEFINE_STRING( BGl_string2490z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2490za700za7za7_3046za7, "&cdaar", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setzd2carz12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762setza7d2carza712za73047z00, BGl_z62setzd2carz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2815z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2815za700za7za7_3048za7, "init-proc", 9 );
DEFINE_STRING( BGl_string2491z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2491za700za7za7_3049za7, "cddar", 5 );
DEFINE_STRING( BGl_string2735z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2735za700za7za7_3050za7, "head1086", 8 );
DEFINE_STRING( BGl_string2654z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2654za700za7za7_3051za7, "$car", 4 );
DEFINE_STRING( BGl_string2573z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2573za700za7za7_3052za7, "delete", 6 );
DEFINE_STRING( BGl_string2492z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2492za700za7za7_3053za7, "&cddar", 6 );
DEFINE_STRING( BGl_string2817z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2817za700za7za7_3054za7, "i", 1 );
DEFINE_STRING( BGl_string2574z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2574za700za7za7_3055za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_STRING( BGl_string2493z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2493za700za7za7_3056za7, "cdadr", 5 );
DEFINE_STRING( BGl_string2818z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2818za700za7za7_3057za7, "&list-tabulate", 14 );
DEFINE_STRING( BGl_string2575z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2575za700za7za7_3058za7, "_delete", 7 );
DEFINE_STRING( BGl_string2494z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2494za700za7za7_3059za7, "&cdadr", 6 );
DEFINE_STRING( BGl_string2819z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2819za700za7za7_3060za7, "&list-split", 11 );
DEFINE_STRING( BGl_string2657z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2657za700za7za7_3061za7, "$cons", 5 );
DEFINE_STRING( BGl_string2576z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2576za700za7za7_3062za7, "loop~0", 6 );
DEFINE_STRING( BGl_string2495z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2495za700za7za7_3063za7, "cdddr", 5 );
DEFINE_STRING( BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2577za700za7za7_3064za7, "procedure", 9 );
DEFINE_STRING( BGl_string2496z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2496za700za7za7_3065za7, "&cdddr", 6 );
DEFINE_STRING( BGl_string2578z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2578za700za7za7_3066za7, "loop~0:Wrong number of arguments", 32 );
DEFINE_STRING( BGl_string2497z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2497za700za7za7_3067za7, "caaaar", 6 );
extern obj_t BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00;
DEFINE_STRING( BGl_string2498z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2498za700za7za7_3068za7, "&caaaar", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_memberzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762memberza762za7za7__3069z00, BGl_z62memberz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2499z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2499za700za7za7_3070za7, "caaadr", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdrza762za7za7__r4_3071z00, BGl_z62cdrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762findza762za7za7__r43072z00, BGl_z62findz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdaddrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdaddrza762za7za7__3073z00, BGl_z62cdaddrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2820z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2820za700za7za7_3074za7, "&list-split!", 12 );
DEFINE_STRING( BGl_string2821z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2821za700za7za7_3075za7, "iota", 4 );
DEFINE_STRING( BGl_string2740z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2740za700za7za7_3076za7, "arg1584", 7 );
DEFINE_STRING( BGl_string2822z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2822za700za7za7_3077za7, "&iota", 5 );
DEFINE_STRING( BGl_string2823z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2823za700za7za7_3078za7, "list-copy", 9 );
DEFINE_STRING( BGl_string2824z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2824za700za7za7_3079za7, "&list-copy", 10 );
DEFINE_STRING( BGl_string2662z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2662za700za7za7_3080za7, "g1077", 5 );
DEFINE_STRING( BGl_string2581z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2581za700za7za7_3081za7, "funcall", 7 );
DEFINE_STRING( BGl_string2825z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2825za700za7za7_3082za7, "tree-copy", 9 );
DEFINE_STRING( BGl_string2583z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2583za700za7za7_3083za7, "eq", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_deletezd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl__deleteza700za7za7__r43084za7, opt_generic_entry, BGl__deletez00zz__r4_pairs_and_lists_6_3z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2827z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2827za700za7za7_3085za7, "delete-duplicates", 17 );
DEFINE_STRING( BGl_string2828z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2828za700za7za7_3086za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string2666z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2666za700za7za7_3087za7, "$cdr", 4 );
DEFINE_STRING( BGl_string2585z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2585za700za7za7_3088za7, "x", 1 );
DEFINE_STRING( BGl_string2829z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2829za700za7za7_3089za7, "_delete-duplicates", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762carza762za7za7__r4_3090z00, BGl_z62carz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2587z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2587za700za7za7_3091za7, "arg1479", 7 );
DEFINE_STRING( BGl_string2669z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2669za700za7za7_3092za7, "labels", 6 );
DEFINE_STRING( BGl_string2589z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2589za700za7za7_3093za7, "delete!", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cadrzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cadrza762za7za7__r43094z00, BGl_z62cadrz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cdadarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762cdadarza762za7za7__3095z00, BGl_z62cdadarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_everyzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762everyza762za7za7__r3096z00, va_generic_entry, BGl_z62everyz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2831z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2831za700za7za7_3097za7, "delete-duplicates!", 18 );
DEFINE_STRING( BGl_string2832z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2832za700za7za7_3098za7, "_delete-duplicates!", 19 );
DEFINE_STRING( BGl_string2833z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2833za700za7za7_3099za7, "recur~0", 7 );
DEFINE_STRING( BGl_string2752z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2752za700za7za7_3100za7, "g1089", 5 );
DEFINE_STRING( BGl_string2590z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2590za700za7za7_3101za7, "_delete!", 8 );
DEFINE_STRING( BGl_string2834z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2834za700za7za7_3102za7, "__r4_pairs_and_lists_6_3", 24 );
DEFINE_STRING( BGl_string2591z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2591za700za7za7_3103za7, "laap", 4 );
DEFINE_STRING( BGl_string2592z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2592za700za7za7_3104za7, "laap:Wrong number of arguments", 30 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setzd2cdrz12zd2envz12zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762setza7d2cdrza712za73105z00, BGl_z62setzd2cdrz12za2zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2675z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2675za700za7za7_3106za7, "tail1075", 8 );
DEFINE_STRING( BGl_string2595z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2595za700za7za7_3107za7, "arg1498", 7 );
DEFINE_STRING( BGl_string2596z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2596za700za7za7_3108za7, "loop:Wrong number of arguments", 30 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_caarzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762caarza762za7za7__r43109z00, BGl_z62caarz62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_reducezd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762reduceza762za7za7__3110z00, BGl_z62reducez62zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2679z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2679za700za7za7_3111za7, "$pair?", 6 );
DEFINE_STRING( BGl_string2599z00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_string2599za700za7za7_3112za7, "arg1495", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_eappendzd22zd2envz00zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762eappendza7d22za7b3113za7, BGl_z62eappendzd22zb0zz__r4_pairs_and_lists_6_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_eappendzd2envzd2zz__r4_pairs_and_lists_6_3z00, BgL_bgl_za762eappendza762za7za7_3114z00, va_generic_entry, BGl_z62eappendz62zz__r4_pairs_and_lists_6_3z00, BUNSPEC, -1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol2800z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2721z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2804z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2643z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2806z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2808z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2614z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2617z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2814z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2700z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2734z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2701z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2653z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2620z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2572z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2816z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2621z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2704z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2705z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2739z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2706z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2626z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2709z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2629z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2661z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2710z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2711z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2826z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2712z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2713z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2665z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2632z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2584z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2586z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2716z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2668z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2635z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2636z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2588z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2637z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2830z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2751z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2720z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2672z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2640z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2803z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2674z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2641z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2642z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2594z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2757z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2724z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2725z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2759z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2726z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2678z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2645z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2727z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2646z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2598z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2647z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2649z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2730z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2731z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2683z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2650z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2813z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2732z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2651z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2766z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2733z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2652z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2736z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2688z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2655z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2737z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2738z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2658z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2659z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2579z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2771z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2741z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2660z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2742z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2743z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2744z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2696z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2663z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2745z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2664z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2746z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2747z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2748z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2667z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2749z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2750z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2670z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2671z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2786z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2753z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2754z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2673z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2755z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2593z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2756z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2676z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2758z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2677z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2597z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2761z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2680z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2762z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2681z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2763z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2682z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2764z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2765z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2685z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2686z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2768z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2687z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2769z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2770z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2690z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2691z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2773z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2692z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2774z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2693z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2775z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2694z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2776z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2695z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2777z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2778z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2779z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2698z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2699z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2780z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2781z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2782z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2783z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2784z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2785z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2788z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2789z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2790z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2791z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2792z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2793z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2799z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2608z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2612z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2615z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2702z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2624z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2707z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2630z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2714z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2633z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2638z00zz__r4_pairs_and_lists_6_3z00) );
ADD_ROOT( (void *)(&BGl_list2605z00zz__r4_pairs_and_lists_6_3z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long BgL_checksumz00_3300, char * BgL_fromz00_3301)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_pairs_and_lists_6_3z00(); 
BGl_cnstzd2initzd2zz__r4_pairs_and_lists_6_3z00(); 
return 
BGl_importedzd2moduleszd2initz00zz__r4_pairs_and_lists_6_3z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_pairs_and_lists_6_3z00(void)
{
{ /* Ieee/pairlist.scm 18 */
BGl_symbol2572z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2573z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2581z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2583z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2584z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2585z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2586z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2587z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2579z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2584z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2586z00zz__r4_pairs_and_lists_6_3z00, BNIL))))); 
BGl_symbol2588z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2589z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2594z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2595z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2593z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2584z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2594z00zz__r4_pairs_and_lists_6_3z00, BNIL))))); 
BGl_symbol2598z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2599z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2597z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2582z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2598z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2584z00zz__r4_pairs_and_lists_6_3z00, BNIL))))); 
BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2607z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2608z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2609z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2605z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2608z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_symbol2612z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2535z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2615z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2616z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2619z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2623z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2624z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2625z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2621z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2624z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2620z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2621z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2628z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2630z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2631z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2629z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2630z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2624z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_symbol2633z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2634z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2632z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2633z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BNIL, BNIL)); 
BGl_symbol2638z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2639z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2643z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2644z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2469z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2651z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2650z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2651z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2653z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2654z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2652z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2653z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2649z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2650z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2652z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2647z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2649z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2646z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2647z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2645z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2646z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2652z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2642z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2643z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2645z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2641z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2642z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2657z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2655z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2643z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2632z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2640z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2641z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2655z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2637z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2638z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2640z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2636z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2637z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2661z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2662z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2665z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2666z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2664z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2665z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2663z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2650z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2664z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2660z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2661z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2663z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2659z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2660z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2668z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2669z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2672z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2610z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2674z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2675z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2673z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2674z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_symbol2678z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2679z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2677z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2678z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_symbol2683z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2684z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2688z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2689z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2693z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2653z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2692z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2693z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2691z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2692z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2690z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2691z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2652z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2687z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2688z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2690z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2686z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2687z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2694z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2688z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2632z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2685z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2686z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2694z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2682z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2685z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2681z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2682z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2696z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2697z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2700z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2674z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2699z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2700z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2702z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2703z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2701z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2702z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2698z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2699z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2701z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_symbol2707z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2708z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2709z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2665z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2706z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2707z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2709z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2705z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2706z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2710z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2707z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2704z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2705z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2710z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2695z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2696z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2698z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2704z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2680z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2681z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2695z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2712z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2630z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_symbol2714z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2715z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2713z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2714z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2622z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(
BINT(33201L), BNIL)))))); 
BGl_list2711z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2712z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2638z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2713z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_list2676z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2677z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2680z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2711z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_list2671z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2673z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2676z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2670z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2671z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2716z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2672z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2661z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2638z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2667z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2668z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2670z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2716z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2658z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2659z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2667z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2635z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2636z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2658z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2626z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2629z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2632z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2635z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_list2617z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2620z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2626z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2614z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2615z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2617z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_symbol2721z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2722z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2720z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2721z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2729z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2727z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2624z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2726z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2727z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2734z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2735z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2739z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2740z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2746z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2745z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2746z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2744z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2745z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2652z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2743z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2744z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2742z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2743z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2741z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2742z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2652z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2738z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2739z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2741z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2737z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2738z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2747z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2739z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2632z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2736z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2737z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2747z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2733z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2734z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2736z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2732z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2733z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2751z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2752z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2753z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2745z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2664z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2750z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2751z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2753z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2749z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2750z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_symbol2757z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2723z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2759z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2760z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2758z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2759z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2762z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2678z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_symbol2766z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2767z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2771z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2772z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2776z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2653z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2775z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2776z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2774z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2775z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2773z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2774z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2652z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2770z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2771z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2773z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2769z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2770z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2777z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2656z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2771z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2632z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2768z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2769z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2777z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2765z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2768z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2764z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2765z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2781z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2759z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2780z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2781z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2782z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2702z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2648z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2779z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2780z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2782z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_symbol2786z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2787z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2788z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2665z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2785z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2786z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2788z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2784z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2785z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2789z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2757z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2786z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2783z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2784z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2789z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2778z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2696z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2779z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2783z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2763z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2764z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2778z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2791z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2630z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, BNIL)); 
BGl_list2792z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2714z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2728z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(
BINT(33725L), BNIL)))))); 
BGl_list2790z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2791z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2734z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2792z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_list2761z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2762z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2763z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2790z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_list2756z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2757z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2758z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2761z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2755z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_list2756z00zz__r4_pairs_and_lists_6_3z00, BNIL); 
BGl_list2793z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2757z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2751z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2734z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2754z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2668z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2755z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2793z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2748z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2749z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2754z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2731z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2732z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2748z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2730z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2627z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2629z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2632z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2731z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_list2725z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2618z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2726z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2730z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_list2724z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2615z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_list2725z00zz__r4_pairs_and_lists_6_3z00, BNIL))); 
BGl_symbol2800z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2801z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2799z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2606z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2800z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_symbol2804z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2805z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2806z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2807z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2808z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2809z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2803z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2804z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2804z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2806z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2808z00zz__r4_pairs_and_lists_6_3z00, BNIL))))); 
BGl_symbol2814z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2815z00zz__r4_pairs_and_lists_6_3z00); 
BGl_symbol2816z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2817z00zz__r4_pairs_and_lists_6_3z00); 
BGl_list2813z00zz__r4_pairs_and_lists_6_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2580z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2814z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2814z00zz__r4_pairs_and_lists_6_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2816z00zz__r4_pairs_and_lists_6_3z00, BNIL)))); 
BGl_symbol2826z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2827z00zz__r4_pairs_and_lists_6_3z00); 
return ( 
BGl_symbol2830z00zz__r4_pairs_and_lists_6_3z00 = 
bstring_to_symbol(BGl_string2831z00zz__r4_pairs_and_lists_6_3z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_pairs_and_lists_6_3z00(void)
{
{ /* Ieee/pairlist.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* append-21011 */
obj_t BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
{
{ 
{ 
 obj_t BgL_headz00_806;
BgL_headz00_806 = 
MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2); 
{ 
 obj_t BgL_prevz00_807; obj_t BgL_tailz00_808;
BgL_prevz00_807 = BgL_headz00_806; 
BgL_tailz00_808 = BgL_l1z00_1; 
BgL_loopz00_809:
if(
PAIRP(BgL_tailz00_808))
{ 
 obj_t BgL_newzd2prevzd2_811;
BgL_newzd2prevzd2_811 = 
MAKE_YOUNG_PAIR(
CAR(BgL_tailz00_808), BgL_l2z00_2); 
SET_CDR(BgL_prevz00_807, BgL_newzd2prevzd2_811); 
{ 
 obj_t BgL_tailz00_3665; obj_t BgL_prevz00_3664;
BgL_prevz00_3664 = BgL_newzd2prevzd2_811; 
BgL_tailz00_3665 = 
CDR(BgL_tailz00_808); 
BgL_tailz00_808 = BgL_tailz00_3665; 
BgL_prevz00_807 = BgL_prevz00_3664; 
goto BgL_loopz00_809;} }  else 
{ BNIL; } 
return 
CDR(BgL_headz00_806);} } } 

}



/* pair? */
BGL_EXPORTED_DEF bool_t BGl_pairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_3)
{
{ /* Ieee/pairlist.scm 228 */
return 
PAIRP(BgL_objz00_3);} 

}



/* &pair? */
obj_t BGl_z62pairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2297, obj_t BgL_objz00_2298)
{
{ /* Ieee/pairlist.scm 228 */
return 
BBOOL(
BGl_pairzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2298));} 

}



/* epair? */
BGL_EXPORTED_DEF bool_t BGl_epairzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_4)
{
{ /* Ieee/pairlist.scm 234 */
return 
EPAIRP(BgL_objz00_4);} 

}



/* &epair? */
obj_t BGl_z62epairzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2299, obj_t BgL_objz00_2300)
{
{ /* Ieee/pairlist.scm 234 */
return 
BBOOL(
BGl_epairzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2300));} 

}



/* pair-or-null? */
BGL_EXPORTED_DEF bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_5)
{
{ /* Ieee/pairlist.scm 240 */
if(
PAIRP(BgL_objz00_5))
{ /* Ieee/pairlist.scm 241 */
return ((bool_t)1);}  else 
{ /* Ieee/pairlist.scm 241 */
return 
NULLP(BgL_objz00_5);} } 

}



/* &pair-or-null? */
obj_t BGl_z62pairzd2orzd2nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2301, obj_t BgL_objz00_2302)
{
{ /* Ieee/pairlist.scm 240 */
return 
BBOOL(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2302));} 

}



/* cons */
BGL_EXPORTED_DEF obj_t BGl_consz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_obj1z00_6, obj_t BgL_obj2z00_7)
{
{ /* Ieee/pairlist.scm 248 */
return 
MAKE_YOUNG_PAIR(BgL_obj1z00_6, BgL_obj2z00_7);} 

}



/* &cons */
obj_t BGl_z62consz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2303, obj_t BgL_obj1z00_2304, obj_t BgL_obj2z00_2305)
{
{ /* Ieee/pairlist.scm 248 */
return 
BGl_consz00zz__r4_pairs_and_lists_6_3z00(BgL_obj1z00_2304, BgL_obj2z00_2305);} 

}



/* econs */
BGL_EXPORTED_DEF obj_t BGl_econsz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_obj1z00_8, obj_t BgL_obj2z00_9, obj_t BgL_obj3z00_10)
{
{ /* Ieee/pairlist.scm 254 */
return 
MAKE_YOUNG_EPAIR(BgL_obj1z00_8, BgL_obj2z00_9, BgL_obj3z00_10);} 

}



/* &econs */
obj_t BGl_z62econsz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2306, obj_t BgL_obj1z00_2307, obj_t BgL_obj2z00_2308, obj_t BgL_obj3z00_2309)
{
{ /* Ieee/pairlist.scm 254 */
return 
BGl_econsz00zz__r4_pairs_and_lists_6_3z00(BgL_obj1z00_2307, BgL_obj2z00_2308, BgL_obj3z00_2309);} 

}



/* car */
BGL_EXPORTED_DEF obj_t BGl_carz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_11)
{
{ /* Ieee/pairlist.scm 260 */
return 
CAR(BgL_pairz00_11);} 

}



/* &car */
obj_t BGl_z62carz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2310, obj_t BgL_pairz00_2311)
{
{ /* Ieee/pairlist.scm 260 */
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_auxz00_3684;
if(
PAIRP(BgL_pairz00_2311))
{ /* Ieee/pairlist.scm 261 */
BgL_auxz00_3684 = BgL_pairz00_2311
; }  else 
{ 
 obj_t BgL_auxz00_3687;
BgL_auxz00_3687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2468z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2311); 
FAILURE(BgL_auxz00_3687,BFALSE,BFALSE);} 
return 
BGl_carz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3684);} } 

}



/* cdr */
BGL_EXPORTED_DEF obj_t BGl_cdrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_12)
{
{ /* Ieee/pairlist.scm 266 */
return 
CDR(BgL_pairz00_12);} 

}



/* &cdr */
obj_t BGl_z62cdrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2312, obj_t BgL_pairz00_2313)
{
{ /* Ieee/pairlist.scm 266 */
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_auxz00_3693;
if(
PAIRP(BgL_pairz00_2313))
{ /* Ieee/pairlist.scm 267 */
BgL_auxz00_3693 = BgL_pairz00_2313
; }  else 
{ 
 obj_t BgL_auxz00_3696;
BgL_auxz00_3696 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2470z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2313); 
FAILURE(BgL_auxz00_3696,BFALSE,BFALSE);} 
return 
BGl_cdrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3693);} } 

}



/* cer */
BGL_EXPORTED_DEF obj_t BGl_cerz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_13)
{
{ /* Ieee/pairlist.scm 272 */
return 
CER(BgL_objz00_13);} 

}



/* &cer */
obj_t BGl_z62cerz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2314, obj_t BgL_objz00_2315)
{
{ /* Ieee/pairlist.scm 272 */
{ /* Ieee/pairlist.scm 273 */
 obj_t BgL_auxz00_3702;
if(
EPAIRP(BgL_objz00_2315))
{ /* Ieee/pairlist.scm 273 */
BgL_auxz00_3702 = BgL_objz00_2315
; }  else 
{ 
 obj_t BgL_auxz00_3705;
BgL_auxz00_3705 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(11240L), BGl_string2471z00zz__r4_pairs_and_lists_6_3z00, BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_objz00_2315); 
FAILURE(BgL_auxz00_3705,BFALSE,BFALSE);} 
return 
BGl_cerz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3702);} } 

}



/* caar */
BGL_EXPORTED_DEF obj_t BGl_caarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_14)
{
{ /* Ieee/pairlist.scm 278 */
{ /* Ieee/pairlist.scm 279 */
 obj_t BgL_pairz00_3164;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1871z00_3165;
BgL_aux1871z00_3165 = 
CAR(BgL_pairz00_14); 
if(
PAIRP(BgL_aux1871z00_3165))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3164 = BgL_aux1871z00_3165; }  else 
{ 
 obj_t BgL_auxz00_3713;
BgL_auxz00_3713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2473z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1871z00_3165); 
FAILURE(BgL_auxz00_3713,BFALSE,BFALSE);} } 
return 
CAR(BgL_pairz00_3164);} } 

}



/* &caar */
obj_t BGl_z62caarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2316, obj_t BgL_pairz00_2317)
{
{ /* Ieee/pairlist.scm 278 */
{ /* Ieee/pairlist.scm 279 */
 obj_t BgL_auxz00_3718;
if(
PAIRP(BgL_pairz00_2317))
{ /* Ieee/pairlist.scm 279 */
BgL_auxz00_3718 = BgL_pairz00_2317
; }  else 
{ 
 obj_t BgL_auxz00_3721;
BgL_auxz00_3721 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(11510L), BGl_string2474z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2317); 
FAILURE(BgL_auxz00_3721,BFALSE,BFALSE);} 
return 
BGl_caarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3718);} } 

}



/* cadr */
BGL_EXPORTED_DEF obj_t BGl_cadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_15)
{
{ /* Ieee/pairlist.scm 284 */
{ /* Ieee/pairlist.scm 285 */
 obj_t BgL_pairz00_3166;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1875z00_3167;
BgL_aux1875z00_3167 = 
CDR(BgL_pairz00_15); 
if(
PAIRP(BgL_aux1875z00_3167))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3166 = BgL_aux1875z00_3167; }  else 
{ 
 obj_t BgL_auxz00_3729;
BgL_auxz00_3729 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2475z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1875z00_3167); 
FAILURE(BgL_auxz00_3729,BFALSE,BFALSE);} } 
return 
CAR(BgL_pairz00_3166);} } 

}



/* &cadr */
obj_t BGl_z62cadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2318, obj_t BgL_pairz00_2319)
{
{ /* Ieee/pairlist.scm 284 */
{ /* Ieee/pairlist.scm 285 */
 obj_t BgL_auxz00_3734;
if(
PAIRP(BgL_pairz00_2319))
{ /* Ieee/pairlist.scm 285 */
BgL_auxz00_3734 = BgL_pairz00_2319
; }  else 
{ 
 obj_t BgL_auxz00_3737;
BgL_auxz00_3737 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(11781L), BGl_string2476z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2319); 
FAILURE(BgL_auxz00_3737,BFALSE,BFALSE);} 
return 
BGl_cadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3734);} } 

}



/* cdar */
BGL_EXPORTED_DEF obj_t BGl_cdarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_16)
{
{ /* Ieee/pairlist.scm 290 */
{ /* Ieee/pairlist.scm 291 */
 obj_t BgL_pairz00_3168;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1879z00_3169;
BgL_aux1879z00_3169 = 
CAR(BgL_pairz00_16); 
if(
PAIRP(BgL_aux1879z00_3169))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3168 = BgL_aux1879z00_3169; }  else 
{ 
 obj_t BgL_auxz00_3745;
BgL_auxz00_3745 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2477z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1879z00_3169); 
FAILURE(BgL_auxz00_3745,BFALSE,BFALSE);} } 
return 
CDR(BgL_pairz00_3168);} } 

}



/* &cdar */
obj_t BGl_z62cdarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2320, obj_t BgL_pairz00_2321)
{
{ /* Ieee/pairlist.scm 290 */
{ /* Ieee/pairlist.scm 291 */
 obj_t BgL_auxz00_3750;
if(
PAIRP(BgL_pairz00_2321))
{ /* Ieee/pairlist.scm 291 */
BgL_auxz00_3750 = BgL_pairz00_2321
; }  else 
{ 
 obj_t BgL_auxz00_3753;
BgL_auxz00_3753 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(12052L), BGl_string2478z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2321); 
FAILURE(BgL_auxz00_3753,BFALSE,BFALSE);} 
return 
BGl_cdarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3750);} } 

}



/* cddr */
BGL_EXPORTED_DEF obj_t BGl_cddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_17)
{
{ /* Ieee/pairlist.scm 296 */
{ /* Ieee/pairlist.scm 297 */
 obj_t BgL_pairz00_3170;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1883z00_3171;
BgL_aux1883z00_3171 = 
CDR(BgL_pairz00_17); 
if(
PAIRP(BgL_aux1883z00_3171))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3170 = BgL_aux1883z00_3171; }  else 
{ 
 obj_t BgL_auxz00_3761;
BgL_auxz00_3761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2479z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1883z00_3171); 
FAILURE(BgL_auxz00_3761,BFALSE,BFALSE);} } 
return 
CDR(BgL_pairz00_3170);} } 

}



/* &cddr */
obj_t BGl_z62cddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2322, obj_t BgL_pairz00_2323)
{
{ /* Ieee/pairlist.scm 296 */
{ /* Ieee/pairlist.scm 297 */
 obj_t BgL_auxz00_3766;
if(
PAIRP(BgL_pairz00_2323))
{ /* Ieee/pairlist.scm 297 */
BgL_auxz00_3766 = BgL_pairz00_2323
; }  else 
{ 
 obj_t BgL_auxz00_3769;
BgL_auxz00_3769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(12323L), BGl_string2480z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2323); 
FAILURE(BgL_auxz00_3769,BFALSE,BFALSE);} 
return 
BGl_cddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3766);} } 

}



/* caaar */
BGL_EXPORTED_DEF obj_t BGl_caaarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_18)
{
{ /* Ieee/pairlist.scm 302 */
{ /* Ieee/pairlist.scm 303 */
 obj_t BgL_pairz00_3172;
{ /* Ieee/pairlist.scm 303 */
 obj_t BgL_pairz00_3173;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1887z00_3174;
BgL_aux1887z00_3174 = 
CAR(BgL_pairz00_18); 
if(
PAIRP(BgL_aux1887z00_3174))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3173 = BgL_aux1887z00_3174; }  else 
{ 
 obj_t BgL_auxz00_3777;
BgL_auxz00_3777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2481z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1887z00_3174); 
FAILURE(BgL_auxz00_3777,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1889z00_3175;
BgL_aux1889z00_3175 = 
CAR(BgL_pairz00_3173); 
if(
PAIRP(BgL_aux1889z00_3175))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3172 = BgL_aux1889z00_3175; }  else 
{ 
 obj_t BgL_auxz00_3784;
BgL_auxz00_3784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2481z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1889z00_3175); 
FAILURE(BgL_auxz00_3784,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3172);} } 

}



/* &caaar */
obj_t BGl_z62caaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2324, obj_t BgL_pairz00_2325)
{
{ /* Ieee/pairlist.scm 302 */
{ /* Ieee/pairlist.scm 303 */
 obj_t BgL_auxz00_3789;
if(
PAIRP(BgL_pairz00_2325))
{ /* Ieee/pairlist.scm 303 */
BgL_auxz00_3789 = BgL_pairz00_2325
; }  else 
{ 
 obj_t BgL_auxz00_3792;
BgL_auxz00_3792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(12600L), BGl_string2482z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2325); 
FAILURE(BgL_auxz00_3792,BFALSE,BFALSE);} 
return 
BGl_caaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3789);} } 

}



/* caadr */
BGL_EXPORTED_DEF obj_t BGl_caadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_19)
{
{ /* Ieee/pairlist.scm 308 */
{ /* Ieee/pairlist.scm 309 */
 obj_t BgL_pairz00_3176;
{ /* Ieee/pairlist.scm 309 */
 obj_t BgL_pairz00_3177;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1893z00_3178;
BgL_aux1893z00_3178 = 
CDR(BgL_pairz00_19); 
if(
PAIRP(BgL_aux1893z00_3178))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3177 = BgL_aux1893z00_3178; }  else 
{ 
 obj_t BgL_auxz00_3800;
BgL_auxz00_3800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2483z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1893z00_3178); 
FAILURE(BgL_auxz00_3800,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1895z00_3179;
BgL_aux1895z00_3179 = 
CAR(BgL_pairz00_3177); 
if(
PAIRP(BgL_aux1895z00_3179))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3176 = BgL_aux1895z00_3179; }  else 
{ 
 obj_t BgL_auxz00_3807;
BgL_auxz00_3807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2483z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1895z00_3179); 
FAILURE(BgL_auxz00_3807,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3176);} } 

}



/* &caadr */
obj_t BGl_z62caadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2326, obj_t BgL_pairz00_2327)
{
{ /* Ieee/pairlist.scm 308 */
{ /* Ieee/pairlist.scm 309 */
 obj_t BgL_auxz00_3812;
if(
PAIRP(BgL_pairz00_2327))
{ /* Ieee/pairlist.scm 309 */
BgL_auxz00_3812 = BgL_pairz00_2327
; }  else 
{ 
 obj_t BgL_auxz00_3815;
BgL_auxz00_3815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(12878L), BGl_string2484z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2327); 
FAILURE(BgL_auxz00_3815,BFALSE,BFALSE);} 
return 
BGl_caadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3812);} } 

}



/* cadar */
BGL_EXPORTED_DEF obj_t BGl_cadarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_20)
{
{ /* Ieee/pairlist.scm 314 */
{ /* Ieee/pairlist.scm 315 */
 obj_t BgL_pairz00_3180;
{ /* Ieee/pairlist.scm 315 */
 obj_t BgL_pairz00_3181;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1899z00_3182;
BgL_aux1899z00_3182 = 
CAR(BgL_pairz00_20); 
if(
PAIRP(BgL_aux1899z00_3182))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3181 = BgL_aux1899z00_3182; }  else 
{ 
 obj_t BgL_auxz00_3823;
BgL_auxz00_3823 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2485z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1899z00_3182); 
FAILURE(BgL_auxz00_3823,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1901z00_3183;
BgL_aux1901z00_3183 = 
CDR(BgL_pairz00_3181); 
if(
PAIRP(BgL_aux1901z00_3183))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3180 = BgL_aux1901z00_3183; }  else 
{ 
 obj_t BgL_auxz00_3830;
BgL_auxz00_3830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2485z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1901z00_3183); 
FAILURE(BgL_auxz00_3830,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3180);} } 

}



/* &cadar */
obj_t BGl_z62cadarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2328, obj_t BgL_pairz00_2329)
{
{ /* Ieee/pairlist.scm 314 */
{ /* Ieee/pairlist.scm 315 */
 obj_t BgL_auxz00_3835;
if(
PAIRP(BgL_pairz00_2329))
{ /* Ieee/pairlist.scm 315 */
BgL_auxz00_3835 = BgL_pairz00_2329
; }  else 
{ 
 obj_t BgL_auxz00_3838;
BgL_auxz00_3838 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(13156L), BGl_string2486z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2329); 
FAILURE(BgL_auxz00_3838,BFALSE,BFALSE);} 
return 
BGl_cadarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3835);} } 

}



/* caddr */
BGL_EXPORTED_DEF obj_t BGl_caddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_21)
{
{ /* Ieee/pairlist.scm 320 */
{ /* Ieee/pairlist.scm 321 */
 obj_t BgL_pairz00_3184;
{ /* Ieee/pairlist.scm 321 */
 obj_t BgL_pairz00_3185;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1905z00_3186;
BgL_aux1905z00_3186 = 
CDR(BgL_pairz00_21); 
if(
PAIRP(BgL_aux1905z00_3186))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3185 = BgL_aux1905z00_3186; }  else 
{ 
 obj_t BgL_auxz00_3846;
BgL_auxz00_3846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2487z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1905z00_3186); 
FAILURE(BgL_auxz00_3846,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1907z00_3187;
BgL_aux1907z00_3187 = 
CDR(BgL_pairz00_3185); 
if(
PAIRP(BgL_aux1907z00_3187))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3184 = BgL_aux1907z00_3187; }  else 
{ 
 obj_t BgL_auxz00_3853;
BgL_auxz00_3853 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2487z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1907z00_3187); 
FAILURE(BgL_auxz00_3853,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3184);} } 

}



/* &caddr */
obj_t BGl_z62caddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2330, obj_t BgL_pairz00_2331)
{
{ /* Ieee/pairlist.scm 320 */
{ /* Ieee/pairlist.scm 321 */
 obj_t BgL_auxz00_3858;
if(
PAIRP(BgL_pairz00_2331))
{ /* Ieee/pairlist.scm 321 */
BgL_auxz00_3858 = BgL_pairz00_2331
; }  else 
{ 
 obj_t BgL_auxz00_3861;
BgL_auxz00_3861 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(13434L), BGl_string2488z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2331); 
FAILURE(BgL_auxz00_3861,BFALSE,BFALSE);} 
return 
BGl_caddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3858);} } 

}



/* cdaar */
BGL_EXPORTED_DEF obj_t BGl_cdaarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_22)
{
{ /* Ieee/pairlist.scm 326 */
{ /* Ieee/pairlist.scm 327 */
 obj_t BgL_pairz00_3188;
{ /* Ieee/pairlist.scm 327 */
 obj_t BgL_pairz00_3189;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1911z00_3190;
BgL_aux1911z00_3190 = 
CAR(BgL_pairz00_22); 
if(
PAIRP(BgL_aux1911z00_3190))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3189 = BgL_aux1911z00_3190; }  else 
{ 
 obj_t BgL_auxz00_3869;
BgL_auxz00_3869 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2489z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1911z00_3190); 
FAILURE(BgL_auxz00_3869,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1913z00_3191;
BgL_aux1913z00_3191 = 
CAR(BgL_pairz00_3189); 
if(
PAIRP(BgL_aux1913z00_3191))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3188 = BgL_aux1913z00_3191; }  else 
{ 
 obj_t BgL_auxz00_3876;
BgL_auxz00_3876 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2489z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1913z00_3191); 
FAILURE(BgL_auxz00_3876,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3188);} } 

}



/* &cdaar */
obj_t BGl_z62cdaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2332, obj_t BgL_pairz00_2333)
{
{ /* Ieee/pairlist.scm 326 */
{ /* Ieee/pairlist.scm 327 */
 obj_t BgL_auxz00_3881;
if(
PAIRP(BgL_pairz00_2333))
{ /* Ieee/pairlist.scm 327 */
BgL_auxz00_3881 = BgL_pairz00_2333
; }  else 
{ 
 obj_t BgL_auxz00_3884;
BgL_auxz00_3884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(13712L), BGl_string2490z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2333); 
FAILURE(BgL_auxz00_3884,BFALSE,BFALSE);} 
return 
BGl_cdaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3881);} } 

}



/* cddar */
BGL_EXPORTED_DEF obj_t BGl_cddarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_23)
{
{ /* Ieee/pairlist.scm 332 */
{ /* Ieee/pairlist.scm 333 */
 obj_t BgL_pairz00_3192;
{ /* Ieee/pairlist.scm 333 */
 obj_t BgL_pairz00_3193;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1917z00_3194;
BgL_aux1917z00_3194 = 
CAR(BgL_pairz00_23); 
if(
PAIRP(BgL_aux1917z00_3194))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3193 = BgL_aux1917z00_3194; }  else 
{ 
 obj_t BgL_auxz00_3892;
BgL_auxz00_3892 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2491z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1917z00_3194); 
FAILURE(BgL_auxz00_3892,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1919z00_3195;
BgL_aux1919z00_3195 = 
CDR(BgL_pairz00_3193); 
if(
PAIRP(BgL_aux1919z00_3195))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3192 = BgL_aux1919z00_3195; }  else 
{ 
 obj_t BgL_auxz00_3899;
BgL_auxz00_3899 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2491z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1919z00_3195); 
FAILURE(BgL_auxz00_3899,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3192);} } 

}



/* &cddar */
obj_t BGl_z62cddarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2334, obj_t BgL_pairz00_2335)
{
{ /* Ieee/pairlist.scm 332 */
{ /* Ieee/pairlist.scm 333 */
 obj_t BgL_auxz00_3904;
if(
PAIRP(BgL_pairz00_2335))
{ /* Ieee/pairlist.scm 333 */
BgL_auxz00_3904 = BgL_pairz00_2335
; }  else 
{ 
 obj_t BgL_auxz00_3907;
BgL_auxz00_3907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(13990L), BGl_string2492z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2335); 
FAILURE(BgL_auxz00_3907,BFALSE,BFALSE);} 
return 
BGl_cddarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3904);} } 

}



/* cdadr */
BGL_EXPORTED_DEF obj_t BGl_cdadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_24)
{
{ /* Ieee/pairlist.scm 338 */
{ /* Ieee/pairlist.scm 339 */
 obj_t BgL_pairz00_3196;
{ /* Ieee/pairlist.scm 339 */
 obj_t BgL_pairz00_3197;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1923z00_3198;
BgL_aux1923z00_3198 = 
CDR(BgL_pairz00_24); 
if(
PAIRP(BgL_aux1923z00_3198))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3197 = BgL_aux1923z00_3198; }  else 
{ 
 obj_t BgL_auxz00_3915;
BgL_auxz00_3915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2493z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1923z00_3198); 
FAILURE(BgL_auxz00_3915,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1925z00_3199;
BgL_aux1925z00_3199 = 
CAR(BgL_pairz00_3197); 
if(
PAIRP(BgL_aux1925z00_3199))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3196 = BgL_aux1925z00_3199; }  else 
{ 
 obj_t BgL_auxz00_3922;
BgL_auxz00_3922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2493z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1925z00_3199); 
FAILURE(BgL_auxz00_3922,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3196);} } 

}



/* &cdadr */
obj_t BGl_z62cdadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2336, obj_t BgL_pairz00_2337)
{
{ /* Ieee/pairlist.scm 338 */
{ /* Ieee/pairlist.scm 339 */
 obj_t BgL_auxz00_3927;
if(
PAIRP(BgL_pairz00_2337))
{ /* Ieee/pairlist.scm 339 */
BgL_auxz00_3927 = BgL_pairz00_2337
; }  else 
{ 
 obj_t BgL_auxz00_3930;
BgL_auxz00_3930 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(14268L), BGl_string2494z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2337); 
FAILURE(BgL_auxz00_3930,BFALSE,BFALSE);} 
return 
BGl_cdadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3927);} } 

}



/* cdddr */
BGL_EXPORTED_DEF obj_t BGl_cdddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_25)
{
{ /* Ieee/pairlist.scm 344 */
{ /* Ieee/pairlist.scm 345 */
 obj_t BgL_pairz00_3200;
{ /* Ieee/pairlist.scm 345 */
 obj_t BgL_pairz00_3201;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1929z00_3202;
BgL_aux1929z00_3202 = 
CDR(BgL_pairz00_25); 
if(
PAIRP(BgL_aux1929z00_3202))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3201 = BgL_aux1929z00_3202; }  else 
{ 
 obj_t BgL_auxz00_3938;
BgL_auxz00_3938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2495z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1929z00_3202); 
FAILURE(BgL_auxz00_3938,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1931z00_3203;
BgL_aux1931z00_3203 = 
CDR(BgL_pairz00_3201); 
if(
PAIRP(BgL_aux1931z00_3203))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3200 = BgL_aux1931z00_3203; }  else 
{ 
 obj_t BgL_auxz00_3945;
BgL_auxz00_3945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2495z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1931z00_3203); 
FAILURE(BgL_auxz00_3945,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3200);} } 

}



/* &cdddr */
obj_t BGl_z62cdddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2338, obj_t BgL_pairz00_2339)
{
{ /* Ieee/pairlist.scm 344 */
{ /* Ieee/pairlist.scm 345 */
 obj_t BgL_auxz00_3950;
if(
PAIRP(BgL_pairz00_2339))
{ /* Ieee/pairlist.scm 345 */
BgL_auxz00_3950 = BgL_pairz00_2339
; }  else 
{ 
 obj_t BgL_auxz00_3953;
BgL_auxz00_3953 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(14546L), BGl_string2496z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2339); 
FAILURE(BgL_auxz00_3953,BFALSE,BFALSE);} 
return 
BGl_cdddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3950);} } 

}



/* caaaar */
BGL_EXPORTED_DEF obj_t BGl_caaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_26)
{
{ /* Ieee/pairlist.scm 350 */
{ /* Ieee/pairlist.scm 351 */
 obj_t BgL_pairz00_3204;
{ /* Ieee/pairlist.scm 351 */
 obj_t BgL_pairz00_3205;
{ /* Ieee/pairlist.scm 351 */
 obj_t BgL_pairz00_3206;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1935z00_3207;
BgL_aux1935z00_3207 = 
CAR(BgL_pairz00_26); 
if(
PAIRP(BgL_aux1935z00_3207))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3206 = BgL_aux1935z00_3207; }  else 
{ 
 obj_t BgL_auxz00_3961;
BgL_auxz00_3961 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2497z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1935z00_3207); 
FAILURE(BgL_auxz00_3961,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1937z00_3208;
BgL_aux1937z00_3208 = 
CAR(BgL_pairz00_3206); 
if(
PAIRP(BgL_aux1937z00_3208))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3205 = BgL_aux1937z00_3208; }  else 
{ 
 obj_t BgL_auxz00_3968;
BgL_auxz00_3968 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2497z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1937z00_3208); 
FAILURE(BgL_auxz00_3968,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1939z00_3209;
BgL_aux1939z00_3209 = 
CAR(BgL_pairz00_3205); 
if(
PAIRP(BgL_aux1939z00_3209))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3204 = BgL_aux1939z00_3209; }  else 
{ 
 obj_t BgL_auxz00_3975;
BgL_auxz00_3975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2497z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1939z00_3209); 
FAILURE(BgL_auxz00_3975,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3204);} } 

}



/* &caaaar */
obj_t BGl_z62caaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2340, obj_t BgL_pairz00_2341)
{
{ /* Ieee/pairlist.scm 350 */
{ /* Ieee/pairlist.scm 351 */
 obj_t BgL_auxz00_3980;
if(
PAIRP(BgL_pairz00_2341))
{ /* Ieee/pairlist.scm 351 */
BgL_auxz00_3980 = BgL_pairz00_2341
; }  else 
{ 
 obj_t BgL_auxz00_3983;
BgL_auxz00_3983 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(14830L), BGl_string2498z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2341); 
FAILURE(BgL_auxz00_3983,BFALSE,BFALSE);} 
return 
BGl_caaaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3980);} } 

}



/* caaadr */
BGL_EXPORTED_DEF obj_t BGl_caaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_27)
{
{ /* Ieee/pairlist.scm 356 */
{ /* Ieee/pairlist.scm 357 */
 obj_t BgL_pairz00_3210;
{ /* Ieee/pairlist.scm 357 */
 obj_t BgL_pairz00_3211;
{ /* Ieee/pairlist.scm 357 */
 obj_t BgL_pairz00_3212;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1943z00_3213;
BgL_aux1943z00_3213 = 
CDR(BgL_pairz00_27); 
if(
PAIRP(BgL_aux1943z00_3213))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3212 = BgL_aux1943z00_3213; }  else 
{ 
 obj_t BgL_auxz00_3991;
BgL_auxz00_3991 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2499z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1943z00_3213); 
FAILURE(BgL_auxz00_3991,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1945z00_3214;
BgL_aux1945z00_3214 = 
CAR(BgL_pairz00_3212); 
if(
PAIRP(BgL_aux1945z00_3214))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3211 = BgL_aux1945z00_3214; }  else 
{ 
 obj_t BgL_auxz00_3998;
BgL_auxz00_3998 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2499z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1945z00_3214); 
FAILURE(BgL_auxz00_3998,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1947z00_3215;
BgL_aux1947z00_3215 = 
CAR(BgL_pairz00_3211); 
if(
PAIRP(BgL_aux1947z00_3215))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3210 = BgL_aux1947z00_3215; }  else 
{ 
 obj_t BgL_auxz00_4005;
BgL_auxz00_4005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2499z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1947z00_3215); 
FAILURE(BgL_auxz00_4005,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3210);} } 

}



/* &caaadr */
obj_t BGl_z62caaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2342, obj_t BgL_pairz00_2343)
{
{ /* Ieee/pairlist.scm 356 */
{ /* Ieee/pairlist.scm 357 */
 obj_t BgL_auxz00_4010;
if(
PAIRP(BgL_pairz00_2343))
{ /* Ieee/pairlist.scm 357 */
BgL_auxz00_4010 = BgL_pairz00_2343
; }  else 
{ 
 obj_t BgL_auxz00_4013;
BgL_auxz00_4013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(15115L), BGl_string2500z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2343); 
FAILURE(BgL_auxz00_4013,BFALSE,BFALSE);} 
return 
BGl_caaadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4010);} } 

}



/* caadar */
BGL_EXPORTED_DEF obj_t BGl_caadarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_28)
{
{ /* Ieee/pairlist.scm 362 */
{ /* Ieee/pairlist.scm 363 */
 obj_t BgL_pairz00_3216;
{ /* Ieee/pairlist.scm 363 */
 obj_t BgL_pairz00_3217;
{ /* Ieee/pairlist.scm 363 */
 obj_t BgL_pairz00_3218;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1951z00_3219;
BgL_aux1951z00_3219 = 
CAR(BgL_pairz00_28); 
if(
PAIRP(BgL_aux1951z00_3219))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3218 = BgL_aux1951z00_3219; }  else 
{ 
 obj_t BgL_auxz00_4021;
BgL_auxz00_4021 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2501z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1951z00_3219); 
FAILURE(BgL_auxz00_4021,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1953z00_3220;
BgL_aux1953z00_3220 = 
CDR(BgL_pairz00_3218); 
if(
PAIRP(BgL_aux1953z00_3220))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3217 = BgL_aux1953z00_3220; }  else 
{ 
 obj_t BgL_auxz00_4028;
BgL_auxz00_4028 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2501z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1953z00_3220); 
FAILURE(BgL_auxz00_4028,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1955z00_3221;
BgL_aux1955z00_3221 = 
CAR(BgL_pairz00_3217); 
if(
PAIRP(BgL_aux1955z00_3221))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3216 = BgL_aux1955z00_3221; }  else 
{ 
 obj_t BgL_auxz00_4035;
BgL_auxz00_4035 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2501z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1955z00_3221); 
FAILURE(BgL_auxz00_4035,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3216);} } 

}



/* &caadar */
obj_t BGl_z62caadarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2344, obj_t BgL_pairz00_2345)
{
{ /* Ieee/pairlist.scm 362 */
{ /* Ieee/pairlist.scm 363 */
 obj_t BgL_auxz00_4040;
if(
PAIRP(BgL_pairz00_2345))
{ /* Ieee/pairlist.scm 363 */
BgL_auxz00_4040 = BgL_pairz00_2345
; }  else 
{ 
 obj_t BgL_auxz00_4043;
BgL_auxz00_4043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(15400L), BGl_string2502z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2345); 
FAILURE(BgL_auxz00_4043,BFALSE,BFALSE);} 
return 
BGl_caadarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4040);} } 

}



/* cadaar */
BGL_EXPORTED_DEF obj_t BGl_cadaarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_29)
{
{ /* Ieee/pairlist.scm 368 */
{ /* Ieee/pairlist.scm 369 */
 obj_t BgL_pairz00_3222;
{ /* Ieee/pairlist.scm 369 */
 obj_t BgL_pairz00_3223;
{ /* Ieee/pairlist.scm 369 */
 obj_t BgL_pairz00_3224;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1959z00_3225;
BgL_aux1959z00_3225 = 
CAR(BgL_pairz00_29); 
if(
PAIRP(BgL_aux1959z00_3225))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3224 = BgL_aux1959z00_3225; }  else 
{ 
 obj_t BgL_auxz00_4051;
BgL_auxz00_4051 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2503z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1959z00_3225); 
FAILURE(BgL_auxz00_4051,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1961z00_3226;
BgL_aux1961z00_3226 = 
CAR(BgL_pairz00_3224); 
if(
PAIRP(BgL_aux1961z00_3226))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3223 = BgL_aux1961z00_3226; }  else 
{ 
 obj_t BgL_auxz00_4058;
BgL_auxz00_4058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2503z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1961z00_3226); 
FAILURE(BgL_auxz00_4058,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1963z00_3227;
BgL_aux1963z00_3227 = 
CDR(BgL_pairz00_3223); 
if(
PAIRP(BgL_aux1963z00_3227))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3222 = BgL_aux1963z00_3227; }  else 
{ 
 obj_t BgL_auxz00_4065;
BgL_auxz00_4065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2503z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1963z00_3227); 
FAILURE(BgL_auxz00_4065,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3222);} } 

}



/* &cadaar */
obj_t BGl_z62cadaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2346, obj_t BgL_pairz00_2347)
{
{ /* Ieee/pairlist.scm 368 */
{ /* Ieee/pairlist.scm 369 */
 obj_t BgL_auxz00_4070;
if(
PAIRP(BgL_pairz00_2347))
{ /* Ieee/pairlist.scm 369 */
BgL_auxz00_4070 = BgL_pairz00_2347
; }  else 
{ 
 obj_t BgL_auxz00_4073;
BgL_auxz00_4073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(15685L), BGl_string2504z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2347); 
FAILURE(BgL_auxz00_4073,BFALSE,BFALSE);} 
return 
BGl_cadaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4070);} } 

}



/* cdaaar */
BGL_EXPORTED_DEF obj_t BGl_cdaaarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_30)
{
{ /* Ieee/pairlist.scm 374 */
{ /* Ieee/pairlist.scm 375 */
 obj_t BgL_pairz00_3228;
{ /* Ieee/pairlist.scm 375 */
 obj_t BgL_pairz00_3229;
{ /* Ieee/pairlist.scm 375 */
 obj_t BgL_pairz00_3230;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1967z00_3231;
BgL_aux1967z00_3231 = 
CAR(BgL_pairz00_30); 
if(
PAIRP(BgL_aux1967z00_3231))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3230 = BgL_aux1967z00_3231; }  else 
{ 
 obj_t BgL_auxz00_4081;
BgL_auxz00_4081 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2505z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1967z00_3231); 
FAILURE(BgL_auxz00_4081,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1969z00_3232;
BgL_aux1969z00_3232 = 
CAR(BgL_pairz00_3230); 
if(
PAIRP(BgL_aux1969z00_3232))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3229 = BgL_aux1969z00_3232; }  else 
{ 
 obj_t BgL_auxz00_4088;
BgL_auxz00_4088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2505z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1969z00_3232); 
FAILURE(BgL_auxz00_4088,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1971z00_3233;
BgL_aux1971z00_3233 = 
CAR(BgL_pairz00_3229); 
if(
PAIRP(BgL_aux1971z00_3233))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3228 = BgL_aux1971z00_3233; }  else 
{ 
 obj_t BgL_auxz00_4095;
BgL_auxz00_4095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2505z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1971z00_3233); 
FAILURE(BgL_auxz00_4095,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3228);} } 

}



/* &cdaaar */
obj_t BGl_z62cdaaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2348, obj_t BgL_pairz00_2349)
{
{ /* Ieee/pairlist.scm 374 */
{ /* Ieee/pairlist.scm 375 */
 obj_t BgL_auxz00_4100;
if(
PAIRP(BgL_pairz00_2349))
{ /* Ieee/pairlist.scm 375 */
BgL_auxz00_4100 = BgL_pairz00_2349
; }  else 
{ 
 obj_t BgL_auxz00_4103;
BgL_auxz00_4103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(15970L), BGl_string2506z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2349); 
FAILURE(BgL_auxz00_4103,BFALSE,BFALSE);} 
return 
BGl_cdaaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4100);} } 

}



/* caaddr */
BGL_EXPORTED_DEF obj_t BGl_caaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_31)
{
{ /* Ieee/pairlist.scm 380 */
{ /* Ieee/pairlist.scm 381 */
 obj_t BgL_pairz00_3234;
{ /* Ieee/pairlist.scm 381 */
 obj_t BgL_pairz00_3235;
{ /* Ieee/pairlist.scm 381 */
 obj_t BgL_pairz00_3236;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1975z00_3237;
BgL_aux1975z00_3237 = 
CDR(BgL_pairz00_31); 
if(
PAIRP(BgL_aux1975z00_3237))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3236 = BgL_aux1975z00_3237; }  else 
{ 
 obj_t BgL_auxz00_4111;
BgL_auxz00_4111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2507z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1975z00_3237); 
FAILURE(BgL_auxz00_4111,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1977z00_3238;
BgL_aux1977z00_3238 = 
CDR(BgL_pairz00_3236); 
if(
PAIRP(BgL_aux1977z00_3238))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3235 = BgL_aux1977z00_3238; }  else 
{ 
 obj_t BgL_auxz00_4118;
BgL_auxz00_4118 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2507z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1977z00_3238); 
FAILURE(BgL_auxz00_4118,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1979z00_3239;
BgL_aux1979z00_3239 = 
CAR(BgL_pairz00_3235); 
if(
PAIRP(BgL_aux1979z00_3239))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3234 = BgL_aux1979z00_3239; }  else 
{ 
 obj_t BgL_auxz00_4125;
BgL_auxz00_4125 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2507z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1979z00_3239); 
FAILURE(BgL_auxz00_4125,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3234);} } 

}



/* &caaddr */
obj_t BGl_z62caaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2350, obj_t BgL_pairz00_2351)
{
{ /* Ieee/pairlist.scm 380 */
{ /* Ieee/pairlist.scm 381 */
 obj_t BgL_auxz00_4130;
if(
PAIRP(BgL_pairz00_2351))
{ /* Ieee/pairlist.scm 381 */
BgL_auxz00_4130 = BgL_pairz00_2351
; }  else 
{ 
 obj_t BgL_auxz00_4133;
BgL_auxz00_4133 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(16255L), BGl_string2508z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2351); 
FAILURE(BgL_auxz00_4133,BFALSE,BFALSE);} 
return 
BGl_caaddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4130);} } 

}



/* caddar */
BGL_EXPORTED_DEF obj_t BGl_caddarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_32)
{
{ /* Ieee/pairlist.scm 386 */
{ /* Ieee/pairlist.scm 387 */
 obj_t BgL_pairz00_3240;
{ /* Ieee/pairlist.scm 387 */
 obj_t BgL_pairz00_3241;
{ /* Ieee/pairlist.scm 387 */
 obj_t BgL_pairz00_3242;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1983z00_3243;
BgL_aux1983z00_3243 = 
CAR(BgL_pairz00_32); 
if(
PAIRP(BgL_aux1983z00_3243))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3242 = BgL_aux1983z00_3243; }  else 
{ 
 obj_t BgL_auxz00_4141;
BgL_auxz00_4141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2509z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1983z00_3243); 
FAILURE(BgL_auxz00_4141,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1985z00_3244;
BgL_aux1985z00_3244 = 
CDR(BgL_pairz00_3242); 
if(
PAIRP(BgL_aux1985z00_3244))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3241 = BgL_aux1985z00_3244; }  else 
{ 
 obj_t BgL_auxz00_4148;
BgL_auxz00_4148 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2509z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1985z00_3244); 
FAILURE(BgL_auxz00_4148,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1987z00_3245;
BgL_aux1987z00_3245 = 
CDR(BgL_pairz00_3241); 
if(
PAIRP(BgL_aux1987z00_3245))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3240 = BgL_aux1987z00_3245; }  else 
{ 
 obj_t BgL_auxz00_4155;
BgL_auxz00_4155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2509z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1987z00_3245); 
FAILURE(BgL_auxz00_4155,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3240);} } 

}



/* &caddar */
obj_t BGl_z62caddarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2352, obj_t BgL_pairz00_2353)
{
{ /* Ieee/pairlist.scm 386 */
{ /* Ieee/pairlist.scm 387 */
 obj_t BgL_auxz00_4160;
if(
PAIRP(BgL_pairz00_2353))
{ /* Ieee/pairlist.scm 387 */
BgL_auxz00_4160 = BgL_pairz00_2353
; }  else 
{ 
 obj_t BgL_auxz00_4163;
BgL_auxz00_4163 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(16540L), BGl_string2510z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2353); 
FAILURE(BgL_auxz00_4163,BFALSE,BFALSE);} 
return 
BGl_caddarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4160);} } 

}



/* cadadr */
BGL_EXPORTED_DEF obj_t BGl_cadadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_33)
{
{ /* Ieee/pairlist.scm 392 */
{ /* Ieee/pairlist.scm 393 */
 obj_t BgL_pairz00_3246;
{ /* Ieee/pairlist.scm 393 */
 obj_t BgL_pairz00_3247;
{ /* Ieee/pairlist.scm 393 */
 obj_t BgL_pairz00_3248;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1991z00_3249;
BgL_aux1991z00_3249 = 
CDR(BgL_pairz00_33); 
if(
PAIRP(BgL_aux1991z00_3249))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3248 = BgL_aux1991z00_3249; }  else 
{ 
 obj_t BgL_auxz00_4171;
BgL_auxz00_4171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2511z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1991z00_3249); 
FAILURE(BgL_auxz00_4171,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux1993z00_3250;
BgL_aux1993z00_3250 = 
CAR(BgL_pairz00_3248); 
if(
PAIRP(BgL_aux1993z00_3250))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3247 = BgL_aux1993z00_3250; }  else 
{ 
 obj_t BgL_auxz00_4178;
BgL_auxz00_4178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2511z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1993z00_3250); 
FAILURE(BgL_auxz00_4178,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1995z00_3251;
BgL_aux1995z00_3251 = 
CDR(BgL_pairz00_3247); 
if(
PAIRP(BgL_aux1995z00_3251))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3246 = BgL_aux1995z00_3251; }  else 
{ 
 obj_t BgL_auxz00_4185;
BgL_auxz00_4185 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2511z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1995z00_3251); 
FAILURE(BgL_auxz00_4185,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3246);} } 

}



/* &cadadr */
obj_t BGl_z62cadadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2354, obj_t BgL_pairz00_2355)
{
{ /* Ieee/pairlist.scm 392 */
{ /* Ieee/pairlist.scm 393 */
 obj_t BgL_auxz00_4190;
if(
PAIRP(BgL_pairz00_2355))
{ /* Ieee/pairlist.scm 393 */
BgL_auxz00_4190 = BgL_pairz00_2355
; }  else 
{ 
 obj_t BgL_auxz00_4193;
BgL_auxz00_4193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(16825L), BGl_string2512z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2355); 
FAILURE(BgL_auxz00_4193,BFALSE,BFALSE);} 
return 
BGl_cadadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4190);} } 

}



/* cadddr */
BGL_EXPORTED_DEF obj_t BGl_cadddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_34)
{
{ /* Ieee/pairlist.scm 398 */
{ /* Ieee/pairlist.scm 399 */
 obj_t BgL_pairz00_3252;
{ /* Ieee/pairlist.scm 399 */
 obj_t BgL_pairz00_3253;
{ /* Ieee/pairlist.scm 399 */
 obj_t BgL_pairz00_3254;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux1999z00_3255;
BgL_aux1999z00_3255 = 
CDR(BgL_pairz00_34); 
if(
PAIRP(BgL_aux1999z00_3255))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3254 = BgL_aux1999z00_3255; }  else 
{ 
 obj_t BgL_auxz00_4201;
BgL_auxz00_4201 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2513z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux1999z00_3255); 
FAILURE(BgL_auxz00_4201,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2001z00_3256;
BgL_aux2001z00_3256 = 
CDR(BgL_pairz00_3254); 
if(
PAIRP(BgL_aux2001z00_3256))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3253 = BgL_aux2001z00_3256; }  else 
{ 
 obj_t BgL_auxz00_4208;
BgL_auxz00_4208 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2513z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2001z00_3256); 
FAILURE(BgL_auxz00_4208,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2003z00_3257;
BgL_aux2003z00_3257 = 
CDR(BgL_pairz00_3253); 
if(
PAIRP(BgL_aux2003z00_3257))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3252 = BgL_aux2003z00_3257; }  else 
{ 
 obj_t BgL_auxz00_4215;
BgL_auxz00_4215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2513z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2003z00_3257); 
FAILURE(BgL_auxz00_4215,BFALSE,BFALSE);} } } 
return 
CAR(BgL_pairz00_3252);} } 

}



/* &cadddr */
obj_t BGl_z62cadddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2356, obj_t BgL_pairz00_2357)
{
{ /* Ieee/pairlist.scm 398 */
{ /* Ieee/pairlist.scm 399 */
 obj_t BgL_auxz00_4220;
if(
PAIRP(BgL_pairz00_2357))
{ /* Ieee/pairlist.scm 399 */
BgL_auxz00_4220 = BgL_pairz00_2357
; }  else 
{ 
 obj_t BgL_auxz00_4223;
BgL_auxz00_4223 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(17110L), BGl_string2514z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2357); 
FAILURE(BgL_auxz00_4223,BFALSE,BFALSE);} 
return 
BGl_cadddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4220);} } 

}



/* cdaadr */
BGL_EXPORTED_DEF obj_t BGl_cdaadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_35)
{
{ /* Ieee/pairlist.scm 404 */
{ /* Ieee/pairlist.scm 405 */
 obj_t BgL_pairz00_3258;
{ /* Ieee/pairlist.scm 405 */
 obj_t BgL_pairz00_3259;
{ /* Ieee/pairlist.scm 405 */
 obj_t BgL_pairz00_3260;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2007z00_3261;
BgL_aux2007z00_3261 = 
CDR(BgL_pairz00_35); 
if(
PAIRP(BgL_aux2007z00_3261))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3260 = BgL_aux2007z00_3261; }  else 
{ 
 obj_t BgL_auxz00_4231;
BgL_auxz00_4231 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2515z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2007z00_3261); 
FAILURE(BgL_auxz00_4231,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2009z00_3262;
BgL_aux2009z00_3262 = 
CAR(BgL_pairz00_3260); 
if(
PAIRP(BgL_aux2009z00_3262))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3259 = BgL_aux2009z00_3262; }  else 
{ 
 obj_t BgL_auxz00_4238;
BgL_auxz00_4238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2515z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2009z00_3262); 
FAILURE(BgL_auxz00_4238,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2011z00_3263;
BgL_aux2011z00_3263 = 
CAR(BgL_pairz00_3259); 
if(
PAIRP(BgL_aux2011z00_3263))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3258 = BgL_aux2011z00_3263; }  else 
{ 
 obj_t BgL_auxz00_4245;
BgL_auxz00_4245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2515z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2011z00_3263); 
FAILURE(BgL_auxz00_4245,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3258);} } 

}



/* &cdaadr */
obj_t BGl_z62cdaadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2358, obj_t BgL_pairz00_2359)
{
{ /* Ieee/pairlist.scm 404 */
{ /* Ieee/pairlist.scm 405 */
 obj_t BgL_auxz00_4250;
if(
PAIRP(BgL_pairz00_2359))
{ /* Ieee/pairlist.scm 405 */
BgL_auxz00_4250 = BgL_pairz00_2359
; }  else 
{ 
 obj_t BgL_auxz00_4253;
BgL_auxz00_4253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(17395L), BGl_string2516z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2359); 
FAILURE(BgL_auxz00_4253,BFALSE,BFALSE);} 
return 
BGl_cdaadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4250);} } 

}



/* cdaddr */
BGL_EXPORTED_DEF obj_t BGl_cdaddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_36)
{
{ /* Ieee/pairlist.scm 410 */
{ /* Ieee/pairlist.scm 411 */
 obj_t BgL_pairz00_3264;
{ /* Ieee/pairlist.scm 411 */
 obj_t BgL_pairz00_3265;
{ /* Ieee/pairlist.scm 411 */
 obj_t BgL_pairz00_3266;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2015z00_3267;
BgL_aux2015z00_3267 = 
CDR(BgL_pairz00_36); 
if(
PAIRP(BgL_aux2015z00_3267))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3266 = BgL_aux2015z00_3267; }  else 
{ 
 obj_t BgL_auxz00_4261;
BgL_auxz00_4261 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2517z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2015z00_3267); 
FAILURE(BgL_auxz00_4261,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2017z00_3268;
BgL_aux2017z00_3268 = 
CDR(BgL_pairz00_3266); 
if(
PAIRP(BgL_aux2017z00_3268))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3265 = BgL_aux2017z00_3268; }  else 
{ 
 obj_t BgL_auxz00_4268;
BgL_auxz00_4268 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2517z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2017z00_3268); 
FAILURE(BgL_auxz00_4268,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2019z00_3269;
BgL_aux2019z00_3269 = 
CAR(BgL_pairz00_3265); 
if(
PAIRP(BgL_aux2019z00_3269))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3264 = BgL_aux2019z00_3269; }  else 
{ 
 obj_t BgL_auxz00_4275;
BgL_auxz00_4275 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2517z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2019z00_3269); 
FAILURE(BgL_auxz00_4275,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3264);} } 

}



/* &cdaddr */
obj_t BGl_z62cdaddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2360, obj_t BgL_pairz00_2361)
{
{ /* Ieee/pairlist.scm 410 */
{ /* Ieee/pairlist.scm 411 */
 obj_t BgL_auxz00_4280;
if(
PAIRP(BgL_pairz00_2361))
{ /* Ieee/pairlist.scm 411 */
BgL_auxz00_4280 = BgL_pairz00_2361
; }  else 
{ 
 obj_t BgL_auxz00_4283;
BgL_auxz00_4283 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(17680L), BGl_string2518z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2361); 
FAILURE(BgL_auxz00_4283,BFALSE,BFALSE);} 
return 
BGl_cdaddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4280);} } 

}



/* cddaar */
BGL_EXPORTED_DEF obj_t BGl_cddaarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_37)
{
{ /* Ieee/pairlist.scm 416 */
{ /* Ieee/pairlist.scm 417 */
 obj_t BgL_pairz00_3270;
{ /* Ieee/pairlist.scm 417 */
 obj_t BgL_pairz00_3271;
{ /* Ieee/pairlist.scm 417 */
 obj_t BgL_pairz00_3272;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2023z00_3273;
BgL_aux2023z00_3273 = 
CAR(BgL_pairz00_37); 
if(
PAIRP(BgL_aux2023z00_3273))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3272 = BgL_aux2023z00_3273; }  else 
{ 
 obj_t BgL_auxz00_4291;
BgL_auxz00_4291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2519z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2023z00_3273); 
FAILURE(BgL_auxz00_4291,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2025z00_3274;
BgL_aux2025z00_3274 = 
CAR(BgL_pairz00_3272); 
if(
PAIRP(BgL_aux2025z00_3274))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3271 = BgL_aux2025z00_3274; }  else 
{ 
 obj_t BgL_auxz00_4298;
BgL_auxz00_4298 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2519z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2025z00_3274); 
FAILURE(BgL_auxz00_4298,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2027z00_3275;
BgL_aux2027z00_3275 = 
CDR(BgL_pairz00_3271); 
if(
PAIRP(BgL_aux2027z00_3275))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3270 = BgL_aux2027z00_3275; }  else 
{ 
 obj_t BgL_auxz00_4305;
BgL_auxz00_4305 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2519z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2027z00_3275); 
FAILURE(BgL_auxz00_4305,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3270);} } 

}



/* &cddaar */
obj_t BGl_z62cddaarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2362, obj_t BgL_pairz00_2363)
{
{ /* Ieee/pairlist.scm 416 */
{ /* Ieee/pairlist.scm 417 */
 obj_t BgL_auxz00_4310;
if(
PAIRP(BgL_pairz00_2363))
{ /* Ieee/pairlist.scm 417 */
BgL_auxz00_4310 = BgL_pairz00_2363
; }  else 
{ 
 obj_t BgL_auxz00_4313;
BgL_auxz00_4313 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(17965L), BGl_string2520z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2363); 
FAILURE(BgL_auxz00_4313,BFALSE,BFALSE);} 
return 
BGl_cddaarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4310);} } 

}



/* cddadr */
BGL_EXPORTED_DEF obj_t BGl_cddadrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_38)
{
{ /* Ieee/pairlist.scm 422 */
{ /* Ieee/pairlist.scm 423 */
 obj_t BgL_pairz00_3276;
{ /* Ieee/pairlist.scm 423 */
 obj_t BgL_pairz00_3277;
{ /* Ieee/pairlist.scm 423 */
 obj_t BgL_pairz00_3278;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2031z00_3279;
BgL_aux2031z00_3279 = 
CDR(BgL_pairz00_38); 
if(
PAIRP(BgL_aux2031z00_3279))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3278 = BgL_aux2031z00_3279; }  else 
{ 
 obj_t BgL_auxz00_4321;
BgL_auxz00_4321 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2521z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2031z00_3279); 
FAILURE(BgL_auxz00_4321,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2033z00_3280;
BgL_aux2033z00_3280 = 
CAR(BgL_pairz00_3278); 
if(
PAIRP(BgL_aux2033z00_3280))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3277 = BgL_aux2033z00_3280; }  else 
{ 
 obj_t BgL_auxz00_4328;
BgL_auxz00_4328 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2521z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2033z00_3280); 
FAILURE(BgL_auxz00_4328,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2035z00_3281;
BgL_aux2035z00_3281 = 
CDR(BgL_pairz00_3277); 
if(
PAIRP(BgL_aux2035z00_3281))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3276 = BgL_aux2035z00_3281; }  else 
{ 
 obj_t BgL_auxz00_4335;
BgL_auxz00_4335 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2521z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2035z00_3281); 
FAILURE(BgL_auxz00_4335,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3276);} } 

}



/* &cddadr */
obj_t BGl_z62cddadrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2364, obj_t BgL_pairz00_2365)
{
{ /* Ieee/pairlist.scm 422 */
{ /* Ieee/pairlist.scm 423 */
 obj_t BgL_auxz00_4340;
if(
PAIRP(BgL_pairz00_2365))
{ /* Ieee/pairlist.scm 423 */
BgL_auxz00_4340 = BgL_pairz00_2365
; }  else 
{ 
 obj_t BgL_auxz00_4343;
BgL_auxz00_4343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(18250L), BGl_string2522z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2365); 
FAILURE(BgL_auxz00_4343,BFALSE,BFALSE);} 
return 
BGl_cddadrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4340);} } 

}



/* cdadar */
BGL_EXPORTED_DEF obj_t BGl_cdadarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_39)
{
{ /* Ieee/pairlist.scm 428 */
{ /* Ieee/pairlist.scm 429 */
 obj_t BgL_pairz00_3282;
{ /* Ieee/pairlist.scm 429 */
 obj_t BgL_pairz00_3283;
{ /* Ieee/pairlist.scm 429 */
 obj_t BgL_pairz00_3284;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2039z00_3285;
BgL_aux2039z00_3285 = 
CAR(BgL_pairz00_39); 
if(
PAIRP(BgL_aux2039z00_3285))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3284 = BgL_aux2039z00_3285; }  else 
{ 
 obj_t BgL_auxz00_4351;
BgL_auxz00_4351 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2523z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2039z00_3285); 
FAILURE(BgL_auxz00_4351,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2041z00_3286;
BgL_aux2041z00_3286 = 
CDR(BgL_pairz00_3284); 
if(
PAIRP(BgL_aux2041z00_3286))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3283 = BgL_aux2041z00_3286; }  else 
{ 
 obj_t BgL_auxz00_4358;
BgL_auxz00_4358 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2523z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2041z00_3286); 
FAILURE(BgL_auxz00_4358,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2043z00_3287;
BgL_aux2043z00_3287 = 
CAR(BgL_pairz00_3283); 
if(
PAIRP(BgL_aux2043z00_3287))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3282 = BgL_aux2043z00_3287; }  else 
{ 
 obj_t BgL_auxz00_4365;
BgL_auxz00_4365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2523z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2043z00_3287); 
FAILURE(BgL_auxz00_4365,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3282);} } 

}



/* &cdadar */
obj_t BGl_z62cdadarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2366, obj_t BgL_pairz00_2367)
{
{ /* Ieee/pairlist.scm 428 */
{ /* Ieee/pairlist.scm 429 */
 obj_t BgL_auxz00_4370;
if(
PAIRP(BgL_pairz00_2367))
{ /* Ieee/pairlist.scm 429 */
BgL_auxz00_4370 = BgL_pairz00_2367
; }  else 
{ 
 obj_t BgL_auxz00_4373;
BgL_auxz00_4373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(18535L), BGl_string2524z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2367); 
FAILURE(BgL_auxz00_4373,BFALSE,BFALSE);} 
return 
BGl_cdadarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4370);} } 

}



/* cdddar */
BGL_EXPORTED_DEF obj_t BGl_cdddarz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_40)
{
{ /* Ieee/pairlist.scm 434 */
{ /* Ieee/pairlist.scm 435 */
 obj_t BgL_pairz00_3288;
{ /* Ieee/pairlist.scm 435 */
 obj_t BgL_pairz00_3289;
{ /* Ieee/pairlist.scm 435 */
 obj_t BgL_pairz00_3290;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2047z00_3291;
BgL_aux2047z00_3291 = 
CAR(BgL_pairz00_40); 
if(
PAIRP(BgL_aux2047z00_3291))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_3290 = BgL_aux2047z00_3291; }  else 
{ 
 obj_t BgL_auxz00_4381;
BgL_auxz00_4381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2525z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2047z00_3291); 
FAILURE(BgL_auxz00_4381,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2049z00_3292;
BgL_aux2049z00_3292 = 
CDR(BgL_pairz00_3290); 
if(
PAIRP(BgL_aux2049z00_3292))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3289 = BgL_aux2049z00_3292; }  else 
{ 
 obj_t BgL_auxz00_4388;
BgL_auxz00_4388 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2525z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2049z00_3292); 
FAILURE(BgL_auxz00_4388,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2051z00_3293;
BgL_aux2051z00_3293 = 
CDR(BgL_pairz00_3289); 
if(
PAIRP(BgL_aux2051z00_3293))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3288 = BgL_aux2051z00_3293; }  else 
{ 
 obj_t BgL_auxz00_4395;
BgL_auxz00_4395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2525z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2051z00_3293); 
FAILURE(BgL_auxz00_4395,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3288);} } 

}



/* &cdddar */
obj_t BGl_z62cdddarz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2368, obj_t BgL_pairz00_2369)
{
{ /* Ieee/pairlist.scm 434 */
{ /* Ieee/pairlist.scm 435 */
 obj_t BgL_auxz00_4400;
if(
PAIRP(BgL_pairz00_2369))
{ /* Ieee/pairlist.scm 435 */
BgL_auxz00_4400 = BgL_pairz00_2369
; }  else 
{ 
 obj_t BgL_auxz00_4403;
BgL_auxz00_4403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(18820L), BGl_string2526z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2369); 
FAILURE(BgL_auxz00_4403,BFALSE,BFALSE);} 
return 
BGl_cdddarz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4400);} } 

}



/* cddddr */
BGL_EXPORTED_DEF obj_t BGl_cddddrz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_41)
{
{ /* Ieee/pairlist.scm 441 */
{ /* Ieee/pairlist.scm 442 */
 obj_t BgL_pairz00_3294;
{ /* Ieee/pairlist.scm 442 */
 obj_t BgL_pairz00_3295;
{ /* Ieee/pairlist.scm 442 */
 obj_t BgL_pairz00_3296;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2055z00_3297;
BgL_aux2055z00_3297 = 
CDR(BgL_pairz00_41); 
if(
PAIRP(BgL_aux2055z00_3297))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3296 = BgL_aux2055z00_3297; }  else 
{ 
 obj_t BgL_auxz00_4411;
BgL_auxz00_4411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2527z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2055z00_3297); 
FAILURE(BgL_auxz00_4411,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2057z00_3298;
BgL_aux2057z00_3298 = 
CDR(BgL_pairz00_3296); 
if(
PAIRP(BgL_aux2057z00_3298))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3295 = BgL_aux2057z00_3298; }  else 
{ 
 obj_t BgL_auxz00_4418;
BgL_auxz00_4418 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2527z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2057z00_3298); 
FAILURE(BgL_auxz00_4418,BFALSE,BFALSE);} } } 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2059z00_3299;
BgL_aux2059z00_3299 = 
CDR(BgL_pairz00_3295); 
if(
PAIRP(BgL_aux2059z00_3299))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_3294 = BgL_aux2059z00_3299; }  else 
{ 
 obj_t BgL_auxz00_4425;
BgL_auxz00_4425 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2527z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2059z00_3299); 
FAILURE(BgL_auxz00_4425,BFALSE,BFALSE);} } } 
return 
CDR(BgL_pairz00_3294);} } 

}



/* &cddddr */
obj_t BGl_z62cddddrz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2370, obj_t BgL_pairz00_2371)
{
{ /* Ieee/pairlist.scm 441 */
{ /* Ieee/pairlist.scm 442 */
 obj_t BgL_auxz00_4430;
if(
PAIRP(BgL_pairz00_2371))
{ /* Ieee/pairlist.scm 442 */
BgL_auxz00_4430 = BgL_pairz00_2371
; }  else 
{ 
 obj_t BgL_auxz00_4433;
BgL_auxz00_4433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(19106L), BGl_string2528z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2371); 
FAILURE(BgL_auxz00_4433,BFALSE,BFALSE);} 
return 
BGl_cddddrz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4430);} } 

}



/* set-car! */
BGL_EXPORTED_DEF obj_t BGl_setzd2carz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_42, obj_t BgL_objz00_43)
{
{ /* Ieee/pairlist.scm 447 */
return 
SET_CAR(BgL_pairz00_42, BgL_objz00_43);} 

}



/* &set-car! */
obj_t BGl_z62setzd2carz12za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2372, obj_t BgL_pairz00_2373, obj_t BgL_objz00_2374)
{
{ /* Ieee/pairlist.scm 447 */
{ /* Ieee/pairlist.scm 448 */
 obj_t BgL_auxz00_4439;
if(
PAIRP(BgL_pairz00_2373))
{ /* Ieee/pairlist.scm 448 */
BgL_auxz00_4439 = BgL_pairz00_2373
; }  else 
{ 
 obj_t BgL_auxz00_4442;
BgL_auxz00_4442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(19382L), BGl_string2529z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2373); 
FAILURE(BgL_auxz00_4442,BFALSE,BFALSE);} 
return 
BGl_setzd2carz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4439, BgL_objz00_2374);} } 

}



/* set-cdr! */
BGL_EXPORTED_DEF obj_t BGl_setzd2cdrz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_pairz00_44, obj_t BgL_objz00_45)
{
{ /* Ieee/pairlist.scm 453 */
return 
SET_CDR(BgL_pairz00_44, BgL_objz00_45);} 

}



/* &set-cdr! */
obj_t BGl_z62setzd2cdrz12za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2375, obj_t BgL_pairz00_2376, obj_t BgL_objz00_2377)
{
{ /* Ieee/pairlist.scm 453 */
{ /* Ieee/pairlist.scm 454 */
 obj_t BgL_auxz00_4448;
if(
PAIRP(BgL_pairz00_2376))
{ /* Ieee/pairlist.scm 454 */
BgL_auxz00_4448 = BgL_pairz00_2376
; }  else 
{ 
 obj_t BgL_auxz00_4451;
BgL_auxz00_4451 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(19665L), BGl_string2530z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_pairz00_2376); 
FAILURE(BgL_auxz00_4451,BFALSE,BFALSE);} 
return 
BGl_setzd2cdrz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4448, BgL_objz00_2377);} } 

}



/* set-cer! */
BGL_EXPORTED_DEF obj_t BGl_setzd2cerz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_epairz00_46, obj_t BgL_objz00_47)
{
{ /* Ieee/pairlist.scm 459 */
return 
SET_CER(BgL_epairz00_46, BgL_objz00_47);} 

}



/* &set-cer! */
obj_t BGl_z62setzd2cerz12za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2378, obj_t BgL_epairz00_2379, obj_t BgL_objz00_2380)
{
{ /* Ieee/pairlist.scm 459 */
{ /* Ieee/pairlist.scm 460 */
 obj_t BgL_auxz00_4457;
if(
EPAIRP(BgL_epairz00_2379))
{ /* Ieee/pairlist.scm 460 */
BgL_auxz00_4457 = BgL_epairz00_2379
; }  else 
{ 
 obj_t BgL_auxz00_4460;
BgL_auxz00_4460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(19949L), BGl_string2531z00zz__r4_pairs_and_lists_6_3z00, BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_epairz00_2379); 
FAILURE(BgL_auxz00_4460,BFALSE,BFALSE);} 
return 
BGl_setzd2cerz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4457, BgL_objz00_2380);} } 

}



/* null? */
BGL_EXPORTED_DEF bool_t BGl_nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_48)
{
{ /* Ieee/pairlist.scm 465 */
return 
NULLP(BgL_objz00_48);} 

}



/* &null? */
obj_t BGl_z62nullzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2381, obj_t BgL_objz00_2382)
{
{ /* Ieee/pairlist.scm 465 */
return 
BBOOL(
BGl_nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2382));} 

}



/* list */
BGL_EXPORTED_DEF obj_t BGl_listz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_49)
{
{ /* Ieee/pairlist.scm 471 */
{ /* Ieee/pairlist.scm 471 */
 bool_t BgL_test3220z00_4468;
if(
PAIRP(BgL_lz00_49))
{ /* Ieee/pairlist.scm 241 */
BgL_test3220z00_4468 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3220z00_4468 = 
NULLP(BgL_lz00_49)
; } 
if(BgL_test3220z00_4468)
{ /* Ieee/pairlist.scm 471 */
return BgL_lz00_49;}  else 
{ 
 obj_t BgL_auxz00_4472;
BgL_auxz00_4472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(20463L), BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_49); 
FAILURE(BgL_auxz00_4472,BFALSE,BFALSE);} } } 

}



/* &list */
obj_t BGl_z62listz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2383, obj_t BgL_lz00_2384)
{
{ /* Ieee/pairlist.scm 471 */
return 
BGl_listz00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2384);} 

}



/* list? */
BGL_EXPORTED_DEF bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_50)
{
{ /* Ieee/pairlist.scm 477 */
{ 
 obj_t BgL_xz00_886; obj_t BgL_prevz00_887; obj_t BgL_xz00_883; obj_t BgL_prevz00_884;
if(
NULLP(BgL_xz00_50))
{ /* Ieee/pairlist.scm 494 */
return ((bool_t)1);}  else 
{ /* Ieee/pairlist.scm 494 */
if(
PAIRP(BgL_xz00_50))
{ /* Ieee/pairlist.scm 496 */
BgL_xz00_883 = 
CDR(BgL_xz00_50); 
BgL_prevz00_884 = BgL_xz00_50; 
BgL_l1z00_885:
if(
NULLP(BgL_xz00_883))
{ /* Ieee/pairlist.scm 479 */
return ((bool_t)1);}  else 
{ /* Ieee/pairlist.scm 479 */
if(
PAIRP(BgL_xz00_883))
{ /* Ieee/pairlist.scm 481 */
if(
(BgL_xz00_883==BgL_prevz00_884))
{ /* Ieee/pairlist.scm 482 */
return ((bool_t)0);}  else 
{ /* Ieee/pairlist.scm 482 */
BgL_xz00_886 = 
CDR(BgL_xz00_883); 
BgL_prevz00_887 = BgL_prevz00_884; 
if(
NULLP(BgL_xz00_886))
{ /* Ieee/pairlist.scm 487 */
return ((bool_t)1);}  else 
{ /* Ieee/pairlist.scm 487 */
if(
PAIRP(BgL_xz00_886))
{ /* Ieee/pairlist.scm 489 */
if(
(BgL_xz00_886==BgL_prevz00_887))
{ /* Ieee/pairlist.scm 490 */
return ((bool_t)0);}  else 
{ 
 obj_t BgL_prevz00_4495; obj_t BgL_xz00_4493;
BgL_xz00_4493 = 
CDR(BgL_xz00_886); 
{ /* Ieee/pairlist.scm 492 */
 obj_t BgL_pairz00_1814;
if(
PAIRP(BgL_prevz00_887))
{ /* Ieee/pairlist.scm 492 */
BgL_pairz00_1814 = BgL_prevz00_887; }  else 
{ 
 obj_t BgL_auxz00_4498;
BgL_auxz00_4498 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(21030L), BGl_string2534z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_887); 
FAILURE(BgL_auxz00_4498,BFALSE,BFALSE);} 
BgL_prevz00_4495 = 
CDR(BgL_pairz00_1814); } 
BgL_prevz00_884 = BgL_prevz00_4495; 
BgL_xz00_883 = BgL_xz00_4493; 
goto BgL_l1z00_885;} }  else 
{ /* Ieee/pairlist.scm 489 */
return ((bool_t)0);} } } }  else 
{ /* Ieee/pairlist.scm 481 */
return ((bool_t)0);} } }  else 
{ /* Ieee/pairlist.scm 496 */
return ((bool_t)0);} } } } 

}



/* &list? */
obj_t BGl_z62listzf3z91zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2385, obj_t BgL_xz00_2386)
{
{ /* Ieee/pairlist.scm 477 */
return 
BBOOL(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_2386));} 

}



/* append-2 */
BGL_EXPORTED_DEF obj_t bgl_append2(obj_t BgL_l1z00_51, obj_t BgL_l2z00_52)
{
{ /* Ieee/pairlist.scm 504 */
{ /* Ieee/pairlist.scm 505 */
 obj_t BgL_headz00_899;
BgL_headz00_899 = 
MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_52); 
{ 
 obj_t BgL_prevz00_900; obj_t BgL_tailz00_901;
BgL_prevz00_900 = BgL_headz00_899; 
BgL_tailz00_901 = BgL_l1z00_51; 
BgL_loopz00_902:
if(
NULLP(BgL_tailz00_901))
{ /* Ieee/pairlist.scm 507 */BNIL; }  else 
{ /* Ieee/pairlist.scm 509 */
 obj_t BgL_newzd2prevzd2_904;
{ /* Ieee/pairlist.scm 509 */
 obj_t BgL_arg1329z00_906;
{ /* Ieee/pairlist.scm 509 */
 obj_t BgL_pairz00_1816;
if(
PAIRP(BgL_tailz00_901))
{ /* Ieee/pairlist.scm 509 */
BgL_pairz00_1816 = BgL_tailz00_901; }  else 
{ 
 obj_t BgL_auxz00_4512;
BgL_auxz00_4512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(21530L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_901); 
FAILURE(BgL_auxz00_4512,BFALSE,BFALSE);} 
BgL_arg1329z00_906 = 
CAR(BgL_pairz00_1816); } 
BgL_newzd2prevzd2_904 = 
MAKE_YOUNG_PAIR(BgL_arg1329z00_906, BgL_l2z00_52); } 
SET_CDR(BgL_prevz00_900, BgL_newzd2prevzd2_904); 
{ /* Ieee/pairlist.scm 511 */
 obj_t BgL_arg1328z00_905;
{ /* Ieee/pairlist.scm 511 */
 obj_t BgL_pairz00_1818;
if(
PAIRP(BgL_tailz00_901))
{ /* Ieee/pairlist.scm 511 */
BgL_pairz00_1818 = BgL_tailz00_901; }  else 
{ 
 obj_t BgL_auxz00_4521;
BgL_auxz00_4521 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(21601L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_901); 
FAILURE(BgL_auxz00_4521,BFALSE,BFALSE);} 
BgL_arg1328z00_905 = 
CDR(BgL_pairz00_1818); } 
{ 
 obj_t BgL_tailz00_4527; obj_t BgL_prevz00_4526;
BgL_prevz00_4526 = BgL_newzd2prevzd2_904; 
BgL_tailz00_4527 = BgL_arg1328z00_905; 
BgL_tailz00_901 = BgL_tailz00_4527; 
BgL_prevz00_900 = BgL_prevz00_4526; 
goto BgL_loopz00_902;} } } 
return 
CDR(BgL_headz00_899);} } } 

}



/* &append-2 */
obj_t BGl_z62appendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2387, obj_t BgL_l1z00_2388, obj_t BgL_l2z00_2389)
{
{ /* Ieee/pairlist.scm 504 */
{ /* Ieee/pairlist.scm 505 */
 obj_t BgL_auxz00_4529;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_l1z00_2388))
{ /* Ieee/pairlist.scm 505 */
BgL_auxz00_4529 = BgL_l1z00_2388
; }  else 
{ 
 obj_t BgL_auxz00_4532;
BgL_auxz00_4532 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(21406L), BGl_string2536z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_l1z00_2388); 
FAILURE(BgL_auxz00_4532,BFALSE,BFALSE);} 
return 
bgl_append2(BgL_auxz00_4529, BgL_l2z00_2389);} } 

}



/* eappend-2 */
BGL_EXPORTED_DEF obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_l1z00_53, obj_t BgL_l2z00_54)
{
{ /* Ieee/pairlist.scm 518 */
{ /* Ieee/pairlist.scm 519 */
 obj_t BgL_headz00_907;
if(
EPAIRP(BgL_l2z00_54))
{ /* Ieee/pairlist.scm 520 */
 obj_t BgL_arg1337z00_919;
{ /* Ieee/pairlist.scm 520 */
 obj_t BgL_objz00_1820;
if(
EPAIRP(BgL_l2z00_54))
{ /* Ieee/pairlist.scm 520 */
BgL_objz00_1820 = BgL_l2z00_54; }  else 
{ 
 obj_t BgL_auxz00_4541;
BgL_auxz00_4541 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(21949L), BGl_string2537z00zz__r4_pairs_and_lists_6_3z00, BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_l2z00_54); 
FAILURE(BgL_auxz00_4541,BFALSE,BFALSE);} 
BgL_arg1337z00_919 = 
CER(BgL_objz00_1820); } 
{ /* Ieee/pairlist.scm 520 */
 obj_t BgL_res1859z00_1821;
BgL_res1859z00_1821 = 
MAKE_YOUNG_EPAIR(BNIL, BgL_l2z00_54, BgL_arg1337z00_919); 
BgL_headz00_907 = BgL_res1859z00_1821; } }  else 
{ /* Ieee/pairlist.scm 519 */
BgL_headz00_907 = 
MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_54); } 
{ 
 obj_t BgL_prevz00_908; obj_t BgL_tailz00_909;
BgL_prevz00_908 = BgL_headz00_907; 
BgL_tailz00_909 = BgL_l1z00_53; 
BgL_loopz00_910:
if(
NULLP(BgL_tailz00_909))
{ /* Ieee/pairlist.scm 523 */BNIL; }  else 
{ /* Ieee/pairlist.scm 525 */
 obj_t BgL_newzd2prevzd2_912;
if(
EPAIRP(BgL_tailz00_909))
{ /* Ieee/pairlist.scm 526 */
 obj_t BgL_arg1333z00_915; obj_t BgL_arg1334z00_916;
{ /* Ieee/pairlist.scm 526 */
 obj_t BgL_pairz00_1822;
if(
PAIRP(BgL_tailz00_909))
{ /* Ieee/pairlist.scm 526 */
BgL_pairz00_1822 = BgL_tailz00_909; }  else 
{ 
 obj_t BgL_auxz00_4554;
BgL_auxz00_4554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22101L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_909); 
FAILURE(BgL_auxz00_4554,BFALSE,BFALSE);} 
BgL_arg1333z00_915 = 
CAR(BgL_pairz00_1822); } 
{ /* Ieee/pairlist.scm 526 */
 obj_t BgL_objz00_1823;
if(
EPAIRP(BgL_tailz00_909))
{ /* Ieee/pairlist.scm 526 */
BgL_objz00_1823 = BgL_tailz00_909; }  else 
{ 
 obj_t BgL_auxz00_4561;
BgL_auxz00_4561 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22115L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_909); 
FAILURE(BgL_auxz00_4561,BFALSE,BFALSE);} 
BgL_arg1334z00_916 = 
CER(BgL_objz00_1823); } 
{ /* Ieee/pairlist.scm 526 */
 obj_t BgL_res1860z00_1824;
BgL_res1860z00_1824 = 
MAKE_YOUNG_EPAIR(BgL_arg1333z00_915, BgL_l2z00_54, BgL_arg1334z00_916); 
BgL_newzd2prevzd2_912 = BgL_res1860z00_1824; } }  else 
{ /* Ieee/pairlist.scm 527 */
 obj_t BgL_arg1335z00_917;
{ /* Ieee/pairlist.scm 527 */
 obj_t BgL_pairz00_1825;
if(
PAIRP(BgL_tailz00_909))
{ /* Ieee/pairlist.scm 527 */
BgL_pairz00_1825 = BgL_tailz00_909; }  else 
{ 
 obj_t BgL_auxz00_4569;
BgL_auxz00_4569 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22143L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_909); 
FAILURE(BgL_auxz00_4569,BFALSE,BFALSE);} 
BgL_arg1335z00_917 = 
CAR(BgL_pairz00_1825); } 
BgL_newzd2prevzd2_912 = 
MAKE_YOUNG_PAIR(BgL_arg1335z00_917, BgL_l2z00_54); } 
SET_CDR(BgL_prevz00_908, BgL_newzd2prevzd2_912); 
{ /* Ieee/pairlist.scm 529 */
 obj_t BgL_arg1331z00_913;
{ /* Ieee/pairlist.scm 529 */
 obj_t BgL_pairz00_1827;
if(
PAIRP(BgL_tailz00_909))
{ /* Ieee/pairlist.scm 529 */
BgL_pairz00_1827 = BgL_tailz00_909; }  else 
{ 
 obj_t BgL_auxz00_4578;
BgL_auxz00_4578 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22215L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_909); 
FAILURE(BgL_auxz00_4578,BFALSE,BFALSE);} 
BgL_arg1331z00_913 = 
CDR(BgL_pairz00_1827); } 
{ 
 obj_t BgL_tailz00_4584; obj_t BgL_prevz00_4583;
BgL_prevz00_4583 = BgL_newzd2prevzd2_912; 
BgL_tailz00_4584 = BgL_arg1331z00_913; 
BgL_tailz00_909 = BgL_tailz00_4584; 
BgL_prevz00_908 = BgL_prevz00_4583; 
goto BgL_loopz00_910;} } } 
return 
CDR(BgL_headz00_907);} } } 

}



/* &eappend-2 */
obj_t BGl_z62eappendzd22zb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2390, obj_t BgL_l1z00_2391, obj_t BgL_l2z00_2392)
{
{ /* Ieee/pairlist.scm 518 */
{ /* Ieee/pairlist.scm 519 */
 obj_t BgL_auxz00_4586;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_l1z00_2391))
{ /* Ieee/pairlist.scm 519 */
BgL_auxz00_4586 = BgL_l1z00_2391
; }  else 
{ 
 obj_t BgL_auxz00_4589;
BgL_auxz00_4589 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(21897L), BGl_string2538z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_l1z00_2391); 
FAILURE(BgL_auxz00_4589,BFALSE,BFALSE);} 
return 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4586, BgL_l2z00_2392);} } 

}



/* append */
BGL_EXPORTED_DEF obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_55)
{
{ /* Ieee/pairlist.scm 536 */
BGL_TAIL return 
BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(BgL_lz00_55);} 

}



/* append-list~2 */
obj_t BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_920)
{
{ /* Ieee/pairlist.scm 537 */
{ /* Ieee/pairlist.scm 538 */
 long BgL_lenz00_922;
{ /* Ieee/pairlist.scm 538 */
 obj_t BgL_auxz00_4595;
{ /* Ieee/pairlist.scm 538 */
 bool_t BgL_test3244z00_4596;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/pairlist.scm 241 */
BgL_test3244z00_4596 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3244z00_4596 = 
NULLP(BgL_lz00_920)
; } 
if(BgL_test3244z00_4596)
{ /* Ieee/pairlist.scm 538 */
BgL_auxz00_4595 = BgL_lz00_920
; }  else 
{ 
 obj_t BgL_auxz00_4600;
BgL_auxz00_4600 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22555L), BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_4600,BFALSE,BFALSE);} } 
BgL_lenz00_922 = 
bgl_list_length(BgL_auxz00_4595); } 
{ 

switch( BgL_lenz00_922) { case 0L : 

return BNIL;break;case 1L : 

{ /* Ieee/pairlist.scm 542 */
 obj_t BgL_pairz00_1831;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/pairlist.scm 542 */
BgL_pairz00_1831 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_4607;
BgL_auxz00_4607 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22624L), BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_4607,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_1831);} break;case 2L : 

{ /* Ieee/pairlist.scm 544 */
 obj_t BgL_arg1338z00_925; obj_t BgL_arg1339z00_926;
{ /* Ieee/pairlist.scm 544 */
 obj_t BgL_pairz00_1832;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/pairlist.scm 544 */
BgL_pairz00_1832 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_4614;
BgL_auxz00_4614 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22671L), BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_4614,BFALSE,BFALSE);} 
BgL_arg1338z00_925 = 
CAR(BgL_pairz00_1832); } 
{ /* Ieee/pairlist.scm 545 */
 obj_t BgL_pairz00_1834;
{ /* Ieee/pairlist.scm 545 */
 obj_t BgL_pairz00_1833;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/pairlist.scm 545 */
BgL_pairz00_1833 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_4621;
BgL_auxz00_4621 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22692L), BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_4621,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2105z00_2737;
BgL_aux2105z00_2737 = 
CDR(BgL_pairz00_1833); 
if(
PAIRP(BgL_aux2105z00_2737))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_1834 = BgL_aux2105z00_2737; }  else 
{ 
 obj_t BgL_auxz00_4628;
BgL_auxz00_4628 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2105z00_2737); 
FAILURE(BgL_auxz00_4628,BFALSE,BFALSE);} } } 
BgL_arg1339z00_926 = 
CAR(BgL_pairz00_1834); } 
{ /* Ieee/pairlist.scm 544 */
 obj_t BgL_auxz00_4633;
{ /* Ieee/pairlist.scm 544 */
 bool_t BgL_test3250z00_4634;
if(
PAIRP(BgL_arg1338z00_925))
{ /* Ieee/pairlist.scm 241 */
BgL_test3250z00_4634 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3250z00_4634 = 
NULLP(BgL_arg1338z00_925)
; } 
if(BgL_test3250z00_4634)
{ /* Ieee/pairlist.scm 544 */
BgL_auxz00_4633 = BgL_arg1338z00_925
; }  else 
{ 
 obj_t BgL_auxz00_4638;
BgL_auxz00_4638 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22656L), BGl_string2539z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1338z00_925); 
FAILURE(BgL_auxz00_4638,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4633, BgL_arg1339z00_926);} } break;
default: 
{ /* Ieee/pairlist.scm 546 */
 obj_t BgL_arg1341z00_928; obj_t BgL_arg1342z00_929;
{ /* Ieee/pairlist.scm 546 */
 obj_t BgL_pairz00_1829;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/pairlist.scm 546 */
BgL_pairz00_1829 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_4645;
BgL_auxz00_4645 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22717L), BGl_string2540z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_4645,BFALSE,BFALSE);} 
BgL_arg1341z00_928 = 
CAR(BgL_pairz00_1829); } 
{ /* Ieee/pairlist.scm 547 */
 obj_t BgL_arg1343z00_930;
{ /* Ieee/pairlist.scm 547 */
 obj_t BgL_pairz00_1830;
if(
PAIRP(BgL_lz00_920))
{ /* Ieee/pairlist.scm 547 */
BgL_pairz00_1830 = BgL_lz00_920; }  else 
{ 
 obj_t BgL_auxz00_4652;
BgL_auxz00_4652 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22746L), BGl_string2540z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_920); 
FAILURE(BgL_auxz00_4652,BFALSE,BFALSE);} 
BgL_arg1343z00_930 = 
CDR(BgL_pairz00_1830); } 
BgL_arg1342z00_929 = 
BGl_appendzd2listze72z35zz__r4_pairs_and_lists_6_3z00(BgL_arg1343z00_930); } 
{ /* Ieee/pairlist.scm 546 */
 obj_t BgL_auxz00_4658;
{ /* Ieee/pairlist.scm 546 */
 bool_t BgL_test3254z00_4659;
if(
PAIRP(BgL_arg1341z00_928))
{ /* Ieee/pairlist.scm 241 */
BgL_test3254z00_4659 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3254z00_4659 = 
NULLP(BgL_arg1341z00_928)
; } 
if(BgL_test3254z00_4659)
{ /* Ieee/pairlist.scm 546 */
BgL_auxz00_4658 = BgL_arg1341z00_928
; }  else 
{ 
 obj_t BgL_auxz00_4663;
BgL_auxz00_4663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(22702L), BGl_string2540z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1341z00_928); 
FAILURE(BgL_auxz00_4663,BFALSE,BFALSE);} } 
return 
BGl_appendzd221011zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4658, BgL_arg1342z00_929);} } } } } } 

}



/* &append */
obj_t BGl_z62appendz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2393, obj_t BgL_lz00_2394)
{
{ /* Ieee/pairlist.scm 536 */
return 
BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2394);} 

}



/* eappend */
BGL_EXPORTED_DEF obj_t BGl_eappendz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_56)
{
{ /* Ieee/pairlist.scm 553 */
BGL_TAIL return 
BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(BgL_lz00_56);} 

}



/* append-list~1 */
obj_t BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_931)
{
{ /* Ieee/pairlist.scm 554 */
{ /* Ieee/pairlist.scm 555 */
 long BgL_lenz00_933;
{ /* Ieee/pairlist.scm 555 */
 obj_t BgL_auxz00_4671;
{ /* Ieee/pairlist.scm 555 */
 bool_t BgL_test3256z00_4672;
if(
PAIRP(BgL_lz00_931))
{ /* Ieee/pairlist.scm 241 */
BgL_test3256z00_4672 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3256z00_4672 = 
NULLP(BgL_lz00_931)
; } 
if(BgL_test3256z00_4672)
{ /* Ieee/pairlist.scm 555 */
BgL_auxz00_4671 = BgL_lz00_931
; }  else 
{ 
 obj_t BgL_auxz00_4676;
BgL_auxz00_4676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23078L), BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_931); 
FAILURE(BgL_auxz00_4676,BFALSE,BFALSE);} } 
BgL_lenz00_933 = 
bgl_list_length(BgL_auxz00_4671); } 
{ 

switch( BgL_lenz00_933) { case 0L : 

return BNIL;break;case 1L : 

{ /* Ieee/pairlist.scm 559 */
 obj_t BgL_pairz00_1837;
if(
PAIRP(BgL_lz00_931))
{ /* Ieee/pairlist.scm 559 */
BgL_pairz00_1837 = BgL_lz00_931; }  else 
{ 
 obj_t BgL_auxz00_4683;
BgL_auxz00_4683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23147L), BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_931); 
FAILURE(BgL_auxz00_4683,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_1837);} break;case 2L : 

{ /* Ieee/pairlist.scm 561 */
 obj_t BgL_arg1344z00_936; obj_t BgL_arg1346z00_937;
{ /* Ieee/pairlist.scm 561 */
 obj_t BgL_pairz00_1838;
if(
PAIRP(BgL_lz00_931))
{ /* Ieee/pairlist.scm 561 */
BgL_pairz00_1838 = BgL_lz00_931; }  else 
{ 
 obj_t BgL_auxz00_4690;
BgL_auxz00_4690 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23195L), BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_931); 
FAILURE(BgL_auxz00_4690,BFALSE,BFALSE);} 
BgL_arg1344z00_936 = 
CAR(BgL_pairz00_1838); } 
{ /* Ieee/pairlist.scm 562 */
 obj_t BgL_pairz00_1840;
{ /* Ieee/pairlist.scm 562 */
 obj_t BgL_pairz00_1839;
if(
PAIRP(BgL_lz00_931))
{ /* Ieee/pairlist.scm 562 */
BgL_pairz00_1839 = BgL_lz00_931; }  else 
{ 
 obj_t BgL_auxz00_4697;
BgL_auxz00_4697 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23217L), BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_931); 
FAILURE(BgL_auxz00_4697,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2123z00_2755;
BgL_aux2123z00_2755 = 
CDR(BgL_pairz00_1839); 
if(
PAIRP(BgL_aux2123z00_2755))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_1840 = BgL_aux2123z00_2755; }  else 
{ 
 obj_t BgL_auxz00_4704;
BgL_auxz00_4704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2123z00_2755); 
FAILURE(BgL_auxz00_4704,BFALSE,BFALSE);} } } 
BgL_arg1346z00_937 = 
CAR(BgL_pairz00_1840); } 
{ /* Ieee/pairlist.scm 561 */
 obj_t BgL_auxz00_4709;
{ /* Ieee/pairlist.scm 561 */
 bool_t BgL_test3262z00_4710;
if(
PAIRP(BgL_arg1344z00_936))
{ /* Ieee/pairlist.scm 241 */
BgL_test3262z00_4710 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3262z00_4710 = 
NULLP(BgL_arg1344z00_936)
; } 
if(BgL_test3262z00_4710)
{ /* Ieee/pairlist.scm 561 */
BgL_auxz00_4709 = BgL_arg1344z00_936
; }  else 
{ 
 obj_t BgL_auxz00_4714;
BgL_auxz00_4714 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23196L), BGl_string2541z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1344z00_936); 
FAILURE(BgL_auxz00_4714,BFALSE,BFALSE);} } 
return 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4709, BgL_arg1346z00_937);} } break;
default: 
{ /* Ieee/pairlist.scm 563 */
 obj_t BgL_arg1348z00_939; obj_t BgL_arg1349z00_940;
{ /* Ieee/pairlist.scm 563 */
 obj_t BgL_pairz00_1835;
if(
PAIRP(BgL_lz00_931))
{ /* Ieee/pairlist.scm 563 */
BgL_pairz00_1835 = BgL_lz00_931; }  else 
{ 
 obj_t BgL_auxz00_4721;
BgL_auxz00_4721 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23243L), BGl_string2542z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_931); 
FAILURE(BgL_auxz00_4721,BFALSE,BFALSE);} 
BgL_arg1348z00_939 = 
CAR(BgL_pairz00_1835); } 
{ /* Ieee/pairlist.scm 564 */
 obj_t BgL_auxz00_4726;
{ /* Ieee/pairlist.scm 564 */
 obj_t BgL_pairz00_1836;
if(
PAIRP(BgL_lz00_931))
{ /* Ieee/pairlist.scm 564 */
BgL_pairz00_1836 = BgL_lz00_931; }  else 
{ 
 obj_t BgL_auxz00_4729;
BgL_auxz00_4729 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23273L), BGl_string2542z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_931); 
FAILURE(BgL_auxz00_4729,BFALSE,BFALSE);} 
BgL_auxz00_4726 = 
CDR(BgL_pairz00_1836); } 
BgL_arg1349z00_940 = 
BGl_appendzd2listze71z35zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4726); } 
{ /* Ieee/pairlist.scm 563 */
 obj_t BgL_auxz00_4735;
{ /* Ieee/pairlist.scm 563 */
 bool_t BgL_test3266z00_4736;
if(
PAIRP(BgL_arg1348z00_939))
{ /* Ieee/pairlist.scm 241 */
BgL_test3266z00_4736 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3266z00_4736 = 
NULLP(BgL_arg1348z00_939)
; } 
if(BgL_test3266z00_4736)
{ /* Ieee/pairlist.scm 563 */
BgL_auxz00_4735 = BgL_arg1348z00_939
; }  else 
{ 
 obj_t BgL_auxz00_4740;
BgL_auxz00_4740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23244L), BGl_string2542z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1348z00_939); 
FAILURE(BgL_auxz00_4740,BFALSE,BFALSE);} } 
return 
BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4735, BgL_arg1349z00_940);} } } } } } 

}



/* &eappend */
obj_t BGl_z62eappendz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2395, obj_t BgL_lz00_2396)
{
{ /* Ieee/pairlist.scm 553 */
return 
BGl_eappendz00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2396);} 

}



/* append! */
BGL_EXPORTED_DEF obj_t BGl_appendz12z12zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_57)
{
{ /* Ieee/pairlist.scm 570 */
{ /* Ieee/pairlist.scm 582 */
 obj_t BgL_aux2127z00_2759;
BgL_aux2127z00_2759 = 
BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(BgL_lz00_57); 
{ /* Ieee/pairlist.scm 582 */
 bool_t BgL_test3268z00_4748;
if(
PAIRP(BgL_aux2127z00_2759))
{ /* Ieee/pairlist.scm 241 */
BgL_test3268z00_4748 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3268z00_4748 = 
NULLP(BgL_aux2127z00_2759)
; } 
if(BgL_test3268z00_4748)
{ /* Ieee/pairlist.scm 582 */
return BgL_aux2127z00_2759;}  else 
{ 
 obj_t BgL_auxz00_4752;
BgL_auxz00_4752 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23817L), BGl_string2543z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2127z00_2759); 
FAILURE(BgL_auxz00_4752,BFALSE,BFALSE);} } } } 

}



/* append-list~0 */
obj_t BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_942)
{
{ /* Ieee/pairlist.scm 571 */
{ /* Ieee/pairlist.scm 572 */
 long BgL_lenz00_944;
{ /* Ieee/pairlist.scm 572 */
 obj_t BgL_auxz00_4756;
{ /* Ieee/pairlist.scm 572 */
 bool_t BgL_test3270z00_4757;
if(
PAIRP(BgL_lz00_942))
{ /* Ieee/pairlist.scm 241 */
BgL_test3270z00_4757 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3270z00_4757 = 
NULLP(BgL_lz00_942)
; } 
if(BgL_test3270z00_4757)
{ /* Ieee/pairlist.scm 572 */
BgL_auxz00_4756 = BgL_lz00_942
; }  else 
{ 
 obj_t BgL_auxz00_4761;
BgL_auxz00_4761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23605L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_942); 
FAILURE(BgL_auxz00_4761,BFALSE,BFALSE);} } 
BgL_lenz00_944 = 
bgl_list_length(BgL_auxz00_4756); } 
{ 

switch( BgL_lenz00_944) { case 0L : 

return BNIL;break;case 1L : 

{ /* Ieee/pairlist.scm 576 */
 obj_t BgL_pairz00_1843;
if(
PAIRP(BgL_lz00_942))
{ /* Ieee/pairlist.scm 576 */
BgL_pairz00_1843 = BgL_lz00_942; }  else 
{ 
 obj_t BgL_auxz00_4768;
BgL_auxz00_4768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23674L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_942); 
FAILURE(BgL_auxz00_4768,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_1843);} break;case 2L : 

{ /* Ieee/pairlist.scm 578 */
 obj_t BgL_arg1351z00_947; obj_t BgL_arg1352z00_948;
{ /* Ieee/pairlist.scm 578 */
 obj_t BgL_pairz00_1844;
if(
PAIRP(BgL_lz00_942))
{ /* Ieee/pairlist.scm 578 */
BgL_pairz00_1844 = BgL_lz00_942; }  else 
{ 
 obj_t BgL_auxz00_4775;
BgL_auxz00_4775 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23722L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_942); 
FAILURE(BgL_auxz00_4775,BFALSE,BFALSE);} 
BgL_arg1351z00_947 = 
CAR(BgL_pairz00_1844); } 
{ /* Ieee/pairlist.scm 579 */
 obj_t BgL_pairz00_1846;
{ /* Ieee/pairlist.scm 579 */
 obj_t BgL_pairz00_1845;
if(
PAIRP(BgL_lz00_942))
{ /* Ieee/pairlist.scm 579 */
BgL_pairz00_1845 = BgL_lz00_942; }  else 
{ 
 obj_t BgL_auxz00_4782;
BgL_auxz00_4782 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23744L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_942); 
FAILURE(BgL_auxz00_4782,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2145z00_2777;
BgL_aux2145z00_2777 = 
CDR(BgL_pairz00_1845); 
if(
PAIRP(BgL_aux2145z00_2777))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_1846 = BgL_aux2145z00_2777; }  else 
{ 
 obj_t BgL_auxz00_4789;
BgL_auxz00_4789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2145z00_2777); 
FAILURE(BgL_auxz00_4789,BFALSE,BFALSE);} } } 
BgL_arg1352z00_948 = 
CAR(BgL_pairz00_1846); } 
{ /* Ieee/pairlist.scm 578 */
 obj_t BgL_auxz00_4803; obj_t BgL_auxz00_4794;
{ /* Ieee/pairlist.scm 579 */
 bool_t BgL_test3278z00_4804;
if(
PAIRP(BgL_arg1352z00_948))
{ /* Ieee/pairlist.scm 241 */
BgL_test3278z00_4804 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3278z00_4804 = 
NULLP(BgL_arg1352z00_948)
; } 
if(BgL_test3278z00_4804)
{ /* Ieee/pairlist.scm 579 */
BgL_auxz00_4803 = BgL_arg1352z00_948
; }  else 
{ 
 obj_t BgL_auxz00_4808;
BgL_auxz00_4808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23746L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1352z00_948); 
FAILURE(BgL_auxz00_4808,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 578 */
 bool_t BgL_test3276z00_4795;
if(
PAIRP(BgL_arg1351z00_947))
{ /* Ieee/pairlist.scm 241 */
BgL_test3276z00_4795 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3276z00_4795 = 
NULLP(BgL_arg1351z00_947)
; } 
if(BgL_test3276z00_4795)
{ /* Ieee/pairlist.scm 578 */
BgL_auxz00_4794 = BgL_arg1351z00_947
; }  else 
{ 
 obj_t BgL_auxz00_4799;
BgL_auxz00_4799 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23723L), BGl_string2544z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1351z00_947); 
FAILURE(BgL_auxz00_4799,BFALSE,BFALSE);} } 
return 
BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4794, BgL_auxz00_4803);} } break;
default: 
{ /* Ieee/pairlist.scm 580 */
 obj_t BgL_arg1356z00_950; obj_t BgL_arg1357z00_951;
{ /* Ieee/pairlist.scm 580 */
 obj_t BgL_pairz00_1841;
if(
PAIRP(BgL_lz00_942))
{ /* Ieee/pairlist.scm 580 */
BgL_pairz00_1841 = BgL_lz00_942; }  else 
{ 
 obj_t BgL_auxz00_4815;
BgL_auxz00_4815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23770L), BGl_string2545z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_942); 
FAILURE(BgL_auxz00_4815,BFALSE,BFALSE);} 
BgL_arg1356z00_950 = 
CAR(BgL_pairz00_1841); } 
{ /* Ieee/pairlist.scm 581 */
 obj_t BgL_arg1358z00_952;
{ /* Ieee/pairlist.scm 581 */
 obj_t BgL_pairz00_1842;
if(
PAIRP(BgL_lz00_942))
{ /* Ieee/pairlist.scm 581 */
BgL_pairz00_1842 = BgL_lz00_942; }  else 
{ 
 obj_t BgL_auxz00_4822;
BgL_auxz00_4822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23800L), BGl_string2545z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_942); 
FAILURE(BgL_auxz00_4822,BFALSE,BFALSE);} 
BgL_arg1358z00_952 = 
CDR(BgL_pairz00_1842); } 
BgL_arg1357z00_951 = 
BGl_appendzd2listze70z35zz__r4_pairs_and_lists_6_3z00(BgL_arg1358z00_952); } 
{ /* Ieee/pairlist.scm 580 */
 obj_t BgL_auxz00_4837; obj_t BgL_auxz00_4828;
{ /* Ieee/pairlist.scm 581 */
 bool_t BgL_test3284z00_4838;
if(
PAIRP(BgL_arg1357z00_951))
{ /* Ieee/pairlist.scm 241 */
BgL_test3284z00_4838 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3284z00_4838 = 
NULLP(BgL_arg1357z00_951)
; } 
if(BgL_test3284z00_4838)
{ /* Ieee/pairlist.scm 581 */
BgL_auxz00_4837 = BgL_arg1357z00_951
; }  else 
{ 
 obj_t BgL_auxz00_4842;
BgL_auxz00_4842 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23802L), BGl_string2545z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1357z00_951); 
FAILURE(BgL_auxz00_4842,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 580 */
 bool_t BgL_test3282z00_4829;
if(
PAIRP(BgL_arg1356z00_950))
{ /* Ieee/pairlist.scm 241 */
BgL_test3282z00_4829 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3282z00_4829 = 
NULLP(BgL_arg1356z00_950)
; } 
if(BgL_test3282z00_4829)
{ /* Ieee/pairlist.scm 580 */
BgL_auxz00_4828 = BgL_arg1356z00_950
; }  else 
{ 
 obj_t BgL_auxz00_4833;
BgL_auxz00_4833 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(23771L), BGl_string2545z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1356z00_950); 
FAILURE(BgL_auxz00_4833,BFALSE,BFALSE);} } 
return 
BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4828, BgL_auxz00_4837);} } } } } } 

}



/* &append! */
obj_t BGl_z62appendz12z70zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2397, obj_t BgL_lz00_2398)
{
{ /* Ieee/pairlist.scm 570 */
return 
BGl_appendz12z12zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2398);} 

}



/* append-2! */
BGL_EXPORTED_DEF obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_58, obj_t BgL_yz00_59)
{
{ /* Ieee/pairlist.scm 587 */
if(
NULLP(BgL_xz00_58))
{ /* Ieee/pairlist.scm 588 */
return BgL_yz00_59;}  else 
{ /* Ieee/pairlist.scm 591 */
 obj_t BgL_arg1360z00_955;
BgL_arg1360z00_955 = 
CDR(BgL_xz00_58); 
{ 
 obj_t BgL_az00_1857; obj_t BgL_bz00_1858;
BgL_az00_1857 = BgL_xz00_58; 
BgL_bz00_1858 = BgL_arg1360z00_955; 
BgL_dozd2loopzd2zd21018zd2_1856:
if(
NULLP(BgL_bz00_1858))
{ /* Ieee/pairlist.scm 590 */
{ /* Ieee/pairlist.scm 593 */
 obj_t BgL_pairz00_1863;
if(
PAIRP(BgL_az00_1857))
{ /* Ieee/pairlist.scm 593 */
BgL_pairz00_1863 = BgL_az00_1857; }  else 
{ 
 obj_t BgL_auxz00_4856;
BgL_auxz00_4856 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24199L), BGl_string2546z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_az00_1857); 
FAILURE(BgL_auxz00_4856,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_1863, BgL_yz00_59); } 
return BgL_xz00_58;}  else 
{ /* Ieee/pairlist.scm 591 */
 obj_t BgL_arg1363z00_1862;
{ /* Ieee/pairlist.scm 591 */
 obj_t BgL_pairz00_1864;
if(
PAIRP(BgL_bz00_1858))
{ /* Ieee/pairlist.scm 591 */
BgL_pairz00_1864 = BgL_bz00_1858; }  else 
{ 
 obj_t BgL_auxz00_4863;
BgL_auxz00_4863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24152L), BGl_string2546z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_bz00_1858); 
FAILURE(BgL_auxz00_4863,BFALSE,BFALSE);} 
BgL_arg1363z00_1862 = 
CDR(BgL_pairz00_1864); } 
{ 
 obj_t BgL_bz00_4869; obj_t BgL_az00_4868;
BgL_az00_4868 = BgL_bz00_1858; 
BgL_bz00_4869 = BgL_arg1363z00_1862; 
BgL_bz00_1858 = BgL_bz00_4869; 
BgL_az00_1857 = BgL_az00_4868; 
goto BgL_dozd2loopzd2zd21018zd2_1856;} } } } } 

}



/* &append-2! */
obj_t BGl_z62appendzd22z12za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2399, obj_t BgL_xz00_2400, obj_t BgL_yz00_2401)
{
{ /* Ieee/pairlist.scm 587 */
{ /* Ieee/pairlist.scm 588 */
 obj_t BgL_auxz00_4877; obj_t BgL_auxz00_4870;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_yz00_2401))
{ /* Ieee/pairlist.scm 588 */
BgL_auxz00_4877 = BgL_yz00_2401
; }  else 
{ 
 obj_t BgL_auxz00_4880;
BgL_auxz00_4880 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24084L), BGl_string2547z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_2401); 
FAILURE(BgL_auxz00_4880,BFALSE,BFALSE);} 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_2400))
{ /* Ieee/pairlist.scm 588 */
BgL_auxz00_4870 = BgL_xz00_2400
; }  else 
{ 
 obj_t BgL_auxz00_4873;
BgL_auxz00_4873 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24084L), BGl_string2547z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_2400); 
FAILURE(BgL_auxz00_4873,BFALSE,BFALSE);} 
return 
BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4870, BgL_auxz00_4877);} } 

}



/* length */
BGL_EXPORTED_DEF long bgl_list_length(obj_t BgL_listz00_60)
{
{ /* Ieee/pairlist.scm 599 */
{ 
 obj_t BgL_lz00_1877; long BgL_resz00_1878;
BgL_lz00_1877 = BgL_listz00_60; 
BgL_resz00_1878 = 0L; 
BgL_loopz00_1876:
if(
NULLP(BgL_lz00_1877))
{ /* Ieee/pairlist.scm 603 */
return BgL_resz00_1878;}  else 
{ 
 long BgL_resz00_4895; obj_t BgL_lz00_4887;
{ /* Ieee/pairlist.scm 606 */
 obj_t BgL_pairz00_1885;
if(
PAIRP(BgL_lz00_1877))
{ /* Ieee/pairlist.scm 606 */
BgL_pairz00_1885 = BgL_lz00_1877; }  else 
{ 
 obj_t BgL_auxz00_4890;
BgL_auxz00_4890 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24572L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1877); 
FAILURE(BgL_auxz00_4890,BFALSE,BFALSE);} 
BgL_lz00_4887 = 
CDR(BgL_pairz00_1885); } 
BgL_resz00_4895 = 
(1L+BgL_resz00_1878); 
BgL_resz00_1878 = BgL_resz00_4895; 
BgL_lz00_1877 = BgL_lz00_4887; 
goto BgL_loopz00_1876;} } } 

}



/* &length */
obj_t BGl_z62lengthz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2402, obj_t BgL_listz00_2403)
{
{ /* Ieee/pairlist.scm 599 */
{ /* Ieee/pairlist.scm 600 */
 long BgL_tmpz00_4897;
{ /* Ieee/pairlist.scm 600 */
 obj_t BgL_auxz00_4898;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2403))
{ /* Ieee/pairlist.scm 600 */
BgL_auxz00_4898 = BgL_listz00_2403
; }  else 
{ 
 obj_t BgL_auxz00_4901;
BgL_auxz00_4901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24469L), BGl_string2548z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2403); 
FAILURE(BgL_auxz00_4901,BFALSE,BFALSE);} 
BgL_tmpz00_4897 = 
bgl_list_length(BgL_auxz00_4898); } 
return 
BINT(BgL_tmpz00_4897);} } 

}



/* reverse */
BGL_EXPORTED_DEF obj_t bgl_reverse(obj_t BgL_lz00_61)
{
{ /* Ieee/pairlist.scm 611 */
{ 
 obj_t BgL_lz00_1900; obj_t BgL_accz00_1901;
BgL_lz00_1900 = BgL_lz00_61; 
BgL_accz00_1901 = BNIL; 
BgL_loopz00_1899:
if(
NULLP(BgL_lz00_1900))
{ /* Ieee/pairlist.scm 614 */
return BgL_accz00_1901;}  else 
{ /* Ieee/pairlist.scm 616 */
 obj_t BgL_arg1370z00_1907; obj_t BgL_arg1371z00_1908;
{ /* Ieee/pairlist.scm 616 */
 obj_t BgL_pairz00_1910;
if(
PAIRP(BgL_lz00_1900))
{ /* Ieee/pairlist.scm 616 */
BgL_pairz00_1910 = BgL_lz00_1900; }  else 
{ 
 obj_t BgL_auxz00_4911;
BgL_auxz00_4911 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24917L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1900); 
FAILURE(BgL_auxz00_4911,BFALSE,BFALSE);} 
BgL_arg1370z00_1907 = 
CDR(BgL_pairz00_1910); } 
{ /* Ieee/pairlist.scm 616 */
 obj_t BgL_arg1372z00_1909;
{ /* Ieee/pairlist.scm 616 */
 obj_t BgL_pairz00_1911;
if(
PAIRP(BgL_lz00_1900))
{ /* Ieee/pairlist.scm 616 */
BgL_pairz00_1911 = BgL_lz00_1900; }  else 
{ 
 obj_t BgL_auxz00_4918;
BgL_auxz00_4918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24931L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1900); 
FAILURE(BgL_auxz00_4918,BFALSE,BFALSE);} 
BgL_arg1372z00_1909 = 
CAR(BgL_pairz00_1911); } 
BgL_arg1371z00_1908 = 
MAKE_YOUNG_PAIR(BgL_arg1372z00_1909, BgL_accz00_1901); } 
{ 
 obj_t BgL_accz00_4925; obj_t BgL_lz00_4924;
BgL_lz00_4924 = BgL_arg1370z00_1907; 
BgL_accz00_4925 = BgL_arg1371z00_1908; 
BgL_accz00_1901 = BgL_accz00_4925; 
BgL_lz00_1900 = BgL_lz00_4924; 
goto BgL_loopz00_1899;} } } } 

}



/* &reverse */
obj_t BGl_z62reversez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2404, obj_t BgL_lz00_2405)
{
{ /* Ieee/pairlist.scm 611 */
{ /* Ieee/pairlist.scm 612 */
 obj_t BgL_auxz00_4926;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2405))
{ /* Ieee/pairlist.scm 612 */
BgL_auxz00_4926 = BgL_lz00_2405
; }  else 
{ 
 obj_t BgL_auxz00_4929;
BgL_auxz00_4929 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(24839L), BGl_string2549z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2405); 
FAILURE(BgL_auxz00_4929,BFALSE,BFALSE);} 
return 
bgl_reverse(BgL_auxz00_4926);} } 

}



/* ereverse */
BGL_EXPORTED_DEF obj_t BGl_ereversez00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_62)
{
{ /* Ieee/pairlist.scm 621 */
{ 
 obj_t BgL_lz00_982; obj_t BgL_accz00_983;
BgL_lz00_982 = BgL_lz00_62; 
BgL_accz00_983 = BNIL; 
BgL_zc3z04anonymousza31373ze3z87_984:
if(
NULLP(BgL_lz00_982))
{ /* Ieee/pairlist.scm 624 */
return BgL_accz00_983;}  else 
{ /* Ieee/pairlist.scm 626 */
 obj_t BgL_arg1375z00_986; obj_t BgL_arg1376z00_987;
{ /* Ieee/pairlist.scm 626 */
 obj_t BgL_pairz00_1912;
if(
PAIRP(BgL_lz00_982))
{ /* Ieee/pairlist.scm 626 */
BgL_pairz00_1912 = BgL_lz00_982; }  else 
{ 
 obj_t BgL_auxz00_4938;
BgL_auxz00_4938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25268L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_982); 
FAILURE(BgL_auxz00_4938,BFALSE,BFALSE);} 
BgL_arg1375z00_986 = 
CDR(BgL_pairz00_1912); } 
if(
EPAIRP(BgL_lz00_982))
{ /* Ieee/pairlist.scm 628 */
 obj_t BgL_arg1378z00_989; obj_t BgL_arg1379z00_990;
{ /* Ieee/pairlist.scm 628 */
 obj_t BgL_pairz00_1913;
if(
PAIRP(BgL_lz00_982))
{ /* Ieee/pairlist.scm 628 */
BgL_pairz00_1913 = BgL_lz00_982; }  else 
{ 
 obj_t BgL_auxz00_4947;
BgL_auxz00_4947 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25306L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_982); 
FAILURE(BgL_auxz00_4947,BFALSE,BFALSE);} 
BgL_arg1378z00_989 = 
CAR(BgL_pairz00_1913); } 
{ /* Ieee/pairlist.scm 628 */
 obj_t BgL_objz00_1914;
if(
EPAIRP(BgL_lz00_982))
{ /* Ieee/pairlist.scm 628 */
BgL_objz00_1914 = BgL_lz00_982; }  else 
{ 
 obj_t BgL_auxz00_4954;
BgL_auxz00_4954 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25318L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_982); 
FAILURE(BgL_auxz00_4954,BFALSE,BFALSE);} 
BgL_arg1379z00_990 = 
CER(BgL_objz00_1914); } 
{ /* Ieee/pairlist.scm 628 */
 obj_t BgL_res1861z00_1915;
BgL_res1861z00_1915 = 
MAKE_YOUNG_EPAIR(BgL_arg1378z00_989, BgL_accz00_983, BgL_arg1379z00_990); 
BgL_arg1376z00_987 = BgL_res1861z00_1915; } }  else 
{ /* Ieee/pairlist.scm 629 */
 obj_t BgL_arg1380z00_991;
{ /* Ieee/pairlist.scm 629 */
 obj_t BgL_pairz00_1916;
if(
PAIRP(BgL_lz00_982))
{ /* Ieee/pairlist.scm 629 */
BgL_pairz00_1916 = BgL_lz00_982; }  else 
{ 
 obj_t BgL_auxz00_4962;
BgL_auxz00_4962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25339L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_982); 
FAILURE(BgL_auxz00_4962,BFALSE,BFALSE);} 
BgL_arg1380z00_991 = 
CAR(BgL_pairz00_1916); } 
BgL_arg1376z00_987 = 
MAKE_YOUNG_PAIR(BgL_arg1380z00_991, BgL_accz00_983); } 
{ 
 obj_t BgL_accz00_4969; obj_t BgL_lz00_4968;
BgL_lz00_4968 = BgL_arg1375z00_986; 
BgL_accz00_4969 = BgL_arg1376z00_987; 
BgL_accz00_983 = BgL_accz00_4969; 
BgL_lz00_982 = BgL_lz00_4968; 
goto BgL_zc3z04anonymousza31373ze3z87_984;} } } } 

}



/* &ereverse */
obj_t BGl_z62ereversez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2406, obj_t BgL_lz00_2407)
{
{ /* Ieee/pairlist.scm 621 */
{ /* Ieee/pairlist.scm 624 */
 obj_t BgL_auxz00_4970;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2407))
{ /* Ieee/pairlist.scm 624 */
BgL_auxz00_4970 = BgL_lz00_2407
; }  else 
{ 
 obj_t BgL_auxz00_4973;
BgL_auxz00_4973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25233L), BGl_string2550z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2407); 
FAILURE(BgL_auxz00_4973,BFALSE,BFALSE);} 
return 
BGl_ereversez00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4970);} } 

}



/* take */
BGL_EXPORTED_DEF obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_listz00_63, long BgL_kz00_64)
{
{ /* Ieee/pairlist.scm 634 */
{ 
 obj_t BgL_listz00_1938; long BgL_kz00_1939; obj_t BgL_resz00_1940;
BgL_listz00_1938 = BgL_listz00_63; 
BgL_kz00_1939 = BgL_kz00_64; 
BgL_resz00_1940 = BNIL; 
BgL_loopz00_1937:
if(
(BgL_kz00_1939==0L))
{ /* Ieee/pairlist.scm 638 */
return 
bgl_reverse_bang(BgL_resz00_1940);}  else 
{ /* Ieee/pairlist.scm 640 */
 obj_t BgL_arg1383z00_1947; long BgL_arg1384z00_1948; obj_t BgL_arg1387z00_1949;
{ /* Ieee/pairlist.scm 640 */
 obj_t BgL_pairz00_1953;
if(
PAIRP(BgL_listz00_1938))
{ /* Ieee/pairlist.scm 640 */
BgL_pairz00_1953 = BgL_listz00_1938; }  else 
{ 
 obj_t BgL_auxz00_4983;
BgL_auxz00_4983 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25708L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_1938); 
FAILURE(BgL_auxz00_4983,BFALSE,BFALSE);} 
BgL_arg1383z00_1947 = 
CDR(BgL_pairz00_1953); } 
BgL_arg1384z00_1948 = 
(BgL_kz00_1939-1L); 
{ /* Ieee/pairlist.scm 640 */
 obj_t BgL_arg1388z00_1950;
{ /* Ieee/pairlist.scm 640 */
 obj_t BgL_pairz00_1955;
if(
PAIRP(BgL_listz00_1938))
{ /* Ieee/pairlist.scm 640 */
BgL_pairz00_1955 = BgL_listz00_1938; }  else 
{ 
 obj_t BgL_auxz00_4991;
BgL_auxz00_4991 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25735L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_1938); 
FAILURE(BgL_auxz00_4991,BFALSE,BFALSE);} 
BgL_arg1388z00_1950 = 
CAR(BgL_pairz00_1955); } 
BgL_arg1387z00_1949 = 
MAKE_YOUNG_PAIR(BgL_arg1388z00_1950, BgL_resz00_1940); } 
{ 
 obj_t BgL_resz00_4999; long BgL_kz00_4998; obj_t BgL_listz00_4997;
BgL_listz00_4997 = BgL_arg1383z00_1947; 
BgL_kz00_4998 = BgL_arg1384z00_1948; 
BgL_resz00_4999 = BgL_arg1387z00_1949; 
BgL_resz00_1940 = BgL_resz00_4999; 
BgL_kz00_1939 = BgL_kz00_4998; 
BgL_listz00_1938 = BgL_listz00_4997; 
goto BgL_loopz00_1937;} } } } 

}



/* &take */
obj_t BGl_z62takez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2408, obj_t BgL_listz00_2409, obj_t BgL_kz00_2410)
{
{ /* Ieee/pairlist.scm 634 */
{ /* Ieee/pairlist.scm 635 */
 long BgL_auxz00_5007; obj_t BgL_auxz00_5000;
{ /* Ieee/pairlist.scm 635 */
 obj_t BgL_tmpz00_5008;
if(
INTEGERP(BgL_kz00_2410))
{ /* Ieee/pairlist.scm 635 */
BgL_tmpz00_5008 = BgL_kz00_2410
; }  else 
{ 
 obj_t BgL_auxz00_5011;
BgL_auxz00_5011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25600L), BGl_string2551z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2410); 
FAILURE(BgL_auxz00_5011,BFALSE,BFALSE);} 
BgL_auxz00_5007 = 
(long)CINT(BgL_tmpz00_5008); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2409))
{ /* Ieee/pairlist.scm 635 */
BgL_auxz00_5000 = BgL_listz00_2409
; }  else 
{ 
 obj_t BgL_auxz00_5003;
BgL_auxz00_5003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25600L), BGl_string2551z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2409); 
FAILURE(BgL_auxz00_5003,BFALSE,BFALSE);} 
return 
BGl_takez00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5000, BgL_auxz00_5007);} } 

}



/* drop */
BGL_EXPORTED_DEF obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_listz00_65, long BgL_kz00_66)
{
{ /* Ieee/pairlist.scm 645 */
BGl_dropz00zz__r4_pairs_and_lists_6_3z00:
if(
(BgL_kz00_66==0L))
{ /* Ieee/pairlist.scm 646 */
return BgL_listz00_65;}  else 
{ /* Ieee/pairlist.scm 648 */
 obj_t BgL_listz00_1963; long BgL_kz00_1964;
{ /* Ieee/pairlist.scm 648 */
 obj_t BgL_pairz00_1961;
if(
PAIRP(BgL_listz00_65))
{ /* Ieee/pairlist.scm 648 */
BgL_pairz00_1961 = BgL_listz00_65; }  else 
{ 
 obj_t BgL_auxz00_5021;
BgL_auxz00_5021 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26044L), BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_65); 
FAILURE(BgL_auxz00_5021,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2188z00_2820;
BgL_aux2188z00_2820 = 
CDR(BgL_pairz00_1961); 
{ /* Ieee/pairlist.scm 267 */
 bool_t BgL_test3313z00_5026;
if(
PAIRP(BgL_aux2188z00_2820))
{ /* Ieee/pairlist.scm 241 */
BgL_test3313z00_5026 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3313z00_5026 = 
NULLP(BgL_aux2188z00_2820)
; } 
if(BgL_test3313z00_5026)
{ /* Ieee/pairlist.scm 267 */
BgL_listz00_1963 = BgL_aux2188z00_2820; }  else 
{ 
 obj_t BgL_auxz00_5030;
BgL_auxz00_5030 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2188z00_2820); 
FAILURE(BgL_auxz00_5030,BFALSE,BFALSE);} } } } 
BgL_kz00_1964 = 
(BgL_kz00_66-1L); 
if(
(BgL_kz00_1964==0L))
{ /* Ieee/pairlist.scm 646 */
return BgL_listz00_1963;}  else 
{ 
 long BgL_kz00_5053; obj_t BgL_listz00_5037;
{ /* Ieee/pairlist.scm 648 */
 obj_t BgL_pairz00_1970;
if(
PAIRP(BgL_listz00_1963))
{ /* Ieee/pairlist.scm 648 */
BgL_pairz00_1970 = BgL_listz00_1963; }  else 
{ 
 obj_t BgL_auxz00_5040;
BgL_auxz00_5040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26044L), BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_1963); 
FAILURE(BgL_auxz00_5040,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2192z00_2824;
BgL_aux2192z00_2824 = 
CDR(BgL_pairz00_1970); 
{ /* Ieee/pairlist.scm 267 */
 bool_t BgL_test3317z00_5045;
if(
PAIRP(BgL_aux2192z00_2824))
{ /* Ieee/pairlist.scm 241 */
BgL_test3317z00_5045 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3317z00_5045 = 
NULLP(BgL_aux2192z00_2824)
; } 
if(BgL_test3317z00_5045)
{ /* Ieee/pairlist.scm 267 */
BgL_listz00_5037 = BgL_aux2192z00_2824; }  else 
{ 
 obj_t BgL_auxz00_5049;
BgL_auxz00_5049 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2192z00_2824); 
FAILURE(BgL_auxz00_5049,BFALSE,BFALSE);} } } } 
BgL_kz00_5053 = 
(BgL_kz00_1964-1L); 
BgL_kz00_66 = BgL_kz00_5053; 
BgL_listz00_65 = BgL_listz00_5037; 
goto BGl_dropz00zz__r4_pairs_and_lists_6_3z00;} } } 

}



/* &drop */
obj_t BGl_z62dropz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2411, obj_t BgL_listz00_2412, obj_t BgL_kz00_2413)
{
{ /* Ieee/pairlist.scm 645 */
{ /* Ieee/pairlist.scm 646 */
 long BgL_auxz00_5062; obj_t BgL_auxz00_5055;
{ /* Ieee/pairlist.scm 646 */
 obj_t BgL_tmpz00_5063;
if(
INTEGERP(BgL_kz00_2413))
{ /* Ieee/pairlist.scm 646 */
BgL_tmpz00_5063 = BgL_kz00_2413
; }  else 
{ 
 obj_t BgL_auxz00_5066;
BgL_auxz00_5066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25998L), BGl_string2554z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2413); 
FAILURE(BgL_auxz00_5066,BFALSE,BFALSE);} 
BgL_auxz00_5062 = 
(long)CINT(BgL_tmpz00_5063); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2412))
{ /* Ieee/pairlist.scm 646 */
BgL_auxz00_5055 = BgL_listz00_2412
; }  else 
{ 
 obj_t BgL_auxz00_5058;
BgL_auxz00_5058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(25998L), BGl_string2554z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2412); 
FAILURE(BgL_auxz00_5058,BFALSE,BFALSE);} 
return 
BGl_dropz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5055, BgL_auxz00_5062);} } 

}



/* list-tail */
BGL_EXPORTED_DEF obj_t BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_listz00_67, long BgL_kz00_68)
{
{ /* Ieee/pairlist.scm 653 */
{ 
 obj_t BgL_listz00_1973; long BgL_kz00_1974;
BgL_listz00_1973 = BgL_listz00_67; 
BgL_kz00_1974 = BgL_kz00_68; 
BgL_dropz00_1972:
if(
(BgL_kz00_1974==0L))
{ /* Ieee/pairlist.scm 646 */
return BgL_listz00_1973;}  else 
{ 
 long BgL_kz00_5090; obj_t BgL_listz00_5074;
{ /* Ieee/pairlist.scm 648 */
 obj_t BgL_pairz00_1983;
if(
PAIRP(BgL_listz00_1973))
{ /* Ieee/pairlist.scm 648 */
BgL_pairz00_1983 = BgL_listz00_1973; }  else 
{ 
 obj_t BgL_auxz00_5077;
BgL_auxz00_5077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26044L), BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_1973); 
FAILURE(BgL_auxz00_5077,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2199z00_2831;
BgL_aux2199z00_2831 = 
CDR(BgL_pairz00_1983); 
{ /* Ieee/pairlist.scm 267 */
 bool_t BgL_test3323z00_5082;
if(
PAIRP(BgL_aux2199z00_2831))
{ /* Ieee/pairlist.scm 241 */
BgL_test3323z00_5082 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3323z00_5082 = 
NULLP(BgL_aux2199z00_2831)
; } 
if(BgL_test3323z00_5082)
{ /* Ieee/pairlist.scm 267 */
BgL_listz00_5074 = BgL_aux2199z00_2831; }  else 
{ 
 obj_t BgL_auxz00_5086;
BgL_auxz00_5086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2553z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2199z00_2831); 
FAILURE(BgL_auxz00_5086,BFALSE,BFALSE);} } } } 
BgL_kz00_5090 = 
(BgL_kz00_1974-1L); 
BgL_kz00_1974 = BgL_kz00_5090; 
BgL_listz00_1973 = BgL_listz00_5074; 
goto BgL_dropz00_1972;} } } 

}



/* &list-tail */
obj_t BGl_z62listzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2414, obj_t BgL_listz00_2415, obj_t BgL_kz00_2416)
{
{ /* Ieee/pairlist.scm 653 */
{ /* Ieee/pairlist.scm 654 */
 long BgL_auxz00_5099; obj_t BgL_auxz00_5092;
{ /* Ieee/pairlist.scm 654 */
 obj_t BgL_tmpz00_5100;
if(
INTEGERP(BgL_kz00_2416))
{ /* Ieee/pairlist.scm 654 */
BgL_tmpz00_5100 = BgL_kz00_2416
; }  else 
{ 
 obj_t BgL_auxz00_5103;
BgL_auxz00_5103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26316L), BGl_string2555z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2416); 
FAILURE(BgL_auxz00_5103,BFALSE,BFALSE);} 
BgL_auxz00_5099 = 
(long)CINT(BgL_tmpz00_5100); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2415))
{ /* Ieee/pairlist.scm 654 */
BgL_auxz00_5092 = BgL_listz00_2415
; }  else 
{ 
 obj_t BgL_auxz00_5095;
BgL_auxz00_5095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26316L), BGl_string2555z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2415); 
FAILURE(BgL_auxz00_5095,BFALSE,BFALSE);} 
return 
BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5092, BgL_auxz00_5099);} } 

}



/* list-ref */
BGL_EXPORTED_DEF obj_t bgl_list_ref(obj_t BgL_listz00_69, long BgL_kz00_70)
{
{ /* Ieee/pairlist.scm 659 */
bgl_list_ref:
if(
(BgL_kz00_70==0L))
{ /* Ieee/pairlist.scm 661 */
 obj_t BgL_pairz00_1990;
if(
PAIRP(BgL_listz00_69))
{ /* Ieee/pairlist.scm 661 */
BgL_pairz00_1990 = BgL_listz00_69; }  else 
{ 
 obj_t BgL_auxz00_5113;
BgL_auxz00_5113 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26611L), BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_69); 
FAILURE(BgL_auxz00_5113,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_1990);}  else 
{ /* Ieee/pairlist.scm 662 */
 obj_t BgL_listz00_1993; long BgL_kz00_1994;
{ /* Ieee/pairlist.scm 662 */
 obj_t BgL_pairz00_1991;
if(
PAIRP(BgL_listz00_69))
{ /* Ieee/pairlist.scm 662 */
BgL_pairz00_1991 = BgL_listz00_69; }  else 
{ 
 obj_t BgL_auxz00_5120;
BgL_auxz00_5120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26639L), BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_69); 
FAILURE(BgL_auxz00_5120,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2208z00_2840;
BgL_aux2208z00_2840 = 
CDR(BgL_pairz00_1991); 
{ /* Ieee/pairlist.scm 267 */
 bool_t BgL_test3330z00_5125;
if(
PAIRP(BgL_aux2208z00_2840))
{ /* Ieee/pairlist.scm 241 */
BgL_test3330z00_5125 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3330z00_5125 = 
NULLP(BgL_aux2208z00_2840)
; } 
if(BgL_test3330z00_5125)
{ /* Ieee/pairlist.scm 267 */
BgL_listz00_1993 = BgL_aux2208z00_2840; }  else 
{ 
 obj_t BgL_auxz00_5129;
BgL_auxz00_5129 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2208z00_2840); 
FAILURE(BgL_auxz00_5129,BFALSE,BFALSE);} } } } 
BgL_kz00_1994 = 
(BgL_kz00_70-1L); 
if(
(BgL_kz00_1994==0L))
{ /* Ieee/pairlist.scm 661 */
 obj_t BgL_pairz00_2000;
if(
PAIRP(BgL_listz00_1993))
{ /* Ieee/pairlist.scm 661 */
BgL_pairz00_2000 = BgL_listz00_1993; }  else 
{ 
 obj_t BgL_auxz00_5138;
BgL_auxz00_5138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26611L), BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_1993); 
FAILURE(BgL_auxz00_5138,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_2000);}  else 
{ 
 long BgL_kz00_5159; obj_t BgL_listz00_5143;
{ /* Ieee/pairlist.scm 662 */
 obj_t BgL_pairz00_2001;
if(
PAIRP(BgL_listz00_1993))
{ /* Ieee/pairlist.scm 662 */
BgL_pairz00_2001 = BgL_listz00_1993; }  else 
{ 
 obj_t BgL_auxz00_5146;
BgL_auxz00_5146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26639L), BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_1993); 
FAILURE(BgL_auxz00_5146,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2214z00_2846;
BgL_aux2214z00_2846 = 
CDR(BgL_pairz00_2001); 
{ /* Ieee/pairlist.scm 267 */
 bool_t BgL_test3335z00_5151;
if(
PAIRP(BgL_aux2214z00_2846))
{ /* Ieee/pairlist.scm 241 */
BgL_test3335z00_5151 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3335z00_5151 = 
NULLP(BgL_aux2214z00_2846)
; } 
if(BgL_test3335z00_5151)
{ /* Ieee/pairlist.scm 267 */
BgL_listz00_5143 = BgL_aux2214z00_2846; }  else 
{ 
 obj_t BgL_auxz00_5155;
BgL_auxz00_5155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2556z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2214z00_2846); 
FAILURE(BgL_auxz00_5155,BFALSE,BFALSE);} } } } 
BgL_kz00_5159 = 
(BgL_kz00_1994-1L); 
BgL_kz00_70 = BgL_kz00_5159; 
BgL_listz00_69 = BgL_listz00_5143; 
goto bgl_list_ref;} } } 

}



/* &list-ref */
obj_t BGl_z62listzd2refzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2417, obj_t BgL_listz00_2418, obj_t BgL_kz00_2419)
{
{ /* Ieee/pairlist.scm 659 */
{ /* Ieee/pairlist.scm 660 */
 long BgL_auxz00_5168; obj_t BgL_auxz00_5161;
{ /* Ieee/pairlist.scm 660 */
 obj_t BgL_tmpz00_5169;
if(
INTEGERP(BgL_kz00_2419))
{ /* Ieee/pairlist.scm 660 */
BgL_tmpz00_5169 = BgL_kz00_2419
; }  else 
{ 
 obj_t BgL_auxz00_5172;
BgL_auxz00_5172 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26583L), BGl_string2557z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2419); 
FAILURE(BgL_auxz00_5172,BFALSE,BFALSE);} 
BgL_auxz00_5168 = 
(long)CINT(BgL_tmpz00_5169); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2418))
{ /* Ieee/pairlist.scm 660 */
BgL_auxz00_5161 = BgL_listz00_2418
; }  else 
{ 
 obj_t BgL_auxz00_5164;
BgL_auxz00_5164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26583L), BGl_string2557z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2418); 
FAILURE(BgL_auxz00_5164,BFALSE,BFALSE);} 
return 
bgl_list_ref(BgL_auxz00_5161, BgL_auxz00_5168);} } 

}



/* list-set! */
BGL_EXPORTED_DEF obj_t BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_listz00_71, long BgL_kz00_72, obj_t BgL_valz00_73)
{
{ /* Ieee/pairlist.scm 667 */
BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00:
if(
(BgL_kz00_72==0L))
{ /* Ieee/pairlist.scm 669 */
 obj_t BgL_pairz00_2008;
if(
PAIRP(BgL_listz00_71))
{ /* Ieee/pairlist.scm 669 */
BgL_pairz00_2008 = BgL_listz00_71; }  else 
{ 
 obj_t BgL_auxz00_5182;
BgL_auxz00_5182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26948L), BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_71); 
FAILURE(BgL_auxz00_5182,BFALSE,BFALSE);} 
return 
SET_CAR(BgL_pairz00_2008, BgL_valz00_73);}  else 
{ /* Ieee/pairlist.scm 670 */
 obj_t BgL_arg1396z00_2004; long BgL_arg1397z00_2005;
{ /* Ieee/pairlist.scm 670 */
 obj_t BgL_pairz00_2009;
if(
PAIRP(BgL_listz00_71))
{ /* Ieee/pairlist.scm 670 */
BgL_pairz00_2009 = BgL_listz00_71; }  else 
{ 
 obj_t BgL_auxz00_5189;
BgL_auxz00_5189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26981L), BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_71); 
FAILURE(BgL_auxz00_5189,BFALSE,BFALSE);} 
BgL_arg1396z00_2004 = 
CDR(BgL_pairz00_2009); } 
BgL_arg1397z00_2005 = 
(BgL_kz00_72-1L); 
{ /* Ieee/pairlist.scm 670 */
 obj_t BgL_listz00_2011;
{ /* Ieee/pairlist.scm 670 */
 bool_t BgL_test3342z00_5195;
if(
PAIRP(BgL_arg1396z00_2004))
{ /* Ieee/pairlist.scm 241 */
BgL_test3342z00_5195 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3342z00_5195 = 
NULLP(BgL_arg1396z00_2004)
; } 
if(BgL_test3342z00_5195)
{ /* Ieee/pairlist.scm 670 */
BgL_listz00_2011 = BgL_arg1396z00_2004; }  else 
{ 
 obj_t BgL_auxz00_5199;
BgL_auxz00_5199 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26985L), BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1396z00_2004); 
FAILURE(BgL_auxz00_5199,BFALSE,BFALSE);} } 
if(
(BgL_arg1397z00_2005==0L))
{ /* Ieee/pairlist.scm 669 */
 obj_t BgL_pairz00_2018;
if(
PAIRP(BgL_listz00_2011))
{ /* Ieee/pairlist.scm 669 */
BgL_pairz00_2018 = BgL_listz00_2011; }  else 
{ 
 obj_t BgL_auxz00_5207;
BgL_auxz00_5207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26948L), BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2011); 
FAILURE(BgL_auxz00_5207,BFALSE,BFALSE);} 
return 
SET_CAR(BgL_pairz00_2018, BgL_valz00_73);}  else 
{ /* Ieee/pairlist.scm 670 */
 obj_t BgL_arg1396z00_2014; long BgL_arg1397z00_2015;
{ /* Ieee/pairlist.scm 670 */
 obj_t BgL_pairz00_2019;
if(
PAIRP(BgL_listz00_2011))
{ /* Ieee/pairlist.scm 670 */
BgL_pairz00_2019 = BgL_listz00_2011; }  else 
{ 
 obj_t BgL_auxz00_5214;
BgL_auxz00_5214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26981L), BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2011); 
FAILURE(BgL_auxz00_5214,BFALSE,BFALSE);} 
BgL_arg1396z00_2014 = 
CDR(BgL_pairz00_2019); } 
BgL_arg1397z00_2015 = 
(BgL_arg1397z00_2005-1L); 
{ 
 long BgL_kz00_5229; obj_t BgL_listz00_5220;
{ /* Ieee/pairlist.scm 670 */
 bool_t BgL_test3347z00_5221;
if(
PAIRP(BgL_arg1396z00_2014))
{ /* Ieee/pairlist.scm 241 */
BgL_test3347z00_5221 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3347z00_5221 = 
NULLP(BgL_arg1396z00_2014)
; } 
if(BgL_test3347z00_5221)
{ /* Ieee/pairlist.scm 670 */
BgL_listz00_5220 = BgL_arg1396z00_2014; }  else 
{ 
 obj_t BgL_auxz00_5225;
BgL_auxz00_5225 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26985L), BGl_string2558z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1396z00_2014); 
FAILURE(BgL_auxz00_5225,BFALSE,BFALSE);} } 
BgL_kz00_5229 = BgL_arg1397z00_2015; 
BgL_kz00_72 = BgL_kz00_5229; 
BgL_listz00_71 = BgL_listz00_5220; 
goto BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00;} } } } } 

}



/* &list-set! */
obj_t BGl_z62listzd2setz12za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2420, obj_t BgL_listz00_2421, obj_t BgL_kz00_2422, obj_t BgL_valz00_2423)
{
{ /* Ieee/pairlist.scm 667 */
{ /* Ieee/pairlist.scm 668 */
 long BgL_auxz00_5237; obj_t BgL_auxz00_5230;
{ /* Ieee/pairlist.scm 668 */
 obj_t BgL_tmpz00_5238;
if(
INTEGERP(BgL_kz00_2422))
{ /* Ieee/pairlist.scm 668 */
BgL_tmpz00_5238 = BgL_kz00_2422
; }  else 
{ 
 obj_t BgL_auxz00_5241;
BgL_auxz00_5241 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26915L), BGl_string2559z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_kz00_2422); 
FAILURE(BgL_auxz00_5241,BFALSE,BFALSE);} 
BgL_auxz00_5237 = 
(long)CINT(BgL_tmpz00_5238); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2421))
{ /* Ieee/pairlist.scm 668 */
BgL_auxz00_5230 = BgL_listz00_2421
; }  else 
{ 
 obj_t BgL_auxz00_5233;
BgL_auxz00_5233 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(26915L), BGl_string2559z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2421); 
FAILURE(BgL_auxz00_5233,BFALSE,BFALSE);} 
return 
BGl_listzd2setz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5230, BgL_auxz00_5237, BgL_valz00_2423);} } 

}



/* last-pair */
BGL_EXPORTED_DEF obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_74)
{
{ /* Ieee/pairlist.scm 675 */
BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00:
{ /* Ieee/pairlist.scm 676 */
 bool_t BgL_test3351z00_5247;
{ /* Ieee/pairlist.scm 229 */
 obj_t BgL_tmpz00_5248;
BgL_tmpz00_5248 = 
CDR(BgL_xz00_74); 
BgL_test3351z00_5247 = 
PAIRP(BgL_tmpz00_5248); } 
if(BgL_test3351z00_5247)
{ /* Ieee/pairlist.scm 677 */
 obj_t BgL_xz00_2026;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2234z00_2866;
BgL_aux2234z00_2866 = 
CDR(BgL_xz00_74); 
if(
PAIRP(BgL_aux2234z00_2866))
{ /* Ieee/pairlist.scm 267 */
BgL_xz00_2026 = BgL_aux2234z00_2866; }  else 
{ 
 obj_t BgL_auxz00_5254;
BgL_auxz00_5254 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2560z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2234z00_2866); 
FAILURE(BgL_auxz00_5254,BFALSE,BFALSE);} } 
{ /* Ieee/pairlist.scm 676 */
 bool_t BgL_test3353z00_5258;
{ /* Ieee/pairlist.scm 229 */
 obj_t BgL_tmpz00_5259;
BgL_tmpz00_5259 = 
CDR(BgL_xz00_2026); 
BgL_test3353z00_5258 = 
PAIRP(BgL_tmpz00_5259); } 
if(BgL_test3353z00_5258)
{ 
 obj_t BgL_xz00_5262;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2236z00_2868;
BgL_aux2236z00_2868 = 
CDR(BgL_xz00_2026); 
if(
PAIRP(BgL_aux2236z00_2868))
{ /* Ieee/pairlist.scm 267 */
BgL_xz00_5262 = BgL_aux2236z00_2868; }  else 
{ 
 obj_t BgL_auxz00_5266;
BgL_auxz00_5266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2560z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2236z00_2868); 
FAILURE(BgL_auxz00_5266,BFALSE,BFALSE);} } 
BgL_xz00_74 = BgL_xz00_5262; 
goto BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00;}  else 
{ /* Ieee/pairlist.scm 676 */
return BgL_xz00_2026;} } }  else 
{ /* Ieee/pairlist.scm 676 */
return BgL_xz00_74;} } } 

}



/* &last-pair */
obj_t BGl_z62lastzd2pairzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2424, obj_t BgL_xz00_2425)
{
{ /* Ieee/pairlist.scm 675 */
{ /* Ieee/pairlist.scm 676 */
 obj_t BgL_auxz00_5270;
if(
PAIRP(BgL_xz00_2425))
{ /* Ieee/pairlist.scm 676 */
BgL_auxz00_5270 = BgL_xz00_2425
; }  else 
{ 
 obj_t BgL_auxz00_5273;
BgL_auxz00_5273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(27252L), BGl_string2561z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_2425); 
FAILURE(BgL_auxz00_5273,BFALSE,BFALSE);} 
return 
BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5270);} } 

}



/* memq */
BGL_EXPORTED_DEF obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_75, obj_t BgL_listz00_76)
{
{ /* Ieee/pairlist.scm 683 */
{ 
 obj_t BgL_listz00_1019;
BgL_listz00_1019 = BgL_listz00_76; 
BgL_zc3z04anonymousza31403ze3z87_1020:
if(
PAIRP(BgL_listz00_1019))
{ /* Ieee/pairlist.scm 685 */
if(
(
CAR(BgL_listz00_1019)==BgL_objz00_75))
{ /* Ieee/pairlist.scm 686 */
return BgL_listz00_1019;}  else 
{ 
 obj_t BgL_listz00_5283;
BgL_listz00_5283 = 
CDR(BgL_listz00_1019); 
BgL_listz00_1019 = BgL_listz00_5283; 
goto BgL_zc3z04anonymousza31403ze3z87_1020;} }  else 
{ /* Ieee/pairlist.scm 685 */
return BFALSE;} } } 

}



/* &memq */
obj_t BGl_z62memqz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2426, obj_t BgL_objz00_2427, obj_t BgL_listz00_2428)
{
{ /* Ieee/pairlist.scm 683 */
{ /* Ieee/pairlist.scm 685 */
 obj_t BgL_auxz00_5285;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2428))
{ /* Ieee/pairlist.scm 685 */
BgL_auxz00_5285 = BgL_listz00_2428
; }  else 
{ 
 obj_t BgL_auxz00_5288;
BgL_auxz00_5288 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(27590L), BGl_string2562z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2428); 
FAILURE(BgL_auxz00_5288,BFALSE,BFALSE);} 
return 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2427, BgL_auxz00_5285);} } 

}



/* memv */
BGL_EXPORTED_DEF obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_77, obj_t BgL_listz00_78)
{
{ /* Ieee/pairlist.scm 694 */
{ 
 obj_t BgL_listz00_1028;
BgL_listz00_1028 = BgL_listz00_78; 
BgL_zc3z04anonymousza31409ze3z87_1029:
if(
PAIRP(BgL_listz00_1028))
{ /* Ieee/pairlist.scm 696 */
if(
BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(
CAR(BgL_listz00_1028), BgL_objz00_77))
{ /* Ieee/pairlist.scm 697 */
return BgL_listz00_1028;}  else 
{ 
 obj_t BgL_listz00_5298;
BgL_listz00_5298 = 
CDR(BgL_listz00_1028); 
BgL_listz00_1028 = BgL_listz00_5298; 
goto BgL_zc3z04anonymousza31409ze3z87_1029;} }  else 
{ /* Ieee/pairlist.scm 696 */
return BFALSE;} } } 

}



/* &memv */
obj_t BGl_z62memvz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2429, obj_t BgL_objz00_2430, obj_t BgL_listz00_2431)
{
{ /* Ieee/pairlist.scm 694 */
{ /* Ieee/pairlist.scm 696 */
 obj_t BgL_auxz00_5300;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2431))
{ /* Ieee/pairlist.scm 696 */
BgL_auxz00_5300 = BgL_listz00_2431
; }  else 
{ 
 obj_t BgL_auxz00_5303;
BgL_auxz00_5303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(27962L), BGl_string2563z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2431); 
FAILURE(BgL_auxz00_5303,BFALSE,BFALSE);} 
return 
BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2430, BgL_auxz00_5300);} } 

}



/* member */
BGL_EXPORTED_DEF obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_79, obj_t BgL_listz00_80)
{
{ /* Ieee/pairlist.scm 705 */
{ 
 obj_t BgL_listz00_1037;
BgL_listz00_1037 = BgL_listz00_80; 
BgL_zc3z04anonymousza31415ze3z87_1038:
if(
PAIRP(BgL_listz00_1037))
{ /* Ieee/pairlist.scm 708 */
if(
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_objz00_79, 
CAR(BgL_listz00_1037)))
{ /* Ieee/pairlist.scm 709 */
return BgL_listz00_1037;}  else 
{ 
 obj_t BgL_listz00_5313;
BgL_listz00_5313 = 
CDR(BgL_listz00_1037); 
BgL_listz00_1037 = BgL_listz00_5313; 
goto BgL_zc3z04anonymousza31415ze3z87_1038;} }  else 
{ /* Ieee/pairlist.scm 708 */
return BFALSE;} } } 

}



/* &member */
obj_t BGl_z62memberz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2432, obj_t BgL_objz00_2433, obj_t BgL_listz00_2434)
{
{ /* Ieee/pairlist.scm 705 */
{ /* Ieee/pairlist.scm 708 */
 obj_t BgL_auxz00_5315;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2434))
{ /* Ieee/pairlist.scm 708 */
BgL_auxz00_5315 = BgL_listz00_2434
; }  else 
{ 
 obj_t BgL_auxz00_5318;
BgL_auxz00_5318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(28345L), BGl_string2564z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2434); 
FAILURE(BgL_auxz00_5318,BFALSE,BFALSE);} 
return 
BGl_memberz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2433, BgL_auxz00_5315);} } 

}



/* assq */
BGL_EXPORTED_DEF obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_81, obj_t BgL_alistz00_82)
{
{ /* Ieee/pairlist.scm 715 */
{ 
 obj_t BgL_alistz00_1046;
BgL_alistz00_1046 = BgL_alistz00_82; 
BgL_zc3z04anonymousza31421ze3z87_1047:
if(
PAIRP(BgL_alistz00_1046))
{ /* Ieee/pairlist.scm 718 */
 bool_t BgL_test3366z00_5325;
{ /* Ieee/pairlist.scm 718 */
 obj_t BgL_tmpz00_5326;
{ /* Ieee/pairlist.scm 718 */
 obj_t BgL_pairz00_2039;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2246z00_2878;
BgL_aux2246z00_2878 = 
CAR(BgL_alistz00_1046); 
if(
PAIRP(BgL_aux2246z00_2878))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2039 = BgL_aux2246z00_2878; }  else 
{ 
 obj_t BgL_auxz00_5330;
BgL_auxz00_5330 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2246z00_2878); 
FAILURE(BgL_auxz00_5330,BFALSE,BFALSE);} } 
BgL_tmpz00_5326 = 
CAR(BgL_pairz00_2039); } 
BgL_test3366z00_5325 = 
(BgL_tmpz00_5326==BgL_objz00_81); } 
if(BgL_test3366z00_5325)
{ /* Ieee/pairlist.scm 718 */
return 
CAR(BgL_alistz00_1046);}  else 
{ 
 obj_t BgL_alistz00_5337;
BgL_alistz00_5337 = 
CDR(BgL_alistz00_1046); 
BgL_alistz00_1046 = BgL_alistz00_5337; 
goto BgL_zc3z04anonymousza31421ze3z87_1047;} }  else 
{ /* Ieee/pairlist.scm 717 */
return BFALSE;} } } 

}



/* &assq */
obj_t BGl_z62assqz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2435, obj_t BgL_objz00_2436, obj_t BgL_alistz00_2437)
{
{ /* Ieee/pairlist.scm 715 */
{ /* Ieee/pairlist.scm 717 */
 obj_t BgL_auxz00_5339;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_alistz00_2437))
{ /* Ieee/pairlist.scm 717 */
BgL_auxz00_5339 = BgL_alistz00_2437
; }  else 
{ 
 obj_t BgL_auxz00_5342;
BgL_auxz00_5342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(28716L), BGl_string2565z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_alistz00_2437); 
FAILURE(BgL_auxz00_5342,BFALSE,BFALSE);} 
return 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2436, BgL_auxz00_5339);} } 

}



/* assv */
BGL_EXPORTED_DEF obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_83, obj_t BgL_alistz00_84)
{
{ /* Ieee/pairlist.scm 726 */
{ 
 obj_t BgL_alistz00_1057;
BgL_alistz00_1057 = BgL_alistz00_84; 
BgL_zc3z04anonymousza31429ze3z87_1058:
if(
PAIRP(BgL_alistz00_1057))
{ /* Ieee/pairlist.scm 729 */
 bool_t BgL_test3370z00_5349;
{ /* Ieee/pairlist.scm 729 */
 obj_t BgL_auxz00_5350;
{ /* Ieee/pairlist.scm 729 */
 obj_t BgL_pairz00_2043;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2250z00_2882;
BgL_aux2250z00_2882 = 
CAR(BgL_alistz00_1057); 
if(
PAIRP(BgL_aux2250z00_2882))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2043 = BgL_aux2250z00_2882; }  else 
{ 
 obj_t BgL_auxz00_5354;
BgL_auxz00_5354 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2250z00_2882); 
FAILURE(BgL_auxz00_5354,BFALSE,BFALSE);} } 
BgL_auxz00_5350 = 
CAR(BgL_pairz00_2043); } 
BgL_test3370z00_5349 = 
BGl_eqvzf3zf3zz__r4_equivalence_6_2z00(BgL_auxz00_5350, BgL_objz00_83); } 
if(BgL_test3370z00_5349)
{ /* Ieee/pairlist.scm 729 */
return 
CAR(BgL_alistz00_1057);}  else 
{ 
 obj_t BgL_alistz00_5361;
BgL_alistz00_5361 = 
CDR(BgL_alistz00_1057); 
BgL_alistz00_1057 = BgL_alistz00_5361; 
goto BgL_zc3z04anonymousza31429ze3z87_1058;} }  else 
{ /* Ieee/pairlist.scm 728 */
return BFALSE;} } } 

}



/* &assv */
obj_t BGl_z62assvz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2438, obj_t BgL_objz00_2439, obj_t BgL_alistz00_2440)
{
{ /* Ieee/pairlist.scm 726 */
{ /* Ieee/pairlist.scm 728 */
 obj_t BgL_auxz00_5363;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_alistz00_2440))
{ /* Ieee/pairlist.scm 728 */
BgL_auxz00_5363 = BgL_alistz00_2440
; }  else 
{ 
 obj_t BgL_auxz00_5366;
BgL_auxz00_5366 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(29135L), BGl_string2566z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_alistz00_2440); 
FAILURE(BgL_auxz00_5366,BFALSE,BFALSE);} 
return 
BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2439, BgL_auxz00_5363);} } 

}



/* assoc */
BGL_EXPORTED_DEF obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_objz00_85, obj_t BgL_alistz00_86)
{
{ /* Ieee/pairlist.scm 737 */
{ 
 obj_t BgL_alistz00_1068;
BgL_alistz00_1068 = BgL_alistz00_86; 
BgL_zc3z04anonymousza31439ze3z87_1069:
if(
PAIRP(BgL_alistz00_1068))
{ /* Ieee/pairlist.scm 740 */
 bool_t BgL_test3374z00_5373;
{ /* Ieee/pairlist.scm 740 */
 obj_t BgL_auxz00_5374;
{ /* Ieee/pairlist.scm 740 */
 obj_t BgL_pairz00_2047;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2254z00_2886;
BgL_aux2254z00_2886 = 
CAR(BgL_alistz00_1068); 
if(
PAIRP(BgL_aux2254z00_2886))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2047 = BgL_aux2254z00_2886; }  else 
{ 
 obj_t BgL_auxz00_5378;
BgL_auxz00_5378 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2254z00_2886); 
FAILURE(BgL_auxz00_5378,BFALSE,BFALSE);} } 
BgL_auxz00_5374 = 
CAR(BgL_pairz00_2047); } 
BgL_test3374z00_5373 = 
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_auxz00_5374, BgL_objz00_85); } 
if(BgL_test3374z00_5373)
{ /* Ieee/pairlist.scm 740 */
return 
CAR(BgL_alistz00_1068);}  else 
{ 
 obj_t BgL_alistz00_5385;
BgL_alistz00_5385 = 
CDR(BgL_alistz00_1068); 
BgL_alistz00_1068 = BgL_alistz00_5385; 
goto BgL_zc3z04anonymousza31439ze3z87_1069;} }  else 
{ /* Ieee/pairlist.scm 739 */
return BFALSE;} } } 

}



/* &assoc */
obj_t BGl_z62assocz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2441, obj_t BgL_objz00_2442, obj_t BgL_alistz00_2443)
{
{ /* Ieee/pairlist.scm 737 */
{ /* Ieee/pairlist.scm 739 */
 obj_t BgL_auxz00_5387;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_alistz00_2443))
{ /* Ieee/pairlist.scm 739 */
BgL_auxz00_5387 = BgL_alistz00_2443
; }  else 
{ 
 obj_t BgL_auxz00_5390;
BgL_auxz00_5390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(29549L), BGl_string2567z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_alistz00_2443); 
FAILURE(BgL_auxz00_5390,BFALSE,BFALSE);} 
return 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_2442, BgL_auxz00_5387);} } 

}



/* remq */
BGL_EXPORTED_DEF obj_t bgl_remq(obj_t BgL_xz00_87, obj_t BgL_yz00_88)
{
{ /* Ieee/pairlist.scm 751 */
bgl_remq:
if(
NULLP(BgL_yz00_88))
{ /* Ieee/pairlist.scm 753 */
return BgL_yz00_88;}  else 
{ /* Ieee/pairlist.scm 753 */
if(
(BgL_xz00_87==
CAR(BgL_yz00_88)))
{ /* Ieee/pairlist.scm 754 */
 obj_t BgL_arg1450z00_1081;
BgL_arg1450z00_1081 = 
CDR(BgL_yz00_88); 
{ 
 obj_t BgL_yz00_5401;
{ /* Ieee/pairlist.scm 754 */
 bool_t BgL_test3379z00_5402;
if(
PAIRP(BgL_arg1450z00_1081))
{ /* Ieee/pairlist.scm 241 */
BgL_test3379z00_5402 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3379z00_5402 = 
NULLP(BgL_arg1450z00_1081)
; } 
if(BgL_test3379z00_5402)
{ /* Ieee/pairlist.scm 754 */
BgL_yz00_5401 = BgL_arg1450z00_1081; }  else 
{ 
 obj_t BgL_auxz00_5406;
BgL_auxz00_5406 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30211L), BGl_string2568z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1450z00_1081); 
FAILURE(BgL_auxz00_5406,BFALSE,BFALSE);} } 
BgL_yz00_88 = BgL_yz00_5401; 
goto bgl_remq;} }  else 
{ /* Ieee/pairlist.scm 755 */
 obj_t BgL_arg1451z00_1082; obj_t BgL_arg1452z00_1083;
BgL_arg1451z00_1082 = 
CAR(BgL_yz00_88); 
{ /* Ieee/pairlist.scm 755 */
 obj_t BgL_arg1453z00_1084;
BgL_arg1453z00_1084 = 
CDR(BgL_yz00_88); 
{ /* Ieee/pairlist.scm 755 */
 obj_t BgL_auxz00_5412;
{ /* Ieee/pairlist.scm 755 */
 bool_t BgL_test3381z00_5413;
if(
PAIRP(BgL_arg1453z00_1084))
{ /* Ieee/pairlist.scm 241 */
BgL_test3381z00_5413 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3381z00_5413 = 
NULLP(BgL_arg1453z00_1084)
; } 
if(BgL_test3381z00_5413)
{ /* Ieee/pairlist.scm 755 */
BgL_auxz00_5412 = BgL_arg1453z00_1084
; }  else 
{ 
 obj_t BgL_auxz00_5417;
BgL_auxz00_5417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30255L), BGl_string2568z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1453z00_1084); 
FAILURE(BgL_auxz00_5417,BFALSE,BFALSE);} } 
BgL_arg1452z00_1083 = 
bgl_remq(BgL_xz00_87, BgL_auxz00_5412); } } 
return 
MAKE_YOUNG_PAIR(BgL_arg1451z00_1082, BgL_arg1452z00_1083);} } } 

}



/* &remq */
obj_t BGl_z62remqz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2444, obj_t BgL_xz00_2445, obj_t BgL_yz00_2446)
{
{ /* Ieee/pairlist.scm 751 */
{ /* Ieee/pairlist.scm 753 */
 obj_t BgL_auxz00_5423;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_yz00_2446))
{ /* Ieee/pairlist.scm 753 */
BgL_auxz00_5423 = BgL_yz00_2446
; }  else 
{ 
 obj_t BgL_auxz00_5426;
BgL_auxz00_5426 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30160L), BGl_string2569z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_2446); 
FAILURE(BgL_auxz00_5426,BFALSE,BFALSE);} 
return 
bgl_remq(BgL_xz00_2445, BgL_auxz00_5423);} } 

}



/* remq! */
BGL_EXPORTED_DEF obj_t bgl_remq_bang(obj_t BgL_xz00_89, obj_t BgL_yz00_90)
{
{ /* Ieee/pairlist.scm 760 */
bgl_remq_bang:
if(
NULLP(BgL_yz00_90))
{ /* Ieee/pairlist.scm 762 */
return BgL_yz00_90;}  else 
{ /* Ieee/pairlist.scm 762 */
if(
(BgL_xz00_89==
CAR(BgL_yz00_90)))
{ /* Ieee/pairlist.scm 763 */
 obj_t BgL_arg1458z00_1089;
BgL_arg1458z00_1089 = 
CDR(BgL_yz00_90); 
{ 
 obj_t BgL_yz00_5437;
{ /* Ieee/pairlist.scm 763 */
 bool_t BgL_test3386z00_5438;
if(
PAIRP(BgL_arg1458z00_1089))
{ /* Ieee/pairlist.scm 241 */
BgL_test3386z00_5438 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3386z00_5438 = 
NULLP(BgL_arg1458z00_1089)
; } 
if(BgL_test3386z00_5438)
{ /* Ieee/pairlist.scm 763 */
BgL_yz00_5437 = BgL_arg1458z00_1089; }  else 
{ 
 obj_t BgL_auxz00_5442;
BgL_auxz00_5442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30572L), BGl_string2570z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1458z00_1089); 
FAILURE(BgL_auxz00_5442,BFALSE,BFALSE);} } 
BgL_yz00_90 = BgL_yz00_5437; 
goto bgl_remq_bang;} }  else 
{ 
 obj_t BgL_prevz00_1091;
BgL_prevz00_1091 = BgL_yz00_90; 
BgL_zc3z04anonymousza31459ze3z87_1092:
{ /* Ieee/pairlist.scm 765 */
 bool_t BgL_test3388z00_5446;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5447;
{ /* Ieee/pairlist.scm 765 */
 obj_t BgL_pairz00_2056;
if(
PAIRP(BgL_prevz00_1091))
{ /* Ieee/pairlist.scm 765 */
BgL_pairz00_2056 = BgL_prevz00_1091; }  else 
{ 
 obj_t BgL_auxz00_5450;
BgL_auxz00_5450 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30643L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1091); 
FAILURE(BgL_auxz00_5450,BFALSE,BFALSE);} 
BgL_tmpz00_5447 = 
CDR(BgL_pairz00_2056); } 
BgL_test3388z00_5446 = 
NULLP(BgL_tmpz00_5447); } 
if(BgL_test3388z00_5446)
{ /* Ieee/pairlist.scm 765 */
return BgL_yz00_90;}  else 
{ /* Ieee/pairlist.scm 767 */
 bool_t BgL_test3390z00_5456;
{ /* Ieee/pairlist.scm 767 */
 obj_t BgL_tmpz00_5457;
{ /* Ieee/pairlist.scm 767 */
 obj_t BgL_pairz00_2057;
if(
PAIRP(BgL_prevz00_1091))
{ /* Ieee/pairlist.scm 767 */
BgL_pairz00_2057 = BgL_prevz00_1091; }  else 
{ 
 obj_t BgL_auxz00_5460;
BgL_auxz00_5460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30708L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1091); 
FAILURE(BgL_auxz00_5460,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 285 */
 obj_t BgL_pairz00_2060;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2270z00_2902;
BgL_aux2270z00_2902 = 
CDR(BgL_pairz00_2057); 
if(
PAIRP(BgL_aux2270z00_2902))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_2060 = BgL_aux2270z00_2902; }  else 
{ 
 obj_t BgL_auxz00_5467;
BgL_auxz00_5467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2270z00_2902); 
FAILURE(BgL_auxz00_5467,BFALSE,BFALSE);} } 
BgL_tmpz00_5457 = 
CAR(BgL_pairz00_2060); } } 
BgL_test3390z00_5456 = 
(BgL_tmpz00_5457==BgL_xz00_89); } 
if(BgL_test3390z00_5456)
{ /* Ieee/pairlist.scm 767 */
{ /* Ieee/pairlist.scm 768 */
 obj_t BgL_arg1464z00_1097;
{ /* Ieee/pairlist.scm 768 */
 obj_t BgL_pairz00_2061;
if(
PAIRP(BgL_prevz00_1091))
{ /* Ieee/pairlist.scm 768 */
BgL_pairz00_2061 = BgL_prevz00_1091; }  else 
{ 
 obj_t BgL_auxz00_5475;
BgL_auxz00_5475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30760L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1091); 
FAILURE(BgL_auxz00_5475,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 297 */
 obj_t BgL_pairz00_2064;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2274z00_2906;
BgL_aux2274z00_2906 = 
CDR(BgL_pairz00_2061); 
if(
PAIRP(BgL_aux2274z00_2906))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_2064 = BgL_aux2274z00_2906; }  else 
{ 
 obj_t BgL_auxz00_5482;
BgL_auxz00_5482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2274z00_2906); 
FAILURE(BgL_auxz00_5482,BFALSE,BFALSE);} } 
BgL_arg1464z00_1097 = 
CDR(BgL_pairz00_2064); } } 
{ /* Ieee/pairlist.scm 768 */
 obj_t BgL_pairz00_2065;
if(
PAIRP(BgL_prevz00_1091))
{ /* Ieee/pairlist.scm 768 */
BgL_pairz00_2065 = BgL_prevz00_1091; }  else 
{ 
 obj_t BgL_auxz00_5489;
BgL_auxz00_5489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30749L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1091); 
FAILURE(BgL_auxz00_5489,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2065, BgL_arg1464z00_1097); } } 
{ 

goto BgL_zc3z04anonymousza31459ze3z87_1092;} }  else 
{ /* Ieee/pairlist.scm 770 */
 obj_t BgL_arg1465z00_1098;
{ /* Ieee/pairlist.scm 770 */
 obj_t BgL_pairz00_2066;
if(
PAIRP(BgL_prevz00_1091))
{ /* Ieee/pairlist.scm 770 */
BgL_pairz00_2066 = BgL_prevz00_1091; }  else 
{ 
 obj_t BgL_auxz00_5496;
BgL_auxz00_5496 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30840L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1091); 
FAILURE(BgL_auxz00_5496,BFALSE,BFALSE);} 
BgL_arg1465z00_1098 = 
CDR(BgL_pairz00_2066); } 
{ 
 obj_t BgL_prevz00_5501;
BgL_prevz00_5501 = BgL_arg1465z00_1098; 
BgL_prevz00_1091 = BgL_prevz00_5501; 
goto BgL_zc3z04anonymousza31459ze3z87_1092;} } } } } } } 

}



/* &remq! */
obj_t BGl_z62remqz12z70zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2447, obj_t BgL_xz00_2448, obj_t BgL_yz00_2449)
{
{ /* Ieee/pairlist.scm 760 */
{ /* Ieee/pairlist.scm 762 */
 obj_t BgL_auxz00_5502;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_yz00_2449))
{ /* Ieee/pairlist.scm 762 */
BgL_auxz00_5502 = BgL_yz00_2449
; }  else 
{ 
 obj_t BgL_auxz00_5505;
BgL_auxz00_5505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(30520L), BGl_string2571z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_2449); 
FAILURE(BgL_auxz00_5505,BFALSE,BFALSE);} 
return 
bgl_remq_bang(BgL_xz00_2448, BgL_auxz00_5502);} } 

}



/* _delete */
obj_t BGl__deletez00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_env1106z00_95, obj_t BgL_opt1105z00_94)
{
{ /* Ieee/pairlist.scm 775 */
{ /* Ieee/pairlist.scm 775 */
 obj_t BgL_g1107z00_1103; obj_t BgL_g1108z00_1104;
BgL_g1107z00_1103 = 
VECTOR_REF(BgL_opt1105z00_94,0L); 
BgL_g1108z00_1104 = 
VECTOR_REF(BgL_opt1105z00_94,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1105z00_94)) { case 2L : 

{ /* Ieee/pairlist.scm 775 */

{ /* Ieee/pairlist.scm 775 */
 obj_t BgL_auxz00_5512;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1108z00_1104))
{ /* Ieee/pairlist.scm 775 */
BgL_auxz00_5512 = BgL_g1108z00_1104
; }  else 
{ 
 obj_t BgL_auxz00_5515;
BgL_auxz00_5515 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31076L), BGl_string2575z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1108z00_1104); 
FAILURE(BgL_auxz00_5515,BFALSE,BFALSE);} 
return 
BGl_deletez00zz__r4_pairs_and_lists_6_3z00(BgL_g1107z00_1103, BgL_auxz00_5512, BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);} } break;case 3L : 

{ /* Ieee/pairlist.scm 775 */
 obj_t BgL_eqz00_1108;
BgL_eqz00_1108 = 
VECTOR_REF(BgL_opt1105z00_94,2L); 
{ /* Ieee/pairlist.scm 775 */

{ /* Ieee/pairlist.scm 775 */
 obj_t BgL_auxz00_5521;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1108z00_1104))
{ /* Ieee/pairlist.scm 775 */
BgL_auxz00_5521 = BgL_g1108z00_1104
; }  else 
{ 
 obj_t BgL_auxz00_5524;
BgL_auxz00_5524 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31076L), BGl_string2575z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1108z00_1104); 
FAILURE(BgL_auxz00_5524,BFALSE,BFALSE);} 
return 
BGl_deletez00zz__r4_pairs_and_lists_6_3z00(BgL_g1107z00_1103, BgL_auxz00_5521, BgL_eqz00_1108);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2572z00zz__r4_pairs_and_lists_6_3z00, BGl_string2574z00zz__r4_pairs_and_lists_6_3z00, 
BINT(
VECTOR_LENGTH(BgL_opt1105z00_94)));} } } } 

}



/* delete */
BGL_EXPORTED_DEF obj_t BGl_deletez00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_91, obj_t BgL_yz00_92, obj_t BgL_eqz00_93)
{
{ /* Ieee/pairlist.scm 775 */
return 
BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_93, BgL_xz00_91, BgL_yz00_92);} 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t BgL_eqz00_2496, obj_t BgL_xz00_1111, obj_t BgL_yz00_1112)
{
{ /* Ieee/pairlist.scm 776 */
BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00:
if(
NULLP(BgL_yz00_1112))
{ /* Ieee/pairlist.scm 779 */
return BgL_yz00_1112;}  else 
{ /* Ieee/pairlist.scm 780 */
 bool_t BgL_test3401z00_5537;
{ /* Ieee/pairlist.scm 780 */
 obj_t BgL_arg1479z00_1121;
{ /* Ieee/pairlist.scm 780 */
 obj_t BgL_pairz00_2067;
if(
PAIRP(BgL_yz00_1112))
{ /* Ieee/pairlist.scm 780 */
BgL_pairz00_2067 = BgL_yz00_1112; }  else 
{ 
 obj_t BgL_auxz00_5540;
BgL_auxz00_5540 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31196L), BGl_string2576z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_1112); 
FAILURE(BgL_auxz00_5540,BFALSE,BFALSE);} 
BgL_arg1479z00_1121 = 
CAR(BgL_pairz00_2067); } 
{ /* Ieee/pairlist.scm 780 */
 obj_t BgL_funz00_2922;
if(
PROCEDUREP(BgL_eqz00_2496))
{ /* Ieee/pairlist.scm 780 */
BgL_funz00_2922 = BgL_eqz00_2496; }  else 
{ 
 obj_t BgL_auxz00_5547;
BgL_auxz00_5547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31185L), BGl_string2576z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_eqz00_2496); 
FAILURE(BgL_auxz00_5547,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_2922, 2))
{ /* Ieee/pairlist.scm 780 */
BgL_test3401z00_5537 = 
CBOOL(
(VA_PROCEDUREP( BgL_funz00_2922 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_2922))(BgL_eqz00_2496, BgL_xz00_1111, BgL_arg1479z00_1121, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_2922))(BgL_eqz00_2496, BgL_xz00_1111, BgL_arg1479z00_1121) ))
; }  else 
{ /* Ieee/pairlist.scm 780 */
FAILURE(BGl_string2578z00zz__r4_pairs_and_lists_6_3z00,BGl_list2579z00zz__r4_pairs_and_lists_6_3z00,BgL_funz00_2922);} } } 
if(BgL_test3401z00_5537)
{ /* Ieee/pairlist.scm 780 */
 obj_t BgL_arg1474z00_1117;
{ /* Ieee/pairlist.scm 780 */
 obj_t BgL_pairz00_2068;
if(
PAIRP(BgL_yz00_1112))
{ /* Ieee/pairlist.scm 780 */
BgL_pairz00_2068 = BgL_yz00_1112; }  else 
{ 
 obj_t BgL_auxz00_5562;
BgL_auxz00_5562 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31213L), BGl_string2576z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_1112); 
FAILURE(BgL_auxz00_5562,BFALSE,BFALSE);} 
BgL_arg1474z00_1117 = 
CDR(BgL_pairz00_2068); } 
{ 
 obj_t BgL_yz00_5567;
BgL_yz00_5567 = BgL_arg1474z00_1117; 
BgL_yz00_1112 = BgL_yz00_5567; 
goto BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00;} }  else 
{ /* Ieee/pairlist.scm 781 */
 obj_t BgL_arg1476z00_1118; obj_t BgL_arg1477z00_1119;
{ /* Ieee/pairlist.scm 781 */
 obj_t BgL_pairz00_2069;
if(
PAIRP(BgL_yz00_1112))
{ /* Ieee/pairlist.scm 781 */
BgL_pairz00_2069 = BgL_yz00_1112; }  else 
{ 
 obj_t BgL_auxz00_5570;
BgL_auxz00_5570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31237L), BGl_string2576z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_1112); 
FAILURE(BgL_auxz00_5570,BFALSE,BFALSE);} 
BgL_arg1476z00_1118 = 
CAR(BgL_pairz00_2069); } 
{ /* Ieee/pairlist.scm 781 */
 obj_t BgL_arg1478z00_1120;
{ /* Ieee/pairlist.scm 781 */
 obj_t BgL_pairz00_2070;
if(
PAIRP(BgL_yz00_1112))
{ /* Ieee/pairlist.scm 781 */
BgL_pairz00_2070 = BgL_yz00_1112; }  else 
{ 
 obj_t BgL_auxz00_5577;
BgL_auxz00_5577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31253L), BGl_string2576z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_1112); 
FAILURE(BgL_auxz00_5577,BFALSE,BFALSE);} 
BgL_arg1478z00_1120 = 
CDR(BgL_pairz00_2070); } 
BgL_arg1477z00_1119 = 
BGl_loopze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_2496, BgL_xz00_1111, BgL_arg1478z00_1120); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1476z00_1118, BgL_arg1477z00_1119);} } } 

}



/* _delete! */
obj_t BGl__deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t BgL_env1112z00_100, obj_t BgL_opt1111z00_99)
{
{ /* Ieee/pairlist.scm 786 */
{ /* Ieee/pairlist.scm 786 */
 obj_t BgL_g1113z00_1123; obj_t BgL_g1114z00_1124;
BgL_g1113z00_1123 = 
VECTOR_REF(BgL_opt1111z00_99,0L); 
BgL_g1114z00_1124 = 
VECTOR_REF(BgL_opt1111z00_99,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1111z00_99)) { case 2L : 

{ /* Ieee/pairlist.scm 786 */

{ /* Ieee/pairlist.scm 786 */
 obj_t BgL_auxz00_5586;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1114z00_1124))
{ /* Ieee/pairlist.scm 786 */
BgL_auxz00_5586 = BgL_g1114z00_1124
; }  else 
{ 
 obj_t BgL_auxz00_5589;
BgL_auxz00_5589 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31485L), BGl_string2590z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1114z00_1124); 
FAILURE(BgL_auxz00_5589,BFALSE,BFALSE);} 
return 
BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(BgL_g1113z00_1123, BgL_auxz00_5586, BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);} } break;case 3L : 

{ /* Ieee/pairlist.scm 786 */
 obj_t BgL_eqz00_1128;
BgL_eqz00_1128 = 
VECTOR_REF(BgL_opt1111z00_99,2L); 
{ /* Ieee/pairlist.scm 786 */

{ /* Ieee/pairlist.scm 786 */
 obj_t BgL_auxz00_5595;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1114z00_1124))
{ /* Ieee/pairlist.scm 786 */
BgL_auxz00_5595 = BgL_g1114z00_1124
; }  else 
{ 
 obj_t BgL_auxz00_5598;
BgL_auxz00_5598 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31485L), BGl_string2590z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1114z00_1124); 
FAILURE(BgL_auxz00_5598,BFALSE,BFALSE);} 
return 
BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(BgL_g1113z00_1123, BgL_auxz00_5595, BgL_eqz00_1128);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2588z00zz__r4_pairs_and_lists_6_3z00, BGl_string2574z00zz__r4_pairs_and_lists_6_3z00, 
BINT(
VECTOR_LENGTH(BgL_opt1111z00_99)));} } } } 

}



/* delete! */
BGL_EXPORTED_DEF obj_t BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_96, obj_t BgL_yz00_97, obj_t BgL_eqz00_98)
{
{ /* Ieee/pairlist.scm 786 */
{ 
 obj_t BgL_xz00_1131; obj_t BgL_yz00_1132;
{ /* Ieee/pairlist.scm 787 */
 obj_t BgL_aux2325z00_2960;
BgL_xz00_1131 = BgL_xz00_96; 
BgL_yz00_1132 = BgL_yz00_97; 
BgL_zc3z04anonymousza31481ze3z87_1133:
if(
NULLP(BgL_yz00_1132))
{ /* Ieee/pairlist.scm 790 */
BgL_aux2325z00_2960 = BgL_yz00_1132; }  else 
{ /* Ieee/pairlist.scm 791 */
 bool_t BgL_test3411z00_5610;
{ /* Ieee/pairlist.scm 791 */
 obj_t BgL_arg1498z00_1150;
{ /* Ieee/pairlist.scm 791 */
 obj_t BgL_pairz00_2071;
if(
PAIRP(BgL_yz00_1132))
{ /* Ieee/pairlist.scm 791 */
BgL_pairz00_2071 = BgL_yz00_1132; }  else 
{ 
 obj_t BgL_auxz00_5613;
BgL_auxz00_5613 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31606L), BGl_string2591z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_1132); 
FAILURE(BgL_auxz00_5613,BFALSE,BFALSE);} 
BgL_arg1498z00_1150 = 
CAR(BgL_pairz00_2071); } 
{ /* Ieee/pairlist.scm 791 */
 obj_t BgL_funz00_2938;
if(
PROCEDUREP(BgL_eqz00_98))
{ /* Ieee/pairlist.scm 791 */
BgL_funz00_2938 = BgL_eqz00_98; }  else 
{ 
 obj_t BgL_auxz00_5620;
BgL_auxz00_5620 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31595L), BGl_string2591z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_eqz00_98); 
FAILURE(BgL_auxz00_5620,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_2938, 2))
{ /* Ieee/pairlist.scm 791 */
BgL_test3411z00_5610 = 
CBOOL(
(VA_PROCEDUREP( BgL_funz00_2938 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_2938))(BgL_eqz00_98, BgL_xz00_1131, BgL_arg1498z00_1150, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_2938))(BgL_eqz00_98, BgL_xz00_1131, BgL_arg1498z00_1150) ))
; }  else 
{ /* Ieee/pairlist.scm 791 */
FAILURE(BGl_string2592z00zz__r4_pairs_and_lists_6_3z00,BGl_list2593z00zz__r4_pairs_and_lists_6_3z00,BgL_funz00_2938);} } } 
if(BgL_test3411z00_5610)
{ /* Ieee/pairlist.scm 791 */
 obj_t BgL_arg1485z00_1137;
{ /* Ieee/pairlist.scm 791 */
 obj_t BgL_pairz00_2072;
if(
PAIRP(BgL_yz00_1132))
{ /* Ieee/pairlist.scm 791 */
BgL_pairz00_2072 = BgL_yz00_1132; }  else 
{ 
 obj_t BgL_auxz00_5635;
BgL_auxz00_5635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31623L), BGl_string2591z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_yz00_1132); 
FAILURE(BgL_auxz00_5635,BFALSE,BFALSE);} 
BgL_arg1485z00_1137 = 
CDR(BgL_pairz00_2072); } 
{ 
 obj_t BgL_yz00_5640;
BgL_yz00_5640 = BgL_arg1485z00_1137; 
BgL_yz00_1132 = BgL_yz00_5640; 
goto BgL_zc3z04anonymousza31481ze3z87_1133;} }  else 
{ 
 obj_t BgL_prevz00_1139;
BgL_prevz00_1139 = BgL_yz00_1132; 
BgL_zc3z04anonymousza31486ze3z87_1140:
{ /* Ieee/pairlist.scm 793 */
 bool_t BgL_test3416z00_5641;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5642;
{ /* Ieee/pairlist.scm 793 */
 obj_t BgL_pairz00_2073;
if(
PAIRP(BgL_prevz00_1139))
{ /* Ieee/pairlist.scm 793 */
BgL_pairz00_2073 = BgL_prevz00_1139; }  else 
{ 
 obj_t BgL_auxz00_5645;
BgL_auxz00_5645 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31680L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1139); 
FAILURE(BgL_auxz00_5645,BFALSE,BFALSE);} 
BgL_tmpz00_5642 = 
CDR(BgL_pairz00_2073); } 
BgL_test3416z00_5641 = 
NULLP(BgL_tmpz00_5642); } 
if(BgL_test3416z00_5641)
{ /* Ieee/pairlist.scm 793 */
BgL_aux2325z00_2960 = BgL_yz00_1132; }  else 
{ /* Ieee/pairlist.scm 795 */
 bool_t BgL_test3418z00_5651;
{ /* Ieee/pairlist.scm 795 */
 obj_t BgL_arg1495z00_1147;
{ /* Ieee/pairlist.scm 795 */
 obj_t BgL_pairz00_2074;
if(
PAIRP(BgL_prevz00_1139))
{ /* Ieee/pairlist.scm 795 */
BgL_pairz00_2074 = BgL_prevz00_1139; }  else 
{ 
 obj_t BgL_auxz00_5654;
BgL_auxz00_5654 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31708L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1139); 
FAILURE(BgL_auxz00_5654,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 285 */
 obj_t BgL_pairz00_2077;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2312z00_2946;
BgL_aux2312z00_2946 = 
CDR(BgL_pairz00_2074); 
if(
PAIRP(BgL_aux2312z00_2946))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_2077 = BgL_aux2312z00_2946; }  else 
{ 
 obj_t BgL_auxz00_5661;
BgL_auxz00_5661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2312z00_2946); 
FAILURE(BgL_auxz00_5661,BFALSE,BFALSE);} } 
BgL_arg1495z00_1147 = 
CAR(BgL_pairz00_2077); } } 
{ /* Ieee/pairlist.scm 795 */
 obj_t BgL_funz00_2950;
if(
PROCEDUREP(BgL_eqz00_98))
{ /* Ieee/pairlist.scm 795 */
BgL_funz00_2950 = BgL_eqz00_98; }  else 
{ 
 obj_t BgL_auxz00_5668;
BgL_auxz00_5668 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31698L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_eqz00_98); 
FAILURE(BgL_auxz00_5668,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_2950, 2))
{ /* Ieee/pairlist.scm 795 */
BgL_test3418z00_5651 = 
CBOOL(
(VA_PROCEDUREP( BgL_funz00_2950 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_2950))(BgL_eqz00_98, BgL_arg1495z00_1147, BgL_xz00_1131, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_2950))(BgL_eqz00_98, BgL_arg1495z00_1147, BgL_xz00_1131) ))
; }  else 
{ /* Ieee/pairlist.scm 795 */
FAILURE(BGl_string2596z00zz__r4_pairs_and_lists_6_3z00,BGl_list2597z00zz__r4_pairs_and_lists_6_3z00,BgL_funz00_2950);} } } 
if(BgL_test3418z00_5651)
{ /* Ieee/pairlist.scm 795 */
{ /* Ieee/pairlist.scm 796 */
 obj_t BgL_arg1492z00_1145;
{ /* Ieee/pairlist.scm 796 */
 obj_t BgL_pairz00_2078;
if(
PAIRP(BgL_prevz00_1139))
{ /* Ieee/pairlist.scm 796 */
BgL_pairz00_2078 = BgL_prevz00_1139; }  else 
{ 
 obj_t BgL_auxz00_5683;
BgL_auxz00_5683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31742L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1139); 
FAILURE(BgL_auxz00_5683,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 297 */
 obj_t BgL_pairz00_2081;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2319z00_2954;
BgL_aux2319z00_2954 = 
CDR(BgL_pairz00_2078); 
if(
PAIRP(BgL_aux2319z00_2954))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_2081 = BgL_aux2319z00_2954; }  else 
{ 
 obj_t BgL_auxz00_5690;
BgL_auxz00_5690 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2319z00_2954); 
FAILURE(BgL_auxz00_5690,BFALSE,BFALSE);} } 
BgL_arg1492z00_1145 = 
CDR(BgL_pairz00_2081); } } 
{ /* Ieee/pairlist.scm 796 */
 obj_t BgL_pairz00_2082;
if(
PAIRP(BgL_prevz00_1139))
{ /* Ieee/pairlist.scm 796 */
BgL_pairz00_2082 = BgL_prevz00_1139; }  else 
{ 
 obj_t BgL_auxz00_5697;
BgL_auxz00_5697 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31731L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1139); 
FAILURE(BgL_auxz00_5697,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2082, BgL_arg1492z00_1145); } } 
{ 

goto BgL_zc3z04anonymousza31486ze3z87_1140;} }  else 
{ /* Ieee/pairlist.scm 798 */
 obj_t BgL_arg1494z00_1146;
{ /* Ieee/pairlist.scm 798 */
 obj_t BgL_pairz00_2083;
if(
PAIRP(BgL_prevz00_1139))
{ /* Ieee/pairlist.scm 798 */
BgL_pairz00_2083 = BgL_prevz00_1139; }  else 
{ 
 obj_t BgL_auxz00_5704;
BgL_auxz00_5704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31786L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_prevz00_1139); 
FAILURE(BgL_auxz00_5704,BFALSE,BFALSE);} 
BgL_arg1494z00_1146 = 
CDR(BgL_pairz00_2083); } 
{ 
 obj_t BgL_prevz00_5709;
BgL_prevz00_5709 = BgL_arg1494z00_1146; 
BgL_prevz00_1139 = BgL_prevz00_5709; 
goto BgL_zc3z04anonymousza31486ze3z87_1140;} } } } } } 
{ /* Ieee/pairlist.scm 787 */
 bool_t BgL_test3427z00_5710;
if(
PAIRP(BgL_aux2325z00_2960))
{ /* Ieee/pairlist.scm 241 */
BgL_test3427z00_5710 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3427z00_5710 = 
NULLP(BgL_aux2325z00_2960)
; } 
if(BgL_test3427z00_5710)
{ /* Ieee/pairlist.scm 787 */
return BgL_aux2325z00_2960;}  else 
{ 
 obj_t BgL_auxz00_5714;
BgL_auxz00_5714 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(31533L), BGl_string2589z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2325z00_2960); 
FAILURE(BgL_auxz00_5714,BFALSE,BFALSE);} } } } } 

}



/* cons* */
BGL_EXPORTED_DEF obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_101, obj_t BgL_yz00_102)
{
{ /* Ieee/pairlist.scm 803 */
if(
NULLP(BgL_yz00_102))
{ /* Ieee/pairlist.scm 808 */
return BgL_xz00_101;}  else 
{ /* Ieee/pairlist.scm 808 */
return 
MAKE_YOUNG_PAIR(BgL_xz00_101, 
BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(BgL_yz00_102));} } 

}



/* cons*1~0 */
obj_t BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(obj_t BgL_xz00_1152)
{
{ /* Ieee/pairlist.scm 804 */
{ /* Ieee/pairlist.scm 804 */
 bool_t BgL_test3430z00_5722;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5723;
{ /* Ieee/pairlist.scm 804 */
 obj_t BgL_pairz00_2084;
if(
PAIRP(BgL_xz00_1152))
{ /* Ieee/pairlist.scm 804 */
BgL_pairz00_2084 = BgL_xz00_1152; }  else 
{ 
 obj_t BgL_auxz00_5726;
BgL_auxz00_5726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32088L), BGl_string2600z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_1152); 
FAILURE(BgL_auxz00_5726,BFALSE,BFALSE);} 
BgL_tmpz00_5723 = 
CDR(BgL_pairz00_2084); } 
BgL_test3430z00_5722 = 
NULLP(BgL_tmpz00_5723); } 
if(BgL_test3430z00_5722)
{ /* Ieee/pairlist.scm 805 */
 obj_t BgL_pairz00_2085;
if(
PAIRP(BgL_xz00_1152))
{ /* Ieee/pairlist.scm 805 */
BgL_pairz00_2085 = BgL_xz00_1152; }  else 
{ 
 obj_t BgL_auxz00_5734;
BgL_auxz00_5734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32107L), BGl_string2600z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_1152); 
FAILURE(BgL_auxz00_5734,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_2085);}  else 
{ /* Ieee/pairlist.scm 807 */
 obj_t BgL_arg1503z00_1158; obj_t BgL_arg1504z00_1159;
{ /* Ieee/pairlist.scm 807 */
 obj_t BgL_pairz00_2086;
if(
PAIRP(BgL_xz00_1152))
{ /* Ieee/pairlist.scm 807 */
BgL_pairz00_2086 = BgL_xz00_1152; }  else 
{ 
 obj_t BgL_auxz00_5741;
BgL_auxz00_5741 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32147L), BGl_string2600z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_1152); 
FAILURE(BgL_auxz00_5741,BFALSE,BFALSE);} 
BgL_arg1503z00_1158 = 
CAR(BgL_pairz00_2086); } 
{ /* Ieee/pairlist.scm 807 */
 obj_t BgL_arg1505z00_1160;
{ /* Ieee/pairlist.scm 807 */
 obj_t BgL_pairz00_2087;
if(
PAIRP(BgL_xz00_1152))
{ /* Ieee/pairlist.scm 807 */
BgL_pairz00_2087 = BgL_xz00_1152; }  else 
{ 
 obj_t BgL_auxz00_5748;
BgL_auxz00_5748 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32163L), BGl_string2600z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_xz00_1152); 
FAILURE(BgL_auxz00_5748,BFALSE,BFALSE);} 
BgL_arg1505z00_1160 = 
CDR(BgL_pairz00_2087); } 
BgL_arg1504z00_1159 = 
BGl_consza21ze70z45zz__r4_pairs_and_lists_6_3z00(BgL_arg1505z00_1160); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1503z00_1158, BgL_arg1504z00_1159);} } } 

}



/* &cons* */
obj_t BGl_z62consza2zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2453, obj_t BgL_xz00_2454, obj_t BgL_yz00_2455)
{
{ /* Ieee/pairlist.scm 803 */
return 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_xz00_2454, BgL_yz00_2455);} 

}



/* reverse! */
BGL_EXPORTED_DEF obj_t bgl_reverse_bang(obj_t BgL_lz00_103)
{
{ /* Ieee/pairlist.scm 815 */
if(
NULLP(BgL_lz00_103))
{ /* Ieee/pairlist.scm 816 */
return BgL_lz00_103;}  else 
{ 
 obj_t BgL_lz00_1165; obj_t BgL_rz00_1166;
{ /* Ieee/pairlist.scm 817 */
 obj_t BgL_aux2343z00_2978;
BgL_lz00_1165 = BgL_lz00_103; 
BgL_rz00_1166 = BNIL; 
BgL_zc3z04anonymousza31508ze3z87_1167:
{ /* Ieee/pairlist.scm 819 */
 bool_t BgL_test3436z00_5758;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5759;
{ /* Ieee/pairlist.scm 819 */
 obj_t BgL_pairz00_2088;
if(
PAIRP(BgL_lz00_1165))
{ /* Ieee/pairlist.scm 819 */
BgL_pairz00_2088 = BgL_lz00_1165; }  else 
{ 
 obj_t BgL_auxz00_5762;
BgL_auxz00_5762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32550L), BGl_string2601z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1165); 
FAILURE(BgL_auxz00_5762,BFALSE,BFALSE);} 
BgL_tmpz00_5759 = 
CDR(BgL_pairz00_2088); } 
BgL_test3436z00_5758 = 
NULLP(BgL_tmpz00_5759); } 
if(BgL_test3436z00_5758)
{ /* Ieee/pairlist.scm 819 */
{ /* Ieee/pairlist.scm 821 */
 obj_t BgL_pairz00_2089;
if(
PAIRP(BgL_lz00_1165))
{ /* Ieee/pairlist.scm 821 */
BgL_pairz00_2089 = BgL_lz00_1165; }  else 
{ 
 obj_t BgL_auxz00_5770;
BgL_auxz00_5770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32602L), BGl_string2601z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1165); 
FAILURE(BgL_auxz00_5770,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2089, BgL_rz00_1166); } 
BgL_aux2343z00_2978 = BgL_lz00_1165; }  else 
{ /* Ieee/pairlist.scm 823 */
 obj_t BgL_cdrlz00_1170;
{ /* Ieee/pairlist.scm 823 */
 obj_t BgL_pairz00_2090;
if(
PAIRP(BgL_lz00_1165))
{ /* Ieee/pairlist.scm 823 */
BgL_pairz00_2090 = BgL_lz00_1165; }  else 
{ 
 obj_t BgL_auxz00_5777;
BgL_auxz00_5777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32651L), BGl_string2601z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1165); 
FAILURE(BgL_auxz00_5777,BFALSE,BFALSE);} 
BgL_cdrlz00_1170 = 
CDR(BgL_pairz00_2090); } 
{ /* Ieee/pairlist.scm 825 */
 obj_t BgL_arg1511z00_1171;
{ /* Ieee/pairlist.scm 825 */
 obj_t BgL_pairz00_2091;
if(
PAIRP(BgL_lz00_1165))
{ /* Ieee/pairlist.scm 825 */
BgL_pairz00_2091 = BgL_lz00_1165; }  else 
{ 
 obj_t BgL_auxz00_5784;
BgL_auxz00_5784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32692L), BGl_string2601z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1165); 
FAILURE(BgL_auxz00_5784,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2091, BgL_rz00_1166); } 
BgL_arg1511z00_1171 = BgL_lz00_1165; 
{ 
 obj_t BgL_rz00_5790; obj_t BgL_lz00_5789;
BgL_lz00_5789 = BgL_cdrlz00_1170; 
BgL_rz00_5790 = BgL_arg1511z00_1171; 
BgL_rz00_1166 = BgL_rz00_5790; 
BgL_lz00_1165 = BgL_lz00_5789; 
goto BgL_zc3z04anonymousza31508ze3z87_1167;} } } } 
{ /* Ieee/pairlist.scm 817 */
 bool_t BgL_test3441z00_5791;
if(
PAIRP(BgL_aux2343z00_2978))
{ /* Ieee/pairlist.scm 241 */
BgL_test3441z00_5791 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3441z00_5791 = 
NULLP(BgL_aux2343z00_2978)
; } 
if(BgL_test3441z00_5791)
{ /* Ieee/pairlist.scm 817 */
return BgL_aux2343z00_2978;}  else 
{ 
 obj_t BgL_auxz00_5795;
BgL_auxz00_5795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32498L), BGl_string2602z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2343z00_2978); 
FAILURE(BgL_auxz00_5795,BFALSE,BFALSE);} } } } } 

}



/* &reverse! */
obj_t BGl_z62reversez12z70zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2456, obj_t BgL_lz00_2457)
{
{ /* Ieee/pairlist.scm 815 */
{ /* Ieee/pairlist.scm 816 */
 obj_t BgL_auxz00_5799;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2457))
{ /* Ieee/pairlist.scm 816 */
BgL_auxz00_5799 = BgL_lz00_2457
; }  else 
{ 
 obj_t BgL_auxz00_5802;
BgL_auxz00_5802 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32477L), BGl_string2603z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2457); 
FAILURE(BgL_auxz00_5802,BFALSE,BFALSE);} 
return 
bgl_reverse_bang(BgL_auxz00_5799);} } 

}



/* every */
BGL_EXPORTED_DEF obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_predz00_104, obj_t BgL_lz00_105)
{
{ /* Ieee/pairlist.scm 831 */
if(
NULLP(BgL_lz00_105))
{ /* Ieee/pairlist.scm 833 */
return BTRUE;}  else 
{ /* Ieee/pairlist.scm 835 */
 bool_t BgL_test3445z00_5809;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5810;
{ /* Ieee/pairlist.scm 835 */
 obj_t BgL_pairz00_2092;
if(
PAIRP(BgL_lz00_105))
{ /* Ieee/pairlist.scm 835 */
BgL_pairz00_2092 = BgL_lz00_105; }  else 
{ 
 obj_t BgL_auxz00_5813;
BgL_auxz00_5813 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33019L), BGl_string2604z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_105); 
FAILURE(BgL_auxz00_5813,BFALSE,BFALSE);} 
BgL_tmpz00_5810 = 
CDR(BgL_pairz00_2092); } 
BgL_test3445z00_5809 = 
NULLP(BgL_tmpz00_5810); } 
if(BgL_test3445z00_5809)
{ /* Ieee/pairlist.scm 836 */
 obj_t BgL_g1025z00_1177;
{ /* Ieee/pairlist.scm 836 */
 obj_t BgL_pairz00_2093;
if(
PAIRP(BgL_lz00_105))
{ /* Ieee/pairlist.scm 836 */
BgL_pairz00_2093 = BgL_lz00_105; }  else 
{ 
 obj_t BgL_auxz00_5821;
BgL_auxz00_5821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33049L), BGl_string2604z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_105); 
FAILURE(BgL_auxz00_5821,BFALSE,BFALSE);} 
BgL_g1025z00_1177 = 
CAR(BgL_pairz00_2093); } 
{ 
 obj_t BgL_lz00_1179;
{ /* Ieee/pairlist.scm 836 */
 bool_t BgL_tmpz00_5826;
BgL_lz00_1179 = BgL_g1025z00_1177; 
BgL_zc3z04anonymousza31518ze3z87_1180:
{ /* Ieee/pairlist.scm 837 */
 bool_t BgL__ortest_1026z00_1181;
BgL__ortest_1026z00_1181 = 
NULLP(BgL_lz00_1179); 
if(BgL__ortest_1026z00_1181)
{ /* Ieee/pairlist.scm 837 */
BgL_tmpz00_5826 = BgL__ortest_1026z00_1181
; }  else 
{ /* Ieee/pairlist.scm 838 */
 obj_t BgL__andtest_1027z00_1182;
{ /* Ieee/pairlist.scm 838 */
 obj_t BgL_arg1522z00_1184;
{ /* Ieee/pairlist.scm 838 */
 obj_t BgL_pairz00_2094;
if(
PAIRP(BgL_lz00_1179))
{ /* Ieee/pairlist.scm 838 */
BgL_pairz00_2094 = BgL_lz00_1179; }  else 
{ 
 obj_t BgL_auxz00_5831;
BgL_auxz00_5831 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33094L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1179); 
FAILURE(BgL_auxz00_5831,BFALSE,BFALSE);} 
BgL_arg1522z00_1184 = 
CAR(BgL_pairz00_2094); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_104, 1))
{ /* Ieee/pairlist.scm 838 */
BgL__andtest_1027z00_1182 = 
BGL_PROCEDURE_CALL1(BgL_predz00_104, BgL_arg1522z00_1184); }  else 
{ /* Ieee/pairlist.scm 838 */
FAILURE(BGl_string2596z00zz__r4_pairs_and_lists_6_3z00,BGl_list2605z00zz__r4_pairs_and_lists_6_3z00,BgL_predz00_104);} } 
if(
CBOOL(BgL__andtest_1027z00_1182))
{ /* Ieee/pairlist.scm 838 */
 obj_t BgL_arg1521z00_1183;
{ /* Ieee/pairlist.scm 838 */
 obj_t BgL_pairz00_2095;
if(
PAIRP(BgL_lz00_1179))
{ /* Ieee/pairlist.scm 838 */
BgL_pairz00_2095 = BgL_lz00_1179; }  else 
{ 
 obj_t BgL_auxz00_5847;
BgL_auxz00_5847 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33109L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1179); 
FAILURE(BgL_auxz00_5847,BFALSE,BFALSE);} 
BgL_arg1521z00_1183 = 
CDR(BgL_pairz00_2095); } 
{ 
 obj_t BgL_lz00_5852;
BgL_lz00_5852 = BgL_arg1521z00_1183; 
BgL_lz00_1179 = BgL_lz00_5852; 
goto BgL_zc3z04anonymousza31518ze3z87_1180;} }  else 
{ /* Ieee/pairlist.scm 838 */
BgL_tmpz00_5826 = ((bool_t)0)
; } } } 
return 
BBOOL(BgL_tmpz00_5826);} } }  else 
{ 
 obj_t BgL_lz00_1187;
{ /* Ieee/pairlist.scm 840 */
 bool_t BgL_tmpz00_5854;
BgL_lz00_1187 = BgL_lz00_105; 
BgL_zc3z04anonymousza31523ze3z87_1188:
{ /* Ieee/pairlist.scm 841 */
 bool_t BgL__ortest_1028z00_1189;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5855;
{ /* Ieee/pairlist.scm 841 */
 obj_t BgL_pairz00_2096;
if(
PAIRP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 841 */
BgL_pairz00_2096 = BgL_lz00_1187; }  else 
{ 
 obj_t BgL_auxz00_5858;
BgL_auxz00_5858 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33173L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1187); 
FAILURE(BgL_auxz00_5858,BFALSE,BFALSE);} 
BgL_tmpz00_5855 = 
CAR(BgL_pairz00_2096); } 
BgL__ortest_1028z00_1189 = 
NULLP(BgL_tmpz00_5855); } 
if(BgL__ortest_1028z00_1189)
{ /* Ieee/pairlist.scm 841 */
BgL_tmpz00_5854 = BgL__ortest_1028z00_1189
; }  else 
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL__andtest_1029z00_1190;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_valz00_3003;
if(
NULLP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 842 */
BgL_valz00_3003 = BNIL; }  else 
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_head1074z00_1211;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_arg1546z00_1224;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2098;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2097;
if(
PAIRP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 842 */
BgL_pairz00_2097 = BgL_lz00_1187; }  else 
{ 
 obj_t BgL_auxz00_5869;
BgL_auxz00_5869 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33201L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1187); 
FAILURE(BgL_auxz00_5869,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2360z00_2996;
BgL_aux2360z00_2996 = 
CAR(BgL_pairz00_2097); 
if(
PAIRP(BgL_aux2360z00_2996))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2098 = BgL_aux2360z00_2996; }  else 
{ 
 obj_t BgL_auxz00_5876;
BgL_auxz00_5876 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2360z00_2996); 
FAILURE(BgL_auxz00_5876,BFALSE,BFALSE);} } } 
BgL_arg1546z00_1224 = 
CAR(BgL_pairz00_2098); } 
BgL_head1074z00_1211 = 
MAKE_YOUNG_PAIR(BgL_arg1546z00_1224, BNIL); } 
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_g1077z00_1212;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2099;
if(
PAIRP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 842 */
BgL_pairz00_2099 = BgL_lz00_1187; }  else 
{ 
 obj_t BgL_auxz00_5884;
BgL_auxz00_5884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33201L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1187); 
FAILURE(BgL_auxz00_5884,BFALSE,BFALSE);} 
BgL_g1077z00_1212 = 
CDR(BgL_pairz00_2099); } 
{ 
 obj_t BgL_l1072z00_1214; obj_t BgL_tail1075z00_1215;
BgL_l1072z00_1214 = BgL_g1077z00_1212; 
BgL_tail1075z00_1215 = BgL_head1074z00_1211; 
BgL_zc3z04anonymousza31538ze3z87_1216:
if(
PAIRP(BgL_l1072z00_1214))
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_newtail1076z00_1218;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_arg1543z00_1220;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2101;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2364z00_3000;
BgL_aux2364z00_3000 = 
CAR(BgL_l1072z00_1214); 
if(
PAIRP(BgL_aux2364z00_3000))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2101 = BgL_aux2364z00_3000; }  else 
{ 
 obj_t BgL_auxz00_5894;
BgL_auxz00_5894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2610z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2364z00_3000); 
FAILURE(BgL_auxz00_5894,BFALSE,BFALSE);} } 
BgL_arg1543z00_1220 = 
CAR(BgL_pairz00_2101); } 
BgL_newtail1076z00_1218 = 
MAKE_YOUNG_PAIR(BgL_arg1543z00_1220, BNIL); } 
SET_CDR(BgL_tail1075z00_1215, BgL_newtail1076z00_1218); 
{ 
 obj_t BgL_tail1075z00_5903; obj_t BgL_l1072z00_5901;
BgL_l1072z00_5901 = 
CDR(BgL_l1072z00_1214); 
BgL_tail1075z00_5903 = BgL_newtail1076z00_1218; 
BgL_tail1075z00_1215 = BgL_tail1075z00_5903; 
BgL_l1072z00_1214 = BgL_l1072z00_5901; 
goto BgL_zc3z04anonymousza31538ze3z87_1216;} }  else 
{ /* Ieee/pairlist.scm 842 */
if(
NULLP(BgL_l1072z00_1214))
{ /* Ieee/pairlist.scm 842 */
BgL_valz00_3003 = BgL_head1074z00_1211; }  else 
{ /* Ieee/pairlist.scm 842 */
BgL_valz00_3003 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, BgL_l1072z00_1214, BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33201L)); } } } } } 
{ /* Ieee/pairlist.scm 842 */
 int BgL_len2366z00_3004;
BgL_len2366z00_3004 = 
(int)(
bgl_list_length(BgL_valz00_3003)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_104, BgL_len2366z00_3004))
{ /* Ieee/pairlist.scm 842 */
BgL__andtest_1029z00_1190 = 
apply(BgL_predz00_104, BgL_valz00_3003); }  else 
{ /* Ieee/pairlist.scm 842 */
FAILURE(BGl_symbol2612z00zz__r4_pairs_and_lists_6_3z00,BGl_string2613z00zz__r4_pairs_and_lists_6_3z00,BGl_list2614z00zz__r4_pairs_and_lists_6_3z00);} } } 
if(
CBOOL(BgL__andtest_1029z00_1190))
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_arg1524z00_1191;
if(
NULLP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 842 */
BgL_arg1524z00_1191 = BNIL; }  else 
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_head1080z00_1194;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_arg1535z00_1207;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2105;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2104;
if(
PAIRP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 842 */
BgL_pairz00_2104 = BgL_lz00_1187; }  else 
{ 
 obj_t BgL_auxz00_5921;
BgL_auxz00_5921 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33220L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1187); 
FAILURE(BgL_auxz00_5921,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2369z00_3007;
BgL_aux2369z00_3007 = 
CAR(BgL_pairz00_2104); 
if(
PAIRP(BgL_aux2369z00_3007))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2105 = BgL_aux2369z00_3007; }  else 
{ 
 obj_t BgL_auxz00_5928;
BgL_auxz00_5928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2369z00_3007); 
FAILURE(BgL_auxz00_5928,BFALSE,BFALSE);} } } 
BgL_arg1535z00_1207 = 
CDR(BgL_pairz00_2105); } 
BgL_head1080z00_1194 = 
MAKE_YOUNG_PAIR(BgL_arg1535z00_1207, BNIL); } 
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_g1083z00_1195;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2106;
if(
PAIRP(BgL_lz00_1187))
{ /* Ieee/pairlist.scm 842 */
BgL_pairz00_2106 = BgL_lz00_1187; }  else 
{ 
 obj_t BgL_auxz00_5936;
BgL_auxz00_5936 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33220L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1187); 
FAILURE(BgL_auxz00_5936,BFALSE,BFALSE);} 
BgL_g1083z00_1195 = 
CDR(BgL_pairz00_2106); } 
{ 
 obj_t BgL_l1078z00_1197; obj_t BgL_tail1081z00_1198;
BgL_l1078z00_1197 = BgL_g1083z00_1195; 
BgL_tail1081z00_1198 = BgL_head1080z00_1194; 
BgL_zc3z04anonymousza31526ze3z87_1199:
if(
PAIRP(BgL_l1078z00_1197))
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_newtail1082z00_1201;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_arg1529z00_1203;
{ /* Ieee/pairlist.scm 842 */
 obj_t BgL_pairz00_2108;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2373z00_3011;
BgL_aux2373z00_3011 = 
CAR(BgL_l1078z00_1197); 
if(
PAIRP(BgL_aux2373z00_3011))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2108 = BgL_aux2373z00_3011; }  else 
{ 
 obj_t BgL_auxz00_5946;
BgL_auxz00_5946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2717z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2373z00_3011); 
FAILURE(BgL_auxz00_5946,BFALSE,BFALSE);} } 
BgL_arg1529z00_1203 = 
CDR(BgL_pairz00_2108); } 
BgL_newtail1082z00_1201 = 
MAKE_YOUNG_PAIR(BgL_arg1529z00_1203, BNIL); } 
SET_CDR(BgL_tail1081z00_1198, BgL_newtail1082z00_1201); 
{ 
 obj_t BgL_tail1081z00_5955; obj_t BgL_l1078z00_5953;
BgL_l1078z00_5953 = 
CDR(BgL_l1078z00_1197); 
BgL_tail1081z00_5955 = BgL_newtail1082z00_1201; 
BgL_tail1081z00_1198 = BgL_tail1081z00_5955; 
BgL_l1078z00_1197 = BgL_l1078z00_5953; 
goto BgL_zc3z04anonymousza31526ze3z87_1199;} }  else 
{ /* Ieee/pairlist.scm 842 */
if(
NULLP(BgL_l1078z00_1197))
{ /* Ieee/pairlist.scm 842 */
BgL_arg1524z00_1191 = BgL_head1080z00_1194; }  else 
{ /* Ieee/pairlist.scm 842 */
BgL_arg1524z00_1191 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, BgL_l1078z00_1197, BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33220L)); } } } } } 
{ 
 obj_t BgL_lz00_5960;
BgL_lz00_5960 = BgL_arg1524z00_1191; 
BgL_lz00_1187 = BgL_lz00_5960; 
goto BgL_zc3z04anonymousza31523ze3z87_1188;} }  else 
{ /* Ieee/pairlist.scm 842 */
BgL_tmpz00_5854 = ((bool_t)0)
; } } } 
return 
BBOOL(BgL_tmpz00_5854);} } } } 

}



/* &every */
obj_t BGl_z62everyz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2458, obj_t BgL_predz00_2459, obj_t BgL_lz00_2460)
{
{ /* Ieee/pairlist.scm 831 */
{ /* Ieee/pairlist.scm 833 */
 obj_t BgL_auxz00_5962;
if(
PROCEDUREP(BgL_predz00_2459))
{ /* Ieee/pairlist.scm 833 */
BgL_auxz00_5962 = BgL_predz00_2459
; }  else 
{ 
 obj_t BgL_auxz00_5965;
BgL_auxz00_5965 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(32978L), BGl_string2718z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2459); 
FAILURE(BgL_auxz00_5965,BFALSE,BFALSE);} 
return 
BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_5962, BgL_lz00_2460);} } 

}



/* any */
BGL_EXPORTED_DEF obj_t BGl_anyz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_predz00_106, obj_t BgL_lz00_107)
{
{ /* Ieee/pairlist.scm 847 */
if(
NULLP(BgL_lz00_107))
{ /* Ieee/pairlist.scm 849 */
return BFALSE;}  else 
{ /* Ieee/pairlist.scm 851 */
 bool_t BgL_test3473z00_5972;
{ /* Ieee/pairlist.scm 466 */
 obj_t BgL_tmpz00_5973;
{ /* Ieee/pairlist.scm 851 */
 obj_t BgL_pairz00_2111;
if(
PAIRP(BgL_lz00_107))
{ /* Ieee/pairlist.scm 851 */
BgL_pairz00_2111 = BgL_lz00_107; }  else 
{ 
 obj_t BgL_auxz00_5976;
BgL_auxz00_5976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33541L), BGl_string2719z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_107); 
FAILURE(BgL_auxz00_5976,BFALSE,BFALSE);} 
BgL_tmpz00_5973 = 
CDR(BgL_pairz00_2111); } 
BgL_test3473z00_5972 = 
NULLP(BgL_tmpz00_5973); } 
if(BgL_test3473z00_5972)
{ /* Ieee/pairlist.scm 852 */
 obj_t BgL_g1030z00_1232;
{ /* Ieee/pairlist.scm 852 */
 obj_t BgL_pairz00_2112;
if(
PAIRP(BgL_lz00_107))
{ /* Ieee/pairlist.scm 852 */
BgL_pairz00_2112 = BgL_lz00_107; }  else 
{ 
 obj_t BgL_auxz00_5984;
BgL_auxz00_5984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33571L), BGl_string2719z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_107); 
FAILURE(BgL_auxz00_5984,BFALSE,BFALSE);} 
BgL_g1030z00_1232 = 
CAR(BgL_pairz00_2112); } 
{ 
 obj_t BgL_lz00_1234;
BgL_lz00_1234 = BgL_g1030z00_1232; 
BgL_zc3z04anonymousza31556ze3z87_1235:
if(
PAIRP(BgL_lz00_1234))
{ /* Ieee/pairlist.scm 854 */
 obj_t BgL__ortest_1032z00_1237;
{ /* Ieee/pairlist.scm 854 */
 obj_t BgL_arg1558z00_1239;
BgL_arg1558z00_1239 = 
CAR(BgL_lz00_1234); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_106, 1))
{ /* Ieee/pairlist.scm 854 */
BgL__ortest_1032z00_1237 = 
BGL_PROCEDURE_CALL1(BgL_predz00_106, BgL_arg1558z00_1239); }  else 
{ /* Ieee/pairlist.scm 854 */
FAILURE(BGl_string2596z00zz__r4_pairs_and_lists_6_3z00,BGl_list2720z00zz__r4_pairs_and_lists_6_3z00,BgL_predz00_106);} } 
if(
CBOOL(BgL__ortest_1032z00_1237))
{ /* Ieee/pairlist.scm 854 */
return BgL__ortest_1032z00_1237;}  else 
{ 
 obj_t BgL_lz00_6001;
BgL_lz00_6001 = 
CDR(BgL_lz00_1234); 
BgL_lz00_1234 = BgL_lz00_6001; 
goto BgL_zc3z04anonymousza31556ze3z87_1235;} }  else 
{ /* Ieee/pairlist.scm 853 */
return BFALSE;} } }  else 
{ 
 obj_t BgL_lz00_1242;
BgL_lz00_1242 = BgL_lz00_107; 
BgL_zc3z04anonymousza31559ze3z87_1243:
{ /* Ieee/pairlist.scm 857 */
 bool_t BgL_test3479z00_6003;
{ /* Ieee/pairlist.scm 229 */
 obj_t BgL_tmpz00_6004;
{ /* Ieee/pairlist.scm 857 */
 obj_t BgL_pairz00_2115;
if(
PAIRP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 857 */
BgL_pairz00_2115 = BgL_lz00_1242; }  else 
{ 
 obj_t BgL_auxz00_6007;
BgL_auxz00_6007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33697L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1242); 
FAILURE(BgL_auxz00_6007,BFALSE,BFALSE);} 
BgL_tmpz00_6004 = 
CAR(BgL_pairz00_2115); } 
BgL_test3479z00_6003 = 
PAIRP(BgL_tmpz00_6004); } 
if(BgL_test3479z00_6003)
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL__ortest_1034z00_1245;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_valz00_3032;
if(
NULLP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 858 */
BgL_valz00_3032 = BNIL; }  else 
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_head1086z00_1266;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_arg1584z00_1279;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2117;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2116;
if(
PAIRP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 858 */
BgL_pairz00_2116 = BgL_lz00_1242; }  else 
{ 
 obj_t BgL_auxz00_6017;
BgL_auxz00_6017 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33725L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1242); 
FAILURE(BgL_auxz00_6017,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2386z00_3025;
BgL_aux2386z00_3025 = 
CAR(BgL_pairz00_2116); 
if(
PAIRP(BgL_aux2386z00_3025))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2117 = BgL_aux2386z00_3025; }  else 
{ 
 obj_t BgL_auxz00_6024;
BgL_auxz00_6024 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2386z00_3025); 
FAILURE(BgL_auxz00_6024,BFALSE,BFALSE);} } } 
BgL_arg1584z00_1279 = 
CAR(BgL_pairz00_2117); } 
BgL_head1086z00_1266 = 
MAKE_YOUNG_PAIR(BgL_arg1584z00_1279, BNIL); } 
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_g1089z00_1267;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2118;
if(
PAIRP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 858 */
BgL_pairz00_2118 = BgL_lz00_1242; }  else 
{ 
 obj_t BgL_auxz00_6032;
BgL_auxz00_6032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33725L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1242); 
FAILURE(BgL_auxz00_6032,BFALSE,BFALSE);} 
BgL_g1089z00_1267 = 
CDR(BgL_pairz00_2118); } 
{ 
 obj_t BgL_l1084z00_1269; obj_t BgL_tail1087z00_1270;
BgL_l1084z00_1269 = BgL_g1089z00_1267; 
BgL_tail1087z00_1270 = BgL_head1086z00_1266; 
BgL_zc3z04anonymousza31577ze3z87_1271:
if(
PAIRP(BgL_l1084z00_1269))
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_newtail1088z00_1273;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_arg1580z00_1275;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2120;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2390z00_3029;
BgL_aux2390z00_3029 = 
CAR(BgL_l1084z00_1269); 
if(
PAIRP(BgL_aux2390z00_3029))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2120 = BgL_aux2390z00_3029; }  else 
{ 
 obj_t BgL_auxz00_6042;
BgL_auxz00_6042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2723z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2390z00_3029); 
FAILURE(BgL_auxz00_6042,BFALSE,BFALSE);} } 
BgL_arg1580z00_1275 = 
CAR(BgL_pairz00_2120); } 
BgL_newtail1088z00_1273 = 
MAKE_YOUNG_PAIR(BgL_arg1580z00_1275, BNIL); } 
SET_CDR(BgL_tail1087z00_1270, BgL_newtail1088z00_1273); 
{ 
 obj_t BgL_tail1087z00_6051; obj_t BgL_l1084z00_6049;
BgL_l1084z00_6049 = 
CDR(BgL_l1084z00_1269); 
BgL_tail1087z00_6051 = BgL_newtail1088z00_1273; 
BgL_tail1087z00_1270 = BgL_tail1087z00_6051; 
BgL_l1084z00_1269 = BgL_l1084z00_6049; 
goto BgL_zc3z04anonymousza31577ze3z87_1271;} }  else 
{ /* Ieee/pairlist.scm 858 */
if(
NULLP(BgL_l1084z00_1269))
{ /* Ieee/pairlist.scm 858 */
BgL_valz00_3032 = BgL_head1086z00_1266; }  else 
{ /* Ieee/pairlist.scm 858 */
BgL_valz00_3032 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, BgL_l1084z00_1269, BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33725L)); } } } } } 
{ /* Ieee/pairlist.scm 858 */
 int BgL_len2392z00_3033;
BgL_len2392z00_3033 = 
(int)(
bgl_list_length(BgL_valz00_3032)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_106, BgL_len2392z00_3033))
{ /* Ieee/pairlist.scm 858 */
BgL__ortest_1034z00_1245 = 
apply(BgL_predz00_106, BgL_valz00_3032); }  else 
{ /* Ieee/pairlist.scm 858 */
FAILURE(BGl_symbol2612z00zz__r4_pairs_and_lists_6_3z00,BGl_string2613z00zz__r4_pairs_and_lists_6_3z00,BGl_list2724z00zz__r4_pairs_and_lists_6_3z00);} } } 
if(
CBOOL(BgL__ortest_1034z00_1245))
{ /* Ieee/pairlist.scm 858 */
return BgL__ortest_1034z00_1245;}  else 
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_arg1561z00_1246;
if(
NULLP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 858 */
BgL_arg1561z00_1246 = BNIL; }  else 
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_head1092z00_1249;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_arg1573z00_1262;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2124;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2123;
if(
PAIRP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 858 */
BgL_pairz00_2123 = BgL_lz00_1242; }  else 
{ 
 obj_t BgL_auxz00_6069;
BgL_auxz00_6069 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33744L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1242); 
FAILURE(BgL_auxz00_6069,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2395z00_3036;
BgL_aux2395z00_3036 = 
CAR(BgL_pairz00_2123); 
if(
PAIRP(BgL_aux2395z00_3036))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2124 = BgL_aux2395z00_3036; }  else 
{ 
 obj_t BgL_auxz00_6076;
BgL_auxz00_6076 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2395z00_3036); 
FAILURE(BgL_auxz00_6076,BFALSE,BFALSE);} } } 
BgL_arg1573z00_1262 = 
CDR(BgL_pairz00_2124); } 
BgL_head1092z00_1249 = 
MAKE_YOUNG_PAIR(BgL_arg1573z00_1262, BNIL); } 
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_g1095z00_1250;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2125;
if(
PAIRP(BgL_lz00_1242))
{ /* Ieee/pairlist.scm 858 */
BgL_pairz00_2125 = BgL_lz00_1242; }  else 
{ 
 obj_t BgL_auxz00_6084;
BgL_auxz00_6084 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33744L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1242); 
FAILURE(BgL_auxz00_6084,BFALSE,BFALSE);} 
BgL_g1095z00_1250 = 
CDR(BgL_pairz00_2125); } 
{ 
 obj_t BgL_l1090z00_1252; obj_t BgL_tail1093z00_1253;
BgL_l1090z00_1252 = BgL_g1095z00_1250; 
BgL_tail1093z00_1253 = BgL_head1092z00_1249; 
BgL_zc3z04anonymousza31563ze3z87_1254:
if(
PAIRP(BgL_l1090z00_1252))
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_newtail1094z00_1256;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_arg1567z00_1258;
{ /* Ieee/pairlist.scm 858 */
 obj_t BgL_pairz00_2127;
{ /* Ieee/pairlist.scm 261 */
 obj_t BgL_aux2399z00_3040;
BgL_aux2399z00_3040 = 
CAR(BgL_l1090z00_1252); 
if(
PAIRP(BgL_aux2399z00_3040))
{ /* Ieee/pairlist.scm 261 */
BgL_pairz00_2127 = BgL_aux2399z00_3040; }  else 
{ 
 obj_t BgL_auxz00_6094;
BgL_auxz00_6094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10711L), BGl_string2794z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2399z00_3040); 
FAILURE(BgL_auxz00_6094,BFALSE,BFALSE);} } 
BgL_arg1567z00_1258 = 
CDR(BgL_pairz00_2127); } 
BgL_newtail1094z00_1256 = 
MAKE_YOUNG_PAIR(BgL_arg1567z00_1258, BNIL); } 
SET_CDR(BgL_tail1093z00_1253, BgL_newtail1094z00_1256); 
{ 
 obj_t BgL_tail1093z00_6103; obj_t BgL_l1090z00_6101;
BgL_l1090z00_6101 = 
CDR(BgL_l1090z00_1252); 
BgL_tail1093z00_6103 = BgL_newtail1094z00_1256; 
BgL_tail1093z00_1253 = BgL_tail1093z00_6103; 
BgL_l1090z00_1252 = BgL_l1090z00_6101; 
goto BgL_zc3z04anonymousza31563ze3z87_1254;} }  else 
{ /* Ieee/pairlist.scm 858 */
if(
NULLP(BgL_l1090z00_1252))
{ /* Ieee/pairlist.scm 858 */
BgL_arg1561z00_1246 = BgL_head1092z00_1249; }  else 
{ /* Ieee/pairlist.scm 858 */
BgL_arg1561z00_1246 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2611z00zz__r4_pairs_and_lists_6_3z00, BGl_string2532z00zz__r4_pairs_and_lists_6_3z00, BgL_l1090z00_1252, BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33744L)); } } } } } 
{ 
 obj_t BgL_lz00_6108;
BgL_lz00_6108 = BgL_arg1561z00_1246; 
BgL_lz00_1242 = BgL_lz00_6108; 
goto BgL_zc3z04anonymousza31559ze3z87_1243;} } }  else 
{ /* Ieee/pairlist.scm 857 */
return BFALSE;} } } } } 

}



/* &any */
obj_t BGl_z62anyz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2461, obj_t BgL_predz00_2462, obj_t BgL_lz00_2463)
{
{ /* Ieee/pairlist.scm 847 */
{ /* Ieee/pairlist.scm 849 */
 obj_t BgL_auxz00_6109;
if(
PROCEDUREP(BgL_predz00_2462))
{ /* Ieee/pairlist.scm 849 */
BgL_auxz00_6109 = BgL_predz00_2462
; }  else 
{ 
 obj_t BgL_auxz00_6112;
BgL_auxz00_6112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(33500L), BGl_string2795z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2462); 
FAILURE(BgL_auxz00_6112,BFALSE,BFALSE);} 
return 
BGl_anyz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6109, BgL_lz00_2463);} } 

}



/* find */
BGL_EXPORTED_DEF obj_t BGl_findz00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_predz00_108, obj_t BgL_listz00_109)
{
{ /* Ieee/pairlist.scm 863 */
{ /* Ieee/pairlist.scm 864 */
 obj_t BgL_g1035z00_2130;
BgL_g1035z00_2130 = 
BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_predz00_108, BgL_listz00_109); 
if(
CBOOL(BgL_g1035z00_2130))
{ /* Ieee/pairlist.scm 864 */
 obj_t BgL_pairz00_2132;
if(
PAIRP(BgL_g1035z00_2130))
{ /* Ieee/pairlist.scm 864 */
BgL_pairz00_2132 = BgL_g1035z00_2130; }  else 
{ 
 obj_t BgL_auxz00_6122;
BgL_auxz00_6122 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34021L), BGl_string2796z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_g1035z00_2130); 
FAILURE(BgL_auxz00_6122,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_2132);}  else 
{ /* Ieee/pairlist.scm 864 */
return BFALSE;} } } 

}



/* &find */
obj_t BGl_z62findz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2464, obj_t BgL_predz00_2465, obj_t BgL_listz00_2466)
{
{ /* Ieee/pairlist.scm 863 */
{ /* Ieee/pairlist.scm 864 */
 obj_t BgL_auxz00_6134; obj_t BgL_auxz00_6127;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2466))
{ /* Ieee/pairlist.scm 864 */
BgL_auxz00_6134 = BgL_listz00_2466
; }  else 
{ 
 obj_t BgL_auxz00_6137;
BgL_auxz00_6137 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34021L), BGl_string2797z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2466); 
FAILURE(BgL_auxz00_6137,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_predz00_2465))
{ /* Ieee/pairlist.scm 864 */
BgL_auxz00_6127 = BgL_predz00_2465
; }  else 
{ 
 obj_t BgL_auxz00_6130;
BgL_auxz00_6130 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34021L), BGl_string2797z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2465); 
FAILURE(BgL_auxz00_6130,BFALSE,BFALSE);} 
return 
BGl_findz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6127, BgL_auxz00_6134);} } 

}



/* find-tail */
BGL_EXPORTED_DEF obj_t BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_predz00_110, obj_t BgL_listz00_111)
{
{ /* Ieee/pairlist.scm 870 */
{ 
 obj_t BgL_listz00_1287;
BgL_listz00_1287 = BgL_listz00_111; 
BgL_zc3z04anonymousza31588ze3z87_1288:
if(
PAIRP(BgL_listz00_1287))
{ /* Ieee/pairlist.scm 873 */
 bool_t BgL_test3503z00_6144;
{ /* Ieee/pairlist.scm 873 */
 obj_t BgL_arg1594z00_1293;
BgL_arg1594z00_1293 = 
CAR(BgL_listz00_1287); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_predz00_110, 1))
{ /* Ieee/pairlist.scm 873 */
BgL_test3503z00_6144 = 
CBOOL(
BGL_PROCEDURE_CALL1(BgL_predz00_110, BgL_arg1594z00_1293))
; }  else 
{ /* Ieee/pairlist.scm 873 */
FAILURE(BGl_string2798z00zz__r4_pairs_and_lists_6_3z00,BGl_list2799z00zz__r4_pairs_and_lists_6_3z00,BgL_predz00_110);} } 
if(BgL_test3503z00_6144)
{ /* Ieee/pairlist.scm 873 */
return BgL_listz00_1287;}  else 
{ 
 obj_t BgL_listz00_6154;
BgL_listz00_6154 = 
CDR(BgL_listz00_1287); 
BgL_listz00_1287 = BgL_listz00_6154; 
goto BgL_zc3z04anonymousza31588ze3z87_1288;} }  else 
{ /* Ieee/pairlist.scm 872 */
return BFALSE;} } } 

}



/* &find-tail */
obj_t BGl_z62findzd2tailzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2467, obj_t BgL_predz00_2468, obj_t BgL_listz00_2469)
{
{ /* Ieee/pairlist.scm 870 */
{ /* Ieee/pairlist.scm 872 */
 obj_t BgL_auxz00_6163; obj_t BgL_auxz00_6156;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2469))
{ /* Ieee/pairlist.scm 872 */
BgL_auxz00_6163 = BgL_listz00_2469
; }  else 
{ 
 obj_t BgL_auxz00_6166;
BgL_auxz00_6166 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34349L), BGl_string2802z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2469); 
FAILURE(BgL_auxz00_6166,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_predz00_2468))
{ /* Ieee/pairlist.scm 872 */
BgL_auxz00_6156 = BgL_predz00_2468
; }  else 
{ 
 obj_t BgL_auxz00_6159;
BgL_auxz00_6159 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34349L), BGl_string2802z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_predz00_2468); 
FAILURE(BgL_auxz00_6159,BFALSE,BFALSE);} 
return 
BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6156, BgL_auxz00_6163);} } 

}



/* reduce */
BGL_EXPORTED_DEF obj_t BGl_reducez00zz__r4_pairs_and_lists_6_3z00(obj_t BgL_fz00_112, obj_t BgL_ridentifyz00_113, obj_t BgL_listz00_114)
{
{ /* Ieee/pairlist.scm 880 */
if(
NULLP(BgL_listz00_114))
{ /* Ieee/pairlist.scm 881 */
return BgL_ridentifyz00_113;}  else 
{ /* Ieee/pairlist.scm 883 */
 obj_t BgL_g1037z00_1296; obj_t BgL_g1038z00_1297;
BgL_g1037z00_1296 = 
CDR(BgL_listz00_114); 
BgL_g1038z00_1297 = 
CAR(BgL_listz00_114); 
{ 
 obj_t BgL_listz00_2150; obj_t BgL_ansz00_2151;
BgL_listz00_2150 = BgL_g1037z00_1296; 
BgL_ansz00_2151 = BgL_g1038z00_1297; 
BgL_loopz00_2149:
if(
PAIRP(BgL_listz00_2150))
{ /* Ieee/pairlist.scm 887 */
 obj_t BgL_arg1598z00_2157; obj_t BgL_arg1601z00_2158;
BgL_arg1598z00_2157 = 
CDR(BgL_listz00_2150); 
{ /* Ieee/pairlist.scm 887 */
 obj_t BgL_arg1602z00_2159;
BgL_arg1602z00_2159 = 
CAR(BgL_listz00_2150); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fz00_112, 2))
{ /* Ieee/pairlist.scm 887 */
BgL_arg1601z00_2158 = 
BGL_PROCEDURE_CALL2(BgL_fz00_112, BgL_arg1602z00_2159, BgL_ansz00_2151); }  else 
{ /* Ieee/pairlist.scm 887 */
FAILURE(BGl_string2596z00zz__r4_pairs_and_lists_6_3z00,BGl_list2803z00zz__r4_pairs_and_lists_6_3z00,BgL_fz00_112);} } 
{ 
 obj_t BgL_ansz00_6188; obj_t BgL_listz00_6187;
BgL_listz00_6187 = BgL_arg1598z00_2157; 
BgL_ansz00_6188 = BgL_arg1601z00_2158; 
BgL_ansz00_2151 = BgL_ansz00_6188; 
BgL_listz00_2150 = BgL_listz00_6187; 
goto BgL_loopz00_2149;} }  else 
{ /* Ieee/pairlist.scm 885 */
return BgL_ansz00_2151;} } } } 

}



/* &reduce */
obj_t BGl_z62reducez62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2470, obj_t BgL_fz00_2471, obj_t BgL_ridentifyz00_2472, obj_t BgL_listz00_2473)
{
{ /* Ieee/pairlist.scm 880 */
{ /* Ieee/pairlist.scm 881 */
 obj_t BgL_auxz00_6196; obj_t BgL_auxz00_6189;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_listz00_2473))
{ /* Ieee/pairlist.scm 881 */
BgL_auxz00_6196 = BgL_listz00_2473
; }  else 
{ 
 obj_t BgL_auxz00_6199;
BgL_auxz00_6199 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34689L), BGl_string2810z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_listz00_2473); 
FAILURE(BgL_auxz00_6199,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_fz00_2471))
{ /* Ieee/pairlist.scm 881 */
BgL_auxz00_6189 = BgL_fz00_2471
; }  else 
{ 
 obj_t BgL_auxz00_6192;
BgL_auxz00_6192 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(34689L), BGl_string2810z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_fz00_2471); 
FAILURE(BgL_auxz00_6192,BFALSE,BFALSE);} 
return 
BGl_reducez00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6189, BgL_ridentifyz00_2472, BgL_auxz00_6196);} } 

}



/* make-list */
BGL_EXPORTED_DEF obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int BgL_nz00_115, obj_t BgL_oz00_116)
{
{ /* Ieee/pairlist.scm 892 */
{ /* Ieee/pairlist.scm 893 */
 obj_t BgL_fillz00_1307;
if(
PAIRP(BgL_oz00_116))
{ /* Ieee/pairlist.scm 893 */
BgL_fillz00_1307 = 
CAR(BgL_oz00_116); }  else 
{ /* Ieee/pairlist.scm 893 */
BgL_fillz00_1307 = BUNSPEC; } 
{ 
 int BgL_iz00_2174; obj_t BgL_rz00_2175;
BgL_iz00_2174 = BgL_nz00_115; 
BgL_rz00_2175 = BNIL; 
BgL_walkz00_2173:
if(
(
(long)(BgL_iz00_2174)<=0L))
{ /* Ieee/pairlist.scm 895 */
return BgL_rz00_2175;}  else 
{ /* Ieee/pairlist.scm 897 */
 long BgL_arg1605z00_2180; obj_t BgL_arg1606z00_2181;
BgL_arg1605z00_2180 = 
(
(long)(BgL_iz00_2174)-1L); 
BgL_arg1606z00_2181 = 
MAKE_YOUNG_PAIR(BgL_fillz00_1307, BgL_rz00_2175); 
{ 
 obj_t BgL_rz00_6215; int BgL_iz00_6213;
BgL_iz00_6213 = 
(int)(BgL_arg1605z00_2180); 
BgL_rz00_6215 = BgL_arg1606z00_2181; 
BgL_rz00_2175 = BgL_rz00_6215; 
BgL_iz00_2174 = BgL_iz00_6213; 
goto BgL_walkz00_2173;} } } } } 

}



/* &make-list */
obj_t BGl_z62makezd2listzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2474, obj_t BgL_nz00_2475, obj_t BgL_oz00_2476)
{
{ /* Ieee/pairlist.scm 892 */
{ /* Ieee/pairlist.scm 893 */
 int BgL_auxz00_6216;
{ /* Ieee/pairlist.scm 893 */
 obj_t BgL_tmpz00_6217;
if(
INTEGERP(BgL_nz00_2475))
{ /* Ieee/pairlist.scm 893 */
BgL_tmpz00_6217 = BgL_nz00_2475
; }  else 
{ 
 obj_t BgL_auxz00_6220;
BgL_auxz00_6220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(35118L), BGl_string2811z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_nz00_2475); 
FAILURE(BgL_auxz00_6220,BFALSE,BFALSE);} 
BgL_auxz00_6216 = 
CINT(BgL_tmpz00_6217); } 
return 
BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6216, BgL_oz00_2476);} } 

}



/* list-tabulate */
BGL_EXPORTED_DEF obj_t BGl_listzd2tabulatezd2zz__r4_pairs_and_lists_6_3z00(int BgL_nz00_117, obj_t BgL_initzd2proczd2_118)
{
{ /* Ieee/pairlist.scm 902 */
{ /* Ieee/pairlist.scm 903 */
 long BgL_g1040z00_1318;
BgL_g1040z00_1318 = 
(
(long)(BgL_nz00_117)-1L); 
{ 
 long BgL_iz00_1321; obj_t BgL_rz00_1322;
BgL_iz00_1321 = BgL_g1040z00_1318; 
BgL_rz00_1322 = BNIL; 
BgL_zc3z04anonymousza31608ze3z87_1323:
if(
(BgL_iz00_1321<0L))
{ /* Ieee/pairlist.scm 904 */
return BgL_rz00_1322;}  else 
{ /* Ieee/pairlist.scm 906 */
 long BgL_arg1610z00_1325; obj_t BgL_arg1611z00_1326;
BgL_arg1610z00_1325 = 
(BgL_iz00_1321-1L); 
{ /* Ieee/pairlist.scm 906 */
 obj_t BgL_arg1612z00_1327;
if(
PROCEDURE_CORRECT_ARITYP(BgL_initzd2proczd2_118, 1))
{ /* Ieee/pairlist.scm 906 */
BgL_arg1612z00_1327 = 
BGL_PROCEDURE_CALL1(BgL_initzd2proczd2_118, 
BINT(BgL_iz00_1321)); }  else 
{ /* Ieee/pairlist.scm 906 */
FAILURE(BGl_string2812z00zz__r4_pairs_and_lists_6_3z00,BGl_list2813z00zz__r4_pairs_and_lists_6_3z00,BgL_initzd2proczd2_118);} 
BgL_arg1611z00_1326 = 
MAKE_YOUNG_PAIR(BgL_arg1612z00_1327, BgL_rz00_1322); } 
{ 
 obj_t BgL_rz00_6241; long BgL_iz00_6240;
BgL_iz00_6240 = BgL_arg1610z00_1325; 
BgL_rz00_6241 = BgL_arg1611z00_1326; 
BgL_rz00_1322 = BgL_rz00_6241; 
BgL_iz00_1321 = BgL_iz00_6240; 
goto BgL_zc3z04anonymousza31608ze3z87_1323;} } } } } 

}



/* &list-tabulate */
obj_t BGl_z62listzd2tabulatezb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2477, obj_t BgL_nz00_2478, obj_t BgL_initzd2proczd2_2479)
{
{ /* Ieee/pairlist.scm 902 */
{ /* Ieee/pairlist.scm 903 */
 obj_t BgL_auxz00_6251; int BgL_auxz00_6242;
if(
PROCEDUREP(BgL_initzd2proczd2_2479))
{ /* Ieee/pairlist.scm 903 */
BgL_auxz00_6251 = BgL_initzd2proczd2_2479
; }  else 
{ 
 obj_t BgL_auxz00_6254;
BgL_auxz00_6254 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(35529L), BGl_string2818z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_initzd2proczd2_2479); 
FAILURE(BgL_auxz00_6254,BFALSE,BFALSE);} 
{ /* Ieee/pairlist.scm 903 */
 obj_t BgL_tmpz00_6243;
if(
INTEGERP(BgL_nz00_2478))
{ /* Ieee/pairlist.scm 903 */
BgL_tmpz00_6243 = BgL_nz00_2478
; }  else 
{ 
 obj_t BgL_auxz00_6246;
BgL_auxz00_6246 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(35529L), BGl_string2818z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_nz00_2478); 
FAILURE(BgL_auxz00_6246,BFALSE,BFALSE);} 
BgL_auxz00_6242 = 
CINT(BgL_tmpz00_6243); } 
return 
BGl_listzd2tabulatezd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6242, BgL_auxz00_6251);} } 

}



/* list-split */
BGL_EXPORTED_DEF obj_t BGl_listzd2splitzd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_119, int BgL_numz00_120, obj_t BgL_fillz00_121)
{
{ /* Ieee/pairlist.scm 911 */
{ 
 obj_t BgL_lz00_1332; long BgL_iz00_1333; obj_t BgL_accz00_1334; obj_t BgL_resz00_1335;
BgL_lz00_1332 = BgL_lz00_119; 
BgL_iz00_1333 = 0L; 
BgL_accz00_1334 = BNIL; 
BgL_resz00_1335 = BNIL; 
BgL_zc3z04anonymousza31613ze3z87_1336:
if(
NULLP(BgL_lz00_1332))
{ /* Ieee/pairlist.scm 918 */
 obj_t BgL_arg1615z00_1338;
{ /* Ieee/pairlist.scm 918 */
 obj_t BgL_arg1616z00_1339;
{ /* Ieee/pairlist.scm 918 */
 bool_t BgL_test3520z00_6261;
if(
NULLP(BgL_fillz00_121))
{ /* Ieee/pairlist.scm 918 */
BgL_test3520z00_6261 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 918 */
if(
(BgL_iz00_1333==
(long)(BgL_numz00_120)))
{ /* Ieee/pairlist.scm 918 */
BgL_test3520z00_6261 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 918 */
BgL_test3520z00_6261 = 
(BgL_iz00_1333==0L)
; } } 
if(BgL_test3520z00_6261)
{ /* Ieee/pairlist.scm 918 */
BgL_arg1616z00_1339 = 
bgl_reverse_bang(BgL_accz00_1334); }  else 
{ /* Ieee/pairlist.scm 920 */
 obj_t BgL_arg1620z00_1343; obj_t BgL_arg1621z00_1344;
BgL_arg1620z00_1343 = 
bgl_reverse_bang(BgL_accz00_1334); 
{ /* Ieee/pairlist.scm 921 */
 long BgL_arg1622z00_1345; obj_t BgL_arg1623z00_1346;
BgL_arg1622z00_1345 = 
(
(long)(BgL_numz00_120)-BgL_iz00_1333); 
{ /* Ieee/pairlist.scm 921 */
 obj_t BgL_pairz00_2192;
if(
PAIRP(BgL_fillz00_121))
{ /* Ieee/pairlist.scm 921 */
BgL_pairz00_2192 = BgL_fillz00_121; }  else 
{ 
 obj_t BgL_auxz00_6274;
BgL_auxz00_6274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36143L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_fillz00_121); 
FAILURE(BgL_auxz00_6274,BFALSE,BFALSE);} 
BgL_arg1623z00_1346 = 
CAR(BgL_pairz00_2192); } 
{ /* Ieee/pairlist.scm 921 */
 obj_t BgL_list1624z00_1347;
BgL_list1624z00_1347 = 
MAKE_YOUNG_PAIR(BgL_arg1623z00_1346, BNIL); 
BgL_arg1621z00_1344 = 
BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
(int)(BgL_arg1622z00_1345), BgL_list1624z00_1347); } } 
BgL_arg1616z00_1339 = 
BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(BgL_arg1620z00_1343, BgL_arg1621z00_1344); } } 
BgL_arg1615z00_1338 = 
MAKE_YOUNG_PAIR(BgL_arg1616z00_1339, BgL_resz00_1335); } 
return 
bgl_reverse_bang(BgL_arg1615z00_1338);}  else 
{ /* Ieee/pairlist.scm 917 */
if(
(BgL_iz00_1333==
(long)(BgL_numz00_120)))
{ /* Ieee/pairlist.scm 924 */
 obj_t BgL_arg1626z00_1351;
BgL_arg1626z00_1351 = 
MAKE_YOUNG_PAIR(
bgl_reverse_bang(BgL_accz00_1334), BgL_resz00_1335); 
{ 
 obj_t BgL_resz00_6292; obj_t BgL_accz00_6291; long BgL_iz00_6290;
BgL_iz00_6290 = 0L; 
BgL_accz00_6291 = BNIL; 
BgL_resz00_6292 = BgL_arg1626z00_1351; 
BgL_resz00_1335 = BgL_resz00_6292; 
BgL_accz00_1334 = BgL_accz00_6291; 
BgL_iz00_1333 = BgL_iz00_6290; 
goto BgL_zc3z04anonymousza31613ze3z87_1336;} }  else 
{ /* Ieee/pairlist.scm 926 */
 obj_t BgL_arg1628z00_1353; long BgL_arg1629z00_1354; obj_t BgL_arg1630z00_1355;
{ /* Ieee/pairlist.scm 926 */
 obj_t BgL_pairz00_2195;
if(
PAIRP(BgL_lz00_1332))
{ /* Ieee/pairlist.scm 926 */
BgL_pairz00_2195 = BgL_lz00_1332; }  else 
{ 
 obj_t BgL_auxz00_6295;
BgL_auxz00_6295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36246L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1332); 
FAILURE(BgL_auxz00_6295,BFALSE,BFALSE);} 
BgL_arg1628z00_1353 = 
CDR(BgL_pairz00_2195); } 
BgL_arg1629z00_1354 = 
(BgL_iz00_1333+1L); 
{ /* Ieee/pairlist.scm 926 */
 obj_t BgL_arg1631z00_1356;
{ /* Ieee/pairlist.scm 926 */
 obj_t BgL_pairz00_2197;
if(
PAIRP(BgL_lz00_1332))
{ /* Ieee/pairlist.scm 926 */
BgL_pairz00_2197 = BgL_lz00_1332; }  else 
{ 
 obj_t BgL_auxz00_6303;
BgL_auxz00_6303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36270L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1332); 
FAILURE(BgL_auxz00_6303,BFALSE,BFALSE);} 
BgL_arg1631z00_1356 = 
CAR(BgL_pairz00_2197); } 
BgL_arg1630z00_1355 = 
MAKE_YOUNG_PAIR(BgL_arg1631z00_1356, BgL_accz00_1334); } 
{ 
 obj_t BgL_accz00_6311; long BgL_iz00_6310; obj_t BgL_lz00_6309;
BgL_lz00_6309 = BgL_arg1628z00_1353; 
BgL_iz00_6310 = BgL_arg1629z00_1354; 
BgL_accz00_6311 = BgL_arg1630z00_1355; 
BgL_accz00_1334 = BgL_accz00_6311; 
BgL_iz00_1333 = BgL_iz00_6310; 
BgL_lz00_1332 = BgL_lz00_6309; 
goto BgL_zc3z04anonymousza31613ze3z87_1336;} } } } } 

}



/* &list-split */
obj_t BGl_z62listzd2splitzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2480, obj_t BgL_lz00_2481, obj_t BgL_numz00_2482, obj_t BgL_fillz00_2483)
{
{ /* Ieee/pairlist.scm 911 */
{ /* Ieee/pairlist.scm 917 */
 int BgL_auxz00_6319; obj_t BgL_auxz00_6312;
{ /* Ieee/pairlist.scm 917 */
 obj_t BgL_tmpz00_6320;
if(
INTEGERP(BgL_numz00_2482))
{ /* Ieee/pairlist.scm 917 */
BgL_tmpz00_6320 = BgL_numz00_2482
; }  else 
{ 
 obj_t BgL_auxz00_6323;
BgL_auxz00_6323 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(35973L), BGl_string2819z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_numz00_2482); 
FAILURE(BgL_auxz00_6323,BFALSE,BFALSE);} 
BgL_auxz00_6319 = 
CINT(BgL_tmpz00_6320); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2481))
{ /* Ieee/pairlist.scm 917 */
BgL_auxz00_6312 = BgL_lz00_2481
; }  else 
{ 
 obj_t BgL_auxz00_6315;
BgL_auxz00_6315 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(35973L), BGl_string2819z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2481); 
FAILURE(BgL_auxz00_6315,BFALSE,BFALSE);} 
return 
BGl_listzd2splitzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6312, BgL_auxz00_6319, BgL_fillz00_2483);} } 

}



/* list-split! */
BGL_EXPORTED_DEF obj_t BGl_listzd2splitz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_122, int BgL_numz00_123, obj_t BgL_fillz00_124)
{
{ /* Ieee/pairlist.scm 931 */
{ 
 obj_t BgL_lz00_1360; long BgL_iz00_1361; obj_t BgL_lastz00_1362; obj_t BgL_accz00_1363; obj_t BgL_rowsz00_1364;
BgL_lz00_1360 = BgL_lz00_122; 
BgL_iz00_1361 = 0L; 
BgL_lastz00_1362 = BFALSE; 
BgL_accz00_1363 = BgL_lz00_122; 
BgL_rowsz00_1364 = BNIL; 
BgL_zc3z04anonymousza31632ze3z87_1365:
if(
NULLP(BgL_lz00_1360))
{ /* Ieee/pairlist.scm 939 */
 obj_t BgL_lrowz00_1367;
{ /* Ieee/pairlist.scm 939 */
 bool_t BgL_test3530z00_6331;
if(
NULLP(BgL_fillz00_124))
{ /* Ieee/pairlist.scm 939 */
BgL_test3530z00_6331 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 939 */
if(
(BgL_iz00_1361==
(long)(BgL_numz00_123)))
{ /* Ieee/pairlist.scm 939 */
BgL_test3530z00_6331 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 939 */
BgL_test3530z00_6331 = 
(BgL_iz00_1361==0L)
; } } 
if(BgL_test3530z00_6331)
{ /* Ieee/pairlist.scm 939 */
BgL_lrowz00_1367 = BgL_accz00_1363; }  else 
{ /* Ieee/pairlist.scm 939 */
{ /* Ieee/pairlist.scm 943 */
 obj_t BgL_arg1638z00_1372;
{ /* Ieee/pairlist.scm 943 */
 long BgL_arg1639z00_1373; obj_t BgL_arg1640z00_1374;
BgL_arg1639z00_1373 = 
(
(long)(BgL_numz00_123)-BgL_iz00_1361); 
{ /* Ieee/pairlist.scm 943 */
 obj_t BgL_pairz00_2203;
if(
PAIRP(BgL_fillz00_124))
{ /* Ieee/pairlist.scm 943 */
BgL_pairz00_2203 = BgL_fillz00_124; }  else 
{ 
 obj_t BgL_auxz00_6342;
BgL_auxz00_6342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36796L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_fillz00_124); 
FAILURE(BgL_auxz00_6342,BFALSE,BFALSE);} 
BgL_arg1640z00_1374 = 
CAR(BgL_pairz00_2203); } 
{ /* Ieee/pairlist.scm 943 */
 obj_t BgL_list1641z00_1375;
BgL_list1641z00_1375 = 
MAKE_YOUNG_PAIR(BgL_arg1640z00_1374, BNIL); 
BgL_arg1638z00_1372 = 
BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
(int)(BgL_arg1639z00_1373), BgL_list1641z00_1375); } } 
{ /* Ieee/pairlist.scm 942 */
 obj_t BgL_pairz00_2204;
if(
PAIRP(BgL_lastz00_1362))
{ /* Ieee/pairlist.scm 942 */
BgL_pairz00_2204 = BgL_lastz00_1362; }  else 
{ 
 obj_t BgL_auxz00_6352;
BgL_auxz00_6352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36752L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lastz00_1362); 
FAILURE(BgL_auxz00_6352,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2204, BgL_arg1638z00_1372); } } 
BgL_lrowz00_1367 = BgL_accz00_1363; } } 
{ /* Ieee/pairlist.scm 945 */
 obj_t BgL_arg1634z00_1368;
BgL_arg1634z00_1368 = 
MAKE_YOUNG_PAIR(BgL_lrowz00_1367, BgL_rowsz00_1364); 
return 
bgl_reverse_bang(BgL_arg1634z00_1368);} }  else 
{ /* Ieee/pairlist.scm 938 */
if(
(BgL_iz00_1361==
(long)(BgL_numz00_123)))
{ /* Ieee/pairlist.scm 946 */
{ /* Ieee/pairlist.scm 947 */
 obj_t BgL_pairz00_2207;
if(
PAIRP(BgL_lastz00_1362))
{ /* Ieee/pairlist.scm 947 */
BgL_pairz00_2207 = BgL_lastz00_1362; }  else 
{ 
 obj_t BgL_auxz00_6364;
BgL_auxz00_6364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36884L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lastz00_1362); 
FAILURE(BgL_auxz00_6364,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2207, BNIL); } 
{ /* Ieee/pairlist.scm 948 */
 obj_t BgL_arg1643z00_1379;
BgL_arg1643z00_1379 = 
MAKE_YOUNG_PAIR(BgL_accz00_1363, BgL_rowsz00_1364); 
{ 
 obj_t BgL_rowsz00_6373; obj_t BgL_accz00_6372; obj_t BgL_lastz00_6371; long BgL_iz00_6370;
BgL_iz00_6370 = 0L; 
BgL_lastz00_6371 = BgL_lz00_1360; 
BgL_accz00_6372 = BgL_lz00_1360; 
BgL_rowsz00_6373 = BgL_arg1643z00_1379; 
BgL_rowsz00_1364 = BgL_rowsz00_6373; 
BgL_accz00_1363 = BgL_accz00_6372; 
BgL_lastz00_1362 = BgL_lastz00_6371; 
BgL_iz00_1361 = BgL_iz00_6370; 
goto BgL_zc3z04anonymousza31632ze3z87_1365;} } }  else 
{ /* Ieee/pairlist.scm 950 */
 obj_t BgL_arg1644z00_1380; long BgL_arg1645z00_1381;
{ /* Ieee/pairlist.scm 950 */
 obj_t BgL_pairz00_2208;
if(
PAIRP(BgL_lz00_1360))
{ /* Ieee/pairlist.scm 950 */
BgL_pairz00_2208 = BgL_lz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6376;
BgL_auxz00_6376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36951L), BGl_string2535z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_1360); 
FAILURE(BgL_auxz00_6376,BFALSE,BFALSE);} 
BgL_arg1644z00_1380 = 
CDR(BgL_pairz00_2208); } 
BgL_arg1645z00_1381 = 
(BgL_iz00_1361+1L); 
{ 
 obj_t BgL_lastz00_6384; long BgL_iz00_6383; obj_t BgL_lz00_6382;
BgL_lz00_6382 = BgL_arg1644z00_1380; 
BgL_iz00_6383 = BgL_arg1645z00_1381; 
BgL_lastz00_6384 = BgL_lz00_1360; 
BgL_lastz00_1362 = BgL_lastz00_6384; 
BgL_iz00_1361 = BgL_iz00_6383; 
BgL_lz00_1360 = BgL_lz00_6382; 
goto BgL_zc3z04anonymousza31632ze3z87_1365;} } } } } 

}



/* &list-split! */
obj_t BGl_z62listzd2splitz12za2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2484, obj_t BgL_lz00_2485, obj_t BgL_numz00_2486, obj_t BgL_fillz00_2487)
{
{ /* Ieee/pairlist.scm 931 */
{ /* Ieee/pairlist.scm 938 */
 int BgL_auxz00_6392; obj_t BgL_auxz00_6385;
{ /* Ieee/pairlist.scm 938 */
 obj_t BgL_tmpz00_6393;
if(
INTEGERP(BgL_numz00_2486))
{ /* Ieee/pairlist.scm 938 */
BgL_tmpz00_6393 = BgL_numz00_2486
; }  else 
{ 
 obj_t BgL_auxz00_6396;
BgL_auxz00_6396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36643L), BGl_string2820z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_numz00_2486); 
FAILURE(BgL_auxz00_6396,BFALSE,BFALSE);} 
BgL_auxz00_6392 = 
CINT(BgL_tmpz00_6393); } 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2485))
{ /* Ieee/pairlist.scm 938 */
BgL_auxz00_6385 = BgL_lz00_2485
; }  else 
{ 
 obj_t BgL_auxz00_6388;
BgL_auxz00_6388 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(36643L), BGl_string2820z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2485); 
FAILURE(BgL_auxz00_6388,BFALSE,BFALSE);} 
return 
BGl_listzd2splitz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6385, BgL_auxz00_6392, BgL_fillz00_2487);} } 

}



/* iota */
BGL_EXPORTED_DEF obj_t BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(int BgL_countz00_125, obj_t BgL_restz00_126)
{
{ /* Ieee/pairlist.scm 955 */
{ /* Ieee/pairlist.scm 956 */
 obj_t BgL_startz00_1383; obj_t BgL_stepz00_1384;
BgL_startz00_1383 = 
BINT(0L); 
BgL_stepz00_1384 = 
BINT(1L); 
if(
PAIRP(BgL_restz00_126))
{ /* Ieee/pairlist.scm 958 */
BgL_startz00_1383 = 
CAR(BgL_restz00_126); 
{ /* Ieee/pairlist.scm 961 */
 bool_t BgL_test3541z00_6407;
{ /* Ieee/pairlist.scm 229 */
 obj_t BgL_tmpz00_6408;
BgL_tmpz00_6408 = 
CDR(BgL_restz00_126); 
BgL_test3541z00_6407 = 
PAIRP(BgL_tmpz00_6408); } 
if(BgL_test3541z00_6407)
{ /* Ieee/pairlist.scm 285 */
 obj_t BgL_pairz00_2215;
{ /* Ieee/pairlist.scm 267 */
 obj_t BgL_aux2444z00_3088;
BgL_aux2444z00_3088 = 
CDR(BgL_restz00_126); 
if(
PAIRP(BgL_aux2444z00_3088))
{ /* Ieee/pairlist.scm 267 */
BgL_pairz00_2215 = BgL_aux2444z00_3088; }  else 
{ 
 obj_t BgL_auxz00_6414;
BgL_auxz00_6414 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(10976L), BGl_string2821z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_aux2444z00_3088); 
FAILURE(BgL_auxz00_6414,BFALSE,BFALSE);} } 
BgL_stepz00_1384 = 
CAR(BgL_pairz00_2215); }  else 
{ /* Ieee/pairlist.scm 961 */BFALSE; } } }  else 
{ /* Ieee/pairlist.scm 958 */BFALSE; } 
{ /* Ieee/pairlist.scm 963 */
 obj_t BgL_g1045z00_1389;
{ /* Ieee/pairlist.scm 963 */
 obj_t BgL_b1097z00_1402;
{ /* Ieee/pairlist.scm 963 */
 long BgL_a1096z00_1404;
BgL_a1096z00_1404 = 
(
(long)(BgL_countz00_125)-1L); 
if(
INTEGERP(BgL_stepz00_1384))
{ /* Ieee/pairlist.scm 963 */
 long BgL_za72za7_2218;
BgL_za72za7_2218 = 
(long)CINT(BgL_stepz00_1384); 
BgL_b1097z00_1402 = 
BINT(
(BgL_a1096z00_1404*BgL_za72za7_2218)); }  else 
{ /* Ieee/pairlist.scm 963 */
BgL_b1097z00_1402 = 
BGl_2za2za2zz__r4_numbers_6_5z00(
BINT(BgL_a1096z00_1404), BgL_stepz00_1384); } } 
{ /* Ieee/pairlist.scm 963 */
 bool_t BgL_test3544z00_6428;
if(
INTEGERP(BgL_startz00_1383))
{ /* Ieee/pairlist.scm 963 */
BgL_test3544z00_6428 = 
INTEGERP(BgL_b1097z00_1402)
; }  else 
{ /* Ieee/pairlist.scm 963 */
BgL_test3544z00_6428 = ((bool_t)0)
; } 
if(BgL_test3544z00_6428)
{ /* Ieee/pairlist.scm 963 */
BgL_g1045z00_1389 = 
ADDFX(BgL_startz00_1383, BgL_b1097z00_1402); }  else 
{ /* Ieee/pairlist.scm 963 */
BgL_g1045z00_1389 = 
BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_startz00_1383, BgL_b1097z00_1402); } } } 
{ 
 int BgL_iz00_1392; obj_t BgL_vz00_1393; obj_t BgL_rz00_1394;
BgL_iz00_1392 = BgL_countz00_125; 
BgL_vz00_1393 = BgL_g1045z00_1389; 
BgL_rz00_1394 = BNIL; 
BgL_zc3z04anonymousza31651ze3z87_1395:
if(
(
(long)(BgL_iz00_1392)<=0L))
{ /* Ieee/pairlist.scm 964 */
return BgL_rz00_1394;}  else 
{ /* Ieee/pairlist.scm 966 */
 long BgL_arg1653z00_1397; obj_t BgL_arg1654z00_1398; obj_t BgL_arg1656z00_1399;
BgL_arg1653z00_1397 = 
(
(long)(BgL_iz00_1392)-1L); 
{ /* Ieee/pairlist.scm 966 */
 bool_t BgL_test3547z00_6439;
if(
INTEGERP(BgL_vz00_1393))
{ /* Ieee/pairlist.scm 966 */
BgL_test3547z00_6439 = 
INTEGERP(BgL_stepz00_1384)
; }  else 
{ /* Ieee/pairlist.scm 966 */
BgL_test3547z00_6439 = ((bool_t)0)
; } 
if(BgL_test3547z00_6439)
{ /* Ieee/pairlist.scm 966 */
BgL_arg1654z00_1398 = 
SUBFX(BgL_vz00_1393, BgL_stepz00_1384); }  else 
{ /* Ieee/pairlist.scm 966 */
BgL_arg1654z00_1398 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_vz00_1393, BgL_stepz00_1384); } } 
BgL_arg1656z00_1399 = 
MAKE_YOUNG_PAIR(BgL_vz00_1393, BgL_rz00_1394); 
{ 
 obj_t BgL_rz00_6449; obj_t BgL_vz00_6448; int BgL_iz00_6446;
BgL_iz00_6446 = 
(int)(BgL_arg1653z00_1397); 
BgL_vz00_6448 = BgL_arg1654z00_1398; 
BgL_rz00_6449 = BgL_arg1656z00_1399; 
BgL_rz00_1394 = BgL_rz00_6449; 
BgL_vz00_1393 = BgL_vz00_6448; 
BgL_iz00_1392 = BgL_iz00_6446; 
goto BgL_zc3z04anonymousza31651ze3z87_1395;} } } } } } 

}



/* &iota */
obj_t BGl_z62iotaz62zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2488, obj_t BgL_countz00_2489, obj_t BgL_restz00_2490)
{
{ /* Ieee/pairlist.scm 955 */
{ /* Ieee/pairlist.scm 956 */
 int BgL_auxz00_6450;
{ /* Ieee/pairlist.scm 956 */
 obj_t BgL_tmpz00_6451;
if(
INTEGERP(BgL_countz00_2489))
{ /* Ieee/pairlist.scm 956 */
BgL_tmpz00_6451 = BgL_countz00_2489
; }  else 
{ 
 obj_t BgL_auxz00_6454;
BgL_auxz00_6454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(37234L), BGl_string2822z00zz__r4_pairs_and_lists_6_3z00, BGl_string2552z00zz__r4_pairs_and_lists_6_3z00, BgL_countz00_2489); 
FAILURE(BgL_auxz00_6454,BFALSE,BFALSE);} 
BgL_auxz00_6450 = 
CINT(BgL_tmpz00_6451); } 
return 
BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6450, BgL_restz00_2490);} } 

}



/* list-copy */
BGL_EXPORTED_DEF obj_t BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_127)
{
{ /* Ieee/pairlist.scm 971 */
if(
NULLP(BgL_lz00_127))
{ /* Ieee/pairlist.scm 972 */
return BgL_lz00_127;}  else 
{ /* Ieee/pairlist.scm 973 */
 obj_t BgL_arg1661z00_1407; obj_t BgL_arg1663z00_1408;
BgL_arg1661z00_1407 = 
CAR(BgL_lz00_127); 
{ /* Ieee/pairlist.scm 973 */
 obj_t BgL_arg1664z00_1409;
BgL_arg1664z00_1409 = 
CDR(BgL_lz00_127); 
{ /* Ieee/pairlist.scm 973 */
 obj_t BgL_auxz00_6464;
{ /* Ieee/pairlist.scm 973 */
 bool_t BgL_test3551z00_6465;
if(
PAIRP(BgL_arg1664z00_1409))
{ /* Ieee/pairlist.scm 241 */
BgL_test3551z00_6465 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3551z00_6465 = 
NULLP(BgL_arg1664z00_1409)
; } 
if(BgL_test3551z00_6465)
{ /* Ieee/pairlist.scm 973 */
BgL_auxz00_6464 = BgL_arg1664z00_1409
; }  else 
{ 
 obj_t BgL_auxz00_6469;
BgL_auxz00_6469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(37830L), BGl_string2823z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_arg1664z00_1409); 
FAILURE(BgL_auxz00_6469,BFALSE,BFALSE);} } 
BgL_arg1663z00_1408 = 
BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6464); } } 
return 
MAKE_YOUNG_PAIR(BgL_arg1661z00_1407, BgL_arg1663z00_1408);} } 

}



/* &list-copy */
obj_t BGl_z62listzd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2491, obj_t BgL_lz00_2492)
{
{ /* Ieee/pairlist.scm 971 */
{ /* Ieee/pairlist.scm 972 */
 obj_t BgL_auxz00_6475;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2492))
{ /* Ieee/pairlist.scm 972 */
BgL_auxz00_6475 = BgL_lz00_2492
; }  else 
{ 
 obj_t BgL_auxz00_6478;
BgL_auxz00_6478 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(37778L), BGl_string2824z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_2492); 
FAILURE(BgL_auxz00_6478,BFALSE,BFALSE);} 
return 
BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6475);} } 

}



/* tree-copy */
BGL_EXPORTED_DEF obj_t BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lz00_128)
{
{ /* Ieee/pairlist.scm 979 */
if(
EPAIRP(BgL_lz00_128))
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_arg1667z00_1411; obj_t BgL_arg1668z00_1412; obj_t BgL_arg1669z00_1413;
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_arg1670z00_1414;
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_pairz00_2227;
if(
PAIRP(BgL_lz00_128))
{ /* Ieee/pairlist.scm 982 */
BgL_pairz00_2227 = BgL_lz00_128; }  else 
{ 
 obj_t BgL_auxz00_6487;
BgL_auxz00_6487 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38147L), BGl_string2825z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_128); 
FAILURE(BgL_auxz00_6487,BFALSE,BFALSE);} 
BgL_arg1670z00_1414 = 
CAR(BgL_pairz00_2227); } 
BgL_arg1667z00_1411 = 
BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1670z00_1414); } 
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_arg1675z00_1415;
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_pairz00_2228;
if(
PAIRP(BgL_lz00_128))
{ /* Ieee/pairlist.scm 982 */
BgL_pairz00_2228 = BgL_lz00_128; }  else 
{ 
 obj_t BgL_auxz00_6495;
BgL_auxz00_6495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38167L), BGl_string2825z00zz__r4_pairs_and_lists_6_3z00, BGl_string2469z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_128); 
FAILURE(BgL_auxz00_6495,BFALSE,BFALSE);} 
BgL_arg1675z00_1415 = 
CDR(BgL_pairz00_2228); } 
BgL_arg1668z00_1412 = 
BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1675z00_1415); } 
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_arg1676z00_1416;
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_objz00_2229;
if(
EPAIRP(BgL_lz00_128))
{ /* Ieee/pairlist.scm 982 */
BgL_objz00_2229 = BgL_lz00_128; }  else 
{ 
 obj_t BgL_auxz00_6503;
BgL_auxz00_6503 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38187L), BGl_string2825z00zz__r4_pairs_and_lists_6_3z00, BGl_string2472z00zz__r4_pairs_and_lists_6_3z00, BgL_lz00_128); 
FAILURE(BgL_auxz00_6503,BFALSE,BFALSE);} 
BgL_arg1676z00_1416 = 
CER(BgL_objz00_2229); } 
BgL_arg1669z00_1413 = 
BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1676z00_1416); } 
{ /* Ieee/pairlist.scm 982 */
 obj_t BgL_res1862z00_2230;
BgL_res1862z00_2230 = 
MAKE_YOUNG_EPAIR(BgL_arg1667z00_1411, BgL_arg1668z00_1412, BgL_arg1669z00_1413); 
return BgL_res1862z00_2230;} }  else 
{ /* Ieee/pairlist.scm 981 */
if(
PAIRP(BgL_lz00_128))
{ /* Ieee/pairlist.scm 983 */
return 
MAKE_YOUNG_PAIR(
BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(
CAR(BgL_lz00_128)), 
BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(
CDR(BgL_lz00_128)));}  else 
{ /* Ieee/pairlist.scm 983 */
return BgL_lz00_128;} } } 

}



/* &tree-copy */
obj_t BGl_z62treezd2copyzb0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_envz00_2493, obj_t BgL_lz00_2494)
{
{ /* Ieee/pairlist.scm 979 */
return 
BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_lz00_2494);} 

}



/* _delete-duplicates */
obj_t BGl__deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_env1118z00_132, obj_t BgL_opt1117z00_131)
{
{ /* Ieee/pairlist.scm 991 */
{ /* Ieee/pairlist.scm 991 */
 obj_t BgL_g1119z00_1422;
BgL_g1119z00_1422 = 
VECTOR_REF(BgL_opt1117z00_131,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1117z00_131)) { case 1L : 

{ /* Ieee/pairlist.scm 991 */

{ /* Ieee/pairlist.scm 991 */
 obj_t BgL_lisz00_2233;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1119z00_1422))
{ /* Ieee/pairlist.scm 991 */
BgL_lisz00_2233 = BgL_g1119z00_1422; }  else 
{ 
 obj_t BgL_auxz00_6521;
BgL_auxz00_6521 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38512L), BGl_string2829z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1119z00_1422); 
FAILURE(BgL_auxz00_6521,BFALSE,BFALSE);} 
return 
BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(
BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_lisz00_2233), BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);} } break;case 2L : 

{ /* Ieee/pairlist.scm 991 */
 obj_t BgL_eqz00_1426;
BgL_eqz00_1426 = 
VECTOR_REF(BgL_opt1117z00_131,1L); 
{ /* Ieee/pairlist.scm 991 */

{ /* Ieee/pairlist.scm 991 */
 obj_t BgL_lisz00_2235;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1119z00_1422))
{ /* Ieee/pairlist.scm 991 */
BgL_lisz00_2235 = BgL_g1119z00_1422; }  else 
{ 
 obj_t BgL_auxz00_6530;
BgL_auxz00_6530 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38512L), BGl_string2829z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1119z00_1422); 
FAILURE(BgL_auxz00_6530,BFALSE,BFALSE);} 
return 
BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(
BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_lisz00_2235), BgL_eqz00_1426);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2826z00zz__r4_pairs_and_lists_6_3z00, BGl_string2828z00zz__r4_pairs_and_lists_6_3z00, 
BINT(
VECTOR_LENGTH(BgL_opt1117z00_131)));} } } } 

}



/* delete-duplicates */
BGL_EXPORTED_DEF obj_t BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lisz00_129, obj_t BgL_eqz00_130)
{
{ /* Ieee/pairlist.scm 991 */
BGL_TAIL return 
BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(
BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(BgL_lisz00_129), BgL_eqz00_130);} 

}



/* _delete-duplicates! */
obj_t BGl__deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_env1123z00_136, obj_t BgL_opt1122z00_135)
{
{ /* Ieee/pairlist.scm 997 */
{ /* Ieee/pairlist.scm 997 */
 obj_t BgL_g1124z00_1429;
BgL_g1124z00_1429 = 
VECTOR_REF(BgL_opt1122z00_135,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1122z00_135)) { case 1L : 

{ /* Ieee/pairlist.scm 997 */

{ /* Ieee/pairlist.scm 997 */
 obj_t BgL_auxz00_6544;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1124z00_1429))
{ /* Ieee/pairlist.scm 997 */
BgL_auxz00_6544 = BgL_g1124z00_1429
; }  else 
{ 
 obj_t BgL_auxz00_6547;
BgL_auxz00_6547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38834L), BGl_string2832z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1124z00_1429); 
FAILURE(BgL_auxz00_6547,BFALSE,BFALSE);} 
return 
BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6544, BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);} } break;case 2L : 

{ /* Ieee/pairlist.scm 997 */
 obj_t BgL_eqz00_1433;
BgL_eqz00_1433 = 
VECTOR_REF(BgL_opt1122z00_135,1L); 
{ /* Ieee/pairlist.scm 997 */

{ /* Ieee/pairlist.scm 997 */
 obj_t BgL_auxz00_6553;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_g1124z00_1429))
{ /* Ieee/pairlist.scm 997 */
BgL_auxz00_6553 = BgL_g1124z00_1429
; }  else 
{ 
 obj_t BgL_auxz00_6556;
BgL_auxz00_6556 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(38834L), BGl_string2832z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_g1124z00_1429); 
FAILURE(BgL_auxz00_6556,BFALSE,BFALSE);} 
return 
BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_6553, BgL_eqz00_1433);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2830z00zz__r4_pairs_and_lists_6_3z00, BGl_string2828z00zz__r4_pairs_and_lists_6_3z00, 
BINT(
VECTOR_LENGTH(BgL_opt1122z00_135)));} } } } 

}



/* delete-duplicates! */
BGL_EXPORTED_DEF obj_t BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t BgL_lisz00_133, obj_t BgL_eqz00_134)
{
{ /* Ieee/pairlist.scm 997 */
if(
PROCEDUREP(BgL_eqz00_134))
{ /* Ieee/pairlist.scm 998 */BFALSE; }  else 
{ /* Ieee/pairlist.scm 998 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_symbol2830z00zz__r4_pairs_and_lists_6_3z00, BGl_string2577z00zz__r4_pairs_and_lists_6_3z00, BgL_eqz00_134); } 
return 
BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_134, BgL_lisz00_133);} 

}



/* recur~0 */
obj_t BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(obj_t BgL_eqz00_2495, obj_t BgL_lisz00_1437)
{
{ /* Ieee/pairlist.scm 1000 */
if(
NULLP(BgL_lisz00_1437))
{ /* Ieee/pairlist.scm 1001 */
return BgL_lisz00_1437;}  else 
{ /* Ieee/pairlist.scm 1002 */
 obj_t BgL_xz00_1440;
BgL_xz00_1440 = 
CAR(BgL_lisz00_1437); 
{ /* Ieee/pairlist.scm 1002 */
 obj_t BgL_tailz00_1441;
BgL_tailz00_1441 = 
CDR(BgL_lisz00_1437); 
{ /* Ieee/pairlist.scm 1003 */
 obj_t BgL_newzd2tailzd2_1442;
{ /* Ieee/pairlist.scm 1004 */
 obj_t BgL_arg1699z00_1443;
{ /* Ieee/pairlist.scm 1004 */
 obj_t BgL_auxz00_6574;
{ /* Ieee/pairlist.scm 1004 */
 bool_t BgL_test3565z00_6575;
if(
PAIRP(BgL_tailz00_1441))
{ /* Ieee/pairlist.scm 241 */
BgL_test3565z00_6575 = ((bool_t)1)
; }  else 
{ /* Ieee/pairlist.scm 241 */
BgL_test3565z00_6575 = 
NULLP(BgL_tailz00_1441)
; } 
if(BgL_test3565z00_6575)
{ /* Ieee/pairlist.scm 1004 */
BgL_auxz00_6574 = BgL_tailz00_1441
; }  else 
{ 
 obj_t BgL_auxz00_6579;
BgL_auxz00_6579 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2467z00zz__r4_pairs_and_lists_6_3z00, 
BINT(39102L), BGl_string2833z00zz__r4_pairs_and_lists_6_3z00, BGl_string2533z00zz__r4_pairs_and_lists_6_3z00, BgL_tailz00_1441); 
FAILURE(BgL_auxz00_6579,BFALSE,BFALSE);} } 
BgL_arg1699z00_1443 = 
BGl_deletez12z12zz__r4_pairs_and_lists_6_3z00(BgL_xz00_1440, BgL_auxz00_6574, BgL_eqz00_2495); } 
BgL_newzd2tailzd2_1442 = 
BGl_recurze70ze7zz__r4_pairs_and_lists_6_3z00(BgL_eqz00_2495, BgL_arg1699z00_1443); } 
{ /* Ieee/pairlist.scm 1004 */

if(
(BgL_tailz00_1441==BgL_newzd2tailzd2_1442))
{ /* Ieee/pairlist.scm 1005 */
return BgL_lisz00_1437;}  else 
{ /* Ieee/pairlist.scm 1005 */
return 
MAKE_YOUNG_PAIR(BgL_xz00_1440, BgL_newzd2tailzd2_1442);} } } } } } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_pairs_and_lists_6_3z00(void)
{
{ /* Ieee/pairlist.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2834z00zz__r4_pairs_and_lists_6_3z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string2834z00zz__r4_pairs_and_lists_6_3z00));} 

}

#ifdef __cplusplus
}
#endif
