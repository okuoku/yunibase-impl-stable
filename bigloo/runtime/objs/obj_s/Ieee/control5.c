/*===========================================================================*/
/*   (Ieee/control5.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/control5.scm -indent -o objs/obj_s/Ieee/control5.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R5_CONTROL_FEATURES_6_4_TYPE_DEFINITIONS
#define BGL___R5_CONTROL_FEATURES_6_4_TYPE_DEFINITIONS
#endif // BGL___R5_CONTROL_FEATURES_6_4_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_z52getzd2mvalueszd2valz52zz__r5_control_features_6_4z00(int);
static obj_t BGl_symbol1831z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1750z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1833z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1752z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1671z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1835z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1754z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1673z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1837z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1756z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1675z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1839z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1758z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1677z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1644z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1726z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1645z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1679z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1646z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1649z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1760z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1842z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1762z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1844z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1682z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1764z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1846z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1684z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1814z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1848z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1767z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1686z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1654z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1769z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1688z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2valuesz00zz__r5_control_features_6_4z00(obj_t, obj_t);
static obj_t BGl_symbol1850z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1852z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1771z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1690z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1854z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1773z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1692z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1856z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1775z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1661z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1695z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_z62z52getzd2mvalueszd2numberz30zz__r5_control_features_6_4z00(obj_t);
static obj_t BGl_symbol1858z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1777z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1745z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1697z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1779z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1699z00zz__r5_control_features_6_4z00 = BUNSPEC;
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_symbol1860z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1862z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1781z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1864z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1783z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1670z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1866z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1785z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1868z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1787z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1871z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1790z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1873z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1792z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1841z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1875z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1794z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1681z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1877z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1796z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1879z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1798z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1766z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1881z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1883z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1885z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1887z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1889z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1694z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_z62z52setzd2mvalueszd2valz12z22zz__r5_control_features_6_4z00(obj_t, obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r5_control_features_6_4z00(void);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62z52getzd2mvalueszd2valz30zz__r5_control_features_6_4z00(obj_t, obj_t);
static obj_t BGl_symbol1891z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1893z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__r5_control_features_6_4z00(void);
static obj_t BGl_symbol1895z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1897z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__r5_control_features_6_4z00(void);
static obj_t BGl_symbol1899z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1789z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1870z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_z62callzd2withzd2valuesz62zz__r5_control_features_6_4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52setzd2mvalueszd2valz12z40zz__r5_control_features_6_4z00(int, obj_t);
static obj_t BGl_z62valuesz62zz__r5_control_features_6_4z00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL int BGl_z52getzd2mvalueszd2numberz52zz__r5_control_features_6_4z00(void);
static obj_t BGl_symbol1701z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1703z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1705z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1707z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_z62z52setzd2mvalueszd2numberz12z22zz__r5_control_features_6_4z00(obj_t, obj_t);
static obj_t BGl_symbol1710z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1630z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1712z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1632z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1714z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1634z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1716z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1718z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1638z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1800z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1720z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1802z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1640z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1722z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1804z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1642z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1724z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1806z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1808z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1727z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1647z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1729z00zz__r5_control_features_6_4z00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_z52setzd2mvalueszd2numberz12z40zz__r5_control_features_6_4z00(int);
static obj_t BGl_symbol1810z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1812z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1731z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1650z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1733z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1652z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1815z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1735z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1817z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1655z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1737z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1819z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1657z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1739z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1659z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1709z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1629z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1821z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1741z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1823z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1743z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1662z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1825z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1664z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1827z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1746z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1666z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1829z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1748z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_symbol1668z00zz__r5_control_features_6_4z00 = BUNSPEC;
static obj_t BGl_list1637z00zz__r5_control_features_6_4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_valuesz00zz__r5_control_features_6_4z00(obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string1872z00zz__r5_control_features_6_4z00, BgL_bgl_string1872za700za7za7_1904za7, "arg1312", 7 );
DEFINE_STRING( BGl_string1791z00zz__r5_control_features_6_4z00, BgL_bgl_string1791za700za7za7_1905za7, "arg1209", 7 );
DEFINE_STRING( BGl_string1874z00zz__r5_control_features_6_4z00, BgL_bgl_string1874za700za7za7_1906za7, "arg1314", 7 );
DEFINE_STRING( BGl_string1793z00zz__r5_control_features_6_4z00, BgL_bgl_string1793za700za7za7_1907za7, "arg1210", 7 );
DEFINE_STRING( BGl_string1876z00zz__r5_control_features_6_4z00, BgL_bgl_string1876za700za7za7_1908za7, "arg1315", 7 );
DEFINE_STRING( BGl_string1795z00zz__r5_control_features_6_4z00, BgL_bgl_string1795za700za7za7_1909za7, "arg1212", 7 );
DEFINE_STRING( BGl_string1878z00zz__r5_control_features_6_4z00, BgL_bgl_string1878za700za7za7_1910za7, "arg1316", 7 );
DEFINE_STRING( BGl_string1797z00zz__r5_control_features_6_4z00, BgL_bgl_string1797za700za7za7_1911za7, "arg1215", 7 );
DEFINE_STRING( BGl_string1799z00zz__r5_control_features_6_4z00, BgL_bgl_string1799za700za7za7_1912za7, "arg1216", 7 );
DEFINE_STRING( BGl_string1880z00zz__r5_control_features_6_4z00, BgL_bgl_string1880za700za7za7_1913za7, "arg1317", 7 );
DEFINE_STRING( BGl_string1882z00zz__r5_control_features_6_4z00, BgL_bgl_string1882za700za7za7_1914za7, "arg1318", 7 );
DEFINE_STRING( BGl_string1884z00zz__r5_control_features_6_4z00, BgL_bgl_string1884za700za7za7_1915za7, "arg1319", 7 );
DEFINE_STRING( BGl_string1886z00zz__r5_control_features_6_4z00, BgL_bgl_string1886za700za7za7_1916za7, "arg1320", 7 );
DEFINE_STRING( BGl_string1888z00zz__r5_control_features_6_4z00, BgL_bgl_string1888za700za7za7_1917za7, "arg1321", 7 );
DEFINE_STRING( BGl_string1890z00zz__r5_control_features_6_4z00, BgL_bgl_string1890za700za7za7_1918za7, "arg1322", 7 );
DEFINE_STRING( BGl_string1892z00zz__r5_control_features_6_4z00, BgL_bgl_string1892za700za7za7_1919za7, "arg1323", 7 );
DEFINE_STRING( BGl_string1894z00zz__r5_control_features_6_4z00, BgL_bgl_string1894za700za7za7_1920za7, "arg1325", 7 );
DEFINE_STRING( BGl_string1896z00zz__r5_control_features_6_4z00, BgL_bgl_string1896za700za7za7_1921za7, "arg1326", 7 );
DEFINE_STRING( BGl_string1898z00zz__r5_control_features_6_4z00, BgL_bgl_string1898za700za7za7_1922za7, "arg1327", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52setzd2mvalueszd2numberz12zd2envz92zz__r5_control_features_6_4z00, BgL_bgl_za762za752setza7d2mval1923za7, BGl_z62z52setzd2mvalueszd2numberz12z22zz__r5_control_features_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1700z00zz__r5_control_features_6_4z00, BgL_bgl_string1700za700za7za7_1924za7, "arg1126", 7 );
DEFINE_STRING( BGl_string1620z00zz__r5_control_features_6_4z00, BgL_bgl_string1620za700za7za7_1925za7, "/tmp/bigloo/runtime/Ieee/control5.scm", 37 );
DEFINE_STRING( BGl_string1702z00zz__r5_control_features_6_4z00, BgL_bgl_string1702za700za7za7_1926za7, "arg1127", 7 );
DEFINE_STRING( BGl_string1621z00zz__r5_control_features_6_4z00, BgL_bgl_string1621za700za7za7_1927za7, "&%set-mvalues-number!", 21 );
DEFINE_STRING( BGl_string1622z00zz__r5_control_features_6_4z00, BgL_bgl_string1622za700za7za7_1928za7, "bint", 4 );
DEFINE_STRING( BGl_string1704z00zz__r5_control_features_6_4z00, BgL_bgl_string1704za700za7za7_1929za7, "arg1129", 7 );
DEFINE_STRING( BGl_string1623z00zz__r5_control_features_6_4z00, BgL_bgl_string1623za700za7za7_1930za7, "&%get-mvalues-val", 17 );
DEFINE_STRING( BGl_string1624z00zz__r5_control_features_6_4z00, BgL_bgl_string1624za700za7za7_1931za7, "&%set-mvalues-val!", 18 );
DEFINE_STRING( BGl_string1706z00zz__r5_control_features_6_4z00, BgL_bgl_string1706za700za7za7_1932za7, "arg1131", 7 );
DEFINE_STRING( BGl_string1625z00zz__r5_control_features_6_4z00, BgL_bgl_string1625za700za7za7_1933za7, "values", 6 );
DEFINE_STRING( BGl_string1626z00zz__r5_control_features_6_4z00, BgL_bgl_string1626za700za7za7_1934za7, "pair", 4 );
DEFINE_STRING( BGl_string1708z00zz__r5_control_features_6_4z00, BgL_bgl_string1708za700za7za7_1935za7, "arg1132", 7 );
DEFINE_STRING( BGl_string1627z00zz__r5_control_features_6_4z00, BgL_bgl_string1627za700za7za7_1936za7, "loop", 4 );
DEFINE_STRING( BGl_string1628z00zz__r5_control_features_6_4z00, BgL_bgl_string1628za700za7za7_1937za7, "call-with-values:Wrong number of arguments", 42 );
DEFINE_STRING( BGl_string1711z00zz__r5_control_features_6_4z00, BgL_bgl_string1711za700za7za7_1938za7, "arg1137", 7 );
DEFINE_STRING( BGl_string1631z00zz__r5_control_features_6_4z00, BgL_bgl_string1631za700za7za7_1939za7, "funcall", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52setzd2mvalueszd2valz12zd2envz92zz__r5_control_features_6_4z00, BgL_bgl_za762za752setza7d2mval1940za7, BGl_z62z52setzd2mvalueszd2valz12z22zz__r5_control_features_6_4z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1713z00zz__r5_control_features_6_4z00, BgL_bgl_string1713za700za7za7_1941za7, "arg1138", 7 );
DEFINE_STRING( BGl_string1633z00zz__r5_control_features_6_4z00, BgL_bgl_string1633za700za7za7_1942za7, "producer", 8 );
DEFINE_STRING( BGl_string1715z00zz__r5_control_features_6_4z00, BgL_bgl_string1715za700za7za7_1943za7, "arg1140", 7 );
DEFINE_STRING( BGl_string1635z00zz__r5_control_features_6_4z00, BgL_bgl_string1635za700za7za7_1944za7, "call-with-values", 16 );
DEFINE_STRING( BGl_string1717z00zz__r5_control_features_6_4z00, BgL_bgl_string1717za700za7za7_1945za7, "arg1141", 7 );
DEFINE_STRING( BGl_string1636z00zz__r5_control_features_6_4z00, BgL_bgl_string1636za700za7za7_1946za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string1719z00zz__r5_control_features_6_4z00, BgL_bgl_string1719za700za7za7_1947za7, "arg1142", 7 );
DEFINE_STRING( BGl_string1639z00zz__r5_control_features_6_4z00, BgL_bgl_string1639za700za7za7_1948za7, "apply", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2valueszd2envzd2zz__r5_control_features_6_4z00, BgL_bgl_za762callza7d2withza7d1949za7, BGl_z62callzd2withzd2valuesz62zz__r5_control_features_6_4z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1801z00zz__r5_control_features_6_4z00, BgL_bgl_string1801za700za7za7_1950za7, "arg1218", 7 );
DEFINE_STRING( BGl_string1721z00zz__r5_control_features_6_4z00, BgL_bgl_string1721za700za7za7_1951za7, "arg1143", 7 );
DEFINE_STRING( BGl_string1803z00zz__r5_control_features_6_4z00, BgL_bgl_string1803za700za7za7_1952za7, "arg1219", 7 );
DEFINE_STRING( BGl_string1641z00zz__r5_control_features_6_4z00, BgL_bgl_string1641za700za7za7_1953za7, "consumer", 8 );
DEFINE_STRING( BGl_string1723z00zz__r5_control_features_6_4z00, BgL_bgl_string1723za700za7za7_1954za7, "arg1145", 7 );
DEFINE_STRING( BGl_string1805z00zz__r5_control_features_6_4z00, BgL_bgl_string1805za700za7za7_1955za7, "arg1220", 7 );
DEFINE_STRING( BGl_string1643z00zz__r5_control_features_6_4z00, BgL_bgl_string1643za700za7za7_1956za7, "res0", 4 );
DEFINE_STRING( BGl_string1725z00zz__r5_control_features_6_4z00, BgL_bgl_string1725za700za7za7_1957za7, "arg1148", 7 );
DEFINE_STRING( BGl_string1807z00zz__r5_control_features_6_4z00, BgL_bgl_string1807za700za7za7_1958za7, "arg1221", 7 );
DEFINE_STRING( BGl_string1809z00zz__r5_control_features_6_4z00, BgL_bgl_string1809za700za7za7_1959za7, "arg1223", 7 );
DEFINE_STRING( BGl_string1728z00zz__r5_control_features_6_4z00, BgL_bgl_string1728za700za7za7_1960za7, "arg1149", 7 );
DEFINE_STRING( BGl_string1648z00zz__r5_control_features_6_4z00, BgL_bgl_string1648za700za7za7_1961za7, "arg1074", 7 );
DEFINE_STRING( BGl_string1811z00zz__r5_control_features_6_4z00, BgL_bgl_string1811za700za7za7_1962za7, "arg1225", 7 );
DEFINE_STRING( BGl_string1730z00zz__r5_control_features_6_4z00, BgL_bgl_string1730za700za7za7_1963za7, "arg1152", 7 );
DEFINE_STRING( BGl_string1813z00zz__r5_control_features_6_4z00, BgL_bgl_string1813za700za7za7_1964za7, "arg1226", 7 );
DEFINE_STRING( BGl_string1732z00zz__r5_control_features_6_4z00, BgL_bgl_string1732za700za7za7_1965za7, "arg1153", 7 );
DEFINE_STRING( BGl_string1651z00zz__r5_control_features_6_4z00, BgL_bgl_string1651za700za7za7_1966za7, "arg1075", 7 );
DEFINE_STRING( BGl_string1734z00zz__r5_control_features_6_4z00, BgL_bgl_string1734za700za7za7_1967za7, "arg1154", 7 );
DEFINE_STRING( BGl_string1653z00zz__r5_control_features_6_4z00, BgL_bgl_string1653za700za7za7_1968za7, "arg1076", 7 );
DEFINE_STRING( BGl_string1816z00zz__r5_control_features_6_4z00, BgL_bgl_string1816za700za7za7_1969za7, "arg1227", 7 );
DEFINE_STRING( BGl_string1736z00zz__r5_control_features_6_4z00, BgL_bgl_string1736za700za7za7_1970za7, "arg1157", 7 );
DEFINE_STRING( BGl_string1818z00zz__r5_control_features_6_4z00, BgL_bgl_string1818za700za7za7_1971za7, "arg1228", 7 );
DEFINE_STRING( BGl_string1656z00zz__r5_control_features_6_4z00, BgL_bgl_string1656za700za7za7_1972za7, "arg1078", 7 );
DEFINE_STRING( BGl_string1738z00zz__r5_control_features_6_4z00, BgL_bgl_string1738za700za7za7_1973za7, "arg1158", 7 );
DEFINE_STRING( BGl_string1658z00zz__r5_control_features_6_4z00, BgL_bgl_string1658za700za7za7_1974za7, "arg1079", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52getzd2mvalueszd2valzd2envz80zz__r5_control_features_6_4z00, BgL_bgl_za762za752getza7d2mval1975za7, BGl_z62z52getzd2mvalueszd2valz30zz__r5_control_features_6_4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1900z00zz__r5_control_features_6_4z00, BgL_bgl_string1900za700za7za7_1976za7, "arg1328", 7 );
DEFINE_STRING( BGl_string1901z00zz__r5_control_features_6_4z00, BgL_bgl_string1901za700za7za7_1977za7, "&call-with-values", 17 );
DEFINE_STRING( BGl_string1820z00zz__r5_control_features_6_4z00, BgL_bgl_string1820za700za7za7_1978za7, "arg1229", 7 );
DEFINE_STRING( BGl_string1902z00zz__r5_control_features_6_4z00, BgL_bgl_string1902za700za7za7_1979za7, "procedure", 9 );
DEFINE_STRING( BGl_string1740z00zz__r5_control_features_6_4z00, BgL_bgl_string1740za700za7za7_1980za7, "arg1162", 7 );
DEFINE_STRING( BGl_string1903z00zz__r5_control_features_6_4z00, BgL_bgl_string1903za700za7za7_1981za7, "__r5_control_features_6_4", 25 );
DEFINE_STRING( BGl_string1822z00zz__r5_control_features_6_4z00, BgL_bgl_string1822za700za7za7_1982za7, "arg1230", 7 );
DEFINE_STRING( BGl_string1660z00zz__r5_control_features_6_4z00, BgL_bgl_string1660za700za7za7_1983za7, "arg1080", 7 );
DEFINE_STRING( BGl_string1742z00zz__r5_control_features_6_4z00, BgL_bgl_string1742za700za7za7_1984za7, "arg1164", 7 );
DEFINE_STRING( BGl_string1824z00zz__r5_control_features_6_4z00, BgL_bgl_string1824za700za7za7_1985za7, "arg1231", 7 );
DEFINE_STRING( BGl_string1744z00zz__r5_control_features_6_4z00, BgL_bgl_string1744za700za7za7_1986za7, "arg1166", 7 );
DEFINE_STRING( BGl_string1663z00zz__r5_control_features_6_4z00, BgL_bgl_string1663za700za7za7_1987za7, "arg1082", 7 );
DEFINE_STRING( BGl_string1826z00zz__r5_control_features_6_4z00, BgL_bgl_string1826za700za7za7_1988za7, "arg1232", 7 );
DEFINE_STRING( BGl_string1665z00zz__r5_control_features_6_4z00, BgL_bgl_string1665za700za7za7_1989za7, "arg1083", 7 );
DEFINE_STRING( BGl_string1828z00zz__r5_control_features_6_4z00, BgL_bgl_string1828za700za7za7_1990za7, "arg1233", 7 );
DEFINE_STRING( BGl_string1747z00zz__r5_control_features_6_4z00, BgL_bgl_string1747za700za7za7_1991za7, "arg1171", 7 );
DEFINE_STRING( BGl_string1667z00zz__r5_control_features_6_4z00, BgL_bgl_string1667za700za7za7_1992za7, "arg1084", 7 );
DEFINE_STRING( BGl_string1749z00zz__r5_control_features_6_4z00, BgL_bgl_string1749za700za7za7_1993za7, "arg1172", 7 );
DEFINE_STRING( BGl_string1669z00zz__r5_control_features_6_4z00, BgL_bgl_string1669za700za7za7_1994za7, "arg1085", 7 );
DEFINE_STRING( BGl_string1830z00zz__r5_control_features_6_4z00, BgL_bgl_string1830za700za7za7_1995za7, "arg1234", 7 );
DEFINE_STRING( BGl_string1832z00zz__r5_control_features_6_4z00, BgL_bgl_string1832za700za7za7_1996za7, "arg1236", 7 );
DEFINE_STRING( BGl_string1751z00zz__r5_control_features_6_4z00, BgL_bgl_string1751za700za7za7_1997za7, "arg1182", 7 );
DEFINE_STRING( BGl_string1834z00zz__r5_control_features_6_4z00, BgL_bgl_string1834za700za7za7_1998za7, "arg1238", 7 );
DEFINE_STRING( BGl_string1753z00zz__r5_control_features_6_4z00, BgL_bgl_string1753za700za7za7_1999za7, "arg1183", 7 );
DEFINE_STRING( BGl_string1672z00zz__r5_control_features_6_4z00, BgL_bgl_string1672za700za7za7_2000za7, "arg1087", 7 );
DEFINE_STRING( BGl_string1836z00zz__r5_control_features_6_4z00, BgL_bgl_string1836za700za7za7_2001za7, "arg1239", 7 );
DEFINE_STRING( BGl_string1755z00zz__r5_control_features_6_4z00, BgL_bgl_string1755za700za7za7_2002za7, "arg1187", 7 );
DEFINE_STRING( BGl_string1674z00zz__r5_control_features_6_4z00, BgL_bgl_string1674za700za7za7_2003za7, "arg1088", 7 );
DEFINE_STRING( BGl_string1838z00zz__r5_control_features_6_4z00, BgL_bgl_string1838za700za7za7_2004za7, "arg1242", 7 );
DEFINE_STRING( BGl_string1757z00zz__r5_control_features_6_4z00, BgL_bgl_string1757za700za7za7_2005za7, "arg1188", 7 );
DEFINE_STRING( BGl_string1676z00zz__r5_control_features_6_4z00, BgL_bgl_string1676za700za7za7_2006za7, "arg1090", 7 );
DEFINE_STRING( BGl_string1759z00zz__r5_control_features_6_4z00, BgL_bgl_string1759za700za7za7_2007za7, "arg1189", 7 );
DEFINE_STRING( BGl_string1678z00zz__r5_control_features_6_4z00, BgL_bgl_string1678za700za7za7_2008za7, "arg1092", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52getzd2mvalueszd2numberzd2envz80zz__r5_control_features_6_4z00, BgL_bgl_za762za752getza7d2mval2009za7, BGl_z62z52getzd2mvalueszd2numberz30zz__r5_control_features_6_4z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1840z00zz__r5_control_features_6_4z00, BgL_bgl_string1840za700za7za7_2010za7, "arg1244", 7 );
DEFINE_STRING( BGl_string1761z00zz__r5_control_features_6_4z00, BgL_bgl_string1761za700za7za7_2011za7, "arg1190", 7 );
DEFINE_STRING( BGl_string1680z00zz__r5_control_features_6_4z00, BgL_bgl_string1680za700za7za7_2012za7, "arg1097", 7 );
DEFINE_STRING( BGl_string1843z00zz__r5_control_features_6_4z00, BgL_bgl_string1843za700za7za7_2013za7, "arg1248", 7 );
DEFINE_STRING( BGl_string1763z00zz__r5_control_features_6_4z00, BgL_bgl_string1763za700za7za7_2014za7, "arg1191", 7 );
DEFINE_STRING( BGl_string1845z00zz__r5_control_features_6_4z00, BgL_bgl_string1845za700za7za7_2015za7, "arg1249", 7 );
DEFINE_STRING( BGl_string1683z00zz__r5_control_features_6_4z00, BgL_bgl_string1683za700za7za7_2016za7, "arg1102", 7 );
DEFINE_STRING( BGl_string1765z00zz__r5_control_features_6_4z00, BgL_bgl_string1765za700za7za7_2017za7, "arg1193", 7 );
DEFINE_STRING( BGl_string1847z00zz__r5_control_features_6_4z00, BgL_bgl_string1847za700za7za7_2018za7, "arg1252", 7 );
DEFINE_STRING( BGl_string1685z00zz__r5_control_features_6_4z00, BgL_bgl_string1685za700za7za7_2019za7, "arg1103", 7 );
DEFINE_STRING( BGl_string1849z00zz__r5_control_features_6_4z00, BgL_bgl_string1849za700za7za7_2020za7, "arg1268", 7 );
DEFINE_STRING( BGl_string1768z00zz__r5_control_features_6_4z00, BgL_bgl_string1768za700za7za7_2021za7, "arg1194", 7 );
DEFINE_STRING( BGl_string1687z00zz__r5_control_features_6_4z00, BgL_bgl_string1687za700za7za7_2022za7, "arg1104", 7 );
DEFINE_STRING( BGl_string1689z00zz__r5_control_features_6_4z00, BgL_bgl_string1689za700za7za7_2023za7, "arg1114", 7 );
DEFINE_STRING( BGl_string1851z00zz__r5_control_features_6_4z00, BgL_bgl_string1851za700za7za7_2024za7, "arg1272", 7 );
DEFINE_STRING( BGl_string1770z00zz__r5_control_features_6_4z00, BgL_bgl_string1770za700za7za7_2025za7, "arg1196", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_valueszd2envzd2zz__r5_control_features_6_4z00, BgL_bgl_za762valuesza762za7za7__2026z00, va_generic_entry, BGl_z62valuesz62zz__r5_control_features_6_4z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string1853z00zz__r5_control_features_6_4z00, BgL_bgl_string1853za700za7za7_2027za7, "arg1284", 7 );
DEFINE_STRING( BGl_string1772z00zz__r5_control_features_6_4z00, BgL_bgl_string1772za700za7za7_2028za7, "arg1197", 7 );
DEFINE_STRING( BGl_string1691z00zz__r5_control_features_6_4z00, BgL_bgl_string1691za700za7za7_2029za7, "arg1115", 7 );
DEFINE_STRING( BGl_string1855z00zz__r5_control_features_6_4z00, BgL_bgl_string1855za700za7za7_2030za7, "arg1304", 7 );
DEFINE_STRING( BGl_string1774z00zz__r5_control_features_6_4z00, BgL_bgl_string1774za700za7za7_2031za7, "arg1198", 7 );
DEFINE_STRING( BGl_string1693z00zz__r5_control_features_6_4z00, BgL_bgl_string1693za700za7za7_2032za7, "arg1122", 7 );
DEFINE_STRING( BGl_string1857z00zz__r5_control_features_6_4z00, BgL_bgl_string1857za700za7za7_2033za7, "arg1305", 7 );
DEFINE_STRING( BGl_string1776z00zz__r5_control_features_6_4z00, BgL_bgl_string1776za700za7za7_2034za7, "arg1199", 7 );
DEFINE_STRING( BGl_string1696z00zz__r5_control_features_6_4z00, BgL_bgl_string1696za700za7za7_2035za7, "arg1123", 7 );
DEFINE_STRING( BGl_string1859z00zz__r5_control_features_6_4z00, BgL_bgl_string1859za700za7za7_2036za7, "arg1306", 7 );
DEFINE_STRING( BGl_string1778z00zz__r5_control_features_6_4z00, BgL_bgl_string1778za700za7za7_2037za7, "arg1200", 7 );
DEFINE_STRING( BGl_string1698z00zz__r5_control_features_6_4z00, BgL_bgl_string1698za700za7za7_2038za7, "arg1125", 7 );
DEFINE_STRING( BGl_string1861z00zz__r5_control_features_6_4z00, BgL_bgl_string1861za700za7za7_2039za7, "arg1307", 7 );
DEFINE_STRING( BGl_string1780z00zz__r5_control_features_6_4z00, BgL_bgl_string1780za700za7za7_2040za7, "arg1201", 7 );
DEFINE_STRING( BGl_string1863z00zz__r5_control_features_6_4z00, BgL_bgl_string1863za700za7za7_2041za7, "arg1308", 7 );
DEFINE_STRING( BGl_string1782z00zz__r5_control_features_6_4z00, BgL_bgl_string1782za700za7za7_2042za7, "arg1202", 7 );
DEFINE_STRING( BGl_string1865z00zz__r5_control_features_6_4z00, BgL_bgl_string1865za700za7za7_2043za7, "arg1309", 7 );
DEFINE_STRING( BGl_string1784z00zz__r5_control_features_6_4z00, BgL_bgl_string1784za700za7za7_2044za7, "arg1203", 7 );
DEFINE_STRING( BGl_string1867z00zz__r5_control_features_6_4z00, BgL_bgl_string1867za700za7za7_2045za7, "arg1310", 7 );
DEFINE_STRING( BGl_string1786z00zz__r5_control_features_6_4z00, BgL_bgl_string1786za700za7za7_2046za7, "arg1206", 7 );
DEFINE_STRING( BGl_string1869z00zz__r5_control_features_6_4z00, BgL_bgl_string1869za700za7za7_2047za7, "arg1311", 7 );
DEFINE_STRING( BGl_string1788z00zz__r5_control_features_6_4z00, BgL_bgl_string1788za700za7za7_2048za7, "arg1208", 7 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1831z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1750z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1833z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1752z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1671z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1835z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1754z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1673z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1837z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1756z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1675z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1839z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1758z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1677z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1644z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1726z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1645z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1679z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1646z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1649z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1760z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1842z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1762z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1844z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1682z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1764z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1846z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1684z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1814z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1848z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1767z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1686z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1654z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1769z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1688z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1850z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1852z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1771z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1690z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1854z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1773z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1692z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1856z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1775z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1661z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1695z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1858z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1777z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1745z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1697z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1779z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1699z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1860z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1862z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1781z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1864z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1783z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1670z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1866z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1785z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1868z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1787z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1871z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1790z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1873z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1792z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1841z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1875z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1794z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1681z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1877z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1796z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1879z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1798z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1766z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1881z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1883z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1885z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1887z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1889z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1694z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1891z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1893z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1895z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1897z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1899z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1789z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1870z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1701z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1703z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1705z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1707z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1710z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1630z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1712z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1632z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1714z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1634z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1716z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1718z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1638z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1800z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1720z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1802z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1640z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1722z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1804z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1642z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1724z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1806z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1808z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1727z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1647z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1729z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1810z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1812z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1731z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1650z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1733z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1652z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1815z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1735z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1817z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1655z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1737z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1819z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1657z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1739z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1659z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1709z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1629z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1821z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1741z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1823z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1743z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1662z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1825z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1664z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1827z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1746z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1666z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1829z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1748z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_symbol1668z00zz__r5_control_features_6_4z00) );
ADD_ROOT( (void *)(&BGl_list1637z00zz__r5_control_features_6_4z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long BgL_checksumz00_1495, char * BgL_fromz00_1496)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00))
{ 
BGl_requirezd2initializa7ationz75zz__r5_control_features_6_4z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r5_control_features_6_4z00(); 
BGl_cnstzd2initzd2zz__r5_control_features_6_4z00(); 
return 
BGl_importedzd2moduleszd2initz00zz__r5_control_features_6_4z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r5_control_features_6_4z00(void)
{
{ /* Ieee/control5.scm 14 */
BGl_symbol1630z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1631z00zz__r5_control_features_6_4z00); 
BGl_symbol1632z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1633z00zz__r5_control_features_6_4z00); 
BGl_list1629z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1632z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1632z00zz__r5_control_features_6_4z00, BNIL))); 
BGl_symbol1634z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1635z00zz__r5_control_features_6_4z00); 
BGl_symbol1638z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1639z00zz__r5_control_features_6_4z00); 
BGl_symbol1640z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1641z00zz__r5_control_features_6_4z00); 
BGl_symbol1642z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1643z00zz__r5_control_features_6_4z00); 
BGl_list1637z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1638z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, BNIL))); 
BGl_list1644z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, BNIL))); 
BGl_list1645z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, BNIL)))); 
BGl_symbol1647z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1648z00zz__r5_control_features_6_4z00); 
BGl_list1646z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1647z00zz__r5_control_features_6_4z00, BNIL))))); 
BGl_symbol1650z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1651z00zz__r5_control_features_6_4z00); 
BGl_symbol1652z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1653z00zz__r5_control_features_6_4z00); 
BGl_list1649z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1650z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1652z00zz__r5_control_features_6_4z00, BNIL)))))); 
BGl_symbol1655z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1656z00zz__r5_control_features_6_4z00); 
BGl_symbol1657z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1658z00zz__r5_control_features_6_4z00); 
BGl_symbol1659z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1660z00zz__r5_control_features_6_4z00); 
BGl_list1654z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1655z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1657z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1659z00zz__r5_control_features_6_4z00, BNIL))))))); 
BGl_symbol1662z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1663z00zz__r5_control_features_6_4z00); 
BGl_symbol1664z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1665z00zz__r5_control_features_6_4z00); 
BGl_symbol1666z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1667z00zz__r5_control_features_6_4z00); 
BGl_symbol1668z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1669z00zz__r5_control_features_6_4z00); 
BGl_list1661z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1662z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1664z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1666z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1668z00zz__r5_control_features_6_4z00, BNIL)))))))); 
BGl_symbol1671z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1672z00zz__r5_control_features_6_4z00); 
BGl_symbol1673z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1674z00zz__r5_control_features_6_4z00); 
BGl_symbol1675z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1676z00zz__r5_control_features_6_4z00); 
BGl_symbol1677z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1678z00zz__r5_control_features_6_4z00); 
BGl_symbol1679z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1680z00zz__r5_control_features_6_4z00); 
BGl_list1670z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1671z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1673z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1675z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1677z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1679z00zz__r5_control_features_6_4z00, BNIL))))))))); 
BGl_symbol1682z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1683z00zz__r5_control_features_6_4z00); 
BGl_symbol1684z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1685z00zz__r5_control_features_6_4z00); 
BGl_symbol1686z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1687z00zz__r5_control_features_6_4z00); 
BGl_symbol1688z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1689z00zz__r5_control_features_6_4z00); 
BGl_symbol1690z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1691z00zz__r5_control_features_6_4z00); 
BGl_symbol1692z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1693z00zz__r5_control_features_6_4z00); 
BGl_list1681z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1682z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1684z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1686z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1688z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1690z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1692z00zz__r5_control_features_6_4z00, BNIL)))))))))); 
BGl_symbol1695z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1696z00zz__r5_control_features_6_4z00); 
BGl_symbol1697z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1698z00zz__r5_control_features_6_4z00); 
BGl_symbol1699z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1700z00zz__r5_control_features_6_4z00); 
BGl_symbol1701z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1702z00zz__r5_control_features_6_4z00); 
BGl_symbol1703z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1704z00zz__r5_control_features_6_4z00); 
BGl_symbol1705z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1706z00zz__r5_control_features_6_4z00); 
BGl_symbol1707z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1708z00zz__r5_control_features_6_4z00); 
BGl_list1694z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1695z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1697z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1699z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1701z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1703z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1705z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1707z00zz__r5_control_features_6_4z00, BNIL))))))))))); 
BGl_symbol1710z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1711z00zz__r5_control_features_6_4z00); 
BGl_symbol1712z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1713z00zz__r5_control_features_6_4z00); 
BGl_symbol1714z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1715z00zz__r5_control_features_6_4z00); 
BGl_symbol1716z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1717z00zz__r5_control_features_6_4z00); 
BGl_symbol1718z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1719z00zz__r5_control_features_6_4z00); 
BGl_symbol1720z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1721z00zz__r5_control_features_6_4z00); 
BGl_symbol1722z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1723z00zz__r5_control_features_6_4z00); 
BGl_symbol1724z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1725z00zz__r5_control_features_6_4z00); 
BGl_list1709z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1710z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1712z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1714z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1716z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1718z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1720z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1722z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1724z00zz__r5_control_features_6_4z00, BNIL)))))))))))); 
BGl_symbol1727z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1728z00zz__r5_control_features_6_4z00); 
BGl_symbol1729z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1730z00zz__r5_control_features_6_4z00); 
BGl_symbol1731z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1732z00zz__r5_control_features_6_4z00); 
BGl_symbol1733z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1734z00zz__r5_control_features_6_4z00); 
BGl_symbol1735z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1736z00zz__r5_control_features_6_4z00); 
BGl_symbol1737z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1738z00zz__r5_control_features_6_4z00); 
BGl_symbol1739z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1740z00zz__r5_control_features_6_4z00); 
BGl_symbol1741z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1742z00zz__r5_control_features_6_4z00); 
BGl_symbol1743z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1744z00zz__r5_control_features_6_4z00); 
BGl_list1726z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1727z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1729z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1731z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1733z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1735z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1737z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1739z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1741z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1743z00zz__r5_control_features_6_4z00, BNIL))))))))))))); 
BGl_symbol1746z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1747z00zz__r5_control_features_6_4z00); 
BGl_symbol1748z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1749z00zz__r5_control_features_6_4z00); 
BGl_symbol1750z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1751z00zz__r5_control_features_6_4z00); 
BGl_symbol1752z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1753z00zz__r5_control_features_6_4z00); 
BGl_symbol1754z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1755z00zz__r5_control_features_6_4z00); 
BGl_symbol1756z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1757z00zz__r5_control_features_6_4z00); 
BGl_symbol1758z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1759z00zz__r5_control_features_6_4z00); 
BGl_symbol1760z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1761z00zz__r5_control_features_6_4z00); 
BGl_symbol1762z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1763z00zz__r5_control_features_6_4z00); 
BGl_symbol1764z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1765z00zz__r5_control_features_6_4z00); 
BGl_list1745z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1746z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1748z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1750z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1752z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1754z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1756z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1758z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1760z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1762z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1764z00zz__r5_control_features_6_4z00, BNIL)))))))))))))); 
BGl_symbol1767z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1768z00zz__r5_control_features_6_4z00); 
BGl_symbol1769z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1770z00zz__r5_control_features_6_4z00); 
BGl_symbol1771z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1772z00zz__r5_control_features_6_4z00); 
BGl_symbol1773z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1774z00zz__r5_control_features_6_4z00); 
BGl_symbol1775z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1776z00zz__r5_control_features_6_4z00); 
BGl_symbol1777z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1778z00zz__r5_control_features_6_4z00); 
BGl_symbol1779z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1780z00zz__r5_control_features_6_4z00); 
BGl_symbol1781z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1782z00zz__r5_control_features_6_4z00); 
BGl_symbol1783z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1784z00zz__r5_control_features_6_4z00); 
BGl_symbol1785z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1786z00zz__r5_control_features_6_4z00); 
BGl_symbol1787z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1788z00zz__r5_control_features_6_4z00); 
BGl_list1766z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1767z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1769z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1771z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1773z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1777z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1779z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1781z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1783z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1785z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1787z00zz__r5_control_features_6_4z00, BNIL))))))))))))))); 
BGl_symbol1790z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1791z00zz__r5_control_features_6_4z00); 
BGl_symbol1792z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1793z00zz__r5_control_features_6_4z00); 
BGl_symbol1794z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1795z00zz__r5_control_features_6_4z00); 
BGl_symbol1796z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1797z00zz__r5_control_features_6_4z00); 
BGl_symbol1798z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1799z00zz__r5_control_features_6_4z00); 
BGl_symbol1800z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1801z00zz__r5_control_features_6_4z00); 
BGl_symbol1802z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1803z00zz__r5_control_features_6_4z00); 
BGl_symbol1804z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1805z00zz__r5_control_features_6_4z00); 
BGl_symbol1806z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1807z00zz__r5_control_features_6_4z00); 
BGl_symbol1808z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1809z00zz__r5_control_features_6_4z00); 
BGl_symbol1810z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1811z00zz__r5_control_features_6_4z00); 
BGl_symbol1812z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1813z00zz__r5_control_features_6_4z00); 
BGl_list1789z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1790z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1792z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1794z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1796z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1798z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1800z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1802z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1804z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1806z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1808z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1810z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1812z00zz__r5_control_features_6_4z00, BNIL)))))))))))))))); 
BGl_symbol1815z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1816z00zz__r5_control_features_6_4z00); 
BGl_symbol1817z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1818z00zz__r5_control_features_6_4z00); 
BGl_symbol1819z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1820z00zz__r5_control_features_6_4z00); 
BGl_symbol1821z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1822z00zz__r5_control_features_6_4z00); 
BGl_symbol1823z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1824z00zz__r5_control_features_6_4z00); 
BGl_symbol1825z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1826z00zz__r5_control_features_6_4z00); 
BGl_symbol1827z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1828z00zz__r5_control_features_6_4z00); 
BGl_symbol1829z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1830z00zz__r5_control_features_6_4z00); 
BGl_symbol1831z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1832z00zz__r5_control_features_6_4z00); 
BGl_symbol1833z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1834z00zz__r5_control_features_6_4z00); 
BGl_symbol1835z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1836z00zz__r5_control_features_6_4z00); 
BGl_symbol1837z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1838z00zz__r5_control_features_6_4z00); 
BGl_symbol1839z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1840z00zz__r5_control_features_6_4z00); 
BGl_list1814z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1815z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1817z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1819z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1821z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1823z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1825z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1827z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1829z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1831z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1833z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1837z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1839z00zz__r5_control_features_6_4z00, BNIL))))))))))))))))); 
BGl_symbol1842z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1843z00zz__r5_control_features_6_4z00); 
BGl_symbol1844z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1845z00zz__r5_control_features_6_4z00); 
BGl_symbol1846z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1847z00zz__r5_control_features_6_4z00); 
BGl_symbol1848z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1849z00zz__r5_control_features_6_4z00); 
BGl_symbol1850z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1851z00zz__r5_control_features_6_4z00); 
BGl_symbol1852z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1853z00zz__r5_control_features_6_4z00); 
BGl_symbol1854z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1855z00zz__r5_control_features_6_4z00); 
BGl_symbol1856z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1857z00zz__r5_control_features_6_4z00); 
BGl_symbol1858z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1859z00zz__r5_control_features_6_4z00); 
BGl_symbol1860z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1861z00zz__r5_control_features_6_4z00); 
BGl_symbol1862z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1863z00zz__r5_control_features_6_4z00); 
BGl_symbol1864z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1865z00zz__r5_control_features_6_4z00); 
BGl_symbol1866z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1867z00zz__r5_control_features_6_4z00); 
BGl_symbol1868z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1869z00zz__r5_control_features_6_4z00); 
BGl_list1841z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1842z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1846z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1848z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1850z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1852z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1854z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1856z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1858z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1860z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1862z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1864z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1866z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1868z00zz__r5_control_features_6_4z00, BNIL)))))))))))))))))); 
BGl_symbol1871z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1872z00zz__r5_control_features_6_4z00); 
BGl_symbol1873z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1874z00zz__r5_control_features_6_4z00); 
BGl_symbol1875z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1876z00zz__r5_control_features_6_4z00); 
BGl_symbol1877z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1878z00zz__r5_control_features_6_4z00); 
BGl_symbol1879z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1880z00zz__r5_control_features_6_4z00); 
BGl_symbol1881z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1882z00zz__r5_control_features_6_4z00); 
BGl_symbol1883z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1884z00zz__r5_control_features_6_4z00); 
BGl_symbol1885z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1886z00zz__r5_control_features_6_4z00); 
BGl_symbol1887z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1888z00zz__r5_control_features_6_4z00); 
BGl_symbol1889z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1890z00zz__r5_control_features_6_4z00); 
BGl_symbol1891z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1892z00zz__r5_control_features_6_4z00); 
BGl_symbol1893z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1894z00zz__r5_control_features_6_4z00); 
BGl_symbol1895z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1896z00zz__r5_control_features_6_4z00); 
BGl_symbol1897z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1898z00zz__r5_control_features_6_4z00); 
BGl_symbol1899z00zz__r5_control_features_6_4z00 = 
bstring_to_symbol(BGl_string1900z00zz__r5_control_features_6_4z00); 
return ( 
BGl_list1870z00zz__r5_control_features_6_4z00 = 
MAKE_YOUNG_PAIR(BGl_symbol1630z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1640z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1642z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1871z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1873z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1875z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1877z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1879z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1881z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1883z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1885z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1887z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1889z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1891z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1893z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1895z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1897z00zz__r5_control_features_6_4z00, 
MAKE_YOUNG_PAIR(BGl_symbol1899z00zz__r5_control_features_6_4z00, BNIL))))))))))))))))))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r5_control_features_6_4z00(void)
{
{ /* Ieee/control5.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* %get-mvalues-number */
BGL_EXPORTED_DEF int BGl_z52getzd2mvalueszd2numberz52zz__r5_control_features_6_4z00(void)
{
{ /* Ieee/control5.scm 66 */
return 
BGL_MVALUES_NUMBER();} 

}



/* &%get-mvalues-number */
obj_t BGl_z62z52getzd2mvalueszd2numberz30zz__r5_control_features_6_4z00(obj_t BgL_envz00_1294)
{
{ /* Ieee/control5.scm 66 */
return 
BINT(
BGl_z52getzd2mvalueszd2numberz52zz__r5_control_features_6_4z00());} 

}



/* %set-mvalues-number! */
BGL_EXPORTED_DEF int BGl_z52setzd2mvalueszd2numberz12z40zz__r5_control_features_6_4z00(int BgL_nz00_3)
{
{ /* Ieee/control5.scm 72 */
return 
BGL_MVALUES_NUMBER_SET(BgL_nz00_3);} 

}



/* &%set-mvalues-number! */
obj_t BGl_z62z52setzd2mvalueszd2numberz12z22zz__r5_control_features_6_4z00(obj_t BgL_envz00_1295, obj_t BgL_nz00_1296)
{
{ /* Ieee/control5.scm 72 */
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1827;
{ /* Ieee/control5.scm 73 */
 int BgL_auxz00_1828;
{ /* Ieee/control5.scm 73 */
 obj_t BgL_tmpz00_1829;
if(
INTEGERP(BgL_nz00_1296))
{ /* Ieee/control5.scm 73 */
BgL_tmpz00_1829 = BgL_nz00_1296
; }  else 
{ 
 obj_t BgL_auxz00_1832;
BgL_auxz00_1832 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(2768L), BGl_string1621z00zz__r5_control_features_6_4z00, BGl_string1622z00zz__r5_control_features_6_4z00, BgL_nz00_1296); 
FAILURE(BgL_auxz00_1832,BFALSE,BFALSE);} 
BgL_auxz00_1828 = 
CINT(BgL_tmpz00_1829); } 
BgL_tmpz00_1827 = 
BGl_z52setzd2mvalueszd2numberz12z40zz__r5_control_features_6_4z00(BgL_auxz00_1828); } 
return 
BINT(BgL_tmpz00_1827);} } 

}



/* %get-mvalues-val */
BGL_EXPORTED_DEF obj_t BGl_z52getzd2mvalueszd2valz52zz__r5_control_features_6_4z00(int BgL_nz00_4)
{
{ /* Ieee/control5.scm 78 */
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1494;
BgL_tmpz00_1494 = 
BGL_MVALUES_VAL(BgL_nz00_4); 
BGL_MVALUES_VAL_SET(BgL_nz00_4, BUNSPEC); 
return BgL_tmpz00_1494;} } 

}



/* &%get-mvalues-val */
obj_t BGl_z62z52getzd2mvalueszd2valz30zz__r5_control_features_6_4z00(obj_t BgL_envz00_1297, obj_t BgL_nz00_1298)
{
{ /* Ieee/control5.scm 78 */
{ /* Ieee/control5.scm 79 */
 int BgL_auxz00_1841;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1842;
if(
INTEGERP(BgL_nz00_1298))
{ /* Ieee/control5.scm 79 */
BgL_tmpz00_1842 = BgL_nz00_1298
; }  else 
{ 
 obj_t BgL_auxz00_1845;
BgL_auxz00_1845 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(3056L), BGl_string1623z00zz__r5_control_features_6_4z00, BGl_string1622z00zz__r5_control_features_6_4z00, BgL_nz00_1298); 
FAILURE(BgL_auxz00_1845,BFALSE,BFALSE);} 
BgL_auxz00_1841 = 
CINT(BgL_tmpz00_1842); } 
return 
BGl_z52getzd2mvalueszd2valz52zz__r5_control_features_6_4z00(BgL_auxz00_1841);} } 

}



/* %set-mvalues-val! */
BGL_EXPORTED_DEF obj_t BGl_z52setzd2mvalueszd2valz12z40zz__r5_control_features_6_4z00(int BgL_nz00_5, obj_t BgL_oz00_6)
{
{ /* Ieee/control5.scm 86 */
return 
BGL_MVALUES_VAL_SET(BgL_nz00_5, BgL_oz00_6);} 

}



/* &%set-mvalues-val! */
obj_t BGl_z62z52setzd2mvalueszd2valz12z22zz__r5_control_features_6_4z00(obj_t BgL_envz00_1299, obj_t BgL_nz00_1300, obj_t BgL_oz00_1301)
{
{ /* Ieee/control5.scm 86 */
{ /* Ieee/control5.scm 87 */
 int BgL_auxz00_1852;
{ /* Ieee/control5.scm 87 */
 obj_t BgL_tmpz00_1853;
if(
INTEGERP(BgL_nz00_1300))
{ /* Ieee/control5.scm 87 */
BgL_tmpz00_1853 = BgL_nz00_1300
; }  else 
{ 
 obj_t BgL_auxz00_1856;
BgL_auxz00_1856 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(3408L), BGl_string1624z00zz__r5_control_features_6_4z00, BGl_string1622z00zz__r5_control_features_6_4z00, BgL_nz00_1300); 
FAILURE(BgL_auxz00_1856,BFALSE,BFALSE);} 
BgL_auxz00_1852 = 
CINT(BgL_tmpz00_1853); } 
return 
BGl_z52setzd2mvalueszd2valz12z40zz__r5_control_features_6_4z00(BgL_auxz00_1852, BgL_oz00_1301);} } 

}



/* values */
BGL_EXPORTED_DEF obj_t BGl_valuesz00zz__r5_control_features_6_4z00(obj_t BgL_argsz00_7)
{
{ /* Ieee/control5.scm 94 */
if(
NULLP(BgL_argsz00_7))
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1864;
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1865;
BgL_tmpz00_1865 = 
(int)(0L); 
BgL_tmpz00_1864 = 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1865); } 
return 
BINT(BgL_tmpz00_1864);}  else 
{ /* Ieee/control5.scm 97 */
 bool_t BgL_test2054z00_1869;
{ /* Ieee/control5.scm 97 */
 obj_t BgL_tmpz00_1870;
{ /* Ieee/control5.scm 97 */
 obj_t BgL_pairz00_1107;
if(
PAIRP(BgL_argsz00_7))
{ /* Ieee/control5.scm 97 */
BgL_pairz00_1107 = BgL_argsz00_7; }  else 
{ 
 obj_t BgL_auxz00_1873;
BgL_auxz00_1873 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(3903L), BGl_string1625z00zz__r5_control_features_6_4z00, BGl_string1626z00zz__r5_control_features_6_4z00, BgL_argsz00_7); 
FAILURE(BgL_auxz00_1873,BFALSE,BFALSE);} 
BgL_tmpz00_1870 = 
CDR(BgL_pairz00_1107); } 
BgL_test2054z00_1869 = 
NULLP(BgL_tmpz00_1870); } 
if(BgL_test2054z00_1869)
{ /* Ieee/control5.scm 97 */
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1879;
BgL_tmpz00_1879 = 
(int)(1L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1879); } 
{ /* Ieee/control5.scm 100 */
 obj_t BgL_pairz00_1108;
if(
PAIRP(BgL_argsz00_7))
{ /* Ieee/control5.scm 100 */
BgL_pairz00_1108 = BgL_argsz00_7; }  else 
{ 
 obj_t BgL_auxz00_1884;
BgL_auxz00_1884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(3965L), BGl_string1625z00zz__r5_control_features_6_4z00, BGl_string1626z00zz__r5_control_features_6_4z00, BgL_argsz00_7); 
FAILURE(BgL_auxz00_1884,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_1108);} }  else 
{ /* Ieee/control5.scm 101 */
 obj_t BgL_res0z00_712;
{ /* Ieee/control5.scm 101 */
 obj_t BgL_pairz00_1109;
if(
PAIRP(BgL_argsz00_7))
{ /* Ieee/control5.scm 101 */
BgL_pairz00_1109 = BgL_argsz00_7; }  else 
{ 
 obj_t BgL_auxz00_1891;
BgL_auxz00_1891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(3993L), BGl_string1625z00zz__r5_control_features_6_4z00, BGl_string1626z00zz__r5_control_features_6_4z00, BgL_argsz00_7); 
FAILURE(BgL_auxz00_1891,BFALSE,BFALSE);} 
BgL_res0z00_712 = 
CAR(BgL_pairz00_1109); } 
{ /* Ieee/control5.scm 103 */
 obj_t BgL_g1012z00_714;
{ /* Ieee/control5.scm 104 */
 obj_t BgL_pairz00_1110;
if(
PAIRP(BgL_argsz00_7))
{ /* Ieee/control5.scm 104 */
BgL_pairz00_1110 = BgL_argsz00_7; }  else 
{ 
 obj_t BgL_auxz00_1898;
BgL_auxz00_1898 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(4059L), BGl_string1625z00zz__r5_control_features_6_4z00, BGl_string1626z00zz__r5_control_features_6_4z00, BgL_argsz00_7); 
FAILURE(BgL_auxz00_1898,BFALSE,BFALSE);} 
BgL_g1012z00_714 = 
CDR(BgL_pairz00_1110); } 
{ 
 long BgL_iz00_716; obj_t BgL_argsz00_717;
BgL_iz00_716 = 1L; 
BgL_argsz00_717 = BgL_g1012z00_714; 
BgL_zc3z04anonymousza31060ze3z87_718:
if(
NULLP(BgL_argsz00_717))
{ /* Ieee/control5.scm 106 */
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1905;
BgL_tmpz00_1905 = 
(int)(BgL_iz00_716); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1905); } 
return BgL_res0z00_712;}  else 
{ /* Ieee/control5.scm 106 */
if(
(BgL_iz00_716==16L))
{ /* Ieee/control5.scm 109 */
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1910;
BgL_tmpz00_1910 = 
(int)(-1L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1910); } 
return BgL_argsz00_7;}  else 
{ /* Ieee/control5.scm 109 */
{ /* Ieee/control5.scm 113 */
 obj_t BgL_arg1063z00_721;
{ /* Ieee/control5.scm 113 */
 obj_t BgL_pairz00_1113;
if(
PAIRP(BgL_argsz00_717))
{ /* Ieee/control5.scm 113 */
BgL_pairz00_1113 = BgL_argsz00_717; }  else 
{ 
 obj_t BgL_auxz00_1915;
BgL_auxz00_1915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(4254L), BGl_string1627z00zz__r5_control_features_6_4z00, BGl_string1626z00zz__r5_control_features_6_4z00, BgL_argsz00_717); 
FAILURE(BgL_auxz00_1915,BFALSE,BFALSE);} 
BgL_arg1063z00_721 = 
CAR(BgL_pairz00_1113); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_1920;
BgL_tmpz00_1920 = 
(int)(BgL_iz00_716); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_1920, BgL_arg1063z00_721); } } 
{ /* Ieee/control5.scm 114 */
 long BgL_arg1065z00_722; obj_t BgL_arg1066z00_723;
BgL_arg1065z00_722 = 
(BgL_iz00_716+1L); 
{ /* Ieee/control5.scm 114 */
 obj_t BgL_pairz00_1116;
if(
PAIRP(BgL_argsz00_717))
{ /* Ieee/control5.scm 114 */
BgL_pairz00_1116 = BgL_argsz00_717; }  else 
{ 
 obj_t BgL_auxz00_1926;
BgL_auxz00_1926 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(4289L), BGl_string1627z00zz__r5_control_features_6_4z00, BGl_string1626z00zz__r5_control_features_6_4z00, BgL_argsz00_717); 
FAILURE(BgL_auxz00_1926,BFALSE,BFALSE);} 
BgL_arg1066z00_723 = 
CDR(BgL_pairz00_1116); } 
{ 
 obj_t BgL_argsz00_1932; long BgL_iz00_1931;
BgL_iz00_1931 = BgL_arg1065z00_722; 
BgL_argsz00_1932 = BgL_arg1066z00_723; 
BgL_argsz00_717 = BgL_argsz00_1932; 
BgL_iz00_716 = BgL_iz00_1931; 
goto BgL_zc3z04anonymousza31060ze3z87_718;} } } } } } } } } 

}



/* &values */
obj_t BGl_z62valuesz62zz__r5_control_features_6_4z00(obj_t BgL_envz00_1302, obj_t BgL_argsz00_1303)
{
{ /* Ieee/control5.scm 94 */
return 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_argsz00_1303);} 

}



/* call-with-values */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2valuesz00zz__r5_control_features_6_4z00(obj_t BgL_producerz00_8, obj_t BgL_consumerz00_9)
{
{ /* Ieee/control5.scm 119 */
{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1934;
BgL_tmpz00_1934 = 
(int)(1L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1934); } 
{ /* Ieee/control5.scm 121 */
 obj_t BgL_res0z00_726;
if(
PROCEDURE_CORRECT_ARITYP(BgL_producerz00_8, 0))
{ /* Ieee/control5.scm 121 */
BgL_res0z00_726 = 
BGL_PROCEDURE_CALL0(BgL_producerz00_8); }  else 
{ /* Ieee/control5.scm 121 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1629z00zz__r5_control_features_6_4z00,BgL_producerz00_8);} 
{ /* Ieee/control5.scm 121 */
 int BgL_numzd2valueszd2_727;
BgL_numzd2valueszd2_727 = 
BGL_MVALUES_NUMBER(); 
{ /* Ieee/control5.scm 122 */

{ /* Ieee/control5.scm 73 */
 int BgL_tmpz00_1944;
BgL_tmpz00_1944 = 
(int)(1L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1944); } 
switch( 
(long)(BgL_numzd2valueszd2_727)) { case -1L : 

{ /* Ieee/control5.scm 135 */
 int BgL_len1597z00_1326;
BgL_len1597z00_1326 = 
(int)(
bgl_list_length(
((obj_t)BgL_res0z00_726))); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, BgL_len1597z00_1326))
{ /* Ieee/control5.scm 135 */
return 
apply(BgL_consumerz00_9, 
((obj_t)BgL_res0z00_726));}  else 
{ /* Ieee/control5.scm 135 */
FAILURE(BGl_symbol1634z00zz__r5_control_features_6_4z00,BGl_string1636z00zz__r5_control_features_6_4z00,BGl_list1637z00zz__r5_control_features_6_4z00);} } break;case 0L : 

if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 0))
{ /* Ieee/control5.scm 137 */
return 
BGL_PROCEDURE_CALL0(BgL_consumerz00_9);}  else 
{ /* Ieee/control5.scm 137 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1644z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} break;case 1L : 

if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 1))
{ /* Ieee/control5.scm 139 */
return 
BGL_PROCEDURE_CALL1(BgL_consumerz00_9, BgL_res0z00_726);}  else 
{ /* Ieee/control5.scm 139 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1645z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} break;case 2L : 

{ /* Ieee/control5.scm 142 */
 obj_t BgL_arg1074z00_730;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1117;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_1969;
BgL_tmpz00_1969 = 
(int)(1L); 
BgL_tmpz00_1117 = 
BGL_MVALUES_VAL(BgL_tmpz00_1969); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_1972;
BgL_tmpz00_1972 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_1972, BUNSPEC); } 
BgL_arg1074z00_730 = BgL_tmpz00_1117; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 2))
{ /* Ieee/control5.scm 141 */
return 
BGL_PROCEDURE_CALL2(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1074z00_730);}  else 
{ /* Ieee/control5.scm 141 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1646z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 3L : 

{ /* Ieee/control5.scm 145 */
 obj_t BgL_arg1075z00_731; obj_t BgL_arg1076z00_732;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1118;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_1983;
BgL_tmpz00_1983 = 
(int)(1L); 
BgL_tmpz00_1118 = 
BGL_MVALUES_VAL(BgL_tmpz00_1983); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_1986;
BgL_tmpz00_1986 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_1986, BUNSPEC); } 
BgL_arg1075z00_731 = BgL_tmpz00_1118; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1119;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_1989;
BgL_tmpz00_1989 = 
(int)(2L); 
BgL_tmpz00_1119 = 
BGL_MVALUES_VAL(BgL_tmpz00_1989); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_1992;
BgL_tmpz00_1992 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_1992, BUNSPEC); } 
BgL_arg1076z00_732 = BgL_tmpz00_1119; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 3))
{ /* Ieee/control5.scm 144 */
return 
BGL_PROCEDURE_CALL3(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1075z00_731, BgL_arg1076z00_732);}  else 
{ /* Ieee/control5.scm 144 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1649z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 4L : 

{ /* Ieee/control5.scm 149 */
 obj_t BgL_arg1078z00_733; obj_t BgL_arg1079z00_734; obj_t BgL_arg1080z00_735;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1120;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2004;
BgL_tmpz00_2004 = 
(int)(1L); 
BgL_tmpz00_1120 = 
BGL_MVALUES_VAL(BgL_tmpz00_2004); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2007;
BgL_tmpz00_2007 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2007, BUNSPEC); } 
BgL_arg1078z00_733 = BgL_tmpz00_1120; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1121;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2010;
BgL_tmpz00_2010 = 
(int)(2L); 
BgL_tmpz00_1121 = 
BGL_MVALUES_VAL(BgL_tmpz00_2010); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2013;
BgL_tmpz00_2013 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2013, BUNSPEC); } 
BgL_arg1079z00_734 = BgL_tmpz00_1121; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1122;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2016;
BgL_tmpz00_2016 = 
(int)(3L); 
BgL_tmpz00_1122 = 
BGL_MVALUES_VAL(BgL_tmpz00_2016); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2019;
BgL_tmpz00_2019 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2019, BUNSPEC); } 
BgL_arg1080z00_735 = BgL_tmpz00_1122; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 4))
{ /* Ieee/control5.scm 148 */
return 
BGL_PROCEDURE_CALL4(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1078z00_733, BgL_arg1079z00_734, BgL_arg1080z00_735);}  else 
{ /* Ieee/control5.scm 148 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1654z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 5L : 

{ /* Ieee/control5.scm 154 */
 obj_t BgL_arg1082z00_736; obj_t BgL_arg1083z00_737; obj_t BgL_arg1084z00_738; obj_t BgL_arg1085z00_739;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1123;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2032;
BgL_tmpz00_2032 = 
(int)(1L); 
BgL_tmpz00_1123 = 
BGL_MVALUES_VAL(BgL_tmpz00_2032); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2035;
BgL_tmpz00_2035 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2035, BUNSPEC); } 
BgL_arg1082z00_736 = BgL_tmpz00_1123; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1124;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2038;
BgL_tmpz00_2038 = 
(int)(2L); 
BgL_tmpz00_1124 = 
BGL_MVALUES_VAL(BgL_tmpz00_2038); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2041;
BgL_tmpz00_2041 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2041, BUNSPEC); } 
BgL_arg1083z00_737 = BgL_tmpz00_1124; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1125;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2044;
BgL_tmpz00_2044 = 
(int)(3L); 
BgL_tmpz00_1125 = 
BGL_MVALUES_VAL(BgL_tmpz00_2044); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2047;
BgL_tmpz00_2047 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2047, BUNSPEC); } 
BgL_arg1084z00_738 = BgL_tmpz00_1125; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1126;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2050;
BgL_tmpz00_2050 = 
(int)(4L); 
BgL_tmpz00_1126 = 
BGL_MVALUES_VAL(BgL_tmpz00_2050); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2053;
BgL_tmpz00_2053 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2053, BUNSPEC); } 
BgL_arg1085z00_739 = BgL_tmpz00_1126; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 5))
{ /* Ieee/control5.scm 153 */
return 
BGL_PROCEDURE_CALL5(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1082z00_736, BgL_arg1083z00_737, BgL_arg1084z00_738, BgL_arg1085z00_739);}  else 
{ /* Ieee/control5.scm 153 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1661z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 6L : 

{ /* Ieee/control5.scm 160 */
 obj_t BgL_arg1087z00_740; obj_t BgL_arg1088z00_741; obj_t BgL_arg1090z00_742; obj_t BgL_arg1092z00_743; obj_t BgL_arg1097z00_744;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1127;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2067;
BgL_tmpz00_2067 = 
(int)(1L); 
BgL_tmpz00_1127 = 
BGL_MVALUES_VAL(BgL_tmpz00_2067); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2070;
BgL_tmpz00_2070 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2070, BUNSPEC); } 
BgL_arg1087z00_740 = BgL_tmpz00_1127; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1128;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2073;
BgL_tmpz00_2073 = 
(int)(2L); 
BgL_tmpz00_1128 = 
BGL_MVALUES_VAL(BgL_tmpz00_2073); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2076;
BgL_tmpz00_2076 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2076, BUNSPEC); } 
BgL_arg1088z00_741 = BgL_tmpz00_1128; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1129;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2079;
BgL_tmpz00_2079 = 
(int)(3L); 
BgL_tmpz00_1129 = 
BGL_MVALUES_VAL(BgL_tmpz00_2079); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2082;
BgL_tmpz00_2082 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2082, BUNSPEC); } 
BgL_arg1090z00_742 = BgL_tmpz00_1129; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1130;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2085;
BgL_tmpz00_2085 = 
(int)(4L); 
BgL_tmpz00_1130 = 
BGL_MVALUES_VAL(BgL_tmpz00_2085); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2088;
BgL_tmpz00_2088 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2088, BUNSPEC); } 
BgL_arg1092z00_743 = BgL_tmpz00_1130; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1131;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2091;
BgL_tmpz00_2091 = 
(int)(5L); 
BgL_tmpz00_1131 = 
BGL_MVALUES_VAL(BgL_tmpz00_2091); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2094;
BgL_tmpz00_2094 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2094, BUNSPEC); } 
BgL_arg1097z00_744 = BgL_tmpz00_1131; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 6))
{ /* Ieee/control5.scm 159 */
return 
BGL_PROCEDURE_CALL6(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1087z00_740, BgL_arg1088z00_741, BgL_arg1090z00_742, BgL_arg1092z00_743, BgL_arg1097z00_744);}  else 
{ /* Ieee/control5.scm 159 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1670z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 7L : 

{ /* Ieee/control5.scm 167 */
 obj_t BgL_arg1102z00_745; obj_t BgL_arg1103z00_746; obj_t BgL_arg1104z00_747; obj_t BgL_arg1114z00_748; obj_t BgL_arg1115z00_749; obj_t BgL_arg1122z00_750;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1132;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2109;
BgL_tmpz00_2109 = 
(int)(1L); 
BgL_tmpz00_1132 = 
BGL_MVALUES_VAL(BgL_tmpz00_2109); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2112;
BgL_tmpz00_2112 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2112, BUNSPEC); } 
BgL_arg1102z00_745 = BgL_tmpz00_1132; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1133;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2115;
BgL_tmpz00_2115 = 
(int)(2L); 
BgL_tmpz00_1133 = 
BGL_MVALUES_VAL(BgL_tmpz00_2115); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2118;
BgL_tmpz00_2118 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2118, BUNSPEC); } 
BgL_arg1103z00_746 = BgL_tmpz00_1133; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1134;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2121;
BgL_tmpz00_2121 = 
(int)(3L); 
BgL_tmpz00_1134 = 
BGL_MVALUES_VAL(BgL_tmpz00_2121); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2124;
BgL_tmpz00_2124 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2124, BUNSPEC); } 
BgL_arg1104z00_747 = BgL_tmpz00_1134; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1135;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2127;
BgL_tmpz00_2127 = 
(int)(4L); 
BgL_tmpz00_1135 = 
BGL_MVALUES_VAL(BgL_tmpz00_2127); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2130;
BgL_tmpz00_2130 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2130, BUNSPEC); } 
BgL_arg1114z00_748 = BgL_tmpz00_1135; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1136;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2133;
BgL_tmpz00_2133 = 
(int)(5L); 
BgL_tmpz00_1136 = 
BGL_MVALUES_VAL(BgL_tmpz00_2133); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2136;
BgL_tmpz00_2136 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2136, BUNSPEC); } 
BgL_arg1115z00_749 = BgL_tmpz00_1136; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1137;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2139;
BgL_tmpz00_2139 = 
(int)(6L); 
BgL_tmpz00_1137 = 
BGL_MVALUES_VAL(BgL_tmpz00_2139); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2142;
BgL_tmpz00_2142 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2142, BUNSPEC); } 
BgL_arg1122z00_750 = BgL_tmpz00_1137; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 7))
{ /* Ieee/control5.scm 166 */
return 
BGL_PROCEDURE_CALL7(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1102z00_745, BgL_arg1103z00_746, BgL_arg1104z00_747, BgL_arg1114z00_748, BgL_arg1115z00_749, BgL_arg1122z00_750);}  else 
{ /* Ieee/control5.scm 166 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1681z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 8L : 

{ /* Ieee/control5.scm 175 */
 obj_t BgL_arg1123z00_751; obj_t BgL_arg1125z00_752; obj_t BgL_arg1126z00_753; obj_t BgL_arg1127z00_754; obj_t BgL_arg1129z00_755; obj_t BgL_arg1131z00_756; obj_t BgL_arg1132z00_757;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1138;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2158;
BgL_tmpz00_2158 = 
(int)(1L); 
BgL_tmpz00_1138 = 
BGL_MVALUES_VAL(BgL_tmpz00_2158); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2161;
BgL_tmpz00_2161 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2161, BUNSPEC); } 
BgL_arg1123z00_751 = BgL_tmpz00_1138; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1139;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2164;
BgL_tmpz00_2164 = 
(int)(2L); 
BgL_tmpz00_1139 = 
BGL_MVALUES_VAL(BgL_tmpz00_2164); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2167;
BgL_tmpz00_2167 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2167, BUNSPEC); } 
BgL_arg1125z00_752 = BgL_tmpz00_1139; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1140;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2170;
BgL_tmpz00_2170 = 
(int)(3L); 
BgL_tmpz00_1140 = 
BGL_MVALUES_VAL(BgL_tmpz00_2170); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2173;
BgL_tmpz00_2173 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2173, BUNSPEC); } 
BgL_arg1126z00_753 = BgL_tmpz00_1140; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1141;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2176;
BgL_tmpz00_2176 = 
(int)(4L); 
BgL_tmpz00_1141 = 
BGL_MVALUES_VAL(BgL_tmpz00_2176); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2179;
BgL_tmpz00_2179 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2179, BUNSPEC); } 
BgL_arg1127z00_754 = BgL_tmpz00_1141; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1142;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2182;
BgL_tmpz00_2182 = 
(int)(5L); 
BgL_tmpz00_1142 = 
BGL_MVALUES_VAL(BgL_tmpz00_2182); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2185;
BgL_tmpz00_2185 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2185, BUNSPEC); } 
BgL_arg1129z00_755 = BgL_tmpz00_1142; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1143;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2188;
BgL_tmpz00_2188 = 
(int)(6L); 
BgL_tmpz00_1143 = 
BGL_MVALUES_VAL(BgL_tmpz00_2188); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2191;
BgL_tmpz00_2191 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2191, BUNSPEC); } 
BgL_arg1131z00_756 = BgL_tmpz00_1143; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1144;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2194;
BgL_tmpz00_2194 = 
(int)(7L); 
BgL_tmpz00_1144 = 
BGL_MVALUES_VAL(BgL_tmpz00_2194); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2197;
BgL_tmpz00_2197 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2197, BUNSPEC); } 
BgL_arg1132z00_757 = BgL_tmpz00_1144; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 8))
{ /* Ieee/control5.scm 174 */
return 
BGL_PROCEDURE_CALL8(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1123z00_751, BgL_arg1125z00_752, BgL_arg1126z00_753, BgL_arg1127z00_754, BgL_arg1129z00_755, BgL_arg1131z00_756, BgL_arg1132z00_757);}  else 
{ /* Ieee/control5.scm 174 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1694z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 9L : 

{ /* Ieee/control5.scm 184 */
 obj_t BgL_arg1137z00_758; obj_t BgL_arg1138z00_759; obj_t BgL_arg1140z00_760; obj_t BgL_arg1141z00_761; obj_t BgL_arg1142z00_762; obj_t BgL_arg1143z00_763; obj_t BgL_arg1145z00_764; obj_t BgL_arg1148z00_765;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1145;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2214;
BgL_tmpz00_2214 = 
(int)(1L); 
BgL_tmpz00_1145 = 
BGL_MVALUES_VAL(BgL_tmpz00_2214); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2217;
BgL_tmpz00_2217 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2217, BUNSPEC); } 
BgL_arg1137z00_758 = BgL_tmpz00_1145; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1146;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2220;
BgL_tmpz00_2220 = 
(int)(2L); 
BgL_tmpz00_1146 = 
BGL_MVALUES_VAL(BgL_tmpz00_2220); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2223;
BgL_tmpz00_2223 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2223, BUNSPEC); } 
BgL_arg1138z00_759 = BgL_tmpz00_1146; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1147;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2226;
BgL_tmpz00_2226 = 
(int)(3L); 
BgL_tmpz00_1147 = 
BGL_MVALUES_VAL(BgL_tmpz00_2226); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2229;
BgL_tmpz00_2229 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2229, BUNSPEC); } 
BgL_arg1140z00_760 = BgL_tmpz00_1147; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1148;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2232;
BgL_tmpz00_2232 = 
(int)(4L); 
BgL_tmpz00_1148 = 
BGL_MVALUES_VAL(BgL_tmpz00_2232); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2235;
BgL_tmpz00_2235 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2235, BUNSPEC); } 
BgL_arg1141z00_761 = BgL_tmpz00_1148; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1149;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2238;
BgL_tmpz00_2238 = 
(int)(5L); 
BgL_tmpz00_1149 = 
BGL_MVALUES_VAL(BgL_tmpz00_2238); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2241;
BgL_tmpz00_2241 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2241, BUNSPEC); } 
BgL_arg1142z00_762 = BgL_tmpz00_1149; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1150;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2244;
BgL_tmpz00_2244 = 
(int)(6L); 
BgL_tmpz00_1150 = 
BGL_MVALUES_VAL(BgL_tmpz00_2244); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2247;
BgL_tmpz00_2247 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2247, BUNSPEC); } 
BgL_arg1143z00_763 = BgL_tmpz00_1150; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1151;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2250;
BgL_tmpz00_2250 = 
(int)(7L); 
BgL_tmpz00_1151 = 
BGL_MVALUES_VAL(BgL_tmpz00_2250); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2253;
BgL_tmpz00_2253 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2253, BUNSPEC); } 
BgL_arg1145z00_764 = BgL_tmpz00_1151; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1152;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2256;
BgL_tmpz00_2256 = 
(int)(8L); 
BgL_tmpz00_1152 = 
BGL_MVALUES_VAL(BgL_tmpz00_2256); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2259;
BgL_tmpz00_2259 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2259, BUNSPEC); } 
BgL_arg1148z00_765 = BgL_tmpz00_1152; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 9))
{ /* Ieee/control5.scm 183 */
return 
BGL_PROCEDURE_CALL9(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1137z00_758, BgL_arg1138z00_759, BgL_arg1140z00_760, BgL_arg1141z00_761, BgL_arg1142z00_762, BgL_arg1143z00_763, BgL_arg1145z00_764, BgL_arg1148z00_765);}  else 
{ /* Ieee/control5.scm 183 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1709z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 10L : 

{ /* Ieee/control5.scm 194 */
 obj_t BgL_arg1149z00_766; obj_t BgL_arg1152z00_767; obj_t BgL_arg1153z00_768; obj_t BgL_arg1154z00_769; obj_t BgL_arg1157z00_770; obj_t BgL_arg1158z00_771; obj_t BgL_arg1162z00_772; obj_t BgL_arg1164z00_773; obj_t BgL_arg1166z00_774;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1153;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2277;
BgL_tmpz00_2277 = 
(int)(1L); 
BgL_tmpz00_1153 = 
BGL_MVALUES_VAL(BgL_tmpz00_2277); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2280;
BgL_tmpz00_2280 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2280, BUNSPEC); } 
BgL_arg1149z00_766 = BgL_tmpz00_1153; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1154;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2283;
BgL_tmpz00_2283 = 
(int)(2L); 
BgL_tmpz00_1154 = 
BGL_MVALUES_VAL(BgL_tmpz00_2283); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2286;
BgL_tmpz00_2286 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2286, BUNSPEC); } 
BgL_arg1152z00_767 = BgL_tmpz00_1154; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1155;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2289;
BgL_tmpz00_2289 = 
(int)(3L); 
BgL_tmpz00_1155 = 
BGL_MVALUES_VAL(BgL_tmpz00_2289); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2292;
BgL_tmpz00_2292 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2292, BUNSPEC); } 
BgL_arg1153z00_768 = BgL_tmpz00_1155; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1156;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2295;
BgL_tmpz00_2295 = 
(int)(4L); 
BgL_tmpz00_1156 = 
BGL_MVALUES_VAL(BgL_tmpz00_2295); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2298;
BgL_tmpz00_2298 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2298, BUNSPEC); } 
BgL_arg1154z00_769 = BgL_tmpz00_1156; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1157;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2301;
BgL_tmpz00_2301 = 
(int)(5L); 
BgL_tmpz00_1157 = 
BGL_MVALUES_VAL(BgL_tmpz00_2301); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2304;
BgL_tmpz00_2304 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2304, BUNSPEC); } 
BgL_arg1157z00_770 = BgL_tmpz00_1157; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1158;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2307;
BgL_tmpz00_2307 = 
(int)(6L); 
BgL_tmpz00_1158 = 
BGL_MVALUES_VAL(BgL_tmpz00_2307); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2310;
BgL_tmpz00_2310 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2310, BUNSPEC); } 
BgL_arg1158z00_771 = BgL_tmpz00_1158; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1159;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2313;
BgL_tmpz00_2313 = 
(int)(7L); 
BgL_tmpz00_1159 = 
BGL_MVALUES_VAL(BgL_tmpz00_2313); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2316;
BgL_tmpz00_2316 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2316, BUNSPEC); } 
BgL_arg1162z00_772 = BgL_tmpz00_1159; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1160;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2319;
BgL_tmpz00_2319 = 
(int)(8L); 
BgL_tmpz00_1160 = 
BGL_MVALUES_VAL(BgL_tmpz00_2319); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2322;
BgL_tmpz00_2322 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2322, BUNSPEC); } 
BgL_arg1164z00_773 = BgL_tmpz00_1160; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1161;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2325;
BgL_tmpz00_2325 = 
(int)(9L); 
BgL_tmpz00_1161 = 
BGL_MVALUES_VAL(BgL_tmpz00_2325); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2328;
BgL_tmpz00_2328 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2328, BUNSPEC); } 
BgL_arg1166z00_774 = BgL_tmpz00_1161; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 10))
{ /* Ieee/control5.scm 193 */
return 
BGL_PROCEDURE_CALL10(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1149z00_766, BgL_arg1152z00_767, BgL_arg1153z00_768, BgL_arg1154z00_769, BgL_arg1157z00_770, BgL_arg1158z00_771, BgL_arg1162z00_772, BgL_arg1164z00_773, BgL_arg1166z00_774);}  else 
{ /* Ieee/control5.scm 193 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1726z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 11L : 

{ /* Ieee/control5.scm 205 */
 obj_t BgL_arg1171z00_775; obj_t BgL_arg1172z00_776; obj_t BgL_arg1182z00_777; obj_t BgL_arg1183z00_778; obj_t BgL_arg1187z00_779; obj_t BgL_arg1188z00_780; obj_t BgL_arg1189z00_781; obj_t BgL_arg1190z00_782; obj_t BgL_arg1191z00_783; obj_t BgL_arg1193z00_784;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1162;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2347;
BgL_tmpz00_2347 = 
(int)(1L); 
BgL_tmpz00_1162 = 
BGL_MVALUES_VAL(BgL_tmpz00_2347); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2350;
BgL_tmpz00_2350 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2350, BUNSPEC); } 
BgL_arg1171z00_775 = BgL_tmpz00_1162; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1163;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2353;
BgL_tmpz00_2353 = 
(int)(2L); 
BgL_tmpz00_1163 = 
BGL_MVALUES_VAL(BgL_tmpz00_2353); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2356;
BgL_tmpz00_2356 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2356, BUNSPEC); } 
BgL_arg1172z00_776 = BgL_tmpz00_1163; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1164;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2359;
BgL_tmpz00_2359 = 
(int)(3L); 
BgL_tmpz00_1164 = 
BGL_MVALUES_VAL(BgL_tmpz00_2359); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2362;
BgL_tmpz00_2362 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2362, BUNSPEC); } 
BgL_arg1182z00_777 = BgL_tmpz00_1164; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1165;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2365;
BgL_tmpz00_2365 = 
(int)(4L); 
BgL_tmpz00_1165 = 
BGL_MVALUES_VAL(BgL_tmpz00_2365); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2368;
BgL_tmpz00_2368 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2368, BUNSPEC); } 
BgL_arg1183z00_778 = BgL_tmpz00_1165; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1166;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2371;
BgL_tmpz00_2371 = 
(int)(5L); 
BgL_tmpz00_1166 = 
BGL_MVALUES_VAL(BgL_tmpz00_2371); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2374;
BgL_tmpz00_2374 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2374, BUNSPEC); } 
BgL_arg1187z00_779 = BgL_tmpz00_1166; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1167;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2377;
BgL_tmpz00_2377 = 
(int)(6L); 
BgL_tmpz00_1167 = 
BGL_MVALUES_VAL(BgL_tmpz00_2377); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2380;
BgL_tmpz00_2380 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2380, BUNSPEC); } 
BgL_arg1188z00_780 = BgL_tmpz00_1167; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1168;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2383;
BgL_tmpz00_2383 = 
(int)(7L); 
BgL_tmpz00_1168 = 
BGL_MVALUES_VAL(BgL_tmpz00_2383); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2386;
BgL_tmpz00_2386 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2386, BUNSPEC); } 
BgL_arg1189z00_781 = BgL_tmpz00_1168; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1169;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2389;
BgL_tmpz00_2389 = 
(int)(8L); 
BgL_tmpz00_1169 = 
BGL_MVALUES_VAL(BgL_tmpz00_2389); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2392;
BgL_tmpz00_2392 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2392, BUNSPEC); } 
BgL_arg1190z00_782 = BgL_tmpz00_1169; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1170;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2395;
BgL_tmpz00_2395 = 
(int)(9L); 
BgL_tmpz00_1170 = 
BGL_MVALUES_VAL(BgL_tmpz00_2395); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2398;
BgL_tmpz00_2398 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2398, BUNSPEC); } 
BgL_arg1191z00_783 = BgL_tmpz00_1170; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1171;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2401;
BgL_tmpz00_2401 = 
(int)(10L); 
BgL_tmpz00_1171 = 
BGL_MVALUES_VAL(BgL_tmpz00_2401); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2404;
BgL_tmpz00_2404 = 
(int)(10L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2404, BUNSPEC); } 
BgL_arg1193z00_784 = BgL_tmpz00_1171; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 11))
{ /* Ieee/control5.scm 204 */
return 
BGL_PROCEDURE_CALL11(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1171z00_775, BgL_arg1172z00_776, BgL_arg1182z00_777, BgL_arg1183z00_778, BgL_arg1187z00_779, BgL_arg1188z00_780, BgL_arg1189z00_781, BgL_arg1190z00_782, BgL_arg1191z00_783, BgL_arg1193z00_784);}  else 
{ /* Ieee/control5.scm 204 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1745z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 12L : 

{ /* Ieee/control5.scm 217 */
 obj_t BgL_arg1194z00_785; obj_t BgL_arg1196z00_786; obj_t BgL_arg1197z00_787; obj_t BgL_arg1198z00_788; obj_t BgL_arg1199z00_789; obj_t BgL_arg1200z00_790; obj_t BgL_arg1201z00_791; obj_t BgL_arg1202z00_792; obj_t BgL_arg1203z00_793; obj_t BgL_arg1206z00_794; obj_t BgL_arg1208z00_795;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1172;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2424;
BgL_tmpz00_2424 = 
(int)(1L); 
BgL_tmpz00_1172 = 
BGL_MVALUES_VAL(BgL_tmpz00_2424); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2427;
BgL_tmpz00_2427 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2427, BUNSPEC); } 
BgL_arg1194z00_785 = BgL_tmpz00_1172; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1173;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2430;
BgL_tmpz00_2430 = 
(int)(2L); 
BgL_tmpz00_1173 = 
BGL_MVALUES_VAL(BgL_tmpz00_2430); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2433;
BgL_tmpz00_2433 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2433, BUNSPEC); } 
BgL_arg1196z00_786 = BgL_tmpz00_1173; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1174;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2436;
BgL_tmpz00_2436 = 
(int)(3L); 
BgL_tmpz00_1174 = 
BGL_MVALUES_VAL(BgL_tmpz00_2436); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2439;
BgL_tmpz00_2439 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2439, BUNSPEC); } 
BgL_arg1197z00_787 = BgL_tmpz00_1174; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1175;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2442;
BgL_tmpz00_2442 = 
(int)(4L); 
BgL_tmpz00_1175 = 
BGL_MVALUES_VAL(BgL_tmpz00_2442); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2445;
BgL_tmpz00_2445 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2445, BUNSPEC); } 
BgL_arg1198z00_788 = BgL_tmpz00_1175; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1176;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2448;
BgL_tmpz00_2448 = 
(int)(5L); 
BgL_tmpz00_1176 = 
BGL_MVALUES_VAL(BgL_tmpz00_2448); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2451;
BgL_tmpz00_2451 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2451, BUNSPEC); } 
BgL_arg1199z00_789 = BgL_tmpz00_1176; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1177;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2454;
BgL_tmpz00_2454 = 
(int)(6L); 
BgL_tmpz00_1177 = 
BGL_MVALUES_VAL(BgL_tmpz00_2454); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2457;
BgL_tmpz00_2457 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2457, BUNSPEC); } 
BgL_arg1200z00_790 = BgL_tmpz00_1177; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1178;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2460;
BgL_tmpz00_2460 = 
(int)(7L); 
BgL_tmpz00_1178 = 
BGL_MVALUES_VAL(BgL_tmpz00_2460); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2463;
BgL_tmpz00_2463 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2463, BUNSPEC); } 
BgL_arg1201z00_791 = BgL_tmpz00_1178; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1179;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2466;
BgL_tmpz00_2466 = 
(int)(8L); 
BgL_tmpz00_1179 = 
BGL_MVALUES_VAL(BgL_tmpz00_2466); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2469;
BgL_tmpz00_2469 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2469, BUNSPEC); } 
BgL_arg1202z00_792 = BgL_tmpz00_1179; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1180;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2472;
BgL_tmpz00_2472 = 
(int)(9L); 
BgL_tmpz00_1180 = 
BGL_MVALUES_VAL(BgL_tmpz00_2472); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2475;
BgL_tmpz00_2475 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2475, BUNSPEC); } 
BgL_arg1203z00_793 = BgL_tmpz00_1180; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1181;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2478;
BgL_tmpz00_2478 = 
(int)(10L); 
BgL_tmpz00_1181 = 
BGL_MVALUES_VAL(BgL_tmpz00_2478); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2481;
BgL_tmpz00_2481 = 
(int)(10L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2481, BUNSPEC); } 
BgL_arg1206z00_794 = BgL_tmpz00_1181; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1182;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2484;
BgL_tmpz00_2484 = 
(int)(11L); 
BgL_tmpz00_1182 = 
BGL_MVALUES_VAL(BgL_tmpz00_2484); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2487;
BgL_tmpz00_2487 = 
(int)(11L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2487, BUNSPEC); } 
BgL_arg1208z00_795 = BgL_tmpz00_1182; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 12))
{ /* Ieee/control5.scm 216 */
return 
BGL_PROCEDURE_CALL12(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1194z00_785, BgL_arg1196z00_786, BgL_arg1197z00_787, BgL_arg1198z00_788, BgL_arg1199z00_789, BgL_arg1200z00_790, BgL_arg1201z00_791, BgL_arg1202z00_792, BgL_arg1203z00_793, BgL_arg1206z00_794, BgL_arg1208z00_795);}  else 
{ /* Ieee/control5.scm 216 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1766z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 13L : 

{ /* Ieee/control5.scm 230 */
 obj_t BgL_arg1209z00_796; obj_t BgL_arg1210z00_797; obj_t BgL_arg1212z00_798; obj_t BgL_arg1215z00_799; obj_t BgL_arg1216z00_800; obj_t BgL_arg1218z00_801; obj_t BgL_arg1219z00_802; obj_t BgL_arg1220z00_803; obj_t BgL_arg1221z00_804; obj_t BgL_arg1223z00_805; obj_t BgL_arg1225z00_806; obj_t BgL_arg1226z00_807;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1183;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2508;
BgL_tmpz00_2508 = 
(int)(1L); 
BgL_tmpz00_1183 = 
BGL_MVALUES_VAL(BgL_tmpz00_2508); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2511;
BgL_tmpz00_2511 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2511, BUNSPEC); } 
BgL_arg1209z00_796 = BgL_tmpz00_1183; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1184;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2514;
BgL_tmpz00_2514 = 
(int)(2L); 
BgL_tmpz00_1184 = 
BGL_MVALUES_VAL(BgL_tmpz00_2514); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2517;
BgL_tmpz00_2517 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2517, BUNSPEC); } 
BgL_arg1210z00_797 = BgL_tmpz00_1184; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1185;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2520;
BgL_tmpz00_2520 = 
(int)(3L); 
BgL_tmpz00_1185 = 
BGL_MVALUES_VAL(BgL_tmpz00_2520); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2523;
BgL_tmpz00_2523 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2523, BUNSPEC); } 
BgL_arg1212z00_798 = BgL_tmpz00_1185; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1186;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2526;
BgL_tmpz00_2526 = 
(int)(4L); 
BgL_tmpz00_1186 = 
BGL_MVALUES_VAL(BgL_tmpz00_2526); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2529;
BgL_tmpz00_2529 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2529, BUNSPEC); } 
BgL_arg1215z00_799 = BgL_tmpz00_1186; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1187;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2532;
BgL_tmpz00_2532 = 
(int)(5L); 
BgL_tmpz00_1187 = 
BGL_MVALUES_VAL(BgL_tmpz00_2532); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2535;
BgL_tmpz00_2535 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2535, BUNSPEC); } 
BgL_arg1216z00_800 = BgL_tmpz00_1187; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1188;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2538;
BgL_tmpz00_2538 = 
(int)(6L); 
BgL_tmpz00_1188 = 
BGL_MVALUES_VAL(BgL_tmpz00_2538); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2541;
BgL_tmpz00_2541 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2541, BUNSPEC); } 
BgL_arg1218z00_801 = BgL_tmpz00_1188; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1189;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2544;
BgL_tmpz00_2544 = 
(int)(7L); 
BgL_tmpz00_1189 = 
BGL_MVALUES_VAL(BgL_tmpz00_2544); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2547;
BgL_tmpz00_2547 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2547, BUNSPEC); } 
BgL_arg1219z00_802 = BgL_tmpz00_1189; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1190;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2550;
BgL_tmpz00_2550 = 
(int)(8L); 
BgL_tmpz00_1190 = 
BGL_MVALUES_VAL(BgL_tmpz00_2550); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2553;
BgL_tmpz00_2553 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2553, BUNSPEC); } 
BgL_arg1220z00_803 = BgL_tmpz00_1190; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1191;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2556;
BgL_tmpz00_2556 = 
(int)(9L); 
BgL_tmpz00_1191 = 
BGL_MVALUES_VAL(BgL_tmpz00_2556); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2559;
BgL_tmpz00_2559 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2559, BUNSPEC); } 
BgL_arg1221z00_804 = BgL_tmpz00_1191; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1192;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2562;
BgL_tmpz00_2562 = 
(int)(10L); 
BgL_tmpz00_1192 = 
BGL_MVALUES_VAL(BgL_tmpz00_2562); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2565;
BgL_tmpz00_2565 = 
(int)(10L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2565, BUNSPEC); } 
BgL_arg1223z00_805 = BgL_tmpz00_1192; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1193;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2568;
BgL_tmpz00_2568 = 
(int)(11L); 
BgL_tmpz00_1193 = 
BGL_MVALUES_VAL(BgL_tmpz00_2568); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2571;
BgL_tmpz00_2571 = 
(int)(11L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2571, BUNSPEC); } 
BgL_arg1225z00_806 = BgL_tmpz00_1193; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1194;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2574;
BgL_tmpz00_2574 = 
(int)(12L); 
BgL_tmpz00_1194 = 
BGL_MVALUES_VAL(BgL_tmpz00_2574); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2577;
BgL_tmpz00_2577 = 
(int)(12L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2577, BUNSPEC); } 
BgL_arg1226z00_807 = BgL_tmpz00_1194; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 13))
{ /* Ieee/control5.scm 229 */
return 
BGL_PROCEDURE_CALL13(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1209z00_796, BgL_arg1210z00_797, BgL_arg1212z00_798, BgL_arg1215z00_799, BgL_arg1216z00_800, BgL_arg1218z00_801, BgL_arg1219z00_802, BgL_arg1220z00_803, BgL_arg1221z00_804, BgL_arg1223z00_805, BgL_arg1225z00_806, BgL_arg1226z00_807);}  else 
{ /* Ieee/control5.scm 229 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1789z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 14L : 

{ /* Ieee/control5.scm 244 */
 obj_t BgL_arg1227z00_808; obj_t BgL_arg1228z00_809; obj_t BgL_arg1229z00_810; obj_t BgL_arg1230z00_811; obj_t BgL_arg1231z00_812; obj_t BgL_arg1232z00_813; obj_t BgL_arg1233z00_814; obj_t BgL_arg1234z00_815; obj_t BgL_arg1236z00_816; obj_t BgL_arg1238z00_817; obj_t BgL_arg1239z00_818; obj_t BgL_arg1242z00_819; obj_t BgL_arg1244z00_820;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1195;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2599;
BgL_tmpz00_2599 = 
(int)(1L); 
BgL_tmpz00_1195 = 
BGL_MVALUES_VAL(BgL_tmpz00_2599); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2602;
BgL_tmpz00_2602 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2602, BUNSPEC); } 
BgL_arg1227z00_808 = BgL_tmpz00_1195; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1196;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2605;
BgL_tmpz00_2605 = 
(int)(2L); 
BgL_tmpz00_1196 = 
BGL_MVALUES_VAL(BgL_tmpz00_2605); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2608;
BgL_tmpz00_2608 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2608, BUNSPEC); } 
BgL_arg1228z00_809 = BgL_tmpz00_1196; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1197;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2611;
BgL_tmpz00_2611 = 
(int)(3L); 
BgL_tmpz00_1197 = 
BGL_MVALUES_VAL(BgL_tmpz00_2611); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2614;
BgL_tmpz00_2614 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2614, BUNSPEC); } 
BgL_arg1229z00_810 = BgL_tmpz00_1197; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1198;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2617;
BgL_tmpz00_2617 = 
(int)(4L); 
BgL_tmpz00_1198 = 
BGL_MVALUES_VAL(BgL_tmpz00_2617); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2620;
BgL_tmpz00_2620 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2620, BUNSPEC); } 
BgL_arg1230z00_811 = BgL_tmpz00_1198; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1199;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2623;
BgL_tmpz00_2623 = 
(int)(5L); 
BgL_tmpz00_1199 = 
BGL_MVALUES_VAL(BgL_tmpz00_2623); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2626;
BgL_tmpz00_2626 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2626, BUNSPEC); } 
BgL_arg1231z00_812 = BgL_tmpz00_1199; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1200;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2629;
BgL_tmpz00_2629 = 
(int)(6L); 
BgL_tmpz00_1200 = 
BGL_MVALUES_VAL(BgL_tmpz00_2629); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2632;
BgL_tmpz00_2632 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2632, BUNSPEC); } 
BgL_arg1232z00_813 = BgL_tmpz00_1200; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1201;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2635;
BgL_tmpz00_2635 = 
(int)(7L); 
BgL_tmpz00_1201 = 
BGL_MVALUES_VAL(BgL_tmpz00_2635); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2638;
BgL_tmpz00_2638 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2638, BUNSPEC); } 
BgL_arg1233z00_814 = BgL_tmpz00_1201; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1202;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2641;
BgL_tmpz00_2641 = 
(int)(8L); 
BgL_tmpz00_1202 = 
BGL_MVALUES_VAL(BgL_tmpz00_2641); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2644;
BgL_tmpz00_2644 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2644, BUNSPEC); } 
BgL_arg1234z00_815 = BgL_tmpz00_1202; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1203;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2647;
BgL_tmpz00_2647 = 
(int)(9L); 
BgL_tmpz00_1203 = 
BGL_MVALUES_VAL(BgL_tmpz00_2647); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2650;
BgL_tmpz00_2650 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2650, BUNSPEC); } 
BgL_arg1236z00_816 = BgL_tmpz00_1203; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1204;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2653;
BgL_tmpz00_2653 = 
(int)(10L); 
BgL_tmpz00_1204 = 
BGL_MVALUES_VAL(BgL_tmpz00_2653); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2656;
BgL_tmpz00_2656 = 
(int)(10L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2656, BUNSPEC); } 
BgL_arg1238z00_817 = BgL_tmpz00_1204; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1205;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2659;
BgL_tmpz00_2659 = 
(int)(11L); 
BgL_tmpz00_1205 = 
BGL_MVALUES_VAL(BgL_tmpz00_2659); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2662;
BgL_tmpz00_2662 = 
(int)(11L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2662, BUNSPEC); } 
BgL_arg1239z00_818 = BgL_tmpz00_1205; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1206;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2665;
BgL_tmpz00_2665 = 
(int)(12L); 
BgL_tmpz00_1206 = 
BGL_MVALUES_VAL(BgL_tmpz00_2665); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2668;
BgL_tmpz00_2668 = 
(int)(12L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2668, BUNSPEC); } 
BgL_arg1242z00_819 = BgL_tmpz00_1206; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1207;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2671;
BgL_tmpz00_2671 = 
(int)(13L); 
BgL_tmpz00_1207 = 
BGL_MVALUES_VAL(BgL_tmpz00_2671); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2674;
BgL_tmpz00_2674 = 
(int)(13L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2674, BUNSPEC); } 
BgL_arg1244z00_820 = BgL_tmpz00_1207; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 14))
{ /* Ieee/control5.scm 243 */
return 
BGL_PROCEDURE_CALL14(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1227z00_808, BgL_arg1228z00_809, BgL_arg1229z00_810, BgL_arg1230z00_811, BgL_arg1231z00_812, BgL_arg1232z00_813, BgL_arg1233z00_814, BgL_arg1234z00_815, BgL_arg1236z00_816, BgL_arg1238z00_817, BgL_arg1239z00_818, BgL_arg1242z00_819, BgL_arg1244z00_820);}  else 
{ /* Ieee/control5.scm 243 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1814z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 15L : 

{ /* Ieee/control5.scm 259 */
 obj_t BgL_arg1248z00_821; obj_t BgL_arg1249z00_822; obj_t BgL_arg1252z00_823; obj_t BgL_arg1268z00_824; obj_t BgL_arg1272z00_825; obj_t BgL_arg1284z00_826; obj_t BgL_arg1304z00_827; obj_t BgL_arg1305z00_828; obj_t BgL_arg1306z00_829; obj_t BgL_arg1307z00_830; obj_t BgL_arg1308z00_831; obj_t BgL_arg1309z00_832; obj_t BgL_arg1310z00_833; obj_t BgL_arg1311z00_834;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1208;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2697;
BgL_tmpz00_2697 = 
(int)(1L); 
BgL_tmpz00_1208 = 
BGL_MVALUES_VAL(BgL_tmpz00_2697); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2700;
BgL_tmpz00_2700 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2700, BUNSPEC); } 
BgL_arg1248z00_821 = BgL_tmpz00_1208; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1209;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2703;
BgL_tmpz00_2703 = 
(int)(2L); 
BgL_tmpz00_1209 = 
BGL_MVALUES_VAL(BgL_tmpz00_2703); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2706;
BgL_tmpz00_2706 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2706, BUNSPEC); } 
BgL_arg1249z00_822 = BgL_tmpz00_1209; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1210;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2709;
BgL_tmpz00_2709 = 
(int)(3L); 
BgL_tmpz00_1210 = 
BGL_MVALUES_VAL(BgL_tmpz00_2709); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2712;
BgL_tmpz00_2712 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2712, BUNSPEC); } 
BgL_arg1252z00_823 = BgL_tmpz00_1210; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1211;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2715;
BgL_tmpz00_2715 = 
(int)(4L); 
BgL_tmpz00_1211 = 
BGL_MVALUES_VAL(BgL_tmpz00_2715); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2718;
BgL_tmpz00_2718 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2718, BUNSPEC); } 
BgL_arg1268z00_824 = BgL_tmpz00_1211; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1212;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2721;
BgL_tmpz00_2721 = 
(int)(5L); 
BgL_tmpz00_1212 = 
BGL_MVALUES_VAL(BgL_tmpz00_2721); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2724;
BgL_tmpz00_2724 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2724, BUNSPEC); } 
BgL_arg1272z00_825 = BgL_tmpz00_1212; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1213;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2727;
BgL_tmpz00_2727 = 
(int)(6L); 
BgL_tmpz00_1213 = 
BGL_MVALUES_VAL(BgL_tmpz00_2727); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2730;
BgL_tmpz00_2730 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2730, BUNSPEC); } 
BgL_arg1284z00_826 = BgL_tmpz00_1213; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1214;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2733;
BgL_tmpz00_2733 = 
(int)(7L); 
BgL_tmpz00_1214 = 
BGL_MVALUES_VAL(BgL_tmpz00_2733); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2736;
BgL_tmpz00_2736 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2736, BUNSPEC); } 
BgL_arg1304z00_827 = BgL_tmpz00_1214; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1215;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2739;
BgL_tmpz00_2739 = 
(int)(8L); 
BgL_tmpz00_1215 = 
BGL_MVALUES_VAL(BgL_tmpz00_2739); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2742;
BgL_tmpz00_2742 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2742, BUNSPEC); } 
BgL_arg1305z00_828 = BgL_tmpz00_1215; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1216;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2745;
BgL_tmpz00_2745 = 
(int)(9L); 
BgL_tmpz00_1216 = 
BGL_MVALUES_VAL(BgL_tmpz00_2745); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2748;
BgL_tmpz00_2748 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2748, BUNSPEC); } 
BgL_arg1306z00_829 = BgL_tmpz00_1216; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1217;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2751;
BgL_tmpz00_2751 = 
(int)(10L); 
BgL_tmpz00_1217 = 
BGL_MVALUES_VAL(BgL_tmpz00_2751); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2754;
BgL_tmpz00_2754 = 
(int)(10L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2754, BUNSPEC); } 
BgL_arg1307z00_830 = BgL_tmpz00_1217; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1218;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2757;
BgL_tmpz00_2757 = 
(int)(11L); 
BgL_tmpz00_1218 = 
BGL_MVALUES_VAL(BgL_tmpz00_2757); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2760;
BgL_tmpz00_2760 = 
(int)(11L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2760, BUNSPEC); } 
BgL_arg1308z00_831 = BgL_tmpz00_1218; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1219;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2763;
BgL_tmpz00_2763 = 
(int)(12L); 
BgL_tmpz00_1219 = 
BGL_MVALUES_VAL(BgL_tmpz00_2763); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2766;
BgL_tmpz00_2766 = 
(int)(12L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2766, BUNSPEC); } 
BgL_arg1309z00_832 = BgL_tmpz00_1219; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1220;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2769;
BgL_tmpz00_2769 = 
(int)(13L); 
BgL_tmpz00_1220 = 
BGL_MVALUES_VAL(BgL_tmpz00_2769); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2772;
BgL_tmpz00_2772 = 
(int)(13L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2772, BUNSPEC); } 
BgL_arg1310z00_833 = BgL_tmpz00_1220; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1221;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2775;
BgL_tmpz00_2775 = 
(int)(14L); 
BgL_tmpz00_1221 = 
BGL_MVALUES_VAL(BgL_tmpz00_2775); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2778;
BgL_tmpz00_2778 = 
(int)(14L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2778, BUNSPEC); } 
BgL_arg1311z00_834 = BgL_tmpz00_1221; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 15))
{ /* Ieee/control5.scm 258 */
return 
BGL_PROCEDURE_CALL15(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1248z00_821, BgL_arg1249z00_822, BgL_arg1252z00_823, BgL_arg1268z00_824, BgL_arg1272z00_825, BgL_arg1284z00_826, BgL_arg1304z00_827, BgL_arg1305z00_828, BgL_arg1306z00_829, BgL_arg1307z00_830, BgL_arg1308z00_831, BgL_arg1309z00_832, BgL_arg1310z00_833, BgL_arg1311z00_834);}  else 
{ /* Ieee/control5.scm 258 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1841z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;case 16L : 

{ /* Ieee/control5.scm 275 */
 obj_t BgL_arg1312z00_835; obj_t BgL_arg1314z00_836; obj_t BgL_arg1315z00_837; obj_t BgL_arg1316z00_838; obj_t BgL_arg1317z00_839; obj_t BgL_arg1318z00_840; obj_t BgL_arg1319z00_841; obj_t BgL_arg1320z00_842; obj_t BgL_arg1321z00_843; obj_t BgL_arg1322z00_844; obj_t BgL_arg1323z00_845; obj_t BgL_arg1325z00_846; obj_t BgL_arg1326z00_847; obj_t BgL_arg1327z00_848; obj_t BgL_arg1328z00_849;
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1222;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2802;
BgL_tmpz00_2802 = 
(int)(1L); 
BgL_tmpz00_1222 = 
BGL_MVALUES_VAL(BgL_tmpz00_2802); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2805;
BgL_tmpz00_2805 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2805, BUNSPEC); } 
BgL_arg1312z00_835 = BgL_tmpz00_1222; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1223;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2808;
BgL_tmpz00_2808 = 
(int)(2L); 
BgL_tmpz00_1223 = 
BGL_MVALUES_VAL(BgL_tmpz00_2808); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2811;
BgL_tmpz00_2811 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2811, BUNSPEC); } 
BgL_arg1314z00_836 = BgL_tmpz00_1223; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1224;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2814;
BgL_tmpz00_2814 = 
(int)(3L); 
BgL_tmpz00_1224 = 
BGL_MVALUES_VAL(BgL_tmpz00_2814); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2817;
BgL_tmpz00_2817 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2817, BUNSPEC); } 
BgL_arg1315z00_837 = BgL_tmpz00_1224; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1225;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2820;
BgL_tmpz00_2820 = 
(int)(4L); 
BgL_tmpz00_1225 = 
BGL_MVALUES_VAL(BgL_tmpz00_2820); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2823;
BgL_tmpz00_2823 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2823, BUNSPEC); } 
BgL_arg1316z00_838 = BgL_tmpz00_1225; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1226;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2826;
BgL_tmpz00_2826 = 
(int)(5L); 
BgL_tmpz00_1226 = 
BGL_MVALUES_VAL(BgL_tmpz00_2826); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2829;
BgL_tmpz00_2829 = 
(int)(5L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2829, BUNSPEC); } 
BgL_arg1317z00_839 = BgL_tmpz00_1226; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1227;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2832;
BgL_tmpz00_2832 = 
(int)(6L); 
BgL_tmpz00_1227 = 
BGL_MVALUES_VAL(BgL_tmpz00_2832); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2835;
BgL_tmpz00_2835 = 
(int)(6L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2835, BUNSPEC); } 
BgL_arg1318z00_840 = BgL_tmpz00_1227; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1228;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2838;
BgL_tmpz00_2838 = 
(int)(7L); 
BgL_tmpz00_1228 = 
BGL_MVALUES_VAL(BgL_tmpz00_2838); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2841;
BgL_tmpz00_2841 = 
(int)(7L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2841, BUNSPEC); } 
BgL_arg1319z00_841 = BgL_tmpz00_1228; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1229;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2844;
BgL_tmpz00_2844 = 
(int)(8L); 
BgL_tmpz00_1229 = 
BGL_MVALUES_VAL(BgL_tmpz00_2844); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2847;
BgL_tmpz00_2847 = 
(int)(8L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2847, BUNSPEC); } 
BgL_arg1320z00_842 = BgL_tmpz00_1229; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1230;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2850;
BgL_tmpz00_2850 = 
(int)(9L); 
BgL_tmpz00_1230 = 
BGL_MVALUES_VAL(BgL_tmpz00_2850); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2853;
BgL_tmpz00_2853 = 
(int)(9L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2853, BUNSPEC); } 
BgL_arg1321z00_843 = BgL_tmpz00_1230; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1231;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2856;
BgL_tmpz00_2856 = 
(int)(10L); 
BgL_tmpz00_1231 = 
BGL_MVALUES_VAL(BgL_tmpz00_2856); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2859;
BgL_tmpz00_2859 = 
(int)(10L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2859, BUNSPEC); } 
BgL_arg1322z00_844 = BgL_tmpz00_1231; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1232;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2862;
BgL_tmpz00_2862 = 
(int)(11L); 
BgL_tmpz00_1232 = 
BGL_MVALUES_VAL(BgL_tmpz00_2862); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2865;
BgL_tmpz00_2865 = 
(int)(11L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2865, BUNSPEC); } 
BgL_arg1323z00_845 = BgL_tmpz00_1232; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1233;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2868;
BgL_tmpz00_2868 = 
(int)(12L); 
BgL_tmpz00_1233 = 
BGL_MVALUES_VAL(BgL_tmpz00_2868); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2871;
BgL_tmpz00_2871 = 
(int)(12L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2871, BUNSPEC); } 
BgL_arg1325z00_846 = BgL_tmpz00_1233; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1234;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2874;
BgL_tmpz00_2874 = 
(int)(13L); 
BgL_tmpz00_1234 = 
BGL_MVALUES_VAL(BgL_tmpz00_2874); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2877;
BgL_tmpz00_2877 = 
(int)(13L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2877, BUNSPEC); } 
BgL_arg1326z00_847 = BgL_tmpz00_1234; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1235;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2880;
BgL_tmpz00_2880 = 
(int)(14L); 
BgL_tmpz00_1235 = 
BGL_MVALUES_VAL(BgL_tmpz00_2880); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2883;
BgL_tmpz00_2883 = 
(int)(14L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2883, BUNSPEC); } 
BgL_arg1327z00_848 = BgL_tmpz00_1235; } 
{ /* Ieee/control5.scm 79 */
 obj_t BgL_tmpz00_1236;
{ /* Ieee/control5.scm 79 */
 int BgL_tmpz00_2886;
BgL_tmpz00_2886 = 
(int)(15L); 
BgL_tmpz00_1236 = 
BGL_MVALUES_VAL(BgL_tmpz00_2886); } 
{ /* Ieee/control5.scm 87 */
 int BgL_tmpz00_2889;
BgL_tmpz00_2889 = 
(int)(15L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_2889, BUNSPEC); } 
BgL_arg1328z00_849 = BgL_tmpz00_1236; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, 16))
{ /* Ieee/control5.scm 274 */
return 
BGL_PROCEDURE_CALL16(BgL_consumerz00_9, BgL_res0z00_726, BgL_arg1312z00_835, BgL_arg1314z00_836, BgL_arg1315z00_837, BgL_arg1316z00_838, BgL_arg1317z00_839, BgL_arg1318z00_840, BgL_arg1319z00_841, BgL_arg1320z00_842, BgL_arg1321z00_843, BgL_arg1322z00_844, BgL_arg1323z00_845, BgL_arg1325z00_846, BgL_arg1326z00_847, BgL_arg1327z00_848, BgL_arg1328z00_849);}  else 
{ /* Ieee/control5.scm 274 */
FAILURE(BGl_string1628z00zz__r5_control_features_6_4z00,BGl_list1870z00zz__r5_control_features_6_4z00,BgL_consumerz00_9);} } break;
default: 
{ /* Ieee/control5.scm 291 */
 int BgL_len1615z00_1363;
BgL_len1615z00_1363 = 
(int)(
bgl_list_length(
((obj_t)BgL_res0z00_726))); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_consumerz00_9, BgL_len1615z00_1363))
{ /* Ieee/control5.scm 291 */
return 
apply(BgL_consumerz00_9, 
((obj_t)BgL_res0z00_726));}  else 
{ /* Ieee/control5.scm 291 */
FAILURE(BGl_symbol1634z00zz__r5_control_features_6_4z00,BGl_string1636z00zz__r5_control_features_6_4z00,BGl_list1637z00zz__r5_control_features_6_4z00);} } } } } } } 

}



/* &call-with-values */
obj_t BGl_z62callzd2withzd2valuesz62zz__r5_control_features_6_4z00(obj_t BgL_envz00_1304, obj_t BgL_producerz00_1305, obj_t BgL_consumerz00_1306)
{
{ /* Ieee/control5.scm 119 */
{ /* Ieee/control5.scm 120 */
 obj_t BgL_auxz00_2932; obj_t BgL_auxz00_2925;
if(
PROCEDUREP(BgL_consumerz00_1306))
{ /* Ieee/control5.scm 120 */
BgL_auxz00_2932 = BgL_consumerz00_1306
; }  else 
{ 
 obj_t BgL_auxz00_2935;
BgL_auxz00_2935 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(4604L), BGl_string1901z00zz__r5_control_features_6_4z00, BGl_string1902z00zz__r5_control_features_6_4z00, BgL_consumerz00_1306); 
FAILURE(BgL_auxz00_2935,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_producerz00_1305))
{ /* Ieee/control5.scm 120 */
BgL_auxz00_2925 = BgL_producerz00_1305
; }  else 
{ 
 obj_t BgL_auxz00_2928;
BgL_auxz00_2928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1620z00zz__r5_control_features_6_4z00, 
BINT(4604L), BGl_string1901z00zz__r5_control_features_6_4z00, BGl_string1902z00zz__r5_control_features_6_4z00, BgL_producerz00_1305); 
FAILURE(BgL_auxz00_2928,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2valuesz00zz__r5_control_features_6_4z00(BgL_auxz00_2925, BgL_auxz00_2932);} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r5_control_features_6_4z00(void)
{
{ /* Ieee/control5.scm 14 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1903z00zz__r5_control_features_6_4z00));} 

}

#ifdef __cplusplus
}
#endif
