/*===========================================================================*/
/*   (Ieee/input.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/input.scm -indent -o objs/obj_s/Ieee/input.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_INPUT_6_10_2_TYPE_DEFINITIONS
#define BGL___R4_INPUT_6_10_2_TYPE_DEFINITIONS

/* object type definitions */
typedef struct BgL_z62exceptionz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
} *BgL_z62exceptionz62_bglt;

typedef struct BgL_z62errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62errorz62_bglt;

typedef struct BgL_z62iozd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2errorzb0_bglt;

typedef struct BgL_z62iozd2closedzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2closedzd2errorz62_bglt;


#endif // BGL___R4_INPUT_6_10_2_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern obj_t BGl_z62iozd2closedzd2errorz62zz__objectz00;
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
static obj_t BGl_symbol4341z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4260z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4263z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4232z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4347z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4266z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4235z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4269z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(obj_t, obj_t, long, long);
static obj_t BGl_z62sendzd2charszf2siza7eze5zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t, obj_t, obj_t);
static int BGl_z52sendcharsz52zz__r4_input_6_10_2z00(obj_t, obj_t, long, long);
static obj_t BGl_z62eofzd2objectzf3z43zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_symbol4353z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4240z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4249z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl__passwordz00zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_passwordz00zz__r4_input_6_10_2z00(obj_t);
extern obj_t BGl_raisez00zz__errorz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza32657ze3ze5zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_symbol4280z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t);
static obj_t BGl_symbol4287z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4289z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t bgl_display_obj(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__gunza7ipza7(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
extern obj_t bgl_file_to_string(char *);
static obj_t BGl_symbol4294z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4296z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern bool_t fexists(char *);
BGL_EXPORTED_DECL obj_t BGl_eofzd2objectzd2zz__r4_input_6_10_2z00(void);
static obj_t BGl__unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__r4_input_6_10_2z00(void);
static obj_t BGl__readzd2linezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
extern bool_t rgc_buffer_insert_substring(obj_t, obj_t, long, long);
static obj_t BGl_z62filezd2positionzd2ze3linez81zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t);
extern obj_t BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern bool_t rgc_buffer_eof_p(obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzf2rpzf2zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t);
static obj_t BGl__peekzd2charzd2zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_z62readzf2rpz90zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_input_6_10_2z00(void);
extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__r4_input_6_10_2z00(void);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62eofzd2objectzb0zz__r4_input_6_10_2z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_filezd2lineszd2zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_input_6_10_2z00(void);
static obj_t BGl_gczd2rootszd2initz00zz__r4_input_6_10_2z00(void);
BGL_EXPORTED_DECL obj_t BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_peekzd2charzd2zz__r4_input_6_10_2z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t);
extern obj_t BGl_z62iozd2errorzb0zz__objectz00;
static obj_t BGl__sendzd2filezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__r4_input_6_10_2z00(void);
extern long bgl_rgc_blit_string(obj_t, char *, long, long);
static obj_t BGl__readzd2stringzd2zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31401ze3ze5zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t, long, long, obj_t);
BGL_EXPORTED_DECL long BGl_sendzd2filezd2zz__r4_input_6_10_2z00(obj_t, obj_t, long, long);
extern obj_t bgl_password(char *);
static obj_t BGl__readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t bgl_reverse_bang(obj_t);
extern obj_t bgl_sendchars(obj_t, obj_t, long, long);
static obj_t BGl__peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl__unreadzd2charz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_z62filezd2lineszb0zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t, long, long, obj_t);
static obj_t BGl__readzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_eofzd2objectzf3z21zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl__charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t, obj_t);
extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
extern obj_t bgl_sendfile(obj_t, obj_t, long, long);
extern bool_t rgc_buffer_insert_char(obj_t, int);
static obj_t BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_symbol4203z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t);
extern int rgc_buffer_unget_char(obj_t, int);
static obj_t BGl_symbol4207z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_symbol4209z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl__readzd2charzd2zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzf2lalrpzf2zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32812ze3ze5zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_z62portzd2ze3stringzd2listz81zz__r4_input_6_10_2z00(obj_t, obj_t);
extern long default_io_bufsiz;
BGL_EXPORTED_DECL obj_t BGl_readzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol4212z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_methodzd2initzd2zz__r4_input_6_10_2z00(void);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol4216z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4219z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_readzd2charzd2zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_z62filezd2ze3stringz53zz__r4_input_6_10_2z00(obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_symbol4302z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4221z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t);
extern obj_t bgl_find_runtime_type(obj_t);
static obj_t BGl_symbol4224z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4306z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4227z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(obj_t, obj_t);
static obj_t BGl_symbol4229z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(unsigned char, obj_t);
static obj_t BGl_z62readzf2lalrpz90zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t, obj_t, obj_t);
extern bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(int, obj_t);
extern bool_t rgc_fill_buffer(obj_t);
static obj_t BGl__readzd2bytezd2zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_symbol4310z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4314z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4233z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_filezd2ze3stringz31zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl__readzd2lineszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_symbol4318z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t make_string(long, unsigned char);
static obj_t BGl__sendzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
extern obj_t BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_list4206z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t bgl_close_input_port(obj_t);
static obj_t BGl__readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t, obj_t);
extern bool_t bgl_rgc_charready(obj_t);
BGL_EXPORTED_DECL obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_symbol4322z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4241z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4243z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4211z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4245z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4214z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4215z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4218z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t make_string_sans_fill(long);
extern obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_portzd2ze3stringzd2listze3zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza32654ze3ze5zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl__readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t, obj_t);
extern obj_t rgc_buffer_substring(obj_t, long, long);
static obj_t BGl_symbol4250z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4333z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4253z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_readzd2lineszd2zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_symbol4337z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_list4223z00zz__r4_input_6_10_2z00 = BUNSPEC;
static obj_t BGl_symbol4257z00zz__r4_input_6_10_2z00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_sendzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list4226z00zz__r4_input_6_10_2z00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl__unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl__readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32639ze3ze5zz__r4_input_6_10_2z00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_peekzd2bytezd2envz00zz__r4_input_6_10_2z00, BgL_bgl__peekza7d2byteza7d2za74357za7, opt_generic_entry, BGl__peekzd2bytezd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2bytezd2envz00zz__r4_input_6_10_2z00, BgL_bgl__readza7d2byteza7d2za74358za7, opt_generic_entry, BGl__readzd2bytezd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzf2lalrpzd2envz20zz__r4_input_6_10_2z00, BgL_bgl_za762readza7f2lalrpza74359za7, va_generic_entry, BGl_z62readzf2lalrpz90zz__r4_input_6_10_2z00, BUNSPEC, -4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2charszd2envz00zz__r4_input_6_10_2z00, BgL_bgl__readza7d2charsza7d24360z00, opt_generic_entry, BGl__readzd2charszd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sendzd2filezd2envz00zz__r4_input_6_10_2z00, BgL_bgl__sendza7d2fileza7d2za74361za7, opt_generic_entry, BGl__sendzd2filezd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2ze3stringzd2envze3zz__r4_input_6_10_2z00, BgL_bgl_za762fileza7d2za7e3str4362za7, BGl_z62filezd2ze3stringz53zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2lineszd2envz00zz__r4_input_6_10_2z00, BgL_bgl_za762fileza7d2linesza74363za7, BGl_z62filezd2lineszb0zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2linezd2newlinezd2envzd2zz__r4_input_6_10_2z00, BgL_bgl__readza7d2lineza7d2n4364z00, opt_generic_entry, BGl__readzd2linezd2newlinez00zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_peekzd2charzd2envz00zz__r4_input_6_10_2z00, BgL_bgl__peekza7d2charza7d2za74365za7, opt_generic_entry, BGl__peekzd2charzd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2charsz12zd2envz12zz__r4_input_6_10_2z00, BgL_bgl__readza7d2charsza7124366z00, opt_generic_entry, BGl__readzd2charsz12zc0zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4200z00zz__r4_input_6_10_2z00, BgL_bgl_string4200za700za7za7_4367za7, "<@anonymous:1401>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2charzd2envz00zz__r4_input_6_10_2z00, BgL_bgl__readza7d2charza7d2za74368za7, opt_generic_entry, BGl__readzd2charzd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4201z00zz__r4_input_6_10_2z00, BgL_bgl_string4201za700za7za7_4369za7, "class-field", 11 );
DEFINE_STRING( BGl_string4202z00zz__r4_input_6_10_2z00, BgL_bgl_string4202za700za7za7_4370za7, "Can't read on a closed input port", 33 );
DEFINE_STRING( BGl_string4204z00zz__r4_input_6_10_2z00, BgL_bgl_string4204za700za7za7_4371za7, "read/rp", 7 );
DEFINE_STRING( BGl_string4205z00zz__r4_input_6_10_2z00, BgL_bgl_string4205za700za7za7_4372za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string4208z00zz__r4_input_6_10_2z00, BgL_bgl_string4208za700za7za7_4373za7, "apply", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2readyzf3zd2envzf3zz__r4_input_6_10_2z00, BgL_bgl__charza7d2readyza7f34374z00, opt_generic_entry, BGl__charzd2readyzf3z21zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4210z00zz__r4_input_6_10_2z00, BgL_bgl_string4210za700za7za7_4375za7, "grammar", 7 );
DEFINE_STRING( BGl_string4213z00zz__r4_input_6_10_2z00, BgL_bgl_string4213za700za7za7_4376za7, "let", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unreadzd2charz12zd2envz12zz__r4_input_6_10_2z00, BgL_bgl__unreadza7d2charza714377z00, opt_generic_entry, BGl__unreadzd2charz12zc0zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4217z00zz__r4_input_6_10_2z00, BgL_bgl_string4217za700za7za7_4378za7, "list1603", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sendzd2charszf2siza7ezd2envz55zz__r4_input_6_10_2z00, BgL_bgl_za762sendza7d2charsza74379za7, BGl_z62sendzd2charszf2siza7eze5zz__r4_input_6_10_2z00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string4300z00zz__r4_input_6_10_2z00, BgL_bgl_string4300za700za7za7_4380za7, "bint", 4 );
DEFINE_STRING( BGl_string4301z00zz__r4_input_6_10_2z00, BgL_bgl_string4301za700za7za7_4381za7, "Illegal negative length", 23 );
DEFINE_STRING( BGl_string4220z00zz__r4_input_6_10_2z00, BgL_bgl_string4220za700za7za7_4382za7, "$cons", 5 );
DEFINE_STRING( BGl_string4303z00zz__r4_input_6_10_2z00, BgL_bgl_string4303za700za7za7_4383za7, "read-chars!", 11 );
DEFINE_STRING( BGl_string4222z00zz__r4_input_6_10_2z00, BgL_bgl_string4222za700za7za7_4384za7, "opts", 4 );
DEFINE_STRING( BGl_string4304z00zz__r4_input_6_10_2z00, BgL_bgl_string4304za700za7za7_4385za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_STRING( BGl_string4305z00zz__r4_input_6_10_2z00, BgL_bgl_string4305za700za7za7_4386za7, "_read-chars!", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzf2rpzd2envz20zz__r4_input_6_10_2z00, BgL_bgl_za762readza7f2rpza790za74387z00, va_generic_entry, BGl_z62readzf2rpz90zz__r4_input_6_10_2z00, BUNSPEC, -3 );
DEFINE_STRING( BGl_string4225z00zz__r4_input_6_10_2z00, BgL_bgl_string4225za700za7za7_4388za7, "quote", 5 );
DEFINE_STRING( BGl_string4307z00zz__r4_input_6_10_2z00, BgL_bgl_string4307za700za7za7_4389za7, "read-fill-string!", 17 );
DEFINE_STRING( BGl_string4308z00zz__r4_input_6_10_2z00, BgL_bgl_string4308za700za7za7_4390za7, "wrong number of arguments: [3..4] expected, provided", 52 );
DEFINE_STRING( BGl_string4309z00zz__r4_input_6_10_2z00, BgL_bgl_string4309za700za7za7_4391za7, "_read-fill-string!", 18 );
DEFINE_STRING( BGl_string4228z00zz__r4_input_6_10_2z00, BgL_bgl_string4228za700za7za7_4392za7, "cons*", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2linezd2envz00zz__r4_input_6_10_2z00, BgL_bgl__readza7d2lineza7d2za74393za7, opt_generic_entry, BGl__readzd2linezd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2stringzd2envz00zz__r4_input_6_10_2z00, BgL_bgl__readza7d2stringza7d4394z00, opt_generic_entry, BGl__readzd2stringzd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unreadzd2stringz12zd2envz12zz__r4_input_6_10_2z00, BgL_bgl__unreadza7d2string4395za7, opt_generic_entry, BGl__unreadzd2stringz12zc0zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4311z00zz__r4_input_6_10_2z00, BgL_bgl_string4311za700za7za7_4396za7, "unread-char!", 12 );
DEFINE_STRING( BGl_string4230z00zz__r4_input_6_10_2z00, BgL_bgl_string4230za700za7za7_4397za7, "port", 4 );
DEFINE_STRING( BGl_string4312z00zz__r4_input_6_10_2z00, BgL_bgl_string4312za700za7za7_4398za7, "_unread-char!", 13 );
DEFINE_STRING( BGl_string4231z00zz__r4_input_6_10_2z00, BgL_bgl_string4231za700za7za7_4399za7, "read/rp:Wrong number of arguments", 33 );
DEFINE_STRING( BGl_string4313z00zz__r4_input_6_10_2z00, BgL_bgl_string4313za700za7za7_4400za7, "Unread char failed", 18 );
DEFINE_STRING( BGl_string4315z00zz__r4_input_6_10_2z00, BgL_bgl_string4315za700za7za7_4401za7, "unread-string!", 14 );
DEFINE_STRING( BGl_string4234z00zz__r4_input_6_10_2z00, BgL_bgl_string4234za700za7za7_4402za7, "funcall", 7 );
DEFINE_STRING( BGl_string4316z00zz__r4_input_6_10_2z00, BgL_bgl_string4316za700za7za7_4403za7, "_unread-string!", 15 );
DEFINE_STRING( BGl_string4317z00zz__r4_input_6_10_2z00, BgL_bgl_string4317za700za7za7_4404za7, "Unread string failed", 20 );
DEFINE_STRING( BGl_string4236z00zz__r4_input_6_10_2z00, BgL_bgl_string4236za700za7za7_4405za7, "Grammar arity mismatch", 22 );
DEFINE_STRING( BGl_string4237z00zz__r4_input_6_10_2z00, BgL_bgl_string4237za700za7za7_4406za7, "&read/rp", 8 );
DEFINE_STRING( BGl_string4319z00zz__r4_input_6_10_2z00, BgL_bgl_string4319za700za7za7_4407za7, "unread-substring!", 17 );
DEFINE_STRING( BGl_string4238z00zz__r4_input_6_10_2z00, BgL_bgl_string4238za700za7za7_4408za7, "procedure", 9 );
DEFINE_STRING( BGl_string4239z00zz__r4_input_6_10_2z00, BgL_bgl_string4239za700za7za7_4409za7, "read/lalrp:Wrong number of arguments", 36 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2ofzd2stringszd2envzd2zz__r4_input_6_10_2z00, BgL_bgl__readza7d2ofza7d2str4410z00, opt_generic_entry, BGl__readzd2ofzd2stringsz00zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4320z00zz__r4_input_6_10_2z00, BgL_bgl_string4320za700za7za7_4411za7, "_unread-substring!", 18 );
DEFINE_STRING( BGl_string4321z00zz__r4_input_6_10_2z00, BgL_bgl_string4321za700za7za7_4412za7, "Invalid positional parameters", 29 );
DEFINE_STRING( BGl_string4323z00zz__r4_input_6_10_2z00, BgL_bgl_string4323za700za7za7_4413za7, "unread-sustring!", 16 );
DEFINE_STRING( BGl_string4242z00zz__r4_input_6_10_2z00, BgL_bgl_string4242za700za7za7_4414za7, "lalr", 4 );
DEFINE_STRING( BGl_string4324z00zz__r4_input_6_10_2z00, BgL_bgl_string4324za700za7za7_4415za7, "&port->string-list", 18 );
DEFINE_STRING( BGl_string4325z00zz__r4_input_6_10_2z00, BgL_bgl_string4325za700za7za7_4416za7, "file:", 5 );
DEFINE_STRING( BGl_string4244z00zz__r4_input_6_10_2z00, BgL_bgl_string4244za700za7za7_4417za7, "rgc", 3 );
DEFINE_STRING( BGl_string4326z00zz__r4_input_6_10_2z00, BgL_bgl_string4326za700za7za7_4418za7, "file->string", 12 );
DEFINE_STRING( BGl_string4327z00zz__r4_input_6_10_2z00, BgL_bgl_string4327za700za7za7_4419za7, "&file->string", 13 );
DEFINE_STRING( BGl_string4246z00zz__r4_input_6_10_2z00, BgL_bgl_string4246za700za7za7_4420za7, "eof-object?-env", 15 );
DEFINE_STRING( BGl_string4328z00zz__r4_input_6_10_2z00, BgL_bgl_string4328za700za7za7_4421za7, "<@anonymous:2639>", 17 );
DEFINE_STRING( BGl_string4247z00zz__r4_input_6_10_2z00, BgL_bgl_string4247za700za7za7_4422za7, "read/lalrp", 10 );
DEFINE_STRING( BGl_string4329z00zz__r4_input_6_10_2z00, BgL_bgl_string4329za700za7za7_4423za7, "send-chars/size", 15 );
DEFINE_STRING( BGl_string4248z00zz__r4_input_6_10_2z00, BgL_bgl_string4248za700za7za7_4424za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2fillzd2stringz12zd2envzc0zz__r4_input_6_10_2z00, BgL_bgl__readza7d2fillza7d2s4425z00, opt_generic_entry, BGl__readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_eofzd2objectzd2envz00zz__r4_input_6_10_2z00, BgL_bgl_za762eofza7d2objectza74426za7, BGl_z62eofzd2objectzb0zz__r4_input_6_10_2z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string4330z00zz__r4_input_6_10_2z00, BgL_bgl_string4330za700za7za7_4427za7, "&send-chars/size", 16 );
DEFINE_STRING( BGl_string4331z00zz__r4_input_6_10_2z00, BgL_bgl_string4331za700za7za7_4428za7, "output-port", 11 );
DEFINE_STRING( BGl_string4332z00zz__r4_input_6_10_2z00, BgL_bgl_string4332za700za7za7_4429za7, "belong", 6 );
DEFINE_STRING( BGl_string4251z00zz__r4_input_6_10_2z00, BgL_bgl_string4251za700za7za7_4430za7, "arg1605", 7 );
DEFINE_STRING( BGl_string4252z00zz__r4_input_6_10_2z00, BgL_bgl_string4252za700za7za7_4431za7, "&read/lalrp", 11 );
DEFINE_STRING( BGl_string4334z00zz__r4_input_6_10_2z00, BgL_bgl_string4334za700za7za7_4432za7, "send-chars::long", 16 );
DEFINE_STRING( BGl_string4335z00zz__r4_input_6_10_2z00, BgL_bgl_string4335za700za7za7_4433za7, "wrong number of arguments: [2..4] expected, provided", 52 );
DEFINE_STRING( BGl_string4254z00zz__r4_input_6_10_2z00, BgL_bgl_string4254za700za7za7_4434za7, "read-char", 9 );
DEFINE_STRING( BGl_string4336z00zz__r4_input_6_10_2z00, BgL_bgl_string4336za700za7za7_4435za7, "_send-chars", 11 );
DEFINE_STRING( BGl_string4255z00zz__r4_input_6_10_2z00, BgL_bgl_string4255za700za7za7_4436za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string4256z00zz__r4_input_6_10_2z00, BgL_bgl_string4256za700za7za7_4437za7, "STATE-0-1040", 12 );
DEFINE_STRING( BGl_string4338z00zz__r4_input_6_10_2z00, BgL_bgl_string4338za700za7za7_4438za7, "send-chars", 10 );
DEFINE_STRING( BGl_string4339z00zz__r4_input_6_10_2z00, BgL_bgl_string4339za700za7za7_4439za7, "Illegal size", 12 );
DEFINE_STRING( BGl_string4258z00zz__r4_input_6_10_2z00, BgL_bgl_string4258za700za7za7_4440za7, "peek-char", 9 );
DEFINE_STRING( BGl_string4259z00zz__r4_input_6_10_2z00, BgL_bgl_string4259za700za7za7_4441za7, "STATE-0-1048", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_eofzd2objectzf3zd2envzf3zz__r4_input_6_10_2z00, BgL_bgl_za762eofza7d2objectza74442za7, BGl_z62eofzd2objectzf3z43zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4340z00zz__r4_input_6_10_2z00, BgL_bgl_string4340za700za7za7_4443za7, "Illegal offset", 14 );
DEFINE_STRING( BGl_string4342z00zz__r4_input_6_10_2z00, BgL_bgl_string4342za700za7za7_4444za7, "send-file::long", 15 );
DEFINE_STRING( BGl_string4261z00zz__r4_input_6_10_2z00, BgL_bgl_string4261za700za7za7_4445za7, "read-byte", 9 );
DEFINE_STRING( BGl_string4343z00zz__r4_input_6_10_2z00, BgL_bgl_string4343za700za7za7_4446za7, "_send-file", 10 );
DEFINE_STRING( BGl_string4262z00zz__r4_input_6_10_2z00, BgL_bgl_string4262za700za7za7_4447za7, "STATE-0-1054", 12 );
DEFINE_STRING( BGl_string4344z00zz__r4_input_6_10_2z00, BgL_bgl_string4344za700za7za7_4448za7, "send-file", 9 );
DEFINE_STRING( BGl_string4345z00zz__r4_input_6_10_2z00, BgL_bgl_string4345za700za7za7_4449za7, "<@anonymous:2654>", 17 );
DEFINE_STRING( BGl_string4264z00zz__r4_input_6_10_2z00, BgL_bgl_string4264za700za7za7_4450za7, "peek-byte", 9 );
DEFINE_STRING( BGl_string4183z00zz__r4_input_6_10_2z00, BgL_bgl_string4183za700za7za7_4451za7, "/tmp/bigloo/runtime/Ieee/input.scm", 34 );
DEFINE_STRING( BGl_string4346z00zz__r4_input_6_10_2z00, BgL_bgl_string4346za700za7za7_4452za7, "&file-lines", 11 );
DEFINE_STRING( BGl_string4265z00zz__r4_input_6_10_2z00, BgL_bgl_string4265za700za7za7_4453za7, "STATE-0-1060", 12 );
DEFINE_STRING( BGl_string4184z00zz__r4_input_6_10_2z00, BgL_bgl_string4184za700za7za7_4454za7, "ignore", 6 );
DEFINE_STRING( BGl_string4185z00zz__r4_input_6_10_2z00, BgL_bgl_string4185za700za7za7_4455za7, "input-port", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_passwordzd2envzd2zz__r4_input_6_10_2z00, BgL_bgl__passwordza700za7za7__4456za7, opt_generic_entry, BGl__passwordz00zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4348z00zz__r4_input_6_10_2z00, BgL_bgl_string4348za700za7za7_4457za7, "file-lines", 10 );
DEFINE_STRING( BGl_string4267z00zz__r4_input_6_10_2z00, BgL_bgl_string4267za700za7za7_4458za7, "char-ready?", 11 );
DEFINE_STRING( BGl_string4186z00zz__r4_input_6_10_2z00, BgL_bgl_string4186za700za7za7_4459za7, "regular-grammar", 15 );
DEFINE_STRING( BGl_string4349z00zz__r4_input_6_10_2z00, BgL_bgl_string4349za700za7za7_4460za7, "Illegal files", 13 );
DEFINE_STRING( BGl_string4268z00zz__r4_input_6_10_2z00, BgL_bgl_string4268za700za7za7_4461za7, "_char-ready?", 12 );
DEFINE_STRING( BGl_string4187z00zz__r4_input_6_10_2z00, BgL_bgl_string4187za700za7za7_4462za7, "Illegal match", 13 );
DEFINE_STRING( BGl_string4188z00zz__r4_input_6_10_2z00, BgL_bgl_string4188za700za7za7_4463za7, "the-failure", 11 );
DEFINE_STRING( BGl_string4189z00zz__r4_input_6_10_2z00, BgL_bgl_string4189za700za7za7_4464za7, "the-substring", 13 );
DEFINE_STRING( BGl_string4350z00zz__r4_input_6_10_2z00, BgL_bgl_string4350za700za7za7_4465za7, "gram", 4 );
DEFINE_STRING( BGl_string4351z00zz__r4_input_6_10_2z00, BgL_bgl_string4351za700za7za7_4466za7, "&file-position->line", 20 );
DEFINE_STRING( BGl_string4270z00zz__r4_input_6_10_2z00, BgL_bgl_string4270za700za7za7_4467za7, "read-line", 9 );
DEFINE_STRING( BGl_string4352z00zz__r4_input_6_10_2z00, BgL_bgl_string4352za700za7za7_4468za7, "&<@anonymous:2812>", 18 );
DEFINE_STRING( BGl_string4271z00zz__r4_input_6_10_2z00, BgL_bgl_string4271za700za7za7_4469za7, "STATE-1-1069", 12 );
DEFINE_STRING( BGl_string4190z00zz__r4_input_6_10_2z00, BgL_bgl_string4190za700za7za7_4470za7, "Illegal range `~a'", 18 );
DEFINE_STRING( BGl_string4272z00zz__r4_input_6_10_2z00, BgL_bgl_string4272za700za7za7_4471za7, "STATE-0-1068", 12 );
DEFINE_STRING( BGl_string4191z00zz__r4_input_6_10_2z00, BgL_bgl_string4191za700za7za7_4472za7, "bstring", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_portzd2ze3stringzd2listzd2envz31zz__r4_input_6_10_2z00, BgL_bgl_za762portza7d2za7e3str4473za7, BGl_z62portzd2ze3stringzd2listz81zz__r4_input_6_10_2z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4354z00zz__r4_input_6_10_2z00, BgL_bgl_string4354za700za7za7_4474za7, "password", 8 );
DEFINE_STRING( BGl_string4273z00zz__r4_input_6_10_2z00, BgL_bgl_string4273za700za7za7_4475za7, "STATE-3-1071", 12 );
DEFINE_STRING( BGl_string4192z00zz__r4_input_6_10_2z00, BgL_bgl_string4192za700za7za7_4476za7, "STATE-8-1113", 12 );
DEFINE_STRING( BGl_string4355z00zz__r4_input_6_10_2z00, BgL_bgl_string4355za700za7za7_4477za7, "_password", 9 );
DEFINE_STRING( BGl_string4274z00zz__r4_input_6_10_2z00, BgL_bgl_string4274za700za7za7_4478za7, "STATE-6-1074", 12 );
DEFINE_STRING( BGl_string4193z00zz__r4_input_6_10_2z00, BgL_bgl_string4193za700za7za7_4479za7, "STATE-6-1111", 12 );
DEFINE_STRING( BGl_string4356z00zz__r4_input_6_10_2z00, BgL_bgl_string4356za700za7za7_4480za7, "__r4_input_6_10_2", 17 );
DEFINE_STRING( BGl_string4275z00zz__r4_input_6_10_2z00, BgL_bgl_string4275za700za7za7_4481za7, "STATE-8-1076", 12 );
DEFINE_STRING( BGl_string4194z00zz__r4_input_6_10_2z00, BgL_bgl_string4194za700za7za7_4482za7, "STATE-9-1114", 12 );
DEFINE_STRING( BGl_string4276z00zz__r4_input_6_10_2z00, BgL_bgl_string4276za700za7za7_4483za7, "", 0 );
DEFINE_STRING( BGl_string4195z00zz__r4_input_6_10_2z00, BgL_bgl_string4195za700za7za7_4484za7, "STATE-0-1105", 12 );
DEFINE_STRING( BGl_string4277z00zz__r4_input_6_10_2z00, BgL_bgl_string4277za700za7za7_4485za7, "loop", 4 );
DEFINE_STRING( BGl_string4196z00zz__r4_input_6_10_2z00, BgL_bgl_string4196za700za7za7_4486za7, "STATE-4-1109", 12 );
DEFINE_STRING( BGl_string4278z00zz__r4_input_6_10_2z00, BgL_bgl_string4278za700za7za7_4487za7, "bchar", 5 );
DEFINE_STRING( BGl_string4197z00zz__r4_input_6_10_2z00, BgL_bgl_string4197za700za7za7_4488za7, "STATE-2-1107", 12 );
DEFINE_STRING( BGl_string4279z00zz__r4_input_6_10_2z00, BgL_bgl_string4279za700za7za7_4489za7, "string-set!", 11 );
DEFINE_STRING( BGl_string4198z00zz__r4_input_6_10_2z00, BgL_bgl_string4198za700za7za7_4490za7, "STATE-3-1108", 12 );
DEFINE_STRING( BGl_string4199z00zz__r4_input_6_10_2z00, BgL_bgl_string4199za700za7za7_4491za7, "STATE-1-1106", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_readzd2lineszd2envz00zz__r4_input_6_10_2z00, BgL_bgl__readza7d2linesza7d24492z00, opt_generic_entry, BGl__readzd2lineszd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sendzd2charszd2envz00zz__r4_input_6_10_2z00, BgL_bgl__sendza7d2charsza7d24493z00, opt_generic_entry, BGl__sendzd2charszd2zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4281z00zz__r4_input_6_10_2z00, BgL_bgl_string4281za700za7za7_4494za7, "read-line-newline", 17 );
DEFINE_STRING( BGl_string4282z00zz__r4_input_6_10_2z00, BgL_bgl_string4282za700za7za7_4495za7, "STATE-6-1089", 12 );
DEFINE_STRING( BGl_string4283z00zz__r4_input_6_10_2z00, BgL_bgl_string4283za700za7za7_4496za7, "STATE-3-1086", 12 );
DEFINE_STRING( BGl_string4284z00zz__r4_input_6_10_2z00, BgL_bgl_string4284za700za7za7_4497za7, "STATE-0-1083", 12 );
DEFINE_STRING( BGl_string4285z00zz__r4_input_6_10_2z00, BgL_bgl_string4285za700za7za7_4498za7, "STATE-1-1084", 12 );
DEFINE_STRING( BGl_string4286z00zz__r4_input_6_10_2z00, BgL_bgl_string4286za700za7za7_4499za7, "STATE-7-1090", 12 );
DEFINE_STRING( BGl_string4288z00zz__r4_input_6_10_2z00, BgL_bgl_string4288za700za7za7_4500za7, "read-lines", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unreadzd2substringz12zd2envz12zz__r4_input_6_10_2z00, BgL_bgl__unreadza7d2substr4501za7, opt_generic_entry, BGl__unreadzd2substringz12zc0zz__r4_input_6_10_2z00, BFALSE, -1 );
DEFINE_STRING( BGl_string4290z00zz__r4_input_6_10_2z00, BgL_bgl_string4290za700za7za7_4502za7, "read-string", 11 );
DEFINE_STRING( BGl_string4291z00zz__r4_input_6_10_2z00, BgL_bgl_string4291za700za7za7_4503za7, "STATE-2-1101", 12 );
DEFINE_STRING( BGl_string4292z00zz__r4_input_6_10_2z00, BgL_bgl_string4292za700za7za7_4504za7, "STATE-1-1100", 12 );
DEFINE_STRING( BGl_string4293z00zz__r4_input_6_10_2z00, BgL_bgl_string4293za700za7za7_4505za7, "STATE-0-1099", 12 );
DEFINE_STRING( BGl_string4295z00zz__r4_input_6_10_2z00, BgL_bgl_string4295za700za7za7_4506za7, "read-of-strings", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2positionzd2ze3linezd2envz31zz__r4_input_6_10_2z00, BgL_bgl_za762fileza7d2positi4507z00, BGl_z62filezd2positionzd2ze3linez81zz__r4_input_6_10_2z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4297z00zz__r4_input_6_10_2z00, BgL_bgl_string4297za700za7za7_4508za7, "read-chars", 10 );
DEFINE_STRING( BGl_string4298z00zz__r4_input_6_10_2z00, BgL_bgl_string4298za700za7za7_4509za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string4299z00zz__r4_input_6_10_2z00, BgL_bgl_string4299za700za7za7_4510za7, "integer", 7 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol4341z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4260z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4263z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4232z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4347z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4266z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4235z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4269z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4353z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4240z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4249z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4280z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4287z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4289z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4294z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4296z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4203z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4207z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4209z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4212z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4216z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4219z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4302z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4221z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4224z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4306z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4227z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4229z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4310z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4314z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4233z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4318z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4206z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4322z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4241z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4243z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4211z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4245z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4214z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4215z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4218z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4250z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4333z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4253z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4337z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4223z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_symbol4257z00zz__r4_input_6_10_2z00) );
ADD_ROOT( (void *)(&BGl_list4226z00zz__r4_input_6_10_2z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long BgL_checksumz00_7080, char * BgL_fromz00_7081)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_input_6_10_2z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_input_6_10_2z00(); 
BGl_cnstzd2initzd2zz__r4_input_6_10_2z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_input_6_10_2z00(); 
return 
BGl_toplevelzd2initzd2zz__r4_input_6_10_2z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
BGl_symbol4203z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4204z00zz__r4_input_6_10_2z00); 
BGl_symbol4207z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4208z00zz__r4_input_6_10_2z00); 
BGl_symbol4209z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4210z00zz__r4_input_6_10_2z00); 
BGl_symbol4212z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4213z00zz__r4_input_6_10_2z00); 
BGl_symbol4216z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4217z00zz__r4_input_6_10_2z00); 
BGl_symbol4219z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4220z00zz__r4_input_6_10_2z00); 
BGl_symbol4221z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4222z00zz__r4_input_6_10_2z00); 
BGl_symbol4224z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4225z00zz__r4_input_6_10_2z00); 
BGl_list4223z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4224z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BNIL, BNIL)); 
BGl_list4218z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4219z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4221z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_list4223z00zz__r4_input_6_10_2z00, BNIL))); 
BGl_list4215z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4216z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_list4218z00zz__r4_input_6_10_2z00, BNIL)); 
BGl_list4214z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_list4215z00zz__r4_input_6_10_2z00, BNIL); 
BGl_symbol4227z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4228z00zz__r4_input_6_10_2z00); 
BGl_symbol4229z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4230z00zz__r4_input_6_10_2z00); 
BGl_list4226z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4227z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4229z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4216z00zz__r4_input_6_10_2z00, BNIL))); 
BGl_list4211z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4212z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_list4214z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_list4226z00zz__r4_input_6_10_2z00, BNIL))); 
BGl_list4206z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4207z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4209z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_list4211z00zz__r4_input_6_10_2z00, BNIL))); 
BGl_symbol4233z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4234z00zz__r4_input_6_10_2z00); 
BGl_list4232z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4233z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4209z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4209z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4229z00zz__r4_input_6_10_2z00, BNIL)))); 
BGl_list4235z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4233z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4209z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4209z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4229z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BUNSPEC, BNIL))))); 
BGl_symbol4241z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4242z00zz__r4_input_6_10_2z00); 
BGl_symbol4243z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4244z00zz__r4_input_6_10_2z00); 
BGl_symbol4245z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4246z00zz__r4_input_6_10_2z00); 
BGl_list4240z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4233z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4241z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4241z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4243z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4229z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4245z00zz__r4_input_6_10_2z00, BNIL)))))); 
BGl_symbol4250z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4251z00zz__r4_input_6_10_2z00); 
BGl_list4249z00zz__r4_input_6_10_2z00 = 
MAKE_YOUNG_PAIR(BGl_symbol4233z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4241z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4241z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4243z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4229z00zz__r4_input_6_10_2z00, 
MAKE_YOUNG_PAIR(BGl_symbol4250z00zz__r4_input_6_10_2z00, BNIL)))))); 
BGl_symbol4253z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4254z00zz__r4_input_6_10_2z00); 
BGl_symbol4257z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4258z00zz__r4_input_6_10_2z00); 
BGl_symbol4260z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4261z00zz__r4_input_6_10_2z00); 
BGl_symbol4263z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4264z00zz__r4_input_6_10_2z00); 
BGl_symbol4266z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4267z00zz__r4_input_6_10_2z00); 
BGl_symbol4269z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4270z00zz__r4_input_6_10_2z00); 
BGl_symbol4280z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4281z00zz__r4_input_6_10_2z00); 
BGl_symbol4287z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4288z00zz__r4_input_6_10_2z00); 
BGl_symbol4289z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4290z00zz__r4_input_6_10_2z00); 
BGl_symbol4294z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4295z00zz__r4_input_6_10_2z00); 
BGl_symbol4296z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4297z00zz__r4_input_6_10_2z00); 
BGl_symbol4302z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4303z00zz__r4_input_6_10_2z00); 
BGl_symbol4306z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4307z00zz__r4_input_6_10_2z00); 
BGl_symbol4310z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4311z00zz__r4_input_6_10_2z00); 
BGl_symbol4314z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4315z00zz__r4_input_6_10_2z00); 
BGl_symbol4318z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4319z00zz__r4_input_6_10_2z00); 
BGl_symbol4322z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4323z00zz__r4_input_6_10_2z00); 
BGl_symbol4333z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4334z00zz__r4_input_6_10_2z00); 
BGl_symbol4337z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4338z00zz__r4_input_6_10_2z00); 
BGl_symbol4341z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4342z00zz__r4_input_6_10_2z00); 
BGl_symbol4347z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4348z00zz__r4_input_6_10_2z00); 
return ( 
BGl_symbol4353z00zz__r4_input_6_10_2z00 = 
bstring_to_symbol(BGl_string4354z00zz__r4_input_6_10_2z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
{ /* Ieee/input.scm 307 */
 obj_t BgL_zc3z04anonymousza31401ze3z87_5987;
{ 
 int BgL_tmpz00_7165;
BgL_tmpz00_7165 = 
(int)(0L); 
BgL_zc3z04anonymousza31401ze3z87_5987 = 
MAKE_EL_PROCEDURE(BgL_tmpz00_7165); } 
return ( 
BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00 = BgL_zc3z04anonymousza31401ze3z87_5987, BUNSPEC) ;} } 

}



/* &<@anonymous:1401> */
obj_t BGl_z62zc3z04anonymousza31401ze3ze5zz__r4_input_6_10_2z00(obj_t BgL_envz00_5988, obj_t BgL_iportz00_5989)
{
{ /* Ieee/input.scm 307 */
{ 
 obj_t BgL_iportz00_6910; long BgL_lastzd2matchzd2_6911; long BgL_forwardz00_6912; long BgL_bufposz00_6913; obj_t BgL_iportz00_6894; long BgL_lastzd2matchzd2_6895; long BgL_forwardz00_6896; long BgL_bufposz00_6897; obj_t BgL_iportz00_6881; long BgL_lastzd2matchzd2_6882; long BgL_forwardz00_6883; long BgL_bufposz00_6884; obj_t BgL_iportz00_6867; long BgL_lastzd2matchzd2_6868; long BgL_forwardz00_6869; long BgL_bufposz00_6870; obj_t BgL_iportz00_6856; long BgL_lastzd2matchzd2_6857; long BgL_forwardz00_6858; long BgL_bufposz00_6859; obj_t BgL_iportz00_6843; long BgL_lastzd2matchzd2_6844; long BgL_forwardz00_6845; long BgL_bufposz00_6846; obj_t BgL_iportz00_6832; long BgL_lastzd2matchzd2_6833; long BgL_forwardz00_6834; long BgL_bufposz00_6835; obj_t BgL_iportz00_6819; long BgL_lastzd2matchzd2_6820; long BgL_forwardz00_6821; long BgL_bufposz00_6822; int BgL_minz00_6801; int BgL_maxz00_6802;
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4512z00_7168;
{ /* Ieee/input.scm 307 */
 obj_t BgL_arg1408z00_6923;
{ /* Ieee/input.scm 307 */
 obj_t BgL_res3454z00_6924;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_res3454z00_6924 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7171;
BgL_auxz00_7171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4200z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7171,BFALSE,BFALSE);} 
BgL_arg1408z00_6923 = BgL_res3454z00_6924; } 
BgL_test4512z00_7168 = 
INPUT_PORT_CLOSEP(BgL_arg1408z00_6923); } 
if(BgL_test4512z00_7168)
{ /* Ieee/input.scm 307 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1404z00_6925;
{ /* Ieee/input.scm 307 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_6926;
{ /* Ieee/input.scm 307 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_6927;
BgL_new1045z00_6927 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 307 */
 long BgL_arg1407z00_6928;
BgL_arg1407z00_6928 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_6927), BgL_arg1407z00_6928); } 
BgL_new1046z00_6926 = BgL_new1045z00_6927; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_6926)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_6926)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_7184;
{ /* Ieee/input.scm 307 */
 obj_t BgL_arg1405z00_6929;
{ /* Ieee/input.scm 307 */
 obj_t BgL_arg1406z00_6930;
{ /* Ieee/input.scm 307 */
 obj_t BgL_classz00_6931;
BgL_classz00_6931 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1406z00_6930 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_6931); } 
BgL_arg1405z00_6929 = 
VECTOR_REF(BgL_arg1406z00_6930,2L); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_auxz00_7188;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1405z00_6929))
{ /* Ieee/input.scm 307 */
BgL_auxz00_7188 = BgL_arg1405z00_6929
; }  else 
{ 
 obj_t BgL_auxz00_7191;
BgL_auxz00_7191 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4200z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg1405z00_6929); 
FAILURE(BgL_auxz00_7191,BFALSE,BFALSE);} 
BgL_auxz00_7184 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_7188); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_6926)))->BgL_stackz00)=((obj_t)BgL_auxz00_7184),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_6926)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_6926)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_7201;
{ /* Ieee/input.scm 307 */
 obj_t BgL_res3455z00_6932;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_res3455z00_6932 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7205;
BgL_auxz00_7205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4200z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7205,BFALSE,BFALSE);} 
BgL_auxz00_7201 = BgL_res3455z00_6932; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_6926)))->BgL_objz00)=((obj_t)BgL_auxz00_7201),BUNSPEC); } 
BgL_arg1404z00_6925 = BgL_new1046z00_6926; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1404z00_6925));}  else 
{ /* Ieee/input.scm 307 */
BgL_ignorez00_6774:
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6785;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6785 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7214;
BgL_auxz00_7214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7214,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_6785); } 
{ /* Ieee/input.scm 307 */
 long BgL_matchz00_6786;
{ /* Ieee/input.scm 307 */
 long BgL_arg1593z00_6787; long BgL_arg1594z00_6788;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6789;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6789 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7221;
BgL_auxz00_7221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7221,BFALSE,BFALSE);} 
BgL_arg1593z00_6787 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6789); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6790;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6790 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7228;
BgL_auxz00_7228 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7228,BFALSE,BFALSE);} 
BgL_arg1594z00_6788 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6790); } 
BgL_iportz00_6856 = BgL_iportz00_5989; 
BgL_lastzd2matchzd2_6857 = 4L; 
BgL_forwardz00_6858 = BgL_arg1593z00_6787; 
BgL_bufposz00_6859 = BgL_arg1594z00_6788; 
BgL_statezd20zd21105z00_6780:
if(
(BgL_forwardz00_6858==BgL_bufposz00_6859))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4520z00_7235;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6860;
if(
INPUT_PORTP(BgL_iportz00_6856))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6860 = BgL_iportz00_6856; }  else 
{ 
 obj_t BgL_auxz00_7238;
BgL_auxz00_7238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4195z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6856); 
FAILURE(BgL_auxz00_7238,BFALSE,BFALSE);} 
BgL_test4520z00_7235 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6860); } 
if(BgL_test4520z00_7235)
{ /* Ieee/input.scm 307 */
 long BgL_arg1455z00_6861; long BgL_arg1456z00_6862;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6863;
if(
INPUT_PORTP(BgL_iportz00_6856))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6863 = BgL_iportz00_6856; }  else 
{ 
 obj_t BgL_auxz00_7245;
BgL_auxz00_7245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4195z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6856); 
FAILURE(BgL_auxz00_7245,BFALSE,BFALSE);} 
BgL_arg1455z00_6861 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6863); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6864;
if(
INPUT_PORTP(BgL_iportz00_6856))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6864 = BgL_iportz00_6856; }  else 
{ 
 obj_t BgL_auxz00_7252;
BgL_auxz00_7252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4195z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6856); 
FAILURE(BgL_auxz00_7252,BFALSE,BFALSE);} 
BgL_arg1456z00_6862 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6864); } 
{ 
 long BgL_bufposz00_7258; long BgL_forwardz00_7257;
BgL_forwardz00_7257 = BgL_arg1455z00_6861; 
BgL_bufposz00_7258 = BgL_arg1456z00_6862; 
BgL_bufposz00_6859 = BgL_bufposz00_7258; 
BgL_forwardz00_6858 = BgL_forwardz00_7257; 
goto BgL_statezd20zd21105z00_6780;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_lastzd2matchzd2_6857; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6865;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6866;
if(
INPUT_PORTP(BgL_iportz00_6856))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6866 = BgL_iportz00_6856; }  else 
{ 
 obj_t BgL_auxz00_7261;
BgL_auxz00_7261 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4195z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6856); 
FAILURE(BgL_auxz00_7261,BFALSE,BFALSE);} 
BgL_curz00_6865 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6866, BgL_forwardz00_6858); } 
{ /* Ieee/input.scm 307 */

if(
(
(long)(BgL_curz00_6865)==34L))
{ /* Ieee/input.scm 307 */
BgL_iportz00_6894 = BgL_iportz00_6856; 
BgL_lastzd2matchzd2_6895 = BgL_lastzd2matchzd2_6857; 
BgL_forwardz00_6896 = 
(1L+BgL_forwardz00_6858); 
BgL_bufposz00_6897 = BgL_bufposz00_6859; 
BgL_statezd23zd21108z00_6783:
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6898;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6899;
if(
INPUT_PORTP(BgL_iportz00_6894))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6899 = BgL_iportz00_6894; }  else 
{ 
 obj_t BgL_auxz00_7271;
BgL_auxz00_7271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4198z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6894); 
FAILURE(BgL_auxz00_7271,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6899, BgL_forwardz00_6896); } 
BgL_newzd2matchzd2_6898 = 1L; 
if(
(BgL_forwardz00_6896==BgL_bufposz00_6897))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4528z00_7278;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6900;
if(
INPUT_PORTP(BgL_iportz00_6894))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6900 = BgL_iportz00_6894; }  else 
{ 
 obj_t BgL_auxz00_7281;
BgL_auxz00_7281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4198z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6894); 
FAILURE(BgL_auxz00_7281,BFALSE,BFALSE);} 
BgL_test4528z00_7278 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6900); } 
if(BgL_test4528z00_7278)
{ /* Ieee/input.scm 307 */
 long BgL_arg1422z00_6901; long BgL_arg1423z00_6902;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6903;
if(
INPUT_PORTP(BgL_iportz00_6894))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6903 = BgL_iportz00_6894; }  else 
{ 
 obj_t BgL_auxz00_7288;
BgL_auxz00_7288 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4198z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6894); 
FAILURE(BgL_auxz00_7288,BFALSE,BFALSE);} 
BgL_arg1422z00_6901 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6903); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6904;
if(
INPUT_PORTP(BgL_iportz00_6894))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6904 = BgL_iportz00_6894; }  else 
{ 
 obj_t BgL_auxz00_7295;
BgL_auxz00_7295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4198z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6894); 
FAILURE(BgL_auxz00_7295,BFALSE,BFALSE);} 
BgL_arg1423z00_6902 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6904); } 
{ 
 long BgL_bufposz00_7301; long BgL_forwardz00_7300;
BgL_forwardz00_7300 = BgL_arg1422z00_6901; 
BgL_bufposz00_7301 = BgL_arg1423z00_6902; 
BgL_bufposz00_6897 = BgL_bufposz00_7301; 
BgL_forwardz00_6896 = BgL_forwardz00_7300; 
goto BgL_statezd23zd21108z00_6783;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6898; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6905;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6906;
if(
INPUT_PORTP(BgL_iportz00_6894))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6906 = BgL_iportz00_6894; }  else 
{ 
 obj_t BgL_auxz00_7304;
BgL_auxz00_7304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4198z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6894); 
FAILURE(BgL_auxz00_7304,BFALSE,BFALSE);} 
BgL_curz00_6905 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6906, BgL_forwardz00_6896); } 
{ /* Ieee/input.scm 307 */

if(
(
(long)(BgL_curz00_6905)==92L))
{ /* Ieee/input.scm 307 */
BgL_iportz00_6832 = BgL_iportz00_6894; 
BgL_lastzd2matchzd2_6833 = BgL_newzd2matchzd2_6898; 
BgL_forwardz00_6834 = 
(1L+BgL_forwardz00_6896); 
BgL_bufposz00_6835 = BgL_bufposz00_6897; 
BgL_statezd26zd21111z00_6778:
if(
(BgL_forwardz00_6834==BgL_bufposz00_6835))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4535z00_7314;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6836;
if(
INPUT_PORTP(BgL_iportz00_6832))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6836 = BgL_iportz00_6832; }  else 
{ 
 obj_t BgL_auxz00_7317;
BgL_auxz00_7317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4193z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6832); 
FAILURE(BgL_auxz00_7317,BFALSE,BFALSE);} 
BgL_test4535z00_7314 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6836); } 
if(BgL_test4535z00_7314)
{ /* Ieee/input.scm 307 */
 long BgL_arg1477z00_6837; long BgL_arg1478z00_6838;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6839;
if(
INPUT_PORTP(BgL_iportz00_6832))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6839 = BgL_iportz00_6832; }  else 
{ 
 obj_t BgL_auxz00_7324;
BgL_auxz00_7324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4193z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6832); 
FAILURE(BgL_auxz00_7324,BFALSE,BFALSE);} 
BgL_arg1477z00_6837 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6839); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6840;
if(
INPUT_PORTP(BgL_iportz00_6832))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6840 = BgL_iportz00_6832; }  else 
{ 
 obj_t BgL_auxz00_7331;
BgL_auxz00_7331 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4193z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6832); 
FAILURE(BgL_auxz00_7331,BFALSE,BFALSE);} 
BgL_arg1478z00_6838 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6840); } 
{ 
 long BgL_bufposz00_7337; long BgL_forwardz00_7336;
BgL_forwardz00_7336 = BgL_arg1477z00_6837; 
BgL_bufposz00_7337 = BgL_arg1478z00_6838; 
BgL_bufposz00_6835 = BgL_bufposz00_7337; 
BgL_forwardz00_6834 = BgL_forwardz00_7336; 
goto BgL_statezd26zd21111z00_6778;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_lastzd2matchzd2_6833; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6841;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6842;
if(
INPUT_PORTP(BgL_iportz00_6832))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6842 = BgL_iportz00_6832; }  else 
{ 
 obj_t BgL_auxz00_7340;
BgL_auxz00_7340 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4193z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6832); 
FAILURE(BgL_auxz00_7340,BFALSE,BFALSE);} 
BgL_curz00_6841 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6842, BgL_forwardz00_6834); } 
{ /* Ieee/input.scm 307 */

if(
(
(long)(BgL_curz00_6841)==10L))
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_lastzd2matchzd2_6833; }  else 
{ /* Ieee/input.scm 307 */
BgL_iportz00_6867 = BgL_iportz00_6832; 
BgL_lastzd2matchzd2_6868 = BgL_lastzd2matchzd2_6833; 
BgL_forwardz00_6869 = 
(1L+BgL_forwardz00_6834); 
BgL_bufposz00_6870 = BgL_bufposz00_6835; 
BgL_statezd24zd21109z00_6781:
if(
(BgL_forwardz00_6869==BgL_bufposz00_6870))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4542z00_7350;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6871;
if(
INPUT_PORTP(BgL_iportz00_6867))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6871 = BgL_iportz00_6867; }  else 
{ 
 obj_t BgL_auxz00_7353;
BgL_auxz00_7353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4196z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6867); 
FAILURE(BgL_auxz00_7353,BFALSE,BFALSE);} 
BgL_test4542z00_7350 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6871); } 
if(BgL_test4542z00_7350)
{ /* Ieee/input.scm 307 */
 long BgL_arg1443z00_6872; long BgL_arg1444z00_6873;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6874;
if(
INPUT_PORTP(BgL_iportz00_6867))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6874 = BgL_iportz00_6867; }  else 
{ 
 obj_t BgL_auxz00_7360;
BgL_auxz00_7360 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4196z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6867); 
FAILURE(BgL_auxz00_7360,BFALSE,BFALSE);} 
BgL_arg1443z00_6872 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6874); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6875;
if(
INPUT_PORTP(BgL_iportz00_6867))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6875 = BgL_iportz00_6867; }  else 
{ 
 obj_t BgL_auxz00_7367;
BgL_auxz00_7367 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4196z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6867); 
FAILURE(BgL_auxz00_7367,BFALSE,BFALSE);} 
BgL_arg1444z00_6873 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6875); } 
{ 
 long BgL_bufposz00_7373; long BgL_forwardz00_7372;
BgL_forwardz00_7372 = BgL_arg1443z00_6872; 
BgL_bufposz00_7373 = BgL_arg1444z00_6873; 
BgL_bufposz00_6870 = BgL_bufposz00_7373; 
BgL_forwardz00_6869 = BgL_forwardz00_7372; 
goto BgL_statezd24zd21109z00_6781;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_lastzd2matchzd2_6868; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6876;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6877;
if(
INPUT_PORTP(BgL_iportz00_6867))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6877 = BgL_iportz00_6867; }  else 
{ 
 obj_t BgL_auxz00_7376;
BgL_auxz00_7376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4196z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6867); 
FAILURE(BgL_auxz00_7376,BFALSE,BFALSE);} 
BgL_curz00_6876 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6877, BgL_forwardz00_6869); } 
{ /* Ieee/input.scm 307 */

if(
(
(long)(BgL_curz00_6876)==92L))
{ 
 long BgL_bufposz00_7388; long BgL_forwardz00_7386; long BgL_lastzd2matchzd2_7385; obj_t BgL_iportz00_7384;
BgL_iportz00_7384 = BgL_iportz00_6867; 
BgL_lastzd2matchzd2_7385 = BgL_lastzd2matchzd2_6868; 
BgL_forwardz00_7386 = 
(1L+BgL_forwardz00_6869); 
BgL_bufposz00_7388 = BgL_bufposz00_6870; 
BgL_bufposz00_6835 = BgL_bufposz00_7388; 
BgL_forwardz00_6834 = BgL_forwardz00_7386; 
BgL_lastzd2matchzd2_6833 = BgL_lastzd2matchzd2_7385; 
BgL_iportz00_6832 = BgL_iportz00_7384; 
goto BgL_statezd26zd21111z00_6778;}  else 
{ /* Ieee/input.scm 307 */
if(
(
(long)(BgL_curz00_6876)==34L))
{ /* Ieee/input.scm 307 */
 long BgL_arg1448z00_6878;
BgL_arg1448z00_6878 = 
(1L+BgL_forwardz00_6869); 
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6879;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6880;
if(
INPUT_PORTP(BgL_iportz00_6867))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6880 = BgL_iportz00_6867; }  else 
{ 
 obj_t BgL_auxz00_7395;
BgL_auxz00_7395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4196z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6867); 
FAILURE(BgL_auxz00_7395,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6880, BgL_arg1448z00_6878); } 
BgL_newzd2matchzd2_6879 = 2L; 
BgL_matchz00_6786 = BgL_newzd2matchzd2_6879; } }  else 
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4550z00_7400;
if(
(
(long)(BgL_curz00_6876)==34L))
{ /* Ieee/input.scm 307 */
BgL_test4550z00_7400 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4550z00_7400 = 
(
(long)(BgL_curz00_6876)==92L)
; } 
if(BgL_test4550z00_7400)
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_lastzd2matchzd2_6868; }  else 
{ 
 long BgL_forwardz00_7406;
BgL_forwardz00_7406 = 
(1L+BgL_forwardz00_6869); 
BgL_forwardz00_6869 = BgL_forwardz00_7406; 
goto BgL_statezd24zd21109z00_6781;} } } } } } } } }  else 
{ /* Ieee/input.scm 307 */
if(
(
(long)(BgL_curz00_6905)==34L))
{ /* Ieee/input.scm 307 */
 long BgL_arg1427z00_6907;
BgL_arg1427z00_6907 = 
(1L+BgL_forwardz00_6896); 
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6908;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6909;
if(
INPUT_PORTP(BgL_iportz00_6894))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6909 = BgL_iportz00_6894; }  else 
{ 
 obj_t BgL_auxz00_7416;
BgL_auxz00_7416 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4198z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6894); 
FAILURE(BgL_auxz00_7416,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6909, BgL_arg1427z00_6907); } 
BgL_newzd2matchzd2_6908 = 2L; 
BgL_matchz00_6786 = BgL_newzd2matchzd2_6908; } }  else 
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4554z00_7421;
if(
(
(long)(BgL_curz00_6905)==34L))
{ /* Ieee/input.scm 307 */
BgL_test4554z00_7421 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4554z00_7421 = 
(
(long)(BgL_curz00_6905)==92L)
; } 
if(BgL_test4554z00_7421)
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6898; }  else 
{ 
 long BgL_bufposz00_7431; long BgL_forwardz00_7429; long BgL_lastzd2matchzd2_7428; obj_t BgL_iportz00_7427;
BgL_iportz00_7427 = BgL_iportz00_6894; 
BgL_lastzd2matchzd2_7428 = BgL_newzd2matchzd2_6898; 
BgL_forwardz00_7429 = 
(1L+BgL_forwardz00_6896); 
BgL_bufposz00_7431 = BgL_bufposz00_6897; 
BgL_bufposz00_6870 = BgL_bufposz00_7431; 
BgL_forwardz00_6869 = BgL_forwardz00_7429; 
BgL_lastzd2matchzd2_6868 = BgL_lastzd2matchzd2_7428; 
BgL_iportz00_6867 = BgL_iportz00_7427; 
goto BgL_statezd24zd21109z00_6781;} } } } } } }  else 
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4556z00_7433;
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4557z00_7434;
if(
(
(long)(BgL_curz00_6865)==10L))
{ /* Ieee/input.scm 307 */
BgL_test4557z00_7434 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4557z00_7434 = 
(
(long)(BgL_curz00_6865)==9L)
; } 
if(BgL_test4557z00_7434)
{ /* Ieee/input.scm 307 */
BgL_test4556z00_7433 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4556z00_7433 = 
(
(long)(BgL_curz00_6865)==32L)
; } } 
if(BgL_test4556z00_7433)
{ /* Ieee/input.scm 307 */
BgL_iportz00_6881 = BgL_iportz00_6856; 
BgL_lastzd2matchzd2_6882 = BgL_lastzd2matchzd2_6857; 
BgL_forwardz00_6883 = 
(1L+BgL_forwardz00_6858); 
BgL_bufposz00_6884 = BgL_bufposz00_6859; 
BgL_statezd22zd21107z00_6782:
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6885;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6886;
if(
INPUT_PORTP(BgL_iportz00_6881))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6886 = BgL_iportz00_6881; }  else 
{ 
 obj_t BgL_auxz00_7444;
BgL_auxz00_7444 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4197z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6881); 
FAILURE(BgL_auxz00_7444,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6886, BgL_forwardz00_6883); } 
BgL_newzd2matchzd2_6885 = 0L; 
if(
(BgL_forwardz00_6883==BgL_bufposz00_6884))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4561z00_7451;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6887;
if(
INPUT_PORTP(BgL_iportz00_6881))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6887 = BgL_iportz00_6881; }  else 
{ 
 obj_t BgL_auxz00_7454;
BgL_auxz00_7454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4197z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6881); 
FAILURE(BgL_auxz00_7454,BFALSE,BFALSE);} 
BgL_test4561z00_7451 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6887); } 
if(BgL_test4561z00_7451)
{ /* Ieee/input.scm 307 */
 long BgL_arg1434z00_6888; long BgL_arg1435z00_6889;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6890;
if(
INPUT_PORTP(BgL_iportz00_6881))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6890 = BgL_iportz00_6881; }  else 
{ 
 obj_t BgL_auxz00_7461;
BgL_auxz00_7461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4197z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6881); 
FAILURE(BgL_auxz00_7461,BFALSE,BFALSE);} 
BgL_arg1434z00_6888 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6890); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6891;
if(
INPUT_PORTP(BgL_iportz00_6881))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6891 = BgL_iportz00_6881; }  else 
{ 
 obj_t BgL_auxz00_7468;
BgL_auxz00_7468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4197z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6881); 
FAILURE(BgL_auxz00_7468,BFALSE,BFALSE);} 
BgL_arg1435z00_6889 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6891); } 
{ 
 long BgL_bufposz00_7474; long BgL_forwardz00_7473;
BgL_forwardz00_7473 = BgL_arg1434z00_6888; 
BgL_bufposz00_7474 = BgL_arg1435z00_6889; 
BgL_bufposz00_6884 = BgL_bufposz00_7474; 
BgL_forwardz00_6883 = BgL_forwardz00_7473; 
goto BgL_statezd22zd21107z00_6782;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6885; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6892;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6893;
if(
INPUT_PORTP(BgL_iportz00_6881))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6893 = BgL_iportz00_6881; }  else 
{ 
 obj_t BgL_auxz00_7477;
BgL_auxz00_7477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4197z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6881); 
FAILURE(BgL_auxz00_7477,BFALSE,BFALSE);} 
BgL_curz00_6892 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6893, BgL_forwardz00_6883); } 
{ /* Ieee/input.scm 307 */

{ /* Ieee/input.scm 307 */
 bool_t BgL_test4567z00_7482;
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4568z00_7483;
if(
(
(long)(BgL_curz00_6892)==10L))
{ /* Ieee/input.scm 307 */
BgL_test4568z00_7483 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4568z00_7483 = 
(
(long)(BgL_curz00_6892)==9L)
; } 
if(BgL_test4568z00_7483)
{ /* Ieee/input.scm 307 */
BgL_test4567z00_7482 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4567z00_7482 = 
(
(long)(BgL_curz00_6892)==32L)
; } } 
if(BgL_test4567z00_7482)
{ /* Ieee/input.scm 307 */
BgL_iportz00_6819 = BgL_iportz00_6881; 
BgL_lastzd2matchzd2_6820 = BgL_newzd2matchzd2_6885; 
BgL_forwardz00_6821 = 
(1L+BgL_forwardz00_6883); 
BgL_bufposz00_6822 = BgL_bufposz00_6884; 
BgL_statezd28zd21113z00_6777:
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6823;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6824;
if(
INPUT_PORTP(BgL_iportz00_6819))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6824 = BgL_iportz00_6819; }  else 
{ 
 obj_t BgL_auxz00_7493;
BgL_auxz00_7493 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4192z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6819); 
FAILURE(BgL_auxz00_7493,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6824, BgL_forwardz00_6821); } 
BgL_newzd2matchzd2_6823 = 0L; 
if(
(BgL_forwardz00_6821==BgL_bufposz00_6822))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4572z00_7500;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6825;
if(
INPUT_PORTP(BgL_iportz00_6819))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6825 = BgL_iportz00_6819; }  else 
{ 
 obj_t BgL_auxz00_7503;
BgL_auxz00_7503 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4192z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6819); 
FAILURE(BgL_auxz00_7503,BFALSE,BFALSE);} 
BgL_test4572z00_7500 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6825); } 
if(BgL_test4572z00_7500)
{ /* Ieee/input.scm 307 */
 long BgL_arg1485z00_6826; long BgL_arg1486z00_6827;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6828;
if(
INPUT_PORTP(BgL_iportz00_6819))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6828 = BgL_iportz00_6819; }  else 
{ 
 obj_t BgL_auxz00_7510;
BgL_auxz00_7510 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4192z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6819); 
FAILURE(BgL_auxz00_7510,BFALSE,BFALSE);} 
BgL_arg1485z00_6826 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6828); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6829;
if(
INPUT_PORTP(BgL_iportz00_6819))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6829 = BgL_iportz00_6819; }  else 
{ 
 obj_t BgL_auxz00_7517;
BgL_auxz00_7517 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4192z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6819); 
FAILURE(BgL_auxz00_7517,BFALSE,BFALSE);} 
BgL_arg1486z00_6827 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6829); } 
{ 
 long BgL_bufposz00_7523; long BgL_forwardz00_7522;
BgL_forwardz00_7522 = BgL_arg1485z00_6826; 
BgL_bufposz00_7523 = BgL_arg1486z00_6827; 
BgL_bufposz00_6822 = BgL_bufposz00_7523; 
BgL_forwardz00_6821 = BgL_forwardz00_7522; 
goto BgL_statezd28zd21113z00_6777;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6823; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6830;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6831;
if(
INPUT_PORTP(BgL_iportz00_6819))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6831 = BgL_iportz00_6819; }  else 
{ 
 obj_t BgL_auxz00_7526;
BgL_auxz00_7526 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4192z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6819); 
FAILURE(BgL_auxz00_7526,BFALSE,BFALSE);} 
BgL_curz00_6830 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6831, BgL_forwardz00_6821); } 
{ /* Ieee/input.scm 307 */

{ /* Ieee/input.scm 307 */
 bool_t BgL_test4577z00_7531;
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4578z00_7532;
if(
(
(long)(BgL_curz00_6830)==10L))
{ /* Ieee/input.scm 307 */
BgL_test4578z00_7532 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4578z00_7532 = 
(
(long)(BgL_curz00_6830)==9L)
; } 
if(BgL_test4578z00_7532)
{ /* Ieee/input.scm 307 */
BgL_test4577z00_7531 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4577z00_7531 = 
(
(long)(BgL_curz00_6830)==32L)
; } } 
if(BgL_test4577z00_7531)
{ 
 long BgL_forwardz00_7541; long BgL_lastzd2matchzd2_7540;
BgL_lastzd2matchzd2_7540 = BgL_newzd2matchzd2_6823; 
BgL_forwardz00_7541 = 
(1L+BgL_forwardz00_6821); 
BgL_forwardz00_6821 = BgL_forwardz00_7541; 
BgL_lastzd2matchzd2_6820 = BgL_lastzd2matchzd2_7540; 
goto BgL_statezd28zd21113z00_6777;}  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6823; } } } } } }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6885; } } } } } }  else 
{ /* Ieee/input.scm 307 */
BgL_iportz00_6910 = BgL_iportz00_6856; 
BgL_lastzd2matchzd2_6911 = BgL_lastzd2matchzd2_6857; 
BgL_forwardz00_6912 = 
(1L+BgL_forwardz00_6858); 
BgL_bufposz00_6913 = BgL_bufposz00_6859; 
BgL_statezd21zd21106z00_6784:
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6914;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6915;
if(
INPUT_PORTP(BgL_iportz00_6910))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6915 = BgL_iportz00_6910; }  else 
{ 
 obj_t BgL_auxz00_7547;
BgL_auxz00_7547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4199z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6910); 
FAILURE(BgL_auxz00_7547,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6915, BgL_forwardz00_6912); } 
BgL_newzd2matchzd2_6914 = 3L; 
if(
(BgL_forwardz00_6912==BgL_bufposz00_6913))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4582z00_7554;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6916;
if(
INPUT_PORTP(BgL_iportz00_6910))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6916 = BgL_iportz00_6910; }  else 
{ 
 obj_t BgL_auxz00_7557;
BgL_auxz00_7557 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4199z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6910); 
FAILURE(BgL_auxz00_7557,BFALSE,BFALSE);} 
BgL_test4582z00_7554 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6916); } 
if(BgL_test4582z00_7554)
{ /* Ieee/input.scm 307 */
 long BgL_arg1412z00_6917; long BgL_arg1413z00_6918;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6919;
if(
INPUT_PORTP(BgL_iportz00_6910))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6919 = BgL_iportz00_6910; }  else 
{ 
 obj_t BgL_auxz00_7564;
BgL_auxz00_7564 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4199z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6910); 
FAILURE(BgL_auxz00_7564,BFALSE,BFALSE);} 
BgL_arg1412z00_6917 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6919); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6920;
if(
INPUT_PORTP(BgL_iportz00_6910))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6920 = BgL_iportz00_6910; }  else 
{ 
 obj_t BgL_auxz00_7571;
BgL_auxz00_7571 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4199z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6910); 
FAILURE(BgL_auxz00_7571,BFALSE,BFALSE);} 
BgL_arg1413z00_6918 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6920); } 
{ 
 long BgL_bufposz00_7577; long BgL_forwardz00_7576;
BgL_forwardz00_7576 = BgL_arg1412z00_6917; 
BgL_bufposz00_7577 = BgL_arg1413z00_6918; 
BgL_bufposz00_6913 = BgL_bufposz00_7577; 
BgL_forwardz00_6912 = BgL_forwardz00_7576; 
goto BgL_statezd21zd21106z00_6784;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6914; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6921;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6922;
if(
INPUT_PORTP(BgL_iportz00_6910))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6922 = BgL_iportz00_6910; }  else 
{ 
 obj_t BgL_auxz00_7580;
BgL_auxz00_7580 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4199z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6910); 
FAILURE(BgL_auxz00_7580,BFALSE,BFALSE);} 
BgL_curz00_6921 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6922, BgL_forwardz00_6912); } 
{ /* Ieee/input.scm 307 */

{ /* Ieee/input.scm 307 */
 bool_t BgL_test4587z00_7585;
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4588z00_7586;
if(
(
(long)(BgL_curz00_6921)==10L))
{ /* Ieee/input.scm 307 */
BgL_test4588z00_7586 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4588z00_7586 = 
(
(long)(BgL_curz00_6921)==9L)
; } 
if(BgL_test4588z00_7586)
{ /* Ieee/input.scm 307 */
BgL_test4587z00_7585 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
if(
(
(long)(BgL_curz00_6921)==32L))
{ /* Ieee/input.scm 307 */
BgL_test4587z00_7585 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4587z00_7585 = 
(
(long)(BgL_curz00_6921)==34L)
; } } } 
if(BgL_test4587z00_7585)
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6914; }  else 
{ /* Ieee/input.scm 307 */
BgL_iportz00_6843 = BgL_iportz00_6910; 
BgL_lastzd2matchzd2_6844 = BgL_newzd2matchzd2_6914; 
BgL_forwardz00_6845 = 
(1L+BgL_forwardz00_6912); 
BgL_bufposz00_6846 = BgL_bufposz00_6913; 
BgL_statezd29zd21114z00_6779:
{ /* Ieee/input.scm 307 */
 long BgL_newzd2matchzd2_6847;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6848;
if(
INPUT_PORTP(BgL_iportz00_6843))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6848 = BgL_iportz00_6843; }  else 
{ 
 obj_t BgL_auxz00_7599;
BgL_auxz00_7599 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4194z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6843); 
FAILURE(BgL_auxz00_7599,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_6848, BgL_forwardz00_6845); } 
BgL_newzd2matchzd2_6847 = 3L; 
if(
(BgL_forwardz00_6845==BgL_bufposz00_6846))
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4593z00_7606;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6849;
if(
INPUT_PORTP(BgL_iportz00_6843))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6849 = BgL_iportz00_6843; }  else 
{ 
 obj_t BgL_auxz00_7609;
BgL_auxz00_7609 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4194z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6843); 
FAILURE(BgL_auxz00_7609,BFALSE,BFALSE);} 
BgL_test4593z00_7606 = 
rgc_fill_buffer(BgL_inputzd2portzd2_6849); } 
if(BgL_test4593z00_7606)
{ /* Ieee/input.scm 307 */
 long BgL_arg1467z00_6850; long BgL_arg1468z00_6851;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6852;
if(
INPUT_PORTP(BgL_iportz00_6843))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6852 = BgL_iportz00_6843; }  else 
{ 
 obj_t BgL_auxz00_7616;
BgL_auxz00_7616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4194z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6843); 
FAILURE(BgL_auxz00_7616,BFALSE,BFALSE);} 
BgL_arg1467z00_6850 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_6852); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6853;
if(
INPUT_PORTP(BgL_iportz00_6843))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6853 = BgL_iportz00_6843; }  else 
{ 
 obj_t BgL_auxz00_7623;
BgL_auxz00_7623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4194z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6843); 
FAILURE(BgL_auxz00_7623,BFALSE,BFALSE);} 
BgL_arg1468z00_6851 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_6853); } 
{ 
 long BgL_bufposz00_7629; long BgL_forwardz00_7628;
BgL_forwardz00_7628 = BgL_arg1467z00_6850; 
BgL_bufposz00_7629 = BgL_arg1468z00_6851; 
BgL_bufposz00_6846 = BgL_bufposz00_7629; 
BgL_forwardz00_6845 = BgL_forwardz00_7628; 
goto BgL_statezd29zd21114z00_6779;} }  else 
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6847; } }  else 
{ /* Ieee/input.scm 307 */
 int BgL_curz00_6854;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6855;
if(
INPUT_PORTP(BgL_iportz00_6843))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6855 = BgL_iportz00_6843; }  else 
{ 
 obj_t BgL_auxz00_7632;
BgL_auxz00_7632 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4194z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_6843); 
FAILURE(BgL_auxz00_7632,BFALSE,BFALSE);} 
BgL_curz00_6854 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_6855, BgL_forwardz00_6845); } 
{ /* Ieee/input.scm 307 */

{ /* Ieee/input.scm 307 */
 bool_t BgL_test4598z00_7637;
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4599z00_7638;
if(
(
(long)(BgL_curz00_6854)==10L))
{ /* Ieee/input.scm 307 */
BgL_test4599z00_7638 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4599z00_7638 = 
(
(long)(BgL_curz00_6854)==9L)
; } 
if(BgL_test4599z00_7638)
{ /* Ieee/input.scm 307 */
BgL_test4598z00_7637 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
if(
(
(long)(BgL_curz00_6854)==32L))
{ /* Ieee/input.scm 307 */
BgL_test4598z00_7637 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 307 */
BgL_test4598z00_7637 = 
(
(long)(BgL_curz00_6854)==34L)
; } } } 
if(BgL_test4598z00_7637)
{ /* Ieee/input.scm 307 */
BgL_matchz00_6786 = BgL_newzd2matchzd2_6847; }  else 
{ 
 long BgL_forwardz00_7650; long BgL_lastzd2matchzd2_7649;
BgL_lastzd2matchzd2_7649 = BgL_newzd2matchzd2_6847; 
BgL_forwardz00_7650 = 
(1L+BgL_forwardz00_6845); 
BgL_forwardz00_6845 = BgL_forwardz00_7650; 
BgL_lastzd2matchzd2_6844 = BgL_lastzd2matchzd2_7649; 
goto BgL_statezd29zd21114z00_6779;} } } } } } } } } } } } } } } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6791;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6791 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7656;
BgL_auxz00_7656 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7656,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_6791); } 
switch( BgL_matchz00_6786) { case 4L : 

{ /* Ieee/input.scm 307 */
 bool_t BgL_test4603z00_7661;
{ /* Ieee/input.scm 307 */
 long BgL_arg1586z00_6798;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6799;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6799 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7664;
BgL_auxz00_7664 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7664,BFALSE,BFALSE);} 
BgL_arg1586z00_6798 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_6799); } 
BgL_test4603z00_7661 = 
(BgL_arg1586z00_6798==0L); } 
if(BgL_test4603z00_7661)
{ /* Ieee/input.scm 307 */
return BEOF;}  else 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6800;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6800 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7672;
BgL_auxz00_7672 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7672,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_6800));} } break;case 3L : 

{ /* Ieee/input.scm 307 */
 long BgL_arg1497z00_6792;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6793;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6793 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7680;
BgL_auxz00_7680 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7680,BFALSE,BFALSE);} 
BgL_arg1497z00_6792 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_6793); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6794;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6794 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7687;
BgL_auxz00_7687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7687,BFALSE,BFALSE);} 
return 
rgc_buffer_substring(BgL_inputzd2portzd2_6794, 0L, BgL_arg1497z00_6792);} } break;case 2L : 

BgL_minz00_6801 = 
(int)(1L); 
BgL_maxz00_6802 = 
(int)(-1L); 
if(
(
(long)(BgL_maxz00_6802)<
(long)(BgL_minz00_6801)))
{ /* Ieee/input.scm 307 */
 long BgL_arg1500z00_6803;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6804;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6804 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7698;
BgL_auxz00_7698 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7698,BFALSE,BFALSE);} 
BgL_arg1500z00_6803 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_6804); } 
{ /* Ieee/input.scm 307 */
 long BgL_za72za7_6805;
BgL_za72za7_6805 = 
(long)(BgL_maxz00_6802); 
BgL_maxz00_6802 = 
(int)(
(BgL_arg1500z00_6803+BgL_za72za7_6805)); } }  else 
{ /* Ieee/input.scm 307 */BFALSE; } 
{ /* Ieee/input.scm 307 */
 bool_t BgL_test4610z00_7706;
if(
(
(long)(BgL_minz00_6801)>=0L))
{ /* Ieee/input.scm 307 */
if(
(
(long)(BgL_maxz00_6802)>=
(long)(BgL_minz00_6801)))
{ /* Ieee/input.scm 307 */
 long BgL_arg1509z00_6806;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6807;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6807 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7716;
BgL_auxz00_7716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7716,BFALSE,BFALSE);} 
BgL_arg1509z00_6806 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_6807); } 
BgL_test4610z00_7706 = 
(
(long)(BgL_maxz00_6802)<=BgL_arg1509z00_6806); }  else 
{ /* Ieee/input.scm 307 */
BgL_test4610z00_7706 = ((bool_t)0)
; } }  else 
{ /* Ieee/input.scm 307 */
BgL_test4610z00_7706 = ((bool_t)0)
; } 
if(BgL_test4610z00_7706)
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6808; long BgL_startz00_6809; long BgL_stopz00_6810;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6808 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7725;
BgL_auxz00_7725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7725,BFALSE,BFALSE);} 
BgL_startz00_6809 = 
(long)(BgL_minz00_6801); 
BgL_stopz00_6810 = 
(long)(BgL_maxz00_6802); 
return 
rgc_buffer_substring(BgL_inputzd2portzd2_6808, BgL_startz00_6809, BgL_stopz00_6810);}  else 
{ /* Ieee/input.scm 307 */
 obj_t BgL_arg1505z00_6811; obj_t BgL_arg1506z00_6812;
{ /* Ieee/input.scm 307 */
 obj_t BgL_arg1507z00_6813;
{ /* Ieee/input.scm 307 */
 long BgL_arg1497z00_6814;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6815;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6815 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7734;
BgL_auxz00_7734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7734,BFALSE,BFALSE);} 
BgL_arg1497z00_6814 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_6815); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6816;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6816 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7741;
BgL_auxz00_7741 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7741,BFALSE,BFALSE);} 
BgL_arg1507z00_6813 = 
rgc_buffer_substring(BgL_inputzd2portzd2_6816, 0L, BgL_arg1497z00_6814); } } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_list1508z00_6817;
BgL_list1508z00_6817 = 
MAKE_YOUNG_PAIR(BgL_arg1507z00_6813, BNIL); 
BgL_arg1505z00_6811 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string4190z00zz__r4_input_6_10_2z00, BgL_list1508z00_6817); } } 
BgL_arg1506z00_6812 = 
MAKE_YOUNG_PAIR(
BINT(BgL_minz00_6801), 
BINT(BgL_maxz00_6802)); 
{ /* Ieee/input.scm 307 */
 obj_t BgL_aux3538z00_6818;
BgL_aux3538z00_6818 = 
BGl_errorz00zz__errorz00(BGl_string4189z00zz__r4_input_6_10_2z00, BgL_arg1505z00_6811, BgL_arg1506z00_6812); 
if(
STRINGP(BgL_aux3538z00_6818))
{ /* Ieee/input.scm 307 */
return BgL_aux3538z00_6818;}  else 
{ 
 obj_t BgL_auxz00_7754;
BgL_auxz00_7754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_aux3538z00_6818); 
FAILURE(BgL_auxz00_7754,BFALSE,BFALSE);} } } } break;case 1L : 

{ /* Ieee/input.scm 307 */
 long BgL_arg1497z00_6795;
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6796;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6796 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7762;
BgL_auxz00_7762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7762,BFALSE,BFALSE);} 
BgL_arg1497z00_6795 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_6796); } 
{ /* Ieee/input.scm 307 */
 obj_t BgL_inputzd2portzd2_6797;
if(
INPUT_PORTP(BgL_iportz00_5989))
{ /* Ieee/input.scm 307 */
BgL_inputzd2portzd2_6797 = BgL_iportz00_5989; }  else 
{ 
 obj_t BgL_auxz00_7769;
BgL_auxz00_7769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(12240L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_5989); 
FAILURE(BgL_auxz00_7769,BFALSE,BFALSE);} 
return 
rgc_buffer_substring(BgL_inputzd2portzd2_6797, 0L, BgL_arg1497z00_6795);} } break;case 0L : 

goto BgL_ignorez00_6774;break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_6786));} } } } } } 

}



/* read/rp */
BGL_EXPORTED_DEF obj_t BGl_readzf2rpzf2zz__r4_input_6_10_2z00(obj_t BgL_grammarz00_3, obj_t BgL_portz00_4, obj_t BgL_optsz00_5)
{
{ /* Ieee/input.scm 106 */
if(
PAIRP(BgL_optsz00_5))
{ /* Ieee/input.scm 109 */
 obj_t BgL_valz00_6173;
{ /* Ieee/input.scm 109 */
 obj_t BgL_list1603z00_1615;
BgL_list1603z00_1615 = 
MAKE_YOUNG_PAIR(BgL_optsz00_5, BNIL); 
BgL_valz00_6173 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_portz00_4, BgL_list1603z00_1615); } 
{ /* Ieee/input.scm 109 */
 int BgL_len3624z00_6174;
BgL_len3624z00_6174 = 
(int)(
bgl_list_length(BgL_valz00_6173)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, BgL_len3624z00_6174))
{ /* Ieee/input.scm 109 */
return 
apply(BgL_grammarz00_3, BgL_valz00_6173);}  else 
{ /* Ieee/input.scm 109 */
FAILURE(BGl_symbol4203z00zz__r4_input_6_10_2z00,BGl_string4205z00zz__r4_input_6_10_2z00,BGl_list4206z00zz__r4_input_6_10_2z00);} } }  else 
{ /* Ieee/input.scm 108 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, 
(int)(1L)))
{ /* Ieee/input.scm 110 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, 1))
{ /* Ieee/input.scm 111 */
return 
BGL_PROCEDURE_CALL1(BgL_grammarz00_3, BgL_portz00_4);}  else 
{ /* Ieee/input.scm 111 */
FAILURE(BGl_string4231z00zz__r4_input_6_10_2z00,BGl_list4232z00zz__r4_input_6_10_2z00,BgL_grammarz00_3);} }  else 
{ /* Ieee/input.scm 110 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, 
(int)(2L)))
{ /* Ieee/input.scm 112 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_grammarz00_3, 2))
{ /* Ieee/input.scm 113 */
return 
BGL_PROCEDURE_CALL2(BgL_grammarz00_3, BgL_portz00_4, BUNSPEC);}  else 
{ /* Ieee/input.scm 113 */
FAILURE(BGl_string4231z00zz__r4_input_6_10_2z00,BGl_list4235z00zz__r4_input_6_10_2z00,BgL_grammarz00_3);} }  else 
{ /* Ieee/input.scm 112 */
return 
BGl_errorz00zz__errorz00(BGl_symbol4203z00zz__r4_input_6_10_2z00, BGl_string4236z00zz__r4_input_6_10_2z00, BgL_grammarz00_3);} } } } 

}



/* &read/rp */
obj_t BGl_z62readzf2rpz90zz__r4_input_6_10_2z00(obj_t BgL_envz00_5990, obj_t BgL_grammarz00_5991, obj_t BgL_portz00_5992, obj_t BgL_optsz00_5993)
{
{ /* Ieee/input.scm 106 */
{ /* Ieee/input.scm 108 */
 obj_t BgL_auxz00_7817; obj_t BgL_auxz00_7810;
if(
INPUT_PORTP(BgL_portz00_5992))
{ /* Ieee/input.scm 108 */
BgL_auxz00_7817 = BgL_portz00_5992
; }  else 
{ 
 obj_t BgL_auxz00_7820;
BgL_auxz00_7820 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(4665L), BGl_string4237z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_portz00_5992); 
FAILURE(BgL_auxz00_7820,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_grammarz00_5991))
{ /* Ieee/input.scm 108 */
BgL_auxz00_7810 = BgL_grammarz00_5991
; }  else 
{ 
 obj_t BgL_auxz00_7813;
BgL_auxz00_7813 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(4665L), BGl_string4237z00zz__r4_input_6_10_2z00, BGl_string4238z00zz__r4_input_6_10_2z00, BgL_grammarz00_5991); 
FAILURE(BgL_auxz00_7813,BFALSE,BFALSE);} 
return 
BGl_readzf2rpzf2zz__r4_input_6_10_2z00(BgL_auxz00_7810, BgL_auxz00_7817, BgL_optsz00_5993);} } 

}



/* read/lalrp */
BGL_EXPORTED_DEF obj_t BGl_readzf2lalrpzf2zz__r4_input_6_10_2z00(obj_t BgL_lalrz00_6, obj_t BgL_rgcz00_7, obj_t BgL_portz00_8, obj_t BgL_eofzd2funzf3z21_9)
{
{ /* Ieee/input.scm 120 */
if(
NULLP(BgL_eofzd2funzf3z21_9))
{ /* Ieee/input.scm 121 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_lalrz00_6, 3))
{ /* Ieee/input.scm 122 */
return 
BGL_PROCEDURE_CALL3(BgL_lalrz00_6, BgL_rgcz00_7, BgL_portz00_8, BGl_eofzd2objectzf3zd2envzf3zz__r4_input_6_10_2z00);}  else 
{ /* Ieee/input.scm 122 */
FAILURE(BGl_string4239z00zz__r4_input_6_10_2z00,BGl_list4240z00zz__r4_input_6_10_2z00,BgL_lalrz00_6);} }  else 
{ /* Ieee/input.scm 123 */
 obj_t BgL_arg1605z00_4806;
{ /* Ieee/input.scm 123 */
 obj_t BgL_pairz00_4807;
if(
PAIRP(BgL_eofzd2funzf3z21_9))
{ /* Ieee/input.scm 123 */
BgL_pairz00_4807 = BgL_eofzd2funzf3z21_9; }  else 
{ 
 obj_t BgL_auxz00_7838;
BgL_auxz00_7838 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5267L), BGl_string4247z00zz__r4_input_6_10_2z00, BGl_string4248z00zz__r4_input_6_10_2z00, BgL_eofzd2funzf3z21_9); 
FAILURE(BgL_auxz00_7838,BFALSE,BFALSE);} 
BgL_arg1605z00_4806 = 
CAR(BgL_pairz00_4807); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_lalrz00_6, 3))
{ /* Ieee/input.scm 123 */
return 
BGL_PROCEDURE_CALL3(BgL_lalrz00_6, BgL_rgcz00_7, BgL_portz00_8, BgL_arg1605z00_4806);}  else 
{ /* Ieee/input.scm 123 */
FAILURE(BGl_string4239z00zz__r4_input_6_10_2z00,BGl_list4249z00zz__r4_input_6_10_2z00,BgL_lalrz00_6);} } } 

}



/* &read/lalrp */
obj_t BGl_z62readzf2lalrpz90zz__r4_input_6_10_2z00(obj_t BgL_envz00_5994, obj_t BgL_lalrz00_5995, obj_t BgL_rgcz00_5996, obj_t BgL_portz00_5997, obj_t BgL_eofzd2funzf3z21_5998)
{
{ /* Ieee/input.scm 120 */
{ /* Ieee/input.scm 121 */
 obj_t BgL_auxz00_7866; obj_t BgL_auxz00_7859; obj_t BgL_auxz00_7852;
if(
INPUT_PORTP(BgL_portz00_5997))
{ /* Ieee/input.scm 121 */
BgL_auxz00_7866 = BgL_portz00_5997
; }  else 
{ 
 obj_t BgL_auxz00_7869;
BgL_auxz00_7869 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5184L), BGl_string4252z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_portz00_5997); 
FAILURE(BgL_auxz00_7869,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_rgcz00_5996))
{ /* Ieee/input.scm 121 */
BgL_auxz00_7859 = BgL_rgcz00_5996
; }  else 
{ 
 obj_t BgL_auxz00_7862;
BgL_auxz00_7862 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5184L), BGl_string4252z00zz__r4_input_6_10_2z00, BGl_string4238z00zz__r4_input_6_10_2z00, BgL_rgcz00_5996); 
FAILURE(BgL_auxz00_7862,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_lalrz00_5995))
{ /* Ieee/input.scm 121 */
BgL_auxz00_7852 = BgL_lalrz00_5995
; }  else 
{ 
 obj_t BgL_auxz00_7855;
BgL_auxz00_7855 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5184L), BGl_string4252z00zz__r4_input_6_10_2z00, BGl_string4238z00zz__r4_input_6_10_2z00, BgL_lalrz00_5995); 
FAILURE(BgL_auxz00_7855,BFALSE,BFALSE);} 
return 
BGl_readzf2lalrpzf2zz__r4_input_6_10_2z00(BgL_auxz00_7852, BgL_auxz00_7859, BgL_auxz00_7866, BgL_eofzd2funzf3z21_5998);} } 

}



/* _read-char */
obj_t BGl__readzd2charzd2zz__r4_input_6_10_2z00(obj_t BgL_env1247z00_12, obj_t BgL_opt1246z00_11)
{
{ /* Ieee/input.scm 128 */
{ /* Ieee/input.scm 128 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1246z00_11)) { case 0L : 

{ /* Ieee/input.scm 128 */
 obj_t BgL_ipz00_1620;
{ /* Ieee/input.scm 128 */
 obj_t BgL_tmpz00_7874;
BgL_tmpz00_7874 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_1620 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7874); } 
{ /* Ieee/input.scm 128 */

return 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1620);} } break;case 1L : 

{ /* Ieee/input.scm 128 */
 obj_t BgL_ipz00_1621;
BgL_ipz00_1621 = 
VECTOR_REF(BgL_opt1246z00_11,0L); 
{ /* Ieee/input.scm 128 */

return 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1621);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4253z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1246z00_11)));} } } } 

}



/* read-char */
BGL_EXPORTED_DEF obj_t BGl_readzd2charzd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_10)
{
{ /* Ieee/input.scm 128 */
{ 
 obj_t BgL_iportz00_1625;
BgL_iportz00_1625 = BgL_ipz00_10; 
{ 

{ /* Ieee/input.scm 129 */
 bool_t BgL_test4635z00_7885;
{ /* Ieee/input.scm 129 */
 obj_t BgL_arg1612z00_1663;
{ /* Ieee/input.scm 129 */
 obj_t BgL_res3456z00_4852;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_res3456z00_4852 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7888;
BgL_auxz00_7888 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7888,BFALSE,BFALSE);} 
BgL_arg1612z00_1663 = BgL_res3456z00_4852; } 
BgL_test4635z00_7885 = 
INPUT_PORT_CLOSEP(BgL_arg1612z00_1663); } 
if(BgL_test4635z00_7885)
{ /* Ieee/input.scm 129 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1608z00_1657;
{ /* Ieee/input.scm 129 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_1658;
{ /* Ieee/input.scm 129 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_1661;
BgL_new1045z00_1661 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 129 */
 long BgL_arg1611z00_1662;
BgL_arg1611z00_1662 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_1661), BgL_arg1611z00_1662); } 
BgL_new1046z00_1658 = BgL_new1045z00_1661; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_1658)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_1658)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_7901;
{ /* Ieee/input.scm 129 */
 obj_t BgL_arg1609z00_1659;
{ /* Ieee/input.scm 129 */
 obj_t BgL_arg1610z00_1660;
{ /* Ieee/input.scm 129 */
 obj_t BgL_classz00_4857;
BgL_classz00_4857 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1610z00_1660 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_4857); } 
BgL_arg1609z00_1659 = 
VECTOR_REF(BgL_arg1610z00_1660,2L); } 
{ /* Ieee/input.scm 129 */
 obj_t BgL_auxz00_7905;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1609z00_1659))
{ /* Ieee/input.scm 129 */
BgL_auxz00_7905 = BgL_arg1609z00_1659
; }  else 
{ 
 obj_t BgL_auxz00_7908;
BgL_auxz00_7908 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg1609z00_1659); 
FAILURE(BgL_auxz00_7908,BFALSE,BFALSE);} 
BgL_auxz00_7901 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_7905); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_1658)))->BgL_stackz00)=((obj_t)BgL_auxz00_7901),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_1658)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_1658)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_7918;
{ /* Ieee/input.scm 129 */
 obj_t BgL_res3457z00_4859;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_res3457z00_4859 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7922;
BgL_auxz00_7922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7922,BFALSE,BFALSE);} 
BgL_auxz00_7918 = BgL_res3457z00_4859; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_1658)))->BgL_objz00)=((obj_t)BgL_auxz00_7918),BUNSPEC); } 
BgL_arg1608z00_1657 = BgL_new1046z00_1658; } 
BGL_TAIL return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1608z00_1657));}  else 
{ /* Ieee/input.scm 129 */
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4827;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4827 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7931;
BgL_auxz00_7931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7931,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_4827); } 
{ /* Ieee/input.scm 129 */
 long BgL_matchz00_1800;
{ /* Ieee/input.scm 129 */
 long BgL_arg1729z00_1803; long BgL_arg1730z00_1804;
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4828;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4828 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7938;
BgL_auxz00_7938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7938,BFALSE,BFALSE);} 
BgL_arg1729z00_1803 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4828); } 
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4829;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4829 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7945;
BgL_auxz00_7945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7945,BFALSE,BFALSE);} 
BgL_arg1730z00_1804 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4829); } 
{ 
 long BgL_forwardz00_4831; long BgL_bufposz00_4832;
BgL_forwardz00_4831 = BgL_arg1729z00_1803; 
BgL_bufposz00_4832 = BgL_arg1730z00_1804; 
BgL_statezd20zd21040z00_4830:
if(
(BgL_forwardz00_4831==BgL_bufposz00_4832))
{ /* Ieee/input.scm 129 */
 bool_t BgL_test4643z00_7952;
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4837;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4837 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7955;
BgL_auxz00_7955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4256z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7955,BFALSE,BFALSE);} 
BgL_test4643z00_7952 = 
rgc_fill_buffer(BgL_inputzd2portzd2_4837); } 
if(BgL_test4643z00_7952)
{ /* Ieee/input.scm 129 */
 long BgL_arg1617z00_4838; long BgL_arg1618z00_4839;
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4840;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4840 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7962;
BgL_auxz00_7962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4256z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7962,BFALSE,BFALSE);} 
BgL_arg1617z00_4838 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4840); } 
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4841;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4841 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7969;
BgL_auxz00_7969 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4256z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7969,BFALSE,BFALSE);} 
BgL_arg1618z00_4839 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4841); } 
{ 
 long BgL_bufposz00_7975; long BgL_forwardz00_7974;
BgL_forwardz00_7974 = BgL_arg1617z00_4838; 
BgL_bufposz00_7975 = BgL_arg1618z00_4839; 
BgL_bufposz00_4832 = BgL_bufposz00_7975; 
BgL_forwardz00_4831 = BgL_forwardz00_7974; 
goto BgL_statezd20zd21040z00_4830;} }  else 
{ /* Ieee/input.scm 129 */
BgL_matchz00_1800 = 1L; } }  else 
{ /* Ieee/input.scm 129 */
 int BgL_curz00_4842;
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4843;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4843 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7978;
BgL_auxz00_7978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4256z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7978,BFALSE,BFALSE);} 
BgL_curz00_4842 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_4843, BgL_forwardz00_4831); } 
{ /* Ieee/input.scm 129 */

{ /* Ieee/input.scm 129 */
 long BgL_arg1619z00_4845;
BgL_arg1619z00_4845 = 
(1L+BgL_forwardz00_4831); 
{ /* Ieee/input.scm 129 */
 long BgL_newzd2matchzd2_4847;
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4848;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4848 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7986;
BgL_auxz00_7986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4256z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7986,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_4848, BgL_arg1619z00_4845); } 
BgL_newzd2matchzd2_4847 = 0L; 
BgL_matchz00_1800 = BgL_newzd2matchzd2_4847; } } } } } } 
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4850;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4850 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_7993;
BgL_auxz00_7993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_7993,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_4850); } 
switch( BgL_matchz00_1800) { case 1L : 

{ /* Ieee/input.scm 129 */
 bool_t BgL_test4650z00_7998;
{ /* Ieee/input.scm 129 */
 long BgL_arg1722z00_1790;
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4824;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4824 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_8001;
BgL_auxz00_8001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_8001,BFALSE,BFALSE);} 
BgL_arg1722z00_1790 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_4824); } 
BgL_test4650z00_7998 = 
(BgL_arg1722z00_1790==0L); } 
if(BgL_test4650z00_7998)
{ /* Ieee/input.scm 129 */
return BEOF;}  else 
{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4826;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4826 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_8009;
BgL_auxz00_8009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_8009,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_4826));} } break;case 0L : 

{ /* Ieee/input.scm 129 */
 obj_t BgL_inputzd2portzd2_4851;
if(
INPUT_PORTP(BgL_iportz00_1625))
{ /* Ieee/input.scm 129 */
BgL_inputzd2portzd2_4851 = BgL_iportz00_1625; }  else 
{ 
 obj_t BgL_auxz00_8017;
BgL_auxz00_8017 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5578L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1625); 
FAILURE(BgL_auxz00_8017,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_4851));} break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_1800));} } } } } } } 

}



/* _peek-char */
obj_t BGl__peekzd2charzd2zz__r4_input_6_10_2z00(obj_t BgL_env1251z00_15, obj_t BgL_opt1250z00_14)
{
{ /* Ieee/input.scm 136 */
{ /* Ieee/input.scm 136 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1250z00_14)) { case 0L : 

{ /* Ieee/input.scm 136 */
 obj_t BgL_ipz00_1837;
{ /* Ieee/input.scm 136 */
 obj_t BgL_tmpz00_8026;
BgL_tmpz00_8026 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_1837 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8026); } 
{ /* Ieee/input.scm 136 */

return 
BGl_peekzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1837);} } break;case 1L : 

{ /* Ieee/input.scm 136 */
 obj_t BgL_ipz00_1838;
BgL_ipz00_1838 = 
VECTOR_REF(BgL_opt1250z00_14,0L); 
{ /* Ieee/input.scm 136 */

return 
BGl_peekzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_1838);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4257z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1250z00_14)));} } } } 

}



/* peek-char */
BGL_EXPORTED_DEF obj_t BGl_peekzd2charzd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_13)
{
{ /* Ieee/input.scm 136 */
{ 
 obj_t BgL_iportz00_1842;
BgL_iportz00_1842 = BgL_ipz00_13; 
{ 

{ /* Ieee/input.scm 137 */
 bool_t BgL_test4654z00_8037;
{ /* Ieee/input.scm 137 */
 obj_t BgL_arg1737z00_1880;
{ /* Ieee/input.scm 137 */
 obj_t BgL_res3459z00_4908;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_res3459z00_4908 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8040;
BgL_auxz00_8040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8040,BFALSE,BFALSE);} 
BgL_arg1737z00_1880 = BgL_res3459z00_4908; } 
BgL_test4654z00_8037 = 
INPUT_PORT_CLOSEP(BgL_arg1737z00_1880); } 
if(BgL_test4654z00_8037)
{ /* Ieee/input.scm 137 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1733z00_1874;
{ /* Ieee/input.scm 137 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_1875;
{ /* Ieee/input.scm 137 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_1878;
BgL_new1045z00_1878 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 137 */
 long BgL_arg1736z00_1879;
BgL_arg1736z00_1879 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_1878), BgL_arg1736z00_1879); } 
BgL_new1046z00_1875 = BgL_new1045z00_1878; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_1875)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_1875)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8053;
{ /* Ieee/input.scm 137 */
 obj_t BgL_arg1734z00_1876;
{ /* Ieee/input.scm 137 */
 obj_t BgL_arg1735z00_1877;
{ /* Ieee/input.scm 137 */
 obj_t BgL_classz00_4913;
BgL_classz00_4913 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1735z00_1877 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_4913); } 
BgL_arg1734z00_1876 = 
VECTOR_REF(BgL_arg1735z00_1877,2L); } 
{ /* Ieee/input.scm 137 */
 obj_t BgL_auxz00_8057;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1734z00_1876))
{ /* Ieee/input.scm 137 */
BgL_auxz00_8057 = BgL_arg1734z00_1876
; }  else 
{ 
 obj_t BgL_auxz00_8060;
BgL_auxz00_8060 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg1734z00_1876); 
FAILURE(BgL_auxz00_8060,BFALSE,BFALSE);} 
BgL_auxz00_8053 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_8057); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_1875)))->BgL_stackz00)=((obj_t)BgL_auxz00_8053),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_1875)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_1875)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8070;
{ /* Ieee/input.scm 137 */
 obj_t BgL_res3460z00_4915;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_res3460z00_4915 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8074;
BgL_auxz00_8074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8074,BFALSE,BFALSE);} 
BgL_auxz00_8070 = BgL_res3460z00_4915; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_1875)))->BgL_objz00)=((obj_t)BgL_auxz00_8070),BUNSPEC); } 
BgL_arg1733z00_1874 = BgL_new1046z00_1875; } 
BGL_TAIL return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1733z00_1874));}  else 
{ /* Ieee/input.scm 137 */
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4879;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4879 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8083;
BgL_auxz00_8083 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8083,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_4879); } 
{ /* Ieee/input.scm 137 */
 long BgL_matchz00_2017;
{ /* Ieee/input.scm 137 */
 long BgL_arg1837z00_2023; long BgL_arg1838z00_2024;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4880;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4880 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8090;
BgL_auxz00_8090 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8090,BFALSE,BFALSE);} 
BgL_arg1837z00_2023 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4880); } 
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4881;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4881 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8097;
BgL_auxz00_8097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8097,BFALSE,BFALSE);} 
BgL_arg1838z00_2024 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4881); } 
{ 
 long BgL_forwardz00_4883; long BgL_bufposz00_4884;
BgL_forwardz00_4883 = BgL_arg1837z00_2023; 
BgL_bufposz00_4884 = BgL_arg1838z00_2024; 
BgL_statezd20zd21048z00_4882:
if(
(BgL_forwardz00_4883==BgL_bufposz00_4884))
{ /* Ieee/input.scm 137 */
 bool_t BgL_test4662z00_8104;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4889;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4889 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8107;
BgL_auxz00_8107 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4259z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8107,BFALSE,BFALSE);} 
BgL_test4662z00_8104 = 
rgc_fill_buffer(BgL_inputzd2portzd2_4889); } 
if(BgL_test4662z00_8104)
{ /* Ieee/input.scm 137 */
 long BgL_arg1743z00_4890; long BgL_arg1744z00_4891;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4892;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4892 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8114;
BgL_auxz00_8114 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4259z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8114,BFALSE,BFALSE);} 
BgL_arg1743z00_4890 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4892); } 
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4893;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4893 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8121;
BgL_auxz00_8121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4259z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8121,BFALSE,BFALSE);} 
BgL_arg1744z00_4891 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4893); } 
{ 
 long BgL_bufposz00_8127; long BgL_forwardz00_8126;
BgL_forwardz00_8126 = BgL_arg1743z00_4890; 
BgL_bufposz00_8127 = BgL_arg1744z00_4891; 
BgL_bufposz00_4884 = BgL_bufposz00_8127; 
BgL_forwardz00_4883 = BgL_forwardz00_8126; 
goto BgL_statezd20zd21048z00_4882;} }  else 
{ /* Ieee/input.scm 137 */
BgL_matchz00_2017 = 1L; } }  else 
{ /* Ieee/input.scm 137 */
 int BgL_curz00_4894;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4895;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4895 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8130;
BgL_auxz00_8130 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4259z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8130,BFALSE,BFALSE);} 
BgL_curz00_4894 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_4895, BgL_forwardz00_4883); } 
{ /* Ieee/input.scm 137 */

{ /* Ieee/input.scm 137 */
 long BgL_arg1745z00_4897;
BgL_arg1745z00_4897 = 
(1L+BgL_forwardz00_4883); 
{ /* Ieee/input.scm 137 */
 long BgL_newzd2matchzd2_4899;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4900;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4900 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8138;
BgL_auxz00_8138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4259z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8138,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_4900, BgL_arg1745z00_4897); } 
BgL_newzd2matchzd2_4899 = 0L; 
BgL_matchz00_2017 = BgL_newzd2matchzd2_4899; } } } } } } 
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4902;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4902 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8145;
BgL_auxz00_8145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8145,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_4902); } 
switch( BgL_matchz00_2017) { case 1L : 

{ /* Ieee/input.scm 137 */
 bool_t BgL_test4669z00_8150;
{ /* Ieee/input.scm 137 */
 long BgL_arg1828z00_2007;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4876;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4876 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8153;
BgL_auxz00_8153 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8153,BFALSE,BFALSE);} 
BgL_arg1828z00_2007 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_4876); } 
BgL_test4669z00_8150 = 
(BgL_arg1828z00_2007==0L); } 
if(BgL_test4669z00_8150)
{ /* Ieee/input.scm 137 */
return BEOF;}  else 
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4878;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4878 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8161;
BgL_auxz00_8161 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8161,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_4878));} } break;case 0L : 

{ /* Ieee/input.scm 139 */
 unsigned char BgL_cz00_2020;
{ /* Ieee/input.scm 137 */
 obj_t BgL_inputzd2portzd2_4903;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_inputzd2portzd2_4903 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8169;
BgL_auxz00_8169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8169,BFALSE,BFALSE);} 
BgL_cz00_2020 = 
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_4903); } 
{ /* Ieee/input.scm 140 */
 obj_t BgL_arg1835z00_2021; long BgL_arg1836z00_2022;
{ /* Ieee/input.scm 140 */
 obj_t BgL_res3458z00_4904;
if(
INPUT_PORTP(BgL_iportz00_1842))
{ /* Ieee/input.scm 137 */
BgL_res3458z00_4904 = BgL_iportz00_1842; }  else 
{ 
 obj_t BgL_auxz00_8176;
BgL_auxz00_8176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(5972L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_1842); 
FAILURE(BgL_auxz00_8176,BFALSE,BFALSE);} 
BgL_arg1835z00_2021 = BgL_res3458z00_4904; } 
BgL_arg1836z00_2022 = 
(
(unsigned char)(BgL_cz00_2020)); 
rgc_buffer_unget_char(BgL_arg1835z00_2021, 
(int)(BgL_arg1836z00_2022)); } 
return 
BCHAR(BgL_cz00_2020);} break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_2017));} } } } } } } 

}



/* _read-byte */
obj_t BGl__readzd2bytezd2zz__r4_input_6_10_2z00(obj_t BgL_env1255z00_18, obj_t BgL_opt1254z00_17)
{
{ /* Ieee/input.scm 147 */
{ /* Ieee/input.scm 147 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1254z00_17)) { case 0L : 

{ /* Ieee/input.scm 147 */
 obj_t BgL_ipz00_2057;
{ /* Ieee/input.scm 147 */
 obj_t BgL_tmpz00_8188;
BgL_tmpz00_8188 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_2057 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8188); } 
{ /* Ieee/input.scm 147 */

return 
BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2057);} } break;case 1L : 

{ /* Ieee/input.scm 147 */
 obj_t BgL_ipz00_2058;
BgL_ipz00_2058 = 
VECTOR_REF(BgL_opt1254z00_17,0L); 
{ /* Ieee/input.scm 147 */

return 
BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2058);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4260z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1254z00_17)));} } } } 

}



/* read-byte */
BGL_EXPORTED_DEF obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_16)
{
{ /* Ieee/input.scm 147 */
{ 
 obj_t BgL_iportz00_2062;
BgL_iportz00_2062 = BgL_ipz00_16; 
{ 

{ /* Ieee/input.scm 148 */
 bool_t BgL_test4674z00_8199;
{ /* Ieee/input.scm 148 */
 obj_t BgL_arg1846z00_2100;
{ /* Ieee/input.scm 148 */
 obj_t BgL_res3461z00_4960;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_res3461z00_4960 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8202;
BgL_auxz00_8202 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8202,BFALSE,BFALSE);} 
BgL_arg1846z00_2100 = BgL_res3461z00_4960; } 
BgL_test4674z00_8199 = 
INPUT_PORT_CLOSEP(BgL_arg1846z00_2100); } 
if(BgL_test4674z00_8199)
{ /* Ieee/input.scm 148 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1842z00_2094;
{ /* Ieee/input.scm 148 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_2095;
{ /* Ieee/input.scm 148 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_2098;
BgL_new1045z00_2098 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 148 */
 long BgL_arg1845z00_2099;
BgL_arg1845z00_2099 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_2098), BgL_arg1845z00_2099); } 
BgL_new1046z00_2095 = BgL_new1045z00_2098; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2095)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2095)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8215;
{ /* Ieee/input.scm 148 */
 obj_t BgL_arg1843z00_2096;
{ /* Ieee/input.scm 148 */
 obj_t BgL_arg1844z00_2097;
{ /* Ieee/input.scm 148 */
 obj_t BgL_classz00_4965;
BgL_classz00_4965 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1844z00_2097 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_4965); } 
BgL_arg1843z00_2096 = 
VECTOR_REF(BgL_arg1844z00_2097,2L); } 
{ /* Ieee/input.scm 148 */
 obj_t BgL_auxz00_8219;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1843z00_2096))
{ /* Ieee/input.scm 148 */
BgL_auxz00_8219 = BgL_arg1843z00_2096
; }  else 
{ 
 obj_t BgL_auxz00_8222;
BgL_auxz00_8222 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg1843z00_2096); 
FAILURE(BgL_auxz00_8222,BFALSE,BFALSE);} 
BgL_auxz00_8215 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_8219); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2095)))->BgL_stackz00)=((obj_t)BgL_auxz00_8215),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2095)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2095)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8232;
{ /* Ieee/input.scm 148 */
 obj_t BgL_res3462z00_4967;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_res3462z00_4967 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8236;
BgL_auxz00_8236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8236,BFALSE,BFALSE);} 
BgL_auxz00_8232 = BgL_res3462z00_4967; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2095)))->BgL_objz00)=((obj_t)BgL_auxz00_8232),BUNSPEC); } 
BgL_arg1842z00_2094 = BgL_new1046z00_2095; } 
BGL_TAIL return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1842z00_2094));}  else 
{ /* Ieee/input.scm 148 */
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4935;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4935 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8245;
BgL_auxz00_8245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8245,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_4935); } 
{ /* Ieee/input.scm 148 */
 long BgL_matchz00_2237;
{ /* Ieee/input.scm 148 */
 long BgL_arg1946z00_2240; long BgL_arg1947z00_2241;
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4936;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4936 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8252;
BgL_auxz00_8252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8252,BFALSE,BFALSE);} 
BgL_arg1946z00_2240 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4936); } 
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4937;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4937 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8259;
BgL_auxz00_8259 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8259,BFALSE,BFALSE);} 
BgL_arg1947z00_2241 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4937); } 
{ 
 long BgL_forwardz00_4939; long BgL_bufposz00_4940;
BgL_forwardz00_4939 = BgL_arg1946z00_2240; 
BgL_bufposz00_4940 = BgL_arg1947z00_2241; 
BgL_statezd20zd21054z00_4938:
if(
(BgL_forwardz00_4939==BgL_bufposz00_4940))
{ /* Ieee/input.scm 148 */
 bool_t BgL_test4682z00_8266;
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4945;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4945 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8269;
BgL_auxz00_8269 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4262z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8269,BFALSE,BFALSE);} 
BgL_test4682z00_8266 = 
rgc_fill_buffer(BgL_inputzd2portzd2_4945); } 
if(BgL_test4682z00_8266)
{ /* Ieee/input.scm 148 */
 long BgL_arg1851z00_4946; long BgL_arg1852z00_4947;
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4948;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4948 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8276;
BgL_auxz00_8276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4262z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8276,BFALSE,BFALSE);} 
BgL_arg1851z00_4946 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4948); } 
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4949;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4949 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8283;
BgL_auxz00_8283 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4262z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8283,BFALSE,BFALSE);} 
BgL_arg1852z00_4947 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4949); } 
{ 
 long BgL_bufposz00_8289; long BgL_forwardz00_8288;
BgL_forwardz00_8288 = BgL_arg1851z00_4946; 
BgL_bufposz00_8289 = BgL_arg1852z00_4947; 
BgL_bufposz00_4940 = BgL_bufposz00_8289; 
BgL_forwardz00_4939 = BgL_forwardz00_8288; 
goto BgL_statezd20zd21054z00_4938;} }  else 
{ /* Ieee/input.scm 148 */
BgL_matchz00_2237 = 1L; } }  else 
{ /* Ieee/input.scm 148 */
 int BgL_curz00_4950;
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4951;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4951 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8292;
BgL_auxz00_8292 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4262z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8292,BFALSE,BFALSE);} 
BgL_curz00_4950 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_4951, BgL_forwardz00_4939); } 
{ /* Ieee/input.scm 148 */

{ /* Ieee/input.scm 148 */
 long BgL_arg1853z00_4953;
BgL_arg1853z00_4953 = 
(1L+BgL_forwardz00_4939); 
{ /* Ieee/input.scm 148 */
 long BgL_newzd2matchzd2_4955;
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4956;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4956 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8300;
BgL_auxz00_8300 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4262z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8300,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_4956, BgL_arg1853z00_4953); } 
BgL_newzd2matchzd2_4955 = 0L; 
BgL_matchz00_2237 = BgL_newzd2matchzd2_4955; } } } } } } 
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4958;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4958 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8307;
BgL_auxz00_8307 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8307,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_4958); } 
switch( BgL_matchz00_2237) { case 1L : 

{ /* Ieee/input.scm 148 */
 bool_t BgL_test4689z00_8312;
{ /* Ieee/input.scm 148 */
 long BgL_arg1939z00_2227;
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4932;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4932 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8315;
BgL_auxz00_8315 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8315,BFALSE,BFALSE);} 
BgL_arg1939z00_2227 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_4932); } 
BgL_test4689z00_8312 = 
(BgL_arg1939z00_2227==0L); } 
if(BgL_test4689z00_8312)
{ /* Ieee/input.scm 148 */
return BEOF;}  else 
{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4934;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4934 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8323;
BgL_auxz00_8323 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8323,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_4934));} } break;case 0L : 

{ /* Ieee/input.scm 148 */
 obj_t BgL_inputzd2portzd2_4959;
if(
INPUT_PORTP(BgL_iportz00_2062))
{ /* Ieee/input.scm 148 */
BgL_inputzd2portzd2_4959 = BgL_iportz00_2062; }  else 
{ 
 obj_t BgL_auxz00_8331;
BgL_auxz00_8331 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6449L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2062); 
FAILURE(BgL_auxz00_8331,BFALSE,BFALSE);} 
return 
BINT(
RGC_BUFFER_BYTE(BgL_inputzd2portzd2_4959));} break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_2237));} } } } } } } 

}



/* _peek-byte */
obj_t BGl__peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t BgL_env1259z00_21, obj_t BgL_opt1258z00_20)
{
{ /* Ieee/input.scm 155 */
{ /* Ieee/input.scm 155 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1258z00_20)) { case 0L : 

{ /* Ieee/input.scm 155 */
 obj_t BgL_ipz00_2274;
{ /* Ieee/input.scm 155 */
 obj_t BgL_tmpz00_8340;
BgL_tmpz00_8340 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_2274 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8340); } 
{ /* Ieee/input.scm 155 */

return 
BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2274);} } break;case 1L : 

{ /* Ieee/input.scm 155 */
 obj_t BgL_ipz00_2275;
BgL_ipz00_2275 = 
VECTOR_REF(BgL_opt1258z00_20,0L); 
{ /* Ieee/input.scm 155 */

return 
BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_2275);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4263z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1258z00_20)));} } } } 

}



/* peek-byte */
BGL_EXPORTED_DEF obj_t BGl_peekzd2bytezd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_19)
{
{ /* Ieee/input.scm 155 */
{ 
 obj_t BgL_iportz00_2279;
BgL_iportz00_2279 = BgL_ipz00_19; 
{ 

{ /* Ieee/input.scm 156 */
 bool_t BgL_test4693z00_8351;
{ /* Ieee/input.scm 156 */
 obj_t BgL_arg1954z00_2317;
{ /* Ieee/input.scm 156 */
 obj_t BgL_res3464z00_5015;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_res3464z00_5015 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8354;
BgL_auxz00_8354 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8354,BFALSE,BFALSE);} 
BgL_arg1954z00_2317 = BgL_res3464z00_5015; } 
BgL_test4693z00_8351 = 
INPUT_PORT_CLOSEP(BgL_arg1954z00_2317); } 
if(BgL_test4693z00_8351)
{ /* Ieee/input.scm 156 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1950z00_2311;
{ /* Ieee/input.scm 156 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_2312;
{ /* Ieee/input.scm 156 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_2315;
BgL_new1045z00_2315 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 156 */
 long BgL_arg1953z00_2316;
BgL_arg1953z00_2316 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_2315), BgL_arg1953z00_2316); } 
BgL_new1046z00_2312 = BgL_new1045z00_2315; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2312)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2312)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8367;
{ /* Ieee/input.scm 156 */
 obj_t BgL_arg1951z00_2313;
{ /* Ieee/input.scm 156 */
 obj_t BgL_arg1952z00_2314;
{ /* Ieee/input.scm 156 */
 obj_t BgL_classz00_5020;
BgL_classz00_5020 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1952z00_2314 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5020); } 
BgL_arg1951z00_2313 = 
VECTOR_REF(BgL_arg1952z00_2314,2L); } 
{ /* Ieee/input.scm 156 */
 obj_t BgL_auxz00_8371;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1951z00_2313))
{ /* Ieee/input.scm 156 */
BgL_auxz00_8371 = BgL_arg1951z00_2313
; }  else 
{ 
 obj_t BgL_auxz00_8374;
BgL_auxz00_8374 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg1951z00_2313); 
FAILURE(BgL_auxz00_8374,BFALSE,BFALSE);} 
BgL_auxz00_8367 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_8371); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2312)))->BgL_stackz00)=((obj_t)BgL_auxz00_8367),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2312)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2312)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8384;
{ /* Ieee/input.scm 156 */
 obj_t BgL_res3465z00_5022;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_res3465z00_5022 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8388;
BgL_auxz00_8388 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8388,BFALSE,BFALSE);} 
BgL_auxz00_8384 = BgL_res3465z00_5022; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2312)))->BgL_objz00)=((obj_t)BgL_auxz00_8384),BUNSPEC); } 
BgL_arg1950z00_2311 = BgL_new1046z00_2312; } 
BGL_TAIL return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1950z00_2311));}  else 
{ /* Ieee/input.scm 156 */
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_4987;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_4987 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8397;
BgL_auxz00_8397 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8397,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_4987); } 
{ /* Ieee/input.scm 156 */
 long BgL_matchz00_2454;
{ /* Ieee/input.scm 156 */
 long BgL_arg2052z00_2459; long BgL_arg2055z00_2460;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_4988;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_4988 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8404;
BgL_auxz00_8404 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8404,BFALSE,BFALSE);} 
BgL_arg2052z00_2459 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_4988); } 
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_4989;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_4989 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8411;
BgL_auxz00_8411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8411,BFALSE,BFALSE);} 
BgL_arg2055z00_2460 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_4989); } 
{ 
 long BgL_forwardz00_4991; long BgL_bufposz00_4992;
BgL_forwardz00_4991 = BgL_arg2052z00_2459; 
BgL_bufposz00_4992 = BgL_arg2055z00_2460; 
BgL_statezd20zd21060z00_4990:
if(
(BgL_forwardz00_4991==BgL_bufposz00_4992))
{ /* Ieee/input.scm 156 */
 bool_t BgL_test4701z00_8418;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_4997;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_4997 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8421;
BgL_auxz00_8421 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4265z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8421,BFALSE,BFALSE);} 
BgL_test4701z00_8418 = 
rgc_fill_buffer(BgL_inputzd2portzd2_4997); } 
if(BgL_test4701z00_8418)
{ /* Ieee/input.scm 156 */
 long BgL_arg1959z00_4998; long BgL_arg1960z00_4999;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_5000;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_5000 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8428;
BgL_auxz00_8428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4265z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8428,BFALSE,BFALSE);} 
BgL_arg1959z00_4998 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5000); } 
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_5001;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_5001 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8435;
BgL_auxz00_8435 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4265z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8435,BFALSE,BFALSE);} 
BgL_arg1960z00_4999 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5001); } 
{ 
 long BgL_bufposz00_8441; long BgL_forwardz00_8440;
BgL_forwardz00_8440 = BgL_arg1959z00_4998; 
BgL_bufposz00_8441 = BgL_arg1960z00_4999; 
BgL_bufposz00_4992 = BgL_bufposz00_8441; 
BgL_forwardz00_4991 = BgL_forwardz00_8440; 
goto BgL_statezd20zd21060z00_4990;} }  else 
{ /* Ieee/input.scm 156 */
BgL_matchz00_2454 = 1L; } }  else 
{ /* Ieee/input.scm 156 */
 int BgL_curz00_5002;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_5003;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_5003 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8444;
BgL_auxz00_8444 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4265z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8444,BFALSE,BFALSE);} 
BgL_curz00_5002 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5003, BgL_forwardz00_4991); } 
{ /* Ieee/input.scm 156 */

{ /* Ieee/input.scm 156 */
 long BgL_arg1961z00_5005;
BgL_arg1961z00_5005 = 
(1L+BgL_forwardz00_4991); 
{ /* Ieee/input.scm 156 */
 long BgL_newzd2matchzd2_5007;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_5008;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_5008 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8452;
BgL_auxz00_8452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4265z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8452,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5008, BgL_arg1961z00_5005); } 
BgL_newzd2matchzd2_5007 = 0L; 
BgL_matchz00_2454 = BgL_newzd2matchzd2_5007; } } } } } } 
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_5010;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_5010 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8459;
BgL_auxz00_8459 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8459,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_5010); } 
switch( BgL_matchz00_2454) { case 1L : 

{ /* Ieee/input.scm 156 */
 bool_t BgL_test4708z00_8464;
{ /* Ieee/input.scm 156 */
 long BgL_arg2044z00_2444;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_4984;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_4984 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8467;
BgL_auxz00_8467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8467,BFALSE,BFALSE);} 
BgL_arg2044z00_2444 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_4984); } 
BgL_test4708z00_8464 = 
(BgL_arg2044z00_2444==0L); } 
if(BgL_test4708z00_8464)
{ /* Ieee/input.scm 156 */
return BEOF;}  else 
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_4986;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_4986 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8475;
BgL_auxz00_8475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8475,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_4986));} } break;case 0L : 

{ /* Ieee/input.scm 158 */
 int BgL_cz00_2457;
{ /* Ieee/input.scm 156 */
 obj_t BgL_inputzd2portzd2_5011;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_inputzd2portzd2_5011 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8483;
BgL_auxz00_8483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8483,BFALSE,BFALSE);} 
BgL_cz00_2457 = 
RGC_BUFFER_BYTE(BgL_inputzd2portzd2_5011); } 
{ /* Ieee/input.scm 159 */
 obj_t BgL_arg2051z00_2458;
{ /* Ieee/input.scm 159 */
 obj_t BgL_res3463z00_5012;
if(
INPUT_PORTP(BgL_iportz00_2279))
{ /* Ieee/input.scm 156 */
BgL_res3463z00_5012 = BgL_iportz00_2279; }  else 
{ 
 obj_t BgL_auxz00_8490;
BgL_auxz00_8490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(6838L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2279); 
FAILURE(BgL_auxz00_8490,BFALSE,BFALSE);} 
BgL_arg2051z00_2458 = BgL_res3463z00_5012; } 
rgc_buffer_unget_char(BgL_arg2051z00_2458, BgL_cz00_2457); } 
return 
BINT(BgL_cz00_2457);} break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_2454));} } } } } } } 

}



/* eof-object */
BGL_EXPORTED_DEF obj_t BGl_eofzd2objectzd2zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 166 */
return BEOF;} 

}



/* &eof-object */
obj_t BGl_z62eofzd2objectzb0zz__r4_input_6_10_2z00(obj_t BgL_envz00_6001)
{
{ /* Ieee/input.scm 166 */
return 
BGl_eofzd2objectzd2zz__r4_input_6_10_2z00();} 

}



/* eof-object? */
BGL_EXPORTED_DEF bool_t BGl_eofzd2objectzf3z21zz__r4_input_6_10_2z00(obj_t BgL_objectz00_22)
{
{ /* Ieee/input.scm 172 */
return 
EOF_OBJECTP(BgL_objectz00_22);} 

}



/* &eof-object? */
obj_t BGl_z62eofzd2objectzf3z43zz__r4_input_6_10_2z00(obj_t BgL_envz00_5999, obj_t BgL_objectz00_6000)
{
{ /* Ieee/input.scm 172 */
return 
BBOOL(
EOF_OBJECTP(BgL_objectz00_6000));} 

}



/* _char-ready? */
obj_t BGl__charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t BgL_env1263z00_25, obj_t BgL_opt1262z00_24)
{
{ /* Ieee/input.scm 178 */
{ /* Ieee/input.scm 178 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1262z00_24)) { case 0L : 

{ /* Ieee/input.scm 178 */
 obj_t BgL_ipz00_6934;
{ /* Ieee/input.scm 178 */
 obj_t BgL_tmpz00_8503;
BgL_tmpz00_8503 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_6934 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8503); } 
{ /* Ieee/input.scm 178 */

return 
BBOOL(
bgl_rgc_charready(BgL_ipz00_6934));} } break;case 1L : 

{ /* Ieee/input.scm 178 */
 obj_t BgL_ipz00_6935;
BgL_ipz00_6935 = 
VECTOR_REF(BgL_opt1262z00_24,0L); 
{ /* Ieee/input.scm 178 */

{ /* Ieee/input.scm 179 */
 bool_t BgL_tmpz00_8509;
{ /* Ieee/input.scm 179 */
 obj_t BgL_tmpz00_8510;
if(
INPUT_PORTP(BgL_ipz00_6935))
{ /* Ieee/input.scm 179 */
BgL_tmpz00_8510 = BgL_ipz00_6935
; }  else 
{ 
 obj_t BgL_auxz00_8513;
BgL_auxz00_8513 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(7849L), BGl_string4268z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6935); 
FAILURE(BgL_auxz00_8513,BFALSE,BFALSE);} 
BgL_tmpz00_8509 = 
bgl_rgc_charready(BgL_tmpz00_8510); } 
return 
BBOOL(BgL_tmpz00_8509);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4266z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1262z00_24)));} } } } 

}



/* char-ready? */
BGL_EXPORTED_DEF bool_t BGl_charzd2readyzf3z21zz__r4_input_6_10_2z00(obj_t BgL_ipz00_23)
{
{ /* Ieee/input.scm 178 */
{ /* Ieee/input.scm 179 */
 obj_t BgL_tmpz00_8524;
if(
INPUT_PORTP(BgL_ipz00_23))
{ /* Ieee/input.scm 179 */
BgL_tmpz00_8524 = BgL_ipz00_23
; }  else 
{ 
 obj_t BgL_auxz00_8527;
BgL_auxz00_8527 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(7849L), BGl_string4267z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_23); 
FAILURE(BgL_auxz00_8527,BFALSE,BFALSE);} 
return 
bgl_rgc_charready(BgL_tmpz00_8524);} } 

}



/* _read-line */
obj_t BGl__readzd2linezd2zz__r4_input_6_10_2z00(obj_t BgL_env1267z00_28, obj_t BgL_opt1266z00_27)
{
{ /* Ieee/input.scm 184 */
{ /* Ieee/input.scm 184 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1266z00_27)) { case 0L : 

{ /* Ieee/input.scm 184 */
 obj_t BgL_ipz00_2498;
{ /* Ieee/input.scm 184 */
 obj_t BgL_tmpz00_8532;
BgL_tmpz00_8532 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_2498 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_8532); } 
{ /* Ieee/input.scm 184 */

return 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_2498);} } break;case 1L : 

{ /* Ieee/input.scm 184 */
 obj_t BgL_ipz00_2499;
BgL_ipz00_2499 = 
VECTOR_REF(BgL_opt1266z00_27,0L); 
{ /* Ieee/input.scm 184 */

return 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_2499);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4269z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1266z00_27)));} } } } 

}



/* read-line */
BGL_EXPORTED_DEF obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_26)
{
{ /* Ieee/input.scm 184 */
{ /* Ieee/input.scm 185 */
 bool_t BgL_test4715z00_8543;
{ /* Ieee/input.scm 185 */
 long BgL_arg2222z00_2840;
{ /* Ieee/input.scm 185 */
 obj_t BgL_tmpz00_8544;
if(
INPUT_PORTP(BgL_ipz00_26))
{ /* Ieee/input.scm 185 */
BgL_tmpz00_8544 = BgL_ipz00_26
; }  else 
{ 
 obj_t BgL_auxz00_8547;
BgL_auxz00_8547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8167L), BGl_string4270z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_26); 
FAILURE(BgL_auxz00_8547,BFALSE,BFALSE);} 
BgL_arg2222z00_2840 = 
BGL_INPUT_PORT_BUFSIZ(BgL_tmpz00_8544); } 
BgL_test4715z00_8543 = 
(BgL_arg2222z00_2840>2L); } 
if(BgL_test4715z00_8543)
{ 
 obj_t BgL_iportz00_2505;
BgL_iportz00_2505 = BgL_ipz00_26; 
{ 
 int BgL_minz00_2660; int BgL_maxz00_2661; obj_t BgL_iportz00_2623; long BgL_lastzd2matchzd2_2624; long BgL_forwardz00_2625; long BgL_bufposz00_2626; obj_t BgL_iportz00_2595; long BgL_lastzd2matchzd2_2596; long BgL_forwardz00_2597; long BgL_bufposz00_2598; obj_t BgL_iportz00_2582; long BgL_lastzd2matchzd2_2583; long BgL_forwardz00_2584; long BgL_bufposz00_2585; obj_t BgL_iportz00_2567; long BgL_lastzd2matchzd2_2568; long BgL_forwardz00_2569; long BgL_bufposz00_2570; obj_t BgL_iportz00_2551; long BgL_lastzd2matchzd2_2552; long BgL_forwardz00_2553; long BgL_bufposz00_2554;
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4717z00_8553;
{ /* Ieee/input.scm 186 */
 obj_t BgL_arg2065z00_2550;
{ /* Ieee/input.scm 186 */
 obj_t BgL_res3466z00_5158;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_res3466z00_5158 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8556;
BgL_auxz00_8556 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8556,BFALSE,BFALSE);} 
BgL_arg2065z00_2550 = BgL_res3466z00_5158; } 
BgL_test4717z00_8553 = 
INPUT_PORT_CLOSEP(BgL_arg2065z00_2550); } 
if(BgL_test4717z00_8553)
{ /* Ieee/input.scm 186 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2061z00_2544;
{ /* Ieee/input.scm 186 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_2545;
{ /* Ieee/input.scm 186 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_2548;
BgL_new1045z00_2548 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 186 */
 long BgL_arg2064z00_2549;
BgL_arg2064z00_2549 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_2548), BgL_arg2064z00_2549); } 
BgL_new1046z00_2545 = BgL_new1045z00_2548; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2545)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2545)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8569;
{ /* Ieee/input.scm 186 */
 obj_t BgL_arg2062z00_2546;
{ /* Ieee/input.scm 186 */
 obj_t BgL_arg2063z00_2547;
{ /* Ieee/input.scm 186 */
 obj_t BgL_classz00_5163;
BgL_classz00_5163 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2063z00_2547 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5163); } 
BgL_arg2062z00_2546 = 
VECTOR_REF(BgL_arg2063z00_2547,2L); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_auxz00_8573;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2062z00_2546))
{ /* Ieee/input.scm 186 */
BgL_auxz00_8573 = BgL_arg2062z00_2546
; }  else 
{ 
 obj_t BgL_auxz00_8576;
BgL_auxz00_8576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2062z00_2546); 
FAILURE(BgL_auxz00_8576,BFALSE,BFALSE);} 
BgL_auxz00_8569 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_8573); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2545)))->BgL_stackz00)=((obj_t)BgL_auxz00_8569),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2545)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2545)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8586;
{ /* Ieee/input.scm 186 */
 obj_t BgL_res3467z00_5165;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_res3467z00_5165 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8590;
BgL_auxz00_8590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8590,BFALSE,BFALSE);} 
BgL_auxz00_8586 = BgL_res3467z00_5165; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2545)))->BgL_objz00)=((obj_t)BgL_auxz00_8586),BUNSPEC); } 
BgL_arg2061z00_2544 = BgL_new1046z00_2545; } 
BGL_TAIL return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2061z00_2544));}  else 
{ /* Ieee/input.scm 186 */
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5146;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5146 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8599;
BgL_auxz00_8599 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8599,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_5146); } 
{ /* Ieee/input.scm 186 */
 long BgL_matchz00_2767;
{ /* Ieee/input.scm 186 */
 long BgL_arg2207z00_2774; long BgL_arg2208z00_2775;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5147;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5147 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8606;
BgL_auxz00_8606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8606,BFALSE,BFALSE);} 
BgL_arg2207z00_2774 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5147); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5148;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5148 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8613;
BgL_auxz00_8613 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8613,BFALSE,BFALSE);} 
BgL_arg2208z00_2775 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5148); } 
BgL_iportz00_2567 = BgL_iportz00_2505; 
BgL_lastzd2matchzd2_2568 = 4L; 
BgL_forwardz00_2569 = BgL_arg2207z00_2774; 
BgL_bufposz00_2570 = BgL_arg2208z00_2775; 
BgL_zc3z04anonymousza32076ze3z87_2571:
if(
(BgL_forwardz00_2569==BgL_bufposz00_2570))
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4725z00_8620;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5050;
if(
INPUT_PORTP(BgL_iportz00_2567))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5050 = BgL_iportz00_2567; }  else 
{ 
 obj_t BgL_auxz00_8623;
BgL_auxz00_8623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4272z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2567); 
FAILURE(BgL_auxz00_8623,BFALSE,BFALSE);} 
BgL_test4725z00_8620 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5050); } 
if(BgL_test4725z00_8620)
{ /* Ieee/input.scm 186 */
 long BgL_arg2079z00_2574; long BgL_arg2080z00_2575;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5051;
if(
INPUT_PORTP(BgL_iportz00_2567))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5051 = BgL_iportz00_2567; }  else 
{ 
 obj_t BgL_auxz00_8630;
BgL_auxz00_8630 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4272z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2567); 
FAILURE(BgL_auxz00_8630,BFALSE,BFALSE);} 
BgL_arg2079z00_2574 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5051); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5052;
if(
INPUT_PORTP(BgL_iportz00_2567))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5052 = BgL_iportz00_2567; }  else 
{ 
 obj_t BgL_auxz00_8637;
BgL_auxz00_8637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4272z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2567); 
FAILURE(BgL_auxz00_8637,BFALSE,BFALSE);} 
BgL_arg2080z00_2575 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5052); } 
{ 
 long BgL_bufposz00_8643; long BgL_forwardz00_8642;
BgL_forwardz00_8642 = BgL_arg2079z00_2574; 
BgL_bufposz00_8643 = BgL_arg2080z00_2575; 
BgL_bufposz00_2570 = BgL_bufposz00_8643; 
BgL_forwardz00_2569 = BgL_forwardz00_8642; 
goto BgL_zc3z04anonymousza32076ze3z87_2571;} }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_lastzd2matchzd2_2568; } }  else 
{ /* Ieee/input.scm 186 */
 int BgL_curz00_2576;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5053;
if(
INPUT_PORTP(BgL_iportz00_2567))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5053 = BgL_iportz00_2567; }  else 
{ 
 obj_t BgL_auxz00_8646;
BgL_auxz00_8646 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4272z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2567); 
FAILURE(BgL_auxz00_8646,BFALSE,BFALSE);} 
BgL_curz00_2576 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5053, BgL_forwardz00_2569); } 
{ /* Ieee/input.scm 186 */

if(
(
(long)(BgL_curz00_2576)==13L))
{ /* Ieee/input.scm 186 */
BgL_iportz00_2582 = BgL_iportz00_2567; 
BgL_lastzd2matchzd2_2583 = BgL_lastzd2matchzd2_2568; 
BgL_forwardz00_2584 = 
(1L+BgL_forwardz00_2569); 
BgL_bufposz00_2585 = BgL_bufposz00_2570; 
BgL_zc3z04anonymousza32087ze3z87_2586:
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_2587;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5063;
if(
INPUT_PORTP(BgL_iportz00_2582))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5063 = BgL_iportz00_2582; }  else 
{ 
 obj_t BgL_auxz00_8656;
BgL_auxz00_8656 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4273z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2582); 
FAILURE(BgL_auxz00_8656,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5063, BgL_forwardz00_2584); } 
BgL_newzd2matchzd2_2587 = 3L; 
if(
(BgL_forwardz00_2584==BgL_bufposz00_2585))
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4733z00_8663;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5067;
if(
INPUT_PORTP(BgL_iportz00_2582))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5067 = BgL_iportz00_2582; }  else 
{ 
 obj_t BgL_auxz00_8666;
BgL_auxz00_8666 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4273z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2582); 
FAILURE(BgL_auxz00_8666,BFALSE,BFALSE);} 
BgL_test4733z00_8663 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5067); } 
if(BgL_test4733z00_8663)
{ /* Ieee/input.scm 186 */
 long BgL_arg2090z00_2590; long BgL_arg2091z00_2591;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5068;
if(
INPUT_PORTP(BgL_iportz00_2582))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5068 = BgL_iportz00_2582; }  else 
{ 
 obj_t BgL_auxz00_8673;
BgL_auxz00_8673 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4273z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2582); 
FAILURE(BgL_auxz00_8673,BFALSE,BFALSE);} 
BgL_arg2090z00_2590 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5068); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5069;
if(
INPUT_PORTP(BgL_iportz00_2582))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5069 = BgL_iportz00_2582; }  else 
{ 
 obj_t BgL_auxz00_8680;
BgL_auxz00_8680 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4273z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2582); 
FAILURE(BgL_auxz00_8680,BFALSE,BFALSE);} 
BgL_arg2091z00_2591 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5069); } 
{ 
 long BgL_bufposz00_8686; long BgL_forwardz00_8685;
BgL_forwardz00_8685 = BgL_arg2090z00_2590; 
BgL_bufposz00_8686 = BgL_arg2091z00_2591; 
BgL_bufposz00_2585 = BgL_bufposz00_8686; 
BgL_forwardz00_2584 = BgL_forwardz00_8685; 
goto BgL_zc3z04anonymousza32087ze3z87_2586;} }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_newzd2matchzd2_2587; } }  else 
{ /* Ieee/input.scm 186 */
 int BgL_curz00_2592;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5070;
if(
INPUT_PORTP(BgL_iportz00_2582))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5070 = BgL_iportz00_2582; }  else 
{ 
 obj_t BgL_auxz00_8689;
BgL_auxz00_8689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4273z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2582); 
FAILURE(BgL_auxz00_8689,BFALSE,BFALSE);} 
BgL_curz00_2592 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5070, BgL_forwardz00_2584); } 
{ /* Ieee/input.scm 186 */

if(
(
(long)(BgL_curz00_2592)==10L))
{ /* Ieee/input.scm 186 */
 long BgL_arg2093z00_2594;
BgL_arg2093z00_2594 = 
(1L+BgL_forwardz00_2584); 
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_5074;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5075;
if(
INPUT_PORTP(BgL_iportz00_2582))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5075 = BgL_iportz00_2582; }  else 
{ 
 obj_t BgL_auxz00_8700;
BgL_auxz00_8700 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4273z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2582); 
FAILURE(BgL_auxz00_8700,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5075, BgL_arg2093z00_2594); } 
BgL_newzd2matchzd2_5074 = 3L; 
BgL_matchz00_2767 = BgL_newzd2matchzd2_5074; } }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_newzd2matchzd2_2587; } } } } }  else 
{ /* Ieee/input.scm 186 */
if(
(
(long)(BgL_curz00_2576)==10L))
{ /* Ieee/input.scm 186 */
 long BgL_arg2084z00_2580;
BgL_arg2084z00_2580 = 
(1L+BgL_forwardz00_2569); 
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_5059;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5060;
if(
INPUT_PORTP(BgL_iportz00_2567))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5060 = BgL_iportz00_2567; }  else 
{ 
 obj_t BgL_auxz00_8712;
BgL_auxz00_8712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4272z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2567); 
FAILURE(BgL_auxz00_8712,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5060, BgL_arg2084z00_2580); } 
BgL_newzd2matchzd2_5059 = 3L; 
BgL_matchz00_2767 = BgL_newzd2matchzd2_5059; } }  else 
{ /* Ieee/input.scm 186 */
BgL_iportz00_2551 = BgL_iportz00_2567; 
BgL_lastzd2matchzd2_2552 = BgL_lastzd2matchzd2_2568; 
BgL_forwardz00_2553 = 
(1L+BgL_forwardz00_2569); 
BgL_bufposz00_2554 = BgL_bufposz00_2570; 
BgL_zc3z04anonymousza32066ze3z87_2555:
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_2556;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5031;
if(
INPUT_PORTP(BgL_iportz00_2551))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5031 = BgL_iportz00_2551; }  else 
{ 
 obj_t BgL_auxz00_8719;
BgL_auxz00_8719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4271z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2551); 
FAILURE(BgL_auxz00_8719,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5031, BgL_forwardz00_2553); } 
BgL_newzd2matchzd2_2556 = 2L; 
if(
(BgL_forwardz00_2553==BgL_bufposz00_2554))
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4744z00_8726;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5035;
if(
INPUT_PORTP(BgL_iportz00_2551))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5035 = BgL_iportz00_2551; }  else 
{ 
 obj_t BgL_auxz00_8729;
BgL_auxz00_8729 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4271z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2551); 
FAILURE(BgL_auxz00_8729,BFALSE,BFALSE);} 
BgL_test4744z00_8726 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5035); } 
if(BgL_test4744z00_8726)
{ /* Ieee/input.scm 186 */
 long BgL_arg2069z00_2559; long BgL_arg2070z00_2560;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5036;
if(
INPUT_PORTP(BgL_iportz00_2551))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5036 = BgL_iportz00_2551; }  else 
{ 
 obj_t BgL_auxz00_8736;
BgL_auxz00_8736 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4271z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2551); 
FAILURE(BgL_auxz00_8736,BFALSE,BFALSE);} 
BgL_arg2069z00_2559 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5036); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5037;
if(
INPUT_PORTP(BgL_iportz00_2551))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5037 = BgL_iportz00_2551; }  else 
{ 
 obj_t BgL_auxz00_8743;
BgL_auxz00_8743 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4271z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2551); 
FAILURE(BgL_auxz00_8743,BFALSE,BFALSE);} 
BgL_arg2070z00_2560 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5037); } 
{ 
 long BgL_bufposz00_8749; long BgL_forwardz00_8748;
BgL_forwardz00_8748 = BgL_arg2069z00_2559; 
BgL_bufposz00_8749 = BgL_arg2070z00_2560; 
BgL_bufposz00_2554 = BgL_bufposz00_8749; 
BgL_forwardz00_2553 = BgL_forwardz00_8748; 
goto BgL_zc3z04anonymousza32066ze3z87_2555;} }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_newzd2matchzd2_2556; } }  else 
{ /* Ieee/input.scm 186 */
 int BgL_curz00_2561;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5038;
if(
INPUT_PORTP(BgL_iportz00_2551))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5038 = BgL_iportz00_2551; }  else 
{ 
 obj_t BgL_auxz00_8752;
BgL_auxz00_8752 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4271z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2551); 
FAILURE(BgL_auxz00_8752,BFALSE,BFALSE);} 
BgL_curz00_2561 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5038, BgL_forwardz00_2553); } 
{ /* Ieee/input.scm 186 */

if(
(
(long)(BgL_curz00_2561)==13L))
{ /* Ieee/input.scm 186 */
BgL_iportz00_2623 = BgL_iportz00_2551; 
BgL_lastzd2matchzd2_2624 = BgL_newzd2matchzd2_2556; 
BgL_forwardz00_2625 = 
(1L+BgL_forwardz00_2553); 
BgL_bufposz00_2626 = BgL_bufposz00_2554; 
BgL_zc3z04anonymousza32106ze3z87_2627:
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_2628;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5100;
if(
INPUT_PORTP(BgL_iportz00_2623))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5100 = BgL_iportz00_2623; }  else 
{ 
 obj_t BgL_auxz00_8762;
BgL_auxz00_8762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4275z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2623); 
FAILURE(BgL_auxz00_8762,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5100, BgL_forwardz00_2625); } 
BgL_newzd2matchzd2_2628 = 0L; 
if(
(BgL_forwardz00_2625==BgL_bufposz00_2626))
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4752z00_8769;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5104;
if(
INPUT_PORTP(BgL_iportz00_2623))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5104 = BgL_iportz00_2623; }  else 
{ 
 obj_t BgL_auxz00_8772;
BgL_auxz00_8772 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4275z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2623); 
FAILURE(BgL_auxz00_8772,BFALSE,BFALSE);} 
BgL_test4752z00_8769 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5104); } 
if(BgL_test4752z00_8769)
{ /* Ieee/input.scm 186 */
 long BgL_arg2109z00_2631; long BgL_arg2110z00_2632;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5105;
if(
INPUT_PORTP(BgL_iportz00_2623))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5105 = BgL_iportz00_2623; }  else 
{ 
 obj_t BgL_auxz00_8779;
BgL_auxz00_8779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4275z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2623); 
FAILURE(BgL_auxz00_8779,BFALSE,BFALSE);} 
BgL_arg2109z00_2631 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5105); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5106;
if(
INPUT_PORTP(BgL_iportz00_2623))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5106 = BgL_iportz00_2623; }  else 
{ 
 obj_t BgL_auxz00_8786;
BgL_auxz00_8786 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4275z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2623); 
FAILURE(BgL_auxz00_8786,BFALSE,BFALSE);} 
BgL_arg2110z00_2632 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5106); } 
{ 
 long BgL_bufposz00_8792; long BgL_forwardz00_8791;
BgL_forwardz00_8791 = BgL_arg2109z00_2631; 
BgL_bufposz00_8792 = BgL_arg2110z00_2632; 
BgL_bufposz00_2626 = BgL_bufposz00_8792; 
BgL_forwardz00_2625 = BgL_forwardz00_8791; 
goto BgL_zc3z04anonymousza32106ze3z87_2627;} }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_newzd2matchzd2_2628; } }  else 
{ /* Ieee/input.scm 186 */
 int BgL_curz00_2633;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5107;
if(
INPUT_PORTP(BgL_iportz00_2623))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5107 = BgL_iportz00_2623; }  else 
{ 
 obj_t BgL_auxz00_8795;
BgL_auxz00_8795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4275z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2623); 
FAILURE(BgL_auxz00_8795,BFALSE,BFALSE);} 
BgL_curz00_2633 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5107, BgL_forwardz00_2625); } 
{ /* Ieee/input.scm 186 */

if(
(
(long)(BgL_curz00_2633)==10L))
{ /* Ieee/input.scm 186 */
 long BgL_arg2112z00_2635;
BgL_arg2112z00_2635 = 
(1L+BgL_forwardz00_2625); 
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_5111;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5112;
if(
INPUT_PORTP(BgL_iportz00_2623))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5112 = BgL_iportz00_2623; }  else 
{ 
 obj_t BgL_auxz00_8806;
BgL_auxz00_8806 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4275z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2623); 
FAILURE(BgL_auxz00_8806,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5112, BgL_arg2112z00_2635); } 
BgL_newzd2matchzd2_5111 = 1L; 
BgL_matchz00_2767 = BgL_newzd2matchzd2_5111; } }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_newzd2matchzd2_2628; } } } } }  else 
{ /* Ieee/input.scm 186 */
if(
(
(long)(BgL_curz00_2561)==10L))
{ /* Ieee/input.scm 186 */
 long BgL_arg2074z00_2565;
BgL_arg2074z00_2565 = 
(1L+BgL_forwardz00_2553); 
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_5044;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5045;
if(
INPUT_PORTP(BgL_iportz00_2551))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5045 = BgL_iportz00_2551; }  else 
{ 
 obj_t BgL_auxz00_8818;
BgL_auxz00_8818 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4271z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2551); 
FAILURE(BgL_auxz00_8818,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5045, BgL_arg2074z00_2565); } 
BgL_newzd2matchzd2_5044 = 0L; 
BgL_matchz00_2767 = BgL_newzd2matchzd2_5044; } }  else 
{ /* Ieee/input.scm 186 */
BgL_iportz00_2595 = BgL_iportz00_2551; 
BgL_lastzd2matchzd2_2596 = BgL_newzd2matchzd2_2556; 
BgL_forwardz00_2597 = 
(1L+BgL_forwardz00_2553); 
BgL_bufposz00_2598 = BgL_bufposz00_2554; 
BgL_zc3z04anonymousza32094ze3z87_2599:
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_2600;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5077;
if(
INPUT_PORTP(BgL_iportz00_2595))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5077 = BgL_iportz00_2595; }  else 
{ 
 obj_t BgL_auxz00_8825;
BgL_auxz00_8825 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4274z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2595); 
FAILURE(BgL_auxz00_8825,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5077, BgL_forwardz00_2597); } 
BgL_newzd2matchzd2_2600 = 2L; 
if(
(BgL_forwardz00_2597==BgL_bufposz00_2598))
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4763z00_8832;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5081;
if(
INPUT_PORTP(BgL_iportz00_2595))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5081 = BgL_iportz00_2595; }  else 
{ 
 obj_t BgL_auxz00_8835;
BgL_auxz00_8835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4274z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2595); 
FAILURE(BgL_auxz00_8835,BFALSE,BFALSE);} 
BgL_test4763z00_8832 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5081); } 
if(BgL_test4763z00_8832)
{ /* Ieee/input.scm 186 */
 long BgL_arg2097z00_2603; long BgL_arg2098z00_2604;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5082;
if(
INPUT_PORTP(BgL_iportz00_2595))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5082 = BgL_iportz00_2595; }  else 
{ 
 obj_t BgL_auxz00_8842;
BgL_auxz00_8842 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4274z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2595); 
FAILURE(BgL_auxz00_8842,BFALSE,BFALSE);} 
BgL_arg2097z00_2603 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5082); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5083;
if(
INPUT_PORTP(BgL_iportz00_2595))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5083 = BgL_iportz00_2595; }  else 
{ 
 obj_t BgL_auxz00_8849;
BgL_auxz00_8849 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4274z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2595); 
FAILURE(BgL_auxz00_8849,BFALSE,BFALSE);} 
BgL_arg2098z00_2604 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5083); } 
{ 
 long BgL_bufposz00_8855; long BgL_forwardz00_8854;
BgL_forwardz00_8854 = BgL_arg2097z00_2603; 
BgL_bufposz00_8855 = BgL_arg2098z00_2604; 
BgL_bufposz00_2598 = BgL_bufposz00_8855; 
BgL_forwardz00_2597 = BgL_forwardz00_8854; 
goto BgL_zc3z04anonymousza32094ze3z87_2599;} }  else 
{ /* Ieee/input.scm 186 */
BgL_matchz00_2767 = BgL_newzd2matchzd2_2600; } }  else 
{ /* Ieee/input.scm 186 */
 int BgL_curz00_2605;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5084;
if(
INPUT_PORTP(BgL_iportz00_2595))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5084 = BgL_iportz00_2595; }  else 
{ 
 obj_t BgL_auxz00_8858;
BgL_auxz00_8858 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4274z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2595); 
FAILURE(BgL_auxz00_8858,BFALSE,BFALSE);} 
BgL_curz00_2605 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5084, BgL_forwardz00_2597); } 
{ /* Ieee/input.scm 186 */

if(
(
(long)(BgL_curz00_2605)==13L))
{ 
 long BgL_bufposz00_8870; long BgL_forwardz00_8868; long BgL_lastzd2matchzd2_8867; obj_t BgL_iportz00_8866;
BgL_iportz00_8866 = BgL_iportz00_2595; 
BgL_lastzd2matchzd2_8867 = BgL_newzd2matchzd2_2600; 
BgL_forwardz00_8868 = 
(1L+BgL_forwardz00_2597); 
BgL_bufposz00_8870 = BgL_bufposz00_2598; 
BgL_bufposz00_2626 = BgL_bufposz00_8870; 
BgL_forwardz00_2625 = BgL_forwardz00_8868; 
BgL_lastzd2matchzd2_2624 = BgL_lastzd2matchzd2_8867; 
BgL_iportz00_2623 = BgL_iportz00_8866; 
goto BgL_zc3z04anonymousza32106ze3z87_2627;}  else 
{ /* Ieee/input.scm 186 */
if(
(
(long)(BgL_curz00_2605)==10L))
{ /* Ieee/input.scm 186 */
 long BgL_arg2102z00_2609;
BgL_arg2102z00_2609 = 
(1L+BgL_forwardz00_2597); 
{ /* Ieee/input.scm 186 */
 long BgL_newzd2matchzd2_5090;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5091;
if(
INPUT_PORTP(BgL_iportz00_2595))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5091 = BgL_iportz00_2595; }  else 
{ 
 obj_t BgL_auxz00_8877;
BgL_auxz00_8877 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4274z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2595); 
FAILURE(BgL_auxz00_8877,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5091, BgL_arg2102z00_2609); } 
BgL_newzd2matchzd2_5090 = 0L; 
BgL_matchz00_2767 = BgL_newzd2matchzd2_5090; } }  else 
{ 
 long BgL_forwardz00_8883; long BgL_lastzd2matchzd2_8882;
BgL_lastzd2matchzd2_8882 = BgL_newzd2matchzd2_2600; 
BgL_forwardz00_8883 = 
(1L+BgL_forwardz00_2597); 
BgL_forwardz00_2597 = BgL_forwardz00_8883; 
BgL_lastzd2matchzd2_2596 = BgL_lastzd2matchzd2_8882; 
goto BgL_zc3z04anonymousza32094ze3z87_2599;} } } } } } } } } } } } } } } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5149;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5149 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8889;
BgL_auxz00_8889 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8889,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_5149); } 
switch( BgL_matchz00_2767) { case 4L : 

{ /* Ieee/input.scm 186 */
 bool_t BgL_test4772z00_8894;
{ /* Ieee/input.scm 186 */
 long BgL_arg2196z00_2757;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5143;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5143 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8897;
BgL_auxz00_8897 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8897,BFALSE,BFALSE);} 
BgL_arg2196z00_2757 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5143); } 
BgL_test4772z00_8894 = 
(BgL_arg2196z00_2757==0L); } 
if(BgL_test4772z00_8894)
{ /* Ieee/input.scm 186 */
return BEOF;}  else 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5145;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5145 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8905;
BgL_auxz00_8905 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8905,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_5145));} } break;case 3L : 

return BGl_string4276z00zz__r4_input_6_10_2z00;break;case 2L : 

{ /* Ieee/input.scm 186 */
 long BgL_arg2121z00_5150;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5151;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5151 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8913;
BgL_auxz00_8913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8913,BFALSE,BFALSE);} 
BgL_arg2121z00_5150 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5151); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5152;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5152 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8920;
BgL_auxz00_8920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8920,BFALSE,BFALSE);} 
return 
rgc_buffer_substring(BgL_inputzd2portzd2_5152, 0L, BgL_arg2121z00_5150);} } break;case 1L : 

{ /* Ieee/input.scm 191 */
 long BgL_arg2203z00_2770;
{ /* Ieee/input.scm 191 */
 long BgL_arg2204z00_2771;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5154;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5154 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8927;
BgL_auxz00_8927 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8927,BFALSE,BFALSE);} 
BgL_arg2204z00_2771 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5154); } 
BgL_arg2203z00_2770 = 
(BgL_arg2204z00_2771-2L); } 
BgL_minz00_2660 = 
(int)(0L); 
BgL_maxz00_2661 = 
(int)(BgL_arg2203z00_2770); 
BgL_lambda2122z00_2662:
if(
(
(long)(BgL_maxz00_2661)<
(long)(BgL_minz00_2660)))
{ /* Ieee/input.scm 186 */
 long BgL_arg2124z00_2664;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5126;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5126 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8939;
BgL_auxz00_8939 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8939,BFALSE,BFALSE);} 
BgL_arg2124z00_2664 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5126); } 
{ /* Ieee/input.scm 186 */
 long BgL_za72za7_5128;
BgL_za72za7_5128 = 
(long)(BgL_maxz00_2661); 
BgL_maxz00_2661 = 
(int)(
(BgL_arg2124z00_2664+BgL_za72za7_5128)); } }  else 
{ /* Ieee/input.scm 186 */BFALSE; } 
{ /* Ieee/input.scm 186 */
 bool_t BgL_test4780z00_8947;
if(
(
(long)(BgL_minz00_2660)>=0L))
{ /* Ieee/input.scm 186 */
if(
(
(long)(BgL_maxz00_2661)>=
(long)(BgL_minz00_2660)))
{ /* Ieee/input.scm 186 */
 long BgL_arg2133z00_2675;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5132;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5132 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8957;
BgL_auxz00_8957 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8957,BFALSE,BFALSE);} 
BgL_arg2133z00_2675 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5132); } 
BgL_test4780z00_8947 = 
(
(long)(BgL_maxz00_2661)<=BgL_arg2133z00_2675); }  else 
{ /* Ieee/input.scm 186 */
BgL_test4780z00_8947 = ((bool_t)0)
; } }  else 
{ /* Ieee/input.scm 186 */
BgL_test4780z00_8947 = ((bool_t)0)
; } 
if(BgL_test4780z00_8947)
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5135; long BgL_startz00_5136; long BgL_stopz00_5137;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5135 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8966;
BgL_auxz00_8966 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8966,BFALSE,BFALSE);} 
BgL_startz00_5136 = 
(long)(BgL_minz00_2660); 
BgL_stopz00_5137 = 
(long)(BgL_maxz00_2661); 
return 
rgc_buffer_substring(BgL_inputzd2portzd2_5135, BgL_startz00_5136, BgL_stopz00_5137);}  else 
{ /* Ieee/input.scm 186 */
 obj_t BgL_arg2129z00_2669; obj_t BgL_arg2130z00_2670;
{ /* Ieee/input.scm 186 */
 obj_t BgL_arg2131z00_2671;
{ /* Ieee/input.scm 186 */
 long BgL_arg2121z00_5138;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5139;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5139 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8975;
BgL_auxz00_8975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8975,BFALSE,BFALSE);} 
BgL_arg2121z00_5138 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5139); } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5140;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5140 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_8982;
BgL_auxz00_8982 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_8982,BFALSE,BFALSE);} 
BgL_arg2131z00_2671 = 
rgc_buffer_substring(BgL_inputzd2portzd2_5140, 0L, BgL_arg2121z00_5138); } } 
{ /* Ieee/input.scm 186 */
 obj_t BgL_list2132z00_2672;
BgL_list2132z00_2672 = 
MAKE_YOUNG_PAIR(BgL_arg2131z00_2671, BNIL); 
BgL_arg2129z00_2669 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string4190z00zz__r4_input_6_10_2z00, BgL_list2132z00_2672); } } 
BgL_arg2130z00_2670 = 
MAKE_YOUNG_PAIR(
BINT(BgL_minz00_2660), 
BINT(BgL_maxz00_2661)); 
{ /* Ieee/input.scm 186 */
 obj_t BgL_aux3839z00_6393;
BgL_aux3839z00_6393 = 
BGl_errorz00zz__errorz00(BGl_string4189z00zz__r4_input_6_10_2z00, BgL_arg2129z00_2669, BgL_arg2130z00_2670); 
if(
STRINGP(BgL_aux3839z00_6393))
{ /* Ieee/input.scm 186 */
return BgL_aux3839z00_6393;}  else 
{ 
 obj_t BgL_auxz00_8995;
BgL_auxz00_8995 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4189z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_aux3839z00_6393); 
FAILURE(BgL_auxz00_8995,BFALSE,BFALSE);} } } } } break;case 0L : 

{ /* Ieee/input.scm 189 */
 long BgL_arg2205z00_2772;
{ /* Ieee/input.scm 189 */
 long BgL_arg2206z00_2773;
{ /* Ieee/input.scm 186 */
 obj_t BgL_inputzd2portzd2_5156;
if(
INPUT_PORTP(BgL_iportz00_2505))
{ /* Ieee/input.scm 186 */
BgL_inputzd2portzd2_5156 = BgL_iportz00_2505; }  else 
{ 
 obj_t BgL_auxz00_9003;
BgL_auxz00_9003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(8196L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2505); 
FAILURE(BgL_auxz00_9003,BFALSE,BFALSE);} 
BgL_arg2206z00_2773 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5156); } 
BgL_arg2205z00_2772 = 
(BgL_arg2206z00_2773-1L); } 
{ 
 int BgL_maxz00_9011; int BgL_minz00_9009;
BgL_minz00_9009 = 
(int)(0L); 
BgL_maxz00_9011 = 
(int)(BgL_arg2205z00_2772); 
BgL_maxz00_2661 = BgL_maxz00_9011; 
BgL_minz00_2660 = BgL_minz00_9009; 
goto BgL_lambda2122z00_2662;} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_2767));} } } } } }  else 
{ /* Ieee/input.scm 201 */
 obj_t BgL_g1080z00_2813; obj_t BgL_g1081z00_2814;
BgL_g1080z00_2813 = 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_26); 
{ /* Ieee/string.scm 172 */

BgL_g1081z00_2814 = 
make_string(100L, ((unsigned char)' ')); } 
{ 
 obj_t BgL_cz00_2816; long BgL_wz00_2817; long BgL_mz00_2818; obj_t BgL_accz00_2819;
BgL_cz00_2816 = BgL_g1080z00_2813; 
BgL_wz00_2817 = 0L; 
BgL_mz00_2818 = 100L; 
BgL_accz00_2819 = BgL_g1081z00_2814; 
BgL_zc3z04anonymousza32209ze3z87_2820:
if(
EOF_OBJECTP(BgL_cz00_2816))
{ /* Ieee/input.scm 206 */
if(
(BgL_wz00_2817==0L))
{ /* Ieee/input.scm 208 */
return BgL_cz00_2816;}  else 
{ /* Ieee/input.scm 208 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_accz00_2819, 0L, BgL_wz00_2817);} }  else 
{ /* Ieee/input.scm 206 */
if(
(BgL_wz00_2817==BgL_mz00_2818))
{ /* Ieee/input.scm 213 */
 long BgL_arg2213z00_2824; obj_t BgL_arg2214z00_2825;
BgL_arg2213z00_2824 = 
(BgL_mz00_2818*2L); 
{ /* Ieee/input.scm 214 */
 obj_t BgL_newzd2acczd2_2826;
{ /* Ieee/input.scm 214 */
 long BgL_arg2215z00_2827;
BgL_arg2215z00_2827 = 
(BgL_mz00_2818*2L); 
{ /* Ieee/string.scm 172 */

BgL_newzd2acczd2_2826 = 
make_string(BgL_arg2215z00_2827, ((unsigned char)' ')); } } 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_accz00_2819, 0L, BgL_newzd2acczd2_2826, 0L, BgL_mz00_2818); 
BgL_arg2214z00_2825 = BgL_newzd2acczd2_2826; } 
{ 
 obj_t BgL_accz00_9030; long BgL_mz00_9029;
BgL_mz00_9029 = BgL_arg2213z00_2824; 
BgL_accz00_9030 = BgL_arg2214z00_2825; 
BgL_accz00_2819 = BgL_accz00_9030; 
BgL_mz00_2818 = BgL_mz00_9029; 
goto BgL_zc3z04anonymousza32209ze3z87_2820;} }  else 
{ /* Ieee/input.scm 217 */
 bool_t BgL_test4792z00_9031;
{ /* Ieee/input.scm 217 */
 unsigned char BgL_char1z00_5173;
{ /* Ieee/input.scm 217 */
 obj_t BgL_tmpz00_9032;
if(
CHARP(BgL_cz00_2816))
{ /* Ieee/input.scm 217 */
BgL_tmpz00_9032 = BgL_cz00_2816
; }  else 
{ 
 obj_t BgL_auxz00_9035;
BgL_auxz00_9035 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9091L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_2816); 
FAILURE(BgL_auxz00_9035,BFALSE,BFALSE);} 
BgL_char1z00_5173 = 
CCHAR(BgL_tmpz00_9032); } 
BgL_test4792z00_9031 = 
(BgL_char1z00_5173==((unsigned char)13)); } 
if(BgL_test4792z00_9031)
{ /* Ieee/input.scm 218 */
 obj_t BgL_c2z00_2831;
BgL_c2z00_2831 = 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_26); 
{ /* Ieee/input.scm 219 */
 bool_t BgL_test4794z00_9042;
{ /* Ieee/input.scm 219 */
 unsigned char BgL_char1z00_5175;
{ /* Ieee/input.scm 219 */
 obj_t BgL_tmpz00_9043;
if(
CHARP(BgL_c2z00_2831))
{ /* Ieee/input.scm 219 */
BgL_tmpz00_9043 = BgL_c2z00_2831
; }  else 
{ 
 obj_t BgL_auxz00_9046;
BgL_auxz00_9046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9152L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_c2z00_2831); 
FAILURE(BgL_auxz00_9046,BFALSE,BFALSE);} 
BgL_char1z00_5175 = 
CCHAR(BgL_tmpz00_9043); } 
BgL_test4794z00_9042 = 
(BgL_char1z00_5175==((unsigned char)10)); } 
if(BgL_test4794z00_9042)
{ /* Ieee/input.scm 219 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_accz00_2819, 0L, BgL_wz00_2817);}  else 
{ /* Ieee/input.scm 219 */
{ /* Ieee/input.scm 222 */
 unsigned char BgL_charz00_5179;
{ /* Ieee/input.scm 222 */
 obj_t BgL_tmpz00_9053;
if(
CHARP(BgL_cz00_2816))
{ /* Ieee/input.scm 222 */
BgL_tmpz00_9053 = BgL_cz00_2816
; }  else 
{ 
 obj_t BgL_auxz00_9056;
BgL_auxz00_9056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9229L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_2816); 
FAILURE(BgL_auxz00_9056,BFALSE,BFALSE);} 
BgL_charz00_5179 = 
CCHAR(BgL_tmpz00_9053); } 
{ /* Ieee/input.scm 222 */
 long BgL_l3482z00_6030;
BgL_l3482z00_6030 = 
STRING_LENGTH(BgL_accz00_2819); 
if(
BOUND_CHECK(BgL_wz00_2817, BgL_l3482z00_6030))
{ /* Ieee/input.scm 222 */
STRING_SET(BgL_accz00_2819, BgL_wz00_2817, BgL_charz00_5179); }  else 
{ 
 obj_t BgL_auxz00_9065;
BgL_auxz00_9065 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9210L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_2819, 
(int)(BgL_l3482z00_6030), 
(int)(BgL_wz00_2817)); 
FAILURE(BgL_auxz00_9065,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_9072; obj_t BgL_cz00_9071;
BgL_cz00_9071 = BgL_c2z00_2831; 
BgL_wz00_9072 = 
(BgL_wz00_2817+1L); 
BgL_wz00_2817 = BgL_wz00_9072; 
BgL_cz00_2816 = BgL_cz00_9071; 
goto BgL_zc3z04anonymousza32209ze3z87_2820;} } } }  else 
{ /* Ieee/input.scm 224 */
 bool_t BgL_test4798z00_9074;
{ /* Ieee/input.scm 224 */
 unsigned char BgL_char1z00_5181;
{ /* Ieee/input.scm 224 */
 obj_t BgL_tmpz00_9075;
if(
CHARP(BgL_cz00_2816))
{ /* Ieee/input.scm 224 */
BgL_tmpz00_9075 = BgL_cz00_2816
; }  else 
{ 
 obj_t BgL_auxz00_9078;
BgL_auxz00_9078 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9280L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_2816); 
FAILURE(BgL_auxz00_9078,BFALSE,BFALSE);} 
BgL_char1z00_5181 = 
CCHAR(BgL_tmpz00_9075); } 
BgL_test4798z00_9074 = 
(BgL_char1z00_5181==((unsigned char)10)); } 
if(BgL_test4798z00_9074)
{ /* Ieee/input.scm 224 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_accz00_2819, 0L, BgL_wz00_2817);}  else 
{ /* Ieee/input.scm 224 */
{ /* Ieee/input.scm 229 */
 unsigned char BgL_charz00_5185;
{ /* Ieee/input.scm 229 */
 obj_t BgL_tmpz00_9085;
if(
CHARP(BgL_cz00_2816))
{ /* Ieee/input.scm 229 */
BgL_tmpz00_9085 = BgL_cz00_2816
; }  else 
{ 
 obj_t BgL_auxz00_9088;
BgL_auxz00_9088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9425L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_2816); 
FAILURE(BgL_auxz00_9088,BFALSE,BFALSE);} 
BgL_charz00_5185 = 
CCHAR(BgL_tmpz00_9085); } 
{ /* Ieee/input.scm 229 */
 long BgL_l3486z00_6034;
BgL_l3486z00_6034 = 
STRING_LENGTH(BgL_accz00_2819); 
if(
BOUND_CHECK(BgL_wz00_2817, BgL_l3486z00_6034))
{ /* Ieee/input.scm 229 */
STRING_SET(BgL_accz00_2819, BgL_wz00_2817, BgL_charz00_5185); }  else 
{ 
 obj_t BgL_auxz00_9097;
BgL_auxz00_9097 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9406L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_2819, 
(int)(BgL_l3486z00_6034), 
(int)(BgL_wz00_2817)); 
FAILURE(BgL_auxz00_9097,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_9105; obj_t BgL_cz00_9103;
BgL_cz00_9103 = 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_26); 
BgL_wz00_9105 = 
(BgL_wz00_2817+1L); 
BgL_wz00_2817 = BgL_wz00_9105; 
BgL_cz00_2816 = BgL_cz00_9103; 
goto BgL_zc3z04anonymousza32209ze3z87_2820;} } } } } } } } } 

}



/* _read-line-newline */
obj_t BGl__readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t BgL_env1271z00_31, obj_t BgL_opt1270z00_30)
{
{ /* Ieee/input.scm 235 */
{ /* Ieee/input.scm 235 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1270z00_30)) { case 0L : 

{ /* Ieee/input.scm 235 */
 obj_t BgL_ipz00_2843;
{ /* Ieee/input.scm 235 */
 obj_t BgL_tmpz00_9107;
BgL_tmpz00_9107 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_2843 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9107); } 
{ /* Ieee/input.scm 235 */

return 
BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00(BgL_ipz00_2843);} } break;case 1L : 

{ /* Ieee/input.scm 235 */
 obj_t BgL_ipz00_2844;
BgL_ipz00_2844 = 
VECTOR_REF(BgL_opt1270z00_30,0L); 
{ /* Ieee/input.scm 235 */

return 
BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00(BgL_ipz00_2844);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4280z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1270z00_30)));} } } } 

}



/* read-line-newline */
BGL_EXPORTED_DEF obj_t BGl_readzd2linezd2newlinez00zz__r4_input_6_10_2z00(obj_t BgL_ipz00_29)
{
{ /* Ieee/input.scm 235 */
{ /* Ieee/input.scm 236 */
 bool_t BgL_test4802z00_9118;
{ /* Ieee/input.scm 236 */
 long BgL_arg2388z00_3170;
{ /* Ieee/input.scm 236 */
 obj_t BgL_tmpz00_9119;
if(
INPUT_PORTP(BgL_ipz00_29))
{ /* Ieee/input.scm 236 */
BgL_tmpz00_9119 = BgL_ipz00_29
; }  else 
{ 
 obj_t BgL_auxz00_9122;
BgL_auxz00_9122 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9799L), BGl_string4281z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_29); 
FAILURE(BgL_auxz00_9122,BFALSE,BFALSE);} 
BgL_arg2388z00_3170 = 
BGL_INPUT_PORT_BUFSIZ(BgL_tmpz00_9119); } 
BgL_test4802z00_9118 = 
(BgL_arg2388z00_3170>2L); } 
if(BgL_test4802z00_9118)
{ 
 obj_t BgL_iportz00_2850;
BgL_iportz00_2850 = BgL_ipz00_29; 
{ 
 obj_t BgL_iportz00_2960; long BgL_lastzd2matchzd2_2961; long BgL_forwardz00_2962; long BgL_bufposz00_2963; obj_t BgL_iportz00_2944; long BgL_lastzd2matchzd2_2945; long BgL_forwardz00_2946; long BgL_bufposz00_2947; obj_t BgL_iportz00_2929; long BgL_lastzd2matchzd2_2930; long BgL_forwardz00_2931; long BgL_bufposz00_2932; obj_t BgL_iportz00_2916; long BgL_lastzd2matchzd2_2917; long BgL_forwardz00_2918; long BgL_bufposz00_2919; obj_t BgL_iportz00_2894; long BgL_lastzd2matchzd2_2895; long BgL_forwardz00_2896; long BgL_bufposz00_2897;
{ /* Ieee/input.scm 237 */
 bool_t BgL_test4804z00_9128;
{ /* Ieee/input.scm 237 */
 obj_t BgL_arg2231z00_2893;
{ /* Ieee/input.scm 237 */
 obj_t BgL_res3468z00_5288;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_res3468z00_5288 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9131;
BgL_auxz00_9131 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9131,BFALSE,BFALSE);} 
BgL_arg2231z00_2893 = BgL_res3468z00_5288; } 
BgL_test4804z00_9128 = 
INPUT_PORT_CLOSEP(BgL_arg2231z00_2893); } 
if(BgL_test4804z00_9128)
{ /* Ieee/input.scm 237 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2227z00_2887;
{ /* Ieee/input.scm 237 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_2888;
{ /* Ieee/input.scm 237 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_2891;
BgL_new1045z00_2891 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 237 */
 long BgL_arg2230z00_2892;
BgL_arg2230z00_2892 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_2891), BgL_arg2230z00_2892); } 
BgL_new1046z00_2888 = BgL_new1045z00_2891; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2888)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2888)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_9144;
{ /* Ieee/input.scm 237 */
 obj_t BgL_arg2228z00_2889;
{ /* Ieee/input.scm 237 */
 obj_t BgL_arg2229z00_2890;
{ /* Ieee/input.scm 237 */
 obj_t BgL_classz00_5293;
BgL_classz00_5293 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2229z00_2890 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5293); } 
BgL_arg2228z00_2889 = 
VECTOR_REF(BgL_arg2229z00_2890,2L); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_auxz00_9148;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2228z00_2889))
{ /* Ieee/input.scm 237 */
BgL_auxz00_9148 = BgL_arg2228z00_2889
; }  else 
{ 
 obj_t BgL_auxz00_9151;
BgL_auxz00_9151 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2228z00_2889); 
FAILURE(BgL_auxz00_9151,BFALSE,BFALSE);} 
BgL_auxz00_9144 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_9148); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_2888)))->BgL_stackz00)=((obj_t)BgL_auxz00_9144),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2888)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2888)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_9161;
{ /* Ieee/input.scm 237 */
 obj_t BgL_res3469z00_5295;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_res3469z00_5295 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9165;
BgL_auxz00_9165 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4210z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9165,BFALSE,BFALSE);} 
BgL_auxz00_9161 = BgL_res3469z00_5295; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_2888)))->BgL_objz00)=((obj_t)BgL_auxz00_9161),BUNSPEC); } 
BgL_arg2227z00_2887 = BgL_new1046z00_2888; } 
BGL_TAIL return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2227z00_2887));}  else 
{ /* Ieee/input.scm 237 */
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5280;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5280 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9174;
BgL_auxz00_9174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9174,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_5280); } 
{ /* Ieee/input.scm 237 */
 long BgL_matchz00_3098;
{ /* Ieee/input.scm 237 */
 long BgL_arg2368z00_3101; long BgL_arg2369z00_3102;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5281;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5281 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9181;
BgL_auxz00_9181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9181,BFALSE,BFALSE);} 
BgL_arg2368z00_3101 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5281); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5282;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5282 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9188;
BgL_auxz00_9188 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9188,BFALSE,BFALSE);} 
BgL_arg2369z00_3102 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5282); } 
BgL_iportz00_2929 = BgL_iportz00_2850; 
BgL_lastzd2matchzd2_2930 = 1L; 
BgL_forwardz00_2931 = BgL_arg2368z00_3101; 
BgL_bufposz00_2932 = BgL_arg2369z00_3102; 
BgL_zc3z04anonymousza32250ze3z87_2933:
if(
(BgL_forwardz00_2931==BgL_bufposz00_2932))
{ /* Ieee/input.scm 237 */
 bool_t BgL_test4812z00_9195;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5225;
if(
INPUT_PORTP(BgL_iportz00_2929))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5225 = BgL_iportz00_2929; }  else 
{ 
 obj_t BgL_auxz00_9198;
BgL_auxz00_9198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4284z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2929); 
FAILURE(BgL_auxz00_9198,BFALSE,BFALSE);} 
BgL_test4812z00_9195 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5225); } 
if(BgL_test4812z00_9195)
{ /* Ieee/input.scm 237 */
 long BgL_arg2253z00_2936; long BgL_arg2254z00_2937;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5226;
if(
INPUT_PORTP(BgL_iportz00_2929))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5226 = BgL_iportz00_2929; }  else 
{ 
 obj_t BgL_auxz00_9205;
BgL_auxz00_9205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4284z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2929); 
FAILURE(BgL_auxz00_9205,BFALSE,BFALSE);} 
BgL_arg2253z00_2936 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5226); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5227;
if(
INPUT_PORTP(BgL_iportz00_2929))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5227 = BgL_iportz00_2929; }  else 
{ 
 obj_t BgL_auxz00_9212;
BgL_auxz00_9212 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4284z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2929); 
FAILURE(BgL_auxz00_9212,BFALSE,BFALSE);} 
BgL_arg2254z00_2937 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5227); } 
{ 
 long BgL_bufposz00_9218; long BgL_forwardz00_9217;
BgL_forwardz00_9217 = BgL_arg2253z00_2936; 
BgL_bufposz00_9218 = BgL_arg2254z00_2937; 
BgL_bufposz00_2932 = BgL_bufposz00_9218; 
BgL_forwardz00_2931 = BgL_forwardz00_9217; 
goto BgL_zc3z04anonymousza32250ze3z87_2933;} }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_lastzd2matchzd2_2930; } }  else 
{ /* Ieee/input.scm 237 */
 int BgL_curz00_2938;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5228;
if(
INPUT_PORTP(BgL_iportz00_2929))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5228 = BgL_iportz00_2929; }  else 
{ 
 obj_t BgL_auxz00_9221;
BgL_auxz00_9221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4284z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2929); 
FAILURE(BgL_auxz00_9221,BFALSE,BFALSE);} 
BgL_curz00_2938 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5228, BgL_forwardz00_2931); } 
{ /* Ieee/input.scm 237 */

if(
(
(long)(BgL_curz00_2938)==13L))
{ /* Ieee/input.scm 237 */
BgL_iportz00_2916 = BgL_iportz00_2929; 
BgL_lastzd2matchzd2_2917 = BgL_lastzd2matchzd2_2930; 
BgL_forwardz00_2918 = 
(1L+BgL_forwardz00_2931); 
BgL_bufposz00_2919 = BgL_bufposz00_2932; 
BgL_zc3z04anonymousza32243ze3z87_2920:
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_2921;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5209;
if(
INPUT_PORTP(BgL_iportz00_2916))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5209 = BgL_iportz00_2916; }  else 
{ 
 obj_t BgL_auxz00_9231;
BgL_auxz00_9231 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4283z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2916); 
FAILURE(BgL_auxz00_9231,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5209, BgL_forwardz00_2918); } 
BgL_newzd2matchzd2_2921 = 0L; 
if(
(BgL_forwardz00_2918==BgL_bufposz00_2919))
{ /* Ieee/input.scm 237 */
 bool_t BgL_test4820z00_9238;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5213;
if(
INPUT_PORTP(BgL_iportz00_2916))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5213 = BgL_iportz00_2916; }  else 
{ 
 obj_t BgL_auxz00_9241;
BgL_auxz00_9241 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4283z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2916); 
FAILURE(BgL_auxz00_9241,BFALSE,BFALSE);} 
BgL_test4820z00_9238 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5213); } 
if(BgL_test4820z00_9238)
{ /* Ieee/input.scm 237 */
 long BgL_arg2246z00_2924; long BgL_arg2247z00_2925;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5214;
if(
INPUT_PORTP(BgL_iportz00_2916))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5214 = BgL_iportz00_2916; }  else 
{ 
 obj_t BgL_auxz00_9248;
BgL_auxz00_9248 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4283z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2916); 
FAILURE(BgL_auxz00_9248,BFALSE,BFALSE);} 
BgL_arg2246z00_2924 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5214); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5215;
if(
INPUT_PORTP(BgL_iportz00_2916))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5215 = BgL_iportz00_2916; }  else 
{ 
 obj_t BgL_auxz00_9255;
BgL_auxz00_9255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4283z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2916); 
FAILURE(BgL_auxz00_9255,BFALSE,BFALSE);} 
BgL_arg2247z00_2925 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5215); } 
{ 
 long BgL_bufposz00_9261; long BgL_forwardz00_9260;
BgL_forwardz00_9260 = BgL_arg2246z00_2924; 
BgL_bufposz00_9261 = BgL_arg2247z00_2925; 
BgL_bufposz00_2919 = BgL_bufposz00_9261; 
BgL_forwardz00_2918 = BgL_forwardz00_9260; 
goto BgL_zc3z04anonymousza32243ze3z87_2920;} }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_newzd2matchzd2_2921; } }  else 
{ /* Ieee/input.scm 237 */
 int BgL_curz00_2926;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5216;
if(
INPUT_PORTP(BgL_iportz00_2916))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5216 = BgL_iportz00_2916; }  else 
{ 
 obj_t BgL_auxz00_9264;
BgL_auxz00_9264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4283z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2916); 
FAILURE(BgL_auxz00_9264,BFALSE,BFALSE);} 
BgL_curz00_2926 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5216, BgL_forwardz00_2918); } 
{ /* Ieee/input.scm 237 */

if(
(
(long)(BgL_curz00_2926)==10L))
{ /* Ieee/input.scm 237 */
 long BgL_arg2249z00_2928;
BgL_arg2249z00_2928 = 
(1L+BgL_forwardz00_2918); 
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_5220;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5221;
if(
INPUT_PORTP(BgL_iportz00_2916))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5221 = BgL_iportz00_2916; }  else 
{ 
 obj_t BgL_auxz00_9275;
BgL_auxz00_9275 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4283z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2916); 
FAILURE(BgL_auxz00_9275,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5221, BgL_arg2249z00_2928); } 
BgL_newzd2matchzd2_5220 = 0L; 
BgL_matchz00_3098 = BgL_newzd2matchzd2_5220; } }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_newzd2matchzd2_2921; } } } } }  else 
{ /* Ieee/input.scm 237 */
if(
(
(long)(BgL_curz00_2938)==10L))
{ /* Ieee/input.scm 237 */
 long BgL_arg2258z00_2942;
BgL_arg2258z00_2942 = 
(1L+BgL_forwardz00_2931); 
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_5234;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5235;
if(
INPUT_PORTP(BgL_iportz00_2929))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5235 = BgL_iportz00_2929; }  else 
{ 
 obj_t BgL_auxz00_9287;
BgL_auxz00_9287 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4284z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2929); 
FAILURE(BgL_auxz00_9287,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5235, BgL_arg2258z00_2942); } 
BgL_newzd2matchzd2_5234 = 0L; 
BgL_matchz00_3098 = BgL_newzd2matchzd2_5234; } }  else 
{ /* Ieee/input.scm 237 */
BgL_iportz00_2944 = BgL_iportz00_2929; 
BgL_lastzd2matchzd2_2945 = BgL_lastzd2matchzd2_2930; 
BgL_forwardz00_2946 = 
(1L+BgL_forwardz00_2931); 
BgL_bufposz00_2947 = BgL_bufposz00_2932; 
BgL_zc3z04anonymousza32260ze3z87_2948:
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_2949;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5238;
if(
INPUT_PORTP(BgL_iportz00_2944))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5238 = BgL_iportz00_2944; }  else 
{ 
 obj_t BgL_auxz00_9294;
BgL_auxz00_9294 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4285z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2944); 
FAILURE(BgL_auxz00_9294,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5238, BgL_forwardz00_2946); } 
BgL_newzd2matchzd2_2949 = 0L; 
if(
(BgL_forwardz00_2946==BgL_bufposz00_2947))
{ /* Ieee/input.scm 237 */
 bool_t BgL_test4831z00_9301;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5242;
if(
INPUT_PORTP(BgL_iportz00_2944))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5242 = BgL_iportz00_2944; }  else 
{ 
 obj_t BgL_auxz00_9304;
BgL_auxz00_9304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4285z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2944); 
FAILURE(BgL_auxz00_9304,BFALSE,BFALSE);} 
BgL_test4831z00_9301 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5242); } 
if(BgL_test4831z00_9301)
{ /* Ieee/input.scm 237 */
 long BgL_arg2263z00_2952; long BgL_arg2264z00_2953;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5243;
if(
INPUT_PORTP(BgL_iportz00_2944))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5243 = BgL_iportz00_2944; }  else 
{ 
 obj_t BgL_auxz00_9311;
BgL_auxz00_9311 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4285z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2944); 
FAILURE(BgL_auxz00_9311,BFALSE,BFALSE);} 
BgL_arg2263z00_2952 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5243); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5244;
if(
INPUT_PORTP(BgL_iportz00_2944))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5244 = BgL_iportz00_2944; }  else 
{ 
 obj_t BgL_auxz00_9318;
BgL_auxz00_9318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4285z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2944); 
FAILURE(BgL_auxz00_9318,BFALSE,BFALSE);} 
BgL_arg2264z00_2953 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5244); } 
{ 
 long BgL_bufposz00_9324; long BgL_forwardz00_9323;
BgL_forwardz00_9323 = BgL_arg2263z00_2952; 
BgL_bufposz00_9324 = BgL_arg2264z00_2953; 
BgL_bufposz00_2947 = BgL_bufposz00_9324; 
BgL_forwardz00_2946 = BgL_forwardz00_9323; 
goto BgL_zc3z04anonymousza32260ze3z87_2948;} }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_newzd2matchzd2_2949; } }  else 
{ /* Ieee/input.scm 237 */
 int BgL_curz00_2954;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5245;
if(
INPUT_PORTP(BgL_iportz00_2944))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5245 = BgL_iportz00_2944; }  else 
{ 
 obj_t BgL_auxz00_9327;
BgL_auxz00_9327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4285z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2944); 
FAILURE(BgL_auxz00_9327,BFALSE,BFALSE);} 
BgL_curz00_2954 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5245, BgL_forwardz00_2946); } 
{ /* Ieee/input.scm 237 */

if(
(
(long)(BgL_curz00_2954)==13L))
{ /* Ieee/input.scm 237 */
BgL_iportz00_2960 = BgL_iportz00_2944; 
BgL_lastzd2matchzd2_2961 = BgL_newzd2matchzd2_2949; 
BgL_forwardz00_2962 = 
(1L+BgL_forwardz00_2946); 
BgL_bufposz00_2963 = BgL_bufposz00_2947; 
BgL_zc3z04anonymousza32270ze3z87_2964:
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_2965;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5255;
if(
INPUT_PORTP(BgL_iportz00_2960))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5255 = BgL_iportz00_2960; }  else 
{ 
 obj_t BgL_auxz00_9337;
BgL_auxz00_9337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4286z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2960); 
FAILURE(BgL_auxz00_9337,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5255, BgL_forwardz00_2962); } 
BgL_newzd2matchzd2_2965 = 0L; 
if(
(BgL_forwardz00_2962==BgL_bufposz00_2963))
{ /* Ieee/input.scm 237 */
 bool_t BgL_test4839z00_9344;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5259;
if(
INPUT_PORTP(BgL_iportz00_2960))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5259 = BgL_iportz00_2960; }  else 
{ 
 obj_t BgL_auxz00_9347;
BgL_auxz00_9347 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4286z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2960); 
FAILURE(BgL_auxz00_9347,BFALSE,BFALSE);} 
BgL_test4839z00_9344 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5259); } 
if(BgL_test4839z00_9344)
{ /* Ieee/input.scm 237 */
 long BgL_arg2273z00_2968; long BgL_arg2274z00_2969;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5260;
if(
INPUT_PORTP(BgL_iportz00_2960))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5260 = BgL_iportz00_2960; }  else 
{ 
 obj_t BgL_auxz00_9354;
BgL_auxz00_9354 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4286z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2960); 
FAILURE(BgL_auxz00_9354,BFALSE,BFALSE);} 
BgL_arg2273z00_2968 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5260); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5261;
if(
INPUT_PORTP(BgL_iportz00_2960))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5261 = BgL_iportz00_2960; }  else 
{ 
 obj_t BgL_auxz00_9361;
BgL_auxz00_9361 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4286z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2960); 
FAILURE(BgL_auxz00_9361,BFALSE,BFALSE);} 
BgL_arg2274z00_2969 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5261); } 
{ 
 long BgL_bufposz00_9367; long BgL_forwardz00_9366;
BgL_forwardz00_9366 = BgL_arg2273z00_2968; 
BgL_bufposz00_9367 = BgL_arg2274z00_2969; 
BgL_bufposz00_2963 = BgL_bufposz00_9367; 
BgL_forwardz00_2962 = BgL_forwardz00_9366; 
goto BgL_zc3z04anonymousza32270ze3z87_2964;} }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_newzd2matchzd2_2965; } }  else 
{ /* Ieee/input.scm 237 */
 int BgL_curz00_2970;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5262;
if(
INPUT_PORTP(BgL_iportz00_2960))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5262 = BgL_iportz00_2960; }  else 
{ 
 obj_t BgL_auxz00_9370;
BgL_auxz00_9370 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4286z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2960); 
FAILURE(BgL_auxz00_9370,BFALSE,BFALSE);} 
BgL_curz00_2970 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5262, BgL_forwardz00_2962); } 
{ /* Ieee/input.scm 237 */

if(
(
(long)(BgL_curz00_2970)==10L))
{ /* Ieee/input.scm 237 */
 long BgL_arg2276z00_2972;
BgL_arg2276z00_2972 = 
(1L+BgL_forwardz00_2962); 
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_5266;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5267;
if(
INPUT_PORTP(BgL_iportz00_2960))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5267 = BgL_iportz00_2960; }  else 
{ 
 obj_t BgL_auxz00_9381;
BgL_auxz00_9381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4286z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2960); 
FAILURE(BgL_auxz00_9381,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5267, BgL_arg2276z00_2972); } 
BgL_newzd2matchzd2_5266 = 0L; 
BgL_matchz00_3098 = BgL_newzd2matchzd2_5266; } }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_newzd2matchzd2_2965; } } } } }  else 
{ /* Ieee/input.scm 237 */
if(
(
(long)(BgL_curz00_2954)==10L))
{ /* Ieee/input.scm 237 */
 long BgL_arg2268z00_2958;
BgL_arg2268z00_2958 = 
(1L+BgL_forwardz00_2946); 
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_5251;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5252;
if(
INPUT_PORTP(BgL_iportz00_2944))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5252 = BgL_iportz00_2944; }  else 
{ 
 obj_t BgL_auxz00_9393;
BgL_auxz00_9393 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4285z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2944); 
FAILURE(BgL_auxz00_9393,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5252, BgL_arg2268z00_2958); } 
BgL_newzd2matchzd2_5251 = 0L; 
BgL_matchz00_3098 = BgL_newzd2matchzd2_5251; } }  else 
{ /* Ieee/input.scm 237 */
BgL_iportz00_2894 = BgL_iportz00_2944; 
BgL_lastzd2matchzd2_2895 = BgL_newzd2matchzd2_2949; 
BgL_forwardz00_2896 = 
(1L+BgL_forwardz00_2946); 
BgL_bufposz00_2897 = BgL_bufposz00_2947; 
BgL_zc3z04anonymousza32232ze3z87_2898:
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_2899;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5189;
if(
INPUT_PORTP(BgL_iportz00_2894))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5189 = BgL_iportz00_2894; }  else 
{ 
 obj_t BgL_auxz00_9400;
BgL_auxz00_9400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4282z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2894); 
FAILURE(BgL_auxz00_9400,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5189, BgL_forwardz00_2896); } 
BgL_newzd2matchzd2_2899 = 0L; 
if(
(BgL_forwardz00_2896==BgL_bufposz00_2897))
{ /* Ieee/input.scm 237 */
 bool_t BgL_test4850z00_9407;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5193;
if(
INPUT_PORTP(BgL_iportz00_2894))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5193 = BgL_iportz00_2894; }  else 
{ 
 obj_t BgL_auxz00_9410;
BgL_auxz00_9410 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4282z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2894); 
FAILURE(BgL_auxz00_9410,BFALSE,BFALSE);} 
BgL_test4850z00_9407 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5193); } 
if(BgL_test4850z00_9407)
{ /* Ieee/input.scm 237 */
 long BgL_arg2235z00_2902; long BgL_arg2236z00_2903;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5194;
if(
INPUT_PORTP(BgL_iportz00_2894))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5194 = BgL_iportz00_2894; }  else 
{ 
 obj_t BgL_auxz00_9417;
BgL_auxz00_9417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4282z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2894); 
FAILURE(BgL_auxz00_9417,BFALSE,BFALSE);} 
BgL_arg2235z00_2902 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5194); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5195;
if(
INPUT_PORTP(BgL_iportz00_2894))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5195 = BgL_iportz00_2894; }  else 
{ 
 obj_t BgL_auxz00_9424;
BgL_auxz00_9424 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4282z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2894); 
FAILURE(BgL_auxz00_9424,BFALSE,BFALSE);} 
BgL_arg2236z00_2903 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5195); } 
{ 
 long BgL_bufposz00_9430; long BgL_forwardz00_9429;
BgL_forwardz00_9429 = BgL_arg2235z00_2902; 
BgL_bufposz00_9430 = BgL_arg2236z00_2903; 
BgL_bufposz00_2897 = BgL_bufposz00_9430; 
BgL_forwardz00_2896 = BgL_forwardz00_9429; 
goto BgL_zc3z04anonymousza32232ze3z87_2898;} }  else 
{ /* Ieee/input.scm 237 */
BgL_matchz00_3098 = BgL_newzd2matchzd2_2899; } }  else 
{ /* Ieee/input.scm 237 */
 int BgL_curz00_2904;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5196;
if(
INPUT_PORTP(BgL_iportz00_2894))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5196 = BgL_iportz00_2894; }  else 
{ 
 obj_t BgL_auxz00_9433;
BgL_auxz00_9433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4282z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2894); 
FAILURE(BgL_auxz00_9433,BFALSE,BFALSE);} 
BgL_curz00_2904 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5196, BgL_forwardz00_2896); } 
{ /* Ieee/input.scm 237 */

if(
(
(long)(BgL_curz00_2904)==13L))
{ 
 long BgL_bufposz00_9445; long BgL_forwardz00_9443; long BgL_lastzd2matchzd2_9442; obj_t BgL_iportz00_9441;
BgL_iportz00_9441 = BgL_iportz00_2894; 
BgL_lastzd2matchzd2_9442 = BgL_newzd2matchzd2_2899; 
BgL_forwardz00_9443 = 
(1L+BgL_forwardz00_2896); 
BgL_bufposz00_9445 = BgL_bufposz00_2897; 
BgL_bufposz00_2963 = BgL_bufposz00_9445; 
BgL_forwardz00_2962 = BgL_forwardz00_9443; 
BgL_lastzd2matchzd2_2961 = BgL_lastzd2matchzd2_9442; 
BgL_iportz00_2960 = BgL_iportz00_9441; 
goto BgL_zc3z04anonymousza32270ze3z87_2964;}  else 
{ /* Ieee/input.scm 237 */
if(
(
(long)(BgL_curz00_2904)==10L))
{ /* Ieee/input.scm 237 */
 long BgL_arg2240z00_2908;
BgL_arg2240z00_2908 = 
(1L+BgL_forwardz00_2896); 
{ /* Ieee/input.scm 237 */
 long BgL_newzd2matchzd2_5202;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5203;
if(
INPUT_PORTP(BgL_iportz00_2894))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5203 = BgL_iportz00_2894; }  else 
{ 
 obj_t BgL_auxz00_9452;
BgL_auxz00_9452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4282z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2894); 
FAILURE(BgL_auxz00_9452,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5203, BgL_arg2240z00_2908); } 
BgL_newzd2matchzd2_5202 = 0L; 
BgL_matchz00_3098 = BgL_newzd2matchzd2_5202; } }  else 
{ 
 long BgL_forwardz00_9458; long BgL_lastzd2matchzd2_9457;
BgL_lastzd2matchzd2_9457 = BgL_newzd2matchzd2_2899; 
BgL_forwardz00_9458 = 
(1L+BgL_forwardz00_2896); 
BgL_forwardz00_2896 = BgL_forwardz00_9458; 
BgL_lastzd2matchzd2_2895 = BgL_lastzd2matchzd2_9457; 
goto BgL_zc3z04anonymousza32232ze3z87_2898;} } } } } } } } } } } } } } } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5283;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5283 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9464;
BgL_auxz00_9464 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9464,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_5283); } 
switch( BgL_matchz00_3098) { case 1L : 

{ /* Ieee/input.scm 237 */
 bool_t BgL_test4859z00_9469;
{ /* Ieee/input.scm 237 */
 long BgL_arg2361z00_3088;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5277;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5277 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9472;
BgL_auxz00_9472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9472,BFALSE,BFALSE);} 
BgL_arg2361z00_3088 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5277); } 
BgL_test4859z00_9469 = 
(BgL_arg2361z00_3088==0L); } 
if(BgL_test4859z00_9469)
{ /* Ieee/input.scm 237 */
return BEOF;}  else 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5279;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5279 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9480;
BgL_auxz00_9480 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4188z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9480,BFALSE,BFALSE);} 
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_5279));} } break;case 0L : 

{ /* Ieee/input.scm 237 */
 long BgL_arg2284z00_5284;
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5285;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5285 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9488;
BgL_auxz00_9488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9488,BFALSE,BFALSE);} 
BgL_arg2284z00_5284 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5285); } 
{ /* Ieee/input.scm 237 */
 obj_t BgL_inputzd2portzd2_5286;
if(
INPUT_PORTP(BgL_iportz00_2850))
{ /* Ieee/input.scm 237 */
BgL_inputzd2portzd2_5286 = BgL_iportz00_2850; }  else 
{ 
 obj_t BgL_auxz00_9495;
BgL_auxz00_9495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(9828L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_2850); 
FAILURE(BgL_auxz00_9495,BFALSE,BFALSE);} 
return 
rgc_buffer_substring(BgL_inputzd2portzd2_5286, 0L, BgL_arg2284z00_5284);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_3098));} } } } } }  else 
{ /* Ieee/input.scm 249 */
 obj_t BgL_g1093z00_3138; obj_t BgL_g1094z00_3139;
BgL_g1093z00_3138 = 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_29); 
{ /* Ieee/string.scm 172 */

BgL_g1094z00_3139 = 
make_string(100L, ((unsigned char)' ')); } 
{ 
 obj_t BgL_cz00_3141; long BgL_wz00_3142; long BgL_mz00_3143; obj_t BgL_accz00_3144;
BgL_cz00_3141 = BgL_g1093z00_3138; 
BgL_wz00_3142 = 0L; 
BgL_mz00_3143 = 100L; 
BgL_accz00_3144 = BgL_g1094z00_3139; 
BgL_zc3z04anonymousza32370ze3z87_3145:
if(
EOF_OBJECTP(BgL_cz00_3141))
{ /* Ieee/input.scm 254 */
if(
(BgL_wz00_3142==0L))
{ /* Ieee/input.scm 256 */
return BgL_cz00_3141;}  else 
{ /* Ieee/input.scm 256 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_accz00_3144, 0L, BgL_wz00_3142);} }  else 
{ /* Ieee/input.scm 254 */
if(
(BgL_wz00_3142==
(BgL_mz00_3143-2L)))
{ /* Ieee/input.scm 261 */
 long BgL_arg2375z00_3150; obj_t BgL_arg2376z00_3151;
BgL_arg2375z00_3150 = 
(BgL_mz00_3143*2L); 
{ /* Ieee/input.scm 262 */
 obj_t BgL_newzd2acczd2_3152;
{ /* Ieee/input.scm 262 */
 long BgL_arg2377z00_3153;
BgL_arg2377z00_3153 = 
(BgL_mz00_3143*2L); 
{ /* Ieee/string.scm 172 */

BgL_newzd2acczd2_3152 = 
make_string(BgL_arg2377z00_3153, ((unsigned char)' ')); } } 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_accz00_3144, 0L, BgL_newzd2acczd2_3152, 0L, BgL_mz00_3143); 
BgL_arg2376z00_3151 = BgL_newzd2acczd2_3152; } 
{ 
 obj_t BgL_accz00_9518; long BgL_mz00_9517;
BgL_mz00_9517 = BgL_arg2375z00_3150; 
BgL_accz00_9518 = BgL_arg2376z00_3151; 
BgL_accz00_3144 = BgL_accz00_9518; 
BgL_mz00_3143 = BgL_mz00_9517; 
goto BgL_zc3z04anonymousza32370ze3z87_3145;} }  else 
{ /* Ieee/input.scm 265 */
 bool_t BgL_test4867z00_9519;
{ /* Ieee/input.scm 265 */
 unsigned char BgL_char1z00_5304;
{ /* Ieee/input.scm 265 */
 obj_t BgL_tmpz00_9520;
if(
CHARP(BgL_cz00_3141))
{ /* Ieee/input.scm 265 */
BgL_tmpz00_9520 = BgL_cz00_3141
; }  else 
{ 
 obj_t BgL_auxz00_9523;
BgL_auxz00_9523 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10651L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_3141); 
FAILURE(BgL_auxz00_9523,BFALSE,BFALSE);} 
BgL_char1z00_5304 = 
CCHAR(BgL_tmpz00_9520); } 
BgL_test4867z00_9519 = 
(BgL_char1z00_5304==((unsigned char)13)); } 
if(BgL_test4867z00_9519)
{ /* Ieee/input.scm 266 */
 obj_t BgL_c2z00_3157;
BgL_c2z00_3157 = 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_29); 
{ /* Ieee/input.scm 267 */
 bool_t BgL_test4869z00_9530;
{ /* Ieee/input.scm 267 */
 unsigned char BgL_char1z00_5306;
{ /* Ieee/input.scm 267 */
 obj_t BgL_tmpz00_9531;
if(
CHARP(BgL_c2z00_3157))
{ /* Ieee/input.scm 267 */
BgL_tmpz00_9531 = BgL_c2z00_3157
; }  else 
{ 
 obj_t BgL_auxz00_9534;
BgL_auxz00_9534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10712L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_c2z00_3157); 
FAILURE(BgL_auxz00_9534,BFALSE,BFALSE);} 
BgL_char1z00_5306 = 
CCHAR(BgL_tmpz00_9531); } 
BgL_test4869z00_9530 = 
(BgL_char1z00_5306==((unsigned char)10)); } 
if(BgL_test4869z00_9530)
{ /* Ieee/input.scm 267 */
{ /* Ieee/input.scm 269 */
 long BgL_l3490z00_6038;
BgL_l3490z00_6038 = 
STRING_LENGTH(BgL_accz00_3144); 
if(
BOUND_CHECK(BgL_wz00_3142, BgL_l3490z00_6038))
{ /* Ieee/input.scm 269 */
STRING_SET(BgL_accz00_3144, BgL_wz00_3142, ((unsigned char)13)); }  else 
{ 
 obj_t BgL_auxz00_9544;
BgL_auxz00_9544 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10743L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_3144, 
(int)(BgL_l3490z00_6038), 
(int)(BgL_wz00_3142)); 
FAILURE(BgL_auxz00_9544,BFALSE,BFALSE);} } 
{ /* Ieee/input.scm 270 */
 long BgL_arg2380z00_3159;
BgL_arg2380z00_3159 = 
(1L+BgL_wz00_3142); 
{ /* Ieee/input.scm 270 */
 long BgL_l3494z00_6042;
BgL_l3494z00_6042 = 
STRING_LENGTH(BgL_accz00_3144); 
if(
BOUND_CHECK(BgL_arg2380z00_3159, BgL_l3494z00_6042))
{ /* Ieee/input.scm 270 */
STRING_SET(BgL_accz00_3144, BgL_arg2380z00_3159, ((unsigned char)10)); }  else 
{ 
 obj_t BgL_auxz00_9555;
BgL_auxz00_9555 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10775L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_3144, 
(int)(BgL_l3494z00_6042), 
(int)(BgL_arg2380z00_3159)); 
FAILURE(BgL_auxz00_9555,BFALSE,BFALSE);} } } 
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_accz00_3144, 0L, 
(BgL_wz00_3142+2L));}  else 
{ /* Ieee/input.scm 267 */
{ /* Ieee/input.scm 273 */
 unsigned char BgL_charz00_5318;
{ /* Ieee/input.scm 273 */
 obj_t BgL_tmpz00_9563;
if(
CHARP(BgL_cz00_3141))
{ /* Ieee/input.scm 273 */
BgL_tmpz00_9563 = BgL_cz00_3141
; }  else 
{ 
 obj_t BgL_auxz00_9566;
BgL_auxz00_9566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10881L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_3141); 
FAILURE(BgL_auxz00_9566,BFALSE,BFALSE);} 
BgL_charz00_5318 = 
CCHAR(BgL_tmpz00_9563); } 
{ /* Ieee/input.scm 273 */
 long BgL_l3498z00_6046;
BgL_l3498z00_6046 = 
STRING_LENGTH(BgL_accz00_3144); 
if(
BOUND_CHECK(BgL_wz00_3142, BgL_l3498z00_6046))
{ /* Ieee/input.scm 273 */
STRING_SET(BgL_accz00_3144, BgL_wz00_3142, BgL_charz00_5318); }  else 
{ 
 obj_t BgL_auxz00_9575;
BgL_auxz00_9575 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10862L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_3144, 
(int)(BgL_l3498z00_6046), 
(int)(BgL_wz00_3142)); 
FAILURE(BgL_auxz00_9575,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_9582; obj_t BgL_cz00_9581;
BgL_cz00_9581 = BgL_c2z00_3157; 
BgL_wz00_9582 = 
(BgL_wz00_3142+1L); 
BgL_wz00_3142 = BgL_wz00_9582; 
BgL_cz00_3141 = BgL_cz00_9581; 
goto BgL_zc3z04anonymousza32370ze3z87_3145;} } } }  else 
{ /* Ieee/input.scm 275 */
 bool_t BgL_test4875z00_9584;
{ /* Ieee/input.scm 275 */
 unsigned char BgL_char1z00_5320;
{ /* Ieee/input.scm 275 */
 obj_t BgL_tmpz00_9585;
if(
CHARP(BgL_cz00_3141))
{ /* Ieee/input.scm 275 */
BgL_tmpz00_9585 = BgL_cz00_3141
; }  else 
{ 
 obj_t BgL_auxz00_9588;
BgL_auxz00_9588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10932L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_3141); 
FAILURE(BgL_auxz00_9588,BFALSE,BFALSE);} 
BgL_char1z00_5320 = 
CCHAR(BgL_tmpz00_9585); } 
BgL_test4875z00_9584 = 
(BgL_char1z00_5320==((unsigned char)10)); } 
if(BgL_test4875z00_9584)
{ /* Ieee/input.scm 275 */
{ /* Ieee/input.scm 277 */
 long BgL_l3502z00_6050;
BgL_l3502z00_6050 = 
STRING_LENGTH(BgL_accz00_3144); 
if(
BOUND_CHECK(BgL_wz00_3142, BgL_l3502z00_6050))
{ /* Ieee/input.scm 277 */
STRING_SET(BgL_accz00_3144, BgL_wz00_3142, ((unsigned char)10)); }  else 
{ 
 obj_t BgL_auxz00_9598;
BgL_auxz00_9598 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(10991L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_3144, 
(int)(BgL_l3502z00_6050), 
(int)(BgL_wz00_3142)); 
FAILURE(BgL_auxz00_9598,BFALSE,BFALSE);} } 
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_accz00_3144, 0L, 
(BgL_wz00_3142+1L));}  else 
{ /* Ieee/input.scm 275 */
{ /* Ieee/input.scm 281 */
 unsigned char BgL_charz00_5328;
{ /* Ieee/input.scm 281 */
 obj_t BgL_tmpz00_9606;
if(
CHARP(BgL_cz00_3141))
{ /* Ieee/input.scm 281 */
BgL_tmpz00_9606 = BgL_cz00_3141
; }  else 
{ 
 obj_t BgL_auxz00_9609;
BgL_auxz00_9609 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11121L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_cz00_3141); 
FAILURE(BgL_auxz00_9609,BFALSE,BFALSE);} 
BgL_charz00_5328 = 
CCHAR(BgL_tmpz00_9606); } 
{ /* Ieee/input.scm 281 */
 long BgL_l3506z00_6054;
BgL_l3506z00_6054 = 
STRING_LENGTH(BgL_accz00_3144); 
if(
BOUND_CHECK(BgL_wz00_3142, BgL_l3506z00_6054))
{ /* Ieee/input.scm 281 */
STRING_SET(BgL_accz00_3144, BgL_wz00_3142, BgL_charz00_5328); }  else 
{ 
 obj_t BgL_auxz00_9618;
BgL_auxz00_9618 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11102L), BGl_string4279z00zz__r4_input_6_10_2z00, BgL_accz00_3144, 
(int)(BgL_l3506z00_6054), 
(int)(BgL_wz00_3142)); 
FAILURE(BgL_auxz00_9618,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_9626; obj_t BgL_cz00_9624;
BgL_cz00_9624 = 
BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_ipz00_29); 
BgL_wz00_9626 = 
(BgL_wz00_3142+1L); 
BgL_wz00_3142 = BgL_wz00_9626; 
BgL_cz00_3141 = BgL_cz00_9624; 
goto BgL_zc3z04anonymousza32370ze3z87_3145;} } } } } } } } } 

}



/* _read-lines */
obj_t BGl__readzd2lineszd2zz__r4_input_6_10_2z00(obj_t BgL_env1275z00_34, obj_t BgL_opt1274z00_33)
{
{ /* Ieee/input.scm 287 */
{ /* Ieee/input.scm 287 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1274z00_33)) { case 0L : 

{ /* Ieee/input.scm 287 */
 obj_t BgL_ipz00_3173;
{ /* Ieee/input.scm 287 */
 obj_t BgL_tmpz00_9628;
BgL_tmpz00_9628 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3173 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9628); } 
{ /* Ieee/input.scm 287 */

return 
BGl_readzd2lineszd2zz__r4_input_6_10_2z00(BgL_ipz00_3173);} } break;case 1L : 

{ /* Ieee/input.scm 287 */
 obj_t BgL_ipz00_3174;
BgL_ipz00_3174 = 
VECTOR_REF(BgL_opt1274z00_33,0L); 
{ /* Ieee/input.scm 287 */

return 
BGl_readzd2lineszd2zz__r4_input_6_10_2z00(BgL_ipz00_3174);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4287z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1274z00_33)));} } } } 

}



/* read-lines */
BGL_EXPORTED_DEF obj_t BGl_readzd2lineszd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_32)
{
{ /* Ieee/input.scm 287 */
{ /* Ieee/input.scm 288 */
 obj_t BgL_g1096z00_3176;
BgL_g1096z00_3176 = 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_32); 
{ 
 obj_t BgL_lz00_5338; obj_t BgL_lsz00_5339;
BgL_lz00_5338 = BgL_g1096z00_3176; 
BgL_lsz00_5339 = BNIL; 
BgL_loopz00_5337:
if(
EOF_OBJECTP(BgL_lz00_5338))
{ /* Ieee/input.scm 290 */
return 
bgl_reverse_bang(BgL_lsz00_5339);}  else 
{ /* Ieee/input.scm 292 */
 obj_t BgL_arg2392z00_5344; obj_t BgL_arg2393z00_5345;
BgL_arg2392z00_5344 = 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_32); 
BgL_arg2393z00_5345 = 
MAKE_YOUNG_PAIR(BgL_lz00_5338, BgL_lsz00_5339); 
{ 
 obj_t BgL_lsz00_9646; obj_t BgL_lz00_9645;
BgL_lz00_9645 = BgL_arg2392z00_5344; 
BgL_lsz00_9646 = BgL_arg2393z00_5345; 
BgL_lsz00_5339 = BgL_lsz00_9646; 
BgL_lz00_5338 = BgL_lz00_9645; 
goto BgL_loopz00_5337;} } } } } 

}



/* _read-string */
obj_t BGl__readzd2stringzd2zz__r4_input_6_10_2z00(obj_t BgL_env1279z00_37, obj_t BgL_opt1278z00_36)
{
{ /* Ieee/input.scm 297 */
{ /* Ieee/input.scm 297 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1278z00_36)) { case 0L : 

{ /* Ieee/input.scm 297 */
 obj_t BgL_ipz00_3188;
{ /* Ieee/input.scm 297 */
 obj_t BgL_tmpz00_9647;
BgL_tmpz00_9647 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3188 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9647); } 
{ /* Ieee/input.scm 297 */

return 
BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_ipz00_3188);} } break;case 1L : 

{ /* Ieee/input.scm 297 */
 obj_t BgL_ipz00_3189;
BgL_ipz00_3189 = 
VECTOR_REF(BgL_opt1278z00_36,0L); 
{ /* Ieee/input.scm 297 */

return 
BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_ipz00_3189);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4289z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1278z00_36)));} } } } 

}



/* read-string */
BGL_EXPORTED_DEF obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_35)
{
{ /* Ieee/input.scm 297 */
{ 
 obj_t BgL_iportz00_3254; long BgL_lastzd2matchzd2_3255; long BgL_forwardz00_3256; long BgL_bufposz00_3257; obj_t BgL_iportz00_3242; long BgL_lastzd2matchzd2_3243; long BgL_forwardz00_3244; long BgL_bufposz00_3245;
{ /* Ieee/input.scm 298 */
 bool_t BgL_test4881z00_9658;
{ /* Ieee/input.scm 298 */
 obj_t BgL_arg2401z00_3230;
{ /* Ieee/input.scm 298 */
 obj_t BgL_res3470z00_5405;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_res3470z00_5405 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9661;
BgL_auxz00_9661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4290z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9661,BFALSE,BFALSE);} 
BgL_arg2401z00_3230 = BgL_res3470z00_5405; } 
BgL_test4881z00_9658 = 
INPUT_PORT_CLOSEP(BgL_arg2401z00_3230); } 
if(BgL_test4881z00_9658)
{ /* Ieee/input.scm 298 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2396z00_3224;
{ /* Ieee/input.scm 298 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_3225;
{ /* Ieee/input.scm 298 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_3228;
BgL_new1045z00_3228 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 298 */
 long BgL_arg2399z00_3229;
BgL_arg2399z00_3229 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_3228), BgL_arg2399z00_3229); } 
BgL_new1046z00_3225 = BgL_new1045z00_3228; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_3225)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_3225)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_9674;
{ /* Ieee/input.scm 298 */
 obj_t BgL_arg2397z00_3226;
{ /* Ieee/input.scm 298 */
 obj_t BgL_arg2398z00_3227;
{ /* Ieee/input.scm 298 */
 obj_t BgL_classz00_5410;
BgL_classz00_5410 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2398z00_3227 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5410); } 
BgL_arg2397z00_3226 = 
VECTOR_REF(BgL_arg2398z00_3227,2L); } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_auxz00_9678;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2397z00_3226))
{ /* Ieee/input.scm 298 */
BgL_auxz00_9678 = BgL_arg2397z00_3226
; }  else 
{ 
 obj_t BgL_auxz00_9681;
BgL_auxz00_9681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4290z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2397z00_3226); 
FAILURE(BgL_auxz00_9681,BFALSE,BFALSE);} 
BgL_auxz00_9674 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_9678); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_3225)))->BgL_stackz00)=((obj_t)BgL_auxz00_9674),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_3225)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_3225)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_9691;
{ /* Ieee/input.scm 298 */
 obj_t BgL_res3471z00_5412;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_res3471z00_5412 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9695;
BgL_auxz00_9695 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4290z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9695,BFALSE,BFALSE);} 
BgL_auxz00_9691 = BgL_res3471z00_5412; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_3225)))->BgL_objz00)=((obj_t)BgL_auxz00_9691),BUNSPEC); } 
BgL_arg2396z00_3224 = BgL_new1046z00_3225; } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_aux4015z00_6569;
BgL_aux4015z00_6569 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2396z00_3224)); 
if(
STRINGP(BgL_aux4015z00_6569))
{ /* Ieee/input.scm 298 */
return BgL_aux4015z00_6569;}  else 
{ 
 obj_t BgL_auxz00_9704;
BgL_auxz00_9704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4290z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_aux4015z00_6569); 
FAILURE(BgL_auxz00_9704,BFALSE,BFALSE);} } }  else 
{ /* Ieee/input.scm 298 */
 obj_t BgL_aux4017z00_6571;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5380;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5380 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9710;
BgL_auxz00_9710 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9710,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_5380); } 
{ /* Ieee/input.scm 298 */
 long BgL_matchz00_3385;
{ /* Ieee/input.scm 298 */
 long BgL_arg2515z00_3390; long BgL_arg2516z00_3391;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5381;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5381 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9717;
BgL_auxz00_9717 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9717,BFALSE,BFALSE);} 
BgL_arg2515z00_3390 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5381); } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5382;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5382 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9724;
BgL_auxz00_9724 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9724,BFALSE,BFALSE);} 
BgL_arg2516z00_3391 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5382); } 
{ 
 long BgL_forwardz00_5384; long BgL_bufposz00_5385;
BgL_forwardz00_5384 = BgL_arg2515z00_3390; 
BgL_bufposz00_5385 = BgL_arg2516z00_3391; 
BgL_statezd20zd21099z00_5383:
if(
(BgL_forwardz00_5384==BgL_bufposz00_5385))
{ /* Ieee/input.scm 298 */
 bool_t BgL_test4893z00_9731;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5390;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5390 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9734;
BgL_auxz00_9734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4293z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9734,BFALSE,BFALSE);} 
BgL_test4893z00_9731 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5390); } 
if(BgL_test4893z00_9731)
{ /* Ieee/input.scm 298 */
 long BgL_arg2405z00_5391; long BgL_arg2407z00_5392;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5393;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5393 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9741;
BgL_auxz00_9741 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4293z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9741,BFALSE,BFALSE);} 
BgL_arg2405z00_5391 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5393); } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5394;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5394 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9748;
BgL_auxz00_9748 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4293z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9748,BFALSE,BFALSE);} 
BgL_arg2407z00_5392 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5394); } 
{ 
 long BgL_bufposz00_9754; long BgL_forwardz00_9753;
BgL_forwardz00_9753 = BgL_arg2405z00_5391; 
BgL_bufposz00_9754 = BgL_arg2407z00_5392; 
BgL_bufposz00_5385 = BgL_bufposz00_9754; 
BgL_forwardz00_5384 = BgL_forwardz00_9753; 
goto BgL_statezd20zd21099z00_5383;} }  else 
{ /* Ieee/input.scm 298 */
BgL_matchz00_3385 = 1L; } }  else 
{ /* Ieee/input.scm 298 */
 int BgL_curz00_5395;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5396;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5396 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9757;
BgL_auxz00_9757 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4293z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9757,BFALSE,BFALSE);} 
BgL_curz00_5395 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5396, BgL_forwardz00_5384); } 
{ /* Ieee/input.scm 298 */

BgL_iportz00_3254 = BgL_ipz00_35; 
BgL_lastzd2matchzd2_3255 = 1L; 
BgL_forwardz00_3256 = 
(1L+BgL_forwardz00_5384); 
BgL_bufposz00_3257 = BgL_bufposz00_5385; 
BgL_zc3z04anonymousza32416ze3z87_3258:
{ /* Ieee/input.scm 298 */
 long BgL_newzd2matchzd2_3259;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5365;
if(
INPUT_PORTP(BgL_iportz00_3254))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5365 = BgL_iportz00_3254; }  else 
{ 
 obj_t BgL_auxz00_9764;
BgL_auxz00_9764 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4292z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3254); 
FAILURE(BgL_auxz00_9764,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5365, BgL_forwardz00_3256); } 
BgL_newzd2matchzd2_3259 = 0L; 
if(
(BgL_forwardz00_3256==BgL_bufposz00_3257))
{ /* Ieee/input.scm 298 */
 bool_t BgL_test4900z00_9771;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5369;
if(
INPUT_PORTP(BgL_iportz00_3254))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5369 = BgL_iportz00_3254; }  else 
{ 
 obj_t BgL_auxz00_9774;
BgL_auxz00_9774 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4292z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3254); 
FAILURE(BgL_auxz00_9774,BFALSE,BFALSE);} 
BgL_test4900z00_9771 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5369); } 
if(BgL_test4900z00_9771)
{ /* Ieee/input.scm 298 */
 long BgL_arg2419z00_3262; long BgL_arg2420z00_3263;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5370;
if(
INPUT_PORTP(BgL_iportz00_3254))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5370 = BgL_iportz00_3254; }  else 
{ 
 obj_t BgL_auxz00_9781;
BgL_auxz00_9781 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4292z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3254); 
FAILURE(BgL_auxz00_9781,BFALSE,BFALSE);} 
BgL_arg2419z00_3262 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5370); } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5371;
if(
INPUT_PORTP(BgL_iportz00_3254))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5371 = BgL_iportz00_3254; }  else 
{ 
 obj_t BgL_auxz00_9788;
BgL_auxz00_9788 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4292z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3254); 
FAILURE(BgL_auxz00_9788,BFALSE,BFALSE);} 
BgL_arg2420z00_3263 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5371); } 
{ 
 long BgL_bufposz00_9794; long BgL_forwardz00_9793;
BgL_forwardz00_9793 = BgL_arg2419z00_3262; 
BgL_bufposz00_9794 = BgL_arg2420z00_3263; 
BgL_bufposz00_3257 = BgL_bufposz00_9794; 
BgL_forwardz00_3256 = BgL_forwardz00_9793; 
goto BgL_zc3z04anonymousza32416ze3z87_3258;} }  else 
{ /* Ieee/input.scm 298 */
BgL_matchz00_3385 = BgL_newzd2matchzd2_3259; } }  else 
{ /* Ieee/input.scm 298 */
 int BgL_curz00_3264;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5372;
if(
INPUT_PORTP(BgL_iportz00_3254))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5372 = BgL_iportz00_3254; }  else 
{ 
 obj_t BgL_auxz00_9797;
BgL_auxz00_9797 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4292z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3254); 
FAILURE(BgL_auxz00_9797,BFALSE,BFALSE);} 
BgL_curz00_3264 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5372, BgL_forwardz00_3256); } 
{ /* Ieee/input.scm 298 */

BgL_iportz00_3242 = BgL_iportz00_3254; 
BgL_lastzd2matchzd2_3243 = BgL_newzd2matchzd2_3259; 
BgL_forwardz00_3244 = 
(1L+BgL_forwardz00_3256); 
BgL_bufposz00_3245 = BgL_bufposz00_3257; 
BgL_zc3z04anonymousza32409ze3z87_3246:
{ /* Ieee/input.scm 298 */
 long BgL_newzd2matchzd2_3247;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5355;
if(
INPUT_PORTP(BgL_iportz00_3242))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5355 = BgL_iportz00_3242; }  else 
{ 
 obj_t BgL_auxz00_9804;
BgL_auxz00_9804 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4291z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3242); 
FAILURE(BgL_auxz00_9804,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_5355, BgL_forwardz00_3244); } 
BgL_newzd2matchzd2_3247 = 0L; 
if(
(BgL_forwardz00_3244==BgL_bufposz00_3245))
{ /* Ieee/input.scm 298 */
 bool_t BgL_test4907z00_9811;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5359;
if(
INPUT_PORTP(BgL_iportz00_3242))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5359 = BgL_iportz00_3242; }  else 
{ 
 obj_t BgL_auxz00_9814;
BgL_auxz00_9814 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4291z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3242); 
FAILURE(BgL_auxz00_9814,BFALSE,BFALSE);} 
BgL_test4907z00_9811 = 
rgc_fill_buffer(BgL_inputzd2portzd2_5359); } 
if(BgL_test4907z00_9811)
{ /* Ieee/input.scm 298 */
 long BgL_arg2412z00_3250; long BgL_arg2414z00_3251;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5360;
if(
INPUT_PORTP(BgL_iportz00_3242))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5360 = BgL_iportz00_3242; }  else 
{ 
 obj_t BgL_auxz00_9821;
BgL_auxz00_9821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4291z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3242); 
FAILURE(BgL_auxz00_9821,BFALSE,BFALSE);} 
BgL_arg2412z00_3250 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_5360); } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5361;
if(
INPUT_PORTP(BgL_iportz00_3242))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5361 = BgL_iportz00_3242; }  else 
{ 
 obj_t BgL_auxz00_9828;
BgL_auxz00_9828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4291z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3242); 
FAILURE(BgL_auxz00_9828,BFALSE,BFALSE);} 
BgL_arg2414z00_3251 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_5361); } 
{ 
 long BgL_bufposz00_9834; long BgL_forwardz00_9833;
BgL_forwardz00_9833 = BgL_arg2412z00_3250; 
BgL_bufposz00_9834 = BgL_arg2414z00_3251; 
BgL_bufposz00_3245 = BgL_bufposz00_9834; 
BgL_forwardz00_3244 = BgL_forwardz00_9833; 
goto BgL_zc3z04anonymousza32409ze3z87_3246;} }  else 
{ /* Ieee/input.scm 298 */
BgL_matchz00_3385 = BgL_newzd2matchzd2_3247; } }  else 
{ /* Ieee/input.scm 298 */
 int BgL_curz00_3252;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5362;
if(
INPUT_PORTP(BgL_iportz00_3242))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5362 = BgL_iportz00_3242; }  else 
{ 
 obj_t BgL_auxz00_9837;
BgL_auxz00_9837 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4291z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_iportz00_3242); 
FAILURE(BgL_auxz00_9837,BFALSE,BFALSE);} 
BgL_curz00_3252 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_5362, BgL_forwardz00_3244); } 
{ /* Ieee/input.scm 298 */

{ 
 long BgL_forwardz00_9843; long BgL_lastzd2matchzd2_9842;
BgL_lastzd2matchzd2_9842 = BgL_newzd2matchzd2_3247; 
BgL_forwardz00_9843 = 
(1L+BgL_forwardz00_3244); 
BgL_forwardz00_3244 = BgL_forwardz00_9843; 
BgL_lastzd2matchzd2_3243 = BgL_lastzd2matchzd2_9842; 
goto BgL_zc3z04anonymousza32409ze3z87_3246;} } } } } } } } } } } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5400;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5400 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9849;
BgL_auxz00_9849 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9849,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_5400); } 
{ 

switch( BgL_matchz00_3385) { case 1L : 

BgL_aux4017z00_6571 = BGl_string4276z00zz__r4_input_6_10_2z00; break;case 0L : 

{ /* Ieee/input.scm 298 */
 long BgL_arg2428z00_5401;
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5402;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5402 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9856;
BgL_auxz00_9856 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9856,BFALSE,BFALSE);} 
BgL_arg2428z00_5401 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_5402); } 
{ /* Ieee/input.scm 298 */
 obj_t BgL_inputzd2portzd2_5403;
if(
INPUT_PORTP(BgL_ipz00_35))
{ /* Ieee/input.scm 298 */
BgL_inputzd2portzd2_5403 = BgL_ipz00_35; }  else 
{ 
 obj_t BgL_auxz00_9863;
BgL_auxz00_9863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4184z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_35); 
FAILURE(BgL_auxz00_9863,BFALSE,BFALSE);} 
BgL_aux4017z00_6571 = 
rgc_buffer_substring(BgL_inputzd2portzd2_5403, 0L, BgL_arg2428z00_5401); } } break;
default: 
BgL_aux4017z00_6571 = 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_3385)); } } } 
if(
STRINGP(BgL_aux4017z00_6571))
{ /* Ieee/input.scm 298 */
return BgL_aux4017z00_6571;}  else 
{ 
 obj_t BgL_auxz00_9873;
BgL_auxz00_9873 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(11873L), BGl_string4290z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_aux4017z00_6571); 
FAILURE(BgL_auxz00_9873,BFALSE,BFALSE);} } } } } 

}



/* _read-of-strings */
obj_t BGl__readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t BgL_env1283z00_40, obj_t BgL_opt1282z00_39)
{
{ /* Ieee/input.scm 320 */
{ /* Ieee/input.scm 320 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1282z00_39)) { case 0L : 

{ /* Ieee/input.scm 320 */
 obj_t BgL_ipz00_3424;
{ /* Ieee/input.scm 320 */
 obj_t BgL_tmpz00_9877;
BgL_tmpz00_9877 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3424 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9877); } 
{ /* Ieee/input.scm 320 */

return 
BGl_z62zc3z04anonymousza31401ze3ze5zz__r4_input_6_10_2z00(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00, BgL_ipz00_3424);} } break;case 1L : 

{ /* Ieee/input.scm 320 */
 obj_t BgL_ipz00_3425;
BgL_ipz00_3425 = 
VECTOR_REF(BgL_opt1282z00_39,0L); 
{ /* Ieee/input.scm 320 */

return 
BGl_z62zc3z04anonymousza31401ze3ze5zz__r4_input_6_10_2z00(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00, BgL_ipz00_3425);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4294z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1282z00_39)));} } } } 

}



/* read-of-strings */
BGL_EXPORTED_DEF obj_t BGl_readzd2ofzd2stringsz00zz__r4_input_6_10_2z00(obj_t BgL_ipz00_38)
{
{ /* Ieee/input.scm 320 */
return 
BGl_z62zc3z04anonymousza31401ze3ze5zz__r4_input_6_10_2z00(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00, BgL_ipz00_38);} 

}



/* _read-chars */
obj_t BGl__readzd2charszd2zz__r4_input_6_10_2z00(obj_t BgL_env1287z00_44, obj_t BgL_opt1286z00_43)
{
{ /* Ieee/input.scm 326 */
{ /* Ieee/input.scm 326 */
 obj_t BgL_g1288z00_3427;
BgL_g1288z00_3427 = 
VECTOR_REF(BgL_opt1286z00_43,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1286z00_43)) { case 1L : 

{ /* Ieee/input.scm 326 */
 obj_t BgL_ipz00_3430;
{ /* Ieee/input.scm 326 */
 obj_t BgL_tmpz00_9899;
BgL_tmpz00_9899 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3430 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_9899); } 
{ /* Ieee/input.scm 326 */

return 
BGl_readzd2charszd2zz__r4_input_6_10_2z00(BgL_g1288z00_3427, BgL_ipz00_3430);} } break;case 2L : 

{ /* Ieee/input.scm 326 */
 obj_t BgL_ipz00_3431;
BgL_ipz00_3431 = 
VECTOR_REF(BgL_opt1286z00_43,1L); 
{ /* Ieee/input.scm 326 */

return 
BGl_readzd2charszd2zz__r4_input_6_10_2z00(BgL_g1288z00_3427, BgL_ipz00_3431);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4296z00zz__r4_input_6_10_2z00, BGl_string4298z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1286z00_43)));} } } } 

}



/* read-chars */
BGL_EXPORTED_DEF obj_t BGl_readzd2charszd2zz__r4_input_6_10_2z00(obj_t BgL_lz00_41, obj_t BgL_ipz00_42)
{
{ /* Ieee/input.scm 326 */
{ /* Ieee/input.scm 327 */
 obj_t BgL_lenz00_3433;
if(
INTEGERP(BgL_lz00_41))
{ /* Ieee/input.scm 328 */
BgL_lenz00_3433 = BgL_lz00_41; }  else 
{ /* Ieee/input.scm 328 */
if(
ELONGP(BgL_lz00_41))
{ /* Ieee/input.scm 329 */
 long BgL_xz00_5415;
BgL_xz00_5415 = 
BELONG_TO_LONG(BgL_lz00_41); 
BgL_lenz00_3433 = 
BINT(
(long)(BgL_xz00_5415)); }  else 
{ /* Ieee/input.scm 329 */
if(
LLONGP(BgL_lz00_41))
{ /* Ieee/input.scm 330 */
 BGL_LONGLONG_T BgL_xz00_5416;
BgL_xz00_5416 = 
BLLONG_TO_LLONG(BgL_lz00_41); 
BgL_lenz00_3433 = 
BINT(
LLONG_TO_LONG(BgL_xz00_5416)); }  else 
{ /* Ieee/input.scm 332 */
 obj_t BgL_arg2534z00_3450;
BgL_arg2534z00_3450 = 
bgl_find_runtime_type(BgL_lz00_41); 
BgL_lenz00_3433 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_symbol4296z00zz__r4_input_6_10_2z00, BGl_string4299z00zz__r4_input_6_10_2z00, BgL_arg2534z00_3450); } } } 
{ /* Ieee/input.scm 334 */
 bool_t BgL_test4919z00_9924;
{ /* Ieee/input.scm 334 */
 long BgL_n1z00_5417;
{ /* Ieee/input.scm 334 */
 obj_t BgL_tmpz00_9925;
if(
INTEGERP(BgL_lenz00_3433))
{ /* Ieee/input.scm 334 */
BgL_tmpz00_9925 = BgL_lenz00_3433
; }  else 
{ 
 obj_t BgL_auxz00_9928;
BgL_auxz00_9928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13328L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3433); 
FAILURE(BgL_auxz00_9928,BFALSE,BFALSE);} 
BgL_n1z00_5417 = 
(long)CINT(BgL_tmpz00_9925); } 
BgL_test4919z00_9924 = 
(BgL_n1z00_5417<=0L); } 
if(BgL_test4919z00_9924)
{ /* Ieee/input.scm 335 */
 bool_t BgL_test4921z00_9934;
{ /* Ieee/input.scm 335 */
 long BgL_n1z00_5418;
{ /* Ieee/input.scm 335 */
 obj_t BgL_tmpz00_9935;
if(
INTEGERP(BgL_lenz00_3433))
{ /* Ieee/input.scm 335 */
BgL_tmpz00_9935 = BgL_lenz00_3433
; }  else 
{ 
 obj_t BgL_auxz00_9938;
BgL_auxz00_9938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13347L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3433); 
FAILURE(BgL_auxz00_9938,BFALSE,BFALSE);} 
BgL_n1z00_5418 = 
(long)CINT(BgL_tmpz00_9935); } 
BgL_test4921z00_9934 = 
(BgL_n1z00_5418==0L); } 
if(BgL_test4921z00_9934)
{ /* Ieee/input.scm 335 */
return BGl_string4276z00zz__r4_input_6_10_2z00;}  else 
{ /* Ieee/input.scm 338 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2524z00_3436;
{ /* Ieee/input.scm 338 */
 BgL_z62iozd2errorzb0_bglt BgL_new1120z00_3437;
{ /* Ieee/input.scm 338 */
 BgL_z62iozd2errorzb0_bglt BgL_new1119z00_3440;
BgL_new1119z00_3440 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 338 */
 long BgL_arg2527z00_3441;
BgL_arg2527z00_3441 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1119z00_3440), BgL_arg2527z00_3441); } 
BgL_new1120z00_3437 = BgL_new1119z00_3440; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1120z00_3437)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1120z00_3437)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_9952;
{ /* Ieee/input.scm 338 */
 obj_t BgL_arg2525z00_3438;
{ /* Ieee/input.scm 338 */
 obj_t BgL_arg2526z00_3439;
{ /* Ieee/input.scm 338 */
 obj_t BgL_classz00_5422;
BgL_classz00_5422 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2526z00_3439 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5422); } 
BgL_arg2525z00_3438 = 
VECTOR_REF(BgL_arg2526z00_3439,2L); } 
{ /* Ieee/input.scm 338 */
 obj_t BgL_auxz00_9956;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2525z00_3438))
{ /* Ieee/input.scm 338 */
BgL_auxz00_9956 = BgL_arg2525z00_3438
; }  else 
{ 
 obj_t BgL_auxz00_9959;
BgL_auxz00_9959 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13386L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2525z00_3438); 
FAILURE(BgL_auxz00_9959,BFALSE,BFALSE);} 
BgL_auxz00_9952 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_9956); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1120z00_3437)))->BgL_stackz00)=((obj_t)BgL_auxz00_9952),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1120z00_3437)))->BgL_procz00)=((obj_t)BGl_symbol4296z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1120z00_3437)))->BgL_msgz00)=((obj_t)BGl_string4301z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1120z00_3437)))->BgL_objz00)=((obj_t)BgL_lenz00_3433),BUNSPEC); 
BgL_arg2524z00_3436 = BgL_new1120z00_3437; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2524z00_3436));} }  else 
{ /* Ieee/input.scm 343 */
 obj_t BgL_sz00_3442;
{ /* Ieee/input.scm 343 */
 long BgL_tmpz00_9973;
{ /* Ieee/input.scm 343 */
 obj_t BgL_tmpz00_9974;
if(
INTEGERP(BgL_lenz00_3433))
{ /* Ieee/input.scm 343 */
BgL_tmpz00_9974 = BgL_lenz00_3433
; }  else 
{ 
 obj_t BgL_auxz00_9977;
BgL_auxz00_9977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13530L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3433); 
FAILURE(BgL_auxz00_9977,BFALSE,BFALSE);} 
BgL_tmpz00_9973 = 
(long)CINT(BgL_tmpz00_9974); } 
BgL_sz00_3442 = 
make_string_sans_fill(BgL_tmpz00_9973); } 
{ /* Ieee/input.scm 343 */
 long BgL_nz00_3443;
{ /* Ieee/input.scm 344 */
 long BgL_auxz00_9990; obj_t BgL_tmpz00_9983;
{ /* Ieee/input.scm 344 */
 obj_t BgL_tmpz00_9992;
if(
INTEGERP(BgL_lenz00_3433))
{ /* Ieee/input.scm 344 */
BgL_tmpz00_9992 = BgL_lenz00_3433
; }  else 
{ 
 obj_t BgL_auxz00_9995;
BgL_auxz00_9995 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13568L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3433); 
FAILURE(BgL_auxz00_9995,BFALSE,BFALSE);} 
BgL_auxz00_9990 = 
(long)CINT(BgL_tmpz00_9992); } 
if(
INPUT_PORTP(BgL_ipz00_42))
{ /* Ieee/input.scm 344 */
BgL_tmpz00_9983 = BgL_ipz00_42
; }  else 
{ 
 obj_t BgL_auxz00_9986;
BgL_auxz00_9986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13561L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_42); 
FAILURE(BgL_auxz00_9986,BFALSE,BFALSE);} 
BgL_nz00_3443 = 
bgl_rgc_blit_string(BgL_tmpz00_9983, 
BSTRING_TO_STRING(BgL_sz00_3442), 0L, BgL_auxz00_9990); } 
{ /* Ieee/input.scm 344 */

if(
(BgL_nz00_3443==0L))
{ /* Ieee/input.scm 347 */
 bool_t BgL_test4928z00_10003;
{ /* Ieee/input.scm 347 */
 obj_t BgL_inputzd2portzd2_5425;
if(
INPUT_PORTP(BgL_ipz00_42))
{ /* Ieee/input.scm 347 */
BgL_inputzd2portzd2_5425 = BgL_ipz00_42; }  else 
{ 
 obj_t BgL_auxz00_10006;
BgL_auxz00_10006 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13624L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_42); 
FAILURE(BgL_auxz00_10006,BFALSE,BFALSE);} 
BgL_test4928z00_10003 = 
rgc_buffer_eof_p(BgL_inputzd2portzd2_5425); } 
if(BgL_test4928z00_10003)
{ /* Ieee/input.scm 347 */
return BEOF;}  else 
{ /* Ieee/input.scm 347 */
return BGl_string4276z00zz__r4_input_6_10_2z00;} }  else 
{ /* Ieee/input.scm 350 */
 bool_t BgL_test4930z00_10011;
{ /* Ieee/input.scm 350 */
 long BgL_n2z00_5427;
{ /* Ieee/input.scm 350 */
 obj_t BgL_tmpz00_10012;
if(
INTEGERP(BgL_lenz00_3433))
{ /* Ieee/input.scm 350 */
BgL_tmpz00_10012 = BgL_lenz00_3433
; }  else 
{ 
 obj_t BgL_auxz00_10015;
BgL_auxz00_10015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13662L), BGl_string4297z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3433); 
FAILURE(BgL_auxz00_10015,BFALSE,BFALSE);} 
BgL_n2z00_5427 = 
(long)CINT(BgL_tmpz00_10012); } 
BgL_test4930z00_10011 = 
(BgL_nz00_3443<BgL_n2z00_5427); } 
if(BgL_test4930z00_10011)
{ /* Ieee/input.scm 350 */
return 
bgl_string_shrink(BgL_sz00_3442, BgL_nz00_3443);}  else 
{ /* Ieee/input.scm 350 */
return BgL_sz00_3442;} } } } } } } } 

}



/* _read-chars! */
obj_t BGl__readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t BgL_env1292z00_49, obj_t BgL_opt1291z00_48)
{
{ /* Ieee/input.scm 358 */
{ /* Ieee/input.scm 358 */
 obj_t BgL_g1293z00_3451; obj_t BgL_g1294z00_3452;
BgL_g1293z00_3451 = 
VECTOR_REF(BgL_opt1291z00_48,0L); 
BgL_g1294z00_3452 = 
VECTOR_REF(BgL_opt1291z00_48,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1291z00_48)) { case 2L : 

{ /* Ieee/input.scm 358 */
 obj_t BgL_ipz00_3455;
{ /* Ieee/input.scm 358 */
 obj_t BgL_tmpz00_10024;
BgL_tmpz00_10024 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3455 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_10024); } 
{ /* Ieee/input.scm 358 */

{ /* Ieee/input.scm 358 */
 obj_t BgL_auxz00_10027;
if(
STRINGP(BgL_g1293z00_3451))
{ /* Ieee/input.scm 358 */
BgL_auxz00_10027 = BgL_g1293z00_3451
; }  else 
{ 
 obj_t BgL_auxz00_10030;
BgL_auxz00_10030 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13935L), BGl_string4305z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1293z00_3451); 
FAILURE(BgL_auxz00_10030,BFALSE,BFALSE);} 
return 
BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10027, BgL_g1294z00_3452, BgL_ipz00_3455);} } } break;case 3L : 

{ /* Ieee/input.scm 358 */
 obj_t BgL_ipz00_3456;
BgL_ipz00_3456 = 
VECTOR_REF(BgL_opt1291z00_48,2L); 
{ /* Ieee/input.scm 358 */

{ /* Ieee/input.scm 358 */
 obj_t BgL_auxz00_10036;
if(
STRINGP(BgL_g1293z00_3451))
{ /* Ieee/input.scm 358 */
BgL_auxz00_10036 = BgL_g1293z00_3451
; }  else 
{ 
 obj_t BgL_auxz00_10039;
BgL_auxz00_10039 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(13935L), BGl_string4305z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1293z00_3451); 
FAILURE(BgL_auxz00_10039,BFALSE,BFALSE);} 
return 
BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10036, BgL_g1294z00_3452, BgL_ipz00_3456);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4302z00zz__r4_input_6_10_2z00, BGl_string4304z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1291z00_48)));} } } } 

}



/* read-chars! */
BGL_EXPORTED_DEF obj_t BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t BgL_bufz00_45, obj_t BgL_lz00_46, obj_t BgL_ipz00_47)
{
{ /* Ieee/input.scm 358 */
{ /* Ieee/input.scm 359 */
 obj_t BgL_lenz00_3458;
if(
INTEGERP(BgL_lz00_46))
{ /* Ieee/input.scm 360 */
BgL_lenz00_3458 = BgL_lz00_46; }  else 
{ /* Ieee/input.scm 360 */
if(
ELONGP(BgL_lz00_46))
{ /* Ieee/input.scm 361 */
 long BgL_xz00_5431;
BgL_xz00_5431 = 
BELONG_TO_LONG(BgL_lz00_46); 
BgL_lenz00_3458 = 
BINT(
(long)(BgL_xz00_5431)); }  else 
{ /* Ieee/input.scm 361 */
if(
LLONGP(BgL_lz00_46))
{ /* Ieee/input.scm 362 */
 BGL_LONGLONG_T BgL_xz00_5432;
BgL_xz00_5432 = 
BLLONG_TO_LLONG(BgL_lz00_46); 
BgL_lenz00_3458 = 
BINT(
LLONG_TO_LONG(BgL_xz00_5432)); }  else 
{ /* Ieee/input.scm 364 */
 obj_t BgL_arg2551z00_3474;
BgL_arg2551z00_3474 = 
bgl_find_runtime_type(BgL_lz00_46); 
BgL_lenz00_3458 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_symbol4302z00zz__r4_input_6_10_2z00, BGl_string4299z00zz__r4_input_6_10_2z00, BgL_arg2551z00_3474); } } } 
{ /* Ieee/input.scm 365 */
 bool_t BgL_test4937z00_10063;
{ /* Ieee/input.scm 365 */
 long BgL_n1z00_5433;
{ /* Ieee/input.scm 365 */
 obj_t BgL_tmpz00_10064;
if(
INTEGERP(BgL_lenz00_3458))
{ /* Ieee/input.scm 365 */
BgL_tmpz00_10064 = BgL_lenz00_3458
; }  else 
{ 
 obj_t BgL_auxz00_10067;
BgL_auxz00_10067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14204L), BGl_string4303z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3458); 
FAILURE(BgL_auxz00_10067,BFALSE,BFALSE);} 
BgL_n1z00_5433 = 
(long)CINT(BgL_tmpz00_10064); } 
BgL_test4937z00_10063 = 
(BgL_n1z00_5433<=0L); } 
if(BgL_test4937z00_10063)
{ /* Ieee/input.scm 366 */
 bool_t BgL_test4939z00_10073;
{ /* Ieee/input.scm 366 */
 long BgL_n1z00_5434;
{ /* Ieee/input.scm 366 */
 obj_t BgL_tmpz00_10074;
if(
INTEGERP(BgL_lenz00_3458))
{ /* Ieee/input.scm 366 */
BgL_tmpz00_10074 = BgL_lenz00_3458
; }  else 
{ 
 obj_t BgL_auxz00_10077;
BgL_auxz00_10077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14223L), BGl_string4303z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3458); 
FAILURE(BgL_auxz00_10077,BFALSE,BFALSE);} 
BgL_n1z00_5434 = 
(long)CINT(BgL_tmpz00_10074); } 
BgL_test4939z00_10073 = 
(BgL_n1z00_5434==0L); } 
if(BgL_test4939z00_10073)
{ /* Ieee/input.scm 366 */
return 
BINT(0L);}  else 
{ /* Ieee/input.scm 369 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2539z00_3461;
{ /* Ieee/input.scm 369 */
 BgL_z62iozd2errorzb0_bglt BgL_new1123z00_3462;
{ /* Ieee/input.scm 369 */
 BgL_z62iozd2errorzb0_bglt BgL_new1122z00_3465;
BgL_new1122z00_3465 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 369 */
 long BgL_arg2543z00_3466;
BgL_arg2543z00_3466 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1122z00_3465), BgL_arg2543z00_3466); } 
BgL_new1123z00_3462 = BgL_new1122z00_3465; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1123z00_3462)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1123z00_3462)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10092;
{ /* Ieee/input.scm 369 */
 obj_t BgL_arg2540z00_3463;
{ /* Ieee/input.scm 369 */
 obj_t BgL_arg2542z00_3464;
{ /* Ieee/input.scm 369 */
 obj_t BgL_classz00_5438;
BgL_classz00_5438 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2542z00_3464 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5438); } 
BgL_arg2540z00_3463 = 
VECTOR_REF(BgL_arg2542z00_3464,2L); } 
{ /* Ieee/input.scm 369 */
 obj_t BgL_auxz00_10096;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2540z00_3463))
{ /* Ieee/input.scm 369 */
BgL_auxz00_10096 = BgL_arg2540z00_3463
; }  else 
{ 
 obj_t BgL_auxz00_10099;
BgL_auxz00_10099 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14256L), BGl_string4303z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2540z00_3463); 
FAILURE(BgL_auxz00_10099,BFALSE,BFALSE);} 
BgL_auxz00_10092 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10096); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1123z00_3462)))->BgL_stackz00)=((obj_t)BgL_auxz00_10092),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1123z00_3462)))->BgL_procz00)=((obj_t)BGl_symbol4296z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1123z00_3462)))->BgL_msgz00)=((obj_t)BGl_string4301z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1123z00_3462)))->BgL_objz00)=((obj_t)BgL_lenz00_3458),BUNSPEC); 
BgL_arg2539z00_3461 = BgL_new1123z00_3462; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2539z00_3461));} }  else 
{ /* Ieee/input.scm 373 */
 obj_t BgL_arg2545z00_3467;
{ /* Ieee/input.scm 373 */
 long BgL_az00_3468;
BgL_az00_3468 = 
STRING_LENGTH(BgL_bufz00_45); 
{ /* Ieee/input.scm 373 */
 bool_t BgL_test4942z00_10114;
{ /* Ieee/input.scm 373 */
 long BgL_n2z00_5442;
{ /* Ieee/input.scm 373 */
 obj_t BgL_tmpz00_10115;
if(
INTEGERP(BgL_lenz00_3458))
{ /* Ieee/input.scm 373 */
BgL_tmpz00_10115 = BgL_lenz00_3458
; }  else 
{ 
 obj_t BgL_auxz00_10118;
BgL_auxz00_10118 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14393L), BGl_string4303z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_lenz00_3458); 
FAILURE(BgL_auxz00_10118,BFALSE,BFALSE);} 
BgL_n2z00_5442 = 
(long)CINT(BgL_tmpz00_10115); } 
BgL_test4942z00_10114 = 
(BgL_az00_3468<BgL_n2z00_5442); } 
if(BgL_test4942z00_10114)
{ /* Ieee/input.scm 373 */
BgL_arg2545z00_3467 = 
BINT(BgL_az00_3468); }  else 
{ /* Ieee/input.scm 373 */
BgL_arg2545z00_3467 = BgL_lenz00_3458; } } } 
{ /* Ieee/input.scm 373 */
 long BgL_tmpz00_10125;
{ /* Ieee/input.scm 373 */
 long BgL_auxz00_10133; obj_t BgL_tmpz00_10126;
{ /* Ieee/input.scm 373 */
 obj_t BgL_tmpz00_10135;
if(
INTEGERP(BgL_arg2545z00_3467))
{ /* Ieee/input.scm 373 */
BgL_tmpz00_10135 = BgL_arg2545z00_3467
; }  else 
{ 
 obj_t BgL_auxz00_10138;
BgL_auxz00_10138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14423L), BGl_string4303z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_arg2545z00_3467); 
FAILURE(BgL_auxz00_10138,BFALSE,BFALSE);} 
BgL_auxz00_10133 = 
(long)CINT(BgL_tmpz00_10135); } 
if(
INPUT_PORTP(BgL_ipz00_47))
{ /* Ieee/input.scm 373 */
BgL_tmpz00_10126 = BgL_ipz00_47
; }  else 
{ 
 obj_t BgL_auxz00_10129;
BgL_auxz00_10129 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14384L), BGl_string4303z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_47); 
FAILURE(BgL_auxz00_10129,BFALSE,BFALSE);} 
BgL_tmpz00_10125 = 
bgl_rgc_blit_string(BgL_tmpz00_10126, 
BSTRING_TO_STRING(BgL_bufz00_45), 0L, BgL_auxz00_10133); } 
return 
BINT(BgL_tmpz00_10125);} } } } } 

}



/* _read-fill-string! */
obj_t BGl__readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t BgL_env1298z00_55, obj_t BgL_opt1297z00_54)
{
{ /* Ieee/input.scm 378 */
{ /* Ieee/input.scm 378 */
 obj_t BgL_g1299z00_6936; obj_t BgL_g1300z00_6937; obj_t BgL_g1301z00_6938;
BgL_g1299z00_6936 = 
VECTOR_REF(BgL_opt1297z00_54,0L); 
BgL_g1300z00_6937 = 
VECTOR_REF(BgL_opt1297z00_54,1L); 
BgL_g1301z00_6938 = 
VECTOR_REF(BgL_opt1297z00_54,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1297z00_54)) { case 3L : 

{ /* Ieee/input.scm 378 */
 obj_t BgL_ipz00_6940;
{ /* Ieee/input.scm 378 */
 obj_t BgL_tmpz00_10148;
BgL_tmpz00_10148 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_6940 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_10148); } 
{ /* Ieee/input.scm 378 */

{ /* Ieee/input.scm 378 */
 obj_t BgL_sz00_6941; long BgL_oz00_6942; long BgL_lenz00_6943;
if(
STRINGP(BgL_g1299z00_6936))
{ /* Ieee/input.scm 378 */
BgL_sz00_6941 = BgL_g1299z00_6936; }  else 
{ 
 obj_t BgL_auxz00_10153;
BgL_auxz00_10153 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14652L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1299z00_6936); 
FAILURE(BgL_auxz00_10153,BFALSE,BFALSE);} 
{ /* Ieee/input.scm 378 */
 obj_t BgL_tmpz00_10157;
if(
INTEGERP(BgL_g1300z00_6937))
{ /* Ieee/input.scm 378 */
BgL_tmpz00_10157 = BgL_g1300z00_6937
; }  else 
{ 
 obj_t BgL_auxz00_10160;
BgL_auxz00_10160 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14652L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1300z00_6937); 
FAILURE(BgL_auxz00_10160,BFALSE,BFALSE);} 
BgL_oz00_6942 = 
(long)CINT(BgL_tmpz00_10157); } 
{ /* Ieee/input.scm 378 */
 obj_t BgL_tmpz00_10165;
if(
INTEGERP(BgL_g1301z00_6938))
{ /* Ieee/input.scm 378 */
BgL_tmpz00_10165 = BgL_g1301z00_6938
; }  else 
{ 
 obj_t BgL_auxz00_10168;
BgL_auxz00_10168 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14652L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1301z00_6938); 
FAILURE(BgL_auxz00_10168,BFALSE,BFALSE);} 
BgL_lenz00_6943 = 
(long)CINT(BgL_tmpz00_10165); } 
if(
(BgL_lenz00_6943<=0L))
{ /* Ieee/input.scm 386 */
if(
(BgL_lenz00_6943==0L))
{ /* Ieee/input.scm 387 */
return 
BINT(0L);}  else 
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2555z00_6944;
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_new1129z00_6945;
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_new1127z00_6946;
BgL_new1127z00_6946 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 390 */
 long BgL_arg2560z00_6947;
BgL_arg2560z00_6947 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1127z00_6946), BgL_arg2560z00_6947); } 
BgL_new1129z00_6945 = BgL_new1127z00_6946; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6945)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6945)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10186;
{ /* Ieee/input.scm 390 */
 obj_t BgL_arg2556z00_6948;
{ /* Ieee/input.scm 390 */
 obj_t BgL_arg2557z00_6949;
{ /* Ieee/input.scm 390 */
 obj_t BgL_classz00_6950;
BgL_classz00_6950 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2557z00_6949 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_6950); } 
BgL_arg2556z00_6948 = 
VECTOR_REF(BgL_arg2557z00_6949,2L); } 
{ /* Ieee/input.scm 390 */
 obj_t BgL_auxz00_10190;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2556z00_6948))
{ /* Ieee/input.scm 390 */
BgL_auxz00_10190 = BgL_arg2556z00_6948
; }  else 
{ 
 obj_t BgL_auxz00_10193;
BgL_auxz00_10193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15052L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2556z00_6948); 
FAILURE(BgL_auxz00_10193,BFALSE,BFALSE);} 
BgL_auxz00_10186 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10190); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6945)))->BgL_stackz00)=((obj_t)BgL_auxz00_10186),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6945)))->BgL_procz00)=((obj_t)BGl_symbol4296z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6945)))->BgL_msgz00)=((obj_t)BGl_string4301z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6945)))->BgL_objz00)=((obj_t)
BINT(BgL_lenz00_6943)),BUNSPEC); 
BgL_arg2555z00_6944 = BgL_new1129z00_6945; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2555z00_6944));} }  else 
{ /* Ieee/input.scm 394 */
 long BgL_nz00_6951;
{ /* Ieee/input.scm 395 */
 long BgL_arg2563z00_6952;
{ /* Ieee/input.scm 395 */
 long BgL_bz00_6953;
BgL_bz00_6953 = 
(
STRING_LENGTH(BgL_sz00_6941)-BgL_oz00_6942); 
if(
(BgL_lenz00_6943<BgL_bz00_6953))
{ /* Ieee/input.scm 395 */
BgL_arg2563z00_6952 = BgL_lenz00_6943; }  else 
{ /* Ieee/input.scm 395 */
BgL_arg2563z00_6952 = BgL_bz00_6953; } } 
BgL_nz00_6951 = 
bgl_rgc_blit_string(BgL_ipz00_6940, 
BSTRING_TO_STRING(BgL_sz00_6941), BgL_oz00_6942, BgL_arg2563z00_6952); } 
{ /* Ieee/input.scm 396 */
 bool_t BgL_test4953z00_10214;
if(
(BgL_nz00_6951==0L))
{ /* Ieee/input.scm 396 */
BgL_test4953z00_10214 = 
rgc_buffer_eof_p(BgL_ipz00_6940)
; }  else 
{ /* Ieee/input.scm 396 */
BgL_test4953z00_10214 = ((bool_t)0)
; } 
if(BgL_test4953z00_10214)
{ /* Ieee/input.scm 396 */
return BEOF;}  else 
{ /* Ieee/input.scm 396 */
return 
BINT(BgL_nz00_6951);} } } } } } break;case 4L : 

{ /* Ieee/input.scm 378 */
 obj_t BgL_ipz00_6954;
BgL_ipz00_6954 = 
VECTOR_REF(BgL_opt1297z00_54,3L); 
{ /* Ieee/input.scm 378 */

{ /* Ieee/input.scm 378 */
 obj_t BgL_sz00_6955; long BgL_oz00_6956; long BgL_lenz00_6957;
if(
STRINGP(BgL_g1299z00_6936))
{ /* Ieee/input.scm 378 */
BgL_sz00_6955 = BgL_g1299z00_6936; }  else 
{ 
 obj_t BgL_auxz00_10222;
BgL_auxz00_10222 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14652L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1299z00_6936); 
FAILURE(BgL_auxz00_10222,BFALSE,BFALSE);} 
{ /* Ieee/input.scm 378 */
 obj_t BgL_tmpz00_10226;
if(
INTEGERP(BgL_g1300z00_6937))
{ /* Ieee/input.scm 378 */
BgL_tmpz00_10226 = BgL_g1300z00_6937
; }  else 
{ 
 obj_t BgL_auxz00_10229;
BgL_auxz00_10229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14652L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1300z00_6937); 
FAILURE(BgL_auxz00_10229,BFALSE,BFALSE);} 
BgL_oz00_6956 = 
(long)CINT(BgL_tmpz00_10226); } 
{ /* Ieee/input.scm 378 */
 obj_t BgL_tmpz00_10234;
if(
INTEGERP(BgL_g1301z00_6938))
{ /* Ieee/input.scm 378 */
BgL_tmpz00_10234 = BgL_g1301z00_6938
; }  else 
{ 
 obj_t BgL_auxz00_10237;
BgL_auxz00_10237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(14652L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1301z00_6938); 
FAILURE(BgL_auxz00_10237,BFALSE,BFALSE);} 
BgL_lenz00_6957 = 
(long)CINT(BgL_tmpz00_10234); } 
if(
(BgL_lenz00_6957<=0L))
{ /* Ieee/input.scm 386 */
if(
(BgL_lenz00_6957==0L))
{ /* Ieee/input.scm 387 */
return 
BINT(0L);}  else 
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2555z00_6958;
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_new1129z00_6959;
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_new1127z00_6960;
BgL_new1127z00_6960 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 390 */
 long BgL_arg2560z00_6961;
BgL_arg2560z00_6961 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1127z00_6960), BgL_arg2560z00_6961); } 
BgL_new1129z00_6959 = BgL_new1127z00_6960; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6959)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6959)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10255;
{ /* Ieee/input.scm 390 */
 obj_t BgL_arg2556z00_6962;
{ /* Ieee/input.scm 390 */
 obj_t BgL_arg2557z00_6963;
{ /* Ieee/input.scm 390 */
 obj_t BgL_classz00_6964;
BgL_classz00_6964 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2557z00_6963 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_6964); } 
BgL_arg2556z00_6962 = 
VECTOR_REF(BgL_arg2557z00_6963,2L); } 
{ /* Ieee/input.scm 390 */
 obj_t BgL_auxz00_10259;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2556z00_6962))
{ /* Ieee/input.scm 390 */
BgL_auxz00_10259 = BgL_arg2556z00_6962
; }  else 
{ 
 obj_t BgL_auxz00_10262;
BgL_auxz00_10262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15052L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2556z00_6962); 
FAILURE(BgL_auxz00_10262,BFALSE,BFALSE);} 
BgL_auxz00_10255 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10259); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6959)))->BgL_stackz00)=((obj_t)BgL_auxz00_10255),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6959)))->BgL_procz00)=((obj_t)BGl_symbol4296z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6959)))->BgL_msgz00)=((obj_t)BGl_string4301z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6959)))->BgL_objz00)=((obj_t)
BINT(BgL_lenz00_6957)),BUNSPEC); 
BgL_arg2555z00_6958 = BgL_new1129z00_6959; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2555z00_6958));} }  else 
{ /* Ieee/input.scm 394 */
 long BgL_nz00_6965;
{ /* Ieee/input.scm 395 */
 long BgL_arg2563z00_6966;
{ /* Ieee/input.scm 395 */
 long BgL_bz00_6967;
BgL_bz00_6967 = 
(
STRING_LENGTH(BgL_sz00_6955)-BgL_oz00_6956); 
if(
(BgL_lenz00_6957<BgL_bz00_6967))
{ /* Ieee/input.scm 395 */
BgL_arg2563z00_6966 = BgL_lenz00_6957; }  else 
{ /* Ieee/input.scm 395 */
BgL_arg2563z00_6966 = BgL_bz00_6967; } } 
{ /* Ieee/input.scm 394 */
 obj_t BgL_tmpz00_10281;
if(
INPUT_PORTP(BgL_ipz00_6954))
{ /* Ieee/input.scm 394 */
BgL_tmpz00_10281 = BgL_ipz00_6954
; }  else 
{ 
 obj_t BgL_auxz00_10284;
BgL_auxz00_10284 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15193L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6954); 
FAILURE(BgL_auxz00_10284,BFALSE,BFALSE);} 
BgL_nz00_6965 = 
bgl_rgc_blit_string(BgL_tmpz00_10281, 
BSTRING_TO_STRING(BgL_sz00_6955), BgL_oz00_6956, BgL_arg2563z00_6966); } } 
{ /* Ieee/input.scm 396 */
 bool_t BgL_test4963z00_10290;
if(
(BgL_nz00_6965==0L))
{ /* Ieee/input.scm 396 */
 obj_t BgL_tmpz00_10293;
if(
INPUT_PORTP(BgL_ipz00_6954))
{ /* Ieee/input.scm 396 */
BgL_tmpz00_10293 = BgL_ipz00_6954
; }  else 
{ 
 obj_t BgL_auxz00_10296;
BgL_auxz00_10296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15294L), BGl_string4309z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6954); 
FAILURE(BgL_auxz00_10296,BFALSE,BFALSE);} 
BgL_test4963z00_10290 = 
rgc_buffer_eof_p(BgL_tmpz00_10293); }  else 
{ /* Ieee/input.scm 396 */
BgL_test4963z00_10290 = ((bool_t)0)
; } 
if(BgL_test4963z00_10290)
{ /* Ieee/input.scm 396 */
return BEOF;}  else 
{ /* Ieee/input.scm 396 */
return 
BINT(BgL_nz00_6965);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4306z00zz__r4_input_6_10_2z00, BGl_string4308z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1297z00_54)));} } } } 

}



/* read-fill-string! */
BGL_EXPORTED_DEF obj_t BGl_readzd2fillzd2stringz12z12zz__r4_input_6_10_2z00(obj_t BgL_sz00_50, long BgL_oz00_51, long BgL_lenz00_52, obj_t BgL_ipz00_53)
{
{ /* Ieee/input.scm 378 */
if(
(BgL_lenz00_52<=0L))
{ /* Ieee/input.scm 386 */
if(
(BgL_lenz00_52==0L))
{ /* Ieee/input.scm 387 */
return 
BINT(0L);}  else 
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2555z00_6968;
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_new1129z00_6969;
{ /* Ieee/input.scm 390 */
 BgL_z62iozd2errorzb0_bglt BgL_new1127z00_6970;
BgL_new1127z00_6970 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 390 */
 long BgL_arg2560z00_6971;
BgL_arg2560z00_6971 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1127z00_6970), BgL_arg2560z00_6971); } 
BgL_new1129z00_6969 = BgL_new1127z00_6970; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6969)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6969)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10320;
{ /* Ieee/input.scm 390 */
 obj_t BgL_arg2556z00_6972;
{ /* Ieee/input.scm 390 */
 obj_t BgL_arg2557z00_6973;
{ /* Ieee/input.scm 390 */
 obj_t BgL_classz00_6974;
BgL_classz00_6974 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2557z00_6973 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_6974); } 
BgL_arg2556z00_6972 = 
VECTOR_REF(BgL_arg2557z00_6973,2L); } 
{ /* Ieee/input.scm 390 */
 obj_t BgL_auxz00_10324;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2556z00_6972))
{ /* Ieee/input.scm 390 */
BgL_auxz00_10324 = BgL_arg2556z00_6972
; }  else 
{ 
 obj_t BgL_auxz00_10327;
BgL_auxz00_10327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15052L), BGl_string4307z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2556z00_6972); 
FAILURE(BgL_auxz00_10327,BFALSE,BFALSE);} 
BgL_auxz00_10320 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10324); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1129z00_6969)))->BgL_stackz00)=((obj_t)BgL_auxz00_10320),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6969)))->BgL_procz00)=((obj_t)BGl_symbol4296z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6969)))->BgL_msgz00)=((obj_t)BGl_string4301z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1129z00_6969)))->BgL_objz00)=((obj_t)
BINT(BgL_lenz00_52)),BUNSPEC); 
BgL_arg2555z00_6968 = BgL_new1129z00_6969; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2555z00_6968));} }  else 
{ /* Ieee/input.scm 394 */
 long BgL_nz00_6975;
{ /* Ieee/input.scm 395 */
 long BgL_arg2563z00_6976;
{ /* Ieee/input.scm 395 */
 long BgL_bz00_6977;
BgL_bz00_6977 = 
(
STRING_LENGTH(BgL_sz00_50)-BgL_oz00_51); 
if(
(BgL_lenz00_52<BgL_bz00_6977))
{ /* Ieee/input.scm 395 */
BgL_arg2563z00_6976 = BgL_lenz00_52; }  else 
{ /* Ieee/input.scm 395 */
BgL_arg2563z00_6976 = BgL_bz00_6977; } } 
{ /* Ieee/input.scm 394 */
 obj_t BgL_tmpz00_10346;
if(
INPUT_PORTP(BgL_ipz00_53))
{ /* Ieee/input.scm 394 */
BgL_tmpz00_10346 = BgL_ipz00_53
; }  else 
{ 
 obj_t BgL_auxz00_10349;
BgL_auxz00_10349 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15193L), BGl_string4307z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_53); 
FAILURE(BgL_auxz00_10349,BFALSE,BFALSE);} 
BgL_nz00_6975 = 
bgl_rgc_blit_string(BgL_tmpz00_10346, 
BSTRING_TO_STRING(BgL_sz00_50), BgL_oz00_51, BgL_arg2563z00_6976); } } 
{ /* Ieee/input.scm 396 */
 bool_t BgL_test4971z00_10355;
if(
(BgL_nz00_6975==0L))
{ /* Ieee/input.scm 396 */
 obj_t BgL_tmpz00_10358;
if(
INPUT_PORTP(BgL_ipz00_53))
{ /* Ieee/input.scm 396 */
BgL_tmpz00_10358 = BgL_ipz00_53
; }  else 
{ 
 obj_t BgL_auxz00_10361;
BgL_auxz00_10361 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15294L), BGl_string4307z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_53); 
FAILURE(BgL_auxz00_10361,BFALSE,BFALSE);} 
BgL_test4971z00_10355 = 
rgc_buffer_eof_p(BgL_tmpz00_10358); }  else 
{ /* Ieee/input.scm 396 */
BgL_test4971z00_10355 = ((bool_t)0)
; } 
if(BgL_test4971z00_10355)
{ /* Ieee/input.scm 396 */
return BEOF;}  else 
{ /* Ieee/input.scm 396 */
return 
BINT(BgL_nz00_6975);} } } } 

}



/* _unread-char! */
obj_t BGl__unreadzd2charz12zc0zz__r4_input_6_10_2z00(obj_t BgL_env1305z00_59, obj_t BgL_opt1304z00_58)
{
{ /* Ieee/input.scm 401 */
{ /* Ieee/input.scm 401 */
 obj_t BgL_g1306z00_3500;
BgL_g1306z00_3500 = 
VECTOR_REF(BgL_opt1304z00_58,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1304z00_58)) { case 1L : 

{ /* Ieee/input.scm 401 */
 obj_t BgL_ipz00_3503;
{ /* Ieee/input.scm 401 */
 obj_t BgL_tmpz00_10368;
BgL_tmpz00_10368 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3503 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_10368); } 
{ /* Ieee/input.scm 401 */

{ /* Ieee/input.scm 401 */
 unsigned char BgL_auxz00_10371;
{ /* Ieee/input.scm 401 */
 obj_t BgL_tmpz00_10372;
if(
CHARP(BgL_g1306z00_3500))
{ /* Ieee/input.scm 401 */
BgL_tmpz00_10372 = BgL_g1306z00_3500
; }  else 
{ 
 obj_t BgL_auxz00_10375;
BgL_auxz00_10375 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15535L), BGl_string4312z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_g1306z00_3500); 
FAILURE(BgL_auxz00_10375,BFALSE,BFALSE);} 
BgL_auxz00_10371 = 
CCHAR(BgL_tmpz00_10372); } 
return 
BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10371, BgL_ipz00_3503);} } } break;case 2L : 

{ /* Ieee/input.scm 401 */
 obj_t BgL_ipz00_3504;
BgL_ipz00_3504 = 
VECTOR_REF(BgL_opt1304z00_58,1L); 
{ /* Ieee/input.scm 401 */

{ /* Ieee/input.scm 401 */
 unsigned char BgL_auxz00_10382;
{ /* Ieee/input.scm 401 */
 obj_t BgL_tmpz00_10383;
if(
CHARP(BgL_g1306z00_3500))
{ /* Ieee/input.scm 401 */
BgL_tmpz00_10383 = BgL_g1306z00_3500
; }  else 
{ 
 obj_t BgL_auxz00_10386;
BgL_auxz00_10386 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15535L), BGl_string4312z00zz__r4_input_6_10_2z00, BGl_string4278z00zz__r4_input_6_10_2z00, BgL_g1306z00_3500); 
FAILURE(BgL_auxz00_10386,BFALSE,BFALSE);} 
BgL_auxz00_10382 = 
CCHAR(BgL_tmpz00_10383); } 
return 
BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10382, BgL_ipz00_3504);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4310z00zz__r4_input_6_10_2z00, BGl_string4298z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1304z00_58)));} } } } 

}



/* unread-char! */
BGL_EXPORTED_DEF obj_t BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(unsigned char BgL_cz00_56, obj_t BgL_ipz00_57)
{
{ /* Ieee/input.scm 401 */
{ /* Ieee/input.scm 402 */
 bool_t BgL_test4976z00_10397;
{ /* Ieee/input.scm 402 */
 long BgL_arg2585z00_3514;
BgL_arg2585z00_3514 = 
(
(unsigned char)(BgL_cz00_56)); 
{ /* Ieee/input.scm 402 */
 obj_t BgL_inputzd2portzd2_5547;
if(
INPUT_PORTP(BgL_ipz00_57))
{ /* Ieee/input.scm 402 */
BgL_inputzd2portzd2_5547 = BgL_ipz00_57; }  else 
{ 
 obj_t BgL_auxz00_10402;
BgL_auxz00_10402 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15639L), BGl_string4311z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_57); 
FAILURE(BgL_auxz00_10402,BFALSE,BFALSE);} 
BgL_test4976z00_10397 = 
rgc_buffer_insert_char(BgL_inputzd2portzd2_5547, 
(int)(BgL_arg2585z00_3514)); } } 
if(BgL_test4976z00_10397)
{ /* Ieee/input.scm 402 */
return BFALSE;}  else 
{ /* Ieee/input.scm 404 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2572z00_3508;
{ /* Ieee/input.scm 404 */
 BgL_z62iozd2errorzb0_bglt BgL_new1133z00_3509;
{ /* Ieee/input.scm 404 */
 BgL_z62iozd2errorzb0_bglt BgL_new1131z00_3512;
BgL_new1131z00_3512 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 404 */
 long BgL_arg2584z00_3513;
BgL_arg2584z00_3513 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1131z00_3512), BgL_arg2584z00_3513); } 
BgL_new1133z00_3509 = BgL_new1131z00_3512; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1133z00_3509)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1133z00_3509)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10416;
{ /* Ieee/input.scm 404 */
 obj_t BgL_arg2578z00_3510;
{ /* Ieee/input.scm 404 */
 obj_t BgL_arg2579z00_3511;
{ /* Ieee/input.scm 404 */
 obj_t BgL_classz00_5552;
BgL_classz00_5552 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2579z00_3511 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5552); } 
BgL_arg2578z00_3510 = 
VECTOR_REF(BgL_arg2579z00_3511,2L); } 
{ /* Ieee/input.scm 404 */
 obj_t BgL_auxz00_10420;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2578z00_3510))
{ /* Ieee/input.scm 404 */
BgL_auxz00_10420 = BgL_arg2578z00_3510
; }  else 
{ 
 obj_t BgL_auxz00_10423;
BgL_auxz00_10423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15676L), BGl_string4311z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2578z00_3510); 
FAILURE(BgL_auxz00_10423,BFALSE,BFALSE);} 
BgL_auxz00_10416 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10420); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1133z00_3509)))->BgL_stackz00)=((obj_t)BgL_auxz00_10416),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1133z00_3509)))->BgL_procz00)=((obj_t)BGl_symbol4310z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1133z00_3509)))->BgL_msgz00)=((obj_t)BGl_string4313z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1133z00_3509)))->BgL_objz00)=((obj_t)
BCHAR(BgL_cz00_56)),BUNSPEC); 
BgL_arg2572z00_3508 = BgL_new1133z00_3509; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2572z00_3508));} } } 

}



/* _unread-string! */
obj_t BGl__unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t BgL_env1310z00_63, obj_t BgL_opt1309z00_62)
{
{ /* Ieee/input.scm 412 */
{ /* Ieee/input.scm 412 */
 obj_t BgL_g1311z00_3515;
BgL_g1311z00_3515 = 
VECTOR_REF(BgL_opt1309z00_62,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1309z00_62)) { case 1L : 

{ /* Ieee/input.scm 412 */
 obj_t BgL_ipz00_3518;
{ /* Ieee/input.scm 412 */
 obj_t BgL_tmpz00_10439;
BgL_tmpz00_10439 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3518 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_10439); } 
{ /* Ieee/input.scm 412 */

{ /* Ieee/input.scm 412 */
 obj_t BgL_auxz00_10442;
if(
STRINGP(BgL_g1311z00_3515))
{ /* Ieee/input.scm 412 */
BgL_auxz00_10442 = BgL_g1311z00_3515
; }  else 
{ 
 obj_t BgL_auxz00_10445;
BgL_auxz00_10445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15998L), BGl_string4316z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1311z00_3515); 
FAILURE(BgL_auxz00_10445,BFALSE,BFALSE);} 
return 
BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10442, BgL_ipz00_3518);} } } break;case 2L : 

{ /* Ieee/input.scm 412 */
 obj_t BgL_ipz00_3519;
BgL_ipz00_3519 = 
VECTOR_REF(BgL_opt1309z00_62,1L); 
{ /* Ieee/input.scm 412 */

{ /* Ieee/input.scm 412 */
 obj_t BgL_auxz00_10451;
if(
STRINGP(BgL_g1311z00_3515))
{ /* Ieee/input.scm 412 */
BgL_auxz00_10451 = BgL_g1311z00_3515
; }  else 
{ 
 obj_t BgL_auxz00_10454;
BgL_auxz00_10454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(15998L), BGl_string4316z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1311z00_3515); 
FAILURE(BgL_auxz00_10454,BFALSE,BFALSE);} 
return 
BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10451, BgL_ipz00_3519);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4314z00zz__r4_input_6_10_2z00, BGl_string4298z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1309z00_62)));} } } } 

}



/* unread-string! */
BGL_EXPORTED_DEF obj_t BGl_unreadzd2stringz12zc0zz__r4_input_6_10_2z00(obj_t BgL_strz00_60, obj_t BgL_ipz00_61)
{
{ /* Ieee/input.scm 412 */
{ /* Ieee/input.scm 413 */
 bool_t BgL_test4981z00_10464;
{ /* Ieee/input.scm 413 */
 long BgL_arg2594z00_3529;
BgL_arg2594z00_3529 = 
STRING_LENGTH(BgL_strz00_60); 
{ /* Ieee/input.scm 413 */
 obj_t BgL_inputzd2portzd2_5556;
if(
INPUT_PORTP(BgL_ipz00_61))
{ /* Ieee/input.scm 413 */
BgL_inputzd2portzd2_5556 = BgL_ipz00_61; }  else 
{ 
 obj_t BgL_auxz00_10468;
BgL_auxz00_10468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16117L), BGl_string4315z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_61); 
FAILURE(BgL_auxz00_10468,BFALSE,BFALSE);} 
BgL_test4981z00_10464 = 
rgc_buffer_insert_substring(BgL_inputzd2portzd2_5556, BgL_strz00_60, 0L, BgL_arg2594z00_3529); } } 
if(BgL_test4981z00_10464)
{ /* Ieee/input.scm 413 */
return BFALSE;}  else 
{ /* Ieee/input.scm 415 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2589z00_3523;
{ /* Ieee/input.scm 415 */
 BgL_z62iozd2errorzb0_bglt BgL_new1136z00_3524;
{ /* Ieee/input.scm 415 */
 BgL_z62iozd2errorzb0_bglt BgL_new1135z00_3527;
BgL_new1135z00_3527 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 415 */
 long BgL_arg2592z00_3528;
BgL_arg2592z00_3528 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1135z00_3527), BgL_arg2592z00_3528); } 
BgL_new1136z00_3524 = BgL_new1135z00_3527; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1136z00_3524)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1136z00_3524)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10481;
{ /* Ieee/input.scm 415 */
 obj_t BgL_arg2590z00_3525;
{ /* Ieee/input.scm 415 */
 obj_t BgL_arg2591z00_3526;
{ /* Ieee/input.scm 415 */
 obj_t BgL_classz00_5562;
BgL_classz00_5562 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2591z00_3526 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5562); } 
BgL_arg2590z00_3525 = 
VECTOR_REF(BgL_arg2591z00_3526,2L); } 
{ /* Ieee/input.scm 415 */
 obj_t BgL_auxz00_10485;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2590z00_3525))
{ /* Ieee/input.scm 415 */
BgL_auxz00_10485 = BgL_arg2590z00_3525
; }  else 
{ 
 obj_t BgL_auxz00_10488;
BgL_auxz00_10488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16168L), BGl_string4315z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2590z00_3525); 
FAILURE(BgL_auxz00_10488,BFALSE,BFALSE);} 
BgL_auxz00_10481 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10485); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1136z00_3524)))->BgL_stackz00)=((obj_t)BgL_auxz00_10481),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1136z00_3524)))->BgL_procz00)=((obj_t)BGl_symbol4314z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1136z00_3524)))->BgL_msgz00)=((obj_t)BGl_string4317z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1136z00_3524)))->BgL_objz00)=((obj_t)BgL_strz00_60),BUNSPEC); 
BgL_arg2589z00_3523 = BgL_new1136z00_3524; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2589z00_3523));} } } 

}



/* _unread-substring! */
obj_t BGl__unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t BgL_env1315z00_69, obj_t BgL_opt1314z00_68)
{
{ /* Ieee/input.scm 423 */
{ /* Ieee/input.scm 423 */
 obj_t BgL_g1316z00_3530; obj_t BgL_g1317z00_3531; obj_t BgL_g1318z00_3532;
BgL_g1316z00_3530 = 
VECTOR_REF(BgL_opt1314z00_68,0L); 
BgL_g1317z00_3531 = 
VECTOR_REF(BgL_opt1314z00_68,1L); 
BgL_g1318z00_3532 = 
VECTOR_REF(BgL_opt1314z00_68,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1314z00_68)) { case 3L : 

{ /* Ieee/input.scm 424 */
 obj_t BgL_ipz00_3535;
{ /* Ieee/input.scm 424 */
 obj_t BgL_tmpz00_10505;
BgL_tmpz00_10505 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_ipz00_3535 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_10505); } 
{ /* Ieee/input.scm 423 */

{ /* Ieee/input.scm 423 */
 long BgL_auxz00_10524; long BgL_auxz00_10515; obj_t BgL_auxz00_10508;
{ /* Ieee/input.scm 423 */
 obj_t BgL_tmpz00_10525;
if(
INTEGERP(BgL_g1318z00_3532))
{ /* Ieee/input.scm 423 */
BgL_tmpz00_10525 = BgL_g1318z00_3532
; }  else 
{ 
 obj_t BgL_auxz00_10528;
BgL_auxz00_10528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16490L), BGl_string4320z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1318z00_3532); 
FAILURE(BgL_auxz00_10528,BFALSE,BFALSE);} 
BgL_auxz00_10524 = 
(long)CINT(BgL_tmpz00_10525); } 
{ /* Ieee/input.scm 423 */
 obj_t BgL_tmpz00_10516;
if(
INTEGERP(BgL_g1317z00_3531))
{ /* Ieee/input.scm 423 */
BgL_tmpz00_10516 = BgL_g1317z00_3531
; }  else 
{ 
 obj_t BgL_auxz00_10519;
BgL_auxz00_10519 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16490L), BGl_string4320z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1317z00_3531); 
FAILURE(BgL_auxz00_10519,BFALSE,BFALSE);} 
BgL_auxz00_10515 = 
(long)CINT(BgL_tmpz00_10516); } 
if(
STRINGP(BgL_g1316z00_3530))
{ /* Ieee/input.scm 423 */
BgL_auxz00_10508 = BgL_g1316z00_3530
; }  else 
{ 
 obj_t BgL_auxz00_10511;
BgL_auxz00_10511 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16490L), BGl_string4320z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1316z00_3530); 
FAILURE(BgL_auxz00_10511,BFALSE,BFALSE);} 
return 
BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10508, BgL_auxz00_10515, BgL_auxz00_10524, BgL_ipz00_3535);} } } break;case 4L : 

{ /* Ieee/input.scm 423 */
 obj_t BgL_ipz00_3536;
BgL_ipz00_3536 = 
VECTOR_REF(BgL_opt1314z00_68,3L); 
{ /* Ieee/input.scm 423 */

{ /* Ieee/input.scm 423 */
 long BgL_auxz00_10551; long BgL_auxz00_10542; obj_t BgL_auxz00_10535;
{ /* Ieee/input.scm 423 */
 obj_t BgL_tmpz00_10552;
if(
INTEGERP(BgL_g1318z00_3532))
{ /* Ieee/input.scm 423 */
BgL_tmpz00_10552 = BgL_g1318z00_3532
; }  else 
{ 
 obj_t BgL_auxz00_10555;
BgL_auxz00_10555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16490L), BGl_string4320z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1318z00_3532); 
FAILURE(BgL_auxz00_10555,BFALSE,BFALSE);} 
BgL_auxz00_10551 = 
(long)CINT(BgL_tmpz00_10552); } 
{ /* Ieee/input.scm 423 */
 obj_t BgL_tmpz00_10543;
if(
INTEGERP(BgL_g1317z00_3531))
{ /* Ieee/input.scm 423 */
BgL_tmpz00_10543 = BgL_g1317z00_3531
; }  else 
{ 
 obj_t BgL_auxz00_10546;
BgL_auxz00_10546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16490L), BGl_string4320z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_g1317z00_3531); 
FAILURE(BgL_auxz00_10546,BFALSE,BFALSE);} 
BgL_auxz00_10542 = 
(long)CINT(BgL_tmpz00_10543); } 
if(
STRINGP(BgL_g1316z00_3530))
{ /* Ieee/input.scm 423 */
BgL_auxz00_10535 = BgL_g1316z00_3530
; }  else 
{ 
 obj_t BgL_auxz00_10538;
BgL_auxz00_10538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16490L), BGl_string4320z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_g1316z00_3530); 
FAILURE(BgL_auxz00_10538,BFALSE,BFALSE);} 
return 
BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_10535, BgL_auxz00_10542, BgL_auxz00_10551, BgL_ipz00_3536);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4318z00zz__r4_input_6_10_2z00, BGl_string4308z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1314z00_68)));} } } } 

}



/* unread-substring! */
BGL_EXPORTED_DEF obj_t BGl_unreadzd2substringz12zc0zz__r4_input_6_10_2z00(obj_t BgL_strz00_64, long BgL_fromz00_65, long BgL_toz00_66, obj_t BgL_ipz00_67)
{
{ /* Ieee/input.scm 423 */
{ /* Ieee/input.scm 425 */
 bool_t BgL_test4990z00_10566;
if(
(BgL_toz00_66>=BgL_fromz00_65))
{ /* Ieee/input.scm 425 */
if(
(BgL_fromz00_65<0L))
{ /* Ieee/input.scm 426 */
BgL_test4990z00_10566 = ((bool_t)1)
; }  else 
{ /* Ieee/input.scm 426 */
BgL_test4990z00_10566 = 
(BgL_toz00_66>
STRING_LENGTH(BgL_strz00_64))
; } }  else 
{ /* Ieee/input.scm 425 */
BgL_test4990z00_10566 = ((bool_t)1)
; } 
if(BgL_test4990z00_10566)
{ /* Ieee/input.scm 428 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2601z00_3542;
{ /* Ieee/input.scm 428 */
 BgL_z62iozd2errorzb0_bglt BgL_new1139z00_3543;
{ /* Ieee/input.scm 428 */
 BgL_z62iozd2errorzb0_bglt BgL_new1138z00_3550;
BgL_new1138z00_3550 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 428 */
 long BgL_arg2611z00_3551;
BgL_arg2611z00_3551 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1138z00_3550), BgL_arg2611z00_3551); } 
BgL_new1139z00_3543 = BgL_new1138z00_3550; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1139z00_3543)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1139z00_3543)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10581;
{ /* Ieee/input.scm 428 */
 obj_t BgL_arg2604z00_3544;
{ /* Ieee/input.scm 428 */
 obj_t BgL_arg2605z00_3545;
{ /* Ieee/input.scm 428 */
 obj_t BgL_classz00_5574;
BgL_classz00_5574 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2605z00_3545 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5574); } 
BgL_arg2604z00_3544 = 
VECTOR_REF(BgL_arg2605z00_3545,2L); } 
{ /* Ieee/input.scm 428 */
 obj_t BgL_auxz00_10585;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2604z00_3544))
{ /* Ieee/input.scm 428 */
BgL_auxz00_10585 = BgL_arg2604z00_3544
; }  else 
{ 
 obj_t BgL_auxz00_10588;
BgL_auxz00_10588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16684L), BGl_string4319z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2604z00_3544); 
FAILURE(BgL_auxz00_10588,BFALSE,BFALSE);} 
BgL_auxz00_10581 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10585); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1139z00_3543)))->BgL_stackz00)=((obj_t)BgL_auxz00_10581),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1139z00_3543)))->BgL_procz00)=((obj_t)BGl_symbol4318z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1139z00_3543)))->BgL_msgz00)=((obj_t)BGl_string4321z00zz__r4_input_6_10_2z00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10598;
{ /* Ieee/input.scm 431 */
 long BgL_arg2607z00_3546;
BgL_arg2607z00_3546 = 
STRING_LENGTH(BgL_strz00_64); 
{ /* Ieee/input.scm 431 */
 obj_t BgL_list2608z00_3547;
{ /* Ieee/input.scm 431 */
 obj_t BgL_arg2609z00_3548;
{ /* Ieee/input.scm 431 */
 obj_t BgL_arg2610z00_3549;
BgL_arg2610z00_3549 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2607z00_3546), BNIL); 
BgL_arg2609z00_3548 = 
MAKE_YOUNG_PAIR(
BINT(BgL_toz00_66), BgL_arg2610z00_3549); } 
BgL_list2608z00_3547 = 
MAKE_YOUNG_PAIR(
BINT(BgL_fromz00_65), BgL_arg2609z00_3548); } 
BgL_auxz00_10598 = BgL_list2608z00_3547; } } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1139z00_3543)))->BgL_objz00)=((obj_t)BgL_auxz00_10598),BUNSPEC); } 
BgL_arg2601z00_3542 = BgL_new1139z00_3543; } 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2601z00_3542)); }  else 
{ /* Ieee/input.scm 425 */BFALSE; } } 
{ /* Ieee/input.scm 432 */
 bool_t BgL_test4994z00_10610;
{ /* Ieee/input.scm 432 */
 obj_t BgL_inputzd2portzd2_5578;
if(
INPUT_PORTP(BgL_ipz00_67))
{ /* Ieee/input.scm 432 */
BgL_inputzd2portzd2_5578 = BgL_ipz00_67; }  else 
{ 
 obj_t BgL_auxz00_10613;
BgL_auxz00_10613 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16866L), BGl_string4319z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_67); 
FAILURE(BgL_auxz00_10613,BFALSE,BFALSE);} 
BgL_test4994z00_10610 = 
rgc_buffer_insert_substring(BgL_inputzd2portzd2_5578, BgL_strz00_64, BgL_fromz00_65, BgL_toz00_66); } 
if(BgL_test4994z00_10610)
{ /* Ieee/input.scm 432 */
return BFALSE;}  else 
{ /* Ieee/input.scm 433 */
 BgL_z62iozd2errorzb0_bglt BgL_arg2614z00_3556;
{ /* Ieee/input.scm 433 */
 BgL_z62iozd2errorzb0_bglt BgL_new1141z00_3557;
{ /* Ieee/input.scm 433 */
 BgL_z62iozd2errorzb0_bglt BgL_new1140z00_3560;
BgL_new1140z00_3560 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Ieee/input.scm 433 */
 long BgL_arg2618z00_3561;
BgL_arg2618z00_3561 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1140z00_3560), BgL_arg2618z00_3561); } 
BgL_new1141z00_3557 = BgL_new1140z00_3560; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1141z00_3557)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1141z00_3557)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_10626;
{ /* Ieee/input.scm 433 */
 obj_t BgL_arg2615z00_3558;
{ /* Ieee/input.scm 433 */
 obj_t BgL_arg2617z00_3559;
{ /* Ieee/input.scm 433 */
 obj_t BgL_classz00_5585;
BgL_classz00_5585 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg2617z00_3559 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5585); } 
BgL_arg2615z00_3558 = 
VECTOR_REF(BgL_arg2617z00_3559,2L); } 
{ /* Ieee/input.scm 433 */
 obj_t BgL_auxz00_10630;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2615z00_3558))
{ /* Ieee/input.scm 433 */
BgL_auxz00_10630 = BgL_arg2615z00_3558
; }  else 
{ 
 obj_t BgL_auxz00_10633;
BgL_auxz00_10633 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(16896L), BGl_string4319z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2615z00_3558); 
FAILURE(BgL_auxz00_10633,BFALSE,BFALSE);} 
BgL_auxz00_10626 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_10630); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1141z00_3557)))->BgL_stackz00)=((obj_t)BgL_auxz00_10626),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1141z00_3557)))->BgL_procz00)=((obj_t)BGl_symbol4322z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1141z00_3557)))->BgL_msgz00)=((obj_t)BGl_string4317z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1141z00_3557)))->BgL_objz00)=((obj_t)BgL_strz00_64),BUNSPEC); 
BgL_arg2614z00_3556 = BgL_new1141z00_3557; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2614z00_3556));} } } 

}



/* port->string-list */
BGL_EXPORTED_DEF obj_t BGl_portzd2ze3stringzd2listze3zz__r4_input_6_10_2z00(obj_t BgL_ipz00_70)
{
{ /* Ieee/input.scm 441 */
{ 
 obj_t BgL_resz00_3564;
BgL_resz00_3564 = BNIL; 
BgL_zc3z04anonymousza32619ze3z87_3565:
{ /* Ieee/input.scm 443 */
 obj_t BgL_expz00_3566;
BgL_expz00_3566 = 
BGl_z62zc3z04anonymousza31401ze3ze5zz__r4_input_6_10_2z00(BGl_za2readzd2ofzd2stringszd2grammarza2zd2zz__r4_input_6_10_2z00, BgL_ipz00_70); 
if(
EOF_OBJECTP(BgL_expz00_3566))
{ /* Ieee/input.scm 444 */
return 
bgl_reverse_bang(BgL_resz00_3564);}  else 
{ /* Ieee/input.scm 446 */
 obj_t BgL_arg2621z00_3568;
BgL_arg2621z00_3568 = 
MAKE_YOUNG_PAIR(BgL_expz00_3566, BgL_resz00_3564); 
{ 
 obj_t BgL_resz00_10655;
BgL_resz00_10655 = BgL_arg2621z00_3568; 
BgL_resz00_3564 = BgL_resz00_10655; 
goto BgL_zc3z04anonymousza32619ze3z87_3565;} } } } } 

}



/* &port->string-list */
obj_t BGl_z62portzd2ze3stringzd2listz81zz__r4_input_6_10_2z00(obj_t BgL_envz00_6002, obj_t BgL_ipz00_6003)
{
{ /* Ieee/input.scm 441 */
{ /* Ieee/input.scm 443 */
 obj_t BgL_auxz00_10656;
if(
INPUT_PORTP(BgL_ipz00_6003))
{ /* Ieee/input.scm 443 */
BgL_auxz00_10656 = BgL_ipz00_6003
; }  else 
{ 
 obj_t BgL_auxz00_10659;
BgL_auxz00_10659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(17279L), BGl_string4324z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6003); 
FAILURE(BgL_auxz00_10659,BFALSE,BFALSE);} 
return 
BGl_portzd2ze3stringzd2listze3zz__r4_input_6_10_2z00(BgL_auxz00_10656);} } 

}



/* %sendchars */
int BGl_z52sendcharsz52zz__r4_input_6_10_2z00(obj_t BgL_ipz00_71, obj_t BgL_opz00_72, long BgL_sza7za7_73, long BgL_offsetz00_74)
{
{ /* Ieee/input.scm 455 */
if(
(BgL_offsetz00_74>=0L))
{ /* Ieee/input.scm 456 */
BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(BgL_ipz00_71, BgL_offsetz00_74); }  else 
{ /* Ieee/input.scm 456 */BFALSE; } 
{ /* Ieee/input.scm 457 */
 long BgL_bufsiza7eza7_3571;
if(
(BgL_sza7za7_73==-1L))
{ /* Ieee/input.scm 458 */
BgL_bufsiza7eza7_3571 = 
BGL_INPUT_PORT_BUFSIZ(BgL_ipz00_71); }  else 
{ /* Ieee/input.scm 458 */
if(
(default_io_bufsiz<BgL_sza7za7_73))
{ /* Ieee/input.scm 460 */
BgL_bufsiza7eza7_3571 = default_io_bufsiz; }  else 
{ /* Ieee/input.scm 460 */
BgL_bufsiza7eza7_3571 = BgL_sza7za7_73; } } 
{ /* Ieee/input.scm 457 */
 obj_t BgL_bufz00_3572;
{ /* Ieee/string.scm 172 */

BgL_bufz00_3572 = 
make_string(BgL_bufsiza7eza7_3571, ((unsigned char)' ')); } 
{ /* Ieee/input.scm 466 */

if(
(BgL_sza7za7_73<0L))
{ 
 long BgL_charszd2readzd2_3577;
{ /* Ieee/input.scm 468 */
 long BgL_tmpz00_10675;
BgL_charszd2readzd2_3577 = 0L; 
BgL_zc3z04anonymousza32624ze3z87_3578:
{ /* Ieee/input.scm 469 */
 obj_t BgL_nz00_3579;
BgL_nz00_3579 = 
BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(BgL_bufz00_3572, 
BINT(BgL_bufsiza7eza7_3571), BgL_ipz00_71); 
{ /* Ieee/input.scm 470 */
 bool_t BgL_test5003z00_10678;
{ /* Ieee/input.scm 470 */
 long BgL_n1z00_5593;
{ /* Ieee/input.scm 470 */
 obj_t BgL_tmpz00_10679;
if(
INTEGERP(BgL_nz00_3579))
{ /* Ieee/input.scm 470 */
BgL_tmpz00_10679 = BgL_nz00_3579
; }  else 
{ 
 obj_t BgL_auxz00_10682;
BgL_auxz00_10682 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18414L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3579); 
FAILURE(BgL_auxz00_10682,BFALSE,BFALSE);} 
BgL_n1z00_5593 = 
(long)CINT(BgL_tmpz00_10679); } 
BgL_test5003z00_10678 = 
(BgL_n1z00_5593==0L); } 
if(BgL_test5003z00_10678)
{ /* Ieee/input.scm 470 */
bgl_flush_output_port(BgL_opz00_72); 
BgL_tmpz00_10675 = BgL_charszd2readzd2_3577; }  else 
{ /* Ieee/input.scm 474 */
 obj_t BgL_sz00_3581;
{ /* Ieee/input.scm 474 */
 bool_t BgL_test5005z00_10689;
{ /* Ieee/input.scm 474 */
 long BgL_n1z00_5595;
{ /* Ieee/input.scm 474 */
 obj_t BgL_tmpz00_10690;
if(
INTEGERP(BgL_nz00_3579))
{ /* Ieee/input.scm 474 */
BgL_tmpz00_10690 = BgL_nz00_3579
; }  else 
{ 
 obj_t BgL_auxz00_10693;
BgL_auxz00_10693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18509L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3579); 
FAILURE(BgL_auxz00_10693,BFALSE,BFALSE);} 
BgL_n1z00_5595 = 
(long)CINT(BgL_tmpz00_10690); } 
BgL_test5005z00_10689 = 
(BgL_n1z00_5595<BgL_bufsiza7eza7_3571); } 
if(BgL_test5005z00_10689)
{ /* Ieee/input.scm 474 */
 long BgL_auxz00_10699;
{ /* Ieee/input.scm 474 */
 obj_t BgL_tmpz00_10700;
if(
INTEGERP(BgL_nz00_3579))
{ /* Ieee/input.scm 474 */
BgL_tmpz00_10700 = BgL_nz00_3579
; }  else 
{ 
 obj_t BgL_auxz00_10703;
BgL_auxz00_10703 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18520L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3579); 
FAILURE(BgL_auxz00_10703,BFALSE,BFALSE);} 
BgL_auxz00_10699 = 
(long)CINT(BgL_tmpz00_10700); } 
BgL_sz00_3581 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_bufz00_3572, 0L, BgL_auxz00_10699); }  else 
{ /* Ieee/input.scm 474 */
BgL_sz00_3581 = BgL_bufz00_3572; } } 
bgl_display_obj(BgL_sz00_3581, BgL_opz00_72); 
{ /* Ieee/input.scm 476 */
 long BgL_arg2626z00_3582;
{ /* Ieee/input.scm 476 */
 long BgL_za72za7_5598;
{ /* Ieee/input.scm 476 */
 obj_t BgL_tmpz00_10710;
if(
INTEGERP(BgL_nz00_3579))
{ /* Ieee/input.scm 476 */
BgL_tmpz00_10710 = BgL_nz00_3579
; }  else 
{ 
 obj_t BgL_auxz00_10713;
BgL_auxz00_10713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18602L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3579); 
FAILURE(BgL_auxz00_10713,BFALSE,BFALSE);} 
BgL_za72za7_5598 = 
(long)CINT(BgL_tmpz00_10710); } 
BgL_arg2626z00_3582 = 
(BgL_charszd2readzd2_3577+BgL_za72za7_5598); } 
{ 
 long BgL_charszd2readzd2_10719;
BgL_charszd2readzd2_10719 = BgL_arg2626z00_3582; 
BgL_charszd2readzd2_3577 = BgL_charszd2readzd2_10719; 
goto BgL_zc3z04anonymousza32624ze3z87_3578;} } } } } 
return 
(int)(BgL_tmpz00_10675);} }  else 
{ 
 long BgL_charszd2readzd2_3586; long BgL_charszd2tozd2readz00_3587; long BgL_sza7za7_3588;
{ /* Ieee/input.scm 477 */
 long BgL_tmpz00_10721;
BgL_charszd2readzd2_3586 = 0L; 
BgL_charszd2tozd2readz00_3587 = BgL_bufsiza7eza7_3571; 
BgL_sza7za7_3588 = BgL_sza7za7_73; 
BgL_zc3z04anonymousza32628ze3z87_3589:
if(
(BgL_charszd2tozd2readz00_3587==0L))
{ /* Ieee/input.scm 480 */
BgL_tmpz00_10721 = BgL_charszd2readzd2_3586
; }  else 
{ /* Ieee/input.scm 482 */
 obj_t BgL_nz00_3591;
BgL_nz00_3591 = 
BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(BgL_bufz00_3572, 
BINT(BgL_charszd2tozd2readz00_3587), BgL_ipz00_71); 
{ /* Ieee/input.scm 483 */
 bool_t BgL_test5010z00_10726;
{ /* Ieee/input.scm 483 */
 long BgL_n1z00_5600;
{ /* Ieee/input.scm 483 */
 obj_t BgL_tmpz00_10727;
if(
INTEGERP(BgL_nz00_3591))
{ /* Ieee/input.scm 483 */
BgL_tmpz00_10727 = BgL_nz00_3591
; }  else 
{ 
 obj_t BgL_auxz00_10730;
BgL_auxz00_10730 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18796L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3591); 
FAILURE(BgL_auxz00_10730,BFALSE,BFALSE);} 
BgL_n1z00_5600 = 
(long)CINT(BgL_tmpz00_10727); } 
BgL_test5010z00_10726 = 
(BgL_n1z00_5600==0L); } 
if(BgL_test5010z00_10726)
{ /* Ieee/input.scm 483 */
bgl_flush_output_port(BgL_opz00_72); 
BgL_tmpz00_10721 = BgL_charszd2readzd2_3586; }  else 
{ /* Ieee/input.scm 487 */
 obj_t BgL_sz00_3593;
{ /* Ieee/input.scm 487 */
 bool_t BgL_test5012z00_10737;
{ /* Ieee/input.scm 487 */
 long BgL_n1z00_5602;
{ /* Ieee/input.scm 487 */
 obj_t BgL_tmpz00_10738;
if(
INTEGERP(BgL_nz00_3591))
{ /* Ieee/input.scm 487 */
BgL_tmpz00_10738 = BgL_nz00_3591
; }  else 
{ 
 obj_t BgL_auxz00_10741;
BgL_auxz00_10741 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18879L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3591); 
FAILURE(BgL_auxz00_10741,BFALSE,BFALSE);} 
BgL_n1z00_5602 = 
(long)CINT(BgL_tmpz00_10738); } 
BgL_test5012z00_10737 = 
(BgL_n1z00_5602<BgL_bufsiza7eza7_3571); } 
if(BgL_test5012z00_10737)
{ /* Ieee/input.scm 487 */
 long BgL_auxz00_10747;
{ /* Ieee/input.scm 487 */
 obj_t BgL_tmpz00_10748;
if(
INTEGERP(BgL_nz00_3591))
{ /* Ieee/input.scm 487 */
BgL_tmpz00_10748 = BgL_nz00_3591
; }  else 
{ 
 obj_t BgL_auxz00_10751;
BgL_auxz00_10751 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18890L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3591); 
FAILURE(BgL_auxz00_10751,BFALSE,BFALSE);} 
BgL_auxz00_10747 = 
(long)CINT(BgL_tmpz00_10748); } 
BgL_sz00_3593 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_bufz00_3572, 0L, BgL_auxz00_10747); }  else 
{ /* Ieee/input.scm 487 */
BgL_sz00_3593 = BgL_bufz00_3572; } } 
bgl_display_obj(BgL_sz00_3593, BgL_opz00_72); 
{ /* Ieee/input.scm 489 */
 long BgL_sza7za7_3594;
{ /* Ieee/input.scm 489 */
 long BgL_za72za7_5605;
{ /* Ieee/input.scm 489 */
 obj_t BgL_tmpz00_10758;
if(
INTEGERP(BgL_nz00_3591))
{ /* Ieee/input.scm 489 */
BgL_tmpz00_10758 = BgL_nz00_3591
; }  else 
{ 
 obj_t BgL_auxz00_10761;
BgL_auxz00_10761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(18963L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3591); 
FAILURE(BgL_auxz00_10761,BFALSE,BFALSE);} 
BgL_za72za7_5605 = 
(long)CINT(BgL_tmpz00_10758); } 
BgL_sza7za7_3594 = 
(BgL_sza7za7_3588-BgL_za72za7_5605); } 
{ /* Ieee/input.scm 489 */
 long BgL_ctrz00_3595;
if(
(BgL_sza7za7_3594<BgL_bufsiza7eza7_3571))
{ /* Ieee/input.scm 490 */
BgL_ctrz00_3595 = BgL_sza7za7_3594; }  else 
{ /* Ieee/input.scm 490 */
BgL_ctrz00_3595 = BgL_bufsiza7eza7_3571; } 
{ /* Ieee/input.scm 490 */

{ /* Ieee/input.scm 491 */
 long BgL_arg2631z00_3596;
{ /* Ieee/input.scm 491 */
 long BgL_za72za7_5609;
{ /* Ieee/input.scm 491 */
 obj_t BgL_tmpz00_10769;
if(
INTEGERP(BgL_nz00_3591))
{ /* Ieee/input.scm 491 */
BgL_tmpz00_10769 = BgL_nz00_3591
; }  else 
{ 
 obj_t BgL_auxz00_10772;
BgL_auxz00_10772 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(19044L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_nz00_3591); 
FAILURE(BgL_auxz00_10772,BFALSE,BFALSE);} 
BgL_za72za7_5609 = 
(long)CINT(BgL_tmpz00_10769); } 
BgL_arg2631z00_3596 = 
(BgL_charszd2readzd2_3586+BgL_za72za7_5609); } 
{ 
 long BgL_sza7za7_10780; long BgL_charszd2tozd2readz00_10779; long BgL_charszd2readzd2_10778;
BgL_charszd2readzd2_10778 = BgL_arg2631z00_3596; 
BgL_charszd2tozd2readz00_10779 = BgL_ctrz00_3595; 
BgL_sza7za7_10780 = BgL_sza7za7_3594; 
BgL_sza7za7_3588 = BgL_sza7za7_10780; 
BgL_charszd2tozd2readz00_3587 = BgL_charszd2tozd2readz00_10779; 
BgL_charszd2readzd2_3586 = BgL_charszd2readzd2_10778; 
goto BgL_zc3z04anonymousza32628ze3z87_3589;} } } } } } } } 
return 
(int)(BgL_tmpz00_10721);} } } } } } 

}



/* file->string */
BGL_EXPORTED_DEF obj_t BGl_filezd2ze3stringz31zz__r4_input_6_10_2z00(obj_t BgL_pathz00_75)
{
{ /* Ieee/input.scm 496 */
{ /* Ieee/input.scm 499 */
 obj_t BgL_iz00_3604;
{ /* Ieee/string.scm 223 */

BgL_iz00_3604 = 
BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_pathz00_75, 
BCHAR(((unsigned char)':')), 
BINT(0L)); } 
if(
CBOOL(BgL_iz00_3604))
{ /* Ieee/input.scm 503 */
 bool_t BgL_test5019z00_10787;
{ /* Ieee/input.scm 503 */

BgL_test5019z00_10787 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BGl_string4325z00zz__r4_input_6_10_2z00, BgL_pathz00_75, BFALSE, BFALSE, BFALSE, BFALSE); } 
if(BgL_test5019z00_10787)
{ /* Ieee/input.scm 504 */
 obj_t BgL_arg2637z00_3612;
BgL_arg2637z00_3612 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_pathz00_75, 5L, 
STRING_LENGTH(BgL_pathz00_75)); 
return 
bgl_file_to_string(
BSTRING_TO_STRING(BgL_arg2637z00_3612));}  else 
{ /* Ieee/input.scm 506 */
 obj_t BgL_ipz00_3614;
{ /* Ieee/port.scm 466 */

BgL_ipz00_3614 = 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_pathz00_75, BTRUE, 
BINT(5000000L)); } 
{ /* Ieee/input.scm 507 */
 obj_t BgL_exitd1143z00_3615;
BgL_exitd1143z00_3615 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/input.scm 509 */
 obj_t BgL_zc3z04anonymousza32639ze3z87_6004;
BgL_zc3z04anonymousza32639ze3z87_6004 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32639ze3ze5zz__r4_input_6_10_2z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza32639ze3z87_6004, 
(int)(0L), BgL_ipz00_3614); 
{ /* Ieee/input.scm 507 */
 obj_t BgL_arg3410z00_5612;
{ /* Ieee/input.scm 507 */
 obj_t BgL_arg3413z00_5613;
BgL_arg3413z00_5613 = 
BGL_EXITD_PROTECT(BgL_exitd1143z00_3615); 
BgL_arg3410z00_5612 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32639ze3z87_6004, BgL_arg3413z00_5613); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1143z00_3615, BgL_arg3410z00_5612); BUNSPEC; } 
{ /* Ieee/input.scm 508 */
 obj_t BgL_tmp1145z00_3617;
BgL_tmp1145z00_3617 = 
BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_ipz00_3614); 
{ /* Ieee/input.scm 507 */
 bool_t BgL_test5020z00_10805;
{ /* Ieee/input.scm 507 */
 obj_t BgL_arg3407z00_5615;
BgL_arg3407z00_5615 = 
BGL_EXITD_PROTECT(BgL_exitd1143z00_3615); 
BgL_test5020z00_10805 = 
PAIRP(BgL_arg3407z00_5615); } 
if(BgL_test5020z00_10805)
{ /* Ieee/input.scm 507 */
 obj_t BgL_arg3405z00_5616;
{ /* Ieee/input.scm 507 */
 obj_t BgL_arg3406z00_5617;
BgL_arg3406z00_5617 = 
BGL_EXITD_PROTECT(BgL_exitd1143z00_3615); 
{ /* Ieee/input.scm 507 */
 obj_t BgL_pairz00_5618;
if(
PAIRP(BgL_arg3406z00_5617))
{ /* Ieee/input.scm 507 */
BgL_pairz00_5618 = BgL_arg3406z00_5617; }  else 
{ 
 obj_t BgL_auxz00_10811;
BgL_auxz00_10811 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(19600L), BGl_string4326z00zz__r4_input_6_10_2z00, BGl_string4248z00zz__r4_input_6_10_2z00, BgL_arg3406z00_5617); 
FAILURE(BgL_auxz00_10811,BFALSE,BFALSE);} 
BgL_arg3405z00_5616 = 
CDR(BgL_pairz00_5618); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1143z00_3615, BgL_arg3405z00_5616); BUNSPEC; }  else 
{ /* Ieee/input.scm 507 */BFALSE; } } 
{ /* Ieee/input.scm 509 */
 obj_t BgL_portz00_5619;
if(
INPUT_PORTP(BgL_ipz00_3614))
{ /* Ieee/input.scm 509 */
BgL_portz00_5619 = BgL_ipz00_3614; }  else 
{ 
 obj_t BgL_auxz00_10819;
BgL_auxz00_10819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(19663L), BGl_string4326z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_3614); 
FAILURE(BgL_auxz00_10819,BFALSE,BFALSE);} 
bgl_close_input_port(BgL_portz00_5619); } 
return BgL_tmp1145z00_3617;} } } } }  else 
{ /* Ieee/input.scm 501 */
return 
bgl_file_to_string(
BSTRING_TO_STRING(BgL_pathz00_75));} } } 

}



/* &file->string */
obj_t BGl_z62filezd2ze3stringz53zz__r4_input_6_10_2z00(obj_t BgL_envz00_6005, obj_t BgL_pathz00_6006)
{
{ /* Ieee/input.scm 496 */
{ /* Ieee/input.scm 499 */
 obj_t BgL_auxz00_10826;
if(
STRINGP(BgL_pathz00_6006))
{ /* Ieee/input.scm 499 */
BgL_auxz00_10826 = BgL_pathz00_6006
; }  else 
{ 
 obj_t BgL_auxz00_10829;
BgL_auxz00_10829 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(19354L), BGl_string4327z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_pathz00_6006); 
FAILURE(BgL_auxz00_10829,BFALSE,BFALSE);} 
return 
BGl_filezd2ze3stringz31zz__r4_input_6_10_2z00(BgL_auxz00_10826);} } 

}



/* &<@anonymous:2639> */
obj_t BGl_z62zc3z04anonymousza32639ze3ze5zz__r4_input_6_10_2z00(obj_t BgL_envz00_6007)
{
{ /* Ieee/input.scm 507 */
{ /* Ieee/input.scm 509 */
 obj_t BgL_ipz00_6008;
BgL_ipz00_6008 = 
PROCEDURE_REF(BgL_envz00_6007, 
(int)(0L)); 
{ /* Ieee/input.scm 509 */
 obj_t BgL_portz00_6978;
if(
INPUT_PORTP(BgL_ipz00_6008))
{ /* Ieee/input.scm 509 */
BgL_portz00_6978 = BgL_ipz00_6008; }  else 
{ 
 obj_t BgL_auxz00_10838;
BgL_auxz00_10838 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(19663L), BGl_string4328z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6008); 
FAILURE(BgL_auxz00_10838,BFALSE,BFALSE);} 
return 
bgl_close_input_port(BgL_portz00_6978);} } } 

}



/* send-chars/size */
BGL_EXPORTED_DEF long BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(obj_t BgL_ipz00_76, obj_t BgL_opz00_77, long BgL_siza7eza7_78, long BgL_offsetz00_79)
{
{ /* Ieee/input.scm 519 */
{ /* Ieee/input.scm 521 */
 long BgL_sza7za7_3632; long BgL_offz00_3633;
BgL_sza7za7_3632 = 
(long)(BgL_siza7eza7_78); 
BgL_offz00_3633 = 
(long)(BgL_offsetz00_79); 
{ /* Ieee/input.scm 525 */
 obj_t BgL__ortest_1147z00_3634;
BgL__ortest_1147z00_3634 = 
bgl_sendchars(BgL_ipz00_76, BgL_opz00_77, BgL_sza7za7_3632, BgL_offz00_3633); 
if(
CBOOL(BgL__ortest_1147z00_3634))
{ /* Ieee/input.scm 525 */
 obj_t BgL_tmpz00_10848;
if(
INTEGERP(BgL__ortest_1147z00_3634))
{ /* Ieee/input.scm 525 */
BgL_tmpz00_10848 = BgL__ortest_1147z00_3634
; }  else 
{ 
 obj_t BgL_auxz00_10851;
BgL_auxz00_10851 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20229L), BGl_string4329z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL__ortest_1147z00_3634); 
FAILURE(BgL_auxz00_10851,BFALSE,BFALSE);} 
return 
(long)CINT(BgL_tmpz00_10848);}  else 
{ /* Ieee/input.scm 527 */
 bool_t BgL_test5027z00_10856;
if(
INPUT_GZIP_PORTP(BgL_ipz00_76))
{ /* Ieee/input.scm 527 */
if(
(BgL_sza7za7_3632==-1L))
{ /* Ieee/input.scm 527 */
BgL_test5027z00_10856 = 
(BgL_offz00_3633==-1L)
; }  else 
{ /* Ieee/input.scm 527 */
BgL_test5027z00_10856 = ((bool_t)0)
; } }  else 
{ /* Ieee/input.scm 527 */
BgL_test5027z00_10856 = ((bool_t)0)
; } 
if(BgL_test5027z00_10856)
{ /* Ieee/input.scm 528 */
 obj_t BgL_tmpz00_10862;
{ /* Ieee/input.scm 528 */
 obj_t BgL_aux4114z00_6668;
BgL_aux4114z00_6668 = 
BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(BgL_ipz00_76, BgL_opz00_77); 
if(
INTEGERP(BgL_aux4114z00_6668))
{ /* Ieee/input.scm 528 */
BgL_tmpz00_10862 = BgL_aux4114z00_6668
; }  else 
{ 
 obj_t BgL_auxz00_10866;
BgL_auxz00_10866 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20329L), BGl_string4329z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_aux4114z00_6668); 
FAILURE(BgL_auxz00_10866,BFALSE,BFALSE);} } 
return 
(long)CINT(BgL_tmpz00_10862);}  else 
{ /* Ieee/input.scm 527 */
return 
(long)(
BGl_z52sendcharsz52zz__r4_input_6_10_2z00(BgL_ipz00_76, BgL_opz00_77, BgL_sza7za7_3632, BgL_offz00_3633));} } } } } 

}



/* &send-chars/size */
obj_t BGl_z62sendzd2charszf2siza7eze5zz__r4_input_6_10_2z00(obj_t BgL_envz00_6009, obj_t BgL_ipz00_6010, obj_t BgL_opz00_6011, obj_t BgL_siza7eza7_6012, obj_t BgL_offsetz00_6013)
{
{ /* Ieee/input.scm 519 */
{ /* Ieee/input.scm 521 */
 long BgL_tmpz00_10873;
{ /* Ieee/input.scm 521 */
 long BgL_auxz00_10897; long BgL_auxz00_10888; obj_t BgL_auxz00_10881; obj_t BgL_auxz00_10874;
{ /* Ieee/input.scm 521 */
 obj_t BgL_tmpz00_10898;
if(
ELONGP(BgL_offsetz00_6013))
{ /* Ieee/input.scm 521 */
BgL_tmpz00_10898 = BgL_offsetz00_6013
; }  else 
{ 
 obj_t BgL_auxz00_10901;
BgL_auxz00_10901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20115L), BGl_string4330z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_offsetz00_6013); 
FAILURE(BgL_auxz00_10901,BFALSE,BFALSE);} 
BgL_auxz00_10897 = 
BELONG_TO_LONG(BgL_tmpz00_10898); } 
{ /* Ieee/input.scm 521 */
 obj_t BgL_tmpz00_10889;
if(
ELONGP(BgL_siza7eza7_6012))
{ /* Ieee/input.scm 521 */
BgL_tmpz00_10889 = BgL_siza7eza7_6012
; }  else 
{ 
 obj_t BgL_auxz00_10892;
BgL_auxz00_10892 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20115L), BGl_string4330z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_siza7eza7_6012); 
FAILURE(BgL_auxz00_10892,BFALSE,BFALSE);} 
BgL_auxz00_10888 = 
BELONG_TO_LONG(BgL_tmpz00_10889); } 
if(
OUTPUT_PORTP(BgL_opz00_6011))
{ /* Ieee/input.scm 521 */
BgL_auxz00_10881 = BgL_opz00_6011
; }  else 
{ 
 obj_t BgL_auxz00_10884;
BgL_auxz00_10884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20115L), BGl_string4330z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_opz00_6011); 
FAILURE(BgL_auxz00_10884,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_ipz00_6010))
{ /* Ieee/input.scm 521 */
BgL_auxz00_10874 = BgL_ipz00_6010
; }  else 
{ 
 obj_t BgL_auxz00_10877;
BgL_auxz00_10877 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20115L), BGl_string4330z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6010); 
FAILURE(BgL_auxz00_10877,BFALSE,BFALSE);} 
BgL_tmpz00_10873 = 
BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(BgL_auxz00_10874, BgL_auxz00_10881, BgL_auxz00_10888, BgL_auxz00_10897); } 
return 
BINT(BgL_tmpz00_10873);} } 

}



/* _send-chars */
obj_t BGl__sendzd2charszd2zz__r4_input_6_10_2z00(obj_t BgL_env1322z00_85, obj_t BgL_opt1321z00_84)
{
{ /* Ieee/input.scm 541 */
{ /* Ieee/input.scm 541 */
 obj_t BgL_g1323z00_3640; obj_t BgL_g1324z00_3641;
BgL_g1323z00_3640 = 
VECTOR_REF(BgL_opt1321z00_84,0L); 
BgL_g1324z00_3641 = 
VECTOR_REF(BgL_opt1321z00_84,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1321z00_84)) { case 2L : 

{ /* Ieee/input.scm 541 */

{ /* Ieee/input.scm 541 */
 long BgL_tmpz00_10910;
{ /* Ieee/input.scm 541 */
 obj_t BgL_auxz00_10918; obj_t BgL_auxz00_10911;
if(
OUTPUT_PORTP(BgL_g1324z00_3641))
{ /* Ieee/input.scm 541 */
BgL_auxz00_10918 = BgL_g1324z00_3641
; }  else 
{ 
 obj_t BgL_auxz00_10921;
BgL_auxz00_10921 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20782L), BGl_string4336z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_g1324z00_3641); 
FAILURE(BgL_auxz00_10921,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_g1323z00_3640))
{ /* Ieee/input.scm 541 */
BgL_auxz00_10911 = BgL_g1323z00_3640
; }  else 
{ 
 obj_t BgL_auxz00_10914;
BgL_auxz00_10914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20782L), BGl_string4336z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_g1323z00_3640); 
FAILURE(BgL_auxz00_10914,BFALSE,BFALSE);} 
BgL_tmpz00_10910 = 
BGl_sendzd2charszd2zz__r4_input_6_10_2z00(BgL_auxz00_10911, BgL_auxz00_10918, 
BINT(-1L), 
BINT(-1L)); } 
return 
BINT(BgL_tmpz00_10910);} } break;case 3L : 

{ /* Ieee/input.scm 541 */
 obj_t BgL_siza7eza7_3646;
BgL_siza7eza7_3646 = 
VECTOR_REF(BgL_opt1321z00_84,2L); 
{ /* Ieee/input.scm 541 */

{ /* Ieee/input.scm 541 */
 long BgL_tmpz00_10930;
{ /* Ieee/input.scm 541 */
 obj_t BgL_auxz00_10938; obj_t BgL_auxz00_10931;
if(
OUTPUT_PORTP(BgL_g1324z00_3641))
{ /* Ieee/input.scm 541 */
BgL_auxz00_10938 = BgL_g1324z00_3641
; }  else 
{ 
 obj_t BgL_auxz00_10941;
BgL_auxz00_10941 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20782L), BGl_string4336z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_g1324z00_3641); 
FAILURE(BgL_auxz00_10941,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_g1323z00_3640))
{ /* Ieee/input.scm 541 */
BgL_auxz00_10931 = BgL_g1323z00_3640
; }  else 
{ 
 obj_t BgL_auxz00_10934;
BgL_auxz00_10934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20782L), BGl_string4336z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_g1323z00_3640); 
FAILURE(BgL_auxz00_10934,BFALSE,BFALSE);} 
BgL_tmpz00_10930 = 
BGl_sendzd2charszd2zz__r4_input_6_10_2z00(BgL_auxz00_10931, BgL_auxz00_10938, BgL_siza7eza7_3646, 
BINT(-1L)); } 
return 
BINT(BgL_tmpz00_10930);} } } break;case 4L : 

{ /* Ieee/input.scm 541 */
 obj_t BgL_siza7eza7_3648;
BgL_siza7eza7_3648 = 
VECTOR_REF(BgL_opt1321z00_84,2L); 
{ /* Ieee/input.scm 541 */
 obj_t BgL_offsetz00_3649;
BgL_offsetz00_3649 = 
VECTOR_REF(BgL_opt1321z00_84,3L); 
{ /* Ieee/input.scm 541 */

{ /* Ieee/input.scm 541 */
 long BgL_tmpz00_10950;
{ /* Ieee/input.scm 541 */
 obj_t BgL_auxz00_10958; obj_t BgL_auxz00_10951;
if(
OUTPUT_PORTP(BgL_g1324z00_3641))
{ /* Ieee/input.scm 541 */
BgL_auxz00_10958 = BgL_g1324z00_3641
; }  else 
{ 
 obj_t BgL_auxz00_10961;
BgL_auxz00_10961 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20782L), BGl_string4336z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_g1324z00_3641); 
FAILURE(BgL_auxz00_10961,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_g1323z00_3640))
{ /* Ieee/input.scm 541 */
BgL_auxz00_10951 = BgL_g1323z00_3640
; }  else 
{ 
 obj_t BgL_auxz00_10954;
BgL_auxz00_10954 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20782L), BGl_string4336z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_g1323z00_3640); 
FAILURE(BgL_auxz00_10954,BFALSE,BFALSE);} 
BgL_tmpz00_10950 = 
BGl_sendzd2charszd2zz__r4_input_6_10_2z00(BgL_auxz00_10951, BgL_auxz00_10958, BgL_siza7eza7_3648, BgL_offsetz00_3649); } 
return 
BINT(BgL_tmpz00_10950);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4333z00zz__r4_input_6_10_2z00, BGl_string4335z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1321z00_84)));} } } } 

}



/* send-chars */
BGL_EXPORTED_DEF long BGl_sendzd2charszd2zz__r4_input_6_10_2z00(obj_t BgL_ipz00_80, obj_t BgL_opz00_81, obj_t BgL_siza7eza7_82, obj_t BgL_offsetz00_83)
{
{ /* Ieee/input.scm 541 */
{ /* Ieee/input.scm 543 */
 long BgL_sza7za7_3651; long BgL_offz00_3652;
if(
INTEGERP(BgL_siza7eza7_82))
{ /* Ieee/input.scm 544 */
 long BgL_tmpz00_10974;
BgL_tmpz00_10974 = 
(long)CINT(BgL_siza7eza7_82); 
BgL_sza7za7_3651 = 
(long)(BgL_tmpz00_10974); }  else 
{ /* Ieee/input.scm 544 */
if(
ELONGP(BgL_siza7eza7_82))
{ /* Ieee/input.scm 545 */
BgL_sza7za7_3651 = 
BELONG_TO_LONG(BgL_siza7eza7_82); }  else 
{ /* Ieee/input.scm 546 */
 obj_t BgL_tmpz00_10980;
{ /* Ieee/input.scm 546 */
 obj_t BgL_aux4135z00_6689;
BgL_aux4135z00_6689 = 
BGl_errorz00zz__errorz00(BGl_symbol4337z00zz__r4_input_6_10_2z00, BGl_string4339z00zz__r4_input_6_10_2z00, BgL_siza7eza7_82); 
if(
ELONGP(BgL_aux4135z00_6689))
{ /* Ieee/input.scm 546 */
BgL_tmpz00_10980 = BgL_aux4135z00_6689
; }  else 
{ 
 obj_t BgL_auxz00_10984;
BgL_auxz00_10984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(20996L), BGl_string4338z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_aux4135z00_6689); 
FAILURE(BgL_auxz00_10984,BFALSE,BFALSE);} } 
BgL_sza7za7_3651 = 
BELONG_TO_LONG(BgL_tmpz00_10980); } } 
if(
INTEGERP(BgL_offsetz00_83))
{ /* Ieee/input.scm 548 */
 long BgL_tmpz00_10991;
BgL_tmpz00_10991 = 
(long)CINT(BgL_offsetz00_83); 
BgL_offz00_3652 = 
(long)(BgL_tmpz00_10991); }  else 
{ /* Ieee/input.scm 548 */
if(
ELONGP(BgL_offsetz00_83))
{ /* Ieee/input.scm 549 */
BgL_offz00_3652 = 
BELONG_TO_LONG(BgL_offsetz00_83); }  else 
{ /* Ieee/input.scm 550 */
 obj_t BgL_tmpz00_10997;
{ /* Ieee/input.scm 550 */
 obj_t BgL_aux4137z00_6691;
BgL_aux4137z00_6691 = 
BGl_errorz00zz__errorz00(BGl_symbol4337z00zz__r4_input_6_10_2z00, BGl_string4340z00zz__r4_input_6_10_2z00, BgL_offsetz00_83); 
if(
ELONGP(BgL_aux4137z00_6691))
{ /* Ieee/input.scm 550 */
BgL_tmpz00_10997 = BgL_aux4137z00_6691
; }  else 
{ 
 obj_t BgL_auxz00_11001;
BgL_auxz00_11001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21141L), BGl_string4338z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_aux4137z00_6691); 
FAILURE(BgL_auxz00_11001,BFALSE,BFALSE);} } 
BgL_offz00_3652 = 
BELONG_TO_LONG(BgL_tmpz00_10997); } } 
return 
BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(BgL_ipz00_80, BgL_opz00_81, BgL_sza7za7_3651, BgL_offz00_3652);} } 

}



/* _send-file */
obj_t BGl__sendzd2filezd2zz__r4_input_6_10_2z00(obj_t BgL_env1328z00_91, obj_t BgL_opt1327z00_90)
{
{ /* Ieee/input.scm 556 */
{ /* Ieee/input.scm 556 */
 obj_t BgL_filez00_3657; obj_t BgL_opz00_3658;
BgL_filez00_3657 = 
VECTOR_REF(BgL_opt1327z00_90,0L); 
BgL_opz00_3658 = 
VECTOR_REF(BgL_opt1327z00_90,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1327z00_90)) { case 2L : 

{ /* Ieee/input.scm 556 */

{ /* Ieee/input.scm 556 */
 long BgL_tmpz00_11009;
{ /* Ieee/input.scm 556 */
 obj_t BgL_auxz00_11017; obj_t BgL_auxz00_11010;
if(
OUTPUT_PORTP(BgL_opz00_3658))
{ /* Ieee/input.scm 556 */
BgL_auxz00_11017 = BgL_opz00_3658
; }  else 
{ 
 obj_t BgL_auxz00_11020;
BgL_auxz00_11020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_opz00_3658); 
FAILURE(BgL_auxz00_11020,BFALSE,BFALSE);} 
if(
STRINGP(BgL_filez00_3657))
{ /* Ieee/input.scm 556 */
BgL_auxz00_11010 = BgL_filez00_3657
; }  else 
{ 
 obj_t BgL_auxz00_11013;
BgL_auxz00_11013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_filez00_3657); 
FAILURE(BgL_auxz00_11013,BFALSE,BFALSE);} 
BgL_tmpz00_11009 = 
BGl_sendzd2filezd2zz__r4_input_6_10_2z00(BgL_auxz00_11010, BgL_auxz00_11017, ((long)-1), ((long)-1)); } 
return 
BINT(BgL_tmpz00_11009);} } break;case 3L : 

{ /* Ieee/input.scm 556 */
 obj_t BgL_siza7eza7_3663;
BgL_siza7eza7_3663 = 
VECTOR_REF(BgL_opt1327z00_90,2L); 
{ /* Ieee/input.scm 556 */

{ /* Ieee/input.scm 556 */
 long BgL_tmpz00_11027;
{ /* Ieee/input.scm 556 */
 long BgL_auxz00_11042; obj_t BgL_auxz00_11035; obj_t BgL_auxz00_11028;
{ /* Ieee/input.scm 556 */
 obj_t BgL_tmpz00_11043;
if(
ELONGP(BgL_siza7eza7_3663))
{ /* Ieee/input.scm 556 */
BgL_tmpz00_11043 = BgL_siza7eza7_3663
; }  else 
{ 
 obj_t BgL_auxz00_11046;
BgL_auxz00_11046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_siza7eza7_3663); 
FAILURE(BgL_auxz00_11046,BFALSE,BFALSE);} 
BgL_auxz00_11042 = 
BELONG_TO_LONG(BgL_tmpz00_11043); } 
if(
OUTPUT_PORTP(BgL_opz00_3658))
{ /* Ieee/input.scm 556 */
BgL_auxz00_11035 = BgL_opz00_3658
; }  else 
{ 
 obj_t BgL_auxz00_11038;
BgL_auxz00_11038 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_opz00_3658); 
FAILURE(BgL_auxz00_11038,BFALSE,BFALSE);} 
if(
STRINGP(BgL_filez00_3657))
{ /* Ieee/input.scm 556 */
BgL_auxz00_11028 = BgL_filez00_3657
; }  else 
{ 
 obj_t BgL_auxz00_11031;
BgL_auxz00_11031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_filez00_3657); 
FAILURE(BgL_auxz00_11031,BFALSE,BFALSE);} 
BgL_tmpz00_11027 = 
BGl_sendzd2filezd2zz__r4_input_6_10_2z00(BgL_auxz00_11028, BgL_auxz00_11035, BgL_auxz00_11042, ((long)-1)); } 
return 
BINT(BgL_tmpz00_11027);} } } break;case 4L : 

{ /* Ieee/input.scm 556 */
 obj_t BgL_siza7eza7_3665;
BgL_siza7eza7_3665 = 
VECTOR_REF(BgL_opt1327z00_90,2L); 
{ /* Ieee/input.scm 556 */
 obj_t BgL_offsetz00_3666;
BgL_offsetz00_3666 = 
VECTOR_REF(BgL_opt1327z00_90,3L); 
{ /* Ieee/input.scm 556 */

{ /* Ieee/input.scm 556 */
 long BgL_tmpz00_11055;
{ /* Ieee/input.scm 556 */
 long BgL_auxz00_11079; long BgL_auxz00_11070; obj_t BgL_auxz00_11063; obj_t BgL_auxz00_11056;
{ /* Ieee/input.scm 556 */
 obj_t BgL_tmpz00_11080;
if(
ELONGP(BgL_offsetz00_3666))
{ /* Ieee/input.scm 556 */
BgL_tmpz00_11080 = BgL_offsetz00_3666
; }  else 
{ 
 obj_t BgL_auxz00_11083;
BgL_auxz00_11083 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_offsetz00_3666); 
FAILURE(BgL_auxz00_11083,BFALSE,BFALSE);} 
BgL_auxz00_11079 = 
BELONG_TO_LONG(BgL_tmpz00_11080); } 
{ /* Ieee/input.scm 556 */
 obj_t BgL_tmpz00_11071;
if(
ELONGP(BgL_siza7eza7_3665))
{ /* Ieee/input.scm 556 */
BgL_tmpz00_11071 = BgL_siza7eza7_3665
; }  else 
{ 
 obj_t BgL_auxz00_11074;
BgL_auxz00_11074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4332z00zz__r4_input_6_10_2z00, BgL_siza7eza7_3665); 
FAILURE(BgL_auxz00_11074,BFALSE,BFALSE);} 
BgL_auxz00_11070 = 
BELONG_TO_LONG(BgL_tmpz00_11071); } 
if(
OUTPUT_PORTP(BgL_opz00_3658))
{ /* Ieee/input.scm 556 */
BgL_auxz00_11063 = BgL_opz00_3658
; }  else 
{ 
 obj_t BgL_auxz00_11066;
BgL_auxz00_11066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4331z00zz__r4_input_6_10_2z00, BgL_opz00_3658); 
FAILURE(BgL_auxz00_11066,BFALSE,BFALSE);} 
if(
STRINGP(BgL_filez00_3657))
{ /* Ieee/input.scm 556 */
BgL_auxz00_11056 = BgL_filez00_3657
; }  else 
{ 
 obj_t BgL_auxz00_11059;
BgL_auxz00_11059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21451L), BGl_string4343z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_filez00_3657); 
FAILURE(BgL_auxz00_11059,BFALSE,BFALSE);} 
BgL_tmpz00_11055 = 
BGl_sendzd2filezd2zz__r4_input_6_10_2z00(BgL_auxz00_11056, BgL_auxz00_11063, BgL_auxz00_11070, BgL_auxz00_11079); } 
return 
BINT(BgL_tmpz00_11055);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4341z00zz__r4_input_6_10_2z00, BGl_string4335z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1327z00_90)));} } } } 

}



/* send-file */
BGL_EXPORTED_DEF long BGl_sendzd2filezd2zz__r4_input_6_10_2z00(obj_t BgL_filez00_86, obj_t BgL_opz00_87, long BgL_siza7eza7_88, long BgL_offsetz00_89)
{
{ /* Ieee/input.scm 556 */
{ /* Ieee/input.scm 560 */
 long BgL_sza7za7_3668; long BgL_offz00_3669;
BgL_sza7za7_3668 = 
(long)(BgL_siza7eza7_88); 
BgL_offz00_3669 = 
(long)(BgL_offsetz00_89); 
{ /* Ieee/input.scm 564 */
 obj_t BgL__ortest_1150z00_3670;
BgL__ortest_1150z00_3670 = 
bgl_sendfile(BgL_filez00_86, BgL_opz00_87, 
(long)(BgL_siza7eza7_88), 
(long)(BgL_offsetz00_89)); 
if(
CBOOL(BgL__ortest_1150z00_3670))
{ /* Ieee/input.scm 564 */
 obj_t BgL_tmpz00_11102;
if(
INTEGERP(BgL__ortest_1150z00_3670))
{ /* Ieee/input.scm 564 */
BgL_tmpz00_11102 = BgL__ortest_1150z00_3670
; }  else 
{ 
 obj_t BgL_auxz00_11105;
BgL_auxz00_11105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21690L), BGl_string4344z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL__ortest_1150z00_3670); 
FAILURE(BgL_auxz00_11105,BFALSE,BFALSE);} 
return 
(long)CINT(BgL_tmpz00_11102);}  else 
{ /* Ieee/input.scm 565 */
 obj_t BgL_ipz00_3671;
{ /* Ieee/port.scm 466 */

BgL_ipz00_3671 = 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_filez00_86, BTRUE, 
BINT(5000000L)); } 
{ /* Ieee/input.scm 566 */
 obj_t BgL_exitd1151z00_3672;
BgL_exitd1151z00_3672 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/input.scm 568 */
 obj_t BgL_zc3z04anonymousza32654ze3z87_6014;
BgL_zc3z04anonymousza32654ze3z87_6014 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32654ze3ze5zz__r4_input_6_10_2z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza32654ze3z87_6014, 
(int)(0L), BgL_ipz00_3671); 
{ /* Ieee/input.scm 566 */
 obj_t BgL_arg3410z00_5629;
{ /* Ieee/input.scm 566 */
 obj_t BgL_arg3413z00_5630;
BgL_arg3413z00_5630 = 
BGL_EXITD_PROTECT(BgL_exitd1151z00_3672); 
BgL_arg3410z00_5629 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32654ze3z87_6014, BgL_arg3413z00_5630); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1151z00_3672, BgL_arg3410z00_5629); BUNSPEC; } 
{ /* Ieee/input.scm 567 */
 long BgL_tmp1153z00_3674;
{ /* Ieee/input.scm 567 */
 obj_t BgL_auxz00_11121;
if(
INPUT_PORTP(BgL_ipz00_3671))
{ /* Ieee/input.scm 567 */
BgL_auxz00_11121 = BgL_ipz00_3671
; }  else 
{ 
 obj_t BgL_auxz00_11124;
BgL_auxz00_11124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21806L), BGl_string4344z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_3671); 
FAILURE(BgL_auxz00_11124,BFALSE,BFALSE);} 
BgL_tmp1153z00_3674 = 
BGl_sendzd2charszf2siza7ez87zz__r4_input_6_10_2z00(BgL_auxz00_11121, BgL_opz00_87, BgL_siza7eza7_88, BgL_offsetz00_89); } 
{ /* Ieee/input.scm 566 */
 bool_t BgL_test5059z00_11129;
{ /* Ieee/input.scm 566 */
 obj_t BgL_arg3407z00_5632;
BgL_arg3407z00_5632 = 
BGL_EXITD_PROTECT(BgL_exitd1151z00_3672); 
BgL_test5059z00_11129 = 
PAIRP(BgL_arg3407z00_5632); } 
if(BgL_test5059z00_11129)
{ /* Ieee/input.scm 566 */
 obj_t BgL_arg3405z00_5633;
{ /* Ieee/input.scm 566 */
 obj_t BgL_arg3406z00_5634;
BgL_arg3406z00_5634 = 
BGL_EXITD_PROTECT(BgL_exitd1151z00_3672); 
{ /* Ieee/input.scm 566 */
 obj_t BgL_pairz00_5635;
if(
PAIRP(BgL_arg3406z00_5634))
{ /* Ieee/input.scm 566 */
BgL_pairz00_5635 = BgL_arg3406z00_5634; }  else 
{ 
 obj_t BgL_auxz00_11135;
BgL_auxz00_11135 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21767L), BGl_string4344z00zz__r4_input_6_10_2z00, BGl_string4248z00zz__r4_input_6_10_2z00, BgL_arg3406z00_5634); 
FAILURE(BgL_auxz00_11135,BFALSE,BFALSE);} 
BgL_arg3405z00_5633 = 
CDR(BgL_pairz00_5635); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1151z00_3672, BgL_arg3405z00_5633); BUNSPEC; }  else 
{ /* Ieee/input.scm 566 */BFALSE; } } 
{ /* Ieee/input.scm 568 */
 obj_t BgL_portz00_5636;
if(
INPUT_PORTP(BgL_ipz00_3671))
{ /* Ieee/input.scm 568 */
BgL_portz00_5636 = BgL_ipz00_3671; }  else 
{ 
 obj_t BgL_auxz00_11143;
BgL_auxz00_11143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21849L), BGl_string4344z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_3671); 
FAILURE(BgL_auxz00_11143,BFALSE,BFALSE);} 
bgl_close_input_port(BgL_portz00_5636); } 
return BgL_tmp1153z00_3674;} } } } } } } 

}



/* &<@anonymous:2654> */
obj_t BGl_z62zc3z04anonymousza32654ze3ze5zz__r4_input_6_10_2z00(obj_t BgL_envz00_6015)
{
{ /* Ieee/input.scm 566 */
{ /* Ieee/input.scm 568 */
 obj_t BgL_ipz00_6016;
BgL_ipz00_6016 = 
PROCEDURE_REF(BgL_envz00_6015, 
(int)(0L)); 
{ /* Ieee/input.scm 568 */
 obj_t BgL_portz00_6979;
if(
INPUT_PORTP(BgL_ipz00_6016))
{ /* Ieee/input.scm 568 */
BgL_portz00_6979 = BgL_ipz00_6016; }  else 
{ 
 obj_t BgL_auxz00_11152;
BgL_auxz00_11152 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(21849L), BGl_string4345z00zz__r4_input_6_10_2z00, BGl_string4185z00zz__r4_input_6_10_2z00, BgL_ipz00_6016); 
FAILURE(BgL_auxz00_11152,BFALSE,BFALSE);} 
return 
bgl_close_input_port(BgL_portz00_6979);} } } 

}



/* file-lines */
BGL_EXPORTED_DEF obj_t BGl_filezd2lineszd2zz__r4_input_6_10_2z00(obj_t BgL_filez00_92)
{
{ /* Ieee/input.scm 580 */
if(
fexists(
BSTRING_TO_STRING(BgL_filez00_92)))
{ /* Ieee/input.scm 605 */
 obj_t BgL_zc3z04anonymousza32657ze3z87_6017;
BgL_zc3z04anonymousza32657ze3z87_6017 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32657ze3ze5zz__r4_input_6_10_2z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza32657ze3z87_6017, 
(int)(0L), BgL_filez00_92); 
return 
BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(BgL_filez00_92, BgL_zc3z04anonymousza32657ze3z87_6017);}  else 
{ /* Ieee/input.scm 601 */
return BFALSE;} } 

}



/* &file-lines */
obj_t BGl_z62filezd2lineszb0zz__r4_input_6_10_2z00(obj_t BgL_envz00_6018, obj_t BgL_filez00_6019)
{
{ /* Ieee/input.scm 580 */
{ /* Ieee/input.scm 583 */
 obj_t BgL_auxz00_11166;
if(
STRINGP(BgL_filez00_6019))
{ /* Ieee/input.scm 583 */
BgL_auxz00_11166 = BgL_filez00_6019
; }  else 
{ 
 obj_t BgL_auxz00_11169;
BgL_auxz00_11169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(22416L), BGl_string4346z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_filez00_6019); 
FAILURE(BgL_auxz00_11169,BFALSE,BFALSE);} 
return 
BGl_filezd2lineszd2zz__r4_input_6_10_2z00(BgL_auxz00_11166);} } 

}



/* &<@anonymous:2657> */
obj_t BGl_z62zc3z04anonymousza32657ze3ze5zz__r4_input_6_10_2z00(obj_t BgL_envz00_6020)
{
{ /* Ieee/input.scm 604 */
{ /* Ieee/input.scm 605 */
 obj_t BgL_filez00_6021;
BgL_filez00_6021 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_6020, 
(int)(0L))); 
{ 
 obj_t BgL_iportz00_6981; long BgL_startz00_6982; obj_t BgL_accz00_6983;
{ /* Ieee/input.scm 605 */
 obj_t BgL_arg2658z00_7031;
{ /* Ieee/input.scm 605 */
 obj_t BgL_tmpz00_11177;
BgL_tmpz00_11177 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2658z00_7031 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_11177); } 
BgL_iportz00_6981 = BgL_arg2658z00_7031; 
BgL_startz00_6982 = 0L; 
BgL_accz00_6983 = BNIL; 
{ 
 obj_t BgL_iportz00_7016; long BgL_lastzd2matchzd2_7017; long BgL_forwardz00_7018; long BgL_bufposz00_7019; obj_t BgL_iportz00_7008; long BgL_lastzd2matchzd2_7009; long BgL_forwardz00_7010; long BgL_bufposz00_7011; obj_t BgL_iportz00_6999; long BgL_lastzd2matchzd2_7000; long BgL_forwardz00_7001; long BgL_bufposz00_7002;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_6981))
{ /* Ieee/input.scm 583 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2660z00_7024;
{ /* Ieee/input.scm 583 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_7025;
{ /* Ieee/input.scm 583 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_7026;
BgL_new1045z00_7026 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 583 */
 long BgL_arg2665z00_7027;
BgL_arg2665z00_7027 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_7026), BgL_arg2665z00_7027); } 
BgL_new1046z00_7025 = BgL_new1045z00_7026; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_7025)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_7025)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_11190;
{ /* Ieee/input.scm 583 */
 obj_t BgL_arg2662z00_7028;
{ /* Ieee/input.scm 583 */
 obj_t BgL_arg2664z00_7029;
{ /* Ieee/input.scm 583 */
 obj_t BgL_classz00_7030;
BgL_classz00_7030 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2664z00_7029 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_7030); } 
BgL_arg2662z00_7028 = 
VECTOR_REF(BgL_arg2664z00_7029,2L); } 
{ /* Ieee/input.scm 583 */
 obj_t BgL_auxz00_11194;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2662z00_7028))
{ /* Ieee/input.scm 583 */
BgL_auxz00_11194 = BgL_arg2662z00_7028
; }  else 
{ 
 obj_t BgL_auxz00_11197;
BgL_auxz00_11197 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(22416L), BGl_string4350z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2662z00_7028); 
FAILURE(BgL_auxz00_11197,BFALSE,BFALSE);} 
BgL_auxz00_11190 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_11194); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_7025)))->BgL_stackz00)=((obj_t)BgL_auxz00_11190),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_7025)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_7025)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_7025)))->BgL_objz00)=((obj_t)BgL_iportz00_6981),BUNSPEC); 
BgL_arg2660z00_7024 = BgL_new1046z00_7025; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2660z00_7024));}  else 
{ /* Ieee/input.scm 583 */
BgL_ignorez00_6984:
RGC_START_MATCH(BgL_iportz00_6981); 
{ /* Ieee/input.scm 583 */
 long BgL_matchz00_6989;
{ /* Ieee/input.scm 583 */
 long BgL_arg2789z00_6990; long BgL_arg2793z00_6991;
BgL_arg2789z00_6990 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6981); 
BgL_arg2793z00_6991 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6981); 
BgL_iportz00_6999 = BgL_iportz00_6981; 
BgL_lastzd2matchzd2_7000 = 2L; 
BgL_forwardz00_7001 = BgL_arg2789z00_6990; 
BgL_bufposz00_7002 = BgL_arg2793z00_6991; 
BgL_statezd20zd21155z00_6986:
if(
(BgL_forwardz00_7001==BgL_bufposz00_7002))
{ /* Ieee/input.scm 583 */
if(
rgc_fill_buffer(BgL_iportz00_6999))
{ /* Ieee/input.scm 583 */
 long BgL_arg2688z00_7003; long BgL_arg2689z00_7004;
BgL_arg2688z00_7003 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6999); 
BgL_arg2689z00_7004 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6999); 
{ 
 long BgL_bufposz00_11221; long BgL_forwardz00_11220;
BgL_forwardz00_11220 = BgL_arg2688z00_7003; 
BgL_bufposz00_11221 = BgL_arg2689z00_7004; 
BgL_bufposz00_7002 = BgL_bufposz00_11221; 
BgL_forwardz00_7001 = BgL_forwardz00_11220; 
goto BgL_statezd20zd21155z00_6986;} }  else 
{ /* Ieee/input.scm 583 */
BgL_matchz00_6989 = BgL_lastzd2matchzd2_7000; } }  else 
{ /* Ieee/input.scm 583 */
 int BgL_curz00_7005;
BgL_curz00_7005 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6999, BgL_forwardz00_7001); 
{ /* Ieee/input.scm 583 */

if(
(
(long)(BgL_curz00_7005)==10L))
{ /* Ieee/input.scm 583 */
 long BgL_arg2691z00_7006;
BgL_arg2691z00_7006 = 
(1L+BgL_forwardz00_7001); 
{ /* Ieee/input.scm 583 */
 long BgL_newzd2matchzd2_7007;
RGC_STOP_MATCH(BgL_iportz00_6999, BgL_arg2691z00_7006); 
BgL_newzd2matchzd2_7007 = 0L; 
BgL_matchz00_6989 = BgL_newzd2matchzd2_7007; } }  else 
{ /* Ieee/input.scm 583 */
BgL_iportz00_7008 = BgL_iportz00_6999; 
BgL_lastzd2matchzd2_7009 = BgL_lastzd2matchzd2_7000; 
BgL_forwardz00_7010 = 
(1L+BgL_forwardz00_7001); 
BgL_bufposz00_7011 = BgL_bufposz00_7002; 
BgL_statezd21zd21156z00_6987:
{ /* Ieee/input.scm 583 */
 long BgL_newzd2matchzd2_7012;
RGC_STOP_MATCH(BgL_iportz00_7008, BgL_forwardz00_7010); 
BgL_newzd2matchzd2_7012 = 1L; 
if(
(BgL_forwardz00_7010==BgL_bufposz00_7011))
{ /* Ieee/input.scm 583 */
if(
rgc_fill_buffer(BgL_iportz00_7008))
{ /* Ieee/input.scm 583 */
 long BgL_arg2680z00_7013; long BgL_arg2681z00_7014;
BgL_arg2680z00_7013 = 
RGC_BUFFER_FORWARD(BgL_iportz00_7008); 
BgL_arg2681z00_7014 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_7008); 
{ 
 long BgL_bufposz00_11236; long BgL_forwardz00_11235;
BgL_forwardz00_11235 = BgL_arg2680z00_7013; 
BgL_bufposz00_11236 = BgL_arg2681z00_7014; 
BgL_bufposz00_7011 = BgL_bufposz00_11236; 
BgL_forwardz00_7010 = BgL_forwardz00_11235; 
goto BgL_statezd21zd21156z00_6987;} }  else 
{ /* Ieee/input.scm 583 */
BgL_matchz00_6989 = BgL_newzd2matchzd2_7012; } }  else 
{ /* Ieee/input.scm 583 */
 int BgL_curz00_7015;
BgL_curz00_7015 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_7008, BgL_forwardz00_7010); 
{ /* Ieee/input.scm 583 */

if(
(
(long)(BgL_curz00_7015)==10L))
{ /* Ieee/input.scm 583 */
BgL_matchz00_6989 = BgL_newzd2matchzd2_7012; }  else 
{ /* Ieee/input.scm 583 */
BgL_iportz00_7016 = BgL_iportz00_7008; 
BgL_lastzd2matchzd2_7017 = BgL_newzd2matchzd2_7012; 
BgL_forwardz00_7018 = 
(1L+BgL_forwardz00_7010); 
BgL_bufposz00_7019 = BgL_bufposz00_7011; 
BgL_statezd24zd21159z00_6988:
{ /* Ieee/input.scm 583 */
 long BgL_newzd2matchzd2_7020;
RGC_STOP_MATCH(BgL_iportz00_7016, BgL_forwardz00_7018); 
BgL_newzd2matchzd2_7020 = 1L; 
if(
(BgL_forwardz00_7018==BgL_bufposz00_7019))
{ /* Ieee/input.scm 583 */
if(
rgc_fill_buffer(BgL_iportz00_7016))
{ /* Ieee/input.scm 583 */
 long BgL_arg2670z00_7021; long BgL_arg2672z00_7022;
BgL_arg2670z00_7021 = 
RGC_BUFFER_FORWARD(BgL_iportz00_7016); 
BgL_arg2672z00_7022 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_7016); 
{ 
 long BgL_bufposz00_11249; long BgL_forwardz00_11248;
BgL_forwardz00_11248 = BgL_arg2670z00_7021; 
BgL_bufposz00_11249 = BgL_arg2672z00_7022; 
BgL_bufposz00_7019 = BgL_bufposz00_11249; 
BgL_forwardz00_7018 = BgL_forwardz00_11248; 
goto BgL_statezd24zd21159z00_6988;} }  else 
{ /* Ieee/input.scm 583 */
BgL_matchz00_6989 = BgL_newzd2matchzd2_7020; } }  else 
{ /* Ieee/input.scm 583 */
 int BgL_curz00_7023;
BgL_curz00_7023 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_7016, BgL_forwardz00_7018); 
{ /* Ieee/input.scm 583 */

if(
(
(long)(BgL_curz00_7023)==10L))
{ /* Ieee/input.scm 583 */
BgL_matchz00_6989 = BgL_newzd2matchzd2_7020; }  else 
{ 
 long BgL_forwardz00_11255; long BgL_lastzd2matchzd2_11254;
BgL_lastzd2matchzd2_11254 = BgL_newzd2matchzd2_7020; 
BgL_forwardz00_11255 = 
(1L+BgL_forwardz00_7018); 
BgL_forwardz00_7018 = BgL_forwardz00_11255; 
BgL_lastzd2matchzd2_7017 = BgL_lastzd2matchzd2_11254; 
goto BgL_statezd24zd21159z00_6988;} } } } } } } } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_6981); 
switch( BgL_matchz00_6989) { case 2L : 

{ /* Ieee/input.scm 593 */
 obj_t BgL_cz00_6992;
{ /* Ieee/input.scm 583 */
 bool_t BgL_test5076z00_11260;
{ /* Ieee/input.scm 583 */
 long BgL_arg2774z00_6998;
BgL_arg2774z00_6998 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_6981); 
BgL_test5076z00_11260 = 
(BgL_arg2774z00_6998==0L); } 
if(BgL_test5076z00_11260)
{ /* Ieee/input.scm 583 */
BgL_cz00_6992 = BEOF; }  else 
{ /* Ieee/input.scm 583 */
BgL_cz00_6992 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_6981)); } } 
if(
EOF_OBJECTP(BgL_cz00_6992))
{ /* Ieee/input.scm 595 */
 long BgL_stopz00_6993;
BgL_stopz00_6993 = 
INPUT_PORT_FILEPOS(BgL_iportz00_6981); 
if(
(BgL_stopz00_6993>BgL_startz00_6982))
{ /* Ieee/input.scm 597 */
 obj_t BgL_arg2783z00_6994;
{ /* Ieee/input.scm 597 */
 obj_t BgL_arg2784z00_6995;
BgL_arg2784z00_6995 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_6982), 
BINT(BgL_stopz00_6993)); 
BgL_arg2783z00_6994 = 
MAKE_YOUNG_PAIR(BgL_arg2784z00_6995, BgL_accz00_6983); } 
return 
bgl_reverse_bang(BgL_arg2783z00_6994);}  else 
{ /* Ieee/input.scm 596 */
return 
bgl_reverse_bang(BgL_accz00_6983);} }  else 
{ /* Ieee/input.scm 594 */
return 
BGl_errorz00zz__errorz00(BGl_symbol4347z00zz__r4_input_6_10_2z00, BGl_string4349z00zz__r4_input_6_10_2z00, BgL_filez00_6021);} } break;case 1L : 

goto BgL_ignorez00_6984;break;case 0L : 

{ /* Ieee/input.scm 585 */
 long BgL_stopz00_6996;
BgL_stopz00_6996 = 
INPUT_PORT_FILEPOS(BgL_iportz00_6981); 
{ /* Ieee/input.scm 585 */
 obj_t BgL_descz00_6997;
BgL_descz00_6997 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_6982), 
BINT(BgL_stopz00_6996)); 
{ /* Ieee/input.scm 586 */

BgL_startz00_6982 = 
(1L+BgL_stopz00_6996); 
BgL_accz00_6983 = 
MAKE_YOUNG_PAIR(BgL_descz00_6997, BgL_accz00_6983); 
goto BgL_ignorez00_6984;} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_6989));} } } } } } } } 

}



/* file-position->line */
BGL_EXPORTED_DEF obj_t BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(int BgL_posz00_93, obj_t BgL_fdescz00_94)
{
{ /* Ieee/input.scm 610 */
if(
PAIRP(BgL_fdescz00_94))
{ 
 obj_t BgL_flinesz00_5709; long BgL_linez00_5710;
BgL_flinesz00_5709 = BgL_fdescz00_94; 
BgL_linez00_5710 = 1L; 
BgL_loopz00_5708:
if(
NULLP(BgL_flinesz00_5709))
{ /* Ieee/input.scm 616 */
return BFALSE;}  else 
{ /* Ieee/input.scm 618 */
 bool_t BgL_test5081z00_11290;
{ /* Ieee/input.scm 618 */
 long BgL_n1z00_5718; long BgL_n2z00_5719;
BgL_n1z00_5718 = 
(long)(BgL_posz00_93); 
{ /* Ieee/input.scm 618 */
 obj_t BgL_pairz00_5714;
if(
PAIRP(BgL_flinesz00_5709))
{ /* Ieee/input.scm 618 */
BgL_pairz00_5714 = BgL_flinesz00_5709; }  else 
{ 
 obj_t BgL_auxz00_11294;
BgL_auxz00_11294 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23446L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4248z00zz__r4_input_6_10_2z00, BgL_flinesz00_5709); 
FAILURE(BgL_auxz00_11294,BFALSE,BFALSE);} 
{ /* Ieee/input.scm 618 */
 obj_t BgL_pairz00_5717;
{ /* Ieee/input.scm 618 */
 obj_t BgL_aux4172z00_6726;
BgL_aux4172z00_6726 = 
CAR(BgL_pairz00_5714); 
if(
PAIRP(BgL_aux4172z00_6726))
{ /* Ieee/input.scm 618 */
BgL_pairz00_5717 = BgL_aux4172z00_6726; }  else 
{ 
 obj_t BgL_auxz00_11301;
BgL_auxz00_11301 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23440L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4248z00zz__r4_input_6_10_2z00, BgL_aux4172z00_6726); 
FAILURE(BgL_auxz00_11301,BFALSE,BFALSE);} } 
{ /* Ieee/input.scm 618 */
 obj_t BgL_tmpz00_11305;
{ /* Ieee/input.scm 618 */
 obj_t BgL_aux4174z00_6728;
BgL_aux4174z00_6728 = 
CDR(BgL_pairz00_5717); 
if(
INTEGERP(BgL_aux4174z00_6728))
{ /* Ieee/input.scm 618 */
BgL_tmpz00_11305 = BgL_aux4174z00_6728
; }  else 
{ 
 obj_t BgL_auxz00_11309;
BgL_auxz00_11309 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23440L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_aux4174z00_6728); 
FAILURE(BgL_auxz00_11309,BFALSE,BFALSE);} } 
BgL_n2z00_5719 = 
(long)CINT(BgL_tmpz00_11305); } } } 
BgL_test5081z00_11290 = 
(BgL_n1z00_5718>=BgL_n2z00_5719); } 
if(BgL_test5081z00_11290)
{ /* Ieee/input.scm 619 */
 obj_t BgL_arg2804z00_5720; long BgL_arg2805z00_5721;
{ /* Ieee/input.scm 619 */
 obj_t BgL_pairz00_5722;
if(
PAIRP(BgL_flinesz00_5709))
{ /* Ieee/input.scm 619 */
BgL_pairz00_5722 = BgL_flinesz00_5709; }  else 
{ 
 obj_t BgL_auxz00_11317;
BgL_auxz00_11317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23473L), BGl_string4277z00zz__r4_input_6_10_2z00, BGl_string4248z00zz__r4_input_6_10_2z00, BgL_flinesz00_5709); 
FAILURE(BgL_auxz00_11317,BFALSE,BFALSE);} 
BgL_arg2804z00_5720 = 
CDR(BgL_pairz00_5722); } 
BgL_arg2805z00_5721 = 
(BgL_linez00_5710+1L); 
{ 
 long BgL_linez00_11324; obj_t BgL_flinesz00_11323;
BgL_flinesz00_11323 = BgL_arg2804z00_5720; 
BgL_linez00_11324 = BgL_arg2805z00_5721; 
BgL_linez00_5710 = BgL_linez00_11324; 
BgL_flinesz00_5709 = BgL_flinesz00_11323; 
goto BgL_loopz00_5708;} }  else 
{ /* Ieee/input.scm 618 */
return 
BINT(BgL_linez00_5710);} } }  else 
{ /* Ieee/input.scm 612 */
if(
STRINGP(BgL_fdescz00_94))
{ /* Ieee/input.scm 622 */
if(
fexists(
BSTRING_TO_STRING(BgL_fdescz00_94)))
{ /* Ieee/input.scm 638 */
 obj_t BgL_zc3z04anonymousza32812ze3z87_6022;
BgL_zc3z04anonymousza32812ze3z87_6022 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32812ze3ze5zz__r4_input_6_10_2z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza32812ze3z87_6022, 
(int)(0L), 
BINT(BgL_posz00_93)); 
return 
BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(BgL_fdescz00_94, BgL_zc3z04anonymousza32812ze3z87_6022);}  else 
{ /* Ieee/input.scm 634 */
return BFALSE;} }  else 
{ /* Ieee/input.scm 622 */
return BFALSE;} } } 

}



/* &file-position->line */
obj_t BGl_z62filezd2positionzd2ze3linez81zz__r4_input_6_10_2z00(obj_t BgL_envz00_6023, obj_t BgL_posz00_6024, obj_t BgL_fdescz00_6025)
{
{ /* Ieee/input.scm 610 */
{ /* Ieee/input.scm 612 */
 int BgL_auxz00_11338;
{ /* Ieee/input.scm 612 */
 obj_t BgL_tmpz00_11339;
if(
INTEGERP(BgL_posz00_6024))
{ /* Ieee/input.scm 612 */
BgL_tmpz00_11339 = BgL_posz00_6024
; }  else 
{ 
 obj_t BgL_auxz00_11342;
BgL_auxz00_11342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23319L), BGl_string4351z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_posz00_6024); 
FAILURE(BgL_auxz00_11342,BFALSE,BFALSE);} 
BgL_auxz00_11338 = 
CINT(BgL_tmpz00_11339); } 
return 
BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(BgL_auxz00_11338, BgL_fdescz00_6025);} } 

}



/* &<@anonymous:2812> */
obj_t BGl_z62zc3z04anonymousza32812ze3ze5zz__r4_input_6_10_2z00(obj_t BgL_envz00_6026)
{
{ /* Ieee/input.scm 637 */
{ /* Ieee/input.scm 638 */
 int BgL_posz00_6027;
{ /* Ieee/input.scm 638 */
 obj_t BgL_tmpz00_11348;
{ /* Ieee/input.scm 638 */
 obj_t BgL_aux4178z00_6732;
BgL_aux4178z00_6732 = 
PROCEDURE_REF(BgL_envz00_6026, 
(int)(0L)); 
if(
INTEGERP(BgL_aux4178z00_6732))
{ /* Ieee/input.scm 638 */
BgL_tmpz00_11348 = BgL_aux4178z00_6732
; }  else 
{ 
 obj_t BgL_auxz00_11353;
BgL_auxz00_11353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23928L), BGl_string4352z00zz__r4_input_6_10_2z00, BGl_string4300z00zz__r4_input_6_10_2z00, BgL_aux4178z00_6732); 
FAILURE(BgL_auxz00_11353,BFALSE,BFALSE);} } 
BgL_posz00_6027 = 
CINT(BgL_tmpz00_11348); } 
{ 
 obj_t BgL_iportz00_7033; obj_t BgL_posz00_7034; long BgL_linez00_7035;
{ /* Ieee/input.scm 638 */
 obj_t BgL_arg2815z00_7076;
{ /* Ieee/input.scm 638 */
 obj_t BgL_tmpz00_11358;
BgL_tmpz00_11358 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2815z00_7076 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_11358); } 
BgL_iportz00_7033 = BgL_arg2815z00_7076; 
BgL_posz00_7034 = 
BINT(BgL_posz00_6027); 
BgL_linez00_7035 = 1L; 
{ 
 obj_t BgL_iportz00_7061; long BgL_lastzd2matchzd2_7062; long BgL_forwardz00_7063; long BgL_bufposz00_7064; obj_t BgL_iportz00_7053; long BgL_lastzd2matchzd2_7054; long BgL_forwardz00_7055; long BgL_bufposz00_7056; obj_t BgL_iportz00_7044; long BgL_lastzd2matchzd2_7045; long BgL_forwardz00_7046; long BgL_bufposz00_7047;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_7033))
{ /* Ieee/input.scm 623 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2818z00_7069;
{ /* Ieee/input.scm 623 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1046z00_7070;
{ /* Ieee/input.scm 623 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1045z00_7071;
BgL_new1045z00_7071 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Ieee/input.scm 623 */
 long BgL_arg2823z00_7072;
BgL_arg2823z00_7072 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1045z00_7071), BgL_arg2823z00_7072); } 
BgL_new1046z00_7070 = BgL_new1045z00_7071; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_7070)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_7070)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_11371;
{ /* Ieee/input.scm 623 */
 obj_t BgL_arg2820z00_7073;
{ /* Ieee/input.scm 623 */
 obj_t BgL_arg2821z00_7074;
{ /* Ieee/input.scm 623 */
 obj_t BgL_classz00_7075;
BgL_classz00_7075 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2821z00_7074 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_7075); } 
BgL_arg2820z00_7073 = 
VECTOR_REF(BgL_arg2821z00_7074,2L); } 
{ /* Ieee/input.scm 623 */
 obj_t BgL_auxz00_11375;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2820z00_7073))
{ /* Ieee/input.scm 623 */
BgL_auxz00_11375 = BgL_arg2820z00_7073
; }  else 
{ 
 obj_t BgL_auxz00_11378;
BgL_auxz00_11378 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(23566L), BGl_string4350z00zz__r4_input_6_10_2z00, BGl_string4201z00zz__r4_input_6_10_2z00, BgL_arg2820z00_7073); 
FAILURE(BgL_auxz00_11378,BFALSE,BFALSE);} 
BgL_auxz00_11371 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_11375); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1046z00_7070)))->BgL_stackz00)=((obj_t)BgL_auxz00_11371),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_7070)))->BgL_procz00)=((obj_t)BGl_string4186z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_7070)))->BgL_msgz00)=((obj_t)BGl_string4202z00zz__r4_input_6_10_2z00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1046z00_7070)))->BgL_objz00)=((obj_t)BgL_iportz00_7033),BUNSPEC); 
BgL_arg2818z00_7069 = BgL_new1046z00_7070; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2818z00_7069));}  else 
{ /* Ieee/input.scm 623 */
BgL_ignorez00_7036:
RGC_START_MATCH(BgL_iportz00_7033); 
{ /* Ieee/input.scm 623 */
 long BgL_matchz00_7040;
{ /* Ieee/input.scm 623 */
 long BgL_arg2974z00_7041; long BgL_arg2975z00_7042;
BgL_arg2974z00_7041 = 
RGC_BUFFER_FORWARD(BgL_iportz00_7033); 
BgL_arg2975z00_7042 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_7033); 
BgL_iportz00_7044 = BgL_iportz00_7033; 
BgL_lastzd2matchzd2_7045 = 2L; 
BgL_forwardz00_7046 = BgL_arg2974z00_7041; 
BgL_bufposz00_7047 = BgL_arg2975z00_7042; 
BgL_statezd20zd21162z00_7037:
if(
(BgL_forwardz00_7046==BgL_bufposz00_7047))
{ /* Ieee/input.scm 623 */
if(
rgc_fill_buffer(BgL_iportz00_7044))
{ /* Ieee/input.scm 623 */
 long BgL_arg2844z00_7048; long BgL_arg2846z00_7049;
BgL_arg2844z00_7048 = 
RGC_BUFFER_FORWARD(BgL_iportz00_7044); 
BgL_arg2846z00_7049 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_7044); 
{ 
 long BgL_bufposz00_11402; long BgL_forwardz00_11401;
BgL_forwardz00_11401 = BgL_arg2844z00_7048; 
BgL_bufposz00_11402 = BgL_arg2846z00_7049; 
BgL_bufposz00_7047 = BgL_bufposz00_11402; 
BgL_forwardz00_7046 = BgL_forwardz00_11401; 
goto BgL_statezd20zd21162z00_7037;} }  else 
{ /* Ieee/input.scm 623 */
BgL_matchz00_7040 = BgL_lastzd2matchzd2_7045; } }  else 
{ /* Ieee/input.scm 623 */
 int BgL_curz00_7050;
BgL_curz00_7050 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_7044, BgL_forwardz00_7046); 
{ /* Ieee/input.scm 623 */

if(
(
(long)(BgL_curz00_7050)==10L))
{ /* Ieee/input.scm 623 */
 long BgL_arg2848z00_7051;
BgL_arg2848z00_7051 = 
(1L+BgL_forwardz00_7046); 
{ /* Ieee/input.scm 623 */
 long BgL_newzd2matchzd2_7052;
RGC_STOP_MATCH(BgL_iportz00_7044, BgL_arg2848z00_7051); 
BgL_newzd2matchzd2_7052 = 0L; 
BgL_matchz00_7040 = BgL_newzd2matchzd2_7052; } }  else 
{ /* Ieee/input.scm 623 */
BgL_iportz00_7053 = BgL_iportz00_7044; 
BgL_lastzd2matchzd2_7054 = BgL_lastzd2matchzd2_7045; 
BgL_forwardz00_7055 = 
(1L+BgL_forwardz00_7046); 
BgL_bufposz00_7056 = BgL_bufposz00_7047; 
BgL_statezd21zd21163z00_7038:
{ /* Ieee/input.scm 623 */
 long BgL_newzd2matchzd2_7057;
RGC_STOP_MATCH(BgL_iportz00_7053, BgL_forwardz00_7055); 
BgL_newzd2matchzd2_7057 = 1L; 
if(
(BgL_forwardz00_7055==BgL_bufposz00_7056))
{ /* Ieee/input.scm 623 */
if(
rgc_fill_buffer(BgL_iportz00_7053))
{ /* Ieee/input.scm 623 */
 long BgL_arg2836z00_7058; long BgL_arg2837z00_7059;
BgL_arg2836z00_7058 = 
RGC_BUFFER_FORWARD(BgL_iportz00_7053); 
BgL_arg2837z00_7059 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_7053); 
{ 
 long BgL_bufposz00_11417; long BgL_forwardz00_11416;
BgL_forwardz00_11416 = BgL_arg2836z00_7058; 
BgL_bufposz00_11417 = BgL_arg2837z00_7059; 
BgL_bufposz00_7056 = BgL_bufposz00_11417; 
BgL_forwardz00_7055 = BgL_forwardz00_11416; 
goto BgL_statezd21zd21163z00_7038;} }  else 
{ /* Ieee/input.scm 623 */
BgL_matchz00_7040 = BgL_newzd2matchzd2_7057; } }  else 
{ /* Ieee/input.scm 623 */
 int BgL_curz00_7060;
BgL_curz00_7060 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_7053, BgL_forwardz00_7055); 
{ /* Ieee/input.scm 623 */

if(
(
(long)(BgL_curz00_7060)==10L))
{ /* Ieee/input.scm 623 */
BgL_matchz00_7040 = BgL_newzd2matchzd2_7057; }  else 
{ /* Ieee/input.scm 623 */
BgL_iportz00_7061 = BgL_iportz00_7053; 
BgL_lastzd2matchzd2_7062 = BgL_newzd2matchzd2_7057; 
BgL_forwardz00_7063 = 
(1L+BgL_forwardz00_7055); 
BgL_bufposz00_7064 = BgL_bufposz00_7056; 
BgL_statezd24zd21166z00_7039:
{ /* Ieee/input.scm 623 */
 long BgL_newzd2matchzd2_7065;
RGC_STOP_MATCH(BgL_iportz00_7061, BgL_forwardz00_7063); 
BgL_newzd2matchzd2_7065 = 1L; 
if(
(BgL_forwardz00_7063==BgL_bufposz00_7064))
{ /* Ieee/input.scm 623 */
if(
rgc_fill_buffer(BgL_iportz00_7061))
{ /* Ieee/input.scm 623 */
 long BgL_arg2828z00_7066; long BgL_arg2829z00_7067;
BgL_arg2828z00_7066 = 
RGC_BUFFER_FORWARD(BgL_iportz00_7061); 
BgL_arg2829z00_7067 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_7061); 
{ 
 long BgL_bufposz00_11430; long BgL_forwardz00_11429;
BgL_forwardz00_11429 = BgL_arg2828z00_7066; 
BgL_bufposz00_11430 = BgL_arg2829z00_7067; 
BgL_bufposz00_7064 = BgL_bufposz00_11430; 
BgL_forwardz00_7063 = BgL_forwardz00_11429; 
goto BgL_statezd24zd21166z00_7039;} }  else 
{ /* Ieee/input.scm 623 */
BgL_matchz00_7040 = BgL_newzd2matchzd2_7065; } }  else 
{ /* Ieee/input.scm 623 */
 int BgL_curz00_7068;
BgL_curz00_7068 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_7061, BgL_forwardz00_7063); 
{ /* Ieee/input.scm 623 */

if(
(
(long)(BgL_curz00_7068)==10L))
{ /* Ieee/input.scm 623 */
BgL_matchz00_7040 = BgL_newzd2matchzd2_7065; }  else 
{ 
 long BgL_forwardz00_11436; long BgL_lastzd2matchzd2_11435;
BgL_lastzd2matchzd2_11435 = BgL_newzd2matchzd2_7065; 
BgL_forwardz00_11436 = 
(1L+BgL_forwardz00_7063); 
BgL_forwardz00_7063 = BgL_forwardz00_11436; 
BgL_lastzd2matchzd2_7062 = BgL_lastzd2matchzd2_11435; 
goto BgL_statezd24zd21166z00_7039;} } } } } } } } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_7033); 
switch( BgL_matchz00_7040) { case 2L : 

return BFALSE;break;case 1L : 

goto BgL_ignorez00_7036;break;case 0L : 

{ /* Ieee/input.scm 625 */
 bool_t BgL_test5101z00_11441;
{ /* Ieee/input.scm 625 */
 long BgL_arg2972z00_7043;
BgL_arg2972z00_7043 = 
INPUT_PORT_FILEPOS(BgL_iportz00_7033); 
BgL_test5101z00_11441 = 
(BgL_arg2972z00_7043>=
(long)CINT(BgL_posz00_7034)); } 
if(BgL_test5101z00_11441)
{ /* Ieee/input.scm 625 */
return 
BINT(BgL_linez00_7035);}  else 
{ /* Ieee/input.scm 625 */
BgL_linez00_7035 = 
(BgL_linez00_7035+1L); 
goto BgL_ignorez00_7036;} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string4186z00zz__r4_input_6_10_2z00, BGl_string4187z00zz__r4_input_6_10_2z00, 
BINT(BgL_matchz00_7040));} } } } } } } } 

}



/* _password */
obj_t BGl__passwordz00zz__r4_input_6_10_2z00(obj_t BgL_env1332z00_97, obj_t BgL_opt1331z00_96)
{
{ /* Ieee/input.scm 645 */
{ /* Ieee/input.scm 645 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1331z00_96)) { case 0L : 

{ /* Ieee/input.scm 645 */

return 
bgl_password(
BSTRING_TO_STRING(BGl_string4276z00zz__r4_input_6_10_2z00));} break;case 1L : 

{ /* Ieee/input.scm 645 */
 obj_t BgL_promptz00_7078;
BgL_promptz00_7078 = 
VECTOR_REF(BgL_opt1331z00_96,0L); 
{ /* Ieee/input.scm 645 */

{ /* Ieee/input.scm 645 */
 obj_t BgL_promptz00_7079;
if(
STRINGP(BgL_promptz00_7078))
{ /* Ieee/input.scm 645 */
BgL_promptz00_7079 = BgL_promptz00_7078; }  else 
{ 
 obj_t BgL_auxz00_11456;
BgL_auxz00_11456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4183z00zz__r4_input_6_10_2z00, 
BINT(24216L), BGl_string4355z00zz__r4_input_6_10_2z00, BGl_string4191z00zz__r4_input_6_10_2z00, BgL_promptz00_7078); 
FAILURE(BgL_auxz00_11456,BFALSE,BFALSE);} 
return 
bgl_password(
BSTRING_TO_STRING(BgL_promptz00_7079));} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4353z00zz__r4_input_6_10_2z00, BGl_string4255z00zz__r4_input_6_10_2z00, 
BINT(
VECTOR_LENGTH(BgL_opt1331z00_96)));} } } } 

}



/* password */
BGL_EXPORTED_DEF obj_t BGl_passwordz00zz__r4_input_6_10_2z00(obj_t BgL_promptz00_95)
{
{ /* Ieee/input.scm 645 */
return 
bgl_password(
BSTRING_TO_STRING(BgL_promptz00_95));} 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_input_6_10_2z00(void)
{
{ /* Ieee/input.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string4356z00zz__r4_input_6_10_2z00)); 
BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L, 
BSTRING_TO_STRING(BGl_string4356z00zz__r4_input_6_10_2z00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string4356z00zz__r4_input_6_10_2z00)); 
return 
BGl_modulezd2initializa7ationz75zz__gunza7ipza7(224363699L, 
BSTRING_TO_STRING(BGl_string4356z00zz__r4_input_6_10_2z00));} 

}

#ifdef __cplusplus
}
#endif
