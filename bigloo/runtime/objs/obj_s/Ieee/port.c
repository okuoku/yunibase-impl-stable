/*===========================================================================*/
/*   (Ieee/port.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/port.scm -indent -o objs/obj_s/Ieee/port.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_PORTS_6_10_1_TYPE_DEFINITIONS
#define BGL___R4_PORTS_6_10_1_TYPE_DEFINITIONS

/* object type definitions */
typedef struct BgL_z62httpzd2redirectionzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_portz00;
   obj_t BgL_urlz00;
} *BgL_z62httpzd2redirectionzb0_bglt;


#endif // BGL___R4_PORTS_6_10_1_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62directoryzd2ze3pathzd2listz81zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2truncatez62zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2seekz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__selectz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_open_input_descriptor(int, obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL long BGl_inputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62filezd2modificationzd2timez62zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_filezd2timeszd2setz12z12zz__r4_ports_6_10_1z00(char *, long, long);
static obj_t BGl_z62zc3z04anonymousza31450ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62filezd2timeszd2setz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
static obj_t BGl_z62filezd2existszf3z43zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2symlinkzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62truncatezd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2flushzd2bufferzd2setz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
extern bool_t reset_eof(obj_t);
extern obj_t BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(obj_t, obj_t);
extern obj_t bgl_directory_to_list(char *);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2clonez12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_closedzd2inputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_symbol2818z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_list2706z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62inputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_vector2968z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62closezd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62flush2101z62zz__r4_ports_6_10_1z00(obj_t);
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31443ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62flush2105z62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_inputzd2portzd2lastzd2tokenzd2positionz00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_directoryzd2ze3pathzd2listze3zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_outputzd2portzd2truncatez00zz__r4_ports_6_10_1z00(obj_t, long);
static obj_t BGl_z62inputzd2portzd2seekzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2errorzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2inputzd2fromzd2stringzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62withzd2errorzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2outputzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2820z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_symbol2822z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_symbol2742z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_outputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
extern obj_t bgl_open_input_procedure(obj_t, obj_t);
static obj_t BGl_symbol2909z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62outputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_zc3z04exitza31609ze3ze70z60zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_filezd2existszf3z21zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62zc3z04anonymousza31460ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl__lockfz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_filezd2uidzd2zz__r4_ports_6_10_1z00(char *);
BGL_EXPORTED_DECL long BGl_inputzd2portzd2lengthz00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_resetzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_currentzd2outputzd2portz00zz__r4_ports_6_10_1z00(void);
extern obj_t bgl_directory_to_vector(char *);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3001z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_reopenzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(obj_t, char *);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__ftpz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__httpz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__urlz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__gunza7ipza7(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_symbol3003z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_input_port_reopen(obj_t);
static obj_t BGl_symbol3005z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_setzd2outputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
extern obj_t bgl_system_failure(int, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3007z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern obj_t bgl_open_output_file(obj_t, obj_t);
extern obj_t bgl_open_input_substring_bang(obj_t, long, long);
extern obj_t BGl_za2classesza2z00zz__objectz00;
static obj_t BGl_symbol2911z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62inputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_copyzd2filezd2zz__r4_ports_6_10_1z00(char *, char *);
BGL_EXPORTED_DECL obj_t BGl_inputzd2gza7ipzd2portzf3z54zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62openzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t BGl_z62httpzd2redirectionzb0zz__httpz00;
static obj_t BGl_symbol2839z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_currentzd2errorzd2portz00zz__r4_ports_6_10_1z00(void);
extern int bgl_input_fill_string(obj_t, obj_t);
static obj_t BGl_z62closedzd2inputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_filezd2typezd2zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31607ze3ze5zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern long bgl_last_access_time(char *);
static obj_t BGl__openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern bool_t fexists(char *);
extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_renamezd2filezd2zz__r4_ports_6_10_1z00(char *, char *);
BGL_EXPORTED_DECL long BGl_outputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_list2810z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl__openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_list2811z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern obj_t bgl_open_input_resource(obj_t, obj_t);
static obj_t BGl_symbol2845z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_closedzd2outputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_symbol2847z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_list2815z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__r4_ports_6_10_1z00(void);
extern long bgl_file_size(char *);
static obj_t BGl_symbol2849z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern obj_t bgl_output_port_seek(obj_t, long);
extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2flushzd2hookzd2zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2errorzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_deletezd2directoryzd2zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62directoryzd2ze3vectorz53zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2reopenz12z12zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2protocolzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_withzd2appendzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2flushzd2bufferzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_open_input_file(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_directoryzd2lengthzd2zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_symbol2851z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_filezd2gza7ipzf3z86zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_closezd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
extern long bgl_last_change_time(char *);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2853z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62outputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_list2741z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2flushzd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_open_output_procedure(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2857z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_list2908z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62z52openzd2inputzd2resourcez30zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62currentzd2errorzd2portz62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31439ze3ze5zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2errorzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_ports_6_10_1z00(void);
BGL_EXPORTED_DECL obj_t BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t, obj_t, int);
static obj_t BGl_keyword2982z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(int, obj_t);
static obj_t BGl_keyword2984z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62z52openzd2inputzd2httpzd2socketze2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_keyword2986z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_genericzd2initzd2zz__r4_ports_6_10_1z00(void);
static obj_t BGl_keyword2987z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62getzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_filezd2modificationzd2timez00zz__r4_ports_6_10_1z00(char *);
BGL_EXPORTED_DECL int BGl_filezd2gidzd2zz__r4_ports_6_10_1z00(char *);
static obj_t BGl__appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62makezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62deletezd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62getzd2outputzd2stringz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2782z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_symbol2864z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_ports_6_10_1z00(void);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2867z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_outputzd2portzd2isattyzf3zf3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62filezd2siza7ez17zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__r4_ports_6_10_1z00(void);
static obj_t BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00(obj_t);
extern bool_t bgl_output_port_timeout_set(obj_t, long);
static obj_t BGl_z62withzd2errorzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2appendzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__r4_ports_6_10_1z00(void);
extern obj_t BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(obj_t);
static obj_t BGl_z62closez62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62inputzd2portzd2reopenz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62reopenzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t);
extern obj_t bgl_open_pipes(obj_t);
BGL_EXPORTED_DECL obj_t BGl_closezd2inputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62inputzd2portzd2protocolz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2871z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_symbol2873z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern obj_t bgl_open_input_mmap(obj_t, obj_t, long, long);
static obj_t BGl_z62filezd2gza7ipzf3ze4zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_list2844z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62makezd2directorieszb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_open_input_pipe(obj_t, obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t socket_close(obj_t);
extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
BGL_EXPORTED_DECL obj_t BGl_currentzd2inputzd2portz00zz__r4_ports_6_10_1z00(void);
static obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00(obj_t);
extern long bgl_file_mode(char *);
extern obj_t bgl_directory_to_path_list(char *, int, unsigned char);
extern long bgl_input_port_timeout(obj_t);
static obj_t BGl_z62withzd2errorzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_filezd2accesszd2timez00zz__r4_ports_6_10_1z00(char *);
extern obj_t BGl_dirnamez00zz__osz00(obj_t);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
static obj_t BGl_zc3z04exitza31502ze3ze70z60zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62withzd2outputzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2881z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_search1311ze70ze7zz__r4_ports_6_10_1z00(long, obj_t, obj_t, long);
static obj_t BGl_z62inputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2883z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_symbol2885z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t);
static obj_t BGl_symbol2888z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_directoryzd2ze3vectorz31zz__r4_ports_6_10_1z00(char *);
extern long bgl_file_uid(char *);
static obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62filezd2uidzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62inputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2mmapzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
extern int bgl_output_string(obj_t, obj_t);
static obj_t BGl__openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62close2100z62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62loopz62zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62close2102z62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62close2103z62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62close2104z62zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2fillzd2barrierzd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62withzd2errorzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62inputzd2gza7ipzd2portzf3z36zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
BGL_EXPORTED_DECL long BGl_filezd2changezd2timez00zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62inputzd2portzd2fillzd2barrierzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62directoryzd2ze3pathzd2vectorz81zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62parserz62zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern bool_t bgl_lockf(obj_t, int, long);
static obj_t BGl_z62zc3z04anonymousza31467ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62callzd2withzd2inputzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31459ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62withzd2inputzd2fromzd2procedurezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_inputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62filezd2modezb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62flushzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62currentzd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t);
extern bool_t bgl_directoryp(char *);
static obj_t BGl_z62withzd2outputzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_getzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t);
extern obj_t BGl_httpzd2parsezd2responsez00zz__httpz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62z52openzd2inputzd2filez30zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_filezd2siza7ez75zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2fillzd2barrierzd2setz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62outputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62callzd2withzd2inputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2989z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62withzd2outputzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
extern bool_t bgl_input_port_timeout_set(obj_t, long);
static obj_t BGl_z62zc3z04anonymousza31468ze3ze5zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62currentzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t);
extern long default_io_bufsiz;
static obj_t BGl_z62deletezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62callzd2withzd2appendzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2protocolzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62withzd2outputzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__r4_ports_6_10_1z00(void);
extern void bgl_input_port_seek(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62setzd2inputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
extern obj_t bgl_input_port_clone(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2errorzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern int bgl_utime(char *, long, long);
extern long bgl_directory_length(char *);
static obj_t BGl_z62directoryzd2lengthzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62withzd2inputzd2fromzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2994z00zz__r4_ports_6_10_1z00 = BUNSPEC;
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2996z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62outputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62resetzd2eofzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t get_output_string(obj_t);
BGL_EXPORTED_DECL bool_t BGl_directoryzf3zf3zz__r4_ports_6_10_1z00(char *);
extern long bgl_last_modification_time(char *);
extern obj_t bgl_open_output_string(obj_t);
static obj_t BGl_z62inputzd2portzd2lastzd2tokenzd2positionz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_makezd2directoryzd2zz__r4_ports_6_10_1z00(char *);
extern obj_t bgl_open_input_substring(obj_t, long, long);
static obj_t BGl_z62inputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62withzd2inputzd2fromzd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2fillzd2barrierzd2setz12z12zz__r4_ports_6_10_1z00(obj_t, long);
extern obj_t bgl_select(long, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern long bgl_file_gid(char *);
static obj_t BGl_z62filezd2gidzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62renamezd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
extern int bgl_symlink(char *, char *);
static obj_t BGl_z62closedzd2outputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_keyword2812z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62withzd2inputzd2fromzd2portzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_truncatezd2filezd2zz__r4_ports_6_10_1z00(char *, long);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2flushzd2bufferzd2setz12z12zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62z52openzd2inputzd2pipez30zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_keyword2816z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62callzd2withzd2outputzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_outputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t);
extern obj_t BGl_urlzd2sanszd2protocolzd2parsezd2zz__urlz00(obj_t, obj_t);
static obj_t BGl_z62inputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_reopen_input_c_string(obj_t, char *);
static obj_t BGl_z62outputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62withzd2appendzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
extern obj_t make_string(long, unsigned char);
static obj_t BGl_z62inputzd2portzd2clonez12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_open_input_c_string(char *);
extern obj_t bgl_close_input_port(obj_t);
static obj_t BGl_z62closezd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31415ze3ze5zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list2981z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62outputzd2portzd2flushzd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2inputzd2fromzd2procedurezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_directory_to_path_vector(char *, int, unsigned char);
BGL_EXPORTED_DECL int BGl_filezd2modezd2zz__r4_ports_6_10_1z00(char *);
BGL_EXPORTED_DECL obj_t BGl_flushzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t);
extern obj_t bgl_file_type(char *);
BGL_EXPORTED_DECL obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31487ze3ze5zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62directoryzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t close_binary_port(obj_t);
BGL_EXPORTED_DECL obj_t BGl_selectz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_deletezd2filezd2zz__r4_ports_6_10_1z00(char *);
extern obj_t BGl_httpz00zz__httpz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62filezd2accesszd2timez62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl__openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2707z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl__openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t bgl_append_output_file(obj_t, obj_t);
static obj_t BGl_symbol2709z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62inputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2inputzd2fromzd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t make_string_sans_fill(long);
static obj_t BGl_z62outputzd2portzd2isattyzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62outputzd2portzd2flushzd2hookzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2lengthz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_keyword2836z00zz__r4_ports_6_10_1z00 = BUNSPEC;
static obj_t BGl_z62makezd2symlinkzb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62resetzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2portzd2flushzd2bufferzd2zz__r4_ports_6_10_1z00(obj_t);
extern long bgl_output_port_timeout(obj_t);
BGL_EXPORTED_DECL obj_t BGl_directoryzd2ze3listz31zz__r4_ports_6_10_1z00(char *);
static obj_t BGl_z62inputzd2mmapzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_symbol2711z00zz__r4_ports_6_10_1z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2protocolz00zz__r4_ports_6_10_1z00(obj_t);
extern bool_t bigloo_strncmp(obj_t, obj_t, long);
static obj_t BGl_z62directoryzd2ze3listz53zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62setzd2outputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62z52openzd2inputzd2descriptorz30zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62flushz62zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62inputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_resetzd2eofzd2zz__r4_ports_6_10_1z00(obj_t);
extern obj_t bgl_close_output_port(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62callzd2withzd2outputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31441ze3ze5zz__r4_ports_6_10_1z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_directoryzd2ze3pathzd2vectorze3zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62copyzd2filezb0zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_lockfz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62inputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62filezd2typezb0zz__r4_ports_6_10_1z00(obj_t, obj_t);
static obj_t BGl_z62filezd2changezd2timez62zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2portzd2seekz00zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31489ze3ze5zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string3010z00zz__r4_ports_6_10_1z00, BgL_bgl_string3010za700za7za7_3011za7, "__r4_ports_6_10_1", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2mmapzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23012z00, opt_generic_entry, BGl__openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2directorieszd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762makeza7d2direct3013z00, BGl_z62makezd2directorieszb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closedzd2inputzd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762closedza7d2inpu3014z00, BGl_z62closedzd2inputzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2bufferzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3015z00, BGl_z62outputzd2portzd2bufferz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2fillzd2barrierzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73016za7, BGl_z62inputzd2portzd2fillzd2barrierzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2920z00zz__r4_ports_6_10_1z00, BgL_bgl_string2920za700za7za7_3017za7, "set-output-port-position!", 25 );
DEFINE_STRING( BGl_string2921z00zz__r4_ports_6_10_1z00, BgL_bgl_string2921za700za7za7_3018za7, "Cannot seek port", 16 );
DEFINE_STRING( BGl_string2840z00zz__r4_ports_6_10_1z00, BgL_bgl_string2840za700za7za7_3019za7, "wrong number of arguments: [1..3] expected, provided", 52 );
DEFINE_STRING( BGl_string2922z00zz__r4_ports_6_10_1z00, BgL_bgl_string2922za700za7za7_3020za7, "&set-output-port-position!", 26 );
DEFINE_STRING( BGl_string2841z00zz__r4_ports_6_10_1z00, BgL_bgl_string2841za700za7za7_3021za7, "_open-input-file", 16 );
DEFINE_STRING( BGl_string2760z00zz__r4_ports_6_10_1z00, BgL_bgl_string2760za700za7za7_3022za7, "<@anonymous:1455>", 17 );
DEFINE_STRING( BGl_string2923z00zz__r4_ports_6_10_1z00, BgL_bgl_string2923za700za7za7_3023za7, "&output-port-position", 21 );
DEFINE_STRING( BGl_string2842z00zz__r4_ports_6_10_1z00, BgL_bgl_string2842za700za7za7_3024za7, "loop", 4 );
DEFINE_STRING( BGl_string2761z00zz__r4_ports_6_10_1z00, BgL_bgl_string2761za700za7za7_3025za7, "with-append-to-file:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string2924z00zz__r4_ports_6_10_1z00, BgL_bgl_string2924za700za7za7_3026za7, "&output-port-isatty?", 20 );
DEFINE_STRING( BGl_string2843z00zz__r4_ports_6_10_1z00, BgL_bgl_string2843za700za7za7_3027za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string2762z00zz__r4_ports_6_10_1z00, BgL_bgl_string2762za700za7za7_3028za7, "with-append-to-file", 19 );
DEFINE_STRING( BGl_string2925z00zz__r4_ports_6_10_1z00, BgL_bgl_string2925za700za7za7_3029za7, "&input-port-name", 16 );
DEFINE_STRING( BGl_string2763z00zz__r4_ports_6_10_1z00, BgL_bgl_string2763za700za7za7_3030za7, "&with-append-to-file", 20 );
DEFINE_STRING( BGl_string2926z00zz__r4_ports_6_10_1z00, BgL_bgl_string2926za700za7za7_3031za7, "&input-port-name-set!", 21 );
DEFINE_STRING( BGl_string2764z00zz__r4_ports_6_10_1z00, BgL_bgl_string2764za700za7za7_3032za7, "<@anonymous:1457>", 17 );
DEFINE_STRING( BGl_string2927z00zz__r4_ports_6_10_1z00, BgL_bgl_string2927za700za7za7_3033za7, "&input-port-length", 18 );
DEFINE_STRING( BGl_string2846z00zz__r4_ports_6_10_1z00, BgL_bgl_string2846za700za7za7_3034za7, "open", 4 );
DEFINE_STRING( BGl_string2765z00zz__r4_ports_6_10_1z00, BgL_bgl_string2765za700za7za7_3035za7, "with-output-to-port:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string2928z00zz__r4_ports_6_10_1z00, BgL_bgl_string2928za700za7za7_3036za7, "&output-port-close-hook", 23 );
DEFINE_STRING( BGl_string2766z00zz__r4_ports_6_10_1z00, BgL_bgl_string2766za700za7za7_3037za7, "with-output-to-port", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2timeoutzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73038za7, BGl_z62inputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2929z00zz__r4_ports_6_10_1z00, BgL_bgl_string2929za700za7za7_3039za7, "&closed-output-port?", 20 );
DEFINE_STRING( BGl_string2848z00zz__r4_ports_6_10_1z00, BgL_bgl_string2848za700za7za7_3040za7, "name", 4 );
DEFINE_STRING( BGl_string2767z00zz__r4_ports_6_10_1z00, BgL_bgl_string2767za700za7za7_3041za7, "&with-output-to-port", 20 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_z52openzd2inputzd2httpzd2socketzd2envz52zz__r4_ports_6_10_1z00, BgL_bgl_za762za752openza7d2inp3042za7, BGl_z62z52openzd2inputzd2httpzd2socketze2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2768z00zz__r4_ports_6_10_1z00, BgL_bgl_string2768za700za7za7_3043za7, "with-output-to-string:Wrong number of arguments", 47 );
DEFINE_STRING( BGl_string2769z00zz__r4_ports_6_10_1z00, BgL_bgl_string2769za700za7za7_3044za7, "with-output-to-string", 21 );
DEFINE_STRING( BGl_string2689z00zz__r4_ports_6_10_1z00, BgL_bgl_string2689za700za7za7_3045za7, "input-port-protocols", 20 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2descriptorzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23046z00, opt_generic_entry, BGl__openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2modificationzd2timezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2modifi3047z00, BGl_z62filezd2modificationzd2timez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2outputzd2tozd2procedurezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2output3048z00, BGl_z62withzd2outputzd2tozd2procedurezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2outputzd2stringzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762getza7d2outputza73049za7, BGl_z62getzd2outputzd2stringz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2namezd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3050z00, BGl_z62outputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2existszf3zd2envzf3zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2exists3051z00, BGl_z62filezd2existszf3z43zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2930z00zz__r4_ports_6_10_1z00, BgL_bgl_string2930za700za7za7_3052za7, "output-port-close-hook-set!", 27 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_z52openzd2inputzd2pipezd2envz80zz__r4_ports_6_10_1z00, BgL_bgl_za762za752openza7d2inp3053za7, BGl_z62z52openzd2inputzd2pipez30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2931z00zz__r4_ports_6_10_1z00, BgL_bgl_string2931za700za7za7_3054za7, "Illegal hook", 12 );
DEFINE_STRING( BGl_string2850z00zz__r4_ports_6_10_1z00, BgL_bgl_string2850za700za7za7_3055za7, "buffer", 6 );
DEFINE_STRING( BGl_string2932z00zz__r4_ports_6_10_1z00, BgL_bgl_string2932za700za7za7_3056za7, "&output-port-close-hook-set!", 28 );
DEFINE_STRING( BGl_string2770z00zz__r4_ports_6_10_1z00, BgL_bgl_string2770za700za7za7_3057za7, "&with-output-to-string", 22 );
DEFINE_STRING( BGl_string2933z00zz__r4_ports_6_10_1z00, BgL_bgl_string2933za700za7za7_3058za7, "&output-port-flush-hook", 23 );
DEFINE_STRING( BGl_string2852z00zz__r4_ports_6_10_1z00, BgL_bgl_string2852za700za7za7_3059za7, "timeout", 7 );
DEFINE_STRING( BGl_string2690z00zz__r4_ports_6_10_1z00, BgL_bgl_string2690za700za7za7_3060za7, "file:", 5 );
DEFINE_STRING( BGl_string2934z00zz__r4_ports_6_10_1z00, BgL_bgl_string2934za700za7za7_3061za7, "output-port-flush-hook-set!", 27 );
DEFINE_STRING( BGl_string2691z00zz__r4_ports_6_10_1z00, BgL_bgl_string2691za700za7za7_3062za7, "string:", 7 );
DEFINE_STRING( BGl_string2935z00zz__r4_ports_6_10_1z00, BgL_bgl_string2935za700za7za7_3063za7, "&output-port-flush-hook-set!", 28 );
DEFINE_STRING( BGl_string2854z00zz__r4_ports_6_10_1z00, BgL_bgl_string2854za700za7za7_3064za7, "open-input-descriptor", 21 );
DEFINE_STRING( BGl_string2773z00zz__r4_ports_6_10_1z00, BgL_bgl_string2773za700za7za7_3065za7, "with-output-to-procedure", 24 );
DEFINE_STRING( BGl_string2936z00zz__r4_ports_6_10_1z00, BgL_bgl_string2936za700za7za7_3066za7, "&output-port-flush-buffer", 25 );
DEFINE_STRING( BGl_string2855z00zz__r4_ports_6_10_1z00, BgL_bgl_string2855za700za7za7_3067za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string2774z00zz__r4_ports_6_10_1z00, BgL_bgl_string2774za700za7za7_3068za7, "with-output-to-procedure:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string2693z00zz__r4_ports_6_10_1z00, BgL_bgl_string2693za700za7za7_3069za7, "| ", 2 );
DEFINE_STRING( BGl_string2937z00zz__r4_ports_6_10_1z00, BgL_bgl_string2937za700za7za7_3070za7, "&output-port-flush-buffer-set!", 30 );
DEFINE_STRING( BGl_string2856z00zz__r4_ports_6_10_1z00, BgL_bgl_string2856za700za7za7_3071za7, "_open-input-descriptor", 22 );
DEFINE_STRING( BGl_string2775z00zz__r4_ports_6_10_1z00, BgL_bgl_string2775za700za7za7_3072za7, "&with-output-to-procedure", 25 );
DEFINE_STRING( BGl_string2694z00zz__r4_ports_6_10_1z00, BgL_bgl_string2694za700za7za7_3073za7, "pipe:", 5 );
DEFINE_STRING( BGl_string2938z00zz__r4_ports_6_10_1z00, BgL_bgl_string2938za700za7za7_3074za7, "&input-port-close-hook", 22 );
DEFINE_STRING( BGl_string2776z00zz__r4_ports_6_10_1z00, BgL_bgl_string2776za700za7za7_3075za7, "<@anonymous:1460>", 17 );
DEFINE_STRING( BGl_string2695z00zz__r4_ports_6_10_1z00, BgL_bgl_string2695za700za7za7_3076za7, "http://", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2appendzd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762callza7d2withza7d3077za7, BGl_z62callzd2withzd2appendzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2939z00zz__r4_ports_6_10_1z00, BgL_bgl_string2939za700za7za7_3078za7, "input-port-close-hook-set!", 26 );
DEFINE_STRING( BGl_string2858z00zz__r4_ports_6_10_1z00, BgL_bgl_string2858za700za7za7_3079za7, "open-input-string", 17 );
DEFINE_STRING( BGl_string2777z00zz__r4_ports_6_10_1z00, BgL_bgl_string2777za700za7za7_3080za7, "with-error-to-string:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string2696z00zz__r4_ports_6_10_1z00, BgL_bgl_string2696za700za7za7_3081za7, "gzip:", 5 );
DEFINE_STRING( BGl_string2859z00zz__r4_ports_6_10_1z00, BgL_bgl_string2859za700za7za7_3082za7, "_open-input-string", 18 );
DEFINE_STRING( BGl_string2778z00zz__r4_ports_6_10_1z00, BgL_bgl_string2778za700za7za7_3083za7, "with-error-to-string", 20 );
DEFINE_STRING( BGl_string2697z00zz__r4_ports_6_10_1z00, BgL_bgl_string2697za700za7za7_3084za7, "zlib:", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2flushzd2bufferzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3085z00, BGl_z62outputzd2portzd2flushzd2bufferzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2779z00zz__r4_ports_6_10_1z00, BgL_bgl_string2779za700za7za7_3086za7, "&with-error-to-string", 21 );
DEFINE_STRING( BGl_string2698z00zz__r4_ports_6_10_1z00, BgL_bgl_string2698za700za7za7_3087za7, "inflate:", 8 );
DEFINE_STRING( BGl_string2699z00zz__r4_ports_6_10_1z00, BgL_bgl_string2699za700za7za7_3088za7, "/resource/", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2filezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23089z00, opt_generic_entry, BGl__openzd2inputzd2filez00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2outputzd2tozd2portzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2output3090z00, BGl_z62withzd2outputzd2tozd2portzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2protocolzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73091za7, BGl_z62inputzd2portzd2protocolz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2stringzd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2strin3092z00, BGl_z62inputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2outputzd2procedurezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2outputza7d3093z00, opt_generic_entry, BGl__openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_copyzd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762copyza7d2fileza7b3094za7, BGl_z62copyzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2gza7ipzd2portzf3zd2envz86zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2gza7a7i3095za7, BGl_z62inputzd2gza7ipzd2portzf3z36zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2errorzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762currentza7d2err3096z00, BGl_z62currentzd2errorzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_directoryzd2ze3pathzd2vectorzd2envz31zz__r4_ports_6_10_1z00, BgL_bgl_za762directoryza7d2za73097za7, BGl_z62directoryzd2ze3pathzd2vectorz81zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2771z00zz__r4_ports_6_10_1z00, BgL_bgl_za762flushza762za7za7__r3098z00, BGl_z62flushz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2940z00zz__r4_ports_6_10_1z00, BgL_bgl_string2940za700za7za7_3099za7, "&input-port-close-hook-set!", 27 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2772z00zz__r4_ports_6_10_1z00, BgL_bgl_za762closeza762za7za7__r3100z00, BGl_z62closez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2941z00zz__r4_ports_6_10_1z00, BgL_bgl_string2941za700za7za7_3101za7, "input-port-seek", 15 );
DEFINE_STRING( BGl_string2860z00zz__r4_ports_6_10_1z00, BgL_bgl_string2860za700za7za7_3102za7, "Illegal start offset", 20 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2692z00zz__r4_ports_6_10_1z00, BgL_bgl_za762za7c3za704anonymo3103za7, BGl_z62zc3z04anonymousza31415ze3ze5zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2942z00zz__r4_ports_6_10_1z00, BgL_bgl_string2942za700za7za7_3104za7, "&input-port-seek", 16 );
DEFINE_STRING( BGl_string2861z00zz__r4_ports_6_10_1z00, BgL_bgl_string2861za700za7za7_3105za7, "Start offset out of bounds", 26 );
DEFINE_STRING( BGl_string2780z00zz__r4_ports_6_10_1z00, BgL_bgl_string2780za700za7za7_3106za7, "with-error-to-file:Wrong number of arguments", 44 );
DEFINE_STRING( BGl_string2943z00zz__r4_ports_6_10_1z00, BgL_bgl_string2943za700za7za7_3107za7, "input-port-seek-set!", 20 );
DEFINE_STRING( BGl_string2862z00zz__r4_ports_6_10_1z00, BgL_bgl_string2862za700za7za7_3108za7, "Start offset greater than end", 29 );
DEFINE_STRING( BGl_string2781z00zz__r4_ports_6_10_1z00, BgL_bgl_string2781za700za7za7_3109za7, "with-error-to-file", 18 );
DEFINE_STRING( BGl_string2944z00zz__r4_ports_6_10_1z00, BgL_bgl_string2944za700za7za7_3110za7, "Illegal seek procedure", 22 );
DEFINE_STRING( BGl_string2863z00zz__r4_ports_6_10_1z00, BgL_bgl_string2863za700za7za7_3111za7, "End offset out of bounds", 24 );
DEFINE_STRING( BGl_string2945z00zz__r4_ports_6_10_1z00, BgL_bgl_string2945za700za7za7_3112za7, "&input-port-seek-set!", 21 );
DEFINE_STRING( BGl_string2783z00zz__r4_ports_6_10_1z00, BgL_bgl_string2783za700za7za7_3113za7, "&with-error-to-file", 19 );
DEFINE_STRING( BGl_string2946z00zz__r4_ports_6_10_1z00, BgL_bgl_string2946za700za7za7_3114za7, "&input-port-buffer", 18 );
DEFINE_STRING( BGl_string2865z00zz__r4_ports_6_10_1z00, BgL_bgl_string2865za700za7za7_3115za7, "open-input-string!", 18 );
DEFINE_STRING( BGl_string2784z00zz__r4_ports_6_10_1z00, BgL_bgl_string2784za700za7za7_3116za7, "<@anonymous:1466>", 17 );
DEFINE_STRING( BGl_string2947z00zz__r4_ports_6_10_1z00, BgL_bgl_string2947za700za7za7_3117za7, "&input-port-buffer-set!", 23 );
DEFINE_STRING( BGl_string2866z00zz__r4_ports_6_10_1z00, BgL_bgl_string2866za700za7za7_3118za7, "_open-input-string!", 19 );
DEFINE_STRING( BGl_string2785z00zz__r4_ports_6_10_1z00, BgL_bgl_string2785za700za7za7_3119za7, "with-error-to-port:Wrong number of arguments", 44 );
DEFINE_STRING( BGl_string2948z00zz__r4_ports_6_10_1z00, BgL_bgl_string2948za700za7za7_3120za7, "&output-port-buffer", 19 );
DEFINE_STRING( BGl_string2786z00zz__r4_ports_6_10_1z00, BgL_bgl_string2786za700za7za7_3121za7, "with-error-to-port", 18 );
DEFINE_STRING( BGl_string2949z00zz__r4_ports_6_10_1z00, BgL_bgl_string2949za700za7za7_3122za7, "&output-port-buffer-set!", 24 );
DEFINE_STRING( BGl_string2868z00zz__r4_ports_6_10_1z00, BgL_bgl_string2868za700za7za7_3123za7, "open-input-mmap", 15 );
DEFINE_STRING( BGl_string2787z00zz__r4_ports_6_10_1z00, BgL_bgl_string2787za700za7za7_3124za7, "&with-error-to-port", 19 );
DEFINE_STRING( BGl_string2869z00zz__r4_ports_6_10_1z00, BgL_bgl_string2869za700za7za7_3125za7, "_open-input-mmap", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2accesszd2timezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2access3126z00, BGl_z62filezd2accesszd2timez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2timeszd2setz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2timesza73127za7, BGl_z62filezd2timeszd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
extern obj_t BGl_openzd2inputzd2inflatezd2filezd2envz00zz__gunza7ipza7;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2flushzd2hookzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3128z00, BGl_z62outputzd2portzd2flushzd2hookzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2errorzd2tozd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2errorza73129za7, BGl_z62withzd2errorzd2tozd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2950z00zz__r4_ports_6_10_1z00, BgL_bgl_string2950za700za7za7_3130za7, "&file-exists?", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2namezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3131z00, BGl_z62outputzd2portzd2namez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2timeoutzd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3132z00, BGl_z62outputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2951z00zz__r4_ports_6_10_1z00, BgL_bgl_string2951za700za7za7_3133za7, "file-gzip?", 10 );
DEFINE_STRING( BGl_string2870z00zz__r4_ports_6_10_1z00, BgL_bgl_string2870za700za7za7_3134za7, "mmap", 4 );
DEFINE_STRING( BGl_string2790z00zz__r4_ports_6_10_1z00, BgL_bgl_string2790za700za7za7_3135za7, "with-error-to-procedure", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_directoryzd2ze3vectorzd2envze3zz__r4_ports_6_10_1z00, BgL_bgl_za762directoryza7d2za73136za7, BGl_z62directoryzd2ze3vectorz53zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
extern obj_t BGl_openzd2inputzd2ftpzd2filezd2envz00zz__ftpz00;
DEFINE_STRING( BGl_string2953z00zz__r4_ports_6_10_1z00, BgL_bgl_string2953za700za7za7_3137za7, "<@exit:1609>~0", 14 );
DEFINE_STRING( BGl_string2872z00zz__r4_ports_6_10_1z00, BgL_bgl_string2872za700za7za7_3138za7, "_open-input-procedure", 21 );
DEFINE_STRING( BGl_string2791z00zz__r4_ports_6_10_1z00, BgL_bgl_string2791za700za7za7_3139za7, "with-error-to-procedure:Wrong number of arguments", 49 );
DEFINE_STRING( BGl_string2954z00zz__r4_ports_6_10_1z00, BgL_bgl_string2954za700za7za7_3140za7, "&delete-file", 12 );
DEFINE_STRING( BGl_string2792z00zz__r4_ports_6_10_1z00, BgL_bgl_string2792za700za7za7_3141za7, "&with-error-to-procedure", 24 );
DEFINE_STRING( BGl_string2955z00zz__r4_ports_6_10_1z00, BgL_bgl_string2955za700za7za7_3142za7, "&make-directory", 15 );
DEFINE_STRING( BGl_string2874z00zz__r4_ports_6_10_1z00, BgL_bgl_string2874za700za7za7_3143za7, "open-input-gzip-port", 20 );
DEFINE_STRING( BGl_string2793z00zz__r4_ports_6_10_1z00, BgL_bgl_string2793za700za7za7_3144za7, "<@anonymous:1468>", 17 );
DEFINE_STRING( BGl_string2956z00zz__r4_ports_6_10_1z00, BgL_bgl_string2956za700za7za7_3145za7, "string-ref", 10 );
DEFINE_STRING( BGl_string2875z00zz__r4_ports_6_10_1z00, BgL_bgl_string2875za700za7za7_3146za7, "_open-input-gzip-port", 21 );
DEFINE_STRING( BGl_string2794z00zz__r4_ports_6_10_1z00, BgL_bgl_string2794za700za7za7_3147za7, "input-port-protocol", 19 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2788z00zz__r4_ports_6_10_1z00, BgL_bgl_za762flush2101za762za73148za7, BGl_z62flush2101z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2procedurezd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2proce3149z00, BGl_z62inputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2957z00zz__r4_ports_6_10_1z00, BgL_bgl_string2957za700za7za7_3150za7, "&make-directories", 17 );
DEFINE_STRING( BGl_string2876z00zz__r4_ports_6_10_1z00, BgL_bgl_string2876za700za7za7_3151za7, "&open-input-c-string", 20 );
DEFINE_STRING( BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_bgl_string2795za700za7za7_3152za7, "pair-nil", 8 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2789z00zz__r4_ports_6_10_1z00, BgL_bgl_za762close2100za762za73153za7, BGl_z62close2100z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2958z00zz__r4_ports_6_10_1z00, BgL_bgl_string2958za700za7za7_3154za7, "&delete-directory", 17 );
DEFINE_STRING( BGl_string2877z00zz__r4_ports_6_10_1z00, BgL_bgl_string2877za700za7za7_3155za7, "&reopen-input-c-string", 22 );
DEFINE_STRING( BGl_string2796z00zz__r4_ports_6_10_1z00, BgL_bgl_string2796za700za7za7_3156za7, "input-port-protocol-set!", 24 );
DEFINE_STRING( BGl_string2959z00zz__r4_ports_6_10_1z00, BgL_bgl_string2959za700za7za7_3157za7, "&rename-file", 12 );
DEFINE_STRING( BGl_string2878z00zz__r4_ports_6_10_1z00, BgL_bgl_string2878za700za7za7_3158za7, "&input-port-timeout", 19 );
DEFINE_STRING( BGl_string2797z00zz__r4_ports_6_10_1z00, BgL_bgl_string2797za700za7za7_3159za7, "Illegal open procedure for protocol", 35 );
DEFINE_STRING( BGl_string2879z00zz__r4_ports_6_10_1z00, BgL_bgl_string2879za700za7za7_3160za7, "&input-port-timeout-set!", 24 );
DEFINE_STRING( BGl_string2798z00zz__r4_ports_6_10_1z00, BgL_bgl_string2798za700za7za7_3161za7, "Illegal buffer", 14 );
DEFINE_STRING( BGl_string2799z00zz__r4_ports_6_10_1z00, BgL_bgl_string2799za700za7za7_3162za7, "get-port-buffer", 15 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_z52openzd2inputzd2descriptorzd2envz80zz__r4_ports_6_10_1z00, BgL_bgl_za762za752openza7d2inp3163za7, BGl_z62z52openzd2inputzd2descriptorz30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2clonez12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73164za7, BGl_z62inputzd2portzd2clonez12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_directoryzd2lengthzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762directoryza7d2l3165z00, BGl_z62directoryzd2lengthzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2changezd2timezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2change3166z00, BGl_z62filezd2changezd2timez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2952z00zz__r4_ports_6_10_1z00, BgL_bgl_za762za7c3za704anonymo3167za7, BGl_z62zc3z04anonymousza31607ze3ze5zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2960z00zz__r4_ports_6_10_1z00, BgL_bgl_string2960za700za7za7_3168za7, "&truncate-file", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2modezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2modeza7b3169za7, BGl_z62filezd2modezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flushzd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762flushza7d2outpu3170z00, BGl_z62flushzd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2961z00zz__r4_ports_6_10_1z00, BgL_bgl_string2961za700za7za7_3171za7, "&output-port-truncate", 21 );
DEFINE_STRING( BGl_string2880z00zz__r4_ports_6_10_1z00, BgL_bgl_string2880za700za7za7_3172za7, "&output-port-timeout", 20 );
DEFINE_STRING( BGl_string2962z00zz__r4_ports_6_10_1z00, BgL_bgl_string2962za700za7za7_3173za7, "&copy-file", 10 );
DEFINE_STRING( BGl_string2963z00zz__r4_ports_6_10_1z00, BgL_bgl_string2963za700za7za7_3174za7, "&directory?", 11 );
DEFINE_STRING( BGl_string2882z00zz__r4_ports_6_10_1z00, BgL_bgl_string2882za700za7za7_3175za7, "_open-output-file", 17 );
DEFINE_STRING( BGl_string2964z00zz__r4_ports_6_10_1z00, BgL_bgl_string2964za700za7za7_3176za7, "&directory-length", 17 );
DEFINE_STRING( BGl_string2965z00zz__r4_ports_6_10_1z00, BgL_bgl_string2965za700za7za7_3177za7, "&directory->list", 16 );
DEFINE_STRING( BGl_string2884z00zz__r4_ports_6_10_1z00, BgL_bgl_string2884za700za7za7_3178za7, "_append-output-file", 19 );
DEFINE_STRING( BGl_string2966z00zz__r4_ports_6_10_1z00, BgL_bgl_string2966za700za7za7_3179za7, "&directory->path-list", 21 );
DEFINE_STRING( BGl_string2967z00zz__r4_ports_6_10_1z00, BgL_bgl_string2967za700za7za7_3180za7, "&directory->vector", 18 );
DEFINE_STRING( BGl_string2886z00zz__r4_ports_6_10_1z00, BgL_bgl_string2886za700za7za7_3181za7, "open-output-string", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2errorzd2tozd2procedurezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2errorza73182za7, BGl_z62withzd2errorzd2tozd2procedurezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2887z00zz__r4_ports_6_10_1z00, BgL_bgl_string2887za700za7za7_3183za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string2969z00zz__r4_ports_6_10_1z00, BgL_bgl_string2969za700za7za7_3184za7, "&directory->path-vector", 23 );
DEFINE_STRING( BGl_string2889z00zz__r4_ports_6_10_1z00, BgL_bgl_string2889za700za7za7_3185za7, "open-output-procedure", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2closezd2hookzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73186za7, BGl_z62inputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762portza7f3za791za7za7_3187za7, BGl_z62portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2lengthzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73188za7, BGl_z62inputzd2portzd2lengthz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2970z00zz__r4_ports_6_10_1z00, BgL_bgl_string2970za700za7za7_3189za7, "&file-modification-time", 23 );
DEFINE_STRING( BGl_string2971z00zz__r4_ports_6_10_1z00, BgL_bgl_string2971za700za7za7_3190za7, "&file-change-time", 17 );
DEFINE_STRING( BGl_string2890z00zz__r4_ports_6_10_1z00, BgL_bgl_string2890za700za7za7_3191za7, "wrong number of arguments: [1..4] expected, provided", 52 );
DEFINE_STRING( BGl_string2972z00zz__r4_ports_6_10_1z00, BgL_bgl_string2972za700za7za7_3192za7, "&file-access-time", 17 );
DEFINE_STRING( BGl_string2973z00zz__r4_ports_6_10_1z00, BgL_bgl_string2973za700za7za7_3193za7, "&file-times-set!", 16 );
DEFINE_STRING( BGl_string2974z00zz__r4_ports_6_10_1z00, BgL_bgl_string2974za700za7za7_3194za7, "belong", 6 );
DEFINE_STRING( BGl_string2893z00zz__r4_ports_6_10_1z00, BgL_bgl_string2893za700za7za7_3195za7, "_open-output-procedure", 22 );
DEFINE_STRING( BGl_string2975z00zz__r4_ports_6_10_1z00, BgL_bgl_string2975za700za7za7_3196za7, "&file-size", 10 );
DEFINE_STRING( BGl_string2976z00zz__r4_ports_6_10_1z00, BgL_bgl_string2976za700za7za7_3197za7, "&file-uid", 9 );
DEFINE_STRING( BGl_string2977z00zz__r4_ports_6_10_1z00, BgL_bgl_string2977za700za7za7_3198za7, "&file-gid", 9 );
DEFINE_STRING( BGl_string2896z00zz__r4_ports_6_10_1z00, BgL_bgl_string2896za700za7za7_3199za7, "Illegal flush procedure", 23 );
extern obj_t BGl_openzd2inputzd2za7libzd2filezd2envza7zz__gunza7ipza7;
DEFINE_STRING( BGl_string2978z00zz__r4_ports_6_10_1z00, BgL_bgl_string2978za700za7za7_3200za7, "&file-mode", 10 );
DEFINE_STRING( BGl_string2897z00zz__r4_ports_6_10_1z00, BgL_bgl_string2897za700za7za7_3201za7, "Illegal close procedure", 23 );
DEFINE_STRING( BGl_string2979z00zz__r4_ports_6_10_1z00, BgL_bgl_string2979za700za7za7_3202za7, "&file-type", 10 );
DEFINE_STRING( BGl_string2898z00zz__r4_ports_6_10_1z00, BgL_bgl_string2898za700za7za7_3203za7, "Illegal write procedure", 23 );
DEFINE_STRING( BGl_string2899z00zz__r4_ports_6_10_1z00, BgL_bgl_string2899za700za7za7_3204za7, "&output-port-timeout-set!", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2czd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762openza7d2inputza73205za7, BGl_z62openzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2errorzd2tozd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2errorza73206za7, BGl_z62withzd2errorzd2tozd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_resetzd2eofzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762resetza7d2eofza7b3207za7, BGl_z62resetzd2eofzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2inputzd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762callza7d2withza7d3208za7, BGl_z62callzd2withzd2inputzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2seekzd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73209za7, BGl_z62inputzd2portzd2seekzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2891z00zz__r4_ports_6_10_1z00, BgL_bgl_za762flush2105za762za73210za7, BGl_z62flush2105z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2892z00zz__r4_ports_6_10_1z00, BgL_bgl_za762close2104za762za73211za7, BGl_z62close2104z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2980z00zz__r4_ports_6_10_1z00, BgL_bgl_string2980za700za7za7_3212za7, "&make-symlink", 13 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2894z00zz__r4_ports_6_10_1z00, BgL_bgl_za762close2103za762za73213za7, BGl_z62close2103z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2895z00zz__r4_ports_6_10_1z00, BgL_bgl_za762close2102za762za73214za7, BGl_z62close2102z62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2983z00zz__r4_ports_6_10_1z00, BgL_bgl_string2983za700za7za7_3215za7, "except", 6 );
DEFINE_STRING( BGl_string2985z00zz__r4_ports_6_10_1z00, BgL_bgl_string2985za700za7za7_3216za7, "read", 4 );
DEFINE_STRING( BGl_string2988z00zz__r4_ports_6_10_1z00, BgL_bgl_string2988za700za7za7_3217za7, "write", 5 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_z52openzd2inputzd2filezd2envz80zz__r4_ports_6_10_1z00, BgL_bgl_za762za752openza7d2inp3218za7, BGl_z62z52openzd2inputzd2filez30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_renamezd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762renameza7d2file3219z00, BGl_z62renamezd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2positionzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3220z00, BGl_z62outputzd2portzd2positionz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2inputzd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762callza7d2withza7d3221za7, BGl_z62callzd2withzd2inputzd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closedzd2outputzd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762closedza7d2outp3222z00, BGl_z62closedzd2outputzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2timeoutzd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73223za7, BGl_z62inputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2outputzd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762callza7d2withza7d3224za7, BGl_z62callzd2withzd2outputzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2symlinkzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762makeza7d2symlin3225z00, BGl_z62makezd2symlinkzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_truncatezd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762truncateza7d2fi3226z00, BGl_z62truncatezd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2flushzd2bufferzd2setz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3227z00, BGl_z62outputzd2portzd2flushzd2bufferzd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2990z00zz__r4_ports_6_10_1z00, BgL_bgl_string2990za700za7za7_3228za7, "select", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_deletezd2directoryzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762deleteza7d2dire3229z00, BGl_z62deletezd2directoryzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2991z00zz__r4_ports_6_10_1z00, BgL_bgl_string2991za700za7za7_3230za7, "Illegal keyword argument", 24 );
DEFINE_STRING( BGl_string2992z00zz__r4_ports_6_10_1z00, BgL_bgl_string2992za700za7za7_3231za7, "_select", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2timeoutzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3232z00, BGl_z62outputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2993z00zz__r4_ports_6_10_1z00, BgL_bgl_string2993za700za7za7_3233za7, "wrong number of arguments: [0..4] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2reopenz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73234za7, BGl_z62inputzd2portzd2reopenz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2995z00zz__r4_ports_6_10_1z00, BgL_bgl_string2995za700za7za7_3235za7, "open-pipes", 10 );
DEFINE_STRING( BGl_string2997z00zz__r4_ports_6_10_1z00, BgL_bgl_string2997za700za7za7_3236za7, "lockf", 5 );
DEFINE_STRING( BGl_string2998z00zz__r4_ports_6_10_1z00, BgL_bgl_string2998za700za7za7_3237za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_STRING( BGl_string2999z00zz__r4_ports_6_10_1z00, BgL_bgl_string2999za700za7za7_3238za7, "_lockf", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2pipeszd2envz00zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2pipesza7d23239z00, opt_generic_entry, BGl__openzd2pipeszd2zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2inputzd2fromzd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2inputza73240za7, BGl_z62withzd2inputzd2fromzd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2namezd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73241za7, BGl_z62inputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2errorzd2tozd2portzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2errorza73242za7, BGl_z62withzd2errorzd2tozd2portzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_directoryzd2ze3pathzd2listzd2envz31zz__r4_ports_6_10_1z00, BgL_bgl_za762directoryza7d2za73243za7, BGl_z62directoryzd2ze3pathzd2listz81zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2truncatezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3244z00, BGl_z62outputzd2portzd2truncatez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2portzd2bufferzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762getza7d2portza7d23245za7, BGl_z62getzd2portzd2bufferz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2inputzd2fromzd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2inputza73246za7, BGl_z62withzd2inputzd2fromzd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2gidzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2gidza7b03247za7, BGl_z62filezd2gidzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2stringzd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2stri3248z00, BGl_z62outputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2siza7ezd2envza7zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2siza7a7e3249za7, BGl_z62filezd2siza7ez17zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2isattyzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3250z00, BGl_z62outputzd2portzd2isattyzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2stringz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23251z00, opt_generic_entry, BGl__openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_reopenzd2inputzd2czd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762reopenza7d2inpu3252z00, BGl_z62reopenzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
extern obj_t BGl_openzd2inputzd2gza7ipzd2filezd2envza7zz__gunza7ipza7;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closezd2inputzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762closeza7d2input3253z00, BGl_z62closezd2inputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2withzd2outputzd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762callza7d2withza7d3254za7, BGl_z62callzd2withzd2outputzd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2bufferzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73255za7, BGl_z62inputzd2portzd2bufferz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_z52openzd2inputzd2resourcezd2envz80zz__r4_ports_6_10_1z00, BgL_bgl_za762za752openza7d2inp3256za7, BGl_z62z52openzd2inputzd2resourcez30zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2700z00zz__r4_ports_6_10_1z00, BgL_bgl_string2700za700za7za7_3257za7, "ftp://", 6 );
DEFINE_STRING( BGl_string2701z00zz__r4_ports_6_10_1z00, BgL_bgl_string2701za700za7za7_3258za7, "fd:", 3 );
DEFINE_STRING( BGl_string2702z00zz__r4_ports_6_10_1z00, BgL_bgl_string2702za700za7za7_3259za7, "/tmp/bigloo/runtime/Ieee/port.scm", 33 );
DEFINE_STRING( BGl_string2703z00zz__r4_ports_6_10_1z00, BgL_bgl_string2703za700za7za7_3260za7, "<@anonymous:1415>", 17 );
DEFINE_STRING( BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_bgl_string2704za700za7za7_3261za7, "bstring", 7 );
DEFINE_STRING( BGl_string2705z00zz__r4_ports_6_10_1z00, BgL_bgl_string2705za700za7za7_3262za7, "call-with-input-file:Wrong number of arguments", 46 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_lockfzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__lockfza700za7za7__r4_3263za7, opt_generic_entry, BGl__lockfz00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2708z00zz__r4_ports_6_10_1z00, BgL_bgl_string2708za700za7za7_3264za7, "funcall", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_directoryzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762directoryza7f3za73265za7, BGl_z62directoryzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2seekzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73266za7, BGl_z62inputzd2portzd2seekz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2directoryzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762makeza7d2direct3267z00, BGl_z62makezd2directoryzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd2outputzd2filezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__appendza7d2output3268za7, opt_generic_entry, BGl__appendzd2outputzd2filez00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2bufferzd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3269z00, BGl_z62outputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2fillzd2barrierzd2setz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73270za7, BGl_z62inputzd2portzd2fillzd2barrierzd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2710z00zz__r4_ports_6_10_1z00, BgL_bgl_string2710za700za7za7_3271za7, "proc", 4 );
DEFINE_STRING( BGl_string2712z00zz__r4_ports_6_10_1z00, BgL_bgl_string2712za700za7za7_3272za7, "port", 4 );
DEFINE_STRING( BGl_string2713z00zz__r4_ports_6_10_1z00, BgL_bgl_string2713za700za7za7_3273za7, "call-with-input-file", 20 );
DEFINE_STRING( BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_bgl_string2714za700za7za7_3274za7, "pair", 4 );
DEFINE_STRING( BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_bgl_string2715za700za7za7_3275za7, "can't open file", 15 );
DEFINE_STRING( BGl_string2716z00zz__r4_ports_6_10_1z00, BgL_bgl_string2716za700za7za7_3276za7, "&call-with-input-file", 21 );
DEFINE_STRING( BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_bgl_string2717za700za7za7_3277za7, "procedure", 9 );
DEFINE_STRING( BGl_string2718z00zz__r4_ports_6_10_1z00, BgL_bgl_string2718za700za7za7_3278za7, "<@anonymous:1439>", 17 );
DEFINE_STRING( BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_bgl_string2719za700za7za7_3279za7, "input-port", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2mmapzd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2mmapza73280za7, BGl_z62inputzd2mmapzd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2closezd2hookzd2setz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3281z00, BGl_z62outputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setzd2inputzd2portzd2positionz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762setza7d2inputza7d3282za7, BGl_z62setzd2inputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2appendzd2tozd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2append3283z00, BGl_z62withzd2appendzd2tozd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2gza7ipzf3zd2envz54zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2gza7a7ip3284za7, BGl_z62filezd2gza7ipzf3ze4zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closezd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762closeza7d2outpu3285z00, BGl_z62closezd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2flushzd2hookzd2setz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3286z00, BGl_z62outputzd2portzd2flushzd2hookzd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2800z00zz__r4_ports_6_10_1z00, BgL_bgl_string2800za700za7za7_3287za7, "&get-port-buffer", 16 );
DEFINE_STRING( BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_bgl_string2801za700za7za7_3288za7, "bint", 4 );
DEFINE_STRING( BGl_string2720z00zz__r4_ports_6_10_1z00, BgL_bgl_string2720za700za7za7_3289za7, "call-with-input-string:Wrong number of arguments", 48 );
DEFINE_STRING( BGl_string2802z00zz__r4_ports_6_10_1z00, BgL_bgl_string2802za700za7za7_3290za7, "%open-input-file", 16 );
DEFINE_STRING( BGl_string2721z00zz__r4_ports_6_10_1z00, BgL_bgl_string2721za700za7za7_3291za7, "&call-with-input-string", 23 );
DEFINE_STRING( BGl_string2803z00zz__r4_ports_6_10_1z00, BgL_bgl_string2803za700za7za7_3292za7, "%open-input-descriptor", 22 );
DEFINE_STRING( BGl_string2722z00zz__r4_ports_6_10_1z00, BgL_bgl_string2722za700za7za7_3293za7, "open-output-file", 16 );
DEFINE_STRING( BGl_string2804z00zz__r4_ports_6_10_1z00, BgL_bgl_string2804za700za7za7_3294za7, "open-input-pipe", 15 );
DEFINE_STRING( BGl_string2723z00zz__r4_ports_6_10_1z00, BgL_bgl_string2723za700za7za7_3295za7, "call-with-output-file:Wrong number of arguments", 47 );
DEFINE_STRING( BGl_string2805z00zz__r4_ports_6_10_1z00, BgL_bgl_string2805za700za7za7_3296za7, "%open-input-pipe", 16 );
DEFINE_STRING( BGl_string2724z00zz__r4_ports_6_10_1z00, BgL_bgl_string2724za700za7za7_3297za7, "call-with-output-file", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2inputzd2fromzd2procedurezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2inputza73298za7, BGl_z62withzd2inputzd2fromzd2procedurezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2806z00zz__r4_ports_6_10_1z00, BgL_bgl_string2806za700za7za7_3299za7, "open-input-file", 15 );
DEFINE_STRING( BGl_string2725z00zz__r4_ports_6_10_1z00, BgL_bgl_string2725za700za7za7_3300za7, "&call-with-output-file", 22 );
DEFINE_STRING( BGl_string2807z00zz__r4_ports_6_10_1z00, BgL_bgl_string2807za700za7za7_3301za7, "%open-input-resource", 20 );
DEFINE_STRING( BGl_string2726z00zz__r4_ports_6_10_1z00, BgL_bgl_string2726za700za7za7_3302za7, "<@anonymous:1441>", 17 );
DEFINE_STRING( BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_bgl_string2727za700za7za7_3303za7, "output-port", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2lastzd2tokenzd2positionzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73304za7, BGl_z62inputzd2portzd2lastzd2tokenzd2positionz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2stringzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23305z00, opt_generic_entry, BGl__openzd2inputzd2stringz00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2809z00zz__r4_ports_6_10_1z00, BgL_bgl_string2809za700za7za7_3306za7, "http", 4 );
DEFINE_STRING( BGl_string2728z00zz__r4_ports_6_10_1z00, BgL_bgl_string2728za700za7za7_3307za7, "append-output-file", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2outputzd2stringzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2outputza7d3308z00, opt_generic_entry, BGl__openzd2outputzd2stringz00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2729z00zz__r4_ports_6_10_1z00, BgL_bgl_string2729za700za7za7_3309za7, "call-with-append-file:Wrong number of arguments", 47 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2outputzd2tozd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2output3310z00, BGl_z62withzd2outputzd2tozd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2closezd2hookzd2setz12zd2envzc0zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73311za7, BGl_z62inputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_selectzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__selectza700za7za7__r43312za7, opt_generic_entry, BGl__selectz00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_deletezd2filezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762deleteza7d2file3313z00, BGl_z62deletezd2filezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2inputzd2fromzd2portzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2inputza73314za7, BGl_z62withzd2inputzd2fromzd2portzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2730z00zz__r4_ports_6_10_1z00, BgL_bgl_string2730za700za7za7_3315za7, "call-with-append-file", 21 );
DEFINE_STRING( BGl_string2731z00zz__r4_ports_6_10_1z00, BgL_bgl_string2731za700za7za7_3316za7, "&call-with-append-file", 22 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2outputzd2tozd2stringzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762withza7d2output3317z00, BGl_z62withzd2outputzd2tozd2stringzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2813z00zz__r4_ports_6_10_1z00, BgL_bgl_string2813za700za7za7_3318za7, "user-agent", 10 );
DEFINE_STRING( BGl_string2732z00zz__r4_ports_6_10_1z00, BgL_bgl_string2732za700za7za7_3319za7, "<@anonymous:1443>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2uidzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2uidza7b03320za7, BGl_z62filezd2uidzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2814z00zz__r4_ports_6_10_1z00, BgL_bgl_string2814za700za7za7_3321za7, "Mozilla/5.0", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2808z00zz__r4_ports_6_10_1z00, BgL_bgl_za762parserza762za7za7__3322z00, BGl_z62parserz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 5 );
DEFINE_STRING( BGl_string2733z00zz__r4_ports_6_10_1z00, BgL_bgl_string2733za700za7za7_3323za7, "call-with-output-string:Wrong number of arguments", 49 );
DEFINE_STRING( BGl_string2734z00zz__r4_ports_6_10_1z00, BgL_bgl_string2734za700za7za7_3324za7, "call-with-output-string", 23 );
DEFINE_STRING( BGl_string2735z00zz__r4_ports_6_10_1z00, BgL_bgl_string2735za700za7za7_3325za7, "&call-with-output-string", 24 );
DEFINE_STRING( BGl_string2817z00zz__r4_ports_6_10_1z00, BgL_bgl_string2817za700za7za7_3326za7, "Connection", 10 );
DEFINE_STRING( BGl_string2736z00zz__r4_ports_6_10_1z00, BgL_bgl_string2736za700za7za7_3327za7, "input-port-reopen!", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_resetzd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762resetza7d2outpu3328z00, BGl_z62resetzd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2outputzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762currentza7d2out3329z00, BGl_z62currentzd2outputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2737z00zz__r4_ports_6_10_1z00, BgL_bgl_string2737za700za7za7_3330za7, "Cannot reopen port", 18 );
DEFINE_STRING( BGl_string2819z00zz__r4_ports_6_10_1z00, BgL_bgl_string2819za700za7za7_3331za7, "close", 5 );
DEFINE_STRING( BGl_string2738z00zz__r4_ports_6_10_1z00, BgL_bgl_string2738za700za7za7_3332za7, "&input-port-reopen!", 19 );
DEFINE_STRING( BGl_string2739z00zz__r4_ports_6_10_1z00, BgL_bgl_string2739za700za7za7_3333za7, "&input-port-clone!", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2procedurezd2portzf3zd2envz21zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2proc3334z00, BGl_z62outputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_directoryzd2ze3listzd2envze3zz__r4_ports_6_10_1z00, BgL_bgl_za762directoryza7d2za73335za7, BGl_z62directoryzd2ze3listz53zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setzd2outputzd2portzd2positionz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762setza7d2outputza73336za7, BGl_z62setzd2outputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzf3zd2envzf3zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73337za7, BGl_z62inputzd2portzf3z43zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzf3zd2envzf3zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3338z00, BGl_z62outputzd2portzf3z43zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2900z00zz__r4_ports_6_10_1z00, BgL_bgl_string2900za700za7za7_3339za7, "&closed-input-port?", 19 );
DEFINE_STRING( BGl_string2901z00zz__r4_ports_6_10_1z00, BgL_bgl_string2901za700za7za7_3340za7, "&close-input-port", 17 );
DEFINE_STRING( BGl_string2902z00zz__r4_ports_6_10_1z00, BgL_bgl_string2902za700za7za7_3341za7, "&get-output-string", 18 );
DEFINE_STRING( BGl_string2821z00zz__r4_ports_6_10_1z00, BgL_bgl_string2821za700za7za7_3342za7, "get", 3 );
DEFINE_STRING( BGl_string2740z00zz__r4_ports_6_10_1z00, BgL_bgl_string2740za700za7za7_3343za7, "with-input-from-file:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string2903z00zz__r4_ports_6_10_1z00, BgL_bgl_string2903za700za7za7_3344za7, "&close-output-port", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2typezd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762fileza7d2typeza7b3345za7, BGl_z62filezd2typezb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2904z00zz__r4_ports_6_10_1z00, BgL_bgl_string2904za700za7za7_3346za7, "&flush-output-port", 18 );
DEFINE_STRING( BGl_string2823z00zz__r4_ports_6_10_1z00, BgL_bgl_string2823za700za7za7_3347za7, "HTTP/1.1", 8 );
DEFINE_STRING( BGl_string2905z00zz__r4_ports_6_10_1z00, BgL_bgl_string2905za700za7za7_3348za7, "&reset-output-port", 18 );
DEFINE_STRING( BGl_string2824z00zz__r4_ports_6_10_1z00, BgL_bgl_string2824za700za7za7_3349za7, "&loop", 5 );
DEFINE_STRING( BGl_string2743z00zz__r4_ports_6_10_1z00, BgL_bgl_string2743za700za7za7_3350za7, "thunk", 5 );
DEFINE_STRING( BGl_string2906z00zz__r4_ports_6_10_1z00, BgL_bgl_string2906za700za7za7_3351za7, "&reset-eof", 10 );
DEFINE_STRING( BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_bgl_string2825za700za7za7_3352za7, "socket", 6 );
DEFINE_STRING( BGl_string2744z00zz__r4_ports_6_10_1z00, BgL_bgl_string2744za700za7za7_3353za7, "with-input-from-file", 20 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2inputzd2portzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762currentza7d2inp3354z00, BGl_z62currentzd2inputzd2portz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2907z00zz__r4_ports_6_10_1z00, BgL_bgl_string2907za700za7za7_3355za7, "set-input-port-position!:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string2826z00zz__r4_ports_6_10_1z00, BgL_bgl_string2826za700za7za7_3356za7, "handler1117", 11 );
DEFINE_STRING( BGl_string2745z00zz__r4_ports_6_10_1z00, BgL_bgl_string2745za700za7za7_3357za7, "&with-input-from-file", 21 );
DEFINE_STRING( BGl_string2827z00zz__r4_ports_6_10_1z00, BgL_bgl_string2827za700za7za7_3358za7, "vector", 6 );
DEFINE_STRING( BGl_string2746z00zz__r4_ports_6_10_1z00, BgL_bgl_string2746za700za7za7_3359za7, "<@anonymous:1449>", 17 );
DEFINE_STRING( BGl_string2828z00zz__r4_ports_6_10_1z00, BgL_bgl_string2828za700za7za7_3360za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string2747z00zz__r4_ports_6_10_1z00, BgL_bgl_string2747za700za7za7_3361za7, "with-input-from-string:Wrong number of arguments", 48 );
DEFINE_STRING( BGl_string2829z00zz__r4_ports_6_10_1z00, BgL_bgl_string2829za700za7za7_3362za7, "&http-redirection", 17 );
DEFINE_STRING( BGl_string2748z00zz__r4_ports_6_10_1z00, BgL_bgl_string2748za700za7za7_3363za7, "with-input-from-string", 22 );
DEFINE_STRING( BGl_string2749z00zz__r4_ports_6_10_1z00, BgL_bgl_string2749za700za7za7_3364za7, "&with-input-from-string", 23 );
DEFINE_STRING( BGl_string3000z00zz__r4_ports_6_10_1z00, BgL_bgl_string3000za700za7za7_3365za7, "symbol", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2portzd2closezd2hookzd2envz00zz__r4_ports_6_10_1z00, BgL_bgl_za762outputza7d2port3366z00, BGl_z62outputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2positionzd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73367za7, BGl_z62inputzd2portzd2positionz62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2procedurezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23368z00, opt_generic_entry, BGl__openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3002z00zz__r4_ports_6_10_1z00, BgL_bgl_string3002za700za7za7_3369za7, "lock", 4 );
DEFINE_STRING( BGl_string3004z00zz__r4_ports_6_10_1z00, BgL_bgl_string3004za700za7za7_3370za7, "tlock", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2gza7ipzd2portzd2envza7zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2inputza7d23371z00, opt_generic_entry, BGl__openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3006z00zz__r4_ports_6_10_1z00, BgL_bgl_string3006za700za7za7_3372za7, "ulock", 5 );
DEFINE_STRING( BGl_string3008z00zz__r4_ports_6_10_1z00, BgL_bgl_string3008za700za7za7_3373za7, "test", 4 );
DEFINE_STRING( BGl_string3009z00zz__r4_ports_6_10_1z00, BgL_bgl_string3009za700za7za7_3374za7, "Bad command", 11 );
DEFINE_STRING( BGl_string2910z00zz__r4_ports_6_10_1z00, BgL_bgl_string2910za700za7za7_3375za7, "useek", 5 );
DEFINE_STRING( BGl_string2830z00zz__r4_ports_6_10_1z00, BgL_bgl_string2830za700za7za7_3376za7, "<@exit:1502>~0", 14 );
DEFINE_STRING( BGl_string2912z00zz__r4_ports_6_10_1z00, BgL_bgl_string2912za700za7za7_3377za7, "pos", 3 );
DEFINE_STRING( BGl_string2831z00zz__r4_ports_6_10_1z00, BgL_bgl_string2831za700za7za7_3378za7, "parser", 6 );
DEFINE_STRING( BGl_string2750z00zz__r4_ports_6_10_1z00, BgL_bgl_string2750za700za7za7_3379za7, "with-input-from-port:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string2913z00zz__r4_ports_6_10_1z00, BgL_bgl_string2913za700za7za7_3380za7, "&set-input-port-position!", 25 );
DEFINE_STRING( BGl_string2832z00zz__r4_ports_6_10_1z00, BgL_bgl_string2832za700za7za7_3381za7, "", 0 );
DEFINE_STRING( BGl_string2751z00zz__r4_ports_6_10_1z00, BgL_bgl_string2751za700za7za7_3382za7, "with-input-from-port", 20 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2namezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73383za7, BGl_z62inputzd2portzd2namez62zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2914z00zz__r4_ports_6_10_1z00, BgL_bgl_string2914za700za7za7_3384za7, "&input-port-position", 20 );
DEFINE_STRING( BGl_string2833z00zz__r4_ports_6_10_1z00, BgL_bgl_string2833za700za7za7_3385za7, "<@anonymous:1489>", 17 );
DEFINE_STRING( BGl_string2752z00zz__r4_ports_6_10_1z00, BgL_bgl_string2752za700za7za7_3386za7, "&with-input-from-port", 21 );
DEFINE_STRING( BGl_string2915z00zz__r4_ports_6_10_1z00, BgL_bgl_string2915za700za7za7_3387za7, "&input-port-fill-barrier", 24 );
DEFINE_STRING( BGl_string2834z00zz__r4_ports_6_10_1z00, BgL_bgl_string2834za700za7za7_3388za7, "bytes=", 6 );
DEFINE_STRING( BGl_string2753z00zz__r4_ports_6_10_1z00, BgL_bgl_string2753za700za7za7_3389za7, "open-input-procedure", 20 );
DEFINE_STRING( BGl_string2916z00zz__r4_ports_6_10_1z00, BgL_bgl_string2916za700za7za7_3390za7, "&input-port-fill-barrier-set!", 29 );
DEFINE_STRING( BGl_string2835z00zz__r4_ports_6_10_1z00, BgL_bgl_string2835za700za7za7_3391za7, "-", 1 );
DEFINE_STRING( BGl_string2754z00zz__r4_ports_6_10_1z00, BgL_bgl_string2754za700za7za7_3392za7, "with-input-from-procedure:Wrong number of arguments", 51 );
DEFINE_STRING( BGl_string2917z00zz__r4_ports_6_10_1z00, BgL_bgl_string2917za700za7za7_3393za7, "&input-port-last-token-position", 31 );
DEFINE_STRING( BGl_string2755z00zz__r4_ports_6_10_1z00, BgL_bgl_string2755za700za7za7_3394za7, "with-input-from-procedure", 25 );
DEFINE_STRING( BGl_string2918z00zz__r4_ports_6_10_1z00, BgL_bgl_string2918za700za7za7_3395za7, "&output-port-name", 17 );
DEFINE_STRING( BGl_string2837z00zz__r4_ports_6_10_1z00, BgL_bgl_string2837za700za7za7_3396za7, "range", 5 );
DEFINE_STRING( BGl_string2756z00zz__r4_ports_6_10_1z00, BgL_bgl_string2756za700za7za7_3397za7, "&with-input-from-procedure", 26 );
DEFINE_STRING( BGl_string2919z00zz__r4_ports_6_10_1z00, BgL_bgl_string2919za700za7za7_3398za7, "&output-port-name-set!", 22 );
DEFINE_STRING( BGl_string2838z00zz__r4_ports_6_10_1z00, BgL_bgl_string2838za700za7za7_3399za7, "<@anonymous:1487>", 17 );
DEFINE_STRING( BGl_string2757z00zz__r4_ports_6_10_1z00, BgL_bgl_string2757za700za7za7_3400za7, "with-output-to-file:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string2758z00zz__r4_ports_6_10_1z00, BgL_bgl_string2758za700za7za7_3401za7, "with-output-to-file", 19 );
DEFINE_STRING( BGl_string2759z00zz__r4_ports_6_10_1z00, BgL_bgl_string2759za700za7za7_3402za7, "&with-output-to-file", 20 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2protocolzd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73403za7, BGl_z62inputzd2portzd2protocolzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2outputzd2filezd2envzd2zz__r4_ports_6_10_1z00, BgL_bgl__openza7d2outputza7d3404z00, opt_generic_entry, BGl__openzd2outputzd2filez00zz__r4_ports_6_10_1z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2portzd2bufferzd2setz12zd2envz12zz__r4_ports_6_10_1z00, BgL_bgl_za762inputza7d2portza73405za7, BGl_z62inputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2818z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2706z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_vector2968z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2820z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2822z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2742z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2909z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol3001z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol3003z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol3005z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol3007z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2911z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2839z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2810z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2811z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2845z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2847z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2815z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2849z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2851z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2853z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2741z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2857z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2908z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2982z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2984z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2986z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2987z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2782z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2864z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2867z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2871z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2873z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2844z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2881z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2883z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2885z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2888z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2989z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2994z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2996z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2812z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2816z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_list2981z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2707z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2709z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_keyword2836z00zz__r4_ports_6_10_1z00) );
ADD_ROOT( (void *)(&BGl_symbol2711z00zz__r4_ports_6_10_1z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long BgL_checksumz00_4648, char * BgL_fromz00_4649)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_ports_6_10_1z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_ports_6_10_1z00(); 
BGl_cnstzd2initzd2zz__r4_ports_6_10_1z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_ports_6_10_1z00(); 
return 
BGl_toplevelzd2initzd2zz__r4_ports_6_10_1z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
BGl_symbol2707z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2708z00zz__r4_ports_6_10_1z00); 
BGl_symbol2709z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2710z00zz__r4_ports_6_10_1z00); 
BGl_symbol2711z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2712z00zz__r4_ports_6_10_1z00); 
BGl_list2706z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2707z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2709z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2709z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2711z00zz__r4_ports_6_10_1z00, BNIL)))); 
BGl_symbol2742z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2743z00zz__r4_ports_6_10_1z00); 
BGl_list2741z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2707z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2742z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2742z00zz__r4_ports_6_10_1z00, BNIL))); 
BGl_symbol2782z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2781z00zz__r4_ports_6_10_1z00); 
BGl_keyword2812z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2813z00zz__r4_ports_6_10_1z00); 
BGl_list2811z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_keyword2812z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_string2814z00zz__r4_ports_6_10_1z00, BNIL)); 
BGl_keyword2816z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2817z00zz__r4_ports_6_10_1z00); 
BGl_symbol2818z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2819z00zz__r4_ports_6_10_1z00); 
BGl_list2815z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_keyword2816z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2818z00zz__r4_ports_6_10_1z00, BNIL)); 
BGl_list2810z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_list2811z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_list2815z00zz__r4_ports_6_10_1z00, BNIL)); 
BGl_symbol2820z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2821z00zz__r4_ports_6_10_1z00); 
BGl_symbol2822z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2809z00zz__r4_ports_6_10_1z00); 
BGl_keyword2836z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2837z00zz__r4_ports_6_10_1z00); 
BGl_symbol2839z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2806z00zz__r4_ports_6_10_1z00); 
BGl_symbol2845z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2846z00zz__r4_ports_6_10_1z00); 
BGl_symbol2847z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2848z00zz__r4_ports_6_10_1z00); 
BGl_symbol2849z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2850z00zz__r4_ports_6_10_1z00); 
BGl_symbol2851z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2852z00zz__r4_ports_6_10_1z00); 
BGl_list2844z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2707z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2845z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2845z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2849z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2851z00zz__r4_ports_6_10_1z00, BNIL)))))); 
BGl_symbol2853z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2854z00zz__r4_ports_6_10_1z00); 
BGl_symbol2857z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2858z00zz__r4_ports_6_10_1z00); 
BGl_symbol2864z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2865z00zz__r4_ports_6_10_1z00); 
BGl_symbol2867z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2868z00zz__r4_ports_6_10_1z00); 
BGl_symbol2871z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2753z00zz__r4_ports_6_10_1z00); 
BGl_symbol2873z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2874z00zz__r4_ports_6_10_1z00); 
BGl_symbol2881z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2722z00zz__r4_ports_6_10_1z00); 
BGl_symbol2883z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2728z00zz__r4_ports_6_10_1z00); 
BGl_symbol2885z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2886z00zz__r4_ports_6_10_1z00); 
BGl_symbol2888z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2889z00zz__r4_ports_6_10_1z00); 
BGl_symbol2909z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2910z00zz__r4_ports_6_10_1z00); 
BGl_symbol2911z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2912z00zz__r4_ports_6_10_1z00); 
BGl_list2908z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2707z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2909z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2909z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2711z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_symbol2911z00zz__r4_ports_6_10_1z00, BNIL))))); 
BGl_vector2968z00zz__r4_ports_6_10_1z00 = 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BNIL); 
BGl_keyword2982z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2983z00zz__r4_ports_6_10_1z00); 
BGl_keyword2984z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2985z00zz__r4_ports_6_10_1z00); 
BGl_keyword2986z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2852z00zz__r4_ports_6_10_1z00); 
BGl_keyword2987z00zz__r4_ports_6_10_1z00 = 
bstring_to_keyword(BGl_string2988z00zz__r4_ports_6_10_1z00); 
BGl_list2981z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BGl_keyword2982z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_keyword2984z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_keyword2986z00zz__r4_ports_6_10_1z00, 
MAKE_YOUNG_PAIR(BGl_keyword2987z00zz__r4_ports_6_10_1z00, BNIL)))); 
BGl_symbol2989z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2990z00zz__r4_ports_6_10_1z00); 
BGl_symbol2994z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2995z00zz__r4_ports_6_10_1z00); 
BGl_symbol2996z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string2997z00zz__r4_ports_6_10_1z00); 
BGl_symbol3001z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string3002z00zz__r4_ports_6_10_1z00); 
BGl_symbol3003z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string3004z00zz__r4_ports_6_10_1z00); 
BGl_symbol3005z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string3006z00zz__r4_ports_6_10_1z00); 
return ( 
BGl_symbol3007z00zz__r4_ports_6_10_1z00 = 
bstring_to_symbol(BGl_string3008z00zz__r4_ports_6_10_1z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00 = 
bgl_make_mutex(BGl_string2689z00zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 924 */
 obj_t BgL_arg1410z00_1402; obj_t BgL_arg1411z00_1403;
BgL_arg1410z00_1402 = 
MAKE_YOUNG_PAIR(BGl_string2690z00zz__r4_ports_6_10_1z00, BGl_z52openzd2inputzd2filezd2envz80zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 925 */
 obj_t BgL_arg1412z00_1404; obj_t BgL_arg1413z00_1405;
BgL_arg1412z00_1404 = 
MAKE_YOUNG_PAIR(BGl_string2691z00zz__r4_ports_6_10_1z00, BGl_proc2692z00zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 926 */
 obj_t BgL_arg1416z00_1415; obj_t BgL_arg1417z00_1416;
BgL_arg1416z00_1415 = 
MAKE_YOUNG_PAIR(BGl_string2693z00zz__r4_ports_6_10_1z00, BGl_z52openzd2inputzd2pipezd2envz80zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 927 */
 obj_t BgL_arg1418z00_1417; obj_t BgL_arg1419z00_1418;
BgL_arg1418z00_1417 = 
MAKE_YOUNG_PAIR(BGl_string2694z00zz__r4_ports_6_10_1z00, BGl_z52openzd2inputzd2pipezd2envz80zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 928 */
 obj_t BgL_arg1420z00_1419; obj_t BgL_arg1421z00_1420;
BgL_arg1420z00_1419 = 
MAKE_YOUNG_PAIR(BGl_string2695z00zz__r4_ports_6_10_1z00, BGl_z52openzd2inputzd2httpzd2socketzd2envz52zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 929 */
 obj_t BgL_arg1422z00_1421; obj_t BgL_arg1423z00_1422;
BgL_arg1422z00_1421 = 
MAKE_YOUNG_PAIR(BGl_string2696z00zz__r4_ports_6_10_1z00, BGl_openzd2inputzd2gza7ipzd2filezd2envza7zz__gunza7ipza7); 
{ /* Ieee/port.scm 930 */
 obj_t BgL_arg1424z00_1423; obj_t BgL_arg1425z00_1424;
BgL_arg1424z00_1423 = 
MAKE_YOUNG_PAIR(BGl_string2697z00zz__r4_ports_6_10_1z00, BGl_openzd2inputzd2za7libzd2filezd2envza7zz__gunza7ipza7); 
{ /* Ieee/port.scm 931 */
 obj_t BgL_arg1426z00_1425; obj_t BgL_arg1427z00_1426;
BgL_arg1426z00_1425 = 
MAKE_YOUNG_PAIR(BGl_string2698z00zz__r4_ports_6_10_1z00, BGl_openzd2inputzd2inflatezd2filezd2envz00zz__gunza7ipza7); 
{ /* Ieee/port.scm 932 */
 obj_t BgL_arg1428z00_1427; obj_t BgL_arg1429z00_1428;
BgL_arg1428z00_1427 = 
MAKE_YOUNG_PAIR(BGl_string2699z00zz__r4_ports_6_10_1z00, BGl_z52openzd2inputzd2resourcezd2envz80zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 933 */
 obj_t BgL_arg1430z00_1429; obj_t BgL_arg1431z00_1430;
BgL_arg1430z00_1429 = 
MAKE_YOUNG_PAIR(BGl_string2700z00zz__r4_ports_6_10_1z00, BGl_openzd2inputzd2ftpzd2filezd2envz00zz__ftpz00); 
{ /* Ieee/port.scm 934 */
 obj_t BgL_arg1434z00_1431;
BgL_arg1434z00_1431 = 
MAKE_YOUNG_PAIR(BGl_string2701z00zz__r4_ports_6_10_1z00, BGl_z52openzd2inputzd2descriptorzd2envz80zz__r4_ports_6_10_1z00); 
BgL_arg1431z00_1430 = 
MAKE_YOUNG_PAIR(BgL_arg1434z00_1431, BNIL); } 
BgL_arg1429z00_1428 = 
MAKE_YOUNG_PAIR(BgL_arg1430z00_1429, BgL_arg1431z00_1430); } 
BgL_arg1427z00_1426 = 
MAKE_YOUNG_PAIR(BgL_arg1428z00_1427, BgL_arg1429z00_1428); } 
BgL_arg1425z00_1424 = 
MAKE_YOUNG_PAIR(BgL_arg1426z00_1425, BgL_arg1427z00_1426); } 
BgL_arg1423z00_1422 = 
MAKE_YOUNG_PAIR(BgL_arg1424z00_1423, BgL_arg1425z00_1424); } 
BgL_arg1421z00_1420 = 
MAKE_YOUNG_PAIR(BgL_arg1422z00_1421, BgL_arg1423z00_1422); } 
BgL_arg1419z00_1418 = 
MAKE_YOUNG_PAIR(BgL_arg1420z00_1419, BgL_arg1421z00_1420); } 
BgL_arg1417z00_1416 = 
MAKE_YOUNG_PAIR(BgL_arg1418z00_1417, BgL_arg1419z00_1418); } 
BgL_arg1413z00_1405 = 
MAKE_YOUNG_PAIR(BgL_arg1416z00_1415, BgL_arg1417z00_1416); } 
BgL_arg1411z00_1403 = 
MAKE_YOUNG_PAIR(BgL_arg1412z00_1404, BgL_arg1413z00_1405); } 
return ( 
BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BgL_arg1410z00_1402, BgL_arg1411z00_1403), BUNSPEC) ;} } 

}



/* &<@anonymous:1415> */
obj_t BGl_z62zc3z04anonymousza31415ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3446, obj_t BgL_sz00_3447, obj_t BgL_pz00_3448, obj_t BgL_tmtz00_3449)
{
{ /* Ieee/port.scm 925 */
{ /* Ieee/port.scm 469 */
 long BgL_endz00_4523;
{ /* Ieee/port.scm 469 */
 obj_t BgL_stringz00_4524;
if(
STRINGP(BgL_sz00_3447))
{ /* Ieee/port.scm 469 */
BgL_stringz00_4524 = BgL_sz00_3447; }  else 
{ 
 obj_t BgL_auxz00_4751;
BgL_auxz00_4751 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(20820L), BGl_string2703z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_sz00_3447); 
FAILURE(BgL_auxz00_4751,BFALSE,BFALSE);} 
BgL_endz00_4523 = 
STRING_LENGTH(BgL_stringz00_4524); } 
{ /* Ieee/port.scm 469 */

{ /* Ieee/port.scm 469 */
 obj_t BgL_auxz00_4756;
if(
STRINGP(BgL_sz00_3447))
{ /* Ieee/port.scm 925 */
BgL_auxz00_4756 = BgL_sz00_3447
; }  else 
{ 
 obj_t BgL_auxz00_4759;
BgL_auxz00_4759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(40760L), BGl_string2703z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_sz00_3447); 
FAILURE(BgL_auxz00_4759,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_auxz00_4756, 
BINT(0L), 
BINT(BgL_endz00_4523));} } } } 

}



/* call-with-input-file */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_3, obj_t BgL_procz00_4)
{
{ /* Ieee/port.scm 569 */
{ /* Ieee/port.scm 570 */
 obj_t BgL_portz00_1440;
{ /* Ieee/port.scm 466 */

BgL_portz00_1440 = 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_stringz00_3, BTRUE, 
BINT(5000000L)); } 
if(
INPUT_PORTP(BgL_portz00_1440))
{ /* Ieee/port.scm 572 */
 obj_t BgL_exitd1047z00_1442;
BgL_exitd1047z00_1442 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 574 */
 obj_t BgL_zc3z04anonymousza31439ze3z87_3486;
BgL_zc3z04anonymousza31439ze3z87_3486 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31439ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31439ze3z87_3486, 
(int)(0L), BgL_portz00_1440); 
{ /* Ieee/port.scm 572 */
 obj_t BgL_arg2070z00_2552;
{ /* Ieee/port.scm 572 */
 obj_t BgL_arg2072z00_2553;
BgL_arg2072z00_2553 = 
BGL_EXITD_PROTECT(BgL_exitd1047z00_1442); 
BgL_arg2070z00_2552 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31439ze3z87_3486, BgL_arg2072z00_2553); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1047z00_1442, BgL_arg2070z00_2552); BUNSPEC; } 
{ /* Ieee/port.scm 573 */
 obj_t BgL_tmp1049z00_1444;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_4, 1))
{ /* Ieee/port.scm 573 */
BgL_tmp1049z00_1444 = 
BGL_PROCEDURE_CALL1(BgL_procz00_4, BgL_portz00_1440); }  else 
{ /* Ieee/port.scm 573 */
FAILURE(BGl_string2705z00zz__r4_ports_6_10_1z00,BGl_list2706z00zz__r4_ports_6_10_1z00,BgL_procz00_4);} 
{ /* Ieee/port.scm 572 */
 bool_t BgL_test3411z00_4786;
{ /* Ieee/port.scm 572 */
 obj_t BgL_arg2069z00_2555;
BgL_arg2069z00_2555 = 
BGL_EXITD_PROTECT(BgL_exitd1047z00_1442); 
BgL_test3411z00_4786 = 
PAIRP(BgL_arg2069z00_2555); } 
if(BgL_test3411z00_4786)
{ /* Ieee/port.scm 572 */
 obj_t BgL_arg2067z00_2556;
{ /* Ieee/port.scm 572 */
 obj_t BgL_arg2068z00_2557;
BgL_arg2068z00_2557 = 
BGL_EXITD_PROTECT(BgL_exitd1047z00_1442); 
{ /* Ieee/port.scm 572 */
 obj_t BgL_pairz00_2558;
if(
PAIRP(BgL_arg2068z00_2557))
{ /* Ieee/port.scm 572 */
BgL_pairz00_2558 = BgL_arg2068z00_2557; }  else 
{ 
 obj_t BgL_auxz00_4792;
BgL_auxz00_4792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(25917L), BGl_string2713z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2557); 
FAILURE(BgL_auxz00_4792,BFALSE,BFALSE);} 
BgL_arg2067z00_2556 = 
CDR(BgL_pairz00_2558); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1047z00_1442, BgL_arg2067z00_2556); BUNSPEC; }  else 
{ /* Ieee/port.scm 572 */BFALSE; } } 
bgl_close_input_port(BgL_portz00_1440); 
return BgL_tmp1049z00_1444;} } }  else 
{ /* Ieee/port.scm 571 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2713z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_3);} } } 

}



/* &call-with-input-file */
obj_t BGl_z62callzd2withzd2inputzd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3487, obj_t BgL_stringz00_3488, obj_t BgL_procz00_3489)
{
{ /* Ieee/port.scm 569 */
{ /* Ieee/port.scm 570 */
 obj_t BgL_auxz00_4807; obj_t BgL_auxz00_4800;
if(
PROCEDUREP(BgL_procz00_3489))
{ /* Ieee/port.scm 570 */
BgL_auxz00_4807 = BgL_procz00_3489
; }  else 
{ 
 obj_t BgL_auxz00_4810;
BgL_auxz00_4810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(25846L), BGl_string2716z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3489); 
FAILURE(BgL_auxz00_4810,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3488))
{ /* Ieee/port.scm 570 */
BgL_auxz00_4800 = BgL_stringz00_3488
; }  else 
{ 
 obj_t BgL_auxz00_4803;
BgL_auxz00_4803 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(25846L), BGl_string2716z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3488); 
FAILURE(BgL_auxz00_4803,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_4800, BgL_auxz00_4807);} } 

}



/* &<@anonymous:1439> */
obj_t BGl_z62zc3z04anonymousza31439ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3490)
{
{ /* Ieee/port.scm 572 */
{ /* Ieee/port.scm 574 */
 obj_t BgL_portz00_3491;
BgL_portz00_3491 = 
PROCEDURE_REF(BgL_envz00_3490, 
(int)(0L)); 
{ /* Ieee/port.scm 574 */
 obj_t BgL_portz00_4525;
if(
INPUT_PORTP(BgL_portz00_3491))
{ /* Ieee/port.scm 574 */
BgL_portz00_4525 = BgL_portz00_3491; }  else 
{ 
 obj_t BgL_auxz00_4819;
BgL_auxz00_4819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(25975L), BGl_string2718z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3491); 
FAILURE(BgL_auxz00_4819,BFALSE,BFALSE);} 
return 
bgl_close_input_port(BgL_portz00_4525);} } } 

}



/* call-with-input-string */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_5, obj_t BgL_procz00_6)
{
{ /* Ieee/port.scm 581 */
{ /* Ieee/port.scm 582 */
 obj_t BgL_portz00_2560;
{ /* Ieee/port.scm 469 */
 long BgL_endz00_2563;
BgL_endz00_2563 = 
STRING_LENGTH(BgL_stringz00_5); 
{ /* Ieee/port.scm 469 */

BgL_portz00_2560 = 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_stringz00_5, 
BINT(0L), 
BINT(BgL_endz00_2563)); } } 
{ /* Ieee/port.scm 584 */
 obj_t BgL_resz00_2564;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_6, 1))
{ /* Ieee/port.scm 584 */
BgL_resz00_2564 = 
BGL_PROCEDURE_CALL1(BgL_procz00_6, BgL_portz00_2560); }  else 
{ /* Ieee/port.scm 584 */
FAILURE(BGl_string2720z00zz__r4_ports_6_10_1z00,BGl_list2706z00zz__r4_ports_6_10_1z00,BgL_procz00_6);} 
bgl_close_input_port(BgL_portz00_2560); 
return BgL_resz00_2564;} } } 

}



/* &call-with-input-string */
obj_t BGl_z62callzd2withzd2inputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3492, obj_t BgL_stringz00_3493, obj_t BgL_procz00_3494)
{
{ /* Ieee/port.scm 581 */
{ /* Ieee/port.scm 582 */
 obj_t BgL_auxz00_4843; obj_t BgL_auxz00_4836;
if(
PROCEDUREP(BgL_procz00_3494))
{ /* Ieee/port.scm 582 */
BgL_auxz00_4843 = BgL_procz00_3494
; }  else 
{ 
 obj_t BgL_auxz00_4846;
BgL_auxz00_4846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(26348L), BGl_string2721z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3494); 
FAILURE(BgL_auxz00_4846,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3493))
{ /* Ieee/port.scm 582 */
BgL_auxz00_4836 = BgL_stringz00_3493
; }  else 
{ 
 obj_t BgL_auxz00_4839;
BgL_auxz00_4839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(26348L), BGl_string2721z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3493); 
FAILURE(BgL_auxz00_4839,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_4836, BgL_auxz00_4843);} } 

}



/* call-with-output-file */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_7, obj_t BgL_procz00_8)
{
{ /* Ieee/port.scm 591 */
{ /* Ieee/port.scm 592 */
 obj_t BgL_portz00_1455;
{ /* Ieee/port.scm 483 */

BgL_portz00_1455 = 
bgl_open_output_file(BgL_stringz00_7, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz))); } 
if(
OUTPUT_PORTP(BgL_portz00_1455))
{ /* Ieee/port.scm 594 */
 obj_t BgL_exitd1051z00_1457;
BgL_exitd1051z00_1457 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 596 */
 obj_t BgL_zc3z04anonymousza31441ze3z87_3495;
BgL_zc3z04anonymousza31441ze3z87_3495 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31441ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31441ze3z87_3495, 
(int)(0L), BgL_portz00_1455); 
{ /* Ieee/port.scm 594 */
 obj_t BgL_arg2070z00_2570;
{ /* Ieee/port.scm 594 */
 obj_t BgL_arg2072z00_2571;
BgL_arg2072z00_2571 = 
BGL_EXITD_PROTECT(BgL_exitd1051z00_1457); 
BgL_arg2070z00_2570 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31441ze3z87_3495, BgL_arg2072z00_2571); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1051z00_1457, BgL_arg2070z00_2570); BUNSPEC; } 
{ /* Ieee/port.scm 595 */
 obj_t BgL_tmp1053z00_1459;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_8, 1))
{ /* Ieee/port.scm 595 */
BgL_tmp1053z00_1459 = 
BGL_PROCEDURE_CALL1(BgL_procz00_8, BgL_portz00_1455); }  else 
{ /* Ieee/port.scm 595 */
FAILURE(BGl_string2723z00zz__r4_ports_6_10_1z00,BGl_list2706z00zz__r4_ports_6_10_1z00,BgL_procz00_8);} 
{ /* Ieee/port.scm 594 */
 bool_t BgL_test3421z00_4872;
{ /* Ieee/port.scm 594 */
 obj_t BgL_arg2069z00_2573;
BgL_arg2069z00_2573 = 
BGL_EXITD_PROTECT(BgL_exitd1051z00_1457); 
BgL_test3421z00_4872 = 
PAIRP(BgL_arg2069z00_2573); } 
if(BgL_test3421z00_4872)
{ /* Ieee/port.scm 594 */
 obj_t BgL_arg2067z00_2574;
{ /* Ieee/port.scm 594 */
 obj_t BgL_arg2068z00_2575;
BgL_arg2068z00_2575 = 
BGL_EXITD_PROTECT(BgL_exitd1051z00_1457); 
{ /* Ieee/port.scm 594 */
 obj_t BgL_pairz00_2576;
if(
PAIRP(BgL_arg2068z00_2575))
{ /* Ieee/port.scm 594 */
BgL_pairz00_2576 = BgL_arg2068z00_2575; }  else 
{ 
 obj_t BgL_auxz00_4878;
BgL_auxz00_4878 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(26871L), BGl_string2724z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2575); 
FAILURE(BgL_auxz00_4878,BFALSE,BFALSE);} 
BgL_arg2067z00_2574 = 
CDR(BgL_pairz00_2576); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1051z00_1457, BgL_arg2067z00_2574); BUNSPEC; }  else 
{ /* Ieee/port.scm 594 */BFALSE; } } 
bgl_close_output_port(BgL_portz00_1455); 
return BgL_tmp1053z00_1459;} } }  else 
{ /* Ieee/port.scm 593 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2724z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_7);} } } 

}



/* &call-with-output-file */
obj_t BGl_z62callzd2withzd2outputzd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3496, obj_t BgL_stringz00_3497, obj_t BgL_procz00_3498)
{
{ /* Ieee/port.scm 591 */
{ /* Ieee/port.scm 592 */
 obj_t BgL_auxz00_4893; obj_t BgL_auxz00_4886;
if(
PROCEDUREP(BgL_procz00_3498))
{ /* Ieee/port.scm 592 */
BgL_auxz00_4893 = BgL_procz00_3498
; }  else 
{ 
 obj_t BgL_auxz00_4896;
BgL_auxz00_4896 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(26798L), BGl_string2725z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3498); 
FAILURE(BgL_auxz00_4896,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3497))
{ /* Ieee/port.scm 592 */
BgL_auxz00_4886 = BgL_stringz00_3497
; }  else 
{ 
 obj_t BgL_auxz00_4889;
BgL_auxz00_4889 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(26798L), BGl_string2725z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3497); 
FAILURE(BgL_auxz00_4889,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_4886, BgL_auxz00_4893);} } 

}



/* &<@anonymous:1441> */
obj_t BGl_z62zc3z04anonymousza31441ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3499)
{
{ /* Ieee/port.scm 594 */
{ /* Ieee/port.scm 596 */
 obj_t BgL_portz00_3500;
BgL_portz00_3500 = 
PROCEDURE_REF(BgL_envz00_3499, 
(int)(0L)); 
{ /* Ieee/port.scm 596 */
 obj_t BgL_portz00_4526;
if(
OUTPUT_PORTP(BgL_portz00_3500))
{ /* Ieee/port.scm 596 */
BgL_portz00_4526 = BgL_portz00_3500; }  else 
{ 
 obj_t BgL_auxz00_4905;
BgL_auxz00_4905 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(26930L), BGl_string2726z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3500); 
FAILURE(BgL_auxz00_4905,BFALSE,BFALSE);} 
return 
bgl_close_output_port(BgL_portz00_4526);} } } 

}



/* call-with-append-file */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2appendzd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_9, obj_t BgL_procz00_10)
{
{ /* Ieee/port.scm 603 */
{ /* Ieee/port.scm 604 */
 obj_t BgL_portz00_1464;
{ /* Ieee/port.scm 484 */

BgL_portz00_1464 = 
bgl_append_output_file(BgL_stringz00_9, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2728z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz))); } 
if(
OUTPUT_PORTP(BgL_portz00_1464))
{ /* Ieee/port.scm 606 */
 obj_t BgL_exitd1055z00_1466;
BgL_exitd1055z00_1466 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 608 */
 obj_t BgL_zc3z04anonymousza31443ze3z87_3501;
BgL_zc3z04anonymousza31443ze3z87_3501 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31443ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31443ze3z87_3501, 
(int)(0L), BgL_portz00_1464); 
{ /* Ieee/port.scm 606 */
 obj_t BgL_arg2070z00_2581;
{ /* Ieee/port.scm 606 */
 obj_t BgL_arg2072z00_2582;
BgL_arg2072z00_2582 = 
BGL_EXITD_PROTECT(BgL_exitd1055z00_1466); 
BgL_arg2070z00_2581 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31443ze3z87_3501, BgL_arg2072z00_2582); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_1466, BgL_arg2070z00_2581); BUNSPEC; } 
{ /* Ieee/port.scm 607 */
 obj_t BgL_tmp1057z00_1468;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_10, 1))
{ /* Ieee/port.scm 607 */
BgL_tmp1057z00_1468 = 
BGL_PROCEDURE_CALL1(BgL_procz00_10, BgL_portz00_1464); }  else 
{ /* Ieee/port.scm 607 */
FAILURE(BGl_string2729z00zz__r4_ports_6_10_1z00,BGl_list2706z00zz__r4_ports_6_10_1z00,BgL_procz00_10);} 
{ /* Ieee/port.scm 606 */
 bool_t BgL_test3428z00_4931;
{ /* Ieee/port.scm 606 */
 obj_t BgL_arg2069z00_2584;
BgL_arg2069z00_2584 = 
BGL_EXITD_PROTECT(BgL_exitd1055z00_1466); 
BgL_test3428z00_4931 = 
PAIRP(BgL_arg2069z00_2584); } 
if(BgL_test3428z00_4931)
{ /* Ieee/port.scm 606 */
 obj_t BgL_arg2067z00_2585;
{ /* Ieee/port.scm 606 */
 obj_t BgL_arg2068z00_2586;
BgL_arg2068z00_2586 = 
BGL_EXITD_PROTECT(BgL_exitd1055z00_1466); 
{ /* Ieee/port.scm 606 */
 obj_t BgL_pairz00_2587;
if(
PAIRP(BgL_arg2068z00_2586))
{ /* Ieee/port.scm 606 */
BgL_pairz00_2587 = BgL_arg2068z00_2586; }  else 
{ 
 obj_t BgL_auxz00_4937;
BgL_auxz00_4937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(27379L), BGl_string2730z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2586); 
FAILURE(BgL_auxz00_4937,BFALSE,BFALSE);} 
BgL_arg2067z00_2585 = 
CDR(BgL_pairz00_2587); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_1466, BgL_arg2067z00_2585); BUNSPEC; }  else 
{ /* Ieee/port.scm 606 */BFALSE; } } 
bgl_close_output_port(BgL_portz00_1464); 
return BgL_tmp1057z00_1468;} } }  else 
{ /* Ieee/port.scm 605 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2730z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_9);} } } 

}



/* &call-with-append-file */
obj_t BGl_z62callzd2withzd2appendzd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3502, obj_t BgL_stringz00_3503, obj_t BgL_procz00_3504)
{
{ /* Ieee/port.scm 603 */
{ /* Ieee/port.scm 604 */
 obj_t BgL_auxz00_4952; obj_t BgL_auxz00_4945;
if(
PROCEDUREP(BgL_procz00_3504))
{ /* Ieee/port.scm 604 */
BgL_auxz00_4952 = BgL_procz00_3504
; }  else 
{ 
 obj_t BgL_auxz00_4955;
BgL_auxz00_4955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(27304L), BGl_string2731z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3504); 
FAILURE(BgL_auxz00_4955,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3503))
{ /* Ieee/port.scm 604 */
BgL_auxz00_4945 = BgL_stringz00_3503
; }  else 
{ 
 obj_t BgL_auxz00_4948;
BgL_auxz00_4948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(27304L), BGl_string2731z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3503); 
FAILURE(BgL_auxz00_4948,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2appendzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_4945, BgL_auxz00_4952);} } 

}



/* &<@anonymous:1443> */
obj_t BGl_z62zc3z04anonymousza31443ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3505)
{
{ /* Ieee/port.scm 606 */
{ /* Ieee/port.scm 608 */
 obj_t BgL_portz00_3506;
BgL_portz00_3506 = 
PROCEDURE_REF(BgL_envz00_3505, 
(int)(0L)); 
{ /* Ieee/port.scm 608 */
 obj_t BgL_portz00_4527;
if(
OUTPUT_PORTP(BgL_portz00_3506))
{ /* Ieee/port.scm 608 */
BgL_portz00_4527 = BgL_portz00_3506; }  else 
{ 
 obj_t BgL_auxz00_4964;
BgL_auxz00_4964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(27438L), BGl_string2732z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3506); 
FAILURE(BgL_auxz00_4964,BFALSE,BFALSE);} 
return 
bgl_close_output_port(BgL_portz00_4527);} } } 

}



/* call-with-output-string */
BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t BgL_procz00_11)
{
{ /* Ieee/port.scm 615 */
{ /* Ieee/port.scm 616 */
 obj_t BgL_portz00_2589;
{ /* Ieee/port.scm 616 */

{ /* Ieee/port.scm 485 */

BgL_portz00_2589 = 
bgl_open_output_string(
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(128L))); } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_11, 1))
{ /* Ieee/port.scm 617 */
BGL_PROCEDURE_CALL1(BgL_procz00_11, BgL_portz00_2589); }  else 
{ /* Ieee/port.scm 617 */
FAILURE(BGl_string2733z00zz__r4_ports_6_10_1z00,BGl_list2706z00zz__r4_ports_6_10_1z00,BgL_procz00_11);} 
{ /* Ieee/port.scm 1285 */
 obj_t BgL_aux2168z00_3943;
BgL_aux2168z00_3943 = 
bgl_close_output_port(BgL_portz00_2589); 
if(
STRINGP(BgL_aux2168z00_3943))
{ /* Ieee/port.scm 1285 */
return BgL_aux2168z00_3943;}  else 
{ 
 obj_t BgL_auxz00_4982;
BgL_auxz00_4982 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(57509L), BGl_string2734z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2168z00_3943); 
FAILURE(BgL_auxz00_4982,BFALSE,BFALSE);} } } } 

}



/* &call-with-output-string */
obj_t BGl_z62callzd2withzd2outputzd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3507, obj_t BgL_procz00_3508)
{
{ /* Ieee/port.scm 615 */
{ /* Ieee/port.scm 616 */
 obj_t BgL_auxz00_4986;
if(
PROCEDUREP(BgL_procz00_3508))
{ /* Ieee/port.scm 616 */
BgL_auxz00_4986 = BgL_procz00_3508
; }  else 
{ 
 obj_t BgL_auxz00_4989;
BgL_auxz00_4989 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(27807L), BGl_string2735z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3508); 
FAILURE(BgL_auxz00_4989,BFALSE,BFALSE);} 
return 
BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_4986);} } 

}



/* input-port? */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t BgL_objz00_12)
{
{ /* Ieee/port.scm 623 */
return 
BBOOL(
INPUT_PORTP(BgL_objz00_12));} 

}



/* &input-port? */
obj_t BGl_z62inputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3509, obj_t BgL_objz00_3510)
{
{ /* Ieee/port.scm 623 */
return 
BGl_inputzd2portzf3z21zz__r4_ports_6_10_1z00(BgL_objz00_3510);} 

}



/* input-string-port? */
BGL_EXPORTED_DEF obj_t BGl_inputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_13)
{
{ /* Ieee/port.scm 629 */
return 
BBOOL(
INPUT_STRING_PORTP(BgL_objz00_13));} 

}



/* &input-string-port? */
obj_t BGl_z62inputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3511, obj_t BgL_objz00_3512)
{
{ /* Ieee/port.scm 629 */
return 
BGl_inputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3512);} 

}



/* input-procedure-port? */
BGL_EXPORTED_DEF obj_t BGl_inputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_14)
{
{ /* Ieee/port.scm 635 */
return 
BBOOL(
INPUT_PROCEDURE_PORTP(BgL_objz00_14));} 

}



/* &input-procedure-port? */
obj_t BGl_z62inputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3513, obj_t BgL_objz00_3514)
{
{ /* Ieee/port.scm 635 */
return 
BGl_inputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3514);} 

}



/* input-gzip-port? */
BGL_EXPORTED_DEF obj_t BGl_inputzd2gza7ipzd2portzf3z54zz__r4_ports_6_10_1z00(obj_t BgL_objz00_15)
{
{ /* Ieee/port.scm 641 */
return 
BBOOL(
INPUT_GZIP_PORTP(BgL_objz00_15));} 

}



/* &input-gzip-port? */
obj_t BGl_z62inputzd2gza7ipzd2portzf3z36zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3515, obj_t BgL_objz00_3516)
{
{ /* Ieee/port.scm 641 */
return 
BGl_inputzd2gza7ipzd2portzf3z54zz__r4_ports_6_10_1z00(BgL_objz00_3516);} 

}



/* input-mmap-port? */
BGL_EXPORTED_DEF obj_t BGl_inputzd2mmapzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_16)
{
{ /* Ieee/port.scm 647 */
return 
BBOOL(
INPUT_MMAP_PORTP(BgL_objz00_16));} 

}



/* &input-mmap-port? */
obj_t BGl_z62inputzd2mmapzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3517, obj_t BgL_objz00_3518)
{
{ /* Ieee/port.scm 647 */
return 
BGl_inputzd2mmapzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3518);} 

}



/* output-port? */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzf3z21zz__r4_ports_6_10_1z00(obj_t BgL_objz00_17)
{
{ /* Ieee/port.scm 653 */
return 
BBOOL(
OUTPUT_PORTP(BgL_objz00_17));} 

}



/* &output-port? */
obj_t BGl_z62outputzd2portzf3z43zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3519, obj_t BgL_objz00_3520)
{
{ /* Ieee/port.scm 653 */
return 
BGl_outputzd2portzf3z21zz__r4_ports_6_10_1z00(BgL_objz00_3520);} 

}



/* output-string-port? */
BGL_EXPORTED_DEF obj_t BGl_outputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_18)
{
{ /* Ieee/port.scm 659 */
return 
BBOOL(
BGL_OUTPUT_STRING_PORTP(BgL_objz00_18));} 

}



/* &output-string-port? */
obj_t BGl_z62outputzd2stringzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3521, obj_t BgL_objz00_3522)
{
{ /* Ieee/port.scm 659 */
return 
BGl_outputzd2stringzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3522);} 

}



/* output-procedure-port? */
BGL_EXPORTED_DEF obj_t BGl_outputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_19)
{
{ /* Ieee/port.scm 665 */
return 
BBOOL(
BGL_OUTPUT_PROCEDURE_PORTP(BgL_objz00_19));} 

}



/* &output-procedure-port? */
obj_t BGl_z62outputzd2procedurezd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3523, obj_t BgL_objz00_3524)
{
{ /* Ieee/port.scm 665 */
return 
BGl_outputzd2procedurezd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3524);} 

}



/* current-input-port */
BGL_EXPORTED_DEF obj_t BGl_currentzd2inputzd2portz00zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 671 */
{ /* Ieee/port.scm 672 */
 obj_t BgL_tmpz00_5018;
BgL_tmpz00_5018 = 
BGL_CURRENT_DYNAMIC_ENV(); 
return 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_5018);} } 

}



/* &current-input-port */
obj_t BGl_z62currentzd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3525)
{
{ /* Ieee/port.scm 671 */
return 
BGl_currentzd2inputzd2portz00zz__r4_ports_6_10_1z00();} 

}



/* current-output-port */
BGL_EXPORTED_DEF obj_t BGl_currentzd2outputzd2portz00zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 677 */
{ /* Ieee/port.scm 678 */
 obj_t BgL_tmpz00_5022;
BgL_tmpz00_5022 = 
BGL_CURRENT_DYNAMIC_ENV(); 
return 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5022);} } 

}



/* &current-output-port */
obj_t BGl_z62currentzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3526)
{
{ /* Ieee/port.scm 677 */
return 
BGl_currentzd2outputzd2portz00zz__r4_ports_6_10_1z00();} 

}



/* current-error-port */
BGL_EXPORTED_DEF obj_t BGl_currentzd2errorzd2portz00zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 683 */
{ /* Ieee/port.scm 684 */
 obj_t BgL_tmpz00_5026;
BgL_tmpz00_5026 = 
BGL_CURRENT_DYNAMIC_ENV(); 
return 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_5026);} } 

}



/* &current-error-port */
obj_t BGl_z62currentzd2errorzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3527)
{
{ /* Ieee/port.scm 683 */
return 
BGl_currentzd2errorzd2portz00zz__r4_ports_6_10_1z00();} 

}



/* input-port-reopen! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2reopenz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_20)
{
{ /* Ieee/port.scm 689 */
if(
CBOOL(
bgl_input_port_reopen(BgL_portz00_20)))
{ /* Ieee/port.scm 690 */
return BFALSE;}  else 
{ /* Ieee/port.scm 690 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2736z00zz__r4_ports_6_10_1z00, BGl_string2737z00zz__r4_ports_6_10_1z00, BgL_portz00_20);} } 

}



/* &input-port-reopen! */
obj_t BGl_z62inputzd2portzd2reopenz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3528, obj_t BgL_portz00_3529)
{
{ /* Ieee/port.scm 689 */
{ /* Ieee/port.scm 690 */
 obj_t BgL_auxz00_5034;
if(
INPUT_PORTP(BgL_portz00_3529))
{ /* Ieee/port.scm 690 */
BgL_auxz00_5034 = BgL_portz00_3529
; }  else 
{ 
 obj_t BgL_auxz00_5037;
BgL_auxz00_5037 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(31434L), BGl_string2738z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3529); 
FAILURE(BgL_auxz00_5037,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2reopenz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_5034);} } 

}



/* input-port-clone! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2clonez12z12zz__r4_ports_6_10_1z00(obj_t BgL_dstz00_21, obj_t BgL_srcz00_22)
{
{ /* Ieee/port.scm 697 */
BGL_TAIL return 
bgl_input_port_clone(BgL_dstz00_21, BgL_srcz00_22);} 

}



/* &input-port-clone! */
obj_t BGl_z62inputzd2portzd2clonez12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3530, obj_t BgL_dstz00_3531, obj_t BgL_srcz00_3532)
{
{ /* Ieee/port.scm 697 */
{ /* Ieee/port.scm 698 */
 obj_t BgL_auxz00_5050; obj_t BgL_auxz00_5043;
if(
INPUT_PORTP(BgL_srcz00_3532))
{ /* Ieee/port.scm 698 */
BgL_auxz00_5050 = BgL_srcz00_3532
; }  else 
{ 
 obj_t BgL_auxz00_5053;
BgL_auxz00_5053 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(31854L), BGl_string2739z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_srcz00_3532); 
FAILURE(BgL_auxz00_5053,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_dstz00_3531))
{ /* Ieee/port.scm 698 */
BgL_auxz00_5043 = BgL_dstz00_3531
; }  else 
{ 
 obj_t BgL_auxz00_5046;
BgL_auxz00_5046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(31854L), BGl_string2739z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_dstz00_3531); 
FAILURE(BgL_auxz00_5046,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2clonez12z12zz__r4_ports_6_10_1z00(BgL_auxz00_5043, BgL_auxz00_5050);} } 

}



/* with-input-from-file */
BGL_EXPORTED_DEF obj_t BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_23, obj_t BgL_thunkz00_24)
{
{ /* Ieee/port.scm 703 */
{ /* Ieee/port.scm 704 */
 obj_t BgL_portz00_1479;
{ /* Ieee/port.scm 466 */

BgL_portz00_1479 = 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_stringz00_23, BTRUE, 
BINT(5000000L)); } 
if(
INPUT_PORTP(BgL_portz00_1479))
{ /* Ieee/port.scm 706 */
 obj_t BgL_denvz00_1481;
BgL_denvz00_1481 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 706 */
 obj_t BgL_oldzd2inputzd2portz00_1482;
BgL_oldzd2inputzd2portz00_1482 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1481); 
{ /* Ieee/port.scm 707 */

{ /* Ieee/port.scm 708 */
 obj_t BgL_exitd1059z00_1483;
BgL_exitd1059z00_1483 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 713 */
 obj_t BgL_zc3z04anonymousza31449ze3z87_3533;
BgL_zc3z04anonymousza31449ze3z87_3533 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31449ze3z87_3533, 
(int)(0L), BgL_denvz00_1481); 
PROCEDURE_SET(BgL_zc3z04anonymousza31449ze3z87_3533, 
(int)(1L), BgL_oldzd2inputzd2portz00_1482); 
PROCEDURE_SET(BgL_zc3z04anonymousza31449ze3z87_3533, 
(int)(2L), BgL_portz00_1479); 
{ /* Ieee/port.scm 708 */
 obj_t BgL_arg2070z00_2598;
{ /* Ieee/port.scm 708 */
 obj_t BgL_arg2072z00_2599;
BgL_arg2072z00_2599 = 
BGL_EXITD_PROTECT(BgL_exitd1059z00_1483); 
BgL_arg2070z00_2598 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31449ze3z87_3533, BgL_arg2072z00_2599); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1059z00_1483, BgL_arg2070z00_2598); BUNSPEC; } 
{ /* Ieee/port.scm 710 */
 obj_t BgL_tmp1061z00_1485;
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1481, BgL_portz00_1479); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_24, 0))
{ /* Ieee/port.scm 711 */
BgL_tmp1061z00_1485 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_24); }  else 
{ /* Ieee/port.scm 711 */
FAILURE(BGl_string2740z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_24);} 
{ /* Ieee/port.scm 708 */
 bool_t BgL_test3442z00_5084;
{ /* Ieee/port.scm 708 */
 obj_t BgL_arg2069z00_2601;
BgL_arg2069z00_2601 = 
BGL_EXITD_PROTECT(BgL_exitd1059z00_1483); 
BgL_test3442z00_5084 = 
PAIRP(BgL_arg2069z00_2601); } 
if(BgL_test3442z00_5084)
{ /* Ieee/port.scm 708 */
 obj_t BgL_arg2067z00_2602;
{ /* Ieee/port.scm 708 */
 obj_t BgL_arg2068z00_2603;
BgL_arg2068z00_2603 = 
BGL_EXITD_PROTECT(BgL_exitd1059z00_1483); 
{ /* Ieee/port.scm 708 */
 obj_t BgL_pairz00_2604;
if(
PAIRP(BgL_arg2068z00_2603))
{ /* Ieee/port.scm 708 */
BgL_pairz00_2604 = BgL_arg2068z00_2603; }  else 
{ 
 obj_t BgL_auxz00_5090;
BgL_auxz00_5090 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32317L), BGl_string2744z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2603); 
FAILURE(BgL_auxz00_5090,BFALSE,BFALSE);} 
BgL_arg2067z00_2602 = 
CDR(BgL_pairz00_2604); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1059z00_1483, BgL_arg2067z00_2602); BUNSPEC; }  else 
{ /* Ieee/port.scm 708 */BFALSE; } } 
BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31449ze3z87_3533); 
return BgL_tmp1061z00_1485;} } } } } }  else 
{ /* Ieee/port.scm 705 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2744z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_23);} } } 

}



/* &with-input-from-file */
obj_t BGl_z62withzd2inputzd2fromzd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3534, obj_t BgL_stringz00_3535, obj_t BgL_thunkz00_3536)
{
{ /* Ieee/port.scm 703 */
{ /* Ieee/port.scm 704 */
 obj_t BgL_auxz00_5105; obj_t BgL_auxz00_5098;
if(
PROCEDUREP(BgL_thunkz00_3536))
{ /* Ieee/port.scm 704 */
BgL_auxz00_5105 = BgL_thunkz00_3536
; }  else 
{ 
 obj_t BgL_auxz00_5108;
BgL_auxz00_5108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32154L), BGl_string2745z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3536); 
FAILURE(BgL_auxz00_5108,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3535))
{ /* Ieee/port.scm 704 */
BgL_auxz00_5098 = BgL_stringz00_3535
; }  else 
{ 
 obj_t BgL_auxz00_5101;
BgL_auxz00_5101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32154L), BGl_string2745z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3535); 
FAILURE(BgL_auxz00_5101,BFALSE,BFALSE);} 
return 
BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5098, BgL_auxz00_5105);} } 

}



/* &<@anonymous:1449> */
obj_t BGl_z62zc3z04anonymousza31449ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3537)
{
{ /* Ieee/port.scm 708 */
{ /* Ieee/port.scm 713 */
 obj_t BgL_denvz00_3538; obj_t BgL_oldzd2inputzd2portz00_3539; obj_t BgL_portz00_3540;
BgL_denvz00_3538 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3537, 
(int)(0L))); 
BgL_oldzd2inputzd2portz00_3539 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3537, 
(int)(1L))); 
BgL_portz00_3540 = 
PROCEDURE_REF(BgL_envz00_3537, 
(int)(2L)); 
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3538, BgL_oldzd2inputzd2portz00_3539); BUNSPEC; 
{ /* Ieee/port.scm 714 */
 obj_t BgL_portz00_4528;
if(
INPUT_PORTP(BgL_portz00_3540))
{ /* Ieee/port.scm 714 */
BgL_portz00_4528 = BgL_portz00_3540; }  else 
{ 
 obj_t BgL_auxz00_5124;
BgL_auxz00_5124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32484L), BGl_string2746z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3540); 
FAILURE(BgL_auxz00_5124,BFALSE,BFALSE);} 
return 
bgl_close_input_port(BgL_portz00_4528);} } } 

}



/* with-input-from-string */
BGL_EXPORTED_DEF obj_t BGl_withzd2inputzd2fromzd2stringzd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_25, obj_t BgL_thunkz00_26)
{
{ /* Ieee/port.scm 721 */
{ /* Ieee/port.scm 722 */
 obj_t BgL_portz00_1491;
{ /* Ieee/port.scm 469 */
 long BgL_endz00_1501;
BgL_endz00_1501 = 
STRING_LENGTH(BgL_stringz00_25); 
{ /* Ieee/port.scm 469 */

BgL_portz00_1491 = 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_stringz00_25, 
BINT(0L), 
BINT(BgL_endz00_1501)); } } 
{ /* Ieee/port.scm 722 */
 obj_t BgL_denvz00_1492;
BgL_denvz00_1492 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 723 */
 obj_t BgL_oldzd2inputzd2portz00_1493;
BgL_oldzd2inputzd2portz00_1493 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1492); 
{ /* Ieee/port.scm 724 */

{ /* Ieee/port.scm 725 */
 obj_t BgL_exitd1063z00_1494;
BgL_exitd1063z00_1494 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 730 */
 obj_t BgL_zc3z04anonymousza31450ze3z87_3541;
BgL_zc3z04anonymousza31450ze3z87_3541 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31450ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31450ze3z87_3541, 
(int)(0L), BgL_denvz00_1492); 
PROCEDURE_SET(BgL_zc3z04anonymousza31450ze3z87_3541, 
(int)(1L), BgL_oldzd2inputzd2portz00_1493); 
PROCEDURE_SET(BgL_zc3z04anonymousza31450ze3z87_3541, 
(int)(2L), BgL_portz00_1491); 
{ /* Ieee/port.scm 725 */
 obj_t BgL_arg2070z00_2607;
{ /* Ieee/port.scm 725 */
 obj_t BgL_arg2072z00_2608;
BgL_arg2072z00_2608 = 
BGL_EXITD_PROTECT(BgL_exitd1063z00_1494); 
BgL_arg2070z00_2607 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31450ze3z87_3541, BgL_arg2072z00_2608); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1063z00_1494, BgL_arg2070z00_2607); BUNSPEC; } 
{ /* Ieee/port.scm 727 */
 obj_t BgL_tmp1065z00_1496;
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1492, BgL_portz00_1491); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_26, 0))
{ /* Ieee/port.scm 728 */
BgL_tmp1065z00_1496 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_26); }  else 
{ /* Ieee/port.scm 728 */
FAILURE(BGl_string2747z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_26);} 
{ /* Ieee/port.scm 725 */
 bool_t BgL_test3448z00_5155;
{ /* Ieee/port.scm 725 */
 obj_t BgL_arg2069z00_2610;
BgL_arg2069z00_2610 = 
BGL_EXITD_PROTECT(BgL_exitd1063z00_1494); 
BgL_test3448z00_5155 = 
PAIRP(BgL_arg2069z00_2610); } 
if(BgL_test3448z00_5155)
{ /* Ieee/port.scm 725 */
 obj_t BgL_arg2067z00_2611;
{ /* Ieee/port.scm 725 */
 obj_t BgL_arg2068z00_2612;
BgL_arg2068z00_2612 = 
BGL_EXITD_PROTECT(BgL_exitd1063z00_1494); 
{ /* Ieee/port.scm 725 */
 obj_t BgL_pairz00_2613;
if(
PAIRP(BgL_arg2068z00_2612))
{ /* Ieee/port.scm 725 */
BgL_pairz00_2613 = BgL_arg2068z00_2612; }  else 
{ 
 obj_t BgL_auxz00_5161;
BgL_auxz00_5161 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32989L), BGl_string2748z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2612); 
FAILURE(BgL_auxz00_5161,BFALSE,BFALSE);} 
BgL_arg2067z00_2611 = 
CDR(BgL_pairz00_2613); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1063z00_1494, BgL_arg2067z00_2611); BUNSPEC; }  else 
{ /* Ieee/port.scm 725 */BFALSE; } } 
BGl_z62zc3z04anonymousza31450ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31450ze3z87_3541); 
return BgL_tmp1065z00_1496;} } } } } } } } 

}



/* &with-input-from-string */
obj_t BGl_z62withzd2inputzd2fromzd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3542, obj_t BgL_stringz00_3543, obj_t BgL_thunkz00_3544)
{
{ /* Ieee/port.scm 721 */
{ /* Ieee/port.scm 722 */
 obj_t BgL_auxz00_5175; obj_t BgL_auxz00_5168;
if(
PROCEDUREP(BgL_thunkz00_3544))
{ /* Ieee/port.scm 722 */
BgL_auxz00_5175 = BgL_thunkz00_3544
; }  else 
{ 
 obj_t BgL_auxz00_5178;
BgL_auxz00_5178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32860L), BGl_string2749z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3544); 
FAILURE(BgL_auxz00_5178,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3543))
{ /* Ieee/port.scm 722 */
BgL_auxz00_5168 = BgL_stringz00_3543
; }  else 
{ 
 obj_t BgL_auxz00_5171;
BgL_auxz00_5171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(32860L), BGl_string2749z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3543); 
FAILURE(BgL_auxz00_5171,BFALSE,BFALSE);} 
return 
BGl_withzd2inputzd2fromzd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5168, BgL_auxz00_5175);} } 

}



/* &<@anonymous:1450> */
obj_t BGl_z62zc3z04anonymousza31450ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3545)
{
{ /* Ieee/port.scm 725 */
{ /* Ieee/port.scm 730 */
 obj_t BgL_denvz00_3546; obj_t BgL_oldzd2inputzd2portz00_3547; obj_t BgL_portz00_3548;
BgL_denvz00_3546 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3545, 
(int)(0L))); 
BgL_oldzd2inputzd2portz00_3547 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3545, 
(int)(1L))); 
BgL_portz00_3548 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3545, 
(int)(2L))); 
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3546, BgL_oldzd2inputzd2portz00_3547); BUNSPEC; 
return 
bgl_close_input_port(BgL_portz00_3548);} } 

}



/* with-input-from-port */
BGL_EXPORTED_DEF obj_t BGl_withzd2inputzd2fromzd2portzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_27, obj_t BgL_thunkz00_28)
{
{ /* Ieee/port.scm 736 */
{ /* Ieee/port.scm 737 */
 obj_t BgL_denvz00_1502;
BgL_denvz00_1502 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 737 */
 obj_t BgL_oldzd2inputzd2portz00_1503;
BgL_oldzd2inputzd2portz00_1503 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1502); 
{ /* Ieee/port.scm 738 */

{ /* Ieee/port.scm 739 */
 obj_t BgL_exitd1067z00_1504;
BgL_exitd1067z00_1504 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ 
 obj_t BgL_zc3z04anonymousza31451ze3z87_3549;
BgL_zc3z04anonymousza31451ze3z87_3549 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31451ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31451ze3z87_3549, 
(int)(0L), BgL_denvz00_1502); 
PROCEDURE_SET(BgL_zc3z04anonymousza31451ze3z87_3549, 
(int)(1L), BgL_oldzd2inputzd2portz00_1503); 
{ /* Ieee/port.scm 739 */
 obj_t BgL_arg2070z00_2614;
{ /* Ieee/port.scm 739 */
 obj_t BgL_arg2072z00_2615;
BgL_arg2072z00_2615 = 
BGL_EXITD_PROTECT(BgL_exitd1067z00_1504); 
BgL_arg2070z00_2614 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31451ze3z87_3549, BgL_arg2072z00_2615); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1504, BgL_arg2070z00_2614); BUNSPEC; } 
{ /* Ieee/port.scm 741 */
 obj_t BgL_tmp1069z00_1506;
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1502, BgL_portz00_27); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_28, 0))
{ /* Ieee/port.scm 742 */
BgL_tmp1069z00_1506 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_28); }  else 
{ /* Ieee/port.scm 742 */
FAILURE(BGl_string2750z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_28);} 
{ /* Ieee/port.scm 739 */
 bool_t BgL_test3453z00_5214;
{ /* Ieee/port.scm 739 */
 obj_t BgL_arg2069z00_2617;
BgL_arg2069z00_2617 = 
BGL_EXITD_PROTECT(BgL_exitd1067z00_1504); 
BgL_test3453z00_5214 = 
PAIRP(BgL_arg2069z00_2617); } 
if(BgL_test3453z00_5214)
{ /* Ieee/port.scm 739 */
 obj_t BgL_arg2067z00_2618;
{ /* Ieee/port.scm 739 */
 obj_t BgL_arg2068z00_2619;
BgL_arg2068z00_2619 = 
BGL_EXITD_PROTECT(BgL_exitd1067z00_1504); 
{ /* Ieee/port.scm 739 */
 obj_t BgL_pairz00_2620;
if(
PAIRP(BgL_arg2068z00_2619))
{ /* Ieee/port.scm 739 */
BgL_pairz00_2620 = BgL_arg2068z00_2619; }  else 
{ 
 obj_t BgL_auxz00_5220;
BgL_auxz00_5220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(33526L), BGl_string2751z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2619); 
FAILURE(BgL_auxz00_5220,BFALSE,BFALSE);} 
BgL_arg2067z00_2618 = 
CDR(BgL_pairz00_2620); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1504, BgL_arg2067z00_2618); BUNSPEC; }  else 
{ /* Ieee/port.scm 739 */BFALSE; } } 
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1502, BgL_oldzd2inputzd2portz00_1503); BUNSPEC; 
return BgL_tmp1069z00_1506;} } } } } } } 

}



/* &with-input-from-port */
obj_t BGl_z62withzd2inputzd2fromzd2portzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3550, obj_t BgL_portz00_3551, obj_t BgL_thunkz00_3552)
{
{ /* Ieee/port.scm 736 */
{ /* Ieee/port.scm 737 */
 obj_t BgL_auxz00_5234; obj_t BgL_auxz00_5227;
if(
PROCEDUREP(BgL_thunkz00_3552))
{ /* Ieee/port.scm 737 */
BgL_auxz00_5234 = BgL_thunkz00_3552
; }  else 
{ 
 obj_t BgL_auxz00_5237;
BgL_auxz00_5237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(33434L), BGl_string2752z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3552); 
FAILURE(BgL_auxz00_5237,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_portz00_3551))
{ /* Ieee/port.scm 737 */
BgL_auxz00_5227 = BgL_portz00_3551
; }  else 
{ 
 obj_t BgL_auxz00_5230;
BgL_auxz00_5230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(33434L), BGl_string2752z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3551); 
FAILURE(BgL_auxz00_5230,BFALSE,BFALSE);} 
return 
BGl_withzd2inputzd2fromzd2portzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5227, BgL_auxz00_5234);} } 

}



/* &<@anonymous:1451> */
obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3553)
{
{ /* Ieee/port.scm 739 */
{ 
 obj_t BgL_denvz00_3554; obj_t BgL_oldzd2inputzd2portz00_3555;
BgL_denvz00_3554 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3553, 
(int)(0L))); 
BgL_oldzd2inputzd2portz00_3555 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3553, 
(int)(1L))); 
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3554, BgL_oldzd2inputzd2portz00_3555); 
return BUNSPEC;} } 

}



/* with-input-from-procedure */
BGL_EXPORTED_DEF obj_t BGl_withzd2inputzd2fromzd2procedurezd2zz__r4_ports_6_10_1z00(obj_t BgL_procz00_29, obj_t BgL_thunkz00_30)
{
{ /* Ieee/port.scm 748 */
{ /* Ieee/port.scm 749 */
 obj_t BgL_portz00_1509;
{ /* Ieee/port.scm 474 */

BgL_portz00_1509 = 
bgl_open_input_procedure(BgL_procz00_29, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2753z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(1024L))); } 
{ /* Ieee/port.scm 751 */
 obj_t BgL_denvz00_1511;
BgL_denvz00_1511 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 751 */
 obj_t BgL_oldzd2inputzd2portz00_1512;
BgL_oldzd2inputzd2portz00_1512 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_denvz00_1511); 
{ /* Ieee/port.scm 752 */

{ /* Ieee/port.scm 753 */
 obj_t BgL_exitd1071z00_1513;
BgL_exitd1071z00_1513 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 758 */
 obj_t BgL_zc3z04anonymousza31453ze3z87_3556;
BgL_zc3z04anonymousza31453ze3z87_3556 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31453ze3z87_3556, 
(int)(0L), BgL_denvz00_1511); 
PROCEDURE_SET(BgL_zc3z04anonymousza31453ze3z87_3556, 
(int)(1L), BgL_oldzd2inputzd2portz00_1512); 
PROCEDURE_SET(BgL_zc3z04anonymousza31453ze3z87_3556, 
(int)(2L), BgL_portz00_1509); 
{ /* Ieee/port.scm 753 */
 obj_t BgL_arg2070z00_2624;
{ /* Ieee/port.scm 753 */
 obj_t BgL_arg2072z00_2625;
BgL_arg2072z00_2625 = 
BGL_EXITD_PROTECT(BgL_exitd1071z00_1513); 
BgL_arg2070z00_2624 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31453ze3z87_3556, BgL_arg2072z00_2625); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1071z00_1513, BgL_arg2070z00_2624); BUNSPEC; } 
{ /* Ieee/port.scm 755 */
 obj_t BgL_tmp1073z00_1515;
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_1511, BgL_portz00_1509); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_30, 0))
{ /* Ieee/port.scm 756 */
BgL_tmp1073z00_1515 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_30); }  else 
{ /* Ieee/port.scm 756 */
FAILURE(BGl_string2754z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_30);} 
{ /* Ieee/port.scm 753 */
 bool_t BgL_test3458z00_5274;
{ /* Ieee/port.scm 753 */
 obj_t BgL_arg2069z00_2627;
BgL_arg2069z00_2627 = 
BGL_EXITD_PROTECT(BgL_exitd1071z00_1513); 
BgL_test3458z00_5274 = 
PAIRP(BgL_arg2069z00_2627); } 
if(BgL_test3458z00_5274)
{ /* Ieee/port.scm 753 */
 obj_t BgL_arg2067z00_2628;
{ /* Ieee/port.scm 753 */
 obj_t BgL_arg2068z00_2629;
BgL_arg2068z00_2629 = 
BGL_EXITD_PROTECT(BgL_exitd1071z00_1513); 
{ /* Ieee/port.scm 753 */
 obj_t BgL_pairz00_2630;
if(
PAIRP(BgL_arg2068z00_2629))
{ /* Ieee/port.scm 753 */
BgL_pairz00_2630 = BgL_arg2068z00_2629; }  else 
{ 
 obj_t BgL_auxz00_5280;
BgL_auxz00_5280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(34100L), BGl_string2755z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2629); 
FAILURE(BgL_auxz00_5280,BFALSE,BFALSE);} 
BgL_arg2067z00_2628 = 
CDR(BgL_pairz00_2630); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1071z00_1513, BgL_arg2067z00_2628); BUNSPEC; }  else 
{ /* Ieee/port.scm 753 */BFALSE; } } 
BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31453ze3z87_3556); 
return BgL_tmp1073z00_1515;} } } } } } } } 

}



/* &with-input-from-procedure */
obj_t BGl_z62withzd2inputzd2fromzd2procedurezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3557, obj_t BgL_procz00_3558, obj_t BgL_thunkz00_3559)
{
{ /* Ieee/port.scm 748 */
{ /* Ieee/port.scm 749 */
 obj_t BgL_auxz00_5294; obj_t BgL_auxz00_5287;
if(
PROCEDUREP(BgL_thunkz00_3559))
{ /* Ieee/port.scm 749 */
BgL_auxz00_5294 = BgL_thunkz00_3559
; }  else 
{ 
 obj_t BgL_auxz00_5297;
BgL_auxz00_5297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(33934L), BGl_string2756z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3559); 
FAILURE(BgL_auxz00_5297,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_procz00_3558))
{ /* Ieee/port.scm 749 */
BgL_auxz00_5287 = BgL_procz00_3558
; }  else 
{ 
 obj_t BgL_auxz00_5290;
BgL_auxz00_5290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(33934L), BGl_string2756z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3558); 
FAILURE(BgL_auxz00_5290,BFALSE,BFALSE);} 
return 
BGl_withzd2inputzd2fromzd2procedurezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5287, BgL_auxz00_5294);} } 

}



/* &<@anonymous:1453> */
obj_t BGl_z62zc3z04anonymousza31453ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3560)
{
{ /* Ieee/port.scm 753 */
{ /* Ieee/port.scm 758 */
 obj_t BgL_denvz00_3561; obj_t BgL_oldzd2inputzd2portz00_3562; obj_t BgL_portz00_3563;
BgL_denvz00_3561 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3560, 
(int)(0L))); 
BgL_oldzd2inputzd2portz00_3562 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3560, 
(int)(1L))); 
BgL_portz00_3563 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3560, 
(int)(2L))); 
BGL_ENV_CURRENT_INPUT_PORT_SET(BgL_denvz00_3561, BgL_oldzd2inputzd2portz00_3562); BUNSPEC; 
return 
bgl_close_input_port(BgL_portz00_3563);} } 

}



/* with-output-to-file */
BGL_EXPORTED_DEF obj_t BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_31, obj_t BgL_thunkz00_32)
{
{ /* Ieee/port.scm 765 */
{ /* Ieee/port.scm 766 */
 obj_t BgL_portz00_1520;
{ /* Ieee/port.scm 483 */

BgL_portz00_1520 = 
bgl_open_output_file(BgL_stringz00_31, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz))); } 
if(
OUTPUT_PORTP(BgL_portz00_1520))
{ /* Ieee/port.scm 768 */
 obj_t BgL_denvz00_1522;
BgL_denvz00_1522 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 768 */
 obj_t BgL_oldzd2outputzd2portz00_1523;
BgL_oldzd2outputzd2portz00_1523 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1522); 
{ /* Ieee/port.scm 769 */

{ /* Ieee/port.scm 770 */
 obj_t BgL_exitd1075z00_1524;
BgL_exitd1075z00_1524 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 775 */
 obj_t BgL_zc3z04anonymousza31455ze3z87_3564;
BgL_zc3z04anonymousza31455ze3z87_3564 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_3564, 
(int)(0L), BgL_denvz00_1522); 
PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_3564, 
(int)(1L), BgL_oldzd2outputzd2portz00_1523); 
PROCEDURE_SET(BgL_zc3z04anonymousza31455ze3z87_3564, 
(int)(2L), BgL_portz00_1520); 
{ /* Ieee/port.scm 770 */
 obj_t BgL_arg2070z00_2634;
{ /* Ieee/port.scm 770 */
 obj_t BgL_arg2072z00_2635;
BgL_arg2072z00_2635 = 
BGL_EXITD_PROTECT(BgL_exitd1075z00_1524); 
BgL_arg2070z00_2634 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31455ze3z87_3564, BgL_arg2072z00_2635); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1524, BgL_arg2070z00_2634); BUNSPEC; } 
{ /* Ieee/port.scm 772 */
 obj_t BgL_tmp1077z00_1526;
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1522, BgL_portz00_1520); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_32, 0))
{ /* Ieee/port.scm 773 */
BgL_tmp1077z00_1526 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_32); }  else 
{ /* Ieee/port.scm 773 */
FAILURE(BGl_string2757z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_32);} 
{ /* Ieee/port.scm 770 */
 bool_t BgL_test3464z00_5340;
{ /* Ieee/port.scm 770 */
 obj_t BgL_arg2069z00_2637;
BgL_arg2069z00_2637 = 
BGL_EXITD_PROTECT(BgL_exitd1075z00_1524); 
BgL_test3464z00_5340 = 
PAIRP(BgL_arg2069z00_2637); } 
if(BgL_test3464z00_5340)
{ /* Ieee/port.scm 770 */
 obj_t BgL_arg2067z00_2638;
{ /* Ieee/port.scm 770 */
 obj_t BgL_arg2068z00_2639;
BgL_arg2068z00_2639 = 
BGL_EXITD_PROTECT(BgL_exitd1075z00_1524); 
{ /* Ieee/port.scm 770 */
 obj_t BgL_pairz00_2640;
if(
PAIRP(BgL_arg2068z00_2639))
{ /* Ieee/port.scm 770 */
BgL_pairz00_2640 = BgL_arg2068z00_2639; }  else 
{ 
 obj_t BgL_auxz00_5346;
BgL_auxz00_5346 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(34782L), BGl_string2758z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2639); 
FAILURE(BgL_auxz00_5346,BFALSE,BFALSE);} 
BgL_arg2067z00_2638 = 
CDR(BgL_pairz00_2640); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1524, BgL_arg2067z00_2638); BUNSPEC; }  else 
{ /* Ieee/port.scm 770 */BFALSE; } } 
BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31455ze3z87_3564); 
return BgL_tmp1077z00_1526;} } } } } }  else 
{ /* Ieee/port.scm 767 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2758z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_31);} } } 

}



/* &with-output-to-file */
obj_t BGl_z62withzd2outputzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3565, obj_t BgL_stringz00_3566, obj_t BgL_thunkz00_3567)
{
{ /* Ieee/port.scm 765 */
{ /* Ieee/port.scm 766 */
 obj_t BgL_auxz00_5361; obj_t BgL_auxz00_5354;
if(
PROCEDUREP(BgL_thunkz00_3567))
{ /* Ieee/port.scm 766 */
BgL_auxz00_5361 = BgL_thunkz00_3567
; }  else 
{ 
 obj_t BgL_auxz00_5364;
BgL_auxz00_5364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(34615L), BGl_string2759z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3567); 
FAILURE(BgL_auxz00_5364,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3566))
{ /* Ieee/port.scm 766 */
BgL_auxz00_5354 = BgL_stringz00_3566
; }  else 
{ 
 obj_t BgL_auxz00_5357;
BgL_auxz00_5357 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(34615L), BGl_string2759z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3566); 
FAILURE(BgL_auxz00_5357,BFALSE,BFALSE);} 
return 
BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5354, BgL_auxz00_5361);} } 

}



/* &<@anonymous:1455> */
obj_t BGl_z62zc3z04anonymousza31455ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3568)
{
{ /* Ieee/port.scm 770 */
{ /* Ieee/port.scm 775 */
 obj_t BgL_denvz00_3569; obj_t BgL_oldzd2outputzd2portz00_3570; obj_t BgL_portz00_3571;
BgL_denvz00_3569 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3568, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3570 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3568, 
(int)(1L))); 
BgL_portz00_3571 = 
PROCEDURE_REF(BgL_envz00_3568, 
(int)(2L)); 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3569, BgL_oldzd2outputzd2portz00_3570); BUNSPEC; 
{ /* Ieee/port.scm 776 */
 obj_t BgL_portz00_4529;
if(
OUTPUT_PORTP(BgL_portz00_3571))
{ /* Ieee/port.scm 776 */
BgL_portz00_4529 = BgL_portz00_3571; }  else 
{ 
 obj_t BgL_auxz00_5380;
BgL_auxz00_5380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(34953L), BGl_string2760z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3571); 
FAILURE(BgL_auxz00_5380,BFALSE,BFALSE);} 
return 
bgl_close_output_port(BgL_portz00_4529);} } } 

}



/* with-append-to-file */
BGL_EXPORTED_DEF obj_t BGl_withzd2appendzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_33, obj_t BgL_thunkz00_34)
{
{ /* Ieee/port.scm 783 */
{ /* Ieee/port.scm 784 */
 obj_t BgL_portz00_1531;
{ /* Ieee/port.scm 484 */

BgL_portz00_1531 = 
bgl_append_output_file(BgL_stringz00_33, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2728z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz))); } 
if(
OUTPUT_PORTP(BgL_portz00_1531))
{ /* Ieee/port.scm 786 */
 obj_t BgL_denvz00_1533;
BgL_denvz00_1533 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 786 */
 obj_t BgL_oldzd2outputzd2portz00_1534;
BgL_oldzd2outputzd2portz00_1534 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1533); 
{ /* Ieee/port.scm 787 */

{ /* Ieee/port.scm 788 */
 obj_t BgL_exitd1079z00_1535;
BgL_exitd1079z00_1535 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 793 */
 obj_t BgL_zc3z04anonymousza31457ze3z87_3572;
BgL_zc3z04anonymousza31457ze3z87_3572 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3572, 
(int)(0L), BgL_denvz00_1533); 
PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3572, 
(int)(1L), BgL_oldzd2outputzd2portz00_1534); 
PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_3572, 
(int)(2L), BgL_portz00_1531); 
{ /* Ieee/port.scm 788 */
 obj_t BgL_arg2070z00_2644;
{ /* Ieee/port.scm 788 */
 obj_t BgL_arg2072z00_2645;
BgL_arg2072z00_2645 = 
BGL_EXITD_PROTECT(BgL_exitd1079z00_1535); 
BgL_arg2070z00_2644 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31457ze3z87_3572, BgL_arg2072z00_2645); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1079z00_1535, BgL_arg2070z00_2644); BUNSPEC; } 
{ /* Ieee/port.scm 790 */
 obj_t BgL_tmp1081z00_1537;
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1533, BgL_portz00_1531); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_34, 0))
{ /* Ieee/port.scm 791 */
BgL_tmp1081z00_1537 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_34); }  else 
{ /* Ieee/port.scm 791 */
FAILURE(BGl_string2761z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_34);} 
{ /* Ieee/port.scm 788 */
 bool_t BgL_test3471z00_5412;
{ /* Ieee/port.scm 788 */
 obj_t BgL_arg2069z00_2647;
BgL_arg2069z00_2647 = 
BGL_EXITD_PROTECT(BgL_exitd1079z00_1535); 
BgL_test3471z00_5412 = 
PAIRP(BgL_arg2069z00_2647); } 
if(BgL_test3471z00_5412)
{ /* Ieee/port.scm 788 */
 obj_t BgL_arg2067z00_2648;
{ /* Ieee/port.scm 788 */
 obj_t BgL_arg2068z00_2649;
BgL_arg2068z00_2649 = 
BGL_EXITD_PROTECT(BgL_exitd1079z00_1535); 
{ /* Ieee/port.scm 788 */
 obj_t BgL_pairz00_2650;
if(
PAIRP(BgL_arg2068z00_2649))
{ /* Ieee/port.scm 788 */
BgL_pairz00_2650 = BgL_arg2068z00_2649; }  else 
{ 
 obj_t BgL_auxz00_5418;
BgL_auxz00_5418 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(35494L), BGl_string2762z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2649); 
FAILURE(BgL_auxz00_5418,BFALSE,BFALSE);} 
BgL_arg2067z00_2648 = 
CDR(BgL_pairz00_2650); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1079z00_1535, BgL_arg2067z00_2648); BUNSPEC; }  else 
{ /* Ieee/port.scm 788 */BFALSE; } } 
BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31457ze3z87_3572); 
return BgL_tmp1081z00_1537;} } } } } }  else 
{ /* Ieee/port.scm 785 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2758z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_33);} } } 

}



/* &with-append-to-file */
obj_t BGl_z62withzd2appendzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3573, obj_t BgL_stringz00_3574, obj_t BgL_thunkz00_3575)
{
{ /* Ieee/port.scm 783 */
{ /* Ieee/port.scm 784 */
 obj_t BgL_auxz00_5433; obj_t BgL_auxz00_5426;
if(
PROCEDUREP(BgL_thunkz00_3575))
{ /* Ieee/port.scm 784 */
BgL_auxz00_5433 = BgL_thunkz00_3575
; }  else 
{ 
 obj_t BgL_auxz00_5436;
BgL_auxz00_5436 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(35325L), BGl_string2763z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3575); 
FAILURE(BgL_auxz00_5436,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3574))
{ /* Ieee/port.scm 784 */
BgL_auxz00_5426 = BgL_stringz00_3574
; }  else 
{ 
 obj_t BgL_auxz00_5429;
BgL_auxz00_5429 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(35325L), BGl_string2763z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3574); 
FAILURE(BgL_auxz00_5429,BFALSE,BFALSE);} 
return 
BGl_withzd2appendzd2tozd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5426, BgL_auxz00_5433);} } 

}



/* &<@anonymous:1457> */
obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3576)
{
{ /* Ieee/port.scm 788 */
{ /* Ieee/port.scm 793 */
 obj_t BgL_denvz00_3577; obj_t BgL_oldzd2outputzd2portz00_3578; obj_t BgL_portz00_3579;
BgL_denvz00_3577 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3576, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3578 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3576, 
(int)(1L))); 
BgL_portz00_3579 = 
PROCEDURE_REF(BgL_envz00_3576, 
(int)(2L)); 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3577, BgL_oldzd2outputzd2portz00_3578); BUNSPEC; 
{ /* Ieee/port.scm 794 */
 obj_t BgL_portz00_4530;
if(
OUTPUT_PORTP(BgL_portz00_3579))
{ /* Ieee/port.scm 794 */
BgL_portz00_4530 = BgL_portz00_3579; }  else 
{ 
 obj_t BgL_auxz00_5452;
BgL_auxz00_5452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(35665L), BGl_string2764z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3579); 
FAILURE(BgL_auxz00_5452,BFALSE,BFALSE);} 
return 
bgl_close_output_port(BgL_portz00_4530);} } } 

}



/* with-output-to-port */
BGL_EXPORTED_DEF obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_35, obj_t BgL_thunkz00_36)
{
{ /* Ieee/port.scm 801 */
{ /* Ieee/port.scm 802 */
 obj_t BgL_denvz00_1542;
BgL_denvz00_1542 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 802 */
 obj_t BgL_oldzd2outputzd2portz00_1543;
BgL_oldzd2outputzd2portz00_1543 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1542); 
{ /* Ieee/port.scm 803 */

{ /* Ieee/port.scm 804 */
 obj_t BgL_exitd1083z00_1544;
BgL_exitd1083z00_1544 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ 
 obj_t BgL_zc3z04anonymousza31458ze3z87_3580;
BgL_zc3z04anonymousza31458ze3z87_3580 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31458ze3z87_3580, 
(int)(0L), BgL_denvz00_1542); 
PROCEDURE_SET(BgL_zc3z04anonymousza31458ze3z87_3580, 
(int)(1L), BgL_oldzd2outputzd2portz00_1543); 
{ /* Ieee/port.scm 804 */
 obj_t BgL_arg2070z00_2651;
{ /* Ieee/port.scm 804 */
 obj_t BgL_arg2072z00_2652;
BgL_arg2072z00_2652 = 
BGL_EXITD_PROTECT(BgL_exitd1083z00_1544); 
BgL_arg2070z00_2651 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31458ze3z87_3580, BgL_arg2072z00_2652); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1083z00_1544, BgL_arg2070z00_2651); BUNSPEC; } 
{ /* Ieee/port.scm 806 */
 obj_t BgL_tmp1085z00_1546;
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1542, BgL_portz00_35); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_36, 0))
{ /* Ieee/port.scm 807 */
BgL_tmp1085z00_1546 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_36); }  else 
{ /* Ieee/port.scm 807 */
FAILURE(BGl_string2765z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_36);} 
{ /* Ieee/port.scm 804 */
 bool_t BgL_test3477z00_5477;
{ /* Ieee/port.scm 804 */
 obj_t BgL_arg2069z00_2654;
BgL_arg2069z00_2654 = 
BGL_EXITD_PROTECT(BgL_exitd1083z00_1544); 
BgL_test3477z00_5477 = 
PAIRP(BgL_arg2069z00_2654); } 
if(BgL_test3477z00_5477)
{ /* Ieee/port.scm 804 */
 obj_t BgL_arg2067z00_2655;
{ /* Ieee/port.scm 804 */
 obj_t BgL_arg2068z00_2656;
BgL_arg2068z00_2656 = 
BGL_EXITD_PROTECT(BgL_exitd1083z00_1544); 
{ /* Ieee/port.scm 804 */
 obj_t BgL_pairz00_2657;
if(
PAIRP(BgL_arg2068z00_2656))
{ /* Ieee/port.scm 804 */
BgL_pairz00_2657 = BgL_arg2068z00_2656; }  else 
{ 
 obj_t BgL_auxz00_5483;
BgL_auxz00_5483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(36129L), BGl_string2766z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2656); 
FAILURE(BgL_auxz00_5483,BFALSE,BFALSE);} 
BgL_arg2067z00_2655 = 
CDR(BgL_pairz00_2657); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1083z00_1544, BgL_arg2067z00_2655); BUNSPEC; }  else 
{ /* Ieee/port.scm 804 */BFALSE; } } 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1542, BgL_oldzd2outputzd2portz00_1543); BUNSPEC; 
return BgL_tmp1085z00_1546;} } } } } } } 

}



/* &with-output-to-port */
obj_t BGl_z62withzd2outputzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3581, obj_t BgL_portz00_3582, obj_t BgL_thunkz00_3583)
{
{ /* Ieee/port.scm 801 */
{ /* Ieee/port.scm 802 */
 obj_t BgL_auxz00_5497; obj_t BgL_auxz00_5490;
if(
PROCEDUREP(BgL_thunkz00_3583))
{ /* Ieee/port.scm 802 */
BgL_auxz00_5497 = BgL_thunkz00_3583
; }  else 
{ 
 obj_t BgL_auxz00_5500;
BgL_auxz00_5500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(36035L), BGl_string2767z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3583); 
FAILURE(BgL_auxz00_5500,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_3582))
{ /* Ieee/port.scm 802 */
BgL_auxz00_5490 = BgL_portz00_3582
; }  else 
{ 
 obj_t BgL_auxz00_5493;
BgL_auxz00_5493 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(36035L), BGl_string2767z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3582); 
FAILURE(BgL_auxz00_5493,BFALSE,BFALSE);} 
return 
BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5490, BgL_auxz00_5497);} } 

}



/* &<@anonymous:1458> */
obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3584)
{
{ /* Ieee/port.scm 804 */
{ 
 obj_t BgL_denvz00_3585; obj_t BgL_oldzd2outputzd2portz00_3586;
BgL_denvz00_3585 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3584, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3586 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3584, 
(int)(1L))); 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3585, BgL_oldzd2outputzd2portz00_3586); 
return BUNSPEC;} } 

}



/* with-output-to-string */
BGL_EXPORTED_DEF obj_t BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t BgL_thunkz00_37)
{
{ /* Ieee/port.scm 813 */
{ /* Ieee/port.scm 814 */
 obj_t BgL_portz00_1549;
{ /* Ieee/port.scm 814 */

{ /* Ieee/port.scm 485 */

BgL_portz00_1549 = 
bgl_open_output_string(
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(128L))); } } 
{ /* Ieee/port.scm 814 */
 obj_t BgL_denvz00_1550;
BgL_denvz00_1550 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 815 */
 obj_t BgL_oldzd2outputzd2portz00_1551;
BgL_oldzd2outputzd2portz00_1551 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1550); 
{ /* Ieee/port.scm 816 */
 obj_t BgL_resz00_3596;
BgL_resz00_3596 = 
MAKE_CELL(BUNSPEC); 
{ /* Ieee/port.scm 817 */

{ /* Ieee/port.scm 818 */
 obj_t BgL_exitd1087z00_1553;
BgL_exitd1087z00_1553 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 823 */
 obj_t BgL_zc3z04anonymousza31459ze3z87_3587;
BgL_zc3z04anonymousza31459ze3z87_3587 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31459ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31459ze3z87_3587, 
(int)(0L), BgL_denvz00_1550); 
PROCEDURE_SET(BgL_zc3z04anonymousza31459ze3z87_3587, 
(int)(1L), BgL_oldzd2outputzd2portz00_1551); 
PROCEDURE_SET(BgL_zc3z04anonymousza31459ze3z87_3587, 
(int)(2L), BgL_portz00_1549); 
PROCEDURE_SET(BgL_zc3z04anonymousza31459ze3z87_3587, 
(int)(3L), 
((obj_t)BgL_resz00_3596)); 
{ /* Ieee/port.scm 818 */
 obj_t BgL_arg2070z00_2660;
{ /* Ieee/port.scm 818 */
 obj_t BgL_arg2072z00_2661;
BgL_arg2072z00_2661 = 
BGL_EXITD_PROTECT(BgL_exitd1087z00_1553); 
BgL_arg2070z00_2660 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31459ze3z87_3587, BgL_arg2072z00_2661); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1087z00_1553, BgL_arg2070z00_2660); BUNSPEC; } 
{ /* Ieee/port.scm 820 */
 obj_t BgL_tmp1089z00_1555;
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1550, BgL_portz00_1549); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_37, 0))
{ /* Ieee/port.scm 821 */
BgL_tmp1089z00_1555 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_37); }  else 
{ /* Ieee/port.scm 821 */
FAILURE(BGl_string2768z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_37);} 
{ /* Ieee/port.scm 818 */
 bool_t BgL_test3482z00_5540;
{ /* Ieee/port.scm 818 */
 obj_t BgL_arg2069z00_2663;
BgL_arg2069z00_2663 = 
BGL_EXITD_PROTECT(BgL_exitd1087z00_1553); 
BgL_test3482z00_5540 = 
PAIRP(BgL_arg2069z00_2663); } 
if(BgL_test3482z00_5540)
{ /* Ieee/port.scm 818 */
 obj_t BgL_arg2067z00_2664;
{ /* Ieee/port.scm 818 */
 obj_t BgL_arg2068z00_2665;
BgL_arg2068z00_2665 = 
BGL_EXITD_PROTECT(BgL_exitd1087z00_1553); 
{ /* Ieee/port.scm 818 */
 obj_t BgL_pairz00_2666;
if(
PAIRP(BgL_arg2068z00_2665))
{ /* Ieee/port.scm 818 */
BgL_pairz00_2666 = BgL_arg2068z00_2665; }  else 
{ 
 obj_t BgL_auxz00_5546;
BgL_auxz00_5546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(36678L), BGl_string2769z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2665); 
FAILURE(BgL_auxz00_5546,BFALSE,BFALSE);} 
BgL_arg2067z00_2664 = 
CDR(BgL_pairz00_2666); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1087z00_1553, BgL_arg2067z00_2664); BUNSPEC; }  else 
{ /* Ieee/port.scm 818 */BFALSE; } } 
BGl_z62zc3z04anonymousza31459ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31459ze3z87_3587); BgL_tmp1089z00_1555; } } } 
{ /* Ieee/port.scm 825 */
 obj_t BgL_aux2236z00_4019;
BgL_aux2236z00_4019 = 
CELL_REF(BgL_resz00_3596); 
if(
STRINGP(BgL_aux2236z00_4019))
{ /* Ieee/port.scm 825 */
return BgL_aux2236z00_4019;}  else 
{ 
 obj_t BgL_auxz00_5555;
BgL_auxz00_5555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(36874L), BGl_string2769z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2236z00_4019); 
FAILURE(BgL_auxz00_5555,BFALSE,BFALSE);} } } } } } } } 

}



/* &with-output-to-string */
obj_t BGl_z62withzd2outputzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3588, obj_t BgL_thunkz00_3589)
{
{ /* Ieee/port.scm 813 */
{ /* Ieee/port.scm 814 */
 obj_t BgL_auxz00_5559;
if(
PROCEDUREP(BgL_thunkz00_3589))
{ /* Ieee/port.scm 814 */
BgL_auxz00_5559 = BgL_thunkz00_3589
; }  else 
{ 
 obj_t BgL_auxz00_5562;
BgL_auxz00_5562 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(36531L), BGl_string2770z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3589); 
FAILURE(BgL_auxz00_5562,BFALSE,BFALSE);} 
return 
BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5559);} } 

}



/* &<@anonymous:1459> */
obj_t BGl_z62zc3z04anonymousza31459ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3590)
{
{ /* Ieee/port.scm 818 */
{ /* Ieee/port.scm 823 */
 obj_t BgL_denvz00_3591; obj_t BgL_oldzd2outputzd2portz00_3592; obj_t BgL_portz00_3593; obj_t BgL_resz00_3594;
BgL_denvz00_3591 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3590, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3592 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3590, 
(int)(1L))); 
BgL_portz00_3593 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3590, 
(int)(2L))); 
BgL_resz00_3594 = 
PROCEDURE_REF(BgL_envz00_3590, 
(int)(3L)); 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3591, BgL_oldzd2outputzd2portz00_3592); BUNSPEC; 
{ /* Ieee/port.scm 824 */
 obj_t BgL_auxz00_4531;
BgL_auxz00_4531 = 
bgl_close_output_port(BgL_portz00_3593); 
return 
CELL_SET(BgL_resz00_3594, BgL_auxz00_4531);} } } 

}



/* with-output-to-procedure */
BGL_EXPORTED_DEF obj_t BGl_withzd2outputzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t BgL_procz00_38, obj_t BgL_thunkz00_39)
{
{ /* Ieee/port.scm 830 */
{ /* Ieee/port.scm 831 */
 obj_t BgL_portz00_1559;
{ /* Ieee/port.scm 486 */

BgL_portz00_1559 = 
BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(BgL_procz00_38, BGl_proc2771z00zz__r4_ports_6_10_1z00, BTRUE, BGl_proc2772z00zz__r4_ports_6_10_1z00); } 
{ /* Ieee/port.scm 831 */
 obj_t BgL_denvz00_1560;
BgL_denvz00_1560 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 832 */
 obj_t BgL_oldzd2outputzd2portz00_1561;
BgL_oldzd2outputzd2portz00_1561 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_denvz00_1560); 
{ /* Ieee/port.scm 833 */
 obj_t BgL_resz00_3612;
BgL_resz00_3612 = 
MAKE_CELL(BUNSPEC); 
{ /* Ieee/port.scm 834 */

{ /* Ieee/port.scm 835 */
 obj_t BgL_exitd1091z00_1563;
BgL_exitd1091z00_1563 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 840 */
 obj_t BgL_zc3z04anonymousza31460ze3z87_3598;
BgL_zc3z04anonymousza31460ze3z87_3598 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31460ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31460ze3z87_3598, 
(int)(0L), BgL_denvz00_1560); 
PROCEDURE_SET(BgL_zc3z04anonymousza31460ze3z87_3598, 
(int)(1L), BgL_oldzd2outputzd2portz00_1561); 
PROCEDURE_SET(BgL_zc3z04anonymousza31460ze3z87_3598, 
(int)(2L), BgL_portz00_1559); 
PROCEDURE_SET(BgL_zc3z04anonymousza31460ze3z87_3598, 
(int)(3L), 
((obj_t)BgL_resz00_3612)); 
{ /* Ieee/port.scm 835 */
 obj_t BgL_arg2070z00_2668;
{ /* Ieee/port.scm 835 */
 obj_t BgL_arg2072z00_2669;
BgL_arg2072z00_2669 = 
BGL_EXITD_PROTECT(BgL_exitd1091z00_1563); 
BgL_arg2070z00_2668 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31460ze3z87_3598, BgL_arg2072z00_2669); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1091z00_1563, BgL_arg2070z00_2668); BUNSPEC; } 
{ /* Ieee/port.scm 837 */
 obj_t BgL_tmp1093z00_1565;
{ /* Ieee/port.scm 837 */
 obj_t BgL_tmpz00_5599;
if(
OUTPUT_PORTP(BgL_portz00_1559))
{ /* Ieee/port.scm 837 */
BgL_tmpz00_5599 = BgL_portz00_1559
; }  else 
{ 
 obj_t BgL_auxz00_5602;
BgL_auxz00_5602 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37370L), BGl_string2773z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_1559); 
FAILURE(BgL_auxz00_5602,BFALSE,BFALSE);} 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_1560, BgL_tmpz00_5599); } BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_39, 0))
{ /* Ieee/port.scm 838 */
BgL_tmp1093z00_1565 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_39); }  else 
{ /* Ieee/port.scm 838 */
FAILURE(BGl_string2774z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_39);} 
{ /* Ieee/port.scm 835 */
 bool_t BgL_test3488z00_5613;
{ /* Ieee/port.scm 835 */
 obj_t BgL_arg2069z00_2671;
BgL_arg2069z00_2671 = 
BGL_EXITD_PROTECT(BgL_exitd1091z00_1563); 
BgL_test3488z00_5613 = 
PAIRP(BgL_arg2069z00_2671); } 
if(BgL_test3488z00_5613)
{ /* Ieee/port.scm 835 */
 obj_t BgL_arg2067z00_2672;
{ /* Ieee/port.scm 835 */
 obj_t BgL_arg2068z00_2673;
BgL_arg2068z00_2673 = 
BGL_EXITD_PROTECT(BgL_exitd1091z00_1563); 
{ /* Ieee/port.scm 835 */
 obj_t BgL_pairz00_2674;
if(
PAIRP(BgL_arg2068z00_2673))
{ /* Ieee/port.scm 835 */
BgL_pairz00_2674 = BgL_arg2068z00_2673; }  else 
{ 
 obj_t BgL_auxz00_5619;
BgL_auxz00_5619 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37307L), BGl_string2773z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2673); 
FAILURE(BgL_auxz00_5619,BFALSE,BFALSE);} 
BgL_arg2067z00_2672 = 
CDR(BgL_pairz00_2674); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1091z00_1563, BgL_arg2067z00_2672); BUNSPEC; }  else 
{ /* Ieee/port.scm 835 */BFALSE; } } 
BGl_z62zc3z04anonymousza31460ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31460ze3z87_3598); BgL_tmp1093z00_1565; } } } 
return 
CELL_REF(BgL_resz00_3612);} } } } } } 

}



/* &with-output-to-procedure */
obj_t BGl_z62withzd2outputzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3601, obj_t BgL_procz00_3602, obj_t BgL_thunkz00_3603)
{
{ /* Ieee/port.scm 830 */
{ /* Ieee/port.scm 831 */
 obj_t BgL_auxz00_5633; obj_t BgL_auxz00_5626;
if(
PROCEDUREP(BgL_thunkz00_3603))
{ /* Ieee/port.scm 831 */
BgL_auxz00_5633 = BgL_thunkz00_3603
; }  else 
{ 
 obj_t BgL_auxz00_5636;
BgL_auxz00_5636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37152L), BGl_string2775z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3603); 
FAILURE(BgL_auxz00_5636,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_procz00_3602))
{ /* Ieee/port.scm 831 */
BgL_auxz00_5626 = BgL_procz00_3602
; }  else 
{ 
 obj_t BgL_auxz00_5629;
BgL_auxz00_5629 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37152L), BGl_string2775z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3602); 
FAILURE(BgL_auxz00_5629,BFALSE,BFALSE);} 
return 
BGl_withzd2outputzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5626, BgL_auxz00_5633);} } 

}



/* &<@anonymous:1460> */
obj_t BGl_z62zc3z04anonymousza31460ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3604)
{
{ /* Ieee/port.scm 835 */
{ /* Ieee/port.scm 840 */
 obj_t BgL_denvz00_3605; obj_t BgL_oldzd2outputzd2portz00_3606; obj_t BgL_portz00_3607; obj_t BgL_resz00_3608;
BgL_denvz00_3605 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3604, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3606 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3604, 
(int)(1L))); 
BgL_portz00_3607 = 
PROCEDURE_REF(BgL_envz00_3604, 
(int)(2L)); 
BgL_resz00_3608 = 
PROCEDURE_REF(BgL_envz00_3604, 
(int)(3L)); 
BGL_ENV_CURRENT_OUTPUT_PORT_SET(BgL_denvz00_3605, BgL_oldzd2outputzd2portz00_3606); BUNSPEC; 
{ /* Ieee/port.scm 841 */
 obj_t BgL_auxz00_4532;
{ /* Ieee/port.scm 841 */
 obj_t BgL_portz00_4533;
if(
OUTPUT_PORTP(BgL_portz00_3607))
{ /* Ieee/port.scm 841 */
BgL_portz00_4533 = BgL_portz00_3607; }  else 
{ 
 obj_t BgL_auxz00_5654;
BgL_auxz00_5654 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37488L), BGl_string2776z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3607); 
FAILURE(BgL_auxz00_5654,BFALSE,BFALSE);} 
BgL_auxz00_4532 = 
bgl_close_output_port(BgL_portz00_4533); } 
return 
CELL_SET(BgL_resz00_3608, BgL_auxz00_4532);} } } 

}



/* &close */
obj_t BGl_z62closez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3610)
{
{ /* Ieee/port.scm 486 */
return 
BBOOL(((bool_t)0));} 

}



/* &flush */
obj_t BGl_z62flushz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3611)
{
{ /* Ieee/port.scm 486 */
return 
BBOOL(((bool_t)0));} 

}



/* with-error-to-string */
BGL_EXPORTED_DEF obj_t BGl_withzd2errorzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t BgL_thunkz00_40)
{
{ /* Ieee/port.scm 847 */
{ /* Ieee/port.scm 848 */
 obj_t BgL_portz00_1576;
{ /* Ieee/port.scm 848 */

{ /* Ieee/port.scm 485 */

BgL_portz00_1576 = 
bgl_open_output_string(
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(128L))); } } 
{ /* Ieee/port.scm 850 */
 obj_t BgL_denvz00_1578;
BgL_denvz00_1578 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 850 */
 obj_t BgL_oldzd2errorzd2portz00_1579;
BgL_oldzd2errorzd2portz00_1579 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1578); 
{ /* Ieee/port.scm 851 */
 obj_t BgL_resz00_3623;
BgL_resz00_3623 = 
MAKE_CELL(BUNSPEC); 
{ /* Ieee/port.scm 852 */

{ /* Ieee/port.scm 853 */
 obj_t BgL_exitd1095z00_1581;
BgL_exitd1095z00_1581 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 858 */
 obj_t BgL_zc3z04anonymousza31464ze3z87_3614;
BgL_zc3z04anonymousza31464ze3z87_3614 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3614, 
(int)(0L), BgL_denvz00_1578); 
PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3614, 
(int)(1L), BgL_oldzd2errorzd2portz00_1579); 
PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3614, 
(int)(2L), BgL_portz00_1576); 
PROCEDURE_SET(BgL_zc3z04anonymousza31464ze3z87_3614, 
(int)(3L), 
((obj_t)BgL_resz00_3623)); 
{ /* Ieee/port.scm 853 */
 obj_t BgL_arg2070z00_2677;
{ /* Ieee/port.scm 853 */
 obj_t BgL_arg2072z00_2678;
BgL_arg2072z00_2678 = 
BGL_EXITD_PROTECT(BgL_exitd1095z00_1581); 
BgL_arg2070z00_2677 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31464ze3z87_3614, BgL_arg2072z00_2678); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1095z00_1581, BgL_arg2070z00_2677); BUNSPEC; } 
{ /* Ieee/port.scm 855 */
 obj_t BgL_tmp1097z00_1583;
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1578, BgL_portz00_1576); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_40, 0))
{ /* Ieee/port.scm 856 */
BgL_tmp1097z00_1583 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_40); }  else 
{ /* Ieee/port.scm 856 */
FAILURE(BGl_string2777z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_40);} 
{ /* Ieee/port.scm 853 */
 bool_t BgL_test3494z00_5689;
{ /* Ieee/port.scm 853 */
 obj_t BgL_arg2069z00_2680;
BgL_arg2069z00_2680 = 
BGL_EXITD_PROTECT(BgL_exitd1095z00_1581); 
BgL_test3494z00_5689 = 
PAIRP(BgL_arg2069z00_2680); } 
if(BgL_test3494z00_5689)
{ /* Ieee/port.scm 853 */
 obj_t BgL_arg2067z00_2681;
{ /* Ieee/port.scm 853 */
 obj_t BgL_arg2068z00_2682;
BgL_arg2068z00_2682 = 
BGL_EXITD_PROTECT(BgL_exitd1095z00_1581); 
{ /* Ieee/port.scm 853 */
 obj_t BgL_pairz00_2683;
if(
PAIRP(BgL_arg2068z00_2682))
{ /* Ieee/port.scm 853 */
BgL_pairz00_2683 = BgL_arg2068z00_2682; }  else 
{ 
 obj_t BgL_auxz00_5695;
BgL_auxz00_5695 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37954L), BGl_string2778z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2682); 
FAILURE(BgL_auxz00_5695,BFALSE,BFALSE);} 
BgL_arg2067z00_2681 = 
CDR(BgL_pairz00_2683); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1095z00_1581, BgL_arg2067z00_2681); BUNSPEC; }  else 
{ /* Ieee/port.scm 853 */BFALSE; } } 
BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31464ze3z87_3614); BgL_tmp1097z00_1583; } } } 
{ /* Ieee/port.scm 860 */
 obj_t BgL_aux2254z00_4039;
BgL_aux2254z00_4039 = 
CELL_REF(BgL_resz00_3623); 
if(
STRINGP(BgL_aux2254z00_4039))
{ /* Ieee/port.scm 860 */
return BgL_aux2254z00_4039;}  else 
{ 
 obj_t BgL_auxz00_5704;
BgL_auxz00_5704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(38147L), BGl_string2778z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2254z00_4039); 
FAILURE(BgL_auxz00_5704,BFALSE,BFALSE);} } } } } } } } 

}



/* &with-error-to-string */
obj_t BGl_z62withzd2errorzd2tozd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3615, obj_t BgL_thunkz00_3616)
{
{ /* Ieee/port.scm 847 */
{ /* Ieee/port.scm 848 */
 obj_t BgL_auxz00_5708;
if(
PROCEDUREP(BgL_thunkz00_3616))
{ /* Ieee/port.scm 848 */
BgL_auxz00_5708 = BgL_thunkz00_3616
; }  else 
{ 
 obj_t BgL_auxz00_5711;
BgL_auxz00_5711 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(37772L), BGl_string2779z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3616); 
FAILURE(BgL_auxz00_5711,BFALSE,BFALSE);} 
return 
BGl_withzd2errorzd2tozd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5708);} } 

}



/* &<@anonymous:1464> */
obj_t BGl_z62zc3z04anonymousza31464ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3617)
{
{ /* Ieee/port.scm 853 */
{ /* Ieee/port.scm 858 */
 obj_t BgL_denvz00_3618; obj_t BgL_oldzd2errorzd2portz00_3619; obj_t BgL_portz00_3620; obj_t BgL_resz00_3621;
BgL_denvz00_3618 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3617, 
(int)(0L))); 
BgL_oldzd2errorzd2portz00_3619 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3617, 
(int)(1L))); 
BgL_portz00_3620 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3617, 
(int)(2L))); 
BgL_resz00_3621 = 
PROCEDURE_REF(BgL_envz00_3617, 
(int)(3L)); 
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3618, BgL_oldzd2errorzd2portz00_3619); BUNSPEC; 
{ /* Ieee/port.scm 859 */
 obj_t BgL_auxz00_4534;
BgL_auxz00_4534 = 
bgl_close_output_port(BgL_portz00_3620); 
return 
CELL_SET(BgL_resz00_3621, BgL_auxz00_4534);} } } 

}



/* with-error-to-file */
BGL_EXPORTED_DEF obj_t BGl_withzd2errorzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_41, obj_t BgL_thunkz00_42)
{
{ /* Ieee/port.scm 869 */
{ /* Ieee/port.scm 870 */
 obj_t BgL_portz00_1587;
{ /* Ieee/port.scm 483 */

BgL_portz00_1587 = 
bgl_open_output_file(BgL_stringz00_41, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz))); } 
if(
OUTPUT_PORTP(BgL_portz00_1587))
{ /* Ieee/port.scm 872 */
 obj_t BgL_denvz00_1589;
BgL_denvz00_1589 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 872 */
 obj_t BgL_oldzd2outputzd2portz00_1590;
BgL_oldzd2outputzd2portz00_1590 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1589); 
{ /* Ieee/port.scm 873 */

{ /* Ieee/port.scm 874 */
 obj_t BgL_exitd1099z00_1591;
BgL_exitd1099z00_1591 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 879 */
 obj_t BgL_zc3z04anonymousza31466ze3z87_3625;
BgL_zc3z04anonymousza31466ze3z87_3625 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3625, 
(int)(0L), BgL_denvz00_1589); 
PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3625, 
(int)(1L), BgL_oldzd2outputzd2portz00_1590); 
PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3625, 
(int)(2L), BgL_portz00_1587); 
{ /* Ieee/port.scm 874 */
 obj_t BgL_arg2070z00_2687;
{ /* Ieee/port.scm 874 */
 obj_t BgL_arg2072z00_2688;
BgL_arg2072z00_2688 = 
BGL_EXITD_PROTECT(BgL_exitd1099z00_1591); 
BgL_arg2070z00_2687 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31466ze3z87_3625, BgL_arg2072z00_2688); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1099z00_1591, BgL_arg2070z00_2687); BUNSPEC; } 
{ /* Ieee/port.scm 876 */
 obj_t BgL_tmp1101z00_1593;
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1589, BgL_portz00_1587); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_42, 0))
{ /* Ieee/port.scm 877 */
BgL_tmp1101z00_1593 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_42); }  else 
{ /* Ieee/port.scm 877 */
FAILURE(BGl_string2780z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_42);} 
{ /* Ieee/port.scm 874 */
 bool_t BgL_test3500z00_5756;
{ /* Ieee/port.scm 874 */
 obj_t BgL_arg2069z00_2690;
BgL_arg2069z00_2690 = 
BGL_EXITD_PROTECT(BgL_exitd1099z00_1591); 
BgL_test3500z00_5756 = 
PAIRP(BgL_arg2069z00_2690); } 
if(BgL_test3500z00_5756)
{ /* Ieee/port.scm 874 */
 obj_t BgL_arg2067z00_2691;
{ /* Ieee/port.scm 874 */
 obj_t BgL_arg2068z00_2692;
BgL_arg2068z00_2692 = 
BGL_EXITD_PROTECT(BgL_exitd1099z00_1591); 
{ /* Ieee/port.scm 874 */
 obj_t BgL_pairz00_2693;
if(
PAIRP(BgL_arg2068z00_2692))
{ /* Ieee/port.scm 874 */
BgL_pairz00_2693 = BgL_arg2068z00_2692; }  else 
{ 
 obj_t BgL_auxz00_5762;
BgL_auxz00_5762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(38709L), BGl_string2781z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2692); 
FAILURE(BgL_auxz00_5762,BFALSE,BFALSE);} 
BgL_arg2067z00_2691 = 
CDR(BgL_pairz00_2693); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1099z00_1591, BgL_arg2067z00_2691); BUNSPEC; }  else 
{ /* Ieee/port.scm 874 */BFALSE; } } 
BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31466ze3z87_3625); 
return BgL_tmp1101z00_1593;} } } } } }  else 
{ /* Ieee/port.scm 871 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_symbol2782z00zz__r4_ports_6_10_1z00, BGl_string2715z00zz__r4_ports_6_10_1z00, BgL_stringz00_41);} } } 

}



/* &with-error-to-file */
obj_t BGl_z62withzd2errorzd2tozd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3626, obj_t BgL_stringz00_3627, obj_t BgL_thunkz00_3628)
{
{ /* Ieee/port.scm 869 */
{ /* Ieee/port.scm 870 */
 obj_t BgL_auxz00_5777; obj_t BgL_auxz00_5770;
if(
PROCEDUREP(BgL_thunkz00_3628))
{ /* Ieee/port.scm 870 */
BgL_auxz00_5777 = BgL_thunkz00_3628
; }  else 
{ 
 obj_t BgL_auxz00_5780;
BgL_auxz00_5780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(38543L), BGl_string2783z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3628); 
FAILURE(BgL_auxz00_5780,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3627))
{ /* Ieee/port.scm 870 */
BgL_auxz00_5770 = BgL_stringz00_3627
; }  else 
{ 
 obj_t BgL_auxz00_5773;
BgL_auxz00_5773 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(38543L), BGl_string2783z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3627); 
FAILURE(BgL_auxz00_5773,BFALSE,BFALSE);} 
return 
BGl_withzd2errorzd2tozd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5770, BgL_auxz00_5777);} } 

}



/* &<@anonymous:1466> */
obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3629)
{
{ /* Ieee/port.scm 874 */
{ /* Ieee/port.scm 879 */
 obj_t BgL_denvz00_3630; obj_t BgL_oldzd2outputzd2portz00_3631; obj_t BgL_portz00_3632;
BgL_denvz00_3630 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3629, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3631 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3629, 
(int)(1L))); 
BgL_portz00_3632 = 
PROCEDURE_REF(BgL_envz00_3629, 
(int)(2L)); 
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3630, BgL_oldzd2outputzd2portz00_3631); BUNSPEC; 
{ /* Ieee/port.scm 880 */
 obj_t BgL_portz00_4535;
if(
OUTPUT_PORTP(BgL_portz00_3632))
{ /* Ieee/port.scm 880 */
BgL_portz00_4535 = BgL_portz00_3632; }  else 
{ 
 obj_t BgL_auxz00_5796;
BgL_auxz00_5796 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(38878L), BGl_string2784z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3632); 
FAILURE(BgL_auxz00_5796,BFALSE,BFALSE);} 
return 
bgl_close_output_port(BgL_portz00_4535);} } } 

}



/* with-error-to-port */
BGL_EXPORTED_DEF obj_t BGl_withzd2errorzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_43, obj_t BgL_thunkz00_44)
{
{ /* Ieee/port.scm 889 */
{ /* Ieee/port.scm 890 */
 obj_t BgL_denvz00_1598;
BgL_denvz00_1598 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 890 */
 obj_t BgL_oldzd2outputzd2portz00_1599;
BgL_oldzd2outputzd2portz00_1599 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1598); 
{ /* Ieee/port.scm 891 */

{ /* Ieee/port.scm 892 */
 obj_t BgL_exitd1103z00_1600;
BgL_exitd1103z00_1600 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ 
 obj_t BgL_zc3z04anonymousza31467ze3z87_3633;
BgL_zc3z04anonymousza31467ze3z87_3633 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31467ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31467ze3z87_3633, 
(int)(0L), BgL_denvz00_1598); 
PROCEDURE_SET(BgL_zc3z04anonymousza31467ze3z87_3633, 
(int)(1L), BgL_oldzd2outputzd2portz00_1599); 
{ /* Ieee/port.scm 892 */
 obj_t BgL_arg2070z00_2694;
{ /* Ieee/port.scm 892 */
 obj_t BgL_arg2072z00_2695;
BgL_arg2072z00_2695 = 
BGL_EXITD_PROTECT(BgL_exitd1103z00_1600); 
BgL_arg2070z00_2694 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31467ze3z87_3633, BgL_arg2072z00_2695); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_1600, BgL_arg2070z00_2694); BUNSPEC; } 
{ /* Ieee/port.scm 894 */
 obj_t BgL_tmp1105z00_1602;
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1598, BgL_portz00_43); BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_44, 0))
{ /* Ieee/port.scm 895 */
BgL_tmp1105z00_1602 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_44); }  else 
{ /* Ieee/port.scm 895 */
FAILURE(BGl_string2785z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_44);} 
{ /* Ieee/port.scm 892 */
 bool_t BgL_test3506z00_5821;
{ /* Ieee/port.scm 892 */
 obj_t BgL_arg2069z00_2697;
BgL_arg2069z00_2697 = 
BGL_EXITD_PROTECT(BgL_exitd1103z00_1600); 
BgL_test3506z00_5821 = 
PAIRP(BgL_arg2069z00_2697); } 
if(BgL_test3506z00_5821)
{ /* Ieee/port.scm 892 */
 obj_t BgL_arg2067z00_2698;
{ /* Ieee/port.scm 892 */
 obj_t BgL_arg2068z00_2699;
BgL_arg2068z00_2699 = 
BGL_EXITD_PROTECT(BgL_exitd1103z00_1600); 
{ /* Ieee/port.scm 892 */
 obj_t BgL_pairz00_2700;
if(
PAIRP(BgL_arg2068z00_2699))
{ /* Ieee/port.scm 892 */
BgL_pairz00_2700 = BgL_arg2068z00_2699; }  else 
{ 
 obj_t BgL_auxz00_5827;
BgL_auxz00_5827 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39359L), BGl_string2786z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2699); 
FAILURE(BgL_auxz00_5827,BFALSE,BFALSE);} 
BgL_arg2067z00_2698 = 
CDR(BgL_pairz00_2700); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_1600, BgL_arg2067z00_2698); BUNSPEC; }  else 
{ /* Ieee/port.scm 892 */BFALSE; } } 
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1598, BgL_oldzd2outputzd2portz00_1599); BUNSPEC; 
return BgL_tmp1105z00_1602;} } } } } } } 

}



/* &with-error-to-port */
obj_t BGl_z62withzd2errorzd2tozd2portzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3634, obj_t BgL_portz00_3635, obj_t BgL_thunkz00_3636)
{
{ /* Ieee/port.scm 889 */
{ /* Ieee/port.scm 890 */
 obj_t BgL_auxz00_5841; obj_t BgL_auxz00_5834;
if(
PROCEDUREP(BgL_thunkz00_3636))
{ /* Ieee/port.scm 890 */
BgL_auxz00_5841 = BgL_thunkz00_3636
; }  else 
{ 
 obj_t BgL_auxz00_5844;
BgL_auxz00_5844 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39266L), BGl_string2787z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3636); 
FAILURE(BgL_auxz00_5844,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_3635))
{ /* Ieee/port.scm 890 */
BgL_auxz00_5834 = BgL_portz00_3635
; }  else 
{ 
 obj_t BgL_auxz00_5837;
BgL_auxz00_5837 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39266L), BGl_string2787z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3635); 
FAILURE(BgL_auxz00_5837,BFALSE,BFALSE);} 
return 
BGl_withzd2errorzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_auxz00_5834, BgL_auxz00_5841);} } 

}



/* &<@anonymous:1467> */
obj_t BGl_z62zc3z04anonymousza31467ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3637)
{
{ /* Ieee/port.scm 892 */
{ 
 obj_t BgL_denvz00_3638; obj_t BgL_oldzd2outputzd2portz00_3639;
BgL_denvz00_3638 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3637, 
(int)(0L))); 
BgL_oldzd2outputzd2portz00_3639 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3637, 
(int)(1L))); 
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3638, BgL_oldzd2outputzd2portz00_3639); 
return BUNSPEC;} } 

}



/* with-error-to-procedure */
BGL_EXPORTED_DEF obj_t BGl_withzd2errorzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(obj_t BgL_procz00_45, obj_t BgL_thunkz00_46)
{
{ /* Ieee/port.scm 901 */
{ /* Ieee/port.scm 902 */
 obj_t BgL_portz00_1605;
{ /* Ieee/port.scm 486 */

BgL_portz00_1605 = 
BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(BgL_procz00_45, BGl_proc2788z00zz__r4_ports_6_10_1z00, BTRUE, BGl_proc2789z00zz__r4_ports_6_10_1z00); } 
{ /* Ieee/port.scm 902 */
 obj_t BgL_denvz00_1606;
BgL_denvz00_1606 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 903 */
 obj_t BgL_oldzd2errorzd2portz00_1607;
BgL_oldzd2errorzd2portz00_1607 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_denvz00_1606); 
{ /* Ieee/port.scm 904 */
 obj_t BgL_resz00_3654;
BgL_resz00_3654 = 
MAKE_CELL(BUNSPEC); 
{ /* Ieee/port.scm 905 */

{ /* Ieee/port.scm 906 */
 obj_t BgL_exitd1107z00_1609;
BgL_exitd1107z00_1609 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Ieee/port.scm 911 */
 obj_t BgL_zc3z04anonymousza31468ze3z87_3640;
BgL_zc3z04anonymousza31468ze3z87_3640 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31468ze3ze5zz__r4_ports_6_10_1z00, 
(int)(0L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31468ze3z87_3640, 
(int)(0L), BgL_denvz00_1606); 
PROCEDURE_SET(BgL_zc3z04anonymousza31468ze3z87_3640, 
(int)(1L), BgL_oldzd2errorzd2portz00_1607); 
PROCEDURE_SET(BgL_zc3z04anonymousza31468ze3z87_3640, 
(int)(2L), BgL_portz00_1605); 
PROCEDURE_SET(BgL_zc3z04anonymousza31468ze3z87_3640, 
(int)(3L), 
((obj_t)BgL_resz00_3654)); 
{ /* Ieee/port.scm 906 */
 obj_t BgL_arg2070z00_2702;
{ /* Ieee/port.scm 906 */
 obj_t BgL_arg2072z00_2703;
BgL_arg2072z00_2703 = 
BGL_EXITD_PROTECT(BgL_exitd1107z00_1609); 
BgL_arg2070z00_2702 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31468ze3z87_3640, BgL_arg2072z00_2703); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1107z00_1609, BgL_arg2070z00_2702); BUNSPEC; } 
{ /* Ieee/port.scm 908 */
 obj_t BgL_tmp1109z00_1611;
{ /* Ieee/port.scm 908 */
 obj_t BgL_tmpz00_5875;
if(
OUTPUT_PORTP(BgL_portz00_1605))
{ /* Ieee/port.scm 908 */
BgL_tmpz00_5875 = BgL_portz00_1605
; }  else 
{ 
 obj_t BgL_auxz00_5878;
BgL_auxz00_5878 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39981L), BGl_string2790z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_1605); 
FAILURE(BgL_auxz00_5878,BFALSE,BFALSE);} 
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_1606, BgL_tmpz00_5875); } BUNSPEC; 
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_46, 0))
{ /* Ieee/port.scm 909 */
BgL_tmp1109z00_1611 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_46); }  else 
{ /* Ieee/port.scm 909 */
FAILURE(BGl_string2791z00zz__r4_ports_6_10_1z00,BGl_list2741z00zz__r4_ports_6_10_1z00,BgL_thunkz00_46);} 
{ /* Ieee/port.scm 906 */
 bool_t BgL_test3512z00_5889;
{ /* Ieee/port.scm 906 */
 obj_t BgL_arg2069z00_2705;
BgL_arg2069z00_2705 = 
BGL_EXITD_PROTECT(BgL_exitd1107z00_1609); 
BgL_test3512z00_5889 = 
PAIRP(BgL_arg2069z00_2705); } 
if(BgL_test3512z00_5889)
{ /* Ieee/port.scm 906 */
 obj_t BgL_arg2067z00_2706;
{ /* Ieee/port.scm 906 */
 obj_t BgL_arg2068z00_2707;
BgL_arg2068z00_2707 = 
BGL_EXITD_PROTECT(BgL_exitd1107z00_1609); 
{ /* Ieee/port.scm 906 */
 obj_t BgL_pairz00_2708;
if(
PAIRP(BgL_arg2068z00_2707))
{ /* Ieee/port.scm 906 */
BgL_pairz00_2708 = BgL_arg2068z00_2707; }  else 
{ 
 obj_t BgL_auxz00_5895;
BgL_auxz00_5895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39919L), BGl_string2790z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2707); 
FAILURE(BgL_auxz00_5895,BFALSE,BFALSE);} 
BgL_arg2067z00_2706 = 
CDR(BgL_pairz00_2708); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1107z00_1609, BgL_arg2067z00_2706); BUNSPEC; }  else 
{ /* Ieee/port.scm 906 */BFALSE; } } 
BGl_z62zc3z04anonymousza31468ze3ze5zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31468ze3z87_3640); BgL_tmp1109z00_1611; } } } 
return 
CELL_REF(BgL_resz00_3654);} } } } } } 

}



/* &with-error-to-procedure */
obj_t BGl_z62withzd2errorzd2tozd2procedurezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3643, obj_t BgL_procz00_3644, obj_t BgL_thunkz00_3645)
{
{ /* Ieee/port.scm 901 */
{ /* Ieee/port.scm 902 */
 obj_t BgL_auxz00_5909; obj_t BgL_auxz00_5902;
if(
PROCEDUREP(BgL_thunkz00_3645))
{ /* Ieee/port.scm 902 */
BgL_auxz00_5909 = BgL_thunkz00_3645
; }  else 
{ 
 obj_t BgL_auxz00_5912;
BgL_auxz00_5912 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39766L), BGl_string2792z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_thunkz00_3645); 
FAILURE(BgL_auxz00_5912,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_procz00_3644))
{ /* Ieee/port.scm 902 */
BgL_auxz00_5902 = BgL_procz00_3644
; }  else 
{ 
 obj_t BgL_auxz00_5905;
BgL_auxz00_5905 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(39766L), BGl_string2792z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3644); 
FAILURE(BgL_auxz00_5905,BFALSE,BFALSE);} 
return 
BGl_withzd2errorzd2tozd2procedurezd2zz__r4_ports_6_10_1z00(BgL_auxz00_5902, BgL_auxz00_5909);} } 

}



/* &<@anonymous:1468> */
obj_t BGl_z62zc3z04anonymousza31468ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3646)
{
{ /* Ieee/port.scm 906 */
{ /* Ieee/port.scm 911 */
 obj_t BgL_denvz00_3647; obj_t BgL_oldzd2errorzd2portz00_3648; obj_t BgL_portz00_3649; obj_t BgL_resz00_3650;
BgL_denvz00_3647 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3646, 
(int)(0L))); 
BgL_oldzd2errorzd2portz00_3648 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3646, 
(int)(1L))); 
BgL_portz00_3649 = 
PROCEDURE_REF(BgL_envz00_3646, 
(int)(2L)); 
BgL_resz00_3650 = 
PROCEDURE_REF(BgL_envz00_3646, 
(int)(3L)); 
BGL_ENV_CURRENT_ERROR_PORT_SET(BgL_denvz00_3647, BgL_oldzd2errorzd2portz00_3648); BUNSPEC; 
{ /* Ieee/port.scm 912 */
 obj_t BgL_auxz00_4536;
{ /* Ieee/port.scm 912 */
 obj_t BgL_portz00_4537;
if(
OUTPUT_PORTP(BgL_portz00_3649))
{ /* Ieee/port.scm 912 */
BgL_portz00_4537 = BgL_portz00_3649; }  else 
{ 
 obj_t BgL_auxz00_5930;
BgL_auxz00_5930 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(40097L), BGl_string2793z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3649); 
FAILURE(BgL_auxz00_5930,BFALSE,BFALSE);} 
BgL_auxz00_4536 = 
bgl_close_output_port(BgL_portz00_4537); } 
return 
CELL_SET(BgL_resz00_3650, BgL_auxz00_4536);} } } 

}



/* &close2100 */
obj_t BGl_z62close2100z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3652)
{
{ /* Ieee/port.scm 486 */
return 
BBOOL(((bool_t)0));} 

}



/* &flush2101 */
obj_t BGl_z62flush2101z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3653)
{
{ /* Ieee/port.scm 486 */
return 
BBOOL(((bool_t)0));} 

}



/* input-port-protocol */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2protocolz00zz__r4_ports_6_10_1z00(obj_t BgL_prototcolz00_47)
{
{ /* Ieee/port.scm 946 */
{ /* Ieee/port.scm 947 */
 obj_t BgL_cellz00_1623;
{ /* Ieee/port.scm 947 */
 obj_t BgL_top3518z00_5938;
BgL_top3518z00_5938 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00); 
BGL_EXITD_PUSH_PROTECT(BgL_top3518z00_5938, BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00); BUNSPEC; 
{ /* Ieee/port.scm 947 */
 obj_t BgL_tmp3517z00_5937;
{ /* Ieee/port.scm 948 */
 obj_t BgL_auxz00_5942;
{ /* Ieee/port.scm 948 */
 obj_t BgL_aux2285z00_4073;
BgL_aux2285z00_4073 = BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00; 
{ /* Ieee/port.scm 948 */
 bool_t BgL_test3519z00_5943;
if(
PAIRP(BgL_aux2285z00_4073))
{ /* Ieee/port.scm 948 */
BgL_test3519z00_5943 = ((bool_t)1)
; }  else 
{ /* Ieee/port.scm 948 */
BgL_test3519z00_5943 = 
NULLP(BgL_aux2285z00_4073)
; } 
if(BgL_test3519z00_5943)
{ /* Ieee/port.scm 948 */
BgL_auxz00_5942 = BgL_aux2285z00_4073
; }  else 
{ 
 obj_t BgL_auxz00_5947;
BgL_auxz00_5947 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(41815L), BGl_string2794z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_aux2285z00_4073); 
FAILURE(BgL_auxz00_5947,BFALSE,BFALSE);} } } 
BgL_tmp3517z00_5937 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_prototcolz00_47, BgL_auxz00_5942); } 
BGL_EXITD_POP_PROTECT(BgL_top3518z00_5938); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00); 
BgL_cellz00_1623 = BgL_tmp3517z00_5937; } } 
if(
PAIRP(BgL_cellz00_1623))
{ /* Ieee/port.scm 949 */
return 
CDR(BgL_cellz00_1623);}  else 
{ /* Ieee/port.scm 949 */
return BFALSE;} } } 

}



/* &input-port-protocol */
obj_t BGl_z62inputzd2portzd2protocolz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3656, obj_t BgL_prototcolz00_3657)
{
{ /* Ieee/port.scm 946 */
return 
BGl_inputzd2portzd2protocolz00zz__r4_ports_6_10_1z00(BgL_prototcolz00_3657);} 

}



/* input-port-protocol-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2protocolzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_protocolz00_48, obj_t BgL_openz00_49)
{
{ /* Ieee/port.scm 956 */
{ /* Ieee/port.scm 957 */
 obj_t BgL_top3523z00_5959;
BgL_top3523z00_5959 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00); 
BGL_EXITD_PUSH_PROTECT(BgL_top3523z00_5959, BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00); BUNSPEC; 
{ /* Ieee/port.scm 957 */
 obj_t BgL_tmp3522z00_5958;
{ /* Ieee/port.scm 958 */
 bool_t BgL_test3524z00_5963;
if(
PROCEDUREP(BgL_openz00_49))
{ /* Ieee/port.scm 958 */
BgL_test3524z00_5963 = 
PROCEDURE_CORRECT_ARITYP(BgL_openz00_49, 
(int)(3L))
; }  else 
{ /* Ieee/port.scm 958 */
BgL_test3524z00_5963 = ((bool_t)0)
; } 
if(BgL_test3524z00_5963)
{ /* Ieee/port.scm 958 */BFALSE; }  else 
{ /* Ieee/port.scm 958 */
BGl_errorz00zz__errorz00(BGl_string2796z00zz__r4_ports_6_10_1z00, BGl_string2797z00zz__r4_ports_6_10_1z00, BgL_protocolz00_48); } } 
{ /* Ieee/port.scm 962 */
 obj_t BgL_cz00_1628;
{ /* Ieee/port.scm 962 */
 obj_t BgL_auxz00_5969;
{ /* Ieee/port.scm 962 */
 obj_t BgL_aux2287z00_4075;
BgL_aux2287z00_4075 = BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00; 
{ /* Ieee/port.scm 962 */
 bool_t BgL_test3526z00_5970;
if(
PAIRP(BgL_aux2287z00_4075))
{ /* Ieee/port.scm 962 */
BgL_test3526z00_5970 = ((bool_t)1)
; }  else 
{ /* Ieee/port.scm 962 */
BgL_test3526z00_5970 = 
NULLP(BgL_aux2287z00_4075)
; } 
if(BgL_test3526z00_5970)
{ /* Ieee/port.scm 962 */
BgL_auxz00_5969 = BgL_aux2287z00_4075
; }  else 
{ 
 obj_t BgL_auxz00_5974;
BgL_auxz00_5974 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(42393L), BGl_string2796z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_aux2287z00_4075); 
FAILURE(BgL_auxz00_5974,BFALSE,BFALSE);} } } 
BgL_cz00_1628 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_protocolz00_48, BgL_auxz00_5969); } 
if(
PAIRP(BgL_cz00_1628))
{ /* Ieee/port.scm 963 */
BgL_tmp3522z00_5958 = 
SET_CDR(BgL_cz00_1628, BgL_openz00_49); }  else 
{ /* Ieee/port.scm 966 */
 obj_t BgL_arg1477z00_1630;
BgL_arg1477z00_1630 = 
MAKE_YOUNG_PAIR(BgL_protocolz00_48, BgL_openz00_49); 
BgL_tmp3522z00_5958 = ( 
BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00 = 
MAKE_YOUNG_PAIR(BgL_arg1477z00_1630, BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00), BUNSPEC) ; } } 
BGL_EXITD_POP_PROTECT(BgL_top3523z00_5959); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2inputzd2portzd2protocolszd2mutexza2zd2zz__r4_ports_6_10_1z00); BgL_tmp3522z00_5958; } } 
return BgL_openz00_49;} 

}



/* &input-port-protocol-set! */
obj_t BGl_z62inputzd2portzd2protocolzd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3658, obj_t BgL_protocolz00_3659, obj_t BgL_openz00_3660)
{
{ /* Ieee/port.scm 956 */
return 
BGl_inputzd2portzd2protocolzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_protocolz00_3659, BgL_openz00_3660);} 

}



/* get-port-buffer */
BGL_EXPORTED_DEF obj_t BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t BgL_whoz00_50, obj_t BgL_bufinfoz00_51, int BgL_defsiza7za7_52)
{
{ /* Ieee/port.scm 972 */
if(
(BgL_bufinfoz00_51==BTRUE))
{ /* Ieee/port.scm 974 */
return 
make_string_sans_fill(
(long)(BgL_defsiza7za7_52));}  else 
{ /* Ieee/port.scm 974 */
if(
(BgL_bufinfoz00_51==BFALSE))
{ /* Ieee/port.scm 976 */
return 
make_string_sans_fill(2L);}  else 
{ /* Ieee/port.scm 976 */
if(
STRINGP(BgL_bufinfoz00_51))
{ /* Ieee/port.scm 978 */
return BgL_bufinfoz00_51;}  else 
{ /* Ieee/port.scm 978 */
if(
INTEGERP(BgL_bufinfoz00_51))
{ /* Ieee/port.scm 980 */
if(
(
(long)CINT(BgL_bufinfoz00_51)>=2L))
{ /* Ieee/port.scm 981 */
return 
make_string_sans_fill(
(long)CINT(BgL_bufinfoz00_51));}  else 
{ /* Ieee/port.scm 981 */
return 
make_string_sans_fill(2L);} }  else 
{ /* Ieee/port.scm 985 */
 obj_t BgL_aux2289z00_4077;
BgL_aux2289z00_4077 = 
BGl_errorz00zz__errorz00(BgL_whoz00_50, BGl_string2798z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_51); 
if(
STRINGP(BgL_aux2289z00_4077))
{ /* Ieee/port.scm 985 */
return BgL_aux2289z00_4077;}  else 
{ 
 obj_t BgL_auxz00_6007;
BgL_auxz00_6007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43135L), BGl_string2799z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2289z00_4077); 
FAILURE(BgL_auxz00_6007,BFALSE,BFALSE);} } } } } } 

}



/* &get-port-buffer */
obj_t BGl_z62getzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3661, obj_t BgL_whoz00_3662, obj_t BgL_bufinfoz00_3663, obj_t BgL_defsiza7za7_3664)
{
{ /* Ieee/port.scm 972 */
{ /* Ieee/port.scm 974 */
 int BgL_auxz00_6011;
{ /* Ieee/port.scm 974 */
 obj_t BgL_tmpz00_6012;
if(
INTEGERP(BgL_defsiza7za7_3664))
{ /* Ieee/port.scm 974 */
BgL_tmpz00_6012 = BgL_defsiza7za7_3664
; }  else 
{ 
 obj_t BgL_auxz00_6015;
BgL_auxz00_6015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(42843L), BGl_string2800z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_defsiza7za7_3664); 
FAILURE(BgL_auxz00_6015,BFALSE,BFALSE);} 
BgL_auxz00_6011 = 
CINT(BgL_tmpz00_6012); } 
return 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BgL_whoz00_3662, BgL_bufinfoz00_3663, BgL_auxz00_6011);} } 

}



/* &%open-input-file */
obj_t BGl_z62z52openzd2inputzd2filez30zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3450, obj_t BgL_stringz00_3451, obj_t BgL_bufz00_3452, obj_t BgL_tmtz00_3453)
{
{ /* Ieee/port.scm 990 */
{ /* Ieee/port.scm 991 */
 obj_t BgL_auxz00_6028; obj_t BgL_tmpz00_6021;
if(
STRINGP(BgL_bufz00_3452))
{ /* Ieee/port.scm 991 */
BgL_auxz00_6028 = BgL_bufz00_3452
; }  else 
{ 
 obj_t BgL_auxz00_6031;
BgL_auxz00_6031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43468L), BGl_string2802z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_bufz00_3452); 
FAILURE(BgL_auxz00_6031,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_3451))
{ /* Ieee/port.scm 991 */
BgL_tmpz00_6021 = BgL_stringz00_3451
; }  else 
{ 
 obj_t BgL_auxz00_6024;
BgL_auxz00_6024 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43461L), BGl_string2802z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3451); 
FAILURE(BgL_auxz00_6024,BFALSE,BFALSE);} 
return 
bgl_open_input_file(BgL_tmpz00_6021, BgL_auxz00_6028);} } 

}



/* &%open-input-descriptor */
obj_t BGl_z62z52openzd2inputzd2descriptorz30zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3482, obj_t BgL_stringz00_3483, obj_t BgL_bufz00_3484, obj_t BgL_tmtz00_3485)
{
{ /* Ieee/port.scm 996 */
{ /* Ieee/port.scm 997 */
 long BgL_fdz00_4540;
{ /* Ieee/port.scm 997 */
 obj_t BgL_arg1481z00_4541;
{ /* Ieee/port.scm 997 */
 long BgL_endz00_4542;
{ /* Ieee/port.scm 997 */
 obj_t BgL_stringz00_4543;
if(
STRINGP(BgL_stringz00_3483))
{ /* Ieee/port.scm 997 */
BgL_stringz00_4543 = BgL_stringz00_3483; }  else 
{ 
 obj_t BgL_auxz00_6038;
BgL_auxz00_6038 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43775L), BGl_string2803z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3483); 
FAILURE(BgL_auxz00_6038,BFALSE,BFALSE);} 
BgL_endz00_4542 = 
STRING_LENGTH(BgL_stringz00_4543); } 
{ /* Ieee/port.scm 997 */

{ /* Ieee/port.scm 997 */
 obj_t BgL_auxz00_6043;
if(
STRINGP(BgL_stringz00_3483))
{ /* Ieee/port.scm 997 */
BgL_auxz00_6043 = BgL_stringz00_3483
; }  else 
{ 
 obj_t BgL_auxz00_6046;
BgL_auxz00_6046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43775L), BGl_string2803z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3483); 
FAILURE(BgL_auxz00_6046,BFALSE,BFALSE);} 
BgL_arg1481z00_4541 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_auxz00_6043, 3L, BgL_endz00_4542); } } } 
{ /* Ieee/port.scm 997 */
 char * BgL_tmpz00_6051;
BgL_tmpz00_6051 = 
BSTRING_TO_STRING(BgL_arg1481z00_4541); 
BgL_fdz00_4540 = 
BGL_STRTOL(BgL_tmpz00_6051, 0L, 10L); } } 
{ /* Ieee/port.scm 998 */
 obj_t BgL_tmpz00_6054;
if(
STRINGP(BgL_bufz00_3484))
{ /* Ieee/port.scm 998 */
BgL_tmpz00_6054 = BgL_bufz00_3484
; }  else 
{ 
 obj_t BgL_auxz00_6058;
BgL_auxz00_6058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43832L), BGl_string2803z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_bufz00_3484); 
FAILURE(BgL_auxz00_6058,BFALSE,BFALSE);} 
return 
bgl_open_input_descriptor(
(int)(BgL_fdz00_4540), BgL_tmpz00_6054);} } } 

}



/* &%open-input-pipe */
obj_t BGl_z62z52openzd2inputzd2pipez30zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3454, obj_t BgL_stringz00_3455, obj_t BgL_bufinfoz00_3456, obj_t BgL_tmtz00_3457)
{
{ /* Ieee/port.scm 1003 */
{ /* Ieee/port.scm 1004 */
 obj_t BgL_bufz00_4545;
BgL_bufz00_4545 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2804z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_3456, 
(int)(1024L)); 
{ /* Ieee/port.scm 1005 */
 obj_t BgL_tmpz00_6065;
if(
STRINGP(BgL_stringz00_3455))
{ /* Ieee/port.scm 1005 */
BgL_tmpz00_6065 = BgL_stringz00_3455
; }  else 
{ 
 obj_t BgL_auxz00_6068;
BgL_auxz00_6068 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(44204L), BGl_string2805z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3455); 
FAILURE(BgL_auxz00_6068,BFALSE,BFALSE);} 
return 
bgl_open_input_pipe(BgL_tmpz00_6065, BgL_bufz00_4545);} } } 

}



/* &%open-input-resource */
obj_t BGl_z62z52openzd2inputzd2resourcez30zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3474, obj_t BgL_filez00_3475, obj_t BgL_bufinfoz00_3476, obj_t BgL_tmtz00_3477)
{
{ /* Ieee/port.scm 1010 */
{ /* Ieee/port.scm 1011 */
 obj_t BgL_bufz00_4546;
BgL_bufz00_4546 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2806z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_3476, 
(int)(default_io_bufsiz)); 
{ /* Ieee/port.scm 1012 */
 obj_t BgL_tmpz00_6075;
if(
STRINGP(BgL_filez00_3475))
{ /* Ieee/port.scm 1012 */
BgL_tmpz00_6075 = BgL_filez00_3475
; }  else 
{ 
 obj_t BgL_auxz00_6078;
BgL_auxz00_6078 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(44597L), BGl_string2807z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3475); 
FAILURE(BgL_auxz00_6078,BFALSE,BFALSE);} 
return 
bgl_open_input_resource(BgL_tmpz00_6075, BgL_bufz00_4546);} } } 

}



/* &%open-input-http-socket */
obj_t BGl_z62z52openzd2inputzd2httpzd2socketze2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3458, obj_t BgL_stringz00_3459, obj_t BgL_bufinfoz00_3460, obj_t BgL_timeoutz00_3461)
{
{ /* Ieee/port.scm 1017 */
{ /* Ieee/port.scm 1031 */
 obj_t BgL_protocolz00_4548;
BgL_protocolz00_4548 = 
BGl_urlzd2sanszd2protocolzd2parsezd2zz__urlz00(BgL_stringz00_3459, BGl_string2809z00zz__r4_ports_6_10_1z00); 
{ /* Ieee/port.scm 1032 */
 obj_t BgL_loginz00_4549; obj_t BgL_hostz00_4550; obj_t BgL_portz00_4551; obj_t BgL_abspathz00_4552;
{ /* Ieee/port.scm 1033 */
 obj_t BgL_tmpz00_4553;
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6084;
BgL_tmpz00_6084 = 
(int)(1L); 
BgL_tmpz00_4553 = 
BGL_MVALUES_VAL(BgL_tmpz00_6084); } 
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6087;
BgL_tmpz00_6087 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6087, BUNSPEC); } 
BgL_loginz00_4549 = BgL_tmpz00_4553; } 
{ /* Ieee/port.scm 1033 */
 obj_t BgL_tmpz00_4554;
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6090;
BgL_tmpz00_6090 = 
(int)(2L); 
BgL_tmpz00_4554 = 
BGL_MVALUES_VAL(BgL_tmpz00_6090); } 
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6093;
BgL_tmpz00_6093 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6093, BUNSPEC); } 
BgL_hostz00_4550 = BgL_tmpz00_4554; } 
{ /* Ieee/port.scm 1033 */
 obj_t BgL_tmpz00_4555;
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6096;
BgL_tmpz00_6096 = 
(int)(3L); 
BgL_tmpz00_4555 = 
BGL_MVALUES_VAL(BgL_tmpz00_6096); } 
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6099;
BgL_tmpz00_6099 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6099, BUNSPEC); } 
BgL_portz00_4551 = BgL_tmpz00_4555; } 
{ /* Ieee/port.scm 1033 */
 obj_t BgL_tmpz00_4556;
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6102;
BgL_tmpz00_6102 = 
(int)(4L); 
BgL_tmpz00_4556 = 
BGL_MVALUES_VAL(BgL_tmpz00_6102); } 
{ /* Ieee/port.scm 1033 */
 int BgL_tmpz00_6105;
BgL_tmpz00_6105 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6105, BUNSPEC); } 
BgL_abspathz00_4552 = BgL_tmpz00_4556; } 
return 
BGl_z62loopz62zz__r4_ports_6_10_1z00(BGl_proc2808z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_3460, BgL_timeoutz00_3461, BgL_portz00_4551, BgL_abspathz00_4552, BgL_loginz00_4549, BgL_hostz00_4550, BFALSE, BGl_list2810z00zz__r4_ports_6_10_1z00);} } } 

}



/* &loop */
obj_t BGl_z62loopz62zz__r4_ports_6_10_1z00(obj_t BgL_parserz00_3675, obj_t BgL_bufinfoz00_3674, obj_t BgL_timeoutz00_3673, obj_t BgL_portz00_3672, obj_t BgL_abspathz00_3671, obj_t BgL_loginz00_3670, obj_t BgL_hostz00_3669, obj_t BgL_ipz00_1649, obj_t BgL_headerz00_1650)
{
{ /* Ieee/port.scm 1033 */
{ /* Ieee/port.scm 1035 */
struct bgl_cell BgL_box3543_3875z00; 
 obj_t BgL_ipz00_3875;
BgL_ipz00_3875 = 
MAKE_CELL_STACK(BgL_ipz00_1649,BgL_box3543_3875z00); 
{ /* Ieee/port.scm 1035 */
 obj_t BgL_sockz00_1652;
{ /* Ieee/port.scm 1035 */
 obj_t BgL_methodz00_1709;
BgL_methodz00_1709 = BGl_symbol2820z00zz__r4_ports_6_10_1z00; 
BgL_sockz00_1652 = 
BGl_httpz00zz__httpz00(BNIL, BFALSE, BFALSE, BUNSPEC, BFALSE, BgL_headerz00_1650, BgL_hostz00_3669, BGl_string2823z00zz__r4_ports_6_10_1z00, BFALSE, BgL_loginz00_3670, BgL_methodz00_1709, BFALSE, BFALSE, BgL_abspathz00_3671, BgL_portz00_3672, BGl_symbol2822z00zz__r4_ports_6_10_1z00, BFALSE, BFALSE, BgL_timeoutz00_3673, BFALSE); } 
{ /* Ieee/port.scm 1035 */
 obj_t BgL_opz00_1653;
{ /* Ieee/port.scm 1041 */
 obj_t BgL_socketz00_2732;
if(
SOCKETP(BgL_sockz00_1652))
{ /* Ieee/port.scm 1041 */
BgL_socketz00_2732 = BgL_sockz00_1652; }  else 
{ 
 obj_t BgL_auxz00_6112;
BgL_auxz00_6112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45596L), BGl_string2824z00zz__r4_ports_6_10_1z00, BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_sockz00_1652); 
FAILURE(BgL_auxz00_6112,BFALSE,BFALSE);} 
BgL_opz00_1653 = 
SOCKET_OUTPUT(BgL_socketz00_2732); } 
{ /* Ieee/port.scm 1041 */

{ /* Ieee/port.scm 1042 */
 bool_t BgL_test3545z00_6117;
{ /* Ieee/port.scm 1042 */
 obj_t BgL_objz00_2733;
BgL_objz00_2733 = 
CELL_REF(BgL_ipz00_3875); 
BgL_test3545z00_6117 = 
INPUT_PORTP(BgL_objz00_2733); } 
if(BgL_test3545z00_6117)
{ /* Ieee/port.scm 1045 */
 obj_t BgL_arg1485z00_1655;
{ /* Ieee/port.scm 1045 */
 obj_t BgL_socketz00_2734;
if(
SOCKETP(BgL_sockz00_1652))
{ /* Ieee/port.scm 1045 */
BgL_socketz00_2734 = BgL_sockz00_1652; }  else 
{ 
 obj_t BgL_auxz00_6121;
BgL_auxz00_6121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45744L), BGl_string2824z00zz__r4_ports_6_10_1z00, BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_sockz00_1652); 
FAILURE(BgL_auxz00_6121,BFALSE,BFALSE);} 
BgL_arg1485z00_1655 = 
SOCKET_INPUT(BgL_socketz00_2734); } 
{ /* Ieee/port.scm 1045 */
 obj_t BgL_dstz00_2735;
{ /* Ieee/port.scm 1045 */
 obj_t BgL_aux2310z00_4098;
BgL_aux2310z00_4098 = 
CELL_REF(BgL_ipz00_3875); 
if(
INPUT_PORTP(BgL_aux2310z00_4098))
{ /* Ieee/port.scm 1045 */
BgL_dstz00_2735 = BgL_aux2310z00_4098; }  else 
{ 
 obj_t BgL_auxz00_6128;
BgL_auxz00_6128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45727L), BGl_string2824z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2310z00_4098); 
FAILURE(BgL_auxz00_6128,BFALSE,BFALSE);} } 
bgl_input_port_clone(BgL_dstz00_2735, BgL_arg1485z00_1655); } }  else 
{ /* Ieee/port.scm 1046 */
 obj_t BgL_auxz00_3876;
{ /* Ieee/port.scm 1046 */
 obj_t BgL_socketz00_2737;
if(
SOCKETP(BgL_sockz00_1652))
{ /* Ieee/port.scm 1046 */
BgL_socketz00_2737 = BgL_sockz00_1652; }  else 
{ 
 obj_t BgL_auxz00_6135;
BgL_auxz00_6135 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45776L), BGl_string2824z00zz__r4_ports_6_10_1z00, BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_sockz00_1652); 
FAILURE(BgL_auxz00_6135,BFALSE,BFALSE);} 
BgL_auxz00_3876 = 
SOCKET_INPUT(BgL_socketz00_2737); } 
CELL_SET(BgL_ipz00_3875, BgL_auxz00_3876); } } 
{ /* Ieee/port.scm 1049 */
 obj_t BgL_zc3z04anonymousza31487ze3z87_3665;
BgL_zc3z04anonymousza31487ze3z87_3665 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31487ze3ze5zz__r4_ports_6_10_1z00, 
(int)(1L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31487ze3z87_3665, 
(int)(0L), BgL_opz00_1653); 
PROCEDURE_SET(BgL_zc3z04anonymousza31487ze3z87_3665, 
(int)(1L), BgL_sockz00_1652); 
{ /* Ieee/port.scm 1047 */
 obj_t BgL_auxz00_6147;
{ /* Ieee/port.scm 1047 */
 obj_t BgL_aux2314z00_4102;
BgL_aux2314z00_4102 = 
CELL_REF(BgL_ipz00_3875); 
if(
INPUT_PORTP(BgL_aux2314z00_4102))
{ /* Ieee/port.scm 1047 */
BgL_auxz00_6147 = BgL_aux2314z00_4102
; }  else 
{ 
 obj_t BgL_auxz00_6150;
BgL_auxz00_6150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45817L), BGl_string2824z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2314z00_4102); 
FAILURE(BgL_auxz00_6150,BFALSE,BFALSE);} } 
BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_6147, BgL_zc3z04anonymousza31487ze3z87_3665); } } 
{ /* Ieee/port.scm 1053 */
 obj_t BgL_zc3z04anonymousza31489ze3z87_3666;
BgL_zc3z04anonymousza31489ze3z87_3666 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31489ze3ze5zz__r4_ports_6_10_1z00, 
(int)(2L), 
(int)(8L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(0L), BgL_hostz00_3669); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(1L), BgL_loginz00_3670); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(2L), BgL_abspathz00_3671); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(3L), BgL_portz00_3672); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(4L), BgL_timeoutz00_3673); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(5L), BgL_bufinfoz00_3674); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(6L), BgL_parserz00_3675); 
PROCEDURE_SET(BgL_zc3z04anonymousza31489ze3z87_3666, 
(int)(7L), BgL_sockz00_1652); 
{ /* Ieee/port.scm 1051 */
 obj_t BgL_auxz00_6174;
{ /* Ieee/port.scm 1051 */
 obj_t BgL_aux2316z00_4104;
BgL_aux2316z00_4104 = 
CELL_REF(BgL_ipz00_3875); 
if(
INPUT_PORTP(BgL_aux2316z00_4104))
{ /* Ieee/port.scm 1051 */
BgL_auxz00_6174 = BgL_aux2316z00_4104
; }  else 
{ 
 obj_t BgL_auxz00_6177;
BgL_auxz00_6177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45921L), BGl_string2824z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2316z00_4104); 
FAILURE(BgL_auxz00_6177,BFALSE,BFALSE);} } 
BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_6174, BgL_zc3z04anonymousza31489ze3z87_3666); } } 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_env1120z00_1676;
BgL_env1120z00_1676 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ 
 obj_t BgL_ez00_1690;
{ /* Ieee/port.scm 1056 */
 obj_t BgL_cell1115z00_1677;
BgL_cell1115z00_1677 = 
MAKE_STACK_CELL(BUNSPEC); 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_val1119z00_1678;
BgL_val1119z00_1678 = 
BGl_zc3z04exitza31502ze3ze70z60zz__r4_ports_6_10_1z00(BgL_parserz00_3675, BgL_opz00_1653, BgL_ipz00_3875, BgL_cell1115z00_1677, BgL_env1120z00_1676); 
if(
(BgL_val1119z00_1678==BgL_cell1115z00_1677))
{ /* Ieee/port.scm 1056 */
{ /* Ieee/port.scm 1056 */
 int BgL_tmpz00_6187;
BgL_tmpz00_6187 = 
(int)(0L); 
BGL_SIGSETMASK(BgL_tmpz00_6187); } 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_arg1501z00_1679;
BgL_arg1501z00_1679 = 
CELL_REF(
((obj_t)BgL_val1119z00_1678)); 
BgL_ez00_1690 = BgL_arg1501z00_1679; 
{ /* Ieee/port.scm 1058 */
 obj_t BgL_socketz00_2741;
if(
SOCKETP(BgL_sockz00_1652))
{ /* Ieee/port.scm 1058 */
BgL_socketz00_2741 = BgL_sockz00_1652; }  else 
{ 
 obj_t BgL_auxz00_6194;
BgL_auxz00_6194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46165L), BGl_string2826z00zz__r4_ports_6_10_1z00, BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_sockz00_1652); 
FAILURE(BgL_auxz00_6194,BFALSE,BFALSE);} 
socket_close(BgL_socketz00_2741); } 
{ /* Ieee/port.scm 1059 */
 bool_t BgL_test3553z00_6199;
{ /* Ieee/port.scm 1059 */
 obj_t BgL_classz00_2742;
BgL_classz00_2742 = BGl_z62httpzd2redirectionzb0zz__httpz00; 
if(
BGL_OBJECTP(BgL_ez00_1690))
{ /* Ieee/port.scm 1059 */
 BgL_objectz00_bglt BgL_arg2033z00_2744;
BgL_arg2033z00_2744 = 
(BgL_objectz00_bglt)(BgL_ez00_1690); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Ieee/port.scm 1059 */
 long BgL_idxz00_2750;
BgL_idxz00_2750 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2033z00_2744); 
{ /* Ieee/port.scm 1059 */
 obj_t BgL_arg2025z00_2751;
{ /* Ieee/port.scm 1059 */
 long BgL_arg2026z00_2752;
BgL_arg2026z00_2752 = 
(BgL_idxz00_2750+3L); 
{ /* Ieee/port.scm 1059 */
 obj_t BgL_vectorz00_2754;
{ /* Ieee/port.scm 1059 */
 obj_t BgL_aux2320z00_4108;
BgL_aux2320z00_4108 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2320z00_4108))
{ /* Ieee/port.scm 1059 */
BgL_vectorz00_2754 = BgL_aux2320z00_4108; }  else 
{ 
 obj_t BgL_auxz00_6209;
BgL_auxz00_6209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46181L), BGl_string2826z00zz__r4_ports_6_10_1z00, BGl_string2827z00zz__r4_ports_6_10_1z00, BgL_aux2320z00_4108); 
FAILURE(BgL_auxz00_6209,BFALSE,BFALSE);} } 
{ /* Ieee/port.scm 1059 */
 bool_t BgL_test3557z00_6213;
{ /* Ieee/port.scm 1059 */
 long BgL_tmpz00_6214;
BgL_tmpz00_6214 = 
VECTOR_LENGTH(BgL_vectorz00_2754); 
BgL_test3557z00_6213 = 
BOUND_CHECK(BgL_arg2026z00_2752, BgL_tmpz00_6214); } 
if(BgL_test3557z00_6213)
{ /* Ieee/port.scm 1059 */
BgL_arg2025z00_2751 = 
VECTOR_REF(BgL_vectorz00_2754,BgL_arg2026z00_2752); }  else 
{ 
 obj_t BgL_auxz00_6218;
BgL_auxz00_6218 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46181L), BGl_string2828z00zz__r4_ports_6_10_1z00, BgL_vectorz00_2754, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2754)), 
(int)(BgL_arg2026z00_2752)); 
FAILURE(BgL_auxz00_6218,BFALSE,BFALSE);} } } } 
BgL_test3553z00_6199 = 
(BgL_arg2025z00_2751==BgL_classz00_2742); } }  else 
{ /* Ieee/port.scm 1059 */
 bool_t BgL_res2073z00_2775;
{ /* Ieee/port.scm 1059 */
 obj_t BgL_oclassz00_2758;
{ /* Ieee/port.scm 1059 */
 obj_t BgL_arg2041z00_2766; long BgL_arg2042z00_2767;
BgL_arg2041z00_2766 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Ieee/port.scm 1059 */
 long BgL_arg2044z00_2768;
BgL_arg2044z00_2768 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2033z00_2744); 
BgL_arg2042z00_2767 = 
(BgL_arg2044z00_2768-OBJECT_TYPE); } 
BgL_oclassz00_2758 = 
VECTOR_REF(BgL_arg2041z00_2766,BgL_arg2042z00_2767); } 
{ /* Ieee/port.scm 1059 */
 bool_t BgL__ortest_1211z00_2759;
BgL__ortest_1211z00_2759 = 
(BgL_classz00_2742==BgL_oclassz00_2758); 
if(BgL__ortest_1211z00_2759)
{ /* Ieee/port.scm 1059 */
BgL_res2073z00_2775 = BgL__ortest_1211z00_2759; }  else 
{ /* Ieee/port.scm 1059 */
 long BgL_odepthz00_2760;
{ /* Ieee/port.scm 1059 */
 obj_t BgL_arg2030z00_2761;
BgL_arg2030z00_2761 = 
(BgL_oclassz00_2758); 
BgL_odepthz00_2760 = 
BGL_CLASS_DEPTH(BgL_arg2030z00_2761); } 
if(
(3L<BgL_odepthz00_2760))
{ /* Ieee/port.scm 1059 */
 obj_t BgL_arg2027z00_2763;
{ /* Ieee/port.scm 1059 */
 obj_t BgL_arg2029z00_2764;
BgL_arg2029z00_2764 = 
(BgL_oclassz00_2758); 
BgL_arg2027z00_2763 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2029z00_2764, 3L); } 
BgL_res2073z00_2775 = 
(BgL_arg2027z00_2763==BgL_classz00_2742); }  else 
{ /* Ieee/port.scm 1059 */
BgL_res2073z00_2775 = ((bool_t)0); } } } } 
BgL_test3553z00_6199 = BgL_res2073z00_2775; } }  else 
{ /* Ieee/port.scm 1059 */
BgL_test3553z00_6199 = ((bool_t)0)
; } } 
if(BgL_test3553z00_6199)
{ /* Ieee/port.scm 1060 */
 BgL_z62httpzd2redirectionzb0_bglt BgL_i1121z00_1693;
{ /* Ieee/port.scm 1061 */
 bool_t BgL_test3560z00_6239;
{ /* Ieee/port.scm 1061 */
 obj_t BgL_classz00_4558;
BgL_classz00_4558 = BGl_z62httpzd2redirectionzb0zz__httpz00; 
if(
BGL_OBJECTP(BgL_ez00_1690))
{ /* Ieee/port.scm 1061 */
 BgL_objectz00_bglt BgL_arg2036z00_4560; long BgL_arg2037z00_4561;
BgL_arg2036z00_4560 = 
(BgL_objectz00_bglt)(BgL_ez00_1690); 
BgL_arg2037z00_4561 = 
BGL_CLASS_DEPTH(BgL_classz00_4558); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Ieee/port.scm 1061 */
 long BgL_idxz00_4569;
BgL_idxz00_4569 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2036z00_4560); 
{ /* Ieee/port.scm 1061 */
 obj_t BgL_arg2025z00_4570;
{ /* Ieee/port.scm 1061 */
 long BgL_arg2026z00_4571;
BgL_arg2026z00_4571 = 
(BgL_idxz00_4569+BgL_arg2037z00_4561); 
BgL_arg2025z00_4570 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg2026z00_4571); } 
BgL_test3560z00_6239 = 
(BgL_arg2025z00_4570==BgL_classz00_4558); } }  else 
{ /* Ieee/port.scm 1061 */
 bool_t BgL_res2087z00_4576;
{ /* Ieee/port.scm 1061 */
 obj_t BgL_oclassz00_4580;
{ /* Ieee/port.scm 1061 */
 obj_t BgL_arg2041z00_4582; long BgL_arg2042z00_4583;
BgL_arg2041z00_4582 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Ieee/port.scm 1061 */
 long BgL_arg2044z00_4584;
BgL_arg2044z00_4584 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2036z00_4560); 
BgL_arg2042z00_4583 = 
(BgL_arg2044z00_4584-OBJECT_TYPE); } 
BgL_oclassz00_4580 = 
VECTOR_REF(BgL_arg2041z00_4582,BgL_arg2042z00_4583); } 
{ /* Ieee/port.scm 1061 */
 bool_t BgL__ortest_1211z00_4590;
BgL__ortest_1211z00_4590 = 
(BgL_classz00_4558==BgL_oclassz00_4580); 
if(BgL__ortest_1211z00_4590)
{ /* Ieee/port.scm 1061 */
BgL_res2087z00_4576 = BgL__ortest_1211z00_4590; }  else 
{ /* Ieee/port.scm 1061 */
 long BgL_odepthz00_4591;
{ /* Ieee/port.scm 1061 */
 obj_t BgL_arg2030z00_4592;
BgL_arg2030z00_4592 = 
(BgL_oclassz00_4580); 
BgL_odepthz00_4591 = 
BGL_CLASS_DEPTH(BgL_arg2030z00_4592); } 
if(
(BgL_arg2037z00_4561<BgL_odepthz00_4591))
{ /* Ieee/port.scm 1061 */
 obj_t BgL_arg2027z00_4596;
{ /* Ieee/port.scm 1061 */
 obj_t BgL_arg2029z00_4597;
BgL_arg2029z00_4597 = 
(BgL_oclassz00_4580); 
BgL_arg2027z00_4596 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2029z00_4597, BgL_arg2037z00_4561); } 
BgL_res2087z00_4576 = 
(BgL_arg2027z00_4596==BgL_classz00_4558); }  else 
{ /* Ieee/port.scm 1061 */
BgL_res2087z00_4576 = ((bool_t)0); } } } } 
BgL_test3560z00_6239 = BgL_res2087z00_4576; } }  else 
{ /* Ieee/port.scm 1061 */
BgL_test3560z00_6239 = ((bool_t)0)
; } } 
if(BgL_test3560z00_6239)
{ /* Ieee/port.scm 1061 */
BgL_i1121z00_1693 = 
((BgL_z62httpzd2redirectionzb0_bglt)BgL_ez00_1690); }  else 
{ 
 obj_t BgL_auxz00_6264;
BgL_auxz00_6264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46258L), BGl_string2826z00zz__r4_ports_6_10_1z00, BGl_string2829z00zz__r4_ports_6_10_1z00, BgL_ez00_1690); 
FAILURE(BgL_auxz00_6264,BFALSE,BFALSE);} } 
{ /* Ieee/port.scm 1061 */
 obj_t BgL_arg1506z00_1694;
BgL_arg1506z00_1694 = 
(((BgL_z62httpzd2redirectionzb0_bglt)COBJECT(BgL_i1121z00_1693))->BgL_urlz00); 
{ /* Ieee/port.scm 466 */

return 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_arg1506z00_1694, BgL_bufinfoz00_3674, 
BINT(5000000L));} } }  else 
{ /* Ieee/port.scm 1059 */
return BFALSE;} } } }  else 
{ /* Ieee/port.scm 1056 */
return BgL_val1119z00_1678;} } } } } } } } } } 

}



/* <@exit:1502>~0 */
obj_t BGl_zc3z04exitza31502ze3ze70z60zz__r4_ports_6_10_1z00(obj_t BgL_parserz00_3874, obj_t BgL_opz00_3873, obj_t BgL_ipz00_3872, obj_t BgL_cell1115z00_3871, obj_t BgL_env1120z00_3870)
{
{ /* Ieee/port.scm 1056 */
jmp_buf_t jmpbuf; 
 void * BgL_an_exit1125z00_1681;
if( SET_EXIT(BgL_an_exit1125z00_1681 ) ) { 
return 
BGL_EXIT_VALUE();
} else {
#if( SIGSETJMP_SAVESIGS == 0 )
  // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
  // bgl_restore_signal_handlers();
#endif

BgL_an_exit1125z00_1681 = 
(void *)jmpbuf; 
PUSH_ENV_EXIT(BgL_env1120z00_3870, BgL_an_exit1125z00_1681, 1L); 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_escape1116z00_1682;
BgL_escape1116z00_1682 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1120z00_3870); 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_res1128z00_1683;
{ /* Ieee/port.scm 1056 */
 obj_t BgL_ohs1112z00_1684;
BgL_ohs1112z00_1684 = 
BGL_ENV_ERROR_HANDLER_GET(BgL_env1120z00_3870); 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_hds1113z00_1685;
BgL_hds1113z00_1685 = 
MAKE_STACK_PAIR(BgL_escape1116z00_1682, BgL_cell1115z00_3871); 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1120z00_3870, BgL_hds1113z00_1685); BUNSPEC; 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_exitd1122z00_1686;
BgL_exitd1122z00_1686 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1120z00_3870); 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_tmp1124z00_1687;
{ /* Ieee/port.scm 1056 */
 obj_t BgL_arg1503z00_1689;
BgL_arg1503z00_1689 = 
BGL_EXITD_PROTECT(BgL_exitd1122z00_1686); 
BgL_tmp1124z00_1687 = 
MAKE_YOUNG_PAIR(BgL_ohs1112z00_1684, BgL_arg1503z00_1689); } 
{ /* Ieee/port.scm 1056 */

BGL_EXITD_PROTECT_SET(BgL_exitd1122z00_1686, BgL_tmp1124z00_1687); BUNSPEC; 
{ /* Ieee/port.scm 1062 */
 obj_t BgL_tmp1123z00_1688;
{ /* Ieee/port.scm 1062 */
 obj_t BgL_auxz00_6282;
{ /* Ieee/port.scm 1062 */
 obj_t BgL_aux2324z00_4114;
BgL_aux2324z00_4114 = 
CELL_REF(BgL_ipz00_3872); 
if(
INPUT_PORTP(BgL_aux2324z00_4114))
{ /* Ieee/port.scm 1062 */
BgL_auxz00_6282 = BgL_aux2324z00_4114
; }  else 
{ 
 obj_t BgL_auxz00_6285;
BgL_auxz00_6285 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46320L), BGl_string2830z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2324z00_4114); 
FAILURE(BgL_auxz00_6285,BFALSE,BFALSE);} } 
BgL_tmp1123z00_1688 = 
BGl_httpzd2parsezd2responsez00zz__httpz00(BgL_auxz00_6282, BgL_opz00_3873, BgL_parserz00_3874); } 
{ /* Ieee/port.scm 1056 */
 bool_t BgL_test3566z00_6290;
{ /* Ieee/port.scm 1056 */
 obj_t BgL_arg2069z00_2777;
BgL_arg2069z00_2777 = 
BGL_EXITD_PROTECT(BgL_exitd1122z00_1686); 
BgL_test3566z00_6290 = 
PAIRP(BgL_arg2069z00_2777); } 
if(BgL_test3566z00_6290)
{ /* Ieee/port.scm 1056 */
 obj_t BgL_arg2067z00_2778;
{ /* Ieee/port.scm 1056 */
 obj_t BgL_arg2068z00_2779;
BgL_arg2068z00_2779 = 
BGL_EXITD_PROTECT(BgL_exitd1122z00_1686); 
{ /* Ieee/port.scm 1056 */
 obj_t BgL_pairz00_2780;
if(
PAIRP(BgL_arg2068z00_2779))
{ /* Ieee/port.scm 1056 */
BgL_pairz00_2780 = BgL_arg2068z00_2779; }  else 
{ 
 obj_t BgL_auxz00_6296;
BgL_auxz00_6296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46113L), BGl_string2830z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2779); 
FAILURE(BgL_auxz00_6296,BFALSE,BFALSE);} 
BgL_arg2067z00_2778 = 
CDR(BgL_pairz00_2780); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1122z00_1686, BgL_arg2067z00_2778); BUNSPEC; }  else 
{ /* Ieee/port.scm 1056 */BFALSE; } } 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1120z00_3870, BgL_ohs1112z00_1684); BUNSPEC; 
BgL_res1128z00_1683 = BgL_tmp1123z00_1688; } } } } } } 
POP_ENV_EXIT(BgL_env1120z00_3870); 
return BgL_res1128z00_1683;} } 
}} 

}



/* &parser */
obj_t BGl_z62parserz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3676, obj_t BgL_ipz00_3677, obj_t BgL_statuszd2codezd2_3678, obj_t BgL_headerz00_3679, obj_t BgL_clenz00_3680, obj_t BgL_tencz00_3681)
{
{ /* Ieee/port.scm 1029 */
{ /* Ieee/port.scm 1020 */
 bool_t BgL_test3568z00_6304;
{ /* Ieee/port.scm 1020 */
 bool_t BgL_test3569z00_6305;
{ /* Ieee/port.scm 1020 */
 long BgL_n1z00_4598;
{ /* Ieee/port.scm 1020 */
 obj_t BgL_tmpz00_6306;
if(
INTEGERP(BgL_statuszd2codezd2_3678))
{ /* Ieee/port.scm 1020 */
BgL_tmpz00_6306 = BgL_statuszd2codezd2_3678
; }  else 
{ 
 obj_t BgL_auxz00_6309;
BgL_auxz00_6309 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(44968L), BGl_string2831z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_statuszd2codezd2_3678); 
FAILURE(BgL_auxz00_6309,BFALSE,BFALSE);} 
BgL_n1z00_4598 = 
(long)CINT(BgL_tmpz00_6306); } 
BgL_test3569z00_6305 = 
(BgL_n1z00_4598>=200L); } 
if(BgL_test3569z00_6305)
{ /* Ieee/port.scm 1020 */
 long BgL_n1z00_4599;
{ /* Ieee/port.scm 1020 */
 obj_t BgL_tmpz00_6315;
if(
INTEGERP(BgL_statuszd2codezd2_3678))
{ /* Ieee/port.scm 1020 */
BgL_tmpz00_6315 = BgL_statuszd2codezd2_3678
; }  else 
{ 
 obj_t BgL_auxz00_6318;
BgL_auxz00_6318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(44991L), BGl_string2831z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_statuszd2codezd2_3678); 
FAILURE(BgL_auxz00_6318,BFALSE,BFALSE);} 
BgL_n1z00_4599 = 
(long)CINT(BgL_tmpz00_6315); } 
BgL_test3568z00_6304 = 
(BgL_n1z00_4599<=299L); }  else 
{ /* Ieee/port.scm 1020 */
BgL_test3568z00_6304 = ((bool_t)0)
; } } 
if(BgL_test3568z00_6304)
{ /* Ieee/port.scm 1020 */
if(
INPUT_PORTP(BgL_ipz00_3677))
{ /* Ieee/port.scm 1024 */
 bool_t BgL_test3573z00_6326;
if(
ELONGP(BgL_clenz00_3680))
{ /* Ieee/port.scm 1024 */
 long BgL_n1z00_4600;
BgL_n1z00_4600 = 
BELONG_TO_LONG(BgL_clenz00_3680); 
BgL_test3573z00_6326 = 
(BgL_n1z00_4600>
(long)(0L)); }  else 
{ /* Ieee/port.scm 1024 */
BgL_test3573z00_6326 = ((bool_t)0)
; } 
if(BgL_test3573z00_6326)
{ /* Ieee/port.scm 1024 */
{ /* Ieee/port.scm 1025 */
 long BgL_arg1513z00_4601;
{ /* Ieee/port.scm 1025 */
 long BgL_xz00_4602;
BgL_xz00_4602 = 
BELONG_TO_LONG(BgL_clenz00_3680); 
BgL_arg1513z00_4601 = 
(long)(BgL_xz00_4602); } 
INPUT_PORT_FILLBARRIER_SET(BgL_ipz00_3677, BgL_arg1513z00_4601); BUNSPEC; BgL_arg1513z00_4601; } 
{ /* Ieee/port.scm 1026 */
 long BgL_tmpz00_6335;
BgL_tmpz00_6335 = 
BELONG_TO_LONG(BgL_clenz00_3680); 
BGL_INPUT_PORT_LENGTH_SET(BgL_ipz00_3677, BgL_tmpz00_6335); } BUNSPEC; 
return BgL_ipz00_3677;}  else 
{ /* Ieee/port.scm 1024 */
return BgL_ipz00_3677;} }  else 
{ /* Ieee/port.scm 469 */

return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BGl_string2832z00zz__r4_ports_6_10_1z00, 
BINT(0L), 
BINT(0L));} }  else 
{ /* Ieee/port.scm 1020 */
return BFALSE;} } } 

}



/* &<@anonymous:1489> */
obj_t BGl_z62zc3z04anonymousza31489ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3682, obj_t BgL_ipz00_3691, obj_t BgL_offsetz00_3692)
{
{ /* Ieee/port.scm 1052 */
{ /* Ieee/port.scm 1053 */
 obj_t BgL_hostz00_3683; obj_t BgL_loginz00_3684; obj_t BgL_abspathz00_3685; obj_t BgL_portz00_3686; obj_t BgL_timeoutz00_3687; obj_t BgL_bufinfoz00_3688; obj_t BgL_parserz00_3689; obj_t BgL_sockz00_3690;
BgL_hostz00_3683 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(0L)); 
BgL_loginz00_3684 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(1L)); 
BgL_abspathz00_3685 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(2L)); 
BgL_portz00_3686 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(3L)); 
BgL_timeoutz00_3687 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(4L)); 
BgL_bufinfoz00_3688 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(5L)); 
BgL_parserz00_3689 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3682, 
(int)(6L))); 
BgL_sockz00_3690 = 
PROCEDURE_REF(BgL_envz00_3682, 
(int)(7L)); 
{ /* Ieee/port.scm 1053 */
 obj_t BgL_socketz00_4604;
if(
SOCKETP(BgL_sockz00_3690))
{ /* Ieee/port.scm 1053 */
BgL_socketz00_4604 = BgL_sockz00_3690; }  else 
{ 
 obj_t BgL_auxz00_6360;
BgL_auxz00_6360 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45970L), BGl_string2833z00zz__r4_ports_6_10_1z00, BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_sockz00_3690); 
FAILURE(BgL_auxz00_6360,BFALSE,BFALSE);} 
socket_close(BgL_socketz00_4604); } 
{ /* Ieee/port.scm 1054 */
 obj_t BgL_rz00_4605;
{ /* Ieee/port.scm 1054 */
 obj_t BgL_arg1499z00_4606;
{ /* Ieee/port.scm 1053 */

{ /* Ieee/port.scm 1053 */
 long BgL_auxz00_6365;
{ /* Ieee/port.scm 1054 */
 obj_t BgL_tmpz00_6366;
if(
INTEGERP(BgL_offsetz00_3692))
{ /* Ieee/port.scm 1054 */
BgL_tmpz00_6366 = BgL_offsetz00_3692
; }  else 
{ 
 obj_t BgL_auxz00_6369;
BgL_auxz00_6369 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(46030L), BGl_string2833z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_offsetz00_3692); 
FAILURE(BgL_auxz00_6369,BFALSE,BFALSE);} 
BgL_auxz00_6365 = 
(long)CINT(BgL_tmpz00_6366); } 
BgL_arg1499z00_4606 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_auxz00_6365, 10L); } } 
BgL_rz00_4605 = 
string_append_3(BGl_string2834z00zz__r4_ports_6_10_1z00, BgL_arg1499z00_4606, BGl_string2835z00zz__r4_ports_6_10_1z00); } 
{ /* Ieee/port.scm 1055 */
 obj_t BgL_arg1490z00_4607;
{ /* Ieee/port.scm 1055 */
 obj_t BgL_arg1492z00_4608; obj_t BgL_arg1494z00_4609;
{ /* Ieee/port.scm 1055 */
 obj_t BgL_arg1495z00_4610;
BgL_arg1495z00_4610 = 
MAKE_YOUNG_PAIR(BgL_rz00_4605, BNIL); 
BgL_arg1492z00_4608 = 
MAKE_YOUNG_PAIR(BGl_keyword2836z00zz__r4_ports_6_10_1z00, BgL_arg1495z00_4610); } 
{ /* Ieee/port.scm 1055 */
 obj_t BgL_arg1497z00_4611;
{ /* Ieee/port.scm 1055 */
 obj_t BgL_arg1498z00_4612;
BgL_arg1498z00_4612 = 
MAKE_YOUNG_PAIR(BGl_string2814z00zz__r4_ports_6_10_1z00, BNIL); 
BgL_arg1497z00_4611 = 
MAKE_YOUNG_PAIR(BGl_keyword2812z00zz__r4_ports_6_10_1z00, BgL_arg1498z00_4612); } 
BgL_arg1494z00_4609 = 
MAKE_YOUNG_PAIR(BgL_arg1497z00_4611, BNIL); } 
BgL_arg1490z00_4607 = 
MAKE_YOUNG_PAIR(BgL_arg1492z00_4608, BgL_arg1494z00_4609); } 
return 
BGl_z62loopz62zz__r4_ports_6_10_1z00(BgL_parserz00_3689, BgL_bufinfoz00_3688, BgL_timeoutz00_3687, BgL_portz00_3686, BgL_abspathz00_3685, BgL_loginz00_3684, BgL_hostz00_3683, BgL_ipz00_3691, BgL_arg1490z00_4607);} } } } 

}



/* &<@anonymous:1487> */
obj_t BGl_z62zc3z04anonymousza31487ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3693, obj_t BgL_ipz00_3696)
{
{ /* Ieee/port.scm 1048 */
{ /* Ieee/port.scm 1049 */
 obj_t BgL_opz00_3694; obj_t BgL_sockz00_3695;
BgL_opz00_3694 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3693, 
(int)(0L))); 
BgL_sockz00_3695 = 
PROCEDURE_REF(BgL_envz00_3693, 
(int)(1L)); 
bgl_close_output_port(BgL_opz00_3694); 
{ /* Ieee/port.scm 1050 */
 obj_t BgL_socketz00_4613;
if(
SOCKETP(BgL_sockz00_3695))
{ /* Ieee/port.scm 1050 */
BgL_socketz00_4613 = BgL_sockz00_3695; }  else 
{ 
 obj_t BgL_auxz00_6391;
BgL_auxz00_6391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(45886L), BGl_string2838z00zz__r4_ports_6_10_1z00, BGl_string2825z00zz__r4_ports_6_10_1z00, BgL_sockz00_3695); 
FAILURE(BgL_auxz00_6391,BFALSE,BFALSE);} 
return 
socket_close(BgL_socketz00_4613);} } } 

}



/* _open-input-file */
obj_t BGl__openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_env1258z00_72, obj_t BgL_opt1257z00_71)
{
{ /* Ieee/port.scm 1084 */
{ /* Ieee/port.scm 1084 */
 obj_t BgL_g1259z00_1738;
BgL_g1259z00_1738 = 
VECTOR_REF(BgL_opt1257z00_71,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1257z00_71)) { case 1L : 

{ /* Ieee/port.scm 1084 */

{ /* Ieee/port.scm 1084 */
 obj_t BgL_auxz00_6397;
if(
STRINGP(BgL_g1259z00_1738))
{ /* Ieee/port.scm 1084 */
BgL_auxz00_6397 = BgL_g1259z00_1738
; }  else 
{ 
 obj_t BgL_auxz00_6400;
BgL_auxz00_6400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(47820L), BGl_string2841z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1259z00_1738); 
FAILURE(BgL_auxz00_6400,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_auxz00_6397, BTRUE, 
BINT(5000000L));} } break;case 2L : 

{ /* Ieee/port.scm 1084 */
 obj_t BgL_bufinfoz00_1743;
BgL_bufinfoz00_1743 = 
VECTOR_REF(BgL_opt1257z00_71,1L); 
{ /* Ieee/port.scm 1084 */

{ /* Ieee/port.scm 1084 */
 obj_t BgL_auxz00_6407;
if(
STRINGP(BgL_g1259z00_1738))
{ /* Ieee/port.scm 1084 */
BgL_auxz00_6407 = BgL_g1259z00_1738
; }  else 
{ 
 obj_t BgL_auxz00_6410;
BgL_auxz00_6410 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(47820L), BGl_string2841z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1259z00_1738); 
FAILURE(BgL_auxz00_6410,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_auxz00_6407, BgL_bufinfoz00_1743, 
BINT(5000000L));} } } break;case 3L : 

{ /* Ieee/port.scm 1084 */
 obj_t BgL_bufinfoz00_1745;
BgL_bufinfoz00_1745 = 
VECTOR_REF(BgL_opt1257z00_71,1L); 
{ /* Ieee/port.scm 1084 */
 obj_t BgL_timeoutz00_1746;
BgL_timeoutz00_1746 = 
VECTOR_REF(BgL_opt1257z00_71,2L); 
{ /* Ieee/port.scm 1084 */

{ /* Ieee/port.scm 1084 */
 obj_t BgL_auxz00_6418;
if(
STRINGP(BgL_g1259z00_1738))
{ /* Ieee/port.scm 1084 */
BgL_auxz00_6418 = BgL_g1259z00_1738
; }  else 
{ 
 obj_t BgL_auxz00_6421;
BgL_auxz00_6421 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(47820L), BGl_string2841z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1259z00_1738); 
FAILURE(BgL_auxz00_6421,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_auxz00_6418, BgL_bufinfoz00_1745, BgL_timeoutz00_1746);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2839z00zz__r4_ports_6_10_1z00, BGl_string2840z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1257z00_71)));} } } } 

}



/* open-input-file */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_68, obj_t BgL_bufinfoz00_69, obj_t BgL_timeoutz00_70)
{
{ /* Ieee/port.scm 1084 */
{ /* Ieee/port.scm 1085 */
 obj_t BgL_bufferz00_1748;
BgL_bufferz00_1748 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2806z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_69, 
(int)(default_io_bufsiz)); 
{ 
 obj_t BgL_protosz00_1750;
BgL_protosz00_1750 = BGl_za2inputzd2portzd2protocolsza2z00zz__r4_ports_6_10_1z00; 
BgL_zc3z04anonymousza31515ze3z87_1751:
if(
NULLP(BgL_protosz00_1750))
{ /* Ieee/port.scm 1087 */
return 
bgl_open_input_file(BgL_stringz00_68, BgL_bufferz00_1748);}  else 
{ /* Ieee/port.scm 1090 */
 obj_t BgL_cellz00_1753;
{ /* Ieee/port.scm 1090 */
 obj_t BgL_pairz00_2782;
if(
PAIRP(BgL_protosz00_1750))
{ /* Ieee/port.scm 1090 */
BgL_pairz00_2782 = BgL_protosz00_1750; }  else 
{ 
 obj_t BgL_auxz00_6438;
BgL_auxz00_6438 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48142L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_protosz00_1750); 
FAILURE(BgL_auxz00_6438,BFALSE,BFALSE);} 
BgL_cellz00_1753 = 
CAR(BgL_pairz00_2782); } 
{ /* Ieee/port.scm 1090 */
 obj_t BgL_identz00_1754;
{ /* Ieee/port.scm 1091 */
 obj_t BgL_pairz00_2783;
if(
PAIRP(BgL_cellz00_1753))
{ /* Ieee/port.scm 1091 */
BgL_pairz00_2783 = BgL_cellz00_1753; }  else 
{ 
 obj_t BgL_auxz00_6445;
BgL_auxz00_6445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48169L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_cellz00_1753); 
FAILURE(BgL_auxz00_6445,BFALSE,BFALSE);} 
BgL_identz00_1754 = 
CAR(BgL_pairz00_2783); } 
{ /* Ieee/port.scm 1091 */
 long BgL_lz00_1755;
{ /* Ieee/port.scm 1092 */
 obj_t BgL_stringz00_2784;
if(
STRINGP(BgL_identz00_1754))
{ /* Ieee/port.scm 1092 */
BgL_stringz00_2784 = BgL_identz00_1754; }  else 
{ 
 obj_t BgL_auxz00_6452;
BgL_auxz00_6452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48185L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_identz00_1754); 
FAILURE(BgL_auxz00_6452,BFALSE,BFALSE);} 
BgL_lz00_1755 = 
STRING_LENGTH(BgL_stringz00_2784); } 
{ /* Ieee/port.scm 1092 */
 obj_t BgL_openz00_1756;
{ /* Ieee/port.scm 1093 */
 obj_t BgL_pairz00_2785;
if(
PAIRP(BgL_cellz00_1753))
{ /* Ieee/port.scm 1093 */
BgL_pairz00_2785 = BgL_cellz00_1753; }  else 
{ 
 obj_t BgL_auxz00_6459;
BgL_auxz00_6459 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48225L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_cellz00_1753); 
FAILURE(BgL_auxz00_6459,BFALSE,BFALSE);} 
BgL_openz00_1756 = 
CDR(BgL_pairz00_2785); } 
{ /* Ieee/port.scm 1093 */

{ /* Ieee/port.scm 1094 */
 bool_t BgL_test3586z00_6464;
{ /* Ieee/port.scm 1094 */
 obj_t BgL_string2z00_2787;
if(
STRINGP(BgL_identz00_1754))
{ /* Ieee/port.scm 1094 */
BgL_string2z00_2787 = BgL_identz00_1754; }  else 
{ 
 obj_t BgL_auxz00_6467;
BgL_auxz00_6467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48259L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_identz00_1754); 
FAILURE(BgL_auxz00_6467,BFALSE,BFALSE);} 
BgL_test3586z00_6464 = 
bigloo_strncmp(BgL_stringz00_68, BgL_string2z00_2787, BgL_lz00_1755); } 
if(BgL_test3586z00_6464)
{ /* Ieee/port.scm 1096 */
 obj_t BgL_namez00_1758;
BgL_namez00_1758 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_68, BgL_lz00_1755, 
STRING_LENGTH(BgL_stringz00_68)); 
{ /* Ieee/port.scm 1097 */
 obj_t BgL_funz00_4143;
if(
PROCEDUREP(BgL_openz00_1756))
{ /* Ieee/port.scm 1097 */
BgL_funz00_4143 = BgL_openz00_1756; }  else 
{ 
 obj_t BgL_auxz00_6476;
BgL_auxz00_6476 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48382L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_openz00_1756); 
FAILURE(BgL_auxz00_6476,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4143, 3))
{ /* Ieee/port.scm 1097 */
return 
(VA_PROCEDUREP( BgL_funz00_4143 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4143))(BgL_openz00_1756, BgL_namez00_1758, BgL_bufferz00_1748, BgL_timeoutz00_70, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4143))(BgL_openz00_1756, BgL_namez00_1758, BgL_bufferz00_1748, BgL_timeoutz00_70) );}  else 
{ /* Ieee/port.scm 1097 */
FAILURE(BGl_string2843z00zz__r4_ports_6_10_1z00,BGl_list2844z00zz__r4_ports_6_10_1z00,BgL_funz00_4143);} } }  else 
{ /* Ieee/port.scm 1098 */
 obj_t BgL_arg1522z00_1760;
{ /* Ieee/port.scm 1098 */
 obj_t BgL_pairz00_2790;
if(
PAIRP(BgL_protosz00_1750))
{ /* Ieee/port.scm 1098 */
BgL_pairz00_2790 = BgL_protosz00_1750; }  else 
{ 
 obj_t BgL_auxz00_6491;
BgL_auxz00_6491 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48427L), BGl_string2842z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_protosz00_1750); 
FAILURE(BgL_auxz00_6491,BFALSE,BFALSE);} 
BgL_arg1522z00_1760 = 
CDR(BgL_pairz00_2790); } 
{ 
 obj_t BgL_protosz00_6496;
BgL_protosz00_6496 = BgL_arg1522z00_1760; 
BgL_protosz00_1750 = BgL_protosz00_6496; 
goto BgL_zc3z04anonymousza31515ze3z87_1751;} } } } } } } } } } } 

}



/* _open-input-descriptor */
obj_t BGl__openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(obj_t BgL_env1263z00_76, obj_t BgL_opt1262z00_75)
{
{ /* Ieee/port.scm 1103 */
{ /* Ieee/port.scm 1103 */
 obj_t BgL_g1264z00_1762;
BgL_g1264z00_1762 = 
VECTOR_REF(BgL_opt1262z00_75,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1262z00_75)) { case 1L : 

{ /* Ieee/port.scm 1103 */

{ /* Ieee/port.scm 1103 */
 int BgL_fdz00_2791;
{ /* Ieee/port.scm 1103 */
 obj_t BgL_tmpz00_6498;
if(
INTEGERP(BgL_g1264z00_1762))
{ /* Ieee/port.scm 1103 */
BgL_tmpz00_6498 = BgL_g1264z00_1762
; }  else 
{ 
 obj_t BgL_auxz00_6501;
BgL_auxz00_6501 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48665L), BGl_string2856z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_g1264z00_1762); 
FAILURE(BgL_auxz00_6501,BFALSE,BFALSE);} 
BgL_fdz00_2791 = 
CINT(BgL_tmpz00_6498); } 
return 
bgl_open_input_descriptor(BgL_fdz00_2791, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2806z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz)));} } break;case 2L : 

{ /* Ieee/port.scm 1103 */
 obj_t BgL_bufinfoz00_1766;
BgL_bufinfoz00_1766 = 
VECTOR_REF(BgL_opt1262z00_75,1L); 
{ /* Ieee/port.scm 1103 */

{ /* Ieee/port.scm 1103 */
 int BgL_fdz00_2793;
{ /* Ieee/port.scm 1103 */
 obj_t BgL_tmpz00_6510;
if(
INTEGERP(BgL_g1264z00_1762))
{ /* Ieee/port.scm 1103 */
BgL_tmpz00_6510 = BgL_g1264z00_1762
; }  else 
{ 
 obj_t BgL_auxz00_6513;
BgL_auxz00_6513 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(48665L), BGl_string2856z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_g1264z00_1762); 
FAILURE(BgL_auxz00_6513,BFALSE,BFALSE);} 
BgL_fdz00_2793 = 
CINT(BgL_tmpz00_6510); } 
return 
bgl_open_input_descriptor(BgL_fdz00_2793, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2806z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1766, 
(int)(default_io_bufsiz)));} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2853z00zz__r4_ports_6_10_1z00, BGl_string2855z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1262z00_75)));} } } } 

}



/* open-input-descriptor */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2descriptorz00zz__r4_ports_6_10_1z00(int BgL_fdz00_73, obj_t BgL_bufinfoz00_74)
{
{ /* Ieee/port.scm 1103 */
return 
bgl_open_input_descriptor(BgL_fdz00_73, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2806z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_74, 
(int)(default_io_bufsiz)));} 

}



/* _open-input-string */
obj_t BGl__openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_env1268z00_81, obj_t BgL_opt1267z00_80)
{
{ /* Ieee/port.scm 1110 */
{ /* Ieee/port.scm 1110 */
 obj_t BgL_stringz00_1769;
BgL_stringz00_1769 = 
VECTOR_REF(BgL_opt1267z00_80,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1267z00_80)) { case 1L : 

{ /* Ieee/port.scm 1111 */
 long BgL_endz00_1773;
{ /* Ieee/port.scm 1111 */
 obj_t BgL_stringz00_2796;
if(
STRINGP(BgL_stringz00_1769))
{ /* Ieee/port.scm 1111 */
BgL_stringz00_2796 = BgL_stringz00_1769; }  else 
{ 
 obj_t BgL_auxz00_6532;
BgL_auxz00_6532 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49140L), BGl_string2859z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1769); 
FAILURE(BgL_auxz00_6532,BFALSE,BFALSE);} 
BgL_endz00_1773 = 
STRING_LENGTH(BgL_stringz00_2796); } 
{ /* Ieee/port.scm 1110 */

{ /* Ieee/port.scm 1110 */
 obj_t BgL_auxz00_6537;
if(
STRINGP(BgL_stringz00_1769))
{ /* Ieee/port.scm 1110 */
BgL_auxz00_6537 = BgL_stringz00_1769
; }  else 
{ 
 obj_t BgL_auxz00_6540;
BgL_auxz00_6540 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49076L), BGl_string2859z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1769); 
FAILURE(BgL_auxz00_6540,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_auxz00_6537, 
BINT(0L), 
BINT(BgL_endz00_1773));} } } break;case 2L : 

{ /* Ieee/port.scm 1110 */
 obj_t BgL_startz00_1774;
BgL_startz00_1774 = 
VECTOR_REF(BgL_opt1267z00_80,1L); 
{ /* Ieee/port.scm 1111 */
 long BgL_endz00_1775;
{ /* Ieee/port.scm 1111 */
 obj_t BgL_stringz00_2797;
if(
STRINGP(BgL_stringz00_1769))
{ /* Ieee/port.scm 1111 */
BgL_stringz00_2797 = BgL_stringz00_1769; }  else 
{ 
 obj_t BgL_auxz00_6550;
BgL_auxz00_6550 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49140L), BGl_string2859z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1769); 
FAILURE(BgL_auxz00_6550,BFALSE,BFALSE);} 
BgL_endz00_1775 = 
STRING_LENGTH(BgL_stringz00_2797); } 
{ /* Ieee/port.scm 1110 */

{ /* Ieee/port.scm 1110 */
 obj_t BgL_auxz00_6555;
if(
STRINGP(BgL_stringz00_1769))
{ /* Ieee/port.scm 1110 */
BgL_auxz00_6555 = BgL_stringz00_1769
; }  else 
{ 
 obj_t BgL_auxz00_6558;
BgL_auxz00_6558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49076L), BGl_string2859z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1769); 
FAILURE(BgL_auxz00_6558,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_auxz00_6555, BgL_startz00_1774, 
BINT(BgL_endz00_1775));} } } } break;case 3L : 

{ /* Ieee/port.scm 1110 */
 obj_t BgL_startz00_1776;
BgL_startz00_1776 = 
VECTOR_REF(BgL_opt1267z00_80,1L); 
{ /* Ieee/port.scm 1110 */
 obj_t BgL_endz00_1777;
BgL_endz00_1777 = 
VECTOR_REF(BgL_opt1267z00_80,2L); 
{ /* Ieee/port.scm 1110 */

{ /* Ieee/port.scm 1110 */
 obj_t BgL_auxz00_6566;
if(
STRINGP(BgL_stringz00_1769))
{ /* Ieee/port.scm 1110 */
BgL_auxz00_6566 = BgL_stringz00_1769
; }  else 
{ 
 obj_t BgL_auxz00_6569;
BgL_auxz00_6569 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49076L), BGl_string2859z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1769); 
FAILURE(BgL_auxz00_6569,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_auxz00_6566, BgL_startz00_1776, BgL_endz00_1777);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2857z00zz__r4_ports_6_10_1z00, BGl_string2840z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1267z00_80)));} } } } 

}



/* open-input-string */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_77, obj_t BgL_startz00_78, obj_t BgL_endz00_79)
{
{ /* Ieee/port.scm 1110 */
{ /* Ieee/port.scm 1113 */
 bool_t BgL_test3598z00_6579;
{ /* Ieee/port.scm 1113 */
 long BgL_n1z00_2798;
{ /* Ieee/port.scm 1113 */
 obj_t BgL_tmpz00_6580;
if(
INTEGERP(BgL_startz00_78))
{ /* Ieee/port.scm 1113 */
BgL_tmpz00_6580 = BgL_startz00_78
; }  else 
{ 
 obj_t BgL_auxz00_6583;
BgL_auxz00_6583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49186L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
FAILURE(BgL_auxz00_6583,BFALSE,BFALSE);} 
BgL_n1z00_2798 = 
(long)CINT(BgL_tmpz00_6580); } 
BgL_test3598z00_6579 = 
(BgL_n1z00_2798<0L); } 
if(BgL_test3598z00_6579)
{ /* Ieee/port.scm 1114 */
 obj_t BgL_aux2369z00_4160;
BgL_aux2369z00_4160 = 
BGl_errorz00zz__errorz00(BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2860z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
if(
INPUT_PORTP(BgL_aux2369z00_4160))
{ /* Ieee/port.scm 1114 */
return BgL_aux2369z00_4160;}  else 
{ 
 obj_t BgL_auxz00_6592;
BgL_auxz00_6592 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49202L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2369z00_4160); 
FAILURE(BgL_auxz00_6592,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1115 */
 bool_t BgL_test3601z00_6596;
{ /* Ieee/port.scm 1115 */
 long BgL_n1z00_2800;
{ /* Ieee/port.scm 1115 */
 obj_t BgL_tmpz00_6597;
if(
INTEGERP(BgL_startz00_78))
{ /* Ieee/port.scm 1115 */
BgL_tmpz00_6597 = BgL_startz00_78
; }  else 
{ 
 obj_t BgL_auxz00_6600;
BgL_auxz00_6600 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49272L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
FAILURE(BgL_auxz00_6600,BFALSE,BFALSE);} 
BgL_n1z00_2800 = 
(long)CINT(BgL_tmpz00_6597); } 
BgL_test3601z00_6596 = 
(BgL_n1z00_2800>
STRING_LENGTH(BgL_stringz00_77)); } 
if(BgL_test3601z00_6596)
{ /* Ieee/port.scm 1116 */
 obj_t BgL_aux2372z00_4163;
BgL_aux2372z00_4163 = 
BGl_errorz00zz__errorz00(BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2861z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
if(
INPUT_PORTP(BgL_aux2372z00_4163))
{ /* Ieee/port.scm 1116 */
return BgL_aux2372z00_4163;}  else 
{ 
 obj_t BgL_auxz00_6610;
BgL_auxz00_6610 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49309L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2372z00_4163); 
FAILURE(BgL_auxz00_6610,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1117 */
 bool_t BgL_test3604z00_6614;
{ /* Ieee/port.scm 1117 */
 long BgL_n1z00_2802; long BgL_n2z00_2803;
{ /* Ieee/port.scm 1117 */
 obj_t BgL_tmpz00_6615;
if(
INTEGERP(BgL_startz00_78))
{ /* Ieee/port.scm 1117 */
BgL_tmpz00_6615 = BgL_startz00_78
; }  else 
{ 
 obj_t BgL_auxz00_6618;
BgL_auxz00_6618 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49385L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
FAILURE(BgL_auxz00_6618,BFALSE,BFALSE);} 
BgL_n1z00_2802 = 
(long)CINT(BgL_tmpz00_6615); } 
{ /* Ieee/port.scm 1117 */
 obj_t BgL_tmpz00_6623;
if(
INTEGERP(BgL_endz00_79))
{ /* Ieee/port.scm 1117 */
BgL_tmpz00_6623 = BgL_endz00_79
; }  else 
{ 
 obj_t BgL_auxz00_6626;
BgL_auxz00_6626 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49391L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_79); 
FAILURE(BgL_auxz00_6626,BFALSE,BFALSE);} 
BgL_n2z00_2803 = 
(long)CINT(BgL_tmpz00_6623); } 
BgL_test3604z00_6614 = 
(BgL_n1z00_2802>BgL_n2z00_2803); } 
if(BgL_test3604z00_6614)
{ /* Ieee/port.scm 1118 */
 obj_t BgL_aux2376z00_4167;
BgL_aux2376z00_4167 = 
BGl_errorz00zz__errorz00(BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2862z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
if(
INPUT_PORTP(BgL_aux2376z00_4167))
{ /* Ieee/port.scm 1118 */
return BgL_aux2376z00_4167;}  else 
{ 
 obj_t BgL_auxz00_6635;
BgL_auxz00_6635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49403L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2376z00_4167); 
FAILURE(BgL_auxz00_6635,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1119 */
 bool_t BgL_test3608z00_6639;
{ /* Ieee/port.scm 1119 */
 long BgL_n1z00_2805;
{ /* Ieee/port.scm 1119 */
 obj_t BgL_tmpz00_6640;
if(
INTEGERP(BgL_endz00_79))
{ /* Ieee/port.scm 1119 */
BgL_tmpz00_6640 = BgL_endz00_79
; }  else 
{ 
 obj_t BgL_auxz00_6643;
BgL_auxz00_6643 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49482L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_79); 
FAILURE(BgL_auxz00_6643,BFALSE,BFALSE);} 
BgL_n1z00_2805 = 
(long)CINT(BgL_tmpz00_6640); } 
BgL_test3608z00_6639 = 
(BgL_n1z00_2805>
STRING_LENGTH(BgL_stringz00_77)); } 
if(BgL_test3608z00_6639)
{ /* Ieee/port.scm 1120 */
 obj_t BgL_aux2379z00_4170;
BgL_aux2379z00_4170 = 
BGl_errorz00zz__errorz00(BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2863z00zz__r4_ports_6_10_1z00, BgL_endz00_79); 
if(
INPUT_PORTP(BgL_aux2379z00_4170))
{ /* Ieee/port.scm 1120 */
return BgL_aux2379z00_4170;}  else 
{ 
 obj_t BgL_auxz00_6653;
BgL_auxz00_6653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49517L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2379z00_4170); 
FAILURE(BgL_auxz00_6653,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1122 */
 long BgL_auxz00_6666; long BgL_tmpz00_6657;
{ /* Ieee/port.scm 1122 */
 obj_t BgL_tmpz00_6667;
if(
INTEGERP(BgL_endz00_79))
{ /* Ieee/port.scm 1122 */
BgL_tmpz00_6667 = BgL_endz00_79
; }  else 
{ 
 obj_t BgL_auxz00_6670;
BgL_auxz00_6670 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49632L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_79); 
FAILURE(BgL_auxz00_6670,BFALSE,BFALSE);} 
BgL_auxz00_6666 = 
(long)CINT(BgL_tmpz00_6667); } 
{ /* Ieee/port.scm 1122 */
 obj_t BgL_tmpz00_6658;
if(
INTEGERP(BgL_startz00_78))
{ /* Ieee/port.scm 1122 */
BgL_tmpz00_6658 = BgL_startz00_78
; }  else 
{ 
 obj_t BgL_auxz00_6661;
BgL_auxz00_6661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49626L), BGl_string2858z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_78); 
FAILURE(BgL_auxz00_6661,BFALSE,BFALSE);} 
BgL_tmpz00_6657 = 
(long)CINT(BgL_tmpz00_6658); } 
return 
bgl_open_input_substring(BgL_stringz00_77, BgL_tmpz00_6657, BgL_auxz00_6666);} } } } } } 

}



/* _open-input-string! */
obj_t BGl__openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t BgL_env1272z00_86, obj_t BgL_opt1271z00_85)
{
{ /* Ieee/port.scm 1127 */
{ /* Ieee/port.scm 1127 */
 obj_t BgL_stringz00_1787;
BgL_stringz00_1787 = 
VECTOR_REF(BgL_opt1271z00_85,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1271z00_85)) { case 1L : 

{ /* Ieee/port.scm 1128 */
 long BgL_endz00_1791;
{ /* Ieee/port.scm 1128 */
 obj_t BgL_stringz00_2807;
if(
STRINGP(BgL_stringz00_1787))
{ /* Ieee/port.scm 1128 */
BgL_stringz00_2807 = BgL_stringz00_1787; }  else 
{ 
 obj_t BgL_auxz00_6679;
BgL_auxz00_6679 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49928L), BGl_string2866z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1787); 
FAILURE(BgL_auxz00_6679,BFALSE,BFALSE);} 
BgL_endz00_1791 = 
STRING_LENGTH(BgL_stringz00_2807); } 
{ /* Ieee/port.scm 1127 */

{ /* Ieee/port.scm 1127 */
 obj_t BgL_auxz00_6684;
if(
STRINGP(BgL_stringz00_1787))
{ /* Ieee/port.scm 1127 */
BgL_auxz00_6684 = BgL_stringz00_1787
; }  else 
{ 
 obj_t BgL_auxz00_6687;
BgL_auxz00_6687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49863L), BGl_string2866z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1787); 
FAILURE(BgL_auxz00_6687,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_6684, 
BINT(0L), 
BINT(BgL_endz00_1791));} } } break;case 2L : 

{ /* Ieee/port.scm 1127 */
 obj_t BgL_startz00_1792;
BgL_startz00_1792 = 
VECTOR_REF(BgL_opt1271z00_85,1L); 
{ /* Ieee/port.scm 1128 */
 long BgL_endz00_1793;
{ /* Ieee/port.scm 1128 */
 obj_t BgL_stringz00_2808;
if(
STRINGP(BgL_stringz00_1787))
{ /* Ieee/port.scm 1128 */
BgL_stringz00_2808 = BgL_stringz00_1787; }  else 
{ 
 obj_t BgL_auxz00_6697;
BgL_auxz00_6697 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49928L), BGl_string2866z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1787); 
FAILURE(BgL_auxz00_6697,BFALSE,BFALSE);} 
BgL_endz00_1793 = 
STRING_LENGTH(BgL_stringz00_2808); } 
{ /* Ieee/port.scm 1127 */

{ /* Ieee/port.scm 1127 */
 obj_t BgL_auxz00_6702;
if(
STRINGP(BgL_stringz00_1787))
{ /* Ieee/port.scm 1127 */
BgL_auxz00_6702 = BgL_stringz00_1787
; }  else 
{ 
 obj_t BgL_auxz00_6705;
BgL_auxz00_6705 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49863L), BGl_string2866z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1787); 
FAILURE(BgL_auxz00_6705,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_6702, BgL_startz00_1792, 
BINT(BgL_endz00_1793));} } } } break;case 3L : 

{ /* Ieee/port.scm 1127 */
 obj_t BgL_startz00_1794;
BgL_startz00_1794 = 
VECTOR_REF(BgL_opt1271z00_85,1L); 
{ /* Ieee/port.scm 1127 */
 obj_t BgL_endz00_1795;
BgL_endz00_1795 = 
VECTOR_REF(BgL_opt1271z00_85,2L); 
{ /* Ieee/port.scm 1127 */

{ /* Ieee/port.scm 1127 */
 obj_t BgL_auxz00_6713;
if(
STRINGP(BgL_stringz00_1787))
{ /* Ieee/port.scm 1127 */
BgL_auxz00_6713 = BgL_stringz00_1787
; }  else 
{ 
 obj_t BgL_auxz00_6716;
BgL_auxz00_6716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49863L), BGl_string2866z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_1787); 
FAILURE(BgL_auxz00_6716,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_6713, BgL_startz00_1794, BgL_endz00_1795);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2864z00zz__r4_ports_6_10_1z00, BGl_string2840z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1271z00_85)));} } } } 

}



/* open-input-string! */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_82, obj_t BgL_startz00_83, obj_t BgL_endz00_84)
{
{ /* Ieee/port.scm 1127 */
{ /* Ieee/port.scm 1130 */
 bool_t BgL_test3618z00_6726;
{ /* Ieee/port.scm 1130 */
 long BgL_n1z00_2809;
{ /* Ieee/port.scm 1130 */
 obj_t BgL_tmpz00_6727;
if(
INTEGERP(BgL_startz00_83))
{ /* Ieee/port.scm 1130 */
BgL_tmpz00_6727 = BgL_startz00_83
; }  else 
{ 
 obj_t BgL_auxz00_6730;
BgL_auxz00_6730 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49974L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
FAILURE(BgL_auxz00_6730,BFALSE,BFALSE);} 
BgL_n1z00_2809 = 
(long)CINT(BgL_tmpz00_6727); } 
BgL_test3618z00_6726 = 
(BgL_n1z00_2809<0L); } 
if(BgL_test3618z00_6726)
{ /* Ieee/port.scm 1131 */
 obj_t BgL_aux2394z00_4185;
BgL_aux2394z00_4185 = 
BGl_errorz00zz__errorz00(BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2860z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
if(
INPUT_PORTP(BgL_aux2394z00_4185))
{ /* Ieee/port.scm 1131 */
return BgL_aux2394z00_4185;}  else 
{ 
 obj_t BgL_auxz00_6739;
BgL_auxz00_6739 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(49990L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2394z00_4185); 
FAILURE(BgL_auxz00_6739,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1132 */
 bool_t BgL_test3621z00_6743;
{ /* Ieee/port.scm 1132 */
 long BgL_n1z00_2811;
{ /* Ieee/port.scm 1132 */
 obj_t BgL_tmpz00_6744;
if(
INTEGERP(BgL_startz00_83))
{ /* Ieee/port.scm 1132 */
BgL_tmpz00_6744 = BgL_startz00_83
; }  else 
{ 
 obj_t BgL_auxz00_6747;
BgL_auxz00_6747 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50061L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
FAILURE(BgL_auxz00_6747,BFALSE,BFALSE);} 
BgL_n1z00_2811 = 
(long)CINT(BgL_tmpz00_6744); } 
BgL_test3621z00_6743 = 
(BgL_n1z00_2811>
STRING_LENGTH(BgL_stringz00_82)); } 
if(BgL_test3621z00_6743)
{ /* Ieee/port.scm 1133 */
 obj_t BgL_aux2397z00_4188;
BgL_aux2397z00_4188 = 
BGl_errorz00zz__errorz00(BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2861z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
if(
INPUT_PORTP(BgL_aux2397z00_4188))
{ /* Ieee/port.scm 1133 */
return BgL_aux2397z00_4188;}  else 
{ 
 obj_t BgL_auxz00_6757;
BgL_auxz00_6757 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50098L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2397z00_4188); 
FAILURE(BgL_auxz00_6757,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1134 */
 bool_t BgL_test3624z00_6761;
{ /* Ieee/port.scm 1134 */
 long BgL_n1z00_2813; long BgL_n2z00_2814;
{ /* Ieee/port.scm 1134 */
 obj_t BgL_tmpz00_6762;
if(
INTEGERP(BgL_startz00_83))
{ /* Ieee/port.scm 1134 */
BgL_tmpz00_6762 = BgL_startz00_83
; }  else 
{ 
 obj_t BgL_auxz00_6765;
BgL_auxz00_6765 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50175L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
FAILURE(BgL_auxz00_6765,BFALSE,BFALSE);} 
BgL_n1z00_2813 = 
(long)CINT(BgL_tmpz00_6762); } 
{ /* Ieee/port.scm 1134 */
 obj_t BgL_tmpz00_6770;
if(
INTEGERP(BgL_endz00_84))
{ /* Ieee/port.scm 1134 */
BgL_tmpz00_6770 = BgL_endz00_84
; }  else 
{ 
 obj_t BgL_auxz00_6773;
BgL_auxz00_6773 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50181L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_84); 
FAILURE(BgL_auxz00_6773,BFALSE,BFALSE);} 
BgL_n2z00_2814 = 
(long)CINT(BgL_tmpz00_6770); } 
BgL_test3624z00_6761 = 
(BgL_n1z00_2813>BgL_n2z00_2814); } 
if(BgL_test3624z00_6761)
{ /* Ieee/port.scm 1135 */
 obj_t BgL_aux2401z00_4192;
BgL_aux2401z00_4192 = 
BGl_errorz00zz__errorz00(BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2862z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
if(
INPUT_PORTP(BgL_aux2401z00_4192))
{ /* Ieee/port.scm 1135 */
return BgL_aux2401z00_4192;}  else 
{ 
 obj_t BgL_auxz00_6782;
BgL_auxz00_6782 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50193L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2401z00_4192); 
FAILURE(BgL_auxz00_6782,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1136 */
 bool_t BgL_test3628z00_6786;
{ /* Ieee/port.scm 1136 */
 long BgL_n1z00_2816;
{ /* Ieee/port.scm 1136 */
 obj_t BgL_tmpz00_6787;
if(
INTEGERP(BgL_endz00_84))
{ /* Ieee/port.scm 1136 */
BgL_tmpz00_6787 = BgL_endz00_84
; }  else 
{ 
 obj_t BgL_auxz00_6790;
BgL_auxz00_6790 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50273L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_84); 
FAILURE(BgL_auxz00_6790,BFALSE,BFALSE);} 
BgL_n1z00_2816 = 
(long)CINT(BgL_tmpz00_6787); } 
BgL_test3628z00_6786 = 
(BgL_n1z00_2816>
STRING_LENGTH(BgL_stringz00_82)); } 
if(BgL_test3628z00_6786)
{ /* Ieee/port.scm 1137 */
 obj_t BgL_aux2404z00_4195;
BgL_aux2404z00_4195 = 
BGl_errorz00zz__errorz00(BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2863z00zz__r4_ports_6_10_1z00, BgL_endz00_84); 
if(
INPUT_PORTP(BgL_aux2404z00_4195))
{ /* Ieee/port.scm 1137 */
return BgL_aux2404z00_4195;}  else 
{ 
 obj_t BgL_auxz00_6800;
BgL_auxz00_6800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50308L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2404z00_4195); 
FAILURE(BgL_auxz00_6800,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1139 */
 long BgL_auxz00_6813; long BgL_tmpz00_6804;
{ /* Ieee/port.scm 1139 */
 obj_t BgL_tmpz00_6814;
if(
INTEGERP(BgL_endz00_84))
{ /* Ieee/port.scm 1139 */
BgL_tmpz00_6814 = BgL_endz00_84
; }  else 
{ 
 obj_t BgL_auxz00_6817;
BgL_auxz00_6817 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50425L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_84); 
FAILURE(BgL_auxz00_6817,BFALSE,BFALSE);} 
BgL_auxz00_6813 = 
(long)CINT(BgL_tmpz00_6814); } 
{ /* Ieee/port.scm 1139 */
 obj_t BgL_tmpz00_6805;
if(
INTEGERP(BgL_startz00_83))
{ /* Ieee/port.scm 1139 */
BgL_tmpz00_6805 = BgL_startz00_83
; }  else 
{ 
 obj_t BgL_auxz00_6808;
BgL_auxz00_6808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50419L), BGl_string2865z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_83); 
FAILURE(BgL_auxz00_6808,BFALSE,BFALSE);} 
BgL_tmpz00_6804 = 
(long)CINT(BgL_tmpz00_6805); } 
return 
bgl_open_input_substring_bang(BgL_stringz00_82, BgL_tmpz00_6804, BgL_auxz00_6813);} } } } } } 

}



/* _open-input-mmap */
obj_t BGl__openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t BgL_env1276z00_91, obj_t BgL_opt1275z00_90)
{
{ /* Ieee/port.scm 1144 */
{ /* Ieee/port.scm 1144 */
 obj_t BgL_mmapz00_1805;
BgL_mmapz00_1805 = 
VECTOR_REF(BgL_opt1275z00_90,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1275z00_90)) { case 1L : 

{ /* Ieee/port.scm 1145 */
 long BgL_endz00_1809;
{ /* Ieee/port.scm 1145 */
 long BgL_arg1549z00_1810;
{ /* Ieee/port.scm 1145 */
 obj_t BgL_objz00_2818;
if(
BGL_MMAPP(BgL_mmapz00_1805))
{ /* Ieee/port.scm 1145 */
BgL_objz00_2818 = BgL_mmapz00_1805; }  else 
{ 
 obj_t BgL_auxz00_6826;
BgL_auxz00_6826 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50750L), BGl_string2869z00zz__r4_ports_6_10_1z00, BGl_string2870z00zz__r4_ports_6_10_1z00, BgL_mmapz00_1805); 
FAILURE(BgL_auxz00_6826,BFALSE,BFALSE);} 
BgL_arg1549z00_1810 = 
BGL_MMAP_LENGTH(BgL_objz00_2818); } 
BgL_endz00_1809 = 
(long)(BgL_arg1549z00_1810); } 
{ /* Ieee/port.scm 1144 */

{ /* Ieee/port.scm 1144 */
 obj_t BgL_auxz00_6832;
if(
BGL_MMAPP(BgL_mmapz00_1805))
{ /* Ieee/port.scm 1144 */
BgL_auxz00_6832 = BgL_mmapz00_1805
; }  else 
{ 
 obj_t BgL_auxz00_6835;
BgL_auxz00_6835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50656L), BGl_string2869z00zz__r4_ports_6_10_1z00, BGl_string2870z00zz__r4_ports_6_10_1z00, BgL_mmapz00_1805); 
FAILURE(BgL_auxz00_6835,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(BgL_auxz00_6832, 
BINT(0L), 
BINT(BgL_endz00_1809));} } } break;case 2L : 

{ /* Ieee/port.scm 1144 */
 obj_t BgL_startz00_1811;
BgL_startz00_1811 = 
VECTOR_REF(BgL_opt1275z00_90,1L); 
{ /* Ieee/port.scm 1145 */
 long BgL_endz00_1812;
{ /* Ieee/port.scm 1145 */
 long BgL_arg1552z00_1813;
{ /* Ieee/port.scm 1145 */
 obj_t BgL_objz00_2820;
if(
BGL_MMAPP(BgL_mmapz00_1805))
{ /* Ieee/port.scm 1145 */
BgL_objz00_2820 = BgL_mmapz00_1805; }  else 
{ 
 obj_t BgL_auxz00_6845;
BgL_auxz00_6845 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50750L), BGl_string2869z00zz__r4_ports_6_10_1z00, BGl_string2870z00zz__r4_ports_6_10_1z00, BgL_mmapz00_1805); 
FAILURE(BgL_auxz00_6845,BFALSE,BFALSE);} 
BgL_arg1552z00_1813 = 
BGL_MMAP_LENGTH(BgL_objz00_2820); } 
BgL_endz00_1812 = 
(long)(BgL_arg1552z00_1813); } 
{ /* Ieee/port.scm 1144 */

{ /* Ieee/port.scm 1144 */
 obj_t BgL_auxz00_6851;
if(
BGL_MMAPP(BgL_mmapz00_1805))
{ /* Ieee/port.scm 1144 */
BgL_auxz00_6851 = BgL_mmapz00_1805
; }  else 
{ 
 obj_t BgL_auxz00_6854;
BgL_auxz00_6854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50656L), BGl_string2869z00zz__r4_ports_6_10_1z00, BGl_string2870z00zz__r4_ports_6_10_1z00, BgL_mmapz00_1805); 
FAILURE(BgL_auxz00_6854,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(BgL_auxz00_6851, BgL_startz00_1811, 
BINT(BgL_endz00_1812));} } } } break;case 3L : 

{ /* Ieee/port.scm 1144 */
 obj_t BgL_startz00_1814;
BgL_startz00_1814 = 
VECTOR_REF(BgL_opt1275z00_90,1L); 
{ /* Ieee/port.scm 1144 */
 obj_t BgL_endz00_1815;
BgL_endz00_1815 = 
VECTOR_REF(BgL_opt1275z00_90,2L); 
{ /* Ieee/port.scm 1144 */

{ /* Ieee/port.scm 1144 */
 obj_t BgL_auxz00_6862;
if(
BGL_MMAPP(BgL_mmapz00_1805))
{ /* Ieee/port.scm 1144 */
BgL_auxz00_6862 = BgL_mmapz00_1805
; }  else 
{ 
 obj_t BgL_auxz00_6865;
BgL_auxz00_6865 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50656L), BGl_string2869z00zz__r4_ports_6_10_1z00, BGl_string2870z00zz__r4_ports_6_10_1z00, BgL_mmapz00_1805); 
FAILURE(BgL_auxz00_6865,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(BgL_auxz00_6862, BgL_startz00_1814, BgL_endz00_1815);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2867z00zz__r4_ports_6_10_1z00, BGl_string2840z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1275z00_90)));} } } } 

}



/* open-input-mmap */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2mmapz00zz__r4_ports_6_10_1z00(obj_t BgL_mmapz00_87, obj_t BgL_startz00_88, obj_t BgL_endz00_89)
{
{ /* Ieee/port.scm 1144 */
{ /* Ieee/port.scm 1147 */
 bool_t BgL_test3638z00_6875;
{ /* Ieee/port.scm 1147 */
 long BgL_n1z00_2822;
{ /* Ieee/port.scm 1147 */
 obj_t BgL_tmpz00_6876;
if(
INTEGERP(BgL_startz00_88))
{ /* Ieee/port.scm 1147 */
BgL_tmpz00_6876 = BgL_startz00_88
; }  else 
{ 
 obj_t BgL_auxz00_6879;
BgL_auxz00_6879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50780L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
FAILURE(BgL_auxz00_6879,BFALSE,BFALSE);} 
BgL_n1z00_2822 = 
(long)CINT(BgL_tmpz00_6876); } 
BgL_test3638z00_6875 = 
(BgL_n1z00_2822<0L); } 
if(BgL_test3638z00_6875)
{ /* Ieee/port.scm 1148 */
 obj_t BgL_aux2419z00_4210;
BgL_aux2419z00_4210 = 
BGl_errorz00zz__errorz00(BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2860z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
if(
INPUT_PORTP(BgL_aux2419z00_4210))
{ /* Ieee/port.scm 1148 */
return BgL_aux2419z00_4210;}  else 
{ 
 obj_t BgL_auxz00_6888;
BgL_auxz00_6888 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50796L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2419z00_4210); 
FAILURE(BgL_auxz00_6888,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1149 */
 bool_t BgL_test3641z00_6892;
{ /* Ieee/port.scm 1149 */
 long BgL_arg1575z00_1832;
{ /* Ieee/port.scm 1149 */
 long BgL_arg1576z00_1833;
BgL_arg1576z00_1833 = 
BGL_MMAP_LENGTH(BgL_mmapz00_87); 
BgL_arg1575z00_1832 = 
(long)(BgL_arg1576z00_1833); } 
{ /* Ieee/port.scm 1149 */
 long BgL_n1z00_2825;
{ /* Ieee/port.scm 1149 */
 obj_t BgL_tmpz00_6895;
if(
INTEGERP(BgL_startz00_88))
{ /* Ieee/port.scm 1149 */
BgL_tmpz00_6895 = BgL_startz00_88
; }  else 
{ 
 obj_t BgL_auxz00_6898;
BgL_auxz00_6898 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50864L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
FAILURE(BgL_auxz00_6898,BFALSE,BFALSE);} 
BgL_n1z00_2825 = 
(long)CINT(BgL_tmpz00_6895); } 
BgL_test3641z00_6892 = 
(BgL_n1z00_2825>BgL_arg1575z00_1832); } } 
if(BgL_test3641z00_6892)
{ /* Ieee/port.scm 1150 */
 obj_t BgL_aux2422z00_4213;
BgL_aux2422z00_4213 = 
BGl_errorz00zz__errorz00(BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2861z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
if(
INPUT_PORTP(BgL_aux2422z00_4213))
{ /* Ieee/port.scm 1150 */
return BgL_aux2422z00_4213;}  else 
{ 
 obj_t BgL_auxz00_6907;
BgL_auxz00_6907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50913L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2422z00_4213); 
FAILURE(BgL_auxz00_6907,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1151 */
 bool_t BgL_test3644z00_6911;
{ /* Ieee/port.scm 1151 */
 long BgL_n1z00_2827; long BgL_n2z00_2828;
{ /* Ieee/port.scm 1151 */
 obj_t BgL_tmpz00_6912;
if(
INTEGERP(BgL_startz00_88))
{ /* Ieee/port.scm 1151 */
BgL_tmpz00_6912 = BgL_startz00_88
; }  else 
{ 
 obj_t BgL_auxz00_6915;
BgL_auxz00_6915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50987L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
FAILURE(BgL_auxz00_6915,BFALSE,BFALSE);} 
BgL_n1z00_2827 = 
(long)CINT(BgL_tmpz00_6912); } 
{ /* Ieee/port.scm 1151 */
 obj_t BgL_tmpz00_6920;
if(
INTEGERP(BgL_endz00_89))
{ /* Ieee/port.scm 1151 */
BgL_tmpz00_6920 = BgL_endz00_89
; }  else 
{ 
 obj_t BgL_auxz00_6923;
BgL_auxz00_6923 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(50993L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_89); 
FAILURE(BgL_auxz00_6923,BFALSE,BFALSE);} 
BgL_n2z00_2828 = 
(long)CINT(BgL_tmpz00_6920); } 
BgL_test3644z00_6911 = 
(BgL_n1z00_2827>BgL_n2z00_2828); } 
if(BgL_test3644z00_6911)
{ /* Ieee/port.scm 1152 */
 obj_t BgL_aux2426z00_4217;
BgL_aux2426z00_4217 = 
BGl_errorz00zz__errorz00(BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2862z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
if(
INPUT_PORTP(BgL_aux2426z00_4217))
{ /* Ieee/port.scm 1152 */
return BgL_aux2426z00_4217;}  else 
{ 
 obj_t BgL_auxz00_6932;
BgL_auxz00_6932 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51005L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2426z00_4217); 
FAILURE(BgL_auxz00_6932,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1153 */
 bool_t BgL_test3648z00_6936;
{ /* Ieee/port.scm 1153 */
 long BgL_arg1571z00_1830;
{ /* Ieee/port.scm 1153 */
 long BgL_arg1573z00_1831;
BgL_arg1573z00_1831 = 
BGL_MMAP_LENGTH(BgL_mmapz00_87); 
BgL_arg1571z00_1830 = 
(long)(BgL_arg1573z00_1831); } 
{ /* Ieee/port.scm 1153 */
 long BgL_n1z00_2831;
{ /* Ieee/port.scm 1153 */
 obj_t BgL_tmpz00_6939;
if(
INTEGERP(BgL_endz00_89))
{ /* Ieee/port.scm 1153 */
BgL_tmpz00_6939 = BgL_endz00_89
; }  else 
{ 
 obj_t BgL_auxz00_6942;
BgL_auxz00_6942 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51082L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_89); 
FAILURE(BgL_auxz00_6942,BFALSE,BFALSE);} 
BgL_n1z00_2831 = 
(long)CINT(BgL_tmpz00_6939); } 
BgL_test3648z00_6936 = 
(BgL_n1z00_2831>BgL_arg1571z00_1830); } } 
if(BgL_test3648z00_6936)
{ /* Ieee/port.scm 1154 */
 obj_t BgL_aux2429z00_4220;
BgL_aux2429z00_4220 = 
BGl_errorz00zz__errorz00(BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2863z00zz__r4_ports_6_10_1z00, BgL_endz00_89); 
if(
INPUT_PORTP(BgL_aux2429z00_4220))
{ /* Ieee/port.scm 1154 */
return BgL_aux2429z00_4220;}  else 
{ 
 obj_t BgL_auxz00_6951;
BgL_auxz00_6951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51129L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_aux2429z00_4220); 
FAILURE(BgL_auxz00_6951,BFALSE,BFALSE);} }  else 
{ /* Ieee/port.scm 1156 */
 obj_t BgL_bufferz00_1825;
BgL_bufferz00_1825 = 
make_string_sans_fill(2L); 
{ /* Ieee/port.scm 1160 */
 long BgL_auxz00_6965; long BgL_tmpz00_6956;
{ /* Ieee/port.scm 1160 */
 obj_t BgL_tmpz00_6966;
if(
INTEGERP(BgL_endz00_89))
{ /* Ieee/port.scm 1160 */
BgL_tmpz00_6966 = BgL_endz00_89
; }  else 
{ 
 obj_t BgL_auxz00_6969;
BgL_auxz00_6969 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51407L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_endz00_89); 
FAILURE(BgL_auxz00_6969,BFALSE,BFALSE);} 
BgL_auxz00_6965 = 
(long)CINT(BgL_tmpz00_6966); } 
{ /* Ieee/port.scm 1160 */
 obj_t BgL_tmpz00_6957;
if(
INTEGERP(BgL_startz00_88))
{ /* Ieee/port.scm 1160 */
BgL_tmpz00_6957 = BgL_startz00_88
; }  else 
{ 
 obj_t BgL_auxz00_6960;
BgL_auxz00_6960 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51401L), BGl_string2868z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_startz00_88); 
FAILURE(BgL_auxz00_6960,BFALSE,BFALSE);} 
BgL_tmpz00_6956 = 
(long)CINT(BgL_tmpz00_6957); } 
return 
bgl_open_input_mmap(BgL_mmapz00_87, BgL_bufferz00_1825, BgL_tmpz00_6956, BgL_auxz00_6965);} } } } } } } 

}



/* _open-input-procedure */
obj_t BGl__openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t BgL_env1280z00_95, obj_t BgL_opt1279z00_94)
{
{ /* Ieee/port.scm 1165 */
{ /* Ieee/port.scm 1165 */
 obj_t BgL_g1281z00_1834;
BgL_g1281z00_1834 = 
VECTOR_REF(BgL_opt1279z00_94,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1279z00_94)) { case 1L : 

{ /* Ieee/port.scm 1165 */

{ /* Ieee/port.scm 1165 */
 obj_t BgL_procz00_2845;
if(
PROCEDUREP(BgL_g1281z00_1834))
{ /* Ieee/port.scm 1165 */
BgL_procz00_2845 = BgL_g1281z00_1834; }  else 
{ 
 obj_t BgL_auxz00_6978;
BgL_auxz00_6978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51639L), BGl_string2872z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_g1281z00_1834); 
FAILURE(BgL_auxz00_6978,BFALSE,BFALSE);} 
return 
bgl_open_input_procedure(BgL_procz00_2845, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2753z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(1024L)));} } break;case 2L : 

{ /* Ieee/port.scm 1165 */
 obj_t BgL_bufinfoz00_1838;
BgL_bufinfoz00_1838 = 
VECTOR_REF(BgL_opt1279z00_94,1L); 
{ /* Ieee/port.scm 1165 */

{ /* Ieee/port.scm 1165 */
 obj_t BgL_procz00_2847;
if(
PROCEDUREP(BgL_g1281z00_1834))
{ /* Ieee/port.scm 1165 */
BgL_procz00_2847 = BgL_g1281z00_1834; }  else 
{ 
 obj_t BgL_auxz00_6988;
BgL_auxz00_6988 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(51639L), BGl_string2872z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_g1281z00_1834); 
FAILURE(BgL_auxz00_6988,BFALSE,BFALSE);} 
return 
bgl_open_input_procedure(BgL_procz00_2847, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2753z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1838, 
(int)(1024L)));} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2871z00zz__r4_ports_6_10_1z00, BGl_string2855z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1279z00_94)));} } } } 

}



/* open-input-procedure */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t BgL_procz00_92, obj_t BgL_bufinfoz00_93)
{
{ /* Ieee/port.scm 1165 */
{ /* Ieee/port.scm 1166 */
 obj_t BgL_bufz00_2849;
{ /* Ieee/port.scm 1166 */
 obj_t BgL_res2075z00_2854;
if(
(BgL_bufinfoz00_93==BTRUE))
{ /* Ieee/port.scm 974 */
BgL_res2075z00_2854 = 
make_string_sans_fill(1024L); }  else 
{ /* Ieee/port.scm 974 */
if(
(BgL_bufinfoz00_93==BFALSE))
{ /* Ieee/port.scm 976 */
BgL_res2075z00_2854 = 
make_string_sans_fill(2L); }  else 
{ /* Ieee/port.scm 976 */
if(
STRINGP(BgL_bufinfoz00_93))
{ /* Ieee/port.scm 978 */
BgL_res2075z00_2854 = BgL_bufinfoz00_93; }  else 
{ /* Ieee/port.scm 978 */
if(
INTEGERP(BgL_bufinfoz00_93))
{ /* Ieee/port.scm 980 */
if(
(
(long)CINT(BgL_bufinfoz00_93)>=2L))
{ /* Ieee/port.scm 981 */
BgL_res2075z00_2854 = 
make_string_sans_fill(
(long)CINT(BgL_bufinfoz00_93)); }  else 
{ /* Ieee/port.scm 981 */
BgL_res2075z00_2854 = 
make_string_sans_fill(2L); } }  else 
{ /* Ieee/port.scm 985 */
 obj_t BgL_aux2437z00_4228;
BgL_aux2437z00_4228 = 
BGl_errorz00zz__errorz00(BGl_string2753z00zz__r4_ports_6_10_1z00, BGl_string2798z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_93); 
if(
STRINGP(BgL_aux2437z00_4228))
{ /* Ieee/port.scm 985 */
BgL_res2075z00_2854 = BgL_aux2437z00_4228; }  else 
{ 
 obj_t BgL_auxz00_7019;
BgL_auxz00_7019 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43135L), BGl_string2753z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2437z00_4228); 
FAILURE(BgL_auxz00_7019,BFALSE,BFALSE);} } } } } 
BgL_bufz00_2849 = BgL_res2075z00_2854; } 
return 
bgl_open_input_procedure(BgL_procz00_92, BgL_bufz00_2849);} } 

}



/* _open-input-gzip-port */
obj_t BGl__openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t BgL_env1285z00_99, obj_t BgL_opt1284z00_98)
{
{ /* Ieee/port.scm 1172 */
{ /* Ieee/port.scm 1172 */
 obj_t BgL_g1286z00_1841;
BgL_g1286z00_1841 = 
VECTOR_REF(BgL_opt1284z00_98,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1284z00_98)) { case 1L : 

{ /* Ieee/port.scm 1172 */

{ /* Ieee/port.scm 1172 */
 obj_t BgL_inz00_2855;
if(
INPUT_PORTP(BgL_g1286z00_1841))
{ /* Ieee/port.scm 1172 */
BgL_inz00_2855 = BgL_g1286z00_1841; }  else 
{ 
 obj_t BgL_auxz00_7027;
BgL_auxz00_7027 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(52033L), BGl_string2875z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_g1286z00_1841); 
FAILURE(BgL_auxz00_7027,BFALSE,BFALSE);} 
return 
BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(BgL_inz00_2855, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2874z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz)));} } break;case 2L : 

{ /* Ieee/port.scm 1172 */
 obj_t BgL_bufinfoz00_1845;
BgL_bufinfoz00_1845 = 
VECTOR_REF(BgL_opt1284z00_98,1L); 
{ /* Ieee/port.scm 1172 */

{ /* Ieee/port.scm 1172 */
 obj_t BgL_inz00_2857;
if(
INPUT_PORTP(BgL_g1286z00_1841))
{ /* Ieee/port.scm 1172 */
BgL_inz00_2857 = BgL_g1286z00_1841; }  else 
{ 
 obj_t BgL_auxz00_7037;
BgL_auxz00_7037 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(52033L), BGl_string2875z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_g1286z00_1841); 
FAILURE(BgL_auxz00_7037,BFALSE,BFALSE);} 
return 
BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(BgL_inz00_2857, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2874z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1845, 
(int)(default_io_bufsiz)));} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2873z00zz__r4_ports_6_10_1z00, BGl_string2855z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1284z00_98)));} } } } 

}



/* open-input-gzip-port */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2gza7ipzd2portz75zz__r4_ports_6_10_1z00(obj_t BgL_inz00_96, obj_t BgL_bufinfoz00_97)
{
{ /* Ieee/port.scm 1172 */
BGL_TAIL return 
BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(BgL_inz00_96, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2874z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_97, 
(int)(default_io_bufsiz)));} 

}



/* open-input-c-string */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(char * BgL_stringz00_100)
{
{ /* Ieee/port.scm 1179 */
BGL_TAIL return 
bgl_open_input_c_string(BgL_stringz00_100);} 

}



/* &open-input-c-string */
obj_t BGl_z62openzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3697, obj_t BgL_stringz00_3698)
{
{ /* Ieee/port.scm 1179 */
{ /* Ieee/port.scm 1180 */
 char * BgL_auxz00_7053;
{ /* Ieee/port.scm 1180 */
 obj_t BgL_tmpz00_7054;
if(
STRINGP(BgL_stringz00_3698))
{ /* Ieee/port.scm 1180 */
BgL_tmpz00_7054 = BgL_stringz00_3698
; }  else 
{ 
 obj_t BgL_auxz00_7057;
BgL_auxz00_7057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(52491L), BGl_string2876z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3698); 
FAILURE(BgL_auxz00_7057,BFALSE,BFALSE);} 
BgL_auxz00_7053 = 
BSTRING_TO_STRING(BgL_tmpz00_7054); } 
return 
BGl_openzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7053);} } 

}



/* reopen-input-c-string */
BGL_EXPORTED_DEF obj_t BGl_reopenzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_101, char * BgL_stringz00_102)
{
{ /* Ieee/port.scm 1185 */
BGL_TAIL return 
bgl_reopen_input_c_string(BgL_portz00_101, BgL_stringz00_102);} 

}



/* &reopen-input-c-string */
obj_t BGl_z62reopenzd2inputzd2czd2stringzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3699, obj_t BgL_portz00_3700, obj_t BgL_stringz00_3701)
{
{ /* Ieee/port.scm 1185 */
{ /* Ieee/port.scm 1186 */
 char * BgL_auxz00_7071; obj_t BgL_auxz00_7064;
{ /* Ieee/port.scm 1186 */
 obj_t BgL_tmpz00_7072;
if(
STRINGP(BgL_stringz00_3701))
{ /* Ieee/port.scm 1186 */
BgL_tmpz00_7072 = BgL_stringz00_3701
; }  else 
{ 
 obj_t BgL_auxz00_7075;
BgL_auxz00_7075 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(52811L), BGl_string2877z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3701); 
FAILURE(BgL_auxz00_7075,BFALSE,BFALSE);} 
BgL_auxz00_7071 = 
BSTRING_TO_STRING(BgL_tmpz00_7072); } 
if(
INPUT_PORTP(BgL_portz00_3700))
{ /* Ieee/port.scm 1186 */
BgL_auxz00_7064 = BgL_portz00_3700
; }  else 
{ 
 obj_t BgL_auxz00_7067;
BgL_auxz00_7067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(52811L), BGl_string2877z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3700); 
FAILURE(BgL_auxz00_7067,BFALSE,BFALSE);} 
return 
BGl_reopenzd2inputzd2czd2stringzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7064, BgL_auxz00_7071);} } 

}



/* input-port-timeout */
BGL_EXPORTED_DEF long BGl_inputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_103)
{
{ /* Ieee/port.scm 1191 */
BGL_TAIL return 
bgl_input_port_timeout(BgL_portz00_103);} 

}



/* &input-port-timeout */
obj_t BGl_z62inputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3702, obj_t BgL_portz00_3703)
{
{ /* Ieee/port.scm 1191 */
{ /* Ieee/port.scm 1192 */
 long BgL_tmpz00_7082;
{ /* Ieee/port.scm 1192 */
 obj_t BgL_auxz00_7083;
if(
INPUT_PORTP(BgL_portz00_3703))
{ /* Ieee/port.scm 1192 */
BgL_auxz00_7083 = BgL_portz00_3703
; }  else 
{ 
 obj_t BgL_auxz00_7086;
BgL_auxz00_7086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(53134L), BGl_string2878z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3703); 
FAILURE(BgL_auxz00_7086,BFALSE,BFALSE);} 
BgL_tmpz00_7082 = 
BGl_inputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(BgL_auxz00_7083); } 
return 
BINT(BgL_tmpz00_7082);} } 

}



/* input-port-timeout-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_104, long BgL_timeoutz00_105)
{
{ /* Ieee/port.scm 1197 */
return 
BBOOL(
bgl_input_port_timeout_set(BgL_portz00_104, BgL_timeoutz00_105));} 

}



/* &input-port-timeout-set! */
obj_t BGl_z62inputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3704, obj_t BgL_portz00_3705, obj_t BgL_timeoutz00_3706)
{
{ /* Ieee/port.scm 1197 */
{ /* Ieee/port.scm 1198 */
 long BgL_auxz00_7101; obj_t BgL_auxz00_7094;
{ /* Ieee/port.scm 1198 */
 obj_t BgL_tmpz00_7102;
if(
INTEGERP(BgL_timeoutz00_3706))
{ /* Ieee/port.scm 1198 */
BgL_tmpz00_7102 = BgL_timeoutz00_3706
; }  else 
{ 
 obj_t BgL_auxz00_7105;
BgL_auxz00_7105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(53460L), BGl_string2879z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_timeoutz00_3706); 
FAILURE(BgL_auxz00_7105,BFALSE,BFALSE);} 
BgL_auxz00_7101 = 
(long)CINT(BgL_tmpz00_7102); } 
if(
INPUT_PORTP(BgL_portz00_3705))
{ /* Ieee/port.scm 1198 */
BgL_auxz00_7094 = BgL_portz00_3705
; }  else 
{ 
 obj_t BgL_auxz00_7097;
BgL_auxz00_7097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(53460L), BGl_string2879z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3705); 
FAILURE(BgL_auxz00_7097,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7094, BgL_auxz00_7101);} } 

}



/* output-port-timeout */
BGL_EXPORTED_DEF long BGl_outputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_106)
{
{ /* Ieee/port.scm 1203 */
BGL_TAIL return 
bgl_output_port_timeout(BgL_portz00_106);} 

}



/* &output-port-timeout */
obj_t BGl_z62outputzd2portzd2timeoutz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3707, obj_t BgL_portz00_3708)
{
{ /* Ieee/port.scm 1203 */
{ /* Ieee/port.scm 1204 */
 long BgL_tmpz00_7112;
{ /* Ieee/port.scm 1204 */
 obj_t BgL_auxz00_7113;
if(
OUTPUT_PORTP(BgL_portz00_3708))
{ /* Ieee/port.scm 1204 */
BgL_auxz00_7113 = BgL_portz00_3708
; }  else 
{ 
 obj_t BgL_auxz00_7116;
BgL_auxz00_7116 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(53788L), BGl_string2880z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3708); 
FAILURE(BgL_auxz00_7116,BFALSE,BFALSE);} 
BgL_tmpz00_7112 = 
BGl_outputzd2portzd2timeoutz00zz__r4_ports_6_10_1z00(BgL_auxz00_7113); } 
return 
BINT(BgL_tmpz00_7112);} } 

}



/* _open-output-file */
obj_t BGl__openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_env1290z00_110, obj_t BgL_opt1289z00_109)
{
{ /* Ieee/port.scm 1209 */
{ /* Ieee/port.scm 1209 */
 obj_t BgL_g1291z00_1848;
BgL_g1291z00_1848 = 
VECTOR_REF(BgL_opt1289z00_109,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1289z00_109)) { case 1L : 

{ /* Ieee/port.scm 1209 */

{ /* Ieee/port.scm 1209 */
 obj_t BgL_stringz00_2860;
if(
STRINGP(BgL_g1291z00_1848))
{ /* Ieee/port.scm 1209 */
BgL_stringz00_2860 = BgL_g1291z00_1848; }  else 
{ 
 obj_t BgL_auxz00_7125;
BgL_auxz00_7125 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(54040L), BGl_string2882z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1291z00_1848); 
FAILURE(BgL_auxz00_7125,BFALSE,BFALSE);} 
return 
bgl_open_output_file(BgL_stringz00_2860, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz)));} } break;case 2L : 

{ /* Ieee/port.scm 1209 */
 obj_t BgL_bufinfoz00_1852;
BgL_bufinfoz00_1852 = 
VECTOR_REF(BgL_opt1289z00_109,1L); 
{ /* Ieee/port.scm 1209 */

{ /* Ieee/port.scm 1209 */
 obj_t BgL_stringz00_2862;
if(
STRINGP(BgL_g1291z00_1848))
{ /* Ieee/port.scm 1209 */
BgL_stringz00_2862 = BgL_g1291z00_1848; }  else 
{ 
 obj_t BgL_auxz00_7135;
BgL_auxz00_7135 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(54040L), BGl_string2882z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1291z00_1848); 
FAILURE(BgL_auxz00_7135,BFALSE,BFALSE);} 
return 
bgl_open_output_file(BgL_stringz00_2862, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1852, 
(int)(default_io_bufsiz)));} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2881z00zz__r4_ports_6_10_1z00, BGl_string2855z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1289z00_109)));} } } } 

}



/* open-output-file */
BGL_EXPORTED_DEF obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_107, obj_t BgL_bufinfoz00_108)
{
{ /* Ieee/port.scm 1209 */
return 
bgl_open_output_file(BgL_stringz00_107, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_108, 
(int)(default_io_bufsiz)));} 

}



/* _append-output-file */
obj_t BGl__appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_env1295z00_114, obj_t BgL_opt1294z00_113)
{
{ /* Ieee/port.scm 1216 */
{ /* Ieee/port.scm 1216 */
 obj_t BgL_g1296z00_1855;
BgL_g1296z00_1855 = 
VECTOR_REF(BgL_opt1294z00_113,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1294z00_113)) { case 1L : 

{ /* Ieee/port.scm 1216 */

{ /* Ieee/port.scm 1216 */
 obj_t BgL_stringz00_2865;
if(
STRINGP(BgL_g1296z00_1855))
{ /* Ieee/port.scm 1216 */
BgL_stringz00_2865 = BgL_g1296z00_1855; }  else 
{ 
 obj_t BgL_auxz00_7153;
BgL_auxz00_7153 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(54441L), BGl_string2884z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1296z00_1855); 
FAILURE(BgL_auxz00_7153,BFALSE,BFALSE);} 
return 
bgl_append_output_file(BgL_stringz00_2865, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2728z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(default_io_bufsiz)));} } break;case 2L : 

{ /* Ieee/port.scm 1216 */
 obj_t BgL_bufinfoz00_1859;
BgL_bufinfoz00_1859 = 
VECTOR_REF(BgL_opt1294z00_113,1L); 
{ /* Ieee/port.scm 1216 */

{ /* Ieee/port.scm 1216 */
 obj_t BgL_stringz00_2867;
if(
STRINGP(BgL_g1296z00_1855))
{ /* Ieee/port.scm 1216 */
BgL_stringz00_2867 = BgL_g1296z00_1855; }  else 
{ 
 obj_t BgL_auxz00_7163;
BgL_auxz00_7163 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(54441L), BGl_string2884z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_g1296z00_1855); 
FAILURE(BgL_auxz00_7163,BFALSE,BFALSE);} 
return 
bgl_append_output_file(BgL_stringz00_2867, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2728z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1859, 
(int)(default_io_bufsiz)));} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2883z00zz__r4_ports_6_10_1z00, BGl_string2855z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1294z00_113)));} } } } 

}



/* append-output-file */
BGL_EXPORTED_DEF obj_t BGl_appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_111, obj_t BgL_bufinfoz00_112)
{
{ /* Ieee/port.scm 1216 */
return 
bgl_append_output_file(BgL_stringz00_111, 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2728z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_112, 
(int)(default_io_bufsiz)));} 

}



/* _open-output-string */
obj_t BGl__openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_env1300z00_117, obj_t BgL_opt1299z00_116)
{
{ /* Ieee/port.scm 1223 */
{ /* Ieee/port.scm 1223 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1299z00_116)) { case 0L : 

{ /* Ieee/port.scm 1223 */

return 
bgl_open_output_string(
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BTRUE, 
(int)(128L)));} break;case 1L : 

{ /* Ieee/port.scm 1223 */
 obj_t BgL_bufinfoz00_1865;
BgL_bufinfoz00_1865 = 
VECTOR_REF(BgL_opt1299z00_116,0L); 
{ /* Ieee/port.scm 1223 */

return 
bgl_open_output_string(
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2722z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_1865, 
(int)(128L)));} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2885z00zz__r4_ports_6_10_1z00, BGl_string2887z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1299z00_116)));} } } } 

}



/* open-output-string */
BGL_EXPORTED_DEF obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_bufinfoz00_115)
{
{ /* Ieee/port.scm 1223 */
{ /* Ieee/port.scm 1224 */
 obj_t BgL_bufz00_2872;
{ /* Ieee/port.scm 1224 */
 obj_t BgL_res2076z00_2877;
if(
(BgL_bufinfoz00_115==BTRUE))
{ /* Ieee/port.scm 974 */
BgL_res2076z00_2877 = 
make_string_sans_fill(128L); }  else 
{ /* Ieee/port.scm 974 */
if(
(BgL_bufinfoz00_115==BFALSE))
{ /* Ieee/port.scm 976 */
BgL_res2076z00_2877 = 
make_string_sans_fill(2L); }  else 
{ /* Ieee/port.scm 976 */
if(
STRINGP(BgL_bufinfoz00_115))
{ /* Ieee/port.scm 978 */
BgL_res2076z00_2877 = BgL_bufinfoz00_115; }  else 
{ /* Ieee/port.scm 978 */
if(
INTEGERP(BgL_bufinfoz00_115))
{ /* Ieee/port.scm 980 */
if(
(
(long)CINT(BgL_bufinfoz00_115)>=2L))
{ /* Ieee/port.scm 981 */
BgL_res2076z00_2877 = 
make_string_sans_fill(
(long)CINT(BgL_bufinfoz00_115)); }  else 
{ /* Ieee/port.scm 981 */
BgL_res2076z00_2877 = 
make_string_sans_fill(2L); } }  else 
{ /* Ieee/port.scm 985 */
 obj_t BgL_aux2464z00_4255;
BgL_aux2464z00_4255 = 
BGl_errorz00zz__errorz00(BGl_string2722z00zz__r4_ports_6_10_1z00, BGl_string2798z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_115); 
if(
STRINGP(BgL_aux2464z00_4255))
{ /* Ieee/port.scm 985 */
BgL_res2076z00_2877 = BgL_aux2464z00_4255; }  else 
{ 
 obj_t BgL_auxz00_7209;
BgL_auxz00_7209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43135L), BGl_string2886z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2464z00_4255); 
FAILURE(BgL_auxz00_7209,BFALSE,BFALSE);} } } } } 
BgL_bufz00_2872 = BgL_res2076z00_2877; } 
return 
bgl_open_output_string(BgL_bufz00_2872);} } 

}



/* _open-output-procedure */
obj_t BGl__openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t BgL_env1304z00_123, obj_t BgL_opt1303z00_122)
{
{ /* Ieee/port.scm 1230 */
{ /* Ieee/port.scm 1230 */
 obj_t BgL_g1305z00_1868;
BgL_g1305z00_1868 = 
VECTOR_REF(BgL_opt1303z00_122,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1303z00_122)) { case 1L : 

{ /* Ieee/port.scm 1230 */

{ /* Ieee/port.scm 1230 */
 obj_t BgL_auxz00_7215;
if(
PROCEDUREP(BgL_g1305z00_1868))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7215 = BgL_g1305z00_1868
; }  else 
{ 
 obj_t BgL_auxz00_7218;
BgL_auxz00_7218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_g1305z00_1868); 
FAILURE(BgL_auxz00_7218,BFALSE,BFALSE);} 
return 
BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(BgL_auxz00_7215, BGl_proc2891z00zz__r4_ports_6_10_1z00, BTRUE, BGl_proc2892z00zz__r4_ports_6_10_1z00);} } break;case 2L : 

{ /* Ieee/port.scm 1230 */
 obj_t BgL_flushz00_1878;
BgL_flushz00_1878 = 
VECTOR_REF(BgL_opt1303z00_122,1L); 
{ /* Ieee/port.scm 1230 */

{ /* Ieee/port.scm 1230 */
 obj_t BgL_auxz00_7231; obj_t BgL_auxz00_7224;
if(
PROCEDUREP(BgL_flushz00_1878))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7231 = BgL_flushz00_1878
; }  else 
{ 
 obj_t BgL_auxz00_7234;
BgL_auxz00_7234 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_flushz00_1878); 
FAILURE(BgL_auxz00_7234,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_g1305z00_1868))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7224 = BgL_g1305z00_1868
; }  else 
{ 
 obj_t BgL_auxz00_7227;
BgL_auxz00_7227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_g1305z00_1868); 
FAILURE(BgL_auxz00_7227,BFALSE,BFALSE);} 
return 
BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(BgL_auxz00_7224, BgL_auxz00_7231, BTRUE, BGl_proc2894z00zz__r4_ports_6_10_1z00);} } } break;case 3L : 

{ /* Ieee/port.scm 1230 */
 obj_t BgL_flushz00_1883;
BgL_flushz00_1883 = 
VECTOR_REF(BgL_opt1303z00_122,1L); 
{ /* Ieee/port.scm 1230 */
 obj_t BgL_bufinfoz00_1884;
BgL_bufinfoz00_1884 = 
VECTOR_REF(BgL_opt1303z00_122,2L); 
{ /* Ieee/port.scm 1230 */

{ /* Ieee/port.scm 1230 */
 obj_t BgL_auxz00_7248; obj_t BgL_auxz00_7241;
if(
PROCEDUREP(BgL_flushz00_1883))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7248 = BgL_flushz00_1883
; }  else 
{ 
 obj_t BgL_auxz00_7251;
BgL_auxz00_7251 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_flushz00_1883); 
FAILURE(BgL_auxz00_7251,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_g1305z00_1868))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7241 = BgL_g1305z00_1868
; }  else 
{ 
 obj_t BgL_auxz00_7244;
BgL_auxz00_7244 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_g1305z00_1868); 
FAILURE(BgL_auxz00_7244,BFALSE,BFALSE);} 
return 
BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(BgL_auxz00_7241, BgL_auxz00_7248, BgL_bufinfoz00_1884, BGl_proc2895z00zz__r4_ports_6_10_1z00);} } } } break;case 4L : 

{ /* Ieee/port.scm 1230 */
 obj_t BgL_flushz00_1888;
BgL_flushz00_1888 = 
VECTOR_REF(BgL_opt1303z00_122,1L); 
{ /* Ieee/port.scm 1230 */
 obj_t BgL_bufinfoz00_1889;
BgL_bufinfoz00_1889 = 
VECTOR_REF(BgL_opt1303z00_122,2L); 
{ /* Ieee/port.scm 1230 */
 obj_t BgL_closez00_1890;
BgL_closez00_1890 = 
VECTOR_REF(BgL_opt1303z00_122,3L); 
{ /* Ieee/port.scm 1230 */

{ /* Ieee/port.scm 1230 */
 obj_t BgL_auxz00_7273; obj_t BgL_auxz00_7266; obj_t BgL_auxz00_7259;
if(
PROCEDUREP(BgL_closez00_1890))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7273 = BgL_closez00_1890
; }  else 
{ 
 obj_t BgL_auxz00_7276;
BgL_auxz00_7276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_closez00_1890); 
FAILURE(BgL_auxz00_7276,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_flushz00_1888))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7266 = BgL_flushz00_1888
; }  else 
{ 
 obj_t BgL_auxz00_7269;
BgL_auxz00_7269 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_flushz00_1888); 
FAILURE(BgL_auxz00_7269,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_g1305z00_1868))
{ /* Ieee/port.scm 1230 */
BgL_auxz00_7259 = BgL_g1305z00_1868
; }  else 
{ 
 obj_t BgL_auxz00_7262;
BgL_auxz00_7262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(55223L), BGl_string2893z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_g1305z00_1868); 
FAILURE(BgL_auxz00_7262,BFALSE,BFALSE);} 
return 
BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(BgL_auxz00_7259, BgL_auxz00_7266, BgL_bufinfoz00_1889, BgL_auxz00_7273);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2888z00zz__r4_ports_6_10_1z00, BGl_string2890z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1303z00_122)));} } } } 

}



/* &close2102 */
obj_t BGl_z62close2102z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3713)
{
{ /* Ieee/port.scm 1234 */
return 
BBOOL(((bool_t)0));} 

}



/* &close2103 */
obj_t BGl_z62close2103z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3714)
{
{ /* Ieee/port.scm 1234 */
return 
BBOOL(((bool_t)0));} 

}



/* &close2104 */
obj_t BGl_z62close2104z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3715)
{
{ /* Ieee/port.scm 1234 */
return 
BBOOL(((bool_t)0));} 

}



/* &flush2105 */
obj_t BGl_z62flush2105z62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3716)
{
{ /* Ieee/port.scm 1232 */
return 
BBOOL(((bool_t)0));} 

}



/* open-output-procedure */
BGL_EXPORTED_DEF obj_t BGl_openzd2outputzd2procedurez00zz__r4_ports_6_10_1z00(obj_t BgL_procz00_118, obj_t BgL_flushz00_119, obj_t BgL_bufinfoz00_120, obj_t BgL_closez00_121)
{
{ /* Ieee/port.scm 1230 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_118, 
(int)(1L)))
{ /* Ieee/port.scm 1241 */
 bool_t BgL_test3689z00_7293;
if(
PROCEDURE_CORRECT_ARITYP(BgL_flushz00_119, 
(int)(0L)))
{ /* Ieee/port.scm 1242 */
BgL_test3689z00_7293 = ((bool_t)0)
; }  else 
{ /* Ieee/port.scm 1242 */
BgL_test3689z00_7293 = ((bool_t)1)
; } 
if(BgL_test3689z00_7293)
{ /* Ieee/port.scm 1241 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2889z00zz__r4_ports_6_10_1z00, BGl_string2896z00zz__r4_ports_6_10_1z00, BgL_flushz00_119);}  else 
{ /* Ieee/port.scm 1247 */
 bool_t BgL_test3691z00_7298;
if(
PROCEDURE_CORRECT_ARITYP(BgL_closez00_121, 
(int)(0L)))
{ /* Ieee/port.scm 1248 */
BgL_test3691z00_7298 = ((bool_t)0)
; }  else 
{ /* Ieee/port.scm 1248 */
BgL_test3691z00_7298 = ((bool_t)1)
; } 
if(BgL_test3691z00_7298)
{ /* Ieee/port.scm 1247 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2889z00zz__r4_ports_6_10_1z00, BGl_string2897z00zz__r4_ports_6_10_1z00, BgL_flushz00_119);}  else 
{ /* Ieee/port.scm 1254 */
 obj_t BgL_bufz00_1896;
{ /* Ieee/port.scm 1254 */
 obj_t BgL_res2077z00_2882;
if(
(BgL_bufinfoz00_120==BTRUE))
{ /* Ieee/port.scm 974 */
BgL_res2077z00_2882 = 
make_string_sans_fill(128L); }  else 
{ /* Ieee/port.scm 974 */
if(
(BgL_bufinfoz00_120==BFALSE))
{ /* Ieee/port.scm 976 */
BgL_res2077z00_2882 = 
make_string_sans_fill(2L); }  else 
{ /* Ieee/port.scm 976 */
if(
STRINGP(BgL_bufinfoz00_120))
{ /* Ieee/port.scm 978 */
BgL_res2077z00_2882 = BgL_bufinfoz00_120; }  else 
{ /* Ieee/port.scm 978 */
if(
INTEGERP(BgL_bufinfoz00_120))
{ /* Ieee/port.scm 980 */
if(
(
(long)CINT(BgL_bufinfoz00_120)>=2L))
{ /* Ieee/port.scm 981 */
BgL_res2077z00_2882 = 
make_string_sans_fill(
(long)CINT(BgL_bufinfoz00_120)); }  else 
{ /* Ieee/port.scm 981 */
BgL_res2077z00_2882 = 
make_string_sans_fill(2L); } }  else 
{ /* Ieee/port.scm 985 */
 obj_t BgL_aux2482z00_4273;
BgL_aux2482z00_4273 = 
BGl_errorz00zz__errorz00(BGl_string2889z00zz__r4_ports_6_10_1z00, BGl_string2798z00zz__r4_ports_6_10_1z00, BgL_bufinfoz00_120); 
if(
STRINGP(BgL_aux2482z00_4273))
{ /* Ieee/port.scm 985 */
BgL_res2077z00_2882 = BgL_aux2482z00_4273; }  else 
{ 
 obj_t BgL_auxz00_7322;
BgL_auxz00_7322 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(43135L), BGl_string2889z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_aux2482z00_4273); 
FAILURE(BgL_auxz00_7322,BFALSE,BFALSE);} } } } } 
BgL_bufz00_1896 = BgL_res2077z00_2882; } 
return 
bgl_open_output_procedure(BgL_procz00_118, BgL_flushz00_119, BgL_closez00_121, BgL_bufz00_1896);} } }  else 
{ /* Ieee/port.scm 1236 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2889z00zz__r4_ports_6_10_1z00, BGl_string2898z00zz__r4_ports_6_10_1z00, BgL_procz00_118);} } 

}



/* output-port-timeout-set! */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_124, long BgL_timeoutz00_125)
{
{ /* Ieee/port.scm 1260 */
return 
BBOOL(
bgl_output_port_timeout_set(BgL_portz00_124, BgL_timeoutz00_125));} 

}



/* &output-port-timeout-set! */
obj_t BGl_z62outputzd2portzd2timeoutzd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3717, obj_t BgL_portz00_3718, obj_t BgL_timeoutz00_3719)
{
{ /* Ieee/port.scm 1260 */
{ /* Ieee/port.scm 1261 */
 long BgL_auxz00_7337; obj_t BgL_auxz00_7330;
{ /* Ieee/port.scm 1261 */
 obj_t BgL_tmpz00_7338;
if(
INTEGERP(BgL_timeoutz00_3719))
{ /* Ieee/port.scm 1261 */
BgL_tmpz00_7338 = BgL_timeoutz00_3719
; }  else 
{ 
 obj_t BgL_auxz00_7341;
BgL_auxz00_7341 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(56320L), BGl_string2899z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_timeoutz00_3719); 
FAILURE(BgL_auxz00_7341,BFALSE,BFALSE);} 
BgL_auxz00_7337 = 
(long)CINT(BgL_tmpz00_7338); } 
if(
OUTPUT_PORTP(BgL_portz00_3718))
{ /* Ieee/port.scm 1261 */
BgL_auxz00_7330 = BgL_portz00_3718
; }  else 
{ 
 obj_t BgL_auxz00_7333;
BgL_auxz00_7333 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(56320L), BGl_string2899z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3718); 
FAILURE(BgL_auxz00_7333,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2timeoutzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7330, BgL_auxz00_7337);} } 

}



/* closed-input-port? */
BGL_EXPORTED_DEF bool_t BGl_closedzd2inputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_portz00_126)
{
{ /* Ieee/port.scm 1266 */
return 
INPUT_PORT_CLOSEP(BgL_portz00_126);} 

}



/* &closed-input-port? */
obj_t BGl_z62closedzd2inputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3720, obj_t BgL_portz00_3721)
{
{ /* Ieee/port.scm 1266 */
{ /* Ieee/port.scm 1267 */
 bool_t BgL_tmpz00_7348;
{ /* Ieee/port.scm 1267 */
 obj_t BgL_auxz00_7349;
if(
INPUT_PORTP(BgL_portz00_3721))
{ /* Ieee/port.scm 1267 */
BgL_auxz00_7349 = BgL_portz00_3721
; }  else 
{ 
 obj_t BgL_auxz00_7352;
BgL_auxz00_7352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(56629L), BGl_string2900z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3721); 
FAILURE(BgL_auxz00_7352,BFALSE,BFALSE);} 
BgL_tmpz00_7348 = 
BGl_closedzd2inputzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_auxz00_7349); } 
return 
BBOOL(BgL_tmpz00_7348);} } 

}



/* close-input-port */
BGL_EXPORTED_DEF obj_t BGl_closezd2inputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_127)
{
{ /* Ieee/port.scm 1272 */
return 
bgl_close_input_port(BgL_portz00_127);} 

}



/* &close-input-port */
obj_t BGl_z62closezd2inputzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3722, obj_t BgL_portz00_3723)
{
{ /* Ieee/port.scm 1272 */
{ /* Ieee/port.scm 1273 */
 obj_t BgL_auxz00_7359;
if(
INPUT_PORTP(BgL_portz00_3723))
{ /* Ieee/port.scm 1273 */
BgL_auxz00_7359 = BgL_portz00_3723
; }  else 
{ 
 obj_t BgL_auxz00_7362;
BgL_auxz00_7362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(56923L), BGl_string2901z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3723); 
FAILURE(BgL_auxz00_7362,BFALSE,BFALSE);} 
return 
BGl_closezd2inputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_7359);} } 

}



/* get-output-string */
BGL_EXPORTED_DEF obj_t BGl_getzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_128)
{
{ /* Ieee/port.scm 1278 */
BGL_TAIL return 
get_output_string(BgL_portz00_128);} 

}



/* &get-output-string */
obj_t BGl_z62getzd2outputzd2stringz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3724, obj_t BgL_portz00_3725)
{
{ /* Ieee/port.scm 1278 */
{ /* Ieee/port.scm 1279 */
 obj_t BgL_auxz00_7368;
if(
OUTPUT_PORTP(BgL_portz00_3725))
{ /* Ieee/port.scm 1279 */
BgL_auxz00_7368 = BgL_portz00_3725
; }  else 
{ 
 obj_t BgL_auxz00_7371;
BgL_auxz00_7371 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(57215L), BGl_string2902z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3725); 
FAILURE(BgL_auxz00_7371,BFALSE,BFALSE);} 
return 
BGl_getzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BgL_auxz00_7368);} } 

}



/* close-output-port */
BGL_EXPORTED_DEF obj_t BGl_closezd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_129)
{
{ /* Ieee/port.scm 1284 */
BGL_TAIL return 
bgl_close_output_port(BgL_portz00_129);} 

}



/* &close-output-port */
obj_t BGl_z62closezd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3726, obj_t BgL_portz00_3727)
{
{ /* Ieee/port.scm 1284 */
{ /* Ieee/port.scm 1285 */
 obj_t BgL_auxz00_7377;
if(
OUTPUT_PORTP(BgL_portz00_3727))
{ /* Ieee/port.scm 1285 */
BgL_auxz00_7377 = BgL_portz00_3727
; }  else 
{ 
 obj_t BgL_auxz00_7380;
BgL_auxz00_7380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(57509L), BGl_string2903z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3727); 
FAILURE(BgL_auxz00_7380,BFALSE,BFALSE);} 
return 
BGl_closezd2outputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_7377);} } 

}



/* flush-output-port */
BGL_EXPORTED_DEF obj_t BGl_flushzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_130)
{
{ /* Ieee/port.scm 1290 */
return 
bgl_flush_output_port(BgL_portz00_130);} 

}



/* &flush-output-port */
obj_t BGl_z62flushzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3728, obj_t BgL_portz00_3729)
{
{ /* Ieee/port.scm 1290 */
{ /* Ieee/port.scm 1291 */
 obj_t BgL_auxz00_7386;
if(
OUTPUT_PORTP(BgL_portz00_3729))
{ /* Ieee/port.scm 1291 */
BgL_auxz00_7386 = BgL_portz00_3729
; }  else 
{ 
 obj_t BgL_auxz00_7389;
BgL_auxz00_7389 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(57802L), BGl_string2904z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3729); 
FAILURE(BgL_auxz00_7389,BFALSE,BFALSE);} 
return 
BGl_flushzd2outputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_7386);} } 

}



/* reset-output-port */
BGL_EXPORTED_DEF obj_t BGl_resetzd2outputzd2portz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_131)
{
{ /* Ieee/port.scm 1296 */
bgl_reset_output_port_error(BgL_portz00_131); 
if(
BGL_OUTPUT_STRING_PORTP(BgL_portz00_131))
{ /* Ieee/port.scm 1298 */
return 
bgl_reset_output_string_port(BgL_portz00_131);}  else 
{ /* Ieee/port.scm 1298 */
return 
bgl_flush_output_port(BgL_portz00_131);} } 

}



/* &reset-output-port */
obj_t BGl_z62resetzd2outputzd2portz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3730, obj_t BgL_portz00_3731)
{
{ /* Ieee/port.scm 1296 */
{ /* Ieee/port.scm 1297 */
 obj_t BgL_auxz00_7399;
if(
OUTPUT_PORTP(BgL_portz00_3731))
{ /* Ieee/port.scm 1297 */
BgL_auxz00_7399 = BgL_portz00_3731
; }  else 
{ 
 obj_t BgL_auxz00_7402;
BgL_auxz00_7402 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(58126L), BGl_string2905z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3731); 
FAILURE(BgL_auxz00_7402,BFALSE,BFALSE);} 
return 
BGl_resetzd2outputzd2portz00zz__r4_ports_6_10_1z00(BgL_auxz00_7399);} } 

}



/* reset-eof */
BGL_EXPORTED_DEF bool_t BGl_resetzd2eofzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_132)
{
{ /* Ieee/port.scm 1305 */
return 
reset_eof(BgL_portz00_132);} 

}



/* &reset-eof */
obj_t BGl_z62resetzd2eofzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3732, obj_t BgL_portz00_3733)
{
{ /* Ieee/port.scm 1305 */
{ /* Ieee/port.scm 1306 */
 bool_t BgL_tmpz00_7408;
{ /* Ieee/port.scm 1306 */
 obj_t BgL_auxz00_7409;
if(
INPUT_PORTP(BgL_portz00_3733))
{ /* Ieee/port.scm 1306 */
BgL_auxz00_7409 = BgL_portz00_3733
; }  else 
{ 
 obj_t BgL_auxz00_7412;
BgL_auxz00_7412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(58496L), BGl_string2906z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3733); 
FAILURE(BgL_auxz00_7412,BFALSE,BFALSE);} 
BgL_tmpz00_7408 = 
BGl_resetzd2eofzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7409); } 
return 
BBOOL(BgL_tmpz00_7408);} } 

}



/* set-input-port-position! */
BGL_EXPORTED_DEF obj_t BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_133, long BgL_posz00_134)
{
{ /* Ieee/port.scm 1311 */
{ /* Ieee/port.scm 1312 */
 obj_t BgL_useekz00_2885;
BgL_useekz00_2885 = 
BGL_INPUT_PORT_USEEK(BgL_portz00_133); 
if(
PROCEDUREP(BgL_useekz00_2885))
{ /* Ieee/port.scm 1313 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_useekz00_2885, 2))
{ /* Ieee/port.scm 1314 */
BGL_PROCEDURE_CALL2(BgL_useekz00_2885, BgL_portz00_133, 
BINT(BgL_posz00_134)); }  else 
{ /* Ieee/port.scm 1314 */
FAILURE(BGl_string2907z00zz__r4_ports_6_10_1z00,BGl_list2908z00zz__r4_ports_6_10_1z00,BgL_useekz00_2885);} }  else 
{ /* Ieee/port.scm 1313 */
bgl_input_port_seek(BgL_portz00_133, BgL_posz00_134); BUNSPEC; } } 
return BUNSPEC;} 

}



/* &set-input-port-position! */
obj_t BGl_z62setzd2inputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3734, obj_t BgL_portz00_3735, obj_t BgL_posz00_3736)
{
{ /* Ieee/port.scm 1311 */
{ /* Ieee/port.scm 1315 */
 long BgL_auxz00_7438; obj_t BgL_auxz00_7431;
{ /* Ieee/port.scm 1315 */
 obj_t BgL_tmpz00_7439;
if(
INTEGERP(BgL_posz00_3736))
{ /* Ieee/port.scm 1315 */
BgL_tmpz00_7439 = BgL_posz00_3736
; }  else 
{ 
 obj_t BgL_auxz00_7442;
BgL_auxz00_7442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(58933L), BGl_string2913z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_posz00_3736); 
FAILURE(BgL_auxz00_7442,BFALSE,BFALSE);} 
BgL_auxz00_7438 = 
(long)CINT(BgL_tmpz00_7439); } 
if(
INPUT_PORTP(BgL_portz00_3735))
{ /* Ieee/port.scm 1315 */
BgL_auxz00_7431 = BgL_portz00_3735
; }  else 
{ 
 obj_t BgL_auxz00_7434;
BgL_auxz00_7434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(58933L), BGl_string2913z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3735); 
FAILURE(BgL_auxz00_7434,BFALSE,BFALSE);} 
return 
BGl_setzd2inputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7431, BgL_auxz00_7438);} } 

}



/* input-port-position */
BGL_EXPORTED_DEF long BGl_inputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_135)
{
{ /* Ieee/port.scm 1321 */
return 
INPUT_PORT_FILEPOS(BgL_portz00_135);} 

}



/* &input-port-position */
obj_t BGl_z62inputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3737, obj_t BgL_portz00_3738)
{
{ /* Ieee/port.scm 1321 */
{ /* Ieee/port.scm 1322 */
 long BgL_tmpz00_7449;
{ /* Ieee/port.scm 1322 */
 obj_t BgL_auxz00_7450;
if(
INPUT_PORTP(BgL_portz00_3738))
{ /* Ieee/port.scm 1322 */
BgL_auxz00_7450 = BgL_portz00_3738
; }  else 
{ 
 obj_t BgL_auxz00_7453;
BgL_auxz00_7453 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(59223L), BGl_string2914z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3738); 
FAILURE(BgL_auxz00_7453,BFALSE,BFALSE);} 
BgL_tmpz00_7449 = 
BGl_inputzd2portzd2positionz00zz__r4_ports_6_10_1z00(BgL_auxz00_7450); } 
return 
BINT(BgL_tmpz00_7449);} } 

}



/* input-port-fill-barrier */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2fillzd2barrierzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_136)
{
{ /* Ieee/port.scm 1327 */
return 
BINT(
INPUT_PORT_FILLBARRIER(BgL_portz00_136));} 

}



/* &input-port-fill-barrier */
obj_t BGl_z62inputzd2portzd2fillzd2barrierzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3739, obj_t BgL_portz00_3740)
{
{ /* Ieee/port.scm 1327 */
{ /* Ieee/port.scm 1328 */
 obj_t BgL_auxz00_7461;
if(
INPUT_PORTP(BgL_portz00_3740))
{ /* Ieee/port.scm 1328 */
BgL_auxz00_7461 = BgL_portz00_3740
; }  else 
{ 
 obj_t BgL_auxz00_7464;
BgL_auxz00_7464 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(59537L), BGl_string2915z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3740); 
FAILURE(BgL_auxz00_7464,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2fillzd2barrierzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7461);} } 

}



/* input-port-fill-barrier-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2fillzd2barrierzd2setz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_137, long BgL_posz00_138)
{
{ /* Ieee/port.scm 1333 */
INPUT_PORT_FILLBARRIER_SET(BgL_portz00_137, BgL_posz00_138); BUNSPEC; 
return 
BINT(BgL_posz00_138);} 

}



/* &input-port-fill-barrier-set! */
obj_t BGl_z62inputzd2portzd2fillzd2barrierzd2setz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3741, obj_t BgL_portz00_3742, obj_t BgL_posz00_3743)
{
{ /* Ieee/port.scm 1333 */
{ /* Ieee/port.scm 1334 */
 long BgL_auxz00_7478; obj_t BgL_auxz00_7471;
{ /* Ieee/port.scm 1334 */
 obj_t BgL_tmpz00_7479;
if(
INTEGERP(BgL_posz00_3743))
{ /* Ieee/port.scm 1334 */
BgL_tmpz00_7479 = BgL_posz00_3743
; }  else 
{ 
 obj_t BgL_auxz00_7482;
BgL_auxz00_7482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(59911L), BGl_string2916z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_posz00_3743); 
FAILURE(BgL_auxz00_7482,BFALSE,BFALSE);} 
BgL_auxz00_7478 = 
(long)CINT(BgL_tmpz00_7479); } 
if(
INPUT_PORTP(BgL_portz00_3742))
{ /* Ieee/port.scm 1334 */
BgL_auxz00_7471 = BgL_portz00_3742
; }  else 
{ 
 obj_t BgL_auxz00_7474;
BgL_auxz00_7474 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(59911L), BGl_string2916z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3742); 
FAILURE(BgL_auxz00_7474,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2fillzd2barrierzd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_7471, BgL_auxz00_7478);} } 

}



/* input-port-last-token-position */
BGL_EXPORTED_DEF long BGl_inputzd2portzd2lastzd2tokenzd2positionz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_139)
{
{ /* Ieee/port.scm 1340 */
{ /* Ieee/port.scm 1341 */
 long BgL_tmpz00_7488;
BgL_tmpz00_7488 = 
INPUT_PORT_TOKENPOS(BgL_portz00_139); 
return 
(long)(BgL_tmpz00_7488);} } 

}



/* &input-port-last-token-position */
obj_t BGl_z62inputzd2portzd2lastzd2tokenzd2positionz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3744, obj_t BgL_portz00_3745)
{
{ /* Ieee/port.scm 1340 */
{ /* Ieee/port.scm 1341 */
 long BgL_tmpz00_7491;
{ /* Ieee/port.scm 1341 */
 obj_t BgL_auxz00_7492;
if(
INPUT_PORTP(BgL_portz00_3745))
{ /* Ieee/port.scm 1341 */
BgL_auxz00_7492 = BgL_portz00_3745
; }  else 
{ 
 obj_t BgL_auxz00_7495;
BgL_auxz00_7495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(60203L), BGl_string2917z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3745); 
FAILURE(BgL_auxz00_7495,BFALSE,BFALSE);} 
BgL_tmpz00_7491 = 
BGl_inputzd2portzd2lastzd2tokenzd2positionz00zz__r4_ports_6_10_1z00(BgL_auxz00_7492); } 
return 
BINT(BgL_tmpz00_7491);} } 

}



/* output-port-name */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_140)
{
{ /* Ieee/port.scm 1346 */
return 
OUTPUT_PORT_NAME(BgL_portz00_140);} 

}



/* &output-port-name */
obj_t BGl_z62outputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3746, obj_t BgL_portz00_3747)
{
{ /* Ieee/port.scm 1346 */
{ /* Ieee/port.scm 1347 */
 obj_t BgL_auxz00_7502;
if(
OUTPUT_PORTP(BgL_portz00_3747))
{ /* Ieee/port.scm 1347 */
BgL_auxz00_7502 = BgL_portz00_3747
; }  else 
{ 
 obj_t BgL_auxz00_7505;
BgL_auxz00_7505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(60509L), BGl_string2918z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3747); 
FAILURE(BgL_auxz00_7505,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2namez00zz__r4_ports_6_10_1z00(BgL_auxz00_7502);} } 

}



/* output-port-name-set! */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_141, obj_t BgL_namez00_142)
{
{ /* Ieee/port.scm 1352 */
OUTPUT_PORT_NAME_SET(BgL_portz00_141, BgL_namez00_142); 
return BUNSPEC;} 

}



/* &output-port-name-set! */
obj_t BGl_z62outputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3748, obj_t BgL_portz00_3749, obj_t BgL_namez00_3750)
{
{ /* Ieee/port.scm 1352 */
{ /* Ieee/port.scm 1352 */
 obj_t BgL_auxz00_7518; obj_t BgL_auxz00_7511;
if(
STRINGP(BgL_namez00_3750))
{ /* Ieee/port.scm 1352 */
BgL_auxz00_7518 = BgL_namez00_3750
; }  else 
{ 
 obj_t BgL_auxz00_7521;
BgL_auxz00_7521 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(60758L), BGl_string2919z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_namez00_3750); 
FAILURE(BgL_auxz00_7521,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_3749))
{ /* Ieee/port.scm 1352 */
BgL_auxz00_7511 = BgL_portz00_3749
; }  else 
{ 
 obj_t BgL_auxz00_7514;
BgL_auxz00_7514 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(60758L), BGl_string2919z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3749); 
FAILURE(BgL_auxz00_7514,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7511, BgL_auxz00_7518);} } 

}



/* set-output-port-position! */
BGL_EXPORTED_DEF obj_t BGl_setzd2outputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_143, long BgL_posz00_144)
{
{ /* Ieee/port.scm 1358 */
if(
CBOOL(
bgl_output_port_seek(BgL_portz00_143, BgL_posz00_144)))
{ /* Ieee/port.scm 1359 */
return BFALSE;}  else 
{ /* Ieee/port.scm 1359 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2920z00zz__r4_ports_6_10_1z00, BGl_string2921z00zz__r4_ports_6_10_1z00, BgL_portz00_143);} } 

}



/* &set-output-port-position! */
obj_t BGl_z62setzd2outputzd2portzd2positionz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3751, obj_t BgL_portz00_3752, obj_t BgL_posz00_3753)
{
{ /* Ieee/port.scm 1358 */
{ /* Ieee/port.scm 1359 */
 long BgL_auxz00_7537; obj_t BgL_auxz00_7530;
{ /* Ieee/port.scm 1359 */
 obj_t BgL_tmpz00_7538;
if(
INTEGERP(BgL_posz00_3753))
{ /* Ieee/port.scm 1359 */
BgL_tmpz00_7538 = BgL_posz00_3753
; }  else 
{ 
 obj_t BgL_auxz00_7541;
BgL_auxz00_7541 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(61143L), BGl_string2922z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_posz00_3753); 
FAILURE(BgL_auxz00_7541,BFALSE,BFALSE);} 
BgL_auxz00_7537 = 
(long)CINT(BgL_tmpz00_7538); } 
if(
OUTPUT_PORTP(BgL_portz00_3752))
{ /* Ieee/port.scm 1359 */
BgL_auxz00_7530 = BgL_portz00_3752
; }  else 
{ 
 obj_t BgL_auxz00_7533;
BgL_auxz00_7533 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(61143L), BGl_string2922z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3752); 
FAILURE(BgL_auxz00_7533,BFALSE,BFALSE);} 
return 
BGl_setzd2outputzd2portzd2positionz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7530, BgL_auxz00_7537);} } 

}



/* output-port-position */
BGL_EXPORTED_DEF long BGl_outputzd2portzd2positionz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_145)
{
{ /* Ieee/port.scm 1366 */
return 
BGL_OUTPUT_PORT_FILEPOS(BgL_portz00_145);} 

}



/* &output-port-position */
obj_t BGl_z62outputzd2portzd2positionz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3754, obj_t BgL_portz00_3755)
{
{ /* Ieee/port.scm 1366 */
{ /* Ieee/port.scm 1367 */
 long BgL_tmpz00_7548;
{ /* Ieee/port.scm 1367 */
 obj_t BgL_auxz00_7549;
if(
OUTPUT_PORTP(BgL_portz00_3755))
{ /* Ieee/port.scm 1367 */
BgL_auxz00_7549 = BgL_portz00_3755
; }  else 
{ 
 obj_t BgL_auxz00_7552;
BgL_auxz00_7552 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(61559L), BGl_string2923z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3755); 
FAILURE(BgL_auxz00_7552,BFALSE,BFALSE);} 
BgL_tmpz00_7548 = 
BGl_outputzd2portzd2positionz00zz__r4_ports_6_10_1z00(BgL_auxz00_7549); } 
return 
BINT(BgL_tmpz00_7548);} } 

}



/* output-port-isatty? */
BGL_EXPORTED_DEF bool_t BGl_outputzd2portzd2isattyzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_portz00_146)
{
{ /* Ieee/port.scm 1372 */
return 
bgl_port_isatty(BgL_portz00_146);} 

}



/* &output-port-isatty? */
obj_t BGl_z62outputzd2portzd2isattyzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3756, obj_t BgL_portz00_3757)
{
{ /* Ieee/port.scm 1372 */
{ /* Ieee/port.scm 1374 */
 bool_t BgL_tmpz00_7559;
{ /* Ieee/port.scm 1374 */
 obj_t BgL_auxz00_7560;
if(
OUTPUT_PORTP(BgL_portz00_3757))
{ /* Ieee/port.scm 1374 */
BgL_auxz00_7560 = BgL_portz00_3757
; }  else 
{ 
 obj_t BgL_auxz00_7563;
BgL_auxz00_7563 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(61887L), BGl_string2924z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3757); 
FAILURE(BgL_auxz00_7563,BFALSE,BFALSE);} 
BgL_tmpz00_7559 = 
BGl_outputzd2portzd2isattyzf3zf3zz__r4_ports_6_10_1z00(BgL_auxz00_7560); } 
return 
BBOOL(BgL_tmpz00_7559);} } 

}



/* input-port-name */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2namez00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_147)
{
{ /* Ieee/port.scm 1380 */
return 
INPUT_PORT_NAME(BgL_portz00_147);} 

}



/* &input-port-name */
obj_t BGl_z62inputzd2portzd2namez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3758, obj_t BgL_portz00_3759)
{
{ /* Ieee/port.scm 1380 */
{ /* Ieee/port.scm 1381 */
 obj_t BgL_auxz00_7570;
if(
INPUT_PORTP(BgL_portz00_3759))
{ /* Ieee/port.scm 1381 */
BgL_auxz00_7570 = BgL_portz00_3759
; }  else 
{ 
 obj_t BgL_auxz00_7573;
BgL_auxz00_7573 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(62194L), BGl_string2925z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3759); 
FAILURE(BgL_auxz00_7573,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2namez00zz__r4_ports_6_10_1z00(BgL_auxz00_7570);} } 

}



/* input-port-name-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_148, obj_t BgL_namez00_149)
{
{ /* Ieee/port.scm 1386 */
INPUT_PORT_NAME_SET(BgL_portz00_148, BgL_namez00_149); 
return BUNSPEC;} 

}



/* &input-port-name-set! */
obj_t BGl_z62inputzd2portzd2namezd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3760, obj_t BgL_portz00_3761, obj_t BgL_namez00_3762)
{
{ /* Ieee/port.scm 1386 */
{ /* Ieee/port.scm 1386 */
 obj_t BgL_auxz00_7586; obj_t BgL_auxz00_7579;
if(
STRINGP(BgL_namez00_3762))
{ /* Ieee/port.scm 1386 */
BgL_auxz00_7586 = BgL_namez00_3762
; }  else 
{ 
 obj_t BgL_auxz00_7589;
BgL_auxz00_7589 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(62442L), BGl_string2926z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_namez00_3762); 
FAILURE(BgL_auxz00_7589,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_portz00_3761))
{ /* Ieee/port.scm 1386 */
BgL_auxz00_7579 = BgL_portz00_3761
; }  else 
{ 
 obj_t BgL_auxz00_7582;
BgL_auxz00_7582 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(62442L), BGl_string2926z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3761); 
FAILURE(BgL_auxz00_7582,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2namezd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7579, BgL_auxz00_7586);} } 

}



/* input-port-length */
BGL_EXPORTED_DEF long BGl_inputzd2portzd2lengthz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_150)
{
{ /* Ieee/port.scm 1392 */
return 
BGL_INPUT_PORT_LENGTH(BgL_portz00_150);} 

}



/* &input-port-length */
obj_t BGl_z62inputzd2portzd2lengthz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3763, obj_t BgL_portz00_3764)
{
{ /* Ieee/port.scm 1392 */
{ /* Ieee/port.scm 1393 */
 long BgL_tmpz00_7595;
{ /* Ieee/port.scm 1393 */
 obj_t BgL_auxz00_7596;
if(
INPUT_PORTP(BgL_portz00_3764))
{ /* Ieee/port.scm 1393 */
BgL_auxz00_7596 = BgL_portz00_3764
; }  else 
{ 
 obj_t BgL_auxz00_7599;
BgL_auxz00_7599 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(62794L), BGl_string2927z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3764); 
FAILURE(BgL_auxz00_7599,BFALSE,BFALSE);} 
BgL_tmpz00_7595 = 
BGl_inputzd2portzd2lengthz00zz__r4_ports_6_10_1z00(BgL_auxz00_7596); } 
return 
make_belong(BgL_tmpz00_7595);} } 

}



/* output-port-close-hook */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_151)
{
{ /* Ieee/port.scm 1398 */
return 
PORT_CHOOK(BgL_portz00_151);} 

}



/* &output-port-close-hook */
obj_t BGl_z62outputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3765, obj_t BgL_portz00_3766)
{
{ /* Ieee/port.scm 1398 */
{ /* Ieee/port.scm 1399 */
 obj_t BgL_auxz00_7606;
if(
OUTPUT_PORTP(BgL_portz00_3766))
{ /* Ieee/port.scm 1399 */
BgL_auxz00_7606 = BgL_portz00_3766
; }  else 
{ 
 obj_t BgL_auxz00_7609;
BgL_auxz00_7609 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(63092L), BGl_string2928z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3766); 
FAILURE(BgL_auxz00_7609,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7606);} } 

}



/* closed-output-port? */
BGL_EXPORTED_DEF bool_t BGl_closedzd2outputzd2portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_portz00_152)
{
{ /* Ieee/port.scm 1404 */
return 
OUTPUT_PORT_CLOSEP(BgL_portz00_152);} 

}



/* &closed-output-port? */
obj_t BGl_z62closedzd2outputzd2portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3767, obj_t BgL_portz00_3768)
{
{ /* Ieee/port.scm 1404 */
{ /* Ieee/port.scm 1405 */
 bool_t BgL_tmpz00_7615;
{ /* Ieee/port.scm 1405 */
 obj_t BgL_auxz00_7616;
if(
OUTPUT_PORTP(BgL_portz00_3768))
{ /* Ieee/port.scm 1405 */
BgL_auxz00_7616 = BgL_portz00_3768
; }  else 
{ 
 obj_t BgL_auxz00_7619;
BgL_auxz00_7619 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(63388L), BGl_string2929z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3768); 
FAILURE(BgL_auxz00_7619,BFALSE,BFALSE);} 
BgL_tmpz00_7615 = 
BGl_closedzd2outputzd2portzf3zf3zz__r4_ports_6_10_1z00(BgL_auxz00_7616); } 
return 
BBOOL(BgL_tmpz00_7615);} } 

}



/* output-port-close-hook-set! */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_153, obj_t BgL_procz00_154)
{
{ /* Ieee/port.scm 1410 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_154, 
(int)(1L)))
{ /* Ieee/port.scm 1411 */
PORT_CHOOK_SET(BgL_portz00_153, BgL_procz00_154); BUNSPEC; 
return BgL_procz00_154;}  else 
{ /* Ieee/port.scm 1411 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2930z00zz__r4_ports_6_10_1z00, BGl_string2931z00zz__r4_ports_6_10_1z00, BgL_procz00_154);} } 

}



/* &output-port-close-hook-set! */
obj_t BGl_z62outputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3769, obj_t BgL_portz00_3770, obj_t BgL_procz00_3771)
{
{ /* Ieee/port.scm 1410 */
{ /* Ieee/port.scm 1411 */
 obj_t BgL_auxz00_7637; obj_t BgL_auxz00_7630;
if(
PROCEDUREP(BgL_procz00_3771))
{ /* Ieee/port.scm 1411 */
BgL_auxz00_7637 = BgL_procz00_3771
; }  else 
{ 
 obj_t BgL_auxz00_7640;
BgL_auxz00_7640 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(63691L), BGl_string2932z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3771); 
FAILURE(BgL_auxz00_7640,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_3770))
{ /* Ieee/port.scm 1411 */
BgL_auxz00_7630 = BgL_portz00_3770
; }  else 
{ 
 obj_t BgL_auxz00_7633;
BgL_auxz00_7633 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(63691L), BGl_string2932z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3770); 
FAILURE(BgL_auxz00_7633,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_7630, BgL_auxz00_7637);} } 

}



/* output-port-flush-hook */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2flushzd2hookzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_155)
{
{ /* Ieee/port.scm 1421 */
return 
BGL_OUTPUT_PORT_FHOOK(BgL_portz00_155);} 

}



/* &output-port-flush-hook */
obj_t BGl_z62outputzd2portzd2flushzd2hookzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3772, obj_t BgL_portz00_3773)
{
{ /* Ieee/port.scm 1421 */
{ /* Ieee/port.scm 1422 */
 obj_t BgL_auxz00_7646;
if(
OUTPUT_PORTP(BgL_portz00_3773))
{ /* Ieee/port.scm 1422 */
BgL_auxz00_7646 = BgL_portz00_3773
; }  else 
{ 
 obj_t BgL_auxz00_7649;
BgL_auxz00_7649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(64180L), BGl_string2933z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3773); 
FAILURE(BgL_auxz00_7649,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2flushzd2hookzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7646);} } 

}



/* output-port-flush-hook-set! */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2flushzd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_156, obj_t BgL_procz00_157)
{
{ /* Ieee/port.scm 1427 */
{ /* Ieee/port.scm 1428 */
 bool_t BgL_test3736z00_7654;
if(
PROCEDUREP(BgL_procz00_157))
{ /* Ieee/port.scm 1428 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_157, 
(int)(2L)))
{ /* Ieee/port.scm 1428 */
BgL_test3736z00_7654 = ((bool_t)0)
; }  else 
{ /* Ieee/port.scm 1428 */
BgL_test3736z00_7654 = ((bool_t)1)
; } }  else 
{ /* Ieee/port.scm 1428 */
BgL_test3736z00_7654 = ((bool_t)0)
; } 
if(BgL_test3736z00_7654)
{ /* Ieee/port.scm 1428 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2934z00zz__r4_ports_6_10_1z00, BGl_string2931z00zz__r4_ports_6_10_1z00, BgL_procz00_157);}  else 
{ /* Ieee/port.scm 1428 */
BGL_OUTPUT_PORT_FHOOK_SET(BgL_portz00_156, BgL_procz00_157); BUNSPEC; 
return BgL_procz00_157;} } } 

}



/* &output-port-flush-hook-set! */
obj_t BGl_z62outputzd2portzd2flushzd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3774, obj_t BgL_portz00_3775, obj_t BgL_procz00_3776)
{
{ /* Ieee/port.scm 1427 */
{ /* Ieee/port.scm 1428 */
 obj_t BgL_auxz00_7662;
if(
OUTPUT_PORTP(BgL_portz00_3775))
{ /* Ieee/port.scm 1428 */
BgL_auxz00_7662 = BgL_portz00_3775
; }  else 
{ 
 obj_t BgL_auxz00_7665;
BgL_auxz00_7665 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(64481L), BGl_string2935z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3775); 
FAILURE(BgL_auxz00_7665,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2flushzd2hookzd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_7662, BgL_procz00_3776);} } 

}



/* output-port-flush-buffer */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2flushzd2bufferzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_158)
{
{ /* Ieee/port.scm 1438 */
return 
BGL_OUTPUT_PORT_FLUSHBUF(BgL_portz00_158);} 

}



/* &output-port-flush-buffer */
obj_t BGl_z62outputzd2portzd2flushzd2bufferzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3777, obj_t BgL_portz00_3778)
{
{ /* Ieee/port.scm 1438 */
{ /* Ieee/port.scm 1439 */
 obj_t BgL_auxz00_7671;
if(
OUTPUT_PORTP(BgL_portz00_3778))
{ /* Ieee/port.scm 1439 */
BgL_auxz00_7671 = BgL_portz00_3778
; }  else 
{ 
 obj_t BgL_auxz00_7674;
BgL_auxz00_7674 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(64971L), BGl_string2936z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3778); 
FAILURE(BgL_auxz00_7674,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2flushzd2bufferzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7671);} } 

}



/* output-port-flush-buffer-set! */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2flushzd2bufferzd2setz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_159, obj_t BgL_bufz00_160)
{
{ /* Ieee/port.scm 1444 */
BGL_OUTPUT_PORT_FLUSHBUF_SET(BgL_portz00_159, BgL_bufz00_160); BUNSPEC; 
return BgL_bufz00_160;} 

}



/* &output-port-flush-buffer-set! */
obj_t BGl_z62outputzd2portzd2flushzd2bufferzd2setz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3779, obj_t BgL_portz00_3780, obj_t BgL_bufz00_3781)
{
{ /* Ieee/port.scm 1444 */
{ /* Ieee/port.scm 1445 */
 obj_t BgL_auxz00_7680;
if(
OUTPUT_PORTP(BgL_portz00_3780))
{ /* Ieee/port.scm 1445 */
BgL_auxz00_7680 = BgL_portz00_3780
; }  else 
{ 
 obj_t BgL_auxz00_7683;
BgL_auxz00_7683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(65319L), BGl_string2937z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3780); 
FAILURE(BgL_auxz00_7683,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2flushzd2bufferzd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_7680, BgL_bufz00_3781);} } 

}



/* input-port-close-hook */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(obj_t BgL_portz00_161)
{
{ /* Ieee/port.scm 1451 */
return 
PORT_CHOOK(BgL_portz00_161);} 

}



/* &input-port-close-hook */
obj_t BGl_z62inputzd2portzd2closezd2hookzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3782, obj_t BgL_portz00_3783)
{
{ /* Ieee/port.scm 1451 */
{ /* Ieee/port.scm 1452 */
 obj_t BgL_auxz00_7689;
if(
INPUT_PORTP(BgL_portz00_3783))
{ /* Ieee/port.scm 1452 */
BgL_auxz00_7689 = BgL_portz00_3783
; }  else 
{ 
 obj_t BgL_auxz00_7692;
BgL_auxz00_7692 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(65599L), BGl_string2938z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3783); 
FAILURE(BgL_auxz00_7692,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2closezd2hookzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7689);} } 

}



/* input-port-close-hook-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t BgL_portz00_162, obj_t BgL_procz00_163)
{
{ /* Ieee/port.scm 1457 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_163, 
(int)(1L)))
{ /* Ieee/port.scm 1458 */
PORT_CHOOK_SET(BgL_portz00_162, BgL_procz00_163); BUNSPEC; 
return BgL_procz00_163;}  else 
{ /* Ieee/port.scm 1458 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2939z00zz__r4_ports_6_10_1z00, BGl_string2931z00zz__r4_ports_6_10_1z00, BgL_procz00_163);} } 

}



/* &input-port-close-hook-set! */
obj_t BGl_z62inputzd2portzd2closezd2hookzd2setz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3784, obj_t BgL_portz00_3785, obj_t BgL_procz00_3786)
{
{ /* Ieee/port.scm 1457 */
{ /* Ieee/port.scm 1458 */
 obj_t BgL_auxz00_7709; obj_t BgL_auxz00_7702;
if(
PROCEDUREP(BgL_procz00_3786))
{ /* Ieee/port.scm 1458 */
BgL_auxz00_7709 = BgL_procz00_3786
; }  else 
{ 
 obj_t BgL_auxz00_7712;
BgL_auxz00_7712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(65898L), BGl_string2940z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3786); 
FAILURE(BgL_auxz00_7712,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_portz00_3785))
{ /* Ieee/port.scm 1458 */
BgL_auxz00_7702 = BgL_portz00_3785
; }  else 
{ 
 obj_t BgL_auxz00_7705;
BgL_auxz00_7705 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(65898L), BGl_string2940z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3785); 
FAILURE(BgL_auxz00_7705,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_7702, BgL_auxz00_7709);} } 

}



/* input-port-seek */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2seekz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_164)
{
{ /* Ieee/port.scm 1468 */
{ /* Ieee/port.scm 1469 */
 obj_t BgL_aux2557z00_4614;
BgL_aux2557z00_4614 = 
BGL_INPUT_PORT_USEEK(BgL_portz00_164); 
if(
PROCEDUREP(BgL_aux2557z00_4614))
{ /* Ieee/port.scm 1469 */
return BgL_aux2557z00_4614;}  else 
{ 
 obj_t BgL_auxz00_7720;
BgL_auxz00_7720 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(66377L), BGl_string2941z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_aux2557z00_4614); 
FAILURE(BgL_auxz00_7720,BFALSE,BFALSE);} } } 

}



/* &input-port-seek */
obj_t BGl_z62inputzd2portzd2seekz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3787, obj_t BgL_portz00_3788)
{
{ /* Ieee/port.scm 1468 */
{ /* Ieee/port.scm 1469 */
 obj_t BgL_auxz00_7724;
if(
INPUT_PORTP(BgL_portz00_3788))
{ /* Ieee/port.scm 1469 */
BgL_auxz00_7724 = BgL_portz00_3788
; }  else 
{ 
 obj_t BgL_auxz00_7727;
BgL_auxz00_7727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(66377L), BGl_string2942z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3788); 
FAILURE(BgL_auxz00_7727,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2seekz00zz__r4_ports_6_10_1z00(BgL_auxz00_7724);} } 

}



/* input-port-seek-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_165, obj_t BgL_procz00_166)
{
{ /* Ieee/port.scm 1474 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_166, 
(int)(2L)))
{ /* Ieee/port.scm 1475 */
BGL_INPUT_PORT_USEEK_SET(BgL_portz00_165, BgL_procz00_166); BUNSPEC; 
return BgL_procz00_166;}  else 
{ /* Ieee/port.scm 1475 */
return 
bgl_system_failure(BGL_IO_PORT_ERROR, BGl_string2943z00zz__r4_ports_6_10_1z00, BGl_string2944z00zz__r4_ports_6_10_1z00, BgL_procz00_166);} } 

}



/* &input-port-seek-set! */
obj_t BGl_z62inputzd2portzd2seekzd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3789, obj_t BgL_portz00_3790, obj_t BgL_procz00_3791)
{
{ /* Ieee/port.scm 1474 */
{ /* Ieee/port.scm 1475 */
 obj_t BgL_auxz00_7744; obj_t BgL_auxz00_7737;
if(
PROCEDUREP(BgL_procz00_3791))
{ /* Ieee/port.scm 1475 */
BgL_auxz00_7744 = BgL_procz00_3791
; }  else 
{ 
 obj_t BgL_auxz00_7747;
BgL_auxz00_7747 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(66670L), BGl_string2945z00zz__r4_ports_6_10_1z00, BGl_string2717z00zz__r4_ports_6_10_1z00, BgL_procz00_3791); 
FAILURE(BgL_auxz00_7747,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_portz00_3790))
{ /* Ieee/port.scm 1475 */
BgL_auxz00_7737 = BgL_portz00_3790
; }  else 
{ 
 obj_t BgL_auxz00_7740;
BgL_auxz00_7740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(66670L), BGl_string2945z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3790); 
FAILURE(BgL_auxz00_7740,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2seekzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7737, BgL_auxz00_7744);} } 

}



/* input-port-buffer */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_167)
{
{ /* Ieee/port.scm 1485 */
return 
BGL_INPUT_PORT_BUFFER(BgL_portz00_167);} 

}



/* &input-port-buffer */
obj_t BGl_z62inputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3792, obj_t BgL_portz00_3793)
{
{ /* Ieee/port.scm 1485 */
{ /* Ieee/port.scm 1486 */
 obj_t BgL_auxz00_7753;
if(
INPUT_PORTP(BgL_portz00_3793))
{ /* Ieee/port.scm 1486 */
BgL_auxz00_7753 = BgL_portz00_3793
; }  else 
{ 
 obj_t BgL_auxz00_7756;
BgL_auxz00_7756 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(67155L), BGl_string2946z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3793); 
FAILURE(BgL_auxz00_7756,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BgL_auxz00_7753);} } 

}



/* input-port-buffer-set! */
BGL_EXPORTED_DEF obj_t BGl_inputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_168, obj_t BgL_bufferz00_169)
{
{ /* Ieee/port.scm 1491 */
bgl_input_port_buffer_set(BgL_portz00_168, BgL_bufferz00_169); BUNSPEC; 
return BgL_portz00_168;} 

}



/* &input-port-buffer-set! */
obj_t BGl_z62inputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3794, obj_t BgL_portz00_3795, obj_t BgL_bufferz00_3796)
{
{ /* Ieee/port.scm 1491 */
{ /* Ieee/port.scm 1492 */
 obj_t BgL_auxz00_7769; obj_t BgL_auxz00_7762;
if(
STRINGP(BgL_bufferz00_3796))
{ /* Ieee/port.scm 1492 */
BgL_auxz00_7769 = BgL_bufferz00_3796
; }  else 
{ 
 obj_t BgL_auxz00_7772;
BgL_auxz00_7772 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(67503L), BGl_string2947z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_bufferz00_3796); 
FAILURE(BgL_auxz00_7772,BFALSE,BFALSE);} 
if(
INPUT_PORTP(BgL_portz00_3795))
{ /* Ieee/port.scm 1492 */
BgL_auxz00_7762 = BgL_portz00_3795
; }  else 
{ 
 obj_t BgL_auxz00_7765;
BgL_auxz00_7765 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(67503L), BGl_string2947z00zz__r4_ports_6_10_1z00, BGl_string2719z00zz__r4_ports_6_10_1z00, BgL_portz00_3795); 
FAILURE(BgL_auxz00_7765,BFALSE,BFALSE);} 
return 
BGl_inputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7762, BgL_auxz00_7769);} } 

}



/* output-port-buffer */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_170)
{
{ /* Ieee/port.scm 1497 */
return 
BGL_OUTPUT_PORT_BUFFER(BgL_portz00_170);} 

}



/* &output-port-buffer */
obj_t BGl_z62outputzd2portzd2bufferz62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3797, obj_t BgL_portz00_3798)
{
{ /* Ieee/port.scm 1497 */
{ /* Ieee/port.scm 1498 */
 obj_t BgL_auxz00_7778;
if(
OUTPUT_PORTP(BgL_portz00_3798))
{ /* Ieee/port.scm 1498 */
BgL_auxz00_7778 = BgL_portz00_3798
; }  else 
{ 
 obj_t BgL_auxz00_7781;
BgL_auxz00_7781 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(67779L), BGl_string2948z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3798); 
FAILURE(BgL_auxz00_7781,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BgL_auxz00_7778);} } 

}



/* output-port-buffer-set! */
BGL_EXPORTED_DEF obj_t BGl_outputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(obj_t BgL_portz00_171, obj_t BgL_bufferz00_172)
{
{ /* Ieee/port.scm 1503 */
bgl_output_port_buffer_set(BgL_portz00_171, BgL_bufferz00_172); BUNSPEC; 
return BgL_portz00_171;} 

}



/* &output-port-buffer-set! */
obj_t BGl_z62outputzd2portzd2bufferzd2setz12za2zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3799, obj_t BgL_portz00_3800, obj_t BgL_bufferz00_3801)
{
{ /* Ieee/port.scm 1503 */
{ /* Ieee/port.scm 1504 */
 obj_t BgL_auxz00_7794; obj_t BgL_auxz00_7787;
if(
STRINGP(BgL_bufferz00_3801))
{ /* Ieee/port.scm 1504 */
BgL_auxz00_7794 = BgL_bufferz00_3801
; }  else 
{ 
 obj_t BgL_auxz00_7797;
BgL_auxz00_7797 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(68130L), BGl_string2949z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_bufferz00_3801); 
FAILURE(BgL_auxz00_7797,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_3800))
{ /* Ieee/port.scm 1504 */
BgL_auxz00_7787 = BgL_portz00_3800
; }  else 
{ 
 obj_t BgL_auxz00_7790;
BgL_auxz00_7790 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(68130L), BGl_string2949z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_portz00_3800); 
FAILURE(BgL_auxz00_7790,BFALSE,BFALSE);} 
return 
BGl_outputzd2portzd2bufferzd2setz12zc0zz__r4_ports_6_10_1z00(BgL_auxz00_7787, BgL_auxz00_7794);} } 

}



/* file-exists? */
BGL_EXPORTED_DEF bool_t BGl_filezd2existszf3z21zz__r4_ports_6_10_1z00(char * BgL_namez00_173)
{
{ /* Ieee/port.scm 1509 */
BGL_TAIL return 
fexists(BgL_namez00_173);} 

}



/* &file-exists? */
obj_t BGl_z62filezd2existszf3z43zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3802, obj_t BgL_namez00_3803)
{
{ /* Ieee/port.scm 1509 */
{ /* Ieee/port.scm 1510 */
 bool_t BgL_tmpz00_7803;
{ /* Ieee/port.scm 1510 */
 char * BgL_auxz00_7804;
{ /* Ieee/port.scm 1510 */
 obj_t BgL_tmpz00_7805;
if(
STRINGP(BgL_namez00_3803))
{ /* Ieee/port.scm 1510 */
BgL_tmpz00_7805 = BgL_namez00_3803
; }  else 
{ 
 obj_t BgL_auxz00_7808;
BgL_auxz00_7808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(68400L), BGl_string2950z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_namez00_3803); 
FAILURE(BgL_auxz00_7808,BFALSE,BFALSE);} 
BgL_auxz00_7804 = 
BSTRING_TO_STRING(BgL_tmpz00_7805); } 
BgL_tmpz00_7803 = 
BGl_filezd2existszf3z21zz__r4_ports_6_10_1z00(BgL_auxz00_7804); } 
return 
BBOOL(BgL_tmpz00_7803);} } 

}



/* file-gzip? */
BGL_EXPORTED_DEF obj_t BGl_filezd2gza7ipzf3z86zz__r4_ports_6_10_1z00(obj_t BgL_namez00_174)
{
{ /* Ieee/port.scm 1515 */
{ /* Ieee/port.scm 1516 */
 bool_t BgL_test3758z00_7815;
{ /* Ieee/port.scm 1516 */
 char * BgL_namez00_2888;
{ /* Ieee/port.scm 1516 */
 obj_t BgL_tmpz00_7816;
if(
STRINGP(BgL_namez00_174))
{ /* Ieee/port.scm 1516 */
BgL_tmpz00_7816 = BgL_namez00_174
; }  else 
{ 
 obj_t BgL_auxz00_7819;
BgL_auxz00_7819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(68693L), BGl_string2951z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_namez00_174); 
FAILURE(BgL_auxz00_7819,BFALSE,BFALSE);} 
BgL_namez00_2888 = 
BSTRING_TO_STRING(BgL_tmpz00_7816); } 
BgL_test3758z00_7815 = 
fexists(BgL_namez00_2888); } 
if(BgL_test3758z00_7815)
{ /* Ieee/port.scm 1517 */
 obj_t BgL_auxz00_7825;
if(
STRINGP(BgL_namez00_174))
{ /* Ieee/port.scm 1517 */
BgL_auxz00_7825 = BgL_namez00_174
; }  else 
{ 
 obj_t BgL_auxz00_7828;
BgL_auxz00_7828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(68700L), BGl_string2951z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_namez00_174); 
FAILURE(BgL_auxz00_7828,BFALSE,BFALSE);} 
return 
BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_7825, BGl_proc2952z00zz__r4_ports_6_10_1z00);}  else 
{ /* Ieee/port.scm 1516 */
return BFALSE;} } } 

}



/* &file-gzip? */
obj_t BGl_z62filezd2gza7ipzf3ze4zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3805, obj_t BgL_namez00_3806)
{
{ /* Ieee/port.scm 1515 */
return 
BGl_filezd2gza7ipzf3z86zz__r4_ports_6_10_1z00(BgL_namez00_3806);} 

}



/* &<@anonymous:1607> */
obj_t BGl_z62zc3z04anonymousza31607ze3ze5zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3807)
{
{ /* Ieee/port.scm 1518 */
{ /* Ieee/port.scm 1519 */
 obj_t BgL_env1150z00_4615;
BgL_env1150z00_4615 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_cell1145z00_4616;
BgL_cell1145z00_4616 = 
MAKE_STACK_CELL(BUNSPEC); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_val1149z00_4617;
BgL_val1149z00_4617 = 
BGl_zc3z04exitza31609ze3ze70z60zz__r4_ports_6_10_1z00(BgL_cell1145z00_4616, BgL_env1150z00_4615); 
if(
(BgL_val1149z00_4617==BgL_cell1145z00_4616))
{ /* Ieee/port.scm 1519 */
{ /* Ieee/port.scm 1519 */
 int BgL_tmpz00_7839;
BgL_tmpz00_7839 = 
(int)(0L); 
BGL_SIGSETMASK(BgL_tmpz00_7839); } 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_arg1608z00_4618;
BgL_arg1608z00_4618 = 
CELL_REF(
((obj_t)BgL_val1149z00_4617)); 
return BFALSE;} }  else 
{ /* Ieee/port.scm 1519 */
return BgL_val1149z00_4617;} } } } } 

}



/* <@exit:1609>~0 */
obj_t BGl_zc3z04exitza31609ze3ze70z60zz__r4_ports_6_10_1z00(obj_t BgL_cell1145z00_3869, obj_t BgL_env1150z00_3868)
{
{ /* Ieee/port.scm 1519 */
jmp_buf_t jmpbuf; 
 void * BgL_an_exit1154z00_1924;
if( SET_EXIT(BgL_an_exit1154z00_1924 ) ) { 
return 
BGL_EXIT_VALUE();
} else {
#if( SIGSETJMP_SAVESIGS == 0 )
  // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
  // bgl_restore_signal_handlers();
#endif

BgL_an_exit1154z00_1924 = 
(void *)jmpbuf; 
PUSH_ENV_EXIT(BgL_env1150z00_3868, BgL_an_exit1154z00_1924, 1L); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_escape1146z00_1925;
BgL_escape1146z00_1925 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1150z00_3868); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_res1157z00_1926;
{ /* Ieee/port.scm 1519 */
 obj_t BgL_ohs1142z00_1927;
BgL_ohs1142z00_1927 = 
BGL_ENV_ERROR_HANDLER_GET(BgL_env1150z00_3868); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_hds1143z00_1928;
BgL_hds1143z00_1928 = 
MAKE_STACK_PAIR(BgL_escape1146z00_1925, BgL_cell1145z00_3869); 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1150z00_3868, BgL_hds1143z00_1928); BUNSPEC; 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_exitd1151z00_1929;
BgL_exitd1151z00_1929 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1150z00_3868); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_tmp1153z00_1930;
{ /* Ieee/port.scm 1519 */
 obj_t BgL_arg1611z00_1933;
BgL_arg1611z00_1933 = 
BGL_EXITD_PROTECT(BgL_exitd1151z00_1929); 
BgL_tmp1153z00_1930 = 
MAKE_YOUNG_PAIR(BgL_ohs1142z00_1927, BgL_arg1611z00_1933); } 
{ /* Ieee/port.scm 1519 */

BGL_EXITD_PROTECT_SET(BgL_exitd1151z00_1929, BgL_tmp1153z00_1930); BUNSPEC; 
{ /* Ieee/port.scm 1522 */
 obj_t BgL_tmp1152z00_1931;
{ /* Ieee/port.scm 1522 */
 obj_t BgL_arg1610z00_1932;
{ /* Ieee/port.scm 672 */
 obj_t BgL_tmpz00_7855;
BgL_tmpz00_7855 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1610z00_1932 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_7855); } 
BgL_tmp1152z00_1931 = 
BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(BgL_arg1610z00_1932); } 
{ /* Ieee/port.scm 1519 */
 bool_t BgL_test3762z00_7859;
{ /* Ieee/port.scm 1519 */
 obj_t BgL_arg2069z00_2891;
BgL_arg2069z00_2891 = 
BGL_EXITD_PROTECT(BgL_exitd1151z00_1929); 
BgL_test3762z00_7859 = 
PAIRP(BgL_arg2069z00_2891); } 
if(BgL_test3762z00_7859)
{ /* Ieee/port.scm 1519 */
 obj_t BgL_arg2067z00_2892;
{ /* Ieee/port.scm 1519 */
 obj_t BgL_arg2068z00_2893;
BgL_arg2068z00_2893 = 
BGL_EXITD_PROTECT(BgL_exitd1151z00_1929); 
{ /* Ieee/port.scm 1519 */
 obj_t BgL_pairz00_2894;
if(
PAIRP(BgL_arg2068z00_2893))
{ /* Ieee/port.scm 1519 */
BgL_pairz00_2894 = BgL_arg2068z00_2893; }  else 
{ 
 obj_t BgL_auxz00_7865;
BgL_auxz00_7865 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(68749L), BGl_string2953z00zz__r4_ports_6_10_1z00, BGl_string2714z00zz__r4_ports_6_10_1z00, BgL_arg2068z00_2893); 
FAILURE(BgL_auxz00_7865,BFALSE,BFALSE);} 
BgL_arg2067z00_2892 = 
CDR(BgL_pairz00_2894); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1151z00_1929, BgL_arg2067z00_2892); BUNSPEC; }  else 
{ /* Ieee/port.scm 1519 */BFALSE; } } 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1150z00_3868, BgL_ohs1142z00_1927); BUNSPEC; 
BgL_res1157z00_1926 = BgL_tmp1152z00_1931; } } } } } } 
POP_ENV_EXIT(BgL_env1150z00_3868); 
return BgL_res1157z00_1926;} } 
}} 

}



/* delete-file */
BGL_EXPORTED_DEF obj_t BGl_deletezd2filezd2zz__r4_ports_6_10_1z00(char * BgL_stringz00_175)
{
{ /* Ieee/port.scm 1527 */
if(
unlink(BgL_stringz00_175))
{ /* Ieee/port.scm 1528 */
return BFALSE;}  else 
{ /* Ieee/port.scm 1528 */
return BTRUE;} } 

}



/* &delete-file */
obj_t BGl_z62deletezd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3808, obj_t BgL_stringz00_3809)
{
{ /* Ieee/port.scm 1527 */
{ /* Ieee/port.scm 1528 */
 char * BgL_auxz00_7875;
{ /* Ieee/port.scm 1528 */
 obj_t BgL_tmpz00_7876;
if(
STRINGP(BgL_stringz00_3809))
{ /* Ieee/port.scm 1528 */
BgL_tmpz00_7876 = BgL_stringz00_3809
; }  else 
{ 
 obj_t BgL_auxz00_7879;
BgL_auxz00_7879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(69104L), BGl_string2954z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3809); 
FAILURE(BgL_auxz00_7879,BFALSE,BFALSE);} 
BgL_auxz00_7875 = 
BSTRING_TO_STRING(BgL_tmpz00_7876); } 
return 
BGl_deletezd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_7875);} } 

}



/* make-directory */
BGL_EXPORTED_DEF bool_t BGl_makezd2directoryzd2zz__r4_ports_6_10_1z00(char * BgL_stringz00_176)
{
{ /* Ieee/port.scm 1533 */
return 
!BGL_MKDIR(BgL_stringz00_176, 511L);} 

}



/* &make-directory */
obj_t BGl_z62makezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3810, obj_t BgL_stringz00_3811)
{
{ /* Ieee/port.scm 1533 */
{ /* Ieee/port.scm 1534 */
 bool_t BgL_tmpz00_7886;
{ /* Ieee/port.scm 1534 */
 char * BgL_auxz00_7887;
{ /* Ieee/port.scm 1534 */
 obj_t BgL_tmpz00_7888;
if(
STRINGP(BgL_stringz00_3811))
{ /* Ieee/port.scm 1534 */
BgL_tmpz00_7888 = BgL_stringz00_3811
; }  else 
{ 
 obj_t BgL_auxz00_7891;
BgL_auxz00_7891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(69399L), BGl_string2955z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3811); 
FAILURE(BgL_auxz00_7891,BFALSE,BFALSE);} 
BgL_auxz00_7887 = 
BSTRING_TO_STRING(BgL_tmpz00_7888); } 
BgL_tmpz00_7886 = 
BGl_makezd2directoryzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7887); } 
return 
BBOOL(BgL_tmpz00_7886);} } 

}



/* make-directories */
BGL_EXPORTED_DEF bool_t BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(obj_t BgL_stringz00_177)
{
{ /* Ieee/port.scm 1539 */
{ /* Ieee/port.scm 1540 */
 bool_t BgL__ortest_1159z00_1939;
BgL__ortest_1159z00_1939 = 
bgl_directoryp(
BSTRING_TO_STRING(BgL_stringz00_177)); 
if(BgL__ortest_1159z00_1939)
{ /* Ieee/port.scm 1540 */
return BgL__ortest_1159z00_1939;}  else 
{ /* Ieee/port.scm 1541 */
 bool_t BgL__ortest_1160z00_1940;
{ /* Ieee/port.scm 1534 */
 char * BgL_tmpz00_7901;
BgL_tmpz00_7901 = 
BSTRING_TO_STRING(BgL_stringz00_177); 
BgL__ortest_1160z00_1940 = 
!BGL_MKDIR(BgL_tmpz00_7901, 511L); } 
if(BgL__ortest_1160z00_1940)
{ /* Ieee/port.scm 1541 */
return BgL__ortest_1160z00_1940;}  else 
{ /* Ieee/port.scm 1542 */
 obj_t BgL_dnamez00_1941;
BgL_dnamez00_1941 = 
BGl_dirnamez00zz__osz00(BgL_stringz00_177); 
{ /* Ieee/port.scm 1543 */
 bool_t BgL_test3769z00_7906;
if(
(
STRING_LENGTH(BgL_dnamez00_1941)==0L))
{ /* Ieee/port.scm 1543 */
BgL_test3769z00_7906 = ((bool_t)1)
; }  else 
{ /* Ieee/port.scm 1543 */
BgL_test3769z00_7906 = 
fexists(
BSTRING_TO_STRING(BgL_dnamez00_1941))
; } 
if(BgL_test3769z00_7906)
{ /* Ieee/port.scm 1543 */
return ((bool_t)0);}  else 
{ /* Ieee/port.scm 1545 */
 bool_t BgL_auxz00_1946;
BgL_auxz00_1946 = 
BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(BgL_dnamez00_1941); 
{ /* Ieee/port.scm 1546 */
 bool_t BgL_test3771z00_7913;
{ /* Ieee/port.scm 1546 */
 unsigned char BgL_char2z00_2907;
BgL_char2z00_2907 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Ieee/port.scm 1546 */
 unsigned char BgL_tmpz00_7915;
{ /* Ieee/port.scm 1546 */
 long BgL_i2112z00_3882;
BgL_i2112z00_3882 = 
(
STRING_LENGTH(BgL_stringz00_177)-1L); 
{ /* Ieee/port.scm 1546 */
 long BgL_l2113z00_3883;
BgL_l2113z00_3883 = 
STRING_LENGTH(BgL_stringz00_177); 
if(
BOUND_CHECK(BgL_i2112z00_3882, BgL_l2113z00_3883))
{ /* Ieee/port.scm 1546 */
BgL_tmpz00_7915 = 
STRING_REF(BgL_stringz00_177, BgL_i2112z00_3882)
; }  else 
{ 
 obj_t BgL_auxz00_7922;
BgL_auxz00_7922 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(69910L), BGl_string2956z00zz__r4_ports_6_10_1z00, BgL_stringz00_177, 
(int)(BgL_l2113z00_3883), 
(int)(BgL_i2112z00_3882)); 
FAILURE(BgL_auxz00_7922,BFALSE,BFALSE);} } } 
BgL_test3771z00_7913 = 
(BgL_tmpz00_7915==BgL_char2z00_2907); } } 
if(BgL_test3771z00_7913)
{ /* Ieee/port.scm 1546 */
return BgL_auxz00_1946;}  else 
{ /* Ieee/port.scm 1534 */
 char * BgL_tmpz00_7929;
BgL_tmpz00_7929 = 
BSTRING_TO_STRING(BgL_stringz00_177); 
return 
!BGL_MKDIR(BgL_tmpz00_7929, 511L);} } } } } } } } 

}



/* &make-directories */
obj_t BGl_z62makezd2directorieszb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3812, obj_t BgL_stringz00_3813)
{
{ /* Ieee/port.scm 1539 */
{ /* Ieee/port.scm 1540 */
 bool_t BgL_tmpz00_7932;
{ /* Ieee/port.scm 1540 */
 obj_t BgL_auxz00_7933;
if(
STRINGP(BgL_stringz00_3813))
{ /* Ieee/port.scm 1540 */
BgL_auxz00_7933 = BgL_stringz00_3813
; }  else 
{ 
 obj_t BgL_auxz00_7936;
BgL_auxz00_7936 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(69687L), BGl_string2957z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3813); 
FAILURE(BgL_auxz00_7936,BFALSE,BFALSE);} 
BgL_tmpz00_7932 = 
BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(BgL_auxz00_7933); } 
return 
BBOOL(BgL_tmpz00_7932);} } 

}



/* delete-directory */
BGL_EXPORTED_DEF obj_t BGl_deletezd2directoryzd2zz__r4_ports_6_10_1z00(char * BgL_stringz00_178)
{
{ /* Ieee/port.scm 1554 */
if(
rmdir(BgL_stringz00_178))
{ /* Ieee/port.scm 1555 */
return BFALSE;}  else 
{ /* Ieee/port.scm 1555 */
return BTRUE;} } 

}



/* &delete-directory */
obj_t BGl_z62deletezd2directoryzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3814, obj_t BgL_stringz00_3815)
{
{ /* Ieee/port.scm 1554 */
{ /* Ieee/port.scm 1555 */
 char * BgL_auxz00_7944;
{ /* Ieee/port.scm 1555 */
 obj_t BgL_tmpz00_7945;
if(
STRINGP(BgL_stringz00_3815))
{ /* Ieee/port.scm 1555 */
BgL_tmpz00_7945 = BgL_stringz00_3815
; }  else 
{ 
 obj_t BgL_auxz00_7948;
BgL_auxz00_7948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(70308L), BGl_string2958z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3815); 
FAILURE(BgL_auxz00_7948,BFALSE,BFALSE);} 
BgL_auxz00_7944 = 
BSTRING_TO_STRING(BgL_tmpz00_7945); } 
return 
BGl_deletezd2directoryzd2zz__r4_ports_6_10_1z00(BgL_auxz00_7944);} } 

}



/* rename-file */
BGL_EXPORTED_DEF bool_t BGl_renamezd2filezd2zz__r4_ports_6_10_1z00(char * BgL_string1z00_179, char * BgL_string2z00_180)
{
{ /* Ieee/port.scm 1560 */
{ /* Ieee/port.scm 1561 */
 int BgL_arg1634z00_4619;
BgL_arg1634z00_4619 = 
rename(BgL_string1z00_179, BgL_string2z00_180); 
return 
(
BINT(BgL_arg1634z00_4619)==
BINT(0L));} } 

}



/* &rename-file */
obj_t BGl_z62renamezd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3816, obj_t BgL_string1z00_3817, obj_t BgL_string2z00_3818)
{
{ /* Ieee/port.scm 1560 */
{ /* Ieee/port.scm 1561 */
 bool_t BgL_tmpz00_7958;
{ /* Ieee/port.scm 1561 */
 char * BgL_auxz00_7968; char * BgL_auxz00_7959;
{ /* Ieee/port.scm 1561 */
 obj_t BgL_tmpz00_7969;
if(
STRINGP(BgL_string2z00_3818))
{ /* Ieee/port.scm 1561 */
BgL_tmpz00_7969 = BgL_string2z00_3818
; }  else 
{ 
 obj_t BgL_auxz00_7972;
BgL_auxz00_7972 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(70623L), BGl_string2959z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_string2z00_3818); 
FAILURE(BgL_auxz00_7972,BFALSE,BFALSE);} 
BgL_auxz00_7968 = 
BSTRING_TO_STRING(BgL_tmpz00_7969); } 
{ /* Ieee/port.scm 1561 */
 obj_t BgL_tmpz00_7960;
if(
STRINGP(BgL_string1z00_3817))
{ /* Ieee/port.scm 1561 */
BgL_tmpz00_7960 = BgL_string1z00_3817
; }  else 
{ 
 obj_t BgL_auxz00_7963;
BgL_auxz00_7963 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(70623L), BGl_string2959z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_string1z00_3817); 
FAILURE(BgL_auxz00_7963,BFALSE,BFALSE);} 
BgL_auxz00_7959 = 
BSTRING_TO_STRING(BgL_tmpz00_7960); } 
BgL_tmpz00_7958 = 
BGl_renamezd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_7959, BgL_auxz00_7968); } 
return 
BBOOL(BgL_tmpz00_7958);} } 

}



/* truncate-file */
BGL_EXPORTED_DEF bool_t BGl_truncatezd2filezd2zz__r4_ports_6_10_1z00(char * BgL_pathz00_181, long BgL_siza7eza7_182)
{
{ /* Ieee/port.scm 1566 */
{ /* Ieee/port.scm 1567 */
 int BgL_xz00_4620;
BgL_xz00_4620 = 
truncate(BgL_pathz00_181, BgL_siza7eza7_182); 
return ((bool_t)1);} } 

}



/* &truncate-file */
obj_t BGl_z62truncatezd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3819, obj_t BgL_pathz00_3820, obj_t BgL_siza7eza7_3821)
{
{ /* Ieee/port.scm 1566 */
{ /* Ieee/port.scm 1567 */
 bool_t BgL_tmpz00_7980;
{ /* Ieee/port.scm 1567 */
 long BgL_auxz00_7990; char * BgL_auxz00_7981;
{ /* Ieee/port.scm 1567 */
 obj_t BgL_tmpz00_7991;
if(
INTEGERP(BgL_siza7eza7_3821))
{ /* Ieee/port.scm 1567 */
BgL_tmpz00_7991 = BgL_siza7eza7_3821
; }  else 
{ 
 obj_t BgL_auxz00_7994;
BgL_auxz00_7994 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(70933L), BGl_string2960z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_siza7eza7_3821); 
FAILURE(BgL_auxz00_7994,BFALSE,BFALSE);} 
BgL_auxz00_7990 = 
(long)CINT(BgL_tmpz00_7991); } 
{ /* Ieee/port.scm 1567 */
 obj_t BgL_tmpz00_7982;
if(
STRINGP(BgL_pathz00_3820))
{ /* Ieee/port.scm 1567 */
BgL_tmpz00_7982 = BgL_pathz00_3820
; }  else 
{ 
 obj_t BgL_auxz00_7985;
BgL_auxz00_7985 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(70933L), BGl_string2960z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_pathz00_3820); 
FAILURE(BgL_auxz00_7985,BFALSE,BFALSE);} 
BgL_auxz00_7981 = 
BSTRING_TO_STRING(BgL_tmpz00_7982); } 
BgL_tmpz00_7980 = 
BGl_truncatezd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_7981, BgL_auxz00_7990); } 
return 
BBOOL(BgL_tmpz00_7980);} } 

}



/* output-port-truncate */
BGL_EXPORTED_DEF bool_t BGl_outputzd2portzd2truncatez00zz__r4_ports_6_10_1z00(obj_t BgL_oportz00_183, long BgL_siza7eza7_184)
{
{ /* Ieee/port.scm 1572 */
{ /* Ieee/port.scm 1573 */
 int BgL_xz00_4621;
BgL_xz00_4621 = 
bgl_output_port_truncate(BgL_oportz00_183, BgL_siza7eza7_184); 
return ((bool_t)1);} } 

}



/* &output-port-truncate */
obj_t BGl_z62outputzd2portzd2truncatez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3822, obj_t BgL_oportz00_3823, obj_t BgL_siza7eza7_3824)
{
{ /* Ieee/port.scm 1572 */
{ /* Ieee/port.scm 1573 */
 bool_t BgL_tmpz00_8002;
{ /* Ieee/port.scm 1573 */
 long BgL_auxz00_8010; obj_t BgL_auxz00_8003;
{ /* Ieee/port.scm 1573 */
 obj_t BgL_tmpz00_8011;
if(
INTEGERP(BgL_siza7eza7_3824))
{ /* Ieee/port.scm 1573 */
BgL_tmpz00_8011 = BgL_siza7eza7_3824
; }  else 
{ 
 obj_t BgL_auxz00_8014;
BgL_auxz00_8014 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(71236L), BGl_string2961z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_siza7eza7_3824); 
FAILURE(BgL_auxz00_8014,BFALSE,BFALSE);} 
BgL_auxz00_8010 = 
(long)CINT(BgL_tmpz00_8011); } 
if(
OUTPUT_PORTP(BgL_oportz00_3823))
{ /* Ieee/port.scm 1573 */
BgL_auxz00_8003 = BgL_oportz00_3823
; }  else 
{ 
 obj_t BgL_auxz00_8006;
BgL_auxz00_8006 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(71236L), BGl_string2961z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_oportz00_3823); 
FAILURE(BgL_auxz00_8006,BFALSE,BFALSE);} 
BgL_tmpz00_8002 = 
BGl_outputzd2portzd2truncatez00zz__r4_ports_6_10_1z00(BgL_auxz00_8003, BgL_auxz00_8010); } 
return 
BBOOL(BgL_tmpz00_8002);} } 

}



/* copy-file */
BGL_EXPORTED_DEF obj_t BGl_copyzd2filezd2zz__r4_ports_6_10_1z00(char * BgL_string1z00_185, char * BgL_string2z00_186)
{
{ /* Ieee/port.scm 1578 */
{ /* Ieee/port.scm 1579 */
 obj_t BgL_piz00_1962;
BgL_piz00_1962 = 
BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(
string_to_bstring(BgL_string1z00_185)); 
if(
BINARY_PORTP(BgL_piz00_1962))
{ /* Ieee/port.scm 1581 */
 obj_t BgL_poz00_1964;
BgL_poz00_1964 = 
BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(
string_to_bstring(BgL_string2z00_186)); 
if(
BINARY_PORTP(BgL_poz00_1964))
{ /* Ieee/port.scm 1586 */
 obj_t BgL_sz00_1966;
{ /* Ieee/string.scm 172 */

BgL_sz00_1966 = 
make_string(1024L, ((unsigned char)' ')); } 
{ /* Ieee/port.scm 1587 */
 int BgL_g1161z00_1967;
BgL_g1161z00_1967 = 
bgl_input_fill_string(BgL_piz00_1962, BgL_sz00_1966); 
{ 
 int BgL_lz00_1969;
{ /* Ieee/port.scm 1587 */
 bool_t BgL_tmpz00_8031;
BgL_lz00_1969 = BgL_g1161z00_1967; 
BgL_zc3z04anonymousza31637ze3z87_1970:
if(
(
(long)(BgL_lz00_1969)==1024L))
{ /* Ieee/port.scm 1588 */
bgl_output_string(
((obj_t)BgL_poz00_1964), BgL_sz00_1966); 
{ /* Ieee/port.scm 1591 */
 int BgL_arg1639z00_1972;
BgL_arg1639z00_1972 = 
bgl_input_fill_string(
((obj_t)BgL_piz00_1962), BgL_sz00_1966); 
{ 
 int BgL_lz00_8039;
BgL_lz00_8039 = BgL_arg1639z00_1972; 
BgL_lz00_1969 = BgL_lz00_8039; 
goto BgL_zc3z04anonymousza31637ze3z87_1970;} } }  else 
{ /* Ieee/port.scm 1588 */
{ /* Ieee/port.scm 1593 */
 obj_t BgL_arg1640z00_1973;
{ /* Ieee/port.scm 1593 */
 long BgL_tmpz00_8040;
BgL_tmpz00_8040 = 
(long)(BgL_lz00_1969); 
BgL_arg1640z00_1973 = 
bgl_string_shrink(BgL_sz00_1966, BgL_tmpz00_8040); } 
bgl_output_string(
((obj_t)BgL_poz00_1964), BgL_arg1640z00_1973); } 
close_binary_port(
((obj_t)BgL_piz00_1962)); 
close_binary_port(
((obj_t)BgL_poz00_1964)); 
BgL_tmpz00_8031 = ((bool_t)1); } 
return 
BBOOL(BgL_tmpz00_8031);} } } }  else 
{ /* Ieee/port.scm 1582 */
close_binary_port(BgL_piz00_1962); 
return BFALSE;} }  else 
{ /* Ieee/port.scm 1580 */
return BFALSE;} } } 

}



/* &copy-file */
obj_t BGl_z62copyzd2filezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3825, obj_t BgL_string1z00_3826, obj_t BgL_string2z00_3827)
{
{ /* Ieee/port.scm 1578 */
{ /* Ieee/port.scm 1579 */
 char * BgL_auxz00_8060; char * BgL_auxz00_8051;
{ /* Ieee/port.scm 1579 */
 obj_t BgL_tmpz00_8061;
if(
STRINGP(BgL_string2z00_3827))
{ /* Ieee/port.scm 1579 */
BgL_tmpz00_8061 = BgL_string2z00_3827
; }  else 
{ 
 obj_t BgL_auxz00_8064;
BgL_auxz00_8064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(71523L), BGl_string2962z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_string2z00_3827); 
FAILURE(BgL_auxz00_8064,BFALSE,BFALSE);} 
BgL_auxz00_8060 = 
BSTRING_TO_STRING(BgL_tmpz00_8061); } 
{ /* Ieee/port.scm 1579 */
 obj_t BgL_tmpz00_8052;
if(
STRINGP(BgL_string1z00_3826))
{ /* Ieee/port.scm 1579 */
BgL_tmpz00_8052 = BgL_string1z00_3826
; }  else 
{ 
 obj_t BgL_auxz00_8055;
BgL_auxz00_8055 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(71523L), BGl_string2962z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_string1z00_3826); 
FAILURE(BgL_auxz00_8055,BFALSE,BFALSE);} 
BgL_auxz00_8051 = 
BSTRING_TO_STRING(BgL_tmpz00_8052); } 
return 
BGl_copyzd2filezd2zz__r4_ports_6_10_1z00(BgL_auxz00_8051, BgL_auxz00_8060);} } 

}



/* port? */
BGL_EXPORTED_DEF bool_t BGl_portzf3zf3zz__r4_ports_6_10_1z00(obj_t BgL_objz00_187)
{
{ /* Ieee/port.scm 1601 */
{ /* Ieee/port.scm 1602 */
 bool_t BgL__ortest_1162z00_4622;
BgL__ortest_1162z00_4622 = 
OUTPUT_PORTP(BgL_objz00_187); 
if(BgL__ortest_1162z00_4622)
{ /* Ieee/port.scm 1602 */
return BgL__ortest_1162z00_4622;}  else 
{ /* Ieee/port.scm 1602 */
return 
INPUT_PORTP(BgL_objz00_187);} } } 

}



/* &port? */
obj_t BGl_z62portzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3828, obj_t BgL_objz00_3829)
{
{ /* Ieee/port.scm 1601 */
return 
BBOOL(
BGl_portzf3zf3zz__r4_ports_6_10_1z00(BgL_objz00_3829));} 

}



/* directory? */
BGL_EXPORTED_DEF bool_t BGl_directoryzf3zf3zz__r4_ports_6_10_1z00(char * BgL_stringz00_188)
{
{ /* Ieee/port.scm 1607 */
BGL_TAIL return 
bgl_directoryp(BgL_stringz00_188);} 

}



/* &directory? */
obj_t BGl_z62directoryzf3z91zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3830, obj_t BgL_stringz00_3831)
{
{ /* Ieee/port.scm 1607 */
{ /* Ieee/port.scm 1608 */
 bool_t BgL_tmpz00_8076;
{ /* Ieee/port.scm 1608 */
 char * BgL_auxz00_8077;
{ /* Ieee/port.scm 1608 */
 obj_t BgL_tmpz00_8078;
if(
STRINGP(BgL_stringz00_3831))
{ /* Ieee/port.scm 1608 */
BgL_tmpz00_8078 = BgL_stringz00_3831
; }  else 
{ 
 obj_t BgL_auxz00_8081;
BgL_auxz00_8081 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(72611L), BGl_string2963z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3831); 
FAILURE(BgL_auxz00_8081,BFALSE,BFALSE);} 
BgL_auxz00_8077 = 
BSTRING_TO_STRING(BgL_tmpz00_8078); } 
BgL_tmpz00_8076 = 
BGl_directoryzf3zf3zz__r4_ports_6_10_1z00(BgL_auxz00_8077); } 
return 
BBOOL(BgL_tmpz00_8076);} } 

}



/* directory-length */
BGL_EXPORTED_DEF obj_t BGl_directoryzd2lengthzd2zz__r4_ports_6_10_1z00(char * BgL_stringz00_189)
{
{ /* Ieee/port.scm 1613 */
return 
BINT(
bgl_directory_length(BgL_stringz00_189));} 

}



/* &directory-length */
obj_t BGl_z62directoryzd2lengthzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3832, obj_t BgL_stringz00_3833)
{
{ /* Ieee/port.scm 1613 */
{ /* Ieee/port.scm 1614 */
 char * BgL_auxz00_8090;
{ /* Ieee/port.scm 1614 */
 obj_t BgL_tmpz00_8091;
if(
STRINGP(BgL_stringz00_3833))
{ /* Ieee/port.scm 1614 */
BgL_tmpz00_8091 = BgL_stringz00_3833
; }  else 
{ 
 obj_t BgL_auxz00_8094;
BgL_auxz00_8094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(72900L), BGl_string2964z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3833); 
FAILURE(BgL_auxz00_8094,BFALSE,BFALSE);} 
BgL_auxz00_8090 = 
BSTRING_TO_STRING(BgL_tmpz00_8091); } 
return 
BGl_directoryzd2lengthzd2zz__r4_ports_6_10_1z00(BgL_auxz00_8090);} } 

}



/* directory->list */
BGL_EXPORTED_DEF obj_t BGl_directoryzd2ze3listz31zz__r4_ports_6_10_1z00(char * BgL_stringz00_190)
{
{ /* Ieee/port.scm 1619 */
BGL_TAIL return 
bgl_directory_to_list(BgL_stringz00_190);} 

}



/* &directory->list */
obj_t BGl_z62directoryzd2ze3listz53zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3834, obj_t BgL_stringz00_3835)
{
{ /* Ieee/port.scm 1619 */
{ /* Ieee/port.scm 1620 */
 char * BgL_auxz00_8101;
{ /* Ieee/port.scm 1620 */
 obj_t BgL_tmpz00_8102;
if(
STRINGP(BgL_stringz00_3835))
{ /* Ieee/port.scm 1620 */
BgL_tmpz00_8102 = BgL_stringz00_3835
; }  else 
{ 
 obj_t BgL_auxz00_8105;
BgL_auxz00_8105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(73194L), BGl_string2965z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3835); 
FAILURE(BgL_auxz00_8105,BFALSE,BFALSE);} 
BgL_auxz00_8101 = 
BSTRING_TO_STRING(BgL_tmpz00_8102); } 
return 
BGl_directoryzd2ze3listz31zz__r4_ports_6_10_1z00(BgL_auxz00_8101);} } 

}



/* directory->path-list */
BGL_EXPORTED_DEF obj_t BGl_directoryzd2ze3pathzd2listze3zz__r4_ports_6_10_1z00(obj_t BgL_dirz00_191)
{
{ /* Ieee/port.scm 1625 */
{ /* Ieee/port.scm 1626 */
 long BgL_lz00_1978;
BgL_lz00_1978 = 
STRING_LENGTH(BgL_dirz00_191); 
if(
(BgL_lz00_1978==0L))
{ /* Ieee/port.scm 1628 */
return BNIL;}  else 
{ /* Ieee/port.scm 1630 */
 bool_t BgL_test3792z00_8114;
{ /* Ieee/port.scm 1630 */
 unsigned char BgL_char2z00_2934;
BgL_char2z00_2934 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Ieee/port.scm 1630 */
 unsigned char BgL_tmpz00_8116;
{ /* Ieee/port.scm 1630 */
 long BgL_i2116z00_3886;
BgL_i2116z00_3886 = 
(BgL_lz00_1978-1L); 
if(
BOUND_CHECK(BgL_i2116z00_3886, BgL_lz00_1978))
{ /* Ieee/port.scm 1630 */
BgL_tmpz00_8116 = 
STRING_REF(BgL_dirz00_191, BgL_i2116z00_3886)
; }  else 
{ 
 obj_t BgL_auxz00_8121;
BgL_auxz00_8121 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(73557L), BGl_string2956z00zz__r4_ports_6_10_1z00, BgL_dirz00_191, 
(int)(BgL_lz00_1978), 
(int)(BgL_i2116z00_3886)); 
FAILURE(BgL_auxz00_8121,BFALSE,BFALSE);} } 
BgL_test3792z00_8114 = 
(BgL_tmpz00_8116==BgL_char2z00_2934); } } 
if(BgL_test3792z00_8114)
{ /* Ieee/port.scm 1633 */
 long BgL_arg1646z00_1984;
BgL_arg1646z00_1984 = 
(BgL_lz00_1978-1L); 
return 
bgl_directory_to_path_list(
BSTRING_TO_STRING(BgL_dirz00_191), 
(int)(BgL_arg1646z00_1984), FILE_SEPARATOR);}  else 
{ /* Ieee/port.scm 1630 */
return 
bgl_directory_to_path_list(
BSTRING_TO_STRING(BgL_dirz00_191), 
(int)(BgL_lz00_1978), FILE_SEPARATOR);} } } } 

}



/* &directory->path-list */
obj_t BGl_z62directoryzd2ze3pathzd2listz81zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3836, obj_t BgL_dirz00_3837)
{
{ /* Ieee/port.scm 1625 */
{ /* Ieee/port.scm 1626 */
 obj_t BgL_auxz00_8135;
if(
STRINGP(BgL_dirz00_3837))
{ /* Ieee/port.scm 1626 */
BgL_auxz00_8135 = BgL_dirz00_3837
; }  else 
{ 
 obj_t BgL_auxz00_8138;
BgL_auxz00_8138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(73482L), BGl_string2966z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_dirz00_3837); 
FAILURE(BgL_auxz00_8138,BFALSE,BFALSE);} 
return 
BGl_directoryzd2ze3pathzd2listze3zz__r4_ports_6_10_1z00(BgL_auxz00_8135);} } 

}



/* directory->vector */
BGL_EXPORTED_DEF obj_t BGl_directoryzd2ze3vectorz31zz__r4_ports_6_10_1z00(char * BgL_stringz00_192)
{
{ /* Ieee/port.scm 1648 */
BGL_TAIL return 
bgl_directory_to_vector(BgL_stringz00_192);} 

}



/* &directory->vector */
obj_t BGl_z62directoryzd2ze3vectorz53zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3838, obj_t BgL_stringz00_3839)
{
{ /* Ieee/port.scm 1648 */
{ /* Ieee/port.scm 1649 */
 char * BgL_auxz00_8144;
{ /* Ieee/port.scm 1649 */
 obj_t BgL_tmpz00_8145;
if(
STRINGP(BgL_stringz00_3839))
{ /* Ieee/port.scm 1649 */
BgL_tmpz00_8145 = BgL_stringz00_3839
; }  else 
{ 
 obj_t BgL_auxz00_8148;
BgL_auxz00_8148 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(74246L), BGl_string2967z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_stringz00_3839); 
FAILURE(BgL_auxz00_8148,BFALSE,BFALSE);} 
BgL_auxz00_8144 = 
BSTRING_TO_STRING(BgL_tmpz00_8145); } 
return 
BGl_directoryzd2ze3vectorz31zz__r4_ports_6_10_1z00(BgL_auxz00_8144);} } 

}



/* directory->path-vector */
BGL_EXPORTED_DEF obj_t BGl_directoryzd2ze3pathzd2vectorze3zz__r4_ports_6_10_1z00(obj_t BgL_dirz00_193)
{
{ /* Ieee/port.scm 1654 */
{ /* Ieee/port.scm 1655 */
 long BgL_lz00_1990;
BgL_lz00_1990 = 
STRING_LENGTH(BgL_dirz00_193); 
if(
(BgL_lz00_1990==0L))
{ /* Ieee/port.scm 1657 */
return BGl_vector2968z00zz__r4_ports_6_10_1z00;}  else 
{ /* Ieee/port.scm 1659 */
 bool_t BgL_test3797z00_8157;
{ /* Ieee/port.scm 1659 */
 unsigned char BgL_char2z00_2942;
BgL_char2z00_2942 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Ieee/port.scm 1659 */
 unsigned char BgL_tmpz00_8159;
{ /* Ieee/port.scm 1659 */
 long BgL_i2120z00_3890;
BgL_i2120z00_3890 = 
(BgL_lz00_1990-1L); 
if(
BOUND_CHECK(BgL_i2120z00_3890, BgL_lz00_1990))
{ /* Ieee/port.scm 1659 */
BgL_tmpz00_8159 = 
STRING_REF(BgL_dirz00_193, BgL_i2120z00_3890)
; }  else 
{ 
 obj_t BgL_auxz00_8164;
BgL_auxz00_8164 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(74614L), BGl_string2956z00zz__r4_ports_6_10_1z00, BgL_dirz00_193, 
(int)(BgL_lz00_1990), 
(int)(BgL_i2120z00_3890)); 
FAILURE(BgL_auxz00_8164,BFALSE,BFALSE);} } 
BgL_test3797z00_8157 = 
(BgL_tmpz00_8159==BgL_char2z00_2942); } } 
if(BgL_test3797z00_8157)
{ /* Ieee/port.scm 1662 */
 long BgL_arg1661z00_1996;
BgL_arg1661z00_1996 = 
(BgL_lz00_1990-1L); 
return 
bgl_directory_to_path_vector(
BSTRING_TO_STRING(BgL_dirz00_193), 
(int)(BgL_arg1661z00_1996), FILE_SEPARATOR);}  else 
{ /* Ieee/port.scm 1659 */
return 
bgl_directory_to_path_vector(
BSTRING_TO_STRING(BgL_dirz00_193), 
(int)(BgL_lz00_1990), FILE_SEPARATOR);} } } } 

}



/* &directory->path-vector */
obj_t BGl_z62directoryzd2ze3pathzd2vectorz81zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3840, obj_t BgL_dirz00_3841)
{
{ /* Ieee/port.scm 1654 */
{ /* Ieee/port.scm 1655 */
 obj_t BgL_auxz00_8178;
if(
STRINGP(BgL_dirz00_3841))
{ /* Ieee/port.scm 1655 */
BgL_auxz00_8178 = BgL_dirz00_3841
; }  else 
{ 
 obj_t BgL_auxz00_8181;
BgL_auxz00_8181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(74538L), BGl_string2969z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_dirz00_3841); 
FAILURE(BgL_auxz00_8181,BFALSE,BFALSE);} 
return 
BGl_directoryzd2ze3pathzd2vectorze3zz__r4_ports_6_10_1z00(BgL_auxz00_8178);} } 

}



/* file-modification-time */
BGL_EXPORTED_DEF long BGl_filezd2modificationzd2timez00zz__r4_ports_6_10_1z00(char * BgL_filez00_194)
{
{ /* Ieee/port.scm 1677 */
BGL_TAIL return 
bgl_last_modification_time(BgL_filez00_194);} 

}



/* &file-modification-time */
obj_t BGl_z62filezd2modificationzd2timez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3842, obj_t BgL_filez00_3843)
{
{ /* Ieee/port.scm 1677 */
{ /* Ieee/port.scm 1678 */
 long BgL_tmpz00_8187;
{ /* Ieee/port.scm 1678 */
 char * BgL_auxz00_8188;
{ /* Ieee/port.scm 1678 */
 obj_t BgL_tmpz00_8189;
if(
STRINGP(BgL_filez00_3843))
{ /* Ieee/port.scm 1678 */
BgL_tmpz00_8189 = BgL_filez00_3843
; }  else 
{ 
 obj_t BgL_auxz00_8192;
BgL_auxz00_8192 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(75310L), BGl_string2970z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3843); 
FAILURE(BgL_auxz00_8192,BFALSE,BFALSE);} 
BgL_auxz00_8188 = 
BSTRING_TO_STRING(BgL_tmpz00_8189); } 
BgL_tmpz00_8187 = 
BGl_filezd2modificationzd2timez00zz__r4_ports_6_10_1z00(BgL_auxz00_8188); } 
return 
make_belong(BgL_tmpz00_8187);} } 

}



/* file-change-time */
BGL_EXPORTED_DEF long BGl_filezd2changezd2timez00zz__r4_ports_6_10_1z00(char * BgL_filez00_195)
{
{ /* Ieee/port.scm 1683 */
BGL_TAIL return 
bgl_last_change_time(BgL_filez00_195);} 

}



/* &file-change-time */
obj_t BGl_z62filezd2changezd2timez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3844, obj_t BgL_filez00_3845)
{
{ /* Ieee/port.scm 1683 */
{ /* Ieee/port.scm 1684 */
 long BgL_tmpz00_8200;
{ /* Ieee/port.scm 1684 */
 char * BgL_auxz00_8201;
{ /* Ieee/port.scm 1684 */
 obj_t BgL_tmpz00_8202;
if(
STRINGP(BgL_filez00_3845))
{ /* Ieee/port.scm 1684 */
BgL_tmpz00_8202 = BgL_filez00_3845
; }  else 
{ 
 obj_t BgL_auxz00_8205;
BgL_auxz00_8205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(75602L), BGl_string2971z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3845); 
FAILURE(BgL_auxz00_8205,BFALSE,BFALSE);} 
BgL_auxz00_8201 = 
BSTRING_TO_STRING(BgL_tmpz00_8202); } 
BgL_tmpz00_8200 = 
BGl_filezd2changezd2timez00zz__r4_ports_6_10_1z00(BgL_auxz00_8201); } 
return 
make_belong(BgL_tmpz00_8200);} } 

}



/* file-access-time */
BGL_EXPORTED_DEF long BGl_filezd2accesszd2timez00zz__r4_ports_6_10_1z00(char * BgL_filez00_196)
{
{ /* Ieee/port.scm 1689 */
BGL_TAIL return 
bgl_last_access_time(BgL_filez00_196);} 

}



/* &file-access-time */
obj_t BGl_z62filezd2accesszd2timez62zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3846, obj_t BgL_filez00_3847)
{
{ /* Ieee/port.scm 1689 */
{ /* Ieee/port.scm 1690 */
 long BgL_tmpz00_8213;
{ /* Ieee/port.scm 1690 */
 char * BgL_auxz00_8214;
{ /* Ieee/port.scm 1690 */
 obj_t BgL_tmpz00_8215;
if(
STRINGP(BgL_filez00_3847))
{ /* Ieee/port.scm 1690 */
BgL_tmpz00_8215 = BgL_filez00_3847
; }  else 
{ 
 obj_t BgL_auxz00_8218;
BgL_auxz00_8218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(75888L), BGl_string2972z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3847); 
FAILURE(BgL_auxz00_8218,BFALSE,BFALSE);} 
BgL_auxz00_8214 = 
BSTRING_TO_STRING(BgL_tmpz00_8215); } 
BgL_tmpz00_8213 = 
BGl_filezd2accesszd2timez00zz__r4_ports_6_10_1z00(BgL_auxz00_8214); } 
return 
make_belong(BgL_tmpz00_8213);} } 

}



/* file-times-set! */
BGL_EXPORTED_DEF int BGl_filezd2timeszd2setz12z12zz__r4_ports_6_10_1z00(char * BgL_filez00_197, long BgL_atimez00_198, long BgL_mtimez00_199)
{
{ /* Ieee/port.scm 1695 */
BGL_TAIL return 
bgl_utime(BgL_filez00_197, BgL_atimez00_198, BgL_mtimez00_199);} 

}



/* &file-times-set! */
obj_t BGl_z62filezd2timeszd2setz12z70zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3848, obj_t BgL_filez00_3849, obj_t BgL_atimez00_3850, obj_t BgL_mtimez00_3851)
{
{ /* Ieee/port.scm 1695 */
{ /* Ieee/port.scm 1696 */
 int BgL_tmpz00_8226;
{ /* Ieee/port.scm 1696 */
 long BgL_auxz00_8245; long BgL_auxz00_8236; char * BgL_auxz00_8227;
{ /* Ieee/port.scm 1696 */
 obj_t BgL_tmpz00_8246;
if(
ELONGP(BgL_mtimez00_3851))
{ /* Ieee/port.scm 1696 */
BgL_tmpz00_8246 = BgL_mtimez00_3851
; }  else 
{ 
 obj_t BgL_auxz00_8249;
BgL_auxz00_8249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(76185L), BGl_string2973z00zz__r4_ports_6_10_1z00, BGl_string2974z00zz__r4_ports_6_10_1z00, BgL_mtimez00_3851); 
FAILURE(BgL_auxz00_8249,BFALSE,BFALSE);} 
BgL_auxz00_8245 = 
BELONG_TO_LONG(BgL_tmpz00_8246); } 
{ /* Ieee/port.scm 1696 */
 obj_t BgL_tmpz00_8237;
if(
ELONGP(BgL_atimez00_3850))
{ /* Ieee/port.scm 1696 */
BgL_tmpz00_8237 = BgL_atimez00_3850
; }  else 
{ 
 obj_t BgL_auxz00_8240;
BgL_auxz00_8240 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(76185L), BGl_string2973z00zz__r4_ports_6_10_1z00, BGl_string2974z00zz__r4_ports_6_10_1z00, BgL_atimez00_3850); 
FAILURE(BgL_auxz00_8240,BFALSE,BFALSE);} 
BgL_auxz00_8236 = 
BELONG_TO_LONG(BgL_tmpz00_8237); } 
{ /* Ieee/port.scm 1696 */
 obj_t BgL_tmpz00_8228;
if(
STRINGP(BgL_filez00_3849))
{ /* Ieee/port.scm 1696 */
BgL_tmpz00_8228 = BgL_filez00_3849
; }  else 
{ 
 obj_t BgL_auxz00_8231;
BgL_auxz00_8231 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(76185L), BGl_string2973z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3849); 
FAILURE(BgL_auxz00_8231,BFALSE,BFALSE);} 
BgL_auxz00_8227 = 
BSTRING_TO_STRING(BgL_tmpz00_8228); } 
BgL_tmpz00_8226 = 
BGl_filezd2timeszd2setz12z12zz__r4_ports_6_10_1z00(BgL_auxz00_8227, BgL_auxz00_8236, BgL_auxz00_8245); } 
return 
BINT(BgL_tmpz00_8226);} } 

}



/* file-size */
BGL_EXPORTED_DEF long BGl_filezd2siza7ez75zz__r4_ports_6_10_1z00(char * BgL_filez00_200)
{
{ /* Ieee/port.scm 1701 */
BGL_TAIL return 
bgl_file_size(BgL_filez00_200);} 

}



/* &file-size */
obj_t BGl_z62filezd2siza7ez17zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3852, obj_t BgL_filez00_3853)
{
{ /* Ieee/port.scm 1701 */
{ /* Ieee/port.scm 1702 */
 long BgL_tmpz00_8257;
{ /* Ieee/port.scm 1702 */
 char * BgL_auxz00_8258;
{ /* Ieee/port.scm 1702 */
 obj_t BgL_tmpz00_8259;
if(
STRINGP(BgL_filez00_3853))
{ /* Ieee/port.scm 1702 */
BgL_tmpz00_8259 = BgL_filez00_3853
; }  else 
{ 
 obj_t BgL_auxz00_8262;
BgL_auxz00_8262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(76470L), BGl_string2975z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3853); 
FAILURE(BgL_auxz00_8262,BFALSE,BFALSE);} 
BgL_auxz00_8258 = 
BSTRING_TO_STRING(BgL_tmpz00_8259); } 
BgL_tmpz00_8257 = 
BGl_filezd2siza7ez75zz__r4_ports_6_10_1z00(BgL_auxz00_8258); } 
return 
make_belong(BgL_tmpz00_8257);} } 

}



/* file-uid */
BGL_EXPORTED_DEF int BGl_filezd2uidzd2zz__r4_ports_6_10_1z00(char * BgL_filez00_201)
{
{ /* Ieee/port.scm 1707 */
return 
(int)(
bgl_file_uid(BgL_filez00_201));} 

}



/* &file-uid */
obj_t BGl_z62filezd2uidzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3854, obj_t BgL_filez00_3855)
{
{ /* Ieee/port.scm 1707 */
{ /* Ieee/port.scm 1708 */
 int BgL_tmpz00_8271;
{ /* Ieee/port.scm 1708 */
 char * BgL_auxz00_8272;
{ /* Ieee/port.scm 1708 */
 obj_t BgL_tmpz00_8273;
if(
STRINGP(BgL_filez00_3855))
{ /* Ieee/port.scm 1708 */
BgL_tmpz00_8273 = BgL_filez00_3855
; }  else 
{ 
 obj_t BgL_auxz00_8276;
BgL_auxz00_8276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(76746L), BGl_string2976z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3855); 
FAILURE(BgL_auxz00_8276,BFALSE,BFALSE);} 
BgL_auxz00_8272 = 
BSTRING_TO_STRING(BgL_tmpz00_8273); } 
BgL_tmpz00_8271 = 
BGl_filezd2uidzd2zz__r4_ports_6_10_1z00(BgL_auxz00_8272); } 
return 
BINT(BgL_tmpz00_8271);} } 

}



/* file-gid */
BGL_EXPORTED_DEF int BGl_filezd2gidzd2zz__r4_ports_6_10_1z00(char * BgL_filez00_202)
{
{ /* Ieee/port.scm 1713 */
return 
(int)(
bgl_file_gid(BgL_filez00_202));} 

}



/* &file-gid */
obj_t BGl_z62filezd2gidzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3856, obj_t BgL_filez00_3857)
{
{ /* Ieee/port.scm 1713 */
{ /* Ieee/port.scm 1714 */
 int BgL_tmpz00_8285;
{ /* Ieee/port.scm 1714 */
 char * BgL_auxz00_8286;
{ /* Ieee/port.scm 1714 */
 obj_t BgL_tmpz00_8287;
if(
STRINGP(BgL_filez00_3857))
{ /* Ieee/port.scm 1714 */
BgL_tmpz00_8287 = BgL_filez00_3857
; }  else 
{ 
 obj_t BgL_auxz00_8290;
BgL_auxz00_8290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(77021L), BGl_string2977z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3857); 
FAILURE(BgL_auxz00_8290,BFALSE,BFALSE);} 
BgL_auxz00_8286 = 
BSTRING_TO_STRING(BgL_tmpz00_8287); } 
BgL_tmpz00_8285 = 
BGl_filezd2gidzd2zz__r4_ports_6_10_1z00(BgL_auxz00_8286); } 
return 
BINT(BgL_tmpz00_8285);} } 

}



/* file-mode */
BGL_EXPORTED_DEF int BGl_filezd2modezd2zz__r4_ports_6_10_1z00(char * BgL_filez00_203)
{
{ /* Ieee/port.scm 1719 */
return 
(int)(
bgl_file_mode(BgL_filez00_203));} 

}



/* &file-mode */
obj_t BGl_z62filezd2modezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3858, obj_t BgL_filez00_3859)
{
{ /* Ieee/port.scm 1719 */
{ /* Ieee/port.scm 1720 */
 int BgL_tmpz00_8299;
{ /* Ieee/port.scm 1720 */
 char * BgL_auxz00_8300;
{ /* Ieee/port.scm 1720 */
 obj_t BgL_tmpz00_8301;
if(
STRINGP(BgL_filez00_3859))
{ /* Ieee/port.scm 1720 */
BgL_tmpz00_8301 = BgL_filez00_3859
; }  else 
{ 
 obj_t BgL_auxz00_8304;
BgL_auxz00_8304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(77297L), BGl_string2978z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3859); 
FAILURE(BgL_auxz00_8304,BFALSE,BFALSE);} 
BgL_auxz00_8300 = 
BSTRING_TO_STRING(BgL_tmpz00_8301); } 
BgL_tmpz00_8299 = 
BGl_filezd2modezd2zz__r4_ports_6_10_1z00(BgL_auxz00_8300); } 
return 
BINT(BgL_tmpz00_8299);} } 

}



/* file-type */
BGL_EXPORTED_DEF obj_t BGl_filezd2typezd2zz__r4_ports_6_10_1z00(char * BgL_filez00_204)
{
{ /* Ieee/port.scm 1725 */
BGL_TAIL return 
bgl_file_type(BgL_filez00_204);} 

}



/* &file-type */
obj_t BGl_z62filezd2typezb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3860, obj_t BgL_filez00_3861)
{
{ /* Ieee/port.scm 1725 */
{ /* Ieee/port.scm 1726 */
 char * BgL_auxz00_8312;
{ /* Ieee/port.scm 1726 */
 obj_t BgL_tmpz00_8313;
if(
STRINGP(BgL_filez00_3861))
{ /* Ieee/port.scm 1726 */
BgL_tmpz00_8313 = BgL_filez00_3861
; }  else 
{ 
 obj_t BgL_auxz00_8316;
BgL_auxz00_8316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(77574L), BGl_string2979z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_filez00_3861); 
FAILURE(BgL_auxz00_8316,BFALSE,BFALSE);} 
BgL_auxz00_8312 = 
BSTRING_TO_STRING(BgL_tmpz00_8313); } 
return 
BGl_filezd2typezd2zz__r4_ports_6_10_1z00(BgL_auxz00_8312);} } 

}



/* make-symlink */
BGL_EXPORTED_DEF obj_t BGl_makezd2symlinkzd2zz__r4_ports_6_10_1z00(obj_t BgL_path1z00_205, obj_t BgL_path2z00_206)
{
{ /* Ieee/port.scm 1731 */
return 
BINT(
bgl_symlink(
BSTRING_TO_STRING(BgL_path1z00_205), 
BSTRING_TO_STRING(BgL_path2z00_206)));} 

}



/* &make-symlink */
obj_t BGl_z62makezd2symlinkzb0zz__r4_ports_6_10_1z00(obj_t BgL_envz00_3862, obj_t BgL_path1z00_3863, obj_t BgL_path2z00_3864)
{
{ /* Ieee/port.scm 1731 */
{ /* Ieee/port.scm 1732 */
 obj_t BgL_auxz00_8333; obj_t BgL_auxz00_8326;
if(
STRINGP(BgL_path2z00_3864))
{ /* Ieee/port.scm 1732 */
BgL_auxz00_8333 = BgL_path2z00_3864
; }  else 
{ 
 obj_t BgL_auxz00_8336;
BgL_auxz00_8336 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(77861L), BGl_string2980z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_path2z00_3864); 
FAILURE(BgL_auxz00_8336,BFALSE,BFALSE);} 
if(
STRINGP(BgL_path1z00_3863))
{ /* Ieee/port.scm 1732 */
BgL_auxz00_8326 = BgL_path1z00_3863
; }  else 
{ 
 obj_t BgL_auxz00_8329;
BgL_auxz00_8329 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(77861L), BGl_string2980z00zz__r4_ports_6_10_1z00, BGl_string2704z00zz__r4_ports_6_10_1z00, BgL_path1z00_3863); 
FAILURE(BgL_auxz00_8329,BFALSE,BFALSE);} 
return 
BGl_makezd2symlinkzd2zz__r4_ports_6_10_1z00(BgL_auxz00_8326, BgL_auxz00_8333);} } 

}



/* _select */
obj_t BGl__selectz00zz__r4_ports_6_10_1z00(obj_t BgL_env1309z00_212, obj_t BgL_opt1308z00_211)
{
{ /* Ieee/port.scm 1737 */
{ /* Ieee/port.scm 1737 */

{ /* Ieee/port.scm 1737 */
 obj_t BgL_exceptz00_4623;
BgL_exceptz00_4623 = BNIL; 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_readz00_4624;
BgL_readz00_4624 = BNIL; 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_timeoutz00_4625;
BgL_timeoutz00_4625 = 
BINT(0L); 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_writez00_4626;
BgL_writez00_4626 = BNIL; 
{ /* Ieee/port.scm 1737 */

{ 
 long BgL_iz00_4628;
BgL_iz00_4628 = 0L; 
BgL_check1312z00_4627:
if(
(BgL_iz00_4628==
VECTOR_LENGTH(BgL_opt1308z00_211)))
{ /* Ieee/port.scm 1737 */BNIL; }  else 
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3814z00_8345;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_arg1684z00_4629;
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3815z00_8346;
{ /* Ieee/port.scm 1737 */
 long BgL_tmpz00_8347;
BgL_tmpz00_8347 = 
VECTOR_LENGTH(BgL_opt1308z00_211); 
BgL_test3815z00_8346 = 
BOUND_CHECK(BgL_iz00_4628, BgL_tmpz00_8347); } 
if(BgL_test3815z00_8346)
{ /* Ieee/port.scm 1737 */
BgL_arg1684z00_4629 = 
VECTOR_REF(BgL_opt1308z00_211,BgL_iz00_4628); }  else 
{ 
 obj_t BgL_auxz00_8351;
BgL_auxz00_8351 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2828z00zz__r4_ports_6_10_1z00, BgL_opt1308z00_211, 
(int)(
VECTOR_LENGTH(BgL_opt1308z00_211)), 
(int)(BgL_iz00_4628)); 
FAILURE(BgL_auxz00_8351,BFALSE,BFALSE);} } 
BgL_test3814z00_8345 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1684z00_4629, BGl_list2981z00zz__r4_ports_6_10_1z00)); } 
if(BgL_test3814z00_8345)
{ 
 long BgL_iz00_8360;
BgL_iz00_8360 = 
(BgL_iz00_4628+2L); 
BgL_iz00_4628 = BgL_iz00_8360; 
goto BgL_check1312z00_4627;}  else 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_arg1681z00_4630;
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3816z00_8362;
{ /* Ieee/port.scm 1737 */
 long BgL_tmpz00_8363;
BgL_tmpz00_8363 = 
VECTOR_LENGTH(BgL_opt1308z00_211); 
BgL_test3816z00_8362 = 
BOUND_CHECK(BgL_iz00_4628, BgL_tmpz00_8363); } 
if(BgL_test3816z00_8362)
{ /* Ieee/port.scm 1737 */
BgL_arg1681z00_4630 = 
VECTOR_REF(BgL_opt1308z00_211,BgL_iz00_4628); }  else 
{ 
 obj_t BgL_auxz00_8367;
BgL_auxz00_8367 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2828z00zz__r4_ports_6_10_1z00, BgL_opt1308z00_211, 
(int)(
VECTOR_LENGTH(BgL_opt1308z00_211)), 
(int)(BgL_iz00_4628)); 
FAILURE(BgL_auxz00_8367,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol2989z00zz__r4_ports_6_10_1z00, BGl_string2991z00zz__r4_ports_6_10_1z00, BgL_arg1681z00_4630); } } } 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_index1314z00_4631;
BgL_index1314z00_4631 = 
BGl_search1311ze70ze7zz__r4_ports_6_10_1z00(
VECTOR_LENGTH(BgL_opt1308z00_211), BgL_opt1308z00_211, BGl_keyword2982z00zz__r4_ports_6_10_1z00, 0L); 
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3817z00_8377;
{ /* Ieee/port.scm 1737 */
 long BgL_n1z00_4632;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8378;
if(
INTEGERP(BgL_index1314z00_4631))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8378 = BgL_index1314z00_4631
; }  else 
{ 
 obj_t BgL_auxz00_8381;
BgL_auxz00_8381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1314z00_4631); 
FAILURE(BgL_auxz00_8381,BFALSE,BFALSE);} 
BgL_n1z00_4632 = 
(long)CINT(BgL_tmpz00_8378); } 
BgL_test3817z00_8377 = 
(BgL_n1z00_4632>=0L); } 
if(BgL_test3817z00_8377)
{ 
 long BgL_auxz00_8387;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8388;
if(
INTEGERP(BgL_index1314z00_4631))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8388 = BgL_index1314z00_4631
; }  else 
{ 
 obj_t BgL_auxz00_8391;
BgL_auxz00_8391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1314z00_4631); 
FAILURE(BgL_auxz00_8391,BFALSE,BFALSE);} 
BgL_auxz00_8387 = 
(long)CINT(BgL_tmpz00_8388); } 
BgL_exceptz00_4623 = 
VECTOR_REF(BgL_opt1308z00_211,BgL_auxz00_8387); }  else 
{ /* Ieee/port.scm 1737 */BFALSE; } } } 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_index1315z00_4633;
BgL_index1315z00_4633 = 
BGl_search1311ze70ze7zz__r4_ports_6_10_1z00(
VECTOR_LENGTH(BgL_opt1308z00_211), BgL_opt1308z00_211, BGl_keyword2984z00zz__r4_ports_6_10_1z00, 0L); 
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3820z00_8399;
{ /* Ieee/port.scm 1737 */
 long BgL_n1z00_4634;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8400;
if(
INTEGERP(BgL_index1315z00_4633))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8400 = BgL_index1315z00_4633
; }  else 
{ 
 obj_t BgL_auxz00_8403;
BgL_auxz00_8403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1315z00_4633); 
FAILURE(BgL_auxz00_8403,BFALSE,BFALSE);} 
BgL_n1z00_4634 = 
(long)CINT(BgL_tmpz00_8400); } 
BgL_test3820z00_8399 = 
(BgL_n1z00_4634>=0L); } 
if(BgL_test3820z00_8399)
{ 
 long BgL_auxz00_8409;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8410;
if(
INTEGERP(BgL_index1315z00_4633))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8410 = BgL_index1315z00_4633
; }  else 
{ 
 obj_t BgL_auxz00_8413;
BgL_auxz00_8413 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1315z00_4633); 
FAILURE(BgL_auxz00_8413,BFALSE,BFALSE);} 
BgL_auxz00_8409 = 
(long)CINT(BgL_tmpz00_8410); } 
BgL_readz00_4624 = 
VECTOR_REF(BgL_opt1308z00_211,BgL_auxz00_8409); }  else 
{ /* Ieee/port.scm 1737 */BFALSE; } } } 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_index1316z00_4635;
BgL_index1316z00_4635 = 
BGl_search1311ze70ze7zz__r4_ports_6_10_1z00(
VECTOR_LENGTH(BgL_opt1308z00_211), BgL_opt1308z00_211, BGl_keyword2986z00zz__r4_ports_6_10_1z00, 0L); 
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3823z00_8421;
{ /* Ieee/port.scm 1737 */
 long BgL_n1z00_4636;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8422;
if(
INTEGERP(BgL_index1316z00_4635))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8422 = BgL_index1316z00_4635
; }  else 
{ 
 obj_t BgL_auxz00_8425;
BgL_auxz00_8425 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1316z00_4635); 
FAILURE(BgL_auxz00_8425,BFALSE,BFALSE);} 
BgL_n1z00_4636 = 
(long)CINT(BgL_tmpz00_8422); } 
BgL_test3823z00_8421 = 
(BgL_n1z00_4636>=0L); } 
if(BgL_test3823z00_8421)
{ 
 long BgL_auxz00_8431;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8432;
if(
INTEGERP(BgL_index1316z00_4635))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8432 = BgL_index1316z00_4635
; }  else 
{ 
 obj_t BgL_auxz00_8435;
BgL_auxz00_8435 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1316z00_4635); 
FAILURE(BgL_auxz00_8435,BFALSE,BFALSE);} 
BgL_auxz00_8431 = 
(long)CINT(BgL_tmpz00_8432); } 
BgL_timeoutz00_4625 = 
VECTOR_REF(BgL_opt1308z00_211,BgL_auxz00_8431); }  else 
{ /* Ieee/port.scm 1737 */BFALSE; } } } 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_index1317z00_4637;
BgL_index1317z00_4637 = 
BGl_search1311ze70ze7zz__r4_ports_6_10_1z00(
VECTOR_LENGTH(BgL_opt1308z00_211), BgL_opt1308z00_211, BGl_keyword2987z00zz__r4_ports_6_10_1z00, 0L); 
{ /* Ieee/port.scm 1737 */
 bool_t BgL_test3826z00_8443;
{ /* Ieee/port.scm 1737 */
 long BgL_n1z00_4638;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8444;
if(
INTEGERP(BgL_index1317z00_4637))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8444 = BgL_index1317z00_4637
; }  else 
{ 
 obj_t BgL_auxz00_8447;
BgL_auxz00_8447 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1317z00_4637); 
FAILURE(BgL_auxz00_8447,BFALSE,BFALSE);} 
BgL_n1z00_4638 = 
(long)CINT(BgL_tmpz00_8444); } 
BgL_test3826z00_8443 = 
(BgL_n1z00_4638>=0L); } 
if(BgL_test3826z00_8443)
{ 
 long BgL_auxz00_8453;
{ /* Ieee/port.scm 1737 */
 obj_t BgL_tmpz00_8454;
if(
INTEGERP(BgL_index1317z00_4637))
{ /* Ieee/port.scm 1737 */
BgL_tmpz00_8454 = BgL_index1317z00_4637
; }  else 
{ 
 obj_t BgL_auxz00_8457;
BgL_auxz00_8457 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78115L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_index1317z00_4637); 
FAILURE(BgL_auxz00_8457,BFALSE,BFALSE);} 
BgL_auxz00_8453 = 
(long)CINT(BgL_tmpz00_8454); } 
BgL_writez00_4626 = 
VECTOR_REF(BgL_opt1308z00_211,BgL_auxz00_8453); }  else 
{ /* Ieee/port.scm 1737 */BFALSE; } } } 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_exceptz00_4639;
BgL_exceptz00_4639 = BgL_exceptz00_4623; 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_readz00_4640;
BgL_readz00_4640 = BgL_readz00_4624; 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_timeoutz00_4641;
BgL_timeoutz00_4641 = BgL_timeoutz00_4625; 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_writez00_4642;
BgL_writez00_4642 = BgL_writez00_4626; 
{ /* Ieee/port.scm 1738 */
 obj_t BgL_auxz00_8486; obj_t BgL_auxz00_8479; obj_t BgL_auxz00_8472; long BgL_tmpz00_8463;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_exceptz00_4639))
{ /* Ieee/port.scm 1738 */
BgL_auxz00_8486 = BgL_exceptz00_4639
; }  else 
{ 
 obj_t BgL_auxz00_8489;
BgL_auxz00_8489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78224L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_exceptz00_4639); 
FAILURE(BgL_auxz00_8489,BFALSE,BFALSE);} 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_writez00_4642))
{ /* Ieee/port.scm 1738 */
BgL_auxz00_8479 = BgL_writez00_4642
; }  else 
{ 
 obj_t BgL_auxz00_8482;
BgL_auxz00_8482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78218L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_writez00_4642); 
FAILURE(BgL_auxz00_8482,BFALSE,BFALSE);} 
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_readz00_4640))
{ /* Ieee/port.scm 1738 */
BgL_auxz00_8472 = BgL_readz00_4640
; }  else 
{ 
 obj_t BgL_auxz00_8475;
BgL_auxz00_8475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78213L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_readz00_4640); 
FAILURE(BgL_auxz00_8475,BFALSE,BFALSE);} 
{ /* Ieee/port.scm 1738 */
 obj_t BgL_tmpz00_8464;
if(
INTEGERP(BgL_timeoutz00_4641))
{ /* Ieee/port.scm 1738 */
BgL_tmpz00_8464 = BgL_timeoutz00_4641
; }  else 
{ 
 obj_t BgL_auxz00_8467;
BgL_auxz00_8467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78205L), BGl_string2992z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_timeoutz00_4641); 
FAILURE(BgL_auxz00_8467,BFALSE,BFALSE);} 
BgL_tmpz00_8463 = 
(long)CINT(BgL_tmpz00_8464); } 
return 
bgl_select(BgL_tmpz00_8463, BgL_auxz00_8472, BgL_auxz00_8479, BgL_auxz00_8486);} } } } } } } } } } } } 

}



/* search1311~0 */
obj_t BGl_search1311ze70ze7zz__r4_ports_6_10_1z00(long BgL_l1310z00_3867, obj_t BgL_opt1308z00_3866, obj_t BgL_k1z00_2946, long BgL_iz00_2947)
{
{ /* Ieee/port.scm 1737 */
BGl_search1311ze70ze7zz__r4_ports_6_10_1z00:
if(
(BgL_iz00_2947==BgL_l1310z00_3867))
{ /* Ieee/port.scm 1737 */
return 
BINT(-1L);}  else 
{ /* Ieee/port.scm 1737 */
if(
(BgL_iz00_2947==
(BgL_l1310z00_3867-1L)))
{ /* Ieee/port.scm 1737 */
return 
BGl_errorz00zz__errorz00(BGl_symbol2989z00zz__r4_ports_6_10_1z00, BGl_string2993z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1308z00_3866)));}  else 
{ /* Ieee/port.scm 1737 */
 obj_t BgL_vz00_2952;
BgL_vz00_2952 = 
VECTOR_REF(BgL_opt1308z00_3866,BgL_iz00_2947); 
if(
(BgL_vz00_2952==BgL_k1z00_2946))
{ /* Ieee/port.scm 1737 */
return 
BINT(
(BgL_iz00_2947+1L));}  else 
{ 
 long BgL_iz00_8508;
BgL_iz00_8508 = 
(BgL_iz00_2947+2L); 
BgL_iz00_2947 = BgL_iz00_8508; 
goto BGl_search1311ze70ze7zz__r4_ports_6_10_1z00;} } } } 

}



/* select */
BGL_EXPORTED_DEF obj_t BGl_selectz00zz__r4_ports_6_10_1z00(obj_t BgL_exceptz00_207, obj_t BgL_readz00_208, obj_t BgL_timeoutz00_209, obj_t BgL_writez00_210)
{
{ /* Ieee/port.scm 1737 */
{ /* Ieee/port.scm 1738 */
 obj_t BgL_auxz00_8537; obj_t BgL_auxz00_8528; obj_t BgL_auxz00_8519; long BgL_tmpz00_8510;
{ /* Ieee/port.scm 1738 */
 bool_t BgL_test3841z00_8538;
if(
PAIRP(BgL_exceptz00_207))
{ /* Ieee/port.scm 1738 */
BgL_test3841z00_8538 = ((bool_t)1)
; }  else 
{ /* Ieee/port.scm 1738 */
BgL_test3841z00_8538 = 
NULLP(BgL_exceptz00_207)
; } 
if(BgL_test3841z00_8538)
{ /* Ieee/port.scm 1738 */
BgL_auxz00_8537 = BgL_exceptz00_207
; }  else 
{ 
 obj_t BgL_auxz00_8542;
BgL_auxz00_8542 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78224L), BGl_string2990z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_exceptz00_207); 
FAILURE(BgL_auxz00_8542,BFALSE,BFALSE);} } 
{ /* Ieee/port.scm 1738 */
 bool_t BgL_test3839z00_8529;
if(
PAIRP(BgL_writez00_210))
{ /* Ieee/port.scm 1738 */
BgL_test3839z00_8529 = ((bool_t)1)
; }  else 
{ /* Ieee/port.scm 1738 */
BgL_test3839z00_8529 = 
NULLP(BgL_writez00_210)
; } 
if(BgL_test3839z00_8529)
{ /* Ieee/port.scm 1738 */
BgL_auxz00_8528 = BgL_writez00_210
; }  else 
{ 
 obj_t BgL_auxz00_8533;
BgL_auxz00_8533 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78218L), BGl_string2990z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_writez00_210); 
FAILURE(BgL_auxz00_8533,BFALSE,BFALSE);} } 
{ /* Ieee/port.scm 1738 */
 bool_t BgL_test3837z00_8520;
if(
PAIRP(BgL_readz00_208))
{ /* Ieee/port.scm 1738 */
BgL_test3837z00_8520 = ((bool_t)1)
; }  else 
{ /* Ieee/port.scm 1738 */
BgL_test3837z00_8520 = 
NULLP(BgL_readz00_208)
; } 
if(BgL_test3837z00_8520)
{ /* Ieee/port.scm 1738 */
BgL_auxz00_8519 = BgL_readz00_208
; }  else 
{ 
 obj_t BgL_auxz00_8524;
BgL_auxz00_8524 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78213L), BGl_string2990z00zz__r4_ports_6_10_1z00, BGl_string2795z00zz__r4_ports_6_10_1z00, BgL_readz00_208); 
FAILURE(BgL_auxz00_8524,BFALSE,BFALSE);} } 
{ /* Ieee/port.scm 1738 */
 obj_t BgL_tmpz00_8511;
if(
INTEGERP(BgL_timeoutz00_209))
{ /* Ieee/port.scm 1738 */
BgL_tmpz00_8511 = BgL_timeoutz00_209
; }  else 
{ 
 obj_t BgL_auxz00_8514;
BgL_auxz00_8514 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78205L), BGl_string2990z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_timeoutz00_209); 
FAILURE(BgL_auxz00_8514,BFALSE,BFALSE);} 
BgL_tmpz00_8510 = 
(long)CINT(BgL_tmpz00_8511); } 
return 
bgl_select(BgL_tmpz00_8510, BgL_auxz00_8519, BgL_auxz00_8528, BgL_auxz00_8537);} } 

}



/* _open-pipes */
obj_t BGl__openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t BgL_env1319z00_215, obj_t BgL_opt1318z00_214)
{
{ /* Ieee/port.scm 1743 */
{ /* Ieee/port.scm 1743 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1318z00_214)) { case 0L : 

{ /* Ieee/port.scm 1743 */

return 
bgl_open_pipes(BFALSE);} break;case 1L : 

{ /* Ieee/port.scm 1743 */
 obj_t BgL_namez00_4647;
BgL_namez00_4647 = 
VECTOR_REF(BgL_opt1318z00_214,0L); 
{ /* Ieee/port.scm 1743 */

return 
bgl_open_pipes(BgL_namez00_4647);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2994z00zz__r4_ports_6_10_1z00, BGl_string2887z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1318z00_214)));} } } } 

}



/* open-pipes */
BGL_EXPORTED_DEF obj_t BGl_openzd2pipeszd2zz__r4_ports_6_10_1z00(obj_t BgL_namez00_213)
{
{ /* Ieee/port.scm 1743 */
BGL_TAIL return 
bgl_open_pipes(BgL_namez00_213);} 

}



/* _lockf */
obj_t BGl__lockfz00zz__r4_ports_6_10_1z00(obj_t BgL_env1323z00_220, obj_t BgL_opt1322z00_219)
{
{ /* Ieee/port.scm 1749 */
{ /* Ieee/port.scm 1749 */
 obj_t BgL_g1324z00_2042; obj_t BgL_g1325z00_2043;
BgL_g1324z00_2042 = 
VECTOR_REF(BgL_opt1322z00_219,0L); 
BgL_g1325z00_2043 = 
VECTOR_REF(BgL_opt1322z00_219,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1322z00_219)) { case 2L : 

{ /* Ieee/port.scm 1749 */

{ /* Ieee/port.scm 1749 */
 bool_t BgL_tmpz00_8558;
{ /* Ieee/port.scm 1749 */
 obj_t BgL_auxz00_8566; obj_t BgL_auxz00_8559;
if(
SYMBOLP(BgL_g1325z00_2043))
{ /* Ieee/port.scm 1749 */
BgL_auxz00_8566 = BgL_g1325z00_2043
; }  else 
{ 
 obj_t BgL_auxz00_8569;
BgL_auxz00_8569 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78746L), BGl_string2999z00zz__r4_ports_6_10_1z00, BGl_string3000z00zz__r4_ports_6_10_1z00, BgL_g1325z00_2043); 
FAILURE(BgL_auxz00_8569,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_g1324z00_2042))
{ /* Ieee/port.scm 1749 */
BgL_auxz00_8559 = BgL_g1324z00_2042
; }  else 
{ 
 obj_t BgL_auxz00_8562;
BgL_auxz00_8562 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78746L), BGl_string2999z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_g1324z00_2042); 
FAILURE(BgL_auxz00_8562,BFALSE,BFALSE);} 
BgL_tmpz00_8558 = 
BGl_lockfz00zz__r4_ports_6_10_1z00(BgL_auxz00_8559, BgL_auxz00_8566, 
BINT(0L)); } 
return 
BBOOL(BgL_tmpz00_8558);} } break;case 3L : 

{ /* Ieee/port.scm 1749 */
 obj_t BgL_lenz00_2047;
BgL_lenz00_2047 = 
VECTOR_REF(BgL_opt1322z00_219,2L); 
{ /* Ieee/port.scm 1749 */

{ /* Ieee/port.scm 1749 */
 bool_t BgL_tmpz00_8577;
{ /* Ieee/port.scm 1749 */
 obj_t BgL_auxz00_8585; obj_t BgL_auxz00_8578;
if(
SYMBOLP(BgL_g1325z00_2043))
{ /* Ieee/port.scm 1749 */
BgL_auxz00_8585 = BgL_g1325z00_2043
; }  else 
{ 
 obj_t BgL_auxz00_8588;
BgL_auxz00_8588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78746L), BGl_string2999z00zz__r4_ports_6_10_1z00, BGl_string3000z00zz__r4_ports_6_10_1z00, BgL_g1325z00_2043); 
FAILURE(BgL_auxz00_8588,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_g1324z00_2042))
{ /* Ieee/port.scm 1749 */
BgL_auxz00_8578 = BgL_g1324z00_2042
; }  else 
{ 
 obj_t BgL_auxz00_8581;
BgL_auxz00_8581 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78746L), BGl_string2999z00zz__r4_ports_6_10_1z00, BGl_string2727z00zz__r4_ports_6_10_1z00, BgL_g1324z00_2042); 
FAILURE(BgL_auxz00_8581,BFALSE,BFALSE);} 
BgL_tmpz00_8577 = 
BGl_lockfz00zz__r4_ports_6_10_1z00(BgL_auxz00_8578, BgL_auxz00_8585, BgL_lenz00_2047); } 
return 
BBOOL(BgL_tmpz00_8577);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2996z00zz__r4_ports_6_10_1z00, BGl_string2998z00zz__r4_ports_6_10_1z00, 
BINT(
VECTOR_LENGTH(BgL_opt1322z00_219)));} } } } 

}



/* lockf */
BGL_EXPORTED_DEF bool_t BGl_lockfz00zz__r4_ports_6_10_1z00(obj_t BgL_portz00_216, obj_t BgL_cmdz00_217, obj_t BgL_lenz00_218)
{
{ /* Ieee/port.scm 1749 */
if(
(BgL_cmdz00_217==BGl_symbol3001z00zz__r4_ports_6_10_1z00))
{ /* Ieee/port.scm 1753 */
 long BgL_tmpz00_8601;
{ /* Ieee/port.scm 1753 */
 obj_t BgL_tmpz00_8602;
if(
INTEGERP(BgL_lenz00_218))
{ /* Ieee/port.scm 1753 */
BgL_tmpz00_8602 = BgL_lenz00_218
; }  else 
{ 
 obj_t BgL_auxz00_8605;
BgL_auxz00_8605 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78871L), BGl_string2997z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_lenz00_218); 
FAILURE(BgL_auxz00_8605,BFALSE,BFALSE);} 
BgL_tmpz00_8601 = 
(long)CINT(BgL_tmpz00_8602); } 
return 
bgl_lockf(BgL_portz00_216, F_LOCK, BgL_tmpz00_8601);}  else 
{ /* Ieee/port.scm 1752 */
if(
(BgL_cmdz00_217==BGl_symbol3003z00zz__r4_ports_6_10_1z00))
{ /* Ieee/port.scm 1754 */
 long BgL_tmpz00_8613;
{ /* Ieee/port.scm 1754 */
 obj_t BgL_tmpz00_8614;
if(
INTEGERP(BgL_lenz00_218))
{ /* Ieee/port.scm 1754 */
BgL_tmpz00_8614 = BgL_lenz00_218
; }  else 
{ 
 obj_t BgL_auxz00_8617;
BgL_auxz00_8617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78911L), BGl_string2997z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_lenz00_218); 
FAILURE(BgL_auxz00_8617,BFALSE,BFALSE);} 
BgL_tmpz00_8613 = 
(long)CINT(BgL_tmpz00_8614); } 
return 
bgl_lockf(BgL_portz00_216, F_TLOCK, BgL_tmpz00_8613);}  else 
{ /* Ieee/port.scm 1752 */
if(
(BgL_cmdz00_217==BGl_symbol3005z00zz__r4_ports_6_10_1z00))
{ /* Ieee/port.scm 1755 */
 long BgL_tmpz00_8625;
{ /* Ieee/port.scm 1755 */
 obj_t BgL_tmpz00_8626;
if(
INTEGERP(BgL_lenz00_218))
{ /* Ieee/port.scm 1755 */
BgL_tmpz00_8626 = BgL_lenz00_218
; }  else 
{ 
 obj_t BgL_auxz00_8629;
BgL_auxz00_8629 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78951L), BGl_string2997z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_lenz00_218); 
FAILURE(BgL_auxz00_8629,BFALSE,BFALSE);} 
BgL_tmpz00_8625 = 
(long)CINT(BgL_tmpz00_8626); } 
return 
bgl_lockf(BgL_portz00_216, F_ULOCK, BgL_tmpz00_8625);}  else 
{ /* Ieee/port.scm 1752 */
if(
(BgL_cmdz00_217==BGl_symbol3007z00zz__r4_ports_6_10_1z00))
{ /* Ieee/port.scm 1756 */
 long BgL_tmpz00_8637;
{ /* Ieee/port.scm 1756 */
 obj_t BgL_tmpz00_8638;
if(
INTEGERP(BgL_lenz00_218))
{ /* Ieee/port.scm 1756 */
BgL_tmpz00_8638 = BgL_lenz00_218
; }  else 
{ 
 obj_t BgL_auxz00_8641;
BgL_auxz00_8641 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2702z00zz__r4_ports_6_10_1z00, 
BINT(78989L), BGl_string2997z00zz__r4_ports_6_10_1z00, BGl_string2801z00zz__r4_ports_6_10_1z00, BgL_lenz00_218); 
FAILURE(BgL_auxz00_8641,BFALSE,BFALSE);} 
BgL_tmpz00_8637 = 
(long)CINT(BgL_tmpz00_8638); } 
return 
bgl_lockf(BgL_portz00_216, F_TEST, BgL_tmpz00_8637);}  else 
{ /* Ieee/port.scm 1752 */
return 
CBOOL(
BGl_errorz00zz__errorz00(BGl_string2997z00zz__r4_ports_6_10_1z00, BGl_string3009z00zz__r4_ports_6_10_1z00, BgL_cmdz00_217));} } } } } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_ports_6_10_1z00(void)
{
{ /* Ieee/port.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__gunza7ipza7(224363699L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__urlz00(337061926L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
BGl_modulezd2initializa7ationz75zz__httpz00(354980671L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00)); 
return 
BGl_modulezd2initializa7ationz75zz__ftpz00(102328204L, 
BSTRING_TO_STRING(BGl_string3010z00zz__r4_ports_6_10_1z00));} 

}

#ifdef __cplusplus
}
#endif
