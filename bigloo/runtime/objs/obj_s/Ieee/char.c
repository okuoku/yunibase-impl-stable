/*===========================================================================*/
/*   (Ieee/char.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/char.scm -indent -o objs/obj_s/Ieee/char.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_CHARACTERS_6_6_TYPE_DEFINITIONS
#define BGL___R4_CHARACTERS_6_6_TYPE_DEFINITIONS
#endif // BGL___R4_CHARACTERS_6_6_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62charzd2lowerzd2casezf3z91zz__r4_characters_6_6z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_charzd2ze3integerz31zz__r4_characters_6_6z00(unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charzd2numericzf3z21zz__r4_characters_6_6z00(unsigned char);
BGL_EXPORTED_DECL unsigned char BGl_charzd2andzd2zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL unsigned char BGl_charzd2downcasezd2zz__r4_characters_6_6z00(unsigned char);
BGL_EXPORTED_DECL unsigned char BGl_integerzd2ze3charzd2urze3zz__r4_characters_6_6z00(long);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00 = BUNSPEC;
static obj_t BGl_z62charzd2cizc3zd3zf3z53zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL unsigned char BGl_charzd2upcasezd2zz__r4_characters_6_6z00(unsigned char);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62charzd2alphabeticzf3z43zz__r4_characters_6_6z00(obj_t, obj_t);
static obj_t BGl_z62charzc3zf3z52zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2notzb0zz__r4_characters_6_6z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2cize3zd3zf3z11zz__r4_characters_6_6z00(unsigned char, unsigned char);
static obj_t BGl_z62charzd3zf3z42zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2cizc3zf3z80zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2ze3integerz53zz__r4_characters_6_6z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2upperzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char);
static obj_t BGl_z62charze3zf3z72zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2numericzf3z43zz__r4_characters_6_6z00(obj_t, obj_t);
static obj_t BGl_z62charzd2downcasezb0zz__r4_characters_6_6z00(obj_t, obj_t);
static obj_t BGl_z62charzd2cize3zd3zf3z73zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
static obj_t BGl_z62charzd2cizd3zf3z90zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_characters_6_6z00(void);
static obj_t BGl_gczd2rootszd2initz00zz__r4_characters_6_6z00(void);
BGL_EXPORTED_DECL bool_t BGl_charzc3zd3zf3ze3zz__r4_characters_6_6z00(unsigned char, unsigned char);
static obj_t BGl_z62charzd2cize3zf3za0zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzc3zd3zf3z81zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2andzb0zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62integerzd2ze3charzd2urz81zz__r4_characters_6_6z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2whitespacezf3z21zz__r4_characters_6_6z00(unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charzf3zf3zz__r4_characters_6_6z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2cizc3zf3ze2zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charzc3zf3z30zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charzd2cizd3zf3zf2zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charzd3zf3z20zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charzd2lowerzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char);
static obj_t BGl_z62integerzd2ze3charz53zz__r4_characters_6_6z00(obj_t, obj_t);
static obj_t BGl_z62charzd2upcasezb0zz__r4_characters_6_6z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2cize3zf3zc2zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charze3zd3zf3zc3zz__r4_characters_6_6z00(unsigned char, unsigned char);
BGL_EXPORTED_DECL bool_t BGl_charze3zf3z10zz__r4_characters_6_6z00(unsigned char, unsigned char);
static obj_t BGl_z62charze3zd3zf3za1zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2upperzd2casezf3z91zz__r4_characters_6_6z00(obj_t, obj_t);
static obj_t BGl_z62charzf3z91zz__r4_characters_6_6z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2alphabeticzf3z21zz__r4_characters_6_6z00(unsigned char);
BGL_EXPORTED_DECL unsigned char BGl_charzd2notzd2zz__r4_characters_6_6z00(unsigned char);
BGL_EXPORTED_DECL unsigned char BGl_charzd2orzd2zz__r4_characters_6_6z00(unsigned char, unsigned char);
static obj_t BGl_z62charzd2orzb0zz__r4_characters_6_6z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62charzd2whitespacezf3z43zz__r4_characters_6_6z00(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_charzd2cizc3zd3zf3z31zz__r4_characters_6_6z00(unsigned char, unsigned char);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_integerzd2ze3charzd2envze3zz__r4_characters_6_6z00, BgL_bgl_za762integerza7d2za7e31521za7, BGl_z62integerzd2ze3charz53zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd3zf3zd2envzf2zz__r4_characters_6_6z00, BgL_bgl_za762charza7d3za7f3za7421522z00, BGl_z62charzd3zf3z42zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2upcasezd2envz00zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2upcase1523z00, BGl_z62charzd2upcasezb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2whitespacezf3zd2envzf3zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2whites1524z00, BGl_z62charzd2whitespacezf3z43zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1500z00zz__r4_characters_6_6z00, BgL_bgl_string1500za700za7za7_1525za7, "&char-ci<?", 10 );
DEFINE_STRING( BGl_string1501z00zz__r4_characters_6_6z00, BgL_bgl_string1501za700za7za7_1526za7, "&char-ci>?", 10 );
DEFINE_STRING( BGl_string1502z00zz__r4_characters_6_6z00, BgL_bgl_string1502za700za7za7_1527za7, "&char-ci<=?", 11 );
DEFINE_STRING( BGl_string1503z00zz__r4_characters_6_6z00, BgL_bgl_string1503za700za7za7_1528za7, "&char-ci>=?", 11 );
DEFINE_STRING( BGl_string1504z00zz__r4_characters_6_6z00, BgL_bgl_string1504za700za7za7_1529za7, "&char-alphabetic?", 17 );
DEFINE_STRING( BGl_string1505z00zz__r4_characters_6_6z00, BgL_bgl_string1505za700za7za7_1530za7, "&char-numeric?", 14 );
DEFINE_STRING( BGl_string1506z00zz__r4_characters_6_6z00, BgL_bgl_string1506za700za7za7_1531za7, "&char-whitespace?", 17 );
DEFINE_STRING( BGl_string1507z00zz__r4_characters_6_6z00, BgL_bgl_string1507za700za7za7_1532za7, "&char-upper-case?", 17 );
DEFINE_STRING( BGl_string1508z00zz__r4_characters_6_6z00, BgL_bgl_string1508za700za7za7_1533za7, "&char-lower-case?", 17 );
DEFINE_STRING( BGl_string1509z00zz__r4_characters_6_6z00, BgL_bgl_string1509za700za7za7_1534za7, "&char->integer", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2numericzf3zd2envzf3zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2numeri1535z00, BGl_z62charzd2numericzf3z43zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1510z00zz__r4_characters_6_6z00, BgL_bgl_string1510za700za7za7_1536za7, "integer->char", 13 );
DEFINE_STRING( BGl_string1511z00zz__r4_characters_6_6z00, BgL_bgl_string1511za700za7za7_1537za7, "integer out of range", 20 );
DEFINE_STRING( BGl_string1512z00zz__r4_characters_6_6z00, BgL_bgl_string1512za700za7za7_1538za7, "&integer->char", 14 );
DEFINE_STRING( BGl_string1513z00zz__r4_characters_6_6z00, BgL_bgl_string1513za700za7za7_1539za7, "bint", 4 );
DEFINE_STRING( BGl_string1514z00zz__r4_characters_6_6z00, BgL_bgl_string1514za700za7za7_1540za7, "&integer->char-ur", 17 );
DEFINE_STRING( BGl_string1515z00zz__r4_characters_6_6z00, BgL_bgl_string1515za700za7za7_1541za7, "&char-upcase", 12 );
DEFINE_STRING( BGl_string1516z00zz__r4_characters_6_6z00, BgL_bgl_string1516za700za7za7_1542za7, "&char-downcase", 14 );
DEFINE_STRING( BGl_string1517z00zz__r4_characters_6_6z00, BgL_bgl_string1517za700za7za7_1543za7, "&char-or", 8 );
DEFINE_STRING( BGl_string1518z00zz__r4_characters_6_6z00, BgL_bgl_string1518za700za7za7_1544za7, "&char-and", 9 );
DEFINE_STRING( BGl_string1519z00zz__r4_characters_6_6z00, BgL_bgl_string1519za700za7za7_1545za7, "&char-not", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2cizd3zf3zd2envz20zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2ciza7d3za71546z00, BGl_z62charzd2cizd3zf3z90zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1520z00zz__r4_characters_6_6z00, BgL_bgl_string1520za700za7za7_1547za7, "__r4_characters_6_6", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charze3zf3zd2envzc2zz__r4_characters_6_6z00, BgL_bgl_za762charza7e3za7f3za7721548z00, BGl_z62charze3zf3z72zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2cize3zd3zf3zd2envzc3zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2ciza7e3za71549z00, BGl_z62charzd2cize3zd3zf3z73zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2alphabeticzf3zd2envzf3zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2alphab1550z00, BGl_z62charzd2alphabeticzf3z43zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2andzd2envz00zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2andza7b01551za7, BGl_z62charzd2andzb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2lowerzd2casezf3zd2envz21zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2lowerza71552za7, BGl_z62charzd2lowerzd2casezf3z91zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2cize3zf3zd2envz10zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2ciza7e3za71553z00, BGl_z62charzd2cize3zf3za0zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charze3zd3zf3zd2envz11zz__r4_characters_6_6z00, BgL_bgl_za762charza7e3za7d3za7f31554z00, BGl_z62charze3zd3zf3za1zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2cizc3zd3zf3zd2envze3zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2ciza7c3za71555z00, BGl_z62charzd2cizc3zd3zf3z53zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1492z00zz__r4_characters_6_6z00, BgL_bgl_string1492za700za7za7_1556za7, "/tmp/bigloo/runtime/Ieee/char.scm", 33 );
DEFINE_STRING( BGl_string1493z00zz__r4_characters_6_6z00, BgL_bgl_string1493za700za7za7_1557za7, "&char=?", 7 );
DEFINE_STRING( BGl_string1494z00zz__r4_characters_6_6z00, BgL_bgl_string1494za700za7za7_1558za7, "bchar", 5 );
DEFINE_STRING( BGl_string1495z00zz__r4_characters_6_6z00, BgL_bgl_string1495za700za7za7_1559za7, "&char<?", 7 );
DEFINE_STRING( BGl_string1496z00zz__r4_characters_6_6z00, BgL_bgl_string1496za700za7za7_1560za7, "&char>?", 7 );
DEFINE_STRING( BGl_string1497z00zz__r4_characters_6_6z00, BgL_bgl_string1497za700za7za7_1561za7, "&char<=?", 8 );
DEFINE_STRING( BGl_string1498z00zz__r4_characters_6_6z00, BgL_bgl_string1498za700za7za7_1562za7, "&char>=?", 8 );
DEFINE_STRING( BGl_string1499z00zz__r4_characters_6_6z00, BgL_bgl_string1499za700za7za7_1563za7, "&char-ci=?", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzf3zd2envz21zz__r4_characters_6_6z00, BgL_bgl_za762charza7f3za791za7za7_1564za7, BGl_z62charzf3z91zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2ze3integerzd2envze3zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2za7e3int1565za7, BGl_z62charzd2ze3integerz53zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzc3zf3zd2envze2zz__r4_characters_6_6z00, BgL_bgl_za762charza7c3za7f3za7521566z00, BGl_z62charzc3zf3z52zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2notzd2envz00zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2notza7b01567za7, BGl_z62charzd2notzb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2downcasezd2envz00zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2downca1568z00, BGl_z62charzd2downcasezb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_integerzd2ze3charzd2urzd2envz31zz__r4_characters_6_6z00, BgL_bgl_za762integerza7d2za7e31569za7, BGl_z62integerzd2ze3charzd2urz81zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2orzd2envz00zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2orza7b0za71570z00, BGl_z62charzd2orzb0zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzc3zd3zf3zd2envz31zz__r4_characters_6_6z00, BgL_bgl_za762charza7c3za7d3za7f31571z00, BGl_z62charzc3zd3zf3z81zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2cizc3zf3zd2envz30zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2ciza7c3za71572z00, BGl_z62charzd2cizc3zf3z80zz__r4_characters_6_6z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2upperzd2casezf3zd2envz21zz__r4_characters_6_6z00, BgL_bgl_za762charza7d2upperza71573za7, BGl_z62charzd2upperzd2casezf3z91zz__r4_characters_6_6z00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long BgL_checksumz00_1213, char * BgL_fromz00_1214)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_characters_6_6z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_characters_6_6z00(); 
return 
BGl_importedzd2moduleszd2initz00zz__r4_characters_6_6z00();}  else 
{ 
return BUNSPEC;} } 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_characters_6_6z00(void)
{
{ /* Ieee/char.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* char? */
BGL_EXPORTED_DEF bool_t BGl_charzf3zf3zz__r4_characters_6_6z00(obj_t BgL_objz00_3)
{
{ /* Ieee/char.scm 131 */
return 
CHARP(BgL_objz00_3);} 

}



/* &char? */
obj_t BGl_z62charzf3z91zz__r4_characters_6_6z00(obj_t BgL_envz00_1083, obj_t BgL_objz00_1084)
{
{ /* Ieee/char.scm 131 */
return 
BBOOL(
BGl_charzf3zf3zz__r4_characters_6_6z00(BgL_objz00_1084));} 

}



/* char=? */
BGL_EXPORTED_DEF bool_t BGl_charzd3zf3z20zz__r4_characters_6_6z00(unsigned char BgL_char1z00_4, unsigned char BgL_char2z00_5)
{
{ /* Ieee/char.scm 137 */
return 
(BgL_char1z00_4==BgL_char2z00_5);} 

}



/* &char=? */
obj_t BGl_z62charzd3zf3z42zz__r4_characters_6_6z00(obj_t BgL_envz00_1085, obj_t BgL_char1z00_1086, obj_t BgL_char2z00_1087)
{
{ /* Ieee/char.scm 137 */
{ /* Ieee/char.scm 138 */
 bool_t BgL_tmpz00_1225;
{ /* Ieee/char.scm 138 */
 unsigned char BgL_auxz00_1235; unsigned char BgL_auxz00_1226;
{ /* Ieee/char.scm 138 */
 obj_t BgL_tmpz00_1236;
if(
CHARP(BgL_char2z00_1087))
{ /* Ieee/char.scm 138 */
BgL_tmpz00_1236 = BgL_char2z00_1087
; }  else 
{ 
 obj_t BgL_auxz00_1239;
BgL_auxz00_1239 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(6542L), BGl_string1493z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1087); 
FAILURE(BgL_auxz00_1239,BFALSE,BFALSE);} 
BgL_auxz00_1235 = 
CCHAR(BgL_tmpz00_1236); } 
{ /* Ieee/char.scm 138 */
 obj_t BgL_tmpz00_1227;
if(
CHARP(BgL_char1z00_1086))
{ /* Ieee/char.scm 138 */
BgL_tmpz00_1227 = BgL_char1z00_1086
; }  else 
{ 
 obj_t BgL_auxz00_1230;
BgL_auxz00_1230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(6542L), BGl_string1493z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1086); 
FAILURE(BgL_auxz00_1230,BFALSE,BFALSE);} 
BgL_auxz00_1226 = 
CCHAR(BgL_tmpz00_1227); } 
BgL_tmpz00_1225 = 
BGl_charzd3zf3z20zz__r4_characters_6_6z00(BgL_auxz00_1226, BgL_auxz00_1235); } 
return 
BBOOL(BgL_tmpz00_1225);} } 

}



/* char<? */
BGL_EXPORTED_DEF bool_t BGl_charzc3zf3z30zz__r4_characters_6_6z00(unsigned char BgL_char1z00_6, unsigned char BgL_char2z00_7)
{
{ /* Ieee/char.scm 143 */
return 
(BgL_char1z00_6<BgL_char2z00_7);} 

}



/* &char<? */
obj_t BGl_z62charzc3zf3z52zz__r4_characters_6_6z00(obj_t BgL_envz00_1088, obj_t BgL_char1z00_1089, obj_t BgL_char2z00_1090)
{
{ /* Ieee/char.scm 143 */
{ /* Ieee/char.scm 144 */
 bool_t BgL_tmpz00_1247;
{ /* Ieee/char.scm 144 */
 unsigned char BgL_auxz00_1257; unsigned char BgL_auxz00_1248;
{ /* Ieee/char.scm 144 */
 obj_t BgL_tmpz00_1258;
if(
CHARP(BgL_char2z00_1090))
{ /* Ieee/char.scm 144 */
BgL_tmpz00_1258 = BgL_char2z00_1090
; }  else 
{ 
 obj_t BgL_auxz00_1261;
BgL_auxz00_1261 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(6828L), BGl_string1495z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1090); 
FAILURE(BgL_auxz00_1261,BFALSE,BFALSE);} 
BgL_auxz00_1257 = 
CCHAR(BgL_tmpz00_1258); } 
{ /* Ieee/char.scm 144 */
 obj_t BgL_tmpz00_1249;
if(
CHARP(BgL_char1z00_1089))
{ /* Ieee/char.scm 144 */
BgL_tmpz00_1249 = BgL_char1z00_1089
; }  else 
{ 
 obj_t BgL_auxz00_1252;
BgL_auxz00_1252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(6828L), BGl_string1495z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1089); 
FAILURE(BgL_auxz00_1252,BFALSE,BFALSE);} 
BgL_auxz00_1248 = 
CCHAR(BgL_tmpz00_1249); } 
BgL_tmpz00_1247 = 
BGl_charzc3zf3z30zz__r4_characters_6_6z00(BgL_auxz00_1248, BgL_auxz00_1257); } 
return 
BBOOL(BgL_tmpz00_1247);} } 

}



/* char>? */
BGL_EXPORTED_DEF bool_t BGl_charze3zf3z10zz__r4_characters_6_6z00(unsigned char BgL_char1z00_8, unsigned char BgL_char2z00_9)
{
{ /* Ieee/char.scm 149 */
return 
(BgL_char1z00_8>BgL_char2z00_9);} 

}



/* &char>? */
obj_t BGl_z62charze3zf3z72zz__r4_characters_6_6z00(obj_t BgL_envz00_1091, obj_t BgL_char1z00_1092, obj_t BgL_char2z00_1093)
{
{ /* Ieee/char.scm 149 */
{ /* Ieee/char.scm 150 */
 bool_t BgL_tmpz00_1269;
{ /* Ieee/char.scm 150 */
 unsigned char BgL_auxz00_1279; unsigned char BgL_auxz00_1270;
{ /* Ieee/char.scm 150 */
 obj_t BgL_tmpz00_1280;
if(
CHARP(BgL_char2z00_1093))
{ /* Ieee/char.scm 150 */
BgL_tmpz00_1280 = BgL_char2z00_1093
; }  else 
{ 
 obj_t BgL_auxz00_1283;
BgL_auxz00_1283 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7115L), BGl_string1496z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1093); 
FAILURE(BgL_auxz00_1283,BFALSE,BFALSE);} 
BgL_auxz00_1279 = 
CCHAR(BgL_tmpz00_1280); } 
{ /* Ieee/char.scm 150 */
 obj_t BgL_tmpz00_1271;
if(
CHARP(BgL_char1z00_1092))
{ /* Ieee/char.scm 150 */
BgL_tmpz00_1271 = BgL_char1z00_1092
; }  else 
{ 
 obj_t BgL_auxz00_1274;
BgL_auxz00_1274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7115L), BGl_string1496z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1092); 
FAILURE(BgL_auxz00_1274,BFALSE,BFALSE);} 
BgL_auxz00_1270 = 
CCHAR(BgL_tmpz00_1271); } 
BgL_tmpz00_1269 = 
BGl_charze3zf3z10zz__r4_characters_6_6z00(BgL_auxz00_1270, BgL_auxz00_1279); } 
return 
BBOOL(BgL_tmpz00_1269);} } 

}



/* char<=? */
BGL_EXPORTED_DEF bool_t BGl_charzc3zd3zf3ze3zz__r4_characters_6_6z00(unsigned char BgL_char1z00_10, unsigned char BgL_char2z00_11)
{
{ /* Ieee/char.scm 155 */
return 
(BgL_char1z00_10<=BgL_char2z00_11);} 

}



/* &char<=? */
obj_t BGl_z62charzc3zd3zf3z81zz__r4_characters_6_6z00(obj_t BgL_envz00_1094, obj_t BgL_char1z00_1095, obj_t BgL_char2z00_1096)
{
{ /* Ieee/char.scm 155 */
{ /* Ieee/char.scm 156 */
 bool_t BgL_tmpz00_1291;
{ /* Ieee/char.scm 156 */
 unsigned char BgL_auxz00_1301; unsigned char BgL_auxz00_1292;
{ /* Ieee/char.scm 156 */
 obj_t BgL_tmpz00_1302;
if(
CHARP(BgL_char2z00_1096))
{ /* Ieee/char.scm 156 */
BgL_tmpz00_1302 = BgL_char2z00_1096
; }  else 
{ 
 obj_t BgL_auxz00_1305;
BgL_auxz00_1305 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7402L), BGl_string1497z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1096); 
FAILURE(BgL_auxz00_1305,BFALSE,BFALSE);} 
BgL_auxz00_1301 = 
CCHAR(BgL_tmpz00_1302); } 
{ /* Ieee/char.scm 156 */
 obj_t BgL_tmpz00_1293;
if(
CHARP(BgL_char1z00_1095))
{ /* Ieee/char.scm 156 */
BgL_tmpz00_1293 = BgL_char1z00_1095
; }  else 
{ 
 obj_t BgL_auxz00_1296;
BgL_auxz00_1296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7402L), BGl_string1497z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1095); 
FAILURE(BgL_auxz00_1296,BFALSE,BFALSE);} 
BgL_auxz00_1292 = 
CCHAR(BgL_tmpz00_1293); } 
BgL_tmpz00_1291 = 
BGl_charzc3zd3zf3ze3zz__r4_characters_6_6z00(BgL_auxz00_1292, BgL_auxz00_1301); } 
return 
BBOOL(BgL_tmpz00_1291);} } 

}



/* char>=? */
BGL_EXPORTED_DEF bool_t BGl_charze3zd3zf3zc3zz__r4_characters_6_6z00(unsigned char BgL_char1z00_12, unsigned char BgL_char2z00_13)
{
{ /* Ieee/char.scm 161 */
return 
(BgL_char1z00_12>=BgL_char2z00_13);} 

}



/* &char>=? */
obj_t BGl_z62charze3zd3zf3za1zz__r4_characters_6_6z00(obj_t BgL_envz00_1097, obj_t BgL_char1z00_1098, obj_t BgL_char2z00_1099)
{
{ /* Ieee/char.scm 161 */
{ /* Ieee/char.scm 162 */
 bool_t BgL_tmpz00_1313;
{ /* Ieee/char.scm 162 */
 unsigned char BgL_auxz00_1323; unsigned char BgL_auxz00_1314;
{ /* Ieee/char.scm 162 */
 obj_t BgL_tmpz00_1324;
if(
CHARP(BgL_char2z00_1099))
{ /* Ieee/char.scm 162 */
BgL_tmpz00_1324 = BgL_char2z00_1099
; }  else 
{ 
 obj_t BgL_auxz00_1327;
BgL_auxz00_1327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7690L), BGl_string1498z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1099); 
FAILURE(BgL_auxz00_1327,BFALSE,BFALSE);} 
BgL_auxz00_1323 = 
CCHAR(BgL_tmpz00_1324); } 
{ /* Ieee/char.scm 162 */
 obj_t BgL_tmpz00_1315;
if(
CHARP(BgL_char1z00_1098))
{ /* Ieee/char.scm 162 */
BgL_tmpz00_1315 = BgL_char1z00_1098
; }  else 
{ 
 obj_t BgL_auxz00_1318;
BgL_auxz00_1318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7690L), BGl_string1498z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1098); 
FAILURE(BgL_auxz00_1318,BFALSE,BFALSE);} 
BgL_auxz00_1314 = 
CCHAR(BgL_tmpz00_1315); } 
BgL_tmpz00_1313 = 
BGl_charze3zd3zf3zc3zz__r4_characters_6_6z00(BgL_auxz00_1314, BgL_auxz00_1323); } 
return 
BBOOL(BgL_tmpz00_1313);} } 

}



/* char-ci=? */
BGL_EXPORTED_DEF bool_t BGl_charzd2cizd3zf3zf2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_14, unsigned char BgL_char2z00_15)
{
{ /* Ieee/char.scm 167 */
return 
(
toupper(BgL_char1z00_14)==
toupper(BgL_char2z00_15));} 

}



/* &char-ci=? */
obj_t BGl_z62charzd2cizd3zf3z90zz__r4_characters_6_6z00(obj_t BgL_envz00_1100, obj_t BgL_char1z00_1101, obj_t BgL_char2z00_1102)
{
{ /* Ieee/char.scm 167 */
{ /* Ieee/char.scm 168 */
 bool_t BgL_tmpz00_1337;
{ /* Ieee/char.scm 168 */
 unsigned char BgL_auxz00_1347; unsigned char BgL_auxz00_1338;
{ /* Ieee/char.scm 168 */
 obj_t BgL_tmpz00_1348;
if(
CHARP(BgL_char2z00_1102))
{ /* Ieee/char.scm 168 */
BgL_tmpz00_1348 = BgL_char2z00_1102
; }  else 
{ 
 obj_t BgL_auxz00_1351;
BgL_auxz00_1351 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7988L), BGl_string1499z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1102); 
FAILURE(BgL_auxz00_1351,BFALSE,BFALSE);} 
BgL_auxz00_1347 = 
CCHAR(BgL_tmpz00_1348); } 
{ /* Ieee/char.scm 168 */
 obj_t BgL_tmpz00_1339;
if(
CHARP(BgL_char1z00_1101))
{ /* Ieee/char.scm 168 */
BgL_tmpz00_1339 = BgL_char1z00_1101
; }  else 
{ 
 obj_t BgL_auxz00_1342;
BgL_auxz00_1342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(7988L), BGl_string1499z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1101); 
FAILURE(BgL_auxz00_1342,BFALSE,BFALSE);} 
BgL_auxz00_1338 = 
CCHAR(BgL_tmpz00_1339); } 
BgL_tmpz00_1337 = 
BGl_charzd2cizd3zf3zf2zz__r4_characters_6_6z00(BgL_auxz00_1338, BgL_auxz00_1347); } 
return 
BBOOL(BgL_tmpz00_1337);} } 

}



/* char-ci<? */
BGL_EXPORTED_DEF bool_t BGl_charzd2cizc3zf3ze2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_16, unsigned char BgL_char2z00_17)
{
{ /* Ieee/char.scm 173 */
return 
(
toupper(BgL_char1z00_16)<
toupper(BgL_char2z00_17));} 

}



/* &char-ci<? */
obj_t BGl_z62charzd2cizc3zf3z80zz__r4_characters_6_6z00(obj_t BgL_envz00_1103, obj_t BgL_char1z00_1104, obj_t BgL_char2z00_1105)
{
{ /* Ieee/char.scm 173 */
{ /* Ieee/char.scm 174 */
 bool_t BgL_tmpz00_1361;
{ /* Ieee/char.scm 174 */
 unsigned char BgL_auxz00_1371; unsigned char BgL_auxz00_1362;
{ /* Ieee/char.scm 174 */
 obj_t BgL_tmpz00_1372;
if(
CHARP(BgL_char2z00_1105))
{ /* Ieee/char.scm 174 */
BgL_tmpz00_1372 = BgL_char2z00_1105
; }  else 
{ 
 obj_t BgL_auxz00_1375;
BgL_auxz00_1375 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(8305L), BGl_string1500z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1105); 
FAILURE(BgL_auxz00_1375,BFALSE,BFALSE);} 
BgL_auxz00_1371 = 
CCHAR(BgL_tmpz00_1372); } 
{ /* Ieee/char.scm 174 */
 obj_t BgL_tmpz00_1363;
if(
CHARP(BgL_char1z00_1104))
{ /* Ieee/char.scm 174 */
BgL_tmpz00_1363 = BgL_char1z00_1104
; }  else 
{ 
 obj_t BgL_auxz00_1366;
BgL_auxz00_1366 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(8305L), BGl_string1500z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1104); 
FAILURE(BgL_auxz00_1366,BFALSE,BFALSE);} 
BgL_auxz00_1362 = 
CCHAR(BgL_tmpz00_1363); } 
BgL_tmpz00_1361 = 
BGl_charzd2cizc3zf3ze2zz__r4_characters_6_6z00(BgL_auxz00_1362, BgL_auxz00_1371); } 
return 
BBOOL(BgL_tmpz00_1361);} } 

}



/* char-ci>? */
BGL_EXPORTED_DEF bool_t BGl_charzd2cize3zf3zc2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_18, unsigned char BgL_char2z00_19)
{
{ /* Ieee/char.scm 179 */
return 
(
toupper(BgL_char1z00_18)>
toupper(BgL_char2z00_19));} 

}



/* &char-ci>? */
obj_t BGl_z62charzd2cize3zf3za0zz__r4_characters_6_6z00(obj_t BgL_envz00_1106, obj_t BgL_char1z00_1107, obj_t BgL_char2z00_1108)
{
{ /* Ieee/char.scm 179 */
{ /* Ieee/char.scm 180 */
 bool_t BgL_tmpz00_1385;
{ /* Ieee/char.scm 180 */
 unsigned char BgL_auxz00_1395; unsigned char BgL_auxz00_1386;
{ /* Ieee/char.scm 180 */
 obj_t BgL_tmpz00_1396;
if(
CHARP(BgL_char2z00_1108))
{ /* Ieee/char.scm 180 */
BgL_tmpz00_1396 = BgL_char2z00_1108
; }  else 
{ 
 obj_t BgL_auxz00_1399;
BgL_auxz00_1399 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(8624L), BGl_string1501z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1108); 
FAILURE(BgL_auxz00_1399,BFALSE,BFALSE);} 
BgL_auxz00_1395 = 
CCHAR(BgL_tmpz00_1396); } 
{ /* Ieee/char.scm 180 */
 obj_t BgL_tmpz00_1387;
if(
CHARP(BgL_char1z00_1107))
{ /* Ieee/char.scm 180 */
BgL_tmpz00_1387 = BgL_char1z00_1107
; }  else 
{ 
 obj_t BgL_auxz00_1390;
BgL_auxz00_1390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(8624L), BGl_string1501z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1107); 
FAILURE(BgL_auxz00_1390,BFALSE,BFALSE);} 
BgL_auxz00_1386 = 
CCHAR(BgL_tmpz00_1387); } 
BgL_tmpz00_1385 = 
BGl_charzd2cize3zf3zc2zz__r4_characters_6_6z00(BgL_auxz00_1386, BgL_auxz00_1395); } 
return 
BBOOL(BgL_tmpz00_1385);} } 

}



/* char-ci<=? */
BGL_EXPORTED_DEF bool_t BGl_charzd2cizc3zd3zf3z31zz__r4_characters_6_6z00(unsigned char BgL_char1z00_20, unsigned char BgL_char2z00_21)
{
{ /* Ieee/char.scm 185 */
return 
(
toupper(BgL_char1z00_20)<=
toupper(BgL_char2z00_21));} 

}



/* &char-ci<=? */
obj_t BGl_z62charzd2cizc3zd3zf3z53zz__r4_characters_6_6z00(obj_t BgL_envz00_1109, obj_t BgL_char1z00_1110, obj_t BgL_char2z00_1111)
{
{ /* Ieee/char.scm 185 */
{ /* Ieee/char.scm 186 */
 bool_t BgL_tmpz00_1409;
{ /* Ieee/char.scm 186 */
 unsigned char BgL_auxz00_1419; unsigned char BgL_auxz00_1410;
{ /* Ieee/char.scm 186 */
 obj_t BgL_tmpz00_1420;
if(
CHARP(BgL_char2z00_1111))
{ /* Ieee/char.scm 186 */
BgL_tmpz00_1420 = BgL_char2z00_1111
; }  else 
{ 
 obj_t BgL_auxz00_1423;
BgL_auxz00_1423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(8943L), BGl_string1502z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1111); 
FAILURE(BgL_auxz00_1423,BFALSE,BFALSE);} 
BgL_auxz00_1419 = 
CCHAR(BgL_tmpz00_1420); } 
{ /* Ieee/char.scm 186 */
 obj_t BgL_tmpz00_1411;
if(
CHARP(BgL_char1z00_1110))
{ /* Ieee/char.scm 186 */
BgL_tmpz00_1411 = BgL_char1z00_1110
; }  else 
{ 
 obj_t BgL_auxz00_1414;
BgL_auxz00_1414 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(8943L), BGl_string1502z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1110); 
FAILURE(BgL_auxz00_1414,BFALSE,BFALSE);} 
BgL_auxz00_1410 = 
CCHAR(BgL_tmpz00_1411); } 
BgL_tmpz00_1409 = 
BGl_charzd2cizc3zd3zf3z31zz__r4_characters_6_6z00(BgL_auxz00_1410, BgL_auxz00_1419); } 
return 
BBOOL(BgL_tmpz00_1409);} } 

}



/* char-ci>=? */
BGL_EXPORTED_DEF bool_t BGl_charzd2cize3zd3zf3z11zz__r4_characters_6_6z00(unsigned char BgL_char1z00_22, unsigned char BgL_char2z00_23)
{
{ /* Ieee/char.scm 191 */
return 
(
toupper(BgL_char1z00_22)>=
toupper(BgL_char2z00_23));} 

}



/* &char-ci>=? */
obj_t BGl_z62charzd2cize3zd3zf3z73zz__r4_characters_6_6z00(obj_t BgL_envz00_1112, obj_t BgL_char1z00_1113, obj_t BgL_char2z00_1114)
{
{ /* Ieee/char.scm 191 */
{ /* Ieee/char.scm 192 */
 bool_t BgL_tmpz00_1433;
{ /* Ieee/char.scm 192 */
 unsigned char BgL_auxz00_1443; unsigned char BgL_auxz00_1434;
{ /* Ieee/char.scm 192 */
 obj_t BgL_tmpz00_1444;
if(
CHARP(BgL_char2z00_1114))
{ /* Ieee/char.scm 192 */
BgL_tmpz00_1444 = BgL_char2z00_1114
; }  else 
{ 
 obj_t BgL_auxz00_1447;
BgL_auxz00_1447 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(9262L), BGl_string1503z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1114); 
FAILURE(BgL_auxz00_1447,BFALSE,BFALSE);} 
BgL_auxz00_1443 = 
CCHAR(BgL_tmpz00_1444); } 
{ /* Ieee/char.scm 192 */
 obj_t BgL_tmpz00_1435;
if(
CHARP(BgL_char1z00_1113))
{ /* Ieee/char.scm 192 */
BgL_tmpz00_1435 = BgL_char1z00_1113
; }  else 
{ 
 obj_t BgL_auxz00_1438;
BgL_auxz00_1438 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(9262L), BGl_string1503z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1113); 
FAILURE(BgL_auxz00_1438,BFALSE,BFALSE);} 
BgL_auxz00_1434 = 
CCHAR(BgL_tmpz00_1435); } 
BgL_tmpz00_1433 = 
BGl_charzd2cize3zd3zf3z11zz__r4_characters_6_6z00(BgL_auxz00_1434, BgL_auxz00_1443); } 
return 
BBOOL(BgL_tmpz00_1433);} } 

}



/* char-alphabetic? */
BGL_EXPORTED_DEF bool_t BGl_charzd2alphabeticzf3z21zz__r4_characters_6_6z00(unsigned char BgL_charz00_24)
{
{ /* Ieee/char.scm 197 */
return 
isalpha(BgL_charz00_24);} 

}



/* &char-alphabetic? */
obj_t BGl_z62charzd2alphabeticzf3z43zz__r4_characters_6_6z00(obj_t BgL_envz00_1115, obj_t BgL_charz00_1116)
{
{ /* Ieee/char.scm 197 */
{ /* Ieee/char.scm 199 */
 bool_t BgL_tmpz00_1455;
{ /* Ieee/char.scm 199 */
 unsigned char BgL_auxz00_1456;
{ /* Ieee/char.scm 199 */
 obj_t BgL_tmpz00_1457;
if(
CHARP(BgL_charz00_1116))
{ /* Ieee/char.scm 199 */
BgL_tmpz00_1457 = BgL_charz00_1116
; }  else 
{ 
 obj_t BgL_auxz00_1460;
BgL_auxz00_1460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(9598L), BGl_string1504z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1116); 
FAILURE(BgL_auxz00_1460,BFALSE,BFALSE);} 
BgL_auxz00_1456 = 
CCHAR(BgL_tmpz00_1457); } 
BgL_tmpz00_1455 = 
BGl_charzd2alphabeticzf3z21zz__r4_characters_6_6z00(BgL_auxz00_1456); } 
return 
BBOOL(BgL_tmpz00_1455);} } 

}



/* char-numeric? */
BGL_EXPORTED_DEF bool_t BGl_charzd2numericzf3z21zz__r4_characters_6_6z00(unsigned char BgL_charz00_25)
{
{ /* Ieee/char.scm 208 */
return 
isdigit(BgL_charz00_25);} 

}



/* &char-numeric? */
obj_t BGl_z62charzd2numericzf3z43zz__r4_characters_6_6z00(obj_t BgL_envz00_1117, obj_t BgL_charz00_1118)
{
{ /* Ieee/char.scm 208 */
{ /* Ieee/char.scm 210 */
 bool_t BgL_tmpz00_1468;
{ /* Ieee/char.scm 210 */
 unsigned char BgL_auxz00_1469;
{ /* Ieee/char.scm 210 */
 obj_t BgL_tmpz00_1470;
if(
CHARP(BgL_charz00_1118))
{ /* Ieee/char.scm 210 */
BgL_tmpz00_1470 = BgL_charz00_1118
; }  else 
{ 
 obj_t BgL_auxz00_1473;
BgL_auxz00_1473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(10040L), BGl_string1505z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1118); 
FAILURE(BgL_auxz00_1473,BFALSE,BFALSE);} 
BgL_auxz00_1469 = 
CCHAR(BgL_tmpz00_1470); } 
BgL_tmpz00_1468 = 
BGl_charzd2numericzf3z21zz__r4_characters_6_6z00(BgL_auxz00_1469); } 
return 
BBOOL(BgL_tmpz00_1468);} } 

}



/* char-whitespace? */
BGL_EXPORTED_DEF bool_t BGl_charzd2whitespacezf3z21zz__r4_characters_6_6z00(unsigned char BgL_charz00_26)
{
{ /* Ieee/char.scm 218 */
return 
isspace(BgL_charz00_26);} 

}



/* &char-whitespace? */
obj_t BGl_z62charzd2whitespacezf3z43zz__r4_characters_6_6z00(obj_t BgL_envz00_1119, obj_t BgL_charz00_1120)
{
{ /* Ieee/char.scm 218 */
{ /* Ieee/char.scm 220 */
 bool_t BgL_tmpz00_1481;
{ /* Ieee/char.scm 220 */
 unsigned char BgL_auxz00_1482;
{ /* Ieee/char.scm 220 */
 obj_t BgL_tmpz00_1483;
if(
CHARP(BgL_charz00_1120))
{ /* Ieee/char.scm 220 */
BgL_tmpz00_1483 = BgL_charz00_1120
; }  else 
{ 
 obj_t BgL_auxz00_1486;
BgL_auxz00_1486 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(10423L), BGl_string1506z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1120); 
FAILURE(BgL_auxz00_1486,BFALSE,BFALSE);} 
BgL_auxz00_1482 = 
CCHAR(BgL_tmpz00_1483); } 
BgL_tmpz00_1481 = 
BGl_charzd2whitespacezf3z21zz__r4_characters_6_6z00(BgL_auxz00_1482); } 
return 
BBOOL(BgL_tmpz00_1481);} } 

}



/* char-upper-case? */
BGL_EXPORTED_DEF bool_t BGl_charzd2upperzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char BgL_charz00_27)
{
{ /* Ieee/char.scm 229 */
return 
isupper(BgL_charz00_27);} 

}



/* &char-upper-case? */
obj_t BGl_z62charzd2upperzd2casezf3z91zz__r4_characters_6_6z00(obj_t BgL_envz00_1121, obj_t BgL_charz00_1122)
{
{ /* Ieee/char.scm 229 */
{ /* Ieee/char.scm 231 */
 bool_t BgL_tmpz00_1494;
{ /* Ieee/char.scm 231 */
 unsigned char BgL_auxz00_1495;
{ /* Ieee/char.scm 231 */
 obj_t BgL_tmpz00_1496;
if(
CHARP(BgL_charz00_1122))
{ /* Ieee/char.scm 231 */
BgL_tmpz00_1496 = BgL_charz00_1122
; }  else 
{ 
 obj_t BgL_auxz00_1499;
BgL_auxz00_1499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(10859L), BGl_string1507z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1122); 
FAILURE(BgL_auxz00_1499,BFALSE,BFALSE);} 
BgL_auxz00_1495 = 
CCHAR(BgL_tmpz00_1496); } 
BgL_tmpz00_1494 = 
BGl_charzd2upperzd2casezf3zf3zz__r4_characters_6_6z00(BgL_auxz00_1495); } 
return 
BBOOL(BgL_tmpz00_1494);} } 

}



/* char-lower-case? */
BGL_EXPORTED_DEF bool_t BGl_charzd2lowerzd2casezf3zf3zz__r4_characters_6_6z00(unsigned char BgL_charz00_28)
{
{ /* Ieee/char.scm 239 */
return 
islower(BgL_charz00_28);} 

}



/* &char-lower-case? */
obj_t BGl_z62charzd2lowerzd2casezf3z91zz__r4_characters_6_6z00(obj_t BgL_envz00_1123, obj_t BgL_charz00_1124)
{
{ /* Ieee/char.scm 239 */
{ /* Ieee/char.scm 241 */
 bool_t BgL_tmpz00_1507;
{ /* Ieee/char.scm 241 */
 unsigned char BgL_auxz00_1508;
{ /* Ieee/char.scm 241 */
 obj_t BgL_tmpz00_1509;
if(
CHARP(BgL_charz00_1124))
{ /* Ieee/char.scm 241 */
BgL_tmpz00_1509 = BgL_charz00_1124
; }  else 
{ 
 obj_t BgL_auxz00_1512;
BgL_auxz00_1512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(11245L), BGl_string1508z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1124); 
FAILURE(BgL_auxz00_1512,BFALSE,BFALSE);} 
BgL_auxz00_1508 = 
CCHAR(BgL_tmpz00_1509); } 
BgL_tmpz00_1507 = 
BGl_charzd2lowerzd2casezf3zf3zz__r4_characters_6_6z00(BgL_auxz00_1508); } 
return 
BBOOL(BgL_tmpz00_1507);} } 

}



/* char->integer */
BGL_EXPORTED_DEF long BGl_charzd2ze3integerz31zz__r4_characters_6_6z00(unsigned char BgL_charz00_29)
{
{ /* Ieee/char.scm 249 */
return 
(BgL_charz00_29);} 

}



/* &char->integer */
obj_t BGl_z62charzd2ze3integerz53zz__r4_characters_6_6z00(obj_t BgL_envz00_1125, obj_t BgL_charz00_1126)
{
{ /* Ieee/char.scm 249 */
{ /* Ieee/char.scm 250 */
 long BgL_tmpz00_1520;
{ /* Ieee/char.scm 250 */
 unsigned char BgL_auxz00_1521;
{ /* Ieee/char.scm 250 */
 obj_t BgL_tmpz00_1522;
if(
CHARP(BgL_charz00_1126))
{ /* Ieee/char.scm 250 */
BgL_tmpz00_1522 = BgL_charz00_1126
; }  else 
{ 
 obj_t BgL_auxz00_1525;
BgL_auxz00_1525 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(11599L), BGl_string1509z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1126); 
FAILURE(BgL_auxz00_1525,BFALSE,BFALSE);} 
BgL_auxz00_1521 = 
CCHAR(BgL_tmpz00_1522); } 
BgL_tmpz00_1520 = 
BGl_charzd2ze3integerz31zz__r4_characters_6_6z00(BgL_auxz00_1521); } 
return 
BINT(BgL_tmpz00_1520);} } 

}



/* integer->char */
BGL_EXPORTED_DEF unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long BgL_intz00_30)
{
{ /* Ieee/char.scm 255 */
{ /* Ieee/char.scm 256 */
 bool_t BgL_test1603z00_1532;
if(
(BgL_intz00_30>=0L))
{ /* Ieee/char.scm 256 */
BgL_test1603z00_1532 = 
(BgL_intz00_30<=255L)
; }  else 
{ /* Ieee/char.scm 256 */
BgL_test1603z00_1532 = ((bool_t)0)
; } 
if(BgL_test1603z00_1532)
{ /* Ieee/char.scm 256 */
return 
(BgL_intz00_30);}  else 
{ /* Ieee/char.scm 258 */
 obj_t BgL_tmpz00_1537;
{ /* Ieee/char.scm 258 */
 obj_t BgL_aux1474z00_1195;
BgL_aux1474z00_1195 = 
BGl_errorz00zz__errorz00(BGl_string1510z00zz__r4_characters_6_6z00, BGl_string1511z00zz__r4_characters_6_6z00, 
BINT(BgL_intz00_30)); 
if(
CHARP(BgL_aux1474z00_1195))
{ /* Ieee/char.scm 258 */
BgL_tmpz00_1537 = BgL_aux1474z00_1195
; }  else 
{ 
 obj_t BgL_auxz00_1542;
BgL_auxz00_1542 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(11951L), BGl_string1510z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_aux1474z00_1195); 
FAILURE(BgL_auxz00_1542,BFALSE,BFALSE);} } 
return 
CCHAR(BgL_tmpz00_1537);} } } 

}



/* &integer->char */
obj_t BGl_z62integerzd2ze3charz53zz__r4_characters_6_6z00(obj_t BgL_envz00_1127, obj_t BgL_intz00_1128)
{
{ /* Ieee/char.scm 255 */
{ /* Ieee/char.scm 256 */
 unsigned char BgL_tmpz00_1547;
{ /* Ieee/char.scm 256 */
 long BgL_auxz00_1548;
{ /* Ieee/char.scm 256 */
 obj_t BgL_tmpz00_1549;
if(
INTEGERP(BgL_intz00_1128))
{ /* Ieee/char.scm 256 */
BgL_tmpz00_1549 = BgL_intz00_1128
; }  else 
{ 
 obj_t BgL_auxz00_1552;
BgL_auxz00_1552 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(11877L), BGl_string1512z00zz__r4_characters_6_6z00, BGl_string1513z00zz__r4_characters_6_6z00, BgL_intz00_1128); 
FAILURE(BgL_auxz00_1552,BFALSE,BFALSE);} 
BgL_auxz00_1548 = 
(long)CINT(BgL_tmpz00_1549); } 
BgL_tmpz00_1547 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_auxz00_1548); } 
return 
BCHAR(BgL_tmpz00_1547);} } 

}



/* integer->char-ur */
BGL_EXPORTED_DEF unsigned char BGl_integerzd2ze3charzd2urze3zz__r4_characters_6_6z00(long BgL_intz00_31)
{
{ /* Ieee/char.scm 263 */
return 
(BgL_intz00_31);} 

}



/* &integer->char-ur */
obj_t BGl_z62integerzd2ze3charzd2urz81zz__r4_characters_6_6z00(obj_t BgL_envz00_1129, obj_t BgL_intz00_1130)
{
{ /* Ieee/char.scm 263 */
{ /* Ieee/char.scm 264 */
 unsigned char BgL_tmpz00_1560;
{ /* Ieee/char.scm 264 */
 long BgL_auxz00_1561;
{ /* Ieee/char.scm 264 */
 obj_t BgL_tmpz00_1562;
if(
INTEGERP(BgL_intz00_1130))
{ /* Ieee/char.scm 264 */
BgL_tmpz00_1562 = BgL_intz00_1130
; }  else 
{ 
 obj_t BgL_auxz00_1565;
BgL_auxz00_1565 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(12268L), BGl_string1514z00zz__r4_characters_6_6z00, BGl_string1513z00zz__r4_characters_6_6z00, BgL_intz00_1130); 
FAILURE(BgL_auxz00_1565,BFALSE,BFALSE);} 
BgL_auxz00_1561 = 
(long)CINT(BgL_tmpz00_1562); } 
BgL_tmpz00_1560 = 
BGl_integerzd2ze3charzd2urze3zz__r4_characters_6_6z00(BgL_auxz00_1561); } 
return 
BCHAR(BgL_tmpz00_1560);} } 

}



/* char-upcase */
BGL_EXPORTED_DEF unsigned char BGl_charzd2upcasezd2zz__r4_characters_6_6z00(unsigned char BgL_charz00_32)
{
{ /* Ieee/char.scm 269 */
return 
toupper(BgL_charz00_32);} 

}



/* &char-upcase */
obj_t BGl_z62charzd2upcasezb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1131, obj_t BgL_charz00_1132)
{
{ /* Ieee/char.scm 269 */
{ /* Ieee/char.scm 270 */
 unsigned char BgL_tmpz00_1573;
{ /* Ieee/char.scm 270 */
 unsigned char BgL_auxz00_1574;
{ /* Ieee/char.scm 270 */
 obj_t BgL_tmpz00_1575;
if(
CHARP(BgL_charz00_1132))
{ /* Ieee/char.scm 270 */
BgL_tmpz00_1575 = BgL_charz00_1132
; }  else 
{ 
 obj_t BgL_auxz00_1578;
BgL_auxz00_1578 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(12552L), BGl_string1515z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1132); 
FAILURE(BgL_auxz00_1578,BFALSE,BFALSE);} 
BgL_auxz00_1574 = 
CCHAR(BgL_tmpz00_1575); } 
BgL_tmpz00_1573 = 
BGl_charzd2upcasezd2zz__r4_characters_6_6z00(BgL_auxz00_1574); } 
return 
BCHAR(BgL_tmpz00_1573);} } 

}



/* char-downcase */
BGL_EXPORTED_DEF unsigned char BGl_charzd2downcasezd2zz__r4_characters_6_6z00(unsigned char BgL_charz00_33)
{
{ /* Ieee/char.scm 275 */
return 
tolower(BgL_charz00_33);} 

}



/* &char-downcase */
obj_t BGl_z62charzd2downcasezb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1133, obj_t BgL_charz00_1134)
{
{ /* Ieee/char.scm 275 */
{ /* Ieee/char.scm 276 */
 unsigned char BgL_tmpz00_1586;
{ /* Ieee/char.scm 276 */
 unsigned char BgL_auxz00_1587;
{ /* Ieee/char.scm 276 */
 obj_t BgL_tmpz00_1588;
if(
CHARP(BgL_charz00_1134))
{ /* Ieee/char.scm 276 */
BgL_tmpz00_1588 = BgL_charz00_1134
; }  else 
{ 
 obj_t BgL_auxz00_1591;
BgL_auxz00_1591 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(12837L), BGl_string1516z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_charz00_1134); 
FAILURE(BgL_auxz00_1591,BFALSE,BFALSE);} 
BgL_auxz00_1587 = 
CCHAR(BgL_tmpz00_1588); } 
BgL_tmpz00_1586 = 
BGl_charzd2downcasezd2zz__r4_characters_6_6z00(BgL_auxz00_1587); } 
return 
BCHAR(BgL_tmpz00_1586);} } 

}



/* char-or */
BGL_EXPORTED_DEF unsigned char BGl_charzd2orzd2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_34, unsigned char BgL_char2z00_35)
{
{ /* Ieee/char.scm 281 */
return 
(BgL_char1z00_34|BgL_char2z00_35);} 

}



/* &char-or */
obj_t BGl_z62charzd2orzb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1135, obj_t BgL_char1z00_1136, obj_t BgL_char2z00_1137)
{
{ /* Ieee/char.scm 281 */
{ /* Ieee/char.scm 282 */
 unsigned char BgL_tmpz00_1599;
{ /* Ieee/char.scm 282 */
 unsigned char BgL_auxz00_1609; unsigned char BgL_auxz00_1600;
{ /* Ieee/char.scm 282 */
 obj_t BgL_tmpz00_1610;
if(
CHARP(BgL_char2z00_1137))
{ /* Ieee/char.scm 282 */
BgL_tmpz00_1610 = BgL_char2z00_1137
; }  else 
{ 
 obj_t BgL_auxz00_1613;
BgL_auxz00_1613 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(13131L), BGl_string1517z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1137); 
FAILURE(BgL_auxz00_1613,BFALSE,BFALSE);} 
BgL_auxz00_1609 = 
CCHAR(BgL_tmpz00_1610); } 
{ /* Ieee/char.scm 282 */
 obj_t BgL_tmpz00_1601;
if(
CHARP(BgL_char1z00_1136))
{ /* Ieee/char.scm 282 */
BgL_tmpz00_1601 = BgL_char1z00_1136
; }  else 
{ 
 obj_t BgL_auxz00_1604;
BgL_auxz00_1604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(13131L), BGl_string1517z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1136); 
FAILURE(BgL_auxz00_1604,BFALSE,BFALSE);} 
BgL_auxz00_1600 = 
CCHAR(BgL_tmpz00_1601); } 
BgL_tmpz00_1599 = 
BGl_charzd2orzd2zz__r4_characters_6_6z00(BgL_auxz00_1600, BgL_auxz00_1609); } 
return 
BCHAR(BgL_tmpz00_1599);} } 

}



/* char-and */
BGL_EXPORTED_DEF unsigned char BGl_charzd2andzd2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_36, unsigned char BgL_char2z00_37)
{
{ /* Ieee/char.scm 287 */
return 
(BgL_char1z00_36&BgL_char2z00_37);} 

}



/* &char-and */
obj_t BGl_z62charzd2andzb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1138, obj_t BgL_char1z00_1139, obj_t BgL_char2z00_1140)
{
{ /* Ieee/char.scm 287 */
{ /* Ieee/char.scm 288 */
 unsigned char BgL_tmpz00_1621;
{ /* Ieee/char.scm 288 */
 unsigned char BgL_auxz00_1631; unsigned char BgL_auxz00_1622;
{ /* Ieee/char.scm 288 */
 obj_t BgL_tmpz00_1632;
if(
CHARP(BgL_char2z00_1140))
{ /* Ieee/char.scm 288 */
BgL_tmpz00_1632 = BgL_char2z00_1140
; }  else 
{ 
 obj_t BgL_auxz00_1635;
BgL_auxz00_1635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(13420L), BGl_string1518z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char2z00_1140); 
FAILURE(BgL_auxz00_1635,BFALSE,BFALSE);} 
BgL_auxz00_1631 = 
CCHAR(BgL_tmpz00_1632); } 
{ /* Ieee/char.scm 288 */
 obj_t BgL_tmpz00_1623;
if(
CHARP(BgL_char1z00_1139))
{ /* Ieee/char.scm 288 */
BgL_tmpz00_1623 = BgL_char1z00_1139
; }  else 
{ 
 obj_t BgL_auxz00_1626;
BgL_auxz00_1626 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(13420L), BGl_string1518z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1139); 
FAILURE(BgL_auxz00_1626,BFALSE,BFALSE);} 
BgL_auxz00_1622 = 
CCHAR(BgL_tmpz00_1623); } 
BgL_tmpz00_1621 = 
BGl_charzd2andzd2zz__r4_characters_6_6z00(BgL_auxz00_1622, BgL_auxz00_1631); } 
return 
BCHAR(BgL_tmpz00_1621);} } 

}



/* char-not */
BGL_EXPORTED_DEF unsigned char BGl_charzd2notzd2zz__r4_characters_6_6z00(unsigned char BgL_char1z00_38)
{
{ /* Ieee/char.scm 293 */
return 
~(BgL_char1z00_38);} 

}



/* &char-not */
obj_t BGl_z62charzd2notzb0zz__r4_characters_6_6z00(obj_t BgL_envz00_1141, obj_t BgL_char1z00_1142)
{
{ /* Ieee/char.scm 293 */
{ /* Ieee/char.scm 294 */
 unsigned char BgL_tmpz00_1643;
{ /* Ieee/char.scm 294 */
 unsigned char BgL_auxz00_1644;
{ /* Ieee/char.scm 294 */
 obj_t BgL_tmpz00_1645;
if(
CHARP(BgL_char1z00_1142))
{ /* Ieee/char.scm 294 */
BgL_tmpz00_1645 = BgL_char1z00_1142
; }  else 
{ 
 obj_t BgL_auxz00_1648;
BgL_auxz00_1648 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1492z00zz__r4_characters_6_6z00, 
BINT(13704L), BGl_string1519z00zz__r4_characters_6_6z00, BGl_string1494z00zz__r4_characters_6_6z00, BgL_char1z00_1142); 
FAILURE(BgL_auxz00_1648,BFALSE,BFALSE);} 
BgL_auxz00_1644 = 
CCHAR(BgL_tmpz00_1645); } 
BgL_tmpz00_1643 = 
BGl_charzd2notzd2zz__r4_characters_6_6z00(BgL_auxz00_1644); } 
return 
BCHAR(BgL_tmpz00_1643);} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_characters_6_6z00(void)
{
{ /* Ieee/char.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1520z00zz__r4_characters_6_6z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1520z00zz__r4_characters_6_6z00));} 

}

#ifdef __cplusplus
}
#endif
