/*===========================================================================*/
/*   (Ieee/output.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Ieee/output.scm -indent -o objs/obj_s/Ieee/output.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___R4_OUTPUT_6_10_3_TYPE_DEFINITIONS
#define BGL___R4_OUTPUT_6_10_3_TYPE_DEFINITIONS

/* object type definitions */

#endif // BGL___R4_OUTPUT_6_10_3_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_symbol2724z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2726z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2729z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2567z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
extern obj_t bgl_write_utf8string(obj_t, obj_t);
static obj_t BGl_symbol2650z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2732z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2652z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2573z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2658z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_displayzd2pairzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t bgl_write_binary_port(obj_t, obj_t);
extern obj_t symbol_for_read(obj_t);
extern obj_t bgl_write_ucs2(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_display_obj(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t bgl_display_ucs2string(obj_t, obj_t);
static obj_t BGl_z62illegalzd2charzd2repz62zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t bgl_write_output_port(obj_t, obj_t);
static obj_t BGl_symbol2677z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2679z00zz__r4_output_6_10_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_writezd2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t bgl_write_cnst(obj_t, obj_t);
static obj_t BGl_z62displayzd2flonumzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t bgl_write_procedure(obj_t, obj_t);
static obj_t BGl_symbol2681z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2683z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__r4_output_6_10_3z00(void);
static obj_t BGl_symbol2687z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t bgl_write_char(obj_t, obj_t);
static obj_t BGl_symbol2690z00zz__r4_output_6_10_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_symbol2694z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2698z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t BGl_objectzd2displayzd2zz__objectz00(BgL_objectz00_bglt, obj_t);
static obj_t BGl_z62writeza2zc0zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__r4_output_6_10_3z00(void);
static obj_t BGl_objectzd2initzd2zz__r4_output_6_10_3z00(void);
extern obj_t BGl_stringzd2skipzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62displayzd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
static obj_t BGl__writezd2bytezd2zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t bgl_write_custom(obj_t, obj_t);
static obj_t BGl_z62printfz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t bgl_write_bignum(obj_t, obj_t);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
extern obj_t bgl_weakptr_data(obj_t);
static obj_t BGl_z62displayzd2substringzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t bgl_write_unknown(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_printz00zz__r4_output_6_10_3z00(obj_t);
extern obj_t BGl_tvectorzd2refzd2zz__tvectorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_writezd2bytezd2zz__r4_output_6_10_3z00(unsigned char, obj_t);
BGL_EXPORTED_DECL obj_t BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62writezd2bytezd22z62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62writezd2symbolzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62newlinezd21zb0zz__r4_output_6_10_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62displayzd2elongzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62writezd22zb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62writezd2charzd22z62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__r4_output_6_10_3z00(void);
static obj_t BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt, obj_t);
extern obj_t bgl_write_input_port(obj_t, obj_t);
extern obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t);
extern obj_t bgl_display_llong(BGL_LONGLONG_T, obj_t);
static obj_t BGl_z62printz62zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62displayz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t make_string(long, unsigned char);
extern obj_t bgl_write_mmap(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_newlinezd21zd2zz__r4_output_6_10_3z00(obj_t);
extern obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
extern obj_t bgl_write_dynamic_env(obj_t, obj_t);
static obj_t BGl_z62writez62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_fprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t BGl_datezd2ze3stringz31zz__datez00(obj_t);
extern obj_t bgl_ill_char_rep(unsigned char);
static obj_t BGl_z62displayza2zc0zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t bgl_display_bignum(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t bgl_close_output_port(obj_t);
static obj_t BGl_z62formatz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62writezd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_newlinez00zz__r4_output_6_10_3z00(obj_t);
extern obj_t bgl_write_llong(BGL_LONGLONG_T, obj_t);
extern obj_t bgl_display_elong(long, obj_t);
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
extern bool_t BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void);
extern obj_t bgl_write_datagram_socket(obj_t, obj_t);
static obj_t BGl_list2700z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_list2704z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_list2708z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_z62writezd2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_list2715z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_list2718z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t string_for_read(obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62displayzd2fixnumzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t bgl_display_ucs2(obj_t, obj_t);
extern obj_t bgl_write_socket(obj_t, obj_t);
extern obj_t ucs2_string_to_utf8_string(obj_t);
static obj_t BGl_list2723z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_list2728z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t bgl_write_elong(long, obj_t);
extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_list2731z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl__writezd2charzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
extern obj_t bgl_write_process(obj_t, obj_t);
static obj_t BGl_z62displayzd2symbolzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2elongzd2zz__r4_output_6_10_3z00(long, obj_t);
static obj_t BGl_cnstzd2initzd2zz__r4_output_6_10_3z00(void);
extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__r4_output_6_10_3z00(void);
static obj_t BGl_xprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__r4_output_6_10_3z00(void);
static obj_t BGl_list2676z00zz__r4_output_6_10_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_writezd2charzd2zz__r4_output_6_10_3z00(unsigned char, obj_t);
extern obj_t bgl_display_fixnum(obj_t, obj_t);
extern obj_t bgl_write_semaphore(obj_t, obj_t);
extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
extern obj_t bgl_write_foreign(obj_t, obj_t);
static obj_t BGl_list2686z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_list2689z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_writezd2pairzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
static obj_t BGl_list2693z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_list2697z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_z62fprintz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2fixnumzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
extern obj_t bgl_write_regexp(obj_t, obj_t);
extern obj_t bgl_write_opaque(obj_t, obj_t);
extern obj_t bgl_write_string(obj_t, bool_t, obj_t);
static obj_t BGl_z62fprintfz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_write_obj(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_writezd2bytezd22z00zz__r4_output_6_10_3z00(unsigned char, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL obj_t BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_writeza2za2zz__r4_output_6_10_3z00(obj_t);
static obj_t BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2604z00zz__r4_output_6_10_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_displayzd2substringzd2zz__r4_output_6_10_3z00(obj_t, long, long, obj_t);
static obj_t BGl_symbol2608z00zz__r4_output_6_10_3z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_writezd2charzd22z00zz__r4_output_6_10_3z00(unsigned char, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayz00zz__r4_output_6_10_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_illegalzd2charzd2repz00zz__r4_output_6_10_3z00(unsigned char);
static obj_t BGl_symbol2611z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_z62displayzd2stringzb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_printfz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_symbol2701z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2705z00zz__r4_output_6_10_3z00 = BUNSPEC;
extern obj_t bgl_real_to_string(double);
static obj_t BGl_symbol2709z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_writezd2mutexzd2zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62displayzd22zb0zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2711z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2713z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_symbol2716z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_z62tprintz62zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2719z00zz__r4_output_6_10_3z00 = BUNSPEC;
static obj_t BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62newlinez62zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd22zd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7d22za7b0za72735z00, BGl_z62writezd22zb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2bytezd22zd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7d2byteza72736za7, BGl_z62writezd2bytezd22z62zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2600z00zz__r4_output_6_10_3z00, BgL_bgl_string2600za700za7za7_2737za7, "bstring", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_formatzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762formatza762za7za7__2738z00, va_generic_entry, BGl_z62formatz62zz__r4_output_6_10_3z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2601z00zz__r4_output_6_10_3z00, BgL_bgl_string2601za700za7za7_2739za7, "print-list", 10 );
DEFINE_STRING( BGl_string2602z00zz__r4_output_6_10_3z00, BgL_bgl_string2602za700za7za7_2740za7, "Tag not allowed here", 20 );
DEFINE_STRING( BGl_string2603z00zz__r4_output_6_10_3z00, BgL_bgl_string2603za700za7za7_2741za7, " . ", 3 );
DEFINE_STRING( BGl_string2605z00zz__r4_output_6_10_3z00, BgL_bgl_string2605za700za7za7_2742za7, "format", 6 );
DEFINE_STRING( BGl_string2606z00zz__r4_output_6_10_3z00, BgL_bgl_string2606za700za7za7_2743za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2607z00zz__r4_output_6_10_3z00, BgL_bgl_string2607za700za7za7_2744za7, "&format", 7 );
DEFINE_STRING( BGl_string2609z00zz__r4_output_6_10_3z00, BgL_bgl_string2609za700za7za7_2745za7, "printf", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_printfzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762printfza762za7za7__2746z00, va_generic_entry, BGl_z62printfz62zz__r4_output_6_10_3z00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2610z00zz__r4_output_6_10_3z00, BgL_bgl_string2610za700za7za7_2747za7, "&printf", 7 );
DEFINE_STRING( BGl_string2612z00zz__r4_output_6_10_3z00, BgL_bgl_string2612za700za7za7_2748za7, "fprintf", 7 );
DEFINE_STRING( BGl_string2613z00zz__r4_output_6_10_3z00, BgL_bgl_string2613za700za7za7_2749za7, "&fprintf", 8 );
DEFINE_STRING( BGl_string2614z00zz__r4_output_6_10_3z00, BgL_bgl_string2614za700za7za7_2750za7, "()", 2 );
DEFINE_STRING( BGl_string2615z00zz__r4_output_6_10_3z00, BgL_bgl_string2615za700za7za7_2751za7, "#f", 2 );
DEFINE_STRING( BGl_string2616z00zz__r4_output_6_10_3z00, BgL_bgl_string2616za700za7za7_2752za7, "#t", 2 );
DEFINE_STRING( BGl_string2617z00zz__r4_output_6_10_3z00, BgL_bgl_string2617za700za7za7_2753za7, "#unspecified", 12 );
DEFINE_STRING( BGl_string2618z00zz__r4_output_6_10_3z00, BgL_bgl_string2618za700za7za7_2754za7, "#<class:", 8 );
DEFINE_STRING( BGl_string2619z00zz__r4_output_6_10_3z00, BgL_bgl_string2619za700za7za7_2755za7, "display-2", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2stringzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2str2756z00, BGl_z62displayzd2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2620z00zz__r4_output_6_10_3z00, BgL_bgl_string2620za700za7za7_2757za7, "class", 5 );
DEFINE_STRING( BGl_string2702z00zz__r4_output_6_10_3z00, BgL_bgl_string2702za700za7za7_2758za7, "arg1699", 7 );
DEFINE_STRING( BGl_string2621z00zz__r4_output_6_10_3z00, BgL_bgl_string2621za700za7za7_2759za7, ">", 1 );
DEFINE_STRING( BGl_string2703z00zz__r4_output_6_10_3z00, BgL_bgl_string2703za700za7za7_2760za7, "write/display-tvector:Wrong number of arguments", 47 );
DEFINE_STRING( BGl_string2622z00zz__r4_output_6_10_3z00, BgL_bgl_string2622za700za7za7_2761za7, "#<condition-variable:", 21 );
DEFINE_STRING( BGl_string2623z00zz__r4_output_6_10_3z00, BgL_bgl_string2623za700za7za7_2762za7, "condvar", 7 );
DEFINE_STRING( BGl_string2624z00zz__r4_output_6_10_3z00, BgL_bgl_string2624za700za7za7_2763za7, "#<cell:", 7 );
DEFINE_STRING( BGl_string2706z00zz__r4_output_6_10_3z00, BgL_bgl_string2706za700za7za7_2764za7, "id", 2 );
DEFINE_STRING( BGl_string2625z00zz__r4_output_6_10_3z00, BgL_bgl_string2625za700za7za7_2765za7, "#eof-object", 11 );
DEFINE_STRING( BGl_string2707z00zz__r4_output_6_10_3z00, BgL_bgl_string2707za700za7za7_2766za7, "procedure", 9 );
DEFINE_STRING( BGl_string2626z00zz__r4_output_6_10_3z00, BgL_bgl_string2626za700za7za7_2767za7, "#!optional", 10 );
DEFINE_STRING( BGl_string2627z00zz__r4_output_6_10_3z00, BgL_bgl_string2627za700za7za7_2768za7, "#!rest", 6 );
DEFINE_STRING( BGl_string2628z00zz__r4_output_6_10_3z00, BgL_bgl_string2628za700za7za7_2769za7, "#!key", 5 );
DEFINE_STRING( BGl_string2629z00zz__r4_output_6_10_3z00, BgL_bgl_string2629za700za7za7_2770za7, "#<output_string_port>", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2substringzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2sub2771z00, BGl_z62displayzd2substringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string2710z00zz__r4_output_6_10_3z00, BgL_bgl_string2710za700za7za7_2772za7, "tvector-ref", 11 );
DEFINE_STRING( BGl_string2630z00zz__r4_output_6_10_3z00, BgL_bgl_string2630za700za7za7_2773za7, "#<output_procedure_port>", 24 );
DEFINE_STRING( BGl_string2712z00zz__r4_output_6_10_3z00, BgL_bgl_string2712za700za7za7_2774za7, "tvec", 4 );
DEFINE_STRING( BGl_string2631z00zz__r4_output_6_10_3z00, BgL_bgl_string2631za700za7za7_2775za7, "#<weakptr:", 10 );
DEFINE_STRING( BGl_string2632z00zz__r4_output_6_10_3z00, BgL_bgl_string2632za700za7za7_2776za7, "process", 7 );
DEFINE_STRING( BGl_string2714z00zz__r4_output_6_10_3z00, BgL_bgl_string2714za700za7za7_2777za7, "i", 1 );
DEFINE_STRING( BGl_string2633z00zz__r4_output_6_10_3z00, BgL_bgl_string2633za700za7za7_2778za7, "socket", 6 );
DEFINE_STRING( BGl_string2634z00zz__r4_output_6_10_3z00, BgL_bgl_string2634za700za7za7_2779za7, "datagram-socket", 15 );
DEFINE_STRING( BGl_string2635z00zz__r4_output_6_10_3z00, BgL_bgl_string2635za700za7za7_2780za7, "regexp", 6 );
DEFINE_STRING( BGl_string2717z00zz__r4_output_6_10_3z00, BgL_bgl_string2717za700za7za7_2781za7, "arg1707", 7 );
DEFINE_STRING( BGl_string2636z00zz__r4_output_6_10_3z00, BgL_bgl_string2636za700za7za7_2782za7, "mmap", 4 );
DEFINE_STRING( BGl_string2637z00zz__r4_output_6_10_3z00, BgL_bgl_string2637za700za7za7_2783za7, "semaphore", 9 );
DEFINE_STRING( BGl_string2638z00zz__r4_output_6_10_3z00, BgL_bgl_string2638za700za7za7_2784za7, "write-2", 7 );
DEFINE_STRING( BGl_string2639z00zz__r4_output_6_10_3z00, BgL_bgl_string2639za700za7za7_2785za7, "#<date:", 7 );
DEFINE_STRING( BGl_string2559z00zz__r4_output_6_10_3z00, BgL_bgl_string2559za700za7za7_2786za7, "tprint", 6 );
DEFINE_STRING( BGl_string2720z00zz__r4_output_6_10_3z00, BgL_bgl_string2720za700za7za7_2787za7, "arg1708", 7 );
DEFINE_STRING( BGl_string2721z00zz__r4_output_6_10_3z00, BgL_bgl_string2721za700za7za7_2788za7, "...)", 4 );
DEFINE_STRING( BGl_string2640z00zz__r4_output_6_10_3z00, BgL_bgl_string2640za700za7za7_2789za7, "#s8:", 4 );
DEFINE_STRING( BGl_string2722z00zz__r4_output_6_10_3z00, BgL_bgl_string2722za700za7za7_2790za7, "write/display-hvector", 21 );
DEFINE_STRING( BGl_string2641z00zz__r4_output_6_10_3z00, BgL_bgl_string2641za700za7za7_2791za7, "#u8:", 4 );
DEFINE_STRING( BGl_string2560z00zz__r4_output_6_10_3z00, BgL_bgl_string2560za700za7za7_2792za7, "newline", 7 );
DEFINE_STRING( BGl_string2642z00zz__r4_output_6_10_3z00, BgL_bgl_string2642za700za7za7_2793za7, "#s16:", 5 );
DEFINE_STRING( BGl_string2561z00zz__r4_output_6_10_3z00, BgL_bgl_string2561za700za7za7_2794za7, "wrong number of optional arguments", 34 );
DEFINE_STRING( BGl_string2643z00zz__r4_output_6_10_3z00, BgL_bgl_string2643za700za7za7_2795za7, "#u16:", 5 );
DEFINE_STRING( BGl_string2562z00zz__r4_output_6_10_3z00, BgL_bgl_string2562za700za7za7_2796za7, "/tmp/bigloo/runtime/Ieee/output.scm", 35 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_newlinezd21zd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762newlineza7d21za7b2797za7, BGl_z62newlinezd21zb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2725z00zz__r4_output_6_10_3z00, BgL_bgl_string2725za700za7za7_2798za7, "vref", 4 );
DEFINE_STRING( BGl_string2644z00zz__r4_output_6_10_3z00, BgL_bgl_string2644za700za7za7_2799za7, "#s32:", 5 );
DEFINE_STRING( BGl_string2563z00zz__r4_output_6_10_3z00, BgL_bgl_string2563za700za7za7_2800za7, "output-port", 11 );
DEFINE_STRING( BGl_string2645z00zz__r4_output_6_10_3z00, BgL_bgl_string2645za700za7za7_2801za7, "#u32:", 5 );
DEFINE_STRING( BGl_string2564z00zz__r4_output_6_10_3z00, BgL_bgl_string2564za700za7za7_2802za7, "&newline-1", 10 );
DEFINE_STRING( BGl_string2727z00zz__r4_output_6_10_3z00, BgL_bgl_string2727za700za7za7_2803za7, "svec", 4 );
DEFINE_STRING( BGl_string2646z00zz__r4_output_6_10_3z00, BgL_bgl_string2646za700za7za7_2804za7, "#s64:", 5 );
DEFINE_STRING( BGl_string2565z00zz__r4_output_6_10_3z00, BgL_bgl_string2565za700za7za7_2805za7, "display", 7 );
DEFINE_STRING( BGl_string2647z00zz__r4_output_6_10_3z00, BgL_bgl_string2647za700za7za7_2806za7, "#u64:", 5 );
DEFINE_STRING( BGl_string2566z00zz__r4_output_6_10_3z00, BgL_bgl_string2566za700za7za7_2807za7, "write", 5 );
DEFINE_STRING( BGl_string2648z00zz__r4_output_6_10_3z00, BgL_bgl_string2648za700za7za7_2808za7, "&display-symbol", 15 );
DEFINE_STRING( BGl_string2649z00zz__r4_output_6_10_3z00, BgL_bgl_string2649za700za7za7_2809za7, "symbol", 6 );
DEFINE_STRING( BGl_string2568z00zz__r4_output_6_10_3z00, BgL_bgl_string2568za700za7za7_2810za7, "write-char", 10 );
DEFINE_STRING( BGl_string2569z00zz__r4_output_6_10_3z00, BgL_bgl_string2569za700za7za7_2811za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_fprintfzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762fprintfza762za7za7_2812z00, va_generic_entry, BGl_z62fprintfz62zz__r4_output_6_10_3z00, BUNSPEC, -3 );
DEFINE_STRING( BGl_string2730z00zz__r4_output_6_10_3z00, BgL_bgl_string2730za700za7za7_2813za7, "arg1718", 7 );
DEFINE_STRING( BGl_string2651z00zz__r4_output_6_10_3z00, BgL_bgl_string2651za700za7za7_2814za7, "+", 1 );
DEFINE_STRING( BGl_string2570z00zz__r4_output_6_10_3z00, BgL_bgl_string2570za700za7za7_2815za7, "_write-char", 11 );
DEFINE_STRING( BGl_string2733z00zz__r4_output_6_10_3z00, BgL_bgl_string2733za700za7za7_2816za7, "arg1720", 7 );
DEFINE_STRING( BGl_string2571z00zz__r4_output_6_10_3z00, BgL_bgl_string2571za700za7za7_2817za7, "bchar", 5 );
DEFINE_STRING( BGl_string2734z00zz__r4_output_6_10_3z00, BgL_bgl_string2734za700za7za7_2818za7, "__r4_output_6_10_3", 18 );
DEFINE_STRING( BGl_string2653z00zz__r4_output_6_10_3z00, BgL_bgl_string2653za700za7za7_2819za7, "-", 1 );
DEFINE_STRING( BGl_string2572z00zz__r4_output_6_10_3z00, BgL_bgl_string2572za700za7za7_2820za7, "&write-char-2", 13 );
DEFINE_STRING( BGl_string2654z00zz__r4_output_6_10_3z00, BgL_bgl_string2654za700za7za7_2821za7, "|", 1 );
DEFINE_STRING( BGl_string2655z00zz__r4_output_6_10_3z00, BgL_bgl_string2655za700za7za7_2822za7, "&write-symbol", 13 );
DEFINE_STRING( BGl_string2574z00zz__r4_output_6_10_3z00, BgL_bgl_string2574za700za7za7_2823za7, "write-byte", 10 );
DEFINE_STRING( BGl_string2656z00zz__r4_output_6_10_3z00, BgL_bgl_string2656za700za7za7_2824za7, "&display-string", 15 );
DEFINE_STRING( BGl_string2575z00zz__r4_output_6_10_3z00, BgL_bgl_string2575za700za7za7_2825za7, "_write-byte", 11 );
DEFINE_STRING( BGl_string2657z00zz__r4_output_6_10_3z00, BgL_bgl_string2657za700za7za7_2826za7, "Illegal index, start=~a end=~a", 30 );
DEFINE_STRING( BGl_string2576z00zz__r4_output_6_10_3z00, BgL_bgl_string2576za700za7za7_2827za7, "bint", 4 );
DEFINE_STRING( BGl_string2577z00zz__r4_output_6_10_3z00, BgL_bgl_string2577za700za7za7_2828za7, "&write-byte-2", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2charzd2envz00zz__r4_output_6_10_3z00, BgL_bgl__writeza7d2charza7d22829z00, opt_generic_entry, BGl__writezd2charzd2zz__r4_output_6_10_3z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2659z00zz__r4_output_6_10_3z00, BgL_bgl_string2659za700za7za7_2830za7, "display-substring", 17 );
DEFINE_STRING( BGl_string2578z00zz__r4_output_6_10_3z00, BgL_bgl_string2578za700za7za7_2831za7, "#Newline", 8 );
DEFINE_STRING( BGl_string2579z00zz__r4_output_6_10_3z00, BgL_bgl_string2579za700za7za7_2832za7, "#Return", 7 );
DEFINE_STRING( BGl_string2660z00zz__r4_output_6_10_3z00, BgL_bgl_string2660za700za7za7_2833za7, "&display-substring", 18 );
DEFINE_STRING( BGl_string2661z00zz__r4_output_6_10_3z00, BgL_bgl_string2661za700za7za7_2834za7, "&write-string", 13 );
DEFINE_STRING( BGl_string2580z00zz__r4_output_6_10_3z00, BgL_bgl_string2580za700za7za7_2835za7, "#Space", 6 );
DEFINE_STRING( BGl_string2662z00zz__r4_output_6_10_3z00, BgL_bgl_string2662za700za7za7_2836za7, "&display-fixnum", 15 );
DEFINE_STRING( BGl_string2581z00zz__r4_output_6_10_3z00, BgL_bgl_string2581za700za7za7_2837za7, "#Tab", 4 );
DEFINE_STRING( BGl_string2663z00zz__r4_output_6_10_3z00, BgL_bgl_string2663za700za7za7_2838za7, "&display-elong", 14 );
DEFINE_STRING( BGl_string2582z00zz__r4_output_6_10_3z00, BgL_bgl_string2582za700za7za7_2839za7, "&illegal-char-rep", 17 );
DEFINE_STRING( BGl_string2664z00zz__r4_output_6_10_3z00, BgL_bgl_string2664za700za7za7_2840za7, "belong", 6 );
DEFINE_STRING( BGl_string2583z00zz__r4_output_6_10_3z00, BgL_bgl_string2583za700za7za7_2841za7, "loop", 4 );
DEFINE_STRING( BGl_string2665z00zz__r4_output_6_10_3z00, BgL_bgl_string2665za700za7za7_2842za7, "&display-flonum", 15 );
DEFINE_STRING( BGl_string2584z00zz__r4_output_6_10_3z00, BgL_bgl_string2584za700za7za7_2843za7, "pair", 4 );
DEFINE_STRING( BGl_string2666z00zz__r4_output_6_10_3z00, BgL_bgl_string2666za700za7za7_2844za7, "real", 4 );
DEFINE_STRING( BGl_string2585z00zz__r4_output_6_10_3z00, BgL_bgl_string2585za700za7za7_2845za7, "&tprint", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writeza2zd2envz70zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7a2za7c0za7za72846za7, va_generic_entry, BGl_z62writeza2zc0zz__r4_output_6_10_3z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string2667z00zz__r4_output_6_10_3z00, BgL_bgl_string2667za700za7za7_2847za7, "&display-ucs2string", 19 );
DEFINE_STRING( BGl_string2586z00zz__r4_output_6_10_3z00, BgL_bgl_string2586za700za7za7_2848za7, "&fprint", 7 );
DEFINE_STRING( BGl_string2668z00zz__r4_output_6_10_3z00, BgL_bgl_string2668za700za7za7_2849za7, "ucs2string", 10 );
DEFINE_STRING( BGl_string2587z00zz__r4_output_6_10_3z00, BgL_bgl_string2587za700za7za7_2850za7, "Insufficient number of arguments", 32 );
DEFINE_STRING( BGl_string2669z00zz__r4_output_6_10_3z00, BgL_bgl_string2669za700za7za7_2851za7, "&write-ucs2string", 17 );
DEFINE_STRING( BGl_string2588z00zz__r4_output_6_10_3z00, BgL_bgl_string2588za700za7za7_2852za7, "case_else1047", 13 );
DEFINE_STRING( BGl_string2589z00zz__r4_output_6_10_3z00, BgL_bgl_string2589za700za7za7_2853za7, "Illegal tag \"", 13 );
DEFINE_STRING( BGl_string2670z00zz__r4_output_6_10_3z00, BgL_bgl_string2670za700za7za7_2854za7, "#<mutex:", 8 );
DEFINE_STRING( BGl_string2671z00zz__r4_output_6_10_3z00, BgL_bgl_string2671za700za7za7_2855za7, "write-mutex", 11 );
DEFINE_STRING( BGl_string2590z00zz__r4_output_6_10_3z00, BgL_bgl_string2590za700za7za7_2856za7, "\"", 1 );
DEFINE_STRING( BGl_string2672z00zz__r4_output_6_10_3z00, BgL_bgl_string2672za700za7za7_2857za7, "mutex", 5 );
DEFINE_STRING( BGl_string2591z00zz__r4_output_6_10_3z00, BgL_bgl_string2591za700za7za7_2858za7, "handle-tag", 10 );
DEFINE_STRING( BGl_string2673z00zz__r4_output_6_10_3z00, BgL_bgl_string2673za700za7za7_2859za7, ":", 1 );
DEFINE_STRING( BGl_string2592z00zz__r4_output_6_10_3z00, BgL_bgl_string2592za700za7za7_2860za7, "Illegal char", 12 );
DEFINE_STRING( BGl_string2674z00zz__r4_output_6_10_3z00, BgL_bgl_string2674za700za7za7_2861za7, "write/display-structure", 23 );
DEFINE_STRING( BGl_string2593z00zz__r4_output_6_10_3z00, BgL_bgl_string2593za700za7za7_2862za7, "number", 6 );
DEFINE_STRING( BGl_string2675z00zz__r4_output_6_10_3z00, BgL_bgl_string2675za700za7za7_2863za7, "write/display-structure:Wrong number of arguments", 49 );
DEFINE_STRING( BGl_string2594z00zz__r4_output_6_10_3z00, BgL_bgl_string2594za700za7za7_2864za7, " ", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2charzd22zd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7d2charza72865za7, BGl_z62writezd2charzd22z62zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2595z00zz__r4_output_6_10_3z00, BgL_bgl_string2595za700za7za7_2866za7, "0123456789", 10 );
DEFINE_STRING( BGl_string2596z00zz__r4_output_6_10_3z00, BgL_bgl_string2596za700za7za7_2867za7, "print-formatted-number", 22 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2ucs2stringzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2ucs2868z00, BGl_z62displayzd2ucs2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2678z00zz__r4_output_6_10_3z00, BgL_bgl_string2678za700za7za7_2869za7, "funcall", 7 );
DEFINE_STRING( BGl_string2597z00zz__r4_output_6_10_3z00, BgL_bgl_string2597za700za7za7_2870za7, "string-ref", 10 );
DEFINE_STRING( BGl_string2598z00zz__r4_output_6_10_3z00, BgL_bgl_string2598za700za7za7_2871za7, "Illegal tag", 11 );
DEFINE_STRING( BGl_string2599z00zz__r4_output_6_10_3z00, BgL_bgl_string2599za700za7za7_2872za7, "print-padded-number", 19 );
DEFINE_STRING( BGl_string2680z00zz__r4_output_6_10_3z00, BgL_bgl_string2680za700za7za7_2873za7, "disp", 4 );
DEFINE_STRING( BGl_string2682z00zz__r4_output_6_10_3z00, BgL_bgl_string2682za700za7za7_2874za7, "arg1664", 7 );
DEFINE_STRING( BGl_string2684z00zz__r4_output_6_10_3z00, BgL_bgl_string2684za700za7za7_2875za7, "port", 4 );
DEFINE_STRING( BGl_string2685z00zz__r4_output_6_10_3z00, BgL_bgl_string2685za700za7za7_2876za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string2688z00zz__r4_output_6_10_3z00, BgL_bgl_string2688za700za7za7_2877za7, "arg1675", 7 );
DEFINE_STRING( BGl_string2691z00zz__r4_output_6_10_3z00, BgL_bgl_string2691za700za7za7_2878za7, "arg1676", 7 );
DEFINE_STRING( BGl_string2692z00zz__r4_output_6_10_3z00, BgL_bgl_string2692za700za7za7_2879za7, "write/display-vector:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string2695z00zz__r4_output_6_10_3z00, BgL_bgl_string2695za700za7za7_2880za7, "tag", 3 );
DEFINE_STRING( BGl_string2696z00zz__r4_output_6_10_3z00, BgL_bgl_string2696za700za7za7_2881za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string2699z00zz__r4_output_6_10_3z00, BgL_bgl_string2699za700za7za7_2882za7, "arg1692", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2symbolzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7d2symbo2883z00, BGl_z62writezd2symbolzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762displayza762za7za7_2884z00, bgl_va_stack_entry, BGl_z62displayz62zz__r4_output_6_10_3z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2elongzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2elo2885z00, BGl_z62displayzd2elongzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762writeza762za7za7__r2886z00, bgl_va_stack_entry, BGl_z62writez62zz__r4_output_6_10_3z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d22za7b2887za7, BGl_z62displayzd22zb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_newlinezd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762newlineza762za7za7_2888z00, va_generic_entry, BGl_z62newlinez62zz__r4_output_6_10_3z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayza2zd2envz70zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7a2za7c02889za7, va_generic_entry, BGl_z62displayza2zc0zz__r4_output_6_10_3z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tprintzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762tprintza762za7za7__2890z00, va_generic_entry, BGl_z62tprintz62zz__r4_output_6_10_3z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2ucs2stringzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7d2ucs2s2891z00, BGl_z62writezd2ucs2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2bytezd2envz00zz__r4_output_6_10_3z00, BgL_bgl__writeza7d2byteza7d22892z00, opt_generic_entry, BGl__writezd2bytezd2zz__r4_output_6_10_3z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2flonumzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2flo2893z00, BGl_z62displayzd2flonumzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_fprintzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762fprintza762za7za7__2894z00, va_generic_entry, BGl_z62fprintz62zz__r4_output_6_10_3z00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_printzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762printza762za7za7__r2895z00, bgl_va_stack_entry, BGl_z62printz62zz__r4_output_6_10_3z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2symbolzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2sym2896z00, BGl_z62displayzd2symbolzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_writezd2stringzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762writeza7d2strin2897z00, BGl_z62writezd2stringzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_illegalzd2charzd2repzd2envzd2zz__r4_output_6_10_3z00, BgL_bgl_za762illegalza7d2cha2898z00, BGl_z62illegalzd2charzd2repz62zz__r4_output_6_10_3z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2fixnumzd2envz00zz__r4_output_6_10_3z00, BgL_bgl_za762displayza7d2fix2899z00, BGl_z62displayzd2fixnumzb0zz__r4_output_6_10_3z00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol2724z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2726z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2729z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2567z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2650z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2732z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2652z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2573z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2658z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2677z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2679z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2681z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2683z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2687z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2690z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2694z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2698z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2700z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2704z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2708z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2715z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2718z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2723z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2728z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2731z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2676z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2686z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2689z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2693z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_list2697z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2604z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2608z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2611z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2701z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2705z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2709z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2711z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2713z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2716z00zz__r4_output_6_10_3z00) );
ADD_ROOT( (void *)(&BGl_symbol2719z00zz__r4_output_6_10_3z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long BgL_checksumz00_4354, char * BgL_fromz00_4355)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00))
{ 
BGl_requirezd2initializa7ationz75zz__r4_output_6_10_3z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__r4_output_6_10_3z00(); 
BGl_cnstzd2initzd2zz__r4_output_6_10_3z00(); 
BGl_importedzd2moduleszd2initz00zz__r4_output_6_10_3z00(); 
return 
BGl_toplevelzd2initzd2zz__r4_output_6_10_3z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
BGl_symbol2567z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2568z00zz__r4_output_6_10_3z00); 
BGl_symbol2573z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2574z00zz__r4_output_6_10_3z00); 
BGl_symbol2604z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2605z00zz__r4_output_6_10_3z00); 
BGl_symbol2608z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2609z00zz__r4_output_6_10_3z00); 
BGl_symbol2611z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2612z00zz__r4_output_6_10_3z00); 
BGl_symbol2650z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2651z00zz__r4_output_6_10_3z00); 
BGl_symbol2652z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2653z00zz__r4_output_6_10_3z00); 
BGl_symbol2658z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2659z00zz__r4_output_6_10_3z00); 
BGl_symbol2677z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2678z00zz__r4_output_6_10_3z00); 
BGl_symbol2679z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2680z00zz__r4_output_6_10_3z00); 
BGl_symbol2681z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2682z00zz__r4_output_6_10_3z00); 
BGl_symbol2683z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2684z00zz__r4_output_6_10_3z00); 
BGl_list2676z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2681z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2687z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2688z00zz__r4_output_6_10_3z00); 
BGl_list2686z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2687z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2690z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2691z00zz__r4_output_6_10_3z00); 
BGl_list2689z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2690z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2694z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2695z00zz__r4_output_6_10_3z00); 
BGl_list2693z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2698z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2699z00zz__r4_output_6_10_3z00); 
BGl_list2697z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2698z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2701z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2702z00zz__r4_output_6_10_3z00); 
BGl_list2700z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2701z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2705z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2706z00zz__r4_output_6_10_3z00); 
BGl_list2704z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2705z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2709z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2710z00zz__r4_output_6_10_3z00); 
BGl_symbol2711z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2712z00zz__r4_output_6_10_3z00); 
BGl_symbol2713z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2714z00zz__r4_output_6_10_3z00); 
BGl_list2708z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2709z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2709z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2711z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2713z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2716z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2717z00zz__r4_output_6_10_3z00); 
BGl_list2715z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2716z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2719z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2720z00zz__r4_output_6_10_3z00); 
BGl_list2718z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2719z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2724z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2725z00zz__r4_output_6_10_3z00); 
BGl_symbol2726z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2727z00zz__r4_output_6_10_3z00); 
BGl_list2723z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2724z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2724z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2713z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2729z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2730z00zz__r4_output_6_10_3z00); 
BGl_list2728z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2729z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))); 
BGl_symbol2732z00zz__r4_output_6_10_3z00 = 
bstring_to_symbol(BGl_string2733z00zz__r4_output_6_10_3z00); 
return ( 
BGl_list2731z00zz__r4_output_6_10_3z00 = 
MAKE_YOUNG_PAIR(BGl_symbol2677z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2679z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2732z00zz__r4_output_6_10_3z00, 
MAKE_YOUNG_PAIR(BGl_symbol2683z00zz__r4_output_6_10_3z00, BNIL))))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
return ( 
BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00 = 
bgl_make_mutex(BGl_string2559z00zz__r4_output_6_10_3z00), BUNSPEC) ;} 

}



/* newline */
BGL_EXPORTED_DEF obj_t BGl_newlinez00zz__r4_output_6_10_3z00(obj_t BgL_portz00_3)
{
{ /* Ieee/output.scm 259 */
{ /* Ieee/output.scm 260 */
 obj_t BgL_portz00_1613;
if(
NULLP(BgL_portz00_3))
{ /* Ieee/output.scm 262 */
 obj_t BgL_tmpz00_4459;
BgL_tmpz00_4459 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1613 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4459); }  else 
{ /* Ieee/output.scm 260 */
if(
PAIRP(BgL_portz00_3))
{ /* Ieee/output.scm 260 */
if(
NULLP(
CDR(
((obj_t)BgL_portz00_3))))
{ /* Ieee/output.scm 260 */
BgL_portz00_1613 = 
CAR(BgL_portz00_3); }  else 
{ /* Ieee/output.scm 260 */
BgL_portz00_1613 = 
BGl_errorz00zz__errorz00(BGl_string2560z00zz__r4_output_6_10_3z00, BGl_string2561z00zz__r4_output_6_10_3z00, BgL_portz00_3); } }  else 
{ /* Ieee/output.scm 260 */
BgL_portz00_1613 = 
BGl_errorz00zz__errorz00(BGl_string2560z00zz__r4_output_6_10_3z00, BGl_string2561z00zz__r4_output_6_10_3z00, BgL_portz00_3); } } 
{ /* Ieee/output.scm 269 */
 obj_t BgL_portz00_2811;
if(
OUTPUT_PORTP(BgL_portz00_1613))
{ /* Ieee/output.scm 269 */
BgL_portz00_2811 = BgL_portz00_1613; }  else 
{ 
 obj_t BgL_auxz00_4473;
BgL_auxz00_4473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(10308L), BGl_string2560z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_1613); 
FAILURE(BgL_auxz00_4473,BFALSE,BFALSE);} 
return 
bgl_display_char(((unsigned char)10), BgL_portz00_2811);} } } 

}



/* &newline */
obj_t BGl_z62newlinez62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3882, obj_t BgL_portz00_3883)
{
{ /* Ieee/output.scm 259 */
return 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_portz00_3883);} 

}



/* newline-1 */
BGL_EXPORTED_DEF obj_t BGl_newlinezd21zd2zz__r4_output_6_10_3z00(obj_t BgL_portz00_4)
{
{ /* Ieee/output.scm 274 */
return 
bgl_display_char(((unsigned char)10), BgL_portz00_4);} 

}



/* &newline-1 */
obj_t BGl_z62newlinezd21zb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3884, obj_t BgL_portz00_3885)
{
{ /* Ieee/output.scm 274 */
{ /* Ieee/output.scm 275 */
 obj_t BgL_auxz00_4480;
if(
OUTPUT_PORTP(BgL_portz00_3885))
{ /* Ieee/output.scm 275 */
BgL_auxz00_4480 = BgL_portz00_3885
; }  else 
{ 
 obj_t BgL_auxz00_4483;
BgL_auxz00_4483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(10587L), BGl_string2564z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3885); 
FAILURE(BgL_auxz00_4483,BFALSE,BFALSE);} 
return 
BGl_newlinezd21zd2zz__r4_output_6_10_3z00(BgL_auxz00_4480);} } 

}



/* display */
BGL_EXPORTED_DEF obj_t BGl_displayz00zz__r4_output_6_10_3z00(obj_t BgL_objz00_5, obj_t BgL_portz00_6)
{
{ /* Ieee/output.scm 280 */
{ /* Ieee/output.scm 281 */
 obj_t BgL_portz00_1623;
if(
NULLP(BgL_portz00_6))
{ /* Ieee/output.scm 283 */
 obj_t BgL_tmpz00_4490;
BgL_tmpz00_4490 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1623 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4490); }  else 
{ /* Ieee/output.scm 281 */
if(
PAIRP(BgL_portz00_6))
{ /* Ieee/output.scm 281 */
if(
NULLP(
CDR(
((obj_t)BgL_portz00_6))))
{ /* Ieee/output.scm 281 */
BgL_portz00_1623 = 
CAR(BgL_portz00_6); }  else 
{ /* Ieee/output.scm 281 */
BgL_portz00_1623 = 
BGl_errorz00zz__errorz00(BGl_string2565z00zz__r4_output_6_10_3z00, BGl_string2561z00zz__r4_output_6_10_3z00, BgL_portz00_6); } }  else 
{ /* Ieee/output.scm 281 */
BgL_portz00_1623 = 
BGl_errorz00zz__errorz00(BGl_string2565z00zz__r4_output_6_10_3z00, BGl_string2561z00zz__r4_output_6_10_3z00, BgL_portz00_6); } } 
{ /* Ieee/output.scm 290 */
 obj_t BgL_auxz00_4502;
if(
OUTPUT_PORTP(BgL_portz00_1623))
{ /* Ieee/output.scm 290 */
BgL_auxz00_4502 = BgL_portz00_1623
; }  else 
{ 
 obj_t BgL_auxz00_4505;
BgL_auxz00_4505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(11076L), BGl_string2565z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_1623); 
FAILURE(BgL_auxz00_4505,BFALSE,BFALSE);} 
return 
bgl_display_obj(BgL_objz00_5, BgL_auxz00_4502);} } } 

}



/* &display */
obj_t BGl_z62displayz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3886, obj_t BgL_objz00_3887, obj_t BgL_portz00_3888)
{
{ /* Ieee/output.scm 280 */
return 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_objz00_3887, BgL_portz00_3888);} 

}



/* write */
BGL_EXPORTED_DEF obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t BgL_objz00_7, obj_t BgL_portz00_8)
{
{ /* Ieee/output.scm 295 */
{ /* Ieee/output.scm 296 */
 obj_t BgL_portz00_1633;
if(
NULLP(BgL_portz00_8))
{ /* Ieee/output.scm 298 */
 obj_t BgL_tmpz00_4513;
BgL_tmpz00_4513 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1633 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4513); }  else 
{ /* Ieee/output.scm 296 */
if(
PAIRP(BgL_portz00_8))
{ /* Ieee/output.scm 296 */
if(
NULLP(
CDR(
((obj_t)BgL_portz00_8))))
{ /* Ieee/output.scm 296 */
BgL_portz00_1633 = 
CAR(BgL_portz00_8); }  else 
{ /* Ieee/output.scm 296 */
BgL_portz00_1633 = 
BGl_errorz00zz__errorz00(BGl_string2566z00zz__r4_output_6_10_3z00, BGl_string2561z00zz__r4_output_6_10_3z00, BgL_portz00_8); } }  else 
{ /* Ieee/output.scm 296 */
BgL_portz00_1633 = 
BGl_errorz00zz__errorz00(BGl_string2566z00zz__r4_output_6_10_3z00, BGl_string2561z00zz__r4_output_6_10_3z00, BgL_portz00_8); } } 
{ /* Ieee/output.scm 305 */
 obj_t BgL_auxz00_4525;
if(
OUTPUT_PORTP(BgL_portz00_1633))
{ /* Ieee/output.scm 305 */
BgL_auxz00_4525 = BgL_portz00_1633
; }  else 
{ 
 obj_t BgL_auxz00_4528;
BgL_auxz00_4528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(11535L), BGl_string2566z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_1633); 
FAILURE(BgL_auxz00_4528,BFALSE,BFALSE);} 
return 
bgl_write_obj(BgL_objz00_7, BgL_auxz00_4525);} } } 

}



/* &write */
obj_t BGl_z62writez62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3889, obj_t BgL_objz00_3890, obj_t BgL_portz00_3891)
{
{ /* Ieee/output.scm 295 */
return 
BGl_writez00zz__r4_output_6_10_3z00(BgL_objz00_3890, BgL_portz00_3891);} 

}



/* _write-char */
obj_t BGl__writezd2charzd2zz__r4_output_6_10_3z00(obj_t BgL_env1163z00_12, obj_t BgL_opt1162z00_11)
{
{ /* Ieee/output.scm 310 */
{ /* Ieee/output.scm 310 */
 obj_t BgL_g1164z00_1643;
BgL_g1164z00_1643 = 
VECTOR_REF(BgL_opt1162z00_11,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1162z00_11)) { case 1L : 

{ /* Ieee/output.scm 310 */
 obj_t BgL_portz00_1646;
{ /* Ieee/output.scm 310 */
 obj_t BgL_tmpz00_4535;
BgL_tmpz00_4535 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1646 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4535); } 
{ /* Ieee/output.scm 310 */

{ /* Ieee/output.scm 310 */
 unsigned char BgL_charz00_2823;
{ /* Ieee/output.scm 310 */
 obj_t BgL_tmpz00_4538;
if(
CHARP(BgL_g1164z00_1643))
{ /* Ieee/output.scm 310 */
BgL_tmpz00_4538 = BgL_g1164z00_1643
; }  else 
{ 
 obj_t BgL_auxz00_4541;
BgL_auxz00_4541 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(11766L), BGl_string2570z00zz__r4_output_6_10_3z00, BGl_string2571z00zz__r4_output_6_10_3z00, BgL_g1164z00_1643); 
FAILURE(BgL_auxz00_4541,BFALSE,BFALSE);} 
BgL_charz00_2823 = 
CCHAR(BgL_tmpz00_4538); } 
return 
bgl_display_char(BgL_charz00_2823, BgL_portz00_1646);} } } break;case 2L : 

{ /* Ieee/output.scm 310 */
 obj_t BgL_portz00_1647;
BgL_portz00_1647 = 
VECTOR_REF(BgL_opt1162z00_11,1L); 
{ /* Ieee/output.scm 310 */

{ /* Ieee/output.scm 310 */
 unsigned char BgL_charz00_2826;
{ /* Ieee/output.scm 310 */
 obj_t BgL_tmpz00_4548;
if(
CHARP(BgL_g1164z00_1643))
{ /* Ieee/output.scm 310 */
BgL_tmpz00_4548 = BgL_g1164z00_1643
; }  else 
{ 
 obj_t BgL_auxz00_4551;
BgL_auxz00_4551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(11766L), BGl_string2570z00zz__r4_output_6_10_3z00, BGl_string2571z00zz__r4_output_6_10_3z00, BgL_g1164z00_1643); 
FAILURE(BgL_auxz00_4551,BFALSE,BFALSE);} 
BgL_charz00_2826 = 
CCHAR(BgL_tmpz00_4548); } 
{ /* Ieee/output.scm 311 */
 obj_t BgL_portz00_2828;
if(
OUTPUT_PORTP(BgL_portz00_1647))
{ /* Ieee/output.scm 311 */
BgL_portz00_2828 = BgL_portz00_1647; }  else 
{ 
 obj_t BgL_auxz00_4558;
BgL_auxz00_4558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(11854L), BGl_string2570z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_1647); 
FAILURE(BgL_auxz00_4558,BFALSE,BFALSE);} 
return 
bgl_display_char(BgL_charz00_2826, BgL_portz00_2828);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2567z00zz__r4_output_6_10_3z00, BGl_string2569z00zz__r4_output_6_10_3z00, 
BINT(
VECTOR_LENGTH(BgL_opt1162z00_11)));} } } } 

}



/* write-char */
BGL_EXPORTED_DEF obj_t BGl_writezd2charzd2zz__r4_output_6_10_3z00(unsigned char BgL_charz00_9, obj_t BgL_portz00_10)
{
{ /* Ieee/output.scm 310 */
{ /* Ieee/output.scm 311 */
 obj_t BgL_portz00_2830;
if(
OUTPUT_PORTP(BgL_portz00_10))
{ /* Ieee/output.scm 311 */
BgL_portz00_2830 = BgL_portz00_10; }  else 
{ 
 obj_t BgL_auxz00_4570;
BgL_auxz00_4570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(11854L), BGl_string2568z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_10); 
FAILURE(BgL_auxz00_4570,BFALSE,BFALSE);} 
return 
bgl_display_char(BgL_charz00_9, BgL_portz00_2830);} } 

}



/* write-char-2 */
BGL_EXPORTED_DEF obj_t BGl_writezd2charzd22z00zz__r4_output_6_10_3z00(unsigned char BgL_charz00_13, obj_t BgL_portz00_14)
{
{ /* Ieee/output.scm 316 */
return 
bgl_display_char(BgL_charz00_13, BgL_portz00_14);} 

}



/* &write-char-2 */
obj_t BGl_z62writezd2charzd22z62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3892, obj_t BgL_charz00_3893, obj_t BgL_portz00_3894)
{
{ /* Ieee/output.scm 316 */
{ /* Ieee/output.scm 317 */
 obj_t BgL_auxz00_4585; unsigned char BgL_auxz00_4576;
if(
OUTPUT_PORTP(BgL_portz00_3894))
{ /* Ieee/output.scm 317 */
BgL_auxz00_4585 = BgL_portz00_3894
; }  else 
{ 
 obj_t BgL_auxz00_4588;
BgL_auxz00_4588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12147L), BGl_string2572z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3894); 
FAILURE(BgL_auxz00_4588,BFALSE,BFALSE);} 
{ /* Ieee/output.scm 317 */
 obj_t BgL_tmpz00_4577;
if(
CHARP(BgL_charz00_3893))
{ /* Ieee/output.scm 317 */
BgL_tmpz00_4577 = BgL_charz00_3893
; }  else 
{ 
 obj_t BgL_auxz00_4580;
BgL_auxz00_4580 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12147L), BGl_string2572z00zz__r4_output_6_10_3z00, BGl_string2571z00zz__r4_output_6_10_3z00, BgL_charz00_3893); 
FAILURE(BgL_auxz00_4580,BFALSE,BFALSE);} 
BgL_auxz00_4576 = 
CCHAR(BgL_tmpz00_4577); } 
return 
BGl_writezd2charzd22z00zz__r4_output_6_10_3z00(BgL_auxz00_4576, BgL_auxz00_4585);} } 

}



/* _write-byte */
obj_t BGl__writezd2bytezd2zz__r4_output_6_10_3z00(obj_t BgL_env1168z00_18, obj_t BgL_opt1167z00_17)
{
{ /* Ieee/output.scm 322 */
{ /* Ieee/output.scm 322 */
 obj_t BgL_g1169z00_1649;
BgL_g1169z00_1649 = 
VECTOR_REF(BgL_opt1167z00_17,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1167z00_17)) { case 1L : 

{ /* Ieee/output.scm 322 */
 obj_t BgL_portz00_1652;
{ /* Ieee/output.scm 322 */
 obj_t BgL_tmpz00_4594;
BgL_tmpz00_4594 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1652 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4594); } 
{ /* Ieee/output.scm 322 */

{ /* Ieee/output.scm 322 */
 unsigned char BgL_bytez00_2832;
{ /* Ieee/output.scm 322 */
 obj_t BgL_tmpz00_4597;
if(
INTEGERP(BgL_g1169z00_1649))
{ /* Ieee/output.scm 322 */
BgL_tmpz00_4597 = BgL_g1169z00_1649
; }  else 
{ 
 obj_t BgL_auxz00_4600;
BgL_auxz00_4600 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12398L), BGl_string2575z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_g1169z00_1649); 
FAILURE(BgL_auxz00_4600,BFALSE,BFALSE);} 
BgL_bytez00_2832 = 
(unsigned char)CINT(BgL_tmpz00_4597); } 
return 
bgl_display_char(BgL_bytez00_2832, BgL_portz00_1652);} } } break;case 2L : 

{ /* Ieee/output.scm 322 */
 obj_t BgL_portz00_1653;
BgL_portz00_1653 = 
VECTOR_REF(BgL_opt1167z00_17,1L); 
{ /* Ieee/output.scm 322 */

{ /* Ieee/output.scm 322 */
 unsigned char BgL_bytez00_2835;
{ /* Ieee/output.scm 322 */
 obj_t BgL_tmpz00_4607;
if(
INTEGERP(BgL_g1169z00_1649))
{ /* Ieee/output.scm 322 */
BgL_tmpz00_4607 = BgL_g1169z00_1649
; }  else 
{ 
 obj_t BgL_auxz00_4610;
BgL_auxz00_4610 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12398L), BGl_string2575z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_g1169z00_1649); 
FAILURE(BgL_auxz00_4610,BFALSE,BFALSE);} 
BgL_bytez00_2835 = 
(unsigned char)CINT(BgL_tmpz00_4607); } 
{ /* Ieee/output.scm 323 */
 obj_t BgL_portz00_2837;
if(
OUTPUT_PORTP(BgL_portz00_1653))
{ /* Ieee/output.scm 323 */
BgL_portz00_2837 = BgL_portz00_1653; }  else 
{ 
 obj_t BgL_auxz00_4617;
BgL_auxz00_4617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12486L), BGl_string2575z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_1653); 
FAILURE(BgL_auxz00_4617,BFALSE,BFALSE);} 
return 
bgl_display_char(BgL_bytez00_2835, BgL_portz00_2837);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2573z00zz__r4_output_6_10_3z00, BGl_string2569z00zz__r4_output_6_10_3z00, 
BINT(
VECTOR_LENGTH(BgL_opt1167z00_17)));} } } } 

}



/* write-byte */
BGL_EXPORTED_DEF obj_t BGl_writezd2bytezd2zz__r4_output_6_10_3z00(unsigned char BgL_bytez00_15, obj_t BgL_portz00_16)
{
{ /* Ieee/output.scm 322 */
{ /* Ieee/output.scm 323 */
 obj_t BgL_portz00_2839;
if(
OUTPUT_PORTP(BgL_portz00_16))
{ /* Ieee/output.scm 323 */
BgL_portz00_2839 = BgL_portz00_16; }  else 
{ 
 obj_t BgL_auxz00_4629;
BgL_auxz00_4629 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12486L), BGl_string2574z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_16); 
FAILURE(BgL_auxz00_4629,BFALSE,BFALSE);} 
return 
bgl_display_char(BgL_bytez00_15, BgL_portz00_2839);} } 

}



/* write-byte-2 */
BGL_EXPORTED_DEF obj_t BGl_writezd2bytezd22z00zz__r4_output_6_10_3z00(unsigned char BgL_bytez00_19, obj_t BgL_portz00_20)
{
{ /* Ieee/output.scm 328 */
return 
bgl_display_char(BgL_bytez00_19, BgL_portz00_20);} 

}



/* &write-byte-2 */
obj_t BGl_z62writezd2bytezd22z62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3895, obj_t BgL_bytez00_3896, obj_t BgL_portz00_3897)
{
{ /* Ieee/output.scm 328 */
{ /* Ieee/output.scm 329 */
 obj_t BgL_auxz00_4644; unsigned char BgL_auxz00_4635;
if(
OUTPUT_PORTP(BgL_portz00_3897))
{ /* Ieee/output.scm 329 */
BgL_auxz00_4644 = BgL_portz00_3897
; }  else 
{ 
 obj_t BgL_auxz00_4647;
BgL_auxz00_4647 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12779L), BGl_string2577z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3897); 
FAILURE(BgL_auxz00_4647,BFALSE,BFALSE);} 
{ /* Ieee/output.scm 329 */
 obj_t BgL_tmpz00_4636;
if(
INTEGERP(BgL_bytez00_3896))
{ /* Ieee/output.scm 329 */
BgL_tmpz00_4636 = BgL_bytez00_3896
; }  else 
{ 
 obj_t BgL_auxz00_4639;
BgL_auxz00_4639 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(12779L), BGl_string2577z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_bytez00_3896); 
FAILURE(BgL_auxz00_4639,BFALSE,BFALSE);} 
BgL_auxz00_4635 = 
(unsigned char)CINT(BgL_tmpz00_4636); } 
return 
BGl_writezd2bytezd22z00zz__r4_output_6_10_3z00(BgL_auxz00_4635, BgL_auxz00_4644);} } 

}



/* illegal-char-rep */
BGL_EXPORTED_DEF obj_t BGl_illegalzd2charzd2repz00zz__r4_output_6_10_3z00(unsigned char BgL_charz00_21)
{
{ /* Ieee/output.scm 334 */
{ /* Ieee/output.scm 335 */
 bool_t BgL_test2926z00_4652;
if(
isalpha(BgL_charz00_21))
{ /* Ieee/output.scm 335 */
BgL_test2926z00_4652 = ((bool_t)1)
; }  else 
{ /* Ieee/output.scm 335 */
BgL_test2926z00_4652 = 
isdigit(BgL_charz00_21)
; } 
if(BgL_test2926z00_4652)
{ /* Ieee/output.scm 335 */
return 
BCHAR(BgL_charz00_21);}  else 
{ 

switch( BgL_charz00_21) { case ((unsigned char)10) : 

return BGl_string2578z00zz__r4_output_6_10_3z00;break;case ((unsigned char)13) : 

return BGl_string2579z00zz__r4_output_6_10_3z00;break;case ((unsigned char)' ') : 

return BGl_string2580z00zz__r4_output_6_10_3z00;break;case ((unsigned char)9) : 

return BGl_string2581z00zz__r4_output_6_10_3z00;break;
default: 
{ /* Ieee/output.scm 347 */
 long BgL_iz00_1660;
BgL_iz00_1660 = 
(BgL_charz00_21); 
if(
(BgL_iz00_1660<33L))
{ /* Ieee/output.scm 348 */
BGL_TAIL return 
bgl_ill_char_rep(BgL_charz00_21);}  else 
{ /* Ieee/output.scm 348 */
return 
BCHAR(BgL_charz00_21);} } } } } } 

}



/* &illegal-char-rep */
obj_t BGl_z62illegalzd2charzd2repz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3898, obj_t BgL_charz00_3899)
{
{ /* Ieee/output.scm 334 */
{ /* Ieee/output.scm 335 */
 unsigned char BgL_auxz00_4663;
{ /* Ieee/output.scm 335 */
 obj_t BgL_tmpz00_4664;
if(
CHARP(BgL_charz00_3899))
{ /* Ieee/output.scm 335 */
BgL_tmpz00_4664 = BgL_charz00_3899
; }  else 
{ 
 obj_t BgL_auxz00_4667;
BgL_auxz00_4667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(13064L), BGl_string2582z00zz__r4_output_6_10_3z00, BGl_string2571z00zz__r4_output_6_10_3z00, BgL_charz00_3899); 
FAILURE(BgL_auxz00_4667,BFALSE,BFALSE);} 
BgL_auxz00_4663 = 
CCHAR(BgL_tmpz00_4664); } 
return 
BGl_illegalzd2charzd2repz00zz__r4_output_6_10_3z00(BgL_auxz00_4663);} } 

}



/* print */
BGL_EXPORTED_DEF obj_t BGl_printz00zz__r4_output_6_10_3z00(obj_t BgL_objz00_22)
{
{ /* Ieee/output.scm 355 */
{ /* Ieee/output.scm 356 */
 obj_t BgL_portz00_1663;
{ /* Ieee/output.scm 356 */
 obj_t BgL_tmpz00_4673;
BgL_tmpz00_4673 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1663 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4673); } 
{ 
 obj_t BgL_lz00_1666; obj_t BgL_resz00_1667;
BgL_lz00_1666 = BgL_objz00_22; 
BgL_resz00_1667 = BNIL; 
BgL_zc3z04anonymousza31328ze3z87_1668:
if(
NULLP(BgL_lz00_1666))
{ /* Ieee/output.scm 359 */
bgl_display_char(((unsigned char)10), BgL_portz00_1663); 
return BgL_resz00_1667;}  else 
{ /* Ieee/output.scm 363 */
 obj_t BgL_vz00_1670;
{ /* Ieee/output.scm 363 */
 obj_t BgL_pairz00_2845;
if(
PAIRP(BgL_lz00_1666))
{ /* Ieee/output.scm 363 */
BgL_pairz00_2845 = BgL_lz00_1666; }  else 
{ 
 obj_t BgL_auxz00_4681;
BgL_auxz00_4681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(13804L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1666); 
FAILURE(BgL_auxz00_4681,BFALSE,BFALSE);} 
BgL_vz00_1670 = 
CAR(BgL_pairz00_2845); } 
bgl_display_obj(BgL_vz00_1670, BgL_portz00_1663); 
{ /* Ieee/output.scm 365 */
 obj_t BgL_arg1331z00_1671;
{ /* Ieee/output.scm 365 */
 obj_t BgL_pairz00_2846;
if(
PAIRP(BgL_lz00_1666))
{ /* Ieee/output.scm 365 */
BgL_pairz00_2846 = BgL_lz00_1666; }  else 
{ 
 obj_t BgL_auxz00_4689;
BgL_auxz00_4689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(13843L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1666); 
FAILURE(BgL_auxz00_4689,BFALSE,BFALSE);} 
BgL_arg1331z00_1671 = 
CDR(BgL_pairz00_2846); } 
{ 
 obj_t BgL_resz00_4695; obj_t BgL_lz00_4694;
BgL_lz00_4694 = BgL_arg1331z00_1671; 
BgL_resz00_4695 = BgL_vz00_1670; 
BgL_resz00_1667 = BgL_resz00_4695; 
BgL_lz00_1666 = BgL_lz00_4694; 
goto BgL_zc3z04anonymousza31328ze3z87_1668;} } } } } } 

}



/* &print */
obj_t BGl_z62printz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3900, obj_t BgL_objz00_3901)
{
{ /* Ieee/output.scm 355 */
return 
BGl_printz00zz__r4_output_6_10_3z00(BgL_objz00_3901);} 

}



/* display* */
BGL_EXPORTED_DEF obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t BgL_objz00_23)
{
{ /* Ieee/output.scm 370 */
{ /* Ieee/output.scm 371 */
 obj_t BgL_portz00_1673;
{ /* Ieee/output.scm 371 */
 obj_t BgL_tmpz00_4697;
BgL_tmpz00_4697 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1673 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4697); } 
{ 
 obj_t BgL_lz00_1675;
BgL_lz00_1675 = BgL_objz00_23; 
BgL_zc3z04anonymousza31332ze3z87_1676:
if(
NULLP(BgL_lz00_1675))
{ /* Ieee/output.scm 373 */
return BUNSPEC;}  else 
{ /* Ieee/output.scm 373 */
{ /* Ieee/output.scm 376 */
 obj_t BgL_arg1334z00_1679;
{ /* Ieee/output.scm 376 */
 obj_t BgL_pairz00_2849;
if(
PAIRP(BgL_lz00_1675))
{ /* Ieee/output.scm 376 */
BgL_pairz00_2849 = BgL_lz00_1675; }  else 
{ 
 obj_t BgL_auxz00_4704;
BgL_auxz00_4704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(14243L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1675); 
FAILURE(BgL_auxz00_4704,BFALSE,BFALSE);} 
BgL_arg1334z00_1679 = 
CAR(BgL_pairz00_2849); } 
bgl_display_obj(BgL_arg1334z00_1679, BgL_portz00_1673); } 
{ /* Ieee/output.scm 377 */
 obj_t BgL_arg1335z00_1680;
{ /* Ieee/output.scm 377 */
 obj_t BgL_pairz00_2850;
if(
PAIRP(BgL_lz00_1675))
{ /* Ieee/output.scm 377 */
BgL_pairz00_2850 = BgL_lz00_1675; }  else 
{ 
 obj_t BgL_auxz00_4712;
BgL_auxz00_4712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(14265L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1675); 
FAILURE(BgL_auxz00_4712,BFALSE,BFALSE);} 
BgL_arg1335z00_1680 = 
CDR(BgL_pairz00_2850); } 
{ 
 obj_t BgL_lz00_4717;
BgL_lz00_4717 = BgL_arg1335z00_1680; 
BgL_lz00_1675 = BgL_lz00_4717; 
goto BgL_zc3z04anonymousza31332ze3z87_1676;} } } } } } 

}



/* &display* */
obj_t BGl_z62displayza2zc0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3902, obj_t BgL_objz00_3903)
{
{ /* Ieee/output.scm 370 */
return 
BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_objz00_3903);} 

}



/* write* */
BGL_EXPORTED_DEF obj_t BGl_writeza2za2zz__r4_output_6_10_3z00(obj_t BgL_objz00_24)
{
{ /* Ieee/output.scm 382 */
{ /* Ieee/output.scm 383 */
 obj_t BgL_portz00_1682;
{ /* Ieee/output.scm 383 */
 obj_t BgL_tmpz00_4719;
BgL_tmpz00_4719 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1682 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4719); } 
{ 
 obj_t BgL_lz00_1684;
BgL_lz00_1684 = BgL_objz00_24; 
BgL_zc3z04anonymousza31336ze3z87_1685:
if(
NULLP(BgL_lz00_1684))
{ /* Ieee/output.scm 385 */
return BUNSPEC;}  else 
{ /* Ieee/output.scm 385 */
{ /* Ieee/output.scm 388 */
 obj_t BgL_arg1338z00_1688;
{ /* Ieee/output.scm 388 */
 obj_t BgL_pairz00_2853;
if(
PAIRP(BgL_lz00_1684))
{ /* Ieee/output.scm 388 */
BgL_pairz00_2853 = BgL_lz00_1684; }  else 
{ 
 obj_t BgL_auxz00_4726;
BgL_auxz00_4726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(14661L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1684); 
FAILURE(BgL_auxz00_4726,BFALSE,BFALSE);} 
BgL_arg1338z00_1688 = 
CAR(BgL_pairz00_2853); } 
bgl_write_obj(BgL_arg1338z00_1688, BgL_portz00_1682); } 
{ /* Ieee/output.scm 389 */
 obj_t BgL_arg1339z00_1689;
{ /* Ieee/output.scm 389 */
 obj_t BgL_pairz00_2854;
if(
PAIRP(BgL_lz00_1684))
{ /* Ieee/output.scm 389 */
BgL_pairz00_2854 = BgL_lz00_1684; }  else 
{ 
 obj_t BgL_auxz00_4734;
BgL_auxz00_4734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(14683L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1684); 
FAILURE(BgL_auxz00_4734,BFALSE,BFALSE);} 
BgL_arg1339z00_1689 = 
CDR(BgL_pairz00_2854); } 
{ 
 obj_t BgL_lz00_4739;
BgL_lz00_4739 = BgL_arg1339z00_1689; 
BgL_lz00_1684 = BgL_lz00_4739; 
goto BgL_zc3z04anonymousza31336ze3z87_1685;} } } } } } 

}



/* &write* */
obj_t BGl_z62writeza2zc0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3904, obj_t BgL_objz00_3905)
{
{ /* Ieee/output.scm 382 */
return 
BGl_writeza2za2zz__r4_output_6_10_3z00(BgL_objz00_3905);} 

}



/* tprint */
BGL_EXPORTED_DEF obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t BgL_portz00_25, obj_t BgL_objz00_26)
{
{ /* Ieee/output.scm 399 */
{ /* Ieee/output.scm 400 */
 obj_t BgL_top2940z00_4742;
BgL_top2940z00_4742 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00); 
BGL_EXITD_PUSH_PROTECT(BgL_top2940z00_4742, BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00); BUNSPEC; 
{ /* Ieee/output.scm 400 */
 obj_t BgL_tmp2939z00_4741;
{ /* Ieee/output.scm 401 */
 obj_t BgL_runner1342z00_1693;
{ /* Ieee/output.scm 401 */
 obj_t BgL_list1340z00_1691;
BgL_list1340z00_1691 = 
MAKE_YOUNG_PAIR(BgL_objz00_26, BNIL); 
BgL_runner1342z00_1693 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_portz00_25, BgL_list1340z00_1691); } 
{ /* Ieee/output.scm 401 */
 obj_t BgL_aux1341z00_1692;
{ /* Ieee/output.scm 401 */
 obj_t BgL_pairz00_2855;
{ /* Ieee/output.scm 401 */
 obj_t BgL_aux2308z00_4053;
BgL_aux2308z00_4053 = BgL_runner1342z00_1693; 
if(
PAIRP(BgL_aux2308z00_4053))
{ /* Ieee/output.scm 401 */
BgL_pairz00_2855 = BgL_aux2308z00_4053; }  else 
{ 
 obj_t BgL_auxz00_4750;
BgL_auxz00_4750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15245L), BGl_string2559z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_aux2308z00_4053); 
FAILURE(BgL_auxz00_4750,BFALSE,BFALSE);} } 
{ /* Ieee/output.scm 401 */
 obj_t BgL_aux2310z00_4055;
BgL_aux2310z00_4055 = 
CAR(BgL_pairz00_2855); 
if(
OUTPUT_PORTP(BgL_aux2310z00_4055))
{ /* Ieee/output.scm 401 */
BgL_aux1341z00_1692 = BgL_aux2310z00_4055; }  else 
{ 
 obj_t BgL_auxz00_4757;
BgL_auxz00_4757 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15245L), BGl_string2559z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_aux2310z00_4055); 
FAILURE(BgL_auxz00_4757,BFALSE,BFALSE);} } } 
{ /* Ieee/output.scm 401 */
 obj_t BgL_pairz00_2856;
{ /* Ieee/output.scm 401 */
 obj_t BgL_aux2312z00_4057;
BgL_aux2312z00_4057 = BgL_runner1342z00_1693; 
if(
PAIRP(BgL_aux2312z00_4057))
{ /* Ieee/output.scm 401 */
BgL_pairz00_2856 = BgL_aux2312z00_4057; }  else 
{ 
 obj_t BgL_auxz00_4763;
BgL_auxz00_4763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15245L), BGl_string2559z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_aux2312z00_4057); 
FAILURE(BgL_auxz00_4763,BFALSE,BFALSE);} } 
BgL_runner1342z00_1693 = 
CDR(BgL_pairz00_2856); } 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_aux1341z00_1692, BgL_runner1342z00_1693); } } 
BgL_tmp2939z00_4741 = 
bgl_flush_output_port(BgL_portz00_25); 
BGL_EXITD_POP_PROTECT(BgL_top2940z00_4742); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_tprintzd2mutexzd2zz__r4_output_6_10_3z00); 
return BgL_tmp2939z00_4741;} } } 

}



/* &tprint */
obj_t BGl_z62tprintz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3906, obj_t BgL_portz00_3907, obj_t BgL_objz00_3908)
{
{ /* Ieee/output.scm 399 */
{ /* Ieee/output.scm 400 */
 obj_t BgL_auxz00_4772;
if(
OUTPUT_PORTP(BgL_portz00_3907))
{ /* Ieee/output.scm 400 */
BgL_auxz00_4772 = BgL_portz00_3907
; }  else 
{ 
 obj_t BgL_auxz00_4775;
BgL_auxz00_4775 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15213L), BGl_string2585z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3907); 
FAILURE(BgL_auxz00_4775,BFALSE,BFALSE);} 
return 
BGl_tprintz00zz__r4_output_6_10_3z00(BgL_auxz00_4772, BgL_objz00_3908);} } 

}



/* fprint */
BGL_EXPORTED_DEF obj_t BGl_fprintz00zz__r4_output_6_10_3z00(obj_t BgL_portz00_27, obj_t BgL_objz00_28)
{
{ /* Ieee/output.scm 407 */
{ 
 obj_t BgL_lz00_1696; obj_t BgL_resz00_1697;
BgL_lz00_1696 = BgL_objz00_28; 
BgL_resz00_1697 = BNIL; 
BgL_zc3z04anonymousza31343ze3z87_1698:
if(
NULLP(BgL_lz00_1696))
{ /* Ieee/output.scm 410 */
bgl_display_char(((unsigned char)10), BgL_portz00_27); 
return BgL_resz00_1697;}  else 
{ /* Ieee/output.scm 414 */
 obj_t BgL_vz00_1700;
{ /* Ieee/output.scm 414 */
 obj_t BgL_pairz00_2858;
if(
PAIRP(BgL_lz00_1696))
{ /* Ieee/output.scm 414 */
BgL_pairz00_2858 = BgL_lz00_1696; }  else 
{ 
 obj_t BgL_auxz00_4785;
BgL_auxz00_4785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15688L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1696); 
FAILURE(BgL_auxz00_4785,BFALSE,BFALSE);} 
BgL_vz00_1700 = 
CAR(BgL_pairz00_2858); } 
{ /* Ieee/output.scm 415 */
 obj_t BgL_arg1346z00_1701;
{ /* Ieee/output.scm 415 */
 obj_t BgL_pairz00_2859;
if(
PAIRP(BgL_lz00_1696))
{ /* Ieee/output.scm 415 */
BgL_pairz00_2859 = BgL_lz00_1696; }  else 
{ 
 obj_t BgL_auxz00_4792;
BgL_auxz00_4792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15715L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1696); 
FAILURE(BgL_auxz00_4792,BFALSE,BFALSE);} 
BgL_arg1346z00_1701 = 
CAR(BgL_pairz00_2859); } 
bgl_display_obj(BgL_arg1346z00_1701, BgL_portz00_27); } 
{ /* Ieee/output.scm 416 */
 obj_t BgL_arg1347z00_1702;
{ /* Ieee/output.scm 416 */
 obj_t BgL_pairz00_2860;
if(
PAIRP(BgL_lz00_1696))
{ /* Ieee/output.scm 416 */
BgL_pairz00_2860 = BgL_lz00_1696; }  else 
{ 
 obj_t BgL_auxz00_4800;
BgL_auxz00_4800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15741L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1696); 
FAILURE(BgL_auxz00_4800,BFALSE,BFALSE);} 
BgL_arg1347z00_1702 = 
CDR(BgL_pairz00_2860); } 
{ 
 obj_t BgL_resz00_4806; obj_t BgL_lz00_4805;
BgL_lz00_4805 = BgL_arg1347z00_1702; 
BgL_resz00_4806 = BgL_vz00_1700; 
BgL_resz00_1697 = BgL_resz00_4806; 
BgL_lz00_1696 = BgL_lz00_4805; 
goto BgL_zc3z04anonymousza31343ze3z87_1698;} } } } } 

}



/* &fprint */
obj_t BGl_z62fprintz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3909, obj_t BgL_portz00_3910, obj_t BgL_objz00_3911)
{
{ /* Ieee/output.scm 407 */
{ /* Ieee/output.scm 410 */
 obj_t BgL_auxz00_4807;
if(
OUTPUT_PORTP(BgL_portz00_3910))
{ /* Ieee/output.scm 410 */
BgL_auxz00_4807 = BgL_portz00_3910
; }  else 
{ 
 obj_t BgL_auxz00_4810;
BgL_auxz00_4810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(15599L), BGl_string2586z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3910); 
FAILURE(BgL_auxz00_4810,BFALSE,BFALSE);} 
return 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_auxz00_4807, BgL_objz00_3911);} } 

}



/* xprintf */
obj_t BGl_xprintfz00zz__r4_output_6_10_3z00(obj_t BgL_procnamez00_29, obj_t BgL_pz00_30, obj_t BgL__fmtz00_31, obj_t BgL_objsz00_32)
{
{ /* Ieee/output.scm 421 */
{ /* Ieee/output.scm 422 */
 long BgL_lenz00_1704;
BgL_lenz00_1704 = 
STRING_LENGTH(BgL__fmtz00_31); 
{ 
 obj_t BgL_iz00_1706; obj_t BgL_osz00_1707;
BgL_iz00_1706 = 
BINT(0L); 
BgL_osz00_1707 = BgL_objsz00_32; 
BgL_zc3z04anonymousza31348ze3z87_1708:
{ 
 long BgL_iz00_1767; obj_t BgL_lz00_1768; obj_t BgL_pz00_1769; obj_t BgL_iz00_1774; obj_t BgL_numz00_1775; long BgL_mincolz00_1776; unsigned char BgL_paddingz00_1777; long BgL_iz00_1788; obj_t BgL_numz00_1789; obj_t BgL_pz00_1790; unsigned char BgL_fz00_1807; long BgL_iz00_1808; bool_t BgL_altzf3zf3_1809;
{ /* Ieee/output.scm 559 */
 bool_t BgL_test2950z00_4816;
{ /* Ieee/output.scm 559 */
 long BgL_n1z00_2965;
{ /* Ieee/output.scm 559 */
 obj_t BgL_tmpz00_4817;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 559 */
BgL_tmpz00_4817 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4820;
BgL_auxz00_4820 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19696L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4820,BFALSE,BFALSE);} 
BgL_n1z00_2965 = 
(long)CINT(BgL_tmpz00_4817); } 
BgL_test2950z00_4816 = 
(BgL_n1z00_2965<BgL_lenz00_1704); } 
if(BgL_test2950z00_4816)
{ /* Ieee/output.scm 560 */
 unsigned char BgL_cz00_1717;
{ /* Ieee/output.scm 560 */
 long BgL_kz00_2968;
{ /* Ieee/output.scm 560 */
 obj_t BgL_tmpz00_4826;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 560 */
BgL_tmpz00_4826 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4829;
BgL_auxz00_4829 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19735L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4829,BFALSE,BFALSE);} 
BgL_kz00_2968 = 
(long)CINT(BgL_tmpz00_4826); } 
{ /* Ieee/output.scm 560 */
 long BgL_l2229z00_3974;
BgL_l2229z00_3974 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_kz00_2968, BgL_l2229z00_3974))
{ /* Ieee/output.scm 560 */
BgL_cz00_1717 = 
STRING_REF(BgL__fmtz00_31, BgL_kz00_2968); }  else 
{ 
 obj_t BgL_auxz00_4838;
BgL_auxz00_4838 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19718L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2229z00_3974), 
(int)(BgL_kz00_2968)); 
FAILURE(BgL_auxz00_4838,BFALSE,BFALSE);} } } 
if(
(BgL_cz00_1717==((unsigned char)'~')))
{ /* Ieee/output.scm 563 */
 bool_t BgL_test2955z00_4846;
{ /* Ieee/output.scm 563 */
 long BgL_n1z00_2972;
{ /* Ieee/output.scm 563 */
 obj_t BgL_tmpz00_4847;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 563 */
BgL_tmpz00_4847 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4850;
BgL_auxz00_4850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19788L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4850,BFALSE,BFALSE);} 
BgL_n1z00_2972 = 
(long)CINT(BgL_tmpz00_4847); } 
BgL_test2955z00_4846 = 
(BgL_n1z00_2972==
(BgL_lenz00_1704-1L)); } 
if(BgL_test2955z00_4846)
{ /* Ieee/output.scm 566 */
 obj_t BgL_arg1354z00_1721;
{ /* Ieee/output.scm 566 */
 long BgL_auxz00_4857;
{ /* Ieee/output.scm 566 */
 obj_t BgL_tmpz00_4858;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 566 */
BgL_tmpz00_4858 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4861;
BgL_auxz00_4861 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19857L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4861,BFALSE,BFALSE);} 
BgL_auxz00_4857 = 
(long)CINT(BgL_tmpz00_4858); } 
BgL_arg1354z00_1721 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL__fmtz00_31, BgL_auxz00_4857, BgL_lenz00_1704); } 
return 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2602z00zz__r4_output_6_10_3z00, BgL_arg1354z00_1721);}  else 
{ /* Ieee/output.scm 567 */
 bool_t BgL_test2958z00_4868;
{ /* Ieee/output.scm 567 */
 unsigned char BgL_tmpz00_4869;
{ /* Ieee/output.scm 567 */
 long BgL_i2232z00_3977;
{ /* Ieee/output.scm 567 */
 long BgL_za71za7_2974;
{ /* Ieee/output.scm 567 */
 obj_t BgL_tmpz00_4870;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 567 */
BgL_tmpz00_4870 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4873;
BgL_auxz00_4873 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19926L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4873,BFALSE,BFALSE);} 
BgL_za71za7_2974 = 
(long)CINT(BgL_tmpz00_4870); } 
BgL_i2232z00_3977 = 
(BgL_za71za7_2974+1L); } 
{ /* Ieee/output.scm 567 */
 long BgL_l2233z00_3978;
BgL_l2233z00_3978 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_i2232z00_3977, BgL_l2233z00_3978))
{ /* Ieee/output.scm 567 */
BgL_tmpz00_4869 = 
STRING_REF(BgL__fmtz00_31, BgL_i2232z00_3977)
; }  else 
{ 
 obj_t BgL_auxz00_4883;
BgL_auxz00_4883 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19904L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2233z00_3978), 
(int)(BgL_i2232z00_3977)); 
FAILURE(BgL_auxz00_4883,BFALSE,BFALSE);} } } 
BgL_test2958z00_4868 = 
(((unsigned char)':')==BgL_tmpz00_4869); } 
if(BgL_test2958z00_4868)
{ /* Ieee/output.scm 568 */
 bool_t BgL_test2961z00_4890;
{ /* Ieee/output.scm 568 */
 long BgL_n1z00_2980;
{ /* Ieee/output.scm 568 */
 obj_t BgL_tmpz00_4891;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 568 */
BgL_tmpz00_4891 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4894;
BgL_auxz00_4894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19945L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4894,BFALSE,BFALSE);} 
BgL_n1z00_2980 = 
(long)CINT(BgL_tmpz00_4891); } 
BgL_test2961z00_4890 = 
(BgL_n1z00_2980==
(BgL_lenz00_1704-2L)); } 
if(BgL_test2961z00_4890)
{ /* Ieee/output.scm 571 */
 obj_t BgL_arg1361z00_1727;
{ /* Ieee/output.scm 571 */
 long BgL_auxz00_4901;
{ /* Ieee/output.scm 571 */
 obj_t BgL_tmpz00_4902;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 571 */
BgL_tmpz00_4902 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4905;
BgL_auxz00_4905 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20026L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4905,BFALSE,BFALSE);} 
BgL_auxz00_4901 = 
(long)CINT(BgL_tmpz00_4902); } 
BgL_arg1361z00_1727 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL__fmtz00_31, BgL_auxz00_4901, BgL_lenz00_1704); } 
return 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2602z00zz__r4_output_6_10_3z00, BgL_arg1361z00_1727);}  else 
{ /* Ieee/output.scm 572 */
 unsigned char BgL_arg1362z00_1728; long BgL_arg1363z00_1729;
{ /* Ieee/output.scm 572 */
 long BgL_i2236z00_3981;
{ /* Ieee/output.scm 572 */
 long BgL_za71za7_2982;
{ /* Ieee/output.scm 572 */
 obj_t BgL_tmpz00_4912;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 572 */
BgL_tmpz00_4912 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4915;
BgL_auxz00_4915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20091L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4915,BFALSE,BFALSE);} 
BgL_za71za7_2982 = 
(long)CINT(BgL_tmpz00_4912); } 
BgL_i2236z00_3981 = 
(BgL_za71za7_2982+2L); } 
{ /* Ieee/output.scm 572 */
 long BgL_l2237z00_3982;
BgL_l2237z00_3982 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_i2236z00_3981, BgL_l2237z00_3982))
{ /* Ieee/output.scm 572 */
BgL_arg1362z00_1728 = 
STRING_REF(BgL__fmtz00_31, BgL_i2236z00_3981); }  else 
{ 
 obj_t BgL_auxz00_4925;
BgL_auxz00_4925 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20069L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2237z00_3982), 
(int)(BgL_i2236z00_3981)); 
FAILURE(BgL_auxz00_4925,BFALSE,BFALSE);} } } 
{ /* Ieee/output.scm 573 */
 long BgL_za71za7_2985;
{ /* Ieee/output.scm 573 */
 obj_t BgL_tmpz00_4931;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 573 */
BgL_tmpz00_4931 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_4934;
BgL_auxz00_4934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20112L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_4934,BFALSE,BFALSE);} 
BgL_za71za7_2985 = 
(long)CINT(BgL_tmpz00_4931); } 
BgL_arg1363z00_1729 = 
(BgL_za71za7_2985+2L); } 
BgL_fz00_1807 = BgL_arg1362z00_1728; 
BgL_iz00_1808 = BgL_arg1363z00_1729; 
BgL_altzf3zf3_1809 = ((bool_t)1); 
BgL_zc3z04anonymousza31418ze3z87_1810:
{ 

switch( BgL_fz00_1807) { case ((unsigned char)'a') : 
case ((unsigned char)'A') : 

if(BgL_altzf3zf3_1809)
{ /* Ieee/output.scm 503 */
 obj_t BgL_arg1420z00_1814;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1420z00_1814 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2902;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2902 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_4947;
BgL_auxz00_4947 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_4947,BFALSE,BFALSE);} 
BgL_arg1420z00_1814 = 
CAR(BgL_pairz00_2902); } 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1420z00_1814, BgL_pz00_30); }  else 
{ /* Ieee/output.scm 504 */
 obj_t BgL_arg1421z00_1815;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1421z00_1815 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2904;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2904 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_4959;
BgL_auxz00_4959 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_4959,BFALSE,BFALSE);} 
BgL_arg1421z00_1815 = 
CAR(BgL_pairz00_2904); } 
bgl_display_obj(BgL_arg1421z00_1815, BgL_pz00_30); } 
{ /* Ieee/output.scm 505 */
 long BgL_arg1422z00_1816; obj_t BgL_arg1423z00_1817;
BgL_arg1422z00_1816 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 505 */
 obj_t BgL_pairz00_2906;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 505 */
BgL_pairz00_2906 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_4968;
BgL_auxz00_4968 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18345L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_4968,BFALSE,BFALSE);} 
BgL_arg1423z00_1817 = 
CDR(BgL_pairz00_2906); } 
{ 
 obj_t BgL_osz00_4975; obj_t BgL_iz00_4973;
BgL_iz00_4973 = 
BINT(BgL_arg1422z00_1816); 
BgL_osz00_4975 = BgL_arg1423z00_1817; 
BgL_osz00_1707 = BgL_osz00_4975; 
BgL_iz00_1706 = BgL_iz00_4973; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'s') : 
case ((unsigned char)'S') : 

if(BgL_altzf3zf3_1809)
{ /* Ieee/output.scm 508 */
 obj_t BgL_arg1424z00_1818;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1424z00_1818 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2908;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2908 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_4983;
BgL_auxz00_4983 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_4983,BFALSE,BFALSE);} 
BgL_arg1424z00_1818 = 
CAR(BgL_pairz00_2908); } 
BGl_writezd2circlezd2zz__pp_circlez00(BgL_arg1424z00_1818, BgL_pz00_30); }  else 
{ /* Ieee/output.scm 509 */
 obj_t BgL_arg1425z00_1819;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1425z00_1819 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2910;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2910 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_4995;
BgL_auxz00_4995 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_4995,BFALSE,BFALSE);} 
BgL_arg1425z00_1819 = 
CAR(BgL_pairz00_2910); } 
{ /* Ieee/output.scm 509 */
 obj_t BgL_list1426z00_1820;
BgL_list1426z00_1820 = 
MAKE_YOUNG_PAIR(BgL_pz00_30, BNIL); 
BGl_writez00zz__r4_output_6_10_3z00(BgL_arg1425z00_1819, BgL_list1426z00_1820); } } 
{ /* Ieee/output.scm 510 */
 long BgL_arg1427z00_1821; obj_t BgL_arg1428z00_1822;
BgL_arg1427z00_1821 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 510 */
 obj_t BgL_pairz00_2912;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 510 */
BgL_pairz00_2912 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5005;
BgL_auxz00_5005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18468L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5005,BFALSE,BFALSE);} 
BgL_arg1428z00_1822 = 
CDR(BgL_pairz00_2912); } 
{ 
 obj_t BgL_osz00_5012; obj_t BgL_iz00_5010;
BgL_iz00_5010 = 
BINT(BgL_arg1427z00_1821); 
BgL_osz00_5012 = BgL_arg1428z00_1822; 
BgL_osz00_1707 = BgL_osz00_5012; 
BgL_iz00_1706 = BgL_iz00_5010; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'v') : 
case ((unsigned char)'V') : 

if(BgL_altzf3zf3_1809)
{ /* Ieee/output.scm 513 */
 obj_t BgL_arg1429z00_1823;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1429z00_1823 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2914;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2914 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5020;
BgL_auxz00_5020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5020,BFALSE,BFALSE);} 
BgL_arg1429z00_1823 = 
CAR(BgL_pairz00_2914); } 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1429z00_1823, BgL_pz00_30); }  else 
{ /* Ieee/output.scm 514 */
 obj_t BgL_arg1430z00_1824;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1430z00_1824 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2916;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2916 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5032;
BgL_auxz00_5032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5032,BFALSE,BFALSE);} 
BgL_arg1430z00_1824 = 
CAR(BgL_pairz00_2916); } 
bgl_display_obj(BgL_arg1430z00_1824, BgL_pz00_30); } 
bgl_display_char(((unsigned char)10), BgL_pz00_30); 
{ /* Ieee/output.scm 516 */
 long BgL_arg1431z00_1825; obj_t BgL_arg1434z00_1826;
BgL_arg1431z00_1825 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 516 */
 obj_t BgL_pairz00_2919;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 516 */
BgL_pairz00_2919 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5042;
BgL_auxz00_5042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18609L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5042,BFALSE,BFALSE);} 
BgL_arg1434z00_1826 = 
CDR(BgL_pairz00_2919); } 
{ 
 obj_t BgL_osz00_5049; obj_t BgL_iz00_5047;
BgL_iz00_5047 = 
BINT(BgL_arg1431z00_1825); 
BgL_osz00_5049 = BgL_arg1434z00_1826; 
BgL_osz00_1707 = BgL_osz00_5049; 
BgL_iz00_1706 = BgL_iz00_5047; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'c') : 
case ((unsigned char)'C') : 

{ /* Ieee/output.scm 518 */
 obj_t BgL_oz00_1827;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_oz00_1827 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2921;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2921 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5056;
BgL_auxz00_5056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5056,BFALSE,BFALSE);} 
BgL_oz00_1827 = 
CAR(BgL_pairz00_2921); } 
if(
CHARP(BgL_oz00_1827))
{ /* Ieee/output.scm 519 */
{ /* Ieee/output.scm 317 */
 unsigned char BgL_tmpz00_5063;
BgL_tmpz00_5063 = 
CCHAR(BgL_oz00_1827); 
bgl_display_char(BgL_tmpz00_5063, BgL_pz00_30); } 
{ /* Ieee/output.scm 523 */
 long BgL_arg1436z00_1829; obj_t BgL_arg1437z00_1830;
BgL_arg1436z00_1829 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 523 */
 obj_t BgL_pairz00_2925;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 523 */
BgL_pairz00_2925 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5069;
BgL_auxz00_5069 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18791L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5069,BFALSE,BFALSE);} 
BgL_arg1437z00_1830 = 
CDR(BgL_pairz00_2925); } 
{ 
 obj_t BgL_osz00_5076; obj_t BgL_iz00_5074;
BgL_iz00_5074 = 
BINT(BgL_arg1436z00_1829); 
BgL_osz00_5076 = BgL_arg1437z00_1830; 
BgL_osz00_1707 = BgL_osz00_5076; 
BgL_iz00_1706 = BgL_iz00_5074; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } }  else 
{ /* Ieee/output.scm 519 */
return 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2592z00zz__r4_output_6_10_3z00, BgL_oz00_1827);} } break;case ((unsigned char)'d') : 
case ((unsigned char)'D') : 

{ /* Ieee/output.scm 525 */
 obj_t BgL_arg1438z00_1831;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1438z00_1831 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2927;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2927 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5084;
BgL_auxz00_5084 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5084,BFALSE,BFALSE);} 
BgL_arg1438z00_1831 = 
CAR(BgL_pairz00_2927); } 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1438z00_1831))
{ /* Ieee/output.scm 432 */
bgl_display_obj(
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1438z00_1831, 
BINT(10L)), BgL_pz00_30); }  else 
{ /* Ieee/output.scm 432 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2593z00zz__r4_output_6_10_3z00, BgL_arg1438z00_1831); } } 
{ /* Ieee/output.scm 526 */
 long BgL_arg1439z00_1832; obj_t BgL_arg1440z00_1833;
BgL_arg1439z00_1832 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 526 */
 obj_t BgL_pairz00_2931;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 526 */
BgL_pairz00_2931 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5098;
BgL_auxz00_5098 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18873L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5098,BFALSE,BFALSE);} 
BgL_arg1440z00_1833 = 
CDR(BgL_pairz00_2931); } 
{ 
 obj_t BgL_osz00_5105; obj_t BgL_iz00_5103;
BgL_iz00_5103 = 
BINT(BgL_arg1439z00_1832); 
BgL_osz00_5105 = BgL_arg1440z00_1833; 
BgL_osz00_1707 = BgL_osz00_5105; 
BgL_iz00_1706 = BgL_iz00_5103; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'x') : 
case ((unsigned char)'X') : 

{ /* Ieee/output.scm 528 */
 obj_t BgL_arg1441z00_1834;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1441z00_1834 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2933;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2933 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5112;
BgL_auxz00_5112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5112,BFALSE,BFALSE);} 
BgL_arg1441z00_1834 = 
CAR(BgL_pairz00_2933); } 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1441z00_1834))
{ /* Ieee/output.scm 432 */
bgl_display_obj(
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1441z00_1834, 
BINT(16L)), BgL_pz00_30); }  else 
{ /* Ieee/output.scm 432 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2593z00zz__r4_output_6_10_3z00, BgL_arg1441z00_1834); } } 
{ /* Ieee/output.scm 529 */
 long BgL_arg1442z00_1835; obj_t BgL_arg1443z00_1836;
BgL_arg1442z00_1835 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 529 */
 obj_t BgL_pairz00_2937;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 529 */
BgL_pairz00_2937 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5126;
BgL_auxz00_5126 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18952L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5126,BFALSE,BFALSE);} 
BgL_arg1443z00_1836 = 
CDR(BgL_pairz00_2937); } 
{ 
 obj_t BgL_osz00_5133; obj_t BgL_iz00_5131;
BgL_iz00_5131 = 
BINT(BgL_arg1442z00_1835); 
BgL_osz00_5133 = BgL_arg1443z00_1836; 
BgL_osz00_1707 = BgL_osz00_5133; 
BgL_iz00_1706 = BgL_iz00_5131; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'o') : 
case ((unsigned char)'O') : 

{ /* Ieee/output.scm 531 */
 obj_t BgL_arg1444z00_1837;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1444z00_1837 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2939;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2939 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5140;
BgL_auxz00_5140 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5140,BFALSE,BFALSE);} 
BgL_arg1444z00_1837 = 
CAR(BgL_pairz00_2939); } 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1444z00_1837))
{ /* Ieee/output.scm 432 */
bgl_display_obj(
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1444z00_1837, 
BINT(8L)), BgL_pz00_30); }  else 
{ /* Ieee/output.scm 432 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2593z00zz__r4_output_6_10_3z00, BgL_arg1444z00_1837); } } 
{ /* Ieee/output.scm 532 */
 long BgL_arg1445z00_1838; obj_t BgL_arg1446z00_1839;
BgL_arg1445z00_1838 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 532 */
 obj_t BgL_pairz00_2943;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 532 */
BgL_pairz00_2943 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5154;
BgL_auxz00_5154 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19030L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5154,BFALSE,BFALSE);} 
BgL_arg1446z00_1839 = 
CDR(BgL_pairz00_2943); } 
{ 
 obj_t BgL_osz00_5161; obj_t BgL_iz00_5159;
BgL_iz00_5159 = 
BINT(BgL_arg1445z00_1838); 
BgL_osz00_5161 = BgL_arg1446z00_1839; 
BgL_osz00_1707 = BgL_osz00_5161; 
BgL_iz00_1706 = BgL_iz00_5159; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'b') : 
case ((unsigned char)'B') : 

{ /* Ieee/output.scm 534 */
 obj_t BgL_arg1447z00_1840;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1447z00_1840 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2945;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2945 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5168;
BgL_auxz00_5168 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5168,BFALSE,BFALSE);} 
BgL_arg1447z00_1840 = 
CAR(BgL_pairz00_2945); } 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_arg1447z00_1840))
{ /* Ieee/output.scm 432 */
bgl_display_obj(
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1447z00_1840, 
BINT(2L)), BgL_pz00_30); }  else 
{ /* Ieee/output.scm 432 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2593z00zz__r4_output_6_10_3z00, BgL_arg1447z00_1840); } } 
{ /* Ieee/output.scm 535 */
 long BgL_arg1448z00_1841; obj_t BgL_arg1449z00_1842;
BgL_arg1448z00_1841 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 535 */
 obj_t BgL_pairz00_2949;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 535 */
BgL_pairz00_2949 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5182;
BgL_auxz00_5182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19108L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5182,BFALSE,BFALSE);} 
BgL_arg1449z00_1842 = 
CDR(BgL_pairz00_2949); } 
{ 
 obj_t BgL_osz00_5189; obj_t BgL_iz00_5187;
BgL_iz00_5187 = 
BINT(BgL_arg1448z00_1841); 
BgL_osz00_5189 = BgL_arg1449z00_1842; 
BgL_osz00_1707 = BgL_osz00_5189; 
BgL_iz00_1706 = BgL_iz00_5187; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'%') : 
case ((unsigned char)'n') : 

bgl_display_char(((unsigned char)10), BgL_pz00_30); 
{ /* Ieee/output.scm 538 */
 long BgL_arg1450z00_1843;
BgL_arg1450z00_1843 = 
(BgL_iz00_1808+1L); 
{ 
 obj_t BgL_iz00_5192;
BgL_iz00_5192 = 
BINT(BgL_arg1450z00_1843); 
BgL_iz00_1706 = BgL_iz00_5192; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'r') : 

bgl_display_char(((unsigned char)13), BgL_pz00_30); 
{ /* Ieee/output.scm 541 */
 long BgL_arg1451z00_1844;
BgL_arg1451z00_1844 = 
(BgL_iz00_1808+1L); 
{ 
 obj_t BgL_iz00_5196;
BgL_iz00_5196 = 
BINT(BgL_arg1451z00_1844); 
BgL_iz00_1706 = BgL_iz00_5196; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'l') : 
case ((unsigned char)'L') : 

{ /* Ieee/output.scm 543 */
 obj_t BgL_arg1452z00_1845;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1452z00_1845 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2956;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2956 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5204;
BgL_auxz00_5204 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5204,BFALSE,BFALSE);} 
BgL_arg1452z00_1845 = 
CAR(BgL_pairz00_2956); } 
BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(BgL_arg1452z00_1845, BgL_pz00_30, BGl_string2594z00zz__r4_output_6_10_3z00); } 
{ /* Ieee/output.scm 544 */
 long BgL_arg1453z00_1846; obj_t BgL_arg1454z00_1847;
BgL_arg1453z00_1846 = 
(BgL_iz00_1808+1L); 
{ /* Ieee/output.scm 544 */
 obj_t BgL_pairz00_2958;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 544 */
BgL_pairz00_2958 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5213;
BgL_auxz00_5213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19314L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5213,BFALSE,BFALSE);} 
BgL_arg1454z00_1847 = 
CDR(BgL_pairz00_2958); } 
{ 
 obj_t BgL_osz00_5220; obj_t BgL_iz00_5218;
BgL_iz00_5218 = 
BINT(BgL_arg1453z00_1846); 
BgL_osz00_5220 = BgL_arg1454z00_1847; 
BgL_osz00_1707 = BgL_osz00_5220; 
BgL_iz00_1706 = BgL_iz00_5218; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;case ((unsigned char)'(') : 

{ /* Ieee/output.scm 546 */
 obj_t BgL_niz00_1848;
{ /* Ieee/output.scm 546 */
 obj_t BgL_arg1456z00_1850;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1456z00_1850 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2960;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2960 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5227;
BgL_auxz00_5227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5227,BFALSE,BFALSE);} 
BgL_arg1456z00_1850 = 
CAR(BgL_pairz00_2960); } 
BgL_iz00_1767 = BgL_iz00_1808; 
BgL_lz00_1768 = BgL_arg1456z00_1850; 
BgL_pz00_1769 = BgL_pz00_30; 
{ /* Ieee/output.scm 452 */
 obj_t BgL_jz00_1771;
BgL_jz00_1771 = 
BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL__fmtz00_31, 
BCHAR(((unsigned char)')')), 
BINT(BgL_iz00_1767)); 
if(
CBOOL(BgL_jz00_1771))
{ /* Ieee/output.scm 455 */
 obj_t BgL_sepz00_1772;
{ /* Ieee/output.scm 455 */
 long BgL_arg1395z00_1773;
BgL_arg1395z00_1773 = 
(BgL_iz00_1767+1L); 
{ /* Ieee/output.scm 455 */
 long BgL_auxz00_5238;
{ /* Ieee/output.scm 455 */
 obj_t BgL_tmpz00_5239;
if(
INTEGERP(BgL_jz00_1771))
{ /* Ieee/output.scm 455 */
BgL_tmpz00_5239 = BgL_jz00_1771
; }  else 
{ 
 obj_t BgL_auxz00_5242;
BgL_auxz00_5242 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16900L), BGl_string2601z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1771); 
FAILURE(BgL_auxz00_5242,BFALSE,BFALSE);} 
BgL_auxz00_5238 = 
(long)CINT(BgL_tmpz00_5239); } 
BgL_sepz00_1772 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL__fmtz00_31, BgL_arg1395z00_1773, BgL_auxz00_5238); } } 
BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(BgL_lz00_1768, BgL_pz00_1769, BgL_sepz00_1772); 
{ 
 obj_t BgL_tmpz00_5249;
if(
INTEGERP(BgL_jz00_1771))
{ /* Ieee/output.scm 457 */
BgL_tmpz00_5249 = BgL_jz00_1771
; }  else 
{ 
 obj_t BgL_auxz00_5252;
BgL_auxz00_5252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16978L), BGl_string2601z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1771); 
FAILURE(BgL_auxz00_5252,BFALSE,BFALSE);} 
BgL_niz00_1848 = 
ADDFX(BgL_tmpz00_5249, 
BINT(1L)); } }  else 
{ /* Ieee/output.scm 453 */
BgL_niz00_1848 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2598z00zz__r4_output_6_10_3z00, BgL__fmtz00_31); } } } 
{ /* Ieee/output.scm 547 */
 obj_t BgL_arg1455z00_1849;
{ /* Ieee/output.scm 547 */
 obj_t BgL_pairz00_2961;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 547 */
BgL_pairz00_2961 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5261;
BgL_auxz00_5261 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19397L), BGl_string2591z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5261,BFALSE,BFALSE);} 
BgL_arg1455z00_1849 = 
CDR(BgL_pairz00_2961); } 
{ 
 obj_t BgL_osz00_5267; obj_t BgL_iz00_5266;
BgL_iz00_5266 = BgL_niz00_1848; 
BgL_osz00_5267 = BgL_arg1455z00_1849; 
BgL_osz00_1707 = BgL_osz00_5267; 
BgL_iz00_1706 = BgL_iz00_5266; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } } break;case ((unsigned char)'~') : 

bgl_display_char(((unsigned char)'~'), BgL_pz00_30); 
{ /* Ieee/output.scm 550 */
 long BgL_arg1457z00_1851;
BgL_arg1457z00_1851 = 
(BgL_iz00_1808+1L); 
{ 
 obj_t BgL_iz00_5270;
BgL_iz00_5270 = 
BINT(BgL_arg1457z00_1851); 
BgL_iz00_1706 = BgL_iz00_5270; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } break;
default: 
if(
isdigit(BgL_fz00_1807))
{ /* Ieee/output.scm 553 */
 obj_t BgL_niz00_1853;
{ /* Ieee/output.scm 553 */
 obj_t BgL_arg1460z00_1855;
if(
NULLP(BgL_osz00_1707))
{ /* Ieee/output.scm 427 */
BgL_arg1460z00_1855 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2587z00zz__r4_output_6_10_3z00, 
BCHAR(BgL_fz00_1807)); }  else 
{ /* Ieee/output.scm 429 */
 obj_t BgL_pairz00_2899;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 429 */
BgL_pairz00_2899 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5280;
BgL_auxz00_5280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16231L), BGl_string2588z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5280,BFALSE,BFALSE);} 
BgL_arg1460z00_1855 = 
CAR(BgL_pairz00_2899); } 
BgL_iz00_1788 = BgL_iz00_1808; 
BgL_numz00_1789 = BgL_arg1460z00_1855; 
BgL_pz00_1790 = BgL_pz00_30; 
if(
BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_numz00_1789))
{ /* Ieee/output.scm 482 */
 obj_t BgL_jz00_1793;
BgL_jz00_1793 = 
BGl_stringzd2skipzd2zz__r4_strings_6_7z00(BgL__fmtz00_31, BGl_string2595z00zz__r4_output_6_10_3z00, 
BINT(BgL_iz00_1788)); 
if(
CBOOL(BgL_jz00_1793))
{ /* Ieee/output.scm 486 */
 bool_t BgL_test3019z00_5291;
{ /* Ieee/output.scm 486 */
 unsigned char BgL_tmpz00_5292;
{ /* Ieee/output.scm 486 */
 long BgL_kz00_2887;
{ /* Ieee/output.scm 486 */
 obj_t BgL_tmpz00_5293;
if(
INTEGERP(BgL_jz00_1793))
{ /* Ieee/output.scm 486 */
BgL_tmpz00_5293 = BgL_jz00_1793
; }  else 
{ 
 obj_t BgL_auxz00_5296;
BgL_auxz00_5296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17839L), BGl_string2596z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1793); 
FAILURE(BgL_auxz00_5296,BFALSE,BFALSE);} 
BgL_kz00_2887 = 
(long)CINT(BgL_tmpz00_5293); } 
{ /* Ieee/output.scm 486 */
 long BgL_l2217z00_3962;
BgL_l2217z00_3962 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_kz00_2887, BgL_l2217z00_3962))
{ /* Ieee/output.scm 486 */
BgL_tmpz00_5292 = 
STRING_REF(BgL__fmtz00_31, BgL_kz00_2887)
; }  else 
{ 
 obj_t BgL_auxz00_5305;
BgL_auxz00_5305 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17822L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2217z00_3962), 
(int)(BgL_kz00_2887)); 
FAILURE(BgL_auxz00_5305,BFALSE,BFALSE);} } } 
BgL_test3019z00_5291 = 
(BgL_tmpz00_5292==((unsigned char)',')); } 
if(BgL_test3019z00_5291)
{ /* Ieee/output.scm 487 */
 bool_t BgL_test3022z00_5312;
{ /* Ieee/output.scm 487 */
 long BgL_n1z00_2891;
{ /* Ieee/output.scm 487 */
 obj_t BgL_tmpz00_5313;
if(
INTEGERP(BgL_jz00_1793))
{ /* Ieee/output.scm 487 */
BgL_tmpz00_5313 = BgL_jz00_1793
; }  else 
{ 
 obj_t BgL_auxz00_5316;
BgL_auxz00_5316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17865L), BGl_string2596z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1793); 
FAILURE(BgL_auxz00_5316,BFALSE,BFALSE);} 
BgL_n1z00_2891 = 
(long)CINT(BgL_tmpz00_5313); } 
BgL_test3022z00_5312 = 
(BgL_n1z00_2891==
(BgL_lenz00_1704-1L)); } 
if(BgL_test3022z00_5312)
{ /* Ieee/output.scm 487 */
BgL_niz00_1853 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2598z00zz__r4_output_6_10_3z00, BgL__fmtz00_31); }  else 
{ /* Ieee/output.scm 489 */
 long BgL_arg1408z00_1798; long BgL_arg1410z00_1799; unsigned char BgL_arg1411z00_1800;
{ /* Ieee/output.scm 489 */
 long BgL_za71za7_2893;
{ /* Ieee/output.scm 489 */
 obj_t BgL_tmpz00_5324;
if(
INTEGERP(BgL_jz00_1793))
{ /* Ieee/output.scm 489 */
BgL_tmpz00_5324 = BgL_jz00_1793
; }  else 
{ 
 obj_t BgL_auxz00_5327;
BgL_auxz00_5327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17954L), BGl_string2596z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1793); 
FAILURE(BgL_auxz00_5327,BFALSE,BFALSE);} 
BgL_za71za7_2893 = 
(long)CINT(BgL_tmpz00_5324); } 
BgL_arg1408z00_1798 = 
(BgL_za71za7_2893+2L); } 
{ /* Ieee/output.scm 491 */
 obj_t BgL_arg1412z00_1801;
{ /* Ieee/output.scm 491 */
 long BgL_auxz00_5333;
{ /* Ieee/output.scm 491 */
 obj_t BgL_tmpz00_5334;
if(
INTEGERP(BgL_jz00_1793))
{ /* Ieee/output.scm 491 */
BgL_tmpz00_5334 = BgL_jz00_1793
; }  else 
{ 
 obj_t BgL_auxz00_5337;
BgL_auxz00_5337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17998L), BGl_string2596z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1793); 
FAILURE(BgL_auxz00_5337,BFALSE,BFALSE);} 
BgL_auxz00_5333 = 
(long)CINT(BgL_tmpz00_5334); } 
BgL_arg1412z00_1801 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL__fmtz00_31, BgL_iz00_1788, BgL_auxz00_5333); } 
{ /* Ieee/output.scm 491 */
 char * BgL_tmpz00_5343;
BgL_tmpz00_5343 = 
BSTRING_TO_STRING(BgL_arg1412z00_1801); 
BgL_arg1410z00_1799 = 
BGL_STRTOL(BgL_tmpz00_5343, 0L, 10L); } } 
{ /* Ieee/output.scm 492 */
 long BgL_i2220z00_3965;
{ /* Ieee/output.scm 492 */
 long BgL_za71za7_2894;
{ /* Ieee/output.scm 492 */
 obj_t BgL_tmpz00_5346;
if(
INTEGERP(BgL_jz00_1793))
{ /* Ieee/output.scm 492 */
BgL_tmpz00_5346 = BgL_jz00_1793
; }  else 
{ 
 obj_t BgL_auxz00_5349;
BgL_auxz00_5349 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18051L), BGl_string2596z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1793); 
FAILURE(BgL_auxz00_5349,BFALSE,BFALSE);} 
BgL_za71za7_2894 = 
(long)CINT(BgL_tmpz00_5346); } 
BgL_i2220z00_3965 = 
(BgL_za71za7_2894+1L); } 
{ /* Ieee/output.scm 492 */
 long BgL_l2221z00_3966;
BgL_l2221z00_3966 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_i2220z00_3965, BgL_l2221z00_3966))
{ /* Ieee/output.scm 492 */
BgL_arg1411z00_1800 = 
STRING_REF(BgL__fmtz00_31, BgL_i2220z00_3965); }  else 
{ 
 obj_t BgL_auxz00_5359;
BgL_auxz00_5359 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18029L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2221z00_3966), 
(int)(BgL_i2220z00_3965)); 
FAILURE(BgL_auxz00_5359,BFALSE,BFALSE);} } } 
BgL_iz00_1774 = 
BINT(BgL_arg1408z00_1798); 
BgL_numz00_1775 = BgL_numz00_1789; 
BgL_mincolz00_1776 = BgL_arg1410z00_1799; 
BgL_paddingz00_1777 = BgL_arg1411z00_1800; 
BgL_zc3z04anonymousza31396ze3z87_1778:
{ /* Ieee/output.scm 460 */
 bool_t BgL_test3028z00_5365;
{ /* Ieee/output.scm 460 */
 long BgL_n1z00_2875;
{ /* Ieee/output.scm 460 */
 obj_t BgL_tmpz00_5366;
if(
INTEGERP(BgL_iz00_1774))
{ /* Ieee/output.scm 460 */
BgL_tmpz00_5366 = BgL_iz00_1774
; }  else 
{ 
 obj_t BgL_auxz00_5369;
BgL_auxz00_5369 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17057L), BGl_string2599z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1774); 
FAILURE(BgL_auxz00_5369,BFALSE,BFALSE);} 
BgL_n1z00_2875 = 
(long)CINT(BgL_tmpz00_5366); } 
BgL_test3028z00_5365 = 
(BgL_n1z00_2875==BgL_lenz00_1704); } 
if(BgL_test3028z00_5365)
{ /* Ieee/output.scm 460 */
BgL_niz00_1853 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2598z00zz__r4_output_6_10_3z00, BgL__fmtz00_31); }  else 
{ /* Ieee/output.scm 462 */
 obj_t BgL_sz00_1780;
{ /* Ieee/output.scm 462 */
 unsigned char BgL_aux1046z00_1786;
{ /* Ieee/output.scm 462 */
 long BgL_kz00_2878;
{ /* Ieee/output.scm 462 */
 obj_t BgL_tmpz00_5376;
if(
INTEGERP(BgL_iz00_1774))
{ /* Ieee/output.scm 462 */
BgL_tmpz00_5376 = BgL_iz00_1774
; }  else 
{ 
 obj_t BgL_auxz00_5379;
BgL_auxz00_5379 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17137L), BGl_string2599z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1774); 
FAILURE(BgL_auxz00_5379,BFALSE,BFALSE);} 
BgL_kz00_2878 = 
(long)CINT(BgL_tmpz00_5376); } 
{ /* Ieee/output.scm 462 */
 long BgL_l2225z00_3970;
BgL_l2225z00_3970 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_kz00_2878, BgL_l2225z00_3970))
{ /* Ieee/output.scm 462 */
BgL_aux1046z00_1786 = 
STRING_REF(BgL__fmtz00_31, BgL_kz00_2878); }  else 
{ 
 obj_t BgL_auxz00_5388;
BgL_auxz00_5388 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17120L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2225z00_3970), 
(int)(BgL_kz00_2878)); 
FAILURE(BgL_auxz00_5388,BFALSE,BFALSE);} } } 
switch( BgL_aux1046z00_1786) { case ((unsigned char)'d') : 
case ((unsigned char)'D') : 

BgL_sz00_1780 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1775, 
BINT(10L)); break;case ((unsigned char)'x') : 
case ((unsigned char)'X') : 

BgL_sz00_1780 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1775, 
BINT(16L)); break;case ((unsigned char)'o') : 
case ((unsigned char)'O') : 

BgL_sz00_1780 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1775, 
BINT(8L)); break;case ((unsigned char)'b') : 
case ((unsigned char)'B') : 

BgL_sz00_1780 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_numz00_1775, 
BINT(2L)); break;
default: 
BgL_sz00_1780 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2598z00zz__r4_output_6_10_3z00, BgL__fmtz00_31); } } 
{ /* Ieee/output.scm 462 */
 long BgL_lz00_1781;
{ /* Ieee/output.scm 473 */
 obj_t BgL_stringz00_2879;
if(
STRINGP(BgL_sz00_1780))
{ /* Ieee/output.scm 473 */
BgL_stringz00_2879 = BgL_sz00_1780; }  else 
{ 
 obj_t BgL_auxz00_5406;
BgL_auxz00_5406 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17424L), BGl_string2599z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_sz00_1780); 
FAILURE(BgL_auxz00_5406,BFALSE,BFALSE);} 
BgL_lz00_1781 = 
STRING_LENGTH(BgL_stringz00_2879); } 
{ /* Ieee/output.scm 473 */

if(
(BgL_lz00_1781<BgL_mincolz00_1776))
{ /* Ieee/output.scm 474 */
bgl_display_obj(
make_string(
(BgL_mincolz00_1776-BgL_lz00_1781), BgL_paddingz00_1777), BgL_pz00_30); }  else 
{ /* Ieee/output.scm 474 */BFALSE; } 
bgl_display_obj(BgL_sz00_1780, BgL_pz00_30); 
{ 
 obj_t BgL_tmpz00_5417;
if(
INTEGERP(BgL_iz00_1774))
{ /* Ieee/output.scm 477 */
BgL_tmpz00_5417 = BgL_iz00_1774
; }  else 
{ 
 obj_t BgL_auxz00_5420;
BgL_auxz00_5420 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(17557L), BGl_string2599z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1774); 
FAILURE(BgL_auxz00_5420,BFALSE,BFALSE);} 
BgL_niz00_1853 = 
ADDFX(BgL_tmpz00_5417, 
BINT(1L)); } } } } } } }  else 
{ /* Ieee/output.scm 496 */
 long BgL_arg1415z00_1804;
{ /* Ieee/output.scm 496 */
 obj_t BgL_arg1416z00_1805;
{ /* Ieee/output.scm 496 */
 long BgL_auxz00_5427;
{ /* Ieee/output.scm 496 */
 obj_t BgL_tmpz00_5428;
if(
INTEGERP(BgL_jz00_1793))
{ /* Ieee/output.scm 496 */
BgL_tmpz00_5428 = BgL_jz00_1793
; }  else 
{ 
 obj_t BgL_auxz00_5431;
BgL_auxz00_5431 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(18137L), BGl_string2596z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_jz00_1793); 
FAILURE(BgL_auxz00_5431,BFALSE,BFALSE);} 
BgL_auxz00_5427 = 
(long)CINT(BgL_tmpz00_5428); } 
BgL_arg1416z00_1805 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL__fmtz00_31, BgL_iz00_1788, BgL_auxz00_5427); } 
{ /* Ieee/output.scm 496 */
 char * BgL_tmpz00_5437;
BgL_tmpz00_5437 = 
BSTRING_TO_STRING(BgL_arg1416z00_1805); 
BgL_arg1415z00_1804 = 
BGL_STRTOL(BgL_tmpz00_5437, 0L, 10L); } } 
{ 
 unsigned char BgL_paddingz00_5443; long BgL_mincolz00_5442; obj_t BgL_numz00_5441; obj_t BgL_iz00_5440;
BgL_iz00_5440 = BgL_jz00_1793; 
BgL_numz00_5441 = BgL_numz00_1789; 
BgL_mincolz00_5442 = BgL_arg1415z00_1804; 
BgL_paddingz00_5443 = ((unsigned char)' '); 
BgL_paddingz00_1777 = BgL_paddingz00_5443; 
BgL_mincolz00_1776 = BgL_mincolz00_5442; 
BgL_numz00_1775 = BgL_numz00_5441; 
BgL_iz00_1774 = BgL_iz00_5440; 
goto BgL_zc3z04anonymousza31396ze3z87_1778;} } }  else 
{ /* Ieee/output.scm 484 */
BgL_niz00_1853 = 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2598z00zz__r4_output_6_10_3z00, BgL__fmtz00_31); } }  else 
{ /* Ieee/output.scm 480 */
BgL_niz00_1853 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procnamez00_29, BGl_string2593z00zz__r4_output_6_10_3z00, BgL_numz00_1789); } } 
{ /* Ieee/output.scm 554 */
 obj_t BgL_arg1459z00_1854;
{ /* Ieee/output.scm 554 */
 obj_t BgL_pairz00_2900;
if(
PAIRP(BgL_osz00_1707))
{ /* Ieee/output.scm 554 */
BgL_pairz00_2900 = BgL_osz00_1707; }  else 
{ 
 obj_t BgL_auxz00_5448;
BgL_auxz00_5448 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(19583L), BGl_string2588z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_osz00_1707); 
FAILURE(BgL_auxz00_5448,BFALSE,BFALSE);} 
BgL_arg1459z00_1854 = 
CDR(BgL_pairz00_2900); } 
{ 
 obj_t BgL_osz00_5454; obj_t BgL_iz00_5453;
BgL_iz00_5453 = BgL_niz00_1853; 
BgL_osz00_5454 = BgL_arg1459z00_1854; 
BgL_osz00_1707 = BgL_osz00_5454; 
BgL_iz00_1706 = BgL_iz00_5453; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } }  else 
{ /* Ieee/output.scm 557 */
 obj_t BgL_arg1461z00_1856;
{ /* Ieee/output.scm 557 */
 obj_t BgL_arg1462z00_1857;
{ /* Ieee/output.scm 557 */
 obj_t BgL_list1463z00_1858;
BgL_list1463z00_1858 = 
MAKE_YOUNG_PAIR(
BCHAR(BgL_fz00_1807), BNIL); 
BgL_arg1462z00_1857 = 
BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_list1463z00_1858); } 
BgL_arg1461z00_1856 = 
string_append_3(BGl_string2589z00zz__r4_output_6_10_3z00, BgL_arg1462z00_1857, BGl_string2590z00zz__r4_output_6_10_3z00); } 
return 
BGl_errorz00zz__errorz00(BgL_procnamez00_29, BgL_arg1461z00_1856, BgL__fmtz00_31);} } } } }  else 
{ /* Ieee/output.scm 576 */
 unsigned char BgL_arg1366z00_1732; long BgL_arg1367z00_1733;
{ /* Ieee/output.scm 576 */
 long BgL_i2240z00_3985;
{ /* Ieee/output.scm 576 */
 long BgL_za71za7_2986;
{ /* Ieee/output.scm 576 */
 obj_t BgL_tmpz00_5461;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 576 */
BgL_tmpz00_5461 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_5464;
BgL_auxz00_5464 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20185L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_5464,BFALSE,BFALSE);} 
BgL_za71za7_2986 = 
(long)CINT(BgL_tmpz00_5461); } 
BgL_i2240z00_3985 = 
(BgL_za71za7_2986+1L); } 
{ /* Ieee/output.scm 576 */
 long BgL_l2241z00_3986;
BgL_l2241z00_3986 = 
STRING_LENGTH(BgL__fmtz00_31); 
if(
BOUND_CHECK(BgL_i2240z00_3985, BgL_l2241z00_3986))
{ /* Ieee/output.scm 576 */
BgL_arg1366z00_1732 = 
STRING_REF(BgL__fmtz00_31, BgL_i2240z00_3985); }  else 
{ 
 obj_t BgL_auxz00_5474;
BgL_auxz00_5474 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20163L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL__fmtz00_31, 
(int)(BgL_l2241z00_3986), 
(int)(BgL_i2240z00_3985)); 
FAILURE(BgL_auxz00_5474,BFALSE,BFALSE);} } } 
{ /* Ieee/output.scm 577 */
 long BgL_za71za7_2989;
{ /* Ieee/output.scm 577 */
 obj_t BgL_tmpz00_5480;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 577 */
BgL_tmpz00_5480 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_5483;
BgL_auxz00_5483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20202L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_5483,BFALSE,BFALSE);} 
BgL_za71za7_2989 = 
(long)CINT(BgL_tmpz00_5480); } 
BgL_arg1367z00_1733 = 
(BgL_za71za7_2989+1L); } 
{ 
 bool_t BgL_altzf3zf3_5491; long BgL_iz00_5490; unsigned char BgL_fz00_5489;
BgL_fz00_5489 = BgL_arg1366z00_1732; 
BgL_iz00_5490 = BgL_arg1367z00_1733; 
BgL_altzf3zf3_5491 = ((bool_t)0); 
BgL_altzf3zf3_1809 = BgL_altzf3zf3_5491; 
BgL_iz00_1808 = BgL_iz00_5490; 
BgL_fz00_1807 = BgL_fz00_5489; 
goto BgL_zc3z04anonymousza31418ze3z87_1810;} } } }  else 
{ /* Ieee/output.scm 561 */
bgl_display_char(BgL_cz00_1717, BgL_pz00_30); 
{ /* Ieee/output.scm 581 */
 long BgL_arg1372z00_1738;
{ /* Ieee/output.scm 581 */
 long BgL_za71za7_2992;
{ /* Ieee/output.scm 581 */
 obj_t BgL_tmpz00_5493;
if(
INTEGERP(BgL_iz00_1706))
{ /* Ieee/output.scm 581 */
BgL_tmpz00_5493 = BgL_iz00_1706
; }  else 
{ 
 obj_t BgL_auxz00_5496;
BgL_auxz00_5496 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20278L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_iz00_1706); 
FAILURE(BgL_auxz00_5496,BFALSE,BFALSE);} 
BgL_za71za7_2992 = 
(long)CINT(BgL_tmpz00_5493); } 
BgL_arg1372z00_1738 = 
(BgL_za71za7_2992+1L); } 
{ 
 obj_t BgL_iz00_5502;
BgL_iz00_5502 = 
BINT(BgL_arg1372z00_1738); 
BgL_iz00_1706 = BgL_iz00_5502; 
goto BgL_zc3z04anonymousza31348ze3z87_1708;} } } }  else 
{ /* Ieee/output.scm 559 */
return BFALSE;} } } } } } 

}



/* print-flat-list~0 */
obj_t BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(obj_t BgL_lz00_1748, obj_t BgL_pz00_1749, obj_t BgL_sepz00_1750)
{
{ /* Ieee/output.scm 449 */
BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00:
if(
PAIRP(BgL_lz00_1748))
{ 
 obj_t BgL_lz00_1754;
BgL_lz00_1754 = BgL_lz00_1748; 
BgL_zc3z04anonymousza31380ze3z87_1755:
{ /* Ieee/output.scm 440 */
 obj_t BgL_arg1382z00_1756;
{ /* Ieee/output.scm 440 */
 obj_t BgL_pairz00_2866;
if(
PAIRP(BgL_lz00_1754))
{ /* Ieee/output.scm 440 */
BgL_pairz00_2866 = BgL_lz00_1754; }  else 
{ 
 obj_t BgL_auxz00_5509;
BgL_auxz00_5509 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16506L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1754); 
FAILURE(BgL_auxz00_5509,BFALSE,BFALSE);} 
BgL_arg1382z00_1756 = 
CAR(BgL_pairz00_2866); } 
BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00(BgL_arg1382z00_1756, BgL_pz00_1749, BgL_sepz00_1750); } 
{ /* Ieee/output.scm 442 */
 bool_t BgL_test3043z00_5515;
{ /* Ieee/output.scm 442 */
 obj_t BgL_tmpz00_5516;
{ /* Ieee/output.scm 442 */
 obj_t BgL_pairz00_2867;
if(
PAIRP(BgL_lz00_1754))
{ /* Ieee/output.scm 442 */
BgL_pairz00_2867 = BgL_lz00_1754; }  else 
{ 
 obj_t BgL_auxz00_5519;
BgL_auxz00_5519 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16548L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1754); 
FAILURE(BgL_auxz00_5519,BFALSE,BFALSE);} 
BgL_tmpz00_5516 = 
CDR(BgL_pairz00_2867); } 
BgL_test3043z00_5515 = 
PAIRP(BgL_tmpz00_5516); } 
if(BgL_test3043z00_5515)
{ /* Ieee/output.scm 442 */
bgl_display_obj(BgL_sepz00_1750, BgL_pz00_1749); 
{ /* Ieee/output.scm 444 */
 obj_t BgL_arg1387z00_1759;
{ /* Ieee/output.scm 444 */
 obj_t BgL_pairz00_2868;
if(
PAIRP(BgL_lz00_1754))
{ /* Ieee/output.scm 444 */
BgL_pairz00_2868 = BgL_lz00_1754; }  else 
{ 
 obj_t BgL_auxz00_5528;
BgL_auxz00_5528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16597L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1754); 
FAILURE(BgL_auxz00_5528,BFALSE,BFALSE);} 
BgL_arg1387z00_1759 = 
CDR(BgL_pairz00_2868); } 
{ 
 obj_t BgL_lz00_5533;
BgL_lz00_5533 = BgL_arg1387z00_1759; 
BgL_lz00_1754 = BgL_lz00_5533; 
goto BgL_zc3z04anonymousza31380ze3z87_1755;} } }  else 
{ /* Ieee/output.scm 445 */
 bool_t BgL_test3046z00_5534;
{ /* Ieee/output.scm 445 */
 obj_t BgL_tmpz00_5535;
{ /* Ieee/output.scm 445 */
 obj_t BgL_pairz00_2869;
if(
PAIRP(BgL_lz00_1754))
{ /* Ieee/output.scm 445 */
BgL_pairz00_2869 = BgL_lz00_1754; }  else 
{ 
 obj_t BgL_auxz00_5538;
BgL_auxz00_5538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16628L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1754); 
FAILURE(BgL_auxz00_5538,BFALSE,BFALSE);} 
BgL_tmpz00_5535 = 
CDR(BgL_pairz00_2869); } 
BgL_test3046z00_5534 = 
NULLP(BgL_tmpz00_5535); } 
if(BgL_test3046z00_5534)
{ /* Ieee/output.scm 445 */
return BFALSE;}  else 
{ /* Ieee/output.scm 445 */
bgl_display_string(BGl_string2603z00zz__r4_output_6_10_3z00, BgL_pz00_1749); 
{ /* Ieee/output.scm 447 */
 obj_t BgL_arg1390z00_1762;
{ /* Ieee/output.scm 447 */
 obj_t BgL_pairz00_2872;
if(
PAIRP(BgL_lz00_1754))
{ /* Ieee/output.scm 447 */
BgL_pairz00_2872 = BgL_lz00_1754; }  else 
{ 
 obj_t BgL_auxz00_5547;
BgL_auxz00_5547 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(16691L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_1754); 
FAILURE(BgL_auxz00_5547,BFALSE,BFALSE);} 
BgL_arg1390z00_1762 = 
CDR(BgL_pairz00_2872); } 
{ 
 obj_t BgL_lz00_5552;
BgL_lz00_5552 = BgL_arg1390z00_1762; 
BgL_lz00_1748 = BgL_lz00_5552; 
goto BGl_printzd2flatzd2listze70ze7zz__r4_output_6_10_3z00;} } } } } }  else 
{ /* Ieee/output.scm 438 */
if(
NULLP(BgL_lz00_1748))
{ /* Ieee/output.scm 448 */
return BFALSE;}  else 
{ /* Ieee/output.scm 448 */
return 
bgl_display_obj(BgL_lz00_1748, BgL_pz00_1749);} } } 

}



/* format */
BGL_EXPORTED_DEF obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t BgL_fmtz00_33, obj_t BgL_objz00_34)
{
{ /* Ieee/output.scm 586 */
{ /* Ieee/output.scm 587 */
 obj_t BgL_pz00_2993;
{ /* Ieee/output.scm 587 */

{ /* Ieee/output.scm 587 */

BgL_pz00_2993 = 
BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE); } } 
{ /* Ieee/output.scm 588 */
 obj_t BgL_auxz00_5557;
{ /* Ieee/output.scm 588 */
 bool_t BgL_test3050z00_5558;
if(
PAIRP(BgL_objz00_34))
{ /* Ieee/output.scm 588 */
BgL_test3050z00_5558 = ((bool_t)1)
; }  else 
{ /* Ieee/output.scm 588 */
BgL_test3050z00_5558 = 
NULLP(BgL_objz00_34)
; } 
if(BgL_test3050z00_5558)
{ /* Ieee/output.scm 588 */
BgL_auxz00_5557 = BgL_objz00_34
; }  else 
{ 
 obj_t BgL_auxz00_5562;
BgL_auxz00_5562 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20612L), BGl_string2605z00zz__r4_output_6_10_3z00, BGl_string2606z00zz__r4_output_6_10_3z00, BgL_objz00_34); 
FAILURE(BgL_auxz00_5562,BFALSE,BFALSE);} } 
BGl_xprintfz00zz__r4_output_6_10_3z00(BGl_symbol2604z00zz__r4_output_6_10_3z00, BgL_pz00_2993, BgL_fmtz00_33, BgL_auxz00_5557); } 
{ /* Ieee/output.scm 589 */
 obj_t BgL_aux2411z00_4156;
BgL_aux2411z00_4156 = 
bgl_close_output_port(BgL_pz00_2993); 
if(
STRINGP(BgL_aux2411z00_4156))
{ /* Ieee/output.scm 589 */
return BgL_aux2411z00_4156;}  else 
{ 
 obj_t BgL_auxz00_5570;
BgL_auxz00_5570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20623L), BGl_string2605z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_aux2411z00_4156); 
FAILURE(BgL_auxz00_5570,BFALSE,BFALSE);} } } } 

}



/* &format */
obj_t BGl_z62formatz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3912, obj_t BgL_fmtz00_3913, obj_t BgL_objz00_3914)
{
{ /* Ieee/output.scm 586 */
{ /* Ieee/output.scm 587 */
 obj_t BgL_auxz00_5574;
if(
STRINGP(BgL_fmtz00_3913))
{ /* Ieee/output.scm 587 */
BgL_auxz00_5574 = BgL_fmtz00_3913
; }  else 
{ 
 obj_t BgL_auxz00_5577;
BgL_auxz00_5577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20551L), BGl_string2607z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_fmtz00_3913); 
FAILURE(BgL_auxz00_5577,BFALSE,BFALSE);} 
return 
BGl_formatz00zz__r4_output_6_10_3z00(BgL_auxz00_5574, BgL_objz00_3914);} } 

}



/* printf */
BGL_EXPORTED_DEF obj_t BGl_printfz00zz__r4_output_6_10_3z00(obj_t BgL_fmtz00_35, obj_t BgL_objz00_36)
{
{ /* Ieee/output.scm 594 */
{ /* Ieee/output.scm 595 */
 obj_t BgL_arg1464z00_2996;
{ /* Ieee/output.scm 595 */
 obj_t BgL_tmpz00_5582;
BgL_tmpz00_5582 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1464z00_2996 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5582); } 
{ /* Ieee/output.scm 595 */
 obj_t BgL_auxz00_5585;
{ /* Ieee/output.scm 595 */
 bool_t BgL_test3054z00_5586;
if(
PAIRP(BgL_objz00_36))
{ /* Ieee/output.scm 595 */
BgL_test3054z00_5586 = ((bool_t)1)
; }  else 
{ /* Ieee/output.scm 595 */
BgL_test3054z00_5586 = 
NULLP(BgL_objz00_36)
; } 
if(BgL_test3054z00_5586)
{ /* Ieee/output.scm 595 */
BgL_auxz00_5585 = BgL_objz00_36
; }  else 
{ 
 obj_t BgL_auxz00_5590;
BgL_auxz00_5590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20943L), BGl_string2609z00zz__r4_output_6_10_3z00, BGl_string2606z00zz__r4_output_6_10_3z00, BgL_objz00_36); 
FAILURE(BgL_auxz00_5590,BFALSE,BFALSE);} } 
return 
BGl_xprintfz00zz__r4_output_6_10_3z00(BGl_symbol2608z00zz__r4_output_6_10_3z00, BgL_arg1464z00_2996, BgL_fmtz00_35, BgL_auxz00_5585);} } } 

}



/* &printf */
obj_t BGl_z62printfz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3915, obj_t BgL_fmtz00_3916, obj_t BgL_objz00_3917)
{
{ /* Ieee/output.scm 594 */
{ /* Ieee/output.scm 595 */
 obj_t BgL_auxz00_5595;
if(
STRINGP(BgL_fmtz00_3916))
{ /* Ieee/output.scm 595 */
BgL_auxz00_5595 = BgL_fmtz00_3916
; }  else 
{ 
 obj_t BgL_auxz00_5598;
BgL_auxz00_5598 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(20917L), BGl_string2610z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_fmtz00_3916); 
FAILURE(BgL_auxz00_5598,BFALSE,BFALSE);} 
return 
BGl_printfz00zz__r4_output_6_10_3z00(BgL_auxz00_5595, BgL_objz00_3917);} } 

}



/* fprintf */
BGL_EXPORTED_DEF obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t BgL_portz00_37, obj_t BgL_fmtz00_38, obj_t BgL_objz00_39)
{
{ /* Ieee/output.scm 600 */
{ /* Ieee/output.scm 601 */
 obj_t BgL_auxz00_5603;
{ /* Ieee/output.scm 601 */
 bool_t BgL_test3057z00_5604;
if(
PAIRP(BgL_objz00_39))
{ /* Ieee/output.scm 601 */
BgL_test3057z00_5604 = ((bool_t)1)
; }  else 
{ /* Ieee/output.scm 601 */
BgL_test3057z00_5604 = 
NULLP(BgL_objz00_39)
; } 
if(BgL_test3057z00_5604)
{ /* Ieee/output.scm 601 */
BgL_auxz00_5603 = BgL_objz00_39
; }  else 
{ 
 obj_t BgL_auxz00_5608;
BgL_auxz00_5608 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(21235L), BGl_string2612z00zz__r4_output_6_10_3z00, BGl_string2606z00zz__r4_output_6_10_3z00, BgL_objz00_39); 
FAILURE(BgL_auxz00_5608,BFALSE,BFALSE);} } 
return 
BGl_xprintfz00zz__r4_output_6_10_3z00(BGl_symbol2611z00zz__r4_output_6_10_3z00, BgL_portz00_37, BgL_fmtz00_38, BgL_auxz00_5603);} } 

}



/* &fprintf */
obj_t BGl_z62fprintfz62zz__r4_output_6_10_3z00(obj_t BgL_envz00_3918, obj_t BgL_portz00_3919, obj_t BgL_fmtz00_3920, obj_t BgL_objz00_3921)
{
{ /* Ieee/output.scm 600 */
{ /* Ieee/output.scm 601 */
 obj_t BgL_auxz00_5620; obj_t BgL_auxz00_5613;
if(
STRINGP(BgL_fmtz00_3920))
{ /* Ieee/output.scm 601 */
BgL_auxz00_5620 = BgL_fmtz00_3920
; }  else 
{ 
 obj_t BgL_auxz00_5623;
BgL_auxz00_5623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(21208L), BGl_string2613z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_fmtz00_3920); 
FAILURE(BgL_auxz00_5623,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_3919))
{ /* Ieee/output.scm 601 */
BgL_auxz00_5613 = BgL_portz00_3919
; }  else 
{ 
 obj_t BgL_auxz00_5616;
BgL_auxz00_5616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(21208L), BGl_string2613z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3919); 
FAILURE(BgL_auxz00_5616,BFALSE,BFALSE);} 
return 
BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_auxz00_5613, BgL_auxz00_5620, BgL_objz00_3921);} } 

}



/* display-2 */
BGL_EXPORTED_DEF obj_t bgl_display_obj(obj_t BgL_objz00_40, obj_t BgL_portz00_41)
{
{ /* Ieee/output.scm 760 */
bgl_display_obj:
if(
STRINGP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
SYMBOLP(BgL_objz00_40))
{ /* Ieee/output.scm 773 */
 obj_t BgL_arg1607z00_3002;
BgL_arg1607z00_3002 = 
SYMBOL_TO_STRING(BgL_objz00_40); 
return 
bgl_display_string(BgL_arg1607z00_3002, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
INTEGERP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_fixnum(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
CHARP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 unsigned char BgL_tmpz00_5640;
BgL_tmpz00_5640 = 
CCHAR(BgL_objz00_40); 
return 
bgl_display_char(BgL_tmpz00_5640, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
PAIRP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
BGl_displayzd2pairzd2zz__r4_output_6_10_3z00(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
NULLP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2614z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
(BgL_objz00_40==BFALSE))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2615z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
(BgL_objz00_40==BTRUE))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2616z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
(BgL_objz00_40==BUNSPEC))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2617z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
ELONGP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_elong(
BELONG_TO_LONG(BgL_objz00_40), BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
REALP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_objz00_3014;
BgL_objz00_3014 = 
DOUBLE_TO_REAL(
REAL_TO_DOUBLE(BgL_objz00_40)); 
{ /* Ieee/output.scm 884 */
 obj_t BgL_arg1650z00_3016;
BgL_arg1650z00_3016 = 
bgl_real_to_string(
REAL_TO_DOUBLE(BgL_objz00_3014)); 
return 
bgl_display_string(BgL_arg1650z00_3016, BgL_portz00_41);} }  else 
{ /* Ieee/output.scm 761 */
if(
KEYWORDP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
bgl_display_char(((unsigned char)':'), BgL_portz00_41); 
{ /* Ieee/output.scm 866 */
 obj_t BgL_arg1649z00_3019;
BgL_arg1649z00_3019 = 
KEYWORD_TO_STRING(BgL_objz00_40); 
return 
bgl_display_string(BgL_arg1649z00_3019, BgL_portz00_41);} }  else 
{ /* Ieee/output.scm 761 */
if(
BGl_classzf3zf3zz__objectz00(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
bgl_display_string(BGl_string2618z00zz__r4_output_6_10_3z00, BgL_portz00_41); 
{ /* Ieee/output.scm 936 */
 obj_t BgL_arg1661z00_3023;
{ /* Ieee/output.scm 936 */
 obj_t BgL_auxz00_5677;
if(
BGl_classzf3zf3zz__objectz00(BgL_objz00_40))
{ /* Ieee/output.scm 936 */
BgL_auxz00_5677 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5680;
BgL_auxz00_5680 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(33951L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2620z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5680,BFALSE,BFALSE);} 
BgL_arg1661z00_3023 = 
BGl_classzd2namezd2zz__objectz00(BgL_auxz00_5677); } 
{ /* Ieee/output.scm 773 */
 obj_t BgL_arg1607z00_3028;
BgL_arg1607z00_3028 = 
SYMBOL_TO_STRING(BgL_arg1661z00_3023); 
bgl_display_string(BgL_arg1607z00_3028, BgL_portz00_41); } } 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
VECTORP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00(BgL_objz00_40, BgL_portz00_41, BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 761 */
if(
LLONGP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_llong(
BLLONG_TO_LLONG(BgL_objz00_40), BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
UCS2_STRINGP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_ucs2string(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
STRUCTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00(BgL_objz00_40, BgL_portz00_41, BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_OBJECTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_list1480z00_1885;
BgL_list1480z00_1885 = 
MAKE_YOUNG_PAIR(BgL_portz00_41, BNIL); 
return 
BGl_objectzd2displayzd2zz__objectz00(
((BgL_objectz00_bglt)BgL_objz00_40), BgL_list1480z00_1885);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_DATEP(BgL_objz00_40))
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_5708;
BgL_tmpz00_5708 = 
BGl_datezd2ze3stringz31zz__datez00(BgL_objz00_40); 
return 
bgl_display_string(BgL_tmpz00_5708, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_MUTEXP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BGL_TAIL return 
BGl_writezd2mutexzd2zz__r4_output_6_10_3z00(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_CONDVARP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
bgl_display_string(BGl_string2622z00zz__r4_output_6_10_3z00, BgL_portz00_41); 
{ /* Ieee/output.scm 928 */
 obj_t BgL_arg1658z00_3039;
{ /* Ieee/output.scm 928 */
 obj_t BgL_objz00_3042;
if(
BGL_CONDVARP(BgL_objz00_40))
{ /* Ieee/output.scm 928 */
BgL_objz00_3042 = BgL_objz00_40; }  else 
{ 
 obj_t BgL_auxz00_5719;
BgL_auxz00_5719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(33588L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2623z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5719,BFALSE,BFALSE);} 
BgL_arg1658z00_3039 = 
BGL_CONDVAR_NAME(BgL_objz00_3042); } 
bgl_display_obj(BgL_arg1658z00_3039, BgL_portz00_41); } 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
UCS2P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_ucs2(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
CELLP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
bgl_display_string(BGl_string2624z00zz__r4_output_6_10_3z00, BgL_portz00_41); 
bgl_display_obj(
CELL_REF(BgL_objz00_40), BgL_portz00_41); 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
EOF_OBJECTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2625z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
(BgL_objz00_40==(BOPTIONAL)))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2626z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
(BgL_objz00_40==(BREST)))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2627z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
(BgL_objz00_40==(BKEY)))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2628z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
PROCEDUREP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_write_procedure(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
OUTPUT_PORTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
if(
BGL_OUTPUT_STRING_PORTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2629z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_OUTPUT_PROCEDURE_PORTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_string(BGl_string2630z00zz__r4_output_6_10_3z00, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
return 
bgl_write_output_port(BgL_objz00_40, BgL_portz00_41);} } }  else 
{ /* Ieee/output.scm 761 */
if(
INPUT_PORTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_write_input_port(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BIGNUMP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_display_bignum(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_HVECTORP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00(BgL_objz00_40, BgL_portz00_41, BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 761 */
if(
TVECTORP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00(BgL_objz00_40, BgL_portz00_41, BGl_displayzd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_WEAKPTRP(BgL_objz00_40))
{ /* Ieee/output.scm 1052 */
 obj_t BgL_dataz00_3063;
BgL_dataz00_3063 = 
bgl_weakptr_data(BgL_objz00_40); 
bgl_display_string(BGl_string2631z00zz__r4_output_6_10_3z00, BgL_portz00_41); 
bgl_display_obj(BgL_dataz00_3063, BgL_portz00_41); 
return 
bgl_display_char(((unsigned char)'>'), BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
FOREIGNP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_write_foreign(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
PROCESSP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_tmpz00_5782;
if(
PROCESSP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BgL_tmpz00_5782 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5785;
BgL_auxz00_5785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(26790L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2632z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5785,BFALSE,BFALSE);} 
return 
bgl_write_process(BgL_tmpz00_5782, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
SOCKETP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_tmpz00_5792;
if(
SOCKETP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BgL_tmpz00_5792 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5795;
BgL_auxz00_5795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(26790L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2633z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5795,BFALSE,BFALSE);} 
return 
bgl_write_socket(BgL_tmpz00_5792, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_DATAGRAM_SOCKETP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_tmpz00_5802;
if(
BGL_DATAGRAM_SOCKETP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BgL_tmpz00_5802 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5805;
BgL_auxz00_5805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(26790L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2634z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5805,BFALSE,BFALSE);} 
return 
bgl_write_datagram_socket(BgL_tmpz00_5802, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_REGEXPP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_tmpz00_5812;
if(
BGL_REGEXPP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BgL_tmpz00_5812 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5815;
BgL_auxz00_5815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(26790L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2635z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5815,BFALSE,BFALSE);} 
return 
bgl_write_regexp(BgL_tmpz00_5812, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_MMAPP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_tmpz00_5822;
if(
BGL_MMAPP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BgL_tmpz00_5822 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5825;
BgL_auxz00_5825 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(26790L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2636z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5825,BFALSE,BFALSE);} 
return 
bgl_write_mmap(BgL_tmpz00_5822, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_SEMAPHOREP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 obj_t BgL_tmpz00_5832;
if(
BGL_SEMAPHOREP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BgL_tmpz00_5832 = BgL_objz00_40
; }  else 
{ 
 obj_t BgL_auxz00_5835;
BgL_auxz00_5835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(26790L), BGl_string2619z00zz__r4_output_6_10_3z00, BGl_string2637z00zz__r4_output_6_10_3z00, BgL_objz00_40); 
FAILURE(BgL_auxz00_5835,BFALSE,BFALSE);} 
return 
bgl_write_semaphore(BgL_tmpz00_5832, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
OPAQUEP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BGL_TAIL return 
bgl_write_opaque(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
CUSTOMP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BGL_TAIL return 
bgl_write_custom(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BINARY_PORTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_write_binary_port(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_DYNAMIC_ENVP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
return 
bgl_write_dynamic_env(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
if(
BGL_INT8P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 long BgL_arg1508z00_1913;
{ /* Ieee/output.scm 761 */
 int8_t BgL_xz00_3065;
BgL_xz00_3065 = 
BGL_BINT8_TO_INT8(BgL_objz00_40); 
{ /* Ieee/output.scm 761 */
 long BgL_arg1996z00_3066;
BgL_arg1996z00_3066 = 
(long)(BgL_xz00_3065); 
BgL_arg1508z00_1913 = 
(long)(BgL_arg1996z00_3066); } } 
{ 
 obj_t BgL_objz00_5857;
BgL_objz00_5857 = 
BINT(BgL_arg1508z00_1913); 
BgL_objz00_40 = BgL_objz00_5857; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_UINT8P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 long BgL_arg1510z00_1915;
{ /* Ieee/output.scm 761 */
 uint8_t BgL_xz00_3068;
BgL_xz00_3068 = 
BGL_BUINT8_TO_UINT8(BgL_objz00_40); 
{ /* Ieee/output.scm 761 */
 long BgL_arg1995z00_3069;
BgL_arg1995z00_3069 = 
(long)(BgL_xz00_3068); 
BgL_arg1510z00_1915 = 
(long)(BgL_arg1995z00_3069); } } 
{ 
 obj_t BgL_objz00_5864;
BgL_objz00_5864 = 
BINT(BgL_arg1510z00_1915); 
BgL_objz00_40 = BgL_objz00_5864; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_INT16P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 long BgL_arg1513z00_1917;
{ /* Ieee/output.scm 761 */
 int16_t BgL_xz00_3071;
BgL_xz00_3071 = 
BGL_BINT16_TO_INT16(BgL_objz00_40); 
{ /* Ieee/output.scm 761 */
 long BgL_arg1994z00_3072;
BgL_arg1994z00_3072 = 
(long)(BgL_xz00_3071); 
BgL_arg1513z00_1917 = 
(long)(BgL_arg1994z00_3072); } } 
{ 
 obj_t BgL_objz00_5871;
BgL_objz00_5871 = 
BINT(BgL_arg1513z00_1917); 
BgL_objz00_40 = BgL_objz00_5871; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_UINT16P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 long BgL_arg1516z00_1919;
{ /* Ieee/output.scm 761 */
 uint16_t BgL_xz00_3074;
BgL_xz00_3074 = 
BGL_BUINT16_TO_UINT16(BgL_objz00_40); 
{ /* Ieee/output.scm 761 */
 long BgL_arg1993z00_3075;
BgL_arg1993z00_3075 = 
(long)(BgL_xz00_3074); 
BgL_arg1516z00_1919 = 
(long)(BgL_arg1993z00_3075); } } 
{ 
 obj_t BgL_objz00_5878;
BgL_objz00_5878 = 
BINT(BgL_arg1516z00_1919); 
BgL_objz00_40 = BgL_objz00_5878; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_INT32P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 long BgL_arg1521z00_1921;
{ /* Ieee/output.scm 761 */
 int32_t BgL_nz00_3077;
BgL_nz00_3077 = 
BGL_BINT32_TO_INT32(BgL_objz00_40); 
BgL_arg1521z00_1921 = 
(long)(BgL_nz00_3077); } 
{ 
 obj_t BgL_objz00_5884;
BgL_objz00_5884 = 
make_belong(BgL_arg1521z00_1921); 
BgL_objz00_40 = BgL_objz00_5884; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_UINT32P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 BGL_LONGLONG_T BgL_arg1523z00_1923;
{ /* Ieee/output.scm 761 */
 uint32_t BgL_nz00_3078;
BgL_nz00_3078 = 
BGL_BUINT32_TO_UINT32(BgL_objz00_40); 
BgL_arg1523z00_1923 = 
(BGL_LONGLONG_T)(BgL_nz00_3078); } 
{ 
 obj_t BgL_objz00_5890;
BgL_objz00_5890 = 
make_bllong(BgL_arg1523z00_1923); 
BgL_objz00_40 = BgL_objz00_5890; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_INT64P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
 BGL_LONGLONG_T BgL_arg1525z00_1925;
{ /* Ieee/output.scm 761 */
 int64_t BgL_nz00_3079;
BgL_nz00_3079 = 
BGL_BINT64_TO_INT64(BgL_objz00_40); 
BgL_arg1525z00_1925 = 
(BGL_LONGLONG_T)(BgL_nz00_3079); } 
{ 
 obj_t BgL_objz00_5896;
BgL_objz00_5896 = 
make_bllong(BgL_arg1525z00_1925); 
BgL_objz00_40 = BgL_objz00_5896; 
goto bgl_display_obj;} }  else 
{ /* Ieee/output.scm 761 */
if(
BGL_UINT64P(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
{ /* Ieee/output.scm 761 */
 uint64_t BgL_vz00_1927;
{ /* Ieee/output.scm 761 */
 uint64_t BgL_arg1529z00_1930;
BgL_arg1529z00_1930 = 
(uint64_t)(10L); 
{ /* Ieee/output.scm 761 */
 uint64_t BgL_za71za7_3080;
BgL_za71za7_3080 = 
BGL_BINT64_TO_INT64(BgL_objz00_40); 
BgL_vz00_1927 = 
(BgL_za71za7_3080/BgL_arg1529z00_1930); } } 
if(
(BgL_vz00_1927>(uint64_t)(0)))
{ /* Ieee/output.scm 761 */
 BGL_LONGLONG_T BgL_arg1528z00_1929;
BgL_arg1528z00_1929 = 
(BGL_LONGLONG_T)(BgL_vz00_1927); 
bgl_display_obj(
make_bllong(BgL_arg1528z00_1929), BgL_portz00_41); }  else 
{ /* Ieee/output.scm 761 */BFALSE; } } 
{ /* Ieee/output.scm 761 */
 long BgL_arg1530z00_1931;
{ /* Ieee/output.scm 761 */
 uint64_t BgL_arg1531z00_1932;
{ /* Ieee/output.scm 761 */
 uint64_t BgL_arg1535z00_1933;
BgL_arg1535z00_1933 = 
(uint64_t)(10L); 
{ /* Ieee/output.scm 761 */
 uint64_t BgL_n1z00_3085;
BgL_n1z00_3085 = 
BGL_BINT64_TO_INT64(BgL_objz00_40); 
{ /* Ieee/output.scm 761 */
 int64_t BgL_tmpz00_5910;
BgL_tmpz00_5910 = 
(int64_t)(BgL_arg1535z00_1933); 
BgL_arg1531z00_1932 = 
(BgL_n1z00_3085%BgL_tmpz00_5910); } } } 
{ /* Ieee/output.scm 761 */
 long BgL_arg1990z00_3088;
BgL_arg1990z00_3088 = 
(long)(BgL_arg1531z00_1932); 
BgL_arg1530z00_1931 = 
(long)(BgL_arg1990z00_3088); } } 
{ 
 obj_t BgL_objz00_5915;
BgL_objz00_5915 = 
BINT(BgL_arg1530z00_1931); 
BgL_objz00_40 = BgL_objz00_5915; 
goto bgl_display_obj;} } }  else 
{ /* Ieee/output.scm 761 */
if(
CNSTP(BgL_objz00_40))
{ /* Ieee/output.scm 761 */
BGL_TAIL return 
bgl_write_cnst(BgL_objz00_40, BgL_portz00_41);}  else 
{ /* Ieee/output.scm 761 */
BGL_TAIL return 
bgl_write_unknown(BgL_objz00_40, BgL_portz00_41);} } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } 

}



/* &display-2 */
obj_t BGl_z62displayzd22zb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3922, obj_t BgL_objz00_3923, obj_t BgL_portz00_3924)
{
{ /* Ieee/output.scm 760 */
return 
bgl_display_obj(BgL_objz00_3923, BgL_portz00_3924);} 

}



/* write-2 */
BGL_EXPORTED_DEF obj_t bgl_write_obj(obj_t BgL_objz00_42, obj_t BgL_portz00_43)
{
{ /* Ieee/output.scm 766 */
if(
STRINGP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
if(
BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00())
{ /* Ieee/output.scm 854 */
 obj_t BgL_strz00_3093;
BgL_strz00_3093 = 
string_for_read(BgL_objz00_42); 
{ /* Ieee/output.scm 855 */
 obj_t BgL_escz00_3094;
{ /* Ieee/output.scm 856 */
 obj_t BgL_tmpz00_3097;
{ /* Ieee/output.scm 856 */
 int BgL_tmpz00_5927;
BgL_tmpz00_5927 = 
(int)(1L); 
BgL_tmpz00_3097 = 
BGL_MVALUES_VAL(BgL_tmpz00_5927); } 
{ /* Ieee/output.scm 856 */
 int BgL_tmpz00_5930;
BgL_tmpz00_5930 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_5930, BUNSPEC); } 
BgL_escz00_3094 = BgL_tmpz00_3097; } 
return 
bgl_write_string(BgL_strz00_3093, 
CBOOL(BgL_escz00_3094), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 851 */
return 
bgl_write_string(
string_for_read(BgL_objz00_42), ((bool_t)0), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
SYMBOLP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
INTEGERP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_display_fixnum(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
CHARP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_char(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
PAIRP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
BGl_writezd2pairzd2zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
NULLP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2614z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
(BgL_objz00_42==BFALSE))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2615z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
(BgL_objz00_42==BTRUE))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2616z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
(BgL_objz00_42==BUNSPEC))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2617z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
ELONGP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_elong(
BELONG_TO_LONG(BgL_objz00_42), BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
REALP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_objz00_3107;
BgL_objz00_3107 = 
DOUBLE_TO_REAL(
REAL_TO_DOUBLE(BgL_objz00_42)); 
{ /* Ieee/output.scm 884 */
 obj_t BgL_arg1650z00_3109;
BgL_arg1650z00_3109 = 
bgl_real_to_string(
REAL_TO_DOUBLE(BgL_objz00_3107)); 
return 
bgl_display_string(BgL_arg1650z00_3109, BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
KEYWORDP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_char(((unsigned char)':'), BgL_portz00_43); 
{ /* Ieee/output.scm 866 */
 obj_t BgL_arg1649z00_3112;
BgL_arg1649z00_3112 = 
KEYWORD_TO_STRING(BgL_objz00_42); 
return 
bgl_display_string(BgL_arg1649z00_3112, BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGl_classzf3zf3zz__objectz00(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2618z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 936 */
 obj_t BgL_arg1661z00_3116;
{ /* Ieee/output.scm 936 */
 obj_t BgL_auxz00_5980;
if(
BGl_classzf3zf3zz__objectz00(BgL_objz00_42))
{ /* Ieee/output.scm 936 */
BgL_auxz00_5980 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_5983;
BgL_auxz00_5983 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(33951L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2620z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_5983,BFALSE,BFALSE);} 
BgL_arg1661z00_3116 = 
BGl_classzd2namezd2zz__objectz00(BgL_auxz00_5980); } 
{ /* Ieee/output.scm 773 */
 obj_t BgL_arg1607z00_3121;
BgL_arg1607z00_3121 = 
SYMBOL_TO_STRING(BgL_arg1661z00_3116); 
bgl_display_string(BgL_arg1607z00_3121, BgL_portz00_43); } } 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
VECTORP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43, BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 767 */
if(
LLONGP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_llong(
BLLONG_TO_LLONG(BgL_objz00_42), BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
UCS2_STRINGP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_utf8string(
string_for_read(
ucs2_string_to_utf8_string(BgL_objz00_42)), BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
STRUCTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43, BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_OBJECTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_list1552z00_1950;
BgL_list1552z00_1950 = 
MAKE_YOUNG_PAIR(BgL_portz00_43, BNIL); 
return 
BGl_objectzd2writezd2zz__objectz00(
((BgL_objectz00_bglt)BgL_objz00_42), BgL_list1552z00_1950);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_DATEP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2639z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6014;
BgL_tmpz00_6014 = 
BGl_datezd2ze3stringz31zz__datez00(BgL_objz00_42); 
bgl_display_string(BgL_tmpz00_6014, BgL_portz00_43); } 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_MUTEXP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BGL_TAIL return 
BGl_writezd2mutexzd2zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_CONDVARP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2622z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 928 */
 obj_t BgL_arg1658z00_3140;
{ /* Ieee/output.scm 928 */
 obj_t BgL_objz00_3143;
if(
BGL_CONDVARP(BgL_objz00_42))
{ /* Ieee/output.scm 928 */
BgL_objz00_3143 = BgL_objz00_42; }  else 
{ 
 obj_t BgL_auxz00_6026;
BgL_auxz00_6026 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(33588L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2623z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6026,BFALSE,BFALSE);} 
BgL_arg1658z00_3140 = 
BGL_CONDVAR_NAME(BgL_objz00_3143); } 
bgl_display_obj(BgL_arg1658z00_3140, BgL_portz00_43); } 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
UCS2P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_ucs2(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
CELLP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2624z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
bgl_write_obj(
CELL_REF(BgL_objz00_42), BgL_portz00_43); 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
EOF_OBJECTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2625z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
(BgL_objz00_42==(BOPTIONAL)))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2626z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
(BgL_objz00_42==(BREST)))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2627z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
(BgL_objz00_42==(BKEY)))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2628z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
PROCEDUREP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_procedure(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
OUTPUT_PORTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
if(
BGL_OUTPUT_STRING_PORTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2629z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_OUTPUT_PROCEDURE_PORTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_display_string(BGl_string2630z00zz__r4_output_6_10_3z00, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
return 
bgl_write_output_port(BgL_objz00_42, BgL_portz00_43);} } }  else 
{ /* Ieee/output.scm 767 */
if(
INPUT_PORTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_input_port(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BIGNUMP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_bignum(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_HVECTORP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43, BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 767 */
if(
TVECTORP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00(BgL_objz00_42, BgL_portz00_43, BGl_writezd22zd2envz00zz__r4_output_6_10_3z00);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_WEAKPTRP(BgL_objz00_42))
{ /* Ieee/output.scm 1052 */
 obj_t BgL_dataz00_3164;
BgL_dataz00_3164 = 
bgl_weakptr_data(BgL_objz00_42); 
bgl_display_string(BGl_string2631z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
bgl_write_obj(BgL_dataz00_3164, BgL_portz00_43); 
return 
bgl_display_char(((unsigned char)'>'), BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
FOREIGNP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_foreign(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
PROCESSP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_tmpz00_6089;
if(
PROCESSP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BgL_tmpz00_6089 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_6092;
BgL_auxz00_6092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27093L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2632z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6092,BFALSE,BFALSE);} 
return 
bgl_write_process(BgL_tmpz00_6089, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
SOCKETP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_tmpz00_6099;
if(
SOCKETP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BgL_tmpz00_6099 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_6102;
BgL_auxz00_6102 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27093L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2633z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6102,BFALSE,BFALSE);} 
return 
bgl_write_socket(BgL_tmpz00_6099, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_DATAGRAM_SOCKETP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_tmpz00_6109;
if(
BGL_DATAGRAM_SOCKETP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BgL_tmpz00_6109 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_6112;
BgL_auxz00_6112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27093L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2634z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6112,BFALSE,BFALSE);} 
return 
bgl_write_datagram_socket(BgL_tmpz00_6109, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_REGEXPP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_tmpz00_6119;
if(
BGL_REGEXPP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BgL_tmpz00_6119 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_6122;
BgL_auxz00_6122 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27093L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2635z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6122,BFALSE,BFALSE);} 
return 
bgl_write_regexp(BgL_tmpz00_6119, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_MMAPP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_tmpz00_6129;
if(
BGL_MMAPP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BgL_tmpz00_6129 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_6132;
BgL_auxz00_6132 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27093L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2636z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6132,BFALSE,BFALSE);} 
return 
bgl_write_mmap(BgL_tmpz00_6129, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_SEMAPHOREP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
 obj_t BgL_tmpz00_6139;
if(
BGL_SEMAPHOREP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BgL_tmpz00_6139 = BgL_objz00_42
; }  else 
{ 
 obj_t BgL_auxz00_6142;
BgL_auxz00_6142 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27093L), BGl_string2638z00zz__r4_output_6_10_3z00, BGl_string2637z00zz__r4_output_6_10_3z00, BgL_objz00_42); 
FAILURE(BgL_auxz00_6142,BFALSE,BFALSE);} 
return 
bgl_write_semaphore(BgL_tmpz00_6139, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
OPAQUEP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BGL_TAIL return 
bgl_write_opaque(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
CUSTOMP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BGL_TAIL return 
bgl_write_custom(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BINARY_PORTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_binary_port(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_DYNAMIC_ENVP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
return 
bgl_write_dynamic_env(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
if(
BGL_INT8P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2640z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1580z00_1978;
{ /* Ieee/output.scm 767 */
 int8_t BgL_xz00_3168;
BgL_xz00_3168 = 
BGL_BINT8_TO_INT8(BgL_objz00_42); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1996z00_3169;
BgL_arg1996z00_3169 = 
(long)(BgL_xz00_3168); 
BgL_arg1580z00_1978 = 
(long)(BgL_arg1996z00_3169); } } 
BGL_TAIL return 
bgl_display_obj(
BINT(BgL_arg1580z00_1978), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_UINT8P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2641z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1582z00_1980;
{ /* Ieee/output.scm 767 */
 uint8_t BgL_xz00_3173;
BgL_xz00_3173 = 
BGL_BUINT8_TO_UINT8(BgL_objz00_42); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1995z00_3174;
BgL_arg1995z00_3174 = 
(long)(BgL_xz00_3173); 
BgL_arg1582z00_1980 = 
(long)(BgL_arg1995z00_3174); } } 
BGL_TAIL return 
bgl_display_obj(
BINT(BgL_arg1582z00_1980), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_INT16P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2642z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1584z00_1982;
{ /* Ieee/output.scm 767 */
 int16_t BgL_xz00_3178;
BgL_xz00_3178 = 
BGL_BINT16_TO_INT16(BgL_objz00_42); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1994z00_3179;
BgL_arg1994z00_3179 = 
(long)(BgL_xz00_3178); 
BgL_arg1584z00_1982 = 
(long)(BgL_arg1994z00_3179); } } 
BGL_TAIL return 
bgl_display_obj(
BINT(BgL_arg1584z00_1982), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_UINT16P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2643z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1586z00_1984;
{ /* Ieee/output.scm 767 */
 uint16_t BgL_xz00_3183;
BgL_xz00_3183 = 
BGL_BUINT16_TO_UINT16(BgL_objz00_42); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1993z00_3184;
BgL_arg1993z00_3184 = 
(long)(BgL_xz00_3183); 
BgL_arg1586z00_1984 = 
(long)(BgL_arg1993z00_3184); } } 
BGL_TAIL return 
bgl_display_obj(
BINT(BgL_arg1586z00_1984), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_INT32P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2644z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 long BgL_arg1589z00_1986;
{ /* Ieee/output.scm 767 */
 int32_t BgL_nz00_3188;
BgL_nz00_3188 = 
BGL_BINT32_TO_INT32(BgL_objz00_42); 
BgL_arg1589z00_1986 = 
(long)(BgL_nz00_3188); } 
BGL_TAIL return 
bgl_display_obj(
make_belong(BgL_arg1589z00_1986), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_UINT32P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2645z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 BGL_LONGLONG_T BgL_arg1593z00_1988;
{ /* Ieee/output.scm 767 */
 uint32_t BgL_nz00_3191;
BgL_nz00_3191 = 
BGL_BUINT32_TO_UINT32(BgL_objz00_42); 
BgL_arg1593z00_1988 = 
(BGL_LONGLONG_T)(BgL_nz00_3191); } 
BGL_TAIL return 
bgl_display_obj(
make_bllong(BgL_arg1593z00_1988), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_INT64P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2646z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 BGL_LONGLONG_T BgL_arg1595z00_1990;
{ /* Ieee/output.scm 767 */
 int64_t BgL_nz00_3194;
BgL_nz00_3194 = 
BGL_BINT64_TO_INT64(BgL_objz00_42); 
BgL_arg1595z00_1990 = 
(BGL_LONGLONG_T)(BgL_nz00_3194); } 
BGL_TAIL return 
bgl_display_obj(
make_bllong(BgL_arg1595z00_1990), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
BGL_UINT64P(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
bgl_display_string(BGl_string2647z00zz__r4_output_6_10_3z00, BgL_portz00_43); 
{ /* Ieee/output.scm 767 */
 uint64_t BgL_vz00_1992;
{ /* Ieee/output.scm 767 */
 uint64_t BgL_arg1601z00_1995;
BgL_arg1601z00_1995 = 
(uint64_t)(10L); 
{ /* Ieee/output.scm 767 */
 uint64_t BgL_za71za7_3197;
BgL_za71za7_3197 = 
BGL_BINT64_TO_INT64(BgL_objz00_42); 
BgL_vz00_1992 = 
(BgL_za71za7_3197/BgL_arg1601z00_1995); } } 
if(
(BgL_vz00_1992>(uint64_t)(0)))
{ /* Ieee/output.scm 767 */
 BGL_LONGLONG_T BgL_arg1598z00_1994;
BgL_arg1598z00_1994 = 
(BGL_LONGLONG_T)(BgL_vz00_1992); 
bgl_display_obj(
make_bllong(BgL_arg1598z00_1994), BgL_portz00_43); }  else 
{ /* Ieee/output.scm 767 */BFALSE; } } 
{ /* Ieee/output.scm 767 */
 long BgL_arg1602z00_1996;
{ /* Ieee/output.scm 767 */
 uint64_t BgL_arg1603z00_1997;
{ /* Ieee/output.scm 767 */
 uint64_t BgL_arg1605z00_1998;
BgL_arg1605z00_1998 = 
(uint64_t)(10L); 
{ /* Ieee/output.scm 767 */
 uint64_t BgL_n1z00_3202;
BgL_n1z00_3202 = 
BGL_BINT64_TO_INT64(BgL_objz00_42); 
{ /* Ieee/output.scm 767 */
 int64_t BgL_tmpz00_6225;
BgL_tmpz00_6225 = 
(int64_t)(BgL_arg1605z00_1998); 
BgL_arg1603z00_1997 = 
(BgL_n1z00_3202%BgL_tmpz00_6225); } } } 
{ /* Ieee/output.scm 767 */
 long BgL_arg1990z00_3205;
BgL_arg1990z00_3205 = 
(long)(BgL_arg1603z00_1997); 
BgL_arg1602z00_1996 = 
(long)(BgL_arg1990z00_3205); } } 
BGL_TAIL return 
bgl_display_obj(
BINT(BgL_arg1602z00_1996), BgL_portz00_43);} }  else 
{ /* Ieee/output.scm 767 */
if(
CNSTP(BgL_objz00_42))
{ /* Ieee/output.scm 767 */
BGL_TAIL return 
bgl_write_cnst(BgL_objz00_42, BgL_portz00_43);}  else 
{ /* Ieee/output.scm 767 */
BGL_TAIL return 
bgl_write_unknown(BgL_objz00_42, BgL_portz00_43);} } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } 

}



/* &write-2 */
obj_t BGl_z62writezd22zb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3925, obj_t BgL_objz00_3926, obj_t BgL_portz00_3927)
{
{ /* Ieee/output.scm 766 */
return 
bgl_write_obj(BgL_objz00_3926, BgL_portz00_3927);} 

}



/* display-symbol */
BGL_EXPORTED_DEF obj_t BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_44, obj_t BgL_portz00_45)
{
{ /* Ieee/output.scm 772 */
{ /* Ieee/output.scm 773 */
 obj_t BgL_arg1607z00_3207;
BgL_arg1607z00_3207 = 
SYMBOL_TO_STRING(BgL_objz00_44); 
return 
bgl_display_string(BgL_arg1607z00_3207, BgL_portz00_45);} } 

}



/* &display-symbol */
obj_t BGl_z62displayzd2symbolzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3928, obj_t BgL_objz00_3929, obj_t BgL_portz00_3930)
{
{ /* Ieee/output.scm 772 */
{ /* Ieee/output.scm 773 */
 obj_t BgL_auxz00_6246; obj_t BgL_auxz00_6239;
if(
OUTPUT_PORTP(BgL_portz00_3930))
{ /* Ieee/output.scm 773 */
BgL_auxz00_6246 = BgL_portz00_3930
; }  else 
{ 
 obj_t BgL_auxz00_6249;
BgL_auxz00_6249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27404L), BGl_string2648z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3930); 
FAILURE(BgL_auxz00_6249,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_objz00_3929))
{ /* Ieee/output.scm 773 */
BgL_auxz00_6239 = BgL_objz00_3929
; }  else 
{ 
 obj_t BgL_auxz00_6242;
BgL_auxz00_6242 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27404L), BGl_string2648z00zz__r4_output_6_10_3z00, BGl_string2649z00zz__r4_output_6_10_3z00, BgL_objz00_3929); 
FAILURE(BgL_auxz00_6242,BFALSE,BFALSE);} 
return 
BGl_displayzd2symbolzd2zz__r4_output_6_10_3z00(BgL_auxz00_6239, BgL_auxz00_6246);} } 

}



/* write-symbol */
BGL_EXPORTED_DEF obj_t BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_46, obj_t BgL_portz00_47)
{
{ /* Ieee/output.scm 778 */
{ /* Ieee/output.scm 785 */
 obj_t BgL_strz00_2002;
BgL_strz00_2002 = 
SYMBOL_TO_STRING(BgL_objz00_46); 
{ /* Ieee/output.scm 785 */
 long BgL_lenz00_2003;
BgL_lenz00_2003 = 
STRING_LENGTH(BgL_strz00_2002); 
{ /* Ieee/output.scm 786 */
 long BgL_lenzd21zd2_2004;
BgL_lenzd21zd2_2004 = 
(BgL_lenz00_2003-1L); 
{ /* Ieee/output.scm 787 */

{ 
 long BgL_iz00_2006; bool_t BgL_az00_2007;
BgL_iz00_2006 = 0L; 
BgL_az00_2007 = ((bool_t)0); 
BgL_zc3z04anonymousza31608ze3z87_2008:
if(
(BgL_iz00_2006==BgL_lenz00_2003))
{ /* Ieee/output.scm 790 */
if(BgL_az00_2007)
{ /* Ieee/output.scm 792 */
return 
bgl_display_string(BgL_strz00_2002, BgL_portz00_47);}  else 
{ /* Ieee/output.scm 793 */
 bool_t BgL_test3196z00_6261;
if(
(BgL_objz00_46==BGl_symbol2650z00zz__r4_output_6_10_3z00))
{ /* Ieee/output.scm 793 */
BgL_test3196z00_6261 = ((bool_t)1)
; }  else 
{ /* Ieee/output.scm 793 */
BgL_test3196z00_6261 = 
(BgL_objz00_46==BGl_symbol2652z00zz__r4_output_6_10_3z00)
; } 
if(BgL_test3196z00_6261)
{ /* Ieee/output.scm 793 */
return 
bgl_display_string(BgL_strz00_2002, BgL_portz00_47);}  else 
{ /* Ieee/output.scm 793 */
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6267;
BgL_tmpz00_6267 = 
symbol_for_read(BgL_strz00_2002); 
bgl_display_string(BgL_tmpz00_6267, BgL_portz00_47); } 
return 
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47);} } }  else 
{ /* Ieee/output.scm 795 */
 unsigned char BgL_cz00_2011;
{ /* Ieee/output.scm 795 */
 long BgL_l2245z00_3990;
BgL_l2245z00_3990 = 
STRING_LENGTH(BgL_strz00_2002); 
if(
BOUND_CHECK(BgL_iz00_2006, BgL_l2245z00_3990))
{ /* Ieee/output.scm 795 */
BgL_cz00_2011 = 
STRING_REF(BgL_strz00_2002, BgL_iz00_2006); }  else 
{ 
 obj_t BgL_auxz00_6275;
BgL_auxz00_6275 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(28116L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL_strz00_2002, 
(int)(BgL_l2245z00_3990), 
(int)(BgL_iz00_2006)); 
FAILURE(BgL_auxz00_6275,BFALSE,BFALSE);} } 
{ 

switch( BgL_cz00_2011) { case ((unsigned char)10) : 
case ((unsigned char)9) : 
case ((unsigned char)13) : 
case ((unsigned char)'`') : 
case ((unsigned char)'\'') : 
case ((unsigned char)'"') : 
case ((unsigned char)'#') : 
case ((unsigned char)'\\') : 
case ((unsigned char)';') : 
case ((unsigned char)'(') : 
case ((unsigned char)')') : 
case ((unsigned char)'[') : 
case ((unsigned char)']') : 
case ((unsigned char)'{') : 
case ((unsigned char)'}') : 
case ((unsigned char)',') : 
case ((unsigned char)'|') : 

bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6282;
BgL_tmpz00_6282 = 
symbol_for_read(BgL_strz00_2002); 
bgl_display_string(BgL_tmpz00_6282, BgL_portz00_47); } 
return 
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47);break;case ((unsigned char)':') : 

if(
(BgL_iz00_2006==0L))
{ /* Ieee/output.scm 803 */
 bool_t BgL_test3200z00_6288;
if(
(BgL_lenzd21zd2_2004>2L))
{ /* Ieee/output.scm 804 */
 unsigned char BgL_tmpz00_6291;
{ /* Ieee/output.scm 804 */
 long BgL_i2248z00_3993;
BgL_i2248z00_3993 = 
(BgL_iz00_2006+1L); 
{ /* Ieee/output.scm 804 */
 long BgL_l2249z00_3994;
BgL_l2249z00_3994 = 
STRING_LENGTH(BgL_strz00_2002); 
if(
BOUND_CHECK(BgL_i2248z00_3993, BgL_l2249z00_3994))
{ /* Ieee/output.scm 804 */
BgL_tmpz00_6291 = 
STRING_REF(BgL_strz00_2002, BgL_i2248z00_3993)
; }  else 
{ 
 obj_t BgL_auxz00_6297;
BgL_auxz00_6297 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(28345L), BGl_string2597z00zz__r4_output_6_10_3z00, BgL_strz00_2002, 
(int)(BgL_l2249z00_3994), 
(int)(BgL_i2248z00_3993)); 
FAILURE(BgL_auxz00_6297,BFALSE,BFALSE);} } } 
BgL_test3200z00_6288 = 
(BgL_tmpz00_6291==((unsigned char)':')); }  else 
{ /* Ieee/output.scm 803 */
BgL_test3200z00_6288 = ((bool_t)0)
; } 
if(BgL_test3200z00_6288)
{ 
 long BgL_iz00_6304;
BgL_iz00_6304 = 
(BgL_iz00_2006+2L); 
BgL_iz00_2006 = BgL_iz00_6304; 
goto BgL_zc3z04anonymousza31608ze3z87_2008;}  else 
{ /* Ieee/output.scm 803 */
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6307;
BgL_tmpz00_6307 = 
symbol_for_read(BgL_strz00_2002); 
bgl_display_string(BgL_tmpz00_6307, BgL_portz00_47); } 
return 
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47);} }  else 
{ /* Ieee/output.scm 802 */
if(
(BgL_iz00_2006==BgL_lenzd21zd2_2004))
{ /* Ieee/output.scm 807 */
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6314;
BgL_tmpz00_6314 = 
symbol_for_read(BgL_strz00_2002); 
bgl_display_string(BgL_tmpz00_6314, BgL_portz00_47); } 
return 
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47);}  else 
{ 
 long BgL_iz00_6318;
BgL_iz00_6318 = 
(BgL_iz00_2006+1L); 
BgL_iz00_2006 = BgL_iz00_6318; 
goto BgL_zc3z04anonymousza31608ze3z87_2008;} } break;case ((unsigned char)'.') : 

if(
(BgL_lenz00_2003==1L))
{ /* Ieee/output.scm 812 */
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6323;
BgL_tmpz00_6323 = 
symbol_for_read(BgL_strz00_2002); 
bgl_display_string(BgL_tmpz00_6323, BgL_portz00_47); } 
return 
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47);}  else 
{ 
 long BgL_iz00_6327;
BgL_iz00_6327 = 
(BgL_iz00_2006+1L); 
BgL_iz00_2006 = BgL_iz00_6327; 
goto BgL_zc3z04anonymousza31608ze3z87_2008;} break;
default: 
{ /* Ieee/output.scm 816 */
 bool_t BgL_test3205z00_6329;
if(
(BgL_cz00_2011<=((unsigned char)' ')))
{ /* Ieee/output.scm 816 */
BgL_test3205z00_6329 = ((bool_t)1)
; }  else 
{ /* Ieee/output.scm 816 */
BgL_test3205z00_6329 = 
(BgL_cz00_2011>=((unsigned char)''))
; } 
if(BgL_test3205z00_6329)
{ /* Ieee/output.scm 816 */
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47); 
{ /* Ieee/output.scm 833 */
 obj_t BgL_tmpz00_6334;
BgL_tmpz00_6334 = 
symbol_for_read(BgL_strz00_2002); 
bgl_display_string(BgL_tmpz00_6334, BgL_portz00_47); } 
return 
bgl_display_string(BGl_string2654z00zz__r4_output_6_10_3z00, BgL_portz00_47);}  else 
{ /* Ieee/output.scm 818 */
 long BgL_arg1627z00_2030; bool_t BgL_arg1628z00_2031;
BgL_arg1627z00_2030 = 
(BgL_iz00_2006+1L); 
if(BgL_az00_2007)
{ /* Ieee/output.scm 819 */
BgL_arg1628z00_2031 = BgL_az00_2007; }  else 
{ /* Ieee/output.scm 820 */
 bool_t BgL__ortest_1053z00_2033;
if(
isdigit(BgL_cz00_2011))
{ /* Ieee/output.scm 820 */
BgL__ortest_1053z00_2033 = ((bool_t)0); }  else 
{ /* Ieee/output.scm 820 */
if(
(BgL_cz00_2011==((unsigned char)'e')))
{ /* Ieee/output.scm 821 */
BgL__ortest_1053z00_2033 = ((bool_t)0); }  else 
{ /* Ieee/output.scm 821 */
if(
(BgL_cz00_2011==((unsigned char)'E')))
{ /* Ieee/output.scm 822 */
BgL__ortest_1053z00_2033 = ((bool_t)0); }  else 
{ /* Ieee/output.scm 822 */
if(
(BgL_cz00_2011==((unsigned char)'-')))
{ /* Ieee/output.scm 823 */
BgL__ortest_1053z00_2033 = ((bool_t)0); }  else 
{ /* Ieee/output.scm 823 */
if(
(BgL_cz00_2011==((unsigned char)'+')))
{ /* Ieee/output.scm 824 */
BgL__ortest_1053z00_2033 = ((bool_t)0); }  else 
{ /* Ieee/output.scm 824 */
BgL__ortest_1053z00_2033 = ((bool_t)1); } } } } } 
if(BgL__ortest_1053z00_2033)
{ /* Ieee/output.scm 820 */
BgL_arg1628z00_2031 = BgL__ortest_1053z00_2033; }  else 
{ /* Ieee/output.scm 820 */
if(
(BgL_iz00_2006==0L))
{ /* Ieee/output.scm 826 */
 bool_t BgL__ortest_1059z00_2035;
BgL__ortest_1059z00_2035 = 
(BgL_cz00_2011==((unsigned char)'e')); 
if(BgL__ortest_1059z00_2035)
{ /* Ieee/output.scm 826 */
BgL_arg1628z00_2031 = BgL__ortest_1059z00_2035; }  else 
{ /* Ieee/output.scm 826 */
BgL_arg1628z00_2031 = 
(BgL_cz00_2011==((unsigned char)'E')); } }  else 
{ /* Ieee/output.scm 825 */
BgL_arg1628z00_2031 = ((bool_t)0); } } } 
{ 
 bool_t BgL_az00_6357; long BgL_iz00_6356;
BgL_iz00_6356 = BgL_arg1627z00_2030; 
BgL_az00_6357 = BgL_arg1628z00_2031; 
BgL_az00_2007 = BgL_az00_6357; 
BgL_iz00_2006 = BgL_iz00_6356; 
goto BgL_zc3z04anonymousza31608ze3z87_2008;} } } } } } } } } } } } 

}



/* &write-symbol */
obj_t BGl_z62writezd2symbolzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3931, obj_t BgL_objz00_3932, obj_t BgL_portz00_3933)
{
{ /* Ieee/output.scm 778 */
{ /* Ieee/output.scm 785 */
 obj_t BgL_auxz00_6366; obj_t BgL_auxz00_6359;
if(
OUTPUT_PORTP(BgL_portz00_3933))
{ /* Ieee/output.scm 785 */
BgL_auxz00_6366 = BgL_portz00_3933
; }  else 
{ 
 obj_t BgL_auxz00_6369;
BgL_auxz00_6369 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27836L), BGl_string2655z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3933); 
FAILURE(BgL_auxz00_6369,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_objz00_3932))
{ /* Ieee/output.scm 785 */
BgL_auxz00_6359 = BgL_objz00_3932
; }  else 
{ 
 obj_t BgL_auxz00_6362;
BgL_auxz00_6362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(27836L), BGl_string2655z00zz__r4_output_6_10_3z00, BGl_string2649z00zz__r4_output_6_10_3z00, BgL_objz00_3932); 
FAILURE(BgL_auxz00_6362,BFALSE,BFALSE);} 
return 
BGl_writezd2symbolzd2zz__r4_output_6_10_3z00(BgL_auxz00_6359, BgL_auxz00_6366);} } 

}



/* display-string */
BGL_EXPORTED_DEF obj_t BGl_displayzd2stringzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_48, obj_t BgL_portz00_49)
{
{ /* Ieee/output.scm 832 */
return 
bgl_display_string(BgL_objz00_48, BgL_portz00_49);} 

}



/* &display-string */
obj_t BGl_z62displayzd2stringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3934, obj_t BgL_objz00_3935, obj_t BgL_portz00_3936)
{
{ /* Ieee/output.scm 832 */
{ /* Ieee/output.scm 833 */
 obj_t BgL_auxz00_6382; obj_t BgL_auxz00_6375;
if(
OUTPUT_PORTP(BgL_portz00_3936))
{ /* Ieee/output.scm 833 */
BgL_auxz00_6382 = BgL_portz00_3936
; }  else 
{ 
 obj_t BgL_auxz00_6385;
BgL_auxz00_6385 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(29178L), BGl_string2656z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3936); 
FAILURE(BgL_auxz00_6385,BFALSE,BFALSE);} 
if(
STRINGP(BgL_objz00_3935))
{ /* Ieee/output.scm 833 */
BgL_auxz00_6375 = BgL_objz00_3935
; }  else 
{ 
 obj_t BgL_auxz00_6378;
BgL_auxz00_6378 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(29178L), BGl_string2656z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_objz00_3935); 
FAILURE(BgL_auxz00_6378,BFALSE,BFALSE);} 
return 
BGl_displayzd2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_6375, BgL_auxz00_6382);} } 

}



/* display-substring */
BGL_EXPORTED_DEF obj_t BGl_displayzd2substringzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_50, long BgL_startz00_51, long BgL_endz00_52, obj_t BgL_portz00_53)
{
{ /* Ieee/output.scm 838 */
{ /* Ieee/output.scm 839 */
 bool_t BgL_test3220z00_6390;
if(
(BgL_endz00_52>=BgL_startz00_51))
{ /* Ieee/output.scm 840 */
 bool_t BgL_test3222z00_6393;
{ /* Ieee/output.scm 840 */
 long BgL_tmpz00_6394;
BgL_tmpz00_6394 = 
(
STRING_LENGTH(BgL_objz00_50)+1L); 
BgL_test3222z00_6393 = 
BOUND_CHECK(BgL_endz00_52, BgL_tmpz00_6394); } 
if(BgL_test3222z00_6393)
{ /* Ieee/output.scm 840 */
BgL_test3220z00_6390 = 
(BgL_startz00_51>=0L)
; }  else 
{ /* Ieee/output.scm 840 */
BgL_test3220z00_6390 = ((bool_t)0)
; } }  else 
{ /* Ieee/output.scm 839 */
BgL_test3220z00_6390 = ((bool_t)0)
; } 
if(BgL_test3220z00_6390)
{ /* Ieee/output.scm 839 */
return 
bgl_display_substring(BgL_objz00_50, BgL_startz00_51, BgL_endz00_52, BgL_portz00_53);}  else 
{ /* Ieee/output.scm 844 */
 obj_t BgL_arg1641z00_4351;
{ /* Ieee/output.scm 844 */
 obj_t BgL_list1642z00_4352;
{ /* Ieee/output.scm 844 */
 obj_t BgL_arg1643z00_4353;
BgL_arg1643z00_4353 = 
MAKE_YOUNG_PAIR(
BINT(BgL_endz00_52), BNIL); 
BgL_list1642z00_4352 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_51), BgL_arg1643z00_4353); } 
BgL_arg1641z00_4351 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string2657z00zz__r4_output_6_10_3z00, BgL_list1642z00_4352); } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2658z00zz__r4_output_6_10_3z00, BgL_arg1641z00_4351, BgL_objz00_50);} } } 

}



/* &display-substring */
obj_t BGl_z62displayzd2substringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3937, obj_t BgL_objz00_3938, obj_t BgL_startz00_3939, obj_t BgL_endz00_3940, obj_t BgL_portz00_3941)
{
{ /* Ieee/output.scm 838 */
{ /* Ieee/output.scm 839 */
 obj_t BgL_auxz00_6431; long BgL_auxz00_6422; long BgL_auxz00_6413; obj_t BgL_auxz00_6406;
if(
OUTPUT_PORTP(BgL_portz00_3941))
{ /* Ieee/output.scm 839 */
BgL_auxz00_6431 = BgL_portz00_3941
; }  else 
{ 
 obj_t BgL_auxz00_6434;
BgL_auxz00_6434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(29486L), BGl_string2660z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3941); 
FAILURE(BgL_auxz00_6434,BFALSE,BFALSE);} 
{ /* Ieee/output.scm 839 */
 obj_t BgL_tmpz00_6423;
if(
INTEGERP(BgL_endz00_3940))
{ /* Ieee/output.scm 839 */
BgL_tmpz00_6423 = BgL_endz00_3940
; }  else 
{ 
 obj_t BgL_auxz00_6426;
BgL_auxz00_6426 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(29486L), BGl_string2660z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_endz00_3940); 
FAILURE(BgL_auxz00_6426,BFALSE,BFALSE);} 
BgL_auxz00_6422 = 
(long)CINT(BgL_tmpz00_6423); } 
{ /* Ieee/output.scm 839 */
 obj_t BgL_tmpz00_6414;
if(
INTEGERP(BgL_startz00_3939))
{ /* Ieee/output.scm 839 */
BgL_tmpz00_6414 = BgL_startz00_3939
; }  else 
{ 
 obj_t BgL_auxz00_6417;
BgL_auxz00_6417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(29486L), BGl_string2660z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_startz00_3939); 
FAILURE(BgL_auxz00_6417,BFALSE,BFALSE);} 
BgL_auxz00_6413 = 
(long)CINT(BgL_tmpz00_6414); } 
if(
STRINGP(BgL_objz00_3938))
{ /* Ieee/output.scm 839 */
BgL_auxz00_6406 = BgL_objz00_3938
; }  else 
{ 
 obj_t BgL_auxz00_6409;
BgL_auxz00_6409 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(29486L), BGl_string2660z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_objz00_3938); 
FAILURE(BgL_auxz00_6409,BFALSE,BFALSE);} 
return 
BGl_displayzd2substringzd2zz__r4_output_6_10_3z00(BgL_auxz00_6406, BgL_auxz00_6413, BgL_auxz00_6422, BgL_auxz00_6431);} } 

}



/* write-string */
BGL_EXPORTED_DEF obj_t BGl_writezd2stringzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_54, obj_t BgL_portz00_55)
{
{ /* Ieee/output.scm 850 */
if(
BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00())
{ /* Ieee/output.scm 854 */
 obj_t BgL_strz00_3317;
BgL_strz00_3317 = 
string_for_read(BgL_objz00_54); 
{ /* Ieee/output.scm 855 */
 obj_t BgL_escz00_3318;
{ /* Ieee/output.scm 856 */
 obj_t BgL_tmpz00_3321;
{ /* Ieee/output.scm 856 */
 int BgL_tmpz00_6442;
BgL_tmpz00_6442 = 
(int)(1L); 
BgL_tmpz00_3321 = 
BGL_MVALUES_VAL(BgL_tmpz00_6442); } 
{ /* Ieee/output.scm 856 */
 int BgL_tmpz00_6445;
BgL_tmpz00_6445 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6445, BUNSPEC); } 
BgL_escz00_3318 = BgL_tmpz00_3321; } 
return 
bgl_write_string(BgL_strz00_3317, 
CBOOL(BgL_escz00_3318), BgL_portz00_55);} }  else 
{ /* Ieee/output.scm 851 */
return 
bgl_write_string(
string_for_read(BgL_objz00_54), ((bool_t)0), BgL_portz00_55);} } 

}



/* &write-string */
obj_t BGl_z62writezd2stringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3942, obj_t BgL_objz00_3943, obj_t BgL_portz00_3944)
{
{ /* Ieee/output.scm 850 */
{ /* Ieee/output.scm 851 */
 obj_t BgL_auxz00_6459; obj_t BgL_auxz00_6452;
if(
OUTPUT_PORTP(BgL_portz00_3944))
{ /* Ieee/output.scm 851 */
BgL_auxz00_6459 = BgL_portz00_3944
; }  else 
{ 
 obj_t BgL_auxz00_6462;
BgL_auxz00_6462 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(30004L), BGl_string2661z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3944); 
FAILURE(BgL_auxz00_6462,BFALSE,BFALSE);} 
if(
STRINGP(BgL_objz00_3943))
{ /* Ieee/output.scm 851 */
BgL_auxz00_6452 = BgL_objz00_3943
; }  else 
{ 
 obj_t BgL_auxz00_6455;
BgL_auxz00_6455 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(30004L), BGl_string2661z00zz__r4_output_6_10_3z00, BGl_string2600z00zz__r4_output_6_10_3z00, BgL_objz00_3943); 
FAILURE(BgL_auxz00_6455,BFALSE,BFALSE);} 
return 
BGl_writezd2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_6452, BgL_auxz00_6459);} } 

}



/* display-fixnum */
BGL_EXPORTED_DEF obj_t BGl_displayzd2fixnumzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_58, obj_t BgL_portz00_59)
{
{ /* Ieee/output.scm 871 */
BGL_TAIL return 
bgl_display_fixnum(BgL_objz00_58, BgL_portz00_59);} 

}



/* &display-fixnum */
obj_t BGl_z62displayzd2fixnumzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3945, obj_t BgL_objz00_3946, obj_t BgL_portz00_3947)
{
{ /* Ieee/output.scm 871 */
{ /* Ieee/output.scm 872 */
 obj_t BgL_auxz00_6475; obj_t BgL_auxz00_6468;
if(
OUTPUT_PORTP(BgL_portz00_3947))
{ /* Ieee/output.scm 872 */
BgL_auxz00_6475 = BgL_portz00_3947
; }  else 
{ 
 obj_t BgL_auxz00_6478;
BgL_auxz00_6478 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(30890L), BGl_string2662z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3947); 
FAILURE(BgL_auxz00_6478,BFALSE,BFALSE);} 
if(
INTEGERP(BgL_objz00_3946))
{ /* Ieee/output.scm 872 */
BgL_auxz00_6468 = BgL_objz00_3946
; }  else 
{ 
 obj_t BgL_auxz00_6471;
BgL_auxz00_6471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(30890L), BGl_string2662z00zz__r4_output_6_10_3z00, BGl_string2576z00zz__r4_output_6_10_3z00, BgL_objz00_3946); 
FAILURE(BgL_auxz00_6471,BFALSE,BFALSE);} 
return 
BGl_displayzd2fixnumzd2zz__r4_output_6_10_3z00(BgL_auxz00_6468, BgL_auxz00_6475);} } 

}



/* display-elong */
BGL_EXPORTED_DEF obj_t BGl_displayzd2elongzd2zz__r4_output_6_10_3z00(long BgL_objz00_60, obj_t BgL_portz00_61)
{
{ /* Ieee/output.scm 877 */
BGL_TAIL return 
bgl_display_elong(BgL_objz00_60, BgL_portz00_61);} 

}



/* &display-elong */
obj_t BGl_z62displayzd2elongzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3948, obj_t BgL_objz00_3949, obj_t BgL_portz00_3950)
{
{ /* Ieee/output.scm 877 */
{ /* Ieee/output.scm 878 */
 obj_t BgL_auxz00_6493; long BgL_auxz00_6484;
if(
OUTPUT_PORTP(BgL_portz00_3950))
{ /* Ieee/output.scm 878 */
BgL_auxz00_6493 = BgL_portz00_3950
; }  else 
{ 
 obj_t BgL_auxz00_6496;
BgL_auxz00_6496 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(31184L), BGl_string2663z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3950); 
FAILURE(BgL_auxz00_6496,BFALSE,BFALSE);} 
{ /* Ieee/output.scm 878 */
 obj_t BgL_tmpz00_6485;
if(
ELONGP(BgL_objz00_3949))
{ /* Ieee/output.scm 878 */
BgL_tmpz00_6485 = BgL_objz00_3949
; }  else 
{ 
 obj_t BgL_auxz00_6488;
BgL_auxz00_6488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(31184L), BGl_string2663z00zz__r4_output_6_10_3z00, BGl_string2664z00zz__r4_output_6_10_3z00, BgL_objz00_3949); 
FAILURE(BgL_auxz00_6488,BFALSE,BFALSE);} 
BgL_auxz00_6484 = 
BELONG_TO_LONG(BgL_tmpz00_6485); } 
return 
BGl_displayzd2elongzd2zz__r4_output_6_10_3z00(BgL_auxz00_6484, BgL_auxz00_6493);} } 

}



/* display-flonum */
BGL_EXPORTED_DEF obj_t BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_62, obj_t BgL_portz00_63)
{
{ /* Ieee/output.scm 883 */
{ /* Ieee/output.scm 884 */
 obj_t BgL_arg1650z00_3327;
BgL_arg1650z00_3327 = 
bgl_real_to_string(
REAL_TO_DOUBLE(BgL_objz00_62)); 
return 
bgl_display_string(BgL_arg1650z00_3327, BgL_portz00_63);} } 

}



/* &display-flonum */
obj_t BGl_z62displayzd2flonumzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3951, obj_t BgL_objz00_3952, obj_t BgL_portz00_3953)
{
{ /* Ieee/output.scm 883 */
{ /* Ieee/output.scm 884 */
 obj_t BgL_auxz00_6511; obj_t BgL_auxz00_6504;
if(
OUTPUT_PORTP(BgL_portz00_3953))
{ /* Ieee/output.scm 884 */
BgL_auxz00_6511 = BgL_portz00_3953
; }  else 
{ 
 obj_t BgL_auxz00_6514;
BgL_auxz00_6514 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(31487L), BGl_string2665z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3953); 
FAILURE(BgL_auxz00_6514,BFALSE,BFALSE);} 
if(
REALP(BgL_objz00_3952))
{ /* Ieee/output.scm 884 */
BgL_auxz00_6504 = BgL_objz00_3952
; }  else 
{ 
 obj_t BgL_auxz00_6507;
BgL_auxz00_6507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(31487L), BGl_string2665z00zz__r4_output_6_10_3z00, BGl_string2666z00zz__r4_output_6_10_3z00, BgL_objz00_3952); 
FAILURE(BgL_auxz00_6507,BFALSE,BFALSE);} 
return 
BGl_displayzd2flonumzd2zz__r4_output_6_10_3z00(BgL_auxz00_6504, BgL_auxz00_6511);} } 

}



/* display-ucs2string */
BGL_EXPORTED_DEF obj_t BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_64, obj_t BgL_portz00_65)
{
{ /* Ieee/output.scm 889 */
BGL_TAIL return 
bgl_display_ucs2string(BgL_objz00_64, BgL_portz00_65);} 

}



/* &display-ucs2string */
obj_t BGl_z62displayzd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3954, obj_t BgL_objz00_3955, obj_t BgL_portz00_3956)
{
{ /* Ieee/output.scm 889 */
{ /* Ieee/output.scm 890 */
 obj_t BgL_auxz00_6527; obj_t BgL_auxz00_6520;
if(
OUTPUT_PORTP(BgL_portz00_3956))
{ /* Ieee/output.scm 890 */
BgL_auxz00_6527 = BgL_portz00_3956
; }  else 
{ 
 obj_t BgL_auxz00_6530;
BgL_auxz00_6530 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(31777L), BGl_string2667z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3956); 
FAILURE(BgL_auxz00_6530,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_objz00_3955))
{ /* Ieee/output.scm 890 */
BgL_auxz00_6520 = BgL_objz00_3955
; }  else 
{ 
 obj_t BgL_auxz00_6523;
BgL_auxz00_6523 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(31777L), BGl_string2667z00zz__r4_output_6_10_3z00, BGl_string2668z00zz__r4_output_6_10_3z00, BgL_objz00_3955); 
FAILURE(BgL_auxz00_6523,BFALSE,BFALSE);} 
return 
BGl_displayzd2ucs2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_6520, BgL_auxz00_6527);} } 

}



/* write-ucs2string */
BGL_EXPORTED_DEF obj_t BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_66, obj_t BgL_portz00_67)
{
{ /* Ieee/output.scm 895 */
return 
bgl_write_utf8string(
string_for_read(
ucs2_string_to_utf8_string(BgL_objz00_66)), BgL_portz00_67);} 

}



/* &write-ucs2string */
obj_t BGl_z62writezd2ucs2stringzb0zz__r4_output_6_10_3z00(obj_t BgL_envz00_3957, obj_t BgL_objz00_3958, obj_t BgL_portz00_3959)
{
{ /* Ieee/output.scm 895 */
{ /* Ieee/output.scm 896 */
 obj_t BgL_auxz00_6545; obj_t BgL_auxz00_6538;
if(
OUTPUT_PORTP(BgL_portz00_3959))
{ /* Ieee/output.scm 896 */
BgL_auxz00_6545 = BgL_portz00_3959
; }  else 
{ 
 obj_t BgL_auxz00_6548;
BgL_auxz00_6548 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(32107L), BGl_string2669z00zz__r4_output_6_10_3z00, BGl_string2563z00zz__r4_output_6_10_3z00, BgL_portz00_3959); 
FAILURE(BgL_auxz00_6548,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_objz00_3958))
{ /* Ieee/output.scm 896 */
BgL_auxz00_6538 = BgL_objz00_3958
; }  else 
{ 
 obj_t BgL_auxz00_6541;
BgL_auxz00_6541 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(32107L), BGl_string2669z00zz__r4_output_6_10_3z00, BGl_string2668z00zz__r4_output_6_10_3z00, BgL_objz00_3958); 
FAILURE(BgL_auxz00_6541,BFALSE,BFALSE);} 
return 
BGl_writezd2ucs2stringzd2zz__r4_output_6_10_3z00(BgL_auxz00_6538, BgL_auxz00_6545);} } 

}



/* write-mutex */
obj_t BGl_writezd2mutexzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_72, obj_t BgL_portz00_73)
{
{ /* Ieee/output.scm 916 */
bgl_display_string(BGl_string2670z00zz__r4_output_6_10_3z00, BgL_portz00_73); 
{ /* Ieee/output.scm 918 */
 obj_t BgL_arg1656z00_2071;
{ /* Ieee/output.scm 918 */
 obj_t BgL_objz00_3346;
if(
BGL_MUTEXP(BgL_objz00_72))
{ /* Ieee/output.scm 918 */
BgL_objz00_3346 = BgL_objz00_72; }  else 
{ 
 obj_t BgL_auxz00_6556;
BgL_auxz00_6556 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(33133L), BGl_string2671z00zz__r4_output_6_10_3z00, BGl_string2672z00zz__r4_output_6_10_3z00, BgL_objz00_72); 
FAILURE(BgL_auxz00_6556,BFALSE,BFALSE);} 
BgL_arg1656z00_2071 = 
BGL_MUTEX_NAME(BgL_objz00_3346); } 
bgl_display_obj(BgL_arg1656z00_2071, BgL_portz00_73); } 
bgl_display_string(BGl_string2673z00zz__r4_output_6_10_3z00, BgL_portz00_73); 
{ /* Ieee/output.scm 920 */
 obj_t BgL_arg1657z00_2072;
{ /* Ieee/output.scm 920 */
 obj_t BgL_tmpz00_6563;
if(
BGL_MUTEXP(BgL_objz00_72))
{ /* Ieee/output.scm 920 */
BgL_tmpz00_6563 = BgL_objz00_72
; }  else 
{ 
 obj_t BgL_auxz00_6566;
BgL_auxz00_6566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(33194L), BGl_string2671z00zz__r4_output_6_10_3z00, BGl_string2672z00zz__r4_output_6_10_3z00, BgL_objz00_72); 
FAILURE(BgL_auxz00_6566,BFALSE,BFALSE);} 
BgL_arg1657z00_2072 = 
BGL_MUTEX_BACKEND(BgL_tmpz00_6563); } 
bgl_display_obj(BgL_arg1657z00_2072, BgL_portz00_73); } 
return 
bgl_display_string(BGl_string2621z00zz__r4_output_6_10_3z00, BgL_portz00_73);} 

}



/* write/display-structure */
obj_t BGl_writezf2displayzd2structurez20zz__r4_output_6_10_3z00(obj_t BgL_objz00_81, obj_t BgL_portz00_82, obj_t BgL_dispz00_83)
{
{ /* Ieee/output.scm 950 */
bgl_display_char(((unsigned char)'#'), BgL_portz00_82); 
bgl_display_char(((unsigned char)'{'), BgL_portz00_82); 
{ /* Ieee/output.scm 953 */
 obj_t BgL_arg1664z00_2076;
{ /* Ieee/output.scm 953 */
 obj_t BgL_res2206z00_3375;
{ /* Ieee/output.scm 953 */
 obj_t BgL_aux2502z00_4247;
BgL_aux2502z00_4247 = 
STRUCT_KEY(BgL_objz00_81); 
if(
SYMBOLP(BgL_aux2502z00_4247))
{ /* Ieee/output.scm 953 */
BgL_res2206z00_3375 = BgL_aux2502z00_4247; }  else 
{ 
 obj_t BgL_auxz00_6578;
BgL_auxz00_6578 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(34692L), BGl_string2674z00zz__r4_output_6_10_3z00, BGl_string2649z00zz__r4_output_6_10_3z00, BgL_aux2502z00_4247); 
FAILURE(BgL_auxz00_6578,BFALSE,BFALSE);} } 
BgL_arg1664z00_2076 = BgL_res2206z00_3375; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_83, 2))
{ /* Ieee/output.scm 953 */
BGL_PROCEDURE_CALL2(BgL_dispz00_83, BgL_arg1664z00_2076, BgL_portz00_82); }  else 
{ /* Ieee/output.scm 953 */
FAILURE(BGl_string2675z00zz__r4_output_6_10_3z00,BGl_list2676z00zz__r4_output_6_10_3z00,BgL_dispz00_83);} } 
if(
(0L==
(long)(
STRUCT_LENGTH(BgL_objz00_81))))
{ /* Ieee/output.scm 954 */
return 
bgl_display_char(((unsigned char)'}'), BgL_portz00_82);}  else 
{ /* Ieee/output.scm 956 */
 long BgL_lenz00_2079;
BgL_lenz00_2079 = 
(
(long)(
STRUCT_LENGTH(BgL_objz00_81))-1L); 
bgl_display_char(((unsigned char)' '), BgL_portz00_82); 
{ 
 long BgL_iz00_2081;
BgL_iz00_2081 = 0L; 
BgL_zc3z04anonymousza31669ze3z87_2082:
if(
(BgL_iz00_2081==BgL_lenz00_2079))
{ /* Ieee/output.scm 960 */
{ /* Ieee/output.scm 961 */
 obj_t BgL_arg1675z00_2084;
BgL_arg1675z00_2084 = 
STRUCT_REF(BgL_objz00_81, 
(int)(BgL_iz00_2081)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_83, 2))
{ /* Ieee/output.scm 961 */
BGL_PROCEDURE_CALL2(BgL_dispz00_83, BgL_arg1675z00_2084, BgL_portz00_82); }  else 
{ /* Ieee/output.scm 961 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2686z00zz__r4_output_6_10_3z00,BgL_dispz00_83);} } 
return 
bgl_display_char(((unsigned char)'}'), BgL_portz00_82);}  else 
{ /* Ieee/output.scm 960 */
{ /* Ieee/output.scm 964 */
 obj_t BgL_arg1676z00_2085;
BgL_arg1676z00_2085 = 
STRUCT_REF(BgL_objz00_81, 
(int)(BgL_iz00_2081)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_83, 2))
{ /* Ieee/output.scm 964 */
BGL_PROCEDURE_CALL2(BgL_dispz00_83, BgL_arg1676z00_2085, BgL_portz00_82); }  else 
{ /* Ieee/output.scm 964 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2689z00zz__r4_output_6_10_3z00,BgL_dispz00_83);} } 
bgl_display_char(((unsigned char)' '), BgL_portz00_82); 
{ 
 long BgL_iz00_6623;
BgL_iz00_6623 = 
(1L+BgL_iz00_2081); 
BgL_iz00_2081 = BgL_iz00_6623; 
goto BgL_zc3z04anonymousza31669ze3z87_2082;} } } } } 

}



/* write/display-vector */
obj_t BGl_writezf2displayzd2vectorz20zz__r4_output_6_10_3z00(obj_t BgL_objz00_84, obj_t BgL_portz00_85, obj_t BgL_dispz00_86)
{
{ /* Ieee/output.scm 971 */
bgl_display_char(((unsigned char)'#'), BgL_portz00_85); 
{ /* Ieee/output.scm 973 */
 int BgL_tagz00_2090;
BgL_tagz00_2090 = 
VECTOR_TAG(BgL_objz00_84); 
if(
(
(long)(BgL_tagz00_2090)>0L))
{ /* Ieee/output.scm 974 */
if(
(
(long)(BgL_tagz00_2090)>=100L))
{ /* Ieee/output.scm 976 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_86, 2))
{ /* Ieee/output.scm 977 */
BGL_PROCEDURE_CALL2(BgL_dispz00_86, 
BINT(BgL_tagz00_2090), BgL_portz00_85); }  else 
{ /* Ieee/output.scm 977 */
FAILURE(BGl_string2692z00zz__r4_output_6_10_3z00,BGl_list2693z00zz__r4_output_6_10_3z00,BgL_dispz00_86);} }  else 
{ /* Ieee/output.scm 976 */
bgl_display_char(((unsigned char)'0'), BgL_portz00_85); 
if(
(
(long)(BgL_tagz00_2090)>=10L))
{ /* Ieee/output.scm 980 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_86, 2))
{ /* Ieee/output.scm 981 */
BGL_PROCEDURE_CALL2(BgL_dispz00_86, 
BINT(BgL_tagz00_2090), BgL_portz00_85); }  else 
{ /* Ieee/output.scm 981 */
FAILURE(BGl_string2692z00zz__r4_output_6_10_3z00,BGl_list2693z00zz__r4_output_6_10_3z00,BgL_dispz00_86);} }  else 
{ /* Ieee/output.scm 980 */
bgl_display_char(((unsigned char)'0'), BgL_portz00_85); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_86, 2))
{ /* Ieee/output.scm 984 */
BGL_PROCEDURE_CALL2(BgL_dispz00_86, 
BINT(BgL_tagz00_2090), BgL_portz00_85); }  else 
{ /* Ieee/output.scm 984 */
FAILURE(BGl_string2692z00zz__r4_output_6_10_3z00,BGl_list2693z00zz__r4_output_6_10_3z00,BgL_dispz00_86);} } } }  else 
{ /* Ieee/output.scm 974 */BFALSE; } } 
bgl_display_char(((unsigned char)'('), BgL_portz00_85); 
if(
(0L==
VECTOR_LENGTH(BgL_objz00_84)))
{ /* Ieee/output.scm 986 */
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_85);}  else 
{ /* Ieee/output.scm 988 */
 long BgL_lenz00_2096;
BgL_lenz00_2096 = 
(
VECTOR_LENGTH(BgL_objz00_84)-1L); 
{ 
 long BgL_iz00_2098;
BgL_iz00_2098 = 0L; 
BgL_zc3z04anonymousza31690ze3z87_2099:
if(
(BgL_iz00_2098==BgL_lenz00_2096))
{ /* Ieee/output.scm 991 */
{ /* Ieee/output.scm 992 */
 obj_t BgL_arg1692z00_2101;
{ /* Ieee/output.scm 992 */
 bool_t BgL_test3256z00_6674;
{ /* Ieee/output.scm 992 */
 long BgL_tmpz00_6675;
BgL_tmpz00_6675 = 
VECTOR_LENGTH(BgL_objz00_84); 
BgL_test3256z00_6674 = 
BOUND_CHECK(BgL_iz00_2098, BgL_tmpz00_6675); } 
if(BgL_test3256z00_6674)
{ /* Ieee/output.scm 992 */
BgL_arg1692z00_2101 = 
VECTOR_REF(BgL_objz00_84,BgL_iz00_2098); }  else 
{ 
 obj_t BgL_auxz00_6679;
BgL_auxz00_6679 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(35832L), BGl_string2696z00zz__r4_output_6_10_3z00, BgL_objz00_84, 
(int)(
VECTOR_LENGTH(BgL_objz00_84)), 
(int)(BgL_iz00_2098)); 
FAILURE(BgL_auxz00_6679,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_86, 2))
{ /* Ieee/output.scm 992 */
BGL_PROCEDURE_CALL2(BgL_dispz00_86, BgL_arg1692z00_2101, BgL_portz00_85); }  else 
{ /* Ieee/output.scm 992 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2697z00zz__r4_output_6_10_3z00,BgL_dispz00_86);} } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_85);}  else 
{ /* Ieee/output.scm 991 */
{ /* Ieee/output.scm 995 */
 obj_t BgL_arg1699z00_2102;
{ /* Ieee/output.scm 995 */
 bool_t BgL_test3258z00_6695;
{ /* Ieee/output.scm 995 */
 long BgL_tmpz00_6696;
BgL_tmpz00_6696 = 
VECTOR_LENGTH(BgL_objz00_84); 
BgL_test3258z00_6695 = 
BOUND_CHECK(BgL_iz00_2098, BgL_tmpz00_6696); } 
if(BgL_test3258z00_6695)
{ /* Ieee/output.scm 995 */
BgL_arg1699z00_2102 = 
VECTOR_REF(BgL_objz00_84,BgL_iz00_2098); }  else 
{ 
 obj_t BgL_auxz00_6700;
BgL_auxz00_6700 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(35903L), BGl_string2696z00zz__r4_output_6_10_3z00, BgL_objz00_84, 
(int)(
VECTOR_LENGTH(BgL_objz00_84)), 
(int)(BgL_iz00_2098)); 
FAILURE(BgL_auxz00_6700,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_86, 2))
{ /* Ieee/output.scm 995 */
BGL_PROCEDURE_CALL2(BgL_dispz00_86, BgL_arg1699z00_2102, BgL_portz00_85); }  else 
{ /* Ieee/output.scm 995 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2700z00zz__r4_output_6_10_3z00,BgL_dispz00_86);} } 
bgl_display_char(((unsigned char)' '), BgL_portz00_85); 
{ 
 long BgL_iz00_6716;
BgL_iz00_6716 = 
(1L+BgL_iz00_2098); 
BgL_iz00_2098 = BgL_iz00_6716; 
goto BgL_zc3z04anonymousza31690ze3z87_2099;} } } } } 

}



/* write/display-tvector */
obj_t BGl_writezf2displayzd2tvectorz20zz__r4_output_6_10_3z00(obj_t BgL_tvecz00_87, obj_t BgL_portz00_88, obj_t BgL_dispz00_89)
{
{ /* Ieee/output.scm 1002 */
{ /* Ieee/output.scm 1003 */
 obj_t BgL_tvectorzd2refzd2_2107; obj_t BgL_idz00_2108;
BgL_tvectorzd2refzd2_2107 = 
BGl_tvectorzd2refzd2zz__tvectorz00(BgL_tvecz00_87); 
BgL_idz00_2108 = 
BGl_tvectorzd2idzd2zz__tvectorz00(BgL_tvecz00_87); 
bgl_display_char(((unsigned char)'#'), BgL_portz00_88); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_89, 2))
{ /* Ieee/output.scm 1006 */
BGL_PROCEDURE_CALL2(BgL_dispz00_89, BgL_idz00_2108, BgL_portz00_88); }  else 
{ /* Ieee/output.scm 1006 */
FAILURE(BGl_string2703z00zz__r4_output_6_10_3z00,BGl_list2704z00zz__r4_output_6_10_3z00,BgL_dispz00_89);} 
bgl_display_char(((unsigned char)'('), BgL_portz00_88); 
if(
CBOOL(BgL_tvectorzd2refzd2_2107))
{ /* Ieee/output.scm 1008 */
if(
(0L==
(long)(
TVECTOR_LENGTH(BgL_tvecz00_87))))
{ /* Ieee/output.scm 1013 */
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_88);}  else 
{ /* Ieee/output.scm 1015 */
 long BgL_lenz00_2111;
BgL_lenz00_2111 = 
(
(long)(
TVECTOR_LENGTH(BgL_tvecz00_87))-1L); 
{ 
 long BgL_iz00_2113;
BgL_iz00_2113 = 0L; 
BgL_zc3z04anonymousza31705ze3z87_2114:
if(
(BgL_iz00_2113==BgL_lenz00_2111))
{ /* Ieee/output.scm 1018 */
{ /* Ieee/output.scm 1019 */
 obj_t BgL_arg1707z00_2116;
{ /* Ieee/output.scm 1019 */
 obj_t BgL_funz00_4269;
if(
PROCEDUREP(BgL_tvectorzd2refzd2_2107))
{ /* Ieee/output.scm 1019 */
BgL_funz00_4269 = BgL_tvectorzd2refzd2_2107; }  else 
{ 
 obj_t BgL_auxz00_6744;
BgL_auxz00_6744 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(36686L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2707z00zz__r4_output_6_10_3z00, BgL_tvectorzd2refzd2_2107); 
FAILURE(BgL_auxz00_6744,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4269, 2))
{ /* Ieee/output.scm 1019 */
BgL_arg1707z00_2116 = 
(VA_PROCEDUREP( BgL_funz00_4269 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4269))(BgL_tvectorzd2refzd2_2107, BgL_tvecz00_87, 
BINT(BgL_iz00_2113), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4269))(BgL_tvectorzd2refzd2_2107, BgL_tvecz00_87, 
BINT(BgL_iz00_2113)) ); }  else 
{ /* Ieee/output.scm 1019 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2708z00zz__r4_output_6_10_3z00,BgL_funz00_4269);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_89, 2))
{ /* Ieee/output.scm 1019 */
BGL_PROCEDURE_CALL2(BgL_dispz00_89, BgL_arg1707z00_2116, BgL_portz00_88); }  else 
{ /* Ieee/output.scm 1019 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2715z00zz__r4_output_6_10_3z00,BgL_dispz00_89);} } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_88);}  else 
{ /* Ieee/output.scm 1018 */
{ /* Ieee/output.scm 1022 */
 obj_t BgL_arg1708z00_2117;
{ /* Ieee/output.scm 1022 */
 obj_t BgL_funz00_4275;
if(
PROCEDUREP(BgL_tvectorzd2refzd2_2107))
{ /* Ieee/output.scm 1022 */
BgL_funz00_4275 = BgL_tvectorzd2refzd2_2107; }  else 
{ 
 obj_t BgL_auxz00_6768;
BgL_auxz00_6768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(36768L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2707z00zz__r4_output_6_10_3z00, BgL_tvectorzd2refzd2_2107); 
FAILURE(BgL_auxz00_6768,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4275, 2))
{ /* Ieee/output.scm 1022 */
BgL_arg1708z00_2117 = 
(VA_PROCEDUREP( BgL_funz00_4275 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4275))(BgL_tvectorzd2refzd2_2107, BgL_tvecz00_87, 
BINT(BgL_iz00_2113), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4275))(BgL_tvectorzd2refzd2_2107, BgL_tvecz00_87, 
BINT(BgL_iz00_2113)) ); }  else 
{ /* Ieee/output.scm 1022 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2708z00zz__r4_output_6_10_3z00,BgL_funz00_4275);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_89, 2))
{ /* Ieee/output.scm 1022 */
BGL_PROCEDURE_CALL2(BgL_dispz00_89, BgL_arg1708z00_2117, BgL_portz00_88); }  else 
{ /* Ieee/output.scm 1022 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2718z00zz__r4_output_6_10_3z00,BgL_dispz00_89);} } 
bgl_display_char(((unsigned char)' '), BgL_portz00_88); 
{ 
 long BgL_iz00_6790;
BgL_iz00_6790 = 
(1L+BgL_iz00_2113); 
BgL_iz00_2113 = BgL_iz00_6790; 
goto BgL_zc3z04anonymousza31705ze3z87_2114;} } } } }  else 
{ /* Ieee/output.scm 1008 */
bgl_display_string(BGl_string2721z00zz__r4_output_6_10_3z00, BgL_portz00_88); 
return BgL_tvecz00_87;} } } 

}



/* write/display-hvector */
obj_t BGl_writezf2displayzd2hvectorz20zz__r4_output_6_10_3z00(obj_t BgL_svecz00_90, obj_t BgL_portz00_91, obj_t BgL_dispz00_92)
{
{ /* Ieee/output.scm 1029 */
{ /* Ieee/output.scm 1030 */
 obj_t BgL_idz00_2122;
BgL_idz00_2122 = 
BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_svecz00_90); 
{ /* Ieee/output.scm 1031 */
 obj_t BgL__z00_2123; obj_t BgL_vrefz00_2124; obj_t BgL__z00_2125; obj_t BgL__z00_2126;
{ /* Ieee/output.scm 1032 */
 obj_t BgL_tmpz00_3411;
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6794;
BgL_tmpz00_6794 = 
(int)(1L); 
BgL_tmpz00_3411 = 
BGL_MVALUES_VAL(BgL_tmpz00_6794); } 
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6797;
BgL_tmpz00_6797 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6797, BUNSPEC); } 
BgL__z00_2123 = BgL_tmpz00_3411; } 
{ /* Ieee/output.scm 1032 */
 obj_t BgL_tmpz00_3412;
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6800;
BgL_tmpz00_6800 = 
(int)(2L); 
BgL_tmpz00_3412 = 
BGL_MVALUES_VAL(BgL_tmpz00_6800); } 
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6803;
BgL_tmpz00_6803 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6803, BUNSPEC); } 
BgL_vrefz00_2124 = BgL_tmpz00_3412; } 
{ /* Ieee/output.scm 1032 */
 obj_t BgL_tmpz00_3413;
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6806;
BgL_tmpz00_6806 = 
(int)(3L); 
BgL_tmpz00_3413 = 
BGL_MVALUES_VAL(BgL_tmpz00_6806); } 
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6809;
BgL_tmpz00_6809 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6809, BUNSPEC); } 
BgL__z00_2125 = BgL_tmpz00_3413; } 
{ /* Ieee/output.scm 1032 */
 obj_t BgL_tmpz00_3414;
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6812;
BgL_tmpz00_6812 = 
(int)(4L); 
BgL_tmpz00_3414 = 
BGL_MVALUES_VAL(BgL_tmpz00_6812); } 
{ /* Ieee/output.scm 1032 */
 int BgL_tmpz00_6815;
BgL_tmpz00_6815 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6815, BUNSPEC); } 
BgL__z00_2126 = BgL_tmpz00_3414; } 
bgl_display_char(((unsigned char)'#'), BgL_portz00_91); 
{ /* Ieee/output.scm 1033 */
 obj_t BgL_objz00_3415;
if(
SYMBOLP(BgL_idz00_2122))
{ /* Ieee/output.scm 1033 */
BgL_objz00_3415 = BgL_idz00_2122; }  else 
{ 
 obj_t BgL_auxz00_6821;
BgL_auxz00_6821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(37262L), BGl_string2722z00zz__r4_output_6_10_3z00, BGl_string2649z00zz__r4_output_6_10_3z00, BgL_idz00_2122); 
FAILURE(BgL_auxz00_6821,BFALSE,BFALSE);} 
{ /* Ieee/output.scm 773 */
 obj_t BgL_arg1607z00_3417;
BgL_arg1607z00_3417 = 
SYMBOL_TO_STRING(BgL_objz00_3415); 
bgl_display_string(BgL_arg1607z00_3417, BgL_portz00_91); } } 
bgl_display_char(((unsigned char)'('), BgL_portz00_91); 
{ /* Ieee/output.scm 1035 */
 bool_t BgL_test3271z00_6828;
{ /* Ieee/output.scm 1035 */
 long BgL_arg1724z00_2139;
BgL_arg1724z00_2139 = 
BGL_HVECTOR_LENGTH(BgL_svecz00_90); 
BgL_test3271z00_6828 = 
(0L==BgL_arg1724z00_2139); } 
if(BgL_test3271z00_6828)
{ /* Ieee/output.scm 1035 */
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_91);}  else 
{ /* Ieee/output.scm 1037 */
 long BgL_lenz00_2129;
{ /* Ieee/output.scm 1037 */
 long BgL_arg1723z00_2138;
BgL_arg1723z00_2138 = 
BGL_HVECTOR_LENGTH(BgL_svecz00_90); 
BgL_lenz00_2129 = 
(BgL_arg1723z00_2138-1L); } 
{ 
 long BgL_iz00_2131;
BgL_iz00_2131 = 0L; 
BgL_zc3z04anonymousza31716ze3z87_2132:
if(
(BgL_iz00_2131==BgL_lenz00_2129))
{ /* Ieee/output.scm 1040 */
{ /* Ieee/output.scm 1041 */
 obj_t BgL_arg1718z00_2134;
{ /* Ieee/output.scm 1041 */
 obj_t BgL_funz00_4283;
if(
PROCEDUREP(BgL_vrefz00_2124))
{ /* Ieee/output.scm 1041 */
BgL_funz00_4283 = BgL_vrefz00_2124; }  else 
{ 
 obj_t BgL_auxz00_6838;
BgL_auxz00_6838 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(37480L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2707z00zz__r4_output_6_10_3z00, BgL_vrefz00_2124); 
FAILURE(BgL_auxz00_6838,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4283, 2))
{ /* Ieee/output.scm 1041 */
BgL_arg1718z00_2134 = 
(VA_PROCEDUREP( BgL_funz00_4283 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4283))(BgL_vrefz00_2124, BgL_svecz00_90, 
BINT(BgL_iz00_2131), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4283))(BgL_vrefz00_2124, BgL_svecz00_90, 
BINT(BgL_iz00_2131)) ); }  else 
{ /* Ieee/output.scm 1041 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2723z00zz__r4_output_6_10_3z00,BgL_funz00_4283);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_92, 2))
{ /* Ieee/output.scm 1041 */
BGL_PROCEDURE_CALL2(BgL_dispz00_92, BgL_arg1718z00_2134, BgL_portz00_91); }  else 
{ /* Ieee/output.scm 1041 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2728z00zz__r4_output_6_10_3z00,BgL_dispz00_92);} } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_91);}  else 
{ /* Ieee/output.scm 1040 */
{ /* Ieee/output.scm 1044 */
 obj_t BgL_arg1720z00_2135;
{ /* Ieee/output.scm 1044 */
 obj_t BgL_funz00_4289;
if(
PROCEDUREP(BgL_vrefz00_2124))
{ /* Ieee/output.scm 1044 */
BgL_funz00_4289 = BgL_vrefz00_2124; }  else 
{ 
 obj_t BgL_auxz00_6862;
BgL_auxz00_6862 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(37555L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2707z00zz__r4_output_6_10_3z00, BgL_vrefz00_2124); 
FAILURE(BgL_auxz00_6862,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4289, 2))
{ /* Ieee/output.scm 1044 */
BgL_arg1720z00_2135 = 
(VA_PROCEDUREP( BgL_funz00_4289 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4289))(BgL_vrefz00_2124, BgL_svecz00_90, 
BINT(BgL_iz00_2131), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4289))(BgL_vrefz00_2124, BgL_svecz00_90, 
BINT(BgL_iz00_2131)) ); }  else 
{ /* Ieee/output.scm 1044 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2723z00zz__r4_output_6_10_3z00,BgL_funz00_4289);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_dispz00_92, 2))
{ /* Ieee/output.scm 1044 */
BGL_PROCEDURE_CALL2(BgL_dispz00_92, BgL_arg1720z00_2135, BgL_portz00_91); }  else 
{ /* Ieee/output.scm 1044 */
FAILURE(BGl_string2685z00zz__r4_output_6_10_3z00,BGl_list2731z00zz__r4_output_6_10_3z00,BgL_dispz00_92);} } 
bgl_display_char(((unsigned char)' '), BgL_portz00_91); 
{ 
 long BgL_iz00_6884;
BgL_iz00_6884 = 
(1L+BgL_iz00_2131); 
BgL_iz00_2131 = BgL_iz00_6884; 
goto BgL_zc3z04anonymousza31716ze3z87_2132;} } } } } } } } 

}



/* display-pair */
obj_t BGl_displayzd2pairzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_96, obj_t BgL_portz00_97)
{
{ /* Ieee/output.scm 1083 */
bgl_display_char(((unsigned char)'('), BgL_portz00_97); 
{ 
 obj_t BgL_lz00_2142;
BgL_lz00_2142 = BgL_objz00_96; 
BgL_zc3z04anonymousza31725ze3z87_2143:
{ /* Ieee/output.scm 1084 */
 bool_t BgL_test3279z00_6887;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_tmpz00_6888;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3428;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3428 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6891;
BgL_auxz00_6891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6891,BFALSE,BFALSE);} 
BgL_tmpz00_6888 = 
CDR(BgL_pairz00_3428); } 
BgL_test3279z00_6887 = 
NULLP(BgL_tmpz00_6888); } 
if(BgL_test3279z00_6887)
{ /* Ieee/output.scm 1084 */
{ /* Ieee/output.scm 1084 */
 obj_t BgL_arg1728z00_2146;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3429;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3429 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6899;
BgL_auxz00_6899 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6899,BFALSE,BFALSE);} 
BgL_arg1728z00_2146 = 
CAR(BgL_pairz00_3429); } 
bgl_display_obj(BgL_arg1728z00_2146, BgL_portz00_97); } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_97);}  else 
{ /* Ieee/output.scm 1084 */
 bool_t BgL_test3282z00_6906;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_tmpz00_6907;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3430;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3430 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6910;
BgL_auxz00_6910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6910,BFALSE,BFALSE);} 
BgL_tmpz00_6907 = 
CDR(BgL_pairz00_3430); } 
BgL_test3282z00_6906 = 
PAIRP(BgL_tmpz00_6907); } 
if(BgL_test3282z00_6906)
{ /* Ieee/output.scm 1084 */
{ /* Ieee/output.scm 1084 */
 obj_t BgL_arg1731z00_2149;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3431;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3431 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6918;
BgL_auxz00_6918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6918,BFALSE,BFALSE);} 
BgL_arg1731z00_2149 = 
CAR(BgL_pairz00_3431); } 
bgl_display_obj(BgL_arg1731z00_2149, BgL_portz00_97); } 
bgl_display_char(((unsigned char)' '), BgL_portz00_97); 
{ /* Ieee/output.scm 1084 */
 obj_t BgL_arg1733z00_2150;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3432;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3432 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6927;
BgL_auxz00_6927 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6927,BFALSE,BFALSE);} 
BgL_arg1733z00_2150 = 
CDR(BgL_pairz00_3432); } 
{ 
 obj_t BgL_lz00_6932;
BgL_lz00_6932 = BgL_arg1733z00_2150; 
BgL_lz00_2142 = BgL_lz00_6932; 
goto BgL_zc3z04anonymousza31725ze3z87_2143;} } }  else 
{ /* Ieee/output.scm 1084 */
{ /* Ieee/output.scm 1084 */
 obj_t BgL_arg1734z00_2151;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3433;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3433 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6935;
BgL_auxz00_6935 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6935,BFALSE,BFALSE);} 
BgL_arg1734z00_2151 = 
CAR(BgL_pairz00_3433); } 
bgl_display_obj(BgL_arg1734z00_2151, BgL_portz00_97); } 
bgl_display_char(((unsigned char)' '), BgL_portz00_97); 
bgl_display_char(((unsigned char)'.'), BgL_portz00_97); 
bgl_display_char(((unsigned char)' '), BgL_portz00_97); 
{ /* Ieee/output.scm 1084 */
 obj_t BgL_arg1735z00_2152;
{ /* Ieee/output.scm 1084 */
 obj_t BgL_pairz00_3434;
if(
PAIRP(BgL_lz00_2142))
{ /* Ieee/output.scm 1084 */
BgL_pairz00_3434 = BgL_lz00_2142; }  else 
{ 
 obj_t BgL_auxz00_6946;
BgL_auxz00_6946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39074L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2142); 
FAILURE(BgL_auxz00_6946,BFALSE,BFALSE);} 
BgL_arg1735z00_2152 = 
CDR(BgL_pairz00_3434); } 
bgl_display_obj(BgL_arg1735z00_2152, BgL_portz00_97); } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_97);} } } } } 

}



/* write-pair */
obj_t BGl_writezd2pairzd2zz__r4_output_6_10_3z00(obj_t BgL_objz00_98, obj_t BgL_portz00_99)
{
{ /* Ieee/output.scm 1089 */
bgl_display_char(((unsigned char)'('), BgL_portz00_99); 
{ 
 obj_t BgL_lz00_2157;
BgL_lz00_2157 = BgL_objz00_98; 
BgL_zc3z04anonymousza31738ze3z87_2158:
{ /* Ieee/output.scm 1090 */
 bool_t BgL_test3288z00_6954;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_tmpz00_6955;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3435;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3435 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_6958;
BgL_auxz00_6958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_6958,BFALSE,BFALSE);} 
BgL_tmpz00_6955 = 
CDR(BgL_pairz00_3435); } 
BgL_test3288z00_6954 = 
NULLP(BgL_tmpz00_6955); } 
if(BgL_test3288z00_6954)
{ /* Ieee/output.scm 1090 */
{ /* Ieee/output.scm 1090 */
 obj_t BgL_arg1741z00_2161;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3436;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3436 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_6966;
BgL_auxz00_6966 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_6966,BFALSE,BFALSE);} 
BgL_arg1741z00_2161 = 
CAR(BgL_pairz00_3436); } 
bgl_write_obj(BgL_arg1741z00_2161, BgL_portz00_99); } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_99);}  else 
{ /* Ieee/output.scm 1090 */
 bool_t BgL_test3291z00_6973;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_tmpz00_6974;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3437;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3437 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_6977;
BgL_auxz00_6977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_6977,BFALSE,BFALSE);} 
BgL_tmpz00_6974 = 
CDR(BgL_pairz00_3437); } 
BgL_test3291z00_6973 = 
PAIRP(BgL_tmpz00_6974); } 
if(BgL_test3291z00_6973)
{ /* Ieee/output.scm 1090 */
{ /* Ieee/output.scm 1090 */
 obj_t BgL_arg1745z00_2164;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3438;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3438 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_6985;
BgL_auxz00_6985 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_6985,BFALSE,BFALSE);} 
BgL_arg1745z00_2164 = 
CAR(BgL_pairz00_3438); } 
bgl_write_obj(BgL_arg1745z00_2164, BgL_portz00_99); } 
bgl_display_char(((unsigned char)' '), BgL_portz00_99); 
{ /* Ieee/output.scm 1090 */
 obj_t BgL_arg1746z00_2165;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3439;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3439 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_6994;
BgL_auxz00_6994 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_6994,BFALSE,BFALSE);} 
BgL_arg1746z00_2165 = 
CDR(BgL_pairz00_3439); } 
{ 
 obj_t BgL_lz00_6999;
BgL_lz00_6999 = BgL_arg1746z00_2165; 
BgL_lz00_2157 = BgL_lz00_6999; 
goto BgL_zc3z04anonymousza31738ze3z87_2158;} } }  else 
{ /* Ieee/output.scm 1090 */
{ /* Ieee/output.scm 1090 */
 obj_t BgL_arg1747z00_2166;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3440;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3440 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_7002;
BgL_auxz00_7002 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_7002,BFALSE,BFALSE);} 
BgL_arg1747z00_2166 = 
CAR(BgL_pairz00_3440); } 
bgl_write_obj(BgL_arg1747z00_2166, BgL_portz00_99); } 
bgl_display_char(((unsigned char)' '), BgL_portz00_99); 
bgl_display_char(((unsigned char)'.'), BgL_portz00_99); 
bgl_display_char(((unsigned char)' '), BgL_portz00_99); 
{ /* Ieee/output.scm 1090 */
 obj_t BgL_arg1748z00_2167;
{ /* Ieee/output.scm 1090 */
 obj_t BgL_pairz00_3441;
if(
PAIRP(BgL_lz00_2157))
{ /* Ieee/output.scm 1090 */
BgL_pairz00_3441 = BgL_lz00_2157; }  else 
{ 
 obj_t BgL_auxz00_7013;
BgL_auxz00_7013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2562z00zz__r4_output_6_10_3z00, 
BINT(39372L), BGl_string2583z00zz__r4_output_6_10_3z00, BGl_string2584z00zz__r4_output_6_10_3z00, BgL_lz00_2157); 
FAILURE(BgL_auxz00_7013,BFALSE,BFALSE);} 
BgL_arg1748z00_2167 = 
CDR(BgL_pairz00_3441); } 
bgl_write_obj(BgL_arg1748z00_2167, BgL_portz00_99); } 
return 
bgl_display_char(((unsigned char)')'), BgL_portz00_99);} } } } } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__r4_output_6_10_3z00(void)
{
{ /* Ieee/output.scm 24 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00)); 
BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00)); 
BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(268155843L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00)); 
BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00)); 
BGl_modulezd2initializa7ationz75zz__srfi4z00(467924964L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string2734z00zz__r4_output_6_10_3z00));} 

}

#ifdef __cplusplus
}
#endif
