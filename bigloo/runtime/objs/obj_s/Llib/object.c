/*===========================================================================*/
/*   (Llib/object.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/object.scm -indent -o objs/obj_s/Llib/object.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___OBJECT_TYPE_DEFINITIONS
#define BGL___OBJECT_TYPE_DEFINITIONS

/* object type definitions */
typedef struct BgL_z62conditionz62_bgl {
   header_t header;
   obj_t widening;
} *BgL_z62conditionz62_bglt;

typedef struct BgL_z62exceptionz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
} *BgL_z62exceptionz62_bglt;

typedef struct BgL_z62errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62errorz62_bglt;

typedef struct BgL_z62typezd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
   obj_t BgL_typez00;
} *BgL_z62typezd2errorzb0_bglt;

typedef struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
   obj_t BgL_indexz00;
} *BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt;

typedef struct BgL_z62iozd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2errorzb0_bglt;

typedef struct BgL_z62iozd2portzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2portzd2errorz62_bglt;

typedef struct BgL_z62iozd2readzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2readzd2errorz62_bglt;

typedef struct BgL_z62iozd2writezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2writezd2errorz62_bglt;

typedef struct BgL_z62iozd2closedzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2closedzd2errorz62_bglt;

typedef struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt;

typedef struct BgL_z62iozd2parsezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2parsezd2errorz62_bglt;

typedef struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt;

typedef struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt;

typedef struct BgL_z62iozd2sigpipezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2sigpipezd2errorz62_bglt;

typedef struct BgL_z62iozd2timeoutzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2timeoutzd2errorz62_bglt;

typedef struct BgL_z62iozd2connectionzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2connectionzd2errorz62_bglt;

typedef struct BgL_z62processzd2exceptionzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62processzd2exceptionzb0_bglt;

typedef struct BgL_z62stackzd2overflowzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62stackzd2overflowzd2errorz62_bglt;

typedef struct BgL_z62securityzd2exceptionzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_messagez00;
} *BgL_z62securityzd2exceptionzb0_bglt;

typedef struct BgL_z62accesszd2controlzd2exceptionz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_messagez00;
   obj_t BgL_objz00;
   obj_t BgL_permissionz00;
} *BgL_z62accesszd2controlzd2exceptionz62_bglt;

typedef struct BgL_z62warningz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_argsz00;
} *BgL_z62warningz62_bglt;

typedef struct BgL_z62evalzd2warningzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_argsz00;
} *BgL_z62evalzd2warningzb0_bglt;


#endif // BGL___OBJECT_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
static obj_t BGl_z62lambda1927z62zz__objectz00(obj_t, obj_t, obj_t);
static BgL_objectz00_bglt BGl_z62lambda1846z62zz__objectz00(obj_t);
static BgL_objectz00_bglt BGl_z62lambda1848z62zz__objectz00(obj_t);
static obj_t BGl_z62objectzd2displayzb0zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62processzd2exceptionzb0zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2allzd2fieldsz62zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3617z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3537z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3619z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3539z00zz__objectz00 = BUNSPEC;
extern obj_t make_vector_uncollectable(long, obj_t);
static obj_t BGl_z62objectzd2print1399zb0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long bgl_types_number(void);
static obj_t BGl_z62findzd2methodzb0zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_doublezd2nbzd2genericsz12z12zz__objectz00(void);
static BgL_z62typezd2errorzb0_bglt BGl_z62lambda1933z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_objectzd2wideningzd2zz__objectz00(BgL_objectz00_bglt);
static BgL_z62typezd2errorzb0_bglt BGl_z62lambda1935z62zz__objectz00(obj_t);
static obj_t BGl_z62z52isa64zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static BgL_z62conditionz62_bglt BGl_z62lambda1857z62zz__objectz00(obj_t);
static BgL_z62conditionz62_bglt BGl_z62lambda1859z62zz__objectz00(obj_t);
static obj_t BGl_z62bigloozd2genericzd2bucketzd2siza7ez17zz__objectz00(obj_t);
static obj_t BGl_z62objectzd2wideningzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3701z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3540z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3542z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3706z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3625z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3544z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3627z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3546z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
static obj_t BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3548z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2nilzd2initz12z70zz__objectz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31952ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1941z62zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1942z62zz__objectz00(obj_t, obj_t, obj_t);
static BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BGl_z62lambda1948z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_z52isazd2objectzf2finalzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t);
static BgL_z62exceptionz62_bglt BGl_z62lambda1869z62zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2subclasseszb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3711z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3633z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t);
static obj_t BGl_z62classzd2fieldzd2mutatorz62zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3716z00zz__objectz00 = BUNSPEC;
extern obj_t bigloo_generic_mutex;
static obj_t BGl_symbol3639z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_bigloozd2genericzd2bucketzd2powzd2zz__objectz00(void);
static obj_t BGl_z62zc3z04anonymousza31880ze3ze5zz__objectz00(obj_t);
extern obj_t bgl_display_obj(obj_t, obj_t);
static BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BGl_z62lambda1950z62zz__objectz00(obj_t);
extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31937ze3ze5zz__objectz00(obj_t, obj_t);
static BgL_z62exceptionz62_bglt BGl_z62lambda1871z62zz__objectz00(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2numzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1956z62zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1957z62zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62lambda1878z62zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1879z62zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3801z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DEF obj_t BGl_za2classesza2z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(obj_t);
static obj_t BGl_symbol3721z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3805z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3482z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3726z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3484z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_classzd2virtualzd2zz__objectz00(obj_t);
static obj_t BGl_symbol3649z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2sigpipezd2errorz62zz__objectz00 = BUNSPEC;
extern obj_t BGl_warningz00zz__errorz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31873ze3ze5zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_methodzd2arrayzd2refz00zz__objectz00(obj_t, obj_t, int);
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldszd2zz__objectz00(obj_t);
static BgL_z62iozd2errorzb0_bglt BGl_z62lambda1963z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2errorzb0_bglt BGl_z62lambda1965z62zz__objectz00(obj_t);
static obj_t BGl_z62lambda1886z62zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1887z62zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3810z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3731z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3650z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3815z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3736z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3655z00zz__objectz00 = BUNSPEC;
static obj_t BGl_initializa7ezd2objectsz12z67zz__objectz00(void);
static obj_t BGl_toplevelzd2initzd2zz__objectz00(void);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2writezd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2modulezb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2methodzd2fromz00zz__objectz00(BgL_objectz00_bglt, obj_t, obj_t);
static obj_t BGl_z62classzd2superzb0zz__objectz00(obj_t, obj_t);
static BgL_z62iozd2portzd2errorz62_bglt BGl_z62lambda1973z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62lambda1893z62zz__objectz00(obj_t, obj_t);
static BgL_z62iozd2portzd2errorz62_bglt BGl_z62lambda1975z62zz__objectz00(obj_t);
static obj_t BGl_z62lambda1894z62zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62typezd2errorzb0zz__objectz00 = BUNSPEC;
static obj_t BGl_z62genericzd2memoryzd2statisticsz62zz__objectz00(obj_t);
static obj_t BGl_symbol3900z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2fieldzd2mutablezf3z91zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3741z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3660z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2fieldzd2defaultzd2valuezf3z43zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62objectzd2equalzf31402z43zz__objectz00(obj_t, obj_t, obj_t);
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_symbol3746z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3665z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32104ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3909z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32007ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62evalzd2classzf3z43zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_objectzd2displayzd2zz__objectz00(BgL_objectz00_bglt, obj_t);
static obj_t BGl_z62objectzf3z91zz__objectz00(obj_t, obj_t);
static BgL_z62iozd2readzd2errorz62_bglt BGl_z62lambda1983z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_genericzd2initzd2zz__objectz00(void);
static BgL_z62iozd2readzd2errorz62_bglt BGl_z62lambda1985z62zz__objectz00(obj_t);
static obj_t BGl_z62objectzd2classzb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2existszd2zz__objectz00(obj_t);
static obj_t BGl_symbol3913z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3751z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3670z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3915z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32121ze3ze5zz__objectz00(obj_t);
static obj_t BGl_symbol3918z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3756z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3675z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t);
static obj_t BGl_za2nbzd2classeszd2maxza2z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_classzd2numzd2zz__objectz00(obj_t);
extern obj_t BGl_displayzd2tracezd2stackz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__objectz00(void);
BGL_EXPORTED_DECL long BGl_classzd2indexzd2zz__objectz00(obj_t);
extern obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(obj_t);
static BgL_z62iozd2writezd2errorz62_bglt BGl_z62lambda1993z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2writezd2errorz62_bglt BGl_z62lambda1995z62zz__objectz00(obj_t);
static obj_t BGl_symbol3922z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3761z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3681z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3925z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32114ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3928z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3766z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32017ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3688z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DEF obj_t BGl_za2inheritancesza2z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DEF obj_t BGl_z62exceptionz62zz__objectz00 = BUNSPEC;
static obj_t BGl_makezd2classzd2virtualzd2slotszd2vectorz00zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62callzd2virtualzd2setterz62zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_objectzd2equalzf3z21zz__objectz00(BgL_objectz00_bglt, BgL_objectz00_bglt);
static obj_t BGl_z62isazf3z91zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3931z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3771z00zz__objectz00 = BUNSPEC;
static obj_t BGl_extendzd2vectorzd2zz__objectz00(obj_t, obj_t, long);
static obj_t BGl_symbol3934z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3692z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32050ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3937z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3776z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3697z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_findzd2classzd2fieldz00zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_classzd2abstractzf3z21zz__objectz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31967ze3ze5zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_genericzd2memoryzd2statisticsz00zz__objectz00(void);
static obj_t BGl_z62classzd2abstractzf3z43zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62classzd2fieldzd2virtualzf3z91zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62exceptionzd2notify1404zb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3942z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3781z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3783z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32132ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32027ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3949z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3788z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_objectzd2printzd2zz__objectz00(BgL_objectz00_bglt, obj_t, obj_t);
static obj_t BGl_z62exceptionzd2notifyzd2z62wa1416z00zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62widezd2objectzf3z43zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2evdatazd2zz__objectz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__objectz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62methodzd2arrayzd2refz62zz__objectz00(obj_t, obj_t, obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t);
extern obj_t make_vector(long, obj_t);
static obj_t BGl_symbol3954z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3796z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3959z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62findzd2methodzd2fromz62zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31977ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31888ze3ze5zz__objectz00(obj_t);
static obj_t BGl_z62genericzd2methodzd2arrayz62zz__objectz00(obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__objectz00(void);
static obj_t BGl_symbol3964z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3883z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3966z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3885z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62callzd2virtualzd2getterz62zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt, obj_t);
static obj_t BGl_symbol3888z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2namez00zz__objectz00(obj_t);
static obj_t BGl_z62objectzd2display1389zb0zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62objectzd2hashnumberzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3970z00zz__objectz00 = BUNSPEC;
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2fieldzd2accessorz62zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol3896z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3978z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3898z00zz__objectz00 = BUNSPEC;
static obj_t BGl_classzd2shrinkzd2zz__objectz00(obj_t);
static obj_t BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_genericzd2methodzd2arrayz00zz__objectz00(obj_t);
static obj_t BGl_z62makezd2classzd2fieldz62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62objectzd2wideningzd2setz12z70zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31987ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_za2classzd2keyza2zd2zz__objectz00 = BUNSPEC;
static obj_t BGl_za2nbzd2genericsza2zd2zz__objectz00 = BUNSPEC;
static obj_t BGl_z62findzd2classzd2byzd2hashzb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_classzd2hashzd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_z52isazf2cdepthzf3z53zz__objectz00(obj_t, obj_t, long);
static obj_t BGl_z62zc3z04anonymousza32039ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62findzd2superzd2classzd2methodzb0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2allocatorzd2zz__objectz00(obj_t);
static obj_t BGl_z62callzd2nextzd2virtualzd2setterzb0zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t);
static obj_t BGl_z62nilzf3z91zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62classzd2allocatorzb0zz__objectz00(obj_t, obj_t);
extern obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_classzd2widezf3z21zz__objectz00(obj_t);
static BgL_z62iozd2closedzd2errorz62_bglt BGl_z62lambda2003z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2closedzd2errorz62_bglt BGl_z62lambda2005z62zz__objectz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_z52isa32zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_objectzd2wideningzd2setz12z12zz__objectz00(BgL_objectz00_bglt, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62securityzd2exceptionzb0zz__objectz00 = BUNSPEC;
static obj_t BGl_z62findzd2classzd2fieldz62zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31997ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62classzd2indexzb0zz__objectz00(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2hashzb0zz__objectz00(obj_t, obj_t);
static BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BGl_z62lambda2013z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32073ze3ze5zz__objectz00(obj_t, obj_t);
static BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BGl_z62lambda2015z62zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_objectz00_bglt, int, obj_t);
static obj_t BGl_z62registerzd2genericz12za2zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2closedzd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_z62genericzd2addzd2evalzd2methodz12za2zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2infoz00zz__objectz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_nilzf3zf3zz__objectz00(BgL_objectz00_bglt);
static obj_t BGl_list3503z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3506z00zz__objectz00 = BUNSPEC;
static BgL_z62stackzd2overflowzd2errorz62_bglt BGl_z62lambda2100z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2fieldzf3z43zz__objectz00(obj_t, obj_t);
static BgL_z62stackzd2overflowzd2errorz62_bglt BGl_z62lambda2102z62zz__objectz00(obj_t);
static BgL_z62iozd2parsezd2errorz62_bglt BGl_z62lambda2023z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2parsezd2errorz62_bglt BGl_z62lambda2025z62zz__objectz00(obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62evalzd2warningzb0zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32147ze3ze5zz__objectz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza32139ze3ze5zz__objectz00(obj_t);
static obj_t BGl_z62classzd2creatorzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3511z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
static obj_t BGl_list3516z00zz__objectz00 = BUNSPEC;
static BgL_z62securityzd2exceptionzb0_bglt BGl_z62lambda2110z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62securityzd2exceptionzb0_bglt BGl_z62lambda2112z62zz__objectz00(obj_t);
static obj_t BGl_list3519z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32172ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32083ze3ze5zz__objectz00(obj_t, obj_t);
static BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BGl_z62lambda2035z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BGl_z62lambda2037z62zz__objectz00(obj_t);
static obj_t BGl_z62lambda2119z62zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2typez00zz__objectz00(obj_t);
static obj_t BGl_z62procedurezd2ze3genericz53zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00 = BUNSPEC;
static obj_t BGl_z62callzd2nextzd2virtualzd2getterzb0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2fieldzd2namez62zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52objectzd2wideningz80zz__objectz00(BgL_objectz00_bglt);
static obj_t BGl_z62classzd2fieldszb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL BgL_objectz00_bglt BGl_allocatezd2instancezd2zz__objectz00(obj_t);
static obj_t BGl_methodzd2arrayzd2setz12z12zz__objectz00(obj_t, obj_t, long, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_list3605z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62lambda2120z62zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_list3608z00zz__objectz00 = BUNSPEC;
extern obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2evdatazd2setz12z12zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32157ze3ze5zz__objectz00(obj_t, obj_t);
static BgL_z62accesszd2controlzd2exceptionz62_bglt BGl_z62lambda2127z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BGl_z62lambda2046z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BGl_z62lambda2048z62zz__objectz00(obj_t);
static obj_t BGl_z62z52isa32zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
static obj_t BGl_list3611z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_objectz00_bglt, int);
static obj_t BGl_z62genericzd2addzd2methodz12z70zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static bool_t BGl_genericszd2addzd2classz12z12zz__objectz00(long, long);
static obj_t BGl_list3616z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62exceptionzd2notifyzd2z62er1412z00zz__objectz00(obj_t, obj_t);
static BgL_z62accesszd2controlzd2exceptionz62_bglt BGl_z62lambda2130z62zz__objectz00(obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62errorz62zz__objectz00 = BUNSPEC;
static bool_t BGl_doublezd2nbzd2classesz12z12zz__objectz00(void);
static obj_t BGl_z62lambda2137z62zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_classzf3zf3zz__objectz00(obj_t);
static obj_t BGl_z62classzd2evfieldszd2setz12z70zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62lambda2138z62zz__objectz00(obj_t, obj_t, obj_t);
static BgL_z62iozd2sigpipezd2errorz62_bglt BGl_z62lambda2058z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_z52isazf2finalzf3z53zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62warningz62zz__objectz00 = BUNSPEC;
static obj_t BGl_z62z52isazd2objectzf2finalzf3ze3zz__objectz00(obj_t, obj_t, obj_t);
extern obj_t BGl_currentzd2threadzd2zz__threadz00(void);
static obj_t BGl_z62classzd2existszb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62objectzd2hashnumber1396zb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3624z00zz__objectz00 = BUNSPEC;
static BgL_z62iozd2sigpipezd2errorz62_bglt BGl_z62lambda2060z62zz__objectz00(obj_t);
static obj_t BGl_za2inheritancezd2cntza2zd2zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32094ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda2145z62zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62stackzd2overflowzd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_z62lambda2146z62zz__objectz00(obj_t, obj_t, obj_t);
extern obj_t BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(obj_t, obj_t);
static BgL_z62iozd2timeoutzd2errorz62_bglt BGl_z62lambda2069z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_za2genericsza2z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DEF obj_t BGl_z62iozd2portzd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2fieldzd2defaultzd2valuezb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3632z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_bigloozd2genericzd2bucketzd2maskzd2zz__objectz00(void);
extern obj_t bgl_make_generic(obj_t);
static obj_t BGl_list3638z00zz__objectz00 = BUNSPEC;
static BgL_z62iozd2timeoutzd2errorz62_bglt BGl_z62lambda2071z62zz__objectz00(obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2connectionzd2errorz62zz__objectz00 = BUNSPEC;
static BgL_z62warningz62_bglt BGl_z62lambda2153z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static BgL_z62warningz62_bglt BGl_z62lambda2155z62zz__objectz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_objectzf3zf3zz__objectz00(obj_t);
static obj_t BGl_cnstzd2initzd2zz__objectz00(void);
static BgL_z62iozd2connectionzd2errorz62_bglt BGl_z62lambda2079z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z52addzd2methodz12z92zz__objectz00(obj_t, obj_t, obj_t);
extern obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t);
extern long bgl_list_length(obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62accesszd2controlzd2exceptionz62zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2fieldzd2infoz62zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2modulezd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t, long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2timeoutzd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_list3481z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2nilzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__objectz00(void);
static obj_t BGl_z62registerzd2classz12za2zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2widezf3z43zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda2161z62zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda2162z62zz__objectz00(obj_t, obj_t, obj_t);
static BgL_z62iozd2connectionzd2errorz62_bglt BGl_z62lambda2081z62zz__objectz00(obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2errorzb0zz__objectz00 = BUNSPEC;
static BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2168z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2classzd2zz__objectz00(obj_t);
static obj_t BGl_z62bigloozd2genericzd2bucketzd2maskzb0zz__objectz00(obj_t);
static obj_t BGl_z62classzd2virtualzb0zz__objectz00(obj_t, obj_t);
static BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2170z62zz__objectz00(obj_t);
static obj_t BGl_z62z52isazd2objectzf2cdepthzf3ze3zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static BgL_z62processzd2exceptionzb0_bglt BGl_z62lambda2090z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00 = BUNSPEC;
static BgL_z62processzd2exceptionzb0_bglt BGl_z62lambda2092z62zz__objectz00(obj_t);
BGL_EXPORTED_DECL long BGl_objectzd2classzd2numz00zz__objectz00(BgL_objectz00_bglt);
BGL_EXPORTED_DEF obj_t BGl_objectz00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62objectzd2printzb0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_objectzd2classzd2zz__objectz00(BgL_objectz00_bglt);
static obj_t BGl_z62exceptionzd2notifyzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62classzd2fieldzd2typez62zz__objectz00(obj_t, obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62classzd2evdatazb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_callzd2nextzd2virtualzd2setterzd2zz__objectz00(obj_t, BgL_objectz00_bglt, int, obj_t);
static obj_t BGl_z62z52objectzd2wideningze2zz__objectz00(obj_t, obj_t);
extern obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_objectz00_bglt, obj_t);
static obj_t BGl_z62bigloozd2typeszd2numberz62zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
static obj_t BGl_z62findzd2classzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3912z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DEF obj_t BGl_z62conditionz62zz__objectz00 = BUNSPEC;
static obj_t BGl_list3917z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62objectzd2writezb0zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_genericzd2addzd2evalzd2methodz12zc0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62genericzd2nozd2defaultzd2behaviorzb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2allzd2fieldsz00zz__objectz00(obj_t);
static obj_t BGl_z62objectzd2write1392zb0zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2nilzd2zz__objectz00(obj_t);
static obj_t BGl_list3920z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3921z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3924z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3927z00zz__objectz00 = BUNSPEC;
extern long bgl_obj_hash_number(obj_t);
extern obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2classzd2byzd2hashzd2zz__objectz00(int);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62classzd2namezb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3930z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_widezd2objectzf3z21zz__objectz00(BgL_objectz00_bglt);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_z62z52isazf2cdepthzf3z31zz__objectz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2evfieldszd2setz12z12zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62exceptionzd2notifyzd2z62io1414z00zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3936z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_objectzd2classzd2numzd2setz12zc0zz__objectz00(BgL_objectz00_bglt, long);
static obj_t BGl_list3939z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_callzd2nextzd2virtualzd2getterzd2zz__objectz00(obj_t, BgL_objectz00_bglt, int);
BGL_EXPORTED_DECL obj_t BGl_classzd2subclasseszd2zz__objectz00(obj_t);
static obj_t BGl_z62z52isazf2finalzf3z31zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62objectzd2equalzf3z43zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_loopze70ze7zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list3940z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3941z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3944z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31850ze3ze5zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_classzd2creatorzd2zz__objectz00(obj_t);
static obj_t BGl_list3948z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_z52isa64zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t, long);
static obj_t BGl_z62z52objectzd2wideningzd2setz12z22zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3504z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3507z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3509z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_evalzd2classzf3z21zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t, obj_t);
static obj_t BGl_z62classzd2constructorzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3953z00zz__objectz00 = BUNSPEC;
static BgL_z62errorz62_bglt BGl_z62lambda1902z62zz__objectz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_za2nbzd2genericszd2maxza2z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3958z00zz__objectz00 = BUNSPEC;
static BgL_z62errorz62_bglt BGl_z62lambda1904z62zz__objectz00(obj_t);
static obj_t BGl_makezd2methodzd2arrayz00zz__objectz00(obj_t);
static obj_t BGl_z62classzf3z91zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62objectzd2classzd2numz62zz__objectz00(obj_t, obj_t);
static obj_t BGl_za2nbzd2classesza2zd2zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3512z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3514z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3517z00zz__objectz00 = BUNSPEC;
static BgL_objectz00_bglt BGl_z62allocatezd2instancezb0zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_genericzd2defaultzd2zz__objectz00(obj_t);
static obj_t BGl_list3963z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3882z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62genericzd2defaultzb0zz__objectz00(obj_t, obj_t);
static obj_t BGl_z62lambda1913z62zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3887z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62lambda1914z62zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_list3969z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62classzd2evdatazd2setz12z70zz__objectz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_bigloozd2genericzd2bucketzd2siza7ez75zz__objectz00(void);
static obj_t BGl_z62bigloozd2genericzd2bucketzd2powzb0zz__objectz00(obj_t);
static obj_t BGl_z62lambda1919z62zz__objectz00(obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_z62iozd2readzd2errorz62zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3606z00zz__objectz00 = BUNSPEC;
static obj_t BGl_symbol3609z00zz__objectz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_z52isazd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_procedurezd2ze3genericz31zz__objectz00(obj_t);
BGL_EXPORTED_DECL long BGl_objectzd2hashnumberzd2zz__objectz00(BgL_objectz00_bglt);
static obj_t BGl_z62lambda1920z62zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31861ze3ze5zz__objectz00(obj_t, obj_t);
static obj_t BGl_list3894z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3895z00zz__objectz00 = BUNSPEC;
static obj_t BGl_list3977z00zz__objectz00 = BUNSPEC;
static obj_t BGl_z62objectzd2classzd2numzd2setz12za2zz__objectz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62lambda1926z62zz__objectz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2nilzd2initz12zd2envzc0zz__objectz00, BgL_bgl_za762classza7d2nilza7d3982za7, BGl_z62classzd2nilzd2initz12z70zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2genericzd2bucketzd2siza7ezd2envza7zz__objectz00, BgL_bgl_za762biglooza7d2gene3983z00, BGl_z62bigloozd2genericzd2bucketzd2siza7ez17zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_genericzd2addzd2methodz12zd2envzc0zz__objectz00, BgL_bgl_za762genericza7d2add3984z00, BGl_z62genericzd2addzd2methodz12z70zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_registerzd2genericz12zd2envz12zz__objectz00, BgL_bgl_za762registerza7d2ge3985z00, BGl_z62registerzd2genericz12za2zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_procedurezd2ze3genericzd2envze3zz__objectz00, BgL_bgl_za762procedureza7d2za73986za7, BGl_z62procedurezd2ze3genericz53zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_GENERIC( BGl_objectzd2hashnumberzd2envz00zz__objectz00, BgL_bgl_za762objectza7d2hash3987z00, BGl_z62objectzd2hashnumberzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2methodzd2fromzd2envzd2zz__objectz00, BgL_bgl_za762findza7d2method3988z00, BGl_z62findzd2methodzd2fromz62zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2superzd2envz00zz__objectz00, BgL_bgl_za762classza7d2super3989z00, BGl_z62classzd2superzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_objectzd2wideningzd2envz00zz__objectz00, BgL_bgl_za762objectza7d2wide3990z00, BGl_z62objectzd2wideningzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2defaultzd2valuezd2envz00zz__objectz00, BgL_bgl_za762classza7d2field3991z00, BGl_z62classzd2fieldzd2defaultzd2valuezb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_evalzd2classzf3zd2envzf3zz__objectz00, BgL_bgl_za762evalza7d2classza73992za7, BGl_z62evalzd2classzf3z43zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_GENERIC( BGl_exceptionzd2notifyzd2envz00zz__objectz00, BgL_bgl_za762exceptionza7d2n3993z00, BGl_z62exceptionzd2notifyzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2classzd2envz00zz__objectz00, BgL_bgl_za762findza7d2classza73994za7, BGl_z62findzd2classzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_GENERIC( BGl_objectzd2writezd2envz00zz__objectz00, BgL_bgl_za762objectza7d2writ3995z00, va_generic_entry, BGl_z62objectzd2writezb0zz__objectz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzf3zd2envz21zz__objectz00, BgL_bgl_za762classza7f3za791za7za73996za7, BGl_z62classzf3z91zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2subclasseszd2envz00zz__objectz00, BgL_bgl_za762classza7d2subcl3997z00, BGl_z62classzd2subclasseszb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2creatorzd2envz00zz__objectz00, BgL_bgl_za762classza7d2creat3998z00, BGl_z62classzd2creatorzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_genericzd2methodzd2arrayzd2envzd2zz__objectz00, BgL_bgl_za762genericza7d2met3999z00, BGl_z62genericzd2methodzd2arrayz62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2namezd2envz00zz__objectz00, BgL_bgl_za762classza7d2nameza74000za7, BGl_z62classzd2namezb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2hashzd2envz00zz__objectz00, BgL_bgl_za762classza7d2hashza74001za7, BGl_z62classzd2hashzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2typezd2envzd2zz__objectz00, BgL_bgl_za762classza7d2field4002z00, BGl_z62classzd2fieldzd2typez62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_genericzd2nozd2defaultzd2behaviorzd2envz00zz__objectz00, BgL_bgl_za762genericza7d2noza74003za7, va_generic_entry, BGl_z62genericzd2nozd2defaultzd2behaviorzb0zz__objectz00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2evdatazd2envz00zz__objectz00, BgL_bgl_za762classza7d2evdat4004z00, BGl_z62classzd2evdatazb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52objectzd2wideningzd2envz52zz__objectz00, BgL_bgl_za762za752objectza7d2w4005za7, BGl_z62z52objectzd2wideningze2zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2mutatorzd2envzd2zz__objectz00, BgL_bgl_za762classza7d2field4006z00, BGl_z62classzd2fieldzd2mutatorz62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2accessorzd2envzd2zz__objectz00, BgL_bgl_za762classza7d2field4007z00, BGl_z62classzd2fieldzd2accessorz62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3500z00zz__objectz00, BgL_bgl_string3500za700za7za7_4008za7, "&class-creator", 14 );
DEFINE_STRING( BGl_string3501z00zz__objectz00, BgL_bgl_string3501za700za7za7_4009za7, "class-nil-init!", 15 );
DEFINE_STRING( BGl_string3502z00zz__objectz00, BgL_bgl_string3502za700za7za7_4010za7, "class-nil-init!:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string3505z00zz__objectz00, BgL_bgl_string3505za700za7za7_4011za7, "fun1536", 7 );
DEFINE_STRING( BGl_string3508z00zz__objectz00, BgL_bgl_string3508za700za7za7_4012za7, "fun1534", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2defaultzd2valuezf3zd2envzf3zz__objectz00, BgL_bgl_za762classza7d2field4013z00, BGl_z62classzd2fieldzd2defaultzd2valuezf3z43zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3510z00zz__objectz00, BgL_bgl_string3510za700za7za7_4014za7, "o", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2virtualzd2envz00zz__objectz00, BgL_bgl_za762classza7d2virtu4015z00, BGl_z62classzd2virtualzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_widezd2objectzf3zd2envzf3zz__objectz00, BgL_bgl_za762wideza7d2object4016z00, BGl_z62widezd2objectzf3z43zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3513z00zz__objectz00, BgL_bgl_string3513za700za7za7_4017za7, "proc", 4 );
DEFINE_STRING( BGl_string3515z00zz__objectz00, BgL_bgl_string3515za700za7za7_4018za7, "wo", 2 );
DEFINE_STRING( BGl_string3518z00zz__objectz00, BgL_bgl_string3518za700za7za7_4019za7, "fun1537", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2nextzd2virtualzd2getterzd2envz00zz__objectz00, BgL_bgl_za762callza7d2nextza7d4020za7, BGl_z62callzd2nextzd2virtualzd2getterzb0zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldszd2envz00zz__objectz00, BgL_bgl_za762classza7d2field4021z00, BGl_z62classzd2fieldszb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3600z00zz__objectz00, BgL_bgl_string3600za700za7za7_4022za7, "&%isa64-object/cdepth?", 22 );
DEFINE_STRING( BGl_string3601z00zz__objectz00, BgL_bgl_string3601za700za7za7_4023za7, "&%isa/final?", 12 );
DEFINE_STRING( BGl_string3520z00zz__objectz00, BgL_bgl_string3520za700za7za7_4024za7, "&class-nil-init!", 16 );
DEFINE_STRING( BGl_string3602z00zz__objectz00, BgL_bgl_string3602za700za7za7_4025za7, "&%isa-object/final?", 19 );
DEFINE_STRING( BGl_string3521z00zz__objectz00, BgL_bgl_string3521za700za7za7_4026za7, "class-nil", 9 );
DEFINE_STRING( BGl_string3603z00zz__objectz00, BgL_bgl_string3603za700za7za7_4027za7, "allocate-instance", 17 );
DEFINE_STRING( BGl_string3522z00zz__objectz00, BgL_bgl_string3522za700za7za7_4028za7, "&class-nil", 10 );
DEFINE_STRING( BGl_string3441z00zz__objectz00, BgL_bgl_string3441za700za7za7_4029za7, "/tmp/bigloo/runtime/Llib/object.scm", 35 );
DEFINE_STRING( BGl_string3604z00zz__objectz00, BgL_bgl_string3604za700za7za7_4030za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string3523z00zz__objectz00, BgL_bgl_string3523za700za7za7_4031za7, "initialize-objects!", 19 );
DEFINE_STRING( BGl_string3442z00zz__objectz00, BgL_bgl_string3442za700za7za7_4032za7, "loop", 4 );
DEFINE_STRING( BGl_string3524z00zz__objectz00, BgL_bgl_string3524za700za7za7_4033za7, "extend-vector", 13 );
DEFINE_STRING( BGl_string3443z00zz__objectz00, BgL_bgl_string3443za700za7za7_4034za7, "bint", 4 );
DEFINE_STRING( BGl_string3525z00zz__objectz00, BgL_bgl_string3525za700za7za7_4035za7, "double-nb-classes!", 18 );
DEFINE_STRING( BGl_string3444z00zz__objectz00, BgL_bgl_string3444za700za7za7_4036za7, "vector", 6 );
DEFINE_STRING( BGl_string3607z00zz__objectz00, BgL_bgl_string3607za700za7za7_4037za7, "alloc", 5 );
DEFINE_STRING( BGl_string3526z00zz__objectz00, BgL_bgl_string3526za700za7za7_4038za7, "double-nb-generics!", 19 );
DEFINE_STRING( BGl_string3445z00zz__objectz00, BgL_bgl_string3445za700za7za7_4039za7, "class", 5 );
DEFINE_STRING( BGl_string3527z00zz__objectz00, BgL_bgl_string3527za700za7za7_4040za7, "&object-class-num", 17 );
DEFINE_STRING( BGl_string3446z00zz__objectz00, BgL_bgl_string3446za700za7za7_4041za7, "&class-exists", 13 );
DEFINE_STRING( BGl_string3528z00zz__objectz00, BgL_bgl_string3528za700za7za7_4042za7, "object", 6 );
DEFINE_STRING( BGl_string3447z00zz__objectz00, BgL_bgl_string3447za700za7za7_4043za7, "symbol", 6 );
DEFINE_STRING( BGl_string3529z00zz__objectz00, BgL_bgl_string3529za700za7za7_4044za7, "&object-class-num-set!", 22 );
DEFINE_STRING( BGl_string3448z00zz__objectz00, BgL_bgl_string3448za700za7za7_4045za7, "find-class", 10 );
DEFINE_STRING( BGl_string3449z00zz__objectz00, BgL_bgl_string3449za700za7za7_4046za7, "Cannot find class", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52isazf2cdepthzf3zd2envz81zz__objectz00, BgL_bgl_za762za752isaza7f2cdep4047za7, BGl_z62z52isazf2cdepthzf3z31zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3610z00zz__objectz00, BgL_bgl_string3610za700za7za7_4048za7, "fun1821", 7 );
DEFINE_STRING( BGl_string3530z00zz__objectz00, BgL_bgl_string3530za700za7za7_4049za7, "&object-class", 13 );
DEFINE_STRING( BGl_string3612z00zz__objectz00, BgL_bgl_string3612za700za7za7_4050za7, "&allocate-instance", 18 );
DEFINE_STRING( BGl_string3531z00zz__objectz00, BgL_bgl_string3531za700za7za7_4051za7, "generic-default", 15 );
DEFINE_STRING( BGl_string3450z00zz__objectz00, BgL_bgl_string3450za700za7za7_4052za7, "&find-class", 11 );
DEFINE_STRING( BGl_string3613z00zz__objectz00, BgL_bgl_string3613za700za7za7_4053za7, "&wide-object?", 13 );
DEFINE_STRING( BGl_string3532z00zz__objectz00, BgL_bgl_string3532za700za7za7_4054za7, "&generic-default", 16 );
DEFINE_STRING( BGl_string3451z00zz__objectz00, BgL_bgl_string3451za700za7za7_4055za7, "&find-class-by-hash", 19 );
DEFINE_STRING( BGl_string3614z00zz__objectz00, BgL_bgl_string3614za700za7za7_4056za7, "call-virtual-getter", 19 );
DEFINE_STRING( BGl_string3533z00zz__objectz00, BgL_bgl_string3533za700za7za7_4057za7, "&generic-method-array", 21 );
DEFINE_STRING( BGl_string3452z00zz__objectz00, BgL_bgl_string3452za700za7za7_4058za7, "eval-class?", 11 );
DEFINE_STRING( BGl_string3615z00zz__objectz00, BgL_bgl_string3615za700za7za7_4059za7, "call-virtual-getter:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string3534z00zz__objectz00, BgL_bgl_string3534za700za7za7_4060za7, "method-array-ref", 16 );
DEFINE_STRING( BGl_string3453z00zz__objectz00, BgL_bgl_string3453za700za7za7_4061za7, "&class-name", 11 );
DEFINE_STRING( BGl_string3535z00zz__objectz00, BgL_bgl_string3535za700za7za7_4062za7, "&method-array-ref", 17 );
DEFINE_STRING( BGl_string3454z00zz__objectz00, BgL_bgl_string3454za700za7za7_4063za7, "&class-module", 13 );
DEFINE_EXPORT_BGL_GENERIC( BGl_objectzd2printzd2envz00zz__objectz00, BgL_bgl_za762objectza7d2prin4064z00, BGl_z62objectzd2printzb0zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3536z00zz__objectz00, BgL_bgl_string3536za700za7za7_4065za7, "method-array-set!", 17 );
DEFINE_STRING( BGl_string3455z00zz__objectz00, BgL_bgl_string3455za700za7za7_4066za7, "&class-index", 12 );
DEFINE_STRING( BGl_string3618z00zz__objectz00, BgL_bgl_string3618za700za7za7_4067za7, "getter", 6 );
DEFINE_STRING( BGl_string3456z00zz__objectz00, BgL_bgl_string3456za700za7za7_4068za7, "&class-num", 10 );
DEFINE_STRING( BGl_string3538z00zz__objectz00, BgL_bgl_string3538za700za7za7_4069za7, "generic", 7 );
DEFINE_STRING( BGl_string3457z00zz__objectz00, BgL_bgl_string3457za700za7za7_4070za7, "&class-virtual", 14 );
DEFINE_STRING( BGl_string3458z00zz__objectz00, BgL_bgl_string3458za700za7za7_4071za7, "&class-evdata", 13 );
DEFINE_STRING( BGl_string3459z00zz__objectz00, BgL_bgl_string3459za700za7za7_4072za7, "&class-evdata-set!", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_objectzf3zd2envz21zz__objectz00, BgL_bgl_za762objectza7f3za791za74073z00, BGl_z62objectzf3z91zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_GENERIC( BGl_objectzd2displayzd2envz00zz__objectz00, BgL_bgl_za762objectza7d2disp4074z00, va_generic_entry, BGl_z62objectzd2displayzb0zz__objectz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_allocatezd2instancezd2envz00zz__objectz00, BgL_bgl_za762allocateza7d2in4075z00, BGl_z62allocatezd2instancezb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3620z00zz__objectz00, BgL_bgl_string3620za700za7za7_4076za7, "obj", 3 );
DEFINE_STRING( BGl_string3702z00zz__objectz00, BgL_bgl_string3702za700za7za7_4077za7, "index", 5 );
DEFINE_STRING( BGl_string3621z00zz__objectz00, BgL_bgl_string3621za700za7za7_4078za7, "&call-virtual-getter", 20 );
DEFINE_STRING( BGl_string3622z00zz__objectz00, BgL_bgl_string3622za700za7za7_4079za7, "call-virtual-setter", 19 );
DEFINE_STRING( BGl_string3541z00zz__objectz00, BgL_bgl_string3541za700za7za7_4080za7, "mtable-size", 11 );
DEFINE_STRING( BGl_string3460z00zz__objectz00, BgL_bgl_string3460za700za7za7_4081za7, "class-evfields-set!", 19 );
DEFINE_STRING( BGl_string3623z00zz__objectz00, BgL_bgl_string3623za700za7za7_4082za7, "call-virtual-setter:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string3461z00zz__objectz00, BgL_bgl_string3461za700za7za7_4083za7, "Fields already set", 18 );
DEFINE_STRING( BGl_string3543z00zz__objectz00, BgL_bgl_string3543za700za7za7_4084za7, "method-array-size", 17 );
DEFINE_STRING( BGl_string3462z00zz__objectz00, BgL_bgl_string3462za700za7za7_4085za7, "Not an eval class", 17 );
DEFINE_STRING( BGl_string3463z00zz__objectz00, BgL_bgl_string3463za700za7za7_4086za7, "&class-evfields-set!", 20 );
DEFINE_STRING( BGl_string3707z00zz__objectz00, BgL_bgl_string3707za700za7za7_4087za7, "&index-out-of-bounds-error", 26 );
DEFINE_STRING( BGl_string3626z00zz__objectz00, BgL_bgl_string3626za700za7za7_4088za7, "setter", 6 );
DEFINE_STRING( BGl_string3545z00zz__objectz00, BgL_bgl_string3545za700za7za7_4089za7, "generic-bucket-size", 19 );
DEFINE_STRING( BGl_string3464z00zz__objectz00, BgL_bgl_string3464za700za7za7_4090za7, "&class-fields", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2classzd2fieldzd2envzd2zz__objectz00, BgL_bgl_za762makeza7d2classza74091za7, BGl_z62makezd2classzd2fieldz62zz__objectz00, 0L, BUNSPEC, 8 );
DEFINE_STRING( BGl_string3465z00zz__objectz00, BgL_bgl_string3465za700za7za7_4092za7, "&class-all-fields", 17 );
DEFINE_STRING( BGl_string3628z00zz__objectz00, BgL_bgl_string3628za700za7za7_4093za7, "value", 5 );
DEFINE_STRING( BGl_string3547z00zz__objectz00, BgL_bgl_string3547za700za7za7_4094za7, "max-class", 9 );
DEFINE_STRING( BGl_string3466z00zz__objectz00, BgL_bgl_string3466za700za7za7_4095za7, "class-field", 11 );
DEFINE_STRING( BGl_string3629z00zz__objectz00, BgL_bgl_string3629za700za7za7_4096za7, "&call-virtual-setter", 20 );
DEFINE_STRING( BGl_string3467z00zz__objectz00, BgL_bgl_string3467za700za7za7_4097za7, "&find-class-field", 17 );
DEFINE_STRING( BGl_string3549z00zz__objectz00, BgL_bgl_string3549za700za7za7_4098za7, "max-generic", 11 );
DEFINE_STRING( BGl_string3468z00zz__objectz00, BgL_bgl_string3468za700za7za7_4099za7, "&make-class-field", 17 );
DEFINE_STRING( BGl_string3469z00zz__objectz00, BgL_bgl_string3469za700za7za7_4100za7, "class-field-name", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2classzd2byzd2hashzd2envz00zz__objectz00, BgL_bgl_za762findza7d2classza74101za7, BGl_z62findzd2classzd2byzd2hashzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3700z00zz__objectz00, BgL_bgl_za762lambda1956za7624102z00, BGl_z62lambda1956z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3703z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4103za7, BGl_z62zc3z04anonymousza31952ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3704z00zz__objectz00, BgL_bgl_za762lambda1950za7624104z00, BGl_z62lambda1950z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52isazd2objectzf2cdepthzf3zd2envz53zz__objectz00, BgL_bgl_za762za752isaza7d2obje4105za7, BGl_z62z52isazd2objectzf2cdepthzf3ze3zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3705z00zz__objectz00, BgL_bgl_za762lambda1948za7624106z00, BGl_z62lambda1948z62zz__objectz00, 0L, BUNSPEC, 7 );
DEFINE_STRING( BGl_string3630z00zz__objectz00, BgL_bgl_string3630za700za7za7_4107za7, "call-next-virtual-getter", 24 );
DEFINE_STRING( BGl_string3712z00zz__objectz00, BgL_bgl_string3712za700za7za7_4108za7, "&io-error", 9 );
DEFINE_STRING( BGl_string3631z00zz__objectz00, BgL_bgl_string3631za700za7za7_4109za7, "call-next-virtual-getter:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string3550z00zz__objectz00, BgL_bgl_string3550za700za7za7_4110za7, "<@anonymous:1627>", 17 );
DEFINE_STRING( BGl_string3551z00zz__objectz00, BgL_bgl_string3551za700za7za7_4111za7, "map", 3 );
DEFINE_STRING( BGl_string3470z00zz__objectz00, BgL_bgl_string3470za700za7za7_4112za7, "&class-field-name", 17 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3708z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4113za7, BGl_z62zc3z04anonymousza31967ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3552z00zz__objectz00, BgL_bgl_string3552za700za7za7_4114za7, "list", 4 );
DEFINE_STRING( BGl_string3471z00zz__objectz00, BgL_bgl_string3471za700za7za7_4115za7, "&class-field-virtual?", 21 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3709z00zz__objectz00, BgL_bgl_za762lambda1965za7624116z00, BGl_z62lambda1965z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3634z00zz__objectz00, BgL_bgl_string3634za700za7za7_4117za7, "fun1836", 7 );
DEFINE_STRING( BGl_string3553z00zz__objectz00, BgL_bgl_string3553za700za7za7_4118za7, "new-class!", 10 );
DEFINE_STRING( BGl_string3472z00zz__objectz00, BgL_bgl_string3472za700za7za7_4119za7, "class-field-accessor", 20 );
DEFINE_STRING( BGl_string3635z00zz__objectz00, BgL_bgl_string3635za700za7za7_4120za7, "&call-next-virtual-getter", 25 );
DEFINE_STRING( BGl_string3554z00zz__objectz00, BgL_bgl_string3554za700za7za7_4121za7, "vector-set!", 11 );
DEFINE_STRING( BGl_string3473z00zz__objectz00, BgL_bgl_string3473za700za7za7_4122za7, "procedure", 9 );
DEFINE_STRING( BGl_string3717z00zz__objectz00, BgL_bgl_string3717za700za7za7_4123za7, "&io-port-error", 14 );
DEFINE_STRING( BGl_string3636z00zz__objectz00, BgL_bgl_string3636za700za7za7_4124za7, "call-next-virtual-setter", 24 );
DEFINE_STRING( BGl_string3555z00zz__objectz00, BgL_bgl_string3555za700za7za7_4125za7, "Illegal super-class for class", 29 );
DEFINE_STRING( BGl_string3474z00zz__objectz00, BgL_bgl_string3474za700za7za7_4126za7, "&class-field-accessor", 21 );
DEFINE_STRING( BGl_string3637z00zz__objectz00, BgL_bgl_string3637za700za7za7_4127za7, "call-next-virtual-setter:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string3556z00zz__objectz00, BgL_bgl_string3556za700za7za7_4128za7, "register-class!", 15 );
DEFINE_STRING( BGl_string3475z00zz__objectz00, BgL_bgl_string3475za700za7za7_4129za7, "&class-field-mutable?", 21 );
DEFINE_STRING( BGl_string3557z00zz__objectz00, BgL_bgl_string3557za700za7za7_4130za7, "Fields not a vector", 19 );
DEFINE_STRING( BGl_string3476z00zz__objectz00, BgL_bgl_string3476za700za7za7_4131za7, "class-field-mutator", 19 );
DEFINE_STRING( BGl_string3558z00zz__objectz00, BgL_bgl_string3558za700za7za7_4132za7, ")", 1 );
DEFINE_STRING( BGl_string3477z00zz__objectz00, BgL_bgl_string3477za700za7za7_4133za7, "&class-field-mutator", 20 );
DEFINE_STRING( BGl_string3559z00zz__objectz00, BgL_bgl_string3559za700za7za7_4134za7, "@", 1 );
DEFINE_STRING( BGl_string3478z00zz__objectz00, BgL_bgl_string3478za700za7za7_4135za7, "&class-field-info", 17 );
DEFINE_STRING( BGl_string3479z00zz__objectz00, BgL_bgl_string3479za700za7za7_4136za7, "&class-field-default-value?", 27 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2infozd2envzd2zz__objectz00, BgL_bgl_za762classza7d2field4137z00, BGl_z62classzd2fieldzd2infoz62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3710z00zz__objectz00, BgL_bgl_za762lambda1963za7624138z00, BGl_z62lambda1963z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3713z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4139za7, BGl_z62zc3z04anonymousza31977ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3714z00zz__objectz00, BgL_bgl_za762lambda1975za7624140z00, BGl_z62lambda1975z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3802z00zz__objectz00, BgL_bgl_string3802za700za7za7_4141za7, "&access-control-exception", 25 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3715z00zz__objectz00, BgL_bgl_za762lambda1973za7624142z00, BGl_z62lambda1973z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STRING( BGl_string3640z00zz__objectz00, BgL_bgl_string3640za700za7za7_4143za7, "fun1839", 7 );
DEFINE_STRING( BGl_string3722z00zz__objectz00, BgL_bgl_string3722za700za7za7_4144za7, "&io-read-error", 14 );
DEFINE_STRING( BGl_string3641z00zz__objectz00, BgL_bgl_string3641za700za7za7_4145za7, "&call-next-virtual-setter", 25 );
DEFINE_STRING( BGl_string3560z00zz__objectz00, BgL_bgl_string3560za700za7za7_4146za7, "\" (", 3 );
DEFINE_STRING( BGl_string3642z00zz__objectz00, BgL_bgl_string3642za700za7za7_4147za7, "&object-widening", 16 );
DEFINE_STRING( BGl_string3561z00zz__objectz00, BgL_bgl_string3561za700za7za7_4148za7, "Dangerous class redefinition: \"", 31 );
DEFINE_STRING( BGl_string3480z00zz__objectz00, BgL_bgl_string3480za700za7za7_4149za7, "class-field-default-value:Wrong number of arguments", 51 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3718z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4150za7, BGl_z62zc3z04anonymousza31987ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3643z00zz__objectz00, BgL_bgl_string3643za700za7za7_4151za7, "&object-widening-set!", 21 );
DEFINE_STRING( BGl_string3562z00zz__objectz00, BgL_bgl_string3562za700za7za7_4152za7, "&register-class!", 16 );
DEFINE_STRING( BGl_string3806z00zz__objectz00, BgL_bgl_string3806za700za7za7_4153za7, "args", 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3719z00zz__objectz00, BgL_bgl_za762lambda1985za7624154z00, BGl_z62lambda1985z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3644z00zz__objectz00, BgL_bgl_string3644za700za7za7_4155za7, "&%object-widening", 17 );
DEFINE_STRING( BGl_string3563z00zz__objectz00, BgL_bgl_string3563za700za7za7_4156za7, "fill-vector-with-virtuals!", 26 );
DEFINE_STRING( BGl_string3645z00zz__objectz00, BgL_bgl_string3645za700za7za7_4157za7, "&%object-widening-set!", 22 );
DEFINE_STRING( BGl_string3564z00zz__objectz00, BgL_bgl_string3564za700za7za7_4158za7, "<@anonymous:1709>", 17 );
DEFINE_STRING( BGl_string3483z00zz__objectz00, BgL_bgl_string3483za700za7za7_4159za7, "funcall", 7 );
DEFINE_STRING( BGl_string3727z00zz__objectz00, BgL_bgl_string3727za700za7za7_4160za7, "&io-write-error", 15 );
DEFINE_STRING( BGl_string3565z00zz__objectz00, BgL_bgl_string3565za700za7za7_4161za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2typeszd2numberzd2envzd2zz__objectz00, BgL_bgl_za762biglooza7d2type4162z00, BGl_z62bigloozd2typeszd2numberz62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3566z00zz__objectz00, BgL_bgl_string3566za700za7za7_4163za7, "for-each", 8 );
DEFINE_STRING( BGl_string3485z00zz__objectz00, BgL_bgl_string3485za700za7za7_4164za7, "p", 1 );
DEFINE_STRING( BGl_string3567z00zz__objectz00, BgL_bgl_string3567za700za7za7_4165za7, "make-class-virtual-slots-vector", 31 );
DEFINE_STRING( BGl_string3486z00zz__objectz00, BgL_bgl_string3486za700za7za7_4166za7, "class-field-default-value", 25 );
DEFINE_STRING( BGl_string3568z00zz__objectz00, BgL_bgl_string3568za700za7za7_4167za7, "make-method-array", 17 );
DEFINE_STRING( BGl_string3487z00zz__objectz00, BgL_bgl_string3487za700za7za7_4168za7, "This field has no default value", 31 );
DEFINE_STRING( BGl_string3569z00zz__objectz00, BgL_bgl_string3569za700za7za7_4169za7, "unoptimal bigloo-generic-bucket-size: ", 38 );
DEFINE_STRING( BGl_string3488z00zz__objectz00, BgL_bgl_string3488za700za7za7_4170za7, "&class-field-default-value", 26 );
DEFINE_STRING( BGl_string3489z00zz__objectz00, BgL_bgl_string3489za700za7za7_4171za7, "&class-field-type", 17 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3800z00zz__objectz00, BgL_bgl_za762lambda2127za7624172z00, BGl_z62lambda2127z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3720z00zz__objectz00, BgL_bgl_za762lambda1983za7624173z00, BGl_z62lambda1983z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3803z00zz__objectz00, BgL_bgl_za762lambda2162za7624174z00, BGl_z62lambda2162z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3804z00zz__objectz00, BgL_bgl_za762lambda2161za7624175z00, BGl_z62lambda2161z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3723z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4176za7, BGl_z62zc3z04anonymousza31997ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3811z00zz__objectz00, BgL_bgl_string3811za700za7za7_4177za7, "&warning", 8 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3724z00zz__objectz00, BgL_bgl_za762lambda1995za7624178z00, BGl_z62lambda1995z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3725z00zz__objectz00, BgL_bgl_za762lambda1993za7624179z00, BGl_z62lambda1993z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3807z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4180za7, BGl_z62zc3z04anonymousza32157ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3732z00zz__objectz00, BgL_bgl_string3732za700za7za7_4181za7, "&io-closed-error", 16 );
DEFINE_STRING( BGl_string3651z00zz__objectz00, BgL_bgl_string3651za700za7za7_4182za7, "__object", 8 );
DEFINE_STRING( BGl_string3570z00zz__objectz00, BgL_bgl_string3570za700za7za7_4183za7, "No default behavior", 19 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3808z00zz__objectz00, BgL_bgl_za762lambda2155za7624184z00, BGl_z62lambda2155z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3646z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4185za7, BGl_z62zc3z04anonymousza31850ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3571z00zz__objectz00, BgL_bgl_string3571za700za7za7_4186za7, "&procedure->generic", 19 );
DEFINE_STRING( BGl_string3490z00zz__objectz00, BgL_bgl_string3490za700za7za7_4187za7, "&class-super", 12 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3809z00zz__objectz00, BgL_bgl_za762lambda2153za7624188z00, BGl_z62lambda2153z62zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3728z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4189za7, BGl_z62zc3z04anonymousza32007ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3647z00zz__objectz00, BgL_bgl_za762lambda1848za7624190z00, BGl_z62lambda1848z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3572z00zz__objectz00, BgL_bgl_string3572za700za7za7_4191za7, "&register-generic!", 18 );
DEFINE_STRING( BGl_string3491z00zz__objectz00, BgL_bgl_string3491za700za7za7_4192za7, "class-allocator", 15 );
DEFINE_STRING( BGl_string3816z00zz__objectz00, BgL_bgl_string3816za700za7za7_4193za7, "&eval-warning", 13 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3729z00zz__objectz00, BgL_bgl_za762lambda2005za7624194z00, BGl_z62lambda2005z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3648z00zz__objectz00, BgL_bgl_za762lambda1846za7624195z00, BGl_z62lambda1846z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3573z00zz__objectz00, BgL_bgl_string3573za700za7za7_4196za7, "register-generic-sans-lock!", 27 );
DEFINE_STRING( BGl_string3492z00zz__objectz00, BgL_bgl_string3492za700za7za7_4197za7, "class-abstract?", 15 );
DEFINE_STRING( BGl_string3817z00zz__objectz00, BgL_bgl_string3817za700za7za7_4198za7, "&<@anonymous:2172>", 18 );
DEFINE_STRING( BGl_string3574z00zz__objectz00, BgL_bgl_string3574za700za7za7_4199za7, "laap", 4 );
DEFINE_STRING( BGl_string3493z00zz__objectz00, BgL_bgl_string3493za700za7za7_4200za7, "&class-abstract?", 16 );
DEFINE_STRING( BGl_string3818z00zz__objectz00, BgL_bgl_string3818za700za7za7_4201za7, "&<@anonymous:2157>", 18 );
DEFINE_STRING( BGl_string3737z00zz__objectz00, BgL_bgl_string3737za700za7za7_4202za7, "&io-file-not-found-error", 24 );
DEFINE_STRING( BGl_string3656z00zz__objectz00, BgL_bgl_string3656za700za7za7_4203za7, "&condition", 10 );
DEFINE_STRING( BGl_string3575z00zz__objectz00, BgL_bgl_string3575za700za7za7_4204za7, "%add-method!", 12 );
DEFINE_STRING( BGl_string3494z00zz__objectz00, BgL_bgl_string3494za700za7za7_4205za7, "class-shrink", 12 );
DEFINE_STRING( BGl_string3819z00zz__objectz00, BgL_bgl_string3819za700za7za7_4206za7, "&lambda2162", 11 );
DEFINE_STRING( BGl_string3576z00zz__objectz00, BgL_bgl_string3576za700za7za7_4207za7, "", 0 );
DEFINE_STRING( BGl_string3495z00zz__objectz00, BgL_bgl_string3495za700za7za7_4208za7, "&class-wide?", 12 );
DEFINE_STRING( BGl_string3577z00zz__objectz00, BgL_bgl_string3577za700za7za7_4209za7, "loop~0", 6 );
DEFINE_STRING( BGl_string3496z00zz__objectz00, BgL_bgl_string3496za700za7za7_4210za7, "&class-subclasses", 17 );
DEFINE_STRING( BGl_string3578z00zz__objectz00, BgL_bgl_string3578za700za7za7_4211za7, "method/generic arity mismatch, expecting ~a", 43 );
DEFINE_STRING( BGl_string3497z00zz__objectz00, BgL_bgl_string3497za700za7za7_4212za7, "&class-allocator", 16 );
DEFINE_STRING( BGl_string3579z00zz__objectz00, BgL_bgl_string3579za700za7za7_4213za7, "generic-add-method!", 19 );
DEFINE_STRING( BGl_string3498z00zz__objectz00, BgL_bgl_string3498za700za7za7_4214za7, "&class-hash", 11 );
DEFINE_STRING( BGl_string3499z00zz__objectz00, BgL_bgl_string3499za700za7za7_4215za7, "&class-constructor", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2existszd2envz00zz__objectz00, BgL_bgl_za762classza7d2exist4216z00, BGl_z62classzd2existszb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2nilzd2envz00zz__objectz00, BgL_bgl_za762classza7d2nilza7b4217za7, BGl_z62classzd2nilzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3730z00zz__objectz00, BgL_bgl_za762lambda2003za7624218z00, BGl_z62lambda2003z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3812z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4219za7, BGl_z62zc3z04anonymousza32172ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3813z00zz__objectz00, BgL_bgl_za762lambda2170za7624220z00, BGl_z62lambda2170z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3901z00zz__objectz00, BgL_bgl_string3901za700za7za7_4221za7, "port", 4 );
DEFINE_STRING( BGl_string3820z00zz__objectz00, BgL_bgl_string3820za700za7za7_4222za7, "&lambda2161", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3814z00zz__objectz00, BgL_bgl_za762lambda2168za7624223z00, BGl_z62lambda2168z62zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3733z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4224za7, BGl_z62zc3z04anonymousza32017ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3652z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4225za7, BGl_z62zc3z04anonymousza31861ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3902z00zz__objectz00, BgL_bgl_string3902za700za7za7_4226za7, "#|", 2 );
DEFINE_STRING( BGl_string3821z00zz__objectz00, BgL_bgl_string3821za700za7za7_4227za7, "&<@anonymous:2132>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3734z00zz__objectz00, BgL_bgl_za762lambda2015za7624228z00, BGl_z62lambda2015z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3653z00zz__objectz00, BgL_bgl_za762lambda1859za7624229z00, BGl_z62lambda1859z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3903z00zz__objectz00, BgL_bgl_string3903za700za7za7_4230za7, " nil|", 5 );
DEFINE_STRING( BGl_string3822z00zz__objectz00, BgL_bgl_string3822za700za7za7_4231za7, "&lambda2127", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3735z00zz__objectz00, BgL_bgl_za762lambda2013za7624232z00, BGl_z62lambda2013z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3654z00zz__objectz00, BgL_bgl_za762lambda1857za7624233z00, BGl_z62lambda1857z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3904z00zz__objectz00, BgL_bgl_string3904za700za7za7_4234za7, "&object-print1399", 17 );
DEFINE_STRING( BGl_string3823z00zz__objectz00, BgL_bgl_string3823za700za7za7_4235za7, "&lambda2146", 11 );
DEFINE_STRING( BGl_string3742z00zz__objectz00, BgL_bgl_string3742za700za7za7_4236za7, "&io-parse-error", 15 );
DEFINE_STRING( BGl_string3661z00zz__objectz00, BgL_bgl_string3661za700za7za7_4237za7, "fname", 5 );
DEFINE_STRING( BGl_string3580z00zz__objectz00, BgL_bgl_string3580za700za7za7_4238za7, "Illegal class for method", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52isa32zd2objectzf2cdepthzf3zd2envz53zz__objectz00, BgL_bgl_za762za752isa32za7d2ob4239za7, BGl_z62z52isa32zd2objectzf2cdepthzf3ze3zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3905z00zz__objectz00, BgL_bgl_string3905za700za7za7_4240za7, "output-port", 11 );
DEFINE_STRING( BGl_string3824z00zz__objectz00, BgL_bgl_string3824za700za7za7_4241za7, "&lambda2145", 11 );
DEFINE_STRING( BGl_string3581z00zz__objectz00, BgL_bgl_string3581za700za7za7_4242za7, "&generic-add-method!", 20 );
DEFINE_STRING( BGl_string3906z00zz__objectz00, BgL_bgl_string3906za700za7za7_4243za7, "&object-hashnumber1396", 22 );
DEFINE_STRING( BGl_string3825z00zz__objectz00, BgL_bgl_string3825za700za7za7_4244za7, "&lambda2138", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3738z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4245za7, BGl_z62zc3z04anonymousza32027ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3657z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4246za7, BGl_z62zc3z04anonymousza31880ze3ze5zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3582z00zz__objectz00, BgL_bgl_string3582za700za7za7_4247za7, "generic-add-eval-method!", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2numzd2envz00zz__objectz00, BgL_bgl_za762classza7d2numza7b4248za7, BGl_z62classzd2numzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3907z00zz__objectz00, BgL_bgl_string3907za700za7za7_4249za7, "&object-write1392", 17 );
DEFINE_STRING( BGl_string3826z00zz__objectz00, BgL_bgl_string3826za700za7za7_4250za7, "&lambda2137", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3739z00zz__objectz00, BgL_bgl_za762lambda2025za7624251z00, BGl_z62lambda2025z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3658z00zz__objectz00, BgL_bgl_za762lambda1879za7624252z00, BGl_z62lambda1879z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3583z00zz__objectz00, BgL_bgl_string3583za700za7za7_4253za7, "&generic-add-eval-method!", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2genericzd2bucketzd2powzd2envz00zz__objectz00, BgL_bgl_za762biglooza7d2gene4254z00, BGl_z62bigloozd2genericzd2bucketzd2powzb0zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3908z00zz__objectz00, BgL_bgl_string3908za700za7za7_4255za7, "&object-display1389", 19 );
DEFINE_STRING( BGl_string3827z00zz__objectz00, BgL_bgl_string3827za700za7za7_4256za7, "&<@anonymous:2114>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3659z00zz__objectz00, BgL_bgl_za762lambda1878za7624257z00, BGl_z62lambda1878z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3584z00zz__objectz00, BgL_bgl_string3584za700za7za7_4258za7, "find-method", 11 );
DEFINE_STRING( BGl_string3828z00zz__objectz00, BgL_bgl_string3828za700za7za7_4259za7, "&lambda2110", 11 );
DEFINE_STRING( BGl_string3747z00zz__objectz00, BgL_bgl_string3747za700za7za7_4260za7, "&io-unknown-host-error", 22 );
DEFINE_STRING( BGl_string3666z00zz__objectz00, BgL_bgl_string3666za700za7za7_4261za7, "location", 8 );
DEFINE_STRING( BGl_string3585z00zz__objectz00, BgL_bgl_string3585za700za7za7_4262za7, "&find-method", 12 );
DEFINE_STRING( BGl_string3829z00zz__objectz00, BgL_bgl_string3829za700za7za7_4263za7, "&lambda2120", 11 );
DEFINE_STRING( BGl_string3586z00zz__objectz00, BgL_bgl_string3586za700za7za7_4264za7, "find-super-class-method", 23 );
DEFINE_STRING( BGl_string3587z00zz__objectz00, BgL_bgl_string3587za700za7za7_4265za7, "&find-super-class-method", 24 );
DEFINE_STRING( BGl_string3588z00zz__objectz00, BgL_bgl_string3588za700za7za7_4266za7, "&find-method-from", 17 );
DEFINE_STRING( BGl_string3589z00zz__objectz00, BgL_bgl_string3589za700za7za7_4267za7, "nil?", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2virtualzf3zd2envz21zz__objectz00, BgL_bgl_za762classza7d2field4268z00, BGl_z62classzd2fieldzd2virtualzf3z91zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2indexzd2envz00zz__objectz00, BgL_bgl_za762classza7d2index4269z00, BGl_z62classzd2indexzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2virtualzd2getterzd2envzd2zz__objectz00, BgL_bgl_za762callza7d2virtua4270z00, BGl_z62callzd2virtualzd2getterz62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3740z00zz__objectz00, BgL_bgl_za762lambda2023za7624271z00, BGl_z62lambda2023z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STRING( BGl_string3910z00zz__objectz00, BgL_bgl_string3910za700za7za7_4272za7, "object-display", 14 );
DEFINE_STRING( BGl_string3911z00zz__objectz00, BgL_bgl_string3911za700za7za7_4273za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string3830z00zz__objectz00, BgL_bgl_string3830za700za7za7_4274za7, "&lambda2119", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3743z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4275za7, BGl_z62zc3z04anonymousza32039ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3662z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4276za7, BGl_z62zc3z04anonymousza31888ze3ze5zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3831z00zz__objectz00, BgL_bgl_string3831za700za7za7_4277za7, "&<@anonymous:2104>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3744z00zz__objectz00, BgL_bgl_za762lambda2037za7624278z00, BGl_z62lambda2037z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3663z00zz__objectz00, BgL_bgl_za762lambda1887za7624279z00, BGl_z62lambda1887z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3832z00zz__objectz00, BgL_bgl_string3832za700za7za7_4280za7, "&<@anonymous:2094>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3745z00zz__objectz00, BgL_bgl_za762lambda2035za7624281z00, BGl_z62lambda2035z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3664z00zz__objectz00, BgL_bgl_za762lambda1886za7624282z00, BGl_z62lambda1886z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3914z00zz__objectz00, BgL_bgl_string3914za700za7za7_4283za7, "apply", 5 );
DEFINE_STRING( BGl_string3833z00zz__objectz00, BgL_bgl_string3833za700za7za7_4284za7, "&<@anonymous:2083>", 18 );
DEFINE_STRING( BGl_string3752z00zz__objectz00, BgL_bgl_string3752za700za7za7_4285za7, "&io-malformed-url-error", 23 );
DEFINE_STRING( BGl_string3671z00zz__objectz00, BgL_bgl_string3671za700za7za7_4286za7, "stack", 5 );
DEFINE_STRING( BGl_string3590z00zz__objectz00, BgL_bgl_string3590za700za7za7_4287za7, "&nil?", 5 );
DEFINE_STRING( BGl_string3834z00zz__objectz00, BgL_bgl_string3834za700za7za7_4288za7, "&<@anonymous:2073>", 18 );
DEFINE_STRING( BGl_string3591z00zz__objectz00, BgL_bgl_string3591za700za7za7_4289za7, "isa?", 4 );
DEFINE_STRING( BGl_string3916z00zz__objectz00, BgL_bgl_string3916za700za7za7_4290za7, "method1391", 10 );
DEFINE_STRING( BGl_string3835z00zz__objectz00, BgL_bgl_string3835za700za7za7_4291za7, "&<@anonymous:2062>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3748z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4292za7, BGl_z62zc3z04anonymousza32050ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3667z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4293za7, BGl_z62zc3z04anonymousza31895ze3ze5zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3592z00zz__objectz00, BgL_bgl_string3592za700za7za7_4294za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string3836z00zz__objectz00, BgL_bgl_string3836za700za7za7_4295za7, "&<@anonymous:2050>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3749z00zz__objectz00, BgL_bgl_za762lambda2048za7624296z00, BGl_z62lambda2048z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3668z00zz__objectz00, BgL_bgl_za762lambda1894za7624297z00, BGl_z62lambda1894z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3593z00zz__objectz00, BgL_bgl_string3593za700za7za7_4298za7, "&isa?", 5 );
DEFINE_STRING( BGl_string3837z00zz__objectz00, BgL_bgl_string3837za700za7za7_4299za7, "&<@anonymous:2039>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3669z00zz__objectz00, BgL_bgl_za762lambda1893za7624300z00, BGl_z62lambda1893z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3594z00zz__objectz00, BgL_bgl_string3594za700za7za7_4301za7, "%isa/cdepth?", 12 );
DEFINE_STRING( BGl_string3919z00zz__objectz00, BgL_bgl_string3919za700za7za7_4302za7, "let", 3 );
DEFINE_STRING( BGl_string3838z00zz__objectz00, BgL_bgl_string3838za700za7za7_4303za7, "&<@anonymous:2027>", 18 );
DEFINE_STRING( BGl_string3757z00zz__objectz00, BgL_bgl_string3757za700za7za7_4304za7, "&io-sigpipe-error", 17 );
DEFINE_STRING( BGl_string3676z00zz__objectz00, BgL_bgl_string3676za700za7za7_4305za7, "&exception", 10 );
DEFINE_STRING( BGl_string3595z00zz__objectz00, BgL_bgl_string3595za700za7za7_4306za7, "&%isa/cdepth?", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_objectzd2classzd2numzd2envzd2zz__objectz00, BgL_bgl_za762objectza7d2clas4307z00, BGl_z62objectzd2classzd2numz62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3839z00zz__objectz00, BgL_bgl_string3839za700za7za7_4308za7, "&<@anonymous:2017>", 18 );
DEFINE_STRING( BGl_string3596z00zz__objectz00, BgL_bgl_string3596za700za7za7_4309za7, "%isa-object/cdepth?", 19 );
DEFINE_STRING( BGl_string3597z00zz__objectz00, BgL_bgl_string3597za700za7za7_4310za7, "&%isa-object/cdepth?", 20 );
DEFINE_STRING( BGl_string3598z00zz__objectz00, BgL_bgl_string3598za700za7za7_4311za7, "&%isa32-object/cdepth?", 22 );
DEFINE_STRING( BGl_string3599z00zz__objectz00, BgL_bgl_string3599za700za7za7_4312za7, "%isa64-object/cdepth?", 21 );
DEFINE_EXPORT_BGL_GENERIC( BGl_objectzd2equalzf3zd2envzf3zz__objectz00, BgL_bgl_za762objectza7d2equa4313z00, BGl_z62objectzd2equalzf3z43zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3750z00zz__objectz00, BgL_bgl_za762lambda2046za7624314z00, BGl_z62lambda2046z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2methodzd2envz00zz__objectz00, BgL_bgl_za762findza7d2method4315z00, BGl_z62findzd2methodzb0zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3840z00zz__objectz00, BgL_bgl_string3840za700za7za7_4316za7, "&<@anonymous:2007>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3753z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4317za7, BGl_z62zc3z04anonymousza32062ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3672z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4318za7, BGl_z62zc3z04anonymousza31873ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3841z00zz__objectz00, BgL_bgl_string3841za700za7za7_4319za7, "&<@anonymous:1997>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3754z00zz__objectz00, BgL_bgl_za762lambda2060za7624320z00, BGl_z62lambda2060z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3673z00zz__objectz00, BgL_bgl_za762lambda1871za7624321z00, BGl_z62lambda1871z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3923z00zz__objectz00, BgL_bgl_string3923za700za7za7_4322za7, "list2204", 8 );
DEFINE_STRING( BGl_string3842z00zz__objectz00, BgL_bgl_string3842za700za7za7_4323za7, "&<@anonymous:1987>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3755z00zz__objectz00, BgL_bgl_za762lambda2058za7624324z00, BGl_z62lambda2058z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3674z00zz__objectz00, BgL_bgl_za762lambda1869za7624325z00, BGl_z62lambda1869z62zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3843z00zz__objectz00, BgL_bgl_string3843za700za7za7_4326za7, "&<@anonymous:1977>", 18 );
DEFINE_STRING( BGl_string3762z00zz__objectz00, BgL_bgl_string3762za700za7za7_4327za7, "&io-timeout-error", 17 );
DEFINE_STRING( BGl_string3844z00zz__objectz00, BgL_bgl_string3844za700za7za7_4328za7, "&<@anonymous:1967>", 18 );
DEFINE_STRING( BGl_string3682z00zz__objectz00, BgL_bgl_string3682za700za7za7_4329za7, "msg", 3 );
DEFINE_STRING( BGl_string3926z00zz__objectz00, BgL_bgl_string3926za700za7za7_4330za7, "$cons", 5 );
DEFINE_STRING( BGl_string3845z00zz__objectz00, BgL_bgl_string3845za700za7za7_4331za7, "&<@anonymous:1952>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3758z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4332za7, BGl_z62zc3z04anonymousza32073ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3677z00zz__objectz00, BgL_bgl_za762lambda1914za7624333z00, BGl_z62lambda1914z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3846z00zz__objectz00, BgL_bgl_string3846za700za7za7_4334za7, "&lambda1957", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3759z00zz__objectz00, BgL_bgl_za762lambda2071za7624335z00, BGl_z62lambda2071z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3678z00zz__objectz00, BgL_bgl_za762lambda1913za7624336z00, BGl_z62lambda1913z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52isa64zd2objectzf2cdepthzf3zd2envz53zz__objectz00, BgL_bgl_za762za752isa64za7d2ob4337za7, BGl_z62z52isa64zd2objectzf2cdepthzf3ze3zz__objectz00, 0L, BUNSPEC, 3 );
extern obj_t BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
DEFINE_STRING( BGl_string3847z00zz__objectz00, BgL_bgl_string3847za700za7za7_4338za7, "&lambda1956", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3679z00zz__objectz00, BgL_bgl_za762lambda1920za7624339z00, BGl_z62lambda1920z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3929z00zz__objectz00, BgL_bgl_string3929za700za7za7_4340za7, "quote", 5 );
DEFINE_STRING( BGl_string3848z00zz__objectz00, BgL_bgl_string3848za700za7za7_4341za7, "&<@anonymous:1937>", 18 );
DEFINE_STRING( BGl_string3767z00zz__objectz00, BgL_bgl_string3767za700za7za7_4342za7, "&io-connection-error", 20 );
DEFINE_STRING( BGl_string3849z00zz__objectz00, BgL_bgl_string3849za700za7za7_4343za7, "&lambda1942", 11 );
DEFINE_STRING( BGl_string3689z00zz__objectz00, BgL_bgl_string3689za700za7za7_4344za7, "&error", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_genericzd2memoryzd2statisticszd2envzd2zz__objectz00, BgL_bgl_za762genericza7d2mem4345z00, BGl_z62genericzd2memoryzd2statisticsz62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2constructorzd2envz00zz__objectz00, BgL_bgl_za762classza7d2const4346z00, BGl_z62classzd2constructorzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3760z00zz__objectz00, BgL_bgl_za762lambda2069za7624347z00, BGl_z62lambda2069z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2superzd2classzd2methodzd2envz00zz__objectz00, BgL_bgl_za762findza7d2superza74348za7, BGl_z62findzd2superzd2classzd2methodzb0zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3680z00zz__objectz00, BgL_bgl_za762lambda1919za7624349z00, BGl_z62lambda1919z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3850z00zz__objectz00, BgL_bgl_string3850za700za7za7_4350za7, "&lambda1941", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3763z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4351za7, BGl_z62zc3z04anonymousza32083ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3932z00zz__objectz00, BgL_bgl_string3932za700za7za7_4352za7, "cons*", 5 );
DEFINE_STRING( BGl_string3851z00zz__objectz00, BgL_bgl_string3851za700za7za7_4353za7, "&<@anonymous:1907>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3764z00zz__objectz00, BgL_bgl_za762lambda2081za7624354z00, BGl_z62lambda2081z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3683z00zz__objectz00, BgL_bgl_za762lambda1927za7624355z00, BGl_z62lambda1927z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3933z00zz__objectz00, BgL_bgl_string3933za700za7za7_4356za7, "&object-display", 15 );
DEFINE_STRING( BGl_string3852z00zz__objectz00, BgL_bgl_string3852za700za7za7_4357za7, "&lambda1927", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3765z00zz__objectz00, BgL_bgl_za762lambda2079za7624358z00, BGl_z62lambda2079z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3684z00zz__objectz00, BgL_bgl_za762lambda1926za7624359z00, BGl_z62lambda1926z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3853z00zz__objectz00, BgL_bgl_string3853za700za7za7_4360za7, "&lambda1926", 11 );
DEFINE_STRING( BGl_string3772z00zz__objectz00, BgL_bgl_string3772za700za7za7_4361za7, "&process-exception", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3685z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4362za7, BGl_z62zc3z04anonymousza31907ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3935z00zz__objectz00, BgL_bgl_string3935za700za7za7_4363za7, "object-write", 12 );
DEFINE_STRING( BGl_string3854z00zz__objectz00, BgL_bgl_string3854za700za7za7_4364za7, "&lambda1920", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3686z00zz__objectz00, BgL_bgl_za762lambda1904za7624365z00, BGl_z62lambda1904z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3855z00zz__objectz00, BgL_bgl_string3855za700za7za7_4366za7, "&lambda1919", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3768z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4367za7, BGl_z62zc3z04anonymousza32094ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3693z00zz__objectz00, BgL_bgl_string3693za700za7za7_4368za7, "type", 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3687z00zz__objectz00, BgL_bgl_za762lambda1902za7624369z00, BGl_z62lambda1902z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STRING( BGl_string3856z00zz__objectz00, BgL_bgl_string3856za700za7za7_4370za7, "&lambda1914", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3769z00zz__objectz00, BgL_bgl_za762lambda2092za7624371z00, BGl_z62lambda2092z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_genericzd2addzd2evalzd2methodz12zd2envz12zz__objectz00, BgL_bgl_za762genericza7d2add4372z00, BGl_z62genericzd2addzd2evalzd2methodz12za2zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string3938z00zz__objectz00, BgL_bgl_string3938za700za7za7_4373za7, "method1395", 10 );
DEFINE_STRING( BGl_string3857z00zz__objectz00, BgL_bgl_string3857za700za7za7_4374za7, "&lambda1913", 11 );
DEFINE_STRING( BGl_string3858z00zz__objectz00, BgL_bgl_string3858za700za7za7_4375za7, "&<@anonymous:1873>", 18 );
DEFINE_STRING( BGl_string3777z00zz__objectz00, BgL_bgl_string3777za700za7za7_4376za7, "&stack-overflow-error", 21 );
DEFINE_STRING( BGl_string3859z00zz__objectz00, BgL_bgl_string3859za700za7za7_4377za7, "&lambda1894", 11 );
DEFINE_STRING( BGl_string3698z00zz__objectz00, BgL_bgl_string3698za700za7za7_4378za7, "&type-error", 11 );
extern obj_t BGl_writezd2envzd2zz__r4_output_6_10_3z00;
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3770z00zz__objectz00, BgL_bgl_za762lambda2090za7624379z00, BGl_z62lambda2090z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3690z00zz__objectz00, BgL_bgl_za762lambda1942za7624380z00, BGl_z62lambda1942z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3691z00zz__objectz00, BgL_bgl_za762lambda1941za7624381z00, BGl_z62lambda1941z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3860z00zz__objectz00, BgL_bgl_string3860za700za7za7_4382za7, "&lambda1893", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3773z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4383za7, BGl_z62zc3z04anonymousza32104ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3861z00zz__objectz00, BgL_bgl_string3861za700za7za7_4384za7, "&lambda1887", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3774z00zz__objectz00, BgL_bgl_za762lambda2102za7624385z00, BGl_z62lambda2102z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3943z00zz__objectz00, BgL_bgl_string3943za700za7za7_4386za7, "list2205", 8 );
DEFINE_STRING( BGl_string3862z00zz__objectz00, BgL_bgl_string3862za700za7za7_4387za7, "&lambda1886", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3775z00zz__objectz00, BgL_bgl_za762lambda2100za7624388z00, BGl_z62lambda2100z62zz__objectz00, 0L, BUNSPEC, 6 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3694z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4389za7, BGl_z62zc3z04anonymousza31937ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3863z00zz__objectz00, BgL_bgl_string3863za700za7za7_4390za7, "&lambda1879", 11 );
DEFINE_STRING( BGl_string3782z00zz__objectz00, BgL_bgl_string3782za700za7za7_4391za7, "message", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3695z00zz__objectz00, BgL_bgl_za762lambda1935za7624392z00, BGl_z62lambda1935z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3945z00zz__objectz00, BgL_bgl_string3945za700za7za7_4393za7, "&object-write", 13 );
DEFINE_STRING( BGl_string3864z00zz__objectz00, BgL_bgl_string3864za700za7za7_4394za7, "&lambda1878", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3696z00zz__objectz00, BgL_bgl_za762lambda1933za7624395z00, BGl_z62lambda1933z62zz__objectz00, 0L, BUNSPEC, 7 );
DEFINE_STRING( BGl_string3946z00zz__objectz00, BgL_bgl_string3946za700za7za7_4396za7, "object-hashnumber", 17 );
DEFINE_STRING( BGl_string3865z00zz__objectz00, BgL_bgl_string3865za700za7za7_4397za7, "&<@anonymous:1861>", 18 );
DEFINE_STRING( BGl_string3784z00zz__objectz00, BgL_bgl_string3784za700za7za7_4398za7, "bstring", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3778z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4399za7, BGl_z62zc3z04anonymousza32121ze3ze5zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3947z00zz__objectz00, BgL_bgl_string3947za700za7za7_4400za7, "object-hashnumber:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3866z00zz__objectz00, BgL_bgl_string3866za700za7za7_4401za7, "&<@anonymous:1850>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3779z00zz__objectz00, BgL_bgl_za762lambda2120za7624402z00, BGl_z62lambda2120z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3699z00zz__objectz00, BgL_bgl_za762lambda1957za7624403z00, BGl_z62lambda1957z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3868z00zz__objectz00, BgL_bgl_string3868za700za7za7_4404za7, "object-display1389", 18 );
DEFINE_STRING( BGl_string3789z00zz__objectz00, BgL_bgl_string3789za700za7za7_4405za7, "&security-exception", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2evfieldszd2setz12zd2envzc0zz__objectz00, BgL_bgl_za762classza7d2evfie4406z00, BGl_z62classzd2evfieldszd2setz12z70zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3780z00zz__objectz00, BgL_bgl_za762lambda2119za7624407z00, BGl_z62lambda2119z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3950z00zz__objectz00, BgL_bgl_string3950za700za7za7_4408za7, "method1398", 10 );
DEFINE_STRING( BGl_string3951z00zz__objectz00, BgL_bgl_string3951za700za7za7_4409za7, "&object-hashnumber", 18 );
DEFINE_STRING( BGl_string3870z00zz__objectz00, BgL_bgl_string3870za700za7za7_4410za7, "object-write1392", 16 );
DEFINE_STRING( BGl_string3952z00zz__objectz00, BgL_bgl_string3952za700za7za7_4411za7, "object-print:Wrong number of arguments", 38 );
DEFINE_STRING( BGl_string3872z00zz__objectz00, BgL_bgl_string3872za700za7za7_4412za7, "object-hashnumber1396", 21 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3785z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4413za7, BGl_z62zc3z04anonymousza32114ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3867z00zz__objectz00, BgL_bgl_za762objectza7d2disp4414z00, va_generic_entry, BGl_z62objectzd2display1389zb0zz__objectz00, BUNSPEC, -2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3786z00zz__objectz00, BgL_bgl_za762lambda2112za7624415z00, BGl_z62lambda2112z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3955z00zz__objectz00, BgL_bgl_string3955za700za7za7_4416za7, "method1401", 10 );
DEFINE_STRING( BGl_string3874z00zz__objectz00, BgL_bgl_string3874za700za7za7_4417za7, "object-print1399", 16 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3787z00zz__objectz00, BgL_bgl_za762lambda2110za7624418z00, BGl_z62lambda2110z62zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string3956z00zz__objectz00, BgL_bgl_string3956za700za7za7_4419za7, "&object-print", 13 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3869z00zz__objectz00, BgL_bgl_za762objectza7d2writ4420z00, va_generic_entry, BGl_z62objectzd2write1392zb0zz__objectz00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string3957z00zz__objectz00, BgL_bgl_string3957za700za7za7_4421za7, "object-equal?:Wrong number of arguments", 39 );
DEFINE_STRING( BGl_string3876z00zz__objectz00, BgL_bgl_string3876za700za7za7_4422za7, "object-equal?1402", 17 );
DEFINE_STRING( BGl_string3878z00zz__objectz00, BgL_bgl_string3878za700za7za7_4423za7, "exception-notify1404", 20 );
DEFINE_STRING( BGl_string3797z00zz__objectz00, BgL_bgl_string3797za700za7za7_4424za7, "permission", 10 );
DEFINE_STRING( BGl_string3879z00zz__objectz00, BgL_bgl_string3879za700za7za7_4425za7, "*** UNKNOWN EXCEPTION: ", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2nextzd2virtualzd2setterzd2envz00zz__objectz00, BgL_bgl_za762callza7d2nextza7d4426za7, BGl_z62callzd2nextzd2virtualzd2setterzb0zz__objectz00, 0L, BUNSPEC, 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_nilzf3zd2envz21zz__objectz00, BgL_bgl_za762nilza7f3za791za7za7__4427za7, BGl_z62nilzf3z91zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3871z00zz__objectz00, BgL_bgl_za762objectza7d2hash4428z00, BGl_z62objectzd2hashnumber1396zb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3790z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4429za7, BGl_z62zc3z04anonymousza32139ze3ze5zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3791z00zz__objectz00, BgL_bgl_za762lambda2138za7624430z00, BGl_z62lambda2138z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3960z00zz__objectz00, BgL_bgl_string3960za700za7za7_4431za7, "method1403", 10 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3873z00zz__objectz00, BgL_bgl_za762objectza7d2prin4432z00, BGl_z62objectzd2print1399zb0zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3792z00zz__objectz00, BgL_bgl_za762lambda2137za7624433z00, BGl_z62lambda2137z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3961z00zz__objectz00, BgL_bgl_string3961za700za7za7_4434za7, "&object-equal?", 14 );
DEFINE_STRING( BGl_string3880z00zz__objectz00, BgL_bgl_string3880za700za7za7_4435za7, " [[", 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3793z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4436za7, BGl_z62zc3z04anonymousza32147ze3ze5zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3962z00zz__objectz00, BgL_bgl_string3962za700za7za7_4437za7, "exception-notify:Wrong number of arguments", 42 );
DEFINE_STRING( BGl_string3881z00zz__objectz00, BgL_bgl_string3881za700za7za7_4438za7, "]]", 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3875z00zz__objectz00, BgL_bgl_za762objectza7d2equa4439z00, BGl_z62objectzd2equalzf31402z43zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3794z00zz__objectz00, BgL_bgl_za762lambda2146za7624440z00, BGl_z62lambda2146z62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3795z00zz__objectz00, BgL_bgl_za762lambda2145za7624441z00, BGl_z62lambda2145z62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3877z00zz__objectz00, BgL_bgl_za762exceptionza7d2n4442z00, BGl_z62exceptionzd2notify1404zb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3965z00zz__objectz00, BgL_bgl_string3965za700za7za7_4443za7, "method1408", 10 );
DEFINE_STRING( BGl_string3884z00zz__objectz00, BgL_bgl_string3884za700za7za7_4444za7, "get-value", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52objectzd2wideningzd2setz12zd2envz92zz__objectz00, BgL_bgl_za762za752objectza7d2w4445za7, BGl_z62z52objectzd2wideningzd2setz12z22zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3798z00zz__objectz00, BgL_bgl_za762za7c3za704anonymo4446za7, BGl_z62zc3z04anonymousza32132ze3ze5zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3967z00zz__objectz00, BgL_bgl_string3967za700za7za7_4447za7, "exc", 3 );
DEFINE_STRING( BGl_string3886z00zz__objectz00, BgL_bgl_string3886za700za7za7_4448za7, "obj1", 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3799z00zz__objectz00, BgL_bgl_za762lambda2130za7624449z00, BGl_z62lambda2130z62zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string3968z00zz__objectz00, BgL_bgl_string3968za700za7za7_4450za7, "exception-notify", 16 );
DEFINE_STRING( BGl_string3889z00zz__objectz00, BgL_bgl_string3889za700za7za7_4451za7, "obj2", 4 );
DEFINE_STRING( BGl_string3971z00zz__objectz00, BgL_bgl_string3971za700za7za7_4452za7, "fun2207", 7 );
DEFINE_STRING( BGl_string3890z00zz__objectz00, BgL_bgl_string3890za700za7za7_4453za7, "&object-equal?1402", 18 );
DEFINE_STRING( BGl_string3891z00zz__objectz00, BgL_bgl_string3891za700za7za7_4454za7, "class-field-write/display", 25 );
DEFINE_STRING( BGl_string3892z00zz__objectz00, BgL_bgl_string3892za700za7za7_4455za7, " [", 2 );
DEFINE_STRING( BGl_string3893z00zz__objectz00, BgL_bgl_string3893za700za7za7_4456za7, "class-field-write/display:Wrong number of arguments", 51 );
DEFINE_STRING( BGl_string3975z00zz__objectz00, BgL_bgl_string3975za700za7za7_4457za7, "&exception-notify-&wa1416", 25 );
DEFINE_STRING( BGl_string3976z00zz__objectz00, BgL_bgl_string3976za700za7za7_4458za7, "call-next-method:Wrong number of arguments", 42 );
DEFINE_STRING( BGl_string3897z00zz__objectz00, BgL_bgl_string3897za700za7za7_4459za7, "print-slot", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_isazf3zd2envz21zz__objectz00, BgL_bgl_za762isaza7f3za791za7za7__4460za7, BGl_z62isazf3z91zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3979z00zz__objectz00, BgL_bgl_string3979za700za7za7_4461za7, "next-method1413", 15 );
DEFINE_STRING( BGl_string3899z00zz__objectz00, BgL_bgl_string3899za700za7za7_4462za7, "arg2185", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52isazd2objectzf2finalzf3zd2envz53zz__objectz00, BgL_bgl_za762za752isaza7d2obje4463za7, BGl_z62z52isazd2objectzf2finalzf3ze3zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2allzd2fieldszd2envzd2zz__objectz00, BgL_bgl_za762classza7d2allza7d4464za7, BGl_z62classzd2allzd2fieldsz62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzf3zd2envzf3zz__objectz00, BgL_bgl_za762classza7d2field4465z00, BGl_z62classzd2fieldzf3z43zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2modulezd2envz00zz__objectz00, BgL_bgl_za762classza7d2modul4466z00, BGl_z62classzd2modulezb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_registerzd2classz12zd2envz12zz__objectz00, BgL_bgl_za762registerza7d2cl4467z00, BGl_z62registerzd2classz12za2zz__objectz00, 0L, BUNSPEC, 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2widezf3zd2envzf3zz__objectz00, BgL_bgl_za762classza7d2wideza74468za7, BGl_z62classzd2widezf3z43zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_genericzd2defaultzd2envz00zz__objectz00, BgL_bgl_za762genericza7d2def4469z00, BGl_z62genericzd2defaultzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2evdatazd2setz12zd2envzc0zz__objectz00, BgL_bgl_za762classza7d2evdat4470z00, BGl_z62classzd2evdatazd2setz12z70zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3972z00zz__objectz00, BgL_bgl_za762exceptionza7d2n4471z00, BGl_z62exceptionzd2notifyzd2z62er1412z00zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_objectzd2wideningzd2setz12zd2envzc0zz__objectz00, BgL_bgl_za762objectza7d2wide4472z00, BGl_z62objectzd2wideningzd2setz12z70zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3973z00zz__objectz00, BgL_bgl_za762exceptionza7d2n4473z00, BGl_z62exceptionzd2notifyzd2z62io1414z00zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3980z00zz__objectz00, BgL_bgl_string3980za700za7za7_4474za7, "&exception-notify-&io1414", 25 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3974z00zz__objectz00, BgL_bgl_za762exceptionza7d2n4475z00, BGl_z62exceptionzd2notifyzd2z62wa1416z00zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3981z00zz__objectz00, BgL_bgl_string3981za700za7za7_4476za7, "&exception-notify-&er1412", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2mutablezf3zd2envz21zz__objectz00, BgL_bgl_za762classza7d2field4477z00, BGl_z62classzd2fieldzd2mutablezf3z91zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52isazf2finalzf3zd2envz81zz__objectz00, BgL_bgl_za762za752isaza7f2fina4478za7, BGl_z62z52isazf2finalzf3z31zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2classzd2fieldzd2envzd2zz__objectz00, BgL_bgl_za762findza7d2classza74479za7, BGl_z62findzd2classzd2fieldz62zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2genericzd2bucketzd2maskzd2envz00zz__objectz00, BgL_bgl_za762biglooza7d2gene4480z00, BGl_z62bigloozd2genericzd2bucketzd2maskzb0zz__objectz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2allocatorzd2envz00zz__objectz00, BgL_bgl_za762classza7d2alloc4481z00, BGl_z62classzd2allocatorzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_objectzd2classzd2numzd2setz12zd2envz12zz__objectz00, BgL_bgl_za762objectza7d2clas4482z00, BGl_z62objectzd2classzd2numzd2setz12za2zz__objectz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_callzd2virtualzd2setterzd2envzd2zz__objectz00, BgL_bgl_za762callza7d2virtua4483z00, BGl_z62callzd2virtualzd2setterz62zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2fieldzd2namezd2envzd2zz__objectz00, BgL_bgl_za762classza7d2field4484z00, BGl_z62classzd2fieldzd2namez62zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_methodzd2arrayzd2refzd2envzd2zz__objectz00, BgL_bgl_za762methodza7d2arra4485z00, BGl_z62methodzd2arrayzd2refz62zz__objectz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_objectzd2classzd2envz00zz__objectz00, BgL_bgl_za762objectza7d2clas4486z00, BGl_z62objectzd2classzb0zz__objectz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_classzd2abstractzf3zd2envzf3zz__objectz00, BgL_bgl_za762classza7d2abstr4487z00, BGl_z62classzd2abstractzf3z43zz__objectz00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_z62processzd2exceptionzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3617z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3537z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3619z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3539z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3701z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3540z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3542z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3706z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3625z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3544z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3627z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3546z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3548z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3711z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3633z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3716z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3639z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3801z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2classesza2z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3721z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3805z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3482z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3726z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3484z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3649z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2sigpipezd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3810z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3731z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3650z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3815z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3736z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3655z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2writezd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62typezd2errorzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3900z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3741z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3660z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3746z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3665z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3909z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2parsezd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3913z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3751z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3670z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3915z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3918z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3756z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3675z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2nbzd2classeszd2maxza2z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3922z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3761z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3681z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3925z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3928z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3766z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3688z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2inheritancesza2z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62exceptionz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3931z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3771z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3934z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3692z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3937z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3776z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3697z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3942z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3781z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3783z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3949z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3788z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3954z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3796z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3959z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3964z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3883z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3966z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3885z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3888z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3970z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3896z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3978z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3898z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2classzd2keyza2zd2zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2nbzd2genericsza2zd2zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62securityzd2exceptionzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2closedzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3503z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3506z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62evalzd2warningzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3511z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3516z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3519z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3605z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3608z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3611z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3616z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62warningz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3624z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2inheritancezd2cntza2zd2zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62stackzd2overflowzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2genericsza2z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2portzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3632z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3638z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2connectionzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62accesszd2controlzd2exceptionz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2timeoutzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3481z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2errorzb0zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_objectz00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3912z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62conditionz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3917z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3920z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3921z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3924z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3927z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3930z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3936z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3939z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3940z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3941z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3944z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3948z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3504z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3507z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3509z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3953z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2nbzd2genericszd2maxza2z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3958z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_za2nbzd2classesza2zd2zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3512z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3514z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3517z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3963z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3882z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3887z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3969z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_z62iozd2readzd2errorz62zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3606z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_symbol3609z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3894z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3895z00zz__objectz00) );
ADD_ROOT( (void *)(&BGl_list3977z00zz__objectz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long BgL_checksumz00_7995, char * BgL_fromz00_7996)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__objectz00))
{ 
BGl_requirezd2initializa7ationz75zz__objectz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__objectz00(); 
BGl_cnstzd2initzd2zz__objectz00(); 
BGl_objectzd2initzd2zz__objectz00(); 
BGl_genericzd2initzd2zz__objectz00(); 
BGl_methodzd2initzd2zz__objectz00(); 
return 
BGl_toplevelzd2initzd2zz__objectz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__objectz00(void)
{
{ /* Llib/object.scm 17 */
BGl_symbol3482z00zz__objectz00 = 
bstring_to_symbol(BGl_string3483z00zz__objectz00); 
BGl_symbol3484z00zz__objectz00 = 
bstring_to_symbol(BGl_string3485z00zz__objectz00); 
BGl_list3481z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3484z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3484z00zz__objectz00, BNIL))); 
BGl_symbol3504z00zz__objectz00 = 
bstring_to_symbol(BGl_string3505z00zz__objectz00); 
BGl_list3503z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3504z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3504z00zz__objectz00, BNIL))); 
BGl_symbol3507z00zz__objectz00 = 
bstring_to_symbol(BGl_string3508z00zz__objectz00); 
BGl_symbol3509z00zz__objectz00 = 
bstring_to_symbol(BGl_string3510z00zz__objectz00); 
BGl_list3506z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3507z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3507z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3509z00zz__objectz00, BNIL)))); 
BGl_symbol3512z00zz__objectz00 = 
bstring_to_symbol(BGl_string3513z00zz__objectz00); 
BGl_symbol3514z00zz__objectz00 = 
bstring_to_symbol(BGl_string3515z00zz__objectz00); 
BGl_list3511z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3512z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3512z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3514z00zz__objectz00, BNIL)))); 
BGl_symbol3517z00zz__objectz00 = 
bstring_to_symbol(BGl_string3518z00zz__objectz00); 
BGl_list3516z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3517z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3517z00zz__objectz00, BNIL))); 
BGl_list3519z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3512z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3512z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3509z00zz__objectz00, BNIL)))); 
BGl_symbol3537z00zz__objectz00 = 
bstring_to_symbol(BGl_string3538z00zz__objectz00); 
BGl_symbol3539z00zz__objectz00 = 
bstring_to_symbol(BGl_string3445z00zz__objectz00); 
BGl_symbol3540z00zz__objectz00 = 
bstring_to_symbol(BGl_string3541z00zz__objectz00); 
BGl_symbol3542z00zz__objectz00 = 
bstring_to_symbol(BGl_string3543z00zz__objectz00); 
BGl_symbol3544z00zz__objectz00 = 
bstring_to_symbol(BGl_string3545z00zz__objectz00); 
BGl_symbol3546z00zz__objectz00 = 
bstring_to_symbol(BGl_string3547z00zz__objectz00); 
BGl_symbol3548z00zz__objectz00 = 
bstring_to_symbol(BGl_string3549z00zz__objectz00); 
BGl_symbol3606z00zz__objectz00 = 
bstring_to_symbol(BGl_string3607z00zz__objectz00); 
BGl_list3605z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3606z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3606z00zz__objectz00, BNIL))); 
BGl_symbol3609z00zz__objectz00 = 
bstring_to_symbol(BGl_string3610z00zz__objectz00); 
BGl_list3608z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3609z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3609z00zz__objectz00, BNIL))); 
BGl_list3611z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3606z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3606z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3509z00zz__objectz00, BNIL)))); 
BGl_symbol3617z00zz__objectz00 = 
bstring_to_symbol(BGl_string3618z00zz__objectz00); 
BGl_symbol3619z00zz__objectz00 = 
bstring_to_symbol(BGl_string3620z00zz__objectz00); 
BGl_list3616z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3617z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3617z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, BNIL)))); 
BGl_symbol3625z00zz__objectz00 = 
bstring_to_symbol(BGl_string3626z00zz__objectz00); 
BGl_symbol3627z00zz__objectz00 = 
bstring_to_symbol(BGl_string3628z00zz__objectz00); 
BGl_list3624z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3625z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3625z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3627z00zz__objectz00, BNIL))))); 
BGl_symbol3633z00zz__objectz00 = 
bstring_to_symbol(BGl_string3634z00zz__objectz00); 
BGl_list3632z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3633z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3633z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, BNIL)))); 
BGl_symbol3639z00zz__objectz00 = 
bstring_to_symbol(BGl_string3640z00zz__objectz00); 
BGl_list3638z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3639z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3639z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3627z00zz__objectz00, BNIL))))); 
BGl_symbol3649z00zz__objectz00 = 
bstring_to_symbol(BGl_string3528z00zz__objectz00); 
BGl_symbol3650z00zz__objectz00 = 
bstring_to_symbol(BGl_string3651z00zz__objectz00); 
BGl_symbol3655z00zz__objectz00 = 
bstring_to_symbol(BGl_string3656z00zz__objectz00); 
BGl_symbol3660z00zz__objectz00 = 
bstring_to_symbol(BGl_string3661z00zz__objectz00); 
BGl_symbol3665z00zz__objectz00 = 
bstring_to_symbol(BGl_string3666z00zz__objectz00); 
BGl_symbol3670z00zz__objectz00 = 
bstring_to_symbol(BGl_string3671z00zz__objectz00); 
BGl_symbol3675z00zz__objectz00 = 
bstring_to_symbol(BGl_string3676z00zz__objectz00); 
BGl_symbol3681z00zz__objectz00 = 
bstring_to_symbol(BGl_string3682z00zz__objectz00); 
BGl_symbol3688z00zz__objectz00 = 
bstring_to_symbol(BGl_string3689z00zz__objectz00); 
BGl_symbol3692z00zz__objectz00 = 
bstring_to_symbol(BGl_string3693z00zz__objectz00); 
BGl_symbol3697z00zz__objectz00 = 
bstring_to_symbol(BGl_string3698z00zz__objectz00); 
BGl_symbol3701z00zz__objectz00 = 
bstring_to_symbol(BGl_string3702z00zz__objectz00); 
BGl_symbol3706z00zz__objectz00 = 
bstring_to_symbol(BGl_string3707z00zz__objectz00); 
BGl_symbol3711z00zz__objectz00 = 
bstring_to_symbol(BGl_string3712z00zz__objectz00); 
BGl_symbol3716z00zz__objectz00 = 
bstring_to_symbol(BGl_string3717z00zz__objectz00); 
BGl_symbol3721z00zz__objectz00 = 
bstring_to_symbol(BGl_string3722z00zz__objectz00); 
BGl_symbol3726z00zz__objectz00 = 
bstring_to_symbol(BGl_string3727z00zz__objectz00); 
BGl_symbol3731z00zz__objectz00 = 
bstring_to_symbol(BGl_string3732z00zz__objectz00); 
BGl_symbol3736z00zz__objectz00 = 
bstring_to_symbol(BGl_string3737z00zz__objectz00); 
BGl_symbol3741z00zz__objectz00 = 
bstring_to_symbol(BGl_string3742z00zz__objectz00); 
BGl_symbol3746z00zz__objectz00 = 
bstring_to_symbol(BGl_string3747z00zz__objectz00); 
BGl_symbol3751z00zz__objectz00 = 
bstring_to_symbol(BGl_string3752z00zz__objectz00); 
BGl_symbol3756z00zz__objectz00 = 
bstring_to_symbol(BGl_string3757z00zz__objectz00); 
BGl_symbol3761z00zz__objectz00 = 
bstring_to_symbol(BGl_string3762z00zz__objectz00); 
BGl_symbol3766z00zz__objectz00 = 
bstring_to_symbol(BGl_string3767z00zz__objectz00); 
BGl_symbol3771z00zz__objectz00 = 
bstring_to_symbol(BGl_string3772z00zz__objectz00); 
BGl_symbol3776z00zz__objectz00 = 
bstring_to_symbol(BGl_string3777z00zz__objectz00); 
BGl_symbol3781z00zz__objectz00 = 
bstring_to_symbol(BGl_string3782z00zz__objectz00); 
BGl_symbol3783z00zz__objectz00 = 
bstring_to_symbol(BGl_string3784z00zz__objectz00); 
BGl_symbol3788z00zz__objectz00 = 
bstring_to_symbol(BGl_string3789z00zz__objectz00); 
BGl_symbol3796z00zz__objectz00 = 
bstring_to_symbol(BGl_string3797z00zz__objectz00); 
BGl_symbol3801z00zz__objectz00 = 
bstring_to_symbol(BGl_string3802z00zz__objectz00); 
BGl_symbol3805z00zz__objectz00 = 
bstring_to_symbol(BGl_string3806z00zz__objectz00); 
BGl_symbol3810z00zz__objectz00 = 
bstring_to_symbol(BGl_string3811z00zz__objectz00); 
BGl_symbol3815z00zz__objectz00 = 
bstring_to_symbol(BGl_string3816z00zz__objectz00); 
BGl_symbol3883z00zz__objectz00 = 
bstring_to_symbol(BGl_string3884z00zz__objectz00); 
BGl_symbol3885z00zz__objectz00 = 
bstring_to_symbol(BGl_string3886z00zz__objectz00); 
BGl_list3882z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3883z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3883z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3885z00zz__objectz00, BNIL)))); 
BGl_symbol3888z00zz__objectz00 = 
bstring_to_symbol(BGl_string3889z00zz__objectz00); 
BGl_list3887z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3883z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3883z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3888z00zz__objectz00, BNIL)))); 
BGl_list3894z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3883z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3883z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, BNIL)))); 
BGl_symbol3896z00zz__objectz00 = 
bstring_to_symbol(BGl_string3897z00zz__objectz00); 
BGl_symbol3898z00zz__objectz00 = 
bstring_to_symbol(BGl_string3899z00zz__objectz00); 
BGl_symbol3900z00zz__objectz00 = 
bstring_to_symbol(BGl_string3901z00zz__objectz00); 
BGl_list3895z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3896z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3896z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3898z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3900z00zz__objectz00, BNIL))))); 
BGl_symbol3909z00zz__objectz00 = 
bstring_to_symbol(BGl_string3910z00zz__objectz00); 
BGl_symbol3913z00zz__objectz00 = 
bstring_to_symbol(BGl_string3914z00zz__objectz00); 
BGl_symbol3915z00zz__objectz00 = 
bstring_to_symbol(BGl_string3916z00zz__objectz00); 
BGl_symbol3918z00zz__objectz00 = 
bstring_to_symbol(BGl_string3919z00zz__objectz00); 
BGl_symbol3922z00zz__objectz00 = 
bstring_to_symbol(BGl_string3923z00zz__objectz00); 
BGl_symbol3925z00zz__objectz00 = 
bstring_to_symbol(BGl_string3926z00zz__objectz00); 
BGl_symbol3928z00zz__objectz00 = 
bstring_to_symbol(BGl_string3929z00zz__objectz00); 
BGl_list3927z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3928z00zz__objectz00, 
MAKE_YOUNG_PAIR(BNIL, BNIL)); 
BGl_list3924z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3925z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3900z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3927z00zz__objectz00, BNIL))); 
BGl_list3921z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3922z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3924z00zz__objectz00, BNIL)); 
BGl_list3920z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_list3921z00zz__objectz00, BNIL); 
BGl_symbol3931z00zz__objectz00 = 
bstring_to_symbol(BGl_string3932z00zz__objectz00); 
BGl_list3930z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3931z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3922z00zz__objectz00, BNIL))); 
BGl_list3917z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3918z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3920z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3930z00zz__objectz00, BNIL))); 
BGl_list3912z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3913z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3915z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3917z00zz__objectz00, BNIL))); 
BGl_symbol3934z00zz__objectz00 = 
bstring_to_symbol(BGl_string3935z00zz__objectz00); 
BGl_symbol3937z00zz__objectz00 = 
bstring_to_symbol(BGl_string3938z00zz__objectz00); 
BGl_symbol3942z00zz__objectz00 = 
bstring_to_symbol(BGl_string3943z00zz__objectz00); 
BGl_list3941z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3942z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3924z00zz__objectz00, BNIL)); 
BGl_list3940z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_list3941z00zz__objectz00, BNIL); 
BGl_list3944z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3931z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3942z00zz__objectz00, BNIL))); 
BGl_list3939z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3918z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3940z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3944z00zz__objectz00, BNIL))); 
BGl_list3936z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3913z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3937z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_list3939z00zz__objectz00, BNIL))); 
BGl_symbol3949z00zz__objectz00 = 
bstring_to_symbol(BGl_string3950z00zz__objectz00); 
BGl_list3948z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3949z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3949z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3649z00zz__objectz00, BNIL)))); 
BGl_symbol3954z00zz__objectz00 = 
bstring_to_symbol(BGl_string3955z00zz__objectz00); 
BGl_list3953z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3954z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3954z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3619z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3900z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3896z00zz__objectz00, BNIL)))))); 
BGl_symbol3959z00zz__objectz00 = 
bstring_to_symbol(BGl_string3960z00zz__objectz00); 
BGl_list3958z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3959z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3959z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3885z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3888z00zz__objectz00, BNIL))))); 
BGl_symbol3964z00zz__objectz00 = 
bstring_to_symbol(BGl_string3965z00zz__objectz00); 
BGl_symbol3966z00zz__objectz00 = 
bstring_to_symbol(BGl_string3967z00zz__objectz00); 
BGl_list3963z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3964z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3964z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3966z00zz__objectz00, BNIL)))); 
BGl_symbol3970z00zz__objectz00 = 
bstring_to_symbol(BGl_string3971z00zz__objectz00); 
BGl_list3969z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3970z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3970z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3966z00zz__objectz00, BNIL)))); 
BGl_symbol3978z00zz__objectz00 = 
bstring_to_symbol(BGl_string3979z00zz__objectz00); 
return ( 
BGl_list3977z00zz__objectz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3482z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3978z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3978z00zz__objectz00, 
MAKE_YOUNG_PAIR(BGl_symbol3966z00zz__objectz00, BNIL)))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__objectz00(void)
{
{ /* Llib/object.scm 17 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__objectz00(void)
{
{ /* Llib/object.scm 17 */BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; BUNSPEC; 
return BUNSPEC;} 

}



/* bigloo-generic-bucket-pow */
BGL_EXPORTED_DEF int BGl_bigloozd2genericzd2bucketzd2powzd2zz__objectz00(void)
{
{ /* Llib/object.scm 426 */
return 
(int)(4L);} 

}



/* &bigloo-generic-bucket-pow */
obj_t BGl_z62bigloozd2genericzd2bucketzd2powzb0zz__objectz00(obj_t BgL_envz00_4755)
{
{ /* Llib/object.scm 426 */
return 
BINT(
BGl_bigloozd2genericzd2bucketzd2powzd2zz__objectz00());} 

}



/* bigloo-generic-bucket-size */
BGL_EXPORTED_DEF int BGl_bigloozd2genericzd2bucketzd2siza7ez75zz__objectz00(void)
{
{ /* Llib/object.scm 428 */
return 
(int)(
(1L << 
(int)(
(long)(
(int)(4L)))));} 

}



/* &bigloo-generic-bucket-size */
obj_t BGl_z62bigloozd2genericzd2bucketzd2siza7ez17zz__objectz00(obj_t BgL_envz00_4756)
{
{ /* Llib/object.scm 428 */
return 
BINT(
BGl_bigloozd2genericzd2bucketzd2siza7ez75zz__objectz00());} 

}



/* bigloo-generic-bucket-mask */
BGL_EXPORTED_DEF int BGl_bigloozd2genericzd2bucketzd2maskzd2zz__objectz00(void)
{
{ /* Llib/object.scm 430 */
return 
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L));} 

}



/* &bigloo-generic-bucket-mask */
obj_t BGl_z62bigloozd2genericzd2bucketzd2maskzb0zz__objectz00(obj_t BgL_envz00_4757)
{
{ /* Llib/object.scm 430 */
return 
BINT(
BGl_bigloozd2genericzd2bucketzd2maskzd2zz__objectz00());} 

}



/* bigloo-types-number */
BGL_EXPORTED_DEF long bgl_types_number(void)
{
{ /* Llib/object.scm 440 */
if(
INTEGERP(BGl_za2nbzd2classesza2zd2zz__objectz00))
{ /* Llib/object.scm 441 */
return 
(OBJECT_TYPE+
(long)CINT(BGl_za2nbzd2classesza2zd2zz__objectz00));}  else 
{ /* Llib/object.scm 441 */
return OBJECT_TYPE;} } 

}



/* &bigloo-types-number */
obj_t BGl_z62bigloozd2typeszd2numberz62zz__objectz00(obj_t BgL_envz00_4758)
{
{ /* Llib/object.scm 440 */
return 
BINT(
bgl_types_number());} 

}



/* class? */
BGL_EXPORTED_DEF bool_t BGl_classzf3zf3zz__objectz00(obj_t BgL_objz00_20)
{
{ /* Llib/object.scm 468 */
return 
BGL_CLASSP(BgL_objz00_20);} 

}



/* &class? */
obj_t BGl_z62classzf3z91zz__objectz00(obj_t BgL_envz00_4759, obj_t BgL_objz00_4760)
{
{ /* Llib/object.scm 468 */
return 
BBOOL(
BGl_classzf3zf3zz__objectz00(BgL_objz00_4760));} 

}



/* class-exists */
BGL_EXPORTED_DEF obj_t BGl_classzd2existszd2zz__objectz00(obj_t BgL_cnamez00_21)
{
{ /* Llib/object.scm 474 */
{ 
 long BgL_iz00_1285;
BgL_iz00_1285 = 0L; 
BgL_zc3z04anonymousza31489ze3z87_1286:
{ /* Llib/object.scm 476 */
 bool_t BgL_test4490z00_8240;
{ /* Llib/object.scm 476 */
 long BgL_n2z00_2990;
{ /* Llib/object.scm 476 */
 obj_t BgL_tmpz00_8241;
{ /* Llib/object.scm 476 */
 obj_t BgL_aux2723z00_5457;
BgL_aux2723z00_5457 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2723z00_5457))
{ /* Llib/object.scm 476 */
BgL_tmpz00_8241 = BgL_aux2723z00_5457
; }  else 
{ 
 obj_t BgL_auxz00_8244;
BgL_auxz00_8244 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(19704L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2723z00_5457); 
FAILURE(BgL_auxz00_8244,BFALSE,BFALSE);} } 
BgL_n2z00_2990 = 
(long)CINT(BgL_tmpz00_8241); } 
BgL_test4490z00_8240 = 
(BgL_iz00_1285==BgL_n2z00_2990); } 
if(BgL_test4490z00_8240)
{ /* Llib/object.scm 476 */
return BFALSE;}  else 
{ /* Llib/object.scm 477 */
 obj_t BgL_claz00_1288;
{ /* Llib/object.scm 477 */
 obj_t BgL_vectorz00_2991;
{ /* Llib/object.scm 477 */
 obj_t BgL_aux2724z00_5458;
BgL_aux2724z00_5458 = BGl_za2classesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2724z00_5458))
{ /* Llib/object.scm 477 */
BgL_vectorz00_2991 = BgL_aux2724z00_5458; }  else 
{ 
 obj_t BgL_auxz00_8252;
BgL_auxz00_8252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(19746L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2724z00_5458); 
FAILURE(BgL_auxz00_8252,BFALSE,BFALSE);} } 
BgL_claz00_1288 = 
VECTOR_REF(BgL_vectorz00_2991,BgL_iz00_1285); } 
{ /* Llib/object.scm 478 */
 bool_t BgL_test4493z00_8257;
{ /* Llib/object.scm 478 */
 obj_t BgL_arg1495z00_1292;
{ /* Llib/object.scm 478 */
 obj_t BgL_classz00_2993;
if(
BGL_CLASSP(BgL_claz00_1288))
{ /* Llib/object.scm 478 */
BgL_classz00_2993 = BgL_claz00_1288; }  else 
{ 
 obj_t BgL_auxz00_8260;
BgL_auxz00_8260 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(19787L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_claz00_1288); 
FAILURE(BgL_auxz00_8260,BFALSE,BFALSE);} 
BgL_arg1495z00_1292 = 
BGL_CLASS_NAME(BgL_classz00_2993); } 
BgL_test4493z00_8257 = 
(BgL_arg1495z00_1292==BgL_cnamez00_21); } 
if(BgL_test4493z00_8257)
{ /* Llib/object.scm 478 */
return BgL_claz00_1288;}  else 
{ 
 long BgL_iz00_8266;
BgL_iz00_8266 = 
(BgL_iz00_1285+1L); 
BgL_iz00_1285 = BgL_iz00_8266; 
goto BgL_zc3z04anonymousza31489ze3z87_1286;} } } } } } 

}



/* &class-exists */
obj_t BGl_z62classzd2existszb0zz__objectz00(obj_t BgL_envz00_4761, obj_t BgL_cnamez00_4762)
{
{ /* Llib/object.scm 474 */
{ /* Llib/object.scm 476 */
 obj_t BgL_auxz00_8268;
if(
SYMBOLP(BgL_cnamez00_4762))
{ /* Llib/object.scm 476 */
BgL_auxz00_8268 = BgL_cnamez00_4762
; }  else 
{ 
 obj_t BgL_auxz00_8271;
BgL_auxz00_8271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(19689L), BGl_string3446z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_cnamez00_4762); 
FAILURE(BgL_auxz00_8271,BFALSE,BFALSE);} 
return 
BGl_classzd2existszd2zz__objectz00(BgL_auxz00_8268);} } 

}



/* find-class */
BGL_EXPORTED_DEF obj_t BGl_findzd2classzd2zz__objectz00(obj_t BgL_cnamez00_22)
{
{ /* Llib/object.scm 485 */
{ /* Llib/object.scm 486 */
 obj_t BgL__ortest_1213z00_2995;
BgL__ortest_1213z00_2995 = 
BGl_classzd2existszd2zz__objectz00(BgL_cnamez00_22); 
if(
CBOOL(BgL__ortest_1213z00_2995))
{ /* Llib/object.scm 486 */
if(
BGL_CLASSP(BgL__ortest_1213z00_2995))
{ /* Llib/object.scm 486 */
return BgL__ortest_1213z00_2995;}  else 
{ 
 obj_t BgL_auxz00_8281;
BgL_auxz00_8281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20086L), BGl_string3448z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL__ortest_1213z00_2995); 
FAILURE(BgL_auxz00_8281,BFALSE,BFALSE);} }  else 
{ /* Llib/object.scm 487 */
 obj_t BgL_aux2732z00_5466;
BgL_aux2732z00_5466 = 
BGl_errorz00zz__errorz00(BGl_string3448z00zz__objectz00, BGl_string3449z00zz__objectz00, BgL_cnamez00_22); 
if(
BGL_CLASSP(BgL_aux2732z00_5466))
{ /* Llib/object.scm 487 */
return BgL_aux2732z00_5466;}  else 
{ 
 obj_t BgL_auxz00_8288;
BgL_auxz00_8288 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20115L), BGl_string3448z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_aux2732z00_5466); 
FAILURE(BgL_auxz00_8288,BFALSE,BFALSE);} } } } 

}



/* &find-class */
obj_t BGl_z62findzd2classzb0zz__objectz00(obj_t BgL_envz00_4763, obj_t BgL_cnamez00_4764)
{
{ /* Llib/object.scm 485 */
{ /* Llib/object.scm 486 */
 obj_t BgL_auxz00_8292;
if(
SYMBOLP(BgL_cnamez00_4764))
{ /* Llib/object.scm 486 */
BgL_auxz00_8292 = BgL_cnamez00_4764
; }  else 
{ 
 obj_t BgL_auxz00_8295;
BgL_auxz00_8295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20086L), BGl_string3450z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_cnamez00_4764); 
FAILURE(BgL_auxz00_8295,BFALSE,BFALSE);} 
return 
BGl_findzd2classzd2zz__objectz00(BgL_auxz00_8292);} } 

}



/* find-class-by-hash */
BGL_EXPORTED_DEF obj_t BGl_findzd2classzd2byzd2hashzd2zz__objectz00(int BgL_hashz00_23)
{
{ /* Llib/object.scm 492 */
{ 
 long BgL_iz00_1296;
BgL_iz00_1296 = 0L; 
BgL_zc3z04anonymousza31496ze3z87_1297:
{ /* Llib/object.scm 494 */
 bool_t BgL_test4500z00_8300;
{ /* Llib/object.scm 494 */
 long BgL_n2z00_2997;
{ /* Llib/object.scm 494 */
 obj_t BgL_tmpz00_8301;
{ /* Llib/object.scm 494 */
 obj_t BgL_aux2736z00_5470;
BgL_aux2736z00_5470 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2736z00_5470))
{ /* Llib/object.scm 494 */
BgL_tmpz00_8301 = BgL_aux2736z00_5470
; }  else 
{ 
 obj_t BgL_auxz00_8304;
BgL_auxz00_8304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20463L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2736z00_5470); 
FAILURE(BgL_auxz00_8304,BFALSE,BFALSE);} } 
BgL_n2z00_2997 = 
(long)CINT(BgL_tmpz00_8301); } 
BgL_test4500z00_8300 = 
(BgL_iz00_1296==BgL_n2z00_2997); } 
if(BgL_test4500z00_8300)
{ /* Llib/object.scm 494 */
return BFALSE;}  else 
{ /* Llib/object.scm 495 */
 obj_t BgL_claz00_1299;
{ /* Llib/object.scm 495 */
 obj_t BgL_vectorz00_2998;
{ /* Llib/object.scm 495 */
 obj_t BgL_aux2737z00_5471;
BgL_aux2737z00_5471 = BGl_za2classesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2737z00_5471))
{ /* Llib/object.scm 495 */
BgL_vectorz00_2998 = BgL_aux2737z00_5471; }  else 
{ 
 obj_t BgL_auxz00_8312;
BgL_auxz00_8312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20505L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2737z00_5471); 
FAILURE(BgL_auxz00_8312,BFALSE,BFALSE);} } 
BgL_claz00_1299 = 
VECTOR_REF(BgL_vectorz00_2998,BgL_iz00_1296); } 
{ /* Llib/object.scm 496 */
 bool_t BgL_test4503z00_8317;
{ /* Llib/object.scm 496 */
 long BgL_arg1501z00_1303;
{ /* Llib/object.scm 496 */
 obj_t BgL_classz00_3000;
if(
BGL_CLASSP(BgL_claz00_1299))
{ /* Llib/object.scm 496 */
BgL_classz00_3000 = BgL_claz00_1299; }  else 
{ 
 obj_t BgL_auxz00_8320;
BgL_auxz00_8320 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20546L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_claz00_1299); 
FAILURE(BgL_auxz00_8320,BFALSE,BFALSE);} 
BgL_arg1501z00_1303 = 
BGL_CLASS_HASH(BgL_classz00_3000); } 
BgL_test4503z00_8317 = 
(
BINT(BgL_arg1501z00_1303)==
BINT(BgL_hashz00_23)); } 
if(BgL_test4503z00_8317)
{ /* Llib/object.scm 496 */
return BgL_claz00_1299;}  else 
{ 
 long BgL_iz00_8328;
BgL_iz00_8328 = 
(BgL_iz00_1296+1L); 
BgL_iz00_1296 = BgL_iz00_8328; 
goto BgL_zc3z04anonymousza31496ze3z87_1297;} } } } } } 

}



/* &find-class-by-hash */
obj_t BGl_z62findzd2classzd2byzd2hashzb0zz__objectz00(obj_t BgL_envz00_4765, obj_t BgL_hashz00_4766)
{
{ /* Llib/object.scm 492 */
{ /* Llib/object.scm 494 */
 int BgL_auxz00_8330;
{ /* Llib/object.scm 494 */
 obj_t BgL_tmpz00_8331;
if(
INTEGERP(BgL_hashz00_4766))
{ /* Llib/object.scm 494 */
BgL_tmpz00_8331 = BgL_hashz00_4766
; }  else 
{ 
 obj_t BgL_auxz00_8334;
BgL_auxz00_8334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20448L), BGl_string3451z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_hashz00_4766); 
FAILURE(BgL_auxz00_8334,BFALSE,BFALSE);} 
BgL_auxz00_8330 = 
CINT(BgL_tmpz00_8331); } 
return 
BGl_findzd2classzd2byzd2hashzd2zz__objectz00(BgL_auxz00_8330);} } 

}



/* eval-class? */
BGL_EXPORTED_DEF bool_t BGl_evalzd2classzf3z21zz__objectz00(obj_t BgL_objz00_24)
{
{ /* Llib/object.scm 503 */
if(
BGL_CLASSP(BgL_objz00_24))
{ /* Llib/object.scm 504 */
 obj_t BgL_classz00_3003;
if(
BGL_CLASSP(BgL_objz00_24))
{ /* Llib/object.scm 504 */
BgL_classz00_3003 = BgL_objz00_24; }  else 
{ 
 obj_t BgL_auxz00_8344;
BgL_auxz00_8344 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(20871L), BGl_string3452z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_objz00_24); 
FAILURE(BgL_auxz00_8344,BFALSE,BFALSE);} 
return 
CBOOL(
BGL_CLASS_EVDATA(BgL_classz00_3003));}  else 
{ /* Llib/object.scm 504 */
return ((bool_t)0);} } 

}



/* &eval-class? */
obj_t BGl_z62evalzd2classzf3z43zz__objectz00(obj_t BgL_envz00_4767, obj_t BgL_objz00_4768)
{
{ /* Llib/object.scm 503 */
return 
BBOOL(
BGl_evalzd2classzf3z21zz__objectz00(BgL_objz00_4768));} 

}



/* class-name */
BGL_EXPORTED_DEF obj_t BGl_classzd2namezd2zz__objectz00(obj_t BgL_classz00_25)
{
{ /* Llib/object.scm 509 */
return 
BGL_CLASS_NAME(BgL_classz00_25);} 

}



/* &class-name */
obj_t BGl_z62classzd2namezb0zz__objectz00(obj_t BgL_envz00_4769, obj_t BgL_classz00_4770)
{
{ /* Llib/object.scm 509 */
{ /* Llib/object.scm 510 */
 obj_t BgL_auxz00_8353;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4770))
{ /* Llib/object.scm 510 */
BgL_auxz00_8353 = BgL_classz00_4770
; }  else 
{ 
 obj_t BgL_auxz00_8356;
BgL_auxz00_8356 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(21131L), BGl_string3453z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4770); 
FAILURE(BgL_auxz00_8356,BFALSE,BFALSE);} 
return 
BGl_classzd2namezd2zz__objectz00(BgL_auxz00_8353);} } 

}



/* class-module */
BGL_EXPORTED_DEF obj_t BGl_classzd2modulezd2zz__objectz00(obj_t BgL_classz00_26)
{
{ /* Llib/object.scm 515 */
return 
BGL_CLASS_MODULE(BgL_classz00_26);} 

}



/* &class-module */
obj_t BGl_z62classzd2modulezb0zz__objectz00(obj_t BgL_envz00_4771, obj_t BgL_classz00_4772)
{
{ /* Llib/object.scm 515 */
{ /* Llib/object.scm 516 */
 obj_t BgL_auxz00_8362;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4772))
{ /* Llib/object.scm 516 */
BgL_auxz00_8362 = BgL_classz00_4772
; }  else 
{ 
 obj_t BgL_auxz00_8365;
BgL_auxz00_8365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(21407L), BGl_string3454z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4772); 
FAILURE(BgL_auxz00_8365,BFALSE,BFALSE);} 
return 
BGl_classzd2modulezd2zz__objectz00(BgL_auxz00_8362);} } 

}



/* class-index */
BGL_EXPORTED_DEF long BGl_classzd2indexzd2zz__objectz00(obj_t BgL_classz00_27)
{
{ /* Llib/object.scm 521 */
return 
BGL_CLASS_INDEX(BgL_classz00_27);} 

}



/* &class-index */
obj_t BGl_z62classzd2indexzb0zz__objectz00(obj_t BgL_envz00_4773, obj_t BgL_classz00_4774)
{
{ /* Llib/object.scm 521 */
{ /* Llib/object.scm 522 */
 long BgL_tmpz00_8371;
{ /* Llib/object.scm 522 */
 obj_t BgL_auxz00_8372;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4774))
{ /* Llib/object.scm 522 */
BgL_auxz00_8372 = BgL_classz00_4774
; }  else 
{ 
 obj_t BgL_auxz00_8375;
BgL_auxz00_8375 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(21691L), BGl_string3455z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4774); 
FAILURE(BgL_auxz00_8375,BFALSE,BFALSE);} 
BgL_tmpz00_8371 = 
BGl_classzd2indexzd2zz__objectz00(BgL_auxz00_8372); } 
return 
BINT(BgL_tmpz00_8371);} } 

}



/* class-num */
BGL_EXPORTED_DEF long BGl_classzd2numzd2zz__objectz00(obj_t BgL_classz00_28)
{
{ /* Llib/object.scm 527 */
return 
BGL_CLASS_NUM(BgL_classz00_28);} 

}



/* &class-num */
obj_t BGl_z62classzd2numzb0zz__objectz00(obj_t BgL_envz00_4775, obj_t BgL_classz00_4776)
{
{ /* Llib/object.scm 527 */
{ /* Llib/object.scm 528 */
 long BgL_tmpz00_8382;
{ /* Llib/object.scm 528 */
 obj_t BgL_auxz00_8383;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4776))
{ /* Llib/object.scm 528 */
BgL_auxz00_8383 = BgL_classz00_4776
; }  else 
{ 
 obj_t BgL_auxz00_8386;
BgL_auxz00_8386 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(21972L), BGl_string3456z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4776); 
FAILURE(BgL_auxz00_8386,BFALSE,BFALSE);} 
BgL_tmpz00_8382 = 
BGl_classzd2numzd2zz__objectz00(BgL_auxz00_8383); } 
return 
BINT(BgL_tmpz00_8382);} } 

}



/* class-virtual */
BGL_EXPORTED_DEF obj_t BGl_classzd2virtualzd2zz__objectz00(obj_t BgL_classz00_29)
{
{ /* Llib/object.scm 533 */
return 
BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_29);} 

}



/* &class-virtual */
obj_t BGl_z62classzd2virtualzb0zz__objectz00(obj_t BgL_envz00_4777, obj_t BgL_classz00_4778)
{
{ /* Llib/object.scm 533 */
{ /* Llib/object.scm 534 */
 obj_t BgL_auxz00_8393;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4778))
{ /* Llib/object.scm 534 */
BgL_auxz00_8393 = BgL_classz00_4778
; }  else 
{ 
 obj_t BgL_auxz00_8396;
BgL_auxz00_8396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(22255L), BGl_string3457z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4778); 
FAILURE(BgL_auxz00_8396,BFALSE,BFALSE);} 
return 
BGl_classzd2virtualzd2zz__objectz00(BgL_auxz00_8393);} } 

}



/* class-evdata */
BGL_EXPORTED_DEF obj_t BGl_classzd2evdatazd2zz__objectz00(obj_t BgL_classz00_30)
{
{ /* Llib/object.scm 539 */
return 
BGL_CLASS_EVDATA(BgL_classz00_30);} 

}



/* &class-evdata */
obj_t BGl_z62classzd2evdatazb0zz__objectz00(obj_t BgL_envz00_4779, obj_t BgL_classz00_4780)
{
{ /* Llib/object.scm 539 */
{ /* Llib/object.scm 540 */
 obj_t BgL_auxz00_8402;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4780))
{ /* Llib/object.scm 540 */
BgL_auxz00_8402 = BgL_classz00_4780
; }  else 
{ 
 obj_t BgL_auxz00_8405;
BgL_auxz00_8405 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(22541L), BGl_string3458z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4780); 
FAILURE(BgL_auxz00_8405,BFALSE,BFALSE);} 
return 
BGl_classzd2evdatazd2zz__objectz00(BgL_auxz00_8402);} } 

}



/* class-evdata-set! */
BGL_EXPORTED_DEF obj_t BGl_classzd2evdatazd2setz12z12zz__objectz00(obj_t BgL_classz00_31, obj_t BgL_dataz00_32)
{
{ /* Llib/object.scm 545 */
return 
BGL_CLASS_EVDATA_SET(BgL_classz00_31, BgL_dataz00_32);} 

}



/* &class-evdata-set! */
obj_t BGl_z62classzd2evdatazd2setz12z70zz__objectz00(obj_t BgL_envz00_4781, obj_t BgL_classz00_4782, obj_t BgL_dataz00_4783)
{
{ /* Llib/object.scm 545 */
{ /* Llib/object.scm 546 */
 obj_t BgL_auxz00_8411;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4782))
{ /* Llib/object.scm 546 */
BgL_auxz00_8411 = BgL_classz00_4782
; }  else 
{ 
 obj_t BgL_auxz00_8414;
BgL_auxz00_8414 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(22829L), BGl_string3459z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4782); 
FAILURE(BgL_auxz00_8414,BFALSE,BFALSE);} 
return 
BGl_classzd2evdatazd2setz12z12zz__objectz00(BgL_auxz00_8411, BgL_dataz00_4783);} } 

}



/* class-evfields-set! */
BGL_EXPORTED_DEF obj_t BGl_classzd2evfieldszd2setz12z12zz__objectz00(obj_t BgL_classz00_33, obj_t BgL_fieldsz00_34)
{
{ /* Llib/object.scm 551 */
{ /* Llib/object.scm 553 */
 bool_t BgL_test4515z00_8419;
{ /* Llib/object.scm 553 */
 bool_t BgL_res2553z00_3006;
if(
BGL_CLASSP(BgL_classz00_33))
{ /* Llib/object.scm 504 */
BgL_res2553z00_3006 = 
CBOOL(
BGL_CLASS_EVDATA(BgL_classz00_33)); }  else 
{ /* Llib/object.scm 504 */
BgL_res2553z00_3006 = ((bool_t)0); } 
BgL_test4515z00_8419 = BgL_res2553z00_3006; } 
if(BgL_test4515z00_8419)
{ /* Llib/object.scm 555 */
 bool_t BgL_test4517z00_8424;
{ /* Llib/object.scm 555 */
 long BgL_arg1509z00_1314;
{ /* Llib/object.scm 555 */
 obj_t BgL_arg1510z00_1315;
BgL_arg1510z00_1315 = 
BGL_CLASS_DIRECT_FIELDS(BgL_classz00_33); 
BgL_arg1509z00_1314 = 
VECTOR_LENGTH(BgL_arg1510z00_1315); } 
BgL_test4517z00_8424 = 
(BgL_arg1509z00_1314>0L); } 
if(BgL_test4517z00_8424)
{ /* Llib/object.scm 555 */
return 
BGl_errorz00zz__errorz00(BGl_string3460z00zz__objectz00, BGl_string3461z00zz__objectz00, BgL_classz00_33);}  else 
{ /* Llib/object.scm 558 */
 obj_t BgL_sfieldsz00_1310;
{ /* Llib/object.scm 558 */
 obj_t BgL_arg1508z00_1313;
BgL_arg1508z00_1313 = 
BGL_CLASS_SUPER(BgL_classz00_33); 
{ /* Llib/object.scm 558 */
 obj_t BgL_classz00_3011;
if(
BGL_CLASSP(BgL_arg1508z00_1313))
{ /* Llib/object.scm 558 */
BgL_classz00_3011 = BgL_arg1508z00_1313; }  else 
{ 
 obj_t BgL_auxz00_8432;
BgL_auxz00_8432 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(23421L), BGl_string3460z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_arg1508z00_1313); 
FAILURE(BgL_auxz00_8432,BFALSE,BFALSE);} 
BgL_sfieldsz00_1310 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3011); } } 
BGL_CLASS_DIRECT_FIELDS_SET(BgL_classz00_33, BgL_fieldsz00_34); 
{ /* Llib/object.scm 560 */
 obj_t BgL_arg1506z00_1311;
{ /* Llib/object.scm 560 */
 obj_t BgL_list1507z00_1312;
BgL_list1507z00_1312 = 
MAKE_YOUNG_PAIR(BgL_fieldsz00_34, BNIL); 
BgL_arg1506z00_1311 = 
BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(BgL_sfieldsz00_1310, BgL_list1507z00_1312); } 
return 
BGL_CLASS_ALL_FIELDS_SET(BgL_classz00_33, BgL_arg1506z00_1311);} } }  else 
{ /* Llib/object.scm 553 */
return 
BGl_errorz00zz__errorz00(BGl_string3460z00zz__objectz00, BGl_string3462z00zz__objectz00, BgL_classz00_33);} } } 

}



/* &class-evfields-set! */
obj_t BGl_z62classzd2evfieldszd2setz12z70zz__objectz00(obj_t BgL_envz00_4784, obj_t BgL_classz00_4785, obj_t BgL_fieldsz00_4786)
{
{ /* Llib/object.scm 551 */
{ /* Llib/object.scm 553 */
 obj_t BgL_auxz00_8449; obj_t BgL_auxz00_8442;
if(
VECTORP(BgL_fieldsz00_4786))
{ /* Llib/object.scm 553 */
BgL_auxz00_8449 = BgL_fieldsz00_4786
; }  else 
{ 
 obj_t BgL_auxz00_8452;
BgL_auxz00_8452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(23143L), BGl_string3463z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_fieldsz00_4786); 
FAILURE(BgL_auxz00_8452,BFALSE,BFALSE);} 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4785))
{ /* Llib/object.scm 553 */
BgL_auxz00_8442 = BgL_classz00_4785
; }  else 
{ 
 obj_t BgL_auxz00_8445;
BgL_auxz00_8445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(23143L), BGl_string3463z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4785); 
FAILURE(BgL_auxz00_8445,BFALSE,BFALSE);} 
return 
BGl_classzd2evfieldszd2setz12z12zz__objectz00(BgL_auxz00_8442, BgL_auxz00_8449);} } 

}



/* class-fields */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldszd2zz__objectz00(obj_t BgL_classz00_35)
{
{ /* Llib/object.scm 565 */
return 
BGL_CLASS_DIRECT_FIELDS(BgL_classz00_35);} 

}



/* &class-fields */
obj_t BGl_z62classzd2fieldszb0zz__objectz00(obj_t BgL_envz00_4787, obj_t BgL_classz00_4788)
{
{ /* Llib/object.scm 565 */
{ /* Llib/object.scm 566 */
 obj_t BgL_auxz00_8458;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4788))
{ /* Llib/object.scm 566 */
BgL_auxz00_8458 = BgL_classz00_4788
; }  else 
{ 
 obj_t BgL_auxz00_8461;
BgL_auxz00_8461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(23794L), BGl_string3464z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4788); 
FAILURE(BgL_auxz00_8461,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldszd2zz__objectz00(BgL_auxz00_8458);} } 

}



/* class-all-fields */
BGL_EXPORTED_DEF obj_t BGl_classzd2allzd2fieldsz00zz__objectz00(obj_t BgL_classz00_36)
{
{ /* Llib/object.scm 571 */
return 
BGL_CLASS_ALL_FIELDS(BgL_classz00_36);} 

}



/* &class-all-fields */
obj_t BGl_z62classzd2allzd2fieldsz62zz__objectz00(obj_t BgL_envz00_4789, obj_t BgL_classz00_4790)
{
{ /* Llib/object.scm 571 */
{ /* Llib/object.scm 572 */
 obj_t BgL_auxz00_8467;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4790))
{ /* Llib/object.scm 572 */
BgL_auxz00_8467 = BgL_classz00_4790
; }  else 
{ 
 obj_t BgL_auxz00_8470;
BgL_auxz00_8470 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(24090L), BGl_string3465z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4790); 
FAILURE(BgL_auxz00_8470,BFALSE,BFALSE);} 
return 
BGl_classzd2allzd2fieldsz00zz__objectz00(BgL_auxz00_8467);} } 

}



/* find-class-field */
BGL_EXPORTED_DEF obj_t BGl_findzd2classzd2fieldz00zz__objectz00(obj_t BgL_classz00_37, obj_t BgL_namez00_38)
{
{ /* Llib/object.scm 577 */
{ /* Llib/object.scm 578 */
 obj_t BgL_fieldsz00_1316;
BgL_fieldsz00_1316 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_37); 
{ /* Llib/object.scm 579 */
 long BgL_g1215z00_1317;
BgL_g1215z00_1317 = 
(
VECTOR_LENGTH(BgL_fieldsz00_1316)-1L); 
{ 
 long BgL_iz00_1319;
BgL_iz00_1319 = BgL_g1215z00_1317; 
BgL_zc3z04anonymousza31511ze3z87_1320:
if(
(BgL_iz00_1319==-1L))
{ /* Llib/object.scm 580 */
return BFALSE;}  else 
{ /* Llib/object.scm 582 */
 obj_t BgL_fz00_1322;
BgL_fz00_1322 = 
VECTOR_REF(BgL_fieldsz00_1316,BgL_iz00_1319); 
{ /* Llib/object.scm 583 */
 bool_t BgL_test4524z00_8481;
{ /* Llib/object.scm 583 */
 obj_t BgL_arg1517z00_1326;
{ /* Llib/object.scm 583 */
 obj_t BgL_res2554z00_3020;
{ /* Llib/object.scm 583 */
 obj_t BgL_fieldz00_3018;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fz00_1322))
{ /* Llib/object.scm 583 */
BgL_fieldz00_3018 = BgL_fz00_1322; }  else 
{ 
 obj_t BgL_auxz00_8484;
BgL_auxz00_8484 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(24580L), BGl_string3442z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fz00_1322); 
FAILURE(BgL_auxz00_8484,BFALSE,BFALSE);} 
{ /* Llib/object.scm 606 */
 obj_t BgL_vectorz00_3019;
BgL_vectorz00_3019 = BgL_fieldz00_3018; 
{ /* Llib/object.scm 606 */
 obj_t BgL_aux2770z00_5504;
BgL_aux2770z00_5504 = 
VECTOR_REF(BgL_vectorz00_3019,0L); 
if(
SYMBOLP(BgL_aux2770z00_5504))
{ /* Llib/object.scm 606 */
BgL_res2554z00_3020 = BgL_aux2770z00_5504; }  else 
{ 
 obj_t BgL_auxz00_8491;
BgL_auxz00_8491 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(25638L), BGl_string3442z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_aux2770z00_5504); 
FAILURE(BgL_auxz00_8491,BFALSE,BFALSE);} } } } 
BgL_arg1517z00_1326 = BgL_res2554z00_3020; } 
BgL_test4524z00_8481 = 
(BgL_arg1517z00_1326==BgL_namez00_38); } 
if(BgL_test4524z00_8481)
{ /* Llib/object.scm 583 */
return BgL_fz00_1322;}  else 
{ 
 long BgL_iz00_8496;
BgL_iz00_8496 = 
(BgL_iz00_1319-1L); 
BgL_iz00_1319 = BgL_iz00_8496; 
goto BgL_zc3z04anonymousza31511ze3z87_1320;} } } } } } } 

}



/* &find-class-field */
obj_t BGl_z62findzd2classzd2fieldz62zz__objectz00(obj_t BgL_envz00_4791, obj_t BgL_classz00_4792, obj_t BgL_namez00_4793)
{
{ /* Llib/object.scm 577 */
{ /* Llib/object.scm 578 */
 obj_t BgL_auxz00_8505; obj_t BgL_auxz00_8498;
if(
SYMBOLP(BgL_namez00_4793))
{ /* Llib/object.scm 578 */
BgL_auxz00_8505 = BgL_namez00_4793
; }  else 
{ 
 obj_t BgL_auxz00_8508;
BgL_auxz00_8508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(24389L), BGl_string3467z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_namez00_4793); 
FAILURE(BgL_auxz00_8508,BFALSE,BFALSE);} 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4792))
{ /* Llib/object.scm 578 */
BgL_auxz00_8498 = BgL_classz00_4792
; }  else 
{ 
 obj_t BgL_auxz00_8501;
BgL_auxz00_8501 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(24389L), BGl_string3467z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4792); 
FAILURE(BgL_auxz00_8501,BFALSE,BFALSE);} 
return 
BGl_findzd2classzd2fieldz00zz__objectz00(BgL_auxz00_8498, BgL_auxz00_8505);} } 

}



/* make-class-field */
BGL_EXPORTED_DEF obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t BgL_namez00_39, obj_t BgL_getterz00_40, obj_t BgL_setterz00_41, bool_t BgL_ronlyz00_42, bool_t BgL_virtualz00_43, obj_t BgL_infoz00_44, obj_t BgL_defaultz00_45, obj_t BgL_typez00_46)
{
{ /* Llib/object.scm 590 */
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3022;
BgL_v1316z00_3022 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3022,0L,BgL_namez00_39); 
VECTOR_SET(BgL_v1316z00_3022,1L,BgL_getterz00_40); 
VECTOR_SET(BgL_v1316z00_3022,2L,BgL_setterz00_41); 
VECTOR_SET(BgL_v1316z00_3022,3L,
BBOOL(BgL_virtualz00_43)); 
VECTOR_SET(BgL_v1316z00_3022,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3022,5L,BgL_infoz00_44); 
VECTOR_SET(BgL_v1316z00_3022,6L,BgL_defaultz00_45); 
VECTOR_SET(BgL_v1316z00_3022,7L,BgL_typez00_46); 
{ 
 obj_t BgL_auxz00_8523;
{ /* Llib/object.scm 591 */
 bool_t BgL_tmpz00_8524;
if(BgL_ronlyz00_42)
{ /* Llib/object.scm 592 */
BgL_tmpz00_8524 = ((bool_t)0)
; }  else 
{ /* Llib/object.scm 592 */
BgL_tmpz00_8524 = ((bool_t)1)
; } 
BgL_auxz00_8523 = 
BBOOL(BgL_tmpz00_8524); } 
VECTOR_SET(BgL_v1316z00_3022,8L,BgL_auxz00_8523); } 
return BgL_v1316z00_3022;} } 

}



/* &make-class-field */
obj_t BGl_z62makezd2classzd2fieldz62zz__objectz00(obj_t BgL_envz00_4794, obj_t BgL_namez00_4795, obj_t BgL_getterz00_4796, obj_t BgL_setterz00_4797, obj_t BgL_ronlyz00_4798, obj_t BgL_virtualz00_4799, obj_t BgL_infoz00_4800, obj_t BgL_defaultz00_4801, obj_t BgL_typez00_4802)
{
{ /* Llib/object.scm 590 */
{ /* Llib/object.scm 591 */
 obj_t BgL_auxz00_8528;
if(
SYMBOLP(BgL_namez00_4795))
{ /* Llib/object.scm 591 */
BgL_auxz00_8528 = BgL_namez00_4795
; }  else 
{ 
 obj_t BgL_auxz00_8531;
BgL_auxz00_8531 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(24930L), BGl_string3468z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_namez00_4795); 
FAILURE(BgL_auxz00_8531,BFALSE,BFALSE);} 
return 
BGl_makezd2classzd2fieldz00zz__objectz00(BgL_auxz00_8528, BgL_getterz00_4796, BgL_setterz00_4797, 
CBOOL(BgL_ronlyz00_4798), 
CBOOL(BgL_virtualz00_4799), BgL_infoz00_4800, BgL_defaultz00_4801, BgL_typez00_4802);} } 

}



/* class-field? */
BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t BgL_objz00_47)
{
{ /* Llib/object.scm 597 */
if(
VECTORP(BgL_objz00_47))
{ /* Llib/object.scm 598 */
if(
(
VECTOR_LENGTH(BgL_objz00_47)==9L))
{ /* Llib/object.scm 599 */
return 
(
VECTOR_REF(BgL_objz00_47,4L)==BGl_makezd2classzd2fieldzd2envzd2zz__objectz00);}  else 
{ /* Llib/object.scm 599 */
return ((bool_t)0);} }  else 
{ /* Llib/object.scm 598 */
return ((bool_t)0);} } 

}



/* &class-field? */
obj_t BGl_z62classzd2fieldzf3z43zz__objectz00(obj_t BgL_envz00_4803, obj_t BgL_objz00_4804)
{
{ /* Llib/object.scm 597 */
return 
BBOOL(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_objz00_4804));} 

}



/* class-field-name */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2namez00zz__objectz00(obj_t BgL_fieldz00_48)
{
{ /* Llib/object.scm 605 */
{ /* Llib/object.scm 606 */
 obj_t BgL_vectorz00_3036;
BgL_vectorz00_3036 = BgL_fieldz00_48; 
{ /* Llib/object.scm 606 */
 obj_t BgL_aux2778z00_5512;
BgL_aux2778z00_5512 = 
VECTOR_REF(BgL_vectorz00_3036,0L); 
if(
SYMBOLP(BgL_aux2778z00_5512))
{ /* Llib/object.scm 606 */
return BgL_aux2778z00_5512;}  else 
{ 
 obj_t BgL_auxz00_8550;
BgL_auxz00_8550 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(25638L), BGl_string3469z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_aux2778z00_5512); 
FAILURE(BgL_auxz00_8550,BFALSE,BFALSE);} } } } 

}



/* &class-field-name */
obj_t BGl_z62classzd2fieldzd2namez62zz__objectz00(obj_t BgL_envz00_4805, obj_t BgL_fieldz00_4806)
{
{ /* Llib/object.scm 605 */
{ /* Llib/object.scm 606 */
 obj_t BgL_auxz00_8554;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4806))
{ /* Llib/object.scm 606 */
BgL_auxz00_8554 = BgL_fieldz00_4806
; }  else 
{ 
 obj_t BgL_auxz00_8557;
BgL_auxz00_8557 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(25638L), BGl_string3470z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4806); 
FAILURE(BgL_auxz00_8557,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldzd2namez00zz__objectz00(BgL_auxz00_8554);} } 

}



/* class-field-virtual? */
BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(obj_t BgL_fieldz00_49)
{
{ /* Llib/object.scm 611 */
{ /* Llib/object.scm 612 */
 obj_t BgL_vectorz00_3037;
BgL_vectorz00_3037 = BgL_fieldz00_49; 
return 
CBOOL(
VECTOR_REF(BgL_vectorz00_3037,3L));} } 

}



/* &class-field-virtual? */
obj_t BGl_z62classzd2fieldzd2virtualzf3z91zz__objectz00(obj_t BgL_envz00_4807, obj_t BgL_fieldz00_4808)
{
{ /* Llib/object.scm 611 */
{ /* Llib/object.scm 612 */
 bool_t BgL_tmpz00_8564;
{ /* Llib/object.scm 612 */
 obj_t BgL_auxz00_8565;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4808))
{ /* Llib/object.scm 612 */
BgL_auxz00_8565 = BgL_fieldz00_4808
; }  else 
{ 
 obj_t BgL_auxz00_8568;
BgL_auxz00_8568 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(25932L), BGl_string3471z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4808); 
FAILURE(BgL_auxz00_8568,BFALSE,BFALSE);} 
BgL_tmpz00_8564 = 
BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_auxz00_8565); } 
return 
BBOOL(BgL_tmpz00_8564);} } 

}



/* class-field-accessor */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t BgL_fieldz00_50)
{
{ /* Llib/object.scm 617 */
{ /* Llib/object.scm 618 */
 obj_t BgL_vectorz00_3038;
BgL_vectorz00_3038 = BgL_fieldz00_50; 
{ /* Llib/object.scm 618 */
 obj_t BgL_aux2784z00_5518;
BgL_aux2784z00_5518 = 
VECTOR_REF(BgL_vectorz00_3038,1L); 
if(
PROCEDUREP(BgL_aux2784z00_5518))
{ /* Llib/object.scm 618 */
return BgL_aux2784z00_5518;}  else 
{ 
 obj_t BgL_auxz00_8577;
BgL_auxz00_8577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26231L), BGl_string3472z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2784z00_5518); 
FAILURE(BgL_auxz00_8577,BFALSE,BFALSE);} } } } 

}



/* &class-field-accessor */
obj_t BGl_z62classzd2fieldzd2accessorz62zz__objectz00(obj_t BgL_envz00_4809, obj_t BgL_fieldz00_4810)
{
{ /* Llib/object.scm 617 */
{ /* Llib/object.scm 618 */
 obj_t BgL_auxz00_8581;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4810))
{ /* Llib/object.scm 618 */
BgL_auxz00_8581 = BgL_fieldz00_4810
; }  else 
{ 
 obj_t BgL_auxz00_8584;
BgL_auxz00_8584 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26231L), BGl_string3474z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4810); 
FAILURE(BgL_auxz00_8584,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldzd2accessorz00zz__objectz00(BgL_auxz00_8581);} } 

}



/* class-field-mutable? */
BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(obj_t BgL_fieldz00_51)
{
{ /* Llib/object.scm 623 */
{ /* Llib/object.scm 624 */
 obj_t BgL_vectorz00_3039;
BgL_vectorz00_3039 = BgL_fieldz00_51; 
return 
CBOOL(
VECTOR_REF(BgL_vectorz00_3039,8L));} } 

}



/* &class-field-mutable? */
obj_t BGl_z62classzd2fieldzd2mutablezf3z91zz__objectz00(obj_t BgL_envz00_4811, obj_t BgL_fieldz00_4812)
{
{ /* Llib/object.scm 623 */
{ /* Llib/object.scm 624 */
 bool_t BgL_tmpz00_8591;
{ /* Llib/object.scm 624 */
 obj_t BgL_auxz00_8592;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4812))
{ /* Llib/object.scm 624 */
BgL_auxz00_8592 = BgL_fieldz00_4812
; }  else 
{ 
 obj_t BgL_auxz00_8595;
BgL_auxz00_8595 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26525L), BGl_string3475z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4812); 
FAILURE(BgL_auxz00_8595,BFALSE,BFALSE);} 
BgL_tmpz00_8591 = 
BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(BgL_auxz00_8592); } 
return 
BBOOL(BgL_tmpz00_8591);} } 

}



/* class-field-mutator */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t BgL_fieldz00_52)
{
{ /* Llib/object.scm 629 */
{ /* Llib/object.scm 630 */
 obj_t BgL_vectorz00_3040;
BgL_vectorz00_3040 = BgL_fieldz00_52; 
{ /* Llib/object.scm 630 */
 obj_t BgL_aux2790z00_5524;
BgL_aux2790z00_5524 = 
VECTOR_REF(BgL_vectorz00_3040,2L); 
if(
PROCEDUREP(BgL_aux2790z00_5524))
{ /* Llib/object.scm 630 */
return BgL_aux2790z00_5524;}  else 
{ 
 obj_t BgL_auxz00_8604;
BgL_auxz00_8604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26823L), BGl_string3476z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2790z00_5524); 
FAILURE(BgL_auxz00_8604,BFALSE,BFALSE);} } } } 

}



/* &class-field-mutator */
obj_t BGl_z62classzd2fieldzd2mutatorz62zz__objectz00(obj_t BgL_envz00_4813, obj_t BgL_fieldz00_4814)
{
{ /* Llib/object.scm 629 */
{ /* Llib/object.scm 630 */
 obj_t BgL_auxz00_8608;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4814))
{ /* Llib/object.scm 630 */
BgL_auxz00_8608 = BgL_fieldz00_4814
; }  else 
{ 
 obj_t BgL_auxz00_8611;
BgL_auxz00_8611 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26823L), BGl_string3477z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4814); 
FAILURE(BgL_auxz00_8611,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldzd2mutatorz00zz__objectz00(BgL_auxz00_8608);} } 

}



/* class-field-info */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2infoz00zz__objectz00(obj_t BgL_fieldz00_53)
{
{ /* Llib/object.scm 635 */
return 
VECTOR_REF(BgL_fieldz00_53,5L);} 

}



/* &class-field-info */
obj_t BGl_z62classzd2fieldzd2infoz62zz__objectz00(obj_t BgL_envz00_4815, obj_t BgL_fieldz00_4816)
{
{ /* Llib/object.scm 635 */
{ /* Llib/object.scm 636 */
 obj_t BgL_auxz00_8617;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4816))
{ /* Llib/object.scm 636 */
BgL_auxz00_8617 = BgL_fieldz00_4816
; }  else 
{ 
 obj_t BgL_auxz00_8620;
BgL_auxz00_8620 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(27107L), BGl_string3478z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4816); 
FAILURE(BgL_auxz00_8620,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldzd2infoz00zz__objectz00(BgL_auxz00_8617);} } 

}



/* class-field-default-value? */
BGL_EXPORTED_DEF bool_t BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(obj_t BgL_fieldz00_54)
{
{ /* Llib/object.scm 641 */
{ /* Llib/object.scm 642 */
 obj_t BgL_tmpz00_8625;
BgL_tmpz00_8625 = 
VECTOR_REF(BgL_fieldz00_54,6L); 
return 
PROCEDUREP(BgL_tmpz00_8625);} } 

}



/* &class-field-default-value? */
obj_t BGl_z62classzd2fieldzd2defaultzd2valuezf3z43zz__objectz00(obj_t BgL_envz00_4817, obj_t BgL_fieldz00_4818)
{
{ /* Llib/object.scm 641 */
{ /* Llib/object.scm 642 */
 bool_t BgL_tmpz00_8628;
{ /* Llib/object.scm 642 */
 obj_t BgL_auxz00_8629;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4818))
{ /* Llib/object.scm 642 */
BgL_auxz00_8629 = BgL_fieldz00_4818
; }  else 
{ 
 obj_t BgL_auxz00_8632;
BgL_auxz00_8632 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(27413L), BGl_string3479z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4818); 
FAILURE(BgL_auxz00_8632,BFALSE,BFALSE);} 
BgL_tmpz00_8628 = 
BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(BgL_auxz00_8629); } 
return 
BBOOL(BgL_tmpz00_8628);} } 

}



/* class-field-default-value */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t BgL_fieldz00_55)
{
{ /* Llib/object.scm 647 */
{ /* Llib/object.scm 650 */
 obj_t BgL_pz00_1336;
BgL_pz00_1336 = 
VECTOR_REF(BgL_fieldz00_55,6L); 
if(
PROCEDUREP(BgL_pz00_1336))
{ /* Llib/object.scm 651 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_pz00_1336, 0))
{ /* Llib/object.scm 652 */
return 
BGL_PROCEDURE_CALL0(BgL_pz00_1336);}  else 
{ /* Llib/object.scm 652 */
FAILURE(BGl_string3480z00zz__objectz00,BGl_list3481z00zz__objectz00,BgL_pz00_1336);} }  else 
{ /* Llib/object.scm 655 */
 obj_t BgL_arg1527z00_1338;
{ /* Llib/object.scm 655 */
 obj_t BgL_res2555z00_3047;
{ /* Llib/object.scm 606 */
 obj_t BgL_vectorz00_3046;
BgL_vectorz00_3046 = BgL_fieldz00_55; 
{ /* Llib/object.scm 606 */
 obj_t BgL_aux2799z00_5534;
BgL_aux2799z00_5534 = 
VECTOR_REF(BgL_vectorz00_3046,0L); 
if(
SYMBOLP(BgL_aux2799z00_5534))
{ /* Llib/object.scm 606 */
BgL_res2555z00_3047 = BgL_aux2799z00_5534; }  else 
{ 
 obj_t BgL_auxz00_8650;
BgL_auxz00_8650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(25638L), BGl_string3486z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_aux2799z00_5534); 
FAILURE(BgL_auxz00_8650,BFALSE,BFALSE);} } } 
BgL_arg1527z00_1338 = BgL_res2555z00_3047; } 
return 
BGl_errorz00zz__errorz00(BGl_string3486z00zz__objectz00, BGl_string3487z00zz__objectz00, BgL_arg1527z00_1338);} } } 

}



/* &class-field-default-value */
obj_t BGl_z62classzd2fieldzd2defaultzd2valuezb0zz__objectz00(obj_t BgL_envz00_4819, obj_t BgL_fieldz00_4820)
{
{ /* Llib/object.scm 647 */
{ /* Llib/object.scm 650 */
 obj_t BgL_auxz00_8655;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4820))
{ /* Llib/object.scm 650 */
BgL_auxz00_8655 = BgL_fieldz00_4820
; }  else 
{ 
 obj_t BgL_auxz00_8658;
BgL_auxz00_8658 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(27799L), BGl_string3488z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4820); 
FAILURE(BgL_auxz00_8658,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_8655);} } 

}



/* class-field-type */
BGL_EXPORTED_DEF obj_t BGl_classzd2fieldzd2typez00zz__objectz00(obj_t BgL_fieldz00_56)
{
{ /* Llib/object.scm 660 */
return 
VECTOR_REF(BgL_fieldz00_56,7L);} 

}



/* &class-field-type */
obj_t BGl_z62classzd2fieldzd2typez62zz__objectz00(obj_t BgL_envz00_4821, obj_t BgL_fieldz00_4822)
{
{ /* Llib/object.scm 660 */
{ /* Llib/object.scm 661 */
 obj_t BgL_auxz00_8664;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_4822))
{ /* Llib/object.scm 661 */
BgL_auxz00_8664 = BgL_fieldz00_4822
; }  else 
{ 
 obj_t BgL_auxz00_8667;
BgL_auxz00_8667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(28238L), BGl_string3489z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_4822); 
FAILURE(BgL_auxz00_8667,BFALSE,BFALSE);} 
return 
BGl_classzd2fieldzd2typez00zz__objectz00(BgL_auxz00_8664);} } 

}



/* class-super */
BGL_EXPORTED_DEF obj_t BGl_classzd2superzd2zz__objectz00(obj_t BgL_classz00_57)
{
{ /* Llib/object.scm 666 */
return 
BGL_CLASS_SUPER(BgL_classz00_57);} 

}



/* &class-super */
obj_t BGl_z62classzd2superzb0zz__objectz00(obj_t BgL_envz00_4823, obj_t BgL_classz00_4824)
{
{ /* Llib/object.scm 666 */
{ /* Llib/object.scm 667 */
 obj_t BgL_auxz00_8673;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4824))
{ /* Llib/object.scm 667 */
BgL_auxz00_8673 = BgL_classz00_4824
; }  else 
{ 
 obj_t BgL_auxz00_8676;
BgL_auxz00_8676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(28517L), BGl_string3490z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4824); 
FAILURE(BgL_auxz00_8676,BFALSE,BFALSE);} 
return 
BGl_classzd2superzd2zz__objectz00(BgL_auxz00_8673);} } 

}



/* class-abstract? */
BGL_EXPORTED_DEF bool_t BGl_classzd2abstractzf3z21zz__objectz00(obj_t BgL_classz00_61)
{
{ /* Llib/object.scm 684 */
{ /* Llib/object.scm 685 */
 obj_t BgL_arg1530z00_3050;
{ /* Llib/object.scm 685 */
 obj_t BgL_res2556z00_3053;
if(
BGL_CLASSP(BgL_classz00_61))
{ /* Llib/object.scm 709 */
BgL_res2556z00_3053 = 
BGL_CLASS_ALLOC_FUN(BgL_classz00_61); }  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux2807z00_5542;
BgL_aux2807z00_5542 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_61); 
if(
PROCEDUREP(BgL_aux2807z00_5542))
{ /* Llib/object.scm 711 */
BgL_res2556z00_3053 = BgL_aux2807z00_5542; }  else 
{ 
 obj_t BgL_auxz00_8687;
BgL_auxz00_8687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3492z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2807z00_5542); 
FAILURE(BgL_auxz00_8687,BFALSE,BFALSE);} } 
BgL_arg1530z00_3050 = BgL_res2556z00_3053; } ((bool_t)1); } 
return ((bool_t)0);} 

}



/* &class-abstract? */
obj_t BGl_z62classzd2abstractzf3z43zz__objectz00(obj_t BgL_envz00_4825, obj_t BgL_classz00_4826)
{
{ /* Llib/object.scm 684 */
{ /* Llib/object.scm 685 */
 bool_t BgL_tmpz00_8691;
{ /* Llib/object.scm 685 */
 obj_t BgL_auxz00_8692;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4826))
{ /* Llib/object.scm 685 */
BgL_auxz00_8692 = BgL_classz00_4826
; }  else 
{ 
 obj_t BgL_auxz00_8695;
BgL_auxz00_8695 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(29406L), BGl_string3493z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4826); 
FAILURE(BgL_auxz00_8695,BFALSE,BFALSE);} 
BgL_tmpz00_8691 = 
BGl_classzd2abstractzf3z21zz__objectz00(BgL_auxz00_8692); } 
return 
BBOOL(BgL_tmpz00_8691);} } 

}



/* class-wide? */
BGL_EXPORTED_DEF bool_t BGl_classzd2widezf3z21zz__objectz00(obj_t BgL_classz00_62)
{
{ /* Llib/object.scm 690 */
{ /* Llib/object.scm 691 */
 obj_t BgL_arg1531z00_3054;
if(
BGL_CLASSP(BgL_classz00_62))
{ /* Llib/object.scm 765 */
BgL_arg1531z00_3054 = 
BGL_CLASS_SHRINK(BgL_classz00_62); }  else 
{ /* Llib/object.scm 765 */
BgL_arg1531z00_3054 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3494z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_62); } 
return 
PROCEDUREP(BgL_arg1531z00_3054);} } 

}



/* &class-wide? */
obj_t BGl_z62classzd2widezf3z43zz__objectz00(obj_t BgL_envz00_4827, obj_t BgL_classz00_4828)
{
{ /* Llib/object.scm 690 */
{ /* Llib/object.scm 691 */
 bool_t BgL_tmpz00_8706;
{ /* Llib/object.scm 691 */
 obj_t BgL_auxz00_8707;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4828))
{ /* Llib/object.scm 691 */
BgL_auxz00_8707 = BgL_classz00_4828
; }  else 
{ 
 obj_t BgL_auxz00_8710;
BgL_auxz00_8710 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(29723L), BGl_string3495z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4828); 
FAILURE(BgL_auxz00_8710,BFALSE,BFALSE);} 
BgL_tmpz00_8706 = 
BGl_classzd2widezf3z21zz__objectz00(BgL_auxz00_8707); } 
return 
BBOOL(BgL_tmpz00_8706);} } 

}



/* class-subclasses */
BGL_EXPORTED_DEF obj_t BGl_classzd2subclasseszd2zz__objectz00(obj_t BgL_classz00_63)
{
{ /* Llib/object.scm 696 */
return 
BGL_CLASS_SUBCLASSES(BgL_classz00_63);} 

}



/* &class-subclasses */
obj_t BGl_z62classzd2subclasseszb0zz__objectz00(obj_t BgL_envz00_4829, obj_t BgL_classz00_4830)
{
{ /* Llib/object.scm 696 */
{ /* Llib/object.scm 697 */
 obj_t BgL_auxz00_8717;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4830))
{ /* Llib/object.scm 697 */
BgL_auxz00_8717 = BgL_classz00_4830
; }  else 
{ 
 obj_t BgL_auxz00_8720;
BgL_auxz00_8720 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30012L), BGl_string3496z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4830); 
FAILURE(BgL_auxz00_8720,BFALSE,BFALSE);} 
return 
BGl_classzd2subclasseszd2zz__objectz00(BgL_auxz00_8717);} } 

}



/* class-allocator */
BGL_EXPORTED_DEF obj_t BGl_classzd2allocatorzd2zz__objectz00(obj_t BgL_classz00_66)
{
{ /* Llib/object.scm 708 */
if(
BGL_CLASSP(BgL_classz00_66))
{ /* Llib/object.scm 709 */
return 
BGL_CLASS_ALLOC_FUN(BgL_classz00_66);}  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux2815z00_5550;
BgL_aux2815z00_5550 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_66); 
if(
PROCEDUREP(BgL_aux2815z00_5550))
{ /* Llib/object.scm 711 */
return BgL_aux2815z00_5550;}  else 
{ 
 obj_t BgL_auxz00_8731;
BgL_auxz00_8731 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3491z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2815z00_5550); 
FAILURE(BgL_auxz00_8731,BFALSE,BFALSE);} } } 

}



/* &class-allocator */
obj_t BGl_z62classzd2allocatorzb0zz__objectz00(obj_t BgL_envz00_4831, obj_t BgL_classz00_4832)
{
{ /* Llib/object.scm 708 */
{ /* Llib/object.scm 709 */
 obj_t BgL_auxz00_8735;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4832))
{ /* Llib/object.scm 709 */
BgL_auxz00_8735 = BgL_classz00_4832
; }  else 
{ 
 obj_t BgL_auxz00_8738;
BgL_auxz00_8738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30608L), BGl_string3497z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4832); 
FAILURE(BgL_auxz00_8738,BFALSE,BFALSE);} 
return 
BGl_classzd2allocatorzd2zz__objectz00(BgL_auxz00_8735);} } 

}



/* class-hash */
BGL_EXPORTED_DEF long BGl_classzd2hashzd2zz__objectz00(obj_t BgL_classz00_67)
{
{ /* Llib/object.scm 716 */
return 
BGL_CLASS_HASH(BgL_classz00_67);} 

}



/* &class-hash */
obj_t BGl_z62classzd2hashzb0zz__objectz00(obj_t BgL_envz00_4833, obj_t BgL_classz00_4834)
{
{ /* Llib/object.scm 716 */
{ /* Llib/object.scm 717 */
 long BgL_tmpz00_8744;
{ /* Llib/object.scm 717 */
 obj_t BgL_auxz00_8745;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4834))
{ /* Llib/object.scm 717 */
BgL_auxz00_8745 = BgL_classz00_4834
; }  else 
{ 
 obj_t BgL_auxz00_8748;
BgL_auxz00_8748 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30973L), BGl_string3498z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4834); 
FAILURE(BgL_auxz00_8748,BFALSE,BFALSE);} 
BgL_tmpz00_8744 = 
BGl_classzd2hashzd2zz__objectz00(BgL_auxz00_8745); } 
return 
BINT(BgL_tmpz00_8744);} } 

}



/* class-constructor */
BGL_EXPORTED_DEF obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t BgL_classz00_68)
{
{ /* Llib/object.scm 722 */
return 
BGL_CLASS_CONSTRUCTOR(BgL_classz00_68);} 

}



/* &class-constructor */
obj_t BGl_z62classzd2constructorzb0zz__objectz00(obj_t BgL_envz00_4835, obj_t BgL_classz00_4836)
{
{ /* Llib/object.scm 722 */
{ /* Llib/object.scm 723 */
 obj_t BgL_auxz00_8755;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4836))
{ /* Llib/object.scm 723 */
BgL_auxz00_8755 = BgL_classz00_4836
; }  else 
{ 
 obj_t BgL_auxz00_8758;
BgL_auxz00_8758 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(31254L), BGl_string3499z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4836); 
FAILURE(BgL_auxz00_8758,BFALSE,BFALSE);} 
return 
BGl_classzd2constructorzd2zz__objectz00(BgL_auxz00_8755);} } 

}



/* class-creator */
BGL_EXPORTED_DEF obj_t BGl_classzd2creatorzd2zz__objectz00(obj_t BgL_classz00_69)
{
{ /* Llib/object.scm 728 */
return 
BGL_CLASS_NEW_FUN(BgL_classz00_69);} 

}



/* &class-creator */
obj_t BGl_z62classzd2creatorzb0zz__objectz00(obj_t BgL_envz00_4837, obj_t BgL_classz00_4838)
{
{ /* Llib/object.scm 728 */
{ /* Llib/object.scm 729 */
 obj_t BgL_auxz00_8764;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4838))
{ /* Llib/object.scm 729 */
BgL_auxz00_8764 = BgL_classz00_4838
; }  else 
{ 
 obj_t BgL_auxz00_8767;
BgL_auxz00_8767 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(31538L), BGl_string3500z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4838); 
FAILURE(BgL_auxz00_8767,BFALSE,BFALSE);} 
return 
BGl_classzd2creatorzd2zz__objectz00(BgL_auxz00_8764);} } 

}



/* class-nil-init! */
BGL_EXPORTED_DEF obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t BgL_classz00_70)
{
{ /* Llib/object.scm 734 */
{ /* Llib/object.scm 735 */
 obj_t BgL_procz00_1344;
BgL_procz00_1344 = 
BGL_CLASS_NIL_FUN(BgL_classz00_70); 
{ /* Llib/object.scm 736 */
 bool_t BgL_test4561z00_8773;
{ /* Llib/object.scm 691 */
 obj_t BgL_tmpz00_8774;
BgL_tmpz00_8774 = 
BGl_classzd2shrinkzd2zz__objectz00(BgL_classz00_70); 
BgL_test4561z00_8773 = 
PROCEDUREP(BgL_tmpz00_8774); } 
if(BgL_test4561z00_8773)
{ /* Llib/object.scm 737 */
 obj_t BgL_superz00_1346;
BgL_superz00_1346 = 
BGL_CLASS_SUPER(BgL_classz00_70); 
{ /* Llib/object.scm 737 */
 obj_t BgL_oz00_1347;
{ /* Llib/object.scm 738 */
 obj_t BgL_fun1536z00_1350;
{ /* Llib/object.scm 738 */
 obj_t BgL_res2557z00_3062;
{ /* Llib/object.scm 738 */
 obj_t BgL_classz00_3060;
if(
BGL_CLASSP(BgL_superz00_1346))
{ /* Llib/object.scm 738 */
BgL_classz00_3060 = BgL_superz00_1346; }  else 
{ 
 obj_t BgL_auxz00_8780;
BgL_auxz00_8780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(31949L), BGl_string3501z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_1346); 
FAILURE(BgL_auxz00_8780,BFALSE,BFALSE);} 
if(
BGL_CLASSP(BgL_classz00_3060))
{ /* Llib/object.scm 709 */
BgL_res2557z00_3062 = 
BGL_CLASS_ALLOC_FUN(BgL_classz00_3060); }  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux2827z00_5562;
BgL_aux2827z00_5562 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_3060); 
if(
PROCEDUREP(BgL_aux2827z00_5562))
{ /* Llib/object.scm 711 */
BgL_res2557z00_3062 = BgL_aux2827z00_5562; }  else 
{ 
 obj_t BgL_auxz00_8790;
BgL_auxz00_8790 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3501z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2827z00_5562); 
FAILURE(BgL_auxz00_8790,BFALSE,BFALSE);} } } 
BgL_fun1536z00_1350 = BgL_res2557z00_3062; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fun1536z00_1350, 0))
{ /* Llib/object.scm 738 */
BgL_oz00_1347 = 
BGL_PROCEDURE_CALL0(BgL_fun1536z00_1350); }  else 
{ /* Llib/object.scm 738 */
FAILURE(BGl_string3502z00zz__objectz00,BGl_list3503z00zz__objectz00,BgL_fun1536z00_1350);} } 
{ /* Llib/object.scm 738 */
 obj_t BgL_woz00_1348;
{ /* Llib/object.scm 739 */
 obj_t BgL_fun1534z00_1349;
{ /* Llib/object.scm 739 */
 obj_t BgL_res2558z00_3065;
if(
BGL_CLASSP(BgL_classz00_70))
{ /* Llib/object.scm 709 */
BgL_res2558z00_3065 = 
BGL_CLASS_ALLOC_FUN(BgL_classz00_70); }  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux2830z00_5566;
BgL_aux2830z00_5566 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_70); 
if(
PROCEDUREP(BgL_aux2830z00_5566))
{ /* Llib/object.scm 711 */
BgL_res2558z00_3065 = BgL_aux2830z00_5566; }  else 
{ 
 obj_t BgL_auxz00_8806;
BgL_auxz00_8806 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3501z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2830z00_5566); 
FAILURE(BgL_auxz00_8806,BFALSE,BFALSE);} } 
BgL_fun1534z00_1349 = BgL_res2558z00_3065; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fun1534z00_1349, 1))
{ /* Llib/object.scm 739 */
BgL_woz00_1348 = 
BGL_PROCEDURE_CALL1(BgL_fun1534z00_1349, BgL_oz00_1347); }  else 
{ /* Llib/object.scm 739 */
FAILURE(BGl_string3502z00zz__objectz00,BGl_list3506z00zz__objectz00,BgL_fun1534z00_1349);} } 
{ /* Llib/object.scm 739 */

BGL_CLASS_NIL_SET(BgL_classz00_70, BgL_woz00_1348); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_1344, 1))
{ /* Llib/object.scm 741 */
BGL_PROCEDURE_CALL1(BgL_procz00_1344, BgL_woz00_1348); }  else 
{ /* Llib/object.scm 741 */
FAILURE(BGl_string3502z00zz__objectz00,BGl_list3511z00zz__objectz00,BgL_procz00_1344);} 
return BgL_woz00_1348;} } } }  else 
{ /* Llib/object.scm 743 */
 obj_t BgL_oz00_1351;
{ /* Llib/object.scm 743 */
 obj_t BgL_fun1537z00_1352;
{ /* Llib/object.scm 743 */
 obj_t BgL_res2559z00_3068;
if(
BGL_CLASSP(BgL_classz00_70))
{ /* Llib/object.scm 709 */
BgL_res2559z00_3068 = 
BGL_CLASS_ALLOC_FUN(BgL_classz00_70); }  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux2834z00_5572;
BgL_aux2834z00_5572 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_70); 
if(
PROCEDUREP(BgL_aux2834z00_5572))
{ /* Llib/object.scm 711 */
BgL_res2559z00_3068 = BgL_aux2834z00_5572; }  else 
{ 
 obj_t BgL_auxz00_8831;
BgL_auxz00_8831 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3501z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2834z00_5572); 
FAILURE(BgL_auxz00_8831,BFALSE,BFALSE);} } 
BgL_fun1537z00_1352 = BgL_res2559z00_3068; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fun1537z00_1352, 0))
{ /* Llib/object.scm 743 */
BgL_oz00_1351 = 
BGL_PROCEDURE_CALL0(BgL_fun1537z00_1352); }  else 
{ /* Llib/object.scm 743 */
FAILURE(BGl_string3502z00zz__objectz00,BGl_list3516z00zz__objectz00,BgL_fun1537z00_1352);} } 
BGL_CLASS_NIL_SET(BgL_classz00_70, BgL_oz00_1351); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_1344, 1))
{ /* Llib/object.scm 745 */
BGL_PROCEDURE_CALL1(BgL_procz00_1344, BgL_oz00_1351); }  else 
{ /* Llib/object.scm 745 */
FAILURE(BGl_string3502z00zz__objectz00,BGl_list3519z00zz__objectz00,BgL_procz00_1344);} 
return BgL_oz00_1351;} } } } 

}



/* &class-nil-init! */
obj_t BGl_z62classzd2nilzd2initz12z70zz__objectz00(obj_t BgL_envz00_4839, obj_t BgL_classz00_4840)
{
{ /* Llib/object.scm 734 */
{ /* Llib/object.scm 735 */
 obj_t BgL_auxz00_8849;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4840))
{ /* Llib/object.scm 735 */
BgL_auxz00_8849 = BgL_classz00_4840
; }  else 
{ 
 obj_t BgL_auxz00_8852;
BgL_auxz00_8852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(31820L), BGl_string3520z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4840); 
FAILURE(BgL_auxz00_8852,BFALSE,BFALSE);} 
return 
BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_auxz00_8849);} } 

}



/* class-nil */
BGL_EXPORTED_DEF obj_t BGl_classzd2nilzd2zz__objectz00(obj_t BgL_classz00_71)
{
{ /* Llib/object.scm 751 */
if(
BGL_CLASSP(BgL_classz00_71))
{ /* Llib/object.scm 758 */
 obj_t BgL__ortest_1218z00_6457;
BgL__ortest_1218z00_6457 = 
BGL_CLASS_NIL(BgL_classz00_71); 
if(
CBOOL(BgL__ortest_1218z00_6457))
{ /* Llib/object.scm 758 */
return BgL__ortest_1218z00_6457;}  else 
{ /* Llib/object.scm 758 */
BGL_TAIL return 
BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_71);} }  else 
{ /* Llib/object.scm 757 */
return 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3521z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_71);} } 

}



/* &class-nil */
obj_t BGl_z62classzd2nilzb0zz__objectz00(obj_t BgL_envz00_4841, obj_t BgL_classz00_4842)
{
{ /* Llib/object.scm 751 */
{ /* Llib/object.scm 757 */
 obj_t BgL_auxz00_8864;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4842))
{ /* Llib/object.scm 757 */
BgL_auxz00_8864 = BgL_classz00_4842
; }  else 
{ 
 obj_t BgL_auxz00_8867;
BgL_auxz00_8867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(32559L), BGl_string3522z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4842); 
FAILURE(BgL_auxz00_8867,BFALSE,BFALSE);} 
return 
BGl_classzd2nilzd2zz__objectz00(BgL_auxz00_8864);} } 

}



/* class-shrink */
obj_t BGl_classzd2shrinkzd2zz__objectz00(obj_t BgL_classz00_72)
{
{ /* Llib/object.scm 764 */
if(
BGL_CLASSP(BgL_classz00_72))
{ /* Llib/object.scm 765 */
return 
BGL_CLASS_SHRINK(BgL_classz00_72);}  else 
{ /* Llib/object.scm 765 */
return 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3494z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_72);} } 

}



/* initialize-objects! */
obj_t BGl_initializa7ezd2objectsz12z67zz__objectz00(void)
{
{ /* Llib/object.scm 807 */
if(
INTEGERP(BGl_za2nbzd2classesza2zd2zz__objectz00))
{ /* Llib/object.scm 808 */
return BFALSE;}  else 
{ /* Llib/object.scm 808 */
BGl_za2nbzd2classesza2zd2zz__objectz00 = 
BINT(0L); 
BGl_za2nbzd2classeszd2maxza2z00zz__objectz00 = 
BINT(64L); 
{ /* Llib/object.scm 811 */
 long BgL_tmpz00_8880;
{ /* Llib/object.scm 811 */
 obj_t BgL_tmpz00_8881;
{ /* Llib/object.scm 811 */
 obj_t BgL_aux2842z00_5582;
BgL_aux2842z00_5582 = BGl_za2nbzd2classeszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2842z00_5582))
{ /* Llib/object.scm 811 */
BgL_tmpz00_8881 = BgL_aux2842z00_5582
; }  else 
{ 
 obj_t BgL_auxz00_8884;
BgL_auxz00_8884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(35307L), BGl_string3523z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2842z00_5582); 
FAILURE(BgL_auxz00_8884,BFALSE,BFALSE);} } 
BgL_tmpz00_8880 = 
(long)CINT(BgL_tmpz00_8881); } 
BGl_za2classesza2z00zz__objectz00 = 
make_vector_uncollectable(BgL_tmpz00_8880, BFALSE); } 
BGl_za2inheritancezd2cntza2zd2zz__objectz00 = 
BINT(0L); 
BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00 = 
BINT(128L); 
BGl_za2inheritancesza2z00zz__objectz00 = 
make_vector_uncollectable(256L, BFALSE); 
BGl_za2nbzd2genericszd2maxza2z00zz__objectz00 = 
BINT(64L); 
BGl_za2nbzd2genericsza2zd2zz__objectz00 = 
BINT(0L); 
{ /* Llib/object.scm 822 */
 long BgL_tmpz00_8895;
{ /* Llib/object.scm 822 */
 obj_t BgL_tmpz00_8896;
{ /* Llib/object.scm 822 */
 obj_t BgL_aux2843z00_5583;
BgL_aux2843z00_5583 = BGl_za2nbzd2genericszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2843z00_5583))
{ /* Llib/object.scm 822 */
BgL_tmpz00_8896 = BgL_aux2843z00_5583
; }  else 
{ 
 obj_t BgL_auxz00_8899;
BgL_auxz00_8899 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(35696L), BGl_string3523z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2843z00_5583); 
FAILURE(BgL_auxz00_8899,BFALSE,BFALSE);} } 
BgL_tmpz00_8895 = 
(long)CINT(BgL_tmpz00_8896); } 
BGl_za2genericsza2z00zz__objectz00 = 
make_vector_uncollectable(BgL_tmpz00_8895, BFALSE); } 
if(
PAIRP(BGl_za2classzd2keyza2zd2zz__objectz00))
{ /* Llib/object.scm 823 */
return BFALSE;}  else 
{ /* Llib/object.scm 823 */
return ( 
BGl_za2classzd2keyza2zd2zz__objectz00 = 
MAKE_YOUNG_PAIR(
BINT(1L), 
BINT(2L)), BUNSPEC) ;} } } 

}



/* extend-vector */
obj_t BGl_extendzd2vectorzd2zz__objectz00(obj_t BgL_oldzd2veczd2_73, obj_t BgL_fillz00_74, long BgL_extendz00_75)
{
{ /* Llib/object.scm 828 */
{ /* Llib/object.scm 829 */
 long BgL_oldzd2lenzd2_1358;
{ /* Llib/object.scm 829 */
 obj_t BgL_vectorz00_3075;
if(
VECTORP(BgL_oldzd2veczd2_73))
{ /* Llib/object.scm 829 */
BgL_vectorz00_3075 = BgL_oldzd2veczd2_73; }  else 
{ 
 obj_t BgL_auxz00_8912;
BgL_auxz00_8912 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36087L), BGl_string3524z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2veczd2_73); 
FAILURE(BgL_auxz00_8912,BFALSE,BFALSE);} 
BgL_oldzd2lenzd2_1358 = 
VECTOR_LENGTH(BgL_vectorz00_3075); } 
{ /* Llib/object.scm 829 */
 long BgL_newzd2lenzd2_1359;
BgL_newzd2lenzd2_1359 = 
(BgL_extendz00_75+BgL_oldzd2lenzd2_1358); 
{ /* Llib/object.scm 830 */
 obj_t BgL_newzd2veczd2_1360;
BgL_newzd2veczd2_1360 = 
make_vector_uncollectable(BgL_newzd2lenzd2_1359, BgL_fillz00_74); 
{ /* Llib/object.scm 831 */

{ 
 long BgL_iz00_1362;
BgL_iz00_1362 = 0L; 
BgL_zc3z04anonymousza31542ze3z87_1363:
if(
(BgL_iz00_1362==BgL_oldzd2lenzd2_1358))
{ /* Llib/object.scm 833 */
return BgL_newzd2veczd2_1360;}  else 
{ /* Llib/object.scm 833 */
{ /* Llib/object.scm 836 */
 obj_t BgL_arg1544z00_1365;
{ /* Llib/object.scm 836 */
 obj_t BgL_vectorz00_3080;
if(
VECTORP(BgL_oldzd2veczd2_73))
{ /* Llib/object.scm 836 */
BgL_vectorz00_3080 = BgL_oldzd2veczd2_73; }  else 
{ 
 obj_t BgL_auxz00_8923;
BgL_auxz00_8923 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36303L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2veczd2_73); 
FAILURE(BgL_auxz00_8923,BFALSE,BFALSE);} 
BgL_arg1544z00_1365 = 
VECTOR_REF(BgL_vectorz00_3080,BgL_iz00_1362); } 
VECTOR_SET(BgL_newzd2veczd2_1360,BgL_iz00_1362,BgL_arg1544z00_1365); } 
{ 
 long BgL_iz00_8929;
BgL_iz00_8929 = 
(BgL_iz00_1362+1L); 
BgL_iz00_1362 = BgL_iz00_8929; 
goto BgL_zc3z04anonymousza31542ze3z87_1363;} } } } } } } } 

}



/* double-nb-classes! */
bool_t BGl_doublezd2nbzd2classesz12z12zz__objectz00(void)
{
{ /* Llib/object.scm 850 */
{ /* Llib/object.scm 851 */
 long BgL_za72za7_3088;
{ /* Llib/object.scm 851 */
 obj_t BgL_tmpz00_8931;
{ /* Llib/object.scm 851 */
 obj_t BgL_aux2848z00_5588;
BgL_aux2848z00_5588 = BGl_za2nbzd2classeszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2848z00_5588))
{ /* Llib/object.scm 851 */
BgL_tmpz00_8931 = BgL_aux2848z00_5588
; }  else 
{ 
 obj_t BgL_auxz00_8934;
BgL_auxz00_8934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(37023L), BGl_string3525z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2848z00_5588); 
FAILURE(BgL_auxz00_8934,BFALSE,BFALSE);} } 
BgL_za72za7_3088 = 
(long)CINT(BgL_tmpz00_8931); } 
BGl_za2nbzd2classeszd2maxza2z00zz__objectz00 = 
BINT(
(2L*BgL_za72za7_3088)); } 
{ /* Llib/object.scm 852 */
 obj_t BgL_oldzd2veczd2_3089;
BgL_oldzd2veczd2_3089 = BGl_za2classesza2z00zz__objectz00; 
{ /* Llib/object.scm 843 */
 obj_t BgL_newzd2veczd2_3090;
{ /* Llib/object.scm 843 */
 long BgL_arg1547z00_3091;
{ /* Llib/object.scm 843 */
 obj_t BgL_vectorz00_3092;
if(
VECTORP(BgL_oldzd2veczd2_3089))
{ /* Llib/object.scm 843 */
BgL_vectorz00_3092 = BgL_oldzd2veczd2_3089; }  else 
{ 
 obj_t BgL_auxz00_8943;
BgL_auxz00_8943 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36661L), BGl_string3525z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2veczd2_3089); 
FAILURE(BgL_auxz00_8943,BFALSE,BFALSE);} 
BgL_arg1547z00_3091 = 
VECTOR_LENGTH(BgL_vectorz00_3092); } 
BgL_newzd2veczd2_3090 = 
BGl_extendzd2vectorzd2zz__objectz00(BgL_oldzd2veczd2_3089, BFALSE, BgL_arg1547z00_3091); } 
{ /* Llib/object.scm 844 */
 obj_t BgL_tmpz00_8949;
if(
VECTORP(BgL_oldzd2veczd2_3089))
{ /* Llib/object.scm 844 */
BgL_tmpz00_8949 = BgL_oldzd2veczd2_3089
; }  else 
{ 
 obj_t BgL_auxz00_8952;
BgL_auxz00_8952 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36707L), BGl_string3525z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2veczd2_3089); 
FAILURE(BgL_auxz00_8952,BFALSE,BFALSE);} 
FREE_VECTOR_UNCOLLECTABLE(BgL_tmpz00_8949); } BUNSPEC; 
BGl_za2classesza2z00zz__objectz00 = BgL_newzd2veczd2_3090; } } 
{ 
 long BgL_iz00_1371;
BgL_iz00_1371 = 0L; 
BgL_zc3z04anonymousza31548ze3z87_1372:
{ /* Llib/object.scm 855 */
 bool_t BgL_test4590z00_8957;
{ /* Llib/object.scm 855 */
 long BgL_n2z00_3094;
{ /* Llib/object.scm 855 */
 obj_t BgL_tmpz00_8958;
{ /* Llib/object.scm 855 */
 obj_t BgL_aux2853z00_5593;
BgL_aux2853z00_5593 = BGl_za2nbzd2genericsza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2853z00_5593))
{ /* Llib/object.scm 855 */
BgL_tmpz00_8958 = BgL_aux2853z00_5593
; }  else 
{ 
 obj_t BgL_auxz00_8961;
BgL_auxz00_8961 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(37192L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2853z00_5593); 
FAILURE(BgL_auxz00_8961,BFALSE,BFALSE);} } 
BgL_n2z00_3094 = 
(long)CINT(BgL_tmpz00_8958); } 
BgL_test4590z00_8957 = 
(BgL_iz00_1371<BgL_n2z00_3094); } 
if(BgL_test4590z00_8957)
{ /* Llib/object.scm 856 */
 obj_t BgL_genz00_1374;
{ /* Llib/object.scm 856 */
 obj_t BgL_vectorz00_3095;
{ /* Llib/object.scm 856 */
 obj_t BgL_aux2854z00_5594;
BgL_aux2854z00_5594 = BGl_za2genericsza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2854z00_5594))
{ /* Llib/object.scm 856 */
BgL_vectorz00_3095 = BgL_aux2854z00_5594; }  else 
{ 
 obj_t BgL_auxz00_8969;
BgL_auxz00_8969 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(37236L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2854z00_5594); 
FAILURE(BgL_auxz00_8969,BFALSE,BFALSE);} } 
BgL_genz00_1374 = 
VECTOR_REF(BgL_vectorz00_3095,BgL_iz00_1371); } 
{ /* Llib/object.scm 856 */
 obj_t BgL_defaultzd2bucketzd2_1375;
{ /* Llib/object.scm 857 */
 obj_t BgL_res2560z00_3097;
{ /* Llib/object.scm 918 */
 obj_t BgL_aux2858z00_5598;
{ /* Llib/object.scm 918 */
 obj_t BgL_tmpz00_8974;
if(
PROCEDUREP(BgL_genz00_1374))
{ /* Llib/object.scm 918 */
BgL_tmpz00_8974 = BgL_genz00_1374
; }  else 
{ 
 obj_t BgL_auxz00_8977;
BgL_auxz00_8977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40183L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1374); 
FAILURE(BgL_auxz00_8977,BFALSE,BFALSE);} 
BgL_aux2858z00_5598 = 
PROCEDURE_REF(BgL_tmpz00_8974, 
(int)(2L)); } 
if(
VECTORP(BgL_aux2858z00_5598))
{ /* Llib/object.scm 918 */
BgL_res2560z00_3097 = BgL_aux2858z00_5598; }  else 
{ 
 obj_t BgL_auxz00_8985;
BgL_auxz00_8985 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40168L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2858z00_5598); 
FAILURE(BgL_auxz00_8985,BFALSE,BFALSE);} } 
BgL_defaultzd2bucketzd2_1375 = BgL_res2560z00_3097; } 
{ /* Llib/object.scm 857 */
 obj_t BgL_oldzd2methodzd2arrayz00_1376;
{ /* Llib/object.scm 858 */
 obj_t BgL_genericz00_3098;
if(
PROCEDUREP(BgL_genz00_1374))
{ /* Llib/object.scm 858 */
BgL_genericz00_3098 = BgL_genz00_1374; }  else 
{ 
 obj_t BgL_auxz00_8991;
BgL_auxz00_8991 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(37341L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1374); 
FAILURE(BgL_auxz00_8991,BFALSE,BFALSE);} 
BgL_oldzd2methodzd2arrayz00_1376 = 
PROCEDURE_REF(BgL_genericz00_3098, 
(int)(1L)); } 
{ /* Llib/object.scm 858 */

{ /* Llib/object.scm 861 */
 obj_t BgL_arg1552z00_1377;
{ /* Llib/object.scm 843 */
 obj_t BgL_newzd2veczd2_3099;
{ /* Llib/object.scm 843 */
 long BgL_arg1547z00_3100;
{ /* Llib/object.scm 843 */
 obj_t BgL_vectorz00_3101;
if(
VECTORP(BgL_oldzd2methodzd2arrayz00_1376))
{ /* Llib/object.scm 843 */
BgL_vectorz00_3101 = BgL_oldzd2methodzd2arrayz00_1376; }  else 
{ 
 obj_t BgL_auxz00_8999;
BgL_auxz00_8999 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36661L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2methodzd2arrayz00_1376); 
FAILURE(BgL_auxz00_8999,BFALSE,BFALSE);} 
BgL_arg1547z00_3100 = 
VECTOR_LENGTH(BgL_vectorz00_3101); } 
BgL_newzd2veczd2_3099 = 
BGl_extendzd2vectorzd2zz__objectz00(BgL_oldzd2methodzd2arrayz00_1376, BgL_defaultzd2bucketzd2_1375, BgL_arg1547z00_3100); } 
{ /* Llib/object.scm 844 */
 obj_t BgL_tmpz00_9005;
if(
VECTORP(BgL_oldzd2methodzd2arrayz00_1376))
{ /* Llib/object.scm 844 */
BgL_tmpz00_9005 = BgL_oldzd2methodzd2arrayz00_1376
; }  else 
{ 
 obj_t BgL_auxz00_9008;
BgL_auxz00_9008 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36707L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2methodzd2arrayz00_1376); 
FAILURE(BgL_auxz00_9008,BFALSE,BFALSE);} 
FREE_VECTOR_UNCOLLECTABLE(BgL_tmpz00_9005); } BUNSPEC; 
BgL_arg1552z00_1377 = BgL_newzd2veczd2_3099; } 
{ /* Llib/object.scm 859 */
 obj_t BgL_genericz00_3102;
if(
PROCEDUREP(BgL_genz00_1374))
{ /* Llib/object.scm 860 */
BgL_genericz00_3102 = BgL_genz00_1374; }  else 
{ 
 obj_t BgL_auxz00_9015;
BgL_auxz00_9015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(37386L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1374); 
FAILURE(BgL_auxz00_9015,BFALSE,BFALSE);} 
PROCEDURE_SET(BgL_genericz00_3102, 
(int)(1L), BgL_arg1552z00_1377); } } 
{ 
 long BgL_iz00_9021;
BgL_iz00_9021 = 
(BgL_iz00_1371+1L); 
BgL_iz00_1371 = BgL_iz00_9021; 
goto BgL_zc3z04anonymousza31548ze3z87_1372;} } } } }  else 
{ /* Llib/object.scm 855 */
return ((bool_t)0);} } } } 

}



/* double-nb-generics! */
obj_t BGl_doublezd2nbzd2genericsz12z12zz__objectz00(void)
{
{ /* Llib/object.scm 867 */
{ /* Llib/object.scm 868 */
 long BgL_za72za7_3105;
{ /* Llib/object.scm 868 */
 obj_t BgL_tmpz00_9023;
{ /* Llib/object.scm 868 */
 obj_t BgL_aux2868z00_5608;
BgL_aux2868z00_5608 = BGl_za2nbzd2genericszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2868z00_5608))
{ /* Llib/object.scm 868 */
BgL_tmpz00_9023 = BgL_aux2868z00_5608
; }  else 
{ 
 obj_t BgL_auxz00_9026;
BgL_auxz00_9026 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(37759L), BGl_string3526z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2868z00_5608); 
FAILURE(BgL_auxz00_9026,BFALSE,BFALSE);} } 
BgL_za72za7_3105 = 
(long)CINT(BgL_tmpz00_9023); } 
BGl_za2nbzd2genericszd2maxza2z00zz__objectz00 = 
BINT(
(2L*BgL_za72za7_3105)); } 
{ /* Llib/object.scm 869 */
 obj_t BgL_oldzd2veczd2_3106;
BgL_oldzd2veczd2_3106 = BGl_za2genericsza2z00zz__objectz00; 
{ /* Llib/object.scm 843 */
 obj_t BgL_newzd2veczd2_3107;
{ /* Llib/object.scm 843 */
 long BgL_arg1547z00_3108;
{ /* Llib/object.scm 843 */
 obj_t BgL_vectorz00_3109;
if(
VECTORP(BgL_oldzd2veczd2_3106))
{ /* Llib/object.scm 843 */
BgL_vectorz00_3109 = BgL_oldzd2veczd2_3106; }  else 
{ 
 obj_t BgL_auxz00_9035;
BgL_auxz00_9035 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36661L), BGl_string3526z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2veczd2_3106); 
FAILURE(BgL_auxz00_9035,BFALSE,BFALSE);} 
BgL_arg1547z00_3108 = 
VECTOR_LENGTH(BgL_vectorz00_3109); } 
BgL_newzd2veczd2_3107 = 
BGl_extendzd2vectorzd2zz__objectz00(BgL_oldzd2veczd2_3106, BFALSE, BgL_arg1547z00_3108); } 
{ /* Llib/object.scm 844 */
 obj_t BgL_tmpz00_9041;
if(
VECTORP(BgL_oldzd2veczd2_3106))
{ /* Llib/object.scm 844 */
BgL_tmpz00_9041 = BgL_oldzd2veczd2_3106
; }  else 
{ 
 obj_t BgL_auxz00_9044;
BgL_auxz00_9044 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(36707L), BGl_string3526z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_oldzd2veczd2_3106); 
FAILURE(BgL_auxz00_9044,BFALSE,BFALSE);} 
FREE_VECTOR_UNCOLLECTABLE(BgL_tmpz00_9041); } BUNSPEC; 
return ( 
BGl_za2genericsza2z00zz__objectz00 = BgL_newzd2veczd2_3107, BUNSPEC) ;} } } 

}



/* object? */
BGL_EXPORTED_DEF bool_t BGl_objectzf3zf3zz__objectz00(obj_t BgL_objz00_78)
{
{ /* Llib/object.scm 874 */
return 
BGL_OBJECTP(BgL_objz00_78);} 

}



/* &object? */
obj_t BGl_z62objectzf3z91zz__objectz00(obj_t BgL_envz00_4843, obj_t BgL_objz00_4844)
{
{ /* Llib/object.scm 874 */
return 
BBOOL(
BGl_objectzf3zf3zz__objectz00(BgL_objz00_4844));} 

}



/* object-class-num */
BGL_EXPORTED_DEF long BGl_objectzd2classzd2numz00zz__objectz00(BgL_objectz00_bglt BgL_objz00_79)
{
{ /* Llib/object.scm 880 */
return 
BGL_OBJECT_CLASS_NUM(BgL_objz00_79);} 

}



/* &object-class-num */
obj_t BGl_z62objectzd2classzd2numz62zz__objectz00(obj_t BgL_envz00_4845, obj_t BgL_objz00_4846)
{
{ /* Llib/object.scm 880 */
{ /* Llib/object.scm 881 */
 long BgL_tmpz00_9053;
{ /* Llib/object.scm 881 */
 BgL_objectz00_bglt BgL_auxz00_9054;
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4846, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 881 */
BgL_auxz00_9054 = 
((BgL_objectz00_bglt)BgL_objz00_4846)
; }  else 
{ 
 obj_t BgL_auxz00_9058;
BgL_auxz00_9058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(38375L), BGl_string3527z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4846); 
FAILURE(BgL_auxz00_9058,BFALSE,BFALSE);} 
BgL_tmpz00_9053 = 
BGl_objectzd2classzd2numz00zz__objectz00(BgL_auxz00_9054); } 
return 
BINT(BgL_tmpz00_9053);} } 

}



/* object-class-num-set! */
BGL_EXPORTED_DEF obj_t BGl_objectzd2classzd2numzd2setz12zc0zz__objectz00(BgL_objectz00_bglt BgL_objz00_80, long BgL_numz00_81)
{
{ /* Llib/object.scm 886 */
return 
BGL_OBJECT_CLASS_NUM_SET(BgL_objz00_80, BgL_numz00_81);} 

}



/* &object-class-num-set! */
obj_t BGl_z62objectzd2classzd2numzd2setz12za2zz__objectz00(obj_t BgL_envz00_4847, obj_t BgL_objz00_4848, obj_t BgL_numz00_4849)
{
{ /* Llib/object.scm 886 */
{ /* Llib/object.scm 887 */
 long BgL_auxz00_9073; BgL_objectz00_bglt BgL_auxz00_9065;
{ /* Llib/object.scm 887 */
 obj_t BgL_tmpz00_9074;
if(
INTEGERP(BgL_numz00_4849))
{ /* Llib/object.scm 887 */
BgL_tmpz00_9074 = BgL_numz00_4849
; }  else 
{ 
 obj_t BgL_auxz00_9077;
BgL_auxz00_9077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(38687L), BGl_string3529z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_numz00_4849); 
FAILURE(BgL_auxz00_9077,BFALSE,BFALSE);} 
BgL_auxz00_9073 = 
(long)CINT(BgL_tmpz00_9074); } 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4848, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 887 */
BgL_auxz00_9065 = 
((BgL_objectz00_bglt)BgL_objz00_4848)
; }  else 
{ 
 obj_t BgL_auxz00_9069;
BgL_auxz00_9069 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(38687L), BGl_string3529z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4848); 
FAILURE(BgL_auxz00_9069,BFALSE,BFALSE);} 
return 
BGl_objectzd2classzd2numzd2setz12zc0zz__objectz00(BgL_auxz00_9065, BgL_auxz00_9073);} } 

}



/* object-class */
BGL_EXPORTED_DEF obj_t BGl_objectzd2classzd2zz__objectz00(BgL_objectz00_bglt BgL_objectz00_82)
{
{ /* Llib/object.scm 892 */
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6458; long BgL_arg1555z00_6459;
BgL_arg1554z00_6458 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6460;
BgL_arg1556z00_6460 = 
BGL_OBJECT_CLASS_NUM(BgL_objectz00_82); 
BgL_arg1555z00_6459 = 
(BgL_arg1556z00_6460-OBJECT_TYPE); } 
return 
VECTOR_REF(BgL_arg1554z00_6458,BgL_arg1555z00_6459);} } 

}



/* &object-class */
obj_t BGl_z62objectzd2classzb0zz__objectz00(obj_t BgL_envz00_4850, obj_t BgL_objectz00_4851)
{
{ /* Llib/object.scm 892 */
{ /* Llib/object.scm 893 */
 BgL_objectz00_bglt BgL_auxz00_9087;
if(
BGl_isazf3zf3zz__objectz00(BgL_objectz00_4851, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 893 */
BgL_auxz00_9087 = 
((BgL_objectz00_bglt)BgL_objectz00_4851)
; }  else 
{ 
 obj_t BgL_auxz00_9091;
BgL_auxz00_9091 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39007L), BGl_string3530z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objectz00_4851); 
FAILURE(BgL_auxz00_9091,BFALSE,BFALSE);} 
return 
BGl_objectzd2classzd2zz__objectz00(BgL_auxz00_9087);} } 

}



/* generic-default */
BGL_EXPORTED_DEF obj_t BGl_genericzd2defaultzd2zz__objectz00(obj_t BgL_genericz00_83)
{
{ /* Llib/object.scm 899 */
{ /* Llib/object.scm 900 */
 obj_t BgL_aux2880z00_6461;
BgL_aux2880z00_6461 = 
PROCEDURE_REF(BgL_genericz00_83, 
(int)(0L)); 
if(
PROCEDUREP(BgL_aux2880z00_6461))
{ /* Llib/object.scm 900 */
return BgL_aux2880z00_6461;}  else 
{ 
 obj_t BgL_auxz00_9100;
BgL_auxz00_9100 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3531z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2880z00_6461); 
FAILURE(BgL_auxz00_9100,BFALSE,BFALSE);} } } 

}



/* &generic-default */
obj_t BGl_z62genericzd2defaultzb0zz__objectz00(obj_t BgL_envz00_4852, obj_t BgL_genericz00_4853)
{
{ /* Llib/object.scm 899 */
{ /* Llib/object.scm 900 */
 obj_t BgL_auxz00_9104;
if(
PROCEDUREP(BgL_genericz00_4853))
{ /* Llib/object.scm 900 */
BgL_auxz00_9104 = BgL_genericz00_4853
; }  else 
{ 
 obj_t BgL_auxz00_9107;
BgL_auxz00_9107 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3532z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4853); 
FAILURE(BgL_auxz00_9107,BFALSE,BFALSE);} 
return 
BGl_genericzd2defaultzd2zz__objectz00(BgL_auxz00_9104);} } 

}



/* generic-method-array */
BGL_EXPORTED_DEF obj_t BGl_genericzd2methodzd2arrayz00zz__objectz00(obj_t BgL_genericz00_86)
{
{ /* Llib/object.scm 908 */
return 
PROCEDURE_REF(BgL_genericz00_86, 
(int)(1L));} 

}



/* &generic-method-array */
obj_t BGl_z62genericzd2methodzd2arrayz62zz__objectz00(obj_t BgL_envz00_4854, obj_t BgL_genericz00_4855)
{
{ /* Llib/object.scm 908 */
{ /* Llib/object.scm 909 */
 obj_t BgL_auxz00_9114;
if(
PROCEDUREP(BgL_genericz00_4855))
{ /* Llib/object.scm 909 */
BgL_auxz00_9114 = BgL_genericz00_4855
; }  else 
{ 
 obj_t BgL_auxz00_9117;
BgL_auxz00_9117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39750L), BGl_string3533z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4855); 
FAILURE(BgL_auxz00_9117,BFALSE,BFALSE);} 
return 
BGl_genericzd2methodzd2arrayz00zz__objectz00(BgL_auxz00_9114);} } 

}



/* method-array-ref */
BGL_EXPORTED_DEF obj_t BGl_methodzd2arrayzd2refz00zz__objectz00(obj_t BgL_genericz00_92, obj_t BgL_arrayz00_93, int BgL_offsetz00_94)
{
{ /* Llib/object.scm 930 */
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_6462;
BgL_offsetz00_6462 = 
(
(long)(BgL_offsetz00_94)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_6463;
BgL_modz00_6463 = 
(BgL_offsetz00_6462 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_6464;
BgL_restz00_6464 = 
(BgL_offsetz00_6462 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_6465;
BgL_bucketz00_6465 = 
VECTOR_REF(BgL_arrayz00_93,BgL_modz00_6463); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_6466;
if(
VECTORP(BgL_bucketz00_6465))
{ /* Llib/object.scm 935 */
BgL_vectorz00_6466 = BgL_bucketz00_6465; }  else 
{ 
 obj_t BgL_auxz00_9141;
BgL_auxz00_9141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3534z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_6465); 
FAILURE(BgL_auxz00_9141,BFALSE,BFALSE);} 
return 
VECTOR_REF(BgL_vectorz00_6466,BgL_restz00_6464);} } } } } } } 

}



/* &method-array-ref */
obj_t BGl_z62methodzd2arrayzd2refz62zz__objectz00(obj_t BgL_envz00_4856, obj_t BgL_genericz00_4857, obj_t BgL_arrayz00_4858, obj_t BgL_offsetz00_4859)
{
{ /* Llib/object.scm 930 */
{ /* Llib/object.scm 931 */
 int BgL_auxz00_9160; obj_t BgL_auxz00_9153; obj_t BgL_auxz00_9146;
{ /* Llib/object.scm 931 */
 obj_t BgL_tmpz00_9161;
if(
INTEGERP(BgL_offsetz00_4859))
{ /* Llib/object.scm 931 */
BgL_tmpz00_9161 = BgL_offsetz00_4859
; }  else 
{ 
 obj_t BgL_auxz00_9164;
BgL_auxz00_9164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40903L), BGl_string3535z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_offsetz00_4859); 
FAILURE(BgL_auxz00_9164,BFALSE,BFALSE);} 
BgL_auxz00_9160 = 
CINT(BgL_tmpz00_9161); } 
if(
VECTORP(BgL_arrayz00_4858))
{ /* Llib/object.scm 931 */
BgL_auxz00_9153 = BgL_arrayz00_4858
; }  else 
{ 
 obj_t BgL_auxz00_9156;
BgL_auxz00_9156 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40903L), BGl_string3535z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arrayz00_4858); 
FAILURE(BgL_auxz00_9156,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_genericz00_4857))
{ /* Llib/object.scm 931 */
BgL_auxz00_9146 = BgL_genericz00_4857
; }  else 
{ 
 obj_t BgL_auxz00_9149;
BgL_auxz00_9149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40903L), BGl_string3535z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4857); 
FAILURE(BgL_auxz00_9149,BFALSE,BFALSE);} 
return 
BGl_methodzd2arrayzd2refz00zz__objectz00(BgL_auxz00_9146, BgL_auxz00_9153, BgL_auxz00_9160);} } 

}



/* method-array-set! */
obj_t BGl_methodzd2arrayzd2setz12z12zz__objectz00(obj_t BgL_genericz00_95, obj_t BgL_arrayz00_96, long BgL_offsetz00_97, obj_t BgL_methodz00_98)
{
{ /* Llib/object.scm 940 */
{ /* Llib/object.scm 941 */
 long BgL_offsetz00_1389;
BgL_offsetz00_1389 = 
(BgL_offsetz00_97-OBJECT_TYPE); 
{ /* Llib/object.scm 941 */
 long BgL_modz00_1390;
{ /* Llib/object.scm 944 */
 uint32_t BgL_arg1580z00_1410;
{ /* Llib/object.scm 944 */
 uint32_t BgL_arg1582z00_1411; uint32_t BgL_arg1583z00_1412;
BgL_arg1582z00_1411 = 
(uint32_t)(BgL_offsetz00_1389); 
{ /* Llib/object.scm 945 */
 int BgL_arg1584z00_1413;
BgL_arg1584z00_1413 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
{ /* Llib/object.scm 945 */
 long BgL_tmpz00_9177;
BgL_tmpz00_9177 = 
(long)(BgL_arg1584z00_1413); 
BgL_arg1583z00_1412 = 
(uint32_t)(BgL_tmpz00_9177); } } 
{ /* Llib/object.scm 943 */
 int32_t BgL_tmpz00_9180;
BgL_tmpz00_9180 = 
(int32_t)(BgL_arg1583z00_1412); 
BgL_arg1580z00_1410 = 
(BgL_arg1582z00_1411/BgL_tmpz00_9180); } } 
BgL_modz00_1390 = 
(long)(BgL_arg1580z00_1410); } 
{ /* Llib/object.scm 942 */
 long BgL_restz00_1391;
{ /* Llib/object.scm 948 */
 uint32_t BgL_arg1575z00_1406;
{ /* Llib/object.scm 948 */
 uint32_t BgL_arg1576z00_1407; uint32_t BgL_arg1578z00_1408;
BgL_arg1576z00_1407 = 
(uint32_t)(BgL_offsetz00_1389); 
{ /* Llib/object.scm 949 */
 int BgL_arg1579z00_1409;
BgL_arg1579z00_1409 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
{ /* Llib/object.scm 949 */
 long BgL_tmpz00_9190;
BgL_tmpz00_9190 = 
(long)(BgL_arg1579z00_1409); 
BgL_arg1578z00_1408 = 
(uint32_t)(BgL_tmpz00_9190); } } 
{ /* Llib/object.scm 947 */
 int32_t BgL_tmpz00_9193;
BgL_tmpz00_9193 = 
(int32_t)(BgL_arg1578z00_1408); 
BgL_arg1575z00_1406 = 
(BgL_arg1576z00_1407%BgL_tmpz00_9193); } } 
BgL_restz00_1391 = 
(long)(BgL_arg1575z00_1406); } 
{ /* Llib/object.scm 946 */

{ /* Llib/object.scm 950 */
 obj_t BgL_bucketz00_1392;
{ /* Llib/object.scm 950 */
 obj_t BgL_vectorz00_3162;
if(
VECTORP(BgL_arrayz00_96))
{ /* Llib/object.scm 950 */
BgL_vectorz00_3162 = BgL_arrayz00_96; }  else 
{ 
 obj_t BgL_auxz00_9199;
BgL_auxz00_9199 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41772L), BGl_string3536z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arrayz00_96); 
FAILURE(BgL_auxz00_9199,BFALSE,BFALSE);} 
BgL_bucketz00_1392 = 
VECTOR_REF(BgL_vectorz00_3162,BgL_modz00_1390); } 
{ /* Llib/object.scm 951 */
 bool_t BgL_test4614z00_9204;
{ /* Llib/object.scm 951 */
 bool_t BgL_test4615z00_9205;
{ /* Llib/object.scm 951 */
 obj_t BgL_tmpz00_9206;
{ /* Llib/object.scm 951 */
 obj_t BgL_res2569z00_3165;
{ /* Llib/object.scm 951 */
 obj_t BgL_genericz00_3164;
if(
PROCEDUREP(BgL_genericz00_95))
{ /* Llib/object.scm 951 */
BgL_genericz00_3164 = BgL_genericz00_95; }  else 
{ 
 obj_t BgL_auxz00_9209;
BgL_auxz00_9209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41824L), BGl_string3536z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_95); 
FAILURE(BgL_auxz00_9209,BFALSE,BFALSE);} 
{ /* Llib/object.scm 900 */
 obj_t BgL_aux2897z00_5643;
BgL_aux2897z00_5643 = 
PROCEDURE_REF(BgL_genericz00_3164, 
(int)(0L)); 
if(
PROCEDUREP(BgL_aux2897z00_5643))
{ /* Llib/object.scm 900 */
BgL_res2569z00_3165 = BgL_aux2897z00_5643; }  else 
{ 
 obj_t BgL_auxz00_9217;
BgL_auxz00_9217 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3536z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux2897z00_5643); 
FAILURE(BgL_auxz00_9217,BFALSE,BFALSE);} } } 
BgL_tmpz00_9206 = BgL_res2569z00_3165; } 
BgL_test4615z00_9205 = 
(BgL_methodz00_98==BgL_tmpz00_9206); } 
if(BgL_test4615z00_9205)
{ /* Llib/object.scm 951 */
BgL_test4614z00_9204 = ((bool_t)1)
; }  else 
{ /* Llib/object.scm 952 */
 bool_t BgL_test4618z00_9222;
{ /* Llib/object.scm 952 */
 obj_t BgL_tmpz00_9223;
{ /* Llib/object.scm 952 */
 obj_t BgL_res2570z00_3166;
{ /* Llib/object.scm 918 */
 obj_t BgL_aux2901z00_5647;
{ /* Llib/object.scm 918 */
 obj_t BgL_tmpz00_9224;
if(
PROCEDUREP(BgL_genericz00_95))
{ /* Llib/object.scm 918 */
BgL_tmpz00_9224 = BgL_genericz00_95
; }  else 
{ 
 obj_t BgL_auxz00_9227;
BgL_auxz00_9227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40183L), BGl_string3536z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_95); 
FAILURE(BgL_auxz00_9227,BFALSE,BFALSE);} 
BgL_aux2901z00_5647 = 
PROCEDURE_REF(BgL_tmpz00_9224, 
(int)(2L)); } 
if(
VECTORP(BgL_aux2901z00_5647))
{ /* Llib/object.scm 918 */
BgL_res2570z00_3166 = BgL_aux2901z00_5647; }  else 
{ 
 obj_t BgL_auxz00_9235;
BgL_auxz00_9235 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40168L), BGl_string3536z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2901z00_5647); 
FAILURE(BgL_auxz00_9235,BFALSE,BFALSE);} } 
BgL_tmpz00_9223 = BgL_res2570z00_3166; } 
BgL_test4618z00_9222 = 
(BgL_bucketz00_1392==BgL_tmpz00_9223); } 
if(BgL_test4618z00_9222)
{ /* Llib/object.scm 952 */
BgL_test4614z00_9204 = ((bool_t)0)
; }  else 
{ /* Llib/object.scm 952 */
BgL_test4614z00_9204 = ((bool_t)1)
; } } } 
if(BgL_test4614z00_9204)
{ /* Llib/object.scm 954 */
 obj_t BgL_vectorz00_3167;
if(
VECTORP(BgL_bucketz00_1392))
{ /* Llib/object.scm 954 */
BgL_vectorz00_3167 = BgL_bucketz00_1392; }  else 
{ 
 obj_t BgL_auxz00_9242;
BgL_auxz00_9242 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41954L), BGl_string3536z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_1392); 
FAILURE(BgL_auxz00_9242,BFALSE,BFALSE);} 
return 
VECTOR_SET(BgL_vectorz00_3167,BgL_restz00_1391,BgL_methodz00_98);}  else 
{ /* Llib/object.scm 956 */
 obj_t BgL_bucketz00_1400;
{ /* Llib/object.scm 956 */
 int BgL_arg1567z00_1401;
BgL_arg1567z00_1401 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
{ /* Llib/object.scm 956 */
 obj_t BgL_auxz00_9252;
if(
VECTORP(BgL_bucketz00_1392))
{ /* Llib/object.scm 956 */
BgL_auxz00_9252 = BgL_bucketz00_1392
; }  else 
{ 
 obj_t BgL_auxz00_9255;
BgL_auxz00_9255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42047L), BGl_string3536z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_1392); 
FAILURE(BgL_auxz00_9255,BFALSE,BFALSE);} 
BgL_bucketz00_1400 = 
BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(BgL_auxz00_9252, 
(long)(BgL_arg1567z00_1401)); } } 
VECTOR_SET(BgL_bucketz00_1400,BgL_restz00_1391,BgL_methodz00_98); 
{ /* Llib/object.scm 958 */
 obj_t BgL_vectorz00_3175;
if(
VECTORP(BgL_arrayz00_96))
{ /* Llib/object.scm 958 */
BgL_vectorz00_3175 = BgL_arrayz00_96; }  else 
{ 
 obj_t BgL_auxz00_9264;
BgL_auxz00_9264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42142L), BGl_string3536z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arrayz00_96); 
FAILURE(BgL_auxz00_9264,BFALSE,BFALSE);} 
return 
VECTOR_SET(BgL_vectorz00_3175,BgL_modz00_1390,BgL_bucketz00_1400);} } } } } } } } } 

}



/* generic-memory-statistics */
BGL_EXPORTED_DEF obj_t BGl_genericzd2memoryzd2statisticsz00zz__objectz00(void)
{
{ /* Llib/object.scm 963 */
{ /* Llib/object.scm 965 */
 obj_t BgL_top4625z00_9270;
BgL_top4625z00_9270 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top4625z00_9270, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 965 */
 obj_t BgL_tmp4624z00_9269;
{ 
 long BgL_gz00_1415; long BgL_siza7eza7_1416;
BgL_gz00_1415 = 0L; 
BgL_siza7eza7_1416 = 0L; 
BgL_zc3z04anonymousza31585ze3z87_1417:
{ /* Llib/object.scm 968 */
 bool_t BgL_test4626z00_9274;
{ /* Llib/object.scm 968 */
 long BgL_n2z00_3178;
{ /* Llib/object.scm 968 */
 obj_t BgL_tmpz00_9275;
{ /* Llib/object.scm 968 */
 obj_t BgL_aux2909z00_5655;
BgL_aux2909z00_5655 = BGl_za2nbzd2genericsza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2909z00_5655))
{ /* Llib/object.scm 968 */
BgL_tmpz00_9275 = BgL_aux2909z00_5655
; }  else 
{ 
 obj_t BgL_auxz00_9278;
BgL_auxz00_9278 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42579L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2909z00_5655); 
FAILURE(BgL_auxz00_9278,BFALSE,BFALSE);} } 
BgL_n2z00_3178 = 
(long)CINT(BgL_tmpz00_9275); } 
BgL_test4626z00_9274 = 
(BgL_gz00_1415==BgL_n2z00_3178); } 
if(BgL_test4626z00_9274)
{ /* Llib/object.scm 969 */
 obj_t BgL_arg1587z00_1419; obj_t BgL_arg1589z00_1420;
{ /* Llib/object.scm 969 */
 obj_t BgL_arg1591z00_1421;
BgL_arg1591z00_1421 = 
MAKE_YOUNG_PAIR(BGl_za2nbzd2genericsza2zd2zz__objectz00, BNIL); 
BgL_arg1587z00_1419 = 
MAKE_YOUNG_PAIR(BGl_symbol3537z00zz__objectz00, BgL_arg1591z00_1421); } 
{ /* Llib/object.scm 970 */
 obj_t BgL_arg1593z00_1422; obj_t BgL_arg1594z00_1423;
{ /* Llib/object.scm 970 */
 obj_t BgL_arg1595z00_1424;
BgL_arg1595z00_1424 = 
MAKE_YOUNG_PAIR(BGl_za2nbzd2classesza2zd2zz__objectz00, BNIL); 
BgL_arg1593z00_1422 = 
MAKE_YOUNG_PAIR(BGl_symbol3539z00zz__objectz00, BgL_arg1595z00_1424); } 
{ /* Llib/object.scm 971 */
 obj_t BgL_arg1598z00_1425; obj_t BgL_arg1601z00_1426;
{ /* Llib/object.scm 971 */
 obj_t BgL_arg1602z00_1427;
BgL_arg1602z00_1427 = 
MAKE_YOUNG_PAIR(
BINT(BgL_siza7eza7_1416), BNIL); 
BgL_arg1598z00_1425 = 
MAKE_YOUNG_PAIR(BGl_symbol3540z00zz__objectz00, BgL_arg1602z00_1427); } 
{ /* Llib/object.scm 974 */
 obj_t BgL_arg1603z00_1428; obj_t BgL_arg1605z00_1429;
{ /* Llib/object.scm 974 */
 obj_t BgL_arg1606z00_1430;
{ /* Llib/object.scm 974 */
 long BgL_arg1607z00_1431;
{ /* Llib/object.scm 974 */
 obj_t BgL_arg1608z00_1432;
{ /* Llib/object.scm 974 */
 obj_t BgL_arg1609z00_1433;
{ /* Llib/object.scm 974 */
 obj_t BgL_vectorz00_3179;
{ /* Llib/object.scm 974 */
 obj_t BgL_aux2910z00_5656;
BgL_aux2910z00_5656 = BGl_za2genericsza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2910z00_5656))
{ /* Llib/object.scm 974 */
BgL_vectorz00_3179 = BgL_aux2910z00_5656; }  else 
{ 
 obj_t BgL_auxz00_9293;
BgL_auxz00_9293 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42781L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2910z00_5656); 
FAILURE(BgL_auxz00_9293,BFALSE,BFALSE);} } 
BgL_arg1609z00_1433 = 
VECTOR_REF(BgL_vectorz00_3179,0L); } 
{ /* Llib/object.scm 973 */
 obj_t BgL_genericz00_3180;
if(
PROCEDUREP(BgL_arg1609z00_1433))
{ /* Llib/object.scm 974 */
BgL_genericz00_3180 = BgL_arg1609z00_1433; }  else 
{ 
 obj_t BgL_auxz00_9300;
BgL_auxz00_9300 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42793L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_arg1609z00_1433); 
FAILURE(BgL_auxz00_9300,BFALSE,BFALSE);} 
BgL_arg1608z00_1432 = 
PROCEDURE_REF(BgL_genericz00_3180, 
(int)(1L)); } } 
{ /* Llib/object.scm 972 */
 obj_t BgL_vectorz00_3181;
if(
VECTORP(BgL_arg1608z00_1432))
{ /* Llib/object.scm 974 */
BgL_vectorz00_3181 = BgL_arg1608z00_1432; }  else 
{ 
 obj_t BgL_auxz00_9308;
BgL_auxz00_9308 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42794L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arg1608z00_1432); 
FAILURE(BgL_auxz00_9308,BFALSE,BFALSE);} 
BgL_arg1607z00_1431 = 
VECTOR_LENGTH(BgL_vectorz00_3181); } } 
BgL_arg1606z00_1430 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1607z00_1431), BNIL); } 
BgL_arg1603z00_1428 = 
MAKE_YOUNG_PAIR(BGl_symbol3542z00zz__objectz00, BgL_arg1606z00_1430); } 
{ /* Llib/object.scm 975 */
 obj_t BgL_arg1610z00_1434; obj_t BgL_arg1611z00_1435;
{ /* Llib/object.scm 975 */
 obj_t BgL_arg1612z00_1436;
{ /* Llib/object.scm 975 */
 int BgL_arg1613z00_1437;
BgL_arg1613z00_1437 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
BgL_arg1612z00_1436 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1613z00_1437), BNIL); } 
BgL_arg1610z00_1434 = 
MAKE_YOUNG_PAIR(BGl_symbol3544z00zz__objectz00, BgL_arg1612z00_1436); } 
{ /* Llib/object.scm 976 */
 obj_t BgL_arg1615z00_1438; obj_t BgL_arg1616z00_1439;
{ /* Llib/object.scm 976 */
 obj_t BgL_arg1617z00_1440;
BgL_arg1617z00_1440 = 
MAKE_YOUNG_PAIR(BGl_za2nbzd2classeszd2maxza2z00zz__objectz00, BNIL); 
BgL_arg1615z00_1438 = 
MAKE_YOUNG_PAIR(BGl_symbol3546z00zz__objectz00, BgL_arg1617z00_1440); } 
{ /* Llib/object.scm 977 */
 obj_t BgL_arg1618z00_1441;
{ /* Llib/object.scm 977 */
 obj_t BgL_arg1619z00_1442;
BgL_arg1619z00_1442 = 
MAKE_YOUNG_PAIR(BGl_za2nbzd2genericszd2maxza2z00zz__objectz00, BNIL); 
BgL_arg1618z00_1441 = 
MAKE_YOUNG_PAIR(BGl_symbol3548z00zz__objectz00, BgL_arg1619z00_1442); } 
BgL_arg1616z00_1439 = 
MAKE_YOUNG_PAIR(BgL_arg1618z00_1441, BNIL); } 
BgL_arg1611z00_1435 = 
MAKE_YOUNG_PAIR(BgL_arg1615z00_1438, BgL_arg1616z00_1439); } 
BgL_arg1605z00_1429 = 
MAKE_YOUNG_PAIR(BgL_arg1610z00_1434, BgL_arg1611z00_1435); } 
BgL_arg1601z00_1426 = 
MAKE_YOUNG_PAIR(BgL_arg1603z00_1428, BgL_arg1605z00_1429); } 
BgL_arg1594z00_1423 = 
MAKE_YOUNG_PAIR(BgL_arg1598z00_1425, BgL_arg1601z00_1426); } 
BgL_arg1589z00_1420 = 
MAKE_YOUNG_PAIR(BgL_arg1593z00_1422, BgL_arg1594z00_1423); } 
BgL_tmp4624z00_9269 = 
MAKE_YOUNG_PAIR(BgL_arg1587z00_1419, BgL_arg1589z00_1420); }  else 
{ /* Llib/object.scm 978 */
 obj_t BgL_genz00_1443;
{ /* Llib/object.scm 978 */
 obj_t BgL_vectorz00_3186;
{ /* Llib/object.scm 978 */
 obj_t BgL_aux2916z00_5662;
BgL_aux2916z00_5662 = BGl_za2genericsza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2916z00_5662))
{ /* Llib/object.scm 978 */
BgL_vectorz00_3186 = BgL_aux2916z00_5662; }  else 
{ 
 obj_t BgL_auxz00_9337;
BgL_auxz00_9337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(42971L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2916z00_5662); 
FAILURE(BgL_auxz00_9337,BFALSE,BFALSE);} } 
BgL_genz00_1443 = 
VECTOR_REF(BgL_vectorz00_3186,BgL_gz00_1415); } 
{ /* Llib/object.scm 978 */
 obj_t BgL_dbuckz00_1444;
{ /* Llib/object.scm 979 */
 obj_t BgL_res2575z00_3188;
{ /* Llib/object.scm 918 */
 obj_t BgL_aux2920z00_5666;
{ /* Llib/object.scm 918 */
 obj_t BgL_tmpz00_9342;
if(
PROCEDUREP(BgL_genz00_1443))
{ /* Llib/object.scm 918 */
BgL_tmpz00_9342 = BgL_genz00_1443
; }  else 
{ 
 obj_t BgL_auxz00_9345;
BgL_auxz00_9345 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40183L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1443); 
FAILURE(BgL_auxz00_9345,BFALSE,BFALSE);} 
BgL_aux2920z00_5666 = 
PROCEDURE_REF(BgL_tmpz00_9342, 
(int)(2L)); } 
if(
VECTORP(BgL_aux2920z00_5666))
{ /* Llib/object.scm 918 */
BgL_res2575z00_3188 = BgL_aux2920z00_5666; }  else 
{ 
 obj_t BgL_auxz00_9353;
BgL_auxz00_9353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40168L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2920z00_5666); 
FAILURE(BgL_auxz00_9353,BFALSE,BFALSE);} } 
BgL_dbuckz00_1444 = BgL_res2575z00_3188; } 
{ /* Llib/object.scm 979 */
 long BgL_dza7za7_1445;
BgL_dza7za7_1445 = 0L; 
{ /* Llib/object.scm 980 */
 obj_t BgL_sza7za7_1446;
{ /* Llib/object.scm 981 */
 obj_t BgL_runner1637z00_1471;
{ /* Llib/object.scm 981 */
 obj_t BgL_l1317z00_1454;
{ /* Llib/object.scm 988 */
 obj_t BgL_arg1636z00_1470;
{ /* Llib/object.scm 988 */
 obj_t BgL_genericz00_3189;
if(
PROCEDUREP(BgL_genz00_1443))
{ /* Llib/object.scm 988 */
BgL_genericz00_3189 = BgL_genz00_1443; }  else 
{ 
 obj_t BgL_auxz00_9359;
BgL_auxz00_9359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43271L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1443); 
FAILURE(BgL_auxz00_9359,BFALSE,BFALSE);} 
BgL_arg1636z00_1470 = 
PROCEDURE_REF(BgL_genericz00_3189, 
(int)(1L)); } 
{ /* Llib/object.scm 988 */
 obj_t BgL_auxz00_9365;
if(
VECTORP(BgL_arg1636z00_1470))
{ /* Llib/object.scm 988 */
BgL_auxz00_9365 = BgL_arg1636z00_1470
; }  else 
{ 
 obj_t BgL_auxz00_9368;
BgL_auxz00_9368 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43274L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arg1636z00_1470); 
FAILURE(BgL_auxz00_9368,BFALSE,BFALSE);} 
BgL_l1317z00_1454 = 
BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_auxz00_9365); } } 
if(
NULLP(BgL_l1317z00_1454))
{ /* Llib/object.scm 981 */
BgL_runner1637z00_1471 = BNIL; }  else 
{ /* Llib/object.scm 981 */
 obj_t BgL_head1319z00_1456;
BgL_head1319z00_1456 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1317z00_1458; obj_t BgL_tail1320z00_1459;
BgL_l1317z00_1458 = BgL_l1317z00_1454; 
BgL_tail1320z00_1459 = BgL_head1319z00_1456; 
BgL_zc3z04anonymousza31627ze3z87_1460:
if(
PAIRP(BgL_l1317z00_1458))
{ /* Llib/object.scm 981 */
 obj_t BgL_newtail1321z00_1462;
{ /* Llib/object.scm 981 */
 long BgL_arg1630z00_1464;
{ /* Llib/object.scm 981 */
 obj_t BgL_bz00_1465;
BgL_bz00_1465 = 
CAR(BgL_l1317z00_1458); 
if(
(BgL_bz00_1465==BgL_dbuckz00_1444))
{ /* Llib/object.scm 983 */
{ /* Llib/object.scm 984 */
 long BgL_arg1631z00_1466;
{ /* Llib/object.scm 984 */
 obj_t BgL_vectorz00_3191;
if(
VECTORP(BgL_bz00_1465))
{ /* Llib/object.scm 984 */
BgL_vectorz00_3191 = BgL_bz00_1465; }  else 
{ 
 obj_t BgL_auxz00_9383;
BgL_auxz00_9383 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43153L), BGl_string3550z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bz00_1465); 
FAILURE(BgL_auxz00_9383,BFALSE,BFALSE);} 
BgL_arg1631z00_1466 = 
VECTOR_LENGTH(BgL_vectorz00_3191); } 
BgL_dza7za7_1445 = 
(BgL_arg1631z00_1466*4L); } 
BgL_arg1630z00_1464 = 0L; }  else 
{ /* Llib/object.scm 987 */
 long BgL_arg1634z00_1467;
{ /* Llib/object.scm 987 */
 obj_t BgL_vectorz00_3193;
if(
VECTORP(BgL_bz00_1465))
{ /* Llib/object.scm 987 */
BgL_vectorz00_3193 = BgL_bz00_1465; }  else 
{ 
 obj_t BgL_auxz00_9391;
BgL_auxz00_9391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43220L), BGl_string3550z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bz00_1465); 
FAILURE(BgL_auxz00_9391,BFALSE,BFALSE);} 
BgL_arg1634z00_1467 = 
VECTOR_LENGTH(BgL_vectorz00_3193); } 
BgL_arg1630z00_1464 = 
(4L*BgL_arg1634z00_1467); } } 
BgL_newtail1321z00_1462 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1630z00_1464), BNIL); } 
SET_CDR(BgL_tail1320z00_1459, BgL_newtail1321z00_1462); 
{ 
 obj_t BgL_tail1320z00_9402; obj_t BgL_l1317z00_9400;
BgL_l1317z00_9400 = 
CDR(BgL_l1317z00_1458); 
BgL_tail1320z00_9402 = BgL_newtail1321z00_1462; 
BgL_tail1320z00_1459 = BgL_tail1320z00_9402; 
BgL_l1317z00_1458 = BgL_l1317z00_9400; 
goto BgL_zc3z04anonymousza31627ze3z87_1460;} }  else 
{ /* Llib/object.scm 981 */
if(
NULLP(BgL_l1317z00_1458))
{ /* Llib/object.scm 981 */
BgL_runner1637z00_1471 = 
CDR(BgL_head1319z00_1456); }  else 
{ /* Llib/object.scm 981 */
BgL_runner1637z00_1471 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3551z00zz__objectz00, BGl_string3552z00zz__objectz00, BgL_l1317z00_1458, BGl_string3441z00zz__objectz00, 
BINT(43061L)); } } } } } 
BgL_sza7za7_1446 = 
BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner1637z00_1471); } 
{ /* Llib/object.scm 981 */
 long BgL_bza7za7_1447;
{ /* Llib/object.scm 989 */
 long BgL_arg1624z00_1452;
{ /* Llib/object.scm 989 */
 obj_t BgL_arg1625z00_1453;
{ /* Llib/object.scm 989 */
 obj_t BgL_genericz00_3198;
if(
PROCEDUREP(BgL_genz00_1443))
{ /* Llib/object.scm 989 */
BgL_genericz00_3198 = BgL_genz00_1443; }  else 
{ 
 obj_t BgL_auxz00_9411;
BgL_auxz00_9411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43334L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1443); 
FAILURE(BgL_auxz00_9411,BFALSE,BFALSE);} 
BgL_arg1625z00_1453 = 
PROCEDURE_REF(BgL_genericz00_3198, 
(int)(1L)); } 
{ /* Llib/object.scm 989 */
 obj_t BgL_vectorz00_3199;
if(
VECTORP(BgL_arg1625z00_1453))
{ /* Llib/object.scm 989 */
BgL_vectorz00_3199 = BgL_arg1625z00_1453; }  else 
{ 
 obj_t BgL_auxz00_9419;
BgL_auxz00_9419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43337L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arg1625z00_1453); 
FAILURE(BgL_auxz00_9419,BFALSE,BFALSE);} 
BgL_arg1624z00_1452 = 
VECTOR_LENGTH(BgL_vectorz00_3199); } } 
BgL_bza7za7_1447 = 
(4L*BgL_arg1624z00_1452); } 
{ /* Llib/object.scm 989 */

{ /* Llib/object.scm 990 */
 long BgL_arg1620z00_1448; long BgL_arg1621z00_1449;
BgL_arg1620z00_1448 = 
(BgL_gz00_1415+1L); 
{ /* Llib/object.scm 991 */
 long BgL_arg1622z00_1450;
{ /* Llib/object.scm 991 */
 long BgL_za72za7_3205;
{ /* Llib/object.scm 991 */
 obj_t BgL_tmpz00_9426;
if(
INTEGERP(BgL_sza7za7_1446))
{ /* Llib/object.scm 991 */
BgL_tmpz00_9426 = BgL_sza7za7_1446
; }  else 
{ 
 obj_t BgL_auxz00_9429;
BgL_auxz00_9429 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43393L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_sza7za7_1446); 
FAILURE(BgL_auxz00_9429,BFALSE,BFALSE);} 
BgL_za72za7_3205 = 
(long)CINT(BgL_tmpz00_9426); } 
BgL_arg1622z00_1450 = 
(
(BgL_bza7za7_1447+BgL_dza7za7_1445)+BgL_za72za7_3205); } 
BgL_arg1621z00_1449 = 
(BgL_siza7eza7_1416+BgL_arg1622z00_1450); } 
{ 
 long BgL_siza7eza7_9438; long BgL_gz00_9437;
BgL_gz00_9437 = BgL_arg1620z00_1448; 
BgL_siza7eza7_9438 = BgL_arg1621z00_1449; 
BgL_siza7eza7_1416 = BgL_siza7eza7_9438; 
BgL_gz00_1415 = BgL_gz00_9437; 
goto BgL_zc3z04anonymousza31585ze3z87_1417;} } } } } } } } } } 
BGL_EXITD_POP_PROTECT(BgL_top4625z00_9270); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); 
return BgL_tmp4624z00_9269;} } } 

}



/* &generic-memory-statistics */
obj_t BGl_z62genericzd2memoryzd2statisticsz62zz__objectz00(obj_t BgL_envz00_4860)
{
{ /* Llib/object.scm 963 */
return 
BGl_genericzd2memoryzd2statisticsz00zz__objectz00();} 

}



/* generics-add-class! */
bool_t BGl_genericszd2addzd2classz12z12zz__objectz00(long BgL_classzd2idxzd2_99, long BgL_superzd2idxzd2_100)
{
{ /* Llib/object.scm 998 */
{ 
 long BgL_gz00_1474;
BgL_gz00_1474 = 0L; 
BgL_zc3z04anonymousza31638ze3z87_1475:
{ /* Llib/object.scm 1000 */
 bool_t BgL_test4645z00_9442;
{ /* Llib/object.scm 1000 */
 long BgL_n2z00_3209;
{ /* Llib/object.scm 1000 */
 obj_t BgL_tmpz00_9443;
{ /* Llib/object.scm 1000 */
 obj_t BgL_aux2935z00_5681;
BgL_aux2935z00_5681 = BGl_za2nbzd2genericsza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2935z00_5681))
{ /* Llib/object.scm 1000 */
BgL_tmpz00_9443 = BgL_aux2935z00_5681
; }  else 
{ 
 obj_t BgL_auxz00_9446;
BgL_auxz00_9446 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43868L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2935z00_5681); 
FAILURE(BgL_auxz00_9446,BFALSE,BFALSE);} } 
BgL_n2z00_3209 = 
(long)CINT(BgL_tmpz00_9443); } 
BgL_test4645z00_9442 = 
(BgL_gz00_1474<BgL_n2z00_3209); } 
if(BgL_test4645z00_9442)
{ /* Llib/object.scm 1001 */
 obj_t BgL_genz00_1477;
{ /* Llib/object.scm 1001 */
 obj_t BgL_vectorz00_3210;
{ /* Llib/object.scm 1001 */
 obj_t BgL_aux2936z00_5682;
BgL_aux2936z00_5682 = BGl_za2genericsza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2936z00_5682))
{ /* Llib/object.scm 1001 */
BgL_vectorz00_3210 = BgL_aux2936z00_5682; }  else 
{ 
 obj_t BgL_auxz00_9454;
BgL_auxz00_9454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43912L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2936z00_5682); 
FAILURE(BgL_auxz00_9454,BFALSE,BFALSE);} } 
BgL_genz00_1477 = 
VECTOR_REF(BgL_vectorz00_3210,BgL_gz00_1474); } 
{ /* Llib/object.scm 1001 */
 obj_t BgL_methodzd2arrayzd2_1478;
{ /* Llib/object.scm 1002 */
 obj_t BgL_genericz00_3212;
if(
PROCEDUREP(BgL_genz00_1477))
{ /* Llib/object.scm 1002 */
BgL_genericz00_3212 = BgL_genz00_1477; }  else 
{ 
 obj_t BgL_auxz00_9461;
BgL_auxz00_9461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(43965L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genz00_1477); 
FAILURE(BgL_auxz00_9461,BFALSE,BFALSE);} 
BgL_methodzd2arrayzd2_1478 = 
PROCEDURE_REF(BgL_genericz00_3212, 
(int)(1L)); } 
{ /* Llib/object.scm 1002 */
 obj_t BgL_methodz00_1479;
{ /* Llib/object.scm 1003 */
 obj_t BgL_arrayz00_3214; int BgL_offsetz00_3215;
if(
VECTORP(BgL_methodzd2arrayzd2_1478))
{ /* Llib/object.scm 1003 */
BgL_arrayz00_3214 = BgL_methodzd2arrayzd2_1478; }  else 
{ 
 obj_t BgL_auxz00_9469;
BgL_auxz00_9469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44003L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_methodzd2arrayzd2_1478); 
FAILURE(BgL_auxz00_9469,BFALSE,BFALSE);} 
BgL_offsetz00_3215 = 
(int)(BgL_superzd2idxzd2_100); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_3216;
BgL_offsetz00_3216 = 
(
(long)(BgL_offsetz00_3215)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_3217;
BgL_modz00_3217 = 
(BgL_offsetz00_3216 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_3219;
BgL_restz00_3219 = 
(BgL_offsetz00_3216 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_3221;
BgL_bucketz00_3221 = 
VECTOR_REF(BgL_arrayz00_3214,BgL_modz00_3217); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_3238;
if(
VECTORP(BgL_bucketz00_3221))
{ /* Llib/object.scm 935 */
BgL_vectorz00_3238 = BgL_bucketz00_3221; }  else 
{ 
 obj_t BgL_auxz00_9493;
BgL_auxz00_9493 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_3221); 
FAILURE(BgL_auxz00_9493,BFALSE,BFALSE);} 
BgL_methodz00_1479 = 
VECTOR_REF(BgL_vectorz00_3238,BgL_restz00_3219); } } } } } } } 
{ /* Llib/object.scm 1003 */

BGl_methodzd2arrayzd2setz12z12zz__objectz00(BgL_genz00_1477, BgL_methodzd2arrayzd2_1478, BgL_classzd2idxzd2_99, BgL_methodz00_1479); 
{ 
 long BgL_gz00_9499;
BgL_gz00_9499 = 
(BgL_gz00_1474+1L); 
BgL_gz00_1474 = BgL_gz00_9499; 
goto BgL_zc3z04anonymousza31638ze3z87_1475;} } } } }  else 
{ /* Llib/object.scm 1000 */
return ((bool_t)0);} } } } 

}



/* register-class! */
BGL_EXPORTED_DEF obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t BgL_namez00_101, obj_t BgL_modulez00_102, obj_t BgL_superz00_103, long BgL_hashz00_104, obj_t BgL_creatorz00_105, obj_t BgL_allocatorz00_106, obj_t BgL_constructorz00_107, obj_t BgL_nilz00_108, obj_t BgL_shrinkz00_109, obj_t BgL_plainz00_110, obj_t BgL_virtualz00_111)
{
{ /* Llib/object.scm 1010 */
{ 

{ /* Llib/object.scm 1075 */
 obj_t BgL_top4652z00_9502;
BgL_top4652z00_9502 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top4652z00_9502, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1075 */
 obj_t BgL_tmp4651z00_9501;
BGl_initializa7ezd2objectsz12z67zz__objectz00(); 
{ /* Llib/object.scm 1077 */
 bool_t BgL_test4653z00_9507;
if(
CBOOL(BgL_superz00_103))
{ /* Llib/object.scm 1077 */
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1077 */
BgL_test4653z00_9507 = ((bool_t)0)
; }  else 
{ /* Llib/object.scm 1077 */
BgL_test4653z00_9507 = ((bool_t)1)
; } }  else 
{ /* Llib/object.scm 1077 */
BgL_test4653z00_9507 = ((bool_t)0)
; } 
if(BgL_test4653z00_9507)
{ /* Llib/object.scm 1077 */
BGl_errorz00zz__errorz00(BgL_namez00_101, BGl_string3555z00zz__objectz00, BgL_superz00_103); }  else 
{ /* Llib/object.scm 1077 */BFALSE; } } 
if(
VECTORP(BgL_plainz00_110))
{ /* Llib/object.scm 1079 */BFALSE; }  else 
{ /* Llib/object.scm 1079 */
BGl_errorz00zz__errorz00(BGl_string3556z00zz__objectz00, BGl_string3557z00zz__objectz00, BgL_plainz00_110); } 
{ /* Llib/object.scm 1081 */
 obj_t BgL_kz00_1487;
BgL_kz00_1487 = 
BGl_classzd2existszd2zz__objectz00(BgL_namez00_101); 
if(
BGL_CLASSP(BgL_kz00_1487))
{ /* Llib/object.scm 1085 */
 bool_t BgL_test4658z00_9519;
{ /* Llib/object.scm 1085 */
 long BgL_arg1663z00_1502;
{ /* Llib/object.scm 1085 */
 obj_t BgL_classz00_3281;
if(
BGL_CLASSP(BgL_kz00_1487))
{ /* Llib/object.scm 1085 */
BgL_classz00_3281 = BgL_kz00_1487; }  else 
{ 
 obj_t BgL_auxz00_9522;
BgL_auxz00_9522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46901L), BGl_string3556z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_kz00_1487); 
FAILURE(BgL_auxz00_9522,BFALSE,BFALSE);} 
BgL_arg1663z00_1502 = 
BGL_CLASS_HASH(BgL_classz00_3281); } 
BgL_test4658z00_9519 = 
(BgL_arg1663z00_1502==BgL_hashz00_104); } 
if(BgL_test4658z00_9519)
{ /* Llib/object.scm 1085 */
if(
BGL_CLASSP(BgL_kz00_1487))
{ /* Llib/object.scm 1086 */
BgL_tmp4651z00_9501 = BgL_kz00_1487; }  else 
{ 
 obj_t BgL_auxz00_9530;
BgL_auxz00_9530 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46916L), BGl_string3556z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_kz00_1487); 
FAILURE(BgL_auxz00_9530,BFALSE,BFALSE);} }  else 
{ /* Llib/object.scm 1085 */
{ /* Llib/object.scm 1090 */
 obj_t BgL_arg1648z00_1491;
{ /* Llib/object.scm 1090 */
 obj_t BgL_classz00_3284;
if(
BGL_CLASSP(BgL_kz00_1487))
{ /* Llib/object.scm 1090 */
BgL_classz00_3284 = BgL_kz00_1487; }  else 
{ 
 obj_t BgL_auxz00_9536;
BgL_auxz00_9536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47048L), BGl_string3556z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_kz00_1487); 
FAILURE(BgL_auxz00_9536,BFALSE,BFALSE);} 
BgL_arg1648z00_1491 = 
BGL_CLASS_MODULE(BgL_classz00_3284); } 
{ /* Llib/object.scm 1088 */
 obj_t BgL_list1649z00_1492;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1650z00_1493;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1651z00_1494;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1652z00_1495;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1653z00_1496;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1654z00_1497;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1656z00_1498;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1657z00_1499;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1658z00_1500;
{ /* Llib/object.scm 1088 */
 obj_t BgL_arg1661z00_1501;
BgL_arg1661z00_1501 = 
MAKE_YOUNG_PAIR(BGl_string3558z00zz__objectz00, BNIL); 
BgL_arg1658z00_1500 = 
MAKE_YOUNG_PAIR(BgL_arg1648z00_1491, BgL_arg1661z00_1501); } 
BgL_arg1657z00_1499 = 
MAKE_YOUNG_PAIR(BGl_string3559z00zz__objectz00, BgL_arg1658z00_1500); } 
BgL_arg1656z00_1498 = 
MAKE_YOUNG_PAIR(BgL_namez00_101, BgL_arg1657z00_1499); } 
BgL_arg1654z00_1497 = 
MAKE_YOUNG_PAIR(BGl_string3560z00zz__objectz00, BgL_arg1656z00_1498); } 
BgL_arg1653z00_1496 = 
MAKE_YOUNG_PAIR(BgL_modulez00_102, BgL_arg1654z00_1497); } 
BgL_arg1652z00_1495 = 
MAKE_YOUNG_PAIR(BGl_string3559z00zz__objectz00, BgL_arg1653z00_1496); } 
BgL_arg1651z00_1494 = 
MAKE_YOUNG_PAIR(BgL_namez00_101, BgL_arg1652z00_1495); } 
BgL_arg1650z00_1493 = 
MAKE_YOUNG_PAIR(BGl_string3561z00zz__objectz00, BgL_arg1651z00_1494); } 
BgL_list1649z00_1492 = 
MAKE_YOUNG_PAIR(BGl_string3556z00zz__objectz00, BgL_arg1650z00_1493); } 
BGl_warningz00zz__errorz00(BgL_list1649z00_1492); } } 
BgL_zc3z04anonymousza31664ze3z87_1503:
{ /* Llib/object.scm 1013 */
 bool_t BgL_test4662z00_9552;
{ /* Llib/object.scm 1013 */
 long BgL_n1z00_3241; long BgL_n2z00_3242;
{ /* Llib/object.scm 1013 */
 obj_t BgL_tmpz00_9553;
{ /* Llib/object.scm 1013 */
 obj_t BgL_aux2944z00_5690;
BgL_aux2944z00_5690 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2944z00_5690))
{ /* Llib/object.scm 1013 */
BgL_tmpz00_9553 = BgL_aux2944z00_5690
; }  else 
{ 
 obj_t BgL_auxz00_9556;
BgL_auxz00_9556 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44486L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2944z00_5690); 
FAILURE(BgL_auxz00_9556,BFALSE,BFALSE);} } 
BgL_n1z00_3241 = 
(long)CINT(BgL_tmpz00_9553); } 
{ /* Llib/object.scm 1013 */
 obj_t BgL_tmpz00_9561;
{ /* Llib/object.scm 1013 */
 obj_t BgL_aux2945z00_5691;
BgL_aux2945z00_5691 = BGl_za2nbzd2classeszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2945z00_5691))
{ /* Llib/object.scm 1013 */
BgL_tmpz00_9561 = BgL_aux2945z00_5691
; }  else 
{ 
 obj_t BgL_auxz00_9564;
BgL_auxz00_9564 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44499L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2945z00_5691); 
FAILURE(BgL_auxz00_9564,BFALSE,BFALSE);} } 
BgL_n2z00_3242 = 
(long)CINT(BgL_tmpz00_9561); } 
BgL_test4662z00_9552 = 
(BgL_n1z00_3241==BgL_n2z00_3242); } 
if(BgL_test4662z00_9552)
{ /* Llib/object.scm 1013 */
BGl_doublezd2nbzd2classesz12z12zz__objectz00(); }  else 
{ /* Llib/object.scm 1013 */((bool_t)0); } } 
{ /* Llib/object.scm 1015 */
 long BgL_numz00_1505;
{ /* Llib/object.scm 1015 */
 long BgL_za72za7_3244;
{ /* Llib/object.scm 1015 */
 obj_t BgL_tmpz00_9571;
{ /* Llib/object.scm 1015 */
 obj_t BgL_aux2946z00_5692;
BgL_aux2946z00_5692 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2946z00_5692))
{ /* Llib/object.scm 1015 */
BgL_tmpz00_9571 = BgL_aux2946z00_5692
; }  else 
{ 
 obj_t BgL_auxz00_9574;
BgL_auxz00_9574 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44586L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2946z00_5692); 
FAILURE(BgL_auxz00_9574,BFALSE,BFALSE);} } 
BgL_za72za7_3244 = 
(long)CINT(BgL_tmpz00_9571); } 
BgL_numz00_1505 = 
(OBJECT_TYPE+BgL_za72za7_3244); } 
{ /* Llib/object.scm 1015 */
 long BgL_depthz00_1506;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1017 */
 long BgL_arg1699z00_1537;
{ /* Llib/object.scm 673 */
 obj_t BgL_tmpz00_9582;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 673 */
BgL_tmpz00_9582 = BgL_superz00_103
; }  else 
{ 
 obj_t BgL_auxz00_9585;
BgL_auxz00_9585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(28814L), BGl_string3553z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_103); 
FAILURE(BgL_auxz00_9585,BFALSE,BFALSE);} 
BgL_arg1699z00_1537 = 
BGL_CLASS_DEPTH(BgL_tmpz00_9582); } 
BgL_depthz00_1506 = 
(BgL_arg1699z00_1537+1L); }  else 
{ /* Llib/object.scm 1016 */
BgL_depthz00_1506 = 0L; } 
{ /* Llib/object.scm 1016 */
 obj_t BgL_fsz00_1507;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1020 */
 obj_t BgL_arg1691z00_1534;
{ /* Llib/object.scm 1020 */
 obj_t BgL_classz00_3246;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1020 */
BgL_classz00_3246 = BgL_superz00_103; }  else 
{ 
 obj_t BgL_auxz00_9595;
BgL_auxz00_9595 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44740L), BGl_string3553z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_103); 
FAILURE(BgL_auxz00_9595,BFALSE,BFALSE);} 
BgL_arg1691z00_1534 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3246); } 
{ /* Llib/object.scm 1020 */
 obj_t BgL_list1692z00_1535;
BgL_list1692z00_1535 = 
MAKE_YOUNG_PAIR(BgL_plainz00_110, BNIL); 
BgL_fsz00_1507 = 
BGl_vectorzd2appendzd2zz__r4_vectors_6_8z00(BgL_arg1691z00_1534, BgL_list1692z00_1535); } }  else 
{ /* Llib/object.scm 1019 */
BgL_fsz00_1507 = BgL_plainz00_110; } 
{ /* Llib/object.scm 1019 */
 obj_t BgL_vsz00_1508;
BgL_vsz00_1508 = 
BGl_makezd2classzd2virtualzd2slotszd2vectorz00zz__objectz00(BgL_superz00_103, BgL_virtualz00_111); 
{ /* Llib/object.scm 1022 */
 obj_t BgL_classz00_1509;
{ /* Llib/object.scm 1023 */
 long BgL_inheritancezd2numzd2_3250; obj_t BgL_allocz00_3252; obj_t BgL_fdz00_3254; obj_t BgL_allfdz00_3255;
{ /* Llib/object.scm 1024 */
 obj_t BgL_tmpz00_9603;
{ /* Llib/object.scm 1024 */
 obj_t BgL_aux2951z00_5697;
BgL_aux2951z00_5697 = BGl_za2inheritancezd2cntza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2951z00_5697))
{ /* Llib/object.scm 1024 */
BgL_tmpz00_9603 = BgL_aux2951z00_5697
; }  else 
{ 
 obj_t BgL_auxz00_9606;
BgL_auxz00_9606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44878L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2951z00_5697); 
FAILURE(BgL_auxz00_9606,BFALSE,BFALSE);} } 
BgL_inheritancezd2numzd2_3250 = 
(long)CINT(BgL_tmpz00_9603); } 
if(
PROCEDUREP(BgL_allocatorz00_106))
{ /* Llib/object.scm 1027 */
BgL_allocz00_3252 = BgL_allocatorz00_106; }  else 
{ 
 obj_t BgL_auxz00_9613;
BgL_auxz00_9613 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44933L), BGl_string3553z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_allocatorz00_106); 
FAILURE(BgL_auxz00_9613,BFALSE,BFALSE);} 
if(
VECTORP(BgL_plainz00_110))
{ /* Llib/object.scm 1029 */
BgL_fdz00_3254 = BgL_plainz00_110; }  else 
{ 
 obj_t BgL_auxz00_9619;
BgL_auxz00_9619 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44966L), BGl_string3553z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_plainz00_110); 
FAILURE(BgL_auxz00_9619,BFALSE,BFALSE);} 
if(
VECTORP(BgL_fsz00_1507))
{ /* Llib/object.scm 1030 */
BgL_allfdz00_3255 = BgL_fsz00_1507; }  else 
{ 
 obj_t BgL_auxz00_9625;
BgL_auxz00_9625 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44981L), BGl_string3553z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_fsz00_1507); 
FAILURE(BgL_auxz00_9625,BFALSE,BFALSE);} 
BgL_classz00_1509 = 
bgl_make_class(BgL_namez00_101, BgL_modulez00_102, BgL_numz00_1505, BgL_inheritancezd2numzd2_3250, BgL_superz00_103, BNIL, BgL_allocz00_3252, BgL_hashz00_104, BgL_fdz00_3254, BgL_allfdz00_3255, BgL_constructorz00_107, BgL_vsz00_1508, BgL_creatorz00_105, BgL_nilz00_108, BgL_shrinkz00_109, BgL_depthz00_1506, BFALSE); } 
{ /* Llib/object.scm 1023 */

if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1042 */
 obj_t BgL_arg1667z00_1511;
{ /* Llib/object.scm 1042 */
 obj_t BgL_arg1668z00_1512;
{ /* Llib/object.scm 1042 */
 obj_t BgL_classz00_3259;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1042 */
BgL_classz00_3259 = BgL_superz00_103; }  else 
{ 
 obj_t BgL_auxz00_9634;
BgL_auxz00_9634 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45284L), BGl_string3553z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_103); 
FAILURE(BgL_auxz00_9634,BFALSE,BFALSE);} 
BgL_arg1668z00_1512 = 
BGL_CLASS_SUBCLASSES(BgL_classz00_3259); } 
BgL_arg1667z00_1511 = 
MAKE_YOUNG_PAIR(BgL_classz00_1509, BgL_arg1668z00_1512); } 
{ /* Llib/object.scm 703 */
 obj_t BgL_tmpz00_9640;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 703 */
BgL_tmpz00_9640 = BgL_superz00_103
; }  else 
{ 
 obj_t BgL_auxz00_9643;
BgL_auxz00_9643 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30338L), BGl_string3553z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_103); 
FAILURE(BgL_auxz00_9643,BFALSE,BFALSE);} 
BGL_CLASS_SUBCLASSES_SET(BgL_tmpz00_9640, BgL_arg1667z00_1511); } }  else 
{ /* Llib/object.scm 1039 */BFALSE; } 
{ /* Llib/object.scm 1044 */
 obj_t BgL_vectorz00_3260; long BgL_kz00_3261;
{ /* Llib/object.scm 1044 */
 obj_t BgL_aux2962z00_5708;
BgL_aux2962z00_5708 = BGl_za2classesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2962z00_5708))
{ /* Llib/object.scm 1044 */
BgL_vectorz00_3260 = BgL_aux2962z00_5708; }  else 
{ 
 obj_t BgL_auxz00_9650;
BgL_auxz00_9650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45378L), BGl_string3553z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2962z00_5708); 
FAILURE(BgL_auxz00_9650,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1044 */
 obj_t BgL_tmpz00_9654;
{ /* Llib/object.scm 1044 */
 obj_t BgL_aux2964z00_5710;
BgL_aux2964z00_5710 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2964z00_5710))
{ /* Llib/object.scm 1044 */
BgL_tmpz00_9654 = BgL_aux2964z00_5710
; }  else 
{ 
 obj_t BgL_auxz00_9657;
BgL_auxz00_9657 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45388L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2964z00_5710); 
FAILURE(BgL_auxz00_9657,BFALSE,BFALSE);} } 
BgL_kz00_3261 = 
(long)CINT(BgL_tmpz00_9654); } 
{ /* Llib/object.scm 1044 */
 bool_t BgL_test4679z00_9662;
{ /* Llib/object.scm 1044 */
 long BgL_tmpz00_9663;
BgL_tmpz00_9663 = 
VECTOR_LENGTH(BgL_vectorz00_3260); 
BgL_test4679z00_9662 = 
BOUND_CHECK(BgL_kz00_3261, BgL_tmpz00_9663); } 
if(BgL_test4679z00_9662)
{ /* Llib/object.scm 1044 */
VECTOR_SET(BgL_vectorz00_3260,BgL_kz00_3261,BgL_classz00_1509); }  else 
{ 
 obj_t BgL_auxz00_9667;
BgL_auxz00_9667 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45365L), BGl_string3554z00zz__objectz00, BgL_vectorz00_3260, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3260)), 
(int)(BgL_kz00_3261)); 
FAILURE(BgL_auxz00_9667,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_tmpz00_9674;
{ /* Llib/object.scm 1046 */
 obj_t BgL_aux2965z00_5711;
BgL_aux2965z00_5711 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2965z00_5711))
{ /* Llib/object.scm 1046 */
BgL_tmpz00_9674 = BgL_aux2965z00_5711
; }  else 
{ 
 obj_t BgL_auxz00_9677;
BgL_auxz00_9677 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45476L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2965z00_5711); 
FAILURE(BgL_auxz00_9677,BFALSE,BFALSE);} } 
BGl_za2nbzd2classesza2zd2zz__objectz00 = 
ADDFX(BgL_tmpz00_9674, 
BINT(1L)); } 
{ /* Llib/object.scm 1051 */
 bool_t BgL_test4681z00_9683;
{ /* Llib/object.scm 1051 */
 long BgL_n2z00_3264;
{ /* Llib/object.scm 1051 */
 obj_t BgL_tmpz00_9684;
{ /* Llib/object.scm 1051 */
 obj_t BgL_aux2966z00_5712;
BgL_aux2966z00_5712 = BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2966z00_5712))
{ /* Llib/object.scm 1051 */
BgL_tmpz00_9684 = BgL_aux2966z00_5712
; }  else 
{ 
 obj_t BgL_auxz00_9687;
BgL_auxz00_9687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45671L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2966z00_5712); 
FAILURE(BgL_auxz00_9687,BFALSE,BFALSE);} } 
BgL_n2z00_3264 = 
(long)CINT(BgL_tmpz00_9684); } 
BgL_test4681z00_9683 = 
(BgL_depthz00_1506>BgL_n2z00_3264); } 
if(BgL_test4681z00_9683)
{ /* Llib/object.scm 1051 */
BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00 = 
BINT(BgL_depthz00_1506); }  else 
{ /* Llib/object.scm 1051 */BFALSE; } } 
{ /* Llib/object.scm 1053 */
 long BgL_idxz00_1514;
{ /* Llib/object.scm 1053 */
 long BgL_za71za7_3265;
{ /* Llib/object.scm 1053 */
 obj_t BgL_tmpz00_9694;
{ /* Llib/object.scm 1053 */
 obj_t BgL_aux2967z00_5713;
BgL_aux2967z00_5713 = BGl_za2inheritancezd2cntza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux2967z00_5713))
{ /* Llib/object.scm 1053 */
BgL_tmpz00_9694 = BgL_aux2967z00_5713
; }  else 
{ 
 obj_t BgL_auxz00_9697;
BgL_auxz00_9697 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45758L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2967z00_5713); 
FAILURE(BgL_auxz00_9697,BFALSE,BFALSE);} } 
BgL_za71za7_3265 = 
(long)CINT(BgL_tmpz00_9694); } 
BgL_idxz00_1514 = 
(BgL_za71za7_3265+BgL_depthz00_1506); } 
{ /* Llib/object.scm 1055 */
 bool_t BgL_test4684z00_9703;
{ /* Llib/object.scm 1055 */
 long BgL_arg1678z00_1521;
{ /* Llib/object.scm 1055 */
 obj_t BgL_vectorz00_3267;
{ /* Llib/object.scm 1055 */
 obj_t BgL_aux2968z00_5714;
BgL_aux2968z00_5714 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2968z00_5714))
{ /* Llib/object.scm 1055 */
BgL_vectorz00_3267 = BgL_aux2968z00_5714; }  else 
{ 
 obj_t BgL_auxz00_9706;
BgL_auxz00_9706 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45863L), BGl_string3553z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2968z00_5714); 
FAILURE(BgL_auxz00_9706,BFALSE,BFALSE);} } 
BgL_arg1678z00_1521 = 
VECTOR_LENGTH(BgL_vectorz00_3267); } 
BgL_test4684z00_9703 = 
(BgL_idxz00_1514>=BgL_arg1678z00_1521); } 
if(BgL_test4684z00_9703)
{ /* Llib/object.scm 1056 */
 obj_t BgL_ovecz00_1517;
BgL_ovecz00_1517 = BGl_za2inheritancesza2z00zz__objectz00; 
{ /* Llib/object.scm 1056 */
 obj_t BgL_nvecz00_1518;
{ /* Llib/object.scm 1058 */
 long BgL_arg1675z00_1519;
{ /* Llib/object.scm 1058 */
 long BgL_arg1676z00_1520;
{ /* Llib/object.scm 1058 */
 obj_t BgL_vectorz00_3270;
if(
VECTORP(BgL_ovecz00_1517))
{ /* Llib/object.scm 1058 */
BgL_vectorz00_3270 = BgL_ovecz00_1517; }  else 
{ 
 obj_t BgL_auxz00_9714;
BgL_auxz00_9714 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45975L), BGl_string3553z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_ovecz00_1517); 
FAILURE(BgL_auxz00_9714,BFALSE,BFALSE);} 
BgL_arg1676z00_1520 = 
VECTOR_LENGTH(BgL_vectorz00_3270); } 
{ /* Llib/object.scm 1058 */
 long BgL_za72za7_3272;
{ /* Llib/object.scm 1059 */
 obj_t BgL_tmpz00_9719;
{ /* Llib/object.scm 1059 */
 obj_t BgL_aux2972z00_5718;
BgL_aux2972z00_5718 = BGl_za2inheritancezd2maxzd2depthza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux2972z00_5718))
{ /* Llib/object.scm 1059 */
BgL_tmpz00_9719 = BgL_aux2972z00_5718
; }  else 
{ 
 obj_t BgL_auxz00_9722;
BgL_auxz00_9722 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(45991L), BGl_string3553z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux2972z00_5718); 
FAILURE(BgL_auxz00_9722,BFALSE,BFALSE);} } 
BgL_za72za7_3272 = 
(long)CINT(BgL_tmpz00_9719); } 
BgL_arg1675z00_1519 = 
(BgL_arg1676z00_1520+BgL_za72za7_3272); } } 
BgL_nvecz00_1518 = 
BGl_extendzd2vectorzd2zz__objectz00(BgL_ovecz00_1517, BFALSE, BgL_arg1675z00_1519); } 
{ /* Llib/object.scm 1057 */

BGl_za2inheritancesza2z00zz__objectz00 = BgL_nvecz00_1518; 
{ /* Llib/object.scm 1061 */
 obj_t BgL_tmpz00_9729;
if(
VECTORP(BgL_ovecz00_1517))
{ /* Llib/object.scm 1061 */
BgL_tmpz00_9729 = BgL_ovecz00_1517
; }  else 
{ 
 obj_t BgL_auxz00_9732;
BgL_auxz00_9732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46090L), BGl_string3553z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_ovecz00_1517); 
FAILURE(BgL_auxz00_9732,BFALSE,BFALSE);} 
FREE_VECTOR_UNCOLLECTABLE(BgL_tmpz00_9729); } BUNSPEC; } } }  else 
{ /* Llib/object.scm 1055 */BFALSE; } } 
{ 
 long BgL_iz00_1523; obj_t BgL_jz00_1524;
BgL_iz00_1523 = 0L; 
BgL_jz00_1524 = BGl_za2inheritancezd2cntza2zd2zz__objectz00; 
BgL_zc3z04anonymousza31679ze3z87_1525:
if(
(BgL_iz00_1523<=BgL_depthz00_1506))
{ /* Llib/object.scm 1065 */
{ /* Llib/object.scm 1068 */
 obj_t BgL_arg1681z00_1527;
BgL_arg1681z00_1527 = 
BGL_CLASS_ANCESTORS_REF(BgL_classz00_1509, BgL_iz00_1523); 
{ /* Llib/object.scm 1067 */
 obj_t BgL_vectorz00_3276; long BgL_kz00_3277;
{ /* Llib/object.scm 1067 */
 obj_t BgL_aux2975z00_5721;
BgL_aux2975z00_5721 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2975z00_5721))
{ /* Llib/object.scm 1067 */
BgL_vectorz00_3276 = BgL_aux2975z00_5721; }  else 
{ 
 obj_t BgL_auxz00_9742;
BgL_auxz00_9742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46260L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux2975z00_5721); 
FAILURE(BgL_auxz00_9742,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1067 */
 obj_t BgL_tmpz00_9746;
if(
INTEGERP(BgL_jz00_1524))
{ /* Llib/object.scm 1067 */
BgL_tmpz00_9746 = BgL_jz00_1524
; }  else 
{ 
 obj_t BgL_auxz00_9749;
BgL_auxz00_9749 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46275L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_jz00_1524); 
FAILURE(BgL_auxz00_9749,BFALSE,BFALSE);} 
BgL_kz00_3277 = 
(long)CINT(BgL_tmpz00_9746); } 
{ /* Llib/object.scm 1067 */
 bool_t BgL_test4692z00_9754;
{ /* Llib/object.scm 1067 */
 long BgL_tmpz00_9755;
BgL_tmpz00_9755 = 
VECTOR_LENGTH(BgL_vectorz00_3276); 
BgL_test4692z00_9754 = 
BOUND_CHECK(BgL_kz00_3277, BgL_tmpz00_9755); } 
if(BgL_test4692z00_9754)
{ /* Llib/object.scm 1067 */
VECTOR_SET(BgL_vectorz00_3276,BgL_kz00_3277,BgL_arg1681z00_1527); }  else 
{ 
 obj_t BgL_auxz00_9759;
BgL_auxz00_9759 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46247L), BGl_string3554z00zz__objectz00, BgL_vectorz00_3276, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3276)), 
(int)(BgL_kz00_3277)); 
FAILURE(BgL_auxz00_9759,BFALSE,BFALSE);} } } } 
{ /* Llib/object.scm 1069 */
 long BgL_arg1684z00_1528; long BgL_arg1685z00_1529;
BgL_arg1684z00_1528 = 
(BgL_iz00_1523+1L); 
{ /* Llib/object.scm 1069 */
 long BgL_za71za7_3279;
{ /* Llib/object.scm 1069 */
 obj_t BgL_tmpz00_9767;
if(
INTEGERP(BgL_jz00_1524))
{ /* Llib/object.scm 1069 */
BgL_tmpz00_9767 = BgL_jz00_1524
; }  else 
{ 
 obj_t BgL_auxz00_9770;
BgL_auxz00_9770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46342L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_jz00_1524); 
FAILURE(BgL_auxz00_9770,BFALSE,BFALSE);} 
BgL_za71za7_3279 = 
(long)CINT(BgL_tmpz00_9767); } 
BgL_arg1685z00_1529 = 
(BgL_za71za7_3279+1L); } 
{ 
 obj_t BgL_jz00_9777; long BgL_iz00_9776;
BgL_iz00_9776 = BgL_arg1684z00_1528; 
BgL_jz00_9777 = 
BINT(BgL_arg1685z00_1529); 
BgL_jz00_1524 = BgL_jz00_9777; 
BgL_iz00_1523 = BgL_iz00_9776; 
goto BgL_zc3z04anonymousza31679ze3z87_1525;} } }  else 
{ /* Llib/object.scm 1065 */
BGl_za2inheritancezd2cntza2zd2zz__objectz00 = BgL_jz00_1524; } } } 
{ /* Llib/object.scm 1072 */
 long BgL_arg1688z00_1531;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1072 */
 obj_t BgL_classz00_3280;
if(
BGL_CLASSP(BgL_superz00_103))
{ /* Llib/object.scm 1072 */
BgL_classz00_3280 = BgL_superz00_103; }  else 
{ 
 obj_t BgL_auxz00_9783;
BgL_auxz00_9783 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(46511L), BGl_string3553z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_103); 
FAILURE(BgL_auxz00_9783,BFALSE,BFALSE);} 
BgL_arg1688z00_1531 = 
BGL_CLASS_INDEX(BgL_classz00_3280); }  else 
{ /* Llib/object.scm 1072 */
BgL_arg1688z00_1531 = BgL_numz00_1505; } 
BGl_genericszd2addzd2classz12z12zz__objectz00(BgL_numz00_1505, BgL_arg1688z00_1531); } 
BgL_tmp4651z00_9501 = BgL_classz00_1509; } } } } } } } }  else 
{ /* Llib/object.scm 1083 */
goto BgL_zc3z04anonymousza31664ze3z87_1503;} } 
BGL_EXITD_POP_PROTECT(BgL_top4652z00_9502); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); 
return BgL_tmp4651z00_9501;} } } } 

}



/* &register-class! */
obj_t BGl_z62registerzd2classz12za2zz__objectz00(obj_t BgL_envz00_4861, obj_t BgL_namez00_4862, obj_t BgL_modulez00_4863, obj_t BgL_superz00_4864, obj_t BgL_hashz00_4865, obj_t BgL_creatorz00_4866, obj_t BgL_allocatorz00_4867, obj_t BgL_constructorz00_4868, obj_t BgL_nilz00_4869, obj_t BgL_shrinkz00_4870, obj_t BgL_plainz00_4871, obj_t BgL_virtualz00_4872)
{
{ /* Llib/object.scm 1010 */
{ /* Llib/object.scm 1013 */
 obj_t BgL_auxz00_9821; obj_t BgL_auxz00_9814; long BgL_auxz00_9805; obj_t BgL_auxz00_9798; obj_t BgL_auxz00_9791;
if(
VECTORP(BgL_virtualz00_4872))
{ /* Llib/object.scm 1013 */
BgL_auxz00_9821 = BgL_virtualz00_4872
; }  else 
{ 
 obj_t BgL_auxz00_9824;
BgL_auxz00_9824 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44475L), BGl_string3562z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_virtualz00_4872); 
FAILURE(BgL_auxz00_9824,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_nilz00_4869))
{ /* Llib/object.scm 1013 */
BgL_auxz00_9814 = BgL_nilz00_4869
; }  else 
{ 
 obj_t BgL_auxz00_9817;
BgL_auxz00_9817 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44475L), BGl_string3562z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_nilz00_4869); 
FAILURE(BgL_auxz00_9817,BFALSE,BFALSE);} 
{ /* Llib/object.scm 1013 */
 obj_t BgL_tmpz00_9806;
if(
INTEGERP(BgL_hashz00_4865))
{ /* Llib/object.scm 1013 */
BgL_tmpz00_9806 = BgL_hashz00_4865
; }  else 
{ 
 obj_t BgL_auxz00_9809;
BgL_auxz00_9809 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44475L), BGl_string3562z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_hashz00_4865); 
FAILURE(BgL_auxz00_9809,BFALSE,BFALSE);} 
BgL_auxz00_9805 = 
(long)CINT(BgL_tmpz00_9806); } 
if(
SYMBOLP(BgL_modulez00_4863))
{ /* Llib/object.scm 1013 */
BgL_auxz00_9798 = BgL_modulez00_4863
; }  else 
{ 
 obj_t BgL_auxz00_9801;
BgL_auxz00_9801 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44475L), BGl_string3562z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_modulez00_4863); 
FAILURE(BgL_auxz00_9801,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_namez00_4862))
{ /* Llib/object.scm 1013 */
BgL_auxz00_9791 = BgL_namez00_4862
; }  else 
{ 
 obj_t BgL_auxz00_9794;
BgL_auxz00_9794 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(44475L), BGl_string3562z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_namez00_4862); 
FAILURE(BgL_auxz00_9794,BFALSE,BFALSE);} 
return 
BGl_registerzd2classz12zc0zz__objectz00(BgL_auxz00_9791, BgL_auxz00_9798, BgL_superz00_4864, BgL_auxz00_9805, BgL_creatorz00_4866, BgL_allocatorz00_4867, BgL_constructorz00_4868, BgL_auxz00_9814, BgL_shrinkz00_4870, BgL_plainz00_4871, BgL_auxz00_9821);} } 

}



/* make-class-virtual-slots-vector */
obj_t BGl_makezd2classzd2virtualzd2slotszd2vectorz00zz__objectz00(obj_t BgL_superz00_112, obj_t BgL_virtualsz00_113)
{
{ /* Llib/object.scm 1096 */
{ 
 obj_t BgL_vecz00_1557;
if(
BGL_CLASSP(BgL_superz00_112))
{ /* Llib/object.scm 1109 */
 obj_t BgL_ovecz00_1541;
{ /* Llib/object.scm 1109 */
 obj_t BgL_classz00_3291;
if(
BGL_CLASSP(BgL_superz00_112))
{ /* Llib/object.scm 1109 */
BgL_classz00_3291 = BgL_superz00_112; }  else 
{ 
 obj_t BgL_auxz00_9833;
BgL_auxz00_9833 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47760L), BGl_string3567z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_112); 
FAILURE(BgL_auxz00_9833,BFALSE,BFALSE);} 
BgL_ovecz00_1541 = 
BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_3291); } 
{ /* Llib/object.scm 1110 */
 long BgL_lenz00_1543;
{ /* Llib/object.scm 1111 */
 obj_t BgL_vectorz00_3293;
if(
VECTORP(BgL_virtualsz00_113))
{ /* Llib/object.scm 1111 */
BgL_vectorz00_3293 = BgL_virtualsz00_113; }  else 
{ 
 obj_t BgL_auxz00_9840;
BgL_auxz00_9840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47831L), BGl_string3567z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_virtualsz00_113); 
FAILURE(BgL_auxz00_9840,BFALSE,BFALSE);} 
BgL_lenz00_1543 = 
VECTOR_LENGTH(BgL_vectorz00_3293); } 
{ /* Llib/object.scm 1111 */
 obj_t BgL_vecz00_1544;
{ /* Llib/object.scm 1112 */
 long BgL_arg1705z00_1552;
BgL_arg1705z00_1552 = 
(
VECTOR_LENGTH(BgL_ovecz00_1541)+BgL_lenz00_1543); 
BgL_vecz00_1544 = 
make_vector(BgL_arg1705z00_1552, BUNSPEC); } 
{ /* Llib/object.scm 1112 */

{ 
 long BgL_iz00_1546;
BgL_iz00_1546 = 0L; 
BgL_zc3z04anonymousza31701ze3z87_1547:
if(
(BgL_iz00_1546==
VECTOR_LENGTH(BgL_ovecz00_1541)))
{ /* Llib/object.scm 1114 */
BgL_vecz00_1557 = BgL_vecz00_1544; 
BgL_lambda1708z00_1558:
{ /* Llib/object.scm 1099 */
 obj_t BgL_g1324z00_1559;
{ /* Llib/object.scm 1102 */
 obj_t BgL_auxz00_9851;
if(
VECTORP(BgL_virtualsz00_113))
{ /* Llib/object.scm 1102 */
BgL_auxz00_9851 = BgL_virtualsz00_113
; }  else 
{ 
 obj_t BgL_auxz00_9854;
BgL_auxz00_9854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47560L), BGl_string3563z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_virtualsz00_113); 
FAILURE(BgL_auxz00_9854,BFALSE,BFALSE);} 
BgL_g1324z00_1559 = 
BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_auxz00_9851); } 
{ 
 obj_t BgL_l1322z00_1561;
BgL_l1322z00_1561 = BgL_g1324z00_1559; 
BgL_zc3z04anonymousza31709ze3z87_1562:
if(
PAIRP(BgL_l1322z00_1561))
{ /* Llib/object.scm 1102 */
{ /* Llib/object.scm 1100 */
 obj_t BgL_virtualz00_1564;
BgL_virtualz00_1564 = 
CAR(BgL_l1322z00_1561); 
{ /* Llib/object.scm 1100 */
 obj_t BgL_numz00_1565;
{ /* Llib/object.scm 1100 */
 obj_t BgL_pairz00_3286;
if(
PAIRP(BgL_virtualz00_1564))
{ /* Llib/object.scm 1100 */
BgL_pairz00_3286 = BgL_virtualz00_1564; }  else 
{ 
 obj_t BgL_auxz00_9864;
BgL_auxz00_9864 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47487L), BGl_string3564z00zz__objectz00, BGl_string3565z00zz__objectz00, BgL_virtualz00_1564); 
FAILURE(BgL_auxz00_9864,BFALSE,BFALSE);} 
BgL_numz00_1565 = 
CAR(BgL_pairz00_3286); } 
{ /* Llib/object.scm 1101 */
 obj_t BgL_arg1711z00_1566;
{ /* Llib/object.scm 1101 */
 obj_t BgL_pairz00_3287;
if(
PAIRP(BgL_virtualz00_1564))
{ /* Llib/object.scm 1101 */
BgL_pairz00_3287 = BgL_virtualz00_1564; }  else 
{ 
 obj_t BgL_auxz00_9871;
BgL_auxz00_9871 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47532L), BGl_string3564z00zz__objectz00, BGl_string3565z00zz__objectz00, BgL_virtualz00_1564); 
FAILURE(BgL_auxz00_9871,BFALSE,BFALSE);} 
BgL_arg1711z00_1566 = 
CDR(BgL_pairz00_3287); } 
{ /* Llib/object.scm 1101 */
 long BgL_kz00_3289;
{ /* Llib/object.scm 1101 */
 obj_t BgL_tmpz00_9876;
if(
INTEGERP(BgL_numz00_1565))
{ /* Llib/object.scm 1101 */
BgL_tmpz00_9876 = BgL_numz00_1565
; }  else 
{ 
 obj_t BgL_auxz00_9879;
BgL_auxz00_9879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47523L), BGl_string3564z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_numz00_1565); 
FAILURE(BgL_auxz00_9879,BFALSE,BFALSE);} 
BgL_kz00_3289 = 
(long)CINT(BgL_tmpz00_9876); } 
{ /* Llib/object.scm 1101 */
 bool_t BgL_test4710z00_9884;
{ /* Llib/object.scm 1101 */
 long BgL_tmpz00_9885;
BgL_tmpz00_9885 = 
VECTOR_LENGTH(BgL_vecz00_1557); 
BgL_test4710z00_9884 = 
BOUND_CHECK(BgL_kz00_3289, BgL_tmpz00_9885); } 
if(BgL_test4710z00_9884)
{ /* Llib/object.scm 1101 */
VECTOR_SET(BgL_vecz00_1557,BgL_kz00_3289,BgL_arg1711z00_1566); }  else 
{ 
 obj_t BgL_auxz00_9889;
BgL_auxz00_9889 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47506L), BGl_string3554z00zz__objectz00, BgL_vecz00_1557, 
(int)(
VECTOR_LENGTH(BgL_vecz00_1557)), 
(int)(BgL_kz00_3289)); 
FAILURE(BgL_auxz00_9889,BFALSE,BFALSE);} } } } } } 
{ 
 obj_t BgL_l1322z00_9896;
BgL_l1322z00_9896 = 
CDR(BgL_l1322z00_1561); 
BgL_l1322z00_1561 = BgL_l1322z00_9896; 
goto BgL_zc3z04anonymousza31709ze3z87_1562;} }  else 
{ /* Llib/object.scm 1102 */
if(
NULLP(BgL_l1322z00_1561))
{ /* Llib/object.scm 1102 */BTRUE; }  else 
{ /* Llib/object.scm 1102 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3566z00zz__objectz00, BGl_string3552z00zz__objectz00, BgL_l1322z00_1561, BGl_string3441z00zz__objectz00, 
BINT(47438L)); } } } } 
return BgL_vecz00_1557;}  else 
{ /* Llib/object.scm 1114 */
{ /* Llib/object.scm 1117 */
 obj_t BgL_arg1703z00_1549;
BgL_arg1703z00_1549 = 
VECTOR_REF(BgL_ovecz00_1541,BgL_iz00_1546); 
{ /* Llib/object.scm 1117 */
 bool_t BgL_test4712z00_9903;
{ /* Llib/object.scm 1117 */
 long BgL_tmpz00_9904;
BgL_tmpz00_9904 = 
VECTOR_LENGTH(BgL_vecz00_1544); 
BgL_test4712z00_9903 = 
BOUND_CHECK(BgL_iz00_1546, BgL_tmpz00_9904); } 
if(BgL_test4712z00_9903)
{ /* Llib/object.scm 1117 */
VECTOR_SET(BgL_vecz00_1544,BgL_iz00_1546,BgL_arg1703z00_1549); }  else 
{ 
 obj_t BgL_auxz00_9908;
BgL_auxz00_9908 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47982L), BGl_string3554z00zz__objectz00, BgL_vecz00_1544, 
(int)(
VECTOR_LENGTH(BgL_vecz00_1544)), 
(int)(BgL_iz00_1546)); 
FAILURE(BgL_auxz00_9908,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_9915;
BgL_iz00_9915 = 
(BgL_iz00_1546+1L); 
BgL_iz00_1546 = BgL_iz00_9915; 
goto BgL_zc3z04anonymousza31701ze3z87_1547;} } } } } } }  else 
{ /* Llib/object.scm 1106 */
 long BgL_lenz00_1554;
{ /* Llib/object.scm 1106 */
 obj_t BgL_vectorz00_3304;
if(
VECTORP(BgL_virtualsz00_113))
{ /* Llib/object.scm 1106 */
BgL_vectorz00_3304 = BgL_virtualsz00_113; }  else 
{ 
 obj_t BgL_auxz00_9919;
BgL_auxz00_9919 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(47645L), BGl_string3567z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_virtualsz00_113); 
FAILURE(BgL_auxz00_9919,BFALSE,BFALSE);} 
BgL_lenz00_1554 = 
VECTOR_LENGTH(BgL_vectorz00_3304); } 
{ /* Llib/object.scm 1106 */
 obj_t BgL_vecz00_1555;
BgL_vecz00_1555 = 
make_vector(BgL_lenz00_1554, BUNSPEC); 
{ /* Llib/object.scm 1107 */

{ 
 obj_t BgL_vecz00_9925;
BgL_vecz00_9925 = BgL_vecz00_1555; 
BgL_vecz00_1557 = BgL_vecz00_9925; 
goto BgL_lambda1708z00_1558;} } } } } } 

}



/* make-method-array */
obj_t BGl_makezd2methodzd2arrayz00zz__objectz00(obj_t BgL_defzd2bucketzd2_115)
{
{ /* Llib/object.scm 1129 */
{ /* Llib/object.scm 1130 */
 long BgL_sz00_1572; long BgL_az00_1573;
{ /* Llib/object.scm 1130 */
 int BgL_arg1725z00_1580;
BgL_arg1725z00_1580 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
{ /* Llib/object.scm 1130 */
 long BgL_n1z00_3312; long BgL_n2z00_3313;
{ /* Llib/object.scm 1130 */
 obj_t BgL_tmpz00_9931;
{ /* Llib/object.scm 1130 */
 obj_t BgL_aux3009z00_5757;
BgL_aux3009z00_5757 = BGl_za2nbzd2classeszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux3009z00_5757))
{ /* Llib/object.scm 1130 */
BgL_tmpz00_9931 = BgL_aux3009z00_5757
; }  else 
{ 
 obj_t BgL_auxz00_9934;
BgL_auxz00_9934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(48667L), BGl_string3568z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3009z00_5757); 
FAILURE(BgL_auxz00_9934,BFALSE,BFALSE);} } 
BgL_n1z00_3312 = 
(long)CINT(BgL_tmpz00_9931); } 
BgL_n2z00_3313 = 
(long)(BgL_arg1725z00_1580); 
BgL_sz00_1572 = 
(BgL_n1z00_3312/BgL_n2z00_3313); } } 
{ /* Llib/object.scm 1131 */
 int BgL_arg1726z00_1581;
BgL_arg1726z00_1581 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
{ /* Llib/object.scm 1131 */
 long BgL_n1z00_3318; long BgL_n2z00_3319;
{ /* Llib/object.scm 1131 */
 obj_t BgL_tmpz00_9946;
{ /* Llib/object.scm 1131 */
 obj_t BgL_aux3010z00_5758;
BgL_aux3010z00_5758 = BGl_za2nbzd2classeszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux3010z00_5758))
{ /* Llib/object.scm 1131 */
BgL_tmpz00_9946 = BgL_aux3010z00_5758
; }  else 
{ 
 obj_t BgL_auxz00_9949;
BgL_auxz00_9949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(48733L), BGl_string3568z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3010z00_5758); 
FAILURE(BgL_auxz00_9949,BFALSE,BFALSE);} } 
BgL_n1z00_3318 = 
(long)CINT(BgL_tmpz00_9946); } 
BgL_n2z00_3319 = 
(long)(BgL_arg1726z00_1581); 
{ /* Llib/object.scm 1131 */
 bool_t BgL_test4716z00_9955;
{ /* Llib/object.scm 1131 */
 long BgL_arg2387z00_3321;
BgL_arg2387z00_3321 = 
(((BgL_n1z00_3318) | (BgL_n2z00_3319)) & -2147483648); 
BgL_test4716z00_9955 = 
(BgL_arg2387z00_3321==0L); } 
if(BgL_test4716z00_9955)
{ /* Llib/object.scm 1131 */
 int32_t BgL_arg2384z00_3322;
{ /* Llib/object.scm 1131 */
 int32_t BgL_arg2385z00_3323; int32_t BgL_arg2386z00_3324;
BgL_arg2385z00_3323 = 
(int32_t)(BgL_n1z00_3318); 
BgL_arg2386z00_3324 = 
(int32_t)(BgL_n2z00_3319); 
BgL_arg2384z00_3322 = 
(BgL_arg2385z00_3323%BgL_arg2386z00_3324); } 
{ /* Llib/object.scm 1131 */
 long BgL_arg2511z00_3329;
BgL_arg2511z00_3329 = 
(long)(BgL_arg2384z00_3322); 
BgL_az00_1573 = 
(long)(BgL_arg2511z00_3329); } }  else 
{ /* Llib/object.scm 1131 */
BgL_az00_1573 = 
(BgL_n1z00_3318%BgL_n2z00_3319); } } } } 
if(
(BgL_az00_1573>0L))
{ /* Llib/object.scm 1132 */
{ /* Llib/object.scm 1136 */
 int BgL_arg1720z00_1575;
BgL_arg1720z00_1575 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
{ /* Llib/object.scm 1134 */
 obj_t BgL_list1721z00_1576;
{ /* Llib/object.scm 1134 */
 obj_t BgL_arg1722z00_1577;
{ /* Llib/object.scm 1134 */
 obj_t BgL_arg1723z00_1578;
BgL_arg1723z00_1578 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1720z00_1575), BNIL); 
BgL_arg1722z00_1577 = 
MAKE_YOUNG_PAIR(BGl_string3569z00zz__objectz00, BgL_arg1723z00_1578); } 
BgL_list1721z00_1576 = 
MAKE_YOUNG_PAIR(BGl_string3568z00zz__objectz00, BgL_arg1722z00_1577); } 
BGl_warningz00zz__errorz00(BgL_list1721z00_1576); } } 
return 
make_vector_uncollectable(
(BgL_sz00_1572+1L), BgL_defzd2bucketzd2_115);}  else 
{ /* Llib/object.scm 1132 */
return 
make_vector_uncollectable(BgL_sz00_1572, BgL_defzd2bucketzd2_115);} } } 

}



/* &generic-no-default-behavior */
obj_t BGl_z62genericzd2nozd2defaultzd2behaviorzb0zz__objectz00(obj_t BgL_envz00_4873, obj_t BgL_lz00_4874)
{
{ /* Llib/object.scm 1143 */
return 
BGl_errorz00zz__errorz00(BGl_string3538z00zz__objectz00, BGl_string3570z00zz__objectz00, BgL_lz00_4874);} 

}



/* procedure->generic */
BGL_EXPORTED_DEF obj_t BGl_procedurezd2ze3genericz31zz__objectz00(obj_t BgL_procz00_117)
{
{ /* Llib/object.scm 1149 */
BGL_TAIL return 
bgl_make_generic(BgL_procz00_117);} 

}



/* &procedure->generic */
obj_t BGl_z62procedurezd2ze3genericz53zz__objectz00(obj_t BgL_envz00_4875, obj_t BgL_procz00_4876)
{
{ /* Llib/object.scm 1149 */
{ /* Llib/object.scm 1150 */
 obj_t BgL_auxz00_9981;
if(
PROCEDUREP(BgL_procz00_4876))
{ /* Llib/object.scm 1150 */
BgL_auxz00_9981 = BgL_procz00_4876
; }  else 
{ 
 obj_t BgL_auxz00_9984;
BgL_auxz00_9984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(49610L), BGl_string3571z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_procz00_4876); 
FAILURE(BgL_auxz00_9984,BFALSE,BFALSE);} 
return 
BGl_procedurezd2ze3genericz31zz__objectz00(BgL_auxz00_9981);} } 

}



/* register-generic! */
BGL_EXPORTED_DEF obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t BgL_genericz00_118, obj_t BgL_defaultz00_119, obj_t BgL_classzd2minzd2_120, obj_t BgL_namez00_121)
{
{ /* Llib/object.scm 1155 */
{ /* Llib/object.scm 1156 */
 obj_t BgL_top4720z00_9990;
BgL_top4720z00_9990 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top4720z00_9990, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp4719z00_9989;
BgL_tmp4719z00_9989 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BgL_genericz00_118, BgL_defaultz00_119, BgL_namez00_121); 
BGL_EXITD_POP_PROTECT(BgL_top4720z00_9990); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); 
return BgL_tmp4719z00_9989;} } } 

}



/* &register-generic! */
obj_t BGl_z62registerzd2genericz12za2zz__objectz00(obj_t BgL_envz00_4877, obj_t BgL_genericz00_4878, obj_t BgL_defaultz00_4879, obj_t BgL_classzd2minzd2_4880, obj_t BgL_namez00_4881)
{
{ /* Llib/object.scm 1155 */
{ /* Llib/object.scm 1156 */
 obj_t BgL_auxz00_10004; obj_t BgL_auxz00_9997;
if(
PROCEDUREP(BgL_defaultz00_4879))
{ /* Llib/object.scm 1156 */
BgL_auxz00_10004 = BgL_defaultz00_4879
; }  else 
{ 
 obj_t BgL_auxz00_10007;
BgL_auxz00_10007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(49917L), BGl_string3572z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_defaultz00_4879); 
FAILURE(BgL_auxz00_10007,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_genericz00_4878))
{ /* Llib/object.scm 1156 */
BgL_auxz00_9997 = BgL_genericz00_4878
; }  else 
{ 
 obj_t BgL_auxz00_10000;
BgL_auxz00_10000 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(49917L), BGl_string3572z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4878); 
FAILURE(BgL_auxz00_10000,BFALSE,BFALSE);} 
return 
BGl_registerzd2genericz12zc0zz__objectz00(BgL_auxz00_9997, BgL_auxz00_10004, BgL_classzd2minzd2_4880, BgL_namez00_4881);} } 

}



/* register-generic-sans-lock! */
obj_t BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(obj_t BgL_genericz00_122, obj_t BgL_defaultz00_123, obj_t BgL_namez00_124)
{
{ /* Llib/object.scm 1169 */
{ /* Llib/object.scm 1170 */
 bool_t BgL_test4723z00_10012;
{ /* Llib/object.scm 1170 */
 obj_t BgL_genericz00_3337;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 1170 */
BgL_genericz00_3337 = BgL_genericz00_122; }  else 
{ 
 obj_t BgL_auxz00_10015;
BgL_auxz00_10015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(50844L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10015,BFALSE,BFALSE);} 
{ /* Llib/object.scm 1124 */
 obj_t BgL_tmpz00_10019;
BgL_tmpz00_10019 = 
PROCEDURE_REF(BgL_genericz00_3337, 
(int)(1L)); 
BgL_test4723z00_10012 = 
VECTORP(BgL_tmpz00_10019); } } 
if(BgL_test4723z00_10012)
{ /* Llib/object.scm 1170 */
if(
PROCEDUREP(BgL_defaultz00_123))
{ /* Llib/object.scm 1189 */
 obj_t BgL_oldzd2defzd2bucketz00_1584; obj_t BgL_newzd2defzd2bucketz00_1585; obj_t BgL_oldzd2defaultzd2_1586;
{ /* Llib/object.scm 1189 */
 obj_t BgL_res2588z00_3340;
{ /* Llib/object.scm 918 */
 obj_t BgL_aux3021z00_5769;
{ /* Llib/object.scm 918 */
 obj_t BgL_tmpz00_10025;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 918 */
BgL_tmpz00_10025 = BgL_genericz00_122
; }  else 
{ 
 obj_t BgL_auxz00_10028;
BgL_auxz00_10028 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40183L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10028,BFALSE,BFALSE);} 
BgL_aux3021z00_5769 = 
PROCEDURE_REF(BgL_tmpz00_10025, 
(int)(2L)); } 
if(
VECTORP(BgL_aux3021z00_5769))
{ /* Llib/object.scm 918 */
BgL_res2588z00_3340 = BgL_aux3021z00_5769; }  else 
{ 
 obj_t BgL_auxz00_10036;
BgL_auxz00_10036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40168L), BGl_string3573z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3021z00_5769); 
FAILURE(BgL_auxz00_10036,BFALSE,BFALSE);} } 
BgL_oldzd2defzd2bucketz00_1584 = BgL_res2588z00_3340; } 
{ /* Llib/object.scm 1191 */
 int BgL_arg1743z00_1609;
BgL_arg1743z00_1609 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
BgL_newzd2defzd2bucketz00_1585 = 
make_vector_uncollectable(
(long)(BgL_arg1743z00_1609), BgL_defaultz00_123); } 
{ /* Llib/object.scm 1192 */
 obj_t BgL_res2591z00_3346;
{ /* Llib/object.scm 1192 */
 obj_t BgL_genericz00_3345;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 1192 */
BgL_genericz00_3345 = BgL_genericz00_122; }  else 
{ 
 obj_t BgL_auxz00_10049;
BgL_auxz00_10049 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51778L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10049,BFALSE,BFALSE);} 
{ /* Llib/object.scm 900 */
 obj_t BgL_aux3025z00_5773;
BgL_aux3025z00_5773 = 
PROCEDURE_REF(BgL_genericz00_3345, 
(int)(0L)); 
if(
PROCEDUREP(BgL_aux3025z00_5773))
{ /* Llib/object.scm 900 */
BgL_res2591z00_3346 = BgL_aux3025z00_5773; }  else 
{ 
 obj_t BgL_auxz00_10057;
BgL_auxz00_10057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3025z00_5773); 
FAILURE(BgL_auxz00_10057,BFALSE,BFALSE);} } } 
BgL_oldzd2defaultzd2_1586 = BgL_res2591z00_3346; } 
{ /* Llib/object.scm 1193 */
 obj_t BgL_marrayz00_1587;
{ /* Llib/object.scm 1193 */
 obj_t BgL_genericz00_3347;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 1193 */
BgL_genericz00_3347 = BgL_genericz00_122; }  else 
{ 
 obj_t BgL_auxz00_10063;
BgL_auxz00_10063 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51828L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10063,BFALSE,BFALSE);} 
BgL_marrayz00_1587 = 
PROCEDURE_REF(BgL_genericz00_3347, 
(int)(1L)); } 
{ /* Llib/object.scm 1193 */
 long BgL_alenz00_1588;
{ /* Llib/object.scm 1194 */
 obj_t BgL_vectorz00_3348;
if(
VECTORP(BgL_marrayz00_1587))
{ /* Llib/object.scm 1194 */
BgL_vectorz00_3348 = BgL_marrayz00_1587; }  else 
{ 
 obj_t BgL_auxz00_10071;
BgL_auxz00_10071 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51868L), BGl_string3573z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_marrayz00_1587); 
FAILURE(BgL_auxz00_10071,BFALSE,BFALSE);} 
BgL_alenz00_1588 = 
VECTOR_LENGTH(BgL_vectorz00_3348); } 
{ /* Llib/object.scm 1194 */

{ 
 long BgL_iz00_1590;
BgL_iz00_1590 = 0L; 
BgL_zc3z04anonymousza31729ze3z87_1591:
if(
(BgL_iz00_1590<BgL_alenz00_1588))
{ /* Llib/object.scm 1197 */
 obj_t BgL_bucketz00_1593;
{ /* Llib/object.scm 1197 */
 obj_t BgL_vectorz00_3351;
if(
VECTORP(BgL_marrayz00_1587))
{ /* Llib/object.scm 1197 */
BgL_vectorz00_3351 = BgL_marrayz00_1587; }  else 
{ 
 obj_t BgL_auxz00_10080;
BgL_auxz00_10080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51960L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_marrayz00_1587); 
FAILURE(BgL_auxz00_10080,BFALSE,BFALSE);} 
BgL_bucketz00_1593 = 
VECTOR_REF(BgL_vectorz00_3351,BgL_iz00_1590); } 
if(
(BgL_bucketz00_1593==BgL_oldzd2defzd2bucketz00_1584))
{ /* Llib/object.scm 1198 */
{ /* Llib/object.scm 1200 */
 obj_t BgL_vectorz00_3353;
if(
VECTORP(BgL_marrayz00_1587))
{ /* Llib/object.scm 1200 */
BgL_vectorz00_3353 = BgL_marrayz00_1587; }  else 
{ 
 obj_t BgL_auxz00_10089;
BgL_auxz00_10089 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52045L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_marrayz00_1587); 
FAILURE(BgL_auxz00_10089,BFALSE,BFALSE);} 
{ /* Llib/object.scm 1200 */
 bool_t BgL_test4736z00_10093;
{ /* Llib/object.scm 1200 */
 long BgL_tmpz00_10094;
BgL_tmpz00_10094 = 
VECTOR_LENGTH(BgL_vectorz00_3353); 
BgL_test4736z00_10093 = 
BOUND_CHECK(BgL_iz00_1590, BgL_tmpz00_10094); } 
if(BgL_test4736z00_10093)
{ /* Llib/object.scm 1200 */
VECTOR_SET(BgL_vectorz00_3353,BgL_iz00_1590,BgL_newzd2defzd2bucketz00_1585); }  else 
{ 
 obj_t BgL_auxz00_10098;
BgL_auxz00_10098 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52032L), BGl_string3554z00zz__objectz00, BgL_vectorz00_3353, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3353)), 
(int)(BgL_iz00_1590)); 
FAILURE(BgL_auxz00_10098,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_10105;
BgL_iz00_10105 = 
(BgL_iz00_1590+1L); 
BgL_iz00_1590 = BgL_iz00_10105; 
goto BgL_zc3z04anonymousza31729ze3z87_1591;} }  else 
{ 
 long BgL_jz00_1596;
BgL_jz00_1596 = 0L; 
BgL_zc3z04anonymousza31732ze3z87_1597:
if(
(BgL_jz00_1596==
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))))
{ 
 long BgL_iz00_10115;
BgL_iz00_10115 = 
(BgL_iz00_1590+1L); 
BgL_iz00_1590 = BgL_iz00_10115; 
goto BgL_zc3z04anonymousza31729ze3z87_1591;}  else 
{ /* Llib/object.scm 1206 */
 bool_t BgL_test4738z00_10117;
{ /* Llib/object.scm 1206 */
 obj_t BgL_arg1740z00_1605;
{ /* Llib/object.scm 1206 */
 obj_t BgL_vectorz00_3363;
if(
VECTORP(BgL_bucketz00_1593))
{ /* Llib/object.scm 1206 */
BgL_vectorz00_3363 = BgL_bucketz00_1593; }  else 
{ 
 obj_t BgL_auxz00_10120;
BgL_auxz00_10120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52237L), BGl_string3574z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_1593); 
FAILURE(BgL_auxz00_10120,BFALSE,BFALSE);} 
BgL_arg1740z00_1605 = 
VECTOR_REF(BgL_vectorz00_3363,BgL_jz00_1596); } 
BgL_test4738z00_10117 = 
(BgL_arg1740z00_1605==BgL_oldzd2defaultzd2_1586); } 
if(BgL_test4738z00_10117)
{ /* Llib/object.scm 1206 */
{ /* Llib/object.scm 1207 */
 obj_t BgL_vectorz00_3365;
if(
VECTORP(BgL_bucketz00_1593))
{ /* Llib/object.scm 1207 */
BgL_vectorz00_3365 = BgL_bucketz00_1593; }  else 
{ 
 obj_t BgL_auxz00_10128;
BgL_auxz00_10128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52278L), BGl_string3574z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_1593); 
FAILURE(BgL_auxz00_10128,BFALSE,BFALSE);} 
{ /* Llib/object.scm 1207 */
 bool_t BgL_test4741z00_10132;
{ /* Llib/object.scm 1207 */
 long BgL_tmpz00_10133;
BgL_tmpz00_10133 = 
VECTOR_LENGTH(BgL_vectorz00_3365); 
BgL_test4741z00_10132 = 
BOUND_CHECK(BgL_jz00_1596, BgL_tmpz00_10133); } 
if(BgL_test4741z00_10132)
{ /* Llib/object.scm 1207 */
VECTOR_SET(BgL_vectorz00_3365,BgL_jz00_1596,BgL_defaultz00_123); }  else 
{ 
 obj_t BgL_auxz00_10137;
BgL_auxz00_10137 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52265L), BGl_string3554z00zz__objectz00, BgL_vectorz00_3365, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3365)), 
(int)(BgL_jz00_1596)); 
FAILURE(BgL_auxz00_10137,BFALSE,BFALSE);} } } 
{ 
 long BgL_jz00_10144;
BgL_jz00_10144 = 
(BgL_jz00_1596+1L); 
BgL_jz00_1596 = BgL_jz00_10144; 
goto BgL_zc3z04anonymousza31732ze3z87_1597;} }  else 
{ 
 long BgL_jz00_10146;
BgL_jz00_10146 = 
(BgL_jz00_1596+1L); 
BgL_jz00_1596 = BgL_jz00_10146; 
goto BgL_zc3z04anonymousza31732ze3z87_1597;} } } }  else 
{ /* Llib/object.scm 1196 */
{ /* Llib/object.scm 1212 */
 obj_t BgL_genericz00_3369;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 1212 */
BgL_genericz00_3369 = BgL_genericz00_122; }  else 
{ 
 obj_t BgL_auxz00_10150;
BgL_auxz00_10150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52405L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10150,BFALSE,BFALSE);} 
PROCEDURE_SET(BgL_genericz00_3369, 
(int)(0L), 
((obj_t)
((obj_t)BgL_defaultz00_123))); } 
{ /* Llib/object.scm 921 */
 obj_t BgL_tmpz00_10158;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 921 */
BgL_tmpz00_10158 = BgL_genericz00_122
; }  else 
{ 
 obj_t BgL_auxz00_10161;
BgL_auxz00_10161 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40283L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10161,BFALSE,BFALSE);} 
PROCEDURE_SET(BgL_tmpz00_10158, 
(int)(2L), BgL_newzd2defzd2bucketz00_1585); } 
FREE_VECTOR_UNCOLLECTABLE(BgL_oldzd2defzd2bucketz00_1584); BUNSPEC; } } } } } }  else 
{ /* Llib/object.scm 1185 */BFALSE; } 
return BUNSPEC;}  else 
{ /* Llib/object.scm 1171 */
 obj_t BgL_defzd2metzd2_1610;
if(
PROCEDUREP(BgL_defaultz00_123))
{ /* Llib/object.scm 1171 */
BgL_defzd2metzd2_1610 = BgL_defaultz00_123; }  else 
{ /* Llib/object.scm 1171 */
BgL_defzd2metzd2_1610 = BGl_genericzd2nozd2defaultzd2behaviorzd2envz00zz__objectz00; } 
{ /* Llib/object.scm 1171 */
 obj_t BgL_defzd2bucketzd2_1611;
{ /* Llib/object.scm 1175 */
 int BgL_arg1746z00_1614;
BgL_arg1746z00_1614 = 
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))); 
BgL_defzd2bucketzd2_1611 = 
make_vector_uncollectable(
(long)(BgL_arg1746z00_1614), BgL_defzd2metzd2_1610); } 
{ /* Llib/object.scm 1174 */

{ /* Llib/object.scm 1176 */
 bool_t BgL_test4745z00_10177;
{ /* Llib/object.scm 1176 */
 long BgL_n1z00_3376; long BgL_n2z00_3377;
{ /* Llib/object.scm 1176 */
 obj_t BgL_tmpz00_10178;
{ /* Llib/object.scm 1176 */
 obj_t BgL_aux3045z00_5793;
BgL_aux3045z00_5793 = BGl_za2nbzd2genericsza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux3045z00_5793))
{ /* Llib/object.scm 1176 */
BgL_tmpz00_10178 = BgL_aux3045z00_5793
; }  else 
{ 
 obj_t BgL_auxz00_10181;
BgL_auxz00_10181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51059L), BGl_string3573z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3045z00_5793); 
FAILURE(BgL_auxz00_10181,BFALSE,BFALSE);} } 
BgL_n1z00_3376 = 
(long)CINT(BgL_tmpz00_10178); } 
{ /* Llib/object.scm 1176 */
 obj_t BgL_tmpz00_10186;
{ /* Llib/object.scm 1176 */
 obj_t BgL_aux3046z00_5794;
BgL_aux3046z00_5794 = BGl_za2nbzd2genericszd2maxza2z00zz__objectz00; 
if(
INTEGERP(BgL_aux3046z00_5794))
{ /* Llib/object.scm 1176 */
BgL_tmpz00_10186 = BgL_aux3046z00_5794
; }  else 
{ 
 obj_t BgL_auxz00_10189;
BgL_auxz00_10189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51073L), BGl_string3573z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3046z00_5794); 
FAILURE(BgL_auxz00_10189,BFALSE,BFALSE);} } 
BgL_n2z00_3377 = 
(long)CINT(BgL_tmpz00_10186); } 
BgL_test4745z00_10177 = 
(BgL_n1z00_3376==BgL_n2z00_3377); } 
if(BgL_test4745z00_10177)
{ /* Llib/object.scm 1176 */
BGl_doublezd2nbzd2genericsz12z12zz__objectz00(); }  else 
{ /* Llib/object.scm 1176 */BFALSE; } } 
{ /* Llib/object.scm 1178 */
 obj_t BgL_vectorz00_3378; long BgL_kz00_3379;
{ /* Llib/object.scm 1178 */
 obj_t BgL_aux3047z00_5795;
BgL_aux3047z00_5795 = BGl_za2genericsza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3047z00_5795))
{ /* Llib/object.scm 1178 */
BgL_vectorz00_3378 = BgL_aux3047z00_5795; }  else 
{ 
 obj_t BgL_auxz00_10198;
BgL_auxz00_10198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51137L), BGl_string3573z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3047z00_5795); 
FAILURE(BgL_auxz00_10198,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1178 */
 obj_t BgL_tmpz00_10202;
{ /* Llib/object.scm 1178 */
 obj_t BgL_aux3049z00_5797;
BgL_aux3049z00_5797 = BGl_za2nbzd2genericsza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux3049z00_5797))
{ /* Llib/object.scm 1178 */
BgL_tmpz00_10202 = BgL_aux3049z00_5797
; }  else 
{ 
 obj_t BgL_auxz00_10205;
BgL_auxz00_10205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51148L), BGl_string3573z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3049z00_5797); 
FAILURE(BgL_auxz00_10205,BFALSE,BFALSE);} } 
BgL_kz00_3379 = 
(long)CINT(BgL_tmpz00_10202); } 
{ /* Llib/object.scm 1178 */
 bool_t BgL_test4750z00_10210;
{ /* Llib/object.scm 1178 */
 long BgL_tmpz00_10211;
BgL_tmpz00_10211 = 
VECTOR_LENGTH(BgL_vectorz00_3378); 
BgL_test4750z00_10210 = 
BOUND_CHECK(BgL_kz00_3379, BgL_tmpz00_10211); } 
if(BgL_test4750z00_10210)
{ /* Llib/object.scm 1178 */
VECTOR_SET(BgL_vectorz00_3378,BgL_kz00_3379,BgL_genericz00_122); }  else 
{ 
 obj_t BgL_auxz00_10215;
BgL_auxz00_10215 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51124L), BGl_string3554z00zz__objectz00, BgL_vectorz00_3378, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3378)), 
(int)(BgL_kz00_3379)); 
FAILURE(BgL_auxz00_10215,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_tmpz00_10222;
{ /* Llib/object.scm 1179 */
 obj_t BgL_aux3050z00_5798;
BgL_aux3050z00_5798 = BGl_za2nbzd2genericsza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux3050z00_5798))
{ /* Llib/object.scm 1179 */
BgL_tmpz00_10222 = BgL_aux3050z00_5798
; }  else 
{ 
 obj_t BgL_auxz00_10225;
BgL_auxz00_10225 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51199L), BGl_string3573z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3050z00_5798); 
FAILURE(BgL_auxz00_10225,BFALSE,BFALSE);} } 
BGl_za2nbzd2genericsza2zd2zz__objectz00 = 
ADDFX(BgL_tmpz00_10222, 
BINT(1L)); } 
{ /* Llib/object.scm 1180 */
 obj_t BgL_genericz00_3381; obj_t BgL_defaultz00_3382;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 1180 */
BgL_genericz00_3381 = BgL_genericz00_122; }  else 
{ 
 obj_t BgL_auxz00_10233;
BgL_auxz00_10233 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51242L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10233,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_defzd2metzd2_1610))
{ /* Llib/object.scm 1180 */
BgL_defaultz00_3382 = BgL_defzd2metzd2_1610; }  else 
{ 
 obj_t BgL_auxz00_10239;
BgL_auxz00_10239 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51250L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_defzd2metzd2_1610); 
FAILURE(BgL_auxz00_10239,BFALSE,BFALSE);} 
PROCEDURE_SET(BgL_genericz00_3381, 
(int)(0L), 
((obj_t)BgL_defaultz00_3382)); } 
{ /* Llib/object.scm 921 */
 obj_t BgL_tmpz00_10246;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 921 */
BgL_tmpz00_10246 = BgL_genericz00_122
; }  else 
{ 
 obj_t BgL_auxz00_10249;
BgL_auxz00_10249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(40283L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10249,BFALSE,BFALSE);} 
PROCEDURE_SET(BgL_tmpz00_10246, 
(int)(2L), BgL_defzd2bucketzd2_1611); } 
{ /* Llib/object.scm 1182 */
 obj_t BgL_arg1745z00_1613;
BgL_arg1745z00_1613 = 
BGl_makezd2methodzd2arrayz00zz__objectz00(BgL_defzd2bucketzd2_1611); 
{ /* Llib/object.scm 1182 */
 obj_t BgL_genericz00_3384;
if(
PROCEDUREP(BgL_genericz00_122))
{ /* Llib/object.scm 1182 */
BgL_genericz00_3384 = BgL_genericz00_122; }  else 
{ 
 obj_t BgL_auxz00_10258;
BgL_auxz00_10258 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(51341L), BGl_string3573z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_122); 
FAILURE(BgL_auxz00_10258,BFALSE,BFALSE);} 
PROCEDURE_SET(BgL_genericz00_3384, 
(int)(1L), BgL_arg1745z00_1613); } } 
return BUNSPEC;} } } } } 

}



/* %add-method! */
obj_t BGl_z52addzd2methodz12z92zz__objectz00(obj_t BgL_genericz00_125, obj_t BgL_classz00_126, obj_t BgL_methodz00_127)
{
{ /* Llib/object.scm 1221 */
{ /* Llib/object.scm 1222 */
 obj_t BgL_top4757z00_10265;
BgL_top4757z00_10265 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top4757z00_10265, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1222 */
 obj_t BgL_tmp4756z00_10264;
{ /* Llib/object.scm 1223 */
 bool_t BgL_test4758z00_10269;
{ /* Llib/object.scm 1223 */
 obj_t BgL_genericz00_3386;
if(
PROCEDUREP(BgL_genericz00_125))
{ /* Llib/object.scm 1223 */
BgL_genericz00_3386 = BgL_genericz00_125; }  else 
{ 
 obj_t BgL_auxz00_10272;
BgL_auxz00_10272 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(52909L), BGl_string3575z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_125); 
FAILURE(BgL_auxz00_10272,BFALSE,BFALSE);} 
{ /* Llib/object.scm 1124 */
 obj_t BgL_tmpz00_10276;
BgL_tmpz00_10276 = 
PROCEDURE_REF(BgL_genericz00_3386, 
(int)(1L)); 
BgL_test4758z00_10269 = 
VECTORP(BgL_tmpz00_10276); } } 
if(BgL_test4758z00_10269)
{ /* Llib/object.scm 1223 */BFALSE; }  else 
{ /* Llib/object.scm 1223 */
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BgL_genericz00_125, BFALSE, BGl_string3576z00zz__objectz00); } } 
{ /* Llib/object.scm 1227 */
 obj_t BgL_methodzd2arrayzd2_1617;
{ /* Llib/object.scm 1227 */
 obj_t BgL_genericz00_3389;
if(
PROCEDUREP(BgL_genericz00_125))
{ /* Llib/object.scm 1227 */
BgL_genericz00_3389 = BgL_genericz00_125; }  else 
{ 
 obj_t BgL_auxz00_10283;
BgL_auxz00_10283 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53106L), BGl_string3575z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_125); 
FAILURE(BgL_auxz00_10283,BFALSE,BFALSE);} 
BgL_methodzd2arrayzd2_1617 = 
PROCEDURE_REF(BgL_genericz00_3389, 
(int)(1L)); } 
{ /* Llib/object.scm 1227 */
 long BgL_cnumz00_1618;
{ /* Llib/object.scm 1228 */
 obj_t BgL_classz00_3390;
if(
BGL_CLASSP(BgL_classz00_126))
{ /* Llib/object.scm 1228 */
BgL_classz00_3390 = BgL_classz00_126; }  else 
{ 
 obj_t BgL_auxz00_10291;
BgL_auxz00_10291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53141L), BGl_string3575z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_126); 
FAILURE(BgL_auxz00_10291,BFALSE,BFALSE);} 
BgL_cnumz00_1618 = 
BGL_CLASS_INDEX(BgL_classz00_3390); } 
{ /* Llib/object.scm 1228 */
 obj_t BgL_previousz00_1619;
{ /* Llib/object.scm 1229 */
 obj_t BgL_arrayz00_3392; int BgL_offsetz00_3393;
if(
VECTORP(BgL_methodzd2arrayzd2_1617))
{ /* Llib/object.scm 1229 */
BgL_arrayz00_3392 = BgL_methodzd2arrayzd2_1617; }  else 
{ 
 obj_t BgL_auxz00_10298;
BgL_auxz00_10298 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53191L), BGl_string3575z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_methodzd2arrayzd2_1617); 
FAILURE(BgL_auxz00_10298,BFALSE,BFALSE);} 
BgL_offsetz00_3393 = 
(int)(BgL_cnumz00_1618); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_3394;
BgL_offsetz00_3394 = 
(
(long)(BgL_offsetz00_3393)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_3395;
BgL_modz00_3395 = 
(BgL_offsetz00_3394 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_3397;
BgL_restz00_3397 = 
(BgL_offsetz00_3394 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_3399;
BgL_bucketz00_3399 = 
VECTOR_REF(BgL_arrayz00_3392,BgL_modz00_3395); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_3416;
if(
VECTORP(BgL_bucketz00_3399))
{ /* Llib/object.scm 935 */
BgL_vectorz00_3416 = BgL_bucketz00_3399; }  else 
{ 
 obj_t BgL_auxz00_10322;
BgL_auxz00_10322 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3575z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_3399); 
FAILURE(BgL_auxz00_10322,BFALSE,BFALSE);} 
BgL_previousz00_1619 = 
VECTOR_REF(BgL_vectorz00_3416,BgL_restz00_3397); } } } } } } } 
{ /* Llib/object.scm 1229 */
 obj_t BgL_defz00_1620;
{ /* Llib/object.scm 1230 */
 obj_t BgL_res2600z00_3419;
{ /* Llib/object.scm 1230 */
 obj_t BgL_genericz00_3418;
if(
PROCEDUREP(BgL_genericz00_125))
{ /* Llib/object.scm 1230 */
BgL_genericz00_3418 = BgL_genericz00_125; }  else 
{ 
 obj_t BgL_auxz00_10329;
BgL_auxz00_10329 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53239L), BGl_string3575z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_125); 
FAILURE(BgL_auxz00_10329,BFALSE,BFALSE);} 
{ /* Llib/object.scm 900 */
 obj_t BgL_aux3071z00_5819;
BgL_aux3071z00_5819 = 
PROCEDURE_REF(BgL_genericz00_3418, 
(int)(0L)); 
if(
PROCEDUREP(BgL_aux3071z00_5819))
{ /* Llib/object.scm 900 */
BgL_res2600z00_3419 = BgL_aux3071z00_5819; }  else 
{ 
 obj_t BgL_auxz00_10337;
BgL_auxz00_10337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3575z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3071z00_5819); 
FAILURE(BgL_auxz00_10337,BFALSE,BFALSE);} } } 
BgL_defz00_1620 = BgL_res2600z00_3419; } 
{ /* Llib/object.scm 1230 */

BGl_loopze70ze7zz__objectz00(BgL_defz00_1620, BgL_previousz00_1619, BgL_methodz00_127, BgL_genericz00_125, BgL_methodzd2arrayzd2_1617, BgL_classz00_126); } } } } } 
BgL_tmp4756z00_10264 = BgL_methodz00_127; 
BGL_EXITD_POP_PROTECT(BgL_top4757z00_10265); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); 
return BgL_tmp4756z00_10264;} } } 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__objectz00(obj_t BgL_defz00_5408, obj_t BgL_previousz00_5407, obj_t BgL_methodz00_5406, obj_t BgL_genericz00_5405, obj_t BgL_methodzd2arrayzd2_5404, obj_t BgL_claza7za7z00_1622)
{
{ /* Llib/object.scm 1231 */
{ /* Llib/object.scm 1232 */
 long BgL_cnz00_1624;
{ /* Llib/object.scm 1232 */
 obj_t BgL_classz00_3420;
if(
BGL_CLASSP(BgL_claza7za7z00_1622))
{ /* Llib/object.scm 1232 */
BgL_classz00_3420 = BgL_claza7za7z00_1622; }  else 
{ 
 obj_t BgL_auxz00_10346;
BgL_auxz00_10346 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53307L), BGl_string3577z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_claza7za7z00_1622); 
FAILURE(BgL_auxz00_10346,BFALSE,BFALSE);} 
BgL_cnz00_1624 = 
BGL_CLASS_INDEX(BgL_classz00_3420); } 
{ /* Llib/object.scm 1232 */
 obj_t BgL_currentz00_1625;
{ /* Llib/object.scm 1233 */
 obj_t BgL_arrayz00_3422; int BgL_offsetz00_3423;
if(
VECTORP(BgL_methodzd2arrayzd2_5404))
{ /* Llib/object.scm 1233 */
BgL_arrayz00_3422 = BgL_methodzd2arrayzd2_5404; }  else 
{ 
 obj_t BgL_auxz00_10353;
BgL_auxz00_10353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53355L), BGl_string3577z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_methodzd2arrayzd2_5404); 
FAILURE(BgL_auxz00_10353,BFALSE,BFALSE);} 
BgL_offsetz00_3423 = 
(int)(BgL_cnz00_1624); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_3424;
BgL_offsetz00_3424 = 
(
(long)(BgL_offsetz00_3423)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_3425;
BgL_modz00_3425 = 
(BgL_offsetz00_3424 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_3427;
BgL_restz00_3427 = 
(BgL_offsetz00_3424 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_3429;
BgL_bucketz00_3429 = 
VECTOR_REF(BgL_arrayz00_3422,BgL_modz00_3425); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_3446;
if(
VECTORP(BgL_bucketz00_3429))
{ /* Llib/object.scm 935 */
BgL_vectorz00_3446 = BgL_bucketz00_3429; }  else 
{ 
 obj_t BgL_auxz00_10377;
BgL_auxz00_10377 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3577z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_3429); 
FAILURE(BgL_auxz00_10377,BFALSE,BFALSE);} 
BgL_currentz00_1625 = 
VECTOR_REF(BgL_vectorz00_3446,BgL_restz00_3427); } } } } } } } 
{ /* Llib/object.scm 1233 */

{ /* Llib/object.scm 1234 */
 bool_t BgL_test4769z00_10382;
if(
(BgL_currentz00_1625==BgL_defz00_5408))
{ /* Llib/object.scm 1234 */
BgL_test4769z00_10382 = ((bool_t)1)
; }  else 
{ /* Llib/object.scm 1234 */
BgL_test4769z00_10382 = 
(BgL_currentz00_1625==BgL_previousz00_5407)
; } 
if(BgL_test4769z00_10382)
{ /* Llib/object.scm 1234 */
BGl_methodzd2arrayzd2setz12z12zz__objectz00(BgL_genericz00_5405, BgL_methodzd2arrayzd2_5404, BgL_cnz00_1624, BgL_methodz00_5406); 
{ /* Llib/object.scm 1239 */
 obj_t BgL_g1327z00_1627;
{ /* Llib/object.scm 1239 */
 obj_t BgL_classz00_3448;
if(
BGL_CLASSP(BgL_claza7za7z00_1622))
{ /* Llib/object.scm 1239 */
BgL_classz00_3448 = BgL_claza7za7z00_1622; }  else 
{ 
 obj_t BgL_auxz00_10389;
BgL_auxz00_10389 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53625L), BGl_string3577z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_claza7za7z00_1622); 
FAILURE(BgL_auxz00_10389,BFALSE,BFALSE);} 
BgL_g1327z00_1627 = 
BGL_CLASS_SUBCLASSES(BgL_classz00_3448); } 
{ 
 obj_t BgL_l1325z00_1629;
BgL_l1325z00_1629 = BgL_g1327z00_1627; 
BgL_zc3z04anonymousza31751ze3z87_1630:
if(
PAIRP(BgL_l1325z00_1629))
{ /* Llib/object.scm 1239 */
BGl_loopze70ze7zz__objectz00(BgL_defz00_5408, BgL_previousz00_5407, BgL_methodz00_5406, BgL_genericz00_5405, BgL_methodzd2arrayzd2_5404, 
CAR(BgL_l1325z00_1629)); 
{ 
 obj_t BgL_l1325z00_10398;
BgL_l1325z00_10398 = 
CDR(BgL_l1325z00_1629); 
BgL_l1325z00_1629 = BgL_l1325z00_10398; 
goto BgL_zc3z04anonymousza31751ze3z87_1630;} }  else 
{ /* Llib/object.scm 1239 */
if(
NULLP(BgL_l1325z00_1629))
{ /* Llib/object.scm 1239 */
return BTRUE;}  else 
{ /* Llib/object.scm 1239 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3566z00zz__objectz00, BGl_string3552z00zz__objectz00, BgL_l1325z00_1629, BGl_string3441z00zz__objectz00, 
BINT(53592L));} } } } }  else 
{ /* Llib/object.scm 1234 */
return BFALSE;} } } } } } 

}



/* generic-add-method! */
BGL_EXPORTED_DEF obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t BgL_genericz00_128, obj_t BgL_classz00_129, obj_t BgL_methodz00_130, obj_t BgL_namez00_131)
{
{ /* Llib/object.scm 1245 */
if(
BGL_CLASSP(BgL_classz00_129))
{ /* Llib/object.scm 1249 */
 bool_t BgL_test4775z00_10406;
{ /* Llib/object.scm 1249 */
 bool_t BgL_test4776z00_10407;
{ /* Llib/object.scm 1249 */
 int BgL_arg1769z00_1651; int BgL_arg1770z00_1652;
BgL_arg1769z00_1651 = 
PROCEDURE_ARITY(BgL_genericz00_128); 
BgL_arg1770z00_1652 = 
PROCEDURE_ARITY(BgL_methodz00_130); 
BgL_test4776z00_10407 = 
(
(long)(BgL_arg1769z00_1651)==
(long)(BgL_arg1770z00_1652)); } 
if(BgL_test4776z00_10407)
{ /* Llib/object.scm 1249 */
BgL_test4775z00_10406 = ((bool_t)0)
; }  else 
{ /* Llib/object.scm 1250 */
 int BgL_arg1768z00_1650;
BgL_arg1768z00_1650 = 
PROCEDURE_ARITY(BgL_genericz00_128); 
BgL_test4775z00_10406 = 
(
(long)(BgL_arg1768z00_1650)>=0L); } } 
if(BgL_test4775z00_10406)
{ /* Llib/object.scm 1252 */
 obj_t BgL_arg1764z00_1645; int BgL_arg1765z00_1646;
{ /* Llib/object.scm 1252 */
 int BgL_arg1766z00_1647;
BgL_arg1766z00_1647 = 
PROCEDURE_ARITY(BgL_genericz00_128); 
{ /* Llib/object.scm 1251 */
 obj_t BgL_list1767z00_1648;
BgL_list1767z00_1648 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1766z00_1647), BNIL); 
BgL_arg1764z00_1645 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string3578z00zz__objectz00, BgL_list1767z00_1648); } } 
BgL_arg1765z00_1646 = 
PROCEDURE_ARITY(BgL_methodz00_130); 
{ /* Llib/object.scm 1251 */
 obj_t BgL_aux3081z00_5829;
BgL_aux3081z00_5829 = 
BGl_errorz00zz__errorz00(BgL_namez00_131, BgL_arg1764z00_1645, 
BINT(BgL_arg1765z00_1646)); 
if(
PROCEDUREP(BgL_aux3081z00_5829))
{ /* Llib/object.scm 1251 */
return BgL_aux3081z00_5829;}  else 
{ 
 obj_t BgL_auxz00_10425;
BgL_auxz00_10425 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(54146L), BGl_string3579z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3081z00_5829); 
FAILURE(BgL_auxz00_10425,BFALSE,BFALSE);} } }  else 
{ /* Llib/object.scm 1255 */
 obj_t BgL_aux3083z00_5831;
BgL_aux3083z00_5831 = 
BGl_z52addzd2methodz12z92zz__objectz00(BgL_genericz00_128, BgL_classz00_129, BgL_methodz00_130); 
if(
PROCEDUREP(BgL_aux3083z00_5831))
{ /* Llib/object.scm 1255 */
return BgL_aux3083z00_5831;}  else 
{ 
 obj_t BgL_auxz00_10432;
BgL_auxz00_10432 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(54296L), BGl_string3579z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3083z00_5831); 
FAILURE(BgL_auxz00_10432,BFALSE,BFALSE);} } }  else 
{ /* Llib/object.scm 1248 */
 obj_t BgL_aux3085z00_5833;
BgL_aux3085z00_5833 = 
BGl_errorz00zz__errorz00(BgL_namez00_131, BGl_string3580z00zz__objectz00, BgL_classz00_129); 
if(
PROCEDUREP(BgL_aux3085z00_5833))
{ /* Llib/object.scm 1248 */
return BgL_aux3085z00_5833;}  else 
{ 
 obj_t BgL_auxz00_10439;
BgL_auxz00_10439 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53976L), BGl_string3579z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3085z00_5833); 
FAILURE(BgL_auxz00_10439,BFALSE,BFALSE);} } } 

}



/* &generic-add-method! */
obj_t BGl_z62genericzd2addzd2methodz12z70zz__objectz00(obj_t BgL_envz00_4882, obj_t BgL_genericz00_4883, obj_t BgL_classz00_4884, obj_t BgL_methodz00_4885, obj_t BgL_namez00_4886)
{
{ /* Llib/object.scm 1245 */
{ /* Llib/object.scm 1247 */
 obj_t BgL_auxz00_10450; obj_t BgL_auxz00_10443;
if(
PROCEDUREP(BgL_methodz00_4885))
{ /* Llib/object.scm 1247 */
BgL_auxz00_10450 = BgL_methodz00_4885
; }  else 
{ 
 obj_t BgL_auxz00_10453;
BgL_auxz00_10453 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53947L), BGl_string3581z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_methodz00_4885); 
FAILURE(BgL_auxz00_10453,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_genericz00_4883))
{ /* Llib/object.scm 1247 */
BgL_auxz00_10443 = BgL_genericz00_4883
; }  else 
{ 
 obj_t BgL_auxz00_10446;
BgL_auxz00_10446 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(53947L), BGl_string3581z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4883); 
FAILURE(BgL_auxz00_10446,BFALSE,BFALSE);} 
return 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BgL_auxz00_10443, BgL_classz00_4884, BgL_auxz00_10450, BgL_namez00_4886);} } 

}



/* generic-add-eval-method! */
BGL_EXPORTED_DEF obj_t BGl_genericzd2addzd2evalzd2methodz12zc0zz__objectz00(obj_t BgL_genericz00_132, obj_t BgL_classz00_133, obj_t BgL_methodz00_134, obj_t BgL_namez00_135)
{
{ /* Llib/object.scm 1265 */
if(
BGL_CLASSP(BgL_classz00_133))
{ /* Llib/object.scm 1269 */
 bool_t BgL_test4783z00_10460;
{ /* Llib/object.scm 1269 */
 bool_t BgL_test4784z00_10461;
{ /* Llib/object.scm 1269 */
 int BgL_arg1791z00_1672; int BgL_arg1792z00_1673;
BgL_arg1791z00_1672 = 
PROCEDURE_ARITY(BgL_genericz00_132); 
BgL_arg1792z00_1673 = 
PROCEDURE_ARITY(BgL_methodz00_134); 
BgL_test4784z00_10461 = 
(
(long)(BgL_arg1791z00_1672)==
(long)(BgL_arg1792z00_1673)); } 
if(BgL_test4784z00_10461)
{ /* Llib/object.scm 1269 */
BgL_test4783z00_10460 = ((bool_t)0)
; }  else 
{ /* Llib/object.scm 1270 */
 bool_t BgL_test4785z00_10467;
{ /* Llib/object.scm 1270 */
 int BgL_arg1790z00_1671;
BgL_arg1790z00_1671 = 
PROCEDURE_ARITY(BgL_genericz00_132); 
BgL_test4785z00_10467 = 
(
(long)(BgL_arg1790z00_1671)>4L); } 
if(BgL_test4785z00_10467)
{ /* Llib/object.scm 1271 */
 int BgL_arg1789z00_1670;
BgL_arg1789z00_1670 = 
PROCEDURE_ARITY(BgL_methodz00_134); 
BgL_test4783z00_10460 = 
(
(long)(BgL_arg1789z00_1670)>=0L); }  else 
{ /* Llib/object.scm 1270 */
BgL_test4783z00_10460 = ((bool_t)0)
; } } } 
if(BgL_test4783z00_10460)
{ /* Llib/object.scm 1273 */
 obj_t BgL_arg1785z00_1664; int BgL_arg1786z00_1665;
{ /* Llib/object.scm 1273 */
 int BgL_arg1787z00_1666;
BgL_arg1787z00_1666 = 
PROCEDURE_ARITY(BgL_genericz00_132); 
{ /* Llib/object.scm 1272 */
 obj_t BgL_list1788z00_1667;
BgL_list1788z00_1667 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1787z00_1666), BNIL); 
BgL_arg1785z00_1664 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string3578z00zz__objectz00, BgL_list1788z00_1667); } } 
BgL_arg1786z00_1665 = 
PROCEDURE_ARITY(BgL_methodz00_134); 
{ /* Llib/object.scm 1272 */
 obj_t BgL_aux3091z00_5839;
BgL_aux3091z00_5839 = 
BGl_errorz00zz__errorz00(BgL_namez00_135, BgL_arg1785z00_1664, 
BINT(BgL_arg1786z00_1665)); 
if(
PROCEDUREP(BgL_aux3091z00_5839))
{ /* Llib/object.scm 1272 */
return BgL_aux3091z00_5839;}  else 
{ 
 obj_t BgL_auxz00_10483;
BgL_auxz00_10483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55241L), BGl_string3582z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3091z00_5839); 
FAILURE(BgL_auxz00_10483,BFALSE,BFALSE);} } }  else 
{ /* Llib/object.scm 1276 */
 obj_t BgL_aux3093z00_5841;
BgL_aux3093z00_5841 = 
BGl_z52addzd2methodz12z92zz__objectz00(BgL_genericz00_132, BgL_classz00_133, BgL_methodz00_134); 
if(
PROCEDUREP(BgL_aux3093z00_5841))
{ /* Llib/object.scm 1276 */
return BgL_aux3093z00_5841;}  else 
{ 
 obj_t BgL_auxz00_10490;
BgL_auxz00_10490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55391L), BGl_string3582z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3093z00_5841); 
FAILURE(BgL_auxz00_10490,BFALSE,BFALSE);} } }  else 
{ /* Llib/object.scm 1268 */
 obj_t BgL_aux3095z00_5843;
BgL_aux3095z00_5843 = 
BGl_errorz00zz__errorz00(BgL_namez00_135, BGl_string3580z00zz__objectz00, BgL_classz00_133); 
if(
PROCEDUREP(BgL_aux3095z00_5843))
{ /* Llib/object.scm 1268 */
return BgL_aux3095z00_5843;}  else 
{ 
 obj_t BgL_auxz00_10497;
BgL_auxz00_10497 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55033L), BGl_string3582z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3095z00_5843); 
FAILURE(BgL_auxz00_10497,BFALSE,BFALSE);} } } 

}



/* &generic-add-eval-method! */
obj_t BGl_z62genericzd2addzd2evalzd2methodz12za2zz__objectz00(obj_t BgL_envz00_4887, obj_t BgL_genericz00_4888, obj_t BgL_classz00_4889, obj_t BgL_methodz00_4890, obj_t BgL_namez00_4891)
{
{ /* Llib/object.scm 1265 */
{ /* Llib/object.scm 1267 */
 obj_t BgL_auxz00_10508; obj_t BgL_auxz00_10501;
if(
PROCEDUREP(BgL_methodz00_4890))
{ /* Llib/object.scm 1267 */
BgL_auxz00_10508 = BgL_methodz00_4890
; }  else 
{ 
 obj_t BgL_auxz00_10511;
BgL_auxz00_10511 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55004L), BGl_string3583z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_methodz00_4890); 
FAILURE(BgL_auxz00_10511,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_genericz00_4888))
{ /* Llib/object.scm 1267 */
BgL_auxz00_10501 = BgL_genericz00_4888
; }  else 
{ 
 obj_t BgL_auxz00_10504;
BgL_auxz00_10504 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55004L), BGl_string3583z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4888); 
FAILURE(BgL_auxz00_10504,BFALSE,BFALSE);} 
return 
BGl_genericzd2addzd2evalzd2methodz12zc0zz__objectz00(BgL_auxz00_10501, BgL_classz00_4889, BgL_auxz00_10508, BgL_namez00_4891);} } 

}



/* find-method */
BGL_EXPORTED_DEF obj_t BGl_findzd2methodzd2zz__objectz00(BgL_objectz00_bglt BgL_objz00_136, obj_t BgL_genericz00_137)
{
{ /* Llib/object.scm 1283 */
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_6467;
BgL_objzd2classzd2numz00_6467 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_136); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_6468;
BgL_arg1793z00_6468 = 
PROCEDURE_REF(BgL_genericz00_137, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arrayz00_6469; int BgL_offsetz00_6470;
if(
VECTORP(BgL_arg1793z00_6468))
{ /* Llib/object.scm 1285 */
BgL_arrayz00_6469 = BgL_arg1793z00_6468; }  else 
{ 
 obj_t BgL_auxz00_10521;
BgL_auxz00_10521 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55952L), BGl_string3584z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arg1793z00_6468); 
FAILURE(BgL_auxz00_10521,BFALSE,BFALSE);} 
BgL_offsetz00_6470 = 
(int)(BgL_objzd2classzd2numz00_6467); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_6471;
BgL_offsetz00_6471 = 
(
(long)(BgL_offsetz00_6470)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_6472;
BgL_modz00_6472 = 
(BgL_offsetz00_6471 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_6473;
BgL_restz00_6473 = 
(BgL_offsetz00_6471 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_6474;
BgL_bucketz00_6474 = 
VECTOR_REF(BgL_arrayz00_6469,BgL_modz00_6472); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_6475;
if(
VECTORP(BgL_bucketz00_6474))
{ /* Llib/object.scm 935 */
BgL_vectorz00_6475 = BgL_bucketz00_6474; }  else 
{ 
 obj_t BgL_auxz00_10545;
BgL_auxz00_10545 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3584z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_6474); 
FAILURE(BgL_auxz00_10545,BFALSE,BFALSE);} 
{ /* Llib/object.scm 935 */
 obj_t BgL_aux3105z00_6476;
BgL_aux3105z00_6476 = 
VECTOR_REF(BgL_vectorz00_6475,BgL_restz00_6473); 
if(
PROCEDUREP(BgL_aux3105z00_6476))
{ /* Llib/object.scm 935 */
return BgL_aux3105z00_6476;}  else 
{ 
 obj_t BgL_auxz00_10552;
BgL_auxz00_10552 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41113L), BGl_string3584z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3105z00_6476); 
FAILURE(BgL_auxz00_10552,BFALSE,BFALSE);} } } } } } } } } } } } 

}



/* &find-method */
obj_t BGl_z62findzd2methodzb0zz__objectz00(obj_t BgL_envz00_4892, obj_t BgL_objz00_4893, obj_t BgL_genericz00_4894)
{
{ /* Llib/object.scm 1283 */
{ /* Llib/object.scm 1284 */
 obj_t BgL_auxz00_10564; BgL_objectz00_bglt BgL_auxz00_10556;
if(
PROCEDUREP(BgL_genericz00_4894))
{ /* Llib/object.scm 1284 */
BgL_auxz00_10564 = BgL_genericz00_4894
; }  else 
{ 
 obj_t BgL_auxz00_10567;
BgL_auxz00_10567 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55845L), BGl_string3585z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4894); 
FAILURE(BgL_auxz00_10567,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4893, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1284 */
BgL_auxz00_10556 = 
((BgL_objectz00_bglt)BgL_objz00_4893)
; }  else 
{ 
 obj_t BgL_auxz00_10560;
BgL_auxz00_10560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(55845L), BGl_string3585z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4893); 
FAILURE(BgL_auxz00_10560,BFALSE,BFALSE);} 
return 
BGl_findzd2methodzd2zz__objectz00(BgL_auxz00_10556, BgL_auxz00_10564);} } 

}



/* find-super-class-method */
BGL_EXPORTED_DEF obj_t BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt BgL_objz00_138, obj_t BgL_genericz00_139, obj_t BgL_classz00_140)
{
{ /* Llib/object.scm 1292 */
{ /* Llib/object.scm 1293 */
 obj_t BgL_g1219z00_1676;
{ /* Llib/object.scm 1293 */
 obj_t BgL_classz00_3500;
if(
BGL_CLASSP(BgL_classz00_140))
{ /* Llib/object.scm 1293 */
BgL_classz00_3500 = BgL_classz00_140; }  else 
{ 
 obj_t BgL_auxz00_10574;
BgL_auxz00_10574 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56428L), BGl_string3586z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_140); 
FAILURE(BgL_auxz00_10574,BFALSE,BFALSE);} 
BgL_g1219z00_1676 = 
BGL_CLASS_SUPER(BgL_classz00_3500); } 
{ 
 obj_t BgL_superz00_1678;
{ /* Llib/object.scm 1293 */
 obj_t BgL_aux3123z00_5873;
BgL_superz00_1678 = BgL_g1219z00_1676; 
BgL_zc3z04anonymousza31794ze3z87_1679:
if(
BGL_CLASSP(BgL_superz00_1678))
{ /* Llib/object.scm 1296 */
 long BgL_objzd2superzd2classzd2numzd2_1681;
{ /* Llib/object.scm 1296 */
 obj_t BgL_classz00_3501;
if(
BGL_CLASSP(BgL_superz00_1678))
{ /* Llib/object.scm 1296 */
BgL_classz00_3501 = BgL_superz00_1678; }  else 
{ 
 obj_t BgL_auxz00_10583;
BgL_auxz00_10583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56540L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_1678); 
FAILURE(BgL_auxz00_10583,BFALSE,BFALSE);} 
BgL_objzd2superzd2classzd2numzd2_1681 = 
BGL_CLASS_INDEX(BgL_classz00_3501); } 
{ /* Llib/object.scm 1297 */
 obj_t BgL_methodz00_1682;
{ /* Llib/object.scm 1298 */
 obj_t BgL_arg1796z00_1684;
BgL_arg1796z00_1684 = 
PROCEDURE_REF(BgL_genericz00_139, 
(int)(1L)); 
{ /* Llib/object.scm 1297 */
 obj_t BgL_arrayz00_3504; int BgL_offsetz00_3505;
if(
VECTORP(BgL_arg1796z00_1684))
{ /* Llib/object.scm 1298 */
BgL_arrayz00_3504 = BgL_arg1796z00_1684; }  else 
{ 
 obj_t BgL_auxz00_10592;
BgL_auxz00_10592 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56633L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arg1796z00_1684); 
FAILURE(BgL_auxz00_10592,BFALSE,BFALSE);} 
BgL_offsetz00_3505 = 
(int)(BgL_objzd2superzd2classzd2numzd2_1681); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_3506;
BgL_offsetz00_3506 = 
(
(long)(BgL_offsetz00_3505)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_3507;
BgL_modz00_3507 = 
(BgL_offsetz00_3506 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_3509;
BgL_restz00_3509 = 
(BgL_offsetz00_3506 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_3511;
BgL_bucketz00_3511 = 
VECTOR_REF(BgL_arrayz00_3504,BgL_modz00_3507); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_3528;
if(
VECTORP(BgL_bucketz00_3511))
{ /* Llib/object.scm 935 */
BgL_vectorz00_3528 = BgL_bucketz00_3511; }  else 
{ 
 obj_t BgL_auxz00_10616;
BgL_auxz00_10616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_3511); 
FAILURE(BgL_auxz00_10616,BFALSE,BFALSE);} 
BgL_methodz00_1682 = 
VECTOR_REF(BgL_vectorz00_3528,BgL_restz00_3509); } } } } } } } } 
if(
CBOOL(BgL_methodz00_1682))
{ /* Llib/object.scm 1300 */
BgL_aux3123z00_5873 = BgL_methodz00_1682; }  else 
{ /* Llib/object.scm 1302 */
 obj_t BgL_newzd2superzd2_1683;
{ /* Llib/object.scm 1302 */
 obj_t BgL_classz00_3530;
if(
BGL_CLASSP(BgL_superz00_1678))
{ /* Llib/object.scm 1302 */
BgL_classz00_3530 = BgL_superz00_1678; }  else 
{ 
 obj_t BgL_auxz00_10625;
BgL_auxz00_10625 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56729L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_1678); 
FAILURE(BgL_auxz00_10625,BFALSE,BFALSE);} 
BgL_newzd2superzd2_1683 = 
BGL_CLASS_SUPER(BgL_classz00_3530); } 
{ 
 obj_t BgL_superz00_10630;
BgL_superz00_10630 = BgL_newzd2superzd2_1683; 
BgL_superz00_1678 = BgL_superz00_10630; 
goto BgL_zc3z04anonymousza31794ze3z87_1679;} } } }  else 
{ /* Llib/object.scm 1295 */
 obj_t BgL_res2613z00_3532;
{ /* Llib/object.scm 900 */
 obj_t BgL_aux3121z00_5871;
BgL_aux3121z00_5871 = 
PROCEDURE_REF(BgL_genericz00_139, 
(int)(0L)); 
if(
PROCEDUREP(BgL_aux3121z00_5871))
{ /* Llib/object.scm 900 */
BgL_res2613z00_3532 = BgL_aux3121z00_5871; }  else 
{ 
 obj_t BgL_auxz00_10635;
BgL_auxz00_10635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3121z00_5871); 
FAILURE(BgL_auxz00_10635,BFALSE,BFALSE);} } 
BgL_aux3123z00_5873 = BgL_res2613z00_3532; } 
if(
PROCEDUREP(BgL_aux3123z00_5873))
{ /* Llib/object.scm 1293 */
return BgL_aux3123z00_5873;}  else 
{ 
 obj_t BgL_auxz00_10641;
BgL_auxz00_10641 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56397L), BGl_string3586z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3123z00_5873); 
FAILURE(BgL_auxz00_10641,BFALSE,BFALSE);} } } } } 

}



/* &find-super-class-method */
obj_t BGl_z62findzd2superzd2classzd2methodzb0zz__objectz00(obj_t BgL_envz00_4895, obj_t BgL_objz00_4896, obj_t BgL_genericz00_4897, obj_t BgL_classz00_4898)
{
{ /* Llib/object.scm 1292 */
{ /* Llib/object.scm 1293 */
 obj_t BgL_auxz00_10653; BgL_objectz00_bglt BgL_auxz00_10645;
if(
PROCEDUREP(BgL_genericz00_4897))
{ /* Llib/object.scm 1293 */
BgL_auxz00_10653 = BgL_genericz00_4897
; }  else 
{ 
 obj_t BgL_auxz00_10656;
BgL_auxz00_10656 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56397L), BGl_string3587z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4897); 
FAILURE(BgL_auxz00_10656,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4896, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1293 */
BgL_auxz00_10645 = 
((BgL_objectz00_bglt)BgL_objz00_4896)
; }  else 
{ 
 obj_t BgL_auxz00_10649;
BgL_auxz00_10649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(56397L), BGl_string3587z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4896); 
FAILURE(BgL_auxz00_10649,BFALSE,BFALSE);} 
return 
BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_auxz00_10645, BgL_auxz00_10653, BgL_classz00_4898);} } 

}



/* find-method-from */
BGL_EXPORTED_DEF obj_t BGl_findzd2methodzd2fromz00zz__objectz00(BgL_objectz00_bglt BgL_objz00_141, obj_t BgL_genericz00_142, obj_t BgL_classz00_143)
{
{ /* Llib/object.scm 1308 */
{ 
 obj_t BgL_classz00_1687;
BgL_classz00_1687 = BgL_classz00_143; 
BgL_zc3z04anonymousza31797ze3z87_1688:
if(
BGL_CLASSP(BgL_classz00_1687))
{ /* Llib/object.scm 1312 */
 long BgL_objzd2superzd2classzd2numzd2_1690;
{ /* Llib/object.scm 1312 */
 obj_t BgL_classz00_3533;
if(
BGL_CLASSP(BgL_classz00_1687))
{ /* Llib/object.scm 1312 */
BgL_classz00_3533 = BgL_classz00_1687; }  else 
{ 
 obj_t BgL_auxz00_10665;
BgL_auxz00_10665 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57158L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_1687); 
FAILURE(BgL_auxz00_10665,BFALSE,BFALSE);} 
BgL_objzd2superzd2classzd2numzd2_1690 = 
BGL_CLASS_INDEX(BgL_classz00_3533); } 
{ /* Llib/object.scm 1313 */
 obj_t BgL_methodz00_1691;
{ /* Llib/object.scm 1314 */
 obj_t BgL_arg1800z00_1693;
BgL_arg1800z00_1693 = 
PROCEDURE_REF(BgL_genericz00_142, 
(int)(1L)); 
{ /* Llib/object.scm 1313 */
 obj_t BgL_arrayz00_3536; int BgL_offsetz00_3537;
if(
VECTORP(BgL_arg1800z00_1693))
{ /* Llib/object.scm 1314 */
BgL_arrayz00_3536 = BgL_arg1800z00_1693; }  else 
{ 
 obj_t BgL_auxz00_10674;
BgL_auxz00_10674 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57251L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_arg1800z00_1693); 
FAILURE(BgL_auxz00_10674,BFALSE,BFALSE);} 
BgL_offsetz00_3537 = 
(int)(BgL_objzd2superzd2classzd2numzd2_1690); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_3538;
BgL_offsetz00_3538 = 
(
(long)(BgL_offsetz00_3537)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_3539;
BgL_modz00_3539 = 
(BgL_offsetz00_3538 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_3541;
BgL_restz00_3541 = 
(BgL_offsetz00_3538 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_3543;
BgL_bucketz00_3543 = 
VECTOR_REF(BgL_arrayz00_3536,BgL_modz00_3539); 
{ /* Llib/object.scm 935 */
 obj_t BgL_vectorz00_3560;
if(
VECTORP(BgL_bucketz00_3543))
{ /* Llib/object.scm 935 */
BgL_vectorz00_3560 = BgL_bucketz00_3543; }  else 
{ 
 obj_t BgL_auxz00_10698;
BgL_auxz00_10698 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(41128L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_bucketz00_3543); 
FAILURE(BgL_auxz00_10698,BFALSE,BFALSE);} 
BgL_methodz00_1691 = 
VECTOR_REF(BgL_vectorz00_3560,BgL_restz00_3541); } } } } } } } } 
if(
CBOOL(BgL_methodz00_1691))
{ /* Llib/object.scm 1316 */
return 
MAKE_YOUNG_PAIR(BgL_classz00_1687, BgL_methodz00_1691);}  else 
{ /* Llib/object.scm 1318 */
 obj_t BgL_arg1799z00_1692;
{ /* Llib/object.scm 1318 */
 obj_t BgL_classz00_3562;
if(
BGL_CLASSP(BgL_classz00_1687))
{ /* Llib/object.scm 1318 */
BgL_classz00_3562 = BgL_classz00_1687; }  else 
{ 
 obj_t BgL_auxz00_10708;
BgL_auxz00_10708 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57349L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_1687); 
FAILURE(BgL_auxz00_10708,BFALSE,BFALSE);} 
BgL_arg1799z00_1692 = 
BGL_CLASS_SUPER(BgL_classz00_3562); } 
{ 
 obj_t BgL_classz00_10713;
BgL_classz00_10713 = BgL_arg1799z00_1692; 
BgL_classz00_1687 = BgL_classz00_10713; 
goto BgL_zc3z04anonymousza31797ze3z87_1688;} } } }  else 
{ /* Llib/object.scm 1310 */
return 
MAKE_YOUNG_PAIR(BFALSE, BFALSE);} } } 

}



/* &find-method-from */
obj_t BGl_z62findzd2methodzd2fromz62zz__objectz00(obj_t BgL_envz00_4899, obj_t BgL_objz00_4900, obj_t BgL_genericz00_4901, obj_t BgL_classz00_4902)
{
{ /* Llib/object.scm 1308 */
{ /* Llib/object.scm 1310 */
 obj_t BgL_auxz00_10723; BgL_objectz00_bglt BgL_auxz00_10715;
if(
PROCEDUREP(BgL_genericz00_4901))
{ /* Llib/object.scm 1310 */
BgL_auxz00_10723 = BgL_genericz00_4901
; }  else 
{ 
 obj_t BgL_auxz00_10726;
BgL_auxz00_10726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57074L), BGl_string3588z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_genericz00_4901); 
FAILURE(BgL_auxz00_10726,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4900, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1310 */
BgL_auxz00_10715 = 
((BgL_objectz00_bglt)BgL_objz00_4900)
; }  else 
{ 
 obj_t BgL_auxz00_10719;
BgL_auxz00_10719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57074L), BGl_string3588z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4900); 
FAILURE(BgL_auxz00_10719,BFALSE,BFALSE);} 
return 
BGl_findzd2methodzd2fromz00zz__objectz00(BgL_auxz00_10715, BgL_auxz00_10723, BgL_classz00_4902);} } 

}



/* nil? */
BGL_EXPORTED_DEF bool_t BGl_nilzf3zf3zz__objectz00(BgL_objectz00_bglt BgL_objz00_144)
{
{ /* Llib/object.scm 1323 */
{ /* Llib/object.scm 1324 */
 obj_t BgL_klassz00_3563;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_3566; long BgL_arg1555z00_3567;
BgL_arg1554z00_3566 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_3568;
BgL_arg1556z00_3568 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_144); 
BgL_arg1555z00_3567 = 
(BgL_arg1556z00_3568-OBJECT_TYPE); } 
BgL_klassz00_3563 = 
VECTOR_REF(BgL_arg1554z00_3566,BgL_arg1555z00_3567); } 
{ /* Llib/object.scm 1325 */
 obj_t BgL_arg1801z00_3564;
{ /* Llib/object.scm 1325 */
 obj_t BgL_classz00_3574;
if(
BGL_CLASSP(BgL_klassz00_3563))
{ /* Llib/object.scm 1325 */
BgL_classz00_3574 = BgL_klassz00_3563; }  else 
{ 
 obj_t BgL_auxz00_10737;
BgL_auxz00_10737 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57675L), BGl_string3589z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_klassz00_3563); 
FAILURE(BgL_auxz00_10737,BFALSE,BFALSE);} 
if(
BGL_CLASSP(BgL_classz00_3574))
{ /* Llib/object.scm 758 */
 obj_t BgL__ortest_1218z00_3576;
BgL__ortest_1218z00_3576 = 
BGL_CLASS_NIL(BgL_classz00_3574); 
if(
CBOOL(BgL__ortest_1218z00_3576))
{ /* Llib/object.scm 758 */
BgL_arg1801z00_3564 = BgL__ortest_1218z00_3576; }  else 
{ /* Llib/object.scm 758 */
BgL_arg1801z00_3564 = 
BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3574); } }  else 
{ /* Llib/object.scm 757 */
BgL_arg1801z00_3564 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3521z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_3574); } } 
return 
(BgL_arg1801z00_3564==
((obj_t)BgL_objz00_144));} } } 

}



/* &nil? */
obj_t BGl_z62nilzf3z91zz__objectz00(obj_t BgL_envz00_4903, obj_t BgL_objz00_4904)
{
{ /* Llib/object.scm 1323 */
{ /* Llib/object.scm 1324 */
 bool_t BgL_tmpz00_10750;
{ /* Llib/object.scm 1324 */
 BgL_objectz00_bglt BgL_auxz00_10751;
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4904, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1324 */
BgL_auxz00_10751 = 
((BgL_objectz00_bglt)BgL_objz00_4904)
; }  else 
{ 
 obj_t BgL_auxz00_10755;
BgL_auxz00_10755 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57619L), BGl_string3590z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4904); 
FAILURE(BgL_auxz00_10755,BFALSE,BFALSE);} 
BgL_tmpz00_10750 = 
BGl_nilzf3zf3zz__objectz00(BgL_auxz00_10751); } 
return 
BBOOL(BgL_tmpz00_10750);} } 

}



/* isa? */
BGL_EXPORTED_DEF bool_t BGl_isazf3zf3zz__objectz00(obj_t BgL_objz00_145, obj_t BgL_classz00_146)
{
{ /* Llib/object.scm 1332 */
if(
BGL_OBJECTP(BgL_objz00_145))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6477; long BgL_arg1804z00_6478;
BgL_arg1803z00_6477 = 
(BgL_objectz00_bglt)(BgL_objz00_145); 
BgL_arg1804z00_6478 = 
BGL_CLASS_DEPTH(BgL_classz00_146); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6479;
BgL_idxz00_6479 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6477); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6480;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6481;
BgL_arg1812z00_6481 = 
(BgL_idxz00_6479+BgL_arg1804z00_6478); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6482;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6483;
BgL_aux3145z00_6483 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6483))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6482 = BgL_aux3145z00_6483; }  else 
{ 
 obj_t BgL_auxz00_10771;
BgL_auxz00_10771 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6483); 
FAILURE(BgL_auxz00_10771,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4822z00_10775;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_10776;
BgL_tmpz00_10776 = 
VECTOR_LENGTH(BgL_vectorz00_6482); 
BgL_test4822z00_10775 = 
BOUND_CHECK(BgL_arg1812z00_6481, BgL_tmpz00_10776); } 
if(BgL_test4822z00_10775)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6480 = 
VECTOR_REF(BgL_vectorz00_6482,BgL_arg1812z00_6481); }  else 
{ 
 obj_t BgL_auxz00_10780;
BgL_auxz00_10780 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6482, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6482)), 
(int)(BgL_arg1812z00_6481)); 
FAILURE(BgL_auxz00_10780,BFALSE,BFALSE);} } } } 
return 
(BgL_arg1811z00_6480==BgL_classz00_146);} }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6484;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6485;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6486; long BgL_arg1555z00_6487;
BgL_arg1554z00_6486 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6488;
BgL_arg1556z00_6488 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6477); 
BgL_arg1555z00_6487 = 
(BgL_arg1556z00_6488-OBJECT_TYPE); } 
BgL_oclassz00_6485 = 
VECTOR_REF(BgL_arg1554z00_6486,BgL_arg1555z00_6487); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6489;
BgL__ortest_1220z00_6489 = 
(BgL_classz00_146==BgL_oclassz00_6485); 
if(BgL__ortest_1220z00_6489)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6484 = BgL__ortest_1220z00_6489; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6490;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6491;
BgL_arg1810z00_6491 = 
(BgL_oclassz00_6485); 
BgL_odepthz00_6490 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6491); } 
if(
(BgL_arg1804z00_6478<BgL_odepthz00_6490))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6492;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6493;
BgL_arg1809z00_6493 = 
(BgL_oclassz00_6485); 
BgL_arg1808z00_6492 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6493, BgL_arg1804z00_6478); } 
BgL_res2618z00_6484 = 
(BgL_arg1808z00_6492==BgL_classz00_146); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6484 = ((bool_t)0); } } } } 
return BgL_res2618z00_6484;} }  else 
{ /* Llib/object.scm 1333 */
return ((bool_t)0);} } 

}



/* &isa? */
obj_t BGl_z62isazf3z91zz__objectz00(obj_t BgL_envz00_4905, obj_t BgL_objz00_4906, obj_t BgL_classz00_4907)
{
{ /* Llib/object.scm 1332 */
{ /* Llib/object.scm 1333 */
 bool_t BgL_tmpz00_10801;
{ /* Llib/object.scm 1333 */
 obj_t BgL_auxz00_10802;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4907))
{ /* Llib/object.scm 1333 */
BgL_auxz00_10802 = BgL_classz00_4907
; }  else 
{ 
 obj_t BgL_auxz00_10805;
BgL_auxz00_10805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(58095L), BGl_string3593z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4907); 
FAILURE(BgL_auxz00_10805,BFALSE,BFALSE);} 
BgL_tmpz00_10801 = 
BGl_isazf3zf3zz__objectz00(BgL_objz00_4906, BgL_auxz00_10802); } 
return 
BBOOL(BgL_tmpz00_10801);} } 

}



/* %isa/cdepth? */
BGL_EXPORTED_DEF bool_t BGl_z52isazf2cdepthzf3z53zz__objectz00(obj_t BgL_objz00_147, obj_t BgL_classz00_148, long BgL_cdepthz00_149)
{
{ /* Llib/object.scm 1339 */
if(
BGL_OBJECTP(BgL_objz00_147))
{ /* Llib/object.scm 1341 */
 BgL_objectz00_bglt BgL_arg1806z00_6494;
BgL_arg1806z00_6494 = 
(BgL_objectz00_bglt)(BgL_objz00_147); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6495;
BgL_idxz00_6495 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1806z00_6494); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6496;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6497;
BgL_arg1812z00_6497 = 
(BgL_idxz00_6495+BgL_cdepthz00_149); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6498;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3149z00_6499;
BgL_aux3149z00_6499 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3149z00_6499))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6498 = BgL_aux3149z00_6499; }  else 
{ 
 obj_t BgL_auxz00_10820;
BgL_auxz00_10820 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3594z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3149z00_6499); 
FAILURE(BgL_auxz00_10820,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4829z00_10824;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_10825;
BgL_tmpz00_10825 = 
VECTOR_LENGTH(BgL_vectorz00_6498); 
BgL_test4829z00_10824 = 
BOUND_CHECK(BgL_arg1812z00_6497, BgL_tmpz00_10825); } 
if(BgL_test4829z00_10824)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6496 = 
VECTOR_REF(BgL_vectorz00_6498,BgL_arg1812z00_6497); }  else 
{ 
 obj_t BgL_auxz00_10829;
BgL_auxz00_10829 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6498, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6498)), 
(int)(BgL_arg1812z00_6497)); 
FAILURE(BgL_auxz00_10829,BFALSE,BFALSE);} } } } 
return 
(BgL_arg1811z00_6496==BgL_classz00_148);} }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2619z00_6500;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6501;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6502; long BgL_arg1555z00_6503;
BgL_arg1554z00_6502 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6504;
BgL_arg1556z00_6504 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1806z00_6494); 
BgL_arg1555z00_6503 = 
(BgL_arg1556z00_6504-OBJECT_TYPE); } 
BgL_oclassz00_6501 = 
VECTOR_REF(BgL_arg1554z00_6502,BgL_arg1555z00_6503); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6505;
BgL__ortest_1220z00_6505 = 
(BgL_classz00_148==BgL_oclassz00_6501); 
if(BgL__ortest_1220z00_6505)
{ /* Llib/object.scm 1363 */
BgL_res2619z00_6500 = BgL__ortest_1220z00_6505; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6506;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6507;
BgL_arg1810z00_6507 = 
(BgL_oclassz00_6501); 
BgL_odepthz00_6506 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6507); } 
if(
(BgL_cdepthz00_149<BgL_odepthz00_6506))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6508;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6509;
BgL_arg1809z00_6509 = 
(BgL_oclassz00_6501); 
BgL_arg1808z00_6508 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6509, BgL_cdepthz00_149); } 
BgL_res2619z00_6500 = 
(BgL_arg1808z00_6508==BgL_classz00_148); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2619z00_6500 = ((bool_t)0); } } } } 
return BgL_res2619z00_6500;} }  else 
{ /* Llib/object.scm 1340 */
return ((bool_t)0);} } 

}



/* &%isa/cdepth? */
obj_t BGl_z62z52isazf2cdepthzf3z31zz__objectz00(obj_t BgL_envz00_4908, obj_t BgL_objz00_4909, obj_t BgL_classz00_4910, obj_t BgL_cdepthz00_4911)
{
{ /* Llib/object.scm 1339 */
{ /* Llib/object.scm 1340 */
 bool_t BgL_tmpz00_10850;
{ /* Llib/object.scm 1340 */
 long BgL_auxz00_10858; obj_t BgL_auxz00_10851;
{ /* Llib/object.scm 1340 */
 obj_t BgL_tmpz00_10859;
if(
INTEGERP(BgL_cdepthz00_4911))
{ /* Llib/object.scm 1340 */
BgL_tmpz00_10859 = BgL_cdepthz00_4911
; }  else 
{ 
 obj_t BgL_auxz00_10862;
BgL_auxz00_10862 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(58462L), BGl_string3595z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_cdepthz00_4911); 
FAILURE(BgL_auxz00_10862,BFALSE,BFALSE);} 
BgL_auxz00_10858 = 
(long)CINT(BgL_tmpz00_10859); } 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4910))
{ /* Llib/object.scm 1340 */
BgL_auxz00_10851 = BgL_classz00_4910
; }  else 
{ 
 obj_t BgL_auxz00_10854;
BgL_auxz00_10854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(58462L), BGl_string3595z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4910); 
FAILURE(BgL_auxz00_10854,BFALSE,BFALSE);} 
BgL_tmpz00_10850 = 
BGl_z52isazf2cdepthzf3z53zz__objectz00(BgL_objz00_4909, BgL_auxz00_10851, BgL_auxz00_10858); } 
return 
BBOOL(BgL_tmpz00_10850);} } 

}



/* %isa-object/cdepth? */
BGL_EXPORTED_DEF bool_t BGl_z52isazd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt BgL_objz00_150, obj_t BgL_classz00_151, long BgL_cdepthz00_152)
{
{ /* Llib/object.scm 1346 */
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6510;
BgL_idxz00_6510 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_objz00_150); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6511;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6512;
BgL_arg1812z00_6512 = 
(BgL_idxz00_6510+BgL_cdepthz00_152); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6513;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3154z00_6514;
BgL_aux3154z00_6514 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3154z00_6514))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6513 = BgL_aux3154z00_6514; }  else 
{ 
 obj_t BgL_auxz00_10875;
BgL_auxz00_10875 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3596z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3154z00_6514); 
FAILURE(BgL_auxz00_10875,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4836z00_10879;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_10880;
BgL_tmpz00_10880 = 
VECTOR_LENGTH(BgL_vectorz00_6513); 
BgL_test4836z00_10879 = 
BOUND_CHECK(BgL_arg1812z00_6512, BgL_tmpz00_10880); } 
if(BgL_test4836z00_10879)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6511 = 
VECTOR_REF(BgL_vectorz00_6513,BgL_arg1812z00_6512); }  else 
{ 
 obj_t BgL_auxz00_10884;
BgL_auxz00_10884 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6513, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6513)), 
(int)(BgL_arg1812z00_6512)); 
FAILURE(BgL_auxz00_10884,BFALSE,BFALSE);} } } } 
return 
(BgL_arg1811z00_6511==BgL_classz00_151);} }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2620z00_6515;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6516;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6517; long BgL_arg1555z00_6518;
BgL_arg1554z00_6517 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6519;
BgL_arg1556z00_6519 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_150); 
BgL_arg1555z00_6518 = 
(BgL_arg1556z00_6519-OBJECT_TYPE); } 
BgL_oclassz00_6516 = 
VECTOR_REF(BgL_arg1554z00_6517,BgL_arg1555z00_6518); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6520;
BgL__ortest_1220z00_6520 = 
(BgL_classz00_151==BgL_oclassz00_6516); 
if(BgL__ortest_1220z00_6520)
{ /* Llib/object.scm 1363 */
BgL_res2620z00_6515 = BgL__ortest_1220z00_6520; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6521;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6522;
BgL_arg1810z00_6522 = 
(BgL_oclassz00_6516); 
BgL_odepthz00_6521 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6522); } 
if(
(BgL_cdepthz00_152<BgL_odepthz00_6521))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6523;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6524;
BgL_arg1809z00_6524 = 
(BgL_oclassz00_6516); 
BgL_arg1808z00_6523 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6524, BgL_cdepthz00_152); } 
BgL_res2620z00_6515 = 
(BgL_arg1808z00_6523==BgL_classz00_151); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2620z00_6515 = ((bool_t)0); } } } } 
return BgL_res2620z00_6515;} } 

}



/* &%isa-object/cdepth? */
obj_t BGl_z62z52isazd2objectzf2cdepthzf3ze3zz__objectz00(obj_t BgL_envz00_4912, obj_t BgL_objz00_4913, obj_t BgL_classz00_4914, obj_t BgL_cdepthz00_4915)
{
{ /* Llib/object.scm 1346 */
{ /* Llib/object.scm 1352 */
 bool_t BgL_tmpz00_10905;
{ /* Llib/object.scm 1352 */
 long BgL_auxz00_10921; obj_t BgL_auxz00_10914; BgL_objectz00_bglt BgL_auxz00_10906;
{ /* Llib/object.scm 1352 */
 obj_t BgL_tmpz00_10922;
if(
INTEGERP(BgL_cdepthz00_4915))
{ /* Llib/object.scm 1352 */
BgL_tmpz00_10922 = BgL_cdepthz00_4915
; }  else 
{ 
 obj_t BgL_auxz00_10925;
BgL_auxz00_10925 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(59046L), BGl_string3597z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_cdepthz00_4915); 
FAILURE(BgL_auxz00_10925,BFALSE,BFALSE);} 
BgL_auxz00_10921 = 
(long)CINT(BgL_tmpz00_10922); } 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4914))
{ /* Llib/object.scm 1352 */
BgL_auxz00_10914 = BgL_classz00_4914
; }  else 
{ 
 obj_t BgL_auxz00_10917;
BgL_auxz00_10917 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(59046L), BGl_string3597z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4914); 
FAILURE(BgL_auxz00_10917,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4913, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1352 */
BgL_auxz00_10906 = 
((BgL_objectz00_bglt)BgL_objz00_4913)
; }  else 
{ 
 obj_t BgL_auxz00_10910;
BgL_auxz00_10910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(59046L), BGl_string3597z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4913); 
FAILURE(BgL_auxz00_10910,BFALSE,BFALSE);} 
BgL_tmpz00_10905 = 
BGl_z52isazd2objectzf2cdepthzf3z81zz__objectz00(BgL_auxz00_10906, BgL_auxz00_10914, BgL_auxz00_10921); } 
return 
BBOOL(BgL_tmpz00_10905);} } 

}



/* %isa32-object/cdepth? */
BGL_EXPORTED_DEF bool_t BGl_z52isa32zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt BgL_objz00_153, obj_t BgL_classz00_154, long BgL_cdepthz00_155)
{
{ /* Llib/object.scm 1361 */
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6525;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6526; long BgL_arg1555z00_6527;
BgL_arg1554z00_6526 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6528;
BgL_arg1556z00_6528 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_153); 
BgL_arg1555z00_6527 = 
(BgL_arg1556z00_6528-OBJECT_TYPE); } 
BgL_oclassz00_6525 = 
VECTOR_REF(BgL_arg1554z00_6526,BgL_arg1555z00_6527); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6529;
BgL__ortest_1220z00_6529 = 
(BgL_classz00_154==BgL_oclassz00_6525); 
if(BgL__ortest_1220z00_6529)
{ /* Llib/object.scm 1363 */
return BgL__ortest_1220z00_6529;}  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6530;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6531;
BgL_arg1810z00_6531 = 
(BgL_oclassz00_6525); 
BgL_odepthz00_6530 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6531); } 
if(
(BgL_cdepthz00_155<BgL_odepthz00_6530))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6532;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6533;
BgL_arg1809z00_6533 = 
(BgL_oclassz00_6525); 
BgL_arg1808z00_6532 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6533, BgL_cdepthz00_155); } 
return 
(BgL_arg1808z00_6532==BgL_classz00_154);}  else 
{ /* Llib/object.scm 1365 */
return ((bool_t)0);} } } } } 

}



/* &%isa32-object/cdepth? */
obj_t BGl_z62z52isa32zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t BgL_envz00_4916, obj_t BgL_objz00_4917, obj_t BgL_classz00_4918, obj_t BgL_cdepthz00_4919)
{
{ /* Llib/object.scm 1361 */
{ /* Llib/object.scm 1362 */
 bool_t BgL_tmpz00_10945;
{ /* Llib/object.scm 1362 */
 long BgL_auxz00_10961; obj_t BgL_auxz00_10954; BgL_objectz00_bglt BgL_auxz00_10946;
{ /* Llib/object.scm 1362 */
 obj_t BgL_tmpz00_10962;
if(
INTEGERP(BgL_cdepthz00_4919))
{ /* Llib/object.scm 1362 */
BgL_tmpz00_10962 = BgL_cdepthz00_4919
; }  else 
{ 
 obj_t BgL_auxz00_10965;
BgL_auxz00_10965 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(59514L), BGl_string3598z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_cdepthz00_4919); 
FAILURE(BgL_auxz00_10965,BFALSE,BFALSE);} 
BgL_auxz00_10961 = 
(long)CINT(BgL_tmpz00_10962); } 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4918))
{ /* Llib/object.scm 1362 */
BgL_auxz00_10954 = BgL_classz00_4918
; }  else 
{ 
 obj_t BgL_auxz00_10957;
BgL_auxz00_10957 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(59514L), BGl_string3598z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4918); 
FAILURE(BgL_auxz00_10957,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4917, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1362 */
BgL_auxz00_10946 = 
((BgL_objectz00_bglt)BgL_objz00_4917)
; }  else 
{ 
 obj_t BgL_auxz00_10950;
BgL_auxz00_10950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(59514L), BGl_string3598z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4917); 
FAILURE(BgL_auxz00_10950,BFALSE,BFALSE);} 
BgL_tmpz00_10945 = 
BGl_z52isa32zd2objectzf2cdepthzf3z81zz__objectz00(BgL_auxz00_10946, BgL_auxz00_10954, BgL_auxz00_10961); } 
return 
BBOOL(BgL_tmpz00_10945);} } 

}



/* %isa64-object/cdepth? */
BGL_EXPORTED_DEF bool_t BGl_z52isa64zd2objectzf2cdepthzf3z81zz__objectz00(BgL_objectz00_bglt BgL_objz00_156, obj_t BgL_classz00_157, long BgL_cdepthz00_158)
{
{ /* Llib/object.scm 1372 */
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6534;
BgL_idxz00_6534 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_objz00_156); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6535;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6536;
BgL_arg1812z00_6536 = 
(BgL_idxz00_6534+BgL_cdepthz00_158); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6537;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3166z00_6538;
BgL_aux3166z00_6538 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3166z00_6538))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6537 = BgL_aux3166z00_6538; }  else 
{ 
 obj_t BgL_auxz00_10976;
BgL_auxz00_10976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3599z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3166z00_6538); 
FAILURE(BgL_auxz00_10976,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4848z00_10980;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_10981;
BgL_tmpz00_10981 = 
VECTOR_LENGTH(BgL_vectorz00_6537); 
BgL_test4848z00_10980 = 
BOUND_CHECK(BgL_arg1812z00_6536, BgL_tmpz00_10981); } 
if(BgL_test4848z00_10980)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6535 = 
VECTOR_REF(BgL_vectorz00_6537,BgL_arg1812z00_6536); }  else 
{ 
 obj_t BgL_auxz00_10985;
BgL_auxz00_10985 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6537, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6537)), 
(int)(BgL_arg1812z00_6536)); 
FAILURE(BgL_auxz00_10985,BFALSE,BFALSE);} } } } 
return 
(BgL_arg1811z00_6535==BgL_classz00_157);} } } 

}



/* &%isa64-object/cdepth? */
obj_t BGl_z62z52isa64zd2objectzf2cdepthzf3ze3zz__objectz00(obj_t BgL_envz00_4920, obj_t BgL_objz00_4921, obj_t BgL_classz00_4922, obj_t BgL_cdepthz00_4923)
{
{ /* Llib/object.scm 1372 */
{ /* Llib/object.scm 1375 */
 bool_t BgL_tmpz00_10993;
{ /* Llib/object.scm 1375 */
 long BgL_auxz00_11009; obj_t BgL_auxz00_11002; BgL_objectz00_bglt BgL_auxz00_10994;
{ /* Llib/object.scm 1375 */
 obj_t BgL_tmpz00_11010;
if(
INTEGERP(BgL_cdepthz00_4923))
{ /* Llib/object.scm 1375 */
BgL_tmpz00_11010 = BgL_cdepthz00_4923
; }  else 
{ 
 obj_t BgL_auxz00_11013;
BgL_auxz00_11013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60057L), BGl_string3600z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_cdepthz00_4923); 
FAILURE(BgL_auxz00_11013,BFALSE,BFALSE);} 
BgL_auxz00_11009 = 
(long)CINT(BgL_tmpz00_11010); } 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4922))
{ /* Llib/object.scm 1375 */
BgL_auxz00_11002 = BgL_classz00_4922
; }  else 
{ 
 obj_t BgL_auxz00_11005;
BgL_auxz00_11005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60057L), BGl_string3600z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4922); 
FAILURE(BgL_auxz00_11005,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4921, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1375 */
BgL_auxz00_10994 = 
((BgL_objectz00_bglt)BgL_objz00_4921)
; }  else 
{ 
 obj_t BgL_auxz00_10998;
BgL_auxz00_10998 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60057L), BGl_string3600z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4921); 
FAILURE(BgL_auxz00_10998,BFALSE,BFALSE);} 
BgL_tmpz00_10993 = 
BGl_z52isa64zd2objectzf2cdepthzf3z81zz__objectz00(BgL_auxz00_10994, BgL_auxz00_11002, BgL_auxz00_11009); } 
return 
BBOOL(BgL_tmpz00_10993);} } 

}



/* %isa/final? */
BGL_EXPORTED_DEF bool_t BGl_z52isazf2finalzf3z53zz__objectz00(obj_t BgL_objz00_159, obj_t BgL_classz00_160)
{
{ /* Llib/object.scm 1383 */
if(
BGL_OBJECTP(BgL_objz00_159))
{ /* Llib/object.scm 1385 */
 obj_t BgL_oclassz00_6539;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6540; long BgL_arg1555z00_6541;
BgL_arg1554z00_6540 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6542;
BgL_arg1556z00_6542 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_objz00_159)); 
BgL_arg1555z00_6541 = 
(BgL_arg1556z00_6542-OBJECT_TYPE); } 
BgL_oclassz00_6539 = 
VECTOR_REF(BgL_arg1554z00_6540,BgL_arg1555z00_6541); } 
return 
(BgL_oclassz00_6539==BgL_classz00_160);}  else 
{ /* Llib/object.scm 1384 */
return ((bool_t)0);} } 

}



/* &%isa/final? */
obj_t BGl_z62z52isazf2finalzf3z31zz__objectz00(obj_t BgL_envz00_4924, obj_t BgL_objz00_4925, obj_t BgL_classz00_4926)
{
{ /* Llib/object.scm 1383 */
{ /* Llib/object.scm 1384 */
 bool_t BgL_tmpz00_11028;
{ /* Llib/object.scm 1384 */
 obj_t BgL_auxz00_11029;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4926))
{ /* Llib/object.scm 1384 */
BgL_auxz00_11029 = BgL_classz00_4926
; }  else 
{ 
 obj_t BgL_auxz00_11032;
BgL_auxz00_11032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60452L), BGl_string3601z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4926); 
FAILURE(BgL_auxz00_11032,BFALSE,BFALSE);} 
BgL_tmpz00_11028 = 
BGl_z52isazf2finalzf3z53zz__objectz00(BgL_objz00_4925, BgL_auxz00_11029); } 
return 
BBOOL(BgL_tmpz00_11028);} } 

}



/* %isa-object/final? */
BGL_EXPORTED_DEF bool_t BGl_z52isazd2objectzf2finalzf3z81zz__objectz00(BgL_objectz00_bglt BgL_objz00_161, obj_t BgL_classz00_162)
{
{ /* Llib/object.scm 1391 */
{ /* Llib/object.scm 1392 */
 obj_t BgL_oclassz00_6543;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6544; long BgL_arg1555z00_6545;
BgL_arg1554z00_6544 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6546;
BgL_arg1556z00_6546 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_161); 
BgL_arg1555z00_6545 = 
(BgL_arg1556z00_6546-OBJECT_TYPE); } 
BgL_oclassz00_6543 = 
VECTOR_REF(BgL_arg1554z00_6544,BgL_arg1555z00_6545); } 
return 
(BgL_oclassz00_6543==BgL_classz00_162);} } 

}



/* &%isa-object/final? */
obj_t BGl_z62z52isazd2objectzf2finalzf3ze3zz__objectz00(obj_t BgL_envz00_4927, obj_t BgL_objz00_4928, obj_t BgL_classz00_4929)
{
{ /* Llib/object.scm 1391 */
{ /* Llib/object.scm 1392 */
 bool_t BgL_tmpz00_11043;
{ /* Llib/object.scm 1392 */
 obj_t BgL_auxz00_11052; BgL_objectz00_bglt BgL_auxz00_11044;
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_4929))
{ /* Llib/object.scm 1392 */
BgL_auxz00_11052 = BgL_classz00_4929
; }  else 
{ 
 obj_t BgL_auxz00_11055;
BgL_auxz00_11055 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60809L), BGl_string3602z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_4929); 
FAILURE(BgL_auxz00_11055,BFALSE,BFALSE);} 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4928, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1392 */
BgL_auxz00_11044 = 
((BgL_objectz00_bglt)BgL_objz00_4928)
; }  else 
{ 
 obj_t BgL_auxz00_11048;
BgL_auxz00_11048 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60809L), BGl_string3602z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4928); 
FAILURE(BgL_auxz00_11048,BFALSE,BFALSE);} 
BgL_tmpz00_11043 = 
BGl_z52isazd2objectzf2finalzf3z81zz__objectz00(BgL_auxz00_11044, BgL_auxz00_11052); } 
return 
BBOOL(BgL_tmpz00_11043);} } 

}



/* allocate-instance */
BGL_EXPORTED_DEF BgL_objectz00_bglt BGl_allocatezd2instancezd2zz__objectz00(obj_t BgL_cnamez00_168)
{
{ /* Llib/object.scm 1418 */
{ 
 long BgL_iz00_1717;
{ /* Llib/object.scm 1419 */
 obj_t BgL_aux3200z00_5968;
BgL_iz00_1717 = 0L; 
BgL_zc3z04anonymousza31814ze3z87_1718:
{ /* Llib/object.scm 1420 */
 bool_t BgL_test4856z00_11061;
{ /* Llib/object.scm 1420 */
 long BgL_n2z00_3734;
{ /* Llib/object.scm 1420 */
 obj_t BgL_tmpz00_11062;
{ /* Llib/object.scm 1420 */
 obj_t BgL_aux3179z00_5943;
BgL_aux3179z00_5943 = BGl_za2nbzd2classesza2zd2zz__objectz00; 
if(
INTEGERP(BgL_aux3179z00_5943))
{ /* Llib/object.scm 1420 */
BgL_tmpz00_11062 = BgL_aux3179z00_5943
; }  else 
{ 
 obj_t BgL_auxz00_11065;
BgL_auxz00_11065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62256L), BGl_string3442z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3179z00_5943); 
FAILURE(BgL_auxz00_11065,BFALSE,BFALSE);} } 
BgL_n2z00_3734 = 
(long)CINT(BgL_tmpz00_11062); } 
BgL_test4856z00_11061 = 
(BgL_iz00_1717==BgL_n2z00_3734); } 
if(BgL_test4856z00_11061)
{ /* Llib/object.scm 1420 */
BgL_aux3200z00_5968 = 
BGl_errorz00zz__errorz00(BGl_string3603z00zz__objectz00, BGl_string3449z00zz__objectz00, BgL_cnamez00_168); }  else 
{ /* Llib/object.scm 1422 */
 obj_t BgL_classz00_1720;
{ /* Llib/object.scm 1422 */
 obj_t BgL_vectorz00_3735;
{ /* Llib/object.scm 1422 */
 obj_t BgL_aux3180z00_5944;
BgL_aux3180z00_5944 = BGl_za2classesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3180z00_5944))
{ /* Llib/object.scm 1422 */
BgL_vectorz00_3735 = BgL_aux3180z00_5944; }  else 
{ 
 obj_t BgL_auxz00_11074;
BgL_auxz00_11074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62358L), BGl_string3442z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3180z00_5944); 
FAILURE(BgL_auxz00_11074,BFALSE,BFALSE);} } 
BgL_classz00_1720 = 
VECTOR_REF(BgL_vectorz00_3735,BgL_iz00_1717); } 
{ /* Llib/object.scm 1423 */
 bool_t BgL_test4859z00_11079;
{ /* Llib/object.scm 1423 */
 obj_t BgL_arg1826z00_1732;
{ /* Llib/object.scm 1423 */
 obj_t BgL_classz00_3737;
if(
BGL_CLASSP(BgL_classz00_1720))
{ /* Llib/object.scm 1423 */
BgL_classz00_3737 = BgL_classz00_1720; }  else 
{ 
 obj_t BgL_auxz00_11082;
BgL_auxz00_11082 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62400L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_1720); 
FAILURE(BgL_auxz00_11082,BFALSE,BFALSE);} 
BgL_arg1826z00_1732 = 
BGL_CLASS_NAME(BgL_classz00_3737); } 
BgL_test4859z00_11079 = 
(BgL_arg1826z00_1732==BgL_cnamez00_168); } 
if(BgL_test4859z00_11079)
{ /* Llib/object.scm 1424 */
 obj_t BgL_allocz00_1723;
{ /* Llib/object.scm 1424 */
 obj_t BgL_res2621z00_3740;
{ /* Llib/object.scm 1424 */
 obj_t BgL_classz00_3738;
if(
BGL_CLASSP(BgL_classz00_1720))
{ /* Llib/object.scm 1424 */
BgL_classz00_3738 = BgL_classz00_1720; }  else 
{ 
 obj_t BgL_auxz00_11090;
BgL_auxz00_11090 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62447L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_1720); 
FAILURE(BgL_auxz00_11090,BFALSE,BFALSE);} 
if(
BGL_CLASSP(BgL_classz00_3738))
{ /* Llib/object.scm 709 */
BgL_res2621z00_3740 = 
BGL_CLASS_ALLOC_FUN(BgL_classz00_3738); }  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux3186z00_5950;
BgL_aux3186z00_5950 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_3738); 
if(
PROCEDUREP(BgL_aux3186z00_5950))
{ /* Llib/object.scm 711 */
BgL_res2621z00_3740 = BgL_aux3186z00_5950; }  else 
{ 
 obj_t BgL_auxz00_11100;
BgL_auxz00_11100 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3186z00_5950); 
FAILURE(BgL_auxz00_11100,BFALSE,BFALSE);} } } 
BgL_allocz00_1723 = BgL_res2621z00_3740; } 
{ /* Llib/object.scm 1425 */
 bool_t BgL_test4864z00_11104;
{ /* Llib/object.scm 1425 */
 obj_t BgL_classz00_3741;
if(
BGL_CLASSP(BgL_classz00_1720))
{ /* Llib/object.scm 1425 */
BgL_classz00_3741 = BgL_classz00_1720; }  else 
{ 
 obj_t BgL_auxz00_11107;
BgL_auxz00_11107 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62479L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_1720); 
FAILURE(BgL_auxz00_11107,BFALSE,BFALSE);} 
{ /* Llib/object.scm 691 */
 obj_t BgL_tmpz00_11111;
BgL_tmpz00_11111 = 
BGl_classzd2shrinkzd2zz__objectz00(BgL_classz00_3741); 
BgL_test4864z00_11104 = 
PROCEDUREP(BgL_tmpz00_11111); } } 
if(BgL_test4864z00_11104)
{ /* Llib/object.scm 1428 */
 bool_t BgL_test4866z00_11114;
{ /* Llib/object.scm 1428 */
 int BgL_arg1822z00_1730;
BgL_arg1822z00_1730 = 
PROCEDURE_ARITY(BgL_allocz00_1723); 
BgL_test4866z00_11114 = 
(
(long)(BgL_arg1822z00_1730)==0L); } 
if(BgL_test4866z00_11114)
{ /* Llib/object.scm 1428 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_allocz00_1723, 0))
{ /* Llib/object.scm 1429 */
BgL_aux3200z00_5968 = 
BGL_PROCEDURE_CALL0(BgL_allocz00_1723); }  else 
{ /* Llib/object.scm 1429 */
FAILURE(BGl_string3604z00zz__objectz00,BGl_list3605z00zz__objectz00,BgL_allocz00_1723);} }  else 
{ /* Llib/object.scm 1430 */
 obj_t BgL_superz00_1727;
{ /* Llib/object.scm 1430 */
 obj_t BgL_classz00_3745;
if(
BGL_CLASSP(BgL_classz00_1720))
{ /* Llib/object.scm 1430 */
BgL_classz00_3745 = BgL_classz00_1720; }  else 
{ 
 obj_t BgL_auxz00_11126;
BgL_auxz00_11126 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62657L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_1720); 
FAILURE(BgL_auxz00_11126,BFALSE,BFALSE);} 
BgL_superz00_1727 = 
BGL_CLASS_SUPER(BgL_classz00_3745); } 
{ /* Llib/object.scm 1430 */
 obj_t BgL_oz00_1728;
{ /* Llib/object.scm 1431 */
 obj_t BgL_fun1821z00_1729;
{ /* Llib/object.scm 1431 */
 obj_t BgL_res2622z00_3748;
{ /* Llib/object.scm 1431 */
 obj_t BgL_classz00_3746;
if(
BGL_CLASSP(BgL_superz00_1727))
{ /* Llib/object.scm 1431 */
BgL_classz00_3746 = BgL_superz00_1727; }  else 
{ 
 obj_t BgL_auxz00_11133;
BgL_auxz00_11133 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62693L), BGl_string3442z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_superz00_1727); 
FAILURE(BgL_auxz00_11133,BFALSE,BFALSE);} 
if(
BGL_CLASSP(BgL_classz00_3746))
{ /* Llib/object.scm 709 */
BgL_res2622z00_3748 = 
BGL_CLASS_ALLOC_FUN(BgL_classz00_3746); }  else 
{ /* Llib/object.scm 711 */
 obj_t BgL_aux3195z00_5960;
BgL_aux3195z00_5960 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3491z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_3746); 
if(
PROCEDUREP(BgL_aux3195z00_5960))
{ /* Llib/object.scm 711 */
BgL_res2622z00_3748 = BgL_aux3195z00_5960; }  else 
{ 
 obj_t BgL_auxz00_11143;
BgL_auxz00_11143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(30666L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3195z00_5960); 
FAILURE(BgL_auxz00_11143,BFALSE,BFALSE);} } } 
BgL_fun1821z00_1729 = BgL_res2622z00_3748; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fun1821z00_1729, 0))
{ /* Llib/object.scm 1431 */
BgL_oz00_1728 = 
BGL_PROCEDURE_CALL0(BgL_fun1821z00_1729); }  else 
{ /* Llib/object.scm 1431 */
FAILURE(BGl_string3604z00zz__objectz00,BGl_list3608z00zz__objectz00,BgL_fun1821z00_1729);} } 
{ /* Llib/object.scm 1431 */

if(
PROCEDURE_CORRECT_ARITYP(BgL_allocz00_1723, 1))
{ /* Llib/object.scm 1432 */
BgL_aux3200z00_5968 = 
BGL_PROCEDURE_CALL1(BgL_allocz00_1723, BgL_oz00_1728); }  else 
{ /* Llib/object.scm 1432 */
FAILURE(BGl_string3604z00zz__objectz00,BGl_list3611z00zz__objectz00,BgL_allocz00_1723);} } } } }  else 
{ /* Llib/object.scm 1425 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_allocz00_1723, 0))
{ /* Llib/object.scm 1433 */
BgL_aux3200z00_5968 = 
BGL_PROCEDURE_CALL0(BgL_allocz00_1723); }  else 
{ /* Llib/object.scm 1433 */
FAILURE(BGl_string3604z00zz__objectz00,BGl_list3605z00zz__objectz00,BgL_allocz00_1723);} } } }  else 
{ 
 long BgL_iz00_11166;
BgL_iz00_11166 = 
(BgL_iz00_1717+1L); 
BgL_iz00_1717 = BgL_iz00_11166; 
goto BgL_zc3z04anonymousza31814ze3z87_1718;} } } } 
{ /* Llib/object.scm 1419 */
 bool_t BgL_test4875z00_11168;
{ /* Llib/object.scm 1419 */
 obj_t BgL_classz00_6547;
BgL_classz00_6547 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_aux3200z00_5968))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6548; long BgL_arg1804z00_6549;
BgL_arg1803z00_6548 = 
(BgL_objectz00_bglt)(BgL_aux3200z00_5968); 
BgL_arg1804z00_6549 = 
BGL_CLASS_DEPTH(BgL_classz00_6547); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6550;
BgL_idxz00_6550 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6548); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6551;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6552;
BgL_arg1812z00_6552 = 
(BgL_idxz00_6550+BgL_arg1804z00_6549); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6553;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6554;
BgL_aux3145z00_6554 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6554))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6553 = BgL_aux3145z00_6554; }  else 
{ 
 obj_t BgL_auxz00_11179;
BgL_auxz00_11179 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6554); 
FAILURE(BgL_auxz00_11179,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4879z00_11183;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_11184;
BgL_tmpz00_11184 = 
VECTOR_LENGTH(BgL_vectorz00_6553); 
BgL_test4879z00_11183 = 
BOUND_CHECK(BgL_arg1812z00_6552, BgL_tmpz00_11184); } 
if(BgL_test4879z00_11183)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6551 = 
VECTOR_REF(BgL_vectorz00_6553,BgL_arg1812z00_6552); }  else 
{ 
 obj_t BgL_auxz00_11188;
BgL_auxz00_11188 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6553, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6553)), 
(int)(BgL_arg1812z00_6552)); 
FAILURE(BgL_auxz00_11188,BFALSE,BFALSE);} } } } 
BgL_test4875z00_11168 = 
(BgL_arg1811z00_6551==BgL_classz00_6547); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6555;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6556;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6557; long BgL_arg1555z00_6558;
BgL_arg1554z00_6557 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6559;
BgL_arg1556z00_6559 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6548); 
BgL_arg1555z00_6558 = 
(BgL_arg1556z00_6559-OBJECT_TYPE); } 
BgL_oclassz00_6556 = 
VECTOR_REF(BgL_arg1554z00_6557,BgL_arg1555z00_6558); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6560;
BgL__ortest_1220z00_6560 = 
(BgL_classz00_6547==BgL_oclassz00_6556); 
if(BgL__ortest_1220z00_6560)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6555 = BgL__ortest_1220z00_6560; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6561;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6562;
BgL_arg1810z00_6562 = 
(BgL_oclassz00_6556); 
BgL_odepthz00_6561 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6562); } 
if(
(BgL_arg1804z00_6549<BgL_odepthz00_6561))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6563;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6564;
BgL_arg1809z00_6564 = 
(BgL_oclassz00_6556); 
BgL_arg1808z00_6563 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6564, BgL_arg1804z00_6549); } 
BgL_res2618z00_6555 = 
(BgL_arg1808z00_6563==BgL_classz00_6547); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6555 = ((bool_t)0); } } } } 
BgL_test4875z00_11168 = BgL_res2618z00_6555; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4875z00_11168 = ((bool_t)0)
; } } 
if(BgL_test4875z00_11168)
{ /* Llib/object.scm 1419 */
return 
((BgL_objectz00_bglt)BgL_aux3200z00_5968);}  else 
{ 
 obj_t BgL_auxz00_11210;
BgL_auxz00_11210 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62221L), BGl_string3603z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_aux3200z00_5968); 
FAILURE(BgL_auxz00_11210,BFALSE,BFALSE);} } } } } 

}



/* &allocate-instance */
BgL_objectz00_bglt BGl_z62allocatezd2instancezb0zz__objectz00(obj_t BgL_envz00_4930, obj_t BgL_cnamez00_4931)
{
{ /* Llib/object.scm 1418 */
{ /* Llib/object.scm 1420 */
 obj_t BgL_auxz00_11214;
if(
SYMBOLP(BgL_cnamez00_4931))
{ /* Llib/object.scm 1420 */
BgL_auxz00_11214 = BgL_cnamez00_4931
; }  else 
{ 
 obj_t BgL_auxz00_11217;
BgL_auxz00_11217 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(62245L), BGl_string3612z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_cnamez00_4931); 
FAILURE(BgL_auxz00_11217,BFALSE,BFALSE);} 
return 
BGl_allocatezd2instancezd2zz__objectz00(BgL_auxz00_11214);} } 

}



/* wide-object? */
BGL_EXPORTED_DEF bool_t BGl_widezd2objectzf3z21zz__objectz00(BgL_objectz00_bglt BgL_objectz00_169)
{
{ /* Llib/object.scm 1439 */
return 
CBOOL(
BGL_OBJECT_WIDENING(BgL_objectz00_169));} 

}



/* &wide-object? */
obj_t BGl_z62widezd2objectzf3z43zz__objectz00(obj_t BgL_envz00_4932, obj_t BgL_objectz00_4933)
{
{ /* Llib/object.scm 1439 */
{ /* Llib/object.scm 1440 */
 bool_t BgL_tmpz00_11224;
{ /* Llib/object.scm 1440 */
 BgL_objectz00_bglt BgL_auxz00_11225;
if(
BGl_isazf3zf3zz__objectz00(BgL_objectz00_4933, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1440 */
BgL_auxz00_11225 = 
((BgL_objectz00_bglt)BgL_objectz00_4933)
; }  else 
{ 
 obj_t BgL_auxz00_11229;
BgL_auxz00_11229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63032L), BGl_string3613z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objectz00_4933); 
FAILURE(BgL_auxz00_11229,BFALSE,BFALSE);} 
BgL_tmpz00_11224 = 
BGl_widezd2objectzf3z21zz__objectz00(BgL_auxz00_11225); } 
return 
BBOOL(BgL_tmpz00_11224);} } 

}



/* call-virtual-getter */
BGL_EXPORTED_DEF obj_t BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_objectz00_bglt BgL_objz00_179, int BgL_numz00_180)
{
{ /* Llib/object.scm 1538 */
{ /* Llib/object.scm 1539 */
 obj_t BgL_classz00_3752;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_3757; long BgL_arg1555z00_3758;
BgL_arg1554z00_3757 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_3759;
BgL_arg1556z00_3759 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_179); 
BgL_arg1555z00_3758 = 
(BgL_arg1556z00_3759-OBJECT_TYPE); } 
BgL_classz00_3752 = 
VECTOR_REF(BgL_arg1554z00_3757,BgL_arg1555z00_3758); } 
{ /* Llib/object.scm 1539 */
 obj_t BgL_getterz00_3753;
{ /* Llib/object.scm 1540 */
 obj_t BgL_arg1828z00_3754;
{ /* Llib/object.scm 1540 */
 obj_t BgL_arg1829z00_3755;
{ /* Llib/object.scm 1540 */
 obj_t BgL_classz00_3765;
if(
BGL_CLASSP(BgL_classz00_3752))
{ /* Llib/object.scm 1540 */
BgL_classz00_3765 = BgL_classz00_3752; }  else 
{ 
 obj_t BgL_auxz00_11241;
BgL_auxz00_11241 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67237L), BGl_string3614z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_3752); 
FAILURE(BgL_auxz00_11241,BFALSE,BFALSE);} 
BgL_arg1829z00_3755 = 
BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_3765); } 
BgL_arg1828z00_3754 = 
VECTOR_REF(BgL_arg1829z00_3755,
(long)(BgL_numz00_180)); } 
{ /* Llib/object.scm 1540 */
 obj_t BgL_pairz00_3768;
if(
PAIRP(BgL_arg1828z00_3754))
{ /* Llib/object.scm 1540 */
BgL_pairz00_3768 = BgL_arg1828z00_3754; }  else 
{ 
 obj_t BgL_auxz00_11250;
BgL_auxz00_11250 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67247L), BGl_string3614z00zz__objectz00, BGl_string3565z00zz__objectz00, BgL_arg1828z00_3754); 
FAILURE(BgL_auxz00_11250,BFALSE,BFALSE);} 
BgL_getterz00_3753 = 
CAR(BgL_pairz00_3768); } } 
{ /* Llib/object.scm 1540 */

{ /* Llib/object.scm 1541 */
 obj_t BgL_funz00_5984;
if(
PROCEDUREP(BgL_getterz00_3753))
{ /* Llib/object.scm 1541 */
BgL_funz00_5984 = BgL_getterz00_3753; }  else 
{ 
 obj_t BgL_auxz00_11257;
BgL_auxz00_11257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67258L), BGl_string3614z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_getterz00_3753); 
FAILURE(BgL_auxz00_11257,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5984, 1))
{ /* Llib/object.scm 1541 */
return 
(VA_PROCEDUREP( BgL_funz00_5984 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5984))(BgL_getterz00_3753, 
((obj_t)BgL_objz00_179), BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5984))(BgL_getterz00_3753, 
((obj_t)BgL_objz00_179)) );}  else 
{ /* Llib/object.scm 1541 */
FAILURE(BGl_string3615z00zz__objectz00,BGl_list3616z00zz__objectz00,BgL_funz00_5984);} } } } } } 

}



/* &call-virtual-getter */
obj_t BGl_z62callzd2virtualzd2getterz62zz__objectz00(obj_t BgL_envz00_4934, obj_t BgL_objz00_4935, obj_t BgL_numz00_4936)
{
{ /* Llib/object.scm 1538 */
{ /* Llib/object.scm 1539 */
 int BgL_auxz00_11277; BgL_objectz00_bglt BgL_auxz00_11269;
{ /* Llib/object.scm 1539 */
 obj_t BgL_tmpz00_11278;
if(
INTEGERP(BgL_numz00_4936))
{ /* Llib/object.scm 1539 */
BgL_tmpz00_11278 = BgL_numz00_4936
; }  else 
{ 
 obj_t BgL_auxz00_11281;
BgL_auxz00_11281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67157L), BGl_string3621z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_numz00_4936); 
FAILURE(BgL_auxz00_11281,BFALSE,BFALSE);} 
BgL_auxz00_11277 = 
CINT(BgL_tmpz00_11278); } 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4935, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1539 */
BgL_auxz00_11269 = 
((BgL_objectz00_bglt)BgL_objz00_4935)
; }  else 
{ 
 obj_t BgL_auxz00_11273;
BgL_auxz00_11273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67157L), BGl_string3621z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4935); 
FAILURE(BgL_auxz00_11273,BFALSE,BFALSE);} 
return 
BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_auxz00_11269, BgL_auxz00_11277);} } 

}



/* call-virtual-setter */
BGL_EXPORTED_DEF obj_t BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_objectz00_bglt BgL_objz00_181, int BgL_numz00_182, obj_t BgL_valuez00_183)
{
{ /* Llib/object.scm 1551 */
{ /* Llib/object.scm 1552 */
 obj_t BgL_classz00_3769;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_3774; long BgL_arg1555z00_3775;
BgL_arg1554z00_3774 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_3776;
BgL_arg1556z00_3776 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_181); 
BgL_arg1555z00_3775 = 
(BgL_arg1556z00_3776-OBJECT_TYPE); } 
BgL_classz00_3769 = 
VECTOR_REF(BgL_arg1554z00_3774,BgL_arg1555z00_3775); } 
{ /* Llib/object.scm 1552 */
 obj_t BgL_setterz00_3770;
{ /* Llib/object.scm 1553 */
 obj_t BgL_arg1831z00_3771;
{ /* Llib/object.scm 1553 */
 obj_t BgL_arg1832z00_3772;
{ /* Llib/object.scm 1553 */
 obj_t BgL_classz00_3782;
if(
BGL_CLASSP(BgL_classz00_3769))
{ /* Llib/object.scm 1553 */
BgL_classz00_3782 = BgL_classz00_3769; }  else 
{ 
 obj_t BgL_auxz00_11293;
BgL_auxz00_11293 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(68006L), BGl_string3622z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_3769); 
FAILURE(BgL_auxz00_11293,BFALSE,BFALSE);} 
BgL_arg1832z00_3772 = 
BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_3782); } 
BgL_arg1831z00_3771 = 
VECTOR_REF(BgL_arg1832z00_3772,
(long)(BgL_numz00_182)); } 
{ /* Llib/object.scm 1553 */
 obj_t BgL_pairz00_3785;
if(
PAIRP(BgL_arg1831z00_3771))
{ /* Llib/object.scm 1553 */
BgL_pairz00_3785 = BgL_arg1831z00_3771; }  else 
{ 
 obj_t BgL_auxz00_11302;
BgL_auxz00_11302 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(68016L), BGl_string3622z00zz__objectz00, BGl_string3565z00zz__objectz00, BgL_arg1831z00_3771); 
FAILURE(BgL_auxz00_11302,BFALSE,BFALSE);} 
BgL_setterz00_3770 = 
CDR(BgL_pairz00_3785); } } 
{ /* Llib/object.scm 1553 */

{ /* Llib/object.scm 1554 */
 obj_t BgL_funz00_5997;
if(
PROCEDUREP(BgL_setterz00_3770))
{ /* Llib/object.scm 1554 */
BgL_funz00_5997 = BgL_setterz00_3770; }  else 
{ 
 obj_t BgL_auxz00_11309;
BgL_auxz00_11309 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(68027L), BGl_string3622z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_setterz00_3770); 
FAILURE(BgL_auxz00_11309,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5997, 2))
{ /* Llib/object.scm 1554 */
return 
(VA_PROCEDUREP( BgL_funz00_5997 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5997))(BgL_setterz00_3770, 
((obj_t)BgL_objz00_181), BgL_valuez00_183, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5997))(BgL_setterz00_3770, 
((obj_t)BgL_objz00_181), BgL_valuez00_183) );}  else 
{ /* Llib/object.scm 1554 */
FAILURE(BGl_string3623z00zz__objectz00,BGl_list3624z00zz__objectz00,BgL_funz00_5997);} } } } } } 

}



/* &call-virtual-setter */
obj_t BGl_z62callzd2virtualzd2setterz62zz__objectz00(obj_t BgL_envz00_4937, obj_t BgL_objz00_4938, obj_t BgL_numz00_4939, obj_t BgL_valuez00_4940)
{
{ /* Llib/object.scm 1551 */
{ /* Llib/object.scm 1552 */
 int BgL_auxz00_11330; BgL_objectz00_bglt BgL_auxz00_11322;
{ /* Llib/object.scm 1552 */
 obj_t BgL_tmpz00_11331;
if(
INTEGERP(BgL_numz00_4939))
{ /* Llib/object.scm 1552 */
BgL_tmpz00_11331 = BgL_numz00_4939
; }  else 
{ 
 obj_t BgL_auxz00_11334;
BgL_auxz00_11334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67926L), BGl_string3629z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_numz00_4939); 
FAILURE(BgL_auxz00_11334,BFALSE,BFALSE);} 
BgL_auxz00_11330 = 
CINT(BgL_tmpz00_11331); } 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4938, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1552 */
BgL_auxz00_11322 = 
((BgL_objectz00_bglt)BgL_objz00_4938)
; }  else 
{ 
 obj_t BgL_auxz00_11326;
BgL_auxz00_11326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(67926L), BGl_string3629z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4938); 
FAILURE(BgL_auxz00_11326,BFALSE,BFALSE);} 
return 
BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_auxz00_11322, BgL_auxz00_11330, BgL_valuez00_4940);} } 

}



/* call-next-virtual-getter */
BGL_EXPORTED_DEF obj_t BGl_callzd2nextzd2virtualzd2getterzd2zz__objectz00(obj_t BgL_classz00_184, BgL_objectz00_bglt BgL_objz00_185, int BgL_numz00_186)
{
{ /* Llib/object.scm 1574 */
{ /* Llib/object.scm 1575 */
 obj_t BgL_nextzd2classzd2_3786;
{ /* Llib/object.scm 1575 */
 obj_t BgL_classz00_3790;
if(
BGL_CLASSP(BgL_classz00_184))
{ /* Llib/object.scm 1575 */
BgL_classz00_3790 = BgL_classz00_184; }  else 
{ 
 obj_t BgL_auxz00_11342;
BgL_auxz00_11342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(69477L), BGl_string3630z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_184); 
FAILURE(BgL_auxz00_11342,BFALSE,BFALSE);} 
BgL_nextzd2classzd2_3786 = 
BGL_CLASS_SUPER(BgL_classz00_3790); } 
{ /* Llib/object.scm 1576 */
 obj_t BgL_fun1836z00_3787;
{ /* Llib/object.scm 1576 */
 obj_t BgL_arg1833z00_3788;
{ /* Llib/object.scm 1576 */
 obj_t BgL_arg1834z00_3789;
{ /* Llib/object.scm 1576 */
 obj_t BgL_classz00_3791;
if(
BGL_CLASSP(BgL_nextzd2classzd2_3786))
{ /* Llib/object.scm 1576 */
BgL_classz00_3791 = BgL_nextzd2classzd2_3786; }  else 
{ 
 obj_t BgL_auxz00_11349;
BgL_auxz00_11349 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(69528L), BGl_string3630z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_nextzd2classzd2_3786); 
FAILURE(BgL_auxz00_11349,BFALSE,BFALSE);} 
BgL_arg1834z00_3789 = 
BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_3791); } 
BgL_arg1833z00_3788 = 
VECTOR_REF(BgL_arg1834z00_3789,
(long)(BgL_numz00_186)); } 
{ /* Llib/object.scm 1576 */
 obj_t BgL_pairz00_3794;
if(
PAIRP(BgL_arg1833z00_3788))
{ /* Llib/object.scm 1576 */
BgL_pairz00_3794 = BgL_arg1833z00_3788; }  else 
{ 
 obj_t BgL_auxz00_11358;
BgL_auxz00_11358 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(69543L), BGl_string3630z00zz__objectz00, BGl_string3565z00zz__objectz00, BgL_arg1833z00_3788); 
FAILURE(BgL_auxz00_11358,BFALSE,BFALSE);} 
BgL_fun1836z00_3787 = 
CAR(BgL_pairz00_3794); } } 
{ /* Llib/object.scm 1576 */
 obj_t BgL_funz00_6012;
if(
PROCEDUREP(BgL_fun1836z00_3787))
{ /* Llib/object.scm 1576 */
BgL_funz00_6012 = BgL_fun1836z00_3787; }  else 
{ 
 obj_t BgL_auxz00_11365;
BgL_auxz00_11365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(69492L), BGl_string3630z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_fun1836z00_3787); 
FAILURE(BgL_auxz00_11365,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_6012, 1))
{ /* Llib/object.scm 1576 */
return 
(VA_PROCEDUREP( BgL_funz00_6012 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_6012))(BgL_fun1836z00_3787, 
((obj_t)BgL_objz00_185), BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_6012))(BgL_fun1836z00_3787, 
((obj_t)BgL_objz00_185)) );}  else 
{ /* Llib/object.scm 1576 */
FAILURE(BGl_string3631z00zz__objectz00,BGl_list3632z00zz__objectz00,BgL_funz00_6012);} } } } } 

}



/* &call-next-virtual-getter */
obj_t BGl_z62callzd2nextzd2virtualzd2getterzb0zz__objectz00(obj_t BgL_envz00_4941, obj_t BgL_classz00_4942, obj_t BgL_objz00_4943, obj_t BgL_numz00_4944)
{
{ /* Llib/object.scm 1574 */
{ /* Llib/object.scm 1575 */
 int BgL_auxz00_11385; BgL_objectz00_bglt BgL_auxz00_11377;
{ /* Llib/object.scm 1575 */
 obj_t BgL_tmpz00_11386;
if(
INTEGERP(BgL_numz00_4944))
{ /* Llib/object.scm 1575 */
BgL_tmpz00_11386 = BgL_numz00_4944
; }  else 
{ 
 obj_t BgL_auxz00_11389;
BgL_auxz00_11389 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(69446L), BGl_string3635z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_numz00_4944); 
FAILURE(BgL_auxz00_11389,BFALSE,BFALSE);} 
BgL_auxz00_11385 = 
CINT(BgL_tmpz00_11386); } 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4943, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1575 */
BgL_auxz00_11377 = 
((BgL_objectz00_bglt)BgL_objz00_4943)
; }  else 
{ 
 obj_t BgL_auxz00_11381;
BgL_auxz00_11381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(69446L), BGl_string3635z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4943); 
FAILURE(BgL_auxz00_11381,BFALSE,BFALSE);} 
return 
BGl_callzd2nextzd2virtualzd2getterzd2zz__objectz00(BgL_classz00_4942, BgL_auxz00_11377, BgL_auxz00_11385);} } 

}



/* call-next-virtual-setter */
BGL_EXPORTED_DEF obj_t BGl_callzd2nextzd2virtualzd2setterzd2zz__objectz00(obj_t BgL_classz00_187, BgL_objectz00_bglt BgL_objz00_188, int BgL_numz00_189, obj_t BgL_valuez00_190)
{
{ /* Llib/object.scm 1586 */
{ /* Llib/object.scm 1587 */
 obj_t BgL_nextzd2classzd2_3795;
{ /* Llib/object.scm 1587 */
 obj_t BgL_classz00_3799;
if(
BGL_CLASSP(BgL_classz00_187))
{ /* Llib/object.scm 1587 */
BgL_classz00_3799 = BgL_classz00_187; }  else 
{ 
 obj_t BgL_auxz00_11397;
BgL_auxz00_11397 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70248L), BGl_string3636z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_187); 
FAILURE(BgL_auxz00_11397,BFALSE,BFALSE);} 
BgL_nextzd2classzd2_3795 = 
BGL_CLASS_SUPER(BgL_classz00_3799); } 
{ /* Llib/object.scm 1588 */
 obj_t BgL_fun1839z00_3796;
{ /* Llib/object.scm 1588 */
 obj_t BgL_arg1837z00_3797;
{ /* Llib/object.scm 1588 */
 obj_t BgL_arg1838z00_3798;
{ /* Llib/object.scm 1588 */
 obj_t BgL_classz00_3800;
if(
BGL_CLASSP(BgL_nextzd2classzd2_3795))
{ /* Llib/object.scm 1588 */
BgL_classz00_3800 = BgL_nextzd2classzd2_3795; }  else 
{ 
 obj_t BgL_auxz00_11404;
BgL_auxz00_11404 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70299L), BGl_string3636z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_nextzd2classzd2_3795); 
FAILURE(BgL_auxz00_11404,BFALSE,BFALSE);} 
BgL_arg1838z00_3798 = 
BGL_CLASS_VIRTUAL_FIELDS(BgL_classz00_3800); } 
BgL_arg1837z00_3797 = 
VECTOR_REF(BgL_arg1838z00_3798,
(long)(BgL_numz00_189)); } 
{ /* Llib/object.scm 1588 */
 obj_t BgL_pairz00_3803;
if(
PAIRP(BgL_arg1837z00_3797))
{ /* Llib/object.scm 1588 */
BgL_pairz00_3803 = BgL_arg1837z00_3797; }  else 
{ 
 obj_t BgL_auxz00_11413;
BgL_auxz00_11413 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70314L), BGl_string3636z00zz__objectz00, BGl_string3565z00zz__objectz00, BgL_arg1837z00_3797); 
FAILURE(BgL_auxz00_11413,BFALSE,BFALSE);} 
BgL_fun1839z00_3796 = 
CDR(BgL_pairz00_3803); } } 
{ /* Llib/object.scm 1588 */
 obj_t BgL_funz00_6027;
if(
PROCEDUREP(BgL_fun1839z00_3796))
{ /* Llib/object.scm 1588 */
BgL_funz00_6027 = BgL_fun1839z00_3796; }  else 
{ 
 obj_t BgL_auxz00_11420;
BgL_auxz00_11420 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70263L), BGl_string3636z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_fun1839z00_3796); 
FAILURE(BgL_auxz00_11420,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_6027, 2))
{ /* Llib/object.scm 1588 */
return 
(VA_PROCEDUREP( BgL_funz00_6027 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_6027))(BgL_fun1839z00_3796, 
((obj_t)BgL_objz00_188), BgL_valuez00_190, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_6027))(BgL_fun1839z00_3796, 
((obj_t)BgL_objz00_188), BgL_valuez00_190) );}  else 
{ /* Llib/object.scm 1588 */
FAILURE(BGl_string3637z00zz__objectz00,BGl_list3638z00zz__objectz00,BgL_funz00_6027);} } } } } 

}



/* &call-next-virtual-setter */
obj_t BGl_z62callzd2nextzd2virtualzd2setterzb0zz__objectz00(obj_t BgL_envz00_4945, obj_t BgL_classz00_4946, obj_t BgL_objz00_4947, obj_t BgL_numz00_4948, obj_t BgL_valuez00_4949)
{
{ /* Llib/object.scm 1586 */
{ /* Llib/object.scm 1587 */
 int BgL_auxz00_11441; BgL_objectz00_bglt BgL_auxz00_11433;
{ /* Llib/object.scm 1587 */
 obj_t BgL_tmpz00_11442;
if(
INTEGERP(BgL_numz00_4948))
{ /* Llib/object.scm 1587 */
BgL_tmpz00_11442 = BgL_numz00_4948
; }  else 
{ 
 obj_t BgL_auxz00_11445;
BgL_auxz00_11445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70217L), BGl_string3641z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_numz00_4948); 
FAILURE(BgL_auxz00_11445,BFALSE,BFALSE);} 
BgL_auxz00_11441 = 
CINT(BgL_tmpz00_11442); } 
if(
BGl_isazf3zf3zz__objectz00(BgL_objz00_4947, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1587 */
BgL_auxz00_11433 = 
((BgL_objectz00_bglt)BgL_objz00_4947)
; }  else 
{ 
 obj_t BgL_auxz00_11437;
BgL_auxz00_11437 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70217L), BGl_string3641z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_4947); 
FAILURE(BgL_auxz00_11437,BFALSE,BFALSE);} 
return 
BGl_callzd2nextzd2virtualzd2setterzd2zz__objectz00(BgL_classz00_4946, BgL_auxz00_11433, BgL_auxz00_11441, BgL_valuez00_4949);} } 

}



/* object-widening */
BGL_EXPORTED_DEF obj_t BGl_objectzd2wideningzd2zz__objectz00(BgL_objectz00_bglt BgL_oz00_191)
{
{ /* Llib/object.scm 1593 */
return 
BGL_OBJECT_WIDENING(BgL_oz00_191);} 

}



/* &object-widening */
obj_t BGl_z62objectzd2wideningzb0zz__objectz00(obj_t BgL_envz00_4950, obj_t BgL_oz00_4951)
{
{ /* Llib/object.scm 1593 */
{ /* Llib/object.scm 1594 */
 BgL_objectz00_bglt BgL_auxz00_11452;
if(
BGl_isazf3zf3zz__objectz00(BgL_oz00_4951, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1594 */
BgL_auxz00_11452 = 
((BgL_objectz00_bglt)BgL_oz00_4951)
; }  else 
{ 
 obj_t BgL_auxz00_11456;
BgL_auxz00_11456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70591L), BGl_string3642z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_oz00_4951); 
FAILURE(BgL_auxz00_11456,BFALSE,BFALSE);} 
return 
BGl_objectzd2wideningzd2zz__objectz00(BgL_auxz00_11452);} } 

}



/* object-widening-set! */
BGL_EXPORTED_DEF obj_t BGl_objectzd2wideningzd2setz12z12zz__objectz00(BgL_objectz00_bglt BgL_oz00_192, obj_t BgL_vz00_193)
{
{ /* Llib/object.scm 1599 */
BGL_OBJECT_WIDENING_SET(BgL_oz00_192, BgL_vz00_193); 
return 
((obj_t)BgL_oz00_192);} 

}



/* &object-widening-set! */
obj_t BGl_z62objectzd2wideningzd2setz12z70zz__objectz00(obj_t BgL_envz00_4952, obj_t BgL_oz00_4953, obj_t BgL_vz00_4954)
{
{ /* Llib/object.scm 1599 */
{ /* Llib/object.scm 1600 */
 BgL_objectz00_bglt BgL_auxz00_11463;
if(
BGl_isazf3zf3zz__objectz00(BgL_oz00_4953, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1600 */
BgL_auxz00_11463 = 
((BgL_objectz00_bglt)BgL_oz00_4953)
; }  else 
{ 
 obj_t BgL_auxz00_11467;
BgL_auxz00_11467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70907L), BGl_string3643z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_oz00_4953); 
FAILURE(BgL_auxz00_11467,BFALSE,BFALSE);} 
return 
BGl_objectzd2wideningzd2setz12z12zz__objectz00(BgL_auxz00_11463, BgL_vz00_4954);} } 

}



/* %object-widening */
BGL_EXPORTED_DEF obj_t BGl_z52objectzd2wideningz80zz__objectz00(BgL_objectz00_bglt BgL_oz00_194)
{
{ /* Llib/object.scm 1609 */
return 
BGL_OBJECT_WIDENING(BgL_oz00_194);} 

}



/* &%object-widening */
obj_t BGl_z62z52objectzd2wideningze2zz__objectz00(obj_t BgL_envz00_4955, obj_t BgL_oz00_4956)
{
{ /* Llib/object.scm 1609 */
{ /* Llib/object.scm 1594 */
 BgL_objectz00_bglt BgL_auxz00_11473;
if(
BGl_isazf3zf3zz__objectz00(BgL_oz00_4956, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1594 */
BgL_auxz00_11473 = 
((BgL_objectz00_bglt)BgL_oz00_4956)
; }  else 
{ 
 obj_t BgL_auxz00_11477;
BgL_auxz00_11477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(70591L), BGl_string3644z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_oz00_4956); 
FAILURE(BgL_auxz00_11477,BFALSE,BFALSE);} 
return 
BGl_z52objectzd2wideningz80zz__objectz00(BgL_auxz00_11473);} } 

}



/* %object-widening-set! */
BGL_EXPORTED_DEF obj_t BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_objectz00_bglt BgL_oz00_195, obj_t BgL_wz00_196)
{
{ /* Llib/object.scm 1618 */
BGL_OBJECT_WIDENING_SET(BgL_oz00_195, BgL_wz00_196); BgL_oz00_195; 
return 
((obj_t)BgL_oz00_195);} 

}



/* &%object-widening-set! */
obj_t BGl_z62z52objectzd2wideningzd2setz12z22zz__objectz00(obj_t BgL_envz00_4957, obj_t BgL_oz00_4958, obj_t BgL_wz00_4959)
{
{ /* Llib/object.scm 1618 */
{ /* Llib/object.scm 1619 */
 BgL_objectz00_bglt BgL_auxz00_11484;
if(
BGl_isazf3zf3zz__objectz00(BgL_oz00_4958, BGl_objectz00zz__objectz00))
{ /* Llib/object.scm 1619 */
BgL_auxz00_11484 = 
((BgL_objectz00_bglt)BgL_oz00_4958)
; }  else 
{ 
 obj_t BgL_auxz00_11488;
BgL_auxz00_11488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(71922L), BGl_string3645z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_oz00_4958); 
FAILURE(BgL_auxz00_11488,BFALSE,BFALSE);} 
return 
BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_auxz00_11484, BgL_wz00_4959);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__objectz00(void)
{
{ /* Llib/object.scm 17 */
{ /* Llib/object.scm 236 */
 obj_t BgL_arg1844z00_1754; obj_t BgL_arg1845z00_1755;
{ /* Llib/object.scm 236 */
 obj_t BgL_v1328z00_1765;
BgL_v1328z00_1765 = 
create_vector(0L); 
BgL_arg1844z00_1754 = BgL_v1328z00_1765; } 
{ /* Llib/object.scm 236 */
 obj_t BgL_v1329z00_1766;
BgL_v1329z00_1766 = 
create_vector(0L); 
BgL_arg1845z00_1755 = BgL_v1329z00_1766; } 
BGl_objectz00zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3649z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BFALSE, 43933L, BGl_proc3648z00zz__objectz00, BGl_proc3647z00zz__objectz00, BFALSE, BGl_proc3646z00zz__objectz00, BFALSE, BgL_arg1844z00_1754, BgL_arg1845z00_1755); } 
{ /* Llib/object.scm 237 */
 obj_t BgL_arg1854z00_1773; obj_t BgL_arg1856z00_1774;
{ /* Llib/object.scm 237 */
 obj_t BgL_v1330z00_1784;
BgL_v1330z00_1784 = 
create_vector(0L); 
BgL_arg1854z00_1773 = BgL_v1330z00_1784; } 
{ /* Llib/object.scm 237 */
 obj_t BgL_v1331z00_1785;
BgL_v1331z00_1785 = 
create_vector(0L); 
BgL_arg1856z00_1774 = BgL_v1331z00_1785; } 
BGl_z62conditionz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3655z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_objectz00zz__objectz00, 33606L, BGl_proc3654z00zz__objectz00, BGl_proc3653z00zz__objectz00, BFALSE, BGl_proc3652z00zz__objectz00, BFALSE, BgL_arg1854z00_1773, BgL_arg1856z00_1774); } 
{ /* Llib/object.scm 239 */
 obj_t BgL_arg1866z00_1792; obj_t BgL_arg1868z00_1793;
{ /* Llib/object.scm 239 */
 obj_t BgL_v1332z00_1806;
BgL_v1332z00_1806 = 
create_vector(3L); 
{ /* Llib/object.scm 239 */
 obj_t BgL_arg1874z00_1807;
{ /* Llib/object.scm 239 */
 obj_t BgL_res2623z00_3833;
{ /* Llib/object.scm 239 */
 obj_t BgL_namez00_3818; obj_t BgL_typez00_3821;
BgL_namez00_3818 = BGl_symbol3660z00zz__objectz00; 
BgL_typez00_3821 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3822;
BgL_v1316z00_3822 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3822,0L,BgL_namez00_3818); 
VECTOR_SET(BgL_v1316z00_3822,1L,BGl_proc3659z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3822,2L,BGl_proc3658z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3822,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3822,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3822,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3822,6L,BGl_proc3657z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3822,7L,BgL_typez00_3821); 
VECTOR_SET(BgL_v1316z00_3822,8L,BTRUE); 
BgL_res2623z00_3833 = BgL_v1316z00_3822; } } 
BgL_arg1874z00_1807 = BgL_res2623z00_3833; } 
VECTOR_SET(BgL_v1332z00_1806,0L,BgL_arg1874z00_1807); } 
{ /* Llib/object.scm 239 */
 obj_t BgL_arg1882z00_1820;
{ /* Llib/object.scm 239 */
 obj_t BgL_res2624z00_3850;
{ /* Llib/object.scm 239 */
 obj_t BgL_namez00_3835; obj_t BgL_typez00_3838;
BgL_namez00_3835 = BGl_symbol3665z00zz__objectz00; 
BgL_typez00_3838 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3839;
BgL_v1316z00_3839 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3839,0L,BgL_namez00_3835); 
VECTOR_SET(BgL_v1316z00_3839,1L,BGl_proc3664z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3839,2L,BGl_proc3663z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3839,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3839,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3839,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3839,6L,BGl_proc3662z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3839,7L,BgL_typez00_3838); 
VECTOR_SET(BgL_v1316z00_3839,8L,BTRUE); 
BgL_res2624z00_3850 = BgL_v1316z00_3839; } } 
BgL_arg1882z00_1820 = BgL_res2624z00_3850; } 
VECTOR_SET(BgL_v1332z00_1806,1L,BgL_arg1882z00_1820); } 
{ /* Llib/object.scm 239 */
 obj_t BgL_arg1889z00_1833;
{ /* Llib/object.scm 239 */
 obj_t BgL_res2625z00_3868;
{ /* Llib/object.scm 239 */
 obj_t BgL_namez00_3853; obj_t BgL_typez00_3856;
BgL_namez00_3853 = BGl_symbol3670z00zz__objectz00; 
BgL_typez00_3856 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3857;
BgL_v1316z00_3857 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3857,0L,BgL_namez00_3853); 
VECTOR_SET(BgL_v1316z00_3857,1L,BGl_proc3669z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3857,2L,BGl_proc3668z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3857,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3857,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3857,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3857,6L,BGl_proc3667z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3857,7L,BgL_typez00_3856); 
VECTOR_SET(BgL_v1316z00_3857,8L,BTRUE); 
BgL_res2625z00_3868 = BgL_v1316z00_3857; } } 
BgL_arg1889z00_1833 = BgL_res2625z00_3868; } 
VECTOR_SET(BgL_v1332z00_1806,2L,BgL_arg1889z00_1833); } 
BgL_arg1866z00_1792 = BgL_v1332z00_1806; } 
{ /* Llib/object.scm 239 */
 obj_t BgL_v1333z00_1847;
BgL_v1333z00_1847 = 
create_vector(0L); 
BgL_arg1868z00_1793 = BgL_v1333z00_1847; } 
BGl_z62exceptionz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3675z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62conditionz62zz__objectz00, 42025L, BGl_proc3674z00zz__objectz00, BGl_proc3673z00zz__objectz00, BFALSE, BGl_proc3672z00zz__objectz00, BFALSE, BgL_arg1866z00_1792, BgL_arg1868z00_1793); } 
{ /* Llib/object.scm 244 */
 obj_t BgL_arg1899z00_1854; obj_t BgL_arg1901z00_1855;
{ /* Llib/object.scm 244 */
 obj_t BgL_v1334z00_1871;
BgL_v1334z00_1871 = 
create_vector(3L); 
{ /* Llib/object.scm 244 */
 obj_t BgL_arg1910z00_1872;
{ /* Llib/object.scm 244 */
 obj_t BgL_res2626z00_3894;
{ /* Llib/object.scm 244 */
 obj_t BgL_namez00_3879; obj_t BgL_typez00_3882;
BgL_namez00_3879 = BGl_symbol3512z00zz__objectz00; 
BgL_typez00_3882 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3883;
BgL_v1316z00_3883 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3883,0L,BgL_namez00_3879); 
VECTOR_SET(BgL_v1316z00_3883,1L,BGl_proc3678z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3883,2L,BGl_proc3677z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3883,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3883,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3883,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3883,6L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3883,7L,BgL_typez00_3882); 
VECTOR_SET(BgL_v1316z00_3883,8L,BTRUE); 
BgL_res2626z00_3894 = BgL_v1316z00_3883; } } 
BgL_arg1910z00_1872 = BgL_res2626z00_3894; } 
VECTOR_SET(BgL_v1334z00_1871,0L,BgL_arg1910z00_1872); } 
{ /* Llib/object.scm 244 */
 obj_t BgL_arg1916z00_1882;
{ /* Llib/object.scm 244 */
 obj_t BgL_res2627z00_3911;
{ /* Llib/object.scm 244 */
 obj_t BgL_namez00_3896; obj_t BgL_typez00_3899;
BgL_namez00_3896 = BGl_symbol3681z00zz__objectz00; 
BgL_typez00_3899 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3900;
BgL_v1316z00_3900 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3900,0L,BgL_namez00_3896); 
VECTOR_SET(BgL_v1316z00_3900,1L,BGl_proc3680z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3900,2L,BGl_proc3679z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3900,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3900,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3900,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3900,6L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3900,7L,BgL_typez00_3899); 
VECTOR_SET(BgL_v1316z00_3900,8L,BFALSE); 
BgL_res2627z00_3911 = BgL_v1316z00_3900; } } 
BgL_arg1916z00_1882 = BgL_res2627z00_3911; } 
VECTOR_SET(BgL_v1334z00_1871,1L,BgL_arg1916z00_1882); } 
{ /* Llib/object.scm 244 */
 obj_t BgL_arg1923z00_1892;
{ /* Llib/object.scm 244 */
 obj_t BgL_res2628z00_3928;
{ /* Llib/object.scm 244 */
 obj_t BgL_namez00_3913; obj_t BgL_typez00_3916;
BgL_namez00_3913 = BGl_symbol3619z00zz__objectz00; 
BgL_typez00_3916 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3917;
BgL_v1316z00_3917 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3917,0L,BgL_namez00_3913); 
VECTOR_SET(BgL_v1316z00_3917,1L,BGl_proc3684z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3917,2L,BGl_proc3683z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3917,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3917,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3917,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3917,6L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3917,7L,BgL_typez00_3916); 
VECTOR_SET(BgL_v1316z00_3917,8L,BFALSE); 
BgL_res2628z00_3928 = BgL_v1316z00_3917; } } 
BgL_arg1923z00_1892 = BgL_res2628z00_3928; } 
VECTOR_SET(BgL_v1334z00_1871,2L,BgL_arg1923z00_1892); } 
BgL_arg1899z00_1854 = BgL_v1334z00_1871; } 
{ /* Llib/object.scm 244 */
 obj_t BgL_v1335z00_1902;
BgL_v1335z00_1902 = 
create_vector(0L); 
BgL_arg1901z00_1855 = BgL_v1335z00_1902; } 
BGl_z62errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3688z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62exceptionz62zz__objectz00, 27634L, BGl_proc3687z00zz__objectz00, BGl_proc3686z00zz__objectz00, BFALSE, BGl_proc3685z00zz__objectz00, BFALSE, BgL_arg1899z00_1854, BgL_arg1901z00_1855); } 
{ /* Llib/object.scm 248 */
 obj_t BgL_arg1931z00_1909; obj_t BgL_arg1932z00_1910;
{ /* Llib/object.scm 248 */
 obj_t BgL_v1336z00_1927;
BgL_v1336z00_1927 = 
create_vector(1L); 
{ /* Llib/object.scm 248 */
 obj_t BgL_arg1938z00_1928;
{ /* Llib/object.scm 248 */
 obj_t BgL_res2629z00_3954;
{ /* Llib/object.scm 248 */
 obj_t BgL_namez00_3939; obj_t BgL_typez00_3942;
BgL_namez00_3939 = BGl_symbol3692z00zz__objectz00; 
BgL_typez00_3942 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3943;
BgL_v1316z00_3943 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3943,0L,BgL_namez00_3939); 
VECTOR_SET(BgL_v1316z00_3943,1L,BGl_proc3691z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3943,2L,BGl_proc3690z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3943,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3943,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3943,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3943,6L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3943,7L,BgL_typez00_3942); 
VECTOR_SET(BgL_v1316z00_3943,8L,BFALSE); 
BgL_res2629z00_3954 = BgL_v1316z00_3943; } } 
BgL_arg1938z00_1928 = BgL_res2629z00_3954; } 
VECTOR_SET(BgL_v1336z00_1927,0L,BgL_arg1938z00_1928); } 
BgL_arg1931z00_1909 = BgL_v1336z00_1927; } 
{ /* Llib/object.scm 248 */
 obj_t BgL_v1337z00_1938;
BgL_v1337z00_1938 = 
create_vector(0L); 
BgL_arg1932z00_1910 = BgL_v1337z00_1938; } 
BGl_z62typezd2errorzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3697z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62errorz62zz__objectz00, 59179L, BGl_proc3696z00zz__objectz00, BGl_proc3695z00zz__objectz00, BFALSE, BGl_proc3694z00zz__objectz00, BFALSE, BgL_arg1931z00_1909, BgL_arg1932z00_1910); } 
{ /* Llib/object.scm 250 */
 obj_t BgL_arg1946z00_1945; obj_t BgL_arg1947z00_1946;
{ /* Llib/object.scm 250 */
 obj_t BgL_v1338z00_1963;
BgL_v1338z00_1963 = 
create_vector(1L); 
{ /* Llib/object.scm 250 */
 obj_t BgL_arg1953z00_1964;
{ /* Llib/object.scm 250 */
 obj_t BgL_res2630z00_3980;
{ /* Llib/object.scm 250 */
 obj_t BgL_namez00_3965; obj_t BgL_typez00_3968;
BgL_namez00_3965 = BGl_symbol3701z00zz__objectz00; 
BgL_typez00_3968 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_3969;
BgL_v1316z00_3969 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_3969,0L,BgL_namez00_3965); 
VECTOR_SET(BgL_v1316z00_3969,1L,BGl_proc3700z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3969,2L,BGl_proc3699z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3969,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3969,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_3969,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3969,6L,BFALSE); 
VECTOR_SET(BgL_v1316z00_3969,7L,BgL_typez00_3968); 
VECTOR_SET(BgL_v1316z00_3969,8L,BFALSE); 
BgL_res2630z00_3980 = BgL_v1316z00_3969; } } 
BgL_arg1953z00_1964 = BgL_res2630z00_3980; } 
VECTOR_SET(BgL_v1338z00_1963,0L,BgL_arg1953z00_1964); } 
BgL_arg1946z00_1945 = BgL_v1338z00_1963; } 
{ /* Llib/object.scm 250 */
 obj_t BgL_v1339z00_1974;
BgL_v1339z00_1974 = 
create_vector(0L); 
BgL_arg1947z00_1946 = BgL_v1339z00_1974; } 
BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3706z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62errorz62zz__objectz00, 17130L, BGl_proc3705z00zz__objectz00, BGl_proc3704z00zz__objectz00, BFALSE, BGl_proc3703z00zz__objectz00, BFALSE, BgL_arg1946z00_1945, BgL_arg1947z00_1946); } 
{ /* Llib/object.scm 252 */
 obj_t BgL_arg1961z00_1981; obj_t BgL_arg1962z00_1982;
{ /* Llib/object.scm 252 */
 obj_t BgL_v1340z00_1998;
BgL_v1340z00_1998 = 
create_vector(0L); 
BgL_arg1961z00_1981 = BgL_v1340z00_1998; } 
{ /* Llib/object.scm 252 */
 obj_t BgL_v1341z00_1999;
BgL_v1341z00_1999 = 
create_vector(0L); 
BgL_arg1962z00_1982 = BgL_v1341z00_1999; } 
BGl_z62iozd2errorzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3711z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62errorz62zz__objectz00, 63818L, BGl_proc3710z00zz__objectz00, BGl_proc3709z00zz__objectz00, BFALSE, BGl_proc3708z00zz__objectz00, BFALSE, BgL_arg1961z00_1981, BgL_arg1962z00_1982); } 
{ /* Llib/object.scm 253 */
 obj_t BgL_arg1971z00_2006; obj_t BgL_arg1972z00_2007;
{ /* Llib/object.scm 253 */
 obj_t BgL_v1342z00_2023;
BgL_v1342z00_2023 = 
create_vector(0L); 
BgL_arg1971z00_2006 = BgL_v1342z00_2023; } 
{ /* Llib/object.scm 253 */
 obj_t BgL_v1343z00_2024;
BgL_v1343z00_2024 = 
create_vector(0L); 
BgL_arg1972z00_2007 = BgL_v1343z00_2024; } 
BGl_z62iozd2portzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3716z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 55779L, BGl_proc3715z00zz__objectz00, BGl_proc3714z00zz__objectz00, BFALSE, BGl_proc3713z00zz__objectz00, BFALSE, BgL_arg1971z00_2006, BgL_arg1972z00_2007); } 
{ /* Llib/object.scm 254 */
 obj_t BgL_arg1981z00_2031; obj_t BgL_arg1982z00_2032;
{ /* Llib/object.scm 254 */
 obj_t BgL_v1344z00_2048;
BgL_v1344z00_2048 = 
create_vector(0L); 
BgL_arg1981z00_2031 = BgL_v1344z00_2048; } 
{ /* Llib/object.scm 254 */
 obj_t BgL_v1345z00_2049;
BgL_v1345z00_2049 = 
create_vector(0L); 
BgL_arg1982z00_2032 = BgL_v1345z00_2049; } 
BGl_z62iozd2readzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3721z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2portzd2errorz62zz__objectz00, 15626L, BGl_proc3720z00zz__objectz00, BGl_proc3719z00zz__objectz00, BFALSE, BGl_proc3718z00zz__objectz00, BFALSE, BgL_arg1981z00_2031, BgL_arg1982z00_2032); } 
{ /* Llib/object.scm 255 */
 obj_t BgL_arg1991z00_2056; obj_t BgL_arg1992z00_2057;
{ /* Llib/object.scm 255 */
 obj_t BgL_v1346z00_2073;
BgL_v1346z00_2073 = 
create_vector(0L); 
BgL_arg1991z00_2056 = BgL_v1346z00_2073; } 
{ /* Llib/object.scm 255 */
 obj_t BgL_v1347z00_2074;
BgL_v1347z00_2074 = 
create_vector(0L); 
BgL_arg1992z00_2057 = BgL_v1347z00_2074; } 
BGl_z62iozd2writezd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3726z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2portzd2errorz62zz__objectz00, 35931L, BGl_proc3725z00zz__objectz00, BGl_proc3724z00zz__objectz00, BFALSE, BGl_proc3723z00zz__objectz00, BFALSE, BgL_arg1991z00_2056, BgL_arg1992z00_2057); } 
{ /* Llib/object.scm 256 */
 obj_t BgL_arg2001z00_2081; obj_t BgL_arg2002z00_2082;
{ /* Llib/object.scm 256 */
 obj_t BgL_v1348z00_2098;
BgL_v1348z00_2098 = 
create_vector(0L); 
BgL_arg2001z00_2081 = BgL_v1348z00_2098; } 
{ /* Llib/object.scm 256 */
 obj_t BgL_v1349z00_2099;
BgL_v1349z00_2099 = 
create_vector(0L); 
BgL_arg2002z00_2082 = BgL_v1349z00_2099; } 
BGl_z62iozd2closedzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3731z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2portzd2errorz62zz__objectz00, 8168L, BGl_proc3730z00zz__objectz00, BGl_proc3729z00zz__objectz00, BFALSE, BGl_proc3728z00zz__objectz00, BFALSE, BgL_arg2001z00_2081, BgL_arg2002z00_2082); } 
{ /* Llib/object.scm 257 */
 obj_t BgL_arg2011z00_2106; obj_t BgL_arg2012z00_2107;
{ /* Llib/object.scm 257 */
 obj_t BgL_v1350z00_2123;
BgL_v1350z00_2123 = 
create_vector(0L); 
BgL_arg2011z00_2106 = BgL_v1350z00_2123; } 
{ /* Llib/object.scm 257 */
 obj_t BgL_v1351z00_2124;
BgL_v1351z00_2124 = 
create_vector(0L); 
BgL_arg2012z00_2107 = BgL_v1351z00_2124; } 
BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3736z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 9101L, BGl_proc3735z00zz__objectz00, BGl_proc3734z00zz__objectz00, BFALSE, BGl_proc3733z00zz__objectz00, BFALSE, BgL_arg2011z00_2106, BgL_arg2012z00_2107); } 
{ /* Llib/object.scm 258 */
 obj_t BgL_arg2021z00_2131; obj_t BgL_arg2022z00_2132;
{ /* Llib/object.scm 258 */
 obj_t BgL_v1352z00_2148;
BgL_v1352z00_2148 = 
create_vector(0L); 
BgL_arg2021z00_2131 = BgL_v1352z00_2148; } 
{ /* Llib/object.scm 258 */
 obj_t BgL_v1353z00_2149;
BgL_v1353z00_2149 = 
create_vector(0L); 
BgL_arg2022z00_2132 = BgL_v1353z00_2149; } 
BGl_z62iozd2parsezd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3741z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 21913L, BGl_proc3740z00zz__objectz00, BGl_proc3739z00zz__objectz00, BFALSE, BGl_proc3738z00zz__objectz00, BFALSE, BgL_arg2021z00_2131, BgL_arg2022z00_2132); } 
{ /* Llib/object.scm 259 */
 obj_t BgL_arg2033z00_2156; obj_t BgL_arg2034z00_2157;
{ /* Llib/object.scm 259 */
 obj_t BgL_v1354z00_2173;
BgL_v1354z00_2173 = 
create_vector(0L); 
BgL_arg2033z00_2156 = BgL_v1354z00_2173; } 
{ /* Llib/object.scm 259 */
 obj_t BgL_v1355z00_2174;
BgL_v1355z00_2174 = 
create_vector(0L); 
BgL_arg2034z00_2157 = BgL_v1355z00_2174; } 
BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3746z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 40825L, BGl_proc3745z00zz__objectz00, BGl_proc3744z00zz__objectz00, BFALSE, BGl_proc3743z00zz__objectz00, BFALSE, BgL_arg2033z00_2156, BgL_arg2034z00_2157); } 
{ /* Llib/object.scm 260 */
 obj_t BgL_arg2044z00_2181; obj_t BgL_arg2045z00_2182;
{ /* Llib/object.scm 260 */
 obj_t BgL_v1356z00_2198;
BgL_v1356z00_2198 = 
create_vector(0L); 
BgL_arg2044z00_2181 = BgL_v1356z00_2198; } 
{ /* Llib/object.scm 260 */
 obj_t BgL_v1357z00_2199;
BgL_v1357z00_2199 = 
create_vector(0L); 
BgL_arg2045z00_2182 = BgL_v1357z00_2199; } 
BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3751z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 43421L, BGl_proc3750z00zz__objectz00, BGl_proc3749z00zz__objectz00, BFALSE, BGl_proc3748z00zz__objectz00, BFALSE, BgL_arg2044z00_2181, BgL_arg2045z00_2182); } 
{ /* Llib/object.scm 261 */
 obj_t BgL_arg2056z00_2206; obj_t BgL_arg2057z00_2207;
{ /* Llib/object.scm 261 */
 obj_t BgL_v1358z00_2223;
BgL_v1358z00_2223 = 
create_vector(0L); 
BgL_arg2056z00_2206 = BgL_v1358z00_2223; } 
{ /* Llib/object.scm 261 */
 obj_t BgL_v1359z00_2224;
BgL_v1359z00_2224 = 
create_vector(0L); 
BgL_arg2057z00_2207 = BgL_v1359z00_2224; } 
BGl_z62iozd2sigpipezd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3756z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 38807L, BGl_proc3755z00zz__objectz00, BGl_proc3754z00zz__objectz00, BFALSE, BGl_proc3753z00zz__objectz00, BFALSE, BgL_arg2056z00_2206, BgL_arg2057z00_2207); } 
{ /* Llib/object.scm 262 */
 obj_t BgL_arg2067z00_2231; obj_t BgL_arg2068z00_2232;
{ /* Llib/object.scm 262 */
 obj_t BgL_v1360z00_2248;
BgL_v1360z00_2248 = 
create_vector(0L); 
BgL_arg2067z00_2231 = BgL_v1360z00_2248; } 
{ /* Llib/object.scm 262 */
 obj_t BgL_v1361z00_2249;
BgL_v1361z00_2249 = 
create_vector(0L); 
BgL_arg2068z00_2232 = BgL_v1361z00_2249; } 
BGl_z62iozd2timeoutzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3761z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 13L, BGl_proc3760z00zz__objectz00, BGl_proc3759z00zz__objectz00, BFALSE, BGl_proc3758z00zz__objectz00, BFALSE, BgL_arg2067z00_2231, BgL_arg2068z00_2232); } 
{ /* Llib/object.scm 263 */
 obj_t BgL_arg2077z00_2256; obj_t BgL_arg2078z00_2257;
{ /* Llib/object.scm 263 */
 obj_t BgL_v1362z00_2273;
BgL_v1362z00_2273 = 
create_vector(0L); 
BgL_arg2077z00_2256 = BgL_v1362z00_2273; } 
{ /* Llib/object.scm 263 */
 obj_t BgL_v1363z00_2274;
BgL_v1363z00_2274 = 
create_vector(0L); 
BgL_arg2078z00_2257 = BgL_v1363z00_2274; } 
BGl_z62iozd2connectionzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3766z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62iozd2errorzb0zz__objectz00, 37044L, BGl_proc3765z00zz__objectz00, BGl_proc3764z00zz__objectz00, BFALSE, BGl_proc3763z00zz__objectz00, BFALSE, BgL_arg2077z00_2256, BgL_arg2078z00_2257); } 
{ /* Llib/object.scm 265 */
 obj_t BgL_arg2088z00_2281; obj_t BgL_arg2089z00_2282;
{ /* Llib/object.scm 265 */
 obj_t BgL_v1364z00_2298;
BgL_v1364z00_2298 = 
create_vector(0L); 
BgL_arg2088z00_2281 = BgL_v1364z00_2298; } 
{ /* Llib/object.scm 265 */
 obj_t BgL_v1365z00_2299;
BgL_v1365z00_2299 = 
create_vector(0L); 
BgL_arg2089z00_2282 = BgL_v1365z00_2299; } 
BGl_z62processzd2exceptionzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3771z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62errorz62zz__objectz00, 8734L, BGl_proc3770z00zz__objectz00, BGl_proc3769z00zz__objectz00, BFALSE, BGl_proc3768z00zz__objectz00, BFALSE, BgL_arg2088z00_2281, BgL_arg2089z00_2282); } 
{ /* Llib/object.scm 267 */
 obj_t BgL_arg2098z00_2306; obj_t BgL_arg2099z00_2307;
{ /* Llib/object.scm 267 */
 obj_t BgL_v1366z00_2323;
BgL_v1366z00_2323 = 
create_vector(0L); 
BgL_arg2098z00_2306 = BgL_v1366z00_2323; } 
{ /* Llib/object.scm 267 */
 obj_t BgL_v1367z00_2324;
BgL_v1367z00_2324 = 
create_vector(0L); 
BgL_arg2099z00_2307 = BgL_v1367z00_2324; } 
BGl_z62stackzd2overflowzd2errorz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3776z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62errorz62zz__objectz00, 16267L, BGl_proc3775z00zz__objectz00, BGl_proc3774z00zz__objectz00, BFALSE, BGl_proc3773z00zz__objectz00, BFALSE, BgL_arg2098z00_2306, BgL_arg2099z00_2307); } 
{ /* Llib/object.scm 269 */
 obj_t BgL_arg2108z00_2331; obj_t BgL_arg2109z00_2332;
{ /* Llib/object.scm 269 */
 obj_t BgL_v1368z00_2346;
BgL_v1368z00_2346 = 
create_vector(1L); 
{ /* Llib/object.scm 269 */
 obj_t BgL_arg2115z00_2347;
{ /* Llib/object.scm 269 */
 obj_t BgL_res2631z00_4132;
{ /* Llib/object.scm 269 */
 obj_t BgL_namez00_4117; obj_t BgL_typez00_4120;
BgL_namez00_4117 = BGl_symbol3781z00zz__objectz00; 
BgL_typez00_4120 = BGl_symbol3783z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_4121;
BgL_v1316z00_4121 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_4121,0L,BgL_namez00_4117); 
VECTOR_SET(BgL_v1316z00_4121,1L,BGl_proc3780z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4121,2L,BGl_proc3779z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4121,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4121,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4121,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4121,6L,BGl_proc3778z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4121,7L,BgL_typez00_4120); 
VECTOR_SET(BgL_v1316z00_4121,8L,BFALSE); 
BgL_res2631z00_4132 = BgL_v1316z00_4121; } } 
BgL_arg2115z00_2347 = BgL_res2631z00_4132; } 
VECTOR_SET(BgL_v1368z00_2346,0L,BgL_arg2115z00_2347); } 
BgL_arg2108z00_2331 = BgL_v1368z00_2346; } 
{ /* Llib/object.scm 269 */
 obj_t BgL_v1369z00_2360;
BgL_v1369z00_2360 = 
create_vector(0L); 
BgL_arg2109z00_2332 = BgL_v1369z00_2360; } 
BGl_z62securityzd2exceptionzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3788z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62exceptionz62zz__objectz00, 17339L, BGl_proc3787z00zz__objectz00, BGl_proc3786z00zz__objectz00, BFALSE, BGl_proc3785z00zz__objectz00, BFALSE, BgL_arg2108z00_2331, BgL_arg2109z00_2332); } 
{ /* Llib/object.scm 271 */
 obj_t BgL_arg2125z00_2367; obj_t BgL_arg2126z00_2368;
{ /* Llib/object.scm 271 */
 obj_t BgL_v1370z00_2384;
BgL_v1370z00_2384 = 
create_vector(2L); 
{ /* Llib/object.scm 271 */
 obj_t BgL_arg2133z00_2385;
{ /* Llib/object.scm 271 */
 obj_t BgL_res2632z00_4158;
{ /* Llib/object.scm 271 */
 obj_t BgL_namez00_4143; obj_t BgL_typez00_4146;
BgL_namez00_4143 = BGl_symbol3619z00zz__objectz00; 
BgL_typez00_4146 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_4147;
BgL_v1316z00_4147 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_4147,0L,BgL_namez00_4143); 
VECTOR_SET(BgL_v1316z00_4147,1L,BGl_proc3792z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4147,2L,BGl_proc3791z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4147,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4147,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4147,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4147,6L,BGl_proc3790z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4147,7L,BgL_typez00_4146); 
VECTOR_SET(BgL_v1316z00_4147,8L,BFALSE); 
BgL_res2632z00_4158 = BgL_v1316z00_4147; } } 
BgL_arg2133z00_2385 = BgL_res2632z00_4158; } 
VECTOR_SET(BgL_v1370z00_2384,0L,BgL_arg2133z00_2385); } 
{ /* Llib/object.scm 271 */
 obj_t BgL_arg2141z00_2398;
{ /* Llib/object.scm 271 */
 obj_t BgL_res2633z00_4175;
{ /* Llib/object.scm 271 */
 obj_t BgL_namez00_4160; obj_t BgL_typez00_4163;
BgL_namez00_4160 = BGl_symbol3796z00zz__objectz00; 
BgL_typez00_4163 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_4164;
BgL_v1316z00_4164 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_4164,0L,BgL_namez00_4160); 
VECTOR_SET(BgL_v1316z00_4164,1L,BGl_proc3795z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4164,2L,BGl_proc3794z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4164,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4164,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4164,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4164,6L,BGl_proc3793z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4164,7L,BgL_typez00_4163); 
VECTOR_SET(BgL_v1316z00_4164,8L,BFALSE); 
BgL_res2633z00_4175 = BgL_v1316z00_4164; } } 
BgL_arg2141z00_2398 = BgL_res2633z00_4175; } 
VECTOR_SET(BgL_v1370z00_2384,1L,BgL_arg2141z00_2398); } 
BgL_arg2125z00_2367 = BgL_v1370z00_2384; } 
{ /* Llib/object.scm 271 */
 obj_t BgL_v1371z00_2411;
BgL_v1371z00_2411 = 
create_vector(0L); 
BgL_arg2126z00_2368 = BgL_v1371z00_2411; } 
BGl_z62accesszd2controlzd2exceptionz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3801z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62securityzd2exceptionzb0zz__objectz00, 64045L, BGl_proc3800z00zz__objectz00, BGl_proc3799z00zz__objectz00, BFALSE, BGl_proc3798z00zz__objectz00, BFALSE, BgL_arg2125z00_2367, BgL_arg2126z00_2368); } 
{ /* Llib/object.scm 275 */
 obj_t BgL_arg2151z00_2418; obj_t BgL_arg2152z00_2419;
{ /* Llib/object.scm 275 */
 obj_t BgL_v1372z00_2433;
BgL_v1372z00_2433 = 
create_vector(1L); 
{ /* Llib/object.scm 275 */
 obj_t BgL_arg2158z00_2434;
{ /* Llib/object.scm 275 */
 obj_t BgL_res2634z00_4201;
{ /* Llib/object.scm 275 */
 obj_t BgL_namez00_4186; obj_t BgL_typez00_4189;
BgL_namez00_4186 = BGl_symbol3805z00zz__objectz00; 
BgL_typez00_4189 = BGl_symbol3619z00zz__objectz00; 
{ /* Llib/object.scm 591 */
 obj_t BgL_v1316z00_4190;
BgL_v1316z00_4190 = 
create_vector(9L); 
VECTOR_SET(BgL_v1316z00_4190,0L,BgL_namez00_4186); 
VECTOR_SET(BgL_v1316z00_4190,1L,BGl_proc3804z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4190,2L,BGl_proc3803z00zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4190,3L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4190,4L,BGl_makezd2classzd2fieldzd2envzd2zz__objectz00); 
VECTOR_SET(BgL_v1316z00_4190,5L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4190,6L,BFALSE); 
VECTOR_SET(BgL_v1316z00_4190,7L,BgL_typez00_4189); 
VECTOR_SET(BgL_v1316z00_4190,8L,BFALSE); 
BgL_res2634z00_4201 = BgL_v1316z00_4190; } } 
BgL_arg2158z00_2434 = BgL_res2634z00_4201; } 
VECTOR_SET(BgL_v1372z00_2433,0L,BgL_arg2158z00_2434); } 
BgL_arg2151z00_2418 = BgL_v1372z00_2433; } 
{ /* Llib/object.scm 275 */
 obj_t BgL_v1373z00_2444;
BgL_v1373z00_2444 = 
create_vector(0L); 
BgL_arg2152z00_2419 = BgL_v1373z00_2444; } 
BGl_z62warningz62zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3810z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62exceptionz62zz__objectz00, 33714L, BGl_proc3809z00zz__objectz00, BGl_proc3808z00zz__objectz00, BFALSE, BGl_proc3807z00zz__objectz00, BFALSE, BgL_arg2151z00_2418, BgL_arg2152z00_2419); } 
{ /* Llib/object.scm 277 */
 obj_t BgL_arg2166z00_2451; obj_t BgL_arg2167z00_2452;
{ /* Llib/object.scm 277 */
 obj_t BgL_v1374z00_2466;
BgL_v1374z00_2466 = 
create_vector(0L); 
BgL_arg2166z00_2451 = BgL_v1374z00_2466; } 
{ /* Llib/object.scm 277 */
 obj_t BgL_v1375z00_2467;
BgL_v1375z00_2467 = 
create_vector(0L); 
BgL_arg2167z00_2452 = BgL_v1375z00_2467; } 
return ( 
BGl_z62evalzd2warningzb0zz__objectz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol3815z00zz__objectz00, BGl_symbol3650z00zz__objectz00, BGl_z62warningz62zz__objectz00, 60850L, BGl_proc3814z00zz__objectz00, BGl_proc3813z00zz__objectz00, BFALSE, BGl_proc3812z00zz__objectz00, BFALSE, BgL_arg2166z00_2451, BgL_arg2167z00_2452), BUNSPEC) ;} } 

}



/* &<@anonymous:2172> */
obj_t BGl_z62zc3z04anonymousza32172ze3ze5zz__objectz00(obj_t BgL_envz00_5062, obj_t BgL_new1209z00_5063)
{
{ /* Llib/object.scm 277 */
{ 
 BgL_z62evalzd2warningzb0_bglt BgL_auxz00_11697;
{ /* Llib/object.scm 277 */
 BgL_z62evalzd2warningzb0_bglt BgL_new1209z00_6583;
{ /* Llib/object.scm 277 */
 bool_t BgL_test4917z00_11698;
{ /* Llib/object.scm 277 */
 obj_t BgL_classz00_6565;
BgL_classz00_6565 = BGl_z62evalzd2warningzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1209z00_5063))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6566; long BgL_arg1804z00_6567;
BgL_arg1803z00_6566 = 
(BgL_objectz00_bglt)(BgL_new1209z00_5063); 
BgL_arg1804z00_6567 = 
BGL_CLASS_DEPTH(BgL_classz00_6565); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6568;
BgL_idxz00_6568 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6566); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6569;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6570;
BgL_arg1812z00_6570 = 
(BgL_idxz00_6568+BgL_arg1804z00_6567); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6571;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6572;
BgL_aux3145z00_6572 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6572))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6571 = BgL_aux3145z00_6572; }  else 
{ 
 obj_t BgL_auxz00_11709;
BgL_auxz00_11709 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6572); 
FAILURE(BgL_auxz00_11709,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4921z00_11713;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_11714;
BgL_tmpz00_11714 = 
VECTOR_LENGTH(BgL_vectorz00_6571); 
BgL_test4921z00_11713 = 
BOUND_CHECK(BgL_arg1812z00_6570, BgL_tmpz00_11714); } 
if(BgL_test4921z00_11713)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6569 = 
VECTOR_REF(BgL_vectorz00_6571,BgL_arg1812z00_6570); }  else 
{ 
 obj_t BgL_auxz00_11718;
BgL_auxz00_11718 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6571, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6571)), 
(int)(BgL_arg1812z00_6570)); 
FAILURE(BgL_auxz00_11718,BFALSE,BFALSE);} } } } 
BgL_test4917z00_11698 = 
(BgL_arg1811z00_6569==BgL_classz00_6565); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6573;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6574;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6575; long BgL_arg1555z00_6576;
BgL_arg1554z00_6575 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6577;
BgL_arg1556z00_6577 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6566); 
BgL_arg1555z00_6576 = 
(BgL_arg1556z00_6577-OBJECT_TYPE); } 
BgL_oclassz00_6574 = 
VECTOR_REF(BgL_arg1554z00_6575,BgL_arg1555z00_6576); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6578;
BgL__ortest_1220z00_6578 = 
(BgL_classz00_6565==BgL_oclassz00_6574); 
if(BgL__ortest_1220z00_6578)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6573 = BgL__ortest_1220z00_6578; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6579;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6580;
BgL_arg1810z00_6580 = 
(BgL_oclassz00_6574); 
BgL_odepthz00_6579 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6580); } 
if(
(BgL_arg1804z00_6567<BgL_odepthz00_6579))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6581;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6582;
BgL_arg1809z00_6582 = 
(BgL_oclassz00_6574); 
BgL_arg1808z00_6581 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6582, BgL_arg1804z00_6567); } 
BgL_res2618z00_6573 = 
(BgL_arg1808z00_6581==BgL_classz00_6565); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6573 = ((bool_t)0); } } } } 
BgL_test4917z00_11698 = BgL_res2618z00_6573; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4917z00_11698 = ((bool_t)0)
; } } 
if(BgL_test4917z00_11698)
{ /* Llib/object.scm 277 */
BgL_new1209z00_6583 = 
((BgL_z62evalzd2warningzb0_bglt)BgL_new1209z00_5063); }  else 
{ 
 obj_t BgL_auxz00_11740;
BgL_auxz00_11740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9895L), BGl_string3817z00zz__objectz00, BGl_string3816z00zz__objectz00, BgL_new1209z00_5063); 
FAILURE(BgL_auxz00_11740,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1209z00_6583)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1209z00_6583)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1209z00_6583)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_new1209z00_6583)))->BgL_argsz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_11697 = BgL_new1209z00_6583; } 
return 
((obj_t)BgL_auxz00_11697);} } 

}



/* &lambda2170 */
BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2170z62zz__objectz00(obj_t BgL_envz00_5064)
{
{ /* Llib/object.scm 277 */
{ /* Llib/object.scm 277 */
 BgL_z62evalzd2warningzb0_bglt BgL_new1208z00_6584;
BgL_new1208z00_6584 = 
((BgL_z62evalzd2warningzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62evalzd2warningzb0_bgl) ))); 
{ /* Llib/object.scm 277 */
 long BgL_arg2171z00_6585;
BgL_arg2171z00_6585 = 
BGL_CLASS_NUM(BGl_z62evalzd2warningzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1208z00_6584), BgL_arg2171z00_6585); } 
return BgL_new1208z00_6584;} } 

}



/* &lambda2168 */
BgL_z62evalzd2warningzb0_bglt BGl_z62lambda2168z62zz__objectz00(obj_t BgL_envz00_5065, obj_t BgL_fname1204z00_5066, obj_t BgL_location1205z00_5067, obj_t BgL_stack1206z00_5068, obj_t BgL_args1207z00_5069)
{
{ /* Llib/object.scm 277 */
{ /* Llib/object.scm 277 */
 BgL_z62evalzd2warningzb0_bglt BgL_new1277z00_6586;
{ /* Llib/object.scm 277 */
 BgL_z62evalzd2warningzb0_bglt BgL_new1276z00_6587;
BgL_new1276z00_6587 = 
((BgL_z62evalzd2warningzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62evalzd2warningzb0_bgl) ))); 
{ /* Llib/object.scm 277 */
 long BgL_arg2169z00_6588;
BgL_arg2169z00_6588 = 
BGL_CLASS_NUM(BGl_z62evalzd2warningzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1276z00_6587), BgL_arg2169z00_6588); } 
BgL_new1277z00_6586 = BgL_new1276z00_6587; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1277z00_6586)))->BgL_fnamez00)=((obj_t)BgL_fname1204z00_5066),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1277z00_6586)))->BgL_locationz00)=((obj_t)BgL_location1205z00_5067),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1277z00_6586)))->BgL_stackz00)=((obj_t)BgL_stack1206z00_5068),BUNSPEC); 
((((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_new1277z00_6586)))->BgL_argsz00)=((obj_t)BgL_args1207z00_5069),BUNSPEC); 
return BgL_new1277z00_6586;} } 

}



/* &<@anonymous:2157> */
obj_t BGl_z62zc3z04anonymousza32157ze3ze5zz__objectz00(obj_t BgL_envz00_5070, obj_t BgL_new1202z00_5071)
{
{ /* Llib/object.scm 275 */
{ 
 BgL_z62warningz62_bglt BgL_auxz00_11769;
{ /* Llib/object.scm 275 */
 BgL_z62warningz62_bglt BgL_new1202z00_6607;
{ /* Llib/object.scm 275 */
 bool_t BgL_test4924z00_11770;
{ /* Llib/object.scm 275 */
 obj_t BgL_classz00_6589;
BgL_classz00_6589 = BGl_z62warningz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1202z00_5071))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6590; long BgL_arg1804z00_6591;
BgL_arg1803z00_6590 = 
(BgL_objectz00_bglt)(BgL_new1202z00_5071); 
BgL_arg1804z00_6591 = 
BGL_CLASS_DEPTH(BgL_classz00_6589); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6592;
BgL_idxz00_6592 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6590); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6593;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6594;
BgL_arg1812z00_6594 = 
(BgL_idxz00_6592+BgL_arg1804z00_6591); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6595;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6596;
BgL_aux3145z00_6596 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6596))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6595 = BgL_aux3145z00_6596; }  else 
{ 
 obj_t BgL_auxz00_11781;
BgL_auxz00_11781 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6596); 
FAILURE(BgL_auxz00_11781,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4928z00_11785;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_11786;
BgL_tmpz00_11786 = 
VECTOR_LENGTH(BgL_vectorz00_6595); 
BgL_test4928z00_11785 = 
BOUND_CHECK(BgL_arg1812z00_6594, BgL_tmpz00_11786); } 
if(BgL_test4928z00_11785)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6593 = 
VECTOR_REF(BgL_vectorz00_6595,BgL_arg1812z00_6594); }  else 
{ 
 obj_t BgL_auxz00_11790;
BgL_auxz00_11790 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6595, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6595)), 
(int)(BgL_arg1812z00_6594)); 
FAILURE(BgL_auxz00_11790,BFALSE,BFALSE);} } } } 
BgL_test4924z00_11770 = 
(BgL_arg1811z00_6593==BgL_classz00_6589); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6597;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6598;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6599; long BgL_arg1555z00_6600;
BgL_arg1554z00_6599 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6601;
BgL_arg1556z00_6601 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6590); 
BgL_arg1555z00_6600 = 
(BgL_arg1556z00_6601-OBJECT_TYPE); } 
BgL_oclassz00_6598 = 
VECTOR_REF(BgL_arg1554z00_6599,BgL_arg1555z00_6600); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6602;
BgL__ortest_1220z00_6602 = 
(BgL_classz00_6589==BgL_oclassz00_6598); 
if(BgL__ortest_1220z00_6602)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6597 = BgL__ortest_1220z00_6602; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6603;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6604;
BgL_arg1810z00_6604 = 
(BgL_oclassz00_6598); 
BgL_odepthz00_6603 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6604); } 
if(
(BgL_arg1804z00_6591<BgL_odepthz00_6603))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6605;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6606;
BgL_arg1809z00_6606 = 
(BgL_oclassz00_6598); 
BgL_arg1808z00_6605 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6606, BgL_arg1804z00_6591); } 
BgL_res2618z00_6597 = 
(BgL_arg1808z00_6605==BgL_classz00_6589); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6597 = ((bool_t)0); } } } } 
BgL_test4924z00_11770 = BgL_res2618z00_6597; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4924z00_11770 = ((bool_t)0)
; } } 
if(BgL_test4924z00_11770)
{ /* Llib/object.scm 275 */
BgL_new1202z00_6607 = 
((BgL_z62warningz62_bglt)BgL_new1202z00_5071); }  else 
{ 
 obj_t BgL_auxz00_11812;
BgL_auxz00_11812 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9836L), BGl_string3818z00zz__objectz00, BGl_string3811z00zz__objectz00, BgL_new1202z00_5071); 
FAILURE(BgL_auxz00_11812,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1202z00_6607)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1202z00_6607)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1202z00_6607)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62warningz62_bglt)COBJECT(BgL_new1202z00_6607))->BgL_argsz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_11769 = BgL_new1202z00_6607; } 
return 
((obj_t)BgL_auxz00_11769);} } 

}



/* &lambda2155 */
BgL_z62warningz62_bglt BGl_z62lambda2155z62zz__objectz00(obj_t BgL_envz00_5072)
{
{ /* Llib/object.scm 275 */
{ /* Llib/object.scm 275 */
 BgL_z62warningz62_bglt BgL_new1201z00_6608;
BgL_new1201z00_6608 = 
((BgL_z62warningz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62warningz62_bgl) ))); 
{ /* Llib/object.scm 275 */
 long BgL_arg2156z00_6609;
BgL_arg2156z00_6609 = 
BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1201z00_6608), BgL_arg2156z00_6609); } 
return BgL_new1201z00_6608;} } 

}



/* &lambda2153 */
BgL_z62warningz62_bglt BGl_z62lambda2153z62zz__objectz00(obj_t BgL_envz00_5073, obj_t BgL_fname1197z00_5074, obj_t BgL_location1198z00_5075, obj_t BgL_stack1199z00_5076, obj_t BgL_args1200z00_5077)
{
{ /* Llib/object.scm 275 */
{ /* Llib/object.scm 275 */
 BgL_z62warningz62_bglt BgL_new1275z00_6610;
{ /* Llib/object.scm 275 */
 BgL_z62warningz62_bglt BgL_new1274z00_6611;
BgL_new1274z00_6611 = 
((BgL_z62warningz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62warningz62_bgl) ))); 
{ /* Llib/object.scm 275 */
 long BgL_arg2154z00_6612;
BgL_arg2154z00_6612 = 
BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1274z00_6611), BgL_arg2154z00_6612); } 
BgL_new1275z00_6610 = BgL_new1274z00_6611; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1275z00_6610)))->BgL_fnamez00)=((obj_t)BgL_fname1197z00_5074),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1275z00_6610)))->BgL_locationz00)=((obj_t)BgL_location1198z00_5075),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1275z00_6610)))->BgL_stackz00)=((obj_t)BgL_stack1199z00_5076),BUNSPEC); 
((((BgL_z62warningz62_bglt)COBJECT(BgL_new1275z00_6610))->BgL_argsz00)=((obj_t)BgL_args1200z00_5077),BUNSPEC); 
return BgL_new1275z00_6610;} } 

}



/* &lambda2162 */
obj_t BGl_z62lambda2162z62zz__objectz00(obj_t BgL_envz00_5078, obj_t BgL_oz00_5079, obj_t BgL_vz00_5080)
{
{ /* Llib/object.scm 275 */
{ /* Llib/object.scm 275 */
 BgL_z62warningz62_bglt BgL_oz00_6631;
{ /* Llib/object.scm 275 */
 bool_t BgL_test4931z00_11839;
{ /* Llib/object.scm 275 */
 obj_t BgL_classz00_6613;
BgL_classz00_6613 = BGl_z62warningz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5079))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6614; long BgL_arg1804z00_6615;
BgL_arg1803z00_6614 = 
(BgL_objectz00_bglt)(BgL_oz00_5079); 
BgL_arg1804z00_6615 = 
BGL_CLASS_DEPTH(BgL_classz00_6613); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6616;
BgL_idxz00_6616 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6614); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6617;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6618;
BgL_arg1812z00_6618 = 
(BgL_idxz00_6616+BgL_arg1804z00_6615); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6619;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6620;
BgL_aux3145z00_6620 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6620))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6619 = BgL_aux3145z00_6620; }  else 
{ 
 obj_t BgL_auxz00_11850;
BgL_auxz00_11850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6620); 
FAILURE(BgL_auxz00_11850,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4935z00_11854;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_11855;
BgL_tmpz00_11855 = 
VECTOR_LENGTH(BgL_vectorz00_6619); 
BgL_test4935z00_11854 = 
BOUND_CHECK(BgL_arg1812z00_6618, BgL_tmpz00_11855); } 
if(BgL_test4935z00_11854)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6617 = 
VECTOR_REF(BgL_vectorz00_6619,BgL_arg1812z00_6618); }  else 
{ 
 obj_t BgL_auxz00_11859;
BgL_auxz00_11859 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6619, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6619)), 
(int)(BgL_arg1812z00_6618)); 
FAILURE(BgL_auxz00_11859,BFALSE,BFALSE);} } } } 
BgL_test4931z00_11839 = 
(BgL_arg1811z00_6617==BgL_classz00_6613); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6621;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6622;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6623; long BgL_arg1555z00_6624;
BgL_arg1554z00_6623 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6625;
BgL_arg1556z00_6625 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6614); 
BgL_arg1555z00_6624 = 
(BgL_arg1556z00_6625-OBJECT_TYPE); } 
BgL_oclassz00_6622 = 
VECTOR_REF(BgL_arg1554z00_6623,BgL_arg1555z00_6624); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6626;
BgL__ortest_1220z00_6626 = 
(BgL_classz00_6613==BgL_oclassz00_6622); 
if(BgL__ortest_1220z00_6626)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6621 = BgL__ortest_1220z00_6626; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6627;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6628;
BgL_arg1810z00_6628 = 
(BgL_oclassz00_6622); 
BgL_odepthz00_6627 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6628); } 
if(
(BgL_arg1804z00_6615<BgL_odepthz00_6627))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6629;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6630;
BgL_arg1809z00_6630 = 
(BgL_oclassz00_6622); 
BgL_arg1808z00_6629 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6630, BgL_arg1804z00_6615); } 
BgL_res2618z00_6621 = 
(BgL_arg1808z00_6629==BgL_classz00_6613); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6621 = ((bool_t)0); } } } } 
BgL_test4931z00_11839 = BgL_res2618z00_6621; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4931z00_11839 = ((bool_t)0)
; } } 
if(BgL_test4931z00_11839)
{ /* Llib/object.scm 275 */
BgL_oz00_6631 = 
((BgL_z62warningz62_bglt)BgL_oz00_5079); }  else 
{ 
 obj_t BgL_auxz00_11881;
BgL_auxz00_11881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9836L), BGl_string3819z00zz__objectz00, BGl_string3811z00zz__objectz00, BgL_oz00_5079); 
FAILURE(BgL_auxz00_11881,BFALSE,BFALSE);} } 
return 
((((BgL_z62warningz62_bglt)COBJECT(BgL_oz00_6631))->BgL_argsz00)=((obj_t)BgL_vz00_5080),BUNSPEC);} } 

}



/* &lambda2161 */
obj_t BGl_z62lambda2161z62zz__objectz00(obj_t BgL_envz00_5081, obj_t BgL_oz00_5082)
{
{ /* Llib/object.scm 275 */
{ /* Llib/object.scm 275 */
 BgL_z62warningz62_bglt BgL_oz00_6650;
{ /* Llib/object.scm 275 */
 bool_t BgL_test4938z00_11886;
{ /* Llib/object.scm 275 */
 obj_t BgL_classz00_6632;
BgL_classz00_6632 = BGl_z62warningz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5082))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6633; long BgL_arg1804z00_6634;
BgL_arg1803z00_6633 = 
(BgL_objectz00_bglt)(BgL_oz00_5082); 
BgL_arg1804z00_6634 = 
BGL_CLASS_DEPTH(BgL_classz00_6632); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6635;
BgL_idxz00_6635 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6633); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6636;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6637;
BgL_arg1812z00_6637 = 
(BgL_idxz00_6635+BgL_arg1804z00_6634); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6638;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6639;
BgL_aux3145z00_6639 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6639))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6638 = BgL_aux3145z00_6639; }  else 
{ 
 obj_t BgL_auxz00_11897;
BgL_auxz00_11897 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6639); 
FAILURE(BgL_auxz00_11897,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4942z00_11901;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_11902;
BgL_tmpz00_11902 = 
VECTOR_LENGTH(BgL_vectorz00_6638); 
BgL_test4942z00_11901 = 
BOUND_CHECK(BgL_arg1812z00_6637, BgL_tmpz00_11902); } 
if(BgL_test4942z00_11901)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6636 = 
VECTOR_REF(BgL_vectorz00_6638,BgL_arg1812z00_6637); }  else 
{ 
 obj_t BgL_auxz00_11906;
BgL_auxz00_11906 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6638, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6638)), 
(int)(BgL_arg1812z00_6637)); 
FAILURE(BgL_auxz00_11906,BFALSE,BFALSE);} } } } 
BgL_test4938z00_11886 = 
(BgL_arg1811z00_6636==BgL_classz00_6632); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6640;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6641;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6642; long BgL_arg1555z00_6643;
BgL_arg1554z00_6642 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6644;
BgL_arg1556z00_6644 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6633); 
BgL_arg1555z00_6643 = 
(BgL_arg1556z00_6644-OBJECT_TYPE); } 
BgL_oclassz00_6641 = 
VECTOR_REF(BgL_arg1554z00_6642,BgL_arg1555z00_6643); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6645;
BgL__ortest_1220z00_6645 = 
(BgL_classz00_6632==BgL_oclassz00_6641); 
if(BgL__ortest_1220z00_6645)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6640 = BgL__ortest_1220z00_6645; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6646;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6647;
BgL_arg1810z00_6647 = 
(BgL_oclassz00_6641); 
BgL_odepthz00_6646 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6647); } 
if(
(BgL_arg1804z00_6634<BgL_odepthz00_6646))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6648;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6649;
BgL_arg1809z00_6649 = 
(BgL_oclassz00_6641); 
BgL_arg1808z00_6648 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6649, BgL_arg1804z00_6634); } 
BgL_res2618z00_6640 = 
(BgL_arg1808z00_6648==BgL_classz00_6632); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6640 = ((bool_t)0); } } } } 
BgL_test4938z00_11886 = BgL_res2618z00_6640; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4938z00_11886 = ((bool_t)0)
; } } 
if(BgL_test4938z00_11886)
{ /* Llib/object.scm 275 */
BgL_oz00_6650 = 
((BgL_z62warningz62_bglt)BgL_oz00_5082); }  else 
{ 
 obj_t BgL_auxz00_11928;
BgL_auxz00_11928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9836L), BGl_string3820z00zz__objectz00, BGl_string3811z00zz__objectz00, BgL_oz00_5082); 
FAILURE(BgL_auxz00_11928,BFALSE,BFALSE);} } 
return 
(((BgL_z62warningz62_bglt)COBJECT(BgL_oz00_6650))->BgL_argsz00);} } 

}



/* &<@anonymous:2132> */
obj_t BGl_z62zc3z04anonymousza32132ze3ze5zz__objectz00(obj_t BgL_envz00_5083, obj_t BgL_new1195z00_5084)
{
{ /* Llib/object.scm 271 */
{ 
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_auxz00_11933;
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1195z00_6669;
{ /* Llib/object.scm 271 */
 bool_t BgL_test4945z00_11934;
{ /* Llib/object.scm 271 */
 obj_t BgL_classz00_6651;
BgL_classz00_6651 = BGl_z62accesszd2controlzd2exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1195z00_5084))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6652; long BgL_arg1804z00_6653;
BgL_arg1803z00_6652 = 
(BgL_objectz00_bglt)(BgL_new1195z00_5084); 
BgL_arg1804z00_6653 = 
BGL_CLASS_DEPTH(BgL_classz00_6651); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6654;
BgL_idxz00_6654 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6652); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6655;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6656;
BgL_arg1812z00_6656 = 
(BgL_idxz00_6654+BgL_arg1804z00_6653); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6657;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6658;
BgL_aux3145z00_6658 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6658))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6657 = BgL_aux3145z00_6658; }  else 
{ 
 obj_t BgL_auxz00_11945;
BgL_auxz00_11945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6658); 
FAILURE(BgL_auxz00_11945,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4949z00_11949;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_11950;
BgL_tmpz00_11950 = 
VECTOR_LENGTH(BgL_vectorz00_6657); 
BgL_test4949z00_11949 = 
BOUND_CHECK(BgL_arg1812z00_6656, BgL_tmpz00_11950); } 
if(BgL_test4949z00_11949)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6655 = 
VECTOR_REF(BgL_vectorz00_6657,BgL_arg1812z00_6656); }  else 
{ 
 obj_t BgL_auxz00_11954;
BgL_auxz00_11954 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6657, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6657)), 
(int)(BgL_arg1812z00_6656)); 
FAILURE(BgL_auxz00_11954,BFALSE,BFALSE);} } } } 
BgL_test4945z00_11934 = 
(BgL_arg1811z00_6655==BgL_classz00_6651); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6659;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6660;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6661; long BgL_arg1555z00_6662;
BgL_arg1554z00_6661 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6663;
BgL_arg1556z00_6663 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6652); 
BgL_arg1555z00_6662 = 
(BgL_arg1556z00_6663-OBJECT_TYPE); } 
BgL_oclassz00_6660 = 
VECTOR_REF(BgL_arg1554z00_6661,BgL_arg1555z00_6662); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6664;
BgL__ortest_1220z00_6664 = 
(BgL_classz00_6651==BgL_oclassz00_6660); 
if(BgL__ortest_1220z00_6664)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6659 = BgL__ortest_1220z00_6664; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6665;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6666;
BgL_arg1810z00_6666 = 
(BgL_oclassz00_6660); 
BgL_odepthz00_6665 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6666); } 
if(
(BgL_arg1804z00_6653<BgL_odepthz00_6665))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6667;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6668;
BgL_arg1809z00_6668 = 
(BgL_oclassz00_6660); 
BgL_arg1808z00_6667 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6668, BgL_arg1804z00_6653); } 
BgL_res2618z00_6659 = 
(BgL_arg1808z00_6667==BgL_classz00_6651); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6659 = ((bool_t)0); } } } } 
BgL_test4945z00_11934 = BgL_res2618z00_6659; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4945z00_11934 = ((bool_t)0)
; } } 
if(BgL_test4945z00_11934)
{ /* Llib/object.scm 271 */
BgL_new1195z00_6669 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BgL_new1195z00_5084); }  else 
{ 
 obj_t BgL_auxz00_11976;
BgL_auxz00_11976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9659L), BGl_string3821z00zz__objectz00, BGl_string3802z00zz__objectz00, BgL_new1195z00_5084); 
FAILURE(BgL_auxz00_11976,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1195z00_6669)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1195z00_6669)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1195z00_6669)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62securityzd2exceptionzb0_bglt)COBJECT(
((BgL_z62securityzd2exceptionzb0_bglt)BgL_new1195z00_6669)))->BgL_messagez00)=((obj_t)BGl_string3576z00zz__objectz00),BUNSPEC); 
((((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_new1195z00_6669))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_new1195z00_6669))->BgL_permissionz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_11933 = BgL_new1195z00_6669; } 
return 
((obj_t)BgL_auxz00_11933);} } 

}



/* &lambda2130 */
BgL_z62accesszd2controlzd2exceptionz62_bglt BGl_z62lambda2130z62zz__objectz00(obj_t BgL_envz00_5085)
{
{ /* Llib/object.scm 271 */
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1194z00_6670;
BgL_new1194z00_6670 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62accesszd2controlzd2exceptionz62_bgl) ))); 
{ /* Llib/object.scm 271 */
 long BgL_arg2131z00_6671;
BgL_arg2131z00_6671 = 
BGL_CLASS_NUM(BGl_z62accesszd2controlzd2exceptionz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1194z00_6670), BgL_arg2131z00_6671); } 
return BgL_new1194z00_6670;} } 

}



/* &lambda2127 */
BgL_z62accesszd2controlzd2exceptionz62_bglt BGl_z62lambda2127z62zz__objectz00(obj_t BgL_envz00_5086, obj_t BgL_fname1188z00_5087, obj_t BgL_location1189z00_5088, obj_t BgL_stack1190z00_5089, obj_t BgL_message1191z00_5090, obj_t BgL_obj1192z00_5091, obj_t BgL_permission1193z00_5092)
{
{ /* Llib/object.scm 271 */
{ /* Llib/object.scm 271 */
 obj_t BgL_message1191z00_6672;
if(
STRINGP(BgL_message1191z00_5090))
{ /* Llib/object.scm 271 */
BgL_message1191z00_6672 = BgL_message1191z00_5090; }  else 
{ 
 obj_t BgL_auxz00_11997;
BgL_auxz00_11997 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9659L), BGl_string3822z00zz__objectz00, BGl_string3784z00zz__objectz00, BgL_message1191z00_5090); 
FAILURE(BgL_auxz00_11997,BFALSE,BFALSE);} 
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1273z00_6673;
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_new1272z00_6674;
BgL_new1272z00_6674 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62accesszd2controlzd2exceptionz62_bgl) ))); 
{ /* Llib/object.scm 271 */
 long BgL_arg2129z00_6675;
BgL_arg2129z00_6675 = 
BGL_CLASS_NUM(BGl_z62accesszd2controlzd2exceptionz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1272z00_6674), BgL_arg2129z00_6675); } 
BgL_new1273z00_6673 = BgL_new1272z00_6674; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1273z00_6673)))->BgL_fnamez00)=((obj_t)BgL_fname1188z00_5087),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1273z00_6673)))->BgL_locationz00)=((obj_t)BgL_location1189z00_5088),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1273z00_6673)))->BgL_stackz00)=((obj_t)BgL_stack1190z00_5089),BUNSPEC); 
((((BgL_z62securityzd2exceptionzb0_bglt)COBJECT(
((BgL_z62securityzd2exceptionzb0_bglt)BgL_new1273z00_6673)))->BgL_messagez00)=((obj_t)BgL_message1191z00_6672),BUNSPEC); 
((((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_new1273z00_6673))->BgL_objz00)=((obj_t)BgL_obj1192z00_5091),BUNSPEC); 
((((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_new1273z00_6673))->BgL_permissionz00)=((obj_t)BgL_permission1193z00_5092),BUNSPEC); 
return BgL_new1273z00_6673;} } } 

}



/* &<@anonymous:2147> */
obj_t BGl_z62zc3z04anonymousza32147ze3ze5zz__objectz00(obj_t BgL_envz00_5093)
{
{ /* Llib/object.scm 271 */
return BUNSPEC;} 

}



/* &lambda2146 */
obj_t BGl_z62lambda2146z62zz__objectz00(obj_t BgL_envz00_5094, obj_t BgL_oz00_5095, obj_t BgL_vz00_5096)
{
{ /* Llib/object.scm 271 */
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_oz00_6694;
{ /* Llib/object.scm 271 */
 bool_t BgL_test4953z00_12015;
{ /* Llib/object.scm 271 */
 obj_t BgL_classz00_6676;
BgL_classz00_6676 = BGl_z62accesszd2controlzd2exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5095))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6677; long BgL_arg1804z00_6678;
BgL_arg1803z00_6677 = 
(BgL_objectz00_bglt)(BgL_oz00_5095); 
BgL_arg1804z00_6678 = 
BGL_CLASS_DEPTH(BgL_classz00_6676); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6679;
BgL_idxz00_6679 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6677); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6680;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6681;
BgL_arg1812z00_6681 = 
(BgL_idxz00_6679+BgL_arg1804z00_6678); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6682;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6683;
BgL_aux3145z00_6683 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6683))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6682 = BgL_aux3145z00_6683; }  else 
{ 
 obj_t BgL_auxz00_12026;
BgL_auxz00_12026 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6683); 
FAILURE(BgL_auxz00_12026,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4957z00_12030;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12031;
BgL_tmpz00_12031 = 
VECTOR_LENGTH(BgL_vectorz00_6682); 
BgL_test4957z00_12030 = 
BOUND_CHECK(BgL_arg1812z00_6681, BgL_tmpz00_12031); } 
if(BgL_test4957z00_12030)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6680 = 
VECTOR_REF(BgL_vectorz00_6682,BgL_arg1812z00_6681); }  else 
{ 
 obj_t BgL_auxz00_12035;
BgL_auxz00_12035 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6682, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6682)), 
(int)(BgL_arg1812z00_6681)); 
FAILURE(BgL_auxz00_12035,BFALSE,BFALSE);} } } } 
BgL_test4953z00_12015 = 
(BgL_arg1811z00_6680==BgL_classz00_6676); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6684;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6685;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6686; long BgL_arg1555z00_6687;
BgL_arg1554z00_6686 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6688;
BgL_arg1556z00_6688 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6677); 
BgL_arg1555z00_6687 = 
(BgL_arg1556z00_6688-OBJECT_TYPE); } 
BgL_oclassz00_6685 = 
VECTOR_REF(BgL_arg1554z00_6686,BgL_arg1555z00_6687); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6689;
BgL__ortest_1220z00_6689 = 
(BgL_classz00_6676==BgL_oclassz00_6685); 
if(BgL__ortest_1220z00_6689)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6684 = BgL__ortest_1220z00_6689; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6690;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6691;
BgL_arg1810z00_6691 = 
(BgL_oclassz00_6685); 
BgL_odepthz00_6690 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6691); } 
if(
(BgL_arg1804z00_6678<BgL_odepthz00_6690))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6692;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6693;
BgL_arg1809z00_6693 = 
(BgL_oclassz00_6685); 
BgL_arg1808z00_6692 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6693, BgL_arg1804z00_6678); } 
BgL_res2618z00_6684 = 
(BgL_arg1808z00_6692==BgL_classz00_6676); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6684 = ((bool_t)0); } } } } 
BgL_test4953z00_12015 = BgL_res2618z00_6684; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4953z00_12015 = ((bool_t)0)
; } } 
if(BgL_test4953z00_12015)
{ /* Llib/object.scm 271 */
BgL_oz00_6694 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BgL_oz00_5095); }  else 
{ 
 obj_t BgL_auxz00_12057;
BgL_auxz00_12057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9659L), BGl_string3823z00zz__objectz00, BGl_string3802z00zz__objectz00, BgL_oz00_5095); 
FAILURE(BgL_auxz00_12057,BFALSE,BFALSE);} } 
return 
((((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_oz00_6694))->BgL_permissionz00)=((obj_t)BgL_vz00_5096),BUNSPEC);} } 

}



/* &lambda2145 */
obj_t BGl_z62lambda2145z62zz__objectz00(obj_t BgL_envz00_5097, obj_t BgL_oz00_5098)
{
{ /* Llib/object.scm 271 */
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_oz00_6713;
{ /* Llib/object.scm 271 */
 bool_t BgL_test4960z00_12062;
{ /* Llib/object.scm 271 */
 obj_t BgL_classz00_6695;
BgL_classz00_6695 = BGl_z62accesszd2controlzd2exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5098))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6696; long BgL_arg1804z00_6697;
BgL_arg1803z00_6696 = 
(BgL_objectz00_bglt)(BgL_oz00_5098); 
BgL_arg1804z00_6697 = 
BGL_CLASS_DEPTH(BgL_classz00_6695); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6698;
BgL_idxz00_6698 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6696); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6699;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6700;
BgL_arg1812z00_6700 = 
(BgL_idxz00_6698+BgL_arg1804z00_6697); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6701;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6702;
BgL_aux3145z00_6702 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6702))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6701 = BgL_aux3145z00_6702; }  else 
{ 
 obj_t BgL_auxz00_12073;
BgL_auxz00_12073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6702); 
FAILURE(BgL_auxz00_12073,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4964z00_12077;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12078;
BgL_tmpz00_12078 = 
VECTOR_LENGTH(BgL_vectorz00_6701); 
BgL_test4964z00_12077 = 
BOUND_CHECK(BgL_arg1812z00_6700, BgL_tmpz00_12078); } 
if(BgL_test4964z00_12077)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6699 = 
VECTOR_REF(BgL_vectorz00_6701,BgL_arg1812z00_6700); }  else 
{ 
 obj_t BgL_auxz00_12082;
BgL_auxz00_12082 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6701, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6701)), 
(int)(BgL_arg1812z00_6700)); 
FAILURE(BgL_auxz00_12082,BFALSE,BFALSE);} } } } 
BgL_test4960z00_12062 = 
(BgL_arg1811z00_6699==BgL_classz00_6695); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6703;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6704;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6705; long BgL_arg1555z00_6706;
BgL_arg1554z00_6705 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6707;
BgL_arg1556z00_6707 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6696); 
BgL_arg1555z00_6706 = 
(BgL_arg1556z00_6707-OBJECT_TYPE); } 
BgL_oclassz00_6704 = 
VECTOR_REF(BgL_arg1554z00_6705,BgL_arg1555z00_6706); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6708;
BgL__ortest_1220z00_6708 = 
(BgL_classz00_6695==BgL_oclassz00_6704); 
if(BgL__ortest_1220z00_6708)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6703 = BgL__ortest_1220z00_6708; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6709;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6710;
BgL_arg1810z00_6710 = 
(BgL_oclassz00_6704); 
BgL_odepthz00_6709 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6710); } 
if(
(BgL_arg1804z00_6697<BgL_odepthz00_6709))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6711;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6712;
BgL_arg1809z00_6712 = 
(BgL_oclassz00_6704); 
BgL_arg1808z00_6711 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6712, BgL_arg1804z00_6697); } 
BgL_res2618z00_6703 = 
(BgL_arg1808z00_6711==BgL_classz00_6695); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6703 = ((bool_t)0); } } } } 
BgL_test4960z00_12062 = BgL_res2618z00_6703; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4960z00_12062 = ((bool_t)0)
; } } 
if(BgL_test4960z00_12062)
{ /* Llib/object.scm 271 */
BgL_oz00_6713 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BgL_oz00_5098); }  else 
{ 
 obj_t BgL_auxz00_12104;
BgL_auxz00_12104 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9659L), BGl_string3824z00zz__objectz00, BGl_string3802z00zz__objectz00, BgL_oz00_5098); 
FAILURE(BgL_auxz00_12104,BFALSE,BFALSE);} } 
return 
(((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_oz00_6713))->BgL_permissionz00);} } 

}



/* &<@anonymous:2139> */
obj_t BGl_z62zc3z04anonymousza32139ze3ze5zz__objectz00(obj_t BgL_envz00_5099)
{
{ /* Llib/object.scm 271 */
return BUNSPEC;} 

}



/* &lambda2138 */
obj_t BGl_z62lambda2138z62zz__objectz00(obj_t BgL_envz00_5100, obj_t BgL_oz00_5101, obj_t BgL_vz00_5102)
{
{ /* Llib/object.scm 271 */
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_oz00_6732;
{ /* Llib/object.scm 271 */
 bool_t BgL_test4967z00_12109;
{ /* Llib/object.scm 271 */
 obj_t BgL_classz00_6714;
BgL_classz00_6714 = BGl_z62accesszd2controlzd2exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5101))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6715; long BgL_arg1804z00_6716;
BgL_arg1803z00_6715 = 
(BgL_objectz00_bglt)(BgL_oz00_5101); 
BgL_arg1804z00_6716 = 
BGL_CLASS_DEPTH(BgL_classz00_6714); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6717;
BgL_idxz00_6717 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6715); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6718;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6719;
BgL_arg1812z00_6719 = 
(BgL_idxz00_6717+BgL_arg1804z00_6716); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6720;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6721;
BgL_aux3145z00_6721 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6721))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6720 = BgL_aux3145z00_6721; }  else 
{ 
 obj_t BgL_auxz00_12120;
BgL_auxz00_12120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6721); 
FAILURE(BgL_auxz00_12120,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4971z00_12124;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12125;
BgL_tmpz00_12125 = 
VECTOR_LENGTH(BgL_vectorz00_6720); 
BgL_test4971z00_12124 = 
BOUND_CHECK(BgL_arg1812z00_6719, BgL_tmpz00_12125); } 
if(BgL_test4971z00_12124)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6718 = 
VECTOR_REF(BgL_vectorz00_6720,BgL_arg1812z00_6719); }  else 
{ 
 obj_t BgL_auxz00_12129;
BgL_auxz00_12129 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6720, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6720)), 
(int)(BgL_arg1812z00_6719)); 
FAILURE(BgL_auxz00_12129,BFALSE,BFALSE);} } } } 
BgL_test4967z00_12109 = 
(BgL_arg1811z00_6718==BgL_classz00_6714); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6722;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6723;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6724; long BgL_arg1555z00_6725;
BgL_arg1554z00_6724 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6726;
BgL_arg1556z00_6726 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6715); 
BgL_arg1555z00_6725 = 
(BgL_arg1556z00_6726-OBJECT_TYPE); } 
BgL_oclassz00_6723 = 
VECTOR_REF(BgL_arg1554z00_6724,BgL_arg1555z00_6725); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6727;
BgL__ortest_1220z00_6727 = 
(BgL_classz00_6714==BgL_oclassz00_6723); 
if(BgL__ortest_1220z00_6727)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6722 = BgL__ortest_1220z00_6727; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6728;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6729;
BgL_arg1810z00_6729 = 
(BgL_oclassz00_6723); 
BgL_odepthz00_6728 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6729); } 
if(
(BgL_arg1804z00_6716<BgL_odepthz00_6728))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6730;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6731;
BgL_arg1809z00_6731 = 
(BgL_oclassz00_6723); 
BgL_arg1808z00_6730 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6731, BgL_arg1804z00_6716); } 
BgL_res2618z00_6722 = 
(BgL_arg1808z00_6730==BgL_classz00_6714); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6722 = ((bool_t)0); } } } } 
BgL_test4967z00_12109 = BgL_res2618z00_6722; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4967z00_12109 = ((bool_t)0)
; } } 
if(BgL_test4967z00_12109)
{ /* Llib/object.scm 271 */
BgL_oz00_6732 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BgL_oz00_5101); }  else 
{ 
 obj_t BgL_auxz00_12151;
BgL_auxz00_12151 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9659L), BGl_string3825z00zz__objectz00, BGl_string3802z00zz__objectz00, BgL_oz00_5101); 
FAILURE(BgL_auxz00_12151,BFALSE,BFALSE);} } 
return 
((((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_oz00_6732))->BgL_objz00)=((obj_t)BgL_vz00_5102),BUNSPEC);} } 

}



/* &lambda2137 */
obj_t BGl_z62lambda2137z62zz__objectz00(obj_t BgL_envz00_5103, obj_t BgL_oz00_5104)
{
{ /* Llib/object.scm 271 */
{ /* Llib/object.scm 271 */
 BgL_z62accesszd2controlzd2exceptionz62_bglt BgL_oz00_6751;
{ /* Llib/object.scm 271 */
 bool_t BgL_test4974z00_12156;
{ /* Llib/object.scm 271 */
 obj_t BgL_classz00_6733;
BgL_classz00_6733 = BGl_z62accesszd2controlzd2exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5104))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6734; long BgL_arg1804z00_6735;
BgL_arg1803z00_6734 = 
(BgL_objectz00_bglt)(BgL_oz00_5104); 
BgL_arg1804z00_6735 = 
BGL_CLASS_DEPTH(BgL_classz00_6733); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6736;
BgL_idxz00_6736 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6734); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6737;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6738;
BgL_arg1812z00_6738 = 
(BgL_idxz00_6736+BgL_arg1804z00_6735); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6739;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6740;
BgL_aux3145z00_6740 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6740))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6739 = BgL_aux3145z00_6740; }  else 
{ 
 obj_t BgL_auxz00_12167;
BgL_auxz00_12167 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6740); 
FAILURE(BgL_auxz00_12167,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4978z00_12171;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12172;
BgL_tmpz00_12172 = 
VECTOR_LENGTH(BgL_vectorz00_6739); 
BgL_test4978z00_12171 = 
BOUND_CHECK(BgL_arg1812z00_6738, BgL_tmpz00_12172); } 
if(BgL_test4978z00_12171)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6737 = 
VECTOR_REF(BgL_vectorz00_6739,BgL_arg1812z00_6738); }  else 
{ 
 obj_t BgL_auxz00_12176;
BgL_auxz00_12176 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6739, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6739)), 
(int)(BgL_arg1812z00_6738)); 
FAILURE(BgL_auxz00_12176,BFALSE,BFALSE);} } } } 
BgL_test4974z00_12156 = 
(BgL_arg1811z00_6737==BgL_classz00_6733); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6741;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6742;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6743; long BgL_arg1555z00_6744;
BgL_arg1554z00_6743 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6745;
BgL_arg1556z00_6745 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6734); 
BgL_arg1555z00_6744 = 
(BgL_arg1556z00_6745-OBJECT_TYPE); } 
BgL_oclassz00_6742 = 
VECTOR_REF(BgL_arg1554z00_6743,BgL_arg1555z00_6744); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6746;
BgL__ortest_1220z00_6746 = 
(BgL_classz00_6733==BgL_oclassz00_6742); 
if(BgL__ortest_1220z00_6746)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6741 = BgL__ortest_1220z00_6746; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6747;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6748;
BgL_arg1810z00_6748 = 
(BgL_oclassz00_6742); 
BgL_odepthz00_6747 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6748); } 
if(
(BgL_arg1804z00_6735<BgL_odepthz00_6747))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6749;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6750;
BgL_arg1809z00_6750 = 
(BgL_oclassz00_6742); 
BgL_arg1808z00_6749 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6750, BgL_arg1804z00_6735); } 
BgL_res2618z00_6741 = 
(BgL_arg1808z00_6749==BgL_classz00_6733); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6741 = ((bool_t)0); } } } } 
BgL_test4974z00_12156 = BgL_res2618z00_6741; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4974z00_12156 = ((bool_t)0)
; } } 
if(BgL_test4974z00_12156)
{ /* Llib/object.scm 271 */
BgL_oz00_6751 = 
((BgL_z62accesszd2controlzd2exceptionz62_bglt)BgL_oz00_5104); }  else 
{ 
 obj_t BgL_auxz00_12198;
BgL_auxz00_12198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9659L), BGl_string3826z00zz__objectz00, BGl_string3802z00zz__objectz00, BgL_oz00_5104); 
FAILURE(BgL_auxz00_12198,BFALSE,BFALSE);} } 
return 
(((BgL_z62accesszd2controlzd2exceptionz62_bglt)COBJECT(BgL_oz00_6751))->BgL_objz00);} } 

}



/* &<@anonymous:2114> */
obj_t BGl_z62zc3z04anonymousza32114ze3ze5zz__objectz00(obj_t BgL_envz00_5105, obj_t BgL_new1186z00_5106)
{
{ /* Llib/object.scm 269 */
{ 
 BgL_z62securityzd2exceptionzb0_bglt BgL_auxz00_12203;
{ /* Llib/object.scm 269 */
 BgL_z62securityzd2exceptionzb0_bglt BgL_new1186z00_6770;
{ /* Llib/object.scm 269 */
 bool_t BgL_test4981z00_12204;
{ /* Llib/object.scm 269 */
 obj_t BgL_classz00_6752;
BgL_classz00_6752 = BGl_z62securityzd2exceptionzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1186z00_5106))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6753; long BgL_arg1804z00_6754;
BgL_arg1803z00_6753 = 
(BgL_objectz00_bglt)(BgL_new1186z00_5106); 
BgL_arg1804z00_6754 = 
BGL_CLASS_DEPTH(BgL_classz00_6752); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6755;
BgL_idxz00_6755 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6753); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6756;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6757;
BgL_arg1812z00_6757 = 
(BgL_idxz00_6755+BgL_arg1804z00_6754); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6758;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6759;
BgL_aux3145z00_6759 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6759))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6758 = BgL_aux3145z00_6759; }  else 
{ 
 obj_t BgL_auxz00_12215;
BgL_auxz00_12215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6759); 
FAILURE(BgL_auxz00_12215,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4985z00_12219;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12220;
BgL_tmpz00_12220 = 
VECTOR_LENGTH(BgL_vectorz00_6758); 
BgL_test4985z00_12219 = 
BOUND_CHECK(BgL_arg1812z00_6757, BgL_tmpz00_12220); } 
if(BgL_test4985z00_12219)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6756 = 
VECTOR_REF(BgL_vectorz00_6758,BgL_arg1812z00_6757); }  else 
{ 
 obj_t BgL_auxz00_12224;
BgL_auxz00_12224 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6758, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6758)), 
(int)(BgL_arg1812z00_6757)); 
FAILURE(BgL_auxz00_12224,BFALSE,BFALSE);} } } } 
BgL_test4981z00_12204 = 
(BgL_arg1811z00_6756==BgL_classz00_6752); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6760;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6761;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6762; long BgL_arg1555z00_6763;
BgL_arg1554z00_6762 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6764;
BgL_arg1556z00_6764 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6753); 
BgL_arg1555z00_6763 = 
(BgL_arg1556z00_6764-OBJECT_TYPE); } 
BgL_oclassz00_6761 = 
VECTOR_REF(BgL_arg1554z00_6762,BgL_arg1555z00_6763); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6765;
BgL__ortest_1220z00_6765 = 
(BgL_classz00_6752==BgL_oclassz00_6761); 
if(BgL__ortest_1220z00_6765)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6760 = BgL__ortest_1220z00_6765; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6766;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6767;
BgL_arg1810z00_6767 = 
(BgL_oclassz00_6761); 
BgL_odepthz00_6766 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6767); } 
if(
(BgL_arg1804z00_6754<BgL_odepthz00_6766))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6768;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6769;
BgL_arg1809z00_6769 = 
(BgL_oclassz00_6761); 
BgL_arg1808z00_6768 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6769, BgL_arg1804z00_6754); } 
BgL_res2618z00_6760 = 
(BgL_arg1808z00_6768==BgL_classz00_6752); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6760 = ((bool_t)0); } } } } 
BgL_test4981z00_12204 = BgL_res2618z00_6760; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4981z00_12204 = ((bool_t)0)
; } } 
if(BgL_test4981z00_12204)
{ /* Llib/object.scm 269 */
BgL_new1186z00_6770 = 
((BgL_z62securityzd2exceptionzb0_bglt)BgL_new1186z00_5106); }  else 
{ 
 obj_t BgL_auxz00_12246;
BgL_auxz00_12246 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9564L), BGl_string3827z00zz__objectz00, BGl_string3789z00zz__objectz00, BgL_new1186z00_5106); 
FAILURE(BgL_auxz00_12246,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1186z00_6770)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1186z00_6770)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1186z00_6770)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62securityzd2exceptionzb0_bglt)COBJECT(BgL_new1186z00_6770))->BgL_messagez00)=((obj_t)BGl_string3576z00zz__objectz00),BUNSPEC); 
BgL_auxz00_12203 = BgL_new1186z00_6770; } 
return 
((obj_t)BgL_auxz00_12203);} } 

}



/* &lambda2112 */
BgL_z62securityzd2exceptionzb0_bglt BGl_z62lambda2112z62zz__objectz00(obj_t BgL_envz00_5107)
{
{ /* Llib/object.scm 269 */
{ /* Llib/object.scm 269 */
 BgL_z62securityzd2exceptionzb0_bglt BgL_new1185z00_6771;
BgL_new1185z00_6771 = 
((BgL_z62securityzd2exceptionzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62securityzd2exceptionzb0_bgl) ))); 
{ /* Llib/object.scm 269 */
 long BgL_arg2113z00_6772;
BgL_arg2113z00_6772 = 
BGL_CLASS_NUM(BGl_z62securityzd2exceptionzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1185z00_6771), BgL_arg2113z00_6772); } 
return BgL_new1185z00_6771;} } 

}



/* &lambda2110 */
BgL_z62securityzd2exceptionzb0_bglt BGl_z62lambda2110z62zz__objectz00(obj_t BgL_envz00_5108, obj_t BgL_fname1181z00_5109, obj_t BgL_location1182z00_5110, obj_t BgL_stack1183z00_5111, obj_t BgL_message1184z00_5112)
{
{ /* Llib/object.scm 269 */
{ /* Llib/object.scm 269 */
 obj_t BgL_message1184z00_6773;
if(
STRINGP(BgL_message1184z00_5112))
{ /* Llib/object.scm 269 */
BgL_message1184z00_6773 = BgL_message1184z00_5112; }  else 
{ 
 obj_t BgL_auxz00_12264;
BgL_auxz00_12264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9564L), BGl_string3828z00zz__objectz00, BGl_string3784z00zz__objectz00, BgL_message1184z00_5112); 
FAILURE(BgL_auxz00_12264,BFALSE,BFALSE);} 
{ /* Llib/object.scm 269 */
 BgL_z62securityzd2exceptionzb0_bglt BgL_new1271z00_6774;
{ /* Llib/object.scm 269 */
 BgL_z62securityzd2exceptionzb0_bglt BgL_new1270z00_6775;
BgL_new1270z00_6775 = 
((BgL_z62securityzd2exceptionzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62securityzd2exceptionzb0_bgl) ))); 
{ /* Llib/object.scm 269 */
 long BgL_arg2111z00_6776;
BgL_arg2111z00_6776 = 
BGL_CLASS_NUM(BGl_z62securityzd2exceptionzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1270z00_6775), BgL_arg2111z00_6776); } 
BgL_new1271z00_6774 = BgL_new1270z00_6775; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1271z00_6774)))->BgL_fnamez00)=((obj_t)BgL_fname1181z00_5109),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1271z00_6774)))->BgL_locationz00)=((obj_t)BgL_location1182z00_5110),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1271z00_6774)))->BgL_stackz00)=((obj_t)BgL_stack1183z00_5111),BUNSPEC); 
((((BgL_z62securityzd2exceptionzb0_bglt)COBJECT(BgL_new1271z00_6774))->BgL_messagez00)=((obj_t)BgL_message1184z00_6773),BUNSPEC); 
return BgL_new1271z00_6774;} } } 

}



/* &<@anonymous:2121> */
obj_t BGl_z62zc3z04anonymousza32121ze3ze5zz__objectz00(obj_t BgL_envz00_5113)
{
{ /* Llib/object.scm 269 */
return BGl_string3576z00zz__objectz00;} 

}



/* &lambda2120 */
obj_t BGl_z62lambda2120z62zz__objectz00(obj_t BgL_envz00_5114, obj_t BgL_oz00_5115, obj_t BgL_vz00_5116)
{
{ /* Llib/object.scm 269 */
{ /* Llib/object.scm 269 */
 BgL_z62securityzd2exceptionzb0_bglt BgL_oz00_6795; obj_t BgL_vz00_6796;
{ /* Llib/object.scm 269 */
 bool_t BgL_test4989z00_12279;
{ /* Llib/object.scm 269 */
 obj_t BgL_classz00_6777;
BgL_classz00_6777 = BGl_z62securityzd2exceptionzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5115))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6778; long BgL_arg1804z00_6779;
BgL_arg1803z00_6778 = 
(BgL_objectz00_bglt)(BgL_oz00_5115); 
BgL_arg1804z00_6779 = 
BGL_CLASS_DEPTH(BgL_classz00_6777); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6780;
BgL_idxz00_6780 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6778); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6781;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6782;
BgL_arg1812z00_6782 = 
(BgL_idxz00_6780+BgL_arg1804z00_6779); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6783;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6784;
BgL_aux3145z00_6784 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6784))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6783 = BgL_aux3145z00_6784; }  else 
{ 
 obj_t BgL_auxz00_12290;
BgL_auxz00_12290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6784); 
FAILURE(BgL_auxz00_12290,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test4993z00_12294;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12295;
BgL_tmpz00_12295 = 
VECTOR_LENGTH(BgL_vectorz00_6783); 
BgL_test4993z00_12294 = 
BOUND_CHECK(BgL_arg1812z00_6782, BgL_tmpz00_12295); } 
if(BgL_test4993z00_12294)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6781 = 
VECTOR_REF(BgL_vectorz00_6783,BgL_arg1812z00_6782); }  else 
{ 
 obj_t BgL_auxz00_12299;
BgL_auxz00_12299 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6783, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6783)), 
(int)(BgL_arg1812z00_6782)); 
FAILURE(BgL_auxz00_12299,BFALSE,BFALSE);} } } } 
BgL_test4989z00_12279 = 
(BgL_arg1811z00_6781==BgL_classz00_6777); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6785;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6786;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6787; long BgL_arg1555z00_6788;
BgL_arg1554z00_6787 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6789;
BgL_arg1556z00_6789 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6778); 
BgL_arg1555z00_6788 = 
(BgL_arg1556z00_6789-OBJECT_TYPE); } 
BgL_oclassz00_6786 = 
VECTOR_REF(BgL_arg1554z00_6787,BgL_arg1555z00_6788); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6790;
BgL__ortest_1220z00_6790 = 
(BgL_classz00_6777==BgL_oclassz00_6786); 
if(BgL__ortest_1220z00_6790)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6785 = BgL__ortest_1220z00_6790; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6791;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6792;
BgL_arg1810z00_6792 = 
(BgL_oclassz00_6786); 
BgL_odepthz00_6791 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6792); } 
if(
(BgL_arg1804z00_6779<BgL_odepthz00_6791))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6793;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6794;
BgL_arg1809z00_6794 = 
(BgL_oclassz00_6786); 
BgL_arg1808z00_6793 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6794, BgL_arg1804z00_6779); } 
BgL_res2618z00_6785 = 
(BgL_arg1808z00_6793==BgL_classz00_6777); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6785 = ((bool_t)0); } } } } 
BgL_test4989z00_12279 = BgL_res2618z00_6785; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4989z00_12279 = ((bool_t)0)
; } } 
if(BgL_test4989z00_12279)
{ /* Llib/object.scm 269 */
BgL_oz00_6795 = 
((BgL_z62securityzd2exceptionzb0_bglt)BgL_oz00_5115); }  else 
{ 
 obj_t BgL_auxz00_12321;
BgL_auxz00_12321 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9564L), BGl_string3829z00zz__objectz00, BGl_string3789z00zz__objectz00, BgL_oz00_5115); 
FAILURE(BgL_auxz00_12321,BFALSE,BFALSE);} } 
if(
STRINGP(BgL_vz00_5116))
{ /* Llib/object.scm 269 */
BgL_vz00_6796 = BgL_vz00_5116; }  else 
{ 
 obj_t BgL_auxz00_12327;
BgL_auxz00_12327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9564L), BGl_string3829z00zz__objectz00, BGl_string3784z00zz__objectz00, BgL_vz00_5116); 
FAILURE(BgL_auxz00_12327,BFALSE,BFALSE);} 
return 
((((BgL_z62securityzd2exceptionzb0_bglt)COBJECT(BgL_oz00_6795))->BgL_messagez00)=((obj_t)BgL_vz00_6796),BUNSPEC);} } 

}



/* &lambda2119 */
obj_t BGl_z62lambda2119z62zz__objectz00(obj_t BgL_envz00_5117, obj_t BgL_oz00_5118)
{
{ /* Llib/object.scm 269 */
{ /* Llib/object.scm 269 */
 BgL_z62securityzd2exceptionzb0_bglt BgL_oz00_6815;
{ /* Llib/object.scm 269 */
 bool_t BgL_test4997z00_12332;
{ /* Llib/object.scm 269 */
 obj_t BgL_classz00_6797;
BgL_classz00_6797 = BGl_z62securityzd2exceptionzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5118))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6798; long BgL_arg1804z00_6799;
BgL_arg1803z00_6798 = 
(BgL_objectz00_bglt)(BgL_oz00_5118); 
BgL_arg1804z00_6799 = 
BGL_CLASS_DEPTH(BgL_classz00_6797); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6800;
BgL_idxz00_6800 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6798); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6801;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6802;
BgL_arg1812z00_6802 = 
(BgL_idxz00_6800+BgL_arg1804z00_6799); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6803;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6804;
BgL_aux3145z00_6804 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6804))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6803 = BgL_aux3145z00_6804; }  else 
{ 
 obj_t BgL_auxz00_12343;
BgL_auxz00_12343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6804); 
FAILURE(BgL_auxz00_12343,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5001z00_12347;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12348;
BgL_tmpz00_12348 = 
VECTOR_LENGTH(BgL_vectorz00_6803); 
BgL_test5001z00_12347 = 
BOUND_CHECK(BgL_arg1812z00_6802, BgL_tmpz00_12348); } 
if(BgL_test5001z00_12347)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6801 = 
VECTOR_REF(BgL_vectorz00_6803,BgL_arg1812z00_6802); }  else 
{ 
 obj_t BgL_auxz00_12352;
BgL_auxz00_12352 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6803, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6803)), 
(int)(BgL_arg1812z00_6802)); 
FAILURE(BgL_auxz00_12352,BFALSE,BFALSE);} } } } 
BgL_test4997z00_12332 = 
(BgL_arg1811z00_6801==BgL_classz00_6797); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6805;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6806;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6807; long BgL_arg1555z00_6808;
BgL_arg1554z00_6807 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6809;
BgL_arg1556z00_6809 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6798); 
BgL_arg1555z00_6808 = 
(BgL_arg1556z00_6809-OBJECT_TYPE); } 
BgL_oclassz00_6806 = 
VECTOR_REF(BgL_arg1554z00_6807,BgL_arg1555z00_6808); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6810;
BgL__ortest_1220z00_6810 = 
(BgL_classz00_6797==BgL_oclassz00_6806); 
if(BgL__ortest_1220z00_6810)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6805 = BgL__ortest_1220z00_6810; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6811;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6812;
BgL_arg1810z00_6812 = 
(BgL_oclassz00_6806); 
BgL_odepthz00_6811 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6812); } 
if(
(BgL_arg1804z00_6799<BgL_odepthz00_6811))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6813;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6814;
BgL_arg1809z00_6814 = 
(BgL_oclassz00_6806); 
BgL_arg1808z00_6813 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6814, BgL_arg1804z00_6799); } 
BgL_res2618z00_6805 = 
(BgL_arg1808z00_6813==BgL_classz00_6797); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6805 = ((bool_t)0); } } } } 
BgL_test4997z00_12332 = BgL_res2618z00_6805; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test4997z00_12332 = ((bool_t)0)
; } } 
if(BgL_test4997z00_12332)
{ /* Llib/object.scm 269 */
BgL_oz00_6815 = 
((BgL_z62securityzd2exceptionzb0_bglt)BgL_oz00_5118); }  else 
{ 
 obj_t BgL_auxz00_12374;
BgL_auxz00_12374 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9564L), BGl_string3830z00zz__objectz00, BGl_string3789z00zz__objectz00, BgL_oz00_5118); 
FAILURE(BgL_auxz00_12374,BFALSE,BFALSE);} } 
return 
(((BgL_z62securityzd2exceptionzb0_bglt)COBJECT(BgL_oz00_6815))->BgL_messagez00);} } 

}



/* &<@anonymous:2104> */
obj_t BGl_z62zc3z04anonymousza32104ze3ze5zz__objectz00(obj_t BgL_envz00_5119, obj_t BgL_new1179z00_5120)
{
{ /* Llib/object.scm 267 */
{ 
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_auxz00_12379;
{ /* Llib/object.scm 267 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1179z00_6834;
{ /* Llib/object.scm 267 */
 bool_t BgL_test5004z00_12380;
{ /* Llib/object.scm 267 */
 obj_t BgL_classz00_6816;
BgL_classz00_6816 = BGl_z62stackzd2overflowzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1179z00_5120))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6817; long BgL_arg1804z00_6818;
BgL_arg1803z00_6817 = 
(BgL_objectz00_bglt)(BgL_new1179z00_5120); 
BgL_arg1804z00_6818 = 
BGL_CLASS_DEPTH(BgL_classz00_6816); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6819;
BgL_idxz00_6819 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6817); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6820;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6821;
BgL_arg1812z00_6821 = 
(BgL_idxz00_6819+BgL_arg1804z00_6818); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6822;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6823;
BgL_aux3145z00_6823 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6823))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6822 = BgL_aux3145z00_6823; }  else 
{ 
 obj_t BgL_auxz00_12391;
BgL_auxz00_12391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6823); 
FAILURE(BgL_auxz00_12391,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5008z00_12395;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12396;
BgL_tmpz00_12396 = 
VECTOR_LENGTH(BgL_vectorz00_6822); 
BgL_test5008z00_12395 = 
BOUND_CHECK(BgL_arg1812z00_6821, BgL_tmpz00_12396); } 
if(BgL_test5008z00_12395)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6820 = 
VECTOR_REF(BgL_vectorz00_6822,BgL_arg1812z00_6821); }  else 
{ 
 obj_t BgL_auxz00_12400;
BgL_auxz00_12400 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6822, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6822)), 
(int)(BgL_arg1812z00_6821)); 
FAILURE(BgL_auxz00_12400,BFALSE,BFALSE);} } } } 
BgL_test5004z00_12380 = 
(BgL_arg1811z00_6820==BgL_classz00_6816); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6824;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6825;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6826; long BgL_arg1555z00_6827;
BgL_arg1554z00_6826 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6828;
BgL_arg1556z00_6828 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6817); 
BgL_arg1555z00_6827 = 
(BgL_arg1556z00_6828-OBJECT_TYPE); } 
BgL_oclassz00_6825 = 
VECTOR_REF(BgL_arg1554z00_6826,BgL_arg1555z00_6827); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6829;
BgL__ortest_1220z00_6829 = 
(BgL_classz00_6816==BgL_oclassz00_6825); 
if(BgL__ortest_1220z00_6829)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6824 = BgL__ortest_1220z00_6829; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6830;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6831;
BgL_arg1810z00_6831 = 
(BgL_oclassz00_6825); 
BgL_odepthz00_6830 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6831); } 
if(
(BgL_arg1804z00_6818<BgL_odepthz00_6830))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6832;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6833;
BgL_arg1809z00_6833 = 
(BgL_oclassz00_6825); 
BgL_arg1808z00_6832 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6833, BgL_arg1804z00_6818); } 
BgL_res2618z00_6824 = 
(BgL_arg1808z00_6832==BgL_classz00_6816); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6824 = ((bool_t)0); } } } } 
BgL_test5004z00_12380 = BgL_res2618z00_6824; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5004z00_12380 = ((bool_t)0)
; } } 
if(BgL_test5004z00_12380)
{ /* Llib/object.scm 267 */
BgL_new1179z00_6834 = 
((BgL_z62stackzd2overflowzd2errorz62_bglt)BgL_new1179z00_5120); }  else 
{ 
 obj_t BgL_auxz00_12422;
BgL_auxz00_12422 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9515L), BGl_string3831z00zz__objectz00, BGl_string3777z00zz__objectz00, BgL_new1179z00_5120); 
FAILURE(BgL_auxz00_12422,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1179z00_6834)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1179z00_6834)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1179z00_6834)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1179z00_6834)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1179z00_6834)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1179z00_6834)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12379 = BgL_new1179z00_6834; } 
return 
((obj_t)BgL_auxz00_12379);} } 

}



/* &lambda2102 */
BgL_z62stackzd2overflowzd2errorz62_bglt BGl_z62lambda2102z62zz__objectz00(obj_t BgL_envz00_5121)
{
{ /* Llib/object.scm 267 */
{ /* Llib/object.scm 267 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1178z00_6835;
BgL_new1178z00_6835 = 
((BgL_z62stackzd2overflowzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62stackzd2overflowzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 267 */
 long BgL_arg2103z00_6836;
BgL_arg2103z00_6836 = 
BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1178z00_6835), BgL_arg2103z00_6836); } 
return BgL_new1178z00_6835;} } 

}



/* &lambda2100 */
BgL_z62stackzd2overflowzd2errorz62_bglt BGl_z62lambda2100z62zz__objectz00(obj_t BgL_envz00_5122, obj_t BgL_fname1172z00_5123, obj_t BgL_location1173z00_5124, obj_t BgL_stack1174z00_5125, obj_t BgL_proc1175z00_5126, obj_t BgL_msg1176z00_5127, obj_t BgL_obj1177z00_5128)
{
{ /* Llib/object.scm 267 */
{ /* Llib/object.scm 267 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1269z00_6837;
{ /* Llib/object.scm 267 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1268z00_6838;
BgL_new1268z00_6838 = 
((BgL_z62stackzd2overflowzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62stackzd2overflowzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 267 */
 long BgL_arg2101z00_6839;
BgL_arg2101z00_6839 = 
BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1268z00_6838), BgL_arg2101z00_6839); } 
BgL_new1269z00_6837 = BgL_new1268z00_6838; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1269z00_6837)))->BgL_fnamez00)=((obj_t)BgL_fname1172z00_5123),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1269z00_6837)))->BgL_locationz00)=((obj_t)BgL_location1173z00_5124),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1269z00_6837)))->BgL_stackz00)=((obj_t)BgL_stack1174z00_5125),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1269z00_6837)))->BgL_procz00)=((obj_t)BgL_proc1175z00_5126),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1269z00_6837)))->BgL_msgz00)=((obj_t)BgL_msg1176z00_5127),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1269z00_6837)))->BgL_objz00)=((obj_t)BgL_obj1177z00_5128),BUNSPEC); 
return BgL_new1269z00_6837;} } 

}



/* &<@anonymous:2094> */
obj_t BGl_z62zc3z04anonymousza32094ze3ze5zz__objectz00(obj_t BgL_envz00_5129, obj_t BgL_new1170z00_5130)
{
{ /* Llib/object.scm 265 */
{ 
 BgL_z62processzd2exceptionzb0_bglt BgL_auxz00_12459;
{ /* Llib/object.scm 265 */
 BgL_z62processzd2exceptionzb0_bglt BgL_new1170z00_6858;
{ /* Llib/object.scm 265 */
 bool_t BgL_test5011z00_12460;
{ /* Llib/object.scm 265 */
 obj_t BgL_classz00_6840;
BgL_classz00_6840 = BGl_z62processzd2exceptionzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1170z00_5130))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6841; long BgL_arg1804z00_6842;
BgL_arg1803z00_6841 = 
(BgL_objectz00_bglt)(BgL_new1170z00_5130); 
BgL_arg1804z00_6842 = 
BGL_CLASS_DEPTH(BgL_classz00_6840); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6843;
BgL_idxz00_6843 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6841); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6844;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6845;
BgL_arg1812z00_6845 = 
(BgL_idxz00_6843+BgL_arg1804z00_6842); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6846;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6847;
BgL_aux3145z00_6847 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6847))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6846 = BgL_aux3145z00_6847; }  else 
{ 
 obj_t BgL_auxz00_12471;
BgL_auxz00_12471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6847); 
FAILURE(BgL_auxz00_12471,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5015z00_12475;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12476;
BgL_tmpz00_12476 = 
VECTOR_LENGTH(BgL_vectorz00_6846); 
BgL_test5015z00_12475 = 
BOUND_CHECK(BgL_arg1812z00_6845, BgL_tmpz00_12476); } 
if(BgL_test5015z00_12475)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6844 = 
VECTOR_REF(BgL_vectorz00_6846,BgL_arg1812z00_6845); }  else 
{ 
 obj_t BgL_auxz00_12480;
BgL_auxz00_12480 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6846, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6846)), 
(int)(BgL_arg1812z00_6845)); 
FAILURE(BgL_auxz00_12480,BFALSE,BFALSE);} } } } 
BgL_test5011z00_12460 = 
(BgL_arg1811z00_6844==BgL_classz00_6840); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6848;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6849;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6850; long BgL_arg1555z00_6851;
BgL_arg1554z00_6850 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6852;
BgL_arg1556z00_6852 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6841); 
BgL_arg1555z00_6851 = 
(BgL_arg1556z00_6852-OBJECT_TYPE); } 
BgL_oclassz00_6849 = 
VECTOR_REF(BgL_arg1554z00_6850,BgL_arg1555z00_6851); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6853;
BgL__ortest_1220z00_6853 = 
(BgL_classz00_6840==BgL_oclassz00_6849); 
if(BgL__ortest_1220z00_6853)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6848 = BgL__ortest_1220z00_6853; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6854;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6855;
BgL_arg1810z00_6855 = 
(BgL_oclassz00_6849); 
BgL_odepthz00_6854 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6855); } 
if(
(BgL_arg1804z00_6842<BgL_odepthz00_6854))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6856;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6857;
BgL_arg1809z00_6857 = 
(BgL_oclassz00_6849); 
BgL_arg1808z00_6856 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6857, BgL_arg1804z00_6842); } 
BgL_res2618z00_6848 = 
(BgL_arg1808z00_6856==BgL_classz00_6840); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6848 = ((bool_t)0); } } } } 
BgL_test5011z00_12460 = BgL_res2618z00_6848; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5011z00_12460 = ((bool_t)0)
; } } 
if(BgL_test5011z00_12460)
{ /* Llib/object.scm 265 */
BgL_new1170z00_6858 = 
((BgL_z62processzd2exceptionzb0_bglt)BgL_new1170z00_5130); }  else 
{ 
 obj_t BgL_auxz00_12502;
BgL_auxz00_12502 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9469L), BGl_string3832z00zz__objectz00, BGl_string3772z00zz__objectz00, BgL_new1170z00_5130); 
FAILURE(BgL_auxz00_12502,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1170z00_6858)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1170z00_6858)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1170z00_6858)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1170z00_6858)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1170z00_6858)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1170z00_6858)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12459 = BgL_new1170z00_6858; } 
return 
((obj_t)BgL_auxz00_12459);} } 

}



/* &lambda2092 */
BgL_z62processzd2exceptionzb0_bglt BGl_z62lambda2092z62zz__objectz00(obj_t BgL_envz00_5131)
{
{ /* Llib/object.scm 265 */
{ /* Llib/object.scm 265 */
 BgL_z62processzd2exceptionzb0_bglt BgL_new1169z00_6859;
BgL_new1169z00_6859 = 
((BgL_z62processzd2exceptionzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62processzd2exceptionzb0_bgl) ))); 
{ /* Llib/object.scm 265 */
 long BgL_arg2093z00_6860;
BgL_arg2093z00_6860 = 
BGL_CLASS_NUM(BGl_z62processzd2exceptionzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1169z00_6859), BgL_arg2093z00_6860); } 
return BgL_new1169z00_6859;} } 

}



/* &lambda2090 */
BgL_z62processzd2exceptionzb0_bglt BGl_z62lambda2090z62zz__objectz00(obj_t BgL_envz00_5132, obj_t BgL_fname1163z00_5133, obj_t BgL_location1164z00_5134, obj_t BgL_stack1165z00_5135, obj_t BgL_proc1166z00_5136, obj_t BgL_msg1167z00_5137, obj_t BgL_obj1168z00_5138)
{
{ /* Llib/object.scm 265 */
{ /* Llib/object.scm 265 */
 BgL_z62processzd2exceptionzb0_bglt BgL_new1267z00_6861;
{ /* Llib/object.scm 265 */
 BgL_z62processzd2exceptionzb0_bglt BgL_new1266z00_6862;
BgL_new1266z00_6862 = 
((BgL_z62processzd2exceptionzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62processzd2exceptionzb0_bgl) ))); 
{ /* Llib/object.scm 265 */
 long BgL_arg2091z00_6863;
BgL_arg2091z00_6863 = 
BGL_CLASS_NUM(BGl_z62processzd2exceptionzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1266z00_6862), BgL_arg2091z00_6863); } 
BgL_new1267z00_6861 = BgL_new1266z00_6862; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1267z00_6861)))->BgL_fnamez00)=((obj_t)BgL_fname1163z00_5133),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1267z00_6861)))->BgL_locationz00)=((obj_t)BgL_location1164z00_5134),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1267z00_6861)))->BgL_stackz00)=((obj_t)BgL_stack1165z00_5135),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1267z00_6861)))->BgL_procz00)=((obj_t)BgL_proc1166z00_5136),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1267z00_6861)))->BgL_msgz00)=((obj_t)BgL_msg1167z00_5137),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1267z00_6861)))->BgL_objz00)=((obj_t)BgL_obj1168z00_5138),BUNSPEC); 
return BgL_new1267z00_6861;} } 

}



/* &<@anonymous:2083> */
obj_t BGl_z62zc3z04anonymousza32083ze3ze5zz__objectz00(obj_t BgL_envz00_5139, obj_t BgL_new1161z00_5140)
{
{ /* Llib/object.scm 263 */
{ 
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_auxz00_12539;
{ /* Llib/object.scm 263 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1161z00_6882;
{ /* Llib/object.scm 263 */
 bool_t BgL_test5018z00_12540;
{ /* Llib/object.scm 263 */
 obj_t BgL_classz00_6864;
BgL_classz00_6864 = BGl_z62iozd2connectionzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1161z00_5140))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6865; long BgL_arg1804z00_6866;
BgL_arg1803z00_6865 = 
(BgL_objectz00_bglt)(BgL_new1161z00_5140); 
BgL_arg1804z00_6866 = 
BGL_CLASS_DEPTH(BgL_classz00_6864); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6867;
BgL_idxz00_6867 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6865); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6868;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6869;
BgL_arg1812z00_6869 = 
(BgL_idxz00_6867+BgL_arg1804z00_6866); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6870;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6871;
BgL_aux3145z00_6871 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6871))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6870 = BgL_aux3145z00_6871; }  else 
{ 
 obj_t BgL_auxz00_12551;
BgL_auxz00_12551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6871); 
FAILURE(BgL_auxz00_12551,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5022z00_12555;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12556;
BgL_tmpz00_12556 = 
VECTOR_LENGTH(BgL_vectorz00_6870); 
BgL_test5022z00_12555 = 
BOUND_CHECK(BgL_arg1812z00_6869, BgL_tmpz00_12556); } 
if(BgL_test5022z00_12555)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6868 = 
VECTOR_REF(BgL_vectorz00_6870,BgL_arg1812z00_6869); }  else 
{ 
 obj_t BgL_auxz00_12560;
BgL_auxz00_12560 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6870, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6870)), 
(int)(BgL_arg1812z00_6869)); 
FAILURE(BgL_auxz00_12560,BFALSE,BFALSE);} } } } 
BgL_test5018z00_12540 = 
(BgL_arg1811z00_6868==BgL_classz00_6864); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6872;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6873;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6874; long BgL_arg1555z00_6875;
BgL_arg1554z00_6874 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6876;
BgL_arg1556z00_6876 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6865); 
BgL_arg1555z00_6875 = 
(BgL_arg1556z00_6876-OBJECT_TYPE); } 
BgL_oclassz00_6873 = 
VECTOR_REF(BgL_arg1554z00_6874,BgL_arg1555z00_6875); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6877;
BgL__ortest_1220z00_6877 = 
(BgL_classz00_6864==BgL_oclassz00_6873); 
if(BgL__ortest_1220z00_6877)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6872 = BgL__ortest_1220z00_6877; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6878;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6879;
BgL_arg1810z00_6879 = 
(BgL_oclassz00_6873); 
BgL_odepthz00_6878 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6879); } 
if(
(BgL_arg1804z00_6866<BgL_odepthz00_6878))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6880;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6881;
BgL_arg1809z00_6881 = 
(BgL_oclassz00_6873); 
BgL_arg1808z00_6880 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6881, BgL_arg1804z00_6866); } 
BgL_res2618z00_6872 = 
(BgL_arg1808z00_6880==BgL_classz00_6864); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6872 = ((bool_t)0); } } } } 
BgL_test5018z00_12540 = BgL_res2618z00_6872; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5018z00_12540 = ((bool_t)0)
; } } 
if(BgL_test5018z00_12540)
{ /* Llib/object.scm 263 */
BgL_new1161z00_6882 = 
((BgL_z62iozd2connectionzd2errorz62_bglt)BgL_new1161z00_5140); }  else 
{ 
 obj_t BgL_auxz00_12582;
BgL_auxz00_12582 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9418L), BGl_string3833z00zz__objectz00, BGl_string3767z00zz__objectz00, BgL_new1161z00_5140); 
FAILURE(BgL_auxz00_12582,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1161z00_6882)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1161z00_6882)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1161z00_6882)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1161z00_6882)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1161z00_6882)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1161z00_6882)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12539 = BgL_new1161z00_6882; } 
return 
((obj_t)BgL_auxz00_12539);} } 

}



/* &lambda2081 */
BgL_z62iozd2connectionzd2errorz62_bglt BGl_z62lambda2081z62zz__objectz00(obj_t BgL_envz00_5141)
{
{ /* Llib/object.scm 263 */
{ /* Llib/object.scm 263 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1160z00_6883;
BgL_new1160z00_6883 = 
((BgL_z62iozd2connectionzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2connectionzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 263 */
 long BgL_arg2082z00_6884;
BgL_arg2082z00_6884 = 
BGL_CLASS_NUM(BGl_z62iozd2connectionzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1160z00_6883), BgL_arg2082z00_6884); } 
return BgL_new1160z00_6883;} } 

}



/* &lambda2079 */
BgL_z62iozd2connectionzd2errorz62_bglt BGl_z62lambda2079z62zz__objectz00(obj_t BgL_envz00_5142, obj_t BgL_fname1154z00_5143, obj_t BgL_location1155z00_5144, obj_t BgL_stack1156z00_5145, obj_t BgL_proc1157z00_5146, obj_t BgL_msg1158z00_5147, obj_t BgL_obj1159z00_5148)
{
{ /* Llib/object.scm 263 */
{ /* Llib/object.scm 263 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1265z00_6885;
{ /* Llib/object.scm 263 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1264z00_6886;
BgL_new1264z00_6886 = 
((BgL_z62iozd2connectionzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2connectionzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 263 */
 long BgL_arg2080z00_6887;
BgL_arg2080z00_6887 = 
BGL_CLASS_NUM(BGl_z62iozd2connectionzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1264z00_6886), BgL_arg2080z00_6887); } 
BgL_new1265z00_6885 = BgL_new1264z00_6886; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1265z00_6885)))->BgL_fnamez00)=((obj_t)BgL_fname1154z00_5143),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1265z00_6885)))->BgL_locationz00)=((obj_t)BgL_location1155z00_5144),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1265z00_6885)))->BgL_stackz00)=((obj_t)BgL_stack1156z00_5145),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1265z00_6885)))->BgL_procz00)=((obj_t)BgL_proc1157z00_5146),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1265z00_6885)))->BgL_msgz00)=((obj_t)BgL_msg1158z00_5147),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1265z00_6885)))->BgL_objz00)=((obj_t)BgL_obj1159z00_5148),BUNSPEC); 
return BgL_new1265z00_6885;} } 

}



/* &<@anonymous:2073> */
obj_t BGl_z62zc3z04anonymousza32073ze3ze5zz__objectz00(obj_t BgL_envz00_5149, obj_t BgL_new1152z00_5150)
{
{ /* Llib/object.scm 262 */
{ 
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_auxz00_12619;
{ /* Llib/object.scm 262 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1152z00_6906;
{ /* Llib/object.scm 262 */
 bool_t BgL_test5025z00_12620;
{ /* Llib/object.scm 262 */
 obj_t BgL_classz00_6888;
BgL_classz00_6888 = BGl_z62iozd2timeoutzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1152z00_5150))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6889; long BgL_arg1804z00_6890;
BgL_arg1803z00_6889 = 
(BgL_objectz00_bglt)(BgL_new1152z00_5150); 
BgL_arg1804z00_6890 = 
BGL_CLASS_DEPTH(BgL_classz00_6888); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6891;
BgL_idxz00_6891 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6889); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6892;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6893;
BgL_arg1812z00_6893 = 
(BgL_idxz00_6891+BgL_arg1804z00_6890); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6894;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6895;
BgL_aux3145z00_6895 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6895))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6894 = BgL_aux3145z00_6895; }  else 
{ 
 obj_t BgL_auxz00_12631;
BgL_auxz00_12631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6895); 
FAILURE(BgL_auxz00_12631,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5029z00_12635;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12636;
BgL_tmpz00_12636 = 
VECTOR_LENGTH(BgL_vectorz00_6894); 
BgL_test5029z00_12635 = 
BOUND_CHECK(BgL_arg1812z00_6893, BgL_tmpz00_12636); } 
if(BgL_test5029z00_12635)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6892 = 
VECTOR_REF(BgL_vectorz00_6894,BgL_arg1812z00_6893); }  else 
{ 
 obj_t BgL_auxz00_12640;
BgL_auxz00_12640 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6894, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6894)), 
(int)(BgL_arg1812z00_6893)); 
FAILURE(BgL_auxz00_12640,BFALSE,BFALSE);} } } } 
BgL_test5025z00_12620 = 
(BgL_arg1811z00_6892==BgL_classz00_6888); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6896;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6897;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6898; long BgL_arg1555z00_6899;
BgL_arg1554z00_6898 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6900;
BgL_arg1556z00_6900 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6889); 
BgL_arg1555z00_6899 = 
(BgL_arg1556z00_6900-OBJECT_TYPE); } 
BgL_oclassz00_6897 = 
VECTOR_REF(BgL_arg1554z00_6898,BgL_arg1555z00_6899); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6901;
BgL__ortest_1220z00_6901 = 
(BgL_classz00_6888==BgL_oclassz00_6897); 
if(BgL__ortest_1220z00_6901)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6896 = BgL__ortest_1220z00_6901; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6902;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6903;
BgL_arg1810z00_6903 = 
(BgL_oclassz00_6897); 
BgL_odepthz00_6902 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6903); } 
if(
(BgL_arg1804z00_6890<BgL_odepthz00_6902))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6904;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6905;
BgL_arg1809z00_6905 = 
(BgL_oclassz00_6897); 
BgL_arg1808z00_6904 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6905, BgL_arg1804z00_6890); } 
BgL_res2618z00_6896 = 
(BgL_arg1808z00_6904==BgL_classz00_6888); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6896 = ((bool_t)0); } } } } 
BgL_test5025z00_12620 = BgL_res2618z00_6896; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5025z00_12620 = ((bool_t)0)
; } } 
if(BgL_test5025z00_12620)
{ /* Llib/object.scm 262 */
BgL_new1152z00_6906 = 
((BgL_z62iozd2timeoutzd2errorz62_bglt)BgL_new1152z00_5150); }  else 
{ 
 obj_t BgL_auxz00_12662;
BgL_auxz00_12662 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9376L), BGl_string3834z00zz__objectz00, BGl_string3762z00zz__objectz00, BgL_new1152z00_5150); 
FAILURE(BgL_auxz00_12662,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1152z00_6906)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1152z00_6906)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1152z00_6906)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1152z00_6906)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1152z00_6906)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1152z00_6906)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12619 = BgL_new1152z00_6906; } 
return 
((obj_t)BgL_auxz00_12619);} } 

}



/* &lambda2071 */
BgL_z62iozd2timeoutzd2errorz62_bglt BGl_z62lambda2071z62zz__objectz00(obj_t BgL_envz00_5151)
{
{ /* Llib/object.scm 262 */
{ /* Llib/object.scm 262 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1151z00_6907;
BgL_new1151z00_6907 = 
((BgL_z62iozd2timeoutzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2timeoutzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 262 */
 long BgL_arg2072z00_6908;
BgL_arg2072z00_6908 = 
BGL_CLASS_NUM(BGl_z62iozd2timeoutzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1151z00_6907), BgL_arg2072z00_6908); } 
return BgL_new1151z00_6907;} } 

}



/* &lambda2069 */
BgL_z62iozd2timeoutzd2errorz62_bglt BGl_z62lambda2069z62zz__objectz00(obj_t BgL_envz00_5152, obj_t BgL_fname1145z00_5153, obj_t BgL_location1146z00_5154, obj_t BgL_stack1147z00_5155, obj_t BgL_proc1148z00_5156, obj_t BgL_msg1149z00_5157, obj_t BgL_obj1150z00_5158)
{
{ /* Llib/object.scm 262 */
{ /* Llib/object.scm 262 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1263z00_6909;
{ /* Llib/object.scm 262 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1262z00_6910;
BgL_new1262z00_6910 = 
((BgL_z62iozd2timeoutzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2timeoutzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 262 */
 long BgL_arg2070z00_6911;
BgL_arg2070z00_6911 = 
BGL_CLASS_NUM(BGl_z62iozd2timeoutzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1262z00_6910), BgL_arg2070z00_6911); } 
BgL_new1263z00_6909 = BgL_new1262z00_6910; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1263z00_6909)))->BgL_fnamez00)=((obj_t)BgL_fname1145z00_5153),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1263z00_6909)))->BgL_locationz00)=((obj_t)BgL_location1146z00_5154),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1263z00_6909)))->BgL_stackz00)=((obj_t)BgL_stack1147z00_5155),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1263z00_6909)))->BgL_procz00)=((obj_t)BgL_proc1148z00_5156),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1263z00_6909)))->BgL_msgz00)=((obj_t)BgL_msg1149z00_5157),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1263z00_6909)))->BgL_objz00)=((obj_t)BgL_obj1150z00_5158),BUNSPEC); 
return BgL_new1263z00_6909;} } 

}



/* &<@anonymous:2062> */
obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__objectz00(obj_t BgL_envz00_5159, obj_t BgL_new1143z00_5160)
{
{ /* Llib/object.scm 261 */
{ 
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_auxz00_12699;
{ /* Llib/object.scm 261 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1143z00_6930;
{ /* Llib/object.scm 261 */
 bool_t BgL_test5032z00_12700;
{ /* Llib/object.scm 261 */
 obj_t BgL_classz00_6912;
BgL_classz00_6912 = BGl_z62iozd2sigpipezd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1143z00_5160))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6913; long BgL_arg1804z00_6914;
BgL_arg1803z00_6913 = 
(BgL_objectz00_bglt)(BgL_new1143z00_5160); 
BgL_arg1804z00_6914 = 
BGL_CLASS_DEPTH(BgL_classz00_6912); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6915;
BgL_idxz00_6915 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6913); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6916;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6917;
BgL_arg1812z00_6917 = 
(BgL_idxz00_6915+BgL_arg1804z00_6914); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6918;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6919;
BgL_aux3145z00_6919 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6919))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6918 = BgL_aux3145z00_6919; }  else 
{ 
 obj_t BgL_auxz00_12711;
BgL_auxz00_12711 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6919); 
FAILURE(BgL_auxz00_12711,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5036z00_12715;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12716;
BgL_tmpz00_12716 = 
VECTOR_LENGTH(BgL_vectorz00_6918); 
BgL_test5036z00_12715 = 
BOUND_CHECK(BgL_arg1812z00_6917, BgL_tmpz00_12716); } 
if(BgL_test5036z00_12715)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6916 = 
VECTOR_REF(BgL_vectorz00_6918,BgL_arg1812z00_6917); }  else 
{ 
 obj_t BgL_auxz00_12720;
BgL_auxz00_12720 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6918, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6918)), 
(int)(BgL_arg1812z00_6917)); 
FAILURE(BgL_auxz00_12720,BFALSE,BFALSE);} } } } 
BgL_test5032z00_12700 = 
(BgL_arg1811z00_6916==BgL_classz00_6912); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6920;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6921;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6922; long BgL_arg1555z00_6923;
BgL_arg1554z00_6922 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6924;
BgL_arg1556z00_6924 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6913); 
BgL_arg1555z00_6923 = 
(BgL_arg1556z00_6924-OBJECT_TYPE); } 
BgL_oclassz00_6921 = 
VECTOR_REF(BgL_arg1554z00_6922,BgL_arg1555z00_6923); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6925;
BgL__ortest_1220z00_6925 = 
(BgL_classz00_6912==BgL_oclassz00_6921); 
if(BgL__ortest_1220z00_6925)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6920 = BgL__ortest_1220z00_6925; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6926;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6927;
BgL_arg1810z00_6927 = 
(BgL_oclassz00_6921); 
BgL_odepthz00_6926 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6927); } 
if(
(BgL_arg1804z00_6914<BgL_odepthz00_6926))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6928;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6929;
BgL_arg1809z00_6929 = 
(BgL_oclassz00_6921); 
BgL_arg1808z00_6928 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6929, BgL_arg1804z00_6914); } 
BgL_res2618z00_6920 = 
(BgL_arg1808z00_6928==BgL_classz00_6912); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6920 = ((bool_t)0); } } } } 
BgL_test5032z00_12700 = BgL_res2618z00_6920; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5032z00_12700 = ((bool_t)0)
; } } 
if(BgL_test5032z00_12700)
{ /* Llib/object.scm 261 */
BgL_new1143z00_6930 = 
((BgL_z62iozd2sigpipezd2errorz62_bglt)BgL_new1143z00_5160); }  else 
{ 
 obj_t BgL_auxz00_12742;
BgL_auxz00_12742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9334L), BGl_string3835z00zz__objectz00, BGl_string3757z00zz__objectz00, BgL_new1143z00_5160); 
FAILURE(BgL_auxz00_12742,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1143z00_6930)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1143z00_6930)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1143z00_6930)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1143z00_6930)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1143z00_6930)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1143z00_6930)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12699 = BgL_new1143z00_6930; } 
return 
((obj_t)BgL_auxz00_12699);} } 

}



/* &lambda2060 */
BgL_z62iozd2sigpipezd2errorz62_bglt BGl_z62lambda2060z62zz__objectz00(obj_t BgL_envz00_5161)
{
{ /* Llib/object.scm 261 */
{ /* Llib/object.scm 261 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1142z00_6931;
BgL_new1142z00_6931 = 
((BgL_z62iozd2sigpipezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2sigpipezd2errorz62_bgl) ))); 
{ /* Llib/object.scm 261 */
 long BgL_arg2061z00_6932;
BgL_arg2061z00_6932 = 
BGL_CLASS_NUM(BGl_z62iozd2sigpipezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1142z00_6931), BgL_arg2061z00_6932); } 
return BgL_new1142z00_6931;} } 

}



/* &lambda2058 */
BgL_z62iozd2sigpipezd2errorz62_bglt BGl_z62lambda2058z62zz__objectz00(obj_t BgL_envz00_5162, obj_t BgL_fname1136z00_5163, obj_t BgL_location1137z00_5164, obj_t BgL_stack1138z00_5165, obj_t BgL_proc1139z00_5166, obj_t BgL_msg1140z00_5167, obj_t BgL_obj1141z00_5168)
{
{ /* Llib/object.scm 261 */
{ /* Llib/object.scm 261 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1261z00_6933;
{ /* Llib/object.scm 261 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1260z00_6934;
BgL_new1260z00_6934 = 
((BgL_z62iozd2sigpipezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2sigpipezd2errorz62_bgl) ))); 
{ /* Llib/object.scm 261 */
 long BgL_arg2059z00_6935;
BgL_arg2059z00_6935 = 
BGL_CLASS_NUM(BGl_z62iozd2sigpipezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1260z00_6934), BgL_arg2059z00_6935); } 
BgL_new1261z00_6933 = BgL_new1260z00_6934; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1261z00_6933)))->BgL_fnamez00)=((obj_t)BgL_fname1136z00_5163),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1261z00_6933)))->BgL_locationz00)=((obj_t)BgL_location1137z00_5164),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1261z00_6933)))->BgL_stackz00)=((obj_t)BgL_stack1138z00_5165),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1261z00_6933)))->BgL_procz00)=((obj_t)BgL_proc1139z00_5166),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1261z00_6933)))->BgL_msgz00)=((obj_t)BgL_msg1140z00_5167),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1261z00_6933)))->BgL_objz00)=((obj_t)BgL_obj1141z00_5168),BUNSPEC); 
return BgL_new1261z00_6933;} } 

}



/* &<@anonymous:2050> */
obj_t BGl_z62zc3z04anonymousza32050ze3ze5zz__objectz00(obj_t BgL_envz00_5169, obj_t BgL_new1134z00_5170)
{
{ /* Llib/object.scm 260 */
{ 
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_auxz00_12779;
{ /* Llib/object.scm 260 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1134z00_6954;
{ /* Llib/object.scm 260 */
 bool_t BgL_test5039z00_12780;
{ /* Llib/object.scm 260 */
 obj_t BgL_classz00_6936;
BgL_classz00_6936 = BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1134z00_5170))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6937; long BgL_arg1804z00_6938;
BgL_arg1803z00_6937 = 
(BgL_objectz00_bglt)(BgL_new1134z00_5170); 
BgL_arg1804z00_6938 = 
BGL_CLASS_DEPTH(BgL_classz00_6936); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6939;
BgL_idxz00_6939 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6937); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6940;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6941;
BgL_arg1812z00_6941 = 
(BgL_idxz00_6939+BgL_arg1804z00_6938); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6942;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6943;
BgL_aux3145z00_6943 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6943))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6942 = BgL_aux3145z00_6943; }  else 
{ 
 obj_t BgL_auxz00_12791;
BgL_auxz00_12791 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6943); 
FAILURE(BgL_auxz00_12791,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5043z00_12795;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12796;
BgL_tmpz00_12796 = 
VECTOR_LENGTH(BgL_vectorz00_6942); 
BgL_test5043z00_12795 = 
BOUND_CHECK(BgL_arg1812z00_6941, BgL_tmpz00_12796); } 
if(BgL_test5043z00_12795)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6940 = 
VECTOR_REF(BgL_vectorz00_6942,BgL_arg1812z00_6941); }  else 
{ 
 obj_t BgL_auxz00_12800;
BgL_auxz00_12800 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6942, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6942)), 
(int)(BgL_arg1812z00_6941)); 
FAILURE(BgL_auxz00_12800,BFALSE,BFALSE);} } } } 
BgL_test5039z00_12780 = 
(BgL_arg1811z00_6940==BgL_classz00_6936); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6944;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6945;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6946; long BgL_arg1555z00_6947;
BgL_arg1554z00_6946 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6948;
BgL_arg1556z00_6948 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6937); 
BgL_arg1555z00_6947 = 
(BgL_arg1556z00_6948-OBJECT_TYPE); } 
BgL_oclassz00_6945 = 
VECTOR_REF(BgL_arg1554z00_6946,BgL_arg1555z00_6947); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6949;
BgL__ortest_1220z00_6949 = 
(BgL_classz00_6936==BgL_oclassz00_6945); 
if(BgL__ortest_1220z00_6949)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6944 = BgL__ortest_1220z00_6949; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6950;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6951;
BgL_arg1810z00_6951 = 
(BgL_oclassz00_6945); 
BgL_odepthz00_6950 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6951); } 
if(
(BgL_arg1804z00_6938<BgL_odepthz00_6950))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6952;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6953;
BgL_arg1809z00_6953 = 
(BgL_oclassz00_6945); 
BgL_arg1808z00_6952 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6953, BgL_arg1804z00_6938); } 
BgL_res2618z00_6944 = 
(BgL_arg1808z00_6952==BgL_classz00_6936); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6944 = ((bool_t)0); } } } } 
BgL_test5039z00_12780 = BgL_res2618z00_6944; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5039z00_12780 = ((bool_t)0)
; } } 
if(BgL_test5039z00_12780)
{ /* Llib/object.scm 260 */
BgL_new1134z00_6954 = 
((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)BgL_new1134z00_5170); }  else 
{ 
 obj_t BgL_auxz00_12822;
BgL_auxz00_12822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9286L), BGl_string3836z00zz__objectz00, BGl_string3752z00zz__objectz00, BgL_new1134z00_5170); 
FAILURE(BgL_auxz00_12822,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1134z00_6954)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1134z00_6954)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1134z00_6954)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1134z00_6954)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1134z00_6954)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1134z00_6954)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12779 = BgL_new1134z00_6954; } 
return 
((obj_t)BgL_auxz00_12779);} } 

}



/* &lambda2048 */
BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BGl_z62lambda2048z62zz__objectz00(obj_t BgL_envz00_5171)
{
{ /* Llib/object.scm 260 */
{ /* Llib/object.scm 260 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1133z00_6955;
BgL_new1133z00_6955 = 
((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 260 */
 long BgL_arg2049z00_6956;
BgL_arg2049z00_6956 = 
BGL_CLASS_NUM(BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1133z00_6955), BgL_arg2049z00_6956); } 
return BgL_new1133z00_6955;} } 

}



/* &lambda2046 */
BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BGl_z62lambda2046z62zz__objectz00(obj_t BgL_envz00_5172, obj_t BgL_fname1127z00_5173, obj_t BgL_location1128z00_5174, obj_t BgL_stack1129z00_5175, obj_t BgL_proc1130z00_5176, obj_t BgL_msg1131z00_5177, obj_t BgL_obj1132z00_5178)
{
{ /* Llib/object.scm 260 */
{ /* Llib/object.scm 260 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1258z00_6957;
{ /* Llib/object.scm 260 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1257z00_6958;
BgL_new1257z00_6958 = 
((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 260 */
 long BgL_arg2047z00_6959;
BgL_arg2047z00_6959 = 
BGL_CLASS_NUM(BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1257z00_6958), BgL_arg2047z00_6959); } 
BgL_new1258z00_6957 = BgL_new1257z00_6958; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1258z00_6957)))->BgL_fnamez00)=((obj_t)BgL_fname1127z00_5173),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1258z00_6957)))->BgL_locationz00)=((obj_t)BgL_location1128z00_5174),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1258z00_6957)))->BgL_stackz00)=((obj_t)BgL_stack1129z00_5175),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1258z00_6957)))->BgL_procz00)=((obj_t)BgL_proc1130z00_5176),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1258z00_6957)))->BgL_msgz00)=((obj_t)BgL_msg1131z00_5177),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1258z00_6957)))->BgL_objz00)=((obj_t)BgL_obj1132z00_5178),BUNSPEC); 
return BgL_new1258z00_6957;} } 

}



/* &<@anonymous:2039> */
obj_t BGl_z62zc3z04anonymousza32039ze3ze5zz__objectz00(obj_t BgL_envz00_5179, obj_t BgL_new1125z00_5180)
{
{ /* Llib/object.scm 259 */
{ 
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_auxz00_12859;
{ /* Llib/object.scm 259 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1125z00_6978;
{ /* Llib/object.scm 259 */
 bool_t BgL_test5046z00_12860;
{ /* Llib/object.scm 259 */
 obj_t BgL_classz00_6960;
BgL_classz00_6960 = BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1125z00_5180))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6961; long BgL_arg1804z00_6962;
BgL_arg1803z00_6961 = 
(BgL_objectz00_bglt)(BgL_new1125z00_5180); 
BgL_arg1804z00_6962 = 
BGL_CLASS_DEPTH(BgL_classz00_6960); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6963;
BgL_idxz00_6963 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6961); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6964;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6965;
BgL_arg1812z00_6965 = 
(BgL_idxz00_6963+BgL_arg1804z00_6962); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6966;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6967;
BgL_aux3145z00_6967 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6967))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6966 = BgL_aux3145z00_6967; }  else 
{ 
 obj_t BgL_auxz00_12871;
BgL_auxz00_12871 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6967); 
FAILURE(BgL_auxz00_12871,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5050z00_12875;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12876;
BgL_tmpz00_12876 = 
VECTOR_LENGTH(BgL_vectorz00_6966); 
BgL_test5050z00_12875 = 
BOUND_CHECK(BgL_arg1812z00_6965, BgL_tmpz00_12876); } 
if(BgL_test5050z00_12875)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6964 = 
VECTOR_REF(BgL_vectorz00_6966,BgL_arg1812z00_6965); }  else 
{ 
 obj_t BgL_auxz00_12880;
BgL_auxz00_12880 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6966, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6966)), 
(int)(BgL_arg1812z00_6965)); 
FAILURE(BgL_auxz00_12880,BFALSE,BFALSE);} } } } 
BgL_test5046z00_12860 = 
(BgL_arg1811z00_6964==BgL_classz00_6960); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6968;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6969;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6970; long BgL_arg1555z00_6971;
BgL_arg1554z00_6970 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6972;
BgL_arg1556z00_6972 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6961); 
BgL_arg1555z00_6971 = 
(BgL_arg1556z00_6972-OBJECT_TYPE); } 
BgL_oclassz00_6969 = 
VECTOR_REF(BgL_arg1554z00_6970,BgL_arg1555z00_6971); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6973;
BgL__ortest_1220z00_6973 = 
(BgL_classz00_6960==BgL_oclassz00_6969); 
if(BgL__ortest_1220z00_6973)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6968 = BgL__ortest_1220z00_6973; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6974;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6975;
BgL_arg1810z00_6975 = 
(BgL_oclassz00_6969); 
BgL_odepthz00_6974 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6975); } 
if(
(BgL_arg1804z00_6962<BgL_odepthz00_6974))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_6976;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_6977;
BgL_arg1809z00_6977 = 
(BgL_oclassz00_6969); 
BgL_arg1808z00_6976 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_6977, BgL_arg1804z00_6962); } 
BgL_res2618z00_6968 = 
(BgL_arg1808z00_6976==BgL_classz00_6960); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6968 = ((bool_t)0); } } } } 
BgL_test5046z00_12860 = BgL_res2618z00_6968; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5046z00_12860 = ((bool_t)0)
; } } 
if(BgL_test5046z00_12860)
{ /* Llib/object.scm 259 */
BgL_new1125z00_6978 = 
((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)BgL_new1125z00_5180); }  else 
{ 
 obj_t BgL_auxz00_12902;
BgL_auxz00_12902 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9239L), BGl_string3837z00zz__objectz00, BGl_string3747z00zz__objectz00, BgL_new1125z00_5180); 
FAILURE(BgL_auxz00_12902,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1125z00_6978)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1125z00_6978)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1125z00_6978)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1125z00_6978)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1125z00_6978)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1125z00_6978)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12859 = BgL_new1125z00_6978; } 
return 
((obj_t)BgL_auxz00_12859);} } 

}



/* &lambda2037 */
BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BGl_z62lambda2037z62zz__objectz00(obj_t BgL_envz00_5181)
{
{ /* Llib/object.scm 259 */
{ /* Llib/object.scm 259 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1124z00_6979;
BgL_new1124z00_6979 = 
((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 259 */
 long BgL_arg2038z00_6980;
BgL_arg2038z00_6980 = 
BGL_CLASS_NUM(BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1124z00_6979), BgL_arg2038z00_6980); } 
return BgL_new1124z00_6979;} } 

}



/* &lambda2035 */
BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BGl_z62lambda2035z62zz__objectz00(obj_t BgL_envz00_5182, obj_t BgL_fname1118z00_5183, obj_t BgL_location1119z00_5184, obj_t BgL_stack1120z00_5185, obj_t BgL_proc1121z00_5186, obj_t BgL_msg1122z00_5187, obj_t BgL_obj1123z00_5188)
{
{ /* Llib/object.scm 259 */
{ /* Llib/object.scm 259 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1256z00_6981;
{ /* Llib/object.scm 259 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1255z00_6982;
BgL_new1255z00_6982 = 
((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 259 */
 long BgL_arg2036z00_6983;
BgL_arg2036z00_6983 = 
BGL_CLASS_NUM(BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1255z00_6982), BgL_arg2036z00_6983); } 
BgL_new1256z00_6981 = BgL_new1255z00_6982; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1256z00_6981)))->BgL_fnamez00)=((obj_t)BgL_fname1118z00_5183),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1256z00_6981)))->BgL_locationz00)=((obj_t)BgL_location1119z00_5184),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1256z00_6981)))->BgL_stackz00)=((obj_t)BgL_stack1120z00_5185),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1256z00_6981)))->BgL_procz00)=((obj_t)BgL_proc1121z00_5186),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1256z00_6981)))->BgL_msgz00)=((obj_t)BgL_msg1122z00_5187),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1256z00_6981)))->BgL_objz00)=((obj_t)BgL_obj1123z00_5188),BUNSPEC); 
return BgL_new1256z00_6981;} } 

}



/* &<@anonymous:2027> */
obj_t BGl_z62zc3z04anonymousza32027ze3ze5zz__objectz00(obj_t BgL_envz00_5189, obj_t BgL_new1116z00_5190)
{
{ /* Llib/object.scm 258 */
{ 
 BgL_z62iozd2parsezd2errorz62_bglt BgL_auxz00_12939;
{ /* Llib/object.scm 258 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1116z00_7002;
{ /* Llib/object.scm 258 */
 bool_t BgL_test5053z00_12940;
{ /* Llib/object.scm 258 */
 obj_t BgL_classz00_6984;
BgL_classz00_6984 = BGl_z62iozd2parsezd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1116z00_5190))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_6985; long BgL_arg1804z00_6986;
BgL_arg1803z00_6985 = 
(BgL_objectz00_bglt)(BgL_new1116z00_5190); 
BgL_arg1804z00_6986 = 
BGL_CLASS_DEPTH(BgL_classz00_6984); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_6987;
BgL_idxz00_6987 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_6985); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_6988;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_6989;
BgL_arg1812z00_6989 = 
(BgL_idxz00_6987+BgL_arg1804z00_6986); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_6990;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_6991;
BgL_aux3145z00_6991 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_6991))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_6990 = BgL_aux3145z00_6991; }  else 
{ 
 obj_t BgL_auxz00_12951;
BgL_auxz00_12951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_6991); 
FAILURE(BgL_auxz00_12951,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5057z00_12955;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_12956;
BgL_tmpz00_12956 = 
VECTOR_LENGTH(BgL_vectorz00_6990); 
BgL_test5057z00_12955 = 
BOUND_CHECK(BgL_arg1812z00_6989, BgL_tmpz00_12956); } 
if(BgL_test5057z00_12955)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_6988 = 
VECTOR_REF(BgL_vectorz00_6990,BgL_arg1812z00_6989); }  else 
{ 
 obj_t BgL_auxz00_12960;
BgL_auxz00_12960 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_6990, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_6990)), 
(int)(BgL_arg1812z00_6989)); 
FAILURE(BgL_auxz00_12960,BFALSE,BFALSE);} } } } 
BgL_test5053z00_12940 = 
(BgL_arg1811z00_6988==BgL_classz00_6984); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_6992;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_6993;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_6994; long BgL_arg1555z00_6995;
BgL_arg1554z00_6994 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_6996;
BgL_arg1556z00_6996 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_6985); 
BgL_arg1555z00_6995 = 
(BgL_arg1556z00_6996-OBJECT_TYPE); } 
BgL_oclassz00_6993 = 
VECTOR_REF(BgL_arg1554z00_6994,BgL_arg1555z00_6995); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_6997;
BgL__ortest_1220z00_6997 = 
(BgL_classz00_6984==BgL_oclassz00_6993); 
if(BgL__ortest_1220z00_6997)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_6992 = BgL__ortest_1220z00_6997; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_6998;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_6999;
BgL_arg1810z00_6999 = 
(BgL_oclassz00_6993); 
BgL_odepthz00_6998 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_6999); } 
if(
(BgL_arg1804z00_6986<BgL_odepthz00_6998))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7000;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7001;
BgL_arg1809z00_7001 = 
(BgL_oclassz00_6993); 
BgL_arg1808z00_7000 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7001, BgL_arg1804z00_6986); } 
BgL_res2618z00_6992 = 
(BgL_arg1808z00_7000==BgL_classz00_6984); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_6992 = ((bool_t)0); } } } } 
BgL_test5053z00_12940 = BgL_res2618z00_6992; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5053z00_12940 = ((bool_t)0)
; } } 
if(BgL_test5053z00_12940)
{ /* Llib/object.scm 258 */
BgL_new1116z00_7002 = 
((BgL_z62iozd2parsezd2errorz62_bglt)BgL_new1116z00_5190); }  else 
{ 
 obj_t BgL_auxz00_12982;
BgL_auxz00_12982 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9199L), BGl_string3838z00zz__objectz00, BGl_string3742z00zz__objectz00, BgL_new1116z00_5190); 
FAILURE(BgL_auxz00_12982,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1116z00_7002)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1116z00_7002)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1116z00_7002)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1116z00_7002)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1116z00_7002)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1116z00_7002)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_12939 = BgL_new1116z00_7002; } 
return 
((obj_t)BgL_auxz00_12939);} } 

}



/* &lambda2025 */
BgL_z62iozd2parsezd2errorz62_bglt BGl_z62lambda2025z62zz__objectz00(obj_t BgL_envz00_5191)
{
{ /* Llib/object.scm 258 */
{ /* Llib/object.scm 258 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1115z00_7003;
BgL_new1115z00_7003 = 
((BgL_z62iozd2parsezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2parsezd2errorz62_bgl) ))); 
{ /* Llib/object.scm 258 */
 long BgL_arg2026z00_7004;
BgL_arg2026z00_7004 = 
BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1115z00_7003), BgL_arg2026z00_7004); } 
return BgL_new1115z00_7003;} } 

}



/* &lambda2023 */
BgL_z62iozd2parsezd2errorz62_bglt BGl_z62lambda2023z62zz__objectz00(obj_t BgL_envz00_5192, obj_t BgL_fname1109z00_5193, obj_t BgL_location1110z00_5194, obj_t BgL_stack1111z00_5195, obj_t BgL_proc1112z00_5196, obj_t BgL_msg1113z00_5197, obj_t BgL_obj1114z00_5198)
{
{ /* Llib/object.scm 258 */
{ /* Llib/object.scm 258 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1254z00_7005;
{ /* Llib/object.scm 258 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1253z00_7006;
BgL_new1253z00_7006 = 
((BgL_z62iozd2parsezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2parsezd2errorz62_bgl) ))); 
{ /* Llib/object.scm 258 */
 long BgL_arg2024z00_7007;
BgL_arg2024z00_7007 = 
BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1253z00_7006), BgL_arg2024z00_7007); } 
BgL_new1254z00_7005 = BgL_new1253z00_7006; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1254z00_7005)))->BgL_fnamez00)=((obj_t)BgL_fname1109z00_5193),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1254z00_7005)))->BgL_locationz00)=((obj_t)BgL_location1110z00_5194),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1254z00_7005)))->BgL_stackz00)=((obj_t)BgL_stack1111z00_5195),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1254z00_7005)))->BgL_procz00)=((obj_t)BgL_proc1112z00_5196),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1254z00_7005)))->BgL_msgz00)=((obj_t)BgL_msg1113z00_5197),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1254z00_7005)))->BgL_objz00)=((obj_t)BgL_obj1114z00_5198),BUNSPEC); 
return BgL_new1254z00_7005;} } 

}



/* &<@anonymous:2017> */
obj_t BGl_z62zc3z04anonymousza32017ze3ze5zz__objectz00(obj_t BgL_envz00_5199, obj_t BgL_new1107z00_5200)
{
{ /* Llib/object.scm 257 */
{ 
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_auxz00_13019;
{ /* Llib/object.scm 257 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1107z00_7026;
{ /* Llib/object.scm 257 */
 bool_t BgL_test5060z00_13020;
{ /* Llib/object.scm 257 */
 obj_t BgL_classz00_7008;
BgL_classz00_7008 = BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1107z00_5200))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7009; long BgL_arg1804z00_7010;
BgL_arg1803z00_7009 = 
(BgL_objectz00_bglt)(BgL_new1107z00_5200); 
BgL_arg1804z00_7010 = 
BGL_CLASS_DEPTH(BgL_classz00_7008); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7011;
BgL_idxz00_7011 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7009); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7012;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7013;
BgL_arg1812z00_7013 = 
(BgL_idxz00_7011+BgL_arg1804z00_7010); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7014;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7015;
BgL_aux3145z00_7015 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7015))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7014 = BgL_aux3145z00_7015; }  else 
{ 
 obj_t BgL_auxz00_13031;
BgL_auxz00_13031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7015); 
FAILURE(BgL_auxz00_13031,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5064z00_13035;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13036;
BgL_tmpz00_13036 = 
VECTOR_LENGTH(BgL_vectorz00_7014); 
BgL_test5064z00_13035 = 
BOUND_CHECK(BgL_arg1812z00_7013, BgL_tmpz00_13036); } 
if(BgL_test5064z00_13035)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7012 = 
VECTOR_REF(BgL_vectorz00_7014,BgL_arg1812z00_7013); }  else 
{ 
 obj_t BgL_auxz00_13040;
BgL_auxz00_13040 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7014, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7014)), 
(int)(BgL_arg1812z00_7013)); 
FAILURE(BgL_auxz00_13040,BFALSE,BFALSE);} } } } 
BgL_test5060z00_13020 = 
(BgL_arg1811z00_7012==BgL_classz00_7008); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7016;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7017;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7018; long BgL_arg1555z00_7019;
BgL_arg1554z00_7018 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7020;
BgL_arg1556z00_7020 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7009); 
BgL_arg1555z00_7019 = 
(BgL_arg1556z00_7020-OBJECT_TYPE); } 
BgL_oclassz00_7017 = 
VECTOR_REF(BgL_arg1554z00_7018,BgL_arg1555z00_7019); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7021;
BgL__ortest_1220z00_7021 = 
(BgL_classz00_7008==BgL_oclassz00_7017); 
if(BgL__ortest_1220z00_7021)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7016 = BgL__ortest_1220z00_7021; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7022;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7023;
BgL_arg1810z00_7023 = 
(BgL_oclassz00_7017); 
BgL_odepthz00_7022 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7023); } 
if(
(BgL_arg1804z00_7010<BgL_odepthz00_7022))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7024;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7025;
BgL_arg1809z00_7025 = 
(BgL_oclassz00_7017); 
BgL_arg1808z00_7024 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7025, BgL_arg1804z00_7010); } 
BgL_res2618z00_7016 = 
(BgL_arg1808z00_7024==BgL_classz00_7008); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7016 = ((bool_t)0); } } } } 
BgL_test5060z00_13020 = BgL_res2618z00_7016; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5060z00_13020 = ((bool_t)0)
; } } 
if(BgL_test5060z00_13020)
{ /* Llib/object.scm 257 */
BgL_new1107z00_7026 = 
((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)BgL_new1107z00_5200); }  else 
{ 
 obj_t BgL_auxz00_13062;
BgL_auxz00_13062 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9150L), BGl_string3839z00zz__objectz00, BGl_string3737z00zz__objectz00, BgL_new1107z00_5200); 
FAILURE(BgL_auxz00_13062,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1107z00_7026)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1107z00_7026)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1107z00_7026)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1107z00_7026)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1107z00_7026)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1107z00_7026)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13019 = BgL_new1107z00_7026; } 
return 
((obj_t)BgL_auxz00_13019);} } 

}



/* &lambda2015 */
BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BGl_z62lambda2015z62zz__objectz00(obj_t BgL_envz00_5201)
{
{ /* Llib/object.scm 257 */
{ /* Llib/object.scm 257 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1106z00_7027;
BgL_new1106z00_7027 = 
((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 257 */
 long BgL_arg2016z00_7028;
BgL_arg2016z00_7028 = 
BGL_CLASS_NUM(BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1106z00_7027), BgL_arg2016z00_7028); } 
return BgL_new1106z00_7027;} } 

}



/* &lambda2013 */
BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BGl_z62lambda2013z62zz__objectz00(obj_t BgL_envz00_5202, obj_t BgL_fname1100z00_5203, obj_t BgL_location1101z00_5204, obj_t BgL_stack1102z00_5205, obj_t BgL_proc1103z00_5206, obj_t BgL_msg1104z00_5207, obj_t BgL_obj1105z00_5208)
{
{ /* Llib/object.scm 257 */
{ /* Llib/object.scm 257 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1252z00_7029;
{ /* Llib/object.scm 257 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1251z00_7030;
BgL_new1251z00_7030 = 
((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 257 */
 long BgL_arg2014z00_7031;
BgL_arg2014z00_7031 = 
BGL_CLASS_NUM(BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1251z00_7030), BgL_arg2014z00_7031); } 
BgL_new1252z00_7029 = BgL_new1251z00_7030; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1252z00_7029)))->BgL_fnamez00)=((obj_t)BgL_fname1100z00_5203),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1252z00_7029)))->BgL_locationz00)=((obj_t)BgL_location1101z00_5204),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1252z00_7029)))->BgL_stackz00)=((obj_t)BgL_stack1102z00_5205),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1252z00_7029)))->BgL_procz00)=((obj_t)BgL_proc1103z00_5206),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1252z00_7029)))->BgL_msgz00)=((obj_t)BgL_msg1104z00_5207),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1252z00_7029)))->BgL_objz00)=((obj_t)BgL_obj1105z00_5208),BUNSPEC); 
return BgL_new1252z00_7029;} } 

}



/* &<@anonymous:2007> */
obj_t BGl_z62zc3z04anonymousza32007ze3ze5zz__objectz00(obj_t BgL_envz00_5209, obj_t BgL_new1098z00_5210)
{
{ /* Llib/object.scm 256 */
{ 
 BgL_z62iozd2closedzd2errorz62_bglt BgL_auxz00_13099;
{ /* Llib/object.scm 256 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1098z00_7050;
{ /* Llib/object.scm 256 */
 bool_t BgL_test5067z00_13100;
{ /* Llib/object.scm 256 */
 obj_t BgL_classz00_7032;
BgL_classz00_7032 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1098z00_5210))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7033; long BgL_arg1804z00_7034;
BgL_arg1803z00_7033 = 
(BgL_objectz00_bglt)(BgL_new1098z00_5210); 
BgL_arg1804z00_7034 = 
BGL_CLASS_DEPTH(BgL_classz00_7032); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7035;
BgL_idxz00_7035 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7033); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7036;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7037;
BgL_arg1812z00_7037 = 
(BgL_idxz00_7035+BgL_arg1804z00_7034); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7038;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7039;
BgL_aux3145z00_7039 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7039))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7038 = BgL_aux3145z00_7039; }  else 
{ 
 obj_t BgL_auxz00_13111;
BgL_auxz00_13111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7039); 
FAILURE(BgL_auxz00_13111,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5071z00_13115;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13116;
BgL_tmpz00_13116 = 
VECTOR_LENGTH(BgL_vectorz00_7038); 
BgL_test5071z00_13115 = 
BOUND_CHECK(BgL_arg1812z00_7037, BgL_tmpz00_13116); } 
if(BgL_test5071z00_13115)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7036 = 
VECTOR_REF(BgL_vectorz00_7038,BgL_arg1812z00_7037); }  else 
{ 
 obj_t BgL_auxz00_13120;
BgL_auxz00_13120 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7038, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7038)), 
(int)(BgL_arg1812z00_7037)); 
FAILURE(BgL_auxz00_13120,BFALSE,BFALSE);} } } } 
BgL_test5067z00_13100 = 
(BgL_arg1811z00_7036==BgL_classz00_7032); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7040;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7041;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7042; long BgL_arg1555z00_7043;
BgL_arg1554z00_7042 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7044;
BgL_arg1556z00_7044 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7033); 
BgL_arg1555z00_7043 = 
(BgL_arg1556z00_7044-OBJECT_TYPE); } 
BgL_oclassz00_7041 = 
VECTOR_REF(BgL_arg1554z00_7042,BgL_arg1555z00_7043); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7045;
BgL__ortest_1220z00_7045 = 
(BgL_classz00_7032==BgL_oclassz00_7041); 
if(BgL__ortest_1220z00_7045)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7040 = BgL__ortest_1220z00_7045; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7046;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7047;
BgL_arg1810z00_7047 = 
(BgL_oclassz00_7041); 
BgL_odepthz00_7046 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7047); } 
if(
(BgL_arg1804z00_7034<BgL_odepthz00_7046))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7048;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7049;
BgL_arg1809z00_7049 = 
(BgL_oclassz00_7041); 
BgL_arg1808z00_7048 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7049, BgL_arg1804z00_7034); } 
BgL_res2618z00_7040 = 
(BgL_arg1808z00_7048==BgL_classz00_7032); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7040 = ((bool_t)0); } } } } 
BgL_test5067z00_13100 = BgL_res2618z00_7040; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5067z00_13100 = ((bool_t)0)
; } } 
if(BgL_test5067z00_13100)
{ /* Llib/object.scm 256 */
BgL_new1098z00_7050 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BgL_new1098z00_5210); }  else 
{ 
 obj_t BgL_auxz00_13142;
BgL_auxz00_13142 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9104L), BGl_string3840z00zz__objectz00, BGl_string3732z00zz__objectz00, BgL_new1098z00_5210); 
FAILURE(BgL_auxz00_13142,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1098z00_7050)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1098z00_7050)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1098z00_7050)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1098z00_7050)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1098z00_7050)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1098z00_7050)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13099 = BgL_new1098z00_7050; } 
return 
((obj_t)BgL_auxz00_13099);} } 

}



/* &lambda2005 */
BgL_z62iozd2closedzd2errorz62_bglt BGl_z62lambda2005z62zz__objectz00(obj_t BgL_envz00_5211)
{
{ /* Llib/object.scm 256 */
{ /* Llib/object.scm 256 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1097z00_7051;
BgL_new1097z00_7051 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 256 */
 long BgL_arg2006z00_7052;
BgL_arg2006z00_7052 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1097z00_7051), BgL_arg2006z00_7052); } 
return BgL_new1097z00_7051;} } 

}



/* &lambda2003 */
BgL_z62iozd2closedzd2errorz62_bglt BGl_z62lambda2003z62zz__objectz00(obj_t BgL_envz00_5212, obj_t BgL_fname1091z00_5213, obj_t BgL_location1092z00_5214, obj_t BgL_stack1093z00_5215, obj_t BgL_proc1094z00_5216, obj_t BgL_msg1095z00_5217, obj_t BgL_obj1096z00_5218)
{
{ /* Llib/object.scm 256 */
{ /* Llib/object.scm 256 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1249z00_7053;
{ /* Llib/object.scm 256 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1248z00_7054;
BgL_new1248z00_7054 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 256 */
 long BgL_arg2004z00_7055;
BgL_arg2004z00_7055 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1248z00_7054), BgL_arg2004z00_7055); } 
BgL_new1249z00_7053 = BgL_new1248z00_7054; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1249z00_7053)))->BgL_fnamez00)=((obj_t)BgL_fname1091z00_5213),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1249z00_7053)))->BgL_locationz00)=((obj_t)BgL_location1092z00_5214),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1249z00_7053)))->BgL_stackz00)=((obj_t)BgL_stack1093z00_5215),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1249z00_7053)))->BgL_procz00)=((obj_t)BgL_proc1094z00_5216),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1249z00_7053)))->BgL_msgz00)=((obj_t)BgL_msg1095z00_5217),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1249z00_7053)))->BgL_objz00)=((obj_t)BgL_obj1096z00_5218),BUNSPEC); 
return BgL_new1249z00_7053;} } 

}



/* &<@anonymous:1997> */
obj_t BGl_z62zc3z04anonymousza31997ze3ze5zz__objectz00(obj_t BgL_envz00_5219, obj_t BgL_new1089z00_5220)
{
{ /* Llib/object.scm 255 */
{ 
 BgL_z62iozd2writezd2errorz62_bglt BgL_auxz00_13179;
{ /* Llib/object.scm 255 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_new1089z00_7074;
{ /* Llib/object.scm 255 */
 bool_t BgL_test5074z00_13180;
{ /* Llib/object.scm 255 */
 obj_t BgL_classz00_7056;
BgL_classz00_7056 = BGl_z62iozd2writezd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1089z00_5220))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7057; long BgL_arg1804z00_7058;
BgL_arg1803z00_7057 = 
(BgL_objectz00_bglt)(BgL_new1089z00_5220); 
BgL_arg1804z00_7058 = 
BGL_CLASS_DEPTH(BgL_classz00_7056); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7059;
BgL_idxz00_7059 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7057); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7060;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7061;
BgL_arg1812z00_7061 = 
(BgL_idxz00_7059+BgL_arg1804z00_7058); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7062;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7063;
BgL_aux3145z00_7063 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7063))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7062 = BgL_aux3145z00_7063; }  else 
{ 
 obj_t BgL_auxz00_13191;
BgL_auxz00_13191 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7063); 
FAILURE(BgL_auxz00_13191,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5078z00_13195;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13196;
BgL_tmpz00_13196 = 
VECTOR_LENGTH(BgL_vectorz00_7062); 
BgL_test5078z00_13195 = 
BOUND_CHECK(BgL_arg1812z00_7061, BgL_tmpz00_13196); } 
if(BgL_test5078z00_13195)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7060 = 
VECTOR_REF(BgL_vectorz00_7062,BgL_arg1812z00_7061); }  else 
{ 
 obj_t BgL_auxz00_13200;
BgL_auxz00_13200 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7062, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7062)), 
(int)(BgL_arg1812z00_7061)); 
FAILURE(BgL_auxz00_13200,BFALSE,BFALSE);} } } } 
BgL_test5074z00_13180 = 
(BgL_arg1811z00_7060==BgL_classz00_7056); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7064;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7065;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7066; long BgL_arg1555z00_7067;
BgL_arg1554z00_7066 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7068;
BgL_arg1556z00_7068 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7057); 
BgL_arg1555z00_7067 = 
(BgL_arg1556z00_7068-OBJECT_TYPE); } 
BgL_oclassz00_7065 = 
VECTOR_REF(BgL_arg1554z00_7066,BgL_arg1555z00_7067); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7069;
BgL__ortest_1220z00_7069 = 
(BgL_classz00_7056==BgL_oclassz00_7065); 
if(BgL__ortest_1220z00_7069)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7064 = BgL__ortest_1220z00_7069; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7070;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7071;
BgL_arg1810z00_7071 = 
(BgL_oclassz00_7065); 
BgL_odepthz00_7070 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7071); } 
if(
(BgL_arg1804z00_7058<BgL_odepthz00_7070))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7072;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7073;
BgL_arg1809z00_7073 = 
(BgL_oclassz00_7065); 
BgL_arg1808z00_7072 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7073, BgL_arg1804z00_7058); } 
BgL_res2618z00_7064 = 
(BgL_arg1808z00_7072==BgL_classz00_7056); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7064 = ((bool_t)0); } } } } 
BgL_test5074z00_13180 = BgL_res2618z00_7064; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5074z00_13180 = ((bool_t)0)
; } } 
if(BgL_test5074z00_13180)
{ /* Llib/object.scm 255 */
BgL_new1089z00_7074 = 
((BgL_z62iozd2writezd2errorz62_bglt)BgL_new1089z00_5220); }  else 
{ 
 obj_t BgL_auxz00_13222;
BgL_auxz00_13222 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9059L), BGl_string3841z00zz__objectz00, BGl_string3727z00zz__objectz00, BgL_new1089z00_5220); 
FAILURE(BgL_auxz00_13222,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1089z00_7074)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1089z00_7074)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1089z00_7074)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1089z00_7074)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1089z00_7074)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1089z00_7074)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13179 = BgL_new1089z00_7074; } 
return 
((obj_t)BgL_auxz00_13179);} } 

}



/* &lambda1995 */
BgL_z62iozd2writezd2errorz62_bglt BGl_z62lambda1995z62zz__objectz00(obj_t BgL_envz00_5221)
{
{ /* Llib/object.scm 255 */
{ /* Llib/object.scm 255 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_new1088z00_7075;
BgL_new1088z00_7075 = 
((BgL_z62iozd2writezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2writezd2errorz62_bgl) ))); 
{ /* Llib/object.scm 255 */
 long BgL_arg1996z00_7076;
BgL_arg1996z00_7076 = 
BGL_CLASS_NUM(BGl_z62iozd2writezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1088z00_7075), BgL_arg1996z00_7076); } 
return BgL_new1088z00_7075;} } 

}



/* &lambda1993 */
BgL_z62iozd2writezd2errorz62_bglt BGl_z62lambda1993z62zz__objectz00(obj_t BgL_envz00_5222, obj_t BgL_fname1082z00_5223, obj_t BgL_location1083z00_5224, obj_t BgL_stack1084z00_5225, obj_t BgL_proc1085z00_5226, obj_t BgL_msg1086z00_5227, obj_t BgL_obj1087z00_5228)
{
{ /* Llib/object.scm 255 */
{ /* Llib/object.scm 255 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_new1247z00_7077;
{ /* Llib/object.scm 255 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_new1246z00_7078;
BgL_new1246z00_7078 = 
((BgL_z62iozd2writezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2writezd2errorz62_bgl) ))); 
{ /* Llib/object.scm 255 */
 long BgL_arg1994z00_7079;
BgL_arg1994z00_7079 = 
BGL_CLASS_NUM(BGl_z62iozd2writezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1246z00_7078), BgL_arg1994z00_7079); } 
BgL_new1247z00_7077 = BgL_new1246z00_7078; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1247z00_7077)))->BgL_fnamez00)=((obj_t)BgL_fname1082z00_5223),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1247z00_7077)))->BgL_locationz00)=((obj_t)BgL_location1083z00_5224),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1247z00_7077)))->BgL_stackz00)=((obj_t)BgL_stack1084z00_5225),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1247z00_7077)))->BgL_procz00)=((obj_t)BgL_proc1085z00_5226),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1247z00_7077)))->BgL_msgz00)=((obj_t)BgL_msg1086z00_5227),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1247z00_7077)))->BgL_objz00)=((obj_t)BgL_obj1087z00_5228),BUNSPEC); 
return BgL_new1247z00_7077;} } 

}



/* &<@anonymous:1987> */
obj_t BGl_z62zc3z04anonymousza31987ze3ze5zz__objectz00(obj_t BgL_envz00_5229, obj_t BgL_new1080z00_5230)
{
{ /* Llib/object.scm 254 */
{ 
 BgL_z62iozd2readzd2errorz62_bglt BgL_auxz00_13259;
{ /* Llib/object.scm 254 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_new1080z00_7098;
{ /* Llib/object.scm 254 */
 bool_t BgL_test5081z00_13260;
{ /* Llib/object.scm 254 */
 obj_t BgL_classz00_7080;
BgL_classz00_7080 = BGl_z62iozd2readzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1080z00_5230))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7081; long BgL_arg1804z00_7082;
BgL_arg1803z00_7081 = 
(BgL_objectz00_bglt)(BgL_new1080z00_5230); 
BgL_arg1804z00_7082 = 
BGL_CLASS_DEPTH(BgL_classz00_7080); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7083;
BgL_idxz00_7083 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7081); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7084;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7085;
BgL_arg1812z00_7085 = 
(BgL_idxz00_7083+BgL_arg1804z00_7082); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7086;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7087;
BgL_aux3145z00_7087 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7087))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7086 = BgL_aux3145z00_7087; }  else 
{ 
 obj_t BgL_auxz00_13271;
BgL_auxz00_13271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7087); 
FAILURE(BgL_auxz00_13271,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5085z00_13275;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13276;
BgL_tmpz00_13276 = 
VECTOR_LENGTH(BgL_vectorz00_7086); 
BgL_test5085z00_13275 = 
BOUND_CHECK(BgL_arg1812z00_7085, BgL_tmpz00_13276); } 
if(BgL_test5085z00_13275)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7084 = 
VECTOR_REF(BgL_vectorz00_7086,BgL_arg1812z00_7085); }  else 
{ 
 obj_t BgL_auxz00_13280;
BgL_auxz00_13280 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7086, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7086)), 
(int)(BgL_arg1812z00_7085)); 
FAILURE(BgL_auxz00_13280,BFALSE,BFALSE);} } } } 
BgL_test5081z00_13260 = 
(BgL_arg1811z00_7084==BgL_classz00_7080); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7088;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7089;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7090; long BgL_arg1555z00_7091;
BgL_arg1554z00_7090 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7092;
BgL_arg1556z00_7092 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7081); 
BgL_arg1555z00_7091 = 
(BgL_arg1556z00_7092-OBJECT_TYPE); } 
BgL_oclassz00_7089 = 
VECTOR_REF(BgL_arg1554z00_7090,BgL_arg1555z00_7091); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7093;
BgL__ortest_1220z00_7093 = 
(BgL_classz00_7080==BgL_oclassz00_7089); 
if(BgL__ortest_1220z00_7093)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7088 = BgL__ortest_1220z00_7093; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7094;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7095;
BgL_arg1810z00_7095 = 
(BgL_oclassz00_7089); 
BgL_odepthz00_7094 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7095); } 
if(
(BgL_arg1804z00_7082<BgL_odepthz00_7094))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7096;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7097;
BgL_arg1809z00_7097 = 
(BgL_oclassz00_7089); 
BgL_arg1808z00_7096 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7097, BgL_arg1804z00_7082); } 
BgL_res2618z00_7088 = 
(BgL_arg1808z00_7096==BgL_classz00_7080); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7088 = ((bool_t)0); } } } } 
BgL_test5081z00_13260 = BgL_res2618z00_7088; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5081z00_13260 = ((bool_t)0)
; } } 
if(BgL_test5081z00_13260)
{ /* Llib/object.scm 254 */
BgL_new1080z00_7098 = 
((BgL_z62iozd2readzd2errorz62_bglt)BgL_new1080z00_5230); }  else 
{ 
 obj_t BgL_auxz00_13302;
BgL_auxz00_13302 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(9015L), BGl_string3842z00zz__objectz00, BGl_string3722z00zz__objectz00, BgL_new1080z00_5230); 
FAILURE(BgL_auxz00_13302,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1080z00_7098)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1080z00_7098)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1080z00_7098)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1080z00_7098)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1080z00_7098)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1080z00_7098)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13259 = BgL_new1080z00_7098; } 
return 
((obj_t)BgL_auxz00_13259);} } 

}



/* &lambda1985 */
BgL_z62iozd2readzd2errorz62_bglt BGl_z62lambda1985z62zz__objectz00(obj_t BgL_envz00_5231)
{
{ /* Llib/object.scm 254 */
{ /* Llib/object.scm 254 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_new1079z00_7099;
BgL_new1079z00_7099 = 
((BgL_z62iozd2readzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2readzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 254 */
 long BgL_arg1986z00_7100;
BgL_arg1986z00_7100 = 
BGL_CLASS_NUM(BGl_z62iozd2readzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1079z00_7099), BgL_arg1986z00_7100); } 
return BgL_new1079z00_7099;} } 

}



/* &lambda1983 */
BgL_z62iozd2readzd2errorz62_bglt BGl_z62lambda1983z62zz__objectz00(obj_t BgL_envz00_5232, obj_t BgL_fname1073z00_5233, obj_t BgL_location1074z00_5234, obj_t BgL_stack1075z00_5235, obj_t BgL_proc1076z00_5236, obj_t BgL_msg1077z00_5237, obj_t BgL_obj1078z00_5238)
{
{ /* Llib/object.scm 254 */
{ /* Llib/object.scm 254 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_new1245z00_7101;
{ /* Llib/object.scm 254 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_new1244z00_7102;
BgL_new1244z00_7102 = 
((BgL_z62iozd2readzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2readzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 254 */
 long BgL_arg1984z00_7103;
BgL_arg1984z00_7103 = 
BGL_CLASS_NUM(BGl_z62iozd2readzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1244z00_7102), BgL_arg1984z00_7103); } 
BgL_new1245z00_7101 = BgL_new1244z00_7102; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1245z00_7101)))->BgL_fnamez00)=((obj_t)BgL_fname1073z00_5233),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1245z00_7101)))->BgL_locationz00)=((obj_t)BgL_location1074z00_5234),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1245z00_7101)))->BgL_stackz00)=((obj_t)BgL_stack1075z00_5235),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1245z00_7101)))->BgL_procz00)=((obj_t)BgL_proc1076z00_5236),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1245z00_7101)))->BgL_msgz00)=((obj_t)BgL_msg1077z00_5237),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1245z00_7101)))->BgL_objz00)=((obj_t)BgL_obj1078z00_5238),BUNSPEC); 
return BgL_new1245z00_7101;} } 

}



/* &<@anonymous:1977> */
obj_t BGl_z62zc3z04anonymousza31977ze3ze5zz__objectz00(obj_t BgL_envz00_5239, obj_t BgL_new1071z00_5240)
{
{ /* Llib/object.scm 253 */
{ 
 BgL_z62iozd2portzd2errorz62_bglt BgL_auxz00_13339;
{ /* Llib/object.scm 253 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_new1071z00_7122;
{ /* Llib/object.scm 253 */
 bool_t BgL_test5088z00_13340;
{ /* Llib/object.scm 253 */
 obj_t BgL_classz00_7104;
BgL_classz00_7104 = BGl_z62iozd2portzd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1071z00_5240))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7105; long BgL_arg1804z00_7106;
BgL_arg1803z00_7105 = 
(BgL_objectz00_bglt)(BgL_new1071z00_5240); 
BgL_arg1804z00_7106 = 
BGL_CLASS_DEPTH(BgL_classz00_7104); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7107;
BgL_idxz00_7107 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7105); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7108;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7109;
BgL_arg1812z00_7109 = 
(BgL_idxz00_7107+BgL_arg1804z00_7106); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7110;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7111;
BgL_aux3145z00_7111 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7111))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7110 = BgL_aux3145z00_7111; }  else 
{ 
 obj_t BgL_auxz00_13351;
BgL_auxz00_13351 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7111); 
FAILURE(BgL_auxz00_13351,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5092z00_13355;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13356;
BgL_tmpz00_13356 = 
VECTOR_LENGTH(BgL_vectorz00_7110); 
BgL_test5092z00_13355 = 
BOUND_CHECK(BgL_arg1812z00_7109, BgL_tmpz00_13356); } 
if(BgL_test5092z00_13355)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7108 = 
VECTOR_REF(BgL_vectorz00_7110,BgL_arg1812z00_7109); }  else 
{ 
 obj_t BgL_auxz00_13360;
BgL_auxz00_13360 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7110, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7110)), 
(int)(BgL_arg1812z00_7109)); 
FAILURE(BgL_auxz00_13360,BFALSE,BFALSE);} } } } 
BgL_test5088z00_13340 = 
(BgL_arg1811z00_7108==BgL_classz00_7104); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7112;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7113;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7114; long BgL_arg1555z00_7115;
BgL_arg1554z00_7114 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7116;
BgL_arg1556z00_7116 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7105); 
BgL_arg1555z00_7115 = 
(BgL_arg1556z00_7116-OBJECT_TYPE); } 
BgL_oclassz00_7113 = 
VECTOR_REF(BgL_arg1554z00_7114,BgL_arg1555z00_7115); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7117;
BgL__ortest_1220z00_7117 = 
(BgL_classz00_7104==BgL_oclassz00_7113); 
if(BgL__ortest_1220z00_7117)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7112 = BgL__ortest_1220z00_7117; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7118;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7119;
BgL_arg1810z00_7119 = 
(BgL_oclassz00_7113); 
BgL_odepthz00_7118 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7119); } 
if(
(BgL_arg1804z00_7106<BgL_odepthz00_7118))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7120;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7121;
BgL_arg1809z00_7121 = 
(BgL_oclassz00_7113); 
BgL_arg1808z00_7120 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7121, BgL_arg1804z00_7106); } 
BgL_res2618z00_7112 = 
(BgL_arg1808z00_7120==BgL_classz00_7104); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7112 = ((bool_t)0); } } } } 
BgL_test5088z00_13340 = BgL_res2618z00_7112; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5088z00_13340 = ((bool_t)0)
; } } 
if(BgL_test5088z00_13340)
{ /* Llib/object.scm 253 */
BgL_new1071z00_7122 = 
((BgL_z62iozd2portzd2errorz62_bglt)BgL_new1071z00_5240); }  else 
{ 
 obj_t BgL_auxz00_13382;
BgL_auxz00_13382 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8976L), BGl_string3843z00zz__objectz00, BGl_string3717z00zz__objectz00, BgL_new1071z00_5240); 
FAILURE(BgL_auxz00_13382,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1071z00_7122)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1071z00_7122)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1071z00_7122)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1071z00_7122)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1071z00_7122)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1071z00_7122)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13339 = BgL_new1071z00_7122; } 
return 
((obj_t)BgL_auxz00_13339);} } 

}



/* &lambda1975 */
BgL_z62iozd2portzd2errorz62_bglt BGl_z62lambda1975z62zz__objectz00(obj_t BgL_envz00_5241)
{
{ /* Llib/object.scm 253 */
{ /* Llib/object.scm 253 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_new1070z00_7123;
BgL_new1070z00_7123 = 
((BgL_z62iozd2portzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2portzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 253 */
 long BgL_arg1976z00_7124;
BgL_arg1976z00_7124 = 
BGL_CLASS_NUM(BGl_z62iozd2portzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1070z00_7123), BgL_arg1976z00_7124); } 
return BgL_new1070z00_7123;} } 

}



/* &lambda1973 */
BgL_z62iozd2portzd2errorz62_bglt BGl_z62lambda1973z62zz__objectz00(obj_t BgL_envz00_5242, obj_t BgL_fname1064z00_5243, obj_t BgL_location1065z00_5244, obj_t BgL_stack1066z00_5245, obj_t BgL_proc1067z00_5246, obj_t BgL_msg1068z00_5247, obj_t BgL_obj1069z00_5248)
{
{ /* Llib/object.scm 253 */
{ /* Llib/object.scm 253 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_new1243z00_7125;
{ /* Llib/object.scm 253 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_new1242z00_7126;
BgL_new1242z00_7126 = 
((BgL_z62iozd2portzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2portzd2errorz62_bgl) ))); 
{ /* Llib/object.scm 253 */
 long BgL_arg1974z00_7127;
BgL_arg1974z00_7127 = 
BGL_CLASS_NUM(BGl_z62iozd2portzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1242z00_7126), BgL_arg1974z00_7127); } 
BgL_new1243z00_7125 = BgL_new1242z00_7126; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1243z00_7125)))->BgL_fnamez00)=((obj_t)BgL_fname1064z00_5243),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1243z00_7125)))->BgL_locationz00)=((obj_t)BgL_location1065z00_5244),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1243z00_7125)))->BgL_stackz00)=((obj_t)BgL_stack1066z00_5245),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1243z00_7125)))->BgL_procz00)=((obj_t)BgL_proc1067z00_5246),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1243z00_7125)))->BgL_msgz00)=((obj_t)BgL_msg1068z00_5247),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1243z00_7125)))->BgL_objz00)=((obj_t)BgL_obj1069z00_5248),BUNSPEC); 
return BgL_new1243z00_7125;} } 

}



/* &<@anonymous:1967> */
obj_t BGl_z62zc3z04anonymousza31967ze3ze5zz__objectz00(obj_t BgL_envz00_5249, obj_t BgL_new1062z00_5250)
{
{ /* Llib/object.scm 252 */
{ 
 BgL_z62iozd2errorzb0_bglt BgL_auxz00_13419;
{ /* Llib/object.scm 252 */
 BgL_z62iozd2errorzb0_bglt BgL_new1062z00_7146;
{ /* Llib/object.scm 252 */
 bool_t BgL_test5095z00_13420;
{ /* Llib/object.scm 252 */
 obj_t BgL_classz00_7128;
BgL_classz00_7128 = BGl_z62iozd2errorzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1062z00_5250))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7129; long BgL_arg1804z00_7130;
BgL_arg1803z00_7129 = 
(BgL_objectz00_bglt)(BgL_new1062z00_5250); 
BgL_arg1804z00_7130 = 
BGL_CLASS_DEPTH(BgL_classz00_7128); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7131;
BgL_idxz00_7131 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7129); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7132;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7133;
BgL_arg1812z00_7133 = 
(BgL_idxz00_7131+BgL_arg1804z00_7130); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7134;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7135;
BgL_aux3145z00_7135 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7135))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7134 = BgL_aux3145z00_7135; }  else 
{ 
 obj_t BgL_auxz00_13431;
BgL_auxz00_13431 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7135); 
FAILURE(BgL_auxz00_13431,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5099z00_13435;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13436;
BgL_tmpz00_13436 = 
VECTOR_LENGTH(BgL_vectorz00_7134); 
BgL_test5099z00_13435 = 
BOUND_CHECK(BgL_arg1812z00_7133, BgL_tmpz00_13436); } 
if(BgL_test5099z00_13435)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7132 = 
VECTOR_REF(BgL_vectorz00_7134,BgL_arg1812z00_7133); }  else 
{ 
 obj_t BgL_auxz00_13440;
BgL_auxz00_13440 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7134, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7134)), 
(int)(BgL_arg1812z00_7133)); 
FAILURE(BgL_auxz00_13440,BFALSE,BFALSE);} } } } 
BgL_test5095z00_13420 = 
(BgL_arg1811z00_7132==BgL_classz00_7128); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7136;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7137;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7138; long BgL_arg1555z00_7139;
BgL_arg1554z00_7138 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7140;
BgL_arg1556z00_7140 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7129); 
BgL_arg1555z00_7139 = 
(BgL_arg1556z00_7140-OBJECT_TYPE); } 
BgL_oclassz00_7137 = 
VECTOR_REF(BgL_arg1554z00_7138,BgL_arg1555z00_7139); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7141;
BgL__ortest_1220z00_7141 = 
(BgL_classz00_7128==BgL_oclassz00_7137); 
if(BgL__ortest_1220z00_7141)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7136 = BgL__ortest_1220z00_7141; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7142;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7143;
BgL_arg1810z00_7143 = 
(BgL_oclassz00_7137); 
BgL_odepthz00_7142 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7143); } 
if(
(BgL_arg1804z00_7130<BgL_odepthz00_7142))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7144;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7145;
BgL_arg1809z00_7145 = 
(BgL_oclassz00_7137); 
BgL_arg1808z00_7144 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7145, BgL_arg1804z00_7130); } 
BgL_res2618z00_7136 = 
(BgL_arg1808z00_7144==BgL_classz00_7128); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7136 = ((bool_t)0); } } } } 
BgL_test5095z00_13420 = BgL_res2618z00_7136; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5095z00_13420 = ((bool_t)0)
; } } 
if(BgL_test5095z00_13420)
{ /* Llib/object.scm 252 */
BgL_new1062z00_7146 = 
((BgL_z62iozd2errorzb0_bglt)BgL_new1062z00_5250); }  else 
{ 
 obj_t BgL_auxz00_13462;
BgL_auxz00_13462 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8945L), BGl_string3844z00zz__objectz00, BGl_string3712z00zz__objectz00, BgL_new1062z00_5250); 
FAILURE(BgL_auxz00_13462,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1062z00_7146)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1062z00_7146)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1062z00_7146)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1062z00_7146)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1062z00_7146)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1062z00_7146)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13419 = BgL_new1062z00_7146; } 
return 
((obj_t)BgL_auxz00_13419);} } 

}



/* &lambda1965 */
BgL_z62iozd2errorzb0_bglt BGl_z62lambda1965z62zz__objectz00(obj_t BgL_envz00_5251)
{
{ /* Llib/object.scm 252 */
{ /* Llib/object.scm 252 */
 BgL_z62iozd2errorzb0_bglt BgL_new1061z00_7147;
BgL_new1061z00_7147 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 252 */
 long BgL_arg1966z00_7148;
BgL_arg1966z00_7148 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1061z00_7147), BgL_arg1966z00_7148); } 
return BgL_new1061z00_7147;} } 

}



/* &lambda1963 */
BgL_z62iozd2errorzb0_bglt BGl_z62lambda1963z62zz__objectz00(obj_t BgL_envz00_5252, obj_t BgL_fname1054z00_5253, obj_t BgL_location1055z00_5254, obj_t BgL_stack1056z00_5255, obj_t BgL_proc1057z00_5256, obj_t BgL_msg1058z00_5257, obj_t BgL_obj1059z00_5258)
{
{ /* Llib/object.scm 252 */
{ /* Llib/object.scm 252 */
 BgL_z62iozd2errorzb0_bglt BgL_new1240z00_7149;
{ /* Llib/object.scm 252 */
 BgL_z62iozd2errorzb0_bglt BgL_new1239z00_7150;
BgL_new1239z00_7150 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 252 */
 long BgL_arg1964z00_7151;
BgL_arg1964z00_7151 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1239z00_7150), BgL_arg1964z00_7151); } 
BgL_new1240z00_7149 = BgL_new1239z00_7150; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1240z00_7149)))->BgL_fnamez00)=((obj_t)BgL_fname1054z00_5253),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1240z00_7149)))->BgL_locationz00)=((obj_t)BgL_location1055z00_5254),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1240z00_7149)))->BgL_stackz00)=((obj_t)BgL_stack1056z00_5255),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1240z00_7149)))->BgL_procz00)=((obj_t)BgL_proc1057z00_5256),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1240z00_7149)))->BgL_msgz00)=((obj_t)BgL_msg1058z00_5257),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1240z00_7149)))->BgL_objz00)=((obj_t)BgL_obj1059z00_5258),BUNSPEC); 
return BgL_new1240z00_7149;} } 

}



/* &<@anonymous:1952> */
obj_t BGl_z62zc3z04anonymousza31952ze3ze5zz__objectz00(obj_t BgL_envz00_5259, obj_t BgL_new1052z00_5260)
{
{ /* Llib/object.scm 250 */
{ 
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_auxz00_13499;
{ /* Llib/object.scm 250 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1052z00_7170;
{ /* Llib/object.scm 250 */
 bool_t BgL_test5102z00_13500;
{ /* Llib/object.scm 250 */
 obj_t BgL_classz00_7152;
BgL_classz00_7152 = BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1052z00_5260))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7153; long BgL_arg1804z00_7154;
BgL_arg1803z00_7153 = 
(BgL_objectz00_bglt)(BgL_new1052z00_5260); 
BgL_arg1804z00_7154 = 
BGL_CLASS_DEPTH(BgL_classz00_7152); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7155;
BgL_idxz00_7155 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7153); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7156;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7157;
BgL_arg1812z00_7157 = 
(BgL_idxz00_7155+BgL_arg1804z00_7154); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7158;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7159;
BgL_aux3145z00_7159 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7159))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7158 = BgL_aux3145z00_7159; }  else 
{ 
 obj_t BgL_auxz00_13511;
BgL_auxz00_13511 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7159); 
FAILURE(BgL_auxz00_13511,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5106z00_13515;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13516;
BgL_tmpz00_13516 = 
VECTOR_LENGTH(BgL_vectorz00_7158); 
BgL_test5106z00_13515 = 
BOUND_CHECK(BgL_arg1812z00_7157, BgL_tmpz00_13516); } 
if(BgL_test5106z00_13515)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7156 = 
VECTOR_REF(BgL_vectorz00_7158,BgL_arg1812z00_7157); }  else 
{ 
 obj_t BgL_auxz00_13520;
BgL_auxz00_13520 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7158, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7158)), 
(int)(BgL_arg1812z00_7157)); 
FAILURE(BgL_auxz00_13520,BFALSE,BFALSE);} } } } 
BgL_test5102z00_13500 = 
(BgL_arg1811z00_7156==BgL_classz00_7152); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7160;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7161;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7162; long BgL_arg1555z00_7163;
BgL_arg1554z00_7162 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7164;
BgL_arg1556z00_7164 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7153); 
BgL_arg1555z00_7163 = 
(BgL_arg1556z00_7164-OBJECT_TYPE); } 
BgL_oclassz00_7161 = 
VECTOR_REF(BgL_arg1554z00_7162,BgL_arg1555z00_7163); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7165;
BgL__ortest_1220z00_7165 = 
(BgL_classz00_7152==BgL_oclassz00_7161); 
if(BgL__ortest_1220z00_7165)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7160 = BgL__ortest_1220z00_7165; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7166;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7167;
BgL_arg1810z00_7167 = 
(BgL_oclassz00_7161); 
BgL_odepthz00_7166 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7167); } 
if(
(BgL_arg1804z00_7154<BgL_odepthz00_7166))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7168;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7169;
BgL_arg1809z00_7169 = 
(BgL_oclassz00_7161); 
BgL_arg1808z00_7168 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7169, BgL_arg1804z00_7154); } 
BgL_res2618z00_7160 = 
(BgL_arg1808z00_7168==BgL_classz00_7152); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7160 = ((bool_t)0); } } } } 
BgL_test5102z00_13500 = BgL_res2618z00_7160; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5102z00_13500 = ((bool_t)0)
; } } 
if(BgL_test5102z00_13500)
{ /* Llib/object.scm 250 */
BgL_new1052z00_7170 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BgL_new1052z00_5260); }  else 
{ 
 obj_t BgL_auxz00_13542;
BgL_auxz00_13542 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8871L), BGl_string3845z00zz__objectz00, BGl_string3707z00zz__objectz00, BgL_new1052z00_5260); 
FAILURE(BgL_auxz00_13542,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1052z00_7170)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1052z00_7170)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1052z00_7170)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1052z00_7170)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1052z00_7170)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1052z00_7170)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)COBJECT(BgL_new1052z00_7170))->BgL_indexz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13499 = BgL_new1052z00_7170; } 
return 
((obj_t)BgL_auxz00_13499);} } 

}



/* &lambda1950 */
BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BGl_z62lambda1950z62zz__objectz00(obj_t BgL_envz00_5261)
{
{ /* Llib/object.scm 250 */
{ /* Llib/object.scm 250 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1051z00_7171;
BgL_new1051z00_7171 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl) ))); 
{ /* Llib/object.scm 250 */
 long BgL_arg1951z00_7172;
BgL_arg1951z00_7172 = 
BGL_CLASS_NUM(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1051z00_7171), BgL_arg1951z00_7172); } 
return BgL_new1051z00_7171;} } 

}



/* &lambda1948 */
BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BGl_z62lambda1948z62zz__objectz00(obj_t BgL_envz00_5262, obj_t BgL_fname1044z00_5263, obj_t BgL_location1045z00_5264, obj_t BgL_stack1046z00_5265, obj_t BgL_proc1047z00_5266, obj_t BgL_msg1048z00_5267, obj_t BgL_obj1049z00_5268, obj_t BgL_index1050z00_5269)
{
{ /* Llib/object.scm 250 */
{ /* Llib/object.scm 250 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1238z00_7173;
{ /* Llib/object.scm 250 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1237z00_7174;
BgL_new1237z00_7174 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl) ))); 
{ /* Llib/object.scm 250 */
 long BgL_arg1949z00_7175;
BgL_arg1949z00_7175 = 
BGL_CLASS_NUM(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1237z00_7174), BgL_arg1949z00_7175); } 
BgL_new1238z00_7173 = BgL_new1237z00_7174; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1238z00_7173)))->BgL_fnamez00)=((obj_t)BgL_fname1044z00_5263),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1238z00_7173)))->BgL_locationz00)=((obj_t)BgL_location1045z00_5264),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1238z00_7173)))->BgL_stackz00)=((obj_t)BgL_stack1046z00_5265),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1238z00_7173)))->BgL_procz00)=((obj_t)BgL_proc1047z00_5266),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1238z00_7173)))->BgL_msgz00)=((obj_t)BgL_msg1048z00_5267),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1238z00_7173)))->BgL_objz00)=((obj_t)BgL_obj1049z00_5268),BUNSPEC); 
((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)COBJECT(BgL_new1238z00_7173))->BgL_indexz00)=((obj_t)BgL_index1050z00_5269),BUNSPEC); 
return BgL_new1238z00_7173;} } 

}



/* &lambda1957 */
obj_t BGl_z62lambda1957z62zz__objectz00(obj_t BgL_envz00_5270, obj_t BgL_oz00_5271, obj_t BgL_vz00_5272)
{
{ /* Llib/object.scm 250 */
{ /* Llib/object.scm 250 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_oz00_7194;
{ /* Llib/object.scm 250 */
 bool_t BgL_test5109z00_13581;
{ /* Llib/object.scm 250 */
 obj_t BgL_classz00_7176;
BgL_classz00_7176 = BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5271))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7177; long BgL_arg1804z00_7178;
BgL_arg1803z00_7177 = 
(BgL_objectz00_bglt)(BgL_oz00_5271); 
BgL_arg1804z00_7178 = 
BGL_CLASS_DEPTH(BgL_classz00_7176); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7179;
BgL_idxz00_7179 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7177); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7180;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7181;
BgL_arg1812z00_7181 = 
(BgL_idxz00_7179+BgL_arg1804z00_7178); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7182;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7183;
BgL_aux3145z00_7183 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7183))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7182 = BgL_aux3145z00_7183; }  else 
{ 
 obj_t BgL_auxz00_13592;
BgL_auxz00_13592 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7183); 
FAILURE(BgL_auxz00_13592,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5113z00_13596;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13597;
BgL_tmpz00_13597 = 
VECTOR_LENGTH(BgL_vectorz00_7182); 
BgL_test5113z00_13596 = 
BOUND_CHECK(BgL_arg1812z00_7181, BgL_tmpz00_13597); } 
if(BgL_test5113z00_13596)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7180 = 
VECTOR_REF(BgL_vectorz00_7182,BgL_arg1812z00_7181); }  else 
{ 
 obj_t BgL_auxz00_13601;
BgL_auxz00_13601 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7182, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7182)), 
(int)(BgL_arg1812z00_7181)); 
FAILURE(BgL_auxz00_13601,BFALSE,BFALSE);} } } } 
BgL_test5109z00_13581 = 
(BgL_arg1811z00_7180==BgL_classz00_7176); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7184;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7185;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7186; long BgL_arg1555z00_7187;
BgL_arg1554z00_7186 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7188;
BgL_arg1556z00_7188 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7177); 
BgL_arg1555z00_7187 = 
(BgL_arg1556z00_7188-OBJECT_TYPE); } 
BgL_oclassz00_7185 = 
VECTOR_REF(BgL_arg1554z00_7186,BgL_arg1555z00_7187); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7189;
BgL__ortest_1220z00_7189 = 
(BgL_classz00_7176==BgL_oclassz00_7185); 
if(BgL__ortest_1220z00_7189)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7184 = BgL__ortest_1220z00_7189; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7190;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7191;
BgL_arg1810z00_7191 = 
(BgL_oclassz00_7185); 
BgL_odepthz00_7190 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7191); } 
if(
(BgL_arg1804z00_7178<BgL_odepthz00_7190))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7192;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7193;
BgL_arg1809z00_7193 = 
(BgL_oclassz00_7185); 
BgL_arg1808z00_7192 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7193, BgL_arg1804z00_7178); } 
BgL_res2618z00_7184 = 
(BgL_arg1808z00_7192==BgL_classz00_7176); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7184 = ((bool_t)0); } } } } 
BgL_test5109z00_13581 = BgL_res2618z00_7184; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5109z00_13581 = ((bool_t)0)
; } } 
if(BgL_test5109z00_13581)
{ /* Llib/object.scm 250 */
BgL_oz00_7194 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BgL_oz00_5271); }  else 
{ 
 obj_t BgL_auxz00_13623;
BgL_auxz00_13623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8871L), BGl_string3846z00zz__objectz00, BGl_string3707z00zz__objectz00, BgL_oz00_5271); 
FAILURE(BgL_auxz00_13623,BFALSE,BFALSE);} } 
return 
((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)COBJECT(BgL_oz00_7194))->BgL_indexz00)=((obj_t)BgL_vz00_5272),BUNSPEC);} } 

}



/* &lambda1956 */
obj_t BGl_z62lambda1956z62zz__objectz00(obj_t BgL_envz00_5273, obj_t BgL_oz00_5274)
{
{ /* Llib/object.scm 250 */
{ /* Llib/object.scm 250 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_oz00_7213;
{ /* Llib/object.scm 250 */
 bool_t BgL_test5116z00_13628;
{ /* Llib/object.scm 250 */
 obj_t BgL_classz00_7195;
BgL_classz00_7195 = BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5274))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7196; long BgL_arg1804z00_7197;
BgL_arg1803z00_7196 = 
(BgL_objectz00_bglt)(BgL_oz00_5274); 
BgL_arg1804z00_7197 = 
BGL_CLASS_DEPTH(BgL_classz00_7195); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7198;
BgL_idxz00_7198 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7196); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7199;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7200;
BgL_arg1812z00_7200 = 
(BgL_idxz00_7198+BgL_arg1804z00_7197); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7201;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7202;
BgL_aux3145z00_7202 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7202))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7201 = BgL_aux3145z00_7202; }  else 
{ 
 obj_t BgL_auxz00_13639;
BgL_auxz00_13639 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7202); 
FAILURE(BgL_auxz00_13639,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5120z00_13643;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13644;
BgL_tmpz00_13644 = 
VECTOR_LENGTH(BgL_vectorz00_7201); 
BgL_test5120z00_13643 = 
BOUND_CHECK(BgL_arg1812z00_7200, BgL_tmpz00_13644); } 
if(BgL_test5120z00_13643)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7199 = 
VECTOR_REF(BgL_vectorz00_7201,BgL_arg1812z00_7200); }  else 
{ 
 obj_t BgL_auxz00_13648;
BgL_auxz00_13648 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7201, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7201)), 
(int)(BgL_arg1812z00_7200)); 
FAILURE(BgL_auxz00_13648,BFALSE,BFALSE);} } } } 
BgL_test5116z00_13628 = 
(BgL_arg1811z00_7199==BgL_classz00_7195); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7203;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7204;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7205; long BgL_arg1555z00_7206;
BgL_arg1554z00_7205 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7207;
BgL_arg1556z00_7207 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7196); 
BgL_arg1555z00_7206 = 
(BgL_arg1556z00_7207-OBJECT_TYPE); } 
BgL_oclassz00_7204 = 
VECTOR_REF(BgL_arg1554z00_7205,BgL_arg1555z00_7206); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7208;
BgL__ortest_1220z00_7208 = 
(BgL_classz00_7195==BgL_oclassz00_7204); 
if(BgL__ortest_1220z00_7208)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7203 = BgL__ortest_1220z00_7208; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7209;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7210;
BgL_arg1810z00_7210 = 
(BgL_oclassz00_7204); 
BgL_odepthz00_7209 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7210); } 
if(
(BgL_arg1804z00_7197<BgL_odepthz00_7209))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7211;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7212;
BgL_arg1809z00_7212 = 
(BgL_oclassz00_7204); 
BgL_arg1808z00_7211 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7212, BgL_arg1804z00_7197); } 
BgL_res2618z00_7203 = 
(BgL_arg1808z00_7211==BgL_classz00_7195); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7203 = ((bool_t)0); } } } } 
BgL_test5116z00_13628 = BgL_res2618z00_7203; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5116z00_13628 = ((bool_t)0)
; } } 
if(BgL_test5116z00_13628)
{ /* Llib/object.scm 250 */
BgL_oz00_7213 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BgL_oz00_5274); }  else 
{ 
 obj_t BgL_auxz00_13670;
BgL_auxz00_13670 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8871L), BGl_string3847z00zz__objectz00, BGl_string3707z00zz__objectz00, BgL_oz00_5274); 
FAILURE(BgL_auxz00_13670,BFALSE,BFALSE);} } 
return 
(((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)COBJECT(BgL_oz00_7213))->BgL_indexz00);} } 

}



/* &<@anonymous:1937> */
obj_t BGl_z62zc3z04anonymousza31937ze3ze5zz__objectz00(obj_t BgL_envz00_5275, obj_t BgL_new1042z00_5276)
{
{ /* Llib/object.scm 248 */
{ 
 BgL_z62typezd2errorzb0_bglt BgL_auxz00_13675;
{ /* Llib/object.scm 248 */
 BgL_z62typezd2errorzb0_bglt BgL_new1042z00_7232;
{ /* Llib/object.scm 248 */
 bool_t BgL_test5123z00_13676;
{ /* Llib/object.scm 248 */
 obj_t BgL_classz00_7214;
BgL_classz00_7214 = BGl_z62typezd2errorzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1042z00_5276))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7215; long BgL_arg1804z00_7216;
BgL_arg1803z00_7215 = 
(BgL_objectz00_bglt)(BgL_new1042z00_5276); 
BgL_arg1804z00_7216 = 
BGL_CLASS_DEPTH(BgL_classz00_7214); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7217;
BgL_idxz00_7217 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7215); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7218;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7219;
BgL_arg1812z00_7219 = 
(BgL_idxz00_7217+BgL_arg1804z00_7216); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7220;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7221;
BgL_aux3145z00_7221 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7221))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7220 = BgL_aux3145z00_7221; }  else 
{ 
 obj_t BgL_auxz00_13687;
BgL_auxz00_13687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7221); 
FAILURE(BgL_auxz00_13687,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5127z00_13691;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13692;
BgL_tmpz00_13692 = 
VECTOR_LENGTH(BgL_vectorz00_7220); 
BgL_test5127z00_13691 = 
BOUND_CHECK(BgL_arg1812z00_7219, BgL_tmpz00_13692); } 
if(BgL_test5127z00_13691)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7218 = 
VECTOR_REF(BgL_vectorz00_7220,BgL_arg1812z00_7219); }  else 
{ 
 obj_t BgL_auxz00_13696;
BgL_auxz00_13696 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7220, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7220)), 
(int)(BgL_arg1812z00_7219)); 
FAILURE(BgL_auxz00_13696,BFALSE,BFALSE);} } } } 
BgL_test5123z00_13676 = 
(BgL_arg1811z00_7218==BgL_classz00_7214); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7222;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7223;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7224; long BgL_arg1555z00_7225;
BgL_arg1554z00_7224 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7226;
BgL_arg1556z00_7226 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7215); 
BgL_arg1555z00_7225 = 
(BgL_arg1556z00_7226-OBJECT_TYPE); } 
BgL_oclassz00_7223 = 
VECTOR_REF(BgL_arg1554z00_7224,BgL_arg1555z00_7225); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7227;
BgL__ortest_1220z00_7227 = 
(BgL_classz00_7214==BgL_oclassz00_7223); 
if(BgL__ortest_1220z00_7227)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7222 = BgL__ortest_1220z00_7227; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7228;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7229;
BgL_arg1810z00_7229 = 
(BgL_oclassz00_7223); 
BgL_odepthz00_7228 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7229); } 
if(
(BgL_arg1804z00_7216<BgL_odepthz00_7228))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7230;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7231;
BgL_arg1809z00_7231 = 
(BgL_oclassz00_7223); 
BgL_arg1808z00_7230 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7231, BgL_arg1804z00_7216); } 
BgL_res2618z00_7222 = 
(BgL_arg1808z00_7230==BgL_classz00_7214); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7222 = ((bool_t)0); } } } } 
BgL_test5123z00_13676 = BgL_res2618z00_7222; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5123z00_13676 = ((bool_t)0)
; } } 
if(BgL_test5123z00_13676)
{ /* Llib/object.scm 248 */
BgL_new1042z00_7232 = 
((BgL_z62typezd2errorzb0_bglt)BgL_new1042z00_5276); }  else 
{ 
 obj_t BgL_auxz00_13718;
BgL_auxz00_13718 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8813L), BGl_string3848z00zz__objectz00, BGl_string3698z00zz__objectz00, BgL_new1042z00_5276); 
FAILURE(BgL_auxz00_13718,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1042z00_7232)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1042z00_7232)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1042z00_7232)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1042z00_7232)))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1042z00_7232)))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1042z00_7232)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_new1042z00_7232))->BgL_typez00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13675 = BgL_new1042z00_7232; } 
return 
((obj_t)BgL_auxz00_13675);} } 

}



/* &lambda1935 */
BgL_z62typezd2errorzb0_bglt BGl_z62lambda1935z62zz__objectz00(obj_t BgL_envz00_5277)
{
{ /* Llib/object.scm 248 */
{ /* Llib/object.scm 248 */
 BgL_z62typezd2errorzb0_bglt BgL_new1041z00_7233;
BgL_new1041z00_7233 = 
((BgL_z62typezd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62typezd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 248 */
 long BgL_arg1936z00_7234;
BgL_arg1936z00_7234 = 
BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1041z00_7233), BgL_arg1936z00_7234); } 
return BgL_new1041z00_7233;} } 

}



/* &lambda1933 */
BgL_z62typezd2errorzb0_bglt BGl_z62lambda1933z62zz__objectz00(obj_t BgL_envz00_5278, obj_t BgL_fname1034z00_5279, obj_t BgL_location1035z00_5280, obj_t BgL_stack1036z00_5281, obj_t BgL_proc1037z00_5282, obj_t BgL_msg1038z00_5283, obj_t BgL_obj1039z00_5284, obj_t BgL_type1040z00_5285)
{
{ /* Llib/object.scm 248 */
{ /* Llib/object.scm 248 */
 BgL_z62typezd2errorzb0_bglt BgL_new1236z00_7235;
{ /* Llib/object.scm 248 */
 BgL_z62typezd2errorzb0_bglt BgL_new1235z00_7236;
BgL_new1235z00_7236 = 
((BgL_z62typezd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62typezd2errorzb0_bgl) ))); 
{ /* Llib/object.scm 248 */
 long BgL_arg1934z00_7237;
BgL_arg1934z00_7237 = 
BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1235z00_7236), BgL_arg1934z00_7237); } 
BgL_new1236z00_7235 = BgL_new1235z00_7236; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1236z00_7235)))->BgL_fnamez00)=((obj_t)BgL_fname1034z00_5279),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1236z00_7235)))->BgL_locationz00)=((obj_t)BgL_location1035z00_5280),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1236z00_7235)))->BgL_stackz00)=((obj_t)BgL_stack1036z00_5281),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1236z00_7235)))->BgL_procz00)=((obj_t)BgL_proc1037z00_5282),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1236z00_7235)))->BgL_msgz00)=((obj_t)BgL_msg1038z00_5283),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1236z00_7235)))->BgL_objz00)=((obj_t)BgL_obj1039z00_5284),BUNSPEC); 
((((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_new1236z00_7235))->BgL_typez00)=((obj_t)BgL_type1040z00_5285),BUNSPEC); 
return BgL_new1236z00_7235;} } 

}



/* &lambda1942 */
obj_t BGl_z62lambda1942z62zz__objectz00(obj_t BgL_envz00_5286, obj_t BgL_oz00_5287, obj_t BgL_vz00_5288)
{
{ /* Llib/object.scm 248 */
{ /* Llib/object.scm 248 */
 BgL_z62typezd2errorzb0_bglt BgL_oz00_7256;
{ /* Llib/object.scm 248 */
 bool_t BgL_test5130z00_13757;
{ /* Llib/object.scm 248 */
 obj_t BgL_classz00_7238;
BgL_classz00_7238 = BGl_z62typezd2errorzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5287))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7239; long BgL_arg1804z00_7240;
BgL_arg1803z00_7239 = 
(BgL_objectz00_bglt)(BgL_oz00_5287); 
BgL_arg1804z00_7240 = 
BGL_CLASS_DEPTH(BgL_classz00_7238); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7241;
BgL_idxz00_7241 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7239); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7242;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7243;
BgL_arg1812z00_7243 = 
(BgL_idxz00_7241+BgL_arg1804z00_7240); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7244;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7245;
BgL_aux3145z00_7245 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7245))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7244 = BgL_aux3145z00_7245; }  else 
{ 
 obj_t BgL_auxz00_13768;
BgL_auxz00_13768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7245); 
FAILURE(BgL_auxz00_13768,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5134z00_13772;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13773;
BgL_tmpz00_13773 = 
VECTOR_LENGTH(BgL_vectorz00_7244); 
BgL_test5134z00_13772 = 
BOUND_CHECK(BgL_arg1812z00_7243, BgL_tmpz00_13773); } 
if(BgL_test5134z00_13772)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7242 = 
VECTOR_REF(BgL_vectorz00_7244,BgL_arg1812z00_7243); }  else 
{ 
 obj_t BgL_auxz00_13777;
BgL_auxz00_13777 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7244, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7244)), 
(int)(BgL_arg1812z00_7243)); 
FAILURE(BgL_auxz00_13777,BFALSE,BFALSE);} } } } 
BgL_test5130z00_13757 = 
(BgL_arg1811z00_7242==BgL_classz00_7238); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7246;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7247;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7248; long BgL_arg1555z00_7249;
BgL_arg1554z00_7248 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7250;
BgL_arg1556z00_7250 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7239); 
BgL_arg1555z00_7249 = 
(BgL_arg1556z00_7250-OBJECT_TYPE); } 
BgL_oclassz00_7247 = 
VECTOR_REF(BgL_arg1554z00_7248,BgL_arg1555z00_7249); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7251;
BgL__ortest_1220z00_7251 = 
(BgL_classz00_7238==BgL_oclassz00_7247); 
if(BgL__ortest_1220z00_7251)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7246 = BgL__ortest_1220z00_7251; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7252;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7253;
BgL_arg1810z00_7253 = 
(BgL_oclassz00_7247); 
BgL_odepthz00_7252 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7253); } 
if(
(BgL_arg1804z00_7240<BgL_odepthz00_7252))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7254;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7255;
BgL_arg1809z00_7255 = 
(BgL_oclassz00_7247); 
BgL_arg1808z00_7254 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7255, BgL_arg1804z00_7240); } 
BgL_res2618z00_7246 = 
(BgL_arg1808z00_7254==BgL_classz00_7238); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7246 = ((bool_t)0); } } } } 
BgL_test5130z00_13757 = BgL_res2618z00_7246; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5130z00_13757 = ((bool_t)0)
; } } 
if(BgL_test5130z00_13757)
{ /* Llib/object.scm 248 */
BgL_oz00_7256 = 
((BgL_z62typezd2errorzb0_bglt)BgL_oz00_5287); }  else 
{ 
 obj_t BgL_auxz00_13799;
BgL_auxz00_13799 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8813L), BGl_string3849z00zz__objectz00, BGl_string3698z00zz__objectz00, BgL_oz00_5287); 
FAILURE(BgL_auxz00_13799,BFALSE,BFALSE);} } 
return 
((((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_oz00_7256))->BgL_typez00)=((obj_t)BgL_vz00_5288),BUNSPEC);} } 

}



/* &lambda1941 */
obj_t BGl_z62lambda1941z62zz__objectz00(obj_t BgL_envz00_5289, obj_t BgL_oz00_5290)
{
{ /* Llib/object.scm 248 */
{ /* Llib/object.scm 248 */
 BgL_z62typezd2errorzb0_bglt BgL_oz00_7275;
{ /* Llib/object.scm 248 */
 bool_t BgL_test5137z00_13804;
{ /* Llib/object.scm 248 */
 obj_t BgL_classz00_7257;
BgL_classz00_7257 = BGl_z62typezd2errorzb0zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5290))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7258; long BgL_arg1804z00_7259;
BgL_arg1803z00_7258 = 
(BgL_objectz00_bglt)(BgL_oz00_5290); 
BgL_arg1804z00_7259 = 
BGL_CLASS_DEPTH(BgL_classz00_7257); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7260;
BgL_idxz00_7260 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7258); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7261;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7262;
BgL_arg1812z00_7262 = 
(BgL_idxz00_7260+BgL_arg1804z00_7259); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7263;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7264;
BgL_aux3145z00_7264 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7264))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7263 = BgL_aux3145z00_7264; }  else 
{ 
 obj_t BgL_auxz00_13815;
BgL_auxz00_13815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7264); 
FAILURE(BgL_auxz00_13815,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5141z00_13819;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13820;
BgL_tmpz00_13820 = 
VECTOR_LENGTH(BgL_vectorz00_7263); 
BgL_test5141z00_13819 = 
BOUND_CHECK(BgL_arg1812z00_7262, BgL_tmpz00_13820); } 
if(BgL_test5141z00_13819)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7261 = 
VECTOR_REF(BgL_vectorz00_7263,BgL_arg1812z00_7262); }  else 
{ 
 obj_t BgL_auxz00_13824;
BgL_auxz00_13824 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7263, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7263)), 
(int)(BgL_arg1812z00_7262)); 
FAILURE(BgL_auxz00_13824,BFALSE,BFALSE);} } } } 
BgL_test5137z00_13804 = 
(BgL_arg1811z00_7261==BgL_classz00_7257); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7265;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7266;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7267; long BgL_arg1555z00_7268;
BgL_arg1554z00_7267 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7269;
BgL_arg1556z00_7269 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7258); 
BgL_arg1555z00_7268 = 
(BgL_arg1556z00_7269-OBJECT_TYPE); } 
BgL_oclassz00_7266 = 
VECTOR_REF(BgL_arg1554z00_7267,BgL_arg1555z00_7268); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7270;
BgL__ortest_1220z00_7270 = 
(BgL_classz00_7257==BgL_oclassz00_7266); 
if(BgL__ortest_1220z00_7270)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7265 = BgL__ortest_1220z00_7270; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7271;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7272;
BgL_arg1810z00_7272 = 
(BgL_oclassz00_7266); 
BgL_odepthz00_7271 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7272); } 
if(
(BgL_arg1804z00_7259<BgL_odepthz00_7271))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7273;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7274;
BgL_arg1809z00_7274 = 
(BgL_oclassz00_7266); 
BgL_arg1808z00_7273 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7274, BgL_arg1804z00_7259); } 
BgL_res2618z00_7265 = 
(BgL_arg1808z00_7273==BgL_classz00_7257); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7265 = ((bool_t)0); } } } } 
BgL_test5137z00_13804 = BgL_res2618z00_7265; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5137z00_13804 = ((bool_t)0)
; } } 
if(BgL_test5137z00_13804)
{ /* Llib/object.scm 248 */
BgL_oz00_7275 = 
((BgL_z62typezd2errorzb0_bglt)BgL_oz00_5290); }  else 
{ 
 obj_t BgL_auxz00_13846;
BgL_auxz00_13846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8813L), BGl_string3850z00zz__objectz00, BGl_string3698z00zz__objectz00, BgL_oz00_5290); 
FAILURE(BgL_auxz00_13846,BFALSE,BFALSE);} } 
return 
(((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_oz00_7275))->BgL_typez00);} } 

}



/* &<@anonymous:1907> */
obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__objectz00(obj_t BgL_envz00_5291, obj_t BgL_new1032z00_5292)
{
{ /* Llib/object.scm 244 */
{ 
 BgL_z62errorz62_bglt BgL_auxz00_13851;
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_new1032z00_7294;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5144z00_13852;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7276;
BgL_classz00_7276 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1032z00_5292))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7277; long BgL_arg1804z00_7278;
BgL_arg1803z00_7277 = 
(BgL_objectz00_bglt)(BgL_new1032z00_5292); 
BgL_arg1804z00_7278 = 
BGL_CLASS_DEPTH(BgL_classz00_7276); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7279;
BgL_idxz00_7279 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7277); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7280;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7281;
BgL_arg1812z00_7281 = 
(BgL_idxz00_7279+BgL_arg1804z00_7278); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7282;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7283;
BgL_aux3145z00_7283 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7283))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7282 = BgL_aux3145z00_7283; }  else 
{ 
 obj_t BgL_auxz00_13863;
BgL_auxz00_13863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7283); 
FAILURE(BgL_auxz00_13863,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5148z00_13867;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13868;
BgL_tmpz00_13868 = 
VECTOR_LENGTH(BgL_vectorz00_7282); 
BgL_test5148z00_13867 = 
BOUND_CHECK(BgL_arg1812z00_7281, BgL_tmpz00_13868); } 
if(BgL_test5148z00_13867)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7280 = 
VECTOR_REF(BgL_vectorz00_7282,BgL_arg1812z00_7281); }  else 
{ 
 obj_t BgL_auxz00_13872;
BgL_auxz00_13872 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7282, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7282)), 
(int)(BgL_arg1812z00_7281)); 
FAILURE(BgL_auxz00_13872,BFALSE,BFALSE);} } } } 
BgL_test5144z00_13852 = 
(BgL_arg1811z00_7280==BgL_classz00_7276); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7284;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7285;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7286; long BgL_arg1555z00_7287;
BgL_arg1554z00_7286 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7288;
BgL_arg1556z00_7288 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7277); 
BgL_arg1555z00_7287 = 
(BgL_arg1556z00_7288-OBJECT_TYPE); } 
BgL_oclassz00_7285 = 
VECTOR_REF(BgL_arg1554z00_7286,BgL_arg1555z00_7287); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7289;
BgL__ortest_1220z00_7289 = 
(BgL_classz00_7276==BgL_oclassz00_7285); 
if(BgL__ortest_1220z00_7289)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7284 = BgL__ortest_1220z00_7289; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7290;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7291;
BgL_arg1810z00_7291 = 
(BgL_oclassz00_7285); 
BgL_odepthz00_7290 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7291); } 
if(
(BgL_arg1804z00_7278<BgL_odepthz00_7290))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7292;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7293;
BgL_arg1809z00_7293 = 
(BgL_oclassz00_7285); 
BgL_arg1808z00_7292 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7293, BgL_arg1804z00_7278); } 
BgL_res2618z00_7284 = 
(BgL_arg1808z00_7292==BgL_classz00_7276); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7284 = ((bool_t)0); } } } } 
BgL_test5144z00_13852 = BgL_res2618z00_7284; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5144z00_13852 = ((bool_t)0)
; } } 
if(BgL_test5144z00_13852)
{ /* Llib/object.scm 244 */
BgL_new1032z00_7294 = 
((BgL_z62errorz62_bglt)BgL_new1032z00_5292); }  else 
{ 
 obj_t BgL_auxz00_13894;
BgL_auxz00_13894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3851z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_new1032z00_5292); 
FAILURE(BgL_auxz00_13894,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1032z00_7294)))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1032z00_7294)))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1032z00_7294)))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1032z00_7294))->BgL_procz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1032z00_7294))->BgL_msgz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1032z00_7294))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_13851 = BgL_new1032z00_7294; } 
return 
((obj_t)BgL_auxz00_13851);} } 

}



/* &lambda1904 */
BgL_z62errorz62_bglt BGl_z62lambda1904z62zz__objectz00(obj_t BgL_envz00_5293)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_new1031z00_7295;
BgL_new1031z00_7295 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/object.scm 244 */
 long BgL_arg1906z00_7296;
BgL_arg1906z00_7296 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1031z00_7295), BgL_arg1906z00_7296); } 
return BgL_new1031z00_7295;} } 

}



/* &lambda1902 */
BgL_z62errorz62_bglt BGl_z62lambda1902z62zz__objectz00(obj_t BgL_envz00_5294, obj_t BgL_fname1025z00_5295, obj_t BgL_location1026z00_5296, obj_t BgL_stack1027z00_5297, obj_t BgL_proc1028z00_5298, obj_t BgL_msg1029z00_5299, obj_t BgL_obj1030z00_5300)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_new1234z00_7297;
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_new1233z00_7298;
BgL_new1233z00_7298 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/object.scm 244 */
 long BgL_arg1903z00_7299;
BgL_arg1903z00_7299 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1233z00_7298), BgL_arg1903z00_7299); } 
BgL_new1234z00_7297 = BgL_new1233z00_7298; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1234z00_7297)))->BgL_fnamez00)=((obj_t)BgL_fname1025z00_5295),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1234z00_7297)))->BgL_locationz00)=((obj_t)BgL_location1026z00_5296),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1234z00_7297)))->BgL_stackz00)=((obj_t)BgL_stack1027z00_5297),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1234z00_7297))->BgL_procz00)=((obj_t)BgL_proc1028z00_5298),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1234z00_7297))->BgL_msgz00)=((obj_t)BgL_msg1029z00_5299),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1234z00_7297))->BgL_objz00)=((obj_t)BgL_obj1030z00_5300),BUNSPEC); 
return BgL_new1234z00_7297;} } 

}



/* &lambda1927 */
obj_t BGl_z62lambda1927z62zz__objectz00(obj_t BgL_envz00_5301, obj_t BgL_oz00_5302, obj_t BgL_vz00_5303)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_oz00_7318;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5151z00_13925;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7300;
BgL_classz00_7300 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5302))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7301; long BgL_arg1804z00_7302;
BgL_arg1803z00_7301 = 
(BgL_objectz00_bglt)(BgL_oz00_5302); 
BgL_arg1804z00_7302 = 
BGL_CLASS_DEPTH(BgL_classz00_7300); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7303;
BgL_idxz00_7303 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7301); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7304;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7305;
BgL_arg1812z00_7305 = 
(BgL_idxz00_7303+BgL_arg1804z00_7302); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7306;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7307;
BgL_aux3145z00_7307 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7307))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7306 = BgL_aux3145z00_7307; }  else 
{ 
 obj_t BgL_auxz00_13936;
BgL_auxz00_13936 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7307); 
FAILURE(BgL_auxz00_13936,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5155z00_13940;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13941;
BgL_tmpz00_13941 = 
VECTOR_LENGTH(BgL_vectorz00_7306); 
BgL_test5155z00_13940 = 
BOUND_CHECK(BgL_arg1812z00_7305, BgL_tmpz00_13941); } 
if(BgL_test5155z00_13940)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7304 = 
VECTOR_REF(BgL_vectorz00_7306,BgL_arg1812z00_7305); }  else 
{ 
 obj_t BgL_auxz00_13945;
BgL_auxz00_13945 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7306, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7306)), 
(int)(BgL_arg1812z00_7305)); 
FAILURE(BgL_auxz00_13945,BFALSE,BFALSE);} } } } 
BgL_test5151z00_13925 = 
(BgL_arg1811z00_7304==BgL_classz00_7300); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7308;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7309;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7310; long BgL_arg1555z00_7311;
BgL_arg1554z00_7310 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7312;
BgL_arg1556z00_7312 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7301); 
BgL_arg1555z00_7311 = 
(BgL_arg1556z00_7312-OBJECT_TYPE); } 
BgL_oclassz00_7309 = 
VECTOR_REF(BgL_arg1554z00_7310,BgL_arg1555z00_7311); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7313;
BgL__ortest_1220z00_7313 = 
(BgL_classz00_7300==BgL_oclassz00_7309); 
if(BgL__ortest_1220z00_7313)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7308 = BgL__ortest_1220z00_7313; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7314;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7315;
BgL_arg1810z00_7315 = 
(BgL_oclassz00_7309); 
BgL_odepthz00_7314 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7315); } 
if(
(BgL_arg1804z00_7302<BgL_odepthz00_7314))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7316;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7317;
BgL_arg1809z00_7317 = 
(BgL_oclassz00_7309); 
BgL_arg1808z00_7316 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7317, BgL_arg1804z00_7302); } 
BgL_res2618z00_7308 = 
(BgL_arg1808z00_7316==BgL_classz00_7300); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7308 = ((bool_t)0); } } } } 
BgL_test5151z00_13925 = BgL_res2618z00_7308; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5151z00_13925 = ((bool_t)0)
; } } 
if(BgL_test5151z00_13925)
{ /* Llib/object.scm 244 */
BgL_oz00_7318 = 
((BgL_z62errorz62_bglt)BgL_oz00_5302); }  else 
{ 
 obj_t BgL_auxz00_13967;
BgL_auxz00_13967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3852z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_oz00_5302); 
FAILURE(BgL_auxz00_13967,BFALSE,BFALSE);} } 
return 
((((BgL_z62errorz62_bglt)COBJECT(BgL_oz00_7318))->BgL_objz00)=((obj_t)BgL_vz00_5303),BUNSPEC);} } 

}



/* &lambda1926 */
obj_t BGl_z62lambda1926z62zz__objectz00(obj_t BgL_envz00_5304, obj_t BgL_oz00_5305)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_oz00_7337;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5158z00_13972;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7319;
BgL_classz00_7319 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5305))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7320; long BgL_arg1804z00_7321;
BgL_arg1803z00_7320 = 
(BgL_objectz00_bglt)(BgL_oz00_5305); 
BgL_arg1804z00_7321 = 
BGL_CLASS_DEPTH(BgL_classz00_7319); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7322;
BgL_idxz00_7322 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7320); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7323;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7324;
BgL_arg1812z00_7324 = 
(BgL_idxz00_7322+BgL_arg1804z00_7321); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7325;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7326;
BgL_aux3145z00_7326 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7326))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7325 = BgL_aux3145z00_7326; }  else 
{ 
 obj_t BgL_auxz00_13983;
BgL_auxz00_13983 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7326); 
FAILURE(BgL_auxz00_13983,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5162z00_13987;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_13988;
BgL_tmpz00_13988 = 
VECTOR_LENGTH(BgL_vectorz00_7325); 
BgL_test5162z00_13987 = 
BOUND_CHECK(BgL_arg1812z00_7324, BgL_tmpz00_13988); } 
if(BgL_test5162z00_13987)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7323 = 
VECTOR_REF(BgL_vectorz00_7325,BgL_arg1812z00_7324); }  else 
{ 
 obj_t BgL_auxz00_13992;
BgL_auxz00_13992 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7325, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7325)), 
(int)(BgL_arg1812z00_7324)); 
FAILURE(BgL_auxz00_13992,BFALSE,BFALSE);} } } } 
BgL_test5158z00_13972 = 
(BgL_arg1811z00_7323==BgL_classz00_7319); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7327;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7328;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7329; long BgL_arg1555z00_7330;
BgL_arg1554z00_7329 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7331;
BgL_arg1556z00_7331 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7320); 
BgL_arg1555z00_7330 = 
(BgL_arg1556z00_7331-OBJECT_TYPE); } 
BgL_oclassz00_7328 = 
VECTOR_REF(BgL_arg1554z00_7329,BgL_arg1555z00_7330); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7332;
BgL__ortest_1220z00_7332 = 
(BgL_classz00_7319==BgL_oclassz00_7328); 
if(BgL__ortest_1220z00_7332)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7327 = BgL__ortest_1220z00_7332; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7333;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7334;
BgL_arg1810z00_7334 = 
(BgL_oclassz00_7328); 
BgL_odepthz00_7333 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7334); } 
if(
(BgL_arg1804z00_7321<BgL_odepthz00_7333))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7335;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7336;
BgL_arg1809z00_7336 = 
(BgL_oclassz00_7328); 
BgL_arg1808z00_7335 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7336, BgL_arg1804z00_7321); } 
BgL_res2618z00_7327 = 
(BgL_arg1808z00_7335==BgL_classz00_7319); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7327 = ((bool_t)0); } } } } 
BgL_test5158z00_13972 = BgL_res2618z00_7327; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5158z00_13972 = ((bool_t)0)
; } } 
if(BgL_test5158z00_13972)
{ /* Llib/object.scm 244 */
BgL_oz00_7337 = 
((BgL_z62errorz62_bglt)BgL_oz00_5305); }  else 
{ 
 obj_t BgL_auxz00_14014;
BgL_auxz00_14014 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3853z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_oz00_5305); 
FAILURE(BgL_auxz00_14014,BFALSE,BFALSE);} } 
return 
(((BgL_z62errorz62_bglt)COBJECT(BgL_oz00_7337))->BgL_objz00);} } 

}



/* &lambda1920 */
obj_t BGl_z62lambda1920z62zz__objectz00(obj_t BgL_envz00_5306, obj_t BgL_oz00_5307, obj_t BgL_vz00_5308)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_oz00_7356;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5165z00_14019;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7338;
BgL_classz00_7338 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5307))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7339; long BgL_arg1804z00_7340;
BgL_arg1803z00_7339 = 
(BgL_objectz00_bglt)(BgL_oz00_5307); 
BgL_arg1804z00_7340 = 
BGL_CLASS_DEPTH(BgL_classz00_7338); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7341;
BgL_idxz00_7341 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7339); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7342;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7343;
BgL_arg1812z00_7343 = 
(BgL_idxz00_7341+BgL_arg1804z00_7340); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7344;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7345;
BgL_aux3145z00_7345 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7345))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7344 = BgL_aux3145z00_7345; }  else 
{ 
 obj_t BgL_auxz00_14030;
BgL_auxz00_14030 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7345); 
FAILURE(BgL_auxz00_14030,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5169z00_14034;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14035;
BgL_tmpz00_14035 = 
VECTOR_LENGTH(BgL_vectorz00_7344); 
BgL_test5169z00_14034 = 
BOUND_CHECK(BgL_arg1812z00_7343, BgL_tmpz00_14035); } 
if(BgL_test5169z00_14034)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7342 = 
VECTOR_REF(BgL_vectorz00_7344,BgL_arg1812z00_7343); }  else 
{ 
 obj_t BgL_auxz00_14039;
BgL_auxz00_14039 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7344, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7344)), 
(int)(BgL_arg1812z00_7343)); 
FAILURE(BgL_auxz00_14039,BFALSE,BFALSE);} } } } 
BgL_test5165z00_14019 = 
(BgL_arg1811z00_7342==BgL_classz00_7338); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7346;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7347;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7348; long BgL_arg1555z00_7349;
BgL_arg1554z00_7348 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7350;
BgL_arg1556z00_7350 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7339); 
BgL_arg1555z00_7349 = 
(BgL_arg1556z00_7350-OBJECT_TYPE); } 
BgL_oclassz00_7347 = 
VECTOR_REF(BgL_arg1554z00_7348,BgL_arg1555z00_7349); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7351;
BgL__ortest_1220z00_7351 = 
(BgL_classz00_7338==BgL_oclassz00_7347); 
if(BgL__ortest_1220z00_7351)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7346 = BgL__ortest_1220z00_7351; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7352;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7353;
BgL_arg1810z00_7353 = 
(BgL_oclassz00_7347); 
BgL_odepthz00_7352 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7353); } 
if(
(BgL_arg1804z00_7340<BgL_odepthz00_7352))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7354;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7355;
BgL_arg1809z00_7355 = 
(BgL_oclassz00_7347); 
BgL_arg1808z00_7354 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7355, BgL_arg1804z00_7340); } 
BgL_res2618z00_7346 = 
(BgL_arg1808z00_7354==BgL_classz00_7338); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7346 = ((bool_t)0); } } } } 
BgL_test5165z00_14019 = BgL_res2618z00_7346; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5165z00_14019 = ((bool_t)0)
; } } 
if(BgL_test5165z00_14019)
{ /* Llib/object.scm 244 */
BgL_oz00_7356 = 
((BgL_z62errorz62_bglt)BgL_oz00_5307); }  else 
{ 
 obj_t BgL_auxz00_14061;
BgL_auxz00_14061 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3854z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_oz00_5307); 
FAILURE(BgL_auxz00_14061,BFALSE,BFALSE);} } 
return 
((((BgL_z62errorz62_bglt)COBJECT(BgL_oz00_7356))->BgL_msgz00)=((obj_t)BgL_vz00_5308),BUNSPEC);} } 

}



/* &lambda1919 */
obj_t BGl_z62lambda1919z62zz__objectz00(obj_t BgL_envz00_5309, obj_t BgL_oz00_5310)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_oz00_7375;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5172z00_14066;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7357;
BgL_classz00_7357 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5310))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7358; long BgL_arg1804z00_7359;
BgL_arg1803z00_7358 = 
(BgL_objectz00_bglt)(BgL_oz00_5310); 
BgL_arg1804z00_7359 = 
BGL_CLASS_DEPTH(BgL_classz00_7357); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7360;
BgL_idxz00_7360 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7358); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7361;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7362;
BgL_arg1812z00_7362 = 
(BgL_idxz00_7360+BgL_arg1804z00_7359); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7363;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7364;
BgL_aux3145z00_7364 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7364))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7363 = BgL_aux3145z00_7364; }  else 
{ 
 obj_t BgL_auxz00_14077;
BgL_auxz00_14077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7364); 
FAILURE(BgL_auxz00_14077,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5176z00_14081;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14082;
BgL_tmpz00_14082 = 
VECTOR_LENGTH(BgL_vectorz00_7363); 
BgL_test5176z00_14081 = 
BOUND_CHECK(BgL_arg1812z00_7362, BgL_tmpz00_14082); } 
if(BgL_test5176z00_14081)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7361 = 
VECTOR_REF(BgL_vectorz00_7363,BgL_arg1812z00_7362); }  else 
{ 
 obj_t BgL_auxz00_14086;
BgL_auxz00_14086 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7363, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7363)), 
(int)(BgL_arg1812z00_7362)); 
FAILURE(BgL_auxz00_14086,BFALSE,BFALSE);} } } } 
BgL_test5172z00_14066 = 
(BgL_arg1811z00_7361==BgL_classz00_7357); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7365;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7366;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7367; long BgL_arg1555z00_7368;
BgL_arg1554z00_7367 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7369;
BgL_arg1556z00_7369 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7358); 
BgL_arg1555z00_7368 = 
(BgL_arg1556z00_7369-OBJECT_TYPE); } 
BgL_oclassz00_7366 = 
VECTOR_REF(BgL_arg1554z00_7367,BgL_arg1555z00_7368); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7370;
BgL__ortest_1220z00_7370 = 
(BgL_classz00_7357==BgL_oclassz00_7366); 
if(BgL__ortest_1220z00_7370)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7365 = BgL__ortest_1220z00_7370; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7371;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7372;
BgL_arg1810z00_7372 = 
(BgL_oclassz00_7366); 
BgL_odepthz00_7371 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7372); } 
if(
(BgL_arg1804z00_7359<BgL_odepthz00_7371))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7373;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7374;
BgL_arg1809z00_7374 = 
(BgL_oclassz00_7366); 
BgL_arg1808z00_7373 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7374, BgL_arg1804z00_7359); } 
BgL_res2618z00_7365 = 
(BgL_arg1808z00_7373==BgL_classz00_7357); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7365 = ((bool_t)0); } } } } 
BgL_test5172z00_14066 = BgL_res2618z00_7365; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5172z00_14066 = ((bool_t)0)
; } } 
if(BgL_test5172z00_14066)
{ /* Llib/object.scm 244 */
BgL_oz00_7375 = 
((BgL_z62errorz62_bglt)BgL_oz00_5310); }  else 
{ 
 obj_t BgL_auxz00_14108;
BgL_auxz00_14108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3855z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_oz00_5310); 
FAILURE(BgL_auxz00_14108,BFALSE,BFALSE);} } 
return 
(((BgL_z62errorz62_bglt)COBJECT(BgL_oz00_7375))->BgL_msgz00);} } 

}



/* &lambda1914 */
obj_t BGl_z62lambda1914z62zz__objectz00(obj_t BgL_envz00_5311, obj_t BgL_oz00_5312, obj_t BgL_vz00_5313)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_oz00_7394;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5179z00_14113;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7376;
BgL_classz00_7376 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5312))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7377; long BgL_arg1804z00_7378;
BgL_arg1803z00_7377 = 
(BgL_objectz00_bglt)(BgL_oz00_5312); 
BgL_arg1804z00_7378 = 
BGL_CLASS_DEPTH(BgL_classz00_7376); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7379;
BgL_idxz00_7379 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7377); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7380;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7381;
BgL_arg1812z00_7381 = 
(BgL_idxz00_7379+BgL_arg1804z00_7378); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7382;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7383;
BgL_aux3145z00_7383 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7383))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7382 = BgL_aux3145z00_7383; }  else 
{ 
 obj_t BgL_auxz00_14124;
BgL_auxz00_14124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7383); 
FAILURE(BgL_auxz00_14124,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5183z00_14128;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14129;
BgL_tmpz00_14129 = 
VECTOR_LENGTH(BgL_vectorz00_7382); 
BgL_test5183z00_14128 = 
BOUND_CHECK(BgL_arg1812z00_7381, BgL_tmpz00_14129); } 
if(BgL_test5183z00_14128)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7380 = 
VECTOR_REF(BgL_vectorz00_7382,BgL_arg1812z00_7381); }  else 
{ 
 obj_t BgL_auxz00_14133;
BgL_auxz00_14133 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7382, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7382)), 
(int)(BgL_arg1812z00_7381)); 
FAILURE(BgL_auxz00_14133,BFALSE,BFALSE);} } } } 
BgL_test5179z00_14113 = 
(BgL_arg1811z00_7380==BgL_classz00_7376); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7384;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7385;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7386; long BgL_arg1555z00_7387;
BgL_arg1554z00_7386 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7388;
BgL_arg1556z00_7388 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7377); 
BgL_arg1555z00_7387 = 
(BgL_arg1556z00_7388-OBJECT_TYPE); } 
BgL_oclassz00_7385 = 
VECTOR_REF(BgL_arg1554z00_7386,BgL_arg1555z00_7387); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7389;
BgL__ortest_1220z00_7389 = 
(BgL_classz00_7376==BgL_oclassz00_7385); 
if(BgL__ortest_1220z00_7389)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7384 = BgL__ortest_1220z00_7389; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7390;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7391;
BgL_arg1810z00_7391 = 
(BgL_oclassz00_7385); 
BgL_odepthz00_7390 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7391); } 
if(
(BgL_arg1804z00_7378<BgL_odepthz00_7390))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7392;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7393;
BgL_arg1809z00_7393 = 
(BgL_oclassz00_7385); 
BgL_arg1808z00_7392 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7393, BgL_arg1804z00_7378); } 
BgL_res2618z00_7384 = 
(BgL_arg1808z00_7392==BgL_classz00_7376); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7384 = ((bool_t)0); } } } } 
BgL_test5179z00_14113 = BgL_res2618z00_7384; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5179z00_14113 = ((bool_t)0)
; } } 
if(BgL_test5179z00_14113)
{ /* Llib/object.scm 244 */
BgL_oz00_7394 = 
((BgL_z62errorz62_bglt)BgL_oz00_5312); }  else 
{ 
 obj_t BgL_auxz00_14155;
BgL_auxz00_14155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3856z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_oz00_5312); 
FAILURE(BgL_auxz00_14155,BFALSE,BFALSE);} } 
return 
((((BgL_z62errorz62_bglt)COBJECT(BgL_oz00_7394))->BgL_procz00)=((obj_t)BgL_vz00_5313),BUNSPEC);} } 

}



/* &lambda1913 */
obj_t BGl_z62lambda1913z62zz__objectz00(obj_t BgL_envz00_5314, obj_t BgL_oz00_5315)
{
{ /* Llib/object.scm 244 */
{ /* Llib/object.scm 244 */
 BgL_z62errorz62_bglt BgL_oz00_7413;
{ /* Llib/object.scm 244 */
 bool_t BgL_test5186z00_14160;
{ /* Llib/object.scm 244 */
 obj_t BgL_classz00_7395;
BgL_classz00_7395 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5315))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7396; long BgL_arg1804z00_7397;
BgL_arg1803z00_7396 = 
(BgL_objectz00_bglt)(BgL_oz00_5315); 
BgL_arg1804z00_7397 = 
BGL_CLASS_DEPTH(BgL_classz00_7395); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7398;
BgL_idxz00_7398 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7396); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7399;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7400;
BgL_arg1812z00_7400 = 
(BgL_idxz00_7398+BgL_arg1804z00_7397); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7401;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7402;
BgL_aux3145z00_7402 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7402))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7401 = BgL_aux3145z00_7402; }  else 
{ 
 obj_t BgL_auxz00_14171;
BgL_auxz00_14171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7402); 
FAILURE(BgL_auxz00_14171,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5190z00_14175;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14176;
BgL_tmpz00_14176 = 
VECTOR_LENGTH(BgL_vectorz00_7401); 
BgL_test5190z00_14175 = 
BOUND_CHECK(BgL_arg1812z00_7400, BgL_tmpz00_14176); } 
if(BgL_test5190z00_14175)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7399 = 
VECTOR_REF(BgL_vectorz00_7401,BgL_arg1812z00_7400); }  else 
{ 
 obj_t BgL_auxz00_14180;
BgL_auxz00_14180 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7401, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7401)), 
(int)(BgL_arg1812z00_7400)); 
FAILURE(BgL_auxz00_14180,BFALSE,BFALSE);} } } } 
BgL_test5186z00_14160 = 
(BgL_arg1811z00_7399==BgL_classz00_7395); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7403;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7404;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7405; long BgL_arg1555z00_7406;
BgL_arg1554z00_7405 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7407;
BgL_arg1556z00_7407 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7396); 
BgL_arg1555z00_7406 = 
(BgL_arg1556z00_7407-OBJECT_TYPE); } 
BgL_oclassz00_7404 = 
VECTOR_REF(BgL_arg1554z00_7405,BgL_arg1555z00_7406); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7408;
BgL__ortest_1220z00_7408 = 
(BgL_classz00_7395==BgL_oclassz00_7404); 
if(BgL__ortest_1220z00_7408)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7403 = BgL__ortest_1220z00_7408; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7409;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7410;
BgL_arg1810z00_7410 = 
(BgL_oclassz00_7404); 
BgL_odepthz00_7409 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7410); } 
if(
(BgL_arg1804z00_7397<BgL_odepthz00_7409))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7411;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7412;
BgL_arg1809z00_7412 = 
(BgL_oclassz00_7404); 
BgL_arg1808z00_7411 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7412, BgL_arg1804z00_7397); } 
BgL_res2618z00_7403 = 
(BgL_arg1808z00_7411==BgL_classz00_7395); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7403 = ((bool_t)0); } } } } 
BgL_test5186z00_14160 = BgL_res2618z00_7403; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5186z00_14160 = ((bool_t)0)
; } } 
if(BgL_test5186z00_14160)
{ /* Llib/object.scm 244 */
BgL_oz00_7413 = 
((BgL_z62errorz62_bglt)BgL_oz00_5315); }  else 
{ 
 obj_t BgL_auxz00_14202;
BgL_auxz00_14202 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8720L), BGl_string3857z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_oz00_5315); 
FAILURE(BgL_auxz00_14202,BFALSE,BFALSE);} } 
return 
(((BgL_z62errorz62_bglt)COBJECT(BgL_oz00_7413))->BgL_procz00);} } 

}



/* &<@anonymous:1873> */
obj_t BGl_z62zc3z04anonymousza31873ze3ze5zz__objectz00(obj_t BgL_envz00_5316, obj_t BgL_new1023z00_5317)
{
{ /* Llib/object.scm 239 */
{ 
 BgL_z62exceptionz62_bglt BgL_auxz00_14207;
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_new1023z00_7432;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5193z00_14208;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7414;
BgL_classz00_7414 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1023z00_5317))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7415; long BgL_arg1804z00_7416;
BgL_arg1803z00_7415 = 
(BgL_objectz00_bglt)(BgL_new1023z00_5317); 
BgL_arg1804z00_7416 = 
BGL_CLASS_DEPTH(BgL_classz00_7414); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7417;
BgL_idxz00_7417 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7415); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7418;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7419;
BgL_arg1812z00_7419 = 
(BgL_idxz00_7417+BgL_arg1804z00_7416); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7420;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7421;
BgL_aux3145z00_7421 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7421))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7420 = BgL_aux3145z00_7421; }  else 
{ 
 obj_t BgL_auxz00_14219;
BgL_auxz00_14219 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7421); 
FAILURE(BgL_auxz00_14219,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5197z00_14223;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14224;
BgL_tmpz00_14224 = 
VECTOR_LENGTH(BgL_vectorz00_7420); 
BgL_test5197z00_14223 = 
BOUND_CHECK(BgL_arg1812z00_7419, BgL_tmpz00_14224); } 
if(BgL_test5197z00_14223)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7418 = 
VECTOR_REF(BgL_vectorz00_7420,BgL_arg1812z00_7419); }  else 
{ 
 obj_t BgL_auxz00_14228;
BgL_auxz00_14228 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7420, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7420)), 
(int)(BgL_arg1812z00_7419)); 
FAILURE(BgL_auxz00_14228,BFALSE,BFALSE);} } } } 
BgL_test5193z00_14208 = 
(BgL_arg1811z00_7418==BgL_classz00_7414); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7422;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7423;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7424; long BgL_arg1555z00_7425;
BgL_arg1554z00_7424 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7426;
BgL_arg1556z00_7426 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7415); 
BgL_arg1555z00_7425 = 
(BgL_arg1556z00_7426-OBJECT_TYPE); } 
BgL_oclassz00_7423 = 
VECTOR_REF(BgL_arg1554z00_7424,BgL_arg1555z00_7425); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7427;
BgL__ortest_1220z00_7427 = 
(BgL_classz00_7414==BgL_oclassz00_7423); 
if(BgL__ortest_1220z00_7427)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7422 = BgL__ortest_1220z00_7427; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7428;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7429;
BgL_arg1810z00_7429 = 
(BgL_oclassz00_7423); 
BgL_odepthz00_7428 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7429); } 
if(
(BgL_arg1804z00_7416<BgL_odepthz00_7428))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7430;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7431;
BgL_arg1809z00_7431 = 
(BgL_oclassz00_7423); 
BgL_arg1808z00_7430 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7431, BgL_arg1804z00_7416); } 
BgL_res2618z00_7422 = 
(BgL_arg1808z00_7430==BgL_classz00_7414); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7422 = ((bool_t)0); } } } } 
BgL_test5193z00_14208 = BgL_res2618z00_7422; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5193z00_14208 = ((bool_t)0)
; } } 
if(BgL_test5193z00_14208)
{ /* Llib/object.scm 239 */
BgL_new1023z00_7432 = 
((BgL_z62exceptionz62_bglt)BgL_new1023z00_5317); }  else 
{ 
 obj_t BgL_auxz00_14250;
BgL_auxz00_14250 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3858z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_new1023z00_5317); 
FAILURE(BgL_auxz00_14250,BFALSE,BFALSE);} } 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_new1023z00_7432))->BgL_fnamez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_new1023z00_7432))->BgL_locationz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_new1023z00_7432))->BgL_stackz00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_14207 = BgL_new1023z00_7432; } 
return 
((obj_t)BgL_auxz00_14207);} } 

}



/* &lambda1871 */
BgL_z62exceptionz62_bglt BGl_z62lambda1871z62zz__objectz00(obj_t BgL_envz00_5318)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_new1022z00_7433;
BgL_new1022z00_7433 = 
((BgL_z62exceptionz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62exceptionz62_bgl) ))); 
{ /* Llib/object.scm 239 */
 long BgL_arg1872z00_7434;
BgL_arg1872z00_7434 = 
BGL_CLASS_NUM(BGl_z62exceptionz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1022z00_7433), BgL_arg1872z00_7434); } 
return BgL_new1022z00_7433;} } 

}



/* &lambda1869 */
BgL_z62exceptionz62_bglt BGl_z62lambda1869z62zz__objectz00(obj_t BgL_envz00_5319, obj_t BgL_fname1019z00_5320, obj_t BgL_location1020z00_5321, obj_t BgL_stack1021z00_5322)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_new1232z00_7435;
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_new1231z00_7436;
BgL_new1231z00_7436 = 
((BgL_z62exceptionz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62exceptionz62_bgl) ))); 
{ /* Llib/object.scm 239 */
 long BgL_arg1870z00_7437;
BgL_arg1870z00_7437 = 
BGL_CLASS_NUM(BGl_z62exceptionz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1231z00_7436), BgL_arg1870z00_7437); } 
BgL_new1232z00_7435 = BgL_new1231z00_7436; } 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_new1232z00_7435))->BgL_fnamez00)=((obj_t)BgL_fname1019z00_5320),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_new1232z00_7435))->BgL_locationz00)=((obj_t)BgL_location1020z00_5321),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_new1232z00_7435))->BgL_stackz00)=((obj_t)BgL_stack1021z00_5322),BUNSPEC); 
return BgL_new1232z00_7435;} } 

}



/* &<@anonymous:1895> */
obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__objectz00(obj_t BgL_envz00_5323)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 242 */

{ /* Llib/object.scm 242 */

return 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);} } } 

}



/* &lambda1894 */
obj_t BGl_z62lambda1894z62zz__objectz00(obj_t BgL_envz00_5324, obj_t BgL_oz00_5325, obj_t BgL_vz00_5326)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_oz00_7456;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5200z00_14270;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7438;
BgL_classz00_7438 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5325))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7439; long BgL_arg1804z00_7440;
BgL_arg1803z00_7439 = 
(BgL_objectz00_bglt)(BgL_oz00_5325); 
BgL_arg1804z00_7440 = 
BGL_CLASS_DEPTH(BgL_classz00_7438); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7441;
BgL_idxz00_7441 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7439); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7442;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7443;
BgL_arg1812z00_7443 = 
(BgL_idxz00_7441+BgL_arg1804z00_7440); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7444;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7445;
BgL_aux3145z00_7445 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7445))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7444 = BgL_aux3145z00_7445; }  else 
{ 
 obj_t BgL_auxz00_14281;
BgL_auxz00_14281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7445); 
FAILURE(BgL_auxz00_14281,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5204z00_14285;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14286;
BgL_tmpz00_14286 = 
VECTOR_LENGTH(BgL_vectorz00_7444); 
BgL_test5204z00_14285 = 
BOUND_CHECK(BgL_arg1812z00_7443, BgL_tmpz00_14286); } 
if(BgL_test5204z00_14285)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7442 = 
VECTOR_REF(BgL_vectorz00_7444,BgL_arg1812z00_7443); }  else 
{ 
 obj_t BgL_auxz00_14290;
BgL_auxz00_14290 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7444, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7444)), 
(int)(BgL_arg1812z00_7443)); 
FAILURE(BgL_auxz00_14290,BFALSE,BFALSE);} } } } 
BgL_test5200z00_14270 = 
(BgL_arg1811z00_7442==BgL_classz00_7438); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7446;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7447;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7448; long BgL_arg1555z00_7449;
BgL_arg1554z00_7448 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7450;
BgL_arg1556z00_7450 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7439); 
BgL_arg1555z00_7449 = 
(BgL_arg1556z00_7450-OBJECT_TYPE); } 
BgL_oclassz00_7447 = 
VECTOR_REF(BgL_arg1554z00_7448,BgL_arg1555z00_7449); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7451;
BgL__ortest_1220z00_7451 = 
(BgL_classz00_7438==BgL_oclassz00_7447); 
if(BgL__ortest_1220z00_7451)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7446 = BgL__ortest_1220z00_7451; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7452;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7453;
BgL_arg1810z00_7453 = 
(BgL_oclassz00_7447); 
BgL_odepthz00_7452 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7453); } 
if(
(BgL_arg1804z00_7440<BgL_odepthz00_7452))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7454;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7455;
BgL_arg1809z00_7455 = 
(BgL_oclassz00_7447); 
BgL_arg1808z00_7454 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7455, BgL_arg1804z00_7440); } 
BgL_res2618z00_7446 = 
(BgL_arg1808z00_7454==BgL_classz00_7438); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7446 = ((bool_t)0); } } } } 
BgL_test5200z00_14270 = BgL_res2618z00_7446; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5200z00_14270 = ((bool_t)0)
; } } 
if(BgL_test5200z00_14270)
{ /* Llib/object.scm 239 */
BgL_oz00_7456 = 
((BgL_z62exceptionz62_bglt)BgL_oz00_5325); }  else 
{ 
 obj_t BgL_auxz00_14312;
BgL_auxz00_14312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3859z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_oz00_5325); 
FAILURE(BgL_auxz00_14312,BFALSE,BFALSE);} } 
return 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_oz00_7456))->BgL_stackz00)=((obj_t)BgL_vz00_5326),BUNSPEC);} } 

}



/* &lambda1893 */
obj_t BGl_z62lambda1893z62zz__objectz00(obj_t BgL_envz00_5327, obj_t BgL_oz00_5328)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_oz00_7475;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5207z00_14317;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7457;
BgL_classz00_7457 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5328))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7458; long BgL_arg1804z00_7459;
BgL_arg1803z00_7458 = 
(BgL_objectz00_bglt)(BgL_oz00_5328); 
BgL_arg1804z00_7459 = 
BGL_CLASS_DEPTH(BgL_classz00_7457); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7460;
BgL_idxz00_7460 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7458); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7461;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7462;
BgL_arg1812z00_7462 = 
(BgL_idxz00_7460+BgL_arg1804z00_7459); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7463;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7464;
BgL_aux3145z00_7464 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7464))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7463 = BgL_aux3145z00_7464; }  else 
{ 
 obj_t BgL_auxz00_14328;
BgL_auxz00_14328 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7464); 
FAILURE(BgL_auxz00_14328,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5211z00_14332;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14333;
BgL_tmpz00_14333 = 
VECTOR_LENGTH(BgL_vectorz00_7463); 
BgL_test5211z00_14332 = 
BOUND_CHECK(BgL_arg1812z00_7462, BgL_tmpz00_14333); } 
if(BgL_test5211z00_14332)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7461 = 
VECTOR_REF(BgL_vectorz00_7463,BgL_arg1812z00_7462); }  else 
{ 
 obj_t BgL_auxz00_14337;
BgL_auxz00_14337 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7463, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7463)), 
(int)(BgL_arg1812z00_7462)); 
FAILURE(BgL_auxz00_14337,BFALSE,BFALSE);} } } } 
BgL_test5207z00_14317 = 
(BgL_arg1811z00_7461==BgL_classz00_7457); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7465;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7466;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7467; long BgL_arg1555z00_7468;
BgL_arg1554z00_7467 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7469;
BgL_arg1556z00_7469 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7458); 
BgL_arg1555z00_7468 = 
(BgL_arg1556z00_7469-OBJECT_TYPE); } 
BgL_oclassz00_7466 = 
VECTOR_REF(BgL_arg1554z00_7467,BgL_arg1555z00_7468); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7470;
BgL__ortest_1220z00_7470 = 
(BgL_classz00_7457==BgL_oclassz00_7466); 
if(BgL__ortest_1220z00_7470)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7465 = BgL__ortest_1220z00_7470; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7471;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7472;
BgL_arg1810z00_7472 = 
(BgL_oclassz00_7466); 
BgL_odepthz00_7471 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7472); } 
if(
(BgL_arg1804z00_7459<BgL_odepthz00_7471))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7473;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7474;
BgL_arg1809z00_7474 = 
(BgL_oclassz00_7466); 
BgL_arg1808z00_7473 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7474, BgL_arg1804z00_7459); } 
BgL_res2618z00_7465 = 
(BgL_arg1808z00_7473==BgL_classz00_7457); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7465 = ((bool_t)0); } } } } 
BgL_test5207z00_14317 = BgL_res2618z00_7465; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5207z00_14317 = ((bool_t)0)
; } } 
if(BgL_test5207z00_14317)
{ /* Llib/object.scm 239 */
BgL_oz00_7475 = 
((BgL_z62exceptionz62_bglt)BgL_oz00_5328); }  else 
{ 
 obj_t BgL_auxz00_14359;
BgL_auxz00_14359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3860z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_oz00_5328); 
FAILURE(BgL_auxz00_14359,BFALSE,BFALSE);} } 
return 
(((BgL_z62exceptionz62_bglt)COBJECT(BgL_oz00_7475))->BgL_stackz00);} } 

}



/* &<@anonymous:1888> */
obj_t BGl_z62zc3z04anonymousza31888ze3ze5zz__objectz00(obj_t BgL_envz00_5329)
{
{ /* Llib/object.scm 239 */
return 
BBOOL(((bool_t)0));} 

}



/* &lambda1887 */
obj_t BGl_z62lambda1887z62zz__objectz00(obj_t BgL_envz00_5330, obj_t BgL_oz00_5331, obj_t BgL_vz00_5332)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_oz00_7494;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5214z00_14365;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7476;
BgL_classz00_7476 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5331))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7477; long BgL_arg1804z00_7478;
BgL_arg1803z00_7477 = 
(BgL_objectz00_bglt)(BgL_oz00_5331); 
BgL_arg1804z00_7478 = 
BGL_CLASS_DEPTH(BgL_classz00_7476); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7479;
BgL_idxz00_7479 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7477); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7480;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7481;
BgL_arg1812z00_7481 = 
(BgL_idxz00_7479+BgL_arg1804z00_7478); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7482;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7483;
BgL_aux3145z00_7483 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7483))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7482 = BgL_aux3145z00_7483; }  else 
{ 
 obj_t BgL_auxz00_14376;
BgL_auxz00_14376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7483); 
FAILURE(BgL_auxz00_14376,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5218z00_14380;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14381;
BgL_tmpz00_14381 = 
VECTOR_LENGTH(BgL_vectorz00_7482); 
BgL_test5218z00_14380 = 
BOUND_CHECK(BgL_arg1812z00_7481, BgL_tmpz00_14381); } 
if(BgL_test5218z00_14380)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7480 = 
VECTOR_REF(BgL_vectorz00_7482,BgL_arg1812z00_7481); }  else 
{ 
 obj_t BgL_auxz00_14385;
BgL_auxz00_14385 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7482, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7482)), 
(int)(BgL_arg1812z00_7481)); 
FAILURE(BgL_auxz00_14385,BFALSE,BFALSE);} } } } 
BgL_test5214z00_14365 = 
(BgL_arg1811z00_7480==BgL_classz00_7476); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7484;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7485;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7486; long BgL_arg1555z00_7487;
BgL_arg1554z00_7486 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7488;
BgL_arg1556z00_7488 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7477); 
BgL_arg1555z00_7487 = 
(BgL_arg1556z00_7488-OBJECT_TYPE); } 
BgL_oclassz00_7485 = 
VECTOR_REF(BgL_arg1554z00_7486,BgL_arg1555z00_7487); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7489;
BgL__ortest_1220z00_7489 = 
(BgL_classz00_7476==BgL_oclassz00_7485); 
if(BgL__ortest_1220z00_7489)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7484 = BgL__ortest_1220z00_7489; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7490;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7491;
BgL_arg1810z00_7491 = 
(BgL_oclassz00_7485); 
BgL_odepthz00_7490 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7491); } 
if(
(BgL_arg1804z00_7478<BgL_odepthz00_7490))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7492;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7493;
BgL_arg1809z00_7493 = 
(BgL_oclassz00_7485); 
BgL_arg1808z00_7492 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7493, BgL_arg1804z00_7478); } 
BgL_res2618z00_7484 = 
(BgL_arg1808z00_7492==BgL_classz00_7476); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7484 = ((bool_t)0); } } } } 
BgL_test5214z00_14365 = BgL_res2618z00_7484; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5214z00_14365 = ((bool_t)0)
; } } 
if(BgL_test5214z00_14365)
{ /* Llib/object.scm 239 */
BgL_oz00_7494 = 
((BgL_z62exceptionz62_bglt)BgL_oz00_5331); }  else 
{ 
 obj_t BgL_auxz00_14407;
BgL_auxz00_14407 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3861z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_oz00_5331); 
FAILURE(BgL_auxz00_14407,BFALSE,BFALSE);} } 
return 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_oz00_7494))->BgL_locationz00)=((obj_t)BgL_vz00_5332),BUNSPEC);} } 

}



/* &lambda1886 */
obj_t BGl_z62lambda1886z62zz__objectz00(obj_t BgL_envz00_5333, obj_t BgL_oz00_5334)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_oz00_7513;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5221z00_14412;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7495;
BgL_classz00_7495 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5334))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7496; long BgL_arg1804z00_7497;
BgL_arg1803z00_7496 = 
(BgL_objectz00_bglt)(BgL_oz00_5334); 
BgL_arg1804z00_7497 = 
BGL_CLASS_DEPTH(BgL_classz00_7495); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7498;
BgL_idxz00_7498 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7496); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7499;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7500;
BgL_arg1812z00_7500 = 
(BgL_idxz00_7498+BgL_arg1804z00_7497); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7501;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7502;
BgL_aux3145z00_7502 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7502))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7501 = BgL_aux3145z00_7502; }  else 
{ 
 obj_t BgL_auxz00_14423;
BgL_auxz00_14423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7502); 
FAILURE(BgL_auxz00_14423,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5225z00_14427;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14428;
BgL_tmpz00_14428 = 
VECTOR_LENGTH(BgL_vectorz00_7501); 
BgL_test5225z00_14427 = 
BOUND_CHECK(BgL_arg1812z00_7500, BgL_tmpz00_14428); } 
if(BgL_test5225z00_14427)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7499 = 
VECTOR_REF(BgL_vectorz00_7501,BgL_arg1812z00_7500); }  else 
{ 
 obj_t BgL_auxz00_14432;
BgL_auxz00_14432 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7501, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7501)), 
(int)(BgL_arg1812z00_7500)); 
FAILURE(BgL_auxz00_14432,BFALSE,BFALSE);} } } } 
BgL_test5221z00_14412 = 
(BgL_arg1811z00_7499==BgL_classz00_7495); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7503;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7504;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7505; long BgL_arg1555z00_7506;
BgL_arg1554z00_7505 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7507;
BgL_arg1556z00_7507 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7496); 
BgL_arg1555z00_7506 = 
(BgL_arg1556z00_7507-OBJECT_TYPE); } 
BgL_oclassz00_7504 = 
VECTOR_REF(BgL_arg1554z00_7505,BgL_arg1555z00_7506); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7508;
BgL__ortest_1220z00_7508 = 
(BgL_classz00_7495==BgL_oclassz00_7504); 
if(BgL__ortest_1220z00_7508)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7503 = BgL__ortest_1220z00_7508; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7509;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7510;
BgL_arg1810z00_7510 = 
(BgL_oclassz00_7504); 
BgL_odepthz00_7509 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7510); } 
if(
(BgL_arg1804z00_7497<BgL_odepthz00_7509))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7511;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7512;
BgL_arg1809z00_7512 = 
(BgL_oclassz00_7504); 
BgL_arg1808z00_7511 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7512, BgL_arg1804z00_7497); } 
BgL_res2618z00_7503 = 
(BgL_arg1808z00_7511==BgL_classz00_7495); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7503 = ((bool_t)0); } } } } 
BgL_test5221z00_14412 = BgL_res2618z00_7503; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5221z00_14412 = ((bool_t)0)
; } } 
if(BgL_test5221z00_14412)
{ /* Llib/object.scm 239 */
BgL_oz00_7513 = 
((BgL_z62exceptionz62_bglt)BgL_oz00_5334); }  else 
{ 
 obj_t BgL_auxz00_14454;
BgL_auxz00_14454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3862z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_oz00_5334); 
FAILURE(BgL_auxz00_14454,BFALSE,BFALSE);} } 
return 
(((BgL_z62exceptionz62_bglt)COBJECT(BgL_oz00_7513))->BgL_locationz00);} } 

}



/* &<@anonymous:1880> */
obj_t BGl_z62zc3z04anonymousza31880ze3ze5zz__objectz00(obj_t BgL_envz00_5335)
{
{ /* Llib/object.scm 239 */
return 
BBOOL(((bool_t)0));} 

}



/* &lambda1879 */
obj_t BGl_z62lambda1879z62zz__objectz00(obj_t BgL_envz00_5336, obj_t BgL_oz00_5337, obj_t BgL_vz00_5338)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_oz00_7532;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5228z00_14460;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7514;
BgL_classz00_7514 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5337))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7515; long BgL_arg1804z00_7516;
BgL_arg1803z00_7515 = 
(BgL_objectz00_bglt)(BgL_oz00_5337); 
BgL_arg1804z00_7516 = 
BGL_CLASS_DEPTH(BgL_classz00_7514); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7517;
BgL_idxz00_7517 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7515); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7518;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7519;
BgL_arg1812z00_7519 = 
(BgL_idxz00_7517+BgL_arg1804z00_7516); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7520;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7521;
BgL_aux3145z00_7521 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7521))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7520 = BgL_aux3145z00_7521; }  else 
{ 
 obj_t BgL_auxz00_14471;
BgL_auxz00_14471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7521); 
FAILURE(BgL_auxz00_14471,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5232z00_14475;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14476;
BgL_tmpz00_14476 = 
VECTOR_LENGTH(BgL_vectorz00_7520); 
BgL_test5232z00_14475 = 
BOUND_CHECK(BgL_arg1812z00_7519, BgL_tmpz00_14476); } 
if(BgL_test5232z00_14475)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7518 = 
VECTOR_REF(BgL_vectorz00_7520,BgL_arg1812z00_7519); }  else 
{ 
 obj_t BgL_auxz00_14480;
BgL_auxz00_14480 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7520, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7520)), 
(int)(BgL_arg1812z00_7519)); 
FAILURE(BgL_auxz00_14480,BFALSE,BFALSE);} } } } 
BgL_test5228z00_14460 = 
(BgL_arg1811z00_7518==BgL_classz00_7514); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7522;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7523;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7524; long BgL_arg1555z00_7525;
BgL_arg1554z00_7524 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7526;
BgL_arg1556z00_7526 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7515); 
BgL_arg1555z00_7525 = 
(BgL_arg1556z00_7526-OBJECT_TYPE); } 
BgL_oclassz00_7523 = 
VECTOR_REF(BgL_arg1554z00_7524,BgL_arg1555z00_7525); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7527;
BgL__ortest_1220z00_7527 = 
(BgL_classz00_7514==BgL_oclassz00_7523); 
if(BgL__ortest_1220z00_7527)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7522 = BgL__ortest_1220z00_7527; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7528;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7529;
BgL_arg1810z00_7529 = 
(BgL_oclassz00_7523); 
BgL_odepthz00_7528 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7529); } 
if(
(BgL_arg1804z00_7516<BgL_odepthz00_7528))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7530;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7531;
BgL_arg1809z00_7531 = 
(BgL_oclassz00_7523); 
BgL_arg1808z00_7530 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7531, BgL_arg1804z00_7516); } 
BgL_res2618z00_7522 = 
(BgL_arg1808z00_7530==BgL_classz00_7514); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7522 = ((bool_t)0); } } } } 
BgL_test5228z00_14460 = BgL_res2618z00_7522; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5228z00_14460 = ((bool_t)0)
; } } 
if(BgL_test5228z00_14460)
{ /* Llib/object.scm 239 */
BgL_oz00_7532 = 
((BgL_z62exceptionz62_bglt)BgL_oz00_5337); }  else 
{ 
 obj_t BgL_auxz00_14502;
BgL_auxz00_14502 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3863z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_oz00_5337); 
FAILURE(BgL_auxz00_14502,BFALSE,BFALSE);} } 
return 
((((BgL_z62exceptionz62_bglt)COBJECT(BgL_oz00_7532))->BgL_fnamez00)=((obj_t)BgL_vz00_5338),BUNSPEC);} } 

}



/* &lambda1878 */
obj_t BGl_z62lambda1878z62zz__objectz00(obj_t BgL_envz00_5339, obj_t BgL_oz00_5340)
{
{ /* Llib/object.scm 239 */
{ /* Llib/object.scm 239 */
 BgL_z62exceptionz62_bglt BgL_oz00_7551;
{ /* Llib/object.scm 239 */
 bool_t BgL_test5235z00_14507;
{ /* Llib/object.scm 239 */
 obj_t BgL_classz00_7533;
BgL_classz00_7533 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_5340))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7534; long BgL_arg1804z00_7535;
BgL_arg1803z00_7534 = 
(BgL_objectz00_bglt)(BgL_oz00_5340); 
BgL_arg1804z00_7535 = 
BGL_CLASS_DEPTH(BgL_classz00_7533); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7536;
BgL_idxz00_7536 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7534); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7537;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7538;
BgL_arg1812z00_7538 = 
(BgL_idxz00_7536+BgL_arg1804z00_7535); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7539;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7540;
BgL_aux3145z00_7540 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7540))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7539 = BgL_aux3145z00_7540; }  else 
{ 
 obj_t BgL_auxz00_14518;
BgL_auxz00_14518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7540); 
FAILURE(BgL_auxz00_14518,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5239z00_14522;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14523;
BgL_tmpz00_14523 = 
VECTOR_LENGTH(BgL_vectorz00_7539); 
BgL_test5239z00_14522 = 
BOUND_CHECK(BgL_arg1812z00_7538, BgL_tmpz00_14523); } 
if(BgL_test5239z00_14522)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7537 = 
VECTOR_REF(BgL_vectorz00_7539,BgL_arg1812z00_7538); }  else 
{ 
 obj_t BgL_auxz00_14527;
BgL_auxz00_14527 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7539, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7539)), 
(int)(BgL_arg1812z00_7538)); 
FAILURE(BgL_auxz00_14527,BFALSE,BFALSE);} } } } 
BgL_test5235z00_14507 = 
(BgL_arg1811z00_7537==BgL_classz00_7533); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7541;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7542;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7543; long BgL_arg1555z00_7544;
BgL_arg1554z00_7543 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7545;
BgL_arg1556z00_7545 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7534); 
BgL_arg1555z00_7544 = 
(BgL_arg1556z00_7545-OBJECT_TYPE); } 
BgL_oclassz00_7542 = 
VECTOR_REF(BgL_arg1554z00_7543,BgL_arg1555z00_7544); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7546;
BgL__ortest_1220z00_7546 = 
(BgL_classz00_7533==BgL_oclassz00_7542); 
if(BgL__ortest_1220z00_7546)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7541 = BgL__ortest_1220z00_7546; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7547;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7548;
BgL_arg1810z00_7548 = 
(BgL_oclassz00_7542); 
BgL_odepthz00_7547 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7548); } 
if(
(BgL_arg1804z00_7535<BgL_odepthz00_7547))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7549;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7550;
BgL_arg1809z00_7550 = 
(BgL_oclassz00_7542); 
BgL_arg1808z00_7549 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7550, BgL_arg1804z00_7535); } 
BgL_res2618z00_7541 = 
(BgL_arg1808z00_7549==BgL_classz00_7533); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7541 = ((bool_t)0); } } } } 
BgL_test5235z00_14507 = BgL_res2618z00_7541; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5235z00_14507 = ((bool_t)0)
; } } 
if(BgL_test5235z00_14507)
{ /* Llib/object.scm 239 */
BgL_oz00_7551 = 
((BgL_z62exceptionz62_bglt)BgL_oz00_5340); }  else 
{ 
 obj_t BgL_auxz00_14549;
BgL_auxz00_14549 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8573L), BGl_string3864z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_oz00_5340); 
FAILURE(BgL_auxz00_14549,BFALSE,BFALSE);} } 
return 
(((BgL_z62exceptionz62_bglt)COBJECT(BgL_oz00_7551))->BgL_fnamez00);} } 

}



/* &<@anonymous:1861> */
obj_t BGl_z62zc3z04anonymousza31861ze3ze5zz__objectz00(obj_t BgL_envz00_5341, obj_t BgL_new1017z00_5342)
{
{ /* Llib/object.scm 237 */
{ 
 BgL_z62conditionz62_bglt BgL_auxz00_14554;
{ /* Llib/object.scm 237 */
 BgL_z62conditionz62_bglt BgL_new1017z00_7570;
{ /* Llib/object.scm 237 */
 bool_t BgL_test5242z00_14555;
{ /* Llib/object.scm 237 */
 obj_t BgL_classz00_7552;
BgL_classz00_7552 = BGl_z62conditionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1017z00_5342))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7553; long BgL_arg1804z00_7554;
BgL_arg1803z00_7553 = 
(BgL_objectz00_bglt)(BgL_new1017z00_5342); 
BgL_arg1804z00_7554 = 
BGL_CLASS_DEPTH(BgL_classz00_7552); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7555;
BgL_idxz00_7555 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7553); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7556;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7557;
BgL_arg1812z00_7557 = 
(BgL_idxz00_7555+BgL_arg1804z00_7554); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7558;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7559;
BgL_aux3145z00_7559 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7559))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7558 = BgL_aux3145z00_7559; }  else 
{ 
 obj_t BgL_auxz00_14566;
BgL_auxz00_14566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7559); 
FAILURE(BgL_auxz00_14566,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5246z00_14570;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14571;
BgL_tmpz00_14571 = 
VECTOR_LENGTH(BgL_vectorz00_7558); 
BgL_test5246z00_14570 = 
BOUND_CHECK(BgL_arg1812z00_7557, BgL_tmpz00_14571); } 
if(BgL_test5246z00_14570)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7556 = 
VECTOR_REF(BgL_vectorz00_7558,BgL_arg1812z00_7557); }  else 
{ 
 obj_t BgL_auxz00_14575;
BgL_auxz00_14575 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7558, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7558)), 
(int)(BgL_arg1812z00_7557)); 
FAILURE(BgL_auxz00_14575,BFALSE,BFALSE);} } } } 
BgL_test5242z00_14555 = 
(BgL_arg1811z00_7556==BgL_classz00_7552); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7560;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7561;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7562; long BgL_arg1555z00_7563;
BgL_arg1554z00_7562 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7564;
BgL_arg1556z00_7564 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7553); 
BgL_arg1555z00_7563 = 
(BgL_arg1556z00_7564-OBJECT_TYPE); } 
BgL_oclassz00_7561 = 
VECTOR_REF(BgL_arg1554z00_7562,BgL_arg1555z00_7563); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7565;
BgL__ortest_1220z00_7565 = 
(BgL_classz00_7552==BgL_oclassz00_7561); 
if(BgL__ortest_1220z00_7565)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7560 = BgL__ortest_1220z00_7565; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7566;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7567;
BgL_arg1810z00_7567 = 
(BgL_oclassz00_7561); 
BgL_odepthz00_7566 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7567); } 
if(
(BgL_arg1804z00_7554<BgL_odepthz00_7566))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7568;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7569;
BgL_arg1809z00_7569 = 
(BgL_oclassz00_7561); 
BgL_arg1808z00_7568 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7569, BgL_arg1804z00_7554); } 
BgL_res2618z00_7560 = 
(BgL_arg1808z00_7568==BgL_classz00_7552); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7560 = ((bool_t)0); } } } } 
BgL_test5242z00_14555 = BgL_res2618z00_7560; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5242z00_14555 = ((bool_t)0)
; } } 
if(BgL_test5242z00_14555)
{ /* Llib/object.scm 237 */
BgL_new1017z00_7570 = 
((BgL_z62conditionz62_bglt)BgL_new1017z00_5342); }  else 
{ 
 obj_t BgL_auxz00_14597;
BgL_auxz00_14597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8543L), BGl_string3865z00zz__objectz00, BGl_string3656z00zz__objectz00, BgL_new1017z00_5342); 
FAILURE(BgL_auxz00_14597,BFALSE,BFALSE);} } 
BgL_auxz00_14554 = BgL_new1017z00_7570; } 
return 
((obj_t)BgL_auxz00_14554);} } 

}



/* &lambda1859 */
BgL_z62conditionz62_bglt BGl_z62lambda1859z62zz__objectz00(obj_t BgL_envz00_5343)
{
{ /* Llib/object.scm 237 */
{ /* Llib/object.scm 237 */
 BgL_z62conditionz62_bglt BgL_new1016z00_7571;
BgL_new1016z00_7571 = 
((BgL_z62conditionz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62conditionz62_bgl) ))); 
{ /* Llib/object.scm 237 */
 long BgL_arg1860z00_7572;
BgL_arg1860z00_7572 = 
BGL_CLASS_NUM(BGl_z62conditionz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1016z00_7571), BgL_arg1860z00_7572); } 
return BgL_new1016z00_7571;} } 

}



/* &lambda1857 */
BgL_z62conditionz62_bglt BGl_z62lambda1857z62zz__objectz00(obj_t BgL_envz00_5344)
{
{ /* Llib/object.scm 237 */
{ /* Llib/object.scm 237 */
 BgL_z62conditionz62_bglt BgL_new1230z00_7573;
{ /* Llib/object.scm 237 */
 BgL_z62conditionz62_bglt BgL_new1229z00_7574;
BgL_new1229z00_7574 = 
((BgL_z62conditionz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62conditionz62_bgl) ))); 
{ /* Llib/object.scm 237 */
 long BgL_arg1858z00_7575;
BgL_arg1858z00_7575 = 
BGL_CLASS_NUM(BGl_z62conditionz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1229z00_7574), BgL_arg1858z00_7575); } 
BgL_new1230z00_7573 = BgL_new1229z00_7574; } 
return BgL_new1230z00_7573;} } 

}



/* &<@anonymous:1850> */
obj_t BGl_z62zc3z04anonymousza31850ze3ze5zz__objectz00(obj_t BgL_envz00_5345, obj_t BgL_new1014z00_5346)
{
{ /* Llib/object.scm 236 */
{ 
 BgL_objectz00_bglt BgL_auxz00_14610;
{ /* Llib/object.scm 236 */
 BgL_objectz00_bglt BgL_new1014z00_7594;
{ /* Llib/object.scm 236 */
 bool_t BgL_test5249z00_14611;
{ /* Llib/object.scm 236 */
 obj_t BgL_classz00_7576;
BgL_classz00_7576 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_new1014z00_5346))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7577; long BgL_arg1804z00_7578;
BgL_arg1803z00_7577 = 
(BgL_objectz00_bglt)(BgL_new1014z00_5346); 
BgL_arg1804z00_7578 = 
BGL_CLASS_DEPTH(BgL_classz00_7576); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7579;
BgL_idxz00_7579 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7577); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7580;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7581;
BgL_arg1812z00_7581 = 
(BgL_idxz00_7579+BgL_arg1804z00_7578); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7582;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7583;
BgL_aux3145z00_7583 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7583))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7582 = BgL_aux3145z00_7583; }  else 
{ 
 obj_t BgL_auxz00_14622;
BgL_auxz00_14622 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7583); 
FAILURE(BgL_auxz00_14622,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5253z00_14626;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14627;
BgL_tmpz00_14627 = 
VECTOR_LENGTH(BgL_vectorz00_7582); 
BgL_test5253z00_14626 = 
BOUND_CHECK(BgL_arg1812z00_7581, BgL_tmpz00_14627); } 
if(BgL_test5253z00_14626)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7580 = 
VECTOR_REF(BgL_vectorz00_7582,BgL_arg1812z00_7581); }  else 
{ 
 obj_t BgL_auxz00_14631;
BgL_auxz00_14631 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7582, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7582)), 
(int)(BgL_arg1812z00_7581)); 
FAILURE(BgL_auxz00_14631,BFALSE,BFALSE);} } } } 
BgL_test5249z00_14611 = 
(BgL_arg1811z00_7580==BgL_classz00_7576); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7584;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7585;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7586; long BgL_arg1555z00_7587;
BgL_arg1554z00_7586 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7588;
BgL_arg1556z00_7588 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7577); 
BgL_arg1555z00_7587 = 
(BgL_arg1556z00_7588-OBJECT_TYPE); } 
BgL_oclassz00_7585 = 
VECTOR_REF(BgL_arg1554z00_7586,BgL_arg1555z00_7587); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7589;
BgL__ortest_1220z00_7589 = 
(BgL_classz00_7576==BgL_oclassz00_7585); 
if(BgL__ortest_1220z00_7589)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7584 = BgL__ortest_1220z00_7589; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7590;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7591;
BgL_arg1810z00_7591 = 
(BgL_oclassz00_7585); 
BgL_odepthz00_7590 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7591); } 
if(
(BgL_arg1804z00_7578<BgL_odepthz00_7590))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7592;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7593;
BgL_arg1809z00_7593 = 
(BgL_oclassz00_7585); 
BgL_arg1808z00_7592 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7593, BgL_arg1804z00_7578); } 
BgL_res2618z00_7584 = 
(BgL_arg1808z00_7592==BgL_classz00_7576); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7584 = ((bool_t)0); } } } } 
BgL_test5249z00_14611 = BgL_res2618z00_7584; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5249z00_14611 = ((bool_t)0)
; } } 
if(BgL_test5249z00_14611)
{ /* Llib/object.scm 236 */
BgL_new1014z00_7594 = 
((BgL_objectz00_bglt)BgL_new1014z00_5346); }  else 
{ 
 obj_t BgL_auxz00_14653;
BgL_auxz00_14653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(8515L), BGl_string3866z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_new1014z00_5346); 
FAILURE(BgL_auxz00_14653,BFALSE,BFALSE);} } 
BgL_auxz00_14610 = BgL_new1014z00_7594; } 
return 
((obj_t)BgL_auxz00_14610);} } 

}



/* &lambda1848 */
BgL_objectz00_bglt BGl_z62lambda1848z62zz__objectz00(obj_t BgL_envz00_5347)
{
{ /* Llib/object.scm 236 */
{ /* Llib/object.scm 236 */
 BgL_objectz00_bglt BgL_new1013z00_7595;
BgL_new1013z00_7595 = 
((BgL_objectz00_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_objectz00_bgl) ))); 
{ /* Llib/object.scm 236 */
 long BgL_arg1849z00_7596;
BgL_arg1849z00_7596 = 
BGL_CLASS_NUM(BGl_objectz00zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(BgL_new1013z00_7595, BgL_arg1849z00_7596); } 
return BgL_new1013z00_7595;} } 

}



/* &lambda1846 */
BgL_objectz00_bglt BGl_z62lambda1846z62zz__objectz00(obj_t BgL_envz00_5348)
{
{ /* Llib/object.scm 236 */
{ /* Llib/object.scm 236 */
 BgL_objectz00_bglt BgL_new1228z00_7597;
{ /* Llib/object.scm 236 */
 BgL_objectz00_bglt BgL_new1226z00_7598;
BgL_new1226z00_7598 = 
((BgL_objectz00_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_objectz00_bgl) ))); 
{ /* Llib/object.scm 236 */
 long BgL_arg1847z00_7599;
BgL_arg1847z00_7599 = 
BGL_CLASS_NUM(BGl_objectz00zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(BgL_new1226z00_7598, BgL_arg1847z00_7599); } 
BgL_new1228z00_7597 = BgL_new1226z00_7598; } 
return BgL_new1228z00_7597;} } 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__objectz00(void)
{
{ /* Llib/object.scm 17 */
{ /* Llib/object.scm 17 */
 obj_t BgL_classzd2minzd2_4225;
BgL_classzd2minzd2_4225 = BGl_objectz00zz__objectz00; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_top5257z00_14665;
BgL_top5257z00_14665 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top5257z00_14665, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp5256z00_14664;
BgL_tmp5256z00_14664 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BGl_objectzd2displayzd2envz00zz__objectz00, BGl_proc3867z00zz__objectz00, BGl_string3868z00zz__objectz00); 
BGL_EXITD_POP_PROTECT(BgL_top5257z00_14665); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); BgL_tmp5256z00_14664; } } } 
{ /* Llib/object.scm 17 */
 obj_t BgL_classzd2minzd2_4230;
BgL_classzd2minzd2_4230 = BGl_objectz00zz__objectz00; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_top5259z00_14673;
BgL_top5259z00_14673 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top5259z00_14673, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp5258z00_14672;
BgL_tmp5258z00_14672 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BGl_objectzd2writezd2envz00zz__objectz00, BGl_proc3869z00zz__objectz00, BGl_string3870z00zz__objectz00); 
BGL_EXITD_POP_PROTECT(BgL_top5259z00_14673); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); BgL_tmp5258z00_14672; } } } 
{ /* Llib/object.scm 17 */
 obj_t BgL_classzd2minzd2_4231;
BgL_classzd2minzd2_4231 = BGl_objectz00zz__objectz00; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_top5261z00_14681;
BgL_top5261z00_14681 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top5261z00_14681, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp5260z00_14680;
BgL_tmp5260z00_14680 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BGl_objectzd2hashnumberzd2envz00zz__objectz00, BGl_proc3871z00zz__objectz00, BGl_string3872z00zz__objectz00); 
BGL_EXITD_POP_PROTECT(BgL_top5261z00_14681); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); BgL_tmp5260z00_14680; } } } 
{ /* Llib/object.scm 17 */
 obj_t BgL_classzd2minzd2_4284;
BgL_classzd2minzd2_4284 = BGl_objectz00zz__objectz00; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_top5263z00_14689;
BgL_top5263z00_14689 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top5263z00_14689, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp5262z00_14688;
BgL_tmp5262z00_14688 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BGl_objectzd2printzd2envz00zz__objectz00, BGl_proc3873z00zz__objectz00, BGl_string3874z00zz__objectz00); 
BGL_EXITD_POP_PROTECT(BgL_top5263z00_14689); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); BgL_tmp5262z00_14688; } } } 
{ /* Llib/object.scm 17 */
 obj_t BgL_classzd2minzd2_4322;
BgL_classzd2minzd2_4322 = BGl_objectz00zz__objectz00; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_top5265z00_14697;
BgL_top5265z00_14697 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top5265z00_14697, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp5264z00_14696;
BgL_tmp5264z00_14696 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BGl_objectzd2equalzf3zd2envzf3zz__objectz00, BGl_proc3875z00zz__objectz00, BGl_string3876z00zz__objectz00); 
BGL_EXITD_POP_PROTECT(BgL_top5265z00_14697); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); BgL_tmp5264z00_14696; } } } 
{ /* Llib/object.scm 1156 */
 obj_t BgL_top5267z00_14705;
BgL_top5267z00_14705 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(bigloo_generic_mutex); 
BGL_EXITD_PUSH_PROTECT(BgL_top5267z00_14705, bigloo_generic_mutex); BUNSPEC; 
{ /* Llib/object.scm 1156 */
 obj_t BgL_tmp5266z00_14704;
BgL_tmp5266z00_14704 = 
BGl_registerzd2genericzd2sanszd2lockz12zc0zz__objectz00(BGl_exceptionzd2notifyzd2envz00zz__objectz00, BGl_proc3877z00zz__objectz00, BGl_string3878z00zz__objectz00); 
BGL_EXITD_POP_PROTECT(BgL_top5267z00_14705); BUNSPEC; 
BGL_MUTEX_UNLOCK(bigloo_generic_mutex); 
return BgL_tmp5266z00_14704;} } } 

}



/* &exception-notify1404 */
obj_t BGl_z62exceptionzd2notify1404zb0zz__objectz00(obj_t BgL_envz00_5361, obj_t BgL_excz00_5362)
{
{ /* Llib/object.scm 1495 */
{ /* Llib/object.scm 1496 */
 obj_t BgL_portz00_7618;
{ /* Llib/object.scm 1496 */
 obj_t BgL_tmpz00_14712;
BgL_tmpz00_14712 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_7618 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_14712); } 
bgl_display_string(BGl_string3879z00zz__objectz00, BgL_portz00_7618); 
BGl_writezd2circlezd2zz__pp_circlez00(BgL_excz00_5362, BgL_portz00_7618); 
if(
CBOOL(
BGl_currentzd2threadzd2zz__threadz00()))
{ /* Llib/object.scm 1499 */
bgl_display_string(BGl_string3880z00zz__objectz00, BgL_portz00_7618); 
bgl_display_obj(
BGl_currentzd2threadzd2zz__threadz00(), BgL_portz00_7618); 
bgl_display_string(BGl_string3881z00zz__objectz00, BgL_portz00_7618); }  else 
{ /* Llib/object.scm 1499 */BFALSE; } 
bgl_display_char(((unsigned char)10), BgL_portz00_7618); 
{ /* Llib/object.scm 1504 */
 obj_t BgL_stackz00_7619;
{ /* Llib/object.scm 1504 */
 bool_t BgL_test5269z00_14725;
{ /* Llib/object.scm 1504 */
 obj_t BgL_classz00_7620;
BgL_classz00_7620 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_excz00_5362))
{ /* Llib/object.scm 1341 */
 BgL_objectz00_bglt BgL_arg1806z00_7621;
BgL_arg1806z00_7621 = 
(BgL_objectz00_bglt)(BgL_excz00_5362); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7622;
BgL_idxz00_7622 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1806z00_7621); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7623;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7624;
BgL_arg1812z00_7624 = 
(BgL_idxz00_7622+2L); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7625;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3360z00_7626;
BgL_aux3360z00_7626 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3360z00_7626))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7625 = BgL_aux3360z00_7626; }  else 
{ 
 obj_t BgL_auxz00_14735;
BgL_auxz00_14735 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3878z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3360z00_7626); 
FAILURE(BgL_auxz00_14735,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5273z00_14739;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14740;
BgL_tmpz00_14740 = 
VECTOR_LENGTH(BgL_vectorz00_7625); 
BgL_test5273z00_14739 = 
BOUND_CHECK(BgL_arg1812z00_7624, BgL_tmpz00_14740); } 
if(BgL_test5273z00_14739)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7623 = 
VECTOR_REF(BgL_vectorz00_7625,BgL_arg1812z00_7624); }  else 
{ 
 obj_t BgL_auxz00_14744;
BgL_auxz00_14744 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7625, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7625)), 
(int)(BgL_arg1812z00_7624)); 
FAILURE(BgL_auxz00_14744,BFALSE,BFALSE);} } } } 
BgL_test5269z00_14725 = 
(BgL_arg1811z00_7623==BgL_classz00_7620); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2639z00_7627;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7628;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7629; long BgL_arg1555z00_7630;
BgL_arg1554z00_7629 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7631;
BgL_arg1556z00_7631 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1806z00_7621); 
BgL_arg1555z00_7630 = 
(BgL_arg1556z00_7631-OBJECT_TYPE); } 
BgL_oclassz00_7628 = 
VECTOR_REF(BgL_arg1554z00_7629,BgL_arg1555z00_7630); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7632;
BgL__ortest_1220z00_7632 = 
(BgL_classz00_7620==BgL_oclassz00_7628); 
if(BgL__ortest_1220z00_7632)
{ /* Llib/object.scm 1363 */
BgL_res2639z00_7627 = BgL__ortest_1220z00_7632; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7633;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7634;
BgL_arg1810z00_7634 = 
(BgL_oclassz00_7628); 
BgL_odepthz00_7633 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7634); } 
if(
(2L<BgL_odepthz00_7633))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7635;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7636;
BgL_arg1809z00_7636 = 
(BgL_oclassz00_7628); 
BgL_arg1808z00_7635 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7636, 2L); } 
BgL_res2639z00_7627 = 
(BgL_arg1808z00_7635==BgL_classz00_7620); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2639z00_7627 = ((bool_t)0); } } } } 
BgL_test5269z00_14725 = BgL_res2639z00_7627; } }  else 
{ /* Llib/object.scm 1340 */
BgL_test5269z00_14725 = ((bool_t)0)
; } } 
if(BgL_test5269z00_14725)
{ /* Llib/object.scm 1505 */
 BgL_z62exceptionz62_bglt BgL_i1223z00_7637;
{ /* Llib/object.scm 1506 */
 bool_t BgL_test5276z00_14765;
{ /* Llib/object.scm 1506 */
 obj_t BgL_classz00_7638;
BgL_classz00_7638 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_excz00_5362))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7639; long BgL_arg1804z00_7640;
BgL_arg1803z00_7639 = 
(BgL_objectz00_bglt)(BgL_excz00_5362); 
BgL_arg1804z00_7640 = 
BGL_CLASS_DEPTH(BgL_classz00_7638); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7641;
BgL_idxz00_7641 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7639); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7642;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7643;
BgL_arg1812z00_7643 = 
(BgL_idxz00_7641+BgL_arg1804z00_7640); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7644;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7645;
BgL_aux3145z00_7645 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7645))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7644 = BgL_aux3145z00_7645; }  else 
{ 
 obj_t BgL_auxz00_14776;
BgL_auxz00_14776 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7645); 
FAILURE(BgL_auxz00_14776,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5280z00_14780;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14781;
BgL_tmpz00_14781 = 
VECTOR_LENGTH(BgL_vectorz00_7644); 
BgL_test5280z00_14780 = 
BOUND_CHECK(BgL_arg1812z00_7643, BgL_tmpz00_14781); } 
if(BgL_test5280z00_14780)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7642 = 
VECTOR_REF(BgL_vectorz00_7644,BgL_arg1812z00_7643); }  else 
{ 
 obj_t BgL_auxz00_14785;
BgL_auxz00_14785 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7644, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7644)), 
(int)(BgL_arg1812z00_7643)); 
FAILURE(BgL_auxz00_14785,BFALSE,BFALSE);} } } } 
BgL_test5276z00_14765 = 
(BgL_arg1811z00_7642==BgL_classz00_7638); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7646;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7647;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7648; long BgL_arg1555z00_7649;
BgL_arg1554z00_7648 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7650;
BgL_arg1556z00_7650 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7639); 
BgL_arg1555z00_7649 = 
(BgL_arg1556z00_7650-OBJECT_TYPE); } 
BgL_oclassz00_7647 = 
VECTOR_REF(BgL_arg1554z00_7648,BgL_arg1555z00_7649); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7651;
BgL__ortest_1220z00_7651 = 
(BgL_classz00_7638==BgL_oclassz00_7647); 
if(BgL__ortest_1220z00_7651)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7646 = BgL__ortest_1220z00_7651; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7652;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7653;
BgL_arg1810z00_7653 = 
(BgL_oclassz00_7647); 
BgL_odepthz00_7652 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7653); } 
if(
(BgL_arg1804z00_7640<BgL_odepthz00_7652))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7654;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7655;
BgL_arg1809z00_7655 = 
(BgL_oclassz00_7647); 
BgL_arg1808z00_7654 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7655, BgL_arg1804z00_7640); } 
BgL_res2618z00_7646 = 
(BgL_arg1808z00_7654==BgL_classz00_7638); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7646 = ((bool_t)0); } } } } 
BgL_test5276z00_14765 = BgL_res2618z00_7646; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5276z00_14765 = ((bool_t)0)
; } } 
if(BgL_test5276z00_14765)
{ /* Llib/object.scm 1506 */
BgL_i1223z00_7637 = 
((BgL_z62exceptionz62_bglt)BgL_excz00_5362); }  else 
{ 
 obj_t BgL_auxz00_14807;
BgL_auxz00_14807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(65434L), BGl_string3878z00zz__objectz00, BGl_string3676z00zz__objectz00, BgL_excz00_5362); 
FAILURE(BgL_auxz00_14807,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1506 */
 obj_t BgL__ortest_1224z00_7656;
BgL__ortest_1224z00_7656 = 
(((BgL_z62exceptionz62_bglt)COBJECT(BgL_i1223z00_7637))->BgL_stackz00); 
if(
CBOOL(BgL__ortest_1224z00_7656))
{ /* Llib/object.scm 1506 */
BgL_stackz00_7619 = BgL__ortest_1224z00_7656; }  else 
{ /* Llib/object.scm 1506 */

{ /* Llib/object.scm 1506 */

BgL_stackz00_7619 = 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE); } } } }  else 
{ /* Llib/object.scm 1507 */

{ /* Llib/object.scm 1507 */

BgL_stackz00_7619 = 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE); } } } 
{ /* Llib/object.scm 1504 */

return 
BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_stackz00_7619, BgL_portz00_7618, 
BINT(1L));} } } } 

}



/* &object-equal?1402 */
obj_t BGl_z62objectzd2equalzf31402z43zz__objectz00(obj_t BgL_envz00_5363, obj_t BgL_obj1z00_5364, obj_t BgL_obj2z00_5365)
{
{ /* Llib/object.scm 1475 */
{ /* Llib/object.scm 1479 */
 bool_t BgL_tmpz00_14818;
{ /* Llib/object.scm 1479 */
 BgL_objectz00_bglt BgL_obj1z00_7693; BgL_objectz00_bglt BgL_obj2z00_7694;
{ /* Llib/object.scm 1479 */
 bool_t BgL_test5284z00_14819;
{ /* Llib/object.scm 1479 */
 obj_t BgL_classz00_7657;
BgL_classz00_7657 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_obj1z00_5364))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7658; long BgL_arg1804z00_7659;
BgL_arg1803z00_7658 = 
(BgL_objectz00_bglt)(BgL_obj1z00_5364); 
BgL_arg1804z00_7659 = 
BGL_CLASS_DEPTH(BgL_classz00_7657); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7660;
BgL_idxz00_7660 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7658); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7661;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7662;
BgL_arg1812z00_7662 = 
(BgL_idxz00_7660+BgL_arg1804z00_7659); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7663;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7664;
BgL_aux3145z00_7664 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7664))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7663 = BgL_aux3145z00_7664; }  else 
{ 
 obj_t BgL_auxz00_14830;
BgL_auxz00_14830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7664); 
FAILURE(BgL_auxz00_14830,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5288z00_14834;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14835;
BgL_tmpz00_14835 = 
VECTOR_LENGTH(BgL_vectorz00_7663); 
BgL_test5288z00_14834 = 
BOUND_CHECK(BgL_arg1812z00_7662, BgL_tmpz00_14835); } 
if(BgL_test5288z00_14834)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7661 = 
VECTOR_REF(BgL_vectorz00_7663,BgL_arg1812z00_7662); }  else 
{ 
 obj_t BgL_auxz00_14839;
BgL_auxz00_14839 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7663, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7663)), 
(int)(BgL_arg1812z00_7662)); 
FAILURE(BgL_auxz00_14839,BFALSE,BFALSE);} } } } 
BgL_test5284z00_14819 = 
(BgL_arg1811z00_7661==BgL_classz00_7657); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7665;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7666;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7667; long BgL_arg1555z00_7668;
BgL_arg1554z00_7667 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7669;
BgL_arg1556z00_7669 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7658); 
BgL_arg1555z00_7668 = 
(BgL_arg1556z00_7669-OBJECT_TYPE); } 
BgL_oclassz00_7666 = 
VECTOR_REF(BgL_arg1554z00_7667,BgL_arg1555z00_7668); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7670;
BgL__ortest_1220z00_7670 = 
(BgL_classz00_7657==BgL_oclassz00_7666); 
if(BgL__ortest_1220z00_7670)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7665 = BgL__ortest_1220z00_7670; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7671;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7672;
BgL_arg1810z00_7672 = 
(BgL_oclassz00_7666); 
BgL_odepthz00_7671 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7672); } 
if(
(BgL_arg1804z00_7659<BgL_odepthz00_7671))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7673;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7674;
BgL_arg1809z00_7674 = 
(BgL_oclassz00_7666); 
BgL_arg1808z00_7673 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7674, BgL_arg1804z00_7659); } 
BgL_res2618z00_7665 = 
(BgL_arg1808z00_7673==BgL_classz00_7657); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7665 = ((bool_t)0); } } } } 
BgL_test5284z00_14819 = BgL_res2618z00_7665; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5284z00_14819 = ((bool_t)0)
; } } 
if(BgL_test5284z00_14819)
{ /* Llib/object.scm 1479 */
BgL_obj1z00_7693 = 
((BgL_objectz00_bglt)BgL_obj1z00_5364); }  else 
{ 
 obj_t BgL_auxz00_14861;
BgL_auxz00_14861 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(64494L), BGl_string3890z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_obj1z00_5364); 
FAILURE(BgL_auxz00_14861,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1479 */
 bool_t BgL_test5291z00_14865;
{ /* Llib/object.scm 1479 */
 obj_t BgL_classz00_7675;
BgL_classz00_7675 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_obj2z00_5365))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7676; long BgL_arg1804z00_7677;
BgL_arg1803z00_7676 = 
(BgL_objectz00_bglt)(BgL_obj2z00_5365); 
BgL_arg1804z00_7677 = 
BGL_CLASS_DEPTH(BgL_classz00_7675); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7678;
BgL_idxz00_7678 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7676); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7679;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7680;
BgL_arg1812z00_7680 = 
(BgL_idxz00_7678+BgL_arg1804z00_7677); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7681;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7682;
BgL_aux3145z00_7682 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7682))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7681 = BgL_aux3145z00_7682; }  else 
{ 
 obj_t BgL_auxz00_14876;
BgL_auxz00_14876 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7682); 
FAILURE(BgL_auxz00_14876,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5295z00_14880;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14881;
BgL_tmpz00_14881 = 
VECTOR_LENGTH(BgL_vectorz00_7681); 
BgL_test5295z00_14880 = 
BOUND_CHECK(BgL_arg1812z00_7680, BgL_tmpz00_14881); } 
if(BgL_test5295z00_14880)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7679 = 
VECTOR_REF(BgL_vectorz00_7681,BgL_arg1812z00_7680); }  else 
{ 
 obj_t BgL_auxz00_14885;
BgL_auxz00_14885 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7681, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7681)), 
(int)(BgL_arg1812z00_7680)); 
FAILURE(BgL_auxz00_14885,BFALSE,BFALSE);} } } } 
BgL_test5291z00_14865 = 
(BgL_arg1811z00_7679==BgL_classz00_7675); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7683;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7684;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7685; long BgL_arg1555z00_7686;
BgL_arg1554z00_7685 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7687;
BgL_arg1556z00_7687 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7676); 
BgL_arg1555z00_7686 = 
(BgL_arg1556z00_7687-OBJECT_TYPE); } 
BgL_oclassz00_7684 = 
VECTOR_REF(BgL_arg1554z00_7685,BgL_arg1555z00_7686); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7688;
BgL__ortest_1220z00_7688 = 
(BgL_classz00_7675==BgL_oclassz00_7684); 
if(BgL__ortest_1220z00_7688)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7683 = BgL__ortest_1220z00_7688; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7689;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7690;
BgL_arg1810z00_7690 = 
(BgL_oclassz00_7684); 
BgL_odepthz00_7689 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7690); } 
if(
(BgL_arg1804z00_7677<BgL_odepthz00_7689))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7691;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7692;
BgL_arg1809z00_7692 = 
(BgL_oclassz00_7684); 
BgL_arg1808z00_7691 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7692, BgL_arg1804z00_7677); } 
BgL_res2618z00_7683 = 
(BgL_arg1808z00_7691==BgL_classz00_7675); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7683 = ((bool_t)0); } } } } 
BgL_test5291z00_14865 = BgL_res2618z00_7683; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5291z00_14865 = ((bool_t)0)
; } } 
if(BgL_test5291z00_14865)
{ /* Llib/object.scm 1479 */
BgL_obj2z00_7694 = 
((BgL_objectz00_bglt)BgL_obj2z00_5365); }  else 
{ 
 obj_t BgL_auxz00_14907;
BgL_auxz00_14907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(64494L), BGl_string3890z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_obj2z00_5365); 
FAILURE(BgL_auxz00_14907,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1479 */
 obj_t BgL_class1z00_7695; obj_t BgL_class2z00_7696;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7697; long BgL_arg1555z00_7698;
BgL_arg1554z00_7697 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7699;
BgL_arg1556z00_7699 = 
BGL_OBJECT_CLASS_NUM(BgL_obj1z00_7693); 
BgL_arg1555z00_7698 = 
(BgL_arg1556z00_7699-OBJECT_TYPE); } 
BgL_class1z00_7695 = 
VECTOR_REF(BgL_arg1554z00_7697,BgL_arg1555z00_7698); } 
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7700; long BgL_arg1555z00_7701;
BgL_arg1554z00_7700 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7702;
BgL_arg1556z00_7702 = 
BGL_OBJECT_CLASS_NUM(BgL_obj2z00_7694); 
BgL_arg1555z00_7701 = 
(BgL_arg1556z00_7702-OBJECT_TYPE); } 
BgL_class2z00_7696 = 
VECTOR_REF(BgL_arg1554z00_7700,BgL_arg1555z00_7701); } 
if(
(BgL_class1z00_7695==BgL_class2z00_7696))
{ /* Llib/object.scm 1482 */
 obj_t BgL_fieldsz00_7703;
{ /* Llib/object.scm 1482 */
 obj_t BgL_classz00_7704;
if(
BGL_CLASSP(BgL_class1z00_7695))
{ /* Llib/object.scm 1482 */
BgL_classz00_7704 = BgL_class1z00_7695; }  else 
{ 
 obj_t BgL_auxz00_14923;
BgL_auxz00_14923 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(64627L), BGl_string3876z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_class1z00_7695); 
FAILURE(BgL_auxz00_14923,BFALSE,BFALSE);} 
BgL_fieldsz00_7703 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_7704); } 
{ /* Llib/object.scm 1483 */
 long BgL_g1222z00_7705;
BgL_g1222z00_7705 = 
(
VECTOR_LENGTH(BgL_fieldsz00_7703)-1L); 
{ 
 long BgL_iz00_7707;
BgL_iz00_7707 = BgL_g1222z00_7705; 
BgL_loopz00_7706:
if(
(BgL_iz00_7707==-1L))
{ /* Llib/object.scm 1485 */
BgL_tmpz00_14818 = ((bool_t)1)
; }  else 
{ /* Llib/object.scm 1487 */
 bool_t BgL_test5301z00_14932;
{ /* Llib/object.scm 1487 */
 obj_t BgL_arg2192z00_7708;
BgL_arg2192z00_7708 = 
VECTOR_REF(BgL_fieldsz00_7703,BgL_iz00_7707); 
{ /* Llib/object.scm 1477 */
 obj_t BgL_getzd2valuezd2_7709;
{ /* Llib/object.scm 1477 */
 obj_t BgL_res2638z00_7710;
{ /* Llib/object.scm 1477 */
 obj_t BgL_fieldz00_7711;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2192z00_7708))
{ /* Llib/object.scm 1477 */
BgL_fieldz00_7711 = BgL_arg2192z00_7708; }  else 
{ 
 obj_t BgL_auxz00_14936;
BgL_auxz00_14936 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(64435L), BGl_string3442z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_arg2192z00_7708); 
FAILURE(BgL_auxz00_14936,BFALSE,BFALSE);} 
{ /* Llib/object.scm 618 */
 obj_t BgL_vectorz00_7712;
BgL_vectorz00_7712 = BgL_fieldz00_7711; 
{ /* Llib/object.scm 618 */
 obj_t BgL_aux3368z00_7713;
BgL_aux3368z00_7713 = 
VECTOR_REF(BgL_vectorz00_7712,1L); 
if(
PROCEDUREP(BgL_aux3368z00_7713))
{ /* Llib/object.scm 618 */
BgL_res2638z00_7710 = BgL_aux3368z00_7713; }  else 
{ 
 obj_t BgL_auxz00_14943;
BgL_auxz00_14943 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26231L), BGl_string3442z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3368z00_7713); 
FAILURE(BgL_auxz00_14943,BFALSE,BFALSE);} } } } 
BgL_getzd2valuezd2_7709 = BgL_res2638z00_7710; } 
{ /* Llib/object.scm 1478 */
 obj_t BgL_arg2196z00_7714; obj_t BgL_arg2197z00_7715;
if(
PROCEDURE_CORRECT_ARITYP(BgL_getzd2valuezd2_7709, 1))
{ /* Llib/object.scm 1478 */
BgL_arg2196z00_7714 = 
BGL_PROCEDURE_CALL1(BgL_getzd2valuezd2_7709, 
((obj_t)BgL_obj1z00_7693)); }  else 
{ /* Llib/object.scm 1478 */
FAILURE(BGl_string3604z00zz__objectz00,BGl_list3882z00zz__objectz00,BgL_getzd2valuezd2_7709);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_getzd2valuezd2_7709, 1))
{ /* Llib/object.scm 1478 */
BgL_arg2197z00_7715 = 
BGL_PROCEDURE_CALL1(BgL_getzd2valuezd2_7709, 
((obj_t)BgL_obj2z00_7694)); }  else 
{ /* Llib/object.scm 1478 */
FAILURE(BGl_string3604z00zz__objectz00,BGl_list3887z00zz__objectz00,BgL_getzd2valuezd2_7709);} 
BgL_test5301z00_14932 = 
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_arg2196z00_7714, BgL_arg2197z00_7715); } } } 
if(BgL_test5301z00_14932)
{ 
 long BgL_iz00_14964;
BgL_iz00_14964 = 
(BgL_iz00_7707-1L); 
BgL_iz00_7707 = BgL_iz00_14964; 
goto BgL_loopz00_7706;}  else 
{ /* Llib/object.scm 1487 */
BgL_tmpz00_14818 = ((bool_t)0)
; } } } } }  else 
{ /* Llib/object.scm 1481 */
BgL_tmpz00_14818 = ((bool_t)0)
; } } } 
return 
BBOOL(BgL_tmpz00_14818);} } 

}



/* &object-print1399 */
obj_t BGl_z62objectzd2print1399zb0zz__objectz00(obj_t BgL_envz00_5366, obj_t BgL_objz00_5367, obj_t BgL_portz00_5368, obj_t BgL_printzd2slotzd2_5369)
{
{ /* Llib/object.scm 1445 */
{ /* Llib/object.scm 1448 */
 BgL_objectz00_bglt BgL_objz00_7734; obj_t BgL_portz00_7735; obj_t BgL_printzd2slotzd2_7736;
{ /* Llib/object.scm 1448 */
 bool_t BgL_test5306z00_14967;
{ /* Llib/object.scm 1448 */
 obj_t BgL_classz00_7716;
BgL_classz00_7716 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objz00_5367))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7717; long BgL_arg1804z00_7718;
BgL_arg1803z00_7717 = 
(BgL_objectz00_bglt)(BgL_objz00_5367); 
BgL_arg1804z00_7718 = 
BGL_CLASS_DEPTH(BgL_classz00_7716); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7719;
BgL_idxz00_7719 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7717); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7720;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7721;
BgL_arg1812z00_7721 = 
(BgL_idxz00_7719+BgL_arg1804z00_7718); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7722;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7723;
BgL_aux3145z00_7723 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7723))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7722 = BgL_aux3145z00_7723; }  else 
{ 
 obj_t BgL_auxz00_14978;
BgL_auxz00_14978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7723); 
FAILURE(BgL_auxz00_14978,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5310z00_14982;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_14983;
BgL_tmpz00_14983 = 
VECTOR_LENGTH(BgL_vectorz00_7722); 
BgL_test5310z00_14982 = 
BOUND_CHECK(BgL_arg1812z00_7721, BgL_tmpz00_14983); } 
if(BgL_test5310z00_14982)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7720 = 
VECTOR_REF(BgL_vectorz00_7722,BgL_arg1812z00_7721); }  else 
{ 
 obj_t BgL_auxz00_14987;
BgL_auxz00_14987 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7722, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7722)), 
(int)(BgL_arg1812z00_7721)); 
FAILURE(BgL_auxz00_14987,BFALSE,BFALSE);} } } } 
BgL_test5306z00_14967 = 
(BgL_arg1811z00_7720==BgL_classz00_7716); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7724;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7725;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7726; long BgL_arg1555z00_7727;
BgL_arg1554z00_7726 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7728;
BgL_arg1556z00_7728 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7717); 
BgL_arg1555z00_7727 = 
(BgL_arg1556z00_7728-OBJECT_TYPE); } 
BgL_oclassz00_7725 = 
VECTOR_REF(BgL_arg1554z00_7726,BgL_arg1555z00_7727); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7729;
BgL__ortest_1220z00_7729 = 
(BgL_classz00_7716==BgL_oclassz00_7725); 
if(BgL__ortest_1220z00_7729)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7724 = BgL__ortest_1220z00_7729; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7730;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7731;
BgL_arg1810z00_7731 = 
(BgL_oclassz00_7725); 
BgL_odepthz00_7730 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7731); } 
if(
(BgL_arg1804z00_7718<BgL_odepthz00_7730))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7732;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7733;
BgL_arg1809z00_7733 = 
(BgL_oclassz00_7725); 
BgL_arg1808z00_7732 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7733, BgL_arg1804z00_7718); } 
BgL_res2618z00_7724 = 
(BgL_arg1808z00_7732==BgL_classz00_7716); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7724 = ((bool_t)0); } } } } 
BgL_test5306z00_14967 = BgL_res2618z00_7724; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5306z00_14967 = ((bool_t)0)
; } } 
if(BgL_test5306z00_14967)
{ /* Llib/object.scm 1448 */
BgL_objz00_7734 = 
((BgL_objectz00_bglt)BgL_objz00_5367); }  else 
{ 
 obj_t BgL_auxz00_15009;
BgL_auxz00_15009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63418L), BGl_string3904z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_5367); 
FAILURE(BgL_auxz00_15009,BFALSE,BFALSE);} } 
if(
OUTPUT_PORTP(BgL_portz00_5368))
{ /* Llib/object.scm 1448 */
BgL_portz00_7735 = BgL_portz00_5368; }  else 
{ 
 obj_t BgL_auxz00_15015;
BgL_auxz00_15015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63418L), BGl_string3904z00zz__objectz00, BGl_string3905z00zz__objectz00, BgL_portz00_5368); 
FAILURE(BgL_auxz00_15015,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_printzd2slotzd2_5369))
{ /* Llib/object.scm 1448 */
BgL_printzd2slotzd2_7736 = BgL_printzd2slotzd2_5369; }  else 
{ 
 obj_t BgL_auxz00_15021;
BgL_auxz00_15021 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63418L), BGl_string3904z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_printzd2slotzd2_5369); 
FAILURE(BgL_auxz00_15021,BFALSE,BFALSE);} 
{ 
 obj_t BgL_fieldz00_7738;
{ /* Llib/object.scm 1457 */
 obj_t BgL_classz00_7750;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7751; long BgL_arg1555z00_7752;
BgL_arg1554z00_7751 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7753;
BgL_arg1556z00_7753 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_7734); 
BgL_arg1555z00_7752 = 
(BgL_arg1556z00_7753-OBJECT_TYPE); } 
BgL_classz00_7750 = 
VECTOR_REF(BgL_arg1554z00_7751,BgL_arg1555z00_7752); } 
{ /* Llib/object.scm 1457 */
 obj_t BgL_classzd2namezd2_7754;
{ /* Llib/object.scm 1458 */
 obj_t BgL_classz00_7755;
if(
BGL_CLASSP(BgL_classz00_7750))
{ /* Llib/object.scm 1458 */
BgL_classz00_7755 = BgL_classz00_7750; }  else 
{ 
 obj_t BgL_auxz00_15031;
BgL_auxz00_15031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63719L), BGl_string3874z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_7750); 
FAILURE(BgL_auxz00_15031,BFALSE,BFALSE);} 
BgL_classzd2namezd2_7754 = 
BGL_CLASS_NAME(BgL_classz00_7755); } 
{ /* Llib/object.scm 1458 */
 obj_t BgL_fieldsz00_7756;
{ /* Llib/object.scm 1459 */
 obj_t BgL_classz00_7757;
if(
BGL_CLASSP(BgL_classz00_7750))
{ /* Llib/object.scm 1459 */
BgL_classz00_7757 = BgL_classz00_7750; }  else 
{ 
 obj_t BgL_auxz00_15038;
BgL_auxz00_15038 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63756L), BGl_string3874z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_7750); 
FAILURE(BgL_auxz00_15038,BFALSE,BFALSE);} 
BgL_fieldsz00_7756 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_7757); } 
{ /* Llib/object.scm 1460 */

bgl_display_string(BGl_string3902z00zz__objectz00, BgL_portz00_7735); 
bgl_display_obj(BgL_classzd2namezd2_7754, BgL_portz00_7735); 
{ /* Llib/object.scm 1463 */
 bool_t BgL_test5317z00_15045;
{ /* Llib/object.scm 1324 */
 obj_t BgL_klassz00_7758;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7759; long BgL_arg1555z00_7760;
BgL_arg1554z00_7759 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7761;
BgL_arg1556z00_7761 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_7734); 
BgL_arg1555z00_7760 = 
(BgL_arg1556z00_7761-OBJECT_TYPE); } 
BgL_klassz00_7758 = 
VECTOR_REF(BgL_arg1554z00_7759,BgL_arg1555z00_7760); } 
{ /* Llib/object.scm 1325 */
 obj_t BgL_arg1801z00_7762;
{ /* Llib/object.scm 1325 */
 obj_t BgL_classz00_7763;
if(
BGL_CLASSP(BgL_klassz00_7758))
{ /* Llib/object.scm 1325 */
BgL_classz00_7763 = BgL_klassz00_7758; }  else 
{ 
 obj_t BgL_auxz00_15052;
BgL_auxz00_15052 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(57675L), BGl_string3874z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_klassz00_7758); 
FAILURE(BgL_auxz00_15052,BFALSE,BFALSE);} 
if(
BGL_CLASSP(BgL_classz00_7763))
{ /* Llib/object.scm 758 */
 obj_t BgL__ortest_1218z00_7764;
BgL__ortest_1218z00_7764 = 
BGL_CLASS_NIL(BgL_classz00_7763); 
if(
CBOOL(BgL__ortest_1218z00_7764))
{ /* Llib/object.scm 758 */
BgL_arg1801z00_7762 = BgL__ortest_1218z00_7764; }  else 
{ /* Llib/object.scm 758 */
BgL_arg1801z00_7762 = 
BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_7763); } }  else 
{ /* Llib/object.scm 757 */
BgL_arg1801z00_7762 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3521z00zz__objectz00, BGl_string3445z00zz__objectz00, BgL_classz00_7763); } } 
BgL_test5317z00_15045 = 
(BgL_arg1801z00_7762==
((obj_t)BgL_objz00_7734)); } } 
if(BgL_test5317z00_15045)
{ /* Llib/object.scm 1463 */
return 
bgl_display_string(BGl_string3903z00zz__objectz00, BgL_portz00_7735);}  else 
{ 
 long BgL_iz00_7766;
BgL_iz00_7766 = 0L; 
BgL_loopz00_7765:
if(
(BgL_iz00_7766==
VECTOR_LENGTH(BgL_fieldsz00_7756)))
{ /* Llib/object.scm 1466 */
return 
bgl_display_char(((unsigned char)'|'), BgL_portz00_7735);}  else 
{ /* Llib/object.scm 1466 */
BgL_fieldz00_7738 = 
VECTOR_REF(BgL_fieldsz00_7756,BgL_iz00_7766); 
{ /* Llib/object.scm 1448 */
 obj_t BgL_namez00_7739;
{ /* Llib/object.scm 1448 */
 obj_t BgL_res2635z00_7740;
{ /* Llib/object.scm 1448 */
 obj_t BgL_fieldz00_7741;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_7738))
{ /* Llib/object.scm 1448 */
BgL_fieldz00_7741 = BgL_fieldz00_7738; }  else 
{ 
 obj_t BgL_auxz00_15072;
BgL_auxz00_15072 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63449L), BGl_string3891z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_7738); 
FAILURE(BgL_auxz00_15072,BFALSE,BFALSE);} 
{ /* Llib/object.scm 606 */
 obj_t BgL_vectorz00_7742;
BgL_vectorz00_7742 = BgL_fieldz00_7741; 
{ /* Llib/object.scm 606 */
 obj_t BgL_aux3378z00_7743;
BgL_aux3378z00_7743 = 
VECTOR_REF(BgL_vectorz00_7742,0L); 
if(
SYMBOLP(BgL_aux3378z00_7743))
{ /* Llib/object.scm 606 */
BgL_res2635z00_7740 = BgL_aux3378z00_7743; }  else 
{ 
 obj_t BgL_auxz00_15079;
BgL_auxz00_15079 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(25638L), BGl_string3891z00zz__objectz00, BGl_string3447z00zz__objectz00, BgL_aux3378z00_7743); 
FAILURE(BgL_auxz00_15079,BFALSE,BFALSE);} } } } 
BgL_namez00_7739 = BgL_res2635z00_7740; } 
{ /* Llib/object.scm 1448 */
 obj_t BgL_getzd2valuezd2_7744;
{ /* Llib/object.scm 1449 */
 obj_t BgL_res2636z00_7745;
{ /* Llib/object.scm 1449 */
 obj_t BgL_fieldz00_7746;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_fieldz00_7738))
{ /* Llib/object.scm 1449 */
BgL_fieldz00_7746 = BgL_fieldz00_7738; }  else 
{ 
 obj_t BgL_auxz00_15085;
BgL_auxz00_15085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63496L), BGl_string3891z00zz__objectz00, BGl_string3466z00zz__objectz00, BgL_fieldz00_7738); 
FAILURE(BgL_auxz00_15085,BFALSE,BFALSE);} 
{ /* Llib/object.scm 618 */
 obj_t BgL_vectorz00_7747;
BgL_vectorz00_7747 = BgL_fieldz00_7746; 
{ /* Llib/object.scm 618 */
 obj_t BgL_aux3382z00_7748;
BgL_aux3382z00_7748 = 
VECTOR_REF(BgL_vectorz00_7747,1L); 
if(
PROCEDUREP(BgL_aux3382z00_7748))
{ /* Llib/object.scm 618 */
BgL_res2636z00_7745 = BgL_aux3382z00_7748; }  else 
{ 
 obj_t BgL_auxz00_15092;
BgL_auxz00_15092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(26231L), BGl_string3891z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3382z00_7748); 
FAILURE(BgL_auxz00_15092,BFALSE,BFALSE);} } } } 
BgL_getzd2valuezd2_7744 = BgL_res2636z00_7745; } 
{ /* Llib/object.scm 1449 */

bgl_display_string(BGl_string3892z00zz__objectz00, BgL_portz00_7735); 
bgl_display_obj(BgL_namez00_7739, BgL_portz00_7735); 
bgl_display_char(((unsigned char)':'), BgL_portz00_7735); 
bgl_display_char(((unsigned char)' '), BgL_portz00_7735); 
{ /* Llib/object.scm 1454 */
 obj_t BgL_arg2185z00_7749;
if(
PROCEDURE_CORRECT_ARITYP(BgL_getzd2valuezd2_7744, 1))
{ /* Llib/object.scm 1454 */
BgL_arg2185z00_7749 = 
BGL_PROCEDURE_CALL1(BgL_getzd2valuezd2_7744, 
((obj_t)BgL_objz00_7734)); }  else 
{ /* Llib/object.scm 1454 */
FAILURE(BGl_string3893z00zz__objectz00,BGl_list3894z00zz__objectz00,BgL_getzd2valuezd2_7744);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_printzd2slotzd2_7736, 2))
{ /* Llib/object.scm 1454 */
BGL_PROCEDURE_CALL2(BgL_printzd2slotzd2_7736, BgL_arg2185z00_7749, BgL_portz00_7735); }  else 
{ /* Llib/object.scm 1454 */
FAILURE(BGl_string3893z00zz__objectz00,BGl_list3895z00zz__objectz00,BgL_printzd2slotzd2_7736);} } 
bgl_display_char(((unsigned char)']'), BgL_portz00_7735); } } } 
{ 
 long BgL_iz00_15118;
BgL_iz00_15118 = 
(BgL_iz00_7766+1L); 
BgL_iz00_7766 = BgL_iz00_15118; 
goto BgL_loopz00_7765;} } } } } } } } } } } 

}



/* &object-hashnumber1396 */
obj_t BGl_z62objectzd2hashnumber1396zb0zz__objectz00(obj_t BgL_envz00_5370, obj_t BgL_objectz00_5371)
{
{ /* Llib/object.scm 1412 */
{ /* Llib/object.scm 1413 */
 long BgL_tmpz00_15120;
{ /* Llib/object.scm 1413 */
 BgL_objectz00_bglt BgL_objectz00_7785;
{ /* Llib/object.scm 1413 */
 bool_t BgL_test5328z00_15121;
{ /* Llib/object.scm 1413 */
 obj_t BgL_classz00_7767;
BgL_classz00_7767 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objectz00_5371))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7768; long BgL_arg1804z00_7769;
BgL_arg1803z00_7768 = 
(BgL_objectz00_bglt)(BgL_objectz00_5371); 
BgL_arg1804z00_7769 = 
BGL_CLASS_DEPTH(BgL_classz00_7767); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7770;
BgL_idxz00_7770 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7768); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7771;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7772;
BgL_arg1812z00_7772 = 
(BgL_idxz00_7770+BgL_arg1804z00_7769); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7773;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7774;
BgL_aux3145z00_7774 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7774))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7773 = BgL_aux3145z00_7774; }  else 
{ 
 obj_t BgL_auxz00_15132;
BgL_auxz00_15132 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7774); 
FAILURE(BgL_auxz00_15132,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5332z00_15136;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15137;
BgL_tmpz00_15137 = 
VECTOR_LENGTH(BgL_vectorz00_7773); 
BgL_test5332z00_15136 = 
BOUND_CHECK(BgL_arg1812z00_7772, BgL_tmpz00_15137); } 
if(BgL_test5332z00_15136)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7771 = 
VECTOR_REF(BgL_vectorz00_7773,BgL_arg1812z00_7772); }  else 
{ 
 obj_t BgL_auxz00_15141;
BgL_auxz00_15141 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7773, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7773)), 
(int)(BgL_arg1812z00_7772)); 
FAILURE(BgL_auxz00_15141,BFALSE,BFALSE);} } } } 
BgL_test5328z00_15121 = 
(BgL_arg1811z00_7771==BgL_classz00_7767); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7775;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7776;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7777; long BgL_arg1555z00_7778;
BgL_arg1554z00_7777 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7779;
BgL_arg1556z00_7779 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7768); 
BgL_arg1555z00_7778 = 
(BgL_arg1556z00_7779-OBJECT_TYPE); } 
BgL_oclassz00_7776 = 
VECTOR_REF(BgL_arg1554z00_7777,BgL_arg1555z00_7778); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7780;
BgL__ortest_1220z00_7780 = 
(BgL_classz00_7767==BgL_oclassz00_7776); 
if(BgL__ortest_1220z00_7780)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7775 = BgL__ortest_1220z00_7780; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7781;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7782;
BgL_arg1810z00_7782 = 
(BgL_oclassz00_7776); 
BgL_odepthz00_7781 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7782); } 
if(
(BgL_arg1804z00_7769<BgL_odepthz00_7781))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7783;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7784;
BgL_arg1809z00_7784 = 
(BgL_oclassz00_7776); 
BgL_arg1808z00_7783 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7784, BgL_arg1804z00_7769); } 
BgL_res2618z00_7775 = 
(BgL_arg1808z00_7783==BgL_classz00_7767); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7775 = ((bool_t)0); } } } } 
BgL_test5328z00_15121 = BgL_res2618z00_7775; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5328z00_15121 = ((bool_t)0)
; } } 
if(BgL_test5328z00_15121)
{ /* Llib/object.scm 1413 */
BgL_objectz00_7785 = 
((BgL_objectz00_bglt)BgL_objectz00_5371); }  else 
{ 
 obj_t BgL_auxz00_15163;
BgL_auxz00_15163 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61916L), BGl_string3906z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objectz00_5371); 
FAILURE(BgL_auxz00_15163,BFALSE,BFALSE);} } 
BgL_tmpz00_15120 = 
bgl_obj_hash_number(
((obj_t)BgL_objectz00_7785)); } 
return 
BINT(BgL_tmpz00_15120);} } 

}



/* &object-write1392 */
obj_t BGl_z62objectzd2write1392zb0zz__objectz00(obj_t BgL_envz00_5372, obj_t BgL_objz00_5373, obj_t BgL_portz00_5374)
{
{ /* Llib/object.scm 1405 */
{ /* Llib/object.scm 1406 */
 BgL_objectz00_bglt BgL_objz00_7804;
{ /* Llib/object.scm 1406 */
 bool_t BgL_test5335z00_15170;
{ /* Llib/object.scm 1406 */
 obj_t BgL_classz00_7786;
BgL_classz00_7786 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objz00_5373))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7787; long BgL_arg1804z00_7788;
BgL_arg1803z00_7787 = 
(BgL_objectz00_bglt)(BgL_objz00_5373); 
BgL_arg1804z00_7788 = 
BGL_CLASS_DEPTH(BgL_classz00_7786); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7789;
BgL_idxz00_7789 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7787); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7790;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7791;
BgL_arg1812z00_7791 = 
(BgL_idxz00_7789+BgL_arg1804z00_7788); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7792;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7793;
BgL_aux3145z00_7793 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7793))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7792 = BgL_aux3145z00_7793; }  else 
{ 
 obj_t BgL_auxz00_15181;
BgL_auxz00_15181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7793); 
FAILURE(BgL_auxz00_15181,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5339z00_15185;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15186;
BgL_tmpz00_15186 = 
VECTOR_LENGTH(BgL_vectorz00_7792); 
BgL_test5339z00_15185 = 
BOUND_CHECK(BgL_arg1812z00_7791, BgL_tmpz00_15186); } 
if(BgL_test5339z00_15185)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7790 = 
VECTOR_REF(BgL_vectorz00_7792,BgL_arg1812z00_7791); }  else 
{ 
 obj_t BgL_auxz00_15190;
BgL_auxz00_15190 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7792, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7792)), 
(int)(BgL_arg1812z00_7791)); 
FAILURE(BgL_auxz00_15190,BFALSE,BFALSE);} } } } 
BgL_test5335z00_15170 = 
(BgL_arg1811z00_7790==BgL_classz00_7786); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7794;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7795;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7796; long BgL_arg1555z00_7797;
BgL_arg1554z00_7796 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7798;
BgL_arg1556z00_7798 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7787); 
BgL_arg1555z00_7797 = 
(BgL_arg1556z00_7798-OBJECT_TYPE); } 
BgL_oclassz00_7795 = 
VECTOR_REF(BgL_arg1554z00_7796,BgL_arg1555z00_7797); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7799;
BgL__ortest_1220z00_7799 = 
(BgL_classz00_7786==BgL_oclassz00_7795); 
if(BgL__ortest_1220z00_7799)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7794 = BgL__ortest_1220z00_7799; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7800;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7801;
BgL_arg1810z00_7801 = 
(BgL_oclassz00_7795); 
BgL_odepthz00_7800 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7801); } 
if(
(BgL_arg1804z00_7788<BgL_odepthz00_7800))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7802;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7803;
BgL_arg1809z00_7803 = 
(BgL_oclassz00_7795); 
BgL_arg1808z00_7802 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7803, BgL_arg1804z00_7788); } 
BgL_res2618z00_7794 = 
(BgL_arg1808z00_7802==BgL_classz00_7786); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7794 = ((bool_t)0); } } } } 
BgL_test5335z00_15170 = BgL_res2618z00_7794; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5335z00_15170 = ((bool_t)0)
; } } 
if(BgL_test5335z00_15170)
{ /* Llib/object.scm 1406 */
BgL_objz00_7804 = 
((BgL_objectz00_bglt)BgL_objz00_5373); }  else 
{ 
 obj_t BgL_auxz00_15212;
BgL_auxz00_15212 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61530L), BGl_string3907z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_5373); 
FAILURE(BgL_auxz00_15212,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1406 */
 obj_t BgL_portz00_7805;
if(
PAIRP(BgL_portz00_5374))
{ /* Llib/object.scm 1406 */
BgL_portz00_7805 = 
CAR(BgL_portz00_5374); }  else 
{ /* Llib/object.scm 1406 */
 obj_t BgL_tmpz00_15219;
BgL_tmpz00_15219 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_7805 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_15219); } 
{ /* Llib/object.scm 1407 */
 obj_t BgL_auxz00_15222;
if(
OUTPUT_PORTP(BgL_portz00_7805))
{ /* Llib/object.scm 1407 */
BgL_auxz00_15222 = BgL_portz00_7805
; }  else 
{ 
 obj_t BgL_auxz00_15225;
BgL_auxz00_15225 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61619L), BGl_string3870z00zz__objectz00, BGl_string3905z00zz__objectz00, BgL_portz00_7805); 
FAILURE(BgL_auxz00_15225,BFALSE,BFALSE);} 
return 
BGl_objectzd2printzd2zz__objectz00(BgL_objz00_7804, BgL_auxz00_15222, BGl_writezd2envzd2zz__r4_output_6_10_3z00);} } } } 

}



/* &object-display1389 */
obj_t BGl_z62objectzd2display1389zb0zz__objectz00(obj_t BgL_envz00_5375, obj_t BgL_objz00_5376, obj_t BgL_portz00_5377)
{
{ /* Llib/object.scm 1398 */
{ /* Llib/object.scm 1399 */
 BgL_objectz00_bglt BgL_objz00_7824;
{ /* Llib/object.scm 1399 */
 bool_t BgL_test5344z00_15230;
{ /* Llib/object.scm 1399 */
 obj_t BgL_classz00_7806;
BgL_classz00_7806 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objz00_5376))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7807; long BgL_arg1804z00_7808;
BgL_arg1803z00_7807 = 
(BgL_objectz00_bglt)(BgL_objz00_5376); 
BgL_arg1804z00_7808 = 
BGL_CLASS_DEPTH(BgL_classz00_7806); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7809;
BgL_idxz00_7809 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7807); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7810;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7811;
BgL_arg1812z00_7811 = 
(BgL_idxz00_7809+BgL_arg1804z00_7808); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7812;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7813;
BgL_aux3145z00_7813 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7813))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7812 = BgL_aux3145z00_7813; }  else 
{ 
 obj_t BgL_auxz00_15241;
BgL_auxz00_15241 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7813); 
FAILURE(BgL_auxz00_15241,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5348z00_15245;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15246;
BgL_tmpz00_15246 = 
VECTOR_LENGTH(BgL_vectorz00_7812); 
BgL_test5348z00_15245 = 
BOUND_CHECK(BgL_arg1812z00_7811, BgL_tmpz00_15246); } 
if(BgL_test5348z00_15245)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7810 = 
VECTOR_REF(BgL_vectorz00_7812,BgL_arg1812z00_7811); }  else 
{ 
 obj_t BgL_auxz00_15250;
BgL_auxz00_15250 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7812, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7812)), 
(int)(BgL_arg1812z00_7811)); 
FAILURE(BgL_auxz00_15250,BFALSE,BFALSE);} } } } 
BgL_test5344z00_15230 = 
(BgL_arg1811z00_7810==BgL_classz00_7806); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7814;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7815;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7816; long BgL_arg1555z00_7817;
BgL_arg1554z00_7816 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7818;
BgL_arg1556z00_7818 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7807); 
BgL_arg1555z00_7817 = 
(BgL_arg1556z00_7818-OBJECT_TYPE); } 
BgL_oclassz00_7815 = 
VECTOR_REF(BgL_arg1554z00_7816,BgL_arg1555z00_7817); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7819;
BgL__ortest_1220z00_7819 = 
(BgL_classz00_7806==BgL_oclassz00_7815); 
if(BgL__ortest_1220z00_7819)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7814 = BgL__ortest_1220z00_7819; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7820;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7821;
BgL_arg1810z00_7821 = 
(BgL_oclassz00_7815); 
BgL_odepthz00_7820 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7821); } 
if(
(BgL_arg1804z00_7808<BgL_odepthz00_7820))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7822;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7823;
BgL_arg1809z00_7823 = 
(BgL_oclassz00_7815); 
BgL_arg1808z00_7822 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7823, BgL_arg1804z00_7808); } 
BgL_res2618z00_7814 = 
(BgL_arg1808z00_7822==BgL_classz00_7806); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7814 = ((bool_t)0); } } } } 
BgL_test5344z00_15230 = BgL_res2618z00_7814; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5344z00_15230 = ((bool_t)0)
; } } 
if(BgL_test5344z00_15230)
{ /* Llib/object.scm 1399 */
BgL_objz00_7824 = 
((BgL_objectz00_bglt)BgL_objz00_5376); }  else 
{ 
 obj_t BgL_auxz00_15272;
BgL_auxz00_15272 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61149L), BGl_string3908z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_5376); 
FAILURE(BgL_auxz00_15272,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1399 */
 obj_t BgL_portz00_7825;
if(
PAIRP(BgL_portz00_5377))
{ /* Llib/object.scm 1399 */
BgL_portz00_7825 = 
CAR(BgL_portz00_5377); }  else 
{ /* Llib/object.scm 1399 */
 obj_t BgL_tmpz00_15279;
BgL_tmpz00_15279 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_7825 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_15279); } 
{ /* Llib/object.scm 1400 */
 obj_t BgL_auxz00_15282;
if(
OUTPUT_PORTP(BgL_portz00_7825))
{ /* Llib/object.scm 1400 */
BgL_auxz00_15282 = BgL_portz00_7825
; }  else 
{ 
 obj_t BgL_auxz00_15285;
BgL_auxz00_15285 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61238L), BGl_string3868z00zz__objectz00, BGl_string3905z00zz__objectz00, BgL_portz00_7825); 
FAILURE(BgL_auxz00_15285,BFALSE,BFALSE);} 
return 
BGl_objectzd2printzd2zz__objectz00(BgL_objz00_7824, BgL_auxz00_15282, BGl_displayzd2envzd2zz__r4_output_6_10_3z00);} } } } 

}



/* object-display */
BGL_EXPORTED_DEF obj_t BGl_objectzd2displayzd2zz__objectz00(BgL_objectz00_bglt BgL_objz00_163, obj_t BgL_portz00_164)
{
{ /* Llib/object.scm 1398 */
{ /* Llib/object.scm 1398 */
 obj_t BgL_method1391z00_2556;
{ /* Llib/object.scm 1398 */
 obj_t BgL_res2644z00_4395;
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_4366;
BgL_objzd2classzd2numz00_4366 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_163); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_4367;
BgL_arg1793z00_4367 = 
PROCEDURE_REF(BGl_objectzd2displayzd2envz00zz__objectz00, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 int BgL_offsetz00_4370;
BgL_offsetz00_4370 = 
(int)(BgL_objzd2classzd2numz00_4366); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_4371;
BgL_offsetz00_4371 = 
(
(long)(BgL_offsetz00_4370)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_4372;
BgL_modz00_4372 = 
(BgL_offsetz00_4371 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_4374;
BgL_restz00_4374 = 
(BgL_offsetz00_4371 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_4376;
BgL_bucketz00_4376 = 
VECTOR_REF(
((obj_t)BgL_arg1793z00_4367),BgL_modz00_4372); 
BgL_res2644z00_4395 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_4376),BgL_restz00_4374); } } } } } } } } 
BgL_method1391z00_2556 = BgL_res2644z00_4395; } 
{ /* Llib/object.scm 1398 */
 obj_t BgL_valz00_6315;
{ /* Llib/object.scm 1398 */
 obj_t BgL_list2204z00_2557;
BgL_list2204z00_2557 = 
MAKE_YOUNG_PAIR(BgL_portz00_164, BNIL); 
BgL_valz00_6315 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
((obj_t)BgL_objz00_163), BgL_list2204z00_2557); } 
{ /* Llib/object.scm 1398 */
 int BgL_len3408z00_6316;
BgL_len3408z00_6316 = 
(int)(
bgl_list_length(BgL_valz00_6315)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1391z00_2556, BgL_len3408z00_6316))
{ /* Llib/object.scm 1398 */
return 
apply(BgL_method1391z00_2556, BgL_valz00_6315);}  else 
{ /* Llib/object.scm 1398 */
FAILURE(BGl_symbol3909z00zz__objectz00,BGl_string3911z00zz__objectz00,BGl_list3912z00zz__objectz00);} } } } } 

}



/* &object-display */
obj_t BGl_z62objectzd2displayzb0zz__objectz00(obj_t BgL_envz00_5378, obj_t BgL_objz00_5379, obj_t BgL_portz00_5380)
{
{ /* Llib/object.scm 1398 */
{ /* Llib/object.scm 1398 */
 BgL_objectz00_bglt BgL_auxz00_15324;
{ /* Llib/object.scm 1398 */
 bool_t BgL_test5354z00_15325;
{ /* Llib/object.scm 1398 */
 obj_t BgL_classz00_7826;
BgL_classz00_7826 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objz00_5379))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7827; long BgL_arg1804z00_7828;
BgL_arg1803z00_7827 = 
(BgL_objectz00_bglt)(BgL_objz00_5379); 
BgL_arg1804z00_7828 = 
BGL_CLASS_DEPTH(BgL_classz00_7826); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7829;
BgL_idxz00_7829 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7827); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7830;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7831;
BgL_arg1812z00_7831 = 
(BgL_idxz00_7829+BgL_arg1804z00_7828); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7832;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7833;
BgL_aux3145z00_7833 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7833))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7832 = BgL_aux3145z00_7833; }  else 
{ 
 obj_t BgL_auxz00_15336;
BgL_auxz00_15336 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7833); 
FAILURE(BgL_auxz00_15336,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5358z00_15340;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15341;
BgL_tmpz00_15341 = 
VECTOR_LENGTH(BgL_vectorz00_7832); 
BgL_test5358z00_15340 = 
BOUND_CHECK(BgL_arg1812z00_7831, BgL_tmpz00_15341); } 
if(BgL_test5358z00_15340)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7830 = 
VECTOR_REF(BgL_vectorz00_7832,BgL_arg1812z00_7831); }  else 
{ 
 obj_t BgL_auxz00_15345;
BgL_auxz00_15345 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7832, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7832)), 
(int)(BgL_arg1812z00_7831)); 
FAILURE(BgL_auxz00_15345,BFALSE,BFALSE);} } } } 
BgL_test5354z00_15325 = 
(BgL_arg1811z00_7830==BgL_classz00_7826); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7834;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7835;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7836; long BgL_arg1555z00_7837;
BgL_arg1554z00_7836 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7838;
BgL_arg1556z00_7838 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7827); 
BgL_arg1555z00_7837 = 
(BgL_arg1556z00_7838-OBJECT_TYPE); } 
BgL_oclassz00_7835 = 
VECTOR_REF(BgL_arg1554z00_7836,BgL_arg1555z00_7837); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7839;
BgL__ortest_1220z00_7839 = 
(BgL_classz00_7826==BgL_oclassz00_7835); 
if(BgL__ortest_1220z00_7839)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7834 = BgL__ortest_1220z00_7839; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7840;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7841;
BgL_arg1810z00_7841 = 
(BgL_oclassz00_7835); 
BgL_odepthz00_7840 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7841); } 
if(
(BgL_arg1804z00_7828<BgL_odepthz00_7840))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7842;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7843;
BgL_arg1809z00_7843 = 
(BgL_oclassz00_7835); 
BgL_arg1808z00_7842 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7843, BgL_arg1804z00_7828); } 
BgL_res2618z00_7834 = 
(BgL_arg1808z00_7842==BgL_classz00_7826); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7834 = ((bool_t)0); } } } } 
BgL_test5354z00_15325 = BgL_res2618z00_7834; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5354z00_15325 = ((bool_t)0)
; } } 
if(BgL_test5354z00_15325)
{ /* Llib/object.scm 1398 */
BgL_auxz00_15324 = 
((BgL_objectz00_bglt)BgL_objz00_5379)
; }  else 
{ 
 obj_t BgL_auxz00_15367;
BgL_auxz00_15367 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61094L), BGl_string3933z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_5379); 
FAILURE(BgL_auxz00_15367,BFALSE,BFALSE);} } 
return 
BGl_objectzd2displayzd2zz__objectz00(BgL_auxz00_15324, BgL_portz00_5380);} } 

}



/* object-write */
BGL_EXPORTED_DEF obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt BgL_objz00_165, obj_t BgL_portz00_166)
{
{ /* Llib/object.scm 1405 */
{ /* Llib/object.scm 1405 */
 obj_t BgL_method1395z00_2558;
{ /* Llib/object.scm 1405 */
 obj_t BgL_res2649z00_4426;
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_4397;
BgL_objzd2classzd2numz00_4397 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_165); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_4398;
BgL_arg1793z00_4398 = 
PROCEDURE_REF(BGl_objectzd2writezd2envz00zz__objectz00, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 int BgL_offsetz00_4401;
BgL_offsetz00_4401 = 
(int)(BgL_objzd2classzd2numz00_4397); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_4402;
BgL_offsetz00_4402 = 
(
(long)(BgL_offsetz00_4401)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_4403;
BgL_modz00_4403 = 
(BgL_offsetz00_4402 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_4405;
BgL_restz00_4405 = 
(BgL_offsetz00_4402 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_4407;
BgL_bucketz00_4407 = 
VECTOR_REF(
((obj_t)BgL_arg1793z00_4398),BgL_modz00_4403); 
BgL_res2649z00_4426 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_4407),BgL_restz00_4405); } } } } } } } } 
BgL_method1395z00_2558 = BgL_res2649z00_4426; } 
{ /* Llib/object.scm 1405 */
 obj_t BgL_valz00_6322;
{ /* Llib/object.scm 1405 */
 obj_t BgL_list2205z00_2559;
BgL_list2205z00_2559 = 
MAKE_YOUNG_PAIR(BgL_portz00_166, BNIL); 
BgL_valz00_6322 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
((obj_t)BgL_objz00_165), BgL_list2205z00_2559); } 
{ /* Llib/object.scm 1405 */
 int BgL_len3411z00_6323;
BgL_len3411z00_6323 = 
(int)(
bgl_list_length(BgL_valz00_6322)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1395z00_2558, BgL_len3411z00_6323))
{ /* Llib/object.scm 1405 */
return 
apply(BgL_method1395z00_2558, BgL_valz00_6322);}  else 
{ /* Llib/object.scm 1405 */
FAILURE(BGl_symbol3934z00zz__objectz00,BGl_string3911z00zz__objectz00,BGl_list3936z00zz__objectz00);} } } } } 

}



/* &object-write */
obj_t BGl_z62objectzd2writezb0zz__objectz00(obj_t BgL_envz00_5381, obj_t BgL_objz00_5382, obj_t BgL_portz00_5383)
{
{ /* Llib/object.scm 1405 */
{ /* Llib/object.scm 1405 */
 BgL_objectz00_bglt BgL_auxz00_15406;
{ /* Llib/object.scm 1405 */
 bool_t BgL_test5362z00_15407;
{ /* Llib/object.scm 1405 */
 obj_t BgL_classz00_7844;
BgL_classz00_7844 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objz00_5382))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7845; long BgL_arg1804z00_7846;
BgL_arg1803z00_7845 = 
(BgL_objectz00_bglt)(BgL_objz00_5382); 
BgL_arg1804z00_7846 = 
BGL_CLASS_DEPTH(BgL_classz00_7844); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7847;
BgL_idxz00_7847 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7845); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7848;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7849;
BgL_arg1812z00_7849 = 
(BgL_idxz00_7847+BgL_arg1804z00_7846); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7850;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7851;
BgL_aux3145z00_7851 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7851))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7850 = BgL_aux3145z00_7851; }  else 
{ 
 obj_t BgL_auxz00_15418;
BgL_auxz00_15418 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7851); 
FAILURE(BgL_auxz00_15418,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5366z00_15422;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15423;
BgL_tmpz00_15423 = 
VECTOR_LENGTH(BgL_vectorz00_7850); 
BgL_test5366z00_15422 = 
BOUND_CHECK(BgL_arg1812z00_7849, BgL_tmpz00_15423); } 
if(BgL_test5366z00_15422)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7848 = 
VECTOR_REF(BgL_vectorz00_7850,BgL_arg1812z00_7849); }  else 
{ 
 obj_t BgL_auxz00_15427;
BgL_auxz00_15427 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7850, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7850)), 
(int)(BgL_arg1812z00_7849)); 
FAILURE(BgL_auxz00_15427,BFALSE,BFALSE);} } } } 
BgL_test5362z00_15407 = 
(BgL_arg1811z00_7848==BgL_classz00_7844); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7852;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7853;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7854; long BgL_arg1555z00_7855;
BgL_arg1554z00_7854 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7856;
BgL_arg1556z00_7856 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7845); 
BgL_arg1555z00_7855 = 
(BgL_arg1556z00_7856-OBJECT_TYPE); } 
BgL_oclassz00_7853 = 
VECTOR_REF(BgL_arg1554z00_7854,BgL_arg1555z00_7855); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7857;
BgL__ortest_1220z00_7857 = 
(BgL_classz00_7844==BgL_oclassz00_7853); 
if(BgL__ortest_1220z00_7857)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7852 = BgL__ortest_1220z00_7857; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7858;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7859;
BgL_arg1810z00_7859 = 
(BgL_oclassz00_7853); 
BgL_odepthz00_7858 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7859); } 
if(
(BgL_arg1804z00_7846<BgL_odepthz00_7858))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7860;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7861;
BgL_arg1809z00_7861 = 
(BgL_oclassz00_7853); 
BgL_arg1808z00_7860 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7861, BgL_arg1804z00_7846); } 
BgL_res2618z00_7852 = 
(BgL_arg1808z00_7860==BgL_classz00_7844); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7852 = ((bool_t)0); } } } } 
BgL_test5362z00_15407 = BgL_res2618z00_7852; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5362z00_15407 = ((bool_t)0)
; } } 
if(BgL_test5362z00_15407)
{ /* Llib/object.scm 1405 */
BgL_auxz00_15406 = 
((BgL_objectz00_bglt)BgL_objz00_5382)
; }  else 
{ 
 obj_t BgL_auxz00_15449;
BgL_auxz00_15449 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61477L), BGl_string3945z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_5382); 
FAILURE(BgL_auxz00_15449,BFALSE,BFALSE);} } 
return 
BGl_objectzd2writezd2zz__objectz00(BgL_auxz00_15406, BgL_portz00_5383);} } 

}



/* object-hashnumber */
BGL_EXPORTED_DEF long BGl_objectzd2hashnumberzd2zz__objectz00(BgL_objectz00_bglt BgL_objectz00_167)
{
{ /* Llib/object.scm 1412 */
{ /* Llib/object.scm 1412 */
 obj_t BgL_method1398z00_2560;
{ /* Llib/object.scm 1412 */
 obj_t BgL_res2654z00_4457;
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_4428;
BgL_objzd2classzd2numz00_4428 = 
BGL_OBJECT_CLASS_NUM(BgL_objectz00_167); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_4429;
BgL_arg1793z00_4429 = 
PROCEDURE_REF(BGl_objectzd2hashnumberzd2envz00zz__objectz00, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 int BgL_offsetz00_4432;
BgL_offsetz00_4432 = 
(int)(BgL_objzd2classzd2numz00_4428); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_4433;
BgL_offsetz00_4433 = 
(
(long)(BgL_offsetz00_4432)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_4434;
BgL_modz00_4434 = 
(BgL_offsetz00_4433 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_4436;
BgL_restz00_4436 = 
(BgL_offsetz00_4433 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_4438;
BgL_bucketz00_4438 = 
VECTOR_REF(
((obj_t)BgL_arg1793z00_4429),BgL_modz00_4434); 
BgL_res2654z00_4457 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_4438),BgL_restz00_4436); } } } } } } } } 
BgL_method1398z00_2560 = BgL_res2654z00_4457; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1398z00_2560, 1))
{ /* Llib/object.scm 1412 */
 obj_t BgL_tmpz00_15480;
{ /* Llib/object.scm 1412 */
 obj_t BgL_aux3415z00_6329;
BgL_aux3415z00_6329 = 
BGL_PROCEDURE_CALL1(BgL_method1398z00_2560, 
((obj_t)BgL_objectz00_167)); 
if(
INTEGERP(BgL_aux3415z00_6329))
{ /* Llib/object.scm 1412 */
BgL_tmpz00_15480 = BgL_aux3415z00_6329
; }  else 
{ 
 obj_t BgL_auxz00_15488;
BgL_auxz00_15488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61856L), BGl_string3946z00zz__objectz00, BGl_string3443z00zz__objectz00, BgL_aux3415z00_6329); 
FAILURE(BgL_auxz00_15488,BFALSE,BFALSE);} } 
return 
(long)CINT(BgL_tmpz00_15480);}  else 
{ /* Llib/object.scm 1412 */
FAILURE(BGl_string3947z00zz__objectz00,BGl_list3948z00zz__objectz00,BgL_method1398z00_2560);} } } 

}



/* &object-hashnumber */
obj_t BGl_z62objectzd2hashnumberzb0zz__objectz00(obj_t BgL_envz00_5384, obj_t BgL_objectz00_5385)
{
{ /* Llib/object.scm 1412 */
{ /* Llib/object.scm 1412 */
 long BgL_tmpz00_15494;
{ /* Llib/object.scm 1412 */
 BgL_objectz00_bglt BgL_auxz00_15495;
{ /* Llib/object.scm 1412 */
 bool_t BgL_test5371z00_15496;
{ /* Llib/object.scm 1412 */
 obj_t BgL_classz00_7862;
BgL_classz00_7862 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objectz00_5385))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7863; long BgL_arg1804z00_7864;
BgL_arg1803z00_7863 = 
(BgL_objectz00_bglt)(BgL_objectz00_5385); 
BgL_arg1804z00_7864 = 
BGL_CLASS_DEPTH(BgL_classz00_7862); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7865;
BgL_idxz00_7865 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7863); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7866;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7867;
BgL_arg1812z00_7867 = 
(BgL_idxz00_7865+BgL_arg1804z00_7864); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7868;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7869;
BgL_aux3145z00_7869 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7869))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7868 = BgL_aux3145z00_7869; }  else 
{ 
 obj_t BgL_auxz00_15507;
BgL_auxz00_15507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7869); 
FAILURE(BgL_auxz00_15507,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5375z00_15511;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15512;
BgL_tmpz00_15512 = 
VECTOR_LENGTH(BgL_vectorz00_7868); 
BgL_test5375z00_15511 = 
BOUND_CHECK(BgL_arg1812z00_7867, BgL_tmpz00_15512); } 
if(BgL_test5375z00_15511)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7866 = 
VECTOR_REF(BgL_vectorz00_7868,BgL_arg1812z00_7867); }  else 
{ 
 obj_t BgL_auxz00_15516;
BgL_auxz00_15516 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7868, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7868)), 
(int)(BgL_arg1812z00_7867)); 
FAILURE(BgL_auxz00_15516,BFALSE,BFALSE);} } } } 
BgL_test5371z00_15496 = 
(BgL_arg1811z00_7866==BgL_classz00_7862); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7870;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7871;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7872; long BgL_arg1555z00_7873;
BgL_arg1554z00_7872 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7874;
BgL_arg1556z00_7874 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7863); 
BgL_arg1555z00_7873 = 
(BgL_arg1556z00_7874-OBJECT_TYPE); } 
BgL_oclassz00_7871 = 
VECTOR_REF(BgL_arg1554z00_7872,BgL_arg1555z00_7873); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7875;
BgL__ortest_1220z00_7875 = 
(BgL_classz00_7862==BgL_oclassz00_7871); 
if(BgL__ortest_1220z00_7875)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7870 = BgL__ortest_1220z00_7875; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7876;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7877;
BgL_arg1810z00_7877 = 
(BgL_oclassz00_7871); 
BgL_odepthz00_7876 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7877); } 
if(
(BgL_arg1804z00_7864<BgL_odepthz00_7876))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7878;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7879;
BgL_arg1809z00_7879 = 
(BgL_oclassz00_7871); 
BgL_arg1808z00_7878 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7879, BgL_arg1804z00_7864); } 
BgL_res2618z00_7870 = 
(BgL_arg1808z00_7878==BgL_classz00_7862); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7870 = ((bool_t)0); } } } } 
BgL_test5371z00_15496 = BgL_res2618z00_7870; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5371z00_15496 = ((bool_t)0)
; } } 
if(BgL_test5371z00_15496)
{ /* Llib/object.scm 1412 */
BgL_auxz00_15495 = 
((BgL_objectz00_bglt)BgL_objectz00_5385)
; }  else 
{ 
 obj_t BgL_auxz00_15538;
BgL_auxz00_15538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(61856L), BGl_string3951z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objectz00_5385); 
FAILURE(BgL_auxz00_15538,BFALSE,BFALSE);} } 
BgL_tmpz00_15494 = 
BGl_objectzd2hashnumberzd2zz__objectz00(BgL_auxz00_15495); } 
return 
BINT(BgL_tmpz00_15494);} } 

}



/* object-print */
BGL_EXPORTED_DEF obj_t BGl_objectzd2printzd2zz__objectz00(BgL_objectz00_bglt BgL_objz00_170, obj_t BgL_portz00_171, obj_t BgL_printzd2slotzd2_172)
{
{ /* Llib/object.scm 1445 */
{ /* Llib/object.scm 1445 */
 obj_t BgL_method1401z00_2561;
{ /* Llib/object.scm 1445 */
 obj_t BgL_res2659z00_4488;
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_4459;
BgL_objzd2classzd2numz00_4459 = 
BGL_OBJECT_CLASS_NUM(BgL_objz00_170); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_4460;
BgL_arg1793z00_4460 = 
PROCEDURE_REF(BGl_objectzd2printzd2envz00zz__objectz00, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 int BgL_offsetz00_4463;
BgL_offsetz00_4463 = 
(int)(BgL_objzd2classzd2numz00_4459); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_4464;
BgL_offsetz00_4464 = 
(
(long)(BgL_offsetz00_4463)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_4465;
BgL_modz00_4465 = 
(BgL_offsetz00_4464 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_4467;
BgL_restz00_4467 = 
(BgL_offsetz00_4464 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_4469;
BgL_bucketz00_4469 = 
VECTOR_REF(
((obj_t)BgL_arg1793z00_4460),BgL_modz00_4465); 
BgL_res2659z00_4488 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_4469),BgL_restz00_4467); } } } } } } } } 
BgL_method1401z00_2561 = BgL_res2659z00_4488; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1401z00_2561, 3))
{ /* Llib/object.scm 1445 */
return 
BGL_PROCEDURE_CALL3(BgL_method1401z00_2561, 
((obj_t)BgL_objz00_170), BgL_portz00_171, BgL_printzd2slotzd2_172);}  else 
{ /* Llib/object.scm 1445 */
FAILURE(BGl_string3952z00zz__objectz00,BGl_list3953z00zz__objectz00,BgL_method1401z00_2561);} } } 

}



/* &object-print */
obj_t BGl_z62objectzd2printzb0zz__objectz00(obj_t BgL_envz00_5386, obj_t BgL_objz00_5387, obj_t BgL_portz00_5388, obj_t BgL_printzd2slotzd2_5389)
{
{ /* Llib/object.scm 1445 */
{ /* Llib/object.scm 1445 */
 obj_t BgL_auxz00_15632; obj_t BgL_auxz00_15625; BgL_objectz00_bglt BgL_auxz00_15578;
if(
PROCEDUREP(BgL_printzd2slotzd2_5389))
{ /* Llib/object.scm 1445 */
BgL_auxz00_15632 = BgL_printzd2slotzd2_5389
; }  else 
{ 
 obj_t BgL_auxz00_15635;
BgL_auxz00_15635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63293L), BGl_string3956z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_printzd2slotzd2_5389); 
FAILURE(BgL_auxz00_15635,BFALSE,BFALSE);} 
if(
OUTPUT_PORTP(BgL_portz00_5388))
{ /* Llib/object.scm 1445 */
BgL_auxz00_15625 = BgL_portz00_5388
; }  else 
{ 
 obj_t BgL_auxz00_15628;
BgL_auxz00_15628 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63293L), BGl_string3956z00zz__objectz00, BGl_string3905z00zz__objectz00, BgL_portz00_5388); 
FAILURE(BgL_auxz00_15628,BFALSE,BFALSE);} 
{ /* Llib/object.scm 1445 */
 bool_t BgL_test5379z00_15579;
{ /* Llib/object.scm 1445 */
 obj_t BgL_classz00_7880;
BgL_classz00_7880 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_objz00_5387))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7881; long BgL_arg1804z00_7882;
BgL_arg1803z00_7881 = 
(BgL_objectz00_bglt)(BgL_objz00_5387); 
BgL_arg1804z00_7882 = 
BGL_CLASS_DEPTH(BgL_classz00_7880); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7883;
BgL_idxz00_7883 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7881); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7884;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7885;
BgL_arg1812z00_7885 = 
(BgL_idxz00_7883+BgL_arg1804z00_7882); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7886;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7887;
BgL_aux3145z00_7887 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7887))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7886 = BgL_aux3145z00_7887; }  else 
{ 
 obj_t BgL_auxz00_15590;
BgL_auxz00_15590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7887); 
FAILURE(BgL_auxz00_15590,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5383z00_15594;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15595;
BgL_tmpz00_15595 = 
VECTOR_LENGTH(BgL_vectorz00_7886); 
BgL_test5383z00_15594 = 
BOUND_CHECK(BgL_arg1812z00_7885, BgL_tmpz00_15595); } 
if(BgL_test5383z00_15594)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7884 = 
VECTOR_REF(BgL_vectorz00_7886,BgL_arg1812z00_7885); }  else 
{ 
 obj_t BgL_auxz00_15599;
BgL_auxz00_15599 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7886, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7886)), 
(int)(BgL_arg1812z00_7885)); 
FAILURE(BgL_auxz00_15599,BFALSE,BFALSE);} } } } 
BgL_test5379z00_15579 = 
(BgL_arg1811z00_7884==BgL_classz00_7880); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7888;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7889;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7890; long BgL_arg1555z00_7891;
BgL_arg1554z00_7890 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7892;
BgL_arg1556z00_7892 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7881); 
BgL_arg1555z00_7891 = 
(BgL_arg1556z00_7892-OBJECT_TYPE); } 
BgL_oclassz00_7889 = 
VECTOR_REF(BgL_arg1554z00_7890,BgL_arg1555z00_7891); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7893;
BgL__ortest_1220z00_7893 = 
(BgL_classz00_7880==BgL_oclassz00_7889); 
if(BgL__ortest_1220z00_7893)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7888 = BgL__ortest_1220z00_7893; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7894;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7895;
BgL_arg1810z00_7895 = 
(BgL_oclassz00_7889); 
BgL_odepthz00_7894 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7895); } 
if(
(BgL_arg1804z00_7882<BgL_odepthz00_7894))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7896;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7897;
BgL_arg1809z00_7897 = 
(BgL_oclassz00_7889); 
BgL_arg1808z00_7896 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7897, BgL_arg1804z00_7882); } 
BgL_res2618z00_7888 = 
(BgL_arg1808z00_7896==BgL_classz00_7880); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7888 = ((bool_t)0); } } } } 
BgL_test5379z00_15579 = BgL_res2618z00_7888; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5379z00_15579 = ((bool_t)0)
; } } 
if(BgL_test5379z00_15579)
{ /* Llib/object.scm 1445 */
BgL_auxz00_15578 = 
((BgL_objectz00_bglt)BgL_objz00_5387)
; }  else 
{ 
 obj_t BgL_auxz00_15621;
BgL_auxz00_15621 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(63293L), BGl_string3956z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_objz00_5387); 
FAILURE(BgL_auxz00_15621,BFALSE,BFALSE);} } 
return 
BGl_objectzd2printzd2zz__objectz00(BgL_auxz00_15578, BgL_auxz00_15625, BgL_auxz00_15632);} } 

}



/* object-equal? */
BGL_EXPORTED_DEF bool_t BGl_objectzd2equalzf3z21zz__objectz00(BgL_objectz00_bglt BgL_obj1z00_173, BgL_objectz00_bglt BgL_obj2z00_174)
{
{ /* Llib/object.scm 1475 */
{ /* Llib/object.scm 1475 */
 obj_t BgL_method1403z00_2562;
{ /* Llib/object.scm 1475 */
 obj_t BgL_res2664z00_4519;
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_4490;
BgL_objzd2classzd2numz00_4490 = 
BGL_OBJECT_CLASS_NUM(BgL_obj1z00_173); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_4491;
BgL_arg1793z00_4491 = 
PROCEDURE_REF(BGl_objectzd2equalzf3zd2envzf3zz__objectz00, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 int BgL_offsetz00_4494;
BgL_offsetz00_4494 = 
(int)(BgL_objzd2classzd2numz00_4490); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_4495;
BgL_offsetz00_4495 = 
(
(long)(BgL_offsetz00_4494)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_4496;
BgL_modz00_4496 = 
(BgL_offsetz00_4495 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_4498;
BgL_restz00_4498 = 
(BgL_offsetz00_4495 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_4500;
BgL_bucketz00_4500 = 
VECTOR_REF(
((obj_t)BgL_arg1793z00_4491),BgL_modz00_4496); 
BgL_res2664z00_4519 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_4500),BgL_restz00_4498); } } } } } } } } 
BgL_method1403z00_2562 = BgL_res2664z00_4519; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1403z00_2562, 2))
{ /* Llib/object.scm 1475 */
return 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_method1403z00_2562, 
((obj_t)BgL_obj1z00_173), 
((obj_t)BgL_obj2z00_174)));}  else 
{ /* Llib/object.scm 1475 */
FAILURE(BGl_string3957z00zz__objectz00,BGl_list3958z00zz__objectz00,BgL_method1403z00_2562);} } } 

}



/* &object-equal? */
obj_t BGl_z62objectzd2equalzf3z43zz__objectz00(obj_t BgL_envz00_5390, obj_t BgL_obj1z00_5391, obj_t BgL_obj2z00_5392)
{
{ /* Llib/object.scm 1475 */
{ /* Llib/object.scm 1475 */
 bool_t BgL_tmpz00_15675;
{ /* Llib/object.scm 1475 */
 BgL_objectz00_bglt BgL_auxz00_15723; BgL_objectz00_bglt BgL_auxz00_15676;
{ /* Llib/object.scm 1475 */
 bool_t BgL_test5396z00_15724;
{ /* Llib/object.scm 1475 */
 obj_t BgL_classz00_7916;
BgL_classz00_7916 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_obj2z00_5392))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7917; long BgL_arg1804z00_7918;
BgL_arg1803z00_7917 = 
(BgL_objectz00_bglt)(BgL_obj2z00_5392); 
BgL_arg1804z00_7918 = 
BGL_CLASS_DEPTH(BgL_classz00_7916); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7919;
BgL_idxz00_7919 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7917); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7920;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7921;
BgL_arg1812z00_7921 = 
(BgL_idxz00_7919+BgL_arg1804z00_7918); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7922;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7923;
BgL_aux3145z00_7923 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7923))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7922 = BgL_aux3145z00_7923; }  else 
{ 
 obj_t BgL_auxz00_15735;
BgL_auxz00_15735 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7923); 
FAILURE(BgL_auxz00_15735,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5400z00_15739;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15740;
BgL_tmpz00_15740 = 
VECTOR_LENGTH(BgL_vectorz00_7922); 
BgL_test5400z00_15739 = 
BOUND_CHECK(BgL_arg1812z00_7921, BgL_tmpz00_15740); } 
if(BgL_test5400z00_15739)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7920 = 
VECTOR_REF(BgL_vectorz00_7922,BgL_arg1812z00_7921); }  else 
{ 
 obj_t BgL_auxz00_15744;
BgL_auxz00_15744 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7922, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7922)), 
(int)(BgL_arg1812z00_7921)); 
FAILURE(BgL_auxz00_15744,BFALSE,BFALSE);} } } } 
BgL_test5396z00_15724 = 
(BgL_arg1811z00_7920==BgL_classz00_7916); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7924;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7925;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7926; long BgL_arg1555z00_7927;
BgL_arg1554z00_7926 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7928;
BgL_arg1556z00_7928 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7917); 
BgL_arg1555z00_7927 = 
(BgL_arg1556z00_7928-OBJECT_TYPE); } 
BgL_oclassz00_7925 = 
VECTOR_REF(BgL_arg1554z00_7926,BgL_arg1555z00_7927); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7929;
BgL__ortest_1220z00_7929 = 
(BgL_classz00_7916==BgL_oclassz00_7925); 
if(BgL__ortest_1220z00_7929)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7924 = BgL__ortest_1220z00_7929; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7930;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7931;
BgL_arg1810z00_7931 = 
(BgL_oclassz00_7925); 
BgL_odepthz00_7930 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7931); } 
if(
(BgL_arg1804z00_7918<BgL_odepthz00_7930))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7932;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7933;
BgL_arg1809z00_7933 = 
(BgL_oclassz00_7925); 
BgL_arg1808z00_7932 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7933, BgL_arg1804z00_7918); } 
BgL_res2618z00_7924 = 
(BgL_arg1808z00_7932==BgL_classz00_7916); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7924 = ((bool_t)0); } } } } 
BgL_test5396z00_15724 = BgL_res2618z00_7924; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5396z00_15724 = ((bool_t)0)
; } } 
if(BgL_test5396z00_15724)
{ /* Llib/object.scm 1475 */
BgL_auxz00_15723 = 
((BgL_objectz00_bglt)BgL_obj2z00_5392)
; }  else 
{ 
 obj_t BgL_auxz00_15766;
BgL_auxz00_15766 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(64288L), BGl_string3961z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_obj2z00_5392); 
FAILURE(BgL_auxz00_15766,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1475 */
 bool_t BgL_test5389z00_15677;
{ /* Llib/object.scm 1475 */
 obj_t BgL_classz00_7898;
BgL_classz00_7898 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_obj1z00_5391))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7899; long BgL_arg1804z00_7900;
BgL_arg1803z00_7899 = 
(BgL_objectz00_bglt)(BgL_obj1z00_5391); 
BgL_arg1804z00_7900 = 
BGL_CLASS_DEPTH(BgL_classz00_7898); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7901;
BgL_idxz00_7901 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7899); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7902;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7903;
BgL_arg1812z00_7903 = 
(BgL_idxz00_7901+BgL_arg1804z00_7900); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7904;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7905;
BgL_aux3145z00_7905 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7905))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7904 = BgL_aux3145z00_7905; }  else 
{ 
 obj_t BgL_auxz00_15688;
BgL_auxz00_15688 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7905); 
FAILURE(BgL_auxz00_15688,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5393z00_15692;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15693;
BgL_tmpz00_15693 = 
VECTOR_LENGTH(BgL_vectorz00_7904); 
BgL_test5393z00_15692 = 
BOUND_CHECK(BgL_arg1812z00_7903, BgL_tmpz00_15693); } 
if(BgL_test5393z00_15692)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7902 = 
VECTOR_REF(BgL_vectorz00_7904,BgL_arg1812z00_7903); }  else 
{ 
 obj_t BgL_auxz00_15697;
BgL_auxz00_15697 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7904, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7904)), 
(int)(BgL_arg1812z00_7903)); 
FAILURE(BgL_auxz00_15697,BFALSE,BFALSE);} } } } 
BgL_test5389z00_15677 = 
(BgL_arg1811z00_7902==BgL_classz00_7898); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7906;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7907;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7908; long BgL_arg1555z00_7909;
BgL_arg1554z00_7908 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7910;
BgL_arg1556z00_7910 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7899); 
BgL_arg1555z00_7909 = 
(BgL_arg1556z00_7910-OBJECT_TYPE); } 
BgL_oclassz00_7907 = 
VECTOR_REF(BgL_arg1554z00_7908,BgL_arg1555z00_7909); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7911;
BgL__ortest_1220z00_7911 = 
(BgL_classz00_7898==BgL_oclassz00_7907); 
if(BgL__ortest_1220z00_7911)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7906 = BgL__ortest_1220z00_7911; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7912;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7913;
BgL_arg1810z00_7913 = 
(BgL_oclassz00_7907); 
BgL_odepthz00_7912 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7913); } 
if(
(BgL_arg1804z00_7900<BgL_odepthz00_7912))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7914;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7915;
BgL_arg1809z00_7915 = 
(BgL_oclassz00_7907); 
BgL_arg1808z00_7914 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7915, BgL_arg1804z00_7900); } 
BgL_res2618z00_7906 = 
(BgL_arg1808z00_7914==BgL_classz00_7898); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7906 = ((bool_t)0); } } } } 
BgL_test5389z00_15677 = BgL_res2618z00_7906; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5389z00_15677 = ((bool_t)0)
; } } 
if(BgL_test5389z00_15677)
{ /* Llib/object.scm 1475 */
BgL_auxz00_15676 = 
((BgL_objectz00_bglt)BgL_obj1z00_5391)
; }  else 
{ 
 obj_t BgL_auxz00_15719;
BgL_auxz00_15719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(64288L), BGl_string3961z00zz__objectz00, BGl_string3528z00zz__objectz00, BgL_obj1z00_5391); 
FAILURE(BgL_auxz00_15719,BFALSE,BFALSE);} } 
BgL_tmpz00_15675 = 
BGl_objectzd2equalzf3z21zz__objectz00(BgL_auxz00_15676, BgL_auxz00_15723); } 
return 
BBOOL(BgL_tmpz00_15675);} } 

}



/* exception-notify */
BGL_EXPORTED_DEF obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t BgL_excz00_175)
{
{ /* Llib/object.scm 1495 */
if(
BGL_OBJECTP(BgL_excz00_175))
{ /* Llib/object.scm 1495 */
 obj_t BgL_method1408z00_2564;
{ /* Llib/object.scm 1495 */
 obj_t BgL_res2669z00_4550;
{ /* Llib/object.scm 1284 */
 long BgL_objzd2classzd2numz00_4521;
BgL_objzd2classzd2numz00_4521 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_excz00_175)); 
{ /* Llib/object.scm 1285 */
 obj_t BgL_arg1793z00_4522;
BgL_arg1793z00_4522 = 
PROCEDURE_REF(BGl_exceptionzd2notifyzd2envz00zz__objectz00, 
(int)(1L)); 
{ /* Llib/object.scm 1285 */
 int BgL_offsetz00_4525;
BgL_offsetz00_4525 = 
(int)(BgL_objzd2classzd2numz00_4521); 
{ /* Llib/object.scm 931 */
 long BgL_offsetz00_4526;
BgL_offsetz00_4526 = 
(
(long)(BgL_offsetz00_4525)-OBJECT_TYPE); 
{ /* Llib/object.scm 931 */
 long BgL_modz00_4527;
BgL_modz00_4527 = 
(BgL_offsetz00_4526 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/object.scm 932 */
 long BgL_restz00_4529;
BgL_restz00_4529 = 
(BgL_offsetz00_4526 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/object.scm 933 */

{ /* Llib/object.scm 934 */
 obj_t BgL_bucketz00_4531;
BgL_bucketz00_4531 = 
VECTOR_REF(
((obj_t)BgL_arg1793z00_4522),BgL_modz00_4527); 
BgL_res2669z00_4550 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_4531),BgL_restz00_4529); } } } } } } } } 
BgL_method1408z00_2564 = BgL_res2669z00_4550; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1408z00_2564, 1))
{ /* Llib/object.scm 1495 */
return 
BGL_PROCEDURE_CALL1(BgL_method1408z00_2564, BgL_excz00_175);}  else 
{ /* Llib/object.scm 1495 */
FAILURE(BGl_string3962z00zz__objectz00,BGl_list3963z00zz__objectz00,BgL_method1408z00_2564);} }  else 
{ /* Llib/object.scm 1495 */
 obj_t BgL_fun2207z00_2565;
{ /* Llib/object.scm 1495 */
 obj_t BgL_res2670z00_4551;
{ /* Llib/object.scm 900 */
 obj_t BgL_aux3431z00_6357;
BgL_aux3431z00_6357 = 
PROCEDURE_REF(BGl_exceptionzd2notifyzd2envz00zz__objectz00, 
(int)(0L)); 
if(
PROCEDUREP(BgL_aux3431z00_6357))
{ /* Llib/object.scm 900 */
BgL_res2670z00_4551 = BgL_aux3431z00_6357; }  else 
{ 
 obj_t BgL_auxz00_15810;
BgL_auxz00_15810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(39357L), BGl_string3968z00zz__objectz00, BGl_string3473z00zz__objectz00, BgL_aux3431z00_6357); 
FAILURE(BgL_auxz00_15810,BFALSE,BFALSE);} } 
BgL_fun2207z00_2565 = BgL_res2670z00_4551; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fun2207z00_2565, 1))
{ /* Llib/object.scm 1495 */
return 
BGL_PROCEDURE_CALL1(BgL_fun2207z00_2565, BgL_excz00_175);}  else 
{ /* Llib/object.scm 1495 */
FAILURE(BGl_string3962z00zz__objectz00,BGl_list3969z00zz__objectz00,BgL_fun2207z00_2565);} } } 

}



/* &exception-notify */
obj_t BGl_z62exceptionzd2notifyzb0zz__objectz00(obj_t BgL_envz00_5393, obj_t BgL_excz00_5394)
{
{ /* Llib/object.scm 1495 */
return 
BGl_exceptionzd2notifyzd2zz__objectz00(BgL_excz00_5394);} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__objectz00(void)
{
{ /* Llib/object.scm 17 */
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_exceptionzd2notifyzd2envz00zz__objectz00, BGl_z62errorz62zz__objectz00, BGl_proc3972z00zz__objectz00, BGl_string3968z00zz__objectz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_exceptionzd2notifyzd2envz00zz__objectz00, BGl_z62iozd2writezd2errorz62zz__objectz00, BGl_proc3973z00zz__objectz00, BGl_string3968z00zz__objectz00); 
return 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_exceptionzd2notifyzd2envz00zz__objectz00, BGl_z62warningz62zz__objectz00, BGl_proc3974z00zz__objectz00, BGl_string3968z00zz__objectz00);} 

}



/* &exception-notify-&wa1416 */
obj_t BGl_z62exceptionzd2notifyzd2z62wa1416z00zz__objectz00(obj_t BgL_envz00_5398, obj_t BgL_excz00_5399)
{
{ /* Llib/object.scm 1527 */
{ /* Llib/object.scm 1528 */
 BgL_z62warningz62_bglt BgL_excz00_7952;
{ /* Llib/object.scm 1528 */
 bool_t BgL_test5407z00_15825;
{ /* Llib/object.scm 1528 */
 obj_t BgL_classz00_7934;
BgL_classz00_7934 = BGl_z62warningz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_excz00_5399))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7935; long BgL_arg1804z00_7936;
BgL_arg1803z00_7935 = 
(BgL_objectz00_bglt)(BgL_excz00_5399); 
BgL_arg1804z00_7936 = 
BGL_CLASS_DEPTH(BgL_classz00_7934); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7937;
BgL_idxz00_7937 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7935); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7938;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7939;
BgL_arg1812z00_7939 = 
(BgL_idxz00_7937+BgL_arg1804z00_7936); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7940;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7941;
BgL_aux3145z00_7941 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7941))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7940 = BgL_aux3145z00_7941; }  else 
{ 
 obj_t BgL_auxz00_15836;
BgL_auxz00_15836 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7941); 
FAILURE(BgL_auxz00_15836,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5411z00_15840;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15841;
BgL_tmpz00_15841 = 
VECTOR_LENGTH(BgL_vectorz00_7940); 
BgL_test5411z00_15840 = 
BOUND_CHECK(BgL_arg1812z00_7939, BgL_tmpz00_15841); } 
if(BgL_test5411z00_15840)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7938 = 
VECTOR_REF(BgL_vectorz00_7940,BgL_arg1812z00_7939); }  else 
{ 
 obj_t BgL_auxz00_15845;
BgL_auxz00_15845 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7940, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7940)), 
(int)(BgL_arg1812z00_7939)); 
FAILURE(BgL_auxz00_15845,BFALSE,BFALSE);} } } } 
BgL_test5407z00_15825 = 
(BgL_arg1811z00_7938==BgL_classz00_7934); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7942;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7943;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7944; long BgL_arg1555z00_7945;
BgL_arg1554z00_7944 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7946;
BgL_arg1556z00_7946 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7935); 
BgL_arg1555z00_7945 = 
(BgL_arg1556z00_7946-OBJECT_TYPE); } 
BgL_oclassz00_7943 = 
VECTOR_REF(BgL_arg1554z00_7944,BgL_arg1555z00_7945); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7947;
BgL__ortest_1220z00_7947 = 
(BgL_classz00_7934==BgL_oclassz00_7943); 
if(BgL__ortest_1220z00_7947)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7942 = BgL__ortest_1220z00_7947; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7948;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7949;
BgL_arg1810z00_7949 = 
(BgL_oclassz00_7943); 
BgL_odepthz00_7948 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7949); } 
if(
(BgL_arg1804z00_7936<BgL_odepthz00_7948))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7950;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7951;
BgL_arg1809z00_7951 = 
(BgL_oclassz00_7943); 
BgL_arg1808z00_7950 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7951, BgL_arg1804z00_7936); } 
BgL_res2618z00_7942 = 
(BgL_arg1808z00_7950==BgL_classz00_7934); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7942 = ((bool_t)0); } } } } 
BgL_test5407z00_15825 = BgL_res2618z00_7942; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5407z00_15825 = ((bool_t)0)
; } } 
if(BgL_test5407z00_15825)
{ /* Llib/object.scm 1528 */
BgL_excz00_7952 = 
((BgL_z62warningz62_bglt)BgL_excz00_5399); }  else 
{ 
 obj_t BgL_auxz00_15867;
BgL_auxz00_15867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(66488L), BGl_string3975z00zz__objectz00, BGl_string3811z00zz__objectz00, BgL_excz00_5399); 
FAILURE(BgL_auxz00_15867,BFALSE,BFALSE);} } 
return 
BGl_warningzd2notifyzd2zz__errorz00(
((obj_t)BgL_excz00_7952));} } 

}



/* &exception-notify-&io1414 */
obj_t BGl_z62exceptionzd2notifyzd2z62io1414z00zz__objectz00(obj_t BgL_envz00_5400, obj_t BgL_excz00_5401)
{
{ /* Llib/object.scm 1519 */
{ /* Llib/object.scm 1519 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_excz00_7971;
{ /* Llib/object.scm 1519 */
 bool_t BgL_test5414z00_15873;
{ /* Llib/object.scm 1519 */
 obj_t BgL_classz00_7953;
BgL_classz00_7953 = BGl_z62iozd2writezd2errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_excz00_5401))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7954; long BgL_arg1804z00_7955;
BgL_arg1803z00_7954 = 
(BgL_objectz00_bglt)(BgL_excz00_5401); 
BgL_arg1804z00_7955 = 
BGL_CLASS_DEPTH(BgL_classz00_7953); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7956;
BgL_idxz00_7956 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7954); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7957;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7958;
BgL_arg1812z00_7958 = 
(BgL_idxz00_7956+BgL_arg1804z00_7955); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7959;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7960;
BgL_aux3145z00_7960 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7960))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7959 = BgL_aux3145z00_7960; }  else 
{ 
 obj_t BgL_auxz00_15884;
BgL_auxz00_15884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7960); 
FAILURE(BgL_auxz00_15884,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5418z00_15888;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15889;
BgL_tmpz00_15889 = 
VECTOR_LENGTH(BgL_vectorz00_7959); 
BgL_test5418z00_15888 = 
BOUND_CHECK(BgL_arg1812z00_7958, BgL_tmpz00_15889); } 
if(BgL_test5418z00_15888)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7957 = 
VECTOR_REF(BgL_vectorz00_7959,BgL_arg1812z00_7958); }  else 
{ 
 obj_t BgL_auxz00_15893;
BgL_auxz00_15893 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7959, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7959)), 
(int)(BgL_arg1812z00_7958)); 
FAILURE(BgL_auxz00_15893,BFALSE,BFALSE);} } } } 
BgL_test5414z00_15873 = 
(BgL_arg1811z00_7957==BgL_classz00_7953); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7961;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7962;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7963; long BgL_arg1555z00_7964;
BgL_arg1554z00_7963 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7965;
BgL_arg1556z00_7965 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7954); 
BgL_arg1555z00_7964 = 
(BgL_arg1556z00_7965-OBJECT_TYPE); } 
BgL_oclassz00_7962 = 
VECTOR_REF(BgL_arg1554z00_7963,BgL_arg1555z00_7964); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7966;
BgL__ortest_1220z00_7966 = 
(BgL_classz00_7953==BgL_oclassz00_7962); 
if(BgL__ortest_1220z00_7966)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7961 = BgL__ortest_1220z00_7966; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7967;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7968;
BgL_arg1810z00_7968 = 
(BgL_oclassz00_7962); 
BgL_odepthz00_7967 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7968); } 
if(
(BgL_arg1804z00_7955<BgL_odepthz00_7967))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7969;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7970;
BgL_arg1809z00_7970 = 
(BgL_oclassz00_7962); 
BgL_arg1808z00_7969 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7970, BgL_arg1804z00_7955); } 
BgL_res2618z00_7961 = 
(BgL_arg1808z00_7969==BgL_classz00_7953); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7961 = ((bool_t)0); } } } } 
BgL_test5414z00_15873 = BgL_res2618z00_7961; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5414z00_15873 = ((bool_t)0)
; } } 
if(BgL_test5414z00_15873)
{ /* Llib/object.scm 1519 */
BgL_excz00_7971 = 
((BgL_z62iozd2writezd2errorz62_bglt)BgL_excz00_5401); }  else 
{ 
 obj_t BgL_auxz00_15915;
BgL_auxz00_15915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(66047L), BGl_string3980z00zz__objectz00, BGl_string3727z00zz__objectz00, BgL_excz00_5401); 
FAILURE(BgL_auxz00_15915,BFALSE,BFALSE);} } 
{ 

{ /* Llib/object.scm 1521 */
 bool_t BgL_test5421z00_15919;
{ /* Llib/object.scm 1521 */
 obj_t BgL_arg2211z00_7974; obj_t BgL_arg2212z00_7975;
BgL_arg2211z00_7974 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_excz00_7971)))->BgL_objz00); 
{ /* Llib/object.scm 1521 */
 obj_t BgL_tmpz00_15922;
BgL_tmpz00_15922 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2212z00_7975 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_15922); } 
BgL_test5421z00_15919 = 
(BgL_arg2211z00_7974==BgL_arg2212z00_7975); } 
if(BgL_test5421z00_15919)
{ /* Llib/object.scm 1521 */
return BFALSE;}  else 
{ /* Llib/object.scm 1521 */
{ /* Llib/object.scm 1519 */
 obj_t BgL_nextzd2method1413zd2_7973;
BgL_nextzd2method1413zd2_7973 = 
BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
((BgL_objectz00_bglt)BgL_excz00_7971), BGl_exceptionzd2notifyzd2envz00zz__objectz00, BGl_z62iozd2writezd2errorz62zz__objectz00); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_nextzd2method1413zd2_7973, 1))
{ /* Llib/object.scm 1519 */
return 
BGL_PROCEDURE_CALL1(BgL_nextzd2method1413zd2_7973, 
((obj_t)BgL_excz00_7971));}  else 
{ /* Llib/object.scm 1519 */
FAILURE(BGl_string3976z00zz__objectz00,BGl_list3977z00zz__objectz00,BgL_nextzd2method1413zd2_7973);} } } } } } } 

}



/* &exception-notify-&er1412 */
obj_t BGl_z62exceptionzd2notifyzd2z62er1412z00zz__objectz00(obj_t BgL_envz00_5402, obj_t BgL_excz00_5403)
{
{ /* Llib/object.scm 1513 */
{ /* Llib/object.scm 1514 */
 BgL_z62errorz62_bglt BgL_excz00_7994;
{ /* Llib/object.scm 1514 */
 bool_t BgL_test5423z00_15936;
{ /* Llib/object.scm 1514 */
 obj_t BgL_classz00_7976;
BgL_classz00_7976 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_excz00_5403))
{ /* Llib/object.scm 1334 */
 BgL_objectz00_bglt BgL_arg1803z00_7977; long BgL_arg1804z00_7978;
BgL_arg1803z00_7977 = 
(BgL_objectz00_bglt)(BgL_excz00_5403); 
BgL_arg1804z00_7978 = 
BGL_CLASS_DEPTH(BgL_classz00_7976); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/object.scm 1375 */
 long BgL_idxz00_7979;
BgL_idxz00_7979 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1803z00_7977); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_arg1811z00_7980;
{ /* Llib/object.scm 1376 */
 long BgL_arg1812z00_7981;
BgL_arg1812z00_7981 = 
(BgL_idxz00_7979+BgL_arg1804z00_7978); 
{ /* Llib/object.scm 1376 */
 obj_t BgL_vectorz00_7982;
{ /* Llib/object.scm 1376 */
 obj_t BgL_aux3145z00_7983;
BgL_aux3145z00_7983 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux3145z00_7983))
{ /* Llib/object.scm 1376 */
BgL_vectorz00_7982 = BgL_aux3145z00_7983; }  else 
{ 
 obj_t BgL_auxz00_15947;
BgL_auxz00_15947 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60120L), BGl_string3591z00zz__objectz00, BGl_string3444z00zz__objectz00, BgL_aux3145z00_7983); 
FAILURE(BgL_auxz00_15947,BFALSE,BFALSE);} } 
{ /* Llib/object.scm 1376 */
 bool_t BgL_test5427z00_15951;
{ /* Llib/object.scm 1376 */
 long BgL_tmpz00_15952;
BgL_tmpz00_15952 = 
VECTOR_LENGTH(BgL_vectorz00_7982); 
BgL_test5427z00_15951 = 
BOUND_CHECK(BgL_arg1812z00_7981, BgL_tmpz00_15952); } 
if(BgL_test5427z00_15951)
{ /* Llib/object.scm 1376 */
BgL_arg1811z00_7980 = 
VECTOR_REF(BgL_vectorz00_7982,BgL_arg1812z00_7981); }  else 
{ 
 obj_t BgL_auxz00_15956;
BgL_auxz00_15956 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(60108L), BGl_string3592z00zz__objectz00, BgL_vectorz00_7982, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_7982)), 
(int)(BgL_arg1812z00_7981)); 
FAILURE(BgL_auxz00_15956,BFALSE,BFALSE);} } } } 
BgL_test5423z00_15936 = 
(BgL_arg1811z00_7980==BgL_classz00_7976); } }  else 
{ /* Llib/object.scm 1354 */
 bool_t BgL_res2618z00_7984;
{ /* Llib/object.scm 1362 */
 obj_t BgL_oclassz00_7985;
{ /* Llib/object.scm 893 */
 obj_t BgL_arg1554z00_7986; long BgL_arg1555z00_7987;
BgL_arg1554z00_7986 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/object.scm 894 */
 long BgL_arg1556z00_7988;
BgL_arg1556z00_7988 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1803z00_7977); 
BgL_arg1555z00_7987 = 
(BgL_arg1556z00_7988-OBJECT_TYPE); } 
BgL_oclassz00_7985 = 
VECTOR_REF(BgL_arg1554z00_7986,BgL_arg1555z00_7987); } 
{ /* Llib/object.scm 1363 */
 bool_t BgL__ortest_1220z00_7989;
BgL__ortest_1220z00_7989 = 
(BgL_classz00_7976==BgL_oclassz00_7985); 
if(BgL__ortest_1220z00_7989)
{ /* Llib/object.scm 1363 */
BgL_res2618z00_7984 = BgL__ortest_1220z00_7989; }  else 
{ /* Llib/object.scm 1364 */
 long BgL_odepthz00_7990;
{ /* Llib/object.scm 1364 */
 obj_t BgL_arg1810z00_7991;
BgL_arg1810z00_7991 = 
(BgL_oclassz00_7985); 
BgL_odepthz00_7990 = 
BGL_CLASS_DEPTH(BgL_arg1810z00_7991); } 
if(
(BgL_arg1804z00_7978<BgL_odepthz00_7990))
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1808z00_7992;
{ /* Llib/object.scm 1366 */
 obj_t BgL_arg1809z00_7993;
BgL_arg1809z00_7993 = 
(BgL_oclassz00_7985); 
BgL_arg1808z00_7992 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1809z00_7993, BgL_arg1804z00_7978); } 
BgL_res2618z00_7984 = 
(BgL_arg1808z00_7992==BgL_classz00_7976); }  else 
{ /* Llib/object.scm 1365 */
BgL_res2618z00_7984 = ((bool_t)0); } } } } 
BgL_test5423z00_15936 = BgL_res2618z00_7984; } }  else 
{ /* Llib/object.scm 1333 */
BgL_test5423z00_15936 = ((bool_t)0)
; } } 
if(BgL_test5423z00_15936)
{ /* Llib/object.scm 1514 */
BgL_excz00_7994 = 
((BgL_z62errorz62_bglt)BgL_excz00_5403); }  else 
{ 
 obj_t BgL_auxz00_15978;
BgL_auxz00_15978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3441z00zz__objectz00, 
BINT(65804L), BGl_string3981z00zz__objectz00, BGl_string3689z00zz__objectz00, BgL_excz00_5403); 
FAILURE(BgL_auxz00_15978,BFALSE,BFALSE);} } 
return 
BGl_errorzd2notifyzd2zz__errorz00(
((obj_t)BgL_excz00_7994));} } 

}

#ifdef __cplusplus
}
#endif
