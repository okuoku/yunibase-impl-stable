/*===========================================================================*/
/*   (Llib/trace.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/trace.scm -indent -o objs/obj_s/Llib/trace.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___TRACE_TYPE_DEFINITIONS
#define BGL___TRACE_TYPE_DEFINITIONS
#endif // BGL___TRACE_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62tracezd2colorzb0zz__tracez00(obj_t, obj_t, obj_t);
static obj_t BGl_list1809z00zz__tracez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_tracezd2marginzd2setz12z12zz__tracez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52withzd2tracez80zz__tracez00(obj_t, obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__tracez00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31338ze3ze5zz__tracez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_tracezd2stringzd2zz__tracez00(obj_t);
static obj_t BGl_symbol1771z00zz__tracez00 = BUNSPEC;
static obj_t BGl_tracezd2alistzd2getz00zz__tracez00(obj_t, obj_t);
static obj_t BGl_symbol1773z00zz__tracez00 = BUNSPEC;
static obj_t BGl_symbol1775z00zz__tracez00 = BUNSPEC;
static obj_t BGl_symbol1778z00zz__tracez00 = BUNSPEC;
extern obj_t bgl_display_obj(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__tracez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tracezd2boldzd2zz__tracez00(obj_t);
static obj_t BGl_symbol1780z00zz__tracez00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31203ze3ze5zz__tracez00(obj_t);
static obj_t BGl_symbol1786z00zz__tracez00 = BUNSPEC;
static obj_t BGl_z62tracezd2activezf3z43zz__tracez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tracezd2portzd2zz__tracez00(void);
static obj_t BGl_toplevelzd2initzd2zz__tracez00(void);
extern int bgl_debug(void);
static obj_t BGl_tracezd2alistzd2zz__tracez00(void);
static obj_t BGl_z62tracezd2boldzb0zz__tracez00(obj_t, obj_t);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__tracez00(void);
static obj_t BGl_genericzd2initzd2zz__tracez00(void);
static obj_t BGl_z62tracezd2portzb0zz__tracez00(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__tracez00(void);
extern obj_t BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__tracez00(void);
static obj_t BGl_objectzd2initzd2zz__tracez00(void);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
extern obj_t BGl_bigloozd2tracezd2zz__paramz00(void);
BGL_EXPORTED_DECL obj_t BGl_tracezd2colorzd2zz__tracez00(int, obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31321ze3ze5zz__tracez00(obj_t);
extern obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
static obj_t BGl_z62z52withzd2traceze2zz__tracez00(obj_t, obj_t, obj_t, obj_t);
extern bool_t BGl_bigloozd2tracezd2colorz00zz__paramz00(void);
BGL_EXPORTED_DECL obj_t BGl_tracezd2portzd2setz12z12zz__tracez00(obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62tracezd2portzd2setz12z70zz__tracez00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31250ze3ze5zz__tracez00(obj_t);
extern obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__tracez00(void);
BGL_EXPORTED_DECL obj_t BGl_tracezd2itemzd2zz__tracez00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31219ze3ze5zz__tracez00(obj_t);
static obj_t BGl_z62tracezd2marginzb0zz__tracez00(obj_t);
static obj_t BGl_za2tracezd2mutexza2zd2zz__tracez00 = BUNSPEC;
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62tracezd2marginzd2setz12z70zz__tracez00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31237ze3ze5zz__tracez00(obj_t);
static obj_t BGl_z62tracezd2itemzb0zz__tracez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tracezd2marginzd2zz__tracez00(void);
static obj_t BGl_symbol1810z00zz__tracez00 = BUNSPEC;
extern obj_t string_append(obj_t, obj_t);
static obj_t BGl_symbol1812z00zz__tracez00 = BUNSPEC;
static obj_t BGl_ttyzd2tracezd2colorz00zz__tracez00(int, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31336ze3ze5zz__tracez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_tracezd2activezf3z21zz__tracez00(obj_t);
static obj_t BGl_z62tracezd2stringzb0zz__tracez00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string1790z00zz__tracez00, BgL_bgl_string1790za700za7za7_1820za7, "bstring", 7 );
DEFINE_STRING( BGl_string1791z00zz__tracez00, BgL_bgl_string1791za700za7za7_1821za7, "&trace-margin-set!", 18 );
DEFINE_STRING( BGl_string1792z00zz__tracez00, BgL_bgl_string1792za700za7za7_1822za7, "&trace-color", 12 );
DEFINE_STRING( BGl_string1793z00zz__tracez00, BgL_bgl_string1793za700za7za7_1823za7, "bint", 4 );
DEFINE_STRING( BGl_string1794z00zz__tracez00, BgL_bgl_string1794za700za7za7_1824za7, "&<@anonymous:1203>", 18 );
DEFINE_STRING( BGl_string1795z00zz__tracez00, BgL_bgl_string1795za700za7za7_1825za7, "m", 1 );
DEFINE_STRING( BGl_string1796z00zz__tracez00, BgL_bgl_string1796za700za7za7_1826za7, "\033[0m\033[1;", 8 );
DEFINE_STRING( BGl_string1797z00zz__tracez00, BgL_bgl_string1797za700za7za7_1827za7, "for-each", 8 );
DEFINE_STRING( BGl_string1798z00zz__tracez00, BgL_bgl_string1798za700za7za7_1828za7, "list", 4 );
DEFINE_STRING( BGl_string1799z00zz__tracez00, BgL_bgl_string1799za700za7za7_1829za7, "\033[0m", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2colorzd2envz00zz__tracez00, BgL_bgl_za762traceza7d2color1830z00, va_generic_entry, BGl_z62tracezd2colorzb0zz__tracez00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52withzd2tracezd2envz52zz__tracez00, BgL_bgl_za762za752withza7d2tra1831za7, BGl_z62z52withzd2traceze2zz__tracez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2marginzd2envz00zz__tracez00, BgL_bgl_za762traceza7d2margi1832z00, BGl_z62tracezd2marginzb0zz__tracez00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2portzd2setz12zd2envzc0zz__tracez00, BgL_bgl_za762traceza7d2portza71833za7, BGl_z62tracezd2portzd2setz12z70zz__tracez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2boldzd2envz00zz__tracez00, BgL_bgl_za762traceza7d2boldza71834za7, va_generic_entry, BGl_z62tracezd2boldzb0zz__tracez00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2itemzd2envz00zz__tracez00, BgL_bgl_za762traceza7d2itemza71835za7, va_generic_entry, BGl_z62tracezd2itemzb0zz__tracez00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2portzd2envz00zz__tracez00, BgL_bgl_za762traceza7d2portza71836za7, BGl_z62tracezd2portzb0zz__tracez00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1800z00zz__tracez00, BgL_bgl_string1800za700za7za7_1837za7, "trace-bold", 10 );
DEFINE_STRING( BGl_string1801z00zz__tracez00, BgL_bgl_string1801za700za7za7_1838za7, "pair", 4 );
DEFINE_STRING( BGl_string1802z00zz__tracez00, BgL_bgl_string1802za700za7za7_1839za7, "tty-trace-color", 15 );
DEFINE_STRING( BGl_string1803z00zz__tracez00, BgL_bgl_string1803za700za7za7_1840za7, "trace-item", 10 );
DEFINE_STRING( BGl_string1804z00zz__tracez00, BgL_bgl_string1804za700za7za7_1841za7, "- ", 2 );
DEFINE_STRING( BGl_string1805z00zz__tracez00, BgL_bgl_string1805za700za7za7_1842za7, "trace-active?", 13 );
DEFINE_STRING( BGl_string1806z00zz__tracez00, BgL_bgl_string1806za700za7za7_1843za7, "  |", 3 );
DEFINE_STRING( BGl_string1807z00zz__tracez00, BgL_bgl_string1807za700za7za7_1844za7, "%with-trace", 11 );
DEFINE_STRING( BGl_string1808z00zz__tracez00, BgL_bgl_string1808za700za7za7_1845za7, "%with-trace:Wrong number of arguments", 37 );
DEFINE_STRING( BGl_string1811z00zz__tracez00, BgL_bgl_string1811za700za7za7_1846za7, "funcall", 7 );
DEFINE_STRING( BGl_string1813z00zz__tracez00, BgL_bgl_string1813za700za7za7_1847za7, "thunk", 5 );
DEFINE_STRING( BGl_string1814z00zz__tracez00, BgL_bgl_string1814za700za7za7_1848za7, "&%with-trace", 12 );
DEFINE_STRING( BGl_string1815z00zz__tracez00, BgL_bgl_string1815za700za7za7_1849za7, "procedure", 9 );
DEFINE_STRING( BGl_string1816z00zz__tracez00, BgL_bgl_string1816za700za7za7_1850za7, "<@anonymous:1321>", 17 );
DEFINE_STRING( BGl_string1817z00zz__tracez00, BgL_bgl_string1817za700za7za7_1851za7, "+ ", 2 );
DEFINE_STRING( BGl_string1818z00zz__tracez00, BgL_bgl_string1818za700za7za7_1852za7, "--+ ", 4 );
DEFINE_STRING( BGl_string1819z00zz__tracez00, BgL_bgl_string1819za700za7za7_1853za7, "__trace", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2activezf3zd2envzf3zz__tracez00, BgL_bgl_za762traceza7d2activ1854z00, BGl_z62tracezd2activezf3z43zz__tracez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2stringzd2envz00zz__tracez00, BgL_bgl_za762traceza7d2strin1855z00, BGl_z62tracezd2stringzb0zz__tracez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1770z00zz__tracez00, BgL_bgl_string1770za700za7za7_1856za7, "trace", 5 );
DEFINE_STRING( BGl_string1772z00zz__tracez00, BgL_bgl_string1772za700za7za7_1857za7, "port", 4 );
DEFINE_STRING( BGl_string1774z00zz__tracez00, BgL_bgl_string1774za700za7za7_1858za7, "depth", 5 );
DEFINE_STRING( BGl_string1776z00zz__tracez00, BgL_bgl_string1776za700za7za7_1859za7, "margin", 6 );
DEFINE_STRING( BGl_string1777z00zz__tracez00, BgL_bgl_string1777za700za7za7_1860za7, "", 0 );
DEFINE_STRING( BGl_string1779z00zz__tracez00, BgL_bgl_string1779za700za7za7_1861za7, "margin-level", 12 );
DEFINE_STRING( BGl_string1781z00zz__tracez00, BgL_bgl_string1781za700za7za7_1862za7, "trace-alist-get", 15 );
DEFINE_STRING( BGl_string1782z00zz__tracez00, BgL_bgl_string1782za700za7za7_1863za7, "Can't find trace-value", 22 );
DEFINE_STRING( BGl_string1783z00zz__tracez00, BgL_bgl_string1783za700za7za7_1864za7, "/tmp/bigloo/runtime/Llib/trace.scm", 34 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tracezd2marginzd2setz12zd2envzc0zz__tracez00, BgL_bgl_za762traceza7d2margi1865z00, BGl_z62tracezd2marginzd2setz12z70zz__tracez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1784z00zz__tracez00, BgL_bgl_string1784za700za7za7_1866za7, "trace-port", 10 );
DEFINE_STRING( BGl_string1785z00zz__tracez00, BgL_bgl_string1785za700za7za7_1867za7, "output-port", 11 );
DEFINE_STRING( BGl_string1787z00zz__tracez00, BgL_bgl_string1787za700za7za7_1868za7, "trace-alist-set!", 16 );
DEFINE_STRING( BGl_string1788z00zz__tracez00, BgL_bgl_string1788za700za7za7_1869za7, "&trace-port-set!", 16 );
DEFINE_STRING( BGl_string1789z00zz__tracez00, BgL_bgl_string1789za700za7za7_1870za7, "trace-margin", 12 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_list1809z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1771z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1773z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1775z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1778z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1780z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1786z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_za2tracezd2mutexza2zd2zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1810z00zz__tracez00) );
ADD_ROOT( (void *)(&BGl_symbol1812z00zz__tracez00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__tracez00(long BgL_checksumz00_2152, char * BgL_fromz00_2153)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__tracez00))
{ 
BGl_requirezd2initializa7ationz75zz__tracez00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__tracez00(); 
BGl_cnstzd2initzd2zz__tracez00(); 
BGl_importedzd2moduleszd2initz00zz__tracez00(); 
return 
BGl_toplevelzd2initzd2zz__tracez00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
BGl_symbol1771z00zz__tracez00 = 
bstring_to_symbol(BGl_string1772z00zz__tracez00); 
BGl_symbol1773z00zz__tracez00 = 
bstring_to_symbol(BGl_string1774z00zz__tracez00); 
BGl_symbol1775z00zz__tracez00 = 
bstring_to_symbol(BGl_string1776z00zz__tracez00); 
BGl_symbol1778z00zz__tracez00 = 
bstring_to_symbol(BGl_string1779z00zz__tracez00); 
BGl_symbol1780z00zz__tracez00 = 
bstring_to_symbol(BGl_string1781z00zz__tracez00); 
BGl_symbol1786z00zz__tracez00 = 
bstring_to_symbol(BGl_string1787z00zz__tracez00); 
BGl_symbol1810z00zz__tracez00 = 
bstring_to_symbol(BGl_string1811z00zz__tracez00); 
BGl_symbol1812z00zz__tracez00 = 
bstring_to_symbol(BGl_string1813z00zz__tracez00); 
return ( 
BGl_list1809z00zz__tracez00 = 
MAKE_YOUNG_PAIR(BGl_symbol1810z00zz__tracez00, 
MAKE_YOUNG_PAIR(BGl_symbol1812z00zz__tracez00, 
MAKE_YOUNG_PAIR(BGl_symbol1812z00zz__tracez00, BNIL))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
return ( 
BGl_za2tracezd2mutexza2zd2zz__tracez00 = 
bgl_make_mutex(BGl_string1770z00zz__tracez00), BUNSPEC) ;} 

}



/* trace-alist */
obj_t BGl_tracezd2alistzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 82 */
{ /* Llib/trace.scm 83 */
 obj_t BgL_alz00_1114;
BgL_alz00_1114 = 
BGL_DEBUG_ALIST_GET(); 
if(
NULLP(BgL_alz00_1114))
{ /* Llib/trace.scm 86 */
 obj_t BgL_newzd2alzd2_1116;
{ /* Llib/trace.scm 86 */
 obj_t BgL_arg1182z00_1117; obj_t BgL_arg1183z00_1118; obj_t BgL_arg1187z00_1119; obj_t BgL_arg1188z00_1120;
{ /* Llib/trace.scm 86 */
 obj_t BgL_arg1194z00_1125;
{ /* Llib/trace.scm 86 */
 obj_t BgL_tmpz00_2177;
BgL_tmpz00_2177 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1194z00_1125 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2177); } 
BgL_arg1182z00_1117 = 
MAKE_YOUNG_PAIR(BGl_symbol1771z00zz__tracez00, BgL_arg1194z00_1125); } 
BgL_arg1183z00_1118 = 
MAKE_YOUNG_PAIR(BGl_symbol1773z00zz__tracez00, 
BINT(0L)); 
BgL_arg1187z00_1119 = 
MAKE_YOUNG_PAIR(BGl_symbol1775z00zz__tracez00, BGl_string1777z00zz__tracez00); 
BgL_arg1188z00_1120 = 
MAKE_YOUNG_PAIR(BGl_symbol1778z00zz__tracez00, 
BINT(0L)); 
{ /* Llib/trace.scm 86 */
 obj_t BgL_list1189z00_1121;
{ /* Llib/trace.scm 86 */
 obj_t BgL_arg1190z00_1122;
{ /* Llib/trace.scm 86 */
 obj_t BgL_arg1191z00_1123;
{ /* Llib/trace.scm 86 */
 obj_t BgL_arg1193z00_1124;
BgL_arg1193z00_1124 = 
MAKE_YOUNG_PAIR(BgL_arg1188z00_1120, BNIL); 
BgL_arg1191z00_1123 = 
MAKE_YOUNG_PAIR(BgL_arg1187z00_1119, BgL_arg1193z00_1124); } 
BgL_arg1190z00_1122 = 
MAKE_YOUNG_PAIR(BgL_arg1183z00_1118, BgL_arg1191z00_1123); } 
BgL_list1189z00_1121 = 
MAKE_YOUNG_PAIR(BgL_arg1182z00_1117, BgL_arg1190z00_1122); } 
BgL_newzd2alzd2_1116 = BgL_list1189z00_1121; } } 
BGL_DEBUG_ALIST_SET(BgL_newzd2alzd2_1116); BUNSPEC; 
return BgL_newzd2alzd2_1116;}  else 
{ /* Llib/trace.scm 84 */
return BgL_alz00_1114;} } } 

}



/* trace-alist-get */
obj_t BGl_tracezd2alistzd2getz00zz__tracez00(obj_t BgL_alistz00_3, obj_t BgL_keyz00_4)
{
{ /* Llib/trace.scm 96 */
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1669;
BgL_cz00_1669 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_4, BgL_alistz00_3); 
if(
PAIRP(BgL_cz00_1669))
{ /* Llib/trace.scm 98 */
return 
CDR(BgL_cz00_1669);}  else 
{ /* Llib/trace.scm 98 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_4);} } } 

}



/* trace-port */
BGL_EXPORTED_DEF obj_t BGl_tracezd2portzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 114 */
{ /* Llib/trace.scm 115 */
 obj_t BgL_arg1197z00_1675;
BgL_arg1197z00_1675 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 115 */
 obj_t BgL_keyz00_1676;
BgL_keyz00_1676 = BGl_symbol1771z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1677;
BgL_cz00_1677 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1676, BgL_arg1197z00_1675); 
if(
PAIRP(BgL_cz00_1677))
{ /* Llib/trace.scm 99 */
 obj_t BgL_aux1723z00_2057;
BgL_aux1723z00_2057 = 
CDR(BgL_cz00_1677); 
if(
OUTPUT_PORTP(BgL_aux1723z00_2057))
{ /* Llib/trace.scm 99 */
return BgL_aux1723z00_2057;}  else 
{ 
 obj_t BgL_auxz00_2203;
BgL_auxz00_2203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(3521L), BGl_string1784z00zz__tracez00, BGl_string1785z00zz__tracez00, BgL_aux1723z00_2057); 
FAILURE(BgL_auxz00_2203,BFALSE,BFALSE);} }  else 
{ /* Llib/trace.scm 100 */
 obj_t BgL_aux1725z00_2059;
BgL_aux1725z00_2059 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1676); 
if(
OUTPUT_PORTP(BgL_aux1725z00_2059))
{ /* Llib/trace.scm 100 */
return BgL_aux1725z00_2059;}  else 
{ 
 obj_t BgL_auxz00_2210;
BgL_auxz00_2210 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(3532L), BGl_string1784z00zz__tracez00, BGl_string1785z00zz__tracez00, BgL_aux1725z00_2059); 
FAILURE(BgL_auxz00_2210,BFALSE,BFALSE);} } } } } } 

}



/* &trace-port */
obj_t BGl_z62tracezd2portzb0zz__tracez00(obj_t BgL_envz00_2008)
{
{ /* Llib/trace.scm 114 */
return 
BGl_tracezd2portzd2zz__tracez00();} 

}



/* trace-port-set! */
BGL_EXPORTED_DEF obj_t BGl_tracezd2portzd2setz12z12zz__tracez00(obj_t BgL_pz00_8)
{
{ /* Llib/trace.scm 120 */
{ /* Llib/trace.scm 121 */
 obj_t BgL_arg1198z00_1680;
BgL_arg1198z00_1680 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 121 */
 obj_t BgL_keyz00_1681;
BgL_keyz00_1681 = BGl_symbol1771z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_1682;
BgL_cz00_1682 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1681, BgL_arg1198z00_1680); 
if(
PAIRP(BgL_cz00_1682))
{ /* Llib/trace.scm 107 */
return 
SET_CDR(BgL_cz00_1682, BgL_pz00_8);}  else 
{ /* Llib/trace.scm 107 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1681);} } } } } 

}



/* &trace-port-set! */
obj_t BGl_z62tracezd2portzd2setz12z70zz__tracez00(obj_t BgL_envz00_2009, obj_t BgL_pz00_2010)
{
{ /* Llib/trace.scm 120 */
{ /* Llib/trace.scm 121 */
 obj_t BgL_auxz00_2221;
if(
OUTPUT_PORTP(BgL_pz00_2010))
{ /* Llib/trace.scm 121 */
BgL_auxz00_2221 = BgL_pz00_2010
; }  else 
{ 
 obj_t BgL_auxz00_2224;
BgL_auxz00_2224 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(4543L), BGl_string1788z00zz__tracez00, BGl_string1785z00zz__tracez00, BgL_pz00_2010); 
FAILURE(BgL_auxz00_2224,BFALSE,BFALSE);} 
return 
BGl_tracezd2portzd2setz12z12zz__tracez00(BgL_auxz00_2221);} } 

}



/* trace-margin */
BGL_EXPORTED_DEF obj_t BGl_tracezd2marginzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 126 */
{ /* Llib/trace.scm 127 */
 obj_t BgL_arg1199z00_1685;
BgL_arg1199z00_1685 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 127 */
 obj_t BgL_keyz00_1686;
BgL_keyz00_1686 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1687;
BgL_cz00_1687 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1686, BgL_arg1199z00_1685); 
if(
PAIRP(BgL_cz00_1687))
{ /* Llib/trace.scm 99 */
 obj_t BgL_aux1729z00_2063;
BgL_aux1729z00_2063 = 
CDR(BgL_cz00_1687); 
if(
STRINGP(BgL_aux1729z00_2063))
{ /* Llib/trace.scm 99 */
return BgL_aux1729z00_2063;}  else 
{ 
 obj_t BgL_auxz00_2236;
BgL_auxz00_2236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(3521L), BGl_string1789z00zz__tracez00, BGl_string1790z00zz__tracez00, BgL_aux1729z00_2063); 
FAILURE(BgL_auxz00_2236,BFALSE,BFALSE);} }  else 
{ /* Llib/trace.scm 100 */
 obj_t BgL_aux1731z00_2065;
BgL_aux1731z00_2065 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1686); 
if(
STRINGP(BgL_aux1731z00_2065))
{ /* Llib/trace.scm 100 */
return BgL_aux1731z00_2065;}  else 
{ 
 obj_t BgL_auxz00_2243;
BgL_auxz00_2243 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(3532L), BGl_string1789z00zz__tracez00, BGl_string1790z00zz__tracez00, BgL_aux1731z00_2065); 
FAILURE(BgL_auxz00_2243,BFALSE,BFALSE);} } } } } } 

}



/* &trace-margin */
obj_t BGl_z62tracezd2marginzb0zz__tracez00(obj_t BgL_envz00_2011)
{
{ /* Llib/trace.scm 126 */
return 
BGl_tracezd2marginzd2zz__tracez00();} 

}



/* trace-margin-set! */
BGL_EXPORTED_DEF obj_t BGl_tracezd2marginzd2setz12z12zz__tracez00(obj_t BgL_mz00_9)
{
{ /* Llib/trace.scm 132 */
{ /* Llib/trace.scm 133 */
 obj_t BgL_arg1200z00_1690;
BgL_arg1200z00_1690 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 133 */
 obj_t BgL_keyz00_1691;
BgL_keyz00_1691 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_1692;
BgL_cz00_1692 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1691, BgL_arg1200z00_1690); 
if(
PAIRP(BgL_cz00_1692))
{ /* Llib/trace.scm 107 */
return 
SET_CDR(BgL_cz00_1692, BgL_mz00_9);}  else 
{ /* Llib/trace.scm 107 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1691);} } } } } 

}



/* &trace-margin-set! */
obj_t BGl_z62tracezd2marginzd2setz12z70zz__tracez00(obj_t BgL_envz00_2012, obj_t BgL_mz00_2013)
{
{ /* Llib/trace.scm 132 */
{ /* Llib/trace.scm 133 */
 obj_t BgL_auxz00_2254;
if(
STRINGP(BgL_mz00_2013))
{ /* Llib/trace.scm 133 */
BgL_auxz00_2254 = BgL_mz00_2013
; }  else 
{ 
 obj_t BgL_auxz00_2257;
BgL_auxz00_2257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(5131L), BGl_string1791z00zz__tracez00, BGl_string1790z00zz__tracez00, BgL_mz00_2013); 
FAILURE(BgL_auxz00_2257,BFALSE,BFALSE);} 
return 
BGl_tracezd2marginzd2setz12z12zz__tracez00(BgL_auxz00_2254);} } 

}



/* trace-color */
BGL_EXPORTED_DEF obj_t BGl_tracezd2colorzd2zz__tracez00(int BgL_colz00_10, obj_t BgL_oz00_11)
{
{ /* Llib/trace.scm 138 */
{ /* Llib/trace.scm 140 */
 obj_t BgL_arg1201z00_1134;
if(
BGl_bigloozd2tracezd2colorz00zz__paramz00())
{ /* Llib/trace.scm 141 */
 obj_t BgL_zc3z04anonymousza31203ze3z87_2014;
BgL_zc3z04anonymousza31203ze3z87_2014 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31203ze3ze5zz__tracez00, 
(int)(0L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31203ze3z87_2014, 
(int)(0L), 
BINT(BgL_colz00_10)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31203ze3z87_2014, 
(int)(1L), BgL_oz00_11); 
BgL_arg1201z00_1134 = BgL_zc3z04anonymousza31203ze3z87_2014; }  else 
{ /* Llib/trace.scm 145 */
 obj_t BgL_zc3z04anonymousza31219ze3z87_2015;
BgL_zc3z04anonymousza31219ze3z87_2015 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31219ze3ze5zz__tracez00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31219ze3z87_2015, 
(int)(0L), BgL_oz00_11); 
BgL_arg1201z00_1134 = BgL_zc3z04anonymousza31219ze3z87_2015; } 
return 
BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(BgL_arg1201z00_1134);} } 

}



/* &trace-color */
obj_t BGl_z62tracezd2colorzb0zz__tracez00(obj_t BgL_envz00_2016, obj_t BgL_colz00_2017, obj_t BgL_oz00_2018)
{
{ /* Llib/trace.scm 138 */
{ /* Llib/trace.scm 140 */
 int BgL_auxz00_2278;
{ /* Llib/trace.scm 140 */
 obj_t BgL_tmpz00_2279;
if(
INTEGERP(BgL_colz00_2017))
{ /* Llib/trace.scm 140 */
BgL_tmpz00_2279 = BgL_colz00_2017
; }  else 
{ 
 obj_t BgL_auxz00_2282;
BgL_auxz00_2282 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(5447L), BGl_string1792z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_colz00_2017); 
FAILURE(BgL_auxz00_2282,BFALSE,BFALSE);} 
BgL_auxz00_2278 = 
CINT(BgL_tmpz00_2279); } 
return 
BGl_tracezd2colorzd2zz__tracez00(BgL_auxz00_2278, BgL_oz00_2018);} } 

}



/* &<@anonymous:1203> */
obj_t BGl_z62zc3z04anonymousza31203ze3ze5zz__tracez00(obj_t BgL_envz00_2019)
{
{ /* Llib/trace.scm 141 */
{ /* Llib/trace.scm 142 */
 int BgL_colz00_2020; obj_t BgL_oz00_2021;
{ /* Llib/trace.scm 142 */
 obj_t BgL_tmpz00_2288;
{ /* Llib/trace.scm 142 */
 obj_t BgL_aux1736z00_2070;
BgL_aux1736z00_2070 = 
PROCEDURE_REF(BgL_envz00_2019, 
(int)(0L)); 
if(
INTEGERP(BgL_aux1736z00_2070))
{ /* Llib/trace.scm 142 */
BgL_tmpz00_2288 = BgL_aux1736z00_2070
; }  else 
{ 
 obj_t BgL_auxz00_2293;
BgL_auxz00_2293 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(5492L), BGl_string1794z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_aux1736z00_2070); 
FAILURE(BgL_auxz00_2293,BFALSE,BFALSE);} } 
BgL_colz00_2020 = 
CINT(BgL_tmpz00_2288); } 
BgL_oz00_2021 = 
PROCEDURE_REF(BgL_envz00_2019, 
(int)(1L)); 
{ /* Llib/trace.scm 142 */
 long BgL_arg1206z00_2114;
BgL_arg1206z00_2114 = 
(31L+
(long)(BgL_colz00_2020)); 
{ /* Llib/trace.scm 142 */
 obj_t BgL_list1207z00_2115;
{ /* Llib/trace.scm 142 */
 obj_t BgL_arg1208z00_2116;
{ /* Llib/trace.scm 142 */
 obj_t BgL_arg1209z00_2117;
BgL_arg1209z00_2117 = 
MAKE_YOUNG_PAIR(BGl_string1795z00zz__tracez00, BNIL); 
BgL_arg1208z00_2116 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1206z00_2114), BgL_arg1209z00_2117); } 
BgL_list1207z00_2115 = 
MAKE_YOUNG_PAIR(BGl_string1796z00zz__tracez00, BgL_arg1208z00_2116); } 
BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1207z00_2115); } } 
{ 
 obj_t BgL_l1077z00_2119;
BgL_l1077z00_2119 = BgL_oz00_2021; 
BgL_zc3z04anonymousza31210ze3z87_2118:
if(
PAIRP(BgL_l1077z00_2119))
{ /* Llib/trace.scm 143 */
{ /* Llib/trace.scm 143 */
 obj_t BgL_arg1212z00_2120;
BgL_arg1212z00_2120 = 
CAR(BgL_l1077z00_2119); 
{ /* Llib/trace.scm 143 */
 obj_t BgL_portz00_2121;
{ /* Llib/trace.scm 143 */
 obj_t BgL_tmpz00_2310;
BgL_tmpz00_2310 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2121 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2310); } 
{ /* Llib/trace.scm 143 */

BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1212z00_2120, BgL_portz00_2121); } } } 
{ 
 obj_t BgL_l1077z00_2314;
BgL_l1077z00_2314 = 
CDR(BgL_l1077z00_2119); 
BgL_l1077z00_2119 = BgL_l1077z00_2314; 
goto BgL_zc3z04anonymousza31210ze3z87_2118;} }  else 
{ /* Llib/trace.scm 143 */
if(
NULLP(BgL_l1077z00_2119))
{ /* Llib/trace.scm 143 */BTRUE; }  else 
{ /* Llib/trace.scm 143 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1797z00zz__tracez00, BGl_string1798z00zz__tracez00, BgL_l1077z00_2119, BGl_string1783z00zz__tracez00, 
BINT(5537L)); } } } 
{ /* Llib/trace.scm 144 */
 obj_t BgL_arg1218z00_2122;
{ /* Llib/trace.scm 144 */
 obj_t BgL_tmpz00_2320;
BgL_tmpz00_2320 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1218z00_2122 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2320); } 
return 
bgl_display_string(BGl_string1799z00zz__tracez00, BgL_arg1218z00_2122);} } } 

}



/* &<@anonymous:1219> */
obj_t BGl_z62zc3z04anonymousza31219ze3ze5zz__tracez00(obj_t BgL_envz00_2022)
{
{ /* Llib/trace.scm 145 */
{ /* Llib/trace.scm 146 */
 obj_t BgL_oz00_2023;
BgL_oz00_2023 = 
PROCEDURE_REF(BgL_envz00_2022, 
(int)(0L)); 
{ 
 obj_t BgL_l1079z00_2124;
BgL_l1079z00_2124 = BgL_oz00_2023; 
BgL_zc3z04anonymousza31220ze3z87_2123:
if(
PAIRP(BgL_l1079z00_2124))
{ /* Llib/trace.scm 146 */
{ /* Llib/trace.scm 146 */
 obj_t BgL_arg1223z00_2125;
BgL_arg1223z00_2125 = 
CAR(BgL_l1079z00_2124); 
{ /* Llib/trace.scm 146 */
 obj_t BgL_portz00_2126;
{ /* Llib/trace.scm 146 */
 obj_t BgL_tmpz00_2329;
BgL_tmpz00_2329 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2126 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2329); } 
{ /* Llib/trace.scm 146 */

BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1223z00_2125, BgL_portz00_2126); } } } 
{ 
 obj_t BgL_l1079z00_2333;
BgL_l1079z00_2333 = 
CDR(BgL_l1079z00_2124); 
BgL_l1079z00_2124 = BgL_l1079z00_2333; 
goto BgL_zc3z04anonymousza31220ze3z87_2123;} }  else 
{ /* Llib/trace.scm 146 */
if(
NULLP(BgL_l1079z00_2124))
{ /* Llib/trace.scm 146 */
return BTRUE;}  else 
{ /* Llib/trace.scm 146 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1797z00zz__tracez00, BGl_string1798z00zz__tracez00, BgL_l1079z00_2124, BGl_string1783z00zz__tracez00, 
BINT(5609L));} } } } } 

}



/* trace-bold */
BGL_EXPORTED_DEF obj_t BGl_tracezd2boldzd2zz__tracez00(obj_t BgL_oz00_12)
{
{ /* Llib/trace.scm 151 */
{ /* Llib/trace.scm 152 */
 obj_t BgL_runner1229z00_1165;
{ /* Llib/trace.scm 152 */
 obj_t BgL_list1227z00_1163;
BgL_list1227z00_1163 = 
MAKE_YOUNG_PAIR(BgL_oz00_12, BNIL); 
BgL_runner1229z00_1165 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
BINT(-30L), BgL_list1227z00_1163); } 
{ /* Llib/trace.scm 152 */
 int BgL_aux1228z00_1164;
{ /* Llib/trace.scm 152 */
 obj_t BgL_pairz00_1705;
{ /* Llib/trace.scm 152 */
 obj_t BgL_aux1737z00_2071;
BgL_aux1737z00_2071 = BgL_runner1229z00_1165; 
if(
PAIRP(BgL_aux1737z00_2071))
{ /* Llib/trace.scm 152 */
BgL_pairz00_1705 = BgL_aux1737z00_2071; }  else 
{ 
 obj_t BgL_auxz00_2344;
BgL_auxz00_2344 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(5892L), BGl_string1800z00zz__tracez00, BGl_string1801z00zz__tracez00, BgL_aux1737z00_2071); 
FAILURE(BgL_auxz00_2344,BFALSE,BFALSE);} } 
{ /* Llib/trace.scm 152 */
 obj_t BgL_tmpz00_2348;
{ /* Llib/trace.scm 152 */
 obj_t BgL_aux1739z00_2073;
BgL_aux1739z00_2073 = 
CAR(BgL_pairz00_1705); 
if(
INTEGERP(BgL_aux1739z00_2073))
{ /* Llib/trace.scm 152 */
BgL_tmpz00_2348 = BgL_aux1739z00_2073
; }  else 
{ 
 obj_t BgL_auxz00_2352;
BgL_auxz00_2352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(5892L), BGl_string1800z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_aux1739z00_2073); 
FAILURE(BgL_auxz00_2352,BFALSE,BFALSE);} } 
BgL_aux1228z00_1164 = 
CINT(BgL_tmpz00_2348); } } 
{ /* Llib/trace.scm 152 */
 obj_t BgL_pairz00_1706;
{ /* Llib/trace.scm 152 */
 obj_t BgL_aux1740z00_2074;
BgL_aux1740z00_2074 = BgL_runner1229z00_1165; 
if(
PAIRP(BgL_aux1740z00_2074))
{ /* Llib/trace.scm 152 */
BgL_pairz00_1706 = BgL_aux1740z00_2074; }  else 
{ 
 obj_t BgL_auxz00_2359;
BgL_auxz00_2359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(5892L), BGl_string1800z00zz__tracez00, BGl_string1801z00zz__tracez00, BgL_aux1740z00_2074); 
FAILURE(BgL_auxz00_2359,BFALSE,BFALSE);} } 
BgL_runner1229z00_1165 = 
CDR(BgL_pairz00_1706); } 
return 
BGl_tracezd2colorzd2zz__tracez00(BgL_aux1228z00_1164, BgL_runner1229z00_1165);} } } 

}



/* &trace-bold */
obj_t BGl_z62tracezd2boldzb0zz__tracez00(obj_t BgL_envz00_2024, obj_t BgL_oz00_2025)
{
{ /* Llib/trace.scm 151 */
return 
BGl_tracezd2boldzd2zz__tracez00(BgL_oz00_2025);} 

}



/* tty-trace-color */
obj_t BGl_ttyzd2tracezd2colorz00zz__tracez00(int BgL_colz00_13, obj_t BgL_oz00_14)
{
{ /* Llib/trace.scm 157 */
{ /* Llib/trace.scm 158 */
 bool_t BgL_test1894z00_2366;
{ /* Llib/trace.scm 158 */
 obj_t BgL_arg1248z00_1184;
{ /* Llib/trace.scm 158 */
 obj_t BgL_res1720z00_1708;
{ /* Llib/trace.scm 115 */
 obj_t BgL_arg1197z00_1707;
BgL_arg1197z00_1707 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 115 */
 obj_t BgL_aux1742z00_2076;
BgL_aux1742z00_2076 = 
BGl_tracezd2alistzd2getz00zz__tracez00(BgL_arg1197z00_1707, BGl_symbol1771z00zz__tracez00); 
if(
OUTPUT_PORTP(BgL_aux1742z00_2076))
{ /* Llib/trace.scm 115 */
BgL_res1720z00_1708 = BgL_aux1742z00_2076; }  else 
{ 
 obj_t BgL_auxz00_2371;
BgL_auxz00_2371 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(4232L), BGl_string1802z00zz__tracez00, BGl_string1785z00zz__tracez00, BgL_aux1742z00_2076); 
FAILURE(BgL_auxz00_2371,BFALSE,BFALSE);} } } 
BgL_arg1248z00_1184 = BgL_res1720z00_1708; } 
BgL_test1894z00_2366 = 
bgl_port_isatty(BgL_arg1248z00_1184); } 
if(BgL_test1894z00_2366)
{ /* Llib/trace.scm 159 */
 obj_t BgL_runner1234z00_1170;
{ /* Llib/trace.scm 159 */
 obj_t BgL_list1232z00_1168;
BgL_list1232z00_1168 = 
MAKE_YOUNG_PAIR(BgL_oz00_14, BNIL); 
BgL_runner1234z00_1170 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
BINT(BgL_colz00_13), BgL_list1232z00_1168); } 
{ /* Llib/trace.scm 159 */
 int BgL_aux1233z00_1169;
{ /* Llib/trace.scm 159 */
 obj_t BgL_pairz00_1710;
{ /* Llib/trace.scm 159 */
 obj_t BgL_aux1744z00_2078;
BgL_aux1744z00_2078 = BgL_runner1234z00_1170; 
if(
PAIRP(BgL_aux1744z00_2078))
{ /* Llib/trace.scm 159 */
BgL_pairz00_1710 = BgL_aux1744z00_2078; }  else 
{ 
 obj_t BgL_auxz00_2381;
BgL_auxz00_2381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(6230L), BGl_string1802z00zz__tracez00, BGl_string1801z00zz__tracez00, BgL_aux1744z00_2078); 
FAILURE(BgL_auxz00_2381,BFALSE,BFALSE);} } 
{ /* Llib/trace.scm 159 */
 obj_t BgL_tmpz00_2385;
{ /* Llib/trace.scm 159 */
 obj_t BgL_aux1746z00_2080;
BgL_aux1746z00_2080 = 
CAR(BgL_pairz00_1710); 
if(
INTEGERP(BgL_aux1746z00_2080))
{ /* Llib/trace.scm 159 */
BgL_tmpz00_2385 = BgL_aux1746z00_2080
; }  else 
{ 
 obj_t BgL_auxz00_2389;
BgL_auxz00_2389 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(6230L), BGl_string1802z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_aux1746z00_2080); 
FAILURE(BgL_auxz00_2389,BFALSE,BFALSE);} } 
BgL_aux1233z00_1169 = 
CINT(BgL_tmpz00_2385); } } 
{ /* Llib/trace.scm 159 */
 obj_t BgL_pairz00_1711;
{ /* Llib/trace.scm 159 */
 obj_t BgL_aux1747z00_2081;
BgL_aux1747z00_2081 = BgL_runner1234z00_1170; 
if(
PAIRP(BgL_aux1747z00_2081))
{ /* Llib/trace.scm 159 */
BgL_pairz00_1711 = BgL_aux1747z00_2081; }  else 
{ 
 obj_t BgL_auxz00_2396;
BgL_auxz00_2396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(6230L), BGl_string1802z00zz__tracez00, BGl_string1801z00zz__tracez00, BgL_aux1747z00_2081); 
FAILURE(BgL_auxz00_2396,BFALSE,BFALSE);} } 
BgL_runner1234z00_1170 = 
CDR(BgL_pairz00_1711); } 
BGL_TAIL return 
BGl_tracezd2colorzd2zz__tracez00(BgL_aux1233z00_1169, BgL_runner1234z00_1170);} }  else 
{ /* Llib/trace.scm 162 */
 obj_t BgL_zc3z04anonymousza31237ze3z87_2026;
BgL_zc3z04anonymousza31237ze3z87_2026 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31237ze3ze5zz__tracez00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31237ze3z87_2026, 
(int)(0L), BgL_oz00_14); 
return 
BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31237ze3z87_2026);} } } 

}



/* &<@anonymous:1237> */
obj_t BGl_z62zc3z04anonymousza31237ze3ze5zz__tracez00(obj_t BgL_envz00_2027)
{
{ /* Llib/trace.scm 161 */
{ /* Llib/trace.scm 162 */
 obj_t BgL_oz00_2028;
BgL_oz00_2028 = 
PROCEDURE_REF(BgL_envz00_2027, 
(int)(0L)); 
{ 
 obj_t BgL_l1081z00_2128;
BgL_l1081z00_2128 = BgL_oz00_2028; 
BgL_zc3z04anonymousza31238ze3z87_2127:
if(
PAIRP(BgL_l1081z00_2128))
{ /* Llib/trace.scm 162 */
{ /* Llib/trace.scm 162 */
 obj_t BgL_arg1242z00_2129;
BgL_arg1242z00_2129 = 
CAR(BgL_l1081z00_2128); 
{ /* Llib/trace.scm 162 */
 obj_t BgL_portz00_2130;
{ /* Llib/trace.scm 162 */
 obj_t BgL_tmpz00_2413;
BgL_tmpz00_2413 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2130 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2413); } 
{ /* Llib/trace.scm 162 */

BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1242z00_2129, BgL_portz00_2130); } } } 
{ 
 obj_t BgL_l1081z00_2417;
BgL_l1081z00_2417 = 
CDR(BgL_l1081z00_2128); 
BgL_l1081z00_2128 = BgL_l1081z00_2417; 
goto BgL_zc3z04anonymousza31238ze3z87_2127;} }  else 
{ /* Llib/trace.scm 162 */
if(
NULLP(BgL_l1081z00_2128))
{ /* Llib/trace.scm 162 */
return BTRUE;}  else 
{ /* Llib/trace.scm 162 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1797z00zz__tracez00, BGl_string1798z00zz__tracez00, BgL_l1081z00_2128, BGl_string1783z00zz__tracez00, 
BINT(6306L));} } } } } 

}



/* trace-string */
BGL_EXPORTED_DEF obj_t BGl_tracezd2stringzd2zz__tracez00(obj_t BgL_oz00_15)
{
{ /* Llib/trace.scm 167 */
{ /* Llib/trace.scm 170 */
 obj_t BgL_zc3z04anonymousza31250ze3z87_2029;
BgL_zc3z04anonymousza31250ze3z87_2029 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31250ze3ze5zz__tracez00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31250ze3z87_2029, 
(int)(0L), BgL_oz00_15); 
return 
BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31250ze3z87_2029);} } 

}



/* &trace-string */
obj_t BGl_z62tracezd2stringzb0zz__tracez00(obj_t BgL_envz00_2030, obj_t BgL_oz00_2031)
{
{ /* Llib/trace.scm 167 */
return 
BGl_tracezd2stringzd2zz__tracez00(BgL_oz00_2031);} 

}



/* &<@anonymous:1250> */
obj_t BGl_z62zc3z04anonymousza31250ze3ze5zz__tracez00(obj_t BgL_envz00_2032)
{
{ /* Llib/trace.scm 169 */
return 
BGl_writez00zz__r4_output_6_10_3z00(
PROCEDURE_REF(BgL_envz00_2032, 
(int)(0L)), BNIL);} 

}



/* trace-item */
BGL_EXPORTED_DEF obj_t BGl_tracezd2itemzd2zz__tracez00(obj_t BgL_argsz00_16)
{
{ /* Llib/trace.scm 175 */
{ /* Llib/trace.scm 176 */
 bool_t BgL_test1901z00_2433;
{ /* Llib/trace.scm 176 */
 int BgL_arg1312z00_1210;
BgL_arg1312z00_1210 = 
bgl_debug(); 
BgL_test1901z00_2433 = 
(
(long)(BgL_arg1312z00_1210)>0L); } 
if(BgL_test1901z00_2433)
{ /* Llib/trace.scm 177 */
 obj_t BgL_alz00_1191;
BgL_alz00_1191 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 178 */
 bool_t BgL_test1902z00_2438;
{ /* Llib/trace.scm 178 */
 obj_t BgL_arg1311z00_1209;
{ /* Llib/trace.scm 178 */
 obj_t BgL_keyz00_1719;
BgL_keyz00_1719 = BGl_symbol1778z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1720;
BgL_cz00_1720 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1719, BgL_alz00_1191); 
if(
PAIRP(BgL_cz00_1720))
{ /* Llib/trace.scm 98 */
BgL_arg1311z00_1209 = 
CDR(BgL_cz00_1720); }  else 
{ /* Llib/trace.scm 98 */
BgL_arg1311z00_1209 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1719); } } } 
BgL_test1902z00_2438 = 
CBOOL(
BGl_tracezd2activezf3z21zz__tracez00(BgL_arg1311z00_1209)); } 
if(BgL_test1902z00_2438)
{ /* Llib/trace.scm 179 */
 obj_t BgL_pz00_1194;
{ /* Llib/trace.scm 179 */
 obj_t BgL_res1721z00_1724;
{ /* Llib/trace.scm 115 */
 obj_t BgL_arg1197z00_1723;
BgL_arg1197z00_1723 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 115 */
 obj_t BgL_aux1749z00_2083;
BgL_aux1749z00_2083 = 
BGl_tracezd2alistzd2getz00zz__tracez00(BgL_arg1197z00_1723, BGl_symbol1771z00zz__tracez00); 
if(
OUTPUT_PORTP(BgL_aux1749z00_2083))
{ /* Llib/trace.scm 115 */
BgL_res1721z00_1724 = BgL_aux1749z00_2083; }  else 
{ 
 obj_t BgL_auxz00_2450;
BgL_auxz00_2450 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(4232L), BGl_string1803z00zz__tracez00, BGl_string1785z00zz__tracez00, BgL_aux1749z00_2083); 
FAILURE(BgL_auxz00_2450,BFALSE,BFALSE);} } } 
BgL_pz00_1194 = BgL_res1721z00_1724; } 
{ /* Llib/trace.scm 180 */
 obj_t BgL_mutex1270z00_1195;
BgL_mutex1270z00_1195 = BGl_za2tracezd2mutexza2zd2zz__tracez00; 
{ /* Llib/trace.scm 180 */
 obj_t BgL_top1906z00_2455;
BgL_top1906z00_2455 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BgL_mutex1270z00_1195); 
BGL_EXITD_PUSH_PROTECT(BgL_top1906z00_2455, BgL_mutex1270z00_1195); BUNSPEC; 
{ /* Llib/trace.scm 180 */
 obj_t BgL_tmp1905z00_2454;
{ /* Llib/trace.scm 181 */
 obj_t BgL_arg1272z00_1196;
{ /* Llib/trace.scm 181 */
 obj_t BgL_keyz00_1725;
BgL_keyz00_1725 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1726;
BgL_cz00_1726 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1725, BgL_alz00_1191); 
if(
PAIRP(BgL_cz00_1726))
{ /* Llib/trace.scm 98 */
BgL_arg1272z00_1196 = 
CDR(BgL_cz00_1726); }  else 
{ /* Llib/trace.scm 98 */
BgL_arg1272z00_1196 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1725); } } } 
bgl_display_obj(BgL_arg1272z00_1196, BgL_pz00_1194); } 
{ /* Llib/trace.scm 182 */
 obj_t BgL_arg1284z00_1197;
{ /* Llib/trace.scm 182 */
 long BgL_arg1304z00_1198;
{ /* Llib/trace.scm 182 */
 obj_t BgL_arg1306z00_1200;
{ /* Llib/trace.scm 182 */
 obj_t BgL_keyz00_1729;
BgL_keyz00_1729 = BGl_symbol1773z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1730;
BgL_cz00_1730 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1729, BgL_alz00_1191); 
if(
PAIRP(BgL_cz00_1730))
{ /* Llib/trace.scm 98 */
BgL_arg1306z00_1200 = 
CDR(BgL_cz00_1730); }  else 
{ /* Llib/trace.scm 98 */
BgL_arg1306z00_1200 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1729); } } } 
{ /* Llib/trace.scm 182 */
 long BgL_za71za7_1733;
{ /* Llib/trace.scm 182 */
 obj_t BgL_tmpz00_2470;
if(
INTEGERP(BgL_arg1306z00_1200))
{ /* Llib/trace.scm 182 */
BgL_tmpz00_2470 = BgL_arg1306z00_1200
; }  else 
{ 
 obj_t BgL_auxz00_2473;
BgL_auxz00_2473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(7188L), BGl_string1803z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_arg1306z00_1200); 
FAILURE(BgL_auxz00_2473,BFALSE,BFALSE);} 
BgL_za71za7_1733 = 
(long)CINT(BgL_tmpz00_2470); } 
BgL_arg1304z00_1198 = 
(BgL_za71za7_1733-1L); } } 
{ /* Llib/trace.scm 182 */
 obj_t BgL_list1305z00_1199;
BgL_list1305z00_1199 = 
MAKE_YOUNG_PAIR(BGl_string1804z00zz__tracez00, BNIL); 
BgL_arg1284z00_1197 = 
BGl_ttyzd2tracezd2colorz00zz__tracez00(
(int)(BgL_arg1304z00_1198), BgL_list1305z00_1199); } } 
bgl_display_obj(BgL_arg1284z00_1197, BgL_pz00_1194); } 
{ 
 obj_t BgL_l1084z00_1202;
BgL_l1084z00_1202 = BgL_argsz00_16; 
BgL_zc3z04anonymousza31307ze3z87_1203:
if(
PAIRP(BgL_l1084z00_1202))
{ /* Llib/trace.scm 183 */
BGl_displayzd2circlezd2zz__pp_circlez00(
CAR(BgL_l1084z00_1202), BgL_pz00_1194); 
{ 
 obj_t BgL_l1084z00_2487;
BgL_l1084z00_2487 = 
CDR(BgL_l1084z00_1202); 
BgL_l1084z00_1202 = BgL_l1084z00_2487; 
goto BgL_zc3z04anonymousza31307ze3z87_1203;} }  else 
{ /* Llib/trace.scm 183 */
if(
NULLP(BgL_l1084z00_1202))
{ /* Llib/trace.scm 183 */BTRUE; }  else 
{ /* Llib/trace.scm 183 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1797z00zz__tracez00, BGl_string1798z00zz__tracez00, BgL_l1084z00_1202, BGl_string1783z00zz__tracez00, 
BINT(7206L)); } } } 
bgl_display_char(((unsigned char)10), BgL_pz00_1194); 
BgL_tmp1905z00_2454 = 
bgl_flush_output_port(BgL_pz00_1194); 
BGL_EXITD_POP_PROTECT(BgL_top1906z00_2455); BUNSPEC; 
BGL_MUTEX_UNLOCK(BgL_mutex1270z00_1195); 
return BgL_tmp1905z00_2454;} } } }  else 
{ /* Llib/trace.scm 178 */
return BFALSE;} } }  else 
{ /* Llib/trace.scm 176 */
return BFALSE;} } } 

}



/* &trace-item */
obj_t BGl_z62tracezd2itemzb0zz__tracez00(obj_t BgL_envz00_2034, obj_t BgL_argsz00_2035)
{
{ /* Llib/trace.scm 175 */
return 
BGl_tracezd2itemzd2zz__tracez00(BgL_argsz00_2035);} 

}



/* trace-active? */
BGL_EXPORTED_DEF obj_t BGl_tracezd2activezf3z21zz__tracez00(obj_t BgL_lvlz00_17)
{
{ /* Llib/trace.scm 190 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_lvlz00_17))
{ /* Llib/trace.scm 192 */
 int BgL_arg1314z00_1212;
BgL_arg1314z00_1212 = 
bgl_debug(); 
{ /* Llib/trace.scm 192 */
 long BgL_n1z00_1738; long BgL_n2z00_1739;
BgL_n1z00_1738 = 
(long)(BgL_arg1314z00_1212); 
{ /* Llib/trace.scm 192 */
 obj_t BgL_tmpz00_2502;
if(
INTEGERP(BgL_lvlz00_17))
{ /* Llib/trace.scm 192 */
BgL_tmpz00_2502 = BgL_lvlz00_17
; }  else 
{ 
 obj_t BgL_auxz00_2505;
BgL_auxz00_2505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(7607L), BGl_string1805z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_lvlz00_17); 
FAILURE(BgL_auxz00_2505,BFALSE,BFALSE);} 
BgL_n2z00_1739 = 
(long)CINT(BgL_tmpz00_2502); } 
return 
BBOOL(
(BgL_n1z00_1738>=BgL_n2z00_1739));} }  else 
{ /* Llib/trace.scm 192 */
if(
SYMBOLP(BgL_lvlz00_17))
{ /* Llib/trace.scm 193 */
return 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_lvlz00_17, 
BGl_bigloozd2tracezd2zz__paramz00());}  else 
{ /* Llib/trace.scm 193 */
return BFALSE;} } } 

}



/* &trace-active? */
obj_t BGl_z62tracezd2activezf3z43zz__tracez00(obj_t BgL_envz00_2036, obj_t BgL_lvlz00_2037)
{
{ /* Llib/trace.scm 190 */
return 
BGl_tracezd2activezf3z21zz__tracez00(BgL_lvlz00_2037);} 

}



/* %with-trace */
BGL_EXPORTED_DEF obj_t BGl_z52withzd2tracez80zz__tracez00(obj_t BgL_lvlz00_18, obj_t BgL_lblz00_19, obj_t BgL_thunkz00_20)
{
{ /* Llib/trace.scm 198 */
{ /* Llib/trace.scm 199 */
 obj_t BgL_alz00_1215;
BgL_alz00_1215 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 199 */
 obj_t BgL_olz00_1216;
{ /* Llib/trace.scm 200 */
 obj_t BgL_keyz00_1740;
BgL_keyz00_1740 = BGl_symbol1778z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1741;
BgL_cz00_1741 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1740, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1741))
{ /* Llib/trace.scm 98 */
BgL_olz00_1216 = 
CDR(BgL_cz00_1741); }  else 
{ /* Llib/trace.scm 98 */
BgL_olz00_1216 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1740); } } } 
{ /* Llib/trace.scm 200 */

{ /* Llib/trace.scm 201 */
 obj_t BgL_keyz00_1744;
BgL_keyz00_1744 = BGl_symbol1778z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_1745;
BgL_cz00_1745 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1744, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1745))
{ /* Llib/trace.scm 107 */
SET_CDR(BgL_cz00_1745, BgL_lvlz00_18); }  else 
{ /* Llib/trace.scm 107 */
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1744); } } } 
if(
CBOOL(
BGl_tracezd2activezf3z21zz__tracez00(BgL_lvlz00_18)))
{ /* Llib/trace.scm 203 */
 obj_t BgL_dz00_1218;
{ /* Llib/trace.scm 203 */
 obj_t BgL_keyz00_1748;
BgL_keyz00_1748 = BGl_symbol1773z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1749;
BgL_cz00_1749 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1748, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1749))
{ /* Llib/trace.scm 98 */
BgL_dz00_1218 = 
CDR(BgL_cz00_1749); }  else 
{ /* Llib/trace.scm 98 */
BgL_dz00_1218 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1748); } } } 
{ /* Llib/trace.scm 203 */
 obj_t BgL_omz00_1219;
{ /* Llib/trace.scm 204 */
 obj_t BgL_keyz00_1752;
BgL_keyz00_1752 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_1753;
BgL_cz00_1753 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1752, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1753))
{ /* Llib/trace.scm 98 */
BgL_omz00_1219 = 
CDR(BgL_cz00_1753); }  else 
{ /* Llib/trace.scm 98 */
BgL_omz00_1219 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1752); } } } 
{ /* Llib/trace.scm 204 */
 obj_t BgL_maz00_1220;
{ /* Llib/trace.scm 205 */
 obj_t BgL_list1337z00_1244;
BgL_list1337z00_1244 = 
MAKE_YOUNG_PAIR(BGl_string1806z00zz__tracez00, BNIL); 
{ /* Llib/trace.scm 205 */
 int BgL_auxz00_2542;
{ /* Llib/trace.scm 205 */
 obj_t BgL_tmpz00_2543;
if(
INTEGERP(BgL_dz00_1218))
{ /* Llib/trace.scm 205 */
BgL_tmpz00_2543 = BgL_dz00_1218
; }  else 
{ 
 obj_t BgL_auxz00_2546;
BgL_auxz00_2546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8157L), BGl_string1807z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_dz00_1218); 
FAILURE(BgL_auxz00_2546,BFALSE,BFALSE);} 
BgL_auxz00_2542 = 
CINT(BgL_tmpz00_2543); } 
BgL_maz00_1220 = 
BGl_ttyzd2tracezd2colorz00zz__tracez00(BgL_auxz00_2542, BgL_list1337z00_1244); } } 
{ /* Llib/trace.scm 205 */

{ /* Llib/trace.scm 206 */
 obj_t BgL_mutex1318z00_1221;
BgL_mutex1318z00_1221 = BGl_za2tracezd2mutexza2zd2zz__tracez00; 
{ /* Llib/trace.scm 206 */
 obj_t BgL_top1922z00_2553;
BgL_top1922z00_2553 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BgL_mutex1318z00_1221); 
BGL_EXITD_PUSH_PROTECT(BgL_top1922z00_2553, BgL_mutex1318z00_1221); BUNSPEC; 
{ /* Llib/trace.scm 206 */
 obj_t BgL_tmp1921z00_2552;
{ /* Llib/trace.scm 207 */
 obj_t BgL_arg1319z00_1222;
{ /* Llib/trace.scm 207 */
 obj_t BgL_res1722z00_1757;
{ /* Llib/trace.scm 115 */
 obj_t BgL_arg1197z00_1756;
BgL_arg1197z00_1756 = 
BGl_tracezd2alistzd2zz__tracez00(); 
{ /* Llib/trace.scm 115 */
 obj_t BgL_aux1754z00_2088;
BgL_aux1754z00_2088 = 
BGl_tracezd2alistzd2getz00zz__tracez00(BgL_arg1197z00_1756, BGl_symbol1771z00zz__tracez00); 
if(
OUTPUT_PORTP(BgL_aux1754z00_2088))
{ /* Llib/trace.scm 115 */
BgL_res1722z00_1757 = BgL_aux1754z00_2088; }  else 
{ 
 obj_t BgL_auxz00_2561;
BgL_auxz00_2561 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(4232L), BGl_string1807z00zz__tracez00, BGl_string1785z00zz__tracez00, BgL_aux1754z00_2088); 
FAILURE(BgL_auxz00_2561,BFALSE,BFALSE);} } } 
BgL_arg1319z00_1222 = BgL_res1722z00_1757; } 
{ /* Llib/trace.scm 209 */
 obj_t BgL_zc3z04anonymousza31321ze3z87_2039;
BgL_zc3z04anonymousza31321ze3z87_2039 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31321ze3ze5zz__tracez00, 
(int)(0L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31321ze3z87_2039, 
(int)(0L), BgL_alz00_1215); 
PROCEDURE_SET(BgL_zc3z04anonymousza31321ze3z87_2039, 
(int)(1L), BgL_lblz00_19); 
PROCEDURE_SET(BgL_zc3z04anonymousza31321ze3z87_2039, 
(int)(2L), BgL_dz00_1218); 
BgL_tmp1921z00_2552 = 
BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_arg1319z00_1222, BgL_zc3z04anonymousza31321ze3z87_2039); } } 
BGL_EXITD_POP_PROTECT(BgL_top1922z00_2553); BUNSPEC; 
BGL_MUTEX_UNLOCK(BgL_mutex1318z00_1221); BgL_tmp1921z00_2552; } } } 
{ /* Llib/trace.scm 215 */
 long BgL_arg1334z00_1237;
{ /* Llib/trace.scm 215 */
 long BgL_za71za7_1769;
{ /* Llib/trace.scm 215 */
 obj_t BgL_tmpz00_2577;
if(
INTEGERP(BgL_dz00_1218))
{ /* Llib/trace.scm 215 */
BgL_tmpz00_2577 = BgL_dz00_1218
; }  else 
{ 
 obj_t BgL_auxz00_2580;
BgL_auxz00_2580 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8534L), BGl_string1807z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_dz00_1218); 
FAILURE(BgL_auxz00_2580,BFALSE,BFALSE);} 
BgL_za71za7_1769 = 
(long)CINT(BgL_tmpz00_2577); } 
BgL_arg1334z00_1237 = 
(BgL_za71za7_1769+1L); } 
{ /* Llib/trace.scm 215 */
 obj_t BgL_keyz00_1770;
BgL_keyz00_1770 = BGl_symbol1773z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_1771;
BgL_cz00_1771 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1770, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1771))
{ /* Llib/trace.scm 108 */
 obj_t BgL_tmpz00_2589;
BgL_tmpz00_2589 = 
BINT(BgL_arg1334z00_1237); 
SET_CDR(BgL_cz00_1771, BgL_tmpz00_2589); }  else 
{ /* Llib/trace.scm 107 */
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1770); } } } } 
{ /* Llib/trace.scm 216 */
 obj_t BgL_arg1335z00_1238;
{ /* Llib/trace.scm 216 */
 obj_t BgL_tmpz00_2593;
if(
STRINGP(BgL_omz00_1219))
{ /* Llib/trace.scm 216 */
BgL_tmpz00_2593 = BgL_omz00_1219
; }  else 
{ 
 obj_t BgL_auxz00_2596;
BgL_auxz00_2596 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8575L), BGl_string1807z00zz__tracez00, BGl_string1790z00zz__tracez00, BgL_omz00_1219); 
FAILURE(BgL_auxz00_2596,BFALSE,BFALSE);} 
BgL_arg1335z00_1238 = 
string_append(BgL_tmpz00_2593, BgL_maz00_1220); } 
{ /* Llib/trace.scm 216 */
 obj_t BgL_keyz00_1774;
BgL_keyz00_1774 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_1775;
BgL_cz00_1775 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1774, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1775))
{ /* Llib/trace.scm 107 */
SET_CDR(BgL_cz00_1775, BgL_arg1335z00_1238); }  else 
{ /* Llib/trace.scm 107 */
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1774); } } } } 
{ /* Llib/trace.scm 217 */
 obj_t BgL_exitd1039z00_1239;
BgL_exitd1039z00_1239 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/trace.scm 220 */
 obj_t BgL_zc3z04anonymousza31336ze3z87_2038;
BgL_zc3z04anonymousza31336ze3z87_2038 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31336ze3ze5zz__tracez00, 
(int)(0L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31336ze3z87_2038, 
(int)(0L), BgL_alz00_1215); 
PROCEDURE_SET(BgL_zc3z04anonymousza31336ze3z87_2038, 
(int)(1L), BgL_dz00_1218); 
PROCEDURE_SET(BgL_zc3z04anonymousza31336ze3z87_2038, 
(int)(2L), BgL_omz00_1219); 
PROCEDURE_SET(BgL_zc3z04anonymousza31336ze3z87_2038, 
(int)(3L), BgL_olz00_1216); 
{ /* Llib/trace.scm 217 */
 obj_t BgL_arg1717z00_1790;
{ /* Llib/trace.scm 217 */
 obj_t BgL_arg1718z00_1791;
BgL_arg1718z00_1791 = 
BGL_EXITD_PROTECT(BgL_exitd1039z00_1239); 
BgL_arg1717z00_1790 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31336ze3z87_2038, BgL_arg1718z00_1791); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1039z00_1239, BgL_arg1717z00_1790); BUNSPEC; } 
{ /* Llib/trace.scm 218 */
 obj_t BgL_tmp1041z00_1241;
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_20, 0))
{ /* Llib/trace.scm 218 */
BgL_tmp1041z00_1241 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_20); }  else 
{ /* Llib/trace.scm 218 */
FAILURE(BGl_string1808z00zz__tracez00,BGl_list1809z00zz__tracez00,BgL_thunkz00_20);} 
{ /* Llib/trace.scm 217 */
 bool_t BgL_test1929z00_2627;
{ /* Llib/trace.scm 217 */
 obj_t BgL_arg1715z00_1793;
BgL_arg1715z00_1793 = 
BGL_EXITD_PROTECT(BgL_exitd1039z00_1239); 
BgL_test1929z00_2627 = 
PAIRP(BgL_arg1715z00_1793); } 
if(BgL_test1929z00_2627)
{ /* Llib/trace.scm 217 */
 obj_t BgL_arg1711z00_1794;
{ /* Llib/trace.scm 217 */
 obj_t BgL_arg1714z00_1795;
BgL_arg1714z00_1795 = 
BGL_EXITD_PROTECT(BgL_exitd1039z00_1239); 
{ /* Llib/trace.scm 217 */
 obj_t BgL_pairz00_1796;
if(
PAIRP(BgL_arg1714z00_1795))
{ /* Llib/trace.scm 217 */
BgL_pairz00_1796 = BgL_arg1714z00_1795; }  else 
{ 
 obj_t BgL_auxz00_2633;
BgL_auxz00_2633 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8604L), BGl_string1807z00zz__tracez00, BGl_string1801z00zz__tracez00, BgL_arg1714z00_1795); 
FAILURE(BgL_auxz00_2633,BFALSE,BFALSE);} 
BgL_arg1711z00_1794 = 
CDR(BgL_pairz00_1796); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1039z00_1239, BgL_arg1711z00_1794); BUNSPEC; }  else 
{ /* Llib/trace.scm 217 */BFALSE; } } 
BGl_z62zc3z04anonymousza31336ze3ze5zz__tracez00(BgL_zc3z04anonymousza31336ze3z87_2038); 
return BgL_tmp1041z00_1241;} } } } } } }  else 
{ /* Llib/trace.scm 223 */
 obj_t BgL_exitd1043z00_1245;
BgL_exitd1043z00_1245 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/trace.scm 225 */
 obj_t BgL_zc3z04anonymousza31338ze3z87_2040;
BgL_zc3z04anonymousza31338ze3z87_2040 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31338ze3ze5zz__tracez00, 
(int)(0L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_2040, 
(int)(0L), BgL_alz00_1215); 
PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_2040, 
(int)(1L), BgL_olz00_1216); 
{ /* Llib/trace.scm 223 */
 obj_t BgL_arg1717z00_1801;
{ /* Llib/trace.scm 223 */
 obj_t BgL_arg1718z00_1802;
BgL_arg1718z00_1802 = 
BGL_EXITD_PROTECT(BgL_exitd1043z00_1245); 
BgL_arg1717z00_1801 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31338ze3z87_2040, BgL_arg1718z00_1802); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1043z00_1245, BgL_arg1717z00_1801); BUNSPEC; } 
{ /* Llib/trace.scm 224 */
 obj_t BgL_tmp1045z00_1247;
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_20, 0))
{ /* Llib/trace.scm 224 */
BgL_tmp1045z00_1247 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_20); }  else 
{ /* Llib/trace.scm 224 */
FAILURE(BGl_string1808z00zz__tracez00,BGl_list1809z00zz__tracez00,BgL_thunkz00_20);} 
{ /* Llib/trace.scm 223 */
 bool_t BgL_test1932z00_2657;
{ /* Llib/trace.scm 223 */
 obj_t BgL_arg1715z00_1804;
BgL_arg1715z00_1804 = 
BGL_EXITD_PROTECT(BgL_exitd1043z00_1245); 
BgL_test1932z00_2657 = 
PAIRP(BgL_arg1715z00_1804); } 
if(BgL_test1932z00_2657)
{ /* Llib/trace.scm 223 */
 obj_t BgL_arg1711z00_1805;
{ /* Llib/trace.scm 223 */
 obj_t BgL_arg1714z00_1806;
BgL_arg1714z00_1806 = 
BGL_EXITD_PROTECT(BgL_exitd1043z00_1245); 
{ /* Llib/trace.scm 223 */
 obj_t BgL_pairz00_1807;
if(
PAIRP(BgL_arg1714z00_1806))
{ /* Llib/trace.scm 223 */
BgL_pairz00_1807 = BgL_arg1714z00_1806; }  else 
{ 
 obj_t BgL_auxz00_2663;
BgL_auxz00_2663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8763L), BGl_string1807z00zz__tracez00, BGl_string1801z00zz__tracez00, BgL_arg1714z00_1806); 
FAILURE(BgL_auxz00_2663,BFALSE,BFALSE);} 
BgL_arg1711z00_1805 = 
CDR(BgL_pairz00_1807); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1043z00_1245, BgL_arg1711z00_1805); BUNSPEC; }  else 
{ /* Llib/trace.scm 223 */BFALSE; } } 
{ /* Llib/trace.scm 225 */
 obj_t BgL_keyz00_1808;
BgL_keyz00_1808 = BGl_symbol1778z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_1809;
BgL_cz00_1809 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_1808, BgL_alz00_1215); 
if(
PAIRP(BgL_cz00_1809))
{ /* Llib/trace.scm 107 */
SET_CDR(BgL_cz00_1809, BgL_olz00_1216); }  else 
{ /* Llib/trace.scm 107 */
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_1808); } } } 
return BgL_tmp1045z00_1247;} } } } } } } 

}



/* &%with-trace */
obj_t BGl_z62z52withzd2traceze2zz__tracez00(obj_t BgL_envz00_2041, obj_t BgL_lvlz00_2042, obj_t BgL_lblz00_2043, obj_t BgL_thunkz00_2044)
{
{ /* Llib/trace.scm 198 */
{ /* Llib/trace.scm 199 */
 obj_t BgL_auxz00_2674;
if(
PROCEDUREP(BgL_thunkz00_2044))
{ /* Llib/trace.scm 199 */
BgL_auxz00_2674 = BgL_thunkz00_2044
; }  else 
{ 
 obj_t BgL_auxz00_2677;
BgL_auxz00_2677 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(7925L), BGl_string1814z00zz__tracez00, BGl_string1815z00zz__tracez00, BgL_thunkz00_2044); 
FAILURE(BgL_auxz00_2677,BFALSE,BFALSE);} 
return 
BGl_z52withzd2tracez80zz__tracez00(BgL_lvlz00_2042, BgL_lblz00_2043, BgL_auxz00_2674);} } 

}



/* &<@anonymous:1336> */
obj_t BGl_z62zc3z04anonymousza31336ze3ze5zz__tracez00(obj_t BgL_envz00_2045)
{
{ /* Llib/trace.scm 217 */
{ /* Llib/trace.scm 220 */
 obj_t BgL_alz00_2046; obj_t BgL_dz00_2047; obj_t BgL_omz00_2048; obj_t BgL_olz00_2049;
BgL_alz00_2046 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2045, 
(int)(0L))); 
BgL_dz00_2047 = 
PROCEDURE_REF(BgL_envz00_2045, 
(int)(1L)); 
BgL_omz00_2048 = 
PROCEDURE_REF(BgL_envz00_2045, 
(int)(2L)); 
BgL_olz00_2049 = 
PROCEDURE_REF(BgL_envz00_2045, 
(int)(3L)); 
{ /* Llib/trace.scm 220 */
 obj_t BgL_keyz00_2131;
BgL_keyz00_2131 = BGl_symbol1773z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_2132;
BgL_cz00_2132 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2131, BgL_alz00_2046); 
if(
PAIRP(BgL_cz00_2132))
{ /* Llib/trace.scm 107 */
SET_CDR(BgL_cz00_2132, BgL_dz00_2047); }  else 
{ /* Llib/trace.scm 107 */
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_2131); } } } 
{ /* Llib/trace.scm 221 */
 obj_t BgL_keyz00_2133;
BgL_keyz00_2133 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_2134;
BgL_cz00_2134 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2133, BgL_alz00_2046); 
if(
PAIRP(BgL_cz00_2134))
{ /* Llib/trace.scm 107 */
SET_CDR(BgL_cz00_2134, BgL_omz00_2048); }  else 
{ /* Llib/trace.scm 107 */
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_2133); } } } 
{ /* Llib/trace.scm 222 */
 obj_t BgL_keyz00_2135;
BgL_keyz00_2135 = BGl_symbol1778z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_2136;
BgL_cz00_2136 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2135, BgL_alz00_2046); 
if(
PAIRP(BgL_cz00_2136))
{ /* Llib/trace.scm 107 */
return 
SET_CDR(BgL_cz00_2136, BgL_olz00_2049);}  else 
{ /* Llib/trace.scm 107 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_2135);} } } } } 

}



/* &<@anonymous:1321> */
obj_t BGl_z62zc3z04anonymousza31321ze3ze5zz__tracez00(obj_t BgL_envz00_2050)
{
{ /* Llib/trace.scm 208 */
{ /* Llib/trace.scm 209 */
 obj_t BgL_alz00_2051; obj_t BgL_lblz00_2052; obj_t BgL_dz00_2053;
BgL_alz00_2051 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2050, 
(int)(0L))); 
BgL_lblz00_2052 = 
PROCEDURE_REF(BgL_envz00_2050, 
(int)(1L)); 
BgL_dz00_2053 = 
PROCEDURE_REF(BgL_envz00_2050, 
(int)(2L)); 
{ /* Llib/trace.scm 209 */
 obj_t BgL_arg1322z00_2137; obj_t BgL_arg1323z00_2138;
{ /* Llib/trace.scm 209 */
 obj_t BgL_keyz00_2139;
BgL_keyz00_2139 = BGl_symbol1775z00zz__tracez00; 
{ /* Llib/trace.scm 97 */
 obj_t BgL_cz00_2140;
BgL_cz00_2140 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2139, BgL_alz00_2051); 
if(
PAIRP(BgL_cz00_2140))
{ /* Llib/trace.scm 98 */
BgL_arg1322z00_2137 = 
CDR(BgL_cz00_2140); }  else 
{ /* Llib/trace.scm 98 */
BgL_arg1322z00_2137 = 
BGl_errorz00zz__errorz00(BGl_symbol1780z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_2139); } } } 
{ /* Llib/trace.scm 209 */
 obj_t BgL_tmpz00_2718;
BgL_tmpz00_2718 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1323z00_2138 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2718); } 
bgl_display_obj(BgL_arg1322z00_2137, BgL_arg1323z00_2138); } 
{ /* Llib/trace.scm 210 */
 obj_t BgL_arg1325z00_2141; obj_t BgL_arg1326z00_2142;
{ /* Llib/trace.scm 210 */
 bool_t BgL_test1940z00_2722;
{ /* Llib/trace.scm 210 */
 long BgL_n1z00_2143;
{ /* Llib/trace.scm 210 */
 obj_t BgL_tmpz00_2723;
if(
INTEGERP(BgL_dz00_2053))
{ /* Llib/trace.scm 210 */
BgL_tmpz00_2723 = BgL_dz00_2053
; }  else 
{ 
 obj_t BgL_auxz00_2726;
BgL_auxz00_2726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8343L), BGl_string1816z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_dz00_2053); 
FAILURE(BgL_auxz00_2726,BFALSE,BFALSE);} 
BgL_n1z00_2143 = 
(long)CINT(BgL_tmpz00_2723); } 
BgL_test1940z00_2722 = 
(BgL_n1z00_2143==0L); } 
if(BgL_test1940z00_2722)
{ /* Llib/trace.scm 211 */
 obj_t BgL_list1328z00_2144;
{ /* Llib/trace.scm 211 */
 obj_t BgL_arg1329z00_2145;
BgL_arg1329z00_2145 = 
MAKE_YOUNG_PAIR(BgL_lblz00_2052, BNIL); 
BgL_list1328z00_2144 = 
MAKE_YOUNG_PAIR(BGl_string1817z00zz__tracez00, BgL_arg1329z00_2145); } 
{ /* Llib/trace.scm 211 */
 int BgL_auxz00_2734;
{ /* Llib/trace.scm 211 */
 obj_t BgL_tmpz00_2735;
if(
INTEGERP(BgL_dz00_2053))
{ /* Llib/trace.scm 211 */
BgL_tmpz00_2735 = BgL_dz00_2053
; }  else 
{ 
 obj_t BgL_auxz00_2738;
BgL_auxz00_2738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8355L), BGl_string1816z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_dz00_2053); 
FAILURE(BgL_auxz00_2738,BFALSE,BFALSE);} 
BgL_auxz00_2734 = 
CINT(BgL_tmpz00_2735); } 
BgL_arg1325z00_2141 = 
BGl_ttyzd2tracezd2colorz00zz__tracez00(BgL_auxz00_2734, BgL_list1328z00_2144); } }  else 
{ /* Llib/trace.scm 212 */
 obj_t BgL_list1330z00_2146;
{ /* Llib/trace.scm 212 */
 obj_t BgL_arg1331z00_2147;
BgL_arg1331z00_2147 = 
MAKE_YOUNG_PAIR(BgL_lblz00_2052, BNIL); 
BgL_list1330z00_2146 = 
MAKE_YOUNG_PAIR(BGl_string1818z00zz__tracez00, BgL_arg1331z00_2147); } 
{ /* Llib/trace.scm 212 */
 int BgL_auxz00_2746;
{ /* Llib/trace.scm 212 */
 obj_t BgL_tmpz00_2747;
if(
INTEGERP(BgL_dz00_2053))
{ /* Llib/trace.scm 212 */
BgL_tmpz00_2747 = BgL_dz00_2053
; }  else 
{ 
 obj_t BgL_auxz00_2750;
BgL_auxz00_2750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1783z00zz__tracez00, 
BINT(8391L), BGl_string1816z00zz__tracez00, BGl_string1793z00zz__tracez00, BgL_dz00_2053); 
FAILURE(BgL_auxz00_2750,BFALSE,BFALSE);} 
BgL_auxz00_2746 = 
CINT(BgL_tmpz00_2747); } 
BgL_arg1325z00_2141 = 
BGl_ttyzd2tracezd2colorz00zz__tracez00(BgL_auxz00_2746, BgL_list1330z00_2146); } } } 
{ /* Llib/trace.scm 210 */
 obj_t BgL_tmpz00_2756;
BgL_tmpz00_2756 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1326z00_2142 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2756); } 
bgl_display_obj(BgL_arg1325z00_2141, BgL_arg1326z00_2142); } 
{ /* Llib/trace.scm 213 */
 obj_t BgL_arg1332z00_2148;
{ /* Llib/trace.scm 213 */
 obj_t BgL_tmpz00_2760;
BgL_tmpz00_2760 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1332z00_2148 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2760); } 
bgl_display_char(((unsigned char)10), BgL_arg1332z00_2148); } 
{ /* Llib/trace.scm 214 */
 obj_t BgL_arg1333z00_2149;
{ /* Llib/trace.scm 214 */
 obj_t BgL_tmpz00_2764;
BgL_tmpz00_2764 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1333z00_2149 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2764); } 
return 
bgl_flush_output_port(BgL_arg1333z00_2149);} } } 

}



/* &<@anonymous:1338> */
obj_t BGl_z62zc3z04anonymousza31338ze3ze5zz__tracez00(obj_t BgL_envz00_2054)
{
{ /* Llib/trace.scm 223 */
{ /* Llib/trace.scm 225 */
 obj_t BgL_alz00_2055; obj_t BgL_olz00_2056;
BgL_alz00_2055 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2054, 
(int)(0L))); 
BgL_olz00_2056 = 
PROCEDURE_REF(BgL_envz00_2054, 
(int)(1L)); 
{ /* Llib/trace.scm 225 */
 obj_t BgL_keyz00_2150;
BgL_keyz00_2150 = BGl_symbol1778z00zz__tracez00; 
{ /* Llib/trace.scm 106 */
 obj_t BgL_cz00_2151;
BgL_cz00_2151 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_keyz00_2150, BgL_alz00_2055); 
if(
PAIRP(BgL_cz00_2151))
{ /* Llib/trace.scm 107 */
return 
SET_CDR(BgL_cz00_2151, BgL_olz00_2056);}  else 
{ /* Llib/trace.scm 107 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1786z00zz__tracez00, BGl_string1782z00zz__tracez00, BgL_keyz00_2150);} } } } } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__tracez00(void)
{
{ /* Llib/trace.scm 15 */
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1819z00zz__tracez00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1819z00zz__tracez00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1819z00zz__tracez00)); 
return 
BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L, 
BSTRING_TO_STRING(BGl_string1819z00zz__tracez00));} 

}

#ifdef __cplusplus
}
#endif
