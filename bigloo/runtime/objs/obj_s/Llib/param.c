/*===========================================================================*/
/*   (Llib/param.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/param.scm -indent -o objs/obj_s/Llib/param.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PARAM_TYPE_DEFINITIONS
#define BGL___PARAM_TYPE_DEFINITIONS
#endif // BGL___PARAM_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutz62zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2loadzd2modulezd2setz12za2zz__paramz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2profilezb0zz__paramz00(obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62bigloozd2debugzd2modulezd2setz12za2zz__paramz00(obj_t, obj_t);
static obj_t BGl_symbol1671z00zz__paramz00 = BUNSPEC;
static obj_t BGl_symbol1673z00zz__paramz00 = BUNSPEC;
static obj_t BGl_symbol1678z00zz__paramz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2tracezd2setz12z12zz__paramz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void);
static obj_t BGl_za2parameterzd2mutexza2zd2zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2profilezd2setz12z70zz__paramz00(obj_t, obj_t);
static bool_t BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00;
BGL_EXPORTED_DECL int BGl_bigloozd2warningzd2zz__paramz00(void);
static obj_t BGl_symbol1683z00zz__paramz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2loadzd2readerz00zz__paramz00(void);
static obj_t BGl_symbol1687z00zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2debugzd2setz12z70zz__paramz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__paramz00 = BUNSPEC;
static obj_t BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2tracezd2stackzd2depthzd2setz12z70zz__paramz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2tracezd2stackzd2depthzb0zz__paramz00(obj_t);
static obj_t BGl_symbol1692z00zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2debugzd2modulez62zz__paramz00(obj_t);
static obj_t BGl_symbol1697z00zz__paramz00 = BUNSPEC;
static obj_t BGl_za2bigloozd2profileza2zd2zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2evalzd2strictzd2modulezb0zz__paramz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zc0zz__paramz00(long);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2compilerzd2debugzd2setz12zc0zz__paramz00(int);
static obj_t BGl_za2bigloozd2warningza2zd2zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2debugzb0zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszb0zz__paramz00(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2loadzd2modulez00zz__paramz00(void);
static obj_t BGl_z62bigloozd2dnszd2enablezd2cachezd2setz12z70zz__paramz00(obj_t, obj_t);
static obj_t BGl_za2bigloozd2debugza2zd2zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2evalzd2strictzd2modulezd2setz12z70zz__paramz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2loadzd2readerz62zz__paramz00(obj_t);
BGL_EXPORTED_DECL long bgl_dns_cache_validity_timeout(void);
static obj_t BGl_toplevelzd2initzd2zz__paramz00(void);
BGL_EXPORTED_DECL int bgl_debug(void);
static bool_t BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00;
static obj_t BGl_z62bigloozd2compilerzd2debugzd2setz12za2zz__paramz00(obj_t, obj_t);
static obj_t BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2dnszd2enablezd2cachezd2setz12z12zz__paramz00(bool_t);
static obj_t BGl_z62bigloozd2warningzd2setz12z70zz__paramz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2initializa7edz12z67zz__paramz00(void);
static obj_t BGl_z62bigloozd2loadzd2modulez62zz__paramz00(obj_t);
BGL_EXPORTED_DECL bool_t bgl_dns_enable_cache(void);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2casezd2sensitivezd2setz12zc0zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2dnszd2enablezd2cachezb0zz__paramz00(obj_t);
extern obj_t BGl_getenvz00zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t bgl_debug_set(int);
static obj_t BGl_cnstzd2initzd2zz__paramz00(void);
BGL_EXPORTED_DECL int BGl_bigloozd2profilezd2zz__paramz00(void);
extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_z62bigloozd2tracezb0zz__paramz00(obj_t);
static obj_t BGl_genericzd2initzd2zz__paramz00(void);
BGL_EXPORTED_DECL bool_t BGl_bigloozd2initializa7edzf3z86zz__paramz00(void);
BGL_EXPORTED_DECL bool_t BGl_bigloozd2evalzd2strictzd2modulezd2zz__paramz00(void);
static obj_t BGl_importedzd2moduleszd2initz00zz__paramz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__paramz00(void);
static obj_t BGl_objectzd2initzd2zz__paramz00(void);
static obj_t BGl_z62bigloozd2libraryzd2pathz62zz__paramz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00(void);
static obj_t BGl_z62bigloozd2modulezd2extensionzd2handlerzb0zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2casezd2sensitivezd2setz12za2zz__paramz00(obj_t, obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2libraryzd2pathzd2setz12zc0zz__paramz00(obj_t);
static obj_t BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00 = BUNSPEC;
static bool_t BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2tracezd2zz__paramz00(void);
extern obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2casezd2sensitivez00zz__paramz00(void);
extern obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2profilezd2setz12z12zz__paramz00(int);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2evalzd2strictzd2modulezd2setz12z12zz__paramz00(bool_t);
BGL_EXPORTED_DECL bool_t BGl_bigloozd2tracezd2colorz00zz__paramz00(void);
static obj_t BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2debugzd2modulezd2setz12zc0zz__paramz00(int);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL int BGl_bigloozd2debugzd2modulez00zz__paramz00(void);
static obj_t BGl_z62bigloozd2modulezd2extensionzd2handlerzd2setz12z70zz__paramz00(obj_t, obj_t);
static long BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00 = 0L;
static obj_t BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 = BUNSPEC;
static obj_t BGl_methodzd2initzd2zz__paramz00(void);
static bool_t BGl_za2bigloozd2initializa7edpza2z75zz__paramz00;
static obj_t BGl_z62bigloozd2warningzb0zz__paramz00(obj_t);
static obj_t BGl_symbol1708z00zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2tracezd2colorz62zz__paramz00(obj_t);
static obj_t BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2strictzd2r5rszd2stringszd2setz12z12zz__paramz00(bool_t);
static obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszd2setz12z70zz__paramz00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12z12zz__paramz00(obj_t);
static obj_t BGl_symbol1710z00zz__paramz00 = BUNSPEC;
static obj_t BGl_symbol1719z00zz__paramz00 = BUNSPEC;
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62bigloozd2casezd2sensitivez62zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12za2zz__paramz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2tracezd2setz12z70zz__paramz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int);
static obj_t BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2initializa7edz12z05zz__paramz00(obj_t);
static bool_t BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(int);
static obj_t BGl_z62bigloozd2libraryzd2pathzd2setz12za2zz__paramz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2compilerzd2debugz62zz__paramz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2tracezd2colorzd2setz12zc0zz__paramz00(bool_t);
static obj_t BGl_z62bigloozd2initializa7edzf3ze4zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2tracezd2colorzd2setz12za2zz__paramz00(obj_t, obj_t);
static obj_t BGl_list1707z00zz__paramz00 = BUNSPEC;
static obj_t BGl_za2bigloozd2traceza2zd2zz__paramz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2libraryzd2pathz00zz__paramz00(void);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2loadzd2readerzd2setz12zc0zz__paramz00(obj_t);
static obj_t BGl_z62bigloozd2loadzd2readerzd2setz12za2zz__paramz00(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static long BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 = 0L;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2loadzd2modulezd2setz12zc0zz__paramz00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2dnsza71726za7, BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2loadzd2modulezd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2load1727z00, BGl_z62bigloozd2loadzd2modulez62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2modu1728z00, BGl_z62bigloozd2modulezd2extensionzd2handlerzd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2debugzd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2debu1729z00, BGl_z62bigloozd2debugzb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2initializa7edz12zd2envzb5zz__paramz00, BgL_bgl_za762biglooza7d2init1730z00, BGl_z62bigloozd2initializa7edz12z05zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2loadzd2readerzd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2load1731z00, BGl_z62bigloozd2loadzd2readerz62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2tracezd2colorzd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2trac1732z00, BGl_z62bigloozd2tracezd2colorzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2debugzd2modulezd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2debu1733z00, BGl_z62bigloozd2debugzd2modulez62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2modulezd2extensionzd2handlerzd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2modu1734z00, BGl_z62bigloozd2modulezd2extensionzd2handlerzb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2loadzd2modulezd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2load1735z00, BGl_z62bigloozd2loadzd2modulezd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2tracezd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2trac1736z00, BGl_z62bigloozd2tracezb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1700z00zz__paramz00, BgL_bgl_string1700za700za7za7_1737za7, "&bigloo-profile-set!", 20 );
DEFINE_STRING( BGl_string1701z00zz__paramz00, BgL_bgl_string1701za700za7za7_1738za7, "bigloo-trace", 12 );
DEFINE_STRING( BGl_string1702z00zz__paramz00, BgL_bgl_string1702za700za7za7_1739za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string1703z00zz__paramz00, BgL_bgl_string1703za700za7za7_1740za7, "&bigloo-trace-set!", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2casezd2sensitivezd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2case1741z00, BGl_z62bigloozd2casezd2sensitivez62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1704z00zz__paramz00, BgL_bgl_string1704za700za7za7_1742za7, "&bigloo-trace-stack-depth-set!", 30 );
DEFINE_STRING( BGl_string1705z00zz__paramz00, BgL_bgl_string1705za700za7za7_1743za7, "bigloo-case-sensitive", 21 );
DEFINE_STRING( BGl_string1706z00zz__paramz00, BgL_bgl_string1706za700za7za7_1744za7, "symbol", 6 );
DEFINE_STRING( BGl_string1709z00zz__paramz00, BgL_bgl_string1709za700za7za7_1745za7, "downcase", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2tracezd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2trac1746z00, BGl_z62bigloozd2tracezd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2strictzd2r5rszd2stringszd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2stri1747z00, BGl_z62bigloozd2strictzd2r5rszd2stringszb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2profilezd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2prof1748z00, BGl_z62bigloozd2profilezd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2dnszd2enablezd2cachezd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2dnsza71749za7, BGl_z62bigloozd2dnszd2enablezd2cachezd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2evalzd2strictzd2modulezd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2eval1750z00, BGl_z62bigloozd2evalzd2strictzd2modulezd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2tracezd2colorzd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2trac1751z00, BGl_z62bigloozd2tracezd2colorz62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1711z00zz__paramz00, BgL_bgl_string1711za700za7za7_1752za7, "upcase", 6 );
DEFINE_STRING( BGl_string1712z00zz__paramz00, BgL_bgl_string1712za700za7za7_1753za7, "bigloo-sensitivity-set!", 23 );
DEFINE_STRING( BGl_string1713z00zz__paramz00, BgL_bgl_string1713za700za7za7_1754za7, "Illegal sensitive value", 23 );
DEFINE_STRING( BGl_string1714z00zz__paramz00, BgL_bgl_string1714za700za7za7_1755za7, "&bigloo-case-sensitive-set!", 27 );
DEFINE_STRING( BGl_string1715z00zz__paramz00, BgL_bgl_string1715za700za7za7_1756za7, "bigloo-library-path", 19 );
DEFINE_STRING( BGl_string1716z00zz__paramz00, BgL_bgl_string1716za700za7za7_1757za7, "every", 5 );
DEFINE_STRING( BGl_string1717z00zz__paramz00, BgL_bgl_string1717za700za7za7_1758za7, "<@anonymous:1219>", 17 );
DEFINE_STRING( BGl_string1718z00zz__paramz00, BgL_bgl_string1718za700za7za7_1759za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2tracezd2stackzd2depthzd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2trac1760z00, BGl_z62bigloozd2tracezd2stackzd2depthzd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2compilerzd2debugzd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2comp1761z00, BGl_z62bigloozd2compilerzd2debugz62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1720z00zz__paramz00, BgL_bgl_string1720za700za7za7_1762za7, "bigloo-library-path-set!", 24 );
DEFINE_STRING( BGl_string1721z00zz__paramz00, BgL_bgl_string1721za700za7za7_1763za7, "Illegal values", 14 );
DEFINE_STRING( BGl_string1722z00zz__paramz00, BgL_bgl_string1722za700za7za7_1764za7, "Illegal list", 12 );
DEFINE_STRING( BGl_string1723z00zz__paramz00, BgL_bgl_string1723za700za7za7_1765za7, "&bigloo-library-path-set!", 25 );
DEFINE_STRING( BGl_string1724z00zz__paramz00, BgL_bgl_string1724za700za7za7_1766za7, "&bigloo-dns-cache-validity-timeout-set!", 39 );
DEFINE_STRING( BGl_string1725z00zz__paramz00, BgL_bgl_string1725za700za7za7_1767za7, "__param", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2compilerzd2debugzd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2comp1768z00, BGl_z62bigloozd2compilerzd2debugzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2loadzd2readerzd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2load1769z00, BGl_z62bigloozd2loadzd2readerzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2strictzd2r5rszd2stringszd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2stri1770z00, BGl_z62bigloozd2strictzd2r5rszd2stringszd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2dnsza71771za7, BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutz62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1662z00zz__paramz00, BgL_bgl_string1662za700za7za7_1772za7, "param", 5 );
DEFINE_STRING( BGl_string1663z00zz__paramz00, BgL_bgl_string1663za700za7za7_1773za7, "BIGLOOTRACE", 11 );
DEFINE_STRING( BGl_string1664z00zz__paramz00, BgL_bgl_string1664za700za7za7_1774za7, "/tmp/bigloo/runtime/Llib/param.scm", 34 );
DEFINE_STRING( BGl_string1665z00zz__paramz00, BgL_bgl_string1665za700za7za7_1775za7, "toplevel-init", 13 );
DEFINE_STRING( BGl_string1666z00zz__paramz00, BgL_bgl_string1666za700za7za7_1776za7, "bstring", 7 );
DEFINE_STRING( BGl_string1667z00zz__paramz00, BgL_bgl_string1667za700za7za7_1777za7, "<@anonymous:1167>", 17 );
DEFINE_STRING( BGl_string1668z00zz__paramz00, BgL_bgl_string1668za700za7za7_1778za7, "map", 3 );
DEFINE_STRING( BGl_string1669z00zz__paramz00, BgL_bgl_string1669za700za7za7_1779za7, "list", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2warningzd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2warn1780z00, BGl_z62bigloozd2warningzb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2casezd2sensitivezd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2case1781z00, BGl_z62bigloozd2casezd2sensitivezd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2debugzd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2debu1782z00, BGl_z62bigloozd2debugzd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1670z00zz__paramz00, BgL_bgl_string1670za700za7za7_1783za7, "BIGLOOSTACKDEPTH", 16 );
DEFINE_STRING( BGl_string1672z00zz__paramz00, BgL_bgl_string1672za700za7za7_1784za7, "sensitive", 9 );
DEFINE_STRING( BGl_string1674z00zz__paramz00, BgL_bgl_string1674za700za7za7_1785za7, "library-directory", 17 );
DEFINE_STRING( BGl_string1675z00zz__paramz00, BgL_bgl_string1675za700za7za7_1786za7, ".", 1 );
DEFINE_STRING( BGl_string1676z00zz__paramz00, BgL_bgl_string1676za700za7za7_1787za7, "bigloo-compiler-debug", 21 );
DEFINE_STRING( BGl_string1677z00zz__paramz00, BgL_bgl_string1677za700za7za7_1788za7, "bint", 4 );
DEFINE_STRING( BGl_string1679z00zz__paramz00, BgL_bgl_string1679za700za7za7_1789za7, "bigloo-compiler-debug-set!", 26 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2profilezd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2prof1790z00, BGl_z62bigloozd2profilezb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2debugzd2modulezd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2debu1791z00, BGl_z62bigloozd2debugzd2modulezd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2initializa7edzf3zd2envz54zz__paramz00, BgL_bgl_za762biglooza7d2init1792z00, BGl_z62bigloozd2initializa7edzf3ze4zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2evalzd2strictzd2modulezd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2eval1793z00, BGl_z62bigloozd2evalzd2strictzd2modulezb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1680z00zz__paramz00, BgL_bgl_string1680za700za7za7_1794za7, "Illegal debug level", 19 );
DEFINE_STRING( BGl_string1681z00zz__paramz00, BgL_bgl_string1681za700za7za7_1795za7, "&bigloo-compiler-debug-set!", 27 );
DEFINE_STRING( BGl_string1682z00zz__paramz00, BgL_bgl_string1682za700za7za7_1796za7, "bigloo-debug", 12 );
DEFINE_STRING( BGl_string1684z00zz__paramz00, BgL_bgl_string1684za700za7za7_1797za7, "bigloo-debug-set!", 17 );
DEFINE_STRING( BGl_string1685z00zz__paramz00, BgL_bgl_string1685za700za7za7_1798za7, "&bigloo-debug-set!", 18 );
DEFINE_STRING( BGl_string1686z00zz__paramz00, BgL_bgl_string1686za700za7za7_1799za7, "bigloo-debug-module", 19 );
DEFINE_STRING( BGl_string1688z00zz__paramz00, BgL_bgl_string1688za700za7za7_1800za7, "bigloo-debug-module-set!", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2libraryzd2pathzd2envzd2zz__paramz00, BgL_bgl_za762biglooza7d2libr1801z00, BGl_z62bigloozd2libraryzd2pathz62zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1689z00zz__paramz00, BgL_bgl_string1689za700za7za7_1802za7, "Illegal debug module level", 26 );
DEFINE_STRING( BGl_string1690z00zz__paramz00, BgL_bgl_string1690za700za7za7_1803za7, "&bigloo-debug-module-set!", 25 );
DEFINE_STRING( BGl_string1691z00zz__paramz00, BgL_bgl_string1691za700za7za7_1804za7, "bigloo-warning", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2libraryzd2pathzd2setz12zd2envz12zz__paramz00, BgL_bgl_za762biglooza7d2libr1805z00, BGl_z62bigloozd2libraryzd2pathzd2setz12za2zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1693z00zz__paramz00, BgL_bgl_string1693za700za7za7_1806za7, "bigloo-warning-set!", 19 );
DEFINE_STRING( BGl_string1694z00zz__paramz00, BgL_bgl_string1694za700za7za7_1807za7, "Illegal warning level", 21 );
DEFINE_STRING( BGl_string1695z00zz__paramz00, BgL_bgl_string1695za700za7za7_1808za7, "&bigloo-warning-set!", 20 );
DEFINE_STRING( BGl_string1696z00zz__paramz00, BgL_bgl_string1696za700za7za7_1809za7, "bigloo-profile", 14 );
DEFINE_STRING( BGl_string1698z00zz__paramz00, BgL_bgl_string1698za700za7za7_1810za7, "bigloo-profile-set!", 19 );
DEFINE_STRING( BGl_string1699z00zz__paramz00, BgL_bgl_string1699za700za7za7_1811za7, "Illegal profile level", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2tracezd2stackzd2depthzd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2trac1812z00, BGl_z62bigloozd2tracezd2stackzd2depthzb0zz__paramz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2warningzd2setz12zd2envzc0zz__paramz00, BgL_bgl_za762biglooza7d2warn1813z00, BGl_z62bigloozd2warningzd2setz12z70zz__paramz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2dnszd2enablezd2cachezd2envz00zz__paramz00, BgL_bgl_za762biglooza7d2dnsza71814za7, BGl_z62bigloozd2dnszd2enablezd2cachezb0zz__paramz00, 0L, BUNSPEC, 0 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1671z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1673z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1678z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2parameterzd2mutexza2zd2zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1683z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1687z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1692z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1697z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2profileza2zd2zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2warningza2zd2zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2debugza2zd2zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1708z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1710z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_symbol1719z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_list1707z00zz__paramz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2traceza2zd2zz__paramz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long BgL_checksumz00_1897, char * BgL_fromz00_1898)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__paramz00))
{ 
BGl_requirezd2initializa7ationz75zz__paramz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__paramz00(); 
BGl_cnstzd2initzd2zz__paramz00(); 
BGl_importedzd2moduleszd2initz00zz__paramz00(); 
return 
BGl_toplevelzd2initzd2zz__paramz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
BGl_symbol1671z00zz__paramz00 = 
bstring_to_symbol(BGl_string1672z00zz__paramz00); 
BGl_symbol1673z00zz__paramz00 = 
bstring_to_symbol(BGl_string1674z00zz__paramz00); 
BGl_symbol1678z00zz__paramz00 = 
bstring_to_symbol(BGl_string1679z00zz__paramz00); 
BGl_symbol1683z00zz__paramz00 = 
bstring_to_symbol(BGl_string1684z00zz__paramz00); 
BGl_symbol1687z00zz__paramz00 = 
bstring_to_symbol(BGl_string1688z00zz__paramz00); 
BGl_symbol1692z00zz__paramz00 = 
bstring_to_symbol(BGl_string1693z00zz__paramz00); 
BGl_symbol1697z00zz__paramz00 = 
bstring_to_symbol(BGl_string1698z00zz__paramz00); 
BGl_symbol1708z00zz__paramz00 = 
bstring_to_symbol(BGl_string1709z00zz__paramz00); 
BGl_symbol1710z00zz__paramz00 = 
bstring_to_symbol(BGl_string1711z00zz__paramz00); 
BGl_list1707z00zz__paramz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1671z00zz__paramz00, 
MAKE_YOUNG_PAIR(BGl_symbol1708z00zz__paramz00, 
MAKE_YOUNG_PAIR(BGl_symbol1710z00zz__paramz00, BNIL))); 
return ( 
BGl_symbol1719z00zz__paramz00 = 
bstring_to_symbol(BGl_string1720z00zz__paramz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
BGl_za2parameterzd2mutexza2zd2zz__paramz00 = 
bgl_make_mutex(BGl_string1662z00zz__paramz00); 
BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00 = ((bool_t)0); 
BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 = 
BINT(0L); 
BGl_za2bigloozd2debugza2zd2zz__paramz00 = 
BINT(0L); 
BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 = 
BINT(0L); 
BGl_za2bigloozd2warningza2zd2zz__paramz00 = 
BINT(1L); 
BGl_za2bigloozd2profileza2zd2zz__paramz00 = 
BINT(0L); 
BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00 = ((bool_t)1); 
{ /* Llib/param.scm 206 */
 obj_t BgL_envzd2valuezd2_1090;
BgL_envzd2valuezd2_1090 = 
BGl_getenvz00zz__osz00(BGl_string1663z00zz__paramz00); 
if(
STRINGP(BgL_envzd2valuezd2_1090))
{ /* Llib/param.scm 208 */
 obj_t BgL_l1072z00_1092;
BgL_l1072z00_1092 = 
BGl_stringzd2splitzd2zz__r4_strings_6_7z00(BgL_envzd2valuezd2_1090, BNIL); 
if(
NULLP(BgL_l1072z00_1092))
{ /* Llib/param.scm 208 */
BGl_za2bigloozd2traceza2zd2zz__paramz00 = BNIL; }  else 
{ /* Llib/param.scm 208 */
 obj_t BgL_head1074z00_1094;
{ /* Llib/param.scm 208 */
 obj_t BgL_arg1187z00_1107;
{ /* Llib/param.scm 208 */
 obj_t BgL_arg1188z00_1108;
BgL_arg1188z00_1108 = 
CAR(BgL_l1072z00_1092); 
{ /* Llib/param.scm 208 */
 obj_t BgL_stringz00_1589;
if(
STRINGP(BgL_arg1188z00_1108))
{ /* Llib/param.scm 208 */
BgL_stringz00_1589 = BgL_arg1188z00_1108; }  else 
{ 
 obj_t BgL_auxz00_1935;
BgL_auxz00_1935 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(7799L), BGl_string1665z00zz__paramz00, BGl_string1666z00zz__paramz00, BgL_arg1188z00_1108); 
FAILURE(BgL_auxz00_1935,BFALSE,BFALSE);} 
BgL_arg1187z00_1107 = 
bstring_to_symbol(BgL_stringz00_1589); } } 
BgL_head1074z00_1094 = 
MAKE_YOUNG_PAIR(BgL_arg1187z00_1107, BNIL); } 
{ /* Llib/param.scm 208 */
 obj_t BgL_g1077z00_1095;
BgL_g1077z00_1095 = 
CDR(BgL_l1072z00_1092); 
{ 
 obj_t BgL_l1072z00_1097; obj_t BgL_tail1075z00_1098;
BgL_l1072z00_1097 = BgL_g1077z00_1095; 
BgL_tail1075z00_1098 = BgL_head1074z00_1094; 
BgL_zc3z04anonymousza31167ze3z87_1099:
if(
PAIRP(BgL_l1072z00_1097))
{ /* Llib/param.scm 208 */
 obj_t BgL_newtail1076z00_1101;
{ /* Llib/param.scm 208 */
 obj_t BgL_arg1172z00_1103;
{ /* Llib/param.scm 208 */
 obj_t BgL_arg1182z00_1104;
BgL_arg1182z00_1104 = 
CAR(BgL_l1072z00_1097); 
{ /* Llib/param.scm 208 */
 obj_t BgL_stringz00_1592;
if(
STRINGP(BgL_arg1182z00_1104))
{ /* Llib/param.scm 208 */
BgL_stringz00_1592 = BgL_arg1182z00_1104; }  else 
{ 
 obj_t BgL_auxz00_1947;
BgL_auxz00_1947 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(7799L), BGl_string1667z00zz__paramz00, BGl_string1666z00zz__paramz00, BgL_arg1182z00_1104); 
FAILURE(BgL_auxz00_1947,BFALSE,BFALSE);} 
BgL_arg1172z00_1103 = 
bstring_to_symbol(BgL_stringz00_1592); } } 
BgL_newtail1076z00_1101 = 
MAKE_YOUNG_PAIR(BgL_arg1172z00_1103, BNIL); } 
SET_CDR(BgL_tail1075z00_1098, BgL_newtail1076z00_1101); 
{ 
 obj_t BgL_tail1075z00_1956; obj_t BgL_l1072z00_1954;
BgL_l1072z00_1954 = 
CDR(BgL_l1072z00_1097); 
BgL_tail1075z00_1956 = BgL_newtail1076z00_1101; 
BgL_tail1075z00_1098 = BgL_tail1075z00_1956; 
BgL_l1072z00_1097 = BgL_l1072z00_1954; 
goto BgL_zc3z04anonymousza31167ze3z87_1099;} }  else 
{ /* Llib/param.scm 208 */
if(
NULLP(BgL_l1072z00_1097))
{ /* Llib/param.scm 208 */
BGl_za2bigloozd2traceza2zd2zz__paramz00 = BgL_head1074z00_1094; }  else 
{ /* Llib/param.scm 208 */
BGl_za2bigloozd2traceza2zd2zz__paramz00 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1668z00zz__paramz00, BGl_string1669z00zz__paramz00, BgL_l1072z00_1097, BGl_string1664z00zz__paramz00, 
BINT(7799L)); } } } } } }  else 
{ /* Llib/param.scm 207 */
BGl_za2bigloozd2traceza2zd2zz__paramz00 = BNIL; } } 
{ /* Llib/param.scm 217 */
 obj_t BgL_envzd2valuezd2_1110;
BgL_envzd2valuezd2_1110 = 
BGl_getenvz00zz__osz00(BGl_string1670z00zz__paramz00); 
if(
STRINGP(BgL_envzd2valuezd2_1110))
{ /* Llib/param.scm 219 */
 char * BgL_tmpz00_1964;
BgL_tmpz00_1964 = 
BSTRING_TO_STRING(BgL_envzd2valuezd2_1110); 
BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 = 
BGL_STRTOL(BgL_tmpz00_1964, 0L, 10L); }  else 
{ /* Llib/param.scm 218 */
BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 = 10L; } } 
BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 = BGl_symbol1671z00zz__paramz00; 
BGl_za2bigloozd2initializa7edpza2z75zz__paramz00 = ((bool_t)0); 
BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00 = BFALSE; 
BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00 = BFALSE; 
BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00 = BFALSE; 
BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00 = ((bool_t)1); 
{ /* Llib/param.scm 281 */
 obj_t BgL_arg1191z00_1112;
BgL_arg1191z00_1112 = 
BGl_bigloozd2configzd2zz__configurez00(BGl_symbol1673z00zz__paramz00); 
{ /* Llib/param.scm 281 */
 obj_t BgL_list1192z00_1113;
{ /* Llib/param.scm 281 */
 obj_t BgL_arg1193z00_1114;
BgL_arg1193z00_1114 = 
MAKE_YOUNG_PAIR(BgL_arg1191z00_1112, BNIL); 
BgL_list1192z00_1113 = 
MAKE_YOUNG_PAIR(BGl_string1675z00zz__paramz00, BgL_arg1193z00_1114); } 
BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 = BgL_list1192z00_1113; } } 
BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00 = ((bool_t)1); 
return ( 
BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00 = 20L, BUNSPEC) ;} 

}



/* bigloo-strict-r5rs-strings */
BGL_EXPORTED_DEF bool_t BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00(void)
{
{ /* Llib/param.scm 135 */
return BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00;} 

}



/* &bigloo-strict-r5rs-strings */
obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszb0zz__paramz00(obj_t BgL_envz00_1796)
{
{ /* Llib/param.scm 135 */
return 
BBOOL(
BGl_bigloozd2strictzd2r5rszd2stringszd2zz__paramz00());} 

}



/* bigloo-strict-r5rs-strings-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2strictzd2r5rszd2stringszd2setz12z12zz__paramz00(bool_t BgL_vz00_3)
{
{ /* Llib/param.scm 135 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 135 */
 obj_t BgL_tmp1823z00_1972;
BgL_tmp1823z00_1972 = ( 
BGl_za2bigloozd2strictzd2r5rszd2stringsza2zd2zz__paramz00 = BgL_vz00_3, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1823z00_1972; } 
return 
BBOOL(BgL_vz00_3);} 

}



/* &bigloo-strict-r5rs-strings-set! */
obj_t BGl_z62bigloozd2strictzd2r5rszd2stringszd2setz12z70zz__paramz00(obj_t BgL_envz00_1797, obj_t BgL_vz00_1798)
{
{ /* Llib/param.scm 135 */
return 
BGl_bigloozd2strictzd2r5rszd2stringszd2setz12z12zz__paramz00(
CBOOL(BgL_vz00_1798));} 

}



/* bigloo-compiler-debug */
BGL_EXPORTED_DEF int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void)
{
{ /* Llib/param.scm 142 */
{ /* Llib/param.scm 142 */
 obj_t BgL_tmpz00_1978;
{ /* Llib/param.scm 142 */
 obj_t BgL_aux1630z00_1853;
BgL_aux1630z00_1853 = BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00; 
if(
INTEGERP(BgL_aux1630z00_1853))
{ /* Llib/param.scm 142 */
BgL_tmpz00_1978 = BgL_aux1630z00_1853
; }  else 
{ 
 obj_t BgL_auxz00_1981;
BgL_auxz00_1981 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(4860L), BGl_string1676z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_aux1630z00_1853); 
FAILURE(BgL_auxz00_1981,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_1978);} } 

}



/* &bigloo-compiler-debug */
obj_t BGl_z62bigloozd2compilerzd2debugz62zz__paramz00(obj_t BgL_envz00_1799)
{
{ /* Llib/param.scm 142 */
return 
BINT(
BGl_bigloozd2compilerzd2debugz00zz__paramz00());} 

}



/* bigloo-compiler-debug-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2compilerzd2debugzd2setz12zc0zz__paramz00(int BgL_vz00_4)
{
{ /* Llib/param.scm 142 */
{ /* Llib/param.scm 142 */
 obj_t BgL_top1826z00_1989;
BgL_top1826z00_1989 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1826z00_1989, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 142 */
 obj_t BgL_tmp1825z00_1988;
if(
(
(long)(BgL_vz00_4)<0L))
{ /* Llib/param.scm 145 */
BgL_tmp1825z00_1988 = ( 
BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1678z00zz__paramz00, BGl_string1680z00zz__paramz00, 
BINT(BgL_vz00_4)), BUNSPEC) ; }  else 
{ /* Llib/param.scm 145 */
BgL_tmp1825z00_1988 = ( 
BGl_za2bigloozd2compilerzd2debugza2z00zz__paramz00 = 
BINT(BgL_vz00_4), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1826z00_1989); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1825z00_1988; } } 
return 
BINT(BgL_vz00_4);} 

}



/* &bigloo-compiler-debug-set! */
obj_t BGl_z62bigloozd2compilerzd2debugzd2setz12za2zz__paramz00(obj_t BgL_envz00_1800, obj_t BgL_vz00_1801)
{
{ /* Llib/param.scm 142 */
{ /* Llib/param.scm 142 */
 int BgL_auxz00_2002;
{ /* Llib/param.scm 142 */
 obj_t BgL_tmpz00_2003;
if(
INTEGERP(BgL_vz00_1801))
{ /* Llib/param.scm 142 */
BgL_tmpz00_2003 = BgL_vz00_1801
; }  else 
{ 
 obj_t BgL_auxz00_2006;
BgL_auxz00_2006 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(4860L), BGl_string1681z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1801); 
FAILURE(BgL_auxz00_2006,BFALSE,BFALSE);} 
BgL_auxz00_2002 = 
CINT(BgL_tmpz00_2003); } 
return 
BGl_bigloozd2compilerzd2debugzd2setz12zc0zz__paramz00(BgL_auxz00_2002);} } 

}



/* bigloo-debug */
BGL_EXPORTED_DEF int bgl_debug(void)
{
{ /* Llib/param.scm 154 */
{ /* Llib/param.scm 154 */
 obj_t BgL_tmpz00_2012;
{ /* Llib/param.scm 154 */
 obj_t BgL_aux1632z00_1855;
BgL_aux1632z00_1855 = BGl_za2bigloozd2debugza2zd2zz__paramz00; 
if(
INTEGERP(BgL_aux1632z00_1855))
{ /* Llib/param.scm 154 */
BgL_tmpz00_2012 = BgL_aux1632z00_1855
; }  else 
{ 
 obj_t BgL_auxz00_2015;
BgL_auxz00_2015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(5413L), BGl_string1682z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_aux1632z00_1855); 
FAILURE(BgL_auxz00_2015,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_2012);} } 

}



/* &bigloo-debug */
obj_t BGl_z62bigloozd2debugzb0zz__paramz00(obj_t BgL_envz00_1802)
{
{ /* Llib/param.scm 154 */
return 
BINT(
bgl_debug());} 

}



/* bigloo-debug-set! */
BGL_EXPORTED_DEF obj_t bgl_debug_set(int BgL_vz00_5)
{
{ /* Llib/param.scm 154 */
{ /* Llib/param.scm 154 */
 obj_t BgL_top1831z00_2023;
BgL_top1831z00_2023 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1831z00_2023, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 154 */
 obj_t BgL_tmp1830z00_2022;
if(
(
(long)(BgL_vz00_5)<0L))
{ /* Llib/param.scm 157 */
BgL_tmp1830z00_2022 = ( 
BGl_za2bigloozd2debugza2zd2zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1683z00zz__paramz00, BGl_string1680z00zz__paramz00, 
BINT(BgL_vz00_5)), BUNSPEC) ; }  else 
{ /* Llib/param.scm 157 */
BgL_tmp1830z00_2022 = ( 
BGl_za2bigloozd2debugza2zd2zz__paramz00 = 
BINT(BgL_vz00_5), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1831z00_2023); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1830z00_2022; } } 
return 
BINT(BgL_vz00_5);} 

}



/* &bigloo-debug-set! */
obj_t BGl_z62bigloozd2debugzd2setz12z70zz__paramz00(obj_t BgL_envz00_1803, obj_t BgL_vz00_1804)
{
{ /* Llib/param.scm 154 */
{ /* Llib/param.scm 154 */
 int BgL_auxz00_2036;
{ /* Llib/param.scm 154 */
 obj_t BgL_tmpz00_2037;
if(
INTEGERP(BgL_vz00_1804))
{ /* Llib/param.scm 154 */
BgL_tmpz00_2037 = BgL_vz00_1804
; }  else 
{ 
 obj_t BgL_auxz00_2040;
BgL_auxz00_2040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(5413L), BGl_string1685z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1804); 
FAILURE(BgL_auxz00_2040,BFALSE,BFALSE);} 
BgL_auxz00_2036 = 
CINT(BgL_tmpz00_2037); } 
return 
bgl_debug_set(BgL_auxz00_2036);} } 

}



/* bigloo-debug-module */
BGL_EXPORTED_DEF int BGl_bigloozd2debugzd2modulez00zz__paramz00(void)
{
{ /* Llib/param.scm 166 */
{ /* Llib/param.scm 166 */
 obj_t BgL_tmpz00_2046;
{ /* Llib/param.scm 166 */
 obj_t BgL_aux1634z00_1857;
BgL_aux1634z00_1857 = BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00; 
if(
INTEGERP(BgL_aux1634z00_1857))
{ /* Llib/param.scm 166 */
BgL_tmpz00_2046 = BgL_aux1634z00_1857
; }  else 
{ 
 obj_t BgL_auxz00_2049;
BgL_auxz00_2049 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(5945L), BGl_string1686z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_aux1634z00_1857); 
FAILURE(BgL_auxz00_2049,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_2046);} } 

}



/* &bigloo-debug-module */
obj_t BGl_z62bigloozd2debugzd2modulez62zz__paramz00(obj_t BgL_envz00_1805)
{
{ /* Llib/param.scm 166 */
return 
BINT(
BGl_bigloozd2debugzd2modulez00zz__paramz00());} 

}



/* bigloo-debug-module-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2debugzd2modulezd2setz12zc0zz__paramz00(int BgL_vz00_6)
{
{ /* Llib/param.scm 166 */
{ /* Llib/param.scm 166 */
 obj_t BgL_top1836z00_2057;
BgL_top1836z00_2057 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1836z00_2057, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 166 */
 obj_t BgL_tmp1835z00_2056;
if(
(
(long)(BgL_vz00_6)<0L))
{ /* Llib/param.scm 169 */
BgL_tmp1835z00_2056 = ( 
BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1687z00zz__paramz00, BGl_string1689z00zz__paramz00, 
BINT(BgL_vz00_6)), BUNSPEC) ; }  else 
{ /* Llib/param.scm 169 */
BgL_tmp1835z00_2056 = ( 
BGl_za2bigloozd2debugzd2moduleza2z00zz__paramz00 = 
BINT(BgL_vz00_6), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1836z00_2057); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1835z00_2056; } } 
return 
BINT(BgL_vz00_6);} 

}



/* &bigloo-debug-module-set! */
obj_t BGl_z62bigloozd2debugzd2modulezd2setz12za2zz__paramz00(obj_t BgL_envz00_1806, obj_t BgL_vz00_1807)
{
{ /* Llib/param.scm 166 */
{ /* Llib/param.scm 166 */
 int BgL_auxz00_2070;
{ /* Llib/param.scm 166 */
 obj_t BgL_tmpz00_2071;
if(
INTEGERP(BgL_vz00_1807))
{ /* Llib/param.scm 166 */
BgL_tmpz00_2071 = BgL_vz00_1807
; }  else 
{ 
 obj_t BgL_auxz00_2074;
BgL_auxz00_2074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(5945L), BGl_string1690z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1807); 
FAILURE(BgL_auxz00_2074,BFALSE,BFALSE);} 
BgL_auxz00_2070 = 
CINT(BgL_tmpz00_2071); } 
return 
BGl_bigloozd2debugzd2modulezd2setz12zc0zz__paramz00(BgL_auxz00_2070);} } 

}



/* bigloo-warning */
BGL_EXPORTED_DEF int BGl_bigloozd2warningzd2zz__paramz00(void)
{
{ /* Llib/param.scm 178 */
{ /* Llib/param.scm 178 */
 obj_t BgL_tmpz00_2080;
{ /* Llib/param.scm 178 */
 obj_t BgL_aux1636z00_1859;
BgL_aux1636z00_1859 = BGl_za2bigloozd2warningza2zd2zz__paramz00; 
if(
INTEGERP(BgL_aux1636z00_1859))
{ /* Llib/param.scm 178 */
BgL_tmpz00_2080 = BgL_aux1636z00_1859
; }  else 
{ 
 obj_t BgL_auxz00_2083;
BgL_auxz00_2083 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(6498L), BGl_string1691z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_aux1636z00_1859); 
FAILURE(BgL_auxz00_2083,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_2080);} } 

}



/* &bigloo-warning */
obj_t BGl_z62bigloozd2warningzb0zz__paramz00(obj_t BgL_envz00_1808)
{
{ /* Llib/param.scm 178 */
return 
BINT(
BGl_bigloozd2warningzd2zz__paramz00());} 

}



/* bigloo-warning-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int BgL_vz00_7)
{
{ /* Llib/param.scm 178 */
{ /* Llib/param.scm 178 */
 obj_t BgL_top1841z00_2091;
BgL_top1841z00_2091 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1841z00_2091, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 178 */
 obj_t BgL_tmp1840z00_2090;
if(
(
(long)(BgL_vz00_7)<0L))
{ /* Llib/param.scm 181 */
BgL_tmp1840z00_2090 = ( 
BGl_za2bigloozd2warningza2zd2zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1692z00zz__paramz00, BGl_string1694z00zz__paramz00, 
BINT(BgL_vz00_7)), BUNSPEC) ; }  else 
{ /* Llib/param.scm 181 */
BgL_tmp1840z00_2090 = ( 
BGl_za2bigloozd2warningza2zd2zz__paramz00 = 
BINT(BgL_vz00_7), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1841z00_2091); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1840z00_2090; } } 
return 
BINT(BgL_vz00_7);} 

}



/* &bigloo-warning-set! */
obj_t BGl_z62bigloozd2warningzd2setz12z70zz__paramz00(obj_t BgL_envz00_1809, obj_t BgL_vz00_1810)
{
{ /* Llib/param.scm 178 */
{ /* Llib/param.scm 178 */
 int BgL_auxz00_2104;
{ /* Llib/param.scm 178 */
 obj_t BgL_tmpz00_2105;
if(
INTEGERP(BgL_vz00_1810))
{ /* Llib/param.scm 178 */
BgL_tmpz00_2105 = BgL_vz00_1810
; }  else 
{ 
 obj_t BgL_auxz00_2108;
BgL_auxz00_2108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(6498L), BGl_string1695z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1810); 
FAILURE(BgL_auxz00_2108,BFALSE,BFALSE);} 
BgL_auxz00_2104 = 
CINT(BgL_tmpz00_2105); } 
return 
BGl_bigloozd2warningzd2setz12z12zz__paramz00(BgL_auxz00_2104);} } 

}



/* bigloo-profile */
BGL_EXPORTED_DEF int BGl_bigloozd2profilezd2zz__paramz00(void)
{
{ /* Llib/param.scm 190 */
{ /* Llib/param.scm 190 */
 obj_t BgL_tmpz00_2114;
{ /* Llib/param.scm 190 */
 obj_t BgL_aux1638z00_1861;
BgL_aux1638z00_1861 = BGl_za2bigloozd2profileza2zd2zz__paramz00; 
if(
INTEGERP(BgL_aux1638z00_1861))
{ /* Llib/param.scm 190 */
BgL_tmpz00_2114 = BgL_aux1638z00_1861
; }  else 
{ 
 obj_t BgL_auxz00_2117;
BgL_auxz00_2117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(7036L), BGl_string1696z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_aux1638z00_1861); 
FAILURE(BgL_auxz00_2117,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_2114);} } 

}



/* &bigloo-profile */
obj_t BGl_z62bigloozd2profilezb0zz__paramz00(obj_t BgL_envz00_1811)
{
{ /* Llib/param.scm 190 */
return 
BINT(
BGl_bigloozd2profilezd2zz__paramz00());} 

}



/* bigloo-profile-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2profilezd2setz12z12zz__paramz00(int BgL_vz00_8)
{
{ /* Llib/param.scm 190 */
{ /* Llib/param.scm 190 */
 obj_t BgL_top1846z00_2125;
BgL_top1846z00_2125 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1846z00_2125, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 190 */
 obj_t BgL_tmp1845z00_2124;
if(
(
(long)(BgL_vz00_8)<0L))
{ /* Llib/param.scm 193 */
BgL_tmp1845z00_2124 = ( 
BGl_za2bigloozd2profileza2zd2zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1697z00zz__paramz00, BGl_string1699z00zz__paramz00, 
BINT(BgL_vz00_8)), BUNSPEC) ; }  else 
{ /* Llib/param.scm 193 */
BgL_tmp1845z00_2124 = ( 
BGl_za2bigloozd2profileza2zd2zz__paramz00 = 
BINT(BgL_vz00_8), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1846z00_2125); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1845z00_2124; } } 
return 
BINT(BgL_vz00_8);} 

}



/* &bigloo-profile-set! */
obj_t BGl_z62bigloozd2profilezd2setz12z70zz__paramz00(obj_t BgL_envz00_1812, obj_t BgL_vz00_1813)
{
{ /* Llib/param.scm 190 */
{ /* Llib/param.scm 190 */
 int BgL_auxz00_2138;
{ /* Llib/param.scm 190 */
 obj_t BgL_tmpz00_2139;
if(
INTEGERP(BgL_vz00_1813))
{ /* Llib/param.scm 190 */
BgL_tmpz00_2139 = BgL_vz00_1813
; }  else 
{ 
 obj_t BgL_auxz00_2142;
BgL_auxz00_2142 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(7036L), BGl_string1700z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1813); 
FAILURE(BgL_auxz00_2142,BFALSE,BFALSE);} 
BgL_auxz00_2138 = 
CINT(BgL_tmpz00_2139); } 
return 
BGl_bigloozd2profilezd2setz12z12zz__paramz00(BgL_auxz00_2138);} } 

}



/* bigloo-trace-color */
BGL_EXPORTED_DEF bool_t BGl_bigloozd2tracezd2colorz00zz__paramz00(void)
{
{ /* Llib/param.scm 200 */
return BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00;} 

}



/* &bigloo-trace-color */
obj_t BGl_z62bigloozd2tracezd2colorz62zz__paramz00(obj_t BgL_envz00_1814)
{
{ /* Llib/param.scm 200 */
return 
BBOOL(
BGl_bigloozd2tracezd2colorz00zz__paramz00());} 

}



/* bigloo-trace-color-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2tracezd2colorzd2setz12zc0zz__paramz00(bool_t BgL_vz00_9)
{
{ /* Llib/param.scm 200 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 200 */
 obj_t BgL_tmp1849z00_2150;
BgL_tmp1849z00_2150 = ( 
BGl_za2bigloozd2tracezd2colorza2z00zz__paramz00 = BgL_vz00_9, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1849z00_2150; } 
return 
BBOOL(BgL_vz00_9);} 

}



/* &bigloo-trace-color-set! */
obj_t BGl_z62bigloozd2tracezd2colorzd2setz12za2zz__paramz00(obj_t BgL_envz00_1815, obj_t BgL_vz00_1816)
{
{ /* Llib/param.scm 200 */
return 
BGl_bigloozd2tracezd2colorzd2setz12zc0zz__paramz00(
CBOOL(BgL_vz00_1816));} 

}



/* bigloo-trace */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2tracezd2zz__paramz00(void)
{
{ /* Llib/param.scm 205 */
{ /* Llib/param.scm 205 */
 obj_t BgL_aux1640z00_1863;
BgL_aux1640z00_1863 = BGl_za2bigloozd2traceza2zd2zz__paramz00; 
{ /* Llib/param.scm 205 */
 bool_t BgL_test1850z00_2156;
if(
PAIRP(BgL_aux1640z00_1863))
{ /* Llib/param.scm 205 */
BgL_test1850z00_2156 = ((bool_t)1)
; }  else 
{ /* Llib/param.scm 205 */
BgL_test1850z00_2156 = 
NULLP(BgL_aux1640z00_1863)
; } 
if(BgL_test1850z00_2156)
{ /* Llib/param.scm 205 */
return BgL_aux1640z00_1863;}  else 
{ 
 obj_t BgL_auxz00_2160;
BgL_auxz00_2160 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(7690L), BGl_string1701z00zz__paramz00, BGl_string1702z00zz__paramz00, BgL_aux1640z00_1863); 
FAILURE(BgL_auxz00_2160,BFALSE,BFALSE);} } } } 

}



/* &bigloo-trace */
obj_t BGl_z62bigloozd2tracezb0zz__paramz00(obj_t BgL_envz00_1817)
{
{ /* Llib/param.scm 205 */
return 
BGl_bigloozd2tracezd2zz__paramz00();} 

}



/* bigloo-trace-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2tracezd2setz12z12zz__paramz00(obj_t BgL_vz00_10)
{
{ /* Llib/param.scm 205 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 205 */
 obj_t BgL_tmp1852z00_2165;
BgL_tmp1852z00_2165 = ( 
BGl_za2bigloozd2traceza2zd2zz__paramz00 = BgL_vz00_10, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1852z00_2165; } 
return BgL_vz00_10;} 

}



/* &bigloo-trace-set! */
obj_t BGl_z62bigloozd2tracezd2setz12z70zz__paramz00(obj_t BgL_envz00_1818, obj_t BgL_vz00_1819)
{
{ /* Llib/param.scm 205 */
{ /* Llib/param.scm 205 */
 obj_t BgL_auxz00_2168;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_vz00_1819))
{ /* Llib/param.scm 205 */
BgL_auxz00_2168 = BgL_vz00_1819
; }  else 
{ 
 obj_t BgL_auxz00_2171;
BgL_auxz00_2171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(7690L), BGl_string1703z00zz__paramz00, BGl_string1702z00zz__paramz00, BgL_vz00_1819); 
FAILURE(BgL_auxz00_2171,BFALSE,BFALSE);} 
return 
BGl_bigloozd2tracezd2setz12z12zz__paramz00(BgL_auxz00_2168);} } 

}



/* bigloo-trace-stack-depth */
BGL_EXPORTED_DEF int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void)
{
{ /* Llib/param.scm 216 */
return 
(int)(BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00);} 

}



/* &bigloo-trace-stack-depth */
obj_t BGl_z62bigloozd2tracezd2stackzd2depthzb0zz__paramz00(obj_t BgL_envz00_1820)
{
{ /* Llib/param.scm 216 */
return 
BINT(
BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00());} 

}



/* bigloo-trace-stack-depth-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(int BgL_vz00_11)
{
{ /* Llib/param.scm 216 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 216 */
 obj_t BgL_tmp1854z00_2179;
BgL_tmp1854z00_2179 = ( 
BGl_za2bigloozd2tracezd2stackzd2depthza2zd2zz__paramz00 = 
(long)(BgL_vz00_11), BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1854z00_2179; } 
return 
BINT(BgL_vz00_11);} 

}



/* &bigloo-trace-stack-depth-set! */
obj_t BGl_z62bigloozd2tracezd2stackzd2depthzd2setz12z70zz__paramz00(obj_t BgL_envz00_1821, obj_t BgL_vz00_1822)
{
{ /* Llib/param.scm 216 */
{ /* Llib/param.scm 216 */
 int BgL_auxz00_2184;
{ /* Llib/param.scm 216 */
 obj_t BgL_tmpz00_2185;
if(
INTEGERP(BgL_vz00_1822))
{ /* Llib/param.scm 216 */
BgL_tmpz00_2185 = BgL_vz00_1822
; }  else 
{ 
 obj_t BgL_auxz00_2188;
BgL_auxz00_2188 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(8226L), BGl_string1704z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1822); 
FAILURE(BgL_auxz00_2188,BFALSE,BFALSE);} 
BgL_auxz00_2184 = 
CINT(BgL_tmpz00_2185); } 
return 
BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(BgL_auxz00_2184);} } 

}



/* bigloo-case-sensitive */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2casezd2sensitivez00zz__paramz00(void)
{
{ /* Llib/param.scm 231 */
{ /* Llib/param.scm 231 */
 obj_t BgL_aux1645z00_1868;
BgL_aux1645z00_1868 = BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00; 
if(
SYMBOLP(BgL_aux1645z00_1868))
{ /* Llib/param.scm 231 */
return BgL_aux1645z00_1868;}  else 
{ 
 obj_t BgL_auxz00_2196;
BgL_auxz00_2196 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(9056L), BGl_string1705z00zz__paramz00, BGl_string1706z00zz__paramz00, BgL_aux1645z00_1868); 
FAILURE(BgL_auxz00_2196,BFALSE,BFALSE);} } } 

}



/* &bigloo-case-sensitive */
obj_t BGl_z62bigloozd2casezd2sensitivez62zz__paramz00(obj_t BgL_envz00_1823)
{
{ /* Llib/param.scm 231 */
return 
BGl_bigloozd2casezd2sensitivez00zz__paramz00();} 

}



/* bigloo-case-sensitive-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2casezd2sensitivezd2setz12zc0zz__paramz00(obj_t BgL_vz00_12)
{
{ /* Llib/param.scm 231 */
{ /* Llib/param.scm 231 */
 obj_t BgL_top1858z00_2202;
BgL_top1858z00_2202 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1858z00_2202, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 231 */
 obj_t BgL_tmp1857z00_2201;
if(
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_vz00_12, BGl_list1707z00zz__paramz00)))
{ /* Llib/param.scm 235 */
BgL_tmp1857z00_2201 = ( 
BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 = BgL_vz00_12, BUNSPEC) ; }  else 
{ /* Llib/param.scm 235 */
BgL_tmp1857z00_2201 = ( 
BGl_za2bigloozd2casezd2sensitiveza2z00zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_string1712z00zz__paramz00, BGl_string1713z00zz__paramz00, BgL_vz00_12), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1858z00_2202); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1857z00_2201; } } 
return BgL_vz00_12;} 

}



/* &bigloo-case-sensitive-set! */
obj_t BGl_z62bigloozd2casezd2sensitivezd2setz12za2zz__paramz00(obj_t BgL_envz00_1824, obj_t BgL_vz00_1825)
{
{ /* Llib/param.scm 231 */
{ /* Llib/param.scm 231 */
 obj_t BgL_auxz00_2212;
if(
SYMBOLP(BgL_vz00_1825))
{ /* Llib/param.scm 231 */
BgL_auxz00_2212 = BgL_vz00_1825
; }  else 
{ 
 obj_t BgL_auxz00_2215;
BgL_auxz00_2215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(9056L), BGl_string1714z00zz__paramz00, BGl_string1706z00zz__paramz00, BgL_vz00_1825); 
FAILURE(BgL_auxz00_2215,BFALSE,BFALSE);} 
return 
BGl_bigloozd2casezd2sensitivezd2setz12zc0zz__paramz00(BgL_auxz00_2212);} } 

}



/* bigloo-initialized! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2initializa7edz12z67zz__paramz00(void)
{
{ /* Llib/param.scm 248 */
return ( 
BGl_za2bigloozd2initializa7edpza2z75zz__paramz00 = ((bool_t)1), BUNSPEC) ;} 

}



/* &bigloo-initialized! */
obj_t BGl_z62bigloozd2initializa7edz12z05zz__paramz00(obj_t BgL_envz00_1826)
{
{ /* Llib/param.scm 248 */
return 
BGl_bigloozd2initializa7edz12z67zz__paramz00();} 

}



/* bigloo-initialized? */
BGL_EXPORTED_DEF bool_t BGl_bigloozd2initializa7edzf3z86zz__paramz00(void)
{
{ /* Llib/param.scm 254 */
return BGl_za2bigloozd2initializa7edpza2z75zz__paramz00;} 

}



/* &bigloo-initialized? */
obj_t BGl_z62bigloozd2initializa7edzf3ze4zz__paramz00(obj_t BgL_envz00_1827)
{
{ /* Llib/param.scm 254 */
return 
BBOOL(
BGl_bigloozd2initializa7edzf3z86zz__paramz00());} 

}



/* bigloo-load-reader */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2loadzd2readerz00zz__paramz00(void)
{
{ /* Llib/param.scm 260 */
return BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00;} 

}



/* &bigloo-load-reader */
obj_t BGl_z62bigloozd2loadzd2readerz62zz__paramz00(obj_t BgL_envz00_1828)
{
{ /* Llib/param.scm 260 */
return 
BGl_bigloozd2loadzd2readerz00zz__paramz00();} 

}



/* bigloo-load-reader-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2loadzd2readerzd2setz12zc0zz__paramz00(obj_t BgL_vz00_13)
{
{ /* Llib/param.scm 260 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 260 */
 obj_t BgL_tmp1861z00_2224;
BgL_tmp1861z00_2224 = ( 
BGl_za2bigloozd2loadzd2readerza2z00zz__paramz00 = BgL_vz00_13, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1861z00_2224; } 
return BgL_vz00_13;} 

}



/* &bigloo-load-reader-set! */
obj_t BGl_z62bigloozd2loadzd2readerzd2setz12za2zz__paramz00(obj_t BgL_envz00_1829, obj_t BgL_vz00_1830)
{
{ /* Llib/param.scm 260 */
return 
BGl_bigloozd2loadzd2readerzd2setz12zc0zz__paramz00(BgL_vz00_1830);} 

}



/* bigloo-load-module */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2loadzd2modulez00zz__paramz00(void)
{
{ /* Llib/param.scm 265 */
return BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00;} 

}



/* &bigloo-load-module */
obj_t BGl_z62bigloozd2loadzd2modulez62zz__paramz00(obj_t BgL_envz00_1831)
{
{ /* Llib/param.scm 265 */
return 
BGl_bigloozd2loadzd2modulez00zz__paramz00();} 

}



/* bigloo-load-module-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2loadzd2modulezd2setz12zc0zz__paramz00(obj_t BgL_vz00_14)
{
{ /* Llib/param.scm 265 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 265 */
 obj_t BgL_tmp1862z00_2229;
BgL_tmp1862z00_2229 = ( 
BGl_za2bigloozd2loadzd2moduleza2z00zz__paramz00 = BgL_vz00_14, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1862z00_2229; } 
return BgL_vz00_14;} 

}



/* &bigloo-load-module-set! */
obj_t BGl_z62bigloozd2loadzd2modulezd2setz12za2zz__paramz00(obj_t BgL_envz00_1832, obj_t BgL_vz00_1833)
{
{ /* Llib/param.scm 265 */
return 
BGl_bigloozd2loadzd2modulezd2setz12zc0zz__paramz00(BgL_vz00_1833);} 

}



/* bigloo-module-extension-handler */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00(void)
{
{ /* Llib/param.scm 270 */
return BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00;} 

}



/* &bigloo-module-extension-handler */
obj_t BGl_z62bigloozd2modulezd2extensionzd2handlerzb0zz__paramz00(obj_t BgL_envz00_1834)
{
{ /* Llib/param.scm 270 */
return 
BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00();} 

}



/* bigloo-module-extension-handler-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12z12zz__paramz00(obj_t BgL_vz00_15)
{
{ /* Llib/param.scm 270 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 270 */
 obj_t BgL_tmp1863z00_2234;
BgL_tmp1863z00_2234 = ( 
BGl_za2bigloozd2modulezd2extensionzd2handlerza2zd2zz__paramz00 = BgL_vz00_15, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1863z00_2234; } 
return BgL_vz00_15;} 

}



/* &bigloo-module-extension-handler-set! */
obj_t BGl_z62bigloozd2modulezd2extensionzd2handlerzd2setz12z70zz__paramz00(obj_t BgL_envz00_1835, obj_t BgL_vz00_1836)
{
{ /* Llib/param.scm 270 */
return 
BGl_bigloozd2modulezd2extensionzd2handlerzd2setz12z12zz__paramz00(BgL_vz00_1836);} 

}



/* bigloo-eval-strict-module */
BGL_EXPORTED_DEF bool_t BGl_bigloozd2evalzd2strictzd2modulezd2zz__paramz00(void)
{
{ /* Llib/param.scm 275 */
return BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00;} 

}



/* &bigloo-eval-strict-module */
obj_t BGl_z62bigloozd2evalzd2strictzd2modulezb0zz__paramz00(obj_t BgL_envz00_1837)
{
{ /* Llib/param.scm 275 */
return 
BBOOL(
BGl_bigloozd2evalzd2strictzd2modulezd2zz__paramz00());} 

}



/* bigloo-eval-strict-module-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2evalzd2strictzd2modulezd2setz12z12zz__paramz00(bool_t BgL_vz00_16)
{
{ /* Llib/param.scm 275 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 275 */
 obj_t BgL_tmp1864z00_2240;
BgL_tmp1864z00_2240 = ( 
BGl_za2bigloozd2evalzd2strictzd2moduleza2zd2zz__paramz00 = BgL_vz00_16, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1864z00_2240; } 
return 
BBOOL(BgL_vz00_16);} 

}



/* &bigloo-eval-strict-module-set! */
obj_t BGl_z62bigloozd2evalzd2strictzd2modulezd2setz12z70zz__paramz00(obj_t BgL_envz00_1838, obj_t BgL_vz00_1839)
{
{ /* Llib/param.scm 275 */
return 
BGl_bigloozd2evalzd2strictzd2modulezd2setz12z12zz__paramz00(
CBOOL(BgL_vz00_1839));} 

}



/* bigloo-library-path */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2libraryzd2pathz00zz__paramz00(void)
{
{ /* Llib/param.scm 280 */
{ /* Llib/param.scm 280 */
 obj_t BgL_aux1649z00_1872;
BgL_aux1649z00_1872 = BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00; 
{ /* Llib/param.scm 280 */
 bool_t BgL_test1865z00_2246;
if(
PAIRP(BgL_aux1649z00_1872))
{ /* Llib/param.scm 280 */
BgL_test1865z00_2246 = ((bool_t)1)
; }  else 
{ /* Llib/param.scm 280 */
BgL_test1865z00_2246 = 
NULLP(BgL_aux1649z00_1872)
; } 
if(BgL_test1865z00_2246)
{ /* Llib/param.scm 280 */
return BgL_aux1649z00_1872;}  else 
{ 
 obj_t BgL_auxz00_2250;
BgL_auxz00_2250 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(11394L), BGl_string1715z00zz__paramz00, BGl_string1702z00zz__paramz00, BgL_aux1649z00_1872); 
FAILURE(BgL_auxz00_2250,BFALSE,BFALSE);} } } } 

}



/* &bigloo-library-path */
obj_t BGl_z62bigloozd2libraryzd2pathz62zz__paramz00(obj_t BgL_envz00_1840)
{
{ /* Llib/param.scm 280 */
return 
BGl_bigloozd2libraryzd2pathz00zz__paramz00();} 

}



/* bigloo-library-path-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2libraryzd2pathzd2setz12zc0zz__paramz00(obj_t BgL_vz00_17)
{
{ /* Llib/param.scm 280 */
{ /* Llib/param.scm 280 */
 obj_t BgL_top1868z00_2256;
BgL_top1868z00_2256 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1868z00_2256, BGl_za2parameterzd2mutexza2zd2zz__paramz00); BUNSPEC; 
{ /* Llib/param.scm 280 */
 obj_t BgL_tmp1867z00_2255;
if(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_vz00_17))
{ /* Llib/param.scm 286 */
 bool_t BgL_test1870z00_2262;
{ 
 obj_t BgL_l1078z00_1176;
{ /* Llib/param.scm 286 */
 obj_t BgL_tmpz00_2263;
BgL_l1078z00_1176 = BgL_vz00_17; 
BgL_zc3z04anonymousza31227ze3z87_1177:
if(
NULLP(BgL_l1078z00_1176))
{ /* Llib/param.scm 286 */
BgL_tmpz00_2263 = BTRUE
; }  else 
{ /* Llib/param.scm 286 */
if(
PAIRP(BgL_l1078z00_1176))
{ /* Llib/param.scm 286 */
 bool_t BgL_test1873z00_2268;
{ /* Llib/param.scm 286 */
 obj_t BgL_tmpz00_2269;
BgL_tmpz00_2269 = 
CAR(BgL_l1078z00_1176); 
BgL_test1873z00_2268 = 
STRINGP(BgL_tmpz00_2269); } 
if(BgL_test1873z00_2268)
{ 
 obj_t BgL_l1078z00_2272;
BgL_l1078z00_2272 = 
CDR(BgL_l1078z00_1176); 
BgL_l1078z00_1176 = BgL_l1078z00_2272; 
goto BgL_zc3z04anonymousza31227ze3z87_1177;}  else 
{ /* Llib/param.scm 286 */
BgL_tmpz00_2263 = BFALSE
; } }  else 
{ /* Llib/param.scm 286 */
BgL_tmpz00_2263 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1716z00zz__paramz00, BGl_string1669z00zz__paramz00, BgL_l1078z00_1176, BGl_string1664z00zz__paramz00, 
BINT(11596L))
; } } 
BgL_test1870z00_2262 = 
CBOOL(BgL_tmpz00_2263); } } 
if(BgL_test1870z00_2262)
{ /* Llib/param.scm 286 */
BgL_tmp1867z00_2255 = ( 
BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 = BgL_vz00_17, BUNSPEC) ; }  else 
{ /* Llib/param.scm 289 */
 obj_t BgL_arg1218z00_1158;
{ /* Llib/param.scm 289 */
 obj_t BgL_hook1085z00_1159;
BgL_hook1085z00_1159 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
{ 
 obj_t BgL_l1082z00_1161; obj_t BgL_h1083z00_1162;
BgL_l1082z00_1161 = BgL_vz00_17; 
BgL_h1083z00_1162 = BgL_hook1085z00_1159; 
BgL_zc3z04anonymousza31219ze3z87_1163:
if(
NULLP(BgL_l1082z00_1161))
{ /* Llib/param.scm 289 */
BgL_arg1218z00_1158 = 
CDR(BgL_hook1085z00_1159); }  else 
{ /* Llib/param.scm 289 */
 bool_t BgL_test1875z00_2281;
{ /* Llib/param.scm 289 */
 bool_t BgL_test1876z00_2282;
{ /* Llib/param.scm 289 */
 obj_t BgL_tmpz00_2283;
{ /* Llib/param.scm 289 */
 obj_t BgL_pairz00_1604;
if(
PAIRP(BgL_l1082z00_1161))
{ /* Llib/param.scm 289 */
BgL_pairz00_1604 = BgL_l1082z00_1161; }  else 
{ 
 obj_t BgL_auxz00_2286;
BgL_auxz00_2286 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(11696L), BGl_string1717z00zz__paramz00, BGl_string1718z00zz__paramz00, BgL_l1082z00_1161); 
FAILURE(BgL_auxz00_2286,BFALSE,BFALSE);} 
BgL_tmpz00_2283 = 
CAR(BgL_pairz00_1604); } 
BgL_test1876z00_2282 = 
STRINGP(BgL_tmpz00_2283); } 
if(BgL_test1876z00_2282)
{ /* Llib/param.scm 289 */
BgL_test1875z00_2281 = ((bool_t)0)
; }  else 
{ /* Llib/param.scm 289 */
BgL_test1875z00_2281 = ((bool_t)1)
; } } 
if(BgL_test1875z00_2281)
{ /* Llib/param.scm 289 */
 obj_t BgL_nh1084z00_1168;
{ /* Llib/param.scm 289 */
 obj_t BgL_arg1225z00_1170;
{ /* Llib/param.scm 289 */
 obj_t BgL_pairz00_1605;
if(
PAIRP(BgL_l1082z00_1161))
{ /* Llib/param.scm 289 */
BgL_pairz00_1605 = BgL_l1082z00_1161; }  else 
{ 
 obj_t BgL_auxz00_2294;
BgL_auxz00_2294 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(11676L), BGl_string1717z00zz__paramz00, BGl_string1718z00zz__paramz00, BgL_l1082z00_1161); 
FAILURE(BgL_auxz00_2294,BFALSE,BFALSE);} 
BgL_arg1225z00_1170 = 
CAR(BgL_pairz00_1605); } 
BgL_nh1084z00_1168 = 
MAKE_YOUNG_PAIR(BgL_arg1225z00_1170, BNIL); } 
SET_CDR(BgL_h1083z00_1162, BgL_nh1084z00_1168); 
{ /* Llib/param.scm 289 */
 obj_t BgL_arg1223z00_1169;
{ /* Llib/param.scm 289 */
 obj_t BgL_pairz00_1607;
if(
PAIRP(BgL_l1082z00_1161))
{ /* Llib/param.scm 289 */
BgL_pairz00_1607 = BgL_l1082z00_1161; }  else 
{ 
 obj_t BgL_auxz00_2303;
BgL_auxz00_2303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(11676L), BGl_string1717z00zz__paramz00, BGl_string1718z00zz__paramz00, BgL_l1082z00_1161); 
FAILURE(BgL_auxz00_2303,BFALSE,BFALSE);} 
BgL_arg1223z00_1169 = 
CDR(BgL_pairz00_1607); } 
{ 
 obj_t BgL_h1083z00_2309; obj_t BgL_l1082z00_2308;
BgL_l1082z00_2308 = BgL_arg1223z00_1169; 
BgL_h1083z00_2309 = BgL_nh1084z00_1168; 
BgL_h1083z00_1162 = BgL_h1083z00_2309; 
BgL_l1082z00_1161 = BgL_l1082z00_2308; 
goto BgL_zc3z04anonymousza31219ze3z87_1163;} } }  else 
{ /* Llib/param.scm 289 */
 obj_t BgL_arg1226z00_1171;
{ /* Llib/param.scm 289 */
 obj_t BgL_pairz00_1608;
if(
PAIRP(BgL_l1082z00_1161))
{ /* Llib/param.scm 289 */
BgL_pairz00_1608 = BgL_l1082z00_1161; }  else 
{ 
 obj_t BgL_auxz00_2312;
BgL_auxz00_2312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(11676L), BGl_string1717z00zz__paramz00, BGl_string1718z00zz__paramz00, BgL_l1082z00_1161); 
FAILURE(BgL_auxz00_2312,BFALSE,BFALSE);} 
BgL_arg1226z00_1171 = 
CDR(BgL_pairz00_1608); } 
{ 
 obj_t BgL_l1082z00_2317;
BgL_l1082z00_2317 = BgL_arg1226z00_1171; 
BgL_l1082z00_1161 = BgL_l1082z00_2317; 
goto BgL_zc3z04anonymousza31219ze3z87_1163;} } } } } 
BgL_tmp1867z00_2255 = ( 
BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1719z00zz__paramz00, BGl_string1721z00zz__paramz00, BgL_arg1218z00_1158), BUNSPEC) ; } }  else 
{ /* Llib/param.scm 284 */
BgL_tmp1867z00_2255 = ( 
BGl_za2bigloozd2libraryzd2pathza2z00zz__paramz00 = 
BGl_errorz00zz__errorz00(BGl_symbol1719z00zz__paramz00, BGl_string1722z00zz__paramz00, BgL_vz00_17), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top1868z00_2256); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1867z00_2255; } } 
return BgL_vz00_17;} 

}



/* &bigloo-library-path-set! */
obj_t BGl_z62bigloozd2libraryzd2pathzd2setz12za2zz__paramz00(obj_t BgL_envz00_1841, obj_t BgL_vz00_1842)
{
{ /* Llib/param.scm 280 */
{ /* Llib/param.scm 280 */
 obj_t BgL_auxz00_2322;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_vz00_1842))
{ /* Llib/param.scm 280 */
BgL_auxz00_2322 = BgL_vz00_1842
; }  else 
{ 
 obj_t BgL_auxz00_2325;
BgL_auxz00_2325 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(11394L), BGl_string1723z00zz__paramz00, BGl_string1702z00zz__paramz00, BgL_vz00_1842); 
FAILURE(BgL_auxz00_2325,BFALSE,BFALSE);} 
return 
BGl_bigloozd2libraryzd2pathzd2setz12zc0zz__paramz00(BgL_auxz00_2322);} } 

}



/* bigloo-dns-enable-cache */
BGL_EXPORTED_DEF bool_t bgl_dns_enable_cache(void)
{
{ /* Llib/param.scm 296 */
return BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00;} 

}



/* &bigloo-dns-enable-cache */
obj_t BGl_z62bigloozd2dnszd2enablezd2cachezb0zz__paramz00(obj_t BgL_envz00_1843)
{
{ /* Llib/param.scm 296 */
return 
BBOOL(
bgl_dns_enable_cache());} 

}



/* bigloo-dns-enable-cache-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2dnszd2enablezd2cachezd2setz12z12zz__paramz00(bool_t BgL_vz00_18)
{
{ /* Llib/param.scm 296 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 296 */
 obj_t BgL_tmp1882z00_2332;
BgL_tmp1882z00_2332 = ( 
BGl_za2bigloozd2dnszd2enablezd2cacheza2zd2zz__paramz00 = BgL_vz00_18, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1882z00_2332; } 
return 
BBOOL(BgL_vz00_18);} 

}



/* &bigloo-dns-enable-cache-set! */
obj_t BGl_z62bigloozd2dnszd2enablezd2cachezd2setz12z70zz__paramz00(obj_t BgL_envz00_1844, obj_t BgL_vz00_1845)
{
{ /* Llib/param.scm 296 */
return 
BGl_bigloozd2dnszd2enablezd2cachezd2setz12z12zz__paramz00(
CBOOL(BgL_vz00_1845));} 

}



/* bigloo-dns-cache-validity-timeout */
BGL_EXPORTED_DEF long bgl_dns_cache_validity_timeout(void)
{
{ /* Llib/param.scm 301 */
return BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00;} 

}



/* &bigloo-dns-cache-validity-timeout */
obj_t BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutz62zz__paramz00(obj_t BgL_envz00_1846)
{
{ /* Llib/param.scm 301 */
return 
BINT(
bgl_dns_cache_validity_timeout());} 

}



/* bigloo-dns-cache-validity-timeout-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zc0zz__paramz00(long BgL_vz00_19)
{
{ /* Llib/param.scm 301 */
BGL_MUTEX_LOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); 
{ /* Llib/param.scm 301 */
 obj_t BgL_tmp1883z00_2340;
BgL_tmp1883z00_2340 = ( 
BGl_za2bigloozd2dnszd2cachezd2validityzd2timeoutza2z00zz__paramz00 = BgL_vz00_19, BUNSPEC) ; 
BGL_MUTEX_UNLOCK(BGl_za2parameterzd2mutexza2zd2zz__paramz00); BgL_tmp1883z00_2340; } 
return 
BINT(BgL_vz00_19);} 

}



/* &bigloo-dns-cache-validity-timeout-set! */
obj_t BGl_z62bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12za2zz__paramz00(obj_t BgL_envz00_1847, obj_t BgL_vz00_1848)
{
{ /* Llib/param.scm 301 */
{ /* Llib/param.scm 301 */
 long BgL_auxz00_2344;
{ /* Llib/param.scm 301 */
 obj_t BgL_tmpz00_2345;
if(
INTEGERP(BgL_vz00_1848))
{ /* Llib/param.scm 301 */
BgL_tmpz00_2345 = BgL_vz00_1848
; }  else 
{ 
 obj_t BgL_auxz00_2348;
BgL_auxz00_2348 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__paramz00, 
BINT(12233L), BGl_string1724z00zz__paramz00, BGl_string1677z00zz__paramz00, BgL_vz00_1848); 
FAILURE(BgL_auxz00_2348,BFALSE,BFALSE);} 
BgL_auxz00_2344 = 
(long)CINT(BgL_tmpz00_2345); } 
return 
BGl_bigloozd2dnszd2cachezd2validityzd2timeoutzd2setz12zc0zz__paramz00(BgL_auxz00_2344);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__paramz00(void)
{
{ /* Llib/param.scm 15 */
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1725z00zz__paramz00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1725z00zz__paramz00)); 
return 
BGl_modulezd2initializa7ationz75zz__configurez00(35034923L, 
BSTRING_TO_STRING(BGl_string1725z00zz__paramz00));} 

}

#ifdef __cplusplus
}
#endif
