/*===========================================================================*/
/*   (Llib/process.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/process.scm -indent -o objs/obj_s/Llib/process.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PROCESS_TYPE_DEFINITIONS
#define BGL___PROCESS_TYPE_DEFINITIONS
#endif // BGL___PROCESS_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern obj_t c_process_xstatus(obj_t);
extern obj_t c_process_list(void);
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__processz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_processzd2errorzd2portz00zz__processz00(obj_t);
static obj_t BGl_z62processzd2pidzb0zz__processz00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__processz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62processzd2killzb0zz__processz00(obj_t, obj_t);
static obj_t BGl_z62runzd2processzb0zz__processz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62processzd2sendzd2signalz62zz__processz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62processzd2waitzb0zz__processz00(obj_t, obj_t);
static obj_t BGl_z62processzd2errorzd2portz62zz__processz00(obj_t, obj_t);
static obj_t BGl_z62processzd2stopzb0zz__processz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2killzd2zz__processz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_processzf3zf3zz__processz00(obj_t);
static obj_t BGl_list1697z00zz__processz00 = BUNSPEC;
static obj_t BGl_z62processzd2outputzd2portz62zz__processz00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__processz00(void);
static obj_t BGl_genericzd2initzd2zz__processz00(void);
BGL_EXPORTED_DECL bool_t BGl_processzd2waitzd2zz__processz00(obj_t);
extern obj_t c_process_kill(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__processz00(void);
static obj_t BGl_z62closezd2processzd2portsz62zz__processz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2inputzd2portz00zz__processz00(obj_t);
static obj_t BGl_keyword1702z00zz__processz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__processz00(void);
BGL_EXPORTED_DECL obj_t BGl_processzd2nilzd2zz__processz00(void);
BGL_EXPORTED_DECL obj_t BGl_processzd2stopzd2zz__processz00(obj_t);
static obj_t BGl_keyword1706z00zz__processz00 = BUNSPEC;
static obj_t BGl_keyword1708z00zz__processz00 = BUNSPEC;
static obj_t BGl_objectzd2initzd2zz__processz00(void);
static obj_t BGl_z62processzd2exitzd2statusz62zz__processz00(obj_t, obj_t);
static obj_t BGl_keyword1710z00zz__processz00 = BUNSPEC;
static obj_t BGl_keyword1712z00zz__processz00 = BUNSPEC;
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t c_process_wait(obj_t);
static obj_t BGl_keyword1714z00zz__processz00 = BUNSPEC;
static obj_t BGl_keyword1716z00zz__processz00 = BUNSPEC;
static obj_t BGl_keyword1718z00zz__processz00 = BUNSPEC;
extern obj_t bgl_reverse_bang(obj_t);
extern obj_t bgl_process_nil(void);
static obj_t BGl_z62processzd2alivezf3z43zz__processz00(obj_t, obj_t);
extern obj_t c_process_stop(obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2continuezd2zz__processz00(obj_t);
static obj_t BGl_z62processzd2inputzd2portz62zz__processz00(obj_t, obj_t);
extern obj_t c_process_continue(obj_t);
BGL_EXPORTED_DECL int BGl_processzd2pidzd2zz__processz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_unregisterzd2processzd2zz__processz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_runzd2processzd2zz__processz00(obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__processz00(void);
static obj_t BGl_z62processzf3z91zz__processz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2sendzd2signalz00zz__processz00(obj_t, int);
extern obj_t c_unregister_process(obj_t);
extern obj_t c_run_process(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62unregisterzd2processzb0zz__processz00(obj_t, obj_t);
extern obj_t bgl_close_input_port(obj_t);
static obj_t BGl_z62processzd2listzb0zz__processz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2outputzd2portz00zz__processz00(obj_t);
extern obj_t c_process_send_signal(obj_t, int);
BGL_EXPORTED_DECL bool_t BGl_processzd2alivezf3z21zz__processz00(obj_t);
static obj_t BGl_keyword1698z00zz__processz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_closezd2processzd2portsz00zz__processz00(obj_t);
static obj_t BGl_z62processzd2nilzb0zz__processz00(obj_t);
static obj_t BGl_z62processzd2continuezb0zz__processz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2exitzd2statusz00zz__processz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_processzd2listzd2zz__processz00(void);
extern obj_t bgl_close_output_port(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern bool_t c_process_alivep(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2errorzd2portzd2envzd2zz__processz00, BgL_bgl_za762processza7d2err1728z00, BGl_z62processzd2errorzd2portz62zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2nilzd2envz00zz__processz00, BgL_bgl_za762processza7d2nil1729z00, BGl_z62processzd2nilzb0zz__processz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2killzd2envz00zz__processz00, BgL_bgl_za762processza7d2kil1730z00, BGl_z62processzd2killzb0zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1700z00zz__processz00, BgL_bgl_string1700za700za7za7_1731za7, "loop", 4 );
DEFINE_STRING( BGl_string1701z00zz__processz00, BgL_bgl_string1701za700za7za7_1732za7, "pair", 4 );
DEFINE_STRING( BGl_string1703z00zz__processz00, BgL_bgl_string1703za700za7za7_1733za7, "wait", 4 );
DEFINE_STRING( BGl_string1704z00zz__processz00, BgL_bgl_string1704za700za7za7_1734za7, "run-process", 11 );
DEFINE_STRING( BGl_string1705z00zz__processz00, BgL_bgl_string1705za700za7za7_1735za7, "Illegal argument", 16 );
DEFINE_STRING( BGl_string1707z00zz__processz00, BgL_bgl_string1707za700za7za7_1736za7, "fork", 4 );
DEFINE_STRING( BGl_string1709z00zz__processz00, BgL_bgl_string1709za700za7za7_1737za7, "input", 5 );
DEFINE_STRING( BGl_string1711z00zz__processz00, BgL_bgl_string1711za700za7za7_1738za7, "output", 6 );
DEFINE_STRING( BGl_string1713z00zz__processz00, BgL_bgl_string1713za700za7za7_1739za7, "null", 4 );
DEFINE_STRING( BGl_string1715z00zz__processz00, BgL_bgl_string1715za700za7za7_1740za7, "error", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2outputzd2portzd2envzd2zz__processz00, BgL_bgl_za762processza7d2out1741z00, BGl_z62processzd2outputzd2portz62zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1717z00zz__processz00, BgL_bgl_string1717za700za7za7_1742za7, "host", 4 );
DEFINE_STRING( BGl_string1719z00zz__processz00, BgL_bgl_string1719za700za7za7_1743za7, "env", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2waitzd2envz00zz__processz00, BgL_bgl_za762processza7d2wai1744z00, BGl_z62processzd2waitzb0zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closezd2processzd2portszd2envzd2zz__processz00, BgL_bgl_za762closeza7d2proce1745z00, BGl_z62closezd2processzd2portsz62zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1720z00zz__processz00, BgL_bgl_string1720za700za7za7_1746za7, "&run-process", 12 );
DEFINE_STRING( BGl_string1721z00zz__processz00, BgL_bgl_string1721za700za7za7_1747za7, "bstring", 7 );
DEFINE_STRING( BGl_string1722z00zz__processz00, BgL_bgl_string1722za700za7za7_1748za7, "close-process-ports", 19 );
DEFINE_STRING( BGl_string1723z00zz__processz00, BgL_bgl_string1723za700za7za7_1749za7, "output-port", 11 );
DEFINE_STRING( BGl_string1724z00zz__processz00, BgL_bgl_string1724za700za7za7_1750za7, "input-port", 10 );
DEFINE_STRING( BGl_string1725z00zz__processz00, BgL_bgl_string1725za700za7za7_1751za7, "&close-process-ports", 20 );
DEFINE_STRING( BGl_string1726z00zz__processz00, BgL_bgl_string1726za700za7za7_1752za7, "&unregister-process", 19 );
DEFINE_STRING( BGl_string1727z00zz__processz00, BgL_bgl_string1727za700za7za7_1753za7, "__process", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unregisterzd2processzd2envz00zz__processz00, BgL_bgl_za762unregisterza7d21754z00, BGl_z62unregisterzd2processzb0zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2exitzd2statuszd2envzd2zz__processz00, BgL_bgl_za762processza7d2exi1755z00, BGl_z62processzd2exitzd2statusz62zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2continuezd2envz00zz__processz00, BgL_bgl_za762processza7d2con1756z00, BGl_z62processzd2continuezb0zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzf3zd2envz21zz__processz00, BgL_bgl_za762processza7f3za7911757za7, BGl_z62processzf3z91zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2alivezf3zd2envzf3zz__processz00, BgL_bgl_za762processza7d2ali1758z00, BGl_z62processzd2alivezf3z43zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2pidzd2envz00zz__processz00, BgL_bgl_za762processza7d2pid1759z00, BGl_z62processzd2pidzb0zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2inputzd2portzd2envzd2zz__processz00, BgL_bgl_za762processza7d2inp1760z00, BGl_z62processzd2inputzd2portz62zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2stopzd2envz00zz__processz00, BgL_bgl_za762processza7d2sto1761z00, BGl_z62processzd2stopzb0zz__processz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1683z00zz__processz00, BgL_bgl_string1683za700za7za7_1762za7, "/tmp/bigloo/runtime/Llib/process.scm", 36 );
DEFINE_STRING( BGl_string1684z00zz__processz00, BgL_bgl_string1684za700za7za7_1763za7, "&process-pid", 12 );
DEFINE_STRING( BGl_string1685z00zz__processz00, BgL_bgl_string1685za700za7za7_1764za7, "process", 7 );
DEFINE_STRING( BGl_string1686z00zz__processz00, BgL_bgl_string1686za700za7za7_1765za7, "&process-output-port", 20 );
DEFINE_STRING( BGl_string1687z00zz__processz00, BgL_bgl_string1687za700za7za7_1766za7, "&process-input-port", 19 );
DEFINE_STRING( BGl_string1688z00zz__processz00, BgL_bgl_string1688za700za7za7_1767za7, "&process-error-port", 19 );
DEFINE_STRING( BGl_string1689z00zz__processz00, BgL_bgl_string1689za700za7za7_1768za7, "&process-alive?", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_runzd2processzd2envz00zz__processz00, BgL_bgl_za762runza7d2process1769z00, va_generic_entry, BGl_z62runzd2processzb0zz__processz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2listzd2envz00zz__processz00, BgL_bgl_za762processza7d2lis1770z00, BGl_z62processzd2listzb0zz__processz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1690z00zz__processz00, BgL_bgl_string1690za700za7za7_1771za7, "&process-wait", 13 );
DEFINE_STRING( BGl_string1691z00zz__processz00, BgL_bgl_string1691za700za7za7_1772za7, "&process-exit-status", 20 );
DEFINE_STRING( BGl_string1692z00zz__processz00, BgL_bgl_string1692za700za7za7_1773za7, "&process-send-signal", 20 );
DEFINE_STRING( BGl_string1693z00zz__processz00, BgL_bgl_string1693za700za7za7_1774za7, "bint", 4 );
DEFINE_STRING( BGl_string1694z00zz__processz00, BgL_bgl_string1694za700za7za7_1775za7, "&process-kill", 13 );
DEFINE_STRING( BGl_string1695z00zz__processz00, BgL_bgl_string1695za700za7za7_1776za7, "&process-stop", 13 );
DEFINE_STRING( BGl_string1696z00zz__processz00, BgL_bgl_string1696za700za7za7_1777za7, "&process-continue", 17 );
DEFINE_STRING( BGl_string1699z00zz__processz00, BgL_bgl_string1699za700za7za7_1778za7, "pipe", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_processzd2sendzd2signalzd2envzd2zz__processz00, BgL_bgl_za762processza7d2sen1779z00, BGl_z62processzd2sendzd2signalz62zz__processz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__processz00) );
ADD_ROOT( (void *)(&BGl_list1697z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1702z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1706z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1708z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1710z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1712z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1714z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1716z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1718z00zz__processz00) );
ADD_ROOT( (void *)(&BGl_keyword1698z00zz__processz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__processz00(long BgL_checksumz00_1918, char * BgL_fromz00_1919)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__processz00))
{ 
BGl_requirezd2initializa7ationz75zz__processz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__processz00(); 
BGl_cnstzd2initzd2zz__processz00(); 
BGl_importedzd2moduleszd2initz00zz__processz00(); 
return 
BGl_methodzd2initzd2zz__processz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__processz00(void)
{
{ /* Llib/process.scm 15 */
BGl_keyword1698z00zz__processz00 = 
bstring_to_keyword(BGl_string1699z00zz__processz00); 
BGl_list1697z00zz__processz00 = 
MAKE_YOUNG_PAIR(BGl_keyword1698z00zz__processz00, BNIL); 
BGl_keyword1702z00zz__processz00 = 
bstring_to_keyword(BGl_string1703z00zz__processz00); 
BGl_keyword1706z00zz__processz00 = 
bstring_to_keyword(BGl_string1707z00zz__processz00); 
BGl_keyword1708z00zz__processz00 = 
bstring_to_keyword(BGl_string1709z00zz__processz00); 
BGl_keyword1710z00zz__processz00 = 
bstring_to_keyword(BGl_string1711z00zz__processz00); 
BGl_keyword1712z00zz__processz00 = 
bstring_to_keyword(BGl_string1713z00zz__processz00); 
BGl_keyword1714z00zz__processz00 = 
bstring_to_keyword(BGl_string1715z00zz__processz00); 
BGl_keyword1716z00zz__processz00 = 
bstring_to_keyword(BGl_string1717z00zz__processz00); 
return ( 
BGl_keyword1718z00zz__processz00 = 
bstring_to_keyword(BGl_string1719z00zz__processz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__processz00(void)
{
{ /* Llib/process.scm 15 */
return 
bgl_gc_roots_register();} 

}



/* process? */
BGL_EXPORTED_DEF bool_t BGl_processzf3zf3zz__processz00(obj_t BgL_objz00_3)
{
{ /* Llib/process.scm 123 */
return 
PROCESSP(BgL_objz00_3);} 

}



/* &process? */
obj_t BGl_z62processzf3z91zz__processz00(obj_t BgL_envz00_1818, obj_t BgL_objz00_1819)
{
{ /* Llib/process.scm 123 */
return 
BBOOL(
BGl_processzf3zf3zz__processz00(BgL_objz00_1819));} 

}



/* process-nil */
BGL_EXPORTED_DEF obj_t BGl_processzd2nilzd2zz__processz00(void)
{
{ /* Llib/process.scm 129 */
BGL_TAIL return 
bgl_process_nil();} 

}



/* &process-nil */
obj_t BGl_z62processzd2nilzb0zz__processz00(obj_t BgL_envz00_1820)
{
{ /* Llib/process.scm 129 */
return 
BGl_processzd2nilzd2zz__processz00();} 

}



/* process-pid */
BGL_EXPORTED_DEF int BGl_processzd2pidzd2zz__processz00(obj_t BgL_procz00_4)
{
{ /* Llib/process.scm 135 */
return 
PROCESS_PID(BgL_procz00_4);} 

}



/* &process-pid */
obj_t BGl_z62processzd2pidzb0zz__processz00(obj_t BgL_envz00_1821, obj_t BgL_procz00_1822)
{
{ /* Llib/process.scm 135 */
{ /* Llib/process.scm 136 */
 int BgL_tmpz00_1944;
{ /* Llib/process.scm 136 */
 obj_t BgL_auxz00_1945;
if(
PROCESSP(BgL_procz00_1822))
{ /* Llib/process.scm 136 */
BgL_auxz00_1945 = BgL_procz00_1822
; }  else 
{ 
 obj_t BgL_auxz00_1948;
BgL_auxz00_1948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(5714L), BGl_string1684z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1822); 
FAILURE(BgL_auxz00_1948,BFALSE,BFALSE);} 
BgL_tmpz00_1944 = 
BGl_processzd2pidzd2zz__processz00(BgL_auxz00_1945); } 
return 
BINT(BgL_tmpz00_1944);} } 

}



/* process-output-port */
BGL_EXPORTED_DEF obj_t BGl_processzd2outputzd2portz00zz__processz00(obj_t BgL_procz00_5)
{
{ /* Llib/process.scm 141 */
return 
PROCESS_OUTPUT_PORT(BgL_procz00_5);} 

}



/* &process-output-port */
obj_t BGl_z62processzd2outputzd2portz62zz__processz00(obj_t BgL_envz00_1823, obj_t BgL_procz00_1824)
{
{ /* Llib/process.scm 141 */
{ /* Llib/process.scm 142 */
 obj_t BgL_auxz00_1955;
if(
PROCESSP(BgL_procz00_1824))
{ /* Llib/process.scm 142 */
BgL_auxz00_1955 = BgL_procz00_1824
; }  else 
{ 
 obj_t BgL_auxz00_1958;
BgL_auxz00_1958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(6004L), BGl_string1686z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1824); 
FAILURE(BgL_auxz00_1958,BFALSE,BFALSE);} 
return 
BGl_processzd2outputzd2portz00zz__processz00(BgL_auxz00_1955);} } 

}



/* process-input-port */
BGL_EXPORTED_DEF obj_t BGl_processzd2inputzd2portz00zz__processz00(obj_t BgL_procz00_6)
{
{ /* Llib/process.scm 147 */
return 
PROCESS_INPUT_PORT(BgL_procz00_6);} 

}



/* &process-input-port */
obj_t BGl_z62processzd2inputzd2portz62zz__processz00(obj_t BgL_envz00_1825, obj_t BgL_procz00_1826)
{
{ /* Llib/process.scm 147 */
{ /* Llib/process.scm 148 */
 obj_t BgL_auxz00_1964;
if(
PROCESSP(BgL_procz00_1826))
{ /* Llib/process.scm 148 */
BgL_auxz00_1964 = BgL_procz00_1826
; }  else 
{ 
 obj_t BgL_auxz00_1967;
BgL_auxz00_1967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(6301L), BGl_string1687z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1826); 
FAILURE(BgL_auxz00_1967,BFALSE,BFALSE);} 
return 
BGl_processzd2inputzd2portz00zz__processz00(BgL_auxz00_1964);} } 

}



/* process-error-port */
BGL_EXPORTED_DEF obj_t BGl_processzd2errorzd2portz00zz__processz00(obj_t BgL_procz00_7)
{
{ /* Llib/process.scm 153 */
return 
PROCESS_ERROR_PORT(BgL_procz00_7);} 

}



/* &process-error-port */
obj_t BGl_z62processzd2errorzd2portz62zz__processz00(obj_t BgL_envz00_1827, obj_t BgL_procz00_1828)
{
{ /* Llib/process.scm 153 */
{ /* Llib/process.scm 154 */
 obj_t BgL_auxz00_1973;
if(
PROCESSP(BgL_procz00_1828))
{ /* Llib/process.scm 154 */
BgL_auxz00_1973 = BgL_procz00_1828
; }  else 
{ 
 obj_t BgL_auxz00_1976;
BgL_auxz00_1976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(6597L), BGl_string1688z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1828); 
FAILURE(BgL_auxz00_1976,BFALSE,BFALSE);} 
return 
BGl_processzd2errorzd2portz00zz__processz00(BgL_auxz00_1973);} } 

}



/* process-alive? */
BGL_EXPORTED_DEF bool_t BGl_processzd2alivezf3z21zz__processz00(obj_t BgL_procz00_8)
{
{ /* Llib/process.scm 159 */
BGL_TAIL return 
c_process_alivep(BgL_procz00_8);} 

}



/* &process-alive? */
obj_t BGl_z62processzd2alivezf3z43zz__processz00(obj_t BgL_envz00_1829, obj_t BgL_procz00_1830)
{
{ /* Llib/process.scm 159 */
{ /* Llib/process.scm 160 */
 bool_t BgL_tmpz00_1982;
{ /* Llib/process.scm 160 */
 obj_t BgL_auxz00_1983;
if(
PROCESSP(BgL_procz00_1830))
{ /* Llib/process.scm 160 */
BgL_auxz00_1983 = BgL_procz00_1830
; }  else 
{ 
 obj_t BgL_auxz00_1986;
BgL_auxz00_1986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(6889L), BGl_string1689z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1830); 
FAILURE(BgL_auxz00_1986,BFALSE,BFALSE);} 
BgL_tmpz00_1982 = 
BGl_processzd2alivezf3z21zz__processz00(BgL_auxz00_1983); } 
return 
BBOOL(BgL_tmpz00_1982);} } 

}



/* process-wait */
BGL_EXPORTED_DEF bool_t BGl_processzd2waitzd2zz__processz00(obj_t BgL_procz00_9)
{
{ /* Llib/process.scm 165 */
if(
c_process_alivep(BgL_procz00_9))
{ /* Llib/process.scm 166 */
return 
CBOOL(
c_process_wait(BgL_procz00_9));}  else 
{ /* Llib/process.scm 166 */
return ((bool_t)0);} } 

}



/* &process-wait */
obj_t BGl_z62processzd2waitzb0zz__processz00(obj_t BgL_envz00_1831, obj_t BgL_procz00_1832)
{
{ /* Llib/process.scm 165 */
{ /* Llib/process.scm 166 */
 bool_t BgL_tmpz00_1996;
{ /* Llib/process.scm 166 */
 obj_t BgL_auxz00_1997;
if(
PROCESSP(BgL_procz00_1832))
{ /* Llib/process.scm 166 */
BgL_auxz00_1997 = BgL_procz00_1832
; }  else 
{ 
 obj_t BgL_auxz00_2000;
BgL_auxz00_2000 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(7175L), BGl_string1690z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1832); 
FAILURE(BgL_auxz00_2000,BFALSE,BFALSE);} 
BgL_tmpz00_1996 = 
BGl_processzd2waitzd2zz__processz00(BgL_auxz00_1997); } 
return 
BBOOL(BgL_tmpz00_1996);} } 

}



/* process-exit-status */
BGL_EXPORTED_DEF obj_t BGl_processzd2exitzd2statusz00zz__processz00(obj_t BgL_procz00_10)
{
{ /* Llib/process.scm 172 */
BGL_TAIL return 
c_process_xstatus(BgL_procz00_10);} 

}



/* &process-exit-status */
obj_t BGl_z62processzd2exitzd2statusz62zz__processz00(obj_t BgL_envz00_1833, obj_t BgL_procz00_1834)
{
{ /* Llib/process.scm 172 */
{ /* Llib/process.scm 173 */
 obj_t BgL_auxz00_2007;
if(
PROCESSP(BgL_procz00_1834))
{ /* Llib/process.scm 173 */
BgL_auxz00_2007 = BgL_procz00_1834
; }  else 
{ 
 obj_t BgL_auxz00_2010;
BgL_auxz00_2010 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(7500L), BGl_string1691z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1834); 
FAILURE(BgL_auxz00_2010,BFALSE,BFALSE);} 
return 
BGl_processzd2exitzd2statusz00zz__processz00(BgL_auxz00_2007);} } 

}



/* process-send-signal */
BGL_EXPORTED_DEF obj_t BGl_processzd2sendzd2signalz00zz__processz00(obj_t BgL_procz00_11, int BgL_signalz00_12)
{
{ /* Llib/process.scm 178 */
BGL_TAIL return 
c_process_send_signal(BgL_procz00_11, BgL_signalz00_12);} 

}



/* &process-send-signal */
obj_t BGl_z62processzd2sendzd2signalz62zz__processz00(obj_t BgL_envz00_1835, obj_t BgL_procz00_1836, obj_t BgL_signalz00_1837)
{
{ /* Llib/process.scm 178 */
{ /* Llib/process.scm 179 */
 int BgL_auxz00_2023; obj_t BgL_auxz00_2016;
{ /* Llib/process.scm 179 */
 obj_t BgL_tmpz00_2024;
if(
INTEGERP(BgL_signalz00_1837))
{ /* Llib/process.scm 179 */
BgL_tmpz00_2024 = BgL_signalz00_1837
; }  else 
{ 
 obj_t BgL_auxz00_2027;
BgL_auxz00_2027 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(7805L), BGl_string1692z00zz__processz00, BGl_string1693z00zz__processz00, BgL_signalz00_1837); 
FAILURE(BgL_auxz00_2027,BFALSE,BFALSE);} 
BgL_auxz00_2023 = 
CINT(BgL_tmpz00_2024); } 
if(
PROCESSP(BgL_procz00_1836))
{ /* Llib/process.scm 179 */
BgL_auxz00_2016 = BgL_procz00_1836
; }  else 
{ 
 obj_t BgL_auxz00_2019;
BgL_auxz00_2019 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(7805L), BGl_string1692z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1836); 
FAILURE(BgL_auxz00_2019,BFALSE,BFALSE);} 
return 
BGl_processzd2sendzd2signalz00zz__processz00(BgL_auxz00_2016, BgL_auxz00_2023);} } 

}



/* process-kill */
BGL_EXPORTED_DEF obj_t BGl_processzd2killzd2zz__processz00(obj_t BgL_procz00_13)
{
{ /* Llib/process.scm 184 */
c_process_kill(BgL_procz00_13); 
BGL_TAIL return 
BGl_closezd2processzd2portsz00zz__processz00(BgL_procz00_13);} 

}



/* &process-kill */
obj_t BGl_z62processzd2killzb0zz__processz00(obj_t BgL_envz00_1838, obj_t BgL_procz00_1839)
{
{ /* Llib/process.scm 184 */
{ /* Llib/process.scm 185 */
 obj_t BgL_auxz00_2035;
if(
PROCESSP(BgL_procz00_1839))
{ /* Llib/process.scm 185 */
BgL_auxz00_2035 = BgL_procz00_1839
; }  else 
{ 
 obj_t BgL_auxz00_2038;
BgL_auxz00_2038 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(8123L), BGl_string1694z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1839); 
FAILURE(BgL_auxz00_2038,BFALSE,BFALSE);} 
return 
BGl_processzd2killzd2zz__processz00(BgL_auxz00_2035);} } 

}



/* process-stop */
BGL_EXPORTED_DEF obj_t BGl_processzd2stopzd2zz__processz00(obj_t BgL_procz00_14)
{
{ /* Llib/process.scm 191 */
BGL_TAIL return 
c_process_stop(BgL_procz00_14);} 

}



/* &process-stop */
obj_t BGl_z62processzd2stopzb0zz__processz00(obj_t BgL_envz00_1840, obj_t BgL_procz00_1841)
{
{ /* Llib/process.scm 191 */
{ /* Llib/process.scm 192 */
 obj_t BgL_auxz00_2044;
if(
PROCESSP(BgL_procz00_1841))
{ /* Llib/process.scm 192 */
BgL_auxz00_2044 = BgL_procz00_1841
; }  else 
{ 
 obj_t BgL_auxz00_2047;
BgL_auxz00_2047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(8417L), BGl_string1695z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1841); 
FAILURE(BgL_auxz00_2047,BFALSE,BFALSE);} 
return 
BGl_processzd2stopzd2zz__processz00(BgL_auxz00_2044);} } 

}



/* process-continue */
BGL_EXPORTED_DEF obj_t BGl_processzd2continuezd2zz__processz00(obj_t BgL_procz00_15)
{
{ /* Llib/process.scm 197 */
BGL_TAIL return 
c_process_continue(BgL_procz00_15);} 

}



/* &process-continue */
obj_t BGl_z62processzd2continuezb0zz__processz00(obj_t BgL_envz00_1842, obj_t BgL_procz00_1843)
{
{ /* Llib/process.scm 197 */
{ /* Llib/process.scm 198 */
 obj_t BgL_auxz00_2053;
if(
PROCESSP(BgL_procz00_1843))
{ /* Llib/process.scm 198 */
BgL_auxz00_2053 = BgL_procz00_1843
; }  else 
{ 
 obj_t BgL_auxz00_2056;
BgL_auxz00_2056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(8705L), BGl_string1696z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1843); 
FAILURE(BgL_auxz00_2056,BFALSE,BFALSE);} 
return 
BGl_processzd2continuezd2zz__processz00(BgL_auxz00_2053);} } 

}



/* process-list */
BGL_EXPORTED_DEF obj_t BGl_processzd2listzd2zz__processz00(void)
{
{ /* Llib/process.scm 203 */
BGL_TAIL return 
c_process_list();} 

}



/* &process-list */
obj_t BGl_z62processzd2listzb0zz__processz00(obj_t BgL_envz00_1844)
{
{ /* Llib/process.scm 203 */
return 
BGl_processzd2listzd2zz__processz00();} 

}



/* run-process */
BGL_EXPORTED_DEF obj_t BGl_runzd2processzd2zz__processz00(obj_t BgL_commandz00_16, obj_t BgL_restz00_17)
{
{ /* Llib/process.scm 212 */
{ /* Llib/process.scm 213 */
 bool_t BgL_forkz00_1114; bool_t BgL_waitz00_1115; obj_t BgL_inputz00_1116; obj_t BgL_outputz00_1117; obj_t BgL_errorz00_1118; obj_t BgL_hostz00_1119; obj_t BgL_pipesz00_1120; obj_t BgL_argsz00_1121; obj_t BgL_envz00_1122;
BgL_forkz00_1114 = ((bool_t)1); 
BgL_waitz00_1115 = ((bool_t)0); 
BgL_inputz00_1116 = BUNSPEC; 
BgL_outputz00_1117 = BUNSPEC; 
BgL_errorz00_1118 = BUNSPEC; 
BgL_hostz00_1119 = BUNSPEC; 
BgL_pipesz00_1120 = BGl_list1697z00zz__processz00; 
BgL_argsz00_1121 = BNIL; 
BgL_envz00_1122 = BNIL; 
{ 
 obj_t BgL_restz00_1125;
{ /* Llib/process.scm 224 */
 obj_t BgL_aux1669z00_1895;
BgL_restz00_1125 = BgL_restz00_17; 
BgL_zc3z04anonymousza31154ze3z87_1126:
if(
NULLP(BgL_restz00_1125))
{ /* Llib/process.scm 229 */
 obj_t BgL_arg1157z00_1128;
BgL_arg1157z00_1128 = 
bgl_reverse_bang(BgL_argsz00_1121); 
BgL_aux1669z00_1895 = 
c_run_process(BgL_hostz00_1119, 
BBOOL(BgL_forkz00_1114), 
BBOOL(BgL_waitz00_1115), BgL_inputz00_1116, BgL_outputz00_1117, BgL_errorz00_1118, BgL_commandz00_16, BgL_arg1157z00_1128, BgL_envz00_1122); }  else 
{ /* Llib/process.scm 230 */
 bool_t BgL_test1795z00_2069;
{ /* Llib/process.scm 230 */
 bool_t BgL_test1796z00_2070;
{ /* Llib/process.scm 230 */
 obj_t BgL_tmpz00_2071;
{ /* Llib/process.scm 230 */
 obj_t BgL_pairz00_1600;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 230 */
BgL_pairz00_1600 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2074;
BgL_auxz00_2074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(9937L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2074,BFALSE,BFALSE);} 
BgL_tmpz00_2071 = 
CAR(BgL_pairz00_1600); } 
BgL_test1796z00_2070 = 
KEYWORDP(BgL_tmpz00_2071); } 
if(BgL_test1796z00_2070)
{ /* Llib/process.scm 230 */
 obj_t BgL_tmpz00_2080;
{ /* Llib/process.scm 230 */
 obj_t BgL_pairz00_1601;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 230 */
BgL_pairz00_1601 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2083;
BgL_auxz00_2083 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(9956L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2083,BFALSE,BFALSE);} 
BgL_tmpz00_2080 = 
CDR(BgL_pairz00_1601); } 
BgL_test1795z00_2069 = 
PAIRP(BgL_tmpz00_2080); }  else 
{ /* Llib/process.scm 230 */
BgL_test1795z00_2069 = ((bool_t)0)
; } } 
if(BgL_test1795z00_2069)
{ /* Llib/process.scm 231 */
 obj_t BgL_valz00_1134;
{ /* Llib/process.scm 231 */
 obj_t BgL_pairz00_1602;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 231 */
BgL_pairz00_1602 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2091;
BgL_auxz00_2091 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(9987L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2091,BFALSE,BFALSE);} 
{ /* Llib/process.scm 231 */
 obj_t BgL_pairz00_1605;
{ /* Llib/process.scm 231 */
 obj_t BgL_aux1655z00_1881;
BgL_aux1655z00_1881 = 
CDR(BgL_pairz00_1602); 
if(
PAIRP(BgL_aux1655z00_1881))
{ /* Llib/process.scm 231 */
BgL_pairz00_1605 = BgL_aux1655z00_1881; }  else 
{ 
 obj_t BgL_auxz00_2098;
BgL_auxz00_2098 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(9981L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_aux1655z00_1881); 
FAILURE(BgL_auxz00_2098,BFALSE,BFALSE);} } 
BgL_valz00_1134 = 
CAR(BgL_pairz00_1605); } } 
{ /* Llib/process.scm 232 */
 obj_t BgL_casezd2valuezd2_1135;
{ /* Llib/process.scm 232 */
 obj_t BgL_pairz00_1606;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 232 */
BgL_pairz00_1606 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2105;
BgL_auxz00_2105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(10008L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2105,BFALSE,BFALSE);} 
BgL_casezd2valuezd2_1135 = 
CAR(BgL_pairz00_1606); } 
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1702z00zz__processz00))
{ /* Llib/process.scm 232 */
if(
BOOLEANP(BgL_valz00_1134))
{ /* Llib/process.scm 234 */
BgL_waitz00_1115 = 
CBOOL(BgL_valz00_1134); }  else 
{ /* Llib/process.scm 234 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1706z00zz__processz00))
{ /* Llib/process.scm 232 */
if(
BOOLEANP(BgL_valz00_1134))
{ /* Llib/process.scm 238 */
BgL_forkz00_1114 = 
CBOOL(BgL_valz00_1134); }  else 
{ /* Llib/process.scm 238 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1708z00zz__processz00))
{ /* Llib/process.scm 242 */
 bool_t BgL_test1807z00_2124;
if(
STRINGP(BgL_valz00_1134))
{ /* Llib/process.scm 242 */
BgL_test1807z00_2124 = ((bool_t)1)
; }  else 
{ /* Llib/process.scm 242 */
BgL_test1807z00_2124 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_valz00_1134, BgL_pipesz00_1120))
; } 
if(BgL_test1807z00_2124)
{ /* Llib/process.scm 242 */
BgL_inputz00_1116 = BgL_valz00_1134; }  else 
{ /* Llib/process.scm 242 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1710z00zz__processz00))
{ /* Llib/process.scm 246 */
 bool_t BgL_test1810z00_2132;
if(
STRINGP(BgL_valz00_1134))
{ /* Llib/process.scm 246 */
BgL_test1810z00_2132 = ((bool_t)1)
; }  else 
{ /* Llib/process.scm 246 */
if(
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_valz00_1134, BgL_pipesz00_1120)))
{ /* Llib/process.scm 246 */
BgL_test1810z00_2132 = ((bool_t)1)
; }  else 
{ /* Llib/process.scm 246 */
BgL_test1810z00_2132 = 
(BgL_valz00_1134==BGl_keyword1712z00zz__processz00)
; } } 
if(BgL_test1810z00_2132)
{ /* Llib/process.scm 246 */
BgL_outputz00_1117 = BgL_valz00_1134; }  else 
{ /* Llib/process.scm 246 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1714z00zz__processz00))
{ /* Llib/process.scm 250 */
 bool_t BgL_test1814z00_2142;
if(
STRINGP(BgL_valz00_1134))
{ /* Llib/process.scm 250 */
BgL_test1814z00_2142 = ((bool_t)1)
; }  else 
{ /* Llib/process.scm 250 */
if(
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_valz00_1134, BgL_pipesz00_1120)))
{ /* Llib/process.scm 250 */
BgL_test1814z00_2142 = ((bool_t)1)
; }  else 
{ /* Llib/process.scm 250 */
BgL_test1814z00_2142 = 
(BgL_valz00_1134==BGl_keyword1712z00zz__processz00)
; } } 
if(BgL_test1814z00_2142)
{ /* Llib/process.scm 250 */
BgL_errorz00_1118 = BgL_valz00_1134; }  else 
{ /* Llib/process.scm 250 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1716z00zz__processz00))
{ /* Llib/process.scm 232 */
if(
STRINGP(BgL_valz00_1134))
{ /* Llib/process.scm 254 */
BgL_hostz00_1119 = BgL_valz00_1134; }  else 
{ /* Llib/process.scm 254 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
if(
(BgL_casezd2valuezd2_1135==BGl_keyword1718z00zz__processz00))
{ /* Llib/process.scm 232 */
if(
STRINGP(BgL_valz00_1134))
{ /* Llib/process.scm 258 */
BgL_envz00_1122 = 
MAKE_YOUNG_PAIR(BgL_valz00_1134, BgL_envz00_1122); }  else 
{ /* Llib/process.scm 258 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } }  else 
{ /* Llib/process.scm 232 */
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } } } } } } } } 
{ /* Llib/process.scm 263 */
 obj_t BgL_arg1187z00_1160;
{ /* Llib/process.scm 263 */
 obj_t BgL_pairz00_1615;
{ /* Llib/process.scm 263 */
 obj_t BgL_pairz00_1614;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 263 */
BgL_pairz00_1614 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2164;
BgL_auxz00_2164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(10738L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2164,BFALSE,BFALSE);} 
{ /* Llib/process.scm 263 */
 obj_t BgL_aux1661z00_1887;
BgL_aux1661z00_1887 = 
CDR(BgL_pairz00_1614); 
if(
PAIRP(BgL_aux1661z00_1887))
{ /* Llib/process.scm 263 */
BgL_pairz00_1615 = BgL_aux1661z00_1887; }  else 
{ 
 obj_t BgL_auxz00_2171;
BgL_auxz00_2171 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(10733L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_aux1661z00_1887); 
FAILURE(BgL_auxz00_2171,BFALSE,BFALSE);} } } 
BgL_arg1187z00_1160 = 
CDR(BgL_pairz00_1615); } 
{ 
 obj_t BgL_restz00_2176;
BgL_restz00_2176 = BgL_arg1187z00_1160; 
BgL_restz00_1125 = BgL_restz00_2176; 
goto BgL_zc3z04anonymousza31154ze3z87_1126;} } }  else 
{ /* Llib/process.scm 264 */
 bool_t BgL_test1823z00_2177;
{ /* Llib/process.scm 264 */
 obj_t BgL_tmpz00_2178;
{ /* Llib/process.scm 264 */
 obj_t BgL_pairz00_1616;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 264 */
BgL_pairz00_1616 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2181;
BgL_auxz00_2181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(10768L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2181,BFALSE,BFALSE);} 
BgL_tmpz00_2178 = 
CAR(BgL_pairz00_1616); } 
BgL_test1823z00_2177 = 
STRINGP(BgL_tmpz00_2178); } 
if(BgL_test1823z00_2177)
{ /* Llib/process.scm 264 */
{ /* Llib/process.scm 265 */
 obj_t BgL_arg1191z00_1164;
{ /* Llib/process.scm 265 */
 obj_t BgL_pairz00_1617;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 265 */
BgL_pairz00_1617 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2189;
BgL_auxz00_2189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(10803L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2189,BFALSE,BFALSE);} 
BgL_arg1191z00_1164 = 
CAR(BgL_pairz00_1617); } 
BgL_argsz00_1121 = 
MAKE_YOUNG_PAIR(BgL_arg1191z00_1164, BgL_argsz00_1121); } 
{ /* Llib/process.scm 266 */
 obj_t BgL_arg1193z00_1165;
{ /* Llib/process.scm 266 */
 obj_t BgL_pairz00_1618;
if(
PAIRP(BgL_restz00_1125))
{ /* Llib/process.scm 266 */
BgL_pairz00_1618 = BgL_restz00_1125; }  else 
{ 
 obj_t BgL_auxz00_2197;
BgL_auxz00_2197 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(10833L), BGl_string1700z00zz__processz00, BGl_string1701z00zz__processz00, BgL_restz00_1125); 
FAILURE(BgL_auxz00_2197,BFALSE,BFALSE);} 
BgL_arg1193z00_1165 = 
CDR(BgL_pairz00_1618); } 
{ 
 obj_t BgL_restz00_2202;
BgL_restz00_2202 = BgL_arg1193z00_1165; 
BgL_restz00_1125 = BgL_restz00_2202; 
goto BgL_zc3z04anonymousza31154ze3z87_1126;} } }  else 
{ /* Llib/process.scm 264 */
BgL_aux1669z00_1895 = 
BGl_errorz00zz__errorz00(BGl_string1704z00zz__processz00, BGl_string1705z00zz__processz00, BgL_restz00_1125); } } } 
if(
PROCESSP(BgL_aux1669z00_1895))
{ /* Llib/process.scm 224 */
return BgL_aux1669z00_1895;}  else 
{ 
 obj_t BgL_auxz00_2206;
BgL_auxz00_2206 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(9761L), BGl_string1704z00zz__processz00, BGl_string1685z00zz__processz00, BgL_aux1669z00_1895); 
FAILURE(BgL_auxz00_2206,BFALSE,BFALSE);} } } } } 

}



/* &run-process */
obj_t BGl_z62runzd2processzb0zz__processz00(obj_t BgL_envz00_1845, obj_t BgL_commandz00_1846, obj_t BgL_restz00_1847)
{
{ /* Llib/process.scm 212 */
{ /* Llib/process.scm 213 */
 obj_t BgL_auxz00_2210;
if(
STRINGP(BgL_commandz00_1846))
{ /* Llib/process.scm 213 */
BgL_auxz00_2210 = BgL_commandz00_1846
; }  else 
{ 
 obj_t BgL_auxz00_2213;
BgL_auxz00_2213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(9491L), BGl_string1720z00zz__processz00, BGl_string1721z00zz__processz00, BgL_commandz00_1846); 
FAILURE(BgL_auxz00_2213,BFALSE,BFALSE);} 
return 
BGl_runzd2processzd2zz__processz00(BgL_auxz00_2210, BgL_restz00_1847);} } 

}



/* close-process-ports */
BGL_EXPORTED_DEF obj_t BGl_closezd2processzd2portsz00zz__processz00(obj_t BgL_procz00_18)
{
{ /* Llib/process.scm 273 */
{ /* Llib/process.scm 274 */
 bool_t BgL_test1829z00_2218;
{ /* Llib/process.scm 274 */
 obj_t BgL_tmpz00_2219;
BgL_tmpz00_2219 = 
PROCESS_INPUT_PORT(BgL_procz00_18); 
BgL_test1829z00_2218 = 
OUTPUT_PORTP(BgL_tmpz00_2219); } 
if(BgL_test1829z00_2218)
{ /* Llib/process.scm 275 */
 obj_t BgL_arg1201z00_1176;
BgL_arg1201z00_1176 = 
PROCESS_INPUT_PORT(BgL_procz00_18); 
{ /* Llib/process.scm 275 */
 obj_t BgL_portz00_1621;
if(
OUTPUT_PORTP(BgL_arg1201z00_1176))
{ /* Llib/process.scm 275 */
BgL_portz00_1621 = BgL_arg1201z00_1176; }  else 
{ 
 obj_t BgL_auxz00_2225;
BgL_auxz00_2225 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(11236L), BGl_string1722z00zz__processz00, BGl_string1723z00zz__processz00, BgL_arg1201z00_1176); 
FAILURE(BgL_auxz00_2225,BFALSE,BFALSE);} 
bgl_close_output_port(BgL_portz00_1621); } }  else 
{ /* Llib/process.scm 274 */BFALSE; } } 
{ /* Llib/process.scm 276 */
 bool_t BgL_test1831z00_2230;
{ /* Llib/process.scm 276 */
 obj_t BgL_tmpz00_2231;
BgL_tmpz00_2231 = 
PROCESS_ERROR_PORT(BgL_procz00_18); 
BgL_test1831z00_2230 = 
INPUT_PORTP(BgL_tmpz00_2231); } 
if(BgL_test1831z00_2230)
{ /* Llib/process.scm 277 */
 obj_t BgL_arg1206z00_1180;
BgL_arg1206z00_1180 = 
PROCESS_ERROR_PORT(BgL_procz00_18); 
{ /* Llib/process.scm 277 */
 obj_t BgL_portz00_1624;
if(
INPUT_PORTP(BgL_arg1206z00_1180))
{ /* Llib/process.scm 277 */
BgL_portz00_1624 = BgL_arg1206z00_1180; }  else 
{ 
 obj_t BgL_auxz00_2237;
BgL_auxz00_2237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(11336L), BGl_string1722z00zz__processz00, BGl_string1724z00zz__processz00, BgL_arg1206z00_1180); 
FAILURE(BgL_auxz00_2237,BFALSE,BFALSE);} 
bgl_close_input_port(BgL_portz00_1624); } }  else 
{ /* Llib/process.scm 276 */BFALSE; } } 
{ /* Llib/process.scm 278 */
 bool_t BgL_test1833z00_2242;
{ /* Llib/process.scm 278 */
 obj_t BgL_tmpz00_2243;
BgL_tmpz00_2243 = 
PROCESS_OUTPUT_PORT(BgL_procz00_18); 
BgL_test1833z00_2242 = 
INPUT_PORTP(BgL_tmpz00_2243); } 
if(BgL_test1833z00_2242)
{ /* Llib/process.scm 279 */
 obj_t BgL_arg1212z00_1184;
BgL_arg1212z00_1184 = 
PROCESS_OUTPUT_PORT(BgL_procz00_18); 
{ /* Llib/process.scm 279 */
 obj_t BgL_portz00_1627;
if(
INPUT_PORTP(BgL_arg1212z00_1184))
{ /* Llib/process.scm 279 */
BgL_portz00_1627 = BgL_arg1212z00_1184; }  else 
{ 
 obj_t BgL_auxz00_2249;
BgL_auxz00_2249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(11438L), BGl_string1722z00zz__processz00, BGl_string1724z00zz__processz00, BgL_arg1212z00_1184); 
FAILURE(BgL_auxz00_2249,BFALSE,BFALSE);} 
return 
bgl_close_input_port(BgL_portz00_1627);} }  else 
{ /* Llib/process.scm 278 */
return BFALSE;} } } 

}



/* &close-process-ports */
obj_t BGl_z62closezd2processzd2portsz62zz__processz00(obj_t BgL_envz00_1848, obj_t BgL_procz00_1849)
{
{ /* Llib/process.scm 273 */
{ /* Llib/process.scm 275 */
 obj_t BgL_auxz00_2254;
if(
PROCESSP(BgL_procz00_1849))
{ /* Llib/process.scm 275 */
BgL_auxz00_2254 = BgL_procz00_1849
; }  else 
{ 
 obj_t BgL_auxz00_2257;
BgL_auxz00_2257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(11238L), BGl_string1725z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1849); 
FAILURE(BgL_auxz00_2257,BFALSE,BFALSE);} 
return 
BGl_closezd2processzd2portsz00zz__processz00(BgL_auxz00_2254);} } 

}



/* unregister-process */
BGL_EXPORTED_DEF obj_t BGl_unregisterzd2processzd2zz__processz00(obj_t BgL_procz00_19)
{
{ /* Llib/process.scm 284 */
BGL_TAIL return 
c_unregister_process(BgL_procz00_19);} 

}



/* &unregister-process */
obj_t BGl_z62unregisterzd2processzb0zz__processz00(obj_t BgL_envz00_1850, obj_t BgL_procz00_1851)
{
{ /* Llib/process.scm 284 */
{ /* Llib/process.scm 285 */
 obj_t BgL_auxz00_2263;
if(
PROCESSP(BgL_procz00_1851))
{ /* Llib/process.scm 285 */
BgL_auxz00_2263 = BgL_procz00_1851
; }  else 
{ 
 obj_t BgL_auxz00_2266;
BgL_auxz00_2266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1683z00zz__processz00, 
BINT(11710L), BGl_string1726z00zz__processz00, BGl_string1685z00zz__processz00, BgL_procz00_1851); 
FAILURE(BgL_auxz00_2266,BFALSE,BFALSE);} 
return 
BGl_unregisterzd2processzd2zz__processz00(BgL_auxz00_2263);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__processz00(void)
{
{ /* Llib/process.scm 15 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__processz00(void)
{
{ /* Llib/process.scm 15 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__processz00(void)
{
{ /* Llib/process.scm 15 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__processz00(void)
{
{ /* Llib/process.scm 15 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1727z00zz__processz00));} 

}

#ifdef __cplusplus
}
#endif
