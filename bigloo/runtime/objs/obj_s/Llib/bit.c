/*===========================================================================*/
/*   (Llib/bit.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/bit.scm -indent -o objs/obj_s/Llib/bit.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BIT_TYPE_DEFINITIONS
#define BGL___BIT_TYPE_DEFINITIONS
#endif // BGL___BIT_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL long BGl_bitzd2andzd2zz__bitz00(long, long);
static obj_t BGl_z62bitzd2oru8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2andu16zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint16_t BGl_bitzd2xoru16zd2zz__bitz00(uint16_t, uint16_t);
static obj_t BGl_z62bitzd2xors32zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2lshs8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2andzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_bitzd2xorzd2zz__bitz00(long, long);
BGL_EXPORTED_DECL int8_t BGl_bitzd2ands8zd2zz__bitz00(int8_t, int8_t);
static obj_t BGl_z62bitzd2xorzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshu32zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2ors64zb0zz__bitz00(obj_t, obj_t, obj_t);
extern obj_t bgl_bignum_and(obj_t, obj_t);
static obj_t BGl_z62bitzd2lshbxzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2rshelongzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bitzd2andbxzd2zz__bitz00(obj_t, obj_t);
extern obj_t bgl_bignum_or(obj_t, obj_t);
static obj_t BGl_z62bitzd2ands64zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int64_t BGl_bitzd2xors64zd2zz__bitz00(int64_t, int64_t);
BGL_EXPORTED_DECL int32_t BGl_bitzd2ors32zd2zz__bitz00(int32_t, int32_t);
static obj_t BGl_z62bitzd2xorllongzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2oru32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int16_t BGl_bitzd2lshs16zd2zz__bitz00(int16_t, long);
BGL_EXPORTED_DECL int32_t BGl_bitzd2rshs32zd2zz__bitz00(int32_t, long);
static obj_t BGl_requirezd2initializa7ationz75zz__bitz00 = BUNSPEC;
static obj_t BGl_z62bitzd2urshzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int32_t BGl_bitzd2nots32zd2zz__bitz00(int32_t);
static obj_t BGl_z62bitzd2rshs16zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2nots16zb0zz__bitz00(obj_t, obj_t);
static obj_t BGl_z62bitzd2andu32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2xoru32zd2zz__bitz00(uint32_t, uint32_t);
static obj_t BGl_z62bitzd2lshu8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2xoru16zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2orelongzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int16_t BGl_bitzd2urshs16zd2zz__bitz00(int16_t, long);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2andu8zd2zz__bitz00(uint8_t, uint8_t);
BGL_EXPORTED_DECL long BGl_bitzd2rshelongzd2zz__bitz00(long, long);
static obj_t BGl_z62bitzd2lshllongzb0zz__bitz00(obj_t, obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2andu64zd2zz__bitz00(uint64_t, uint64_t);
static obj_t BGl_z62bitzd2andllongzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_bitzd2xorllongzd2zz__bitz00(BGL_LONGLONG_T, BGL_LONGLONG_T);
static obj_t BGl_z62bitzd2xors64zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshelongzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint16_t BGl_bitzd2oru16zd2zz__bitz00(uint16_t, uint16_t);
BGL_EXPORTED_DECL int32_t BGl_bitzd2lshs32zd2zz__bitz00(int32_t, long);
BGL_EXPORTED_DECL obj_t BGl_bitzd2maskbxzd2zz__bitz00(obj_t, long);
static obj_t BGl_z62bitzd2lshs16zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshu64zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int16_t BGl_bitzd2rshu16zd2zz__bitz00(int16_t, long);
static obj_t BGl_z62bitzd2rshs32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint16_t BGl_bitzd2notu16zd2zz__bitz00(uint16_t);
static obj_t BGl_z62bitzd2nots32zb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL int8_t BGl_bitzd2lshs8zd2zz__bitz00(int8_t, long);
extern obj_t bgl_bignum_lsh(obj_t, long);
static obj_t BGl_z62bitzd2xoru32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int32_t BGl_bitzd2urshs32zd2zz__bitz00(int32_t, long);
BGL_EXPORTED_DECL int64_t BGl_bitzd2ors64zd2zz__bitz00(int64_t, int64_t);
BGL_EXPORTED_DECL obj_t BGl_bitzd2lshbxzd2zz__bitz00(obj_t, long);
static obj_t BGl_z62bitzd2nots8zb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_bitzd2lshllongzd2zz__bitz00(BGL_LONGLONG_T, long);
static obj_t BGl_z62bitzd2oru64zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int64_t BGl_bitzd2rshs64zd2zz__bitz00(int64_t, long);
BGL_EXPORTED_DECL int64_t BGl_bitzd2nots64zd2zz__bitz00(int64_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_bitzd2andllongzd2zz__bitz00(BGL_LONGLONG_T, BGL_LONGLONG_T);
static obj_t BGl_z62bitzd2notbxzb0zz__bitz00(obj_t, obj_t);
static obj_t BGl_z62bitzd2notllongzb0zz__bitz00(obj_t, obj_t);
static obj_t BGl_z62bitzd2xorelongzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2andu64zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2xoru64zd2zz__bitz00(uint64_t, uint64_t);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2oru32zd2zz__bitz00(uint32_t, uint32_t);
BGL_EXPORTED_DECL uint16_t BGl_bitzd2lshu16zd2zz__bitz00(uint16_t, long);
static obj_t BGl_z62bitzd2lshs32zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2orzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2rshu32zd2zz__bitz00(uint32_t, long);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2notu32zd2zz__bitz00(uint32_t);
static obj_t BGl_z62bitzd2maskbxzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2rshu16zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2notu16zb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2lshu8zd2zz__bitz00(uint8_t, long);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_bitzd2orllongzd2zz__bitz00(BGL_LONGLONG_T, BGL_LONGLONG_T);
BGL_EXPORTED_DECL uint16_t BGl_bitzd2urshu16zd2zz__bitz00(uint16_t, long);
static obj_t BGl_importedzd2moduleszd2initz00zz__bitz00(void);
static obj_t BGl_z62bitzd2lshelongzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__bitz00(void);
static obj_t BGl_z62bitzd2notu8zb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL int64_t BGl_bitzd2lshs64zd2zz__bitz00(int64_t, long);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_bitzd2notllongzd2zz__bitz00(BGL_LONGLONG_T);
static obj_t BGl_z62bitzd2andelongzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_bitzd2xorelongzd2zz__bitz00(long, long);
static obj_t BGl_z62bitzd2rshs64zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2nots64zb0zz__bitz00(obj_t, obj_t);
static obj_t BGl_z62bitzd2xoru64zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int64_t BGl_bitzd2urshs64zd2zz__bitz00(int64_t, long);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2lshu32zd2zz__bitz00(uint32_t, long);
static obj_t BGl_z62bitzd2lshu16zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int8_t BGl_bitzd2ors8zd2zz__bitz00(int8_t, int8_t);
static obj_t BGl_z62bitzd2rshu32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_bitzd2rshzd2zz__bitz00(long, long);
static obj_t BGl_z62bitzd2notu32zb0zz__bitz00(obj_t, obj_t);
static obj_t BGl_z62bitzd2rshzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bitzd2orbxzd2zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_bitzd2lshelongzd2zz__bitz00(long, long);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2urshu32zd2zz__bitz00(uint32_t, long);
BGL_EXPORTED_DECL int8_t BGl_bitzd2nots8zd2zz__bitz00(int8_t);
static obj_t BGl_z62bitzd2rshs8zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2oru64zd2zz__bitz00(uint64_t, uint64_t);
extern obj_t bgl_bignum_not(obj_t);
BGL_EXPORTED_DECL long BGl_bitzd2andelongzd2zz__bitz00(long, long);
static obj_t BGl_z62bitzd2lshs64zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2xors8zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bitzd2notbxzd2zz__bitz00(obj_t);
static obj_t BGl_z62bitzd2notelongzb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2rshu64zd2zz__bitz00(uint64_t, long);
static obj_t BGl_z62bitzd2rshbxzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2notu64zd2zz__bitz00(uint64_t);
static obj_t BGl_z62bitzd2xorbxzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2lshu32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int8_t BGl_bitzd2urshs8zd2zz__bitz00(int8_t, long);
BGL_EXPORTED_DECL int16_t BGl_bitzd2ands16zd2zz__bitz00(int16_t, int16_t);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2oru8zd2zz__bitz00(uint8_t, uint8_t);
BGL_EXPORTED_DECL long BGl_bitzd2orelongzd2zz__bitz00(long, long);
extern obj_t bgl_bignum_mask(obj_t, long);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2notu8zd2zz__bitz00(uint8_t);
static obj_t BGl_z62bitzd2rshu8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshs16zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2lshu64zd2zz__bitz00(uint64_t, long);
BGL_EXPORTED_DECL long BGl_bitzd2notelongzd2zz__bitz00(long);
static obj_t BGl_z62bitzd2xoru8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2rshu64zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2notu64zb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL unsigned BGL_LONGLONG_T BGl_bitzd2urshllongzd2zz__bitz00(unsigned BGL_LONGLONG_T, long);
static obj_t BGl_z62bitzd2ors16zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_bitzd2urshu64zd2zz__bitz00(uint64_t, long);
BGL_EXPORTED_DECL long BGl_bitzd2orzd2zz__bitz00(long, long);
BGL_EXPORTED_DECL unsigned long BGl_bitzd2urshzd2zz__bitz00(unsigned long, long);
BGL_EXPORTED_DECL long BGl_bitzd2lshzd2zz__bitz00(long, long);
BGL_EXPORTED_DECL int32_t BGl_bitzd2ands32zd2zz__bitz00(int32_t, int32_t);
static obj_t BGl_z62bitzd2lshzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2urshu8zd2zz__bitz00(uint8_t, long);
BGL_EXPORTED_DECL long BGl_bitzd2notzd2zz__bitz00(long);
static obj_t BGl_z62bitzd2urshs8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2ands16zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int16_t BGl_bitzd2xors16zd2zz__bitz00(int16_t, int16_t);
static obj_t BGl_z62bitzd2notzb0zz__bitz00(obj_t, obj_t);
BGL_EXPORTED_DECL int8_t BGl_bitzd2rshs8zd2zz__bitz00(int8_t, long);
extern obj_t bgl_bignum_rsh(obj_t, long);
static obj_t BGl_z62bitzd2urshs32zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2ands8zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int8_t BGl_bitzd2xors8zd2zz__bitz00(int8_t, int8_t);
BGL_EXPORTED_DECL obj_t BGl_bitzd2rshbxzd2zz__bitz00(obj_t, long);
extern obj_t bgl_bignum_xor(obj_t, obj_t);
static obj_t BGl_z62bitzd2lshu64zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2rshllongzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2andbxzb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bitzd2xorbxzd2zz__bitz00(obj_t, obj_t);
static obj_t BGl_z62bitzd2ors32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint16_t BGl_bitzd2andu16zd2zz__bitz00(uint16_t, uint16_t);
static obj_t BGl_z62bitzd2ands32zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int32_t BGl_bitzd2xors32zd2zz__bitz00(int32_t, int32_t);
static obj_t BGl_z62bitzd2ors8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshu8zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2xors16zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2rshu8zd2zz__bitz00(uint8_t, long);
static obj_t BGl_z62bitzd2orbxzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2orllongzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshu16zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2andu8zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint8_t BGl_bitzd2xoru8zd2zz__bitz00(uint8_t, uint8_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_bitzd2rshllongzd2zz__bitz00(BGL_LONGLONG_T, long);
BGL_EXPORTED_DECL int64_t BGl_bitzd2ands64zd2zz__bitz00(int64_t, int64_t);
BGL_EXPORTED_DECL unsigned long BGl_bitzd2urshelongzd2zz__bitz00(unsigned long, long);
BGL_EXPORTED_DECL int16_t BGl_bitzd2ors16zd2zz__bitz00(int16_t, int16_t);
static obj_t BGl_z62bitzd2oru16zb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshllongzb0zz__bitz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bitzd2urshs64zb0zz__bitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int16_t BGl_bitzd2rshs16zd2zz__bitz00(int16_t, long);
BGL_EXPORTED_DECL int16_t BGl_bitzd2nots16zd2zz__bitz00(int16_t);
BGL_EXPORTED_DECL uint32_t BGl_bitzd2andu32zd2zz__bitz00(uint32_t, uint32_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2oru32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2oru32za7b1786za7, BGl_z62bitzd2oru32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshu16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshu16za71787za7, BGl_z62bitzd2lshu16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshu32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshu32za71788za7, BGl_z62bitzd2rshu32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notu32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notu32za71789za7, BGl_z62bitzd2notu32zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2oru8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2oru8za7b01790za7, BGl_z62bitzd2oru8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xors32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xors32za71791za7, BGl_z62bitzd2xors32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2orelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2orelong1792z00, BGl_z62bitzd2orelongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshs16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshs161793z00, BGl_z62bitzd2urshs16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notu8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notu8za7b1794za7, BGl_z62bitzd2notu8zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andu8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andu8za7b1795za7, BGl_z62bitzd2andu8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshu64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshu64za71796za7, BGl_z62bitzd2lshu64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notelon1797z00, BGl_z62bitzd2notelongzb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshllon1798z00, BGl_z62bitzd2rshllongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshllo1799z00, BGl_z62bitzd2urshllongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshs64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshs641800z00, BGl_z62bitzd2urshs64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshza7b01801za7, BGl_z62bitzd2urshzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andu32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andu32za71802za7, BGl_z62bitzd2andu32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xoru16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xoru16za71803za7, BGl_z62bitzd2xoru16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xorzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xorza7b0za71804z00, BGl_z62bitzd2xorzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2orbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2orbxza7b01805za7, BGl_z62bitzd2orbxzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notbxza7b1806za7, BGl_z62bitzd2notbxzb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andbxza7b1807za7, BGl_z62bitzd2andbxzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xoru64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xoru64za71808za7, BGl_z62bitzd2xoru64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ors32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ors32za7b1809za7, BGl_z62bitzd2ors32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshs16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshs16za71810za7, BGl_z62bitzd2lshs16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshs32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshs32za71811za7, BGl_z62bitzd2rshs32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2nots32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2nots32za71812za7, BGl_z62bitzd2nots32zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshu8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshu8za7b1813za7, BGl_z62bitzd2lshu8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshu8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshu8za7b1814za7, BGl_z62bitzd2rshu8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshs64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshs64za71815za7, BGl_z62bitzd2lshs64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xoru8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xoru8za7b1816za7, BGl_z62bitzd2xoru8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xorllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xorllon1817z00, BGl_z62bitzd2xorllongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2oru16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2oru16za7b1818za7, BGl_z62bitzd2oru16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshza7b0za71819z00, BGl_z62bitzd2lshzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshu16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshu16za71820za7, BGl_z62bitzd2rshu16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ands32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ands32za71821za7, BGl_z62bitzd2ands32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ors8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ors8za7b01822za7, BGl_z62bitzd2ors8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notu16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notu16za71823za7, BGl_z62bitzd2notu16zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshu8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshu8za71824za7, BGl_z62bitzd2urshu8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xors16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xors16za71825za7, BGl_z62bitzd2xors16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshelon1826z00, BGl_z62bitzd2lshelongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshu32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshu321827z00, BGl_z62bitzd2urshu32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2nots8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2nots8za7b1828za7, BGl_z62bitzd2nots8zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1700z00zz__bitz00, BgL_bgl_string1700za700za7za7_1829za7, "bint16", 6 );
DEFINE_STRING( BGl_string1701z00zz__bitz00, BgL_bgl_string1701za700za7za7_1830za7, "&bit-oru16", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ands8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ands8za7b1831za7, BGl_z62bitzd2ands8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1702z00zz__bitz00, BgL_bgl_string1702za700za7za7_1832za7, "buint16", 7 );
DEFINE_STRING( BGl_string1703z00zz__bitz00, BgL_bgl_string1703za700za7za7_1833za7, "&bit-ors32", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2oru64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2oru64za7b1834za7, BGl_z62bitzd2oru64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1704z00zz__bitz00, BgL_bgl_string1704za700za7za7_1835za7, "bint32", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshbxza7b1836za7, BGl_z62bitzd2lshbxzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1705z00zz__bitz00, BgL_bgl_string1705za700za7za7_1837za7, "&bit-oru32", 10 );
DEFINE_STRING( BGl_string1706z00zz__bitz00, BgL_bgl_string1706za700za7za7_1838za7, "buint32", 7 );
DEFINE_STRING( BGl_string1707z00zz__bitz00, BgL_bgl_string1707za700za7za7_1839za7, "&bit-ors64", 10 );
DEFINE_STRING( BGl_string1708z00zz__bitz00, BgL_bgl_string1708za700za7za7_1840za7, "bint64", 6 );
DEFINE_STRING( BGl_string1709z00zz__bitz00, BgL_bgl_string1709za700za7za7_1841za7, "&bit-oru64", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshbxza7b1842za7, BGl_z62bitzd2rshbxzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andelon1843z00, BGl_z62bitzd2andelongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshu64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshu64za71844za7, BGl_z62bitzd2rshu64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notu64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notu64za71845za7, BGl_z62bitzd2notu64zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xorbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xorbxza7b1846za7, BGl_z62bitzd2xorbxzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xors64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xors64za71847za7, BGl_z62bitzd2xors64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1710z00zz__bitz00, BgL_bgl_string1710za700za7za7_1848za7, "buint64", 7 );
DEFINE_STRING( BGl_string1711z00zz__bitz00, BgL_bgl_string1711za700za7za7_1849za7, "&bit-orbx", 9 );
DEFINE_STRING( BGl_string1712z00zz__bitz00, BgL_bgl_string1712za700za7za7_1850za7, "bignum", 6 );
DEFINE_STRING( BGl_string1713z00zz__bitz00, BgL_bgl_string1713za700za7za7_1851za7, "&bit-and", 8 );
DEFINE_STRING( BGl_string1714z00zz__bitz00, BgL_bgl_string1714za700za7za7_1852za7, "&bit-andelong", 13 );
DEFINE_STRING( BGl_string1715z00zz__bitz00, BgL_bgl_string1715za700za7za7_1853za7, "&bit-andllong", 13 );
DEFINE_STRING( BGl_string1716z00zz__bitz00, BgL_bgl_string1716za700za7za7_1854za7, "&bit-ands8", 10 );
DEFINE_STRING( BGl_string1717z00zz__bitz00, BgL_bgl_string1717za700za7za7_1855za7, "&bit-andu8", 10 );
DEFINE_STRING( BGl_string1718z00zz__bitz00, BgL_bgl_string1718za700za7za7_1856za7, "&bit-ands16", 11 );
DEFINE_STRING( BGl_string1719z00zz__bitz00, BgL_bgl_string1719za700za7za7_1857za7, "&bit-andu16", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andu16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andu16za71858za7, BGl_z62bitzd2andu16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2orllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2orllong1859z00, BGl_z62bitzd2orllongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1720z00zz__bitz00, BgL_bgl_string1720za700za7za7_1860za7, "&bit-ands32", 11 );
DEFINE_STRING( BGl_string1721z00zz__bitz00, BgL_bgl_string1721za700za7za7_1861za7, "&bit-andu32", 11 );
DEFINE_STRING( BGl_string1722z00zz__bitz00, BgL_bgl_string1722za700za7za7_1862za7, "&bit-ands64", 11 );
DEFINE_STRING( BGl_string1723z00zz__bitz00, BgL_bgl_string1723za700za7za7_1863za7, "&bit-andu64", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshelon1864z00, BGl_z62bitzd2rshelongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1724z00zz__bitz00, BgL_bgl_string1724za700za7za7_1865za7, "&bit-andbx", 10 );
DEFINE_STRING( BGl_string1725z00zz__bitz00, BgL_bgl_string1725za700za7za7_1866za7, "&bit-maskbx", 11 );
DEFINE_STRING( BGl_string1726z00zz__bitz00, BgL_bgl_string1726za700za7za7_1867za7, "&bit-xor", 8 );
DEFINE_STRING( BGl_string1727z00zz__bitz00, BgL_bgl_string1727za700za7za7_1868za7, "&bit-xorelong", 13 );
DEFINE_STRING( BGl_string1728z00zz__bitz00, BgL_bgl_string1728za700za7za7_1869za7, "&bit-xorllong", 13 );
DEFINE_STRING( BGl_string1729z00zz__bitz00, BgL_bgl_string1729za700za7za7_1870za7, "&bit-xors8", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notllon1871z00, BGl_z62bitzd2notllongzb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andu64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andu64za71872za7, BGl_z62bitzd2andu64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshelo1873z00, BGl_z62bitzd2urshelongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ors16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ors16za7b1874za7, BGl_z62bitzd2ors16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1730z00zz__bitz00, BgL_bgl_string1730za700za7za7_1875za7, "&bit-xoru8", 10 );
DEFINE_STRING( BGl_string1731z00zz__bitz00, BgL_bgl_string1731za700za7za7_1876za7, "&bit-xors16", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshu32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshu32za71877za7, BGl_z62bitzd2lshu32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1732z00zz__bitz00, BgL_bgl_string1732za700za7za7_1878za7, "&bit-xoru16", 11 );
DEFINE_STRING( BGl_string1733z00zz__bitz00, BgL_bgl_string1733za700za7za7_1879za7, "&bit-xors32", 11 );
DEFINE_STRING( BGl_string1734z00zz__bitz00, BgL_bgl_string1734za700za7za7_1880za7, "&bit-xoru32", 11 );
DEFINE_STRING( BGl_string1735z00zz__bitz00, BgL_bgl_string1735za700za7za7_1881za7, "&bit-xors64", 11 );
DEFINE_STRING( BGl_string1736z00zz__bitz00, BgL_bgl_string1736za700za7za7_1882za7, "&bit-xoru64", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2maskbxzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2maskbxza71883za7, BGl_z62bitzd2maskbxzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1737z00zz__bitz00, BgL_bgl_string1737za700za7za7_1884za7, "&bit-xorbx", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshs16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshs16za71885za7, BGl_z62bitzd2rshs16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1738z00zz__bitz00, BgL_bgl_string1738za700za7za7_1886za7, "&bit-not", 8 );
DEFINE_STRING( BGl_string1739z00zz__bitz00, BgL_bgl_string1739za700za7za7_1887za7, "&bit-notelong", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2nots16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2nots16za71888za7, BGl_z62bitzd2nots16zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshza7b0za71889z00, BGl_z62bitzd2rshzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshs8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshs8za7b1890za7, BGl_z62bitzd2lshs8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andza7b0za71891z00, BGl_z62bitzd2andzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshs8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshs8za7b1892za7, BGl_z62bitzd2rshs8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshs32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshs321893z00, BGl_z62bitzd2urshs32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ors64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ors64za7b1894za7, BGl_z62bitzd2ors64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1740z00zz__bitz00, BgL_bgl_string1740za700za7za7_1895za7, "&bit-notllong", 13 );
DEFINE_STRING( BGl_string1741z00zz__bitz00, BgL_bgl_string1741za700za7za7_1896za7, "&bit-nots8", 10 );
DEFINE_STRING( BGl_string1742z00zz__bitz00, BgL_bgl_string1742za700za7za7_1897za7, "&bit-notu8", 10 );
DEFINE_STRING( BGl_string1743z00zz__bitz00, BgL_bgl_string1743za700za7za7_1898za7, "&bit-nots16", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xors8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xors8za7b1899za7, BGl_z62bitzd2xors8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1744z00zz__bitz00, BgL_bgl_string1744za700za7za7_1900za7, "&bit-notu16", 11 );
DEFINE_STRING( BGl_string1745z00zz__bitz00, BgL_bgl_string1745za700za7za7_1901za7, "&bit-nots32", 11 );
DEFINE_STRING( BGl_string1746z00zz__bitz00, BgL_bgl_string1746za700za7za7_1902za7, "&bit-notu32", 11 );
DEFINE_STRING( BGl_string1747z00zz__bitz00, BgL_bgl_string1747za700za7za7_1903za7, "&bit-nots64", 11 );
DEFINE_STRING( BGl_string1748z00zz__bitz00, BgL_bgl_string1748za700za7za7_1904za7, "&bit-notu64", 11 );
DEFINE_STRING( BGl_string1749z00zz__bitz00, BgL_bgl_string1749za700za7za7_1905za7, "&bit-notbx", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2rshs64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2rshs64za71906za7, BGl_z62bitzd2rshs64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2nots64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2nots64za71907za7, BGl_z62bitzd2nots64zb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1750z00zz__bitz00, BgL_bgl_string1750za700za7za7_1908za7, "&bit-rsh", 8 );
DEFINE_STRING( BGl_string1751z00zz__bitz00, BgL_bgl_string1751za700za7za7_1909za7, "&bit-rshelong", 13 );
DEFINE_STRING( BGl_string1752z00zz__bitz00, BgL_bgl_string1752za700za7za7_1910za7, "&bit-rshllong", 13 );
DEFINE_STRING( BGl_string1753z00zz__bitz00, BgL_bgl_string1753za700za7za7_1911za7, "&bit-rshs8", 10 );
DEFINE_STRING( BGl_string1754z00zz__bitz00, BgL_bgl_string1754za700za7za7_1912za7, "&bit-rshu8", 10 );
DEFINE_STRING( BGl_string1755z00zz__bitz00, BgL_bgl_string1755za700za7za7_1913za7, "&bit-rshs16", 11 );
DEFINE_STRING( BGl_string1756z00zz__bitz00, BgL_bgl_string1756za700za7za7_1914za7, "&bit-rshu16", 11 );
DEFINE_STRING( BGl_string1757z00zz__bitz00, BgL_bgl_string1757za700za7za7_1915za7, "&bit-rshs32", 11 );
DEFINE_STRING( BGl_string1758z00zz__bitz00, BgL_bgl_string1758za700za7za7_1916za7, "&bit-rshu32", 11 );
DEFINE_STRING( BGl_string1759z00zz__bitz00, BgL_bgl_string1759za700za7za7_1917za7, "&bit-rshs64", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshs8zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshs8za71918za7, BGl_z62bitzd2urshs8zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ands16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ands16za71919za7, BGl_z62bitzd2ands16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xoru32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xoru32za71920za7, BGl_z62bitzd2xoru32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshu16zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshu161921z00, BGl_z62bitzd2urshu16zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1760z00zz__bitz00, BgL_bgl_string1760za700za7za7_1922za7, "&bit-rshu64", 11 );
DEFINE_STRING( BGl_string1761z00zz__bitz00, BgL_bgl_string1761za700za7za7_1923za7, "&bit-rshbx", 10 );
DEFINE_STRING( BGl_string1762z00zz__bitz00, BgL_bgl_string1762za700za7za7_1924za7, "&bit-ursh", 9 );
DEFINE_STRING( BGl_string1763z00zz__bitz00, BgL_bgl_string1763za700za7za7_1925za7, "&bit-urshelong", 14 );
DEFINE_STRING( BGl_string1764z00zz__bitz00, BgL_bgl_string1764za700za7za7_1926za7, "&bit-urshllong", 14 );
DEFINE_STRING( BGl_string1765z00zz__bitz00, BgL_bgl_string1765za700za7za7_1927za7, "&bit-urshu8", 11 );
DEFINE_STRING( BGl_string1766z00zz__bitz00, BgL_bgl_string1766za700za7za7_1928za7, "&bit-urshs8", 11 );
DEFINE_STRING( BGl_string1767z00zz__bitz00, BgL_bgl_string1767za700za7za7_1929za7, "&bit-urshs16", 12 );
DEFINE_STRING( BGl_string1768z00zz__bitz00, BgL_bgl_string1768za700za7za7_1930za7, "&bit-urshu16", 12 );
DEFINE_STRING( BGl_string1769z00zz__bitz00, BgL_bgl_string1769za700za7za7_1931za7, "&bit-urshs32", 12 );
DEFINE_STRING( BGl_string1688z00zz__bitz00, BgL_bgl_string1688za700za7za7_1932za7, "/tmp/bigloo/runtime/Llib/bit.scm", 32 );
DEFINE_STRING( BGl_string1689z00zz__bitz00, BgL_bgl_string1689za700za7za7_1933za7, "&bit-or", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2xorelongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2xorelon1934z00, BGl_z62bitzd2xorelongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2ands64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2ands64za71935za7, BGl_z62bitzd2ands64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshs32zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshs32za71936za7, BGl_z62bitzd2lshs32zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2urshu64zd2envz00zz__bitz00, BgL_bgl_za762bitza7d2urshu641937z00, BGl_z62bitzd2urshu64zb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2orzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2orza7b0za7za71938za7, BGl_z62bitzd2orzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1770z00zz__bitz00, BgL_bgl_string1770za700za7za7_1939za7, "&bit-urshu32", 12 );
DEFINE_STRING( BGl_string1771z00zz__bitz00, BgL_bgl_string1771za700za7za7_1940za7, "&bit-urshs64", 12 );
DEFINE_STRING( BGl_string1690z00zz__bitz00, BgL_bgl_string1690za700za7za7_1941za7, "bint", 4 );
DEFINE_STRING( BGl_string1772z00zz__bitz00, BgL_bgl_string1772za700za7za7_1942za7, "&bit-urshu64", 12 );
DEFINE_STRING( BGl_string1691z00zz__bitz00, BgL_bgl_string1691za700za7za7_1943za7, "&bit-orelong", 12 );
DEFINE_STRING( BGl_string1773z00zz__bitz00, BgL_bgl_string1773za700za7za7_1944za7, "&bit-lsh", 8 );
DEFINE_STRING( BGl_string1692z00zz__bitz00, BgL_bgl_string1692za700za7za7_1945za7, "belong", 6 );
DEFINE_STRING( BGl_string1774z00zz__bitz00, BgL_bgl_string1774za700za7za7_1946za7, "&bit-lshelong", 13 );
DEFINE_STRING( BGl_string1693z00zz__bitz00, BgL_bgl_string1693za700za7za7_1947za7, "&bit-orllong", 12 );
DEFINE_STRING( BGl_string1775z00zz__bitz00, BgL_bgl_string1775za700za7za7_1948za7, "&bit-lshllong", 13 );
DEFINE_STRING( BGl_string1694z00zz__bitz00, BgL_bgl_string1694za700za7za7_1949za7, "bllong", 6 );
DEFINE_STRING( BGl_string1776z00zz__bitz00, BgL_bgl_string1776za700za7za7_1950za7, "&bit-lshs8", 10 );
DEFINE_STRING( BGl_string1695z00zz__bitz00, BgL_bgl_string1695za700za7za7_1951za7, "&bit-ors8", 9 );
DEFINE_STRING( BGl_string1777z00zz__bitz00, BgL_bgl_string1777za700za7za7_1952za7, "&bit-lshu8", 10 );
DEFINE_STRING( BGl_string1696z00zz__bitz00, BgL_bgl_string1696za700za7za7_1953za7, "bint8", 5 );
DEFINE_STRING( BGl_string1778z00zz__bitz00, BgL_bgl_string1778za700za7za7_1954za7, "&bit-lshs16", 11 );
DEFINE_STRING( BGl_string1697z00zz__bitz00, BgL_bgl_string1697za700za7za7_1955za7, "&bit-oru8", 9 );
DEFINE_STRING( BGl_string1779z00zz__bitz00, BgL_bgl_string1779za700za7za7_1956za7, "&bit-lshu16", 11 );
DEFINE_STRING( BGl_string1698z00zz__bitz00, BgL_bgl_string1698za700za7za7_1957za7, "buint8", 6 );
DEFINE_STRING( BGl_string1699z00zz__bitz00, BgL_bgl_string1699za700za7za7_1958za7, "&bit-ors16", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2notzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2notza7b0za71959z00, BGl_z62bitzd2notzb0zz__bitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2lshllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2lshllon1960z00, BGl_z62bitzd2lshllongzb0zz__bitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1780z00zz__bitz00, BgL_bgl_string1780za700za7za7_1961za7, "&bit-lshs32", 11 );
DEFINE_STRING( BGl_string1781z00zz__bitz00, BgL_bgl_string1781za700za7za7_1962za7, "&bit-lshu32", 11 );
DEFINE_STRING( BGl_string1782z00zz__bitz00, BgL_bgl_string1782za700za7za7_1963za7, "&bit-lshs64", 11 );
DEFINE_STRING( BGl_string1783z00zz__bitz00, BgL_bgl_string1783za700za7za7_1964za7, "&bit-lshu64", 11 );
DEFINE_STRING( BGl_string1784z00zz__bitz00, BgL_bgl_string1784za700za7za7_1965za7, "&bit-lshbx", 10 );
DEFINE_STRING( BGl_string1785z00zz__bitz00, BgL_bgl_string1785za700za7za7_1966za7, "__bit", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bitzd2andllongzd2envz00zz__bitz00, BgL_bgl_za762bitza7d2andllon1967z00, BGl_z62bitzd2andllongzb0zz__bitz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__bitz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long BgL_checksumz00_1717, char * BgL_fromz00_1718)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__bitz00))
{ 
BGl_requirezd2initializa7ationz75zz__bitz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__bitz00(); 
return 
BGl_importedzd2moduleszd2initz00zz__bitz00();}  else 
{ 
return BUNSPEC;} } 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__bitz00(void)
{
{ /* Llib/bit.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* bit-or */
BGL_EXPORTED_DEF long BGl_bitzd2orzd2zz__bitz00(long BgL_xz00_3, long BgL_yz00_4)
{
{ /* Llib/bit.scm 543 */
return 
(BgL_xz00_3 | BgL_yz00_4);} 

}



/* &bit-or */
obj_t BGl_z62bitzd2orzb0zz__bitz00(obj_t BgL_envz00_1211, obj_t BgL_xz00_1212, obj_t BgL_yz00_1213)
{
{ /* Llib/bit.scm 543 */
{ /* Llib/bit.scm 543 */
 long BgL_tmpz00_1726;
{ /* Llib/bit.scm 543 */
 long BgL_auxz00_1736; long BgL_auxz00_1727;
{ /* Llib/bit.scm 543 */
 obj_t BgL_tmpz00_1737;
if(
INTEGERP(BgL_yz00_1213))
{ /* Llib/bit.scm 543 */
BgL_tmpz00_1737 = BgL_yz00_1213
; }  else 
{ 
 obj_t BgL_auxz00_1740;
BgL_auxz00_1740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25198L), BGl_string1689z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1213); 
FAILURE(BgL_auxz00_1740,BFALSE,BFALSE);} 
BgL_auxz00_1736 = 
(long)CINT(BgL_tmpz00_1737); } 
{ /* Llib/bit.scm 543 */
 obj_t BgL_tmpz00_1728;
if(
INTEGERP(BgL_xz00_1212))
{ /* Llib/bit.scm 543 */
BgL_tmpz00_1728 = BgL_xz00_1212
; }  else 
{ 
 obj_t BgL_auxz00_1731;
BgL_auxz00_1731 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25198L), BGl_string1689z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1212); 
FAILURE(BgL_auxz00_1731,BFALSE,BFALSE);} 
BgL_auxz00_1727 = 
(long)CINT(BgL_tmpz00_1728); } 
BgL_tmpz00_1726 = 
BGl_bitzd2orzd2zz__bitz00(BgL_auxz00_1727, BgL_auxz00_1736); } 
return 
BINT(BgL_tmpz00_1726);} } 

}



/* bit-orelong */
BGL_EXPORTED_DEF long BGl_bitzd2orelongzd2zz__bitz00(long BgL_xz00_5, long BgL_yz00_6)
{
{ /* Llib/bit.scm 544 */
return 
(BgL_xz00_5 | BgL_yz00_6);} 

}



/* &bit-orelong */
obj_t BGl_z62bitzd2orelongzb0zz__bitz00(obj_t BgL_envz00_1214, obj_t BgL_xz00_1215, obj_t BgL_yz00_1216)
{
{ /* Llib/bit.scm 544 */
{ /* Llib/bit.scm 544 */
 long BgL_tmpz00_1748;
{ /* Llib/bit.scm 544 */
 long BgL_auxz00_1758; long BgL_auxz00_1749;
{ /* Llib/bit.scm 544 */
 obj_t BgL_tmpz00_1759;
if(
ELONGP(BgL_yz00_1216))
{ /* Llib/bit.scm 544 */
BgL_tmpz00_1759 = BgL_yz00_1216
; }  else 
{ 
 obj_t BgL_auxz00_1762;
BgL_auxz00_1762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25245L), BGl_string1691z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_yz00_1216); 
FAILURE(BgL_auxz00_1762,BFALSE,BFALSE);} 
BgL_auxz00_1758 = 
BELONG_TO_LONG(BgL_tmpz00_1759); } 
{ /* Llib/bit.scm 544 */
 obj_t BgL_tmpz00_1750;
if(
ELONGP(BgL_xz00_1215))
{ /* Llib/bit.scm 544 */
BgL_tmpz00_1750 = BgL_xz00_1215
; }  else 
{ 
 obj_t BgL_auxz00_1753;
BgL_auxz00_1753 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25245L), BGl_string1691z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1215); 
FAILURE(BgL_auxz00_1753,BFALSE,BFALSE);} 
BgL_auxz00_1749 = 
BELONG_TO_LONG(BgL_tmpz00_1750); } 
BgL_tmpz00_1748 = 
BGl_bitzd2orelongzd2zz__bitz00(BgL_auxz00_1749, BgL_auxz00_1758); } 
return 
make_belong(BgL_tmpz00_1748);} } 

}



/* bit-orllong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2orllongzd2zz__bitz00(BGL_LONGLONG_T BgL_xz00_7, BGL_LONGLONG_T BgL_yz00_8)
{
{ /* Llib/bit.scm 545 */
return 
(BgL_xz00_7 | BgL_yz00_8);} 

}



/* &bit-orllong */
obj_t BGl_z62bitzd2orllongzb0zz__bitz00(obj_t BgL_envz00_1217, obj_t BgL_xz00_1218, obj_t BgL_yz00_1219)
{
{ /* Llib/bit.scm 545 */
{ /* Llib/bit.scm 545 */
 BGL_LONGLONG_T BgL_tmpz00_1770;
{ /* Llib/bit.scm 545 */
 BGL_LONGLONG_T BgL_auxz00_1780; BGL_LONGLONG_T BgL_auxz00_1771;
{ /* Llib/bit.scm 545 */
 obj_t BgL_tmpz00_1781;
if(
LLONGP(BgL_yz00_1219))
{ /* Llib/bit.scm 545 */
BgL_tmpz00_1781 = BgL_yz00_1219
; }  else 
{ 
 obj_t BgL_auxz00_1784;
BgL_auxz00_1784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25297L), BGl_string1693z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_yz00_1219); 
FAILURE(BgL_auxz00_1784,BFALSE,BFALSE);} 
BgL_auxz00_1780 = 
BLLONG_TO_LLONG(BgL_tmpz00_1781); } 
{ /* Llib/bit.scm 545 */
 obj_t BgL_tmpz00_1772;
if(
LLONGP(BgL_xz00_1218))
{ /* Llib/bit.scm 545 */
BgL_tmpz00_1772 = BgL_xz00_1218
; }  else 
{ 
 obj_t BgL_auxz00_1775;
BgL_auxz00_1775 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25297L), BGl_string1693z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1218); 
FAILURE(BgL_auxz00_1775,BFALSE,BFALSE);} 
BgL_auxz00_1771 = 
BLLONG_TO_LLONG(BgL_tmpz00_1772); } 
BgL_tmpz00_1770 = 
BGl_bitzd2orllongzd2zz__bitz00(BgL_auxz00_1771, BgL_auxz00_1780); } 
return 
make_bllong(BgL_tmpz00_1770);} } 

}



/* bit-ors8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2ors8zd2zz__bitz00(int8_t BgL_xz00_9, int8_t BgL_yz00_10)
{
{ /* Llib/bit.scm 546 */
return 
(BgL_xz00_9 | BgL_yz00_10);} 

}



/* &bit-ors8 */
obj_t BGl_z62bitzd2ors8zb0zz__bitz00(obj_t BgL_envz00_1220, obj_t BgL_xz00_1221, obj_t BgL_yz00_1222)
{
{ /* Llib/bit.scm 546 */
{ /* Llib/bit.scm 546 */
 int8_t BgL_tmpz00_1792;
{ /* Llib/bit.scm 546 */
 int8_t BgL_auxz00_1802; int8_t BgL_auxz00_1793;
{ /* Llib/bit.scm 546 */
 obj_t BgL_tmpz00_1803;
if(
BGL_INT8P(BgL_yz00_1222))
{ /* Llib/bit.scm 546 */
BgL_tmpz00_1803 = BgL_yz00_1222
; }  else 
{ 
 obj_t BgL_auxz00_1806;
BgL_auxz00_1806 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25346L), BGl_string1695z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_yz00_1222); 
FAILURE(BgL_auxz00_1806,BFALSE,BFALSE);} 
BgL_auxz00_1802 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_1803); } 
{ /* Llib/bit.scm 546 */
 obj_t BgL_tmpz00_1794;
if(
BGL_INT8P(BgL_xz00_1221))
{ /* Llib/bit.scm 546 */
BgL_tmpz00_1794 = BgL_xz00_1221
; }  else 
{ 
 obj_t BgL_auxz00_1797;
BgL_auxz00_1797 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25346L), BGl_string1695z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1221); 
FAILURE(BgL_auxz00_1797,BFALSE,BFALSE);} 
BgL_auxz00_1793 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_1794); } 
BgL_tmpz00_1792 = 
BGl_bitzd2ors8zd2zz__bitz00(BgL_auxz00_1793, BgL_auxz00_1802); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_1792);} } 

}



/* bit-oru8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2oru8zd2zz__bitz00(uint8_t BgL_xz00_11, uint8_t BgL_yz00_12)
{
{ /* Llib/bit.scm 547 */
return 
(BgL_xz00_11 | BgL_yz00_12);} 

}



/* &bit-oru8 */
obj_t BGl_z62bitzd2oru8zb0zz__bitz00(obj_t BgL_envz00_1223, obj_t BgL_xz00_1224, obj_t BgL_yz00_1225)
{
{ /* Llib/bit.scm 547 */
{ /* Llib/bit.scm 547 */
 uint8_t BgL_tmpz00_1814;
{ /* Llib/bit.scm 547 */
 uint8_t BgL_auxz00_1824; uint8_t BgL_auxz00_1815;
{ /* Llib/bit.scm 547 */
 obj_t BgL_tmpz00_1825;
if(
BGL_UINT8P(BgL_yz00_1225))
{ /* Llib/bit.scm 547 */
BgL_tmpz00_1825 = BgL_yz00_1225
; }  else 
{ 
 obj_t BgL_auxz00_1828;
BgL_auxz00_1828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25392L), BGl_string1697z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_yz00_1225); 
FAILURE(BgL_auxz00_1828,BFALSE,BFALSE);} 
BgL_auxz00_1824 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_1825); } 
{ /* Llib/bit.scm 547 */
 obj_t BgL_tmpz00_1816;
if(
BGL_UINT8P(BgL_xz00_1224))
{ /* Llib/bit.scm 547 */
BgL_tmpz00_1816 = BgL_xz00_1224
; }  else 
{ 
 obj_t BgL_auxz00_1819;
BgL_auxz00_1819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25392L), BGl_string1697z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1224); 
FAILURE(BgL_auxz00_1819,BFALSE,BFALSE);} 
BgL_auxz00_1815 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_1816); } 
BgL_tmpz00_1814 = 
BGl_bitzd2oru8zd2zz__bitz00(BgL_auxz00_1815, BgL_auxz00_1824); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_1814);} } 

}



/* bit-ors16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2ors16zd2zz__bitz00(int16_t BgL_xz00_13, int16_t BgL_yz00_14)
{
{ /* Llib/bit.scm 548 */
return 
(BgL_xz00_13 | BgL_yz00_14);} 

}



/* &bit-ors16 */
obj_t BGl_z62bitzd2ors16zb0zz__bitz00(obj_t BgL_envz00_1226, obj_t BgL_xz00_1227, obj_t BgL_yz00_1228)
{
{ /* Llib/bit.scm 548 */
{ /* Llib/bit.scm 548 */
 int16_t BgL_tmpz00_1836;
{ /* Llib/bit.scm 548 */
 int16_t BgL_auxz00_1846; int16_t BgL_auxz00_1837;
{ /* Llib/bit.scm 548 */
 obj_t BgL_tmpz00_1847;
if(
BGL_INT16P(BgL_yz00_1228))
{ /* Llib/bit.scm 548 */
BgL_tmpz00_1847 = BgL_yz00_1228
; }  else 
{ 
 obj_t BgL_auxz00_1850;
BgL_auxz00_1850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25439L), BGl_string1699z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_yz00_1228); 
FAILURE(BgL_auxz00_1850,BFALSE,BFALSE);} 
BgL_auxz00_1846 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_1847); } 
{ /* Llib/bit.scm 548 */
 obj_t BgL_tmpz00_1838;
if(
BGL_INT16P(BgL_xz00_1227))
{ /* Llib/bit.scm 548 */
BgL_tmpz00_1838 = BgL_xz00_1227
; }  else 
{ 
 obj_t BgL_auxz00_1841;
BgL_auxz00_1841 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25439L), BGl_string1699z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1227); 
FAILURE(BgL_auxz00_1841,BFALSE,BFALSE);} 
BgL_auxz00_1837 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_1838); } 
BgL_tmpz00_1836 = 
BGl_bitzd2ors16zd2zz__bitz00(BgL_auxz00_1837, BgL_auxz00_1846); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_1836);} } 

}



/* bit-oru16 */
BGL_EXPORTED_DEF uint16_t BGl_bitzd2oru16zd2zz__bitz00(uint16_t BgL_xz00_15, uint16_t BgL_yz00_16)
{
{ /* Llib/bit.scm 549 */
return 
(BgL_xz00_15 | BgL_yz00_16);} 

}



/* &bit-oru16 */
obj_t BGl_z62bitzd2oru16zb0zz__bitz00(obj_t BgL_envz00_1229, obj_t BgL_xz00_1230, obj_t BgL_yz00_1231)
{
{ /* Llib/bit.scm 549 */
{ /* Llib/bit.scm 549 */
 uint16_t BgL_tmpz00_1858;
{ /* Llib/bit.scm 549 */
 uint16_t BgL_auxz00_1868; uint16_t BgL_auxz00_1859;
{ /* Llib/bit.scm 549 */
 obj_t BgL_tmpz00_1869;
if(
BGL_UINT16P(BgL_yz00_1231))
{ /* Llib/bit.scm 549 */
BgL_tmpz00_1869 = BgL_yz00_1231
; }  else 
{ 
 obj_t BgL_auxz00_1872;
BgL_auxz00_1872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25487L), BGl_string1701z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_yz00_1231); 
FAILURE(BgL_auxz00_1872,BFALSE,BFALSE);} 
BgL_auxz00_1868 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_1869); } 
{ /* Llib/bit.scm 549 */
 obj_t BgL_tmpz00_1860;
if(
BGL_UINT16P(BgL_xz00_1230))
{ /* Llib/bit.scm 549 */
BgL_tmpz00_1860 = BgL_xz00_1230
; }  else 
{ 
 obj_t BgL_auxz00_1863;
BgL_auxz00_1863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25487L), BGl_string1701z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_xz00_1230); 
FAILURE(BgL_auxz00_1863,BFALSE,BFALSE);} 
BgL_auxz00_1859 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_1860); } 
BgL_tmpz00_1858 = 
BGl_bitzd2oru16zd2zz__bitz00(BgL_auxz00_1859, BgL_auxz00_1868); } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_1858);} } 

}



/* bit-ors32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2ors32zd2zz__bitz00(int32_t BgL_xz00_17, int32_t BgL_yz00_18)
{
{ /* Llib/bit.scm 550 */
return 
(BgL_xz00_17 | BgL_yz00_18);} 

}



/* &bit-ors32 */
obj_t BGl_z62bitzd2ors32zb0zz__bitz00(obj_t BgL_envz00_1232, obj_t BgL_xz00_1233, obj_t BgL_yz00_1234)
{
{ /* Llib/bit.scm 550 */
{ /* Llib/bit.scm 550 */
 int32_t BgL_tmpz00_1880;
{ /* Llib/bit.scm 550 */
 int32_t BgL_auxz00_1890; int32_t BgL_auxz00_1881;
{ /* Llib/bit.scm 550 */
 obj_t BgL_tmpz00_1891;
if(
BGL_INT32P(BgL_yz00_1234))
{ /* Llib/bit.scm 550 */
BgL_tmpz00_1891 = BgL_yz00_1234
; }  else 
{ 
 obj_t BgL_auxz00_1894;
BgL_auxz00_1894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25535L), BGl_string1703z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_yz00_1234); 
FAILURE(BgL_auxz00_1894,BFALSE,BFALSE);} 
BgL_auxz00_1890 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_1891); } 
{ /* Llib/bit.scm 550 */
 obj_t BgL_tmpz00_1882;
if(
BGL_INT32P(BgL_xz00_1233))
{ /* Llib/bit.scm 550 */
BgL_tmpz00_1882 = BgL_xz00_1233
; }  else 
{ 
 obj_t BgL_auxz00_1885;
BgL_auxz00_1885 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25535L), BGl_string1703z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1233); 
FAILURE(BgL_auxz00_1885,BFALSE,BFALSE);} 
BgL_auxz00_1881 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_1882); } 
BgL_tmpz00_1880 = 
BGl_bitzd2ors32zd2zz__bitz00(BgL_auxz00_1881, BgL_auxz00_1890); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_1880);} } 

}



/* bit-oru32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2oru32zd2zz__bitz00(uint32_t BgL_xz00_19, uint32_t BgL_yz00_20)
{
{ /* Llib/bit.scm 551 */
return 
(BgL_xz00_19 | BgL_yz00_20);} 

}



/* &bit-oru32 */
obj_t BGl_z62bitzd2oru32zb0zz__bitz00(obj_t BgL_envz00_1235, obj_t BgL_xz00_1236, obj_t BgL_yz00_1237)
{
{ /* Llib/bit.scm 551 */
{ /* Llib/bit.scm 551 */
 uint32_t BgL_tmpz00_1902;
{ /* Llib/bit.scm 551 */
 uint32_t BgL_auxz00_1912; uint32_t BgL_auxz00_1903;
{ /* Llib/bit.scm 551 */
 obj_t BgL_tmpz00_1913;
if(
BGL_UINT32P(BgL_yz00_1237))
{ /* Llib/bit.scm 551 */
BgL_tmpz00_1913 = BgL_yz00_1237
; }  else 
{ 
 obj_t BgL_auxz00_1916;
BgL_auxz00_1916 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25583L), BGl_string1705z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_yz00_1237); 
FAILURE(BgL_auxz00_1916,BFALSE,BFALSE);} 
BgL_auxz00_1912 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_1913); } 
{ /* Llib/bit.scm 551 */
 obj_t BgL_tmpz00_1904;
if(
BGL_UINT32P(BgL_xz00_1236))
{ /* Llib/bit.scm 551 */
BgL_tmpz00_1904 = BgL_xz00_1236
; }  else 
{ 
 obj_t BgL_auxz00_1907;
BgL_auxz00_1907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25583L), BGl_string1705z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1236); 
FAILURE(BgL_auxz00_1907,BFALSE,BFALSE);} 
BgL_auxz00_1903 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_1904); } 
BgL_tmpz00_1902 = 
BGl_bitzd2oru32zd2zz__bitz00(BgL_auxz00_1903, BgL_auxz00_1912); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_1902);} } 

}



/* bit-ors64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2ors64zd2zz__bitz00(int64_t BgL_xz00_21, int64_t BgL_yz00_22)
{
{ /* Llib/bit.scm 552 */
return 
(BgL_xz00_21 | BgL_yz00_22);} 

}



/* &bit-ors64 */
obj_t BGl_z62bitzd2ors64zb0zz__bitz00(obj_t BgL_envz00_1238, obj_t BgL_xz00_1239, obj_t BgL_yz00_1240)
{
{ /* Llib/bit.scm 552 */
{ /* Llib/bit.scm 552 */
 int64_t BgL_tmpz00_1924;
{ /* Llib/bit.scm 552 */
 int64_t BgL_auxz00_1934; int64_t BgL_auxz00_1925;
{ /* Llib/bit.scm 552 */
 obj_t BgL_tmpz00_1935;
if(
BGL_INT64P(BgL_yz00_1240))
{ /* Llib/bit.scm 552 */
BgL_tmpz00_1935 = BgL_yz00_1240
; }  else 
{ 
 obj_t BgL_auxz00_1938;
BgL_auxz00_1938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25631L), BGl_string1707z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_yz00_1240); 
FAILURE(BgL_auxz00_1938,BFALSE,BFALSE);} 
BgL_auxz00_1934 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_1935); } 
{ /* Llib/bit.scm 552 */
 obj_t BgL_tmpz00_1926;
if(
BGL_INT64P(BgL_xz00_1239))
{ /* Llib/bit.scm 552 */
BgL_tmpz00_1926 = BgL_xz00_1239
; }  else 
{ 
 obj_t BgL_auxz00_1929;
BgL_auxz00_1929 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25631L), BGl_string1707z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1239); 
FAILURE(BgL_auxz00_1929,BFALSE,BFALSE);} 
BgL_auxz00_1925 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_1926); } 
BgL_tmpz00_1924 = 
BGl_bitzd2ors64zd2zz__bitz00(BgL_auxz00_1925, BgL_auxz00_1934); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_1924);} } 

}



/* bit-oru64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2oru64zd2zz__bitz00(uint64_t BgL_xz00_23, uint64_t BgL_yz00_24)
{
{ /* Llib/bit.scm 553 */
return 
(BgL_xz00_23 | BgL_yz00_24);} 

}



/* &bit-oru64 */
obj_t BGl_z62bitzd2oru64zb0zz__bitz00(obj_t BgL_envz00_1241, obj_t BgL_xz00_1242, obj_t BgL_yz00_1243)
{
{ /* Llib/bit.scm 553 */
{ /* Llib/bit.scm 553 */
 uint64_t BgL_tmpz00_1946;
{ /* Llib/bit.scm 553 */
 uint64_t BgL_auxz00_1956; uint64_t BgL_auxz00_1947;
{ /* Llib/bit.scm 553 */
 obj_t BgL_tmpz00_1957;
if(
BGL_UINT64P(BgL_yz00_1243))
{ /* Llib/bit.scm 553 */
BgL_tmpz00_1957 = BgL_yz00_1243
; }  else 
{ 
 obj_t BgL_auxz00_1960;
BgL_auxz00_1960 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25679L), BGl_string1709z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_yz00_1243); 
FAILURE(BgL_auxz00_1960,BFALSE,BFALSE);} 
BgL_auxz00_1956 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_1957); } 
{ /* Llib/bit.scm 553 */
 obj_t BgL_tmpz00_1948;
if(
BGL_UINT64P(BgL_xz00_1242))
{ /* Llib/bit.scm 553 */
BgL_tmpz00_1948 = BgL_xz00_1242
; }  else 
{ 
 obj_t BgL_auxz00_1951;
BgL_auxz00_1951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25679L), BGl_string1709z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1242); 
FAILURE(BgL_auxz00_1951,BFALSE,BFALSE);} 
BgL_auxz00_1947 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_1948); } 
BgL_tmpz00_1946 = 
BGl_bitzd2oru64zd2zz__bitz00(BgL_auxz00_1947, BgL_auxz00_1956); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_1946);} } 

}



/* bit-orbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2orbxzd2zz__bitz00(obj_t BgL_xz00_25, obj_t BgL_yz00_26)
{
{ /* Llib/bit.scm 554 */
BGL_TAIL return 
bgl_bignum_or(BgL_xz00_25, BgL_yz00_26);} 

}



/* &bit-orbx */
obj_t BGl_z62bitzd2orbxzb0zz__bitz00(obj_t BgL_envz00_1244, obj_t BgL_xz00_1245, obj_t BgL_yz00_1246)
{
{ /* Llib/bit.scm 554 */
{ /* Llib/bit.scm 554 */
 obj_t BgL_auxz00_1975; obj_t BgL_auxz00_1968;
if(
BIGNUMP(BgL_yz00_1246))
{ /* Llib/bit.scm 554 */
BgL_auxz00_1975 = BgL_yz00_1246
; }  else 
{ 
 obj_t BgL_auxz00_1978;
BgL_auxz00_1978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25726L), BGl_string1711z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_yz00_1246); 
FAILURE(BgL_auxz00_1978,BFALSE,BFALSE);} 
if(
BIGNUMP(BgL_xz00_1245))
{ /* Llib/bit.scm 554 */
BgL_auxz00_1968 = BgL_xz00_1245
; }  else 
{ 
 obj_t BgL_auxz00_1971;
BgL_auxz00_1971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25726L), BGl_string1711z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1245); 
FAILURE(BgL_auxz00_1971,BFALSE,BFALSE);} 
return 
BGl_bitzd2orbxzd2zz__bitz00(BgL_auxz00_1968, BgL_auxz00_1975);} } 

}



/* bit-and */
BGL_EXPORTED_DEF long BGl_bitzd2andzd2zz__bitz00(long BgL_xz00_27, long BgL_yz00_28)
{
{ /* Llib/bit.scm 559 */
return 
(BgL_xz00_27 & BgL_yz00_28);} 

}



/* &bit-and */
obj_t BGl_z62bitzd2andzb0zz__bitz00(obj_t BgL_envz00_1247, obj_t BgL_xz00_1248, obj_t BgL_yz00_1249)
{
{ /* Llib/bit.scm 559 */
{ /* Llib/bit.scm 559 */
 long BgL_tmpz00_1984;
{ /* Llib/bit.scm 559 */
 long BgL_auxz00_1994; long BgL_auxz00_1985;
{ /* Llib/bit.scm 559 */
 obj_t BgL_tmpz00_1995;
if(
INTEGERP(BgL_yz00_1249))
{ /* Llib/bit.scm 559 */
BgL_tmpz00_1995 = BgL_yz00_1249
; }  else 
{ 
 obj_t BgL_auxz00_1998;
BgL_auxz00_1998 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25994L), BGl_string1713z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1249); 
FAILURE(BgL_auxz00_1998,BFALSE,BFALSE);} 
BgL_auxz00_1994 = 
(long)CINT(BgL_tmpz00_1995); } 
{ /* Llib/bit.scm 559 */
 obj_t BgL_tmpz00_1986;
if(
INTEGERP(BgL_xz00_1248))
{ /* Llib/bit.scm 559 */
BgL_tmpz00_1986 = BgL_xz00_1248
; }  else 
{ 
 obj_t BgL_auxz00_1989;
BgL_auxz00_1989 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(25994L), BGl_string1713z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1248); 
FAILURE(BgL_auxz00_1989,BFALSE,BFALSE);} 
BgL_auxz00_1985 = 
(long)CINT(BgL_tmpz00_1986); } 
BgL_tmpz00_1984 = 
BGl_bitzd2andzd2zz__bitz00(BgL_auxz00_1985, BgL_auxz00_1994); } 
return 
BINT(BgL_tmpz00_1984);} } 

}



/* bit-andelong */
BGL_EXPORTED_DEF long BGl_bitzd2andelongzd2zz__bitz00(long BgL_xz00_29, long BgL_yz00_30)
{
{ /* Llib/bit.scm 560 */
return 
(BgL_xz00_29 & BgL_yz00_30);} 

}



/* &bit-andelong */
obj_t BGl_z62bitzd2andelongzb0zz__bitz00(obj_t BgL_envz00_1250, obj_t BgL_xz00_1251, obj_t BgL_yz00_1252)
{
{ /* Llib/bit.scm 560 */
{ /* Llib/bit.scm 560 */
 long BgL_tmpz00_2006;
{ /* Llib/bit.scm 560 */
 long BgL_auxz00_2016; long BgL_auxz00_2007;
{ /* Llib/bit.scm 560 */
 obj_t BgL_tmpz00_2017;
if(
ELONGP(BgL_yz00_1252))
{ /* Llib/bit.scm 560 */
BgL_tmpz00_2017 = BgL_yz00_1252
; }  else 
{ 
 obj_t BgL_auxz00_2020;
BgL_auxz00_2020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26043L), BGl_string1714z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_yz00_1252); 
FAILURE(BgL_auxz00_2020,BFALSE,BFALSE);} 
BgL_auxz00_2016 = 
BELONG_TO_LONG(BgL_tmpz00_2017); } 
{ /* Llib/bit.scm 560 */
 obj_t BgL_tmpz00_2008;
if(
ELONGP(BgL_xz00_1251))
{ /* Llib/bit.scm 560 */
BgL_tmpz00_2008 = BgL_xz00_1251
; }  else 
{ 
 obj_t BgL_auxz00_2011;
BgL_auxz00_2011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26043L), BGl_string1714z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1251); 
FAILURE(BgL_auxz00_2011,BFALSE,BFALSE);} 
BgL_auxz00_2007 = 
BELONG_TO_LONG(BgL_tmpz00_2008); } 
BgL_tmpz00_2006 = 
BGl_bitzd2andelongzd2zz__bitz00(BgL_auxz00_2007, BgL_auxz00_2016); } 
return 
make_belong(BgL_tmpz00_2006);} } 

}



/* bit-andllong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2andllongzd2zz__bitz00(BGL_LONGLONG_T BgL_xz00_31, BGL_LONGLONG_T BgL_yz00_32)
{
{ /* Llib/bit.scm 561 */
return 
(BgL_xz00_31 & BgL_yz00_32);} 

}



/* &bit-andllong */
obj_t BGl_z62bitzd2andllongzb0zz__bitz00(obj_t BgL_envz00_1253, obj_t BgL_xz00_1254, obj_t BgL_yz00_1255)
{
{ /* Llib/bit.scm 561 */
{ /* Llib/bit.scm 561 */
 BGL_LONGLONG_T BgL_tmpz00_2028;
{ /* Llib/bit.scm 561 */
 BGL_LONGLONG_T BgL_auxz00_2038; BGL_LONGLONG_T BgL_auxz00_2029;
{ /* Llib/bit.scm 561 */
 obj_t BgL_tmpz00_2039;
if(
LLONGP(BgL_yz00_1255))
{ /* Llib/bit.scm 561 */
BgL_tmpz00_2039 = BgL_yz00_1255
; }  else 
{ 
 obj_t BgL_auxz00_2042;
BgL_auxz00_2042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26097L), BGl_string1715z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_yz00_1255); 
FAILURE(BgL_auxz00_2042,BFALSE,BFALSE);} 
BgL_auxz00_2038 = 
BLLONG_TO_LLONG(BgL_tmpz00_2039); } 
{ /* Llib/bit.scm 561 */
 obj_t BgL_tmpz00_2030;
if(
LLONGP(BgL_xz00_1254))
{ /* Llib/bit.scm 561 */
BgL_tmpz00_2030 = BgL_xz00_1254
; }  else 
{ 
 obj_t BgL_auxz00_2033;
BgL_auxz00_2033 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26097L), BGl_string1715z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1254); 
FAILURE(BgL_auxz00_2033,BFALSE,BFALSE);} 
BgL_auxz00_2029 = 
BLLONG_TO_LLONG(BgL_tmpz00_2030); } 
BgL_tmpz00_2028 = 
BGl_bitzd2andllongzd2zz__bitz00(BgL_auxz00_2029, BgL_auxz00_2038); } 
return 
make_bllong(BgL_tmpz00_2028);} } 

}



/* bit-ands8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2ands8zd2zz__bitz00(int8_t BgL_xz00_33, int8_t BgL_yz00_34)
{
{ /* Llib/bit.scm 562 */
return 
(BgL_xz00_33 & BgL_yz00_34);} 

}



/* &bit-ands8 */
obj_t BGl_z62bitzd2ands8zb0zz__bitz00(obj_t BgL_envz00_1256, obj_t BgL_xz00_1257, obj_t BgL_yz00_1258)
{
{ /* Llib/bit.scm 562 */
{ /* Llib/bit.scm 562 */
 int8_t BgL_tmpz00_2050;
{ /* Llib/bit.scm 562 */
 int8_t BgL_auxz00_2060; int8_t BgL_auxz00_2051;
{ /* Llib/bit.scm 562 */
 obj_t BgL_tmpz00_2061;
if(
BGL_INT8P(BgL_yz00_1258))
{ /* Llib/bit.scm 562 */
BgL_tmpz00_2061 = BgL_yz00_1258
; }  else 
{ 
 obj_t BgL_auxz00_2064;
BgL_auxz00_2064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26148L), BGl_string1716z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_yz00_1258); 
FAILURE(BgL_auxz00_2064,BFALSE,BFALSE);} 
BgL_auxz00_2060 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_2061); } 
{ /* Llib/bit.scm 562 */
 obj_t BgL_tmpz00_2052;
if(
BGL_INT8P(BgL_xz00_1257))
{ /* Llib/bit.scm 562 */
BgL_tmpz00_2052 = BgL_xz00_1257
; }  else 
{ 
 obj_t BgL_auxz00_2055;
BgL_auxz00_2055 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26148L), BGl_string1716z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1257); 
FAILURE(BgL_auxz00_2055,BFALSE,BFALSE);} 
BgL_auxz00_2051 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_2052); } 
BgL_tmpz00_2050 = 
BGl_bitzd2ands8zd2zz__bitz00(BgL_auxz00_2051, BgL_auxz00_2060); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_2050);} } 

}



/* bit-andu8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2andu8zd2zz__bitz00(uint8_t BgL_xz00_35, uint8_t BgL_yz00_36)
{
{ /* Llib/bit.scm 563 */
return 
(BgL_xz00_35 & BgL_yz00_36);} 

}



/* &bit-andu8 */
obj_t BGl_z62bitzd2andu8zb0zz__bitz00(obj_t BgL_envz00_1259, obj_t BgL_xz00_1260, obj_t BgL_yz00_1261)
{
{ /* Llib/bit.scm 563 */
{ /* Llib/bit.scm 563 */
 uint8_t BgL_tmpz00_2072;
{ /* Llib/bit.scm 563 */
 uint8_t BgL_auxz00_2082; uint8_t BgL_auxz00_2073;
{ /* Llib/bit.scm 563 */
 obj_t BgL_tmpz00_2083;
if(
BGL_UINT8P(BgL_yz00_1261))
{ /* Llib/bit.scm 563 */
BgL_tmpz00_2083 = BgL_yz00_1261
; }  else 
{ 
 obj_t BgL_auxz00_2086;
BgL_auxz00_2086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26196L), BGl_string1717z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_yz00_1261); 
FAILURE(BgL_auxz00_2086,BFALSE,BFALSE);} 
BgL_auxz00_2082 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_2083); } 
{ /* Llib/bit.scm 563 */
 obj_t BgL_tmpz00_2074;
if(
BGL_UINT8P(BgL_xz00_1260))
{ /* Llib/bit.scm 563 */
BgL_tmpz00_2074 = BgL_xz00_1260
; }  else 
{ 
 obj_t BgL_auxz00_2077;
BgL_auxz00_2077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26196L), BGl_string1717z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1260); 
FAILURE(BgL_auxz00_2077,BFALSE,BFALSE);} 
BgL_auxz00_2073 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_2074); } 
BgL_tmpz00_2072 = 
BGl_bitzd2andu8zd2zz__bitz00(BgL_auxz00_2073, BgL_auxz00_2082); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_2072);} } 

}



/* bit-ands16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2ands16zd2zz__bitz00(int16_t BgL_xz00_37, int16_t BgL_yz00_38)
{
{ /* Llib/bit.scm 564 */
return 
(BgL_xz00_37 & BgL_yz00_38);} 

}



/* &bit-ands16 */
obj_t BGl_z62bitzd2ands16zb0zz__bitz00(obj_t BgL_envz00_1262, obj_t BgL_xz00_1263, obj_t BgL_yz00_1264)
{
{ /* Llib/bit.scm 564 */
{ /* Llib/bit.scm 564 */
 int16_t BgL_tmpz00_2094;
{ /* Llib/bit.scm 564 */
 int16_t BgL_auxz00_2104; int16_t BgL_auxz00_2095;
{ /* Llib/bit.scm 564 */
 obj_t BgL_tmpz00_2105;
if(
BGL_INT16P(BgL_yz00_1264))
{ /* Llib/bit.scm 564 */
BgL_tmpz00_2105 = BgL_yz00_1264
; }  else 
{ 
 obj_t BgL_auxz00_2108;
BgL_auxz00_2108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26245L), BGl_string1718z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_yz00_1264); 
FAILURE(BgL_auxz00_2108,BFALSE,BFALSE);} 
BgL_auxz00_2104 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2105); } 
{ /* Llib/bit.scm 564 */
 obj_t BgL_tmpz00_2096;
if(
BGL_INT16P(BgL_xz00_1263))
{ /* Llib/bit.scm 564 */
BgL_tmpz00_2096 = BgL_xz00_1263
; }  else 
{ 
 obj_t BgL_auxz00_2099;
BgL_auxz00_2099 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26245L), BGl_string1718z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1263); 
FAILURE(BgL_auxz00_2099,BFALSE,BFALSE);} 
BgL_auxz00_2095 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2096); } 
BgL_tmpz00_2094 = 
BGl_bitzd2ands16zd2zz__bitz00(BgL_auxz00_2095, BgL_auxz00_2104); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_2094);} } 

}



/* bit-andu16 */
BGL_EXPORTED_DEF uint16_t BGl_bitzd2andu16zd2zz__bitz00(uint16_t BgL_xz00_39, uint16_t BgL_yz00_40)
{
{ /* Llib/bit.scm 565 */
return 
(BgL_xz00_39 & BgL_yz00_40);} 

}



/* &bit-andu16 */
obj_t BGl_z62bitzd2andu16zb0zz__bitz00(obj_t BgL_envz00_1265, obj_t BgL_xz00_1266, obj_t BgL_yz00_1267)
{
{ /* Llib/bit.scm 565 */
{ /* Llib/bit.scm 565 */
 uint16_t BgL_tmpz00_2116;
{ /* Llib/bit.scm 565 */
 uint16_t BgL_auxz00_2126; uint16_t BgL_auxz00_2117;
{ /* Llib/bit.scm 565 */
 obj_t BgL_tmpz00_2127;
if(
BGL_UINT16P(BgL_yz00_1267))
{ /* Llib/bit.scm 565 */
BgL_tmpz00_2127 = BgL_yz00_1267
; }  else 
{ 
 obj_t BgL_auxz00_2130;
BgL_auxz00_2130 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26295L), BGl_string1719z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_yz00_1267); 
FAILURE(BgL_auxz00_2130,BFALSE,BFALSE);} 
BgL_auxz00_2126 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_2127); } 
{ /* Llib/bit.scm 565 */
 obj_t BgL_tmpz00_2118;
if(
BGL_UINT16P(BgL_xz00_1266))
{ /* Llib/bit.scm 565 */
BgL_tmpz00_2118 = BgL_xz00_1266
; }  else 
{ 
 obj_t BgL_auxz00_2121;
BgL_auxz00_2121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26295L), BGl_string1719z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_xz00_1266); 
FAILURE(BgL_auxz00_2121,BFALSE,BFALSE);} 
BgL_auxz00_2117 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_2118); } 
BgL_tmpz00_2116 = 
BGl_bitzd2andu16zd2zz__bitz00(BgL_auxz00_2117, BgL_auxz00_2126); } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_2116);} } 

}



/* bit-ands32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2ands32zd2zz__bitz00(int32_t BgL_xz00_41, int32_t BgL_yz00_42)
{
{ /* Llib/bit.scm 566 */
return 
(BgL_xz00_41 & BgL_yz00_42);} 

}



/* &bit-ands32 */
obj_t BGl_z62bitzd2ands32zb0zz__bitz00(obj_t BgL_envz00_1268, obj_t BgL_xz00_1269, obj_t BgL_yz00_1270)
{
{ /* Llib/bit.scm 566 */
{ /* Llib/bit.scm 566 */
 int32_t BgL_tmpz00_2138;
{ /* Llib/bit.scm 566 */
 int32_t BgL_auxz00_2148; int32_t BgL_auxz00_2139;
{ /* Llib/bit.scm 566 */
 obj_t BgL_tmpz00_2149;
if(
BGL_INT32P(BgL_yz00_1270))
{ /* Llib/bit.scm 566 */
BgL_tmpz00_2149 = BgL_yz00_1270
; }  else 
{ 
 obj_t BgL_auxz00_2152;
BgL_auxz00_2152 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26345L), BGl_string1720z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_yz00_1270); 
FAILURE(BgL_auxz00_2152,BFALSE,BFALSE);} 
BgL_auxz00_2148 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_2149); } 
{ /* Llib/bit.scm 566 */
 obj_t BgL_tmpz00_2140;
if(
BGL_INT32P(BgL_xz00_1269))
{ /* Llib/bit.scm 566 */
BgL_tmpz00_2140 = BgL_xz00_1269
; }  else 
{ 
 obj_t BgL_auxz00_2143;
BgL_auxz00_2143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26345L), BGl_string1720z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1269); 
FAILURE(BgL_auxz00_2143,BFALSE,BFALSE);} 
BgL_auxz00_2139 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_2140); } 
BgL_tmpz00_2138 = 
BGl_bitzd2ands32zd2zz__bitz00(BgL_auxz00_2139, BgL_auxz00_2148); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_2138);} } 

}



/* bit-andu32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2andu32zd2zz__bitz00(uint32_t BgL_xz00_43, uint32_t BgL_yz00_44)
{
{ /* Llib/bit.scm 567 */
return 
(BgL_xz00_43 & BgL_yz00_44);} 

}



/* &bit-andu32 */
obj_t BGl_z62bitzd2andu32zb0zz__bitz00(obj_t BgL_envz00_1271, obj_t BgL_xz00_1272, obj_t BgL_yz00_1273)
{
{ /* Llib/bit.scm 567 */
{ /* Llib/bit.scm 567 */
 uint32_t BgL_tmpz00_2160;
{ /* Llib/bit.scm 567 */
 uint32_t BgL_auxz00_2170; uint32_t BgL_auxz00_2161;
{ /* Llib/bit.scm 567 */
 obj_t BgL_tmpz00_2171;
if(
BGL_UINT32P(BgL_yz00_1273))
{ /* Llib/bit.scm 567 */
BgL_tmpz00_2171 = BgL_yz00_1273
; }  else 
{ 
 obj_t BgL_auxz00_2174;
BgL_auxz00_2174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26395L), BGl_string1721z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_yz00_1273); 
FAILURE(BgL_auxz00_2174,BFALSE,BFALSE);} 
BgL_auxz00_2170 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_2171); } 
{ /* Llib/bit.scm 567 */
 obj_t BgL_tmpz00_2162;
if(
BGL_UINT32P(BgL_xz00_1272))
{ /* Llib/bit.scm 567 */
BgL_tmpz00_2162 = BgL_xz00_1272
; }  else 
{ 
 obj_t BgL_auxz00_2165;
BgL_auxz00_2165 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26395L), BGl_string1721z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1272); 
FAILURE(BgL_auxz00_2165,BFALSE,BFALSE);} 
BgL_auxz00_2161 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_2162); } 
BgL_tmpz00_2160 = 
BGl_bitzd2andu32zd2zz__bitz00(BgL_auxz00_2161, BgL_auxz00_2170); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_2160);} } 

}



/* bit-ands64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2ands64zd2zz__bitz00(int64_t BgL_xz00_45, int64_t BgL_yz00_46)
{
{ /* Llib/bit.scm 568 */
return 
(BgL_xz00_45 & BgL_yz00_46);} 

}



/* &bit-ands64 */
obj_t BGl_z62bitzd2ands64zb0zz__bitz00(obj_t BgL_envz00_1274, obj_t BgL_xz00_1275, obj_t BgL_yz00_1276)
{
{ /* Llib/bit.scm 568 */
{ /* Llib/bit.scm 568 */
 int64_t BgL_tmpz00_2182;
{ /* Llib/bit.scm 568 */
 int64_t BgL_auxz00_2192; int64_t BgL_auxz00_2183;
{ /* Llib/bit.scm 568 */
 obj_t BgL_tmpz00_2193;
if(
BGL_INT64P(BgL_yz00_1276))
{ /* Llib/bit.scm 568 */
BgL_tmpz00_2193 = BgL_yz00_1276
; }  else 
{ 
 obj_t BgL_auxz00_2196;
BgL_auxz00_2196 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26445L), BGl_string1722z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_yz00_1276); 
FAILURE(BgL_auxz00_2196,BFALSE,BFALSE);} 
BgL_auxz00_2192 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2193); } 
{ /* Llib/bit.scm 568 */
 obj_t BgL_tmpz00_2184;
if(
BGL_INT64P(BgL_xz00_1275))
{ /* Llib/bit.scm 568 */
BgL_tmpz00_2184 = BgL_xz00_1275
; }  else 
{ 
 obj_t BgL_auxz00_2187;
BgL_auxz00_2187 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26445L), BGl_string1722z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1275); 
FAILURE(BgL_auxz00_2187,BFALSE,BFALSE);} 
BgL_auxz00_2183 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2184); } 
BgL_tmpz00_2182 = 
BGl_bitzd2ands64zd2zz__bitz00(BgL_auxz00_2183, BgL_auxz00_2192); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_2182);} } 

}



/* bit-andu64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2andu64zd2zz__bitz00(uint64_t BgL_xz00_47, uint64_t BgL_yz00_48)
{
{ /* Llib/bit.scm 569 */
return 
(BgL_xz00_47 & BgL_yz00_48);} 

}



/* &bit-andu64 */
obj_t BGl_z62bitzd2andu64zb0zz__bitz00(obj_t BgL_envz00_1277, obj_t BgL_xz00_1278, obj_t BgL_yz00_1279)
{
{ /* Llib/bit.scm 569 */
{ /* Llib/bit.scm 569 */
 uint64_t BgL_tmpz00_2204;
{ /* Llib/bit.scm 569 */
 uint64_t BgL_auxz00_2214; uint64_t BgL_auxz00_2205;
{ /* Llib/bit.scm 569 */
 obj_t BgL_tmpz00_2215;
if(
BGL_UINT64P(BgL_yz00_1279))
{ /* Llib/bit.scm 569 */
BgL_tmpz00_2215 = BgL_yz00_1279
; }  else 
{ 
 obj_t BgL_auxz00_2218;
BgL_auxz00_2218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26495L), BGl_string1723z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_yz00_1279); 
FAILURE(BgL_auxz00_2218,BFALSE,BFALSE);} 
BgL_auxz00_2214 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2215); } 
{ /* Llib/bit.scm 569 */
 obj_t BgL_tmpz00_2206;
if(
BGL_UINT64P(BgL_xz00_1278))
{ /* Llib/bit.scm 569 */
BgL_tmpz00_2206 = BgL_xz00_1278
; }  else 
{ 
 obj_t BgL_auxz00_2209;
BgL_auxz00_2209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26495L), BGl_string1723z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1278); 
FAILURE(BgL_auxz00_2209,BFALSE,BFALSE);} 
BgL_auxz00_2205 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2206); } 
BgL_tmpz00_2204 = 
BGl_bitzd2andu64zd2zz__bitz00(BgL_auxz00_2205, BgL_auxz00_2214); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_2204);} } 

}



/* bit-andbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2andbxzd2zz__bitz00(obj_t BgL_xz00_49, obj_t BgL_yz00_50)
{
{ /* Llib/bit.scm 570 */
BGL_TAIL return 
bgl_bignum_and(BgL_xz00_49, BgL_yz00_50);} 

}



/* &bit-andbx */
obj_t BGl_z62bitzd2andbxzb0zz__bitz00(obj_t BgL_envz00_1280, obj_t BgL_xz00_1281, obj_t BgL_yz00_1282)
{
{ /* Llib/bit.scm 570 */
{ /* Llib/bit.scm 570 */
 obj_t BgL_auxz00_2233; obj_t BgL_auxz00_2226;
if(
BIGNUMP(BgL_yz00_1282))
{ /* Llib/bit.scm 570 */
BgL_auxz00_2233 = BgL_yz00_1282
; }  else 
{ 
 obj_t BgL_auxz00_2236;
BgL_auxz00_2236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26544L), BGl_string1724z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_yz00_1282); 
FAILURE(BgL_auxz00_2236,BFALSE,BFALSE);} 
if(
BIGNUMP(BgL_xz00_1281))
{ /* Llib/bit.scm 570 */
BgL_auxz00_2226 = BgL_xz00_1281
; }  else 
{ 
 obj_t BgL_auxz00_2229;
BgL_auxz00_2229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26544L), BGl_string1724z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1281); 
FAILURE(BgL_auxz00_2229,BFALSE,BFALSE);} 
return 
BGl_bitzd2andbxzd2zz__bitz00(BgL_auxz00_2226, BgL_auxz00_2233);} } 

}



/* bit-maskbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2maskbxzd2zz__bitz00(obj_t BgL_xz00_51, long BgL_nz00_52)
{
{ /* Llib/bit.scm 572 */
BGL_TAIL return 
bgl_bignum_mask(BgL_xz00_51, BgL_nz00_52);} 

}



/* &bit-maskbx */
obj_t BGl_z62bitzd2maskbxzb0zz__bitz00(obj_t BgL_envz00_1283, obj_t BgL_xz00_1284, obj_t BgL_nz00_1285)
{
{ /* Llib/bit.scm 572 */
{ /* Llib/bit.scm 572 */
 long BgL_auxz00_2249; obj_t BgL_auxz00_2242;
{ /* Llib/bit.scm 572 */
 obj_t BgL_tmpz00_2250;
if(
INTEGERP(BgL_nz00_1285))
{ /* Llib/bit.scm 572 */
BgL_tmpz00_2250 = BgL_nz00_1285
; }  else 
{ 
 obj_t BgL_auxz00_2253;
BgL_auxz00_2253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26594L), BGl_string1725z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_nz00_1285); 
FAILURE(BgL_auxz00_2253,BFALSE,BFALSE);} 
BgL_auxz00_2249 = 
(long)CINT(BgL_tmpz00_2250); } 
if(
BIGNUMP(BgL_xz00_1284))
{ /* Llib/bit.scm 572 */
BgL_auxz00_2242 = BgL_xz00_1284
; }  else 
{ 
 obj_t BgL_auxz00_2245;
BgL_auxz00_2245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26594L), BGl_string1725z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1284); 
FAILURE(BgL_auxz00_2245,BFALSE,BFALSE);} 
return 
BGl_bitzd2maskbxzd2zz__bitz00(BgL_auxz00_2242, BgL_auxz00_2249);} } 

}



/* bit-xor */
BGL_EXPORTED_DEF long BGl_bitzd2xorzd2zz__bitz00(long BgL_xz00_53, long BgL_yz00_54)
{
{ /* Llib/bit.scm 577 */
return 
(BgL_xz00_53 ^ BgL_yz00_54);} 

}



/* &bit-xor */
obj_t BGl_z62bitzd2xorzb0zz__bitz00(obj_t BgL_envz00_1286, obj_t BgL_xz00_1287, obj_t BgL_yz00_1288)
{
{ /* Llib/bit.scm 577 */
{ /* Llib/bit.scm 577 */
 long BgL_tmpz00_2260;
{ /* Llib/bit.scm 577 */
 long BgL_auxz00_2270; long BgL_auxz00_2261;
{ /* Llib/bit.scm 577 */
 obj_t BgL_tmpz00_2271;
if(
INTEGERP(BgL_yz00_1288))
{ /* Llib/bit.scm 577 */
BgL_tmpz00_2271 = BgL_yz00_1288
; }  else 
{ 
 obj_t BgL_auxz00_2274;
BgL_auxz00_2274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26864L), BGl_string1726z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1288); 
FAILURE(BgL_auxz00_2274,BFALSE,BFALSE);} 
BgL_auxz00_2270 = 
(long)CINT(BgL_tmpz00_2271); } 
{ /* Llib/bit.scm 577 */
 obj_t BgL_tmpz00_2262;
if(
INTEGERP(BgL_xz00_1287))
{ /* Llib/bit.scm 577 */
BgL_tmpz00_2262 = BgL_xz00_1287
; }  else 
{ 
 obj_t BgL_auxz00_2265;
BgL_auxz00_2265 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26864L), BGl_string1726z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1287); 
FAILURE(BgL_auxz00_2265,BFALSE,BFALSE);} 
BgL_auxz00_2261 = 
(long)CINT(BgL_tmpz00_2262); } 
BgL_tmpz00_2260 = 
BGl_bitzd2xorzd2zz__bitz00(BgL_auxz00_2261, BgL_auxz00_2270); } 
return 
BINT(BgL_tmpz00_2260);} } 

}



/* bit-xorelong */
BGL_EXPORTED_DEF long BGl_bitzd2xorelongzd2zz__bitz00(long BgL_xz00_55, long BgL_yz00_56)
{
{ /* Llib/bit.scm 578 */
return 
(BgL_xz00_55 ^ BgL_yz00_56);} 

}



/* &bit-xorelong */
obj_t BGl_z62bitzd2xorelongzb0zz__bitz00(obj_t BgL_envz00_1289, obj_t BgL_xz00_1290, obj_t BgL_yz00_1291)
{
{ /* Llib/bit.scm 578 */
{ /* Llib/bit.scm 578 */
 long BgL_tmpz00_2282;
{ /* Llib/bit.scm 578 */
 long BgL_auxz00_2292; long BgL_auxz00_2283;
{ /* Llib/bit.scm 578 */
 obj_t BgL_tmpz00_2293;
if(
ELONGP(BgL_yz00_1291))
{ /* Llib/bit.scm 578 */
BgL_tmpz00_2293 = BgL_yz00_1291
; }  else 
{ 
 obj_t BgL_auxz00_2296;
BgL_auxz00_2296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26913L), BGl_string1727z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_yz00_1291); 
FAILURE(BgL_auxz00_2296,BFALSE,BFALSE);} 
BgL_auxz00_2292 = 
BELONG_TO_LONG(BgL_tmpz00_2293); } 
{ /* Llib/bit.scm 578 */
 obj_t BgL_tmpz00_2284;
if(
ELONGP(BgL_xz00_1290))
{ /* Llib/bit.scm 578 */
BgL_tmpz00_2284 = BgL_xz00_1290
; }  else 
{ 
 obj_t BgL_auxz00_2287;
BgL_auxz00_2287 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26913L), BGl_string1727z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1290); 
FAILURE(BgL_auxz00_2287,BFALSE,BFALSE);} 
BgL_auxz00_2283 = 
BELONG_TO_LONG(BgL_tmpz00_2284); } 
BgL_tmpz00_2282 = 
BGl_bitzd2xorelongzd2zz__bitz00(BgL_auxz00_2283, BgL_auxz00_2292); } 
return 
make_belong(BgL_tmpz00_2282);} } 

}



/* bit-xorllong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2xorllongzd2zz__bitz00(BGL_LONGLONG_T BgL_xz00_57, BGL_LONGLONG_T BgL_yz00_58)
{
{ /* Llib/bit.scm 579 */
return 
(BgL_xz00_57 ^ BgL_yz00_58);} 

}



/* &bit-xorllong */
obj_t BGl_z62bitzd2xorllongzb0zz__bitz00(obj_t BgL_envz00_1292, obj_t BgL_xz00_1293, obj_t BgL_yz00_1294)
{
{ /* Llib/bit.scm 579 */
{ /* Llib/bit.scm 579 */
 BGL_LONGLONG_T BgL_tmpz00_2304;
{ /* Llib/bit.scm 579 */
 BGL_LONGLONG_T BgL_auxz00_2314; BGL_LONGLONG_T BgL_auxz00_2305;
{ /* Llib/bit.scm 579 */
 obj_t BgL_tmpz00_2315;
if(
LLONGP(BgL_yz00_1294))
{ /* Llib/bit.scm 579 */
BgL_tmpz00_2315 = BgL_yz00_1294
; }  else 
{ 
 obj_t BgL_auxz00_2318;
BgL_auxz00_2318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26967L), BGl_string1728z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_yz00_1294); 
FAILURE(BgL_auxz00_2318,BFALSE,BFALSE);} 
BgL_auxz00_2314 = 
BLLONG_TO_LLONG(BgL_tmpz00_2315); } 
{ /* Llib/bit.scm 579 */
 obj_t BgL_tmpz00_2306;
if(
LLONGP(BgL_xz00_1293))
{ /* Llib/bit.scm 579 */
BgL_tmpz00_2306 = BgL_xz00_1293
; }  else 
{ 
 obj_t BgL_auxz00_2309;
BgL_auxz00_2309 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(26967L), BGl_string1728z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1293); 
FAILURE(BgL_auxz00_2309,BFALSE,BFALSE);} 
BgL_auxz00_2305 = 
BLLONG_TO_LLONG(BgL_tmpz00_2306); } 
BgL_tmpz00_2304 = 
BGl_bitzd2xorllongzd2zz__bitz00(BgL_auxz00_2305, BgL_auxz00_2314); } 
return 
make_bllong(BgL_tmpz00_2304);} } 

}



/* bit-xors8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2xors8zd2zz__bitz00(int8_t BgL_xz00_59, int8_t BgL_yz00_60)
{
{ /* Llib/bit.scm 580 */
return 
(BgL_xz00_59 ^ BgL_yz00_60);} 

}



/* &bit-xors8 */
obj_t BGl_z62bitzd2xors8zb0zz__bitz00(obj_t BgL_envz00_1295, obj_t BgL_xz00_1296, obj_t BgL_yz00_1297)
{
{ /* Llib/bit.scm 580 */
{ /* Llib/bit.scm 580 */
 int8_t BgL_tmpz00_2326;
{ /* Llib/bit.scm 580 */
 int8_t BgL_auxz00_2336; int8_t BgL_auxz00_2327;
{ /* Llib/bit.scm 580 */
 obj_t BgL_tmpz00_2337;
if(
BGL_INT8P(BgL_yz00_1297))
{ /* Llib/bit.scm 580 */
BgL_tmpz00_2337 = BgL_yz00_1297
; }  else 
{ 
 obj_t BgL_auxz00_2340;
BgL_auxz00_2340 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27018L), BGl_string1729z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_yz00_1297); 
FAILURE(BgL_auxz00_2340,BFALSE,BFALSE);} 
BgL_auxz00_2336 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_2337); } 
{ /* Llib/bit.scm 580 */
 obj_t BgL_tmpz00_2328;
if(
BGL_INT8P(BgL_xz00_1296))
{ /* Llib/bit.scm 580 */
BgL_tmpz00_2328 = BgL_xz00_1296
; }  else 
{ 
 obj_t BgL_auxz00_2331;
BgL_auxz00_2331 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27018L), BGl_string1729z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1296); 
FAILURE(BgL_auxz00_2331,BFALSE,BFALSE);} 
BgL_auxz00_2327 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_2328); } 
BgL_tmpz00_2326 = 
BGl_bitzd2xors8zd2zz__bitz00(BgL_auxz00_2327, BgL_auxz00_2336); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_2326);} } 

}



/* bit-xoru8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2xoru8zd2zz__bitz00(uint8_t BgL_xz00_61, uint8_t BgL_yz00_62)
{
{ /* Llib/bit.scm 581 */
return 
(BgL_xz00_61 ^ BgL_yz00_62);} 

}



/* &bit-xoru8 */
obj_t BGl_z62bitzd2xoru8zb0zz__bitz00(obj_t BgL_envz00_1298, obj_t BgL_xz00_1299, obj_t BgL_yz00_1300)
{
{ /* Llib/bit.scm 581 */
{ /* Llib/bit.scm 581 */
 uint8_t BgL_tmpz00_2348;
{ /* Llib/bit.scm 581 */
 uint8_t BgL_auxz00_2358; uint8_t BgL_auxz00_2349;
{ /* Llib/bit.scm 581 */
 obj_t BgL_tmpz00_2359;
if(
BGL_UINT8P(BgL_yz00_1300))
{ /* Llib/bit.scm 581 */
BgL_tmpz00_2359 = BgL_yz00_1300
; }  else 
{ 
 obj_t BgL_auxz00_2362;
BgL_auxz00_2362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27066L), BGl_string1730z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_yz00_1300); 
FAILURE(BgL_auxz00_2362,BFALSE,BFALSE);} 
BgL_auxz00_2358 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_2359); } 
{ /* Llib/bit.scm 581 */
 obj_t BgL_tmpz00_2350;
if(
BGL_UINT8P(BgL_xz00_1299))
{ /* Llib/bit.scm 581 */
BgL_tmpz00_2350 = BgL_xz00_1299
; }  else 
{ 
 obj_t BgL_auxz00_2353;
BgL_auxz00_2353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27066L), BGl_string1730z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1299); 
FAILURE(BgL_auxz00_2353,BFALSE,BFALSE);} 
BgL_auxz00_2349 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_2350); } 
BgL_tmpz00_2348 = 
BGl_bitzd2xoru8zd2zz__bitz00(BgL_auxz00_2349, BgL_auxz00_2358); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_2348);} } 

}



/* bit-xors16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2xors16zd2zz__bitz00(int16_t BgL_xz00_63, int16_t BgL_yz00_64)
{
{ /* Llib/bit.scm 582 */
return 
(BgL_xz00_63 ^ BgL_yz00_64);} 

}



/* &bit-xors16 */
obj_t BGl_z62bitzd2xors16zb0zz__bitz00(obj_t BgL_envz00_1301, obj_t BgL_xz00_1302, obj_t BgL_yz00_1303)
{
{ /* Llib/bit.scm 582 */
{ /* Llib/bit.scm 582 */
 int16_t BgL_tmpz00_2370;
{ /* Llib/bit.scm 582 */
 int16_t BgL_auxz00_2380; int16_t BgL_auxz00_2371;
{ /* Llib/bit.scm 582 */
 obj_t BgL_tmpz00_2381;
if(
BGL_INT16P(BgL_yz00_1303))
{ /* Llib/bit.scm 582 */
BgL_tmpz00_2381 = BgL_yz00_1303
; }  else 
{ 
 obj_t BgL_auxz00_2384;
BgL_auxz00_2384 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27115L), BGl_string1731z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_yz00_1303); 
FAILURE(BgL_auxz00_2384,BFALSE,BFALSE);} 
BgL_auxz00_2380 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2381); } 
{ /* Llib/bit.scm 582 */
 obj_t BgL_tmpz00_2372;
if(
BGL_INT16P(BgL_xz00_1302))
{ /* Llib/bit.scm 582 */
BgL_tmpz00_2372 = BgL_xz00_1302
; }  else 
{ 
 obj_t BgL_auxz00_2375;
BgL_auxz00_2375 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27115L), BGl_string1731z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1302); 
FAILURE(BgL_auxz00_2375,BFALSE,BFALSE);} 
BgL_auxz00_2371 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2372); } 
BgL_tmpz00_2370 = 
BGl_bitzd2xors16zd2zz__bitz00(BgL_auxz00_2371, BgL_auxz00_2380); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_2370);} } 

}



/* bit-xoru16 */
BGL_EXPORTED_DEF uint16_t BGl_bitzd2xoru16zd2zz__bitz00(uint16_t BgL_xz00_65, uint16_t BgL_yz00_66)
{
{ /* Llib/bit.scm 583 */
return 
(BgL_xz00_65 ^ BgL_yz00_66);} 

}



/* &bit-xoru16 */
obj_t BGl_z62bitzd2xoru16zb0zz__bitz00(obj_t BgL_envz00_1304, obj_t BgL_xz00_1305, obj_t BgL_yz00_1306)
{
{ /* Llib/bit.scm 583 */
{ /* Llib/bit.scm 583 */
 uint16_t BgL_tmpz00_2392;
{ /* Llib/bit.scm 583 */
 uint16_t BgL_auxz00_2402; uint16_t BgL_auxz00_2393;
{ /* Llib/bit.scm 583 */
 obj_t BgL_tmpz00_2403;
if(
BGL_UINT16P(BgL_yz00_1306))
{ /* Llib/bit.scm 583 */
BgL_tmpz00_2403 = BgL_yz00_1306
; }  else 
{ 
 obj_t BgL_auxz00_2406;
BgL_auxz00_2406 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27165L), BGl_string1732z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_yz00_1306); 
FAILURE(BgL_auxz00_2406,BFALSE,BFALSE);} 
BgL_auxz00_2402 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_2403); } 
{ /* Llib/bit.scm 583 */
 obj_t BgL_tmpz00_2394;
if(
BGL_UINT16P(BgL_xz00_1305))
{ /* Llib/bit.scm 583 */
BgL_tmpz00_2394 = BgL_xz00_1305
; }  else 
{ 
 obj_t BgL_auxz00_2397;
BgL_auxz00_2397 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27165L), BGl_string1732z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_xz00_1305); 
FAILURE(BgL_auxz00_2397,BFALSE,BFALSE);} 
BgL_auxz00_2393 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_2394); } 
BgL_tmpz00_2392 = 
BGl_bitzd2xoru16zd2zz__bitz00(BgL_auxz00_2393, BgL_auxz00_2402); } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_2392);} } 

}



/* bit-xors32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2xors32zd2zz__bitz00(int32_t BgL_xz00_67, int32_t BgL_yz00_68)
{
{ /* Llib/bit.scm 584 */
return 
(BgL_xz00_67 ^ BgL_yz00_68);} 

}



/* &bit-xors32 */
obj_t BGl_z62bitzd2xors32zb0zz__bitz00(obj_t BgL_envz00_1307, obj_t BgL_xz00_1308, obj_t BgL_yz00_1309)
{
{ /* Llib/bit.scm 584 */
{ /* Llib/bit.scm 584 */
 int32_t BgL_tmpz00_2414;
{ /* Llib/bit.scm 584 */
 int32_t BgL_auxz00_2424; int32_t BgL_auxz00_2415;
{ /* Llib/bit.scm 584 */
 obj_t BgL_tmpz00_2425;
if(
BGL_INT32P(BgL_yz00_1309))
{ /* Llib/bit.scm 584 */
BgL_tmpz00_2425 = BgL_yz00_1309
; }  else 
{ 
 obj_t BgL_auxz00_2428;
BgL_auxz00_2428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27215L), BGl_string1733z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_yz00_1309); 
FAILURE(BgL_auxz00_2428,BFALSE,BFALSE);} 
BgL_auxz00_2424 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_2425); } 
{ /* Llib/bit.scm 584 */
 obj_t BgL_tmpz00_2416;
if(
BGL_INT32P(BgL_xz00_1308))
{ /* Llib/bit.scm 584 */
BgL_tmpz00_2416 = BgL_xz00_1308
; }  else 
{ 
 obj_t BgL_auxz00_2419;
BgL_auxz00_2419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27215L), BGl_string1733z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1308); 
FAILURE(BgL_auxz00_2419,BFALSE,BFALSE);} 
BgL_auxz00_2415 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_2416); } 
BgL_tmpz00_2414 = 
BGl_bitzd2xors32zd2zz__bitz00(BgL_auxz00_2415, BgL_auxz00_2424); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_2414);} } 

}



/* bit-xoru32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2xoru32zd2zz__bitz00(uint32_t BgL_xz00_69, uint32_t BgL_yz00_70)
{
{ /* Llib/bit.scm 585 */
return 
(BgL_xz00_69 ^ BgL_yz00_70);} 

}



/* &bit-xoru32 */
obj_t BGl_z62bitzd2xoru32zb0zz__bitz00(obj_t BgL_envz00_1310, obj_t BgL_xz00_1311, obj_t BgL_yz00_1312)
{
{ /* Llib/bit.scm 585 */
{ /* Llib/bit.scm 585 */
 uint32_t BgL_tmpz00_2436;
{ /* Llib/bit.scm 585 */
 uint32_t BgL_auxz00_2446; uint32_t BgL_auxz00_2437;
{ /* Llib/bit.scm 585 */
 obj_t BgL_tmpz00_2447;
if(
BGL_UINT32P(BgL_yz00_1312))
{ /* Llib/bit.scm 585 */
BgL_tmpz00_2447 = BgL_yz00_1312
; }  else 
{ 
 obj_t BgL_auxz00_2450;
BgL_auxz00_2450 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27265L), BGl_string1734z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_yz00_1312); 
FAILURE(BgL_auxz00_2450,BFALSE,BFALSE);} 
BgL_auxz00_2446 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_2447); } 
{ /* Llib/bit.scm 585 */
 obj_t BgL_tmpz00_2438;
if(
BGL_UINT32P(BgL_xz00_1311))
{ /* Llib/bit.scm 585 */
BgL_tmpz00_2438 = BgL_xz00_1311
; }  else 
{ 
 obj_t BgL_auxz00_2441;
BgL_auxz00_2441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27265L), BGl_string1734z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1311); 
FAILURE(BgL_auxz00_2441,BFALSE,BFALSE);} 
BgL_auxz00_2437 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_2438); } 
BgL_tmpz00_2436 = 
BGl_bitzd2xoru32zd2zz__bitz00(BgL_auxz00_2437, BgL_auxz00_2446); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_2436);} } 

}



/* bit-xors64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2xors64zd2zz__bitz00(int64_t BgL_xz00_71, int64_t BgL_yz00_72)
{
{ /* Llib/bit.scm 586 */
return 
(BgL_xz00_71 ^ BgL_yz00_72);} 

}



/* &bit-xors64 */
obj_t BGl_z62bitzd2xors64zb0zz__bitz00(obj_t BgL_envz00_1313, obj_t BgL_xz00_1314, obj_t BgL_yz00_1315)
{
{ /* Llib/bit.scm 586 */
{ /* Llib/bit.scm 586 */
 int64_t BgL_tmpz00_2458;
{ /* Llib/bit.scm 586 */
 int64_t BgL_auxz00_2468; int64_t BgL_auxz00_2459;
{ /* Llib/bit.scm 586 */
 obj_t BgL_tmpz00_2469;
if(
BGL_INT64P(BgL_yz00_1315))
{ /* Llib/bit.scm 586 */
BgL_tmpz00_2469 = BgL_yz00_1315
; }  else 
{ 
 obj_t BgL_auxz00_2472;
BgL_auxz00_2472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27315L), BGl_string1735z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_yz00_1315); 
FAILURE(BgL_auxz00_2472,BFALSE,BFALSE);} 
BgL_auxz00_2468 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2469); } 
{ /* Llib/bit.scm 586 */
 obj_t BgL_tmpz00_2460;
if(
BGL_INT64P(BgL_xz00_1314))
{ /* Llib/bit.scm 586 */
BgL_tmpz00_2460 = BgL_xz00_1314
; }  else 
{ 
 obj_t BgL_auxz00_2463;
BgL_auxz00_2463 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27315L), BGl_string1735z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1314); 
FAILURE(BgL_auxz00_2463,BFALSE,BFALSE);} 
BgL_auxz00_2459 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2460); } 
BgL_tmpz00_2458 = 
BGl_bitzd2xors64zd2zz__bitz00(BgL_auxz00_2459, BgL_auxz00_2468); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_2458);} } 

}



/* bit-xoru64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2xoru64zd2zz__bitz00(uint64_t BgL_xz00_73, uint64_t BgL_yz00_74)
{
{ /* Llib/bit.scm 587 */
return 
(BgL_xz00_73 ^ BgL_yz00_74);} 

}



/* &bit-xoru64 */
obj_t BGl_z62bitzd2xoru64zb0zz__bitz00(obj_t BgL_envz00_1316, obj_t BgL_xz00_1317, obj_t BgL_yz00_1318)
{
{ /* Llib/bit.scm 587 */
{ /* Llib/bit.scm 587 */
 uint64_t BgL_tmpz00_2480;
{ /* Llib/bit.scm 587 */
 uint64_t BgL_auxz00_2490; uint64_t BgL_auxz00_2481;
{ /* Llib/bit.scm 587 */
 obj_t BgL_tmpz00_2491;
if(
BGL_UINT64P(BgL_yz00_1318))
{ /* Llib/bit.scm 587 */
BgL_tmpz00_2491 = BgL_yz00_1318
; }  else 
{ 
 obj_t BgL_auxz00_2494;
BgL_auxz00_2494 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27365L), BGl_string1736z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_yz00_1318); 
FAILURE(BgL_auxz00_2494,BFALSE,BFALSE);} 
BgL_auxz00_2490 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2491); } 
{ /* Llib/bit.scm 587 */
 obj_t BgL_tmpz00_2482;
if(
BGL_UINT64P(BgL_xz00_1317))
{ /* Llib/bit.scm 587 */
BgL_tmpz00_2482 = BgL_xz00_1317
; }  else 
{ 
 obj_t BgL_auxz00_2485;
BgL_auxz00_2485 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27365L), BGl_string1736z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1317); 
FAILURE(BgL_auxz00_2485,BFALSE,BFALSE);} 
BgL_auxz00_2481 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2482); } 
BgL_tmpz00_2480 = 
BGl_bitzd2xoru64zd2zz__bitz00(BgL_auxz00_2481, BgL_auxz00_2490); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_2480);} } 

}



/* bit-xorbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2xorbxzd2zz__bitz00(obj_t BgL_xz00_75, obj_t BgL_yz00_76)
{
{ /* Llib/bit.scm 588 */
BGL_TAIL return 
bgl_bignum_xor(BgL_xz00_75, BgL_yz00_76);} 

}



/* &bit-xorbx */
obj_t BGl_z62bitzd2xorbxzb0zz__bitz00(obj_t BgL_envz00_1319, obj_t BgL_xz00_1320, obj_t BgL_yz00_1321)
{
{ /* Llib/bit.scm 588 */
{ /* Llib/bit.scm 588 */
 obj_t BgL_auxz00_2509; obj_t BgL_auxz00_2502;
if(
BIGNUMP(BgL_yz00_1321))
{ /* Llib/bit.scm 588 */
BgL_auxz00_2509 = BgL_yz00_1321
; }  else 
{ 
 obj_t BgL_auxz00_2512;
BgL_auxz00_2512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27414L), BGl_string1737z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_yz00_1321); 
FAILURE(BgL_auxz00_2512,BFALSE,BFALSE);} 
if(
BIGNUMP(BgL_xz00_1320))
{ /* Llib/bit.scm 588 */
BgL_auxz00_2502 = BgL_xz00_1320
; }  else 
{ 
 obj_t BgL_auxz00_2505;
BgL_auxz00_2505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27414L), BGl_string1737z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1320); 
FAILURE(BgL_auxz00_2505,BFALSE,BFALSE);} 
return 
BGl_bitzd2xorbxzd2zz__bitz00(BgL_auxz00_2502, BgL_auxz00_2509);} } 

}



/* bit-not */
BGL_EXPORTED_DEF long BGl_bitzd2notzd2zz__bitz00(long BgL_xz00_77)
{
{ /* Llib/bit.scm 593 */
return 
~(BgL_xz00_77);} 

}



/* &bit-not */
obj_t BGl_z62bitzd2notzb0zz__bitz00(obj_t BgL_envz00_1322, obj_t BgL_xz00_1323)
{
{ /* Llib/bit.scm 593 */
{ /* Llib/bit.scm 593 */
 long BgL_tmpz00_2518;
{ /* Llib/bit.scm 593 */
 long BgL_auxz00_2519;
{ /* Llib/bit.scm 593 */
 obj_t BgL_tmpz00_2520;
if(
INTEGERP(BgL_xz00_1323))
{ /* Llib/bit.scm 593 */
BgL_tmpz00_2520 = BgL_xz00_1323
; }  else 
{ 
 obj_t BgL_auxz00_2523;
BgL_auxz00_2523 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27681L), BGl_string1738z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1323); 
FAILURE(BgL_auxz00_2523,BFALSE,BFALSE);} 
BgL_auxz00_2519 = 
(long)CINT(BgL_tmpz00_2520); } 
BgL_tmpz00_2518 = 
BGl_bitzd2notzd2zz__bitz00(BgL_auxz00_2519); } 
return 
BINT(BgL_tmpz00_2518);} } 

}



/* bit-notelong */
BGL_EXPORTED_DEF long BGl_bitzd2notelongzd2zz__bitz00(long BgL_xz00_78)
{
{ /* Llib/bit.scm 594 */
return 
~(BgL_xz00_78);} 

}



/* &bit-notelong */
obj_t BGl_z62bitzd2notelongzb0zz__bitz00(obj_t BgL_envz00_1324, obj_t BgL_xz00_1325)
{
{ /* Llib/bit.scm 594 */
{ /* Llib/bit.scm 594 */
 long BgL_tmpz00_2531;
{ /* Llib/bit.scm 594 */
 long BgL_auxz00_2532;
{ /* Llib/bit.scm 594 */
 obj_t BgL_tmpz00_2533;
if(
ELONGP(BgL_xz00_1325))
{ /* Llib/bit.scm 594 */
BgL_tmpz00_2533 = BgL_xz00_1325
; }  else 
{ 
 obj_t BgL_auxz00_2536;
BgL_auxz00_2536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27726L), BGl_string1739z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1325); 
FAILURE(BgL_auxz00_2536,BFALSE,BFALSE);} 
BgL_auxz00_2532 = 
BELONG_TO_LONG(BgL_tmpz00_2533); } 
BgL_tmpz00_2531 = 
BGl_bitzd2notelongzd2zz__bitz00(BgL_auxz00_2532); } 
return 
make_belong(BgL_tmpz00_2531);} } 

}



/* bit-notllong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2notllongzd2zz__bitz00(BGL_LONGLONG_T BgL_xz00_79)
{
{ /* Llib/bit.scm 595 */
return 
~(BgL_xz00_79);} 

}



/* &bit-notllong */
obj_t BGl_z62bitzd2notllongzb0zz__bitz00(obj_t BgL_envz00_1326, obj_t BgL_xz00_1327)
{
{ /* Llib/bit.scm 595 */
{ /* Llib/bit.scm 595 */
 BGL_LONGLONG_T BgL_tmpz00_2544;
{ /* Llib/bit.scm 595 */
 BGL_LONGLONG_T BgL_auxz00_2545;
{ /* Llib/bit.scm 595 */
 obj_t BgL_tmpz00_2546;
if(
LLONGP(BgL_xz00_1327))
{ /* Llib/bit.scm 595 */
BgL_tmpz00_2546 = BgL_xz00_1327
; }  else 
{ 
 obj_t BgL_auxz00_2549;
BgL_auxz00_2549 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27776L), BGl_string1740z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1327); 
FAILURE(BgL_auxz00_2549,BFALSE,BFALSE);} 
BgL_auxz00_2545 = 
BLLONG_TO_LLONG(BgL_tmpz00_2546); } 
BgL_tmpz00_2544 = 
BGl_bitzd2notllongzd2zz__bitz00(BgL_auxz00_2545); } 
return 
make_bllong(BgL_tmpz00_2544);} } 

}



/* bit-nots8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2nots8zd2zz__bitz00(int8_t BgL_xz00_80)
{
{ /* Llib/bit.scm 596 */
return 
~(BgL_xz00_80);} 

}



/* &bit-nots8 */
obj_t BGl_z62bitzd2nots8zb0zz__bitz00(obj_t BgL_envz00_1328, obj_t BgL_xz00_1329)
{
{ /* Llib/bit.scm 596 */
{ /* Llib/bit.scm 596 */
 int8_t BgL_tmpz00_2557;
{ /* Llib/bit.scm 596 */
 int8_t BgL_auxz00_2558;
{ /* Llib/bit.scm 596 */
 obj_t BgL_tmpz00_2559;
if(
BGL_INT8P(BgL_xz00_1329))
{ /* Llib/bit.scm 596 */
BgL_tmpz00_2559 = BgL_xz00_1329
; }  else 
{ 
 obj_t BgL_auxz00_2562;
BgL_auxz00_2562 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27823L), BGl_string1741z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1329); 
FAILURE(BgL_auxz00_2562,BFALSE,BFALSE);} 
BgL_auxz00_2558 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_2559); } 
BgL_tmpz00_2557 = 
BGl_bitzd2nots8zd2zz__bitz00(BgL_auxz00_2558); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_2557);} } 

}



/* bit-notu8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2notu8zd2zz__bitz00(uint8_t BgL_xz00_81)
{
{ /* Llib/bit.scm 597 */
return 
~(BgL_xz00_81);} 

}



/* &bit-notu8 */
obj_t BGl_z62bitzd2notu8zb0zz__bitz00(obj_t BgL_envz00_1330, obj_t BgL_xz00_1331)
{
{ /* Llib/bit.scm 597 */
{ /* Llib/bit.scm 597 */
 uint8_t BgL_tmpz00_2570;
{ /* Llib/bit.scm 597 */
 uint8_t BgL_auxz00_2571;
{ /* Llib/bit.scm 597 */
 obj_t BgL_tmpz00_2572;
if(
BGL_UINT8P(BgL_xz00_1331))
{ /* Llib/bit.scm 597 */
BgL_tmpz00_2572 = BgL_xz00_1331
; }  else 
{ 
 obj_t BgL_auxz00_2575;
BgL_auxz00_2575 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27867L), BGl_string1742z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1331); 
FAILURE(BgL_auxz00_2575,BFALSE,BFALSE);} 
BgL_auxz00_2571 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_2572); } 
BgL_tmpz00_2570 = 
BGl_bitzd2notu8zd2zz__bitz00(BgL_auxz00_2571); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_2570);} } 

}



/* bit-nots16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2nots16zd2zz__bitz00(int16_t BgL_xz00_82)
{
{ /* Llib/bit.scm 598 */
return 
~(BgL_xz00_82);} 

}



/* &bit-nots16 */
obj_t BGl_z62bitzd2nots16zb0zz__bitz00(obj_t BgL_envz00_1332, obj_t BgL_xz00_1333)
{
{ /* Llib/bit.scm 598 */
{ /* Llib/bit.scm 598 */
 int16_t BgL_tmpz00_2583;
{ /* Llib/bit.scm 598 */
 int16_t BgL_auxz00_2584;
{ /* Llib/bit.scm 598 */
 obj_t BgL_tmpz00_2585;
if(
BGL_INT16P(BgL_xz00_1333))
{ /* Llib/bit.scm 598 */
BgL_tmpz00_2585 = BgL_xz00_1333
; }  else 
{ 
 obj_t BgL_auxz00_2588;
BgL_auxz00_2588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27912L), BGl_string1743z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1333); 
FAILURE(BgL_auxz00_2588,BFALSE,BFALSE);} 
BgL_auxz00_2584 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2585); } 
BgL_tmpz00_2583 = 
BGl_bitzd2nots16zd2zz__bitz00(BgL_auxz00_2584); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_2583);} } 

}



/* bit-notu16 */
BGL_EXPORTED_DEF uint16_t BGl_bitzd2notu16zd2zz__bitz00(uint16_t BgL_xz00_83)
{
{ /* Llib/bit.scm 599 */
return 
~(BgL_xz00_83);} 

}



/* &bit-notu16 */
obj_t BGl_z62bitzd2notu16zb0zz__bitz00(obj_t BgL_envz00_1334, obj_t BgL_xz00_1335)
{
{ /* Llib/bit.scm 599 */
{ /* Llib/bit.scm 599 */
 uint16_t BgL_tmpz00_2596;
{ /* Llib/bit.scm 599 */
 uint16_t BgL_auxz00_2597;
{ /* Llib/bit.scm 599 */
 obj_t BgL_tmpz00_2598;
if(
BGL_UINT16P(BgL_xz00_1335))
{ /* Llib/bit.scm 599 */
BgL_tmpz00_2598 = BgL_xz00_1335
; }  else 
{ 
 obj_t BgL_auxz00_2601;
BgL_auxz00_2601 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(27958L), BGl_string1744z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_xz00_1335); 
FAILURE(BgL_auxz00_2601,BFALSE,BFALSE);} 
BgL_auxz00_2597 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_2598); } 
BgL_tmpz00_2596 = 
BGl_bitzd2notu16zd2zz__bitz00(BgL_auxz00_2597); } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_2596);} } 

}



/* bit-nots32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2nots32zd2zz__bitz00(int32_t BgL_xz00_84)
{
{ /* Llib/bit.scm 600 */
return 
~(BgL_xz00_84);} 

}



/* &bit-nots32 */
obj_t BGl_z62bitzd2nots32zb0zz__bitz00(obj_t BgL_envz00_1336, obj_t BgL_xz00_1337)
{
{ /* Llib/bit.scm 600 */
{ /* Llib/bit.scm 600 */
 int32_t BgL_tmpz00_2609;
{ /* Llib/bit.scm 600 */
 int32_t BgL_auxz00_2610;
{ /* Llib/bit.scm 600 */
 obj_t BgL_tmpz00_2611;
if(
BGL_INT32P(BgL_xz00_1337))
{ /* Llib/bit.scm 600 */
BgL_tmpz00_2611 = BgL_xz00_1337
; }  else 
{ 
 obj_t BgL_auxz00_2614;
BgL_auxz00_2614 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28004L), BGl_string1745z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1337); 
FAILURE(BgL_auxz00_2614,BFALSE,BFALSE);} 
BgL_auxz00_2610 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_2611); } 
BgL_tmpz00_2609 = 
BGl_bitzd2nots32zd2zz__bitz00(BgL_auxz00_2610); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_2609);} } 

}



/* bit-notu32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2notu32zd2zz__bitz00(uint32_t BgL_xz00_85)
{
{ /* Llib/bit.scm 601 */
return 
~(BgL_xz00_85);} 

}



/* &bit-notu32 */
obj_t BGl_z62bitzd2notu32zb0zz__bitz00(obj_t BgL_envz00_1338, obj_t BgL_xz00_1339)
{
{ /* Llib/bit.scm 601 */
{ /* Llib/bit.scm 601 */
 uint32_t BgL_tmpz00_2622;
{ /* Llib/bit.scm 601 */
 uint32_t BgL_auxz00_2623;
{ /* Llib/bit.scm 601 */
 obj_t BgL_tmpz00_2624;
if(
BGL_UINT32P(BgL_xz00_1339))
{ /* Llib/bit.scm 601 */
BgL_tmpz00_2624 = BgL_xz00_1339
; }  else 
{ 
 obj_t BgL_auxz00_2627;
BgL_auxz00_2627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28050L), BGl_string1746z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1339); 
FAILURE(BgL_auxz00_2627,BFALSE,BFALSE);} 
BgL_auxz00_2623 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_2624); } 
BgL_tmpz00_2622 = 
BGl_bitzd2notu32zd2zz__bitz00(BgL_auxz00_2623); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_2622);} } 

}



/* bit-nots64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2nots64zd2zz__bitz00(int64_t BgL_xz00_86)
{
{ /* Llib/bit.scm 602 */
return 
~(BgL_xz00_86);} 

}



/* &bit-nots64 */
obj_t BGl_z62bitzd2nots64zb0zz__bitz00(obj_t BgL_envz00_1340, obj_t BgL_xz00_1341)
{
{ /* Llib/bit.scm 602 */
{ /* Llib/bit.scm 602 */
 int64_t BgL_tmpz00_2635;
{ /* Llib/bit.scm 602 */
 int64_t BgL_auxz00_2636;
{ /* Llib/bit.scm 602 */
 obj_t BgL_tmpz00_2637;
if(
BGL_INT64P(BgL_xz00_1341))
{ /* Llib/bit.scm 602 */
BgL_tmpz00_2637 = BgL_xz00_1341
; }  else 
{ 
 obj_t BgL_auxz00_2640;
BgL_auxz00_2640 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28096L), BGl_string1747z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1341); 
FAILURE(BgL_auxz00_2640,BFALSE,BFALSE);} 
BgL_auxz00_2636 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2637); } 
BgL_tmpz00_2635 = 
BGl_bitzd2nots64zd2zz__bitz00(BgL_auxz00_2636); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_2635);} } 

}



/* bit-notu64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2notu64zd2zz__bitz00(uint64_t BgL_xz00_87)
{
{ /* Llib/bit.scm 603 */
return 
~(BgL_xz00_87);} 

}



/* &bit-notu64 */
obj_t BGl_z62bitzd2notu64zb0zz__bitz00(obj_t BgL_envz00_1342, obj_t BgL_xz00_1343)
{
{ /* Llib/bit.scm 603 */
{ /* Llib/bit.scm 603 */
 uint64_t BgL_tmpz00_2648;
{ /* Llib/bit.scm 603 */
 uint64_t BgL_auxz00_2649;
{ /* Llib/bit.scm 603 */
 obj_t BgL_tmpz00_2650;
if(
BGL_UINT64P(BgL_xz00_1343))
{ /* Llib/bit.scm 603 */
BgL_tmpz00_2650 = BgL_xz00_1343
; }  else 
{ 
 obj_t BgL_auxz00_2653;
BgL_auxz00_2653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28142L), BGl_string1748z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1343); 
FAILURE(BgL_auxz00_2653,BFALSE,BFALSE);} 
BgL_auxz00_2649 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2650); } 
BgL_tmpz00_2648 = 
BGl_bitzd2notu64zd2zz__bitz00(BgL_auxz00_2649); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_2648);} } 

}



/* bit-notbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2notbxzd2zz__bitz00(obj_t BgL_xz00_88)
{
{ /* Llib/bit.scm 604 */
BGL_TAIL return 
bgl_bignum_not(BgL_xz00_88);} 

}



/* &bit-notbx */
obj_t BGl_z62bitzd2notbxzb0zz__bitz00(obj_t BgL_envz00_1344, obj_t BgL_xz00_1345)
{
{ /* Llib/bit.scm 604 */
{ /* Llib/bit.scm 604 */
 obj_t BgL_auxz00_2661;
if(
BIGNUMP(BgL_xz00_1345))
{ /* Llib/bit.scm 604 */
BgL_auxz00_2661 = BgL_xz00_1345
; }  else 
{ 
 obj_t BgL_auxz00_2664;
BgL_auxz00_2664 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28187L), BGl_string1749z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1345); 
FAILURE(BgL_auxz00_2664,BFALSE,BFALSE);} 
return 
BGl_bitzd2notbxzd2zz__bitz00(BgL_auxz00_2661);} } 

}



/* bit-rsh */
BGL_EXPORTED_DEF long BGl_bitzd2rshzd2zz__bitz00(long BgL_xz00_89, long BgL_yz00_90)
{
{ /* Llib/bit.scm 609 */
return 
(BgL_xz00_89 >> 
(int)(BgL_yz00_90));} 

}



/* &bit-rsh */
obj_t BGl_z62bitzd2rshzb0zz__bitz00(obj_t BgL_envz00_1346, obj_t BgL_xz00_1347, obj_t BgL_yz00_1348)
{
{ /* Llib/bit.scm 609 */
{ /* Llib/bit.scm 609 */
 long BgL_tmpz00_2671;
{ /* Llib/bit.scm 609 */
 long BgL_auxz00_2681; long BgL_auxz00_2672;
{ /* Llib/bit.scm 609 */
 obj_t BgL_tmpz00_2682;
if(
INTEGERP(BgL_yz00_1348))
{ /* Llib/bit.scm 609 */
BgL_tmpz00_2682 = BgL_yz00_1348
; }  else 
{ 
 obj_t BgL_auxz00_2685;
BgL_auxz00_2685 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28457L), BGl_string1750z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1348); 
FAILURE(BgL_auxz00_2685,BFALSE,BFALSE);} 
BgL_auxz00_2681 = 
(long)CINT(BgL_tmpz00_2682); } 
{ /* Llib/bit.scm 609 */
 obj_t BgL_tmpz00_2673;
if(
INTEGERP(BgL_xz00_1347))
{ /* Llib/bit.scm 609 */
BgL_tmpz00_2673 = BgL_xz00_1347
; }  else 
{ 
 obj_t BgL_auxz00_2676;
BgL_auxz00_2676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28457L), BGl_string1750z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1347); 
FAILURE(BgL_auxz00_2676,BFALSE,BFALSE);} 
BgL_auxz00_2672 = 
(long)CINT(BgL_tmpz00_2673); } 
BgL_tmpz00_2671 = 
BGl_bitzd2rshzd2zz__bitz00(BgL_auxz00_2672, BgL_auxz00_2681); } 
return 
BINT(BgL_tmpz00_2671);} } 

}



/* bit-rshelong */
BGL_EXPORTED_DEF long BGl_bitzd2rshelongzd2zz__bitz00(long BgL_xz00_91, long BgL_yz00_92)
{
{ /* Llib/bit.scm 610 */
return 
(BgL_xz00_91 >> 
(int)(BgL_yz00_92));} 

}



/* &bit-rshelong */
obj_t BGl_z62bitzd2rshelongzb0zz__bitz00(obj_t BgL_envz00_1349, obj_t BgL_xz00_1350, obj_t BgL_yz00_1351)
{
{ /* Llib/bit.scm 610 */
{ /* Llib/bit.scm 610 */
 long BgL_tmpz00_2694;
{ /* Llib/bit.scm 610 */
 long BgL_auxz00_2704; long BgL_auxz00_2695;
{ /* Llib/bit.scm 610 */
 obj_t BgL_tmpz00_2705;
if(
INTEGERP(BgL_yz00_1351))
{ /* Llib/bit.scm 610 */
BgL_tmpz00_2705 = BgL_yz00_1351
; }  else 
{ 
 obj_t BgL_auxz00_2708;
BgL_auxz00_2708 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28506L), BGl_string1751z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1351); 
FAILURE(BgL_auxz00_2708,BFALSE,BFALSE);} 
BgL_auxz00_2704 = 
(long)CINT(BgL_tmpz00_2705); } 
{ /* Llib/bit.scm 610 */
 obj_t BgL_tmpz00_2696;
if(
ELONGP(BgL_xz00_1350))
{ /* Llib/bit.scm 610 */
BgL_tmpz00_2696 = BgL_xz00_1350
; }  else 
{ 
 obj_t BgL_auxz00_2699;
BgL_auxz00_2699 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28506L), BGl_string1751z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1350); 
FAILURE(BgL_auxz00_2699,BFALSE,BFALSE);} 
BgL_auxz00_2695 = 
BELONG_TO_LONG(BgL_tmpz00_2696); } 
BgL_tmpz00_2694 = 
BGl_bitzd2rshelongzd2zz__bitz00(BgL_auxz00_2695, BgL_auxz00_2704); } 
return 
make_belong(BgL_tmpz00_2694);} } 

}



/* bit-rshllong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2rshllongzd2zz__bitz00(BGL_LONGLONG_T BgL_xz00_93, long BgL_yz00_94)
{
{ /* Llib/bit.scm 611 */
return 
(BgL_xz00_93 >> 
(int)(BgL_yz00_94));} 

}



/* &bit-rshllong */
obj_t BGl_z62bitzd2rshllongzb0zz__bitz00(obj_t BgL_envz00_1352, obj_t BgL_xz00_1353, obj_t BgL_yz00_1354)
{
{ /* Llib/bit.scm 611 */
{ /* Llib/bit.scm 611 */
 BGL_LONGLONG_T BgL_tmpz00_2717;
{ /* Llib/bit.scm 611 */
 long BgL_auxz00_2727; BGL_LONGLONG_T BgL_auxz00_2718;
{ /* Llib/bit.scm 611 */
 obj_t BgL_tmpz00_2728;
if(
INTEGERP(BgL_yz00_1354))
{ /* Llib/bit.scm 611 */
BgL_tmpz00_2728 = BgL_yz00_1354
; }  else 
{ 
 obj_t BgL_auxz00_2731;
BgL_auxz00_2731 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28560L), BGl_string1752z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1354); 
FAILURE(BgL_auxz00_2731,BFALSE,BFALSE);} 
BgL_auxz00_2727 = 
(long)CINT(BgL_tmpz00_2728); } 
{ /* Llib/bit.scm 611 */
 obj_t BgL_tmpz00_2719;
if(
LLONGP(BgL_xz00_1353))
{ /* Llib/bit.scm 611 */
BgL_tmpz00_2719 = BgL_xz00_1353
; }  else 
{ 
 obj_t BgL_auxz00_2722;
BgL_auxz00_2722 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28560L), BGl_string1752z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1353); 
FAILURE(BgL_auxz00_2722,BFALSE,BFALSE);} 
BgL_auxz00_2718 = 
BLLONG_TO_LLONG(BgL_tmpz00_2719); } 
BgL_tmpz00_2717 = 
BGl_bitzd2rshllongzd2zz__bitz00(BgL_auxz00_2718, BgL_auxz00_2727); } 
return 
make_bllong(BgL_tmpz00_2717);} } 

}



/* bit-rshs8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2rshs8zd2zz__bitz00(int8_t BgL_xz00_95, long BgL_yz00_96)
{
{ /* Llib/bit.scm 612 */
return 
(BgL_xz00_95 >> 
(int)(BgL_yz00_96));} 

}



/* &bit-rshs8 */
obj_t BGl_z62bitzd2rshs8zb0zz__bitz00(obj_t BgL_envz00_1355, obj_t BgL_xz00_1356, obj_t BgL_yz00_1357)
{
{ /* Llib/bit.scm 612 */
{ /* Llib/bit.scm 612 */
 int8_t BgL_tmpz00_2740;
{ /* Llib/bit.scm 612 */
 long BgL_auxz00_2750; int8_t BgL_auxz00_2741;
{ /* Llib/bit.scm 612 */
 obj_t BgL_tmpz00_2751;
if(
INTEGERP(BgL_yz00_1357))
{ /* Llib/bit.scm 612 */
BgL_tmpz00_2751 = BgL_yz00_1357
; }  else 
{ 
 obj_t BgL_auxz00_2754;
BgL_auxz00_2754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28611L), BGl_string1753z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1357); 
FAILURE(BgL_auxz00_2754,BFALSE,BFALSE);} 
BgL_auxz00_2750 = 
(long)CINT(BgL_tmpz00_2751); } 
{ /* Llib/bit.scm 612 */
 obj_t BgL_tmpz00_2742;
if(
BGL_INT8P(BgL_xz00_1356))
{ /* Llib/bit.scm 612 */
BgL_tmpz00_2742 = BgL_xz00_1356
; }  else 
{ 
 obj_t BgL_auxz00_2745;
BgL_auxz00_2745 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28611L), BGl_string1753z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1356); 
FAILURE(BgL_auxz00_2745,BFALSE,BFALSE);} 
BgL_auxz00_2741 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_2742); } 
BgL_tmpz00_2740 = 
BGl_bitzd2rshs8zd2zz__bitz00(BgL_auxz00_2741, BgL_auxz00_2750); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_2740);} } 

}



/* bit-rshu8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2rshu8zd2zz__bitz00(uint8_t BgL_xz00_97, long BgL_yz00_98)
{
{ /* Llib/bit.scm 613 */
return 
(BgL_xz00_97 >> 
(int)(BgL_yz00_98));} 

}



/* &bit-rshu8 */
obj_t BGl_z62bitzd2rshu8zb0zz__bitz00(obj_t BgL_envz00_1358, obj_t BgL_xz00_1359, obj_t BgL_yz00_1360)
{
{ /* Llib/bit.scm 613 */
{ /* Llib/bit.scm 613 */
 uint8_t BgL_tmpz00_2763;
{ /* Llib/bit.scm 613 */
 long BgL_auxz00_2773; uint8_t BgL_auxz00_2764;
{ /* Llib/bit.scm 613 */
 obj_t BgL_tmpz00_2774;
if(
INTEGERP(BgL_yz00_1360))
{ /* Llib/bit.scm 613 */
BgL_tmpz00_2774 = BgL_yz00_1360
; }  else 
{ 
 obj_t BgL_auxz00_2777;
BgL_auxz00_2777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28659L), BGl_string1754z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1360); 
FAILURE(BgL_auxz00_2777,BFALSE,BFALSE);} 
BgL_auxz00_2773 = 
(long)CINT(BgL_tmpz00_2774); } 
{ /* Llib/bit.scm 613 */
 obj_t BgL_tmpz00_2765;
if(
BGL_UINT8P(BgL_xz00_1359))
{ /* Llib/bit.scm 613 */
BgL_tmpz00_2765 = BgL_xz00_1359
; }  else 
{ 
 obj_t BgL_auxz00_2768;
BgL_auxz00_2768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28659L), BGl_string1754z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1359); 
FAILURE(BgL_auxz00_2768,BFALSE,BFALSE);} 
BgL_auxz00_2764 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_2765); } 
BgL_tmpz00_2763 = 
BGl_bitzd2rshu8zd2zz__bitz00(BgL_auxz00_2764, BgL_auxz00_2773); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_2763);} } 

}



/* bit-rshs16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2rshs16zd2zz__bitz00(int16_t BgL_xz00_99, long BgL_yz00_100)
{
{ /* Llib/bit.scm 614 */
return 
(BgL_xz00_99 >> 
(int)(BgL_yz00_100));} 

}



/* &bit-rshs16 */
obj_t BGl_z62bitzd2rshs16zb0zz__bitz00(obj_t BgL_envz00_1361, obj_t BgL_xz00_1362, obj_t BgL_yz00_1363)
{
{ /* Llib/bit.scm 614 */
{ /* Llib/bit.scm 614 */
 int16_t BgL_tmpz00_2786;
{ /* Llib/bit.scm 614 */
 long BgL_auxz00_2796; int16_t BgL_auxz00_2787;
{ /* Llib/bit.scm 614 */
 obj_t BgL_tmpz00_2797;
if(
INTEGERP(BgL_yz00_1363))
{ /* Llib/bit.scm 614 */
BgL_tmpz00_2797 = BgL_yz00_1363
; }  else 
{ 
 obj_t BgL_auxz00_2800;
BgL_auxz00_2800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28708L), BGl_string1755z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1363); 
FAILURE(BgL_auxz00_2800,BFALSE,BFALSE);} 
BgL_auxz00_2796 = 
(long)CINT(BgL_tmpz00_2797); } 
{ /* Llib/bit.scm 614 */
 obj_t BgL_tmpz00_2788;
if(
BGL_INT16P(BgL_xz00_1362))
{ /* Llib/bit.scm 614 */
BgL_tmpz00_2788 = BgL_xz00_1362
; }  else 
{ 
 obj_t BgL_auxz00_2791;
BgL_auxz00_2791 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28708L), BGl_string1755z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1362); 
FAILURE(BgL_auxz00_2791,BFALSE,BFALSE);} 
BgL_auxz00_2787 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2788); } 
BgL_tmpz00_2786 = 
BGl_bitzd2rshs16zd2zz__bitz00(BgL_auxz00_2787, BgL_auxz00_2796); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_2786);} } 

}



/* bit-rshu16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2rshu16zd2zz__bitz00(int16_t BgL_xz00_101, long BgL_yz00_102)
{
{ /* Llib/bit.scm 615 */
{ /* Llib/bit.scm 615 */
 uint16_t BgL_tmpz00_2807;
BgL_tmpz00_2807 = 
(
(uint16_t)(BgL_xz00_101) >> 
(int)(BgL_yz00_102)); 
return 
(int16_t)(BgL_tmpz00_2807);} } 

}



/* &bit-rshu16 */
obj_t BGl_z62bitzd2rshu16zb0zz__bitz00(obj_t BgL_envz00_1364, obj_t BgL_xz00_1365, obj_t BgL_yz00_1366)
{
{ /* Llib/bit.scm 615 */
{ /* Llib/bit.scm 615 */
 int16_t BgL_tmpz00_2812;
{ /* Llib/bit.scm 615 */
 long BgL_auxz00_2822; int16_t BgL_auxz00_2813;
{ /* Llib/bit.scm 615 */
 obj_t BgL_tmpz00_2823;
if(
INTEGERP(BgL_yz00_1366))
{ /* Llib/bit.scm 615 */
BgL_tmpz00_2823 = BgL_yz00_1366
; }  else 
{ 
 obj_t BgL_auxz00_2826;
BgL_auxz00_2826 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28758L), BGl_string1756z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1366); 
FAILURE(BgL_auxz00_2826,BFALSE,BFALSE);} 
BgL_auxz00_2822 = 
(long)CINT(BgL_tmpz00_2823); } 
{ /* Llib/bit.scm 615 */
 obj_t BgL_tmpz00_2814;
if(
BGL_INT16P(BgL_xz00_1365))
{ /* Llib/bit.scm 615 */
BgL_tmpz00_2814 = BgL_xz00_1365
; }  else 
{ 
 obj_t BgL_auxz00_2817;
BgL_auxz00_2817 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28758L), BGl_string1756z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1365); 
FAILURE(BgL_auxz00_2817,BFALSE,BFALSE);} 
BgL_auxz00_2813 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_2814); } 
BgL_tmpz00_2812 = 
BGl_bitzd2rshu16zd2zz__bitz00(BgL_auxz00_2813, BgL_auxz00_2822); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_2812);} } 

}



/* bit-rshs32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2rshs32zd2zz__bitz00(int32_t BgL_xz00_103, long BgL_yz00_104)
{
{ /* Llib/bit.scm 616 */
return 
(BgL_xz00_103 >> 
(int)(BgL_yz00_104));} 

}



/* &bit-rshs32 */
obj_t BGl_z62bitzd2rshs32zb0zz__bitz00(obj_t BgL_envz00_1367, obj_t BgL_xz00_1368, obj_t BgL_yz00_1369)
{
{ /* Llib/bit.scm 616 */
{ /* Llib/bit.scm 616 */
 int32_t BgL_tmpz00_2835;
{ /* Llib/bit.scm 616 */
 long BgL_auxz00_2845; int32_t BgL_auxz00_2836;
{ /* Llib/bit.scm 616 */
 obj_t BgL_tmpz00_2846;
if(
INTEGERP(BgL_yz00_1369))
{ /* Llib/bit.scm 616 */
BgL_tmpz00_2846 = BgL_yz00_1369
; }  else 
{ 
 obj_t BgL_auxz00_2849;
BgL_auxz00_2849 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28808L), BGl_string1757z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1369); 
FAILURE(BgL_auxz00_2849,BFALSE,BFALSE);} 
BgL_auxz00_2845 = 
(long)CINT(BgL_tmpz00_2846); } 
{ /* Llib/bit.scm 616 */
 obj_t BgL_tmpz00_2837;
if(
BGL_INT32P(BgL_xz00_1368))
{ /* Llib/bit.scm 616 */
BgL_tmpz00_2837 = BgL_xz00_1368
; }  else 
{ 
 obj_t BgL_auxz00_2840;
BgL_auxz00_2840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28808L), BGl_string1757z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1368); 
FAILURE(BgL_auxz00_2840,BFALSE,BFALSE);} 
BgL_auxz00_2836 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_2837); } 
BgL_tmpz00_2835 = 
BGl_bitzd2rshs32zd2zz__bitz00(BgL_auxz00_2836, BgL_auxz00_2845); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_2835);} } 

}



/* bit-rshu32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2rshu32zd2zz__bitz00(uint32_t BgL_xz00_105, long BgL_yz00_106)
{
{ /* Llib/bit.scm 617 */
return 
(BgL_xz00_105 >> 
(int)(BgL_yz00_106));} 

}



/* &bit-rshu32 */
obj_t BGl_z62bitzd2rshu32zb0zz__bitz00(obj_t BgL_envz00_1370, obj_t BgL_xz00_1371, obj_t BgL_yz00_1372)
{
{ /* Llib/bit.scm 617 */
{ /* Llib/bit.scm 617 */
 uint32_t BgL_tmpz00_2858;
{ /* Llib/bit.scm 617 */
 long BgL_auxz00_2868; uint32_t BgL_auxz00_2859;
{ /* Llib/bit.scm 617 */
 obj_t BgL_tmpz00_2869;
if(
INTEGERP(BgL_yz00_1372))
{ /* Llib/bit.scm 617 */
BgL_tmpz00_2869 = BgL_yz00_1372
; }  else 
{ 
 obj_t BgL_auxz00_2872;
BgL_auxz00_2872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28858L), BGl_string1758z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1372); 
FAILURE(BgL_auxz00_2872,BFALSE,BFALSE);} 
BgL_auxz00_2868 = 
(long)CINT(BgL_tmpz00_2869); } 
{ /* Llib/bit.scm 617 */
 obj_t BgL_tmpz00_2860;
if(
BGL_UINT32P(BgL_xz00_1371))
{ /* Llib/bit.scm 617 */
BgL_tmpz00_2860 = BgL_xz00_1371
; }  else 
{ 
 obj_t BgL_auxz00_2863;
BgL_auxz00_2863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28858L), BGl_string1758z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1371); 
FAILURE(BgL_auxz00_2863,BFALSE,BFALSE);} 
BgL_auxz00_2859 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_2860); } 
BgL_tmpz00_2858 = 
BGl_bitzd2rshu32zd2zz__bitz00(BgL_auxz00_2859, BgL_auxz00_2868); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_2858);} } 

}



/* bit-rshs64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2rshs64zd2zz__bitz00(int64_t BgL_xz00_107, long BgL_yz00_108)
{
{ /* Llib/bit.scm 618 */
return 
(BgL_xz00_107 >> 
(int)(BgL_yz00_108));} 

}



/* &bit-rshs64 */
obj_t BGl_z62bitzd2rshs64zb0zz__bitz00(obj_t BgL_envz00_1373, obj_t BgL_xz00_1374, obj_t BgL_yz00_1375)
{
{ /* Llib/bit.scm 618 */
{ /* Llib/bit.scm 618 */
 int64_t BgL_tmpz00_2881;
{ /* Llib/bit.scm 618 */
 long BgL_auxz00_2891; int64_t BgL_auxz00_2882;
{ /* Llib/bit.scm 618 */
 obj_t BgL_tmpz00_2892;
if(
INTEGERP(BgL_yz00_1375))
{ /* Llib/bit.scm 618 */
BgL_tmpz00_2892 = BgL_yz00_1375
; }  else 
{ 
 obj_t BgL_auxz00_2895;
BgL_auxz00_2895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28908L), BGl_string1759z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1375); 
FAILURE(BgL_auxz00_2895,BFALSE,BFALSE);} 
BgL_auxz00_2891 = 
(long)CINT(BgL_tmpz00_2892); } 
{ /* Llib/bit.scm 618 */
 obj_t BgL_tmpz00_2883;
if(
BGL_INT64P(BgL_xz00_1374))
{ /* Llib/bit.scm 618 */
BgL_tmpz00_2883 = BgL_xz00_1374
; }  else 
{ 
 obj_t BgL_auxz00_2886;
BgL_auxz00_2886 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28908L), BGl_string1759z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1374); 
FAILURE(BgL_auxz00_2886,BFALSE,BFALSE);} 
BgL_auxz00_2882 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2883); } 
BgL_tmpz00_2881 = 
BGl_bitzd2rshs64zd2zz__bitz00(BgL_auxz00_2882, BgL_auxz00_2891); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_2881);} } 

}



/* bit-rshu64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2rshu64zd2zz__bitz00(uint64_t BgL_xz00_109, long BgL_yz00_110)
{
{ /* Llib/bit.scm 619 */
return 
(BgL_xz00_109 >> 
(int)(BgL_yz00_110));} 

}



/* &bit-rshu64 */
obj_t BGl_z62bitzd2rshu64zb0zz__bitz00(obj_t BgL_envz00_1376, obj_t BgL_xz00_1377, obj_t BgL_yz00_1378)
{
{ /* Llib/bit.scm 619 */
{ /* Llib/bit.scm 619 */
 uint64_t BgL_tmpz00_2904;
{ /* Llib/bit.scm 619 */
 long BgL_auxz00_2914; uint64_t BgL_auxz00_2905;
{ /* Llib/bit.scm 619 */
 obj_t BgL_tmpz00_2915;
if(
INTEGERP(BgL_yz00_1378))
{ /* Llib/bit.scm 619 */
BgL_tmpz00_2915 = BgL_yz00_1378
; }  else 
{ 
 obj_t BgL_auxz00_2918;
BgL_auxz00_2918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28958L), BGl_string1760z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1378); 
FAILURE(BgL_auxz00_2918,BFALSE,BFALSE);} 
BgL_auxz00_2914 = 
(long)CINT(BgL_tmpz00_2915); } 
{ /* Llib/bit.scm 619 */
 obj_t BgL_tmpz00_2906;
if(
BGL_UINT64P(BgL_xz00_1377))
{ /* Llib/bit.scm 619 */
BgL_tmpz00_2906 = BgL_xz00_1377
; }  else 
{ 
 obj_t BgL_auxz00_2909;
BgL_auxz00_2909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(28958L), BGl_string1760z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1377); 
FAILURE(BgL_auxz00_2909,BFALSE,BFALSE);} 
BgL_auxz00_2905 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_2906); } 
BgL_tmpz00_2904 = 
BGl_bitzd2rshu64zd2zz__bitz00(BgL_auxz00_2905, BgL_auxz00_2914); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_2904);} } 

}



/* bit-rshbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2rshbxzd2zz__bitz00(obj_t BgL_xz00_111, long BgL_yz00_112)
{
{ /* Llib/bit.scm 620 */
BGL_TAIL return 
bgl_bignum_rsh(BgL_xz00_111, BgL_yz00_112);} 

}



/* &bit-rshbx */
obj_t BGl_z62bitzd2rshbxzb0zz__bitz00(obj_t BgL_envz00_1379, obj_t BgL_xz00_1380, obj_t BgL_yz00_1381)
{
{ /* Llib/bit.scm 620 */
{ /* Llib/bit.scm 620 */
 long BgL_auxz00_2933; obj_t BgL_auxz00_2926;
{ /* Llib/bit.scm 620 */
 obj_t BgL_tmpz00_2934;
if(
INTEGERP(BgL_yz00_1381))
{ /* Llib/bit.scm 620 */
BgL_tmpz00_2934 = BgL_yz00_1381
; }  else 
{ 
 obj_t BgL_auxz00_2937;
BgL_auxz00_2937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29007L), BGl_string1761z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1381); 
FAILURE(BgL_auxz00_2937,BFALSE,BFALSE);} 
BgL_auxz00_2933 = 
(long)CINT(BgL_tmpz00_2934); } 
if(
BIGNUMP(BgL_xz00_1380))
{ /* Llib/bit.scm 620 */
BgL_auxz00_2926 = BgL_xz00_1380
; }  else 
{ 
 obj_t BgL_auxz00_2929;
BgL_auxz00_2929 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29007L), BGl_string1761z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1380); 
FAILURE(BgL_auxz00_2929,BFALSE,BFALSE);} 
return 
BGl_bitzd2rshbxzd2zz__bitz00(BgL_auxz00_2926, BgL_auxz00_2933);} } 

}



/* bit-ursh */
BGL_EXPORTED_DEF unsigned long BGl_bitzd2urshzd2zz__bitz00(unsigned long BgL_xz00_113, long BgL_yz00_114)
{
{ /* Llib/bit.scm 625 */
return 
(BgL_xz00_113 >> 
(int)(BgL_yz00_114));} 

}



/* &bit-ursh */
obj_t BGl_z62bitzd2urshzb0zz__bitz00(obj_t BgL_envz00_1382, obj_t BgL_xz00_1383, obj_t BgL_yz00_1384)
{
{ /* Llib/bit.scm 625 */
{ /* Llib/bit.scm 625 */
 unsigned long BgL_tmpz00_2945;
{ /* Llib/bit.scm 625 */
 long BgL_auxz00_2955; unsigned long BgL_auxz00_2946;
{ /* Llib/bit.scm 625 */
 obj_t BgL_tmpz00_2956;
if(
INTEGERP(BgL_yz00_1384))
{ /* Llib/bit.scm 625 */
BgL_tmpz00_2956 = BgL_yz00_1384
; }  else 
{ 
 obj_t BgL_auxz00_2959;
BgL_auxz00_2959 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29277L), BGl_string1762z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1384); 
FAILURE(BgL_auxz00_2959,BFALSE,BFALSE);} 
BgL_auxz00_2955 = 
(long)CINT(BgL_tmpz00_2956); } 
{ /* Llib/bit.scm 625 */
 obj_t BgL_tmpz00_2947;
if(
INTEGERP(BgL_xz00_1383))
{ /* Llib/bit.scm 625 */
BgL_tmpz00_2947 = BgL_xz00_1383
; }  else 
{ 
 obj_t BgL_auxz00_2950;
BgL_auxz00_2950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29277L), BGl_string1762z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1383); 
FAILURE(BgL_auxz00_2950,BFALSE,BFALSE);} 
BgL_auxz00_2946 = 
(unsigned long)CINT(BgL_tmpz00_2947); } 
BgL_tmpz00_2945 = 
BGl_bitzd2urshzd2zz__bitz00(BgL_auxz00_2946, BgL_auxz00_2955); } 
return 
BINT(BgL_tmpz00_2945);} } 

}



/* bit-urshelong */
BGL_EXPORTED_DEF unsigned long BGl_bitzd2urshelongzd2zz__bitz00(unsigned long BgL_xz00_115, long BgL_yz00_116)
{
{ /* Llib/bit.scm 626 */
return 
(BgL_xz00_115 >> 
(int)(BgL_yz00_116));} 

}



/* &bit-urshelong */
obj_t BGl_z62bitzd2urshelongzb0zz__bitz00(obj_t BgL_envz00_1385, obj_t BgL_xz00_1386, obj_t BgL_yz00_1387)
{
{ /* Llib/bit.scm 626 */
{ /* Llib/bit.scm 626 */
 long BgL_tmpz00_2968;
{ /* Llib/bit.scm 626 */
 long BgL_auxz00_2978; unsigned long BgL_auxz00_2969;
{ /* Llib/bit.scm 626 */
 obj_t BgL_tmpz00_2979;
if(
INTEGERP(BgL_yz00_1387))
{ /* Llib/bit.scm 626 */
BgL_tmpz00_2979 = BgL_yz00_1387
; }  else 
{ 
 obj_t BgL_auxz00_2982;
BgL_auxz00_2982 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29328L), BGl_string1763z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1387); 
FAILURE(BgL_auxz00_2982,BFALSE,BFALSE);} 
BgL_auxz00_2978 = 
(long)CINT(BgL_tmpz00_2979); } 
{ /* Llib/bit.scm 626 */
 obj_t BgL_tmpz00_2970;
if(
ELONGP(BgL_xz00_1386))
{ /* Llib/bit.scm 626 */
BgL_tmpz00_2970 = BgL_xz00_1386
; }  else 
{ 
 obj_t BgL_auxz00_2973;
BgL_auxz00_2973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29328L), BGl_string1763z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1386); 
FAILURE(BgL_auxz00_2973,BFALSE,BFALSE);} 
BgL_auxz00_2969 = 
BELONG_TO_LONG(BgL_tmpz00_2970); } 
BgL_tmpz00_2968 = 
BGl_bitzd2urshelongzd2zz__bitz00(BgL_auxz00_2969, BgL_auxz00_2978); } 
return 
make_belong(BgL_tmpz00_2968);} } 

}



/* bit-urshllong */
BGL_EXPORTED_DEF unsigned BGL_LONGLONG_T BGl_bitzd2urshllongzd2zz__bitz00(unsigned BGL_LONGLONG_T BgL_xz00_117, long BgL_yz00_118)
{
{ /* Llib/bit.scm 627 */
return 
(BgL_xz00_117 >> 
(int)(BgL_yz00_118));} 

}



/* &bit-urshllong */
obj_t BGl_z62bitzd2urshllongzb0zz__bitz00(obj_t BgL_envz00_1388, obj_t BgL_xz00_1389, obj_t BgL_yz00_1390)
{
{ /* Llib/bit.scm 627 */
{ /* Llib/bit.scm 627 */
 unsigned BGL_LONGLONG_T BgL_tmpz00_2991;
{ /* Llib/bit.scm 627 */
 long BgL_auxz00_3001; unsigned BGL_LONGLONG_T BgL_auxz00_2992;
{ /* Llib/bit.scm 627 */
 obj_t BgL_tmpz00_3002;
if(
INTEGERP(BgL_yz00_1390))
{ /* Llib/bit.scm 627 */
BgL_tmpz00_3002 = BgL_yz00_1390
; }  else 
{ 
 obj_t BgL_auxz00_3005;
BgL_auxz00_3005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29384L), BGl_string1764z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1390); 
FAILURE(BgL_auxz00_3005,BFALSE,BFALSE);} 
BgL_auxz00_3001 = 
(long)CINT(BgL_tmpz00_3002); } 
{ /* Llib/bit.scm 627 */
 obj_t BgL_tmpz00_2993;
if(
LLONGP(BgL_xz00_1389))
{ /* Llib/bit.scm 627 */
BgL_tmpz00_2993 = BgL_xz00_1389
; }  else 
{ 
 obj_t BgL_auxz00_2996;
BgL_auxz00_2996 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29384L), BGl_string1764z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1389); 
FAILURE(BgL_auxz00_2996,BFALSE,BFALSE);} 
BgL_auxz00_2992 = 
BLLONG_TO_LLONG(BgL_tmpz00_2993); } 
BgL_tmpz00_2991 = 
BGl_bitzd2urshllongzd2zz__bitz00(BgL_auxz00_2992, BgL_auxz00_3001); } 
return 
make_bllong(BgL_tmpz00_2991);} } 

}



/* bit-urshu8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2urshu8zd2zz__bitz00(uint8_t BgL_xz00_119, long BgL_yz00_120)
{
{ /* Llib/bit.scm 628 */
return 
(BgL_xz00_119 >> 
(int)(BgL_yz00_120));} 

}



/* &bit-urshu8 */
obj_t BGl_z62bitzd2urshu8zb0zz__bitz00(obj_t BgL_envz00_1391, obj_t BgL_xz00_1392, obj_t BgL_yz00_1393)
{
{ /* Llib/bit.scm 628 */
{ /* Llib/bit.scm 628 */
 uint8_t BgL_tmpz00_3014;
{ /* Llib/bit.scm 628 */
 long BgL_auxz00_3024; uint8_t BgL_auxz00_3015;
{ /* Llib/bit.scm 628 */
 obj_t BgL_tmpz00_3025;
if(
INTEGERP(BgL_yz00_1393))
{ /* Llib/bit.scm 628 */
BgL_tmpz00_3025 = BgL_yz00_1393
; }  else 
{ 
 obj_t BgL_auxz00_3028;
BgL_auxz00_3028 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29437L), BGl_string1765z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1393); 
FAILURE(BgL_auxz00_3028,BFALSE,BFALSE);} 
BgL_auxz00_3024 = 
(long)CINT(BgL_tmpz00_3025); } 
{ /* Llib/bit.scm 628 */
 obj_t BgL_tmpz00_3016;
if(
BGL_UINT8P(BgL_xz00_1392))
{ /* Llib/bit.scm 628 */
BgL_tmpz00_3016 = BgL_xz00_1392
; }  else 
{ 
 obj_t BgL_auxz00_3019;
BgL_auxz00_3019 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29437L), BGl_string1765z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1392); 
FAILURE(BgL_auxz00_3019,BFALSE,BFALSE);} 
BgL_auxz00_3015 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_3016); } 
BgL_tmpz00_3014 = 
BGl_bitzd2urshu8zd2zz__bitz00(BgL_auxz00_3015, BgL_auxz00_3024); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_3014);} } 

}



/* bit-urshs8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2urshs8zd2zz__bitz00(int8_t BgL_xz00_121, long BgL_yz00_122)
{
{ /* Llib/bit.scm 629 */
return 
(BgL_xz00_121 >> 
(int)(BgL_yz00_122));} 

}



/* &bit-urshs8 */
obj_t BGl_z62bitzd2urshs8zb0zz__bitz00(obj_t BgL_envz00_1394, obj_t BgL_xz00_1395, obj_t BgL_yz00_1396)
{
{ /* Llib/bit.scm 629 */
{ /* Llib/bit.scm 629 */
 int8_t BgL_tmpz00_3037;
{ /* Llib/bit.scm 629 */
 long BgL_auxz00_3047; int8_t BgL_auxz00_3038;
{ /* Llib/bit.scm 629 */
 obj_t BgL_tmpz00_3048;
if(
INTEGERP(BgL_yz00_1396))
{ /* Llib/bit.scm 629 */
BgL_tmpz00_3048 = BgL_yz00_1396
; }  else 
{ 
 obj_t BgL_auxz00_3051;
BgL_auxz00_3051 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29487L), BGl_string1766z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1396); 
FAILURE(BgL_auxz00_3051,BFALSE,BFALSE);} 
BgL_auxz00_3047 = 
(long)CINT(BgL_tmpz00_3048); } 
{ /* Llib/bit.scm 629 */
 obj_t BgL_tmpz00_3039;
if(
BGL_INT8P(BgL_xz00_1395))
{ /* Llib/bit.scm 629 */
BgL_tmpz00_3039 = BgL_xz00_1395
; }  else 
{ 
 obj_t BgL_auxz00_3042;
BgL_auxz00_3042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29487L), BGl_string1766z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1395); 
FAILURE(BgL_auxz00_3042,BFALSE,BFALSE);} 
BgL_auxz00_3038 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_3039); } 
BgL_tmpz00_3037 = 
BGl_bitzd2urshs8zd2zz__bitz00(BgL_auxz00_3038, BgL_auxz00_3047); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_3037);} } 

}



/* bit-urshs16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2urshs16zd2zz__bitz00(int16_t BgL_xz00_123, long BgL_yz00_124)
{
{ /* Llib/bit.scm 630 */
return 
(BgL_xz00_123 >> 
(int)(BgL_yz00_124));} 

}



/* &bit-urshs16 */
obj_t BGl_z62bitzd2urshs16zb0zz__bitz00(obj_t BgL_envz00_1397, obj_t BgL_xz00_1398, obj_t BgL_yz00_1399)
{
{ /* Llib/bit.scm 630 */
{ /* Llib/bit.scm 630 */
 int16_t BgL_tmpz00_3060;
{ /* Llib/bit.scm 630 */
 long BgL_auxz00_3070; int16_t BgL_auxz00_3061;
{ /* Llib/bit.scm 630 */
 obj_t BgL_tmpz00_3071;
if(
INTEGERP(BgL_yz00_1399))
{ /* Llib/bit.scm 630 */
BgL_tmpz00_3071 = BgL_yz00_1399
; }  else 
{ 
 obj_t BgL_auxz00_3074;
BgL_auxz00_3074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29538L), BGl_string1767z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1399); 
FAILURE(BgL_auxz00_3074,BFALSE,BFALSE);} 
BgL_auxz00_3070 = 
(long)CINT(BgL_tmpz00_3071); } 
{ /* Llib/bit.scm 630 */
 obj_t BgL_tmpz00_3062;
if(
BGL_INT16P(BgL_xz00_1398))
{ /* Llib/bit.scm 630 */
BgL_tmpz00_3062 = BgL_xz00_1398
; }  else 
{ 
 obj_t BgL_auxz00_3065;
BgL_auxz00_3065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29538L), BGl_string1767z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1398); 
FAILURE(BgL_auxz00_3065,BFALSE,BFALSE);} 
BgL_auxz00_3061 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_3062); } 
BgL_tmpz00_3060 = 
BGl_bitzd2urshs16zd2zz__bitz00(BgL_auxz00_3061, BgL_auxz00_3070); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_3060);} } 

}



/* bit-urshu16 */
BGL_EXPORTED_DEF uint16_t BGl_bitzd2urshu16zd2zz__bitz00(uint16_t BgL_xz00_125, long BgL_yz00_126)
{
{ /* Llib/bit.scm 631 */
return 
(BgL_xz00_125 >> 
(int)(BgL_yz00_126));} 

}



/* &bit-urshu16 */
obj_t BGl_z62bitzd2urshu16zb0zz__bitz00(obj_t BgL_envz00_1400, obj_t BgL_xz00_1401, obj_t BgL_yz00_1402)
{
{ /* Llib/bit.scm 631 */
{ /* Llib/bit.scm 631 */
 uint16_t BgL_tmpz00_3083;
{ /* Llib/bit.scm 631 */
 long BgL_auxz00_3093; uint16_t BgL_auxz00_3084;
{ /* Llib/bit.scm 631 */
 obj_t BgL_tmpz00_3094;
if(
INTEGERP(BgL_yz00_1402))
{ /* Llib/bit.scm 631 */
BgL_tmpz00_3094 = BgL_yz00_1402
; }  else 
{ 
 obj_t BgL_auxz00_3097;
BgL_auxz00_3097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29590L), BGl_string1768z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1402); 
FAILURE(BgL_auxz00_3097,BFALSE,BFALSE);} 
BgL_auxz00_3093 = 
(long)CINT(BgL_tmpz00_3094); } 
{ /* Llib/bit.scm 631 */
 obj_t BgL_tmpz00_3085;
if(
BGL_UINT16P(BgL_xz00_1401))
{ /* Llib/bit.scm 631 */
BgL_tmpz00_3085 = BgL_xz00_1401
; }  else 
{ 
 obj_t BgL_auxz00_3088;
BgL_auxz00_3088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29590L), BGl_string1768z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_xz00_1401); 
FAILURE(BgL_auxz00_3088,BFALSE,BFALSE);} 
BgL_auxz00_3084 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_3085); } 
BgL_tmpz00_3083 = 
BGl_bitzd2urshu16zd2zz__bitz00(BgL_auxz00_3084, BgL_auxz00_3093); } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_3083);} } 

}



/* bit-urshs32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2urshs32zd2zz__bitz00(int32_t BgL_xz00_127, long BgL_yz00_128)
{
{ /* Llib/bit.scm 632 */
return 
(BgL_xz00_127 >> 
(int)(BgL_yz00_128));} 

}



/* &bit-urshs32 */
obj_t BGl_z62bitzd2urshs32zb0zz__bitz00(obj_t BgL_envz00_1403, obj_t BgL_xz00_1404, obj_t BgL_yz00_1405)
{
{ /* Llib/bit.scm 632 */
{ /* Llib/bit.scm 632 */
 int32_t BgL_tmpz00_3106;
{ /* Llib/bit.scm 632 */
 long BgL_auxz00_3116; int32_t BgL_auxz00_3107;
{ /* Llib/bit.scm 632 */
 obj_t BgL_tmpz00_3117;
if(
INTEGERP(BgL_yz00_1405))
{ /* Llib/bit.scm 632 */
BgL_tmpz00_3117 = BgL_yz00_1405
; }  else 
{ 
 obj_t BgL_auxz00_3120;
BgL_auxz00_3120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29642L), BGl_string1769z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1405); 
FAILURE(BgL_auxz00_3120,BFALSE,BFALSE);} 
BgL_auxz00_3116 = 
(long)CINT(BgL_tmpz00_3117); } 
{ /* Llib/bit.scm 632 */
 obj_t BgL_tmpz00_3108;
if(
BGL_INT32P(BgL_xz00_1404))
{ /* Llib/bit.scm 632 */
BgL_tmpz00_3108 = BgL_xz00_1404
; }  else 
{ 
 obj_t BgL_auxz00_3111;
BgL_auxz00_3111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29642L), BGl_string1769z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1404); 
FAILURE(BgL_auxz00_3111,BFALSE,BFALSE);} 
BgL_auxz00_3107 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_3108); } 
BgL_tmpz00_3106 = 
BGl_bitzd2urshs32zd2zz__bitz00(BgL_auxz00_3107, BgL_auxz00_3116); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_3106);} } 

}



/* bit-urshu32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2urshu32zd2zz__bitz00(uint32_t BgL_xz00_129, long BgL_yz00_130)
{
{ /* Llib/bit.scm 633 */
return 
(BgL_xz00_129 >> 
(int)(BgL_yz00_130));} 

}



/* &bit-urshu32 */
obj_t BGl_z62bitzd2urshu32zb0zz__bitz00(obj_t BgL_envz00_1406, obj_t BgL_xz00_1407, obj_t BgL_yz00_1408)
{
{ /* Llib/bit.scm 633 */
{ /* Llib/bit.scm 633 */
 uint32_t BgL_tmpz00_3129;
{ /* Llib/bit.scm 633 */
 long BgL_auxz00_3139; uint32_t BgL_auxz00_3130;
{ /* Llib/bit.scm 633 */
 obj_t BgL_tmpz00_3140;
if(
INTEGERP(BgL_yz00_1408))
{ /* Llib/bit.scm 633 */
BgL_tmpz00_3140 = BgL_yz00_1408
; }  else 
{ 
 obj_t BgL_auxz00_3143;
BgL_auxz00_3143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29694L), BGl_string1770z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1408); 
FAILURE(BgL_auxz00_3143,BFALSE,BFALSE);} 
BgL_auxz00_3139 = 
(long)CINT(BgL_tmpz00_3140); } 
{ /* Llib/bit.scm 633 */
 obj_t BgL_tmpz00_3131;
if(
BGL_UINT32P(BgL_xz00_1407))
{ /* Llib/bit.scm 633 */
BgL_tmpz00_3131 = BgL_xz00_1407
; }  else 
{ 
 obj_t BgL_auxz00_3134;
BgL_auxz00_3134 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29694L), BGl_string1770z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1407); 
FAILURE(BgL_auxz00_3134,BFALSE,BFALSE);} 
BgL_auxz00_3130 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_3131); } 
BgL_tmpz00_3129 = 
BGl_bitzd2urshu32zd2zz__bitz00(BgL_auxz00_3130, BgL_auxz00_3139); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_3129);} } 

}



/* bit-urshs64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2urshs64zd2zz__bitz00(int64_t BgL_xz00_131, long BgL_yz00_132)
{
{ /* Llib/bit.scm 634 */
return 
(BgL_xz00_131 >> 
(int)(BgL_yz00_132));} 

}



/* &bit-urshs64 */
obj_t BGl_z62bitzd2urshs64zb0zz__bitz00(obj_t BgL_envz00_1409, obj_t BgL_xz00_1410, obj_t BgL_yz00_1411)
{
{ /* Llib/bit.scm 634 */
{ /* Llib/bit.scm 634 */
 int64_t BgL_tmpz00_3152;
{ /* Llib/bit.scm 634 */
 long BgL_auxz00_3162; int64_t BgL_auxz00_3153;
{ /* Llib/bit.scm 634 */
 obj_t BgL_tmpz00_3163;
if(
INTEGERP(BgL_yz00_1411))
{ /* Llib/bit.scm 634 */
BgL_tmpz00_3163 = BgL_yz00_1411
; }  else 
{ 
 obj_t BgL_auxz00_3166;
BgL_auxz00_3166 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29746L), BGl_string1771z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1411); 
FAILURE(BgL_auxz00_3166,BFALSE,BFALSE);} 
BgL_auxz00_3162 = 
(long)CINT(BgL_tmpz00_3163); } 
{ /* Llib/bit.scm 634 */
 obj_t BgL_tmpz00_3154;
if(
BGL_INT64P(BgL_xz00_1410))
{ /* Llib/bit.scm 634 */
BgL_tmpz00_3154 = BgL_xz00_1410
; }  else 
{ 
 obj_t BgL_auxz00_3157;
BgL_auxz00_3157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29746L), BGl_string1771z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1410); 
FAILURE(BgL_auxz00_3157,BFALSE,BFALSE);} 
BgL_auxz00_3153 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_3154); } 
BgL_tmpz00_3152 = 
BGl_bitzd2urshs64zd2zz__bitz00(BgL_auxz00_3153, BgL_auxz00_3162); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_3152);} } 

}



/* bit-urshu64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2urshu64zd2zz__bitz00(uint64_t BgL_xz00_133, long BgL_yz00_134)
{
{ /* Llib/bit.scm 635 */
return 
(BgL_xz00_133 >> 
(int)(BgL_yz00_134));} 

}



/* &bit-urshu64 */
obj_t BGl_z62bitzd2urshu64zb0zz__bitz00(obj_t BgL_envz00_1412, obj_t BgL_xz00_1413, obj_t BgL_yz00_1414)
{
{ /* Llib/bit.scm 635 */
{ /* Llib/bit.scm 635 */
 uint64_t BgL_tmpz00_3175;
{ /* Llib/bit.scm 635 */
 long BgL_auxz00_3185; uint64_t BgL_auxz00_3176;
{ /* Llib/bit.scm 635 */
 obj_t BgL_tmpz00_3186;
if(
INTEGERP(BgL_yz00_1414))
{ /* Llib/bit.scm 635 */
BgL_tmpz00_3186 = BgL_yz00_1414
; }  else 
{ 
 obj_t BgL_auxz00_3189;
BgL_auxz00_3189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29798L), BGl_string1772z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1414); 
FAILURE(BgL_auxz00_3189,BFALSE,BFALSE);} 
BgL_auxz00_3185 = 
(long)CINT(BgL_tmpz00_3186); } 
{ /* Llib/bit.scm 635 */
 obj_t BgL_tmpz00_3177;
if(
BGL_UINT64P(BgL_xz00_1413))
{ /* Llib/bit.scm 635 */
BgL_tmpz00_3177 = BgL_xz00_1413
; }  else 
{ 
 obj_t BgL_auxz00_3180;
BgL_auxz00_3180 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(29798L), BGl_string1772z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1413); 
FAILURE(BgL_auxz00_3180,BFALSE,BFALSE);} 
BgL_auxz00_3176 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_3177); } 
BgL_tmpz00_3175 = 
BGl_bitzd2urshu64zd2zz__bitz00(BgL_auxz00_3176, BgL_auxz00_3185); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_3175);} } 

}



/* bit-lsh */
BGL_EXPORTED_DEF long BGl_bitzd2lshzd2zz__bitz00(long BgL_xz00_135, long BgL_yz00_136)
{
{ /* Llib/bit.scm 640 */
return 
(BgL_xz00_135 << 
(int)(BgL_yz00_136));} 

}



/* &bit-lsh */
obj_t BGl_z62bitzd2lshzb0zz__bitz00(obj_t BgL_envz00_1415, obj_t BgL_xz00_1416, obj_t BgL_yz00_1417)
{
{ /* Llib/bit.scm 640 */
{ /* Llib/bit.scm 640 */
 long BgL_tmpz00_3198;
{ /* Llib/bit.scm 640 */
 long BgL_auxz00_3208; long BgL_auxz00_3199;
{ /* Llib/bit.scm 640 */
 obj_t BgL_tmpz00_3209;
if(
INTEGERP(BgL_yz00_1417))
{ /* Llib/bit.scm 640 */
BgL_tmpz00_3209 = BgL_yz00_1417
; }  else 
{ 
 obj_t BgL_auxz00_3212;
BgL_auxz00_3212 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30076L), BGl_string1773z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1417); 
FAILURE(BgL_auxz00_3212,BFALSE,BFALSE);} 
BgL_auxz00_3208 = 
(long)CINT(BgL_tmpz00_3209); } 
{ /* Llib/bit.scm 640 */
 obj_t BgL_tmpz00_3200;
if(
INTEGERP(BgL_xz00_1416))
{ /* Llib/bit.scm 640 */
BgL_tmpz00_3200 = BgL_xz00_1416
; }  else 
{ 
 obj_t BgL_auxz00_3203;
BgL_auxz00_3203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30076L), BGl_string1773z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_xz00_1416); 
FAILURE(BgL_auxz00_3203,BFALSE,BFALSE);} 
BgL_auxz00_3199 = 
(long)CINT(BgL_tmpz00_3200); } 
BgL_tmpz00_3198 = 
BGl_bitzd2lshzd2zz__bitz00(BgL_auxz00_3199, BgL_auxz00_3208); } 
return 
BINT(BgL_tmpz00_3198);} } 

}



/* bit-lshelong */
BGL_EXPORTED_DEF long BGl_bitzd2lshelongzd2zz__bitz00(long BgL_xz00_137, long BgL_yz00_138)
{
{ /* Llib/bit.scm 641 */
return 
(BgL_xz00_137 << 
(int)(BgL_yz00_138));} 

}



/* &bit-lshelong */
obj_t BGl_z62bitzd2lshelongzb0zz__bitz00(obj_t BgL_envz00_1418, obj_t BgL_xz00_1419, obj_t BgL_yz00_1420)
{
{ /* Llib/bit.scm 641 */
{ /* Llib/bit.scm 641 */
 long BgL_tmpz00_3221;
{ /* Llib/bit.scm 641 */
 long BgL_auxz00_3231; long BgL_auxz00_3222;
{ /* Llib/bit.scm 641 */
 obj_t BgL_tmpz00_3232;
if(
INTEGERP(BgL_yz00_1420))
{ /* Llib/bit.scm 641 */
BgL_tmpz00_3232 = BgL_yz00_1420
; }  else 
{ 
 obj_t BgL_auxz00_3235;
BgL_auxz00_3235 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30125L), BGl_string1774z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1420); 
FAILURE(BgL_auxz00_3235,BFALSE,BFALSE);} 
BgL_auxz00_3231 = 
(long)CINT(BgL_tmpz00_3232); } 
{ /* Llib/bit.scm 641 */
 obj_t BgL_tmpz00_3223;
if(
ELONGP(BgL_xz00_1419))
{ /* Llib/bit.scm 641 */
BgL_tmpz00_3223 = BgL_xz00_1419
; }  else 
{ 
 obj_t BgL_auxz00_3226;
BgL_auxz00_3226 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30125L), BGl_string1774z00zz__bitz00, BGl_string1692z00zz__bitz00, BgL_xz00_1419); 
FAILURE(BgL_auxz00_3226,BFALSE,BFALSE);} 
BgL_auxz00_3222 = 
BELONG_TO_LONG(BgL_tmpz00_3223); } 
BgL_tmpz00_3221 = 
BGl_bitzd2lshelongzd2zz__bitz00(BgL_auxz00_3222, BgL_auxz00_3231); } 
return 
make_belong(BgL_tmpz00_3221);} } 

}



/* bit-lshllong */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_bitzd2lshllongzd2zz__bitz00(BGL_LONGLONG_T BgL_xz00_139, long BgL_yz00_140)
{
{ /* Llib/bit.scm 642 */
return 
(BgL_xz00_139 << 
(int)(BgL_yz00_140));} 

}



/* &bit-lshllong */
obj_t BGl_z62bitzd2lshllongzb0zz__bitz00(obj_t BgL_envz00_1421, obj_t BgL_xz00_1422, obj_t BgL_yz00_1423)
{
{ /* Llib/bit.scm 642 */
{ /* Llib/bit.scm 642 */
 BGL_LONGLONG_T BgL_tmpz00_3244;
{ /* Llib/bit.scm 642 */
 long BgL_auxz00_3254; BGL_LONGLONG_T BgL_auxz00_3245;
{ /* Llib/bit.scm 642 */
 obj_t BgL_tmpz00_3255;
if(
INTEGERP(BgL_yz00_1423))
{ /* Llib/bit.scm 642 */
BgL_tmpz00_3255 = BgL_yz00_1423
; }  else 
{ 
 obj_t BgL_auxz00_3258;
BgL_auxz00_3258 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30179L), BGl_string1775z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1423); 
FAILURE(BgL_auxz00_3258,BFALSE,BFALSE);} 
BgL_auxz00_3254 = 
(long)CINT(BgL_tmpz00_3255); } 
{ /* Llib/bit.scm 642 */
 obj_t BgL_tmpz00_3246;
if(
LLONGP(BgL_xz00_1422))
{ /* Llib/bit.scm 642 */
BgL_tmpz00_3246 = BgL_xz00_1422
; }  else 
{ 
 obj_t BgL_auxz00_3249;
BgL_auxz00_3249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30179L), BGl_string1775z00zz__bitz00, BGl_string1694z00zz__bitz00, BgL_xz00_1422); 
FAILURE(BgL_auxz00_3249,BFALSE,BFALSE);} 
BgL_auxz00_3245 = 
BLLONG_TO_LLONG(BgL_tmpz00_3246); } 
BgL_tmpz00_3244 = 
BGl_bitzd2lshllongzd2zz__bitz00(BgL_auxz00_3245, BgL_auxz00_3254); } 
return 
make_bllong(BgL_tmpz00_3244);} } 

}



/* bit-lshs8 */
BGL_EXPORTED_DEF int8_t BGl_bitzd2lshs8zd2zz__bitz00(int8_t BgL_xz00_141, long BgL_yz00_142)
{
{ /* Llib/bit.scm 643 */
return 
(BgL_xz00_141 << 
(int)(BgL_yz00_142));} 

}



/* &bit-lshs8 */
obj_t BGl_z62bitzd2lshs8zb0zz__bitz00(obj_t BgL_envz00_1424, obj_t BgL_xz00_1425, obj_t BgL_yz00_1426)
{
{ /* Llib/bit.scm 643 */
{ /* Llib/bit.scm 643 */
 int8_t BgL_tmpz00_3267;
{ /* Llib/bit.scm 643 */
 long BgL_auxz00_3277; int8_t BgL_auxz00_3268;
{ /* Llib/bit.scm 643 */
 obj_t BgL_tmpz00_3278;
if(
INTEGERP(BgL_yz00_1426))
{ /* Llib/bit.scm 643 */
BgL_tmpz00_3278 = BgL_yz00_1426
; }  else 
{ 
 obj_t BgL_auxz00_3281;
BgL_auxz00_3281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30230L), BGl_string1776z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1426); 
FAILURE(BgL_auxz00_3281,BFALSE,BFALSE);} 
BgL_auxz00_3277 = 
(long)CINT(BgL_tmpz00_3278); } 
{ /* Llib/bit.scm 643 */
 obj_t BgL_tmpz00_3269;
if(
BGL_INT8P(BgL_xz00_1425))
{ /* Llib/bit.scm 643 */
BgL_tmpz00_3269 = BgL_xz00_1425
; }  else 
{ 
 obj_t BgL_auxz00_3272;
BgL_auxz00_3272 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30230L), BGl_string1776z00zz__bitz00, BGl_string1696z00zz__bitz00, BgL_xz00_1425); 
FAILURE(BgL_auxz00_3272,BFALSE,BFALSE);} 
BgL_auxz00_3268 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_3269); } 
BgL_tmpz00_3267 = 
BGl_bitzd2lshs8zd2zz__bitz00(BgL_auxz00_3268, BgL_auxz00_3277); } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_3267);} } 

}



/* bit-lshu8 */
BGL_EXPORTED_DEF uint8_t BGl_bitzd2lshu8zd2zz__bitz00(uint8_t BgL_xz00_143, long BgL_yz00_144)
{
{ /* Llib/bit.scm 644 */
return 
(BgL_xz00_143 << 
(int)(BgL_yz00_144));} 

}



/* &bit-lshu8 */
obj_t BGl_z62bitzd2lshu8zb0zz__bitz00(obj_t BgL_envz00_1427, obj_t BgL_xz00_1428, obj_t BgL_yz00_1429)
{
{ /* Llib/bit.scm 644 */
{ /* Llib/bit.scm 644 */
 uint8_t BgL_tmpz00_3290;
{ /* Llib/bit.scm 644 */
 long BgL_auxz00_3300; uint8_t BgL_auxz00_3291;
{ /* Llib/bit.scm 644 */
 obj_t BgL_tmpz00_3301;
if(
INTEGERP(BgL_yz00_1429))
{ /* Llib/bit.scm 644 */
BgL_tmpz00_3301 = BgL_yz00_1429
; }  else 
{ 
 obj_t BgL_auxz00_3304;
BgL_auxz00_3304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30278L), BGl_string1777z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1429); 
FAILURE(BgL_auxz00_3304,BFALSE,BFALSE);} 
BgL_auxz00_3300 = 
(long)CINT(BgL_tmpz00_3301); } 
{ /* Llib/bit.scm 644 */
 obj_t BgL_tmpz00_3292;
if(
BGL_UINT8P(BgL_xz00_1428))
{ /* Llib/bit.scm 644 */
BgL_tmpz00_3292 = BgL_xz00_1428
; }  else 
{ 
 obj_t BgL_auxz00_3295;
BgL_auxz00_3295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30278L), BGl_string1777z00zz__bitz00, BGl_string1698z00zz__bitz00, BgL_xz00_1428); 
FAILURE(BgL_auxz00_3295,BFALSE,BFALSE);} 
BgL_auxz00_3291 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_3292); } 
BgL_tmpz00_3290 = 
BGl_bitzd2lshu8zd2zz__bitz00(BgL_auxz00_3291, BgL_auxz00_3300); } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_3290);} } 

}



/* bit-lshs16 */
BGL_EXPORTED_DEF int16_t BGl_bitzd2lshs16zd2zz__bitz00(int16_t BgL_xz00_145, long BgL_yz00_146)
{
{ /* Llib/bit.scm 645 */
return 
(BgL_xz00_145 << 
(int)(BgL_yz00_146));} 

}



/* &bit-lshs16 */
obj_t BGl_z62bitzd2lshs16zb0zz__bitz00(obj_t BgL_envz00_1430, obj_t BgL_xz00_1431, obj_t BgL_yz00_1432)
{
{ /* Llib/bit.scm 645 */
{ /* Llib/bit.scm 645 */
 int16_t BgL_tmpz00_3313;
{ /* Llib/bit.scm 645 */
 long BgL_auxz00_3323; int16_t BgL_auxz00_3314;
{ /* Llib/bit.scm 645 */
 obj_t BgL_tmpz00_3324;
if(
INTEGERP(BgL_yz00_1432))
{ /* Llib/bit.scm 645 */
BgL_tmpz00_3324 = BgL_yz00_1432
; }  else 
{ 
 obj_t BgL_auxz00_3327;
BgL_auxz00_3327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30327L), BGl_string1778z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1432); 
FAILURE(BgL_auxz00_3327,BFALSE,BFALSE);} 
BgL_auxz00_3323 = 
(long)CINT(BgL_tmpz00_3324); } 
{ /* Llib/bit.scm 645 */
 obj_t BgL_tmpz00_3315;
if(
BGL_INT16P(BgL_xz00_1431))
{ /* Llib/bit.scm 645 */
BgL_tmpz00_3315 = BgL_xz00_1431
; }  else 
{ 
 obj_t BgL_auxz00_3318;
BgL_auxz00_3318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30327L), BGl_string1778z00zz__bitz00, BGl_string1700z00zz__bitz00, BgL_xz00_1431); 
FAILURE(BgL_auxz00_3318,BFALSE,BFALSE);} 
BgL_auxz00_3314 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_3315); } 
BgL_tmpz00_3313 = 
BGl_bitzd2lshs16zd2zz__bitz00(BgL_auxz00_3314, BgL_auxz00_3323); } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_3313);} } 

}



/* bit-lshu16 */
BGL_EXPORTED_DEF uint16_t BGl_bitzd2lshu16zd2zz__bitz00(uint16_t BgL_xz00_147, long BgL_yz00_148)
{
{ /* Llib/bit.scm 646 */
return 
(BgL_xz00_147 << 
(int)(BgL_yz00_148));} 

}



/* &bit-lshu16 */
obj_t BGl_z62bitzd2lshu16zb0zz__bitz00(obj_t BgL_envz00_1433, obj_t BgL_xz00_1434, obj_t BgL_yz00_1435)
{
{ /* Llib/bit.scm 646 */
{ /* Llib/bit.scm 646 */
 uint16_t BgL_tmpz00_3336;
{ /* Llib/bit.scm 646 */
 long BgL_auxz00_3346; uint16_t BgL_auxz00_3337;
{ /* Llib/bit.scm 646 */
 obj_t BgL_tmpz00_3347;
if(
INTEGERP(BgL_yz00_1435))
{ /* Llib/bit.scm 646 */
BgL_tmpz00_3347 = BgL_yz00_1435
; }  else 
{ 
 obj_t BgL_auxz00_3350;
BgL_auxz00_3350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30377L), BGl_string1779z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1435); 
FAILURE(BgL_auxz00_3350,BFALSE,BFALSE);} 
BgL_auxz00_3346 = 
(long)CINT(BgL_tmpz00_3347); } 
{ /* Llib/bit.scm 646 */
 obj_t BgL_tmpz00_3338;
if(
BGL_UINT16P(BgL_xz00_1434))
{ /* Llib/bit.scm 646 */
BgL_tmpz00_3338 = BgL_xz00_1434
; }  else 
{ 
 obj_t BgL_auxz00_3341;
BgL_auxz00_3341 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30377L), BGl_string1779z00zz__bitz00, BGl_string1702z00zz__bitz00, BgL_xz00_1434); 
FAILURE(BgL_auxz00_3341,BFALSE,BFALSE);} 
BgL_auxz00_3337 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_3338); } 
BgL_tmpz00_3336 = 
BGl_bitzd2lshu16zd2zz__bitz00(BgL_auxz00_3337, BgL_auxz00_3346); } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_3336);} } 

}



/* bit-lshs32 */
BGL_EXPORTED_DEF int32_t BGl_bitzd2lshs32zd2zz__bitz00(int32_t BgL_xz00_149, long BgL_yz00_150)
{
{ /* Llib/bit.scm 647 */
return 
(BgL_xz00_149 << 
(int)(BgL_yz00_150));} 

}



/* &bit-lshs32 */
obj_t BGl_z62bitzd2lshs32zb0zz__bitz00(obj_t BgL_envz00_1436, obj_t BgL_xz00_1437, obj_t BgL_yz00_1438)
{
{ /* Llib/bit.scm 647 */
{ /* Llib/bit.scm 647 */
 int32_t BgL_tmpz00_3359;
{ /* Llib/bit.scm 647 */
 long BgL_auxz00_3369; int32_t BgL_auxz00_3360;
{ /* Llib/bit.scm 647 */
 obj_t BgL_tmpz00_3370;
if(
INTEGERP(BgL_yz00_1438))
{ /* Llib/bit.scm 647 */
BgL_tmpz00_3370 = BgL_yz00_1438
; }  else 
{ 
 obj_t BgL_auxz00_3373;
BgL_auxz00_3373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30427L), BGl_string1780z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1438); 
FAILURE(BgL_auxz00_3373,BFALSE,BFALSE);} 
BgL_auxz00_3369 = 
(long)CINT(BgL_tmpz00_3370); } 
{ /* Llib/bit.scm 647 */
 obj_t BgL_tmpz00_3361;
if(
BGL_INT32P(BgL_xz00_1437))
{ /* Llib/bit.scm 647 */
BgL_tmpz00_3361 = BgL_xz00_1437
; }  else 
{ 
 obj_t BgL_auxz00_3364;
BgL_auxz00_3364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30427L), BGl_string1780z00zz__bitz00, BGl_string1704z00zz__bitz00, BgL_xz00_1437); 
FAILURE(BgL_auxz00_3364,BFALSE,BFALSE);} 
BgL_auxz00_3360 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_3361); } 
BgL_tmpz00_3359 = 
BGl_bitzd2lshs32zd2zz__bitz00(BgL_auxz00_3360, BgL_auxz00_3369); } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_3359);} } 

}



/* bit-lshu32 */
BGL_EXPORTED_DEF uint32_t BGl_bitzd2lshu32zd2zz__bitz00(uint32_t BgL_xz00_151, long BgL_yz00_152)
{
{ /* Llib/bit.scm 648 */
return 
(BgL_xz00_151 << 
(int)(BgL_yz00_152));} 

}



/* &bit-lshu32 */
obj_t BGl_z62bitzd2lshu32zb0zz__bitz00(obj_t BgL_envz00_1439, obj_t BgL_xz00_1440, obj_t BgL_yz00_1441)
{
{ /* Llib/bit.scm 648 */
{ /* Llib/bit.scm 648 */
 uint32_t BgL_tmpz00_3382;
{ /* Llib/bit.scm 648 */
 long BgL_auxz00_3392; uint32_t BgL_auxz00_3383;
{ /* Llib/bit.scm 648 */
 obj_t BgL_tmpz00_3393;
if(
INTEGERP(BgL_yz00_1441))
{ /* Llib/bit.scm 648 */
BgL_tmpz00_3393 = BgL_yz00_1441
; }  else 
{ 
 obj_t BgL_auxz00_3396;
BgL_auxz00_3396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30477L), BGl_string1781z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1441); 
FAILURE(BgL_auxz00_3396,BFALSE,BFALSE);} 
BgL_auxz00_3392 = 
(long)CINT(BgL_tmpz00_3393); } 
{ /* Llib/bit.scm 648 */
 obj_t BgL_tmpz00_3384;
if(
BGL_UINT32P(BgL_xz00_1440))
{ /* Llib/bit.scm 648 */
BgL_tmpz00_3384 = BgL_xz00_1440
; }  else 
{ 
 obj_t BgL_auxz00_3387;
BgL_auxz00_3387 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30477L), BGl_string1781z00zz__bitz00, BGl_string1706z00zz__bitz00, BgL_xz00_1440); 
FAILURE(BgL_auxz00_3387,BFALSE,BFALSE);} 
BgL_auxz00_3383 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_3384); } 
BgL_tmpz00_3382 = 
BGl_bitzd2lshu32zd2zz__bitz00(BgL_auxz00_3383, BgL_auxz00_3392); } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_3382);} } 

}



/* bit-lshs64 */
BGL_EXPORTED_DEF int64_t BGl_bitzd2lshs64zd2zz__bitz00(int64_t BgL_xz00_153, long BgL_yz00_154)
{
{ /* Llib/bit.scm 649 */
return 
(BgL_xz00_153 << 
(int)(BgL_yz00_154));} 

}



/* &bit-lshs64 */
obj_t BGl_z62bitzd2lshs64zb0zz__bitz00(obj_t BgL_envz00_1442, obj_t BgL_xz00_1443, obj_t BgL_yz00_1444)
{
{ /* Llib/bit.scm 649 */
{ /* Llib/bit.scm 649 */
 int64_t BgL_tmpz00_3405;
{ /* Llib/bit.scm 649 */
 long BgL_auxz00_3415; int64_t BgL_auxz00_3406;
{ /* Llib/bit.scm 649 */
 obj_t BgL_tmpz00_3416;
if(
INTEGERP(BgL_yz00_1444))
{ /* Llib/bit.scm 649 */
BgL_tmpz00_3416 = BgL_yz00_1444
; }  else 
{ 
 obj_t BgL_auxz00_3419;
BgL_auxz00_3419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30527L), BGl_string1782z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1444); 
FAILURE(BgL_auxz00_3419,BFALSE,BFALSE);} 
BgL_auxz00_3415 = 
(long)CINT(BgL_tmpz00_3416); } 
{ /* Llib/bit.scm 649 */
 obj_t BgL_tmpz00_3407;
if(
BGL_INT64P(BgL_xz00_1443))
{ /* Llib/bit.scm 649 */
BgL_tmpz00_3407 = BgL_xz00_1443
; }  else 
{ 
 obj_t BgL_auxz00_3410;
BgL_auxz00_3410 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30527L), BGl_string1782z00zz__bitz00, BGl_string1708z00zz__bitz00, BgL_xz00_1443); 
FAILURE(BgL_auxz00_3410,BFALSE,BFALSE);} 
BgL_auxz00_3406 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_3407); } 
BgL_tmpz00_3405 = 
BGl_bitzd2lshs64zd2zz__bitz00(BgL_auxz00_3406, BgL_auxz00_3415); } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_3405);} } 

}



/* bit-lshu64 */
BGL_EXPORTED_DEF uint64_t BGl_bitzd2lshu64zd2zz__bitz00(uint64_t BgL_xz00_155, long BgL_yz00_156)
{
{ /* Llib/bit.scm 650 */
return 
(BgL_xz00_155 << 
(int)(BgL_yz00_156));} 

}



/* &bit-lshu64 */
obj_t BGl_z62bitzd2lshu64zb0zz__bitz00(obj_t BgL_envz00_1445, obj_t BgL_xz00_1446, obj_t BgL_yz00_1447)
{
{ /* Llib/bit.scm 650 */
{ /* Llib/bit.scm 650 */
 uint64_t BgL_tmpz00_3428;
{ /* Llib/bit.scm 650 */
 long BgL_auxz00_3438; uint64_t BgL_auxz00_3429;
{ /* Llib/bit.scm 650 */
 obj_t BgL_tmpz00_3439;
if(
INTEGERP(BgL_yz00_1447))
{ /* Llib/bit.scm 650 */
BgL_tmpz00_3439 = BgL_yz00_1447
; }  else 
{ 
 obj_t BgL_auxz00_3442;
BgL_auxz00_3442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30577L), BGl_string1783z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1447); 
FAILURE(BgL_auxz00_3442,BFALSE,BFALSE);} 
BgL_auxz00_3438 = 
(long)CINT(BgL_tmpz00_3439); } 
{ /* Llib/bit.scm 650 */
 obj_t BgL_tmpz00_3430;
if(
BGL_UINT64P(BgL_xz00_1446))
{ /* Llib/bit.scm 650 */
BgL_tmpz00_3430 = BgL_xz00_1446
; }  else 
{ 
 obj_t BgL_auxz00_3433;
BgL_auxz00_3433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30577L), BGl_string1783z00zz__bitz00, BGl_string1710z00zz__bitz00, BgL_xz00_1446); 
FAILURE(BgL_auxz00_3433,BFALSE,BFALSE);} 
BgL_auxz00_3429 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_3430); } 
BgL_tmpz00_3428 = 
BGl_bitzd2lshu64zd2zz__bitz00(BgL_auxz00_3429, BgL_auxz00_3438); } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_3428);} } 

}



/* bit-lshbx */
BGL_EXPORTED_DEF obj_t BGl_bitzd2lshbxzd2zz__bitz00(obj_t BgL_xz00_157, long BgL_yz00_158)
{
{ /* Llib/bit.scm 651 */
BGL_TAIL return 
bgl_bignum_lsh(BgL_xz00_157, BgL_yz00_158);} 

}



/* &bit-lshbx */
obj_t BGl_z62bitzd2lshbxzb0zz__bitz00(obj_t BgL_envz00_1448, obj_t BgL_xz00_1449, obj_t BgL_yz00_1450)
{
{ /* Llib/bit.scm 651 */
{ /* Llib/bit.scm 651 */
 long BgL_auxz00_3457; obj_t BgL_auxz00_3450;
{ /* Llib/bit.scm 651 */
 obj_t BgL_tmpz00_3458;
if(
INTEGERP(BgL_yz00_1450))
{ /* Llib/bit.scm 651 */
BgL_tmpz00_3458 = BgL_yz00_1450
; }  else 
{ 
 obj_t BgL_auxz00_3461;
BgL_auxz00_3461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30626L), BGl_string1784z00zz__bitz00, BGl_string1690z00zz__bitz00, BgL_yz00_1450); 
FAILURE(BgL_auxz00_3461,BFALSE,BFALSE);} 
BgL_auxz00_3457 = 
(long)CINT(BgL_tmpz00_3458); } 
if(
BIGNUMP(BgL_xz00_1449))
{ /* Llib/bit.scm 651 */
BgL_auxz00_3450 = BgL_xz00_1449
; }  else 
{ 
 obj_t BgL_auxz00_3453;
BgL_auxz00_3453 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1688z00zz__bitz00, 
BINT(30626L), BGl_string1784z00zz__bitz00, BGl_string1712z00zz__bitz00, BgL_xz00_1449); 
FAILURE(BgL_auxz00_3453,BFALSE,BFALSE);} 
return 
BGl_bitzd2lshbxzd2zz__bitz00(BgL_auxz00_3450, BgL_auxz00_3457);} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__bitz00(void)
{
{ /* Llib/bit.scm 14 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1785z00zz__bitz00));} 

}

#ifdef __cplusplus
}
#endif
