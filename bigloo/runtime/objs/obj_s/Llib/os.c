/*===========================================================================*/
/*   (Llib/os.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/os.scm -indent -o objs/obj_s/Llib/os.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___OS_TYPE_DEFINITIONS
#define BGL___OS_TYPE_DEFINITIONS
#endif // BGL___OS_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62makezd2sharedzd2libzd2namezb0zz__osz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_sharedzd2libraryzd2suffixz00zz__osz00(void);
extern bool_t bgl_chmod(char *, bool_t, bool_t, bool_t);
static obj_t BGl_symbol2800z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_setgidz00zz__osz00(int);
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
BGL_EXPORTED_DEF obj_t BGl_za2defaultzd2javazd2packageza2z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2802z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2721z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62basenamez62zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_getuidz00zz__osz00(void);
extern bool_t bgl_setrlimit(long, long, long);
static obj_t BGl_z62getpidz62zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_getpwnamz00zz__osz00(obj_t);
static obj_t BGl_symbol2804z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2724z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2806z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_systemzd2ze3stringz31zz__osz00(obj_t);
static obj_t BGl_symbol2808z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62oszd2classzb0zz__osz00(obj_t);
extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62pwdz62zz__osz00(obj_t);
static obj_t BGl_z62prefixz62zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_sleepz00zz__osz00(long);
static obj_t BGl_z62defaultzd2scriptzd2namez62zz__osz00(obj_t);
static obj_t BGl_limitzd2resourcezd2noz00zz__osz00(obj_t, obj_t);
static obj_t BGl_z62ioctlz62zz__osz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez17zz__osz00(obj_t, obj_t);
static obj_t BGl_symbol2812z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_makezd2staticzd2libraryzd2namezd2zz__osz00(obj_t);
static obj_t BGl_symbol2814z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2816z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2818z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62makezd2staticzd2libzd2namezb0zz__osz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62makezd2filezd2pathz62zz__osz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_defaultzd2basenamezd2zz__osz00(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_openlogz00zz__osz00(obj_t, int, int);
BGL_EXPORTED_DECL obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t, obj_t);
static obj_t BGl_z62makezd2sharedzd2libraryzd2namezb0zz__osz00(obj_t, obj_t);
extern int bgl_getgid(void);
static obj_t BGl_z62openlogz62zz__osz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_setrlimitz12z12zz__osz00(obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_filezd2separatorzd2zz__osz00(void);
static obj_t BGl_symbol2900z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_umaskz00zz__osz00(obj_t);
static obj_t BGl_symbol2820z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2822z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2824z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2826z00zz__osz00 = BUNSPEC;
extern void bgl_sleep(long);
static obj_t BGl_symbol2828z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2749z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2669z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62chmodz62zz__osz00(obj_t, obj_t, obj_t);
extern obj_t bgl_display_obj(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_dynamiczd2unloadzd2zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2ze3listze3zz__osz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31783ze3ze5zz__osz00(obj_t, obj_t);
extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_oszd2charsetzd2zz__osz00(void);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62commandzd2linezb0zz__osz00(obj_t);
static obj_t BGl_z62filezd2namezd2ze3listz81zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_putenvz00zz__osz00(char *, char *);
static obj_t BGl_symbol2830z00zz__osz00 = BUNSPEC;
extern obj_t bgl_getgroups(void);
static obj_t BGl_symbol2832z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_makezd2staticzd2libzd2namezd2zz__osz00(obj_t, obj_t);
extern int bgl_setenv(char *, char *);
static obj_t BGl_symbol2671z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2834z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2754z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2836z00zz__osz00 = BUNSPEC;
extern obj_t bgl_dload(char *, char *, char *);
static obj_t BGl_symbol2838z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2758z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62syslogzd2levelzb0zz__osz00(obj_t, obj_t);
static obj_t BGl__dynamiczd2loadzd2symbolz00zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_getgidz00zz__osz00(void);
extern bool_t fexists(char *);
BGL_EXPORTED_DECL obj_t BGl_oszd2versionzd2zz__osz00(void);
extern obj_t BGl_warningz00zz__errorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_relativezd2filezd2namez00zz__osz00(obj_t, obj_t);
extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62makezd2filezd2namez62zz__osz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62sharedzd2libraryzd2suffixz62zz__osz00(obj_t);
static obj_t BGl_symbol2840z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62setrlimitz12z70zz__osz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2760z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_systemz00zz__osz00(obj_t);
static obj_t BGl_symbol2842z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_syslogzd2optionzd2zz__osz00(obj_t);
static obj_t BGl_symbol2844z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2682z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2846z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2848z00zz__osz00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__osz00(void);
static obj_t BGl_symbol2768z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2symbolzd2setzd2zz__osz00(obj_t, obj_t);
static obj_t BGl_z62pathzd2separatorzb0zz__osz00(obj_t);
static obj_t BGl_z62dynamiczd2loadzd2symbolzd2setzb0zz__osz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_commandzd2linezd2zz__osz00(void);
BGL_EXPORTED_DECL obj_t BGl_suffixz00zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_closelogz00zz__osz00(void);
static obj_t BGl_z62sleepz62zz__osz00(obj_t, obj_t);
static obj_t BGl_z62oszd2charsetzb0zz__osz00(obj_t);
static obj_t BGl__umaskz00zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_syslogzd2facilityzd2zz__osz00(obj_t);
static obj_t BGl_symbol2850z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62getgroupsz62zz__osz00(obj_t);
extern long bgl_bignum_to_long(obj_t);
BGL_EXPORTED_DECL int BGl_syslogzd2levelzd2zz__osz00(obj_t);
static obj_t BGl_z62syslogzd2facilityzb0zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_getenvz00zz__osz00(obj_t);
static obj_t BGl_symbol2855z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2canonicaliza7eza7zz__osz00(obj_t);
static obj_t BGl_symbol2857z00zz__osz00 = BUNSPEC;
static obj_t BGl_list2663z00zz__osz00 = BUNSPEC;
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
static obj_t BGl_symbol2859z00zz__osz00 = BUNSPEC;
static obj_t BGl_mingwzd2dirnamezd2zz__osz00(obj_t);
static obj_t BGl_z62findzd2filezf2pathz42zz__osz00(obj_t, obj_t, obj_t);
extern obj_t bigloo_module_mangle(obj_t, obj_t);
extern obj_t command_line;
static obj_t BGl_cnstzd2initzd2zz__osz00(void);
static obj_t BGl_z62oszd2versionzb0zz__osz00(obj_t);
static obj_t BGl_z62relativezd2filezd2namez62zz__osz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_getpidz00zz__osz00(void);
static obj_t BGl_genericzd2initzd2zz__osz00(void);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62oszd2tmpzb0zz__osz00(obj_t);
static obj_t BGl_symbol2861z00zz__osz00 = BUNSPEC;
extern obj_t bgl_getpwnam(char *);
extern obj_t bgl_dlsym_set(obj_t, obj_t);
static obj_t BGl_symbol2863z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62getpwnamz62zz__osz00(obj_t, obj_t);
static long BGl_requestzd2ze3elongz31zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_getpwuidz00zz__osz00(int);
BGL_EXPORTED_DECL bool_t BGl_chdirz00zz__osz00(char *);
static obj_t BGl_symbol2865z00zz__osz00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__osz00(void);
static obj_t BGl_mingwzd2basenamezd2zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(obj_t);
static obj_t BGl_symbol2867z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_prefixz00zz__osz00(obj_t);
static obj_t BGl_symbol2869z00zz__osz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__osz00(void);
static obj_t BGl_symbol2789z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_oszd2namezd2zz__osz00(void);
static obj_t BGl_z62filezd2namezd2canonicaliza7ez12zd7zz__osz00(obj_t, obj_t);
static obj_t BGl_z62setuidz62zz__osz00(obj_t, obj_t);
static obj_t BGl_z62oszd2namezb0zz__osz00(obj_t);
static obj_t BGl_objectzd2initzd2zz__osz00(void);
extern obj_t bgl_get_signal_handler(int);
BGL_EXPORTED_DECL int BGl_sigsetmaskz00zz__osz00(int);
extern bool_t bgl_ioctl(obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_makezd2filezd2pathz00zz__osz00(obj_t, obj_t, obj_t);
static obj_t BGl__getenvz00zz__osz00(obj_t, obj_t);
static obj_t BGl_z62makezd2staticzd2libraryzd2namezb0zz__osz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31320ze3ze5zz__osz00(obj_t);
static obj_t BGl_symbol2874z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_staticzd2libraryzd2suffixz00zz__osz00(void);
extern int bgl_dunload(obj_t);
static obj_t BGl_symbol2876z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_oszd2archzd2zz__osz00(void);
static obj_t BGl_symbol2878z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL char * BGl_executablezd2namezd2zz__osz00(void);
static obj_t BGl_z62oszd2archzb0zz__osz00(obj_t);
static obj_t BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(obj_t, obj_t, long);
static obj_t BGl_z62executablezd2namezb0zz__osz00(obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t bgl_reverse_bang(obj_t);
BGL_EXPORTED_DECL obj_t BGl_dirnamez00zz__osz00(obj_t);
extern char * bgl_dload_error(void);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
static obj_t BGl_z62dirnamez62zz__osz00(obj_t, obj_t);
static obj_t BGl_z62syslogz62zz__osz00(obj_t, obj_t, obj_t);
extern obj_t BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
extern obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
static obj_t BGl_symbol2880z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2882z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_pwdz00zz__osz00(void);
static obj_t BGl_symbol2884z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62syslogzd2optionzb0zz__osz00(obj_t, obj_t);
static obj_t BGl_symbol2886z00zz__osz00 = BUNSPEC;
extern obj_t bgl_dlsym(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2888z00zz__osz00 = BUNSPEC;
static obj_t BGl__dynamiczd2loadzd2zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_defaultzd2scriptzd2namez00zz__osz00(void);
extern obj_t bgl_getrlimit(long);
BGL_EXPORTED_DECL obj_t BGl_getzd2signalzd2handlerz00zz__osz00(int);
BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(obj_t);
static long BGl_popzd2directoryze70z35zz__osz00(unsigned char, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_getgroupsz00zz__osz00(void);
static obj_t BGl_z62sigsetmaskz62zz__osz00(obj_t, obj_t);
static obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez12z05zz__osz00(obj_t, obj_t);
static obj_t BGl_z62datez62zz__osz00(obj_t);
static obj_t BGl_z62signalz62zz__osz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_unixzd2pathzd2ze3listze3zz__osz00(obj_t);
static obj_t BGl_symbol2890z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2892z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62unixzd2pathzd2ze3listz81zz__osz00(obj_t, obj_t);
static obj_t BGl_symbol2894z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2896z00zz__osz00 = BUNSPEC;
static obj_t BGl_symbol2898z00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_basenamez00zz__osz00(obj_t);
extern obj_t bstring_to_symbol(obj_t);
extern obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_oszd2classzd2zz__osz00(void);
static obj_t BGl_z62setgidz62zz__osz00(obj_t, obj_t);
static obj_t BGl_z62getuidz62zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_pathzd2separatorzd2zz__osz00(void);
static obj_t BGl_z62systemzd2ze3stringz53zz__osz00(obj_t, obj_t);
static obj_t BGl_z62chdirz62zz__osz00(obj_t, obj_t);
extern int bgl_setuid(int);
static obj_t BGl_z62getrlimitz62zz__osz00(obj_t, obj_t);
static obj_t BGl_appendzd221011zd2zz__osz00(obj_t, obj_t);
static obj_t BGl_z62getzd2signalzd2handlerz62zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_filezd2namezd2unixzd2canonicaliza7ez12z67zz__osz00(obj_t);
static obj_t BGl_z62closelogz62zz__osz00(obj_t);
BGL_EXPORTED_DECL char * BGl_datez00zz__osz00(void);
static obj_t BGl_methodzd2initzd2zz__osz00(void);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_z62staticzd2libraryzd2suffixz62zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2sharedzd2libraryzd2namezd2zz__osz00(obj_t);
extern obj_t bgl_getenv_all(void);
extern obj_t blit_string(obj_t, long, obj_t, long, long);
static obj_t BGl_ioctlzd2requestszd2tablez00zz__osz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_oszd2tmpzd2zz__osz00(void);
BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2symbolzd2getzd2zz__osz00(obj_t);
static obj_t BGl_z62filezd2separatorzb0zz__osz00(obj_t);
static obj_t BGl_z62dynamiczd2loadzd2symbolzd2getzb0zz__osz00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t bgl_getpwuid(int);
BGL_EXPORTED_DECL obj_t BGl_setuidz00zz__osz00(int);
extern char * c_date(void);
static obj_t BGl_z62getpwuidz62zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2zz__osz00(obj_t, obj_t, obj_t);
extern char * executable_name;
static obj_t BGl_z62dynamiczd2unloadzb0zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ioctlz00zz__osz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_defaultzd2executablezd2namez00zz__osz00(void);
extern obj_t bgl_signal(int, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ioctlzd2registerzd2requestz12z12zz__osz00(obj_t, uint64_t);
extern obj_t make_string(long, unsigned char);
static obj_t BGl_z62putenvz62zz__osz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62defaultzd2executablezd2namez62zz__osz00(obj_t);
extern obj_t bgl_close_input_port(obj_t);
static obj_t BGl_z62ioctlzd2registerzd2requestz12z70zz__osz00(obj_t, obj_t, obj_t);
static obj_t BGl_defaultzd2dirnamezd2zz__osz00(obj_t);
static obj_t BGl_z62getgidz62zz__osz00(obj_t);
extern obj_t bgl_dlsym_get(obj_t);
extern obj_t string_append(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_syslogz00zz__osz00(int, obj_t);
static obj_t BGl_symbol2705z00zz__osz00 = BUNSPEC;
extern int bgl_setgid(int);
static obj_t BGl_symbol2707z00zz__osz00 = BUNSPEC;
extern int bgl_getuid(void);
static obj_t BGl_symbol2709z00zz__osz00 = BUNSPEC;
static obj_t BGl_z62systemz62zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_chmodz00zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_getppidz00zz__osz00(void);
extern obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_getrlimitz00zz__osz00(obj_t);
static obj_t BGl_z62getppidz62zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_dynamiczd2loadzd2symbolz00zz__osz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62suffixz62zz__osz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_signalz00zz__osz00(int, obj_t);
extern obj_t BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
static obj_t BGl_symbol2718z00zz__osz00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62filezd2namezd2canonicaliza7ezc5zz__osz00(obj_t, obj_t);
extern long BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00(obj_t, long);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2namezd2canonicaliza7ezd2envz75zz__osz00, BgL_bgl_za762fileza7d2nameza7d2908za7, BGl_z62filezd2namezd2canonicaliza7ezc5zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2841z00zz__osz00, BgL_bgl_string2841za700za7za7_2909za7, "LOG_LPR", 7 );
DEFINE_STRING( BGl_string2761z00zz__osz00, BgL_bgl_string2761za700za7za7_2910za7, "__dload_noinit", 14 );
DEFINE_STRING( BGl_string2680z00zz__osz00, BgL_bgl_string2680za700za7za7_2911za7, "&get-signal-handler", 19 );
DEFINE_STRING( BGl_string2843z00zz__osz00, BgL_bgl_string2843za700za7za7_2912za7, "LOG_MAIL", 8 );
DEFINE_STRING( BGl_string2762z00zz__osz00, BgL_bgl_string2762za700za7za7_2913za7, "dynamic-load: ", 14 );
DEFINE_STRING( BGl_string2681z00zz__osz00, BgL_bgl_string2681za700za7za7_2914za7, "&sigsetmask", 11 );
DEFINE_STRING( BGl_string2763z00zz__osz00, BgL_bgl_string2763za700za7za7_2915za7, "Cannot find library init entry point -- ", 40 );
DEFINE_STRING( BGl_string2845z00zz__osz00, BgL_bgl_string2845za700za7za7_2916za7, "LOG_NEWS", 8 );
DEFINE_STRING( BGl_string2764z00zz__osz00, BgL_bgl_string2764za700za7za7_2917za7, "Cannot find library init entry point", 36 );
DEFINE_STRING( BGl_string2683z00zz__osz00, BgL_bgl_string2683za700za7za7_2918za7, "getenv", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openlogzd2envzd2zz__osz00, BgL_bgl_za762openlogza762za7za7_2919z00, BGl_z62openlogz62zz__osz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2sharedzd2libzd2namezd2envz00zz__osz00, BgL_bgl_za762makeza7d2shared2920z00, BGl_z62makezd2sharedzd2libzd2namezb0zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2765z00zz__osz00, BgL_bgl_string2765za700za7za7_2921za7, "Can't find library", 18 );
DEFINE_STRING( BGl_string2684z00zz__osz00, BgL_bgl_string2684za700za7za7_2922za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string2847z00zz__osz00, BgL_bgl_string2847za700za7za7_2923za7, "LOG_SYSLOG", 10 );
DEFINE_STRING( BGl_string2766z00zz__osz00, BgL_bgl_string2766za700za7za7_2924za7, "dynamic-unload", 14 );
DEFINE_STRING( BGl_string2685z00zz__osz00, BgL_bgl_string2685za700za7za7_2925za7, "win32", 5 );
DEFINE_STRING( BGl_string2767z00zz__osz00, BgL_bgl_string2767za700za7za7_2926za7, "&dynamic-unload", 15 );
DEFINE_STRING( BGl_string2686z00zz__osz00, BgL_bgl_string2686za700za7za7_2927za7, "HOME", 4 );
DEFINE_STRING( BGl_string2849z00zz__osz00, BgL_bgl_string2849za700za7za7_2928za7, "LOG_USER", 8 );
DEFINE_STRING( BGl_string2687z00zz__osz00, BgL_bgl_string2687za700za7za7_2929za7, "USERPROFILE", 11 );
DEFINE_STRING( BGl_string2769z00zz__osz00, BgL_bgl_string2769za700za7za7_2930za7, "dynamic-load-symbol", 19 );
DEFINE_STRING( BGl_string2688z00zz__osz00, BgL_bgl_string2688za700za7za7_2931za7, "&putenv", 7 );
DEFINE_STRING( BGl_string2689z00zz__osz00, BgL_bgl_string2689za700za7za7_2932za7, "bstring", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_syslogzd2envzd2zz__osz00, BgL_bgl_za762syslogza762za7za7__2933z00, va_generic_entry, BGl_z62syslogz62zz__osz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getppidzd2envzd2zz__osz00, BgL_bgl_za762getppidza762za7za7_2934z00, BGl_z62getppidz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2851z00zz__osz00, BgL_bgl_string2851za700za7za7_2935za7, "LOG_UUCP", 8 );
DEFINE_STRING( BGl_string2770z00zz__osz00, BgL_bgl_string2770za700za7za7_2936za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getrlimitzd2envzd2zz__osz00, BgL_bgl_za762getrlimitza762za72937za7, BGl_z62getrlimitz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2852z00zz__osz00, BgL_bgl_string2852za700za7za7_2938za7, "syslog-facility", 15 );
DEFINE_STRING( BGl_string2771z00zz__osz00, BgL_bgl_string2771za700za7za7_2939za7, "_dynamic-load-symbol", 20 );
DEFINE_STRING( BGl_string2690z00zz__osz00, BgL_bgl_string2690za700za7za7_2940za7, "system", 6 );
DEFINE_STRING( BGl_string2853z00zz__osz00, BgL_bgl_string2853za700za7za7_2941za7, "unknown facility", 16 );
DEFINE_STRING( BGl_string2772z00zz__osz00, BgL_bgl_string2772za700za7za7_2942za7, "&dynamic-load-symbol-get", 24 );
DEFINE_STRING( BGl_string2691z00zz__osz00, BgL_bgl_string2691za700za7za7_2943za7, "pair", 4 );
DEFINE_STRING( BGl_string2854z00zz__osz00, BgL_bgl_string2854za700za7za7_2944za7, "&syslog-facility", 16 );
DEFINE_STRING( BGl_string2773z00zz__osz00, BgL_bgl_string2773za700za7za7_2945za7, "custom", 6 );
DEFINE_STRING( BGl_string2692z00zz__osz00, BgL_bgl_string2692za700za7za7_2946za7, "| ", 2 );
DEFINE_STRING( BGl_string2774z00zz__osz00, BgL_bgl_string2774za700za7za7_2947za7, "&dynamic-load-symbol-set", 24 );
DEFINE_STRING( BGl_string2693z00zz__osz00, BgL_bgl_string2693za700za7za7_2948za7, "system->string", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2loadzd2symbolzd2envzd2zz__osz00, BgL_bgl__dynamicza7d2loadza72949z00, opt_generic_entry, BGl__dynamiczd2loadzd2symbolz00zz__osz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2856z00zz__osz00, BgL_bgl_string2856za700za7za7_2950za7, "LOG_EMERG", 9 );
DEFINE_STRING( BGl_string2775z00zz__osz00, BgL_bgl_string2775za700za7za7_2951za7, "&unix-path->list", 16 );
DEFINE_STRING( BGl_string2694z00zz__osz00, BgL_bgl_string2694za700za7za7_2952za7, "input-port", 10 );
DEFINE_STRING( BGl_string2776z00zz__osz00, BgL_bgl_string2776za700za7za7_2953za7, "&setuid", 7 );
DEFINE_STRING( BGl_string2695z00zz__osz00, BgL_bgl_string2695za700za7za7_2954za7, "<@anonymous:1320>", 17 );
DEFINE_STRING( BGl_string2858z00zz__osz00, BgL_bgl_string2858za700za7za7_2955za7, "LOG_ALERT", 9 );
DEFINE_STRING( BGl_string2777z00zz__osz00, BgL_bgl_string2777za700za7za7_2956za7, "&setgid", 7 );
DEFINE_STRING( BGl_string2696z00zz__osz00, BgL_bgl_string2696za700za7za7_2957za7, "string-ref", 10 );
DEFINE_STRING( BGl_string2778z00zz__osz00, BgL_bgl_string2778za700za7za7_2958za7, "&getpwnam", 9 );
DEFINE_STRING( BGl_string2697z00zz__osz00, BgL_bgl_string2697za700za7za7_2959za7, "&chdir", 6 );
DEFINE_STRING( BGl_string2779z00zz__osz00, BgL_bgl_string2779za700za7za7_2960za7, "&getpwuid", 9 );
DEFINE_STRING( BGl_string2698z00zz__osz00, BgL_bgl_string2698za700za7za7_2961za7, "mingw", 5 );
DEFINE_STRING( BGl_string2699z00zz__osz00, BgL_bgl_string2699za700za7za7_2962za7, "&basename", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sigsetmaskzd2envzd2zz__osz00, BgL_bgl_za762sigsetmaskza7622963z00, BGl_z62sigsetmaskz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2envzd2zz__osz00, BgL_bgl_za762dateza762za7za7__os2964z00, BGl_z62datez62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2filezf2pathzd2envzf2zz__osz00, BgL_bgl_za762findza7d2fileza7f2965za7, BGl_z62findzd2filezf2pathz42zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2860z00zz__osz00, BgL_bgl_string2860za700za7za7_2966za7, "LOG_CRIT", 8 );
DEFINE_STRING( BGl_string2780z00zz__osz00, BgL_bgl_string2780za700za7za7_2967za7, "&ioctl-register-request!", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2sharedzd2libraryzd2namezd2envz00zz__osz00, BgL_bgl_za762makeza7d2shared2968z00, BGl_z62makezd2sharedzd2libraryzd2namezb0zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2862z00zz__osz00, BgL_bgl_string2862za700za7za7_2969za7, "LOG_ERR", 7 );
DEFINE_STRING( BGl_string2781z00zz__osz00, BgL_bgl_string2781za700za7za7_2970za7, "buint64", 7 );
DEFINE_STRING( BGl_string2782z00zz__osz00, BgL_bgl_string2782za700za7za7_2971za7, "ioctl", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getgidzd2envzd2zz__osz00, BgL_bgl_za762getgidza762za7za7__2972z00, BGl_z62getgidz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2864z00zz__osz00, BgL_bgl_string2864za700za7za7_2973za7, "LOG_WARNING", 11 );
DEFINE_STRING( BGl_string2783z00zz__osz00, BgL_bgl_string2783za700za7za7_2974za7, "number of string", 16 );
DEFINE_STRING( BGl_string2784z00zz__osz00, BgL_bgl_string2784za700za7za7_2975za7, "request->elong", 14 );
DEFINE_STRING( BGl_string2866z00zz__osz00, BgL_bgl_string2866za700za7za7_2976za7, "LOG_NOTICE", 10 );
DEFINE_STRING( BGl_string2785z00zz__osz00, BgL_bgl_string2785za700za7za7_2977za7, "belong", 6 );
DEFINE_STRING( BGl_string2786z00zz__osz00, BgL_bgl_string2786za700za7za7_2978za7, "->elong", 7 );
DEFINE_STRING( BGl_string2868z00zz__osz00, BgL_bgl_string2868za700za7za7_2979za7, "LOG_INFO", 8 );
DEFINE_STRING( BGl_string2787z00zz__osz00, BgL_bgl_string2787za700za7za7_2980za7, "real", 4 );
DEFINE_STRING( BGl_string2788z00zz__osz00, BgL_bgl_string2788za700za7za7_2981za7, "elong pair", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2loadzd2symbolzd2getzd2envz00zz__osz00, BgL_bgl_za762dynamicza7d2loa2982z00, BGl_z62dynamiczd2loadzd2symbolzd2getzb0zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_pwdzd2envzd2zz__osz00, BgL_bgl_za762pwdza762za7za7__osza72983za7, BGl_z62pwdz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setuidzd2envzd2zz__osz00, BgL_bgl_za762setuidza762za7za7__2984z00, BGl_z62setuidz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2870z00zz__osz00, BgL_bgl_string2870za700za7za7_2985za7, "LOG_DEBUG", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2loadzd2envz00zz__osz00, BgL_bgl__dynamicza7d2loadza72986z00, opt_generic_entry, BGl__dynamiczd2loadzd2zz__osz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2871z00zz__osz00, BgL_bgl_string2871za700za7za7_2987za7, "syslog-level", 12 );
DEFINE_STRING( BGl_string2790z00zz__osz00, BgL_bgl_string2790za700za7za7_2988za7, "umask", 5 );
DEFINE_STRING( BGl_string2872z00zz__osz00, BgL_bgl_string2872za700za7za7_2989za7, "unknown level", 13 );
DEFINE_STRING( BGl_string2791z00zz__osz00, BgL_bgl_string2791za700za7za7_2990za7, "_umask", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_commandzd2linezd2envz00zz__osz00, BgL_bgl_za762commandza7d2lin2991z00, BGl_z62commandzd2linezb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2873z00zz__osz00, BgL_bgl_string2873za700za7za7_2992za7, "&syslog-level", 13 );
DEFINE_STRING( BGl_string2792z00zz__osz00, BgL_bgl_string2792za700za7za7_2993za7, "long", 4 );
DEFINE_STRING( BGl_string2793z00zz__osz00, BgL_bgl_string2793za700za7za7_2994za7, "&openlog", 8 );
DEFINE_STRING( BGl_string2875z00zz__osz00, BgL_bgl_string2875za700za7za7_2995za7, "CORE", 4 );
DEFINE_STRING( BGl_string2794z00zz__osz00, BgL_bgl_string2794za700za7za7_2996za7, "%s", 2 );
DEFINE_STRING( BGl_string2795z00zz__osz00, BgL_bgl_string2795za700za7za7_2997za7, "&syslog", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closelogzd2envzd2zz__osz00, BgL_bgl_za762closelogza762za7za72998z00, BGl_z62closelogz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2877z00zz__osz00, BgL_bgl_string2877za700za7za7_2999za7, "CPU", 3 );
DEFINE_STRING( BGl_string2796z00zz__osz00, BgL_bgl_string2796za700za7za7_3000za7, "<@anonymous:1784>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sleepzd2envzd2zz__osz00, BgL_bgl_za762sleepza762za7za7__o3001z00, BGl_z62sleepz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2797z00zz__osz00, BgL_bgl_string2797za700za7za7_3002za7, "output-port", 11 );
DEFINE_STRING( BGl_string2879z00zz__osz00, BgL_bgl_string2879za700za7za7_3003za7, "DATA", 4 );
DEFINE_STRING( BGl_string2798z00zz__osz00, BgL_bgl_string2798za700za7za7_3004za7, "for-each", 8 );
DEFINE_STRING( BGl_string2799z00zz__osz00, BgL_bgl_string2799za700za7za7_3005za7, "list", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_defaultzd2executablezd2namezd2envzd2zz__osz00, BgL_bgl_za762defaultza7d2exe3006z00, BGl_z62defaultzd2executablezd2namez62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2881z00zz__osz00, BgL_bgl_string2881za700za7za7_3007za7, "FSIZE", 5 );
DEFINE_STRING( BGl_string2883z00zz__osz00, BgL_bgl_string2883za700za7za7_3008za7, "LOCKS", 5 );
DEFINE_STRING( BGl_string2885z00zz__osz00, BgL_bgl_string2885za700za7za7_3009za7, "MEMLOCK", 7 );
DEFINE_STRING( BGl_string2887z00zz__osz00, BgL_bgl_string2887za700za7za7_3010za7, "MSGQUEUE", 8 );
DEFINE_STRING( BGl_string2889z00zz__osz00, BgL_bgl_string2889za700za7za7_3011za7, "NICE", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_umaskzd2envzd2zz__osz00, BgL_bgl__umaskza700za7za7__osza73012z00, opt_generic_entry, BGl__umaskz00zz__osz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_pathzd2separatorzd2envz00zz__osz00, BgL_bgl_za762pathza7d2separa3013z00, BGl_z62pathzd2separatorzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2namezd2canonicaliza7ez12zd2envz67zz__osz00, BgL_bgl_za762fileza7d2nameza7d3014za7, BGl_z62filezd2namezd2canonicaliza7ez12zd7zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oszd2namezd2envz00zz__osz00, BgL_bgl_za762osza7d2nameza7b0za73015z00, BGl_z62oszd2namezb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2891z00zz__osz00, BgL_bgl_string2891za700za7za7_3016za7, "NOFILE", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2unloadzd2envz00zz__osz00, BgL_bgl_za762dynamicza7d2unl3017z00, BGl_z62dynamiczd2unloadzb0zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2namezd2ze3listzd2envz31zz__osz00, BgL_bgl_za762fileza7d2nameza7d3018za7, BGl_z62filezd2namezd2ze3listz81zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2893z00zz__osz00, BgL_bgl_string2893za700za7za7_3019za7, "NPROC", 5 );
DEFINE_STRING( BGl_string2895z00zz__osz00, BgL_bgl_string2895za700za7za7_3020za7, "RSS", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oszd2charsetzd2envz00zz__osz00, BgL_bgl_za762osza7d2charsetza73021za7, BGl_z62oszd2charsetzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2897z00zz__osz00, BgL_bgl_string2897za700za7za7_3022za7, "RTTIME", 6 );
DEFINE_STRING( BGl_string2899z00zz__osz00, BgL_bgl_string2899za700za7za7_3023za7, "SIGPENDING", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_signalzd2envzd2zz__osz00, BgL_bgl_za762signalza762za7za7__3024z00, BGl_z62signalz62zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2staticzd2libzd2namezd2envz00zz__osz00, BgL_bgl_za762makeza7d2static3025z00, BGl_z62makezd2staticzd2libzd2namezb0zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_staticzd2libraryzd2suffixzd2envzd2zz__osz00, BgL_bgl_za762staticza7d2libr3026z00, BGl_z62staticzd2libraryzd2suffixz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_executablezd2namezd2envz00zz__osz00, BgL_bgl_za762executableza7d23027z00, BGl_z62executablezd2namezb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oszd2versionzd2envz00zz__osz00, BgL_bgl_za762osza7d2versionza73028za7, BGl_z62oszd2versionzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dirnamezd2envzd2zz__osz00, BgL_bgl_za762dirnameza762za7za7_3029z00, BGl_z62dirnamez62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getuidzd2envzd2zz__osz00, BgL_bgl_za762getuidza762za7za7__3030z00, BGl_z62getuidz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_systemzd2envzd2zz__osz00, BgL_bgl_za762systemza762za7za7__3031z00, va_generic_entry, BGl_z62systemz62zz__osz00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_suffixzd2envzd2zz__osz00, BgL_bgl_za762suffixza762za7za7__3032z00, BGl_z62suffixz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ioctlzd2envzd2zz__osz00, BgL_bgl_za762ioctlza762za7za7__o3033z00, BGl_z62ioctlz62zz__osz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2namezd2unixzd2canonicaliza7ezd2envza7zz__osz00, BgL_bgl_za762fileza7d2nameza7d3034za7, BGl_z62filezd2namezd2unixzd2canonicaliza7ez17zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getgroupszd2envzd2zz__osz00, BgL_bgl_za762getgroupsza762za73035za7, BGl_z62getgroupsz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getenvzd2envzd2zz__osz00, BgL_bgl__getenvza700za7za7__os3036za7, opt_generic_entry, BGl__getenvz00zz__osz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_basenamezd2envzd2zz__osz00, BgL_bgl_za762basenameza762za7za73037z00, BGl_z62basenamez62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getpidzd2envzd2zz__osz00, BgL_bgl_za762getpidza762za7za7__3038z00, BGl_z62getpidz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setrlimitz12zd2envzc0zz__osz00, BgL_bgl_za762setrlimitza712za73039za7, BGl_z62setrlimitz12z70zz__osz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2separatorzd2envz00zz__osz00, BgL_bgl_za762fileza7d2separa3040z00, BGl_z62filezd2separatorzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getpwuidzd2envzd2zz__osz00, BgL_bgl_za762getpwuidza762za7za73041z00, BGl_z62getpwuidz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_chmodzd2envzd2zz__osz00, BgL_bgl_za762chmodza762za7za7__o3042z00, va_generic_entry, BGl_z62chmodz62zz__osz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_putenvzd2envzd2zz__osz00, BgL_bgl_za762putenvza762za7za7__3043z00, BGl_z62putenvz62zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2filezd2pathzd2envzd2zz__osz00, BgL_bgl_za762makeza7d2fileza7d3044za7, va_generic_entry, BGl_z62makezd2filezd2pathz62zz__osz00, BUNSPEC, -3 );
DEFINE_STRING( BGl_string2700z00zz__osz00, BgL_bgl_string2700za700za7za7_3045za7, "&prefix", 7 );
DEFINE_STRING( BGl_string2701z00zz__osz00, BgL_bgl_string2701za700za7za7_3046za7, "&dirname", 8 );
DEFINE_STRING( BGl_string2702z00zz__osz00, BgL_bgl_string2702za700za7za7_3047za7, "", 0 );
DEFINE_STRING( BGl_string2703z00zz__osz00, BgL_bgl_string2703za700za7za7_3048za7, "&suffix", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oszd2archzd2envz00zz__osz00, BgL_bgl_za762osza7d2archza7b0za73049z00, BGl_z62oszd2archzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2704z00zz__osz00, BgL_bgl_string2704za700za7za7_3050za7, "loop", 4 );
DEFINE_STRING( BGl_string2706z00zz__osz00, BgL_bgl_string2706za700za7za7_3051za7, "read", 4 );
DEFINE_STRING( BGl_string2708z00zz__osz00, BgL_bgl_string2708za700za7za7_3052za7, "write", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_sharedzd2libraryzd2suffixzd2envzd2zz__osz00, BgL_bgl_za762sharedza7d2libr3053z00, BGl_z62sharedzd2libraryzd2suffixz62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oszd2tmpzd2envz00zz__osz00, BgL_bgl_za762osza7d2tmpza7b0za7za73054za7, BGl_z62oszd2tmpzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getpwnamzd2envzd2zz__osz00, BgL_bgl_za762getpwnamza762za7za73055z00, BGl_z62getpwnamz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_systemzd2ze3stringzd2envze3zz__osz00, BgL_bgl_za762systemza7d2za7e3s3056za7, va_generic_entry, BGl_z62systemzd2ze3stringz53zz__osz00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string2710z00zz__osz00, BgL_bgl_string2710za700za7za7_3057za7, "execute", 7 );
DEFINE_STRING( BGl_string2711z00zz__osz00, BgL_bgl_string2711za700za7za7_3058za7, "chmod", 5 );
DEFINE_STRING( BGl_string2712z00zz__osz00, BgL_bgl_string2712za700za7za7_3059za7, "Unknown mode", 12 );
DEFINE_STRING( BGl_string2713z00zz__osz00, BgL_bgl_string2713za700za7za7_3060za7, "&chmod", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2loadzd2symbolzd2setzd2envz00zz__osz00, BgL_bgl_za762dynamicza7d2loa3061z00, BGl_z62dynamiczd2loadzd2symbolzd2setzb0zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2714z00zz__osz00, BgL_bgl_string2714za700za7za7_3062za7, "&make-file-name", 15 );
DEFINE_STRING( BGl_string2715z00zz__osz00, BgL_bgl_string2715za700za7za7_3063za7, "make-file-path", 14 );
DEFINE_STRING( BGl_string2716z00zz__osz00, BgL_bgl_string2716za700za7za7_3064za7, "string", 6 );
DEFINE_STRING( BGl_string2717z00zz__osz00, BgL_bgl_string2717za700za7za7_3065za7, "&make-file-path", 15 );
DEFINE_STRING( BGl_string2719z00zz__osz00, BgL_bgl_string2719za700za7za7_3066za7, "bigloo-c", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_defaultzd2scriptzd2namezd2envzd2zz__osz00, BgL_bgl_za762defaultza7d2scr3067z00, BGl_z62defaultzd2scriptzd2namez62zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2staticzd2libraryzd2namezd2envz00zz__osz00, BgL_bgl_za762makeza7d2static3068z00, BGl_z62makezd2staticzd2libraryzd2namezb0zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ioctlzd2registerzd2requestz12zd2envzc0zz__osz00, BgL_bgl_za762ioctlza7d2regis3069z00, BGl_z62ioctlzd2registerzd2requestz12z70zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unixzd2pathzd2ze3listzd2envz31zz__osz00, BgL_bgl_za762unixza7d2pathza7d3070za7, BGl_z62unixzd2pathzd2ze3listz81zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_syslogzd2levelzd2envz00zz__osz00, BgL_bgl_za762syslogza7d2leve3071z00, BGl_z62syslogzd2levelzb0zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2801z00zz__osz00, BgL_bgl_string2801za700za7za7_3072za7, "LOG_CONS", 8 );
DEFINE_STRING( BGl_string2720z00zz__osz00, BgL_bgl_string2720za700za7za7_3073za7, "lib", 3 );
DEFINE_STRING( BGl_string2803z00zz__osz00, BgL_bgl_string2803za700za7za7_3074za7, "LOG_NDELAY", 10 );
DEFINE_STRING( BGl_string2722z00zz__osz00, BgL_bgl_string2722za700za7za7_3075za7, "bigloo-jvm", 10 );
DEFINE_STRING( BGl_string2723z00zz__osz00, BgL_bgl_string2723za700za7za7_3076za7, ".zip", 4 );
DEFINE_STRING( BGl_string2805z00zz__osz00, BgL_bgl_string2805za700za7za7_3077za7, "LOG_NOWAIT", 10 );
DEFINE_STRING( BGl_string2725z00zz__osz00, BgL_bgl_string2725za700za7za7_3078za7, "bigloo-.net", 11 );
DEFINE_STRING( BGl_string2807z00zz__osz00, BgL_bgl_string2807za700za7za7_3079za7, "LOG_ODELAY", 10 );
DEFINE_STRING( BGl_string2726z00zz__osz00, BgL_bgl_string2726za700za7za7_3080za7, ".dll", 4 );
DEFINE_STRING( BGl_string2727z00zz__osz00, BgL_bgl_string2727za700za7za7_3081za7, "make-static-lib-name", 20 );
DEFINE_STRING( BGl_string2809z00zz__osz00, BgL_bgl_string2809za700za7za7_3082za7, "LOG_PID", 7 );
DEFINE_STRING( BGl_string2728z00zz__osz00, BgL_bgl_string2728za700za7za7_3083za7, "Unknown backend", 15 );
DEFINE_STRING( BGl_string2729z00zz__osz00, BgL_bgl_string2729za700za7za7_3084za7, "&make-static-lib-name", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2filezd2namezd2envzd2zz__osz00, BgL_bgl_za762makeza7d2fileza7d3085za7, BGl_z62makezd2filezd2namez62zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_oszd2classzd2envz00zz__osz00, BgL_bgl_za762osza7d2classza7b03086za7, BGl_z62oszd2classzb0zz__osz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_chdirzd2envzd2zz__osz00, BgL_bgl_za762chdirza762za7za7__o3087z00, BGl_z62chdirz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2810z00zz__osz00, BgL_bgl_string2810za700za7za7_3088za7, "syslog-option", 13 );
DEFINE_STRING( BGl_string2811z00zz__osz00, BgL_bgl_string2811za700za7za7_3089za7, "unknown option", 14 );
DEFINE_STRING( BGl_string2730z00zz__osz00, BgL_bgl_string2730za700za7za7_3090za7, "symbol", 6 );
DEFINE_STRING( BGl_string2731z00zz__osz00, BgL_bgl_string2731za700za7za7_3091za7, "make-shared-lib-name", 20 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_prefixzd2envzd2zz__osz00, BgL_bgl_za762prefixza762za7za7__3092z00, BGl_z62prefixz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2813z00zz__osz00, BgL_bgl_string2813za700za7za7_3093za7, "LOG_AUTH", 8 );
DEFINE_STRING( BGl_string2732z00zz__osz00, BgL_bgl_string2732za700za7za7_3094za7, "&make-shared-lib-name", 21 );
DEFINE_STRING( BGl_string2733z00zz__osz00, BgL_bgl_string2733za700za7za7_3095za7, "&find-file/path", 15 );
DEFINE_STRING( BGl_string2815z00zz__osz00, BgL_bgl_string2815za700za7za7_3096za7, "LOG_AUTHPRIV", 12 );
DEFINE_STRING( BGl_string2734z00zz__osz00, BgL_bgl_string2734za700za7za7_3097za7, "&file-name->list", 16 );
DEFINE_STRING( BGl_string2735z00zz__osz00, BgL_bgl_string2735za700za7za7_3098za7, "string-set!", 11 );
DEFINE_STRING( BGl_string2817z00zz__osz00, BgL_bgl_string2817za700za7za7_3099za7, "LOG_CRON", 8 );
DEFINE_STRING( BGl_string2736z00zz__osz00, BgL_bgl_string2736za700za7za7_3100za7, "&file-name-canonicalize", 23 );
DEFINE_STRING( BGl_string2737z00zz__osz00, BgL_bgl_string2737za700za7za7_3101za7, "&file-name-canonicalize!", 24 );
DEFINE_STRING( BGl_string2819z00zz__osz00, BgL_bgl_string2819za700za7za7_3102za7, "LOG_DAEMON", 10 );
DEFINE_STRING( BGl_string2738z00zz__osz00, BgL_bgl_string2738za700za7za7_3103za7, "file-name-unix-canonicalize", 27 );
DEFINE_STRING( BGl_string2739z00zz__osz00, BgL_bgl_string2739za700za7za7_3104za7, "..", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_filezd2namezd2unixzd2canonicaliza7ez12zd2envzb5zz__osz00, BgL_bgl_za762fileza7d2nameza7d3105za7, BGl_z62filezd2namezd2unixzd2canonicaliza7ez12z05zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2901z00zz__osz00, BgL_bgl_string2901za700za7za7_3106za7, "STACK", 5 );
DEFINE_STRING( BGl_string2902z00zz__osz00, BgL_bgl_string2902za700za7za7_3107za7, "illegal limit resource", 22 );
DEFINE_STRING( BGl_string2821z00zz__osz00, BgL_bgl_string2821za700za7za7_3108za7, "LOG_FTP", 7 );
DEFINE_STRING( BGl_string2740z00zz__osz00, BgL_bgl_string2740za700za7za7_3109za7, "&file-name-unix-canonicalize", 28 );
DEFINE_STRING( BGl_string2903z00zz__osz00, BgL_bgl_string2903za700za7za7_3110za7, "integer-or-symbol", 17 );
DEFINE_STRING( BGl_string2741z00zz__osz00, BgL_bgl_string2741za700za7za7_3111za7, "&file-name-unix-canonicalize!", 29 );
DEFINE_STRING( BGl_string2904z00zz__osz00, BgL_bgl_string2904za700za7za7_3112za7, "getrlimit", 9 );
DEFINE_STRING( BGl_string2823z00zz__osz00, BgL_bgl_string2823za700za7za7_3113za7, "LOG_KERN", 8 );
DEFINE_STRING( BGl_string2742z00zz__osz00, BgL_bgl_string2742za700za7za7_3114za7, "make-file", 9 );
DEFINE_STRING( BGl_string2905z00zz__osz00, BgL_bgl_string2905za700za7za7_3115za7, "setrlimit!", 10 );
DEFINE_STRING( BGl_string2743z00zz__osz00, BgL_bgl_string2743za700za7za7_3116za7, "relative-file-name", 18 );
DEFINE_STRING( BGl_string2906z00zz__osz00, BgL_bgl_string2906za700za7za7_3117za7, "&setrlimit!", 11 );
DEFINE_STRING( BGl_string2825z00zz__osz00, BgL_bgl_string2825za700za7za7_3118za7, "LOG_LOCAL0", 10 );
DEFINE_STRING( BGl_string2744z00zz__osz00, BgL_bgl_string2744za700za7za7_3119za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2907z00zz__osz00, BgL_bgl_string2907za700za7za7_3120za7, "__os", 4 );
DEFINE_STRING( BGl_string2745z00zz__osz00, BgL_bgl_string2745za700za7za7_3121za7, "&relative-file-name", 19 );
DEFINE_STRING( BGl_string2664z00zz__osz00, BgL_bgl_string2664za700za7za7_3122za7, ".", 1 );
DEFINE_STRING( BGl_string2827z00zz__osz00, BgL_bgl_string2827za700za7za7_3123za7, "LOG_LOCAL1", 10 );
DEFINE_STRING( BGl_string2746z00zz__osz00, BgL_bgl_string2746za700za7za7_3124za7, "&make-static-library-name", 25 );
DEFINE_STRING( BGl_string2665z00zz__osz00, BgL_bgl_string2665za700za7za7_3125za7, "bigloo.foreign", 14 );
DEFINE_STRING( BGl_string2747z00zz__osz00, BgL_bgl_string2747za700za7za7_3126za7, "&make-shared-library-name", 25 );
DEFINE_STRING( BGl_string2666z00zz__osz00, BgL_bgl_string2666za700za7za7_3127za7, "LANG", 4 );
DEFINE_STRING( BGl_string2829z00zz__osz00, BgL_bgl_string2829za700za7za7_3128za7, "LOG_LOCAL2", 10 );
DEFINE_STRING( BGl_string2748z00zz__osz00, BgL_bgl_string2748za700za7za7_3129za7, "&sleep", 6 );
DEFINE_STRING( BGl_string2667z00zz__osz00, BgL_bgl_string2667za700za7za7_3130za7, "LC_CTYPE", 8 );
DEFINE_STRING( BGl_string2668z00zz__osz00, BgL_bgl_string2668za700za7za7_3131za7, "LC_ALL", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_relativezd2filezd2namezd2envzd2zz__osz00, BgL_bgl_za762relativeza7d2fi3132z00, BGl_z62relativezd2filezd2namez62zz__osz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setgidzd2envzd2zz__osz00, BgL_bgl_za762setgidza762za7za7__3133z00, BGl_z62setgidz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_syslogzd2optionzd2envz00zz__osz00, BgL_bgl_za762syslogza7d2opti3134z00, va_generic_entry, BGl_z62syslogzd2optionzb0zz__osz00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string2831z00zz__osz00, BgL_bgl_string2831za700za7za7_3135za7, "LOG_LOCAL3", 10 );
DEFINE_STRING( BGl_string2750z00zz__osz00, BgL_bgl_string2750za700za7za7_3136za7, "dynamic-load", 12 );
DEFINE_STRING( BGl_string2751z00zz__osz00, BgL_bgl_string2751za700za7za7_3137za7, "wrong number of arguments: [1..3] expected, provided", 52 );
DEFINE_STRING( BGl_string2670z00zz__osz00, BgL_bgl_string2670za700za7za7_3138za7, "ignore", 6 );
DEFINE_STRING( BGl_string2833z00zz__osz00, BgL_bgl_string2833za700za7za7_3139za7, "LOG_LOCAL4", 10 );
DEFINE_STRING( BGl_string2752z00zz__osz00, BgL_bgl_string2752za700za7za7_3140za7, "_dynamic-load", 13 );
DEFINE_STRING( BGl_string2753z00zz__osz00, BgL_bgl_string2753za700za7za7_3141za7, "module-initialization", 21 );
DEFINE_STRING( BGl_string2672z00zz__osz00, BgL_bgl_string2672za700za7za7_3142za7, "default", 7 );
DEFINE_STRING( BGl_string2835z00zz__osz00, BgL_bgl_string2835za700za7za7_3143za7, "LOG_LOCAL5", 10 );
DEFINE_STRING( BGl_string2673z00zz__osz00, BgL_bgl_string2673za700za7za7_3144za7, "/tmp/bigloo/runtime/Llib/os.scm", 31 );
DEFINE_STRING( BGl_string2755z00zz__osz00, BgL_bgl_string2755za700za7za7_3145za7, "__dload_noarch", 14 );
DEFINE_STRING( BGl_string2674z00zz__osz00, BgL_bgl_string2674za700za7za7_3146za7, "signal", 6 );
DEFINE_STRING( BGl_string2837z00zz__osz00, BgL_bgl_string2837za700za7za7_3147za7, "LOG_LOCAL6", 10 );
DEFINE_STRING( BGl_string2756z00zz__osz00, BgL_bgl_string2756za700za7za7_3148za7, "dynamic-load:", 13 );
DEFINE_STRING( BGl_string2675z00zz__osz00, BgL_bgl_string2675za700za7za7_3149za7, "procedure", 9 );
DEFINE_STRING( BGl_string2757z00zz__osz00, BgL_bgl_string2757za700za7za7_3150za7, "Not supported on this architecture", 34 );
DEFINE_STRING( BGl_string2676z00zz__osz00, BgL_bgl_string2676za700za7za7_3151za7, "Illegal signal", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2signalzd2handlerzd2envzd2zz__osz00, BgL_bgl_za762getza7d2signalza73152za7, BGl_z62getzd2signalzd2handlerz62zz__osz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2839z00zz__osz00, BgL_bgl_string2839za700za7za7_3153za7, "LOG_LOCAL7", 10 );
DEFINE_STRING( BGl_string2677z00zz__osz00, BgL_bgl_string2677za700za7za7_3154za7, "Wrong number of arguments", 25 );
DEFINE_STRING( BGl_string2759z00zz__osz00, BgL_bgl_string2759za700za7za7_3155za7, "__dload_error", 13 );
DEFINE_STRING( BGl_string2678z00zz__osz00, BgL_bgl_string2678za700za7za7_3156za7, "&signal", 7 );
DEFINE_STRING( BGl_string2679z00zz__osz00, BgL_bgl_string2679za700za7za7_3157za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_syslogzd2facilityzd2envz00zz__osz00, BgL_bgl_za762syslogza7d2faci3158z00, BGl_z62syslogzd2facilityzb0zz__osz00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol2800z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_za2defaultzd2javazd2packageza2z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2802z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2721z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2804z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2724z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2806z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2808z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2812z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2814z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2816z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2818z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2900z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2820z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2822z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2824z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2826z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2828z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2749z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2669z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2830z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2832z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2671z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2834z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2754z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2836z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2838z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2758z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2840z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2760z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2842z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2844z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2682z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2846z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2848z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2768z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2850z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2855z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2857z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_list2663z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2859z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2861z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2863z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2865z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2867z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2869z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2789z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2874z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2876z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2878z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2880z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2882z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2884z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2886z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2888z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2890z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2892z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2894z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2896z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2898z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_ioctlzd2requestszd2tablez00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2705z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2707z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2709z00zz__osz00) );
ADD_ROOT( (void *)(&BGl_symbol2718z00zz__osz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__osz00(long BgL_checksumz00_4200, char * BgL_fromz00_4201)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__osz00))
{ 
BGl_requirezd2initializa7ationz75zz__osz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__osz00(); 
BGl_cnstzd2initzd2zz__osz00(); 
BGl_importedzd2moduleszd2initz00zz__osz00(); 
return 
BGl_toplevelzd2initzd2zz__osz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__osz00(void)
{
{ /* Llib/os.scm 18 */
BGl_list2663z00zz__osz00 = 
MAKE_YOUNG_PAIR(BGl_string2664z00zz__osz00, BNIL); 
BGl_symbol2669z00zz__osz00 = 
bstring_to_symbol(BGl_string2670z00zz__osz00); 
BGl_symbol2671z00zz__osz00 = 
bstring_to_symbol(BGl_string2672z00zz__osz00); 
BGl_symbol2682z00zz__osz00 = 
bstring_to_symbol(BGl_string2683z00zz__osz00); 
BGl_symbol2705z00zz__osz00 = 
bstring_to_symbol(BGl_string2706z00zz__osz00); 
BGl_symbol2707z00zz__osz00 = 
bstring_to_symbol(BGl_string2708z00zz__osz00); 
BGl_symbol2709z00zz__osz00 = 
bstring_to_symbol(BGl_string2710z00zz__osz00); 
BGl_symbol2718z00zz__osz00 = 
bstring_to_symbol(BGl_string2719z00zz__osz00); 
BGl_symbol2721z00zz__osz00 = 
bstring_to_symbol(BGl_string2722z00zz__osz00); 
BGl_symbol2724z00zz__osz00 = 
bstring_to_symbol(BGl_string2725z00zz__osz00); 
BGl_symbol2749z00zz__osz00 = 
bstring_to_symbol(BGl_string2750z00zz__osz00); 
BGl_symbol2754z00zz__osz00 = 
bstring_to_symbol(BGl_string2755z00zz__osz00); 
BGl_symbol2758z00zz__osz00 = 
bstring_to_symbol(BGl_string2759z00zz__osz00); 
BGl_symbol2760z00zz__osz00 = 
bstring_to_symbol(BGl_string2761z00zz__osz00); 
BGl_symbol2768z00zz__osz00 = 
bstring_to_symbol(BGl_string2769z00zz__osz00); 
BGl_symbol2789z00zz__osz00 = 
bstring_to_symbol(BGl_string2790z00zz__osz00); 
BGl_symbol2800z00zz__osz00 = 
bstring_to_symbol(BGl_string2801z00zz__osz00); 
BGl_symbol2802z00zz__osz00 = 
bstring_to_symbol(BGl_string2803z00zz__osz00); 
BGl_symbol2804z00zz__osz00 = 
bstring_to_symbol(BGl_string2805z00zz__osz00); 
BGl_symbol2806z00zz__osz00 = 
bstring_to_symbol(BGl_string2807z00zz__osz00); 
BGl_symbol2808z00zz__osz00 = 
bstring_to_symbol(BGl_string2809z00zz__osz00); 
BGl_symbol2812z00zz__osz00 = 
bstring_to_symbol(BGl_string2813z00zz__osz00); 
BGl_symbol2814z00zz__osz00 = 
bstring_to_symbol(BGl_string2815z00zz__osz00); 
BGl_symbol2816z00zz__osz00 = 
bstring_to_symbol(BGl_string2817z00zz__osz00); 
BGl_symbol2818z00zz__osz00 = 
bstring_to_symbol(BGl_string2819z00zz__osz00); 
BGl_symbol2820z00zz__osz00 = 
bstring_to_symbol(BGl_string2821z00zz__osz00); 
BGl_symbol2822z00zz__osz00 = 
bstring_to_symbol(BGl_string2823z00zz__osz00); 
BGl_symbol2824z00zz__osz00 = 
bstring_to_symbol(BGl_string2825z00zz__osz00); 
BGl_symbol2826z00zz__osz00 = 
bstring_to_symbol(BGl_string2827z00zz__osz00); 
BGl_symbol2828z00zz__osz00 = 
bstring_to_symbol(BGl_string2829z00zz__osz00); 
BGl_symbol2830z00zz__osz00 = 
bstring_to_symbol(BGl_string2831z00zz__osz00); 
BGl_symbol2832z00zz__osz00 = 
bstring_to_symbol(BGl_string2833z00zz__osz00); 
BGl_symbol2834z00zz__osz00 = 
bstring_to_symbol(BGl_string2835z00zz__osz00); 
BGl_symbol2836z00zz__osz00 = 
bstring_to_symbol(BGl_string2837z00zz__osz00); 
BGl_symbol2838z00zz__osz00 = 
bstring_to_symbol(BGl_string2839z00zz__osz00); 
BGl_symbol2840z00zz__osz00 = 
bstring_to_symbol(BGl_string2841z00zz__osz00); 
BGl_symbol2842z00zz__osz00 = 
bstring_to_symbol(BGl_string2843z00zz__osz00); 
BGl_symbol2844z00zz__osz00 = 
bstring_to_symbol(BGl_string2845z00zz__osz00); 
BGl_symbol2846z00zz__osz00 = 
bstring_to_symbol(BGl_string2847z00zz__osz00); 
BGl_symbol2848z00zz__osz00 = 
bstring_to_symbol(BGl_string2849z00zz__osz00); 
BGl_symbol2850z00zz__osz00 = 
bstring_to_symbol(BGl_string2851z00zz__osz00); 
BGl_symbol2855z00zz__osz00 = 
bstring_to_symbol(BGl_string2856z00zz__osz00); 
BGl_symbol2857z00zz__osz00 = 
bstring_to_symbol(BGl_string2858z00zz__osz00); 
BGl_symbol2859z00zz__osz00 = 
bstring_to_symbol(BGl_string2860z00zz__osz00); 
BGl_symbol2861z00zz__osz00 = 
bstring_to_symbol(BGl_string2862z00zz__osz00); 
BGl_symbol2863z00zz__osz00 = 
bstring_to_symbol(BGl_string2864z00zz__osz00); 
BGl_symbol2865z00zz__osz00 = 
bstring_to_symbol(BGl_string2866z00zz__osz00); 
BGl_symbol2867z00zz__osz00 = 
bstring_to_symbol(BGl_string2868z00zz__osz00); 
BGl_symbol2869z00zz__osz00 = 
bstring_to_symbol(BGl_string2870z00zz__osz00); 
BGl_symbol2874z00zz__osz00 = 
bstring_to_symbol(BGl_string2875z00zz__osz00); 
BGl_symbol2876z00zz__osz00 = 
bstring_to_symbol(BGl_string2877z00zz__osz00); 
BGl_symbol2878z00zz__osz00 = 
bstring_to_symbol(BGl_string2879z00zz__osz00); 
BGl_symbol2880z00zz__osz00 = 
bstring_to_symbol(BGl_string2881z00zz__osz00); 
BGl_symbol2882z00zz__osz00 = 
bstring_to_symbol(BGl_string2883z00zz__osz00); 
BGl_symbol2884z00zz__osz00 = 
bstring_to_symbol(BGl_string2885z00zz__osz00); 
BGl_symbol2886z00zz__osz00 = 
bstring_to_symbol(BGl_string2887z00zz__osz00); 
BGl_symbol2888z00zz__osz00 = 
bstring_to_symbol(BGl_string2889z00zz__osz00); 
BGl_symbol2890z00zz__osz00 = 
bstring_to_symbol(BGl_string2891z00zz__osz00); 
BGl_symbol2892z00zz__osz00 = 
bstring_to_symbol(BGl_string2893z00zz__osz00); 
BGl_symbol2894z00zz__osz00 = 
bstring_to_symbol(BGl_string2895z00zz__osz00); 
BGl_symbol2896z00zz__osz00 = 
bstring_to_symbol(BGl_string2897z00zz__osz00); 
BGl_symbol2898z00zz__osz00 = 
bstring_to_symbol(BGl_string2899z00zz__osz00); 
return ( 
BGl_symbol2900z00zz__osz00 = 
bstring_to_symbol(BGl_string2901z00zz__osz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__osz00(void)
{
{ /* Llib/os.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__osz00(void)
{
{ /* Llib/os.scm 18 */
BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00 = BGl_list2663z00zz__osz00; 
BGl_za2defaultzd2javazd2packageza2z00zz__osz00 = BGl_string2665z00zz__osz00; 
return ( 
BGl_ioctlzd2requestszd2tablez00zz__osz00 = BNIL, BUNSPEC) ;} 

}



/* append-21011 */
obj_t BGl_appendzd221011zd2zz__osz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
{
{ 
{ 
 obj_t BgL_headz00_1270;
BgL_headz00_1270 = 
MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2); 
{ 
 obj_t BgL_prevz00_1271; obj_t BgL_tailz00_1272;
BgL_prevz00_1271 = BgL_headz00_1270; 
BgL_tailz00_1272 = BgL_l1z00_1; 
BgL_loopz00_1273:
if(
PAIRP(BgL_tailz00_1272))
{ 
 obj_t BgL_newzd2prevzd2_1275;
BgL_newzd2prevzd2_1275 = 
MAKE_YOUNG_PAIR(
CAR(BgL_tailz00_1272), BgL_l2z00_2); 
SET_CDR(BgL_prevz00_1271, BgL_newzd2prevzd2_1275); 
{ 
 obj_t BgL_tailz00_4280; obj_t BgL_prevz00_4279;
BgL_prevz00_4279 = BgL_newzd2prevzd2_1275; 
BgL_tailz00_4280 = 
CDR(BgL_tailz00_1272); 
BgL_tailz00_1272 = BgL_tailz00_4280; 
BgL_prevz00_1271 = BgL_prevz00_4279; 
goto BgL_loopz00_1273;} }  else 
{ BNIL; } 
return 
CDR(BgL_headz00_1270);} } } 

}



/* default-executable-name */
BGL_EXPORTED_DEF obj_t BGl_defaultzd2executablezd2namez00zz__osz00(void)
{
{ /* Llib/os.scm 316 */
return 
string_to_bstring(BGL_DEFAULT_A_OUT);} 

}



/* &default-executable-name */
obj_t BGl_z62defaultzd2executablezd2namez62zz__osz00(obj_t BgL_envz00_3533)
{
{ /* Llib/os.scm 316 */
return 
BGl_defaultzd2executablezd2namez00zz__osz00();} 

}



/* default-script-name */
BGL_EXPORTED_DEF obj_t BGl_defaultzd2scriptzd2namez00zz__osz00(void)
{
{ /* Llib/os.scm 317 */
return 
string_to_bstring(BGL_DEFAULT_A_BAT);} 

}



/* &default-script-name */
obj_t BGl_z62defaultzd2scriptzd2namez62zz__osz00(obj_t BgL_envz00_3534)
{
{ /* Llib/os.scm 317 */
return 
BGl_defaultzd2scriptzd2namez00zz__osz00();} 

}



/* os-class */
BGL_EXPORTED_DEF obj_t BGl_oszd2classzd2zz__osz00(void)
{
{ /* Llib/os.scm 318 */
return 
string_to_bstring(OS_CLASS);} 

}



/* &os-class */
obj_t BGl_z62oszd2classzb0zz__osz00(obj_t BgL_envz00_3535)
{
{ /* Llib/os.scm 318 */
return 
BGl_oszd2classzd2zz__osz00();} 

}



/* os-name */
BGL_EXPORTED_DEF obj_t BGl_oszd2namezd2zz__osz00(void)
{
{ /* Llib/os.scm 319 */
return 
string_to_bstring(OS_NAME);} 

}



/* &os-name */
obj_t BGl_z62oszd2namezb0zz__osz00(obj_t BgL_envz00_3536)
{
{ /* Llib/os.scm 319 */
return 
BGl_oszd2namezd2zz__osz00();} 

}



/* os-arch */
BGL_EXPORTED_DEF obj_t BGl_oszd2archzd2zz__osz00(void)
{
{ /* Llib/os.scm 320 */
return 
string_to_bstring(OS_ARCH);} 

}



/* &os-arch */
obj_t BGl_z62oszd2archzb0zz__osz00(obj_t BgL_envz00_3537)
{
{ /* Llib/os.scm 320 */
return 
BGl_oszd2archzd2zz__osz00();} 

}



/* os-version */
BGL_EXPORTED_DEF obj_t BGl_oszd2versionzd2zz__osz00(void)
{
{ /* Llib/os.scm 321 */
return 
string_to_bstring(OS_VERSION);} 

}



/* &os-version */
obj_t BGl_z62oszd2versionzb0zz__osz00(obj_t BgL_envz00_3538)
{
{ /* Llib/os.scm 321 */
return 
BGl_oszd2versionzd2zz__osz00();} 

}



/* os-tmp */
BGL_EXPORTED_DEF obj_t BGl_oszd2tmpzd2zz__osz00(void)
{
{ /* Llib/os.scm 322 */
return 
string_to_bstring(OS_TMP);} 

}



/* &os-tmp */
obj_t BGl_z62oszd2tmpzb0zz__osz00(obj_t BgL_envz00_3539)
{
{ /* Llib/os.scm 322 */
return 
BGl_oszd2tmpzd2zz__osz00();} 

}



/* file-separator */
BGL_EXPORTED_DEF obj_t BGl_filezd2separatorzd2zz__osz00(void)
{
{ /* Llib/os.scm 323 */
return 
BCHAR(FILE_SEPARATOR);} 

}



/* &file-separator */
obj_t BGl_z62filezd2separatorzb0zz__osz00(obj_t BgL_envz00_3540)
{
{ /* Llib/os.scm 323 */
return 
BGl_filezd2separatorzd2zz__osz00();} 

}



/* path-separator */
BGL_EXPORTED_DEF obj_t BGl_pathzd2separatorzd2zz__osz00(void)
{
{ /* Llib/os.scm 324 */
return 
BCHAR(PATH_SEPARATOR);} 

}



/* &path-separator */
obj_t BGl_z62pathzd2separatorzb0zz__osz00(obj_t BgL_envz00_3541)
{
{ /* Llib/os.scm 324 */
return 
BGl_pathzd2separatorzd2zz__osz00();} 

}



/* static-library-suffix */
BGL_EXPORTED_DEF obj_t BGl_staticzd2libraryzd2suffixz00zz__osz00(void)
{
{ /* Llib/os.scm 325 */
return 
string_to_bstring(STATIC_LIB_SUFFIX);} 

}



/* &static-library-suffix */
obj_t BGl_z62staticzd2libraryzd2suffixz62zz__osz00(obj_t BgL_envz00_3542)
{
{ /* Llib/os.scm 325 */
return 
BGl_staticzd2libraryzd2suffixz00zz__osz00();} 

}



/* shared-library-suffix */
BGL_EXPORTED_DEF obj_t BGl_sharedzd2libraryzd2suffixz00zz__osz00(void)
{
{ /* Llib/os.scm 326 */
return 
string_to_bstring(SHARED_LIB_SUFFIX);} 

}



/* &shared-library-suffix */
obj_t BGl_z62sharedzd2libraryzd2suffixz62zz__osz00(obj_t BgL_envz00_3543)
{
{ /* Llib/os.scm 326 */
return 
BGl_sharedzd2libraryzd2suffixz00zz__osz00();} 

}



/* os-charset */
BGL_EXPORTED_DEF obj_t BGl_oszd2charsetzd2zz__osz00(void)
{
{ /* Llib/os.scm 331 */
{ /* Llib/os.scm 333 */
 obj_t BgL_g1039z00_1278;
BgL_g1039z00_1278 = 
BGl_getenvz00zz__osz00(BGl_string2666z00zz__osz00); 
if(
CBOOL(BgL_g1039z00_1278))
{ /* Llib/os.scm 333 */
return BgL_g1039z00_1278;}  else 
{ /* Llib/os.scm 334 */
 obj_t BgL_g1041z00_1281;
BgL_g1041z00_1281 = 
BGl_getenvz00zz__osz00(BGl_string2667z00zz__osz00); 
if(
CBOOL(BgL_g1041z00_1281))
{ /* Llib/os.scm 334 */
return BgL_g1041z00_1281;}  else 
{ /* Llib/os.scm 335 */
 obj_t BgL_g1043z00_1284;
BgL_g1043z00_1284 = 
BGl_getenvz00zz__osz00(BGl_string2668z00zz__osz00); 
if(
CBOOL(BgL_g1043z00_1284))
{ /* Llib/os.scm 335 */
return BgL_g1043z00_1284;}  else 
{ /* Llib/os.scm 335 */
return 
string_to_bstring(OS_CHARSET);} } } } } 

}



/* &os-charset */
obj_t BGl_z62oszd2charsetzb0zz__osz00(obj_t BgL_envz00_3544)
{
{ /* Llib/os.scm 331 */
return 
BGl_oszd2charsetzd2zz__osz00();} 

}



/* command-line */
BGL_EXPORTED_DEF obj_t BGl_commandzd2linezd2zz__osz00(void)
{
{ /* Llib/os.scm 341 */
return command_line;} 

}



/* &command-line */
obj_t BGl_z62commandzd2linezb0zz__osz00(obj_t BgL_envz00_3545)
{
{ /* Llib/os.scm 341 */
return 
BGl_commandzd2linezd2zz__osz00();} 

}



/* executable-name */
BGL_EXPORTED_DEF char * BGl_executablezd2namezd2zz__osz00(void)
{
{ /* Llib/os.scm 347 */
return executable_name;} 

}



/* &executable-name */
obj_t BGl_z62executablezd2namezb0zz__osz00(obj_t BgL_envz00_3546)
{
{ /* Llib/os.scm 347 */
return 
string_to_bstring(
BGl_executablezd2namezd2zz__osz00());} 

}



/* signal */
BGL_EXPORTED_DEF obj_t BGl_signalz00zz__osz00(int BgL_numz00_3, obj_t BgL_procz00_4)
{
{ /* Llib/os.scm 353 */
if(
(BgL_procz00_4==BGl_symbol2669z00zz__osz00))
{ /* Llib/os.scm 355 */
BGL_TAIL return 
bgl_signal(BgL_numz00_3, BTRUE);}  else 
{ /* Llib/os.scm 355 */
if(
(BgL_procz00_4==BGl_symbol2671z00zz__osz00))
{ /* Llib/os.scm 357 */
BGL_TAIL return 
bgl_signal(BgL_numz00_3, BFALSE);}  else 
{ /* Llib/os.scm 359 */
 bool_t BgL_test3166z00_4325;
{ /* Llib/os.scm 359 */
 int BgL_arg1252z00_1291;
{ /* Llib/os.scm 359 */
 obj_t BgL_procz00_2491;
if(
PROCEDUREP(BgL_procz00_4))
{ /* Llib/os.scm 359 */
BgL_procz00_2491 = BgL_procz00_4; }  else 
{ 
 obj_t BgL_auxz00_4328;
BgL_auxz00_4328 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(14323L), BGl_string2674z00zz__osz00, BGl_string2675z00zz__osz00, BgL_procz00_4); 
FAILURE(BgL_auxz00_4328,BFALSE,BFALSE);} 
BgL_arg1252z00_1291 = 
PROCEDURE_ARITY(BgL_procz00_2491); } 
BgL_test3166z00_4325 = 
(
(long)(BgL_arg1252z00_1291)==1L); } 
if(BgL_test3166z00_4325)
{ /* Llib/os.scm 359 */
if(
(
(long)(BgL_numz00_3)<0L))
{ /* Llib/os.scm 361 */
return BUNSPEC;}  else 
{ /* Llib/os.scm 361 */
if(
(
(long)(BgL_numz00_3)>31L))
{ /* Llib/os.scm 363 */
return 
BGl_errorz00zz__errorz00(BGl_string2674z00zz__osz00, BGl_string2676z00zz__osz00, 
BINT(BgL_numz00_3));}  else 
{ /* Llib/os.scm 363 */
BGL_TAIL return 
bgl_signal(BgL_numz00_3, BgL_procz00_4);} } }  else 
{ /* Llib/os.scm 359 */
return 
BGl_errorz00zz__errorz00(BGl_string2674z00zz__osz00, BGl_string2677z00zz__osz00, BgL_procz00_4);} } } } 

}



/* &signal */
obj_t BGl_z62signalz62zz__osz00(obj_t BgL_envz00_3547, obj_t BgL_numz00_3548, obj_t BgL_procz00_3549)
{
{ /* Llib/os.scm 353 */
{ /* Llib/os.scm 355 */
 int BgL_auxz00_4345;
{ /* Llib/os.scm 355 */
 obj_t BgL_tmpz00_4346;
if(
INTEGERP(BgL_numz00_3548))
{ /* Llib/os.scm 355 */
BgL_tmpz00_4346 = BgL_numz00_3548
; }  else 
{ 
 obj_t BgL_auxz00_4349;
BgL_auxz00_4349 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(14192L), BGl_string2678z00zz__osz00, BGl_string2679z00zz__osz00, BgL_numz00_3548); 
FAILURE(BgL_auxz00_4349,BFALSE,BFALSE);} 
BgL_auxz00_4345 = 
CINT(BgL_tmpz00_4346); } 
return 
BGl_signalz00zz__osz00(BgL_auxz00_4345, BgL_procz00_3549);} } 

}



/* get-signal-handler */
BGL_EXPORTED_DEF obj_t BGl_getzd2signalzd2handlerz00zz__osz00(int BgL_numz00_5)
{
{ /* Llib/os.scm 371 */
{ /* Llib/os.scm 372 */
 obj_t BgL_vz00_1292;
BgL_vz00_1292 = 
bgl_get_signal_handler(BgL_numz00_5); 
if(
(BgL_vz00_1292==BTRUE))
{ /* Llib/os.scm 374 */
return BGl_symbol2669z00zz__osz00;}  else 
{ /* Llib/os.scm 374 */
if(
(BgL_vz00_1292==BFALSE))
{ /* Llib/os.scm 375 */
return BGl_symbol2671z00zz__osz00;}  else 
{ /* Llib/os.scm 375 */
return BgL_vz00_1292;} } } } 

}



/* &get-signal-handler */
obj_t BGl_z62getzd2signalzd2handlerz62zz__osz00(obj_t BgL_envz00_3550, obj_t BgL_numz00_3551)
{
{ /* Llib/os.scm 371 */
{ /* Llib/os.scm 372 */
 int BgL_auxz00_4360;
{ /* Llib/os.scm 372 */
 obj_t BgL_tmpz00_4361;
if(
INTEGERP(BgL_numz00_3551))
{ /* Llib/os.scm 372 */
BgL_tmpz00_4361 = BgL_numz00_3551
; }  else 
{ 
 obj_t BgL_auxz00_4364;
BgL_auxz00_4364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(14797L), BGl_string2680z00zz__osz00, BGl_string2679z00zz__osz00, BgL_numz00_3551); 
FAILURE(BgL_auxz00_4364,BFALSE,BFALSE);} 
BgL_auxz00_4360 = 
CINT(BgL_tmpz00_4361); } 
return 
BGl_getzd2signalzd2handlerz00zz__osz00(BgL_auxz00_4360);} } 

}



/* sigsetmask */
BGL_EXPORTED_DEF int BGl_sigsetmaskz00zz__osz00(int BgL_nz00_6)
{
{ /* Llib/os.scm 381 */
return 
BGL_SIGSETMASK(BgL_nz00_6);} 

}



/* &sigsetmask */
obj_t BGl_z62sigsetmaskz62zz__osz00(obj_t BgL_envz00_3552, obj_t BgL_nz00_3553)
{
{ /* Llib/os.scm 381 */
{ /* Llib/os.scm 382 */
 int BgL_tmpz00_4371;
{ /* Llib/os.scm 382 */
 int BgL_auxz00_4372;
{ /* Llib/os.scm 382 */
 obj_t BgL_tmpz00_4373;
if(
INTEGERP(BgL_nz00_3553))
{ /* Llib/os.scm 382 */
BgL_tmpz00_4373 = BgL_nz00_3553
; }  else 
{ 
 obj_t BgL_auxz00_4376;
BgL_auxz00_4376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(15163L), BGl_string2681z00zz__osz00, BGl_string2679z00zz__osz00, BgL_nz00_3553); 
FAILURE(BgL_auxz00_4376,BFALSE,BFALSE);} 
BgL_auxz00_4372 = 
CINT(BgL_tmpz00_4373); } 
BgL_tmpz00_4371 = 
BGl_sigsetmaskz00zz__osz00(BgL_auxz00_4372); } 
return 
BINT(BgL_tmpz00_4371);} } 

}



/* _getenv */
obj_t BGl__getenvz00zz__osz00(obj_t BgL_env1142z00_9, obj_t BgL_opt1141z00_8)
{
{ /* Llib/os.scm 387 */
{ /* Llib/os.scm 387 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1141z00_8)) { case 0L : 

{ /* Llib/os.scm 387 */

return 
BGl_getenvz00zz__osz00(BFALSE);} break;case 1L : 

{ /* Llib/os.scm 387 */
 obj_t BgL_namez00_1296;
BgL_namez00_1296 = 
VECTOR_REF(BgL_opt1141z00_8,0L); 
{ /* Llib/os.scm 387 */

return 
BGl_getenvz00zz__osz00(BgL_namez00_1296);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2682z00zz__osz00, BGl_string2684z00zz__osz00, 
BINT(
VECTOR_LENGTH(BgL_opt1141z00_8)));} } } } 

}



/* getenv */
BGL_EXPORTED_DEF obj_t BGl_getenvz00zz__osz00(obj_t BgL_namez00_7)
{
{ /* Llib/os.scm 387 */
if(
STRINGP(BgL_namez00_7))
{ /* Llib/os.scm 388 */
{ /* Llib/os.scm 390 */
 bool_t BgL_test3176z00_4393;
{ /* Llib/os.scm 390 */
 bool_t BgL_test3177z00_4394;
{ /* Llib/os.scm 390 */
 obj_t BgL_string1z00_2496;
BgL_string1z00_2496 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 390 */
 long BgL_l1z00_2498;
BgL_l1z00_2498 = 
STRING_LENGTH(BgL_string1z00_2496); 
if(
(BgL_l1z00_2498==5L))
{ /* Llib/os.scm 390 */
 int BgL_arg1925z00_2501;
{ /* Llib/os.scm 390 */
 char * BgL_auxz00_4401; char * BgL_tmpz00_4399;
BgL_auxz00_4401 = 
BSTRING_TO_STRING(BGl_string2685z00zz__osz00); 
BgL_tmpz00_4399 = 
BSTRING_TO_STRING(BgL_string1z00_2496); 
BgL_arg1925z00_2501 = 
memcmp(BgL_tmpz00_4399, BgL_auxz00_4401, BgL_l1z00_2498); } 
BgL_test3177z00_4394 = 
(
(long)(BgL_arg1925z00_2501)==0L); }  else 
{ /* Llib/os.scm 390 */
BgL_test3177z00_4394 = ((bool_t)0)
; } } } 
if(BgL_test3177z00_4394)
{ /* Llib/os.scm 390 */
 obj_t BgL_string1z00_2507;
BgL_string1z00_2507 = BgL_namez00_7; 
{ /* Llib/os.scm 390 */
 long BgL_l1z00_2509;
BgL_l1z00_2509 = 
STRING_LENGTH(BgL_string1z00_2507); 
if(
(BgL_l1z00_2509==4L))
{ /* Llib/os.scm 390 */
 int BgL_arg1925z00_2512;
{ /* Llib/os.scm 390 */
 char * BgL_auxz00_4411; char * BgL_tmpz00_4409;
BgL_auxz00_4411 = 
BSTRING_TO_STRING(BGl_string2686z00zz__osz00); 
BgL_tmpz00_4409 = 
BSTRING_TO_STRING(BgL_string1z00_2507); 
BgL_arg1925z00_2512 = 
memcmp(BgL_tmpz00_4409, BgL_auxz00_4411, BgL_l1z00_2509); } 
BgL_test3176z00_4393 = 
(
(long)(BgL_arg1925z00_2512)==0L); }  else 
{ /* Llib/os.scm 390 */
BgL_test3176z00_4393 = ((bool_t)0)
; } } }  else 
{ /* Llib/os.scm 390 */
BgL_test3176z00_4393 = ((bool_t)0)
; } } 
if(BgL_test3176z00_4393)
{ /* Llib/os.scm 390 */
BgL_namez00_7 = BGl_string2687z00zz__osz00; }  else 
{ /* Llib/os.scm 390 */BFALSE; } } 
{ /* Llib/os.scm 392 */
 bool_t BgL_test3180z00_4416;
{ /* Llib/os.scm 392 */
 char * BgL_tmpz00_4417;
BgL_tmpz00_4417 = 
BSTRING_TO_STRING(BgL_namez00_7); 
BgL_test3180z00_4416 = 
(long)getenv(BgL_tmpz00_4417); } 
if(BgL_test3180z00_4416)
{ /* Llib/os.scm 393 */
 char * BgL_resultz00_1306;
{ /* Llib/os.scm 393 */
 char * BgL_tmpz00_4420;
BgL_tmpz00_4420 = 
BSTRING_TO_STRING(BgL_namez00_7); 
BgL_resultz00_1306 = 
(char *)getenv(BgL_tmpz00_4420); } 
if(
STRING_PTR_NULL(BgL_resultz00_1306))
{ /* Llib/os.scm 394 */
return BFALSE;}  else 
{ /* Llib/os.scm 394 */
return 
string_to_bstring(BgL_resultz00_1306);} }  else 
{ /* Llib/os.scm 392 */
return BFALSE;} } }  else 
{ /* Llib/os.scm 388 */
return 
bgl_getenv_all();} } 

}



/* putenv */
BGL_EXPORTED_DEF obj_t BGl_putenvz00zz__osz00(char * BgL_stringz00_10, char * BgL_valz00_11)
{
{ /* Llib/os.scm 403 */
{ /* Llib/os.scm 404 */
 bool_t BgL_test3182z00_4427;
{ /* Llib/os.scm 404 */
 bool_t BgL_test3183z00_4428;
{ /* Llib/os.scm 404 */
 obj_t BgL_string1z00_2519;
BgL_string1z00_2519 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 404 */
 long BgL_l1z00_2521;
BgL_l1z00_2521 = 
STRING_LENGTH(BgL_string1z00_2519); 
if(
(BgL_l1z00_2521==5L))
{ /* Llib/os.scm 404 */
 int BgL_arg1925z00_2524;
{ /* Llib/os.scm 404 */
 char * BgL_auxz00_4435; char * BgL_tmpz00_4433;
BgL_auxz00_4435 = 
BSTRING_TO_STRING(BGl_string2685z00zz__osz00); 
BgL_tmpz00_4433 = 
BSTRING_TO_STRING(BgL_string1z00_2519); 
BgL_arg1925z00_2524 = 
memcmp(BgL_tmpz00_4433, BgL_auxz00_4435, BgL_l1z00_2521); } 
BgL_test3183z00_4428 = 
(
(long)(BgL_arg1925z00_2524)==0L); }  else 
{ /* Llib/os.scm 404 */
BgL_test3183z00_4428 = ((bool_t)0)
; } } } 
if(BgL_test3183z00_4428)
{ /* Llib/os.scm 405 */
 obj_t BgL_string1z00_2530;
BgL_string1z00_2530 = 
string_to_bstring(BgL_stringz00_10); 
{ /* Llib/os.scm 405 */
 long BgL_l1z00_2532;
BgL_l1z00_2532 = 
STRING_LENGTH(BgL_string1z00_2530); 
if(
(BgL_l1z00_2532==4L))
{ /* Llib/os.scm 405 */
 int BgL_arg1925z00_2535;
{ /* Llib/os.scm 405 */
 char * BgL_auxz00_4446; char * BgL_tmpz00_4444;
BgL_auxz00_4446 = 
BSTRING_TO_STRING(BGl_string2686z00zz__osz00); 
BgL_tmpz00_4444 = 
BSTRING_TO_STRING(BgL_string1z00_2530); 
BgL_arg1925z00_2535 = 
memcmp(BgL_tmpz00_4444, BgL_auxz00_4446, BgL_l1z00_2532); } 
BgL_test3182z00_4427 = 
(
(long)(BgL_arg1925z00_2535)==0L); }  else 
{ /* Llib/os.scm 405 */
BgL_test3182z00_4427 = ((bool_t)0)
; } } }  else 
{ /* Llib/os.scm 404 */
BgL_test3182z00_4427 = ((bool_t)0)
; } } 
if(BgL_test3182z00_4427)
{ /* Llib/os.scm 404 */
BgL_stringz00_10 = 
BSTRING_TO_STRING(BGl_string2687z00zz__osz00); }  else 
{ /* Llib/os.scm 404 */BFALSE; } } 
{ /* Llib/os.scm 407 */
 int BgL_arg1312z00_1314;
BgL_arg1312z00_1314 = 
bgl_setenv(BgL_stringz00_10, BgL_valz00_11); 
return 
BBOOL(
(
(long)(BgL_arg1312z00_1314)==0L));} } 

}



/* &putenv */
obj_t BGl_z62putenvz62zz__osz00(obj_t BgL_envz00_3554, obj_t BgL_stringz00_3555, obj_t BgL_valz00_3556)
{
{ /* Llib/os.scm 403 */
{ /* Llib/os.scm 406 */
 char * BgL_auxz00_4465; char * BgL_auxz00_4456;
{ /* Llib/os.scm 406 */
 obj_t BgL_tmpz00_4466;
if(
STRINGP(BgL_valz00_3556))
{ /* Llib/os.scm 406 */
BgL_tmpz00_4466 = BgL_valz00_3556
; }  else 
{ 
 obj_t BgL_auxz00_4469;
BgL_auxz00_4469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16093L), BGl_string2688z00zz__osz00, BGl_string2689z00zz__osz00, BgL_valz00_3556); 
FAILURE(BgL_auxz00_4469,BFALSE,BFALSE);} 
BgL_auxz00_4465 = 
BSTRING_TO_STRING(BgL_tmpz00_4466); } 
{ /* Llib/os.scm 406 */
 obj_t BgL_tmpz00_4457;
if(
STRINGP(BgL_stringz00_3555))
{ /* Llib/os.scm 406 */
BgL_tmpz00_4457 = BgL_stringz00_3555
; }  else 
{ 
 obj_t BgL_auxz00_4460;
BgL_auxz00_4460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16093L), BGl_string2688z00zz__osz00, BGl_string2689z00zz__osz00, BgL_stringz00_3555); 
FAILURE(BgL_auxz00_4460,BFALSE,BFALSE);} 
BgL_auxz00_4456 = 
BSTRING_TO_STRING(BgL_tmpz00_4457); } 
return 
BGl_putenvz00zz__osz00(BgL_auxz00_4456, BgL_auxz00_4465);} } 

}



/* system */
BGL_EXPORTED_DEF obj_t BGl_systemz00zz__osz00(obj_t BgL_stringsz00_12)
{
{ /* Llib/os.scm 412 */
if(
NULLP(BgL_stringsz00_12))
{ /* Llib/os.scm 414 */
return BFALSE;}  else 
{ /* Llib/os.scm 416 */
 bool_t BgL_test3189z00_4477;
{ /* Llib/os.scm 416 */
 obj_t BgL_tmpz00_4478;
{ /* Llib/os.scm 416 */
 obj_t BgL_pairz00_2542;
if(
PAIRP(BgL_stringsz00_12))
{ /* Llib/os.scm 416 */
BgL_pairz00_2542 = BgL_stringsz00_12; }  else 
{ 
 obj_t BgL_auxz00_4481;
BgL_auxz00_4481 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16441L), BGl_string2690z00zz__osz00, BGl_string2691z00zz__osz00, BgL_stringsz00_12); 
FAILURE(BgL_auxz00_4481,BFALSE,BFALSE);} 
BgL_tmpz00_4478 = 
CDR(BgL_pairz00_2542); } 
BgL_test3189z00_4477 = 
NULLP(BgL_tmpz00_4478); } 
if(BgL_test3189z00_4477)
{ /* Llib/os.scm 417 */
 obj_t BgL_arg1316z00_1318;
{ /* Llib/os.scm 417 */
 obj_t BgL_pairz00_2543;
if(
PAIRP(BgL_stringsz00_12))
{ /* Llib/os.scm 417 */
BgL_pairz00_2543 = BgL_stringsz00_12; }  else 
{ 
 obj_t BgL_auxz00_4489;
BgL_auxz00_4489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16473L), BGl_string2690z00zz__osz00, BGl_string2691z00zz__osz00, BgL_stringsz00_12); 
FAILURE(BgL_auxz00_4489,BFALSE,BFALSE);} 
BgL_arg1316z00_1318 = 
CAR(BgL_pairz00_2543); } 
{ /* Llib/os.scm 417 */
 int BgL_tmpz00_4494;
{ /* Llib/os.scm 417 */
 char * BgL_tmpz00_4495;
{ /* Llib/os.scm 417 */
 obj_t BgL_tmpz00_4496;
if(
STRINGP(BgL_arg1316z00_1318))
{ /* Llib/os.scm 417 */
BgL_tmpz00_4496 = BgL_arg1316z00_1318
; }  else 
{ 
 obj_t BgL_auxz00_4499;
BgL_auxz00_4499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16480L), BGl_string2690z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1316z00_1318); 
FAILURE(BgL_auxz00_4499,BFALSE,BFALSE);} 
BgL_tmpz00_4495 = 
BSTRING_TO_STRING(BgL_tmpz00_4496); } 
BgL_tmpz00_4494 = 
system(BgL_tmpz00_4495); } 
return 
BINT(BgL_tmpz00_4494);} }  else 
{ /* Llib/os.scm 419 */
 obj_t BgL_arg1317z00_1319;
BgL_arg1317z00_1319 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_stringsz00_12); 
{ /* Llib/os.scm 419 */
 int BgL_tmpz00_4507;
{ /* Llib/os.scm 419 */
 char * BgL_tmpz00_4508;
BgL_tmpz00_4508 = 
BSTRING_TO_STRING(BgL_arg1317z00_1319); 
BgL_tmpz00_4507 = 
system(BgL_tmpz00_4508); } 
return 
BINT(BgL_tmpz00_4507);} } } } 

}



/* &system */
obj_t BGl_z62systemz62zz__osz00(obj_t BgL_envz00_3557, obj_t BgL_stringsz00_3558)
{
{ /* Llib/os.scm 412 */
return 
BGl_systemz00zz__osz00(BgL_stringsz00_3558);} 

}



/* system->string */
BGL_EXPORTED_DEF obj_t BGl_systemzd2ze3stringz31zz__osz00(obj_t BgL_stringsz00_13)
{
{ /* Llib/os.scm 424 */
{ /* Llib/os.scm 425 */
 obj_t BgL_pz00_1322;
{ /* Llib/os.scm 425 */
 obj_t BgL_arg1321z00_1328;
{ /* Llib/os.scm 425 */
 obj_t BgL_runner1324z00_1333;
{ /* Llib/os.scm 425 */
 obj_t BgL_list1323z00_1332;
BgL_list1323z00_1332 = 
MAKE_YOUNG_PAIR(BgL_stringsz00_13, BNIL); 
BgL_runner1324z00_1333 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BGl_string2692z00zz__osz00, BgL_list1323z00_1332); } 
BgL_arg1321z00_1328 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_runner1324z00_1333); } 
{ /* Ieee/port.scm 466 */

BgL_pz00_1322 = 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_arg1321z00_1328, BTRUE, 
BINT(5000000L)); } } 
{ /* Llib/os.scm 426 */
 obj_t BgL_exitd1046z00_1323;
BgL_exitd1046z00_1323 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/os.scm 428 */
 obj_t BgL_zc3z04anonymousza31320ze3z87_3559;
BgL_zc3z04anonymousza31320ze3z87_3559 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31320ze3ze5zz__osz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31320ze3z87_3559, 
(int)(0L), BgL_pz00_1322); 
{ /* Llib/os.scm 426 */
 obj_t BgL_arg2193z00_2545;
{ /* Llib/os.scm 426 */
 obj_t BgL_arg2194z00_2546;
BgL_arg2194z00_2546 = 
BGL_EXITD_PROTECT(BgL_exitd1046z00_1323); 
BgL_arg2193z00_2545 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31320ze3z87_3559, BgL_arg2194z00_2546); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_1323, BgL_arg2193z00_2545); BUNSPEC; } 
{ /* Llib/os.scm 427 */
 obj_t BgL_tmp1048z00_1325;
BgL_tmp1048z00_1325 = 
BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_pz00_1322); 
{ /* Llib/os.scm 426 */
 bool_t BgL_test3193z00_4528;
{ /* Llib/os.scm 426 */
 obj_t BgL_arg2192z00_2548;
BgL_arg2192z00_2548 = 
BGL_EXITD_PROTECT(BgL_exitd1046z00_1323); 
BgL_test3193z00_4528 = 
PAIRP(BgL_arg2192z00_2548); } 
if(BgL_test3193z00_4528)
{ /* Llib/os.scm 426 */
 obj_t BgL_arg2190z00_2549;
{ /* Llib/os.scm 426 */
 obj_t BgL_arg2191z00_2550;
BgL_arg2191z00_2550 = 
BGL_EXITD_PROTECT(BgL_exitd1046z00_1323); 
{ /* Llib/os.scm 426 */
 obj_t BgL_pairz00_2551;
if(
PAIRP(BgL_arg2191z00_2550))
{ /* Llib/os.scm 426 */
BgL_pairz00_2551 = BgL_arg2191z00_2550; }  else 
{ 
 obj_t BgL_auxz00_4534;
BgL_auxz00_4534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16881L), BGl_string2693z00zz__osz00, BGl_string2691z00zz__osz00, BgL_arg2191z00_2550); 
FAILURE(BgL_auxz00_4534,BFALSE,BFALSE);} 
BgL_arg2190z00_2549 = 
CDR(BgL_pairz00_2551); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_1323, BgL_arg2190z00_2549); BUNSPEC; }  else 
{ /* Llib/os.scm 426 */BFALSE; } } 
{ /* Llib/os.scm 428 */
 obj_t BgL_portz00_2552;
if(
INPUT_PORTP(BgL_pz00_1322))
{ /* Llib/os.scm 428 */
BgL_portz00_2552 = BgL_pz00_1322; }  else 
{ 
 obj_t BgL_auxz00_4542;
BgL_auxz00_4542 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16935L), BGl_string2693z00zz__osz00, BGl_string2694z00zz__osz00, BgL_pz00_1322); 
FAILURE(BgL_auxz00_4542,BFALSE,BFALSE);} 
bgl_close_input_port(BgL_portz00_2552); } 
return BgL_tmp1048z00_1325;} } } } } 

}



/* &system->string */
obj_t BGl_z62systemzd2ze3stringz53zz__osz00(obj_t BgL_envz00_3560, obj_t BgL_stringsz00_3561)
{
{ /* Llib/os.scm 424 */
return 
BGl_systemzd2ze3stringz31zz__osz00(BgL_stringsz00_3561);} 

}



/* &<@anonymous:1320> */
obj_t BGl_z62zc3z04anonymousza31320ze3ze5zz__osz00(obj_t BgL_envz00_3562)
{
{ /* Llib/os.scm 426 */
{ /* Llib/os.scm 428 */
 obj_t BgL_pz00_3563;
BgL_pz00_3563 = 
PROCEDURE_REF(BgL_envz00_3562, 
(int)(0L)); 
{ /* Llib/os.scm 428 */
 obj_t BgL_portz00_4195;
if(
INPUT_PORTP(BgL_pz00_3563))
{ /* Llib/os.scm 428 */
BgL_portz00_4195 = BgL_pz00_3563; }  else 
{ 
 obj_t BgL_auxz00_4552;
BgL_auxz00_4552 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(16935L), BGl_string2695z00zz__osz00, BGl_string2694z00zz__osz00, BgL_pz00_3563); 
FAILURE(BgL_auxz00_4552,BFALSE,BFALSE);} 
return 
bgl_close_input_port(BgL_portz00_4195);} } } 

}



/* date */
BGL_EXPORTED_DEF char * BGl_datez00zz__osz00(void)
{
{ /* Llib/os.scm 433 */
{ /* Llib/os.scm 434 */
 char * BgL_dtz00_1334;
BgL_dtz00_1334 = 
c_date(); 
{ /* Llib/os.scm 434 */
 long BgL_lenz00_1335;
BgL_lenz00_1335 = 
STRING_LENGTH(
string_to_bstring(BgL_dtz00_1334)); 
{ /* Llib/os.scm 435 */

{ /* Llib/os.scm 436 */
 bool_t BgL_test3197z00_4560;
{ /* Llib/os.scm 436 */
 unsigned char BgL_tmpz00_4561;
{ /* Llib/os.scm 436 */
 obj_t BgL_s2199z00_3669; long BgL_i2200z00_3670;
BgL_s2199z00_3669 = 
string_to_bstring(BgL_dtz00_1334); 
BgL_i2200z00_3670 = 
(BgL_lenz00_1335-1L); 
{ /* Llib/os.scm 436 */
 long BgL_l2201z00_3671;
BgL_l2201z00_3671 = 
STRING_LENGTH(BgL_s2199z00_3669); 
if(
BOUND_CHECK(BgL_i2200z00_3670, BgL_l2201z00_3671))
{ /* Llib/os.scm 436 */
BgL_tmpz00_4561 = 
STRING_REF(BgL_s2199z00_3669, BgL_i2200z00_3670)
; }  else 
{ 
 obj_t BgL_auxz00_4568;
BgL_auxz00_4568 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(17250L), BGl_string2696z00zz__osz00, BgL_s2199z00_3669, 
(int)(BgL_l2201z00_3671), 
(int)(BgL_i2200z00_3670)); 
FAILURE(BgL_auxz00_4568,BFALSE,BFALSE);} } } 
BgL_test3197z00_4560 = 
(BgL_tmpz00_4561==((unsigned char)10)); } 
if(BgL_test3197z00_4560)
{ /* Llib/os.scm 437 */
 long BgL_arg1328z00_1339;
BgL_arg1328z00_1339 = 
(BgL_lenz00_1335-1L); 
return 
BSTRING_TO_STRING(
BGl_substringz00zz__r4_strings_6_7z00(
string_to_bstring(BgL_dtz00_1334), 0L, BgL_arg1328z00_1339));}  else 
{ /* Llib/os.scm 436 */
return BgL_dtz00_1334;} } } } } } 

}



/* &date */
obj_t BGl_z62datez62zz__osz00(obj_t BgL_envz00_3564)
{
{ /* Llib/os.scm 433 */
return 
string_to_bstring(
BGl_datez00zz__osz00());} 

}



/* chdir */
BGL_EXPORTED_DEF bool_t BGl_chdirz00zz__osz00(char * BgL_dirnamez00_14)
{
{ /* Llib/os.scm 443 */
if(
chdir(BgL_dirnamez00_14))
{ /* Llib/os.scm 444 */
return ((bool_t)0);}  else 
{ /* Llib/os.scm 444 */
return ((bool_t)1);} } 

}



/* &chdir */
obj_t BGl_z62chdirz62zz__osz00(obj_t BgL_envz00_3565, obj_t BgL_dirnamez00_3566)
{
{ /* Llib/os.scm 443 */
{ /* Llib/os.scm 444 */
 bool_t BgL_tmpz00_4583;
{ /* Llib/os.scm 444 */
 char * BgL_auxz00_4584;
{ /* Llib/os.scm 444 */
 obj_t BgL_tmpz00_4585;
if(
STRINGP(BgL_dirnamez00_3566))
{ /* Llib/os.scm 444 */
BgL_tmpz00_4585 = BgL_dirnamez00_3566
; }  else 
{ 
 obj_t BgL_auxz00_4588;
BgL_auxz00_4588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(17587L), BGl_string2697z00zz__osz00, BGl_string2689z00zz__osz00, BgL_dirnamez00_3566); 
FAILURE(BgL_auxz00_4588,BFALSE,BFALSE);} 
BgL_auxz00_4584 = 
BSTRING_TO_STRING(BgL_tmpz00_4585); } 
BgL_tmpz00_4583 = 
BGl_chdirz00zz__osz00(BgL_auxz00_4584); } 
return 
BBOOL(BgL_tmpz00_4583);} } 

}



/* pwd */
BGL_EXPORTED_DEF obj_t BGl_pwdz00zz__osz00(void)
{
{ /* Llib/os.scm 449 */
{ /* Llib/os.scm 450 */
 obj_t BgL_stringz00_1343;
{ /* Ieee/string.scm 172 */

BgL_stringz00_1343 = 
make_string(1024L, ((unsigned char)' ')); } 
{ /* Llib/os.scm 451 */
 char * BgL_tmpz00_4596;
{ /* Llib/os.scm 451 */
 int BgL_auxz00_4599; char * BgL_tmpz00_4597;
BgL_auxz00_4599 = 
(int)(1024L); 
BgL_tmpz00_4597 = 
BSTRING_TO_STRING(BgL_stringz00_1343); 
BgL_tmpz00_4596 = 
(char *)(long)getcwd(BgL_tmpz00_4597, BgL_auxz00_4599); } 
return 
string_to_bstring(BgL_tmpz00_4596);} } } 

}



/* &pwd */
obj_t BGl_z62pwdz62zz__osz00(obj_t BgL_envz00_3567)
{
{ /* Llib/os.scm 449 */
return 
BGl_pwdz00zz__osz00();} 

}



/* basename */
BGL_EXPORTED_DEF obj_t BGl_basenamez00zz__osz00(obj_t BgL_stringz00_15)
{
{ /* Llib/os.scm 456 */
{ /* Llib/os.scm 457 */
 bool_t BgL_test3201z00_4604;
{ /* Llib/os.scm 457 */
 obj_t BgL_string1z00_2564;
BgL_string1z00_2564 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 457 */
 long BgL_l1z00_2566;
BgL_l1z00_2566 = 
STRING_LENGTH(BgL_string1z00_2564); 
if(
(BgL_l1z00_2566==5L))
{ /* Llib/os.scm 457 */
 int BgL_arg1925z00_2569;
{ /* Llib/os.scm 457 */
 char * BgL_auxz00_4611; char * BgL_tmpz00_4609;
BgL_auxz00_4611 = 
BSTRING_TO_STRING(BGl_string2698z00zz__osz00); 
BgL_tmpz00_4609 = 
BSTRING_TO_STRING(BgL_string1z00_2564); 
BgL_arg1925z00_2569 = 
memcmp(BgL_tmpz00_4609, BgL_auxz00_4611, BgL_l1z00_2566); } 
BgL_test3201z00_4604 = 
(
(long)(BgL_arg1925z00_2569)==0L); }  else 
{ /* Llib/os.scm 457 */
BgL_test3201z00_4604 = ((bool_t)0)
; } } } 
if(BgL_test3201z00_4604)
{ /* Llib/os.scm 457 */
BGL_TAIL return 
BGl_mingwzd2basenamezd2zz__osz00(BgL_stringz00_15);}  else 
{ /* Llib/os.scm 457 */
BGL_TAIL return 
BGl_defaultzd2basenamezd2zz__osz00(BgL_stringz00_15);} } } 

}



/* &basename */
obj_t BGl_z62basenamez62zz__osz00(obj_t BgL_envz00_3568, obj_t BgL_stringz00_3569)
{
{ /* Llib/os.scm 456 */
{ /* Llib/os.scm 457 */
 obj_t BgL_auxz00_4618;
if(
STRINGP(BgL_stringz00_3569))
{ /* Llib/os.scm 457 */
BgL_auxz00_4618 = BgL_stringz00_3569
; }  else 
{ 
 obj_t BgL_auxz00_4621;
BgL_auxz00_4621 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(18178L), BGl_string2699z00zz__osz00, BGl_string2689z00zz__osz00, BgL_stringz00_3569); 
FAILURE(BgL_auxz00_4621,BFALSE,BFALSE);} 
return 
BGl_basenamez00zz__osz00(BgL_auxz00_4618);} } 

}



/* mingw-basename */
obj_t BGl_mingwzd2basenamezd2zz__osz00(obj_t BgL_stringz00_16)
{
{ /* Llib/os.scm 464 */
{ /* Llib/os.scm 465 */
 long BgL_nz00_1349; bool_t BgL_stopz00_1350;
BgL_nz00_1349 = 
STRING_LENGTH(BgL_stringz00_16); 
BgL_stopz00_1350 = ((bool_t)0); 
{ 
 long BgL_iz00_1353;
BgL_iz00_1353 = 
(BgL_nz00_1349-1L); 
BgL_zc3z04anonymousza31337ze3z87_1354:
if(
(
BBOOL(BgL_stopz00_1350)==BTRUE))
{ /* Llib/os.scm 467 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_16, 
(BgL_iz00_1353+2L), BgL_nz00_1349);}  else 
{ /* Llib/os.scm 467 */
if(
(BgL_iz00_1353<0L))
{ /* Llib/os.scm 470 */
BgL_stopz00_1350 = ((bool_t)1); }  else 
{ /* Llib/os.scm 472 */
 bool_t BgL__ortest_1051z00_1357;
{ /* Llib/os.scm 472 */
 unsigned char BgL_tmpz00_4634;
{ /* Llib/os.scm 472 */
 long BgL_l2205z00_3675;
BgL_l2205z00_3675 = 
STRING_LENGTH(BgL_stringz00_16); 
if(
BOUND_CHECK(BgL_iz00_1353, BgL_l2205z00_3675))
{ /* Llib/os.scm 472 */
BgL_tmpz00_4634 = 
STRING_REF(BgL_stringz00_16, BgL_iz00_1353)
; }  else 
{ 
 obj_t BgL_auxz00_4639;
BgL_auxz00_4639 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(18723L), BGl_string2696z00zz__osz00, BgL_stringz00_16, 
(int)(BgL_l2205z00_3675), 
(int)(BgL_iz00_1353)); 
FAILURE(BgL_auxz00_4639,BFALSE,BFALSE);} } 
BgL__ortest_1051z00_1357 = 
(BgL_tmpz00_4634==((unsigned char)'\\')); } 
if(BgL__ortest_1051z00_1357)
{ /* Llib/os.scm 472 */
BgL_stopz00_1350 = BgL__ortest_1051z00_1357; }  else 
{ /* Llib/os.scm 473 */
 unsigned char BgL_tmpz00_4647;
{ /* Llib/os.scm 473 */
 long BgL_l2209z00_3679;
BgL_l2209z00_3679 = 
STRING_LENGTH(BgL_stringz00_16); 
if(
BOUND_CHECK(BgL_iz00_1353, BgL_l2209z00_3679))
{ /* Llib/os.scm 473 */
BgL_tmpz00_4647 = 
STRING_REF(BgL_stringz00_16, BgL_iz00_1353)
; }  else 
{ 
 obj_t BgL_auxz00_4652;
BgL_auxz00_4652 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(18766L), BGl_string2696z00zz__osz00, BgL_stringz00_16, 
(int)(BgL_l2209z00_3679), 
(int)(BgL_iz00_1353)); 
FAILURE(BgL_auxz00_4652,BFALSE,BFALSE);} } 
BgL_stopz00_1350 = 
(BgL_tmpz00_4647==((unsigned char)'/')); } } 
{ 
 long BgL_iz00_4659;
BgL_iz00_4659 = 
(BgL_iz00_1353-1L); 
BgL_iz00_1353 = BgL_iz00_4659; 
goto BgL_zc3z04anonymousza31337ze3z87_1354;} } } } } 

}



/* default-basename */
obj_t BGl_defaultzd2basenamezd2zz__osz00(obj_t BgL_stringz00_17)
{
{ /* Llib/os.scm 478 */
{ /* Llib/os.scm 479 */
 long BgL_lenz00_1362;
BgL_lenz00_1362 = 
(
STRING_LENGTH(BgL_stringz00_17)-1L); 
{ /* Llib/os.scm 479 */
 long BgL_startz00_1363;
{ /* Llib/os.scm 480 */
 bool_t BgL_test3209z00_4664;
if(
(BgL_lenz00_1362>0L))
{ /* Llib/os.scm 481 */
 unsigned char BgL_char2z00_2594;
BgL_char2z00_2594 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 481 */
 unsigned char BgL_tmpz00_4668;
{ /* Llib/os.scm 481 */
 long BgL_l2213z00_3683;
BgL_l2213z00_3683 = 
STRING_LENGTH(BgL_stringz00_17); 
if(
BOUND_CHECK(BgL_lenz00_1362, BgL_l2213z00_3683))
{ /* Llib/os.scm 481 */
BgL_tmpz00_4668 = 
STRING_REF(BgL_stringz00_17, BgL_lenz00_1362)
; }  else 
{ 
 obj_t BgL_auxz00_4673;
BgL_auxz00_4673 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(19149L), BGl_string2696z00zz__osz00, BgL_stringz00_17, 
(int)(BgL_l2213z00_3683), 
(int)(BgL_lenz00_1362)); 
FAILURE(BgL_auxz00_4673,BFALSE,BFALSE);} } 
BgL_test3209z00_4664 = 
(BgL_tmpz00_4668==BgL_char2z00_2594); } }  else 
{ /* Llib/os.scm 480 */
BgL_test3209z00_4664 = ((bool_t)0)
; } 
if(BgL_test3209z00_4664)
{ /* Llib/os.scm 480 */
BgL_startz00_1363 = 
(BgL_lenz00_1362-1L); }  else 
{ /* Llib/os.scm 480 */
BgL_startz00_1363 = BgL_lenz00_1362; } } 
{ /* Llib/os.scm 480 */

{ 
 long BgL_indexz00_1365;
BgL_indexz00_1365 = BgL_startz00_1363; 
BgL_zc3z04anonymousza31343ze3z87_1366:
if(
(BgL_indexz00_1365==-1L))
{ /* Llib/os.scm 487 */
return BgL_stringz00_17;}  else 
{ /* Llib/os.scm 489 */
 bool_t BgL_test3213z00_4683;
{ /* Llib/os.scm 489 */
 unsigned char BgL_char2z00_2600;
BgL_char2z00_2600 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 489 */
 unsigned char BgL_tmpz00_4685;
{ /* Llib/os.scm 489 */
 long BgL_l2217z00_3687;
BgL_l2217z00_3687 = 
STRING_LENGTH(BgL_stringz00_17); 
if(
BOUND_CHECK(BgL_indexz00_1365, BgL_l2217z00_3687))
{ /* Llib/os.scm 489 */
BgL_tmpz00_4685 = 
STRING_REF(BgL_stringz00_17, BgL_indexz00_1365)
; }  else 
{ 
 obj_t BgL_auxz00_4690;
BgL_auxz00_4690 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(19326L), BGl_string2696z00zz__osz00, BgL_stringz00_17, 
(int)(BgL_l2217z00_3687), 
(int)(BgL_indexz00_1365)); 
FAILURE(BgL_auxz00_4690,BFALSE,BFALSE);} } 
BgL_test3213z00_4683 = 
(BgL_tmpz00_4685==BgL_char2z00_2600); } } 
if(BgL_test3213z00_4683)
{ /* Llib/os.scm 489 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_17, 
(BgL_indexz00_1365+1L), 
(BgL_startz00_1363+1L));}  else 
{ 
 long BgL_indexz00_4700;
BgL_indexz00_4700 = 
(BgL_indexz00_1365-1L); 
BgL_indexz00_1365 = BgL_indexz00_4700; 
goto BgL_zc3z04anonymousza31343ze3z87_1366;} } } } } } } 

}



/* prefix */
BGL_EXPORTED_DEF obj_t BGl_prefixz00zz__osz00(obj_t BgL_stringz00_18)
{
{ /* Llib/os.scm 497 */
{ /* Llib/os.scm 498 */
 long BgL_lenz00_1381;
BgL_lenz00_1381 = 
(
STRING_LENGTH(BgL_stringz00_18)-1L); 
{ 
 long BgL_ez00_1383; long BgL_sz00_1384;
BgL_ez00_1383 = BgL_lenz00_1381; 
BgL_sz00_1384 = BgL_lenz00_1381; 
BgL_zc3z04anonymousza31358ze3z87_1385:
if(
(BgL_sz00_1384<=0L))
{ /* Llib/os.scm 502 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_18, 0L, 
(1L+BgL_ez00_1383));}  else 
{ /* Llib/os.scm 505 */
 bool_t BgL_test3216z00_4708;
{ /* Llib/os.scm 505 */
 bool_t BgL_test3217z00_4709;
{ /* Llib/os.scm 505 */
 obj_t BgL_tmpz00_4710;
{ /* Llib/os.scm 505 */
 long BgL_l2221z00_3691;
BgL_l2221z00_3691 = 
STRING_LENGTH(BgL_stringz00_18); 
if(
BOUND_CHECK(BgL_sz00_1384, BgL_l2221z00_3691))
{ /* Llib/os.scm 505 */
BgL_tmpz00_4710 = 
BCHAR(
STRING_REF(BgL_stringz00_18, BgL_sz00_1384))
; }  else 
{ 
 obj_t BgL_auxz00_4716;
BgL_auxz00_4716 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(19947L), BGl_string2696z00zz__osz00, BgL_stringz00_18, 
(int)(BgL_l2221z00_3691), 
(int)(BgL_sz00_1384)); 
FAILURE(BgL_auxz00_4716,BFALSE,BFALSE);} } 
BgL_test3217z00_4709 = 
(BgL_tmpz00_4710==
BCHAR(((unsigned char)'.'))); } 
if(BgL_test3217z00_4709)
{ /* Llib/os.scm 505 */
BgL_test3216z00_4708 = 
(BgL_ez00_1383==BgL_lenz00_1381)
; }  else 
{ /* Llib/os.scm 505 */
BgL_test3216z00_4708 = ((bool_t)0)
; } } 
if(BgL_test3216z00_4708)
{ 
 long BgL_sz00_4727; long BgL_ez00_4725;
BgL_ez00_4725 = 
(BgL_sz00_1384-1L); 
BgL_sz00_4727 = 
(BgL_sz00_1384-1L); 
BgL_sz00_1384 = BgL_sz00_4727; 
BgL_ez00_1383 = BgL_ez00_4725; 
goto BgL_zc3z04anonymousza31358ze3z87_1385;}  else 
{ 
 long BgL_sz00_4729;
BgL_sz00_4729 = 
(BgL_sz00_1384-1L); 
BgL_sz00_1384 = BgL_sz00_4729; 
goto BgL_zc3z04anonymousza31358ze3z87_1385;} } } } } 

}



/* &prefix */
obj_t BGl_z62prefixz62zz__osz00(obj_t BgL_envz00_3570, obj_t BgL_stringz00_3571)
{
{ /* Llib/os.scm 497 */
{ /* Llib/os.scm 498 */
 obj_t BgL_auxz00_4731;
if(
STRINGP(BgL_stringz00_3571))
{ /* Llib/os.scm 498 */
BgL_auxz00_4731 = BgL_stringz00_3571
; }  else 
{ 
 obj_t BgL_auxz00_4734;
BgL_auxz00_4734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(19723L), BGl_string2700z00zz__osz00, BGl_string2689z00zz__osz00, BgL_stringz00_3571); 
FAILURE(BgL_auxz00_4734,BFALSE,BFALSE);} 
return 
BGl_prefixz00zz__osz00(BgL_auxz00_4731);} } 

}



/* dirname */
BGL_EXPORTED_DEF obj_t BGl_dirnamez00zz__osz00(obj_t BgL_stringz00_19)
{
{ /* Llib/os.scm 513 */
{ /* Llib/os.scm 514 */
 bool_t BgL_test3220z00_4739;
{ /* Llib/os.scm 514 */
 obj_t BgL_string1z00_2617;
BgL_string1z00_2617 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 514 */
 long BgL_l1z00_2619;
BgL_l1z00_2619 = 
STRING_LENGTH(BgL_string1z00_2617); 
if(
(BgL_l1z00_2619==5L))
{ /* Llib/os.scm 514 */
 int BgL_arg1925z00_2622;
{ /* Llib/os.scm 514 */
 char * BgL_auxz00_4746; char * BgL_tmpz00_4744;
BgL_auxz00_4746 = 
BSTRING_TO_STRING(BGl_string2698z00zz__osz00); 
BgL_tmpz00_4744 = 
BSTRING_TO_STRING(BgL_string1z00_2617); 
BgL_arg1925z00_2622 = 
memcmp(BgL_tmpz00_4744, BgL_auxz00_4746, BgL_l1z00_2619); } 
BgL_test3220z00_4739 = 
(
(long)(BgL_arg1925z00_2622)==0L); }  else 
{ /* Llib/os.scm 514 */
BgL_test3220z00_4739 = ((bool_t)0)
; } } } 
if(BgL_test3220z00_4739)
{ /* Llib/os.scm 514 */
BGL_TAIL return 
BGl_mingwzd2dirnamezd2zz__osz00(BgL_stringz00_19);}  else 
{ /* Llib/os.scm 514 */
BGL_TAIL return 
BGl_defaultzd2dirnamezd2zz__osz00(BgL_stringz00_19);} } } 

}



/* &dirname */
obj_t BGl_z62dirnamez62zz__osz00(obj_t BgL_envz00_3572, obj_t BgL_stringz00_3573)
{
{ /* Llib/os.scm 513 */
{ /* Llib/os.scm 514 */
 obj_t BgL_auxz00_4753;
if(
STRINGP(BgL_stringz00_3573))
{ /* Llib/os.scm 514 */
BgL_auxz00_4753 = BgL_stringz00_3573
; }  else 
{ 
 obj_t BgL_auxz00_4756;
BgL_auxz00_4756 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(20345L), BGl_string2701z00zz__osz00, BGl_string2689z00zz__osz00, BgL_stringz00_3573); 
FAILURE(BgL_auxz00_4756,BFALSE,BFALSE);} 
return 
BGl_dirnamez00zz__osz00(BgL_auxz00_4753);} } 

}



/* mingw-dirname */
obj_t BGl_mingwzd2dirnamezd2zz__osz00(obj_t BgL_stringz00_20)
{
{ /* Llib/os.scm 521 */
{ /* Llib/os.scm 522 */
 long BgL_nz00_1402; bool_t BgL_stopz00_1403;
BgL_nz00_1402 = 
STRING_LENGTH(BgL_stringz00_20); 
BgL_stopz00_1403 = ((bool_t)0); 
{ 
 long BgL_iz00_1406;
BgL_iz00_1406 = 
(BgL_nz00_1402-1L); 
BgL_zc3z04anonymousza31374ze3z87_1407:
if(
(
BBOOL(BgL_stopz00_1403)==BTRUE))
{ /* Llib/os.scm 524 */
if(
(BgL_iz00_1406<0L))
{ /* Llib/os.scm 526 */
return BGl_string2664z00zz__osz00;}  else 
{ /* Llib/os.scm 526 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_20, 0L, 
(BgL_iz00_1406+1L));} }  else 
{ /* Llib/os.scm 524 */
if(
(BgL_iz00_1406<0L))
{ /* Llib/os.scm 529 */
BgL_stopz00_1403 = ((bool_t)1); }  else 
{ /* Llib/os.scm 531 */
 bool_t BgL__ortest_1053z00_1411;
{ /* Llib/os.scm 531 */
 unsigned char BgL_tmpz00_4771;
{ /* Llib/os.scm 531 */
 long BgL_l2225z00_3695;
BgL_l2225z00_3695 = 
STRING_LENGTH(BgL_stringz00_20); 
if(
BOUND_CHECK(BgL_iz00_1406, BgL_l2225z00_3695))
{ /* Llib/os.scm 531 */
BgL_tmpz00_4771 = 
STRING_REF(BgL_stringz00_20, BgL_iz00_1406)
; }  else 
{ 
 obj_t BgL_auxz00_4776;
BgL_auxz00_4776 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(20921L), BGl_string2696z00zz__osz00, BgL_stringz00_20, 
(int)(BgL_l2225z00_3695), 
(int)(BgL_iz00_1406)); 
FAILURE(BgL_auxz00_4776,BFALSE,BFALSE);} } 
BgL__ortest_1053z00_1411 = 
(BgL_tmpz00_4771==((unsigned char)'\\')); } 
if(BgL__ortest_1053z00_1411)
{ /* Llib/os.scm 531 */
BgL_stopz00_1403 = BgL__ortest_1053z00_1411; }  else 
{ /* Llib/os.scm 532 */
 unsigned char BgL_tmpz00_4784;
{ /* Llib/os.scm 532 */
 long BgL_l2229z00_3699;
BgL_l2229z00_3699 = 
STRING_LENGTH(BgL_stringz00_20); 
if(
BOUND_CHECK(BgL_iz00_1406, BgL_l2229z00_3699))
{ /* Llib/os.scm 532 */
BgL_tmpz00_4784 = 
STRING_REF(BgL_stringz00_20, BgL_iz00_1406)
; }  else 
{ 
 obj_t BgL_auxz00_4789;
BgL_auxz00_4789 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(20964L), BGl_string2696z00zz__osz00, BgL_stringz00_20, 
(int)(BgL_l2229z00_3699), 
(int)(BgL_iz00_1406)); 
FAILURE(BgL_auxz00_4789,BFALSE,BFALSE);} } 
BgL_stopz00_1403 = 
(BgL_tmpz00_4784==((unsigned char)'/')); } } 
{ 
 long BgL_iz00_4796;
BgL_iz00_4796 = 
(BgL_iz00_1406-1L); 
BgL_iz00_1406 = BgL_iz00_4796; 
goto BgL_zc3z04anonymousza31374ze3z87_1407;} } } } } 

}



/* default-dirname */
obj_t BGl_defaultzd2dirnamezd2zz__osz00(obj_t BgL_stringz00_21)
{
{ /* Llib/os.scm 537 */
{ /* Llib/os.scm 538 */
 long BgL_lenz00_1416;
BgL_lenz00_1416 = 
(
STRING_LENGTH(BgL_stringz00_21)-1L); 
if(
(BgL_lenz00_1416==-1L))
{ /* Llib/os.scm 539 */
return BGl_string2664z00zz__osz00;}  else 
{ 
 long BgL_readz00_1419;
BgL_readz00_1419 = BgL_lenz00_1416; 
BgL_zc3z04anonymousza31382ze3z87_1420:
if(
(BgL_readz00_1419==0L))
{ /* Llib/os.scm 544 */
 bool_t BgL_test3231z00_4805;
{ /* Llib/os.scm 544 */
 unsigned char BgL_char2z00_2649;
BgL_char2z00_2649 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 544 */
 unsigned char BgL_tmpz00_4807;
{ /* Llib/os.scm 544 */
 long BgL_l2233z00_3703;
BgL_l2233z00_3703 = 
STRING_LENGTH(BgL_stringz00_21); 
if(
BOUND_CHECK(BgL_readz00_1419, BgL_l2233z00_3703))
{ /* Llib/os.scm 544 */
BgL_tmpz00_4807 = 
STRING_REF(BgL_stringz00_21, BgL_readz00_1419)
; }  else 
{ 
 obj_t BgL_auxz00_4812;
BgL_auxz00_4812 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(21399L), BGl_string2696z00zz__osz00, BgL_stringz00_21, 
(int)(BgL_l2233z00_3703), 
(int)(BgL_readz00_1419)); 
FAILURE(BgL_auxz00_4812,BFALSE,BFALSE);} } 
BgL_test3231z00_4805 = 
(BgL_tmpz00_4807==BgL_char2z00_2649); } } 
if(BgL_test3231z00_4805)
{ /* Llib/os.scm 544 */
return 
make_string(1L, 
(unsigned char)(FILE_SEPARATOR));}  else 
{ /* Llib/os.scm 544 */
return BGl_string2664z00zz__osz00;} }  else 
{ /* Llib/os.scm 547 */
 bool_t BgL_test3233z00_4821;
{ /* Llib/os.scm 547 */
 unsigned char BgL_char2z00_2653;
BgL_char2z00_2653 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 547 */
 unsigned char BgL_tmpz00_4823;
{ /* Llib/os.scm 547 */
 long BgL_l2237z00_3707;
BgL_l2237z00_3707 = 
STRING_LENGTH(BgL_stringz00_21); 
if(
BOUND_CHECK(BgL_readz00_1419, BgL_l2237z00_3707))
{ /* Llib/os.scm 547 */
BgL_tmpz00_4823 = 
STRING_REF(BgL_stringz00_21, BgL_readz00_1419)
; }  else 
{ 
 obj_t BgL_auxz00_4828;
BgL_auxz00_4828 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(21518L), BGl_string2696z00zz__osz00, BgL_stringz00_21, 
(int)(BgL_l2237z00_3707), 
(int)(BgL_readz00_1419)); 
FAILURE(BgL_auxz00_4828,BFALSE,BFALSE);} } 
BgL_test3233z00_4821 = 
(BgL_tmpz00_4823==BgL_char2z00_2653); } } 
if(BgL_test3233z00_4821)
{ /* Llib/os.scm 547 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_21, 0L, BgL_readz00_1419);}  else 
{ 
 long BgL_readz00_4836;
BgL_readz00_4836 = 
(BgL_readz00_1419-1L); 
BgL_readz00_1419 = BgL_readz00_4836; 
goto BgL_zc3z04anonymousza31382ze3z87_1420;} } } } } 

}



/* suffix */
BGL_EXPORTED_DEF obj_t BGl_suffixz00zz__osz00(obj_t BgL_stringz00_22)
{
{ /* Llib/os.scm 555 */
{ /* Llib/os.scm 556 */
 long BgL_lenz00_1431;
BgL_lenz00_1431 = 
STRING_LENGTH(BgL_stringz00_22); 
{ /* Llib/os.scm 556 */
 long BgL_lenzd21zd2_1432;
BgL_lenzd21zd2_1432 = 
(BgL_lenz00_1431-1L); 
{ /* Llib/os.scm 557 */

{ 
 long BgL_readz00_1434;
BgL_readz00_1434 = BgL_lenzd21zd2_1432; 
BgL_zc3z04anonymousza31393ze3z87_1435:
if(
(BgL_readz00_1434<0L))
{ /* Llib/os.scm 560 */
return BGl_string2702z00zz__osz00;}  else 
{ /* Llib/os.scm 562 */
 bool_t BgL_test3236z00_4842;
{ /* Llib/os.scm 562 */
 unsigned char BgL_char2z00_2661;
BgL_char2z00_2661 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 562 */
 unsigned char BgL_tmpz00_4844;
{ /* Llib/os.scm 562 */
 long BgL_l2241z00_3711;
BgL_l2241z00_3711 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
BOUND_CHECK(BgL_readz00_1434, BgL_l2241z00_3711))
{ /* Llib/os.scm 562 */
BgL_tmpz00_4844 = 
STRING_REF(BgL_stringz00_22, BgL_readz00_1434)
; }  else 
{ 
 obj_t BgL_auxz00_4849;
BgL_auxz00_4849 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22061L), BGl_string2696z00zz__osz00, BgL_stringz00_22, 
(int)(BgL_l2241z00_3711), 
(int)(BgL_readz00_1434)); 
FAILURE(BgL_auxz00_4849,BFALSE,BFALSE);} } 
BgL_test3236z00_4842 = 
(BgL_tmpz00_4844==BgL_char2z00_2661); } } 
if(BgL_test3236z00_4842)
{ /* Llib/os.scm 562 */
return BGl_string2702z00zz__osz00;}  else 
{ /* Llib/os.scm 564 */
 bool_t BgL_test3238z00_4856;
{ /* Llib/os.scm 564 */
 unsigned char BgL_tmpz00_4857;
{ /* Llib/os.scm 564 */
 long BgL_l2245z00_3715;
BgL_l2245z00_3715 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
BOUND_CHECK(BgL_readz00_1434, BgL_l2245z00_3715))
{ /* Llib/os.scm 564 */
BgL_tmpz00_4857 = 
STRING_REF(BgL_stringz00_22, BgL_readz00_1434)
; }  else 
{ 
 obj_t BgL_auxz00_4862;
BgL_auxz00_4862 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22141L), BGl_string2696z00zz__osz00, BgL_stringz00_22, 
(int)(BgL_l2245z00_3715), 
(int)(BgL_readz00_1434)); 
FAILURE(BgL_auxz00_4862,BFALSE,BFALSE);} } 
BgL_test3238z00_4856 = 
(BgL_tmpz00_4857==((unsigned char)'.')); } 
if(BgL_test3238z00_4856)
{ /* Llib/os.scm 564 */
if(
(BgL_readz00_1434==BgL_lenzd21zd2_1432))
{ /* Llib/os.scm 566 */
return BGl_string2702z00zz__osz00;}  else 
{ /* Llib/os.scm 566 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_22, 
(BgL_readz00_1434+1L), BgL_lenz00_1431);} }  else 
{ 
 long BgL_readz00_4873;
BgL_readz00_4873 = 
(BgL_readz00_1434-1L); 
BgL_readz00_1434 = BgL_readz00_4873; 
goto BgL_zc3z04anonymousza31393ze3z87_1435;} } } } } } } } 

}



/* &suffix */
obj_t BGl_z62suffixz62zz__osz00(obj_t BgL_envz00_3574, obj_t BgL_stringz00_3575)
{
{ /* Llib/os.scm 555 */
{ /* Llib/os.scm 556 */
 obj_t BgL_auxz00_4875;
if(
STRINGP(BgL_stringz00_3575))
{ /* Llib/os.scm 556 */
BgL_auxz00_4875 = BgL_stringz00_3575
; }  else 
{ 
 obj_t BgL_auxz00_4878;
BgL_auxz00_4878 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(21884L), BGl_string2703z00zz__osz00, BGl_string2689z00zz__osz00, BgL_stringz00_3575); 
FAILURE(BgL_auxz00_4878,BFALSE,BFALSE);} 
return 
BGl_suffixz00zz__osz00(BgL_auxz00_4875);} } 

}



/* chmod */
BGL_EXPORTED_DEF bool_t BGl_chmodz00zz__osz00(obj_t BgL_filez00_23, obj_t BgL_modez00_24)
{
{ /* Llib/os.scm 576 */
{ 
 obj_t BgL_modez00_1448; bool_t BgL_readzf3zf3_1449; bool_t BgL_writezf3zf3_1450; bool_t BgL_execzf3zf3_1451;
{ /* Llib/os.scm 577 */
 obj_t BgL_tmpz00_4883;
BgL_modez00_1448 = BgL_modez00_24; 
BgL_readzf3zf3_1449 = ((bool_t)0); 
BgL_writezf3zf3_1450 = ((bool_t)0); 
BgL_execzf3zf3_1451 = ((bool_t)0); 
BgL_zc3z04anonymousza31404ze3z87_1452:
if(
NULLP(BgL_modez00_1448))
{ /* Llib/os.scm 582 */
BgL_tmpz00_4883 = 
BBOOL(
bgl_chmod(
BSTRING_TO_STRING(BgL_filez00_23), BgL_readzf3zf3_1449, BgL_writezf3zf3_1450, BgL_execzf3zf3_1451))
; }  else 
{ /* Llib/os.scm 584 */
 bool_t BgL_test3243z00_4889;
{ /* Llib/os.scm 584 */
 obj_t BgL_tmpz00_4890;
{ /* Llib/os.scm 584 */
 obj_t BgL_pairz00_2670;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 584 */
BgL_pairz00_2670 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4893;
BgL_auxz00_4893 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22804L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4893,BFALSE,BFALSE);} 
BgL_tmpz00_4890 = 
CAR(BgL_pairz00_2670); } 
BgL_test3243z00_4889 = 
INTEGERP(BgL_tmpz00_4890); } 
if(BgL_test3243z00_4889)
{ /* Llib/os.scm 585 */
 obj_t BgL_arg1408z00_1456;
{ /* Llib/os.scm 585 */
 obj_t BgL_pairz00_2671;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 585 */
BgL_pairz00_2671 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4901;
BgL_auxz00_4901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22837L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4901,BFALSE,BFALSE);} 
BgL_arg1408z00_1456 = 
CAR(BgL_pairz00_2671); } 
{ /* Llib/os.scm 585 */
 bool_t BgL_tmpz00_4906;
{ /* Llib/os.scm 585 */
 int BgL_auxz00_4909; char * BgL_tmpz00_4907;
{ /* Llib/os.scm 585 */
 obj_t BgL_tmpz00_4910;
if(
INTEGERP(BgL_arg1408z00_1456))
{ /* Llib/os.scm 585 */
BgL_tmpz00_4910 = BgL_arg1408z00_1456
; }  else 
{ 
 obj_t BgL_auxz00_4913;
BgL_auxz00_4913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22841L), BGl_string2704z00zz__osz00, BGl_string2679z00zz__osz00, BgL_arg1408z00_1456); 
FAILURE(BgL_auxz00_4913,BFALSE,BFALSE);} 
BgL_auxz00_4909 = 
CINT(BgL_tmpz00_4910); } 
BgL_tmpz00_4907 = 
BSTRING_TO_STRING(BgL_filez00_23); 
BgL_tmpz00_4906 = 
chmod(BgL_tmpz00_4907, BgL_auxz00_4909); } 
BgL_tmpz00_4883 = 
BBOOL(BgL_tmpz00_4906); } }  else 
{ /* Llib/os.scm 586 */
 bool_t BgL_test3247z00_4920;
{ /* Llib/os.scm 586 */
 obj_t BgL_tmpz00_4921;
{ /* Llib/os.scm 586 */
 obj_t BgL_pairz00_2672;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 586 */
BgL_pairz00_2672 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4924;
BgL_auxz00_4924 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22858L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4924,BFALSE,BFALSE);} 
BgL_tmpz00_4921 = 
CAR(BgL_pairz00_2672); } 
BgL_test3247z00_4920 = 
(BgL_tmpz00_4921==BGl_symbol2705z00zz__osz00); } 
if(BgL_test3247z00_4920)
{ /* Llib/os.scm 587 */
 obj_t BgL_arg1412z00_1459;
{ /* Llib/os.scm 587 */
 obj_t BgL_pairz00_2673;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 587 */
BgL_pairz00_2673 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4932;
BgL_auxz00_4932 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22885L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4932,BFALSE,BFALSE);} 
BgL_arg1412z00_1459 = 
CDR(BgL_pairz00_2673); } 
{ 
 bool_t BgL_readzf3zf3_4938; obj_t BgL_modez00_4937;
BgL_modez00_4937 = BgL_arg1412z00_1459; 
BgL_readzf3zf3_4938 = ((bool_t)1); 
BgL_readzf3zf3_1449 = BgL_readzf3zf3_4938; 
BgL_modez00_1448 = BgL_modez00_4937; 
goto BgL_zc3z04anonymousza31404ze3z87_1452;} }  else 
{ /* Llib/os.scm 591 */
 bool_t BgL_test3250z00_4939;
{ /* Llib/os.scm 591 */
 obj_t BgL_tmpz00_4940;
{ /* Llib/os.scm 591 */
 obj_t BgL_pairz00_2674;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 591 */
BgL_pairz00_2674 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4943;
BgL_auxz00_4943 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22928L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4943,BFALSE,BFALSE);} 
BgL_tmpz00_4940 = 
CAR(BgL_pairz00_2674); } 
BgL_test3250z00_4939 = 
(BgL_tmpz00_4940==BGl_symbol2707z00zz__osz00); } 
if(BgL_test3250z00_4939)
{ /* Llib/os.scm 592 */
 obj_t BgL_arg1415z00_1462;
{ /* Llib/os.scm 592 */
 obj_t BgL_pairz00_2675;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 592 */
BgL_pairz00_2675 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4951;
BgL_auxz00_4951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22956L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4951,BFALSE,BFALSE);} 
BgL_arg1415z00_1462 = 
CDR(BgL_pairz00_2675); } 
{ 
 bool_t BgL_writezf3zf3_4957; obj_t BgL_modez00_4956;
BgL_modez00_4956 = BgL_arg1415z00_1462; 
BgL_writezf3zf3_4957 = ((bool_t)1); 
BgL_writezf3zf3_1450 = BgL_writezf3zf3_4957; 
BgL_modez00_1448 = BgL_modez00_4956; 
goto BgL_zc3z04anonymousza31404ze3z87_1452;} }  else 
{ /* Llib/os.scm 596 */
 bool_t BgL_test3253z00_4958;
{ /* Llib/os.scm 596 */
 obj_t BgL_tmpz00_4959;
{ /* Llib/os.scm 596 */
 obj_t BgL_pairz00_2676;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 596 */
BgL_pairz00_2676 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4962;
BgL_auxz00_4962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22998L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4962,BFALSE,BFALSE);} 
BgL_tmpz00_4959 = 
CAR(BgL_pairz00_2676); } 
BgL_test3253z00_4958 = 
(BgL_tmpz00_4959==BGl_symbol2709z00zz__osz00); } 
if(BgL_test3253z00_4958)
{ /* Llib/os.scm 597 */
 obj_t BgL_arg1418z00_1465;
{ /* Llib/os.scm 597 */
 obj_t BgL_pairz00_2677;
if(
PAIRP(BgL_modez00_1448))
{ /* Llib/os.scm 597 */
BgL_pairz00_2677 = BgL_modez00_1448; }  else 
{ 
 obj_t BgL_auxz00_4970;
BgL_auxz00_4970 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(23028L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_modez00_1448); 
FAILURE(BgL_auxz00_4970,BFALSE,BFALSE);} 
BgL_arg1418z00_1465 = 
CDR(BgL_pairz00_2677); } 
{ 
 bool_t BgL_execzf3zf3_4976; obj_t BgL_modez00_4975;
BgL_modez00_4975 = BgL_arg1418z00_1465; 
BgL_execzf3zf3_4976 = ((bool_t)1); 
BgL_execzf3zf3_1451 = BgL_execzf3zf3_4976; 
BgL_modez00_1448 = BgL_modez00_4975; 
goto BgL_zc3z04anonymousza31404ze3z87_1452;} }  else 
{ /* Llib/os.scm 596 */
BgL_tmpz00_4883 = 
BGl_errorz00zz__errorz00(BGl_string2711z00zz__osz00, BGl_string2712z00zz__osz00, BgL_modez00_1448)
; } } } } } 
return 
CBOOL(BgL_tmpz00_4883);} } } 

}



/* &chmod */
obj_t BGl_z62chmodz62zz__osz00(obj_t BgL_envz00_3576, obj_t BgL_filez00_3577, obj_t BgL_modez00_3578)
{
{ /* Llib/os.scm 576 */
{ /* Llib/os.scm 582 */
 bool_t BgL_tmpz00_4979;
{ /* Llib/os.scm 582 */
 obj_t BgL_auxz00_4980;
if(
STRINGP(BgL_filez00_3577))
{ /* Llib/os.scm 582 */
BgL_auxz00_4980 = BgL_filez00_3577
; }  else 
{ 
 obj_t BgL_auxz00_4983;
BgL_auxz00_4983 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(22735L), BGl_string2713z00zz__osz00, BGl_string2689z00zz__osz00, BgL_filez00_3577); 
FAILURE(BgL_auxz00_4983,BFALSE,BFALSE);} 
BgL_tmpz00_4979 = 
BGl_chmodz00zz__osz00(BgL_auxz00_4980, BgL_modez00_3578); } 
return 
BBOOL(BgL_tmpz00_4979);} } 

}



/* make-file-name */
BGL_EXPORTED_DEF obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t BgL_directoryz00_25, obj_t BgL_filez00_26)
{
{ /* Llib/os.scm 610 */
{ 
 long BgL_ldirz00_1490;
{ /* Llib/os.scm 618 */
 long BgL_ldirz00_1472;
BgL_ldirz00_1472 = 
STRING_LENGTH(BgL_directoryz00_25); 
{ /* Llib/os.scm 620 */
 bool_t BgL_test3257z00_4990;
if(
(BgL_ldirz00_1472==1L))
{ /* Llib/os.scm 620 */
 unsigned char BgL_tmpz00_4993;
if(
BOUND_CHECK(0L, BgL_ldirz00_1472))
{ /* Llib/os.scm 620 */
BgL_tmpz00_4993 = 
STRING_REF(BgL_directoryz00_25, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_4997;
BgL_auxz00_4997 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(23962L), BGl_string2696z00zz__osz00, BgL_directoryz00_25, 
(int)(BgL_ldirz00_1472), 
(int)(0L)); 
FAILURE(BgL_auxz00_4997,BFALSE,BFALSE);} 
BgL_test3257z00_4990 = 
(BgL_tmpz00_4993==((unsigned char)'.')); }  else 
{ /* Llib/os.scm 620 */
BgL_test3257z00_4990 = ((bool_t)0)
; } 
if(BgL_test3257z00_4990)
{ /* Llib/os.scm 620 */
return BgL_filez00_26;}  else 
{ /* Llib/os.scm 620 */
if(
(BgL_ldirz00_1472==0L))
{ /* Llib/os.scm 623 */
 long BgL_lfilez00_1477;
BgL_lfilez00_1477 = 
STRING_LENGTH(BgL_filez00_26); 
{ /* Llib/os.scm 623 */
 long BgL_lenz00_1478;
BgL_lenz00_1478 = 
(1L+BgL_lfilez00_1477); 
{ /* Llib/os.scm 624 */
 obj_t BgL_strz00_1479;
BgL_strz00_1479 = 
make_string(BgL_lenz00_1478, 
(unsigned char)(FILE_SEPARATOR)); 
{ /* Llib/os.scm 625 */

blit_string(BgL_filez00_26, 0L, BgL_strz00_1479, 1L, BgL_lfilez00_1477); 
return BgL_strz00_1479;} } } }  else 
{ /* Llib/os.scm 628 */
 bool_t BgL_test3261z00_5011;
{ /* Llib/os.scm 628 */
 unsigned char BgL_char2z00_2707;
BgL_char2z00_2707 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 628 */
 unsigned char BgL_tmpz00_5013;
{ /* Llib/os.scm 628 */
 long BgL_i2252z00_3722;
BgL_i2252z00_3722 = 
(BgL_ldirz00_1472-1L); 
{ /* Llib/os.scm 628 */
 long BgL_l2253z00_3723;
BgL_l2253z00_3723 = 
STRING_LENGTH(BgL_directoryz00_25); 
if(
BOUND_CHECK(BgL_i2252z00_3722, BgL_l2253z00_3723))
{ /* Llib/os.scm 628 */
BgL_tmpz00_5013 = 
STRING_REF(BgL_directoryz00_25, BgL_i2252z00_3722)
; }  else 
{ 
 obj_t BgL_auxz00_5019;
BgL_auxz00_5019 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(24197L), BGl_string2696z00zz__osz00, BgL_directoryz00_25, 
(int)(BgL_l2253z00_3723), 
(int)(BgL_i2252z00_3722)); 
FAILURE(BgL_auxz00_5019,BFALSE,BFALSE);} } } 
BgL_test3261z00_5011 = 
(BgL_tmpz00_5013==BgL_char2z00_2707); } } 
if(BgL_test3261z00_5011)
{ /* Llib/os.scm 629 */
 long BgL_lfilez00_1483;
BgL_lfilez00_1483 = 
STRING_LENGTH(BgL_filez00_26); 
{ /* Llib/os.scm 629 */
 long BgL_lenz00_1484;
BgL_lenz00_1484 = 
(BgL_ldirz00_1472+BgL_lfilez00_1483); 
{ /* Llib/os.scm 630 */
 obj_t BgL_strz00_1485;
BgL_strz00_1485 = 
make_string(BgL_lenz00_1484, 
(unsigned char)(FILE_SEPARATOR)); 
{ /* Llib/os.scm 631 */

blit_string(BgL_directoryz00_25, 0L, BgL_strz00_1485, 0L, BgL_ldirz00_1472); 
blit_string(BgL_filez00_26, 0L, BgL_strz00_1485, BgL_ldirz00_1472, BgL_lfilez00_1483); 
return BgL_strz00_1485;} } } }  else 
{ /* Llib/os.scm 628 */
BgL_ldirz00_1490 = BgL_ldirz00_1472; 
{ /* Llib/os.scm 612 */
 long BgL_lfilez00_1492;
BgL_lfilez00_1492 = 
STRING_LENGTH(BgL_filez00_26); 
{ /* Llib/os.scm 612 */
 long BgL_lenz00_1493;
BgL_lenz00_1493 = 
(BgL_ldirz00_1490+
(BgL_lfilez00_1492+1L)); 
{ /* Llib/os.scm 613 */
 obj_t BgL_strz00_1494;
BgL_strz00_1494 = 
make_string(BgL_lenz00_1493, 
(unsigned char)(FILE_SEPARATOR)); 
{ /* Llib/os.scm 614 */

blit_string(BgL_directoryz00_25, 0L, BgL_strz00_1494, 0L, BgL_ldirz00_1490); 
blit_string(BgL_filez00_26, 0L, BgL_strz00_1494, 
(1L+BgL_ldirz00_1490), BgL_lfilez00_1492); 
return BgL_strz00_1494;} } } } } } } } } } } 

}



/* &make-file-name */
obj_t BGl_z62makezd2filezd2namez62zz__osz00(obj_t BgL_envz00_3579, obj_t BgL_directoryz00_3580, obj_t BgL_filez00_3581)
{
{ /* Llib/os.scm 610 */
{ /* Llib/os.scm 612 */
 obj_t BgL_auxz00_5047; obj_t BgL_auxz00_5040;
if(
STRINGP(BgL_filez00_3581))
{ /* Llib/os.scm 612 */
BgL_auxz00_5047 = BgL_filez00_3581
; }  else 
{ 
 obj_t BgL_auxz00_5050;
BgL_auxz00_5050 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(23650L), BGl_string2714z00zz__osz00, BGl_string2689z00zz__osz00, BgL_filez00_3581); 
FAILURE(BgL_auxz00_5050,BFALSE,BFALSE);} 
if(
STRINGP(BgL_directoryz00_3580))
{ /* Llib/os.scm 612 */
BgL_auxz00_5040 = BgL_directoryz00_3580
; }  else 
{ 
 obj_t BgL_auxz00_5043;
BgL_auxz00_5043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(23650L), BGl_string2714z00zz__osz00, BGl_string2689z00zz__osz00, BgL_directoryz00_3580); 
FAILURE(BgL_auxz00_5043,BFALSE,BFALSE);} 
return 
BGl_makezd2filezd2namez00zz__osz00(BgL_auxz00_5040, BgL_auxz00_5047);} } 

}



/* make-file-path */
BGL_EXPORTED_DEF obj_t BGl_makezd2filezd2pathz00zz__osz00(obj_t BgL_directoryz00_27, obj_t BgL_filez00_28, obj_t BgL_objz00_29)
{
{ /* Llib/os.scm 644 */
{ /* Llib/os.scm 645 */
 bool_t BgL_test3265z00_5055;
if(
(
STRING_LENGTH(BgL_directoryz00_27)==0L))
{ /* Llib/os.scm 645 */
BgL_test3265z00_5055 = 
NULLP(BgL_objz00_29)
; }  else 
{ /* Llib/os.scm 645 */
BgL_test3265z00_5055 = ((bool_t)0)
; } 
if(BgL_test3265z00_5055)
{ /* Llib/os.scm 645 */
return BgL_filez00_28;}  else 
{ /* Llib/os.scm 647 */
 long BgL_ldirz00_1502;
BgL_ldirz00_1502 = 
STRING_LENGTH(BgL_directoryz00_27); 
{ /* Llib/os.scm 647 */
 long BgL_lfilez00_1503;
BgL_lfilez00_1503 = 
STRING_LENGTH(BgL_filez00_28); 
{ /* Llib/os.scm 648 */
 obj_t BgL_lenz00_1504;
{ /* Llib/os.scm 649 */
 long BgL_g1054z00_1522;
BgL_g1054z00_1522 = 
(BgL_ldirz00_1502+
(1L+BgL_lfilez00_1503)); 
{ 
 obj_t BgL_objz00_1524; long BgL_lz00_1525;
BgL_objz00_1524 = BgL_objz00_29; 
BgL_lz00_1525 = BgL_g1054z00_1522; 
BgL_zc3z04anonymousza31452ze3z87_1526:
if(
NULLP(BgL_objz00_1524))
{ /* Llib/os.scm 652 */
BgL_lenz00_1504 = 
BINT(BgL_lz00_1525); }  else 
{ /* Llib/os.scm 654 */
 bool_t BgL_test3268z00_5067;
{ /* Llib/os.scm 654 */
 obj_t BgL_tmpz00_5068;
{ /* Llib/os.scm 654 */
 obj_t BgL_pairz00_2726;
if(
PAIRP(BgL_objz00_1524))
{ /* Llib/os.scm 654 */
BgL_pairz00_2726 = BgL_objz00_1524; }  else 
{ 
 obj_t BgL_auxz00_5071;
BgL_auxz00_5071 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25307L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1524); 
FAILURE(BgL_auxz00_5071,BFALSE,BFALSE);} 
BgL_tmpz00_5068 = 
CAR(BgL_pairz00_2726); } 
BgL_test3268z00_5067 = 
STRINGP(BgL_tmpz00_5068); } 
if(BgL_test3268z00_5067)
{ /* Llib/os.scm 658 */
 obj_t BgL_arg1456z00_1530; long BgL_arg1457z00_1531;
{ /* Llib/os.scm 658 */
 obj_t BgL_pairz00_2727;
if(
PAIRP(BgL_objz00_1524))
{ /* Llib/os.scm 658 */
BgL_pairz00_2727 = BgL_objz00_1524; }  else 
{ 
 obj_t BgL_auxz00_5079;
BgL_auxz00_5079 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25410L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1524); 
FAILURE(BgL_auxz00_5079,BFALSE,BFALSE);} 
BgL_arg1456z00_1530 = 
CDR(BgL_pairz00_2727); } 
{ /* Llib/os.scm 658 */
 long BgL_tmpz00_5084;
{ /* Llib/os.scm 659 */
 long BgL_tmpz00_5085;
{ /* Llib/os.scm 659 */
 obj_t BgL_stringz00_2729;
{ /* Llib/os.scm 659 */
 obj_t BgL_pairz00_2728;
if(
PAIRP(BgL_objz00_1524))
{ /* Llib/os.scm 659 */
BgL_pairz00_2728 = BgL_objz00_1524; }  else 
{ 
 obj_t BgL_auxz00_5088;
BgL_auxz00_5088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25459L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1524); 
FAILURE(BgL_auxz00_5088,BFALSE,BFALSE);} 
{ /* Llib/os.scm 659 */
 obj_t BgL_aux2475z00_3945;
BgL_aux2475z00_3945 = 
CAR(BgL_pairz00_2728); 
if(
STRINGP(BgL_aux2475z00_3945))
{ /* Llib/os.scm 659 */
BgL_stringz00_2729 = BgL_aux2475z00_3945; }  else 
{ 
 obj_t BgL_auxz00_5095;
BgL_auxz00_5095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25454L), BGl_string2704z00zz__osz00, BGl_string2689z00zz__osz00, BgL_aux2475z00_3945); 
FAILURE(BgL_auxz00_5095,BFALSE,BFALSE);} } } 
BgL_tmpz00_5085 = 
STRING_LENGTH(BgL_stringz00_2729); } 
BgL_tmpz00_5084 = 
(BgL_tmpz00_5085+BgL_lz00_1525); } 
BgL_arg1457z00_1531 = 
(1L+BgL_tmpz00_5084); } 
{ 
 long BgL_lz00_5103; obj_t BgL_objz00_5102;
BgL_objz00_5102 = BgL_arg1456z00_1530; 
BgL_lz00_5103 = BgL_arg1457z00_1531; 
BgL_lz00_1525 = BgL_lz00_5103; 
BgL_objz00_1524 = BgL_objz00_5102; 
goto BgL_zc3z04anonymousza31452ze3z87_1526;} }  else 
{ /* Llib/os.scm 656 */
 obj_t BgL_arg1461z00_1535;
{ /* Llib/os.scm 656 */
 obj_t BgL_pairz00_2733;
if(
PAIRP(BgL_objz00_1524))
{ /* Llib/os.scm 656 */
BgL_pairz00_2733 = BgL_objz00_1524; }  else 
{ 
 obj_t BgL_auxz00_5106;
BgL_auxz00_5106 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25377L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1524); 
FAILURE(BgL_auxz00_5106,BFALSE,BFALSE);} 
BgL_arg1461z00_1535 = 
CAR(BgL_pairz00_2733); } 
BgL_lenz00_1504 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string2715z00zz__osz00, BGl_string2716z00zz__osz00, BgL_arg1461z00_1535); } } } } 
{ /* Llib/os.scm 649 */
 obj_t BgL_strz00_1505;
{ /* Llib/os.scm 661 */
 long BgL_kz00_2734;
{ /* Llib/os.scm 661 */
 obj_t BgL_tmpz00_5112;
if(
INTEGERP(BgL_lenz00_1504))
{ /* Llib/os.scm 661 */
BgL_tmpz00_5112 = BgL_lenz00_1504
; }  else 
{ 
 obj_t BgL_auxz00_5115;
BgL_auxz00_5115 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25510L), BGl_string2715z00zz__osz00, BGl_string2679z00zz__osz00, BgL_lenz00_1504); 
FAILURE(BgL_auxz00_5115,BFALSE,BFALSE);} 
BgL_kz00_2734 = 
(long)CINT(BgL_tmpz00_5112); } 
BgL_strz00_1505 = 
make_string(BgL_kz00_2734, 
(unsigned char)(FILE_SEPARATOR)); } 
{ /* Llib/os.scm 661 */

blit_string(BgL_directoryz00_27, 0L, BgL_strz00_1505, 0L, BgL_ldirz00_1502); 
blit_string(BgL_filez00_28, 0L, BgL_strz00_1505, 
(1L+BgL_ldirz00_1502), BgL_lfilez00_1503); 
{ /* Llib/os.scm 664 */
 long BgL_g1055z00_1507;
BgL_g1055z00_1507 = 
(1L+
(BgL_ldirz00_1502+BgL_lfilez00_1503)); 
{ 
 obj_t BgL_objz00_1509; long BgL_wz00_1510;
BgL_objz00_1509 = BgL_objz00_29; 
BgL_wz00_1510 = BgL_g1055z00_1507; 
BgL_zc3z04anonymousza31443ze3z87_1511:
if(
NULLP(BgL_objz00_1509))
{ /* Llib/os.scm 666 */
return BgL_strz00_1505;}  else 
{ /* Llib/os.scm 668 */
 long BgL_loz00_1513;
{ /* Llib/os.scm 668 */
 obj_t BgL_stringz00_2747;
{ /* Llib/os.scm 668 */
 obj_t BgL_pairz00_2746;
if(
PAIRP(BgL_objz00_1509))
{ /* Llib/os.scm 668 */
BgL_pairz00_2746 = BgL_objz00_1509; }  else 
{ 
 obj_t BgL_auxz00_5131;
BgL_auxz00_5131 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25758L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1509); 
FAILURE(BgL_auxz00_5131,BFALSE,BFALSE);} 
{ /* Llib/os.scm 668 */
 obj_t BgL_aux2482z00_3952;
BgL_aux2482z00_3952 = 
CAR(BgL_pairz00_2746); 
if(
STRINGP(BgL_aux2482z00_3952))
{ /* Llib/os.scm 668 */
BgL_stringz00_2747 = BgL_aux2482z00_3952; }  else 
{ 
 obj_t BgL_auxz00_5138;
BgL_auxz00_5138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25753L), BGl_string2704z00zz__osz00, BGl_string2689z00zz__osz00, BgL_aux2482z00_3952); 
FAILURE(BgL_auxz00_5138,BFALSE,BFALSE);} } } 
BgL_loz00_1513 = 
STRING_LENGTH(BgL_stringz00_2747); } 
{ /* Llib/os.scm 669 */
 obj_t BgL_arg1445z00_1514; long BgL_arg1446z00_1515;
{ /* Llib/os.scm 669 */
 obj_t BgL_pairz00_2748;
if(
PAIRP(BgL_objz00_1509))
{ /* Llib/os.scm 669 */
BgL_pairz00_2748 = BgL_objz00_1509; }  else 
{ 
 obj_t BgL_auxz00_5145;
BgL_auxz00_5145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25794L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1509); 
FAILURE(BgL_auxz00_5145,BFALSE,BFALSE);} 
BgL_arg1445z00_1514 = 
CAR(BgL_pairz00_2748); } 
BgL_arg1446z00_1515 = 
(1L+BgL_wz00_1510); 
{ /* Llib/os.scm 669 */
 obj_t BgL_s1z00_2750;
if(
STRINGP(BgL_arg1445z00_1514))
{ /* Llib/os.scm 669 */
BgL_s1z00_2750 = BgL_arg1445z00_1514; }  else 
{ 
 obj_t BgL_auxz00_5153;
BgL_auxz00_5153 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25797L), BGl_string2704z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1445z00_1514); 
FAILURE(BgL_auxz00_5153,BFALSE,BFALSE);} 
blit_string(BgL_s1z00_2750, 0L, BgL_strz00_1505, BgL_arg1446z00_1515, BgL_loz00_1513); } } 
{ /* Llib/os.scm 670 */
 obj_t BgL_arg1447z00_1516; long BgL_arg1448z00_1517;
{ /* Llib/os.scm 670 */
 obj_t BgL_pairz00_2754;
if(
PAIRP(BgL_objz00_1509))
{ /* Llib/os.scm 670 */
BgL_pairz00_2754 = BgL_objz00_1509; }  else 
{ 
 obj_t BgL_auxz00_5160;
BgL_auxz00_5160 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25836L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_objz00_1509); 
FAILURE(BgL_auxz00_5160,BFALSE,BFALSE);} 
BgL_arg1447z00_1516 = 
CDR(BgL_pairz00_2754); } 
BgL_arg1448z00_1517 = 
(BgL_wz00_1510+
(BgL_loz00_1513+1L)); 
{ 
 long BgL_wz00_5168; obj_t BgL_objz00_5167;
BgL_objz00_5167 = BgL_arg1447z00_1516; 
BgL_wz00_5168 = BgL_arg1448z00_1517; 
BgL_wz00_1510 = BgL_wz00_5168; 
BgL_objz00_1509 = BgL_objz00_5167; 
goto BgL_zc3z04anonymousza31443ze3z87_1511;} } } } } } } } } } } } 

}



/* &make-file-path */
obj_t BGl_z62makezd2filezd2pathz62zz__osz00(obj_t BgL_envz00_3582, obj_t BgL_directoryz00_3583, obj_t BgL_filez00_3584, obj_t BgL_objz00_3585)
{
{ /* Llib/os.scm 644 */
{ /* Llib/os.scm 645 */
 obj_t BgL_auxz00_5176; obj_t BgL_auxz00_5169;
if(
STRINGP(BgL_filez00_3584))
{ /* Llib/os.scm 645 */
BgL_auxz00_5176 = BgL_filez00_3584
; }  else 
{ 
 obj_t BgL_auxz00_5179;
BgL_auxz00_5179 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25020L), BGl_string2717z00zz__osz00, BGl_string2689z00zz__osz00, BgL_filez00_3584); 
FAILURE(BgL_auxz00_5179,BFALSE,BFALSE);} 
if(
STRINGP(BgL_directoryz00_3583))
{ /* Llib/os.scm 645 */
BgL_auxz00_5169 = BgL_directoryz00_3583
; }  else 
{ 
 obj_t BgL_auxz00_5172;
BgL_auxz00_5172 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(25020L), BGl_string2717z00zz__osz00, BGl_string2689z00zz__osz00, BgL_directoryz00_3583); 
FAILURE(BgL_auxz00_5172,BFALSE,BFALSE);} 
return 
BGl_makezd2filezd2pathz00zz__osz00(BgL_auxz00_5169, BgL_auxz00_5176, BgL_objz00_3585);} } 

}



/* make-static-lib-name */
BGL_EXPORTED_DEF obj_t BGl_makezd2staticzd2libzd2namezd2zz__osz00(obj_t BgL_libz00_30, obj_t BgL_backendz00_31)
{
{ /* Llib/os.scm 675 */
if(
(BgL_backendz00_31==BGl_symbol2718z00zz__osz00))
{ /* Llib/os.scm 678 */
 bool_t BgL_test3284z00_5186;
{ /* Llib/os.scm 678 */
 obj_t BgL_string1z00_2759;
BgL_string1z00_2759 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 678 */
 long BgL_l1z00_2761;
BgL_l1z00_2761 = 
STRING_LENGTH(BgL_string1z00_2759); 
if(
(BgL_l1z00_2761==5L))
{ /* Llib/os.scm 678 */
 int BgL_arg1925z00_2764;
{ /* Llib/os.scm 678 */
 char * BgL_auxz00_5193; char * BgL_tmpz00_5191;
BgL_auxz00_5193 = 
BSTRING_TO_STRING(BGl_string2685z00zz__osz00); 
BgL_tmpz00_5191 = 
BSTRING_TO_STRING(BgL_string1z00_2759); 
BgL_arg1925z00_2764 = 
memcmp(BgL_tmpz00_5191, BgL_auxz00_5193, BgL_l1z00_2761); } 
BgL_test3284z00_5186 = 
(
(long)(BgL_arg1925z00_2764)==0L); }  else 
{ /* Llib/os.scm 678 */
BgL_test3284z00_5186 = ((bool_t)0)
; } } } 
if(BgL_test3284z00_5186)
{ /* Llib/os.scm 678 */
return 
string_append_3(BgL_libz00_30, BGl_string2664z00zz__osz00, 
string_to_bstring(STATIC_LIB_SUFFIX));}  else 
{ /* Llib/os.scm 680 */
 obj_t BgL_list1468z00_1545;
{ /* Llib/os.scm 680 */
 obj_t BgL_arg1469z00_1546;
{ /* Llib/os.scm 680 */
 obj_t BgL_arg1472z00_1547;
{ /* Llib/os.scm 680 */
 obj_t BgL_arg1473z00_1548;
BgL_arg1473z00_1548 = 
MAKE_YOUNG_PAIR(
string_to_bstring(STATIC_LIB_SUFFIX), BNIL); 
BgL_arg1472z00_1547 = 
MAKE_YOUNG_PAIR(BGl_string2664z00zz__osz00, BgL_arg1473z00_1548); } 
BgL_arg1469z00_1546 = 
MAKE_YOUNG_PAIR(BgL_libz00_30, BgL_arg1472z00_1547); } 
BgL_list1468z00_1545 = 
MAKE_YOUNG_PAIR(BGl_string2720z00zz__osz00, BgL_arg1469z00_1546); } 
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1468z00_1545);} }  else 
{ /* Llib/os.scm 676 */
if(
(BgL_backendz00_31==BGl_symbol2721z00zz__osz00))
{ /* Llib/os.scm 676 */
return 
string_append(BgL_libz00_30, BGl_string2723z00zz__osz00);}  else 
{ /* Llib/os.scm 676 */
if(
(BgL_backendz00_31==BGl_symbol2724z00zz__osz00))
{ /* Llib/os.scm 676 */
return 
string_append(BgL_libz00_30, BGl_string2726z00zz__osz00);}  else 
{ /* Llib/os.scm 676 */
return 
BGl_errorz00zz__errorz00(BGl_string2727z00zz__osz00, BGl_string2728z00zz__osz00, BgL_backendz00_31);} } } } 

}



/* &make-static-lib-name */
obj_t BGl_z62makezd2staticzd2libzd2namezb0zz__osz00(obj_t BgL_envz00_3586, obj_t BgL_libz00_3587, obj_t BgL_backendz00_3588)
{
{ /* Llib/os.scm 675 */
{ /* Llib/os.scm 676 */
 obj_t BgL_auxz00_5220; obj_t BgL_auxz00_5213;
if(
SYMBOLP(BgL_backendz00_3588))
{ /* Llib/os.scm 676 */
BgL_auxz00_5220 = BgL_backendz00_3588
; }  else 
{ 
 obj_t BgL_auxz00_5223;
BgL_auxz00_5223 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(26136L), BGl_string2729z00zz__osz00, BGl_string2730z00zz__osz00, BgL_backendz00_3588); 
FAILURE(BgL_auxz00_5223,BFALSE,BFALSE);} 
if(
STRINGP(BgL_libz00_3587))
{ /* Llib/os.scm 676 */
BgL_auxz00_5213 = BgL_libz00_3587
; }  else 
{ 
 obj_t BgL_auxz00_5216;
BgL_auxz00_5216 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(26136L), BGl_string2729z00zz__osz00, BGl_string2689z00zz__osz00, BgL_libz00_3587); 
FAILURE(BgL_auxz00_5216,BFALSE,BFALSE);} 
return 
BGl_makezd2staticzd2libzd2namezd2zz__osz00(BgL_auxz00_5213, BgL_auxz00_5220);} } 

}



/* make-shared-lib-name */
BGL_EXPORTED_DEF obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t BgL_libz00_32, obj_t BgL_backendz00_33)
{
{ /* Llib/os.scm 691 */
if(
(BgL_backendz00_33==BGl_symbol2718z00zz__osz00))
{ /* Llib/os.scm 694 */
 bool_t BgL_test3291z00_5230;
{ /* Llib/os.scm 694 */
 obj_t BgL_string1z00_2773;
BgL_string1z00_2773 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 694 */
 long BgL_l1z00_2775;
BgL_l1z00_2775 = 
STRING_LENGTH(BgL_string1z00_2773); 
if(
(BgL_l1z00_2775==5L))
{ /* Llib/os.scm 694 */
 int BgL_arg1925z00_2778;
{ /* Llib/os.scm 694 */
 char * BgL_auxz00_5237; char * BgL_tmpz00_5235;
BgL_auxz00_5237 = 
BSTRING_TO_STRING(BGl_string2685z00zz__osz00); 
BgL_tmpz00_5235 = 
BSTRING_TO_STRING(BgL_string1z00_2773); 
BgL_arg1925z00_2778 = 
memcmp(BgL_tmpz00_5235, BgL_auxz00_5237, BgL_l1z00_2775); } 
BgL_test3291z00_5230 = 
(
(long)(BgL_arg1925z00_2778)==0L); }  else 
{ /* Llib/os.scm 694 */
BgL_test3291z00_5230 = ((bool_t)0)
; } } } 
if(BgL_test3291z00_5230)
{ /* Llib/os.scm 694 */
return 
string_append_3(BgL_libz00_32, BGl_string2664z00zz__osz00, 
string_to_bstring(STATIC_LIB_SUFFIX));}  else 
{ /* Llib/os.scm 696 */
 obj_t BgL_list1480z00_1556;
{ /* Llib/os.scm 696 */
 obj_t BgL_arg1481z00_1557;
{ /* Llib/os.scm 696 */
 obj_t BgL_arg1482z00_1558;
{ /* Llib/os.scm 696 */
 obj_t BgL_arg1483z00_1559;
BgL_arg1483z00_1559 = 
MAKE_YOUNG_PAIR(
string_to_bstring(SHARED_LIB_SUFFIX), BNIL); 
BgL_arg1482z00_1558 = 
MAKE_YOUNG_PAIR(BGl_string2664z00zz__osz00, BgL_arg1483z00_1559); } 
BgL_arg1481z00_1557 = 
MAKE_YOUNG_PAIR(BgL_libz00_32, BgL_arg1482z00_1558); } 
BgL_list1480z00_1556 = 
MAKE_YOUNG_PAIR(BGl_string2720z00zz__osz00, BgL_arg1481z00_1557); } 
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1480z00_1556);} }  else 
{ /* Llib/os.scm 692 */
if(
(BgL_backendz00_33==BGl_symbol2721z00zz__osz00))
{ /* Llib/os.scm 692 */
return 
string_append(BgL_libz00_32, BGl_string2723z00zz__osz00);}  else 
{ /* Llib/os.scm 692 */
if(
(BgL_backendz00_33==BGl_symbol2724z00zz__osz00))
{ /* Llib/os.scm 692 */
return 
string_append(BgL_libz00_32, BGl_string2726z00zz__osz00);}  else 
{ /* Llib/os.scm 692 */
return 
BGl_errorz00zz__errorz00(BGl_string2731z00zz__osz00, BGl_string2728z00zz__osz00, BgL_backendz00_33);} } } } 

}



/* &make-shared-lib-name */
obj_t BGl_z62makezd2sharedzd2libzd2namezb0zz__osz00(obj_t BgL_envz00_3589, obj_t BgL_libz00_3590, obj_t BgL_backendz00_3591)
{
{ /* Llib/os.scm 691 */
{ /* Llib/os.scm 692 */
 obj_t BgL_auxz00_5264; obj_t BgL_auxz00_5257;
if(
SYMBOLP(BgL_backendz00_3591))
{ /* Llib/os.scm 692 */
BgL_auxz00_5264 = BgL_backendz00_3591
; }  else 
{ 
 obj_t BgL_auxz00_5267;
BgL_auxz00_5267 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(26792L), BGl_string2732z00zz__osz00, BGl_string2730z00zz__osz00, BgL_backendz00_3591); 
FAILURE(BgL_auxz00_5267,BFALSE,BFALSE);} 
if(
STRINGP(BgL_libz00_3590))
{ /* Llib/os.scm 692 */
BgL_auxz00_5257 = BgL_libz00_3590
; }  else 
{ 
 obj_t BgL_auxz00_5260;
BgL_auxz00_5260 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(26792L), BGl_string2732z00zz__osz00, BGl_string2689z00zz__osz00, BgL_libz00_3590); 
FAILURE(BgL_auxz00_5260,BFALSE,BFALSE);} 
return 
BGl_makezd2sharedzd2libzd2namezd2zz__osz00(BgL_auxz00_5257, BgL_auxz00_5264);} } 

}



/* find-file/path */
BGL_EXPORTED_DEF obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t BgL_filezd2namezd2_34, obj_t BgL_pathz00_35)
{
{ /* Llib/os.scm 707 */
{ 
 obj_t BgL_filezd2namezd2_1583;
if(
(
STRING_LENGTH(BgL_filezd2namezd2_34)==0L))
{ /* Llib/os.scm 719 */
return BFALSE;}  else 
{ /* Llib/os.scm 721 */
 bool_t BgL_test3298z00_5275;
{ /* Llib/os.scm 721 */
 bool_t BgL_test3299z00_5276;
{ /* Llib/os.scm 721 */
 unsigned char BgL_char2z00_2818;
BgL_char2z00_2818 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 721 */
 unsigned char BgL_tmpz00_5278;
{ /* Llib/os.scm 721 */
 long BgL_l2277z00_3747;
BgL_l2277z00_3747 = 
STRING_LENGTH(BgL_filezd2namezd2_34); 
if(
BOUND_CHECK(0L, BgL_l2277z00_3747))
{ /* Llib/os.scm 721 */
BgL_tmpz00_5278 = 
STRING_REF(BgL_filezd2namezd2_34, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_5283;
BgL_auxz00_5283 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27908L), BGl_string2696z00zz__osz00, BgL_filezd2namezd2_34, 
(int)(BgL_l2277z00_3747), 
(int)(0L)); 
FAILURE(BgL_auxz00_5283,BFALSE,BFALSE);} } 
BgL_test3299z00_5276 = 
(BgL_tmpz00_5278==BgL_char2z00_2818); } } 
if(BgL_test3299z00_5276)
{ /* Llib/os.scm 721 */
BgL_test3298z00_5275 = ((bool_t)1)
; }  else 
{ /* Llib/os.scm 721 */
BgL_filezd2namezd2_1583 = BgL_filezd2namezd2_34; 
{ /* Llib/os.scm 709 */
 bool_t BgL_test3301z00_5290;
{ /* Llib/os.scm 709 */
 obj_t BgL_string1z00_2786;
BgL_string1z00_2786 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/os.scm 709 */
 long BgL_l1z00_2788;
BgL_l1z00_2788 = 
STRING_LENGTH(BgL_string1z00_2786); 
if(
(BgL_l1z00_2788==5L))
{ /* Llib/os.scm 709 */
 int BgL_arg1925z00_2791;
{ /* Llib/os.scm 709 */
 char * BgL_auxz00_5297; char * BgL_tmpz00_5295;
BgL_auxz00_5297 = 
BSTRING_TO_STRING(BGl_string2698z00zz__osz00); 
BgL_tmpz00_5295 = 
BSTRING_TO_STRING(BgL_string1z00_2786); 
BgL_arg1925z00_2791 = 
memcmp(BgL_tmpz00_5295, BgL_auxz00_5297, BgL_l1z00_2788); } 
BgL_test3301z00_5290 = 
(
(long)(BgL_arg1925z00_2791)==0L); }  else 
{ /* Llib/os.scm 709 */
BgL_test3301z00_5290 = ((bool_t)0)
; } } } 
if(BgL_test3301z00_5290)
{ /* Llib/os.scm 710 */
 bool_t BgL__ortest_1056z00_1587;
{ /* Llib/os.scm 710 */
 unsigned char BgL_tmpz00_5302;
{ /* Llib/os.scm 710 */
 long BgL_l2257z00_3727;
BgL_l2257z00_3727 = 
STRING_LENGTH(BgL_filezd2namezd2_1583); 
if(
BOUND_CHECK(0L, BgL_l2257z00_3727))
{ /* Llib/os.scm 710 */
BgL_tmpz00_5302 = 
STRING_REF(BgL_filezd2namezd2_1583, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_5307;
BgL_auxz00_5307 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27547L), BGl_string2696z00zz__osz00, BgL_filezd2namezd2_1583, 
(int)(BgL_l2257z00_3727), 
(int)(0L)); 
FAILURE(BgL_auxz00_5307,BFALSE,BFALSE);} } 
BgL__ortest_1056z00_1587 = 
(BgL_tmpz00_5302==((unsigned char)'/')); } 
if(BgL__ortest_1056z00_1587)
{ /* Llib/os.scm 710 */
BgL_test3298z00_5275 = BgL__ortest_1056z00_1587
; }  else 
{ /* Llib/os.scm 711 */
 bool_t BgL__ortest_1057z00_1588;
{ /* Llib/os.scm 711 */
 unsigned char BgL_tmpz00_5315;
{ /* Llib/os.scm 711 */
 long BgL_l2261z00_3731;
BgL_l2261z00_3731 = 
STRING_LENGTH(BgL_filezd2namezd2_1583); 
if(
BOUND_CHECK(0L, BgL_l2261z00_3731))
{ /* Llib/os.scm 711 */
BgL_tmpz00_5315 = 
STRING_REF(BgL_filezd2namezd2_1583, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_5320;
BgL_auxz00_5320 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27592L), BGl_string2696z00zz__osz00, BgL_filezd2namezd2_1583, 
(int)(BgL_l2261z00_3731), 
(int)(0L)); 
FAILURE(BgL_auxz00_5320,BFALSE,BFALSE);} } 
BgL__ortest_1057z00_1588 = 
(BgL_tmpz00_5315==((unsigned char)'\\')); } 
if(BgL__ortest_1057z00_1588)
{ /* Llib/os.scm 711 */
BgL_test3298z00_5275 = BgL__ortest_1057z00_1588
; }  else 
{ /* Llib/os.scm 711 */
if(
(
STRING_LENGTH(BgL_filezd2namezd2_1583)>2L))
{ /* Llib/os.scm 713 */
 bool_t BgL_test3308z00_5331;
{ /* Llib/os.scm 713 */
 unsigned char BgL_tmpz00_5332;
{ /* Llib/os.scm 713 */
 long BgL_l2265z00_3735;
BgL_l2265z00_3735 = 
STRING_LENGTH(BgL_filezd2namezd2_1583); 
if(
BOUND_CHECK(1L, BgL_l2265z00_3735))
{ /* Llib/os.scm 713 */
BgL_tmpz00_5332 = 
STRING_REF(BgL_filezd2namezd2_1583, 1L)
; }  else 
{ 
 obj_t BgL_auxz00_5337;
BgL_auxz00_5337 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27684L), BGl_string2696z00zz__osz00, BgL_filezd2namezd2_1583, 
(int)(BgL_l2265z00_3735), 
(int)(1L)); 
FAILURE(BgL_auxz00_5337,BFALSE,BFALSE);} } 
BgL_test3308z00_5331 = 
(BgL_tmpz00_5332==((unsigned char)':')); } 
if(BgL_test3308z00_5331)
{ /* Llib/os.scm 714 */
 bool_t BgL__ortest_1059z00_1592;
{ /* Llib/os.scm 714 */
 unsigned char BgL_tmpz00_5344;
{ /* Llib/os.scm 714 */
 long BgL_l2269z00_3739;
BgL_l2269z00_3739 = 
STRING_LENGTH(BgL_filezd2namezd2_1583); 
if(
BOUND_CHECK(2L, BgL_l2269z00_3739))
{ /* Llib/os.scm 714 */
BgL_tmpz00_5344 = 
STRING_REF(BgL_filezd2namezd2_1583, 2L)
; }  else 
{ 
 obj_t BgL_auxz00_5349;
BgL_auxz00_5349 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27735L), BGl_string2696z00zz__osz00, BgL_filezd2namezd2_1583, 
(int)(BgL_l2269z00_3739), 
(int)(2L)); 
FAILURE(BgL_auxz00_5349,BFALSE,BFALSE);} } 
BgL__ortest_1059z00_1592 = 
(BgL_tmpz00_5344==((unsigned char)'/')); } 
if(BgL__ortest_1059z00_1592)
{ /* Llib/os.scm 714 */
BgL_test3298z00_5275 = BgL__ortest_1059z00_1592
; }  else 
{ /* Llib/os.scm 715 */
 unsigned char BgL_tmpz00_5357;
{ /* Llib/os.scm 715 */
 long BgL_l2273z00_3743;
BgL_l2273z00_3743 = 
STRING_LENGTH(BgL_filezd2namezd2_1583); 
if(
BOUND_CHECK(2L, BgL_l2273z00_3743))
{ /* Llib/os.scm 715 */
BgL_tmpz00_5357 = 
STRING_REF(BgL_filezd2namezd2_1583, 2L)
; }  else 
{ 
 obj_t BgL_auxz00_5362;
BgL_auxz00_5362 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27779L), BGl_string2696z00zz__osz00, BgL_filezd2namezd2_1583, 
(int)(BgL_l2273z00_3743), 
(int)(2L)); 
FAILURE(BgL_auxz00_5362,BFALSE,BFALSE);} } 
BgL_test3298z00_5275 = 
(BgL_tmpz00_5357==((unsigned char)'\\')); } }  else 
{ /* Llib/os.scm 713 */
BgL_test3298z00_5275 = ((bool_t)0)
; } }  else 
{ /* Llib/os.scm 712 */
BgL_test3298z00_5275 = ((bool_t)0)
; } } } }  else 
{ /* Llib/os.scm 709 */
BgL_test3298z00_5275 = ((bool_t)0)
; } } } } 
if(BgL_test3298z00_5275)
{ /* Llib/os.scm 721 */
if(
fexists(
BSTRING_TO_STRING(BgL_filezd2namezd2_34)))
{ /* Llib/os.scm 723 */
return BgL_filezd2namezd2_34;}  else 
{ /* Llib/os.scm 723 */
return BFALSE;} }  else 
{ 
 obj_t BgL_pathz00_1572;
BgL_pathz00_1572 = BgL_pathz00_35; 
BgL_zc3z04anonymousza31495ze3z87_1573:
if(
NULLP(BgL_pathz00_1572))
{ /* Llib/os.scm 728 */
return BFALSE;}  else 
{ /* Llib/os.scm 730 */
 obj_t BgL_fnamez00_1575;
{ /* Llib/os.scm 730 */
 obj_t BgL_arg1499z00_1578;
{ /* Llib/os.scm 730 */
 obj_t BgL_pairz00_2820;
if(
PAIRP(BgL_pathz00_1572))
{ /* Llib/os.scm 730 */
BgL_pairz00_2820 = BgL_pathz00_1572; }  else 
{ 
 obj_t BgL_auxz00_5376;
BgL_auxz00_5376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(28188L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_pathz00_1572); 
FAILURE(BgL_auxz00_5376,BFALSE,BFALSE);} 
BgL_arg1499z00_1578 = 
CAR(BgL_pairz00_2820); } 
{ /* Llib/os.scm 730 */
 obj_t BgL_auxz00_5381;
if(
STRINGP(BgL_arg1499z00_1578))
{ /* Llib/os.scm 730 */
BgL_auxz00_5381 = BgL_arg1499z00_1578
; }  else 
{ 
 obj_t BgL_auxz00_5384;
BgL_auxz00_5384 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(28192L), BGl_string2704z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1499z00_1578); 
FAILURE(BgL_auxz00_5384,BFALSE,BFALSE);} 
BgL_fnamez00_1575 = 
BGl_makezd2filezd2namez00zz__osz00(BgL_auxz00_5381, BgL_filezd2namezd2_34); } } 
if(
fexists(
BSTRING_TO_STRING(BgL_fnamez00_1575)))
{ /* Llib/os.scm 731 */
return BgL_fnamez00_1575;}  else 
{ /* Llib/os.scm 733 */
 obj_t BgL_arg1498z00_1577;
{ /* Llib/os.scm 733 */
 obj_t BgL_pairz00_2822;
if(
PAIRP(BgL_pathz00_1572))
{ /* Llib/os.scm 733 */
BgL_pairz00_2822 = BgL_pathz00_1572; }  else 
{ 
 obj_t BgL_auxz00_5394;
BgL_auxz00_5394 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(28266L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_pathz00_1572); 
FAILURE(BgL_auxz00_5394,BFALSE,BFALSE);} 
BgL_arg1498z00_1577 = 
CDR(BgL_pairz00_2822); } 
{ 
 obj_t BgL_pathz00_5399;
BgL_pathz00_5399 = BgL_arg1498z00_1577; 
BgL_pathz00_1572 = BgL_pathz00_5399; 
goto BgL_zc3z04anonymousza31495ze3z87_1573;} } } } } } } 

}



/* &find-file/path */
obj_t BGl_z62findzd2filezf2pathz42zz__osz00(obj_t BgL_envz00_3592, obj_t BgL_filezd2namezd2_3593, obj_t BgL_pathz00_3594)
{
{ /* Llib/os.scm 707 */
{ /* Llib/os.scm 709 */
 obj_t BgL_auxz00_5400;
if(
STRINGP(BgL_filezd2namezd2_3593))
{ /* Llib/os.scm 709 */
BgL_auxz00_5400 = BgL_filezd2namezd2_3593
; }  else 
{ 
 obj_t BgL_auxz00_5403;
BgL_auxz00_5403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(27498L), BGl_string2733z00zz__osz00, BGl_string2689z00zz__osz00, BgL_filezd2namezd2_3593); 
FAILURE(BgL_auxz00_5403,BFALSE,BFALSE);} 
return 
BGl_findzd2filezf2pathz20zz__osz00(BgL_auxz00_5400, BgL_pathz00_3594);} } 

}



/* file-name->list */
BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2ze3listze3zz__osz00(obj_t BgL_namez00_36)
{
{ /* Llib/os.scm 738 */
{ /* Llib/os.scm 739 */
 long BgL_lenz00_1601;
BgL_lenz00_1601 = 
STRING_LENGTH(BgL_namez00_36); 
{ /* Llib/os.scm 740 */
 bool_t BgL_test3320z00_5409;
if(
(BgL_lenz00_1601==1L))
{ /* Llib/os.scm 740 */
 unsigned char BgL_char2z00_2827;
BgL_char2z00_2827 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 740 */
 unsigned char BgL_tmpz00_5413;
if(
BOUND_CHECK(0L, BgL_lenz00_1601))
{ /* Llib/os.scm 740 */
BgL_tmpz00_5413 = 
STRING_REF(BgL_namez00_36, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_5417;
BgL_auxz00_5417 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(28606L), BGl_string2696z00zz__osz00, BgL_namez00_36, 
(int)(BgL_lenz00_1601), 
(int)(0L)); 
FAILURE(BgL_auxz00_5417,BFALSE,BFALSE);} 
BgL_test3320z00_5409 = 
(BgL_tmpz00_5413==BgL_char2z00_2827); } }  else 
{ /* Llib/os.scm 740 */
BgL_test3320z00_5409 = ((bool_t)0)
; } 
if(BgL_test3320z00_5409)
{ /* Llib/os.scm 741 */
 obj_t BgL_list1519z00_1606;
BgL_list1519z00_1606 = 
MAKE_YOUNG_PAIR(BGl_string2702z00zz__osz00, BNIL); 
return BgL_list1519z00_1606;}  else 
{ 
 long BgL_startz00_1609; long BgL_stopz00_1610; obj_t BgL_resz00_1611;
BgL_startz00_1609 = 0L; 
BgL_stopz00_1610 = 0L; 
BgL_resz00_1611 = BNIL; 
BgL_zc3z04anonymousza31520ze3z87_1612:
if(
(BgL_stopz00_1610==BgL_lenz00_1601))
{ /* Llib/os.scm 747 */
 obj_t BgL_arg1522z00_1614;
BgL_arg1522z00_1614 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_namez00_36, BgL_startz00_1609, BgL_stopz00_1610), BgL_resz00_1611); 
return 
bgl_reverse_bang(BgL_arg1522z00_1614);}  else 
{ /* Llib/os.scm 748 */
 bool_t BgL_test3324z00_5430;
{ /* Llib/os.scm 748 */
 unsigned char BgL_char2z00_2834;
BgL_char2z00_2834 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 748 */
 unsigned char BgL_tmpz00_5432;
{ /* Llib/os.scm 748 */
 long BgL_l2285z00_3755;
BgL_l2285z00_3755 = 
STRING_LENGTH(BgL_namez00_36); 
if(
BOUND_CHECK(BgL_stopz00_1610, BgL_l2285z00_3755))
{ /* Llib/os.scm 748 */
BgL_tmpz00_5432 = 
STRING_REF(BgL_namez00_36, BgL_stopz00_1610)
; }  else 
{ 
 obj_t BgL_auxz00_5437;
BgL_auxz00_5437 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(28811L), BGl_string2696z00zz__osz00, BgL_namez00_36, 
(int)(BgL_l2285z00_3755), 
(int)(BgL_stopz00_1610)); 
FAILURE(BgL_auxz00_5437,BFALSE,BFALSE);} } 
BgL_test3324z00_5430 = 
(BgL_tmpz00_5432==BgL_char2z00_2834); } } 
if(BgL_test3324z00_5430)
{ /* Llib/os.scm 749 */
 long BgL_arg1527z00_1619; long BgL_arg1528z00_1620; obj_t BgL_arg1529z00_1621;
BgL_arg1527z00_1619 = 
(BgL_stopz00_1610+1L); 
BgL_arg1528z00_1620 = 
(BgL_stopz00_1610+1L); 
BgL_arg1529z00_1621 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_namez00_36, BgL_startz00_1609, BgL_stopz00_1610), BgL_resz00_1611); 
{ 
 obj_t BgL_resz00_5450; long BgL_stopz00_5449; long BgL_startz00_5448;
BgL_startz00_5448 = BgL_arg1527z00_1619; 
BgL_stopz00_5449 = BgL_arg1528z00_1620; 
BgL_resz00_5450 = BgL_arg1529z00_1621; 
BgL_resz00_1611 = BgL_resz00_5450; 
BgL_stopz00_1610 = BgL_stopz00_5449; 
BgL_startz00_1609 = BgL_startz00_5448; 
goto BgL_zc3z04anonymousza31520ze3z87_1612;} }  else 
{ 
 long BgL_stopz00_5451;
BgL_stopz00_5451 = 
(BgL_stopz00_1610+1L); 
BgL_stopz00_1610 = BgL_stopz00_5451; 
goto BgL_zc3z04anonymousza31520ze3z87_1612;} } } } } } 

}



/* &file-name->list */
obj_t BGl_z62filezd2namezd2ze3listz81zz__osz00(obj_t BgL_envz00_3595, obj_t BgL_namez00_3596)
{
{ /* Llib/os.scm 738 */
{ /* Llib/os.scm 739 */
 obj_t BgL_auxz00_5453;
if(
STRINGP(BgL_namez00_3596))
{ /* Llib/os.scm 739 */
BgL_auxz00_5453 = BgL_namez00_3596
; }  else 
{ 
 obj_t BgL_auxz00_5456;
BgL_auxz00_5456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(28537L), BGl_string2734z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3596); 
FAILURE(BgL_auxz00_5456,BFALSE,BFALSE);} 
return 
BGl_filezd2namezd2ze3listze3zz__osz00(BgL_auxz00_5453);} } 

}



/* file-name-canonicalize-inner */
obj_t BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(obj_t BgL_srcz00_37, obj_t BgL_resz00_38, long BgL_indexz00_39)
{
{ /* Llib/os.scm 758 */
{ /* Llib/os.scm 760 */
 long BgL_lenz00_1631; long BgL_iz00_1632; long BgL_jz00_1633;
BgL_lenz00_1631 = 
STRING_LENGTH(BgL_srcz00_37); 
BgL_iz00_1632 = BgL_indexz00_39; 
BgL_jz00_1633 = BgL_indexz00_39; 
{ 

BgL_zc3z04anonymousza31607ze3z87_1694:
{ /* Llib/os.scm 874 */
 bool_t BgL_test3327z00_5462;
if(
(BgL_iz00_1632<
(BgL_lenz00_1631-3L)))
{ /* Llib/os.scm 875 */
 bool_t BgL_test3329z00_5466;
{ /* Llib/os.scm 875 */
 unsigned char BgL_tmpz00_5467;
{ /* Llib/os.scm 875 */
 long BgL_kz00_2974;
BgL_kz00_2974 = BgL_iz00_1632; 
{ /* Llib/os.scm 875 */
 long BgL_l2289z00_3759;
BgL_l2289z00_3759 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2974, BgL_l2289z00_3759))
{ /* Llib/os.scm 875 */
BgL_tmpz00_5467 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2974)
; }  else 
{ 
 obj_t BgL_auxz00_5472;
BgL_auxz00_5472 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32205L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2289z00_3759), 
(int)(BgL_kz00_2974)); 
FAILURE(BgL_auxz00_5472,BFALSE,BFALSE);} } } 
BgL_test3329z00_5466 = 
(BgL_tmpz00_5467==((unsigned char)'.')); } 
if(BgL_test3329z00_5466)
{ /* Llib/os.scm 876 */
 bool_t BgL_test3331z00_5479;
{ /* Llib/os.scm 876 */
 unsigned char BgL_arg1631z00_1723;
{ /* Llib/os.scm 876 */
 long BgL_i2292z00_3762;
BgL_i2292z00_3762 = 
(BgL_iz00_1632+1L); 
{ /* Llib/os.scm 876 */
 long BgL_l2293z00_3763;
BgL_l2293z00_3763 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_i2292z00_3762, BgL_l2293z00_3763))
{ /* Llib/os.scm 876 */
BgL_arg1631z00_1723 = 
STRING_REF(BgL_srcz00_37, BgL_i2292z00_3762); }  else 
{ 
 obj_t BgL_auxz00_5485;
BgL_auxz00_5485 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32241L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2293z00_3763), 
(int)(BgL_i2292z00_3762)); 
FAILURE(BgL_auxz00_5485,BFALSE,BFALSE);} } } 
BgL_test3331z00_5479 = 
(BgL_arg1631z00_1723==((unsigned char)'.')); } 
if(BgL_test3331z00_5479)
{ /* Llib/os.scm 877 */
 unsigned char BgL_arg1629z00_1721;
{ /* Llib/os.scm 877 */
 long BgL_i2296z00_3766;
BgL_i2296z00_3766 = 
(BgL_iz00_1632+2L); 
{ /* Llib/os.scm 877 */
 long BgL_l2297z00_3767;
BgL_l2297z00_3767 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_i2296z00_3766, BgL_l2297z00_3767))
{ /* Llib/os.scm 877 */
BgL_arg1629z00_1721 = 
STRING_REF(BgL_srcz00_37, BgL_i2296z00_3766); }  else 
{ 
 obj_t BgL_auxz00_5497;
BgL_auxz00_5497 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32285L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2297z00_3767), 
(int)(BgL_i2296z00_3766)); 
FAILURE(BgL_auxz00_5497,BFALSE,BFALSE);} } } 
BgL_test3327z00_5462 = 
(BgL_arg1629z00_1721==
(unsigned char)(FILE_SEPARATOR)); }  else 
{ /* Llib/os.scm 876 */
BgL_test3327z00_5462 = ((bool_t)0)
; } }  else 
{ /* Llib/os.scm 875 */
BgL_test3327z00_5462 = ((bool_t)0)
; } }  else 
{ /* Llib/os.scm 874 */
BgL_test3327z00_5462 = ((bool_t)0)
; } 
if(BgL_test3327z00_5462)
{ /* Llib/os.scm 874 */
{ /* Llib/os.scm 879 */
 long BgL_kz00_2988;
BgL_kz00_2988 = BgL_jz00_1633; 
{ /* Llib/os.scm 879 */
 long BgL_l2301z00_3771;
BgL_l2301z00_3771 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2988, BgL_l2301z00_3771))
{ /* Llib/os.scm 879 */
STRING_SET(BgL_resz00_38, BgL_kz00_2988, ((unsigned char)'.')); }  else 
{ 
 obj_t BgL_auxz00_5509;
BgL_auxz00_5509 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32333L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2301z00_3771), 
(int)(BgL_kz00_2988)); 
FAILURE(BgL_auxz00_5509,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 880 */
 long BgL_arg1622z00_1709;
BgL_arg1622z00_1709 = 
(BgL_jz00_1633+1L); 
{ /* Llib/os.scm 880 */
 long BgL_l2305z00_3775;
BgL_l2305z00_3775 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_arg1622z00_1709, BgL_l2305z00_3775))
{ /* Llib/os.scm 880 */
STRING_SET(BgL_resz00_38, BgL_arg1622z00_1709, ((unsigned char)'.')); }  else 
{ 
 obj_t BgL_auxz00_5520;
BgL_auxz00_5520 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32359L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2305z00_3775), 
(int)(BgL_arg1622z00_1709)); 
FAILURE(BgL_auxz00_5520,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 881 */
 long BgL_arg1623z00_1710;
BgL_arg1623z00_1710 = 
(BgL_jz00_1633+2L); 
{ /* Llib/os.scm 881 */
 unsigned char BgL_charz00_2997;
BgL_charz00_2997 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 881 */
 long BgL_l2309z00_3779;
BgL_l2309z00_3779 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_arg1623z00_1710, BgL_l2309z00_3779))
{ /* Llib/os.scm 881 */
STRING_SET(BgL_resz00_38, BgL_arg1623z00_1710, BgL_charz00_2997); }  else 
{ 
 obj_t BgL_auxz00_5532;
BgL_auxz00_5532 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32393L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2309z00_3779), 
(int)(BgL_arg1623z00_1710)); 
FAILURE(BgL_auxz00_5532,BFALSE,BFALSE);} } } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+3L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+3L); 
{ 

BgL_zc3z04anonymousza31624ze3z87_1712:
if(
(BgL_iz00_1632==BgL_lenz00_1631))
{ /* Llib/os.scm 885 */
return 
bgl_string_shrink(BgL_resz00_38, BgL_jz00_1633);}  else 
{ /* Llib/os.scm 887 */
 bool_t BgL_test3338z00_5543;
{ /* Llib/os.scm 887 */
 unsigned char BgL_char2z00_3007;
BgL_char2z00_3007 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 887 */
 unsigned char BgL_tmpz00_5545;
{ /* Llib/os.scm 887 */
 long BgL_kz00_3005;
BgL_kz00_3005 = BgL_iz00_1632; 
{ /* Llib/os.scm 887 */
 long BgL_l2313z00_3783;
BgL_l2313z00_3783 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_3005, BgL_l2313z00_3783))
{ /* Llib/os.scm 887 */
BgL_tmpz00_5545 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_3005)
; }  else 
{ 
 obj_t BgL_auxz00_5550;
BgL_auxz00_5550 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(32556L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2313z00_3783), 
(int)(BgL_kz00_3005)); 
FAILURE(BgL_auxz00_5550,BFALSE,BFALSE);} } } 
BgL_test3338z00_5543 = 
(BgL_tmpz00_5545==BgL_char2z00_3007); } } 
if(BgL_test3338z00_5543)
{ /* Llib/os.scm 887 */
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
goto BgL_zc3z04anonymousza31624ze3z87_1712;}  else 
{ /* Llib/os.scm 887 */
goto BgL_zc3z04anonymousza31607ze3z87_1694;} } } }  else 
{ /* Llib/os.scm 874 */
BgL_zc3z04anonymousza31553ze3z87_1653:
if(
(BgL_iz00_1632==BgL_lenz00_1631))
{ /* Llib/os.scm 797 */
return 
bgl_string_shrink(BgL_resz00_38, BgL_jz00_1633);}  else 
{ /* Llib/os.scm 799 */
 unsigned char BgL_cz00_1655;
{ /* Llib/os.scm 799 */
 long BgL_kz00_2873;
BgL_kz00_2873 = BgL_iz00_1632; 
{ /* Llib/os.scm 799 */
 long BgL_l2317z00_3787;
BgL_l2317z00_3787 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2873, BgL_l2317z00_3787))
{ /* Llib/os.scm 799 */
BgL_cz00_1655 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2873); }  else 
{ 
 obj_t BgL_auxz00_5565;
BgL_auxz00_5565 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(30179L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2317z00_3787), 
(int)(BgL_kz00_2873)); 
FAILURE(BgL_auxz00_5565,BFALSE,BFALSE);} } } 
if(
(BgL_cz00_1655==
(unsigned char)(FILE_SEPARATOR)))
{ /* Llib/os.scm 801 */
{ /* Llib/os.scm 803 */
 long BgL_kz00_2877;
BgL_kz00_2877 = BgL_jz00_1633; 
{ /* Llib/os.scm 803 */
 long BgL_l2321z00_3791;
BgL_l2321z00_3791 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2877, BgL_l2321z00_3791))
{ /* Llib/os.scm 803 */
STRING_SET(BgL_resz00_38, BgL_kz00_2877, BgL_cz00_1655); }  else 
{ 
 obj_t BgL_auxz00_5578;
BgL_auxz00_5578 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(30261L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2321z00_3791), 
(int)(BgL_kz00_2877)); 
FAILURE(BgL_auxz00_5578,BFALSE,BFALSE);} } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
BgL_zc3z04anonymousza31540ze3z87_1639:
if(
(BgL_iz00_1632==BgL_lenz00_1631))
{ /* Llib/os.scm 766 */
return 
bgl_string_shrink(BgL_resz00_38, BgL_jz00_1633);}  else 
{ /* Llib/os.scm 768 */
 bool_t BgL_test3345z00_5589;
{ /* Llib/os.scm 768 */
 unsigned char BgL_char2z00_2846;
BgL_char2z00_2846 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 768 */
 unsigned char BgL_tmpz00_5591;
{ /* Llib/os.scm 768 */
 long BgL_kz00_2844;
BgL_kz00_2844 = BgL_iz00_1632; 
{ /* Llib/os.scm 768 */
 long BgL_l2393z00_3863;
BgL_l2393z00_3863 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2844, BgL_l2393z00_3863))
{ /* Llib/os.scm 768 */
BgL_tmpz00_5591 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2844)
; }  else 
{ 
 obj_t BgL_auxz00_5596;
BgL_auxz00_5596 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(29489L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2393z00_3863), 
(int)(BgL_kz00_2844)); 
FAILURE(BgL_auxz00_5596,BFALSE,BFALSE);} } } 
BgL_test3345z00_5589 = 
(BgL_tmpz00_5591==BgL_char2z00_2846); } } 
if(BgL_test3345z00_5589)
{ /* Llib/os.scm 768 */
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
goto BgL_zc3z04anonymousza31540ze3z87_1639;}  else 
{ /* Llib/os.scm 768 */
goto BgL_zc3z04anonymousza31553ze3z87_1653;} } }  else 
{ /* Llib/os.scm 801 */
if(
(BgL_cz00_1655==((unsigned char)'.')))
{ /* Llib/os.scm 807 */
if(
(BgL_iz00_1632==
(BgL_lenz00_1631-1L)))
{ /* Llib/os.scm 809 */
if(
(BgL_jz00_1633==0L))
{ /* Llib/os.scm 811 */
{ /* Llib/os.scm 813 */
 long BgL_kz00_2888;
BgL_kz00_2888 = BgL_jz00_1633; 
{ /* Llib/os.scm 813 */
 long BgL_l2325z00_3795;
BgL_l2325z00_3795 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2888, BgL_l2325z00_3795))
{ /* Llib/os.scm 813 */
STRING_SET(BgL_resz00_38, BgL_kz00_2888, ((unsigned char)'.')); }  else 
{ 
 obj_t BgL_auxz00_5615;
BgL_auxz00_5615 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(30482L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2325z00_3795), 
(int)(BgL_kz00_2888)); 
FAILURE(BgL_auxz00_5615,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 814 */
 long BgL_tmpz00_5621;
BgL_tmpz00_5621 = 
(BgL_jz00_1633+1L); 
return 
bgl_string_shrink(BgL_resz00_38, BgL_tmpz00_5621);} }  else 
{ /* Llib/os.scm 815 */
 long BgL_tmpz00_5624;
BgL_tmpz00_5624 = 
(BgL_jz00_1633-1L); 
return 
bgl_string_shrink(BgL_resz00_38, BgL_tmpz00_5624);} }  else 
{ /* Llib/os.scm 816 */
 bool_t BgL_test3351z00_5627;
{ /* Llib/os.scm 816 */
 unsigned char BgL_arg1603z00_1691;
{ /* Llib/os.scm 816 */
 long BgL_i2328z00_3798;
BgL_i2328z00_3798 = 
(BgL_iz00_1632+1L); 
{ /* Llib/os.scm 816 */
 long BgL_l2329z00_3799;
BgL_l2329z00_3799 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_i2328z00_3798, BgL_l2329z00_3799))
{ /* Llib/os.scm 816 */
BgL_arg1603z00_1691 = 
STRING_REF(BgL_srcz00_37, BgL_i2328z00_3798); }  else 
{ 
 obj_t BgL_auxz00_5633;
BgL_auxz00_5633 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(30606L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2329z00_3799), 
(int)(BgL_i2328z00_3798)); 
FAILURE(BgL_auxz00_5633,BFALSE,BFALSE);} } } 
BgL_test3351z00_5627 = 
(BgL_arg1603z00_1691==
(unsigned char)(FILE_SEPARATOR)); } 
if(BgL_test3351z00_5627)
{ /* Llib/os.scm 816 */
if(
(BgL_iz00_1632==
(BgL_lenz00_1631-2L)))
{ /* Llib/os.scm 817 */
{ /* Llib/os.scm 820 */
 long BgL_kz00_2905;
BgL_kz00_2905 = BgL_jz00_1633; 
{ /* Llib/os.scm 820 */
 long BgL_l2333z00_3803;
BgL_l2333z00_3803 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2905, BgL_l2333z00_3803))
{ /* Llib/os.scm 820 */
STRING_SET(BgL_resz00_38, BgL_kz00_2905, ((unsigned char)'.')); }  else 
{ 
 obj_t BgL_auxz00_5648;
BgL_auxz00_5648 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(30724L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2333z00_3803), 
(int)(BgL_kz00_2905)); 
FAILURE(BgL_auxz00_5648,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 821 */
 long BgL_tmpz00_5654;
BgL_tmpz00_5654 = 
(BgL_jz00_1633+1L); 
return 
bgl_string_shrink(BgL_resz00_38, BgL_tmpz00_5654);} }  else 
{ /* Llib/os.scm 817 */
BgL_iz00_1632 = 
(BgL_iz00_1632+2L); 
goto BgL_zc3z04anonymousza31540ze3z87_1639;} }  else 
{ /* Llib/os.scm 826 */
 bool_t BgL_test3355z00_5658;
{ /* Llib/os.scm 826 */
 unsigned char BgL_arg1601z00_1689;
{ /* Llib/os.scm 826 */
 long BgL_i2336z00_3806;
BgL_i2336z00_3806 = 
(BgL_iz00_1632+1L); 
{ /* Llib/os.scm 826 */
 long BgL_l2337z00_3807;
BgL_l2337z00_3807 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_i2336z00_3806, BgL_l2337z00_3807))
{ /* Llib/os.scm 826 */
BgL_arg1601z00_1689 = 
STRING_REF(BgL_srcz00_37, BgL_i2336z00_3806); }  else 
{ 
 obj_t BgL_auxz00_5664;
BgL_auxz00_5664 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(30927L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2337z00_3807), 
(int)(BgL_i2336z00_3806)); 
FAILURE(BgL_auxz00_5664,BFALSE,BFALSE);} } } 
BgL_test3355z00_5658 = 
(BgL_arg1601z00_1689==((unsigned char)'.')); } 
if(BgL_test3355z00_5658)
{ /* Llib/os.scm 826 */
if(
(BgL_iz00_1632==
(BgL_lenz00_1631-2L)))
{ /* Llib/os.scm 829 */
if(
(BgL_jz00_1633==0L))
{ /* Llib/os.scm 832 */
return 
bgl_string_shrink(BgL_resz00_38, 0L);}  else 
{ /* Llib/os.scm 832 */
if(
(BgL_jz00_1633==1L))
{ /* Llib/os.scm 834 */
return 
bgl_string_shrink(BgL_resz00_38, 1L);}  else 
{ /* Llib/os.scm 834 */
BgL_jz00_1633 = 
BGl_popzd2directoryze70z35zz__osz00(FILE_SEPARATOR, BgL_resz00_38, BgL_jz00_1633); 
if(
(BgL_jz00_1633>1L))
{ /* Llib/os.scm 839 */
 long BgL_tmpz00_5683;
BgL_tmpz00_5683 = 
(BgL_jz00_1633-1L); 
return 
bgl_string_shrink(BgL_resz00_38, BgL_tmpz00_5683);}  else 
{ /* Llib/os.scm 838 */
return 
bgl_string_shrink(BgL_resz00_38, BgL_jz00_1633);} } } }  else 
{ /* Llib/os.scm 841 */
 bool_t BgL_test3361z00_5687;
{ /* Llib/os.scm 841 */
 unsigned char BgL_arg1593z00_1685;
{ /* Llib/os.scm 841 */
 long BgL_i2340z00_3810;
BgL_i2340z00_3810 = 
(BgL_iz00_1632+2L); 
{ /* Llib/os.scm 841 */
 long BgL_l2341z00_3811;
BgL_l2341z00_3811 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_i2340z00_3810, BgL_l2341z00_3811))
{ /* Llib/os.scm 841 */
BgL_arg1593z00_1685 = 
STRING_REF(BgL_srcz00_37, BgL_i2340z00_3810); }  else 
{ 
 obj_t BgL_auxz00_5693;
BgL_auxz00_5693 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31305L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2341z00_3811), 
(int)(BgL_i2340z00_3810)); 
FAILURE(BgL_auxz00_5693,BFALSE,BFALSE);} } } 
BgL_test3361z00_5687 = 
(BgL_arg1593z00_1685==
(unsigned char)(FILE_SEPARATOR)); } 
if(BgL_test3361z00_5687)
{ /* Llib/os.scm 841 */
if(
(BgL_jz00_1633>=2L))
{ /* Llib/os.scm 843 */
BgL_jz00_1633 = 
BGl_popzd2directoryze70z35zz__osz00(FILE_SEPARATOR, BgL_resz00_38, BgL_jz00_1633); }  else 
{ /* Llib/os.scm 843 */BFALSE; } 
BgL_iz00_1632 = 
(BgL_iz00_1632+3L); 
goto BgL_zc3z04anonymousza31540ze3z87_1639;}  else 
{ /* Llib/os.scm 841 */
{ /* Llib/os.scm 848 */
 long BgL_kz00_2937;
BgL_kz00_2937 = BgL_jz00_1633; 
{ /* Llib/os.scm 848 */
 long BgL_l2345z00_3815;
BgL_l2345z00_3815 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2937, BgL_l2345z00_3815))
{ /* Llib/os.scm 848 */
STRING_SET(BgL_resz00_38, BgL_kz00_2937, BgL_cz00_1655); }  else 
{ 
 obj_t BgL_auxz00_5709;
BgL_auxz00_5709 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31497L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2345z00_3815), 
(int)(BgL_kz00_2937)); 
FAILURE(BgL_auxz00_5709,BFALSE,BFALSE);} } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
{ /* Llib/os.scm 851 */
 unsigned char BgL_arg1589z00_1683;
{ /* Llib/os.scm 851 */
 long BgL_kz00_2942;
BgL_kz00_2942 = BgL_iz00_1632; 
{ /* Llib/os.scm 851 */
 long BgL_l2349z00_3819;
BgL_l2349z00_3819 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2942, BgL_l2349z00_3819))
{ /* Llib/os.scm 851 */
BgL_arg1589z00_1683 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2942); }  else 
{ 
 obj_t BgL_auxz00_5721;
BgL_auxz00_5721 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31597L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2349z00_3819), 
(int)(BgL_kz00_2942)); 
FAILURE(BgL_auxz00_5721,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 851 */
 long BgL_kz00_2944;
BgL_kz00_2944 = BgL_jz00_1633; 
{ /* Llib/os.scm 851 */
 long BgL_l2353z00_3823;
BgL_l2353z00_3823 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2944, BgL_l2353z00_3823))
{ /* Llib/os.scm 851 */
STRING_SET(BgL_resz00_38, BgL_kz00_2944, BgL_arg1589z00_1683); }  else 
{ 
 obj_t BgL_auxz00_5731;
BgL_auxz00_5731 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31578L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2353z00_3823), 
(int)(BgL_kz00_2944)); 
FAILURE(BgL_auxz00_5731,BFALSE,BFALSE);} } } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
{ /* Llib/os.scm 854 */
 unsigned char BgL_arg1591z00_1684;
{ /* Llib/os.scm 854 */
 long BgL_kz00_2949;
BgL_kz00_2949 = BgL_iz00_1632; 
{ /* Llib/os.scm 854 */
 long BgL_l2357z00_3827;
BgL_l2357z00_3827 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2949, BgL_l2357z00_3827))
{ /* Llib/os.scm 854 */
BgL_arg1591z00_1684 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2949); }  else 
{ 
 obj_t BgL_auxz00_5743;
BgL_auxz00_5743 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31695L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2357z00_3827), 
(int)(BgL_kz00_2949)); 
FAILURE(BgL_auxz00_5743,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 854 */
 long BgL_kz00_2951;
BgL_kz00_2951 = BgL_jz00_1633; 
{ /* Llib/os.scm 854 */
 long BgL_l2361z00_3831;
BgL_l2361z00_3831 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2951, BgL_l2361z00_3831))
{ /* Llib/os.scm 854 */
STRING_SET(BgL_resz00_38, BgL_kz00_2951, BgL_arg1591z00_1684); }  else 
{ 
 obj_t BgL_auxz00_5753;
BgL_auxz00_5753 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31676L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2361z00_3831), 
(int)(BgL_kz00_2951)); 
FAILURE(BgL_auxz00_5753,BFALSE,BFALSE);} } } } 
BgL_zc3z04anonymousza31547ze3z87_1644:
if(
(BgL_iz00_1632==BgL_lenz00_1631))
{ /* Llib/os.scm 775 */
return 
bgl_string_shrink(BgL_resz00_38, BgL_jz00_1633);}  else 
{ /* Llib/os.scm 777 */
 unsigned char BgL_cz00_1646;
{ /* Llib/os.scm 777 */
 long BgL_kz00_2853;
BgL_kz00_2853 = BgL_iz00_1632; 
{ /* Llib/os.scm 777 */
 long BgL_l2381z00_3851;
BgL_l2381z00_3851 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2853, BgL_l2381z00_3851))
{ /* Llib/os.scm 777 */
BgL_cz00_1646 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2853); }  else 
{ 
 obj_t BgL_auxz00_5766;
BgL_auxz00_5766 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(29686L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2381z00_3851), 
(int)(BgL_kz00_2853)); 
FAILURE(BgL_auxz00_5766,BFALSE,BFALSE);} } } 
if(
(BgL_cz00_1646==
(unsigned char)(FILE_SEPARATOR)))
{ /* Llib/os.scm 779 */
{ /* Llib/os.scm 780 */
 long BgL_kz00_2857;
BgL_kz00_2857 = BgL_jz00_1633; 
{ /* Llib/os.scm 780 */
 long BgL_l2385z00_3855;
BgL_l2385z00_3855 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2857, BgL_l2385z00_3855))
{ /* Llib/os.scm 780 */
STRING_SET(BgL_resz00_38, BgL_kz00_2857, BgL_cz00_1646); }  else 
{ 
 obj_t BgL_auxz00_5779;
BgL_auxz00_5779 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(29742L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2385z00_3855), 
(int)(BgL_kz00_2857)); 
FAILURE(BgL_auxz00_5779,BFALSE,BFALSE);} } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
goto BgL_zc3z04anonymousza31540ze3z87_1639;}  else 
{ /* Llib/os.scm 779 */
{ /* Llib/os.scm 785 */
 long BgL_kz00_2862;
BgL_kz00_2862 = BgL_jz00_1633; 
{ /* Llib/os.scm 785 */
 long BgL_l2389z00_3859;
BgL_l2389z00_3859 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2862, BgL_l2389z00_3859))
{ /* Llib/os.scm 785 */
STRING_SET(BgL_resz00_38, BgL_kz00_2862, BgL_cz00_1646); }  else 
{ 
 obj_t BgL_auxz00_5791;
BgL_auxz00_5791 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(29856L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2389z00_3859), 
(int)(BgL_kz00_2862)); 
FAILURE(BgL_auxz00_5791,BFALSE,BFALSE);} } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
goto BgL_zc3z04anonymousza31547ze3z87_1644;} } } } }  else 
{ /* Llib/os.scm 826 */
{ /* Llib/os.scm 858 */
 long BgL_kz00_2954;
BgL_kz00_2954 = BgL_jz00_1633; 
{ /* Llib/os.scm 858 */
 long BgL_l2365z00_3835;
BgL_l2365z00_3835 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2954, BgL_l2365z00_3835))
{ /* Llib/os.scm 858 */
STRING_SET(BgL_resz00_38, BgL_kz00_2954, BgL_cz00_1655); }  else 
{ 
 obj_t BgL_auxz00_5803;
BgL_auxz00_5803 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31780L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2365z00_3835), 
(int)(BgL_kz00_2954)); 
FAILURE(BgL_auxz00_5803,BFALSE,BFALSE);} } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
{ /* Llib/os.scm 861 */
 unsigned char BgL_arg1598z00_1688;
{ /* Llib/os.scm 861 */
 long BgL_kz00_2959;
BgL_kz00_2959 = BgL_iz00_1632; 
{ /* Llib/os.scm 861 */
 long BgL_l2369z00_3839;
BgL_l2369z00_3839 = 
STRING_LENGTH(BgL_srcz00_37); 
if(
BOUND_CHECK(BgL_kz00_2959, BgL_l2369z00_3839))
{ /* Llib/os.scm 861 */
BgL_arg1598z00_1688 = 
STRING_REF(BgL_srcz00_37, BgL_kz00_2959); }  else 
{ 
 obj_t BgL_auxz00_5815;
BgL_auxz00_5815 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31868L), BGl_string2696z00zz__osz00, BgL_srcz00_37, 
(int)(BgL_l2369z00_3839), 
(int)(BgL_kz00_2959)); 
FAILURE(BgL_auxz00_5815,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 861 */
 long BgL_kz00_2961;
BgL_kz00_2961 = BgL_jz00_1633; 
{ /* Llib/os.scm 861 */
 long BgL_l2373z00_3843;
BgL_l2373z00_3843 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2961, BgL_l2373z00_3843))
{ /* Llib/os.scm 861 */
STRING_SET(BgL_resz00_38, BgL_kz00_2961, BgL_arg1598z00_1688); }  else 
{ 
 obj_t BgL_auxz00_5825;
BgL_auxz00_5825 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31849L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2373z00_3843), 
(int)(BgL_kz00_2961)); 
FAILURE(BgL_auxz00_5825,BFALSE,BFALSE);} } } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
goto BgL_zc3z04anonymousza31547ze3z87_1644;} } } }  else 
{ /* Llib/os.scm 807 */
{ /* Llib/os.scm 867 */
 long BgL_kz00_2966;
BgL_kz00_2966 = BgL_jz00_1633; 
{ /* Llib/os.scm 867 */
 long BgL_l2377z00_3847;
BgL_l2377z00_3847 = 
STRING_LENGTH(BgL_resz00_38); 
if(
BOUND_CHECK(BgL_kz00_2966, BgL_l2377z00_3847))
{ /* Llib/os.scm 867 */
STRING_SET(BgL_resz00_38, BgL_kz00_2966, BgL_cz00_1655); }  else 
{ 
 obj_t BgL_auxz00_5837;
BgL_auxz00_5837 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(31991L), BGl_string2735z00zz__osz00, BgL_resz00_38, 
(int)(BgL_l2377z00_3847), 
(int)(BgL_kz00_2966)); 
FAILURE(BgL_auxz00_5837,BFALSE,BFALSE);} } } 
BgL_iz00_1632 = 
(BgL_iz00_1632+1L); 
BgL_jz00_1633 = 
(BgL_jz00_1633+1L); 
goto BgL_zc3z04anonymousza31547ze3z87_1644;} } } } } } } } 

}



/* pop-directory~0 */
long BGl_popzd2directoryze70z35zz__osz00(unsigned char BgL_sepz00_3668, obj_t BgL_resz00_3667, long BgL_jz00_1648)
{
{ /* Llib/os.scm 794 */
{ /* Llib/os.scm 791 */
 obj_t BgL_njz00_1650;
{ /* Llib/os.scm 791 */
 long BgL_arg1552z00_1652;
BgL_arg1552z00_1652 = 
(BgL_jz00_1648-1L); 
BgL_njz00_1650 = 
BGl_stringzd2indexzd2rightz00zz__r4_strings_6_7z00(BgL_resz00_3667, 
BCHAR(BgL_sepz00_3668), 
BINT(BgL_arg1552z00_1652)); } 
if(
INTEGERP(BgL_njz00_1650))
{ /* Llib/os.scm 792 */
return 
(
(long)CINT(BgL_njz00_1650)+1L);}  else 
{ /* Llib/os.scm 792 */
return 0L;} } } 

}



/* file-name-canonicalize */
BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2canonicaliza7eza7zz__osz00(obj_t BgL_namez00_40)
{
{ /* Llib/os.scm 899 */
{ /* Llib/os.scm 900 */
 obj_t BgL_arg1638z00_3009;
{ /* Llib/os.scm 900 */
 long BgL_arg1639z00_3010;
BgL_arg1639z00_3010 = 
STRING_LENGTH(BgL_namez00_40); 
{ /* Llib/os.scm 900 */

BgL_arg1638z00_3009 = 
make_string(BgL_arg1639z00_3010, ((unsigned char)' ')); } } 
return 
BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(BgL_namez00_40, BgL_arg1638z00_3009, 0L);} } 

}



/* &file-name-canonicalize */
obj_t BGl_z62filezd2namezd2canonicaliza7ezc5zz__osz00(obj_t BgL_envz00_3597, obj_t BgL_namez00_3598)
{
{ /* Llib/os.scm 899 */
{ /* Llib/os.scm 900 */
 obj_t BgL_auxz00_5856;
if(
STRINGP(BgL_namez00_3598))
{ /* Llib/os.scm 900 */
BgL_auxz00_5856 = BgL_namez00_3598
; }  else 
{ 
 obj_t BgL_auxz00_5859;
BgL_auxz00_5859 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(33025L), BGl_string2736z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3598); 
FAILURE(BgL_auxz00_5859,BFALSE,BFALSE);} 
return 
BGl_filezd2namezd2canonicaliza7eza7zz__osz00(BgL_auxz00_5856);} } 

}



/* file-name-canonicalize! */
BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(obj_t BgL_namez00_41)
{
{ /* Llib/os.scm 905 */
{ /* Llib/os.scm 906 */
 long BgL_lenz00_1736;
BgL_lenz00_1736 = 
STRING_LENGTH(BgL_namez00_41); 
{ 
 long BgL_iz00_1739; long BgL_sz00_1740;
BgL_iz00_1739 = 0L; 
BgL_sz00_1740 = 0L; 
BgL_zc3z04anonymousza31640ze3z87_1741:
if(
(BgL_iz00_1739==BgL_lenz00_1736))
{ /* Llib/os.scm 910 */
return BgL_namez00_41;}  else 
{ /* Llib/os.scm 912 */
 unsigned char BgL_cz00_1743;
{ /* Llib/os.scm 912 */
 long BgL_l2397z00_3867;
BgL_l2397z00_3867 = 
STRING_LENGTH(BgL_namez00_41); 
if(
BOUND_CHECK(BgL_iz00_1739, BgL_l2397z00_3867))
{ /* Llib/os.scm 912 */
BgL_cz00_1743 = 
STRING_REF(BgL_namez00_41, BgL_iz00_1739); }  else 
{ 
 obj_t BgL_auxz00_5871;
BgL_auxz00_5871 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(33452L), BGl_string2696z00zz__osz00, BgL_namez00_41, 
(int)(BgL_l2397z00_3867), 
(int)(BgL_iz00_1739)); 
FAILURE(BgL_auxz00_5871,BFALSE,BFALSE);} } 
if(
(BgL_cz00_1743==
(unsigned char)(FILE_SEPARATOR)))
{ /* Llib/os.scm 914 */
if(
(BgL_sz00_1740==
(BgL_iz00_1739-1L)))
{ /* Llib/os.scm 916 */
 obj_t BgL_resz00_1747;
{ /* Ieee/string.scm 172 */

BgL_resz00_1747 = 
make_string(BgL_lenz00_1736, ((unsigned char)' ')); } 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_namez00_41, 0L, BgL_resz00_1747, 0L, BgL_iz00_1739); 
return 
BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(BgL_namez00_41, BgL_resz00_1747, BgL_sz00_1740);}  else 
{ 
 long BgL_sz00_5888; long BgL_iz00_5886;
BgL_iz00_5886 = 
(BgL_iz00_1739+1L); 
BgL_sz00_5888 = BgL_iz00_1739; 
BgL_sz00_1740 = BgL_sz00_5888; 
BgL_iz00_1739 = BgL_iz00_5886; 
goto BgL_zc3z04anonymousza31640ze3z87_1741;} }  else 
{ /* Llib/os.scm 920 */
 bool_t BgL_test3384z00_5889;
if(
(BgL_cz00_1743==((unsigned char)'.')))
{ /* Llib/os.scm 920 */
BgL_test3384z00_5889 = 
(BgL_sz00_1740>=0L)
; }  else 
{ /* Llib/os.scm 920 */
BgL_test3384z00_5889 = ((bool_t)0)
; } 
if(BgL_test3384z00_5889)
{ /* Llib/os.scm 921 */
 obj_t BgL_resz00_1754;
{ /* Ieee/string.scm 172 */

BgL_resz00_1754 = 
make_string(BgL_lenz00_1736, ((unsigned char)' ')); } 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_namez00_41, 0L, BgL_resz00_1754, 0L, BgL_iz00_1739); 
return 
BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(BgL_namez00_41, BgL_resz00_1754, BgL_sz00_1740);}  else 
{ 
 long BgL_sz00_5898; long BgL_iz00_5896;
BgL_iz00_5896 = 
(BgL_iz00_1739+1L); 
BgL_sz00_5898 = -1L; 
BgL_sz00_1740 = BgL_sz00_5898; 
BgL_iz00_1739 = BgL_iz00_5896; 
goto BgL_zc3z04anonymousza31640ze3z87_1741;} } } } } } 

}



/* &file-name-canonicalize! */
obj_t BGl_z62filezd2namezd2canonicaliza7ez12zd7zz__osz00(obj_t BgL_envz00_3599, obj_t BgL_namez00_3600)
{
{ /* Llib/os.scm 905 */
{ /* Llib/os.scm 906 */
 obj_t BgL_auxz00_5899;
if(
STRINGP(BgL_namez00_3600))
{ /* Llib/os.scm 906 */
BgL_auxz00_5899 = BgL_namez00_3600
; }  else 
{ 
 obj_t BgL_auxz00_5902;
BgL_auxz00_5902 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(33316L), BGl_string2737z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3600); 
FAILURE(BgL_auxz00_5902,BFALSE,BFALSE);} 
return 
BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(BgL_auxz00_5899);} } 

}



/* file-name-unix-canonicalize */
BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(obj_t BgL_namez00_42)
{
{ /* Llib/os.scm 930 */
{ /* Llib/os.scm 931 */
 long BgL_lenz00_1760;
BgL_lenz00_1760 = 
STRING_LENGTH(BgL_namez00_42); 
if(
(BgL_lenz00_1760==0L))
{ /* Llib/os.scm 933 */
return BgL_namez00_42;}  else 
{ /* Llib/os.scm 935 */
 bool_t BgL_test3388z00_5910;
{ /* Llib/os.scm 935 */
 unsigned char BgL_tmpz00_5911;
if(
BOUND_CHECK(0L, BgL_lenz00_1760))
{ /* Llib/os.scm 935 */
BgL_tmpz00_5911 = 
STRING_REF(BgL_namez00_42, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_5915;
BgL_auxz00_5915 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(34244L), BGl_string2696z00zz__osz00, BgL_namez00_42, 
(int)(BgL_lenz00_1760), 
(int)(0L)); 
FAILURE(BgL_auxz00_5915,BFALSE,BFALSE);} 
BgL_test3388z00_5910 = 
(BgL_tmpz00_5911==((unsigned char)'~')); } 
if(BgL_test3388z00_5910)
{ /* Llib/os.scm 935 */
if(
(BgL_lenz00_1760==1L))
{ /* Llib/os.scm 939 */
 obj_t BgL_arg1654z00_1765;
BgL_arg1654z00_1765 = 
BGl_getenvz00zz__osz00(BGl_string2686z00zz__osz00); 
{ /* Llib/os.scm 939 */
 obj_t BgL_auxz00_5925;
if(
STRINGP(BgL_arg1654z00_1765))
{ /* Llib/os.scm 939 */
BgL_auxz00_5925 = BgL_arg1654z00_1765
; }  else 
{ 
 obj_t BgL_auxz00_5928;
BgL_auxz00_5928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(34377L), BGl_string2738z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1654z00_1765); 
FAILURE(BgL_auxz00_5928,BFALSE,BFALSE);} 
BGL_TAIL return 
BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(BgL_auxz00_5925);} }  else 
{ /* Llib/os.scm 940 */
 bool_t BgL_test3392z00_5933;
{ /* Llib/os.scm 940 */
 unsigned char BgL_char2z00_3040;
BgL_char2z00_3040 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/os.scm 940 */
 unsigned char BgL_tmpz00_5935;
{ /* Llib/os.scm 940 */
 long BgL_l2405z00_3875;
BgL_l2405z00_3875 = 
STRING_LENGTH(BgL_namez00_42); 
if(
BOUND_CHECK(1L, BgL_l2405z00_3875))
{ /* Llib/os.scm 940 */
BgL_tmpz00_5935 = 
STRING_REF(BgL_namez00_42, 1L)
; }  else 
{ 
 obj_t BgL_auxz00_5940;
BgL_auxz00_5940 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(34396L), BGl_string2696z00zz__osz00, BgL_namez00_42, 
(int)(BgL_l2405z00_3875), 
(int)(1L)); 
FAILURE(BgL_auxz00_5940,BFALSE,BFALSE);} } 
BgL_test3392z00_5933 = 
(BgL_tmpz00_5935==BgL_char2z00_3040); } } 
if(BgL_test3392z00_5933)
{ /* Llib/os.scm 943 */
 obj_t BgL_arg1661z00_1769;
{ /* Llib/os.scm 943 */
 obj_t BgL_arg1663z00_1770; obj_t BgL_arg1664z00_1771;
BgL_arg1663z00_1770 = 
BGl_getenvz00zz__osz00(BGl_string2686z00zz__osz00); 
BgL_arg1664z00_1771 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_namez00_42, 1L, BgL_lenz00_1760); 
{ /* Llib/os.scm 943 */
 obj_t BgL_tmpz00_5949;
if(
STRINGP(BgL_arg1663z00_1770))
{ /* Llib/os.scm 943 */
BgL_tmpz00_5949 = BgL_arg1663z00_1770
; }  else 
{ 
 obj_t BgL_auxz00_5952;
BgL_auxz00_5952 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(34508L), BGl_string2738z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1663z00_1770); 
FAILURE(BgL_auxz00_5952,BFALSE,BFALSE);} 
BgL_arg1661z00_1769 = 
string_append(BgL_tmpz00_5949, BgL_arg1664z00_1771); } } 
BGL_TAIL return 
BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(BgL_arg1661z00_1769);}  else 
{ /* Llib/os.scm 947 */
 obj_t BgL_arg1667z00_1772;
{ /* Llib/os.scm 947 */
 obj_t BgL_arg1668z00_1773; obj_t BgL_arg1669z00_1774;
BgL_arg1668z00_1773 = 
BGl_getenvz00zz__osz00(BGl_string2686z00zz__osz00); 
BgL_arg1669z00_1774 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_namez00_42, 1L, BgL_lenz00_1760); 
{ /* Llib/os.scm 947 */
 obj_t BgL_list1670z00_1775;
BgL_list1670z00_1775 = 
MAKE_YOUNG_PAIR(BgL_arg1669z00_1774, BNIL); 
{ /* Llib/os.scm 947 */
 obj_t BgL_auxz00_5961;
if(
STRINGP(BgL_arg1668z00_1773))
{ /* Llib/os.scm 947 */
BgL_auxz00_5961 = BgL_arg1668z00_1773
; }  else 
{ 
 obj_t BgL_auxz00_5964;
BgL_auxz00_5964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(34653L), BGl_string2738z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1668z00_1773); 
FAILURE(BgL_auxz00_5964,BFALSE,BFALSE);} 
BgL_arg1667z00_1772 = 
BGl_makezd2filezd2pathz00zz__osz00(BgL_auxz00_5961, BGl_string2739z00zz__osz00, BgL_list1670z00_1775); } } } 
BGL_TAIL return 
BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(BgL_arg1667z00_1772);} } }  else 
{ /* Llib/os.scm 900 */
 obj_t BgL_arg1638z00_3042;
{ /* Llib/os.scm 900 */
 long BgL_arg1639z00_3043;
BgL_arg1639z00_3043 = 
STRING_LENGTH(BgL_namez00_42); 
{ /* Llib/os.scm 900 */

BgL_arg1638z00_3042 = 
make_string(BgL_arg1639z00_3043, ((unsigned char)' ')); } } 
return 
BGl_filezd2namezd2canonicaliza7ezd2innerz75zz__osz00(BgL_namez00_42, BgL_arg1638z00_3042, 0L);} } } } 

}



/* &file-name-unix-canonicalize */
obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez17zz__osz00(obj_t BgL_envz00_3601, obj_t BgL_namez00_3602)
{
{ /* Llib/os.scm 930 */
{ /* Llib/os.scm 931 */
 obj_t BgL_auxz00_5973;
if(
STRINGP(BgL_namez00_3602))
{ /* Llib/os.scm 931 */
BgL_auxz00_5973 = BgL_namez00_3602
; }  else 
{ 
 obj_t BgL_auxz00_5976;
BgL_auxz00_5976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(34163L), BGl_string2740z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3602); 
FAILURE(BgL_auxz00_5976,BFALSE,BFALSE);} 
return 
BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(BgL_auxz00_5973);} } 

}



/* file-name-unix-canonicalize! */
BGL_EXPORTED_DEF obj_t BGl_filezd2namezd2unixzd2canonicaliza7ez12z67zz__osz00(obj_t BgL_namez00_43)
{
{ /* Llib/os.scm 954 */
{ /* Llib/os.scm 955 */
 long BgL_lenz00_1779;
BgL_lenz00_1779 = 
STRING_LENGTH(BgL_namez00_43); 
if(
(BgL_lenz00_1779==0L))
{ /* Llib/os.scm 957 */
return BgL_namez00_43;}  else 
{ /* Llib/os.scm 959 */
 bool_t BgL_test3398z00_5984;
{ /* Llib/os.scm 959 */
 unsigned char BgL_tmpz00_5985;
if(
BOUND_CHECK(0L, BgL_lenz00_1779))
{ /* Llib/os.scm 959 */
BgL_tmpz00_5985 = 
STRING_REF(BgL_namez00_43, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_5989;
BgL_auxz00_5989 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35120L), BGl_string2696z00zz__osz00, BgL_namez00_43, 
(int)(BgL_lenz00_1779), 
(int)(0L)); 
FAILURE(BgL_auxz00_5989,BFALSE,BFALSE);} 
BgL_test3398z00_5984 = 
(BgL_tmpz00_5985==((unsigned char)'~')); } 
if(BgL_test3398z00_5984)
{ /* Llib/os.scm 959 */
BGL_TAIL return 
BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(BgL_namez00_43);}  else 
{ /* Llib/os.scm 959 */
BGL_TAIL return 
BGl_filezd2namezd2canonicaliza7ez12zb5zz__osz00(BgL_namez00_43);} } } } 

}



/* &file-name-unix-canonicalize! */
obj_t BGl_z62filezd2namezd2unixzd2canonicaliza7ez12z05zz__osz00(obj_t BgL_envz00_3603, obj_t BgL_namez00_3604)
{
{ /* Llib/os.scm 954 */
{ /* Llib/os.scm 955 */
 obj_t BgL_auxz00_5998;
if(
STRINGP(BgL_namez00_3604))
{ /* Llib/os.scm 955 */
BgL_auxz00_5998 = BgL_namez00_3604
; }  else 
{ 
 obj_t BgL_auxz00_6001;
BgL_auxz00_6001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35039L), BGl_string2741z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3604); 
FAILURE(BgL_auxz00_6001,BFALSE,BFALSE);} 
return 
BGl_filezd2namezd2unixzd2canonicaliza7ez12z67zz__osz00(BgL_auxz00_5998);} } 

}



/* relative-file-name */
BGL_EXPORTED_DEF obj_t BGl_relativezd2filezd2namez00zz__osz00(obj_t BgL_namez00_44, obj_t BgL_basez00_45)
{
{ /* Llib/os.scm 967 */
{ 
 obj_t BgL_fz00_1808;
{ /* Llib/os.scm 976 */
 obj_t BgL_fz00_1785;
BgL_fz00_1785 = 
BGl_filezd2namezd2ze3listze3zz__osz00(BgL_namez00_44); 
{ /* Llib/os.scm 977 */
 bool_t BgL_test3401z00_6007;
{ /* Llib/os.scm 977 */
 obj_t BgL_arg1707z00_1807;
{ /* Llib/os.scm 977 */
 obj_t BgL_pairz00_3068;
if(
NULLP(BgL_fz00_1785))
{ 
 obj_t BgL_auxz00_6010;
BgL_auxz00_6010 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35724L), BGl_string2743z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1785); 
FAILURE(BgL_auxz00_6010,BFALSE,BFALSE);}  else 
{ /* Llib/os.scm 977 */
BgL_pairz00_3068 = BgL_fz00_1785; } 
BgL_arg1707z00_1807 = 
CAR(BgL_pairz00_3068); } 
{ /* Llib/os.scm 977 */
 obj_t BgL_string1z00_3069;
if(
STRINGP(BgL_arg1707z00_1807))
{ /* Llib/os.scm 977 */
BgL_string1z00_3069 = BgL_arg1707z00_1807; }  else 
{ 
 obj_t BgL_auxz00_6017;
BgL_auxz00_6017 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35725L), BGl_string2743z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1707z00_1807); 
FAILURE(BgL_auxz00_6017,BFALSE,BFALSE);} 
{ /* Llib/os.scm 977 */
 long BgL_l1z00_3071;
BgL_l1z00_3071 = 
STRING_LENGTH(BgL_string1z00_3069); 
if(
(BgL_l1z00_3071==0L))
{ /* Llib/os.scm 977 */
 int BgL_arg1925z00_3074;
{ /* Llib/os.scm 977 */
 char * BgL_auxz00_6026; char * BgL_tmpz00_6024;
BgL_auxz00_6026 = 
BSTRING_TO_STRING(BGl_string2702z00zz__osz00); 
BgL_tmpz00_6024 = 
BSTRING_TO_STRING(BgL_string1z00_3069); 
BgL_arg1925z00_3074 = 
memcmp(BgL_tmpz00_6024, BgL_auxz00_6026, BgL_l1z00_3071); } 
BgL_test3401z00_6007 = 
(
(long)(BgL_arg1925z00_3074)==0L); }  else 
{ /* Llib/os.scm 977 */
BgL_test3401z00_6007 = ((bool_t)0)
; } } } } 
if(BgL_test3401z00_6007)
{ /* Llib/os.scm 979 */
 obj_t BgL_g1061z00_1788;
BgL_g1061z00_1788 = 
BGl_filezd2namezd2ze3listze3zz__osz00(BgL_basez00_45); 
{ 
 obj_t BgL_fz00_1790; obj_t BgL_bz00_1791;
{ /* Llib/os.scm 979 */
 obj_t BgL_aux2574z00_4044;
BgL_fz00_1790 = BgL_fz00_1785; 
BgL_bz00_1791 = BgL_g1061z00_1788; 
BgL_zc3z04anonymousza31687ze3z87_1792:
if(
NULLP(BgL_fz00_1790))
{ /* Llib/os.scm 982 */
BgL_aux2574z00_4044 = BGl_string2702z00zz__osz00; }  else 
{ /* Llib/os.scm 982 */
if(
NULLP(BgL_bz00_1791))
{ /* Llib/os.scm 984 */
BgL_fz00_1808 = BgL_fz00_1790; 
BgL_zc3z04anonymousza31708ze3z87_1809:
{ /* Llib/os.scm 970 */
 bool_t BgL_test3407z00_6036;
{ /* Llib/os.scm 970 */
 obj_t BgL_tmpz00_6037;
{ /* Llib/os.scm 970 */
 obj_t BgL_pairz00_3053;
if(
PAIRP(BgL_fz00_1808))
{ /* Llib/os.scm 970 */
BgL_pairz00_3053 = BgL_fz00_1808; }  else 
{ 
 obj_t BgL_auxz00_6040;
BgL_auxz00_6040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35544L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1808); 
FAILURE(BgL_auxz00_6040,BFALSE,BFALSE);} 
BgL_tmpz00_6037 = 
CDR(BgL_pairz00_3053); } 
BgL_test3407z00_6036 = 
NULLP(BgL_tmpz00_6037); } 
if(BgL_test3407z00_6036)
{ /* Llib/os.scm 971 */
 obj_t BgL_pairz00_3054;
if(
PAIRP(BgL_fz00_1808))
{ /* Llib/os.scm 971 */
BgL_pairz00_3054 = BgL_fz00_1808; }  else 
{ 
 obj_t BgL_auxz00_6048;
BgL_auxz00_6048 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35556L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1808); 
FAILURE(BgL_auxz00_6048,BFALSE,BFALSE);} 
BgL_aux2574z00_4044 = 
CAR(BgL_pairz00_3054); }  else 
{ /* Llib/os.scm 972 */
 bool_t BgL_test3410z00_6053;
{ /* Llib/os.scm 972 */
 obj_t BgL_tmpz00_6054;
{ /* Llib/os.scm 972 */
 obj_t BgL_pairz00_3055;
if(
PAIRP(BgL_fz00_1808))
{ /* Llib/os.scm 972 */
BgL_pairz00_3055 = BgL_fz00_1808; }  else 
{ 
 obj_t BgL_auxz00_6057;
BgL_auxz00_6057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35576L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1808); 
FAILURE(BgL_auxz00_6057,BFALSE,BFALSE);} 
{ /* Llib/os.scm 972 */
 obj_t BgL_pairz00_3058;
{ /* Llib/os.scm 972 */
 obj_t BgL_aux2532z00_4002;
BgL_aux2532z00_4002 = 
CDR(BgL_pairz00_3055); 
if(
PAIRP(BgL_aux2532z00_4002))
{ /* Llib/os.scm 972 */
BgL_pairz00_3058 = BgL_aux2532z00_4002; }  else 
{ 
 obj_t BgL_auxz00_6064;
BgL_auxz00_6064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35570L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_aux2532z00_4002); 
FAILURE(BgL_auxz00_6064,BFALSE,BFALSE);} } 
BgL_tmpz00_6054 = 
CDR(BgL_pairz00_3058); } } 
BgL_test3410z00_6053 = 
NULLP(BgL_tmpz00_6054); } 
if(BgL_test3410z00_6053)
{ /* Llib/os.scm 973 */
 obj_t BgL_arg1714z00_1814; obj_t BgL_arg1715z00_1815;
{ /* Llib/os.scm 973 */
 obj_t BgL_pairz00_3059;
if(
PAIRP(BgL_fz00_1808))
{ /* Llib/os.scm 973 */
BgL_pairz00_3059 = BgL_fz00_1808; }  else 
{ 
 obj_t BgL_auxz00_6072;
BgL_auxz00_6072 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35604L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1808); 
FAILURE(BgL_auxz00_6072,BFALSE,BFALSE);} 
BgL_arg1714z00_1814 = 
CAR(BgL_pairz00_3059); } 
{ /* Llib/os.scm 973 */
 obj_t BgL_pairz00_3060;
if(
PAIRP(BgL_fz00_1808))
{ /* Llib/os.scm 973 */
BgL_pairz00_3060 = BgL_fz00_1808; }  else 
{ 
 obj_t BgL_auxz00_6079;
BgL_auxz00_6079 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35613L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1808); 
FAILURE(BgL_auxz00_6079,BFALSE,BFALSE);} 
{ /* Llib/os.scm 973 */
 obj_t BgL_pairz00_3063;
{ /* Llib/os.scm 973 */
 obj_t BgL_aux2538z00_4008;
BgL_aux2538z00_4008 = 
CDR(BgL_pairz00_3060); 
if(
PAIRP(BgL_aux2538z00_4008))
{ /* Llib/os.scm 973 */
BgL_pairz00_3063 = BgL_aux2538z00_4008; }  else 
{ 
 obj_t BgL_auxz00_6086;
BgL_auxz00_6086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35607L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_aux2538z00_4008); 
FAILURE(BgL_auxz00_6086,BFALSE,BFALSE);} } 
BgL_arg1715z00_1815 = 
CAR(BgL_pairz00_3063); } } 
{ /* Llib/os.scm 973 */
 obj_t BgL_auxz00_6098; obj_t BgL_auxz00_6091;
if(
STRINGP(BgL_arg1715z00_1815))
{ /* Llib/os.scm 973 */
BgL_auxz00_6098 = BgL_arg1715z00_1815
; }  else 
{ 
 obj_t BgL_auxz00_6101;
BgL_auxz00_6101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35614L), BGl_string2742z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1715z00_1815); 
FAILURE(BgL_auxz00_6101,BFALSE,BFALSE);} 
if(
STRINGP(BgL_arg1714z00_1814))
{ /* Llib/os.scm 973 */
BgL_auxz00_6091 = BgL_arg1714z00_1814
; }  else 
{ 
 obj_t BgL_auxz00_6094;
BgL_auxz00_6094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35605L), BGl_string2742z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1714z00_1814); 
FAILURE(BgL_auxz00_6094,BFALSE,BFALSE);} 
BgL_aux2574z00_4044 = 
BGl_makezd2filezd2namez00zz__osz00(BgL_auxz00_6091, BgL_auxz00_6098); } }  else 
{ /* Llib/os.scm 975 */
 obj_t BgL_runner1718z00_1818;
BgL_runner1718z00_1818 = BgL_fz00_1808; 
{ /* Llib/os.scm 975 */
 obj_t BgL_aux1716z00_1816;
{ /* Llib/os.scm 975 */
 obj_t BgL_pairz00_3064;
{ /* Llib/os.scm 975 */
 obj_t BgL_aux2544z00_4014;
BgL_aux2544z00_4014 = BgL_runner1718z00_1818; 
if(
PAIRP(BgL_aux2544z00_4014))
{ /* Llib/os.scm 975 */
BgL_pairz00_3064 = BgL_aux2544z00_4014; }  else 
{ 
 obj_t BgL_auxz00_6108;
BgL_auxz00_6108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35629L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_aux2544z00_4014); 
FAILURE(BgL_auxz00_6108,BFALSE,BFALSE);} } 
{ /* Llib/os.scm 975 */
 obj_t BgL_aux2546z00_4016;
BgL_aux2546z00_4016 = 
CAR(BgL_pairz00_3064); 
if(
STRINGP(BgL_aux2546z00_4016))
{ /* Llib/os.scm 975 */
BgL_aux1716z00_1816 = BgL_aux2546z00_4016; }  else 
{ 
 obj_t BgL_auxz00_6115;
BgL_auxz00_6115 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35629L), BGl_string2742z00zz__osz00, BGl_string2689z00zz__osz00, BgL_aux2546z00_4016); 
FAILURE(BgL_auxz00_6115,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 975 */
 obj_t BgL_pairz00_3065;
{ /* Llib/os.scm 975 */
 obj_t BgL_aux2548z00_4018;
BgL_aux2548z00_4018 = BgL_runner1718z00_1818; 
if(
PAIRP(BgL_aux2548z00_4018))
{ /* Llib/os.scm 975 */
BgL_pairz00_3065 = BgL_aux2548z00_4018; }  else 
{ 
 obj_t BgL_auxz00_6121;
BgL_auxz00_6121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35629L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_aux2548z00_4018); 
FAILURE(BgL_auxz00_6121,BFALSE,BFALSE);} } 
BgL_runner1718z00_1818 = 
CDR(BgL_pairz00_3065); } 
{ /* Llib/os.scm 975 */
 obj_t BgL_aux1717z00_1817;
{ /* Llib/os.scm 975 */
 obj_t BgL_pairz00_3066;
{ /* Llib/os.scm 975 */
 obj_t BgL_aux2550z00_4020;
BgL_aux2550z00_4020 = BgL_runner1718z00_1818; 
if(
PAIRP(BgL_aux2550z00_4020))
{ /* Llib/os.scm 975 */
BgL_pairz00_3066 = BgL_aux2550z00_4020; }  else 
{ 
 obj_t BgL_auxz00_6128;
BgL_auxz00_6128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35629L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_aux2550z00_4020); 
FAILURE(BgL_auxz00_6128,BFALSE,BFALSE);} } 
{ /* Llib/os.scm 975 */
 obj_t BgL_aux2552z00_4022;
BgL_aux2552z00_4022 = 
CAR(BgL_pairz00_3066); 
if(
STRINGP(BgL_aux2552z00_4022))
{ /* Llib/os.scm 975 */
BgL_aux1717z00_1817 = BgL_aux2552z00_4022; }  else 
{ 
 obj_t BgL_auxz00_6135;
BgL_auxz00_6135 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35629L), BGl_string2742z00zz__osz00, BGl_string2689z00zz__osz00, BgL_aux2552z00_4022); 
FAILURE(BgL_auxz00_6135,BFALSE,BFALSE);} } } 
{ /* Llib/os.scm 975 */
 obj_t BgL_pairz00_3067;
{ /* Llib/os.scm 975 */
 obj_t BgL_aux2554z00_4024;
BgL_aux2554z00_4024 = BgL_runner1718z00_1818; 
if(
PAIRP(BgL_aux2554z00_4024))
{ /* Llib/os.scm 975 */
BgL_pairz00_3067 = BgL_aux2554z00_4024; }  else 
{ 
 obj_t BgL_auxz00_6141;
BgL_auxz00_6141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35629L), BGl_string2742z00zz__osz00, BGl_string2691z00zz__osz00, BgL_aux2554z00_4024); 
FAILURE(BgL_auxz00_6141,BFALSE,BFALSE);} } 
BgL_runner1718z00_1818 = 
CDR(BgL_pairz00_3067); } 
BgL_aux2574z00_4044 = 
BGl_makezd2filezd2pathz00zz__osz00(BgL_aux1716z00_1816, BgL_aux1717z00_1817, BgL_runner1718z00_1818); } } } } } }  else 
{ /* Llib/os.scm 986 */
 bool_t BgL_test3424z00_6147;
{ /* Llib/os.scm 986 */
 obj_t BgL_arg1705z00_1804; obj_t BgL_arg1706z00_1805;
{ /* Llib/os.scm 986 */
 obj_t BgL_pairz00_3080;
if(
PAIRP(BgL_fz00_1790))
{ /* Llib/os.scm 986 */
BgL_pairz00_3080 = BgL_fz00_1790; }  else 
{ 
 obj_t BgL_auxz00_6150;
BgL_auxz00_6150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35881L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1790); 
FAILURE(BgL_auxz00_6150,BFALSE,BFALSE);} 
BgL_arg1705z00_1804 = 
CAR(BgL_pairz00_3080); } 
{ /* Llib/os.scm 986 */
 obj_t BgL_pairz00_3081;
if(
PAIRP(BgL_bz00_1791))
{ /* Llib/os.scm 986 */
BgL_pairz00_3081 = BgL_bz00_1791; }  else 
{ 
 obj_t BgL_auxz00_6157;
BgL_auxz00_6157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35889L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_bz00_1791); 
FAILURE(BgL_auxz00_6157,BFALSE,BFALSE);} 
BgL_arg1706z00_1805 = 
CAR(BgL_pairz00_3081); } 
{ /* Llib/os.scm 986 */
 obj_t BgL_string1z00_3082; obj_t BgL_string2z00_3083;
if(
STRINGP(BgL_arg1705z00_1804))
{ /* Llib/os.scm 986 */
BgL_string1z00_3082 = BgL_arg1705z00_1804; }  else 
{ 
 obj_t BgL_auxz00_6164;
BgL_auxz00_6164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35882L), BGl_string2704z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1705z00_1804); 
FAILURE(BgL_auxz00_6164,BFALSE,BFALSE);} 
if(
STRINGP(BgL_arg1706z00_1805))
{ /* Llib/os.scm 986 */
BgL_string2z00_3083 = BgL_arg1706z00_1805; }  else 
{ 
 obj_t BgL_auxz00_6170;
BgL_auxz00_6170 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35890L), BGl_string2704z00zz__osz00, BGl_string2689z00zz__osz00, BgL_arg1706z00_1805); 
FAILURE(BgL_auxz00_6170,BFALSE,BFALSE);} 
{ /* Llib/os.scm 986 */
 long BgL_l1z00_3084;
BgL_l1z00_3084 = 
STRING_LENGTH(BgL_string1z00_3082); 
if(
(BgL_l1z00_3084==
STRING_LENGTH(BgL_string2z00_3083)))
{ /* Llib/os.scm 986 */
 int BgL_arg1925z00_3087;
{ /* Llib/os.scm 986 */
 char * BgL_auxz00_6180; char * BgL_tmpz00_6178;
BgL_auxz00_6180 = 
BSTRING_TO_STRING(BgL_string2z00_3083); 
BgL_tmpz00_6178 = 
BSTRING_TO_STRING(BgL_string1z00_3082); 
BgL_arg1925z00_3087 = 
memcmp(BgL_tmpz00_6178, BgL_auxz00_6180, BgL_l1z00_3084); } 
BgL_test3424z00_6147 = 
(
(long)(BgL_arg1925z00_3087)==0L); }  else 
{ /* Llib/os.scm 986 */
BgL_test3424z00_6147 = ((bool_t)0)
; } } } } 
if(BgL_test3424z00_6147)
{ /* Llib/os.scm 989 */
 obj_t BgL_arg1699z00_1798; obj_t BgL_arg1700z00_1799;
{ /* Llib/os.scm 989 */
 obj_t BgL_pairz00_3093;
if(
PAIRP(BgL_fz00_1790))
{ /* Llib/os.scm 989 */
BgL_pairz00_3093 = BgL_fz00_1790; }  else 
{ 
 obj_t BgL_auxz00_6187;
BgL_auxz00_6187 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35971L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_fz00_1790); 
FAILURE(BgL_auxz00_6187,BFALSE,BFALSE);} 
BgL_arg1699z00_1798 = 
CDR(BgL_pairz00_3093); } 
{ /* Llib/os.scm 989 */
 obj_t BgL_pairz00_3094;
if(
PAIRP(BgL_bz00_1791))
{ /* Llib/os.scm 989 */
BgL_pairz00_3094 = BgL_bz00_1791; }  else 
{ 
 obj_t BgL_auxz00_6194;
BgL_auxz00_6194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35979L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_bz00_1791); 
FAILURE(BgL_auxz00_6194,BFALSE,BFALSE);} 
BgL_arg1700z00_1799 = 
CDR(BgL_pairz00_3094); } 
{ 
 obj_t BgL_bz00_6200; obj_t BgL_fz00_6199;
BgL_fz00_6199 = BgL_arg1699z00_1798; 
BgL_bz00_6200 = BgL_arg1700z00_1799; 
BgL_bz00_1791 = BgL_bz00_6200; 
BgL_fz00_1790 = BgL_fz00_6199; 
goto BgL_zc3z04anonymousza31687ze3z87_1792;} }  else 
{ /* Llib/os.scm 987 */
 obj_t BgL_arg1701z00_1800;
{ /* Llib/os.scm 987 */
 obj_t BgL_arg1702z00_1801;
{ /* Llib/os.scm 987 */
 long BgL_arg1703z00_1802;
{ /* Llib/os.scm 987 */
 obj_t BgL_auxz00_6201;
{ /* Llib/os.scm 987 */
 bool_t BgL_test3432z00_6202;
if(
PAIRP(BgL_bz00_1791))
{ /* Llib/os.scm 987 */
BgL_test3432z00_6202 = ((bool_t)1)
; }  else 
{ /* Llib/os.scm 987 */
BgL_test3432z00_6202 = 
NULLP(BgL_bz00_1791)
; } 
if(BgL_test3432z00_6202)
{ /* Llib/os.scm 987 */
BgL_auxz00_6201 = BgL_bz00_1791
; }  else 
{ 
 obj_t BgL_auxz00_6206;
BgL_auxz00_6206 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35935L), BGl_string2704z00zz__osz00, BGl_string2744z00zz__osz00, BgL_bz00_1791); 
FAILURE(BgL_auxz00_6206,BFALSE,BFALSE);} } 
BgL_arg1703z00_1802 = 
bgl_list_length(BgL_auxz00_6201); } 
{ /* Llib/os.scm 987 */
 obj_t BgL_list1704z00_1803;
BgL_list1704z00_1803 = 
MAKE_YOUNG_PAIR(BGl_string2739z00zz__osz00, BNIL); 
BgL_arg1702z00_1801 = 
BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
(int)(BgL_arg1703z00_1802), BgL_list1704z00_1803); } } 
BgL_arg1701z00_1800 = 
BGl_appendzd221011zd2zz__osz00(BgL_arg1702z00_1801, BgL_fz00_1790); } 
{ 
 obj_t BgL_fz00_6215;
BgL_fz00_6215 = BgL_arg1701z00_1800; 
BgL_fz00_1808 = BgL_fz00_6215; 
goto BgL_zc3z04anonymousza31708ze3z87_1809;} } } } 
if(
STRINGP(BgL_aux2574z00_4044))
{ /* Llib/os.scm 979 */
return BgL_aux2574z00_4044;}  else 
{ 
 obj_t BgL_auxz00_6218;
BgL_auxz00_6218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35743L), BGl_string2743z00zz__osz00, BGl_string2689z00zz__osz00, BgL_aux2574z00_4044); 
FAILURE(BgL_auxz00_6218,BFALSE,BFALSE);} } } }  else 
{ /* Llib/os.scm 977 */
return BgL_namez00_44;} } } } } 

}



/* &relative-file-name */
obj_t BGl_z62relativezd2filezd2namez62zz__osz00(obj_t BgL_envz00_3605, obj_t BgL_namez00_3606, obj_t BgL_basez00_3607)
{
{ /* Llib/os.scm 967 */
{ /* Llib/os.scm 970 */
 obj_t BgL_auxz00_6229; obj_t BgL_auxz00_6222;
if(
STRINGP(BgL_basez00_3607))
{ /* Llib/os.scm 970 */
BgL_auxz00_6229 = BgL_basez00_3607
; }  else 
{ 
 obj_t BgL_auxz00_6232;
BgL_auxz00_6232 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35531L), BGl_string2745z00zz__osz00, BGl_string2689z00zz__osz00, BgL_basez00_3607); 
FAILURE(BgL_auxz00_6232,BFALSE,BFALSE);} 
if(
STRINGP(BgL_namez00_3606))
{ /* Llib/os.scm 970 */
BgL_auxz00_6222 = BgL_namez00_3606
; }  else 
{ 
 obj_t BgL_auxz00_6225;
BgL_auxz00_6225 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(35531L), BGl_string2745z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3606); 
FAILURE(BgL_auxz00_6225,BFALSE,BFALSE);} 
return 
BGl_relativezd2filezd2namez00zz__osz00(BgL_auxz00_6222, BgL_auxz00_6229);} } 

}



/* make-static-library-name */
BGL_EXPORTED_DEF obj_t BGl_makezd2staticzd2libraryzd2namezd2zz__osz00(obj_t BgL_libnamez00_46)
{
{ /* Llib/os.scm 996 */
return 
string_append_3(BgL_libnamez00_46, BGl_string2664z00zz__osz00, 
string_to_bstring(STATIC_LIB_SUFFIX));} 

}



/* &make-static-library-name */
obj_t BGl_z62makezd2staticzd2libraryzd2namezb0zz__osz00(obj_t BgL_envz00_3608, obj_t BgL_libnamez00_3609)
{
{ /* Llib/os.scm 996 */
{ /* Llib/os.scm 997 */
 obj_t BgL_auxz00_6239;
if(
STRINGP(BgL_libnamez00_3609))
{ /* Llib/os.scm 997 */
BgL_auxz00_6239 = BgL_libnamez00_3609
; }  else 
{ 
 obj_t BgL_auxz00_6242;
BgL_auxz00_6242 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(36415L), BGl_string2746z00zz__osz00, BGl_string2689z00zz__osz00, BgL_libnamez00_3609); 
FAILURE(BgL_auxz00_6242,BFALSE,BFALSE);} 
return 
BGl_makezd2staticzd2libraryzd2namezd2zz__osz00(BgL_auxz00_6239);} } 

}



/* make-shared-library-name */
BGL_EXPORTED_DEF obj_t BGl_makezd2sharedzd2libraryzd2namezd2zz__osz00(obj_t BgL_libnamez00_47)
{
{ /* Llib/os.scm 1004 */
return 
string_append_3(BgL_libnamez00_47, BGl_string2664z00zz__osz00, 
string_to_bstring(SHARED_LIB_SUFFIX));} 

}



/* &make-shared-library-name */
obj_t BGl_z62makezd2sharedzd2libraryzd2namezb0zz__osz00(obj_t BgL_envz00_3610, obj_t BgL_libnamez00_3611)
{
{ /* Llib/os.scm 1004 */
{ /* Llib/os.scm 1005 */
 obj_t BgL_auxz00_6249;
if(
STRINGP(BgL_libnamez00_3611))
{ /* Llib/os.scm 1005 */
BgL_auxz00_6249 = BgL_libnamez00_3611
; }  else 
{ 
 obj_t BgL_auxz00_6252;
BgL_auxz00_6252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(36900L), BGl_string2747z00zz__osz00, BGl_string2689z00zz__osz00, BgL_libnamez00_3611); 
FAILURE(BgL_auxz00_6252,BFALSE,BFALSE);} 
return 
BGl_makezd2sharedzd2libraryzd2namezd2zz__osz00(BgL_auxz00_6249);} } 

}



/* sleep */
BGL_EXPORTED_DEF obj_t BGl_sleepz00zz__osz00(long BgL_msz00_48)
{
{ /* Llib/os.scm 1010 */
bgl_sleep(BgL_msz00_48); BUNSPEC; 
return 
BINT(BgL_msz00_48);} 

}



/* &sleep */
obj_t BGl_z62sleepz62zz__osz00(obj_t BgL_envz00_3612, obj_t BgL_msz00_3613)
{
{ /* Llib/os.scm 1010 */
{ /* Llib/os.scm 1011 */
 long BgL_auxz00_6259;
{ /* Llib/os.scm 1011 */
 obj_t BgL_tmpz00_6260;
if(
INTEGERP(BgL_msz00_3613))
{ /* Llib/os.scm 1011 */
BgL_tmpz00_6260 = BgL_msz00_3613
; }  else 
{ 
 obj_t BgL_auxz00_6263;
BgL_auxz00_6263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(37222L), BGl_string2748z00zz__osz00, BGl_string2679z00zz__osz00, BgL_msz00_3613); 
FAILURE(BgL_auxz00_6263,BFALSE,BFALSE);} 
BgL_auxz00_6259 = 
(long)CINT(BgL_tmpz00_6260); } 
return 
BGl_sleepz00zz__osz00(BgL_auxz00_6259);} } 

}



/* _dynamic-load */
obj_t BGl__dynamiczd2loadzd2zz__osz00(obj_t BgL_env1146z00_53, obj_t BgL_opt1145z00_52)
{
{ /* Llib/os.scm 1031 */
{ /* Llib/os.scm 1031 */
 obj_t BgL_g1147z00_1822;
BgL_g1147z00_1822 = 
VECTOR_REF(BgL_opt1145z00_52,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1145z00_52)) { case 1L : 

{ /* Llib/os.scm 1031 */

{ /* Llib/os.scm 1031 */
 obj_t BgL_auxz00_6270;
if(
STRINGP(BgL_g1147z00_1822))
{ /* Llib/os.scm 1031 */
BgL_auxz00_6270 = BgL_g1147z00_1822
; }  else 
{ 
 obj_t BgL_auxz00_6273;
BgL_auxz00_6273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(38281L), BGl_string2752z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1147z00_1822); 
FAILURE(BgL_auxz00_6273,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2loadzd2zz__osz00(BgL_auxz00_6270, 
string_to_bstring(BGL_DYNAMIC_LOAD_INIT), BFALSE);} } break;case 2L : 

{ /* Llib/os.scm 1031 */
 obj_t BgL_initz00_1827;
BgL_initz00_1827 = 
VECTOR_REF(BgL_opt1145z00_52,1L); 
{ /* Llib/os.scm 1031 */

{ /* Llib/os.scm 1031 */
 obj_t BgL_auxz00_6280;
if(
STRINGP(BgL_g1147z00_1822))
{ /* Llib/os.scm 1031 */
BgL_auxz00_6280 = BgL_g1147z00_1822
; }  else 
{ 
 obj_t BgL_auxz00_6283;
BgL_auxz00_6283 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(38281L), BGl_string2752z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1147z00_1822); 
FAILURE(BgL_auxz00_6283,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2loadzd2zz__osz00(BgL_auxz00_6280, BgL_initz00_1827, BFALSE);} } } break;case 3L : 

{ /* Llib/os.scm 1031 */
 obj_t BgL_initz00_1829;
BgL_initz00_1829 = 
VECTOR_REF(BgL_opt1145z00_52,1L); 
{ /* Llib/os.scm 1031 */
 obj_t BgL_modulez00_1830;
BgL_modulez00_1830 = 
VECTOR_REF(BgL_opt1145z00_52,2L); 
{ /* Llib/os.scm 1031 */

{ /* Llib/os.scm 1031 */
 obj_t BgL_auxz00_6290;
if(
STRINGP(BgL_g1147z00_1822))
{ /* Llib/os.scm 1031 */
BgL_auxz00_6290 = BgL_g1147z00_1822
; }  else 
{ 
 obj_t BgL_auxz00_6293;
BgL_auxz00_6293 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(38281L), BGl_string2752z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1147z00_1822); 
FAILURE(BgL_auxz00_6293,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2loadzd2zz__osz00(BgL_auxz00_6290, BgL_initz00_1829, BgL_modulez00_1830);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2749z00zz__osz00, BGl_string2751z00zz__osz00, 
BINT(
VECTOR_LENGTH(BgL_opt1145z00_52)));} } } } 

}



/* dynamic-load */
BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2zz__osz00(obj_t BgL_libz00_49, obj_t BgL_initz00_50, obj_t BgL_modulez00_51)
{
{ /* Llib/os.scm 1031 */
{ /* Llib/os.scm 1039 */
 obj_t BgL_flibz00_1834; obj_t BgL_modz00_1835;
BgL_flibz00_1834 = 
BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_49, BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00); 
if(
CBOOL(BgL_modulez00_51))
{ /* Llib/os.scm 1047 */
 obj_t BgL_arg1735z00_1856;
{ /* Llib/os.scm 1047 */
 obj_t BgL_symbolz00_3096;
if(
SYMBOLP(BgL_modulez00_51))
{ /* Llib/os.scm 1047 */
BgL_symbolz00_3096 = BgL_modulez00_51; }  else 
{ 
 obj_t BgL_auxz00_6308;
BgL_auxz00_6308 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(38811L), BGl_string2750z00zz__osz00, BGl_string2730z00zz__osz00, BgL_modulez00_51); 
FAILURE(BgL_auxz00_6308,BFALSE,BFALSE);} 
BgL_arg1735z00_1856 = 
SYMBOL_TO_STRING(BgL_symbolz00_3096); } 
BgL_modz00_1835 = 
bigloo_module_mangle(BGl_string2753z00zz__osz00, BgL_arg1735z00_1856); }  else 
{ /* Llib/os.scm 1043 */
BgL_modz00_1835 = BGl_string2702z00zz__osz00; } 
if(
STRINGP(BgL_flibz00_1834))
{ /* Llib/os.scm 1053 */
 obj_t BgL_iniz00_1837;
if(
CBOOL(BgL_initz00_50))
{ /* Llib/os.scm 1053 */
BgL_iniz00_1837 = BgL_initz00_50; }  else 
{ /* Llib/os.scm 1053 */
BgL_iniz00_1837 = BGl_string2702z00zz__osz00; } 
{ /* Llib/os.scm 1054 */
 obj_t BgL_valz00_1838;
{ /* Llib/os.scm 1055 */
 char * BgL_tmpz00_6318;
{ /* Llib/os.scm 1055 */
 obj_t BgL_tmpz00_6320;
if(
STRINGP(BgL_iniz00_1837))
{ /* Llib/os.scm 1055 */
BgL_tmpz00_6320 = BgL_iniz00_1837
; }  else 
{ 
 obj_t BgL_auxz00_6323;
BgL_auxz00_6323 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(39018L), BGl_string2750z00zz__osz00, BGl_string2689z00zz__osz00, BgL_iniz00_1837); 
FAILURE(BgL_auxz00_6323,BFALSE,BFALSE);} 
BgL_tmpz00_6318 = 
BSTRING_TO_STRING(BgL_tmpz00_6320); } 
BgL_valz00_1838 = 
bgl_dload(
BSTRING_TO_STRING(BgL_flibz00_1834), BgL_tmpz00_6318, 
BSTRING_TO_STRING(BgL_modz00_1835)); } 
{ /* Llib/os.scm 1055 */
 obj_t BgL_modz00_1839;
{ /* Llib/os.scm 1056 */
 obj_t BgL_tmpz00_3097;
{ /* Llib/os.scm 1056 */
 int BgL_tmpz00_6330;
BgL_tmpz00_6330 = 
(int)(1L); 
BgL_tmpz00_3097 = 
BGL_MVALUES_VAL(BgL_tmpz00_6330); } 
{ /* Llib/os.scm 1056 */
 int BgL_tmpz00_6333;
BgL_tmpz00_6333 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6333, BUNSPEC); } 
BgL_modz00_1839 = BgL_tmpz00_3097; } 
if(
(BgL_valz00_1838==BGl_symbol2754z00zz__osz00))
{ /* Llib/os.scm 1056 */
return 
BGl_errorz00zz__errorz00(BGl_string2756z00zz__osz00, BGl_string2757z00zz__osz00, BgL_flibz00_1834);}  else 
{ /* Llib/os.scm 1056 */
if(
(BgL_valz00_1838==BGl_symbol2758z00zz__osz00))
{ /* Llib/os.scm 1060 */
 char * BgL_arg1727z00_1843;
BgL_arg1727z00_1843 = 
bgl_dload_error(); 
{ /* Llib/os.scm 1034 */
 obj_t BgL_arg1737z00_3100;
BgL_arg1737z00_3100 = 
string_append(BGl_string2756z00zz__osz00, BgL_flibz00_1834); 
return 
BGl_errorz00zz__errorz00(BgL_arg1737z00_3100, 
string_to_bstring(BgL_arg1727z00_1843), BgL_flibz00_1834);} }  else 
{ /* Llib/os.scm 1056 */
if(
(BgL_valz00_1838==BGl_symbol2760z00zz__osz00))
{ /* Llib/os.scm 1063 */
 bool_t BgL_test3451z00_6347;
if(
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_initz00_50, 
string_to_bstring(BGL_DYNAMIC_LOAD_INIT)))
{ /* Llib/os.scm 1063 */
if(
CBOOL(BgL_modulez00_51))
{ /* Llib/os.scm 1063 */
BgL_test3451z00_6347 = ((bool_t)0)
; }  else 
{ /* Llib/os.scm 1063 */
BgL_test3451z00_6347 = ((bool_t)1)
; } }  else 
{ /* Llib/os.scm 1063 */
BgL_test3451z00_6347 = ((bool_t)0)
; } 
if(BgL_test3451z00_6347)
{ /* Llib/os.scm 1064 */
 obj_t BgL_arg1731z00_1847;
BgL_arg1731z00_1847 = 
string_append(BGl_string2762z00zz__osz00, BgL_flibz00_1834); 
{ /* Llib/os.scm 1064 */
 obj_t BgL_list1732z00_1848;
{ /* Llib/os.scm 1064 */
 obj_t BgL_arg1733z00_1849;
{ /* Llib/os.scm 1064 */
 obj_t BgL_arg1734z00_1850;
BgL_arg1734z00_1850 = 
MAKE_YOUNG_PAIR(BgL_initz00_50, BNIL); 
BgL_arg1733z00_1849 = 
MAKE_YOUNG_PAIR(BGl_string2763z00zz__osz00, BgL_arg1734z00_1850); } 
BgL_list1732z00_1848 = 
MAKE_YOUNG_PAIR(BgL_arg1731z00_1847, BgL_arg1733z00_1849); } 
return 
BGl_warningz00zz__errorz00(BgL_list1732z00_1848);} }  else 
{ /* Llib/os.scm 1063 */
if(
CBOOL(BgL_initz00_50))
{ /* Llib/os.scm 1067 */
return 
BGl_errorz00zz__errorz00(
string_append(BGl_string2756z00zz__osz00, BgL_flibz00_1834), BGl_string2764z00zz__osz00, BgL_initz00_50);}  else 
{ /* Llib/os.scm 1067 */
{ /* Llib/os.scm 1068 */
 int BgL_tmpz00_6362;
BgL_tmpz00_6362 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6362); } 
{ /* Llib/os.scm 1068 */
 int BgL_tmpz00_6365;
BgL_tmpz00_6365 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6365, BgL_modz00_1839); } 
return BUNSPEC;} } }  else 
{ /* Llib/os.scm 1056 */
{ /* Llib/os.scm 1074 */
 int BgL_tmpz00_6368;
BgL_tmpz00_6368 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6368); } 
{ /* Llib/os.scm 1074 */
 int BgL_tmpz00_6371;
BgL_tmpz00_6371 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6371, BgL_modz00_1839); } 
return BgL_valz00_1838;} } } } } }  else 
{ /* Llib/os.scm 1051 */
return 
BGl_errorz00zz__errorz00(BGl_string2756z00zz__osz00, BGl_string2765z00zz__osz00, BgL_libz00_49);} } } 

}



/* dynamic-unload */
BGL_EXPORTED_DEF obj_t BGl_dynamiczd2unloadzd2zz__osz00(obj_t BgL_libz00_54)
{
{ /* Llib/os.scm 1079 */
{ /* Llib/os.scm 1080 */
 obj_t BgL_flibz00_1867;
BgL_flibz00_1867 = 
BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_54, BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00); 
if(
STRINGP(BgL_flibz00_1867))
{ /* Llib/os.scm 1089 */
 int BgL_arg1740z00_1869;
BgL_arg1740z00_1869 = 
bgl_dunload(BgL_flibz00_1867); 
return 
BBOOL(
(
(long)(BgL_arg1740z00_1869)==0L));}  else 
{ /* Llib/os.scm 1087 */
return 
BGl_errorz00zz__errorz00(BGl_string2766z00zz__osz00, BGl_string2765z00zz__osz00, BgL_libz00_54);} } } 

}



/* &dynamic-unload */
obj_t BGl_z62dynamiczd2unloadzb0zz__osz00(obj_t BgL_envz00_3614, obj_t BgL_libz00_3615)
{
{ /* Llib/os.scm 1079 */
{ /* Llib/os.scm 1080 */
 obj_t BgL_auxz00_6383;
if(
STRINGP(BgL_libz00_3615))
{ /* Llib/os.scm 1080 */
BgL_auxz00_6383 = BgL_libz00_3615
; }  else 
{ 
 obj_t BgL_auxz00_6386;
BgL_auxz00_6386 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(39828L), BGl_string2767z00zz__osz00, BGl_string2689z00zz__osz00, BgL_libz00_3615); 
FAILURE(BgL_auxz00_6386,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2unloadzd2zz__osz00(BgL_auxz00_6383);} } 

}



/* _dynamic-load-symbol */
obj_t BGl__dynamiczd2loadzd2symbolz00zz__osz00(obj_t BgL_env1151z00_59, obj_t BgL_opt1150z00_58)
{
{ /* Llib/os.scm 1094 */
{ /* Llib/os.scm 1094 */
 obj_t BgL_g1152z00_1870; obj_t BgL_g1153z00_1871;
BgL_g1152z00_1870 = 
VECTOR_REF(BgL_opt1150z00_58,0L); 
BgL_g1153z00_1871 = 
VECTOR_REF(BgL_opt1150z00_58,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1150z00_58)) { case 2L : 

{ /* Llib/os.scm 1094 */

{ /* Llib/os.scm 1094 */
 obj_t BgL_libz00_3104; obj_t BgL_namez00_3105;
if(
STRINGP(BgL_g1152z00_1870))
{ /* Llib/os.scm 1094 */
BgL_libz00_3104 = BgL_g1152z00_1870; }  else 
{ 
 obj_t BgL_auxz00_6395;
BgL_auxz00_6395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40337L), BGl_string2771z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1152z00_1870); 
FAILURE(BgL_auxz00_6395,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1153z00_1871))
{ /* Llib/os.scm 1094 */
BgL_namez00_3105 = BgL_g1153z00_1871; }  else 
{ 
 obj_t BgL_auxz00_6401;
BgL_auxz00_6401 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40337L), BGl_string2771z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1153z00_1871); 
FAILURE(BgL_auxz00_6401,BFALSE,BFALSE);} 
{ /* Llib/os.scm 1095 */
 obj_t BgL_flibz00_3107;
BgL_flibz00_3107 = 
BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_3104, BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00); 
{ /* Llib/os.scm 1103 */
 obj_t BgL_tmpz00_6406;
if(
STRINGP(BgL_flibz00_3107))
{ /* Llib/os.scm 1103 */
BgL_tmpz00_6406 = BgL_flibz00_3107
; }  else 
{ 
 obj_t BgL_auxz00_6409;
BgL_auxz00_6409 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40664L), BGl_string2771z00zz__osz00, BGl_string2689z00zz__osz00, BgL_flibz00_3107); 
FAILURE(BgL_auxz00_6409,BFALSE,BFALSE);} 
return 
bgl_dlsym(BgL_tmpz00_6406, BgL_namez00_3105, BgL_namez00_3105);} } } } break;case 3L : 

{ /* Llib/os.scm 1094 */
 obj_t BgL_modulez00_1875;
BgL_modulez00_1875 = 
VECTOR_REF(BgL_opt1150z00_58,2L); 
{ /* Llib/os.scm 1094 */

{ /* Llib/os.scm 1094 */
 obj_t BgL_libz00_3109; obj_t BgL_namez00_3110;
if(
STRINGP(BgL_g1152z00_1870))
{ /* Llib/os.scm 1094 */
BgL_libz00_3109 = BgL_g1152z00_1870; }  else 
{ 
 obj_t BgL_auxz00_6417;
BgL_auxz00_6417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40337L), BGl_string2771z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1152z00_1870); 
FAILURE(BgL_auxz00_6417,BFALSE,BFALSE);} 
if(
STRINGP(BgL_g1153z00_1871))
{ /* Llib/os.scm 1094 */
BgL_namez00_3110 = BgL_g1153z00_1871; }  else 
{ 
 obj_t BgL_auxz00_6423;
BgL_auxz00_6423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40337L), BGl_string2771z00zz__osz00, BGl_string2689z00zz__osz00, BgL_g1153z00_1871); 
FAILURE(BgL_auxz00_6423,BFALSE,BFALSE);} 
{ /* Llib/os.scm 1095 */
 obj_t BgL_symz00_3111; obj_t BgL_flibz00_3112;
if(
STRINGP(BgL_modulez00_1875))
{ /* Llib/os.scm 1095 */
BgL_symz00_3111 = 
bigloo_module_mangle(BgL_namez00_3110, BgL_modulez00_1875); }  else 
{ /* Llib/os.scm 1095 */
BgL_symz00_3111 = BgL_namez00_3110; } 
BgL_flibz00_3112 = 
BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_3109, BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00); 
{ /* Llib/os.scm 1103 */
 obj_t BgL_tmpz00_6431;
if(
STRINGP(BgL_flibz00_3112))
{ /* Llib/os.scm 1103 */
BgL_tmpz00_6431 = BgL_flibz00_3112
; }  else 
{ 
 obj_t BgL_auxz00_6434;
BgL_auxz00_6434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40664L), BGl_string2771z00zz__osz00, BGl_string2689z00zz__osz00, BgL_flibz00_3112); 
FAILURE(BgL_auxz00_6434,BFALSE,BFALSE);} 
return 
bgl_dlsym(BgL_tmpz00_6431, BgL_namez00_3110, BgL_symz00_3111);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2768z00zz__osz00, BGl_string2770z00zz__osz00, 
BINT(
VECTOR_LENGTH(BgL_opt1150z00_58)));} } } } 

}



/* dynamic-load-symbol */
BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2symbolz00zz__osz00(obj_t BgL_libz00_55, obj_t BgL_namez00_56, obj_t BgL_modulez00_57)
{
{ /* Llib/os.scm 1094 */
{ /* Llib/os.scm 1095 */
 obj_t BgL_symz00_3114; obj_t BgL_flibz00_3115;
if(
STRINGP(BgL_modulez00_57))
{ /* Llib/os.scm 1095 */
BgL_symz00_3114 = 
bigloo_module_mangle(BgL_namez00_56, BgL_modulez00_57); }  else 
{ /* Llib/os.scm 1095 */
BgL_symz00_3114 = BgL_namez00_56; } 
BgL_flibz00_3115 = 
BGl_findzd2filezf2pathz20zz__osz00(BgL_libz00_55, BGl_za2dynamiczd2loadzd2pathza2z00zz__osz00); 
{ /* Llib/os.scm 1103 */
 obj_t BgL_tmpz00_6448;
if(
STRINGP(BgL_flibz00_3115))
{ /* Llib/os.scm 1103 */
BgL_tmpz00_6448 = BgL_flibz00_3115
; }  else 
{ 
 obj_t BgL_auxz00_6451;
BgL_auxz00_6451 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40664L), BGl_string2769z00zz__osz00, BGl_string2689z00zz__osz00, BgL_flibz00_3115); 
FAILURE(BgL_auxz00_6451,BFALSE,BFALSE);} 
return 
bgl_dlsym(BgL_tmpz00_6448, BgL_namez00_56, BgL_symz00_3114);} } } 

}



/* dynamic-load-symbol-get */
BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2symbolzd2getzd2zz__osz00(obj_t BgL_symz00_60)
{
{ /* Llib/os.scm 1108 */
BGL_TAIL return 
bgl_dlsym_get(BgL_symz00_60);} 

}



/* &dynamic-load-symbol-get */
obj_t BGl_z62dynamiczd2loadzd2symbolzd2getzb0zz__osz00(obj_t BgL_envz00_3616, obj_t BgL_symz00_3617)
{
{ /* Llib/os.scm 1108 */
{ /* Llib/os.scm 1109 */
 obj_t BgL_auxz00_6457;
if(
CUSTOMP(BgL_symz00_3617))
{ /* Llib/os.scm 1109 */
BgL_auxz00_6457 = BgL_symz00_3617
; }  else 
{ 
 obj_t BgL_auxz00_6460;
BgL_auxz00_6460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(40960L), BGl_string2772z00zz__osz00, BGl_string2773z00zz__osz00, BgL_symz00_3617); 
FAILURE(BgL_auxz00_6460,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2loadzd2symbolzd2getzd2zz__osz00(BgL_auxz00_6457);} } 

}



/* dynamic-load-symbol-set */
BGL_EXPORTED_DEF obj_t BGl_dynamiczd2loadzd2symbolzd2setzd2zz__osz00(obj_t BgL_symz00_61, obj_t BgL_valz00_62)
{
{ /* Llib/os.scm 1114 */
BGL_TAIL return 
bgl_dlsym_set(BgL_symz00_61, BgL_valz00_62);} 

}



/* &dynamic-load-symbol-set */
obj_t BGl_z62dynamiczd2loadzd2symbolzd2setzb0zz__osz00(obj_t BgL_envz00_3618, obj_t BgL_symz00_3619, obj_t BgL_valz00_3620)
{
{ /* Llib/os.scm 1114 */
{ /* Llib/os.scm 1115 */
 obj_t BgL_auxz00_6466;
if(
CUSTOMP(BgL_symz00_3619))
{ /* Llib/os.scm 1115 */
BgL_auxz00_6466 = BgL_symz00_3619
; }  else 
{ 
 obj_t BgL_auxz00_6469;
BgL_auxz00_6469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(41270L), BGl_string2774z00zz__osz00, BGl_string2773z00zz__osz00, BgL_symz00_3619); 
FAILURE(BgL_auxz00_6469,BFALSE,BFALSE);} 
return 
BGl_dynamiczd2loadzd2symbolzd2setzd2zz__osz00(BgL_auxz00_6466, BgL_valz00_3620);} } 

}



/* unix-path->list */
BGL_EXPORTED_DEF obj_t BGl_unixzd2pathzd2ze3listze3zz__osz00(obj_t BgL_strz00_63)
{
{ /* Llib/os.scm 1120 */
{ /* Llib/os.scm 1121 */
 long BgL_stopz00_1880;
BgL_stopz00_1880 = 
STRING_LENGTH(BgL_strz00_63); 
{ 
 long BgL_markz00_1884; long BgL_rz00_1885; obj_t BgL_resz00_1886;
BgL_markz00_1884 = 0L; 
BgL_rz00_1885 = 0L; 
BgL_resz00_1886 = BNIL; 
BgL_zc3z04anonymousza31743ze3z87_1887:
if(
(BgL_stopz00_1880==BgL_rz00_1885))
{ /* Llib/os.scm 1128 */
 obj_t BgL_resz00_1889;
if(
(BgL_markz00_1884<BgL_rz00_1885))
{ /* Llib/os.scm 1128 */
BgL_resz00_1889 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_63, BgL_markz00_1884, BgL_rz00_1885), BgL_resz00_1886); }  else 
{ /* Llib/os.scm 1128 */
BgL_resz00_1889 = BgL_resz00_1886; } 
return 
bgl_reverse_bang(BgL_resz00_1889);}  else 
{ /* Llib/os.scm 1132 */
 bool_t BgL_test3470z00_6482;
{ /* Llib/os.scm 1132 */
 unsigned char BgL_char2z00_3125;
BgL_char2z00_3125 = 
(unsigned char)(PATH_SEPARATOR); 
{ /* Llib/os.scm 1132 */
 unsigned char BgL_tmpz00_6484;
{ /* Llib/os.scm 1132 */
 long BgL_l2413z00_3883;
BgL_l2413z00_3883 = 
STRING_LENGTH(BgL_strz00_63); 
if(
BOUND_CHECK(BgL_rz00_1885, BgL_l2413z00_3883))
{ /* Llib/os.scm 1132 */
BgL_tmpz00_6484 = 
STRING_REF(BgL_strz00_63, BgL_rz00_1885)
; }  else 
{ 
 obj_t BgL_auxz00_6489;
BgL_auxz00_6489 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(41809L), BGl_string2696z00zz__osz00, BgL_strz00_63, 
(int)(BgL_l2413z00_3883), 
(int)(BgL_rz00_1885)); 
FAILURE(BgL_auxz00_6489,BFALSE,BFALSE);} } 
BgL_test3470z00_6482 = 
(BgL_tmpz00_6484==BgL_char2z00_3125); } } 
if(BgL_test3470z00_6482)
{ /* Llib/os.scm 1132 */
if(
(BgL_markz00_1884<BgL_rz00_1885))
{ /* Llib/os.scm 1134 */
 long BgL_arg1750z00_1895; long BgL_arg1751z00_1896; obj_t BgL_arg1752z00_1897;
BgL_arg1750z00_1895 = 
(1L+BgL_rz00_1885); 
BgL_arg1751z00_1896 = 
(1L+BgL_rz00_1885); 
BgL_arg1752z00_1897 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_63, BgL_markz00_1884, BgL_rz00_1885), BgL_resz00_1886); 
{ 
 obj_t BgL_resz00_6504; long BgL_rz00_6503; long BgL_markz00_6502;
BgL_markz00_6502 = BgL_arg1750z00_1895; 
BgL_rz00_6503 = BgL_arg1751z00_1896; 
BgL_resz00_6504 = BgL_arg1752z00_1897; 
BgL_resz00_1886 = BgL_resz00_6504; 
BgL_rz00_1885 = BgL_rz00_6503; 
BgL_markz00_1884 = BgL_markz00_6502; 
goto BgL_zc3z04anonymousza31743ze3z87_1887;} }  else 
{ 
 long BgL_rz00_6507; long BgL_markz00_6505;
BgL_markz00_6505 = 
(1L+BgL_rz00_1885); 
BgL_rz00_6507 = 
(1L+BgL_rz00_1885); 
BgL_rz00_1885 = BgL_rz00_6507; 
BgL_markz00_1884 = BgL_markz00_6505; 
goto BgL_zc3z04anonymousza31743ze3z87_1887;} }  else 
{ 
 long BgL_rz00_6509;
BgL_rz00_6509 = 
(1L+BgL_rz00_1885); 
BgL_rz00_1885 = BgL_rz00_6509; 
goto BgL_zc3z04anonymousza31743ze3z87_1887;} } } } } 

}



/* &unix-path->list */
obj_t BGl_z62unixzd2pathzd2ze3listz81zz__osz00(obj_t BgL_envz00_3621, obj_t BgL_strz00_3622)
{
{ /* Llib/os.scm 1120 */
{ /* Llib/os.scm 1121 */
 obj_t BgL_auxz00_6511;
if(
STRINGP(BgL_strz00_3622))
{ /* Llib/os.scm 1121 */
BgL_auxz00_6511 = BgL_strz00_3622
; }  else 
{ 
 obj_t BgL_auxz00_6514;
BgL_auxz00_6514 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(41552L), BGl_string2775z00zz__osz00, BGl_string2689z00zz__osz00, BgL_strz00_3622); 
FAILURE(BgL_auxz00_6514,BFALSE,BFALSE);} 
return 
BGl_unixzd2pathzd2ze3listze3zz__osz00(BgL_auxz00_6511);} } 

}



/* getuid */
BGL_EXPORTED_DEF int BGl_getuidz00zz__osz00(void)
{
{ /* Llib/os.scm 1142 */
BGL_TAIL return 
bgl_getuid();} 

}



/* &getuid */
obj_t BGl_z62getuidz62zz__osz00(obj_t BgL_envz00_3623)
{
{ /* Llib/os.scm 1142 */
return 
BINT(
BGl_getuidz00zz__osz00());} 

}



/* setuid */
BGL_EXPORTED_DEF obj_t BGl_setuidz00zz__osz00(int BgL_uidz00_64)
{
{ /* Llib/os.scm 1152 */
return 
BINT(
bgl_setuid(BgL_uidz00_64));} 

}



/* &setuid */
obj_t BGl_z62setuidz62zz__osz00(obj_t BgL_envz00_3624, obj_t BgL_uidz00_3625)
{
{ /* Llib/os.scm 1152 */
{ /* Llib/os.scm 1155 */
 int BgL_auxz00_6524;
{ /* Llib/os.scm 1155 */
 obj_t BgL_tmpz00_6525;
if(
INTEGERP(BgL_uidz00_3625))
{ /* Llib/os.scm 1155 */
BgL_tmpz00_6525 = BgL_uidz00_3625
; }  else 
{ 
 obj_t BgL_auxz00_6528;
BgL_auxz00_6528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(42607L), BGl_string2776z00zz__osz00, BGl_string2679z00zz__osz00, BgL_uidz00_3625); 
FAILURE(BgL_auxz00_6528,BFALSE,BFALSE);} 
BgL_auxz00_6524 = 
CINT(BgL_tmpz00_6525); } 
return 
BGl_setuidz00zz__osz00(BgL_auxz00_6524);} } 

}



/* getgid */
BGL_EXPORTED_DEF int BGl_getgidz00zz__osz00(void)
{
{ /* Llib/os.scm 1162 */
BGL_TAIL return 
bgl_getgid();} 

}



/* &getgid */
obj_t BGl_z62getgidz62zz__osz00(obj_t BgL_envz00_3626)
{
{ /* Llib/os.scm 1162 */
return 
BINT(
BGl_getgidz00zz__osz00());} 

}



/* setgid */
BGL_EXPORTED_DEF obj_t BGl_setgidz00zz__osz00(int BgL_uidz00_65)
{
{ /* Llib/os.scm 1172 */
return 
BINT(
bgl_setgid(BgL_uidz00_65));} 

}



/* &setgid */
obj_t BGl_z62setgidz62zz__osz00(obj_t BgL_envz00_3627, obj_t BgL_uidz00_3628)
{
{ /* Llib/os.scm 1172 */
{ /* Llib/os.scm 1175 */
 int BgL_auxz00_6539;
{ /* Llib/os.scm 1175 */
 obj_t BgL_tmpz00_6540;
if(
INTEGERP(BgL_uidz00_3628))
{ /* Llib/os.scm 1175 */
BgL_tmpz00_6540 = BgL_uidz00_3628
; }  else 
{ 
 obj_t BgL_auxz00_6543;
BgL_auxz00_6543 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(43288L), BGl_string2777z00zz__osz00, BGl_string2679z00zz__osz00, BgL_uidz00_3628); 
FAILURE(BgL_auxz00_6543,BFALSE,BFALSE);} 
BgL_auxz00_6539 = 
CINT(BgL_tmpz00_6540); } 
return 
BGl_setgidz00zz__osz00(BgL_auxz00_6539);} } 

}



/* getpwnam */
BGL_EXPORTED_DEF obj_t BGl_getpwnamz00zz__osz00(obj_t BgL_namez00_66)
{
{ /* Llib/os.scm 1182 */
return 
bgl_getpwnam(
BSTRING_TO_STRING(BgL_namez00_66));} 

}



/* &getpwnam */
obj_t BGl_z62getpwnamz62zz__osz00(obj_t BgL_envz00_3629, obj_t BgL_namez00_3630)
{
{ /* Llib/os.scm 1182 */
{ /* Llib/os.scm 1185 */
 obj_t BgL_auxz00_6551;
if(
STRINGP(BgL_namez00_3630))
{ /* Llib/os.scm 1185 */
BgL_auxz00_6551 = BgL_namez00_3630
; }  else 
{ 
 obj_t BgL_auxz00_6554;
BgL_auxz00_6554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(43658L), BGl_string2778z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3630); 
FAILURE(BgL_auxz00_6554,BFALSE,BFALSE);} 
return 
BGl_getpwnamz00zz__osz00(BgL_auxz00_6551);} } 

}



/* getpwuid */
BGL_EXPORTED_DEF obj_t BGl_getpwuidz00zz__osz00(int BgL_uidz00_67)
{
{ /* Llib/os.scm 1192 */
BGL_TAIL return 
bgl_getpwuid(BgL_uidz00_67);} 

}



/* &getpwuid */
obj_t BGl_z62getpwuidz62zz__osz00(obj_t BgL_envz00_3631, obj_t BgL_uidz00_3632)
{
{ /* Llib/os.scm 1192 */
{ /* Llib/os.scm 1195 */
 int BgL_auxz00_6560;
{ /* Llib/os.scm 1195 */
 obj_t BgL_tmpz00_6561;
if(
INTEGERP(BgL_uidz00_3632))
{ /* Llib/os.scm 1195 */
BgL_tmpz00_6561 = BgL_uidz00_3632
; }  else 
{ 
 obj_t BgL_auxz00_6564;
BgL_auxz00_6564 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(43986L), BGl_string2779z00zz__osz00, BGl_string2679z00zz__osz00, BgL_uidz00_3632); 
FAILURE(BgL_auxz00_6564,BFALSE,BFALSE);} 
BgL_auxz00_6560 = 
CINT(BgL_tmpz00_6561); } 
return 
BGl_getpwuidz00zz__osz00(BgL_auxz00_6560);} } 

}



/* getpid */
BGL_EXPORTED_DEF int BGl_getpidz00zz__osz00(void)
{
{ /* Llib/os.scm 1201 */
return 
getpid();} 

}



/* &getpid */
obj_t BGl_z62getpidz62zz__osz00(obj_t BgL_envz00_3633)
{
{ /* Llib/os.scm 1201 */
return 
BINT(
BGl_getpidz00zz__osz00());} 

}



/* getppid */
BGL_EXPORTED_DEF int BGl_getppidz00zz__osz00(void)
{
{ /* Llib/os.scm 1209 */
return 
getppid();} 

}



/* &getppid */
obj_t BGl_z62getppidz62zz__osz00(obj_t BgL_envz00_3634)
{
{ /* Llib/os.scm 1209 */
return 
BINT(
BGl_getppidz00zz__osz00());} 

}



/* getgroups */
BGL_EXPORTED_DEF obj_t BGl_getgroupsz00zz__osz00(void)
{
{ /* Llib/os.scm 1217 */
BGL_TAIL return 
bgl_getgroups();} 

}



/* &getgroups */
obj_t BGl_z62getgroupsz62zz__osz00(obj_t BgL_envz00_3635)
{
{ /* Llib/os.scm 1217 */
return 
BGl_getgroupsz00zz__osz00();} 

}



/* ioctl-register-request! */
BGL_EXPORTED_DEF obj_t BGl_ioctlzd2registerzd2requestz12z12zz__osz00(obj_t BgL_namez00_68, uint64_t BgL_valz00_69)
{
{ /* Llib/os.scm 1231 */
{ /* Llib/os.scm 1232 */
 obj_t BgL_arg1758z00_3133;
BgL_arg1758z00_3133 = 
MAKE_YOUNG_PAIR(BgL_namez00_68, 
BGL_UINT64_TO_BUINT64(BgL_valz00_69)); 
return ( 
BGl_ioctlzd2requestszd2tablez00zz__osz00 = 
MAKE_YOUNG_PAIR(BgL_arg1758z00_3133, BGl_ioctlzd2requestszd2tablez00zz__osz00), BUNSPEC) ;} } 

}



/* &ioctl-register-request! */
obj_t BGl_z62ioctlzd2registerzd2requestz12z70zz__osz00(obj_t BgL_envz00_3636, obj_t BgL_namez00_3637, obj_t BgL_valz00_3638)
{
{ /* Llib/os.scm 1231 */
{ /* Llib/os.scm 1232 */
 uint64_t BgL_auxz00_6588; obj_t BgL_auxz00_6581;
{ /* Llib/os.scm 1232 */
 obj_t BgL_tmpz00_6589;
if(
BGL_UINT64P(BgL_valz00_3638))
{ /* Llib/os.scm 1232 */
BgL_tmpz00_6589 = BgL_valz00_3638
; }  else 
{ 
 obj_t BgL_auxz00_6592;
BgL_auxz00_6592 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(45505L), BGl_string2780z00zz__osz00, BGl_string2781z00zz__osz00, BgL_valz00_3638); 
FAILURE(BgL_auxz00_6592,BFALSE,BFALSE);} 
BgL_auxz00_6588 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_6589); } 
if(
STRINGP(BgL_namez00_3637))
{ /* Llib/os.scm 1232 */
BgL_auxz00_6581 = BgL_namez00_3637
; }  else 
{ 
 obj_t BgL_auxz00_6584;
BgL_auxz00_6584 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(45505L), BGl_string2780z00zz__osz00, BGl_string2689z00zz__osz00, BgL_namez00_3637); 
FAILURE(BgL_auxz00_6584,BFALSE,BFALSE);} 
return 
BGl_ioctlzd2registerzd2requestz12z12zz__osz00(BgL_auxz00_6581, BgL_auxz00_6588);} } 

}



/* request->elong */
long BGl_requestzd2ze3elongz31zz__osz00(obj_t BgL_reqz00_70)
{
{ /* Llib/os.scm 1237 */
{ 
 obj_t BgL_objz00_1906;
{ /* Llib/os.scm 1238 */
 obj_t BgL_tmpz00_6598;
{ /* Llib/os.scm 1238 */
 obj_t BgL_aux2628z00_4098;
BgL_objz00_1906 = BgL_reqz00_70; 
BgL_zc3z04anonymousza31759ze3z87_1907:
if(
ELONGP(BgL_objz00_1906))
{ /* Llib/os.scm 1240 */
BgL_aux2628z00_4098 = BgL_objz00_1906; }  else 
{ /* Llib/os.scm 1240 */
if(
INTEGERP(BgL_objz00_1906))
{ /* Llib/os.scm 1243 */
 long BgL_xz00_3134;
BgL_xz00_3134 = 
(long)CINT(BgL_objz00_1906); 
{ /* Llib/os.scm 1243 */
 long BgL_tmpz00_6604;
BgL_tmpz00_6604 = 
(long)(BgL_xz00_3134); 
BgL_aux2628z00_4098 = 
make_belong(BgL_tmpz00_6604); } }  else 
{ /* Llib/os.scm 1242 */
if(
REALP(BgL_objz00_1906))
{ /* Llib/os.scm 1245 */
 long BgL_res2196z00_3136;
{ /* Llib/os.scm 1245 */
 double BgL_xz00_3135;
BgL_xz00_3135 = 
REAL_TO_DOUBLE(BgL_objz00_1906); 
{ /* Llib/os.scm 1245 */
 long BgL_tmpz00_6610;
BgL_tmpz00_6610 = 
(long)(BgL_xz00_3135); 
BgL_res2196z00_3136 = 
(long)(BgL_tmpz00_6610); } } 
BgL_aux2628z00_4098 = 
make_belong(BgL_res2196z00_3136); }  else 
{ /* Llib/os.scm 1244 */
if(
STRINGP(BgL_objz00_1906))
{ /* Llib/os.scm 1247 */
 obj_t BgL_cellz00_1912;
BgL_cellz00_1912 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_1906, BGl_ioctlzd2requestszd2tablez00zz__osz00); 
if(
PAIRP(BgL_cellz00_1912))
{ /* Llib/os.scm 1248 */
BgL_aux2628z00_4098 = 
CDR(BgL_cellz00_1912); }  else 
{ /* Llib/os.scm 1250 */
 obj_t BgL_arg1765z00_1914;
{ /* Ieee/number.scm 175 */

BgL_arg1765z00_1914 = 
BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(BgL_objz00_1906, 
BINT(10L)); } 
{ 
 obj_t BgL_objz00_6622;
BgL_objz00_6622 = BgL_arg1765z00_1914; 
BgL_objz00_1906 = BgL_objz00_6622; 
goto BgL_zc3z04anonymousza31759ze3z87_1907;} } }  else 
{ /* Llib/os.scm 1246 */
if(
BIGNUMP(BgL_objz00_1906))
{ /* Llib/os.scm 1252 */
 long BgL_tmpz00_6625;
BgL_tmpz00_6625 = 
bgl_bignum_to_long(BgL_objz00_1906); 
BgL_aux2628z00_4098 = 
make_belong(BgL_tmpz00_6625); }  else 
{ /* Llib/os.scm 1251 */
BgL_aux2628z00_4098 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string2782z00zz__osz00, BGl_string2783z00zz__osz00, BgL_reqz00_70); } } } } } 
if(
ELONGP(BgL_aux2628z00_4098))
{ /* Llib/os.scm 1238 */
BgL_tmpz00_6598 = BgL_aux2628z00_4098
; }  else 
{ 
 obj_t BgL_auxz00_6631;
BgL_auxz00_6631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(45840L), BGl_string2784z00zz__osz00, BGl_string2785z00zz__osz00, BgL_aux2628z00_4098); 
FAILURE(BgL_auxz00_6631,BFALSE,BFALSE);} } 
return 
BELONG_TO_LONG(BgL_tmpz00_6598);} } } 

}



/* ioctl */
BGL_EXPORTED_DEF bool_t BGl_ioctlz00zz__osz00(obj_t BgL_devz00_71, obj_t BgL_requestz00_72, obj_t BgL_valz00_73)
{
{ /* Llib/os.scm 1259 */
{ 
 obj_t BgL_nz00_1922;
{ /* Llib/os.scm 1271 */
 long BgL_arg1768z00_1920; obj_t BgL_arg1769z00_1921;
BgL_arg1768z00_1920 = 
BGl_requestzd2ze3elongz31zz__osz00(BgL_requestz00_72); 
BgL_nz00_1922 = BgL_valz00_73; 
BgL_lambda1770z00_1923:
if(
ELONGP(BgL_nz00_1922))
{ /* Llib/os.scm 1263 */
BgL_arg1769z00_1921 = BgL_nz00_1922; }  else 
{ /* Llib/os.scm 1263 */
if(
INTEGERP(BgL_nz00_1922))
{ /* Llib/os.scm 1264 */
 long BgL_xz00_3139;
BgL_xz00_3139 = 
(long)CINT(BgL_nz00_1922); 
{ /* Llib/os.scm 1264 */
 long BgL_tmpz00_6642;
BgL_tmpz00_6642 = 
(long)(BgL_xz00_3139); 
BgL_arg1769z00_1921 = 
make_belong(BgL_tmpz00_6642); } }  else 
{ /* Llib/os.scm 1264 */
if(
BIGNUMP(BgL_nz00_1922))
{ /* Llib/os.scm 1265 */
 long BgL_tmpz00_6647;
BgL_tmpz00_6647 = 
bgl_bignum_to_long(BgL_nz00_1922); 
BgL_arg1769z00_1921 = 
make_belong(BgL_tmpz00_6647); }  else 
{ /* Llib/os.scm 1265 */
if(
STRINGP(BgL_nz00_1922))
{ /* Ieee/fixnum.scm 1007 */

{ /* Ieee/fixnum.scm 1007 */
 long BgL_tmpz00_6652;
BgL_tmpz00_6652 = 
BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00(BgL_nz00_1922, 10L); 
BgL_arg1769z00_1921 = 
make_belong(BgL_tmpz00_6652); } }  else 
{ /* Llib/os.scm 1267 */
 bool_t BgL_test3491z00_6655;
if(
INTEGERP(BgL_nz00_1922))
{ /* Llib/os.scm 1267 */
BgL_test3491z00_6655 = ((bool_t)1)
; }  else 
{ /* Llib/os.scm 1267 */
BgL_test3491z00_6655 = 
REALP(BgL_nz00_1922)
; } 
if(BgL_test3491z00_6655)
{ /* Llib/os.scm 1267 */
 long BgL_arg1777z00_1931;
{ /* Llib/os.scm 1267 */
 double BgL_xz00_3141;
{ /* Llib/os.scm 1267 */
 obj_t BgL_tmpz00_6659;
if(
REALP(BgL_nz00_1922))
{ /* Llib/os.scm 1267 */
BgL_tmpz00_6659 = BgL_nz00_1922
; }  else 
{ 
 obj_t BgL_auxz00_6662;
BgL_auxz00_6662 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(46672L), BGl_string2786z00zz__osz00, BGl_string2787z00zz__osz00, BgL_nz00_1922); 
FAILURE(BgL_auxz00_6662,BFALSE,BFALSE);} 
BgL_xz00_3141 = 
REAL_TO_DOUBLE(BgL_tmpz00_6659); } 
BgL_arg1777z00_1931 = 
(long)(BgL_xz00_3141); } 
{ 
 obj_t BgL_nz00_6668;
BgL_nz00_6668 = 
BINT(BgL_arg1777z00_1931); 
BgL_nz00_1922 = BgL_nz00_6668; 
goto BgL_lambda1770z00_1923;} }  else 
{ /* Llib/os.scm 1267 */
BgL_arg1769z00_1921 = 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string2782z00zz__osz00, BGl_string2788z00zz__osz00, BgL_nz00_1922); } } } } } 
{ /* Llib/os.scm 1271 */
 long BgL_tmpz00_6671;
{ /* Llib/os.scm 1271 */
 obj_t BgL_tmpz00_6672;
if(
ELONGP(BgL_arg1769z00_1921))
{ /* Llib/os.scm 1271 */
BgL_tmpz00_6672 = BgL_arg1769z00_1921
; }  else 
{ 
 obj_t BgL_auxz00_6675;
BgL_auxz00_6675 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(46832L), BGl_string2782z00zz__osz00, BGl_string2785z00zz__osz00, BgL_arg1769z00_1921); 
FAILURE(BgL_auxz00_6675,BFALSE,BFALSE);} 
BgL_tmpz00_6671 = 
BELONG_TO_LONG(BgL_tmpz00_6672); } 
return 
bgl_ioctl(BgL_devz00_71, BgL_arg1768z00_1920, BgL_tmpz00_6671);} } } } 

}



/* &ioctl */
obj_t BGl_z62ioctlz62zz__osz00(obj_t BgL_envz00_3639, obj_t BgL_devz00_3640, obj_t BgL_requestz00_3641, obj_t BgL_valz00_3642)
{
{ /* Llib/os.scm 1259 */
return 
BBOOL(
BGl_ioctlz00zz__osz00(BgL_devz00_3640, BgL_requestz00_3641, BgL_valz00_3642));} 

}



/* _umask */
obj_t BGl__umaskz00zz__osz00(obj_t BgL_env1157z00_76, obj_t BgL_opt1156z00_75)
{
{ /* Llib/os.scm 1277 */
{ /* Llib/os.scm 1277 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1156z00_75)) { case 0L : 

{ /* Llib/os.scm 1277 */

{ /* Llib/os.scm 1277 */
 int BgL_res2197z00_3144;
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/os.scm 1279 */
 long BgL_tmpz00_6685;
{ /* Llib/os.scm 1279 */
 long BgL_tmpz00_6686;
{ /* Llib/os.scm 1277 */
 obj_t BgL_aux2634z00_4104;
BgL_aux2634z00_4104 = BFALSE; 
{ 
 obj_t BgL_auxz00_6687;
BgL_auxz00_6687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(47121L), BGl_string2791z00zz__osz00, BGl_string2792z00zz__osz00, BgL_aux2634z00_4104); 
FAILURE(BgL_auxz00_6687,BFALSE,BFALSE);} } 
BgL_tmpz00_6686 = 0L; 
BgL_tmpz00_6685 = 
umask(BgL_tmpz00_6686); } 
BgL_res2197z00_3144 = 
(int)(BgL_tmpz00_6685); }  else 
{ /* Llib/os.scm 1280 */
 long BgL_oldz00_3143;
BgL_oldz00_3143 = 
umask(0L); 
umask(BgL_oldz00_3143); 
BgL_res2197z00_3144 = 
(int)(BgL_oldz00_3143); } 
return 
BINT(BgL_res2197z00_3144);} } break;case 1L : 

{ /* Llib/os.scm 1277 */
 obj_t BgL_maskz00_1936;
BgL_maskz00_1936 = 
VECTOR_REF(BgL_opt1156z00_75,0L); 
{ /* Llib/os.scm 1277 */

{ /* Llib/os.scm 1277 */
 int BgL_res2198z00_3147;
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_maskz00_1936))
{ /* Llib/os.scm 1279 */
 long BgL_tmpz00_6700;
{ /* Llib/os.scm 1279 */
 long BgL_tmpz00_6701;
{ /* Llib/os.scm 1279 */
 obj_t BgL_tmpz00_6702;
if(
INTEGERP(BgL_maskz00_1936))
{ /* Llib/os.scm 1279 */
BgL_tmpz00_6702 = BgL_maskz00_1936
; }  else 
{ 
 obj_t BgL_auxz00_6705;
BgL_auxz00_6705 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(47191L), BGl_string2791z00zz__osz00, BGl_string2679z00zz__osz00, BgL_maskz00_1936); 
FAILURE(BgL_auxz00_6705,BFALSE,BFALSE);} 
BgL_tmpz00_6701 = 
(long)CINT(BgL_tmpz00_6702); } 
BgL_tmpz00_6700 = 
umask(BgL_tmpz00_6701); } 
BgL_res2198z00_3147 = 
(int)(BgL_tmpz00_6700); }  else 
{ /* Llib/os.scm 1280 */
 long BgL_oldz00_3146;
BgL_oldz00_3146 = 
umask(0L); 
umask(BgL_oldz00_3146); 
BgL_res2198z00_3147 = 
(int)(BgL_oldz00_3146); } 
return 
BINT(BgL_res2198z00_3147);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2789z00zz__osz00, BGl_string2684z00zz__osz00, 
BINT(
VECTOR_LENGTH(BgL_opt1156z00_75)));} } } } 

}



/* umask */
BGL_EXPORTED_DEF int BGl_umaskz00zz__osz00(obj_t BgL_maskz00_74)
{
{ /* Llib/os.scm 1277 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_maskz00_74))
{ /* Llib/os.scm 1279 */
 long BgL_tmpz00_6723;
{ /* Llib/os.scm 1279 */
 long BgL_tmpz00_6724;
{ /* Llib/os.scm 1279 */
 obj_t BgL_tmpz00_6725;
if(
INTEGERP(BgL_maskz00_74))
{ /* Llib/os.scm 1279 */
BgL_tmpz00_6725 = BgL_maskz00_74
; }  else 
{ 
 obj_t BgL_auxz00_6728;
BgL_auxz00_6728 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(47191L), BGl_string2790z00zz__osz00, BGl_string2679z00zz__osz00, BgL_maskz00_74); 
FAILURE(BgL_auxz00_6728,BFALSE,BFALSE);} 
BgL_tmpz00_6724 = 
(long)CINT(BgL_tmpz00_6725); } 
BgL_tmpz00_6723 = 
umask(BgL_tmpz00_6724); } 
return 
(int)(BgL_tmpz00_6723);}  else 
{ /* Llib/os.scm 1280 */
 long BgL_oldz00_3149;
BgL_oldz00_3149 = 
umask(0L); 
umask(BgL_oldz00_3149); 
return 
(int)(BgL_oldz00_3149);} } 

}



/* openlog */
BGL_EXPORTED_DEF obj_t BGl_openlogz00zz__osz00(obj_t BgL_identz00_77, int BgL_optionz00_78, int BgL_facilityz00_79)
{
{ /* Llib/os.scm 1287 */
{ /* Llib/os.scm 1290 */
 char * BgL_tmpz00_6738;
BgL_tmpz00_6738 = 
BSTRING_TO_STRING(BgL_identz00_77); 
openlog(BgL_tmpz00_6738, BgL_optionz00_78, BgL_facilityz00_79); } BUNSPEC; 
return BUNSPEC;} 

}



/* &openlog */
obj_t BGl_z62openlogz62zz__osz00(obj_t BgL_envz00_3643, obj_t BgL_identz00_3644, obj_t BgL_optionz00_3645, obj_t BgL_facilityz00_3646)
{
{ /* Llib/os.scm 1287 */
{ /* Llib/os.scm 1290 */
 int BgL_auxz00_6757; int BgL_auxz00_6748; obj_t BgL_auxz00_6741;
{ /* Llib/os.scm 1290 */
 obj_t BgL_tmpz00_6758;
if(
INTEGERP(BgL_facilityz00_3646))
{ /* Llib/os.scm 1290 */
BgL_tmpz00_6758 = BgL_facilityz00_3646
; }  else 
{ 
 obj_t BgL_auxz00_6761;
BgL_auxz00_6761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(47645L), BGl_string2793z00zz__osz00, BGl_string2679z00zz__osz00, BgL_facilityz00_3646); 
FAILURE(BgL_auxz00_6761,BFALSE,BFALSE);} 
BgL_auxz00_6757 = 
CINT(BgL_tmpz00_6758); } 
{ /* Llib/os.scm 1290 */
 obj_t BgL_tmpz00_6749;
if(
INTEGERP(BgL_optionz00_3645))
{ /* Llib/os.scm 1290 */
BgL_tmpz00_6749 = BgL_optionz00_3645
; }  else 
{ 
 obj_t BgL_auxz00_6752;
BgL_auxz00_6752 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(47645L), BGl_string2793z00zz__osz00, BGl_string2679z00zz__osz00, BgL_optionz00_3645); 
FAILURE(BgL_auxz00_6752,BFALSE,BFALSE);} 
BgL_auxz00_6748 = 
CINT(BgL_tmpz00_6749); } 
if(
STRINGP(BgL_identz00_3644))
{ /* Llib/os.scm 1290 */
BgL_auxz00_6741 = BgL_identz00_3644
; }  else 
{ 
 obj_t BgL_auxz00_6744;
BgL_auxz00_6744 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(47645L), BGl_string2793z00zz__osz00, BGl_string2689z00zz__osz00, BgL_identz00_3644); 
FAILURE(BgL_auxz00_6744,BFALSE,BFALSE);} 
return 
BGl_openlogz00zz__osz00(BgL_auxz00_6741, BgL_auxz00_6748, BgL_auxz00_6757);} } 

}



/* syslog */
BGL_EXPORTED_DEF obj_t BGl_syslogz00zz__osz00(int BgL_priorityz00_80, obj_t BgL_argsz00_81)
{
{ /* Llib/os.scm 1296 */
{ /* Llib/os.scm 1302 */
 obj_t BgL_arg1781z00_1940;
{ /* Llib/os.scm 1302 */
 obj_t BgL_zc3z04anonymousza31783ze3z87_3647;
BgL_zc3z04anonymousza31783ze3z87_3647 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31783ze3ze5zz__osz00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31783ze3z87_3647, 
(int)(0L), BgL_argsz00_81); 
BgL_arg1781z00_1940 = 
BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(BgL_zc3z04anonymousza31783ze3z87_3647); } 
{ /* Llib/os.scm 1299 */
 char * BgL_auxz00_6775; char * BgL_tmpz00_6773;
BgL_auxz00_6775 = 
BSTRING_TO_STRING(BgL_arg1781z00_1940); 
BgL_tmpz00_6773 = 
BSTRING_TO_STRING(BGl_string2794z00zz__osz00); 
syslog(BgL_priorityz00_80, BgL_tmpz00_6773, BgL_auxz00_6775); } BUNSPEC; } 
return BUNSPEC;} 

}



/* &syslog */
obj_t BGl_z62syslogz62zz__osz00(obj_t BgL_envz00_3648, obj_t BgL_priorityz00_3649, obj_t BgL_argsz00_3650)
{
{ /* Llib/os.scm 1296 */
{ /* Llib/os.scm 1302 */
 int BgL_auxz00_6778;
{ /* Llib/os.scm 1302 */
 obj_t BgL_tmpz00_6779;
if(
INTEGERP(BgL_priorityz00_3649))
{ /* Llib/os.scm 1302 */
BgL_tmpz00_6779 = BgL_priorityz00_3649
; }  else 
{ 
 obj_t BgL_auxz00_6782;
BgL_auxz00_6782 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(48113L), BGl_string2795z00zz__osz00, BGl_string2679z00zz__osz00, BgL_priorityz00_3649); 
FAILURE(BgL_auxz00_6782,BFALSE,BFALSE);} 
BgL_auxz00_6778 = 
CINT(BgL_tmpz00_6779); } 
return 
BGl_syslogz00zz__osz00(BgL_auxz00_6778, BgL_argsz00_3650);} } 

}



/* &<@anonymous:1783> */
obj_t BGl_z62zc3z04anonymousza31783ze3ze5zz__osz00(obj_t BgL_envz00_3651, obj_t BgL_opz00_3653)
{
{ /* Llib/os.scm 1301 */
{ /* Llib/os.scm 1302 */
 obj_t BgL_argsz00_3652;
BgL_argsz00_3652 = 
PROCEDURE_REF(BgL_envz00_3651, 
(int)(0L)); 
{ 
 obj_t BgL_l1123z00_4198;
BgL_l1123z00_4198 = BgL_argsz00_3652; 
BgL_zc3z04anonymousza31784ze3z87_4197:
if(
PAIRP(BgL_l1123z00_4198))
{ /* Llib/os.scm 1302 */
{ /* Llib/os.scm 1302 */
 obj_t BgL_az00_4199;
BgL_az00_4199 = 
CAR(BgL_l1123z00_4198); 
{ /* Llib/os.scm 1302 */
 obj_t BgL_auxz00_6793;
if(
OUTPUT_PORTP(BgL_opz00_3653))
{ /* Llib/os.scm 1302 */
BgL_auxz00_6793 = BgL_opz00_3653
; }  else 
{ 
 obj_t BgL_auxz00_6796;
BgL_auxz00_6796 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(48088L), BGl_string2796z00zz__osz00, BGl_string2797z00zz__osz00, BgL_opz00_3653); 
FAILURE(BgL_auxz00_6796,BFALSE,BFALSE);} 
bgl_display_obj(BgL_az00_4199, BgL_auxz00_6793); } } 
{ 
 obj_t BgL_l1123z00_6801;
BgL_l1123z00_6801 = 
CDR(BgL_l1123z00_4198); 
BgL_l1123z00_4198 = BgL_l1123z00_6801; 
goto BgL_zc3z04anonymousza31784ze3z87_4197;} }  else 
{ /* Llib/os.scm 1302 */
if(
NULLP(BgL_l1123z00_4198))
{ /* Llib/os.scm 1302 */
return BTRUE;}  else 
{ /* Llib/os.scm 1302 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2798z00zz__osz00, BGl_string2799z00zz__osz00, BgL_l1123z00_4198, BGl_string2673z00zz__osz00, 
BINT(48066L));} } } } } 

}



/* closelog */
BGL_EXPORTED_DEF obj_t BGl_closelogz00zz__osz00(void)
{
{ /* Llib/os.scm 1308 */
closelog(); BUNSPEC; 
return BUNSPEC;} 

}



/* &closelog */
obj_t BGl_z62closelogz62zz__osz00(obj_t BgL_envz00_3654)
{
{ /* Llib/os.scm 1308 */
return 
BGl_closelogz00zz__osz00();} 

}



/* syslog-option */
BGL_EXPORTED_DEF int BGl_syslogzd2optionzd2zz__osz00(obj_t BgL_optsz00_82)
{
{ /* Llib/os.scm 1316 */
{ 
 obj_t BgL_optsz00_1954; long BgL_oz00_1955;
{ /* Llib/os.scm 1319 */
 long BgL_tmpz00_6809;
BgL_optsz00_1954 = BgL_optsz00_82; 
BgL_oz00_1955 = 0L; 
BgL_zc3z04anonymousza31788ze3z87_1956:
if(
NULLP(BgL_optsz00_1954))
{ /* Llib/os.scm 1321 */
BgL_tmpz00_6809 = BgL_oz00_1955
; }  else 
{ /* Llib/os.scm 1323 */
 obj_t BgL_arg1790z00_1958; long BgL_arg1791z00_1959;
{ /* Llib/os.scm 1323 */
 obj_t BgL_pairz00_3152;
if(
PAIRP(BgL_optsz00_1954))
{ /* Llib/os.scm 1323 */
BgL_pairz00_3152 = BgL_optsz00_1954; }  else 
{ 
 obj_t BgL_auxz00_6814;
BgL_auxz00_6814 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(48879L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_optsz00_1954); 
FAILURE(BgL_auxz00_6814,BFALSE,BFALSE);} 
BgL_arg1790z00_1958 = 
CDR(BgL_pairz00_3152); } 
{ /* Llib/os.scm 1325 */
 obj_t BgL_arg1792z00_1960;
{ /* Llib/os.scm 1325 */
 obj_t BgL_casezd2valuezd2_1961;
{ /* Llib/os.scm 1325 */
 obj_t BgL_pairz00_3153;
if(
PAIRP(BgL_optsz00_1954))
{ /* Llib/os.scm 1325 */
BgL_pairz00_3153 = BgL_optsz00_1954; }  else 
{ 
 obj_t BgL_auxz00_6821;
BgL_auxz00_6821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(48915L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_optsz00_1954); 
FAILURE(BgL_auxz00_6821,BFALSE,BFALSE);} 
BgL_casezd2valuezd2_1961 = 
CAR(BgL_pairz00_3153); } 
if(
(BgL_casezd2valuezd2_1961==BGl_symbol2800z00zz__osz00))
{ /* Llib/os.scm 1325 */
BgL_arg1792z00_1960 = 
BINT(LOG_CONS); }  else 
{ /* Llib/os.scm 1325 */
if(
(BgL_casezd2valuezd2_1961==BGl_symbol2802z00zz__osz00))
{ /* Llib/os.scm 1325 */
BgL_arg1792z00_1960 = 
BINT(LOG_NDELAY); }  else 
{ /* Llib/os.scm 1325 */
if(
(BgL_casezd2valuezd2_1961==BGl_symbol2804z00zz__osz00))
{ /* Llib/os.scm 1325 */
BgL_arg1792z00_1960 = 
BINT(LOG_NOWAIT); }  else 
{ /* Llib/os.scm 1325 */
if(
(BgL_casezd2valuezd2_1961==BGl_symbol2806z00zz__osz00))
{ /* Llib/os.scm 1325 */
BgL_arg1792z00_1960 = 
BINT(LOG_ODELAY); }  else 
{ /* Llib/os.scm 1325 */
if(
(BgL_casezd2valuezd2_1961==BGl_symbol2808z00zz__osz00))
{ /* Llib/os.scm 1325 */
BgL_arg1792z00_1960 = 
BINT(LOG_PID); }  else 
{ /* Llib/os.scm 1333 */
 obj_t BgL_arg1798z00_1967;
{ /* Llib/os.scm 1333 */
 obj_t BgL_pairz00_3159;
if(
PAIRP(BgL_optsz00_1954))
{ /* Llib/os.scm 1333 */
BgL_pairz00_3159 = BgL_optsz00_1954; }  else 
{ 
 obj_t BgL_auxz00_6843;
BgL_auxz00_6843 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(49236L), BGl_string2704z00zz__osz00, BGl_string2691z00zz__osz00, BgL_optsz00_1954); 
FAILURE(BgL_auxz00_6843,BFALSE,BFALSE);} 
BgL_arg1798z00_1967 = 
CAR(BgL_pairz00_3159); } 
BgL_arg1792z00_1960 = 
BGl_errorz00zz__errorz00(BGl_string2810z00zz__osz00, BGl_string2811z00zz__osz00, BgL_arg1798z00_1967); } } } } } } 
{ /* Llib/os.scm 1324 */
 long BgL_yz00_3161;
{ /* Llib/os.scm 1333 */
 obj_t BgL_tmpz00_6849;
if(
INTEGERP(BgL_arg1792z00_1960))
{ /* Llib/os.scm 1333 */
BgL_tmpz00_6849 = BgL_arg1792z00_1960
; }  else 
{ 
 obj_t BgL_auxz00_6852;
BgL_auxz00_6852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(49243L), BGl_string2704z00zz__osz00, BGl_string2679z00zz__osz00, BgL_arg1792z00_1960); 
FAILURE(BgL_auxz00_6852,BFALSE,BFALSE);} 
BgL_yz00_3161 = 
(long)CINT(BgL_tmpz00_6849); } 
BgL_arg1791z00_1959 = 
(BgL_oz00_1955 | BgL_yz00_3161); } } 
{ 
 long BgL_oz00_6859; obj_t BgL_optsz00_6858;
BgL_optsz00_6858 = BgL_arg1790z00_1958; 
BgL_oz00_6859 = BgL_arg1791z00_1959; 
BgL_oz00_1955 = BgL_oz00_6859; 
BgL_optsz00_1954 = BgL_optsz00_6858; 
goto BgL_zc3z04anonymousza31788ze3z87_1956;} } 
return 
(int)(BgL_tmpz00_6809);} } } 

}



/* &syslog-option */
obj_t BGl_z62syslogzd2optionzb0zz__osz00(obj_t BgL_envz00_3655, obj_t BgL_optsz00_3656)
{
{ /* Llib/os.scm 1316 */
return 
BINT(
BGl_syslogzd2optionzd2zz__osz00(BgL_optsz00_3656));} 

}



/* syslog-facility */
BGL_EXPORTED_DEF int BGl_syslogzd2facilityzd2zz__osz00(obj_t BgL_facilityz00_83)
{
{ /* Llib/os.scm 1340 */
if(
(BgL_facilityz00_83==BGl_symbol2812z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_AUTH;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2814z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_AUTHPRIV;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2816z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_CRON;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2818z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_DAEMON;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2820z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_FTP;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2822z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_KERN;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2824z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL0;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2826z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL1;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2828z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL2;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2830z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL3;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2832z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL4;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2834z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL5;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2836z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL6;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2838z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LOCAL7;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2840z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_LPR;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2842z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_MAIL;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2844z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_NEWS;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2846z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_SYSLOG;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2848z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_USER;}  else 
{ /* Llib/os.scm 1343 */
if(
(BgL_facilityz00_83==BGl_symbol2850z00zz__osz00))
{ /* Llib/os.scm 1343 */
return LOG_UUCP;}  else 
{ /* Llib/os.scm 1364 */
 obj_t BgL_tmpz00_6903;
{ /* Llib/os.scm 1364 */
 obj_t BgL_aux2651z00_4121;
BgL_aux2651z00_4121 = 
BGl_errorz00zz__errorz00(BGl_string2852z00zz__osz00, BGl_string2853z00zz__osz00, BgL_facilityz00_83); 
if(
INTEGERP(BgL_aux2651z00_4121))
{ /* Llib/os.scm 1364 */
BgL_tmpz00_6903 = BgL_aux2651z00_4121
; }  else 
{ 
 obj_t BgL_auxz00_6907;
BgL_auxz00_6907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(50329L), BGl_string2852z00zz__osz00, BGl_string2679z00zz__osz00, BgL_aux2651z00_4121); 
FAILURE(BgL_auxz00_6907,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_6903);} } } } } } } } } } } } } } } } } } } } } 

}



/* &syslog-facility */
obj_t BGl_z62syslogzd2facilityzb0zz__osz00(obj_t BgL_envz00_3657, obj_t BgL_facilityz00_3658)
{
{ /* Llib/os.scm 1340 */
{ /* Llib/os.scm 1343 */
 int BgL_tmpz00_6912;
{ /* Llib/os.scm 1343 */
 obj_t BgL_auxz00_6913;
if(
SYMBOLP(BgL_facilityz00_3658))
{ /* Llib/os.scm 1343 */
BgL_auxz00_6913 = BgL_facilityz00_3658
; }  else 
{ 
 obj_t BgL_auxz00_6916;
BgL_auxz00_6916 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(49601L), BGl_string2854z00zz__osz00, BGl_string2730z00zz__osz00, BgL_facilityz00_3658); 
FAILURE(BgL_auxz00_6916,BFALSE,BFALSE);} 
BgL_tmpz00_6912 = 
BGl_syslogzd2facilityzd2zz__osz00(BgL_auxz00_6913); } 
return 
BINT(BgL_tmpz00_6912);} } 

}



/* syslog-level */
BGL_EXPORTED_DEF int BGl_syslogzd2levelzd2zz__osz00(obj_t BgL_lvlz00_84)
{
{ /* Llib/os.scm 1371 */
if(
(BgL_lvlz00_84==BGl_symbol2855z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_EMERG;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2857z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_ALERT;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2859z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_CRIT;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2861z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_ERR;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2863z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_WARNING;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2865z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_NOTICE;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2867z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_INFO;}  else 
{ /* Llib/os.scm 1374 */
if(
(BgL_lvlz00_84==BGl_symbol2869z00zz__osz00))
{ /* Llib/os.scm 1374 */
return LOG_DEBUG;}  else 
{ /* Llib/os.scm 1383 */
 obj_t BgL_tmpz00_6938;
{ /* Llib/os.scm 1383 */
 obj_t BgL_aux2654z00_4124;
BgL_aux2654z00_4124 = 
BGl_errorz00zz__errorz00(BGl_string2871z00zz__osz00, BGl_string2872z00zz__osz00, BgL_lvlz00_84); 
if(
INTEGERP(BgL_aux2654z00_4124))
{ /* Llib/os.scm 1383 */
BgL_tmpz00_6938 = BgL_aux2654z00_4124
; }  else 
{ 
 obj_t BgL_auxz00_6942;
BgL_auxz00_6942 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(51026L), BGl_string2871z00zz__osz00, BGl_string2679z00zz__osz00, BgL_aux2654z00_4124); 
FAILURE(BgL_auxz00_6942,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_6938);} } } } } } } } } 

}



/* &syslog-level */
obj_t BGl_z62syslogzd2levelzb0zz__osz00(obj_t BgL_envz00_3659, obj_t BgL_lvlz00_3660)
{
{ /* Llib/os.scm 1371 */
{ /* Llib/os.scm 1374 */
 int BgL_tmpz00_6947;
{ /* Llib/os.scm 1374 */
 obj_t BgL_auxz00_6948;
if(
SYMBOLP(BgL_lvlz00_3660))
{ /* Llib/os.scm 1374 */
BgL_auxz00_6948 = BgL_lvlz00_3660
; }  else 
{ 
 obj_t BgL_auxz00_6951;
BgL_auxz00_6951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(50729L), BGl_string2873z00zz__osz00, BGl_string2730z00zz__osz00, BgL_lvlz00_3660); 
FAILURE(BgL_auxz00_6951,BFALSE,BFALSE);} 
BgL_tmpz00_6947 = 
BGl_syslogzd2levelzd2zz__osz00(BgL_auxz00_6948); } 
return 
BINT(BgL_tmpz00_6947);} } 

}



/* limit-resource-no */
obj_t BGl_limitzd2resourcezd2noz00zz__osz00(obj_t BgL_rz00_85, obj_t BgL_idz00_86)
{
{ /* Llib/os.scm 1390 */
{ 
 obj_t BgL_rz00_2002;
if(
INTEGERP(BgL_rz00_85))
{ /* Llib/os.scm 1414 */
return BgL_rz00_85;}  else 
{ /* Llib/os.scm 1414 */
if(
SYMBOLP(BgL_rz00_85))
{ /* Llib/os.scm 1415 */
BgL_rz00_2002 = BgL_rz00_85; 
if(
(BgL_rz00_2002==BGl_symbol2874z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_CORE);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2876z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_CPU);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2878z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_DATA);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2880z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_FSIZE);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2882z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_LOCKS);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2884z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_MEMLOCK);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2886z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_MSGQUEUE);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2888z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_NICE);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2890z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_NOFILE);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2892z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_NPROC);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2894z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_RSS);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2896z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_RTTIME);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2898z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_SIGPENDING);}  else 
{ /* Llib/os.scm 1395 */
if(
(BgL_rz00_2002==BGl_symbol2900z00zz__osz00))
{ /* Llib/os.scm 1395 */
return 
BINT(RLIMIT_STACK);}  else 
{ /* Llib/os.scm 1395 */
return 
BGl_errorz00zz__errorz00(BgL_idz00_86, BGl_string2902z00zz__osz00, BgL_rz00_2002);} } } } } } } } } } } } } } }  else 
{ /* Llib/os.scm 1415 */
return 
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_idz00_86, BGl_string2903z00zz__osz00, BgL_rz00_85);} } } } 

}



/* getrlimit */
BGL_EXPORTED_DEF obj_t BGl_getrlimitz00zz__osz00(obj_t BgL_rz00_87)
{
{ /* Llib/os.scm 1421 */
{ /* Llib/os.scm 1424 */
 obj_t BgL_arg1844z00_3204;
BgL_arg1844z00_3204 = 
BGl_limitzd2resourcezd2noz00zz__osz00(BgL_rz00_87, BGl_string2904z00zz__osz00); 
{ /* Llib/os.scm 1424 */
 long BgL_tmpz00_7006;
{ /* Llib/os.scm 1424 */
 obj_t BgL_tmpz00_7007;
if(
INTEGERP(BgL_arg1844z00_3204))
{ /* Llib/os.scm 1424 */
BgL_tmpz00_7007 = BgL_arg1844z00_3204
; }  else 
{ 
 obj_t BgL_auxz00_7010;
BgL_auxz00_7010 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(52487L), BGl_string2904z00zz__osz00, BGl_string2679z00zz__osz00, BgL_arg1844z00_3204); 
FAILURE(BgL_auxz00_7010,BFALSE,BFALSE);} 
BgL_tmpz00_7006 = 
(long)CINT(BgL_tmpz00_7007); } 
return 
bgl_getrlimit(BgL_tmpz00_7006);} } } 

}



/* &getrlimit */
obj_t BGl_z62getrlimitz62zz__osz00(obj_t BgL_envz00_3661, obj_t BgL_rz00_3662)
{
{ /* Llib/os.scm 1421 */
return 
BGl_getrlimitz00zz__osz00(BgL_rz00_3662);} 

}



/* setrlimit! */
BGL_EXPORTED_DEF bool_t BGl_setrlimitz12z12zz__osz00(obj_t BgL_rz00_88, long BgL_softz00_89, long BgL_hardz00_90)
{
{ /* Llib/os.scm 1431 */
{ /* Llib/os.scm 1434 */
 obj_t BgL_arg1845z00_3205;
BgL_arg1845z00_3205 = 
BGl_limitzd2resourcezd2noz00zz__osz00(BgL_rz00_88, BGl_string2905z00zz__osz00); 
{ /* Llib/os.scm 1434 */
 long BgL_tmpz00_7018;
{ /* Llib/os.scm 1434 */
 obj_t BgL_tmpz00_7019;
if(
INTEGERP(BgL_arg1845z00_3205))
{ /* Llib/os.scm 1434 */
BgL_tmpz00_7019 = BgL_arg1845z00_3205
; }  else 
{ 
 obj_t BgL_auxz00_7022;
BgL_auxz00_7022 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(52902L), BGl_string2905z00zz__osz00, BGl_string2679z00zz__osz00, BgL_arg1845z00_3205); 
FAILURE(BgL_auxz00_7022,BFALSE,BFALSE);} 
BgL_tmpz00_7018 = 
(long)CINT(BgL_tmpz00_7019); } 
return 
bgl_setrlimit(BgL_tmpz00_7018, 
(long)(BgL_softz00_89), 
(long)(BgL_hardz00_90));} } } 

}



/* &setrlimit! */
obj_t BGl_z62setrlimitz12z70zz__osz00(obj_t BgL_envz00_3663, obj_t BgL_rz00_3664, obj_t BgL_softz00_3665, obj_t BgL_hardz00_3666)
{
{ /* Llib/os.scm 1431 */
{ /* Llib/os.scm 1434 */
 bool_t BgL_tmpz00_7030;
{ /* Llib/os.scm 1434 */
 long BgL_auxz00_7040; long BgL_auxz00_7031;
{ /* Llib/os.scm 1434 */
 obj_t BgL_tmpz00_7041;
if(
ELONGP(BgL_hardz00_3666))
{ /* Llib/os.scm 1434 */
BgL_tmpz00_7041 = BgL_hardz00_3666
; }  else 
{ 
 obj_t BgL_auxz00_7044;
BgL_auxz00_7044 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(52869L), BGl_string2906z00zz__osz00, BGl_string2785z00zz__osz00, BgL_hardz00_3666); 
FAILURE(BgL_auxz00_7044,BFALSE,BFALSE);} 
BgL_auxz00_7040 = 
BELONG_TO_LONG(BgL_tmpz00_7041); } 
{ /* Llib/os.scm 1434 */
 obj_t BgL_tmpz00_7032;
if(
ELONGP(BgL_softz00_3665))
{ /* Llib/os.scm 1434 */
BgL_tmpz00_7032 = BgL_softz00_3665
; }  else 
{ 
 obj_t BgL_auxz00_7035;
BgL_auxz00_7035 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2673z00zz__osz00, 
BINT(52869L), BGl_string2906z00zz__osz00, BGl_string2785z00zz__osz00, BgL_softz00_3665); 
FAILURE(BgL_auxz00_7035,BFALSE,BFALSE);} 
BgL_auxz00_7031 = 
BELONG_TO_LONG(BgL_tmpz00_7032); } 
BgL_tmpz00_7030 = 
BGl_setrlimitz12z12zz__osz00(BgL_rz00_3664, BgL_auxz00_7031, BgL_auxz00_7040); } 
return 
BBOOL(BgL_tmpz00_7030);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__osz00(void)
{
{ /* Llib/os.scm 18 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__osz00(void)
{
{ /* Llib/os.scm 18 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__osz00(void)
{
{ /* Llib/os.scm 18 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__osz00(void)
{
{ /* Llib/os.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2907z00zz__osz00)); 
return 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string2907z00zz__osz00));} 

}

#ifdef __cplusplus
}
#endif
