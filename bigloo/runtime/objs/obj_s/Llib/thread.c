/*===========================================================================*/
/*   (Llib/thread.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/thread.scm -indent -o objs/obj_s/Llib/thread.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___THREAD_TYPE_DEFINITIONS
#define BGL___THREAD_TYPE_DEFINITIONS

/* object type definitions */
typedef struct BgL_nothreadzd2backendzd2_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_namez00;
} *BgL_nothreadzd2backendzd2_bglt;

typedef struct BgL_threadzd2backendzd2_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_namez00;
} *BgL_threadzd2backendzd2_bglt;

typedef struct BgL_threadz00_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_namez00;
} *BgL_threadz00_bglt;

typedef struct BgL_nothreadz00_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_namez00;
   obj_t BgL_bodyz00;
   obj_t BgL_z52specificz52;
   obj_t BgL_z52cleanupz52;
   obj_t BgL_endzd2resultzd2;
   obj_t BgL_endzd2exceptionzd2;
   obj_t BgL_z52namez52;
} *BgL_nothreadz00_bglt;


#endif // BGL___THREAD_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62threadzd2killz12zd2nothrea1242z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2800z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2721z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2specificzd2noth1244z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2641z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2480z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2805z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2643z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2482z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2namezd2setz12z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2parameterzd2setz12z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2726z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2645z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2647z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2729z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2486z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2649z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2487z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62z52userzd2currentzd2thread1227z30zz__threadz00(obj_t, obj_t);
extern obj_t bgl_make_nil_condvar(void);
static obj_t BGl_z52currentzd2threadz80zz__threadz00(void);
BGL_EXPORTED_DECL obj_t BGl_threadzd2cleanupzd2zz__threadz00(BgL_threadz00_bglt);
static obj_t BGl_list2457z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2namezd2setz12zd2not1254za2zz__threadz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_threadz00_bglt, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tbzd2condvarzd2initializa7ez12zb5zz__threadz00(BgL_threadzd2backendzd2_bglt, obj_t);
static obj_t BGl_z62threadzd2cleanupzd2setz121215z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62z52userzd2threadzd2yieldz121231z22zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2joinz12za2zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl__makezd2mutexzd2zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62z52userzd2currentzd2threadz30zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2732z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2651z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2700z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2653z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2492z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2737z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2656z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62tbzd2currentzd2threadzd2no1184zb0zz__threadz00(obj_t, obj_t);
extern obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
static obj_t BGl_list2705z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_conditionzd2variablezd2nilz00zz__threadz00(void);
BGL_EXPORTED_DECL obj_t BGl_mutexzd2lockz12zc0zz__threadz00(obj_t, long);
static obj_t BGl_symbol2658z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2496z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_threadzd2specificzd2zz__threadz00(BgL_threadz00_bglt);
static obj_t BGl_list2708z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2709z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__threadz00(obj_t);
static obj_t BGl_z62threadzd2specificzb0zz__threadz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_withzd2lockzd2zz__threadz00(obj_t, obj_t);
static obj_t BGl_za2threadzd2backendsza2zd2zz__threadz00 = BUNSPEC;
extern obj_t BGl_raisez00zz__errorz00(obj_t);
extern obj_t bgl_remq_bang(obj_t, obj_t);
extern obj_t BGl_exitz00zz__errorz00(obj_t);
static obj_t BGl_z62threadzd2joinz121202za2zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2740z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2660z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2662z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_threadz00_bglt, obj_t);
static obj_t BGl_list2712z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2746z00zz__threadz00 = BUNSPEC;
extern void bgl_sleep(long);
static obj_t BGl_symbol2667z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2553z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2717z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2669z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31525ze3ze5zz__threadz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31444ze3ze5zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2joinz12zc0zz__threadz00(BgL_threadz00_bglt, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_threadz00_bglt);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL bool_t BGl_conditionzd2variablezd2broadcastz12z12zz__threadz00(obj_t);
static obj_t BGl_z62tbzd2mutexzd2initializa7ez12zd7zz__threadz00(obj_t, obj_t, obj_t);
extern obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2mutexzd2zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2sleepz12zc0zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_defaultzd2threadzd2backendz00zz__threadz00(void);
extern obj_t BGl_za2classesza2z00zz__objectz00;
static obj_t BGl_symbol2751z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2753z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2720z00zz__threadz00 = BUNSPEC;
extern obj_t BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_objectz00_bglt, int);
static obj_t BGl_symbol2674z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2804z00zz__threadz00 = BUNSPEC;
extern obj_t bgl_nanoseconds_to_date(BGL_LONGLONG_T);
static obj_t BGl_symbol2758z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2725z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2namezb0zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2parameterzb0zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31542ze3ze5zz__threadz00(obj_t);
extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31364ze3ze5zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31437ze3ze5zz__threadz00(obj_t);
extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
static obj_t BGl_z62threadzd2startzd2joinabl1199z62zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mutexzd2statezd2zz__threadz00(obj_t);
static obj_t BGl_z62z52userzd2threadzd2sleepz121229z22zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2680z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2763z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_conditionzd2variablezd2namez00zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_currentzd2threadzd2zz__threadz00(void);
static obj_t BGl_list2731z00zz__threadz00 = BUNSPEC;
static obj_t BGl_threadzd2startz12zd2nothre1234ze70zf5zz__threadz00(BgL_nothreadz00_bglt, obj_t);
static obj_t BGl_symbol2765z00zz__threadz00 = BUNSPEC;
static obj_t BGl_nothreadzd2backendzd2zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2847z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2startzd2joinablez12z70zz__threadz00(obj_t, obj_t);
static obj_t BGl_list2734z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2686z00zz__threadz00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__threadz00(void);
static obj_t BGl_list2735z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2736z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2688z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62conditionzd2variablezd2namez62zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_threadz00_bglt);
static obj_t BGl_list2739z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_withzd2timedzd2lockz00zz__threadz00(obj_t, int, obj_t);
extern obj_t bgl_make_nil_mutex(void);
static obj_t BGl_z62zc3z04anonymousza31349ze3ze5zz__threadz00(obj_t);
static BgL_nothreadz00_bglt BGl_z62lambda1418z62zz__threadz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tbzd2mutexzd2initializa7ez12zb5zz__threadz00(BgL_threadzd2backendzd2_bglt, obj_t);
static obj_t BGl_z62withzd2timedzd2lockz62zz__threadz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2cleanupzd2nothr1248z62zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_currentzd2threadzd2backendz00zz__threadz00(void);
static BgL_nothreadzd2backendzd2_bglt BGl_za2nothreadzd2backendza2zd2zz__threadz00;
static obj_t BGl_symbol2770z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2692z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2856z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2775z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2742z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2694z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2745z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2697z00zz__threadz00 = BUNSPEC;
static BgL_nothreadz00_bglt BGl_z62lambda1421z62zz__threadz00(obj_t);
static obj_t BGl_list2666z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2specificzd2setz121211z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31390ze3ze5zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62currentzd2dynamiczd2envz62zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2startz12zc0zz__threadz00(BgL_threadz00_bglt, obj_t);
static obj_t BGl_z62lambda1427z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62lambda1428z62zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2name1218zb0zz__threadz00(obj_t, obj_t);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__threadz00(void);
static obj_t BGl_genericzd2initzd2zz__threadz00(void);
BGL_EXPORTED_DECL obj_t BGl_mutexzd2namezd2zz__threadz00(obj_t);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62tbzd2currentzd2thread1179z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2781z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62mutexzd2nilzb0zz__threadz00(obj_t);
extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t, long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list2750z00zz__threadz00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__threadz00(void);
static BgL_threadz00_bglt BGl_z62tbzd2makezd2threadz62zz__threadz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2786z00zz__threadz00 = BUNSPEC;
static obj_t BGl__makezd2spinlockzd2zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mutexzd2unlockz12zc0zz__threadz00(obj_t);
static obj_t BGl_list2673z00zz__threadz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__threadz00(void);
static obj_t BGl_z62zc3z04anonymousza31561ze3ze5zz__threadz00(obj_t);
static obj_t BGl_list2757z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31383ze3ze5zz__threadz00(obj_t);
extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t);
static obj_t BGl_list2679z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62lambda1435z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62lambda1436z62zz__threadz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_currentzd2threadzd2backendzd2setz12zc0zz__threadz00(BgL_threadzd2backendzd2_bglt);
static obj_t BGl_z62threadzd2killz12za2zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62mutexzf3z91zz__threadz00(obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__threadz00(void);
extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62threadzd2specific1209zb0zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2yieldz12zc0zz__threadz00(void);
static obj_t BGl_z62conditionzd2variablezf3z43zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62defaultzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2startz12zd2nothre1234z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62conditionzd2variablezd2broadcastz12z70zz__threadz00(obj_t, obj_t);
static obj_t BGl__makezd2threadzd2zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2sleepz12za2zz__threadz00(obj_t, obj_t);
extern double BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(obj_t);
static obj_t BGl_symbol2791z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31401ze3ze5zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2793z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2specificzd2setz12z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2876z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2762z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2878z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2846z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2798z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2685z00zz__threadz00 = BUNSPEC;
static BgL_threadzd2backendzd2_bglt BGl_z62lambda1360z62zz__threadz00(obj_t, obj_t);
extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
static obj_t BGl_z62lambda1442z62zz__threadz00(obj_t, obj_t);
static BgL_threadzd2backendzd2_bglt BGl_z62lambda1362z62zz__threadz00(obj_t);
static obj_t BGl_z62lambda1443z62zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31465ze3ze5zz__threadz00(obj_t);
static obj_t BGl_list2769z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__threadz00(obj_t, obj_t);
extern obj_t BGl_z62exceptionz62zz__objectz00;
extern obj_t BGl_objectz00zz__objectz00;
static obj_t BGl_z62lambda1368z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62lambda1449z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62lambda1369z62zz__threadz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_getzd2threadzd2backendz00zz__threadz00(obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62dynamiczd2envzf3z43zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2killz121207za2zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62objectzd2writezd2thread1192z62zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62mutexzd2namezb0zz__threadz00(obj_t, obj_t);
static obj_t BGl_list2691z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2855z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2774z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62lambda1450z62zz__threadz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2killz12zc0zz__threadz00(BgL_threadz00_bglt, int);
static obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__threadz00(obj_t);
static obj_t BGl_z62z52userzd2threadzd2sleepz12z22zz__threadz00(obj_t, obj_t, obj_t);
static BgL_threadz00_bglt BGl_z62lambda1374z62zz__threadz00(obj_t);
static obj_t BGl_z62lambda1456z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2specificzd2setz121246z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62lambda1457z62zz__threadz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2initializa7ez12z67zz__threadz00(BgL_threadz00_bglt);
static obj_t BGl_z62threadzd2cleanup1213zb0zz__threadz00(obj_t, obj_t);
extern obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_threadz00_bglt, obj_t);
static obj_t BGl_z62threadzd2terminatez12zd2no1240z70zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2joinz12zd2nothrea1238z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31411ze3ze5zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31403ze3ze5zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2780z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2785z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62lambda1381z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31394ze3ze5zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2terminatez12za2zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62lambda1382z62zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62lambda1463z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62lambda1464z62zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2startz12za2zz__threadz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_dynamiczd2envzf3z21zz__threadz00(obj_t);
extern BGL_LONGLONG_T bgl_current_nanoseconds(void);
extern bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
static obj_t BGl_symbol2500z00zz__threadz00 = BUNSPEC;
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_symbol2421z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62getzd2threadzd2backendz62zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2namezd2setz12z12zz__threadz00(BgL_threadz00_bglt, obj_t);
static obj_t BGl_symbol2424z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_mutexzd2nilzd2zz__threadz00(void);
BGL_EXPORTED_DECL BgL_threadz00_bglt BGl_makezd2threadzd2zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2startzd2joinablez12z12zz__threadz00(BgL_threadz00_bglt);
BGL_EXPORTED_DECL bool_t BGl_conditionzd2variablezd2signalz12z12zz__threadz00(obj_t);
static obj_t BGl_z62tbzd2mutexzd2initializa7ez121185zd7zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2startz121197za2zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2cleanupzd2setz12z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_list2790z00zz__threadz00 = BUNSPEC;
static obj_t BGl_list2875z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2cleanupzb0zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62mutexzd2unlockz12za2zz__threadz00(obj_t, obj_t);
extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_list2797z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31395ze3ze5zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62tbzd2condvarzd2initializa7ez12zd7zz__threadz00(obj_t, obj_t, obj_t);
static BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1473z62zz__threadz00(obj_t, obj_t);
static BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1475z62zz__threadz00(obj_t);
static obj_t BGl_symbol2510z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2initializa7ez12z05zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2512z00zz__threadz00 = BUNSPEC;
static obj_t BGl_methodzd2initzd2zz__threadz00(void);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62threadzd2yieldz12za2zz__threadz00(obj_t);
static obj_t BGl_symbol2516z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_threadzd2namezd2zz__threadz00(BgL_threadz00_bglt);
static obj_t BGl_z62threadzd2namezd2setz121220z70zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_zc3z04exitza31557ze3ze70z60zz__threadz00(BgL_nothreadz00_bglt, obj_t, obj_t);
static obj_t BGl_z62threadzd2initializa7ez121195z05zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2spinlockzd2zz__threadz00(obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
extern obj_t BGl_objectzd2writezd2zz__objectz00(BgL_objectz00_bglt, obj_t);
static obj_t BGl_z62conditionzd2variablezd2nilz62zz__threadz00(obj_t);
static obj_t BGl_z62tbzd2currentzd2threadz62zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DEF obj_t BGl_nothreadz00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31477ze3ze5zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2521z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2440z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_threadzd2parameterzd2setz12z12zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62threadzd2cleanupzd2setz12zd21250za2zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2443z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2526z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2445z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL BgL_threadz00_bglt BGl_tbzd2makezd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt, obj_t, obj_t);
static obj_t BGl_symbol2447z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62mutexzd2statezb0zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62objectzd2printzd2thread1194z62zz__threadz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62conditionzd2variablezd2signalz12z70zz__threadz00(obj_t, obj_t);
extern long bgl_date_to_seconds(obj_t);
static BgL_threadz00_bglt BGl_z62tbzd2makezd2threadzd2nothr1182zb0zz__threadz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_mutexzf3zf3zz__threadz00(obj_t);
static obj_t BGl_z62z52userzd2threadzd2yieldz12z22zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31389ze3ze5zz__threadz00(obj_t, obj_t);
static obj_t BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BUNSPEC;
static obj_t BGl_z62tbzd2makezd2thread1177z62zz__threadz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2531z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2450z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62defaultzd2threadzd2backendz62zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2conditionzd2variablez00zz__threadz00(obj_t);
static obj_t BGl_symbol2536z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_threadzd2parameterzd2zz__threadz00(obj_t);
static obj_t BGl_symbol2458z00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_currentzd2dynamiczd2envz00zz__threadz00(void);
static obj_t BGl_z62zc3z04anonymousza31423ze3ze5zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_threadzd2terminatez12zc0zz__threadz00(BgL_threadz00_bglt);
static obj_t BGl_za2mutexzd2nilza2zd2zz__threadz00 = BUNSPEC;
static obj_t BGl_z62withzd2lockzb0zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl__conditionzd2variablezd2waitz12z12zz__threadz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_conditionzd2variablezd2waitz12z12zz__threadz00(obj_t, obj_t, long);
static obj_t BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2701z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2703z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2541z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2460z00zz__threadz00 = BUNSPEC;
static obj_t BGl__makezd2conditionzd2variablez00zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2706z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2545z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2628z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2466z00zz__threadz00 = BUNSPEC;
static obj_t BGl__mutexzd2lockz12zc0zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2468z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62objectzd2displayzd2threa1190z62zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62currentzd2threadzb0zz__threadz00(obj_t);
BGL_EXPORTED_DEF obj_t BGl_threadz00zz__threadz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_conditionzd2variablezf3z21zz__threadz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_defaultzd2threadzd2backendzd2setz12zc0zz__threadz00(BgL_threadzd2backendzd2_bglt);
static obj_t BGl_z62threadzd2terminatez121204za2zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62currentzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t, obj_t);
static obj_t BGl_symbol2710z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62tbzd2condvarzd2initializa71187zc5zz__threadz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2713z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2632z00zz__threadz00 = BUNSPEC;
static obj_t BGl_z62currentzd2threadzd2backendz62zz__threadz00(obj_t);
static obj_t BGl_symbol2715z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2472z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2635z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2554z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2718z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2637z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2556z00zz__threadz00 = BUNSPEC;
static obj_t BGl_symbol2639z00zz__threadz00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt);
BGL_EXPORTED_DEF obj_t BGl_threadzd2backendzd2zz__threadz00 = BUNSPEC;
static obj_t BGl_z62threadzd2startzd2joinabl1236z62zz__threadz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31409ze3ze5zz__threadz00(obj_t, obj_t);
extern obj_t BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_objectz00_bglt, int, obj_t);
static obj_t BGl_z62threadzd2namezd2nothread1252z62zz__threadz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2conditionzd2variablezd2envzd2zz__threadz00, BgL_bgl__makeza7d2conditio2881za7, opt_generic_entry, BGl__makezd2conditionzd2variablez00zz__threadz00, BFALSE, -1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2831z00zz__threadz00, BgL_bgl_za762threadza7d2clea2882z00, BGl_z62threadzd2cleanupzd2setz12zd21250za2zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2833z00zz__threadz00, BgL_bgl_za762threadza7d2name2883z00, BGl_z62threadzd2namezd2nothread1252z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2590z00zz__threadz00, BgL_bgl_za762tbza7d2currentza72884za7, BGl_z62tbzd2currentzd2thread1179z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2840z00zz__threadz00, BgL_bgl_string2840za700za7za7_2885za7, "&thread-cleanup-nothr1248", 25 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2834z00zz__threadz00, BgL_bgl_za762threadza7d2name2886z00, BGl_z62threadzd2namezd2setz12zd2not1254za2zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2841z00zz__threadz00, BgL_bgl_string2841za700za7za7_2887za7, "&thread-specific-set!1246", 25 );
DEFINE_STRING( BGl_string2760z00zz__threadz00, BgL_bgl_string2760za700za7za7_2888za7, "&thread-specific", 16 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2592z00zz__threadz00, BgL_bgl_za762tbza7d2mutexza7d22889za7, BGl_z62tbzd2mutexzd2initializa7ez121185zd7zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2842z00zz__threadz00, BgL_bgl_string2842za700za7za7_2890za7, "&thread-specific-noth1244", 25 );
DEFINE_STRING( BGl_string2761z00zz__threadz00, BgL_bgl_string2761za700za7za7_2891za7, "thread-specific-set!:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string2843z00zz__threadz00, BgL_bgl_string2843za700za7za7_2892za7, "&thread-kill!-nothrea1242", 25 );
DEFINE_STRING( BGl_string2681z00zz__threadz00, BgL_bgl_string2681za700za7za7_2893za7, "method1186", 10 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2594z00zz__threadz00, BgL_bgl_za762tbza7d2condvarza72894za7, BGl_z62tbzd2condvarzd2initializa71187zc5zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2844z00zz__threadz00, BgL_bgl_string2844za700za7za7_2895za7, "thread-terminate!-no1240", 24 );
DEFINE_STRING( BGl_string2682z00zz__threadz00, BgL_bgl_string2682za700za7za7_2896za7, "&tb-mutex-initialize!", 21 );
DEFINE_STRING( BGl_string2845z00zz__threadz00, BgL_bgl_string2845za700za7za7_2897za7, "thread-terminate!-no1240:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string2764z00zz__threadz00, BgL_bgl_string2764za700za7za7_2898za7, "method1212", 10 );
DEFINE_STRING( BGl_string2683z00zz__threadz00, BgL_bgl_string2683za700za7za7_2899za7, "tb-condvar-initialize!", 22 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2596z00zz__threadz00, BgL_bgl_za762threadza7d2init2900z00, BGl_z62threadzd2initializa7ez121195z05zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2684z00zz__threadz00, BgL_bgl_string2684za700za7za7_2901za7, "tb-condvar-initialize!:Wrong number of arguments", 48 );
DEFINE_STRING( BGl_string2766z00zz__threadz00, BgL_bgl_string2766za700za7za7_2902za7, "v", 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2598z00zz__threadz00, BgL_bgl_za762threadza7d2star2903z00, va_generic_entry, BGl_z62threadzd2startz121197za2zz__threadz00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2848z00zz__threadz00, BgL_bgl_string2848za700za7za7_2904za7, "fun1576", 7 );
DEFINE_STRING( BGl_string2767z00zz__threadz00, BgL_bgl_string2767za700za7za7_2905za7, "&thread-specific-set!", 21 );
DEFINE_STRING( BGl_string2849z00zz__threadz00, BgL_bgl_string2849za700za7za7_2906za7, "&thread-terminate!-no1240", 25 );
DEFINE_STRING( BGl_string2768z00zz__threadz00, BgL_bgl_string2768za700za7za7_2907za7, "thread-cleanup:Wrong number of arguments", 40 );
DEFINE_STRING( BGl_string2687z00zz__threadz00, BgL_bgl_string2687za700za7za7_2908za7, "method1188", 10 );
DEFINE_STRING( BGl_string2689z00zz__threadz00, BgL_bgl_string2689za700za7za7_2909za7, "&tb-condvar-initialize!", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dynamiczd2envzf3zd2envzf3zz__threadz00, BgL_bgl_za762dynamicza7d2env2910z00, BGl_z62dynamiczd2envzf3z43zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conditionzd2variablezd2waitz12zd2envzc0zz__threadz00, BgL_bgl__conditionza7d2var2911za7, opt_generic_entry, BGl__conditionzd2variablezd2waitz12z12zz__threadz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conditionzd2variablezd2signalz12zd2envzc0zz__threadz00, BgL_bgl_za762conditionza7d2v2912z00, BGl_z62conditionzd2variablezd2signalz12z70zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2850z00zz__threadz00, BgL_bgl_string2850za700za7za7_2913za7, "thread-join!-nothrea1238", 24 );
DEFINE_STRING( BGl_string2851z00zz__threadz00, BgL_bgl_string2851za700za7za7_2914za7, "&thread-join!-nothrea1238", 25 );
DEFINE_STRING( BGl_string2852z00zz__threadz00, BgL_bgl_string2852za700za7za7_2915za7, "&thread-start-joinabl1236", 25 );
DEFINE_STRING( BGl_string2771z00zz__threadz00, BgL_bgl_string2771za700za7za7_2916za7, "method1214", 10 );
DEFINE_STRING( BGl_string2690z00zz__threadz00, BgL_bgl_string2690za700za7za7_2917za7, "thread-initialize!:Wrong number of arguments", 44 );
DEFINE_STRING( BGl_string2853z00zz__threadz00, BgL_bgl_string2853za700za7za7_2918za7, "&thread-start!-nothre1234", 25 );
DEFINE_STRING( BGl_string2772z00zz__threadz00, BgL_bgl_string2772za700za7za7_2919za7, "&thread-cleanup", 15 );
DEFINE_STRING( BGl_string2854z00zz__threadz00, BgL_bgl_string2854za700za7za7_2920za7, "<@exit:1557>~0:Wrong number of arguments", 40 );
DEFINE_STRING( BGl_string2773z00zz__threadz00, BgL_bgl_string2773za700za7za7_2921za7, "thread-cleanup-set!:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string2693z00zz__threadz00, BgL_bgl_string2693za700za7za7_2922za7, "method1196", 10 );
DEFINE_STRING( BGl_string2857z00zz__threadz00, BgL_bgl_string2857za700za7za7_2923za7, "fun1558", 7 );
DEFINE_STRING( BGl_string2776z00zz__threadz00, BgL_bgl_string2776za700za7za7_2924za7, "method1217", 10 );
DEFINE_STRING( BGl_string2695z00zz__threadz00, BgL_bgl_string2695za700za7za7_2925za7, "th", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2threadzd2backendzd2setz12zd2envz12zz__threadz00, BgL_bgl_za762currentza7d2thr2926z00, BGl_z62currentzd2threadzd2backendzd2setz12za2zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2858z00zz__threadz00, BgL_bgl_string2858za700za7za7_2927za7, "<@exit:1557>~0", 14 );
DEFINE_STRING( BGl_string2777z00zz__threadz00, BgL_bgl_string2777za700za7za7_2928za7, "&thread-cleanup-set!", 20 );
DEFINE_STRING( BGl_string2696z00zz__threadz00, BgL_bgl_string2696za700za7za7_2929za7, "&thread-initialize!", 19 );
DEFINE_STRING( BGl_string2859z00zz__threadz00, BgL_bgl_string2859za700za7za7_2930za7, "thread-start!-nothre1234~0", 26 );
DEFINE_STRING( BGl_string2778z00zz__threadz00, BgL_bgl_string2778za700za7za7_2931za7, "thread-name", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conditionzd2variablezf3zd2envzf3zz__threadz00, BgL_bgl_za762conditionza7d2v2932z00, BGl_z62conditionzd2variablezf3z43zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_defaultzd2threadzd2backendzd2setz12zd2envz12zz__threadz00, BgL_bgl_za762defaultza7d2thr2933z00, BGl_z62defaultzd2threadzd2backendzd2setz12za2zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2779z00zz__threadz00, BgL_bgl_string2779za700za7za7_2934za7, "thread-name:Wrong number of arguments", 37 );
DEFINE_STRING( BGl_string2698z00zz__threadz00, BgL_bgl_string2698za700za7za7_2935za7, "thread-start!", 13 );
DEFINE_STRING( BGl_string2699z00zz__threadz00, BgL_bgl_string2699za700za7za7_2936za7, "Wrong number of arguments", 25 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2namezd2envz00zz__threadz00, BgL_bgl_za762threadza7d2name2937z00, BGl_z62threadzd2namezb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2spinlockzd2envz00zz__threadz00, BgL_bgl__makeza7d2spinlock2938za7, opt_generic_entry, BGl__makezd2spinlockzd2zz__threadz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2860z00zz__threadz00, BgL_bgl_string2860za700za7za7_2939za7, "&object-print-thread1194", 24 );
DEFINE_STRING( BGl_string2861z00zz__threadz00, BgL_bgl_string2861za700za7za7_2940za7, "object-write-thread1192", 23 );
DEFINE_STRING( BGl_string2862z00zz__threadz00, BgL_bgl_string2862za700za7za7_2941za7, "output-port", 11 );
DEFINE_STRING( BGl_string2863z00zz__threadz00, BgL_bgl_string2863za700za7za7_2942za7, "&object-write-thread1192", 24 );
DEFINE_STRING( BGl_string2782z00zz__threadz00, BgL_bgl_string2782za700za7za7_2943za7, "method1219", 10 );
DEFINE_STRING( BGl_string2864z00zz__threadz00, BgL_bgl_string2864za700za7za7_2944za7, "<@anonymous:1542>", 17 );
DEFINE_STRING( BGl_string2783z00zz__threadz00, BgL_bgl_string2783za700za7za7_2945za7, "&thread-name", 12 );
DEFINE_STRING( BGl_string2865z00zz__threadz00, BgL_bgl_string2865za700za7za7_2946za7, "class", 5 );
DEFINE_STRING( BGl_string2784z00zz__threadz00, BgL_bgl_string2784za700za7za7_2947za7, "thread-name-set!:Wrong number of arguments", 42 );
DEFINE_STRING( BGl_string2866z00zz__threadz00, BgL_bgl_string2866za700za7za7_2948za7, ">", 1 );
DEFINE_STRING( BGl_string2867z00zz__threadz00, BgL_bgl_string2867za700za7za7_2949za7, ":", 1 );
DEFINE_STRING( BGl_string2868z00zz__threadz00, BgL_bgl_string2868za700za7za7_2950za7, "#<", 2 );
DEFINE_STRING( BGl_string2787z00zz__threadz00, BgL_bgl_string2787za700za7za7_2951za7, "method1221", 10 );
DEFINE_STRING( BGl_string2869z00zz__threadz00, BgL_bgl_string2869za700za7za7_2952za7, "object-display-threa1190", 24 );
DEFINE_STRING( BGl_string2788z00zz__threadz00, BgL_bgl_string2788za700za7za7_2953za7, "&thread-name-set!", 17 );
DEFINE_STRING( BGl_string2789z00zz__threadz00, BgL_bgl_string2789za700za7za7_2954za7, "%user-current-thread:Wrong number of arguments", 46 );
DEFINE_EXPORT_BGL_GENERIC( BGl_tbzd2makezd2threadzd2envzd2zz__threadz00, BgL_bgl_za762tbza7d2makeza7d2t2955za7, BGl_z62tbzd2makezd2threadz62zz__threadz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conditionzd2variablezd2namezd2envzd2zz__threadz00, BgL_bgl_za762conditionza7d2v2956z00, BGl_z62conditionzd2variablezd2namez62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2threadzd2envz00zz__threadz00, BgL_bgl_za762currentza7d2thr2957z00, BGl_z62currentzd2threadzb0zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2cleanupzd2envz00zz__threadz00, BgL_bgl_za762threadza7d2clea2958z00, BGl_z62threadzd2cleanupzb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2870z00zz__threadz00, BgL_bgl_string2870za700za7za7_2959za7, "&object-display-threa1190", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mutexzf3zd2envz21zz__threadz00, BgL_bgl_za762mutexza7f3za791za7za72960za7, BGl_z62mutexzf3z91zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2871z00zz__threadz00, BgL_bgl_string2871za700za7za7_2961za7, "<@anonymous:1525>", 17 );
DEFINE_STRING( BGl_string2872z00zz__threadz00, BgL_bgl_string2872za700za7za7_2962za7, "&tb-current-thread-no1184", 25 );
DEFINE_STRING( BGl_string2873z00zz__threadz00, BgL_bgl_string2873za700za7za7_2963za7, "tb-make-thread-nothr1182", 24 );
DEFINE_STRING( BGl_string2792z00zz__threadz00, BgL_bgl_string2792za700za7za7_2964za7, "method1228", 10 );
DEFINE_STRING( BGl_string2874z00zz__threadz00, BgL_bgl_string2874za700za7za7_2965za7, "tb-make-thread-nothr1182:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string2794z00zz__threadz00, BgL_bgl_string2794za700za7za7_2966za7, "o", 1 );
DEFINE_STRING( BGl_string2795z00zz__threadz00, BgL_bgl_string2795za700za7za7_2967za7, "&%user-current-thread", 21 );
DEFINE_STRING( BGl_string2877z00zz__threadz00, BgL_bgl_string2877za700za7za7_2968za7, "fun1519", 7 );
DEFINE_STRING( BGl_string2796z00zz__threadz00, BgL_bgl_string2796za700za7za7_2969za7, "%user-thread-sleep!:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string2879z00zz__threadz00, BgL_bgl_string2879za700za7za7_2970za7, "new1062", 7 );
DEFINE_STRING( BGl_string2799z00zz__threadz00, BgL_bgl_string2799za700za7za7_2971za7, "method1230", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_threadzd2parameterzd2envz00zz__threadz00, BgL_bgl_za762threadza7d2para2972z00, BGl_z62threadzd2parameterzb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2880z00zz__threadz00, BgL_bgl_string2880za700za7za7_2973za7, "&tb-make-thread-nothr1182", 25 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2startz12zd2envz12zz__threadz00, BgL_bgl_za762threadza7d2star2974z00, va_generic_entry, BGl_z62threadzd2startz12za2zz__threadz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mutexzd2nilzd2envz00zz__threadz00, BgL_bgl_za762mutexza7d2nilza7b2975za7, BGl_z62mutexzd2nilzb0zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2threadzd2envz00zz__threadz00, BgL_bgl__makeza7d2threadza7d2976z00, opt_generic_entry, BGl__makezd2threadzd2zz__threadz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2411z00zz__threadz00, BgL_bgl_string2411za700za7za7_2977za7, "nothread", 8 );
DEFINE_STRING( BGl_string2412z00zz__threadz00, BgL_bgl_string2412za700za7za7_2978za7, "/tmp/bigloo/runtime/Llib/thread.scm", 35 );
DEFINE_STRING( BGl_string2413z00zz__threadz00, BgL_bgl_string2413za700za7za7_2979za7, "&current-thread-backend-set!", 28 );
DEFINE_STRING( BGl_string2414z00zz__threadz00, BgL_bgl_string2414za700za7za7_2980za7, "thread-backend", 14 );
DEFINE_STRING( BGl_string2415z00zz__threadz00, BgL_bgl_string2415za700za7za7_2981za7, "default-thread-backend", 22 );
DEFINE_STRING( BGl_string2416z00zz__threadz00, BgL_bgl_string2416za700za7za7_2982za7, "pair", 4 );
DEFINE_STRING( BGl_string2417z00zz__threadz00, BgL_bgl_string2417za700za7za7_2983za7, "&default-thread-backend-set!", 28 );
DEFINE_STRING( BGl_string2418z00zz__threadz00, BgL_bgl_string2418za700za7za7_2984za7, "loop", 4 );
DEFINE_STRING( BGl_string2419z00zz__threadz00, BgL_bgl_string2419za700za7za7_2985za7, "&get-thread-backend", 19 );
DEFINE_EXPORT_BGL_GENERIC( BGl_z52userzd2currentzd2threadzd2envz80zz__threadz00, BgL_bgl_za762za752userza7d2cur2986za7, BGl_z62z52userzd2currentzd2threadz30zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2501z00zz__threadz00, BgL_bgl_string2501za700za7za7_2987za7, "cleanup", 7 );
DEFINE_STRING( BGl_string2420z00zz__threadz00, BgL_bgl_string2420za700za7za7_2988za7, "bstring", 7 );
DEFINE_STRING( BGl_string2422z00zz__threadz00, BgL_bgl_string2422za700za7za7_2989za7, "make-thread", 11 );
DEFINE_STRING( BGl_string2423z00zz__threadz00, BgL_bgl_string2423za700za7za7_2990za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string2425z00zz__threadz00, BgL_bgl_string2425za700za7za7_2991za7, "thread", 6 );
DEFINE_STRING( BGl_string2426z00zz__threadz00, BgL_bgl_string2426za700za7za7_2992za7, "_make-thread", 12 );
extern obj_t BGl_objectzd2writezd2envz00zz__objectz00;
DEFINE_STRING( BGl_string2427z00zz__threadz00, BgL_bgl_string2427za700za7za7_2993za7, "procedure", 9 );
DEFINE_STRING( BGl_string2428z00zz__threadz00, BgL_bgl_string2428za700za7za7_2994za7, "%current-thread", 15 );
DEFINE_STRING( BGl_string2429z00zz__threadz00, BgL_bgl_string2429za700za7za7_2995za7, "vector", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mutexzd2statezd2envz00zz__threadz00, BgL_bgl_za762mutexza7d2state2996z00, BGl_z62mutexzd2statezb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2502z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo2997za7, BGl_z62zc3z04anonymousza31401ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2503z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo2998za7, BGl_z62zc3z04anonymousza31403ze3ze5zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2504z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo2999za7, BGl_z62zc3z04anonymousza31409ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2511z00zz__threadz00, BgL_bgl_string2511za700za7za7_3000za7, "body", 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2505z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3001za7, BGl_z62zc3z04anonymousza31411ze3ze5zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2430z00zz__threadz00, BgL_bgl_string2430za700za7za7_3002za7, "vector-ref", 10 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2506z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3003za7, BGl_z62zc3z04anonymousza31376ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2431z00zz__threadz00, BgL_bgl_string2431za700za7za7_3004za7, "current-thread", 14 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2507z00zz__threadz00, BgL_bgl_za762lambda1374za7623005z00, BGl_z62lambda1374z62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2432z00zz__threadz00, BgL_bgl_string2432za700za7za7_3006za7, "thread-sleep!", 13 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2508z00zz__threadz00, BgL_bgl_za762lambda1428za7623007z00, BGl_z62lambda1428z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2433z00zz__threadz00, BgL_bgl_string2433za700za7za7_3008za7, "thread-yield!", 13 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2509z00zz__threadz00, BgL_bgl_za762lambda1427za7623009z00, BGl_z62lambda1427z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2434z00zz__threadz00, BgL_bgl_string2434za700za7za7_3010za7, "thread-parameter", 16 );
DEFINE_STRING( BGl_string2435z00zz__threadz00, BgL_bgl_string2435za700za7za7_3011za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2517z00zz__threadz00, BgL_bgl_string2517za700za7za7_3012za7, "%specific", 9 );
DEFINE_STRING( BGl_string2436z00zz__threadz00, BgL_bgl_string2436za700za7za7_3013za7, "&thread-parameter", 17 );
DEFINE_STRING( BGl_string2437z00zz__threadz00, BgL_bgl_string2437za700za7za7_3014za7, "symbol", 6 );
DEFINE_STRING( BGl_string2438z00zz__threadz00, BgL_bgl_string2438za700za7za7_3015za7, "thread-parameter-set!", 21 );
DEFINE_STRING( BGl_string2439z00zz__threadz00, BgL_bgl_string2439za700za7za7_3016za7, "&thread-parameter-set!", 22 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00, BgL_bgl_za762threadza7d2init3017z00, BGl_z62threadzd2initializa7ez12z05zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2threadzd2backendzd2envzd2zz__threadz00, BgL_bgl_za762currentza7d2thr3018z00, BGl_z62currentzd2threadzd2backendz62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2513z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3019za7, BGl_z62zc3z04anonymousza31437ze3ze5zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2601z00zz__threadz00, BgL_bgl_string2601za700za7za7_3020za7, "thread-start-joinabl1199", 24 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2514z00zz__threadz00, BgL_bgl_za762lambda1436za7623021z00, BGl_z62lambda1436z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conditionzd2variablezd2nilzd2envzd2zz__threadz00, BgL_bgl_za762conditionza7d2v3022z00, BGl_z62conditionzd2variablezd2nilz62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mutexzd2lockz12zd2envz12zz__threadz00, BgL_bgl__mutexza7d2lockza7123023z00, opt_generic_entry, BGl__mutexzd2lockz12zc0zz__threadz00, BFALSE, -1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2515z00zz__threadz00, BgL_bgl_za762lambda1435za7623024z00, BGl_z62lambda1435z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2dynamiczd2envzd2envzd2zz__threadz00, BgL_bgl_za762currentza7d2dyn3025z00, BGl_z62currentzd2dynamiczd2envz62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2603z00zz__threadz00, BgL_bgl_string2603za700za7za7_3026za7, "thread-join!1202", 16 );
DEFINE_STRING( BGl_string2522z00zz__threadz00, BgL_bgl_string2522za700za7za7_3027za7, "%cleanup", 8 );
DEFINE_STRING( BGl_string2441z00zz__threadz00, BgL_bgl_string2441za700za7za7_3028za7, "make-mutex", 10 );
DEFINE_STRING( BGl_string2442z00zz__threadz00, BgL_bgl_string2442za700za7za7_3029za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string2605z00zz__threadz00, BgL_bgl_string2605za700za7za7_3030za7, "thread-terminate!1204", 21 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2518z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3031za7, BGl_z62zc3z04anonymousza31444ze3ze5zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2519z00zz__threadz00, BgL_bgl_za762lambda1443za7623032z00, BGl_z62lambda1443z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2444z00zz__threadz00, BgL_bgl_string2444za700za7za7_3033za7, "mutex", 5 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2terminatez12zd2envz12zz__threadz00, BgL_bgl_za762threadza7d2term3034z00, BGl_z62threadzd2terminatez12za2zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2607z00zz__threadz00, BgL_bgl_string2607za700za7za7_3035za7, "thread-kill!1207", 16 );
DEFINE_STRING( BGl_string2527z00zz__threadz00, BgL_bgl_string2527za700za7za7_3036za7, "end-result", 10 );
DEFINE_STRING( BGl_string2446z00zz__threadz00, BgL_bgl_string2446za700za7za7_3037za7, "make-spinlock", 13 );
DEFINE_STRING( BGl_string2609z00zz__threadz00, BgL_bgl_string2609za700za7za7_3038za7, "thread-specific1209", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2lockzd2envz00zz__threadz00, BgL_bgl_za762withza7d2lockza7b3039za7, BGl_z62withzd2lockzb0zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2448z00zz__threadz00, BgL_bgl_string2448za700za7za7_3040za7, "spinlock", 8 );
DEFINE_STRING( BGl_string2449z00zz__threadz00, BgL_bgl_string2449za700za7za7_3041za7, "&mutex-name", 11 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00, BgL_bgl_za762threadza7d2name3042z00, BGl_z62threadzd2namezd2setz12z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mutexzd2namezd2envz00zz__threadz00, BgL_bgl_za762mutexza7d2nameza73043za7, BGl_z62mutexzd2namezb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2600z00zz__threadz00, BgL_bgl_za762threadza7d2star3044z00, BGl_z62threadzd2startzd2joinabl1199z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2520z00zz__threadz00, BgL_bgl_za762lambda1442za7623045z00, BGl_z62lambda1442z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2602z00zz__threadz00, BgL_bgl_za762threadza7d2join3046z00, va_generic_entry, BGl_z62threadzd2joinz121202za2zz__threadz00, BUNSPEC, -2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2604z00zz__threadz00, BgL_bgl_za762threadza7d2term3047z00, BGl_z62threadzd2terminatez121204za2zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2523z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3048za7, BGl_z62zc3z04anonymousza31451ze3ze5zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2611z00zz__threadz00, BgL_bgl_string2611za700za7za7_3049za7, "thread-specific-set!1211", 24 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2524z00zz__threadz00, BgL_bgl_za762lambda1450za7623050z00, BGl_z62lambda1450z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2606z00zz__threadz00, BgL_bgl_za762threadza7d2kill3051z00, BGl_z62threadzd2killz121207za2zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2525z00zz__threadz00, BgL_bgl_za762lambda1449za7623052z00, BGl_z62lambda1449z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2613z00zz__threadz00, BgL_bgl_string2613za700za7za7_3053za7, "thread-cleanup1213", 18 );
DEFINE_STRING( BGl_string2532z00zz__threadz00, BgL_bgl_string2532za700za7za7_3054za7, "end-exception", 13 );
DEFINE_STRING( BGl_string2451z00zz__threadz00, BgL_bgl_string2451za700za7za7_3055za7, "mutex-lock!", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mutexzd2unlockz12zd2envz12zz__threadz00, BgL_bgl_za762mutexza7d2unloc3056z00, BGl_z62mutexzd2unlockz12za2zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2608z00zz__threadz00, BgL_bgl_za762threadza7d2spec3057z00, BGl_z62threadzd2specific1209zb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2452z00zz__threadz00, BgL_bgl_string2452za700za7za7_3058za7, "_mutex-lock!", 12 );
DEFINE_STRING( BGl_string2615z00zz__threadz00, BgL_bgl_string2615za700za7za7_3059za7, "thread-cleanup-set!1215", 23 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2528z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3060za7, BGl_z62zc3z04anonymousza31458ze3ze5zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2453z00zz__threadz00, BgL_bgl_string2453za700za7za7_3061za7, "bint", 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2529z00zz__threadz00, BgL_bgl_za762lambda1457za7623062z00, BGl_z62lambda1457z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2454z00zz__threadz00, BgL_bgl_string2454za700za7za7_3063za7, "&mutex-unlock!", 14 );
DEFINE_STRING( BGl_string2617z00zz__threadz00, BgL_bgl_string2617za700za7za7_3064za7, "thread-name1218", 15 );
DEFINE_STRING( BGl_string2455z00zz__threadz00, BgL_bgl_string2455za700za7za7_3065za7, "&mutex-state", 12 );
DEFINE_STRING( BGl_string2537z00zz__threadz00, BgL_bgl_string2537za700za7za7_3066za7, "%name", 5 );
DEFINE_STRING( BGl_string2456z00zz__threadz00, BgL_bgl_string2456za700za7za7_3067za7, "with-lock:Wrong number of arguments", 35 );
DEFINE_STRING( BGl_string2619z00zz__threadz00, BgL_bgl_string2619za700za7za7_3068za7, "thread-name-set!1220", 20 );
extern obj_t BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00;
DEFINE_STRING( BGl_string2459z00zz__threadz00, BgL_bgl_string2459za700za7za7_3069za7, "funcall", 7 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2joinz12zd2envz12zz__threadz00, BgL_bgl_za762threadza7d2join3070z00, va_generic_entry, BGl_z62threadzd2joinz12za2zz__threadz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_threadzd2yieldz12zd2envz12zz__threadz00, BgL_bgl_za762threadza7d2yiel3071z00, BGl_z62threadzd2yieldz12za2zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2mutexzd2envz00zz__threadz00, BgL_bgl__makeza7d2mutexza7d23072z00, opt_generic_entry, BGl__makezd2mutexzd2zz__threadz00, BFALSE, -1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2610z00zz__threadz00, BgL_bgl_za762threadza7d2spec3073z00, BGl_z62threadzd2specificzd2setz121211z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2530z00zz__threadz00, BgL_bgl_za762lambda1456za7623074z00, BGl_z62lambda1456z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2612z00zz__threadz00, BgL_bgl_za762threadza7d2clea3075z00, BGl_z62threadzd2cleanup1213zb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2614z00zz__threadz00, BgL_bgl_za762threadza7d2clea3076z00, BGl_z62threadzd2cleanupzd2setz121215z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2533z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3077za7, BGl_z62zc3z04anonymousza31465ze3ze5zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_GENERIC( BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00, BgL_bgl_za762tbza7d2currentza73078za7, BGl_z62tbzd2currentzd2threadz62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2702z00zz__threadz00, BgL_bgl_string2702za700za7za7_3079za7, "apply", 5 );
DEFINE_STRING( BGl_string2621z00zz__threadz00, BgL_bgl_string2621za700za7za7_3080za7, "%user-current-thread1227", 24 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2534z00zz__threadz00, BgL_bgl_za762lambda1464za7623081z00, BGl_z62lambda1464z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2616z00zz__threadz00, BgL_bgl_za762threadza7d2name3082z00, BGl_z62threadzd2name1218zb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2535z00zz__threadz00, BgL_bgl_za762lambda1463za7623083z00, BGl_z62lambda1463z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2704z00zz__threadz00, BgL_bgl_string2704za700za7za7_3084za7, "method1198", 10 );
DEFINE_STRING( BGl_string2623z00zz__threadz00, BgL_bgl_string2623za700za7za7_3085za7, "%user-thread-sleep!1229", 23 );
DEFINE_STRING( BGl_string2461z00zz__threadz00, BgL_bgl_string2461za700za7za7_3086za7, "thunk", 5 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2618z00zz__threadz00, BgL_bgl_za762threadza7d2name3087z00, BGl_z62threadzd2namezd2setz121220z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2462z00zz__threadz00, BgL_bgl_string2462za700za7za7_3088za7, "&with-lock", 10 );
DEFINE_STRING( BGl_string2625z00zz__threadz00, BgL_bgl_string2625za700za7za7_3089za7, "%user-thread-yield!1231", 23 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2538z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3090za7, BGl_z62zc3z04anonymousza31423ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2463z00zz__threadz00, BgL_bgl_string2463za700za7za7_3091za7, "with-timed-lock:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string2707z00zz__threadz00, BgL_bgl_string2707za700za7za7_3092za7, "let", 3 );
DEFINE_STRING( BGl_string2626z00zz__threadz00, BgL_bgl_string2626za700za7za7_3093za7, "&%user-thread-yield!1231", 24 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2539z00zz__threadz00, BgL_bgl_za762lambda1421za7623094z00, BGl_z62lambda1421z62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2464z00zz__threadz00, BgL_bgl_string2464za700za7za7_3095za7, "with-timed-lock", 15 );
DEFINE_STRING( BGl_string2627z00zz__threadz00, BgL_bgl_string2627za700za7za7_3096za7, "belong", 6 );
DEFINE_STRING( BGl_string2546z00zz__threadz00, BgL_bgl_string2546za700za7za7_3097za7, "nothread-backend", 16 );
DEFINE_STRING( BGl_string2465z00zz__threadz00, BgL_bgl_string2465za700za7za7_3098za7, "&with-timed-lock", 16 );
DEFINE_STRING( BGl_string2547z00zz__threadz00, BgL_bgl_string2547za700za7za7_3099za7, "", 0 );
DEFINE_STRING( BGl_string2629z00zz__threadz00, BgL_bgl_string2629za700za7za7_3100za7, "date, real, or integer", 22 );
DEFINE_STRING( BGl_string2548z00zz__threadz00, BgL_bgl_string2548za700za7za7_3101za7, "&<@anonymous:1477>", 18 );
DEFINE_STRING( BGl_string2467z00zz__threadz00, BgL_bgl_string2467za700za7za7_3102za7, "make-condition-variable", 23 );
DEFINE_STRING( BGl_string2549z00zz__threadz00, BgL_bgl_string2549za700za7za7_3103za7, "&lambda1473", 11 );
DEFINE_STRING( BGl_string2469z00zz__threadz00, BgL_bgl_string2469za700za7za7_3104za7, "condition-variable", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_threadzd2parameterzd2setz12zd2envzc0zz__threadz00, BgL_bgl_za762threadza7d2para3105z00, BGl_z62threadzd2parameterzd2setz12z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2620z00zz__threadz00, BgL_bgl_za762za752userza7d2cur3106za7, BGl_z62z52userzd2currentzd2thread1227z30zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2540z00zz__threadz00, BgL_bgl_za762lambda1418za7623107z00, BGl_z62lambda1418z62zz__threadz00, 0L, BUNSPEC, 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2622z00zz__threadz00, BgL_bgl_za762za752userza7d2thr3108za7, BGl_z62z52userzd2threadzd2sleepz121229z22zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2542z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3109za7, BGl_z62zc3z04anonymousza31477ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2711z00zz__threadz00, BgL_bgl_string2711za700za7za7_3110za7, "list1517", 8 );
DEFINE_STRING( BGl_string2630z00zz__threadz00, BgL_bgl_string2630za700za7za7_3111za7, "&%user-thread-sleep!1229", 24 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2624z00zz__threadz00, BgL_bgl_za762za752userza7d2thr3112za7, BGl_z62z52userzd2threadzd2yieldz121231z22zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2543z00zz__threadz00, BgL_bgl_za762lambda1475za7623113z00, BGl_z62lambda1475z62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2631z00zz__threadz00, BgL_bgl_string2631za700za7za7_3114za7, "&%user-current-thread1227", 25 );
DEFINE_STRING( BGl_string2550z00zz__threadz00, BgL_bgl_string2550za700za7za7_3115za7, "&<@anonymous:1423>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2544z00zz__threadz00, BgL_bgl_za762lambda1473za7623116z00, BGl_z62lambda1473z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2551z00zz__threadz00, BgL_bgl_string2551za700za7za7_3117za7, "lambda1418", 10 );
DEFINE_STRING( BGl_string2470z00zz__threadz00, BgL_bgl_string2470za700za7za7_3118za7, "&condition-variable-name", 24 );
DEFINE_EXPORT_BGL_GENERIC( BGl_z52userzd2threadzd2yieldz12zd2envz92zz__threadz00, BgL_bgl_za762za752userza7d2thr3119za7, BGl_z62z52userzd2threadzd2yieldz12z22zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2714z00zz__threadz00, BgL_bgl_string2714za700za7za7_3120za7, "$cons", 5 );
DEFINE_STRING( BGl_string2633z00zz__threadz00, BgL_bgl_string2633za700za7za7_3121za7, "No method for this object", 25 );
DEFINE_STRING( BGl_string2552z00zz__threadz00, BgL_bgl_string2552za700za7za7_3122za7, "lambda1418:Wrong number of arguments", 36 );
DEFINE_STRING( BGl_string2471z00zz__threadz00, BgL_bgl_string2471za700za7za7_3123za7, "condvar", 7 );
DEFINE_STRING( BGl_string2634z00zz__threadz00, BgL_bgl_string2634za700za7za7_3124za7, "&thread-name-set!1220", 21 );
DEFINE_STRING( BGl_string2716z00zz__threadz00, BgL_bgl_string2716za700za7za7_3125za7, "sc", 2 );
DEFINE_STRING( BGl_string2473z00zz__threadz00, BgL_bgl_string2473za700za7za7_3126za7, "condition-variable-wait!", 24 );
DEFINE_STRING( BGl_string2636z00zz__threadz00, BgL_bgl_string2636za700za7za7_3127za7, "&thread-name1218", 16 );
DEFINE_STRING( BGl_string2555z00zz__threadz00, BgL_bgl_string2555za700za7za7_3128za7, "fun1419", 7 );
DEFINE_STRING( BGl_string2474z00zz__threadz00, BgL_bgl_string2474za700za7za7_3129za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2timedzd2lockzd2envzd2zz__threadz00, BgL_bgl_za762withza7d2timedza73130za7, BGl_z62withzd2timedzd2lockz62zz__threadz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2475z00zz__threadz00, BgL_bgl_string2475za700za7za7_3131za7, "_condition-variable-wait!", 25 );
DEFINE_STRING( BGl_string2719z00zz__threadz00, BgL_bgl_string2719za700za7za7_3132za7, "quote", 5 );
DEFINE_STRING( BGl_string2638z00zz__threadz00, BgL_bgl_string2638za700za7za7_3133za7, "&thread-cleanup-set!1215", 24 );
DEFINE_STRING( BGl_string2557z00zz__threadz00, BgL_bgl_string2557za700za7za7_3134za7, "new1115", 7 );
DEFINE_STRING( BGl_string2476z00zz__threadz00, BgL_bgl_string2476za700za7za7_3135za7, "&condition-variable-signal!", 27 );
DEFINE_STRING( BGl_string2558z00zz__threadz00, BgL_bgl_string2558za700za7za7_3136za7, "&lambda1418", 11 );
DEFINE_STRING( BGl_string2477z00zz__threadz00, BgL_bgl_string2477za700za7za7_3137za7, "&condition-variable-broadcast!", 30 );
DEFINE_STRING( BGl_string2559z00zz__threadz00, BgL_bgl_string2559za700za7za7_3138za7, "bigloo", 6 );
DEFINE_EXPORT_BGL_GENERIC( BGl_tbzd2mutexzd2initializa7ez12zd2envz67zz__threadz00, BgL_bgl_za762tbza7d2mutexza7d23139za7, BGl_z62tbzd2mutexzd2initializa7ez12zd7zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00, BgL_bgl_za762threadza7d2spec3140z00, BGl_z62threadzd2specificzd2setz12z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2specificzd2envz00zz__threadz00, BgL_bgl_za762threadza7d2spec3141z00, BGl_z62threadzd2specificzb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2801z00zz__threadz00, BgL_bgl_string2801za700za7za7_3142za7, "d", 1 );
DEFINE_STRING( BGl_string2802z00zz__threadz00, BgL_bgl_string2802za700za7za7_3143za7, "&%user-thread-sleep!", 20 );
DEFINE_STRING( BGl_string2640z00zz__threadz00, BgL_bgl_string2640za700za7za7_3144za7, "&thread-cleanup1213", 19 );
DEFINE_STRING( BGl_string2803z00zz__threadz00, BgL_bgl_string2803za700za7za7_3145za7, "%user-thread-yield!:Wrong number of arguments", 45 );
DEFINE_STRING( BGl_string2722z00zz__threadz00, BgL_bgl_string2722za700za7za7_3146za7, "cons*", 5 );
DEFINE_STRING( BGl_string2560z00zz__threadz00, BgL_bgl_string2560za700za7za7_3147za7, "&lambda1464", 11 );
DEFINE_STRING( BGl_string2723z00zz__threadz00, BgL_bgl_string2723za700za7za7_3148za7, "&thread-start!", 14 );
DEFINE_STRING( BGl_string2642z00zz__threadz00, BgL_bgl_string2642za700za7za7_3149za7, "&thread-specific-set!1211", 25 );
DEFINE_STRING( BGl_string2561z00zz__threadz00, BgL_bgl_string2561za700za7za7_3150za7, "&lambda1463", 11 );
DEFINE_STRING( BGl_string2724z00zz__threadz00, BgL_bgl_string2724za700za7za7_3151za7, "thread-start-joinable!:Wrong number of arguments", 48 );
DEFINE_STRING( BGl_string2562z00zz__threadz00, BgL_bgl_string2562za700za7za7_3152za7, "&lambda1457", 11 );
DEFINE_STRING( BGl_string2481z00zz__threadz00, BgL_bgl_string2481za700za7za7_3153za7, "name", 4 );
DEFINE_STRING( BGl_string2806z00zz__threadz00, BgL_bgl_string2806za700za7za7_3154za7, "method1232", 10 );
DEFINE_STRING( BGl_string2644z00zz__threadz00, BgL_bgl_string2644za700za7za7_3155za7, "&thread-specific1209", 20 );
DEFINE_STRING( BGl_string2563z00zz__threadz00, BgL_bgl_string2563za700za7za7_3156za7, "&lambda1456", 11 );
extern obj_t BGl_objectzd2printzd2envz00zz__objectz00;
DEFINE_STRING( BGl_string2807z00zz__threadz00, BgL_bgl_string2807za700za7za7_3157za7, "&%user-thread-yield!", 20 );
DEFINE_STRING( BGl_string2564z00zz__threadz00, BgL_bgl_string2564za700za7za7_3158za7, "&lambda1450", 11 );
DEFINE_STRING( BGl_string2727z00zz__threadz00, BgL_bgl_string2727za700za7za7_3159za7, "method1201", 10 );
DEFINE_STRING( BGl_string2646z00zz__threadz00, BgL_bgl_string2646za700za7za7_3160za7, "&thread-kill!1207", 17 );
DEFINE_STRING( BGl_string2565z00zz__threadz00, BgL_bgl_string2565za700za7za7_3161za7, "&lambda1449", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2478z00zz__threadz00, BgL_bgl_za762lambda1369za7623162z00, BGl_z62lambda1369z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2728z00zz__threadz00, BgL_bgl_string2728za700za7za7_3163za7, "&thread-start-joinable!", 23 );
DEFINE_STRING( BGl_string2566z00zz__threadz00, BgL_bgl_string2566za700za7za7_3164za7, "&lambda1443", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2479z00zz__threadz00, BgL_bgl_za762lambda1368za7623165z00, BGl_z62lambda1368z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2648z00zz__threadz00, BgL_bgl_string2648za700za7za7_3166za7, "&thread-terminate!1204", 22 );
DEFINE_STRING( BGl_string2567z00zz__threadz00, BgL_bgl_string2567za700za7za7_3167za7, "&lambda1442", 11 );
DEFINE_STRING( BGl_string2568z00zz__threadz00, BgL_bgl_string2568za700za7za7_3168za7, "&lambda1436", 11 );
DEFINE_STRING( BGl_string2569z00zz__threadz00, BgL_bgl_string2569za700za7za7_3169za7, "&lambda1435", 11 );
DEFINE_STRING( BGl_string2488z00zz__threadz00, BgL_bgl_string2488za700za7za7_3170za7, "__thread", 8 );
extern obj_t BGl_objectzd2displayzd2envz00zz__objectz00;
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00, BgL_bgl_za762threadza7d2star3171z00, BGl_z62threadzd2startzd2joinablez12z70zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00, BgL_bgl_za762threadza7d2clea3172z00, BGl_z62threadzd2cleanupzd2setz12z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2810z00zz__threadz00, BgL_bgl_string2810za700za7za7_3173za7, "tb-current-thread", 17 );
DEFINE_STRING( BGl_string2730z00zz__threadz00, BgL_bgl_string2730za700za7za7_3174za7, "thread-join!", 12 );
DEFINE_STRING( BGl_string2812z00zz__threadz00, BgL_bgl_string2812za700za7za7_3175za7, "object-display", 14 );
DEFINE_STRING( BGl_string2650z00zz__threadz00, BgL_bgl_string2650za700za7za7_3176za7, "&thread-join!1202", 17 );
DEFINE_STRING( BGl_string2570z00zz__threadz00, BgL_bgl_string2570za700za7za7_3177za7, "&lambda1428", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2483z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3178za7, BGl_z62zc3z04anonymousza31364ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2814z00zz__threadz00, BgL_bgl_string2814za700za7za7_3179za7, "object-write", 12 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2808z00zz__threadz00, BgL_bgl_za762tbza7d2makeza7d2t3180za7, BGl_z62tbzd2makezd2threadzd2nothr1182zb0zz__threadz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2733z00zz__threadz00, BgL_bgl_string2733za700za7za7_3181za7, "method1203", 10 );
DEFINE_STRING( BGl_string2652z00zz__threadz00, BgL_bgl_string2652za700za7za7_3182za7, "&thread-start-joinabl1199", 25 );
DEFINE_STRING( BGl_string2571z00zz__threadz00, BgL_bgl_string2571za700za7za7_3183za7, "&lambda1427", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2484z00zz__threadz00, BgL_bgl_za762lambda1362za7623184z00, BGl_z62lambda1362z62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2809z00zz__threadz00, BgL_bgl_za762tbza7d2currentza73185za7, BGl_z62tbzd2currentzd2threadzd2no1184zb0zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2572z00zz__threadz00, BgL_bgl_string2572za700za7za7_3186za7, "&<@anonymous:1376>", 18 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2485z00zz__threadz00, BgL_bgl_za762lambda1360za7623187z00, BGl_z62lambda1360z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2816z00zz__threadz00, BgL_bgl_string2816za700za7za7_3188za7, "object-print", 12 );
DEFINE_STRING( BGl_string2654z00zz__threadz00, BgL_bgl_string2654za700za7za7_3189za7, "&thread-start!1197", 18 );
DEFINE_STRING( BGl_string2573z00zz__threadz00, BgL_bgl_string2573za700za7za7_3190za7, "<@anonymous:1411>", 17 );
DEFINE_STRING( BGl_string2655z00zz__threadz00, BgL_bgl_string2655za700za7za7_3191za7, "&thread-initialize!1195", 23 );
DEFINE_STRING( BGl_string2574z00zz__threadz00, BgL_bgl_string2574za700za7za7_3192za7, "<@anonymous:1409>", 17 );
DEFINE_STRING( BGl_string2493z00zz__threadz00, BgL_bgl_string2493za700za7za7_3193za7, "obj", 3 );
DEFINE_STRING( BGl_string2575z00zz__threadz00, BgL_bgl_string2575za700za7za7_3194za7, "<@anonymous:1403>", 17 );
DEFINE_STRING( BGl_string2819z00zz__threadz00, BgL_bgl_string2819za700za7za7_3195za7, "thread-start-joinable!", 22 );
DEFINE_STRING( BGl_string2738z00zz__threadz00, BgL_bgl_string2738za700za7za7_3196za7, "list1518", 8 );
DEFINE_STRING( BGl_string2657z00zz__threadz00, BgL_bgl_string2657za700za7za7_3197za7, "&tb-condvar-initializ1187", 25 );
DEFINE_STRING( BGl_string2576z00zz__threadz00, BgL_bgl_string2576za700za7za7_3198za7, "<@anonymous:1401>", 17 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2489z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3199za7, BGl_z62zc3z04anonymousza31383ze3ze5zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2577z00zz__threadz00, BgL_bgl_string2577za700za7za7_3200za7, "<@anonymous:1395>", 17 );
DEFINE_STRING( BGl_string2659z00zz__threadz00, BgL_bgl_string2659za700za7za7_3201za7, "&tb-mutex-initialize!1185", 25 );
DEFINE_STRING( BGl_string2578z00zz__threadz00, BgL_bgl_string2578za700za7za7_3202za7, "object", 6 );
DEFINE_STRING( BGl_string2497z00zz__threadz00, BgL_bgl_string2497za700za7za7_3203za7, "specific", 8 );
DEFINE_STRING( BGl_string2579z00zz__threadz00, BgL_bgl_string2579za700za7za7_3204za7, "<@anonymous:1394>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_conditionzd2variablezd2broadcastz12zd2envzc0zz__threadz00, BgL_bgl_za762conditionza7d2v3205z00, BGl_z62conditionzd2variablezd2broadcastz12z70zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_threadzd2sleepz12zd2envz12zz__threadz00, BgL_bgl_za762threadza7d2slee3206z00, BGl_z62threadzd2sleepz12za2zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_defaultzd2threadzd2backendzd2envzd2zz__threadz00, BgL_bgl_za762defaultza7d2thr3207z00, BGl_z62defaultzd2threadzd2backendz62zz__threadz00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2811z00zz__threadz00, BgL_bgl_za762objectza7d2disp3208z00, va_generic_entry, BGl_z62objectzd2displayzd2threa1190z62zz__threadz00, BUNSPEC, -2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2813z00zz__threadz00, BgL_bgl_za762objectza7d2writ3209z00, va_generic_entry, BGl_z62objectzd2writezd2thread1192z62zz__threadz00, BUNSPEC, -2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2490z00zz__threadz00, BgL_bgl_za762lambda1382za7623210z00, BGl_z62lambda1382z62zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2815z00zz__threadz00, BgL_bgl_za762objectza7d2prin3211z00, BGl_z62objectzd2printzd2thread1194z62zz__threadz00, 0L, BUNSPEC, 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2491z00zz__threadz00, BgL_bgl_za762lambda1381za7623212z00, BGl_z62lambda1381z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2822z00zz__threadz00, BgL_bgl_string2822za700za7za7_3213za7, "thread-terminate!", 17 );
DEFINE_STRING( BGl_string2741z00zz__threadz00, BgL_bgl_string2741za700za7za7_3214za7, "timeout", 7 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2817z00zz__threadz00, BgL_bgl_za762threadza7d2star3215z00, va_generic_entry, BGl_z62threadzd2startz12zd2nothre1234z70zz__threadz00, BUNSPEC, -2 );
DEFINE_STRING( BGl_string2661z00zz__threadz00, BgL_bgl_string2661za700za7za7_3216za7, "&tb-current-thread1179", 22 );
DEFINE_STRING( BGl_string2580z00zz__threadz00, BgL_bgl_string2580za700za7za7_3217za7, "<@anonymous:1390>", 17 );
DEFINE_STRING( BGl_string2824z00zz__threadz00, BgL_bgl_string2824za700za7za7_3218za7, "thread-kill!", 12 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2818z00zz__threadz00, BgL_bgl_za762threadza7d2star3219z00, BGl_z62threadzd2startzd2joinabl1236z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2743z00zz__threadz00, BgL_bgl_string2743za700za7za7_3220za7, "&thread-join!", 13 );
DEFINE_STRING( BGl_string2581z00zz__threadz00, BgL_bgl_string2581za700za7za7_3221za7, "<@anonymous:1389>", 17 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2494z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3222za7, BGl_z62zc3z04anonymousza31390ze3ze5zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2744z00zz__threadz00, BgL_bgl_string2744za700za7za7_3223za7, "thread-terminate!:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2663z00zz__threadz00, BgL_bgl_string2663za700za7za7_3224za7, "&tb-make-thread1177", 19 );
DEFINE_STRING( BGl_string2582z00zz__threadz00, BgL_bgl_string2582za700za7za7_3225za7, "&lambda1382", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2495z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3226za7, BGl_z62zc3z04anonymousza31389ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2826z00zz__threadz00, BgL_bgl_string2826za700za7za7_3227za7, "thread-specific", 15 );
DEFINE_STRING( BGl_string2664z00zz__threadz00, BgL_bgl_string2664za700za7za7_3228za7, "tb-make-thread", 14 );
DEFINE_STRING( BGl_string2583z00zz__threadz00, BgL_bgl_string2583za700za7za7_3229za7, "&lambda1381", 11 );
DEFINE_STRING( BGl_string2665z00zz__threadz00, BgL_bgl_string2665za700za7za7_3230za7, "tb-make-thread:Wrong number of arguments", 40 );
DEFINE_STRING( BGl_string2584z00zz__threadz00, BgL_bgl_string2584za700za7za7_3231za7, "&<@anonymous:1364>", 18 );
DEFINE_STRING( BGl_string2828z00zz__threadz00, BgL_bgl_string2828za700za7za7_3232za7, "thread-specific-set!", 20 );
DEFINE_STRING( BGl_string2747z00zz__threadz00, BgL_bgl_string2747za700za7za7_3233za7, "method1206", 10 );
DEFINE_STRING( BGl_string2585z00zz__threadz00, BgL_bgl_string2585za700za7za7_3234za7, "&lambda1360", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2498z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3235za7, BGl_z62zc3z04anonymousza31395ze3ze5zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2748z00zz__threadz00, BgL_bgl_string2748za700za7za7_3236za7, "&thread-terminate!", 18 );
DEFINE_STRING( BGl_string2586z00zz__threadz00, BgL_bgl_string2586za700za7za7_3237za7, "&lambda1369", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2499z00zz__threadz00, BgL_bgl_za762za7c3za704anonymo3238za7, BGl_z62zc3z04anonymousza31394ze3ze5zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2749z00zz__threadz00, BgL_bgl_string2749za700za7za7_3239za7, "thread-kill!:Wrong number of arguments", 38 );
DEFINE_STRING( BGl_string2668z00zz__threadz00, BgL_bgl_string2668za700za7za7_3240za7, "method1178", 10 );
DEFINE_STRING( BGl_string2587z00zz__threadz00, BgL_bgl_string2587za700za7za7_3241za7, "&lambda1368", 11 );
DEFINE_STRING( BGl_string2589z00zz__threadz00, BgL_bgl_string2589za700za7za7_3242za7, "tb-make-thread1177", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2threadzd2backendzd2envzd2zz__threadz00, BgL_bgl_za762getza7d2threadza73243za7, BGl_z62getzd2threadzd2backendz62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2820z00zz__threadz00, BgL_bgl_za762threadza7d2join3244z00, va_generic_entry, BGl_z62threadzd2joinz12zd2nothrea1238z70zz__threadz00, BUNSPEC, -2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2821z00zz__threadz00, BgL_bgl_za762threadza7d2term3245z00, BGl_z62threadzd2terminatez12zd2no1240z70zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2823z00zz__threadz00, BgL_bgl_za762threadza7d2kill3246z00, BGl_z62threadzd2killz12zd2nothrea1242z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2830z00zz__threadz00, BgL_bgl_string2830za700za7za7_3247za7, "thread-cleanup", 14 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2825z00zz__threadz00, BgL_bgl_za762threadza7d2spec3248z00, BGl_z62threadzd2specificzd2noth1244z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2832z00zz__threadz00, BgL_bgl_string2832za700za7za7_3249za7, "thread-cleanup-set!", 19 );
DEFINE_STRING( BGl_string2670z00zz__threadz00, BgL_bgl_string2670za700za7za7_3250za7, "tb", 2 );
DEFINE_EXPORT_BGL_GENERIC( BGl_z52userzd2threadzd2sleepz12zd2envz92zz__threadz00, BgL_bgl_za762za752userza7d2thr3251za7, BGl_z62z52userzd2threadzd2sleepz12z22zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_GENERIC( BGl_tbzd2condvarzd2initializa7ez12zd2envz67zz__threadz00, BgL_bgl_za762tbza7d2condvarza73252za7, BGl_z62tbzd2condvarzd2initializa7ez12zd7zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2827z00zz__threadz00, BgL_bgl_za762threadza7d2spec3253z00, BGl_z62threadzd2specificzd2setz121246z70zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2752z00zz__threadz00, BgL_bgl_string2752za700za7za7_3254za7, "method1208", 10 );
DEFINE_STRING( BGl_string2671z00zz__threadz00, BgL_bgl_string2671za700za7za7_3255za7, "&tb-make-thread", 15 );
DEFINE_STRING( BGl_string2672z00zz__threadz00, BgL_bgl_string2672za700za7za7_3256za7, "tb-current-thread:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2591z00zz__threadz00, BgL_bgl_string2591za700za7za7_3257za7, "tb-current-thread1179", 21 );
DEFINE_EXPORT_BGL_GENERIC( BGl_threadzd2killz12zd2envz12zz__threadz00, BgL_bgl_za762threadza7d2kill3258z00, BGl_z62threadzd2killz12za2zz__threadz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2835z00zz__threadz00, BgL_bgl_string2835za700za7za7_3259za7, "thread-name-set!", 16 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2829z00zz__threadz00, BgL_bgl_za762threadza7d2clea3260z00, BGl_z62threadzd2cleanupzd2nothr1248z62zz__threadz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2754z00zz__threadz00, BgL_bgl_string2754za700za7za7_3261za7, "n", 1 );
DEFINE_STRING( BGl_string2836z00zz__threadz00, BgL_bgl_string2836za700za7za7_3262za7, "thread-name-set!-not1254", 24 );
DEFINE_STRING( BGl_string2755z00zz__threadz00, BgL_bgl_string2755za700za7za7_3263za7, "&thread-kill!", 13 );
DEFINE_STRING( BGl_string2593z00zz__threadz00, BgL_bgl_string2593za700za7za7_3264za7, "tb-mutex-initialize!1185", 24 );
DEFINE_STRING( BGl_string2837z00zz__threadz00, BgL_bgl_string2837za700za7za7_3265za7, "&thread-name-set!-not1254", 25 );
DEFINE_STRING( BGl_string2756z00zz__threadz00, BgL_bgl_string2756za700za7za7_3266za7, "thread-specific:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string2675z00zz__threadz00, BgL_bgl_string2675za700za7za7_3267za7, "method1180", 10 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2588z00zz__threadz00, BgL_bgl_za762tbza7d2makeza7d2t3268za7, BGl_z62tbzd2makezd2thread1177z62zz__threadz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2838z00zz__threadz00, BgL_bgl_string2838za700za7za7_3269za7, "&thread-name-nothread1252", 25 );
DEFINE_STRING( BGl_string2676z00zz__threadz00, BgL_bgl_string2676za700za7za7_3270za7, "&tb-current-thread", 18 );
DEFINE_STRING( BGl_string2595z00zz__threadz00, BgL_bgl_string2595za700za7za7_3271za7, "tb-condvar-initializ1187", 24 );
DEFINE_STRING( BGl_string2839z00zz__threadz00, BgL_bgl_string2839za700za7za7_3272za7, "&thread-cleanup-set!-1250", 25 );
DEFINE_STRING( BGl_string2677z00zz__threadz00, BgL_bgl_string2677za700za7za7_3273za7, "tb-mutex-initialize!", 20 );
DEFINE_STRING( BGl_string2759z00zz__threadz00, BgL_bgl_string2759za700za7za7_3274za7, "method1210", 10 );
DEFINE_STRING( BGl_string2678z00zz__threadz00, BgL_bgl_string2678za700za7za7_3275za7, "tb-mutex-initialize!:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string2597z00zz__threadz00, BgL_bgl_string2597za700za7za7_3276za7, "thread-initialize!1195", 22 );
DEFINE_STRING( BGl_string2599z00zz__threadz00, BgL_bgl_string2599za700za7za7_3277za7, "thread-start!1197", 17 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol2800z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2721z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2641z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2480z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2805z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2643z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2482z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2726z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2645z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2647z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2729z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2486z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2649z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2487z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2457z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2732z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2651z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2700z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2653z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2492z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2737z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2656z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2705z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2658z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2496z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2708z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2709z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__threadz00) );
ADD_ROOT( (void *)(&BGl_za2threadzd2backendsza2zd2zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2740z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2660z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2662z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2712z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2746z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2667z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2553z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2717z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2669z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2751z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2753z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2720z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2674z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2804z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2758z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2725z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2680z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2763z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2731z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2765z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_nothreadzd2backendzd2zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2847z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2734z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2686z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2735z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2736z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2688z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2739z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_za2nothreadzd2backendza2zd2zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2770z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2692z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2856z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2775z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2742z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2694z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2745z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2697z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2666z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2781z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2750z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2786z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2673z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2757z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2679z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2791z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2793z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2876z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2762z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2878z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2846z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2798z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2685z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2769z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2691z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2855z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2774z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2780z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2785z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2500z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2421z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2424z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2790z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2875z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_list2797z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2510z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2512z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2516z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_nothreadz00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2521z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2440z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2443z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2526z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2445z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2447z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_za2nothreadzd2currentza2zd2zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2531z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2450z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2536z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2458z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_za2mutexzd2nilza2zd2zz__threadz00) );
ADD_ROOT( (void *)(&BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2701z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2703z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2541z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2460z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2706z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2545z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2628z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2466z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2468z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_threadz00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2710z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2713z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2632z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2715z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2472z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2635z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2554z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2718z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2637z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2556z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_symbol2639z00zz__threadz00) );
ADD_ROOT( (void *)(&BGl_threadzd2backendzd2zz__threadz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long BgL_checksumz00_8567, char * BgL_fromz00_8568)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__threadz00))
{ 
BGl_requirezd2initializa7ationz75zz__threadz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__threadz00(); 
BGl_cnstzd2initzd2zz__threadz00(); 
BGl_importedzd2moduleszd2initz00zz__threadz00(); 
BGl_objectzd2initzd2zz__threadz00(); 
BGl_genericzd2initzd2zz__threadz00(); 
BGl_methodzd2initzd2zz__threadz00(); 
return 
BGl_toplevelzd2initzd2zz__threadz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
BGl_symbol2421z00zz__threadz00 = 
bstring_to_symbol(BGl_string2422z00zz__threadz00); 
BGl_symbol2424z00zz__threadz00 = 
bstring_to_symbol(BGl_string2425z00zz__threadz00); 
BGl_symbol2440z00zz__threadz00 = 
bstring_to_symbol(BGl_string2441z00zz__threadz00); 
BGl_symbol2443z00zz__threadz00 = 
bstring_to_symbol(BGl_string2444z00zz__threadz00); 
BGl_symbol2445z00zz__threadz00 = 
bstring_to_symbol(BGl_string2446z00zz__threadz00); 
BGl_symbol2447z00zz__threadz00 = 
bstring_to_symbol(BGl_string2448z00zz__threadz00); 
BGl_symbol2450z00zz__threadz00 = 
bstring_to_symbol(BGl_string2451z00zz__threadz00); 
BGl_symbol2458z00zz__threadz00 = 
bstring_to_symbol(BGl_string2459z00zz__threadz00); 
BGl_symbol2460z00zz__threadz00 = 
bstring_to_symbol(BGl_string2461z00zz__threadz00); 
BGl_list2457z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2460z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2460z00zz__threadz00, BNIL))); 
BGl_symbol2466z00zz__threadz00 = 
bstring_to_symbol(BGl_string2467z00zz__threadz00); 
BGl_symbol2468z00zz__threadz00 = 
bstring_to_symbol(BGl_string2469z00zz__threadz00); 
BGl_symbol2472z00zz__threadz00 = 
bstring_to_symbol(BGl_string2473z00zz__threadz00); 
BGl_symbol2480z00zz__threadz00 = 
bstring_to_symbol(BGl_string2481z00zz__threadz00); 
BGl_symbol2482z00zz__threadz00 = 
bstring_to_symbol(BGl_string2420z00zz__threadz00); 
BGl_symbol2486z00zz__threadz00 = 
bstring_to_symbol(BGl_string2414z00zz__threadz00); 
BGl_symbol2487z00zz__threadz00 = 
bstring_to_symbol(BGl_string2488z00zz__threadz00); 
BGl_symbol2492z00zz__threadz00 = 
bstring_to_symbol(BGl_string2493z00zz__threadz00); 
BGl_symbol2496z00zz__threadz00 = 
bstring_to_symbol(BGl_string2497z00zz__threadz00); 
BGl_symbol2500z00zz__threadz00 = 
bstring_to_symbol(BGl_string2501z00zz__threadz00); 
BGl_symbol2510z00zz__threadz00 = 
bstring_to_symbol(BGl_string2511z00zz__threadz00); 
BGl_symbol2512z00zz__threadz00 = 
bstring_to_symbol(BGl_string2427z00zz__threadz00); 
BGl_symbol2516z00zz__threadz00 = 
bstring_to_symbol(BGl_string2517z00zz__threadz00); 
BGl_symbol2521z00zz__threadz00 = 
bstring_to_symbol(BGl_string2522z00zz__threadz00); 
BGl_symbol2526z00zz__threadz00 = 
bstring_to_symbol(BGl_string2527z00zz__threadz00); 
BGl_symbol2531z00zz__threadz00 = 
bstring_to_symbol(BGl_string2532z00zz__threadz00); 
BGl_symbol2536z00zz__threadz00 = 
bstring_to_symbol(BGl_string2537z00zz__threadz00); 
BGl_symbol2541z00zz__threadz00 = 
bstring_to_symbol(BGl_string2411z00zz__threadz00); 
BGl_symbol2545z00zz__threadz00 = 
bstring_to_symbol(BGl_string2546z00zz__threadz00); 
BGl_symbol2554z00zz__threadz00 = 
bstring_to_symbol(BGl_string2555z00zz__threadz00); 
BGl_symbol2556z00zz__threadz00 = 
bstring_to_symbol(BGl_string2557z00zz__threadz00); 
BGl_list2553z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2554z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2554z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2556z00zz__threadz00, BNIL)))); 
BGl_symbol2628z00zz__threadz00 = 
bstring_to_symbol(BGl_string2432z00zz__threadz00); 
BGl_symbol2632z00zz__threadz00 = 
bstring_to_symbol(BGl_string2619z00zz__threadz00); 
BGl_symbol2635z00zz__threadz00 = 
bstring_to_symbol(BGl_string2617z00zz__threadz00); 
BGl_symbol2637z00zz__threadz00 = 
bstring_to_symbol(BGl_string2615z00zz__threadz00); 
BGl_symbol2639z00zz__threadz00 = 
bstring_to_symbol(BGl_string2613z00zz__threadz00); 
BGl_symbol2641z00zz__threadz00 = 
bstring_to_symbol(BGl_string2611z00zz__threadz00); 
BGl_symbol2643z00zz__threadz00 = 
bstring_to_symbol(BGl_string2609z00zz__threadz00); 
BGl_symbol2645z00zz__threadz00 = 
bstring_to_symbol(BGl_string2607z00zz__threadz00); 
BGl_symbol2647z00zz__threadz00 = 
bstring_to_symbol(BGl_string2605z00zz__threadz00); 
BGl_symbol2649z00zz__threadz00 = 
bstring_to_symbol(BGl_string2603z00zz__threadz00); 
BGl_symbol2651z00zz__threadz00 = 
bstring_to_symbol(BGl_string2601z00zz__threadz00); 
BGl_symbol2653z00zz__threadz00 = 
bstring_to_symbol(BGl_string2599z00zz__threadz00); 
BGl_symbol2656z00zz__threadz00 = 
bstring_to_symbol(BGl_string2595z00zz__threadz00); 
BGl_symbol2658z00zz__threadz00 = 
bstring_to_symbol(BGl_string2593z00zz__threadz00); 
BGl_symbol2660z00zz__threadz00 = 
bstring_to_symbol(BGl_string2591z00zz__threadz00); 
BGl_symbol2662z00zz__threadz00 = 
bstring_to_symbol(BGl_string2589z00zz__threadz00); 
BGl_symbol2667z00zz__threadz00 = 
bstring_to_symbol(BGl_string2668z00zz__threadz00); 
BGl_symbol2669z00zz__threadz00 = 
bstring_to_symbol(BGl_string2670z00zz__threadz00); 
BGl_list2666z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2667z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2667z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2669z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2510z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2480z00zz__threadz00, BNIL)))))); 
BGl_symbol2674z00zz__threadz00 = 
bstring_to_symbol(BGl_string2675z00zz__threadz00); 
BGl_list2673z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2674z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2674z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2669z00zz__threadz00, BNIL)))); 
BGl_symbol2680z00zz__threadz00 = 
bstring_to_symbol(BGl_string2681z00zz__threadz00); 
BGl_list2679z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2680z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2669z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2443z00zz__threadz00, BNIL))))); 
BGl_symbol2686z00zz__threadz00 = 
bstring_to_symbol(BGl_string2687z00zz__threadz00); 
BGl_symbol2688z00zz__threadz00 = 
bstring_to_symbol(BGl_string2471z00zz__threadz00); 
BGl_list2685z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2686z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2686z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2669z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2688z00zz__threadz00, BNIL))))); 
BGl_symbol2692z00zz__threadz00 = 
bstring_to_symbol(BGl_string2693z00zz__threadz00); 
BGl_symbol2694z00zz__threadz00 = 
bstring_to_symbol(BGl_string2695z00zz__threadz00); 
BGl_list2691z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2692z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, BNIL)))); 
BGl_symbol2697z00zz__threadz00 = 
bstring_to_symbol(BGl_string2698z00zz__threadz00); 
BGl_symbol2701z00zz__threadz00 = 
bstring_to_symbol(BGl_string2702z00zz__threadz00); 
BGl_symbol2703z00zz__threadz00 = 
bstring_to_symbol(BGl_string2704z00zz__threadz00); 
BGl_symbol2706z00zz__threadz00 = 
bstring_to_symbol(BGl_string2707z00zz__threadz00); 
BGl_symbol2710z00zz__threadz00 = 
bstring_to_symbol(BGl_string2711z00zz__threadz00); 
BGl_symbol2713z00zz__threadz00 = 
bstring_to_symbol(BGl_string2714z00zz__threadz00); 
BGl_symbol2715z00zz__threadz00 = 
bstring_to_symbol(BGl_string2716z00zz__threadz00); 
BGl_symbol2718z00zz__threadz00 = 
bstring_to_symbol(BGl_string2719z00zz__threadz00); 
BGl_list2717z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2718z00zz__threadz00, 
MAKE_YOUNG_PAIR(BNIL, BNIL)); 
BGl_list2712z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2713z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2715z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2717z00zz__threadz00, BNIL))); 
BGl_list2709z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2710z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2712z00zz__threadz00, BNIL)); 
BGl_list2708z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_list2709z00zz__threadz00, BNIL); 
BGl_symbol2721z00zz__threadz00 = 
bstring_to_symbol(BGl_string2722z00zz__threadz00); 
BGl_list2720z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2721z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2710z00zz__threadz00, BNIL))); 
BGl_list2705z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2706z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2708z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2720z00zz__threadz00, BNIL))); 
BGl_list2700z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2701z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2703z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2705z00zz__threadz00, BNIL))); 
BGl_symbol2726z00zz__threadz00 = 
bstring_to_symbol(BGl_string2727z00zz__threadz00); 
BGl_list2725z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2726z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, BNIL)))); 
BGl_symbol2729z00zz__threadz00 = 
bstring_to_symbol(BGl_string2730z00zz__threadz00); 
BGl_symbol2732z00zz__threadz00 = 
bstring_to_symbol(BGl_string2733z00zz__threadz00); 
BGl_symbol2737z00zz__threadz00 = 
bstring_to_symbol(BGl_string2738z00zz__threadz00); 
BGl_symbol2740z00zz__threadz00 = 
bstring_to_symbol(BGl_string2741z00zz__threadz00); 
BGl_list2739z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2713z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2740z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2717z00zz__threadz00, BNIL))); 
BGl_list2736z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2737z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2739z00zz__threadz00, BNIL)); 
BGl_list2735z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_list2736z00zz__threadz00, BNIL); 
BGl_list2742z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2721z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2737z00zz__threadz00, BNIL))); 
BGl_list2734z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2706z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2735z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2742z00zz__threadz00, BNIL))); 
BGl_list2731z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2701z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2732z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_list2734z00zz__threadz00, BNIL))); 
BGl_symbol2746z00zz__threadz00 = 
bstring_to_symbol(BGl_string2747z00zz__threadz00); 
BGl_list2745z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2746z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2746z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, BNIL)))); 
BGl_symbol2751z00zz__threadz00 = 
bstring_to_symbol(BGl_string2752z00zz__threadz00); 
BGl_symbol2753z00zz__threadz00 = 
bstring_to_symbol(BGl_string2754z00zz__threadz00); 
BGl_list2750z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2751z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2751z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2753z00zz__threadz00, BNIL))))); 
BGl_symbol2758z00zz__threadz00 = 
bstring_to_symbol(BGl_string2759z00zz__threadz00); 
BGl_list2757z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2758z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2758z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, BNIL)))); 
BGl_symbol2763z00zz__threadz00 = 
bstring_to_symbol(BGl_string2764z00zz__threadz00); 
BGl_symbol2765z00zz__threadz00 = 
bstring_to_symbol(BGl_string2766z00zz__threadz00); 
BGl_list2762z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2763z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2763z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2765z00zz__threadz00, BNIL))))); 
BGl_symbol2770z00zz__threadz00 = 
bstring_to_symbol(BGl_string2771z00zz__threadz00); 
BGl_list2769z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2770z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2770z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, BNIL)))); 
BGl_symbol2775z00zz__threadz00 = 
bstring_to_symbol(BGl_string2776z00zz__threadz00); 
BGl_list2774z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2775z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2775z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2765z00zz__threadz00, BNIL))))); 
BGl_symbol2781z00zz__threadz00 = 
bstring_to_symbol(BGl_string2782z00zz__threadz00); 
BGl_list2780z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2781z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2781z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, BNIL)))); 
BGl_symbol2786z00zz__threadz00 = 
bstring_to_symbol(BGl_string2787z00zz__threadz00); 
BGl_list2785z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2786z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2786z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2694z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2765z00zz__threadz00, BNIL))))); 
BGl_symbol2791z00zz__threadz00 = 
bstring_to_symbol(BGl_string2792z00zz__threadz00); 
BGl_symbol2793z00zz__threadz00 = 
bstring_to_symbol(BGl_string2794z00zz__threadz00); 
BGl_list2790z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2791z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2791z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2793z00zz__threadz00, BNIL)))); 
BGl_symbol2798z00zz__threadz00 = 
bstring_to_symbol(BGl_string2799z00zz__threadz00); 
BGl_symbol2800z00zz__threadz00 = 
bstring_to_symbol(BGl_string2801z00zz__threadz00); 
BGl_list2797z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2798z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2798z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2793z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2800z00zz__threadz00, BNIL))))); 
BGl_symbol2805z00zz__threadz00 = 
bstring_to_symbol(BGl_string2806z00zz__threadz00); 
BGl_list2804z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2805z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2805z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2793z00zz__threadz00, BNIL)))); 
BGl_symbol2847z00zz__threadz00 = 
bstring_to_symbol(BGl_string2848z00zz__threadz00); 
BGl_list2846z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2847z00zz__threadz00, BNIL))); 
BGl_symbol2856z00zz__threadz00 = 
bstring_to_symbol(BGl_string2857z00zz__threadz00); 
BGl_list2855z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2856z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2856z00zz__threadz00, BNIL))); 
BGl_symbol2876z00zz__threadz00 = 
bstring_to_symbol(BGl_string2877z00zz__threadz00); 
BGl_symbol2878z00zz__threadz00 = 
bstring_to_symbol(BGl_string2879z00zz__threadz00); 
return ( 
BGl_list2875z00zz__threadz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2458z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2876z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2876z00zz__threadz00, 
MAKE_YOUNG_PAIR(BGl_symbol2878z00zz__threadz00, BNIL)))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
{ /* Llib/thread.scm 272 */
 BgL_nothreadzd2backendzd2_bglt BgL_new1058z00_1223;
{ /* Llib/thread.scm 273 */
 BgL_nothreadzd2backendzd2_bglt BgL_new1057z00_1224;
BgL_new1057z00_1224 = 
((BgL_nothreadzd2backendzd2_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_nothreadzd2backendzd2_bgl) ))); 
{ /* Llib/thread.scm 273 */
 long BgL_arg1316z00_1225;
BgL_arg1316z00_1225 = 
BGL_CLASS_NUM(BGl_nothreadzd2backendzd2zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1057z00_1224), BgL_arg1316z00_1225); } 
BgL_new1058z00_1223 = BgL_new1057z00_1224; } 
((((BgL_threadzd2backendzd2_bglt)COBJECT(
((BgL_threadzd2backendzd2_bglt)BgL_new1058z00_1223)))->BgL_namez00)=((obj_t)BGl_string2411z00zz__threadz00),BUNSPEC); 
BGl_za2nothreadzd2backendza2zd2zz__threadz00 = BgL_new1058z00_1223; } 
{ /* Llib/thread.scm 279 */
 obj_t BgL_list1317z00_1226;
BgL_list1317z00_1226 = 
MAKE_YOUNG_PAIR(
((obj_t)BGl_za2nothreadzd2backendza2zd2zz__threadz00), BNIL); 
BGl_za2threadzd2backendsza2zd2zz__threadz00 = BgL_list1317z00_1226; } 
BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BFALSE; 
BGl_za2mutexzd2nilza2zd2zz__threadz00 = 
bgl_make_nil_mutex(); 
return ( 
BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00 = 
bgl_make_nil_condvar(), BUNSPEC) ;} 

}



/* dynamic-env? */
BGL_EXPORTED_DEF bool_t BGl_dynamiczd2envzf3z21zz__threadz00(obj_t BgL_objz00_3)
{
{ /* Llib/thread.scm 259 */
return 
BGL_DYNAMIC_ENVP(BgL_objz00_3);} 

}



/* &dynamic-env? */
obj_t BGl_z62dynamiczd2envzf3z43zz__threadz00(obj_t BgL_envz00_3458, obj_t BgL_objz00_3459)
{
{ /* Llib/thread.scm 259 */
return 
BBOOL(
BGl_dynamiczd2envzf3z21zz__threadz00(BgL_objz00_3459));} 

}



/* current-dynamic-env */
BGL_EXPORTED_DEF obj_t BGl_currentzd2dynamiczd2envz00zz__threadz00(void)
{
{ /* Llib/thread.scm 265 */
return 
BGL_CURRENT_DYNAMIC_ENV();} 

}



/* &current-dynamic-env */
obj_t BGl_z62currentzd2dynamiczd2envz62zz__threadz00(obj_t BgL_envz00_3460)
{
{ /* Llib/thread.scm 265 */
return 
BGl_currentzd2dynamiczd2envz00zz__threadz00();} 

}



/* current-thread-backend */
BGL_EXPORTED_DEF obj_t BGl_currentzd2threadzd2backendz00zz__threadz00(void)
{
{ /* Llib/thread.scm 284 */
return 
BGL_THREAD_BACKEND();} 

}



/* &current-thread-backend */
obj_t BGl_z62currentzd2threadzd2backendz62zz__threadz00(obj_t BgL_envz00_3461)
{
{ /* Llib/thread.scm 284 */
return 
BGl_currentzd2threadzd2backendz00zz__threadz00();} 

}



/* current-thread-backend-set! */
BGL_EXPORTED_DEF obj_t BGl_currentzd2threadzd2backendzd2setz12zc0zz__threadz00(BgL_threadzd2backendzd2_bglt BgL_tbz00_4)
{
{ /* Llib/thread.scm 290 */
{ /* Llib/thread.scm 291 */
 obj_t BgL_tmpz00_8810;
BgL_tmpz00_8810 = 
((obj_t)BgL_tbz00_4); 
BGL_THREAD_BACKEND_SET(BgL_tmpz00_8810); } BUNSPEC; 
return 
((obj_t)BgL_tbz00_4);} 

}



/* &current-thread-backend-set! */
obj_t BGl_z62currentzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t BgL_envz00_3462, obj_t BgL_tbz00_3463)
{
{ /* Llib/thread.scm 290 */
{ /* Llib/thread.scm 291 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_8814;
if(
BGl_isazf3zf3zz__objectz00(BgL_tbz00_3463, BGl_threadzd2backendzd2zz__threadz00))
{ /* Llib/thread.scm 291 */
BgL_auxz00_8814 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3463)
; }  else 
{ 
 obj_t BgL_auxz00_8818;
BgL_auxz00_8818 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(11215L), BGl_string2413z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3463); 
FAILURE(BgL_auxz00_8818,BFALSE,BFALSE);} 
return 
BGl_currentzd2threadzd2backendzd2setz12zc0zz__threadz00(BgL_auxz00_8814);} } 

}



/* default-thread-backend */
BGL_EXPORTED_DEF obj_t BGl_defaultzd2threadzd2backendz00zz__threadz00(void)
{
{ /* Llib/thread.scm 297 */
{ /* Llib/thread.scm 298 */
 obj_t BgL_pairz00_2229;
{ /* Llib/thread.scm 298 */
 obj_t BgL_aux2077z00_3855;
BgL_aux2077z00_3855 = BGl_za2threadzd2backendsza2zd2zz__threadz00; 
if(
NULLP(BgL_aux2077z00_3855))
{ 
 obj_t BgL_auxz00_8825;
BgL_auxz00_8825 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(11494L), BGl_string2415z00zz__threadz00, BGl_string2416z00zz__threadz00, BgL_aux2077z00_3855); 
FAILURE(BgL_auxz00_8825,BFALSE,BFALSE);}  else 
{ /* Llib/thread.scm 298 */
BgL_pairz00_2229 = BgL_aux2077z00_3855; } } 
return 
CAR(BgL_pairz00_2229);} } 

}



/* &default-thread-backend */
obj_t BGl_z62defaultzd2threadzd2backendz62zz__threadz00(obj_t BgL_envz00_3464)
{
{ /* Llib/thread.scm 297 */
return 
BGl_defaultzd2threadzd2backendz00zz__threadz00();} 

}



/* default-thread-backend-set! */
BGL_EXPORTED_DEF obj_t BGl_defaultzd2threadzd2backendzd2setz12zc0zz__threadz00(BgL_threadzd2backendzd2_bglt BgL_tbz00_5)
{
{ /* Llib/thread.scm 303 */
BGl_za2threadzd2backendsza2zd2zz__threadz00 = 
bgl_remq_bang(
((obj_t)BgL_tbz00_5), BGl_za2threadzd2backendsza2zd2zz__threadz00); 
return ( 
BGl_za2threadzd2backendsza2zd2zz__threadz00 = 
MAKE_YOUNG_PAIR(
((obj_t)BgL_tbz00_5), BGl_za2threadzd2backendsza2zd2zz__threadz00), BUNSPEC) ;} 

}



/* &default-thread-backend-set! */
obj_t BGl_z62defaultzd2threadzd2backendzd2setz12za2zz__threadz00(obj_t BgL_envz00_3465, obj_t BgL_tbz00_3466)
{
{ /* Llib/thread.scm 303 */
{ /* Llib/thread.scm 304 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_8835;
if(
BGl_isazf3zf3zz__objectz00(BgL_tbz00_3466, BGl_threadzd2backendzd2zz__threadz00))
{ /* Llib/thread.scm 304 */
BgL_auxz00_8835 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3466)
; }  else 
{ 
 obj_t BgL_auxz00_8839;
BgL_auxz00_8839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(11833L), BGl_string2417z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3466); 
FAILURE(BgL_auxz00_8839,BFALSE,BFALSE);} 
return 
BGl_defaultzd2threadzd2backendzd2setz12zc0zz__threadz00(BgL_auxz00_8835);} } 

}



/* get-thread-backend */
BGL_EXPORTED_DEF obj_t BGl_getzd2threadzd2backendz00zz__threadz00(obj_t BgL_namez00_6)
{
{ /* Llib/thread.scm 310 */
{ 
 obj_t BgL_tbsz00_1236;
BgL_tbsz00_1236 = BGl_za2threadzd2backendsza2zd2zz__threadz00; 
BgL_zc3z04anonymousza31321ze3z87_1237:
if(
PAIRP(BgL_tbsz00_1236))
{ /* Llib/thread.scm 313 */
 BgL_threadzd2backendzd2_bglt BgL_i1059z00_1239;
{ /* Llib/thread.scm 313 */
 obj_t BgL_aux2081z00_3861;
BgL_aux2081z00_3861 = 
CAR(BgL_tbsz00_1236); 
{ /* Llib/thread.scm 313 */
 bool_t BgL_test3283z00_8847;
{ /* Llib/thread.scm 313 */
 obj_t BgL_classz00_4487;
BgL_classz00_4487 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_aux2081z00_3861))
{ /* Llib/thread.scm 313 */
 BgL_objectz00_bglt BgL_arg1932z00_4489; long BgL_arg1933z00_4490;
BgL_arg1932z00_4489 = 
(BgL_objectz00_bglt)(BgL_aux2081z00_3861); 
BgL_arg1933z00_4490 = 
BGL_CLASS_DEPTH(BgL_classz00_4487); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 313 */
 long BgL_idxz00_4498;
BgL_idxz00_4498 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4489); 
{ /* Llib/thread.scm 313 */
 obj_t BgL_arg1923z00_4499;
{ /* Llib/thread.scm 313 */
 long BgL_arg1924z00_4500;
BgL_arg1924z00_4500 = 
(BgL_idxz00_4498+BgL_arg1933z00_4490); 
BgL_arg1923z00_4499 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4500); } 
BgL_test3283z00_8847 = 
(BgL_arg1923z00_4499==BgL_classz00_4487); } }  else 
{ /* Llib/thread.scm 313 */
 bool_t BgL_res2044z00_4505;
{ /* Llib/thread.scm 313 */
 obj_t BgL_oclassz00_4509;
{ /* Llib/thread.scm 313 */
 obj_t BgL_arg1937z00_4511; long BgL_arg1938z00_4512;
BgL_arg1937z00_4511 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 313 */
 long BgL_arg1939z00_4513;
BgL_arg1939z00_4513 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4489); 
BgL_arg1938z00_4512 = 
(BgL_arg1939z00_4513-OBJECT_TYPE); } 
BgL_oclassz00_4509 = 
VECTOR_REF(BgL_arg1937z00_4511,BgL_arg1938z00_4512); } 
{ /* Llib/thread.scm 313 */
 bool_t BgL__ortest_1147z00_4519;
BgL__ortest_1147z00_4519 = 
(BgL_classz00_4487==BgL_oclassz00_4509); 
if(BgL__ortest_1147z00_4519)
{ /* Llib/thread.scm 313 */
BgL_res2044z00_4505 = BgL__ortest_1147z00_4519; }  else 
{ /* Llib/thread.scm 313 */
 long BgL_odepthz00_4520;
{ /* Llib/thread.scm 313 */
 obj_t BgL_arg1927z00_4521;
BgL_arg1927z00_4521 = 
(BgL_oclassz00_4509); 
BgL_odepthz00_4520 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4521); } 
if(
(BgL_arg1933z00_4490<BgL_odepthz00_4520))
{ /* Llib/thread.scm 313 */
 obj_t BgL_arg1925z00_4525;
{ /* Llib/thread.scm 313 */
 obj_t BgL_arg1926z00_4526;
BgL_arg1926z00_4526 = 
(BgL_oclassz00_4509); 
BgL_arg1925z00_4525 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4526, BgL_arg1933z00_4490); } 
BgL_res2044z00_4505 = 
(BgL_arg1925z00_4525==BgL_classz00_4487); }  else 
{ /* Llib/thread.scm 313 */
BgL_res2044z00_4505 = ((bool_t)0); } } } } 
BgL_test3283z00_8847 = BgL_res2044z00_4505; } }  else 
{ /* Llib/thread.scm 313 */
BgL_test3283z00_8847 = ((bool_t)0)
; } } 
if(BgL_test3283z00_8847)
{ /* Llib/thread.scm 313 */
BgL_i1059z00_1239 = 
((BgL_threadzd2backendzd2_bglt)BgL_aux2081z00_3861); }  else 
{ 
 obj_t BgL_auxz00_8872;
BgL_auxz00_8872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12243L), BGl_string2418z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_aux2081z00_3861); 
FAILURE(BgL_auxz00_8872,BFALSE,BFALSE);} } } 
{ /* Llib/thread.scm 314 */
 bool_t BgL_test3288z00_8876;
{ /* Llib/thread.scm 314 */
 obj_t BgL_arg1326z00_1243;
BgL_arg1326z00_1243 = 
(((BgL_threadzd2backendzd2_bglt)COBJECT(BgL_i1059z00_1239))->BgL_namez00); 
{ /* Llib/thread.scm 314 */
 long BgL_l1z00_2233;
BgL_l1z00_2233 = 
STRING_LENGTH(BgL_arg1326z00_1243); 
if(
(BgL_l1z00_2233==
STRING_LENGTH(BgL_namez00_6)))
{ /* Llib/thread.scm 314 */
 int BgL_arg1645z00_2236;
{ /* Llib/thread.scm 314 */
 char * BgL_auxz00_8884; char * BgL_tmpz00_8882;
BgL_auxz00_8884 = 
BSTRING_TO_STRING(BgL_namez00_6); 
BgL_tmpz00_8882 = 
BSTRING_TO_STRING(BgL_arg1326z00_1243); 
BgL_arg1645z00_2236 = 
memcmp(BgL_tmpz00_8882, BgL_auxz00_8884, BgL_l1z00_2233); } 
BgL_test3288z00_8876 = 
(
(long)(BgL_arg1645z00_2236)==0L); }  else 
{ /* Llib/thread.scm 314 */
BgL_test3288z00_8876 = ((bool_t)0)
; } } } 
if(BgL_test3288z00_8876)
{ /* Llib/thread.scm 314 */
return 
CAR(BgL_tbsz00_1236);}  else 
{ 
 obj_t BgL_tbsz00_8890;
BgL_tbsz00_8890 = 
CDR(BgL_tbsz00_1236); 
BgL_tbsz00_1236 = BgL_tbsz00_8890; 
goto BgL_zc3z04anonymousza31321ze3z87_1237;} } }  else 
{ /* Llib/thread.scm 312 */
return BFALSE;} } } 

}



/* &get-thread-backend */
obj_t BGl_z62getzd2threadzd2backendz62zz__threadz00(obj_t BgL_envz00_3467, obj_t BgL_namez00_3468)
{
{ /* Llib/thread.scm 310 */
{ /* Llib/thread.scm 312 */
 obj_t BgL_auxz00_8892;
if(
STRINGP(BgL_namez00_3468))
{ /* Llib/thread.scm 312 */
BgL_auxz00_8892 = BgL_namez00_3468
; }  else 
{ 
 obj_t BgL_auxz00_8895;
BgL_auxz00_8895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12194L), BGl_string2419z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_namez00_3468); 
FAILURE(BgL_auxz00_8895,BFALSE,BFALSE);} 
return 
BGl_getzd2threadzd2backendz00zz__threadz00(BgL_auxz00_8892);} } 

}



/* _make-thread */
obj_t BGl__makezd2threadzd2zz__threadz00(obj_t BgL_env1223z00_47, obj_t BgL_opt1222z00_46)
{
{ /* Llib/thread.scm 445 */
{ /* Llib/thread.scm 445 */
 obj_t BgL_g1224z00_4527;
BgL_g1224z00_4527 = 
VECTOR_REF(BgL_opt1222z00_46,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1222z00_46)) { case 1L : 

{ /* Llib/thread.scm 445 */
 obj_t BgL_namez00_4529;
BgL_namez00_4529 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2424z00zz__threadz00); 
{ /* Llib/thread.scm 445 */

{ /* Llib/thread.scm 445 */
 obj_t BgL_bodyz00_4530;
if(
PROCEDUREP(BgL_g1224z00_4527))
{ /* Llib/thread.scm 445 */
BgL_bodyz00_4530 = BgL_g1224z00_4527; }  else 
{ 
 obj_t BgL_auxz00_8904;
BgL_auxz00_8904 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19158L), BGl_string2426z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_g1224z00_4527); 
FAILURE(BgL_auxz00_8904,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1328z00_4531;
BgL_arg1328z00_4531 = 
BGl_defaultzd2threadzd2backendz00zz__threadz00(); 
{ 
 BgL_threadz00_bglt BgL_auxz00_8909;
{ /* Llib/thread.scm 446 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_8910;
if(
BGl_isazf3zf3zz__objectz00(BgL_arg1328z00_4531, BGl_threadzd2backendzd2zz__threadz00))
{ /* Llib/thread.scm 446 */
BgL_auxz00_8910 = 
((BgL_threadzd2backendzd2_bglt)BgL_arg1328z00_4531)
; }  else 
{ 
 obj_t BgL_auxz00_8914;
BgL_auxz00_8914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19269L), BGl_string2426z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_arg1328z00_4531); 
FAILURE(BgL_auxz00_8914,BFALSE,BFALSE);} 
BgL_auxz00_8909 = 
BGl_tbzd2makezd2threadz00zz__threadz00(BgL_auxz00_8910, BgL_bodyz00_4530, BgL_namez00_4529); } 
return 
((obj_t)BgL_auxz00_8909);} } } } } break;case 2L : 

{ /* Llib/thread.scm 445 */
 obj_t BgL_namez00_4532;
BgL_namez00_4532 = 
VECTOR_REF(BgL_opt1222z00_46,1L); 
{ /* Llib/thread.scm 445 */

{ /* Llib/thread.scm 445 */
 obj_t BgL_bodyz00_4533;
if(
PROCEDUREP(BgL_g1224z00_4527))
{ /* Llib/thread.scm 445 */
BgL_bodyz00_4533 = BgL_g1224z00_4527; }  else 
{ 
 obj_t BgL_auxz00_8923;
BgL_auxz00_8923 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19158L), BGl_string2426z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_g1224z00_4527); 
FAILURE(BgL_auxz00_8923,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1328z00_4534;
BgL_arg1328z00_4534 = 
BGl_defaultzd2threadzd2backendz00zz__threadz00(); 
{ 
 BgL_threadz00_bglt BgL_auxz00_8928;
{ /* Llib/thread.scm 446 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_8929;
if(
BGl_isazf3zf3zz__objectz00(BgL_arg1328z00_4534, BGl_threadzd2backendzd2zz__threadz00))
{ /* Llib/thread.scm 446 */
BgL_auxz00_8929 = 
((BgL_threadzd2backendzd2_bglt)BgL_arg1328z00_4534)
; }  else 
{ 
 obj_t BgL_auxz00_8933;
BgL_auxz00_8933 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19269L), BGl_string2426z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_arg1328z00_4534); 
FAILURE(BgL_auxz00_8933,BFALSE,BFALSE);} 
BgL_auxz00_8928 = 
BGl_tbzd2makezd2threadz00zz__threadz00(BgL_auxz00_8929, BgL_bodyz00_4533, BgL_namez00_4532); } 
return 
((obj_t)BgL_auxz00_8928);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2421z00zz__threadz00, BGl_string2423z00zz__threadz00, 
BINT(
VECTOR_LENGTH(BgL_opt1222z00_46)));} } } } 

}



/* make-thread */
BGL_EXPORTED_DEF BgL_threadz00_bglt BGl_makezd2threadzd2zz__threadz00(obj_t BgL_bodyz00_44, obj_t BgL_namez00_45)
{
{ /* Llib/thread.scm 445 */
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1328z00_4535;
BgL_arg1328z00_4535 = 
BGl_defaultzd2threadzd2backendz00zz__threadz00(); 
{ /* Llib/thread.scm 446 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_8945;
{ /* Llib/thread.scm 446 */
 bool_t BgL_test3295z00_8946;
{ /* Llib/thread.scm 446 */
 obj_t BgL_classz00_4536;
BgL_classz00_4536 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_arg1328z00_4535))
{ /* Llib/thread.scm 446 */
 BgL_objectz00_bglt BgL_arg1932z00_4538; long BgL_arg1933z00_4539;
BgL_arg1932z00_4538 = 
(BgL_objectz00_bglt)(BgL_arg1328z00_4535); 
BgL_arg1933z00_4539 = 
BGL_CLASS_DEPTH(BgL_classz00_4536); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 446 */
 long BgL_idxz00_4547;
BgL_idxz00_4547 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4538); 
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1923z00_4548;
{ /* Llib/thread.scm 446 */
 long BgL_arg1924z00_4549;
BgL_arg1924z00_4549 = 
(BgL_idxz00_4547+BgL_arg1933z00_4539); 
BgL_arg1923z00_4548 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4549); } 
BgL_test3295z00_8946 = 
(BgL_arg1923z00_4548==BgL_classz00_4536); } }  else 
{ /* Llib/thread.scm 446 */
 bool_t BgL_res2044z00_4554;
{ /* Llib/thread.scm 446 */
 obj_t BgL_oclassz00_4558;
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1937z00_4560; long BgL_arg1938z00_4561;
BgL_arg1937z00_4560 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 446 */
 long BgL_arg1939z00_4562;
BgL_arg1939z00_4562 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4538); 
BgL_arg1938z00_4561 = 
(BgL_arg1939z00_4562-OBJECT_TYPE); } 
BgL_oclassz00_4558 = 
VECTOR_REF(BgL_arg1937z00_4560,BgL_arg1938z00_4561); } 
{ /* Llib/thread.scm 446 */
 bool_t BgL__ortest_1147z00_4568;
BgL__ortest_1147z00_4568 = 
(BgL_classz00_4536==BgL_oclassz00_4558); 
if(BgL__ortest_1147z00_4568)
{ /* Llib/thread.scm 446 */
BgL_res2044z00_4554 = BgL__ortest_1147z00_4568; }  else 
{ /* Llib/thread.scm 446 */
 long BgL_odepthz00_4569;
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1927z00_4570;
BgL_arg1927z00_4570 = 
(BgL_oclassz00_4558); 
BgL_odepthz00_4569 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4570); } 
if(
(BgL_arg1933z00_4539<BgL_odepthz00_4569))
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1925z00_4574;
{ /* Llib/thread.scm 446 */
 obj_t BgL_arg1926z00_4575;
BgL_arg1926z00_4575 = 
(BgL_oclassz00_4558); 
BgL_arg1925z00_4574 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4575, BgL_arg1933z00_4539); } 
BgL_res2044z00_4554 = 
(BgL_arg1925z00_4574==BgL_classz00_4536); }  else 
{ /* Llib/thread.scm 446 */
BgL_res2044z00_4554 = ((bool_t)0); } } } } 
BgL_test3295z00_8946 = BgL_res2044z00_4554; } }  else 
{ /* Llib/thread.scm 446 */
BgL_test3295z00_8946 = ((bool_t)0)
; } } 
if(BgL_test3295z00_8946)
{ /* Llib/thread.scm 446 */
BgL_auxz00_8945 = 
((BgL_threadzd2backendzd2_bglt)BgL_arg1328z00_4535)
; }  else 
{ 
 obj_t BgL_auxz00_8971;
BgL_auxz00_8971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19269L), BGl_string2422z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_arg1328z00_4535); 
FAILURE(BgL_auxz00_8971,BFALSE,BFALSE);} } 
return 
BGl_tbzd2makezd2threadz00zz__threadz00(BgL_auxz00_8945, BgL_bodyz00_44, BgL_namez00_45);} } } 

}



/* %current-thread */
obj_t BGl_z52currentzd2threadz80zz__threadz00(void)
{
{ /* Llib/thread.scm 457 */
{ /* Llib/thread.scm 458 */
 obj_t BgL_tbz00_1252;
BgL_tbz00_1252 = 
BGL_THREAD_BACKEND(); 
{ /* Llib/thread.scm 459 */
 bool_t BgL_test3300z00_8977;
{ /* Llib/thread.scm 459 */
 obj_t BgL_classz00_2255;
BgL_classz00_2255 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_1252))
{ /* Llib/thread.scm 459 */
 BgL_objectz00_bglt BgL_arg1930z00_2257;
BgL_arg1930z00_2257 = 
(BgL_objectz00_bglt)(BgL_tbz00_1252); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 459 */
 long BgL_idxz00_2263;
BgL_idxz00_2263 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1930z00_2257); 
{ /* Llib/thread.scm 459 */
 obj_t BgL_arg1923z00_2264;
{ /* Llib/thread.scm 459 */
 long BgL_arg1924z00_2265;
BgL_arg1924z00_2265 = 
(BgL_idxz00_2263+1L); 
{ /* Llib/thread.scm 459 */
 obj_t BgL_vectorz00_2267;
{ /* Llib/thread.scm 459 */
 obj_t BgL_aux2095z00_3883;
BgL_aux2095z00_3883 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2095z00_3883))
{ /* Llib/thread.scm 459 */
BgL_vectorz00_2267 = BgL_aux2095z00_3883; }  else 
{ 
 obj_t BgL_auxz00_8987;
BgL_auxz00_8987 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19862L), BGl_string2428z00zz__threadz00, BGl_string2429z00zz__threadz00, BgL_aux2095z00_3883); 
FAILURE(BgL_auxz00_8987,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 459 */
 bool_t BgL_test3304z00_8991;
{ /* Llib/thread.scm 459 */
 long BgL_tmpz00_8992;
BgL_tmpz00_8992 = 
VECTOR_LENGTH(BgL_vectorz00_2267); 
BgL_test3304z00_8991 = 
BOUND_CHECK(BgL_arg1924z00_2265, BgL_tmpz00_8992); } 
if(BgL_test3304z00_8991)
{ /* Llib/thread.scm 459 */
BgL_arg1923z00_2264 = 
VECTOR_REF(BgL_vectorz00_2267,BgL_arg1924z00_2265); }  else 
{ 
 obj_t BgL_auxz00_8996;
BgL_auxz00_8996 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19862L), BGl_string2430z00zz__threadz00, BgL_vectorz00_2267, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2267)), 
(int)(BgL_arg1924z00_2265)); 
FAILURE(BgL_auxz00_8996,BFALSE,BFALSE);} } } } 
BgL_test3300z00_8977 = 
(BgL_arg1923z00_2264==BgL_classz00_2255); } }  else 
{ /* Llib/thread.scm 459 */
 bool_t BgL_res1944z00_2288;
{ /* Llib/thread.scm 459 */
 obj_t BgL_oclassz00_2271;
{ /* Llib/thread.scm 459 */
 obj_t BgL_arg1937z00_2279; long BgL_arg1938z00_2280;
BgL_arg1937z00_2279 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 459 */
 long BgL_arg1939z00_2281;
BgL_arg1939z00_2281 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1930z00_2257); 
BgL_arg1938z00_2280 = 
(BgL_arg1939z00_2281-OBJECT_TYPE); } 
BgL_oclassz00_2271 = 
VECTOR_REF(BgL_arg1937z00_2279,BgL_arg1938z00_2280); } 
{ /* Llib/thread.scm 459 */
 bool_t BgL__ortest_1147z00_2272;
BgL__ortest_1147z00_2272 = 
(BgL_classz00_2255==BgL_oclassz00_2271); 
if(BgL__ortest_1147z00_2272)
{ /* Llib/thread.scm 459 */
BgL_res1944z00_2288 = BgL__ortest_1147z00_2272; }  else 
{ /* Llib/thread.scm 459 */
 long BgL_odepthz00_2273;
{ /* Llib/thread.scm 459 */
 obj_t BgL_arg1927z00_2274;
BgL_arg1927z00_2274 = 
(BgL_oclassz00_2271); 
BgL_odepthz00_2273 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_2274); } 
if(
(1L<BgL_odepthz00_2273))
{ /* Llib/thread.scm 459 */
 obj_t BgL_arg1925z00_2276;
{ /* Llib/thread.scm 459 */
 obj_t BgL_arg1926z00_2277;
BgL_arg1926z00_2277 = 
(BgL_oclassz00_2271); 
BgL_arg1925z00_2276 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_2277, 1L); } 
BgL_res1944z00_2288 = 
(BgL_arg1925z00_2276==BgL_classz00_2255); }  else 
{ /* Llib/thread.scm 459 */
BgL_res1944z00_2288 = ((bool_t)0); } } } } 
BgL_test3300z00_8977 = BgL_res1944z00_2288; } }  else 
{ /* Llib/thread.scm 459 */
BgL_test3300z00_8977 = ((bool_t)0)
; } } 
if(BgL_test3300z00_8977)
{ /* Llib/thread.scm 460 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_9017;
{ /* Llib/thread.scm 460 */
 bool_t BgL_test3307z00_9018;
{ /* Llib/thread.scm 460 */
 obj_t BgL_classz00_4576;
BgL_classz00_4576 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_1252))
{ /* Llib/thread.scm 460 */
 BgL_objectz00_bglt BgL_arg1932z00_4578; long BgL_arg1933z00_4579;
BgL_arg1932z00_4578 = 
(BgL_objectz00_bglt)(BgL_tbz00_1252); 
BgL_arg1933z00_4579 = 
BGL_CLASS_DEPTH(BgL_classz00_4576); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 460 */
 long BgL_idxz00_4587;
BgL_idxz00_4587 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4578); 
{ /* Llib/thread.scm 460 */
 obj_t BgL_arg1923z00_4588;
{ /* Llib/thread.scm 460 */
 long BgL_arg1924z00_4589;
BgL_arg1924z00_4589 = 
(BgL_idxz00_4587+BgL_arg1933z00_4579); 
BgL_arg1923z00_4588 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4589); } 
BgL_test3307z00_9018 = 
(BgL_arg1923z00_4588==BgL_classz00_4576); } }  else 
{ /* Llib/thread.scm 460 */
 bool_t BgL_res2044z00_4594;
{ /* Llib/thread.scm 460 */
 obj_t BgL_oclassz00_4598;
{ /* Llib/thread.scm 460 */
 obj_t BgL_arg1937z00_4600; long BgL_arg1938z00_4601;
BgL_arg1937z00_4600 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 460 */
 long BgL_arg1939z00_4602;
BgL_arg1939z00_4602 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4578); 
BgL_arg1938z00_4601 = 
(BgL_arg1939z00_4602-OBJECT_TYPE); } 
BgL_oclassz00_4598 = 
VECTOR_REF(BgL_arg1937z00_4600,BgL_arg1938z00_4601); } 
{ /* Llib/thread.scm 460 */
 bool_t BgL__ortest_1147z00_4608;
BgL__ortest_1147z00_4608 = 
(BgL_classz00_4576==BgL_oclassz00_4598); 
if(BgL__ortest_1147z00_4608)
{ /* Llib/thread.scm 460 */
BgL_res2044z00_4594 = BgL__ortest_1147z00_4608; }  else 
{ /* Llib/thread.scm 460 */
 long BgL_odepthz00_4609;
{ /* Llib/thread.scm 460 */
 obj_t BgL_arg1927z00_4610;
BgL_arg1927z00_4610 = 
(BgL_oclassz00_4598); 
BgL_odepthz00_4609 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4610); } 
if(
(BgL_arg1933z00_4579<BgL_odepthz00_4609))
{ /* Llib/thread.scm 460 */
 obj_t BgL_arg1925z00_4614;
{ /* Llib/thread.scm 460 */
 obj_t BgL_arg1926z00_4615;
BgL_arg1926z00_4615 = 
(BgL_oclassz00_4598); 
BgL_arg1925z00_4614 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4615, BgL_arg1933z00_4579); } 
BgL_res2044z00_4594 = 
(BgL_arg1925z00_4614==BgL_classz00_4576); }  else 
{ /* Llib/thread.scm 460 */
BgL_res2044z00_4594 = ((bool_t)0); } } } } 
BgL_test3307z00_9018 = BgL_res2044z00_4594; } }  else 
{ /* Llib/thread.scm 460 */
BgL_test3307z00_9018 = ((bool_t)0)
; } } 
if(BgL_test3307z00_9018)
{ /* Llib/thread.scm 460 */
BgL_auxz00_9017 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_1252)
; }  else 
{ 
 obj_t BgL_auxz00_9043;
BgL_auxz00_9043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19909L), BGl_string2428z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_1252); 
FAILURE(BgL_auxz00_9043,BFALSE,BFALSE);} } 
return 
BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_auxz00_9017);}  else 
{ /* Llib/thread.scm 459 */
return BFALSE;} } } } 

}



/* current-thread */
BGL_EXPORTED_DEF obj_t BGl_currentzd2threadzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 465 */
{ /* Llib/thread.scm 466 */
 obj_t BgL_thz00_1254;
BgL_thz00_1254 = 
BGl_z52currentzd2threadz80zz__threadz00(); 
{ /* Llib/thread.scm 467 */
 bool_t BgL_test3312z00_9049;
{ /* Llib/thread.scm 467 */
 obj_t BgL_classz00_2289;
BgL_classz00_2289 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_1254))
{ /* Llib/thread.scm 467 */
 BgL_objectz00_bglt BgL_arg1930z00_2291;
BgL_arg1930z00_2291 = 
(BgL_objectz00_bglt)(BgL_thz00_1254); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 467 */
 long BgL_idxz00_2297;
BgL_idxz00_2297 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1930z00_2291); 
{ /* Llib/thread.scm 467 */
 obj_t BgL_arg1923z00_2298;
{ /* Llib/thread.scm 467 */
 long BgL_arg1924z00_2299;
BgL_arg1924z00_2299 = 
(BgL_idxz00_2297+1L); 
{ /* Llib/thread.scm 467 */
 obj_t BgL_vectorz00_2301;
{ /* Llib/thread.scm 467 */
 obj_t BgL_aux2099z00_3889;
BgL_aux2099z00_3889 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2099z00_3889))
{ /* Llib/thread.scm 467 */
BgL_vectorz00_2301 = BgL_aux2099z00_3889; }  else 
{ 
 obj_t BgL_auxz00_9059;
BgL_auxz00_9059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20209L), BGl_string2431z00zz__threadz00, BGl_string2429z00zz__threadz00, BgL_aux2099z00_3889); 
FAILURE(BgL_auxz00_9059,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 467 */
 bool_t BgL_test3316z00_9063;
{ /* Llib/thread.scm 467 */
 long BgL_tmpz00_9064;
BgL_tmpz00_9064 = 
VECTOR_LENGTH(BgL_vectorz00_2301); 
BgL_test3316z00_9063 = 
BOUND_CHECK(BgL_arg1924z00_2299, BgL_tmpz00_9064); } 
if(BgL_test3316z00_9063)
{ /* Llib/thread.scm 467 */
BgL_arg1923z00_2298 = 
VECTOR_REF(BgL_vectorz00_2301,BgL_arg1924z00_2299); }  else 
{ 
 obj_t BgL_auxz00_9068;
BgL_auxz00_9068 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20209L), BGl_string2430z00zz__threadz00, BgL_vectorz00_2301, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2301)), 
(int)(BgL_arg1924z00_2299)); 
FAILURE(BgL_auxz00_9068,BFALSE,BFALSE);} } } } 
BgL_test3312z00_9049 = 
(BgL_arg1923z00_2298==BgL_classz00_2289); } }  else 
{ /* Llib/thread.scm 467 */
 bool_t BgL_res1945z00_2322;
{ /* Llib/thread.scm 467 */
 obj_t BgL_oclassz00_2305;
{ /* Llib/thread.scm 467 */
 obj_t BgL_arg1937z00_2313; long BgL_arg1938z00_2314;
BgL_arg1937z00_2313 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 467 */
 long BgL_arg1939z00_2315;
BgL_arg1939z00_2315 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1930z00_2291); 
BgL_arg1938z00_2314 = 
(BgL_arg1939z00_2315-OBJECT_TYPE); } 
BgL_oclassz00_2305 = 
VECTOR_REF(BgL_arg1937z00_2313,BgL_arg1938z00_2314); } 
{ /* Llib/thread.scm 467 */
 bool_t BgL__ortest_1147z00_2306;
BgL__ortest_1147z00_2306 = 
(BgL_classz00_2289==BgL_oclassz00_2305); 
if(BgL__ortest_1147z00_2306)
{ /* Llib/thread.scm 467 */
BgL_res1945z00_2322 = BgL__ortest_1147z00_2306; }  else 
{ /* Llib/thread.scm 467 */
 long BgL_odepthz00_2307;
{ /* Llib/thread.scm 467 */
 obj_t BgL_arg1927z00_2308;
BgL_arg1927z00_2308 = 
(BgL_oclassz00_2305); 
BgL_odepthz00_2307 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_2308); } 
if(
(1L<BgL_odepthz00_2307))
{ /* Llib/thread.scm 467 */
 obj_t BgL_arg1925z00_2310;
{ /* Llib/thread.scm 467 */
 obj_t BgL_arg1926z00_2311;
BgL_arg1926z00_2311 = 
(BgL_oclassz00_2305); 
BgL_arg1925z00_2310 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_2311, 1L); } 
BgL_res1945z00_2322 = 
(BgL_arg1925z00_2310==BgL_classz00_2289); }  else 
{ /* Llib/thread.scm 467 */
BgL_res1945z00_2322 = ((bool_t)0); } } } } 
BgL_test3312z00_9049 = BgL_res1945z00_2322; } }  else 
{ /* Llib/thread.scm 467 */
BgL_test3312z00_9049 = ((bool_t)0)
; } } 
if(BgL_test3312z00_9049)
{ /* Llib/thread.scm 468 */
 BgL_threadz00_bglt BgL_auxz00_9089;
{ /* Llib/thread.scm 468 */
 bool_t BgL_test3319z00_9090;
{ /* Llib/thread.scm 468 */
 obj_t BgL_classz00_4616;
BgL_classz00_4616 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_1254))
{ /* Llib/thread.scm 468 */
 BgL_objectz00_bglt BgL_arg1932z00_4618; long BgL_arg1933z00_4619;
BgL_arg1932z00_4618 = 
(BgL_objectz00_bglt)(BgL_thz00_1254); 
BgL_arg1933z00_4619 = 
BGL_CLASS_DEPTH(BgL_classz00_4616); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 468 */
 long BgL_idxz00_4627;
BgL_idxz00_4627 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4618); 
{ /* Llib/thread.scm 468 */
 obj_t BgL_arg1923z00_4628;
{ /* Llib/thread.scm 468 */
 long BgL_arg1924z00_4629;
BgL_arg1924z00_4629 = 
(BgL_idxz00_4627+BgL_arg1933z00_4619); 
BgL_arg1923z00_4628 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4629); } 
BgL_test3319z00_9090 = 
(BgL_arg1923z00_4628==BgL_classz00_4616); } }  else 
{ /* Llib/thread.scm 468 */
 bool_t BgL_res2044z00_4634;
{ /* Llib/thread.scm 468 */
 obj_t BgL_oclassz00_4638;
{ /* Llib/thread.scm 468 */
 obj_t BgL_arg1937z00_4640; long BgL_arg1938z00_4641;
BgL_arg1937z00_4640 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 468 */
 long BgL_arg1939z00_4642;
BgL_arg1939z00_4642 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4618); 
BgL_arg1938z00_4641 = 
(BgL_arg1939z00_4642-OBJECT_TYPE); } 
BgL_oclassz00_4638 = 
VECTOR_REF(BgL_arg1937z00_4640,BgL_arg1938z00_4641); } 
{ /* Llib/thread.scm 468 */
 bool_t BgL__ortest_1147z00_4648;
BgL__ortest_1147z00_4648 = 
(BgL_classz00_4616==BgL_oclassz00_4638); 
if(BgL__ortest_1147z00_4648)
{ /* Llib/thread.scm 468 */
BgL_res2044z00_4634 = BgL__ortest_1147z00_4648; }  else 
{ /* Llib/thread.scm 468 */
 long BgL_odepthz00_4649;
{ /* Llib/thread.scm 468 */
 obj_t BgL_arg1927z00_4650;
BgL_arg1927z00_4650 = 
(BgL_oclassz00_4638); 
BgL_odepthz00_4649 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4650); } 
if(
(BgL_arg1933z00_4619<BgL_odepthz00_4649))
{ /* Llib/thread.scm 468 */
 obj_t BgL_arg1925z00_4654;
{ /* Llib/thread.scm 468 */
 obj_t BgL_arg1926z00_4655;
BgL_arg1926z00_4655 = 
(BgL_oclassz00_4638); 
BgL_arg1925z00_4654 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4655, BgL_arg1933z00_4619); } 
BgL_res2044z00_4634 = 
(BgL_arg1925z00_4654==BgL_classz00_4616); }  else 
{ /* Llib/thread.scm 468 */
BgL_res2044z00_4634 = ((bool_t)0); } } } } 
BgL_test3319z00_9090 = BgL_res2044z00_4634; } }  else 
{ /* Llib/thread.scm 468 */
BgL_test3319z00_9090 = ((bool_t)0)
; } } 
if(BgL_test3319z00_9090)
{ /* Llib/thread.scm 468 */
BgL_auxz00_9089 = 
((BgL_threadz00_bglt)BgL_thz00_1254)
; }  else 
{ 
 obj_t BgL_auxz00_9115;
BgL_auxz00_9115 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20250L), BGl_string2431z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_1254); 
FAILURE(BgL_auxz00_9115,BFALSE,BFALSE);} } 
return 
BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_auxz00_9089);}  else 
{ /* Llib/thread.scm 467 */
return BFALSE;} } } } 

}



/* &current-thread */
obj_t BGl_z62currentzd2threadzb0zz__threadz00(obj_t BgL_envz00_3469)
{
{ /* Llib/thread.scm 465 */
return 
BGl_currentzd2threadzd2zz__threadz00();} 

}



/* thread-sleep! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2sleepz12zc0zz__threadz00(obj_t BgL_objz00_51)
{
{ /* Llib/thread.scm 494 */
{ /* Llib/thread.scm 495 */
 obj_t BgL_arg1331z00_4656;
BgL_arg1331z00_4656 = 
BGl_currentzd2threadzd2zz__threadz00(); 
{ /* Llib/thread.scm 495 */
 BgL_threadz00_bglt BgL_auxz00_9122;
{ /* Llib/thread.scm 495 */
 bool_t BgL_test3324z00_9123;
{ /* Llib/thread.scm 495 */
 obj_t BgL_classz00_4657;
BgL_classz00_4657 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_arg1331z00_4656))
{ /* Llib/thread.scm 495 */
 BgL_objectz00_bglt BgL_arg1932z00_4659; long BgL_arg1933z00_4660;
BgL_arg1932z00_4659 = 
(BgL_objectz00_bglt)(BgL_arg1331z00_4656); 
BgL_arg1933z00_4660 = 
BGL_CLASS_DEPTH(BgL_classz00_4657); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 495 */
 long BgL_idxz00_4668;
BgL_idxz00_4668 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4659); 
{ /* Llib/thread.scm 495 */
 obj_t BgL_arg1923z00_4669;
{ /* Llib/thread.scm 495 */
 long BgL_arg1924z00_4670;
BgL_arg1924z00_4670 = 
(BgL_idxz00_4668+BgL_arg1933z00_4660); 
BgL_arg1923z00_4669 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4670); } 
BgL_test3324z00_9123 = 
(BgL_arg1923z00_4669==BgL_classz00_4657); } }  else 
{ /* Llib/thread.scm 495 */
 bool_t BgL_res2044z00_4675;
{ /* Llib/thread.scm 495 */
 obj_t BgL_oclassz00_4679;
{ /* Llib/thread.scm 495 */
 obj_t BgL_arg1937z00_4681; long BgL_arg1938z00_4682;
BgL_arg1937z00_4681 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 495 */
 long BgL_arg1939z00_4683;
BgL_arg1939z00_4683 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4659); 
BgL_arg1938z00_4682 = 
(BgL_arg1939z00_4683-OBJECT_TYPE); } 
BgL_oclassz00_4679 = 
VECTOR_REF(BgL_arg1937z00_4681,BgL_arg1938z00_4682); } 
{ /* Llib/thread.scm 495 */
 bool_t BgL__ortest_1147z00_4689;
BgL__ortest_1147z00_4689 = 
(BgL_classz00_4657==BgL_oclassz00_4679); 
if(BgL__ortest_1147z00_4689)
{ /* Llib/thread.scm 495 */
BgL_res2044z00_4675 = BgL__ortest_1147z00_4689; }  else 
{ /* Llib/thread.scm 495 */
 long BgL_odepthz00_4690;
{ /* Llib/thread.scm 495 */
 obj_t BgL_arg1927z00_4691;
BgL_arg1927z00_4691 = 
(BgL_oclassz00_4679); 
BgL_odepthz00_4690 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4691); } 
if(
(BgL_arg1933z00_4660<BgL_odepthz00_4690))
{ /* Llib/thread.scm 495 */
 obj_t BgL_arg1925z00_4695;
{ /* Llib/thread.scm 495 */
 obj_t BgL_arg1926z00_4696;
BgL_arg1926z00_4696 = 
(BgL_oclassz00_4679); 
BgL_arg1925z00_4695 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4696, BgL_arg1933z00_4660); } 
BgL_res2044z00_4675 = 
(BgL_arg1925z00_4695==BgL_classz00_4657); }  else 
{ /* Llib/thread.scm 495 */
BgL_res2044z00_4675 = ((bool_t)0); } } } } 
BgL_test3324z00_9123 = BgL_res2044z00_4675; } }  else 
{ /* Llib/thread.scm 495 */
BgL_test3324z00_9123 = ((bool_t)0)
; } } 
if(BgL_test3324z00_9123)
{ /* Llib/thread.scm 495 */
BgL_auxz00_9122 = 
((BgL_threadz00_bglt)BgL_arg1331z00_4656)
; }  else 
{ 
 obj_t BgL_auxz00_9148;
BgL_auxz00_9148 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(21345L), BGl_string2432z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_arg1331z00_4656); 
FAILURE(BgL_auxz00_9148,BFALSE,BFALSE);} } 
return 
BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_auxz00_9122, BgL_objz00_51);} } } 

}



/* &thread-sleep! */
obj_t BGl_z62threadzd2sleepz12za2zz__threadz00(obj_t BgL_envz00_3470, obj_t BgL_objz00_3471)
{
{ /* Llib/thread.scm 494 */
return 
BGl_threadzd2sleepz12zc0zz__threadz00(BgL_objz00_3471);} 

}



/* thread-yield! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2yieldz12zc0zz__threadz00(void)
{
{ /* Llib/thread.scm 506 */
{ /* Llib/thread.scm 507 */
 obj_t BgL_arg1332z00_4697;
BgL_arg1332z00_4697 = 
BGl_currentzd2threadzd2zz__threadz00(); 
{ /* Llib/thread.scm 507 */
 BgL_threadz00_bglt BgL_auxz00_9155;
{ /* Llib/thread.scm 507 */
 bool_t BgL_test3329z00_9156;
{ /* Llib/thread.scm 507 */
 obj_t BgL_classz00_4698;
BgL_classz00_4698 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_arg1332z00_4697))
{ /* Llib/thread.scm 507 */
 BgL_objectz00_bglt BgL_arg1932z00_4700; long BgL_arg1933z00_4701;
BgL_arg1932z00_4700 = 
(BgL_objectz00_bglt)(BgL_arg1332z00_4697); 
BgL_arg1933z00_4701 = 
BGL_CLASS_DEPTH(BgL_classz00_4698); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 507 */
 long BgL_idxz00_4709;
BgL_idxz00_4709 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4700); 
{ /* Llib/thread.scm 507 */
 obj_t BgL_arg1923z00_4710;
{ /* Llib/thread.scm 507 */
 long BgL_arg1924z00_4711;
BgL_arg1924z00_4711 = 
(BgL_idxz00_4709+BgL_arg1933z00_4701); 
BgL_arg1923z00_4710 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4711); } 
BgL_test3329z00_9156 = 
(BgL_arg1923z00_4710==BgL_classz00_4698); } }  else 
{ /* Llib/thread.scm 507 */
 bool_t BgL_res2044z00_4716;
{ /* Llib/thread.scm 507 */
 obj_t BgL_oclassz00_4720;
{ /* Llib/thread.scm 507 */
 obj_t BgL_arg1937z00_4722; long BgL_arg1938z00_4723;
BgL_arg1937z00_4722 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 507 */
 long BgL_arg1939z00_4724;
BgL_arg1939z00_4724 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4700); 
BgL_arg1938z00_4723 = 
(BgL_arg1939z00_4724-OBJECT_TYPE); } 
BgL_oclassz00_4720 = 
VECTOR_REF(BgL_arg1937z00_4722,BgL_arg1938z00_4723); } 
{ /* Llib/thread.scm 507 */
 bool_t BgL__ortest_1147z00_4730;
BgL__ortest_1147z00_4730 = 
(BgL_classz00_4698==BgL_oclassz00_4720); 
if(BgL__ortest_1147z00_4730)
{ /* Llib/thread.scm 507 */
BgL_res2044z00_4716 = BgL__ortest_1147z00_4730; }  else 
{ /* Llib/thread.scm 507 */
 long BgL_odepthz00_4731;
{ /* Llib/thread.scm 507 */
 obj_t BgL_arg1927z00_4732;
BgL_arg1927z00_4732 = 
(BgL_oclassz00_4720); 
BgL_odepthz00_4731 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4732); } 
if(
(BgL_arg1933z00_4701<BgL_odepthz00_4731))
{ /* Llib/thread.scm 507 */
 obj_t BgL_arg1925z00_4736;
{ /* Llib/thread.scm 507 */
 obj_t BgL_arg1926z00_4737;
BgL_arg1926z00_4737 = 
(BgL_oclassz00_4720); 
BgL_arg1925z00_4736 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4737, BgL_arg1933z00_4701); } 
BgL_res2044z00_4716 = 
(BgL_arg1925z00_4736==BgL_classz00_4698); }  else 
{ /* Llib/thread.scm 507 */
BgL_res2044z00_4716 = ((bool_t)0); } } } } 
BgL_test3329z00_9156 = BgL_res2044z00_4716; } }  else 
{ /* Llib/thread.scm 507 */
BgL_test3329z00_9156 = ((bool_t)0)
; } } 
if(BgL_test3329z00_9156)
{ /* Llib/thread.scm 507 */
BgL_auxz00_9155 = 
((BgL_threadz00_bglt)BgL_arg1332z00_4697)
; }  else 
{ 
 obj_t BgL_auxz00_9181;
BgL_auxz00_9181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(21936L), BGl_string2433z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_arg1332z00_4697); 
FAILURE(BgL_auxz00_9181,BFALSE,BFALSE);} } 
return 
BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_auxz00_9155);} } } 

}



/* &thread-yield! */
obj_t BGl_z62threadzd2yieldz12za2zz__threadz00(obj_t BgL_envz00_3472)
{
{ /* Llib/thread.scm 506 */
return 
BGl_threadzd2yieldz12zc0zz__threadz00();} 

}



/* thread-parameter */
BGL_EXPORTED_DEF obj_t BGl_threadzd2parameterzd2zz__threadz00(obj_t BgL_idz00_53)
{
{ /* Llib/thread.scm 512 */
{ /* Llib/thread.scm 513 */
 obj_t BgL_cz00_2325;
{ /* Llib/thread.scm 513 */
 obj_t BgL_arg1334z00_2326;
BgL_arg1334z00_2326 = 
BGL_PARAMETERS(); 
{ /* Llib/thread.scm 513 */
 obj_t BgL_auxz00_9188;
{ /* Llib/thread.scm 513 */
 bool_t BgL_test3334z00_9189;
if(
PAIRP(BgL_arg1334z00_2326))
{ /* Llib/thread.scm 513 */
BgL_test3334z00_9189 = ((bool_t)1)
; }  else 
{ /* Llib/thread.scm 513 */
BgL_test3334z00_9189 = 
NULLP(BgL_arg1334z00_2326)
; } 
if(BgL_test3334z00_9189)
{ /* Llib/thread.scm 513 */
BgL_auxz00_9188 = BgL_arg1334z00_2326
; }  else 
{ 
 obj_t BgL_auxz00_9193;
BgL_auxz00_9193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(22236L), BGl_string2434z00zz__threadz00, BGl_string2435z00zz__threadz00, BgL_arg1334z00_2326); 
FAILURE(BgL_auxz00_9193,BFALSE,BFALSE);} } 
BgL_cz00_2325 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_53, BgL_auxz00_9188); } } 
if(
PAIRP(BgL_cz00_2325))
{ /* Llib/thread.scm 514 */
return 
CDR(BgL_cz00_2325);}  else 
{ /* Llib/thread.scm 514 */
return BFALSE;} } } 

}



/* &thread-parameter */
obj_t BGl_z62threadzd2parameterzb0zz__threadz00(obj_t BgL_envz00_3473, obj_t BgL_idz00_3474)
{
{ /* Llib/thread.scm 512 */
{ /* Llib/thread.scm 513 */
 obj_t BgL_auxz00_9201;
if(
SYMBOLP(BgL_idz00_3474))
{ /* Llib/thread.scm 513 */
BgL_auxz00_9201 = BgL_idz00_3474
; }  else 
{ 
 obj_t BgL_auxz00_9204;
BgL_auxz00_9204 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(22199L), BGl_string2436z00zz__threadz00, BGl_string2437z00zz__threadz00, BgL_idz00_3474); 
FAILURE(BgL_auxz00_9204,BFALSE,BFALSE);} 
return 
BGl_threadzd2parameterzd2zz__threadz00(BgL_auxz00_9201);} } 

}



/* thread-parameter-set! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2parameterzd2setz12z12zz__threadz00(obj_t BgL_idz00_54, obj_t BgL_valz00_55)
{
{ /* Llib/thread.scm 521 */
{ /* Llib/thread.scm 522 */
 obj_t BgL_cz00_1261;
{ /* Llib/thread.scm 522 */
 obj_t BgL_arg1339z00_1266;
BgL_arg1339z00_1266 = 
BGL_PARAMETERS(); 
{ /* Llib/thread.scm 522 */
 obj_t BgL_auxz00_9210;
{ /* Llib/thread.scm 522 */
 bool_t BgL_test3338z00_9211;
if(
PAIRP(BgL_arg1339z00_1266))
{ /* Llib/thread.scm 522 */
BgL_test3338z00_9211 = ((bool_t)1)
; }  else 
{ /* Llib/thread.scm 522 */
BgL_test3338z00_9211 = 
NULLP(BgL_arg1339z00_1266)
; } 
if(BgL_test3338z00_9211)
{ /* Llib/thread.scm 522 */
BgL_auxz00_9210 = BgL_arg1339z00_1266
; }  else 
{ 
 obj_t BgL_auxz00_9215;
BgL_auxz00_9215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(22583L), BGl_string2438z00zz__threadz00, BGl_string2435z00zz__threadz00, BgL_arg1339z00_1266); 
FAILURE(BgL_auxz00_9215,BFALSE,BFALSE);} } 
BgL_cz00_1261 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_54, BgL_auxz00_9210); } } 
if(
PAIRP(BgL_cz00_1261))
{ /* Llib/thread.scm 523 */
return 
SET_CDR(BgL_cz00_1261, BgL_valz00_55);}  else 
{ /* Llib/thread.scm 523 */
{ /* Llib/thread.scm 527 */
 obj_t BgL_arg1336z00_1263;
{ /* Llib/thread.scm 527 */
 obj_t BgL_arg1337z00_1264; obj_t BgL_arg1338z00_1265;
BgL_arg1337z00_1264 = 
MAKE_YOUNG_PAIR(BgL_idz00_54, BgL_valz00_55); 
BgL_arg1338z00_1265 = 
BGL_PARAMETERS(); 
BgL_arg1336z00_1263 = 
MAKE_YOUNG_PAIR(BgL_arg1337z00_1264, BgL_arg1338z00_1265); } 
BGL_PARAMETERS_SET(BgL_arg1336z00_1263); } 
return BgL_valz00_55;} } } 

}



/* &thread-parameter-set! */
obj_t BGl_z62threadzd2parameterzd2setz12z70zz__threadz00(obj_t BgL_envz00_3475, obj_t BgL_idz00_3476, obj_t BgL_valz00_3477)
{
{ /* Llib/thread.scm 521 */
{ /* Llib/thread.scm 522 */
 obj_t BgL_auxz00_9227;
if(
SYMBOLP(BgL_idz00_3476))
{ /* Llib/thread.scm 522 */
BgL_auxz00_9227 = BgL_idz00_3476
; }  else 
{ 
 obj_t BgL_auxz00_9230;
BgL_auxz00_9230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(22546L), BGl_string2439z00zz__threadz00, BGl_string2437z00zz__threadz00, BgL_idz00_3476); 
FAILURE(BgL_auxz00_9230,BFALSE,BFALSE);} 
return 
BGl_threadzd2parameterzd2setz12z12zz__threadz00(BgL_auxz00_9227, BgL_valz00_3477);} } 

}



/* mutex? */
BGL_EXPORTED_DEF bool_t BGl_mutexzf3zf3zz__threadz00(obj_t BgL_objz00_73)
{
{ /* Llib/thread.scm 614 */
return 
BGL_MUTEXP(BgL_objz00_73);} 

}



/* &mutex? */
obj_t BGl_z62mutexzf3z91zz__threadz00(obj_t BgL_envz00_3478, obj_t BgL_objz00_3479)
{
{ /* Llib/thread.scm 614 */
return 
BBOOL(
BGl_mutexzf3zf3zz__threadz00(BgL_objz00_3479));} 

}



/* _make-mutex */
obj_t BGl__makezd2mutexzd2zz__threadz00(obj_t BgL_env1256z00_76, obj_t BgL_opt1255z00_75)
{
{ /* Llib/thread.scm 620 */
{ /* Llib/thread.scm 620 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1255z00_75)) { case 0L : 

{ /* Llib/thread.scm 620 */
 obj_t BgL_namez00_4741;
BgL_namez00_4741 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2443z00zz__threadz00); 
{ /* Llib/thread.scm 620 */

return 
bgl_make_mutex(BgL_namez00_4741);} } break;case 1L : 

{ /* Llib/thread.scm 620 */
 obj_t BgL_namez00_4742;
BgL_namez00_4742 = 
VECTOR_REF(BgL_opt1255z00_75,0L); 
{ /* Llib/thread.scm 620 */

return 
bgl_make_mutex(BgL_namez00_4742);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2440z00zz__threadz00, BGl_string2442z00zz__threadz00, 
BINT(
VECTOR_LENGTH(BgL_opt1255z00_75)));} } } } 

}



/* make-mutex */
BGL_EXPORTED_DEF obj_t BGl_makezd2mutexzd2zz__threadz00(obj_t BgL_namez00_74)
{
{ /* Llib/thread.scm 620 */
return 
bgl_make_mutex(BgL_namez00_74);} 

}



/* _make-spinlock */
obj_t BGl__makezd2spinlockzd2zz__threadz00(obj_t BgL_env1260z00_79, obj_t BgL_opt1259z00_78)
{
{ /* Llib/thread.scm 626 */
{ /* Llib/thread.scm 626 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1259z00_78)) { case 0L : 

{ /* Llib/thread.scm 626 */
 obj_t BgL_namez00_4744;
BgL_namez00_4744 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2447z00zz__threadz00); 
{ /* Llib/thread.scm 626 */

return 
bgl_make_spinlock(BgL_namez00_4744);} } break;case 1L : 

{ /* Llib/thread.scm 626 */
 obj_t BgL_namez00_4745;
BgL_namez00_4745 = 
VECTOR_REF(BgL_opt1259z00_78,0L); 
{ /* Llib/thread.scm 626 */

return 
bgl_make_spinlock(BgL_namez00_4745);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2445z00zz__threadz00, BGl_string2442z00zz__threadz00, 
BINT(
VECTOR_LENGTH(BgL_opt1259z00_78)));} } } } 

}



/* make-spinlock */
BGL_EXPORTED_DEF obj_t BGl_makezd2spinlockzd2zz__threadz00(obj_t BgL_namez00_77)
{
{ /* Llib/thread.scm 626 */
return 
bgl_make_spinlock(BgL_namez00_77);} 

}



/* mutex-nil */
BGL_EXPORTED_DEF obj_t BGl_mutexzd2nilzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 633 */
return BGl_za2mutexzd2nilza2zd2zz__threadz00;} 

}



/* &mutex-nil */
obj_t BGl_z62mutexzd2nilzb0zz__threadz00(obj_t BgL_envz00_3480)
{
{ /* Llib/thread.scm 633 */
return 
BGl_mutexzd2nilzd2zz__threadz00();} 

}



/* mutex-name */
BGL_EXPORTED_DEF obj_t BGl_mutexzd2namezd2zz__threadz00(obj_t BgL_objz00_80)
{
{ /* Llib/thread.scm 638 */
return 
BGL_MUTEX_NAME(BgL_objz00_80);} 

}



/* &mutex-name */
obj_t BGl_z62mutexzd2namezb0zz__threadz00(obj_t BgL_envz00_3481, obj_t BgL_objz00_3482)
{
{ /* Llib/thread.scm 638 */
{ /* Llib/thread.scm 639 */
 obj_t BgL_auxz00_9260;
if(
BGL_MUTEXP(BgL_objz00_3482))
{ /* Llib/thread.scm 639 */
BgL_auxz00_9260 = BgL_objz00_3482
; }  else 
{ 
 obj_t BgL_auxz00_9263;
BgL_auxz00_9263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(28093L), BGl_string2449z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_objz00_3482); 
FAILURE(BgL_auxz00_9263,BFALSE,BFALSE);} 
return 
BGl_mutexzd2namezd2zz__threadz00(BgL_auxz00_9260);} } 

}



/* _mutex-lock! */
obj_t BGl__mutexzd2lockz12zc0zz__threadz00(obj_t BgL_env1264z00_84, obj_t BgL_opt1263z00_83)
{
{ /* Llib/thread.scm 644 */
{ /* Llib/thread.scm 644 */
 obj_t BgL_g1265z00_4746;
BgL_g1265z00_4746 = 
VECTOR_REF(BgL_opt1263z00_83,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1263z00_83)) { case 1L : 

{ /* Llib/thread.scm 644 */

{ /* Llib/thread.scm 644 */
 obj_t BgL_mz00_4748;
if(
BGL_MUTEXP(BgL_g1265z00_4746))
{ /* Llib/thread.scm 644 */
BgL_mz00_4748 = BgL_g1265z00_4746; }  else 
{ 
 obj_t BgL_auxz00_9271;
BgL_auxz00_9271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(28335L), BGl_string2452z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_g1265z00_4746); 
FAILURE(BgL_auxz00_9271,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 646 */
 int BgL_arg1344z00_4749;
BgL_arg1344z00_4749 = 
BGL_MUTEX_LOCK(BgL_mz00_4748); 
return 
BBOOL(
(
(long)(BgL_arg1344z00_4749)==0L));} } } break;case 2L : 

{ /* Llib/thread.scm 644 */
 obj_t BgL_timeoutz00_4750;
BgL_timeoutz00_4750 = 
VECTOR_REF(BgL_opt1263z00_83,1L); 
{ /* Llib/thread.scm 644 */

{ /* Llib/thread.scm 644 */
 obj_t BgL_mz00_4751; long BgL_timeoutz00_4752;
if(
BGL_MUTEXP(BgL_g1265z00_4746))
{ /* Llib/thread.scm 644 */
BgL_mz00_4751 = BgL_g1265z00_4746; }  else 
{ 
 obj_t BgL_auxz00_9282;
BgL_auxz00_9282 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(28335L), BGl_string2452z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_g1265z00_4746); 
FAILURE(BgL_auxz00_9282,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 644 */
 obj_t BgL_tmpz00_9286;
if(
INTEGERP(BgL_timeoutz00_4750))
{ /* Llib/thread.scm 644 */
BgL_tmpz00_9286 = BgL_timeoutz00_4750
; }  else 
{ 
 obj_t BgL_auxz00_9289;
BgL_auxz00_9289 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(28335L), BGl_string2452z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_timeoutz00_4750); 
FAILURE(BgL_auxz00_9289,BFALSE,BFALSE);} 
BgL_timeoutz00_4752 = 
(long)CINT(BgL_tmpz00_9286); } 
if(
(BgL_timeoutz00_4752==0L))
{ /* Llib/thread.scm 646 */
 int BgL_arg1344z00_4753;
BgL_arg1344z00_4753 = 
BGL_MUTEX_LOCK(BgL_mz00_4751); 
return 
BBOOL(
(
(long)(BgL_arg1344z00_4753)==0L));}  else 
{ /* Llib/thread.scm 647 */
 int BgL_arg1346z00_4754;
BgL_arg1346z00_4754 = 
BGL_MUTEX_TIMED_LOCK(BgL_mz00_4751, BgL_timeoutz00_4752); 
return 
BBOOL(
(
(long)(BgL_arg1346z00_4754)==0L));} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2450z00zz__threadz00, BGl_string2423z00zz__threadz00, 
BINT(
VECTOR_LENGTH(BgL_opt1263z00_83)));} } } } 

}



/* mutex-lock! */
BGL_EXPORTED_DEF obj_t BGl_mutexzd2lockz12zc0zz__threadz00(obj_t BgL_mz00_81, long BgL_timeoutz00_82)
{
{ /* Llib/thread.scm 644 */
if(
(BgL_timeoutz00_82==0L))
{ /* Llib/thread.scm 646 */
 int BgL_arg1344z00_4755;
BgL_arg1344z00_4755 = 
BGL_MUTEX_LOCK(BgL_mz00_81); 
return 
BBOOL(
(
(long)(BgL_arg1344z00_4755)==0L));}  else 
{ /* Llib/thread.scm 647 */
 int BgL_arg1346z00_4756;
BgL_arg1346z00_4756 = 
BGL_MUTEX_TIMED_LOCK(BgL_mz00_81, BgL_timeoutz00_82); 
return 
BBOOL(
(
(long)(BgL_arg1346z00_4756)==0L));} } 

}



/* mutex-unlock! */
BGL_EXPORTED_DEF obj_t BGl_mutexzd2unlockz12zc0zz__threadz00(obj_t BgL_mz00_85)
{
{ /* Llib/thread.scm 652 */
{ /* Llib/thread.scm 653 */
 int BgL_arg1347z00_4757;
BgL_arg1347z00_4757 = 
BGL_MUTEX_UNLOCK(BgL_mz00_85); 
return 
BBOOL(
(
(long)(BgL_arg1347z00_4757)==0L));} } 

}



/* &mutex-unlock! */
obj_t BGl_z62mutexzd2unlockz12za2zz__threadz00(obj_t BgL_envz00_3483, obj_t BgL_mz00_3484)
{
{ /* Llib/thread.scm 652 */
{ /* Llib/thread.scm 653 */
 obj_t BgL_auxz00_9323;
if(
BGL_MUTEXP(BgL_mz00_3484))
{ /* Llib/thread.scm 653 */
BgL_auxz00_9323 = BgL_mz00_3484
; }  else 
{ 
 obj_t BgL_auxz00_9326;
BgL_auxz00_9326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(28760L), BGl_string2454z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_mz00_3484); 
FAILURE(BgL_auxz00_9326,BFALSE,BFALSE);} 
return 
BGl_mutexzd2unlockz12zc0zz__threadz00(BgL_auxz00_9323);} } 

}



/* mutex-state */
BGL_EXPORTED_DEF obj_t BGl_mutexzd2statezd2zz__threadz00(obj_t BgL_mutexz00_86)
{
{ /* Llib/thread.scm 658 */
return 
BGL_MUTEX_STATE(BgL_mutexz00_86);} 

}



/* &mutex-state */
obj_t BGl_z62mutexzd2statezb0zz__threadz00(obj_t BgL_envz00_3485, obj_t BgL_mutexz00_3486)
{
{ /* Llib/thread.scm 658 */
{ /* Llib/thread.scm 659 */
 obj_t BgL_auxz00_9332;
if(
BGL_MUTEXP(BgL_mutexz00_3486))
{ /* Llib/thread.scm 659 */
BgL_auxz00_9332 = BgL_mutexz00_3486
; }  else 
{ 
 obj_t BgL_auxz00_9335;
BgL_auxz00_9335 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29043L), BGl_string2455z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_mutexz00_3486); 
FAILURE(BgL_auxz00_9335,BFALSE,BFALSE);} 
return 
BGl_mutexzd2statezd2zz__threadz00(BgL_auxz00_9332);} } 

}



/* with-lock */
BGL_EXPORTED_DEF obj_t BGl_withzd2lockzd2zz__threadz00(obj_t BgL_mutexz00_88, obj_t BgL_thunkz00_89)
{
{ /* Llib/thread.scm 670 */
{ /* Llib/thread.scm 671 */
 obj_t BgL_top3351z00_9341;
BgL_top3351z00_9341 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BgL_mutexz00_88); 
BGL_EXITD_PUSH_PROTECT(BgL_top3351z00_9341, BgL_mutexz00_88); BUNSPEC; 
{ /* Llib/thread.scm 671 */
 obj_t BgL_tmp3350z00_9340;
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_89, 0))
{ /* Llib/thread.scm 672 */
BgL_tmp3350z00_9340 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_89); }  else 
{ /* Llib/thread.scm 672 */
FAILURE(BGl_string2456z00zz__threadz00,BGl_list2457z00zz__threadz00,BgL_thunkz00_89);} 
BGL_EXITD_POP_PROTECT(BgL_top3351z00_9341); BUNSPEC; 
BGL_MUTEX_UNLOCK(BgL_mutexz00_88); 
return BgL_tmp3350z00_9340;} } } 

}



/* &with-lock */
obj_t BGl_z62withzd2lockzb0zz__threadz00(obj_t BgL_envz00_3487, obj_t BgL_mutexz00_3488, obj_t BgL_thunkz00_3489)
{
{ /* Llib/thread.scm 670 */
{ /* Llib/thread.scm 671 */
 obj_t BgL_auxz00_9360; obj_t BgL_auxz00_9353;
if(
PROCEDUREP(BgL_thunkz00_3489))
{ /* Llib/thread.scm 671 */
BgL_auxz00_9360 = BgL_thunkz00_3489
; }  else 
{ 
 obj_t BgL_auxz00_9363;
BgL_auxz00_9363 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29606L), BGl_string2462z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_thunkz00_3489); 
FAILURE(BgL_auxz00_9363,BFALSE,BFALSE);} 
if(
BGL_MUTEXP(BgL_mutexz00_3488))
{ /* Llib/thread.scm 671 */
BgL_auxz00_9353 = BgL_mutexz00_3488
; }  else 
{ 
 obj_t BgL_auxz00_9356;
BgL_auxz00_9356 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29606L), BGl_string2462z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_mutexz00_3488); 
FAILURE(BgL_auxz00_9356,BFALSE,BFALSE);} 
return 
BGl_withzd2lockzd2zz__threadz00(BgL_auxz00_9353, BgL_auxz00_9360);} } 

}



/* with-timed-lock */
BGL_EXPORTED_DEF obj_t BGl_withzd2timedzd2lockz00zz__threadz00(obj_t BgL_mutexz00_90, int BgL_timeoutz00_91, obj_t BgL_thunkz00_92)
{
{ /* Llib/thread.scm 677 */
{ /* Llib/thread.scm 678 */
 bool_t BgL_test3355z00_9368;
{ /* Llib/thread.scm 678 */
 long BgL_timeoutz00_2375;
BgL_timeoutz00_2375 = 
(long)(BgL_timeoutz00_91); 
if(
(BgL_timeoutz00_2375==0L))
{ /* Llib/thread.scm 646 */
 int BgL_arg1344z00_2377;
BgL_arg1344z00_2377 = 
BGL_MUTEX_LOCK(BgL_mutexz00_90); 
BgL_test3355z00_9368 = 
(
(long)(BgL_arg1344z00_2377)==0L); }  else 
{ /* Llib/thread.scm 647 */
 int BgL_arg1346z00_2378;
BgL_arg1346z00_2378 = 
BGL_MUTEX_TIMED_LOCK(BgL_mutexz00_90, BgL_timeoutz00_2375); 
BgL_test3355z00_9368 = 
(
(long)(BgL_arg1346z00_2378)==0L); } } 
if(BgL_test3355z00_9368)
{ /* Llib/thread.scm 679 */
 obj_t BgL_exitd1103z00_2371;
BgL_exitd1103z00_2371 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/thread.scm 681 */
 obj_t BgL_zc3z04anonymousza31349ze3z87_3490;
BgL_zc3z04anonymousza31349ze3z87_3490 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31349ze3ze5zz__threadz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31349ze3z87_3490, 
(int)(0L), BgL_mutexz00_90); 
{ /* Llib/thread.scm 679 */
 obj_t BgL_arg1919z00_2385;
{ /* Llib/thread.scm 679 */
 obj_t BgL_arg1920z00_2386;
BgL_arg1920z00_2386 = 
BGL_EXITD_PROTECT(BgL_exitd1103z00_2371); 
BgL_arg1919z00_2385 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31349ze3z87_3490, BgL_arg1920z00_2386); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_2371, BgL_arg1919z00_2385); BUNSPEC; } 
{ /* Llib/thread.scm 680 */
 obj_t BgL_tmp1105z00_2373;
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_92, 0))
{ /* Llib/thread.scm 680 */
BgL_tmp1105z00_2373 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_92); }  else 
{ /* Llib/thread.scm 680 */
FAILURE(BGl_string2463z00zz__threadz00,BGl_list2457z00zz__threadz00,BgL_thunkz00_92);} 
{ /* Llib/thread.scm 679 */
 bool_t BgL_test3358z00_9393;
{ /* Llib/thread.scm 679 */
 obj_t BgL_arg1918z00_2388;
BgL_arg1918z00_2388 = 
BGL_EXITD_PROTECT(BgL_exitd1103z00_2371); 
BgL_test3358z00_9393 = 
PAIRP(BgL_arg1918z00_2388); } 
if(BgL_test3358z00_9393)
{ /* Llib/thread.scm 679 */
 obj_t BgL_arg1916z00_2389;
{ /* Llib/thread.scm 679 */
 obj_t BgL_arg1917z00_2390;
BgL_arg1917z00_2390 = 
BGL_EXITD_PROTECT(BgL_exitd1103z00_2371); 
{ /* Llib/thread.scm 679 */
 obj_t BgL_pairz00_2391;
if(
PAIRP(BgL_arg1917z00_2390))
{ /* Llib/thread.scm 679 */
BgL_pairz00_2391 = BgL_arg1917z00_2390; }  else 
{ 
 obj_t BgL_auxz00_9399;
BgL_auxz00_9399 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29953L), BGl_string2464z00zz__threadz00, BGl_string2416z00zz__threadz00, BgL_arg1917z00_2390); 
FAILURE(BgL_auxz00_9399,BFALSE,BFALSE);} 
BgL_arg1916z00_2389 = 
CDR(BgL_pairz00_2391); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_2371, BgL_arg1916z00_2389); BUNSPEC; }  else 
{ /* Llib/thread.scm 679 */BFALSE; } } 
{ /* Llib/thread.scm 653 */
 int BgL_arg1347z00_2393;
BgL_arg1347z00_2393 = 
BGL_MUTEX_UNLOCK(BgL_mutexz00_90); 
(
(long)(BgL_arg1347z00_2393)==0L); } 
return BgL_tmp1105z00_2373;} } }  else 
{ /* Llib/thread.scm 678 */
return BFALSE;} } } 

}



/* &with-timed-lock */
obj_t BGl_z62withzd2timedzd2lockz62zz__threadz00(obj_t BgL_envz00_3491, obj_t BgL_mutexz00_3492, obj_t BgL_timeoutz00_3493, obj_t BgL_thunkz00_3494)
{
{ /* Llib/thread.scm 677 */
{ /* Llib/thread.scm 678 */
 obj_t BgL_auxz00_9424; int BgL_auxz00_9415; obj_t BgL_auxz00_9408;
if(
PROCEDUREP(BgL_thunkz00_3494))
{ /* Llib/thread.scm 678 */
BgL_auxz00_9424 = BgL_thunkz00_3494
; }  else 
{ 
 obj_t BgL_auxz00_9427;
BgL_auxz00_9427 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29913L), BGl_string2465z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_thunkz00_3494); 
FAILURE(BgL_auxz00_9427,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 678 */
 obj_t BgL_tmpz00_9416;
if(
INTEGERP(BgL_timeoutz00_3493))
{ /* Llib/thread.scm 678 */
BgL_tmpz00_9416 = BgL_timeoutz00_3493
; }  else 
{ 
 obj_t BgL_auxz00_9419;
BgL_auxz00_9419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29913L), BGl_string2465z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_timeoutz00_3493); 
FAILURE(BgL_auxz00_9419,BFALSE,BFALSE);} 
BgL_auxz00_9415 = 
CINT(BgL_tmpz00_9416); } 
if(
BGL_MUTEXP(BgL_mutexz00_3492))
{ /* Llib/thread.scm 678 */
BgL_auxz00_9408 = BgL_mutexz00_3492
; }  else 
{ 
 obj_t BgL_auxz00_9411;
BgL_auxz00_9411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(29913L), BGl_string2465z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_mutexz00_3492); 
FAILURE(BgL_auxz00_9411,BFALSE,BFALSE);} 
return 
BGl_withzd2timedzd2lockz00zz__threadz00(BgL_auxz00_9408, BgL_auxz00_9415, BgL_auxz00_9424);} } 

}



/* &<@anonymous:1349> */
obj_t BGl_z62zc3z04anonymousza31349ze3ze5zz__threadz00(obj_t BgL_envz00_3495)
{
{ /* Llib/thread.scm 679 */
{ /* Llib/thread.scm 653 */
 obj_t BgL_mutexz00_3496;
BgL_mutexz00_3496 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_3495, 
(int)(0L))); 
{ /* Llib/thread.scm 653 */
 bool_t BgL_tmpz00_9435;
{ /* Llib/thread.scm 653 */
 int BgL_arg1347z00_4758;
BgL_arg1347z00_4758 = 
BGL_MUTEX_UNLOCK(BgL_mutexz00_3496); 
BgL_tmpz00_9435 = 
(
(long)(BgL_arg1347z00_4758)==0L); } 
return 
BBOOL(BgL_tmpz00_9435);} } } 

}



/* condition-variable? */
BGL_EXPORTED_DEF bool_t BGl_conditionzd2variablezf3z21zz__threadz00(obj_t BgL_objz00_93)
{
{ /* Llib/thread.scm 686 */
return 
BGL_CONDVARP(BgL_objz00_93);} 

}



/* &condition-variable? */
obj_t BGl_z62conditionzd2variablezf3z43zz__threadz00(obj_t BgL_envz00_3497, obj_t BgL_objz00_3498)
{
{ /* Llib/thread.scm 686 */
return 
BBOOL(
BGl_conditionzd2variablezf3z21zz__threadz00(BgL_objz00_3498));} 

}



/* _make-condition-variable */
obj_t BGl__makezd2conditionzd2variablez00zz__threadz00(obj_t BgL_env1269z00_96, obj_t BgL_opt1268z00_95)
{
{ /* Llib/thread.scm 692 */
{ /* Llib/thread.scm 692 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1268z00_95)) { case 0L : 

{ /* Llib/thread.scm 693 */
 obj_t BgL_namez00_4760;
BgL_namez00_4760 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2468z00zz__threadz00); 
{ /* Llib/thread.scm 692 */

return 
bgl_make_condvar(BgL_namez00_4760);} } break;case 1L : 

{ /* Llib/thread.scm 692 */
 obj_t BgL_namez00_4761;
BgL_namez00_4761 = 
VECTOR_REF(BgL_opt1268z00_95,0L); 
{ /* Llib/thread.scm 692 */

return 
bgl_make_condvar(BgL_namez00_4761);} } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2466z00zz__threadz00, BGl_string2442z00zz__threadz00, 
BINT(
VECTOR_LENGTH(BgL_opt1268z00_95)));} } } } 

}



/* make-condition-variable */
BGL_EXPORTED_DEF obj_t BGl_makezd2conditionzd2variablez00zz__threadz00(obj_t BgL_namez00_94)
{
{ /* Llib/thread.scm 692 */
return 
bgl_make_condvar(BgL_namez00_94);} 

}



/* condition-variable-nil */
BGL_EXPORTED_DEF obj_t BGl_conditionzd2variablezd2nilz00zz__threadz00(void)
{
{ /* Llib/thread.scm 700 */
return BGl_za2conditionzd2variablezd2nilza2z00zz__threadz00;} 

}



/* &condition-variable-nil */
obj_t BGl_z62conditionzd2variablezd2nilz62zz__threadz00(obj_t BgL_envz00_3499)
{
{ /* Llib/thread.scm 700 */
return 
BGl_conditionzd2variablezd2nilz00zz__threadz00();} 

}



/* condition-variable-name */
BGL_EXPORTED_DEF obj_t BGl_conditionzd2variablezd2namez00zz__threadz00(obj_t BgL_objz00_97)
{
{ /* Llib/thread.scm 705 */
return 
BGL_CONDVAR_NAME(BgL_objz00_97);} 

}



/* &condition-variable-name */
obj_t BGl_z62conditionzd2variablezd2namez62zz__threadz00(obj_t BgL_envz00_3500, obj_t BgL_objz00_3501)
{
{ /* Llib/thread.scm 705 */
{ /* Llib/thread.scm 706 */
 obj_t BgL_auxz00_9455;
if(
BGL_CONDVARP(BgL_objz00_3501))
{ /* Llib/thread.scm 706 */
BgL_auxz00_9455 = BgL_objz00_3501
; }  else 
{ 
 obj_t BgL_auxz00_9458;
BgL_auxz00_9458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31238L), BGl_string2470z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_objz00_3501); 
FAILURE(BgL_auxz00_9458,BFALSE,BFALSE);} 
return 
BGl_conditionzd2variablezd2namez00zz__threadz00(BgL_auxz00_9455);} } 

}



/* _condition-variable-wait! */
obj_t BGl__conditionzd2variablezd2waitz12z12zz__threadz00(obj_t BgL_env1273z00_102, obj_t BgL_opt1272z00_101)
{
{ /* Llib/thread.scm 711 */
{ /* Llib/thread.scm 711 */
 obj_t BgL_g1274z00_4762; obj_t BgL_g1275z00_4763;
BgL_g1274z00_4762 = 
VECTOR_REF(BgL_opt1272z00_101,0L); 
BgL_g1275z00_4763 = 
VECTOR_REF(BgL_opt1272z00_101,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1272z00_101)) { case 2L : 

{ /* Llib/thread.scm 711 */

{ /* Llib/thread.scm 711 */
 obj_t BgL_cz00_4765; obj_t BgL_mz00_4766;
if(
BGL_CONDVARP(BgL_g1274z00_4762))
{ /* Llib/thread.scm 711 */
BgL_cz00_4765 = BgL_g1274z00_4762; }  else 
{ 
 obj_t BgL_auxz00_9467;
BgL_auxz00_9467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31482L), BGl_string2475z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_g1274z00_4762); 
FAILURE(BgL_auxz00_9467,BFALSE,BFALSE);} 
if(
BGL_MUTEXP(BgL_g1275z00_4763))
{ /* Llib/thread.scm 711 */
BgL_mz00_4766 = BgL_g1275z00_4763; }  else 
{ 
 obj_t BgL_auxz00_9473;
BgL_auxz00_9473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31482L), BGl_string2475z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_g1275z00_4763); 
FAILURE(BgL_auxz00_9473,BFALSE,BFALSE);} 
return 
BBOOL(
BGL_CONDVAR_WAIT(BgL_cz00_4765, BgL_mz00_4766));} } break;case 3L : 

{ /* Llib/thread.scm 711 */
 obj_t BgL_timeoutz00_4767;
BgL_timeoutz00_4767 = 
VECTOR_REF(BgL_opt1272z00_101,2L); 
{ /* Llib/thread.scm 711 */

{ /* Llib/thread.scm 711 */
 obj_t BgL_cz00_4768; obj_t BgL_mz00_4769; long BgL_timeoutz00_4770;
if(
BGL_CONDVARP(BgL_g1274z00_4762))
{ /* Llib/thread.scm 711 */
BgL_cz00_4768 = BgL_g1274z00_4762; }  else 
{ 
 obj_t BgL_auxz00_9482;
BgL_auxz00_9482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31482L), BGl_string2475z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_g1274z00_4762); 
FAILURE(BgL_auxz00_9482,BFALSE,BFALSE);} 
if(
BGL_MUTEXP(BgL_g1275z00_4763))
{ /* Llib/thread.scm 711 */
BgL_mz00_4769 = BgL_g1275z00_4763; }  else 
{ 
 obj_t BgL_auxz00_9488;
BgL_auxz00_9488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31482L), BGl_string2475z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_g1275z00_4763); 
FAILURE(BgL_auxz00_9488,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 711 */
 obj_t BgL_tmpz00_9492;
if(
INTEGERP(BgL_timeoutz00_4767))
{ /* Llib/thread.scm 711 */
BgL_tmpz00_9492 = BgL_timeoutz00_4767
; }  else 
{ 
 obj_t BgL_auxz00_9495;
BgL_auxz00_9495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31482L), BGl_string2475z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_timeoutz00_4767); 
FAILURE(BgL_auxz00_9495,BFALSE,BFALSE);} 
BgL_timeoutz00_4770 = 
(long)CINT(BgL_tmpz00_9492); } 
if(
(BgL_timeoutz00_4770==0L))
{ /* Llib/thread.scm 712 */
return 
BBOOL(
BGL_CONDVAR_WAIT(BgL_cz00_4768, BgL_mz00_4769));}  else 
{ /* Llib/thread.scm 712 */
return 
BBOOL(
BGL_CONDVAR_TIMED_WAIT(BgL_cz00_4768, BgL_mz00_4769, BgL_timeoutz00_4770));} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2472z00zz__threadz00, BGl_string2474z00zz__threadz00, 
BINT(
VECTOR_LENGTH(BgL_opt1272z00_101)));} } } } 

}



/* condition-variable-wait! */
BGL_EXPORTED_DEF bool_t BGl_conditionzd2variablezd2waitz12z12zz__threadz00(obj_t BgL_cz00_98, obj_t BgL_mz00_99, long BgL_timeoutz00_100)
{
{ /* Llib/thread.scm 711 */
if(
(BgL_timeoutz00_100==0L))
{ /* Llib/thread.scm 712 */
return 
BGL_CONDVAR_WAIT(BgL_cz00_98, BgL_mz00_99);}  else 
{ /* Llib/thread.scm 712 */
return 
BGL_CONDVAR_TIMED_WAIT(BgL_cz00_98, BgL_mz00_99, BgL_timeoutz00_100);} } 

}



/* condition-variable-signal! */
BGL_EXPORTED_DEF bool_t BGl_conditionzd2variablezd2signalz12z12zz__threadz00(obj_t BgL_cz00_103)
{
{ /* Llib/thread.scm 719 */
return 
BGL_CONDVAR_SIGNAL(BgL_cz00_103);} 

}



/* &condition-variable-signal! */
obj_t BGl_z62conditionzd2variablezd2signalz12z70zz__threadz00(obj_t BgL_envz00_3502, obj_t BgL_cz00_3503)
{
{ /* Llib/thread.scm 719 */
{ /* Llib/thread.scm 720 */
 bool_t BgL_tmpz00_9516;
{ /* Llib/thread.scm 720 */
 obj_t BgL_auxz00_9517;
if(
BGL_CONDVARP(BgL_cz00_3503))
{ /* Llib/thread.scm 720 */
BgL_auxz00_9517 = BgL_cz00_3503
; }  else 
{ 
 obj_t BgL_auxz00_9520;
BgL_auxz00_9520 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(31924L), BGl_string2476z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_cz00_3503); 
FAILURE(BgL_auxz00_9520,BFALSE,BFALSE);} 
BgL_tmpz00_9516 = 
BGl_conditionzd2variablezd2signalz12z12zz__threadz00(BgL_auxz00_9517); } 
return 
BBOOL(BgL_tmpz00_9516);} } 

}



/* condition-variable-broadcast! */
BGL_EXPORTED_DEF bool_t BGl_conditionzd2variablezd2broadcastz12z12zz__threadz00(obj_t BgL_cz00_104)
{
{ /* Llib/thread.scm 725 */
return 
BGL_CONDVAR_BROADCAST(BgL_cz00_104);} 

}



/* &condition-variable-broadcast! */
obj_t BGl_z62conditionzd2variablezd2broadcastz12z70zz__threadz00(obj_t BgL_envz00_3504, obj_t BgL_cz00_3505)
{
{ /* Llib/thread.scm 725 */
{ /* Llib/thread.scm 726 */
 bool_t BgL_tmpz00_9527;
{ /* Llib/thread.scm 726 */
 obj_t BgL_auxz00_9528;
if(
BGL_CONDVARP(BgL_cz00_3505))
{ /* Llib/thread.scm 726 */
BgL_auxz00_9528 = BgL_cz00_3505
; }  else 
{ 
 obj_t BgL_auxz00_9531;
BgL_auxz00_9531 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(32221L), BGl_string2477z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_cz00_3505); 
FAILURE(BgL_auxz00_9531,BFALSE,BFALSE);} 
BgL_tmpz00_9527 = 
BGl_conditionzd2variablezd2broadcastz12z12zz__threadz00(BgL_auxz00_9528); } 
return 
BBOOL(BgL_tmpz00_9527);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1358z00_1309; obj_t BgL_arg1359z00_1310;
{ /* Llib/thread.scm 161 */
 obj_t BgL_v1162z00_1321;
BgL_v1162z00_1321 = 
create_vector(1L); 
VECTOR_SET(BgL_v1162z00_1321,0L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2480z00zz__threadz00, BGl_proc2479z00zz__threadz00, BGl_proc2478z00zz__threadz00, ((bool_t)1), ((bool_t)0), BFALSE, BFALSE, BGl_symbol2482z00zz__threadz00)); 
BgL_arg1358z00_1309 = BgL_v1162z00_1321; } 
{ /* Llib/thread.scm 161 */
 obj_t BgL_v1163z00_1332;
BgL_v1163z00_1332 = 
create_vector(0L); 
BgL_arg1359z00_1310 = BgL_v1163z00_1332; } 
BGl_threadzd2backendzd2zz__threadz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2486z00zz__threadz00, BGl_symbol2487z00zz__threadz00, BGl_objectz00zz__objectz00, 20365L, BGl_proc2485z00zz__threadz00, BGl_proc2484z00zz__threadz00, BFALSE, BGl_proc2483z00zz__threadz00, BFALSE, BgL_arg1358z00_1309, BgL_arg1359z00_1310); } 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1372z00_1338; obj_t BgL_arg1373z00_1339;
{ /* Llib/thread.scm 164 */
 obj_t BgL_v1164z00_1345;
BgL_v1164z00_1345 = 
create_vector(3L); 
VECTOR_SET(BgL_v1164z00_1345,0L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2480z00zz__threadz00, BGl_proc2491z00zz__threadz00, BGl_proc2490z00zz__threadz00, ((bool_t)0), ((bool_t)0), BFALSE, BGl_proc2489z00zz__threadz00, BGl_symbol2492z00zz__threadz00)); 
VECTOR_SET(BgL_v1164z00_1345,1L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2496z00zz__threadz00, BGl_proc2495z00zz__threadz00, BGl_proc2494z00zz__threadz00, ((bool_t)0), ((bool_t)1), BFALSE, BFALSE, BGl_symbol2492z00zz__threadz00)); 
VECTOR_SET(BgL_v1164z00_1345,2L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2500z00zz__threadz00, BGl_proc2499z00zz__threadz00, BGl_proc2498z00zz__threadz00, ((bool_t)0), ((bool_t)1), BFALSE, BFALSE, BGl_symbol2492z00zz__threadz00)); 
BgL_arg1372z00_1338 = BgL_v1164z00_1345; } 
{ /* Llib/thread.scm 164 */
 obj_t BgL_v1165z00_1379;
BgL_v1165z00_1379 = 
create_vector(2L); 
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1396z00_1380;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1397z00_1381;
BgL_arg1397z00_1381 = 
MAKE_YOUNG_PAIR(BGl_proc2502z00zz__threadz00, BGl_proc2503z00zz__threadz00); 
BgL_arg1396z00_1380 = 
MAKE_YOUNG_PAIR(
BINT(0L), BgL_arg1397z00_1381); } 
VECTOR_SET(BgL_v1165z00_1379,0L,BgL_arg1396z00_1380); } 
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1405z00_1397;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1406z00_1398;
BgL_arg1406z00_1398 = 
MAKE_YOUNG_PAIR(BGl_proc2504z00zz__threadz00, BGl_proc2505z00zz__threadz00); 
BgL_arg1405z00_1397 = 
MAKE_YOUNG_PAIR(
BINT(1L), BgL_arg1406z00_1398); } 
VECTOR_SET(BgL_v1165z00_1379,1L,BgL_arg1405z00_1397); } 
BgL_arg1373z00_1339 = BgL_v1165z00_1379; } 
BGl_threadz00zz__threadz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2424z00zz__threadz00, BGl_symbol2487z00zz__threadz00, BGl_objectz00zz__objectz00, 35587L, BFALSE, BGl_proc2507z00zz__threadz00, BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00, BGl_proc2506z00zz__threadz00, BFALSE, BgL_arg1372z00_1338, BgL_arg1373z00_1339); } 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1416z00_1419; obj_t BgL_arg1417z00_1420;
{ /* Llib/thread.scm 170 */
 obj_t BgL_v1166z00_1438;
BgL_v1166z00_1438 = 
create_vector(6L); 
VECTOR_SET(BgL_v1166z00_1438,0L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2510z00zz__threadz00, BGl_proc2509z00zz__threadz00, BGl_proc2508z00zz__threadz00, ((bool_t)1), ((bool_t)0), BFALSE, BFALSE, BGl_symbol2512z00zz__threadz00)); 
VECTOR_SET(BgL_v1166z00_1438,1L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2516z00zz__threadz00, BGl_proc2515z00zz__threadz00, BGl_proc2514z00zz__threadz00, ((bool_t)0), ((bool_t)0), BFALSE, BGl_proc2513z00zz__threadz00, BGl_symbol2492z00zz__threadz00)); 
VECTOR_SET(BgL_v1166z00_1438,2L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2521z00zz__threadz00, BGl_proc2520z00zz__threadz00, BGl_proc2519z00zz__threadz00, ((bool_t)0), ((bool_t)0), BFALSE, BGl_proc2518z00zz__threadz00, BGl_symbol2492z00zz__threadz00)); 
VECTOR_SET(BgL_v1166z00_1438,3L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2526z00zz__threadz00, BGl_proc2525z00zz__threadz00, BGl_proc2524z00zz__threadz00, ((bool_t)0), ((bool_t)0), BFALSE, BGl_proc2523z00zz__threadz00, BGl_symbol2492z00zz__threadz00)); 
VECTOR_SET(BgL_v1166z00_1438,4L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2531z00zz__threadz00, BGl_proc2530z00zz__threadz00, BGl_proc2529z00zz__threadz00, ((bool_t)0), ((bool_t)0), BFALSE, BGl_proc2528z00zz__threadz00, BGl_symbol2492z00zz__threadz00)); 
VECTOR_SET(BgL_v1166z00_1438,5L,
BGl_makezd2classzd2fieldz00zz__objectz00(BGl_symbol2536z00zz__threadz00, BGl_proc2535z00zz__threadz00, BGl_proc2534z00zz__threadz00, ((bool_t)0), ((bool_t)0), BFALSE, BGl_proc2533z00zz__threadz00, BGl_symbol2482z00zz__threadz00)); 
BgL_arg1416z00_1419 = BgL_v1166z00_1438; } 
{ /* Llib/thread.scm 170 */
 obj_t BgL_v1167z00_1514;
BgL_v1167z00_1514 = 
create_vector(0L); 
BgL_arg1417z00_1420 = BgL_v1167z00_1514; } 
BGl_nothreadz00zz__threadz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2541z00zz__threadz00, BGl_symbol2487z00zz__threadz00, BGl_threadz00zz__threadz00, 50609L, BGl_proc2540z00zz__threadz00, BGl_proc2539z00zz__threadz00, BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00, BGl_proc2538z00zz__threadz00, BFALSE, BgL_arg1416z00_1419, BgL_arg1417z00_1420); } 
{ /* Llib/thread.scm 159 */
 obj_t BgL_arg1469z00_1521; obj_t BgL_arg1472z00_1522;
{ /* Llib/thread.scm 159 */
 obj_t BgL_v1168z00_1533;
BgL_v1168z00_1533 = 
create_vector(0L); 
BgL_arg1469z00_1521 = BgL_v1168z00_1533; } 
{ /* Llib/thread.scm 159 */
 obj_t BgL_v1169z00_1534;
BgL_v1169z00_1534 = 
create_vector(0L); 
BgL_arg1472z00_1522 = BgL_v1169z00_1534; } 
return ( 
BGl_nothreadzd2backendzd2zz__threadz00 = 
BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2545z00zz__threadz00, BGl_symbol2487z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00, 1284L, BGl_proc2544z00zz__threadz00, BGl_proc2543z00zz__threadz00, BFALSE, BGl_proc2542z00zz__threadz00, BFALSE, BgL_arg1469z00_1521, BgL_arg1472z00_1522), BUNSPEC) ;} } 

}



/* &<@anonymous:1477> */
obj_t BGl_z62zc3z04anonymousza31477ze3ze5zz__threadz00(obj_t BgL_envz00_3550, obj_t BgL_new1056z00_3551)
{
{ /* Llib/thread.scm 159 */
{ 
 BgL_nothreadzd2backendzd2_bglt BgL_auxz00_9577;
{ /* Llib/thread.scm 159 */
 BgL_nothreadzd2backendzd2_bglt BgL_new1056z00_4811;
{ /* Llib/thread.scm 159 */
 bool_t BgL_test3373z00_9578;
{ /* Llib/thread.scm 159 */
 obj_t BgL_classz00_4771;
BgL_classz00_4771 = BGl_nothreadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_new1056z00_3551))
{ /* Llib/thread.scm 159 */
 BgL_objectz00_bglt BgL_arg1932z00_4773; long BgL_arg1933z00_4774;
BgL_arg1932z00_4773 = 
(BgL_objectz00_bglt)(BgL_new1056z00_3551); 
BgL_arg1933z00_4774 = 
BGL_CLASS_DEPTH(BgL_classz00_4771); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 159 */
 long BgL_idxz00_4782;
BgL_idxz00_4782 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4773); 
{ /* Llib/thread.scm 159 */
 obj_t BgL_arg1923z00_4783;
{ /* Llib/thread.scm 159 */
 long BgL_arg1924z00_4784;
BgL_arg1924z00_4784 = 
(BgL_idxz00_4782+BgL_arg1933z00_4774); 
BgL_arg1923z00_4783 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4784); } 
BgL_test3373z00_9578 = 
(BgL_arg1923z00_4783==BgL_classz00_4771); } }  else 
{ /* Llib/thread.scm 159 */
 bool_t BgL_res2044z00_4789;
{ /* Llib/thread.scm 159 */
 obj_t BgL_oclassz00_4793;
{ /* Llib/thread.scm 159 */
 obj_t BgL_arg1937z00_4795; long BgL_arg1938z00_4796;
BgL_arg1937z00_4795 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 159 */
 long BgL_arg1939z00_4797;
BgL_arg1939z00_4797 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4773); 
BgL_arg1938z00_4796 = 
(BgL_arg1939z00_4797-OBJECT_TYPE); } 
BgL_oclassz00_4793 = 
VECTOR_REF(BgL_arg1937z00_4795,BgL_arg1938z00_4796); } 
{ /* Llib/thread.scm 159 */
 bool_t BgL__ortest_1147z00_4803;
BgL__ortest_1147z00_4803 = 
(BgL_classz00_4771==BgL_oclassz00_4793); 
if(BgL__ortest_1147z00_4803)
{ /* Llib/thread.scm 159 */
BgL_res2044z00_4789 = BgL__ortest_1147z00_4803; }  else 
{ /* Llib/thread.scm 159 */
 long BgL_odepthz00_4804;
{ /* Llib/thread.scm 159 */
 obj_t BgL_arg1927z00_4805;
BgL_arg1927z00_4805 = 
(BgL_oclassz00_4793); 
BgL_odepthz00_4804 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4805); } 
if(
(BgL_arg1933z00_4774<BgL_odepthz00_4804))
{ /* Llib/thread.scm 159 */
 obj_t BgL_arg1925z00_4809;
{ /* Llib/thread.scm 159 */
 obj_t BgL_arg1926z00_4810;
BgL_arg1926z00_4810 = 
(BgL_oclassz00_4793); 
BgL_arg1925z00_4809 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4810, BgL_arg1933z00_4774); } 
BgL_res2044z00_4789 = 
(BgL_arg1925z00_4809==BgL_classz00_4771); }  else 
{ /* Llib/thread.scm 159 */
BgL_res2044z00_4789 = ((bool_t)0); } } } } 
BgL_test3373z00_9578 = BgL_res2044z00_4789; } }  else 
{ /* Llib/thread.scm 159 */
BgL_test3373z00_9578 = ((bool_t)0)
; } } 
if(BgL_test3373z00_9578)
{ /* Llib/thread.scm 159 */
BgL_new1056z00_4811 = 
((BgL_nothreadzd2backendzd2_bglt)BgL_new1056z00_3551); }  else 
{ 
 obj_t BgL_auxz00_9603;
BgL_auxz00_9603 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5661L), BGl_string2548z00zz__threadz00, BGl_string2546z00zz__threadz00, BgL_new1056z00_3551); 
FAILURE(BgL_auxz00_9603,BFALSE,BFALSE);} } 
((((BgL_threadzd2backendzd2_bglt)COBJECT(
((BgL_threadzd2backendzd2_bglt)BgL_new1056z00_4811)))->BgL_namez00)=((obj_t)BGl_string2547z00zz__threadz00),BUNSPEC); 
BgL_auxz00_9577 = BgL_new1056z00_4811; } 
return 
((obj_t)BgL_auxz00_9577);} } 

}



/* &lambda1475 */
BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1475z62zz__threadz00(obj_t BgL_envz00_3552)
{
{ /* Llib/thread.scm 159 */
{ /* Llib/thread.scm 159 */
 BgL_nothreadzd2backendzd2_bglt BgL_new1055z00_4812;
BgL_new1055z00_4812 = 
((BgL_nothreadzd2backendzd2_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_nothreadzd2backendzd2_bgl) ))); 
{ /* Llib/thread.scm 159 */
 long BgL_arg1476z00_4813;
BgL_arg1476z00_4813 = 
BGL_CLASS_NUM(BGl_nothreadzd2backendzd2zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1055z00_4812), BgL_arg1476z00_4813); } 
return BgL_new1055z00_4812;} } 

}



/* &lambda1473 */
BgL_nothreadzd2backendzd2_bglt BGl_z62lambda1473z62zz__threadz00(obj_t BgL_envz00_3553, obj_t BgL_name1054z00_3554)
{
{ /* Llib/thread.scm 159 */
{ /* Llib/thread.scm 159 */
 obj_t BgL_name1054z00_4814;
if(
STRINGP(BgL_name1054z00_3554))
{ /* Llib/thread.scm 159 */
BgL_name1054z00_4814 = BgL_name1054z00_3554; }  else 
{ 
 obj_t BgL_auxz00_9616;
BgL_auxz00_9616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5661L), BGl_string2549z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_name1054z00_3554); 
FAILURE(BgL_auxz00_9616,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 159 */
 BgL_nothreadzd2backendzd2_bglt BgL_new1118z00_4815;
{ /* Llib/thread.scm 159 */
 BgL_nothreadzd2backendzd2_bglt BgL_new1116z00_4816;
BgL_new1116z00_4816 = 
((BgL_nothreadzd2backendzd2_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_nothreadzd2backendzd2_bgl) ))); 
{ /* Llib/thread.scm 159 */
 long BgL_arg1474z00_4817;
BgL_arg1474z00_4817 = 
BGL_CLASS_NUM(BGl_nothreadzd2backendzd2zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1116z00_4816), BgL_arg1474z00_4817); } 
BgL_new1118z00_4815 = BgL_new1116z00_4816; } 
((((BgL_threadzd2backendzd2_bglt)COBJECT(
((BgL_threadzd2backendzd2_bglt)BgL_new1118z00_4815)))->BgL_namez00)=((obj_t)BgL_name1054z00_4814),BUNSPEC); 
return BgL_new1118z00_4815;} } } 

}



/* &<@anonymous:1423> */
obj_t BGl_z62zc3z04anonymousza31423ze3ze5zz__threadz00(obj_t BgL_envz00_3555, obj_t BgL_new1052z00_3556)
{
{ /* Llib/thread.scm 170 */
{ 
 BgL_nothreadz00_bglt BgL_auxz00_9626;
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_new1052z00_4858;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3379z00_9627;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_4818;
BgL_classz00_4818 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_new1052z00_3556))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_4820; long BgL_arg1933z00_4821;
BgL_arg1932z00_4820 = 
(BgL_objectz00_bglt)(BgL_new1052z00_3556); 
BgL_arg1933z00_4821 = 
BGL_CLASS_DEPTH(BgL_classz00_4818); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_4829;
BgL_idxz00_4829 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4820); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_4830;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_4831;
BgL_arg1924z00_4831 = 
(BgL_idxz00_4829+BgL_arg1933z00_4821); 
BgL_arg1923z00_4830 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4831); } 
BgL_test3379z00_9627 = 
(BgL_arg1923z00_4830==BgL_classz00_4818); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_4836;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_4840;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_4842; long BgL_arg1938z00_4843;
BgL_arg1937z00_4842 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_4844;
BgL_arg1939z00_4844 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4820); 
BgL_arg1938z00_4843 = 
(BgL_arg1939z00_4844-OBJECT_TYPE); } 
BgL_oclassz00_4840 = 
VECTOR_REF(BgL_arg1937z00_4842,BgL_arg1938z00_4843); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_4850;
BgL__ortest_1147z00_4850 = 
(BgL_classz00_4818==BgL_oclassz00_4840); 
if(BgL__ortest_1147z00_4850)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4836 = BgL__ortest_1147z00_4850; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_4851;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_4852;
BgL_arg1927z00_4852 = 
(BgL_oclassz00_4840); 
BgL_odepthz00_4851 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4852); } 
if(
(BgL_arg1933z00_4821<BgL_odepthz00_4851))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_4856;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_4857;
BgL_arg1926z00_4857 = 
(BgL_oclassz00_4840); 
BgL_arg1925z00_4856 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4857, BgL_arg1933z00_4821); } 
BgL_res2044z00_4836 = 
(BgL_arg1925z00_4856==BgL_classz00_4818); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4836 = ((bool_t)0); } } } } 
BgL_test3379z00_9627 = BgL_res2044z00_4836; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3379z00_9627 = ((bool_t)0)
; } } 
if(BgL_test3379z00_9627)
{ /* Llib/thread.scm 170 */
BgL_new1052z00_4858 = 
((BgL_nothreadz00_bglt)BgL_new1052z00_3556); }  else 
{ 
 obj_t BgL_auxz00_9652;
BgL_auxz00_9652 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2550z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_new1052z00_3556); 
FAILURE(BgL_auxz00_9652,BFALSE,BFALSE);} } 
((((BgL_threadz00_bglt)COBJECT(
((BgL_threadz00_bglt)BgL_new1052z00_4858)))->BgL_namez00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1052z00_4858))->BgL_bodyz00)=((obj_t)BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1052z00_4858))->BgL_z52specificz52)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1052z00_4858))->BgL_z52cleanupz52)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1052z00_4858))->BgL_endzd2resultzd2)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1052z00_4858))->BgL_endzd2exceptionzd2)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1052z00_4858))->BgL_z52namez52)=((obj_t)BGl_string2547z00zz__threadz00),BUNSPEC); 
BgL_auxz00_9626 = BgL_new1052z00_4858; } 
return 
((obj_t)BgL_auxz00_9626);} } 

}



/* &lambda1421 */
BgL_nothreadz00_bglt BGl_z62lambda1421z62zz__threadz00(obj_t BgL_envz00_3557)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_new1051z00_4859;
BgL_new1051z00_4859 = 
((BgL_nothreadz00_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_nothreadz00_bgl) ))); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1422z00_4860;
BgL_arg1422z00_4860 = 
BGL_CLASS_NUM(BGl_nothreadz00zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1051z00_4859), BgL_arg1422z00_4860); } 
return BgL_new1051z00_4859;} } 

}



/* &lambda1418 */
BgL_nothreadz00_bglt BGl_z62lambda1418z62zz__threadz00(obj_t BgL_envz00_3558, obj_t BgL_name1044z00_3559, obj_t BgL_body1045z00_3560, obj_t BgL_z52specific1046z52_3561, obj_t BgL_z52cleanup1047z52_3562, obj_t BgL_endzd2result1048zd2_3563, obj_t BgL_endzd2exception1049zd2_3564, obj_t BgL_z52name1050z52_3565)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 obj_t BgL_body1045z00_4861; obj_t BgL_z52name1050z52_4862;
if(
PROCEDUREP(BgL_body1045z00_3560))
{ /* Llib/thread.scm 170 */
BgL_body1045z00_4861 = BgL_body1045z00_3560; }  else 
{ 
 obj_t BgL_auxz00_9671;
BgL_auxz00_9671 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2558z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_body1045z00_3560); 
FAILURE(BgL_auxz00_9671,BFALSE,BFALSE);} 
if(
STRINGP(BgL_z52name1050z52_3565))
{ /* Llib/thread.scm 170 */
BgL_z52name1050z52_4862 = BgL_z52name1050z52_3565; }  else 
{ 
 obj_t BgL_auxz00_9677;
BgL_auxz00_9677 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2558z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_z52name1050z52_3565); 
FAILURE(BgL_auxz00_9677,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_new1115z00_4863;
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_new1114z00_4864;
BgL_new1114z00_4864 = 
((BgL_nothreadz00_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_nothreadz00_bgl) ))); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1420z00_4865;
BgL_arg1420z00_4865 = 
BGL_CLASS_NUM(BGl_nothreadz00zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1114z00_4864), BgL_arg1420z00_4865); } 
BgL_new1115z00_4863 = BgL_new1114z00_4864; } 
((((BgL_threadz00_bglt)COBJECT(
((BgL_threadz00_bglt)BgL_new1115z00_4863)))->BgL_namez00)=((obj_t)BgL_name1044z00_3559),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1115z00_4863))->BgL_bodyz00)=((obj_t)BgL_body1045z00_4861),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1115z00_4863))->BgL_z52specificz52)=((obj_t)BgL_z52specific1046z52_3561),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1115z00_4863))->BgL_z52cleanupz52)=((obj_t)BgL_z52cleanup1047z52_3562),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1115z00_4863))->BgL_endzd2resultzd2)=((obj_t)BgL_endzd2result1048zd2_3563),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1115z00_4863))->BgL_endzd2exceptionzd2)=((obj_t)BgL_endzd2exception1049zd2_3564),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1115z00_4863))->BgL_z52namez52)=((obj_t)BgL_z52name1050z52_4862),BUNSPEC); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_fun1419z00_4866;
BgL_fun1419z00_4866 = 
BGl_classzd2constructorzd2zz__objectz00(BGl_nothreadz00zz__threadz00); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_funz00_4867;
if(
PROCEDUREP(BgL_fun1419z00_4866))
{ /* Llib/thread.scm 170 */
BgL_funz00_4867 = BgL_fun1419z00_4866; }  else 
{ 
 obj_t BgL_auxz00_9696;
BgL_auxz00_9696 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2551z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_fun1419z00_4866); 
FAILURE(BgL_auxz00_9696,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4867, 1))
{ /* Llib/thread.scm 170 */
(VA_PROCEDUREP( BgL_funz00_4867 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4867))(BgL_fun1419z00_4866, 
((obj_t)BgL_new1115z00_4863), BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4867))(BgL_fun1419z00_4866, 
((obj_t)BgL_new1115z00_4863)) ); }  else 
{ /* Llib/thread.scm 170 */
FAILURE(BGl_string2552z00zz__threadz00,BGl_list2553z00zz__threadz00,BgL_funz00_4867);} } } 
return BgL_new1115z00_4863;} } } 

}



/* &<@anonymous:1465> */
obj_t BGl_z62zc3z04anonymousza31465ze3ze5zz__threadz00(obj_t BgL_envz00_3566)
{
{ /* Llib/thread.scm 170 */
return BGl_string2559z00zz__threadz00;} 

}



/* &lambda1464 */
obj_t BGl_z62lambda1464z62zz__threadz00(obj_t BgL_envz00_3567, obj_t BgL_oz00_3568, obj_t BgL_vz00_3569)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_4908; obj_t BgL_vz00_4909;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3388z00_9708;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_4868;
BgL_classz00_4868 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3568))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_4870; long BgL_arg1933z00_4871;
BgL_arg1932z00_4870 = 
(BgL_objectz00_bglt)(BgL_oz00_3568); 
BgL_arg1933z00_4871 = 
BGL_CLASS_DEPTH(BgL_classz00_4868); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_4879;
BgL_idxz00_4879 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4870); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_4880;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_4881;
BgL_arg1924z00_4881 = 
(BgL_idxz00_4879+BgL_arg1933z00_4871); 
BgL_arg1923z00_4880 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4881); } 
BgL_test3388z00_9708 = 
(BgL_arg1923z00_4880==BgL_classz00_4868); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_4886;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_4890;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_4892; long BgL_arg1938z00_4893;
BgL_arg1937z00_4892 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_4894;
BgL_arg1939z00_4894 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4870); 
BgL_arg1938z00_4893 = 
(BgL_arg1939z00_4894-OBJECT_TYPE); } 
BgL_oclassz00_4890 = 
VECTOR_REF(BgL_arg1937z00_4892,BgL_arg1938z00_4893); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_4900;
BgL__ortest_1147z00_4900 = 
(BgL_classz00_4868==BgL_oclassz00_4890); 
if(BgL__ortest_1147z00_4900)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4886 = BgL__ortest_1147z00_4900; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_4901;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_4902;
BgL_arg1927z00_4902 = 
(BgL_oclassz00_4890); 
BgL_odepthz00_4901 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4902); } 
if(
(BgL_arg1933z00_4871<BgL_odepthz00_4901))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_4906;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_4907;
BgL_arg1926z00_4907 = 
(BgL_oclassz00_4890); 
BgL_arg1925z00_4906 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4907, BgL_arg1933z00_4871); } 
BgL_res2044z00_4886 = 
(BgL_arg1925z00_4906==BgL_classz00_4868); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4886 = ((bool_t)0); } } } } 
BgL_test3388z00_9708 = BgL_res2044z00_4886; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3388z00_9708 = ((bool_t)0)
; } } 
if(BgL_test3388z00_9708)
{ /* Llib/thread.scm 170 */
BgL_oz00_4908 = 
((BgL_nothreadz00_bglt)BgL_oz00_3568); }  else 
{ 
 obj_t BgL_auxz00_9733;
BgL_auxz00_9733 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2560z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3568); 
FAILURE(BgL_auxz00_9733,BFALSE,BFALSE);} } 
if(
STRINGP(BgL_vz00_3569))
{ /* Llib/thread.scm 170 */
BgL_vz00_4909 = BgL_vz00_3569; }  else 
{ 
 obj_t BgL_auxz00_9739;
BgL_auxz00_9739 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2560z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_vz00_3569); 
FAILURE(BgL_auxz00_9739,BFALSE,BFALSE);} 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_4908))->BgL_z52namez52)=((obj_t)BgL_vz00_4909),BUNSPEC);} } 

}



/* &lambda1463 */
obj_t BGl_z62lambda1463z62zz__threadz00(obj_t BgL_envz00_3570, obj_t BgL_oz00_3571)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_4950;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3394z00_9744;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_4910;
BgL_classz00_4910 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3571))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_4912; long BgL_arg1933z00_4913;
BgL_arg1932z00_4912 = 
(BgL_objectz00_bglt)(BgL_oz00_3571); 
BgL_arg1933z00_4913 = 
BGL_CLASS_DEPTH(BgL_classz00_4910); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_4921;
BgL_idxz00_4921 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4912); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_4922;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_4923;
BgL_arg1924z00_4923 = 
(BgL_idxz00_4921+BgL_arg1933z00_4913); 
BgL_arg1923z00_4922 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4923); } 
BgL_test3394z00_9744 = 
(BgL_arg1923z00_4922==BgL_classz00_4910); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_4928;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_4932;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_4934; long BgL_arg1938z00_4935;
BgL_arg1937z00_4934 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_4936;
BgL_arg1939z00_4936 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4912); 
BgL_arg1938z00_4935 = 
(BgL_arg1939z00_4936-OBJECT_TYPE); } 
BgL_oclassz00_4932 = 
VECTOR_REF(BgL_arg1937z00_4934,BgL_arg1938z00_4935); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_4942;
BgL__ortest_1147z00_4942 = 
(BgL_classz00_4910==BgL_oclassz00_4932); 
if(BgL__ortest_1147z00_4942)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4928 = BgL__ortest_1147z00_4942; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_4943;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_4944;
BgL_arg1927z00_4944 = 
(BgL_oclassz00_4932); 
BgL_odepthz00_4943 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4944); } 
if(
(BgL_arg1933z00_4913<BgL_odepthz00_4943))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_4948;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_4949;
BgL_arg1926z00_4949 = 
(BgL_oclassz00_4932); 
BgL_arg1925z00_4948 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4949, BgL_arg1933z00_4913); } 
BgL_res2044z00_4928 = 
(BgL_arg1925z00_4948==BgL_classz00_4910); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4928 = ((bool_t)0); } } } } 
BgL_test3394z00_9744 = BgL_res2044z00_4928; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3394z00_9744 = ((bool_t)0)
; } } 
if(BgL_test3394z00_9744)
{ /* Llib/thread.scm 170 */
BgL_oz00_4950 = 
((BgL_nothreadz00_bglt)BgL_oz00_3571); }  else 
{ 
 obj_t BgL_auxz00_9769;
BgL_auxz00_9769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2561z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3571); 
FAILURE(BgL_auxz00_9769,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_4950))->BgL_z52namez52);} } 

}



/* &<@anonymous:1458> */
obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__threadz00(obj_t BgL_envz00_3572)
{
{ /* Llib/thread.scm 170 */
return BUNSPEC;} 

}



/* &lambda1457 */
obj_t BGl_z62lambda1457z62zz__threadz00(obj_t BgL_envz00_3573, obj_t BgL_oz00_3574, obj_t BgL_vz00_3575)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_4991;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3399z00_9774;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_4951;
BgL_classz00_4951 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3574))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_4953; long BgL_arg1933z00_4954;
BgL_arg1932z00_4953 = 
(BgL_objectz00_bglt)(BgL_oz00_3574); 
BgL_arg1933z00_4954 = 
BGL_CLASS_DEPTH(BgL_classz00_4951); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_4962;
BgL_idxz00_4962 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4953); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_4963;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_4964;
BgL_arg1924z00_4964 = 
(BgL_idxz00_4962+BgL_arg1933z00_4954); 
BgL_arg1923z00_4963 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_4964); } 
BgL_test3399z00_9774 = 
(BgL_arg1923z00_4963==BgL_classz00_4951); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_4969;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_4973;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_4975; long BgL_arg1938z00_4976;
BgL_arg1937z00_4975 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_4977;
BgL_arg1939z00_4977 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4953); 
BgL_arg1938z00_4976 = 
(BgL_arg1939z00_4977-OBJECT_TYPE); } 
BgL_oclassz00_4973 = 
VECTOR_REF(BgL_arg1937z00_4975,BgL_arg1938z00_4976); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_4983;
BgL__ortest_1147z00_4983 = 
(BgL_classz00_4951==BgL_oclassz00_4973); 
if(BgL__ortest_1147z00_4983)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4969 = BgL__ortest_1147z00_4983; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_4984;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_4985;
BgL_arg1927z00_4985 = 
(BgL_oclassz00_4973); 
BgL_odepthz00_4984 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_4985); } 
if(
(BgL_arg1933z00_4954<BgL_odepthz00_4984))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_4989;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_4990;
BgL_arg1926z00_4990 = 
(BgL_oclassz00_4973); 
BgL_arg1925z00_4989 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_4990, BgL_arg1933z00_4954); } 
BgL_res2044z00_4969 = 
(BgL_arg1925z00_4989==BgL_classz00_4951); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_4969 = ((bool_t)0); } } } } 
BgL_test3399z00_9774 = BgL_res2044z00_4969; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3399z00_9774 = ((bool_t)0)
; } } 
if(BgL_test3399z00_9774)
{ /* Llib/thread.scm 170 */
BgL_oz00_4991 = 
((BgL_nothreadz00_bglt)BgL_oz00_3574); }  else 
{ 
 obj_t BgL_auxz00_9799;
BgL_auxz00_9799 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2562z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3574); 
FAILURE(BgL_auxz00_9799,BFALSE,BFALSE);} } 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_4991))->BgL_endzd2exceptionzd2)=((obj_t)BgL_vz00_3575),BUNSPEC);} } 

}



/* &lambda1456 */
obj_t BGl_z62lambda1456z62zz__threadz00(obj_t BgL_envz00_3576, obj_t BgL_oz00_3577)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5032;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3404z00_9804;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_4992;
BgL_classz00_4992 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3577))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_4994; long BgL_arg1933z00_4995;
BgL_arg1932z00_4994 = 
(BgL_objectz00_bglt)(BgL_oz00_3577); 
BgL_arg1933z00_4995 = 
BGL_CLASS_DEPTH(BgL_classz00_4992); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5003;
BgL_idxz00_5003 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_4994); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5004;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5005;
BgL_arg1924z00_5005 = 
(BgL_idxz00_5003+BgL_arg1933z00_4995); 
BgL_arg1923z00_5004 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5005); } 
BgL_test3404z00_9804 = 
(BgL_arg1923z00_5004==BgL_classz00_4992); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5010;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5014;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5016; long BgL_arg1938z00_5017;
BgL_arg1937z00_5016 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5018;
BgL_arg1939z00_5018 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_4994); 
BgL_arg1938z00_5017 = 
(BgL_arg1939z00_5018-OBJECT_TYPE); } 
BgL_oclassz00_5014 = 
VECTOR_REF(BgL_arg1937z00_5016,BgL_arg1938z00_5017); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5024;
BgL__ortest_1147z00_5024 = 
(BgL_classz00_4992==BgL_oclassz00_5014); 
if(BgL__ortest_1147z00_5024)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5010 = BgL__ortest_1147z00_5024; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5025;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5026;
BgL_arg1927z00_5026 = 
(BgL_oclassz00_5014); 
BgL_odepthz00_5025 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5026); } 
if(
(BgL_arg1933z00_4995<BgL_odepthz00_5025))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5030;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5031;
BgL_arg1926z00_5031 = 
(BgL_oclassz00_5014); 
BgL_arg1925z00_5030 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5031, BgL_arg1933z00_4995); } 
BgL_res2044z00_5010 = 
(BgL_arg1925z00_5030==BgL_classz00_4992); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5010 = ((bool_t)0); } } } } 
BgL_test3404z00_9804 = BgL_res2044z00_5010; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3404z00_9804 = ((bool_t)0)
; } } 
if(BgL_test3404z00_9804)
{ /* Llib/thread.scm 170 */
BgL_oz00_5032 = 
((BgL_nothreadz00_bglt)BgL_oz00_3577); }  else 
{ 
 obj_t BgL_auxz00_9829;
BgL_auxz00_9829 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2563z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3577); 
FAILURE(BgL_auxz00_9829,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5032))->BgL_endzd2exceptionzd2);} } 

}



/* &<@anonymous:1451> */
obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__threadz00(obj_t BgL_envz00_3578)
{
{ /* Llib/thread.scm 170 */
return BUNSPEC;} 

}



/* &lambda1450 */
obj_t BGl_z62lambda1450z62zz__threadz00(obj_t BgL_envz00_3579, obj_t BgL_oz00_3580, obj_t BgL_vz00_3581)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5073;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3409z00_9834;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5033;
BgL_classz00_5033 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3580))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5035; long BgL_arg1933z00_5036;
BgL_arg1932z00_5035 = 
(BgL_objectz00_bglt)(BgL_oz00_3580); 
BgL_arg1933z00_5036 = 
BGL_CLASS_DEPTH(BgL_classz00_5033); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5044;
BgL_idxz00_5044 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5035); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5045;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5046;
BgL_arg1924z00_5046 = 
(BgL_idxz00_5044+BgL_arg1933z00_5036); 
BgL_arg1923z00_5045 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5046); } 
BgL_test3409z00_9834 = 
(BgL_arg1923z00_5045==BgL_classz00_5033); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5051;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5055;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5057; long BgL_arg1938z00_5058;
BgL_arg1937z00_5057 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5059;
BgL_arg1939z00_5059 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5035); 
BgL_arg1938z00_5058 = 
(BgL_arg1939z00_5059-OBJECT_TYPE); } 
BgL_oclassz00_5055 = 
VECTOR_REF(BgL_arg1937z00_5057,BgL_arg1938z00_5058); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5065;
BgL__ortest_1147z00_5065 = 
(BgL_classz00_5033==BgL_oclassz00_5055); 
if(BgL__ortest_1147z00_5065)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5051 = BgL__ortest_1147z00_5065; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5066;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5067;
BgL_arg1927z00_5067 = 
(BgL_oclassz00_5055); 
BgL_odepthz00_5066 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5067); } 
if(
(BgL_arg1933z00_5036<BgL_odepthz00_5066))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5071;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5072;
BgL_arg1926z00_5072 = 
(BgL_oclassz00_5055); 
BgL_arg1925z00_5071 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5072, BgL_arg1933z00_5036); } 
BgL_res2044z00_5051 = 
(BgL_arg1925z00_5071==BgL_classz00_5033); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5051 = ((bool_t)0); } } } } 
BgL_test3409z00_9834 = BgL_res2044z00_5051; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3409z00_9834 = ((bool_t)0)
; } } 
if(BgL_test3409z00_9834)
{ /* Llib/thread.scm 170 */
BgL_oz00_5073 = 
((BgL_nothreadz00_bglt)BgL_oz00_3580); }  else 
{ 
 obj_t BgL_auxz00_9859;
BgL_auxz00_9859 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2564z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3580); 
FAILURE(BgL_auxz00_9859,BFALSE,BFALSE);} } 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5073))->BgL_endzd2resultzd2)=((obj_t)BgL_vz00_3581),BUNSPEC);} } 

}



/* &lambda1449 */
obj_t BGl_z62lambda1449z62zz__threadz00(obj_t BgL_envz00_3582, obj_t BgL_oz00_3583)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5114;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3414z00_9864;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5074;
BgL_classz00_5074 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3583))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5076; long BgL_arg1933z00_5077;
BgL_arg1932z00_5076 = 
(BgL_objectz00_bglt)(BgL_oz00_3583); 
BgL_arg1933z00_5077 = 
BGL_CLASS_DEPTH(BgL_classz00_5074); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5085;
BgL_idxz00_5085 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5076); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5086;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5087;
BgL_arg1924z00_5087 = 
(BgL_idxz00_5085+BgL_arg1933z00_5077); 
BgL_arg1923z00_5086 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5087); } 
BgL_test3414z00_9864 = 
(BgL_arg1923z00_5086==BgL_classz00_5074); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5092;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5096;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5098; long BgL_arg1938z00_5099;
BgL_arg1937z00_5098 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5100;
BgL_arg1939z00_5100 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5076); 
BgL_arg1938z00_5099 = 
(BgL_arg1939z00_5100-OBJECT_TYPE); } 
BgL_oclassz00_5096 = 
VECTOR_REF(BgL_arg1937z00_5098,BgL_arg1938z00_5099); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5106;
BgL__ortest_1147z00_5106 = 
(BgL_classz00_5074==BgL_oclassz00_5096); 
if(BgL__ortest_1147z00_5106)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5092 = BgL__ortest_1147z00_5106; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5107;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5108;
BgL_arg1927z00_5108 = 
(BgL_oclassz00_5096); 
BgL_odepthz00_5107 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5108); } 
if(
(BgL_arg1933z00_5077<BgL_odepthz00_5107))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5112;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5113;
BgL_arg1926z00_5113 = 
(BgL_oclassz00_5096); 
BgL_arg1925z00_5112 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5113, BgL_arg1933z00_5077); } 
BgL_res2044z00_5092 = 
(BgL_arg1925z00_5112==BgL_classz00_5074); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5092 = ((bool_t)0); } } } } 
BgL_test3414z00_9864 = BgL_res2044z00_5092; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3414z00_9864 = ((bool_t)0)
; } } 
if(BgL_test3414z00_9864)
{ /* Llib/thread.scm 170 */
BgL_oz00_5114 = 
((BgL_nothreadz00_bglt)BgL_oz00_3583); }  else 
{ 
 obj_t BgL_auxz00_9889;
BgL_auxz00_9889 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2565z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3583); 
FAILURE(BgL_auxz00_9889,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5114))->BgL_endzd2resultzd2);} } 

}



/* &<@anonymous:1444> */
obj_t BGl_z62zc3z04anonymousza31444ze3ze5zz__threadz00(obj_t BgL_envz00_3584)
{
{ /* Llib/thread.scm 170 */
return 
BBOOL(((bool_t)0));} 

}



/* &lambda1443 */
obj_t BGl_z62lambda1443z62zz__threadz00(obj_t BgL_envz00_3585, obj_t BgL_oz00_3586, obj_t BgL_vz00_3587)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5155;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3419z00_9895;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5115;
BgL_classz00_5115 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3586))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5117; long BgL_arg1933z00_5118;
BgL_arg1932z00_5117 = 
(BgL_objectz00_bglt)(BgL_oz00_3586); 
BgL_arg1933z00_5118 = 
BGL_CLASS_DEPTH(BgL_classz00_5115); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5126;
BgL_idxz00_5126 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5117); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5127;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5128;
BgL_arg1924z00_5128 = 
(BgL_idxz00_5126+BgL_arg1933z00_5118); 
BgL_arg1923z00_5127 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5128); } 
BgL_test3419z00_9895 = 
(BgL_arg1923z00_5127==BgL_classz00_5115); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5133;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5137;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5139; long BgL_arg1938z00_5140;
BgL_arg1937z00_5139 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5141;
BgL_arg1939z00_5141 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5117); 
BgL_arg1938z00_5140 = 
(BgL_arg1939z00_5141-OBJECT_TYPE); } 
BgL_oclassz00_5137 = 
VECTOR_REF(BgL_arg1937z00_5139,BgL_arg1938z00_5140); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5147;
BgL__ortest_1147z00_5147 = 
(BgL_classz00_5115==BgL_oclassz00_5137); 
if(BgL__ortest_1147z00_5147)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5133 = BgL__ortest_1147z00_5147; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5148;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5149;
BgL_arg1927z00_5149 = 
(BgL_oclassz00_5137); 
BgL_odepthz00_5148 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5149); } 
if(
(BgL_arg1933z00_5118<BgL_odepthz00_5148))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5153;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5154;
BgL_arg1926z00_5154 = 
(BgL_oclassz00_5137); 
BgL_arg1925z00_5153 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5154, BgL_arg1933z00_5118); } 
BgL_res2044z00_5133 = 
(BgL_arg1925z00_5153==BgL_classz00_5115); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5133 = ((bool_t)0); } } } } 
BgL_test3419z00_9895 = BgL_res2044z00_5133; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3419z00_9895 = ((bool_t)0)
; } } 
if(BgL_test3419z00_9895)
{ /* Llib/thread.scm 170 */
BgL_oz00_5155 = 
((BgL_nothreadz00_bglt)BgL_oz00_3586); }  else 
{ 
 obj_t BgL_auxz00_9920;
BgL_auxz00_9920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2566z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3586); 
FAILURE(BgL_auxz00_9920,BFALSE,BFALSE);} } 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5155))->BgL_z52cleanupz52)=((obj_t)BgL_vz00_3587),BUNSPEC);} } 

}



/* &lambda1442 */
obj_t BGl_z62lambda1442z62zz__threadz00(obj_t BgL_envz00_3588, obj_t BgL_oz00_3589)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5196;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3424z00_9925;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5156;
BgL_classz00_5156 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3589))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5158; long BgL_arg1933z00_5159;
BgL_arg1932z00_5158 = 
(BgL_objectz00_bglt)(BgL_oz00_3589); 
BgL_arg1933z00_5159 = 
BGL_CLASS_DEPTH(BgL_classz00_5156); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5167;
BgL_idxz00_5167 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5158); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5168;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5169;
BgL_arg1924z00_5169 = 
(BgL_idxz00_5167+BgL_arg1933z00_5159); 
BgL_arg1923z00_5168 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5169); } 
BgL_test3424z00_9925 = 
(BgL_arg1923z00_5168==BgL_classz00_5156); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5174;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5178;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5180; long BgL_arg1938z00_5181;
BgL_arg1937z00_5180 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5182;
BgL_arg1939z00_5182 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5158); 
BgL_arg1938z00_5181 = 
(BgL_arg1939z00_5182-OBJECT_TYPE); } 
BgL_oclassz00_5178 = 
VECTOR_REF(BgL_arg1937z00_5180,BgL_arg1938z00_5181); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5188;
BgL__ortest_1147z00_5188 = 
(BgL_classz00_5156==BgL_oclassz00_5178); 
if(BgL__ortest_1147z00_5188)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5174 = BgL__ortest_1147z00_5188; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5189;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5190;
BgL_arg1927z00_5190 = 
(BgL_oclassz00_5178); 
BgL_odepthz00_5189 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5190); } 
if(
(BgL_arg1933z00_5159<BgL_odepthz00_5189))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5194;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5195;
BgL_arg1926z00_5195 = 
(BgL_oclassz00_5178); 
BgL_arg1925z00_5194 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5195, BgL_arg1933z00_5159); } 
BgL_res2044z00_5174 = 
(BgL_arg1925z00_5194==BgL_classz00_5156); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5174 = ((bool_t)0); } } } } 
BgL_test3424z00_9925 = BgL_res2044z00_5174; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3424z00_9925 = ((bool_t)0)
; } } 
if(BgL_test3424z00_9925)
{ /* Llib/thread.scm 170 */
BgL_oz00_5196 = 
((BgL_nothreadz00_bglt)BgL_oz00_3589); }  else 
{ 
 obj_t BgL_auxz00_9950;
BgL_auxz00_9950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2567z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3589); 
FAILURE(BgL_auxz00_9950,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5196))->BgL_z52cleanupz52);} } 

}



/* &<@anonymous:1437> */
obj_t BGl_z62zc3z04anonymousza31437ze3ze5zz__threadz00(obj_t BgL_envz00_3590)
{
{ /* Llib/thread.scm 170 */
return BUNSPEC;} 

}



/* &lambda1436 */
obj_t BGl_z62lambda1436z62zz__threadz00(obj_t BgL_envz00_3591, obj_t BgL_oz00_3592, obj_t BgL_vz00_3593)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5237;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3429z00_9955;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5197;
BgL_classz00_5197 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3592))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5199; long BgL_arg1933z00_5200;
BgL_arg1932z00_5199 = 
(BgL_objectz00_bglt)(BgL_oz00_3592); 
BgL_arg1933z00_5200 = 
BGL_CLASS_DEPTH(BgL_classz00_5197); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5208;
BgL_idxz00_5208 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5199); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5209;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5210;
BgL_arg1924z00_5210 = 
(BgL_idxz00_5208+BgL_arg1933z00_5200); 
BgL_arg1923z00_5209 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5210); } 
BgL_test3429z00_9955 = 
(BgL_arg1923z00_5209==BgL_classz00_5197); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5215;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5219;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5221; long BgL_arg1938z00_5222;
BgL_arg1937z00_5221 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5223;
BgL_arg1939z00_5223 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5199); 
BgL_arg1938z00_5222 = 
(BgL_arg1939z00_5223-OBJECT_TYPE); } 
BgL_oclassz00_5219 = 
VECTOR_REF(BgL_arg1937z00_5221,BgL_arg1938z00_5222); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5229;
BgL__ortest_1147z00_5229 = 
(BgL_classz00_5197==BgL_oclassz00_5219); 
if(BgL__ortest_1147z00_5229)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5215 = BgL__ortest_1147z00_5229; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5230;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5231;
BgL_arg1927z00_5231 = 
(BgL_oclassz00_5219); 
BgL_odepthz00_5230 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5231); } 
if(
(BgL_arg1933z00_5200<BgL_odepthz00_5230))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5235;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5236;
BgL_arg1926z00_5236 = 
(BgL_oclassz00_5219); 
BgL_arg1925z00_5235 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5236, BgL_arg1933z00_5200); } 
BgL_res2044z00_5215 = 
(BgL_arg1925z00_5235==BgL_classz00_5197); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5215 = ((bool_t)0); } } } } 
BgL_test3429z00_9955 = BgL_res2044z00_5215; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3429z00_9955 = ((bool_t)0)
; } } 
if(BgL_test3429z00_9955)
{ /* Llib/thread.scm 170 */
BgL_oz00_5237 = 
((BgL_nothreadz00_bglt)BgL_oz00_3592); }  else 
{ 
 obj_t BgL_auxz00_9980;
BgL_auxz00_9980 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2568z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3592); 
FAILURE(BgL_auxz00_9980,BFALSE,BFALSE);} } 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5237))->BgL_z52specificz52)=((obj_t)BgL_vz00_3593),BUNSPEC);} } 

}



/* &lambda1435 */
obj_t BGl_z62lambda1435z62zz__threadz00(obj_t BgL_envz00_3594, obj_t BgL_oz00_3595)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5278;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3434z00_9985;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5238;
BgL_classz00_5238 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3595))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5240; long BgL_arg1933z00_5241;
BgL_arg1932z00_5240 = 
(BgL_objectz00_bglt)(BgL_oz00_3595); 
BgL_arg1933z00_5241 = 
BGL_CLASS_DEPTH(BgL_classz00_5238); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5249;
BgL_idxz00_5249 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5240); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5250;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5251;
BgL_arg1924z00_5251 = 
(BgL_idxz00_5249+BgL_arg1933z00_5241); 
BgL_arg1923z00_5250 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5251); } 
BgL_test3434z00_9985 = 
(BgL_arg1923z00_5250==BgL_classz00_5238); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5256;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5260;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5262; long BgL_arg1938z00_5263;
BgL_arg1937z00_5262 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5264;
BgL_arg1939z00_5264 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5240); 
BgL_arg1938z00_5263 = 
(BgL_arg1939z00_5264-OBJECT_TYPE); } 
BgL_oclassz00_5260 = 
VECTOR_REF(BgL_arg1937z00_5262,BgL_arg1938z00_5263); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5270;
BgL__ortest_1147z00_5270 = 
(BgL_classz00_5238==BgL_oclassz00_5260); 
if(BgL__ortest_1147z00_5270)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5256 = BgL__ortest_1147z00_5270; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5271;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5272;
BgL_arg1927z00_5272 = 
(BgL_oclassz00_5260); 
BgL_odepthz00_5271 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5272); } 
if(
(BgL_arg1933z00_5241<BgL_odepthz00_5271))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5276;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5277;
BgL_arg1926z00_5277 = 
(BgL_oclassz00_5260); 
BgL_arg1925z00_5276 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5277, BgL_arg1933z00_5241); } 
BgL_res2044z00_5256 = 
(BgL_arg1925z00_5276==BgL_classz00_5238); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5256 = ((bool_t)0); } } } } 
BgL_test3434z00_9985 = BgL_res2044z00_5256; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3434z00_9985 = ((bool_t)0)
; } } 
if(BgL_test3434z00_9985)
{ /* Llib/thread.scm 170 */
BgL_oz00_5278 = 
((BgL_nothreadz00_bglt)BgL_oz00_3595); }  else 
{ 
 obj_t BgL_auxz00_10010;
BgL_auxz00_10010 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2569z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3595); 
FAILURE(BgL_auxz00_10010,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5278))->BgL_z52specificz52);} } 

}



/* &lambda1428 */
obj_t BGl_z62lambda1428z62zz__threadz00(obj_t BgL_envz00_3596, obj_t BgL_oz00_3597, obj_t BgL_vz00_3598)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5319; obj_t BgL_vz00_5320;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3439z00_10015;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5279;
BgL_classz00_5279 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3597))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5281; long BgL_arg1933z00_5282;
BgL_arg1932z00_5281 = 
(BgL_objectz00_bglt)(BgL_oz00_3597); 
BgL_arg1933z00_5282 = 
BGL_CLASS_DEPTH(BgL_classz00_5279); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5290;
BgL_idxz00_5290 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5281); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5291;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5292;
BgL_arg1924z00_5292 = 
(BgL_idxz00_5290+BgL_arg1933z00_5282); 
BgL_arg1923z00_5291 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5292); } 
BgL_test3439z00_10015 = 
(BgL_arg1923z00_5291==BgL_classz00_5279); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5297;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5301;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5303; long BgL_arg1938z00_5304;
BgL_arg1937z00_5303 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5305;
BgL_arg1939z00_5305 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5281); 
BgL_arg1938z00_5304 = 
(BgL_arg1939z00_5305-OBJECT_TYPE); } 
BgL_oclassz00_5301 = 
VECTOR_REF(BgL_arg1937z00_5303,BgL_arg1938z00_5304); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5311;
BgL__ortest_1147z00_5311 = 
(BgL_classz00_5279==BgL_oclassz00_5301); 
if(BgL__ortest_1147z00_5311)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5297 = BgL__ortest_1147z00_5311; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5312;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5313;
BgL_arg1927z00_5313 = 
(BgL_oclassz00_5301); 
BgL_odepthz00_5312 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5313); } 
if(
(BgL_arg1933z00_5282<BgL_odepthz00_5312))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5317;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5318;
BgL_arg1926z00_5318 = 
(BgL_oclassz00_5301); 
BgL_arg1925z00_5317 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5318, BgL_arg1933z00_5282); } 
BgL_res2044z00_5297 = 
(BgL_arg1925z00_5317==BgL_classz00_5279); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5297 = ((bool_t)0); } } } } 
BgL_test3439z00_10015 = BgL_res2044z00_5297; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3439z00_10015 = ((bool_t)0)
; } } 
if(BgL_test3439z00_10015)
{ /* Llib/thread.scm 170 */
BgL_oz00_5319 = 
((BgL_nothreadz00_bglt)BgL_oz00_3597); }  else 
{ 
 obj_t BgL_auxz00_10040;
BgL_auxz00_10040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2570z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3597); 
FAILURE(BgL_auxz00_10040,BFALSE,BFALSE);} } 
if(
PROCEDUREP(BgL_vz00_3598))
{ /* Llib/thread.scm 170 */
BgL_vz00_5320 = BgL_vz00_3598; }  else 
{ 
 obj_t BgL_auxz00_10046;
BgL_auxz00_10046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2570z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_vz00_3598); 
FAILURE(BgL_auxz00_10046,BFALSE,BFALSE);} 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5319))->BgL_bodyz00)=((obj_t)BgL_vz00_5320),BUNSPEC);} } 

}



/* &lambda1427 */
obj_t BGl_z62lambda1427z62zz__threadz00(obj_t BgL_envz00_3599, obj_t BgL_oz00_3600)
{
{ /* Llib/thread.scm 170 */
{ /* Llib/thread.scm 170 */
 BgL_nothreadz00_bglt BgL_oz00_5361;
{ /* Llib/thread.scm 170 */
 bool_t BgL_test3445z00_10051;
{ /* Llib/thread.scm 170 */
 obj_t BgL_classz00_5321;
BgL_classz00_5321 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3600))
{ /* Llib/thread.scm 170 */
 BgL_objectz00_bglt BgL_arg1932z00_5323; long BgL_arg1933z00_5324;
BgL_arg1932z00_5323 = 
(BgL_objectz00_bglt)(BgL_oz00_3600); 
BgL_arg1933z00_5324 = 
BGL_CLASS_DEPTH(BgL_classz00_5321); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 170 */
 long BgL_idxz00_5332;
BgL_idxz00_5332 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5323); 
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1923z00_5333;
{ /* Llib/thread.scm 170 */
 long BgL_arg1924z00_5334;
BgL_arg1924z00_5334 = 
(BgL_idxz00_5332+BgL_arg1933z00_5324); 
BgL_arg1923z00_5333 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5334); } 
BgL_test3445z00_10051 = 
(BgL_arg1923z00_5333==BgL_classz00_5321); } }  else 
{ /* Llib/thread.scm 170 */
 bool_t BgL_res2044z00_5339;
{ /* Llib/thread.scm 170 */
 obj_t BgL_oclassz00_5343;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1937z00_5345; long BgL_arg1938z00_5346;
BgL_arg1937z00_5345 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 170 */
 long BgL_arg1939z00_5347;
BgL_arg1939z00_5347 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5323); 
BgL_arg1938z00_5346 = 
(BgL_arg1939z00_5347-OBJECT_TYPE); } 
BgL_oclassz00_5343 = 
VECTOR_REF(BgL_arg1937z00_5345,BgL_arg1938z00_5346); } 
{ /* Llib/thread.scm 170 */
 bool_t BgL__ortest_1147z00_5353;
BgL__ortest_1147z00_5353 = 
(BgL_classz00_5321==BgL_oclassz00_5343); 
if(BgL__ortest_1147z00_5353)
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5339 = BgL__ortest_1147z00_5353; }  else 
{ /* Llib/thread.scm 170 */
 long BgL_odepthz00_5354;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1927z00_5355;
BgL_arg1927z00_5355 = 
(BgL_oclassz00_5343); 
BgL_odepthz00_5354 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5355); } 
if(
(BgL_arg1933z00_5324<BgL_odepthz00_5354))
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1925z00_5359;
{ /* Llib/thread.scm 170 */
 obj_t BgL_arg1926z00_5360;
BgL_arg1926z00_5360 = 
(BgL_oclassz00_5343); 
BgL_arg1925z00_5359 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5360, BgL_arg1933z00_5324); } 
BgL_res2044z00_5339 = 
(BgL_arg1925z00_5359==BgL_classz00_5321); }  else 
{ /* Llib/thread.scm 170 */
BgL_res2044z00_5339 = ((bool_t)0); } } } } 
BgL_test3445z00_10051 = BgL_res2044z00_5339; } }  else 
{ /* Llib/thread.scm 170 */
BgL_test3445z00_10051 = ((bool_t)0)
; } } 
if(BgL_test3445z00_10051)
{ /* Llib/thread.scm 170 */
BgL_oz00_5361 = 
((BgL_nothreadz00_bglt)BgL_oz00_3600); }  else 
{ 
 obj_t BgL_auxz00_10076;
BgL_auxz00_10076 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(6013L), BGl_string2571z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_oz00_3600); 
FAILURE(BgL_auxz00_10076,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_oz00_5361))->BgL_bodyz00);} } 

}



/* &<@anonymous:1376> */
obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__threadz00(obj_t BgL_envz00_3601, obj_t BgL_new1042z00_3602)
{
{ /* Llib/thread.scm 164 */
{ 
 BgL_threadz00_bglt BgL_auxz00_10081;
{ /* Llib/thread.scm 164 */
 BgL_threadz00_bglt BgL_new1042z00_5402;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3450z00_10082;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_5362;
BgL_classz00_5362 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_new1042z00_3602))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_5364; long BgL_arg1933z00_5365;
BgL_arg1932z00_5364 = 
(BgL_objectz00_bglt)(BgL_new1042z00_3602); 
BgL_arg1933z00_5365 = 
BGL_CLASS_DEPTH(BgL_classz00_5362); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_5373;
BgL_idxz00_5373 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5364); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_5374;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_5375;
BgL_arg1924z00_5375 = 
(BgL_idxz00_5373+BgL_arg1933z00_5365); 
BgL_arg1923z00_5374 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5375); } 
BgL_test3450z00_10082 = 
(BgL_arg1923z00_5374==BgL_classz00_5362); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_5380;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_5384;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_5386; long BgL_arg1938z00_5387;
BgL_arg1937z00_5386 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_5388;
BgL_arg1939z00_5388 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5364); 
BgL_arg1938z00_5387 = 
(BgL_arg1939z00_5388-OBJECT_TYPE); } 
BgL_oclassz00_5384 = 
VECTOR_REF(BgL_arg1937z00_5386,BgL_arg1938z00_5387); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_5394;
BgL__ortest_1147z00_5394 = 
(BgL_classz00_5362==BgL_oclassz00_5384); 
if(BgL__ortest_1147z00_5394)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5380 = BgL__ortest_1147z00_5394; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_5395;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_5396;
BgL_arg1927z00_5396 = 
(BgL_oclassz00_5384); 
BgL_odepthz00_5395 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5396); } 
if(
(BgL_arg1933z00_5365<BgL_odepthz00_5395))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_5400;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_5401;
BgL_arg1926z00_5401 = 
(BgL_oclassz00_5384); 
BgL_arg1925z00_5400 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5401, BgL_arg1933z00_5365); } 
BgL_res2044z00_5380 = 
(BgL_arg1925z00_5400==BgL_classz00_5362); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5380 = ((bool_t)0); } } } } 
BgL_test3450z00_10082 = BgL_res2044z00_5380; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3450z00_10082 = ((bool_t)0)
; } } 
if(BgL_test3450z00_10082)
{ /* Llib/thread.scm 164 */
BgL_new1042z00_5402 = 
((BgL_threadz00_bglt)BgL_new1042z00_3602); }  else 
{ 
 obj_t BgL_auxz00_10107;
BgL_auxz00_10107 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2572z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_new1042z00_3602); 
FAILURE(BgL_auxz00_10107,BFALSE,BFALSE);} } 
((((BgL_threadz00_bglt)COBJECT(BgL_new1042z00_5402))->BgL_namez00)=((obj_t)BUNSPEC),BUNSPEC); 
BgL_auxz00_10081 = BgL_new1042z00_5402; } 
return 
((obj_t)BgL_auxz00_10081);} } 

}



/* &lambda1374 */
BgL_threadz00_bglt BGl_z62lambda1374z62zz__threadz00(obj_t BgL_envz00_3603)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_threadz00_bglt BgL_new1041z00_5403;
BgL_new1041z00_5403 = 
((BgL_threadz00_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_threadz00_bgl) ))); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1375z00_5404;
BgL_arg1375z00_5404 = 
BGL_CLASS_NUM(BGl_threadz00zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1041z00_5403), BgL_arg1375z00_5404); } 
return BgL_new1041z00_5403;} } 

}



/* &<@anonymous:1411> */
obj_t BGl_z62zc3z04anonymousza31411ze3ze5zz__threadz00(obj_t BgL_envz00_3604, obj_t BgL_oz00_3605, obj_t BgL_vz00_3606)
{
{ /* Llib/thread.scm 168 */
{ /* Llib/thread.scm 168 */
 BgL_threadz00_bglt BgL_auxz00_10117;
{ /* Llib/thread.scm 168 */
 bool_t BgL_test3455z00_10118;
{ /* Llib/thread.scm 168 */
 obj_t BgL_classz00_5445;
BgL_classz00_5445 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3605))
{ /* Llib/thread.scm 168 */
 BgL_objectz00_bglt BgL_arg1932z00_5447; long BgL_arg1933z00_5448;
BgL_arg1932z00_5447 = 
(BgL_objectz00_bglt)(BgL_oz00_3605); 
BgL_arg1933z00_5448 = 
BGL_CLASS_DEPTH(BgL_classz00_5445); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 168 */
 long BgL_idxz00_5456;
BgL_idxz00_5456 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5447); 
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1923z00_5457;
{ /* Llib/thread.scm 168 */
 long BgL_arg1924z00_5458;
BgL_arg1924z00_5458 = 
(BgL_idxz00_5456+BgL_arg1933z00_5448); 
BgL_arg1923z00_5457 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5458); } 
BgL_test3455z00_10118 = 
(BgL_arg1923z00_5457==BgL_classz00_5445); } }  else 
{ /* Llib/thread.scm 168 */
 bool_t BgL_res2044z00_5463;
{ /* Llib/thread.scm 168 */
 obj_t BgL_oclassz00_5467;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1937z00_5469; long BgL_arg1938z00_5470;
BgL_arg1937z00_5469 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 168 */
 long BgL_arg1939z00_5471;
BgL_arg1939z00_5471 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5447); 
BgL_arg1938z00_5470 = 
(BgL_arg1939z00_5471-OBJECT_TYPE); } 
BgL_oclassz00_5467 = 
VECTOR_REF(BgL_arg1937z00_5469,BgL_arg1938z00_5470); } 
{ /* Llib/thread.scm 168 */
 bool_t BgL__ortest_1147z00_5477;
BgL__ortest_1147z00_5477 = 
(BgL_classz00_5445==BgL_oclassz00_5467); 
if(BgL__ortest_1147z00_5477)
{ /* Llib/thread.scm 168 */
BgL_res2044z00_5463 = BgL__ortest_1147z00_5477; }  else 
{ /* Llib/thread.scm 168 */
 long BgL_odepthz00_5478;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1927z00_5479;
BgL_arg1927z00_5479 = 
(BgL_oclassz00_5467); 
BgL_odepthz00_5478 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5479); } 
if(
(BgL_arg1933z00_5448<BgL_odepthz00_5478))
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1925z00_5483;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1926z00_5484;
BgL_arg1926z00_5484 = 
(BgL_oclassz00_5467); 
BgL_arg1925z00_5483 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5484, BgL_arg1933z00_5448); } 
BgL_res2044z00_5463 = 
(BgL_arg1925z00_5483==BgL_classz00_5445); }  else 
{ /* Llib/thread.scm 168 */
BgL_res2044z00_5463 = ((bool_t)0); } } } } 
BgL_test3455z00_10118 = BgL_res2044z00_5463; } }  else 
{ /* Llib/thread.scm 168 */
BgL_test3455z00_10118 = ((bool_t)0)
; } } 
if(BgL_test3455z00_10118)
{ /* Llib/thread.scm 168 */
BgL_auxz00_10117 = 
((BgL_threadz00_bglt)BgL_oz00_3605)
; }  else 
{ 
 obj_t BgL_auxz00_10143;
BgL_auxz00_10143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5949L), BGl_string2573z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3605); 
FAILURE(BgL_auxz00_10143,BFALSE,BFALSE);} } 
return 
BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_auxz00_10117, BgL_vz00_3606);} } 

}



/* &<@anonymous:1409> */
obj_t BGl_z62zc3z04anonymousza31409ze3ze5zz__threadz00(obj_t BgL_envz00_3607, obj_t BgL_oz00_3608)
{
{ /* Llib/thread.scm 168 */
{ /* Llib/thread.scm 168 */
 BgL_threadz00_bglt BgL_auxz00_10148;
{ /* Llib/thread.scm 168 */
 bool_t BgL_test3460z00_10149;
{ /* Llib/thread.scm 168 */
 obj_t BgL_classz00_5525;
BgL_classz00_5525 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3608))
{ /* Llib/thread.scm 168 */
 BgL_objectz00_bglt BgL_arg1932z00_5527; long BgL_arg1933z00_5528;
BgL_arg1932z00_5527 = 
(BgL_objectz00_bglt)(BgL_oz00_3608); 
BgL_arg1933z00_5528 = 
BGL_CLASS_DEPTH(BgL_classz00_5525); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 168 */
 long BgL_idxz00_5536;
BgL_idxz00_5536 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5527); 
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1923z00_5537;
{ /* Llib/thread.scm 168 */
 long BgL_arg1924z00_5538;
BgL_arg1924z00_5538 = 
(BgL_idxz00_5536+BgL_arg1933z00_5528); 
BgL_arg1923z00_5537 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5538); } 
BgL_test3460z00_10149 = 
(BgL_arg1923z00_5537==BgL_classz00_5525); } }  else 
{ /* Llib/thread.scm 168 */
 bool_t BgL_res2044z00_5543;
{ /* Llib/thread.scm 168 */
 obj_t BgL_oclassz00_5547;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1937z00_5549; long BgL_arg1938z00_5550;
BgL_arg1937z00_5549 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 168 */
 long BgL_arg1939z00_5551;
BgL_arg1939z00_5551 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5527); 
BgL_arg1938z00_5550 = 
(BgL_arg1939z00_5551-OBJECT_TYPE); } 
BgL_oclassz00_5547 = 
VECTOR_REF(BgL_arg1937z00_5549,BgL_arg1938z00_5550); } 
{ /* Llib/thread.scm 168 */
 bool_t BgL__ortest_1147z00_5557;
BgL__ortest_1147z00_5557 = 
(BgL_classz00_5525==BgL_oclassz00_5547); 
if(BgL__ortest_1147z00_5557)
{ /* Llib/thread.scm 168 */
BgL_res2044z00_5543 = BgL__ortest_1147z00_5557; }  else 
{ /* Llib/thread.scm 168 */
 long BgL_odepthz00_5558;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1927z00_5559;
BgL_arg1927z00_5559 = 
(BgL_oclassz00_5547); 
BgL_odepthz00_5558 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5559); } 
if(
(BgL_arg1933z00_5528<BgL_odepthz00_5558))
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1925z00_5563;
{ /* Llib/thread.scm 168 */
 obj_t BgL_arg1926z00_5564;
BgL_arg1926z00_5564 = 
(BgL_oclassz00_5547); 
BgL_arg1925z00_5563 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5564, BgL_arg1933z00_5528); } 
BgL_res2044z00_5543 = 
(BgL_arg1925z00_5563==BgL_classz00_5525); }  else 
{ /* Llib/thread.scm 168 */
BgL_res2044z00_5543 = ((bool_t)0); } } } } 
BgL_test3460z00_10149 = BgL_res2044z00_5543; } }  else 
{ /* Llib/thread.scm 168 */
BgL_test3460z00_10149 = ((bool_t)0)
; } } 
if(BgL_test3460z00_10149)
{ /* Llib/thread.scm 168 */
BgL_auxz00_10148 = 
((BgL_threadz00_bglt)BgL_oz00_3608)
; }  else 
{ 
 obj_t BgL_auxz00_10174;
BgL_auxz00_10174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5949L), BGl_string2574z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3608); 
FAILURE(BgL_auxz00_10174,BFALSE,BFALSE);} } 
return 
BGl_threadzd2cleanupzd2zz__threadz00(BgL_auxz00_10148);} } 

}



/* &<@anonymous:1403> */
obj_t BGl_z62zc3z04anonymousza31403ze3ze5zz__threadz00(obj_t BgL_envz00_3609, obj_t BgL_oz00_3610, obj_t BgL_vz00_3611)
{
{ /* Llib/thread.scm 167 */
{ /* Llib/thread.scm 167 */
 BgL_threadz00_bglt BgL_auxz00_10179;
{ /* Llib/thread.scm 167 */
 bool_t BgL_test3465z00_10180;
{ /* Llib/thread.scm 167 */
 obj_t BgL_classz00_5605;
BgL_classz00_5605 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3610))
{ /* Llib/thread.scm 167 */
 BgL_objectz00_bglt BgL_arg1932z00_5607; long BgL_arg1933z00_5608;
BgL_arg1932z00_5607 = 
(BgL_objectz00_bglt)(BgL_oz00_3610); 
BgL_arg1933z00_5608 = 
BGL_CLASS_DEPTH(BgL_classz00_5605); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 167 */
 long BgL_idxz00_5616;
BgL_idxz00_5616 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5607); 
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1923z00_5617;
{ /* Llib/thread.scm 167 */
 long BgL_arg1924z00_5618;
BgL_arg1924z00_5618 = 
(BgL_idxz00_5616+BgL_arg1933z00_5608); 
BgL_arg1923z00_5617 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5618); } 
BgL_test3465z00_10180 = 
(BgL_arg1923z00_5617==BgL_classz00_5605); } }  else 
{ /* Llib/thread.scm 167 */
 bool_t BgL_res2044z00_5623;
{ /* Llib/thread.scm 167 */
 obj_t BgL_oclassz00_5627;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1937z00_5629; long BgL_arg1938z00_5630;
BgL_arg1937z00_5629 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 167 */
 long BgL_arg1939z00_5631;
BgL_arg1939z00_5631 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5607); 
BgL_arg1938z00_5630 = 
(BgL_arg1939z00_5631-OBJECT_TYPE); } 
BgL_oclassz00_5627 = 
VECTOR_REF(BgL_arg1937z00_5629,BgL_arg1938z00_5630); } 
{ /* Llib/thread.scm 167 */
 bool_t BgL__ortest_1147z00_5637;
BgL__ortest_1147z00_5637 = 
(BgL_classz00_5605==BgL_oclassz00_5627); 
if(BgL__ortest_1147z00_5637)
{ /* Llib/thread.scm 167 */
BgL_res2044z00_5623 = BgL__ortest_1147z00_5637; }  else 
{ /* Llib/thread.scm 167 */
 long BgL_odepthz00_5638;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1927z00_5639;
BgL_arg1927z00_5639 = 
(BgL_oclassz00_5627); 
BgL_odepthz00_5638 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5639); } 
if(
(BgL_arg1933z00_5608<BgL_odepthz00_5638))
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1925z00_5643;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1926z00_5644;
BgL_arg1926z00_5644 = 
(BgL_oclassz00_5627); 
BgL_arg1925z00_5643 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5644, BgL_arg1933z00_5608); } 
BgL_res2044z00_5623 = 
(BgL_arg1925z00_5643==BgL_classz00_5605); }  else 
{ /* Llib/thread.scm 167 */
BgL_res2044z00_5623 = ((bool_t)0); } } } } 
BgL_test3465z00_10180 = BgL_res2044z00_5623; } }  else 
{ /* Llib/thread.scm 167 */
BgL_test3465z00_10180 = ((bool_t)0)
; } } 
if(BgL_test3465z00_10180)
{ /* Llib/thread.scm 167 */
BgL_auxz00_10179 = 
((BgL_threadz00_bglt)BgL_oz00_3610)
; }  else 
{ 
 obj_t BgL_auxz00_10205;
BgL_auxz00_10205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5881L), BGl_string2575z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3610); 
FAILURE(BgL_auxz00_10205,BFALSE,BFALSE);} } 
return 
BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_auxz00_10179, BgL_vz00_3611);} } 

}



/* &<@anonymous:1401> */
obj_t BGl_z62zc3z04anonymousza31401ze3ze5zz__threadz00(obj_t BgL_envz00_3612, obj_t BgL_oz00_3613)
{
{ /* Llib/thread.scm 167 */
{ /* Llib/thread.scm 167 */
 BgL_threadz00_bglt BgL_auxz00_10210;
{ /* Llib/thread.scm 167 */
 bool_t BgL_test3470z00_10211;
{ /* Llib/thread.scm 167 */
 obj_t BgL_classz00_5685;
BgL_classz00_5685 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3613))
{ /* Llib/thread.scm 167 */
 BgL_objectz00_bglt BgL_arg1932z00_5687; long BgL_arg1933z00_5688;
BgL_arg1932z00_5687 = 
(BgL_objectz00_bglt)(BgL_oz00_3613); 
BgL_arg1933z00_5688 = 
BGL_CLASS_DEPTH(BgL_classz00_5685); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 167 */
 long BgL_idxz00_5696;
BgL_idxz00_5696 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5687); 
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1923z00_5697;
{ /* Llib/thread.scm 167 */
 long BgL_arg1924z00_5698;
BgL_arg1924z00_5698 = 
(BgL_idxz00_5696+BgL_arg1933z00_5688); 
BgL_arg1923z00_5697 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5698); } 
BgL_test3470z00_10211 = 
(BgL_arg1923z00_5697==BgL_classz00_5685); } }  else 
{ /* Llib/thread.scm 167 */
 bool_t BgL_res2044z00_5703;
{ /* Llib/thread.scm 167 */
 obj_t BgL_oclassz00_5707;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1937z00_5709; long BgL_arg1938z00_5710;
BgL_arg1937z00_5709 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 167 */
 long BgL_arg1939z00_5711;
BgL_arg1939z00_5711 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5687); 
BgL_arg1938z00_5710 = 
(BgL_arg1939z00_5711-OBJECT_TYPE); } 
BgL_oclassz00_5707 = 
VECTOR_REF(BgL_arg1937z00_5709,BgL_arg1938z00_5710); } 
{ /* Llib/thread.scm 167 */
 bool_t BgL__ortest_1147z00_5717;
BgL__ortest_1147z00_5717 = 
(BgL_classz00_5685==BgL_oclassz00_5707); 
if(BgL__ortest_1147z00_5717)
{ /* Llib/thread.scm 167 */
BgL_res2044z00_5703 = BgL__ortest_1147z00_5717; }  else 
{ /* Llib/thread.scm 167 */
 long BgL_odepthz00_5718;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1927z00_5719;
BgL_arg1927z00_5719 = 
(BgL_oclassz00_5707); 
BgL_odepthz00_5718 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5719); } 
if(
(BgL_arg1933z00_5688<BgL_odepthz00_5718))
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1925z00_5723;
{ /* Llib/thread.scm 167 */
 obj_t BgL_arg1926z00_5724;
BgL_arg1926z00_5724 = 
(BgL_oclassz00_5707); 
BgL_arg1925z00_5723 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5724, BgL_arg1933z00_5688); } 
BgL_res2044z00_5703 = 
(BgL_arg1925z00_5723==BgL_classz00_5685); }  else 
{ /* Llib/thread.scm 167 */
BgL_res2044z00_5703 = ((bool_t)0); } } } } 
BgL_test3470z00_10211 = BgL_res2044z00_5703; } }  else 
{ /* Llib/thread.scm 167 */
BgL_test3470z00_10211 = ((bool_t)0)
; } } 
if(BgL_test3470z00_10211)
{ /* Llib/thread.scm 167 */
BgL_auxz00_10210 = 
((BgL_threadz00_bglt)BgL_oz00_3613)
; }  else 
{ 
 obj_t BgL_auxz00_10236;
BgL_auxz00_10236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5881L), BGl_string2576z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3613); 
FAILURE(BgL_auxz00_10236,BFALSE,BFALSE);} } 
return 
BGl_threadzd2specificzd2zz__threadz00(BgL_auxz00_10210);} } 

}



/* &<@anonymous:1395> */
obj_t BGl_z62zc3z04anonymousza31395ze3ze5zz__threadz00(obj_t BgL_envz00_3614, obj_t BgL_oz00_3615, obj_t BgL_vz00_3616)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_auxz00_10241;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3475z00_10242;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_5765;
BgL_classz00_5765 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_3615))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_5767; long BgL_arg1933z00_5768;
BgL_arg1932z00_5767 = 
(BgL_objectz00_bglt)(BgL_oz00_3615); 
BgL_arg1933z00_5768 = 
BGL_CLASS_DEPTH(BgL_classz00_5765); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_5776;
BgL_idxz00_5776 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5767); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_5777;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_5778;
BgL_arg1924z00_5778 = 
(BgL_idxz00_5776+BgL_arg1933z00_5768); 
BgL_arg1923z00_5777 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5778); } 
BgL_test3475z00_10242 = 
(BgL_arg1923z00_5777==BgL_classz00_5765); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_5783;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_5787;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_5789; long BgL_arg1938z00_5790;
BgL_arg1937z00_5789 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_5791;
BgL_arg1939z00_5791 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5767); 
BgL_arg1938z00_5790 = 
(BgL_arg1939z00_5791-OBJECT_TYPE); } 
BgL_oclassz00_5787 = 
VECTOR_REF(BgL_arg1937z00_5789,BgL_arg1938z00_5790); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_5797;
BgL__ortest_1147z00_5797 = 
(BgL_classz00_5765==BgL_oclassz00_5787); 
if(BgL__ortest_1147z00_5797)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5783 = BgL__ortest_1147z00_5797; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_5798;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_5799;
BgL_arg1927z00_5799 = 
(BgL_oclassz00_5787); 
BgL_odepthz00_5798 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5799); } 
if(
(BgL_arg1933z00_5768<BgL_odepthz00_5798))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_5803;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_5804;
BgL_arg1926z00_5804 = 
(BgL_oclassz00_5787); 
BgL_arg1925z00_5803 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5804, BgL_arg1933z00_5768); } 
BgL_res2044z00_5783 = 
(BgL_arg1925z00_5803==BgL_classz00_5765); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5783 = ((bool_t)0); } } } } 
BgL_test3475z00_10242 = BgL_res2044z00_5783; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3475z00_10242 = ((bool_t)0)
; } } 
if(BgL_test3475z00_10242)
{ /* Llib/thread.scm 164 */
BgL_auxz00_10241 = 
((BgL_objectz00_bglt)BgL_oz00_3615)
; }  else 
{ 
 obj_t BgL_auxz00_10267;
BgL_auxz00_10267 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2577z00zz__threadz00, BGl_string2578z00zz__threadz00, BgL_oz00_3615); 
FAILURE(BgL_auxz00_10267,BFALSE,BFALSE);} } 
return 
BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_auxz00_10241, 
(int)(1L), BgL_vz00_3616);} } 

}



/* &<@anonymous:1394> */
obj_t BGl_z62zc3z04anonymousza31394ze3ze5zz__threadz00(obj_t BgL_envz00_3617, obj_t BgL_oz00_3618)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_auxz00_10273;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3480z00_10274;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_5845;
BgL_classz00_5845 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_3618))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_5847; long BgL_arg1933z00_5848;
BgL_arg1932z00_5847 = 
(BgL_objectz00_bglt)(BgL_oz00_3618); 
BgL_arg1933z00_5848 = 
BGL_CLASS_DEPTH(BgL_classz00_5845); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_5856;
BgL_idxz00_5856 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5847); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_5857;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_5858;
BgL_arg1924z00_5858 = 
(BgL_idxz00_5856+BgL_arg1933z00_5848); 
BgL_arg1923z00_5857 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5858); } 
BgL_test3480z00_10274 = 
(BgL_arg1923z00_5857==BgL_classz00_5845); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_5863;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_5867;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_5869; long BgL_arg1938z00_5870;
BgL_arg1937z00_5869 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_5871;
BgL_arg1939z00_5871 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5847); 
BgL_arg1938z00_5870 = 
(BgL_arg1939z00_5871-OBJECT_TYPE); } 
BgL_oclassz00_5867 = 
VECTOR_REF(BgL_arg1937z00_5869,BgL_arg1938z00_5870); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_5877;
BgL__ortest_1147z00_5877 = 
(BgL_classz00_5845==BgL_oclassz00_5867); 
if(BgL__ortest_1147z00_5877)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5863 = BgL__ortest_1147z00_5877; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_5878;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_5879;
BgL_arg1927z00_5879 = 
(BgL_oclassz00_5867); 
BgL_odepthz00_5878 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5879); } 
if(
(BgL_arg1933z00_5848<BgL_odepthz00_5878))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_5883;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_5884;
BgL_arg1926z00_5884 = 
(BgL_oclassz00_5867); 
BgL_arg1925z00_5883 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5884, BgL_arg1933z00_5848); } 
BgL_res2044z00_5863 = 
(BgL_arg1925z00_5883==BgL_classz00_5845); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5863 = ((bool_t)0); } } } } 
BgL_test3480z00_10274 = BgL_res2044z00_5863; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3480z00_10274 = ((bool_t)0)
; } } 
if(BgL_test3480z00_10274)
{ /* Llib/thread.scm 164 */
BgL_auxz00_10273 = 
((BgL_objectz00_bglt)BgL_oz00_3618)
; }  else 
{ 
 obj_t BgL_auxz00_10299;
BgL_auxz00_10299 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2579z00zz__threadz00, BGl_string2578z00zz__threadz00, BgL_oz00_3618); 
FAILURE(BgL_auxz00_10299,BFALSE,BFALSE);} } 
return 
BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_auxz00_10273, 
(int)(1L));} } 

}



/* &<@anonymous:1390> */
obj_t BGl_z62zc3z04anonymousza31390ze3ze5zz__threadz00(obj_t BgL_envz00_3619, obj_t BgL_oz00_3620, obj_t BgL_vz00_3621)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_auxz00_10305;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3485z00_10306;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_5925;
BgL_classz00_5925 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_3620))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_5927; long BgL_arg1933z00_5928;
BgL_arg1932z00_5927 = 
(BgL_objectz00_bglt)(BgL_oz00_3620); 
BgL_arg1933z00_5928 = 
BGL_CLASS_DEPTH(BgL_classz00_5925); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_5936;
BgL_idxz00_5936 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_5927); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_5937;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_5938;
BgL_arg1924z00_5938 = 
(BgL_idxz00_5936+BgL_arg1933z00_5928); 
BgL_arg1923z00_5937 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_5938); } 
BgL_test3485z00_10306 = 
(BgL_arg1923z00_5937==BgL_classz00_5925); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_5943;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_5947;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_5949; long BgL_arg1938z00_5950;
BgL_arg1937z00_5949 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_5951;
BgL_arg1939z00_5951 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_5927); 
BgL_arg1938z00_5950 = 
(BgL_arg1939z00_5951-OBJECT_TYPE); } 
BgL_oclassz00_5947 = 
VECTOR_REF(BgL_arg1937z00_5949,BgL_arg1938z00_5950); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_5957;
BgL__ortest_1147z00_5957 = 
(BgL_classz00_5925==BgL_oclassz00_5947); 
if(BgL__ortest_1147z00_5957)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5943 = BgL__ortest_1147z00_5957; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_5958;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_5959;
BgL_arg1927z00_5959 = 
(BgL_oclassz00_5947); 
BgL_odepthz00_5958 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_5959); } 
if(
(BgL_arg1933z00_5928<BgL_odepthz00_5958))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_5963;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_5964;
BgL_arg1926z00_5964 = 
(BgL_oclassz00_5947); 
BgL_arg1925z00_5963 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_5964, BgL_arg1933z00_5928); } 
BgL_res2044z00_5943 = 
(BgL_arg1925z00_5963==BgL_classz00_5925); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_5943 = ((bool_t)0); } } } } 
BgL_test3485z00_10306 = BgL_res2044z00_5943; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3485z00_10306 = ((bool_t)0)
; } } 
if(BgL_test3485z00_10306)
{ /* Llib/thread.scm 164 */
BgL_auxz00_10305 = 
((BgL_objectz00_bglt)BgL_oz00_3620)
; }  else 
{ 
 obj_t BgL_auxz00_10331;
BgL_auxz00_10331 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2580z00zz__threadz00, BGl_string2578z00zz__threadz00, BgL_oz00_3620); 
FAILURE(BgL_auxz00_10331,BFALSE,BFALSE);} } 
return 
BGl_callzd2virtualzd2setterz00zz__objectz00(BgL_auxz00_10305, 
(int)(0L), BgL_vz00_3621);} } 

}



/* &<@anonymous:1389> */
obj_t BGl_z62zc3z04anonymousza31389ze3ze5zz__threadz00(obj_t BgL_envz00_3622, obj_t BgL_oz00_3623)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_auxz00_10337;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3490z00_10338;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_6005;
BgL_classz00_6005 = BGl_objectz00zz__objectz00; 
if(
BGL_OBJECTP(BgL_oz00_3623))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_6007; long BgL_arg1933z00_6008;
BgL_arg1932z00_6007 = 
(BgL_objectz00_bglt)(BgL_oz00_3623); 
BgL_arg1933z00_6008 = 
BGL_CLASS_DEPTH(BgL_classz00_6005); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_6016;
BgL_idxz00_6016 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6007); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_6017;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_6018;
BgL_arg1924z00_6018 = 
(BgL_idxz00_6016+BgL_arg1933z00_6008); 
BgL_arg1923z00_6017 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6018); } 
BgL_test3490z00_10338 = 
(BgL_arg1923z00_6017==BgL_classz00_6005); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_6023;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_6027;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_6029; long BgL_arg1938z00_6030;
BgL_arg1937z00_6029 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_6031;
BgL_arg1939z00_6031 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6007); 
BgL_arg1938z00_6030 = 
(BgL_arg1939z00_6031-OBJECT_TYPE); } 
BgL_oclassz00_6027 = 
VECTOR_REF(BgL_arg1937z00_6029,BgL_arg1938z00_6030); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_6037;
BgL__ortest_1147z00_6037 = 
(BgL_classz00_6005==BgL_oclassz00_6027); 
if(BgL__ortest_1147z00_6037)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_6023 = BgL__ortest_1147z00_6037; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_6038;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_6039;
BgL_arg1927z00_6039 = 
(BgL_oclassz00_6027); 
BgL_odepthz00_6038 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6039); } 
if(
(BgL_arg1933z00_6008<BgL_odepthz00_6038))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_6043;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_6044;
BgL_arg1926z00_6044 = 
(BgL_oclassz00_6027); 
BgL_arg1925z00_6043 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6044, BgL_arg1933z00_6008); } 
BgL_res2044z00_6023 = 
(BgL_arg1925z00_6043==BgL_classz00_6005); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_6023 = ((bool_t)0); } } } } 
BgL_test3490z00_10338 = BgL_res2044z00_6023; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3490z00_10338 = ((bool_t)0)
; } } 
if(BgL_test3490z00_10338)
{ /* Llib/thread.scm 164 */
BgL_auxz00_10337 = 
((BgL_objectz00_bglt)BgL_oz00_3623)
; }  else 
{ 
 obj_t BgL_auxz00_10363;
BgL_auxz00_10363 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2581z00zz__threadz00, BGl_string2578z00zz__threadz00, BgL_oz00_3623); 
FAILURE(BgL_auxz00_10363,BFALSE,BFALSE);} } 
return 
BGl_callzd2virtualzd2getterz00zz__objectz00(BgL_auxz00_10337, 
(int)(0L));} } 

}



/* &<@anonymous:1383> */
obj_t BGl_z62zc3z04anonymousza31383ze3ze5zz__threadz00(obj_t BgL_envz00_3624)
{
{ /* Llib/thread.scm 164 */
return 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2424z00zz__threadz00);} 

}



/* &lambda1382 */
obj_t BGl_z62lambda1382z62zz__threadz00(obj_t BgL_envz00_3625, obj_t BgL_oz00_3626, obj_t BgL_vz00_3627)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_threadz00_bglt BgL_oz00_6085;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3495z00_10370;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_6045;
BgL_classz00_6045 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3626))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_6047; long BgL_arg1933z00_6048;
BgL_arg1932z00_6047 = 
(BgL_objectz00_bglt)(BgL_oz00_3626); 
BgL_arg1933z00_6048 = 
BGL_CLASS_DEPTH(BgL_classz00_6045); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_6056;
BgL_idxz00_6056 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6047); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_6057;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_6058;
BgL_arg1924z00_6058 = 
(BgL_idxz00_6056+BgL_arg1933z00_6048); 
BgL_arg1923z00_6057 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6058); } 
BgL_test3495z00_10370 = 
(BgL_arg1923z00_6057==BgL_classz00_6045); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_6063;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_6067;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_6069; long BgL_arg1938z00_6070;
BgL_arg1937z00_6069 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_6071;
BgL_arg1939z00_6071 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6047); 
BgL_arg1938z00_6070 = 
(BgL_arg1939z00_6071-OBJECT_TYPE); } 
BgL_oclassz00_6067 = 
VECTOR_REF(BgL_arg1937z00_6069,BgL_arg1938z00_6070); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_6077;
BgL__ortest_1147z00_6077 = 
(BgL_classz00_6045==BgL_oclassz00_6067); 
if(BgL__ortest_1147z00_6077)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_6063 = BgL__ortest_1147z00_6077; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_6078;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_6079;
BgL_arg1927z00_6079 = 
(BgL_oclassz00_6067); 
BgL_odepthz00_6078 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6079); } 
if(
(BgL_arg1933z00_6048<BgL_odepthz00_6078))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_6083;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_6084;
BgL_arg1926z00_6084 = 
(BgL_oclassz00_6067); 
BgL_arg1925z00_6083 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6084, BgL_arg1933z00_6048); } 
BgL_res2044z00_6063 = 
(BgL_arg1925z00_6083==BgL_classz00_6045); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_6063 = ((bool_t)0); } } } } 
BgL_test3495z00_10370 = BgL_res2044z00_6063; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3495z00_10370 = ((bool_t)0)
; } } 
if(BgL_test3495z00_10370)
{ /* Llib/thread.scm 164 */
BgL_oz00_6085 = 
((BgL_threadz00_bglt)BgL_oz00_3626); }  else 
{ 
 obj_t BgL_auxz00_10395;
BgL_auxz00_10395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2582z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3626); 
FAILURE(BgL_auxz00_10395,BFALSE,BFALSE);} } 
return 
((((BgL_threadz00_bglt)COBJECT(BgL_oz00_6085))->BgL_namez00)=((obj_t)BgL_vz00_3627),BUNSPEC);} } 

}



/* &lambda1381 */
obj_t BGl_z62lambda1381z62zz__threadz00(obj_t BgL_envz00_3628, obj_t BgL_oz00_3629)
{
{ /* Llib/thread.scm 164 */
{ /* Llib/thread.scm 164 */
 BgL_threadz00_bglt BgL_oz00_6126;
{ /* Llib/thread.scm 164 */
 bool_t BgL_test3500z00_10400;
{ /* Llib/thread.scm 164 */
 obj_t BgL_classz00_6086;
BgL_classz00_6086 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3629))
{ /* Llib/thread.scm 164 */
 BgL_objectz00_bglt BgL_arg1932z00_6088; long BgL_arg1933z00_6089;
BgL_arg1932z00_6088 = 
(BgL_objectz00_bglt)(BgL_oz00_3629); 
BgL_arg1933z00_6089 = 
BGL_CLASS_DEPTH(BgL_classz00_6086); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 164 */
 long BgL_idxz00_6097;
BgL_idxz00_6097 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6088); 
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1923z00_6098;
{ /* Llib/thread.scm 164 */
 long BgL_arg1924z00_6099;
BgL_arg1924z00_6099 = 
(BgL_idxz00_6097+BgL_arg1933z00_6089); 
BgL_arg1923z00_6098 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6099); } 
BgL_test3500z00_10400 = 
(BgL_arg1923z00_6098==BgL_classz00_6086); } }  else 
{ /* Llib/thread.scm 164 */
 bool_t BgL_res2044z00_6104;
{ /* Llib/thread.scm 164 */
 obj_t BgL_oclassz00_6108;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1937z00_6110; long BgL_arg1938z00_6111;
BgL_arg1937z00_6110 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 164 */
 long BgL_arg1939z00_6112;
BgL_arg1939z00_6112 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6088); 
BgL_arg1938z00_6111 = 
(BgL_arg1939z00_6112-OBJECT_TYPE); } 
BgL_oclassz00_6108 = 
VECTOR_REF(BgL_arg1937z00_6110,BgL_arg1938z00_6111); } 
{ /* Llib/thread.scm 164 */
 bool_t BgL__ortest_1147z00_6118;
BgL__ortest_1147z00_6118 = 
(BgL_classz00_6086==BgL_oclassz00_6108); 
if(BgL__ortest_1147z00_6118)
{ /* Llib/thread.scm 164 */
BgL_res2044z00_6104 = BgL__ortest_1147z00_6118; }  else 
{ /* Llib/thread.scm 164 */
 long BgL_odepthz00_6119;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1927z00_6120;
BgL_arg1927z00_6120 = 
(BgL_oclassz00_6108); 
BgL_odepthz00_6119 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6120); } 
if(
(BgL_arg1933z00_6089<BgL_odepthz00_6119))
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1925z00_6124;
{ /* Llib/thread.scm 164 */
 obj_t BgL_arg1926z00_6125;
BgL_arg1926z00_6125 = 
(BgL_oclassz00_6108); 
BgL_arg1925z00_6124 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6125, BgL_arg1933z00_6089); } 
BgL_res2044z00_6104 = 
(BgL_arg1925z00_6124==BgL_classz00_6086); }  else 
{ /* Llib/thread.scm 164 */
BgL_res2044z00_6104 = ((bool_t)0); } } } } 
BgL_test3500z00_10400 = BgL_res2044z00_6104; } }  else 
{ /* Llib/thread.scm 164 */
BgL_test3500z00_10400 = ((bool_t)0)
; } } 
if(BgL_test3500z00_10400)
{ /* Llib/thread.scm 164 */
BgL_oz00_6126 = 
((BgL_threadz00_bglt)BgL_oz00_3629); }  else 
{ 
 obj_t BgL_auxz00_10425;
BgL_auxz00_10425 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5779L), BGl_string2583z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3629); 
FAILURE(BgL_auxz00_10425,BFALSE,BFALSE);} } 
return 
(((BgL_threadz00_bglt)COBJECT(BgL_oz00_6126))->BgL_namez00);} } 

}



/* &<@anonymous:1364> */
obj_t BGl_z62zc3z04anonymousza31364ze3ze5zz__threadz00(obj_t BgL_envz00_3630, obj_t BgL_new1039z00_3631)
{
{ /* Llib/thread.scm 161 */
{ 
 BgL_threadzd2backendzd2_bglt BgL_auxz00_10430;
{ /* Llib/thread.scm 161 */
 BgL_threadzd2backendzd2_bglt BgL_new1039z00_6167;
{ /* Llib/thread.scm 161 */
 bool_t BgL_test3505z00_10431;
{ /* Llib/thread.scm 161 */
 obj_t BgL_classz00_6127;
BgL_classz00_6127 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_new1039z00_3631))
{ /* Llib/thread.scm 161 */
 BgL_objectz00_bglt BgL_arg1932z00_6129; long BgL_arg1933z00_6130;
BgL_arg1932z00_6129 = 
(BgL_objectz00_bglt)(BgL_new1039z00_3631); 
BgL_arg1933z00_6130 = 
BGL_CLASS_DEPTH(BgL_classz00_6127); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 161 */
 long BgL_idxz00_6138;
BgL_idxz00_6138 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6129); 
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1923z00_6139;
{ /* Llib/thread.scm 161 */
 long BgL_arg1924z00_6140;
BgL_arg1924z00_6140 = 
(BgL_idxz00_6138+BgL_arg1933z00_6130); 
BgL_arg1923z00_6139 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6140); } 
BgL_test3505z00_10431 = 
(BgL_arg1923z00_6139==BgL_classz00_6127); } }  else 
{ /* Llib/thread.scm 161 */
 bool_t BgL_res2044z00_6145;
{ /* Llib/thread.scm 161 */
 obj_t BgL_oclassz00_6149;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1937z00_6151; long BgL_arg1938z00_6152;
BgL_arg1937z00_6151 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 161 */
 long BgL_arg1939z00_6153;
BgL_arg1939z00_6153 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6129); 
BgL_arg1938z00_6152 = 
(BgL_arg1939z00_6153-OBJECT_TYPE); } 
BgL_oclassz00_6149 = 
VECTOR_REF(BgL_arg1937z00_6151,BgL_arg1938z00_6152); } 
{ /* Llib/thread.scm 161 */
 bool_t BgL__ortest_1147z00_6159;
BgL__ortest_1147z00_6159 = 
(BgL_classz00_6127==BgL_oclassz00_6149); 
if(BgL__ortest_1147z00_6159)
{ /* Llib/thread.scm 161 */
BgL_res2044z00_6145 = BgL__ortest_1147z00_6159; }  else 
{ /* Llib/thread.scm 161 */
 long BgL_odepthz00_6160;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1927z00_6161;
BgL_arg1927z00_6161 = 
(BgL_oclassz00_6149); 
BgL_odepthz00_6160 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6161); } 
if(
(BgL_arg1933z00_6130<BgL_odepthz00_6160))
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1925z00_6165;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1926z00_6166;
BgL_arg1926z00_6166 = 
(BgL_oclassz00_6149); 
BgL_arg1925z00_6165 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6166, BgL_arg1933z00_6130); } 
BgL_res2044z00_6145 = 
(BgL_arg1925z00_6165==BgL_classz00_6127); }  else 
{ /* Llib/thread.scm 161 */
BgL_res2044z00_6145 = ((bool_t)0); } } } } 
BgL_test3505z00_10431 = BgL_res2044z00_6145; } }  else 
{ /* Llib/thread.scm 161 */
BgL_test3505z00_10431 = ((bool_t)0)
; } } 
if(BgL_test3505z00_10431)
{ /* Llib/thread.scm 161 */
BgL_new1039z00_6167 = 
((BgL_threadzd2backendzd2_bglt)BgL_new1039z00_3631); }  else 
{ 
 obj_t BgL_auxz00_10456;
BgL_auxz00_10456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5716L), BGl_string2584z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_new1039z00_3631); 
FAILURE(BgL_auxz00_10456,BFALSE,BFALSE);} } 
((((BgL_threadzd2backendzd2_bglt)COBJECT(BgL_new1039z00_6167))->BgL_namez00)=((obj_t)BGl_string2547z00zz__threadz00),BUNSPEC); 
BgL_auxz00_10430 = BgL_new1039z00_6167; } 
return 
((obj_t)BgL_auxz00_10430);} } 

}



/* &lambda1362 */
BgL_threadzd2backendzd2_bglt BGl_z62lambda1362z62zz__threadz00(obj_t BgL_envz00_3632)
{
{ /* Llib/thread.scm 161 */
{ /* Llib/thread.scm 161 */
 BgL_threadzd2backendzd2_bglt BgL_new1038z00_6168;
BgL_new1038z00_6168 = 
((BgL_threadzd2backendzd2_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_threadzd2backendzd2_bgl) ))); 
{ /* Llib/thread.scm 161 */
 long BgL_arg1363z00_6169;
BgL_arg1363z00_6169 = 
BGL_CLASS_NUM(BGl_threadzd2backendzd2zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1038z00_6168), BgL_arg1363z00_6169); } 
return BgL_new1038z00_6168;} } 

}



/* &lambda1360 */
BgL_threadzd2backendzd2_bglt BGl_z62lambda1360z62zz__threadz00(obj_t BgL_envz00_3633, obj_t BgL_name1037z00_3634)
{
{ /* Llib/thread.scm 161 */
{ /* Llib/thread.scm 161 */
 obj_t BgL_name1037z00_6170;
if(
STRINGP(BgL_name1037z00_3634))
{ /* Llib/thread.scm 161 */
BgL_name1037z00_6170 = BgL_name1037z00_3634; }  else 
{ 
 obj_t BgL_auxz00_10468;
BgL_auxz00_10468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5716L), BGl_string2585z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_name1037z00_3634); 
FAILURE(BgL_auxz00_10468,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 161 */
 BgL_threadzd2backendzd2_bglt BgL_new1113z00_6171;
{ /* Llib/thread.scm 161 */
 BgL_threadzd2backendzd2_bglt BgL_new1111z00_6172;
BgL_new1111z00_6172 = 
((BgL_threadzd2backendzd2_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_threadzd2backendzd2_bgl) ))); 
{ /* Llib/thread.scm 161 */
 long BgL_arg1361z00_6173;
BgL_arg1361z00_6173 = 
BGL_CLASS_NUM(BGl_threadzd2backendzd2zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1111z00_6172), BgL_arg1361z00_6173); } 
BgL_new1113z00_6171 = BgL_new1111z00_6172; } 
((((BgL_threadzd2backendzd2_bglt)COBJECT(BgL_new1113z00_6171))->BgL_namez00)=((obj_t)BgL_name1037z00_6170),BUNSPEC); 
return BgL_new1113z00_6171;} } } 

}



/* &lambda1369 */
obj_t BGl_z62lambda1369z62zz__threadz00(obj_t BgL_envz00_3635, obj_t BgL_oz00_3636, obj_t BgL_vz00_3637)
{
{ /* Llib/thread.scm 161 */
{ /* Llib/thread.scm 161 */
 BgL_threadzd2backendzd2_bglt BgL_oz00_6214; obj_t BgL_vz00_6215;
{ /* Llib/thread.scm 161 */
 bool_t BgL_test3511z00_10477;
{ /* Llib/thread.scm 161 */
 obj_t BgL_classz00_6174;
BgL_classz00_6174 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3636))
{ /* Llib/thread.scm 161 */
 BgL_objectz00_bglt BgL_arg1932z00_6176; long BgL_arg1933z00_6177;
BgL_arg1932z00_6176 = 
(BgL_objectz00_bglt)(BgL_oz00_3636); 
BgL_arg1933z00_6177 = 
BGL_CLASS_DEPTH(BgL_classz00_6174); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 161 */
 long BgL_idxz00_6185;
BgL_idxz00_6185 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6176); 
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1923z00_6186;
{ /* Llib/thread.scm 161 */
 long BgL_arg1924z00_6187;
BgL_arg1924z00_6187 = 
(BgL_idxz00_6185+BgL_arg1933z00_6177); 
BgL_arg1923z00_6186 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6187); } 
BgL_test3511z00_10477 = 
(BgL_arg1923z00_6186==BgL_classz00_6174); } }  else 
{ /* Llib/thread.scm 161 */
 bool_t BgL_res2044z00_6192;
{ /* Llib/thread.scm 161 */
 obj_t BgL_oclassz00_6196;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1937z00_6198; long BgL_arg1938z00_6199;
BgL_arg1937z00_6198 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 161 */
 long BgL_arg1939z00_6200;
BgL_arg1939z00_6200 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6176); 
BgL_arg1938z00_6199 = 
(BgL_arg1939z00_6200-OBJECT_TYPE); } 
BgL_oclassz00_6196 = 
VECTOR_REF(BgL_arg1937z00_6198,BgL_arg1938z00_6199); } 
{ /* Llib/thread.scm 161 */
 bool_t BgL__ortest_1147z00_6206;
BgL__ortest_1147z00_6206 = 
(BgL_classz00_6174==BgL_oclassz00_6196); 
if(BgL__ortest_1147z00_6206)
{ /* Llib/thread.scm 161 */
BgL_res2044z00_6192 = BgL__ortest_1147z00_6206; }  else 
{ /* Llib/thread.scm 161 */
 long BgL_odepthz00_6207;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1927z00_6208;
BgL_arg1927z00_6208 = 
(BgL_oclassz00_6196); 
BgL_odepthz00_6207 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6208); } 
if(
(BgL_arg1933z00_6177<BgL_odepthz00_6207))
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1925z00_6212;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1926z00_6213;
BgL_arg1926z00_6213 = 
(BgL_oclassz00_6196); 
BgL_arg1925z00_6212 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6213, BgL_arg1933z00_6177); } 
BgL_res2044z00_6192 = 
(BgL_arg1925z00_6212==BgL_classz00_6174); }  else 
{ /* Llib/thread.scm 161 */
BgL_res2044z00_6192 = ((bool_t)0); } } } } 
BgL_test3511z00_10477 = BgL_res2044z00_6192; } }  else 
{ /* Llib/thread.scm 161 */
BgL_test3511z00_10477 = ((bool_t)0)
; } } 
if(BgL_test3511z00_10477)
{ /* Llib/thread.scm 161 */
BgL_oz00_6214 = 
((BgL_threadzd2backendzd2_bglt)BgL_oz00_3636); }  else 
{ 
 obj_t BgL_auxz00_10502;
BgL_auxz00_10502 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5716L), BGl_string2586z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_oz00_3636); 
FAILURE(BgL_auxz00_10502,BFALSE,BFALSE);} } 
if(
STRINGP(BgL_vz00_3637))
{ /* Llib/thread.scm 161 */
BgL_vz00_6215 = BgL_vz00_3637; }  else 
{ 
 obj_t BgL_auxz00_10508;
BgL_auxz00_10508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5716L), BGl_string2586z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_vz00_3637); 
FAILURE(BgL_auxz00_10508,BFALSE,BFALSE);} 
return 
((((BgL_threadzd2backendzd2_bglt)COBJECT(BgL_oz00_6214))->BgL_namez00)=((obj_t)BgL_vz00_6215),BUNSPEC);} } 

}



/* &lambda1368 */
obj_t BGl_z62lambda1368z62zz__threadz00(obj_t BgL_envz00_3638, obj_t BgL_oz00_3639)
{
{ /* Llib/thread.scm 161 */
{ /* Llib/thread.scm 161 */
 BgL_threadzd2backendzd2_bglt BgL_oz00_6256;
{ /* Llib/thread.scm 161 */
 bool_t BgL_test3517z00_10513;
{ /* Llib/thread.scm 161 */
 obj_t BgL_classz00_6216;
BgL_classz00_6216 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3639))
{ /* Llib/thread.scm 161 */
 BgL_objectz00_bglt BgL_arg1932z00_6218; long BgL_arg1933z00_6219;
BgL_arg1932z00_6218 = 
(BgL_objectz00_bglt)(BgL_oz00_3639); 
BgL_arg1933z00_6219 = 
BGL_CLASS_DEPTH(BgL_classz00_6216); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 161 */
 long BgL_idxz00_6227;
BgL_idxz00_6227 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6218); 
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1923z00_6228;
{ /* Llib/thread.scm 161 */
 long BgL_arg1924z00_6229;
BgL_arg1924z00_6229 = 
(BgL_idxz00_6227+BgL_arg1933z00_6219); 
BgL_arg1923z00_6228 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6229); } 
BgL_test3517z00_10513 = 
(BgL_arg1923z00_6228==BgL_classz00_6216); } }  else 
{ /* Llib/thread.scm 161 */
 bool_t BgL_res2044z00_6234;
{ /* Llib/thread.scm 161 */
 obj_t BgL_oclassz00_6238;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1937z00_6240; long BgL_arg1938z00_6241;
BgL_arg1937z00_6240 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 161 */
 long BgL_arg1939z00_6242;
BgL_arg1939z00_6242 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6218); 
BgL_arg1938z00_6241 = 
(BgL_arg1939z00_6242-OBJECT_TYPE); } 
BgL_oclassz00_6238 = 
VECTOR_REF(BgL_arg1937z00_6240,BgL_arg1938z00_6241); } 
{ /* Llib/thread.scm 161 */
 bool_t BgL__ortest_1147z00_6248;
BgL__ortest_1147z00_6248 = 
(BgL_classz00_6216==BgL_oclassz00_6238); 
if(BgL__ortest_1147z00_6248)
{ /* Llib/thread.scm 161 */
BgL_res2044z00_6234 = BgL__ortest_1147z00_6248; }  else 
{ /* Llib/thread.scm 161 */
 long BgL_odepthz00_6249;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1927z00_6250;
BgL_arg1927z00_6250 = 
(BgL_oclassz00_6238); 
BgL_odepthz00_6249 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6250); } 
if(
(BgL_arg1933z00_6219<BgL_odepthz00_6249))
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1925z00_6254;
{ /* Llib/thread.scm 161 */
 obj_t BgL_arg1926z00_6255;
BgL_arg1926z00_6255 = 
(BgL_oclassz00_6238); 
BgL_arg1925z00_6254 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6255, BgL_arg1933z00_6219); } 
BgL_res2044z00_6234 = 
(BgL_arg1925z00_6254==BgL_classz00_6216); }  else 
{ /* Llib/thread.scm 161 */
BgL_res2044z00_6234 = ((bool_t)0); } } } } 
BgL_test3517z00_10513 = BgL_res2044z00_6234; } }  else 
{ /* Llib/thread.scm 161 */
BgL_test3517z00_10513 = ((bool_t)0)
; } } 
if(BgL_test3517z00_10513)
{ /* Llib/thread.scm 161 */
BgL_oz00_6256 = 
((BgL_threadzd2backendzd2_bglt)BgL_oz00_3639); }  else 
{ 
 obj_t BgL_auxz00_10538;
BgL_auxz00_10538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(5716L), BGl_string2587z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_oz00_3639); 
FAILURE(BgL_auxz00_10538,BFALSE,BFALSE);} } 
return 
(((BgL_threadzd2backendzd2_bglt)COBJECT(BgL_oz00_6256))->BgL_namez00);} } 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
BGl_registerzd2genericz12zc0zz__objectz00(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00, BGl_proc2588z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00, BGl_string2589z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00, BGl_proc2590z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00, BGl_string2591z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_tbzd2mutexzd2initializa7ez12zd2envz67zz__threadz00, BGl_proc2592z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00, BGl_string2593z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_tbzd2condvarzd2initializa7ez12zd2envz67zz__threadz00, BGl_proc2594z00zz__threadz00, BGl_threadzd2backendzd2zz__threadz00, BGl_string2595z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00, BGl_proc2596z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2597z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2startz12zd2envz12zz__threadz00, BGl_proc2598z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2599z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00, BGl_proc2600z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2601z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2joinz12zd2envz12zz__threadz00, BGl_proc2602z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2603z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2terminatez12zd2envz12zz__threadz00, BGl_proc2604z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2605z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2killz12zd2envz12zz__threadz00, BGl_proc2606z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2607z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2specificzd2envz00zz__threadz00, BGl_proc2608z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2609z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00, BGl_proc2610z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2611z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2cleanupzd2envz00zz__threadz00, BGl_proc2612z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2613z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00, BGl_proc2614z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2615z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2namezd2envz00zz__threadz00, BGl_proc2616z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2617z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00, BGl_proc2618z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2619z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_z52userzd2currentzd2threadzd2envz80zz__threadz00, BGl_proc2620z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2621z00zz__threadz00); 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_z52userzd2threadzd2sleepz12zd2envz92zz__threadz00, BGl_proc2622z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2623z00zz__threadz00); 
return 
BGl_registerzd2genericz12zc0zz__objectz00(BGl_z52userzd2threadzd2yieldz12zd2envz92zz__threadz00, BGl_proc2624z00zz__threadz00, BGl_threadz00zz__threadz00, BGl_string2625z00zz__threadz00);} 

}



/* &%user-thread-yield!1231 */
obj_t BGl_z62z52userzd2threadzd2yieldz121231z22zz__threadz00(obj_t BgL_envz00_3661, obj_t BgL_oz00_3662)
{
{ /* Llib/thread.scm 500 */
{ /* Llib/thread.scm 500 */
 BgL_threadz00_bglt BgL_oz00_6297;
{ /* Llib/thread.scm 500 */
 bool_t BgL_test3522z00_10562;
{ /* Llib/thread.scm 500 */
 obj_t BgL_classz00_6257;
BgL_classz00_6257 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3662))
{ /* Llib/thread.scm 500 */
 BgL_objectz00_bglt BgL_arg1932z00_6259; long BgL_arg1933z00_6260;
BgL_arg1932z00_6259 = 
(BgL_objectz00_bglt)(BgL_oz00_3662); 
BgL_arg1933z00_6260 = 
BGL_CLASS_DEPTH(BgL_classz00_6257); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 500 */
 long BgL_idxz00_6268;
BgL_idxz00_6268 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6259); 
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1923z00_6269;
{ /* Llib/thread.scm 500 */
 long BgL_arg1924z00_6270;
BgL_arg1924z00_6270 = 
(BgL_idxz00_6268+BgL_arg1933z00_6260); 
BgL_arg1923z00_6269 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6270); } 
BgL_test3522z00_10562 = 
(BgL_arg1923z00_6269==BgL_classz00_6257); } }  else 
{ /* Llib/thread.scm 500 */
 bool_t BgL_res2044z00_6275;
{ /* Llib/thread.scm 500 */
 obj_t BgL_oclassz00_6279;
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1937z00_6281; long BgL_arg1938z00_6282;
BgL_arg1937z00_6281 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 500 */
 long BgL_arg1939z00_6283;
BgL_arg1939z00_6283 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6259); 
BgL_arg1938z00_6282 = 
(BgL_arg1939z00_6283-OBJECT_TYPE); } 
BgL_oclassz00_6279 = 
VECTOR_REF(BgL_arg1937z00_6281,BgL_arg1938z00_6282); } 
{ /* Llib/thread.scm 500 */
 bool_t BgL__ortest_1147z00_6289;
BgL__ortest_1147z00_6289 = 
(BgL_classz00_6257==BgL_oclassz00_6279); 
if(BgL__ortest_1147z00_6289)
{ /* Llib/thread.scm 500 */
BgL_res2044z00_6275 = BgL__ortest_1147z00_6289; }  else 
{ /* Llib/thread.scm 500 */
 long BgL_odepthz00_6290;
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1927z00_6291;
BgL_arg1927z00_6291 = 
(BgL_oclassz00_6279); 
BgL_odepthz00_6290 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6291); } 
if(
(BgL_arg1933z00_6260<BgL_odepthz00_6290))
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1925z00_6295;
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1926z00_6296;
BgL_arg1926z00_6296 = 
(BgL_oclassz00_6279); 
BgL_arg1925z00_6295 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6296, BgL_arg1933z00_6260); } 
BgL_res2044z00_6275 = 
(BgL_arg1925z00_6295==BgL_classz00_6257); }  else 
{ /* Llib/thread.scm 500 */
BgL_res2044z00_6275 = ((bool_t)0); } } } } 
BgL_test3522z00_10562 = BgL_res2044z00_6275; } }  else 
{ /* Llib/thread.scm 500 */
BgL_test3522z00_10562 = ((bool_t)0)
; } } 
if(BgL_test3522z00_10562)
{ /* Llib/thread.scm 500 */
BgL_oz00_6297 = 
((BgL_threadz00_bglt)BgL_oz00_3662); }  else 
{ 
 obj_t BgL_auxz00_10587;
BgL_auxz00_10587 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(21576L), BGl_string2626z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3662); 
FAILURE(BgL_auxz00_10587,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* &%user-thread-sleep!1229 */
obj_t BGl_z62z52userzd2threadzd2sleepz121229z22zz__threadz00(obj_t BgL_envz00_3663, obj_t BgL_oz00_3664, obj_t BgL_dz00_3665)
{
{ /* Llib/thread.scm 473 */
{ /* Llib/thread.scm 475 */
 BgL_threadz00_bglt BgL_oz00_6338;
{ /* Llib/thread.scm 475 */
 bool_t BgL_test3527z00_10591;
{ /* Llib/thread.scm 475 */
 obj_t BgL_classz00_6298;
BgL_classz00_6298 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3664))
{ /* Llib/thread.scm 475 */
 BgL_objectz00_bglt BgL_arg1932z00_6300; long BgL_arg1933z00_6301;
BgL_arg1932z00_6300 = 
(BgL_objectz00_bglt)(BgL_oz00_3664); 
BgL_arg1933z00_6301 = 
BGL_CLASS_DEPTH(BgL_classz00_6298); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 475 */
 long BgL_idxz00_6309;
BgL_idxz00_6309 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6300); 
{ /* Llib/thread.scm 475 */
 obj_t BgL_arg1923z00_6310;
{ /* Llib/thread.scm 475 */
 long BgL_arg1924z00_6311;
BgL_arg1924z00_6311 = 
(BgL_idxz00_6309+BgL_arg1933z00_6301); 
BgL_arg1923z00_6310 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6311); } 
BgL_test3527z00_10591 = 
(BgL_arg1923z00_6310==BgL_classz00_6298); } }  else 
{ /* Llib/thread.scm 475 */
 bool_t BgL_res2044z00_6316;
{ /* Llib/thread.scm 475 */
 obj_t BgL_oclassz00_6320;
{ /* Llib/thread.scm 475 */
 obj_t BgL_arg1937z00_6322; long BgL_arg1938z00_6323;
BgL_arg1937z00_6322 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 475 */
 long BgL_arg1939z00_6324;
BgL_arg1939z00_6324 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6300); 
BgL_arg1938z00_6323 = 
(BgL_arg1939z00_6324-OBJECT_TYPE); } 
BgL_oclassz00_6320 = 
VECTOR_REF(BgL_arg1937z00_6322,BgL_arg1938z00_6323); } 
{ /* Llib/thread.scm 475 */
 bool_t BgL__ortest_1147z00_6330;
BgL__ortest_1147z00_6330 = 
(BgL_classz00_6298==BgL_oclassz00_6320); 
if(BgL__ortest_1147z00_6330)
{ /* Llib/thread.scm 475 */
BgL_res2044z00_6316 = BgL__ortest_1147z00_6330; }  else 
{ /* Llib/thread.scm 475 */
 long BgL_odepthz00_6331;
{ /* Llib/thread.scm 475 */
 obj_t BgL_arg1927z00_6332;
BgL_arg1927z00_6332 = 
(BgL_oclassz00_6320); 
BgL_odepthz00_6331 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6332); } 
if(
(BgL_arg1933z00_6301<BgL_odepthz00_6331))
{ /* Llib/thread.scm 475 */
 obj_t BgL_arg1925z00_6336;
{ /* Llib/thread.scm 475 */
 obj_t BgL_arg1926z00_6337;
BgL_arg1926z00_6337 = 
(BgL_oclassz00_6320); 
BgL_arg1925z00_6336 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6337, BgL_arg1933z00_6301); } 
BgL_res2044z00_6316 = 
(BgL_arg1925z00_6336==BgL_classz00_6298); }  else 
{ /* Llib/thread.scm 475 */
BgL_res2044z00_6316 = ((bool_t)0); } } } } 
BgL_test3527z00_10591 = BgL_res2044z00_6316; } }  else 
{ /* Llib/thread.scm 475 */
BgL_test3527z00_10591 = ((bool_t)0)
; } } 
if(BgL_test3527z00_10591)
{ /* Llib/thread.scm 475 */
BgL_oz00_6338 = 
((BgL_threadz00_bglt)BgL_oz00_3664); }  else 
{ 
 obj_t BgL_auxz00_10616;
BgL_auxz00_10616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20545L), BGl_string2630z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3664); 
FAILURE(BgL_auxz00_10616,BFALSE,BFALSE);} } 
if(
BGL_DATEP(BgL_dz00_3665))
{ /* Llib/thread.scm 476 */
 long BgL_cdtz00_6339;
BgL_cdtz00_6339 = 
bgl_date_to_seconds(
bgl_nanoseconds_to_date(
bgl_current_nanoseconds())); 
{ /* Llib/thread.scm 476 */
 long BgL_dtz00_6340;
BgL_dtz00_6340 = 
bgl_date_to_seconds(BgL_dz00_3665); 
{ /* Llib/thread.scm 477 */
 obj_t BgL_az00_6341;
{ /* Llib/thread.scm 478 */
 long BgL_a1151z00_6342;
{ /* Llib/thread.scm 478 */
 long BgL_res1946z00_6343;
{ /* Llib/thread.scm 478 */
 long BgL_tmpz00_10626;
BgL_tmpz00_10626 = 
(BgL_dtz00_6340-BgL_cdtz00_6339); 
BgL_res1946z00_6343 = 
(long)(BgL_tmpz00_10626); } 
BgL_a1151z00_6342 = BgL_res1946z00_6343; } 
{ /* Llib/thread.scm 478 */

BgL_az00_6341 = 
BGl_2za2za2zz__r4_numbers_6_5z00(
make_belong(BgL_a1151z00_6342), 
BINT(1000000L)); } } 
{ /* Llib/thread.scm 478 */

{ /* Llib/thread.scm 479 */
 bool_t BgL_test3533z00_10632;
{ /* Llib/thread.scm 479 */
 long BgL_n1z00_6344;
{ /* Llib/thread.scm 479 */
 obj_t BgL_tmpz00_10633;
if(
ELONGP(BgL_az00_6341))
{ /* Llib/thread.scm 479 */
BgL_tmpz00_10633 = BgL_az00_6341
; }  else 
{ 
 obj_t BgL_auxz00_10636;
BgL_auxz00_10636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20694L), BGl_string2623z00zz__threadz00, BGl_string2627z00zz__threadz00, BgL_az00_6341); 
FAILURE(BgL_auxz00_10636,BFALSE,BFALSE);} 
BgL_n1z00_6344 = 
BELONG_TO_LONG(BgL_tmpz00_10633); } 
BgL_test3533z00_10632 = 
(BgL_n1z00_6344>((long)0)); } 
if(BgL_test3533z00_10632)
{ /* Llib/thread.scm 479 */
 long BgL_arg1501z00_6345;
{ /* Llib/thread.scm 479 */
 long BgL_xz00_6346;
{ /* Llib/thread.scm 479 */
 obj_t BgL_tmpz00_10642;
if(
ELONGP(BgL_az00_6341))
{ /* Llib/thread.scm 479 */
BgL_tmpz00_10642 = BgL_az00_6341
; }  else 
{ 
 obj_t BgL_auxz00_10645;
BgL_auxz00_10645 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20723L), BGl_string2623z00zz__threadz00, BGl_string2627z00zz__threadz00, BgL_az00_6341); 
FAILURE(BgL_auxz00_10645,BFALSE,BFALSE);} 
BgL_xz00_6346 = 
BELONG_TO_LONG(BgL_tmpz00_10642); } 
BgL_arg1501z00_6345 = 
(long)(BgL_xz00_6346); } 
bgl_sleep(BgL_arg1501z00_6345); BUNSPEC; 
return 
BINT(BgL_arg1501z00_6345);}  else 
{ /* Llib/thread.scm 479 */
return BFALSE;} } } } } }  else 
{ /* Llib/thread.scm 475 */
if(
INTEGERP(BgL_dz00_3665))
{ /* Llib/thread.scm 481 */
 obj_t BgL_arg1505z00_6347;
BgL_arg1505z00_6347 = 
BINT(
(
(long)CINT(BgL_dz00_3665)*1000L)); 
{ /* Llib/thread.scm 481 */
 long BgL_msz00_6348;
{ /* Llib/thread.scm 481 */
 obj_t BgL_tmpz00_10658;
if(
INTEGERP(BgL_arg1505z00_6347))
{ /* Llib/thread.scm 481 */
BgL_tmpz00_10658 = BgL_arg1505z00_6347
; }  else 
{ 
 obj_t BgL_auxz00_10661;
BgL_auxz00_10661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20772L), BGl_string2623z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_arg1505z00_6347); 
FAILURE(BgL_auxz00_10661,BFALSE,BFALSE);} 
BgL_msz00_6348 = 
(long)CINT(BgL_tmpz00_10658); } 
bgl_sleep(BgL_msz00_6348); BUNSPEC; 
return 
BINT(BgL_msz00_6348);} }  else 
{ /* Llib/thread.scm 480 */
if(
ELONGP(BgL_dz00_3665))
{ /* Llib/thread.scm 483 */
 long BgL_arg1507z00_6349;
{ /* Llib/thread.scm 483 */
 long BgL_a1153z00_6350;
{ /* Llib/thread.scm 483 */
 long BgL_xz00_6351;
BgL_xz00_6351 = 
BELONG_TO_LONG(BgL_dz00_3665); 
BgL_a1153z00_6350 = 
(long)(BgL_xz00_6351); } 
{ /* Llib/thread.scm 483 */

BgL_arg1507z00_6349 = 
(BgL_a1153z00_6350*1000L); } } 
bgl_sleep(BgL_arg1507z00_6349); BUNSPEC; 
return 
BINT(BgL_arg1507z00_6349);}  else 
{ /* Llib/thread.scm 482 */
if(
LLONGP(BgL_dz00_3665))
{ /* Llib/thread.scm 485 */
 long BgL_arg1510z00_6352;
{ /* Llib/thread.scm 485 */
 long BgL_a1155z00_6353;
{ /* Llib/thread.scm 485 */
 BGL_LONGLONG_T BgL_tmpz00_10677;
BgL_tmpz00_10677 = 
BLLONG_TO_LLONG(BgL_dz00_3665); 
BgL_a1155z00_6353 = 
LLONG_TO_LONG(BgL_tmpz00_10677); } 
{ /* Llib/thread.scm 485 */

BgL_arg1510z00_6352 = 
(BgL_a1155z00_6353*1000L); } } 
bgl_sleep(BgL_arg1510z00_6352); BUNSPEC; 
return 
BINT(BgL_arg1510z00_6352);}  else 
{ /* Llib/thread.scm 486 */
 bool_t BgL_test3540z00_10683;
if(
INTEGERP(BgL_dz00_3665))
{ /* Llib/thread.scm 486 */
BgL_test3540z00_10683 = ((bool_t)1)
; }  else 
{ /* Llib/thread.scm 486 */
BgL_test3540z00_10683 = 
REALP(BgL_dz00_3665)
; } 
if(BgL_test3540z00_10683)
{ /* Llib/thread.scm 487 */
 long BgL_arg1513z00_6354;
{ /* Llib/thread.scm 487 */
 double BgL_arg1514z00_6355;
if(
REALP(BgL_dz00_3665))
{ /* Llib/thread.scm 487 */
BgL_arg1514z00_6355 = 
(
REAL_TO_DOUBLE(BgL_dz00_3665)*((double)1000000.0)); }  else 
{ /* Llib/thread.scm 487 */
BgL_arg1514z00_6355 = 
(
BGl_numberzd2ze3flonumz31zz__r4_numbers_6_5z00(BgL_dz00_3665)*((double)1000000.0)); } 
BgL_arg1513z00_6354 = 
(long)(BgL_arg1514z00_6355); } 
bgl_sleep(BgL_arg1513z00_6354); BUNSPEC; 
return 
BINT(BgL_arg1513z00_6354);}  else 
{ /* Llib/thread.scm 486 */
return 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_symbol2628z00zz__threadz00, BGl_string2629z00zz__threadz00, BgL_dz00_3665);} } } } } } } 

}



/* &%user-current-thread1227 */
obj_t BGl_z62z52userzd2currentzd2thread1227z30zz__threadz00(obj_t BgL_envz00_3666, obj_t BgL_oz00_3667)
{
{ /* Llib/thread.scm 451 */
{ 
 BgL_threadz00_bglt BgL_auxz00_10697;
{ /* Llib/thread.scm 451 */
 BgL_threadz00_bglt BgL_oz00_6396;
{ /* Llib/thread.scm 451 */
 bool_t BgL_test3543z00_10698;
{ /* Llib/thread.scm 451 */
 obj_t BgL_classz00_6356;
BgL_classz00_6356 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3667))
{ /* Llib/thread.scm 451 */
 BgL_objectz00_bglt BgL_arg1932z00_6358; long BgL_arg1933z00_6359;
BgL_arg1932z00_6358 = 
(BgL_objectz00_bglt)(BgL_oz00_3667); 
BgL_arg1933z00_6359 = 
BGL_CLASS_DEPTH(BgL_classz00_6356); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 451 */
 long BgL_idxz00_6367;
BgL_idxz00_6367 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6358); 
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1923z00_6368;
{ /* Llib/thread.scm 451 */
 long BgL_arg1924z00_6369;
BgL_arg1924z00_6369 = 
(BgL_idxz00_6367+BgL_arg1933z00_6359); 
BgL_arg1923z00_6368 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6369); } 
BgL_test3543z00_10698 = 
(BgL_arg1923z00_6368==BgL_classz00_6356); } }  else 
{ /* Llib/thread.scm 451 */
 bool_t BgL_res2044z00_6374;
{ /* Llib/thread.scm 451 */
 obj_t BgL_oclassz00_6378;
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1937z00_6380; long BgL_arg1938z00_6381;
BgL_arg1937z00_6380 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 451 */
 long BgL_arg1939z00_6382;
BgL_arg1939z00_6382 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6358); 
BgL_arg1938z00_6381 = 
(BgL_arg1939z00_6382-OBJECT_TYPE); } 
BgL_oclassz00_6378 = 
VECTOR_REF(BgL_arg1937z00_6380,BgL_arg1938z00_6381); } 
{ /* Llib/thread.scm 451 */
 bool_t BgL__ortest_1147z00_6388;
BgL__ortest_1147z00_6388 = 
(BgL_classz00_6356==BgL_oclassz00_6378); 
if(BgL__ortest_1147z00_6388)
{ /* Llib/thread.scm 451 */
BgL_res2044z00_6374 = BgL__ortest_1147z00_6388; }  else 
{ /* Llib/thread.scm 451 */
 long BgL_odepthz00_6389;
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1927z00_6390;
BgL_arg1927z00_6390 = 
(BgL_oclassz00_6378); 
BgL_odepthz00_6389 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6390); } 
if(
(BgL_arg1933z00_6359<BgL_odepthz00_6389))
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1925z00_6394;
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1926z00_6395;
BgL_arg1926z00_6395 = 
(BgL_oclassz00_6378); 
BgL_arg1925z00_6394 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6395, BgL_arg1933z00_6359); } 
BgL_res2044z00_6374 = 
(BgL_arg1925z00_6394==BgL_classz00_6356); }  else 
{ /* Llib/thread.scm 451 */
BgL_res2044z00_6374 = ((bool_t)0); } } } } 
BgL_test3543z00_10698 = BgL_res2044z00_6374; } }  else 
{ /* Llib/thread.scm 451 */
BgL_test3543z00_10698 = ((bool_t)0)
; } } 
if(BgL_test3543z00_10698)
{ /* Llib/thread.scm 451 */
BgL_oz00_6396 = 
((BgL_threadz00_bglt)BgL_oz00_3667); }  else 
{ 
 obj_t BgL_auxz00_10723;
BgL_auxz00_10723 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19506L), BGl_string2631z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3667); 
FAILURE(BgL_auxz00_10723,BFALSE,BFALSE);} } 
BgL_auxz00_10697 = BgL_oz00_6396; } 
return 
((obj_t)BgL_auxz00_10697);} } 

}



/* &thread-name-set!1220 */
obj_t BGl_z62threadzd2namezd2setz121220z70zz__threadz00(obj_t BgL_envz00_3668, obj_t BgL_thz00_3669, obj_t BgL_vz00_3670)
{
{ /* Llib/thread.scm 440 */
{ /* Llib/thread.scm 440 */
 BgL_threadz00_bglt BgL_thz00_6437; obj_t BgL_vz00_6438;
{ /* Llib/thread.scm 440 */
 bool_t BgL_test3548z00_10728;
{ /* Llib/thread.scm 440 */
 obj_t BgL_classz00_6397;
BgL_classz00_6397 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3669))
{ /* Llib/thread.scm 440 */
 BgL_objectz00_bglt BgL_arg1932z00_6399; long BgL_arg1933z00_6400;
BgL_arg1932z00_6399 = 
(BgL_objectz00_bglt)(BgL_thz00_3669); 
BgL_arg1933z00_6400 = 
BGL_CLASS_DEPTH(BgL_classz00_6397); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 440 */
 long BgL_idxz00_6408;
BgL_idxz00_6408 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6399); 
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1923z00_6409;
{ /* Llib/thread.scm 440 */
 long BgL_arg1924z00_6410;
BgL_arg1924z00_6410 = 
(BgL_idxz00_6408+BgL_arg1933z00_6400); 
BgL_arg1923z00_6409 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6410); } 
BgL_test3548z00_10728 = 
(BgL_arg1923z00_6409==BgL_classz00_6397); } }  else 
{ /* Llib/thread.scm 440 */
 bool_t BgL_res2044z00_6415;
{ /* Llib/thread.scm 440 */
 obj_t BgL_oclassz00_6419;
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1937z00_6421; long BgL_arg1938z00_6422;
BgL_arg1937z00_6421 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 440 */
 long BgL_arg1939z00_6423;
BgL_arg1939z00_6423 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6399); 
BgL_arg1938z00_6422 = 
(BgL_arg1939z00_6423-OBJECT_TYPE); } 
BgL_oclassz00_6419 = 
VECTOR_REF(BgL_arg1937z00_6421,BgL_arg1938z00_6422); } 
{ /* Llib/thread.scm 440 */
 bool_t BgL__ortest_1147z00_6429;
BgL__ortest_1147z00_6429 = 
(BgL_classz00_6397==BgL_oclassz00_6419); 
if(BgL__ortest_1147z00_6429)
{ /* Llib/thread.scm 440 */
BgL_res2044z00_6415 = BgL__ortest_1147z00_6429; }  else 
{ /* Llib/thread.scm 440 */
 long BgL_odepthz00_6430;
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1927z00_6431;
BgL_arg1927z00_6431 = 
(BgL_oclassz00_6419); 
BgL_odepthz00_6430 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6431); } 
if(
(BgL_arg1933z00_6400<BgL_odepthz00_6430))
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1925z00_6435;
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1926z00_6436;
BgL_arg1926z00_6436 = 
(BgL_oclassz00_6419); 
BgL_arg1925z00_6435 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6436, BgL_arg1933z00_6400); } 
BgL_res2044z00_6415 = 
(BgL_arg1925z00_6435==BgL_classz00_6397); }  else 
{ /* Llib/thread.scm 440 */
BgL_res2044z00_6415 = ((bool_t)0); } } } } 
BgL_test3548z00_10728 = BgL_res2044z00_6415; } }  else 
{ /* Llib/thread.scm 440 */
BgL_test3548z00_10728 = ((bool_t)0)
; } } 
if(BgL_test3548z00_10728)
{ /* Llib/thread.scm 440 */
BgL_thz00_6437 = 
((BgL_threadz00_bglt)BgL_thz00_3669); }  else 
{ 
 obj_t BgL_auxz00_10753;
BgL_auxz00_10753 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18877L), BGl_string2634z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3669); 
FAILURE(BgL_auxz00_10753,BFALSE,BFALSE);} } 
if(
STRINGP(BgL_vz00_3670))
{ /* Llib/thread.scm 440 */
BgL_vz00_6438 = BgL_vz00_3670; }  else 
{ 
 obj_t BgL_auxz00_10759;
BgL_auxz00_10759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18877L), BGl_string2634z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_vz00_3670); 
FAILURE(BgL_auxz00_10759,BFALSE,BFALSE);} 
return 
BGl_errorz00zz__errorz00(BGl_symbol2632z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6437));} } 

}



/* &thread-name1218 */
obj_t BGl_z62threadzd2name1218zb0zz__threadz00(obj_t BgL_envz00_3671, obj_t BgL_thz00_3672)
{
{ /* Llib/thread.scm 435 */
{ /* Llib/thread.scm 435 */
 BgL_threadz00_bglt BgL_thz00_6479;
{ /* Llib/thread.scm 435 */
 bool_t BgL_test3554z00_10765;
{ /* Llib/thread.scm 435 */
 obj_t BgL_classz00_6439;
BgL_classz00_6439 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3672))
{ /* Llib/thread.scm 435 */
 BgL_objectz00_bglt BgL_arg1932z00_6441; long BgL_arg1933z00_6442;
BgL_arg1932z00_6441 = 
(BgL_objectz00_bglt)(BgL_thz00_3672); 
BgL_arg1933z00_6442 = 
BGL_CLASS_DEPTH(BgL_classz00_6439); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 435 */
 long BgL_idxz00_6450;
BgL_idxz00_6450 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6441); 
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1923z00_6451;
{ /* Llib/thread.scm 435 */
 long BgL_arg1924z00_6452;
BgL_arg1924z00_6452 = 
(BgL_idxz00_6450+BgL_arg1933z00_6442); 
BgL_arg1923z00_6451 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6452); } 
BgL_test3554z00_10765 = 
(BgL_arg1923z00_6451==BgL_classz00_6439); } }  else 
{ /* Llib/thread.scm 435 */
 bool_t BgL_res2044z00_6457;
{ /* Llib/thread.scm 435 */
 obj_t BgL_oclassz00_6461;
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1937z00_6463; long BgL_arg1938z00_6464;
BgL_arg1937z00_6463 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 435 */
 long BgL_arg1939z00_6465;
BgL_arg1939z00_6465 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6441); 
BgL_arg1938z00_6464 = 
(BgL_arg1939z00_6465-OBJECT_TYPE); } 
BgL_oclassz00_6461 = 
VECTOR_REF(BgL_arg1937z00_6463,BgL_arg1938z00_6464); } 
{ /* Llib/thread.scm 435 */
 bool_t BgL__ortest_1147z00_6471;
BgL__ortest_1147z00_6471 = 
(BgL_classz00_6439==BgL_oclassz00_6461); 
if(BgL__ortest_1147z00_6471)
{ /* Llib/thread.scm 435 */
BgL_res2044z00_6457 = BgL__ortest_1147z00_6471; }  else 
{ /* Llib/thread.scm 435 */
 long BgL_odepthz00_6472;
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1927z00_6473;
BgL_arg1927z00_6473 = 
(BgL_oclassz00_6461); 
BgL_odepthz00_6472 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6473); } 
if(
(BgL_arg1933z00_6442<BgL_odepthz00_6472))
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1925z00_6477;
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1926z00_6478;
BgL_arg1926z00_6478 = 
(BgL_oclassz00_6461); 
BgL_arg1925z00_6477 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6478, BgL_arg1933z00_6442); } 
BgL_res2044z00_6457 = 
(BgL_arg1925z00_6477==BgL_classz00_6439); }  else 
{ /* Llib/thread.scm 435 */
BgL_res2044z00_6457 = ((bool_t)0); } } } } 
BgL_test3554z00_10765 = BgL_res2044z00_6457; } }  else 
{ /* Llib/thread.scm 435 */
BgL_test3554z00_10765 = ((bool_t)0)
; } } 
if(BgL_test3554z00_10765)
{ /* Llib/thread.scm 435 */
BgL_thz00_6479 = 
((BgL_threadz00_bglt)BgL_thz00_3672); }  else 
{ 
 obj_t BgL_auxz00_10790;
BgL_auxz00_10790 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18612L), BGl_string2636z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3672); 
FAILURE(BgL_auxz00_10790,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2635z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6479));} } 

}



/* &thread-cleanup-set!1215 */
obj_t BGl_z62threadzd2cleanupzd2setz121215z70zz__threadz00(obj_t BgL_envz00_3673, obj_t BgL_thz00_3674, obj_t BgL_vz00_3675)
{
{ /* Llib/thread.scm 430 */
{ /* Llib/thread.scm 430 */
 BgL_threadz00_bglt BgL_thz00_6520;
{ /* Llib/thread.scm 430 */
 bool_t BgL_test3559z00_10796;
{ /* Llib/thread.scm 430 */
 obj_t BgL_classz00_6480;
BgL_classz00_6480 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3674))
{ /* Llib/thread.scm 430 */
 BgL_objectz00_bglt BgL_arg1932z00_6482; long BgL_arg1933z00_6483;
BgL_arg1932z00_6482 = 
(BgL_objectz00_bglt)(BgL_thz00_3674); 
BgL_arg1933z00_6483 = 
BGL_CLASS_DEPTH(BgL_classz00_6480); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 430 */
 long BgL_idxz00_6491;
BgL_idxz00_6491 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6482); 
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1923z00_6492;
{ /* Llib/thread.scm 430 */
 long BgL_arg1924z00_6493;
BgL_arg1924z00_6493 = 
(BgL_idxz00_6491+BgL_arg1933z00_6483); 
BgL_arg1923z00_6492 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6493); } 
BgL_test3559z00_10796 = 
(BgL_arg1923z00_6492==BgL_classz00_6480); } }  else 
{ /* Llib/thread.scm 430 */
 bool_t BgL_res2044z00_6498;
{ /* Llib/thread.scm 430 */
 obj_t BgL_oclassz00_6502;
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1937z00_6504; long BgL_arg1938z00_6505;
BgL_arg1937z00_6504 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 430 */
 long BgL_arg1939z00_6506;
BgL_arg1939z00_6506 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6482); 
BgL_arg1938z00_6505 = 
(BgL_arg1939z00_6506-OBJECT_TYPE); } 
BgL_oclassz00_6502 = 
VECTOR_REF(BgL_arg1937z00_6504,BgL_arg1938z00_6505); } 
{ /* Llib/thread.scm 430 */
 bool_t BgL__ortest_1147z00_6512;
BgL__ortest_1147z00_6512 = 
(BgL_classz00_6480==BgL_oclassz00_6502); 
if(BgL__ortest_1147z00_6512)
{ /* Llib/thread.scm 430 */
BgL_res2044z00_6498 = BgL__ortest_1147z00_6512; }  else 
{ /* Llib/thread.scm 430 */
 long BgL_odepthz00_6513;
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1927z00_6514;
BgL_arg1927z00_6514 = 
(BgL_oclassz00_6502); 
BgL_odepthz00_6513 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6514); } 
if(
(BgL_arg1933z00_6483<BgL_odepthz00_6513))
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1925z00_6518;
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1926z00_6519;
BgL_arg1926z00_6519 = 
(BgL_oclassz00_6502); 
BgL_arg1925z00_6518 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6519, BgL_arg1933z00_6483); } 
BgL_res2044z00_6498 = 
(BgL_arg1925z00_6518==BgL_classz00_6480); }  else 
{ /* Llib/thread.scm 430 */
BgL_res2044z00_6498 = ((bool_t)0); } } } } 
BgL_test3559z00_10796 = BgL_res2044z00_6498; } }  else 
{ /* Llib/thread.scm 430 */
BgL_test3559z00_10796 = ((bool_t)0)
; } } 
if(BgL_test3559z00_10796)
{ /* Llib/thread.scm 430 */
BgL_thz00_6520 = 
((BgL_threadz00_bglt)BgL_thz00_3674); }  else 
{ 
 obj_t BgL_auxz00_10821;
BgL_auxz00_10821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18332L), BGl_string2638z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3674); 
FAILURE(BgL_auxz00_10821,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2637z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6520));} } 

}



/* &thread-cleanup1213 */
obj_t BGl_z62threadzd2cleanup1213zb0zz__threadz00(obj_t BgL_envz00_3676, obj_t BgL_thz00_3677)
{
{ /* Llib/thread.scm 425 */
{ /* Llib/thread.scm 425 */
 BgL_threadz00_bglt BgL_thz00_6561;
{ /* Llib/thread.scm 425 */
 bool_t BgL_test3564z00_10827;
{ /* Llib/thread.scm 425 */
 obj_t BgL_classz00_6521;
BgL_classz00_6521 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3677))
{ /* Llib/thread.scm 425 */
 BgL_objectz00_bglt BgL_arg1932z00_6523; long BgL_arg1933z00_6524;
BgL_arg1932z00_6523 = 
(BgL_objectz00_bglt)(BgL_thz00_3677); 
BgL_arg1933z00_6524 = 
BGL_CLASS_DEPTH(BgL_classz00_6521); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 425 */
 long BgL_idxz00_6532;
BgL_idxz00_6532 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6523); 
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1923z00_6533;
{ /* Llib/thread.scm 425 */
 long BgL_arg1924z00_6534;
BgL_arg1924z00_6534 = 
(BgL_idxz00_6532+BgL_arg1933z00_6524); 
BgL_arg1923z00_6533 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6534); } 
BgL_test3564z00_10827 = 
(BgL_arg1923z00_6533==BgL_classz00_6521); } }  else 
{ /* Llib/thread.scm 425 */
 bool_t BgL_res2044z00_6539;
{ /* Llib/thread.scm 425 */
 obj_t BgL_oclassz00_6543;
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1937z00_6545; long BgL_arg1938z00_6546;
BgL_arg1937z00_6545 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 425 */
 long BgL_arg1939z00_6547;
BgL_arg1939z00_6547 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6523); 
BgL_arg1938z00_6546 = 
(BgL_arg1939z00_6547-OBJECT_TYPE); } 
BgL_oclassz00_6543 = 
VECTOR_REF(BgL_arg1937z00_6545,BgL_arg1938z00_6546); } 
{ /* Llib/thread.scm 425 */
 bool_t BgL__ortest_1147z00_6553;
BgL__ortest_1147z00_6553 = 
(BgL_classz00_6521==BgL_oclassz00_6543); 
if(BgL__ortest_1147z00_6553)
{ /* Llib/thread.scm 425 */
BgL_res2044z00_6539 = BgL__ortest_1147z00_6553; }  else 
{ /* Llib/thread.scm 425 */
 long BgL_odepthz00_6554;
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1927z00_6555;
BgL_arg1927z00_6555 = 
(BgL_oclassz00_6543); 
BgL_odepthz00_6554 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6555); } 
if(
(BgL_arg1933z00_6524<BgL_odepthz00_6554))
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1925z00_6559;
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1926z00_6560;
BgL_arg1926z00_6560 = 
(BgL_oclassz00_6543); 
BgL_arg1925z00_6559 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6560, BgL_arg1933z00_6524); } 
BgL_res2044z00_6539 = 
(BgL_arg1925z00_6559==BgL_classz00_6521); }  else 
{ /* Llib/thread.scm 425 */
BgL_res2044z00_6539 = ((bool_t)0); } } } } 
BgL_test3564z00_10827 = BgL_res2044z00_6539; } }  else 
{ /* Llib/thread.scm 425 */
BgL_test3564z00_10827 = ((bool_t)0)
; } } 
if(BgL_test3564z00_10827)
{ /* Llib/thread.scm 425 */
BgL_thz00_6561 = 
((BgL_threadz00_bglt)BgL_thz00_3677); }  else 
{ 
 obj_t BgL_auxz00_10852;
BgL_auxz00_10852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18064L), BGl_string2640z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3677); 
FAILURE(BgL_auxz00_10852,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2639z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6561));} } 

}



/* &thread-specific-set!1211 */
obj_t BGl_z62threadzd2specificzd2setz121211z70zz__threadz00(obj_t BgL_envz00_3678, obj_t BgL_thz00_3679, obj_t BgL_vz00_3680)
{
{ /* Llib/thread.scm 420 */
{ /* Llib/thread.scm 420 */
 BgL_threadz00_bglt BgL_thz00_6602;
{ /* Llib/thread.scm 420 */
 bool_t BgL_test3569z00_10858;
{ /* Llib/thread.scm 420 */
 obj_t BgL_classz00_6562;
BgL_classz00_6562 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3679))
{ /* Llib/thread.scm 420 */
 BgL_objectz00_bglt BgL_arg1932z00_6564; long BgL_arg1933z00_6565;
BgL_arg1932z00_6564 = 
(BgL_objectz00_bglt)(BgL_thz00_3679); 
BgL_arg1933z00_6565 = 
BGL_CLASS_DEPTH(BgL_classz00_6562); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 420 */
 long BgL_idxz00_6573;
BgL_idxz00_6573 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6564); 
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1923z00_6574;
{ /* Llib/thread.scm 420 */
 long BgL_arg1924z00_6575;
BgL_arg1924z00_6575 = 
(BgL_idxz00_6573+BgL_arg1933z00_6565); 
BgL_arg1923z00_6574 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6575); } 
BgL_test3569z00_10858 = 
(BgL_arg1923z00_6574==BgL_classz00_6562); } }  else 
{ /* Llib/thread.scm 420 */
 bool_t BgL_res2044z00_6580;
{ /* Llib/thread.scm 420 */
 obj_t BgL_oclassz00_6584;
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1937z00_6586; long BgL_arg1938z00_6587;
BgL_arg1937z00_6586 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 420 */
 long BgL_arg1939z00_6588;
BgL_arg1939z00_6588 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6564); 
BgL_arg1938z00_6587 = 
(BgL_arg1939z00_6588-OBJECT_TYPE); } 
BgL_oclassz00_6584 = 
VECTOR_REF(BgL_arg1937z00_6586,BgL_arg1938z00_6587); } 
{ /* Llib/thread.scm 420 */
 bool_t BgL__ortest_1147z00_6594;
BgL__ortest_1147z00_6594 = 
(BgL_classz00_6562==BgL_oclassz00_6584); 
if(BgL__ortest_1147z00_6594)
{ /* Llib/thread.scm 420 */
BgL_res2044z00_6580 = BgL__ortest_1147z00_6594; }  else 
{ /* Llib/thread.scm 420 */
 long BgL_odepthz00_6595;
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1927z00_6596;
BgL_arg1927z00_6596 = 
(BgL_oclassz00_6584); 
BgL_odepthz00_6595 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6596); } 
if(
(BgL_arg1933z00_6565<BgL_odepthz00_6595))
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1925z00_6600;
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1926z00_6601;
BgL_arg1926z00_6601 = 
(BgL_oclassz00_6584); 
BgL_arg1925z00_6600 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6601, BgL_arg1933z00_6565); } 
BgL_res2044z00_6580 = 
(BgL_arg1925z00_6600==BgL_classz00_6562); }  else 
{ /* Llib/thread.scm 420 */
BgL_res2044z00_6580 = ((bool_t)0); } } } } 
BgL_test3569z00_10858 = BgL_res2044z00_6580; } }  else 
{ /* Llib/thread.scm 420 */
BgL_test3569z00_10858 = ((bool_t)0)
; } } 
if(BgL_test3569z00_10858)
{ /* Llib/thread.scm 420 */
BgL_thz00_6602 = 
((BgL_threadz00_bglt)BgL_thz00_3679); }  else 
{ 
 obj_t BgL_auxz00_10883;
BgL_auxz00_10883 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17783L), BGl_string2642z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3679); 
FAILURE(BgL_auxz00_10883,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2641z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6602));} } 

}



/* &thread-specific1209 */
obj_t BGl_z62threadzd2specific1209zb0zz__threadz00(obj_t BgL_envz00_3681, obj_t BgL_thz00_3682)
{
{ /* Llib/thread.scm 415 */
{ /* Llib/thread.scm 415 */
 BgL_threadz00_bglt BgL_thz00_6643;
{ /* Llib/thread.scm 415 */
 bool_t BgL_test3574z00_10889;
{ /* Llib/thread.scm 415 */
 obj_t BgL_classz00_6603;
BgL_classz00_6603 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3682))
{ /* Llib/thread.scm 415 */
 BgL_objectz00_bglt BgL_arg1932z00_6605; long BgL_arg1933z00_6606;
BgL_arg1932z00_6605 = 
(BgL_objectz00_bglt)(BgL_thz00_3682); 
BgL_arg1933z00_6606 = 
BGL_CLASS_DEPTH(BgL_classz00_6603); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 415 */
 long BgL_idxz00_6614;
BgL_idxz00_6614 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6605); 
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1923z00_6615;
{ /* Llib/thread.scm 415 */
 long BgL_arg1924z00_6616;
BgL_arg1924z00_6616 = 
(BgL_idxz00_6614+BgL_arg1933z00_6606); 
BgL_arg1923z00_6615 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6616); } 
BgL_test3574z00_10889 = 
(BgL_arg1923z00_6615==BgL_classz00_6603); } }  else 
{ /* Llib/thread.scm 415 */
 bool_t BgL_res2044z00_6621;
{ /* Llib/thread.scm 415 */
 obj_t BgL_oclassz00_6625;
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1937z00_6627; long BgL_arg1938z00_6628;
BgL_arg1937z00_6627 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 415 */
 long BgL_arg1939z00_6629;
BgL_arg1939z00_6629 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6605); 
BgL_arg1938z00_6628 = 
(BgL_arg1939z00_6629-OBJECT_TYPE); } 
BgL_oclassz00_6625 = 
VECTOR_REF(BgL_arg1937z00_6627,BgL_arg1938z00_6628); } 
{ /* Llib/thread.scm 415 */
 bool_t BgL__ortest_1147z00_6635;
BgL__ortest_1147z00_6635 = 
(BgL_classz00_6603==BgL_oclassz00_6625); 
if(BgL__ortest_1147z00_6635)
{ /* Llib/thread.scm 415 */
BgL_res2044z00_6621 = BgL__ortest_1147z00_6635; }  else 
{ /* Llib/thread.scm 415 */
 long BgL_odepthz00_6636;
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1927z00_6637;
BgL_arg1927z00_6637 = 
(BgL_oclassz00_6625); 
BgL_odepthz00_6636 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6637); } 
if(
(BgL_arg1933z00_6606<BgL_odepthz00_6636))
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1925z00_6641;
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1926z00_6642;
BgL_arg1926z00_6642 = 
(BgL_oclassz00_6625); 
BgL_arg1925z00_6641 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6642, BgL_arg1933z00_6606); } 
BgL_res2044z00_6621 = 
(BgL_arg1925z00_6641==BgL_classz00_6603); }  else 
{ /* Llib/thread.scm 415 */
BgL_res2044z00_6621 = ((bool_t)0); } } } } 
BgL_test3574z00_10889 = BgL_res2044z00_6621; } }  else 
{ /* Llib/thread.scm 415 */
BgL_test3574z00_10889 = ((bool_t)0)
; } } 
if(BgL_test3574z00_10889)
{ /* Llib/thread.scm 415 */
BgL_thz00_6643 = 
((BgL_threadz00_bglt)BgL_thz00_3682); }  else 
{ 
 obj_t BgL_auxz00_10914;
BgL_auxz00_10914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17514L), BGl_string2644z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3682); 
FAILURE(BgL_auxz00_10914,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2643z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6643));} } 

}



/* &thread-kill!1207 */
obj_t BGl_z62threadzd2killz121207za2zz__threadz00(obj_t BgL_envz00_3683, obj_t BgL_thz00_3684, obj_t BgL_nz00_3685)
{
{ /* Llib/thread.scm 410 */
{ /* Llib/thread.scm 410 */
 BgL_threadz00_bglt BgL_thz00_6684; int BgL_nz00_6685;
{ /* Llib/thread.scm 410 */
 bool_t BgL_test3579z00_10920;
{ /* Llib/thread.scm 410 */
 obj_t BgL_classz00_6644;
BgL_classz00_6644 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3684))
{ /* Llib/thread.scm 410 */
 BgL_objectz00_bglt BgL_arg1932z00_6646; long BgL_arg1933z00_6647;
BgL_arg1932z00_6646 = 
(BgL_objectz00_bglt)(BgL_thz00_3684); 
BgL_arg1933z00_6647 = 
BGL_CLASS_DEPTH(BgL_classz00_6644); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 410 */
 long BgL_idxz00_6655;
BgL_idxz00_6655 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6646); 
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1923z00_6656;
{ /* Llib/thread.scm 410 */
 long BgL_arg1924z00_6657;
BgL_arg1924z00_6657 = 
(BgL_idxz00_6655+BgL_arg1933z00_6647); 
BgL_arg1923z00_6656 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6657); } 
BgL_test3579z00_10920 = 
(BgL_arg1923z00_6656==BgL_classz00_6644); } }  else 
{ /* Llib/thread.scm 410 */
 bool_t BgL_res2044z00_6662;
{ /* Llib/thread.scm 410 */
 obj_t BgL_oclassz00_6666;
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1937z00_6668; long BgL_arg1938z00_6669;
BgL_arg1937z00_6668 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 410 */
 long BgL_arg1939z00_6670;
BgL_arg1939z00_6670 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6646); 
BgL_arg1938z00_6669 = 
(BgL_arg1939z00_6670-OBJECT_TYPE); } 
BgL_oclassz00_6666 = 
VECTOR_REF(BgL_arg1937z00_6668,BgL_arg1938z00_6669); } 
{ /* Llib/thread.scm 410 */
 bool_t BgL__ortest_1147z00_6676;
BgL__ortest_1147z00_6676 = 
(BgL_classz00_6644==BgL_oclassz00_6666); 
if(BgL__ortest_1147z00_6676)
{ /* Llib/thread.scm 410 */
BgL_res2044z00_6662 = BgL__ortest_1147z00_6676; }  else 
{ /* Llib/thread.scm 410 */
 long BgL_odepthz00_6677;
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1927z00_6678;
BgL_arg1927z00_6678 = 
(BgL_oclassz00_6666); 
BgL_odepthz00_6677 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6678); } 
if(
(BgL_arg1933z00_6647<BgL_odepthz00_6677))
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1925z00_6682;
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1926z00_6683;
BgL_arg1926z00_6683 = 
(BgL_oclassz00_6666); 
BgL_arg1925z00_6682 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6683, BgL_arg1933z00_6647); } 
BgL_res2044z00_6662 = 
(BgL_arg1925z00_6682==BgL_classz00_6644); }  else 
{ /* Llib/thread.scm 410 */
BgL_res2044z00_6662 = ((bool_t)0); } } } } 
BgL_test3579z00_10920 = BgL_res2044z00_6662; } }  else 
{ /* Llib/thread.scm 410 */
BgL_test3579z00_10920 = ((bool_t)0)
; } } 
if(BgL_test3579z00_10920)
{ /* Llib/thread.scm 410 */
BgL_thz00_6684 = 
((BgL_threadz00_bglt)BgL_thz00_3684); }  else 
{ 
 obj_t BgL_auxz00_10945;
BgL_auxz00_10945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17241L), BGl_string2646z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3684); 
FAILURE(BgL_auxz00_10945,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 410 */
 obj_t BgL_tmpz00_10949;
if(
INTEGERP(BgL_nz00_3685))
{ /* Llib/thread.scm 410 */
BgL_tmpz00_10949 = BgL_nz00_3685
; }  else 
{ 
 obj_t BgL_auxz00_10952;
BgL_auxz00_10952 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17241L), BGl_string2646z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_nz00_3685); 
FAILURE(BgL_auxz00_10952,BFALSE,BFALSE);} 
BgL_nz00_6685 = 
CINT(BgL_tmpz00_10949); } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2645z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6684));} } 

}



/* &thread-terminate!1204 */
obj_t BGl_z62threadzd2terminatez121204za2zz__threadz00(obj_t BgL_envz00_3686, obj_t BgL_thz00_3687)
{
{ /* Llib/thread.scm 405 */
{ /* Llib/thread.scm 405 */
 BgL_threadz00_bglt BgL_thz00_6726;
{ /* Llib/thread.scm 405 */
 bool_t BgL_test3585z00_10959;
{ /* Llib/thread.scm 405 */
 obj_t BgL_classz00_6686;
BgL_classz00_6686 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3687))
{ /* Llib/thread.scm 405 */
 BgL_objectz00_bglt BgL_arg1932z00_6688; long BgL_arg1933z00_6689;
BgL_arg1932z00_6688 = 
(BgL_objectz00_bglt)(BgL_thz00_3687); 
BgL_arg1933z00_6689 = 
BGL_CLASS_DEPTH(BgL_classz00_6686); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 405 */
 long BgL_idxz00_6697;
BgL_idxz00_6697 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6688); 
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1923z00_6698;
{ /* Llib/thread.scm 405 */
 long BgL_arg1924z00_6699;
BgL_arg1924z00_6699 = 
(BgL_idxz00_6697+BgL_arg1933z00_6689); 
BgL_arg1923z00_6698 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6699); } 
BgL_test3585z00_10959 = 
(BgL_arg1923z00_6698==BgL_classz00_6686); } }  else 
{ /* Llib/thread.scm 405 */
 bool_t BgL_res2044z00_6704;
{ /* Llib/thread.scm 405 */
 obj_t BgL_oclassz00_6708;
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1937z00_6710; long BgL_arg1938z00_6711;
BgL_arg1937z00_6710 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 405 */
 long BgL_arg1939z00_6712;
BgL_arg1939z00_6712 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6688); 
BgL_arg1938z00_6711 = 
(BgL_arg1939z00_6712-OBJECT_TYPE); } 
BgL_oclassz00_6708 = 
VECTOR_REF(BgL_arg1937z00_6710,BgL_arg1938z00_6711); } 
{ /* Llib/thread.scm 405 */
 bool_t BgL__ortest_1147z00_6718;
BgL__ortest_1147z00_6718 = 
(BgL_classz00_6686==BgL_oclassz00_6708); 
if(BgL__ortest_1147z00_6718)
{ /* Llib/thread.scm 405 */
BgL_res2044z00_6704 = BgL__ortest_1147z00_6718; }  else 
{ /* Llib/thread.scm 405 */
 long BgL_odepthz00_6719;
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1927z00_6720;
BgL_arg1927z00_6720 = 
(BgL_oclassz00_6708); 
BgL_odepthz00_6719 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6720); } 
if(
(BgL_arg1933z00_6689<BgL_odepthz00_6719))
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1925z00_6724;
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1926z00_6725;
BgL_arg1926z00_6725 = 
(BgL_oclassz00_6708); 
BgL_arg1925z00_6724 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6725, BgL_arg1933z00_6689); } 
BgL_res2044z00_6704 = 
(BgL_arg1925z00_6724==BgL_classz00_6686); }  else 
{ /* Llib/thread.scm 405 */
BgL_res2044z00_6704 = ((bool_t)0); } } } } 
BgL_test3585z00_10959 = BgL_res2044z00_6704; } }  else 
{ /* Llib/thread.scm 405 */
BgL_test3585z00_10959 = ((bool_t)0)
; } } 
if(BgL_test3585z00_10959)
{ /* Llib/thread.scm 405 */
BgL_thz00_6726 = 
((BgL_threadz00_bglt)BgL_thz00_3687); }  else 
{ 
 obj_t BgL_auxz00_10984;
BgL_auxz00_10984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16970L), BGl_string2648z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3687); 
FAILURE(BgL_auxz00_10984,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2647z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6726));} } 

}



/* &thread-join!1202 */
obj_t BGl_z62threadzd2joinz121202za2zz__threadz00(obj_t BgL_envz00_3688, obj_t BgL_thz00_3689, obj_t BgL_timeoutz00_3690)
{
{ /* Llib/thread.scm 400 */
{ /* Llib/thread.scm 400 */
 BgL_threadz00_bglt BgL_thz00_6767;
{ /* Llib/thread.scm 400 */
 bool_t BgL_test3590z00_10990;
{ /* Llib/thread.scm 400 */
 obj_t BgL_classz00_6727;
BgL_classz00_6727 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3689))
{ /* Llib/thread.scm 400 */
 BgL_objectz00_bglt BgL_arg1932z00_6729; long BgL_arg1933z00_6730;
BgL_arg1932z00_6729 = 
(BgL_objectz00_bglt)(BgL_thz00_3689); 
BgL_arg1933z00_6730 = 
BGL_CLASS_DEPTH(BgL_classz00_6727); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 400 */
 long BgL_idxz00_6738;
BgL_idxz00_6738 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6729); 
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1923z00_6739;
{ /* Llib/thread.scm 400 */
 long BgL_arg1924z00_6740;
BgL_arg1924z00_6740 = 
(BgL_idxz00_6738+BgL_arg1933z00_6730); 
BgL_arg1923z00_6739 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6740); } 
BgL_test3590z00_10990 = 
(BgL_arg1923z00_6739==BgL_classz00_6727); } }  else 
{ /* Llib/thread.scm 400 */
 bool_t BgL_res2044z00_6745;
{ /* Llib/thread.scm 400 */
 obj_t BgL_oclassz00_6749;
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1937z00_6751; long BgL_arg1938z00_6752;
BgL_arg1937z00_6751 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 400 */
 long BgL_arg1939z00_6753;
BgL_arg1939z00_6753 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6729); 
BgL_arg1938z00_6752 = 
(BgL_arg1939z00_6753-OBJECT_TYPE); } 
BgL_oclassz00_6749 = 
VECTOR_REF(BgL_arg1937z00_6751,BgL_arg1938z00_6752); } 
{ /* Llib/thread.scm 400 */
 bool_t BgL__ortest_1147z00_6759;
BgL__ortest_1147z00_6759 = 
(BgL_classz00_6727==BgL_oclassz00_6749); 
if(BgL__ortest_1147z00_6759)
{ /* Llib/thread.scm 400 */
BgL_res2044z00_6745 = BgL__ortest_1147z00_6759; }  else 
{ /* Llib/thread.scm 400 */
 long BgL_odepthz00_6760;
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1927z00_6761;
BgL_arg1927z00_6761 = 
(BgL_oclassz00_6749); 
BgL_odepthz00_6760 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6761); } 
if(
(BgL_arg1933z00_6730<BgL_odepthz00_6760))
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1925z00_6765;
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1926z00_6766;
BgL_arg1926z00_6766 = 
(BgL_oclassz00_6749); 
BgL_arg1925z00_6765 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6766, BgL_arg1933z00_6730); } 
BgL_res2044z00_6745 = 
(BgL_arg1925z00_6765==BgL_classz00_6727); }  else 
{ /* Llib/thread.scm 400 */
BgL_res2044z00_6745 = ((bool_t)0); } } } } 
BgL_test3590z00_10990 = BgL_res2044z00_6745; } }  else 
{ /* Llib/thread.scm 400 */
BgL_test3590z00_10990 = ((bool_t)0)
; } } 
if(BgL_test3590z00_10990)
{ /* Llib/thread.scm 400 */
BgL_thz00_6767 = 
((BgL_threadz00_bglt)BgL_thz00_3689); }  else 
{ 
 obj_t BgL_auxz00_11015;
BgL_auxz00_11015 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16694L), BGl_string2650z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3689); 
FAILURE(BgL_auxz00_11015,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2649z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6767));} } 

}



/* &thread-start-joinabl1199 */
obj_t BGl_z62threadzd2startzd2joinabl1199z62zz__threadz00(obj_t BgL_envz00_3691, obj_t BgL_thz00_3692)
{
{ /* Llib/thread.scm 395 */
{ /* Llib/thread.scm 395 */
 BgL_threadz00_bglt BgL_thz00_6808;
{ /* Llib/thread.scm 395 */
 bool_t BgL_test3595z00_11021;
{ /* Llib/thread.scm 395 */
 obj_t BgL_classz00_6768;
BgL_classz00_6768 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3692))
{ /* Llib/thread.scm 395 */
 BgL_objectz00_bglt BgL_arg1932z00_6770; long BgL_arg1933z00_6771;
BgL_arg1932z00_6770 = 
(BgL_objectz00_bglt)(BgL_thz00_3692); 
BgL_arg1933z00_6771 = 
BGL_CLASS_DEPTH(BgL_classz00_6768); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 395 */
 long BgL_idxz00_6779;
BgL_idxz00_6779 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6770); 
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1923z00_6780;
{ /* Llib/thread.scm 395 */
 long BgL_arg1924z00_6781;
BgL_arg1924z00_6781 = 
(BgL_idxz00_6779+BgL_arg1933z00_6771); 
BgL_arg1923z00_6780 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6781); } 
BgL_test3595z00_11021 = 
(BgL_arg1923z00_6780==BgL_classz00_6768); } }  else 
{ /* Llib/thread.scm 395 */
 bool_t BgL_res2044z00_6786;
{ /* Llib/thread.scm 395 */
 obj_t BgL_oclassz00_6790;
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1937z00_6792; long BgL_arg1938z00_6793;
BgL_arg1937z00_6792 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 395 */
 long BgL_arg1939z00_6794;
BgL_arg1939z00_6794 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6770); 
BgL_arg1938z00_6793 = 
(BgL_arg1939z00_6794-OBJECT_TYPE); } 
BgL_oclassz00_6790 = 
VECTOR_REF(BgL_arg1937z00_6792,BgL_arg1938z00_6793); } 
{ /* Llib/thread.scm 395 */
 bool_t BgL__ortest_1147z00_6800;
BgL__ortest_1147z00_6800 = 
(BgL_classz00_6768==BgL_oclassz00_6790); 
if(BgL__ortest_1147z00_6800)
{ /* Llib/thread.scm 395 */
BgL_res2044z00_6786 = BgL__ortest_1147z00_6800; }  else 
{ /* Llib/thread.scm 395 */
 long BgL_odepthz00_6801;
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1927z00_6802;
BgL_arg1927z00_6802 = 
(BgL_oclassz00_6790); 
BgL_odepthz00_6801 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6802); } 
if(
(BgL_arg1933z00_6771<BgL_odepthz00_6801))
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1925z00_6806;
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1926z00_6807;
BgL_arg1926z00_6807 = 
(BgL_oclassz00_6790); 
BgL_arg1925z00_6806 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6807, BgL_arg1933z00_6771); } 
BgL_res2044z00_6786 = 
(BgL_arg1925z00_6806==BgL_classz00_6768); }  else 
{ /* Llib/thread.scm 395 */
BgL_res2044z00_6786 = ((bool_t)0); } } } } 
BgL_test3595z00_11021 = BgL_res2044z00_6786; } }  else 
{ /* Llib/thread.scm 395 */
BgL_test3595z00_11021 = ((bool_t)0)
; } } 
if(BgL_test3595z00_11021)
{ /* Llib/thread.scm 395 */
BgL_thz00_6808 = 
((BgL_threadz00_bglt)BgL_thz00_3692); }  else 
{ 
 obj_t BgL_auxz00_11046;
BgL_auxz00_11046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16418L), BGl_string2652z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3692); 
FAILURE(BgL_auxz00_11046,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2651z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6808));} } 

}



/* &thread-start!1197 */
obj_t BGl_z62threadzd2startz121197za2zz__threadz00(obj_t BgL_envz00_3693, obj_t BgL_thz00_3694, obj_t BgL_scz00_3695)
{
{ /* Llib/thread.scm 390 */
{ /* Llib/thread.scm 390 */
 BgL_threadz00_bglt BgL_thz00_6849;
{ /* Llib/thread.scm 390 */
 bool_t BgL_test3600z00_11052;
{ /* Llib/thread.scm 390 */
 obj_t BgL_classz00_6809;
BgL_classz00_6809 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3694))
{ /* Llib/thread.scm 390 */
 BgL_objectz00_bglt BgL_arg1932z00_6811; long BgL_arg1933z00_6812;
BgL_arg1932z00_6811 = 
(BgL_objectz00_bglt)(BgL_thz00_3694); 
BgL_arg1933z00_6812 = 
BGL_CLASS_DEPTH(BgL_classz00_6809); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 390 */
 long BgL_idxz00_6820;
BgL_idxz00_6820 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6811); 
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1923z00_6821;
{ /* Llib/thread.scm 390 */
 long BgL_arg1924z00_6822;
BgL_arg1924z00_6822 = 
(BgL_idxz00_6820+BgL_arg1933z00_6812); 
BgL_arg1923z00_6821 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6822); } 
BgL_test3600z00_11052 = 
(BgL_arg1923z00_6821==BgL_classz00_6809); } }  else 
{ /* Llib/thread.scm 390 */
 bool_t BgL_res2044z00_6827;
{ /* Llib/thread.scm 390 */
 obj_t BgL_oclassz00_6831;
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1937z00_6833; long BgL_arg1938z00_6834;
BgL_arg1937z00_6833 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 390 */
 long BgL_arg1939z00_6835;
BgL_arg1939z00_6835 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6811); 
BgL_arg1938z00_6834 = 
(BgL_arg1939z00_6835-OBJECT_TYPE); } 
BgL_oclassz00_6831 = 
VECTOR_REF(BgL_arg1937z00_6833,BgL_arg1938z00_6834); } 
{ /* Llib/thread.scm 390 */
 bool_t BgL__ortest_1147z00_6841;
BgL__ortest_1147z00_6841 = 
(BgL_classz00_6809==BgL_oclassz00_6831); 
if(BgL__ortest_1147z00_6841)
{ /* Llib/thread.scm 390 */
BgL_res2044z00_6827 = BgL__ortest_1147z00_6841; }  else 
{ /* Llib/thread.scm 390 */
 long BgL_odepthz00_6842;
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1927z00_6843;
BgL_arg1927z00_6843 = 
(BgL_oclassz00_6831); 
BgL_odepthz00_6842 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6843); } 
if(
(BgL_arg1933z00_6812<BgL_odepthz00_6842))
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1925z00_6847;
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1926z00_6848;
BgL_arg1926z00_6848 = 
(BgL_oclassz00_6831); 
BgL_arg1925z00_6847 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6848, BgL_arg1933z00_6812); } 
BgL_res2044z00_6827 = 
(BgL_arg1925z00_6847==BgL_classz00_6809); }  else 
{ /* Llib/thread.scm 390 */
BgL_res2044z00_6827 = ((bool_t)0); } } } } 
BgL_test3600z00_11052 = BgL_res2044z00_6827; } }  else 
{ /* Llib/thread.scm 390 */
BgL_test3600z00_11052 = ((bool_t)0)
; } } 
if(BgL_test3600z00_11052)
{ /* Llib/thread.scm 390 */
BgL_thz00_6849 = 
((BgL_threadz00_bglt)BgL_thz00_3694); }  else 
{ 
 obj_t BgL_auxz00_11077;
BgL_auxz00_11077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16146L), BGl_string2654z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3694); 
FAILURE(BgL_auxz00_11077,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2653z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_thz00_6849));} } 

}



/* &thread-initialize!1195 */
obj_t BGl_z62threadzd2initializa7ez121195z05zz__threadz00(obj_t BgL_envz00_3696, obj_t BgL_thz00_3697)
{
{ /* Llib/thread.scm 384 */
{ 
 BgL_threadz00_bglt BgL_auxz00_11083;
{ /* Llib/thread.scm 384 */
 BgL_threadz00_bglt BgL_thz00_6890;
{ /* Llib/thread.scm 384 */
 bool_t BgL_test3605z00_11084;
{ /* Llib/thread.scm 384 */
 obj_t BgL_classz00_6850;
BgL_classz00_6850 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3697))
{ /* Llib/thread.scm 384 */
 BgL_objectz00_bglt BgL_arg1932z00_6852; long BgL_arg1933z00_6853;
BgL_arg1932z00_6852 = 
(BgL_objectz00_bglt)(BgL_thz00_3697); 
BgL_arg1933z00_6853 = 
BGL_CLASS_DEPTH(BgL_classz00_6850); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 384 */
 long BgL_idxz00_6861;
BgL_idxz00_6861 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6852); 
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1923z00_6862;
{ /* Llib/thread.scm 384 */
 long BgL_arg1924z00_6863;
BgL_arg1924z00_6863 = 
(BgL_idxz00_6861+BgL_arg1933z00_6853); 
BgL_arg1923z00_6862 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6863); } 
BgL_test3605z00_11084 = 
(BgL_arg1923z00_6862==BgL_classz00_6850); } }  else 
{ /* Llib/thread.scm 384 */
 bool_t BgL_res2044z00_6868;
{ /* Llib/thread.scm 384 */
 obj_t BgL_oclassz00_6872;
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1937z00_6874; long BgL_arg1938z00_6875;
BgL_arg1937z00_6874 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 384 */
 long BgL_arg1939z00_6876;
BgL_arg1939z00_6876 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6852); 
BgL_arg1938z00_6875 = 
(BgL_arg1939z00_6876-OBJECT_TYPE); } 
BgL_oclassz00_6872 = 
VECTOR_REF(BgL_arg1937z00_6874,BgL_arg1938z00_6875); } 
{ /* Llib/thread.scm 384 */
 bool_t BgL__ortest_1147z00_6882;
BgL__ortest_1147z00_6882 = 
(BgL_classz00_6850==BgL_oclassz00_6872); 
if(BgL__ortest_1147z00_6882)
{ /* Llib/thread.scm 384 */
BgL_res2044z00_6868 = BgL__ortest_1147z00_6882; }  else 
{ /* Llib/thread.scm 384 */
 long BgL_odepthz00_6883;
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1927z00_6884;
BgL_arg1927z00_6884 = 
(BgL_oclassz00_6872); 
BgL_odepthz00_6883 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6884); } 
if(
(BgL_arg1933z00_6853<BgL_odepthz00_6883))
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1925z00_6888;
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1926z00_6889;
BgL_arg1926z00_6889 = 
(BgL_oclassz00_6872); 
BgL_arg1925z00_6888 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6889, BgL_arg1933z00_6853); } 
BgL_res2044z00_6868 = 
(BgL_arg1925z00_6888==BgL_classz00_6850); }  else 
{ /* Llib/thread.scm 384 */
BgL_res2044z00_6868 = ((bool_t)0); } } } } 
BgL_test3605z00_11084 = BgL_res2044z00_6868; } }  else 
{ /* Llib/thread.scm 384 */
BgL_test3605z00_11084 = ((bool_t)0)
; } } 
if(BgL_test3605z00_11084)
{ /* Llib/thread.scm 384 */
BgL_thz00_6890 = 
((BgL_threadz00_bglt)BgL_thz00_3697); }  else 
{ 
 obj_t BgL_auxz00_11109;
BgL_auxz00_11109 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(15868L), BGl_string2655z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3697); 
FAILURE(BgL_auxz00_11109,BFALSE,BFALSE);} } 
BgL_auxz00_11083 = BgL_thz00_6890; } 
return 
((obj_t)BgL_auxz00_11083);} } 

}



/* &tb-condvar-initializ1187 */
obj_t BGl_z62tbzd2condvarzd2initializa71187zc5zz__threadz00(obj_t BgL_envz00_3698, obj_t BgL_tbz00_3699, obj_t BgL_condvarz00_3700)
{
{ /* Llib/thread.scm 355 */
{ /* Llib/thread.scm 355 */
 BgL_threadzd2backendzd2_bglt BgL_tbz00_6931; obj_t BgL_condvarz00_6932;
{ /* Llib/thread.scm 355 */
 bool_t BgL_test3610z00_11114;
{ /* Llib/thread.scm 355 */
 obj_t BgL_classz00_6891;
BgL_classz00_6891 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3699))
{ /* Llib/thread.scm 355 */
 BgL_objectz00_bglt BgL_arg1932z00_6893; long BgL_arg1933z00_6894;
BgL_arg1932z00_6893 = 
(BgL_objectz00_bglt)(BgL_tbz00_3699); 
BgL_arg1933z00_6894 = 
BGL_CLASS_DEPTH(BgL_classz00_6891); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 355 */
 long BgL_idxz00_6902;
BgL_idxz00_6902 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6893); 
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1923z00_6903;
{ /* Llib/thread.scm 355 */
 long BgL_arg1924z00_6904;
BgL_arg1924z00_6904 = 
(BgL_idxz00_6902+BgL_arg1933z00_6894); 
BgL_arg1923z00_6903 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6904); } 
BgL_test3610z00_11114 = 
(BgL_arg1923z00_6903==BgL_classz00_6891); } }  else 
{ /* Llib/thread.scm 355 */
 bool_t BgL_res2044z00_6909;
{ /* Llib/thread.scm 355 */
 obj_t BgL_oclassz00_6913;
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1937z00_6915; long BgL_arg1938z00_6916;
BgL_arg1937z00_6915 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 355 */
 long BgL_arg1939z00_6917;
BgL_arg1939z00_6917 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6893); 
BgL_arg1938z00_6916 = 
(BgL_arg1939z00_6917-OBJECT_TYPE); } 
BgL_oclassz00_6913 = 
VECTOR_REF(BgL_arg1937z00_6915,BgL_arg1938z00_6916); } 
{ /* Llib/thread.scm 355 */
 bool_t BgL__ortest_1147z00_6923;
BgL__ortest_1147z00_6923 = 
(BgL_classz00_6891==BgL_oclassz00_6913); 
if(BgL__ortest_1147z00_6923)
{ /* Llib/thread.scm 355 */
BgL_res2044z00_6909 = BgL__ortest_1147z00_6923; }  else 
{ /* Llib/thread.scm 355 */
 long BgL_odepthz00_6924;
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1927z00_6925;
BgL_arg1927z00_6925 = 
(BgL_oclassz00_6913); 
BgL_odepthz00_6924 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6925); } 
if(
(BgL_arg1933z00_6894<BgL_odepthz00_6924))
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1925z00_6929;
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1926z00_6930;
BgL_arg1926z00_6930 = 
(BgL_oclassz00_6913); 
BgL_arg1925z00_6929 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6930, BgL_arg1933z00_6894); } 
BgL_res2044z00_6909 = 
(BgL_arg1925z00_6929==BgL_classz00_6891); }  else 
{ /* Llib/thread.scm 355 */
BgL_res2044z00_6909 = ((bool_t)0); } } } } 
BgL_test3610z00_11114 = BgL_res2044z00_6909; } }  else 
{ /* Llib/thread.scm 355 */
BgL_test3610z00_11114 = ((bool_t)0)
; } } 
if(BgL_test3610z00_11114)
{ /* Llib/thread.scm 355 */
BgL_tbz00_6931 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3699); }  else 
{ 
 obj_t BgL_auxz00_11139;
BgL_auxz00_11139 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14317L), BGl_string2657z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3699); 
FAILURE(BgL_auxz00_11139,BFALSE,BFALSE);} } 
if(
BGL_CONDVARP(BgL_condvarz00_3700))
{ /* Llib/thread.scm 355 */
BgL_condvarz00_6932 = BgL_condvarz00_3700; }  else 
{ 
 obj_t BgL_auxz00_11145;
BgL_auxz00_11145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14317L), BGl_string2657z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_condvarz00_3700); 
FAILURE(BgL_auxz00_11145,BFALSE,BFALSE);} 
return 
BGl_errorz00zz__errorz00(BGl_symbol2656z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_tbz00_6931));} } 

}



/* &tb-mutex-initialize!1185 */
obj_t BGl_z62tbzd2mutexzd2initializa7ez121185zd7zz__threadz00(obj_t BgL_envz00_3701, obj_t BgL_tbz00_3702, obj_t BgL_mutexz00_3703)
{
{ /* Llib/thread.scm 350 */
{ /* Llib/thread.scm 350 */
 BgL_threadzd2backendzd2_bglt BgL_tbz00_6973; obj_t BgL_mutexz00_6974;
{ /* Llib/thread.scm 350 */
 bool_t BgL_test3616z00_11151;
{ /* Llib/thread.scm 350 */
 obj_t BgL_classz00_6933;
BgL_classz00_6933 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3702))
{ /* Llib/thread.scm 350 */
 BgL_objectz00_bglt BgL_arg1932z00_6935; long BgL_arg1933z00_6936;
BgL_arg1932z00_6935 = 
(BgL_objectz00_bglt)(BgL_tbz00_3702); 
BgL_arg1933z00_6936 = 
BGL_CLASS_DEPTH(BgL_classz00_6933); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 350 */
 long BgL_idxz00_6944;
BgL_idxz00_6944 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6935); 
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1923z00_6945;
{ /* Llib/thread.scm 350 */
 long BgL_arg1924z00_6946;
BgL_arg1924z00_6946 = 
(BgL_idxz00_6944+BgL_arg1933z00_6936); 
BgL_arg1923z00_6945 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6946); } 
BgL_test3616z00_11151 = 
(BgL_arg1923z00_6945==BgL_classz00_6933); } }  else 
{ /* Llib/thread.scm 350 */
 bool_t BgL_res2044z00_6951;
{ /* Llib/thread.scm 350 */
 obj_t BgL_oclassz00_6955;
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1937z00_6957; long BgL_arg1938z00_6958;
BgL_arg1937z00_6957 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 350 */
 long BgL_arg1939z00_6959;
BgL_arg1939z00_6959 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6935); 
BgL_arg1938z00_6958 = 
(BgL_arg1939z00_6959-OBJECT_TYPE); } 
BgL_oclassz00_6955 = 
VECTOR_REF(BgL_arg1937z00_6957,BgL_arg1938z00_6958); } 
{ /* Llib/thread.scm 350 */
 bool_t BgL__ortest_1147z00_6965;
BgL__ortest_1147z00_6965 = 
(BgL_classz00_6933==BgL_oclassz00_6955); 
if(BgL__ortest_1147z00_6965)
{ /* Llib/thread.scm 350 */
BgL_res2044z00_6951 = BgL__ortest_1147z00_6965; }  else 
{ /* Llib/thread.scm 350 */
 long BgL_odepthz00_6966;
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1927z00_6967;
BgL_arg1927z00_6967 = 
(BgL_oclassz00_6955); 
BgL_odepthz00_6966 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_6967); } 
if(
(BgL_arg1933z00_6936<BgL_odepthz00_6966))
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1925z00_6971;
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1926z00_6972;
BgL_arg1926z00_6972 = 
(BgL_oclassz00_6955); 
BgL_arg1925z00_6971 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_6972, BgL_arg1933z00_6936); } 
BgL_res2044z00_6951 = 
(BgL_arg1925z00_6971==BgL_classz00_6933); }  else 
{ /* Llib/thread.scm 350 */
BgL_res2044z00_6951 = ((bool_t)0); } } } } 
BgL_test3616z00_11151 = BgL_res2044z00_6951; } }  else 
{ /* Llib/thread.scm 350 */
BgL_test3616z00_11151 = ((bool_t)0)
; } } 
if(BgL_test3616z00_11151)
{ /* Llib/thread.scm 350 */
BgL_tbz00_6973 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3702); }  else 
{ 
 obj_t BgL_auxz00_11176;
BgL_auxz00_11176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14029L), BGl_string2659z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3702); 
FAILURE(BgL_auxz00_11176,BFALSE,BFALSE);} } 
if(
BGL_MUTEXP(BgL_mutexz00_3703))
{ /* Llib/thread.scm 350 */
BgL_mutexz00_6974 = BgL_mutexz00_3703; }  else 
{ 
 obj_t BgL_auxz00_11182;
BgL_auxz00_11182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14029L), BGl_string2659z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_mutexz00_3703); 
FAILURE(BgL_auxz00_11182,BFALSE,BFALSE);} 
return 
BGl_errorz00zz__errorz00(BGl_symbol2658z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_tbz00_6973));} } 

}



/* &tb-current-thread1179 */
obj_t BGl_z62tbzd2currentzd2thread1179z62zz__threadz00(obj_t BgL_envz00_3704, obj_t BgL_tbz00_3705)
{
{ /* Llib/thread.scm 326 */
{ /* Llib/thread.scm 326 */
 BgL_threadzd2backendzd2_bglt BgL_tbz00_7015;
{ /* Llib/thread.scm 326 */
 bool_t BgL_test3622z00_11188;
{ /* Llib/thread.scm 326 */
 obj_t BgL_classz00_6975;
BgL_classz00_6975 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3705))
{ /* Llib/thread.scm 326 */
 BgL_objectz00_bglt BgL_arg1932z00_6977; long BgL_arg1933z00_6978;
BgL_arg1932z00_6977 = 
(BgL_objectz00_bglt)(BgL_tbz00_3705); 
BgL_arg1933z00_6978 = 
BGL_CLASS_DEPTH(BgL_classz00_6975); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 326 */
 long BgL_idxz00_6986;
BgL_idxz00_6986 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_6977); 
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1923z00_6987;
{ /* Llib/thread.scm 326 */
 long BgL_arg1924z00_6988;
BgL_arg1924z00_6988 = 
(BgL_idxz00_6986+BgL_arg1933z00_6978); 
BgL_arg1923z00_6987 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_6988); } 
BgL_test3622z00_11188 = 
(BgL_arg1923z00_6987==BgL_classz00_6975); } }  else 
{ /* Llib/thread.scm 326 */
 bool_t BgL_res2044z00_6993;
{ /* Llib/thread.scm 326 */
 obj_t BgL_oclassz00_6997;
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1937z00_6999; long BgL_arg1938z00_7000;
BgL_arg1937z00_6999 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 326 */
 long BgL_arg1939z00_7001;
BgL_arg1939z00_7001 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_6977); 
BgL_arg1938z00_7000 = 
(BgL_arg1939z00_7001-OBJECT_TYPE); } 
BgL_oclassz00_6997 = 
VECTOR_REF(BgL_arg1937z00_6999,BgL_arg1938z00_7000); } 
{ /* Llib/thread.scm 326 */
 bool_t BgL__ortest_1147z00_7007;
BgL__ortest_1147z00_7007 = 
(BgL_classz00_6975==BgL_oclassz00_6997); 
if(BgL__ortest_1147z00_7007)
{ /* Llib/thread.scm 326 */
BgL_res2044z00_6993 = BgL__ortest_1147z00_7007; }  else 
{ /* Llib/thread.scm 326 */
 long BgL_odepthz00_7008;
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1927z00_7009;
BgL_arg1927z00_7009 = 
(BgL_oclassz00_6997); 
BgL_odepthz00_7008 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7009); } 
if(
(BgL_arg1933z00_6978<BgL_odepthz00_7008))
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1925z00_7013;
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1926z00_7014;
BgL_arg1926z00_7014 = 
(BgL_oclassz00_6997); 
BgL_arg1925z00_7013 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7014, BgL_arg1933z00_6978); } 
BgL_res2044z00_6993 = 
(BgL_arg1925z00_7013==BgL_classz00_6975); }  else 
{ /* Llib/thread.scm 326 */
BgL_res2044z00_6993 = ((bool_t)0); } } } } 
BgL_test3622z00_11188 = BgL_res2044z00_6993; } }  else 
{ /* Llib/thread.scm 326 */
BgL_test3622z00_11188 = ((bool_t)0)
; } } 
if(BgL_test3622z00_11188)
{ /* Llib/thread.scm 326 */
BgL_tbz00_7015 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3705); }  else 
{ 
 obj_t BgL_auxz00_11213;
BgL_auxz00_11213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12844L), BGl_string2661z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3705); 
FAILURE(BgL_auxz00_11213,BFALSE,BFALSE);} } 
return 
BGl_errorz00zz__errorz00(BGl_symbol2660z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_tbz00_7015));} } 

}



/* &tb-make-thread1177 */
obj_t BGl_z62tbzd2makezd2thread1177z62zz__threadz00(obj_t BgL_envz00_3706, obj_t BgL_tbz00_3707, obj_t BgL_bodyz00_3708, obj_t BgL_namez00_3709)
{
{ /* Llib/thread.scm 321 */
{ /* Llib/thread.scm 321 */
 BgL_threadzd2backendzd2_bglt BgL_tbz00_7056; obj_t BgL_bodyz00_7057;
{ /* Llib/thread.scm 321 */
 bool_t BgL_test3627z00_11219;
{ /* Llib/thread.scm 321 */
 obj_t BgL_classz00_7016;
BgL_classz00_7016 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3707))
{ /* Llib/thread.scm 321 */
 BgL_objectz00_bglt BgL_arg1932z00_7018; long BgL_arg1933z00_7019;
BgL_arg1932z00_7018 = 
(BgL_objectz00_bglt)(BgL_tbz00_3707); 
BgL_arg1933z00_7019 = 
BGL_CLASS_DEPTH(BgL_classz00_7016); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 321 */
 long BgL_idxz00_7027;
BgL_idxz00_7027 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7018); 
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1923z00_7028;
{ /* Llib/thread.scm 321 */
 long BgL_arg1924z00_7029;
BgL_arg1924z00_7029 = 
(BgL_idxz00_7027+BgL_arg1933z00_7019); 
BgL_arg1923z00_7028 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7029); } 
BgL_test3627z00_11219 = 
(BgL_arg1923z00_7028==BgL_classz00_7016); } }  else 
{ /* Llib/thread.scm 321 */
 bool_t BgL_res2044z00_7034;
{ /* Llib/thread.scm 321 */
 obj_t BgL_oclassz00_7038;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1937z00_7040; long BgL_arg1938z00_7041;
BgL_arg1937z00_7040 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 321 */
 long BgL_arg1939z00_7042;
BgL_arg1939z00_7042 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7018); 
BgL_arg1938z00_7041 = 
(BgL_arg1939z00_7042-OBJECT_TYPE); } 
BgL_oclassz00_7038 = 
VECTOR_REF(BgL_arg1937z00_7040,BgL_arg1938z00_7041); } 
{ /* Llib/thread.scm 321 */
 bool_t BgL__ortest_1147z00_7048;
BgL__ortest_1147z00_7048 = 
(BgL_classz00_7016==BgL_oclassz00_7038); 
if(BgL__ortest_1147z00_7048)
{ /* Llib/thread.scm 321 */
BgL_res2044z00_7034 = BgL__ortest_1147z00_7048; }  else 
{ /* Llib/thread.scm 321 */
 long BgL_odepthz00_7049;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1927z00_7050;
BgL_arg1927z00_7050 = 
(BgL_oclassz00_7038); 
BgL_odepthz00_7049 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7050); } 
if(
(BgL_arg1933z00_7019<BgL_odepthz00_7049))
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1925z00_7054;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1926z00_7055;
BgL_arg1926z00_7055 = 
(BgL_oclassz00_7038); 
BgL_arg1925z00_7054 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7055, BgL_arg1933z00_7019); } 
BgL_res2044z00_7034 = 
(BgL_arg1925z00_7054==BgL_classz00_7016); }  else 
{ /* Llib/thread.scm 321 */
BgL_res2044z00_7034 = ((bool_t)0); } } } } 
BgL_test3627z00_11219 = BgL_res2044z00_7034; } }  else 
{ /* Llib/thread.scm 321 */
BgL_test3627z00_11219 = ((bool_t)0)
; } } 
if(BgL_test3627z00_11219)
{ /* Llib/thread.scm 321 */
BgL_tbz00_7056 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3707); }  else 
{ 
 obj_t BgL_auxz00_11244;
BgL_auxz00_11244 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12558L), BGl_string2663z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3707); 
FAILURE(BgL_auxz00_11244,BFALSE,BFALSE);} } 
if(
PROCEDUREP(BgL_bodyz00_3708))
{ /* Llib/thread.scm 321 */
BgL_bodyz00_7057 = BgL_bodyz00_3708; }  else 
{ 
 obj_t BgL_auxz00_11250;
BgL_auxz00_11250 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12558L), BGl_string2663z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_bodyz00_3708); 
FAILURE(BgL_auxz00_11250,BFALSE,BFALSE);} 
return 
BGl_errorz00zz__errorz00(BGl_symbol2662z00zz__threadz00, BGl_string2633z00zz__threadz00, 
((obj_t)BgL_tbz00_7056));} } 

}



/* tb-make-thread */
BGL_EXPORTED_DEF BgL_threadz00_bglt BGl_tbzd2makezd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt BgL_tbz00_7, obj_t BgL_bodyz00_8, obj_t BgL_namez00_9)
{
{ /* Llib/thread.scm 321 */
{ /* Llib/thread.scm 321 */
 obj_t BgL_method1178z00_1651;
{ /* Llib/thread.scm 321 */
 obj_t BgL_res1951z00_2514;
{ /* Llib/thread.scm 321 */
 long BgL_objzd2classzd2numz00_2485;
BgL_objzd2classzd2numz00_2485 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_tbz00_7)); 
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1934z00_2486;
BgL_arg1934z00_2486 = 
PROCEDURE_REF(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 321 */
 int BgL_offsetz00_2489;
BgL_offsetz00_2489 = 
(int)(BgL_objzd2classzd2numz00_2485); 
{ /* Llib/thread.scm 321 */
 long BgL_offsetz00_2490;
BgL_offsetz00_2490 = 
(
(long)(BgL_offsetz00_2489)-OBJECT_TYPE); 
{ /* Llib/thread.scm 321 */
 long BgL_modz00_2491;
BgL_modz00_2491 = 
(BgL_offsetz00_2490 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 321 */
 long BgL_restz00_2493;
BgL_restz00_2493 = 
(BgL_offsetz00_2490 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 321 */

{ /* Llib/thread.scm 321 */
 obj_t BgL_bucketz00_2495;
BgL_bucketz00_2495 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2486),BgL_modz00_2491); 
BgL_res1951z00_2514 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2495),BgL_restz00_2493); } } } } } } } } 
BgL_method1178z00_1651 = BgL_res1951z00_2514; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1178z00_1651, 3))
{ /* Llib/thread.scm 321 */
 obj_t BgL_aux2280z00_4173;
BgL_aux2280z00_4173 = 
BGL_PROCEDURE_CALL3(BgL_method1178z00_1651, 
((obj_t)BgL_tbz00_7), BgL_bodyz00_8, BgL_namez00_9); 
{ /* Llib/thread.scm 321 */
 bool_t BgL_test3634z00_11290;
{ /* Llib/thread.scm 321 */
 obj_t BgL_classz00_7058;
BgL_classz00_7058 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_aux2280z00_4173))
{ /* Llib/thread.scm 321 */
 BgL_objectz00_bglt BgL_arg1932z00_7060; long BgL_arg1933z00_7061;
BgL_arg1932z00_7060 = 
(BgL_objectz00_bglt)(BgL_aux2280z00_4173); 
BgL_arg1933z00_7061 = 
BGL_CLASS_DEPTH(BgL_classz00_7058); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 321 */
 long BgL_idxz00_7069;
BgL_idxz00_7069 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7060); 
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1923z00_7070;
{ /* Llib/thread.scm 321 */
 long BgL_arg1924z00_7071;
BgL_arg1924z00_7071 = 
(BgL_idxz00_7069+BgL_arg1933z00_7061); 
BgL_arg1923z00_7070 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7071); } 
BgL_test3634z00_11290 = 
(BgL_arg1923z00_7070==BgL_classz00_7058); } }  else 
{ /* Llib/thread.scm 321 */
 bool_t BgL_res2044z00_7076;
{ /* Llib/thread.scm 321 */
 obj_t BgL_oclassz00_7080;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1937z00_7082; long BgL_arg1938z00_7083;
BgL_arg1937z00_7082 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 321 */
 long BgL_arg1939z00_7084;
BgL_arg1939z00_7084 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7060); 
BgL_arg1938z00_7083 = 
(BgL_arg1939z00_7084-OBJECT_TYPE); } 
BgL_oclassz00_7080 = 
VECTOR_REF(BgL_arg1937z00_7082,BgL_arg1938z00_7083); } 
{ /* Llib/thread.scm 321 */
 bool_t BgL__ortest_1147z00_7090;
BgL__ortest_1147z00_7090 = 
(BgL_classz00_7058==BgL_oclassz00_7080); 
if(BgL__ortest_1147z00_7090)
{ /* Llib/thread.scm 321 */
BgL_res2044z00_7076 = BgL__ortest_1147z00_7090; }  else 
{ /* Llib/thread.scm 321 */
 long BgL_odepthz00_7091;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1927z00_7092;
BgL_arg1927z00_7092 = 
(BgL_oclassz00_7080); 
BgL_odepthz00_7091 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7092); } 
if(
(BgL_arg1933z00_7061<BgL_odepthz00_7091))
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1925z00_7096;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1926z00_7097;
BgL_arg1926z00_7097 = 
(BgL_oclassz00_7080); 
BgL_arg1925z00_7096 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7097, BgL_arg1933z00_7061); } 
BgL_res2044z00_7076 = 
(BgL_arg1925z00_7096==BgL_classz00_7058); }  else 
{ /* Llib/thread.scm 321 */
BgL_res2044z00_7076 = ((bool_t)0); } } } } 
BgL_test3634z00_11290 = BgL_res2044z00_7076; } }  else 
{ /* Llib/thread.scm 321 */
BgL_test3634z00_11290 = ((bool_t)0)
; } } 
if(BgL_test3634z00_11290)
{ /* Llib/thread.scm 321 */
return 
((BgL_threadz00_bglt)BgL_aux2280z00_4173);}  else 
{ 
 obj_t BgL_auxz00_11315;
BgL_auxz00_11315 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12558L), BGl_string2664z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_aux2280z00_4173); 
FAILURE(BgL_auxz00_11315,BFALSE,BFALSE);} } }  else 
{ /* Llib/thread.scm 321 */
FAILURE(BGl_string2665z00zz__threadz00,BGl_list2666z00zz__threadz00,BgL_method1178z00_1651);} } } 

}



/* &tb-make-thread */
BgL_threadz00_bglt BGl_z62tbzd2makezd2threadz62zz__threadz00(obj_t BgL_envz00_3710, obj_t BgL_tbz00_3711, obj_t BgL_bodyz00_3712, obj_t BgL_namez00_3713)
{
{ /* Llib/thread.scm 321 */
{ /* Llib/thread.scm 321 */
 obj_t BgL_auxz00_11350; BgL_threadzd2backendzd2_bglt BgL_auxz00_11320;
if(
PROCEDUREP(BgL_bodyz00_3712))
{ /* Llib/thread.scm 321 */
BgL_auxz00_11350 = BgL_bodyz00_3712
; }  else 
{ 
 obj_t BgL_auxz00_11353;
BgL_auxz00_11353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12558L), BGl_string2671z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_bodyz00_3712); 
FAILURE(BgL_auxz00_11353,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 321 */
 bool_t BgL_test3639z00_11321;
{ /* Llib/thread.scm 321 */
 obj_t BgL_classz00_7098;
BgL_classz00_7098 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3711))
{ /* Llib/thread.scm 321 */
 BgL_objectz00_bglt BgL_arg1932z00_7100; long BgL_arg1933z00_7101;
BgL_arg1932z00_7100 = 
(BgL_objectz00_bglt)(BgL_tbz00_3711); 
BgL_arg1933z00_7101 = 
BGL_CLASS_DEPTH(BgL_classz00_7098); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 321 */
 long BgL_idxz00_7109;
BgL_idxz00_7109 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7100); 
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1923z00_7110;
{ /* Llib/thread.scm 321 */
 long BgL_arg1924z00_7111;
BgL_arg1924z00_7111 = 
(BgL_idxz00_7109+BgL_arg1933z00_7101); 
BgL_arg1923z00_7110 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7111); } 
BgL_test3639z00_11321 = 
(BgL_arg1923z00_7110==BgL_classz00_7098); } }  else 
{ /* Llib/thread.scm 321 */
 bool_t BgL_res2044z00_7116;
{ /* Llib/thread.scm 321 */
 obj_t BgL_oclassz00_7120;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1937z00_7122; long BgL_arg1938z00_7123;
BgL_arg1937z00_7122 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 321 */
 long BgL_arg1939z00_7124;
BgL_arg1939z00_7124 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7100); 
BgL_arg1938z00_7123 = 
(BgL_arg1939z00_7124-OBJECT_TYPE); } 
BgL_oclassz00_7120 = 
VECTOR_REF(BgL_arg1937z00_7122,BgL_arg1938z00_7123); } 
{ /* Llib/thread.scm 321 */
 bool_t BgL__ortest_1147z00_7130;
BgL__ortest_1147z00_7130 = 
(BgL_classz00_7098==BgL_oclassz00_7120); 
if(BgL__ortest_1147z00_7130)
{ /* Llib/thread.scm 321 */
BgL_res2044z00_7116 = BgL__ortest_1147z00_7130; }  else 
{ /* Llib/thread.scm 321 */
 long BgL_odepthz00_7131;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1927z00_7132;
BgL_arg1927z00_7132 = 
(BgL_oclassz00_7120); 
BgL_odepthz00_7131 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7132); } 
if(
(BgL_arg1933z00_7101<BgL_odepthz00_7131))
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1925z00_7136;
{ /* Llib/thread.scm 321 */
 obj_t BgL_arg1926z00_7137;
BgL_arg1926z00_7137 = 
(BgL_oclassz00_7120); 
BgL_arg1925z00_7136 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7137, BgL_arg1933z00_7101); } 
BgL_res2044z00_7116 = 
(BgL_arg1925z00_7136==BgL_classz00_7098); }  else 
{ /* Llib/thread.scm 321 */
BgL_res2044z00_7116 = ((bool_t)0); } } } } 
BgL_test3639z00_11321 = BgL_res2044z00_7116; } }  else 
{ /* Llib/thread.scm 321 */
BgL_test3639z00_11321 = ((bool_t)0)
; } } 
if(BgL_test3639z00_11321)
{ /* Llib/thread.scm 321 */
BgL_auxz00_11320 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3711)
; }  else 
{ 
 obj_t BgL_auxz00_11346;
BgL_auxz00_11346 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12558L), BGl_string2671z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3711); 
FAILURE(BgL_auxz00_11346,BFALSE,BFALSE);} } 
return 
BGl_tbzd2makezd2threadz00zz__threadz00(BgL_auxz00_11320, BgL_auxz00_11350, BgL_namez00_3713);} } 

}



/* tb-current-thread */
BGL_EXPORTED_DEF obj_t BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_threadzd2backendzd2_bglt BgL_tbz00_10)
{
{ /* Llib/thread.scm 326 */
{ /* Llib/thread.scm 326 */
 obj_t BgL_method1180z00_1652;
{ /* Llib/thread.scm 326 */
 obj_t BgL_res1956z00_2545;
{ /* Llib/thread.scm 326 */
 long BgL_objzd2classzd2numz00_2516;
BgL_objzd2classzd2numz00_2516 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_tbz00_10)); 
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1934z00_2517;
BgL_arg1934z00_2517 = 
PROCEDURE_REF(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 326 */
 int BgL_offsetz00_2520;
BgL_offsetz00_2520 = 
(int)(BgL_objzd2classzd2numz00_2516); 
{ /* Llib/thread.scm 326 */
 long BgL_offsetz00_2521;
BgL_offsetz00_2521 = 
(
(long)(BgL_offsetz00_2520)-OBJECT_TYPE); 
{ /* Llib/thread.scm 326 */
 long BgL_modz00_2522;
BgL_modz00_2522 = 
(BgL_offsetz00_2521 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 326 */
 long BgL_restz00_2524;
BgL_restz00_2524 = 
(BgL_offsetz00_2521 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 326 */

{ /* Llib/thread.scm 326 */
 obj_t BgL_bucketz00_2526;
BgL_bucketz00_2526 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2517),BgL_modz00_2522); 
BgL_res1956z00_2545 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2526),BgL_restz00_2524); } } } } } } } } 
BgL_method1180z00_1652 = BgL_res1956z00_2545; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1180z00_1652, 1))
{ /* Llib/thread.scm 326 */
return 
BGL_PROCEDURE_CALL1(BgL_method1180z00_1652, 
((obj_t)BgL_tbz00_10));}  else 
{ /* Llib/thread.scm 326 */
FAILURE(BGl_string2672z00zz__threadz00,BGl_list2673z00zz__threadz00,BgL_method1180z00_1652);} } } 

}



/* &tb-current-thread */
obj_t BGl_z62tbzd2currentzd2threadz62zz__threadz00(obj_t BgL_envz00_3714, obj_t BgL_tbz00_3715)
{
{ /* Llib/thread.scm 326 */
{ /* Llib/thread.scm 326 */
 BgL_threadzd2backendzd2_bglt BgL_auxz00_11391;
{ /* Llib/thread.scm 326 */
 bool_t BgL_test3646z00_11392;
{ /* Llib/thread.scm 326 */
 obj_t BgL_classz00_7138;
BgL_classz00_7138 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3715))
{ /* Llib/thread.scm 326 */
 BgL_objectz00_bglt BgL_arg1932z00_7140; long BgL_arg1933z00_7141;
BgL_arg1932z00_7140 = 
(BgL_objectz00_bglt)(BgL_tbz00_3715); 
BgL_arg1933z00_7141 = 
BGL_CLASS_DEPTH(BgL_classz00_7138); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 326 */
 long BgL_idxz00_7149;
BgL_idxz00_7149 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7140); 
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1923z00_7150;
{ /* Llib/thread.scm 326 */
 long BgL_arg1924z00_7151;
BgL_arg1924z00_7151 = 
(BgL_idxz00_7149+BgL_arg1933z00_7141); 
BgL_arg1923z00_7150 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7151); } 
BgL_test3646z00_11392 = 
(BgL_arg1923z00_7150==BgL_classz00_7138); } }  else 
{ /* Llib/thread.scm 326 */
 bool_t BgL_res2044z00_7156;
{ /* Llib/thread.scm 326 */
 obj_t BgL_oclassz00_7160;
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1937z00_7162; long BgL_arg1938z00_7163;
BgL_arg1937z00_7162 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 326 */
 long BgL_arg1939z00_7164;
BgL_arg1939z00_7164 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7140); 
BgL_arg1938z00_7163 = 
(BgL_arg1939z00_7164-OBJECT_TYPE); } 
BgL_oclassz00_7160 = 
VECTOR_REF(BgL_arg1937z00_7162,BgL_arg1938z00_7163); } 
{ /* Llib/thread.scm 326 */
 bool_t BgL__ortest_1147z00_7170;
BgL__ortest_1147z00_7170 = 
(BgL_classz00_7138==BgL_oclassz00_7160); 
if(BgL__ortest_1147z00_7170)
{ /* Llib/thread.scm 326 */
BgL_res2044z00_7156 = BgL__ortest_1147z00_7170; }  else 
{ /* Llib/thread.scm 326 */
 long BgL_odepthz00_7171;
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1927z00_7172;
BgL_arg1927z00_7172 = 
(BgL_oclassz00_7160); 
BgL_odepthz00_7171 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7172); } 
if(
(BgL_arg1933z00_7141<BgL_odepthz00_7171))
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1925z00_7176;
{ /* Llib/thread.scm 326 */
 obj_t BgL_arg1926z00_7177;
BgL_arg1926z00_7177 = 
(BgL_oclassz00_7160); 
BgL_arg1925z00_7176 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7177, BgL_arg1933z00_7141); } 
BgL_res2044z00_7156 = 
(BgL_arg1925z00_7176==BgL_classz00_7138); }  else 
{ /* Llib/thread.scm 326 */
BgL_res2044z00_7156 = ((bool_t)0); } } } } 
BgL_test3646z00_11392 = BgL_res2044z00_7156; } }  else 
{ /* Llib/thread.scm 326 */
BgL_test3646z00_11392 = ((bool_t)0)
; } } 
if(BgL_test3646z00_11392)
{ /* Llib/thread.scm 326 */
BgL_auxz00_11391 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3715)
; }  else 
{ 
 obj_t BgL_auxz00_11417;
BgL_auxz00_11417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(12844L), BGl_string2676z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3715); 
FAILURE(BgL_auxz00_11417,BFALSE,BFALSE);} } 
return 
BGl_tbzd2currentzd2threadz00zz__threadz00(BgL_auxz00_11391);} } 

}



/* tb-mutex-initialize! */
BGL_EXPORTED_DEF obj_t BGl_tbzd2mutexzd2initializa7ez12zb5zz__threadz00(BgL_threadzd2backendzd2_bglt BgL_tbz00_15, obj_t BgL_mutexz00_16)
{
{ /* Llib/thread.scm 350 */
{ /* Llib/thread.scm 350 */
 obj_t BgL_method1186z00_1653;
{ /* Llib/thread.scm 350 */
 obj_t BgL_res1961z00_2576;
{ /* Llib/thread.scm 350 */
 long BgL_objzd2classzd2numz00_2547;
BgL_objzd2classzd2numz00_2547 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_tbz00_15)); 
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1934z00_2548;
BgL_arg1934z00_2548 = 
PROCEDURE_REF(BGl_tbzd2mutexzd2initializa7ez12zd2envz67zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 350 */
 int BgL_offsetz00_2551;
BgL_offsetz00_2551 = 
(int)(BgL_objzd2classzd2numz00_2547); 
{ /* Llib/thread.scm 350 */
 long BgL_offsetz00_2552;
BgL_offsetz00_2552 = 
(
(long)(BgL_offsetz00_2551)-OBJECT_TYPE); 
{ /* Llib/thread.scm 350 */
 long BgL_modz00_2553;
BgL_modz00_2553 = 
(BgL_offsetz00_2552 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 350 */
 long BgL_restz00_2555;
BgL_restz00_2555 = 
(BgL_offsetz00_2552 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 350 */

{ /* Llib/thread.scm 350 */
 obj_t BgL_bucketz00_2557;
BgL_bucketz00_2557 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2548),BgL_modz00_2553); 
BgL_res1961z00_2576 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2557),BgL_restz00_2555); } } } } } } } } 
BgL_method1186z00_1653 = BgL_res1961z00_2576; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1186z00_1653, 2))
{ /* Llib/thread.scm 350 */
 obj_t BgL_aux2290z00_4191;
BgL_aux2290z00_4191 = 
BGL_PROCEDURE_CALL2(BgL_method1186z00_1653, 
((obj_t)BgL_tbz00_15), BgL_mutexz00_16); 
if(
BGL_MUTEXP(BgL_aux2290z00_4191))
{ /* Llib/thread.scm 350 */
return BgL_aux2290z00_4191;}  else 
{ 
 obj_t BgL_auxz00_11457;
BgL_auxz00_11457 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14029L), BGl_string2677z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_aux2290z00_4191); 
FAILURE(BgL_auxz00_11457,BFALSE,BFALSE);} }  else 
{ /* Llib/thread.scm 350 */
FAILURE(BGl_string2678z00zz__threadz00,BGl_list2679z00zz__threadz00,BgL_method1186z00_1653);} } } 

}



/* &tb-mutex-initialize! */
obj_t BGl_z62tbzd2mutexzd2initializa7ez12zd7zz__threadz00(obj_t BgL_envz00_3716, obj_t BgL_tbz00_3717, obj_t BgL_mutexz00_3718)
{
{ /* Llib/thread.scm 350 */
{ /* Llib/thread.scm 350 */
 obj_t BgL_auxz00_11492; BgL_threadzd2backendzd2_bglt BgL_auxz00_11462;
if(
BGL_MUTEXP(BgL_mutexz00_3718))
{ /* Llib/thread.scm 350 */
BgL_auxz00_11492 = BgL_mutexz00_3718
; }  else 
{ 
 obj_t BgL_auxz00_11495;
BgL_auxz00_11495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14029L), BGl_string2682z00zz__threadz00, BGl_string2444z00zz__threadz00, BgL_mutexz00_3718); 
FAILURE(BgL_auxz00_11495,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 350 */
 bool_t BgL_test3653z00_11463;
{ /* Llib/thread.scm 350 */
 obj_t BgL_classz00_7178;
BgL_classz00_7178 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3717))
{ /* Llib/thread.scm 350 */
 BgL_objectz00_bglt BgL_arg1932z00_7180; long BgL_arg1933z00_7181;
BgL_arg1932z00_7180 = 
(BgL_objectz00_bglt)(BgL_tbz00_3717); 
BgL_arg1933z00_7181 = 
BGL_CLASS_DEPTH(BgL_classz00_7178); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 350 */
 long BgL_idxz00_7189;
BgL_idxz00_7189 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7180); 
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1923z00_7190;
{ /* Llib/thread.scm 350 */
 long BgL_arg1924z00_7191;
BgL_arg1924z00_7191 = 
(BgL_idxz00_7189+BgL_arg1933z00_7181); 
BgL_arg1923z00_7190 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7191); } 
BgL_test3653z00_11463 = 
(BgL_arg1923z00_7190==BgL_classz00_7178); } }  else 
{ /* Llib/thread.scm 350 */
 bool_t BgL_res2044z00_7196;
{ /* Llib/thread.scm 350 */
 obj_t BgL_oclassz00_7200;
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1937z00_7202; long BgL_arg1938z00_7203;
BgL_arg1937z00_7202 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 350 */
 long BgL_arg1939z00_7204;
BgL_arg1939z00_7204 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7180); 
BgL_arg1938z00_7203 = 
(BgL_arg1939z00_7204-OBJECT_TYPE); } 
BgL_oclassz00_7200 = 
VECTOR_REF(BgL_arg1937z00_7202,BgL_arg1938z00_7203); } 
{ /* Llib/thread.scm 350 */
 bool_t BgL__ortest_1147z00_7210;
BgL__ortest_1147z00_7210 = 
(BgL_classz00_7178==BgL_oclassz00_7200); 
if(BgL__ortest_1147z00_7210)
{ /* Llib/thread.scm 350 */
BgL_res2044z00_7196 = BgL__ortest_1147z00_7210; }  else 
{ /* Llib/thread.scm 350 */
 long BgL_odepthz00_7211;
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1927z00_7212;
BgL_arg1927z00_7212 = 
(BgL_oclassz00_7200); 
BgL_odepthz00_7211 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7212); } 
if(
(BgL_arg1933z00_7181<BgL_odepthz00_7211))
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1925z00_7216;
{ /* Llib/thread.scm 350 */
 obj_t BgL_arg1926z00_7217;
BgL_arg1926z00_7217 = 
(BgL_oclassz00_7200); 
BgL_arg1925z00_7216 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7217, BgL_arg1933z00_7181); } 
BgL_res2044z00_7196 = 
(BgL_arg1925z00_7216==BgL_classz00_7178); }  else 
{ /* Llib/thread.scm 350 */
BgL_res2044z00_7196 = ((bool_t)0); } } } } 
BgL_test3653z00_11463 = BgL_res2044z00_7196; } }  else 
{ /* Llib/thread.scm 350 */
BgL_test3653z00_11463 = ((bool_t)0)
; } } 
if(BgL_test3653z00_11463)
{ /* Llib/thread.scm 350 */
BgL_auxz00_11462 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3717)
; }  else 
{ 
 obj_t BgL_auxz00_11488;
BgL_auxz00_11488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14029L), BGl_string2682z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3717); 
FAILURE(BgL_auxz00_11488,BFALSE,BFALSE);} } 
return 
BGl_tbzd2mutexzd2initializa7ez12zb5zz__threadz00(BgL_auxz00_11462, BgL_auxz00_11492);} } 

}



/* tb-condvar-initialize! */
BGL_EXPORTED_DEF obj_t BGl_tbzd2condvarzd2initializa7ez12zb5zz__threadz00(BgL_threadzd2backendzd2_bglt BgL_tbz00_17, obj_t BgL_condvarz00_18)
{
{ /* Llib/thread.scm 355 */
{ /* Llib/thread.scm 355 */
 obj_t BgL_method1188z00_1654;
{ /* Llib/thread.scm 355 */
 obj_t BgL_res1966z00_2607;
{ /* Llib/thread.scm 355 */
 long BgL_objzd2classzd2numz00_2578;
BgL_objzd2classzd2numz00_2578 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_tbz00_17)); 
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1934z00_2579;
BgL_arg1934z00_2579 = 
PROCEDURE_REF(BGl_tbzd2condvarzd2initializa7ez12zd2envz67zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 355 */
 int BgL_offsetz00_2582;
BgL_offsetz00_2582 = 
(int)(BgL_objzd2classzd2numz00_2578); 
{ /* Llib/thread.scm 355 */
 long BgL_offsetz00_2583;
BgL_offsetz00_2583 = 
(
(long)(BgL_offsetz00_2582)-OBJECT_TYPE); 
{ /* Llib/thread.scm 355 */
 long BgL_modz00_2584;
BgL_modz00_2584 = 
(BgL_offsetz00_2583 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 355 */
 long BgL_restz00_2586;
BgL_restz00_2586 = 
(BgL_offsetz00_2583 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 355 */

{ /* Llib/thread.scm 355 */
 obj_t BgL_bucketz00_2588;
BgL_bucketz00_2588 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2579),BgL_modz00_2584); 
BgL_res1966z00_2607 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2588),BgL_restz00_2586); } } } } } } } } 
BgL_method1188z00_1654 = BgL_res1966z00_2607; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1188z00_1654, 2))
{ /* Llib/thread.scm 355 */
 obj_t BgL_aux2297z00_4201;
BgL_aux2297z00_4201 = 
BGL_PROCEDURE_CALL2(BgL_method1188z00_1654, 
((obj_t)BgL_tbz00_17), BgL_condvarz00_18); 
if(
BGL_CONDVARP(BgL_aux2297z00_4201))
{ /* Llib/thread.scm 355 */
return BgL_aux2297z00_4201;}  else 
{ 
 obj_t BgL_auxz00_11535;
BgL_auxz00_11535 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14317L), BGl_string2683z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_aux2297z00_4201); 
FAILURE(BgL_auxz00_11535,BFALSE,BFALSE);} }  else 
{ /* Llib/thread.scm 355 */
FAILURE(BGl_string2684z00zz__threadz00,BGl_list2685z00zz__threadz00,BgL_method1188z00_1654);} } } 

}



/* &tb-condvar-initialize! */
obj_t BGl_z62tbzd2condvarzd2initializa7ez12zd7zz__threadz00(obj_t BgL_envz00_3719, obj_t BgL_tbz00_3720, obj_t BgL_condvarz00_3721)
{
{ /* Llib/thread.scm 355 */
{ /* Llib/thread.scm 355 */
 obj_t BgL_auxz00_11570; BgL_threadzd2backendzd2_bglt BgL_auxz00_11540;
if(
BGL_CONDVARP(BgL_condvarz00_3721))
{ /* Llib/thread.scm 355 */
BgL_auxz00_11570 = BgL_condvarz00_3721
; }  else 
{ 
 obj_t BgL_auxz00_11573;
BgL_auxz00_11573 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14317L), BGl_string2689z00zz__threadz00, BGl_string2471z00zz__threadz00, BgL_condvarz00_3721); 
FAILURE(BgL_auxz00_11573,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 355 */
 bool_t BgL_test3661z00_11541;
{ /* Llib/thread.scm 355 */
 obj_t BgL_classz00_7218;
BgL_classz00_7218 = BGl_threadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3720))
{ /* Llib/thread.scm 355 */
 BgL_objectz00_bglt BgL_arg1932z00_7220; long BgL_arg1933z00_7221;
BgL_arg1932z00_7220 = 
(BgL_objectz00_bglt)(BgL_tbz00_3720); 
BgL_arg1933z00_7221 = 
BGL_CLASS_DEPTH(BgL_classz00_7218); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 355 */
 long BgL_idxz00_7229;
BgL_idxz00_7229 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7220); 
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1923z00_7230;
{ /* Llib/thread.scm 355 */
 long BgL_arg1924z00_7231;
BgL_arg1924z00_7231 = 
(BgL_idxz00_7229+BgL_arg1933z00_7221); 
BgL_arg1923z00_7230 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7231); } 
BgL_test3661z00_11541 = 
(BgL_arg1923z00_7230==BgL_classz00_7218); } }  else 
{ /* Llib/thread.scm 355 */
 bool_t BgL_res2044z00_7236;
{ /* Llib/thread.scm 355 */
 obj_t BgL_oclassz00_7240;
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1937z00_7242; long BgL_arg1938z00_7243;
BgL_arg1937z00_7242 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 355 */
 long BgL_arg1939z00_7244;
BgL_arg1939z00_7244 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7220); 
BgL_arg1938z00_7243 = 
(BgL_arg1939z00_7244-OBJECT_TYPE); } 
BgL_oclassz00_7240 = 
VECTOR_REF(BgL_arg1937z00_7242,BgL_arg1938z00_7243); } 
{ /* Llib/thread.scm 355 */
 bool_t BgL__ortest_1147z00_7250;
BgL__ortest_1147z00_7250 = 
(BgL_classz00_7218==BgL_oclassz00_7240); 
if(BgL__ortest_1147z00_7250)
{ /* Llib/thread.scm 355 */
BgL_res2044z00_7236 = BgL__ortest_1147z00_7250; }  else 
{ /* Llib/thread.scm 355 */
 long BgL_odepthz00_7251;
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1927z00_7252;
BgL_arg1927z00_7252 = 
(BgL_oclassz00_7240); 
BgL_odepthz00_7251 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7252); } 
if(
(BgL_arg1933z00_7221<BgL_odepthz00_7251))
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1925z00_7256;
{ /* Llib/thread.scm 355 */
 obj_t BgL_arg1926z00_7257;
BgL_arg1926z00_7257 = 
(BgL_oclassz00_7240); 
BgL_arg1925z00_7256 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7257, BgL_arg1933z00_7221); } 
BgL_res2044z00_7236 = 
(BgL_arg1925z00_7256==BgL_classz00_7218); }  else 
{ /* Llib/thread.scm 355 */
BgL_res2044z00_7236 = ((bool_t)0); } } } } 
BgL_test3661z00_11541 = BgL_res2044z00_7236; } }  else 
{ /* Llib/thread.scm 355 */
BgL_test3661z00_11541 = ((bool_t)0)
; } } 
if(BgL_test3661z00_11541)
{ /* Llib/thread.scm 355 */
BgL_auxz00_11540 = 
((BgL_threadzd2backendzd2_bglt)BgL_tbz00_3720)
; }  else 
{ 
 obj_t BgL_auxz00_11566;
BgL_auxz00_11566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14317L), BGl_string2689z00zz__threadz00, BGl_string2414z00zz__threadz00, BgL_tbz00_3720); 
FAILURE(BgL_auxz00_11566,BFALSE,BFALSE);} } 
return 
BGl_tbzd2condvarzd2initializa7ez12zb5zz__threadz00(BgL_auxz00_11540, BgL_auxz00_11570);} } 

}



/* thread-initialize! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2initializa7ez12z67zz__threadz00(BgL_threadz00_bglt BgL_thz00_26)
{
{ /* Llib/thread.scm 384 */
{ /* Llib/thread.scm 384 */
 obj_t BgL_method1196z00_1655;
{ /* Llib/thread.scm 384 */
 obj_t BgL_res1971z00_2638;
{ /* Llib/thread.scm 384 */
 long BgL_objzd2classzd2numz00_2609;
BgL_objzd2classzd2numz00_2609 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_26)); 
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1934z00_2610;
BgL_arg1934z00_2610 = 
PROCEDURE_REF(BGl_threadzd2initializa7ez12zd2envzb5zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 384 */
 int BgL_offsetz00_2613;
BgL_offsetz00_2613 = 
(int)(BgL_objzd2classzd2numz00_2609); 
{ /* Llib/thread.scm 384 */
 long BgL_offsetz00_2614;
BgL_offsetz00_2614 = 
(
(long)(BgL_offsetz00_2613)-OBJECT_TYPE); 
{ /* Llib/thread.scm 384 */
 long BgL_modz00_2615;
BgL_modz00_2615 = 
(BgL_offsetz00_2614 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 384 */
 long BgL_restz00_2617;
BgL_restz00_2617 = 
(BgL_offsetz00_2614 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 384 */

{ /* Llib/thread.scm 384 */
 obj_t BgL_bucketz00_2619;
BgL_bucketz00_2619 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2610),BgL_modz00_2615); 
BgL_res1971z00_2638 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2619),BgL_restz00_2617); } } } } } } } } 
BgL_method1196z00_1655 = BgL_res1971z00_2638; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1196z00_1655, 1))
{ /* Llib/thread.scm 384 */
return 
BGL_PROCEDURE_CALL1(BgL_method1196z00_1655, 
((obj_t)BgL_thz00_26));}  else 
{ /* Llib/thread.scm 384 */
FAILURE(BGl_string2690z00zz__threadz00,BGl_list2691z00zz__threadz00,BgL_method1196z00_1655);} } } 

}



/* &thread-initialize! */
obj_t BGl_z62threadzd2initializa7ez12z05zz__threadz00(obj_t BgL_envz00_3640, obj_t BgL_thz00_3641)
{
{ /* Llib/thread.scm 384 */
{ /* Llib/thread.scm 384 */
 BgL_threadz00_bglt BgL_auxz00_11611;
{ /* Llib/thread.scm 384 */
 bool_t BgL_test3668z00_11612;
{ /* Llib/thread.scm 384 */
 obj_t BgL_classz00_7258;
BgL_classz00_7258 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3641))
{ /* Llib/thread.scm 384 */
 BgL_objectz00_bglt BgL_arg1932z00_7260; long BgL_arg1933z00_7261;
BgL_arg1932z00_7260 = 
(BgL_objectz00_bglt)(BgL_thz00_3641); 
BgL_arg1933z00_7261 = 
BGL_CLASS_DEPTH(BgL_classz00_7258); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 384 */
 long BgL_idxz00_7269;
BgL_idxz00_7269 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7260); 
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1923z00_7270;
{ /* Llib/thread.scm 384 */
 long BgL_arg1924z00_7271;
BgL_arg1924z00_7271 = 
(BgL_idxz00_7269+BgL_arg1933z00_7261); 
BgL_arg1923z00_7270 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7271); } 
BgL_test3668z00_11612 = 
(BgL_arg1923z00_7270==BgL_classz00_7258); } }  else 
{ /* Llib/thread.scm 384 */
 bool_t BgL_res2044z00_7276;
{ /* Llib/thread.scm 384 */
 obj_t BgL_oclassz00_7280;
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1937z00_7282; long BgL_arg1938z00_7283;
BgL_arg1937z00_7282 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 384 */
 long BgL_arg1939z00_7284;
BgL_arg1939z00_7284 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7260); 
BgL_arg1938z00_7283 = 
(BgL_arg1939z00_7284-OBJECT_TYPE); } 
BgL_oclassz00_7280 = 
VECTOR_REF(BgL_arg1937z00_7282,BgL_arg1938z00_7283); } 
{ /* Llib/thread.scm 384 */
 bool_t BgL__ortest_1147z00_7290;
BgL__ortest_1147z00_7290 = 
(BgL_classz00_7258==BgL_oclassz00_7280); 
if(BgL__ortest_1147z00_7290)
{ /* Llib/thread.scm 384 */
BgL_res2044z00_7276 = BgL__ortest_1147z00_7290; }  else 
{ /* Llib/thread.scm 384 */
 long BgL_odepthz00_7291;
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1927z00_7292;
BgL_arg1927z00_7292 = 
(BgL_oclassz00_7280); 
BgL_odepthz00_7291 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7292); } 
if(
(BgL_arg1933z00_7261<BgL_odepthz00_7291))
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1925z00_7296;
{ /* Llib/thread.scm 384 */
 obj_t BgL_arg1926z00_7297;
BgL_arg1926z00_7297 = 
(BgL_oclassz00_7280); 
BgL_arg1925z00_7296 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7297, BgL_arg1933z00_7261); } 
BgL_res2044z00_7276 = 
(BgL_arg1925z00_7296==BgL_classz00_7258); }  else 
{ /* Llib/thread.scm 384 */
BgL_res2044z00_7276 = ((bool_t)0); } } } } 
BgL_test3668z00_11612 = BgL_res2044z00_7276; } }  else 
{ /* Llib/thread.scm 384 */
BgL_test3668z00_11612 = ((bool_t)0)
; } } 
if(BgL_test3668z00_11612)
{ /* Llib/thread.scm 384 */
BgL_auxz00_11611 = 
((BgL_threadz00_bglt)BgL_thz00_3641)
; }  else 
{ 
 obj_t BgL_auxz00_11637;
BgL_auxz00_11637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(15868L), BGl_string2696z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3641); 
FAILURE(BgL_auxz00_11637,BFALSE,BFALSE);} } 
return 
BGl_threadzd2initializa7ez12z67zz__threadz00(BgL_auxz00_11611);} } 

}



/* thread-start! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2startz12zc0zz__threadz00(BgL_threadz00_bglt BgL_thz00_27, obj_t BgL_scz00_28)
{
{ /* Llib/thread.scm 390 */
{ /* Llib/thread.scm 390 */
 obj_t BgL_method1198z00_1656;
{ /* Llib/thread.scm 390 */
 obj_t BgL_res1976z00_2669;
{ /* Llib/thread.scm 390 */
 long BgL_objzd2classzd2numz00_2640;
BgL_objzd2classzd2numz00_2640 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_27)); 
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1934z00_2641;
BgL_arg1934z00_2641 = 
PROCEDURE_REF(BGl_threadzd2startz12zd2envz12zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 390 */
 int BgL_offsetz00_2644;
BgL_offsetz00_2644 = 
(int)(BgL_objzd2classzd2numz00_2640); 
{ /* Llib/thread.scm 390 */
 long BgL_offsetz00_2645;
BgL_offsetz00_2645 = 
(
(long)(BgL_offsetz00_2644)-OBJECT_TYPE); 
{ /* Llib/thread.scm 390 */
 long BgL_modz00_2646;
BgL_modz00_2646 = 
(BgL_offsetz00_2645 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 390 */
 long BgL_restz00_2648;
BgL_restz00_2648 = 
(BgL_offsetz00_2645 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 390 */

{ /* Llib/thread.scm 390 */
 obj_t BgL_bucketz00_2650;
BgL_bucketz00_2650 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2641),BgL_modz00_2646); 
BgL_res1976z00_2669 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2650),BgL_restz00_2648); } } } } } } } } 
BgL_method1198z00_1656 = BgL_res1976z00_2669; } 
{ /* Llib/thread.scm 390 */
 obj_t BgL_valz00_4217;
{ /* Llib/thread.scm 390 */
 obj_t BgL_list1517z00_1657;
BgL_list1517z00_1657 = 
MAKE_YOUNG_PAIR(BgL_scz00_28, BNIL); 
BgL_valz00_4217 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
((obj_t)BgL_thz00_27), BgL_list1517z00_1657); } 
{ /* Llib/thread.scm 390 */
 int BgL_len2306z00_4218;
BgL_len2306z00_4218 = 
(int)(
bgl_list_length(BgL_valz00_4217)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1198z00_1656, BgL_len2306z00_4218))
{ /* Llib/thread.scm 390 */
return 
apply(BgL_method1198z00_1656, BgL_valz00_4217);}  else 
{ /* Llib/thread.scm 390 */
FAILURE(BGl_symbol2697z00zz__threadz00,BGl_string2699z00zz__threadz00,BGl_list2700z00zz__threadz00);} } } } } 

}



/* &thread-start! */
obj_t BGl_z62threadzd2startz12za2zz__threadz00(obj_t BgL_envz00_3722, obj_t BgL_thz00_3723, obj_t BgL_scz00_3724)
{
{ /* Llib/thread.scm 390 */
{ /* Llib/thread.scm 390 */
 BgL_threadz00_bglt BgL_auxz00_11677;
{ /* Llib/thread.scm 390 */
 bool_t BgL_test3674z00_11678;
{ /* Llib/thread.scm 390 */
 obj_t BgL_classz00_7298;
BgL_classz00_7298 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3723))
{ /* Llib/thread.scm 390 */
 BgL_objectz00_bglt BgL_arg1932z00_7300; long BgL_arg1933z00_7301;
BgL_arg1932z00_7300 = 
(BgL_objectz00_bglt)(BgL_thz00_3723); 
BgL_arg1933z00_7301 = 
BGL_CLASS_DEPTH(BgL_classz00_7298); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 390 */
 long BgL_idxz00_7309;
BgL_idxz00_7309 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7300); 
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1923z00_7310;
{ /* Llib/thread.scm 390 */
 long BgL_arg1924z00_7311;
BgL_arg1924z00_7311 = 
(BgL_idxz00_7309+BgL_arg1933z00_7301); 
BgL_arg1923z00_7310 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7311); } 
BgL_test3674z00_11678 = 
(BgL_arg1923z00_7310==BgL_classz00_7298); } }  else 
{ /* Llib/thread.scm 390 */
 bool_t BgL_res2044z00_7316;
{ /* Llib/thread.scm 390 */
 obj_t BgL_oclassz00_7320;
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1937z00_7322; long BgL_arg1938z00_7323;
BgL_arg1937z00_7322 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 390 */
 long BgL_arg1939z00_7324;
BgL_arg1939z00_7324 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7300); 
BgL_arg1938z00_7323 = 
(BgL_arg1939z00_7324-OBJECT_TYPE); } 
BgL_oclassz00_7320 = 
VECTOR_REF(BgL_arg1937z00_7322,BgL_arg1938z00_7323); } 
{ /* Llib/thread.scm 390 */
 bool_t BgL__ortest_1147z00_7330;
BgL__ortest_1147z00_7330 = 
(BgL_classz00_7298==BgL_oclassz00_7320); 
if(BgL__ortest_1147z00_7330)
{ /* Llib/thread.scm 390 */
BgL_res2044z00_7316 = BgL__ortest_1147z00_7330; }  else 
{ /* Llib/thread.scm 390 */
 long BgL_odepthz00_7331;
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1927z00_7332;
BgL_arg1927z00_7332 = 
(BgL_oclassz00_7320); 
BgL_odepthz00_7331 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7332); } 
if(
(BgL_arg1933z00_7301<BgL_odepthz00_7331))
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1925z00_7336;
{ /* Llib/thread.scm 390 */
 obj_t BgL_arg1926z00_7337;
BgL_arg1926z00_7337 = 
(BgL_oclassz00_7320); 
BgL_arg1925z00_7336 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7337, BgL_arg1933z00_7301); } 
BgL_res2044z00_7316 = 
(BgL_arg1925z00_7336==BgL_classz00_7298); }  else 
{ /* Llib/thread.scm 390 */
BgL_res2044z00_7316 = ((bool_t)0); } } } } 
BgL_test3674z00_11678 = BgL_res2044z00_7316; } }  else 
{ /* Llib/thread.scm 390 */
BgL_test3674z00_11678 = ((bool_t)0)
; } } 
if(BgL_test3674z00_11678)
{ /* Llib/thread.scm 390 */
BgL_auxz00_11677 = 
((BgL_threadz00_bglt)BgL_thz00_3723)
; }  else 
{ 
 obj_t BgL_auxz00_11703;
BgL_auxz00_11703 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16146L), BGl_string2723z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3723); 
FAILURE(BgL_auxz00_11703,BFALSE,BFALSE);} } 
return 
BGl_threadzd2startz12zc0zz__threadz00(BgL_auxz00_11677, BgL_scz00_3724);} } 

}



/* thread-start-joinable! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2startzd2joinablez12z12zz__threadz00(BgL_threadz00_bglt BgL_thz00_29)
{
{ /* Llib/thread.scm 395 */
{ /* Llib/thread.scm 395 */
 obj_t BgL_method1201z00_1658;
{ /* Llib/thread.scm 395 */
 obj_t BgL_res1981z00_2700;
{ /* Llib/thread.scm 395 */
 long BgL_objzd2classzd2numz00_2671;
BgL_objzd2classzd2numz00_2671 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_29)); 
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1934z00_2672;
BgL_arg1934z00_2672 = 
PROCEDURE_REF(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 395 */
 int BgL_offsetz00_2675;
BgL_offsetz00_2675 = 
(int)(BgL_objzd2classzd2numz00_2671); 
{ /* Llib/thread.scm 395 */
 long BgL_offsetz00_2676;
BgL_offsetz00_2676 = 
(
(long)(BgL_offsetz00_2675)-OBJECT_TYPE); 
{ /* Llib/thread.scm 395 */
 long BgL_modz00_2677;
BgL_modz00_2677 = 
(BgL_offsetz00_2676 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 395 */
 long BgL_restz00_2679;
BgL_restz00_2679 = 
(BgL_offsetz00_2676 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 395 */

{ /* Llib/thread.scm 395 */
 obj_t BgL_bucketz00_2681;
BgL_bucketz00_2681 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2672),BgL_modz00_2677); 
BgL_res1981z00_2700 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2681),BgL_restz00_2679); } } } } } } } } 
BgL_method1201z00_1658 = BgL_res1981z00_2700; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1201z00_1658, 1))
{ /* Llib/thread.scm 395 */
return 
BGL_PROCEDURE_CALL1(BgL_method1201z00_1658, 
((obj_t)BgL_thz00_29));}  else 
{ /* Llib/thread.scm 395 */
FAILURE(BGl_string2724z00zz__threadz00,BGl_list2725z00zz__threadz00,BgL_method1201z00_1658);} } } 

}



/* &thread-start-joinable! */
obj_t BGl_z62threadzd2startzd2joinablez12z70zz__threadz00(obj_t BgL_envz00_3725, obj_t BgL_thz00_3726)
{
{ /* Llib/thread.scm 395 */
{ /* Llib/thread.scm 395 */
 BgL_threadz00_bglt BgL_auxz00_11741;
{ /* Llib/thread.scm 395 */
 bool_t BgL_test3680z00_11742;
{ /* Llib/thread.scm 395 */
 obj_t BgL_classz00_7338;
BgL_classz00_7338 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3726))
{ /* Llib/thread.scm 395 */
 BgL_objectz00_bglt BgL_arg1932z00_7340; long BgL_arg1933z00_7341;
BgL_arg1932z00_7340 = 
(BgL_objectz00_bglt)(BgL_thz00_3726); 
BgL_arg1933z00_7341 = 
BGL_CLASS_DEPTH(BgL_classz00_7338); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 395 */
 long BgL_idxz00_7349;
BgL_idxz00_7349 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7340); 
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1923z00_7350;
{ /* Llib/thread.scm 395 */
 long BgL_arg1924z00_7351;
BgL_arg1924z00_7351 = 
(BgL_idxz00_7349+BgL_arg1933z00_7341); 
BgL_arg1923z00_7350 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7351); } 
BgL_test3680z00_11742 = 
(BgL_arg1923z00_7350==BgL_classz00_7338); } }  else 
{ /* Llib/thread.scm 395 */
 bool_t BgL_res2044z00_7356;
{ /* Llib/thread.scm 395 */
 obj_t BgL_oclassz00_7360;
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1937z00_7362; long BgL_arg1938z00_7363;
BgL_arg1937z00_7362 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 395 */
 long BgL_arg1939z00_7364;
BgL_arg1939z00_7364 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7340); 
BgL_arg1938z00_7363 = 
(BgL_arg1939z00_7364-OBJECT_TYPE); } 
BgL_oclassz00_7360 = 
VECTOR_REF(BgL_arg1937z00_7362,BgL_arg1938z00_7363); } 
{ /* Llib/thread.scm 395 */
 bool_t BgL__ortest_1147z00_7370;
BgL__ortest_1147z00_7370 = 
(BgL_classz00_7338==BgL_oclassz00_7360); 
if(BgL__ortest_1147z00_7370)
{ /* Llib/thread.scm 395 */
BgL_res2044z00_7356 = BgL__ortest_1147z00_7370; }  else 
{ /* Llib/thread.scm 395 */
 long BgL_odepthz00_7371;
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1927z00_7372;
BgL_arg1927z00_7372 = 
(BgL_oclassz00_7360); 
BgL_odepthz00_7371 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7372); } 
if(
(BgL_arg1933z00_7341<BgL_odepthz00_7371))
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1925z00_7376;
{ /* Llib/thread.scm 395 */
 obj_t BgL_arg1926z00_7377;
BgL_arg1926z00_7377 = 
(BgL_oclassz00_7360); 
BgL_arg1925z00_7376 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7377, BgL_arg1933z00_7341); } 
BgL_res2044z00_7356 = 
(BgL_arg1925z00_7376==BgL_classz00_7338); }  else 
{ /* Llib/thread.scm 395 */
BgL_res2044z00_7356 = ((bool_t)0); } } } } 
BgL_test3680z00_11742 = BgL_res2044z00_7356; } }  else 
{ /* Llib/thread.scm 395 */
BgL_test3680z00_11742 = ((bool_t)0)
; } } 
if(BgL_test3680z00_11742)
{ /* Llib/thread.scm 395 */
BgL_auxz00_11741 = 
((BgL_threadz00_bglt)BgL_thz00_3726)
; }  else 
{ 
 obj_t BgL_auxz00_11767;
BgL_auxz00_11767 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16418L), BGl_string2728z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3726); 
FAILURE(BgL_auxz00_11767,BFALSE,BFALSE);} } 
return 
BGl_threadzd2startzd2joinablez12z12zz__threadz00(BgL_auxz00_11741);} } 

}



/* thread-join! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2joinz12zc0zz__threadz00(BgL_threadz00_bglt BgL_thz00_30, obj_t BgL_timeoutz00_31)
{
{ /* Llib/thread.scm 400 */
{ /* Llib/thread.scm 400 */
 obj_t BgL_method1203z00_1659;
{ /* Llib/thread.scm 400 */
 obj_t BgL_res1986z00_2731;
{ /* Llib/thread.scm 400 */
 long BgL_objzd2classzd2numz00_2702;
BgL_objzd2classzd2numz00_2702 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_30)); 
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1934z00_2703;
BgL_arg1934z00_2703 = 
PROCEDURE_REF(BGl_threadzd2joinz12zd2envz12zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 400 */
 int BgL_offsetz00_2706;
BgL_offsetz00_2706 = 
(int)(BgL_objzd2classzd2numz00_2702); 
{ /* Llib/thread.scm 400 */
 long BgL_offsetz00_2707;
BgL_offsetz00_2707 = 
(
(long)(BgL_offsetz00_2706)-OBJECT_TYPE); 
{ /* Llib/thread.scm 400 */
 long BgL_modz00_2708;
BgL_modz00_2708 = 
(BgL_offsetz00_2707 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 400 */
 long BgL_restz00_2710;
BgL_restz00_2710 = 
(BgL_offsetz00_2707 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 400 */

{ /* Llib/thread.scm 400 */
 obj_t BgL_bucketz00_2712;
BgL_bucketz00_2712 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2703),BgL_modz00_2708); 
BgL_res1986z00_2731 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2712),BgL_restz00_2710); } } } } } } } } 
BgL_method1203z00_1659 = BgL_res1986z00_2731; } 
{ /* Llib/thread.scm 400 */
 obj_t BgL_valz00_4230;
{ /* Llib/thread.scm 400 */
 obj_t BgL_list1518z00_1660;
BgL_list1518z00_1660 = 
MAKE_YOUNG_PAIR(BgL_timeoutz00_31, BNIL); 
BgL_valz00_4230 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
((obj_t)BgL_thz00_30), BgL_list1518z00_1660); } 
{ /* Llib/thread.scm 400 */
 int BgL_len2312z00_4231;
BgL_len2312z00_4231 = 
(int)(
bgl_list_length(BgL_valz00_4230)); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1203z00_1659, BgL_len2312z00_4231))
{ /* Llib/thread.scm 400 */
return 
apply(BgL_method1203z00_1659, BgL_valz00_4230);}  else 
{ /* Llib/thread.scm 400 */
FAILURE(BGl_symbol2729z00zz__threadz00,BGl_string2699z00zz__threadz00,BGl_list2731z00zz__threadz00);} } } } } 

}



/* &thread-join! */
obj_t BGl_z62threadzd2joinz12za2zz__threadz00(obj_t BgL_envz00_3727, obj_t BgL_thz00_3728, obj_t BgL_timeoutz00_3729)
{
{ /* Llib/thread.scm 400 */
{ /* Llib/thread.scm 400 */
 BgL_threadz00_bglt BgL_auxz00_11807;
{ /* Llib/thread.scm 400 */
 bool_t BgL_test3686z00_11808;
{ /* Llib/thread.scm 400 */
 obj_t BgL_classz00_7378;
BgL_classz00_7378 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3728))
{ /* Llib/thread.scm 400 */
 BgL_objectz00_bglt BgL_arg1932z00_7380; long BgL_arg1933z00_7381;
BgL_arg1932z00_7380 = 
(BgL_objectz00_bglt)(BgL_thz00_3728); 
BgL_arg1933z00_7381 = 
BGL_CLASS_DEPTH(BgL_classz00_7378); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 400 */
 long BgL_idxz00_7389;
BgL_idxz00_7389 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7380); 
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1923z00_7390;
{ /* Llib/thread.scm 400 */
 long BgL_arg1924z00_7391;
BgL_arg1924z00_7391 = 
(BgL_idxz00_7389+BgL_arg1933z00_7381); 
BgL_arg1923z00_7390 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7391); } 
BgL_test3686z00_11808 = 
(BgL_arg1923z00_7390==BgL_classz00_7378); } }  else 
{ /* Llib/thread.scm 400 */
 bool_t BgL_res2044z00_7396;
{ /* Llib/thread.scm 400 */
 obj_t BgL_oclassz00_7400;
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1937z00_7402; long BgL_arg1938z00_7403;
BgL_arg1937z00_7402 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 400 */
 long BgL_arg1939z00_7404;
BgL_arg1939z00_7404 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7380); 
BgL_arg1938z00_7403 = 
(BgL_arg1939z00_7404-OBJECT_TYPE); } 
BgL_oclassz00_7400 = 
VECTOR_REF(BgL_arg1937z00_7402,BgL_arg1938z00_7403); } 
{ /* Llib/thread.scm 400 */
 bool_t BgL__ortest_1147z00_7410;
BgL__ortest_1147z00_7410 = 
(BgL_classz00_7378==BgL_oclassz00_7400); 
if(BgL__ortest_1147z00_7410)
{ /* Llib/thread.scm 400 */
BgL_res2044z00_7396 = BgL__ortest_1147z00_7410; }  else 
{ /* Llib/thread.scm 400 */
 long BgL_odepthz00_7411;
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1927z00_7412;
BgL_arg1927z00_7412 = 
(BgL_oclassz00_7400); 
BgL_odepthz00_7411 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7412); } 
if(
(BgL_arg1933z00_7381<BgL_odepthz00_7411))
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1925z00_7416;
{ /* Llib/thread.scm 400 */
 obj_t BgL_arg1926z00_7417;
BgL_arg1926z00_7417 = 
(BgL_oclassz00_7400); 
BgL_arg1925z00_7416 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7417, BgL_arg1933z00_7381); } 
BgL_res2044z00_7396 = 
(BgL_arg1925z00_7416==BgL_classz00_7378); }  else 
{ /* Llib/thread.scm 400 */
BgL_res2044z00_7396 = ((bool_t)0); } } } } 
BgL_test3686z00_11808 = BgL_res2044z00_7396; } }  else 
{ /* Llib/thread.scm 400 */
BgL_test3686z00_11808 = ((bool_t)0)
; } } 
if(BgL_test3686z00_11808)
{ /* Llib/thread.scm 400 */
BgL_auxz00_11807 = 
((BgL_threadz00_bglt)BgL_thz00_3728)
; }  else 
{ 
 obj_t BgL_auxz00_11833;
BgL_auxz00_11833 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16694L), BGl_string2743z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3728); 
FAILURE(BgL_auxz00_11833,BFALSE,BFALSE);} } 
return 
BGl_threadzd2joinz12zc0zz__threadz00(BgL_auxz00_11807, BgL_timeoutz00_3729);} } 

}



/* thread-terminate! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2terminatez12zc0zz__threadz00(BgL_threadz00_bglt BgL_thz00_32)
{
{ /* Llib/thread.scm 405 */
{ /* Llib/thread.scm 405 */
 obj_t BgL_method1206z00_1661;
{ /* Llib/thread.scm 405 */
 obj_t BgL_res1991z00_2762;
{ /* Llib/thread.scm 405 */
 long BgL_objzd2classzd2numz00_2733;
BgL_objzd2classzd2numz00_2733 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_32)); 
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1934z00_2734;
BgL_arg1934z00_2734 = 
PROCEDURE_REF(BGl_threadzd2terminatez12zd2envz12zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 405 */
 int BgL_offsetz00_2737;
BgL_offsetz00_2737 = 
(int)(BgL_objzd2classzd2numz00_2733); 
{ /* Llib/thread.scm 405 */
 long BgL_offsetz00_2738;
BgL_offsetz00_2738 = 
(
(long)(BgL_offsetz00_2737)-OBJECT_TYPE); 
{ /* Llib/thread.scm 405 */
 long BgL_modz00_2739;
BgL_modz00_2739 = 
(BgL_offsetz00_2738 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 405 */
 long BgL_restz00_2741;
BgL_restz00_2741 = 
(BgL_offsetz00_2738 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 405 */

{ /* Llib/thread.scm 405 */
 obj_t BgL_bucketz00_2743;
BgL_bucketz00_2743 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2734),BgL_modz00_2739); 
BgL_res1991z00_2762 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2743),BgL_restz00_2741); } } } } } } } } 
BgL_method1206z00_1661 = BgL_res1991z00_2762; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1206z00_1661, 1))
{ /* Llib/thread.scm 405 */
return 
BGL_PROCEDURE_CALL1(BgL_method1206z00_1661, 
((obj_t)BgL_thz00_32));}  else 
{ /* Llib/thread.scm 405 */
FAILURE(BGl_string2744z00zz__threadz00,BGl_list2745z00zz__threadz00,BgL_method1206z00_1661);} } } 

}



/* &thread-terminate! */
obj_t BGl_z62threadzd2terminatez12za2zz__threadz00(obj_t BgL_envz00_3730, obj_t BgL_thz00_3731)
{
{ /* Llib/thread.scm 405 */
{ /* Llib/thread.scm 405 */
 BgL_threadz00_bglt BgL_auxz00_11871;
{ /* Llib/thread.scm 405 */
 bool_t BgL_test3692z00_11872;
{ /* Llib/thread.scm 405 */
 obj_t BgL_classz00_7418;
BgL_classz00_7418 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3731))
{ /* Llib/thread.scm 405 */
 BgL_objectz00_bglt BgL_arg1932z00_7420; long BgL_arg1933z00_7421;
BgL_arg1932z00_7420 = 
(BgL_objectz00_bglt)(BgL_thz00_3731); 
BgL_arg1933z00_7421 = 
BGL_CLASS_DEPTH(BgL_classz00_7418); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 405 */
 long BgL_idxz00_7429;
BgL_idxz00_7429 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7420); 
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1923z00_7430;
{ /* Llib/thread.scm 405 */
 long BgL_arg1924z00_7431;
BgL_arg1924z00_7431 = 
(BgL_idxz00_7429+BgL_arg1933z00_7421); 
BgL_arg1923z00_7430 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7431); } 
BgL_test3692z00_11872 = 
(BgL_arg1923z00_7430==BgL_classz00_7418); } }  else 
{ /* Llib/thread.scm 405 */
 bool_t BgL_res2044z00_7436;
{ /* Llib/thread.scm 405 */
 obj_t BgL_oclassz00_7440;
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1937z00_7442; long BgL_arg1938z00_7443;
BgL_arg1937z00_7442 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 405 */
 long BgL_arg1939z00_7444;
BgL_arg1939z00_7444 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7420); 
BgL_arg1938z00_7443 = 
(BgL_arg1939z00_7444-OBJECT_TYPE); } 
BgL_oclassz00_7440 = 
VECTOR_REF(BgL_arg1937z00_7442,BgL_arg1938z00_7443); } 
{ /* Llib/thread.scm 405 */
 bool_t BgL__ortest_1147z00_7450;
BgL__ortest_1147z00_7450 = 
(BgL_classz00_7418==BgL_oclassz00_7440); 
if(BgL__ortest_1147z00_7450)
{ /* Llib/thread.scm 405 */
BgL_res2044z00_7436 = BgL__ortest_1147z00_7450; }  else 
{ /* Llib/thread.scm 405 */
 long BgL_odepthz00_7451;
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1927z00_7452;
BgL_arg1927z00_7452 = 
(BgL_oclassz00_7440); 
BgL_odepthz00_7451 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7452); } 
if(
(BgL_arg1933z00_7421<BgL_odepthz00_7451))
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1925z00_7456;
{ /* Llib/thread.scm 405 */
 obj_t BgL_arg1926z00_7457;
BgL_arg1926z00_7457 = 
(BgL_oclassz00_7440); 
BgL_arg1925z00_7456 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7457, BgL_arg1933z00_7421); } 
BgL_res2044z00_7436 = 
(BgL_arg1925z00_7456==BgL_classz00_7418); }  else 
{ /* Llib/thread.scm 405 */
BgL_res2044z00_7436 = ((bool_t)0); } } } } 
BgL_test3692z00_11872 = BgL_res2044z00_7436; } }  else 
{ /* Llib/thread.scm 405 */
BgL_test3692z00_11872 = ((bool_t)0)
; } } 
if(BgL_test3692z00_11872)
{ /* Llib/thread.scm 405 */
BgL_auxz00_11871 = 
((BgL_threadz00_bglt)BgL_thz00_3731)
; }  else 
{ 
 obj_t BgL_auxz00_11897;
BgL_auxz00_11897 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(16970L), BGl_string2748z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3731); 
FAILURE(BgL_auxz00_11897,BFALSE,BFALSE);} } 
return 
BGl_threadzd2terminatez12zc0zz__threadz00(BgL_auxz00_11871);} } 

}



/* thread-kill! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2killz12zc0zz__threadz00(BgL_threadz00_bglt BgL_thz00_33, int BgL_nz00_34)
{
{ /* Llib/thread.scm 410 */
{ /* Llib/thread.scm 410 */
 obj_t BgL_method1208z00_1662;
{ /* Llib/thread.scm 410 */
 obj_t BgL_res1996z00_2793;
{ /* Llib/thread.scm 410 */
 long BgL_objzd2classzd2numz00_2764;
BgL_objzd2classzd2numz00_2764 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_33)); 
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1934z00_2765;
BgL_arg1934z00_2765 = 
PROCEDURE_REF(BGl_threadzd2killz12zd2envz12zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 410 */
 int BgL_offsetz00_2768;
BgL_offsetz00_2768 = 
(int)(BgL_objzd2classzd2numz00_2764); 
{ /* Llib/thread.scm 410 */
 long BgL_offsetz00_2769;
BgL_offsetz00_2769 = 
(
(long)(BgL_offsetz00_2768)-OBJECT_TYPE); 
{ /* Llib/thread.scm 410 */
 long BgL_modz00_2770;
BgL_modz00_2770 = 
(BgL_offsetz00_2769 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 410 */
 long BgL_restz00_2772;
BgL_restz00_2772 = 
(BgL_offsetz00_2769 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 410 */

{ /* Llib/thread.scm 410 */
 obj_t BgL_bucketz00_2774;
BgL_bucketz00_2774 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2765),BgL_modz00_2770); 
BgL_res1996z00_2793 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2774),BgL_restz00_2772); } } } } } } } } 
BgL_method1208z00_1662 = BgL_res1996z00_2793; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1208z00_1662, 2))
{ /* Llib/thread.scm 410 */
return 
BGL_PROCEDURE_CALL2(BgL_method1208z00_1662, 
((obj_t)BgL_thz00_33), 
BINT(BgL_nz00_34));}  else 
{ /* Llib/thread.scm 410 */
FAILURE(BGl_string2749z00zz__threadz00,BGl_list2750z00zz__threadz00,BgL_method1208z00_1662);} } } 

}



/* &thread-kill! */
obj_t BGl_z62threadzd2killz12za2zz__threadz00(obj_t BgL_envz00_3732, obj_t BgL_thz00_3733, obj_t BgL_nz00_3734)
{
{ /* Llib/thread.scm 410 */
{ /* Llib/thread.scm 410 */
 int BgL_auxz00_11967; BgL_threadz00_bglt BgL_auxz00_11937;
{ /* Llib/thread.scm 410 */
 obj_t BgL_tmpz00_11968;
if(
INTEGERP(BgL_nz00_3734))
{ /* Llib/thread.scm 410 */
BgL_tmpz00_11968 = BgL_nz00_3734
; }  else 
{ 
 obj_t BgL_auxz00_11971;
BgL_auxz00_11971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17241L), BGl_string2755z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_nz00_3734); 
FAILURE(BgL_auxz00_11971,BFALSE,BFALSE);} 
BgL_auxz00_11967 = 
CINT(BgL_tmpz00_11968); } 
{ /* Llib/thread.scm 410 */
 bool_t BgL_test3698z00_11938;
{ /* Llib/thread.scm 410 */
 obj_t BgL_classz00_7458;
BgL_classz00_7458 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3733))
{ /* Llib/thread.scm 410 */
 BgL_objectz00_bglt BgL_arg1932z00_7460; long BgL_arg1933z00_7461;
BgL_arg1932z00_7460 = 
(BgL_objectz00_bglt)(BgL_thz00_3733); 
BgL_arg1933z00_7461 = 
BGL_CLASS_DEPTH(BgL_classz00_7458); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 410 */
 long BgL_idxz00_7469;
BgL_idxz00_7469 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7460); 
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1923z00_7470;
{ /* Llib/thread.scm 410 */
 long BgL_arg1924z00_7471;
BgL_arg1924z00_7471 = 
(BgL_idxz00_7469+BgL_arg1933z00_7461); 
BgL_arg1923z00_7470 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7471); } 
BgL_test3698z00_11938 = 
(BgL_arg1923z00_7470==BgL_classz00_7458); } }  else 
{ /* Llib/thread.scm 410 */
 bool_t BgL_res2044z00_7476;
{ /* Llib/thread.scm 410 */
 obj_t BgL_oclassz00_7480;
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1937z00_7482; long BgL_arg1938z00_7483;
BgL_arg1937z00_7482 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 410 */
 long BgL_arg1939z00_7484;
BgL_arg1939z00_7484 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7460); 
BgL_arg1938z00_7483 = 
(BgL_arg1939z00_7484-OBJECT_TYPE); } 
BgL_oclassz00_7480 = 
VECTOR_REF(BgL_arg1937z00_7482,BgL_arg1938z00_7483); } 
{ /* Llib/thread.scm 410 */
 bool_t BgL__ortest_1147z00_7490;
BgL__ortest_1147z00_7490 = 
(BgL_classz00_7458==BgL_oclassz00_7480); 
if(BgL__ortest_1147z00_7490)
{ /* Llib/thread.scm 410 */
BgL_res2044z00_7476 = BgL__ortest_1147z00_7490; }  else 
{ /* Llib/thread.scm 410 */
 long BgL_odepthz00_7491;
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1927z00_7492;
BgL_arg1927z00_7492 = 
(BgL_oclassz00_7480); 
BgL_odepthz00_7491 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7492); } 
if(
(BgL_arg1933z00_7461<BgL_odepthz00_7491))
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1925z00_7496;
{ /* Llib/thread.scm 410 */
 obj_t BgL_arg1926z00_7497;
BgL_arg1926z00_7497 = 
(BgL_oclassz00_7480); 
BgL_arg1925z00_7496 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7497, BgL_arg1933z00_7461); } 
BgL_res2044z00_7476 = 
(BgL_arg1925z00_7496==BgL_classz00_7458); }  else 
{ /* Llib/thread.scm 410 */
BgL_res2044z00_7476 = ((bool_t)0); } } } } 
BgL_test3698z00_11938 = BgL_res2044z00_7476; } }  else 
{ /* Llib/thread.scm 410 */
BgL_test3698z00_11938 = ((bool_t)0)
; } } 
if(BgL_test3698z00_11938)
{ /* Llib/thread.scm 410 */
BgL_auxz00_11937 = 
((BgL_threadz00_bglt)BgL_thz00_3733)
; }  else 
{ 
 obj_t BgL_auxz00_11963;
BgL_auxz00_11963 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17241L), BGl_string2755z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3733); 
FAILURE(BgL_auxz00_11963,BFALSE,BFALSE);} } 
return 
BGl_threadzd2killz12zc0zz__threadz00(BgL_auxz00_11937, BgL_auxz00_11967);} } 

}



/* thread-specific */
BGL_EXPORTED_DEF obj_t BGl_threadzd2specificzd2zz__threadz00(BgL_threadz00_bglt BgL_thz00_35)
{
{ /* Llib/thread.scm 415 */
{ /* Llib/thread.scm 415 */
 obj_t BgL_method1210z00_1663;
{ /* Llib/thread.scm 415 */
 obj_t BgL_res2001z00_2824;
{ /* Llib/thread.scm 415 */
 long BgL_objzd2classzd2numz00_2795;
BgL_objzd2classzd2numz00_2795 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_35)); 
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1934z00_2796;
BgL_arg1934z00_2796 = 
PROCEDURE_REF(BGl_threadzd2specificzd2envz00zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 415 */
 int BgL_offsetz00_2799;
BgL_offsetz00_2799 = 
(int)(BgL_objzd2classzd2numz00_2795); 
{ /* Llib/thread.scm 415 */
 long BgL_offsetz00_2800;
BgL_offsetz00_2800 = 
(
(long)(BgL_offsetz00_2799)-OBJECT_TYPE); 
{ /* Llib/thread.scm 415 */
 long BgL_modz00_2801;
BgL_modz00_2801 = 
(BgL_offsetz00_2800 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 415 */
 long BgL_restz00_2803;
BgL_restz00_2803 = 
(BgL_offsetz00_2800 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 415 */

{ /* Llib/thread.scm 415 */
 obj_t BgL_bucketz00_2805;
BgL_bucketz00_2805 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2796),BgL_modz00_2801); 
BgL_res2001z00_2824 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2805),BgL_restz00_2803); } } } } } } } } 
BgL_method1210z00_1663 = BgL_res2001z00_2824; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1210z00_1663, 1))
{ /* Llib/thread.scm 415 */
return 
BGL_PROCEDURE_CALL1(BgL_method1210z00_1663, 
((obj_t)BgL_thz00_35));}  else 
{ /* Llib/thread.scm 415 */
FAILURE(BGl_string2756z00zz__threadz00,BGl_list2757z00zz__threadz00,BgL_method1210z00_1663);} } } 

}



/* &thread-specific */
obj_t BGl_z62threadzd2specificzb0zz__threadz00(obj_t BgL_envz00_3735, obj_t BgL_thz00_3736)
{
{ /* Llib/thread.scm 415 */
{ /* Llib/thread.scm 415 */
 BgL_threadz00_bglt BgL_auxz00_12010;
{ /* Llib/thread.scm 415 */
 bool_t BgL_test3705z00_12011;
{ /* Llib/thread.scm 415 */
 obj_t BgL_classz00_7498;
BgL_classz00_7498 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3736))
{ /* Llib/thread.scm 415 */
 BgL_objectz00_bglt BgL_arg1932z00_7500; long BgL_arg1933z00_7501;
BgL_arg1932z00_7500 = 
(BgL_objectz00_bglt)(BgL_thz00_3736); 
BgL_arg1933z00_7501 = 
BGL_CLASS_DEPTH(BgL_classz00_7498); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 415 */
 long BgL_idxz00_7509;
BgL_idxz00_7509 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7500); 
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1923z00_7510;
{ /* Llib/thread.scm 415 */
 long BgL_arg1924z00_7511;
BgL_arg1924z00_7511 = 
(BgL_idxz00_7509+BgL_arg1933z00_7501); 
BgL_arg1923z00_7510 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7511); } 
BgL_test3705z00_12011 = 
(BgL_arg1923z00_7510==BgL_classz00_7498); } }  else 
{ /* Llib/thread.scm 415 */
 bool_t BgL_res2044z00_7516;
{ /* Llib/thread.scm 415 */
 obj_t BgL_oclassz00_7520;
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1937z00_7522; long BgL_arg1938z00_7523;
BgL_arg1937z00_7522 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 415 */
 long BgL_arg1939z00_7524;
BgL_arg1939z00_7524 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7500); 
BgL_arg1938z00_7523 = 
(BgL_arg1939z00_7524-OBJECT_TYPE); } 
BgL_oclassz00_7520 = 
VECTOR_REF(BgL_arg1937z00_7522,BgL_arg1938z00_7523); } 
{ /* Llib/thread.scm 415 */
 bool_t BgL__ortest_1147z00_7530;
BgL__ortest_1147z00_7530 = 
(BgL_classz00_7498==BgL_oclassz00_7520); 
if(BgL__ortest_1147z00_7530)
{ /* Llib/thread.scm 415 */
BgL_res2044z00_7516 = BgL__ortest_1147z00_7530; }  else 
{ /* Llib/thread.scm 415 */
 long BgL_odepthz00_7531;
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1927z00_7532;
BgL_arg1927z00_7532 = 
(BgL_oclassz00_7520); 
BgL_odepthz00_7531 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7532); } 
if(
(BgL_arg1933z00_7501<BgL_odepthz00_7531))
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1925z00_7536;
{ /* Llib/thread.scm 415 */
 obj_t BgL_arg1926z00_7537;
BgL_arg1926z00_7537 = 
(BgL_oclassz00_7520); 
BgL_arg1925z00_7536 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7537, BgL_arg1933z00_7501); } 
BgL_res2044z00_7516 = 
(BgL_arg1925z00_7536==BgL_classz00_7498); }  else 
{ /* Llib/thread.scm 415 */
BgL_res2044z00_7516 = ((bool_t)0); } } } } 
BgL_test3705z00_12011 = BgL_res2044z00_7516; } }  else 
{ /* Llib/thread.scm 415 */
BgL_test3705z00_12011 = ((bool_t)0)
; } } 
if(BgL_test3705z00_12011)
{ /* Llib/thread.scm 415 */
BgL_auxz00_12010 = 
((BgL_threadz00_bglt)BgL_thz00_3736)
; }  else 
{ 
 obj_t BgL_auxz00_12036;
BgL_auxz00_12036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17514L), BGl_string2760z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3736); 
FAILURE(BgL_auxz00_12036,BFALSE,BFALSE);} } 
return 
BGl_threadzd2specificzd2zz__threadz00(BgL_auxz00_12010);} } 

}



/* thread-specific-set! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_threadz00_bglt BgL_thz00_36, obj_t BgL_vz00_37)
{
{ /* Llib/thread.scm 420 */
{ /* Llib/thread.scm 420 */
 obj_t BgL_method1212z00_1664;
{ /* Llib/thread.scm 420 */
 obj_t BgL_res2006z00_2855;
{ /* Llib/thread.scm 420 */
 long BgL_objzd2classzd2numz00_2826;
BgL_objzd2classzd2numz00_2826 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_36)); 
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1934z00_2827;
BgL_arg1934z00_2827 = 
PROCEDURE_REF(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 420 */
 int BgL_offsetz00_2830;
BgL_offsetz00_2830 = 
(int)(BgL_objzd2classzd2numz00_2826); 
{ /* Llib/thread.scm 420 */
 long BgL_offsetz00_2831;
BgL_offsetz00_2831 = 
(
(long)(BgL_offsetz00_2830)-OBJECT_TYPE); 
{ /* Llib/thread.scm 420 */
 long BgL_modz00_2832;
BgL_modz00_2832 = 
(BgL_offsetz00_2831 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 420 */
 long BgL_restz00_2834;
BgL_restz00_2834 = 
(BgL_offsetz00_2831 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 420 */

{ /* Llib/thread.scm 420 */
 obj_t BgL_bucketz00_2836;
BgL_bucketz00_2836 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2827),BgL_modz00_2832); 
BgL_res2006z00_2855 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2836),BgL_restz00_2834); } } } } } } } } 
BgL_method1212z00_1664 = BgL_res2006z00_2855; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1212z00_1664, 2))
{ /* Llib/thread.scm 420 */
return 
BGL_PROCEDURE_CALL2(BgL_method1212z00_1664, 
((obj_t)BgL_thz00_36), BgL_vz00_37);}  else 
{ /* Llib/thread.scm 420 */
FAILURE(BGl_string2761z00zz__threadz00,BGl_list2762z00zz__threadz00,BgL_method1212z00_1664);} } } 

}



/* &thread-specific-set! */
obj_t BGl_z62threadzd2specificzd2setz12z70zz__threadz00(obj_t BgL_envz00_3737, obj_t BgL_thz00_3738, obj_t BgL_vz00_3739)
{
{ /* Llib/thread.scm 420 */
{ /* Llib/thread.scm 420 */
 BgL_threadz00_bglt BgL_auxz00_12075;
{ /* Llib/thread.scm 420 */
 bool_t BgL_test3711z00_12076;
{ /* Llib/thread.scm 420 */
 obj_t BgL_classz00_7538;
BgL_classz00_7538 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3738))
{ /* Llib/thread.scm 420 */
 BgL_objectz00_bglt BgL_arg1932z00_7540; long BgL_arg1933z00_7541;
BgL_arg1932z00_7540 = 
(BgL_objectz00_bglt)(BgL_thz00_3738); 
BgL_arg1933z00_7541 = 
BGL_CLASS_DEPTH(BgL_classz00_7538); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 420 */
 long BgL_idxz00_7549;
BgL_idxz00_7549 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7540); 
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1923z00_7550;
{ /* Llib/thread.scm 420 */
 long BgL_arg1924z00_7551;
BgL_arg1924z00_7551 = 
(BgL_idxz00_7549+BgL_arg1933z00_7541); 
BgL_arg1923z00_7550 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7551); } 
BgL_test3711z00_12076 = 
(BgL_arg1923z00_7550==BgL_classz00_7538); } }  else 
{ /* Llib/thread.scm 420 */
 bool_t BgL_res2044z00_7556;
{ /* Llib/thread.scm 420 */
 obj_t BgL_oclassz00_7560;
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1937z00_7562; long BgL_arg1938z00_7563;
BgL_arg1937z00_7562 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 420 */
 long BgL_arg1939z00_7564;
BgL_arg1939z00_7564 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7540); 
BgL_arg1938z00_7563 = 
(BgL_arg1939z00_7564-OBJECT_TYPE); } 
BgL_oclassz00_7560 = 
VECTOR_REF(BgL_arg1937z00_7562,BgL_arg1938z00_7563); } 
{ /* Llib/thread.scm 420 */
 bool_t BgL__ortest_1147z00_7570;
BgL__ortest_1147z00_7570 = 
(BgL_classz00_7538==BgL_oclassz00_7560); 
if(BgL__ortest_1147z00_7570)
{ /* Llib/thread.scm 420 */
BgL_res2044z00_7556 = BgL__ortest_1147z00_7570; }  else 
{ /* Llib/thread.scm 420 */
 long BgL_odepthz00_7571;
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1927z00_7572;
BgL_arg1927z00_7572 = 
(BgL_oclassz00_7560); 
BgL_odepthz00_7571 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7572); } 
if(
(BgL_arg1933z00_7541<BgL_odepthz00_7571))
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1925z00_7576;
{ /* Llib/thread.scm 420 */
 obj_t BgL_arg1926z00_7577;
BgL_arg1926z00_7577 = 
(BgL_oclassz00_7560); 
BgL_arg1925z00_7576 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7577, BgL_arg1933z00_7541); } 
BgL_res2044z00_7556 = 
(BgL_arg1925z00_7576==BgL_classz00_7538); }  else 
{ /* Llib/thread.scm 420 */
BgL_res2044z00_7556 = ((bool_t)0); } } } } 
BgL_test3711z00_12076 = BgL_res2044z00_7556; } }  else 
{ /* Llib/thread.scm 420 */
BgL_test3711z00_12076 = ((bool_t)0)
; } } 
if(BgL_test3711z00_12076)
{ /* Llib/thread.scm 420 */
BgL_auxz00_12075 = 
((BgL_threadz00_bglt)BgL_thz00_3738)
; }  else 
{ 
 obj_t BgL_auxz00_12101;
BgL_auxz00_12101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(17783L), BGl_string2767z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3738); 
FAILURE(BgL_auxz00_12101,BFALSE,BFALSE);} } 
return 
BGl_threadzd2specificzd2setz12z12zz__threadz00(BgL_auxz00_12075, BgL_vz00_3739);} } 

}



/* thread-cleanup */
BGL_EXPORTED_DEF obj_t BGl_threadzd2cleanupzd2zz__threadz00(BgL_threadz00_bglt BgL_thz00_38)
{
{ /* Llib/thread.scm 425 */
{ /* Llib/thread.scm 425 */
 obj_t BgL_method1214z00_1665;
{ /* Llib/thread.scm 425 */
 obj_t BgL_res2011z00_2886;
{ /* Llib/thread.scm 425 */
 long BgL_objzd2classzd2numz00_2857;
BgL_objzd2classzd2numz00_2857 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_38)); 
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1934z00_2858;
BgL_arg1934z00_2858 = 
PROCEDURE_REF(BGl_threadzd2cleanupzd2envz00zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 425 */
 int BgL_offsetz00_2861;
BgL_offsetz00_2861 = 
(int)(BgL_objzd2classzd2numz00_2857); 
{ /* Llib/thread.scm 425 */
 long BgL_offsetz00_2862;
BgL_offsetz00_2862 = 
(
(long)(BgL_offsetz00_2861)-OBJECT_TYPE); 
{ /* Llib/thread.scm 425 */
 long BgL_modz00_2863;
BgL_modz00_2863 = 
(BgL_offsetz00_2862 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 425 */
 long BgL_restz00_2865;
BgL_restz00_2865 = 
(BgL_offsetz00_2862 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 425 */

{ /* Llib/thread.scm 425 */
 obj_t BgL_bucketz00_2867;
BgL_bucketz00_2867 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2858),BgL_modz00_2863); 
BgL_res2011z00_2886 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2867),BgL_restz00_2865); } } } } } } } } 
BgL_method1214z00_1665 = BgL_res2011z00_2886; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1214z00_1665, 1))
{ /* Llib/thread.scm 425 */
return 
BGL_PROCEDURE_CALL1(BgL_method1214z00_1665, 
((obj_t)BgL_thz00_38));}  else 
{ /* Llib/thread.scm 425 */
FAILURE(BGl_string2768z00zz__threadz00,BGl_list2769z00zz__threadz00,BgL_method1214z00_1665);} } } 

}



/* &thread-cleanup */
obj_t BGl_z62threadzd2cleanupzb0zz__threadz00(obj_t BgL_envz00_3740, obj_t BgL_thz00_3741)
{
{ /* Llib/thread.scm 425 */
{ /* Llib/thread.scm 425 */
 BgL_threadz00_bglt BgL_auxz00_12139;
{ /* Llib/thread.scm 425 */
 bool_t BgL_test3717z00_12140;
{ /* Llib/thread.scm 425 */
 obj_t BgL_classz00_7578;
BgL_classz00_7578 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3741))
{ /* Llib/thread.scm 425 */
 BgL_objectz00_bglt BgL_arg1932z00_7580; long BgL_arg1933z00_7581;
BgL_arg1932z00_7580 = 
(BgL_objectz00_bglt)(BgL_thz00_3741); 
BgL_arg1933z00_7581 = 
BGL_CLASS_DEPTH(BgL_classz00_7578); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 425 */
 long BgL_idxz00_7589;
BgL_idxz00_7589 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7580); 
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1923z00_7590;
{ /* Llib/thread.scm 425 */
 long BgL_arg1924z00_7591;
BgL_arg1924z00_7591 = 
(BgL_idxz00_7589+BgL_arg1933z00_7581); 
BgL_arg1923z00_7590 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7591); } 
BgL_test3717z00_12140 = 
(BgL_arg1923z00_7590==BgL_classz00_7578); } }  else 
{ /* Llib/thread.scm 425 */
 bool_t BgL_res2044z00_7596;
{ /* Llib/thread.scm 425 */
 obj_t BgL_oclassz00_7600;
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1937z00_7602; long BgL_arg1938z00_7603;
BgL_arg1937z00_7602 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 425 */
 long BgL_arg1939z00_7604;
BgL_arg1939z00_7604 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7580); 
BgL_arg1938z00_7603 = 
(BgL_arg1939z00_7604-OBJECT_TYPE); } 
BgL_oclassz00_7600 = 
VECTOR_REF(BgL_arg1937z00_7602,BgL_arg1938z00_7603); } 
{ /* Llib/thread.scm 425 */
 bool_t BgL__ortest_1147z00_7610;
BgL__ortest_1147z00_7610 = 
(BgL_classz00_7578==BgL_oclassz00_7600); 
if(BgL__ortest_1147z00_7610)
{ /* Llib/thread.scm 425 */
BgL_res2044z00_7596 = BgL__ortest_1147z00_7610; }  else 
{ /* Llib/thread.scm 425 */
 long BgL_odepthz00_7611;
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1927z00_7612;
BgL_arg1927z00_7612 = 
(BgL_oclassz00_7600); 
BgL_odepthz00_7611 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7612); } 
if(
(BgL_arg1933z00_7581<BgL_odepthz00_7611))
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1925z00_7616;
{ /* Llib/thread.scm 425 */
 obj_t BgL_arg1926z00_7617;
BgL_arg1926z00_7617 = 
(BgL_oclassz00_7600); 
BgL_arg1925z00_7616 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7617, BgL_arg1933z00_7581); } 
BgL_res2044z00_7596 = 
(BgL_arg1925z00_7616==BgL_classz00_7578); }  else 
{ /* Llib/thread.scm 425 */
BgL_res2044z00_7596 = ((bool_t)0); } } } } 
BgL_test3717z00_12140 = BgL_res2044z00_7596; } }  else 
{ /* Llib/thread.scm 425 */
BgL_test3717z00_12140 = ((bool_t)0)
; } } 
if(BgL_test3717z00_12140)
{ /* Llib/thread.scm 425 */
BgL_auxz00_12139 = 
((BgL_threadz00_bglt)BgL_thz00_3741)
; }  else 
{ 
 obj_t BgL_auxz00_12165;
BgL_auxz00_12165 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18064L), BGl_string2772z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3741); 
FAILURE(BgL_auxz00_12165,BFALSE,BFALSE);} } 
return 
BGl_threadzd2cleanupzd2zz__threadz00(BgL_auxz00_12139);} } 

}



/* thread-cleanup-set! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_threadz00_bglt BgL_thz00_39, obj_t BgL_vz00_40)
{
{ /* Llib/thread.scm 430 */
{ /* Llib/thread.scm 430 */
 obj_t BgL_method1217z00_1666;
{ /* Llib/thread.scm 430 */
 obj_t BgL_res2016z00_2917;
{ /* Llib/thread.scm 430 */
 long BgL_objzd2classzd2numz00_2888;
BgL_objzd2classzd2numz00_2888 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_39)); 
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1934z00_2889;
BgL_arg1934z00_2889 = 
PROCEDURE_REF(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 430 */
 int BgL_offsetz00_2892;
BgL_offsetz00_2892 = 
(int)(BgL_objzd2classzd2numz00_2888); 
{ /* Llib/thread.scm 430 */
 long BgL_offsetz00_2893;
BgL_offsetz00_2893 = 
(
(long)(BgL_offsetz00_2892)-OBJECT_TYPE); 
{ /* Llib/thread.scm 430 */
 long BgL_modz00_2894;
BgL_modz00_2894 = 
(BgL_offsetz00_2893 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 430 */
 long BgL_restz00_2896;
BgL_restz00_2896 = 
(BgL_offsetz00_2893 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 430 */

{ /* Llib/thread.scm 430 */
 obj_t BgL_bucketz00_2898;
BgL_bucketz00_2898 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2889),BgL_modz00_2894); 
BgL_res2016z00_2917 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2898),BgL_restz00_2896); } } } } } } } } 
BgL_method1217z00_1666 = BgL_res2016z00_2917; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1217z00_1666, 2))
{ /* Llib/thread.scm 430 */
return 
BGL_PROCEDURE_CALL2(BgL_method1217z00_1666, 
((obj_t)BgL_thz00_39), BgL_vz00_40);}  else 
{ /* Llib/thread.scm 430 */
FAILURE(BGl_string2773z00zz__threadz00,BGl_list2774z00zz__threadz00,BgL_method1217z00_1666);} } } 

}



/* &thread-cleanup-set! */
obj_t BGl_z62threadzd2cleanupzd2setz12z70zz__threadz00(obj_t BgL_envz00_3742, obj_t BgL_thz00_3743, obj_t BgL_vz00_3744)
{
{ /* Llib/thread.scm 430 */
{ /* Llib/thread.scm 430 */
 BgL_threadz00_bglt BgL_auxz00_12204;
{ /* Llib/thread.scm 430 */
 bool_t BgL_test3723z00_12205;
{ /* Llib/thread.scm 430 */
 obj_t BgL_classz00_7618;
BgL_classz00_7618 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3743))
{ /* Llib/thread.scm 430 */
 BgL_objectz00_bglt BgL_arg1932z00_7620; long BgL_arg1933z00_7621;
BgL_arg1932z00_7620 = 
(BgL_objectz00_bglt)(BgL_thz00_3743); 
BgL_arg1933z00_7621 = 
BGL_CLASS_DEPTH(BgL_classz00_7618); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 430 */
 long BgL_idxz00_7629;
BgL_idxz00_7629 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7620); 
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1923z00_7630;
{ /* Llib/thread.scm 430 */
 long BgL_arg1924z00_7631;
BgL_arg1924z00_7631 = 
(BgL_idxz00_7629+BgL_arg1933z00_7621); 
BgL_arg1923z00_7630 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7631); } 
BgL_test3723z00_12205 = 
(BgL_arg1923z00_7630==BgL_classz00_7618); } }  else 
{ /* Llib/thread.scm 430 */
 bool_t BgL_res2044z00_7636;
{ /* Llib/thread.scm 430 */
 obj_t BgL_oclassz00_7640;
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1937z00_7642; long BgL_arg1938z00_7643;
BgL_arg1937z00_7642 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 430 */
 long BgL_arg1939z00_7644;
BgL_arg1939z00_7644 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7620); 
BgL_arg1938z00_7643 = 
(BgL_arg1939z00_7644-OBJECT_TYPE); } 
BgL_oclassz00_7640 = 
VECTOR_REF(BgL_arg1937z00_7642,BgL_arg1938z00_7643); } 
{ /* Llib/thread.scm 430 */
 bool_t BgL__ortest_1147z00_7650;
BgL__ortest_1147z00_7650 = 
(BgL_classz00_7618==BgL_oclassz00_7640); 
if(BgL__ortest_1147z00_7650)
{ /* Llib/thread.scm 430 */
BgL_res2044z00_7636 = BgL__ortest_1147z00_7650; }  else 
{ /* Llib/thread.scm 430 */
 long BgL_odepthz00_7651;
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1927z00_7652;
BgL_arg1927z00_7652 = 
(BgL_oclassz00_7640); 
BgL_odepthz00_7651 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7652); } 
if(
(BgL_arg1933z00_7621<BgL_odepthz00_7651))
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1925z00_7656;
{ /* Llib/thread.scm 430 */
 obj_t BgL_arg1926z00_7657;
BgL_arg1926z00_7657 = 
(BgL_oclassz00_7640); 
BgL_arg1925z00_7656 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7657, BgL_arg1933z00_7621); } 
BgL_res2044z00_7636 = 
(BgL_arg1925z00_7656==BgL_classz00_7618); }  else 
{ /* Llib/thread.scm 430 */
BgL_res2044z00_7636 = ((bool_t)0); } } } } 
BgL_test3723z00_12205 = BgL_res2044z00_7636; } }  else 
{ /* Llib/thread.scm 430 */
BgL_test3723z00_12205 = ((bool_t)0)
; } } 
if(BgL_test3723z00_12205)
{ /* Llib/thread.scm 430 */
BgL_auxz00_12204 = 
((BgL_threadz00_bglt)BgL_thz00_3743)
; }  else 
{ 
 obj_t BgL_auxz00_12230;
BgL_auxz00_12230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18332L), BGl_string2777z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3743); 
FAILURE(BgL_auxz00_12230,BFALSE,BFALSE);} } 
return 
BGl_threadzd2cleanupzd2setz12z12zz__threadz00(BgL_auxz00_12204, BgL_vz00_3744);} } 

}



/* thread-name */
BGL_EXPORTED_DEF obj_t BGl_threadzd2namezd2zz__threadz00(BgL_threadz00_bglt BgL_thz00_41)
{
{ /* Llib/thread.scm 435 */
{ /* Llib/thread.scm 435 */
 obj_t BgL_method1219z00_1667;
{ /* Llib/thread.scm 435 */
 obj_t BgL_res2021z00_2948;
{ /* Llib/thread.scm 435 */
 long BgL_objzd2classzd2numz00_2919;
BgL_objzd2classzd2numz00_2919 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_41)); 
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1934z00_2920;
BgL_arg1934z00_2920 = 
PROCEDURE_REF(BGl_threadzd2namezd2envz00zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 435 */
 int BgL_offsetz00_2923;
BgL_offsetz00_2923 = 
(int)(BgL_objzd2classzd2numz00_2919); 
{ /* Llib/thread.scm 435 */
 long BgL_offsetz00_2924;
BgL_offsetz00_2924 = 
(
(long)(BgL_offsetz00_2923)-OBJECT_TYPE); 
{ /* Llib/thread.scm 435 */
 long BgL_modz00_2925;
BgL_modz00_2925 = 
(BgL_offsetz00_2924 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 435 */
 long BgL_restz00_2927;
BgL_restz00_2927 = 
(BgL_offsetz00_2924 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 435 */

{ /* Llib/thread.scm 435 */
 obj_t BgL_bucketz00_2929;
BgL_bucketz00_2929 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2920),BgL_modz00_2925); 
BgL_res2021z00_2948 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2929),BgL_restz00_2927); } } } } } } } } 
BgL_method1219z00_1667 = BgL_res2021z00_2948; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1219z00_1667, 1))
{ /* Llib/thread.scm 435 */
 obj_t BgL_aux2335z00_4274;
BgL_aux2335z00_4274 = 
BGL_PROCEDURE_CALL1(BgL_method1219z00_1667, 
((obj_t)BgL_thz00_41)); 
if(
STRINGP(BgL_aux2335z00_4274))
{ /* Llib/thread.scm 435 */
return BgL_aux2335z00_4274;}  else 
{ 
 obj_t BgL_auxz00_12269;
BgL_auxz00_12269 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18612L), BGl_string2778z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_aux2335z00_4274); 
FAILURE(BgL_auxz00_12269,BFALSE,BFALSE);} }  else 
{ /* Llib/thread.scm 435 */
FAILURE(BGl_string2779z00zz__threadz00,BGl_list2780z00zz__threadz00,BgL_method1219z00_1667);} } } 

}



/* &thread-name */
obj_t BGl_z62threadzd2namezb0zz__threadz00(obj_t BgL_envz00_3745, obj_t BgL_thz00_3746)
{
{ /* Llib/thread.scm 435 */
{ /* Llib/thread.scm 435 */
 BgL_threadz00_bglt BgL_auxz00_12274;
{ /* Llib/thread.scm 435 */
 bool_t BgL_test3730z00_12275;
{ /* Llib/thread.scm 435 */
 obj_t BgL_classz00_7658;
BgL_classz00_7658 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3746))
{ /* Llib/thread.scm 435 */
 BgL_objectz00_bglt BgL_arg1932z00_7660; long BgL_arg1933z00_7661;
BgL_arg1932z00_7660 = 
(BgL_objectz00_bglt)(BgL_thz00_3746); 
BgL_arg1933z00_7661 = 
BGL_CLASS_DEPTH(BgL_classz00_7658); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 435 */
 long BgL_idxz00_7669;
BgL_idxz00_7669 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7660); 
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1923z00_7670;
{ /* Llib/thread.scm 435 */
 long BgL_arg1924z00_7671;
BgL_arg1924z00_7671 = 
(BgL_idxz00_7669+BgL_arg1933z00_7661); 
BgL_arg1923z00_7670 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7671); } 
BgL_test3730z00_12275 = 
(BgL_arg1923z00_7670==BgL_classz00_7658); } }  else 
{ /* Llib/thread.scm 435 */
 bool_t BgL_res2044z00_7676;
{ /* Llib/thread.scm 435 */
 obj_t BgL_oclassz00_7680;
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1937z00_7682; long BgL_arg1938z00_7683;
BgL_arg1937z00_7682 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 435 */
 long BgL_arg1939z00_7684;
BgL_arg1939z00_7684 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7660); 
BgL_arg1938z00_7683 = 
(BgL_arg1939z00_7684-OBJECT_TYPE); } 
BgL_oclassz00_7680 = 
VECTOR_REF(BgL_arg1937z00_7682,BgL_arg1938z00_7683); } 
{ /* Llib/thread.scm 435 */
 bool_t BgL__ortest_1147z00_7690;
BgL__ortest_1147z00_7690 = 
(BgL_classz00_7658==BgL_oclassz00_7680); 
if(BgL__ortest_1147z00_7690)
{ /* Llib/thread.scm 435 */
BgL_res2044z00_7676 = BgL__ortest_1147z00_7690; }  else 
{ /* Llib/thread.scm 435 */
 long BgL_odepthz00_7691;
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1927z00_7692;
BgL_arg1927z00_7692 = 
(BgL_oclassz00_7680); 
BgL_odepthz00_7691 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7692); } 
if(
(BgL_arg1933z00_7661<BgL_odepthz00_7691))
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1925z00_7696;
{ /* Llib/thread.scm 435 */
 obj_t BgL_arg1926z00_7697;
BgL_arg1926z00_7697 = 
(BgL_oclassz00_7680); 
BgL_arg1925z00_7696 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7697, BgL_arg1933z00_7661); } 
BgL_res2044z00_7676 = 
(BgL_arg1925z00_7696==BgL_classz00_7658); }  else 
{ /* Llib/thread.scm 435 */
BgL_res2044z00_7676 = ((bool_t)0); } } } } 
BgL_test3730z00_12275 = BgL_res2044z00_7676; } }  else 
{ /* Llib/thread.scm 435 */
BgL_test3730z00_12275 = ((bool_t)0)
; } } 
if(BgL_test3730z00_12275)
{ /* Llib/thread.scm 435 */
BgL_auxz00_12274 = 
((BgL_threadz00_bglt)BgL_thz00_3746)
; }  else 
{ 
 obj_t BgL_auxz00_12300;
BgL_auxz00_12300 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18612L), BGl_string2783z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3746); 
FAILURE(BgL_auxz00_12300,BFALSE,BFALSE);} } 
return 
BGl_threadzd2namezd2zz__threadz00(BgL_auxz00_12274);} } 

}



/* thread-name-set! */
BGL_EXPORTED_DEF obj_t BGl_threadzd2namezd2setz12z12zz__threadz00(BgL_threadz00_bglt BgL_thz00_42, obj_t BgL_vz00_43)
{
{ /* Llib/thread.scm 440 */
{ /* Llib/thread.scm 440 */
 obj_t BgL_method1221z00_1668;
{ /* Llib/thread.scm 440 */
 obj_t BgL_res2026z00_2979;
{ /* Llib/thread.scm 440 */
 long BgL_objzd2classzd2numz00_2950;
BgL_objzd2classzd2numz00_2950 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_thz00_42)); 
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1934z00_2951;
BgL_arg1934z00_2951 = 
PROCEDURE_REF(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 440 */
 int BgL_offsetz00_2954;
BgL_offsetz00_2954 = 
(int)(BgL_objzd2classzd2numz00_2950); 
{ /* Llib/thread.scm 440 */
 long BgL_offsetz00_2955;
BgL_offsetz00_2955 = 
(
(long)(BgL_offsetz00_2954)-OBJECT_TYPE); 
{ /* Llib/thread.scm 440 */
 long BgL_modz00_2956;
BgL_modz00_2956 = 
(BgL_offsetz00_2955 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 440 */
 long BgL_restz00_2958;
BgL_restz00_2958 = 
(BgL_offsetz00_2955 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 440 */

{ /* Llib/thread.scm 440 */
 obj_t BgL_bucketz00_2960;
BgL_bucketz00_2960 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2951),BgL_modz00_2956); 
BgL_res2026z00_2979 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2960),BgL_restz00_2958); } } } } } } } } 
BgL_method1221z00_1668 = BgL_res2026z00_2979; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1221z00_1668, 2))
{ /* Llib/thread.scm 440 */
return 
BGL_PROCEDURE_CALL2(BgL_method1221z00_1668, 
((obj_t)BgL_thz00_42), BgL_vz00_43);}  else 
{ /* Llib/thread.scm 440 */
FAILURE(BGl_string2784z00zz__threadz00,BGl_list2785z00zz__threadz00,BgL_method1221z00_1668);} } } 

}



/* &thread-name-set! */
obj_t BGl_z62threadzd2namezd2setz12z70zz__threadz00(obj_t BgL_envz00_3747, obj_t BgL_thz00_3748, obj_t BgL_vz00_3749)
{
{ /* Llib/thread.scm 440 */
{ /* Llib/thread.scm 440 */
 obj_t BgL_auxz00_12369; BgL_threadz00_bglt BgL_auxz00_12339;
if(
STRINGP(BgL_vz00_3749))
{ /* Llib/thread.scm 440 */
BgL_auxz00_12369 = BgL_vz00_3749
; }  else 
{ 
 obj_t BgL_auxz00_12372;
BgL_auxz00_12372 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18877L), BGl_string2788z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_vz00_3749); 
FAILURE(BgL_auxz00_12372,BFALSE,BFALSE);} 
{ /* Llib/thread.scm 440 */
 bool_t BgL_test3736z00_12340;
{ /* Llib/thread.scm 440 */
 obj_t BgL_classz00_7698;
BgL_classz00_7698 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3748))
{ /* Llib/thread.scm 440 */
 BgL_objectz00_bglt BgL_arg1932z00_7700; long BgL_arg1933z00_7701;
BgL_arg1932z00_7700 = 
(BgL_objectz00_bglt)(BgL_thz00_3748); 
BgL_arg1933z00_7701 = 
BGL_CLASS_DEPTH(BgL_classz00_7698); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 440 */
 long BgL_idxz00_7709;
BgL_idxz00_7709 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7700); 
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1923z00_7710;
{ /* Llib/thread.scm 440 */
 long BgL_arg1924z00_7711;
BgL_arg1924z00_7711 = 
(BgL_idxz00_7709+BgL_arg1933z00_7701); 
BgL_arg1923z00_7710 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7711); } 
BgL_test3736z00_12340 = 
(BgL_arg1923z00_7710==BgL_classz00_7698); } }  else 
{ /* Llib/thread.scm 440 */
 bool_t BgL_res2044z00_7716;
{ /* Llib/thread.scm 440 */
 obj_t BgL_oclassz00_7720;
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1937z00_7722; long BgL_arg1938z00_7723;
BgL_arg1937z00_7722 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 440 */
 long BgL_arg1939z00_7724;
BgL_arg1939z00_7724 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7700); 
BgL_arg1938z00_7723 = 
(BgL_arg1939z00_7724-OBJECT_TYPE); } 
BgL_oclassz00_7720 = 
VECTOR_REF(BgL_arg1937z00_7722,BgL_arg1938z00_7723); } 
{ /* Llib/thread.scm 440 */
 bool_t BgL__ortest_1147z00_7730;
BgL__ortest_1147z00_7730 = 
(BgL_classz00_7698==BgL_oclassz00_7720); 
if(BgL__ortest_1147z00_7730)
{ /* Llib/thread.scm 440 */
BgL_res2044z00_7716 = BgL__ortest_1147z00_7730; }  else 
{ /* Llib/thread.scm 440 */
 long BgL_odepthz00_7731;
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1927z00_7732;
BgL_arg1927z00_7732 = 
(BgL_oclassz00_7720); 
BgL_odepthz00_7731 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7732); } 
if(
(BgL_arg1933z00_7701<BgL_odepthz00_7731))
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1925z00_7736;
{ /* Llib/thread.scm 440 */
 obj_t BgL_arg1926z00_7737;
BgL_arg1926z00_7737 = 
(BgL_oclassz00_7720); 
BgL_arg1925z00_7736 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7737, BgL_arg1933z00_7701); } 
BgL_res2044z00_7716 = 
(BgL_arg1925z00_7736==BgL_classz00_7698); }  else 
{ /* Llib/thread.scm 440 */
BgL_res2044z00_7716 = ((bool_t)0); } } } } 
BgL_test3736z00_12340 = BgL_res2044z00_7716; } }  else 
{ /* Llib/thread.scm 440 */
BgL_test3736z00_12340 = ((bool_t)0)
; } } 
if(BgL_test3736z00_12340)
{ /* Llib/thread.scm 440 */
BgL_auxz00_12339 = 
((BgL_threadz00_bglt)BgL_thz00_3748)
; }  else 
{ 
 obj_t BgL_auxz00_12365;
BgL_auxz00_12365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(18877L), BGl_string2788z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_thz00_3748); 
FAILURE(BgL_auxz00_12365,BFALSE,BFALSE);} } 
return 
BGl_threadzd2namezd2setz12z12zz__threadz00(BgL_auxz00_12339, BgL_auxz00_12369);} } 

}



/* %user-current-thread */
BGL_EXPORTED_DEF obj_t BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_threadz00_bglt BgL_oz00_48)
{
{ /* Llib/thread.scm 451 */
{ /* Llib/thread.scm 451 */
 obj_t BgL_method1228z00_1669;
{ /* Llib/thread.scm 451 */
 obj_t BgL_res2031z00_3010;
{ /* Llib/thread.scm 451 */
 long BgL_objzd2classzd2numz00_2981;
BgL_objzd2classzd2numz00_2981 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_oz00_48)); 
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1934z00_2982;
BgL_arg1934z00_2982 = 
PROCEDURE_REF(BGl_z52userzd2currentzd2threadzd2envz80zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 451 */
 int BgL_offsetz00_2985;
BgL_offsetz00_2985 = 
(int)(BgL_objzd2classzd2numz00_2981); 
{ /* Llib/thread.scm 451 */
 long BgL_offsetz00_2986;
BgL_offsetz00_2986 = 
(
(long)(BgL_offsetz00_2985)-OBJECT_TYPE); 
{ /* Llib/thread.scm 451 */
 long BgL_modz00_2987;
BgL_modz00_2987 = 
(BgL_offsetz00_2986 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 451 */
 long BgL_restz00_2989;
BgL_restz00_2989 = 
(BgL_offsetz00_2986 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 451 */

{ /* Llib/thread.scm 451 */
 obj_t BgL_bucketz00_2991;
BgL_bucketz00_2991 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_2982),BgL_modz00_2987); 
BgL_res2031z00_3010 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_2991),BgL_restz00_2989); } } } } } } } } 
BgL_method1228z00_1669 = BgL_res2031z00_3010; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1228z00_1669, 1))
{ /* Llib/thread.scm 451 */
return 
BGL_PROCEDURE_CALL1(BgL_method1228z00_1669, 
((obj_t)BgL_oz00_48));}  else 
{ /* Llib/thread.scm 451 */
FAILURE(BGl_string2789z00zz__threadz00,BGl_list2790z00zz__threadz00,BgL_method1228z00_1669);} } } 

}



/* &%user-current-thread */
obj_t BGl_z62z52userzd2currentzd2threadz30zz__threadz00(obj_t BgL_envz00_3750, obj_t BgL_oz00_3751)
{
{ /* Llib/thread.scm 451 */
{ /* Llib/thread.scm 451 */
 BgL_threadz00_bglt BgL_auxz00_12410;
{ /* Llib/thread.scm 451 */
 bool_t BgL_test3743z00_12411;
{ /* Llib/thread.scm 451 */
 obj_t BgL_classz00_7738;
BgL_classz00_7738 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3751))
{ /* Llib/thread.scm 451 */
 BgL_objectz00_bglt BgL_arg1932z00_7740; long BgL_arg1933z00_7741;
BgL_arg1932z00_7740 = 
(BgL_objectz00_bglt)(BgL_oz00_3751); 
BgL_arg1933z00_7741 = 
BGL_CLASS_DEPTH(BgL_classz00_7738); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 451 */
 long BgL_idxz00_7749;
BgL_idxz00_7749 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7740); 
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1923z00_7750;
{ /* Llib/thread.scm 451 */
 long BgL_arg1924z00_7751;
BgL_arg1924z00_7751 = 
(BgL_idxz00_7749+BgL_arg1933z00_7741); 
BgL_arg1923z00_7750 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7751); } 
BgL_test3743z00_12411 = 
(BgL_arg1923z00_7750==BgL_classz00_7738); } }  else 
{ /* Llib/thread.scm 451 */
 bool_t BgL_res2044z00_7756;
{ /* Llib/thread.scm 451 */
 obj_t BgL_oclassz00_7760;
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1937z00_7762; long BgL_arg1938z00_7763;
BgL_arg1937z00_7762 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 451 */
 long BgL_arg1939z00_7764;
BgL_arg1939z00_7764 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7740); 
BgL_arg1938z00_7763 = 
(BgL_arg1939z00_7764-OBJECT_TYPE); } 
BgL_oclassz00_7760 = 
VECTOR_REF(BgL_arg1937z00_7762,BgL_arg1938z00_7763); } 
{ /* Llib/thread.scm 451 */
 bool_t BgL__ortest_1147z00_7770;
BgL__ortest_1147z00_7770 = 
(BgL_classz00_7738==BgL_oclassz00_7760); 
if(BgL__ortest_1147z00_7770)
{ /* Llib/thread.scm 451 */
BgL_res2044z00_7756 = BgL__ortest_1147z00_7770; }  else 
{ /* Llib/thread.scm 451 */
 long BgL_odepthz00_7771;
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1927z00_7772;
BgL_arg1927z00_7772 = 
(BgL_oclassz00_7760); 
BgL_odepthz00_7771 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7772); } 
if(
(BgL_arg1933z00_7741<BgL_odepthz00_7771))
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1925z00_7776;
{ /* Llib/thread.scm 451 */
 obj_t BgL_arg1926z00_7777;
BgL_arg1926z00_7777 = 
(BgL_oclassz00_7760); 
BgL_arg1925z00_7776 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7777, BgL_arg1933z00_7741); } 
BgL_res2044z00_7756 = 
(BgL_arg1925z00_7776==BgL_classz00_7738); }  else 
{ /* Llib/thread.scm 451 */
BgL_res2044z00_7756 = ((bool_t)0); } } } } 
BgL_test3743z00_12411 = BgL_res2044z00_7756; } }  else 
{ /* Llib/thread.scm 451 */
BgL_test3743z00_12411 = ((bool_t)0)
; } } 
if(BgL_test3743z00_12411)
{ /* Llib/thread.scm 451 */
BgL_auxz00_12410 = 
((BgL_threadz00_bglt)BgL_oz00_3751)
; }  else 
{ 
 obj_t BgL_auxz00_12436;
BgL_auxz00_12436 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(19506L), BGl_string2795z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3751); 
FAILURE(BgL_auxz00_12436,BFALSE,BFALSE);} } 
return 
BGl_z52userzd2currentzd2threadz52zz__threadz00(BgL_auxz00_12410);} } 

}



/* %user-thread-sleep! */
BGL_EXPORTED_DEF obj_t BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_threadz00_bglt BgL_oz00_49, obj_t BgL_dz00_50)
{
{ /* Llib/thread.scm 473 */
{ /* Llib/thread.scm 473 */
 obj_t BgL_method1230z00_1670;
{ /* Llib/thread.scm 473 */
 obj_t BgL_res2036z00_3041;
{ /* Llib/thread.scm 473 */
 long BgL_objzd2classzd2numz00_3012;
BgL_objzd2classzd2numz00_3012 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_oz00_49)); 
{ /* Llib/thread.scm 473 */
 obj_t BgL_arg1934z00_3013;
BgL_arg1934z00_3013 = 
PROCEDURE_REF(BGl_z52userzd2threadzd2sleepz12zd2envz92zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 473 */
 int BgL_offsetz00_3016;
BgL_offsetz00_3016 = 
(int)(BgL_objzd2classzd2numz00_3012); 
{ /* Llib/thread.scm 473 */
 long BgL_offsetz00_3017;
BgL_offsetz00_3017 = 
(
(long)(BgL_offsetz00_3016)-OBJECT_TYPE); 
{ /* Llib/thread.scm 473 */
 long BgL_modz00_3018;
BgL_modz00_3018 = 
(BgL_offsetz00_3017 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 473 */
 long BgL_restz00_3020;
BgL_restz00_3020 = 
(BgL_offsetz00_3017 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 473 */

{ /* Llib/thread.scm 473 */
 obj_t BgL_bucketz00_3022;
BgL_bucketz00_3022 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_3013),BgL_modz00_3018); 
BgL_res2036z00_3041 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_3022),BgL_restz00_3020); } } } } } } } } 
BgL_method1230z00_1670 = BgL_res2036z00_3041; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1230z00_1670, 2))
{ /* Llib/thread.scm 473 */
return 
BGL_PROCEDURE_CALL2(BgL_method1230z00_1670, 
((obj_t)BgL_oz00_49), BgL_dz00_50);}  else 
{ /* Llib/thread.scm 473 */
FAILURE(BGl_string2796z00zz__threadz00,BGl_list2797z00zz__threadz00,BgL_method1230z00_1670);} } } 

}



/* &%user-thread-sleep! */
obj_t BGl_z62z52userzd2threadzd2sleepz12z22zz__threadz00(obj_t BgL_envz00_3752, obj_t BgL_oz00_3753, obj_t BgL_dz00_3754)
{
{ /* Llib/thread.scm 473 */
{ /* Llib/thread.scm 473 */
 BgL_threadz00_bglt BgL_auxz00_12475;
{ /* Llib/thread.scm 473 */
 bool_t BgL_test3749z00_12476;
{ /* Llib/thread.scm 473 */
 obj_t BgL_classz00_7778;
BgL_classz00_7778 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3753))
{ /* Llib/thread.scm 473 */
 BgL_objectz00_bglt BgL_arg1932z00_7780; long BgL_arg1933z00_7781;
BgL_arg1932z00_7780 = 
(BgL_objectz00_bglt)(BgL_oz00_3753); 
BgL_arg1933z00_7781 = 
BGL_CLASS_DEPTH(BgL_classz00_7778); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 473 */
 long BgL_idxz00_7789;
BgL_idxz00_7789 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7780); 
{ /* Llib/thread.scm 473 */
 obj_t BgL_arg1923z00_7790;
{ /* Llib/thread.scm 473 */
 long BgL_arg1924z00_7791;
BgL_arg1924z00_7791 = 
(BgL_idxz00_7789+BgL_arg1933z00_7781); 
BgL_arg1923z00_7790 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7791); } 
BgL_test3749z00_12476 = 
(BgL_arg1923z00_7790==BgL_classz00_7778); } }  else 
{ /* Llib/thread.scm 473 */
 bool_t BgL_res2044z00_7796;
{ /* Llib/thread.scm 473 */
 obj_t BgL_oclassz00_7800;
{ /* Llib/thread.scm 473 */
 obj_t BgL_arg1937z00_7802; long BgL_arg1938z00_7803;
BgL_arg1937z00_7802 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 473 */
 long BgL_arg1939z00_7804;
BgL_arg1939z00_7804 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7780); 
BgL_arg1938z00_7803 = 
(BgL_arg1939z00_7804-OBJECT_TYPE); } 
BgL_oclassz00_7800 = 
VECTOR_REF(BgL_arg1937z00_7802,BgL_arg1938z00_7803); } 
{ /* Llib/thread.scm 473 */
 bool_t BgL__ortest_1147z00_7810;
BgL__ortest_1147z00_7810 = 
(BgL_classz00_7778==BgL_oclassz00_7800); 
if(BgL__ortest_1147z00_7810)
{ /* Llib/thread.scm 473 */
BgL_res2044z00_7796 = BgL__ortest_1147z00_7810; }  else 
{ /* Llib/thread.scm 473 */
 long BgL_odepthz00_7811;
{ /* Llib/thread.scm 473 */
 obj_t BgL_arg1927z00_7812;
BgL_arg1927z00_7812 = 
(BgL_oclassz00_7800); 
BgL_odepthz00_7811 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7812); } 
if(
(BgL_arg1933z00_7781<BgL_odepthz00_7811))
{ /* Llib/thread.scm 473 */
 obj_t BgL_arg1925z00_7816;
{ /* Llib/thread.scm 473 */
 obj_t BgL_arg1926z00_7817;
BgL_arg1926z00_7817 = 
(BgL_oclassz00_7800); 
BgL_arg1925z00_7816 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7817, BgL_arg1933z00_7781); } 
BgL_res2044z00_7796 = 
(BgL_arg1925z00_7816==BgL_classz00_7778); }  else 
{ /* Llib/thread.scm 473 */
BgL_res2044z00_7796 = ((bool_t)0); } } } } 
BgL_test3749z00_12476 = BgL_res2044z00_7796; } }  else 
{ /* Llib/thread.scm 473 */
BgL_test3749z00_12476 = ((bool_t)0)
; } } 
if(BgL_test3749z00_12476)
{ /* Llib/thread.scm 473 */
BgL_auxz00_12475 = 
((BgL_threadz00_bglt)BgL_oz00_3753)
; }  else 
{ 
 obj_t BgL_auxz00_12501;
BgL_auxz00_12501 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(20480L), BGl_string2802z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3753); 
FAILURE(BgL_auxz00_12501,BFALSE,BFALSE);} } 
return 
BGl_z52userzd2threadzd2sleepz12z40zz__threadz00(BgL_auxz00_12475, BgL_dz00_3754);} } 

}



/* %user-thread-yield! */
BGL_EXPORTED_DEF obj_t BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_threadz00_bglt BgL_oz00_52)
{
{ /* Llib/thread.scm 500 */
{ /* Llib/thread.scm 500 */
 obj_t BgL_method1232z00_1671;
{ /* Llib/thread.scm 500 */
 obj_t BgL_res2041z00_3072;
{ /* Llib/thread.scm 500 */
 long BgL_objzd2classzd2numz00_3043;
BgL_objzd2classzd2numz00_3043 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_oz00_52)); 
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1934z00_3044;
BgL_arg1934z00_3044 = 
PROCEDURE_REF(BGl_z52userzd2threadzd2yieldz12zd2envz92zz__threadz00, 
(int)(1L)); 
{ /* Llib/thread.scm 500 */
 int BgL_offsetz00_3047;
BgL_offsetz00_3047 = 
(int)(BgL_objzd2classzd2numz00_3043); 
{ /* Llib/thread.scm 500 */
 long BgL_offsetz00_3048;
BgL_offsetz00_3048 = 
(
(long)(BgL_offsetz00_3047)-OBJECT_TYPE); 
{ /* Llib/thread.scm 500 */
 long BgL_modz00_3049;
BgL_modz00_3049 = 
(BgL_offsetz00_3048 >> 
(int)(
(long)(
(int)(4L)))); 
{ /* Llib/thread.scm 500 */
 long BgL_restz00_3051;
BgL_restz00_3051 = 
(BgL_offsetz00_3048 & 
(long)(
(int)(
(
(long)(
(int)(
(1L << 
(int)(
(long)(
(int)(4L))))))-1L)))); 
{ /* Llib/thread.scm 500 */

{ /* Llib/thread.scm 500 */
 obj_t BgL_bucketz00_3053;
BgL_bucketz00_3053 = 
VECTOR_REF(
((obj_t)BgL_arg1934z00_3044),BgL_modz00_3049); 
BgL_res2041z00_3072 = 
VECTOR_REF(
((obj_t)BgL_bucketz00_3053),BgL_restz00_3051); } } } } } } } } 
BgL_method1232z00_1671 = BgL_res2041z00_3072; } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_method1232z00_1671, 1))
{ /* Llib/thread.scm 500 */
return 
BGL_PROCEDURE_CALL1(BgL_method1232z00_1671, 
((obj_t)BgL_oz00_52));}  else 
{ /* Llib/thread.scm 500 */
FAILURE(BGl_string2803z00zz__threadz00,BGl_list2804z00zz__threadz00,BgL_method1232z00_1671);} } } 

}



/* &%user-thread-yield! */
obj_t BGl_z62z52userzd2threadzd2yieldz12z22zz__threadz00(obj_t BgL_envz00_3755, obj_t BgL_oz00_3756)
{
{ /* Llib/thread.scm 500 */
{ /* Llib/thread.scm 500 */
 BgL_threadz00_bglt BgL_auxz00_12539;
{ /* Llib/thread.scm 500 */
 bool_t BgL_test3755z00_12540;
{ /* Llib/thread.scm 500 */
 obj_t BgL_classz00_7818;
BgL_classz00_7818 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3756))
{ /* Llib/thread.scm 500 */
 BgL_objectz00_bglt BgL_arg1932z00_7820; long BgL_arg1933z00_7821;
BgL_arg1932z00_7820 = 
(BgL_objectz00_bglt)(BgL_oz00_3756); 
BgL_arg1933z00_7821 = 
BGL_CLASS_DEPTH(BgL_classz00_7818); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 500 */
 long BgL_idxz00_7829;
BgL_idxz00_7829 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7820); 
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1923z00_7830;
{ /* Llib/thread.scm 500 */
 long BgL_arg1924z00_7831;
BgL_arg1924z00_7831 = 
(BgL_idxz00_7829+BgL_arg1933z00_7821); 
BgL_arg1923z00_7830 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7831); } 
BgL_test3755z00_12540 = 
(BgL_arg1923z00_7830==BgL_classz00_7818); } }  else 
{ /* Llib/thread.scm 500 */
 bool_t BgL_res2044z00_7836;
{ /* Llib/thread.scm 500 */
 obj_t BgL_oclassz00_7840;
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1937z00_7842; long BgL_arg1938z00_7843;
BgL_arg1937z00_7842 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 500 */
 long BgL_arg1939z00_7844;
BgL_arg1939z00_7844 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7820); 
BgL_arg1938z00_7843 = 
(BgL_arg1939z00_7844-OBJECT_TYPE); } 
BgL_oclassz00_7840 = 
VECTOR_REF(BgL_arg1937z00_7842,BgL_arg1938z00_7843); } 
{ /* Llib/thread.scm 500 */
 bool_t BgL__ortest_1147z00_7850;
BgL__ortest_1147z00_7850 = 
(BgL_classz00_7818==BgL_oclassz00_7840); 
if(BgL__ortest_1147z00_7850)
{ /* Llib/thread.scm 500 */
BgL_res2044z00_7836 = BgL__ortest_1147z00_7850; }  else 
{ /* Llib/thread.scm 500 */
 long BgL_odepthz00_7851;
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1927z00_7852;
BgL_arg1927z00_7852 = 
(BgL_oclassz00_7840); 
BgL_odepthz00_7851 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7852); } 
if(
(BgL_arg1933z00_7821<BgL_odepthz00_7851))
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1925z00_7856;
{ /* Llib/thread.scm 500 */
 obj_t BgL_arg1926z00_7857;
BgL_arg1926z00_7857 = 
(BgL_oclassz00_7840); 
BgL_arg1925z00_7856 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7857, BgL_arg1933z00_7821); } 
BgL_res2044z00_7836 = 
(BgL_arg1925z00_7856==BgL_classz00_7818); }  else 
{ /* Llib/thread.scm 500 */
BgL_res2044z00_7836 = ((bool_t)0); } } } } 
BgL_test3755z00_12540 = BgL_res2044z00_7836; } }  else 
{ /* Llib/thread.scm 500 */
BgL_test3755z00_12540 = ((bool_t)0)
; } } 
if(BgL_test3755z00_12540)
{ /* Llib/thread.scm 500 */
BgL_auxz00_12539 = 
((BgL_threadz00_bglt)BgL_oz00_3756)
; }  else 
{ 
 obj_t BgL_auxz00_12565;
BgL_auxz00_12565 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(21576L), BGl_string2807z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3756); 
FAILURE(BgL_auxz00_12565,BFALSE,BFALSE);} } 
return 
BGl_z52userzd2threadzd2yieldz12z40zz__threadz00(BgL_auxz00_12539);} } 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_tbzd2makezd2threadzd2envzd2zz__threadz00, BGl_nothreadzd2backendzd2zz__threadz00, BGl_proc2808z00zz__threadz00, BGl_string2664z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_tbzd2currentzd2threadzd2envzd2zz__threadz00, BGl_nothreadzd2backendzd2zz__threadz00, BGl_proc2809z00zz__threadz00, BGl_string2810z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_objectzd2displayzd2envz00zz__objectz00, BGl_threadz00zz__threadz00, BGl_proc2811z00zz__threadz00, BGl_string2812z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_objectzd2writezd2envz00zz__objectz00, BGl_threadz00zz__threadz00, BGl_proc2813z00zz__threadz00, BGl_string2814z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_objectzd2printzd2envz00zz__objectz00, BGl_threadz00zz__threadz00, BGl_proc2815z00zz__threadz00, BGl_string2816z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2startz12zd2envz12zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2817z00zz__threadz00, BGl_string2698z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2startzd2joinablez12zd2envzc0zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2818z00zz__threadz00, BGl_string2819z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2joinz12zd2envz12zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2820z00zz__threadz00, BGl_string2730z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2terminatez12zd2envz12zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2821z00zz__threadz00, BGl_string2822z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2killz12zd2envz12zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2823z00zz__threadz00, BGl_string2824z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2specificzd2envz00zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2825z00zz__threadz00, BGl_string2826z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2specificzd2setz12zd2envzc0zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2827z00zz__threadz00, BGl_string2828z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2cleanupzd2envz00zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2829z00zz__threadz00, BGl_string2830z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2cleanupzd2setz12zd2envzc0zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2831z00zz__threadz00, BGl_string2832z00zz__threadz00); 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2namezd2envz00zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2833z00zz__threadz00, BGl_string2778z00zz__threadz00); 
return 
BGl_genericzd2addzd2methodz12z12zz__objectz00(BGl_threadzd2namezd2setz12zd2envzc0zz__threadz00, BGl_nothreadz00zz__threadz00, BGl_proc2834z00zz__threadz00, BGl_string2835z00zz__threadz00);} 

}



/* &thread-name-set!-not1254 */
obj_t BGl_z62threadzd2namezd2setz12zd2not1254za2zz__threadz00(obj_t BgL_envz00_3776, obj_t BgL_thz00_3777, obj_t BgL_vz00_3778)
{
{ /* Llib/thread.scm 608 */
{ /* Llib/thread.scm 609 */
 BgL_nothreadz00_bglt BgL_thz00_7898;
{ /* Llib/thread.scm 609 */
 bool_t BgL_test3760z00_12586;
{ /* Llib/thread.scm 609 */
 obj_t BgL_classz00_7858;
BgL_classz00_7858 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3777))
{ /* Llib/thread.scm 609 */
 BgL_objectz00_bglt BgL_arg1932z00_7860; long BgL_arg1933z00_7861;
BgL_arg1932z00_7860 = 
(BgL_objectz00_bglt)(BgL_thz00_3777); 
BgL_arg1933z00_7861 = 
BGL_CLASS_DEPTH(BgL_classz00_7858); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 609 */
 long BgL_idxz00_7869;
BgL_idxz00_7869 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7860); 
{ /* Llib/thread.scm 609 */
 obj_t BgL_arg1923z00_7870;
{ /* Llib/thread.scm 609 */
 long BgL_arg1924z00_7871;
BgL_arg1924z00_7871 = 
(BgL_idxz00_7869+BgL_arg1933z00_7861); 
BgL_arg1923z00_7870 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7871); } 
BgL_test3760z00_12586 = 
(BgL_arg1923z00_7870==BgL_classz00_7858); } }  else 
{ /* Llib/thread.scm 609 */
 bool_t BgL_res2044z00_7876;
{ /* Llib/thread.scm 609 */
 obj_t BgL_oclassz00_7880;
{ /* Llib/thread.scm 609 */
 obj_t BgL_arg1937z00_7882; long BgL_arg1938z00_7883;
BgL_arg1937z00_7882 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 609 */
 long BgL_arg1939z00_7884;
BgL_arg1939z00_7884 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7860); 
BgL_arg1938z00_7883 = 
(BgL_arg1939z00_7884-OBJECT_TYPE); } 
BgL_oclassz00_7880 = 
VECTOR_REF(BgL_arg1937z00_7882,BgL_arg1938z00_7883); } 
{ /* Llib/thread.scm 609 */
 bool_t BgL__ortest_1147z00_7890;
BgL__ortest_1147z00_7890 = 
(BgL_classz00_7858==BgL_oclassz00_7880); 
if(BgL__ortest_1147z00_7890)
{ /* Llib/thread.scm 609 */
BgL_res2044z00_7876 = BgL__ortest_1147z00_7890; }  else 
{ /* Llib/thread.scm 609 */
 long BgL_odepthz00_7891;
{ /* Llib/thread.scm 609 */
 obj_t BgL_arg1927z00_7892;
BgL_arg1927z00_7892 = 
(BgL_oclassz00_7880); 
BgL_odepthz00_7891 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7892); } 
if(
(BgL_arg1933z00_7861<BgL_odepthz00_7891))
{ /* Llib/thread.scm 609 */
 obj_t BgL_arg1925z00_7896;
{ /* Llib/thread.scm 609 */
 obj_t BgL_arg1926z00_7897;
BgL_arg1926z00_7897 = 
(BgL_oclassz00_7880); 
BgL_arg1925z00_7896 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7897, BgL_arg1933z00_7861); } 
BgL_res2044z00_7876 = 
(BgL_arg1925z00_7896==BgL_classz00_7858); }  else 
{ /* Llib/thread.scm 609 */
BgL_res2044z00_7876 = ((bool_t)0); } } } } 
BgL_test3760z00_12586 = BgL_res2044z00_7876; } }  else 
{ /* Llib/thread.scm 609 */
BgL_test3760z00_12586 = ((bool_t)0)
; } } 
if(BgL_test3760z00_12586)
{ /* Llib/thread.scm 609 */
BgL_thz00_7898 = 
((BgL_nothreadz00_bglt)BgL_thz00_3777); }  else 
{ 
 obj_t BgL_auxz00_12611;
BgL_auxz00_12611 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(26595L), BGl_string2837z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3777); 
FAILURE(BgL_auxz00_12611,BFALSE,BFALSE);} } 
{ 
 obj_t BgL_auxz00_12615;
if(
STRINGP(BgL_vz00_3778))
{ /* Llib/thread.scm 609 */
BgL_auxz00_12615 = BgL_vz00_3778
; }  else 
{ 
 obj_t BgL_auxz00_12618;
BgL_auxz00_12618 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(26629L), BGl_string2836z00zz__threadz00, BGl_string2420z00zz__threadz00, BgL_vz00_3778); 
FAILURE(BgL_auxz00_12618,BFALSE,BFALSE);} 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_7898))->BgL_z52namez52)=((obj_t)BgL_auxz00_12615),BUNSPEC);} } } 

}



/* &thread-name-nothread1252 */
obj_t BGl_z62threadzd2namezd2nothread1252z62zz__threadz00(obj_t BgL_envz00_3779, obj_t BgL_thz00_3780)
{
{ /* Llib/thread.scm 602 */
{ /* Llib/thread.scm 603 */
 BgL_nothreadz00_bglt BgL_thz00_7939;
{ /* Llib/thread.scm 603 */
 bool_t BgL_test3766z00_12623;
{ /* Llib/thread.scm 603 */
 obj_t BgL_classz00_7899;
BgL_classz00_7899 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3780))
{ /* Llib/thread.scm 603 */
 BgL_objectz00_bglt BgL_arg1932z00_7901; long BgL_arg1933z00_7902;
BgL_arg1932z00_7901 = 
(BgL_objectz00_bglt)(BgL_thz00_3780); 
BgL_arg1933z00_7902 = 
BGL_CLASS_DEPTH(BgL_classz00_7899); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 603 */
 long BgL_idxz00_7910;
BgL_idxz00_7910 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7901); 
{ /* Llib/thread.scm 603 */
 obj_t BgL_arg1923z00_7911;
{ /* Llib/thread.scm 603 */
 long BgL_arg1924z00_7912;
BgL_arg1924z00_7912 = 
(BgL_idxz00_7910+BgL_arg1933z00_7902); 
BgL_arg1923z00_7911 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7912); } 
BgL_test3766z00_12623 = 
(BgL_arg1923z00_7911==BgL_classz00_7899); } }  else 
{ /* Llib/thread.scm 603 */
 bool_t BgL_res2044z00_7917;
{ /* Llib/thread.scm 603 */
 obj_t BgL_oclassz00_7921;
{ /* Llib/thread.scm 603 */
 obj_t BgL_arg1937z00_7923; long BgL_arg1938z00_7924;
BgL_arg1937z00_7923 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 603 */
 long BgL_arg1939z00_7925;
BgL_arg1939z00_7925 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7901); 
BgL_arg1938z00_7924 = 
(BgL_arg1939z00_7925-OBJECT_TYPE); } 
BgL_oclassz00_7921 = 
VECTOR_REF(BgL_arg1937z00_7923,BgL_arg1938z00_7924); } 
{ /* Llib/thread.scm 603 */
 bool_t BgL__ortest_1147z00_7931;
BgL__ortest_1147z00_7931 = 
(BgL_classz00_7899==BgL_oclassz00_7921); 
if(BgL__ortest_1147z00_7931)
{ /* Llib/thread.scm 603 */
BgL_res2044z00_7917 = BgL__ortest_1147z00_7931; }  else 
{ /* Llib/thread.scm 603 */
 long BgL_odepthz00_7932;
{ /* Llib/thread.scm 603 */
 obj_t BgL_arg1927z00_7933;
BgL_arg1927z00_7933 = 
(BgL_oclassz00_7921); 
BgL_odepthz00_7932 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7933); } 
if(
(BgL_arg1933z00_7902<BgL_odepthz00_7932))
{ /* Llib/thread.scm 603 */
 obj_t BgL_arg1925z00_7937;
{ /* Llib/thread.scm 603 */
 obj_t BgL_arg1926z00_7938;
BgL_arg1926z00_7938 = 
(BgL_oclassz00_7921); 
BgL_arg1925z00_7937 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7938, BgL_arg1933z00_7902); } 
BgL_res2044z00_7917 = 
(BgL_arg1925z00_7937==BgL_classz00_7899); }  else 
{ /* Llib/thread.scm 603 */
BgL_res2044z00_7917 = ((bool_t)0); } } } } 
BgL_test3766z00_12623 = BgL_res2044z00_7917; } }  else 
{ /* Llib/thread.scm 603 */
BgL_test3766z00_12623 = ((bool_t)0)
; } } 
if(BgL_test3766z00_12623)
{ /* Llib/thread.scm 603 */
BgL_thz00_7939 = 
((BgL_nothreadz00_bglt)BgL_thz00_3780); }  else 
{ 
 obj_t BgL_auxz00_12648;
BgL_auxz00_12648 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(26278L), BGl_string2838z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3780); 
FAILURE(BgL_auxz00_12648,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_7939))->BgL_z52namez52);} } 

}



/* &thread-cleanup-set!-1250 */
obj_t BGl_z62threadzd2cleanupzd2setz12zd21250za2zz__threadz00(obj_t BgL_envz00_3781, obj_t BgL_thz00_3782, obj_t BgL_vz00_3783)
{
{ /* Llib/thread.scm 596 */
{ /* Llib/thread.scm 597 */
 BgL_nothreadz00_bglt BgL_thz00_7980;
{ /* Llib/thread.scm 597 */
 bool_t BgL_test3771z00_12653;
{ /* Llib/thread.scm 597 */
 obj_t BgL_classz00_7940;
BgL_classz00_7940 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3782))
{ /* Llib/thread.scm 597 */
 BgL_objectz00_bglt BgL_arg1932z00_7942; long BgL_arg1933z00_7943;
BgL_arg1932z00_7942 = 
(BgL_objectz00_bglt)(BgL_thz00_3782); 
BgL_arg1933z00_7943 = 
BGL_CLASS_DEPTH(BgL_classz00_7940); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 597 */
 long BgL_idxz00_7951;
BgL_idxz00_7951 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7942); 
{ /* Llib/thread.scm 597 */
 obj_t BgL_arg1923z00_7952;
{ /* Llib/thread.scm 597 */
 long BgL_arg1924z00_7953;
BgL_arg1924z00_7953 = 
(BgL_idxz00_7951+BgL_arg1933z00_7943); 
BgL_arg1923z00_7952 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7953); } 
BgL_test3771z00_12653 = 
(BgL_arg1923z00_7952==BgL_classz00_7940); } }  else 
{ /* Llib/thread.scm 597 */
 bool_t BgL_res2044z00_7958;
{ /* Llib/thread.scm 597 */
 obj_t BgL_oclassz00_7962;
{ /* Llib/thread.scm 597 */
 obj_t BgL_arg1937z00_7964; long BgL_arg1938z00_7965;
BgL_arg1937z00_7964 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 597 */
 long BgL_arg1939z00_7966;
BgL_arg1939z00_7966 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7942); 
BgL_arg1938z00_7965 = 
(BgL_arg1939z00_7966-OBJECT_TYPE); } 
BgL_oclassz00_7962 = 
VECTOR_REF(BgL_arg1937z00_7964,BgL_arg1938z00_7965); } 
{ /* Llib/thread.scm 597 */
 bool_t BgL__ortest_1147z00_7972;
BgL__ortest_1147z00_7972 = 
(BgL_classz00_7940==BgL_oclassz00_7962); 
if(BgL__ortest_1147z00_7972)
{ /* Llib/thread.scm 597 */
BgL_res2044z00_7958 = BgL__ortest_1147z00_7972; }  else 
{ /* Llib/thread.scm 597 */
 long BgL_odepthz00_7973;
{ /* Llib/thread.scm 597 */
 obj_t BgL_arg1927z00_7974;
BgL_arg1927z00_7974 = 
(BgL_oclassz00_7962); 
BgL_odepthz00_7973 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_7974); } 
if(
(BgL_arg1933z00_7943<BgL_odepthz00_7973))
{ /* Llib/thread.scm 597 */
 obj_t BgL_arg1925z00_7978;
{ /* Llib/thread.scm 597 */
 obj_t BgL_arg1926z00_7979;
BgL_arg1926z00_7979 = 
(BgL_oclassz00_7962); 
BgL_arg1925z00_7978 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_7979, BgL_arg1933z00_7943); } 
BgL_res2044z00_7958 = 
(BgL_arg1925z00_7978==BgL_classz00_7940); }  else 
{ /* Llib/thread.scm 597 */
BgL_res2044z00_7958 = ((bool_t)0); } } } } 
BgL_test3771z00_12653 = BgL_res2044z00_7958; } }  else 
{ /* Llib/thread.scm 597 */
BgL_test3771z00_12653 = ((bool_t)0)
; } } 
if(BgL_test3771z00_12653)
{ /* Llib/thread.scm 597 */
BgL_thz00_7980 = 
((BgL_nothreadz00_bglt)BgL_thz00_3782); }  else 
{ 
 obj_t BgL_auxz00_12678;
BgL_auxz00_12678 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(25953L), BGl_string2839z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3782); 
FAILURE(BgL_auxz00_12678,BFALSE,BFALSE);} } 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_7980))->BgL_z52cleanupz52)=((obj_t)BgL_vz00_3783),BUNSPEC);} } 

}



/* &thread-cleanup-nothr1248 */
obj_t BGl_z62threadzd2cleanupzd2nothr1248z62zz__threadz00(obj_t BgL_envz00_3784, obj_t BgL_thz00_3785)
{
{ /* Llib/thread.scm 590 */
{ /* Llib/thread.scm 591 */
 BgL_nothreadz00_bglt BgL_thz00_8021;
{ /* Llib/thread.scm 591 */
 bool_t BgL_test3776z00_12683;
{ /* Llib/thread.scm 591 */
 obj_t BgL_classz00_7981;
BgL_classz00_7981 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3785))
{ /* Llib/thread.scm 591 */
 BgL_objectz00_bglt BgL_arg1932z00_7983; long BgL_arg1933z00_7984;
BgL_arg1932z00_7983 = 
(BgL_objectz00_bglt)(BgL_thz00_3785); 
BgL_arg1933z00_7984 = 
BGL_CLASS_DEPTH(BgL_classz00_7981); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 591 */
 long BgL_idxz00_7992;
BgL_idxz00_7992 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_7983); 
{ /* Llib/thread.scm 591 */
 obj_t BgL_arg1923z00_7993;
{ /* Llib/thread.scm 591 */
 long BgL_arg1924z00_7994;
BgL_arg1924z00_7994 = 
(BgL_idxz00_7992+BgL_arg1933z00_7984); 
BgL_arg1923z00_7993 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_7994); } 
BgL_test3776z00_12683 = 
(BgL_arg1923z00_7993==BgL_classz00_7981); } }  else 
{ /* Llib/thread.scm 591 */
 bool_t BgL_res2044z00_7999;
{ /* Llib/thread.scm 591 */
 obj_t BgL_oclassz00_8003;
{ /* Llib/thread.scm 591 */
 obj_t BgL_arg1937z00_8005; long BgL_arg1938z00_8006;
BgL_arg1937z00_8005 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 591 */
 long BgL_arg1939z00_8007;
BgL_arg1939z00_8007 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_7983); 
BgL_arg1938z00_8006 = 
(BgL_arg1939z00_8007-OBJECT_TYPE); } 
BgL_oclassz00_8003 = 
VECTOR_REF(BgL_arg1937z00_8005,BgL_arg1938z00_8006); } 
{ /* Llib/thread.scm 591 */
 bool_t BgL__ortest_1147z00_8013;
BgL__ortest_1147z00_8013 = 
(BgL_classz00_7981==BgL_oclassz00_8003); 
if(BgL__ortest_1147z00_8013)
{ /* Llib/thread.scm 591 */
BgL_res2044z00_7999 = BgL__ortest_1147z00_8013; }  else 
{ /* Llib/thread.scm 591 */
 long BgL_odepthz00_8014;
{ /* Llib/thread.scm 591 */
 obj_t BgL_arg1927z00_8015;
BgL_arg1927z00_8015 = 
(BgL_oclassz00_8003); 
BgL_odepthz00_8014 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8015); } 
if(
(BgL_arg1933z00_7984<BgL_odepthz00_8014))
{ /* Llib/thread.scm 591 */
 obj_t BgL_arg1925z00_8019;
{ /* Llib/thread.scm 591 */
 obj_t BgL_arg1926z00_8020;
BgL_arg1926z00_8020 = 
(BgL_oclassz00_8003); 
BgL_arg1925z00_8019 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8020, BgL_arg1933z00_7984); } 
BgL_res2044z00_7999 = 
(BgL_arg1925z00_8019==BgL_classz00_7981); }  else 
{ /* Llib/thread.scm 591 */
BgL_res2044z00_7999 = ((bool_t)0); } } } } 
BgL_test3776z00_12683 = BgL_res2044z00_7999; } }  else 
{ /* Llib/thread.scm 591 */
BgL_test3776z00_12683 = ((bool_t)0)
; } } 
if(BgL_test3776z00_12683)
{ /* Llib/thread.scm 591 */
BgL_thz00_8021 = 
((BgL_nothreadz00_bglt)BgL_thz00_3785); }  else 
{ 
 obj_t BgL_auxz00_12708;
BgL_auxz00_12708 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(25627L), BGl_string2840z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3785); 
FAILURE(BgL_auxz00_12708,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8021))->BgL_z52cleanupz52);} } 

}



/* &thread-specific-set!1246 */
obj_t BGl_z62threadzd2specificzd2setz121246z70zz__threadz00(obj_t BgL_envz00_3786, obj_t BgL_thz00_3787, obj_t BgL_vz00_3788)
{
{ /* Llib/thread.scm 584 */
{ /* Llib/thread.scm 585 */
 BgL_nothreadz00_bglt BgL_thz00_8062;
{ /* Llib/thread.scm 585 */
 bool_t BgL_test3781z00_12713;
{ /* Llib/thread.scm 585 */
 obj_t BgL_classz00_8022;
BgL_classz00_8022 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3787))
{ /* Llib/thread.scm 585 */
 BgL_objectz00_bglt BgL_arg1932z00_8024; long BgL_arg1933z00_8025;
BgL_arg1932z00_8024 = 
(BgL_objectz00_bglt)(BgL_thz00_3787); 
BgL_arg1933z00_8025 = 
BGL_CLASS_DEPTH(BgL_classz00_8022); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 585 */
 long BgL_idxz00_8033;
BgL_idxz00_8033 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8024); 
{ /* Llib/thread.scm 585 */
 obj_t BgL_arg1923z00_8034;
{ /* Llib/thread.scm 585 */
 long BgL_arg1924z00_8035;
BgL_arg1924z00_8035 = 
(BgL_idxz00_8033+BgL_arg1933z00_8025); 
BgL_arg1923z00_8034 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8035); } 
BgL_test3781z00_12713 = 
(BgL_arg1923z00_8034==BgL_classz00_8022); } }  else 
{ /* Llib/thread.scm 585 */
 bool_t BgL_res2044z00_8040;
{ /* Llib/thread.scm 585 */
 obj_t BgL_oclassz00_8044;
{ /* Llib/thread.scm 585 */
 obj_t BgL_arg1937z00_8046; long BgL_arg1938z00_8047;
BgL_arg1937z00_8046 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 585 */
 long BgL_arg1939z00_8048;
BgL_arg1939z00_8048 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8024); 
BgL_arg1938z00_8047 = 
(BgL_arg1939z00_8048-OBJECT_TYPE); } 
BgL_oclassz00_8044 = 
VECTOR_REF(BgL_arg1937z00_8046,BgL_arg1938z00_8047); } 
{ /* Llib/thread.scm 585 */
 bool_t BgL__ortest_1147z00_8054;
BgL__ortest_1147z00_8054 = 
(BgL_classz00_8022==BgL_oclassz00_8044); 
if(BgL__ortest_1147z00_8054)
{ /* Llib/thread.scm 585 */
BgL_res2044z00_8040 = BgL__ortest_1147z00_8054; }  else 
{ /* Llib/thread.scm 585 */
 long BgL_odepthz00_8055;
{ /* Llib/thread.scm 585 */
 obj_t BgL_arg1927z00_8056;
BgL_arg1927z00_8056 = 
(BgL_oclassz00_8044); 
BgL_odepthz00_8055 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8056); } 
if(
(BgL_arg1933z00_8025<BgL_odepthz00_8055))
{ /* Llib/thread.scm 585 */
 obj_t BgL_arg1925z00_8060;
{ /* Llib/thread.scm 585 */
 obj_t BgL_arg1926z00_8061;
BgL_arg1926z00_8061 = 
(BgL_oclassz00_8044); 
BgL_arg1925z00_8060 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8061, BgL_arg1933z00_8025); } 
BgL_res2044z00_8040 = 
(BgL_arg1925z00_8060==BgL_classz00_8022); }  else 
{ /* Llib/thread.scm 585 */
BgL_res2044z00_8040 = ((bool_t)0); } } } } 
BgL_test3781z00_12713 = BgL_res2044z00_8040; } }  else 
{ /* Llib/thread.scm 585 */
BgL_test3781z00_12713 = ((bool_t)0)
; } } 
if(BgL_test3781z00_12713)
{ /* Llib/thread.scm 585 */
BgL_thz00_8062 = 
((BgL_nothreadz00_bglt)BgL_thz00_3787); }  else 
{ 
 obj_t BgL_auxz00_12738;
BgL_auxz00_12738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(25297L), BGl_string2841z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3787); 
FAILURE(BgL_auxz00_12738,BFALSE,BFALSE);} } 
return 
((((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8062))->BgL_z52specificz52)=((obj_t)BgL_vz00_3788),BUNSPEC);} } 

}



/* &thread-specific-noth1244 */
obj_t BGl_z62threadzd2specificzd2noth1244z62zz__threadz00(obj_t BgL_envz00_3789, obj_t BgL_thz00_3790)
{
{ /* Llib/thread.scm 578 */
{ /* Llib/thread.scm 579 */
 BgL_nothreadz00_bglt BgL_thz00_8103;
{ /* Llib/thread.scm 579 */
 bool_t BgL_test3786z00_12743;
{ /* Llib/thread.scm 579 */
 obj_t BgL_classz00_8063;
BgL_classz00_8063 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3790))
{ /* Llib/thread.scm 579 */
 BgL_objectz00_bglt BgL_arg1932z00_8065; long BgL_arg1933z00_8066;
BgL_arg1932z00_8065 = 
(BgL_objectz00_bglt)(BgL_thz00_3790); 
BgL_arg1933z00_8066 = 
BGL_CLASS_DEPTH(BgL_classz00_8063); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 579 */
 long BgL_idxz00_8074;
BgL_idxz00_8074 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8065); 
{ /* Llib/thread.scm 579 */
 obj_t BgL_arg1923z00_8075;
{ /* Llib/thread.scm 579 */
 long BgL_arg1924z00_8076;
BgL_arg1924z00_8076 = 
(BgL_idxz00_8074+BgL_arg1933z00_8066); 
BgL_arg1923z00_8075 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8076); } 
BgL_test3786z00_12743 = 
(BgL_arg1923z00_8075==BgL_classz00_8063); } }  else 
{ /* Llib/thread.scm 579 */
 bool_t BgL_res2044z00_8081;
{ /* Llib/thread.scm 579 */
 obj_t BgL_oclassz00_8085;
{ /* Llib/thread.scm 579 */
 obj_t BgL_arg1937z00_8087; long BgL_arg1938z00_8088;
BgL_arg1937z00_8087 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 579 */
 long BgL_arg1939z00_8089;
BgL_arg1939z00_8089 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8065); 
BgL_arg1938z00_8088 = 
(BgL_arg1939z00_8089-OBJECT_TYPE); } 
BgL_oclassz00_8085 = 
VECTOR_REF(BgL_arg1937z00_8087,BgL_arg1938z00_8088); } 
{ /* Llib/thread.scm 579 */
 bool_t BgL__ortest_1147z00_8095;
BgL__ortest_1147z00_8095 = 
(BgL_classz00_8063==BgL_oclassz00_8085); 
if(BgL__ortest_1147z00_8095)
{ /* Llib/thread.scm 579 */
BgL_res2044z00_8081 = BgL__ortest_1147z00_8095; }  else 
{ /* Llib/thread.scm 579 */
 long BgL_odepthz00_8096;
{ /* Llib/thread.scm 579 */
 obj_t BgL_arg1927z00_8097;
BgL_arg1927z00_8097 = 
(BgL_oclassz00_8085); 
BgL_odepthz00_8096 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8097); } 
if(
(BgL_arg1933z00_8066<BgL_odepthz00_8096))
{ /* Llib/thread.scm 579 */
 obj_t BgL_arg1925z00_8101;
{ /* Llib/thread.scm 579 */
 obj_t BgL_arg1926z00_8102;
BgL_arg1926z00_8102 = 
(BgL_oclassz00_8085); 
BgL_arg1925z00_8101 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8102, BgL_arg1933z00_8066); } 
BgL_res2044z00_8081 = 
(BgL_arg1925z00_8101==BgL_classz00_8063); }  else 
{ /* Llib/thread.scm 579 */
BgL_res2044z00_8081 = ((bool_t)0); } } } } 
BgL_test3786z00_12743 = BgL_res2044z00_8081; } }  else 
{ /* Llib/thread.scm 579 */
BgL_test3786z00_12743 = ((bool_t)0)
; } } 
if(BgL_test3786z00_12743)
{ /* Llib/thread.scm 579 */
BgL_thz00_8103 = 
((BgL_nothreadz00_bglt)BgL_thz00_3790); }  else 
{ 
 obj_t BgL_auxz00_12768;
BgL_auxz00_12768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(24968L), BGl_string2842z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3790); 
FAILURE(BgL_auxz00_12768,BFALSE,BFALSE);} } 
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8103))->BgL_z52specificz52);} } 

}



/* &thread-kill!-nothrea1242 */
obj_t BGl_z62threadzd2killz12zd2nothrea1242z70zz__threadz00(obj_t BgL_envz00_3791, obj_t BgL_thz00_3792, obj_t BgL_nz00_3793)
{
{ /* Llib/thread.scm 572 */
{ /* Llib/thread.scm 572 */
 BgL_nothreadz00_bglt BgL_thz00_8144; int BgL_nz00_8145;
{ /* Llib/thread.scm 572 */
 bool_t BgL_test3791z00_12773;
{ /* Llib/thread.scm 572 */
 obj_t BgL_classz00_8104;
BgL_classz00_8104 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3792))
{ /* Llib/thread.scm 572 */
 BgL_objectz00_bglt BgL_arg1932z00_8106; long BgL_arg1933z00_8107;
BgL_arg1932z00_8106 = 
(BgL_objectz00_bglt)(BgL_thz00_3792); 
BgL_arg1933z00_8107 = 
BGL_CLASS_DEPTH(BgL_classz00_8104); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 572 */
 long BgL_idxz00_8115;
BgL_idxz00_8115 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8106); 
{ /* Llib/thread.scm 572 */
 obj_t BgL_arg1923z00_8116;
{ /* Llib/thread.scm 572 */
 long BgL_arg1924z00_8117;
BgL_arg1924z00_8117 = 
(BgL_idxz00_8115+BgL_arg1933z00_8107); 
BgL_arg1923z00_8116 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8117); } 
BgL_test3791z00_12773 = 
(BgL_arg1923z00_8116==BgL_classz00_8104); } }  else 
{ /* Llib/thread.scm 572 */
 bool_t BgL_res2044z00_8122;
{ /* Llib/thread.scm 572 */
 obj_t BgL_oclassz00_8126;
{ /* Llib/thread.scm 572 */
 obj_t BgL_arg1937z00_8128; long BgL_arg1938z00_8129;
BgL_arg1937z00_8128 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 572 */
 long BgL_arg1939z00_8130;
BgL_arg1939z00_8130 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8106); 
BgL_arg1938z00_8129 = 
(BgL_arg1939z00_8130-OBJECT_TYPE); } 
BgL_oclassz00_8126 = 
VECTOR_REF(BgL_arg1937z00_8128,BgL_arg1938z00_8129); } 
{ /* Llib/thread.scm 572 */
 bool_t BgL__ortest_1147z00_8136;
BgL__ortest_1147z00_8136 = 
(BgL_classz00_8104==BgL_oclassz00_8126); 
if(BgL__ortest_1147z00_8136)
{ /* Llib/thread.scm 572 */
BgL_res2044z00_8122 = BgL__ortest_1147z00_8136; }  else 
{ /* Llib/thread.scm 572 */
 long BgL_odepthz00_8137;
{ /* Llib/thread.scm 572 */
 obj_t BgL_arg1927z00_8138;
BgL_arg1927z00_8138 = 
(BgL_oclassz00_8126); 
BgL_odepthz00_8137 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8138); } 
if(
(BgL_arg1933z00_8107<BgL_odepthz00_8137))
{ /* Llib/thread.scm 572 */
 obj_t BgL_arg1925z00_8142;
{ /* Llib/thread.scm 572 */
 obj_t BgL_arg1926z00_8143;
BgL_arg1926z00_8143 = 
(BgL_oclassz00_8126); 
BgL_arg1925z00_8142 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8143, BgL_arg1933z00_8107); } 
BgL_res2044z00_8122 = 
(BgL_arg1925z00_8142==BgL_classz00_8104); }  else 
{ /* Llib/thread.scm 572 */
BgL_res2044z00_8122 = ((bool_t)0); } } } } 
BgL_test3791z00_12773 = BgL_res2044z00_8122; } }  else 
{ /* Llib/thread.scm 572 */
BgL_test3791z00_12773 = ((bool_t)0)
; } } 
if(BgL_test3791z00_12773)
{ /* Llib/thread.scm 572 */
BgL_thz00_8144 = 
((BgL_nothreadz00_bglt)BgL_thz00_3792); }  else 
{ 
 obj_t BgL_auxz00_12798;
BgL_auxz00_12798 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(24629L), BGl_string2843z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3792); 
FAILURE(BgL_auxz00_12798,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 572 */
 obj_t BgL_tmpz00_12802;
if(
INTEGERP(BgL_nz00_3793))
{ /* Llib/thread.scm 572 */
BgL_tmpz00_12802 = BgL_nz00_3793
; }  else 
{ 
 obj_t BgL_auxz00_12805;
BgL_auxz00_12805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(24629L), BGl_string2843z00zz__threadz00, BGl_string2453z00zz__threadz00, BgL_nz00_3793); 
FAILURE(BgL_auxz00_12805,BFALSE,BFALSE);} 
BgL_nz00_8145 = 
CINT(BgL_tmpz00_12802); } 
return BUNSPEC;} } 

}



/* &thread-terminate!-no1240 */
obj_t BGl_z62threadzd2terminatez12zd2no1240z70zz__threadz00(obj_t BgL_envz00_3794, obj_t BgL_thz00_3795)
{
{ /* Llib/thread.scm 564 */
{ /* Llib/thread.scm 565 */
 BgL_nothreadz00_bglt BgL_thz00_8186;
{ /* Llib/thread.scm 565 */
 bool_t BgL_test3797z00_12810;
{ /* Llib/thread.scm 565 */
 obj_t BgL_classz00_8146;
BgL_classz00_8146 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3795))
{ /* Llib/thread.scm 565 */
 BgL_objectz00_bglt BgL_arg1932z00_8148; long BgL_arg1933z00_8149;
BgL_arg1932z00_8148 = 
(BgL_objectz00_bglt)(BgL_thz00_3795); 
BgL_arg1933z00_8149 = 
BGL_CLASS_DEPTH(BgL_classz00_8146); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 565 */
 long BgL_idxz00_8157;
BgL_idxz00_8157 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8148); 
{ /* Llib/thread.scm 565 */
 obj_t BgL_arg1923z00_8158;
{ /* Llib/thread.scm 565 */
 long BgL_arg1924z00_8159;
BgL_arg1924z00_8159 = 
(BgL_idxz00_8157+BgL_arg1933z00_8149); 
BgL_arg1923z00_8158 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8159); } 
BgL_test3797z00_12810 = 
(BgL_arg1923z00_8158==BgL_classz00_8146); } }  else 
{ /* Llib/thread.scm 565 */
 bool_t BgL_res2044z00_8164;
{ /* Llib/thread.scm 565 */
 obj_t BgL_oclassz00_8168;
{ /* Llib/thread.scm 565 */
 obj_t BgL_arg1937z00_8170; long BgL_arg1938z00_8171;
BgL_arg1937z00_8170 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 565 */
 long BgL_arg1939z00_8172;
BgL_arg1939z00_8172 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8148); 
BgL_arg1938z00_8171 = 
(BgL_arg1939z00_8172-OBJECT_TYPE); } 
BgL_oclassz00_8168 = 
VECTOR_REF(BgL_arg1937z00_8170,BgL_arg1938z00_8171); } 
{ /* Llib/thread.scm 565 */
 bool_t BgL__ortest_1147z00_8178;
BgL__ortest_1147z00_8178 = 
(BgL_classz00_8146==BgL_oclassz00_8168); 
if(BgL__ortest_1147z00_8178)
{ /* Llib/thread.scm 565 */
BgL_res2044z00_8164 = BgL__ortest_1147z00_8178; }  else 
{ /* Llib/thread.scm 565 */
 long BgL_odepthz00_8179;
{ /* Llib/thread.scm 565 */
 obj_t BgL_arg1927z00_8180;
BgL_arg1927z00_8180 = 
(BgL_oclassz00_8168); 
BgL_odepthz00_8179 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8180); } 
if(
(BgL_arg1933z00_8149<BgL_odepthz00_8179))
{ /* Llib/thread.scm 565 */
 obj_t BgL_arg1925z00_8184;
{ /* Llib/thread.scm 565 */
 obj_t BgL_arg1926z00_8185;
BgL_arg1926z00_8185 = 
(BgL_oclassz00_8168); 
BgL_arg1925z00_8184 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8185, BgL_arg1933z00_8149); } 
BgL_res2044z00_8164 = 
(BgL_arg1925z00_8184==BgL_classz00_8146); }  else 
{ /* Llib/thread.scm 565 */
BgL_res2044z00_8164 = ((bool_t)0); } } } } 
BgL_test3797z00_12810 = BgL_res2044z00_8164; } }  else 
{ /* Llib/thread.scm 565 */
BgL_test3797z00_12810 = ((bool_t)0)
; } } 
if(BgL_test3797z00_12810)
{ /* Llib/thread.scm 565 */
BgL_thz00_8186 = 
((BgL_nothreadz00_bglt)BgL_thz00_3795); }  else 
{ 
 obj_t BgL_auxz00_12835;
BgL_auxz00_12835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(24306L), BGl_string2849z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3795); 
FAILURE(BgL_auxz00_12835,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 566 */
 bool_t BgL_test3802z00_12839;
{ /* Llib/thread.scm 566 */
 obj_t BgL_tmpz00_12840;
BgL_tmpz00_12840 = 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8186))->BgL_z52cleanupz52); 
BgL_test3802z00_12839 = 
PROCEDUREP(BgL_tmpz00_12840); } 
if(BgL_test3802z00_12839)
{ /* Llib/thread.scm 566 */
 obj_t BgL_fun1576z00_8187;
BgL_fun1576z00_8187 = 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8186))->BgL_z52cleanupz52); 
{ /* Llib/thread.scm 566 */
 obj_t BgL_funz00_8188;
if(
PROCEDUREP(BgL_fun1576z00_8187))
{ /* Llib/thread.scm 566 */
BgL_funz00_8188 = BgL_fun1576z00_8187; }  else 
{ 
 obj_t BgL_auxz00_12846;
BgL_auxz00_12846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(24377L), BGl_string2844z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_fun1576z00_8187); 
FAILURE(BgL_auxz00_12846,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_8188, 0))
{ /* Llib/thread.scm 566 */
(VA_PROCEDUREP( BgL_funz00_8188 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_8188))(BgL_fun1576z00_8187, BEOA) : ((obj_t (*)(obj_t))PROCEDURE_ENTRY(BgL_funz00_8188))(BgL_fun1576z00_8187) ); }  else 
{ /* Llib/thread.scm 566 */
FAILURE(BGl_string2845z00zz__threadz00,BGl_list2846z00zz__threadz00,BgL_funz00_8188);} } }  else 
{ /* Llib/thread.scm 566 */BFALSE; } } 
{ /* Llib/thread.scm 567 */
 obj_t BgL_list1579z00_8189;
BgL_list1579z00_8189 = 
MAKE_YOUNG_PAIR(
BINT(0L), BNIL); 
return 
BGl_exitz00zz__errorz00(BgL_list1579z00_8189);} } } 

}



/* &thread-join!-nothrea1238 */
obj_t BGl_z62threadzd2joinz12zd2nothrea1238z70zz__threadz00(obj_t BgL_envz00_3796, obj_t BgL_thz00_3797, obj_t BgL_timeoutz00_3798)
{
{ /* Llib/thread.scm 555 */
{ /* Llib/thread.scm 556 */
 BgL_nothreadz00_bglt BgL_thz00_8230;
{ /* Llib/thread.scm 556 */
 bool_t BgL_test3805z00_12859;
{ /* Llib/thread.scm 556 */
 obj_t BgL_classz00_8190;
BgL_classz00_8190 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3797))
{ /* Llib/thread.scm 556 */
 BgL_objectz00_bglt BgL_arg1932z00_8192; long BgL_arg1933z00_8193;
BgL_arg1932z00_8192 = 
(BgL_objectz00_bglt)(BgL_thz00_3797); 
BgL_arg1933z00_8193 = 
BGL_CLASS_DEPTH(BgL_classz00_8190); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 556 */
 long BgL_idxz00_8201;
BgL_idxz00_8201 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8192); 
{ /* Llib/thread.scm 556 */
 obj_t BgL_arg1923z00_8202;
{ /* Llib/thread.scm 556 */
 long BgL_arg1924z00_8203;
BgL_arg1924z00_8203 = 
(BgL_idxz00_8201+BgL_arg1933z00_8193); 
BgL_arg1923z00_8202 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8203); } 
BgL_test3805z00_12859 = 
(BgL_arg1923z00_8202==BgL_classz00_8190); } }  else 
{ /* Llib/thread.scm 556 */
 bool_t BgL_res2044z00_8208;
{ /* Llib/thread.scm 556 */
 obj_t BgL_oclassz00_8212;
{ /* Llib/thread.scm 556 */
 obj_t BgL_arg1937z00_8214; long BgL_arg1938z00_8215;
BgL_arg1937z00_8214 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 556 */
 long BgL_arg1939z00_8216;
BgL_arg1939z00_8216 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8192); 
BgL_arg1938z00_8215 = 
(BgL_arg1939z00_8216-OBJECT_TYPE); } 
BgL_oclassz00_8212 = 
VECTOR_REF(BgL_arg1937z00_8214,BgL_arg1938z00_8215); } 
{ /* Llib/thread.scm 556 */
 bool_t BgL__ortest_1147z00_8222;
BgL__ortest_1147z00_8222 = 
(BgL_classz00_8190==BgL_oclassz00_8212); 
if(BgL__ortest_1147z00_8222)
{ /* Llib/thread.scm 556 */
BgL_res2044z00_8208 = BgL__ortest_1147z00_8222; }  else 
{ /* Llib/thread.scm 556 */
 long BgL_odepthz00_8223;
{ /* Llib/thread.scm 556 */
 obj_t BgL_arg1927z00_8224;
BgL_arg1927z00_8224 = 
(BgL_oclassz00_8212); 
BgL_odepthz00_8223 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8224); } 
if(
(BgL_arg1933z00_8193<BgL_odepthz00_8223))
{ /* Llib/thread.scm 556 */
 obj_t BgL_arg1925z00_8228;
{ /* Llib/thread.scm 556 */
 obj_t BgL_arg1926z00_8229;
BgL_arg1926z00_8229 = 
(BgL_oclassz00_8212); 
BgL_arg1925z00_8228 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8229, BgL_arg1933z00_8193); } 
BgL_res2044z00_8208 = 
(BgL_arg1925z00_8228==BgL_classz00_8190); }  else 
{ /* Llib/thread.scm 556 */
BgL_res2044z00_8208 = ((bool_t)0); } } } } 
BgL_test3805z00_12859 = BgL_res2044z00_8208; } }  else 
{ /* Llib/thread.scm 556 */
BgL_test3805z00_12859 = ((bool_t)0)
; } } 
if(BgL_test3805z00_12859)
{ /* Llib/thread.scm 556 */
BgL_thz00_8230 = 
((BgL_nothreadz00_bglt)BgL_thz00_3797); }  else 
{ 
 obj_t BgL_auxz00_12884;
BgL_auxz00_12884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23895L), BGl_string2851z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3797); 
FAILURE(BgL_auxz00_12884,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 557 */
 bool_t BgL_test3810z00_12888;
{ /* Llib/thread.scm 557 */
 obj_t BgL_arg1571z00_8231;
BgL_arg1571z00_8231 = 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8230))->BgL_endzd2exceptionzd2); 
{ /* Llib/thread.scm 557 */
 obj_t BgL_classz00_8232;
BgL_classz00_8232 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_arg1571z00_8231))
{ /* Llib/thread.scm 557 */
 BgL_objectz00_bglt BgL_arg1930z00_8233;
BgL_arg1930z00_8233 = 
(BgL_objectz00_bglt)(BgL_arg1571z00_8231); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 557 */
 long BgL_idxz00_8234;
BgL_idxz00_8234 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1930z00_8233); 
{ /* Llib/thread.scm 557 */
 obj_t BgL_arg1923z00_8235;
{ /* Llib/thread.scm 557 */
 long BgL_arg1924z00_8236;
BgL_arg1924z00_8236 = 
(BgL_idxz00_8234+2L); 
{ /* Llib/thread.scm 557 */
 obj_t BgL_vectorz00_8237;
{ /* Llib/thread.scm 557 */
 obj_t BgL_aux2375z00_8238;
BgL_aux2375z00_8238 = BGl_za2inheritancesza2z00zz__objectz00; 
if(
VECTORP(BgL_aux2375z00_8238))
{ /* Llib/thread.scm 557 */
BgL_vectorz00_8237 = BgL_aux2375z00_8238; }  else 
{ 
 obj_t BgL_auxz00_12899;
BgL_auxz00_12899 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23958L), BGl_string2850z00zz__threadz00, BGl_string2429z00zz__threadz00, BgL_aux2375z00_8238); 
FAILURE(BgL_auxz00_12899,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 557 */
 bool_t BgL_test3814z00_12903;
{ /* Llib/thread.scm 557 */
 long BgL_tmpz00_12904;
BgL_tmpz00_12904 = 
VECTOR_LENGTH(BgL_vectorz00_8237); 
BgL_test3814z00_12903 = 
BOUND_CHECK(BgL_arg1924z00_8236, BgL_tmpz00_12904); } 
if(BgL_test3814z00_12903)
{ /* Llib/thread.scm 557 */
BgL_arg1923z00_8235 = 
VECTOR_REF(BgL_vectorz00_8237,BgL_arg1924z00_8236); }  else 
{ 
 obj_t BgL_auxz00_12908;
BgL_auxz00_12908 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23958L), BGl_string2430z00zz__threadz00, BgL_vectorz00_8237, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_8237)), 
(int)(BgL_arg1924z00_8236)); 
FAILURE(BgL_auxz00_12908,BFALSE,BFALSE);} } } } 
BgL_test3810z00_12888 = 
(BgL_arg1923z00_8235==BgL_classz00_8232); } }  else 
{ /* Llib/thread.scm 557 */
 bool_t BgL_res2042z00_8239;
{ /* Llib/thread.scm 557 */
 obj_t BgL_oclassz00_8240;
{ /* Llib/thread.scm 557 */
 obj_t BgL_arg1937z00_8241; long BgL_arg1938z00_8242;
BgL_arg1937z00_8241 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 557 */
 long BgL_arg1939z00_8243;
BgL_arg1939z00_8243 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1930z00_8233); 
BgL_arg1938z00_8242 = 
(BgL_arg1939z00_8243-OBJECT_TYPE); } 
BgL_oclassz00_8240 = 
VECTOR_REF(BgL_arg1937z00_8241,BgL_arg1938z00_8242); } 
{ /* Llib/thread.scm 557 */
 bool_t BgL__ortest_1147z00_8244;
BgL__ortest_1147z00_8244 = 
(BgL_classz00_8232==BgL_oclassz00_8240); 
if(BgL__ortest_1147z00_8244)
{ /* Llib/thread.scm 557 */
BgL_res2042z00_8239 = BgL__ortest_1147z00_8244; }  else 
{ /* Llib/thread.scm 557 */
 long BgL_odepthz00_8245;
{ /* Llib/thread.scm 557 */
 obj_t BgL_arg1927z00_8246;
BgL_arg1927z00_8246 = 
(BgL_oclassz00_8240); 
BgL_odepthz00_8245 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8246); } 
if(
(2L<BgL_odepthz00_8245))
{ /* Llib/thread.scm 557 */
 obj_t BgL_arg1925z00_8247;
{ /* Llib/thread.scm 557 */
 obj_t BgL_arg1926z00_8248;
BgL_arg1926z00_8248 = 
(BgL_oclassz00_8240); 
BgL_arg1925z00_8247 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8248, 2L); } 
BgL_res2042z00_8239 = 
(BgL_arg1925z00_8247==BgL_classz00_8232); }  else 
{ /* Llib/thread.scm 557 */
BgL_res2042z00_8239 = ((bool_t)0); } } } } 
BgL_test3810z00_12888 = BgL_res2042z00_8239; } }  else 
{ /* Llib/thread.scm 557 */
BgL_test3810z00_12888 = ((bool_t)0)
; } } } 
if(BgL_test3810z00_12888)
{ /* Llib/thread.scm 557 */
return 
BGl_raisez00zz__errorz00(
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8230))->BgL_endzd2exceptionzd2));}  else 
{ /* Llib/thread.scm 557 */
return 
(((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_8230))->BgL_endzd2resultzd2);} } } } 

}



/* &thread-start-joinabl1236 */
obj_t BGl_z62threadzd2startzd2joinabl1236z62zz__threadz00(obj_t BgL_envz00_3799, obj_t BgL_thz00_3800)
{
{ /* Llib/thread.scm 549 */
{ /* Llib/thread.scm 550 */
 BgL_nothreadz00_bglt BgL_thz00_8289;
{ /* Llib/thread.scm 550 */
 bool_t BgL_test3817z00_12932;
{ /* Llib/thread.scm 550 */
 obj_t BgL_classz00_8249;
BgL_classz00_8249 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3800))
{ /* Llib/thread.scm 550 */
 BgL_objectz00_bglt BgL_arg1932z00_8251; long BgL_arg1933z00_8252;
BgL_arg1932z00_8251 = 
(BgL_objectz00_bglt)(BgL_thz00_3800); 
BgL_arg1933z00_8252 = 
BGL_CLASS_DEPTH(BgL_classz00_8249); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 550 */
 long BgL_idxz00_8260;
BgL_idxz00_8260 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8251); 
{ /* Llib/thread.scm 550 */
 obj_t BgL_arg1923z00_8261;
{ /* Llib/thread.scm 550 */
 long BgL_arg1924z00_8262;
BgL_arg1924z00_8262 = 
(BgL_idxz00_8260+BgL_arg1933z00_8252); 
BgL_arg1923z00_8261 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8262); } 
BgL_test3817z00_12932 = 
(BgL_arg1923z00_8261==BgL_classz00_8249); } }  else 
{ /* Llib/thread.scm 550 */
 bool_t BgL_res2044z00_8267;
{ /* Llib/thread.scm 550 */
 obj_t BgL_oclassz00_8271;
{ /* Llib/thread.scm 550 */
 obj_t BgL_arg1937z00_8273; long BgL_arg1938z00_8274;
BgL_arg1937z00_8273 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 550 */
 long BgL_arg1939z00_8275;
BgL_arg1939z00_8275 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8251); 
BgL_arg1938z00_8274 = 
(BgL_arg1939z00_8275-OBJECT_TYPE); } 
BgL_oclassz00_8271 = 
VECTOR_REF(BgL_arg1937z00_8273,BgL_arg1938z00_8274); } 
{ /* Llib/thread.scm 550 */
 bool_t BgL__ortest_1147z00_8281;
BgL__ortest_1147z00_8281 = 
(BgL_classz00_8249==BgL_oclassz00_8271); 
if(BgL__ortest_1147z00_8281)
{ /* Llib/thread.scm 550 */
BgL_res2044z00_8267 = BgL__ortest_1147z00_8281; }  else 
{ /* Llib/thread.scm 550 */
 long BgL_odepthz00_8282;
{ /* Llib/thread.scm 550 */
 obj_t BgL_arg1927z00_8283;
BgL_arg1927z00_8283 = 
(BgL_oclassz00_8271); 
BgL_odepthz00_8282 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8283); } 
if(
(BgL_arg1933z00_8252<BgL_odepthz00_8282))
{ /* Llib/thread.scm 550 */
 obj_t BgL_arg1925z00_8287;
{ /* Llib/thread.scm 550 */
 obj_t BgL_arg1926z00_8288;
BgL_arg1926z00_8288 = 
(BgL_oclassz00_8271); 
BgL_arg1925z00_8287 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8288, BgL_arg1933z00_8252); } 
BgL_res2044z00_8267 = 
(BgL_arg1925z00_8287==BgL_classz00_8249); }  else 
{ /* Llib/thread.scm 550 */
BgL_res2044z00_8267 = ((bool_t)0); } } } } 
BgL_test3817z00_12932 = BgL_res2044z00_8267; } }  else 
{ /* Llib/thread.scm 550 */
BgL_test3817z00_12932 = ((bool_t)0)
; } } 
if(BgL_test3817z00_12932)
{ /* Llib/thread.scm 550 */
BgL_thz00_8289 = 
((BgL_nothreadz00_bglt)BgL_thz00_3800); }  else 
{ 
 obj_t BgL_auxz00_12957;
BgL_auxz00_12957 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23596L), BGl_string2852z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3800); 
FAILURE(BgL_auxz00_12957,BFALSE,BFALSE);} } 
return 
BGl_threadzd2startz12zc0zz__threadz00(
((BgL_threadz00_bglt)BgL_thz00_8289), BNIL);} } 

}



/* &thread-start!-nothre1234 */
obj_t BGl_z62threadzd2startz12zd2nothre1234z70zz__threadz00(obj_t BgL_envz00_3801, obj_t BgL_thz00_3802, obj_t BgL_scdz00_3803)
{
{ /* Llib/thread.scm 533 */
{ /* Llib/thread.scm 534 */
 BgL_nothreadz00_bglt BgL_auxz00_12963;
{ /* Llib/thread.scm 534 */
 bool_t BgL_test3822z00_12964;
{ /* Llib/thread.scm 534 */
 obj_t BgL_classz00_8290;
BgL_classz00_8290 = BGl_nothreadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_thz00_3802))
{ /* Llib/thread.scm 534 */
 BgL_objectz00_bglt BgL_arg1932z00_8292; long BgL_arg1933z00_8293;
BgL_arg1932z00_8292 = 
(BgL_objectz00_bglt)(BgL_thz00_3802); 
BgL_arg1933z00_8293 = 
BGL_CLASS_DEPTH(BgL_classz00_8290); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 534 */
 long BgL_idxz00_8301;
BgL_idxz00_8301 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8292); 
{ /* Llib/thread.scm 534 */
 obj_t BgL_arg1923z00_8302;
{ /* Llib/thread.scm 534 */
 long BgL_arg1924z00_8303;
BgL_arg1924z00_8303 = 
(BgL_idxz00_8301+BgL_arg1933z00_8293); 
BgL_arg1923z00_8302 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8303); } 
BgL_test3822z00_12964 = 
(BgL_arg1923z00_8302==BgL_classz00_8290); } }  else 
{ /* Llib/thread.scm 534 */
 bool_t BgL_res2044z00_8308;
{ /* Llib/thread.scm 534 */
 obj_t BgL_oclassz00_8312;
{ /* Llib/thread.scm 534 */
 obj_t BgL_arg1937z00_8314; long BgL_arg1938z00_8315;
BgL_arg1937z00_8314 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 534 */
 long BgL_arg1939z00_8316;
BgL_arg1939z00_8316 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8292); 
BgL_arg1938z00_8315 = 
(BgL_arg1939z00_8316-OBJECT_TYPE); } 
BgL_oclassz00_8312 = 
VECTOR_REF(BgL_arg1937z00_8314,BgL_arg1938z00_8315); } 
{ /* Llib/thread.scm 534 */
 bool_t BgL__ortest_1147z00_8322;
BgL__ortest_1147z00_8322 = 
(BgL_classz00_8290==BgL_oclassz00_8312); 
if(BgL__ortest_1147z00_8322)
{ /* Llib/thread.scm 534 */
BgL_res2044z00_8308 = BgL__ortest_1147z00_8322; }  else 
{ /* Llib/thread.scm 534 */
 long BgL_odepthz00_8323;
{ /* Llib/thread.scm 534 */
 obj_t BgL_arg1927z00_8324;
BgL_arg1927z00_8324 = 
(BgL_oclassz00_8312); 
BgL_odepthz00_8323 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8324); } 
if(
(BgL_arg1933z00_8293<BgL_odepthz00_8323))
{ /* Llib/thread.scm 534 */
 obj_t BgL_arg1925z00_8328;
{ /* Llib/thread.scm 534 */
 obj_t BgL_arg1926z00_8329;
BgL_arg1926z00_8329 = 
(BgL_oclassz00_8312); 
BgL_arg1925z00_8328 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8329, BgL_arg1933z00_8293); } 
BgL_res2044z00_8308 = 
(BgL_arg1925z00_8328==BgL_classz00_8290); }  else 
{ /* Llib/thread.scm 534 */
BgL_res2044z00_8308 = ((bool_t)0); } } } } 
BgL_test3822z00_12964 = BgL_res2044z00_8308; } }  else 
{ /* Llib/thread.scm 534 */
BgL_test3822z00_12964 = ((bool_t)0)
; } } 
if(BgL_test3822z00_12964)
{ /* Llib/thread.scm 534 */
BgL_auxz00_12963 = 
((BgL_nothreadz00_bglt)BgL_thz00_3802)
; }  else 
{ 
 obj_t BgL_auxz00_12989;
BgL_auxz00_12989 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23004L), BGl_string2853z00zz__threadz00, BGl_string2411z00zz__threadz00, BgL_thz00_3802); 
FAILURE(BgL_auxz00_12989,BFALSE,BFALSE);} } 
return 
BGl_threadzd2startz12zd2nothre1234ze70zf5zz__threadz00(BgL_auxz00_12963, BgL_scdz00_3803);} } 

}



/* <@exit:1557>~0 */
obj_t BGl_zc3z04exitza31557ze3ze70z60zz__threadz00(BgL_nothreadz00_bglt BgL_i1071z00_3838, obj_t BgL_cell1075z00_3837, obj_t BgL_env1080z00_3836)
{
{ /* Llib/thread.scm 538 */
jmp_buf_t jmpbuf; 
 void * BgL_an_exit1084z00_1748;
if( SET_EXIT(BgL_an_exit1084z00_1748 ) ) { 
return 
BGL_EXIT_VALUE();
} else {
#if( SIGSETJMP_SAVESIGS == 0 )
  // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
  // bgl_restore_signal_handlers();
#endif

BgL_an_exit1084z00_1748 = 
(void *)jmpbuf; 
PUSH_ENV_EXIT(BgL_env1080z00_3836, BgL_an_exit1084z00_1748, 1L); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_escape1076z00_1749;
BgL_escape1076z00_1749 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1080z00_3836); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_res1087z00_1750;
{ /* Llib/thread.scm 538 */
 obj_t BgL_ohs1072z00_1751;
BgL_ohs1072z00_1751 = 
BGL_ENV_ERROR_HANDLER_GET(BgL_env1080z00_3836); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_hds1073z00_1752;
BgL_hds1073z00_1752 = 
MAKE_STACK_PAIR(BgL_escape1076z00_1749, BgL_cell1075z00_3837); 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1080z00_3836, BgL_hds1073z00_1752); BUNSPEC; 
{ /* Llib/thread.scm 538 */
 obj_t BgL_exitd1081z00_1753;
BgL_exitd1081z00_1753 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1080z00_3836); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_tmp1083z00_1754;
{ /* Llib/thread.scm 538 */
 obj_t BgL_arg1559z00_1757;
BgL_arg1559z00_1757 = 
BGL_EXITD_PROTECT(BgL_exitd1081z00_1753); 
BgL_tmp1083z00_1754 = 
MAKE_YOUNG_PAIR(BgL_ohs1072z00_1751, BgL_arg1559z00_1757); } 
{ /* Llib/thread.scm 538 */

BGL_EXITD_PROTECT_SET(BgL_exitd1081z00_1753, BgL_tmp1083z00_1754); BUNSPEC; 
{ /* Llib/thread.scm 538 */
 obj_t BgL_tmp1082z00_1755;
{ 
 obj_t BgL_auxz00_13005;
{ /* Llib/thread.scm 542 */
 obj_t BgL_fun1558z00_1756;
BgL_fun1558z00_1756 = 
(((BgL_nothreadz00_bglt)COBJECT(BgL_i1071z00_3838))->BgL_bodyz00); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_fun1558z00_1756, 0))
{ /* Llib/thread.scm 542 */
BgL_auxz00_13005 = 
BGL_PROCEDURE_CALL0(BgL_fun1558z00_1756)
; }  else 
{ /* Llib/thread.scm 542 */
FAILURE(BGl_string2854z00zz__threadz00,BGl_list2855z00zz__threadz00,BgL_fun1558z00_1756);} } 
BgL_tmp1082z00_1755 = 
((((BgL_nothreadz00_bglt)COBJECT(BgL_i1071z00_3838))->BgL_endzd2resultzd2)=((obj_t)BgL_auxz00_13005),BUNSPEC); } 
{ /* Llib/thread.scm 538 */
 bool_t BgL_test3828z00_13014;
{ /* Llib/thread.scm 538 */
 obj_t BgL_arg1918z00_3102;
BgL_arg1918z00_3102 = 
BGL_EXITD_PROTECT(BgL_exitd1081z00_1753); 
BgL_test3828z00_13014 = 
PAIRP(BgL_arg1918z00_3102); } 
if(BgL_test3828z00_13014)
{ /* Llib/thread.scm 538 */
 obj_t BgL_arg1916z00_3103;
{ /* Llib/thread.scm 538 */
 obj_t BgL_arg1917z00_3104;
BgL_arg1917z00_3104 = 
BGL_EXITD_PROTECT(BgL_exitd1081z00_1753); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_pairz00_3105;
if(
PAIRP(BgL_arg1917z00_3104))
{ /* Llib/thread.scm 538 */
BgL_pairz00_3105 = BgL_arg1917z00_3104; }  else 
{ 
 obj_t BgL_auxz00_13020;
BgL_auxz00_13020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23160L), BGl_string2858z00zz__threadz00, BGl_string2416z00zz__threadz00, BgL_arg1917z00_3104); 
FAILURE(BgL_auxz00_13020,BFALSE,BFALSE);} 
BgL_arg1916z00_3103 = 
CDR(BgL_pairz00_3105); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1081z00_1753, BgL_arg1916z00_3103); BUNSPEC; }  else 
{ /* Llib/thread.scm 538 */BFALSE; } } 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1080z00_3836, BgL_ohs1072z00_1751); BUNSPEC; 
BgL_res1087z00_1750 = BgL_tmp1082z00_1755; } } } } } } 
POP_ENV_EXIT(BgL_env1080z00_3836); 
return BgL_res1087z00_1750;} } 
}} 

}



/* thread-start!-nothre1234~0 */
obj_t BGl_threadzd2startz12zd2nothre1234ze70zf5zz__threadz00(BgL_nothreadz00_bglt BgL_thz00_1733, obj_t BgL_scdz00_1734)
{
{ /* Llib/thread.scm 533 */
{ /* Llib/thread.scm 534 */
 obj_t BgL_threadz00_1737;
BgL_threadz00_1737 = BGl_za2nothreadzd2currentza2zd2zz__threadz00; 
{ /* Llib/thread.scm 535 */
 obj_t BgL_exitd1067z00_1738;
BgL_exitd1067z00_1738 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/thread.scm 544 */
 obj_t BgL_zc3z04anonymousza31561ze3z87_3767;
BgL_zc3z04anonymousza31561ze3z87_3767 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31561ze3ze5zz__threadz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_3767, 
(int)(0L), BgL_threadz00_1737); 
{ /* Llib/thread.scm 535 */
 obj_t BgL_arg1919z00_3099;
{ /* Llib/thread.scm 535 */
 obj_t BgL_arg1920z00_3100;
BgL_arg1920z00_3100 = 
BGL_EXITD_PROTECT(BgL_exitd1067z00_1738); 
BgL_arg1919z00_3099 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31561ze3z87_3767, BgL_arg1920z00_3100); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1738, BgL_arg1919z00_3099); BUNSPEC; } 
{ /* Llib/thread.scm 536 */
 BgL_nothreadz00_bglt BgL_tmp1069z00_1740;
BGl_za2nothreadzd2currentza2zd2zz__threadz00 = 
((obj_t)BgL_thz00_1733); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_env1080z00_1743;
BgL_env1080z00_1743 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_cell1075z00_1744;
BgL_cell1075z00_1744 = 
MAKE_STACK_CELL(BUNSPEC); 
{ /* Llib/thread.scm 538 */
 obj_t BgL_val1079z00_1745;
BgL_val1079z00_1745 = 
BGl_zc3z04exitza31557ze3ze70z60zz__threadz00(BgL_thz00_1733, BgL_cell1075z00_1744, BgL_env1080z00_1743); 
if(
(BgL_val1079z00_1745==BgL_cell1075z00_1744))
{ /* Llib/thread.scm 538 */
{ /* Llib/thread.scm 538 */
 int BgL_tmpz00_13043;
BgL_tmpz00_13043 = 
(int)(0L); 
BGL_SIGSETMASK(BgL_tmpz00_13043); } 
{ /* Llib/thread.scm 538 */
 obj_t BgL_arg1556z00_1746;
BgL_arg1556z00_1746 = 
CELL_REF(
((obj_t)BgL_val1079z00_1745)); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_thz00_1733))->BgL_endzd2exceptionzd2)=((obj_t)BgL_arg1556z00_1746),BUNSPEC); 
BGl_raisez00zz__errorz00(BgL_arg1556z00_1746); } }  else 
{ /* Llib/thread.scm 538 */BgL_val1079z00_1745; } } } } 
BgL_tmp1069z00_1740 = BgL_thz00_1733; 
{ /* Llib/thread.scm 535 */
 bool_t BgL_test3831z00_13050;
{ /* Llib/thread.scm 535 */
 obj_t BgL_arg1918z00_3108;
BgL_arg1918z00_3108 = 
BGL_EXITD_PROTECT(BgL_exitd1067z00_1738); 
BgL_test3831z00_13050 = 
PAIRP(BgL_arg1918z00_3108); } 
if(BgL_test3831z00_13050)
{ /* Llib/thread.scm 535 */
 obj_t BgL_arg1916z00_3109;
{ /* Llib/thread.scm 535 */
 obj_t BgL_arg1917z00_3110;
BgL_arg1917z00_3110 = 
BGL_EXITD_PROTECT(BgL_exitd1067z00_1738); 
{ /* Llib/thread.scm 535 */
 obj_t BgL_pairz00_3111;
if(
PAIRP(BgL_arg1917z00_3110))
{ /* Llib/thread.scm 535 */
BgL_pairz00_3111 = BgL_arg1917z00_3110; }  else 
{ 
 obj_t BgL_auxz00_13056;
BgL_auxz00_13056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(23045L), BGl_string2859z00zz__threadz00, BGl_string2416z00zz__threadz00, BgL_arg1917z00_3110); 
FAILURE(BgL_auxz00_13056,BFALSE,BFALSE);} 
BgL_arg1916z00_3109 = 
CDR(BgL_pairz00_3111); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_1738, BgL_arg1916z00_3109); BUNSPEC; }  else 
{ /* Llib/thread.scm 535 */BFALSE; } } 
BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BgL_threadz00_1737; 
return 
((obj_t)BgL_tmp1069z00_1740);} } } } } 

}



/* &<@anonymous:1561> */
obj_t BGl_z62zc3z04anonymousza31561ze3ze5zz__threadz00(obj_t BgL_envz00_3804)
{
{ /* Llib/thread.scm 535 */
{ /* Llib/thread.scm 544 */
 obj_t BgL_threadz00_3805;
BgL_threadz00_3805 = 
PROCEDURE_REF(BgL_envz00_3804, 
(int)(0L)); 
return ( 
BGl_za2nothreadzd2currentza2zd2zz__threadz00 = BgL_threadz00_3805, BUNSPEC) ;} } 

}



/* &object-print-thread1194 */
obj_t BGl_z62objectzd2printzd2thread1194z62zz__threadz00(obj_t BgL_envz00_3806, obj_t BgL_oz00_3807, obj_t BgL_portz00_3808, obj_t BgL_printzd2slotzd2_3809)
{
{ /* Llib/thread.scm 378 */
{ /* Llib/thread.scm 379 */
 BgL_threadz00_bglt BgL_oz00_8370;
{ /* Llib/thread.scm 379 */
 bool_t BgL_test3833z00_13065;
{ /* Llib/thread.scm 379 */
 obj_t BgL_classz00_8330;
BgL_classz00_8330 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3807))
{ /* Llib/thread.scm 379 */
 BgL_objectz00_bglt BgL_arg1932z00_8332; long BgL_arg1933z00_8333;
BgL_arg1932z00_8332 = 
(BgL_objectz00_bglt)(BgL_oz00_3807); 
BgL_arg1933z00_8333 = 
BGL_CLASS_DEPTH(BgL_classz00_8330); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 379 */
 long BgL_idxz00_8341;
BgL_idxz00_8341 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8332); 
{ /* Llib/thread.scm 379 */
 obj_t BgL_arg1923z00_8342;
{ /* Llib/thread.scm 379 */
 long BgL_arg1924z00_8343;
BgL_arg1924z00_8343 = 
(BgL_idxz00_8341+BgL_arg1933z00_8333); 
BgL_arg1923z00_8342 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8343); } 
BgL_test3833z00_13065 = 
(BgL_arg1923z00_8342==BgL_classz00_8330); } }  else 
{ /* Llib/thread.scm 379 */
 bool_t BgL_res2044z00_8348;
{ /* Llib/thread.scm 379 */
 obj_t BgL_oclassz00_8352;
{ /* Llib/thread.scm 379 */
 obj_t BgL_arg1937z00_8354; long BgL_arg1938z00_8355;
BgL_arg1937z00_8354 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 379 */
 long BgL_arg1939z00_8356;
BgL_arg1939z00_8356 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8332); 
BgL_arg1938z00_8355 = 
(BgL_arg1939z00_8356-OBJECT_TYPE); } 
BgL_oclassz00_8352 = 
VECTOR_REF(BgL_arg1937z00_8354,BgL_arg1938z00_8355); } 
{ /* Llib/thread.scm 379 */
 bool_t BgL__ortest_1147z00_8362;
BgL__ortest_1147z00_8362 = 
(BgL_classz00_8330==BgL_oclassz00_8352); 
if(BgL__ortest_1147z00_8362)
{ /* Llib/thread.scm 379 */
BgL_res2044z00_8348 = BgL__ortest_1147z00_8362; }  else 
{ /* Llib/thread.scm 379 */
 long BgL_odepthz00_8363;
{ /* Llib/thread.scm 379 */
 obj_t BgL_arg1927z00_8364;
BgL_arg1927z00_8364 = 
(BgL_oclassz00_8352); 
BgL_odepthz00_8363 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8364); } 
if(
(BgL_arg1933z00_8333<BgL_odepthz00_8363))
{ /* Llib/thread.scm 379 */
 obj_t BgL_arg1925z00_8368;
{ /* Llib/thread.scm 379 */
 obj_t BgL_arg1926z00_8369;
BgL_arg1926z00_8369 = 
(BgL_oclassz00_8352); 
BgL_arg1925z00_8368 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8369, BgL_arg1933z00_8333); } 
BgL_res2044z00_8348 = 
(BgL_arg1925z00_8368==BgL_classz00_8330); }  else 
{ /* Llib/thread.scm 379 */
BgL_res2044z00_8348 = ((bool_t)0); } } } } 
BgL_test3833z00_13065 = BgL_res2044z00_8348; } }  else 
{ /* Llib/thread.scm 379 */
BgL_test3833z00_13065 = ((bool_t)0)
; } } 
if(BgL_test3833z00_13065)
{ /* Llib/thread.scm 379 */
BgL_oz00_8370 = 
((BgL_threadz00_bglt)BgL_oz00_3807); }  else 
{ 
 obj_t BgL_auxz00_13090;
BgL_auxz00_13090 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(15622L), BGl_string2860z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3807); 
FAILURE(BgL_auxz00_13090,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 379 */
 obj_t BgL_list1555z00_8371;
BgL_list1555z00_8371 = 
MAKE_YOUNG_PAIR(BgL_portz00_3808, BNIL); 
return 
BGl_objectzd2writezd2zz__objectz00(
((BgL_objectz00_bglt)BgL_oz00_8370), BgL_list1555z00_8371);} } } 

}



/* &object-write-thread1192 */
obj_t BGl_z62objectzd2writezd2thread1192z62zz__threadz00(obj_t BgL_envz00_3810, obj_t BgL_oz00_3811, obj_t BgL_portz00_3812)
{
{ /* Llib/thread.scm 369 */
{ /* Llib/thread.scm 370 */
 BgL_threadz00_bglt BgL_oz00_8412;
{ /* Llib/thread.scm 370 */
 bool_t BgL_test3838z00_13097;
{ /* Llib/thread.scm 370 */
 obj_t BgL_classz00_8372;
BgL_classz00_8372 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3811))
{ /* Llib/thread.scm 370 */
 BgL_objectz00_bglt BgL_arg1932z00_8374; long BgL_arg1933z00_8375;
BgL_arg1932z00_8374 = 
(BgL_objectz00_bglt)(BgL_oz00_3811); 
BgL_arg1933z00_8375 = 
BGL_CLASS_DEPTH(BgL_classz00_8372); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 370 */
 long BgL_idxz00_8383;
BgL_idxz00_8383 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8374); 
{ /* Llib/thread.scm 370 */
 obj_t BgL_arg1923z00_8384;
{ /* Llib/thread.scm 370 */
 long BgL_arg1924z00_8385;
BgL_arg1924z00_8385 = 
(BgL_idxz00_8383+BgL_arg1933z00_8375); 
BgL_arg1923z00_8384 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8385); } 
BgL_test3838z00_13097 = 
(BgL_arg1923z00_8384==BgL_classz00_8372); } }  else 
{ /* Llib/thread.scm 370 */
 bool_t BgL_res2044z00_8390;
{ /* Llib/thread.scm 370 */
 obj_t BgL_oclassz00_8394;
{ /* Llib/thread.scm 370 */
 obj_t BgL_arg1937z00_8396; long BgL_arg1938z00_8397;
BgL_arg1937z00_8396 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 370 */
 long BgL_arg1939z00_8398;
BgL_arg1939z00_8398 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8374); 
BgL_arg1938z00_8397 = 
(BgL_arg1939z00_8398-OBJECT_TYPE); } 
BgL_oclassz00_8394 = 
VECTOR_REF(BgL_arg1937z00_8396,BgL_arg1938z00_8397); } 
{ /* Llib/thread.scm 370 */
 bool_t BgL__ortest_1147z00_8404;
BgL__ortest_1147z00_8404 = 
(BgL_classz00_8372==BgL_oclassz00_8394); 
if(BgL__ortest_1147z00_8404)
{ /* Llib/thread.scm 370 */
BgL_res2044z00_8390 = BgL__ortest_1147z00_8404; }  else 
{ /* Llib/thread.scm 370 */
 long BgL_odepthz00_8405;
{ /* Llib/thread.scm 370 */
 obj_t BgL_arg1927z00_8406;
BgL_arg1927z00_8406 = 
(BgL_oclassz00_8394); 
BgL_odepthz00_8405 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8406); } 
if(
(BgL_arg1933z00_8375<BgL_odepthz00_8405))
{ /* Llib/thread.scm 370 */
 obj_t BgL_arg1925z00_8410;
{ /* Llib/thread.scm 370 */
 obj_t BgL_arg1926z00_8411;
BgL_arg1926z00_8411 = 
(BgL_oclassz00_8394); 
BgL_arg1925z00_8410 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8411, BgL_arg1933z00_8375); } 
BgL_res2044z00_8390 = 
(BgL_arg1925z00_8410==BgL_classz00_8372); }  else 
{ /* Llib/thread.scm 370 */
BgL_res2044z00_8390 = ((bool_t)0); } } } } 
BgL_test3838z00_13097 = BgL_res2044z00_8390; } }  else 
{ /* Llib/thread.scm 370 */
BgL_test3838z00_13097 = ((bool_t)0)
; } } 
if(BgL_test3838z00_13097)
{ /* Llib/thread.scm 370 */
BgL_oz00_8412 = 
((BgL_threadz00_bglt)BgL_oz00_3811); }  else 
{ 
 obj_t BgL_auxz00_13122;
BgL_auxz00_13122 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(15158L), BGl_string2863z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3811); 
FAILURE(BgL_auxz00_13122,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 370 */
 obj_t BgL_arg1539z00_8413;
if(
PAIRP(BgL_portz00_3812))
{ /* Llib/thread.scm 370 */
BgL_arg1539z00_8413 = 
CAR(BgL_portz00_3812); }  else 
{ /* Llib/thread.scm 370 */
 obj_t BgL_tmpz00_13129;
BgL_tmpz00_13129 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1539z00_8413 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_13129); } 
{ /* Llib/thread.scm 372 */
 obj_t BgL_zc3z04anonymousza31542ze3z87_8414;
BgL_zc3z04anonymousza31542ze3z87_8414 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31542ze3ze5zz__threadz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31542ze3z87_8414, 
(int)(0L), 
((obj_t)BgL_oz00_8412)); 
{ /* Llib/thread.scm 370 */
 obj_t BgL_auxz00_13138;
if(
OUTPUT_PORTP(BgL_arg1539z00_8413))
{ /* Llib/thread.scm 370 */
BgL_auxz00_13138 = BgL_arg1539z00_8413
; }  else 
{ 
 obj_t BgL_auxz00_13141;
BgL_auxz00_13141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(15207L), BGl_string2861z00zz__threadz00, BGl_string2862z00zz__threadz00, BgL_arg1539z00_8413); 
FAILURE(BgL_auxz00_13141,BFALSE,BFALSE);} 
return 
BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_auxz00_13138, BgL_zc3z04anonymousza31542ze3z87_8414);} } } } } 

}



/* &<@anonymous:1542> */
obj_t BGl_z62zc3z04anonymousza31542ze3ze5zz__threadz00(obj_t BgL_envz00_3813)
{
{ /* Llib/thread.scm 371 */
{ /* Llib/thread.scm 372 */
 BgL_threadz00_bglt BgL_oz00_3814;
BgL_oz00_3814 = 
((BgL_threadz00_bglt)
PROCEDURE_REF(BgL_envz00_3813, 
(int)(0L))); 
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1543z00_8415; obj_t BgL_arg1544z00_8416;
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1553z00_8417;
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1937z00_8418; long BgL_arg1938z00_8419;
BgL_arg1937z00_8418 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 373 */
 long BgL_arg1939z00_8420;
BgL_arg1939z00_8420 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_oz00_3814)); 
BgL_arg1938z00_8419 = 
(BgL_arg1939z00_8420-OBJECT_TYPE); } 
BgL_arg1553z00_8417 = 
VECTOR_REF(BgL_arg1937z00_8418,BgL_arg1938z00_8419); } 
{ /* Llib/thread.scm 373 */
 obj_t BgL_auxz00_13154;
if(
BGl_classzf3zf3zz__objectz00(BgL_arg1553z00_8417))
{ /* Llib/thread.scm 373 */
BgL_auxz00_13154 = BgL_arg1553z00_8417
; }  else 
{ 
 obj_t BgL_auxz00_13157;
BgL_auxz00_13157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(15319L), BGl_string2864z00zz__threadz00, BGl_string2865z00zz__threadz00, BgL_arg1553z00_8417); 
FAILURE(BgL_auxz00_13157,BFALSE,BFALSE);} 
BgL_arg1543z00_8415 = 
BGl_classzd2namezd2zz__objectz00(BgL_auxz00_13154); } } 
BgL_arg1544z00_8416 = 
(((BgL_threadz00_bglt)COBJECT(BgL_oz00_3814))->BgL_namez00); 
{ /* Llib/thread.scm 373 */
 obj_t BgL_list1545z00_8421;
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1546z00_8422;
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1547z00_8423;
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1549z00_8424;
{ /* Llib/thread.scm 373 */
 obj_t BgL_arg1552z00_8425;
BgL_arg1552z00_8425 = 
MAKE_YOUNG_PAIR(BGl_string2866z00zz__threadz00, BNIL); 
BgL_arg1549z00_8424 = 
MAKE_YOUNG_PAIR(BgL_arg1544z00_8416, BgL_arg1552z00_8425); } 
BgL_arg1547z00_8423 = 
MAKE_YOUNG_PAIR(BGl_string2867z00zz__threadz00, BgL_arg1549z00_8424); } 
BgL_arg1546z00_8422 = 
MAKE_YOUNG_PAIR(BgL_arg1543z00_8415, BgL_arg1547z00_8423); } 
BgL_list1545z00_8421 = 
MAKE_YOUNG_PAIR(BGl_string2868z00zz__threadz00, BgL_arg1546z00_8422); } 
return 
BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1545z00_8421);} } } } 

}



/* &object-display-threa1190 */
obj_t BGl_z62objectzd2displayzd2threa1190z62zz__threadz00(obj_t BgL_envz00_3815, obj_t BgL_oz00_3816, obj_t BgL_portz00_3817)
{
{ /* Llib/thread.scm 360 */
{ /* Llib/thread.scm 361 */
 BgL_threadz00_bglt BgL_oz00_8466;
{ /* Llib/thread.scm 361 */
 bool_t BgL_test3846z00_13169;
{ /* Llib/thread.scm 361 */
 obj_t BgL_classz00_8426;
BgL_classz00_8426 = BGl_threadz00zz__threadz00; 
if(
BGL_OBJECTP(BgL_oz00_3816))
{ /* Llib/thread.scm 361 */
 BgL_objectz00_bglt BgL_arg1932z00_8428; long BgL_arg1933z00_8429;
BgL_arg1932z00_8428 = 
(BgL_objectz00_bglt)(BgL_oz00_3816); 
BgL_arg1933z00_8429 = 
BGL_CLASS_DEPTH(BgL_classz00_8426); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 361 */
 long BgL_idxz00_8437;
BgL_idxz00_8437 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8428); 
{ /* Llib/thread.scm 361 */
 obj_t BgL_arg1923z00_8438;
{ /* Llib/thread.scm 361 */
 long BgL_arg1924z00_8439;
BgL_arg1924z00_8439 = 
(BgL_idxz00_8437+BgL_arg1933z00_8429); 
BgL_arg1923z00_8438 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8439); } 
BgL_test3846z00_13169 = 
(BgL_arg1923z00_8438==BgL_classz00_8426); } }  else 
{ /* Llib/thread.scm 361 */
 bool_t BgL_res2044z00_8444;
{ /* Llib/thread.scm 361 */
 obj_t BgL_oclassz00_8448;
{ /* Llib/thread.scm 361 */
 obj_t BgL_arg1937z00_8450; long BgL_arg1938z00_8451;
BgL_arg1937z00_8450 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 361 */
 long BgL_arg1939z00_8452;
BgL_arg1939z00_8452 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8428); 
BgL_arg1938z00_8451 = 
(BgL_arg1939z00_8452-OBJECT_TYPE); } 
BgL_oclassz00_8448 = 
VECTOR_REF(BgL_arg1937z00_8450,BgL_arg1938z00_8451); } 
{ /* Llib/thread.scm 361 */
 bool_t BgL__ortest_1147z00_8458;
BgL__ortest_1147z00_8458 = 
(BgL_classz00_8426==BgL_oclassz00_8448); 
if(BgL__ortest_1147z00_8458)
{ /* Llib/thread.scm 361 */
BgL_res2044z00_8444 = BgL__ortest_1147z00_8458; }  else 
{ /* Llib/thread.scm 361 */
 long BgL_odepthz00_8459;
{ /* Llib/thread.scm 361 */
 obj_t BgL_arg1927z00_8460;
BgL_arg1927z00_8460 = 
(BgL_oclassz00_8448); 
BgL_odepthz00_8459 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8460); } 
if(
(BgL_arg1933z00_8429<BgL_odepthz00_8459))
{ /* Llib/thread.scm 361 */
 obj_t BgL_arg1925z00_8464;
{ /* Llib/thread.scm 361 */
 obj_t BgL_arg1926z00_8465;
BgL_arg1926z00_8465 = 
(BgL_oclassz00_8448); 
BgL_arg1925z00_8464 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8465, BgL_arg1933z00_8429); } 
BgL_res2044z00_8444 = 
(BgL_arg1925z00_8464==BgL_classz00_8426); }  else 
{ /* Llib/thread.scm 361 */
BgL_res2044z00_8444 = ((bool_t)0); } } } } 
BgL_test3846z00_13169 = BgL_res2044z00_8444; } }  else 
{ /* Llib/thread.scm 361 */
BgL_test3846z00_13169 = ((bool_t)0)
; } } 
if(BgL_test3846z00_13169)
{ /* Llib/thread.scm 361 */
BgL_oz00_8466 = 
((BgL_threadz00_bglt)BgL_oz00_3816); }  else 
{ 
 obj_t BgL_auxz00_13194;
BgL_auxz00_13194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14682L), BGl_string2870z00zz__threadz00, BGl_string2425z00zz__threadz00, BgL_oz00_3816); 
FAILURE(BgL_auxz00_13194,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 361 */
 obj_t BgL_arg1522z00_8467;
if(
PAIRP(BgL_portz00_3817))
{ /* Llib/thread.scm 361 */
BgL_arg1522z00_8467 = 
CAR(BgL_portz00_3817); }  else 
{ /* Llib/thread.scm 361 */
 obj_t BgL_tmpz00_13201;
BgL_tmpz00_13201 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1522z00_8467 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_13201); } 
{ /* Llib/thread.scm 363 */
 obj_t BgL_zc3z04anonymousza31525ze3z87_8468;
BgL_zc3z04anonymousza31525ze3z87_8468 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31525ze3ze5zz__threadz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31525ze3z87_8468, 
(int)(0L), 
((obj_t)BgL_oz00_8466)); 
{ /* Llib/thread.scm 361 */
 obj_t BgL_auxz00_13210;
if(
OUTPUT_PORTP(BgL_arg1522z00_8467))
{ /* Llib/thread.scm 361 */
BgL_auxz00_13210 = BgL_arg1522z00_8467
; }  else 
{ 
 obj_t BgL_auxz00_13213;
BgL_auxz00_13213 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14731L), BGl_string2869z00zz__threadz00, BGl_string2862z00zz__threadz00, BgL_arg1522z00_8467); 
FAILURE(BgL_auxz00_13213,BFALSE,BFALSE);} 
return 
BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_auxz00_13210, BgL_zc3z04anonymousza31525ze3z87_8468);} } } } } 

}



/* &<@anonymous:1525> */
obj_t BGl_z62zc3z04anonymousza31525ze3ze5zz__threadz00(obj_t BgL_envz00_3818)
{
{ /* Llib/thread.scm 362 */
{ /* Llib/thread.scm 363 */
 BgL_threadz00_bglt BgL_oz00_3819;
BgL_oz00_3819 = 
((BgL_threadz00_bglt)
PROCEDURE_REF(BgL_envz00_3818, 
(int)(0L))); 
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1526z00_8469; obj_t BgL_arg1527z00_8470;
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1536z00_8471;
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1937z00_8472; long BgL_arg1938z00_8473;
BgL_arg1937z00_8472 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 364 */
 long BgL_arg1939z00_8474;
BgL_arg1939z00_8474 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_oz00_3819)); 
BgL_arg1938z00_8473 = 
(BgL_arg1939z00_8474-OBJECT_TYPE); } 
BgL_arg1536z00_8471 = 
VECTOR_REF(BgL_arg1937z00_8472,BgL_arg1938z00_8473); } 
{ /* Llib/thread.scm 364 */
 obj_t BgL_auxz00_13226;
if(
BGl_classzf3zf3zz__objectz00(BgL_arg1536z00_8471))
{ /* Llib/thread.scm 364 */
BgL_auxz00_13226 = BgL_arg1536z00_8471
; }  else 
{ 
 obj_t BgL_auxz00_13229;
BgL_auxz00_13229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(14843L), BGl_string2871z00zz__threadz00, BGl_string2865z00zz__threadz00, BgL_arg1536z00_8471); 
FAILURE(BgL_auxz00_13229,BFALSE,BFALSE);} 
BgL_arg1526z00_8469 = 
BGl_classzd2namezd2zz__objectz00(BgL_auxz00_13226); } } 
BgL_arg1527z00_8470 = 
(((BgL_threadz00_bglt)COBJECT(BgL_oz00_3819))->BgL_namez00); 
{ /* Llib/thread.scm 364 */
 obj_t BgL_list1528z00_8475;
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1529z00_8476;
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1530z00_8477;
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1531z00_8478;
{ /* Llib/thread.scm 364 */
 obj_t BgL_arg1535z00_8479;
BgL_arg1535z00_8479 = 
MAKE_YOUNG_PAIR(BGl_string2866z00zz__threadz00, BNIL); 
BgL_arg1531z00_8478 = 
MAKE_YOUNG_PAIR(BgL_arg1527z00_8470, BgL_arg1535z00_8479); } 
BgL_arg1530z00_8477 = 
MAKE_YOUNG_PAIR(BGl_string2867z00zz__threadz00, BgL_arg1531z00_8478); } 
BgL_arg1529z00_8476 = 
MAKE_YOUNG_PAIR(BgL_arg1526z00_8469, BgL_arg1530z00_8477); } 
BgL_list1528z00_8475 = 
MAKE_YOUNG_PAIR(BGl_string2868z00zz__threadz00, BgL_arg1529z00_8476); } 
return 
BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1528z00_8475);} } } } 

}



/* &tb-current-thread-no1184 */
obj_t BGl_z62tbzd2currentzd2threadzd2no1184zb0zz__threadz00(obj_t BgL_envz00_3820, obj_t BgL_tbz00_3821)
{
{ /* Llib/thread.scm 344 */
{ /* Llib/thread.scm 344 */
 BgL_nothreadzd2backendzd2_bglt BgL_tbz00_8520;
{ /* Llib/thread.scm 344 */
 bool_t BgL_test3854z00_13241;
{ /* Llib/thread.scm 344 */
 obj_t BgL_classz00_8480;
BgL_classz00_8480 = BGl_nothreadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3821))
{ /* Llib/thread.scm 344 */
 BgL_objectz00_bglt BgL_arg1932z00_8482; long BgL_arg1933z00_8483;
BgL_arg1932z00_8482 = 
(BgL_objectz00_bglt)(BgL_tbz00_3821); 
BgL_arg1933z00_8483 = 
BGL_CLASS_DEPTH(BgL_classz00_8480); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 344 */
 long BgL_idxz00_8491;
BgL_idxz00_8491 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8482); 
{ /* Llib/thread.scm 344 */
 obj_t BgL_arg1923z00_8492;
{ /* Llib/thread.scm 344 */
 long BgL_arg1924z00_8493;
BgL_arg1924z00_8493 = 
(BgL_idxz00_8491+BgL_arg1933z00_8483); 
BgL_arg1923z00_8492 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8493); } 
BgL_test3854z00_13241 = 
(BgL_arg1923z00_8492==BgL_classz00_8480); } }  else 
{ /* Llib/thread.scm 344 */
 bool_t BgL_res2044z00_8498;
{ /* Llib/thread.scm 344 */
 obj_t BgL_oclassz00_8502;
{ /* Llib/thread.scm 344 */
 obj_t BgL_arg1937z00_8504; long BgL_arg1938z00_8505;
BgL_arg1937z00_8504 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 344 */
 long BgL_arg1939z00_8506;
BgL_arg1939z00_8506 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8482); 
BgL_arg1938z00_8505 = 
(BgL_arg1939z00_8506-OBJECT_TYPE); } 
BgL_oclassz00_8502 = 
VECTOR_REF(BgL_arg1937z00_8504,BgL_arg1938z00_8505); } 
{ /* Llib/thread.scm 344 */
 bool_t BgL__ortest_1147z00_8512;
BgL__ortest_1147z00_8512 = 
(BgL_classz00_8480==BgL_oclassz00_8502); 
if(BgL__ortest_1147z00_8512)
{ /* Llib/thread.scm 344 */
BgL_res2044z00_8498 = BgL__ortest_1147z00_8512; }  else 
{ /* Llib/thread.scm 344 */
 long BgL_odepthz00_8513;
{ /* Llib/thread.scm 344 */
 obj_t BgL_arg1927z00_8514;
BgL_arg1927z00_8514 = 
(BgL_oclassz00_8502); 
BgL_odepthz00_8513 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8514); } 
if(
(BgL_arg1933z00_8483<BgL_odepthz00_8513))
{ /* Llib/thread.scm 344 */
 obj_t BgL_arg1925z00_8518;
{ /* Llib/thread.scm 344 */
 obj_t BgL_arg1926z00_8519;
BgL_arg1926z00_8519 = 
(BgL_oclassz00_8502); 
BgL_arg1925z00_8518 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8519, BgL_arg1933z00_8483); } 
BgL_res2044z00_8498 = 
(BgL_arg1925z00_8518==BgL_classz00_8480); }  else 
{ /* Llib/thread.scm 344 */
BgL_res2044z00_8498 = ((bool_t)0); } } } } 
BgL_test3854z00_13241 = BgL_res2044z00_8498; } }  else 
{ /* Llib/thread.scm 344 */
BgL_test3854z00_13241 = ((bool_t)0)
; } } 
if(BgL_test3854z00_13241)
{ /* Llib/thread.scm 344 */
BgL_tbz00_8520 = 
((BgL_nothreadzd2backendzd2_bglt)BgL_tbz00_3821); }  else 
{ 
 obj_t BgL_auxz00_13266;
BgL_auxz00_13266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(13727L), BGl_string2872z00zz__threadz00, BGl_string2546z00zz__threadz00, BgL_tbz00_3821); 
FAILURE(BgL_auxz00_13266,BFALSE,BFALSE);} } 
return BGl_za2nothreadzd2currentza2zd2zz__threadz00;} } 

}



/* &tb-make-thread-nothr1182 */
BgL_threadz00_bglt BGl_z62tbzd2makezd2threadzd2nothr1182zb0zz__threadz00(obj_t BgL_envz00_3822, obj_t BgL_tbz00_3823, obj_t BgL_bodyz00_3824, obj_t BgL_namez00_3825)
{
{ /* Llib/thread.scm 336 */
{ /* Llib/thread.scm 337 */
 BgL_nothreadzd2backendzd2_bglt BgL_tbz00_8561;
{ /* Llib/thread.scm 337 */
 bool_t BgL_test3859z00_13270;
{ /* Llib/thread.scm 337 */
 obj_t BgL_classz00_8521;
BgL_classz00_8521 = BGl_nothreadzd2backendzd2zz__threadz00; 
if(
BGL_OBJECTP(BgL_tbz00_3823))
{ /* Llib/thread.scm 337 */
 BgL_objectz00_bglt BgL_arg1932z00_8523; long BgL_arg1933z00_8524;
BgL_arg1932z00_8523 = 
(BgL_objectz00_bglt)(BgL_tbz00_3823); 
BgL_arg1933z00_8524 = 
BGL_CLASS_DEPTH(BgL_classz00_8521); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/thread.scm 337 */
 long BgL_idxz00_8532;
BgL_idxz00_8532 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg1932z00_8523); 
{ /* Llib/thread.scm 337 */
 obj_t BgL_arg1923z00_8533;
{ /* Llib/thread.scm 337 */
 long BgL_arg1924z00_8534;
BgL_arg1924z00_8534 = 
(BgL_idxz00_8532+BgL_arg1933z00_8524); 
BgL_arg1923z00_8533 = 
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,BgL_arg1924z00_8534); } 
BgL_test3859z00_13270 = 
(BgL_arg1923z00_8533==BgL_classz00_8521); } }  else 
{ /* Llib/thread.scm 337 */
 bool_t BgL_res2044z00_8539;
{ /* Llib/thread.scm 337 */
 obj_t BgL_oclassz00_8543;
{ /* Llib/thread.scm 337 */
 obj_t BgL_arg1937z00_8545; long BgL_arg1938z00_8546;
BgL_arg1937z00_8545 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/thread.scm 337 */
 long BgL_arg1939z00_8547;
BgL_arg1939z00_8547 = 
BGL_OBJECT_CLASS_NUM(BgL_arg1932z00_8523); 
BgL_arg1938z00_8546 = 
(BgL_arg1939z00_8547-OBJECT_TYPE); } 
BgL_oclassz00_8543 = 
VECTOR_REF(BgL_arg1937z00_8545,BgL_arg1938z00_8546); } 
{ /* Llib/thread.scm 337 */
 bool_t BgL__ortest_1147z00_8553;
BgL__ortest_1147z00_8553 = 
(BgL_classz00_8521==BgL_oclassz00_8543); 
if(BgL__ortest_1147z00_8553)
{ /* Llib/thread.scm 337 */
BgL_res2044z00_8539 = BgL__ortest_1147z00_8553; }  else 
{ /* Llib/thread.scm 337 */
 long BgL_odepthz00_8554;
{ /* Llib/thread.scm 337 */
 obj_t BgL_arg1927z00_8555;
BgL_arg1927z00_8555 = 
(BgL_oclassz00_8543); 
BgL_odepthz00_8554 = 
BGL_CLASS_DEPTH(BgL_arg1927z00_8555); } 
if(
(BgL_arg1933z00_8524<BgL_odepthz00_8554))
{ /* Llib/thread.scm 337 */
 obj_t BgL_arg1925z00_8559;
{ /* Llib/thread.scm 337 */
 obj_t BgL_arg1926z00_8560;
BgL_arg1926z00_8560 = 
(BgL_oclassz00_8543); 
BgL_arg1925z00_8559 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg1926z00_8560, BgL_arg1933z00_8524); } 
BgL_res2044z00_8539 = 
(BgL_arg1925z00_8559==BgL_classz00_8521); }  else 
{ /* Llib/thread.scm 337 */
BgL_res2044z00_8539 = ((bool_t)0); } } } } 
BgL_test3859z00_13270 = BgL_res2044z00_8539; } }  else 
{ /* Llib/thread.scm 337 */
BgL_test3859z00_13270 = ((bool_t)0)
; } } 
if(BgL_test3859z00_13270)
{ /* Llib/thread.scm 337 */
BgL_tbz00_8561 = 
((BgL_nothreadzd2backendzd2_bglt)BgL_tbz00_3823); }  else 
{ 
 obj_t BgL_auxz00_13295;
BgL_auxz00_13295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(13443L), BGl_string2880z00zz__threadz00, BGl_string2546z00zz__threadz00, BgL_tbz00_3823); 
FAILURE(BgL_auxz00_13295,BFALSE,BFALSE);} } 
{ /* Llib/thread.scm 337 */
 BgL_nothreadz00_bglt BgL_new1062z00_8562;
{ /* Llib/thread.scm 339 */
 BgL_nothreadz00_bglt BgL_new1061z00_8563;
BgL_new1061z00_8563 = 
((BgL_nothreadz00_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_nothreadz00_bgl) ))); 
{ /* Llib/thread.scm 339 */
 long BgL_arg1521z00_8564;
BgL_arg1521z00_8564 = 
BGL_CLASS_NUM(BGl_nothreadz00zz__threadz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1061z00_8563), BgL_arg1521z00_8564); } 
BgL_new1062z00_8562 = BgL_new1061z00_8563; } 
((((BgL_threadz00_bglt)COBJECT(
((BgL_threadz00_bglt)BgL_new1062z00_8562)))->BgL_namez00)=((obj_t)BgL_namez00_3825),BUNSPEC); 
{ 
 obj_t BgL_auxz00_13305;
if(
PROCEDUREP(BgL_bodyz00_3824))
{ /* Llib/thread.scm 338 */
BgL_auxz00_13305 = BgL_bodyz00_3824
; }  else 
{ 
 obj_t BgL_auxz00_13308;
BgL_auxz00_13308 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(13472L), BGl_string2873z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_bodyz00_3824); 
FAILURE(BgL_auxz00_13308,BFALSE,BFALSE);} 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1062z00_8562))->BgL_bodyz00)=((obj_t)BgL_auxz00_13305),BUNSPEC); } 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1062z00_8562))->BgL_z52specificz52)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1062z00_8562))->BgL_z52cleanupz52)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1062z00_8562))->BgL_endzd2resultzd2)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1062z00_8562))->BgL_endzd2exceptionzd2)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_nothreadz00_bglt)COBJECT(BgL_new1062z00_8562))->BgL_z52namez52)=((obj_t)BGl_string2559z00zz__threadz00),BUNSPEC); 
{ /* Llib/thread.scm 337 */
 obj_t BgL_fun1519z00_8565;
BgL_fun1519z00_8565 = 
BGl_classzd2constructorzd2zz__objectz00(BGl_nothreadz00zz__threadz00); 
{ /* Llib/thread.scm 337 */
 obj_t BgL_funz00_8566;
if(
PROCEDUREP(BgL_fun1519z00_8565))
{ /* Llib/thread.scm 337 */
BgL_funz00_8566 = BgL_fun1519z00_8565; }  else 
{ 
 obj_t BgL_auxz00_13321;
BgL_auxz00_13321 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2412z00zz__threadz00, 
BINT(13443L), BGl_string2873z00zz__threadz00, BGl_string2427z00zz__threadz00, BgL_fun1519z00_8565); 
FAILURE(BgL_auxz00_13321,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_8566, 1))
{ /* Llib/thread.scm 337 */
(VA_PROCEDUREP( BgL_funz00_8566 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_8566))(BgL_fun1519z00_8565, 
((obj_t)BgL_new1062z00_8562), BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_8566))(BgL_fun1519z00_8565, 
((obj_t)BgL_new1062z00_8562)) ); }  else 
{ /* Llib/thread.scm 337 */
FAILURE(BGl_string2874z00zz__threadz00,BGl_list2875z00zz__threadz00,BgL_funz00_8566);} } } 
return 
((BgL_threadz00_bglt)BgL_new1062z00_8562);} } } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__threadz00(void)
{
{ /* Llib/thread.scm 17 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2488z00zz__threadz00)); 
return 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string2488z00zz__threadz00));} 

}

#ifdef __cplusplus
}
#endif
