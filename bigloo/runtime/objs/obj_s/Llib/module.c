/*===========================================================================*/
/*   (Llib/module.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/module.scm -indent -o objs/obj_s/Llib/module.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MODULE_TYPE_DEFINITIONS
#define BGL___MODULE_TYPE_DEFINITIONS
#endif // BGL___MODULE_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_symbol1832z00zz__modulez00 = BUNSPEC;
static obj_t BGl_symbol1838z00zz__modulez00 = BUNSPEC;
static obj_t BGl_z62modulezd2addzd2accessz12z70zz__modulez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol1840z00zz__modulez00 = BUNSPEC;
static obj_t BGl_symbol1842z00zz__modulez00 = BUNSPEC;
static obj_t BGl_symbol1844z00zz__modulez00 = BUNSPEC;
static obj_t BGl_z62bigloozd2modulezd2resolverzd2setz12za2zz__modulez00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__modulez00 = BUNSPEC;
static obj_t BGl_resolvezd2abasezf2bucketz20zz__modulez00(obj_t, obj_t);
static obj_t BGl_za2afileszd2tableza2zd2zz__modulez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void);
extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
extern obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_symbol1865z00zz__modulez00 = BUNSPEC;
static obj_t BGl_symbol1867z00zz__modulez00 = BUNSPEC;
static obj_t BGl_symbol1869z00zz__modulez00 = BUNSPEC;
static obj_t BGl_list1837z00zz__modulez00 = BUNSPEC;
extern bool_t fexists(char *);
extern obj_t BGl_warningz00zz__errorz00(obj_t);
extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62zc3z04z52bigloozd2modulezd2res1186ze3z14zz__modulez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__modulez00(void);
static obj_t BGl_z62modulezd2abasezd2setz12z70zz__modulez00(obj_t, obj_t);
extern obj_t BGl_filezd2namezd2canonicaliza7eza7zz__osz00(obj_t);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__modulez00(void);
extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_genericzd2initzd2zz__modulez00(void);
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__modulez00(void);
static obj_t BGl_list1864z00zz__modulez00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__modulez00(void);
static obj_t BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 = BUNSPEC;
static obj_t BGl_objectzd2initzd2zz__modulez00(void);
extern obj_t BGl_dirnamez00zz__osz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2abasezd2setz12z12zz__modulez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2modulezd2resolverzd2setz12zc0zz__modulez00(obj_t);
extern bool_t bgl_directoryp(char *);
extern obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
extern obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00(obj_t, obj_t, obj_t);
extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62modulezd2abasezb0zz__modulez00(obj_t);
static obj_t BGl_modulezd2readzd2accesszd2filezd2zz__modulez00(obj_t);
static obj_t BGl_methodzd2initzd2zz__modulez00(void);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
extern obj_t BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2abasezd2zz__modulez00(void);
static obj_t BGl_z62bigloozd2modulezd2resolverz62zz__modulez00(obj_t);
static obj_t BGl_z62modulezd2defaultzd2resolverz62zz__modulez00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(obj_t);
static obj_t BGl_z62modulezd2loadzd2accesszd2filezb0zz__modulez00(obj_t, obj_t);
static obj_t BGl_resolvezd2abaseza2z70zz__modulez00(obj_t);
extern obj_t string_append(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31351ze3ze5zz__modulez00(obj_t, obj_t);
static obj_t BGl_afilezd2tablezd2zz__modulez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_modulezd2addzd2accessz12z12zz__modulez00(obj_t, obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_moduleszd2mutexzd2zz__modulez00 = BUNSPEC;
extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string1870z00zz__modulez00, BgL_bgl_string1870za700za7za7_1891za7, "files", 5 );
DEFINE_STRING( BGl_string1871z00zz__modulez00, BgL_bgl_string1871za700za7za7_1892za7, "relative", 8 );
DEFINE_STRING( BGl_string1872z00zz__modulez00, BgL_bgl_string1872za700za7za7_1893za7, "string-ref", 10 );
DEFINE_STRING( BGl_string1873z00zz__modulez00, BgL_bgl_string1873za700za7za7_1894za7, "map", 3 );
DEFINE_STRING( BGl_string1874z00zz__modulez00, BgL_bgl_string1874za700za7za7_1895za7, "list", 4 );
DEFINE_STRING( BGl_string1875z00zz__modulez00, BgL_bgl_string1875za700za7za7_1896za7, "&module-add-access!", 19 );
DEFINE_STRING( BGl_string1876z00zz__modulez00, BgL_bgl_string1876za700za7za7_1897za7, "module-read-access-file", 23 );
DEFINE_STRING( BGl_string1877z00zz__modulez00, BgL_bgl_string1877za700za7za7_1898za7, "input-port", 10 );
DEFINE_STRING( BGl_string1878z00zz__modulez00, BgL_bgl_string1878za700za7za7_1899za7, "<@anonymous:1310>", 17 );
DEFINE_STRING( BGl_string1879z00zz__modulez00, BgL_bgl_string1879za700za7za7_1900za7, "Illegal entry -- ", 17 );
DEFINE_STRING( BGl_string1880z00zz__modulez00, BgL_bgl_string1880za700za7za7_1901za7, "module-load-access-file", 23 );
DEFINE_STRING( BGl_string1881z00zz__modulez00, BgL_bgl_string1881za700za7za7_1902za7, "struct", 6 );
DEFINE_STRING( BGl_string1882z00zz__modulez00, BgL_bgl_string1882za700za7za7_1903za7, ".afile", 6 );
DEFINE_STRING( BGl_string1883z00zz__modulez00, BgL_bgl_string1883za700za7za7_1904za7, "&module-load-access-file", 24 );
DEFINE_STRING( BGl_string1884z00zz__modulez00, BgL_bgl_string1884za700za7za7_1905za7, "", 0 );
DEFINE_STRING( BGl_string1885z00zz__modulez00, BgL_bgl_string1885za700za7za7_1906za7, "relative-path", 13 );
DEFINE_STRING( BGl_string1886z00zz__modulez00, BgL_bgl_string1886za700za7za7_1907za7, "<@anonymous:1351>", 17 );
DEFINE_STRING( BGl_string1887z00zz__modulez00, BgL_bgl_string1887za700za7za7_1908za7, "<@anonymous:1352>", 17 );
DEFINE_STRING( BGl_string1888z00zz__modulez00, BgL_bgl_string1888za700za7za7_1909za7, "<@anonymous:1356>", 17 );
DEFINE_STRING( BGl_string1889z00zz__modulez00, BgL_bgl_string1889za700za7za7_1910za7, "for-each", 8 );
DEFINE_STRING( BGl_string1890z00zz__modulez00, BgL_bgl_string1890za700za7za7_1911za7, "__module", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_modulezd2abasezd2envz00zz__modulez00, BgL_bgl_za762moduleza7d2abas1912z00, BGl_z62modulezd2abasezb0zz__modulez00, 0L, BUNSPEC, 0 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_modulezd2defaultzd2resolverzd2envzd2zz__modulez00, BgL_bgl_za762moduleza7d2defa1913z00, BGl_z62modulezd2defaultzd2resolverz62zz__modulez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_modulezd2loadzd2accesszd2filezd2envz00zz__modulez00, BgL_bgl_za762moduleza7d2load1914z00, BGl_z62modulezd2loadzd2accesszd2filezb0zz__modulez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2modulezd2resolverzd2setz12zd2envz12zz__modulez00, BgL_bgl_za762biglooza7d2modu1915z00, BGl_z62bigloozd2modulezd2resolverzd2setz12za2zz__modulez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2modulezd2resolverzd2envzd2zz__modulez00, BgL_bgl_za762biglooza7d2modu1916z00, BGl_z62bigloozd2modulezd2resolverz62zz__modulez00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_modulezd2abasezd2setz12zd2envzc0zz__modulez00, BgL_bgl_za762moduleza7d2abas1917z00, BGl_z62modulezd2abasezd2setz12z70zz__modulez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1828z00zz__modulez00, BgL_bgl_string1828za700za7za7_1918za7, "modules", 7 );
DEFINE_STRING( BGl_string1829z00zz__modulez00, BgL_bgl_string1829za700za7za7_1919za7, "/tmp/bigloo/runtime/Llib/module.scm", 35 );
DEFINE_STRING( BGl_string1830z00zz__modulez00, BgL_bgl_string1830za700za7za7_1920za7, "bigloo-module-resolver", 22 );
DEFINE_STRING( BGl_string1831z00zz__modulez00, BgL_bgl_string1831za700za7za7_1921za7, "procedure", 9 );
DEFINE_STRING( BGl_string1833z00zz__modulez00, BgL_bgl_string1833za700za7za7_1922za7, "bigloo-module-resolver-set!", 27 );
DEFINE_STRING( BGl_string1834z00zz__modulez00, BgL_bgl_string1834za700za7za7_1923za7, "Illegal resolver", 16 );
DEFINE_STRING( BGl_string1835z00zz__modulez00, BgL_bgl_string1835za700za7za7_1924za7, "&bigloo-module-resolver-set!", 28 );
DEFINE_STRING( BGl_string1836z00zz__modulez00, BgL_bgl_string1836za700za7za7_1925za7, "<@%bigloo-module-res1186>:Wrong number of arguments", 51 );
DEFINE_STRING( BGl_string1839z00zz__modulez00, BgL_bgl_string1839za700za7za7_1926za7, "funcall", 7 );
DEFINE_STRING( BGl_string1841z00zz__modulez00, BgL_bgl_string1841za700za7za7_1927za7, "resolve", 7 );
DEFINE_STRING( BGl_string1843z00zz__modulez00, BgL_bgl_string1843za700za7za7_1928za7, "module", 6 );
DEFINE_STRING( BGl_string1845z00zz__modulez00, BgL_bgl_string1845za700za7za7_1929za7, "abase", 5 );
DEFINE_STRING( BGl_string1846z00zz__modulez00, BgL_bgl_string1846za700za7za7_1930za7, ".", 1 );
DEFINE_STRING( BGl_string1847z00zz__modulez00, BgL_bgl_string1847za700za7za7_1931za7, "module-default-resolver", 23 );
DEFINE_STRING( BGl_string1848z00zz__modulez00, BgL_bgl_string1848za700za7za7_1932za7, "bstring", 7 );
DEFINE_STRING( BGl_string1849z00zz__modulez00, BgL_bgl_string1849za700za7za7_1933za7, "&module-default-resolver", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_modulezd2addzd2accessz12zd2envzc0zz__modulez00, BgL_bgl_za762moduleza7d2addza71934za7, BGl_z62modulezd2addzd2accessz12z70zz__modulez00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string1850z00zz__modulez00, BgL_bgl_string1850za700za7za7_1935za7, "symbol", 6 );
DEFINE_STRING( BGl_string1851z00zz__modulez00, BgL_bgl_string1851za700za7za7_1936za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string1852z00zz__modulez00, BgL_bgl_string1852za700za7za7_1937za7, "loop", 4 );
DEFINE_STRING( BGl_string1853z00zz__modulez00, BgL_bgl_string1853za700za7za7_1938za7, "pair", 4 );
DEFINE_STRING( BGl_string1854z00zz__modulez00, BgL_bgl_string1854za700za7za7_1939za7, "resolve-abase/bucket", 20 );
DEFINE_STRING( BGl_string1855z00zz__modulez00, BgL_bgl_string1855za700za7za7_1940za7, "<@anonymous:1205>", 17 );
DEFINE_STRING( BGl_string1856z00zz__modulez00, BgL_bgl_string1856za700za7za7_1941za7, ".scm", 4 );
DEFINE_STRING( BGl_string1857z00zz__modulez00, BgL_bgl_string1857za700za7za7_1942za7, "module-add-access-inner!", 24 );
DEFINE_STRING( BGl_string1858z00zz__modulez00, BgL_bgl_string1858za700za7za7_1943za7, "\"", 1 );
DEFINE_STRING( BGl_string1859z00zz__modulez00, BgL_bgl_string1859za700za7za7_1944za7, "] in directory \"", 16 );
DEFINE_STRING( BGl_string1860z00zz__modulez00, BgL_bgl_string1860za700za7za7_1945za7, " ", 1 );
DEFINE_STRING( BGl_string1861z00zz__modulez00, BgL_bgl_string1861za700za7za7_1946za7, " [", 2 );
DEFINE_STRING( BGl_string1862z00zz__modulez00, BgL_bgl_string1862za700za7za7_1947za7, "access redefinition -- ", 23 );
DEFINE_STRING( BGl_string1863z00zz__modulez00, BgL_bgl_string1863za700za7za7_1948za7, "add-access!", 11 );
DEFINE_STRING( BGl_string1866z00zz__modulez00, BgL_bgl_string1866za700za7za7_1949za7, "set-cdr!", 8 );
DEFINE_STRING( BGl_string1868z00zz__modulez00, BgL_bgl_string1868za700za7za7_1950za7, "cell", 4 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1832z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1838z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1840z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1842z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1844z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__modulez00) );
ADD_ROOT( (void *)(&BGl_za2afileszd2tableza2zd2zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1865z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1867z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_symbol1869z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_list1837z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_list1864z00zz__modulez00) );
ADD_ROOT( (void *)(&BGl_z52bigloozd2modulezd2resolverz52zz__modulez00) );
ADD_ROOT( (void *)(&BGl_afilezd2tablezd2zz__modulez00) );
ADD_ROOT( (void *)(&BGl_moduleszd2mutexzd2zz__modulez00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long BgL_checksumz00_2211, char * BgL_fromz00_2212)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__modulez00))
{ 
BGl_requirezd2initializa7ationz75zz__modulez00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__modulez00(); 
BGl_cnstzd2initzd2zz__modulez00(); 
BGl_importedzd2moduleszd2initz00zz__modulez00(); 
return 
BGl_toplevelzd2initzd2zz__modulez00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
BGl_symbol1832z00zz__modulez00 = 
bstring_to_symbol(BGl_string1833z00zz__modulez00); 
BGl_symbol1838z00zz__modulez00 = 
bstring_to_symbol(BGl_string1839z00zz__modulez00); 
BGl_symbol1840z00zz__modulez00 = 
bstring_to_symbol(BGl_string1841z00zz__modulez00); 
BGl_symbol1842z00zz__modulez00 = 
bstring_to_symbol(BGl_string1843z00zz__modulez00); 
BGl_symbol1844z00zz__modulez00 = 
bstring_to_symbol(BGl_string1845z00zz__modulez00); 
BGl_list1837z00zz__modulez00 = 
MAKE_YOUNG_PAIR(BGl_symbol1838z00zz__modulez00, 
MAKE_YOUNG_PAIR(BGl_symbol1840z00zz__modulez00, 
MAKE_YOUNG_PAIR(BGl_symbol1840z00zz__modulez00, 
MAKE_YOUNG_PAIR(BGl_symbol1842z00zz__modulez00, 
MAKE_YOUNG_PAIR(BGl_symbol1844z00zz__modulez00, BNIL))))); 
BGl_symbol1865z00zz__modulez00 = 
bstring_to_symbol(BGl_string1866z00zz__modulez00); 
BGl_symbol1867z00zz__modulez00 = 
bstring_to_symbol(BGl_string1868z00zz__modulez00); 
BGl_symbol1869z00zz__modulez00 = 
bstring_to_symbol(BGl_string1870z00zz__modulez00); 
return ( 
BGl_list1864z00zz__modulez00 = 
MAKE_YOUNG_PAIR(BGl_symbol1865z00zz__modulez00, 
MAKE_YOUNG_PAIR(BGl_symbol1867z00zz__modulez00, 
MAKE_YOUNG_PAIR(BGl_symbol1869z00zz__modulez00, BNIL))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
BGl_moduleszd2mutexzd2zz__modulez00 = 
bgl_make_mutex(BGl_string1828z00zz__modulez00); 
BGl_afilezd2tablezd2zz__modulez00 = BNIL; 
BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 = BGl_modulezd2defaultzd2resolverzd2envzd2zz__modulez00; 
{ /* Llib/module.scm 218 */
 obj_t BgL_list1175z00_1120;
BgL_list1175z00_1120 = 
MAKE_YOUNG_PAIR(
BINT(256L), BNIL); 
return ( 
BGl_za2afileszd2tableza2zd2zz__modulez00 = 
BGl_makezd2hashtablezd2zz__hashz00(BgL_list1175z00_1120), BUNSPEC) ;} } 

}



/* module-abase */
BGL_EXPORTED_DEF obj_t BGl_modulezd2abasezd2zz__modulez00(void)
{
{ /* Llib/module.scm 75 */
return 
BGL_ABASE();} 

}



/* &module-abase */
obj_t BGl_z62modulezd2abasezb0zz__modulez00(obj_t BgL_envz00_2047)
{
{ /* Llib/module.scm 75 */
return 
BGl_modulezd2abasezd2zz__modulez00();} 

}



/* module-abase-set! */
BGL_EXPORTED_DEF obj_t BGl_modulezd2abasezd2setz12z12zz__modulez00(obj_t BgL_valz00_3)
{
{ /* Llib/module.scm 78 */
return 
BGL_ABASE_SET(BgL_valz00_3);} 

}



/* &module-abase-set! */
obj_t BGl_z62modulezd2abasezd2setz12z70zz__modulez00(obj_t BgL_envz00_2048, obj_t BgL_valz00_2049)
{
{ /* Llib/module.scm 78 */
return 
BGl_modulezd2abasezd2setz12z12zz__modulez00(BgL_valz00_2049);} 

}



/* bigloo-module-resolver */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void)
{
{ /* Llib/module.scm 91 */
{ /* Llib/module.scm 91 */
 obj_t BgL_aux1747z00_2080;
BgL_aux1747z00_2080 = BGl_z52bigloozd2modulezd2resolverz52zz__modulez00; 
if(
PROCEDUREP(BgL_aux1747z00_2080))
{ /* Llib/module.scm 91 */
return BgL_aux1747z00_2080;}  else 
{ 
 obj_t BgL_auxz00_2247;
BgL_auxz00_2247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(3416L), BGl_string1830z00zz__modulez00, BGl_string1831z00zz__modulez00, BgL_aux1747z00_2080); 
FAILURE(BgL_auxz00_2247,BFALSE,BFALSE);} } } 

}



/* &bigloo-module-resolver */
obj_t BGl_z62bigloozd2modulezd2resolverz62zz__modulez00(obj_t BgL_envz00_2050)
{
{ /* Llib/module.scm 91 */
return 
BGl_bigloozd2modulezd2resolverz00zz__modulez00();} 

}



/* bigloo-module-resolver-set! */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2modulezd2resolverzd2setz12zc0zz__modulez00(obj_t BgL_resolvez00_4)
{
{ /* Llib/module.scm 94 */
{ /* Llib/module.scm 95 */
 obj_t BgL_top1954z00_2253;
BgL_top1954z00_2253 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1954z00_2253, BGl_moduleszd2mutexzd2zz__modulez00); BUNSPEC; 
{ /* Llib/module.scm 95 */
 obj_t BgL_tmp1953z00_2252;
if(
PROCEDURE_CORRECT_ARITYP(BgL_resolvez00_4, 
(int)(2L)))
{ /* Llib/module.scm 100 */
 obj_t BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2051;
BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2051 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04z52bigloozd2modulezd2res1186ze3z14zz__modulez00, 
(int)(3L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2051, 
(int)(0L), BgL_resolvez00_4); 
BgL_tmp1953z00_2252 = ( 
BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 = BgL_zc3z04z52bigloozd2modulezd2res1186ze3z76_2051, BUNSPEC) ; }  else 
{ /* Llib/module.scm 97 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_resolvez00_4, 
(int)(3L)))
{ /* Llib/module.scm 101 */
BgL_tmp1953z00_2252 = ( 
BGl_z52bigloozd2modulezd2resolverz52zz__modulez00 = BgL_resolvez00_4, BUNSPEC) ; }  else 
{ /* Llib/module.scm 101 */
BgL_tmp1953z00_2252 = 
BGl_errorz00zz__errorz00(BGl_symbol1832z00zz__modulez00, BGl_string1834z00zz__modulez00, BgL_resolvez00_4); } } 
BGL_EXITD_POP_PROTECT(BgL_top1954z00_2253); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
return BgL_tmp1953z00_2252;} } } 

}



/* &bigloo-module-resolver-set! */
obj_t BGl_z62bigloozd2modulezd2resolverzd2setz12za2zz__modulez00(obj_t BgL_envz00_2052, obj_t BgL_resolvez00_2053)
{
{ /* Llib/module.scm 94 */
{ /* Llib/module.scm 95 */
 obj_t BgL_auxz00_2271;
if(
PROCEDUREP(BgL_resolvez00_2053))
{ /* Llib/module.scm 95 */
BgL_auxz00_2271 = BgL_resolvez00_2053
; }  else 
{ 
 obj_t BgL_auxz00_2274;
BgL_auxz00_2274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(3527L), BGl_string1835z00zz__modulez00, BGl_string1831z00zz__modulez00, BgL_resolvez00_2053); 
FAILURE(BgL_auxz00_2274,BFALSE,BFALSE);} 
return 
BGl_bigloozd2modulezd2resolverzd2setz12zc0zz__modulez00(BgL_auxz00_2271);} } 

}



/* &<@%bigloo-module-res1186> */
obj_t BGl_z62zc3z04z52bigloozd2modulezd2res1186ze3z14zz__modulez00(obj_t BgL_envz00_2054, obj_t BgL_modulez00_2056, obj_t BgL_filesz00_2057, obj_t BgL_abasez00_2058)
{
{ /* Llib/module.scm 100 */
{ /* Llib/module.scm 100 */
 obj_t BgL_resolvez00_2055;
BgL_resolvez00_2055 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2054, 
(int)(0L))); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_resolvez00_2055, 2))
{ /* Llib/module.scm 100 */
return 
BGL_PROCEDURE_CALL2(BgL_resolvez00_2055, BgL_modulez00_2056, BgL_abasez00_2058);}  else 
{ /* Llib/module.scm 100 */
FAILURE(BGl_string1836z00zz__modulez00,BGl_list1837z00zz__modulez00,BgL_resolvez00_2055);} } } 

}



/* &module-default-resolver */
obj_t BGl_z62modulezd2defaultzd2resolverz62zz__modulez00(obj_t BgL_envz00_2043, obj_t BgL_modz00_2044, obj_t BgL_filesz00_2045, obj_t BgL_abasez00_2046)
{
{ /* Llib/module.scm 113 */
{ /* Llib/module.scm 114 */
 obj_t BgL_modz00_2171; obj_t BgL_filesz00_2172;
if(
SYMBOLP(BgL_modz00_2044))
{ /* Llib/module.scm 114 */
BgL_modz00_2171 = BgL_modz00_2044; }  else 
{ 
 obj_t BgL_auxz00_2292;
BgL_auxz00_2292 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(4515L), BGl_string1849z00zz__modulez00, BGl_string1850z00zz__modulez00, BgL_modz00_2044); 
FAILURE(BgL_auxz00_2292,BFALSE,BFALSE);} 
{ /* Llib/module.scm 114 */
 bool_t BgL_test1960z00_2296;
if(
PAIRP(BgL_filesz00_2045))
{ /* Llib/module.scm 114 */
BgL_test1960z00_2296 = ((bool_t)1)
; }  else 
{ /* Llib/module.scm 114 */
BgL_test1960z00_2296 = 
NULLP(BgL_filesz00_2045)
; } 
if(BgL_test1960z00_2296)
{ /* Llib/module.scm 114 */
BgL_filesz00_2172 = BgL_filesz00_2045; }  else 
{ 
 obj_t BgL_auxz00_2300;
BgL_auxz00_2300 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(4515L), BGl_string1849z00zz__modulez00, BGl_string1851z00zz__modulez00, BgL_filesz00_2045); 
FAILURE(BgL_auxz00_2300,BFALSE,BFALSE);} } 
{ /* Llib/module.scm 114 */
 obj_t BgL_top1963z00_2305;
BgL_top1963z00_2305 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
BGL_EXITD_PUSH_PROTECT(BgL_top1963z00_2305, BGl_moduleszd2mutexzd2zz__modulez00); BUNSPEC; 
{ /* Llib/module.scm 114 */
 obj_t BgL_tmp1962z00_2304;
if(
NULLP(BgL_filesz00_2172))
{ /* Llib/module.scm 116 */
if(
NULLP(BgL_abasez00_2046))
{ /* Llib/module.scm 150 */
 obj_t BgL_basez00_2173;
BgL_basez00_2173 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BGl_string1846z00zz__modulez00, BGl_afilezd2tablezd2zz__modulez00); 
if(
PAIRP(BgL_basez00_2173))
{ /* Llib/module.scm 151 */
BgL_tmp1962z00_2304 = 
BGl_resolvezd2abasezf2bucketz20zz__modulez00(BgL_modz00_2171, BgL_basez00_2173); }  else 
{ /* Llib/module.scm 151 */
BgL_tmp1962z00_2304 = BNIL; } }  else 
{ /* Llib/module.scm 119 */
if(
STRINGP(BgL_abasez00_2046))
{ /* Llib/module.scm 150 */
 obj_t BgL_basez00_2174;
BgL_basez00_2174 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_abasez00_2046, BGl_afilezd2tablezd2zz__modulez00); 
if(
PAIRP(BgL_basez00_2174))
{ /* Llib/module.scm 151 */
BgL_tmp1962z00_2304 = 
BGl_resolvezd2abasezf2bucketz20zz__modulez00(BgL_modz00_2171, BgL_basez00_2174); }  else 
{ /* Llib/module.scm 151 */
BgL_tmp1962z00_2304 = BNIL; } }  else 
{ /* Llib/module.scm 121 */
if(
PAIRP(BgL_abasez00_2046))
{ 
 obj_t BgL_abasez00_2176;
BgL_abasez00_2176 = BgL_abasez00_2046; 
BgL_loopz00_2175:
if(
PAIRP(BgL_abasez00_2176))
{ /* Llib/module.scm 126 */
 obj_t BgL_resolvez00_2177;
{ /* Llib/module.scm 126 */
 obj_t BgL_arg1197z00_2178;
BgL_arg1197z00_2178 = 
CAR(BgL_abasez00_2176); 
{ /* Llib/module.scm 150 */
 obj_t BgL_basez00_2179;
BgL_basez00_2179 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1197z00_2178, BGl_afilezd2tablezd2zz__modulez00); 
if(
PAIRP(BgL_basez00_2179))
{ /* Llib/module.scm 151 */
BgL_resolvez00_2177 = 
BGl_resolvezd2abasezf2bucketz20zz__modulez00(BgL_modz00_2171, BgL_basez00_2179); }  else 
{ /* Llib/module.scm 151 */
BgL_resolvez00_2177 = BNIL; } } } 
if(
PAIRP(BgL_resolvez00_2177))
{ /* Llib/module.scm 127 */
BgL_tmp1962z00_2304 = BgL_resolvez00_2177; }  else 
{ 
 obj_t BgL_abasez00_2334;
BgL_abasez00_2334 = 
CDR(BgL_abasez00_2176); 
BgL_abasez00_2176 = BgL_abasez00_2334; 
goto BgL_loopz00_2175;} }  else 
{ /* Llib/module.scm 125 */
BgL_tmp1962z00_2304 = BNIL; } }  else 
{ /* Llib/module.scm 123 */
BgL_tmp1962z00_2304 = 
BGl_resolvezd2abaseza2z70zz__modulez00(BgL_modz00_2171); } } } }  else 
{ /* Llib/module.scm 116 */
{ /* Llib/module.scm 117 */
 obj_t BgL_auxz00_2337;
if(
STRINGP(BgL_abasez00_2046))
{ /* Llib/module.scm 117 */
BgL_auxz00_2337 = BgL_abasez00_2046
; }  else 
{ 
 obj_t BgL_auxz00_2340;
BgL_auxz00_2340 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(4604L), BGl_string1847z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_abasez00_2046); 
FAILURE(BgL_auxz00_2340,BFALSE,BFALSE);} 
BGl_modulezd2addzd2accessz12z12zz__modulez00(BgL_modz00_2171, BgL_filesz00_2172, BgL_auxz00_2337); } 
BgL_tmp1962z00_2304 = BgL_filesz00_2172; } 
BGL_EXITD_POP_PROTECT(BgL_top1963z00_2305); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
return BgL_tmp1962z00_2304;} } } } 

}



/* resolve-abase* */
obj_t BGl_resolvezd2abaseza2z70zz__modulez00(obj_t BgL_modz00_8)
{
{ /* Llib/module.scm 137 */
{ 
 obj_t BgL_afilez00_1153;
BgL_afilez00_1153 = BGl_afilezd2tablezd2zz__modulez00; 
BgL_zc3z04anonymousza31198ze3z87_1154:
if(
NULLP(BgL_afilez00_1153))
{ /* Llib/module.scm 139 */
return BNIL;}  else 
{ /* Llib/module.scm 141 */
 obj_t BgL_fz00_1156;
{ /* Llib/module.scm 141 */
 obj_t BgL_arg1202z00_1159;
{ /* Llib/module.scm 141 */
 obj_t BgL_pairz00_1760;
if(
PAIRP(BgL_afilez00_1153))
{ /* Llib/module.scm 141 */
BgL_pairz00_1760 = BgL_afilez00_1153; }  else 
{ 
 obj_t BgL_auxz00_2351;
BgL_auxz00_2351 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(5305L), BGl_string1852z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_afilez00_1153); 
FAILURE(BgL_auxz00_2351,BFALSE,BFALSE);} 
BgL_arg1202z00_1159 = 
CAR(BgL_pairz00_1760); } 
BgL_fz00_1156 = 
BGl_resolvezd2abasezf2bucketz20zz__modulez00(BgL_modz00_8, BgL_arg1202z00_1159); } 
if(
PAIRP(BgL_fz00_1156))
{ /* Llib/module.scm 142 */
return BgL_fz00_1156;}  else 
{ /* Llib/module.scm 144 */
 obj_t BgL_arg1201z00_1158;
{ /* Llib/module.scm 144 */
 obj_t BgL_pairz00_1761;
if(
PAIRP(BgL_afilez00_1153))
{ /* Llib/module.scm 144 */
BgL_pairz00_1761 = BgL_afilez00_1153; }  else 
{ 
 obj_t BgL_auxz00_2361;
BgL_auxz00_2361 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(5354L), BGl_string1852z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_afilez00_1153); 
FAILURE(BgL_auxz00_2361,BFALSE,BFALSE);} 
BgL_arg1201z00_1158 = 
CDR(BgL_pairz00_1761); } 
{ 
 obj_t BgL_afilez00_2366;
BgL_afilez00_2366 = BgL_arg1201z00_1158; 
BgL_afilez00_1153 = BgL_afilez00_2366; 
goto BgL_zc3z04anonymousza31198ze3z87_1154;} } } } } 

}



/* resolve-abase/bucket */
obj_t BGl_resolvezd2abasezf2bucketz20zz__modulez00(obj_t BgL_modz00_11, obj_t BgL_basez00_12)
{
{ /* Llib/module.scm 158 */
{ /* Llib/module.scm 159 */
 obj_t BgL_cellz00_1163;
{ /* Llib/module.scm 159 */
 obj_t BgL_auxz00_2367;
{ /* Llib/module.scm 159 */
 obj_t BgL_pairz00_1764;
if(
PAIRP(BgL_basez00_12))
{ /* Llib/module.scm 159 */
BgL_pairz00_1764 = BgL_basez00_12; }  else 
{ 
 obj_t BgL_auxz00_2370;
BgL_auxz00_2370 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6028L), BGl_string1854z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_basez00_12); 
FAILURE(BgL_auxz00_2370,BFALSE,BFALSE);} 
{ /* Llib/module.scm 159 */
 obj_t BgL_aux1764z00_2098;
BgL_aux1764z00_2098 = 
CDR(BgL_pairz00_1764); 
{ /* Llib/module.scm 159 */
 bool_t BgL_test1979z00_2375;
if(
PAIRP(BgL_aux1764z00_2098))
{ /* Llib/module.scm 159 */
BgL_test1979z00_2375 = ((bool_t)1)
; }  else 
{ /* Llib/module.scm 159 */
BgL_test1979z00_2375 = 
NULLP(BgL_aux1764z00_2098)
; } 
if(BgL_test1979z00_2375)
{ /* Llib/module.scm 159 */
BgL_auxz00_2367 = BgL_aux1764z00_2098
; }  else 
{ 
 obj_t BgL_auxz00_2379;
BgL_auxz00_2379 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6023L), BGl_string1854z00zz__modulez00, BGl_string1851z00zz__modulez00, BgL_aux1764z00_2098); 
FAILURE(BgL_auxz00_2379,BFALSE,BFALSE);} } } } 
BgL_cellz00_1163 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_modz00_11, BgL_auxz00_2367); } 
if(
PAIRP(BgL_cellz00_1163))
{ /* Llib/module.scm 161 */
 obj_t BgL_hook1075z00_1165;
BgL_hook1075z00_1165 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
{ /* Llib/module.scm 161 */
 obj_t BgL_g1077z00_1166;
BgL_g1077z00_1166 = 
CDR(BgL_cellz00_1163); 
{ 
 obj_t BgL_l1072z00_1168; obj_t BgL_h1073z00_1169;
BgL_l1072z00_1168 = BgL_g1077z00_1166; 
BgL_h1073z00_1169 = BgL_hook1075z00_1165; 
BgL_zc3z04anonymousza31205ze3z87_1170:
if(
NULLP(BgL_l1072z00_1168))
{ /* Llib/module.scm 161 */
return 
CDR(BgL_hook1075z00_1165);}  else 
{ /* Llib/module.scm 161 */
 bool_t BgL_test1983z00_2391;
{ /* Llib/module.scm 161 */
 obj_t BgL_tmpz00_2392;
{ /* Llib/module.scm 161 */
 obj_t BgL_pairz00_1767;
if(
PAIRP(BgL_l1072z00_1168))
{ /* Llib/module.scm 161 */
BgL_pairz00_1767 = BgL_l1072z00_1168; }  else 
{ 
 obj_t BgL_auxz00_2395;
BgL_auxz00_2395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6079L), BGl_string1855z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1072z00_1168); 
FAILURE(BgL_auxz00_2395,BFALSE,BFALSE);} 
BgL_tmpz00_2392 = 
CAR(BgL_pairz00_1767); } 
BgL_test1983z00_2391 = 
STRINGP(BgL_tmpz00_2392); } 
if(BgL_test1983z00_2391)
{ /* Llib/module.scm 161 */
 obj_t BgL_nh1074z00_1174;
{ /* Llib/module.scm 161 */
 obj_t BgL_arg1212z00_1176;
{ /* Llib/module.scm 161 */
 obj_t BgL_pairz00_1768;
if(
PAIRP(BgL_l1072z00_1168))
{ /* Llib/module.scm 161 */
BgL_pairz00_1768 = BgL_l1072z00_1168; }  else 
{ 
 obj_t BgL_auxz00_2403;
BgL_auxz00_2403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6079L), BGl_string1855z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1072z00_1168); 
FAILURE(BgL_auxz00_2403,BFALSE,BFALSE);} 
BgL_arg1212z00_1176 = 
CAR(BgL_pairz00_1768); } 
BgL_nh1074z00_1174 = 
MAKE_YOUNG_PAIR(BgL_arg1212z00_1176, BNIL); } 
SET_CDR(BgL_h1073z00_1169, BgL_nh1074z00_1174); 
{ /* Llib/module.scm 161 */
 obj_t BgL_arg1210z00_1175;
{ /* Llib/module.scm 161 */
 obj_t BgL_pairz00_1770;
if(
PAIRP(BgL_l1072z00_1168))
{ /* Llib/module.scm 161 */
BgL_pairz00_1770 = BgL_l1072z00_1168; }  else 
{ 
 obj_t BgL_auxz00_2412;
BgL_auxz00_2412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6079L), BGl_string1855z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1072z00_1168); 
FAILURE(BgL_auxz00_2412,BFALSE,BFALSE);} 
BgL_arg1210z00_1175 = 
CDR(BgL_pairz00_1770); } 
{ 
 obj_t BgL_h1073z00_2418; obj_t BgL_l1072z00_2417;
BgL_l1072z00_2417 = BgL_arg1210z00_1175; 
BgL_h1073z00_2418 = BgL_nh1074z00_1174; 
BgL_h1073z00_1169 = BgL_h1073z00_2418; 
BgL_l1072z00_1168 = BgL_l1072z00_2417; 
goto BgL_zc3z04anonymousza31205ze3z87_1170;} } }  else 
{ /* Llib/module.scm 161 */
 obj_t BgL_arg1215z00_1177;
{ /* Llib/module.scm 161 */
 obj_t BgL_pairz00_1771;
if(
PAIRP(BgL_l1072z00_1168))
{ /* Llib/module.scm 161 */
BgL_pairz00_1771 = BgL_l1072z00_1168; }  else 
{ 
 obj_t BgL_auxz00_2421;
BgL_auxz00_2421 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6079L), BGl_string1855z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1072z00_1168); 
FAILURE(BgL_auxz00_2421,BFALSE,BFALSE);} 
BgL_arg1215z00_1177 = 
CDR(BgL_pairz00_1771); } 
{ 
 obj_t BgL_l1072z00_2426;
BgL_l1072z00_2426 = BgL_arg1215z00_1177; 
BgL_l1072z00_1168 = BgL_l1072z00_2426; 
goto BgL_zc3z04anonymousza31205ze3z87_1170;} } } } } }  else 
{ /* Llib/module.scm 162 */
 obj_t BgL_fz00_1180;
{ /* Llib/module.scm 162 */
 obj_t BgL_arg1219z00_1183;
{ /* Llib/module.scm 162 */
 obj_t BgL_arg1424z00_1773;
BgL_arg1424z00_1773 = 
SYMBOL_TO_STRING(BgL_modz00_11); 
BgL_arg1219z00_1183 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1424z00_1773); } 
BgL_fz00_1180 = 
string_append(BgL_arg1219z00_1183, BGl_string1856z00zz__modulez00); } 
if(
fexists(
BSTRING_TO_STRING(BgL_fz00_1180)))
{ /* Llib/module.scm 164 */
 obj_t BgL_list1218z00_1182;
BgL_list1218z00_1182 = 
MAKE_YOUNG_PAIR(BgL_fz00_1180, BNIL); 
return BgL_list1218z00_1182;}  else 
{ /* Llib/module.scm 163 */
return BNIL;} } } } 

}



/* module-add-access-inner! */
obj_t BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00(obj_t BgL_modulez00_13, obj_t BgL_filesz00_14, obj_t BgL_abasez00_15)
{
{ /* Llib/module.scm 170 */
{ /* Llib/module.scm 171 */
 obj_t BgL_basez00_1185;
BgL_basez00_1185 = 
BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_abasez00_15, BGl_afilezd2tablezd2zz__modulez00); 
if(
CBOOL(BgL_basez00_1185))
{ /* Llib/module.scm 175 */
 obj_t BgL_cellz00_1186;
{ /* Llib/module.scm 175 */
 obj_t BgL_auxz00_2437;
{ /* Llib/module.scm 175 */
 obj_t BgL_pairz00_1776;
if(
PAIRP(BgL_basez00_1185))
{ /* Llib/module.scm 175 */
BgL_pairz00_1776 = BgL_basez00_1185; }  else 
{ 
 obj_t BgL_auxz00_2440;
BgL_auxz00_2440 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6657L), BGl_string1857z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_basez00_1185); 
FAILURE(BgL_auxz00_2440,BFALSE,BFALSE);} 
{ /* Llib/module.scm 175 */
 obj_t BgL_aux1776z00_2110;
BgL_aux1776z00_2110 = 
CDR(BgL_pairz00_1776); 
{ /* Llib/module.scm 175 */
 bool_t BgL_test1991z00_2445;
if(
PAIRP(BgL_aux1776z00_2110))
{ /* Llib/module.scm 175 */
BgL_test1991z00_2445 = ((bool_t)1)
; }  else 
{ /* Llib/module.scm 175 */
BgL_test1991z00_2445 = 
NULLP(BgL_aux1776z00_2110)
; } 
if(BgL_test1991z00_2445)
{ /* Llib/module.scm 175 */
BgL_auxz00_2437 = BgL_aux1776z00_2110
; }  else 
{ 
 obj_t BgL_auxz00_2449;
BgL_auxz00_2449 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6652L), BGl_string1857z00zz__modulez00, BGl_string1851z00zz__modulez00, BgL_aux1776z00_2110); 
FAILURE(BgL_auxz00_2449,BFALSE,BFALSE);} } } } 
BgL_cellz00_1186 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_modulez00_13, BgL_auxz00_2437); } 
if(
CBOOL(BgL_cellz00_1186))
{ /* Llib/module.scm 178 */
 bool_t BgL_test1994z00_2456;
{ /* Llib/module.scm 178 */
 obj_t BgL_auxz00_2457;
{ /* Llib/module.scm 178 */
 obj_t BgL_pairz00_1777;
if(
PAIRP(BgL_cellz00_1186))
{ /* Llib/module.scm 178 */
BgL_pairz00_1777 = BgL_cellz00_1186; }  else 
{ 
 obj_t BgL_auxz00_2460;
BgL_auxz00_2460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6768L), BGl_string1857z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_cellz00_1186); 
FAILURE(BgL_auxz00_2460,BFALSE,BFALSE);} 
BgL_auxz00_2457 = 
CDR(BgL_pairz00_1777); } 
BgL_test1994z00_2456 = 
BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_auxz00_2457, BgL_filesz00_14); } 
if(BgL_test1994z00_2456)
{ /* Llib/module.scm 178 */
return BFALSE;}  else 
{ /* Llib/module.scm 178 */
{ /* Llib/module.scm 181 */
 obj_t BgL_arg1223z00_1189;
{ /* Llib/module.scm 181 */
 obj_t BgL_pairz00_1778;
if(
PAIRP(BgL_cellz00_1186))
{ /* Llib/module.scm 181 */
BgL_pairz00_1778 = BgL_cellz00_1186; }  else 
{ 
 obj_t BgL_auxz00_2468;
BgL_auxz00_2468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6869L), BGl_string1857z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_cellz00_1186); 
FAILURE(BgL_auxz00_2468,BFALSE,BFALSE);} 
BgL_arg1223z00_1189 = 
CDR(BgL_pairz00_1778); } 
{ /* Llib/module.scm 179 */
 obj_t BgL_list1224z00_1190;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1225z00_1191;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1226z00_1192;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1227z00_1193;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1228z00_1194;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1229z00_1195;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1230z00_1196;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1231z00_1197;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1232z00_1198;
{ /* Llib/module.scm 179 */
 obj_t BgL_arg1233z00_1199;
BgL_arg1233z00_1199 = 
MAKE_YOUNG_PAIR(BGl_string1858z00zz__modulez00, BNIL); 
BgL_arg1232z00_1198 = 
MAKE_YOUNG_PAIR(BgL_abasez00_15, BgL_arg1233z00_1199); } 
BgL_arg1231z00_1197 = 
MAKE_YOUNG_PAIR(BGl_string1859z00zz__modulez00, BgL_arg1232z00_1198); } 
BgL_arg1230z00_1196 = 
MAKE_YOUNG_PAIR(BgL_filesz00_14, BgL_arg1231z00_1197); } 
BgL_arg1229z00_1195 = 
MAKE_YOUNG_PAIR(BGl_string1860z00zz__modulez00, BgL_arg1230z00_1196); } 
BgL_arg1228z00_1194 = 
MAKE_YOUNG_PAIR(BgL_arg1223z00_1189, BgL_arg1229z00_1195); } 
BgL_arg1227z00_1193 = 
MAKE_YOUNG_PAIR(BGl_string1861z00zz__modulez00, BgL_arg1228z00_1194); } 
BgL_arg1226z00_1192 = 
MAKE_YOUNG_PAIR(BgL_modulez00_13, BgL_arg1227z00_1193); } 
BgL_arg1225z00_1191 = 
MAKE_YOUNG_PAIR(BGl_string1862z00zz__modulez00, BgL_arg1226z00_1192); } 
BgL_list1224z00_1190 = 
MAKE_YOUNG_PAIR(BGl_string1863z00zz__modulez00, BgL_arg1225z00_1191); } 
BGl_warningz00zz__errorz00(BgL_list1224z00_1190); } } 
return BGl_list1864z00zz__modulez00;} }  else 
{ /* Llib/module.scm 177 */
 obj_t BgL_arg1236z00_1201;
{ /* Llib/module.scm 177 */
 obj_t BgL_arg1238z00_1202; obj_t BgL_arg1239z00_1203;
BgL_arg1238z00_1202 = 
MAKE_YOUNG_PAIR(BgL_modulez00_13, BgL_filesz00_14); 
{ /* Llib/module.scm 177 */
 obj_t BgL_pairz00_1779;
if(
PAIRP(BgL_basez00_1185))
{ /* Llib/module.scm 177 */
BgL_pairz00_1779 = BgL_basez00_1185; }  else 
{ 
 obj_t BgL_auxz00_2487;
BgL_auxz00_2487 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6736L), BGl_string1857z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_basez00_1185); 
FAILURE(BgL_auxz00_2487,BFALSE,BFALSE);} 
BgL_arg1239z00_1203 = 
CDR(BgL_pairz00_1779); } 
BgL_arg1236z00_1201 = 
MAKE_YOUNG_PAIR(BgL_arg1238z00_1202, BgL_arg1239z00_1203); } 
{ /* Llib/module.scm 177 */
 obj_t BgL_pairz00_1780;
if(
PAIRP(BgL_basez00_1185))
{ /* Llib/module.scm 177 */
BgL_pairz00_1780 = BgL_basez00_1185; }  else 
{ 
 obj_t BgL_auxz00_2495;
BgL_auxz00_2495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(6700L), BGl_string1857z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_basez00_1185); 
FAILURE(BgL_auxz00_2495,BFALSE,BFALSE);} 
return 
SET_CDR(BgL_pairz00_1780, BgL_arg1236z00_1201);} } }  else 
{ /* Llib/module.scm 174 */
 obj_t BgL_arg1244z00_1205;
{ /* Llib/module.scm 174 */
 obj_t BgL_arg1248z00_1206;
{ /* Llib/module.scm 174 */
 obj_t BgL_arg1249z00_1207;
BgL_arg1249z00_1207 = 
MAKE_YOUNG_PAIR(BgL_modulez00_13, BgL_filesz00_14); 
{ /* Llib/module.scm 174 */
 obj_t BgL_list1250z00_1208;
BgL_list1250z00_1208 = 
MAKE_YOUNG_PAIR(BgL_arg1249z00_1207, BNIL); 
BgL_arg1248z00_1206 = BgL_list1250z00_1208; } } 
BgL_arg1244z00_1205 = 
MAKE_YOUNG_PAIR(BgL_abasez00_15, BgL_arg1248z00_1206); } 
return ( 
BGl_afilezd2tablezd2zz__modulez00 = 
MAKE_YOUNG_PAIR(BgL_arg1244z00_1205, BGl_afilezd2tablezd2zz__modulez00), BUNSPEC) ;} } } 

}



/* module-add-access! */
BGL_EXPORTED_DEF obj_t BGl_modulezd2addzd2accessz12z12zz__modulez00(obj_t BgL_modulez00_16, obj_t BgL_filesz00_17, obj_t BgL_abasez00_18)
{
{ /* Llib/module.scm 189 */
{ 
 obj_t BgL_fz00_1225; obj_t BgL_basez00_1226;
{ /* Llib/module.scm 197 */
 obj_t BgL_top2000z00_2505;
BgL_top2000z00_2505 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
BGL_EXITD_PUSH_PROTECT(BgL_top2000z00_2505, BGl_moduleszd2mutexzd2zz__modulez00); BUNSPEC; 
{ /* Llib/module.scm 197 */
 obj_t BgL_tmp1999z00_2504;
{ /* Llib/module.scm 199 */
 obj_t BgL_arg1252z00_1210;
{ /* Llib/module.scm 199 */
 obj_t BgL_head1080z00_1213;
BgL_head1080z00_1213 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1078z00_1215; obj_t BgL_tail1081z00_1216;
BgL_l1078z00_1215 = BgL_filesz00_17; 
BgL_tail1081z00_1216 = BgL_head1080z00_1213; 
BgL_zc3z04anonymousza31254ze3z87_1217:
if(
PAIRP(BgL_l1078z00_1215))
{ /* Llib/module.scm 199 */
 obj_t BgL_newtail1082z00_1219;
{ /* Llib/module.scm 199 */
 obj_t BgL_tmpz00_2512;
BgL_fz00_1225 = 
CAR(BgL_l1078z00_1215); 
BgL_basez00_1226 = BgL_abasez00_18; 
{ /* Llib/module.scm 192 */
 bool_t BgL_test2002z00_2513;
{ /* Llib/module.scm 192 */
 bool_t BgL_test2003z00_2514;
{ /* Llib/module.scm 192 */
 long BgL_tmpz00_2515;
{ /* Llib/module.scm 192 */
 obj_t BgL_stringz00_1782;
if(
STRINGP(BgL_fz00_1225))
{ /* Llib/module.scm 192 */
BgL_stringz00_1782 = BgL_fz00_1225; }  else 
{ 
 obj_t BgL_auxz00_2518;
BgL_auxz00_2518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7334L), BGl_string1871z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_fz00_1225); 
FAILURE(BgL_auxz00_2518,BFALSE,BFALSE);} 
BgL_tmpz00_2515 = 
STRING_LENGTH(BgL_stringz00_1782); } 
BgL_test2003z00_2514 = 
(BgL_tmpz00_2515>0L); } 
if(BgL_test2003z00_2514)
{ /* Llib/module.scm 193 */
 unsigned char BgL_char2z00_1786;
BgL_char2z00_1786 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/module.scm 193 */
 unsigned char BgL_tmpz00_2525;
{ /* Llib/module.scm 193 */
 obj_t BgL_stringz00_1784;
if(
STRINGP(BgL_fz00_1225))
{ /* Llib/module.scm 193 */
BgL_stringz00_1784 = BgL_fz00_1225; }  else 
{ 
 obj_t BgL_auxz00_2528;
BgL_auxz00_2528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7383L), BGl_string1871z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_fz00_1225); 
FAILURE(BgL_auxz00_2528,BFALSE,BFALSE);} 
{ /* Llib/module.scm 193 */
 long BgL_l1741z00_2074;
BgL_l1741z00_2074 = 
STRING_LENGTH(BgL_stringz00_1784); 
if(
BOUND_CHECK(0L, BgL_l1741z00_2074))
{ /* Llib/module.scm 193 */
BgL_tmpz00_2525 = 
STRING_REF(BgL_stringz00_1784, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_2536;
BgL_auxz00_2536 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7371L), BGl_string1872z00zz__modulez00, BgL_stringz00_1784, 
(int)(BgL_l1741z00_2074), 
(int)(0L)); 
FAILURE(BgL_auxz00_2536,BFALSE,BFALSE);} } } 
BgL_test2002z00_2513 = 
(BgL_tmpz00_2525==BgL_char2z00_1786); } }  else 
{ /* Llib/module.scm 192 */
BgL_test2002z00_2513 = ((bool_t)0)
; } } 
if(BgL_test2002z00_2513)
{ /* Llib/module.scm 192 */
BgL_tmpz00_2512 = BgL_fz00_1225
; }  else 
{ /* Llib/module.scm 195 */
 obj_t BgL_arg1307z00_1233;
{ /* Llib/module.scm 195 */
 obj_t BgL_auxz00_2543;
if(
STRINGP(BgL_fz00_1225))
{ /* Llib/module.scm 195 */
BgL_auxz00_2543 = BgL_fz00_1225
; }  else 
{ 
 obj_t BgL_auxz00_2546;
BgL_auxz00_2546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7466L), BGl_string1871z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_fz00_1225); 
FAILURE(BgL_auxz00_2546,BFALSE,BFALSE);} 
BgL_arg1307z00_1233 = 
BGl_makezd2filezd2namez00zz__osz00(BgL_basez00_1226, BgL_auxz00_2543); } 
BgL_tmpz00_2512 = 
BGl_filezd2namezd2canonicaliza7eza7zz__osz00(BgL_arg1307z00_1233); } } 
BgL_newtail1082z00_1219 = 
MAKE_YOUNG_PAIR(BgL_tmpz00_2512, BNIL); } 
SET_CDR(BgL_tail1081z00_1216, BgL_newtail1082z00_1219); 
{ 
 obj_t BgL_tail1081z00_2557; obj_t BgL_l1078z00_2555;
BgL_l1078z00_2555 = 
CDR(BgL_l1078z00_1215); 
BgL_tail1081z00_2557 = BgL_newtail1082z00_1219; 
BgL_tail1081z00_1216 = BgL_tail1081z00_2557; 
BgL_l1078z00_1215 = BgL_l1078z00_2555; 
goto BgL_zc3z04anonymousza31254ze3z87_1217;} }  else 
{ /* Llib/module.scm 199 */
if(
NULLP(BgL_l1078z00_1215))
{ /* Llib/module.scm 199 */
BgL_arg1252z00_1210 = 
CDR(BgL_head1080z00_1213); }  else 
{ /* Llib/module.scm 199 */
BgL_arg1252z00_1210 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1873z00zz__modulez00, BGl_string1874z00zz__modulez00, BgL_l1078z00_1215, BGl_string1829z00zz__modulez00, 
BINT(7547L)); } } } } 
BgL_tmp1999z00_2504 = 
BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00(BgL_modulez00_16, BgL_arg1252z00_1210, BgL_abasez00_18); } 
BGL_EXITD_POP_PROTECT(BgL_top2000z00_2505); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
return BgL_tmp1999z00_2504;} } } } 

}



/* &module-add-access! */
obj_t BGl_z62modulezd2addzd2accessz12z70zz__modulez00(obj_t BgL_envz00_2059, obj_t BgL_modulez00_2060, obj_t BgL_filesz00_2061, obj_t BgL_abasez00_2062)
{
{ /* Llib/module.scm 189 */
{ /* Llib/module.scm 192 */
 obj_t BgL_auxz00_2580; obj_t BgL_auxz00_2573; obj_t BgL_auxz00_2566;
if(
STRINGP(BgL_abasez00_2062))
{ /* Llib/module.scm 192 */
BgL_auxz00_2580 = BgL_abasez00_2062
; }  else 
{ 
 obj_t BgL_auxz00_2583;
BgL_auxz00_2583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7320L), BGl_string1875z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_abasez00_2062); 
FAILURE(BgL_auxz00_2583,BFALSE,BFALSE);} 
if(
PAIRP(BgL_filesz00_2061))
{ /* Llib/module.scm 192 */
BgL_auxz00_2573 = BgL_filesz00_2061
; }  else 
{ 
 obj_t BgL_auxz00_2576;
BgL_auxz00_2576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7320L), BGl_string1875z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_filesz00_2061); 
FAILURE(BgL_auxz00_2576,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_modulez00_2060))
{ /* Llib/module.scm 192 */
BgL_auxz00_2566 = BgL_modulez00_2060
; }  else 
{ 
 obj_t BgL_auxz00_2569;
BgL_auxz00_2569 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7320L), BGl_string1875z00zz__modulez00, BGl_string1850z00zz__modulez00, BgL_modulez00_2060); 
FAILURE(BgL_auxz00_2569,BFALSE,BFALSE);} 
return 
BGl_modulezd2addzd2accessz12z12zz__modulez00(BgL_auxz00_2566, BgL_auxz00_2573, BgL_auxz00_2580);} } 

}



/* module-read-access-file */
obj_t BGl_modulezd2readzd2accesszd2filezd2zz__modulez00(obj_t BgL_portz00_19)
{
{ /* Llib/module.scm 205 */
{ /* Llib/module.scm 206 */
 obj_t BgL_hook1087z00_1238;
BgL_hook1087z00_1238 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
{ /* Llib/module.scm 212 */
 obj_t BgL_g1089z00_1239;
{ /* Llib/module.scm 212 */

{ /* Llib/module.scm 212 */
 obj_t BgL_iportz00_1274;
if(
INPUT_PORTP(BgL_portz00_19))
{ /* Llib/module.scm 212 */
BgL_iportz00_1274 = BgL_portz00_19; }  else 
{ 
 obj_t BgL_auxz00_2591;
BgL_auxz00_2591 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8048L), BGl_string1876z00zz__modulez00, BGl_string1877z00zz__modulez00, BgL_portz00_19); 
FAILURE(BgL_auxz00_2591,BFALSE,BFALSE);} 
{ /* Llib/module.scm 212 */

BgL_g1089z00_1239 = 
BGl_readz00zz__readerz00(BgL_iportz00_1274, BFALSE); } } } 
{ 
 obj_t BgL_l1084z00_1241; obj_t BgL_h1085z00_1242;
BgL_l1084z00_1241 = BgL_g1089z00_1239; 
BgL_h1085z00_1242 = BgL_hook1087z00_1238; 
BgL_zc3z04anonymousza31310ze3z87_1243:
if(
NULLP(BgL_l1084z00_1241))
{ /* Llib/module.scm 212 */
return 
CDR(BgL_hook1087z00_1238);}  else 
{ /* Llib/module.scm 212 */
 bool_t BgL_test2014z00_2599;
{ /* Llib/module.scm 207 */
 obj_t BgL_xz00_1264;
{ /* Llib/module.scm 207 */
 obj_t BgL_pairz00_1792;
if(
PAIRP(BgL_l1084z00_1241))
{ /* Llib/module.scm 207 */
BgL_pairz00_1792 = BgL_l1084z00_1241; }  else 
{ 
 obj_t BgL_auxz00_2602;
BgL_auxz00_2602 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(7894L), BGl_string1878z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1084z00_1241); 
FAILURE(BgL_auxz00_2602,BFALSE,BFALSE);} 
BgL_xz00_1264 = 
CAR(BgL_pairz00_1792); } 
{ /* Llib/module.scm 207 */
 bool_t BgL_test2016z00_2607;
if(
PAIRP(BgL_xz00_1264))
{ /* Llib/module.scm 207 */
 bool_t BgL_test2018z00_2610;
{ /* Llib/module.scm 207 */
 obj_t BgL_tmpz00_2611;
BgL_tmpz00_2611 = 
CAR(BgL_xz00_1264); 
BgL_test2018z00_2610 = 
SYMBOLP(BgL_tmpz00_2611); } 
if(BgL_test2018z00_2610)
{ /* Llib/module.scm 207 */
BgL_test2016z00_2607 = 
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(
CDR(BgL_xz00_1264))
; }  else 
{ /* Llib/module.scm 207 */
BgL_test2016z00_2607 = ((bool_t)0)
; } }  else 
{ /* Llib/module.scm 207 */
BgL_test2016z00_2607 = ((bool_t)0)
; } 
if(BgL_test2016z00_2607)
{ /* Llib/module.scm 207 */
BgL_test2014z00_2599 = ((bool_t)1)
; }  else 
{ /* Llib/module.scm 207 */
{ /* Llib/module.scm 210 */
 obj_t BgL_list1328z00_1266;
{ /* Llib/module.scm 210 */
 obj_t BgL_arg1329z00_1267;
{ /* Llib/module.scm 210 */
 obj_t BgL_arg1331z00_1268;
BgL_arg1331z00_1268 = 
MAKE_YOUNG_PAIR(BgL_xz00_1264, BNIL); 
BgL_arg1329z00_1267 = 
MAKE_YOUNG_PAIR(BGl_string1879z00zz__modulez00, BgL_arg1331z00_1268); } 
BgL_list1328z00_1266 = 
MAKE_YOUNG_PAIR(BGl_string1876z00zz__modulez00, BgL_arg1329z00_1267); } 
BGl_warningz00zz__errorz00(BgL_list1328z00_1266); } 
BgL_test2014z00_2599 = ((bool_t)0); } } } 
if(BgL_test2014z00_2599)
{ /* Llib/module.scm 212 */
 obj_t BgL_nh1086z00_1260;
{ /* Llib/module.scm 212 */
 obj_t BgL_arg1326z00_1262;
{ /* Llib/module.scm 212 */
 obj_t BgL_pairz00_1795;
if(
PAIRP(BgL_l1084z00_1241))
{ /* Llib/module.scm 212 */
BgL_pairz00_1795 = BgL_l1084z00_1241; }  else 
{ 
 obj_t BgL_auxz00_2622;
BgL_auxz00_2622 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8048L), BGl_string1878z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1084z00_1241); 
FAILURE(BgL_auxz00_2622,BFALSE,BFALSE);} 
BgL_arg1326z00_1262 = 
CAR(BgL_pairz00_1795); } 
BgL_nh1086z00_1260 = 
MAKE_YOUNG_PAIR(BgL_arg1326z00_1262, BNIL); } 
SET_CDR(BgL_h1085z00_1242, BgL_nh1086z00_1260); 
{ /* Llib/module.scm 212 */
 obj_t BgL_arg1325z00_1261;
{ /* Llib/module.scm 212 */
 obj_t BgL_pairz00_1797;
if(
PAIRP(BgL_l1084z00_1241))
{ /* Llib/module.scm 212 */
BgL_pairz00_1797 = BgL_l1084z00_1241; }  else 
{ 
 obj_t BgL_auxz00_2631;
BgL_auxz00_2631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8048L), BGl_string1878z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1084z00_1241); 
FAILURE(BgL_auxz00_2631,BFALSE,BFALSE);} 
BgL_arg1325z00_1261 = 
CDR(BgL_pairz00_1797); } 
{ 
 obj_t BgL_h1085z00_2637; obj_t BgL_l1084z00_2636;
BgL_l1084z00_2636 = BgL_arg1325z00_1261; 
BgL_h1085z00_2637 = BgL_nh1086z00_1260; 
BgL_h1085z00_1242 = BgL_h1085z00_2637; 
BgL_l1084z00_1241 = BgL_l1084z00_2636; 
goto BgL_zc3z04anonymousza31310ze3z87_1243;} } }  else 
{ /* Llib/module.scm 212 */
 obj_t BgL_arg1327z00_1263;
{ /* Llib/module.scm 212 */
 obj_t BgL_pairz00_1798;
if(
PAIRP(BgL_l1084z00_1241))
{ /* Llib/module.scm 212 */
BgL_pairz00_1798 = BgL_l1084z00_1241; }  else 
{ 
 obj_t BgL_auxz00_2640;
BgL_auxz00_2640 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8048L), BGl_string1878z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1084z00_1241); 
FAILURE(BgL_auxz00_2640,BFALSE,BFALSE);} 
BgL_arg1327z00_1263 = 
CDR(BgL_pairz00_1798); } 
{ 
 obj_t BgL_l1084z00_2645;
BgL_l1084z00_2645 = BgL_arg1327z00_1263; 
BgL_l1084z00_1241 = BgL_l1084z00_2645; 
goto BgL_zc3z04anonymousza31310ze3z87_1243;} } } } } } } 

}



/* module-load-access-file */
BGL_EXPORTED_DEF obj_t BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(obj_t BgL_pathz00_20)
{
{ /* Llib/module.scm 223 */
{ 
 obj_t BgL_filez00_1303; obj_t BgL_dirz00_1304; obj_t BgL_abasez00_1305;
{ /* Llib/module.scm 246 */
 obj_t BgL_pathz00_1278;
BgL_pathz00_1278 = 
BGl_filezd2namezd2canonicaliza7eza7zz__osz00(BgL_pathz00_20); 
{ /* Llib/module.scm 247 */
 obj_t BgL_top2023z00_2648;
BgL_top2023z00_2648 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
BGL_EXITD_PUSH_PROTECT(BgL_top2023z00_2648, BGl_moduleszd2mutexzd2zz__modulez00); BUNSPEC; 
{ /* Llib/module.scm 247 */
 obj_t BgL_tmp2022z00_2647;
{ /* Llib/module.scm 248 */
 obj_t BgL__ortest_1039z00_1279;
{ /* Llib/module.scm 248 */
 obj_t BgL_auxz00_2652;
{ /* Llib/module.scm 248 */
 obj_t BgL_aux1808z00_2142;
BgL_aux1808z00_2142 = BGl_za2afileszd2tableza2zd2zz__modulez00; 
if(
STRUCTP(BgL_aux1808z00_2142))
{ /* Llib/module.scm 248 */
BgL_auxz00_2652 = BgL_aux1808z00_2142
; }  else 
{ 
 obj_t BgL_auxz00_2655;
BgL_auxz00_2655 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9338L), BGl_string1880z00zz__modulez00, BGl_string1881z00zz__modulez00, BgL_aux1808z00_2142); 
FAILURE(BgL_auxz00_2655,BFALSE,BFALSE);} } 
BgL__ortest_1039z00_1279 = 
BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_2652, BgL_pathz00_1278); } 
if(
CBOOL(BgL__ortest_1039z00_1279))
{ /* Llib/module.scm 248 */
BgL_tmp2022z00_2647 = BgL__ortest_1039z00_1279; }  else 
{ /* Llib/module.scm 248 */
if(
bgl_directoryp(
BSTRING_TO_STRING(BgL_pathz00_1278)))
{ 
 obj_t BgL_dz00_1282;
BgL_dz00_1282 = BgL_pathz00_1278; 
BgL_zc3z04anonymousza31335ze3z87_1283:
{ /* Llib/module.scm 252 */
 obj_t BgL_filez00_1284;
BgL_filez00_1284 = 
BGl_makezd2filezd2namez00zz__osz00(BgL_dz00_1282, BGl_string1882z00zz__modulez00); 
if(
fexists(
BSTRING_TO_STRING(BgL_filez00_1284)))
{ /* Llib/module.scm 253 */
BgL_filez00_1303 = BgL_filez00_1284; 
BgL_dirz00_1304 = BgL_dz00_1282; 
BgL_abasez00_1305 = BgL_pathz00_1278; 
BgL_zc3z04anonymousza31349ze3z87_1306:
{ /* Llib/module.scm 234 */
 obj_t BgL_zc3z04anonymousza31351ze3z87_2063;
BgL_zc3z04anonymousza31351ze3z87_2063 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31351ze3ze5zz__modulez00, 
(int)(1L), 
(int)(4L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31351ze3z87_2063, 
(int)(0L), BgL_pathz00_20); 
PROCEDURE_SET(BgL_zc3z04anonymousza31351ze3z87_2063, 
(int)(1L), BgL_filez00_1303); 
PROCEDURE_SET(BgL_zc3z04anonymousza31351ze3z87_2063, 
(int)(2L), BgL_dirz00_1304); 
PROCEDURE_SET(BgL_zc3z04anonymousza31351ze3z87_2063, 
(int)(3L), BgL_abasez00_1305); 
BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(BgL_filez00_1303, BgL_zc3z04anonymousza31351ze3z87_2063); } 
BgL_tmp2022z00_2647 = BgL_filez00_1303; }  else 
{ /* Llib/module.scm 255 */
 obj_t BgL_parentz00_1286;
BgL_parentz00_1286 = 
BGl_dirnamez00zz__osz00(BgL_dz00_1282); 
{ /* Llib/module.scm 256 */
 bool_t BgL_test2028z00_2682;
{ /* Llib/module.scm 256 */
 long BgL_l1z00_1836;
BgL_l1z00_1836 = 
STRING_LENGTH(BgL_parentz00_1286); 
if(
(BgL_l1z00_1836==
STRING_LENGTH(BgL_dz00_1282)))
{ /* Llib/module.scm 256 */
 int BgL_arg1417z00_1839;
{ /* Llib/module.scm 256 */
 char * BgL_auxz00_2689; char * BgL_tmpz00_2687;
BgL_auxz00_2689 = 
BSTRING_TO_STRING(BgL_dz00_1282); 
BgL_tmpz00_2687 = 
BSTRING_TO_STRING(BgL_parentz00_1286); 
BgL_arg1417z00_1839 = 
memcmp(BgL_tmpz00_2687, BgL_auxz00_2689, BgL_l1z00_1836); } 
BgL_test2028z00_2682 = 
(
(long)(BgL_arg1417z00_1839)==0L); }  else 
{ /* Llib/module.scm 256 */
BgL_test2028z00_2682 = ((bool_t)0)
; } } 
if(BgL_test2028z00_2682)
{ /* Llib/module.scm 256 */
BgL_tmp2022z00_2647 = BFALSE; }  else 
{ 
 obj_t BgL_dz00_2694;
BgL_dz00_2694 = BgL_parentz00_1286; 
BgL_dz00_1282 = BgL_dz00_2694; 
goto BgL_zc3z04anonymousza31335ze3z87_1283;} } } } }  else 
{ /* Llib/module.scm 250 */
if(
fexists(
BSTRING_TO_STRING(BgL_pathz00_1278)))
{ /* Llib/module.scm 259 */
 obj_t BgL_dirz00_1290;
BgL_dirz00_1290 = 
BGl_dirnamez00zz__osz00(BgL_pathz00_1278); 
{ 
 obj_t BgL_abasez00_2701; obj_t BgL_dirz00_2700; obj_t BgL_filez00_2699;
BgL_filez00_2699 = BgL_pathz00_1278; 
BgL_dirz00_2700 = BgL_dirz00_1290; 
BgL_abasez00_2701 = BgL_dirz00_1290; 
BgL_abasez00_1305 = BgL_abasez00_2701; 
BgL_dirz00_1304 = BgL_dirz00_2700; 
BgL_filez00_1303 = BgL_filez00_2699; 
goto BgL_zc3z04anonymousza31349ze3z87_1306;} }  else 
{ /* Llib/module.scm 258 */
BgL_tmp2022z00_2647 = BFALSE; } } } } 
BGL_EXITD_POP_PROTECT(BgL_top2023z00_2648); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_moduleszd2mutexzd2zz__modulez00); 
return BgL_tmp2022z00_2647;} } } } } 

}



/* &module-load-access-file */
obj_t BGl_z62modulezd2loadzd2accesszd2filezb0zz__modulez00(obj_t BgL_envz00_2064, obj_t BgL_pathz00_2065)
{
{ /* Llib/module.scm 223 */
{ /* Llib/module.scm 227 */
 obj_t BgL_auxz00_2704;
if(
STRINGP(BgL_pathz00_2065))
{ /* Llib/module.scm 227 */
BgL_auxz00_2704 = BgL_pathz00_2065
; }  else 
{ 
 obj_t BgL_auxz00_2707;
BgL_auxz00_2707 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8648L), BGl_string1883z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_pathz00_2065); 
FAILURE(BgL_auxz00_2707,BFALSE,BFALSE);} 
return 
BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(BgL_auxz00_2704);} } 

}



/* &<@anonymous:1351> */
obj_t BGl_z62zc3z04anonymousza31351ze3ze5zz__modulez00(obj_t BgL_envz00_2066, obj_t BgL_portz00_2071)
{
{ /* Llib/module.scm 233 */
{ /* Llib/module.scm 234 */
 obj_t BgL_pathz00_2067; obj_t BgL_filez00_2068; obj_t BgL_dirz00_2069; obj_t BgL_abasez00_2070;
BgL_pathz00_2067 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2066, 
(int)(0L))); 
BgL_filez00_2068 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2066, 
(int)(1L))); 
BgL_dirz00_2069 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2066, 
(int)(2L))); 
BgL_abasez00_2070 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_2066, 
(int)(3L))); 
{ 
 obj_t BgL_fz00_2183; obj_t BgL_abasez00_2184;
{ /* Llib/module.scm 234 */
 obj_t BgL_auxz00_2724;
{ /* Llib/module.scm 234 */
 obj_t BgL_aux1814z00_2190;
BgL_aux1814z00_2190 = BGl_za2afileszd2tableza2zd2zz__modulez00; 
if(
STRUCTP(BgL_aux1814z00_2190))
{ /* Llib/module.scm 234 */
BgL_auxz00_2724 = BgL_aux1814z00_2190
; }  else 
{ 
 obj_t BgL_auxz00_2727;
BgL_auxz00_2727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8915L), BGl_string1886z00zz__modulez00, BGl_string1881z00zz__modulez00, BgL_aux1814z00_2190); 
FAILURE(BgL_auxz00_2727,BFALSE,BFALSE);} } 
BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_2724, BgL_pathz00_2067, BgL_filez00_2068); } 
{ /* Llib/module.scm 235 */
 obj_t BgL_g1095z00_2191;
BgL_g1095z00_2191 = 
BGl_modulezd2readzd2accesszd2filezd2zz__modulez00(BgL_portz00_2071); 
{ 
 obj_t BgL_l1093z00_2193;
BgL_l1093z00_2193 = BgL_g1095z00_2191; 
BgL_zc3z04anonymousza31352ze3z87_2192:
if(
PAIRP(BgL_l1093z00_2193))
{ /* Llib/module.scm 243 */
{ /* Llib/module.scm 236 */
 obj_t BgL_accessz00_2194;
BgL_accessz00_2194 = 
CAR(BgL_l1093z00_2193); 
{ /* Llib/module.scm 236 */
 obj_t BgL_infoz00_2195;
{ /* Llib/module.scm 236 */
 bool_t BgL_test2034z00_2736;
{ /* Llib/module.scm 236 */
 long BgL_l1z00_2196;
BgL_l1z00_2196 = 
STRING_LENGTH(BgL_dirz00_2069); 
if(
(BgL_l1z00_2196==1L))
{ /* Llib/module.scm 236 */
 int BgL_arg1417z00_2197;
{ /* Llib/module.scm 236 */
 char * BgL_auxz00_2742; char * BgL_tmpz00_2740;
BgL_auxz00_2742 = 
BSTRING_TO_STRING(BGl_string1846z00zz__modulez00); 
BgL_tmpz00_2740 = 
BSTRING_TO_STRING(BgL_dirz00_2069); 
BgL_arg1417z00_2197 = 
memcmp(BgL_tmpz00_2740, BgL_auxz00_2742, BgL_l1z00_2196); } 
BgL_test2034z00_2736 = 
(
(long)(BgL_arg1417z00_2197)==0L); }  else 
{ /* Llib/module.scm 236 */
BgL_test2034z00_2736 = ((bool_t)0)
; } } 
if(BgL_test2034z00_2736)
{ /* Llib/module.scm 237 */
 obj_t BgL_pairz00_2198;
if(
PAIRP(BgL_accessz00_2194))
{ /* Llib/module.scm 237 */
BgL_pairz00_2198 = BgL_accessz00_2194; }  else 
{ 
 obj_t BgL_auxz00_2749;
BgL_auxz00_2749 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9023L), BGl_string1887z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_accessz00_2194); 
FAILURE(BgL_auxz00_2749,BFALSE,BFALSE);} 
BgL_infoz00_2195 = 
CDR(BgL_pairz00_2198); }  else 
{ /* Llib/module.scm 238 */
 obj_t BgL_l01092z00_2199;
{ /* Llib/module.scm 240 */
 obj_t BgL_pairz00_2200;
if(
PAIRP(BgL_accessz00_2194))
{ /* Llib/module.scm 240 */
BgL_pairz00_2200 = BgL_accessz00_2194; }  else 
{ 
 obj_t BgL_auxz00_2756;
BgL_auxz00_2756 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9100L), BGl_string1887z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_accessz00_2194); 
FAILURE(BgL_auxz00_2756,BFALSE,BFALSE);} 
BgL_l01092z00_2199 = 
CDR(BgL_pairz00_2200); } 
{ 
 obj_t BgL_l1091z00_2202;
BgL_l1091z00_2202 = BgL_l01092z00_2199; 
BgL_zc3z04anonymousza31356ze3z87_2201:
if(
NULLP(BgL_l1091z00_2202))
{ /* Llib/module.scm 240 */
BgL_infoz00_2195 = BgL_l01092z00_2199; }  else 
{ /* Llib/module.scm 240 */
{ /* Llib/module.scm 239 */
 obj_t BgL_arg1358z00_2203;
{ /* Llib/module.scm 239 */
 obj_t BgL_fz00_2204;
{ /* Llib/module.scm 239 */
 obj_t BgL_pairz00_2205;
if(
PAIRP(BgL_l1091z00_2202))
{ /* Llib/module.scm 239 */
BgL_pairz00_2205 = BgL_l1091z00_2202; }  else 
{ 
 obj_t BgL_auxz00_2765;
BgL_auxz00_2765 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9063L), BGl_string1888z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1091z00_2202); 
FAILURE(BgL_auxz00_2765,BFALSE,BFALSE);} 
BgL_fz00_2204 = 
CAR(BgL_pairz00_2205); } 
BgL_fz00_2183 = BgL_fz00_2204; 
BgL_abasez00_2184 = BgL_dirz00_2069; 
if(
STRINGP(BgL_fz00_2183))
{ /* Llib/module.scm 228 */
 bool_t BgL_test2041z00_2772;
{ /* Llib/module.scm 228 */
 bool_t BgL_test2042z00_2773;
{ /* Llib/module.scm 228 */
 long BgL_l1z00_2185;
BgL_l1z00_2185 = 
STRING_LENGTH(BgL_fz00_2183); 
if(
(BgL_l1z00_2185==0L))
{ /* Llib/module.scm 228 */
 int BgL_arg1417z00_2186;
{ /* Llib/module.scm 228 */
 char * BgL_auxz00_2779; char * BgL_tmpz00_2777;
BgL_auxz00_2779 = 
BSTRING_TO_STRING(BGl_string1884z00zz__modulez00); 
BgL_tmpz00_2777 = 
BSTRING_TO_STRING(BgL_fz00_2183); 
BgL_arg1417z00_2186 = 
memcmp(BgL_tmpz00_2777, BgL_auxz00_2779, BgL_l1z00_2185); } 
BgL_test2042z00_2773 = 
(
(long)(BgL_arg1417z00_2186)==0L); }  else 
{ /* Llib/module.scm 228 */
BgL_test2042z00_2773 = ((bool_t)0)
; } } 
if(BgL_test2042z00_2773)
{ /* Llib/module.scm 228 */
BgL_test2041z00_2772 = ((bool_t)1)
; }  else 
{ /* Llib/module.scm 228 */
 unsigned char BgL_char2z00_2187;
BgL_char2z00_2187 = 
(unsigned char)(FILE_SEPARATOR); 
{ /* Llib/module.scm 228 */
 unsigned char BgL_tmpz00_2785;
{ /* Llib/module.scm 228 */
 long BgL_l1745z00_2188;
BgL_l1745z00_2188 = 
STRING_LENGTH(BgL_fz00_2183); 
if(
BOUND_CHECK(0L, BgL_l1745z00_2188))
{ /* Llib/module.scm 228 */
BgL_tmpz00_2785 = 
STRING_REF(BgL_fz00_2183, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_2790;
BgL_auxz00_2790 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8701L), BGl_string1872z00zz__modulez00, BgL_fz00_2183, 
(int)(BgL_l1745z00_2188), 
(int)(0L)); 
FAILURE(BgL_auxz00_2790,BFALSE,BFALSE);} } 
BgL_test2041z00_2772 = 
(BgL_tmpz00_2785==BgL_char2z00_2187); } } } 
if(BgL_test2041z00_2772)
{ /* Llib/module.scm 228 */
BgL_arg1358z00_2203 = BgL_fz00_2183; }  else 
{ /* Llib/module.scm 229 */
 obj_t BgL_arg1346z00_2189;
{ /* Llib/module.scm 229 */
 obj_t BgL_auxz00_2797;
if(
STRINGP(BgL_abasez00_2184))
{ /* Llib/module.scm 229 */
BgL_auxz00_2797 = BgL_abasez00_2184
; }  else 
{ 
 obj_t BgL_auxz00_2800;
BgL_auxz00_2800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(8788L), BGl_string1885z00zz__modulez00, BGl_string1848z00zz__modulez00, BgL_abasez00_2184); 
FAILURE(BgL_auxz00_2800,BFALSE,BFALSE);} 
BgL_arg1346z00_2189 = 
BGl_makezd2filezd2namez00zz__osz00(BgL_auxz00_2797, BgL_fz00_2183); } 
BgL_arg1358z00_2203 = 
BGl_filezd2namezd2canonicaliza7eza7zz__osz00(BgL_arg1346z00_2189); } }  else 
{ /* Llib/module.scm 227 */
BgL_arg1358z00_2203 = BgL_fz00_2183; } } 
{ /* Llib/module.scm 240 */
 obj_t BgL_pairz00_2206;
if(
PAIRP(BgL_l1091z00_2202))
{ /* Llib/module.scm 240 */
BgL_pairz00_2206 = BgL_l1091z00_2202; }  else 
{ 
 obj_t BgL_auxz00_2808;
BgL_auxz00_2808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9095L), BGl_string1888z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1091z00_2202); 
FAILURE(BgL_auxz00_2808,BFALSE,BFALSE);} 
SET_CAR(BgL_pairz00_2206, BgL_arg1358z00_2203); } } 
{ /* Llib/module.scm 240 */
 obj_t BgL_arg1359z00_2207;
{ /* Llib/module.scm 240 */
 obj_t BgL_pairz00_2208;
if(
PAIRP(BgL_l1091z00_2202))
{ /* Llib/module.scm 240 */
BgL_pairz00_2208 = BgL_l1091z00_2202; }  else 
{ 
 obj_t BgL_auxz00_2815;
BgL_auxz00_2815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9095L), BGl_string1888z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_l1091z00_2202); 
FAILURE(BgL_auxz00_2815,BFALSE,BFALSE);} 
BgL_arg1359z00_2207 = 
CDR(BgL_pairz00_2208); } 
{ 
 obj_t BgL_l1091z00_2820;
BgL_l1091z00_2820 = BgL_arg1359z00_2207; 
BgL_l1091z00_2202 = BgL_l1091z00_2820; 
goto BgL_zc3z04anonymousza31356ze3z87_2201;} } } } } } 
{ /* Llib/module.scm 242 */
 obj_t BgL_arg1354z00_2209;
{ /* Llib/module.scm 242 */
 obj_t BgL_pairz00_2210;
if(
PAIRP(BgL_accessz00_2194))
{ /* Llib/module.scm 242 */
BgL_pairz00_2210 = BgL_accessz00_2194; }  else 
{ 
 obj_t BgL_auxz00_2823;
BgL_auxz00_2823 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1829z00zz__modulez00, 
BINT(9160L), BGl_string1887z00zz__modulez00, BGl_string1853z00zz__modulez00, BgL_accessz00_2194); 
FAILURE(BgL_auxz00_2823,BFALSE,BFALSE);} 
BgL_arg1354z00_2209 = 
CAR(BgL_pairz00_2210); } 
BGl_modulezd2addzd2accesszd2innerz12zc0zz__modulez00(BgL_arg1354z00_2209, BgL_infoz00_2195, BgL_abasez00_2070); } } } 
{ 
 obj_t BgL_l1093z00_2829;
BgL_l1093z00_2829 = 
CDR(BgL_l1093z00_2193); 
BgL_l1093z00_2193 = BgL_l1093z00_2829; 
goto BgL_zc3z04anonymousza31352ze3z87_2192;} }  else 
{ /* Llib/module.scm 243 */
if(
NULLP(BgL_l1093z00_2193))
{ /* Llib/module.scm 243 */
return BTRUE;}  else 
{ /* Llib/module.scm 243 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string1889z00zz__modulez00, BGl_string1874z00zz__modulez00, BgL_l1093z00_2193, BGl_string1829z00zz__modulez00, 
BINT(8946L));} } } } } } } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__modulez00(void)
{
{ /* Llib/module.scm 17 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
BGl_modulezd2initializa7ationz75zz__configurez00(35034923L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
BGl_modulezd2initializa7ationz75zz__readerz00(220648206L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00)); 
return 
BGl_modulezd2initializa7ationz75zz__hashz00(482391669L, 
BSTRING_TO_STRING(BGl_string1890z00zz__modulez00));} 

}

#ifdef __cplusplus
}
#endif
