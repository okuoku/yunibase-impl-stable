/*===========================================================================*/
/*   (Llib/unicode.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/unicode.scm -indent -o objs/obj_s/Llib/unicode.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___UNICODE_TYPE_DEFINITIONS
#define BGL___UNICODE_TYPE_DEFINITIONS
#endif // BGL___UNICODE_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2downcasez00zz__unicodez00(obj_t);
static obj_t BGl_vector4814z00zz__unicodez00 = BUNSPEC;
static obj_t BGl__utf8zd2stringzf3z21zz__unicodez00(obj_t, obj_t);
static obj_t BGl_symbol4982z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(obj_t, long);
static obj_t BGl_symbol4984z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(obj_t, long);
static obj_t BGl_list4790z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2upcasez12z12zz__unicodez00(obj_t);
BGL_EXPORTED_DECL int BGl_ucs2zd2stringzd2lengthz00zz__unicodez00(obj_t);
static obj_t BGl_list4791z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4792z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62asciizd2stringzf3z43zz__unicodez00(obj_t, obj_t);
static obj_t BGl_list4793z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4794z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4795z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4796z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4797z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4798z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4799z00zz__unicodez00 = BUNSPEC;
extern obj_t c_ucs2_string_copy(obj_t);
BGL_EXPORTED_DECL obj_t BGl_subucs2zd2stringzd2urz00zz__unicodez00(obj_t, int, int);
static obj_t BGl_z62utf8zd2stringzd2appendza2zc0zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_isozd2latinzd2ze3utf8ze3zz__unicodez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2encodez00zz__unicodez00(obj_t, bool_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2appendz00zz__unicodez00(obj_t);
static obj_t BGl_z62isozd2latinzd2ze3utf8z81zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(obj_t);
static obj_t BGl__makezd2ucs2zd2stringz00zz__unicodez00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2ze3utf8zd2stringz31zz__unicodez00(obj_t);
static obj_t BGl_z62ucs2zd2stringzd2cizc3zd3zf3z81zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_utf8zd2stringzd2lengthz00zz__unicodez00(obj_t);
static obj_t BGl_z62ucs2zd2stringzd2cizc3zf3z52zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_8bitszd2invzd2latinzd215zd2zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_utf8zd2stringzd2leftzd2replacementzf3z21zz__unicodez00(obj_t, long, long);
BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2stringzd2refzd2urzd2zz__unicodez00(obj_t, int);
static obj_t BGl_makezd2tablezd2entryze70ze7zz__unicodez00(obj_t, long);
static obj_t BGl_z62ucs2zd2stringze3zd3zf3z73zz__unicodez00(obj_t, obj_t, obj_t);
static long BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(obj_t, long, obj_t);
static long BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2appendz00zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2lengthz62zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2fillz12z12zz__unicodez00(obj_t, ucs2_t);
extern obj_t ucs2_string_append(obj_t, obj_t);
extern bool_t ucs2_string_cige(obj_t, obj_t);
extern obj_t string_for_read(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__unicodez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62ucs2zd2stringzd2cizd3zf3z42zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzc3zf3ze2zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2charzd2siza7ezc5zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2setz12z70zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_vector4921z00zz__unicodez00 = BUNSPEC;
extern obj_t ucs2_string_to_utf8_string(obj_t);
extern bool_t ucs2_string_cigt(obj_t, obj_t);
static obj_t BGl_utf8zd2collapsez12zc0zz__unicodez00(obj_t, long, obj_t, obj_t);
static obj_t BGl_z62utf8zd2ze3isozd2latinz12z93zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2appendz62zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62subucs2zd2stringzb0zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2ze3ucs2zd2stringz31zz__unicodez00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzc3zd3zf3z31zz__unicodez00(obj_t, obj_t);
extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
static obj_t BGl_z62ucs2zd2stringzd2cize3zf3z72zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd3zf3zf2zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2stringzd2lengthz62zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z628bitszd2ze3utf8z53zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_z62utf8zd2ze38bitsz53zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2refzd2urzb0zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_inversezd2utf8zd2tablez00zz__unicodez00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_utf8zd2stringzf3z21zz__unicodez00(obj_t, bool_t);
static obj_t BGl_toplevelzd2initzd2zz__unicodez00(void);
extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
BGL_EXPORTED_DECL obj_t BGl_cp1252zd2ze3utf8z12z23zz__unicodez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3cp1252z12z23zz__unicodez00(obj_t);
static obj_t BGl_z62utf8zd2stringzd2appendz62zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3isozd2latinzd215z12z23zz__unicodez00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringze3zf3zc2zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3isozd2latinze3zz__unicodez00(obj_t);
extern obj_t utf8_string_to_ucs2_string(obj_t);
static obj_t BGl_z62utf8zd2ze3isozd2latinz81zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t, bool_t, long, long);
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_addzd2tablezd2entryze70ze7zz__unicodez00(obj_t, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2substringzd2zz__unicodez00(obj_t, int, int);
extern bool_t ucs2_strcmp(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2copyz62zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2setzd2urz12zc0zz__unicodez00(obj_t, int, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cizc3zd3zf3ze3zz__unicodez00(obj_t, obj_t);
extern unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__unicodez00(void);
static obj_t BGl_z62ucs2zd2stringzd2cize3zd3zf3za1zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__unicodez00(void);
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
extern long bgl_list_length(obj_t);
static obj_t BGl_z62utf8zd2stringzd2leftzd2replacementzf3z43zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62inversezd2utf8zd2tablez62zz__unicodez00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__unicodez00(void);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t);
extern ucs2_t ucs2_tolower(ucs2_t);
static obj_t BGl_gczd2rootszd2initz00zz__unicodez00(void);
static obj_t BGl_objectzd2initzd2zz__unicodez00(void);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2zz__unicodez00(obj_t);
static obj_t BGl_z62ucs2zd2stringzc3zf3z80zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl__utf8zd2stringzd2encodez00zz__unicodez00(obj_t, obj_t);
extern bool_t ucs2_string_ge(obj_t, obj_t);
extern obj_t c_subucs2_string(obj_t, int, int);
extern bool_t ucs2_strcicmp(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexze3zz__unicodez00(obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringze3zd3zf3z11zz__unicodez00(obj_t, obj_t);
extern bool_t ucs2_string_gt(obj_t, obj_t);
static obj_t BGl_z62stringzd2indexzd2ze3utf8zd2stringzd2indexz81zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2ze3utf8zd2stringz53zz__unicodez00(obj_t, obj_t);
extern obj_t bgl_reverse_bang(obj_t);
extern bool_t ucs2_string_cile(obj_t, obj_t);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd3zf3z90zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2downcasez12z12zz__unicodez00(obj_t);
static obj_t BGl__utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t, long, obj_t, obj_t);
static obj_t BGl_symbol4904z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_symbol4907z00zz__unicodez00 = BUNSPEC;
extern bool_t ucs2_string_cilt(obj_t, obj_t);
static obj_t BGl__utf8zd2substringzd2zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62cp1252zd2ze3utf8z12z41zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2ze3cp1252z12z41zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2upcasez12z70zz__unicodez00(obj_t, obj_t);
static obj_t BGl_cp1252zd2invzd2zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62utf8zd2stringzd2rightzd2replacementzf3z43zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2appendza2za2zz__unicodez00(obj_t);
BGL_EXPORTED_DECL long BGl_utf8zd2charzd2siza7eza7zz__unicodez00(unsigned char);
BGL_EXPORTED_DECL obj_t BGl_isozd2latinzd2ze3utf8z12zf1zz__unicodez00(obj_t);
static obj_t BGl_symbol4910z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62ucs2zd2stringze3zf3za0zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2substringzd2urz62zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t, obj_t);
static obj_t BGl_list4800z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62stringzd2minimalzd2charsetz62zz__unicodez00(obj_t, obj_t);
static obj_t BGl_list4801z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_symbol4916z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4802z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4803z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_tablezd2ze38bitsze70zd6zz__unicodez00(obj_t, obj_t, obj_t, int, obj_t, obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_subucs2zd2stringzd2zz__unicodez00(obj_t, int, int);
static obj_t BGl_list4804z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_makezd2ucs2zd2stringz00zz__unicodez00(int, ucs2_t);
static obj_t BGl_symbol4919z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4805z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4806z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4807z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4808z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4809z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(obj_t, obj_t, long, obj_t);
static obj_t BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(obj_t, obj_t, int, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cize3zd3zf3zc3zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3isozd2latinzd215z31zz__unicodez00(obj_t);
extern ucs2_t ucs2_toupper(ucs2_t);
static obj_t BGl_z62utf8zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2stringzd2ze3ucs2zd2stringz53zz__unicodez00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_list4810z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_symbol4844z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4811z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4812z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2stringzd2refz00zz__unicodez00(obj_t, int);
static obj_t BGl_list4813z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4815z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62ucs2zd2stringzd2ze3listz81zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2refz62zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_errorzd2toozd2shortze70ze7zz__unicodez00(int, obj_t, obj_t);
extern obj_t make_ucs2_string(int, ucs2_t);
static obj_t BGl_methodzd2initzd2zz__unicodez00(void);
static obj_t BGl_8bitszd2invzd2zz__unicodez00 = BUNSPEC;
static obj_t BGl_symbol4933z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_symbol4855z00zz__unicodez00 = BUNSPEC;
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_symbol4858z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2upcasez00zz__unicodez00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cizc3zf3z30zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2ze3isozd2latinzd215z53zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_8bitszd2ze3utf8z31zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze38bitsz31zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z628bitszd2ze3utf8z12z41zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_z62utf8zd2ze38bitsz12z41zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_loopze70ze7zz__unicodez00(long, bool_t, long, obj_t, obj_t, long, long);
static obj_t BGl_symbol4942z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_loopze71ze7zz__unicodez00(long, long, obj_t, long);
static obj_t BGl_z62ucs2zd2stringzd2downcasez12z70zz__unicodez00(obj_t, obj_t);
static obj_t BGl_loopze72ze7zz__unicodez00(obj_t, obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_loopze73ze7zz__unicodez00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_asciizd2stringzf3z21zz__unicodez00(obj_t);
extern bool_t ucs2_string_le(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cizd3zf3z20zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexze3zz__unicodez00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2setz12z12zz__unicodez00(obj_t, int, ucs2_t);
static obj_t BGl_z62utf8zd2ze3isozd2latinzd215z12z41zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2stringzd2indexzd2ze3stringzd2indexz81zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2substringzd2urz00zz__unicodez00(obj_t, int, int);
extern bool_t ucs2_string_lt(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3isozd2latinz12zf1zz__unicodez00(obj_t);
static obj_t BGl_z62subucs2zd2stringzd2urz62zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzf3z43zz__unicodez00(obj_t, obj_t);
extern obj_t make_string(long, unsigned char);
static obj_t BGl_list4922z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62listzd2ze3ucs2zd2stringz81zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62cp1252zd2ze3utf8z53zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2ze3cp1252z53zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2upcasez62zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2setzd2urz12za2zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2refz00zz__unicodez00(obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzd2cize3zf3z10zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2stringzd2refz62zz__unicodez00(obj_t, obj_t, obj_t);
static obj_t BGl_cp1252z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_8bitszd2ze3utf8z12z23zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze38bitsz12z23zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzc3zd3zf3z53zz__unicodez00(obj_t, obj_t, obj_t);
extern obj_t string_append(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2stringzd2downcasez62zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2ze3listze3zz__unicodez00(obj_t);
extern obj_t make_string_sans_fill(long);
extern obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
static obj_t BGl_z62ucs2zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_utf8zd2stringzd2rightzd2replacementzf3z21zz__unicodez00(obj_t, long, long);
static obj_t BGl_list4775z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4776z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4777z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_utf8zd2substringzd2zz__unicodez00(obj_t, long, long);
static obj_t BGl_list4778z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4779z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62ucs2zd2stringzd2fillz12z70zz__unicodez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2stringzf3z21zz__unicodez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2minimalzd2charsetz00zz__unicodez00(obj_t);
static obj_t BGl_z62ucs2zd2stringzb0zz__unicodez00(obj_t, obj_t);
static obj_t BGl_z62isozd2latinzd2ze3utf8z12z93zz__unicodez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_ucs2zd2stringzd2copyz00zz__unicodez00(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_list4780z00zz__unicodez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_cp1252zd2ze3utf8z31zz__unicodez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2ze3cp1252z31zz__unicodez00(obj_t);
static obj_t BGl_list4781z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4782z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_z62ucs2zd2substringzb0zz__unicodez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol4978z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4783z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4784z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4785z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4786z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4787z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4788z00zz__unicodez00 = BUNSPEC;
static obj_t BGl_list4789z00zz__unicodez00 = BUNSPEC;
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3ucs2zd2stringzd2envz31zz__unicodez00, BgL_bgl_za762listza7d2za7e3ucs4988za7, BGl_z62listzd2ze3ucs2zd2stringz81zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2substringzd2envz00zz__unicodez00, BgL_bgl_za762ucs2za7d2substr4989z00, BGl_z62ucs2zd2substringzb0zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2setzd2urz12zd2envz12zz__unicodez00, BgL_bgl_za762ucs2za7d2string4990z00, BGl_z62ucs2zd2stringzd2setzd2urz12za2zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2refzd2envzd2zz__unicodez00, BgL_bgl_za762utf8za7d2string4991z00, BGl_z62utf8zd2stringzd2refz62zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_8bitszd2ze3utf8z12zd2envzf1zz__unicodez00, BgL_bgl_za7628bitsza7d2za7e3ut4992za7, BGl_z628bitszd2ze3utf8z12z41zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze38bitsz12zd2envzf1zz__unicodez00, BgL_bgl_za762utf8za7d2za7e38bi4993za7, BGl_z62utf8zd2ze38bitsz12z41zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2appendzd2envzd2zz__unicodez00, BgL_bgl_za762utf8za7d2string4994z00, BGl_z62utf8zd2stringzd2appendz62zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2substringzd2envz00zz__unicodez00, BgL_bgl__utf8za7d2substrin4995za7, opt_generic_entry, BGl__utf8zd2substringzd2zz__unicodez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzc3zf3zd2envz30zz__unicodez00, BgL_bgl_za762ucs2za7d2string4996z00, BGl_z62ucs2zd2stringzc3zf3z80zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2copyzd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2string4997z00, BGl_z62ucs2zd2stringzd2copyz62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexzd2envz31zz__unicodez00, BgL_bgl_za762stringza7d2inde4998z00, BGl_z62stringzd2indexzd2ze3utf8zd2stringzd2indexz81zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringze3zd3zf3zd2envzc3zz__unicodez00, BgL_bgl_za762ucs2za7d2string4999z00, BGl_z62ucs2zd2stringze3zd3zf3z73zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2downcasez12zd2envzc0zz__unicodez00, BgL_bgl_za762ucs2za7d2string5000z00, BGl_z62ucs2zd2stringzd2downcasez12z70zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cp1252zd2ze3utf8z12zd2envzf1zz__unicodez00, BgL_bgl_za762cp1252za7d2za7e3u5001za7, BGl_z62cp1252zd2ze3utf8z12z41zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze3cp1252z12zd2envzf1zz__unicodez00, BgL_bgl_za762utf8za7d2za7e3cp15002za7, BGl_z62utf8zd2ze3cp1252z12z41zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4816z00zz__unicodez00, BgL_bgl_string4816za700za7za7_5003za7, "€", 3 );
DEFINE_STRING( BGl_string4817z00zz__unicodez00, BgL_bgl_string4817za700za7za7_5004za7, "", 0 );
DEFINE_STRING( BGl_string4818z00zz__unicodez00, BgL_bgl_string4818za700za7za7_5005za7, "‚", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2cizd3zf3zd2envzf2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5006z00, BGl_z62ucs2zd2stringzd2cizd3zf3z42zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4819z00zz__unicodez00, BgL_bgl_string4819za700za7za7_5007za7, "ƒ", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_isozd2latinzd2ze3utf8z12zd2envz23zz__unicodez00, BgL_bgl_za762isoza7d2latinza7d5008za7, BGl_z62isozd2latinzd2ze3utf8z12z93zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2normaliza7ezd2utf16zd2envz75zz__unicodez00, BgL_bgl__utf8za7d2normaliza75009z00, opt_generic_entry, BGl__utf8zd2normaliza7ezd2utf16za7zz__unicodez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_subucs2zd2stringzd2envz00zz__unicodez00, BgL_bgl_za762subucs2za7d2str5010z00, BGl_z62subucs2zd2stringzb0zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string4900z00zz__unicodez00, BgL_bgl_string4900za700za7za7_5011za7, "add-table-entry~0", 17 );
DEFINE_STRING( BGl_string4901z00zz__unicodez00, BgL_bgl_string4901za700za7za7_5012za7, "&inverse-utf8-table", 19 );
DEFINE_STRING( BGl_string4820z00zz__unicodez00, BgL_bgl_string4820za700za7za7_5013za7, "„", 3 );
DEFINE_STRING( BGl_string4902z00zz__unicodez00, BgL_bgl_string4902za700za7za7_5014za7, "vector", 6 );
DEFINE_STRING( BGl_string4821z00zz__unicodez00, BgL_bgl_string4821za700za7za7_5015za7, "…", 3 );
DEFINE_STRING( BGl_string4903z00zz__unicodez00, BgL_bgl_string4903za700za7za7_5016za7, "&utf8-string->ucs2-string", 25 );
DEFINE_STRING( BGl_string4822z00zz__unicodez00, BgL_bgl_string4822za700za7za7_5017za7, "†", 3 );
DEFINE_STRING( BGl_string4823z00zz__unicodez00, BgL_bgl_string4823za700za7za7_5018za7, "‡", 3 );
DEFINE_STRING( BGl_string4905z00zz__unicodez00, BgL_bgl_string4905za700za7za7_5019za7, "ascii", 5 );
DEFINE_STRING( BGl_string4824z00zz__unicodez00, BgL_bgl_string4824za700za7za7_5020za7, "ˆ", 2 );
DEFINE_STRING( BGl_string4906z00zz__unicodez00, BgL_bgl_string4906za700za7za7_5021za7, "&ascii-string?", 14 );
DEFINE_STRING( BGl_string4825z00zz__unicodez00, BgL_bgl_string4825za700za7za7_5022za7, "‰", 3 );
DEFINE_STRING( BGl_string4826z00zz__unicodez00, BgL_bgl_string4826za700za7za7_5023za7, "Š", 2 );
DEFINE_STRING( BGl_string4908z00zz__unicodez00, BgL_bgl_string4908za700za7za7_5024za7, "utf8-string?", 12 );
DEFINE_STRING( BGl_string4827z00zz__unicodez00, BgL_bgl_string4827za700za7za7_5025za7, "‹", 3 );
DEFINE_STRING( BGl_string4909z00zz__unicodez00, BgL_bgl_string4909za700za7za7_5026za7, "_utf8-string?", 13 );
DEFINE_STRING( BGl_string4828z00zz__unicodez00, BgL_bgl_string4828za700za7za7_5027za7, "Œ", 2 );
DEFINE_STRING( BGl_string4829z00zz__unicodez00, BgL_bgl_string4829za700za7za7_5028za7, "Ž", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2refzd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5029z00, BGl_z62ucs2zd2stringzd2refz62zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2minimalzd2charsetzd2envz00zz__unicodez00, BgL_bgl_za762ucs2za7d2string5030z00, BGl_z62ucs2zd2stringzd2minimalzd2charsetzb0zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4911z00zz__unicodez00, BgL_bgl_string4911za700za7za7_5031za7, "utf8-string-encode", 18 );
DEFINE_STRING( BGl_string4830z00zz__unicodez00, BgL_bgl_string4830za700za7za7_5032za7, "‘", 3 );
DEFINE_STRING( BGl_string4912z00zz__unicodez00, BgL_bgl_string4912za700za7za7_5033za7, "wrong number of arguments: [1..4] expected, provided", 52 );
DEFINE_STRING( BGl_string4831z00zz__unicodez00, BgL_bgl_string4831za700za7za7_5034za7, "’", 3 );
DEFINE_STRING( BGl_string4913z00zz__unicodez00, BgL_bgl_string4913za700za7za7_5035za7, "_utf8-string-encode", 19 );
DEFINE_STRING( BGl_string4832z00zz__unicodez00, BgL_bgl_string4832za700za7za7_5036za7, "“", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2rightzd2replacementzf3zd2envzf3zz__unicodez00, BgL_bgl_za762utf8za7d2string5037z00, BGl_z62utf8zd2stringzd2rightzd2replacementzf3z43zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string4914z00zz__unicodez00, BgL_bgl_string4914za700za7za7_5038za7, "Illegal indexes", 15 );
DEFINE_STRING( BGl_string4833z00zz__unicodez00, BgL_bgl_string4833za700za7za7_5039za7, "”", 3 );
DEFINE_STRING( BGl_string4915z00zz__unicodez00, BgL_bgl_string4915za700za7za7_5040za7, "string-set!", 11 );
DEFINE_STRING( BGl_string4834z00zz__unicodez00, BgL_bgl_string4834za700za7za7_5041za7, "•", 3 );
DEFINE_STRING( BGl_string4835z00zz__unicodez00, BgL_bgl_string4835za700za7za7_5042za7, "–", 3 );
DEFINE_STRING( BGl_string4917z00zz__unicodez00, BgL_bgl_string4917za700za7za7_5043za7, "utf8-normalize-utf16", 20 );
DEFINE_STRING( BGl_string4836z00zz__unicodez00, BgL_bgl_string4836za700za7za7_5044za7, "—", 3 );
DEFINE_STRING( BGl_string4918z00zz__unicodez00, BgL_bgl_string4918za700za7za7_5045za7, "_utf8-normalize-utf16", 21 );
DEFINE_STRING( BGl_string4837z00zz__unicodez00, BgL_bgl_string4837za700za7za7_5046za7, "˜", 2 );
DEFINE_STRING( BGl_string4838z00zz__unicodez00, BgL_bgl_string4838za700za7za7_5047za7, "™", 3 );
DEFINE_STRING( BGl_string4839z00zz__unicodez00, BgL_bgl_string4839za700za7za7_5048za7, "š", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2minimalzd2charsetzd2envzd2zz__unicodez00, BgL_bgl_za762stringza7d2mini5049z00, BGl_z62stringzd2minimalzd2charsetz62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2upcasezd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5050z00, BGl_z62ucs2zd2stringzd2upcasez62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4920z00zz__unicodez00, BgL_bgl_string4920za700za7za7_5051za7, "utf8", 4 );
DEFINE_STRING( BGl_string4840z00zz__unicodez00, BgL_bgl_string4840za700za7za7_5052za7, "›", 3 );
DEFINE_STRING( BGl_string4841z00zz__unicodez00, BgL_bgl_string4841za700za7za7_5053za7, "œ", 2 );
DEFINE_STRING( BGl_string4923z00zz__unicodez00, BgL_bgl_string4923za700za7za7_5054za7, "utf8-char-size", 14 );
DEFINE_STRING( BGl_string4842z00zz__unicodez00, BgL_bgl_string4842za700za7za7_5055za7, "ž", 2 );
DEFINE_STRING( BGl_string4924z00zz__unicodez00, BgL_bgl_string4924za700za7za7_5056za7, "&utf8-char-size", 15 );
DEFINE_STRING( BGl_string4843z00zz__unicodez00, BgL_bgl_string4843za700za7za7_5057za7, "Ÿ", 2 );
DEFINE_STRING( BGl_string4925z00zz__unicodez00, BgL_bgl_string4925za700za7za7_5058za7, "bchar", 5 );
DEFINE_STRING( BGl_string4926z00zz__unicodez00, BgL_bgl_string4926za700za7za7_5059za7, "&utf8-string-length", 19 );
DEFINE_STRING( BGl_string4845z00zz__unicodez00, BgL_bgl_string4845za700za7za7_5060za7, "make-ucs2-string", 16 );
DEFINE_STRING( BGl_string4927z00zz__unicodez00, BgL_bgl_string4927za700za7za7_5061za7, "&utf8-string-ref", 16 );
DEFINE_STRING( BGl_string4846z00zz__unicodez00, BgL_bgl_string4846za700za7za7_5062za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string4928z00zz__unicodez00, BgL_bgl_string4928za700za7za7_5063za7, "&utf8-string-index->string-index", 32 );
DEFINE_STRING( BGl_string4847z00zz__unicodez00, BgL_bgl_string4847za700za7za7_5064za7, "/tmp/bigloo/runtime/Llib/unicode.scm", 36 );
DEFINE_STRING( BGl_string4929z00zz__unicodez00, BgL_bgl_string4929za700za7za7_5065za7, "&string-index->utf8-string-index", 32 );
DEFINE_STRING( BGl_string4848z00zz__unicodez00, BgL_bgl_string4848za700za7za7_5066za7, "_make-ucs2-string", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzc3zd3zf3zd2envze3zz__unicodez00, BgL_bgl_za762ucs2za7d2string5067z00, BGl_z62ucs2zd2stringzc3zd3zf3z53zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4849z00zz__unicodez00, BgL_bgl_string4849za700za7za7_5068za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd3zf3zd2envz20zz__unicodez00, BgL_bgl_za762ucs2za7d2string5069z00, BGl_z62ucs2zd2stringzd3zf3z90zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2downcasezd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5070z00, BGl_z62ucs2zd2stringzd2downcasez62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inversezd2utf8zd2tablezd2envzd2zz__unicodez00, BgL_bgl_za762inverseza7d2utf5071z00, BGl_z62inversezd2utf8zd2tablez62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4930z00zz__unicodez00, BgL_bgl_string4930za700za7za7_5072za7, "&utf8-string-left-replacement?", 30 );
DEFINE_STRING( BGl_string4931z00zz__unicodez00, BgL_bgl_string4931za700za7za7_5073za7, "&utf8-string-right-replacement?", 31 );
DEFINE_STRING( BGl_string4850z00zz__unicodez00, BgL_bgl_string4850za700za7za7_5074za7, "bucs2", 5 );
DEFINE_STRING( BGl_string4932z00zz__unicodez00, BgL_bgl_string4932za700za7za7_5075za7, "utf8-collapse!", 14 );
DEFINE_STRING( BGl_string4851z00zz__unicodez00, BgL_bgl_string4851za700za7za7_5076za7, "&ucs2-string-length", 19 );
DEFINE_STRING( BGl_string4852z00zz__unicodez00, BgL_bgl_string4852za700za7za7_5077za7, "ucs2string", 10 );
DEFINE_STRING( BGl_string4934z00zz__unicodez00, BgL_bgl_string4934za700za7za7_5078za7, "utf8-string-append-fill!", 24 );
DEFINE_STRING( BGl_string4853z00zz__unicodez00, BgL_bgl_string4853za700za7za7_5079za7, "index out of range [0..", 23 );
DEFINE_STRING( BGl_string4935z00zz__unicodez00, BgL_bgl_string4935za700za7za7_5080za7, "wrong number of arguments: [3..4] expected, provided", 52 );
DEFINE_STRING( BGl_string4854z00zz__unicodez00, BgL_bgl_string4854za700za7za7_5081za7, "]", 1 );
DEFINE_STRING( BGl_string4936z00zz__unicodez00, BgL_bgl_string4936za700za7za7_5082za7, "_utf8-string-append-fill!", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexzd2envz31zz__unicodez00, BgL_bgl_za762utf8za7d2string5083z00, BGl_z62utf8zd2stringzd2indexzd2ze3stringzd2indexz81zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4937z00zz__unicodez00, BgL_bgl_string4937za700za7za7_5084za7, "&utf8-string-append", 19 );
DEFINE_STRING( BGl_string4856z00zz__unicodez00, BgL_bgl_string4856za700za7za7_5085za7, "ucs2-string-ref", 15 );
DEFINE_STRING( BGl_string4938z00zz__unicodez00, BgL_bgl_string4938za700za7za7_5086za7, "<@anonymous:2644>", 17 );
DEFINE_STRING( BGl_string4857z00zz__unicodez00, BgL_bgl_string4857za700za7za7_5087za7, "&ucs2-string-ref", 16 );
DEFINE_STRING( BGl_string4939z00zz__unicodez00, BgL_bgl_string4939za700za7za7_5088za7, "for-each", 8 );
DEFINE_STRING( BGl_string4859z00zz__unicodez00, BgL_bgl_string4859za700za7za7_5089za7, "ucs2-string-set!", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2setz12zd2envzc0zz__unicodez00, BgL_bgl_za762ucs2za7d2string5090z00, BGl_z62ucs2zd2stringzd2setz12z70zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2substringzd2urzd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2substr5091z00, BGl_z62ucs2zd2substringzd2urz62zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_isozd2latinzd2ze3utf8zd2envz31zz__unicodez00, BgL_bgl_za762isoza7d2latinza7d5092za7, BGl_z62isozd2latinzd2ze3utf8z81zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze3isozd2latinz12zd2envz23zz__unicodez00, BgL_bgl_za762utf8za7d2za7e3iso5093za7, BGl_z62utf8zd2ze3isozd2latinz12z93zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2encodezd2envzd2zz__unicodez00, BgL_bgl__utf8za7d2stringza7d5094z00, opt_generic_entry, BGl__utf8zd2stringzd2encodez00zz__unicodez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2appendzd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5095z00, va_generic_entry, BGl_z62ucs2zd2stringzd2appendz62zz__unicodez00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2ucs2zd2stringzd2envzd2zz__unicodez00, BgL_bgl__makeza7d2ucs2za7d2s5096z00, opt_generic_entry, BGl__makezd2ucs2zd2stringz00zz__unicodez00, BFALSE, -1 );
DEFINE_STRING( BGl_string4940z00zz__unicodez00, BgL_bgl_string4940za700za7za7_5097za7, "list", 4 );
DEFINE_STRING( BGl_string4941z00zz__unicodez00, BgL_bgl_string4941za700za7za7_5098za7, "<@anonymous:2650>", 17 );
DEFINE_STRING( BGl_string4860z00zz__unicodez00, BgL_bgl_string4860za700za7za7_5099za7, "&ucs2-string-set!", 17 );
DEFINE_STRING( BGl_string4861z00zz__unicodez00, BgL_bgl_string4861za700za7za7_5100za7, "&ucs2-string-ref-ur", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2cize3zd3zf3zd2envz11zz__unicodez00, BgL_bgl_za762ucs2za7d2string5101z00, BGl_z62ucs2zd2stringzd2cize3zd3zf3za1zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4943z00zz__unicodez00, BgL_bgl_string4943za700za7za7_5102za7, "utf8-substring", 14 );
DEFINE_STRING( BGl_string4862z00zz__unicodez00, BgL_bgl_string4862za700za7za7_5103za7, "&ucs2-string-set-ur!", 20 );
DEFINE_STRING( BGl_string4944z00zz__unicodez00, BgL_bgl_string4944za700za7za7_5104za7, "wrong number of arguments: [2..3] expected, provided", 52 );
DEFINE_STRING( BGl_string4863z00zz__unicodez00, BgL_bgl_string4863za700za7za7_5105za7, "&ucs2-string=?", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2minimalzd2charsetzd2envz00zz__unicodez00, BgL_bgl_za762utf8za7d2string5106z00, BGl_z62utf8zd2stringzd2minimalzd2charsetzb0zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4945z00zz__unicodez00, BgL_bgl_string4945za700za7za7_5107za7, "_utf8-substring", 15 );
DEFINE_STRING( BGl_string4864z00zz__unicodez00, BgL_bgl_string4864za700za7za7_5108za7, "&ucs2-string-ci=?", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2cize3zf3zd2envzc2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5109z00, BGl_z62ucs2zd2stringzd2cize3zf3z72zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4946z00zz__unicodez00, BgL_bgl_string4946za700za7za7_5110za7, "Illegal start index \"", 21 );
DEFINE_STRING( BGl_string4865z00zz__unicodez00, BgL_bgl_string4865za700za7za7_5111za7, "&ucs2-string<?", 14 );
DEFINE_STRING( BGl_string4947z00zz__unicodez00, BgL_bgl_string4947za700za7za7_5112za7, "\"", 1 );
DEFINE_STRING( BGl_string4866z00zz__unicodez00, BgL_bgl_string4866za700za7za7_5113za7, "&ucs2-string>?", 14 );
DEFINE_STRING( BGl_string4948z00zz__unicodez00, BgL_bgl_string4948za700za7za7_5114za7, "Illegal end index \"", 19 );
DEFINE_STRING( BGl_string4867z00zz__unicodez00, BgL_bgl_string4867za700za7za7_5115za7, "&ucs2-string<=?", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2lengthzd2envzd2zz__unicodez00, BgL_bgl_za762utf8za7d2string5116z00, BGl_z62utf8zd2stringzd2lengthz62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4949z00zz__unicodez00, BgL_bgl_string4949za700za7za7_5117za7, "error-ill", 9 );
DEFINE_STRING( BGl_string4868z00zz__unicodez00, BgL_bgl_string4868za700za7za7_5118za7, "&ucs2-string>=?", 15 );
DEFINE_STRING( BGl_string4869z00zz__unicodez00, BgL_bgl_string4869za700za7za7_5119za7, "&ucs2-string-ci<?", 17 );
DEFINE_STRING( BGl_string4950z00zz__unicodez00, BgL_bgl_string4950za700za7za7_5120za7, "Illegal character \"~x\" at index ~a", 34 );
DEFINE_STRING( BGl_string4951z00zz__unicodez00, BgL_bgl_string4951za700za7za7_5121za7, "utf8->8bits", 11 );
DEFINE_STRING( BGl_string4870z00zz__unicodez00, BgL_bgl_string4870za700za7za7_5122za7, "&ucs2-string-ci>?", 17 );
DEFINE_STRING( BGl_string4952z00zz__unicodez00, BgL_bgl_string4952za700za7za7_5123za7, "error-too-short~0", 17 );
DEFINE_STRING( BGl_string4871z00zz__unicodez00, BgL_bgl_string4871za700za7za7_5124za7, "&ucs2-string-ci<=?", 18 );
DEFINE_STRING( BGl_string4953z00zz__unicodez00, BgL_bgl_string4953za700za7za7_5125za7, "String too short", 16 );
DEFINE_STRING( BGl_string4872z00zz__unicodez00, BgL_bgl_string4872za700za7za7_5126za7, "&ucs2-string-ci>=?", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2fillz12zd2envzc0zz__unicodez00, BgL_bgl_za762ucs2za7d2string5127z00, BGl_z62ucs2zd2stringzd2fillz12z70zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4954z00zz__unicodez00, BgL_bgl_string4954za700za7za7_5128za7, "error-subtable", 14 );
DEFINE_STRING( BGl_string4873z00zz__unicodez00, BgL_bgl_string4873za700za7za7_5129za7, "subucs2-string", 14 );
DEFINE_STRING( BGl_string4955z00zz__unicodez00, BgL_bgl_string4955za700za7za7_5130za7, "Cannot encode at index ", 23 );
DEFINE_STRING( BGl_string4874z00zz__unicodez00, BgL_bgl_string4874za700za7za7_5131za7, "Illegal index", 13 );
DEFINE_STRING( BGl_string4956z00zz__unicodez00, BgL_bgl_string4956za700za7za7_5132za7, "table->8bits~0", 14 );
DEFINE_STRING( BGl_string4875z00zz__unicodez00, BgL_bgl_string4875za700za7za7_5133za7, "&subucs2-string", 15 );
DEFINE_STRING( BGl_string4957z00zz__unicodez00, BgL_bgl_string4957za700za7za7_5134za7, "liip", 4 );
DEFINE_STRING( BGl_string4876z00zz__unicodez00, BgL_bgl_string4876za700za7za7_5135za7, "&subucs2-string-ur", 18 );
DEFINE_STRING( BGl_string4958z00zz__unicodez00, BgL_bgl_string4958za700za7za7_5136za7, "&utf8->8bits", 12 );
DEFINE_STRING( BGl_string4877z00zz__unicodez00, BgL_bgl_string4877za700za7za7_5137za7, "ucs2-substring", 14 );
DEFINE_STRING( BGl_string4959z00zz__unicodez00, BgL_bgl_string4959za700za7za7_5138za7, "utf8->8bits!", 12 );
DEFINE_STRING( BGl_string4878z00zz__unicodez00, BgL_bgl_string4878za700za7za7_5139za7, "&ucs2-substring", 15 );
DEFINE_STRING( BGl_string4879z00zz__unicodez00, BgL_bgl_string4879za700za7za7_5140za7, "&ucs2-substring-ur", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2envz00zz__unicodez00, BgL_bgl_za762ucs2za7d2string5141z00, va_generic_entry, BGl_z62ucs2zd2stringzb0zz__unicodez00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cp1252zd2ze3utf8zd2envze3zz__unicodez00, BgL_bgl_za762cp1252za7d2za7e3u5142za7, BGl_z62cp1252zd2ze3utf8z53zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze3cp1252zd2envze3zz__unicodez00, BgL_bgl_za762utf8za7d2za7e3cp15143za7, BGl_z62utf8zd2ze3cp1252z53zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4960z00zz__unicodez00, BgL_bgl_string4960za700za7za7_5144za7, "&utf8->8bits!", 13 );
DEFINE_STRING( BGl_string4961z00zz__unicodez00, BgL_bgl_string4961za700za7za7_5145za7, "utf8->iso-latin", 15 );
DEFINE_STRING( BGl_string4880z00zz__unicodez00, BgL_bgl_string4880za700za7za7_5146za7, "ucs2-string-append", 18 );
DEFINE_STRING( BGl_string4962z00zz__unicodez00, BgL_bgl_string4962za700za7za7_5147za7, "&utf8->iso-latin", 16 );
DEFINE_STRING( BGl_string4881z00zz__unicodez00, BgL_bgl_string4881za700za7za7_5148za7, "loop~3", 6 );
DEFINE_STRING( BGl_string4963z00zz__unicodez00, BgL_bgl_string4963za700za7za7_5149za7, "utf8->iso-latin!", 16 );
DEFINE_STRING( BGl_string4882z00zz__unicodez00, BgL_bgl_string4882za700za7za7_5150za7, "pair", 4 );
DEFINE_STRING( BGl_string4964z00zz__unicodez00, BgL_bgl_string4964za700za7za7_5151za7, "&utf8->iso-latin!", 17 );
DEFINE_STRING( BGl_string4883z00zz__unicodez00, BgL_bgl_string4883za700za7za7_5152za7, "list->ucs2-string", 17 );
DEFINE_STRING( BGl_string4965z00zz__unicodez00, BgL_bgl_string4965za700za7za7_5153za7, "utf8->iso-latin-15", 18 );
DEFINE_STRING( BGl_string4884z00zz__unicodez00, BgL_bgl_string4884za700za7za7_5154za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string4966z00zz__unicodez00, BgL_bgl_string4966za700za7za7_5155za7, "&utf8->iso-latin-15", 19 );
DEFINE_STRING( BGl_string4885z00zz__unicodez00, BgL_bgl_string4885za700za7za7_5156za7, "loop", 4 );
DEFINE_STRING( BGl_string4967z00zz__unicodez00, BgL_bgl_string4967za700za7za7_5157za7, "utf8->iso-latin-15!", 19 );
DEFINE_STRING( BGl_string4886z00zz__unicodez00, BgL_bgl_string4886za700za7za7_5158za7, "&ucs2-string->list", 18 );
DEFINE_STRING( BGl_string4968z00zz__unicodez00, BgL_bgl_string4968za700za7za7_5159za7, "&utf8->iso-latin-15!", 20 );
DEFINE_STRING( BGl_string4887z00zz__unicodez00, BgL_bgl_string4887za700za7za7_5160za7, "&ucs2-string-copy", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_8bitszd2ze3utf8zd2envze3zz__unicodez00, BgL_bgl_za7628bitsza7d2za7e3ut5161za7, BGl_z628bitszd2ze3utf8z53zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze38bitszd2envze3zz__unicodez00, BgL_bgl_za762utf8za7d2za7e38bi5162za7, BGl_z62utf8zd2ze38bitsz53zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4969z00zz__unicodez00, BgL_bgl_string4969za700za7za7_5163za7, "&utf8->cp1252", 13 );
DEFINE_STRING( BGl_string4888z00zz__unicodez00, BgL_bgl_string4888za700za7za7_5164za7, "&ucs2-string-fill!", 18 );
DEFINE_STRING( BGl_string4889z00zz__unicodez00, BgL_bgl_string4889za700za7za7_5165za7, "&ucs2-string-upcase", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2appendzd2fillz12zd2envz12zz__unicodez00, BgL_bgl__utf8za7d2stringza7d5166z00, opt_generic_entry, BGl__utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzf3zd2envzf3zz__unicodez00, BgL_bgl__utf8za7d2stringza7f5167z00, opt_generic_entry, BGl__utf8zd2stringzf3z21zz__unicodez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_asciizd2stringzf3zd2envzf3zz__unicodez00, BgL_bgl_za762asciiza7d2strin5168z00, BGl_z62asciizd2stringzf3z43zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4970z00zz__unicodez00, BgL_bgl_string4970za700za7za7_5169za7, "&utf8->cp1252!", 14 );
DEFINE_STRING( BGl_string4971z00zz__unicodez00, BgL_bgl_string4971za700za7za7_5170za7, "table-ref-len", 13 );
DEFINE_STRING( BGl_string4890z00zz__unicodez00, BgL_bgl_string4890za700za7za7_5171za7, "&ucs2-string-downcase", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze3isozd2latinzd215z12zd2envzf1zz__unicodez00, BgL_bgl_za762utf8za7d2za7e3iso5172za7, BGl_z62utf8zd2ze3isozd2latinzd215z12z41zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4972z00zz__unicodez00, BgL_bgl_string4972za700za7za7_5173za7, "&8bits->utf8", 12 );
DEFINE_STRING( BGl_string4891z00zz__unicodez00, BgL_bgl_string4891za700za7za7_5174za7, "&ucs2-string-upcase!", 20 );
DEFINE_STRING( BGl_string4973z00zz__unicodez00, BgL_bgl_string4973za700za7za7_5175za7, "&8bits->utf8!", 13 );
DEFINE_STRING( BGl_string4892z00zz__unicodez00, BgL_bgl_string4892za700za7za7_5176za7, "&ucs2-string-downcase!", 22 );
DEFINE_STRING( BGl_string4974z00zz__unicodez00, BgL_bgl_string4974za700za7za7_5177za7, "&iso-latin->utf8", 16 );
DEFINE_STRING( BGl_string4893z00zz__unicodez00, BgL_bgl_string4893za700za7za7_5178za7, "&ucs2-string->utf8-string", 25 );
DEFINE_STRING( BGl_string4975z00zz__unicodez00, BgL_bgl_string4975za700za7za7_5179za7, "&iso-latin->utf8!", 17 );
DEFINE_STRING( BGl_string4894z00zz__unicodez00, BgL_bgl_string4894za700za7za7_5180za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string4976z00zz__unicodez00, BgL_bgl_string4976za700za7za7_5181za7, "&cp1252->utf8", 13 );
DEFINE_STRING( BGl_string4895z00zz__unicodez00, BgL_bgl_string4895za700za7za7_5182za7, "bstring", 7 );
DEFINE_STRING( BGl_string4977z00zz__unicodez00, BgL_bgl_string4977za700za7za7_5183za7, "&cp1252->utf8!", 14 );
DEFINE_STRING( BGl_string4896z00zz__unicodez00, BgL_bgl_string4896za700za7za7_5184za7, "loop~1", 6 );
DEFINE_STRING( BGl_string4897z00zz__unicodez00, BgL_bgl_string4897za700za7za7_5185za7, "string-ref", 10 );
DEFINE_STRING( BGl_string4979z00zz__unicodez00, BgL_bgl_string4979za700za7za7_5186za7, "latin1", 6 );
DEFINE_STRING( BGl_string4898z00zz__unicodez00, BgL_bgl_string4898za700za7za7_5187za7, "make-table-entry~0", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringze3zf3zd2envz10zz__unicodez00, BgL_bgl_za762ucs2za7d2string5188z00, BGl_z62ucs2zd2stringze3zf3za0zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4899z00zz__unicodez00, BgL_bgl_string4899za700za7za7_5189za7, "loop~2", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_subucs2zd2stringzd2urzd2envzd2zz__unicodez00, BgL_bgl_za762subucs2za7d2str5190z00, BGl_z62subucs2zd2stringzd2urz62zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze3isozd2latinzd2envz31zz__unicodez00, BgL_bgl_za762utf8za7d2za7e3iso5191za7, BGl_z62utf8zd2ze3isozd2latinz81zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2cizc3zd3zf3zd2envz31zz__unicodez00, BgL_bgl_za762ucs2za7d2string5192z00, BGl_z62ucs2zd2stringzd2cizc3zd3zf3z81zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2ze3isozd2latinzd215zd2envze3zz__unicodez00, BgL_bgl_za762utf8za7d2za7e3iso5193za7, BGl_z62utf8zd2ze3isozd2latinzd215z53zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4980z00zz__unicodez00, BgL_bgl_string4980za700za7za7_5194za7, "&string-minimal-charset", 23 );
DEFINE_STRING( BGl_string4981z00zz__unicodez00, BgL_bgl_string4981za700za7za7_5195za7, "&utf8-string-minimal-charset", 28 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2ze3utf8zd2stringzd2envze3zz__unicodez00, BgL_bgl_za762ucs2za7d2string5196z00, BGl_z62ucs2zd2stringzd2ze3utf8zd2stringz53zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4983z00zz__unicodez00, BgL_bgl_string4983za700za7za7_5197za7, "ucs2", 4 );
DEFINE_STRING( BGl_string4985z00zz__unicodez00, BgL_bgl_string4985za700za7za7_5198za7, "utf16", 5 );
DEFINE_STRING( BGl_string4986z00zz__unicodez00, BgL_bgl_string4986za700za7za7_5199za7, "&ucs2-string-minimal-charset", 28 );
DEFINE_STRING( BGl_string4987z00zz__unicodez00, BgL_bgl_string4987za700za7za7_5200za7, "__unicode", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2leftzd2replacementzf3zd2envzf3zz__unicodez00, BgL_bgl_za762utf8za7d2string5201z00, BGl_z62utf8zd2stringzd2leftzd2replacementzf3z43zz__unicodez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2refzd2urzd2envz00zz__unicodez00, BgL_bgl_za762ucs2za7d2string5202z00, BGl_z62ucs2zd2stringzd2refzd2urzb0zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2ze3listzd2envz31zz__unicodez00, BgL_bgl_za762ucs2za7d2string5203z00, BGl_z62ucs2zd2stringzd2ze3listz81zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzf3zd2envzf3zz__unicodez00, BgL_bgl_za762ucs2za7d2string5204z00, BGl_z62ucs2zd2stringzf3z43zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2cizc3zf3zd2envze2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5205z00, BGl_z62ucs2zd2stringzd2cizc3zf3z52zz__unicodez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2ze3ucs2zd2stringzd2envze3zz__unicodez00, BgL_bgl_za762utf8za7d2string5206z00, BGl_z62utf8zd2stringzd2ze3ucs2zd2stringz53zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2upcasez12zd2envzc0zz__unicodez00, BgL_bgl_za762ucs2za7d2string5207z00, BGl_z62ucs2zd2stringzd2upcasez12z70zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2stringzd2lengthzd2envzd2zz__unicodez00, BgL_bgl_za762ucs2za7d2string5208z00, BGl_z62ucs2zd2stringzd2lengthz62zz__unicodez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2appendza2zd2envz70zz__unicodez00, BgL_bgl_za762utf8za7d2string5209z00, va_generic_entry, BGl_z62utf8zd2stringzd2appendza2zc0zz__unicodez00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2charzd2siza7ezd2envz75zz__unicodez00, BgL_bgl_za762utf8za7d2charza7d5210za7, BGl_z62utf8zd2charzd2siza7ezc5zz__unicodez00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_vector4814z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4982z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4984z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4790z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4791z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4792z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4793z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4794z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4795z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4796z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4797z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4798z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4799z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_8bitszd2invzd2latinzd215zd2zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_vector4921z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4904z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4907z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_cp1252zd2invzd2zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4910z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4800z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4801z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4916z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4802z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4803z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4804z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4919z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4805z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4806z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4807z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4808z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4809z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4810z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4844z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4811z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4812z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4813z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4815z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_8bitszd2invzd2zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4933z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4855z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4858z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4942z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4922z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_cp1252z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4775z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4776z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4777z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4778z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4779z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4780z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4781z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4782z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_symbol4978z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4783z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4784z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4785z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4786z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4787z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4788z00zz__unicodez00) );
ADD_ROOT( (void *)(&BGl_list4789z00zz__unicodez00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__unicodez00(long BgL_checksumz00_7708, char * BgL_fromz00_7709)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__unicodez00))
{ 
BGl_requirezd2initializa7ationz75zz__unicodez00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__unicodez00(); 
BGl_cnstzd2initzd2zz__unicodez00(); 
BGl_importedzd2moduleszd2initz00zz__unicodez00(); 
return 
BGl_toplevelzd2initzd2zz__unicodez00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
BGl_list4778z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(144L), 
BCHAR(((unsigned char)'-'))); 
BGl_list4779z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(145L), 
BCHAR(((unsigned char)'-'))); 
BGl_list4780z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(146L), 
BCHAR(((unsigned char)'-'))); 
BGl_list4781z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(147L), 
BCHAR(((unsigned char)'-'))); 
BGl_list4782z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(148L), 
BCHAR(((unsigned char)'-'))); 
BGl_list4783z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(149L), 
BCHAR(((unsigned char)'-'))); 
BGl_list4784z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(152L), 
BCHAR(((unsigned char)'`'))); 
BGl_list4785z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(153L), 
BCHAR(((unsigned char)'\''))); 
BGl_list4786z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(154L), 
BCHAR(((unsigned char)','))); 
BGl_list4787z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(155L), 
BCHAR(((unsigned char)'`'))); 
BGl_list4788z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(164L), 
BCHAR(((unsigned char)'.'))); 
BGl_list4789z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(167L), 
BCHAR(((unsigned char)'.'))); 
BGl_list4790z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(178L), 
BCHAR(((unsigned char)'\''))); 
BGl_list4791z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(179L), 
BCHAR(((unsigned char)'"'))); 
BGl_list4792z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(181L), 
BCHAR(((unsigned char)'`'))); 
BGl_list4793z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(187L), 
BCHAR(((unsigned char)'"'))); 
BGl_list4794z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(184L), 
BCHAR(((unsigned char)'^'))); 
BGl_list4795z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(185L), 
BCHAR(((unsigned char)'<'))); 
BGl_list4796z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(186L), 
BCHAR(((unsigned char)'>'))); 
BGl_list4777z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(128L), 
MAKE_YOUNG_PAIR(BGl_list4778z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4779z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4780z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4781z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4782z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4783z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4784z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4785z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4786z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4787z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4788z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4789z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4790z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4791z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4792z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4793z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4794z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4795z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4796z00zz__unicodez00, BNIL)))))))))))))))))))); 
BGl_list4798z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(131L), 
BCHAR(((unsigned char)'*'))); 
BGl_list4797z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(129L), 
MAKE_YOUNG_PAIR(BGl_list4798z00zz__unicodez00, BNIL)); 
BGl_list4800z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(141L), 
BCHAR(((unsigned char)'('))); 
BGl_list4801z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(142L), 
BCHAR(((unsigned char)')'))); 
BGl_list4799z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(130L), 
MAKE_YOUNG_PAIR(BGl_list4800z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4801z00zz__unicodez00, BNIL))); 
BGl_list4776z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(226L), 
MAKE_YOUNG_PAIR(BGl_list4777z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4797z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4799z00zz__unicodez00, BNIL)))); 
BGl_list4775z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(BGl_list4776z00zz__unicodez00, BNIL); 
BGl_list4805z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(172L), 
BCHAR(((unsigned char)164))); 
BGl_list4804z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(130L), 
MAKE_YOUNG_PAIR(BGl_list4800z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4801z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4805z00zz__unicodez00, BNIL)))); 
BGl_list4803z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(226L), 
MAKE_YOUNG_PAIR(BGl_list4777z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4797z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4804z00zz__unicodez00, BNIL)))); 
BGl_list4807z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(147L), 
BCHAR(((unsigned char)189))); 
BGl_list4808z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(146L), 
BCHAR(((unsigned char)188))); 
BGl_list4809z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(184L), 
BCHAR(((unsigned char)190))); 
BGl_list4810z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(161L), 
BCHAR(((unsigned char)168))); 
BGl_list4811z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(160L), 
BCHAR(((unsigned char)166))); 
BGl_list4812z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(190L), 
BCHAR(((unsigned char)184))); 
BGl_list4813z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(189L), 
BCHAR(((unsigned char)180))); 
BGl_list4806z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(197L), 
MAKE_YOUNG_PAIR(BGl_list4807z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4808z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4809z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4810z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4811z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4812z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4813z00zz__unicodez00, BNIL)))))))); 
BGl_list4802z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(BGl_list4803z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_list4806z00zz__unicodez00, BNIL)); 
BGl_list4815z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(BGl_string4816z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4817z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4818z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4819z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4820z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4821z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4822z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4823z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4824z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4825z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4826z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4827z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4828z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4817z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4829z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4817z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4817z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4830z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4831z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4832z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4833z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4834z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4835z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4836z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4837z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4838z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4839z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4840z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4841z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4817z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4842z00zz__unicodez00, 
MAKE_YOUNG_PAIR(BGl_string4843z00zz__unicodez00, BNIL)))))))))))))))))))))))))))))))); 
BGl_vector4814z00zz__unicodez00 = 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BGl_list4815z00zz__unicodez00); 
BGl_symbol4844z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4845z00zz__unicodez00); 
BGl_symbol4855z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4856z00zz__unicodez00); 
BGl_symbol4858z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4859z00zz__unicodez00); 
BGl_symbol4904z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4905z00zz__unicodez00); 
BGl_symbol4907z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4908z00zz__unicodez00); 
BGl_symbol4910z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4911z00zz__unicodez00); 
BGl_symbol4916z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4917z00zz__unicodez00); 
BGl_symbol4919z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4920z00zz__unicodez00); 
BGl_list4922z00zz__unicodez00 = 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(1L), 
MAKE_YOUNG_PAIR(
BINT(2L), 
MAKE_YOUNG_PAIR(
BINT(2L), 
MAKE_YOUNG_PAIR(
BINT(2L), 
MAKE_YOUNG_PAIR(
BINT(2L), 
MAKE_YOUNG_PAIR(
BINT(2L), 
MAKE_YOUNG_PAIR(
BINT(2L), 
MAKE_YOUNG_PAIR(
BINT(3L), 
MAKE_YOUNG_PAIR(
BINT(4L), BNIL)))))))))))))))); 
BGl_vector4921z00zz__unicodez00 = 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BGl_list4922z00zz__unicodez00); 
BGl_symbol4933z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4934z00zz__unicodez00); 
BGl_symbol4942z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4943z00zz__unicodez00); 
BGl_symbol4978z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4979z00zz__unicodez00); 
BGl_symbol4982z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4983z00zz__unicodez00); 
return ( 
BGl_symbol4984z00zz__unicodez00 = 
bstring_to_symbol(BGl_string4985z00zz__unicodez00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
BGl_8bitszd2invzd2zz__unicodez00 = BGl_list4775z00zz__unicodez00; 
BGl_8bitszd2invzd2latinzd215zd2zz__unicodez00 = BGl_list4802z00zz__unicodez00; 
BGl_cp1252z00zz__unicodez00 = BGl_vector4814z00zz__unicodez00; 
return ( 
BGl_cp1252zd2invzd2zz__unicodez00 = BFALSE, BUNSPEC) ;} 

}



/* ucs2-string? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzf3z21zz__unicodez00(obj_t BgL_objz00_3)
{
{ /* Llib/unicode.scm 225 */
return 
UCS2_STRINGP(BgL_objz00_3);} 

}



/* &ucs2-string? */
obj_t BGl_z62ucs2zd2stringzf3z43zz__unicodez00(obj_t BgL_envz00_6101, obj_t BgL_objz00_6102)
{
{ /* Llib/unicode.scm 225 */
return 
BBOOL(
BGl_ucs2zd2stringzf3z21zz__unicodez00(BgL_objz00_6102));} 

}



/* _make-ucs2-string */
obj_t BGl__makezd2ucs2zd2stringz00zz__unicodez00(obj_t BgL_env1124z00_7, obj_t BgL_opt1123z00_6)
{
{ /* Llib/unicode.scm 231 */
{ /* Llib/unicode.scm 231 */
 obj_t BgL_g1125z00_7682;
BgL_g1125z00_7682 = 
VECTOR_REF(BgL_opt1123z00_6,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1123z00_6)) { case 1L : 

{ /* Llib/unicode.scm 232 */
 ucs2_t BgL_fillerz00_7684;
{ /* Llib/unicode.scm 232 */
 int BgL_tmpz00_7946;
BgL_tmpz00_7946 = 
(int)(
(
(unsigned char)(
(char)(((unsigned char)' '))))); 
BgL_fillerz00_7684 = 
BGL_INT_TO_UCS2(BgL_tmpz00_7946); } 
{ /* Llib/unicode.scm 231 */

{ /* Llib/unicode.scm 231 */
 int BgL_kz00_7685;
{ /* Llib/unicode.scm 231 */
 obj_t BgL_tmpz00_7952;
if(
INTEGERP(BgL_g1125z00_7682))
{ /* Llib/unicode.scm 231 */
BgL_tmpz00_7952 = BgL_g1125z00_7682
; }  else 
{ 
 obj_t BgL_auxz00_7955;
BgL_auxz00_7955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(10346L), BGl_string4848z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_g1125z00_7682); 
FAILURE(BgL_auxz00_7955,BFALSE,BFALSE);} 
BgL_kz00_7685 = 
CINT(BgL_tmpz00_7952); } 
return 
make_ucs2_string(BgL_kz00_7685, BgL_fillerz00_7684);} } } break;case 2L : 

{ /* Llib/unicode.scm 231 */
 obj_t BgL_fillerz00_7686;
BgL_fillerz00_7686 = 
VECTOR_REF(BgL_opt1123z00_6,1L); 
{ /* Llib/unicode.scm 231 */

{ /* Llib/unicode.scm 231 */
 int BgL_kz00_7687; ucs2_t BgL_fillerz00_7688;
{ /* Llib/unicode.scm 231 */
 obj_t BgL_tmpz00_7962;
if(
INTEGERP(BgL_g1125z00_7682))
{ /* Llib/unicode.scm 231 */
BgL_tmpz00_7962 = BgL_g1125z00_7682
; }  else 
{ 
 obj_t BgL_auxz00_7965;
BgL_auxz00_7965 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(10346L), BGl_string4848z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_g1125z00_7682); 
FAILURE(BgL_auxz00_7965,BFALSE,BFALSE);} 
BgL_kz00_7687 = 
CINT(BgL_tmpz00_7962); } 
{ /* Llib/unicode.scm 231 */
 obj_t BgL_tmpz00_7970;
if(
UCS2P(BgL_fillerz00_7686))
{ /* Llib/unicode.scm 231 */
BgL_tmpz00_7970 = BgL_fillerz00_7686
; }  else 
{ 
 obj_t BgL_auxz00_7973;
BgL_auxz00_7973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(10346L), BGl_string4848z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_fillerz00_7686); 
FAILURE(BgL_auxz00_7973,BFALSE,BFALSE);} 
BgL_fillerz00_7688 = 
CUCS2(BgL_tmpz00_7970); } 
return 
make_ucs2_string(BgL_kz00_7687, BgL_fillerz00_7688);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4844z00zz__unicodez00, BGl_string4846z00zz__unicodez00, 
BINT(
VECTOR_LENGTH(BgL_opt1123z00_6)));} } } } 

}



/* make-ucs2-string */
BGL_EXPORTED_DEF obj_t BGl_makezd2ucs2zd2stringz00zz__unicodez00(int BgL_kz00_4, ucs2_t BgL_fillerz00_5)
{
{ /* Llib/unicode.scm 231 */
BGL_TAIL return 
make_ucs2_string(BgL_kz00_4, BgL_fillerz00_5);} 

}



/* ucs2-string */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2zz__unicodez00(obj_t BgL_ucs2sz00_8)
{
{ /* Llib/unicode.scm 238 */
BGL_TAIL return 
BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(BgL_ucs2sz00_8);} 

}



/* &ucs2-string */
obj_t BGl_z62ucs2zd2stringzb0zz__unicodez00(obj_t BgL_envz00_6103, obj_t BgL_ucs2sz00_6104)
{
{ /* Llib/unicode.scm 238 */
return 
BGl_ucs2zd2stringzd2zz__unicodez00(BgL_ucs2sz00_6104);} 

}



/* ucs2-string-length */
BGL_EXPORTED_DEF int BGl_ucs2zd2stringzd2lengthz00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_9)
{
{ /* Llib/unicode.scm 244 */
return 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_9);} 

}



/* &ucs2-string-length */
obj_t BGl_z62ucs2zd2stringzd2lengthz62zz__unicodez00(obj_t BgL_envz00_6105, obj_t BgL_ucs2zd2stringzd2_6106)
{
{ /* Llib/unicode.scm 244 */
{ /* Llib/unicode.scm 245 */
 int BgL_tmpz00_7988;
{ /* Llib/unicode.scm 245 */
 obj_t BgL_auxz00_7989;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6106))
{ /* Llib/unicode.scm 245 */
BgL_auxz00_7989 = BgL_ucs2zd2stringzd2_6106
; }  else 
{ 
 obj_t BgL_auxz00_7992;
BgL_auxz00_7992 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11033L), BGl_string4851z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6106); 
FAILURE(BgL_auxz00_7992,BFALSE,BFALSE);} 
BgL_tmpz00_7988 = 
BGl_ucs2zd2stringzd2lengthz00zz__unicodez00(BgL_auxz00_7989); } 
return 
BINT(BgL_tmpz00_7988);} } 

}



/* ucs2-string-ref */
BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2stringzd2refz00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_10, int BgL_kz00_11)
{
{ /* Llib/unicode.scm 250 */
{ /* Llib/unicode.scm 251 */
 bool_t BgL_test5216z00_7998;
{ /* Llib/unicode.scm 251 */
 long BgL_auxz00_8001; long BgL_tmpz00_7999;
BgL_auxz00_8001 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_10)); 
BgL_tmpz00_7999 = 
(long)(BgL_kz00_11); 
BgL_test5216z00_7998 = 
BOUND_CHECK(BgL_tmpz00_7999, BgL_auxz00_8001); } 
if(BgL_test5216z00_7998)
{ /* Llib/unicode.scm 251 */
return 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_10, BgL_kz00_11);}  else 
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1227z00_7689;
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1228z00_7690;
{ /* Llib/unicode.scm 256 */

BgL_arg1228z00_7690 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_10))-1L), 10L); } 
BgL_arg1227z00_7689 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1228z00_7690, BGl_string4854z00zz__unicodez00); } 
{ /* Llib/unicode.scm 253 */
 obj_t BgL_tmpz00_8011;
{ /* Llib/unicode.scm 253 */
 obj_t BgL_aux4422z00_7691;
BgL_aux4422z00_7691 = 
BGl_errorz00zz__errorz00(BGl_symbol4855z00zz__unicodez00, BgL_arg1227z00_7689, 
BINT(BgL_kz00_11)); 
if(
UCS2P(BgL_aux4422z00_7691))
{ /* Llib/unicode.scm 253 */
BgL_tmpz00_8011 = BgL_aux4422z00_7691
; }  else 
{ 
 obj_t BgL_auxz00_8016;
BgL_auxz00_8016 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11452L), BGl_string4856z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_aux4422z00_7691); 
FAILURE(BgL_auxz00_8016,BFALSE,BFALSE);} } 
return 
CUCS2(BgL_tmpz00_8011);} } } } 

}



/* &ucs2-string-ref */
obj_t BGl_z62ucs2zd2stringzd2refz62zz__unicodez00(obj_t BgL_envz00_6107, obj_t BgL_ucs2zd2stringzd2_6108, obj_t BgL_kz00_6109)
{
{ /* Llib/unicode.scm 250 */
{ /* Llib/unicode.scm 251 */
 ucs2_t BgL_tmpz00_8021;
{ /* Llib/unicode.scm 251 */
 int BgL_auxz00_8029; obj_t BgL_auxz00_8022;
{ /* Llib/unicode.scm 251 */
 obj_t BgL_tmpz00_8030;
if(
INTEGERP(BgL_kz00_6109))
{ /* Llib/unicode.scm 251 */
BgL_tmpz00_8030 = BgL_kz00_6109
; }  else 
{ 
 obj_t BgL_auxz00_8033;
BgL_auxz00_8033 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11342L), BGl_string4857z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_kz00_6109); 
FAILURE(BgL_auxz00_8033,BFALSE,BFALSE);} 
BgL_auxz00_8029 = 
CINT(BgL_tmpz00_8030); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6108))
{ /* Llib/unicode.scm 251 */
BgL_auxz00_8022 = BgL_ucs2zd2stringzd2_6108
; }  else 
{ 
 obj_t BgL_auxz00_8025;
BgL_auxz00_8025 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11342L), BGl_string4857z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6108); 
FAILURE(BgL_auxz00_8025,BFALSE,BFALSE);} 
BgL_tmpz00_8021 = 
BGl_ucs2zd2stringzd2refz00zz__unicodez00(BgL_auxz00_8022, BgL_auxz00_8029); } 
return 
BUCS2(BgL_tmpz00_8021);} } 

}



/* ucs2-string-set! */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2setz12z12zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_12, int BgL_kz00_13, ucs2_t BgL_ucs2z00_14)
{
{ /* Llib/unicode.scm 263 */
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5220z00_8040;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8043; long BgL_tmpz00_8041;
BgL_auxz00_8043 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_12)); 
BgL_tmpz00_8041 = 
(long)(BgL_kz00_13); 
BgL_test5220z00_8040 = 
BOUND_CHECK(BgL_tmpz00_8041, BgL_auxz00_8043); } 
if(BgL_test5220z00_8040)
{ /* Llib/unicode.scm 264 */
return 
UCS2_STRING_SET(BgL_ucs2zd2stringzd2_12, BgL_kz00_13, BgL_ucs2z00_14);}  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_7692;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_7693;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_7693 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_12))-1L), 10L); } 
BgL_arg1236z00_7692 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_7693, BGl_string4854z00zz__unicodez00); } 
return 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_7692, 
BINT(BgL_kz00_13));} } } 

}



/* &ucs2-string-set! */
obj_t BGl_z62ucs2zd2stringzd2setz12z70zz__unicodez00(obj_t BgL_envz00_6110, obj_t BgL_ucs2zd2stringzd2_6111, obj_t BgL_kz00_6112, obj_t BgL_ucs2z00_6113)
{
{ /* Llib/unicode.scm 263 */
{ /* Llib/unicode.scm 264 */
 ucs2_t BgL_auxz00_8071; int BgL_auxz00_8062; obj_t BgL_auxz00_8055;
{ /* Llib/unicode.scm 264 */
 obj_t BgL_tmpz00_8072;
if(
UCS2P(BgL_ucs2z00_6113))
{ /* Llib/unicode.scm 264 */
BgL_tmpz00_8072 = BgL_ucs2z00_6113
; }  else 
{ 
 obj_t BgL_auxz00_8075;
BgL_auxz00_8075 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11905L), BGl_string4860z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_ucs2z00_6113); 
FAILURE(BgL_auxz00_8075,BFALSE,BFALSE);} 
BgL_auxz00_8071 = 
CUCS2(BgL_tmpz00_8072); } 
{ /* Llib/unicode.scm 264 */
 obj_t BgL_tmpz00_8063;
if(
INTEGERP(BgL_kz00_6112))
{ /* Llib/unicode.scm 264 */
BgL_tmpz00_8063 = BgL_kz00_6112
; }  else 
{ 
 obj_t BgL_auxz00_8066;
BgL_auxz00_8066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11905L), BGl_string4860z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_kz00_6112); 
FAILURE(BgL_auxz00_8066,BFALSE,BFALSE);} 
BgL_auxz00_8062 = 
CINT(BgL_tmpz00_8063); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6111))
{ /* Llib/unicode.scm 264 */
BgL_auxz00_8055 = BgL_ucs2zd2stringzd2_6111
; }  else 
{ 
 obj_t BgL_auxz00_8058;
BgL_auxz00_8058 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11905L), BGl_string4860z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6111); 
FAILURE(BgL_auxz00_8058,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2setz12z12zz__unicodez00(BgL_auxz00_8055, BgL_auxz00_8062, BgL_auxz00_8071);} } 

}



/* ucs2-string-ref-ur */
BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2stringzd2refzd2urzd2zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_15, int BgL_kz00_16)
{
{ /* Llib/unicode.scm 276 */
return 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_15, BgL_kz00_16);} 

}



/* &ucs2-string-ref-ur */
obj_t BGl_z62ucs2zd2stringzd2refzd2urzb0zz__unicodez00(obj_t BgL_envz00_6114, obj_t BgL_ucs2zd2stringzd2_6115, obj_t BgL_kz00_6116)
{
{ /* Llib/unicode.scm 276 */
{ /* Llib/unicode.scm 277 */
 ucs2_t BgL_tmpz00_8082;
{ /* Llib/unicode.scm 277 */
 int BgL_auxz00_8090; obj_t BgL_auxz00_8083;
{ /* Llib/unicode.scm 277 */
 obj_t BgL_tmpz00_8091;
if(
INTEGERP(BgL_kz00_6116))
{ /* Llib/unicode.scm 277 */
BgL_tmpz00_8091 = BgL_kz00_6116
; }  else 
{ 
 obj_t BgL_auxz00_8094;
BgL_auxz00_8094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(12471L), BGl_string4861z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_kz00_6116); 
FAILURE(BgL_auxz00_8094,BFALSE,BFALSE);} 
BgL_auxz00_8090 = 
CINT(BgL_tmpz00_8091); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6115))
{ /* Llib/unicode.scm 277 */
BgL_auxz00_8083 = BgL_ucs2zd2stringzd2_6115
; }  else 
{ 
 obj_t BgL_auxz00_8086;
BgL_auxz00_8086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(12471L), BGl_string4861z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6115); 
FAILURE(BgL_auxz00_8086,BFALSE,BFALSE);} 
BgL_tmpz00_8082 = 
BGl_ucs2zd2stringzd2refzd2urzd2zz__unicodez00(BgL_auxz00_8083, BgL_auxz00_8090); } 
return 
BUCS2(BgL_tmpz00_8082);} } 

}



/* ucs2-string-set-ur! */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2setzd2urz12zc0zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_17, int BgL_kz00_18, ucs2_t BgL_ucs2z00_19)
{
{ /* Llib/unicode.scm 282 */
return 
UCS2_STRING_SET(BgL_ucs2zd2stringzd2_17, BgL_kz00_18, BgL_ucs2z00_19);} 

}



/* &ucs2-string-set-ur! */
obj_t BGl_z62ucs2zd2stringzd2setzd2urz12za2zz__unicodez00(obj_t BgL_envz00_6117, obj_t BgL_ucs2zd2stringzd2_6118, obj_t BgL_kz00_6119, obj_t BgL_ucs2z00_6120)
{
{ /* Llib/unicode.scm 282 */
{ /* Llib/unicode.scm 283 */
 ucs2_t BgL_auxz00_8118; int BgL_auxz00_8109; obj_t BgL_auxz00_8102;
{ /* Llib/unicode.scm 283 */
 obj_t BgL_tmpz00_8119;
if(
UCS2P(BgL_ucs2z00_6120))
{ /* Llib/unicode.scm 283 */
BgL_tmpz00_8119 = BgL_ucs2z00_6120
; }  else 
{ 
 obj_t BgL_auxz00_8122;
BgL_auxz00_8122 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(12789L), BGl_string4862z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_ucs2z00_6120); 
FAILURE(BgL_auxz00_8122,BFALSE,BFALSE);} 
BgL_auxz00_8118 = 
CUCS2(BgL_tmpz00_8119); } 
{ /* Llib/unicode.scm 283 */
 obj_t BgL_tmpz00_8110;
if(
INTEGERP(BgL_kz00_6119))
{ /* Llib/unicode.scm 283 */
BgL_tmpz00_8110 = BgL_kz00_6119
; }  else 
{ 
 obj_t BgL_auxz00_8113;
BgL_auxz00_8113 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(12789L), BGl_string4862z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_kz00_6119); 
FAILURE(BgL_auxz00_8113,BFALSE,BFALSE);} 
BgL_auxz00_8109 = 
CINT(BgL_tmpz00_8110); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6118))
{ /* Llib/unicode.scm 283 */
BgL_auxz00_8102 = BgL_ucs2zd2stringzd2_6118
; }  else 
{ 
 obj_t BgL_auxz00_8105;
BgL_auxz00_8105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(12789L), BGl_string4862z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6118); 
FAILURE(BgL_auxz00_8105,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2setzd2urz12zc0zz__unicodez00(BgL_auxz00_8102, BgL_auxz00_8109, BgL_auxz00_8118);} } 

}



/* ucs2-string=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd3zf3zf2zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_20, obj_t BgL_ucs2zd2string2zd2_21)
{
{ /* Llib/unicode.scm 288 */
BGL_TAIL return 
ucs2_strcmp(BgL_ucs2zd2string1zd2_20, BgL_ucs2zd2string2zd2_21);} 

}



/* &ucs2-string=? */
obj_t BGl_z62ucs2zd2stringzd3zf3z90zz__unicodez00(obj_t BgL_envz00_6121, obj_t BgL_ucs2zd2string1zd2_6122, obj_t BgL_ucs2zd2string2zd2_6123)
{
{ /* Llib/unicode.scm 288 */
{ /* Llib/unicode.scm 289 */
 bool_t BgL_tmpz00_8129;
{ /* Llib/unicode.scm 289 */
 obj_t BgL_auxz00_8137; obj_t BgL_auxz00_8130;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6123))
{ /* Llib/unicode.scm 289 */
BgL_auxz00_8137 = BgL_ucs2zd2string2zd2_6123
; }  else 
{ 
 obj_t BgL_auxz00_8140;
BgL_auxz00_8140 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(13113L), BGl_string4863z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6123); 
FAILURE(BgL_auxz00_8140,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6122))
{ /* Llib/unicode.scm 289 */
BgL_auxz00_8130 = BgL_ucs2zd2string1zd2_6122
; }  else 
{ 
 obj_t BgL_auxz00_8133;
BgL_auxz00_8133 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(13113L), BGl_string4863z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6122); 
FAILURE(BgL_auxz00_8133,BFALSE,BFALSE);} 
BgL_tmpz00_8129 = 
BGl_ucs2zd2stringzd3zf3zf2zz__unicodez00(BgL_auxz00_8130, BgL_auxz00_8137); } 
return 
BBOOL(BgL_tmpz00_8129);} } 

}



/* ucs2-string-ci=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cizd3zf3z20zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_22, obj_t BgL_ucs2zd2string2zd2_23)
{
{ /* Llib/unicode.scm 294 */
BGL_TAIL return 
ucs2_strcicmp(BgL_ucs2zd2string1zd2_22, BgL_ucs2zd2string2zd2_23);} 

}



/* &ucs2-string-ci=? */
obj_t BGl_z62ucs2zd2stringzd2cizd3zf3z42zz__unicodez00(obj_t BgL_envz00_6124, obj_t BgL_ucs2zd2string1zd2_6125, obj_t BgL_ucs2zd2string2zd2_6126)
{
{ /* Llib/unicode.scm 294 */
{ /* Llib/unicode.scm 295 */
 bool_t BgL_tmpz00_8147;
{ /* Llib/unicode.scm 295 */
 obj_t BgL_auxz00_8155; obj_t BgL_auxz00_8148;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6126))
{ /* Llib/unicode.scm 295 */
BgL_auxz00_8155 = BgL_ucs2zd2string2zd2_6126
; }  else 
{ 
 obj_t BgL_auxz00_8158;
BgL_auxz00_8158 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(13444L), BGl_string4864z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6126); 
FAILURE(BgL_auxz00_8158,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6125))
{ /* Llib/unicode.scm 295 */
BgL_auxz00_8148 = BgL_ucs2zd2string1zd2_6125
; }  else 
{ 
 obj_t BgL_auxz00_8151;
BgL_auxz00_8151 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(13444L), BGl_string4864z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6125); 
FAILURE(BgL_auxz00_8151,BFALSE,BFALSE);} 
BgL_tmpz00_8147 = 
BGl_ucs2zd2stringzd2cizd3zf3z20zz__unicodez00(BgL_auxz00_8148, BgL_auxz00_8155); } 
return 
BBOOL(BgL_tmpz00_8147);} } 

}



/* ucs2-string<? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzc3zf3ze2zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_24, obj_t BgL_ucs2zd2string2zd2_25)
{
{ /* Llib/unicode.scm 300 */
BGL_TAIL return 
ucs2_string_lt(BgL_ucs2zd2string1zd2_24, BgL_ucs2zd2string2zd2_25);} 

}



/* &ucs2-string<? */
obj_t BGl_z62ucs2zd2stringzc3zf3z80zz__unicodez00(obj_t BgL_envz00_6127, obj_t BgL_ucs2zd2string1zd2_6128, obj_t BgL_ucs2zd2string2zd2_6129)
{
{ /* Llib/unicode.scm 300 */
{ /* Llib/unicode.scm 301 */
 bool_t BgL_tmpz00_8165;
{ /* Llib/unicode.scm 301 */
 obj_t BgL_auxz00_8173; obj_t BgL_auxz00_8166;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6129))
{ /* Llib/unicode.scm 301 */
BgL_auxz00_8173 = BgL_ucs2zd2string2zd2_6129
; }  else 
{ 
 obj_t BgL_auxz00_8176;
BgL_auxz00_8176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(13770L), BGl_string4865z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6129); 
FAILURE(BgL_auxz00_8176,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6128))
{ /* Llib/unicode.scm 301 */
BgL_auxz00_8166 = BgL_ucs2zd2string1zd2_6128
; }  else 
{ 
 obj_t BgL_auxz00_8169;
BgL_auxz00_8169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(13770L), BGl_string4865z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6128); 
FAILURE(BgL_auxz00_8169,BFALSE,BFALSE);} 
BgL_tmpz00_8165 = 
BGl_ucs2zd2stringzc3zf3ze2zz__unicodez00(BgL_auxz00_8166, BgL_auxz00_8173); } 
return 
BBOOL(BgL_tmpz00_8165);} } 

}



/* ucs2-string>? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringze3zf3zc2zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_26, obj_t BgL_ucs2zd2string2zd2_27)
{
{ /* Llib/unicode.scm 306 */
BGL_TAIL return 
ucs2_string_gt(BgL_ucs2zd2string1zd2_26, BgL_ucs2zd2string2zd2_27);} 

}



/* &ucs2-string>? */
obj_t BGl_z62ucs2zd2stringze3zf3za0zz__unicodez00(obj_t BgL_envz00_6130, obj_t BgL_ucs2zd2string1zd2_6131, obj_t BgL_ucs2zd2string2zd2_6132)
{
{ /* Llib/unicode.scm 306 */
{ /* Llib/unicode.scm 307 */
 bool_t BgL_tmpz00_8183;
{ /* Llib/unicode.scm 307 */
 obj_t BgL_auxz00_8191; obj_t BgL_auxz00_8184;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6132))
{ /* Llib/unicode.scm 307 */
BgL_auxz00_8191 = BgL_ucs2zd2string2zd2_6132
; }  else 
{ 
 obj_t BgL_auxz00_8194;
BgL_auxz00_8194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(14097L), BGl_string4866z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6132); 
FAILURE(BgL_auxz00_8194,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6131))
{ /* Llib/unicode.scm 307 */
BgL_auxz00_8184 = BgL_ucs2zd2string1zd2_6131
; }  else 
{ 
 obj_t BgL_auxz00_8187;
BgL_auxz00_8187 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(14097L), BGl_string4866z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6131); 
FAILURE(BgL_auxz00_8187,BFALSE,BFALSE);} 
BgL_tmpz00_8183 = 
BGl_ucs2zd2stringze3zf3zc2zz__unicodez00(BgL_auxz00_8184, BgL_auxz00_8191); } 
return 
BBOOL(BgL_tmpz00_8183);} } 

}



/* ucs2-string<=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzc3zd3zf3z31zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_28, obj_t BgL_ucs2zd2string2zd2_29)
{
{ /* Llib/unicode.scm 312 */
BGL_TAIL return 
ucs2_string_le(BgL_ucs2zd2string1zd2_28, BgL_ucs2zd2string2zd2_29);} 

}



/* &ucs2-string<=? */
obj_t BGl_z62ucs2zd2stringzc3zd3zf3z53zz__unicodez00(obj_t BgL_envz00_6133, obj_t BgL_ucs2zd2string1zd2_6134, obj_t BgL_ucs2zd2string2zd2_6135)
{
{ /* Llib/unicode.scm 312 */
{ /* Llib/unicode.scm 313 */
 bool_t BgL_tmpz00_8201;
{ /* Llib/unicode.scm 313 */
 obj_t BgL_auxz00_8209; obj_t BgL_auxz00_8202;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6135))
{ /* Llib/unicode.scm 313 */
BgL_auxz00_8209 = BgL_ucs2zd2string2zd2_6135
; }  else 
{ 
 obj_t BgL_auxz00_8212;
BgL_auxz00_8212 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(14425L), BGl_string4867z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6135); 
FAILURE(BgL_auxz00_8212,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6134))
{ /* Llib/unicode.scm 313 */
BgL_auxz00_8202 = BgL_ucs2zd2string1zd2_6134
; }  else 
{ 
 obj_t BgL_auxz00_8205;
BgL_auxz00_8205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(14425L), BGl_string4867z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6134); 
FAILURE(BgL_auxz00_8205,BFALSE,BFALSE);} 
BgL_tmpz00_8201 = 
BGl_ucs2zd2stringzc3zd3zf3z31zz__unicodez00(BgL_auxz00_8202, BgL_auxz00_8209); } 
return 
BBOOL(BgL_tmpz00_8201);} } 

}



/* ucs2-string>=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringze3zd3zf3z11zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_30, obj_t BgL_ucs2zd2string2zd2_31)
{
{ /* Llib/unicode.scm 318 */
BGL_TAIL return 
ucs2_string_ge(BgL_ucs2zd2string1zd2_30, BgL_ucs2zd2string2zd2_31);} 

}



/* &ucs2-string>=? */
obj_t BGl_z62ucs2zd2stringze3zd3zf3z73zz__unicodez00(obj_t BgL_envz00_6136, obj_t BgL_ucs2zd2string1zd2_6137, obj_t BgL_ucs2zd2string2zd2_6138)
{
{ /* Llib/unicode.scm 318 */
{ /* Llib/unicode.scm 319 */
 bool_t BgL_tmpz00_8219;
{ /* Llib/unicode.scm 319 */
 obj_t BgL_auxz00_8227; obj_t BgL_auxz00_8220;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6138))
{ /* Llib/unicode.scm 319 */
BgL_auxz00_8227 = BgL_ucs2zd2string2zd2_6138
; }  else 
{ 
 obj_t BgL_auxz00_8230;
BgL_auxz00_8230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(14753L), BGl_string4868z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6138); 
FAILURE(BgL_auxz00_8230,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6137))
{ /* Llib/unicode.scm 319 */
BgL_auxz00_8220 = BgL_ucs2zd2string1zd2_6137
; }  else 
{ 
 obj_t BgL_auxz00_8223;
BgL_auxz00_8223 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(14753L), BGl_string4868z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6137); 
FAILURE(BgL_auxz00_8223,BFALSE,BFALSE);} 
BgL_tmpz00_8219 = 
BGl_ucs2zd2stringze3zd3zf3z11zz__unicodez00(BgL_auxz00_8220, BgL_auxz00_8227); } 
return 
BBOOL(BgL_tmpz00_8219);} } 

}



/* ucs2-string-ci<? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cizc3zf3z30zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_32, obj_t BgL_ucs2zd2string2zd2_33)
{
{ /* Llib/unicode.scm 324 */
BGL_TAIL return 
ucs2_string_cilt(BgL_ucs2zd2string1zd2_32, BgL_ucs2zd2string2zd2_33);} 

}



/* &ucs2-string-ci<? */
obj_t BGl_z62ucs2zd2stringzd2cizc3zf3z52zz__unicodez00(obj_t BgL_envz00_6139, obj_t BgL_ucs2zd2string1zd2_6140, obj_t BgL_ucs2zd2string2zd2_6141)
{
{ /* Llib/unicode.scm 324 */
{ /* Llib/unicode.scm 325 */
 bool_t BgL_tmpz00_8237;
{ /* Llib/unicode.scm 325 */
 obj_t BgL_auxz00_8245; obj_t BgL_auxz00_8238;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6141))
{ /* Llib/unicode.scm 325 */
BgL_auxz00_8245 = BgL_ucs2zd2string2zd2_6141
; }  else 
{ 
 obj_t BgL_auxz00_8248;
BgL_auxz00_8248 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(15083L), BGl_string4869z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6141); 
FAILURE(BgL_auxz00_8248,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6140))
{ /* Llib/unicode.scm 325 */
BgL_auxz00_8238 = BgL_ucs2zd2string1zd2_6140
; }  else 
{ 
 obj_t BgL_auxz00_8241;
BgL_auxz00_8241 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(15083L), BGl_string4869z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6140); 
FAILURE(BgL_auxz00_8241,BFALSE,BFALSE);} 
BgL_tmpz00_8237 = 
BGl_ucs2zd2stringzd2cizc3zf3z30zz__unicodez00(BgL_auxz00_8238, BgL_auxz00_8245); } 
return 
BBOOL(BgL_tmpz00_8237);} } 

}



/* ucs2-string-ci>? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cize3zf3z10zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_34, obj_t BgL_ucs2zd2string2zd2_35)
{
{ /* Llib/unicode.scm 330 */
BGL_TAIL return 
ucs2_string_cigt(BgL_ucs2zd2string1zd2_34, BgL_ucs2zd2string2zd2_35);} 

}



/* &ucs2-string-ci>? */
obj_t BGl_z62ucs2zd2stringzd2cize3zf3z72zz__unicodez00(obj_t BgL_envz00_6142, obj_t BgL_ucs2zd2string1zd2_6143, obj_t BgL_ucs2zd2string2zd2_6144)
{
{ /* Llib/unicode.scm 330 */
{ /* Llib/unicode.scm 331 */
 bool_t BgL_tmpz00_8255;
{ /* Llib/unicode.scm 331 */
 obj_t BgL_auxz00_8263; obj_t BgL_auxz00_8256;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6144))
{ /* Llib/unicode.scm 331 */
BgL_auxz00_8263 = BgL_ucs2zd2string2zd2_6144
; }  else 
{ 
 obj_t BgL_auxz00_8266;
BgL_auxz00_8266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(15415L), BGl_string4870z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6144); 
FAILURE(BgL_auxz00_8266,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6143))
{ /* Llib/unicode.scm 331 */
BgL_auxz00_8256 = BgL_ucs2zd2string1zd2_6143
; }  else 
{ 
 obj_t BgL_auxz00_8259;
BgL_auxz00_8259 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(15415L), BGl_string4870z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6143); 
FAILURE(BgL_auxz00_8259,BFALSE,BFALSE);} 
BgL_tmpz00_8255 = 
BGl_ucs2zd2stringzd2cize3zf3z10zz__unicodez00(BgL_auxz00_8256, BgL_auxz00_8263); } 
return 
BBOOL(BgL_tmpz00_8255);} } 

}



/* ucs2-string-ci<=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cizc3zd3zf3ze3zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_36, obj_t BgL_ucs2zd2string2zd2_37)
{
{ /* Llib/unicode.scm 336 */
BGL_TAIL return 
ucs2_string_cile(BgL_ucs2zd2string1zd2_36, BgL_ucs2zd2string2zd2_37);} 

}



/* &ucs2-string-ci<=? */
obj_t BGl_z62ucs2zd2stringzd2cizc3zd3zf3z81zz__unicodez00(obj_t BgL_envz00_6145, obj_t BgL_ucs2zd2string1zd2_6146, obj_t BgL_ucs2zd2string2zd2_6147)
{
{ /* Llib/unicode.scm 336 */
{ /* Llib/unicode.scm 337 */
 bool_t BgL_tmpz00_8273;
{ /* Llib/unicode.scm 337 */
 obj_t BgL_auxz00_8281; obj_t BgL_auxz00_8274;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6147))
{ /* Llib/unicode.scm 337 */
BgL_auxz00_8281 = BgL_ucs2zd2string2zd2_6147
; }  else 
{ 
 obj_t BgL_auxz00_8284;
BgL_auxz00_8284 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(15748L), BGl_string4871z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6147); 
FAILURE(BgL_auxz00_8284,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6146))
{ /* Llib/unicode.scm 337 */
BgL_auxz00_8274 = BgL_ucs2zd2string1zd2_6146
; }  else 
{ 
 obj_t BgL_auxz00_8277;
BgL_auxz00_8277 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(15748L), BGl_string4871z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6146); 
FAILURE(BgL_auxz00_8277,BFALSE,BFALSE);} 
BgL_tmpz00_8273 = 
BGl_ucs2zd2stringzd2cizc3zd3zf3ze3zz__unicodez00(BgL_auxz00_8274, BgL_auxz00_8281); } 
return 
BBOOL(BgL_tmpz00_8273);} } 

}



/* ucs2-string-ci>=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2stringzd2cize3zd3zf3zc3zz__unicodez00(obj_t BgL_ucs2zd2string1zd2_38, obj_t BgL_ucs2zd2string2zd2_39)
{
{ /* Llib/unicode.scm 342 */
BGL_TAIL return 
ucs2_string_cige(BgL_ucs2zd2string1zd2_38, BgL_ucs2zd2string2zd2_39);} 

}



/* &ucs2-string-ci>=? */
obj_t BGl_z62ucs2zd2stringzd2cize3zd3zf3za1zz__unicodez00(obj_t BgL_envz00_6148, obj_t BgL_ucs2zd2string1zd2_6149, obj_t BgL_ucs2zd2string2zd2_6150)
{
{ /* Llib/unicode.scm 342 */
{ /* Llib/unicode.scm 343 */
 bool_t BgL_tmpz00_8291;
{ /* Llib/unicode.scm 343 */
 obj_t BgL_auxz00_8299; obj_t BgL_auxz00_8292;
if(
UCS2_STRINGP(BgL_ucs2zd2string2zd2_6150))
{ /* Llib/unicode.scm 343 */
BgL_auxz00_8299 = BgL_ucs2zd2string2zd2_6150
; }  else 
{ 
 obj_t BgL_auxz00_8302;
BgL_auxz00_8302 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(16081L), BGl_string4872z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string2zd2_6150); 
FAILURE(BgL_auxz00_8302,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_ucs2zd2string1zd2_6149))
{ /* Llib/unicode.scm 343 */
BgL_auxz00_8292 = BgL_ucs2zd2string1zd2_6149
; }  else 
{ 
 obj_t BgL_auxz00_8295;
BgL_auxz00_8295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(16081L), BGl_string4872z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2string1zd2_6149); 
FAILURE(BgL_auxz00_8295,BFALSE,BFALSE);} 
BgL_tmpz00_8291 = 
BGl_ucs2zd2stringzd2cize3zd3zf3zc3zz__unicodez00(BgL_auxz00_8292, BgL_auxz00_8299); } 
return 
BBOOL(BgL_tmpz00_8291);} } 

}



/* subucs2-string */
BGL_EXPORTED_DEF obj_t BGl_subucs2zd2stringzd2zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_40, int BgL_startz00_41, int BgL_endz00_42)
{
{ /* Llib/unicode.scm 348 */
{ /* Llib/unicode.scm 350 */
 bool_t BgL_test5249z00_8308;
if(
(
(long)(BgL_endz00_42)>=
(long)(BgL_startz00_41)))
{ /* Llib/unicode.scm 351 */
 bool_t BgL_test5251z00_8313;
{ /* Llib/unicode.scm 351 */
 long BgL_auxz00_8316; long BgL_tmpz00_8314;
BgL_auxz00_8316 = 
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_40))+1L); 
BgL_tmpz00_8314 = 
(long)(BgL_startz00_41); 
BgL_test5251z00_8313 = 
BOUND_CHECK(BgL_tmpz00_8314, BgL_auxz00_8316); } 
if(BgL_test5251z00_8313)
{ /* Llib/unicode.scm 353 */
 long BgL_auxz00_8323; long BgL_tmpz00_8321;
BgL_auxz00_8323 = 
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_40))+1L); 
BgL_tmpz00_8321 = 
(long)(BgL_endz00_42); 
BgL_test5249z00_8308 = 
BOUND_CHECK(BgL_tmpz00_8321, BgL_auxz00_8323); }  else 
{ /* Llib/unicode.scm 351 */
BgL_test5249z00_8308 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 350 */
BgL_test5249z00_8308 = ((bool_t)0)
; } 
if(BgL_test5249z00_8308)
{ /* Llib/unicode.scm 350 */
BGL_TAIL return 
c_subucs2_string(BgL_ucs2zd2stringzd2_40, BgL_startz00_41, BgL_endz00_42);}  else 
{ /* Llib/unicode.scm 358 */
 obj_t BgL_arg1304z00_7694;
BgL_arg1304z00_7694 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_41), 
BINT(BgL_endz00_42)); 
{ /* Llib/unicode.scm 358 */
 obj_t BgL_aux4480z00_7695;
BgL_aux4480z00_7695 = 
BGl_errorz00zz__errorz00(BGl_string4873z00zz__unicodez00, BGl_string4874z00zz__unicodez00, BgL_arg1304z00_7694); 
if(
UCS2_STRINGP(BgL_aux4480z00_7695))
{ /* Llib/unicode.scm 358 */
return BgL_aux4480z00_7695;}  else 
{ 
 obj_t BgL_auxz00_8335;
BgL_auxz00_8335 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(16715L), BGl_string4873z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_aux4480z00_7695); 
FAILURE(BgL_auxz00_8335,BFALSE,BFALSE);} } } } } 

}



/* &subucs2-string */
obj_t BGl_z62subucs2zd2stringzb0zz__unicodez00(obj_t BgL_envz00_6151, obj_t BgL_ucs2zd2stringzd2_6152, obj_t BgL_startz00_6153, obj_t BgL_endz00_6154)
{
{ /* Llib/unicode.scm 348 */
{ /* Llib/unicode.scm 350 */
 int BgL_auxz00_8355; int BgL_auxz00_8346; obj_t BgL_auxz00_8339;
{ /* Llib/unicode.scm 350 */
 obj_t BgL_tmpz00_8356;
if(
INTEGERP(BgL_endz00_6154))
{ /* Llib/unicode.scm 350 */
BgL_tmpz00_8356 = BgL_endz00_6154
; }  else 
{ 
 obj_t BgL_auxz00_8359;
BgL_auxz00_8359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(16454L), BGl_string4875z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_6154); 
FAILURE(BgL_auxz00_8359,BFALSE,BFALSE);} 
BgL_auxz00_8355 = 
CINT(BgL_tmpz00_8356); } 
{ /* Llib/unicode.scm 350 */
 obj_t BgL_tmpz00_8347;
if(
INTEGERP(BgL_startz00_6153))
{ /* Llib/unicode.scm 350 */
BgL_tmpz00_8347 = BgL_startz00_6153
; }  else 
{ 
 obj_t BgL_auxz00_8350;
BgL_auxz00_8350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(16454L), BGl_string4875z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_6153); 
FAILURE(BgL_auxz00_8350,BFALSE,BFALSE);} 
BgL_auxz00_8346 = 
CINT(BgL_tmpz00_8347); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6152))
{ /* Llib/unicode.scm 350 */
BgL_auxz00_8339 = BgL_ucs2zd2stringzd2_6152
; }  else 
{ 
 obj_t BgL_auxz00_8342;
BgL_auxz00_8342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(16454L), BGl_string4875z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6152); 
FAILURE(BgL_auxz00_8342,BFALSE,BFALSE);} 
return 
BGl_subucs2zd2stringzd2zz__unicodez00(BgL_auxz00_8339, BgL_auxz00_8346, BgL_auxz00_8355);} } 

}



/* subucs2-string-ur */
BGL_EXPORTED_DEF obj_t BGl_subucs2zd2stringzd2urz00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_43, int BgL_startz00_44, int BgL_endz00_45)
{
{ /* Llib/unicode.scm 363 */
BGL_TAIL return 
c_subucs2_string(BgL_ucs2zd2stringzd2_43, BgL_startz00_44, BgL_endz00_45);} 

}



/* &subucs2-string-ur */
obj_t BGl_z62subucs2zd2stringzd2urz62zz__unicodez00(obj_t BgL_envz00_6155, obj_t BgL_ucs2zd2stringzd2_6156, obj_t BgL_startz00_6157, obj_t BgL_endz00_6158)
{
{ /* Llib/unicode.scm 363 */
{ /* Llib/unicode.scm 364 */
 int BgL_auxz00_8382; int BgL_auxz00_8373; obj_t BgL_auxz00_8366;
{ /* Llib/unicode.scm 364 */
 obj_t BgL_tmpz00_8383;
if(
INTEGERP(BgL_endz00_6158))
{ /* Llib/unicode.scm 364 */
BgL_tmpz00_8383 = BgL_endz00_6158
; }  else 
{ 
 obj_t BgL_auxz00_8386;
BgL_auxz00_8386 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17058L), BGl_string4876z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_6158); 
FAILURE(BgL_auxz00_8386,BFALSE,BFALSE);} 
BgL_auxz00_8382 = 
CINT(BgL_tmpz00_8383); } 
{ /* Llib/unicode.scm 364 */
 obj_t BgL_tmpz00_8374;
if(
INTEGERP(BgL_startz00_6157))
{ /* Llib/unicode.scm 364 */
BgL_tmpz00_8374 = BgL_startz00_6157
; }  else 
{ 
 obj_t BgL_auxz00_8377;
BgL_auxz00_8377 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17058L), BGl_string4876z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_6157); 
FAILURE(BgL_auxz00_8377,BFALSE,BFALSE);} 
BgL_auxz00_8373 = 
CINT(BgL_tmpz00_8374); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6156))
{ /* Llib/unicode.scm 364 */
BgL_auxz00_8366 = BgL_ucs2zd2stringzd2_6156
; }  else 
{ 
 obj_t BgL_auxz00_8369;
BgL_auxz00_8369 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17058L), BGl_string4876z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6156); 
FAILURE(BgL_auxz00_8369,BFALSE,BFALSE);} 
return 
BGl_subucs2zd2stringzd2urz00zz__unicodez00(BgL_auxz00_8366, BgL_auxz00_8373, BgL_auxz00_8382);} } 

}



/* ucs2-substring */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2substringzd2zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_46, int BgL_startz00_47, int BgL_endz00_48)
{
{ /* Llib/unicode.scm 369 */
{ /* Llib/unicode.scm 371 */
 bool_t BgL_test5259z00_8392;
if(
(
(long)(BgL_endz00_48)>=
(long)(BgL_startz00_47)))
{ /* Llib/unicode.scm 372 */
 bool_t BgL_test5261z00_8397;
{ /* Llib/unicode.scm 372 */
 long BgL_auxz00_8400; long BgL_tmpz00_8398;
BgL_auxz00_8400 = 
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_46))+1L); 
BgL_tmpz00_8398 = 
(long)(BgL_startz00_47); 
BgL_test5261z00_8397 = 
BOUND_CHECK(BgL_tmpz00_8398, BgL_auxz00_8400); } 
if(BgL_test5261z00_8397)
{ /* Llib/unicode.scm 374 */
 long BgL_auxz00_8407; long BgL_tmpz00_8405;
BgL_auxz00_8407 = 
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_46))+1L); 
BgL_tmpz00_8405 = 
(long)(BgL_endz00_48); 
BgL_test5259z00_8392 = 
BOUND_CHECK(BgL_tmpz00_8405, BgL_auxz00_8407); }  else 
{ /* Llib/unicode.scm 372 */
BgL_test5259z00_8392 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 371 */
BgL_test5259z00_8392 = ((bool_t)0)
; } 
if(BgL_test5259z00_8392)
{ /* Llib/unicode.scm 371 */
BGL_TAIL return 
c_subucs2_string(BgL_ucs2zd2stringzd2_46, BgL_startz00_47, BgL_endz00_48);}  else 
{ /* Llib/unicode.scm 379 */
 obj_t BgL_arg1319z00_7696;
BgL_arg1319z00_7696 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_47), 
BINT(BgL_endz00_48)); 
{ /* Llib/unicode.scm 379 */
 obj_t BgL_aux4490z00_7697;
BgL_aux4490z00_7697 = 
BGl_errorz00zz__errorz00(BGl_string4873z00zz__unicodez00, BGl_string4874z00zz__unicodez00, BgL_arg1319z00_7696); 
if(
UCS2_STRINGP(BgL_aux4490z00_7697))
{ /* Llib/unicode.scm 379 */
return BgL_aux4490z00_7697;}  else 
{ 
 obj_t BgL_auxz00_8419;
BgL_auxz00_8419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17687L), BGl_string4877z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_aux4490z00_7697); 
FAILURE(BgL_auxz00_8419,BFALSE,BFALSE);} } } } } 

}



/* &ucs2-substring */
obj_t BGl_z62ucs2zd2substringzb0zz__unicodez00(obj_t BgL_envz00_6159, obj_t BgL_ucs2zd2stringzd2_6160, obj_t BgL_startz00_6161, obj_t BgL_endz00_6162)
{
{ /* Llib/unicode.scm 369 */
{ /* Llib/unicode.scm 371 */
 int BgL_auxz00_8439; int BgL_auxz00_8430; obj_t BgL_auxz00_8423;
{ /* Llib/unicode.scm 371 */
 obj_t BgL_tmpz00_8440;
if(
INTEGERP(BgL_endz00_6162))
{ /* Llib/unicode.scm 371 */
BgL_tmpz00_8440 = BgL_endz00_6162
; }  else 
{ 
 obj_t BgL_auxz00_8443;
BgL_auxz00_8443 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17426L), BGl_string4878z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_6162); 
FAILURE(BgL_auxz00_8443,BFALSE,BFALSE);} 
BgL_auxz00_8439 = 
CINT(BgL_tmpz00_8440); } 
{ /* Llib/unicode.scm 371 */
 obj_t BgL_tmpz00_8431;
if(
INTEGERP(BgL_startz00_6161))
{ /* Llib/unicode.scm 371 */
BgL_tmpz00_8431 = BgL_startz00_6161
; }  else 
{ 
 obj_t BgL_auxz00_8434;
BgL_auxz00_8434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17426L), BGl_string4878z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_6161); 
FAILURE(BgL_auxz00_8434,BFALSE,BFALSE);} 
BgL_auxz00_8430 = 
CINT(BgL_tmpz00_8431); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6160))
{ /* Llib/unicode.scm 371 */
BgL_auxz00_8423 = BgL_ucs2zd2stringzd2_6160
; }  else 
{ 
 obj_t BgL_auxz00_8426;
BgL_auxz00_8426 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(17426L), BGl_string4878z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6160); 
FAILURE(BgL_auxz00_8426,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2substringzd2zz__unicodez00(BgL_auxz00_8423, BgL_auxz00_8430, BgL_auxz00_8439);} } 

}



/* ucs2-substring-ur */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2substringzd2urz00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_49, int BgL_startz00_50, int BgL_endz00_51)
{
{ /* Llib/unicode.scm 384 */
BGL_TAIL return 
c_subucs2_string(BgL_ucs2zd2stringzd2_49, BgL_startz00_50, BgL_endz00_51);} 

}



/* &ucs2-substring-ur */
obj_t BGl_z62ucs2zd2substringzd2urz62zz__unicodez00(obj_t BgL_envz00_6163, obj_t BgL_ucs2zd2stringzd2_6164, obj_t BgL_startz00_6165, obj_t BgL_endz00_6166)
{
{ /* Llib/unicode.scm 384 */
{ /* Llib/unicode.scm 385 */
 int BgL_auxz00_8466; int BgL_auxz00_8457; obj_t BgL_auxz00_8450;
{ /* Llib/unicode.scm 385 */
 obj_t BgL_tmpz00_8467;
if(
INTEGERP(BgL_endz00_6166))
{ /* Llib/unicode.scm 385 */
BgL_tmpz00_8467 = BgL_endz00_6166
; }  else 
{ 
 obj_t BgL_auxz00_8470;
BgL_auxz00_8470 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18030L), BGl_string4879z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_6166); 
FAILURE(BgL_auxz00_8470,BFALSE,BFALSE);} 
BgL_auxz00_8466 = 
CINT(BgL_tmpz00_8467); } 
{ /* Llib/unicode.scm 385 */
 obj_t BgL_tmpz00_8458;
if(
INTEGERP(BgL_startz00_6165))
{ /* Llib/unicode.scm 385 */
BgL_tmpz00_8458 = BgL_startz00_6165
; }  else 
{ 
 obj_t BgL_auxz00_8461;
BgL_auxz00_8461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18030L), BGl_string4879z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_6165); 
FAILURE(BgL_auxz00_8461,BFALSE,BFALSE);} 
BgL_auxz00_8457 = 
CINT(BgL_tmpz00_8458); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6164))
{ /* Llib/unicode.scm 385 */
BgL_auxz00_8450 = BgL_ucs2zd2stringzd2_6164
; }  else 
{ 
 obj_t BgL_auxz00_8453;
BgL_auxz00_8453 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18030L), BGl_string4879z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6164); 
FAILURE(BgL_auxz00_8453,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2substringzd2urz00zz__unicodez00(BgL_auxz00_8450, BgL_auxz00_8457, BgL_auxz00_8466);} } 

}



/* ucs2-string-append */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2appendz00zz__unicodez00(obj_t BgL_listz00_52)
{
{ /* Llib/unicode.scm 390 */
if(
NULLP(BgL_listz00_52))
{ /* Llib/unicode.scm 137 */
 ucs2_t BgL_fillerz00_1316;
{ /* Llib/unicode.scm 137 */
 int BgL_tmpz00_8478;
BgL_tmpz00_8478 = 
(int)(
(
(unsigned char)(
(char)(((unsigned char)' '))))); 
BgL_fillerz00_1316 = 
BGL_INT_TO_UCS2(BgL_tmpz00_8478); } 
{ /* Llib/unicode.scm 137 */

return 
make_ucs2_string(
(int)(0L), BgL_fillerz00_1316);} }  else 
{ /* Llib/unicode.scm 393 */
 obj_t BgL_aux4500z00_7392;
BgL_aux4500z00_7392 = 
BGl_loopze73ze7zz__unicodez00(BgL_listz00_52); 
if(
UCS2_STRINGP(BgL_aux4500z00_7392))
{ /* Llib/unicode.scm 393 */
return BgL_aux4500z00_7392;}  else 
{ 
 obj_t BgL_auxz00_8489;
BgL_auxz00_8489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18385L), BGl_string4880z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_aux4500z00_7392); 
FAILURE(BgL_auxz00_8489,BFALSE,BFALSE);} } } 

}



/* loop~3 */
obj_t BGl_loopze73ze7zz__unicodez00(obj_t BgL_listz00_1318)
{
{ /* Llib/unicode.scm 393 */
{ /* Llib/unicode.scm 394 */
 bool_t BgL_test5271z00_8493;
{ /* Llib/unicode.scm 394 */
 obj_t BgL_tmpz00_8494;
{ /* Llib/unicode.scm 394 */
 obj_t BgL_pairz00_3585;
if(
PAIRP(BgL_listz00_1318))
{ /* Llib/unicode.scm 394 */
BgL_pairz00_3585 = BgL_listz00_1318; }  else 
{ 
 obj_t BgL_auxz00_8497;
BgL_auxz00_8497 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18428L), BGl_string4881z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_listz00_1318); 
FAILURE(BgL_auxz00_8497,BFALSE,BFALSE);} 
BgL_tmpz00_8494 = 
CDR(BgL_pairz00_3585); } 
BgL_test5271z00_8493 = 
NULLP(BgL_tmpz00_8494); } 
if(BgL_test5271z00_8493)
{ /* Llib/unicode.scm 395 */
 obj_t BgL_pairz00_3586;
if(
PAIRP(BgL_listz00_1318))
{ /* Llib/unicode.scm 395 */
BgL_pairz00_3586 = BgL_listz00_1318; }  else 
{ 
 obj_t BgL_auxz00_8505;
BgL_auxz00_8505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18447L), BGl_string4881z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_listz00_1318); 
FAILURE(BgL_auxz00_8505,BFALSE,BFALSE);} 
return 
CAR(BgL_pairz00_3586);}  else 
{ /* Llib/unicode.scm 396 */
 obj_t BgL_arg1328z00_1322; obj_t BgL_arg1329z00_1323;
{ /* Llib/unicode.scm 396 */
 obj_t BgL_pairz00_3587;
if(
PAIRP(BgL_listz00_1318))
{ /* Llib/unicode.scm 396 */
BgL_pairz00_3587 = BgL_listz00_1318; }  else 
{ 
 obj_t BgL_auxz00_8512;
BgL_auxz00_8512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18487L), BGl_string4881z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_listz00_1318); 
FAILURE(BgL_auxz00_8512,BFALSE,BFALSE);} 
BgL_arg1328z00_1322 = 
CAR(BgL_pairz00_3587); } 
{ /* Llib/unicode.scm 396 */
 obj_t BgL_arg1331z00_1324;
{ /* Llib/unicode.scm 396 */
 obj_t BgL_pairz00_3588;
if(
PAIRP(BgL_listz00_1318))
{ /* Llib/unicode.scm 396 */
BgL_pairz00_3588 = BgL_listz00_1318; }  else 
{ 
 obj_t BgL_auxz00_8519;
BgL_auxz00_8519 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18504L), BGl_string4881z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_listz00_1318); 
FAILURE(BgL_auxz00_8519,BFALSE,BFALSE);} 
BgL_arg1331z00_1324 = 
CDR(BgL_pairz00_3588); } 
BgL_arg1329z00_1323 = 
BGl_loopze73ze7zz__unicodez00(BgL_arg1331z00_1324); } 
{ /* Llib/unicode.scm 396 */
 obj_t BgL_auxz00_8532; obj_t BgL_tmpz00_8525;
if(
UCS2_STRINGP(BgL_arg1329z00_1323))
{ /* Llib/unicode.scm 396 */
BgL_auxz00_8532 = BgL_arg1329z00_1323
; }  else 
{ 
 obj_t BgL_auxz00_8535;
BgL_auxz00_8535 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18509L), BGl_string4881z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_arg1329z00_1323); 
FAILURE(BgL_auxz00_8535,BFALSE,BFALSE);} 
if(
UCS2_STRINGP(BgL_arg1328z00_1322))
{ /* Llib/unicode.scm 396 */
BgL_tmpz00_8525 = BgL_arg1328z00_1322
; }  else 
{ 
 obj_t BgL_auxz00_8528;
BgL_auxz00_8528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18491L), BGl_string4881z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_arg1328z00_1322); 
FAILURE(BgL_auxz00_8528,BFALSE,BFALSE);} 
return 
ucs2_string_append(BgL_tmpz00_8525, BgL_auxz00_8532);} } } } 

}



/* &ucs2-string-append */
obj_t BGl_z62ucs2zd2stringzd2appendz62zz__unicodez00(obj_t BgL_envz00_6167, obj_t BgL_listz00_6168)
{
{ /* Llib/unicode.scm 390 */
return 
BGl_ucs2zd2stringzd2appendz00zz__unicodez00(BgL_listz00_6168);} 

}



/* list->ucs2-string */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(obj_t BgL_listz00_53)
{
{ /* Llib/unicode.scm 401 */
{ /* Llib/unicode.scm 402 */
 long BgL_lenz00_1327;
{ /* Llib/unicode.scm 402 */
 obj_t BgL_auxz00_8541;
{ /* Llib/unicode.scm 402 */
 bool_t BgL_test5278z00_8542;
if(
PAIRP(BgL_listz00_53))
{ /* Llib/unicode.scm 402 */
BgL_test5278z00_8542 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 402 */
BgL_test5278z00_8542 = 
NULLP(BgL_listz00_53)
; } 
if(BgL_test5278z00_8542)
{ /* Llib/unicode.scm 402 */
BgL_auxz00_8541 = BgL_listz00_53
; }  else 
{ 
 obj_t BgL_auxz00_8546;
BgL_auxz00_8546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18798L), BGl_string4883z00zz__unicodez00, BGl_string4884z00zz__unicodez00, BgL_listz00_53); 
FAILURE(BgL_auxz00_8546,BFALSE,BFALSE);} } 
BgL_lenz00_1327 = 
bgl_list_length(BgL_auxz00_8541); } 
{ /* Llib/unicode.scm 402 */
 obj_t BgL_ucs2zd2stringzd2_1328;
{ /* Llib/unicode.scm 137 */
 ucs2_t BgL_fillerz00_1339;
{ /* Llib/unicode.scm 137 */
 int BgL_tmpz00_8551;
BgL_tmpz00_8551 = 
(int)(
(
(unsigned char)(
(char)(((unsigned char)' '))))); 
BgL_fillerz00_1339 = 
BGL_INT_TO_UCS2(BgL_tmpz00_8551); } 
{ /* Llib/unicode.scm 137 */

BgL_ucs2zd2stringzd2_1328 = 
make_ucs2_string(
(int)(BgL_lenz00_1327), BgL_fillerz00_1339); } } 
{ /* Llib/unicode.scm 403 */

{ 
 long BgL_iz00_3615; obj_t BgL_lz00_3616;
BgL_iz00_3615 = 0L; 
BgL_lz00_3616 = BgL_listz00_53; 
BgL_loopz00_3614:
if(
(BgL_iz00_3615==BgL_lenz00_1327))
{ /* Llib/unicode.scm 406 */
return BgL_ucs2zd2stringzd2_1328;}  else 
{ /* Llib/unicode.scm 406 */
{ /* Llib/unicode.scm 409 */
 obj_t BgL_arg1335z00_3620;
{ /* Llib/unicode.scm 409 */
 obj_t BgL_pairz00_3621;
if(
PAIRP(BgL_lz00_3616))
{ /* Llib/unicode.scm 409 */
BgL_pairz00_3621 = BgL_lz00_3616; }  else 
{ 
 obj_t BgL_auxz00_8563;
BgL_auxz00_8563 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18970L), BGl_string4885z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_lz00_3616); 
FAILURE(BgL_auxz00_8563,BFALSE,BFALSE);} 
BgL_arg1335z00_3620 = 
CAR(BgL_pairz00_3621); } 
{ /* Llib/unicode.scm 409 */
 int BgL_kz00_3623; ucs2_t BgL_ucs2z00_3624;
BgL_kz00_3623 = 
(int)(BgL_iz00_3615); 
{ /* Llib/unicode.scm 409 */
 obj_t BgL_tmpz00_8569;
if(
UCS2P(BgL_arg1335z00_3620))
{ /* Llib/unicode.scm 409 */
BgL_tmpz00_8569 = BgL_arg1335z00_3620
; }  else 
{ 
 obj_t BgL_auxz00_8572;
BgL_auxz00_8572 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18971L), BGl_string4885z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_arg1335z00_3620); 
FAILURE(BgL_auxz00_8572,BFALSE,BFALSE);} 
BgL_ucs2z00_3624 = 
CUCS2(BgL_tmpz00_8569); } 
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5283z00_8577;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8580; long BgL_tmpz00_8578;
BgL_auxz00_8580 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_1328)); 
BgL_tmpz00_8578 = 
(long)(BgL_kz00_3623); 
BgL_test5283z00_8577 = 
BOUND_CHECK(BgL_tmpz00_8578, BgL_auxz00_8580); } 
if(BgL_test5283z00_8577)
{ /* Llib/unicode.scm 264 */
UCS2_STRING_SET(BgL_ucs2zd2stringzd2_1328, BgL_kz00_3623, BgL_ucs2z00_3624); }  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_3628;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_3629;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_3629 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_1328))-1L), 10L); } 
BgL_arg1236z00_3628 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_3629, BGl_string4854z00zz__unicodez00); } 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_3628, 
BINT(BgL_kz00_3623)); } } } } 
{ /* Llib/unicode.scm 410 */
 long BgL_arg1336z00_3636; obj_t BgL_arg1337z00_3637;
BgL_arg1336z00_3636 = 
(BgL_iz00_3615+1L); 
{ /* Llib/unicode.scm 410 */
 obj_t BgL_pairz00_3639;
if(
PAIRP(BgL_lz00_3616))
{ /* Llib/unicode.scm 410 */
BgL_pairz00_3639 = BgL_lz00_3616; }  else 
{ 
 obj_t BgL_auxz00_8595;
BgL_auxz00_8595 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(18997L), BGl_string4885z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_lz00_3616); 
FAILURE(BgL_auxz00_8595,BFALSE,BFALSE);} 
BgL_arg1337z00_3637 = 
CDR(BgL_pairz00_3639); } 
{ 
 obj_t BgL_lz00_8601; long BgL_iz00_8600;
BgL_iz00_8600 = BgL_arg1336z00_3636; 
BgL_lz00_8601 = BgL_arg1337z00_3637; 
BgL_lz00_3616 = BgL_lz00_8601; 
BgL_iz00_3615 = BgL_iz00_8600; 
goto BgL_loopz00_3614;} } } } } } } } 

}



/* &list->ucs2-string */
obj_t BGl_z62listzd2ze3ucs2zd2stringz81zz__unicodez00(obj_t BgL_envz00_6169, obj_t BgL_listz00_6170)
{
{ /* Llib/unicode.scm 401 */
return 
BGl_listzd2ze3ucs2zd2stringze3zz__unicodez00(BgL_listz00_6170);} 

}



/* ucs2-string->list */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2ze3listze3zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_54)
{
{ /* Llib/unicode.scm 415 */
{ /* Llib/unicode.scm 416 */
 int BgL_lenz00_1340;
BgL_lenz00_1340 = 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_54); 
{ 
 long BgL_iz00_3659; obj_t BgL_accz00_3660;
BgL_iz00_3659 = 0L; 
BgL_accz00_3660 = BNIL; 
BgL_loopz00_3658:
if(
(BgL_iz00_3659==
(long)(BgL_lenz00_1340)))
{ /* Llib/unicode.scm 419 */
return 
bgl_reverse_bang(BgL_accz00_3660);}  else 
{ /* Llib/unicode.scm 421 */
 long BgL_arg1340z00_3664; obj_t BgL_arg1341z00_3665;
BgL_arg1340z00_3664 = 
(BgL_iz00_3659+1L); 
{ /* Llib/unicode.scm 422 */
 ucs2_t BgL_arg1342z00_3667;
{ /* Llib/unicode.scm 422 */
 ucs2_t BgL_res3361z00_3668;
{ /* Llib/unicode.scm 422 */
 int BgL_kz00_3670;
BgL_kz00_3670 = 
(int)(BgL_iz00_3659); 
{ /* Llib/unicode.scm 251 */
 bool_t BgL_test5286z00_8610;
{ /* Llib/unicode.scm 251 */
 long BgL_auxz00_8613; long BgL_tmpz00_8611;
BgL_auxz00_8613 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_54)); 
BgL_tmpz00_8611 = 
(long)(BgL_kz00_3670); 
BgL_test5286z00_8610 = 
BOUND_CHECK(BgL_tmpz00_8611, BgL_auxz00_8613); } 
if(BgL_test5286z00_8610)
{ /* Llib/unicode.scm 251 */
BgL_res3361z00_3668 = 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_54, BgL_kz00_3670); }  else 
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1227z00_3674;
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1228z00_3675;
{ /* Llib/unicode.scm 256 */

BgL_arg1228z00_3675 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_54))-1L), 10L); } 
BgL_arg1227z00_3674 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1228z00_3675, BGl_string4854z00zz__unicodez00); } 
{ /* Llib/unicode.scm 253 */
 obj_t BgL_tmpz00_8623;
{ /* Llib/unicode.scm 253 */
 obj_t BgL_aux4522z00_7414;
BgL_aux4522z00_7414 = 
BGl_errorz00zz__errorz00(BGl_symbol4855z00zz__unicodez00, BgL_arg1227z00_3674, 
BINT(BgL_kz00_3670)); 
if(
UCS2P(BgL_aux4522z00_7414))
{ /* Llib/unicode.scm 253 */
BgL_tmpz00_8623 = BgL_aux4522z00_7414
; }  else 
{ 
 obj_t BgL_auxz00_8628;
BgL_auxz00_8628 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11452L), BGl_string4885z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_aux4522z00_7414); 
FAILURE(BgL_auxz00_8628,BFALSE,BFALSE);} } 
BgL_res3361z00_3668 = 
CUCS2(BgL_tmpz00_8623); } } } } 
BgL_arg1342z00_3667 = BgL_res3361z00_3668; } 
BgL_arg1341z00_3665 = 
MAKE_YOUNG_PAIR(
BUCS2(BgL_arg1342z00_3667), BgL_accz00_3660); } 
{ 
 obj_t BgL_accz00_8636; long BgL_iz00_8635;
BgL_iz00_8635 = BgL_arg1340z00_3664; 
BgL_accz00_8636 = BgL_arg1341z00_3665; 
BgL_accz00_3660 = BgL_accz00_8636; 
BgL_iz00_3659 = BgL_iz00_8635; 
goto BgL_loopz00_3658;} } } } } 

}



/* &ucs2-string->list */
obj_t BGl_z62ucs2zd2stringzd2ze3listz81zz__unicodez00(obj_t BgL_envz00_6171, obj_t BgL_ucs2zd2stringzd2_6172)
{
{ /* Llib/unicode.scm 415 */
{ /* Llib/unicode.scm 416 */
 obj_t BgL_auxz00_8637;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6172))
{ /* Llib/unicode.scm 416 */
BgL_auxz00_8637 = BgL_ucs2zd2stringzd2_6172
; }  else 
{ 
 obj_t BgL_auxz00_8640;
BgL_auxz00_8640 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(19272L), BGl_string4886z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6172); 
FAILURE(BgL_auxz00_8640,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2ze3listze3zz__unicodez00(BgL_auxz00_8637);} } 

}



/* ucs2-string-copy */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2copyz00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_55)
{
{ /* Llib/unicode.scm 428 */
BGL_TAIL return 
c_ucs2_string_copy(BgL_ucs2zd2stringzd2_55);} 

}



/* &ucs2-string-copy */
obj_t BGl_z62ucs2zd2stringzd2copyz62zz__unicodez00(obj_t BgL_envz00_6173, obj_t BgL_ucs2zd2stringzd2_6174)
{
{ /* Llib/unicode.scm 428 */
{ /* Llib/unicode.scm 429 */
 obj_t BgL_auxz00_8646;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6174))
{ /* Llib/unicode.scm 429 */
BgL_auxz00_8646 = BgL_ucs2zd2stringzd2_6174
; }  else 
{ 
 obj_t BgL_auxz00_8649;
BgL_auxz00_8649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(19747L), BGl_string4887z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6174); 
FAILURE(BgL_auxz00_8649,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2copyz00zz__unicodez00(BgL_auxz00_8646);} } 

}



/* ucs2-string-fill! */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2fillz12z12zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_56, ucs2_t BgL_ucs2z00_57)
{
{ /* Llib/unicode.scm 434 */
{ /* Llib/unicode.scm 435 */
 int BgL_lenz00_1351;
BgL_lenz00_1351 = 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_56); 
{ 
 long BgL_iz00_3701;
BgL_iz00_3701 = 0L; 
BgL_loopz00_3700:
if(
(BgL_iz00_3701==
(long)(BgL_lenz00_1351)))
{ /* Llib/unicode.scm 437 */
return BgL_ucs2zd2stringzd2_56;}  else 
{ /* Llib/unicode.scm 437 */
{ /* Llib/unicode.scm 440 */
 int BgL_kz00_3706;
BgL_kz00_3706 = 
(int)(BgL_iz00_3701); 
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5291z00_8659;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8662; long BgL_tmpz00_8660;
BgL_auxz00_8662 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_56)); 
BgL_tmpz00_8660 = 
(long)(BgL_kz00_3706); 
BgL_test5291z00_8659 = 
BOUND_CHECK(BgL_tmpz00_8660, BgL_auxz00_8662); } 
if(BgL_test5291z00_8659)
{ /* Llib/unicode.scm 264 */
UCS2_STRING_SET(BgL_ucs2zd2stringzd2_56, BgL_kz00_3706, BgL_ucs2z00_57); }  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_3711;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_3712;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_3712 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_56))-1L), 10L); } 
BgL_arg1236z00_3711 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_3712, BGl_string4854z00zz__unicodez00); } 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_3711, 
BINT(BgL_kz00_3706)); } } } 
{ 
 long BgL_iz00_8674;
BgL_iz00_8674 = 
(BgL_iz00_3701+1L); 
BgL_iz00_3701 = BgL_iz00_8674; 
goto BgL_loopz00_3700;} } } } } 

}



/* &ucs2-string-fill! */
obj_t BGl_z62ucs2zd2stringzd2fillz12z70zz__unicodez00(obj_t BgL_envz00_6175, obj_t BgL_ucs2zd2stringzd2_6176, obj_t BgL_ucs2z00_6177)
{
{ /* Llib/unicode.scm 434 */
{ /* Llib/unicode.scm 435 */
 ucs2_t BgL_auxz00_8683; obj_t BgL_auxz00_8676;
{ /* Llib/unicode.scm 435 */
 obj_t BgL_tmpz00_8684;
if(
UCS2P(BgL_ucs2z00_6177))
{ /* Llib/unicode.scm 435 */
BgL_tmpz00_8684 = BgL_ucs2z00_6177
; }  else 
{ 
 obj_t BgL_auxz00_8687;
BgL_auxz00_8687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(20052L), BGl_string4888z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_ucs2z00_6177); 
FAILURE(BgL_auxz00_8687,BFALSE,BFALSE);} 
BgL_auxz00_8683 = 
CUCS2(BgL_tmpz00_8684); } 
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6176))
{ /* Llib/unicode.scm 435 */
BgL_auxz00_8676 = BgL_ucs2zd2stringzd2_6176
; }  else 
{ 
 obj_t BgL_auxz00_8679;
BgL_auxz00_8679 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(20052L), BGl_string4888z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6176); 
FAILURE(BgL_auxz00_8679,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2fillz12z12zz__unicodez00(BgL_auxz00_8676, BgL_auxz00_8683);} } 

}



/* ucs2-string-upcase */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2upcasez00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_58)
{
{ /* Llib/unicode.scm 446 */
{ /* Llib/unicode.scm 447 */
 int BgL_lenz00_1358;
BgL_lenz00_1358 = 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_58); 
{ /* Llib/unicode.scm 447 */
 obj_t BgL_resz00_1359;
{ /* Llib/unicode.scm 137 */
 ucs2_t BgL_fillerz00_1369;
{ /* Llib/unicode.scm 137 */
 int BgL_tmpz00_8694;
BgL_tmpz00_8694 = 
(int)(
(
(unsigned char)(
(char)(((unsigned char)' '))))); 
BgL_fillerz00_1369 = 
BGL_INT_TO_UCS2(BgL_tmpz00_8694); } 
{ /* Llib/unicode.scm 137 */

BgL_resz00_1359 = 
make_ucs2_string(BgL_lenz00_1358, BgL_fillerz00_1369); } } 
{ /* Llib/unicode.scm 448 */

{ 
 long BgL_iz00_1361;
BgL_iz00_1361 = 0L; 
BgL_zc3z04anonymousza31347ze3z87_1362:
if(
(BgL_iz00_1361==
(long)(BgL_lenz00_1358)))
{ /* Llib/unicode.scm 450 */
return BgL_resz00_1359;}  else 
{ /* Llib/unicode.scm 450 */
{ /* Llib/unicode.scm 454 */
 ucs2_t BgL_arg1349z00_1364;
{ /* Llib/unicode.scm 454 */
 ucs2_t BgL_arg1350z00_1365;
{ /* Llib/unicode.scm 454 */
 ucs2_t BgL_res3362z00_3743;
{ /* Llib/unicode.scm 454 */
 int BgL_kz00_3731;
BgL_kz00_3731 = 
(int)(BgL_iz00_1361); 
{ /* Llib/unicode.scm 251 */
 bool_t BgL_test5295z00_8705;
{ /* Llib/unicode.scm 251 */
 long BgL_auxz00_8708; long BgL_tmpz00_8706;
BgL_auxz00_8708 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_58)); 
BgL_tmpz00_8706 = 
(long)(BgL_kz00_3731); 
BgL_test5295z00_8705 = 
BOUND_CHECK(BgL_tmpz00_8706, BgL_auxz00_8708); } 
if(BgL_test5295z00_8705)
{ /* Llib/unicode.scm 251 */
BgL_res3362z00_3743 = 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_58, BgL_kz00_3731); }  else 
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1227z00_3734;
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1228z00_3735;
{ /* Llib/unicode.scm 256 */

BgL_arg1228z00_3735 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_58))-1L), 10L); } 
BgL_arg1227z00_3734 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1228z00_3735, BGl_string4854z00zz__unicodez00); } 
{ /* Llib/unicode.scm 253 */
 obj_t BgL_tmpz00_8718;
{ /* Llib/unicode.scm 253 */
 obj_t BgL_aux4532z00_7424;
BgL_aux4532z00_7424 = 
BGl_errorz00zz__errorz00(BGl_symbol4855z00zz__unicodez00, BgL_arg1227z00_3734, 
BINT(BgL_kz00_3731)); 
if(
UCS2P(BgL_aux4532z00_7424))
{ /* Llib/unicode.scm 253 */
BgL_tmpz00_8718 = BgL_aux4532z00_7424
; }  else 
{ 
 obj_t BgL_auxz00_8723;
BgL_auxz00_8723 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11452L), BGl_string4885z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_aux4532z00_7424); 
FAILURE(BgL_auxz00_8723,BFALSE,BFALSE);} } 
BgL_res3362z00_3743 = 
CUCS2(BgL_tmpz00_8718); } } } } 
BgL_arg1350z00_1365 = BgL_res3362z00_3743; } 
BgL_arg1349z00_1364 = 
ucs2_toupper(BgL_arg1350z00_1365); } 
{ /* Llib/unicode.scm 453 */
 int BgL_kz00_3746;
BgL_kz00_3746 = 
(int)(BgL_iz00_1361); 
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5297z00_8730;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8733; long BgL_tmpz00_8731;
BgL_auxz00_8733 = 
(long)(
UCS2_STRING_LENGTH(BgL_resz00_1359)); 
BgL_tmpz00_8731 = 
(long)(BgL_kz00_3746); 
BgL_test5297z00_8730 = 
BOUND_CHECK(BgL_tmpz00_8731, BgL_auxz00_8733); } 
if(BgL_test5297z00_8730)
{ /* Llib/unicode.scm 264 */
UCS2_STRING_SET(BgL_resz00_1359, BgL_kz00_3746, BgL_arg1349z00_1364); }  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_3750;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_3751;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_3751 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_resz00_1359))-1L), 10L); } 
BgL_arg1236z00_3750 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_3751, BGl_string4854z00zz__unicodez00); } 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_3750, 
BINT(BgL_kz00_3746)); } } } } 
{ 
 long BgL_iz00_8745;
BgL_iz00_8745 = 
(BgL_iz00_1361+1L); 
BgL_iz00_1361 = BgL_iz00_8745; 
goto BgL_zc3z04anonymousza31347ze3z87_1362;} } } } } } } 

}



/* &ucs2-string-upcase */
obj_t BGl_z62ucs2zd2stringzd2upcasez62zz__unicodez00(obj_t BgL_envz00_6178, obj_t BgL_ucs2zd2stringzd2_6179)
{
{ /* Llib/unicode.scm 446 */
{ /* Llib/unicode.scm 447 */
 obj_t BgL_auxz00_8747;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6179))
{ /* Llib/unicode.scm 447 */
BgL_auxz00_8747 = BgL_ucs2zd2stringzd2_6179
; }  else 
{ 
 obj_t BgL_auxz00_8750;
BgL_auxz00_8750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(20502L), BGl_string4889z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6179); 
FAILURE(BgL_auxz00_8750,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2upcasez00zz__unicodez00(BgL_auxz00_8747);} } 

}



/* ucs2-string-downcase */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2downcasez00zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_59)
{
{ /* Llib/unicode.scm 460 */
{ /* Llib/unicode.scm 461 */
 int BgL_lenz00_1370;
BgL_lenz00_1370 = 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_59); 
{ /* Llib/unicode.scm 461 */
 obj_t BgL_resz00_1371;
{ /* Llib/unicode.scm 137 */
 ucs2_t BgL_fillerz00_1381;
{ /* Llib/unicode.scm 137 */
 int BgL_tmpz00_8756;
BgL_tmpz00_8756 = 
(int)(
(
(unsigned char)(
(char)(((unsigned char)' '))))); 
BgL_fillerz00_1381 = 
BGL_INT_TO_UCS2(BgL_tmpz00_8756); } 
{ /* Llib/unicode.scm 137 */

BgL_resz00_1371 = 
make_ucs2_string(BgL_lenz00_1370, BgL_fillerz00_1381); } } 
{ /* Llib/unicode.scm 462 */

{ 
 long BgL_iz00_1373;
BgL_iz00_1373 = 0L; 
BgL_zc3z04anonymousza31352ze3z87_1374:
if(
(BgL_iz00_1373==
(long)(BgL_lenz00_1370)))
{ /* Llib/unicode.scm 464 */
return BgL_resz00_1371;}  else 
{ /* Llib/unicode.scm 464 */
{ /* Llib/unicode.scm 468 */
 ucs2_t BgL_arg1354z00_1376;
{ /* Llib/unicode.scm 468 */
 ucs2_t BgL_arg1356z00_1377;
{ /* Llib/unicode.scm 468 */
 ucs2_t BgL_res3363z00_3782;
{ /* Llib/unicode.scm 468 */
 int BgL_kz00_3770;
BgL_kz00_3770 = 
(int)(BgL_iz00_1373); 
{ /* Llib/unicode.scm 251 */
 bool_t BgL_test5300z00_8767;
{ /* Llib/unicode.scm 251 */
 long BgL_auxz00_8770; long BgL_tmpz00_8768;
BgL_auxz00_8770 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_59)); 
BgL_tmpz00_8768 = 
(long)(BgL_kz00_3770); 
BgL_test5300z00_8767 = 
BOUND_CHECK(BgL_tmpz00_8768, BgL_auxz00_8770); } 
if(BgL_test5300z00_8767)
{ /* Llib/unicode.scm 251 */
BgL_res3363z00_3782 = 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_59, BgL_kz00_3770); }  else 
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1227z00_3773;
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1228z00_3774;
{ /* Llib/unicode.scm 256 */

BgL_arg1228z00_3774 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_59))-1L), 10L); } 
BgL_arg1227z00_3773 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1228z00_3774, BGl_string4854z00zz__unicodez00); } 
{ /* Llib/unicode.scm 253 */
 obj_t BgL_tmpz00_8780;
{ /* Llib/unicode.scm 253 */
 obj_t BgL_aux4536z00_7428;
BgL_aux4536z00_7428 = 
BGl_errorz00zz__errorz00(BGl_symbol4855z00zz__unicodez00, BgL_arg1227z00_3773, 
BINT(BgL_kz00_3770)); 
if(
UCS2P(BgL_aux4536z00_7428))
{ /* Llib/unicode.scm 253 */
BgL_tmpz00_8780 = BgL_aux4536z00_7428
; }  else 
{ 
 obj_t BgL_auxz00_8785;
BgL_auxz00_8785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11452L), BGl_string4885z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_aux4536z00_7428); 
FAILURE(BgL_auxz00_8785,BFALSE,BFALSE);} } 
BgL_res3363z00_3782 = 
CUCS2(BgL_tmpz00_8780); } } } } 
BgL_arg1356z00_1377 = BgL_res3363z00_3782; } 
BgL_arg1354z00_1376 = 
ucs2_tolower(BgL_arg1356z00_1377); } 
{ /* Llib/unicode.scm 467 */
 int BgL_kz00_3785;
BgL_kz00_3785 = 
(int)(BgL_iz00_1373); 
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5302z00_8792;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8795; long BgL_tmpz00_8793;
BgL_auxz00_8795 = 
(long)(
UCS2_STRING_LENGTH(BgL_resz00_1371)); 
BgL_tmpz00_8793 = 
(long)(BgL_kz00_3785); 
BgL_test5302z00_8792 = 
BOUND_CHECK(BgL_tmpz00_8793, BgL_auxz00_8795); } 
if(BgL_test5302z00_8792)
{ /* Llib/unicode.scm 264 */
UCS2_STRING_SET(BgL_resz00_1371, BgL_kz00_3785, BgL_arg1354z00_1376); }  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_3789;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_3790;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_3790 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_resz00_1371))-1L), 10L); } 
BgL_arg1236z00_3789 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_3790, BGl_string4854z00zz__unicodez00); } 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_3789, 
BINT(BgL_kz00_3785)); } } } } 
{ 
 long BgL_iz00_8807;
BgL_iz00_8807 = 
(BgL_iz00_1373+1L); 
BgL_iz00_1373 = BgL_iz00_8807; 
goto BgL_zc3z04anonymousza31352ze3z87_1374;} } } } } } } 

}



/* &ucs2-string-downcase */
obj_t BGl_z62ucs2zd2stringzd2downcasez62zz__unicodez00(obj_t BgL_envz00_6180, obj_t BgL_ucs2zd2stringzd2_6181)
{
{ /* Llib/unicode.scm 460 */
{ /* Llib/unicode.scm 461 */
 obj_t BgL_auxz00_8809;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6181))
{ /* Llib/unicode.scm 461 */
BgL_auxz00_8809 = BgL_ucs2zd2stringzd2_6181
; }  else 
{ 
 obj_t BgL_auxz00_8812;
BgL_auxz00_8812 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(21017L), BGl_string4890z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6181); 
FAILURE(BgL_auxz00_8812,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2downcasez00zz__unicodez00(BgL_auxz00_8809);} } 

}



/* ucs2-string-upcase! */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2upcasez12z12zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_60)
{
{ /* Llib/unicode.scm 474 */
{ /* Llib/unicode.scm 475 */
 int BgL_lenz00_1382;
BgL_lenz00_1382 = 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60); 
{ /* Llib/unicode.scm 476 */

{ 
 long BgL_iz00_1385;
BgL_iz00_1385 = 0L; 
BgL_zc3z04anonymousza31358ze3z87_1386:
if(
(BgL_iz00_1385==
(long)(BgL_lenz00_1382)))
{ /* Llib/unicode.scm 478 */
return BgL_ucs2zd2stringzd2_60;}  else 
{ /* Llib/unicode.scm 478 */
{ /* Llib/unicode.scm 482 */
 ucs2_t BgL_arg1360z00_1388;
{ /* Llib/unicode.scm 482 */
 ucs2_t BgL_arg1361z00_1389;
{ /* Llib/unicode.scm 482 */
 ucs2_t BgL_res3364z00_3815;
{ /* Llib/unicode.scm 482 */
 int BgL_kz00_3803;
BgL_kz00_3803 = 
(int)(BgL_iz00_1385); 
{ /* Llib/unicode.scm 251 */
 bool_t BgL_test5305z00_8822;
{ /* Llib/unicode.scm 251 */
 long BgL_auxz00_8825; long BgL_tmpz00_8823;
BgL_auxz00_8825 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60)); 
BgL_tmpz00_8823 = 
(long)(BgL_kz00_3803); 
BgL_test5305z00_8822 = 
BOUND_CHECK(BgL_tmpz00_8823, BgL_auxz00_8825); } 
if(BgL_test5305z00_8822)
{ /* Llib/unicode.scm 251 */
BgL_res3364z00_3815 = 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_60, BgL_kz00_3803); }  else 
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1227z00_3806;
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1228z00_3807;
{ /* Llib/unicode.scm 256 */

BgL_arg1228z00_3807 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60))-1L), 10L); } 
BgL_arg1227z00_3806 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1228z00_3807, BGl_string4854z00zz__unicodez00); } 
{ /* Llib/unicode.scm 253 */
 obj_t BgL_tmpz00_8835;
{ /* Llib/unicode.scm 253 */
 obj_t BgL_aux4540z00_7432;
BgL_aux4540z00_7432 = 
BGl_errorz00zz__errorz00(BGl_symbol4855z00zz__unicodez00, BgL_arg1227z00_3806, 
BINT(BgL_kz00_3803)); 
if(
UCS2P(BgL_aux4540z00_7432))
{ /* Llib/unicode.scm 253 */
BgL_tmpz00_8835 = BgL_aux4540z00_7432
; }  else 
{ 
 obj_t BgL_auxz00_8840;
BgL_auxz00_8840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11452L), BGl_string4885z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_aux4540z00_7432); 
FAILURE(BgL_auxz00_8840,BFALSE,BFALSE);} } 
BgL_res3364z00_3815 = 
CUCS2(BgL_tmpz00_8835); } } } } 
BgL_arg1361z00_1389 = BgL_res3364z00_3815; } 
BgL_arg1360z00_1388 = 
ucs2_toupper(BgL_arg1361z00_1389); } 
{ /* Llib/unicode.scm 481 */
 int BgL_kz00_3818;
BgL_kz00_3818 = 
(int)(BgL_iz00_1385); 
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5307z00_8847;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8850; long BgL_tmpz00_8848;
BgL_auxz00_8850 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60)); 
BgL_tmpz00_8848 = 
(long)(BgL_kz00_3818); 
BgL_test5307z00_8847 = 
BOUND_CHECK(BgL_tmpz00_8848, BgL_auxz00_8850); } 
if(BgL_test5307z00_8847)
{ /* Llib/unicode.scm 264 */
UCS2_STRING_SET(BgL_ucs2zd2stringzd2_60, BgL_kz00_3818, BgL_arg1360z00_1388); }  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_3822;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_3823;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_3823 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_60))-1L), 10L); } 
BgL_arg1236z00_3822 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_3823, BGl_string4854z00zz__unicodez00); } 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_3822, 
BINT(BgL_kz00_3818)); } } } } 
{ 
 long BgL_iz00_8862;
BgL_iz00_8862 = 
(BgL_iz00_1385+1L); 
BgL_iz00_1385 = BgL_iz00_8862; 
goto BgL_zc3z04anonymousza31358ze3z87_1386;} } } } } } 

}



/* &ucs2-string-upcase! */
obj_t BGl_z62ucs2zd2stringzd2upcasez12z70zz__unicodez00(obj_t BgL_envz00_6182, obj_t BgL_ucs2zd2stringzd2_6183)
{
{ /* Llib/unicode.scm 474 */
{ /* Llib/unicode.scm 475 */
 obj_t BgL_auxz00_8864;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6183))
{ /* Llib/unicode.scm 475 */
BgL_auxz00_8864 = BgL_ucs2zd2stringzd2_6183
; }  else 
{ 
 obj_t BgL_auxz00_8867;
BgL_auxz00_8867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(21533L), BGl_string4891z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6183); 
FAILURE(BgL_auxz00_8867,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2upcasez12z12zz__unicodez00(BgL_auxz00_8864);} } 

}



/* ucs2-string-downcase! */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2downcasez12z12zz__unicodez00(obj_t BgL_ucs2zd2stringzd2_61)
{
{ /* Llib/unicode.scm 488 */
{ /* Llib/unicode.scm 489 */
 int BgL_lenz00_1392;
BgL_lenz00_1392 = 
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61); 
{ /* Llib/unicode.scm 490 */

{ 
 long BgL_iz00_1395;
BgL_iz00_1395 = 0L; 
BgL_zc3z04anonymousza31363ze3z87_1396:
if(
(BgL_iz00_1395==
(long)(BgL_lenz00_1392)))
{ /* Llib/unicode.scm 492 */
return BgL_ucs2zd2stringzd2_61;}  else 
{ /* Llib/unicode.scm 492 */
{ /* Llib/unicode.scm 496 */
 ucs2_t BgL_arg1365z00_1398;
{ /* Llib/unicode.scm 496 */
 ucs2_t BgL_arg1366z00_1399;
{ /* Llib/unicode.scm 496 */
 ucs2_t BgL_res3365z00_3848;
{ /* Llib/unicode.scm 496 */
 int BgL_kz00_3836;
BgL_kz00_3836 = 
(int)(BgL_iz00_1395); 
{ /* Llib/unicode.scm 251 */
 bool_t BgL_test5310z00_8877;
{ /* Llib/unicode.scm 251 */
 long BgL_auxz00_8880; long BgL_tmpz00_8878;
BgL_auxz00_8880 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61)); 
BgL_tmpz00_8878 = 
(long)(BgL_kz00_3836); 
BgL_test5310z00_8877 = 
BOUND_CHECK(BgL_tmpz00_8878, BgL_auxz00_8880); } 
if(BgL_test5310z00_8877)
{ /* Llib/unicode.scm 251 */
BgL_res3365z00_3848 = 
UCS2_STRING_REF(BgL_ucs2zd2stringzd2_61, BgL_kz00_3836); }  else 
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1227z00_3839;
{ /* Llib/unicode.scm 256 */
 obj_t BgL_arg1228z00_3840;
{ /* Llib/unicode.scm 256 */

BgL_arg1228z00_3840 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61))-1L), 10L); } 
BgL_arg1227z00_3839 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1228z00_3840, BGl_string4854z00zz__unicodez00); } 
{ /* Llib/unicode.scm 253 */
 obj_t BgL_tmpz00_8890;
{ /* Llib/unicode.scm 253 */
 obj_t BgL_aux4544z00_7436;
BgL_aux4544z00_7436 = 
BGl_errorz00zz__errorz00(BGl_symbol4855z00zz__unicodez00, BgL_arg1227z00_3839, 
BINT(BgL_kz00_3836)); 
if(
UCS2P(BgL_aux4544z00_7436))
{ /* Llib/unicode.scm 253 */
BgL_tmpz00_8890 = BgL_aux4544z00_7436
; }  else 
{ 
 obj_t BgL_auxz00_8895;
BgL_auxz00_8895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(11452L), BGl_string4885z00zz__unicodez00, BGl_string4850z00zz__unicodez00, BgL_aux4544z00_7436); 
FAILURE(BgL_auxz00_8895,BFALSE,BFALSE);} } 
BgL_res3365z00_3848 = 
CUCS2(BgL_tmpz00_8890); } } } } 
BgL_arg1366z00_1399 = BgL_res3365z00_3848; } 
BgL_arg1365z00_1398 = 
ucs2_tolower(BgL_arg1366z00_1399); } 
{ /* Llib/unicode.scm 495 */
 int BgL_kz00_3851;
BgL_kz00_3851 = 
(int)(BgL_iz00_1395); 
{ /* Llib/unicode.scm 264 */
 bool_t BgL_test5312z00_8902;
{ /* Llib/unicode.scm 264 */
 long BgL_auxz00_8905; long BgL_tmpz00_8903;
BgL_auxz00_8905 = 
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61)); 
BgL_tmpz00_8903 = 
(long)(BgL_kz00_3851); 
BgL_test5312z00_8902 = 
BOUND_CHECK(BgL_tmpz00_8903, BgL_auxz00_8905); } 
if(BgL_test5312z00_8902)
{ /* Llib/unicode.scm 264 */
UCS2_STRING_SET(BgL_ucs2zd2stringzd2_61, BgL_kz00_3851, BgL_arg1365z00_1398); }  else 
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1236z00_3855;
{ /* Llib/unicode.scm 269 */
 obj_t BgL_arg1238z00_3856;
{ /* Llib/unicode.scm 269 */

BgL_arg1238z00_3856 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_ucs2zd2stringzd2_61))-1L), 10L); } 
BgL_arg1236z00_3855 = 
string_append_3(BGl_string4853z00zz__unicodez00, BgL_arg1238z00_3856, BGl_string4854z00zz__unicodez00); } 
BGl_errorz00zz__errorz00(BGl_symbol4858z00zz__unicodez00, BgL_arg1236z00_3855, 
BINT(BgL_kz00_3851)); } } } } 
{ 
 long BgL_iz00_8917;
BgL_iz00_8917 = 
(BgL_iz00_1395+1L); 
BgL_iz00_1395 = BgL_iz00_8917; 
goto BgL_zc3z04anonymousza31363ze3z87_1396;} } } } } } 

}



/* &ucs2-string-downcase! */
obj_t BGl_z62ucs2zd2stringzd2downcasez12z70zz__unicodez00(obj_t BgL_envz00_6184, obj_t BgL_ucs2zd2stringzd2_6185)
{
{ /* Llib/unicode.scm 488 */
{ /* Llib/unicode.scm 489 */
 obj_t BgL_auxz00_8919;
if(
UCS2_STRINGP(BgL_ucs2zd2stringzd2_6185))
{ /* Llib/unicode.scm 489 */
BgL_auxz00_8919 = BgL_ucs2zd2stringzd2_6185
; }  else 
{ 
 obj_t BgL_auxz00_8922;
BgL_auxz00_8922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(22038L), BGl_string4892z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2zd2stringzd2_6185); 
FAILURE(BgL_auxz00_8922,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2downcasez12z12zz__unicodez00(BgL_auxz00_8919);} } 

}



/* ucs2-string->utf8-string */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2ze3utf8zd2stringz31zz__unicodez00(obj_t BgL_ucs2z00_62)
{
{ /* Llib/unicode.scm 502 */
BGL_TAIL return 
ucs2_string_to_utf8_string(BgL_ucs2z00_62);} 

}



/* &ucs2-string->utf8-string */
obj_t BGl_z62ucs2zd2stringzd2ze3utf8zd2stringz53zz__unicodez00(obj_t BgL_envz00_6186, obj_t BgL_ucs2z00_6187)
{
{ /* Llib/unicode.scm 502 */
{ /* Llib/unicode.scm 503 */
 obj_t BgL_auxz00_8928;
if(
UCS2_STRINGP(BgL_ucs2z00_6187))
{ /* Llib/unicode.scm 503 */
BgL_auxz00_8928 = BgL_ucs2z00_6187
; }  else 
{ 
 obj_t BgL_auxz00_8931;
BgL_auxz00_8931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(22554L), BGl_string4893z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_ucs2z00_6187); 
FAILURE(BgL_auxz00_8931,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2ze3utf8zd2stringz31zz__unicodez00(BgL_auxz00_8928);} } 

}



/* inverse-utf8-table */
BGL_EXPORTED_DEF obj_t BGl_inversezd2utf8zd2tablez00zz__unicodez00(obj_t BgL_tablez00_63)
{
{ /* Llib/unicode.scm 611 */
{ 
 long BgL_iz00_1407; obj_t BgL_resz00_1408;
BgL_iz00_1407 = 0L; 
BgL_resz00_1408 = BNIL; 
BgL_zc3z04anonymousza31368ze3z87_1409:
if(
(BgL_iz00_1407==
VECTOR_LENGTH(BgL_tablez00_63)))
{ /* Llib/unicode.scm 636 */
return BgL_resz00_1408;}  else 
{ /* Llib/unicode.scm 638 */
 obj_t BgL_sz00_1411;
{ /* Llib/unicode.scm 638 */
 bool_t BgL_test5316z00_8939;
{ /* Llib/unicode.scm 638 */
 long BgL_tmpz00_8940;
BgL_tmpz00_8940 = 
VECTOR_LENGTH(BgL_tablez00_63); 
BgL_test5316z00_8939 = 
BOUND_CHECK(BgL_iz00_1407, BgL_tmpz00_8940); } 
if(BgL_test5316z00_8939)
{ /* Llib/unicode.scm 638 */
BgL_sz00_1411 = 
VECTOR_REF(BgL_tablez00_63,BgL_iz00_1407); }  else 
{ 
 obj_t BgL_auxz00_8944;
BgL_auxz00_8944 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(26084L), BGl_string4894z00zz__unicodez00, BgL_tablez00_63, 
(int)(
VECTOR_LENGTH(BgL_tablez00_63)), 
(int)(BgL_iz00_1407)); 
FAILURE(BgL_auxz00_8944,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 639 */
 bool_t BgL_test5317z00_8951;
{ /* Llib/unicode.scm 639 */
 long BgL_tmpz00_8952;
{ /* Llib/unicode.scm 639 */
 obj_t BgL_stringz00_3886;
if(
STRINGP(BgL_sz00_1411))
{ /* Llib/unicode.scm 639 */
BgL_stringz00_3886 = BgL_sz00_1411; }  else 
{ 
 obj_t BgL_auxz00_8955;
BgL_auxz00_8955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(26118L), BGl_string4885z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_sz00_1411); 
FAILURE(BgL_auxz00_8955,BFALSE,BFALSE);} 
BgL_tmpz00_8952 = 
STRING_LENGTH(BgL_stringz00_3886); } 
BgL_test5317z00_8951 = 
(BgL_tmpz00_8952>0L); } 
if(BgL_test5317z00_8951)
{ 
 obj_t BgL_resz00_8963; long BgL_iz00_8961;
BgL_iz00_8961 = 
(BgL_iz00_1407+1L); 
BgL_resz00_8963 = 
BGl_addzd2tablezd2entryze70ze7zz__unicodez00(BgL_resz00_1408, BgL_sz00_1411, 
(128L+BgL_iz00_1407)); 
BgL_resz00_1408 = BgL_resz00_8963; 
BgL_iz00_1407 = BgL_iz00_8961; 
goto BgL_zc3z04anonymousza31368ze3z87_1409;}  else 
{ 
 long BgL_iz00_8966;
BgL_iz00_8966 = 
(BgL_iz00_1407+1L); 
BgL_iz00_1407 = BgL_iz00_8966; 
goto BgL_zc3z04anonymousza31368ze3z87_1409;} } } } } 

}



/* loop~1 */
obj_t BGl_loopze71ze7zz__unicodez00(long BgL_lenz00_6271, long BgL_charz00_6270, obj_t BgL_sz00_6269, long BgL_iz00_1425)
{
{ /* Llib/unicode.scm 615 */
if(
(BgL_lenz00_6271==BgL_iz00_1425))
{ /* Llib/unicode.scm 616 */
return 
BCHAR(
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_charz00_6270));}  else 
{ /* Llib/unicode.scm 618 */
 obj_t BgL_arg1382z00_1428;
{ /* Llib/unicode.scm 618 */
 long BgL_arg1384z00_1430; obj_t BgL_arg1387z00_1431;
{ /* Llib/unicode.scm 618 */
 unsigned char BgL_tmpz00_8972;
{ /* Llib/unicode.scm 618 */
 obj_t BgL_stringz00_3868;
if(
STRINGP(BgL_sz00_6269))
{ /* Llib/unicode.scm 618 */
BgL_stringz00_3868 = BgL_sz00_6269; }  else 
{ 
 obj_t BgL_auxz00_8975;
BgL_auxz00_8975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25620L), BGl_string4896z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_sz00_6269); 
FAILURE(BgL_auxz00_8975,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 618 */
 long BgL_l3386z00_6278;
BgL_l3386z00_6278 = 
STRING_LENGTH(BgL_stringz00_3868); 
if(
BOUND_CHECK(BgL_iz00_1425, BgL_l3386z00_6278))
{ /* Llib/unicode.scm 618 */
BgL_tmpz00_8972 = 
STRING_REF(BgL_stringz00_3868, BgL_iz00_1425)
; }  else 
{ 
 obj_t BgL_auxz00_8983;
BgL_auxz00_8983 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25608L), BGl_string4897z00zz__unicodez00, BgL_stringz00_3868, 
(int)(BgL_l3386z00_6278), 
(int)(BgL_iz00_1425)); 
FAILURE(BgL_auxz00_8983,BFALSE,BFALSE);} } } 
BgL_arg1384z00_1430 = 
(BgL_tmpz00_8972); } 
BgL_arg1387z00_1431 = 
BGl_loopze71ze7zz__unicodez00(BgL_lenz00_6271, BgL_charz00_6270, BgL_sz00_6269, 
(BgL_iz00_1425+1L)); 
BgL_arg1382z00_1428 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1384z00_1430), BgL_arg1387z00_1431); } 
{ /* Llib/unicode.scm 618 */
 obj_t BgL_list1383z00_1429;
BgL_list1383z00_1429 = 
MAKE_YOUNG_PAIR(BgL_arg1382z00_1428, BNIL); 
return BgL_list1383z00_1429;} } } 

}



/* make-table-entry~0 */
obj_t BGl_makezd2tablezd2entryze70ze7zz__unicodez00(obj_t BgL_sz00_1420, long BgL_charz00_1421)
{
{ /* Llib/unicode.scm 618 */
{ /* Llib/unicode.scm 614 */
 long BgL_lenz00_1423;
{ /* Llib/unicode.scm 614 */
 obj_t BgL_stringz00_3865;
if(
STRINGP(BgL_sz00_1420))
{ /* Llib/unicode.scm 614 */
BgL_stringz00_3865 = BgL_sz00_1420; }  else 
{ 
 obj_t BgL_auxz00_8997;
BgL_auxz00_8997 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25495L), BGl_string4898z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_sz00_1420); 
FAILURE(BgL_auxz00_8997,BFALSE,BFALSE);} 
BgL_lenz00_1423 = 
STRING_LENGTH(BgL_stringz00_3865); } 
return 
BGl_loopze71ze7zz__unicodez00(BgL_lenz00_1423, BgL_charz00_1421, BgL_sz00_1420, 0L);} } 

}



/* loop~2 */
obj_t BGl_loopze72ze7zz__unicodez00(obj_t BgL_ez00_1441, obj_t BgL_tablez00_1442)
{
{ /* Llib/unicode.scm 621 */
if(
NULLP(BgL_ez00_1441))
{ /* Llib/unicode.scm 623 */
return BgL_tablez00_1442;}  else 
{ /* Llib/unicode.scm 625 */
 obj_t BgL_nz00_1445;
{ /* Llib/unicode.scm 625 */
 obj_t BgL_pairz00_3874;
if(
PAIRP(BgL_ez00_1441))
{ /* Llib/unicode.scm 625 */
BgL_pairz00_3874 = BgL_ez00_1441; }  else 
{ 
 obj_t BgL_auxz00_9007;
BgL_auxz00_9007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25815L), BGl_string4899z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_ez00_1441); 
FAILURE(BgL_auxz00_9007,BFALSE,BFALSE);} 
BgL_nz00_1445 = 
CAR(BgL_pairz00_3874); } 
{ /* Llib/unicode.scm 625 */
 obj_t BgL_oz00_1446;
{ /* Llib/unicode.scm 626 */
 obj_t BgL_auxz00_9012;
{ /* Llib/unicode.scm 626 */
 bool_t BgL_test5325z00_9013;
if(
PAIRP(BgL_tablez00_1442))
{ /* Llib/unicode.scm 626 */
BgL_test5325z00_9013 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 626 */
BgL_test5325z00_9013 = 
NULLP(BgL_tablez00_1442)
; } 
if(BgL_test5325z00_9013)
{ /* Llib/unicode.scm 626 */
BgL_auxz00_9012 = BgL_tablez00_1442
; }  else 
{ 
 obj_t BgL_auxz00_9017;
BgL_auxz00_9017 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25836L), BGl_string4899z00zz__unicodez00, BGl_string4884z00zz__unicodez00, BgL_tablez00_1442); 
FAILURE(BgL_auxz00_9017,BFALSE,BFALSE);} } 
BgL_oz00_1446 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_nz00_1445, BgL_auxz00_9012); } 
{ /* Llib/unicode.scm 626 */

if(
CBOOL(BgL_oz00_1446))
{ /* Llib/unicode.scm 628 */
 obj_t BgL_stz00_1447;
{ /* Llib/unicode.scm 628 */
 obj_t BgL_pairz00_3875;
if(
PAIRP(BgL_oz00_1446))
{ /* Llib/unicode.scm 628 */
BgL_pairz00_3875 = BgL_oz00_1446; }  else 
{ 
 obj_t BgL_auxz00_9026;
BgL_auxz00_9026 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25874L), BGl_string4899z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_oz00_1446); 
FAILURE(BgL_auxz00_9026,BFALSE,BFALSE);} 
BgL_stz00_1447 = 
CDR(BgL_pairz00_3875); } 
{ /* Llib/unicode.scm 629 */
 obj_t BgL_arg1393z00_1448;
{ /* Llib/unicode.scm 629 */
 obj_t BgL_arg1394z00_1449;
{ /* Llib/unicode.scm 629 */
 obj_t BgL_pairz00_3876;
if(
PAIRP(BgL_ez00_1441))
{ /* Llib/unicode.scm 629 */
BgL_pairz00_3876 = BgL_ez00_1441; }  else 
{ 
 obj_t BgL_auxz00_9033;
BgL_auxz00_9033 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25912L), BGl_string4899z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_ez00_1441); 
FAILURE(BgL_auxz00_9033,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 629 */
 obj_t BgL_pairz00_3879;
{ /* Llib/unicode.scm 629 */
 obj_t BgL_aux4564z00_7456;
BgL_aux4564z00_7456 = 
CDR(BgL_pairz00_3876); 
if(
PAIRP(BgL_aux4564z00_7456))
{ /* Llib/unicode.scm 629 */
BgL_pairz00_3879 = BgL_aux4564z00_7456; }  else 
{ 
 obj_t BgL_auxz00_9040;
BgL_auxz00_9040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25906L), BGl_string4899z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_aux4564z00_7456); 
FAILURE(BgL_auxz00_9040,BFALSE,BFALSE);} } 
BgL_arg1394z00_1449 = 
CAR(BgL_pairz00_3879); } } 
BgL_arg1393z00_1448 = 
BGl_loopze72ze7zz__unicodez00(BgL_arg1394z00_1449, BgL_stz00_1447); } 
{ /* Llib/unicode.scm 629 */
 obj_t BgL_pairz00_3880;
if(
PAIRP(BgL_oz00_1446))
{ /* Llib/unicode.scm 629 */
BgL_pairz00_3880 = BgL_oz00_1446; }  else 
{ 
 obj_t BgL_auxz00_9048;
BgL_auxz00_9048 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25898L), BGl_string4899z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_oz00_1446); 
FAILURE(BgL_auxz00_9048,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_3880, BgL_arg1393z00_1448); } } 
return BgL_tablez00_1442;}  else 
{ /* Llib/unicode.scm 627 */
return 
MAKE_YOUNG_PAIR(BgL_ez00_1441, BgL_tablez00_1442);} } } } } 

}



/* add-table-entry~0 */
obj_t BGl_addzd2tablezd2entryze70ze7zz__unicodez00(obj_t BgL_tablez00_1435, obj_t BgL_sz00_1436, long BgL_charz00_1437)
{
{ /* Llib/unicode.scm 631 */
{ /* Llib/unicode.scm 621 */
 obj_t BgL_g1042z00_1439;
{ /* Llib/unicode.scm 621 */
 obj_t BgL_arg1395z00_1451;
BgL_arg1395z00_1451 = 
BGl_makezd2tablezd2entryze70ze7zz__unicodez00(BgL_sz00_1436, BgL_charz00_1437); 
{ /* Llib/unicode.scm 621 */
 obj_t BgL_pairz00_3873;
if(
PAIRP(BgL_arg1395z00_1451))
{ /* Llib/unicode.scm 621 */
BgL_pairz00_3873 = BgL_arg1395z00_1451; }  else 
{ 
 obj_t BgL_auxz00_9057;
BgL_auxz00_9057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25744L), BGl_string4900z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_arg1395z00_1451); 
FAILURE(BgL_auxz00_9057,BFALSE,BFALSE);} 
BgL_g1042z00_1439 = 
CAR(BgL_pairz00_3873); } } 
return 
BGl_loopze72ze7zz__unicodez00(BgL_g1042z00_1439, BgL_tablez00_1435);} } 

}



/* &inverse-utf8-table */
obj_t BGl_z62inversezd2utf8zd2tablez62zz__unicodez00(obj_t BgL_envz00_6188, obj_t BgL_tablez00_6189)
{
{ /* Llib/unicode.scm 611 */
{ /* Llib/unicode.scm 614 */
 obj_t BgL_auxz00_9063;
if(
VECTORP(BgL_tablez00_6189))
{ /* Llib/unicode.scm 614 */
BgL_auxz00_9063 = BgL_tablez00_6189
; }  else 
{ 
 obj_t BgL_auxz00_9066;
BgL_auxz00_9066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(25484L), BGl_string4901z00zz__unicodez00, BGl_string4902z00zz__unicodez00, BgL_tablez00_6189); 
FAILURE(BgL_auxz00_9066,BFALSE,BFALSE);} 
return 
BGl_inversezd2utf8zd2tablez00zz__unicodez00(BgL_auxz00_9063);} } 

}



/* utf8-string->ucs2-string */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2ze3ucs2zd2stringz31zz__unicodez00(obj_t BgL_utf8z00_64)
{
{ /* Llib/unicode.scm 646 */
BGL_TAIL return 
utf8_string_to_ucs2_string(BgL_utf8z00_64);} 

}



/* &utf8-string->ucs2-string */
obj_t BGl_z62utf8zd2stringzd2ze3ucs2zd2stringz53zz__unicodez00(obj_t BgL_envz00_6190, obj_t BgL_utf8z00_6191)
{
{ /* Llib/unicode.scm 646 */
{ /* Llib/unicode.scm 647 */
 obj_t BgL_auxz00_9072;
if(
STRINGP(BgL_utf8z00_6191))
{ /* Llib/unicode.scm 647 */
BgL_auxz00_9072 = BgL_utf8z00_6191
; }  else 
{ 
 obj_t BgL_auxz00_9075;
BgL_auxz00_9075 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(26505L), BGl_string4903z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_utf8z00_6191); 
FAILURE(BgL_auxz00_9075,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2ze3ucs2zd2stringz31zz__unicodez00(BgL_auxz00_9072);} } 

}



/* utf8->8bits-length */
long BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(obj_t BgL_strz00_65, long BgL_lenz00_66)
{
{ /* Llib/unicode.scm 652 */
{ 
 long BgL_siza7eza7_1455; long BgL_iz00_1456;
BgL_siza7eza7_1455 = 0L; 
BgL_iz00_1456 = 0L; 
BgL_zc3z04anonymousza31396ze3z87_1457:
if(
(BgL_iz00_1456>=BgL_lenz00_66))
{ /* Llib/unicode.scm 655 */
return BgL_siza7eza7_1455;}  else 
{ /* Llib/unicode.scm 657 */
 long BgL_cz00_1459;
{ /* Llib/unicode.scm 657 */
 unsigned char BgL_tmpz00_9082;
{ /* Llib/unicode.scm 657 */
 long BgL_l3390z00_6282;
BgL_l3390z00_6282 = 
STRING_LENGTH(BgL_strz00_65); 
if(
BOUND_CHECK(BgL_iz00_1456, BgL_l3390z00_6282))
{ /* Llib/unicode.scm 657 */
BgL_tmpz00_9082 = 
STRING_REF(BgL_strz00_65, BgL_iz00_1456)
; }  else 
{ 
 obj_t BgL_auxz00_9087;
BgL_auxz00_9087 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(26895L), BGl_string4897z00zz__unicodez00, BgL_strz00_65, 
(int)(BgL_l3390z00_6282), 
(int)(BgL_iz00_1456)); 
FAILURE(BgL_auxz00_9087,BFALSE,BFALSE);} } 
BgL_cz00_1459 = 
(BgL_tmpz00_9082); } 
if(
(BgL_cz00_1459<194L))
{ 
 long BgL_iz00_9098; long BgL_siza7eza7_9096;
BgL_siza7eza7_9096 = 
(BgL_siza7eza7_1455+1L); 
BgL_iz00_9098 = 
(BgL_iz00_1456+1L); 
BgL_iz00_1456 = BgL_iz00_9098; 
BgL_siza7eza7_1455 = BgL_siza7eza7_9096; 
goto BgL_zc3z04anonymousza31396ze3z87_1457;}  else 
{ /* Llib/unicode.scm 659 */
if(
(BgL_cz00_1459<=223L))
{ 
 long BgL_iz00_9104; long BgL_siza7eza7_9102;
BgL_siza7eza7_9102 = 
(BgL_siza7eza7_1455+1L); 
BgL_iz00_9104 = 
(BgL_iz00_1456+2L); 
BgL_iz00_1456 = BgL_iz00_9104; 
BgL_siza7eza7_1455 = BgL_siza7eza7_9102; 
goto BgL_zc3z04anonymousza31396ze3z87_1457;}  else 
{ /* Llib/unicode.scm 661 */
if(
(BgL_cz00_1459<=239L))
{ 
 long BgL_iz00_9110; long BgL_siza7eza7_9108;
BgL_siza7eza7_9108 = 
(BgL_siza7eza7_1455+1L); 
BgL_iz00_9110 = 
(BgL_iz00_1456+3L); 
BgL_iz00_1456 = BgL_iz00_9110; 
BgL_siza7eza7_1455 = BgL_siza7eza7_9108; 
goto BgL_zc3z04anonymousza31396ze3z87_1457;}  else 
{ /* Llib/unicode.scm 663 */
if(
(BgL_cz00_1459<=247L))
{ 
 long BgL_iz00_9116; long BgL_siza7eza7_9114;
BgL_siza7eza7_9114 = 
(BgL_siza7eza7_1455+1L); 
BgL_iz00_9116 = 
(BgL_iz00_1456+4L); 
BgL_iz00_1456 = BgL_iz00_9116; 
BgL_siza7eza7_1455 = BgL_siza7eza7_9114; 
goto BgL_zc3z04anonymousza31396ze3z87_1457;}  else 
{ /* Llib/unicode.scm 665 */
if(
(BgL_cz00_1459<=251L))
{ 
 long BgL_iz00_9122; long BgL_siza7eza7_9120;
BgL_siza7eza7_9120 = 
(BgL_siza7eza7_1455+1L); 
BgL_iz00_9122 = 
(BgL_iz00_1456+5L); 
BgL_iz00_1456 = BgL_iz00_9122; 
BgL_siza7eza7_1455 = BgL_siza7eza7_9120; 
goto BgL_zc3z04anonymousza31396ze3z87_1457;}  else 
{ 
 long BgL_iz00_9126; long BgL_siza7eza7_9124;
BgL_siza7eza7_9124 = 
(BgL_siza7eza7_1455+1L); 
BgL_iz00_9126 = 
(BgL_iz00_1456+6L); 
BgL_iz00_1456 = BgL_iz00_9126; 
BgL_siza7eza7_1455 = BgL_siza7eza7_9124; 
goto BgL_zc3z04anonymousza31396ze3z87_1457;} } } } } } } } 

}



/* ascii-string? */
BGL_EXPORTED_DEF bool_t BGl_asciizd2stringzf3z21zz__unicodez00(obj_t BgL_strz00_67)
{
{ /* Llib/unicode.scm 675 */
return 
(
BGl_stringzd2minimalzd2charsetz00zz__unicodez00(BgL_strz00_67)==BGl_symbol4904z00zz__unicodez00);} 

}



/* &ascii-string? */
obj_t BGl_z62asciizd2stringzf3z43zz__unicodez00(obj_t BgL_envz00_6192, obj_t BgL_strz00_6193)
{
{ /* Llib/unicode.scm 675 */
{ /* Llib/unicode.scm 676 */
 bool_t BgL_tmpz00_9130;
{ /* Llib/unicode.scm 676 */
 obj_t BgL_auxz00_9131;
if(
STRINGP(BgL_strz00_6193))
{ /* Llib/unicode.scm 676 */
BgL_auxz00_9131 = BgL_strz00_6193
; }  else 
{ 
 obj_t BgL_auxz00_9134;
BgL_auxz00_9134 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(27489L), BGl_string4906z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6193); 
FAILURE(BgL_auxz00_9134,BFALSE,BFALSE);} 
BgL_tmpz00_9130 = 
BGl_asciizd2stringzf3z21zz__unicodez00(BgL_auxz00_9131); } 
return 
BBOOL(BgL_tmpz00_9130);} } 

}



/* _utf8-string? */
obj_t BGl__utf8zd2stringzf3z21zz__unicodez00(obj_t BgL_env1129z00_71, obj_t BgL_opt1128z00_70)
{
{ /* Llib/unicode.scm 681 */
{ /* Llib/unicode.scm 681 */
 obj_t BgL_g1130z00_1480;
BgL_g1130z00_1480 = 
VECTOR_REF(BgL_opt1128z00_70,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1128z00_70)) { case 1L : 

{ /* Llib/unicode.scm 681 */

{ /* Llib/unicode.scm 681 */
 bool_t BgL_tmpz00_9141;
{ /* Llib/unicode.scm 681 */
 obj_t BgL_auxz00_9142;
if(
STRINGP(BgL_g1130z00_1480))
{ /* Llib/unicode.scm 681 */
BgL_auxz00_9142 = BgL_g1130z00_1480
; }  else 
{ 
 obj_t BgL_auxz00_9145;
BgL_auxz00_9145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(27750L), BGl_string4909z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_g1130z00_1480); 
FAILURE(BgL_auxz00_9145,BFALSE,BFALSE);} 
BgL_tmpz00_9141 = 
BGl_utf8zd2stringzf3z21zz__unicodez00(BgL_auxz00_9142, ((bool_t)0)); } 
return 
BBOOL(BgL_tmpz00_9141);} } break;case 2L : 

{ /* Llib/unicode.scm 681 */
 obj_t BgL_strictz00_1484;
BgL_strictz00_1484 = 
VECTOR_REF(BgL_opt1128z00_70,1L); 
{ /* Llib/unicode.scm 681 */

{ /* Llib/unicode.scm 681 */
 bool_t BgL_tmpz00_9152;
{ /* Llib/unicode.scm 681 */
 obj_t BgL_auxz00_9153;
if(
STRINGP(BgL_g1130z00_1480))
{ /* Llib/unicode.scm 681 */
BgL_auxz00_9153 = BgL_g1130z00_1480
; }  else 
{ 
 obj_t BgL_auxz00_9156;
BgL_auxz00_9156 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(27750L), BGl_string4909z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_g1130z00_1480); 
FAILURE(BgL_auxz00_9156,BFALSE,BFALSE);} 
BgL_tmpz00_9152 = 
BGl_utf8zd2stringzf3z21zz__unicodez00(BgL_auxz00_9153, 
CBOOL(BgL_strictz00_1484)); } 
return 
BBOOL(BgL_tmpz00_9152);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4907z00zz__unicodez00, BGl_string4846z00zz__unicodez00, 
BINT(
VECTOR_LENGTH(BgL_opt1128z00_70)));} } } } 

}



/* utf8-string? */
BGL_EXPORTED_DEF bool_t BGl_utf8zd2stringzf3z21zz__unicodez00(obj_t BgL_strz00_68, bool_t BgL_strictz00_69)
{
{ /* Llib/unicode.scm 681 */
{ /* Llib/unicode.scm 687 */
 long BgL_lenz00_1487;
BgL_lenz00_1487 = 
STRING_LENGTH(BgL_strz00_68); 
{ 
 long BgL_rz00_1489;
BgL_rz00_1489 = 0L; 
BgL_zc3z04anonymousza31419ze3z87_1490:
if(
(BgL_rz00_1489==BgL_lenz00_1487))
{ /* Llib/unicode.scm 689 */
return ((bool_t)1);}  else 
{ /* Llib/unicode.scm 691 */
 long BgL_nz00_1493;
{ /* Llib/unicode.scm 692 */
 unsigned char BgL_tmpz00_9171;
{ /* Llib/unicode.scm 691 */
 long BgL_l3394z00_6286;
BgL_l3394z00_6286 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_rz00_1489, BgL_l3394z00_6286))
{ /* Llib/unicode.scm 691 */
BgL_tmpz00_9171 = 
STRING_REF(BgL_strz00_68, BgL_rz00_1489)
; }  else 
{ 
 obj_t BgL_auxz00_9176;
BgL_auxz00_9176 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28020L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3394z00_6286), 
(int)(BgL_rz00_1489)); 
FAILURE(BgL_auxz00_9176,BFALSE,BFALSE);} } 
BgL_nz00_1493 = 
(BgL_tmpz00_9171); } 
{ /* Llib/unicode.scm 692 */

if(
(BgL_nz00_1493<=127L))
{ 
 long BgL_rz00_9185;
BgL_rz00_9185 = 
(BgL_rz00_1489+1L); 
BgL_rz00_1489 = BgL_rz00_9185; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 694 */
if(
(BgL_nz00_1493<194L))
{ /* Llib/unicode.scm 697 */
return ((bool_t)0);}  else 
{ /* Llib/unicode.scm 697 */
if(
(BgL_nz00_1493<223L))
{ /* Llib/unicode.scm 702 */
 bool_t BgL_test5350z00_9191;
if(
(
(1L+BgL_rz00_1489)<BgL_lenz00_1487))
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_3937;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9195;
{ /* Llib/unicode.scm 703 */
 long BgL_i3397z00_6289;
BgL_i3397z00_6289 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 703 */
 long BgL_l3398z00_6290;
BgL_l3398z00_6290 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3397z00_6289, BgL_l3398z00_6290))
{ /* Llib/unicode.scm 703 */
BgL_tmpz00_9195 = 
STRING_REF(BgL_strz00_68, BgL_i3397z00_6289)
; }  else 
{ 
 obj_t BgL_auxz00_9201;
BgL_auxz00_9201 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28296L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3398z00_6290), 
(int)(BgL_i3397z00_6289)); 
FAILURE(BgL_auxz00_9201,BFALSE,BFALSE);} } } 
BgL_nz00_3937 = 
(BgL_tmpz00_9195); } 
if(
(BgL_nz00_3937>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5350z00_9191 = 
(BgL_nz00_3937<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5350z00_9191 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 702 */
BgL_test5350z00_9191 = ((bool_t)0)
; } 
if(BgL_test5350z00_9191)
{ 
 long BgL_rz00_9211;
BgL_rz00_9211 = 
(BgL_rz00_1489+2L); 
BgL_rz00_1489 = BgL_rz00_9211; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 702 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 705 */
 bool_t BgL_test5354z00_9213;
if(
(BgL_nz00_1493>=216L))
{ /* Llib/unicode.scm 705 */
BgL_test5354z00_9213 = 
(BgL_nz00_1493<=219L)
; }  else 
{ /* Llib/unicode.scm 705 */
BgL_test5354z00_9213 = ((bool_t)0)
; } 
if(BgL_test5354z00_9213)
{ /* Llib/unicode.scm 707 */
 bool_t BgL_test5356z00_9217;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-3L)))
{ /* Llib/unicode.scm 708 */
 bool_t BgL_test5358z00_9221;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_3951;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9222;
{ /* Llib/unicode.scm 708 */
 long BgL_i3401z00_6293;
BgL_i3401z00_6293 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 708 */
 long BgL_l3402z00_6294;
BgL_l3402z00_6294 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3401z00_6293, BgL_l3402z00_6294))
{ /* Llib/unicode.scm 708 */
BgL_tmpz00_9222 = 
STRING_REF(BgL_strz00_68, BgL_i3401z00_6293)
; }  else 
{ 
 obj_t BgL_auxz00_9228;
BgL_auxz00_9228 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28483L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3402z00_6294), 
(int)(BgL_i3401z00_6293)); 
FAILURE(BgL_auxz00_9228,BFALSE,BFALSE);} } } 
BgL_nz00_3951 = 
(BgL_tmpz00_9222); } 
if(
(BgL_nz00_3951>=220L))
{ /* Llib/unicode.scm 685 */
BgL_test5358z00_9221 = 
(BgL_nz00_3951<=223L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5358z00_9221 = ((bool_t)0)
; } } 
if(BgL_test5358z00_9221)
{ /* Llib/unicode.scm 709 */
 bool_t BgL_test5361z00_9238;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_3959;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9239;
{ /* Llib/unicode.scm 709 */
 long BgL_i3405z00_6297;
BgL_i3405z00_6297 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 709 */
 long BgL_l3406z00_6298;
BgL_l3406z00_6298 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3405z00_6297, BgL_l3406z00_6298))
{ /* Llib/unicode.scm 709 */
BgL_tmpz00_9239 = 
STRING_REF(BgL_strz00_68, BgL_i3405z00_6297)
; }  else 
{ 
 obj_t BgL_auxz00_9245;
BgL_auxz00_9245 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28542L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3406z00_6298), 
(int)(BgL_i3405z00_6297)); 
FAILURE(BgL_auxz00_9245,BFALSE,BFALSE);} } } 
BgL_nz00_3959 = 
(BgL_tmpz00_9239); } 
if(
(BgL_nz00_3959>=220L))
{ /* Llib/unicode.scm 685 */
BgL_test5361z00_9238 = 
(BgL_nz00_3959<=223L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5361z00_9238 = ((bool_t)0)
; } } 
if(BgL_test5361z00_9238)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_3967;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9255;
{ /* Llib/unicode.scm 710 */
 long BgL_i3409z00_6301;
BgL_i3409z00_6301 = 
(BgL_rz00_1489+3L); 
{ /* Llib/unicode.scm 710 */
 long BgL_l3410z00_6302;
BgL_l3410z00_6302 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3409z00_6301, BgL_l3410z00_6302))
{ /* Llib/unicode.scm 710 */
BgL_tmpz00_9255 = 
STRING_REF(BgL_strz00_68, BgL_i3409z00_6301)
; }  else 
{ 
 obj_t BgL_auxz00_9261;
BgL_auxz00_9261 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28601L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3410z00_6302), 
(int)(BgL_i3409z00_6301)); 
FAILURE(BgL_auxz00_9261,BFALSE,BFALSE);} } } 
BgL_nz00_3967 = 
(BgL_tmpz00_9255); } 
if(
(BgL_nz00_3967>=220L))
{ /* Llib/unicode.scm 685 */
BgL_test5356z00_9217 = 
(BgL_nz00_3967<=223L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5356z00_9217 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 709 */
BgL_test5356z00_9217 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 708 */
BgL_test5356z00_9217 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 707 */
BgL_test5356z00_9217 = ((bool_t)0)
; } 
if(BgL_test5356z00_9217)
{ 
 long BgL_rz00_9271;
BgL_rz00_9271 = 
(BgL_rz00_1489+4L); 
BgL_rz00_1489 = BgL_rz00_9271; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 707 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 705 */
if(
(BgL_nz00_1493<=223L))
{ /* Llib/unicode.scm 714 */
 bool_t BgL_test5367z00_9275;
if(
(
(1L+BgL_rz00_1489)<BgL_lenz00_1487))
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_3980;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9279;
{ /* Llib/unicode.scm 715 */
 long BgL_i3413z00_6305;
BgL_i3413z00_6305 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 715 */
 long BgL_l3414z00_6306;
BgL_l3414z00_6306 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3413z00_6305, BgL_l3414z00_6306))
{ /* Llib/unicode.scm 715 */
BgL_tmpz00_9279 = 
STRING_REF(BgL_strz00_68, BgL_i3413z00_6305)
; }  else 
{ 
 obj_t BgL_auxz00_9285;
BgL_auxz00_9285 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28767L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3414z00_6306), 
(int)(BgL_i3413z00_6305)); 
FAILURE(BgL_auxz00_9285,BFALSE,BFALSE);} } } 
BgL_nz00_3980 = 
(BgL_tmpz00_9279); } 
if(
(BgL_nz00_3980>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5367z00_9275 = 
(BgL_nz00_3980<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5367z00_9275 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 714 */
BgL_test5367z00_9275 = ((bool_t)0)
; } 
if(BgL_test5367z00_9275)
{ 
 long BgL_rz00_9295;
BgL_rz00_9295 = 
(BgL_rz00_1489+2L); 
BgL_rz00_1489 = BgL_rz00_9295; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 714 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 712 */
if(
(BgL_nz00_1493<=239L))
{ /* Llib/unicode.scm 719 */
 bool_t BgL_test5372z00_9299;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-2L)))
{ /* Llib/unicode.scm 720 */
 bool_t BgL_test5374z00_9303;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_3993;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9304;
{ /* Llib/unicode.scm 720 */
 long BgL_i3417z00_6309;
BgL_i3417z00_6309 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 720 */
 long BgL_l3418z00_6310;
BgL_l3418z00_6310 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3417z00_6309, BgL_l3418z00_6310))
{ /* Llib/unicode.scm 720 */
BgL_tmpz00_9304 = 
STRING_REF(BgL_strz00_68, BgL_i3417z00_6309)
; }  else 
{ 
 obj_t BgL_auxz00_9310;
BgL_auxz00_9310 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28938L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3418z00_6310), 
(int)(BgL_i3417z00_6309)); 
FAILURE(BgL_auxz00_9310,BFALSE,BFALSE);} } } 
BgL_nz00_3993 = 
(BgL_tmpz00_9304); } 
if(
(BgL_nz00_3993>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5374z00_9303 = 
(BgL_nz00_3993<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5374z00_9303 = ((bool_t)0)
; } } 
if(BgL_test5374z00_9303)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4001;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9320;
{ /* Llib/unicode.scm 721 */
 long BgL_i3421z00_6313;
BgL_i3421z00_6313 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 721 */
 long BgL_l3422z00_6314;
BgL_l3422z00_6314 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3421z00_6313, BgL_l3422z00_6314))
{ /* Llib/unicode.scm 721 */
BgL_tmpz00_9320 = 
STRING_REF(BgL_strz00_68, BgL_i3421z00_6313)
; }  else 
{ 
 obj_t BgL_auxz00_9326;
BgL_auxz00_9326 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(28997L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3422z00_6314), 
(int)(BgL_i3421z00_6313)); 
FAILURE(BgL_auxz00_9326,BFALSE,BFALSE);} } } 
BgL_nz00_4001 = 
(BgL_tmpz00_9320); } 
if(
(BgL_nz00_4001>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5372z00_9299 = 
(BgL_nz00_4001<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5372z00_9299 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 720 */
BgL_test5372z00_9299 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 719 */
BgL_test5372z00_9299 = ((bool_t)0)
; } 
if(BgL_test5372z00_9299)
{ 
 long BgL_rz00_9336;
BgL_rz00_9336 = 
(BgL_rz00_1489+3L); 
BgL_rz00_1489 = BgL_rz00_9336; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 719 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 717 */
if(
(BgL_nz00_1493==240L))
{ /* Llib/unicode.scm 725 */
 bool_t BgL_test5380z00_9340;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-3L)))
{ /* Llib/unicode.scm 726 */
 bool_t BgL_test5382z00_9344;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4014;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9345;
{ /* Llib/unicode.scm 726 */
 long BgL_i3425z00_6317;
BgL_i3425z00_6317 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 726 */
 long BgL_l3426z00_6318;
BgL_l3426z00_6318 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3425z00_6317, BgL_l3426z00_6318))
{ /* Llib/unicode.scm 726 */
BgL_tmpz00_9345 = 
STRING_REF(BgL_strz00_68, BgL_i3425z00_6317)
; }  else 
{ 
 obj_t BgL_auxz00_9351;
BgL_auxz00_9351 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29176L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3426z00_6318), 
(int)(BgL_i3425z00_6317)); 
FAILURE(BgL_auxz00_9351,BFALSE,BFALSE);} } } 
BgL_nz00_4014 = 
(BgL_tmpz00_9345); } 
if(
(BgL_nz00_4014>=144L))
{ /* Llib/unicode.scm 685 */
BgL_test5382z00_9344 = 
(BgL_nz00_4014<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5382z00_9344 = ((bool_t)0)
; } } 
if(BgL_test5382z00_9344)
{ /* Llib/unicode.scm 727 */
 bool_t BgL_test5385z00_9361;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4022;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9362;
{ /* Llib/unicode.scm 727 */
 long BgL_i3429z00_6321;
BgL_i3429z00_6321 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 727 */
 long BgL_l3430z00_6322;
BgL_l3430z00_6322 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3429z00_6321, BgL_l3430z00_6322))
{ /* Llib/unicode.scm 727 */
BgL_tmpz00_9362 = 
STRING_REF(BgL_strz00_68, BgL_i3429z00_6321)
; }  else 
{ 
 obj_t BgL_auxz00_9368;
BgL_auxz00_9368 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29235L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3430z00_6322), 
(int)(BgL_i3429z00_6321)); 
FAILURE(BgL_auxz00_9368,BFALSE,BFALSE);} } } 
BgL_nz00_4022 = 
(BgL_tmpz00_9362); } 
if(
(BgL_nz00_4022>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5385z00_9361 = 
(BgL_nz00_4022<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5385z00_9361 = ((bool_t)0)
; } } 
if(BgL_test5385z00_9361)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4030;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9378;
{ /* Llib/unicode.scm 728 */
 long BgL_i3433z00_6325;
BgL_i3433z00_6325 = 
(BgL_rz00_1489+3L); 
{ /* Llib/unicode.scm 728 */
 long BgL_l3434z00_6326;
BgL_l3434z00_6326 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3433z00_6325, BgL_l3434z00_6326))
{ /* Llib/unicode.scm 728 */
BgL_tmpz00_9378 = 
STRING_REF(BgL_strz00_68, BgL_i3433z00_6325)
; }  else 
{ 
 obj_t BgL_auxz00_9384;
BgL_auxz00_9384 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29294L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3434z00_6326), 
(int)(BgL_i3433z00_6325)); 
FAILURE(BgL_auxz00_9384,BFALSE,BFALSE);} } } 
BgL_nz00_4030 = 
(BgL_tmpz00_9378); } 
if(
(BgL_nz00_4030>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5380z00_9340 = 
(BgL_nz00_4030<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5380z00_9340 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 727 */
BgL_test5380z00_9340 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 726 */
BgL_test5380z00_9340 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 725 */
BgL_test5380z00_9340 = ((bool_t)0)
; } 
if(BgL_test5380z00_9340)
{ 
 long BgL_rz00_9394;
BgL_rz00_9394 = 
(BgL_rz00_1489+4L); 
BgL_rz00_1489 = BgL_rz00_9394; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 725 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 730 */
 bool_t BgL_test5390z00_9396;
if(
(BgL_nz00_1493==244L))
{ /* Llib/unicode.scm 730 */
BgL_test5390z00_9396 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 731 */
 bool_t BgL_test5392z00_9399;
{ /* Llib/unicode.scm 731 */
 bool_t BgL__ortest_1047z00_1739;
BgL__ortest_1047z00_1739 = 
(BgL_nz00_1493==248L); 
if(BgL__ortest_1047z00_1739)
{ /* Llib/unicode.scm 731 */
BgL_test5392z00_9399 = BgL__ortest_1047z00_1739
; }  else 
{ /* Llib/unicode.scm 731 */
BgL_test5392z00_9399 = 
(BgL_nz00_1493==252L)
; } } 
if(BgL_test5392z00_9399)
{ /* Llib/unicode.scm 731 */
if(BgL_strictz00_69)
{ /* Llib/unicode.scm 731 */
BgL_test5390z00_9396 = ((bool_t)0)
; }  else 
{ /* Llib/unicode.scm 731 */
BgL_test5390z00_9396 = ((bool_t)1)
; } }  else 
{ /* Llib/unicode.scm 731 */
BgL_test5390z00_9396 = ((bool_t)0)
; } } 
if(BgL_test5390z00_9396)
{ /* Llib/unicode.scm 733 */
 bool_t BgL_test5395z00_9404;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-3L)))
{ /* Llib/unicode.scm 734 */
 bool_t BgL_test5397z00_9408;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4045;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9409;
{ /* Llib/unicode.scm 734 */
 long BgL_i3437z00_6329;
BgL_i3437z00_6329 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 734 */
 long BgL_l3438z00_6330;
BgL_l3438z00_6330 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3437z00_6329, BgL_l3438z00_6330))
{ /* Llib/unicode.scm 734 */
BgL_tmpz00_9409 = 
STRING_REF(BgL_strz00_68, BgL_i3437z00_6329)
; }  else 
{ 
 obj_t BgL_auxz00_9415;
BgL_auxz00_9415 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29531L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3438z00_6330), 
(int)(BgL_i3437z00_6329)); 
FAILURE(BgL_auxz00_9415,BFALSE,BFALSE);} } } 
BgL_nz00_4045 = 
(BgL_tmpz00_9409); } 
if(
(BgL_nz00_4045>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5397z00_9408 = 
(BgL_nz00_4045<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5397z00_9408 = ((bool_t)0)
; } } 
if(BgL_test5397z00_9408)
{ /* Llib/unicode.scm 735 */
 bool_t BgL_test5400z00_9425;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4053;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9426;
{ /* Llib/unicode.scm 735 */
 long BgL_i3441z00_6333;
BgL_i3441z00_6333 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 735 */
 long BgL_l3442z00_6334;
BgL_l3442z00_6334 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3441z00_6333, BgL_l3442z00_6334))
{ /* Llib/unicode.scm 735 */
BgL_tmpz00_9426 = 
STRING_REF(BgL_strz00_68, BgL_i3441z00_6333)
; }  else 
{ 
 obj_t BgL_auxz00_9432;
BgL_auxz00_9432 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29590L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3442z00_6334), 
(int)(BgL_i3441z00_6333)); 
FAILURE(BgL_auxz00_9432,BFALSE,BFALSE);} } } 
BgL_nz00_4053 = 
(BgL_tmpz00_9426); } 
if(
(BgL_nz00_4053>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5400z00_9425 = 
(BgL_nz00_4053<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5400z00_9425 = ((bool_t)0)
; } } 
if(BgL_test5400z00_9425)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4061;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9442;
{ /* Llib/unicode.scm 736 */
 long BgL_i3445z00_6337;
BgL_i3445z00_6337 = 
(BgL_rz00_1489+3L); 
{ /* Llib/unicode.scm 736 */
 long BgL_l3446z00_6338;
BgL_l3446z00_6338 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3445z00_6337, BgL_l3446z00_6338))
{ /* Llib/unicode.scm 736 */
BgL_tmpz00_9442 = 
STRING_REF(BgL_strz00_68, BgL_i3445z00_6337)
; }  else 
{ 
 obj_t BgL_auxz00_9448;
BgL_auxz00_9448 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29649L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3446z00_6338), 
(int)(BgL_i3445z00_6337)); 
FAILURE(BgL_auxz00_9448,BFALSE,BFALSE);} } } 
BgL_nz00_4061 = 
(BgL_tmpz00_9442); } 
if(
(BgL_nz00_4061>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5395z00_9404 = 
(BgL_nz00_4061<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5395z00_9404 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 735 */
BgL_test5395z00_9404 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 734 */
BgL_test5395z00_9404 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 733 */
BgL_test5395z00_9404 = ((bool_t)0)
; } 
if(BgL_test5395z00_9404)
{ 
 long BgL_rz00_9458;
BgL_rz00_9458 = 
(BgL_rz00_1489+4L); 
BgL_rz00_1489 = BgL_rz00_9458; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 733 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 730 */
if(
(BgL_nz00_1493<=247L))
{ /* Llib/unicode.scm 740 */
 bool_t BgL_test5406z00_9462;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-3L)))
{ /* Llib/unicode.scm 741 */
 bool_t BgL_test5408z00_9466;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4074;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9467;
{ /* Llib/unicode.scm 741 */
 long BgL_i3449z00_6341;
BgL_i3449z00_6341 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 741 */
 long BgL_l3450z00_6342;
BgL_l3450z00_6342 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3449z00_6341, BgL_l3450z00_6342))
{ /* Llib/unicode.scm 741 */
BgL_tmpz00_9467 = 
STRING_REF(BgL_strz00_68, BgL_i3449z00_6341)
; }  else 
{ 
 obj_t BgL_auxz00_9473;
BgL_auxz00_9473 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29820L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3450z00_6342), 
(int)(BgL_i3449z00_6341)); 
FAILURE(BgL_auxz00_9473,BFALSE,BFALSE);} } } 
BgL_nz00_4074 = 
(BgL_tmpz00_9467); } 
if(
(BgL_nz00_4074>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5408z00_9466 = 
(BgL_nz00_4074<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5408z00_9466 = ((bool_t)0)
; } } 
if(BgL_test5408z00_9466)
{ /* Llib/unicode.scm 742 */
 bool_t BgL_test5411z00_9483;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4082;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9484;
{ /* Llib/unicode.scm 742 */
 long BgL_i3453z00_6345;
BgL_i3453z00_6345 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 742 */
 long BgL_l3454z00_6346;
BgL_l3454z00_6346 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3453z00_6345, BgL_l3454z00_6346))
{ /* Llib/unicode.scm 742 */
BgL_tmpz00_9484 = 
STRING_REF(BgL_strz00_68, BgL_i3453z00_6345)
; }  else 
{ 
 obj_t BgL_auxz00_9490;
BgL_auxz00_9490 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29879L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3454z00_6346), 
(int)(BgL_i3453z00_6345)); 
FAILURE(BgL_auxz00_9490,BFALSE,BFALSE);} } } 
BgL_nz00_4082 = 
(BgL_tmpz00_9484); } 
if(
(BgL_nz00_4082>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5411z00_9483 = 
(BgL_nz00_4082<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5411z00_9483 = ((bool_t)0)
; } } 
if(BgL_test5411z00_9483)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4090;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9500;
{ /* Llib/unicode.scm 743 */
 long BgL_i3457z00_6349;
BgL_i3457z00_6349 = 
(BgL_rz00_1489+3L); 
{ /* Llib/unicode.scm 743 */
 long BgL_l3458z00_6350;
BgL_l3458z00_6350 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3457z00_6349, BgL_l3458z00_6350))
{ /* Llib/unicode.scm 743 */
BgL_tmpz00_9500 = 
STRING_REF(BgL_strz00_68, BgL_i3457z00_6349)
; }  else 
{ 
 obj_t BgL_auxz00_9506;
BgL_auxz00_9506 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(29938L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3458z00_6350), 
(int)(BgL_i3457z00_6349)); 
FAILURE(BgL_auxz00_9506,BFALSE,BFALSE);} } } 
BgL_nz00_4090 = 
(BgL_tmpz00_9500); } 
if(
(BgL_nz00_4090>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5406z00_9462 = 
(BgL_nz00_4090<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5406z00_9462 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 742 */
BgL_test5406z00_9462 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 741 */
BgL_test5406z00_9462 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 740 */
BgL_test5406z00_9462 = ((bool_t)0)
; } 
if(BgL_test5406z00_9462)
{ 
 long BgL_rz00_9516;
BgL_rz00_9516 = 
(BgL_rz00_1489+4L); 
BgL_rz00_1489 = BgL_rz00_9516; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 740 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 738 */
if(
(BgL_nz00_1493<=251L))
{ /* Llib/unicode.scm 746 */
 bool_t BgL_test5417z00_9520;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-4L)))
{ /* Llib/unicode.scm 747 */
 bool_t BgL_test5419z00_9524;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4103;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9525;
{ /* Llib/unicode.scm 747 */
 long BgL_i3461z00_6353;
BgL_i3461z00_6353 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 747 */
 long BgL_l3462z00_6354;
BgL_l3462z00_6354 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3461z00_6353, BgL_l3462z00_6354))
{ /* Llib/unicode.scm 747 */
BgL_tmpz00_9525 = 
STRING_REF(BgL_strz00_68, BgL_i3461z00_6353)
; }  else 
{ 
 obj_t BgL_auxz00_9531;
BgL_auxz00_9531 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30083L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3462z00_6354), 
(int)(BgL_i3461z00_6353)); 
FAILURE(BgL_auxz00_9531,BFALSE,BFALSE);} } } 
BgL_nz00_4103 = 
(BgL_tmpz00_9525); } 
if(
(BgL_nz00_4103>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5419z00_9524 = 
(BgL_nz00_4103<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5419z00_9524 = ((bool_t)0)
; } } 
if(BgL_test5419z00_9524)
{ /* Llib/unicode.scm 748 */
 bool_t BgL_test5422z00_9541;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4111;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9542;
{ /* Llib/unicode.scm 748 */
 long BgL_i3465z00_6357;
BgL_i3465z00_6357 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 748 */
 long BgL_l3466z00_6358;
BgL_l3466z00_6358 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3465z00_6357, BgL_l3466z00_6358))
{ /* Llib/unicode.scm 748 */
BgL_tmpz00_9542 = 
STRING_REF(BgL_strz00_68, BgL_i3465z00_6357)
; }  else 
{ 
 obj_t BgL_auxz00_9548;
BgL_auxz00_9548 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30142L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3466z00_6358), 
(int)(BgL_i3465z00_6357)); 
FAILURE(BgL_auxz00_9548,BFALSE,BFALSE);} } } 
BgL_nz00_4111 = 
(BgL_tmpz00_9542); } 
if(
(BgL_nz00_4111>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5422z00_9541 = 
(BgL_nz00_4111<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5422z00_9541 = ((bool_t)0)
; } } 
if(BgL_test5422z00_9541)
{ /* Llib/unicode.scm 749 */
 bool_t BgL_test5425z00_9558;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4119;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9559;
{ /* Llib/unicode.scm 749 */
 long BgL_i3469z00_6361;
BgL_i3469z00_6361 = 
(BgL_rz00_1489+3L); 
{ /* Llib/unicode.scm 749 */
 long BgL_l3470z00_6362;
BgL_l3470z00_6362 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3469z00_6361, BgL_l3470z00_6362))
{ /* Llib/unicode.scm 749 */
BgL_tmpz00_9559 = 
STRING_REF(BgL_strz00_68, BgL_i3469z00_6361)
; }  else 
{ 
 obj_t BgL_auxz00_9565;
BgL_auxz00_9565 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30201L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3470z00_6362), 
(int)(BgL_i3469z00_6361)); 
FAILURE(BgL_auxz00_9565,BFALSE,BFALSE);} } } 
BgL_nz00_4119 = 
(BgL_tmpz00_9559); } 
if(
(BgL_nz00_4119>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5425z00_9558 = 
(BgL_nz00_4119<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5425z00_9558 = ((bool_t)0)
; } } 
if(BgL_test5425z00_9558)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4127;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9575;
{ /* Llib/unicode.scm 750 */
 long BgL_i3473z00_6365;
BgL_i3473z00_6365 = 
(BgL_rz00_1489+4L); 
{ /* Llib/unicode.scm 750 */
 long BgL_l3474z00_6366;
BgL_l3474z00_6366 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3473z00_6365, BgL_l3474z00_6366))
{ /* Llib/unicode.scm 750 */
BgL_tmpz00_9575 = 
STRING_REF(BgL_strz00_68, BgL_i3473z00_6365)
; }  else 
{ 
 obj_t BgL_auxz00_9581;
BgL_auxz00_9581 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30260L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3474z00_6366), 
(int)(BgL_i3473z00_6365)); 
FAILURE(BgL_auxz00_9581,BFALSE,BFALSE);} } } 
BgL_nz00_4127 = 
(BgL_tmpz00_9575); } 
if(
(BgL_nz00_4127>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5417z00_9520 = 
(BgL_nz00_4127<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5417z00_9520 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 749 */
BgL_test5417z00_9520 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 748 */
BgL_test5417z00_9520 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 747 */
BgL_test5417z00_9520 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 746 */
BgL_test5417z00_9520 = ((bool_t)0)
; } 
if(BgL_test5417z00_9520)
{ 
 long BgL_rz00_9591;
BgL_rz00_9591 = 
(BgL_rz00_1489+5L); 
BgL_rz00_1489 = BgL_rz00_9591; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 746 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 745 */
if(
(BgL_nz00_1493<=253L))
{ /* Llib/unicode.scm 753 */
 bool_t BgL_test5431z00_9595;
if(
(BgL_rz00_1489<
(BgL_lenz00_1487-5L)))
{ /* Llib/unicode.scm 754 */
 bool_t BgL_test5433z00_9599;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4140;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9600;
{ /* Llib/unicode.scm 754 */
 long BgL_i3477z00_6369;
BgL_i3477z00_6369 = 
(BgL_rz00_1489+1L); 
{ /* Llib/unicode.scm 754 */
 long BgL_l3478z00_6370;
BgL_l3478z00_6370 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3477z00_6369, BgL_l3478z00_6370))
{ /* Llib/unicode.scm 754 */
BgL_tmpz00_9600 = 
STRING_REF(BgL_strz00_68, BgL_i3477z00_6369)
; }  else 
{ 
 obj_t BgL_auxz00_9606;
BgL_auxz00_9606 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30405L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3478z00_6370), 
(int)(BgL_i3477z00_6369)); 
FAILURE(BgL_auxz00_9606,BFALSE,BFALSE);} } } 
BgL_nz00_4140 = 
(BgL_tmpz00_9600); } 
if(
(BgL_nz00_4140>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5433z00_9599 = 
(BgL_nz00_4140<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5433z00_9599 = ((bool_t)0)
; } } 
if(BgL_test5433z00_9599)
{ /* Llib/unicode.scm 755 */
 bool_t BgL_test5436z00_9616;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4148;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9617;
{ /* Llib/unicode.scm 755 */
 long BgL_i3481z00_6373;
BgL_i3481z00_6373 = 
(BgL_rz00_1489+2L); 
{ /* Llib/unicode.scm 755 */
 long BgL_l3482z00_6374;
BgL_l3482z00_6374 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3481z00_6373, BgL_l3482z00_6374))
{ /* Llib/unicode.scm 755 */
BgL_tmpz00_9617 = 
STRING_REF(BgL_strz00_68, BgL_i3481z00_6373)
; }  else 
{ 
 obj_t BgL_auxz00_9623;
BgL_auxz00_9623 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30464L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3482z00_6374), 
(int)(BgL_i3481z00_6373)); 
FAILURE(BgL_auxz00_9623,BFALSE,BFALSE);} } } 
BgL_nz00_4148 = 
(BgL_tmpz00_9617); } 
if(
(BgL_nz00_4148>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5436z00_9616 = 
(BgL_nz00_4148<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5436z00_9616 = ((bool_t)0)
; } } 
if(BgL_test5436z00_9616)
{ /* Llib/unicode.scm 756 */
 bool_t BgL_test5439z00_9633;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4156;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9634;
{ /* Llib/unicode.scm 756 */
 long BgL_i3485z00_6377;
BgL_i3485z00_6377 = 
(BgL_rz00_1489+3L); 
{ /* Llib/unicode.scm 756 */
 long BgL_l3486z00_6378;
BgL_l3486z00_6378 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3485z00_6377, BgL_l3486z00_6378))
{ /* Llib/unicode.scm 756 */
BgL_tmpz00_9634 = 
STRING_REF(BgL_strz00_68, BgL_i3485z00_6377)
; }  else 
{ 
 obj_t BgL_auxz00_9640;
BgL_auxz00_9640 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30523L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3486z00_6378), 
(int)(BgL_i3485z00_6377)); 
FAILURE(BgL_auxz00_9640,BFALSE,BFALSE);} } } 
BgL_nz00_4156 = 
(BgL_tmpz00_9634); } 
if(
(BgL_nz00_4156>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5439z00_9633 = 
(BgL_nz00_4156<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5439z00_9633 = ((bool_t)0)
; } } 
if(BgL_test5439z00_9633)
{ /* Llib/unicode.scm 757 */
 bool_t BgL_test5442z00_9650;
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4164;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9651;
{ /* Llib/unicode.scm 757 */
 long BgL_i3489z00_6381;
BgL_i3489z00_6381 = 
(BgL_rz00_1489+4L); 
{ /* Llib/unicode.scm 757 */
 long BgL_l3490z00_6382;
BgL_l3490z00_6382 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3489z00_6381, BgL_l3490z00_6382))
{ /* Llib/unicode.scm 757 */
BgL_tmpz00_9651 = 
STRING_REF(BgL_strz00_68, BgL_i3489z00_6381)
; }  else 
{ 
 obj_t BgL_auxz00_9657;
BgL_auxz00_9657 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30582L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3490z00_6382), 
(int)(BgL_i3489z00_6381)); 
FAILURE(BgL_auxz00_9657,BFALSE,BFALSE);} } } 
BgL_nz00_4164 = 
(BgL_tmpz00_9651); } 
if(
(BgL_nz00_4164>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5442z00_9650 = 
(BgL_nz00_4164<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5442z00_9650 = ((bool_t)0)
; } } 
if(BgL_test5442z00_9650)
{ /* Llib/unicode.scm 684 */
 long BgL_nz00_4172;
{ /* Llib/unicode.scm 684 */
 unsigned char BgL_tmpz00_9667;
{ /* Llib/unicode.scm 758 */
 long BgL_i3493z00_6385;
BgL_i3493z00_6385 = 
(BgL_rz00_1489+5L); 
{ /* Llib/unicode.scm 758 */
 long BgL_l3494z00_6386;
BgL_l3494z00_6386 = 
STRING_LENGTH(BgL_strz00_68); 
if(
BOUND_CHECK(BgL_i3493z00_6385, BgL_l3494z00_6386))
{ /* Llib/unicode.scm 758 */
BgL_tmpz00_9667 = 
STRING_REF(BgL_strz00_68, BgL_i3493z00_6385)
; }  else 
{ 
 obj_t BgL_auxz00_9673;
BgL_auxz00_9673 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(30641L), BGl_string4897z00zz__unicodez00, BgL_strz00_68, 
(int)(BgL_l3494z00_6386), 
(int)(BgL_i3493z00_6385)); 
FAILURE(BgL_auxz00_9673,BFALSE,BFALSE);} } } 
BgL_nz00_4172 = 
(BgL_tmpz00_9667); } 
if(
(BgL_nz00_4172>=128L))
{ /* Llib/unicode.scm 685 */
BgL_test5431z00_9595 = 
(BgL_nz00_4172<=191L)
; }  else 
{ /* Llib/unicode.scm 685 */
BgL_test5431z00_9595 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 757 */
BgL_test5431z00_9595 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 756 */
BgL_test5431z00_9595 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 755 */
BgL_test5431z00_9595 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 754 */
BgL_test5431z00_9595 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 753 */
BgL_test5431z00_9595 = ((bool_t)0)
; } 
if(BgL_test5431z00_9595)
{ 
 long BgL_rz00_9683;
BgL_rz00_9683 = 
(BgL_rz00_1489+6L); 
BgL_rz00_1489 = BgL_rz00_9683; 
goto BgL_zc3z04anonymousza31419ze3z87_1490;}  else 
{ /* Llib/unicode.scm 753 */
return ((bool_t)0);} }  else 
{ /* Llib/unicode.scm 752 */
return ((bool_t)0);} } } } } } } } } } } } } } } } 

}



/* _utf8-string-encode */
obj_t BGl__utf8zd2stringzd2encodez00zz__unicodez00(obj_t BgL_env1134z00_77, obj_t BgL_opt1133z00_76)
{
{ /* Llib/unicode.scm 774 */
{ /* Llib/unicode.scm 774 */
 obj_t BgL_strz00_1749;
BgL_strz00_1749 = 
VECTOR_REF(BgL_opt1133z00_76,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1133z00_76)) { case 1L : 

{ /* Llib/unicode.scm 774 */
 long BgL_endz00_1754;
{ /* Llib/unicode.scm 774 */
 obj_t BgL_stringz00_4178;
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_stringz00_4178 = BgL_strz00_1749; }  else 
{ 
 obj_t BgL_auxz00_9688;
BgL_auxz00_9688 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31642L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9688,BFALSE,BFALSE);} 
BgL_endz00_1754 = 
STRING_LENGTH(BgL_stringz00_4178); } 
{ /* Llib/unicode.scm 774 */

{ /* Llib/unicode.scm 774 */
 obj_t BgL_auxz00_9693;
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_auxz00_9693 = BgL_strz00_1749
; }  else 
{ 
 obj_t BgL_auxz00_9696;
BgL_auxz00_9696 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9696,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2encodez00zz__unicodez00(BgL_auxz00_9693, ((bool_t)0), 0L, BgL_endz00_1754);} } } break;case 2L : 

{ /* Llib/unicode.scm 774 */
 obj_t BgL_strictz00_1755;
BgL_strictz00_1755 = 
VECTOR_REF(BgL_opt1133z00_76,1L); 
{ /* Llib/unicode.scm 774 */
 long BgL_endz00_1757;
{ /* Llib/unicode.scm 774 */
 obj_t BgL_stringz00_4179;
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_stringz00_4179 = BgL_strz00_1749; }  else 
{ 
 obj_t BgL_auxz00_9704;
BgL_auxz00_9704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31642L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9704,BFALSE,BFALSE);} 
BgL_endz00_1757 = 
STRING_LENGTH(BgL_stringz00_4179); } 
{ /* Llib/unicode.scm 774 */

{ /* Llib/unicode.scm 774 */
 obj_t BgL_auxz00_9709;
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_auxz00_9709 = BgL_strz00_1749
; }  else 
{ 
 obj_t BgL_auxz00_9712;
BgL_auxz00_9712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9712,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2encodez00zz__unicodez00(BgL_auxz00_9709, 
CBOOL(BgL_strictz00_1755), 0L, BgL_endz00_1757);} } } } break;case 3L : 

{ /* Llib/unicode.scm 774 */
 obj_t BgL_strictz00_1758;
BgL_strictz00_1758 = 
VECTOR_REF(BgL_opt1133z00_76,1L); 
{ /* Llib/unicode.scm 774 */
 obj_t BgL_startz00_1759;
BgL_startz00_1759 = 
VECTOR_REF(BgL_opt1133z00_76,2L); 
{ /* Llib/unicode.scm 774 */
 long BgL_endz00_1760;
{ /* Llib/unicode.scm 774 */
 obj_t BgL_stringz00_4180;
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_stringz00_4180 = BgL_strz00_1749; }  else 
{ 
 obj_t BgL_auxz00_9722;
BgL_auxz00_9722 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31642L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9722,BFALSE,BFALSE);} 
BgL_endz00_1760 = 
STRING_LENGTH(BgL_stringz00_4180); } 
{ /* Llib/unicode.scm 774 */

{ /* Llib/unicode.scm 774 */
 long BgL_auxz00_9734; obj_t BgL_auxz00_9727;
{ /* Llib/unicode.scm 774 */
 obj_t BgL_tmpz00_9736;
if(
INTEGERP(BgL_startz00_1759))
{ /* Llib/unicode.scm 774 */
BgL_tmpz00_9736 = BgL_startz00_1759
; }  else 
{ 
 obj_t BgL_auxz00_9739;
BgL_auxz00_9739 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_1759); 
FAILURE(BgL_auxz00_9739,BFALSE,BFALSE);} 
BgL_auxz00_9734 = 
(long)CINT(BgL_tmpz00_9736); } 
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_auxz00_9727 = BgL_strz00_1749
; }  else 
{ 
 obj_t BgL_auxz00_9730;
BgL_auxz00_9730 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9730,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2encodez00zz__unicodez00(BgL_auxz00_9727, 
CBOOL(BgL_strictz00_1758), BgL_auxz00_9734, BgL_endz00_1760);} } } } } break;case 4L : 

{ /* Llib/unicode.scm 774 */
 obj_t BgL_strictz00_1761;
BgL_strictz00_1761 = 
VECTOR_REF(BgL_opt1133z00_76,1L); 
{ /* Llib/unicode.scm 774 */
 obj_t BgL_startz00_1762;
BgL_startz00_1762 = 
VECTOR_REF(BgL_opt1133z00_76,2L); 
{ /* Llib/unicode.scm 774 */
 obj_t BgL_endz00_1763;
BgL_endz00_1763 = 
VECTOR_REF(BgL_opt1133z00_76,3L); 
{ /* Llib/unicode.scm 774 */

{ /* Llib/unicode.scm 774 */
 long BgL_auxz00_9765; long BgL_auxz00_9755; obj_t BgL_auxz00_9748;
{ /* Llib/unicode.scm 774 */
 obj_t BgL_tmpz00_9766;
if(
INTEGERP(BgL_endz00_1763))
{ /* Llib/unicode.scm 774 */
BgL_tmpz00_9766 = BgL_endz00_1763
; }  else 
{ 
 obj_t BgL_auxz00_9769;
BgL_auxz00_9769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_1763); 
FAILURE(BgL_auxz00_9769,BFALSE,BFALSE);} 
BgL_auxz00_9765 = 
(long)CINT(BgL_tmpz00_9766); } 
{ /* Llib/unicode.scm 774 */
 obj_t BgL_tmpz00_9757;
if(
INTEGERP(BgL_startz00_1762))
{ /* Llib/unicode.scm 774 */
BgL_tmpz00_9757 = BgL_startz00_1762
; }  else 
{ 
 obj_t BgL_auxz00_9760;
BgL_auxz00_9760 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_1762); 
FAILURE(BgL_auxz00_9760,BFALSE,BFALSE);} 
BgL_auxz00_9755 = 
(long)CINT(BgL_tmpz00_9757); } 
if(
STRINGP(BgL_strz00_1749))
{ /* Llib/unicode.scm 774 */
BgL_auxz00_9748 = BgL_strz00_1749
; }  else 
{ 
 obj_t BgL_auxz00_9751;
BgL_auxz00_9751 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31550L), BGl_string4913z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_1749); 
FAILURE(BgL_auxz00_9751,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2encodez00zz__unicodez00(BgL_auxz00_9748, 
CBOOL(BgL_strictz00_1761), BgL_auxz00_9755, BgL_auxz00_9765);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4910z00zz__unicodez00, BGl_string4912z00zz__unicodez00, 
BINT(
VECTOR_LENGTH(BgL_opt1133z00_76)));} } } } 

}



/* utf8-string-encode */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2encodez00zz__unicodez00(obj_t BgL_strz00_72, bool_t BgL_strictz00_73, long BgL_startz00_74, long BgL_endz00_75)
{
{ /* Llib/unicode.scm 774 */
{ /* Llib/unicode.scm 786 */
 bool_t BgL_test5457z00_9780;
if(
(BgL_endz00_75>
STRING_LENGTH(BgL_strz00_72)))
{ /* Llib/unicode.scm 786 */
BgL_test5457z00_9780 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 786 */
if(
(BgL_startz00_74<0L))
{ /* Llib/unicode.scm 787 */
BgL_test5457z00_9780 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 787 */
BgL_test5457z00_9780 = 
(BgL_startz00_74>BgL_endz00_75)
; } } 
if(BgL_test5457z00_9780)
{ /* Llib/unicode.scm 789 */
 obj_t BgL_arg1708z00_1772;
BgL_arg1708z00_1772 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_74), 
BINT(BgL_endz00_75)); 
{ /* Llib/unicode.scm 789 */
 obj_t BgL_aux4597z00_7489;
BgL_aux4597z00_7489 = 
BGl_errorz00zz__errorz00(BGl_string4911z00zz__unicodez00, BGl_string4914z00zz__unicodez00, BgL_arg1708z00_1772); 
if(
STRINGP(BgL_aux4597z00_7489))
{ /* Llib/unicode.scm 789 */
return BgL_aux4597z00_7489;}  else 
{ 
 obj_t BgL_auxz00_9793;
BgL_auxz00_9793 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32066L), BGl_string4911z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4597z00_7489); 
FAILURE(BgL_auxz00_9793,BFALSE,BFALSE);} } }  else 
{ /* Llib/unicode.scm 790 */
 long BgL_lenz00_1773;
BgL_lenz00_1773 = 
(BgL_endz00_75-BgL_startz00_74); 
{ /* Llib/unicode.scm 790 */
 obj_t BgL_resz00_1774;
{ /* Llib/unicode.scm 791 */
 long BgL_arg2063z00_2135;
BgL_arg2063z00_2135 = 
(3L*BgL_lenz00_1773); 
{ /* Ieee/string.scm 172 */

BgL_resz00_1774 = 
make_string(BgL_arg2063z00_2135, ((unsigned char)' ')); } } 
{ /* Llib/unicode.scm 791 */

{ /* Llib/unicode.scm 792 */
 obj_t BgL_aux4599z00_7491;
BgL_aux4599z00_7491 = 
BGl_loopze70ze7zz__unicodez00(BgL_endz00_75, BgL_strictz00_73, BgL_lenz00_1773, BgL_resz00_1774, BgL_strz00_72, BgL_startz00_74, 0L); 
if(
STRINGP(BgL_aux4599z00_7491))
{ /* Llib/unicode.scm 792 */
return BgL_aux4599z00_7491;}  else 
{ 
 obj_t BgL_auxz00_9803;
BgL_auxz00_9803 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32209L), BGl_string4911z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4599z00_7491); 
FAILURE(BgL_auxz00_9803,BFALSE,BFALSE);} } } } } } } 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__unicodez00(long BgL_endz00_6268, bool_t BgL_strictz00_6267, long BgL_lenz00_6266, obj_t BgL_resz00_6265, obj_t BgL_strz00_6264, long BgL_rz00_1776, long BgL_wz00_1777)
{
{ /* Llib/unicode.scm 792 */
BGl_loopze70ze7zz__unicodez00:
if(
(BgL_rz00_1776==BgL_endz00_6268))
{ /* Llib/unicode.scm 794 */
return 
bgl_string_shrink(BgL_resz00_6265, BgL_wz00_1777);}  else 
{ /* Llib/unicode.scm 796 */
 unsigned char BgL_cz00_1780;
{ /* Llib/unicode.scm 796 */
 long BgL_l3498z00_6390;
BgL_l3498z00_6390 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_rz00_1776, BgL_l3498z00_6390))
{ /* Llib/unicode.scm 796 */
BgL_cz00_1780 = 
STRING_REF(BgL_strz00_6264, BgL_rz00_1776); }  else 
{ 
 obj_t BgL_auxz00_9814;
BgL_auxz00_9814 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32305L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3498z00_6390), 
(int)(BgL_rz00_1776)); 
FAILURE(BgL_auxz00_9814,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 796 */
 long BgL_nz00_1781;
BgL_nz00_1781 = 
(BgL_cz00_1780); 
{ /* Llib/unicode.scm 797 */

if(
(BgL_nz00_1781<=127L))
{ /* Llib/unicode.scm 799 */
{ /* Llib/unicode.scm 801 */
 long BgL_l3502z00_6394;
BgL_l3502z00_6394 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3502z00_6394))
{ /* Llib/unicode.scm 801 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_9827;
BgL_auxz00_9827 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32403L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3502z00_6394), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_9827,BFALSE,BFALSE);} } 
{ 
 long BgL_wz00_9835; long BgL_rz00_9833;
BgL_rz00_9833 = 
(BgL_rz00_1776+1L); 
BgL_wz00_9835 = 
(BgL_wz00_1777+1L); 
BgL_wz00_1777 = BgL_wz00_9835; 
BgL_rz00_1776 = BgL_rz00_9833; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 799 */
if(
(BgL_nz00_1781<194L))
{ /* Llib/unicode.scm 803 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_9842; long BgL_rz00_9840;
BgL_rz00_9840 = 
(BgL_rz00_1776+1L); 
BgL_wz00_9842 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_9842; 
BgL_rz00_1776 = BgL_rz00_9840; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 803 */
if(
(BgL_nz00_1781<223L))
{ /* Llib/unicode.scm 809 */
 bool_t BgL_test5468z00_9846;
if(
(
(1L+BgL_rz00_1776)<BgL_lenz00_6266))
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4232;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_9850;
{ /* Llib/unicode.scm 810 */
 long BgL_i3505z00_6397;
BgL_i3505z00_6397 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 810 */
 long BgL_l3506z00_6398;
BgL_l3506z00_6398 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3505z00_6397, BgL_l3506z00_6398))
{ /* Llib/unicode.scm 810 */
BgL_tmpz00_9850 = 
STRING_REF(BgL_strz00_6264, BgL_i3505z00_6397)
; }  else 
{ 
 obj_t BgL_auxz00_9856;
BgL_auxz00_9856 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32659L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3506z00_6398), 
(int)(BgL_i3505z00_6397)); 
FAILURE(BgL_auxz00_9856,BFALSE,BFALSE);} } } 
BgL_nz00_4232 = 
(BgL_tmpz00_9850); } 
if(
(BgL_nz00_4232>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5468z00_9846 = 
(BgL_nz00_4232<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5468z00_9846 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 809 */
BgL_test5468z00_9846 = ((bool_t)0)
; } 
if(BgL_test5468z00_9846)
{ /* Llib/unicode.scm 809 */
{ /* Llib/unicode.scm 812 */
 long BgL_l3510z00_6402;
BgL_l3510z00_6402 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3510z00_6402))
{ /* Llib/unicode.scm 812 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_9870;
BgL_auxz00_9870 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32722L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3510z00_6402), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_9870,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 813 */
 long BgL_arg1726z00_1795; unsigned char BgL_arg1727z00_1796;
BgL_arg1726z00_1795 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 813 */
 long BgL_i3513z00_6405;
BgL_i3513z00_6405 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 813 */
 long BgL_l3514z00_6406;
BgL_l3514z00_6406 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3513z00_6405, BgL_l3514z00_6406))
{ /* Llib/unicode.scm 813 */
BgL_arg1727z00_1796 = 
STRING_REF(BgL_strz00_6264, BgL_i3513z00_6405); }  else 
{ 
 obj_t BgL_auxz00_9882;
BgL_auxz00_9882 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32781L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3514z00_6406), 
(int)(BgL_i3513z00_6405)); 
FAILURE(BgL_auxz00_9882,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 813 */
 long BgL_l3518z00_6410;
BgL_l3518z00_6410 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1726z00_1795, BgL_l3518z00_6410))
{ /* Llib/unicode.scm 813 */
STRING_SET(BgL_resz00_6265, BgL_arg1726z00_1795, BgL_arg1727z00_1796); }  else 
{ 
 obj_t BgL_auxz00_9892;
BgL_auxz00_9892 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(32754L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3518z00_6410), 
(int)(BgL_arg1726z00_1795)); 
FAILURE(BgL_auxz00_9892,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_9900; long BgL_rz00_9898;
BgL_rz00_9898 = 
(BgL_rz00_1776+2L); 
BgL_wz00_9900 = 
(BgL_wz00_1777+2L); 
BgL_wz00_1777 = BgL_wz00_9900; 
BgL_rz00_1776 = BgL_rz00_9898; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 809 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_9905; long BgL_rz00_9903;
BgL_rz00_9903 = 
(BgL_rz00_1776+1L); 
BgL_wz00_9905 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_9905; 
BgL_rz00_1776 = BgL_rz00_9903; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 818 */
 bool_t BgL_test5475z00_9907;
if(
(BgL_nz00_1781>=216L))
{ /* Llib/unicode.scm 818 */
BgL_test5475z00_9907 = 
(BgL_nz00_1781<=219L)
; }  else 
{ /* Llib/unicode.scm 818 */
BgL_test5475z00_9907 = ((bool_t)0)
; } 
if(BgL_test5475z00_9907)
{ /* Llib/unicode.scm 820 */
 bool_t BgL_test5477z00_9911;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-3L)))
{ /* Llib/unicode.scm 821 */
 bool_t BgL_test5479z00_9915;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4259;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_9916;
{ /* Llib/unicode.scm 821 */
 long BgL_i3521z00_6413;
BgL_i3521z00_6413 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 821 */
 long BgL_l3522z00_6414;
BgL_l3522z00_6414 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3521z00_6413, BgL_l3522z00_6414))
{ /* Llib/unicode.scm 821 */
BgL_tmpz00_9916 = 
STRING_REF(BgL_strz00_6264, BgL_i3521z00_6413)
; }  else 
{ 
 obj_t BgL_auxz00_9922;
BgL_auxz00_9922 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33054L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3522z00_6414), 
(int)(BgL_i3521z00_6413)); 
FAILURE(BgL_auxz00_9922,BFALSE,BFALSE);} } } 
BgL_nz00_4259 = 
(BgL_tmpz00_9916); } 
if(
(BgL_nz00_4259>=220L))
{ /* Llib/unicode.scm 778 */
BgL_test5479z00_9915 = 
(BgL_nz00_4259<=223L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5479z00_9915 = ((bool_t)0)
; } } 
if(BgL_test5479z00_9915)
{ /* Llib/unicode.scm 822 */
 bool_t BgL_test5482z00_9932;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4267;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_9933;
{ /* Llib/unicode.scm 822 */
 long BgL_i3525z00_6417;
BgL_i3525z00_6417 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 822 */
 long BgL_l3526z00_6418;
BgL_l3526z00_6418 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3525z00_6417, BgL_l3526z00_6418))
{ /* Llib/unicode.scm 822 */
BgL_tmpz00_9933 = 
STRING_REF(BgL_strz00_6264, BgL_i3525z00_6417)
; }  else 
{ 
 obj_t BgL_auxz00_9939;
BgL_auxz00_9939 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33110L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3526z00_6418), 
(int)(BgL_i3525z00_6417)); 
FAILURE(BgL_auxz00_9939,BFALSE,BFALSE);} } } 
BgL_nz00_4267 = 
(BgL_tmpz00_9933); } 
if(
(BgL_nz00_4267>=220L))
{ /* Llib/unicode.scm 778 */
BgL_test5482z00_9932 = 
(BgL_nz00_4267<=223L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5482z00_9932 = ((bool_t)0)
; } } 
if(BgL_test5482z00_9932)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4275;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_9949;
{ /* Llib/unicode.scm 823 */
 long BgL_i3529z00_6421;
BgL_i3529z00_6421 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 823 */
 long BgL_l3530z00_6422;
BgL_l3530z00_6422 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3529z00_6421, BgL_l3530z00_6422))
{ /* Llib/unicode.scm 823 */
BgL_tmpz00_9949 = 
STRING_REF(BgL_strz00_6264, BgL_i3529z00_6421)
; }  else 
{ 
 obj_t BgL_auxz00_9955;
BgL_auxz00_9955 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33166L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3530z00_6422), 
(int)(BgL_i3529z00_6421)); 
FAILURE(BgL_auxz00_9955,BFALSE,BFALSE);} } } 
BgL_nz00_4275 = 
(BgL_tmpz00_9949); } 
if(
(BgL_nz00_4275>=220L))
{ /* Llib/unicode.scm 778 */
BgL_test5477z00_9911 = 
(BgL_nz00_4275<=223L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5477z00_9911 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 822 */
BgL_test5477z00_9911 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 821 */
BgL_test5477z00_9911 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 820 */
BgL_test5477z00_9911 = ((bool_t)0)
; } 
if(BgL_test5477z00_9911)
{ /* Llib/unicode.scm 820 */
{ /* Llib/unicode.scm 825 */
 long BgL_l3534z00_6426;
BgL_l3534z00_6426 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3534z00_6426))
{ /* Llib/unicode.scm 825 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_9969;
BgL_auxz00_9969 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33227L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3534z00_6426), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_9969,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 826 */
 long BgL_arg1756z00_1824; unsigned char BgL_arg1757z00_1825;
BgL_arg1756z00_1824 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 826 */
 long BgL_i3537z00_6429;
BgL_i3537z00_6429 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 826 */
 long BgL_l3538z00_6430;
BgL_l3538z00_6430 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3537z00_6429, BgL_l3538z00_6430))
{ /* Llib/unicode.scm 826 */
BgL_arg1757z00_1825 = 
STRING_REF(BgL_strz00_6264, BgL_i3537z00_6429); }  else 
{ 
 obj_t BgL_auxz00_9981;
BgL_auxz00_9981 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33285L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3538z00_6430), 
(int)(BgL_i3537z00_6429)); 
FAILURE(BgL_auxz00_9981,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 826 */
 long BgL_l3542z00_6434;
BgL_l3542z00_6434 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1756z00_1824, BgL_l3542z00_6434))
{ /* Llib/unicode.scm 826 */
STRING_SET(BgL_resz00_6265, BgL_arg1756z00_1824, BgL_arg1757z00_1825); }  else 
{ 
 obj_t BgL_auxz00_9991;
BgL_auxz00_9991 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33258L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3542z00_6434), 
(int)(BgL_arg1756z00_1824)); 
FAILURE(BgL_auxz00_9991,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 827 */
 long BgL_arg1759z00_1827; unsigned char BgL_arg1760z00_1828;
BgL_arg1759z00_1827 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 827 */
 long BgL_i3545z00_6437;
BgL_i3545z00_6437 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 827 */
 long BgL_l3546z00_6438;
BgL_l3546z00_6438 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3545z00_6437, BgL_l3546z00_6438))
{ /* Llib/unicode.scm 827 */
BgL_arg1760z00_1828 = 
STRING_REF(BgL_strz00_6264, BgL_i3545z00_6437); }  else 
{ 
 obj_t BgL_auxz00_10003;
BgL_auxz00_10003 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33349L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3546z00_6438), 
(int)(BgL_i3545z00_6437)); 
FAILURE(BgL_auxz00_10003,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 827 */
 long BgL_l3550z00_6442;
BgL_l3550z00_6442 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1759z00_1827, BgL_l3550z00_6442))
{ /* Llib/unicode.scm 827 */
STRING_SET(BgL_resz00_6265, BgL_arg1759z00_1827, BgL_arg1760z00_1828); }  else 
{ 
 obj_t BgL_auxz00_10013;
BgL_auxz00_10013 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33322L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3550z00_6442), 
(int)(BgL_arg1759z00_1827)); 
FAILURE(BgL_auxz00_10013,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 828 */
 long BgL_arg1762z00_1830; unsigned char BgL_arg1763z00_1831;
BgL_arg1762z00_1830 = 
(BgL_wz00_1777+3L); 
{ /* Llib/unicode.scm 828 */
 long BgL_i3553z00_6445;
BgL_i3553z00_6445 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 828 */
 long BgL_l3554z00_6446;
BgL_l3554z00_6446 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3553z00_6445, BgL_l3554z00_6446))
{ /* Llib/unicode.scm 828 */
BgL_arg1763z00_1831 = 
STRING_REF(BgL_strz00_6264, BgL_i3553z00_6445); }  else 
{ 
 obj_t BgL_auxz00_10025;
BgL_auxz00_10025 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33413L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3554z00_6446), 
(int)(BgL_i3553z00_6445)); 
FAILURE(BgL_auxz00_10025,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 828 */
 long BgL_l3558z00_6450;
BgL_l3558z00_6450 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1762z00_1830, BgL_l3558z00_6450))
{ /* Llib/unicode.scm 828 */
STRING_SET(BgL_resz00_6265, BgL_arg1762z00_1830, BgL_arg1763z00_1831); }  else 
{ 
 obj_t BgL_auxz00_10035;
BgL_auxz00_10035 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33386L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3558z00_6450), 
(int)(BgL_arg1762z00_1830)); 
FAILURE(BgL_auxz00_10035,BFALSE,BFALSE);} } } 
BGl_loopze70ze7zz__unicodez00(BgL_endz00_6268, BgL_strictz00_6267, BgL_lenz00_6266, BgL_resz00_6265, BgL_strz00_6264, 
(BgL_rz00_1776+4L), 
(BgL_wz00_1777+4L)); 
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10047; long BgL_rz00_10045;
BgL_rz00_10045 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10047 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10047; 
BgL_rz00_1776 = BgL_rz00_10045; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 820 */
return BFALSE;} }  else 
{ /* Llib/unicode.scm 818 */
if(
(BgL_nz00_1781<=223L))
{ /* Llib/unicode.scm 835 */
 bool_t BgL_test5495z00_10051;
if(
(
(1L+BgL_rz00_1776)<BgL_lenz00_6266))
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4315;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10055;
{ /* Llib/unicode.scm 836 */
 long BgL_i3561z00_6453;
BgL_i3561z00_6453 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 836 */
 long BgL_l3562z00_6454;
BgL_l3562z00_6454 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3561z00_6453, BgL_l3562z00_6454))
{ /* Llib/unicode.scm 836 */
BgL_tmpz00_10055 = 
STRING_REF(BgL_strz00_6264, BgL_i3561z00_6453)
; }  else 
{ 
 obj_t BgL_auxz00_10061;
BgL_auxz00_10061 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33657L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3562z00_6454), 
(int)(BgL_i3561z00_6453)); 
FAILURE(BgL_auxz00_10061,BFALSE,BFALSE);} } } 
BgL_nz00_4315 = 
(BgL_tmpz00_10055); } 
if(
(BgL_nz00_4315>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5495z00_10051 = 
(BgL_nz00_4315<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5495z00_10051 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 835 */
BgL_test5495z00_10051 = ((bool_t)0)
; } 
if(BgL_test5495z00_10051)
{ /* Llib/unicode.scm 835 */
{ /* Llib/unicode.scm 838 */
 long BgL_l3566z00_6458;
BgL_l3566z00_6458 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3566z00_6458))
{ /* Llib/unicode.scm 838 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10075;
BgL_auxz00_10075 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33720L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3566z00_6458), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10075,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 839 */
 long BgL_arg1785z00_1854; unsigned char BgL_arg1786z00_1855;
BgL_arg1785z00_1854 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 839 */
 long BgL_i3569z00_6461;
BgL_i3569z00_6461 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 839 */
 long BgL_l3570z00_6462;
BgL_l3570z00_6462 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3569z00_6461, BgL_l3570z00_6462))
{ /* Llib/unicode.scm 839 */
BgL_arg1786z00_1855 = 
STRING_REF(BgL_strz00_6264, BgL_i3569z00_6461); }  else 
{ 
 obj_t BgL_auxz00_10087;
BgL_auxz00_10087 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33779L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3570z00_6462), 
(int)(BgL_i3569z00_6461)); 
FAILURE(BgL_auxz00_10087,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 839 */
 long BgL_l3574z00_6466;
BgL_l3574z00_6466 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1785z00_1854, BgL_l3574z00_6466))
{ /* Llib/unicode.scm 839 */
STRING_SET(BgL_resz00_6265, BgL_arg1785z00_1854, BgL_arg1786z00_1855); }  else 
{ 
 obj_t BgL_auxz00_10097;
BgL_auxz00_10097 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(33752L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3574z00_6466), 
(int)(BgL_arg1785z00_1854)); 
FAILURE(BgL_auxz00_10097,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_10105; long BgL_rz00_10103;
BgL_rz00_10103 = 
(BgL_rz00_1776+2L); 
BgL_wz00_10105 = 
(BgL_wz00_1777+2L); 
BgL_wz00_1777 = BgL_wz00_10105; 
BgL_rz00_1776 = BgL_rz00_10103; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 835 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10110; long BgL_rz00_10108;
BgL_rz00_10108 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10110 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10110; 
BgL_rz00_1776 = BgL_rz00_10108; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 833 */
if(
(BgL_nz00_1781<=239L))
{ /* Llib/unicode.scm 846 */
 bool_t BgL_test5504z00_10114;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-2L)))
{ /* Llib/unicode.scm 847 */
 bool_t BgL_test5506z00_10118;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4341;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10119;
{ /* Llib/unicode.scm 847 */
 long BgL_i3577z00_6469;
BgL_i3577z00_6469 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 847 */
 long BgL_l3578z00_6470;
BgL_l3578z00_6470 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3577z00_6469, BgL_l3578z00_6470))
{ /* Llib/unicode.scm 847 */
BgL_tmpz00_10119 = 
STRING_REF(BgL_strz00_6264, BgL_i3577z00_6469)
; }  else 
{ 
 obj_t BgL_auxz00_10125;
BgL_auxz00_10125 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34032L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3578z00_6470), 
(int)(BgL_i3577z00_6469)); 
FAILURE(BgL_auxz00_10125,BFALSE,BFALSE);} } } 
BgL_nz00_4341 = 
(BgL_tmpz00_10119); } 
if(
(BgL_nz00_4341>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5506z00_10118 = 
(BgL_nz00_4341<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5506z00_10118 = ((bool_t)0)
; } } 
if(BgL_test5506z00_10118)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4349;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10135;
{ /* Llib/unicode.scm 848 */
 long BgL_i3581z00_6473;
BgL_i3581z00_6473 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 848 */
 long BgL_l3582z00_6474;
BgL_l3582z00_6474 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3581z00_6473, BgL_l3582z00_6474))
{ /* Llib/unicode.scm 848 */
BgL_tmpz00_10135 = 
STRING_REF(BgL_strz00_6264, BgL_i3581z00_6473)
; }  else 
{ 
 obj_t BgL_auxz00_10141;
BgL_auxz00_10141 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34086L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3582z00_6474), 
(int)(BgL_i3581z00_6473)); 
FAILURE(BgL_auxz00_10141,BFALSE,BFALSE);} } } 
BgL_nz00_4349 = 
(BgL_tmpz00_10135); } 
if(
(BgL_nz00_4349>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5504z00_10114 = 
(BgL_nz00_4349<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5504z00_10114 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 847 */
BgL_test5504z00_10114 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 846 */
BgL_test5504z00_10114 = ((bool_t)0)
; } 
if(BgL_test5504z00_10114)
{ /* Llib/unicode.scm 846 */
{ /* Llib/unicode.scm 850 */
 long BgL_l3586z00_6478;
BgL_l3586z00_6478 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3586z00_6478))
{ /* Llib/unicode.scm 850 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10155;
BgL_auxz00_10155 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34149L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3586z00_6478), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10155,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 851 */
 long BgL_arg1807z00_1877; unsigned char BgL_arg1808z00_1878;
BgL_arg1807z00_1877 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 851 */
 long BgL_i3589z00_6481;
BgL_i3589z00_6481 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 851 */
 long BgL_l3590z00_6482;
BgL_l3590z00_6482 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3589z00_6481, BgL_l3590z00_6482))
{ /* Llib/unicode.scm 851 */
BgL_arg1808z00_1878 = 
STRING_REF(BgL_strz00_6264, BgL_i3589z00_6481); }  else 
{ 
 obj_t BgL_auxz00_10167;
BgL_auxz00_10167 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34208L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3590z00_6482), 
(int)(BgL_i3589z00_6481)); 
FAILURE(BgL_auxz00_10167,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 851 */
 long BgL_l3594z00_6486;
BgL_l3594z00_6486 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1807z00_1877, BgL_l3594z00_6486))
{ /* Llib/unicode.scm 851 */
STRING_SET(BgL_resz00_6265, BgL_arg1807z00_1877, BgL_arg1808z00_1878); }  else 
{ 
 obj_t BgL_auxz00_10177;
BgL_auxz00_10177 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34181L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3594z00_6486), 
(int)(BgL_arg1807z00_1877)); 
FAILURE(BgL_auxz00_10177,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 852 */
 long BgL_arg1810z00_1880; unsigned char BgL_arg1811z00_1881;
BgL_arg1810z00_1880 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 852 */
 long BgL_i3597z00_6489;
BgL_i3597z00_6489 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 852 */
 long BgL_l3598z00_6490;
BgL_l3598z00_6490 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3597z00_6489, BgL_l3598z00_6490))
{ /* Llib/unicode.scm 852 */
BgL_arg1811z00_1881 = 
STRING_REF(BgL_strz00_6264, BgL_i3597z00_6489); }  else 
{ 
 obj_t BgL_auxz00_10189;
BgL_auxz00_10189 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34273L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3598z00_6490), 
(int)(BgL_i3597z00_6489)); 
FAILURE(BgL_auxz00_10189,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 852 */
 long BgL_l3602z00_6494;
BgL_l3602z00_6494 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1810z00_1880, BgL_l3602z00_6494))
{ /* Llib/unicode.scm 852 */
STRING_SET(BgL_resz00_6265, BgL_arg1810z00_1880, BgL_arg1811z00_1881); }  else 
{ 
 obj_t BgL_auxz00_10199;
BgL_auxz00_10199 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34246L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3602z00_6494), 
(int)(BgL_arg1810z00_1880)); 
FAILURE(BgL_auxz00_10199,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_10207; long BgL_rz00_10205;
BgL_rz00_10205 = 
(BgL_rz00_1776+3L); 
BgL_wz00_10207 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10207; 
BgL_rz00_1776 = BgL_rz00_10205; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 846 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10212; long BgL_rz00_10210;
BgL_rz00_10210 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10212 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10212; 
BgL_rz00_1776 = BgL_rz00_10210; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 844 */
if(
(BgL_nz00_1781==240L))
{ /* Llib/unicode.scm 859 */
 bool_t BgL_test5517z00_10216;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-3L)))
{ /* Llib/unicode.scm 860 */
 bool_t BgL_test5519z00_10220;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4382;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10221;
{ /* Llib/unicode.scm 860 */
 long BgL_i3605z00_6497;
BgL_i3605z00_6497 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 860 */
 long BgL_l3606z00_6498;
BgL_l3606z00_6498 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3605z00_6497, BgL_l3606z00_6498))
{ /* Llib/unicode.scm 860 */
BgL_tmpz00_10221 = 
STRING_REF(BgL_strz00_6264, BgL_i3605z00_6497)
; }  else 
{ 
 obj_t BgL_auxz00_10227;
BgL_auxz00_10227 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34534L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3606z00_6498), 
(int)(BgL_i3605z00_6497)); 
FAILURE(BgL_auxz00_10227,BFALSE,BFALSE);} } } 
BgL_nz00_4382 = 
(BgL_tmpz00_10221); } 
if(
(BgL_nz00_4382>=144L))
{ /* Llib/unicode.scm 778 */
BgL_test5519z00_10220 = 
(BgL_nz00_4382<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5519z00_10220 = ((bool_t)0)
; } } 
if(BgL_test5519z00_10220)
{ /* Llib/unicode.scm 861 */
 bool_t BgL_test5522z00_10237;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4390;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10238;
{ /* Llib/unicode.scm 861 */
 long BgL_i3609z00_6501;
BgL_i3609z00_6501 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 861 */
 long BgL_l3610z00_6502;
BgL_l3610z00_6502 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3609z00_6501, BgL_l3610z00_6502))
{ /* Llib/unicode.scm 861 */
BgL_tmpz00_10238 = 
STRING_REF(BgL_strz00_6264, BgL_i3609z00_6501)
; }  else 
{ 
 obj_t BgL_auxz00_10244;
BgL_auxz00_10244 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34588L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3610z00_6502), 
(int)(BgL_i3609z00_6501)); 
FAILURE(BgL_auxz00_10244,BFALSE,BFALSE);} } } 
BgL_nz00_4390 = 
(BgL_tmpz00_10238); } 
if(
(BgL_nz00_4390>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5522z00_10237 = 
(BgL_nz00_4390<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5522z00_10237 = ((bool_t)0)
; } } 
if(BgL_test5522z00_10237)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4398;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10254;
{ /* Llib/unicode.scm 862 */
 long BgL_i3613z00_6505;
BgL_i3613z00_6505 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 862 */
 long BgL_l3614z00_6506;
BgL_l3614z00_6506 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3613z00_6505, BgL_l3614z00_6506))
{ /* Llib/unicode.scm 862 */
BgL_tmpz00_10254 = 
STRING_REF(BgL_strz00_6264, BgL_i3613z00_6505)
; }  else 
{ 
 obj_t BgL_auxz00_10260;
BgL_auxz00_10260 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34642L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3614z00_6506), 
(int)(BgL_i3613z00_6505)); 
FAILURE(BgL_auxz00_10260,BFALSE,BFALSE);} } } 
BgL_nz00_4398 = 
(BgL_tmpz00_10254); } 
if(
(BgL_nz00_4398>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5517z00_10216 = 
(BgL_nz00_4398<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5517z00_10216 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 861 */
BgL_test5517z00_10216 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 860 */
BgL_test5517z00_10216 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 859 */
BgL_test5517z00_10216 = ((bool_t)0)
; } 
if(BgL_test5517z00_10216)
{ /* Llib/unicode.scm 859 */
{ /* Llib/unicode.scm 864 */
 long BgL_l3618z00_6510;
BgL_l3618z00_6510 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3618z00_6510))
{ /* Llib/unicode.scm 864 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10274;
BgL_auxz00_10274 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34705L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3618z00_6510), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10274,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 865 */
 long BgL_arg1842z00_1911; unsigned char BgL_arg1843z00_1912;
BgL_arg1842z00_1911 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 865 */
 long BgL_i3621z00_6513;
BgL_i3621z00_6513 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 865 */
 long BgL_l3622z00_6514;
BgL_l3622z00_6514 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3621z00_6513, BgL_l3622z00_6514))
{ /* Llib/unicode.scm 865 */
BgL_arg1843z00_1912 = 
STRING_REF(BgL_strz00_6264, BgL_i3621z00_6513); }  else 
{ 
 obj_t BgL_auxz00_10286;
BgL_auxz00_10286 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34764L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3622z00_6514), 
(int)(BgL_i3621z00_6513)); 
FAILURE(BgL_auxz00_10286,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 865 */
 long BgL_l3626z00_6518;
BgL_l3626z00_6518 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1842z00_1911, BgL_l3626z00_6518))
{ /* Llib/unicode.scm 865 */
STRING_SET(BgL_resz00_6265, BgL_arg1842z00_1911, BgL_arg1843z00_1912); }  else 
{ 
 obj_t BgL_auxz00_10296;
BgL_auxz00_10296 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34737L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3626z00_6518), 
(int)(BgL_arg1842z00_1911)); 
FAILURE(BgL_auxz00_10296,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 866 */
 long BgL_arg1845z00_1914; unsigned char BgL_arg1846z00_1915;
BgL_arg1845z00_1914 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 866 */
 long BgL_i3629z00_6521;
BgL_i3629z00_6521 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 866 */
 long BgL_l3630z00_6522;
BgL_l3630z00_6522 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3629z00_6521, BgL_l3630z00_6522))
{ /* Llib/unicode.scm 866 */
BgL_arg1846z00_1915 = 
STRING_REF(BgL_strz00_6264, BgL_i3629z00_6521); }  else 
{ 
 obj_t BgL_auxz00_10308;
BgL_auxz00_10308 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34829L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3630z00_6522), 
(int)(BgL_i3629z00_6521)); 
FAILURE(BgL_auxz00_10308,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 866 */
 long BgL_l3634z00_6526;
BgL_l3634z00_6526 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1845z00_1914, BgL_l3634z00_6526))
{ /* Llib/unicode.scm 866 */
STRING_SET(BgL_resz00_6265, BgL_arg1845z00_1914, BgL_arg1846z00_1915); }  else 
{ 
 obj_t BgL_auxz00_10318;
BgL_auxz00_10318 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34802L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3634z00_6526), 
(int)(BgL_arg1845z00_1914)); 
FAILURE(BgL_auxz00_10318,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 867 */
 long BgL_arg1848z00_1917; unsigned char BgL_arg1849z00_1918;
BgL_arg1848z00_1917 = 
(BgL_wz00_1777+3L); 
{ /* Llib/unicode.scm 867 */
 long BgL_i3637z00_6529;
BgL_i3637z00_6529 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 867 */
 long BgL_l3638z00_6530;
BgL_l3638z00_6530 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3637z00_6529, BgL_l3638z00_6530))
{ /* Llib/unicode.scm 867 */
BgL_arg1849z00_1918 = 
STRING_REF(BgL_strz00_6264, BgL_i3637z00_6529); }  else 
{ 
 obj_t BgL_auxz00_10330;
BgL_auxz00_10330 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34894L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3638z00_6530), 
(int)(BgL_i3637z00_6529)); 
FAILURE(BgL_auxz00_10330,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 867 */
 long BgL_l3642z00_6534;
BgL_l3642z00_6534 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1848z00_1917, BgL_l3642z00_6534))
{ /* Llib/unicode.scm 867 */
STRING_SET(BgL_resz00_6265, BgL_arg1848z00_1917, BgL_arg1849z00_1918); }  else 
{ 
 obj_t BgL_auxz00_10340;
BgL_auxz00_10340 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(34867L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3642z00_6534), 
(int)(BgL_arg1848z00_1917)); 
FAILURE(BgL_auxz00_10340,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_10348; long BgL_rz00_10346;
BgL_rz00_10346 = 
(BgL_rz00_1776+4L); 
BgL_wz00_10348 = 
(BgL_wz00_1777+4L); 
BgL_wz00_1777 = BgL_wz00_10348; 
BgL_rz00_1776 = BgL_rz00_10346; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 859 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10353; long BgL_rz00_10351;
BgL_rz00_10351 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10353 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10353; 
BgL_rz00_1776 = BgL_rz00_10351; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 872 */
 bool_t BgL_test5534z00_10355;
if(
(BgL_nz00_1781==244L))
{ /* Llib/unicode.scm 872 */
BgL_test5534z00_10355 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 873 */
 bool_t BgL_test5536z00_10358;
{ /* Llib/unicode.scm 873 */
 bool_t BgL__ortest_1051z00_2132;
BgL__ortest_1051z00_2132 = 
(BgL_nz00_1781==248L); 
if(BgL__ortest_1051z00_2132)
{ /* Llib/unicode.scm 873 */
BgL_test5536z00_10358 = BgL__ortest_1051z00_2132
; }  else 
{ /* Llib/unicode.scm 873 */
BgL_test5536z00_10358 = 
(BgL_nz00_1781==252L)
; } } 
if(BgL_test5536z00_10358)
{ /* Llib/unicode.scm 873 */
if(BgL_strictz00_6267)
{ /* Llib/unicode.scm 873 */
BgL_test5534z00_10355 = ((bool_t)0)
; }  else 
{ /* Llib/unicode.scm 873 */
BgL_test5534z00_10355 = ((bool_t)1)
; } }  else 
{ /* Llib/unicode.scm 873 */
BgL_test5534z00_10355 = ((bool_t)0)
; } } 
if(BgL_test5534z00_10355)
{ /* Llib/unicode.scm 875 */
 bool_t BgL_test5539z00_10363;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-3L)))
{ /* Llib/unicode.scm 876 */
 bool_t BgL_test5541z00_10367;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4440;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10368;
{ /* Llib/unicode.scm 876 */
 long BgL_i3645z00_6537;
BgL_i3645z00_6537 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 876 */
 long BgL_l3646z00_6538;
BgL_l3646z00_6538 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3645z00_6537, BgL_l3646z00_6538))
{ /* Llib/unicode.scm 876 */
BgL_tmpz00_10368 = 
STRING_REF(BgL_strz00_6264, BgL_i3645z00_6537)
; }  else 
{ 
 obj_t BgL_auxz00_10374;
BgL_auxz00_10374 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35217L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3646z00_6538), 
(int)(BgL_i3645z00_6537)); 
FAILURE(BgL_auxz00_10374,BFALSE,BFALSE);} } } 
BgL_nz00_4440 = 
(BgL_tmpz00_10368); } 
if(
(BgL_nz00_4440>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5541z00_10367 = 
(BgL_nz00_4440<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5541z00_10367 = ((bool_t)0)
; } } 
if(BgL_test5541z00_10367)
{ /* Llib/unicode.scm 877 */
 bool_t BgL_test5544z00_10384;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4448;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10385;
{ /* Llib/unicode.scm 877 */
 long BgL_i3649z00_6541;
BgL_i3649z00_6541 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 877 */
 long BgL_l3650z00_6542;
BgL_l3650z00_6542 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3649z00_6541, BgL_l3650z00_6542))
{ /* Llib/unicode.scm 877 */
BgL_tmpz00_10385 = 
STRING_REF(BgL_strz00_6264, BgL_i3649z00_6541)
; }  else 
{ 
 obj_t BgL_auxz00_10391;
BgL_auxz00_10391 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35271L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3650z00_6542), 
(int)(BgL_i3649z00_6541)); 
FAILURE(BgL_auxz00_10391,BFALSE,BFALSE);} } } 
BgL_nz00_4448 = 
(BgL_tmpz00_10385); } 
if(
(BgL_nz00_4448>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5544z00_10384 = 
(BgL_nz00_4448<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5544z00_10384 = ((bool_t)0)
; } } 
if(BgL_test5544z00_10384)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4456;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10401;
{ /* Llib/unicode.scm 878 */
 long BgL_i3653z00_6545;
BgL_i3653z00_6545 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 878 */
 long BgL_l3654z00_6546;
BgL_l3654z00_6546 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3653z00_6545, BgL_l3654z00_6546))
{ /* Llib/unicode.scm 878 */
BgL_tmpz00_10401 = 
STRING_REF(BgL_strz00_6264, BgL_i3653z00_6545)
; }  else 
{ 
 obj_t BgL_auxz00_10407;
BgL_auxz00_10407 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35325L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3654z00_6546), 
(int)(BgL_i3653z00_6545)); 
FAILURE(BgL_auxz00_10407,BFALSE,BFALSE);} } } 
BgL_nz00_4456 = 
(BgL_tmpz00_10401); } 
if(
(BgL_nz00_4456>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5539z00_10363 = 
(BgL_nz00_4456<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5539z00_10363 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 877 */
BgL_test5539z00_10363 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 876 */
BgL_test5539z00_10363 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 875 */
BgL_test5539z00_10363 = ((bool_t)0)
; } 
if(BgL_test5539z00_10363)
{ /* Llib/unicode.scm 875 */
{ /* Llib/unicode.scm 880 */
 long BgL_l3658z00_6550;
BgL_l3658z00_6550 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3658z00_6550))
{ /* Llib/unicode.scm 880 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10421;
BgL_auxz00_10421 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35388L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3658z00_6550), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10421,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 881 */
 long BgL_arg1884z00_1954; unsigned char BgL_arg1885z00_1955;
BgL_arg1884z00_1954 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 881 */
 long BgL_i3661z00_6553;
BgL_i3661z00_6553 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 881 */
 long BgL_l3662z00_6554;
BgL_l3662z00_6554 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3661z00_6553, BgL_l3662z00_6554))
{ /* Llib/unicode.scm 881 */
BgL_arg1885z00_1955 = 
STRING_REF(BgL_strz00_6264, BgL_i3661z00_6553); }  else 
{ 
 obj_t BgL_auxz00_10433;
BgL_auxz00_10433 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35447L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3662z00_6554), 
(int)(BgL_i3661z00_6553)); 
FAILURE(BgL_auxz00_10433,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 881 */
 long BgL_l3666z00_6558;
BgL_l3666z00_6558 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1884z00_1954, BgL_l3666z00_6558))
{ /* Llib/unicode.scm 881 */
STRING_SET(BgL_resz00_6265, BgL_arg1884z00_1954, BgL_arg1885z00_1955); }  else 
{ 
 obj_t BgL_auxz00_10443;
BgL_auxz00_10443 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35420L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3666z00_6558), 
(int)(BgL_arg1884z00_1954)); 
FAILURE(BgL_auxz00_10443,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 882 */
 long BgL_arg1888z00_1957; unsigned char BgL_arg1889z00_1958;
BgL_arg1888z00_1957 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 882 */
 long BgL_i3669z00_6561;
BgL_i3669z00_6561 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 882 */
 long BgL_l3670z00_6562;
BgL_l3670z00_6562 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3669z00_6561, BgL_l3670z00_6562))
{ /* Llib/unicode.scm 882 */
BgL_arg1889z00_1958 = 
STRING_REF(BgL_strz00_6264, BgL_i3669z00_6561); }  else 
{ 
 obj_t BgL_auxz00_10455;
BgL_auxz00_10455 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35512L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3670z00_6562), 
(int)(BgL_i3669z00_6561)); 
FAILURE(BgL_auxz00_10455,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 882 */
 long BgL_l3674z00_6566;
BgL_l3674z00_6566 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1888z00_1957, BgL_l3674z00_6566))
{ /* Llib/unicode.scm 882 */
STRING_SET(BgL_resz00_6265, BgL_arg1888z00_1957, BgL_arg1889z00_1958); }  else 
{ 
 obj_t BgL_auxz00_10465;
BgL_auxz00_10465 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35485L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3674z00_6566), 
(int)(BgL_arg1888z00_1957)); 
FAILURE(BgL_auxz00_10465,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 883 */
 long BgL_arg1891z00_1960; unsigned char BgL_arg1892z00_1961;
BgL_arg1891z00_1960 = 
(BgL_wz00_1777+3L); 
{ /* Llib/unicode.scm 883 */
 long BgL_i3677z00_6569;
BgL_i3677z00_6569 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 883 */
 long BgL_l3678z00_6570;
BgL_l3678z00_6570 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3677z00_6569, BgL_l3678z00_6570))
{ /* Llib/unicode.scm 883 */
BgL_arg1892z00_1961 = 
STRING_REF(BgL_strz00_6264, BgL_i3677z00_6569); }  else 
{ 
 obj_t BgL_auxz00_10477;
BgL_auxz00_10477 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35577L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3678z00_6570), 
(int)(BgL_i3677z00_6569)); 
FAILURE(BgL_auxz00_10477,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 883 */
 long BgL_l3682z00_6574;
BgL_l3682z00_6574 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1891z00_1960, BgL_l3682z00_6574))
{ /* Llib/unicode.scm 883 */
STRING_SET(BgL_resz00_6265, BgL_arg1891z00_1960, BgL_arg1892z00_1961); }  else 
{ 
 obj_t BgL_auxz00_10487;
BgL_auxz00_10487 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35550L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3682z00_6574), 
(int)(BgL_arg1891z00_1960)); 
FAILURE(BgL_auxz00_10487,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_10495; long BgL_rz00_10493;
BgL_rz00_10493 = 
(BgL_rz00_1776+4L); 
BgL_wz00_10495 = 
(BgL_wz00_1777+4L); 
BgL_wz00_1777 = BgL_wz00_10495; 
BgL_rz00_1776 = BgL_rz00_10493; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 875 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10500; long BgL_rz00_10498;
BgL_rz00_10498 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10500 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10500; 
BgL_rz00_1776 = BgL_rz00_10498; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 872 */
if(
(BgL_nz00_1781<=247L))
{ /* Llib/unicode.scm 890 */
 bool_t BgL_test5557z00_10504;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-3L)))
{ /* Llib/unicode.scm 891 */
 bool_t BgL_test5559z00_10508;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4496;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10509;
{ /* Llib/unicode.scm 891 */
 long BgL_i3685z00_6577;
BgL_i3685z00_6577 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 891 */
 long BgL_l3686z00_6578;
BgL_l3686z00_6578 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3685z00_6577, BgL_l3686z00_6578))
{ /* Llib/unicode.scm 891 */
BgL_tmpz00_10509 = 
STRING_REF(BgL_strz00_6264, BgL_i3685z00_6577)
; }  else 
{ 
 obj_t BgL_auxz00_10515;
BgL_auxz00_10515 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35830L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3686z00_6578), 
(int)(BgL_i3685z00_6577)); 
FAILURE(BgL_auxz00_10515,BFALSE,BFALSE);} } } 
BgL_nz00_4496 = 
(BgL_tmpz00_10509); } 
if(
(BgL_nz00_4496>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5559z00_10508 = 
(BgL_nz00_4496<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5559z00_10508 = ((bool_t)0)
; } } 
if(BgL_test5559z00_10508)
{ /* Llib/unicode.scm 892 */
 bool_t BgL_test5562z00_10525;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4504;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10526;
{ /* Llib/unicode.scm 892 */
 long BgL_i3689z00_6581;
BgL_i3689z00_6581 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 892 */
 long BgL_l3690z00_6582;
BgL_l3690z00_6582 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3689z00_6581, BgL_l3690z00_6582))
{ /* Llib/unicode.scm 892 */
BgL_tmpz00_10526 = 
STRING_REF(BgL_strz00_6264, BgL_i3689z00_6581)
; }  else 
{ 
 obj_t BgL_auxz00_10532;
BgL_auxz00_10532 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35884L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3690z00_6582), 
(int)(BgL_i3689z00_6581)); 
FAILURE(BgL_auxz00_10532,BFALSE,BFALSE);} } } 
BgL_nz00_4504 = 
(BgL_tmpz00_10526); } 
if(
(BgL_nz00_4504>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5562z00_10525 = 
(BgL_nz00_4504<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5562z00_10525 = ((bool_t)0)
; } } 
if(BgL_test5562z00_10525)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4512;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10542;
{ /* Llib/unicode.scm 893 */
 long BgL_i3693z00_6585;
BgL_i3693z00_6585 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 893 */
 long BgL_l3694z00_6586;
BgL_l3694z00_6586 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3693z00_6585, BgL_l3694z00_6586))
{ /* Llib/unicode.scm 893 */
BgL_tmpz00_10542 = 
STRING_REF(BgL_strz00_6264, BgL_i3693z00_6585)
; }  else 
{ 
 obj_t BgL_auxz00_10548;
BgL_auxz00_10548 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(35938L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3694z00_6586), 
(int)(BgL_i3693z00_6585)); 
FAILURE(BgL_auxz00_10548,BFALSE,BFALSE);} } } 
BgL_nz00_4512 = 
(BgL_tmpz00_10542); } 
if(
(BgL_nz00_4512>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5557z00_10504 = 
(BgL_nz00_4512<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5557z00_10504 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 892 */
BgL_test5557z00_10504 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 891 */
BgL_test5557z00_10504 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 890 */
BgL_test5557z00_10504 = ((bool_t)0)
; } 
if(BgL_test5557z00_10504)
{ /* Llib/unicode.scm 890 */
{ /* Llib/unicode.scm 895 */
 long BgL_l3698z00_6590;
BgL_l3698z00_6590 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3698z00_6590))
{ /* Llib/unicode.scm 895 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10562;
BgL_auxz00_10562 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36001L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3698z00_6590), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10562,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 896 */
 long BgL_arg1931z00_1994; unsigned char BgL_arg1932z00_1995;
BgL_arg1931z00_1994 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 896 */
 long BgL_i3701z00_6593;
BgL_i3701z00_6593 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 896 */
 long BgL_l3702z00_6594;
BgL_l3702z00_6594 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3701z00_6593, BgL_l3702z00_6594))
{ /* Llib/unicode.scm 896 */
BgL_arg1932z00_1995 = 
STRING_REF(BgL_strz00_6264, BgL_i3701z00_6593); }  else 
{ 
 obj_t BgL_auxz00_10574;
BgL_auxz00_10574 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36060L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3702z00_6594), 
(int)(BgL_i3701z00_6593)); 
FAILURE(BgL_auxz00_10574,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 896 */
 long BgL_l3706z00_6598;
BgL_l3706z00_6598 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1931z00_1994, BgL_l3706z00_6598))
{ /* Llib/unicode.scm 896 */
STRING_SET(BgL_resz00_6265, BgL_arg1931z00_1994, BgL_arg1932z00_1995); }  else 
{ 
 obj_t BgL_auxz00_10584;
BgL_auxz00_10584 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36033L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3706z00_6598), 
(int)(BgL_arg1931z00_1994)); 
FAILURE(BgL_auxz00_10584,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 897 */
 long BgL_arg1934z00_1997; unsigned char BgL_arg1935z00_1998;
BgL_arg1934z00_1997 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 897 */
 long BgL_i3709z00_6601;
BgL_i3709z00_6601 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 897 */
 long BgL_l3710z00_6602;
BgL_l3710z00_6602 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3709z00_6601, BgL_l3710z00_6602))
{ /* Llib/unicode.scm 897 */
BgL_arg1935z00_1998 = 
STRING_REF(BgL_strz00_6264, BgL_i3709z00_6601); }  else 
{ 
 obj_t BgL_auxz00_10596;
BgL_auxz00_10596 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36125L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3710z00_6602), 
(int)(BgL_i3709z00_6601)); 
FAILURE(BgL_auxz00_10596,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 897 */
 long BgL_l3714z00_6606;
BgL_l3714z00_6606 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1934z00_1997, BgL_l3714z00_6606))
{ /* Llib/unicode.scm 897 */
STRING_SET(BgL_resz00_6265, BgL_arg1934z00_1997, BgL_arg1935z00_1998); }  else 
{ 
 obj_t BgL_auxz00_10606;
BgL_auxz00_10606 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36098L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3714z00_6606), 
(int)(BgL_arg1934z00_1997)); 
FAILURE(BgL_auxz00_10606,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 898 */
 long BgL_arg1937z00_2000; unsigned char BgL_arg1938z00_2001;
BgL_arg1937z00_2000 = 
(BgL_wz00_1777+3L); 
{ /* Llib/unicode.scm 898 */
 long BgL_i3717z00_6609;
BgL_i3717z00_6609 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 898 */
 long BgL_l3718z00_6610;
BgL_l3718z00_6610 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3717z00_6609, BgL_l3718z00_6610))
{ /* Llib/unicode.scm 898 */
BgL_arg1938z00_2001 = 
STRING_REF(BgL_strz00_6264, BgL_i3717z00_6609); }  else 
{ 
 obj_t BgL_auxz00_10618;
BgL_auxz00_10618 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36190L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3718z00_6610), 
(int)(BgL_i3717z00_6609)); 
FAILURE(BgL_auxz00_10618,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 898 */
 long BgL_l3722z00_6614;
BgL_l3722z00_6614 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1937z00_2000, BgL_l3722z00_6614))
{ /* Llib/unicode.scm 898 */
STRING_SET(BgL_resz00_6265, BgL_arg1937z00_2000, BgL_arg1938z00_2001); }  else 
{ 
 obj_t BgL_auxz00_10628;
BgL_auxz00_10628 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36163L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3722z00_6614), 
(int)(BgL_arg1937z00_2000)); 
FAILURE(BgL_auxz00_10628,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_10636; long BgL_rz00_10634;
BgL_rz00_10634 = 
(BgL_rz00_1776+4L); 
BgL_wz00_10636 = 
(BgL_wz00_1777+4L); 
BgL_wz00_1777 = BgL_wz00_10636; 
BgL_rz00_1776 = BgL_rz00_10634; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 890 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10641; long BgL_rz00_10639;
BgL_rz00_10639 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10641 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10641; 
BgL_rz00_1776 = BgL_rz00_10639; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 888 */
if(
(BgL_nz00_1781<=251L))
{ /* Llib/unicode.scm 904 */
 bool_t BgL_test5575z00_10645;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-4L)))
{ /* Llib/unicode.scm 905 */
 bool_t BgL_test5577z00_10649;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4552;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10650;
{ /* Llib/unicode.scm 905 */
 long BgL_i3725z00_6617;
BgL_i3725z00_6617 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 905 */
 long BgL_l3726z00_6618;
BgL_l3726z00_6618 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3725z00_6617, BgL_l3726z00_6618))
{ /* Llib/unicode.scm 905 */
BgL_tmpz00_10650 = 
STRING_REF(BgL_strz00_6264, BgL_i3725z00_6617)
; }  else 
{ 
 obj_t BgL_auxz00_10656;
BgL_auxz00_10656 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36420L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3726z00_6618), 
(int)(BgL_i3725z00_6617)); 
FAILURE(BgL_auxz00_10656,BFALSE,BFALSE);} } } 
BgL_nz00_4552 = 
(BgL_tmpz00_10650); } 
if(
(BgL_nz00_4552>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5577z00_10649 = 
(BgL_nz00_4552<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5577z00_10649 = ((bool_t)0)
; } } 
if(BgL_test5577z00_10649)
{ /* Llib/unicode.scm 906 */
 bool_t BgL_test5580z00_10666;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4560;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10667;
{ /* Llib/unicode.scm 906 */
 long BgL_i3729z00_6621;
BgL_i3729z00_6621 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 906 */
 long BgL_l3730z00_6622;
BgL_l3730z00_6622 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3729z00_6621, BgL_l3730z00_6622))
{ /* Llib/unicode.scm 906 */
BgL_tmpz00_10667 = 
STRING_REF(BgL_strz00_6264, BgL_i3729z00_6621)
; }  else 
{ 
 obj_t BgL_auxz00_10673;
BgL_auxz00_10673 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36474L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3730z00_6622), 
(int)(BgL_i3729z00_6621)); 
FAILURE(BgL_auxz00_10673,BFALSE,BFALSE);} } } 
BgL_nz00_4560 = 
(BgL_tmpz00_10667); } 
if(
(BgL_nz00_4560>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5580z00_10666 = 
(BgL_nz00_4560<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5580z00_10666 = ((bool_t)0)
; } } 
if(BgL_test5580z00_10666)
{ /* Llib/unicode.scm 907 */
 bool_t BgL_test5583z00_10683;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4568;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10684;
{ /* Llib/unicode.scm 907 */
 long BgL_i3733z00_6625;
BgL_i3733z00_6625 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 907 */
 long BgL_l3734z00_6626;
BgL_l3734z00_6626 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3733z00_6625, BgL_l3734z00_6626))
{ /* Llib/unicode.scm 907 */
BgL_tmpz00_10684 = 
STRING_REF(BgL_strz00_6264, BgL_i3733z00_6625)
; }  else 
{ 
 obj_t BgL_auxz00_10690;
BgL_auxz00_10690 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36528L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3734z00_6626), 
(int)(BgL_i3733z00_6625)); 
FAILURE(BgL_auxz00_10690,BFALSE,BFALSE);} } } 
BgL_nz00_4568 = 
(BgL_tmpz00_10684); } 
if(
(BgL_nz00_4568>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5583z00_10683 = 
(BgL_nz00_4568<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5583z00_10683 = ((bool_t)0)
; } } 
if(BgL_test5583z00_10683)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4576;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10700;
{ /* Llib/unicode.scm 908 */
 long BgL_i3737z00_6629;
BgL_i3737z00_6629 = 
(BgL_rz00_1776+4L); 
{ /* Llib/unicode.scm 908 */
 long BgL_l3738z00_6630;
BgL_l3738z00_6630 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3737z00_6629, BgL_l3738z00_6630))
{ /* Llib/unicode.scm 908 */
BgL_tmpz00_10700 = 
STRING_REF(BgL_strz00_6264, BgL_i3737z00_6629)
; }  else 
{ 
 obj_t BgL_auxz00_10706;
BgL_auxz00_10706 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36582L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3738z00_6630), 
(int)(BgL_i3737z00_6629)); 
FAILURE(BgL_auxz00_10706,BFALSE,BFALSE);} } } 
BgL_nz00_4576 = 
(BgL_tmpz00_10700); } 
if(
(BgL_nz00_4576>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5575z00_10645 = 
(BgL_nz00_4576<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5575z00_10645 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 907 */
BgL_test5575z00_10645 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 906 */
BgL_test5575z00_10645 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 905 */
BgL_test5575z00_10645 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 904 */
BgL_test5575z00_10645 = ((bool_t)0)
; } 
if(BgL_test5575z00_10645)
{ /* Llib/unicode.scm 904 */
{ /* Llib/unicode.scm 910 */
 long BgL_l3742z00_6634;
BgL_l3742z00_6634 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3742z00_6634))
{ /* Llib/unicode.scm 910 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10720;
BgL_auxz00_10720 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36645L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3742z00_6634), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10720,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 911 */
 long BgL_arg1973z00_2039; unsigned char BgL_arg1974z00_2040;
BgL_arg1973z00_2039 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 911 */
 long BgL_i3745z00_6637;
BgL_i3745z00_6637 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 911 */
 long BgL_l3746z00_6638;
BgL_l3746z00_6638 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3745z00_6637, BgL_l3746z00_6638))
{ /* Llib/unicode.scm 911 */
BgL_arg1974z00_2040 = 
STRING_REF(BgL_strz00_6264, BgL_i3745z00_6637); }  else 
{ 
 obj_t BgL_auxz00_10732;
BgL_auxz00_10732 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36704L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3746z00_6638), 
(int)(BgL_i3745z00_6637)); 
FAILURE(BgL_auxz00_10732,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 911 */
 long BgL_l3750z00_6642;
BgL_l3750z00_6642 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1973z00_2039, BgL_l3750z00_6642))
{ /* Llib/unicode.scm 911 */
STRING_SET(BgL_resz00_6265, BgL_arg1973z00_2039, BgL_arg1974z00_2040); }  else 
{ 
 obj_t BgL_auxz00_10742;
BgL_auxz00_10742 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36677L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3750z00_6642), 
(int)(BgL_arg1973z00_2039)); 
FAILURE(BgL_auxz00_10742,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 912 */
 long BgL_arg1976z00_2042; unsigned char BgL_arg1977z00_2043;
BgL_arg1976z00_2042 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 912 */
 long BgL_i3753z00_6645;
BgL_i3753z00_6645 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 912 */
 long BgL_l3754z00_6646;
BgL_l3754z00_6646 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3753z00_6645, BgL_l3754z00_6646))
{ /* Llib/unicode.scm 912 */
BgL_arg1977z00_2043 = 
STRING_REF(BgL_strz00_6264, BgL_i3753z00_6645); }  else 
{ 
 obj_t BgL_auxz00_10754;
BgL_auxz00_10754 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36769L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3754z00_6646), 
(int)(BgL_i3753z00_6645)); 
FAILURE(BgL_auxz00_10754,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 912 */
 long BgL_l3758z00_6650;
BgL_l3758z00_6650 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1976z00_2042, BgL_l3758z00_6650))
{ /* Llib/unicode.scm 912 */
STRING_SET(BgL_resz00_6265, BgL_arg1976z00_2042, BgL_arg1977z00_2043); }  else 
{ 
 obj_t BgL_auxz00_10764;
BgL_auxz00_10764 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36742L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3758z00_6650), 
(int)(BgL_arg1976z00_2042)); 
FAILURE(BgL_auxz00_10764,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 913 */
 long BgL_arg1979z00_2045; unsigned char BgL_arg1980z00_2046;
BgL_arg1979z00_2045 = 
(BgL_wz00_1777+3L); 
{ /* Llib/unicode.scm 913 */
 long BgL_i3761z00_6653;
BgL_i3761z00_6653 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 913 */
 long BgL_l3762z00_6654;
BgL_l3762z00_6654 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3761z00_6653, BgL_l3762z00_6654))
{ /* Llib/unicode.scm 913 */
BgL_arg1980z00_2046 = 
STRING_REF(BgL_strz00_6264, BgL_i3761z00_6653); }  else 
{ 
 obj_t BgL_auxz00_10776;
BgL_auxz00_10776 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36834L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3762z00_6654), 
(int)(BgL_i3761z00_6653)); 
FAILURE(BgL_auxz00_10776,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 913 */
 long BgL_l3766z00_6658;
BgL_l3766z00_6658 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1979z00_2045, BgL_l3766z00_6658))
{ /* Llib/unicode.scm 913 */
STRING_SET(BgL_resz00_6265, BgL_arg1979z00_2045, BgL_arg1980z00_2046); }  else 
{ 
 obj_t BgL_auxz00_10786;
BgL_auxz00_10786 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36807L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3766z00_6658), 
(int)(BgL_arg1979z00_2045)); 
FAILURE(BgL_auxz00_10786,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 914 */
 long BgL_arg1982z00_2048; unsigned char BgL_arg1983z00_2049;
BgL_arg1982z00_2048 = 
(BgL_wz00_1777+4L); 
{ /* Llib/unicode.scm 914 */
 long BgL_i3769z00_6661;
BgL_i3769z00_6661 = 
(BgL_rz00_1776+4L); 
{ /* Llib/unicode.scm 914 */
 long BgL_l3770z00_6662;
BgL_l3770z00_6662 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3769z00_6661, BgL_l3770z00_6662))
{ /* Llib/unicode.scm 914 */
BgL_arg1983z00_2049 = 
STRING_REF(BgL_strz00_6264, BgL_i3769z00_6661); }  else 
{ 
 obj_t BgL_auxz00_10798;
BgL_auxz00_10798 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36899L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3770z00_6662), 
(int)(BgL_i3769z00_6661)); 
FAILURE(BgL_auxz00_10798,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 914 */
 long BgL_l3774z00_6666;
BgL_l3774z00_6666 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg1982z00_2048, BgL_l3774z00_6666))
{ /* Llib/unicode.scm 914 */
STRING_SET(BgL_resz00_6265, BgL_arg1982z00_2048, BgL_arg1983z00_2049); }  else 
{ 
 obj_t BgL_auxz00_10808;
BgL_auxz00_10808 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(36872L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3774z00_6666), 
(int)(BgL_arg1982z00_2048)); 
FAILURE(BgL_auxz00_10808,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_10816; long BgL_rz00_10814;
BgL_rz00_10814 = 
(BgL_rz00_1776+5L); 
BgL_wz00_10816 = 
(BgL_wz00_1777+5L); 
BgL_wz00_1777 = BgL_wz00_10816; 
BgL_rz00_1776 = BgL_rz00_10814; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 904 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_10821; long BgL_rz00_10819;
BgL_rz00_10819 = 
(BgL_rz00_1776+1L); 
BgL_wz00_10821 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_10821; 
BgL_rz00_1776 = BgL_rz00_10819; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 903 */
if(
(BgL_nz00_1781<=253L))
{ /* Llib/unicode.scm 920 */
 bool_t BgL_test5598z00_10825;
if(
(BgL_rz00_1776<
(BgL_lenz00_6266-5L)))
{ /* Llib/unicode.scm 921 */
 bool_t BgL_test5600z00_10829;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4623;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10830;
{ /* Llib/unicode.scm 921 */
 long BgL_i3777z00_6669;
BgL_i3777z00_6669 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 921 */
 long BgL_l3778z00_6670;
BgL_l3778z00_6670 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3777z00_6669, BgL_l3778z00_6670))
{ /* Llib/unicode.scm 921 */
BgL_tmpz00_10830 = 
STRING_REF(BgL_strz00_6264, BgL_i3777z00_6669)
; }  else 
{ 
 obj_t BgL_auxz00_10836;
BgL_auxz00_10836 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37129L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3778z00_6670), 
(int)(BgL_i3777z00_6669)); 
FAILURE(BgL_auxz00_10836,BFALSE,BFALSE);} } } 
BgL_nz00_4623 = 
(BgL_tmpz00_10830); } 
if(
(BgL_nz00_4623>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5600z00_10829 = 
(BgL_nz00_4623<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5600z00_10829 = ((bool_t)0)
; } } 
if(BgL_test5600z00_10829)
{ /* Llib/unicode.scm 922 */
 bool_t BgL_test5603z00_10846;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4631;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10847;
{ /* Llib/unicode.scm 922 */
 long BgL_i3781z00_6673;
BgL_i3781z00_6673 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 922 */
 long BgL_l3782z00_6674;
BgL_l3782z00_6674 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3781z00_6673, BgL_l3782z00_6674))
{ /* Llib/unicode.scm 922 */
BgL_tmpz00_10847 = 
STRING_REF(BgL_strz00_6264, BgL_i3781z00_6673)
; }  else 
{ 
 obj_t BgL_auxz00_10853;
BgL_auxz00_10853 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37183L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3782z00_6674), 
(int)(BgL_i3781z00_6673)); 
FAILURE(BgL_auxz00_10853,BFALSE,BFALSE);} } } 
BgL_nz00_4631 = 
(BgL_tmpz00_10847); } 
if(
(BgL_nz00_4631>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5603z00_10846 = 
(BgL_nz00_4631<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5603z00_10846 = ((bool_t)0)
; } } 
if(BgL_test5603z00_10846)
{ /* Llib/unicode.scm 923 */
 bool_t BgL_test5606z00_10863;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4639;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10864;
{ /* Llib/unicode.scm 923 */
 long BgL_i3785z00_6677;
BgL_i3785z00_6677 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 923 */
 long BgL_l3786z00_6678;
BgL_l3786z00_6678 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3785z00_6677, BgL_l3786z00_6678))
{ /* Llib/unicode.scm 923 */
BgL_tmpz00_10864 = 
STRING_REF(BgL_strz00_6264, BgL_i3785z00_6677)
; }  else 
{ 
 obj_t BgL_auxz00_10870;
BgL_auxz00_10870 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37237L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3786z00_6678), 
(int)(BgL_i3785z00_6677)); 
FAILURE(BgL_auxz00_10870,BFALSE,BFALSE);} } } 
BgL_nz00_4639 = 
(BgL_tmpz00_10864); } 
if(
(BgL_nz00_4639>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5606z00_10863 = 
(BgL_nz00_4639<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5606z00_10863 = ((bool_t)0)
; } } 
if(BgL_test5606z00_10863)
{ /* Llib/unicode.scm 924 */
 bool_t BgL_test5609z00_10880;
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4647;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10881;
{ /* Llib/unicode.scm 924 */
 long BgL_i3789z00_6681;
BgL_i3789z00_6681 = 
(BgL_rz00_1776+4L); 
{ /* Llib/unicode.scm 924 */
 long BgL_l3790z00_6682;
BgL_l3790z00_6682 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3789z00_6681, BgL_l3790z00_6682))
{ /* Llib/unicode.scm 924 */
BgL_tmpz00_10881 = 
STRING_REF(BgL_strz00_6264, BgL_i3789z00_6681)
; }  else 
{ 
 obj_t BgL_auxz00_10887;
BgL_auxz00_10887 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37291L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3790z00_6682), 
(int)(BgL_i3789z00_6681)); 
FAILURE(BgL_auxz00_10887,BFALSE,BFALSE);} } } 
BgL_nz00_4647 = 
(BgL_tmpz00_10881); } 
if(
(BgL_nz00_4647>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5609z00_10880 = 
(BgL_nz00_4647<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5609z00_10880 = ((bool_t)0)
; } } 
if(BgL_test5609z00_10880)
{ /* Llib/unicode.scm 777 */
 long BgL_nz00_4655;
{ /* Llib/unicode.scm 777 */
 unsigned char BgL_tmpz00_10897;
{ /* Llib/unicode.scm 925 */
 long BgL_i3793z00_6685;
BgL_i3793z00_6685 = 
(BgL_rz00_1776+5L); 
{ /* Llib/unicode.scm 925 */
 long BgL_l3794z00_6686;
BgL_l3794z00_6686 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3793z00_6685, BgL_l3794z00_6686))
{ /* Llib/unicode.scm 925 */
BgL_tmpz00_10897 = 
STRING_REF(BgL_strz00_6264, BgL_i3793z00_6685)
; }  else 
{ 
 obj_t BgL_auxz00_10903;
BgL_auxz00_10903 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37345L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3794z00_6686), 
(int)(BgL_i3793z00_6685)); 
FAILURE(BgL_auxz00_10903,BFALSE,BFALSE);} } } 
BgL_nz00_4655 = 
(BgL_tmpz00_10897); } 
if(
(BgL_nz00_4655>=128L))
{ /* Llib/unicode.scm 778 */
BgL_test5598z00_10825 = 
(BgL_nz00_4655<=191L)
; }  else 
{ /* Llib/unicode.scm 778 */
BgL_test5598z00_10825 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 924 */
BgL_test5598z00_10825 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 923 */
BgL_test5598z00_10825 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 922 */
BgL_test5598z00_10825 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 921 */
BgL_test5598z00_10825 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 920 */
BgL_test5598z00_10825 = ((bool_t)0)
; } 
if(BgL_test5598z00_10825)
{ /* Llib/unicode.scm 920 */
{ /* Llib/unicode.scm 927 */
 long BgL_l3798z00_6690;
BgL_l3798z00_6690 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_wz00_1777, BgL_l3798z00_6690))
{ /* Llib/unicode.scm 927 */
STRING_SET(BgL_resz00_6265, BgL_wz00_1777, BgL_cz00_1780); }  else 
{ 
 obj_t BgL_auxz00_10917;
BgL_auxz00_10917 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37408L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3798z00_6690), 
(int)(BgL_wz00_1777)); 
FAILURE(BgL_auxz00_10917,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 928 */
 long BgL_arg2027z00_2095; unsigned char BgL_arg2029z00_2096;
BgL_arg2027z00_2095 = 
(BgL_wz00_1777+1L); 
{ /* Llib/unicode.scm 928 */
 long BgL_i3801z00_6693;
BgL_i3801z00_6693 = 
(BgL_rz00_1776+1L); 
{ /* Llib/unicode.scm 928 */
 long BgL_l3802z00_6694;
BgL_l3802z00_6694 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3801z00_6693, BgL_l3802z00_6694))
{ /* Llib/unicode.scm 928 */
BgL_arg2029z00_2096 = 
STRING_REF(BgL_strz00_6264, BgL_i3801z00_6693); }  else 
{ 
 obj_t BgL_auxz00_10929;
BgL_auxz00_10929 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37467L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3802z00_6694), 
(int)(BgL_i3801z00_6693)); 
FAILURE(BgL_auxz00_10929,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 928 */
 long BgL_l3806z00_6698;
BgL_l3806z00_6698 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg2027z00_2095, BgL_l3806z00_6698))
{ /* Llib/unicode.scm 928 */
STRING_SET(BgL_resz00_6265, BgL_arg2027z00_2095, BgL_arg2029z00_2096); }  else 
{ 
 obj_t BgL_auxz00_10939;
BgL_auxz00_10939 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37440L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3806z00_6698), 
(int)(BgL_arg2027z00_2095)); 
FAILURE(BgL_auxz00_10939,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 929 */
 long BgL_arg2031z00_2098; unsigned char BgL_arg2033z00_2099;
BgL_arg2031z00_2098 = 
(BgL_wz00_1777+2L); 
{ /* Llib/unicode.scm 929 */
 long BgL_i3809z00_6701;
BgL_i3809z00_6701 = 
(BgL_rz00_1776+2L); 
{ /* Llib/unicode.scm 929 */
 long BgL_l3810z00_6702;
BgL_l3810z00_6702 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3809z00_6701, BgL_l3810z00_6702))
{ /* Llib/unicode.scm 929 */
BgL_arg2033z00_2099 = 
STRING_REF(BgL_strz00_6264, BgL_i3809z00_6701); }  else 
{ 
 obj_t BgL_auxz00_10951;
BgL_auxz00_10951 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37532L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3810z00_6702), 
(int)(BgL_i3809z00_6701)); 
FAILURE(BgL_auxz00_10951,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 929 */
 long BgL_l3814z00_6706;
BgL_l3814z00_6706 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg2031z00_2098, BgL_l3814z00_6706))
{ /* Llib/unicode.scm 929 */
STRING_SET(BgL_resz00_6265, BgL_arg2031z00_2098, BgL_arg2033z00_2099); }  else 
{ 
 obj_t BgL_auxz00_10961;
BgL_auxz00_10961 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37505L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3814z00_6706), 
(int)(BgL_arg2031z00_2098)); 
FAILURE(BgL_auxz00_10961,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 930 */
 long BgL_arg2036z00_2101; unsigned char BgL_arg2037z00_2102;
BgL_arg2036z00_2101 = 
(BgL_wz00_1777+3L); 
{ /* Llib/unicode.scm 930 */
 long BgL_i3817z00_6709;
BgL_i3817z00_6709 = 
(BgL_rz00_1776+3L); 
{ /* Llib/unicode.scm 930 */
 long BgL_l3818z00_6710;
BgL_l3818z00_6710 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3817z00_6709, BgL_l3818z00_6710))
{ /* Llib/unicode.scm 930 */
BgL_arg2037z00_2102 = 
STRING_REF(BgL_strz00_6264, BgL_i3817z00_6709); }  else 
{ 
 obj_t BgL_auxz00_10973;
BgL_auxz00_10973 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37597L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3818z00_6710), 
(int)(BgL_i3817z00_6709)); 
FAILURE(BgL_auxz00_10973,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 930 */
 long BgL_l3822z00_6714;
BgL_l3822z00_6714 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg2036z00_2101, BgL_l3822z00_6714))
{ /* Llib/unicode.scm 930 */
STRING_SET(BgL_resz00_6265, BgL_arg2036z00_2101, BgL_arg2037z00_2102); }  else 
{ 
 obj_t BgL_auxz00_10983;
BgL_auxz00_10983 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37570L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3822z00_6714), 
(int)(BgL_arg2036z00_2101)); 
FAILURE(BgL_auxz00_10983,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 931 */
 long BgL_arg2039z00_2104; unsigned char BgL_arg2040z00_2105;
BgL_arg2039z00_2104 = 
(BgL_wz00_1777+4L); 
{ /* Llib/unicode.scm 931 */
 long BgL_i3825z00_6717;
BgL_i3825z00_6717 = 
(BgL_rz00_1776+4L); 
{ /* Llib/unicode.scm 931 */
 long BgL_l3826z00_6718;
BgL_l3826z00_6718 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3825z00_6717, BgL_l3826z00_6718))
{ /* Llib/unicode.scm 931 */
BgL_arg2040z00_2105 = 
STRING_REF(BgL_strz00_6264, BgL_i3825z00_6717); }  else 
{ 
 obj_t BgL_auxz00_10995;
BgL_auxz00_10995 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37662L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3826z00_6718), 
(int)(BgL_i3825z00_6717)); 
FAILURE(BgL_auxz00_10995,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 931 */
 long BgL_l3830z00_6722;
BgL_l3830z00_6722 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg2039z00_2104, BgL_l3830z00_6722))
{ /* Llib/unicode.scm 931 */
STRING_SET(BgL_resz00_6265, BgL_arg2039z00_2104, BgL_arg2040z00_2105); }  else 
{ 
 obj_t BgL_auxz00_11005;
BgL_auxz00_11005 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37635L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3830z00_6722), 
(int)(BgL_arg2039z00_2104)); 
FAILURE(BgL_auxz00_11005,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 932 */
 long BgL_arg2042z00_2107; unsigned char BgL_arg2044z00_2108;
BgL_arg2042z00_2107 = 
(BgL_wz00_1777+5L); 
{ /* Llib/unicode.scm 932 */
 long BgL_i3833z00_6725;
BgL_i3833z00_6725 = 
(BgL_rz00_1776+5L); 
{ /* Llib/unicode.scm 932 */
 long BgL_l3834z00_6726;
BgL_l3834z00_6726 = 
STRING_LENGTH(BgL_strz00_6264); 
if(
BOUND_CHECK(BgL_i3833z00_6725, BgL_l3834z00_6726))
{ /* Llib/unicode.scm 932 */
BgL_arg2044z00_2108 = 
STRING_REF(BgL_strz00_6264, BgL_i3833z00_6725); }  else 
{ 
 obj_t BgL_auxz00_11017;
BgL_auxz00_11017 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37727L), BGl_string4897z00zz__unicodez00, BgL_strz00_6264, 
(int)(BgL_l3834z00_6726), 
(int)(BgL_i3833z00_6725)); 
FAILURE(BgL_auxz00_11017,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 932 */
 long BgL_l3838z00_6730;
BgL_l3838z00_6730 = 
STRING_LENGTH(BgL_resz00_6265); 
if(
BOUND_CHECK(BgL_arg2042z00_2107, BgL_l3838z00_6730))
{ /* Llib/unicode.scm 932 */
STRING_SET(BgL_resz00_6265, BgL_arg2042z00_2107, BgL_arg2044z00_2108); }  else 
{ 
 obj_t BgL_auxz00_11027;
BgL_auxz00_11027 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(37700L), BGl_string4915z00zz__unicodez00, BgL_resz00_6265, 
(int)(BgL_l3838z00_6730), 
(int)(BgL_arg2042z00_2107)); 
FAILURE(BgL_auxz00_11027,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_11035; long BgL_rz00_11033;
BgL_rz00_11033 = 
(BgL_rz00_1776+6L); 
BgL_wz00_11035 = 
(BgL_wz00_1777+6L); 
BgL_wz00_1777 = BgL_wz00_11035; 
BgL_rz00_1776 = BgL_rz00_11033; 
goto BGl_loopze70ze7zz__unicodez00;} }  else 
{ /* Llib/unicode.scm 920 */
BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(BgL_resz00_6265, BgL_wz00_1777); 
{ 
 long BgL_wz00_11040; long BgL_rz00_11038;
BgL_rz00_11038 = 
(BgL_rz00_1776+1L); 
BgL_wz00_11040 = 
(BgL_wz00_1777+3L); 
BgL_wz00_1777 = BgL_wz00_11040; 
BgL_rz00_1776 = BgL_rz00_11038; 
goto BGl_loopze70ze7zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 919 */
return BFALSE;} } } } } } } } } } } } } } } 

}



/* string-unicode-fix!~1 */
obj_t BGl_stringzd2unicodezd2fixz12ze71zf5zz__unicodez00(obj_t BgL_sz00_2147, long BgL_jz00_2148)
{
{ /* Llib/unicode.scm 782 */
{ /* Llib/unicode.scm 782 */
 long BgL_l3842z00_6734;
BgL_l3842z00_6734 = 
STRING_LENGTH(BgL_sz00_2147); 
if(
BOUND_CHECK(BgL_jz00_2148, BgL_l3842z00_6734))
{ /* Llib/unicode.scm 782 */
STRING_SET(BgL_sz00_2147, BgL_jz00_2148, ((unsigned char)239)); }  else 
{ 
 obj_t BgL_auxz00_11046;
BgL_auxz00_11046 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31874L), BGl_string4915z00zz__unicodez00, BgL_sz00_2147, 
(int)(BgL_l3842z00_6734), 
(int)(BgL_jz00_2148)); 
FAILURE(BgL_auxz00_11046,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 783 */
 long BgL_arg2067z00_2150;
BgL_arg2067z00_2150 = 
(BgL_jz00_2148+1L); 
{ /* Llib/unicode.scm 783 */
 long BgL_l3846z00_6738;
BgL_l3846z00_6738 = 
STRING_LENGTH(BgL_sz00_2147); 
if(
BOUND_CHECK(BgL_arg2067z00_2150, BgL_l3846z00_6738))
{ /* Llib/unicode.scm 783 */
STRING_SET(BgL_sz00_2147, BgL_arg2067z00_2150, ((unsigned char)191)); }  else 
{ 
 obj_t BgL_auxz00_11057;
BgL_auxz00_11057 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31904L), BGl_string4915z00zz__unicodez00, BgL_sz00_2147, 
(int)(BgL_l3846z00_6738), 
(int)(BgL_arg2067z00_2150)); 
FAILURE(BgL_auxz00_11057,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 784 */
 long BgL_arg2068z00_2151;
BgL_arg2068z00_2151 = 
(BgL_jz00_2148+2L); 
{ /* Llib/unicode.scm 784 */
 long BgL_l3850z00_6742;
BgL_l3850z00_6742 = 
STRING_LENGTH(BgL_sz00_2147); 
if(
BOUND_CHECK(BgL_arg2068z00_2151, BgL_l3850z00_6742))
{ /* Llib/unicode.scm 784 */
return 
STRING_SET(BgL_sz00_2147, BgL_arg2068z00_2151, ((unsigned char)189));}  else 
{ 
 obj_t BgL_auxz00_11068;
BgL_auxz00_11068 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(31942L), BGl_string4915z00zz__unicodez00, BgL_sz00_2147, 
(int)(BgL_l3850z00_6742), 
(int)(BgL_arg2068z00_2151)); 
FAILURE(BgL_auxz00_11068,BFALSE,BFALSE);} } } } 

}



/* _utf8-normalize-utf16 */
obj_t BGl__utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t BgL_env1138z00_83, obj_t BgL_opt1137z00_82)
{
{ /* Llib/unicode.scm 947 */
{ /* Llib/unicode.scm 947 */
 obj_t BgL_strz00_2154;
BgL_strz00_2154 = 
VECTOR_REF(BgL_opt1137z00_82,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1137z00_82)) { case 1L : 

{ /* Llib/unicode.scm 947 */
 long BgL_endz00_2159;
{ /* Llib/unicode.scm 947 */
 obj_t BgL_stringz00_4702;
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_stringz00_4702 = BgL_strz00_2154; }  else 
{ 
 obj_t BgL_auxz00_11077;
BgL_auxz00_11077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38527L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11077,BFALSE,BFALSE);} 
BgL_endz00_2159 = 
STRING_LENGTH(BgL_stringz00_4702); } 
{ /* Llib/unicode.scm 947 */

{ /* Llib/unicode.scm 947 */
 obj_t BgL_auxz00_11082;
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_auxz00_11082 = BgL_strz00_2154
; }  else 
{ 
 obj_t BgL_auxz00_11085;
BgL_auxz00_11085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11085,BFALSE,BFALSE);} 
return 
BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(BgL_auxz00_11082, ((bool_t)0), 0L, BgL_endz00_2159);} } } break;case 2L : 

{ /* Llib/unicode.scm 947 */
 obj_t BgL_strictz00_2160;
BgL_strictz00_2160 = 
VECTOR_REF(BgL_opt1137z00_82,1L); 
{ /* Llib/unicode.scm 947 */
 long BgL_endz00_2162;
{ /* Llib/unicode.scm 947 */
 obj_t BgL_stringz00_4703;
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_stringz00_4703 = BgL_strz00_2154; }  else 
{ 
 obj_t BgL_auxz00_11093;
BgL_auxz00_11093 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38527L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11093,BFALSE,BFALSE);} 
BgL_endz00_2162 = 
STRING_LENGTH(BgL_stringz00_4703); } 
{ /* Llib/unicode.scm 947 */

{ /* Llib/unicode.scm 947 */
 obj_t BgL_auxz00_11098;
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_auxz00_11098 = BgL_strz00_2154
; }  else 
{ 
 obj_t BgL_auxz00_11101;
BgL_auxz00_11101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11101,BFALSE,BFALSE);} 
return 
BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(BgL_auxz00_11098, 
CBOOL(BgL_strictz00_2160), 0L, BgL_endz00_2162);} } } } break;case 3L : 

{ /* Llib/unicode.scm 947 */
 obj_t BgL_strictz00_2163;
BgL_strictz00_2163 = 
VECTOR_REF(BgL_opt1137z00_82,1L); 
{ /* Llib/unicode.scm 947 */
 obj_t BgL_startz00_2164;
BgL_startz00_2164 = 
VECTOR_REF(BgL_opt1137z00_82,2L); 
{ /* Llib/unicode.scm 947 */
 long BgL_endz00_2165;
{ /* Llib/unicode.scm 947 */
 obj_t BgL_stringz00_4704;
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_stringz00_4704 = BgL_strz00_2154; }  else 
{ 
 obj_t BgL_auxz00_11111;
BgL_auxz00_11111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38527L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11111,BFALSE,BFALSE);} 
BgL_endz00_2165 = 
STRING_LENGTH(BgL_stringz00_4704); } 
{ /* Llib/unicode.scm 947 */

{ /* Llib/unicode.scm 947 */
 long BgL_auxz00_11123; obj_t BgL_auxz00_11116;
{ /* Llib/unicode.scm 947 */
 obj_t BgL_tmpz00_11125;
if(
INTEGERP(BgL_startz00_2164))
{ /* Llib/unicode.scm 947 */
BgL_tmpz00_11125 = BgL_startz00_2164
; }  else 
{ 
 obj_t BgL_auxz00_11128;
BgL_auxz00_11128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_2164); 
FAILURE(BgL_auxz00_11128,BFALSE,BFALSE);} 
BgL_auxz00_11123 = 
(long)CINT(BgL_tmpz00_11125); } 
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_auxz00_11116 = BgL_strz00_2154
; }  else 
{ 
 obj_t BgL_auxz00_11119;
BgL_auxz00_11119 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11119,BFALSE,BFALSE);} 
return 
BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(BgL_auxz00_11116, 
CBOOL(BgL_strictz00_2163), BgL_auxz00_11123, BgL_endz00_2165);} } } } } break;case 4L : 

{ /* Llib/unicode.scm 947 */
 obj_t BgL_strictz00_2166;
BgL_strictz00_2166 = 
VECTOR_REF(BgL_opt1137z00_82,1L); 
{ /* Llib/unicode.scm 947 */
 obj_t BgL_startz00_2167;
BgL_startz00_2167 = 
VECTOR_REF(BgL_opt1137z00_82,2L); 
{ /* Llib/unicode.scm 947 */
 obj_t BgL_endz00_2168;
BgL_endz00_2168 = 
VECTOR_REF(BgL_opt1137z00_82,3L); 
{ /* Llib/unicode.scm 947 */

{ /* Llib/unicode.scm 947 */
 long BgL_auxz00_11154; long BgL_auxz00_11144; obj_t BgL_auxz00_11137;
{ /* Llib/unicode.scm 947 */
 obj_t BgL_tmpz00_11155;
if(
INTEGERP(BgL_endz00_2168))
{ /* Llib/unicode.scm 947 */
BgL_tmpz00_11155 = BgL_endz00_2168
; }  else 
{ 
 obj_t BgL_auxz00_11158;
BgL_auxz00_11158 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_2168); 
FAILURE(BgL_auxz00_11158,BFALSE,BFALSE);} 
BgL_auxz00_11154 = 
(long)CINT(BgL_tmpz00_11155); } 
{ /* Llib/unicode.scm 947 */
 obj_t BgL_tmpz00_11146;
if(
INTEGERP(BgL_startz00_2167))
{ /* Llib/unicode.scm 947 */
BgL_tmpz00_11146 = BgL_startz00_2167
; }  else 
{ 
 obj_t BgL_auxz00_11149;
BgL_auxz00_11149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_startz00_2167); 
FAILURE(BgL_auxz00_11149,BFALSE,BFALSE);} 
BgL_auxz00_11144 = 
(long)CINT(BgL_tmpz00_11146); } 
if(
STRINGP(BgL_strz00_2154))
{ /* Llib/unicode.scm 947 */
BgL_auxz00_11137 = BgL_strz00_2154
; }  else 
{ 
 obj_t BgL_auxz00_11140;
BgL_auxz00_11140 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38433L), BGl_string4918z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2154); 
FAILURE(BgL_auxz00_11140,BFALSE,BFALSE);} 
return 
BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(BgL_auxz00_11137, 
CBOOL(BgL_strictz00_2166), BgL_auxz00_11144, BgL_auxz00_11154);} } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4916z00zz__unicodez00, BGl_string4912z00zz__unicodez00, 
BINT(
VECTOR_LENGTH(BgL_opt1137z00_82)));} } } } 

}



/* utf8-normalize-utf16 */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2normaliza7ezd2utf16za7zz__unicodez00(obj_t BgL_strz00_78, bool_t BgL_strictz00_79, long BgL_startz00_80, long BgL_endz00_81)
{
{ /* Llib/unicode.scm 947 */
{ /* Llib/unicode.scm 960 */
 bool_t BgL_test5638z00_11169;
if(
(BgL_endz00_81>
STRING_LENGTH(BgL_strz00_78)))
{ /* Llib/unicode.scm 960 */
BgL_test5638z00_11169 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 960 */
if(
(BgL_startz00_80<0L))
{ /* Llib/unicode.scm 961 */
BgL_test5638z00_11169 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 961 */
BgL_test5638z00_11169 = 
(BgL_startz00_80>BgL_endz00_81)
; } } 
if(BgL_test5638z00_11169)
{ /* Llib/unicode.scm 963 */
 obj_t BgL_arg2076z00_2177;
BgL_arg2076z00_2177 = 
MAKE_YOUNG_PAIR(
BINT(BgL_startz00_80), 
BINT(BgL_endz00_81)); 
{ /* Llib/unicode.scm 963 */
 obj_t BgL_aux4618z00_7510;
BgL_aux4618z00_7510 = 
BGl_errorz00zz__errorz00(BGl_string4917z00zz__unicodez00, BGl_string4914z00zz__unicodez00, BgL_arg2076z00_2177); 
if(
STRINGP(BgL_aux4618z00_7510))
{ /* Llib/unicode.scm 963 */
return BgL_aux4618z00_7510;}  else 
{ 
 obj_t BgL_auxz00_11182;
BgL_auxz00_11182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38957L), BGl_string4917z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4618z00_7510); 
FAILURE(BgL_auxz00_11182,BFALSE,BFALSE);} } }  else 
{ /* Llib/unicode.scm 965 */
 long BgL_lenz00_2178;
BgL_lenz00_2178 = 
(BgL_endz00_81-BgL_startz00_80); 
{ /* Llib/unicode.scm 965 */
 obj_t BgL_resz00_2179;
{ /* Llib/unicode.scm 966 */
 long BgL_arg2536z00_2646;
BgL_arg2536z00_2646 = 
(3L*BgL_lenz00_2178); 
{ /* Ieee/string.scm 172 */

BgL_resz00_2179 = 
make_string(BgL_arg2536z00_2646, ((unsigned char)' ')); } } 
{ /* Llib/unicode.scm 966 */

{ 
 long BgL_rz00_2181; long BgL_wz00_2182; bool_t BgL_asciiz00_2183;
BgL_rz00_2181 = BgL_startz00_80; 
BgL_wz00_2182 = 0L; 
BgL_asciiz00_2183 = ((bool_t)1); 
BgL_zc3z04anonymousza32077ze3z87_2184:
if(
(BgL_rz00_2181==BgL_endz00_81))
{ /* Llib/unicode.scm 971 */
 obj_t BgL_val0_1102z00_2186; obj_t BgL_val1_1103z00_2187;
BgL_val0_1102z00_2186 = 
bgl_string_shrink(BgL_resz00_2179, BgL_wz00_2182); 
if(BgL_asciiz00_2183)
{ /* Llib/unicode.scm 971 */
BgL_val1_1103z00_2187 = BGl_symbol4904z00zz__unicodez00; }  else 
{ /* Llib/unicode.scm 971 */
BgL_val1_1103z00_2187 = BGl_symbol4919z00zz__unicodez00; } 
{ /* Llib/unicode.scm 971 */
 int BgL_tmpz00_11193;
BgL_tmpz00_11193 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11193); } 
{ /* Llib/unicode.scm 971 */
 int BgL_tmpz00_11196;
BgL_tmpz00_11196 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_11196, BgL_val1_1103z00_2187); } 
return BgL_val0_1102z00_2186;}  else 
{ /* Llib/unicode.scm 972 */
 unsigned char BgL_cz00_2188;
{ /* Llib/unicode.scm 972 */
 long BgL_l3854z00_6746;
BgL_l3854z00_6746 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_rz00_2181, BgL_l3854z00_6746))
{ /* Llib/unicode.scm 972 */
BgL_cz00_2188 = 
STRING_REF(BgL_strz00_78, BgL_rz00_2181); }  else 
{ 
 obj_t BgL_auxz00_11203;
BgL_auxz00_11203 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39262L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3854z00_6746), 
(int)(BgL_rz00_2181)); 
FAILURE(BgL_auxz00_11203,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 972 */
 long BgL_nz00_2189;
BgL_nz00_2189 = 
(BgL_cz00_2188); 
{ /* Llib/unicode.scm 973 */

if(
(BgL_nz00_2189<=127L))
{ /* Llib/unicode.scm 975 */
{ /* Llib/unicode.scm 977 */
 long BgL_l3858z00_6750;
BgL_l3858z00_6750 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3858z00_6750))
{ /* Llib/unicode.scm 977 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_11216;
BgL_auxz00_11216 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39360L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3858z00_6750), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11216,BFALSE,BFALSE);} } 
{ 
 long BgL_wz00_11224; long BgL_rz00_11222;
BgL_rz00_11222 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11224 = 
(BgL_wz00_2182+1L); 
BgL_wz00_2182 = BgL_wz00_11224; 
BgL_rz00_2181 = BgL_rz00_11222; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 975 */
if(
(BgL_nz00_2189<194L))
{ /* Llib/unicode.scm 979 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11233; long BgL_wz00_11231; long BgL_rz00_11229;
BgL_rz00_11229 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11231 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11233 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11233; 
BgL_wz00_2182 = BgL_wz00_11231; 
BgL_rz00_2181 = BgL_rz00_11229; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 979 */
if(
(BgL_nz00_2189<216L))
{ /* Llib/unicode.scm 985 */
 bool_t BgL_test5649z00_11236;
if(
(
(1L+BgL_rz00_2181)<BgL_endz00_81))
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_4756;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11240;
{ /* Llib/unicode.scm 986 */
 long BgL_i3861z00_6753;
BgL_i3861z00_6753 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 986 */
 long BgL_l3862z00_6754;
BgL_l3862z00_6754 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3861z00_6753, BgL_l3862z00_6754))
{ /* Llib/unicode.scm 986 */
BgL_tmpz00_11240 = 
STRING_REF(BgL_strz00_78, BgL_i3861z00_6753)
; }  else 
{ 
 obj_t BgL_auxz00_11246;
BgL_auxz00_11246 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39625L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3862z00_6754), 
(int)(BgL_i3861z00_6753)); 
FAILURE(BgL_auxz00_11246,BFALSE,BFALSE);} } } 
BgL_nz00_4756 = 
(BgL_tmpz00_11240); } 
if(
(BgL_nz00_4756>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5649z00_11236 = 
(BgL_nz00_4756<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5649z00_11236 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 985 */
BgL_test5649z00_11236 = ((bool_t)0)
; } 
if(BgL_test5649z00_11236)
{ /* Llib/unicode.scm 985 */
{ /* Llib/unicode.scm 988 */
 long BgL_l3866z00_6758;
BgL_l3866z00_6758 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3866z00_6758))
{ /* Llib/unicode.scm 988 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_11260;
BgL_auxz00_11260 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39688L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3866z00_6758), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11260,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 989 */
 long BgL_arg2093z00_2203; unsigned char BgL_arg2094z00_2204;
BgL_arg2093z00_2203 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 989 */
 long BgL_i3869z00_6761;
BgL_i3869z00_6761 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 989 */
 long BgL_l3870z00_6762;
BgL_l3870z00_6762 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3869z00_6761, BgL_l3870z00_6762))
{ /* Llib/unicode.scm 989 */
BgL_arg2094z00_2204 = 
STRING_REF(BgL_strz00_78, BgL_i3869z00_6761); }  else 
{ 
 obj_t BgL_auxz00_11272;
BgL_auxz00_11272 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39747L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3870z00_6762), 
(int)(BgL_i3869z00_6761)); 
FAILURE(BgL_auxz00_11272,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 989 */
 long BgL_l3874z00_6766;
BgL_l3874z00_6766 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2093z00_2203, BgL_l3874z00_6766))
{ /* Llib/unicode.scm 989 */
STRING_SET(BgL_resz00_2179, BgL_arg2093z00_2203, BgL_arg2094z00_2204); }  else 
{ 
 obj_t BgL_auxz00_11282;
BgL_auxz00_11282 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39720L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3874z00_6766), 
(int)(BgL_arg2093z00_2203)); 
FAILURE(BgL_auxz00_11282,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_11292; long BgL_wz00_11290; long BgL_rz00_11288;
BgL_rz00_11288 = 
(BgL_rz00_2181+2L); 
BgL_wz00_11290 = 
(BgL_wz00_2182+2L); 
BgL_asciiz00_11292 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11292; 
BgL_wz00_2182 = BgL_wz00_11290; 
BgL_rz00_2181 = BgL_rz00_11288; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 985 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11298; long BgL_wz00_11296; long BgL_rz00_11294;
BgL_rz00_11294 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11296 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11298 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11298; 
BgL_wz00_2182 = BgL_wz00_11296; 
BgL_rz00_2181 = BgL_rz00_11294; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 983 */
if(
(BgL_nz00_2189<=223L))
{ /* Llib/unicode.scm 995 */
 bool_t BgL_test5657z00_11301;
if(
(
(1L+BgL_rz00_2181)<BgL_endz00_81))
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_4782;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11305;
{ /* Llib/unicode.scm 996 */
 long BgL_i3877z00_6769;
BgL_i3877z00_6769 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 996 */
 long BgL_l3878z00_6770;
BgL_l3878z00_6770 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3877z00_6769, BgL_l3878z00_6770))
{ /* Llib/unicode.scm 996 */
BgL_tmpz00_11305 = 
STRING_REF(BgL_strz00_78, BgL_i3877z00_6769)
; }  else 
{ 
 obj_t BgL_auxz00_11311;
BgL_auxz00_11311 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(39983L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3878z00_6770), 
(int)(BgL_i3877z00_6769)); 
FAILURE(BgL_auxz00_11311,BFALSE,BFALSE);} } } 
BgL_nz00_4782 = 
(BgL_tmpz00_11305); } 
if(
(BgL_nz00_4782>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5657z00_11301 = 
(BgL_nz00_4782<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5657z00_11301 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 995 */
BgL_test5657z00_11301 = ((bool_t)0)
; } 
if(BgL_test5657z00_11301)
{ /* Llib/unicode.scm 995 */
{ /* Llib/unicode.scm 998 */
 long BgL_l3882z00_6774;
BgL_l3882z00_6774 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3882z00_6774))
{ /* Llib/unicode.scm 998 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_11325;
BgL_auxz00_11325 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40046L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3882z00_6774), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11325,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 999 */
 long BgL_arg2110z00_2221; unsigned char BgL_arg2111z00_2222;
BgL_arg2110z00_2221 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 999 */
 long BgL_i3885z00_6777;
BgL_i3885z00_6777 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 999 */
 long BgL_l3886z00_6778;
BgL_l3886z00_6778 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3885z00_6777, BgL_l3886z00_6778))
{ /* Llib/unicode.scm 999 */
BgL_arg2111z00_2222 = 
STRING_REF(BgL_strz00_78, BgL_i3885z00_6777); }  else 
{ 
 obj_t BgL_auxz00_11337;
BgL_auxz00_11337 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40105L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3886z00_6778), 
(int)(BgL_i3885z00_6777)); 
FAILURE(BgL_auxz00_11337,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 999 */
 long BgL_l3890z00_6782;
BgL_l3890z00_6782 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2110z00_2221, BgL_l3890z00_6782))
{ /* Llib/unicode.scm 999 */
STRING_SET(BgL_resz00_2179, BgL_arg2110z00_2221, BgL_arg2111z00_2222); }  else 
{ 
 obj_t BgL_auxz00_11347;
BgL_auxz00_11347 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40078L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3890z00_6782), 
(int)(BgL_arg2110z00_2221)); 
FAILURE(BgL_auxz00_11347,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_11357; long BgL_wz00_11355; long BgL_rz00_11353;
BgL_rz00_11353 = 
(BgL_rz00_2181+2L); 
BgL_wz00_11355 = 
(BgL_wz00_2182+2L); 
BgL_asciiz00_11357 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11357; 
BgL_wz00_2182 = BgL_wz00_11355; 
BgL_rz00_2181 = BgL_rz00_11353; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 995 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11363; long BgL_wz00_11361; long BgL_rz00_11359;
BgL_rz00_11359 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11361 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11363 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11363; 
BgL_wz00_2182 = BgL_wz00_11361; 
BgL_rz00_2181 = BgL_rz00_11359; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 994 */
if(
(BgL_nz00_2189==237L))
{ /* Llib/unicode.scm 1007 */
 bool_t BgL_test5665z00_11366;
if(
(BgL_rz00_2181<
(BgL_endz00_81-2L)))
{ /* Llib/unicode.scm 1008 */
 bool_t BgL_test5667z00_11370;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_4808;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11371;
{ /* Llib/unicode.scm 1008 */
 long BgL_i3893z00_6785;
BgL_i3893z00_6785 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1008 */
 long BgL_l3894z00_6786;
BgL_l3894z00_6786 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3893z00_6785, BgL_l3894z00_6786))
{ /* Llib/unicode.scm 1008 */
BgL_tmpz00_11371 = 
STRING_REF(BgL_strz00_78, BgL_i3893z00_6785)
; }  else 
{ 
 obj_t BgL_auxz00_11377;
BgL_auxz00_11377 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40423L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3894z00_6786), 
(int)(BgL_i3893z00_6785)); 
FAILURE(BgL_auxz00_11377,BFALSE,BFALSE);} } } 
BgL_nz00_4808 = 
(BgL_tmpz00_11371); } 
if(
(BgL_nz00_4808>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5667z00_11370 = 
(BgL_nz00_4808<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5667z00_11370 = ((bool_t)0)
; } } 
if(BgL_test5667z00_11370)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_4816;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11387;
{ /* Llib/unicode.scm 1009 */
 long BgL_i3897z00_6789;
BgL_i3897z00_6789 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1009 */
 long BgL_l3898z00_6790;
BgL_l3898z00_6790 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3897z00_6789, BgL_l3898z00_6790))
{ /* Llib/unicode.scm 1009 */
BgL_tmpz00_11387 = 
STRING_REF(BgL_strz00_78, BgL_i3897z00_6789)
; }  else 
{ 
 obj_t BgL_auxz00_11393;
BgL_auxz00_11393 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40477L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3898z00_6790), 
(int)(BgL_i3897z00_6789)); 
FAILURE(BgL_auxz00_11393,BFALSE,BFALSE);} } } 
BgL_nz00_4816 = 
(BgL_tmpz00_11387); } 
if(
(BgL_nz00_4816>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5665z00_11366 = 
(BgL_nz00_4816<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5665z00_11366 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1008 */
BgL_test5665z00_11366 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1007 */
BgL_test5665z00_11366 = ((bool_t)0)
; } 
if(BgL_test5665z00_11366)
{ /* Llib/unicode.scm 1015 */
 long BgL_n1z00_2244;
{ /* Llib/unicode.scm 1015 */
 unsigned char BgL_tmpz00_11403;
{ /* Llib/unicode.scm 1015 */
 long BgL_i3901z00_6793;
BgL_i3901z00_6793 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1015 */
 long BgL_l3902z00_6794;
BgL_l3902z00_6794 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3901z00_6793, BgL_l3902z00_6794))
{ /* Llib/unicode.scm 1015 */
BgL_tmpz00_11403 = 
STRING_REF(BgL_strz00_78, BgL_i3901z00_6793)
; }  else 
{ 
 obj_t BgL_auxz00_11409;
BgL_auxz00_11409 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40830L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3902z00_6794), 
(int)(BgL_i3901z00_6793)); 
FAILURE(BgL_auxz00_11409,BFALSE,BFALSE);} } } 
BgL_n1z00_2244 = 
(BgL_tmpz00_11403); } 
{ /* Llib/unicode.scm 1015 */
 long BgL_n2z00_2245;
{ /* Llib/unicode.scm 1016 */
 unsigned char BgL_tmpz00_11416;
{ /* Llib/unicode.scm 1016 */
 long BgL_i3905z00_6797;
BgL_i3905z00_6797 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1016 */
 long BgL_l3906z00_6798;
BgL_l3906z00_6798 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3905z00_6797, BgL_l3906z00_6798))
{ /* Llib/unicode.scm 1016 */
BgL_tmpz00_11416 = 
STRING_REF(BgL_strz00_78, BgL_i3905z00_6797)
; }  else 
{ 
 obj_t BgL_auxz00_11422;
BgL_auxz00_11422 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(40885L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3906z00_6798), 
(int)(BgL_i3905z00_6797)); 
FAILURE(BgL_auxz00_11422,BFALSE,BFALSE);} } } 
BgL_n2z00_2245 = 
(BgL_tmpz00_11416); } 
{ /* Llib/unicode.scm 1016 */
 long BgL_ucs2z00_2246;
BgL_ucs2z00_2246 = 
(
(
(BgL_nz00_2189 & 15L) << 
(int)(12L))+
(
(
(BgL_n1z00_2244 & 63L) << 
(int)(6L))+
(BgL_n2z00_2245 & 63L))); 
{ /* Llib/unicode.scm 1017 */

if(
(BgL_ucs2z00_2246>57343L))
{ /* Llib/unicode.scm 1021 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11445; long BgL_wz00_11443; long BgL_rz00_11441;
BgL_rz00_11441 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11443 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11445 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11445; 
BgL_wz00_2182 = BgL_wz00_11443; 
BgL_rz00_2181 = BgL_rz00_11441; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1024 */
 bool_t BgL_test5675z00_11446;
if(
(BgL_rz00_2181<=
(BgL_endz00_81-4L)))
{ /* Llib/unicode.scm 1025 */
 long BgL_tmpz00_11450;
{ /* Llib/unicode.scm 1025 */
 unsigned char BgL_tmpz00_11451;
{ /* Llib/unicode.scm 1025 */
 long BgL_i3909z00_6801;
BgL_i3909z00_6801 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1025 */
 long BgL_l3910z00_6802;
BgL_l3910z00_6802 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3909z00_6801, BgL_l3910z00_6802))
{ /* Llib/unicode.scm 1025 */
BgL_tmpz00_11451 = 
STRING_REF(BgL_strz00_78, BgL_i3909z00_6801)
; }  else 
{ 
 obj_t BgL_auxz00_11457;
BgL_auxz00_11457 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(41211L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3910z00_6802), 
(int)(BgL_i3909z00_6801)); 
FAILURE(BgL_auxz00_11457,BFALSE,BFALSE);} } } 
BgL_tmpz00_11450 = 
(BgL_tmpz00_11451); } 
BgL_test5675z00_11446 = 
(BgL_tmpz00_11450==237L); }  else 
{ /* Llib/unicode.scm 1024 */
BgL_test5675z00_11446 = ((bool_t)0)
; } 
if(BgL_test5675z00_11446)
{ /* Llib/unicode.scm 1027 */
 long BgL_m1z00_2258;
{ /* Llib/unicode.scm 1028 */
 unsigned char BgL_tmpz00_11465;
{ /* Llib/unicode.scm 1028 */
 long BgL_i3913z00_6805;
BgL_i3913z00_6805 = 
(BgL_rz00_2181+4L); 
{ /* Llib/unicode.scm 1028 */
 long BgL_l3914z00_6806;
BgL_l3914z00_6806 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3913z00_6805, BgL_l3914z00_6806))
{ /* Llib/unicode.scm 1028 */
BgL_tmpz00_11465 = 
STRING_REF(BgL_strz00_78, BgL_i3913z00_6805)
; }  else 
{ 
 obj_t BgL_auxz00_11471;
BgL_auxz00_11471 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(41325L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3914z00_6806), 
(int)(BgL_i3913z00_6805)); 
FAILURE(BgL_auxz00_11471,BFALSE,BFALSE);} } } 
BgL_m1z00_2258 = 
(BgL_tmpz00_11465); } 
{ /* Llib/unicode.scm 1028 */
 long BgL_m2z00_2259;
{ /* Llib/unicode.scm 1029 */
 unsigned char BgL_tmpz00_11478;
{ /* Llib/unicode.scm 1029 */
 long BgL_i3917z00_6809;
BgL_i3917z00_6809 = 
(BgL_rz00_2181+5L); 
{ /* Llib/unicode.scm 1029 */
 long BgL_l3918z00_6810;
BgL_l3918z00_6810 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3917z00_6809, BgL_l3918z00_6810))
{ /* Llib/unicode.scm 1029 */
BgL_tmpz00_11478 = 
STRING_REF(BgL_strz00_78, BgL_i3917z00_6809)
; }  else 
{ 
 obj_t BgL_auxz00_11484;
BgL_auxz00_11484 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(41380L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3918z00_6810), 
(int)(BgL_i3917z00_6809)); 
FAILURE(BgL_auxz00_11484,BFALSE,BFALSE);} } } 
BgL_m2z00_2259 = 
(BgL_tmpz00_11478); } 
{ /* Llib/unicode.scm 1029 */
 long BgL_nuz00_2260;
BgL_nuz00_2260 = 
(
(
(237L & 15L) << 
(int)(12L))+
(
(
(BgL_m1z00_2258 & 63L) << 
(int)(6L))+
(BgL_m2z00_2259 & 63L))); 
{ /* Llib/unicode.scm 1030 */

if(
(BgL_nuz00_2260>=56320L))
{ /* Llib/unicode.scm 1034 */
 long BgL_za7za7za7za7za7za7z00_2262;
BgL_za7za7za7za7za7za7z00_2262 = 
(BgL_nuz00_2260 & 63L); 
{ /* Llib/unicode.scm 1034 */
 long BgL_yyyyz00_2263;
BgL_yyyyz00_2263 = 
(
(BgL_nuz00_2260 & 1023L) >> 
(int)(6L)); 
{ /* Llib/unicode.scm 1035 */
 long BgL_xxz00_2264;
BgL_xxz00_2264 = 
(BgL_ucs2z00_2246 & 3L); 
{ /* Llib/unicode.scm 1036 */
 long BgL_wwwwz00_2265;
BgL_wwwwz00_2265 = 
(
(BgL_ucs2z00_2246 >> 
(int)(2L)) & 15L); 
{ /* Llib/unicode.scm 1037 */
 long BgL_vvvvz00_2266;
BgL_vvvvz00_2266 = 
(
(BgL_ucs2z00_2246 >> 
(int)(6L)) & 15L); 
{ /* Llib/unicode.scm 1038 */
 long BgL_uuuuuz00_2267;
BgL_uuuuuz00_2267 = 
(BgL_vvvvz00_2266+1L); 
{ /* Llib/unicode.scm 1039 */

{ /* Llib/unicode.scm 1040 */
 long BgL_arg2145z00_2268; unsigned char BgL_arg2146z00_2269;
BgL_arg2145z00_2268 = 
(BgL_wz00_2182+3L); 
BgL_arg2146z00_2269 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(128L+BgL_za7za7za7za7za7za7z00_2262)); 
{ /* Llib/unicode.scm 1040 */
 long BgL_l3922z00_6814;
BgL_l3922z00_6814 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2145z00_2268, BgL_l3922z00_6814))
{ /* Llib/unicode.scm 1040 */
STRING_SET(BgL_resz00_2179, BgL_arg2145z00_2268, BgL_arg2146z00_2269); }  else 
{ 
 obj_t BgL_auxz00_11521;
BgL_auxz00_11521 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(41805L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3922z00_6814), 
(int)(BgL_arg2145z00_2268)); 
FAILURE(BgL_auxz00_11521,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1043 */
 long BgL_arg2148z00_2271; unsigned char BgL_arg2149z00_2272;
BgL_arg2148z00_2271 = 
(BgL_wz00_2182+2L); 
BgL_arg2149z00_2272 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(128L+
(
(BgL_xxz00_2264 << 
(int)(4L)) | BgL_yyyyz00_2263))); 
{ /* Llib/unicode.scm 1043 */
 long BgL_l3926z00_6818;
BgL_l3926z00_6818 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2148z00_2271, BgL_l3926z00_6818))
{ /* Llib/unicode.scm 1043 */
STRING_SET(BgL_resz00_2179, BgL_arg2148z00_2271, BgL_arg2149z00_2272); }  else 
{ 
 obj_t BgL_auxz00_11537;
BgL_auxz00_11537 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(41892L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3926z00_6818), 
(int)(BgL_arg2148z00_2271)); 
FAILURE(BgL_auxz00_11537,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1047 */
 long BgL_arg2154z00_2276; unsigned char BgL_arg2155z00_2277;
BgL_arg2154z00_2276 = 
(BgL_wz00_2182+1L); 
BgL_arg2155z00_2277 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(128L+
(BgL_wwwwz00_2265 | 
(
(BgL_uuuuuz00_2267 & 3L) << 
(int)(4L))))); 
{ /* Llib/unicode.scm 1047 */
 long BgL_l3930z00_6822;
BgL_l3930z00_6822 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2154z00_2276, BgL_l3930z00_6822))
{ /* Llib/unicode.scm 1047 */
STRING_SET(BgL_resz00_2179, BgL_arg2154z00_2276, BgL_arg2155z00_2277); }  else 
{ 
 obj_t BgL_auxz00_11554;
BgL_auxz00_11554 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(42013L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3930z00_6822), 
(int)(BgL_arg2154z00_2276)); 
FAILURE(BgL_auxz00_11554,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1054 */
 unsigned char BgL_arg2160z00_2282;
BgL_arg2160z00_2282 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(240L | 
(BgL_uuuuuz00_2267 >> 
(int)(2L)))); 
{ /* Llib/unicode.scm 1052 */
 long BgL_l3934z00_6826;
BgL_l3934z00_6826 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3934z00_6826))
{ /* Llib/unicode.scm 1052 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_arg2160z00_2282); }  else 
{ 
 obj_t BgL_auxz00_11568;
BgL_auxz00_11568 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(42159L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3934z00_6826), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11568,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_11578; long BgL_wz00_11576; long BgL_rz00_11574;
BgL_rz00_11574 = 
(BgL_rz00_2181+6L); 
BgL_wz00_11576 = 
(BgL_wz00_2182+4L); 
BgL_asciiz00_11578 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11578; 
BgL_wz00_2182 = BgL_wz00_11576; 
BgL_rz00_2181 = BgL_rz00_11574; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } } } } } } }  else 
{ /* Llib/unicode.scm 1033 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11584; long BgL_wz00_11582; long BgL_rz00_11580;
BgL_rz00_11580 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11582 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11584 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11584; 
BgL_wz00_2182 = BgL_wz00_11582; 
BgL_rz00_2181 = BgL_rz00_11580; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } } } } }  else 
{ /* Llib/unicode.scm 1024 */
if(
(BgL_ucs2z00_2246<=56319L))
{ /* Llib/unicode.scm 1060 */
 long BgL_xxz00_2303;
BgL_xxz00_2303 = 
(BgL_ucs2z00_2246 & 3L); 
{ /* Llib/unicode.scm 1060 */
 long BgL_wwwwz00_2304;
BgL_wwwwz00_2304 = 
(
(BgL_ucs2z00_2246 >> 
(int)(2L)) & 15L); 
{ /* Llib/unicode.scm 1061 */
 long BgL_vvvvz00_2305;
BgL_vvvvz00_2305 = 
(
(BgL_ucs2z00_2246 >> 
(int)(6L)) & 15L); 
{ /* Llib/unicode.scm 1062 */
 long BgL_uuuuuz00_2306;
BgL_uuuuuz00_2306 = 
(BgL_vvvvz00_2305+1L); 
{ /* Llib/unicode.scm 1063 */

{ /* Llib/unicode.scm 1064 */
 long BgL_arg2181z00_2307; unsigned char BgL_arg2182z00_2308;
BgL_arg2181z00_2307 = 
(BgL_wz00_2182+3L); 
BgL_arg2182z00_2308 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(128L | 
(BgL_uuuuuz00_2306 >> 
(int)(2L)))); 
{ /* Llib/unicode.scm 1064 */
 long BgL_l3938z00_6830;
BgL_l3938z00_6830 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2181z00_2307, BgL_l3938z00_6830))
{ /* Llib/unicode.scm 1064 */
STRING_SET(BgL_resz00_2179, BgL_arg2181z00_2307, BgL_arg2182z00_2308); }  else 
{ 
 obj_t BgL_auxz00_11604;
BgL_auxz00_11604 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(42572L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3938z00_6830), 
(int)(BgL_arg2181z00_2307)); 
FAILURE(BgL_auxz00_11604,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1066 */
 long BgL_arg2185z00_2311; unsigned char BgL_arg2186z00_2312;
BgL_arg2185z00_2311 = 
(BgL_wz00_2182+2L); 
BgL_arg2186z00_2312 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(
(BgL_xxz00_2303 << 
(int)(4L))+128L)); 
{ /* Llib/unicode.scm 1066 */
 long BgL_l3942z00_6834;
BgL_l3942z00_6834 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2185z00_2311, BgL_l3942z00_6834))
{ /* Llib/unicode.scm 1066 */
STRING_SET(BgL_resz00_2179, BgL_arg2185z00_2311, BgL_arg2186z00_2312); }  else 
{ 
 obj_t BgL_auxz00_11619;
BgL_auxz00_11619 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(42664L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3942z00_6834), 
(int)(BgL_arg2185z00_2311)); 
FAILURE(BgL_auxz00_11619,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1068 */
 long BgL_arg2189z00_2315; unsigned char BgL_arg2190z00_2316;
BgL_arg2189z00_2315 = 
(BgL_wz00_2182+1L); 
BgL_arg2190z00_2316 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(128L+
(BgL_wwwwz00_2304 | 
(
(BgL_uuuuuz00_2306 & 3L) << 
(int)(4L))))); 
{ /* Llib/unicode.scm 1068 */
 long BgL_l3946z00_6838;
BgL_l3946z00_6838 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2189z00_2315, BgL_l3946z00_6838))
{ /* Llib/unicode.scm 1068 */
STRING_SET(BgL_resz00_2179, BgL_arg2189z00_2315, BgL_arg2190z00_2316); }  else 
{ 
 obj_t BgL_auxz00_11636;
BgL_auxz00_11636 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(42750L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3946z00_6838), 
(int)(BgL_arg2189z00_2315)); 
FAILURE(BgL_auxz00_11636,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1073 */
 long BgL_l3950z00_6842;
BgL_l3950z00_6842 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3950z00_6842))
{ /* Llib/unicode.scm 1073 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, ((unsigned char)248)); }  else 
{ 
 obj_t BgL_auxz00_11646;
BgL_auxz00_11646 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(42894L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3950z00_6842), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11646,BFALSE,BFALSE);} } 
{ 
 bool_t BgL_asciiz00_11656; long BgL_wz00_11654; long BgL_rz00_11652;
BgL_rz00_11652 = 
(BgL_rz00_2181+3L); 
BgL_wz00_11654 = 
(BgL_wz00_2182+4L); 
BgL_asciiz00_11656 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11656; 
BgL_wz00_2182 = BgL_wz00_11654; 
BgL_rz00_2181 = BgL_rz00_11652; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } } } } }  else 
{ /* Llib/unicode.scm 1076 */
 long BgL_za7za7za7za7za7za7z00_2325; long BgL_yyyyz00_2326;
BgL_za7za7za7za7za7za7z00_2325 = 
(BgL_nz00_2189 & 63L); 
BgL_yyyyz00_2326 = 
(
(BgL_nz00_2189 & 1023L) >> 
(int)(6L)); 
{ /* Llib/unicode.scm 1078 */
 long BgL_arg2200z00_2327; unsigned char BgL_arg2201z00_2328;
BgL_arg2200z00_2327 = 
(BgL_wz00_2182+3L); 
BgL_arg2201z00_2328 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(BgL_za7za7za7za7za7za7z00_2325+128L)); 
{ /* Llib/unicode.scm 1078 */
 long BgL_l3954z00_6846;
BgL_l3954z00_6846 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2200z00_2327, BgL_l3954z00_6846))
{ /* Llib/unicode.scm 1078 */
STRING_SET(BgL_resz00_2179, BgL_arg2200z00_2327, BgL_arg2201z00_2328); }  else 
{ 
 obj_t BgL_auxz00_11668;
BgL_auxz00_11668 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(43082L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3954z00_6846), 
(int)(BgL_arg2200z00_2327)); 
FAILURE(BgL_auxz00_11668,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1080 */
 long BgL_arg2203z00_2330; unsigned char BgL_arg2204z00_2331;
BgL_arg2203z00_2330 = 
(BgL_wz00_2182+2L); 
BgL_arg2204z00_2331 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(BgL_yyyyz00_2326+128L)); 
{ /* Llib/unicode.scm 1080 */
 long BgL_l3958z00_6850;
BgL_l3958z00_6850 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2203z00_2330, BgL_l3958z00_6850))
{ /* Llib/unicode.scm 1080 */
STRING_SET(BgL_resz00_2179, BgL_arg2203z00_2330, BgL_arg2204z00_2331); }  else 
{ 
 obj_t BgL_auxz00_11681;
BgL_auxz00_11681 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(43160L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3958z00_6850), 
(int)(BgL_arg2203z00_2330)); 
FAILURE(BgL_auxz00_11681,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1082 */
 long BgL_arg2206z00_2333;
BgL_arg2206z00_2333 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1082 */
 long BgL_l3962z00_6854;
BgL_l3962z00_6854 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2206z00_2333, BgL_l3962z00_6854))
{ /* Llib/unicode.scm 1082 */
STRING_SET(BgL_resz00_2179, BgL_arg2206z00_2333, ((unsigned char)128)); }  else 
{ 
 obj_t BgL_auxz00_11692;
BgL_auxz00_11692 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(43236L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3962z00_6854), 
(int)(BgL_arg2206z00_2333)); 
FAILURE(BgL_auxz00_11692,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1083 */
 long BgL_l3966z00_6858;
BgL_l3966z00_6858 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3966z00_6858))
{ /* Llib/unicode.scm 1083 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, ((unsigned char)252)); }  else 
{ 
 obj_t BgL_auxz00_11702;
BgL_auxz00_11702 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(43295L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3966z00_6858), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11702,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1088 */
 bool_t BgL_test5694z00_11708;
if(
(BgL_wz00_2182>=4L))
{ /* Llib/unicode.scm 1089 */
 bool_t BgL_test5696z00_11711;
{ /* Llib/unicode.scm 1089 */
 long BgL_indexz00_4965;
BgL_indexz00_4965 = 
(BgL_wz00_2182-4L); 
if(
(
(BgL_wz00_2182+4L)>=
(BgL_indexz00_4965+4L)))
{ /* Llib/unicode.scm 1286 */
 long BgL_tmpz00_11717;
{ /* Llib/unicode.scm 1286 */
 unsigned char BgL_tmpz00_11718;
{ /* Llib/unicode.scm 1286 */
 long BgL_l3970z00_6862;
BgL_l3970z00_6862 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_indexz00_4965, BgL_l3970z00_6862))
{ /* Llib/unicode.scm 1286 */
BgL_tmpz00_11718 = 
STRING_REF(BgL_resz00_2179, BgL_indexz00_4965)
; }  else 
{ 
 obj_t BgL_auxz00_11723;
BgL_auxz00_11723 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51414L), BGl_string4897z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3970z00_6862), 
(int)(BgL_indexz00_4965)); 
FAILURE(BgL_auxz00_11723,BFALSE,BFALSE);} } 
BgL_tmpz00_11717 = 
(BgL_tmpz00_11718); } 
BgL_test5696z00_11711 = 
(BgL_tmpz00_11717==248L); }  else 
{ /* Llib/unicode.scm 1285 */
BgL_test5696z00_11711 = ((bool_t)0)
; } } 
if(BgL_test5696z00_11711)
{ /* Llib/unicode.scm 1089 */
if(
(
(BgL_wz00_2182+4L)>=
(BgL_wz00_2182+4L)))
{ /* Llib/unicode.scm 1296 */
 long BgL_tmpz00_11735;
{ /* Llib/unicode.scm 1296 */
 unsigned char BgL_tmpz00_11736;
{ /* Llib/unicode.scm 1296 */
 long BgL_l3974z00_6866;
BgL_l3974z00_6866 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3974z00_6866))
{ /* Llib/unicode.scm 1296 */
BgL_tmpz00_11736 = 
STRING_REF(BgL_resz00_2179, BgL_wz00_2182)
; }  else 
{ 
 obj_t BgL_auxz00_11741;
BgL_auxz00_11741 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52010L), BGl_string4897z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3974z00_6866), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11741,BFALSE,BFALSE);} } 
BgL_tmpz00_11735 = 
(BgL_tmpz00_11736); } 
BgL_test5694z00_11708 = 
(BgL_tmpz00_11735==252L); }  else 
{ /* Llib/unicode.scm 1295 */
BgL_test5694z00_11708 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1089 */
BgL_test5694z00_11708 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1088 */
BgL_test5694z00_11708 = ((bool_t)0)
; } 
if(BgL_test5694z00_11708)
{ /* Llib/unicode.scm 1088 */
BGl_utf8zd2collapsez12zc0zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182, BgL_resz00_2179, 
BINT(BgL_wz00_2182)); 
{ 
 bool_t BgL_asciiz00_11753; long BgL_rz00_11751;
BgL_rz00_11751 = 
(BgL_rz00_2181+3L); 
BgL_asciiz00_11753 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11753; 
BgL_rz00_2181 = BgL_rz00_11751; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ 
 bool_t BgL_asciiz00_11758; long BgL_wz00_11756; long BgL_rz00_11754;
BgL_rz00_11754 = 
(BgL_rz00_2181+3L); 
BgL_wz00_11756 = 
(BgL_wz00_2182+4L); 
BgL_asciiz00_11758 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11758; 
BgL_wz00_2182 = BgL_wz00_11756; 
BgL_rz00_2181 = BgL_rz00_11754; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } } } } } } } }  else 
{ /* Llib/unicode.scm 1007 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11764; long BgL_wz00_11762; long BgL_rz00_11760;
BgL_rz00_11760 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11762 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11764 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11764; 
BgL_wz00_2182 = BgL_wz00_11762; 
BgL_rz00_2181 = BgL_rz00_11760; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1005 */
if(
(BgL_nz00_2189<=239L))
{ /* Llib/unicode.scm 1102 */
 bool_t BgL_test5702z00_11767;
if(
(BgL_rz00_2181<
(BgL_endz00_81-2L)))
{ /* Llib/unicode.scm 1103 */
 bool_t BgL_test5704z00_11771;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5004;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11772;
{ /* Llib/unicode.scm 1103 */
 long BgL_i3977z00_6869;
BgL_i3977z00_6869 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1103 */
 long BgL_l3978z00_6870;
BgL_l3978z00_6870 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3977z00_6869, BgL_l3978z00_6870))
{ /* Llib/unicode.scm 1103 */
BgL_tmpz00_11772 = 
STRING_REF(BgL_strz00_78, BgL_i3977z00_6869)
; }  else 
{ 
 obj_t BgL_auxz00_11778;
BgL_auxz00_11778 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44002L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3978z00_6870), 
(int)(BgL_i3977z00_6869)); 
FAILURE(BgL_auxz00_11778,BFALSE,BFALSE);} } } 
BgL_nz00_5004 = 
(BgL_tmpz00_11772); } 
if(
(BgL_nz00_5004>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5704z00_11771 = 
(BgL_nz00_5004<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5704z00_11771 = ((bool_t)0)
; } } 
if(BgL_test5704z00_11771)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5012;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11788;
{ /* Llib/unicode.scm 1104 */
 long BgL_i3981z00_6873;
BgL_i3981z00_6873 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1104 */
 long BgL_l3982z00_6874;
BgL_l3982z00_6874 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3981z00_6873, BgL_l3982z00_6874))
{ /* Llib/unicode.scm 1104 */
BgL_tmpz00_11788 = 
STRING_REF(BgL_strz00_78, BgL_i3981z00_6873)
; }  else 
{ 
 obj_t BgL_auxz00_11794;
BgL_auxz00_11794 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44056L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3982z00_6874), 
(int)(BgL_i3981z00_6873)); 
FAILURE(BgL_auxz00_11794,BFALSE,BFALSE);} } } 
BgL_nz00_5012 = 
(BgL_tmpz00_11788); } 
if(
(BgL_nz00_5012>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5702z00_11767 = 
(BgL_nz00_5012<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5702z00_11767 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1103 */
BgL_test5702z00_11767 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1102 */
BgL_test5702z00_11767 = ((bool_t)0)
; } 
if(BgL_test5702z00_11767)
{ /* Llib/unicode.scm 1102 */
{ /* Llib/unicode.scm 1106 */
 long BgL_l3986z00_6878;
BgL_l3986z00_6878 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l3986z00_6878))
{ /* Llib/unicode.scm 1106 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_11808;
BgL_auxz00_11808 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44119L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3986z00_6878), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11808,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1107 */
 long BgL_arg2255z00_2387; unsigned char BgL_arg2256z00_2388;
BgL_arg2255z00_2387 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1107 */
 long BgL_i3989z00_6881;
BgL_i3989z00_6881 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1107 */
 long BgL_l3990z00_6882;
BgL_l3990z00_6882 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3989z00_6881, BgL_l3990z00_6882))
{ /* Llib/unicode.scm 1107 */
BgL_arg2256z00_2388 = 
STRING_REF(BgL_strz00_78, BgL_i3989z00_6881); }  else 
{ 
 obj_t BgL_auxz00_11820;
BgL_auxz00_11820 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44178L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3990z00_6882), 
(int)(BgL_i3989z00_6881)); 
FAILURE(BgL_auxz00_11820,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1107 */
 long BgL_l3994z00_6886;
BgL_l3994z00_6886 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2255z00_2387, BgL_l3994z00_6886))
{ /* Llib/unicode.scm 1107 */
STRING_SET(BgL_resz00_2179, BgL_arg2255z00_2387, BgL_arg2256z00_2388); }  else 
{ 
 obj_t BgL_auxz00_11830;
BgL_auxz00_11830 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44151L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l3994z00_6886), 
(int)(BgL_arg2255z00_2387)); 
FAILURE(BgL_auxz00_11830,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1108 */
 long BgL_arg2258z00_2390; unsigned char BgL_arg2259z00_2391;
BgL_arg2258z00_2390 = 
(BgL_wz00_2182+2L); 
{ /* Llib/unicode.scm 1108 */
 long BgL_i3997z00_6889;
BgL_i3997z00_6889 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1108 */
 long BgL_l3998z00_6890;
BgL_l3998z00_6890 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i3997z00_6889, BgL_l3998z00_6890))
{ /* Llib/unicode.scm 1108 */
BgL_arg2259z00_2391 = 
STRING_REF(BgL_strz00_78, BgL_i3997z00_6889); }  else 
{ 
 obj_t BgL_auxz00_11842;
BgL_auxz00_11842 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44243L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l3998z00_6890), 
(int)(BgL_i3997z00_6889)); 
FAILURE(BgL_auxz00_11842,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1108 */
 long BgL_l4002z00_6894;
BgL_l4002z00_6894 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2258z00_2390, BgL_l4002z00_6894))
{ /* Llib/unicode.scm 1108 */
STRING_SET(BgL_resz00_2179, BgL_arg2258z00_2390, BgL_arg2259z00_2391); }  else 
{ 
 obj_t BgL_auxz00_11852;
BgL_auxz00_11852 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44216L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4002z00_6894), 
(int)(BgL_arg2258z00_2390)); 
FAILURE(BgL_auxz00_11852,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_11862; long BgL_wz00_11860; long BgL_rz00_11858;
BgL_rz00_11858 = 
(BgL_rz00_2181+3L); 
BgL_wz00_11860 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11862 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11862; 
BgL_wz00_2182 = BgL_wz00_11860; 
BgL_rz00_2181 = BgL_rz00_11858; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1102 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_11868; long BgL_wz00_11866; long BgL_rz00_11864;
BgL_rz00_11864 = 
(BgL_rz00_2181+1L); 
BgL_wz00_11866 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_11868 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_11868; 
BgL_wz00_2182 = BgL_wz00_11866; 
BgL_rz00_2181 = BgL_rz00_11864; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1100 */
if(
(BgL_nz00_2189==240L))
{ /* Llib/unicode.scm 1115 */
 bool_t BgL_test5715z00_11871;
if(
(BgL_rz00_2181<
(BgL_endz00_81-3L)))
{ /* Llib/unicode.scm 1116 */
 bool_t BgL_test5717z00_11875;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5045;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11876;
{ /* Llib/unicode.scm 1116 */
 long BgL_i4005z00_6897;
BgL_i4005z00_6897 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1116 */
 long BgL_l4006z00_6898;
BgL_l4006z00_6898 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4005z00_6897, BgL_l4006z00_6898))
{ /* Llib/unicode.scm 1116 */
BgL_tmpz00_11876 = 
STRING_REF(BgL_strz00_78, BgL_i4005z00_6897)
; }  else 
{ 
 obj_t BgL_auxz00_11882;
BgL_auxz00_11882 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44510L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4006z00_6898), 
(int)(BgL_i4005z00_6897)); 
FAILURE(BgL_auxz00_11882,BFALSE,BFALSE);} } } 
BgL_nz00_5045 = 
(BgL_tmpz00_11876); } 
if(
(BgL_nz00_5045>=144L))
{ /* Llib/unicode.scm 951 */
BgL_test5717z00_11875 = 
(BgL_nz00_5045<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5717z00_11875 = ((bool_t)0)
; } } 
if(BgL_test5717z00_11875)
{ /* Llib/unicode.scm 1117 */
 bool_t BgL_test5720z00_11892;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5053;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11893;
{ /* Llib/unicode.scm 1117 */
 long BgL_i4009z00_6901;
BgL_i4009z00_6901 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1117 */
 long BgL_l4010z00_6902;
BgL_l4010z00_6902 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4009z00_6901, BgL_l4010z00_6902))
{ /* Llib/unicode.scm 1117 */
BgL_tmpz00_11893 = 
STRING_REF(BgL_strz00_78, BgL_i4009z00_6901)
; }  else 
{ 
 obj_t BgL_auxz00_11899;
BgL_auxz00_11899 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44564L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4010z00_6902), 
(int)(BgL_i4009z00_6901)); 
FAILURE(BgL_auxz00_11899,BFALSE,BFALSE);} } } 
BgL_nz00_5053 = 
(BgL_tmpz00_11893); } 
if(
(BgL_nz00_5053>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5720z00_11892 = 
(BgL_nz00_5053<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5720z00_11892 = ((bool_t)0)
; } } 
if(BgL_test5720z00_11892)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5061;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_11909;
{ /* Llib/unicode.scm 1118 */
 long BgL_i4013z00_6905;
BgL_i4013z00_6905 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1118 */
 long BgL_l4014z00_6906;
BgL_l4014z00_6906 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4013z00_6905, BgL_l4014z00_6906))
{ /* Llib/unicode.scm 1118 */
BgL_tmpz00_11909 = 
STRING_REF(BgL_strz00_78, BgL_i4013z00_6905)
; }  else 
{ 
 obj_t BgL_auxz00_11915;
BgL_auxz00_11915 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44618L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4014z00_6906), 
(int)(BgL_i4013z00_6905)); 
FAILURE(BgL_auxz00_11915,BFALSE,BFALSE);} } } 
BgL_nz00_5061 = 
(BgL_tmpz00_11909); } 
if(
(BgL_nz00_5061>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5715z00_11871 = 
(BgL_nz00_5061<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5715z00_11871 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1117 */
BgL_test5715z00_11871 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1116 */
BgL_test5715z00_11871 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1115 */
BgL_test5715z00_11871 = ((bool_t)0)
; } 
if(BgL_test5715z00_11871)
{ /* Llib/unicode.scm 1115 */
{ /* Llib/unicode.scm 1120 */
 long BgL_l4018z00_6910;
BgL_l4018z00_6910 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l4018z00_6910))
{ /* Llib/unicode.scm 1120 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_11929;
BgL_auxz00_11929 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44681L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4018z00_6910), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_11929,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1121 */
 long BgL_arg2288z00_2421; unsigned char BgL_arg2289z00_2422;
BgL_arg2288z00_2421 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1121 */
 long BgL_i4021z00_6913;
BgL_i4021z00_6913 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1121 */
 long BgL_l4022z00_6914;
BgL_l4022z00_6914 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4021z00_6913, BgL_l4022z00_6914))
{ /* Llib/unicode.scm 1121 */
BgL_arg2289z00_2422 = 
STRING_REF(BgL_strz00_78, BgL_i4021z00_6913); }  else 
{ 
 obj_t BgL_auxz00_11941;
BgL_auxz00_11941 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44740L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4022z00_6914), 
(int)(BgL_i4021z00_6913)); 
FAILURE(BgL_auxz00_11941,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1121 */
 long BgL_l4026z00_6918;
BgL_l4026z00_6918 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2288z00_2421, BgL_l4026z00_6918))
{ /* Llib/unicode.scm 1121 */
STRING_SET(BgL_resz00_2179, BgL_arg2288z00_2421, BgL_arg2289z00_2422); }  else 
{ 
 obj_t BgL_auxz00_11951;
BgL_auxz00_11951 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44713L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4026z00_6918), 
(int)(BgL_arg2288z00_2421)); 
FAILURE(BgL_auxz00_11951,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1122 */
 long BgL_arg2291z00_2424; unsigned char BgL_arg2292z00_2425;
BgL_arg2291z00_2424 = 
(BgL_wz00_2182+2L); 
{ /* Llib/unicode.scm 1122 */
 long BgL_i4029z00_6921;
BgL_i4029z00_6921 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1122 */
 long BgL_l4030z00_6922;
BgL_l4030z00_6922 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4029z00_6921, BgL_l4030z00_6922))
{ /* Llib/unicode.scm 1122 */
BgL_arg2292z00_2425 = 
STRING_REF(BgL_strz00_78, BgL_i4029z00_6921); }  else 
{ 
 obj_t BgL_auxz00_11963;
BgL_auxz00_11963 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44805L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4030z00_6922), 
(int)(BgL_i4029z00_6921)); 
FAILURE(BgL_auxz00_11963,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1122 */
 long BgL_l4034z00_6926;
BgL_l4034z00_6926 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2291z00_2424, BgL_l4034z00_6926))
{ /* Llib/unicode.scm 1122 */
STRING_SET(BgL_resz00_2179, BgL_arg2291z00_2424, BgL_arg2292z00_2425); }  else 
{ 
 obj_t BgL_auxz00_11973;
BgL_auxz00_11973 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44778L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4034z00_6926), 
(int)(BgL_arg2291z00_2424)); 
FAILURE(BgL_auxz00_11973,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1123 */
 long BgL_arg2294z00_2427; unsigned char BgL_arg2295z00_2428;
BgL_arg2294z00_2427 = 
(BgL_wz00_2182+3L); 
{ /* Llib/unicode.scm 1123 */
 long BgL_i4037z00_6929;
BgL_i4037z00_6929 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1123 */
 long BgL_l4038z00_6930;
BgL_l4038z00_6930 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4037z00_6929, BgL_l4038z00_6930))
{ /* Llib/unicode.scm 1123 */
BgL_arg2295z00_2428 = 
STRING_REF(BgL_strz00_78, BgL_i4037z00_6929); }  else 
{ 
 obj_t BgL_auxz00_11985;
BgL_auxz00_11985 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44870L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4038z00_6930), 
(int)(BgL_i4037z00_6929)); 
FAILURE(BgL_auxz00_11985,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1123 */
 long BgL_l4042z00_6934;
BgL_l4042z00_6934 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2294z00_2427, BgL_l4042z00_6934))
{ /* Llib/unicode.scm 1123 */
STRING_SET(BgL_resz00_2179, BgL_arg2294z00_2427, BgL_arg2295z00_2428); }  else 
{ 
 obj_t BgL_auxz00_11995;
BgL_auxz00_11995 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(44843L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4042z00_6934), 
(int)(BgL_arg2294z00_2427)); 
FAILURE(BgL_auxz00_11995,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_12005; long BgL_wz00_12003; long BgL_rz00_12001;
BgL_rz00_12001 = 
(BgL_rz00_2181+4L); 
BgL_wz00_12003 = 
(BgL_wz00_2182+4L); 
BgL_asciiz00_12005 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12005; 
BgL_wz00_2182 = BgL_wz00_12003; 
BgL_rz00_2181 = BgL_rz00_12001; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1115 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_12011; long BgL_wz00_12009; long BgL_rz00_12007;
BgL_rz00_12007 = 
(BgL_rz00_2181+1L); 
BgL_wz00_12009 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_12011 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12011; 
BgL_wz00_2182 = BgL_wz00_12009; 
BgL_rz00_2181 = BgL_rz00_12007; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1128 */
 bool_t BgL_test5732z00_12012;
if(
(BgL_nz00_2189==244L))
{ /* Llib/unicode.scm 1128 */
BgL_test5732z00_12012 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1129 */
 bool_t BgL_test5734z00_12015;
{ /* Llib/unicode.scm 1129 */
 bool_t BgL__ortest_1055z00_2644;
BgL__ortest_1055z00_2644 = 
(BgL_nz00_2189==248L); 
if(BgL__ortest_1055z00_2644)
{ /* Llib/unicode.scm 1129 */
BgL_test5734z00_12015 = BgL__ortest_1055z00_2644
; }  else 
{ /* Llib/unicode.scm 1129 */
BgL_test5734z00_12015 = 
(BgL_nz00_2189==252L)
; } } 
if(BgL_test5734z00_12015)
{ /* Llib/unicode.scm 1129 */
if(BgL_strictz00_79)
{ /* Llib/unicode.scm 1129 */
BgL_test5732z00_12012 = ((bool_t)0)
; }  else 
{ /* Llib/unicode.scm 1129 */
BgL_test5732z00_12012 = ((bool_t)1)
; } }  else 
{ /* Llib/unicode.scm 1129 */
BgL_test5732z00_12012 = ((bool_t)0)
; } } 
if(BgL_test5732z00_12012)
{ /* Llib/unicode.scm 1131 */
 bool_t BgL_test5737z00_12020;
if(
(BgL_rz00_2181<
(BgL_endz00_81-3L)))
{ /* Llib/unicode.scm 1132 */
 bool_t BgL_test5739z00_12024;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5103;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12025;
{ /* Llib/unicode.scm 1132 */
 long BgL_i4045z00_6937;
BgL_i4045z00_6937 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1132 */
 long BgL_l4046z00_6938;
BgL_l4046z00_6938 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4045z00_6937, BgL_l4046z00_6938))
{ /* Llib/unicode.scm 1132 */
BgL_tmpz00_12025 = 
STRING_REF(BgL_strz00_78, BgL_i4045z00_6937)
; }  else 
{ 
 obj_t BgL_auxz00_12031;
BgL_auxz00_12031 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45199L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4046z00_6938), 
(int)(BgL_i4045z00_6937)); 
FAILURE(BgL_auxz00_12031,BFALSE,BFALSE);} } } 
BgL_nz00_5103 = 
(BgL_tmpz00_12025); } 
if(
(BgL_nz00_5103>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5739z00_12024 = 
(BgL_nz00_5103<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5739z00_12024 = ((bool_t)0)
; } } 
if(BgL_test5739z00_12024)
{ /* Llib/unicode.scm 1133 */
 bool_t BgL_test5742z00_12041;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5111;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12042;
{ /* Llib/unicode.scm 1133 */
 long BgL_i4049z00_6941;
BgL_i4049z00_6941 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1133 */
 long BgL_l4050z00_6942;
BgL_l4050z00_6942 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4049z00_6941, BgL_l4050z00_6942))
{ /* Llib/unicode.scm 1133 */
BgL_tmpz00_12042 = 
STRING_REF(BgL_strz00_78, BgL_i4049z00_6941)
; }  else 
{ 
 obj_t BgL_auxz00_12048;
BgL_auxz00_12048 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45253L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4050z00_6942), 
(int)(BgL_i4049z00_6941)); 
FAILURE(BgL_auxz00_12048,BFALSE,BFALSE);} } } 
BgL_nz00_5111 = 
(BgL_tmpz00_12042); } 
if(
(BgL_nz00_5111>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5742z00_12041 = 
(BgL_nz00_5111<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5742z00_12041 = ((bool_t)0)
; } } 
if(BgL_test5742z00_12041)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5119;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12058;
{ /* Llib/unicode.scm 1134 */
 long BgL_i4053z00_6945;
BgL_i4053z00_6945 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1134 */
 long BgL_l4054z00_6946;
BgL_l4054z00_6946 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4053z00_6945, BgL_l4054z00_6946))
{ /* Llib/unicode.scm 1134 */
BgL_tmpz00_12058 = 
STRING_REF(BgL_strz00_78, BgL_i4053z00_6945)
; }  else 
{ 
 obj_t BgL_auxz00_12064;
BgL_auxz00_12064 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45307L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4054z00_6946), 
(int)(BgL_i4053z00_6945)); 
FAILURE(BgL_auxz00_12064,BFALSE,BFALSE);} } } 
BgL_nz00_5119 = 
(BgL_tmpz00_12058); } 
if(
(BgL_nz00_5119>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5737z00_12020 = 
(BgL_nz00_5119<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5737z00_12020 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1133 */
BgL_test5737z00_12020 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1132 */
BgL_test5737z00_12020 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1131 */
BgL_test5737z00_12020 = ((bool_t)0)
; } 
if(BgL_test5737z00_12020)
{ /* Llib/unicode.scm 1131 */
{ /* Llib/unicode.scm 1136 */
 long BgL_l4058z00_6950;
BgL_l4058z00_6950 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l4058z00_6950))
{ /* Llib/unicode.scm 1136 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_12078;
BgL_auxz00_12078 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45370L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4058z00_6950), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_12078,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1137 */
 long BgL_arg2330z00_2464; unsigned char BgL_arg2331z00_2465;
BgL_arg2330z00_2464 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1137 */
 long BgL_i4061z00_6953;
BgL_i4061z00_6953 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1137 */
 long BgL_l4062z00_6954;
BgL_l4062z00_6954 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4061z00_6953, BgL_l4062z00_6954))
{ /* Llib/unicode.scm 1137 */
BgL_arg2331z00_2465 = 
STRING_REF(BgL_strz00_78, BgL_i4061z00_6953); }  else 
{ 
 obj_t BgL_auxz00_12090;
BgL_auxz00_12090 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45429L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4062z00_6954), 
(int)(BgL_i4061z00_6953)); 
FAILURE(BgL_auxz00_12090,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1137 */
 long BgL_l4066z00_6958;
BgL_l4066z00_6958 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2330z00_2464, BgL_l4066z00_6958))
{ /* Llib/unicode.scm 1137 */
STRING_SET(BgL_resz00_2179, BgL_arg2330z00_2464, BgL_arg2331z00_2465); }  else 
{ 
 obj_t BgL_auxz00_12100;
BgL_auxz00_12100 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45402L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4066z00_6958), 
(int)(BgL_arg2330z00_2464)); 
FAILURE(BgL_auxz00_12100,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1138 */
 long BgL_arg2335z00_2467; unsigned char BgL_arg2336z00_2468;
BgL_arg2335z00_2467 = 
(BgL_wz00_2182+2L); 
{ /* Llib/unicode.scm 1138 */
 long BgL_i4069z00_6961;
BgL_i4069z00_6961 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1138 */
 long BgL_l4070z00_6962;
BgL_l4070z00_6962 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4069z00_6961, BgL_l4070z00_6962))
{ /* Llib/unicode.scm 1138 */
BgL_arg2336z00_2468 = 
STRING_REF(BgL_strz00_78, BgL_i4069z00_6961); }  else 
{ 
 obj_t BgL_auxz00_12112;
BgL_auxz00_12112 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45494L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4070z00_6962), 
(int)(BgL_i4069z00_6961)); 
FAILURE(BgL_auxz00_12112,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1138 */
 long BgL_l4074z00_6966;
BgL_l4074z00_6966 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2335z00_2467, BgL_l4074z00_6966))
{ /* Llib/unicode.scm 1138 */
STRING_SET(BgL_resz00_2179, BgL_arg2335z00_2467, BgL_arg2336z00_2468); }  else 
{ 
 obj_t BgL_auxz00_12122;
BgL_auxz00_12122 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45467L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4074z00_6966), 
(int)(BgL_arg2335z00_2467)); 
FAILURE(BgL_auxz00_12122,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1139 */
 long BgL_arg2338z00_2470; unsigned char BgL_arg2339z00_2471;
BgL_arg2338z00_2470 = 
(BgL_wz00_2182+3L); 
{ /* Llib/unicode.scm 1139 */
 long BgL_i4077z00_6969;
BgL_i4077z00_6969 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1139 */
 long BgL_l4078z00_6970;
BgL_l4078z00_6970 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4077z00_6969, BgL_l4078z00_6970))
{ /* Llib/unicode.scm 1139 */
BgL_arg2339z00_2471 = 
STRING_REF(BgL_strz00_78, BgL_i4077z00_6969); }  else 
{ 
 obj_t BgL_auxz00_12134;
BgL_auxz00_12134 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45559L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4078z00_6970), 
(int)(BgL_i4077z00_6969)); 
FAILURE(BgL_auxz00_12134,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1139 */
 long BgL_l4082z00_6974;
BgL_l4082z00_6974 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2338z00_2470, BgL_l4082z00_6974))
{ /* Llib/unicode.scm 1139 */
STRING_SET(BgL_resz00_2179, BgL_arg2338z00_2470, BgL_arg2339z00_2471); }  else 
{ 
 obj_t BgL_auxz00_12144;
BgL_auxz00_12144 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45532L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4082z00_6974), 
(int)(BgL_arg2338z00_2470)); 
FAILURE(BgL_auxz00_12144,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_12154; long BgL_wz00_12152; long BgL_rz00_12150;
BgL_rz00_12150 = 
(BgL_rz00_2181+4L); 
BgL_wz00_12152 = 
(BgL_wz00_2182+4L); 
BgL_asciiz00_12154 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12154; 
BgL_wz00_2182 = BgL_wz00_12152; 
BgL_rz00_2181 = BgL_rz00_12150; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1131 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_12160; long BgL_wz00_12158; long BgL_rz00_12156;
BgL_rz00_12156 = 
(BgL_rz00_2181+1L); 
BgL_wz00_12158 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_12160 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12160; 
BgL_wz00_2182 = BgL_wz00_12158; 
BgL_rz00_2181 = BgL_rz00_12156; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1128 */
if(
(BgL_nz00_2189<=247L))
{ /* Llib/unicode.scm 1146 */
 bool_t BgL_test5755z00_12163;
if(
(BgL_rz00_2181<
(BgL_endz00_81-3L)))
{ /* Llib/unicode.scm 1147 */
 bool_t BgL_test5757z00_12167;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5159;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12168;
{ /* Llib/unicode.scm 1147 */
 long BgL_i4085z00_6977;
BgL_i4085z00_6977 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1147 */
 long BgL_l4086z00_6978;
BgL_l4086z00_6978 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4085z00_6977, BgL_l4086z00_6978))
{ /* Llib/unicode.scm 1147 */
BgL_tmpz00_12168 = 
STRING_REF(BgL_strz00_78, BgL_i4085z00_6977)
; }  else 
{ 
 obj_t BgL_auxz00_12174;
BgL_auxz00_12174 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45818L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4086z00_6978), 
(int)(BgL_i4085z00_6977)); 
FAILURE(BgL_auxz00_12174,BFALSE,BFALSE);} } } 
BgL_nz00_5159 = 
(BgL_tmpz00_12168); } 
if(
(BgL_nz00_5159>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5757z00_12167 = 
(BgL_nz00_5159<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5757z00_12167 = ((bool_t)0)
; } } 
if(BgL_test5757z00_12167)
{ /* Llib/unicode.scm 1148 */
 bool_t BgL_test5760z00_12184;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5167;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12185;
{ /* Llib/unicode.scm 1148 */
 long BgL_i4089z00_6981;
BgL_i4089z00_6981 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1148 */
 long BgL_l4090z00_6982;
BgL_l4090z00_6982 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4089z00_6981, BgL_l4090z00_6982))
{ /* Llib/unicode.scm 1148 */
BgL_tmpz00_12185 = 
STRING_REF(BgL_strz00_78, BgL_i4089z00_6981)
; }  else 
{ 
 obj_t BgL_auxz00_12191;
BgL_auxz00_12191 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45872L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4090z00_6982), 
(int)(BgL_i4089z00_6981)); 
FAILURE(BgL_auxz00_12191,BFALSE,BFALSE);} } } 
BgL_nz00_5167 = 
(BgL_tmpz00_12185); } 
if(
(BgL_nz00_5167>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5760z00_12184 = 
(BgL_nz00_5167<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5760z00_12184 = ((bool_t)0)
; } } 
if(BgL_test5760z00_12184)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5175;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12201;
{ /* Llib/unicode.scm 1149 */
 long BgL_i4093z00_6985;
BgL_i4093z00_6985 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1149 */
 long BgL_l4094z00_6986;
BgL_l4094z00_6986 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4093z00_6985, BgL_l4094z00_6986))
{ /* Llib/unicode.scm 1149 */
BgL_tmpz00_12201 = 
STRING_REF(BgL_strz00_78, BgL_i4093z00_6985)
; }  else 
{ 
 obj_t BgL_auxz00_12207;
BgL_auxz00_12207 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45926L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4094z00_6986), 
(int)(BgL_i4093z00_6985)); 
FAILURE(BgL_auxz00_12207,BFALSE,BFALSE);} } } 
BgL_nz00_5175 = 
(BgL_tmpz00_12201); } 
if(
(BgL_nz00_5175>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5755z00_12163 = 
(BgL_nz00_5175<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5755z00_12163 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1148 */
BgL_test5755z00_12163 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1147 */
BgL_test5755z00_12163 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1146 */
BgL_test5755z00_12163 = ((bool_t)0)
; } 
if(BgL_test5755z00_12163)
{ /* Llib/unicode.scm 1146 */
{ /* Llib/unicode.scm 1151 */
 long BgL_l4098z00_6990;
BgL_l4098z00_6990 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l4098z00_6990))
{ /* Llib/unicode.scm 1151 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_12221;
BgL_auxz00_12221 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(45989L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4098z00_6990), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_12221,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1152 */
 long BgL_arg2375z00_2504; unsigned char BgL_arg2376z00_2505;
BgL_arg2375z00_2504 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1152 */
 long BgL_i4101z00_6993;
BgL_i4101z00_6993 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1152 */
 long BgL_l4102z00_6994;
BgL_l4102z00_6994 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4101z00_6993, BgL_l4102z00_6994))
{ /* Llib/unicode.scm 1152 */
BgL_arg2376z00_2505 = 
STRING_REF(BgL_strz00_78, BgL_i4101z00_6993); }  else 
{ 
 obj_t BgL_auxz00_12233;
BgL_auxz00_12233 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46048L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4102z00_6994), 
(int)(BgL_i4101z00_6993)); 
FAILURE(BgL_auxz00_12233,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1152 */
 long BgL_l4106z00_6998;
BgL_l4106z00_6998 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2375z00_2504, BgL_l4106z00_6998))
{ /* Llib/unicode.scm 1152 */
STRING_SET(BgL_resz00_2179, BgL_arg2375z00_2504, BgL_arg2376z00_2505); }  else 
{ 
 obj_t BgL_auxz00_12243;
BgL_auxz00_12243 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46021L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4106z00_6998), 
(int)(BgL_arg2375z00_2504)); 
FAILURE(BgL_auxz00_12243,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1153 */
 long BgL_arg2378z00_2507; unsigned char BgL_arg2379z00_2508;
BgL_arg2378z00_2507 = 
(BgL_wz00_2182+2L); 
{ /* Llib/unicode.scm 1153 */
 long BgL_i4109z00_7001;
BgL_i4109z00_7001 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1153 */
 long BgL_l4110z00_7002;
BgL_l4110z00_7002 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4109z00_7001, BgL_l4110z00_7002))
{ /* Llib/unicode.scm 1153 */
BgL_arg2379z00_2508 = 
STRING_REF(BgL_strz00_78, BgL_i4109z00_7001); }  else 
{ 
 obj_t BgL_auxz00_12255;
BgL_auxz00_12255 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46113L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4110z00_7002), 
(int)(BgL_i4109z00_7001)); 
FAILURE(BgL_auxz00_12255,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1153 */
 long BgL_l4114z00_7006;
BgL_l4114z00_7006 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2378z00_2507, BgL_l4114z00_7006))
{ /* Llib/unicode.scm 1153 */
STRING_SET(BgL_resz00_2179, BgL_arg2378z00_2507, BgL_arg2379z00_2508); }  else 
{ 
 obj_t BgL_auxz00_12265;
BgL_auxz00_12265 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46086L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4114z00_7006), 
(int)(BgL_arg2378z00_2507)); 
FAILURE(BgL_auxz00_12265,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1154 */
 long BgL_arg2381z00_2510; unsigned char BgL_arg2382z00_2511;
BgL_arg2381z00_2510 = 
(BgL_wz00_2182+3L); 
{ /* Llib/unicode.scm 1154 */
 long BgL_i4117z00_7009;
BgL_i4117z00_7009 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1154 */
 long BgL_l4118z00_7010;
BgL_l4118z00_7010 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4117z00_7009, BgL_l4118z00_7010))
{ /* Llib/unicode.scm 1154 */
BgL_arg2382z00_2511 = 
STRING_REF(BgL_strz00_78, BgL_i4117z00_7009); }  else 
{ 
 obj_t BgL_auxz00_12277;
BgL_auxz00_12277 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46178L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4118z00_7010), 
(int)(BgL_i4117z00_7009)); 
FAILURE(BgL_auxz00_12277,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1154 */
 long BgL_l4122z00_7014;
BgL_l4122z00_7014 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2381z00_2510, BgL_l4122z00_7014))
{ /* Llib/unicode.scm 1154 */
STRING_SET(BgL_resz00_2179, BgL_arg2381z00_2510, BgL_arg2382z00_2511); }  else 
{ 
 obj_t BgL_auxz00_12287;
BgL_auxz00_12287 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46151L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4122z00_7014), 
(int)(BgL_arg2381z00_2510)); 
FAILURE(BgL_auxz00_12287,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_12297; long BgL_wz00_12295; long BgL_rz00_12293;
BgL_rz00_12293 = 
(BgL_rz00_2181+4L); 
BgL_wz00_12295 = 
(BgL_wz00_2182+4L); 
BgL_asciiz00_12297 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12297; 
BgL_wz00_2182 = BgL_wz00_12295; 
BgL_rz00_2181 = BgL_rz00_12293; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1146 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_12303; long BgL_wz00_12301; long BgL_rz00_12299;
BgL_rz00_12299 = 
(BgL_rz00_2181+1L); 
BgL_wz00_12301 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_12303 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12303; 
BgL_wz00_2182 = BgL_wz00_12301; 
BgL_rz00_2181 = BgL_rz00_12299; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1144 */
if(
(BgL_nz00_2189<=251L))
{ /* Llib/unicode.scm 1160 */
 bool_t BgL_test5773z00_12306;
if(
(BgL_rz00_2181<
(BgL_endz00_81-4L)))
{ /* Llib/unicode.scm 1161 */
 bool_t BgL_test5775z00_12310;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5215;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12311;
{ /* Llib/unicode.scm 1161 */
 long BgL_i4125z00_7017;
BgL_i4125z00_7017 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1161 */
 long BgL_l4126z00_7018;
BgL_l4126z00_7018 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4125z00_7017, BgL_l4126z00_7018))
{ /* Llib/unicode.scm 1161 */
BgL_tmpz00_12311 = 
STRING_REF(BgL_strz00_78, BgL_i4125z00_7017)
; }  else 
{ 
 obj_t BgL_auxz00_12317;
BgL_auxz00_12317 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46414L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4126z00_7018), 
(int)(BgL_i4125z00_7017)); 
FAILURE(BgL_auxz00_12317,BFALSE,BFALSE);} } } 
BgL_nz00_5215 = 
(BgL_tmpz00_12311); } 
if(
(BgL_nz00_5215>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5775z00_12310 = 
(BgL_nz00_5215<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5775z00_12310 = ((bool_t)0)
; } } 
if(BgL_test5775z00_12310)
{ /* Llib/unicode.scm 1162 */
 bool_t BgL_test5778z00_12327;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5223;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12328;
{ /* Llib/unicode.scm 1162 */
 long BgL_i4129z00_7021;
BgL_i4129z00_7021 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1162 */
 long BgL_l4130z00_7022;
BgL_l4130z00_7022 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4129z00_7021, BgL_l4130z00_7022))
{ /* Llib/unicode.scm 1162 */
BgL_tmpz00_12328 = 
STRING_REF(BgL_strz00_78, BgL_i4129z00_7021)
; }  else 
{ 
 obj_t BgL_auxz00_12334;
BgL_auxz00_12334 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46468L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4130z00_7022), 
(int)(BgL_i4129z00_7021)); 
FAILURE(BgL_auxz00_12334,BFALSE,BFALSE);} } } 
BgL_nz00_5223 = 
(BgL_tmpz00_12328); } 
if(
(BgL_nz00_5223>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5778z00_12327 = 
(BgL_nz00_5223<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5778z00_12327 = ((bool_t)0)
; } } 
if(BgL_test5778z00_12327)
{ /* Llib/unicode.scm 1163 */
 bool_t BgL_test5781z00_12344;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5231;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12345;
{ /* Llib/unicode.scm 1163 */
 long BgL_i4133z00_7025;
BgL_i4133z00_7025 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1163 */
 long BgL_l4134z00_7026;
BgL_l4134z00_7026 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4133z00_7025, BgL_l4134z00_7026))
{ /* Llib/unicode.scm 1163 */
BgL_tmpz00_12345 = 
STRING_REF(BgL_strz00_78, BgL_i4133z00_7025)
; }  else 
{ 
 obj_t BgL_auxz00_12351;
BgL_auxz00_12351 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46522L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4134z00_7026), 
(int)(BgL_i4133z00_7025)); 
FAILURE(BgL_auxz00_12351,BFALSE,BFALSE);} } } 
BgL_nz00_5231 = 
(BgL_tmpz00_12345); } 
if(
(BgL_nz00_5231>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5781z00_12344 = 
(BgL_nz00_5231<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5781z00_12344 = ((bool_t)0)
; } } 
if(BgL_test5781z00_12344)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5239;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12361;
{ /* Llib/unicode.scm 1164 */
 long BgL_i4137z00_7029;
BgL_i4137z00_7029 = 
(BgL_rz00_2181+4L); 
{ /* Llib/unicode.scm 1164 */
 long BgL_l4138z00_7030;
BgL_l4138z00_7030 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4137z00_7029, BgL_l4138z00_7030))
{ /* Llib/unicode.scm 1164 */
BgL_tmpz00_12361 = 
STRING_REF(BgL_strz00_78, BgL_i4137z00_7029)
; }  else 
{ 
 obj_t BgL_auxz00_12367;
BgL_auxz00_12367 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46576L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4138z00_7030), 
(int)(BgL_i4137z00_7029)); 
FAILURE(BgL_auxz00_12367,BFALSE,BFALSE);} } } 
BgL_nz00_5239 = 
(BgL_tmpz00_12361); } 
if(
(BgL_nz00_5239>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5773z00_12306 = 
(BgL_nz00_5239<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5773z00_12306 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1163 */
BgL_test5773z00_12306 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1162 */
BgL_test5773z00_12306 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1161 */
BgL_test5773z00_12306 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1160 */
BgL_test5773z00_12306 = ((bool_t)0)
; } 
if(BgL_test5773z00_12306)
{ /* Llib/unicode.scm 1160 */
{ /* Llib/unicode.scm 1166 */
 long BgL_l4142z00_7034;
BgL_l4142z00_7034 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l4142z00_7034))
{ /* Llib/unicode.scm 1166 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_12381;
BgL_auxz00_12381 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46639L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4142z00_7034), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_12381,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1167 */
 long BgL_arg2423z00_2549; unsigned char BgL_arg2424z00_2550;
BgL_arg2423z00_2549 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1167 */
 long BgL_i4145z00_7037;
BgL_i4145z00_7037 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1167 */
 long BgL_l4146z00_7038;
BgL_l4146z00_7038 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4145z00_7037, BgL_l4146z00_7038))
{ /* Llib/unicode.scm 1167 */
BgL_arg2424z00_2550 = 
STRING_REF(BgL_strz00_78, BgL_i4145z00_7037); }  else 
{ 
 obj_t BgL_auxz00_12393;
BgL_auxz00_12393 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46698L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4146z00_7038), 
(int)(BgL_i4145z00_7037)); 
FAILURE(BgL_auxz00_12393,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1167 */
 long BgL_l4150z00_7042;
BgL_l4150z00_7042 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2423z00_2549, BgL_l4150z00_7042))
{ /* Llib/unicode.scm 1167 */
STRING_SET(BgL_resz00_2179, BgL_arg2423z00_2549, BgL_arg2424z00_2550); }  else 
{ 
 obj_t BgL_auxz00_12403;
BgL_auxz00_12403 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46671L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4150z00_7042), 
(int)(BgL_arg2423z00_2549)); 
FAILURE(BgL_auxz00_12403,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1168 */
 long BgL_arg2426z00_2552; unsigned char BgL_arg2428z00_2553;
BgL_arg2426z00_2552 = 
(BgL_wz00_2182+2L); 
{ /* Llib/unicode.scm 1168 */
 long BgL_i4153z00_7045;
BgL_i4153z00_7045 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1168 */
 long BgL_l4154z00_7046;
BgL_l4154z00_7046 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4153z00_7045, BgL_l4154z00_7046))
{ /* Llib/unicode.scm 1168 */
BgL_arg2428z00_2553 = 
STRING_REF(BgL_strz00_78, BgL_i4153z00_7045); }  else 
{ 
 obj_t BgL_auxz00_12415;
BgL_auxz00_12415 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46763L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4154z00_7046), 
(int)(BgL_i4153z00_7045)); 
FAILURE(BgL_auxz00_12415,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1168 */
 long BgL_l4158z00_7050;
BgL_l4158z00_7050 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2426z00_2552, BgL_l4158z00_7050))
{ /* Llib/unicode.scm 1168 */
STRING_SET(BgL_resz00_2179, BgL_arg2426z00_2552, BgL_arg2428z00_2553); }  else 
{ 
 obj_t BgL_auxz00_12425;
BgL_auxz00_12425 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46736L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4158z00_7050), 
(int)(BgL_arg2426z00_2552)); 
FAILURE(BgL_auxz00_12425,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1169 */
 long BgL_arg2430z00_2555; unsigned char BgL_arg2431z00_2556;
BgL_arg2430z00_2555 = 
(BgL_wz00_2182+3L); 
{ /* Llib/unicode.scm 1169 */
 long BgL_i4161z00_7053;
BgL_i4161z00_7053 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1169 */
 long BgL_l4162z00_7054;
BgL_l4162z00_7054 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4161z00_7053, BgL_l4162z00_7054))
{ /* Llib/unicode.scm 1169 */
BgL_arg2431z00_2556 = 
STRING_REF(BgL_strz00_78, BgL_i4161z00_7053); }  else 
{ 
 obj_t BgL_auxz00_12437;
BgL_auxz00_12437 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46828L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4162z00_7054), 
(int)(BgL_i4161z00_7053)); 
FAILURE(BgL_auxz00_12437,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1169 */
 long BgL_l4166z00_7058;
BgL_l4166z00_7058 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2430z00_2555, BgL_l4166z00_7058))
{ /* Llib/unicode.scm 1169 */
STRING_SET(BgL_resz00_2179, BgL_arg2430z00_2555, BgL_arg2431z00_2556); }  else 
{ 
 obj_t BgL_auxz00_12447;
BgL_auxz00_12447 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46801L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4166z00_7058), 
(int)(BgL_arg2430z00_2555)); 
FAILURE(BgL_auxz00_12447,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1170 */
 long BgL_arg2434z00_2558; unsigned char BgL_arg2435z00_2559;
BgL_arg2434z00_2558 = 
(BgL_wz00_2182+4L); 
{ /* Llib/unicode.scm 1170 */
 long BgL_i4169z00_7061;
BgL_i4169z00_7061 = 
(BgL_rz00_2181+4L); 
{ /* Llib/unicode.scm 1170 */
 long BgL_l4170z00_7062;
BgL_l4170z00_7062 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4169z00_7061, BgL_l4170z00_7062))
{ /* Llib/unicode.scm 1170 */
BgL_arg2435z00_2559 = 
STRING_REF(BgL_strz00_78, BgL_i4169z00_7061); }  else 
{ 
 obj_t BgL_auxz00_12459;
BgL_auxz00_12459 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46893L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4170z00_7062), 
(int)(BgL_i4169z00_7061)); 
FAILURE(BgL_auxz00_12459,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1170 */
 long BgL_l4174z00_7066;
BgL_l4174z00_7066 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2434z00_2558, BgL_l4174z00_7066))
{ /* Llib/unicode.scm 1170 */
STRING_SET(BgL_resz00_2179, BgL_arg2434z00_2558, BgL_arg2435z00_2559); }  else 
{ 
 obj_t BgL_auxz00_12469;
BgL_auxz00_12469 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(46866L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4174z00_7066), 
(int)(BgL_arg2434z00_2558)); 
FAILURE(BgL_auxz00_12469,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_12479; long BgL_wz00_12477; long BgL_rz00_12475;
BgL_rz00_12475 = 
(BgL_rz00_2181+5L); 
BgL_wz00_12477 = 
(BgL_wz00_2182+5L); 
BgL_asciiz00_12479 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12479; 
BgL_wz00_2182 = BgL_wz00_12477; 
BgL_rz00_2181 = BgL_rz00_12475; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1160 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_12485; long BgL_wz00_12483; long BgL_rz00_12481;
BgL_rz00_12481 = 
(BgL_rz00_2181+1L); 
BgL_wz00_12483 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_12485 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12485; 
BgL_wz00_2182 = BgL_wz00_12483; 
BgL_rz00_2181 = BgL_rz00_12481; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1159 */
if(
(BgL_nz00_2189<=253L))
{ /* Llib/unicode.scm 1176 */
 bool_t BgL_test5796z00_12488;
if(
(BgL_rz00_2181<
(BgL_endz00_81-5L)))
{ /* Llib/unicode.scm 1177 */
 bool_t BgL_test5798z00_12492;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5286;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12493;
{ /* Llib/unicode.scm 1177 */
 long BgL_i4177z00_7069;
BgL_i4177z00_7069 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1177 */
 long BgL_l4178z00_7070;
BgL_l4178z00_7070 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4177z00_7069, BgL_l4178z00_7070))
{ /* Llib/unicode.scm 1177 */
BgL_tmpz00_12493 = 
STRING_REF(BgL_strz00_78, BgL_i4177z00_7069)
; }  else 
{ 
 obj_t BgL_auxz00_12499;
BgL_auxz00_12499 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47129L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4178z00_7070), 
(int)(BgL_i4177z00_7069)); 
FAILURE(BgL_auxz00_12499,BFALSE,BFALSE);} } } 
BgL_nz00_5286 = 
(BgL_tmpz00_12493); } 
if(
(BgL_nz00_5286>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5798z00_12492 = 
(BgL_nz00_5286<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5798z00_12492 = ((bool_t)0)
; } } 
if(BgL_test5798z00_12492)
{ /* Llib/unicode.scm 1178 */
 bool_t BgL_test5801z00_12509;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5294;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12510;
{ /* Llib/unicode.scm 1178 */
 long BgL_i4181z00_7073;
BgL_i4181z00_7073 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1178 */
 long BgL_l4182z00_7074;
BgL_l4182z00_7074 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4181z00_7073, BgL_l4182z00_7074))
{ /* Llib/unicode.scm 1178 */
BgL_tmpz00_12510 = 
STRING_REF(BgL_strz00_78, BgL_i4181z00_7073)
; }  else 
{ 
 obj_t BgL_auxz00_12516;
BgL_auxz00_12516 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47183L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4182z00_7074), 
(int)(BgL_i4181z00_7073)); 
FAILURE(BgL_auxz00_12516,BFALSE,BFALSE);} } } 
BgL_nz00_5294 = 
(BgL_tmpz00_12510); } 
if(
(BgL_nz00_5294>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5801z00_12509 = 
(BgL_nz00_5294<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5801z00_12509 = ((bool_t)0)
; } } 
if(BgL_test5801z00_12509)
{ /* Llib/unicode.scm 1179 */
 bool_t BgL_test5804z00_12526;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5302;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12527;
{ /* Llib/unicode.scm 1179 */
 long BgL_i4185z00_7077;
BgL_i4185z00_7077 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1179 */
 long BgL_l4186z00_7078;
BgL_l4186z00_7078 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4185z00_7077, BgL_l4186z00_7078))
{ /* Llib/unicode.scm 1179 */
BgL_tmpz00_12527 = 
STRING_REF(BgL_strz00_78, BgL_i4185z00_7077)
; }  else 
{ 
 obj_t BgL_auxz00_12533;
BgL_auxz00_12533 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47237L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4186z00_7078), 
(int)(BgL_i4185z00_7077)); 
FAILURE(BgL_auxz00_12533,BFALSE,BFALSE);} } } 
BgL_nz00_5302 = 
(BgL_tmpz00_12527); } 
if(
(BgL_nz00_5302>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5804z00_12526 = 
(BgL_nz00_5302<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5804z00_12526 = ((bool_t)0)
; } } 
if(BgL_test5804z00_12526)
{ /* Llib/unicode.scm 1180 */
 bool_t BgL_test5807z00_12543;
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5310;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12544;
{ /* Llib/unicode.scm 1180 */
 long BgL_i4189z00_7081;
BgL_i4189z00_7081 = 
(BgL_rz00_2181+4L); 
{ /* Llib/unicode.scm 1180 */
 long BgL_l4190z00_7082;
BgL_l4190z00_7082 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4189z00_7081, BgL_l4190z00_7082))
{ /* Llib/unicode.scm 1180 */
BgL_tmpz00_12544 = 
STRING_REF(BgL_strz00_78, BgL_i4189z00_7081)
; }  else 
{ 
 obj_t BgL_auxz00_12550;
BgL_auxz00_12550 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47291L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4190z00_7082), 
(int)(BgL_i4189z00_7081)); 
FAILURE(BgL_auxz00_12550,BFALSE,BFALSE);} } } 
BgL_nz00_5310 = 
(BgL_tmpz00_12544); } 
if(
(BgL_nz00_5310>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5807z00_12543 = 
(BgL_nz00_5310<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5807z00_12543 = ((bool_t)0)
; } } 
if(BgL_test5807z00_12543)
{ /* Llib/unicode.scm 950 */
 long BgL_nz00_5318;
{ /* Llib/unicode.scm 950 */
 unsigned char BgL_tmpz00_12560;
{ /* Llib/unicode.scm 1181 */
 long BgL_i4193z00_7085;
BgL_i4193z00_7085 = 
(BgL_rz00_2181+5L); 
{ /* Llib/unicode.scm 1181 */
 long BgL_l4194z00_7086;
BgL_l4194z00_7086 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4193z00_7085, BgL_l4194z00_7086))
{ /* Llib/unicode.scm 1181 */
BgL_tmpz00_12560 = 
STRING_REF(BgL_strz00_78, BgL_i4193z00_7085)
; }  else 
{ 
 obj_t BgL_auxz00_12566;
BgL_auxz00_12566 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47345L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4194z00_7086), 
(int)(BgL_i4193z00_7085)); 
FAILURE(BgL_auxz00_12566,BFALSE,BFALSE);} } } 
BgL_nz00_5318 = 
(BgL_tmpz00_12560); } 
if(
(BgL_nz00_5318>=128L))
{ /* Llib/unicode.scm 951 */
BgL_test5796z00_12488 = 
(BgL_nz00_5318<=191L)
; }  else 
{ /* Llib/unicode.scm 951 */
BgL_test5796z00_12488 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1180 */
BgL_test5796z00_12488 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1179 */
BgL_test5796z00_12488 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1178 */
BgL_test5796z00_12488 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1177 */
BgL_test5796z00_12488 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1176 */
BgL_test5796z00_12488 = ((bool_t)0)
; } 
if(BgL_test5796z00_12488)
{ /* Llib/unicode.scm 1176 */
{ /* Llib/unicode.scm 1183 */
 long BgL_l4198z00_7090;
BgL_l4198z00_7090 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_wz00_2182, BgL_l4198z00_7090))
{ /* Llib/unicode.scm 1183 */
STRING_SET(BgL_resz00_2179, BgL_wz00_2182, BgL_cz00_2188); }  else 
{ 
 obj_t BgL_auxz00_12580;
BgL_auxz00_12580 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47408L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4198z00_7090), 
(int)(BgL_wz00_2182)); 
FAILURE(BgL_auxz00_12580,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1184 */
 long BgL_arg2488z00_2605; unsigned char BgL_arg2490z00_2606;
BgL_arg2488z00_2605 = 
(BgL_wz00_2182+1L); 
{ /* Llib/unicode.scm 1184 */
 long BgL_i4201z00_7093;
BgL_i4201z00_7093 = 
(BgL_rz00_2181+1L); 
{ /* Llib/unicode.scm 1184 */
 long BgL_l4202z00_7094;
BgL_l4202z00_7094 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4201z00_7093, BgL_l4202z00_7094))
{ /* Llib/unicode.scm 1184 */
BgL_arg2490z00_2606 = 
STRING_REF(BgL_strz00_78, BgL_i4201z00_7093); }  else 
{ 
 obj_t BgL_auxz00_12592;
BgL_auxz00_12592 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47467L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4202z00_7094), 
(int)(BgL_i4201z00_7093)); 
FAILURE(BgL_auxz00_12592,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1184 */
 long BgL_l4206z00_7098;
BgL_l4206z00_7098 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2488z00_2605, BgL_l4206z00_7098))
{ /* Llib/unicode.scm 1184 */
STRING_SET(BgL_resz00_2179, BgL_arg2488z00_2605, BgL_arg2490z00_2606); }  else 
{ 
 obj_t BgL_auxz00_12602;
BgL_auxz00_12602 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47440L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4206z00_7098), 
(int)(BgL_arg2488z00_2605)); 
FAILURE(BgL_auxz00_12602,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1185 */
 long BgL_arg2492z00_2608; unsigned char BgL_arg2493z00_2609;
BgL_arg2492z00_2608 = 
(BgL_wz00_2182+2L); 
{ /* Llib/unicode.scm 1185 */
 long BgL_i4209z00_7101;
BgL_i4209z00_7101 = 
(BgL_rz00_2181+2L); 
{ /* Llib/unicode.scm 1185 */
 long BgL_l4210z00_7102;
BgL_l4210z00_7102 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4209z00_7101, BgL_l4210z00_7102))
{ /* Llib/unicode.scm 1185 */
BgL_arg2493z00_2609 = 
STRING_REF(BgL_strz00_78, BgL_i4209z00_7101); }  else 
{ 
 obj_t BgL_auxz00_12614;
BgL_auxz00_12614 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47532L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4210z00_7102), 
(int)(BgL_i4209z00_7101)); 
FAILURE(BgL_auxz00_12614,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1185 */
 long BgL_l4214z00_7106;
BgL_l4214z00_7106 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2492z00_2608, BgL_l4214z00_7106))
{ /* Llib/unicode.scm 1185 */
STRING_SET(BgL_resz00_2179, BgL_arg2492z00_2608, BgL_arg2493z00_2609); }  else 
{ 
 obj_t BgL_auxz00_12624;
BgL_auxz00_12624 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47505L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4214z00_7106), 
(int)(BgL_arg2492z00_2608)); 
FAILURE(BgL_auxz00_12624,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1186 */
 long BgL_arg2497z00_2611; unsigned char BgL_arg2500z00_2612;
BgL_arg2497z00_2611 = 
(BgL_wz00_2182+3L); 
{ /* Llib/unicode.scm 1186 */
 long BgL_i4217z00_7109;
BgL_i4217z00_7109 = 
(BgL_rz00_2181+3L); 
{ /* Llib/unicode.scm 1186 */
 long BgL_l4218z00_7110;
BgL_l4218z00_7110 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4217z00_7109, BgL_l4218z00_7110))
{ /* Llib/unicode.scm 1186 */
BgL_arg2500z00_2612 = 
STRING_REF(BgL_strz00_78, BgL_i4217z00_7109); }  else 
{ 
 obj_t BgL_auxz00_12636;
BgL_auxz00_12636 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47597L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4218z00_7110), 
(int)(BgL_i4217z00_7109)); 
FAILURE(BgL_auxz00_12636,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1186 */
 long BgL_l4222z00_7114;
BgL_l4222z00_7114 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2497z00_2611, BgL_l4222z00_7114))
{ /* Llib/unicode.scm 1186 */
STRING_SET(BgL_resz00_2179, BgL_arg2497z00_2611, BgL_arg2500z00_2612); }  else 
{ 
 obj_t BgL_auxz00_12646;
BgL_auxz00_12646 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47570L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4222z00_7114), 
(int)(BgL_arg2497z00_2611)); 
FAILURE(BgL_auxz00_12646,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1187 */
 long BgL_arg2502z00_2614; unsigned char BgL_arg2503z00_2615;
BgL_arg2502z00_2614 = 
(BgL_wz00_2182+4L); 
{ /* Llib/unicode.scm 1187 */
 long BgL_i4225z00_7117;
BgL_i4225z00_7117 = 
(BgL_rz00_2181+4L); 
{ /* Llib/unicode.scm 1187 */
 long BgL_l4226z00_7118;
BgL_l4226z00_7118 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4225z00_7117, BgL_l4226z00_7118))
{ /* Llib/unicode.scm 1187 */
BgL_arg2503z00_2615 = 
STRING_REF(BgL_strz00_78, BgL_i4225z00_7117); }  else 
{ 
 obj_t BgL_auxz00_12658;
BgL_auxz00_12658 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47662L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4226z00_7118), 
(int)(BgL_i4225z00_7117)); 
FAILURE(BgL_auxz00_12658,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1187 */
 long BgL_l4230z00_7122;
BgL_l4230z00_7122 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2502z00_2614, BgL_l4230z00_7122))
{ /* Llib/unicode.scm 1187 */
STRING_SET(BgL_resz00_2179, BgL_arg2502z00_2614, BgL_arg2503z00_2615); }  else 
{ 
 obj_t BgL_auxz00_12668;
BgL_auxz00_12668 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47635L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4230z00_7122), 
(int)(BgL_arg2502z00_2614)); 
FAILURE(BgL_auxz00_12668,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1188 */
 long BgL_arg2506z00_2617; unsigned char BgL_arg2508z00_2618;
BgL_arg2506z00_2617 = 
(BgL_wz00_2182+5L); 
{ /* Llib/unicode.scm 1188 */
 long BgL_i4233z00_7125;
BgL_i4233z00_7125 = 
(BgL_rz00_2181+5L); 
{ /* Llib/unicode.scm 1188 */
 long BgL_l4234z00_7126;
BgL_l4234z00_7126 = 
STRING_LENGTH(BgL_strz00_78); 
if(
BOUND_CHECK(BgL_i4233z00_7125, BgL_l4234z00_7126))
{ /* Llib/unicode.scm 1188 */
BgL_arg2508z00_2618 = 
STRING_REF(BgL_strz00_78, BgL_i4233z00_7125); }  else 
{ 
 obj_t BgL_auxz00_12680;
BgL_auxz00_12680 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47727L), BGl_string4897z00zz__unicodez00, BgL_strz00_78, 
(int)(BgL_l4234z00_7126), 
(int)(BgL_i4233z00_7125)); 
FAILURE(BgL_auxz00_12680,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1188 */
 long BgL_l4238z00_7130;
BgL_l4238z00_7130 = 
STRING_LENGTH(BgL_resz00_2179); 
if(
BOUND_CHECK(BgL_arg2506z00_2617, BgL_l4238z00_7130))
{ /* Llib/unicode.scm 1188 */
STRING_SET(BgL_resz00_2179, BgL_arg2506z00_2617, BgL_arg2508z00_2618); }  else 
{ 
 obj_t BgL_auxz00_12690;
BgL_auxz00_12690 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(47700L), BGl_string4915z00zz__unicodez00, BgL_resz00_2179, 
(int)(BgL_l4238z00_7130), 
(int)(BgL_arg2506z00_2617)); 
FAILURE(BgL_auxz00_12690,BFALSE,BFALSE);} } } 
{ 
 bool_t BgL_asciiz00_12700; long BgL_wz00_12698; long BgL_rz00_12696;
BgL_rz00_12696 = 
(BgL_rz00_2181+6L); 
BgL_wz00_12698 = 
(BgL_wz00_2182+6L); 
BgL_asciiz00_12700 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12700; 
BgL_wz00_2182 = BgL_wz00_12698; 
BgL_rz00_2181 = BgL_rz00_12696; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} }  else 
{ /* Llib/unicode.scm 1176 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_12706; long BgL_wz00_12704; long BgL_rz00_12702;
BgL_rz00_12702 = 
(BgL_rz00_2181+1L); 
BgL_wz00_12704 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_12706 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12706; 
BgL_wz00_2182 = BgL_wz00_12704; 
BgL_rz00_2181 = BgL_rz00_12702; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } }  else 
{ /* Llib/unicode.scm 1175 */
BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(BgL_resz00_2179, BgL_wz00_2182); 
{ 
 bool_t BgL_asciiz00_12712; long BgL_wz00_12710; long BgL_rz00_12708;
BgL_rz00_12708 = 
(BgL_rz00_2181+1L); 
BgL_wz00_12710 = 
(BgL_wz00_2182+3L); 
BgL_asciiz00_12712 = ((bool_t)0); 
BgL_asciiz00_2183 = BgL_asciiz00_12712; 
BgL_wz00_2182 = BgL_wz00_12710; 
BgL_rz00_2181 = BgL_rz00_12708; 
goto BgL_zc3z04anonymousza32077ze3z87_2184;} } } } } } } } } } } } } } } } } } } } } 

}



/* string-unicode-fix!~0 */
obj_t BGl_stringzd2unicodezd2fixz12ze70zf5zz__unicodez00(obj_t BgL_sz00_2658, long BgL_jz00_2659)
{
{ /* Llib/unicode.scm 955 */
{ /* Llib/unicode.scm 955 */
 long BgL_l4242z00_7134;
BgL_l4242z00_7134 = 
STRING_LENGTH(BgL_sz00_2658); 
if(
BOUND_CHECK(BgL_jz00_2659, BgL_l4242z00_7134))
{ /* Llib/unicode.scm 955 */
STRING_SET(BgL_sz00_2658, BgL_jz00_2659, ((unsigned char)239)); }  else 
{ 
 obj_t BgL_auxz00_12717;
BgL_auxz00_12717 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38759L), BGl_string4915z00zz__unicodez00, BgL_sz00_2658, 
(int)(BgL_l4242z00_7134), 
(int)(BgL_jz00_2659)); 
FAILURE(BgL_auxz00_12717,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 956 */
 long BgL_arg2540z00_2661;
BgL_arg2540z00_2661 = 
(BgL_jz00_2659+1L); 
{ /* Llib/unicode.scm 956 */
 long BgL_l4246z00_7138;
BgL_l4246z00_7138 = 
STRING_LENGTH(BgL_sz00_2658); 
if(
BOUND_CHECK(BgL_arg2540z00_2661, BgL_l4246z00_7138))
{ /* Llib/unicode.scm 956 */
STRING_SET(BgL_sz00_2658, BgL_arg2540z00_2661, ((unsigned char)191)); }  else 
{ 
 obj_t BgL_auxz00_12728;
BgL_auxz00_12728 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38789L), BGl_string4915z00zz__unicodez00, BgL_sz00_2658, 
(int)(BgL_l4246z00_7138), 
(int)(BgL_arg2540z00_2661)); 
FAILURE(BgL_auxz00_12728,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 957 */
 long BgL_arg2542z00_2662;
BgL_arg2542z00_2662 = 
(BgL_jz00_2659+2L); 
{ /* Llib/unicode.scm 957 */
 long BgL_l4250z00_7142;
BgL_l4250z00_7142 = 
STRING_LENGTH(BgL_sz00_2658); 
if(
BOUND_CHECK(BgL_arg2542z00_2662, BgL_l4250z00_7142))
{ /* Llib/unicode.scm 957 */
return 
STRING_SET(BgL_sz00_2658, BgL_arg2542z00_2662, ((unsigned char)189));}  else 
{ 
 obj_t BgL_auxz00_12739;
BgL_auxz00_12739 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(38827L), BGl_string4915z00zz__unicodez00, BgL_sz00_2658, 
(int)(BgL_l4250z00_7142), 
(int)(BgL_arg2542z00_2662)); 
FAILURE(BgL_auxz00_12739,BFALSE,BFALSE);} } } } 

}



/* utf8-char-size */
BGL_EXPORTED_DEF long BGl_utf8zd2charzd2siza7eza7zz__unicodez00(unsigned char BgL_cz00_84)
{
{ /* Llib/unicode.scm 1201 */
{ /* Llib/unicode.scm 1203 */
 long BgL_arg2543z00_7700;
BgL_arg2543z00_7700 = 
(
(
(unsigned char)(BgL_cz00_84)) >> 
(int)(4L)); 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_vectorz00_7701;
BgL_vectorz00_7701 = BGl_vector4921z00zz__unicodez00; 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_tmpz00_12749;
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_aux4620z00_7702;
BgL_aux4620z00_7702 = 
VECTOR_REF(BgL_vectorz00_7701,BgL_arg2543z00_7700); 
if(
INTEGERP(BgL_aux4620z00_7702))
{ /* Llib/unicode.scm 1202 */
BgL_tmpz00_12749 = BgL_aux4620z00_7702
; }  else 
{ 
 obj_t BgL_auxz00_12753;
BgL_auxz00_12753 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48257L), BGl_string4923z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_aux4620z00_7702); 
FAILURE(BgL_auxz00_12753,BFALSE,BFALSE);} } 
return 
(long)CINT(BgL_tmpz00_12749);} } } } 

}



/* &utf8-char-size */
obj_t BGl_z62utf8zd2charzd2siza7ezc5zz__unicodez00(obj_t BgL_envz00_6194, obj_t BgL_cz00_6195)
{
{ /* Llib/unicode.scm 1201 */
{ /* Llib/unicode.scm 1203 */
 long BgL_tmpz00_12758;
{ /* Llib/unicode.scm 1203 */
 unsigned char BgL_auxz00_12759;
{ /* Llib/unicode.scm 1203 */
 obj_t BgL_tmpz00_12760;
if(
CHARP(BgL_cz00_6195))
{ /* Llib/unicode.scm 1203 */
BgL_tmpz00_12760 = BgL_cz00_6195
; }  else 
{ 
 obj_t BgL_auxz00_12763;
BgL_auxz00_12763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48323L), BGl_string4924z00zz__unicodez00, BGl_string4925z00zz__unicodez00, BgL_cz00_6195); 
FAILURE(BgL_auxz00_12763,BFALSE,BFALSE);} 
BgL_auxz00_12759 = 
CCHAR(BgL_tmpz00_12760); } 
BgL_tmpz00_12758 = 
BGl_utf8zd2charzd2siza7eza7zz__unicodez00(BgL_auxz00_12759); } 
return 
BINT(BgL_tmpz00_12758);} } 

}



/* utf8-string-length */
BGL_EXPORTED_DEF long BGl_utf8zd2stringzd2lengthz00zz__unicodez00(obj_t BgL_strz00_86)
{
{ /* Llib/unicode.scm 1217 */
{ /* Llib/unicode.scm 1218 */
 long BgL_lenz00_2669;
BgL_lenz00_2669 = 
STRING_LENGTH(BgL_strz00_86); 
{ 
 long BgL_rz00_5390; long BgL_lz00_5391;
BgL_rz00_5390 = 0L; 
BgL_lz00_5391 = 0L; 
BgL_loopz00_5389:
if(
(BgL_rz00_5390==BgL_lenz00_2669))
{ /* Llib/unicode.scm 1221 */
return BgL_lz00_5391;}  else 
{ /* Llib/unicode.scm 1223 */
 long BgL_arg2551z00_5395; long BgL_arg2552z00_5396;
{ /* Llib/unicode.scm 1223 */
 long BgL_arg2553z00_5397;
{ /* Llib/unicode.scm 1223 */
 unsigned char BgL_arg2554z00_5398;
{ /* Llib/unicode.scm 1223 */
 long BgL_l4254z00_7146;
BgL_l4254z00_7146 = 
STRING_LENGTH(BgL_strz00_86); 
if(
BOUND_CHECK(BgL_rz00_5390, BgL_l4254z00_7146))
{ /* Llib/unicode.scm 1223 */
BgL_arg2554z00_5398 = 
STRING_REF(BgL_strz00_86, BgL_rz00_5390); }  else 
{ 
 obj_t BgL_auxz00_12777;
BgL_auxz00_12777 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(49228L), BGl_string4897z00zz__unicodez00, BgL_strz00_86, 
(int)(BgL_l4254z00_7146), 
(int)(BgL_rz00_5390)); 
FAILURE(BgL_auxz00_12777,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1223 */
 long BgL_res3367z00_5401;
{ /* Llib/unicode.scm 1223 */
 unsigned char BgL_cz00_5402;
BgL_cz00_5402 = 
(char)(BgL_arg2554z00_5398); 
{ /* Llib/unicode.scm 1203 */
 long BgL_arg2543z00_5403;
BgL_arg2543z00_5403 = 
(
(
(unsigned char)(BgL_cz00_5402)) >> 
(int)(4L)); 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_vectorz00_5407;
BgL_vectorz00_5407 = BGl_vector4921z00zz__unicodez00; 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_tmpz00_12788;
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_aux4623z00_7515;
BgL_aux4623z00_7515 = 
VECTOR_REF(BgL_vectorz00_5407,BgL_arg2543z00_5403); 
if(
INTEGERP(BgL_aux4623z00_7515))
{ /* Llib/unicode.scm 1202 */
BgL_tmpz00_12788 = BgL_aux4623z00_7515
; }  else 
{ 
 obj_t BgL_auxz00_12792;
BgL_auxz00_12792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48257L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_aux4623z00_7515); 
FAILURE(BgL_auxz00_12792,BFALSE,BFALSE);} } 
BgL_res3367z00_5401 = 
(long)CINT(BgL_tmpz00_12788); } } } } 
BgL_arg2553z00_5397 = BgL_res3367z00_5401; } } 
BgL_arg2551z00_5395 = 
(BgL_rz00_5390+BgL_arg2553z00_5397); } 
BgL_arg2552z00_5396 = 
(BgL_lz00_5391+1L); 
{ 
 long BgL_lz00_12800; long BgL_rz00_12799;
BgL_rz00_12799 = BgL_arg2551z00_5395; 
BgL_lz00_12800 = BgL_arg2552z00_5396; 
BgL_lz00_5391 = BgL_lz00_12800; 
BgL_rz00_5390 = BgL_rz00_12799; 
goto BgL_loopz00_5389;} } } } } 

}



/* &utf8-string-length */
obj_t BGl_z62utf8zd2stringzd2lengthz62zz__unicodez00(obj_t BgL_envz00_6196, obj_t BgL_strz00_6197)
{
{ /* Llib/unicode.scm 1217 */
{ /* Llib/unicode.scm 1218 */
 long BgL_tmpz00_12801;
{ /* Llib/unicode.scm 1218 */
 obj_t BgL_auxz00_12802;
if(
STRINGP(BgL_strz00_6197))
{ /* Llib/unicode.scm 1218 */
BgL_auxz00_12802 = BgL_strz00_6197
; }  else 
{ 
 obj_t BgL_auxz00_12805;
BgL_auxz00_12805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(49101L), BGl_string4926z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6197); 
FAILURE(BgL_auxz00_12805,BFALSE,BFALSE);} 
BgL_tmpz00_12801 = 
BGl_utf8zd2stringzd2lengthz00zz__unicodez00(BgL_auxz00_12802); } 
return 
BINT(BgL_tmpz00_12801);} } 

}



/* utf8-string-ref */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2refz00zz__unicodez00(obj_t BgL_strz00_87, long BgL_iz00_88)
{
{ /* Llib/unicode.scm 1228 */
{ 
 long BgL_rz00_2682; long BgL_iz00_2683;
BgL_rz00_2682 = 0L; 
BgL_iz00_2683 = BgL_iz00_88; 
BgL_zc3z04anonymousza32555ze3z87_2684:
{ /* Llib/unicode.scm 1231 */
 unsigned char BgL_cz00_2685;
{ /* Llib/unicode.scm 1231 */
 long BgL_l4258z00_7150;
BgL_l4258z00_7150 = 
STRING_LENGTH(BgL_strz00_87); 
if(
BOUND_CHECK(BgL_rz00_2682, BgL_l4258z00_7150))
{ /* Llib/unicode.scm 1231 */
BgL_cz00_2685 = 
STRING_REF(BgL_strz00_87, BgL_rz00_2682); }  else 
{ 
 obj_t BgL_auxz00_12815;
BgL_auxz00_12815 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(49597L), BGl_string4897z00zz__unicodez00, BgL_strz00_87, 
(int)(BgL_l4258z00_7150), 
(int)(BgL_rz00_2682)); 
FAILURE(BgL_auxz00_12815,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1231 */
 long BgL_sz00_2686;
{ /* Llib/unicode.scm 1232 */
 long BgL_res3368z00_5422;
{ /* Llib/unicode.scm 1232 */
 unsigned char BgL_cz00_5415;
BgL_cz00_5415 = 
(char)(BgL_cz00_2685); 
{ /* Llib/unicode.scm 1203 */
 long BgL_arg2543z00_5416;
BgL_arg2543z00_5416 = 
(
(
(unsigned char)(BgL_cz00_5415)) >> 
(int)(4L)); 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_vectorz00_5420;
BgL_vectorz00_5420 = BGl_vector4921z00zz__unicodez00; 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_tmpz00_12826;
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_aux4626z00_7518;
BgL_aux4626z00_7518 = 
VECTOR_REF(BgL_vectorz00_5420,BgL_arg2543z00_5416); 
if(
INTEGERP(BgL_aux4626z00_7518))
{ /* Llib/unicode.scm 1202 */
BgL_tmpz00_12826 = BgL_aux4626z00_7518
; }  else 
{ 
 obj_t BgL_auxz00_12830;
BgL_auxz00_12830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48257L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_aux4626z00_7518); 
FAILURE(BgL_auxz00_12830,BFALSE,BFALSE);} } 
BgL_res3368z00_5422 = 
(long)CINT(BgL_tmpz00_12826); } } } } 
BgL_sz00_2686 = BgL_res3368z00_5422; } 
{ /* Llib/unicode.scm 1232 */

if(
(BgL_iz00_2683==0L))
{ /* Llib/unicode.scm 1233 */
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_87, BgL_rz00_2682, 
(BgL_rz00_2682+BgL_sz00_2686));}  else 
{ 
 long BgL_iz00_12841; long BgL_rz00_12839;
BgL_rz00_12839 = 
(BgL_rz00_2682+BgL_sz00_2686); 
BgL_iz00_12841 = 
(BgL_iz00_2683-1L); 
BgL_iz00_2683 = BgL_iz00_12841; 
BgL_rz00_2682 = BgL_rz00_12839; 
goto BgL_zc3z04anonymousza32555ze3z87_2684;} } } } } } 

}



/* &utf8-string-ref */
obj_t BGl_z62utf8zd2stringzd2refz62zz__unicodez00(obj_t BgL_envz00_6198, obj_t BgL_strz00_6199, obj_t BgL_iz00_6200)
{
{ /* Llib/unicode.scm 1228 */
{ /* Llib/unicode.scm 1231 */
 long BgL_auxz00_12850; obj_t BgL_auxz00_12843;
{ /* Llib/unicode.scm 1231 */
 obj_t BgL_tmpz00_12851;
if(
INTEGERP(BgL_iz00_6200))
{ /* Llib/unicode.scm 1231 */
BgL_tmpz00_12851 = BgL_iz00_6200
; }  else 
{ 
 obj_t BgL_auxz00_12854;
BgL_auxz00_12854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(49587L), BGl_string4927z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_iz00_6200); 
FAILURE(BgL_auxz00_12854,BFALSE,BFALSE);} 
BgL_auxz00_12850 = 
(long)CINT(BgL_tmpz00_12851); } 
if(
STRINGP(BgL_strz00_6199))
{ /* Llib/unicode.scm 1231 */
BgL_auxz00_12843 = BgL_strz00_6199
; }  else 
{ 
 obj_t BgL_auxz00_12846;
BgL_auxz00_12846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(49587L), BGl_string4927z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6199); 
FAILURE(BgL_auxz00_12846,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2refz00zz__unicodez00(BgL_auxz00_12843, BgL_auxz00_12850);} } 

}



/* utf8-string-index->string-index */
BGL_EXPORTED_DEF long BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexze3zz__unicodez00(obj_t BgL_strz00_89, long BgL_iz00_90)
{
{ /* Llib/unicode.scm 1240 */
if(
(BgL_iz00_90<0L))
{ /* Llib/unicode.scm 1242 */
return -1L;}  else 
{ /* Llib/unicode.scm 1245 */
 long BgL_lenz00_2693;
BgL_lenz00_2693 = 
STRING_LENGTH(BgL_strz00_89); 
{ 
 long BgL_rz00_2695; long BgL_iz00_2696;
BgL_rz00_2695 = 0L; 
BgL_iz00_2696 = BgL_iz00_90; 
BgL_zc3z04anonymousza32563ze3z87_2697:
if(
(BgL_iz00_2696==0L))
{ /* Llib/unicode.scm 1248 */
return BgL_rz00_2695;}  else 
{ /* Llib/unicode.scm 1248 */
if(
(BgL_rz00_2695<BgL_lenz00_2693))
{ /* Llib/unicode.scm 1252 */
 long BgL_arg2566z00_2701; long BgL_arg2567z00_2702;
{ /* Llib/unicode.scm 1252 */
 long BgL_arg2568z00_2703;
{ /* Llib/unicode.scm 1252 */
 long BgL_res3369z00_5443;
{ /* Llib/unicode.scm 1252 */
 unsigned char BgL_cz00_5436;
{ /* Llib/unicode.scm 1251 */
 long BgL_l4262z00_7154;
BgL_l4262z00_7154 = 
STRING_LENGTH(BgL_strz00_89); 
if(
BOUND_CHECK(BgL_rz00_2695, BgL_l4262z00_7154))
{ /* Llib/unicode.scm 1251 */
BgL_cz00_5436 = 
(char)(
STRING_REF(BgL_strz00_89, BgL_rz00_2695)); }  else 
{ 
 obj_t BgL_auxz00_12872;
BgL_auxz00_12872 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(50171L), BGl_string4897z00zz__unicodez00, BgL_strz00_89, 
(int)(BgL_l4262z00_7154), 
(int)(BgL_rz00_2695)); 
FAILURE(BgL_auxz00_12872,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1203 */
 long BgL_arg2543z00_5437;
BgL_arg2543z00_5437 = 
(
(
(unsigned char)(BgL_cz00_5436)) >> 
(int)(4L)); 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_vectorz00_5441;
BgL_vectorz00_5441 = BGl_vector4921z00zz__unicodez00; 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_tmpz00_12882;
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_aux4630z00_7522;
BgL_aux4630z00_7522 = 
VECTOR_REF(BgL_vectorz00_5441,BgL_arg2543z00_5437); 
if(
INTEGERP(BgL_aux4630z00_7522))
{ /* Llib/unicode.scm 1202 */
BgL_tmpz00_12882 = BgL_aux4630z00_7522
; }  else 
{ 
 obj_t BgL_auxz00_12886;
BgL_auxz00_12886 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48257L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_aux4630z00_7522); 
FAILURE(BgL_auxz00_12886,BFALSE,BFALSE);} } 
BgL_res3369z00_5443 = 
(long)CINT(BgL_tmpz00_12882); } } } } 
BgL_arg2568z00_2703 = BgL_res3369z00_5443; } 
BgL_arg2566z00_2701 = 
(BgL_rz00_2695+BgL_arg2568z00_2703); } 
BgL_arg2567z00_2702 = 
(BgL_iz00_2696-1L); 
{ 
 long BgL_iz00_12894; long BgL_rz00_12893;
BgL_rz00_12893 = BgL_arg2566z00_2701; 
BgL_iz00_12894 = BgL_arg2567z00_2702; 
BgL_iz00_2696 = BgL_iz00_12894; 
BgL_rz00_2695 = BgL_rz00_12893; 
goto BgL_zc3z04anonymousza32563ze3z87_2697;} }  else 
{ /* Llib/unicode.scm 1250 */
return -1L;} } } } } 

}



/* &utf8-string-index->string-index */
obj_t BGl_z62utf8zd2stringzd2indexzd2ze3stringzd2indexz81zz__unicodez00(obj_t BgL_envz00_6201, obj_t BgL_strz00_6202, obj_t BgL_iz00_6203)
{
{ /* Llib/unicode.scm 1240 */
{ /* Llib/unicode.scm 1242 */
 long BgL_tmpz00_12895;
{ /* Llib/unicode.scm 1242 */
 long BgL_auxz00_12903; obj_t BgL_auxz00_12896;
{ /* Llib/unicode.scm 1242 */
 obj_t BgL_tmpz00_12904;
if(
INTEGERP(BgL_iz00_6203))
{ /* Llib/unicode.scm 1242 */
BgL_tmpz00_12904 = BgL_iz00_6203
; }  else 
{ 
 obj_t BgL_auxz00_12907;
BgL_auxz00_12907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(50012L), BGl_string4928z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_iz00_6203); 
FAILURE(BgL_auxz00_12907,BFALSE,BFALSE);} 
BgL_auxz00_12903 = 
(long)CINT(BgL_tmpz00_12904); } 
if(
STRINGP(BgL_strz00_6202))
{ /* Llib/unicode.scm 1242 */
BgL_auxz00_12896 = BgL_strz00_6202
; }  else 
{ 
 obj_t BgL_auxz00_12899;
BgL_auxz00_12899 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(50012L), BGl_string4928z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6202); 
FAILURE(BgL_auxz00_12899,BFALSE,BFALSE);} 
BgL_tmpz00_12895 = 
BGl_utf8zd2stringzd2indexzd2ze3stringzd2indexze3zz__unicodez00(BgL_auxz00_12896, BgL_auxz00_12903); } 
return 
BINT(BgL_tmpz00_12895);} } 

}



/* string-index->utf8-string-index */
BGL_EXPORTED_DEF long BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexze3zz__unicodez00(obj_t BgL_strz00_91, long BgL_iz00_92)
{
{ /* Llib/unicode.scm 1259 */
if(
(BgL_iz00_92<0L))
{ /* Llib/unicode.scm 1261 */
return -1L;}  else 
{ /* Llib/unicode.scm 1264 */
 long BgL_lenz00_2706;
BgL_lenz00_2706 = 
STRING_LENGTH(BgL_strz00_91); 
{ 
 long BgL_mz00_2708; long BgL_jz00_2709; long BgL_iz00_2710;
BgL_mz00_2708 = 0L; 
BgL_jz00_2709 = BgL_iz00_92; 
BgL_iz00_2710 = BgL_iz00_92; 
BgL_zc3z04anonymousza32570ze3z87_2711:
if(
(BgL_iz00_2710<=0L))
{ /* Llib/unicode.scm 1269 */
return BgL_jz00_2709;}  else 
{ /* Llib/unicode.scm 1269 */
if(
(BgL_mz00_2708>=BgL_lenz00_2706))
{ /* Llib/unicode.scm 1271 */
return -1L;}  else 
{ /* Llib/unicode.scm 1274 */
 long BgL_sz00_2715;
{ /* Llib/unicode.scm 1275 */
 long BgL_res3370z00_5461;
{ /* Llib/unicode.scm 1275 */
 unsigned char BgL_cz00_5454;
{ /* Llib/unicode.scm 1274 */
 long BgL_l4266z00_7158;
BgL_l4266z00_7158 = 
STRING_LENGTH(BgL_strz00_91); 
if(
BOUND_CHECK(BgL_mz00_2708, BgL_l4266z00_7158))
{ /* Llib/unicode.scm 1274 */
BgL_cz00_5454 = 
(char)(
STRING_REF(BgL_strz00_91, BgL_mz00_2708)); }  else 
{ 
 obj_t BgL_auxz00_12926;
BgL_auxz00_12926 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(50748L), BGl_string4897z00zz__unicodez00, BgL_strz00_91, 
(int)(BgL_l4266z00_7158), 
(int)(BgL_mz00_2708)); 
FAILURE(BgL_auxz00_12926,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1203 */
 long BgL_arg2543z00_5455;
BgL_arg2543z00_5455 = 
(
(
(unsigned char)(BgL_cz00_5454)) >> 
(int)(4L)); 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_vectorz00_5459;
BgL_vectorz00_5459 = BGl_vector4921z00zz__unicodez00; 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_tmpz00_12936;
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_aux4634z00_7526;
BgL_aux4634z00_7526 = 
VECTOR_REF(BgL_vectorz00_5459,BgL_arg2543z00_5455); 
if(
INTEGERP(BgL_aux4634z00_7526))
{ /* Llib/unicode.scm 1202 */
BgL_tmpz00_12936 = BgL_aux4634z00_7526
; }  else 
{ 
 obj_t BgL_auxz00_12940;
BgL_auxz00_12940 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48257L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_aux4634z00_7526); 
FAILURE(BgL_auxz00_12940,BFALSE,BFALSE);} } 
BgL_res3370z00_5461 = 
(long)CINT(BgL_tmpz00_12936); } } } } 
BgL_sz00_2715 = BgL_res3370z00_5461; } 
{ /* Llib/unicode.scm 1275 */

{ 
 long BgL_iz00_12950; long BgL_jz00_12947; long BgL_mz00_12945;
BgL_mz00_12945 = 
(BgL_mz00_2708+BgL_sz00_2715); 
BgL_jz00_12947 = 
(BgL_jz00_2709-
(BgL_sz00_2715-1L)); 
BgL_iz00_12950 = 
(BgL_iz00_2710-BgL_sz00_2715); 
BgL_iz00_2710 = BgL_iz00_12950; 
BgL_jz00_2709 = BgL_jz00_12947; 
BgL_mz00_2708 = BgL_mz00_12945; 
goto BgL_zc3z04anonymousza32570ze3z87_2711;} } } } } } } 

}



/* &string-index->utf8-string-index */
obj_t BGl_z62stringzd2indexzd2ze3utf8zd2stringzd2indexz81zz__unicodez00(obj_t BgL_envz00_6204, obj_t BgL_strz00_6205, obj_t BgL_iz00_6206)
{
{ /* Llib/unicode.scm 1259 */
{ /* Llib/unicode.scm 1261 */
 long BgL_tmpz00_12952;
{ /* Llib/unicode.scm 1261 */
 long BgL_auxz00_12960; obj_t BgL_auxz00_12953;
{ /* Llib/unicode.scm 1261 */
 obj_t BgL_tmpz00_12961;
if(
INTEGERP(BgL_iz00_6206))
{ /* Llib/unicode.scm 1261 */
BgL_tmpz00_12961 = BgL_iz00_6206
; }  else 
{ 
 obj_t BgL_auxz00_12964;
BgL_auxz00_12964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(50551L), BGl_string4929z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_iz00_6206); 
FAILURE(BgL_auxz00_12964,BFALSE,BFALSE);} 
BgL_auxz00_12960 = 
(long)CINT(BgL_tmpz00_12961); } 
if(
STRINGP(BgL_strz00_6205))
{ /* Llib/unicode.scm 1261 */
BgL_auxz00_12953 = BgL_strz00_6205
; }  else 
{ 
 obj_t BgL_auxz00_12956;
BgL_auxz00_12956 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(50551L), BGl_string4929z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6205); 
FAILURE(BgL_auxz00_12956,BFALSE,BFALSE);} 
BgL_tmpz00_12952 = 
BGl_stringzd2indexzd2ze3utf8zd2stringzd2indexze3zz__unicodez00(BgL_auxz00_12953, BgL_auxz00_12960); } 
return 
BINT(BgL_tmpz00_12952);} } 

}



/* utf8-string-left-replacement? */
BGL_EXPORTED_DEF bool_t BGl_utf8zd2stringzd2leftzd2replacementzf3z21zz__unicodez00(obj_t BgL_strz00_93, long BgL_lenz00_94, long BgL_indexz00_95)
{
{ /* Llib/unicode.scm 1284 */
if(
(BgL_lenz00_94>=
(BgL_indexz00_95+4L)))
{ /* Llib/unicode.scm 1286 */
 long BgL_tmpz00_12974;
{ /* Llib/unicode.scm 1286 */
 unsigned char BgL_tmpz00_12975;
{ /* Llib/unicode.scm 1286 */
 long BgL_l4270z00_7703;
BgL_l4270z00_7703 = 
STRING_LENGTH(BgL_strz00_93); 
if(
BOUND_CHECK(BgL_indexz00_95, BgL_l4270z00_7703))
{ /* Llib/unicode.scm 1286 */
BgL_tmpz00_12975 = 
STRING_REF(BgL_strz00_93, BgL_indexz00_95)
; }  else 
{ 
 obj_t BgL_auxz00_12980;
BgL_auxz00_12980 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51414L), BGl_string4897z00zz__unicodez00, BgL_strz00_93, 
(int)(BgL_l4270z00_7703), 
(int)(BgL_indexz00_95)); 
FAILURE(BgL_auxz00_12980,BFALSE,BFALSE);} } 
BgL_tmpz00_12974 = 
(BgL_tmpz00_12975); } 
return 
(BgL_tmpz00_12974==248L);}  else 
{ /* Llib/unicode.scm 1285 */
return ((bool_t)0);} } 

}



/* &utf8-string-left-replacement? */
obj_t BGl_z62utf8zd2stringzd2leftzd2replacementzf3z43zz__unicodez00(obj_t BgL_envz00_6207, obj_t BgL_strz00_6208, obj_t BgL_lenz00_6209, obj_t BgL_indexz00_6210)
{
{ /* Llib/unicode.scm 1284 */
{ /* Llib/unicode.scm 1285 */
 bool_t BgL_tmpz00_12988;
{ /* Llib/unicode.scm 1285 */
 long BgL_auxz00_13005; long BgL_auxz00_12996; obj_t BgL_auxz00_12989;
{ /* Llib/unicode.scm 1285 */
 obj_t BgL_tmpz00_13006;
if(
INTEGERP(BgL_indexz00_6210))
{ /* Llib/unicode.scm 1285 */
BgL_tmpz00_13006 = BgL_indexz00_6210
; }  else 
{ 
 obj_t BgL_auxz00_13009;
BgL_auxz00_13009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51368L), BGl_string4930z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_indexz00_6210); 
FAILURE(BgL_auxz00_13009,BFALSE,BFALSE);} 
BgL_auxz00_13005 = 
(long)CINT(BgL_tmpz00_13006); } 
{ /* Llib/unicode.scm 1285 */
 obj_t BgL_tmpz00_12997;
if(
INTEGERP(BgL_lenz00_6209))
{ /* Llib/unicode.scm 1285 */
BgL_tmpz00_12997 = BgL_lenz00_6209
; }  else 
{ 
 obj_t BgL_auxz00_13000;
BgL_auxz00_13000 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51368L), BGl_string4930z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_lenz00_6209); 
FAILURE(BgL_auxz00_13000,BFALSE,BFALSE);} 
BgL_auxz00_12996 = 
(long)CINT(BgL_tmpz00_12997); } 
if(
STRINGP(BgL_strz00_6208))
{ /* Llib/unicode.scm 1285 */
BgL_auxz00_12989 = BgL_strz00_6208
; }  else 
{ 
 obj_t BgL_auxz00_12992;
BgL_auxz00_12992 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51368L), BGl_string4930z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6208); 
FAILURE(BgL_auxz00_12992,BFALSE,BFALSE);} 
BgL_tmpz00_12988 = 
BGl_utf8zd2stringzd2leftzd2replacementzf3z21zz__unicodez00(BgL_auxz00_12989, BgL_auxz00_12996, BgL_auxz00_13005); } 
return 
BBOOL(BgL_tmpz00_12988);} } 

}



/* utf8-string-right-replacement? */
BGL_EXPORTED_DEF bool_t BGl_utf8zd2stringzd2rightzd2replacementzf3z21zz__unicodez00(obj_t BgL_strz00_96, long BgL_lenz00_97, long BgL_indexz00_98)
{
{ /* Llib/unicode.scm 1294 */
if(
(BgL_lenz00_97>=
(BgL_indexz00_98+4L)))
{ /* Llib/unicode.scm 1296 */
 long BgL_tmpz00_13019;
{ /* Llib/unicode.scm 1296 */
 unsigned char BgL_tmpz00_13020;
{ /* Llib/unicode.scm 1296 */
 long BgL_l4274z00_7704;
BgL_l4274z00_7704 = 
STRING_LENGTH(BgL_strz00_96); 
if(
BOUND_CHECK(BgL_indexz00_98, BgL_l4274z00_7704))
{ /* Llib/unicode.scm 1296 */
BgL_tmpz00_13020 = 
STRING_REF(BgL_strz00_96, BgL_indexz00_98)
; }  else 
{ 
 obj_t BgL_auxz00_13025;
BgL_auxz00_13025 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52010L), BGl_string4897z00zz__unicodez00, BgL_strz00_96, 
(int)(BgL_l4274z00_7704), 
(int)(BgL_indexz00_98)); 
FAILURE(BgL_auxz00_13025,BFALSE,BFALSE);} } 
BgL_tmpz00_13019 = 
(BgL_tmpz00_13020); } 
return 
(BgL_tmpz00_13019==252L);}  else 
{ /* Llib/unicode.scm 1295 */
return ((bool_t)0);} } 

}



/* &utf8-string-right-replacement? */
obj_t BGl_z62utf8zd2stringzd2rightzd2replacementzf3z43zz__unicodez00(obj_t BgL_envz00_6211, obj_t BgL_strz00_6212, obj_t BgL_lenz00_6213, obj_t BgL_indexz00_6214)
{
{ /* Llib/unicode.scm 1294 */
{ /* Llib/unicode.scm 1295 */
 bool_t BgL_tmpz00_13033;
{ /* Llib/unicode.scm 1295 */
 long BgL_auxz00_13050; long BgL_auxz00_13041; obj_t BgL_auxz00_13034;
{ /* Llib/unicode.scm 1295 */
 obj_t BgL_tmpz00_13051;
if(
INTEGERP(BgL_indexz00_6214))
{ /* Llib/unicode.scm 1295 */
BgL_tmpz00_13051 = BgL_indexz00_6214
; }  else 
{ 
 obj_t BgL_auxz00_13054;
BgL_auxz00_13054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51964L), BGl_string4931z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_indexz00_6214); 
FAILURE(BgL_auxz00_13054,BFALSE,BFALSE);} 
BgL_auxz00_13050 = 
(long)CINT(BgL_tmpz00_13051); } 
{ /* Llib/unicode.scm 1295 */
 obj_t BgL_tmpz00_13042;
if(
INTEGERP(BgL_lenz00_6213))
{ /* Llib/unicode.scm 1295 */
BgL_tmpz00_13042 = BgL_lenz00_6213
; }  else 
{ 
 obj_t BgL_auxz00_13045;
BgL_auxz00_13045 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51964L), BGl_string4931z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_lenz00_6213); 
FAILURE(BgL_auxz00_13045,BFALSE,BFALSE);} 
BgL_auxz00_13041 = 
(long)CINT(BgL_tmpz00_13042); } 
if(
STRINGP(BgL_strz00_6212))
{ /* Llib/unicode.scm 1295 */
BgL_auxz00_13034 = BgL_strz00_6212
; }  else 
{ 
 obj_t BgL_auxz00_13037;
BgL_auxz00_13037 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51964L), BGl_string4931z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6212); 
FAILURE(BgL_auxz00_13037,BFALSE,BFALSE);} 
BgL_tmpz00_13033 = 
BGl_utf8zd2stringzd2rightzd2replacementzf3z21zz__unicodez00(BgL_auxz00_13034, BgL_auxz00_13041, BgL_auxz00_13050); } 
return 
BBOOL(BgL_tmpz00_13033);} } 

}



/* utf8-collapse! */
obj_t BGl_utf8zd2collapsez12zc0zz__unicodez00(obj_t BgL_bufferz00_99, long BgL_indexbz00_100, obj_t BgL_strz00_101, obj_t BgL_indexsz00_102)
{
{ /* Llib/unicode.scm 1301 */
{ /* Llib/unicode.scm 1303 */
 long BgL_cl1z00_2729;
{ /* Llib/unicode.scm 1303 */
 unsigned char BgL_tmpz00_13061;
{ /* Llib/unicode.scm 1303 */
 long BgL_i4277z00_7169;
BgL_i4277z00_7169 = 
(BgL_indexbz00_100-4L); 
{ /* Llib/unicode.scm 1303 */
 long BgL_l4278z00_7170;
BgL_l4278z00_7170 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_i4277z00_7169, BgL_l4278z00_7170))
{ /* Llib/unicode.scm 1303 */
BgL_tmpz00_13061 = 
STRING_REF(BgL_bufferz00_99, BgL_i4277z00_7169)
; }  else 
{ 
 obj_t BgL_auxz00_13067;
BgL_auxz00_13067 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52377L), BGl_string4897z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4278z00_7170), 
(int)(BgL_i4277z00_7169)); 
FAILURE(BgL_auxz00_13067,BFALSE,BFALSE);} } } 
BgL_cl1z00_2729 = 
(BgL_tmpz00_13061); } 
{ /* Llib/unicode.scm 1303 */
 long BgL_cl2z00_2730;
{ /* Llib/unicode.scm 1304 */
 unsigned char BgL_tmpz00_13074;
{ /* Llib/unicode.scm 1304 */
 long BgL_i4281z00_7173;
BgL_i4281z00_7173 = 
(BgL_indexbz00_100-3L); 
{ /* Llib/unicode.scm 1304 */
 long BgL_l4282z00_7174;
BgL_l4282z00_7174 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_i4281z00_7173, BgL_l4282z00_7174))
{ /* Llib/unicode.scm 1304 */
BgL_tmpz00_13074 = 
STRING_REF(BgL_bufferz00_99, BgL_i4281z00_7173)
; }  else 
{ 
 obj_t BgL_auxz00_13080;
BgL_auxz00_13080 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52440L), BGl_string4897z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4282z00_7174), 
(int)(BgL_i4281z00_7173)); 
FAILURE(BgL_auxz00_13080,BFALSE,BFALSE);} } } 
BgL_cl2z00_2730 = 
(BgL_tmpz00_13074); } 
{ /* Llib/unicode.scm 1304 */
 long BgL_cl3z00_2731;
{ /* Llib/unicode.scm 1305 */
 unsigned char BgL_tmpz00_13087;
{ /* Llib/unicode.scm 1305 */
 long BgL_i4285z00_7177;
BgL_i4285z00_7177 = 
(BgL_indexbz00_100-2L); 
{ /* Llib/unicode.scm 1305 */
 long BgL_l4286z00_7178;
BgL_l4286z00_7178 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_i4285z00_7177, BgL_l4286z00_7178))
{ /* Llib/unicode.scm 1305 */
BgL_tmpz00_13087 = 
STRING_REF(BgL_bufferz00_99, BgL_i4285z00_7177)
; }  else 
{ 
 obj_t BgL_auxz00_13093;
BgL_auxz00_13093 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52503L), BGl_string4897z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4286z00_7178), 
(int)(BgL_i4285z00_7177)); 
FAILURE(BgL_auxz00_13093,BFALSE,BFALSE);} } } 
BgL_cl3z00_2731 = 
(BgL_tmpz00_13087); } 
{ /* Llib/unicode.scm 1305 */
 long BgL_cl4z00_2732;
{ /* Llib/unicode.scm 1306 */
 unsigned char BgL_tmpz00_13100;
{ /* Llib/unicode.scm 1306 */
 long BgL_i4289z00_7181;
BgL_i4289z00_7181 = 
(BgL_indexbz00_100-1L); 
{ /* Llib/unicode.scm 1306 */
 long BgL_l4290z00_7182;
BgL_l4290z00_7182 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_i4289z00_7181, BgL_l4290z00_7182))
{ /* Llib/unicode.scm 1306 */
BgL_tmpz00_13100 = 
STRING_REF(BgL_bufferz00_99, BgL_i4289z00_7181)
; }  else 
{ 
 obj_t BgL_auxz00_13106;
BgL_auxz00_13106 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52566L), BGl_string4897z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4290z00_7182), 
(int)(BgL_i4289z00_7181)); 
FAILURE(BgL_auxz00_13106,BFALSE,BFALSE);} } } 
BgL_cl4z00_2732 = 
(BgL_tmpz00_13100); } 
{ /* Llib/unicode.scm 1307 */
 long BgL_cr3z00_2734;
{ /* Llib/unicode.scm 1308 */
 unsigned char BgL_tmpz00_13113;
{ /* Llib/unicode.scm 1308 */
 long BgL_i4293z00_7185;
{ /* Llib/unicode.scm 1308 */
 long BgL_za71za7_5511;
{ /* Llib/unicode.scm 1308 */
 obj_t BgL_tmpz00_13114;
if(
INTEGERP(BgL_indexsz00_102))
{ /* Llib/unicode.scm 1308 */
BgL_tmpz00_13114 = BgL_indexsz00_102
; }  else 
{ 
 obj_t BgL_auxz00_13117;
BgL_auxz00_13117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52713L), BGl_string4932z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_indexsz00_102); 
FAILURE(BgL_auxz00_13117,BFALSE,BFALSE);} 
BgL_za71za7_5511 = 
(long)CINT(BgL_tmpz00_13114); } 
BgL_i4293z00_7185 = 
(BgL_za71za7_5511+2L); } 
{ /* Llib/unicode.scm 1308 */
 long BgL_l4294z00_7186;
BgL_l4294z00_7186 = 
STRING_LENGTH(BgL_strz00_101); 
if(
BOUND_CHECK(BgL_i4293z00_7185, BgL_l4294z00_7186))
{ /* Llib/unicode.scm 1308 */
BgL_tmpz00_13113 = 
STRING_REF(BgL_strz00_101, BgL_i4293z00_7185)
; }  else 
{ 
 obj_t BgL_auxz00_13127;
BgL_auxz00_13127 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52689L), BGl_string4897z00zz__unicodez00, BgL_strz00_101, 
(int)(BgL_l4294z00_7186), 
(int)(BgL_i4293z00_7185)); 
FAILURE(BgL_auxz00_13127,BFALSE,BFALSE);} } } 
BgL_cr3z00_2734 = 
(BgL_tmpz00_13113); } 
{ /* Llib/unicode.scm 1308 */
 long BgL_cr4z00_2735;
{ /* Llib/unicode.scm 1309 */
 unsigned char BgL_tmpz00_13134;
{ /* Llib/unicode.scm 1309 */
 long BgL_i4297z00_7189;
{ /* Llib/unicode.scm 1309 */
 long BgL_za71za7_5515;
{ /* Llib/unicode.scm 1309 */
 obj_t BgL_tmpz00_13135;
if(
INTEGERP(BgL_indexsz00_102))
{ /* Llib/unicode.scm 1309 */
BgL_tmpz00_13135 = BgL_indexsz00_102
; }  else 
{ 
 obj_t BgL_auxz00_13138;
BgL_auxz00_13138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52773L), BGl_string4932z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_indexsz00_102); 
FAILURE(BgL_auxz00_13138,BFALSE,BFALSE);} 
BgL_za71za7_5515 = 
(long)CINT(BgL_tmpz00_13135); } 
BgL_i4297z00_7189 = 
(BgL_za71za7_5515+3L); } 
{ /* Llib/unicode.scm 1309 */
 long BgL_l4298z00_7190;
BgL_l4298z00_7190 = 
STRING_LENGTH(BgL_strz00_101); 
if(
BOUND_CHECK(BgL_i4297z00_7189, BgL_l4298z00_7190))
{ /* Llib/unicode.scm 1309 */
BgL_tmpz00_13134 = 
STRING_REF(BgL_strz00_101, BgL_i4297z00_7189)
; }  else 
{ 
 obj_t BgL_auxz00_13148;
BgL_auxz00_13148 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52749L), BGl_string4897z00zz__unicodez00, BgL_strz00_101, 
(int)(BgL_l4298z00_7190), 
(int)(BgL_i4297z00_7189)); 
FAILURE(BgL_auxz00_13148,BFALSE,BFALSE);} } } 
BgL_cr4z00_2735 = 
(BgL_tmpz00_13134); } 
{ /* Llib/unicode.scm 1310 */
 long BgL_yyyyz00_2737;
BgL_yyyyz00_2737 = 
(15L & BgL_cr3z00_2734); 
{ /* Llib/unicode.scm 1311 */
 long BgL_xxz00_2738;
BgL_xxz00_2738 = 
(
(BgL_cl3z00_2731 >> 
(int)(4L)) & 3L); 
{ /* Llib/unicode.scm 1313 */
 long BgL_uuuuuz00_2740;
BgL_uuuuuz00_2740 = 
(
(
(BgL_cl4z00_2732 & 7L) << 
(int)(2L)) | 
(
(BgL_cl2z00_2730 >> 
(int)(4L)) & 3L)); 
{ /* Llib/unicode.scm 1314 */

{ /* Llib/unicode.scm 1318 */
 long BgL_arg2594z00_2741; unsigned char BgL_arg2595z00_2742;
BgL_arg2594z00_2741 = 
(BgL_indexbz00_100-4L); 
BgL_arg2595z00_2742 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(
(BgL_cl1z00_2729 & 240L) | 
(BgL_uuuuuz00_2740 >> 
(int)(2L)))); 
{ /* Llib/unicode.scm 1318 */
 long BgL_l4302z00_7194;
BgL_l4302z00_7194 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_arg2594z00_2741, BgL_l4302z00_7194))
{ /* Llib/unicode.scm 1318 */
STRING_SET(BgL_bufferz00_99, BgL_arg2594z00_2741, BgL_arg2595z00_2742); }  else 
{ 
 obj_t BgL_auxz00_13176;
BgL_auxz00_13176 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(53040L), BGl_string4915z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4302z00_7194), 
(int)(BgL_arg2594z00_2741)); 
FAILURE(BgL_auxz00_13176,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1324 */
 long BgL_arg2600z00_2746; unsigned char BgL_arg2601z00_2747;
BgL_arg2600z00_2746 = 
(BgL_indexbz00_100-3L); 
BgL_arg2601z00_2747 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cl2z00_2730); 
{ /* Llib/unicode.scm 1324 */
 long BgL_l4306z00_7198;
BgL_l4306z00_7198 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_arg2600z00_2746, BgL_l4306z00_7198))
{ /* Llib/unicode.scm 1324 */
STRING_SET(BgL_bufferz00_99, BgL_arg2600z00_2746, BgL_arg2601z00_2747); }  else 
{ 
 obj_t BgL_auxz00_13188;
BgL_auxz00_13188 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(53189L), BGl_string4915z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4306z00_7198), 
(int)(BgL_arg2600z00_2746)); 
FAILURE(BgL_auxz00_13188,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1327 */
 long BgL_arg2604z00_2748; unsigned char BgL_arg2605z00_2749;
BgL_arg2604z00_2748 = 
(BgL_indexbz00_100-2L); 
BgL_arg2605z00_2749 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(128L | 
(
(BgL_xxz00_2738 << 
(int)(4L)) | BgL_yyyyz00_2737))); 
{ /* Llib/unicode.scm 1327 */
 long BgL_l4310z00_7202;
BgL_l4310z00_7202 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_arg2604z00_2748, BgL_l4310z00_7202))
{ /* Llib/unicode.scm 1327 */
STRING_SET(BgL_bufferz00_99, BgL_arg2604z00_2748, BgL_arg2605z00_2749); }  else 
{ 
 obj_t BgL_auxz00_13204;
BgL_auxz00_13204 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(53269L), BGl_string4915z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4310z00_7202), 
(int)(BgL_arg2604z00_2748)); 
FAILURE(BgL_auxz00_13204,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1332 */
 long BgL_arg2610z00_2753; unsigned char BgL_arg2611z00_2754;
BgL_arg2610z00_2753 = 
(BgL_indexbz00_100-1L); 
BgL_arg2611z00_2754 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cr4z00_2735); 
{ /* Llib/unicode.scm 1332 */
 long BgL_l4314z00_7206;
BgL_l4314z00_7206 = 
STRING_LENGTH(BgL_bufferz00_99); 
if(
BOUND_CHECK(BgL_arg2610z00_2753, BgL_l4314z00_7206))
{ /* Llib/unicode.scm 1332 */
return 
STRING_SET(BgL_bufferz00_99, BgL_arg2610z00_2753, BgL_arg2611z00_2754);}  else 
{ 
 obj_t BgL_auxz00_13216;
BgL_auxz00_13216 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(53401L), BGl_string4915z00zz__unicodez00, BgL_bufferz00_99, 
(int)(BgL_l4314z00_7206), 
(int)(BgL_arg2610z00_2753)); 
FAILURE(BgL_auxz00_13216,BFALSE,BFALSE);} } } } } } } } } } } } } } 

}



/* _utf8-string-append-fill! */
obj_t BGl__utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t BgL_env1142z00_108, obj_t BgL_opt1141z00_107)
{
{ /* Llib/unicode.scm 1344 */
{ /* Llib/unicode.scm 1344 */
 obj_t BgL_g1143z00_2774; obj_t BgL_g1144z00_2775; obj_t BgL_g1145z00_2776;
BgL_g1143z00_2774 = 
VECTOR_REF(BgL_opt1141z00_107,0L); 
BgL_g1144z00_2775 = 
VECTOR_REF(BgL_opt1141z00_107,1L); 
BgL_g1145z00_2776 = 
VECTOR_REF(BgL_opt1141z00_107,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1141z00_107)) { case 3L : 

{ /* Llib/unicode.scm 1344 */

{ /* Llib/unicode.scm 1344 */
 long BgL_tmpz00_13225;
{ /* Llib/unicode.scm 1344 */
 obj_t BgL_auxz00_13242; long BgL_auxz00_13233; obj_t BgL_auxz00_13226;
if(
STRINGP(BgL_g1145z00_2776))
{ /* Llib/unicode.scm 1344 */
BgL_auxz00_13242 = BgL_g1145z00_2776
; }  else 
{ 
 obj_t BgL_auxz00_13245;
BgL_auxz00_13245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54131L), BGl_string4936z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_g1145z00_2776); 
FAILURE(BgL_auxz00_13245,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 1344 */
 obj_t BgL_tmpz00_13234;
if(
INTEGERP(BgL_g1144z00_2775))
{ /* Llib/unicode.scm 1344 */
BgL_tmpz00_13234 = BgL_g1144z00_2775
; }  else 
{ 
 obj_t BgL_auxz00_13237;
BgL_auxz00_13237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54131L), BGl_string4936z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_g1144z00_2775); 
FAILURE(BgL_auxz00_13237,BFALSE,BFALSE);} 
BgL_auxz00_13233 = 
(long)CINT(BgL_tmpz00_13234); } 
if(
STRINGP(BgL_g1143z00_2774))
{ /* Llib/unicode.scm 1344 */
BgL_auxz00_13226 = BgL_g1143z00_2774
; }  else 
{ 
 obj_t BgL_auxz00_13229;
BgL_auxz00_13229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54131L), BGl_string4936z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_g1143z00_2774); 
FAILURE(BgL_auxz00_13229,BFALSE,BFALSE);} 
BgL_tmpz00_13225 = 
BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(BgL_auxz00_13226, BgL_auxz00_13233, BgL_auxz00_13242, 
BINT(0L)); } 
return 
BINT(BgL_tmpz00_13225);} } break;case 4L : 

{ /* Llib/unicode.scm 1344 */
 obj_t BgL_offsetz00_2780;
BgL_offsetz00_2780 = 
VECTOR_REF(BgL_opt1141z00_107,3L); 
{ /* Llib/unicode.scm 1344 */

{ /* Llib/unicode.scm 1344 */
 long BgL_tmpz00_13253;
{ /* Llib/unicode.scm 1344 */
 obj_t BgL_auxz00_13270; long BgL_auxz00_13261; obj_t BgL_auxz00_13254;
if(
STRINGP(BgL_g1145z00_2776))
{ /* Llib/unicode.scm 1344 */
BgL_auxz00_13270 = BgL_g1145z00_2776
; }  else 
{ 
 obj_t BgL_auxz00_13273;
BgL_auxz00_13273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54131L), BGl_string4936z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_g1145z00_2776); 
FAILURE(BgL_auxz00_13273,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 1344 */
 obj_t BgL_tmpz00_13262;
if(
INTEGERP(BgL_g1144z00_2775))
{ /* Llib/unicode.scm 1344 */
BgL_tmpz00_13262 = BgL_g1144z00_2775
; }  else 
{ 
 obj_t BgL_auxz00_13265;
BgL_auxz00_13265 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54131L), BGl_string4936z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_g1144z00_2775); 
FAILURE(BgL_auxz00_13265,BFALSE,BFALSE);} 
BgL_auxz00_13261 = 
(long)CINT(BgL_tmpz00_13262); } 
if(
STRINGP(BgL_g1143z00_2774))
{ /* Llib/unicode.scm 1344 */
BgL_auxz00_13254 = BgL_g1143z00_2774
; }  else 
{ 
 obj_t BgL_auxz00_13257;
BgL_auxz00_13257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54131L), BGl_string4936z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_g1143z00_2774); 
FAILURE(BgL_auxz00_13257,BFALSE,BFALSE);} 
BgL_tmpz00_13253 = 
BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(BgL_auxz00_13254, BgL_auxz00_13261, BgL_auxz00_13270, BgL_offsetz00_2780); } 
return 
BINT(BgL_tmpz00_13253);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4933z00zz__unicodez00, BGl_string4935z00zz__unicodez00, 
BINT(
VECTOR_LENGTH(BgL_opt1141z00_107)));} } } } 

}



/* utf8-string-append-fill! */
BGL_EXPORTED_DEF long BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(obj_t BgL_bufferz00_103, long BgL_indexz00_104, obj_t BgL_strz00_105, obj_t BgL_offsetz00_106)
{
{ /* Llib/unicode.scm 1344 */
{ /* Llib/unicode.scm 1345 */
 long BgL_lenz00_2782;
BgL_lenz00_2782 = 
STRING_LENGTH(BgL_strz00_105); 
{ /* Llib/unicode.scm 1347 */
 bool_t BgL_test5879z00_13285;
if(
(BgL_indexz00_104>=4L))
{ /* Llib/unicode.scm 1348 */
 bool_t BgL_test5881z00_13288;
{ /* Llib/unicode.scm 1348 */
 long BgL_indexz00_5558;
{ /* Llib/unicode.scm 1348 */
 obj_t BgL_tmpz00_13289;
if(
INTEGERP(BgL_offsetz00_106))
{ /* Llib/unicode.scm 1348 */
BgL_tmpz00_13289 = BgL_offsetz00_106
; }  else 
{ 
 obj_t BgL_auxz00_13292;
BgL_auxz00_13292 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54324L), BGl_string4934z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_offsetz00_106); 
FAILURE(BgL_auxz00_13292,BFALSE,BFALSE);} 
BgL_indexz00_5558 = 
(long)CINT(BgL_tmpz00_13289); } 
if(
(BgL_lenz00_2782>=
(BgL_indexz00_5558+4L)))
{ /* Llib/unicode.scm 1296 */
 long BgL_tmpz00_13300;
{ /* Llib/unicode.scm 1296 */
 unsigned char BgL_tmpz00_13301;
{ /* Llib/unicode.scm 1296 */
 long BgL_l4318z00_7210;
BgL_l4318z00_7210 = 
STRING_LENGTH(BgL_strz00_105); 
if(
BOUND_CHECK(BgL_indexz00_5558, BgL_l4318z00_7210))
{ /* Llib/unicode.scm 1296 */
BgL_tmpz00_13301 = 
STRING_REF(BgL_strz00_105, BgL_indexz00_5558)
; }  else 
{ 
 obj_t BgL_auxz00_13306;
BgL_auxz00_13306 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(52010L), BGl_string4897z00zz__unicodez00, BgL_strz00_105, 
(int)(BgL_l4318z00_7210), 
(int)(BgL_indexz00_5558)); 
FAILURE(BgL_auxz00_13306,BFALSE,BFALSE);} } 
BgL_tmpz00_13300 = 
(BgL_tmpz00_13301); } 
BgL_test5881z00_13288 = 
(BgL_tmpz00_13300==252L); }  else 
{ /* Llib/unicode.scm 1295 */
BgL_test5881z00_13288 = ((bool_t)0)
; } } 
if(BgL_test5881z00_13288)
{ /* Llib/unicode.scm 1349 */
 long BgL_indexz00_5573;
BgL_indexz00_5573 = 
(BgL_indexz00_104-4L); 
if(
(BgL_indexz00_104>=
(BgL_indexz00_5573+4L)))
{ /* Llib/unicode.scm 1286 */
 long BgL_tmpz00_13318;
{ /* Llib/unicode.scm 1286 */
 unsigned char BgL_tmpz00_13319;
{ /* Llib/unicode.scm 1286 */
 long BgL_l4322z00_7214;
BgL_l4322z00_7214 = 
STRING_LENGTH(BgL_bufferz00_103); 
if(
BOUND_CHECK(BgL_indexz00_5573, BgL_l4322z00_7214))
{ /* Llib/unicode.scm 1286 */
BgL_tmpz00_13319 = 
STRING_REF(BgL_bufferz00_103, BgL_indexz00_5573)
; }  else 
{ 
 obj_t BgL_auxz00_13324;
BgL_auxz00_13324 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(51414L), BGl_string4897z00zz__unicodez00, BgL_bufferz00_103, 
(int)(BgL_l4322z00_7214), 
(int)(BgL_indexz00_5573)); 
FAILURE(BgL_auxz00_13324,BFALSE,BFALSE);} } 
BgL_tmpz00_13318 = 
(BgL_tmpz00_13319); } 
BgL_test5879z00_13285 = 
(BgL_tmpz00_13318==248L); }  else 
{ /* Llib/unicode.scm 1285 */
BgL_test5879z00_13285 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1348 */
BgL_test5879z00_13285 = ((bool_t)0)
; } }  else 
{ /* Llib/unicode.scm 1347 */
BgL_test5879z00_13285 = ((bool_t)0)
; } 
if(BgL_test5879z00_13285)
{ /* Llib/unicode.scm 1347 */
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_strz00_105, 4L, BgL_bufferz00_103, BgL_indexz00_104, 
(BgL_lenz00_2782-4L)); 
BGl_utf8zd2collapsez12zc0zz__unicodez00(BgL_bufferz00_103, BgL_indexz00_104, BgL_strz00_105, BgL_offsetz00_106); 
return 
(BgL_indexz00_104+
(BgL_lenz00_2782-4L));}  else 
{ /* Llib/unicode.scm 1347 */
{ /* Llib/unicode.scm 1355 */
 long BgL_arg2640z00_2789;
{ /* Llib/unicode.scm 1355 */
 long BgL_za72za7_5590;
{ /* Llib/unicode.scm 1355 */
 obj_t BgL_tmpz00_13337;
if(
INTEGERP(BgL_offsetz00_106))
{ /* Llib/unicode.scm 1355 */
BgL_tmpz00_13337 = BgL_offsetz00_106
; }  else 
{ 
 obj_t BgL_auxz00_13340;
BgL_auxz00_13340 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54603L), BGl_string4934z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_offsetz00_106); 
FAILURE(BgL_auxz00_13340,BFALSE,BFALSE);} 
BgL_za72za7_5590 = 
(long)CINT(BgL_tmpz00_13337); } 
BgL_arg2640z00_2789 = 
(BgL_lenz00_2782-BgL_za72za7_5590); } 
{ /* Llib/unicode.scm 1355 */
 long BgL_auxz00_13346;
{ /* Llib/unicode.scm 1355 */
 obj_t BgL_tmpz00_13347;
if(
INTEGERP(BgL_offsetz00_106))
{ /* Llib/unicode.scm 1355 */
BgL_tmpz00_13347 = BgL_offsetz00_106
; }  else 
{ 
 obj_t BgL_auxz00_13350;
BgL_auxz00_13350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54556L), BGl_string4934z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_offsetz00_106); 
FAILURE(BgL_auxz00_13350,BFALSE,BFALSE);} 
BgL_auxz00_13346 = 
(long)CINT(BgL_tmpz00_13347); } 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_strz00_105, BgL_auxz00_13346, BgL_bufferz00_103, BgL_indexz00_104, BgL_arg2640z00_2789); } } 
{ /* Llib/unicode.scm 1356 */
 long BgL_tmpz00_13356;
{ /* Llib/unicode.scm 1356 */
 long BgL_za72za7_5592;
{ /* Llib/unicode.scm 1356 */
 obj_t BgL_tmpz00_13357;
if(
INTEGERP(BgL_offsetz00_106))
{ /* Llib/unicode.scm 1356 */
BgL_tmpz00_13357 = BgL_offsetz00_106
; }  else 
{ 
 obj_t BgL_auxz00_13360;
BgL_auxz00_13360 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(54635L), BGl_string4934z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_offsetz00_106); 
FAILURE(BgL_auxz00_13360,BFALSE,BFALSE);} 
BgL_za72za7_5592 = 
(long)CINT(BgL_tmpz00_13357); } 
BgL_tmpz00_13356 = 
(BgL_lenz00_2782-BgL_za72za7_5592); } 
return 
(BgL_indexz00_104+BgL_tmpz00_13356);} } } } } 

}



/* utf8-string-append */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2appendz00zz__unicodez00(obj_t BgL_leftz00_109, obj_t BgL_rightz00_110)
{
{ /* Llib/unicode.scm 1366 */
{ /* Llib/unicode.scm 1367 */
 long BgL_llenz00_2794;
BgL_llenz00_2794 = 
STRING_LENGTH(BgL_leftz00_109); 
{ /* Llib/unicode.scm 1367 */
 long BgL_rlenz00_2795;
BgL_rlenz00_2795 = 
STRING_LENGTH(BgL_rightz00_110); 
{ /* Llib/unicode.scm 1368 */
 obj_t BgL_bufferz00_2796;
BgL_bufferz00_2796 = 
make_string_sans_fill(
(BgL_llenz00_2794+BgL_rlenz00_2795)); 
{ /* Llib/unicode.scm 1369 */

BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_leftz00_109, 0L, BgL_bufferz00_2796, 0L, BgL_llenz00_2794); 
{ /* Llib/unicode.scm 1371 */
 long BgL_nindexz00_2797;
{ /* Llib/unicode.scm 181 */

BgL_nindexz00_2797 = 
BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(BgL_bufferz00_2796, BgL_llenz00_2794, BgL_rightz00_110, 
BINT(0L)); } 
return 
bgl_string_shrink(BgL_bufferz00_2796, BgL_nindexz00_2797);} } } } } } 

}



/* &utf8-string-append */
obj_t BGl_z62utf8zd2stringzd2appendz62zz__unicodez00(obj_t BgL_envz00_6215, obj_t BgL_leftz00_6216, obj_t BgL_rightz00_6217)
{
{ /* Llib/unicode.scm 1366 */
{ /* Llib/unicode.scm 1367 */
 obj_t BgL_auxz00_13382; obj_t BgL_auxz00_13375;
if(
STRINGP(BgL_rightz00_6217))
{ /* Llib/unicode.scm 1367 */
BgL_auxz00_13382 = BgL_rightz00_6217
; }  else 
{ 
 obj_t BgL_auxz00_13385;
BgL_auxz00_13385 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(55284L), BGl_string4937z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_rightz00_6217); 
FAILURE(BgL_auxz00_13385,BFALSE,BFALSE);} 
if(
STRINGP(BgL_leftz00_6216))
{ /* Llib/unicode.scm 1367 */
BgL_auxz00_13375 = BgL_leftz00_6216
; }  else 
{ 
 obj_t BgL_auxz00_13378;
BgL_auxz00_13378 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(55284L), BGl_string4937z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_leftz00_6216); 
FAILURE(BgL_auxz00_13378,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2appendz00zz__unicodez00(BgL_auxz00_13375, BgL_auxz00_13382);} } 

}



/* utf8-string-append* */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2appendza2za2zz__unicodez00(obj_t BgL_stringsz00_111)
{
{ /* Llib/unicode.scm 1379 */
{ /* Llib/unicode.scm 1380 */
 long BgL_lenz00_2803;
BgL_lenz00_2803 = 0L; 
{ 
 obj_t BgL_l1105z00_2805;
BgL_l1105z00_2805 = BgL_stringsz00_111; 
BgL_zc3z04anonymousza32644ze3z87_2806:
if(
PAIRP(BgL_l1105z00_2805))
{ /* Llib/unicode.scm 1381 */
{ /* Llib/unicode.scm 1382 */
 obj_t BgL_strz00_2808;
BgL_strz00_2808 = 
CAR(BgL_l1105z00_2805); 
{ /* Llib/unicode.scm 1382 */
 long BgL_za71za7_5603;
BgL_za71za7_5603 = BgL_lenz00_2803; 
{ /* Llib/unicode.scm 1382 */
 long BgL_tmpz00_13393;
{ /* Llib/unicode.scm 1382 */
 obj_t BgL_stringz00_5602;
if(
STRINGP(BgL_strz00_2808))
{ /* Llib/unicode.scm 1382 */
BgL_stringz00_5602 = BgL_strz00_2808; }  else 
{ 
 obj_t BgL_auxz00_13396;
BgL_auxz00_13396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56031L), BGl_string4938z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2808); 
FAILURE(BgL_auxz00_13396,BFALSE,BFALSE);} 
BgL_tmpz00_13393 = 
STRING_LENGTH(BgL_stringz00_5602); } 
BgL_lenz00_2803 = 
(BgL_za71za7_5603+BgL_tmpz00_13393); } } } 
{ 
 obj_t BgL_l1105z00_13402;
BgL_l1105z00_13402 = 
CDR(BgL_l1105z00_2805); 
BgL_l1105z00_2805 = BgL_l1105z00_13402; 
goto BgL_zc3z04anonymousza32644ze3z87_2806;} }  else 
{ /* Llib/unicode.scm 1381 */
if(
NULLP(BgL_l1105z00_2805))
{ /* Llib/unicode.scm 1381 */BTRUE; }  else 
{ /* Llib/unicode.scm 1381 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string4939z00zz__unicodez00, BGl_string4940z00zz__unicodez00, BgL_l1105z00_2805, BGl_string4847z00zz__unicodez00, 
BINT(55983L)); } } } 
{ /* Llib/unicode.scm 1384 */
 obj_t BgL_bufferz00_2813; long BgL_indexz00_2814;
BgL_bufferz00_2813 = 
make_string_sans_fill(BgL_lenz00_2803); 
BgL_indexz00_2814 = 0L; 
{ 
 obj_t BgL_l1107z00_2816;
BgL_l1107z00_2816 = BgL_stringsz00_111; 
BgL_zc3z04anonymousza32650ze3z87_2817:
if(
PAIRP(BgL_l1107z00_2816))
{ /* Llib/unicode.scm 1386 */
{ /* Llib/unicode.scm 1387 */
 obj_t BgL_strz00_2819;
BgL_strz00_2819 = 
CAR(BgL_l1107z00_2816); 
{ /* Llib/unicode.scm 1387 */
 long BgL_g1144z00_2821;
BgL_g1144z00_2821 = BgL_indexz00_2814; 
{ /* Llib/unicode.scm 181 */

{ /* Llib/unicode.scm 181 */
 obj_t BgL_auxz00_13412;
if(
STRINGP(BgL_strz00_2819))
{ /* Llib/unicode.scm 1387 */
BgL_auxz00_13412 = BgL_strz00_2819
; }  else 
{ 
 obj_t BgL_auxz00_13415;
BgL_auxz00_13415 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56214L), BGl_string4941z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2819); 
FAILURE(BgL_auxz00_13415,BFALSE,BFALSE);} 
BgL_indexz00_2814 = 
BGl_utf8zd2stringzd2appendzd2fillz12zc0zz__unicodez00(BgL_bufferz00_2813, BgL_g1144z00_2821, BgL_auxz00_13412, 
BINT(0L)); } } } } 
{ 
 obj_t BgL_l1107z00_13421;
BgL_l1107z00_13421 = 
CDR(BgL_l1107z00_2816); 
BgL_l1107z00_2816 = BgL_l1107z00_13421; 
goto BgL_zc3z04anonymousza32650ze3z87_2817;} }  else 
{ /* Llib/unicode.scm 1386 */
if(
NULLP(BgL_l1107z00_2816))
{ /* Llib/unicode.scm 1386 */BTRUE; }  else 
{ /* Llib/unicode.scm 1386 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string4939z00zz__unicodez00, BGl_string4940z00zz__unicodez00, BgL_l1107z00_2816, BGl_string4847z00zz__unicodez00, 
BINT(56131L)); } } } 
return 
bgl_string_shrink(BgL_bufferz00_2813, BgL_indexz00_2814);} } } 

}



/* &utf8-string-append* */
obj_t BGl_z62utf8zd2stringzd2appendza2zc0zz__unicodez00(obj_t BgL_envz00_6218, obj_t BgL_stringsz00_6219)
{
{ /* Llib/unicode.scm 1379 */
return 
BGl_utf8zd2stringzd2appendza2za2zz__unicodez00(BgL_stringsz00_6219);} 

}



/* _utf8-substring */
obj_t BGl__utf8zd2substringzd2zz__unicodez00(obj_t BgL_env1149z00_116, obj_t BgL_opt1148z00_115)
{
{ /* Llib/unicode.scm 1394 */
{ /* Llib/unicode.scm 1394 */
 obj_t BgL_strz00_2827; obj_t BgL_g1150z00_2828;
BgL_strz00_2827 = 
VECTOR_REF(BgL_opt1148z00_115,0L); 
BgL_g1150z00_2828 = 
VECTOR_REF(BgL_opt1148z00_115,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1148z00_115)) { case 2L : 

{ /* Llib/unicode.scm 1395 */
 long BgL_endz00_2831;
{ /* Llib/unicode.scm 1395 */
 obj_t BgL_auxz00_13431;
if(
STRINGP(BgL_strz00_2827))
{ /* Llib/unicode.scm 1395 */
BgL_auxz00_13431 = BgL_strz00_2827
; }  else 
{ 
 obj_t BgL_auxz00_13434;
BgL_auxz00_13434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56588L), BGl_string4945z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2827); 
FAILURE(BgL_auxz00_13434,BFALSE,BFALSE);} 
BgL_endz00_2831 = 
BGl_utf8zd2stringzd2lengthz00zz__unicodez00(BgL_auxz00_13431); } 
{ /* Llib/unicode.scm 1394 */

{ /* Llib/unicode.scm 1394 */
 long BgL_auxz00_13446; obj_t BgL_auxz00_13439;
{ /* Llib/unicode.scm 1394 */
 obj_t BgL_tmpz00_13447;
if(
INTEGERP(BgL_g1150z00_2828))
{ /* Llib/unicode.scm 1394 */
BgL_tmpz00_13447 = BgL_g1150z00_2828
; }  else 
{ 
 obj_t BgL_auxz00_13450;
BgL_auxz00_13450 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56493L), BGl_string4945z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_g1150z00_2828); 
FAILURE(BgL_auxz00_13450,BFALSE,BFALSE);} 
BgL_auxz00_13446 = 
(long)CINT(BgL_tmpz00_13447); } 
if(
STRINGP(BgL_strz00_2827))
{ /* Llib/unicode.scm 1394 */
BgL_auxz00_13439 = BgL_strz00_2827
; }  else 
{ 
 obj_t BgL_auxz00_13442;
BgL_auxz00_13442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56493L), BGl_string4945z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2827); 
FAILURE(BgL_auxz00_13442,BFALSE,BFALSE);} 
return 
BGl_utf8zd2substringzd2zz__unicodez00(BgL_auxz00_13439, BgL_auxz00_13446, BgL_endz00_2831);} } } break;case 3L : 

{ /* Llib/unicode.scm 1394 */
 obj_t BgL_endz00_2832;
BgL_endz00_2832 = 
VECTOR_REF(BgL_opt1148z00_115,2L); 
{ /* Llib/unicode.scm 1394 */

{ /* Llib/unicode.scm 1394 */
 long BgL_auxz00_13473; long BgL_auxz00_13464; obj_t BgL_auxz00_13457;
{ /* Llib/unicode.scm 1394 */
 obj_t BgL_tmpz00_13474;
if(
INTEGERP(BgL_endz00_2832))
{ /* Llib/unicode.scm 1394 */
BgL_tmpz00_13474 = BgL_endz00_2832
; }  else 
{ 
 obj_t BgL_auxz00_13477;
BgL_auxz00_13477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56493L), BGl_string4945z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_endz00_2832); 
FAILURE(BgL_auxz00_13477,BFALSE,BFALSE);} 
BgL_auxz00_13473 = 
(long)CINT(BgL_tmpz00_13474); } 
{ /* Llib/unicode.scm 1394 */
 obj_t BgL_tmpz00_13465;
if(
INTEGERP(BgL_g1150z00_2828))
{ /* Llib/unicode.scm 1394 */
BgL_tmpz00_13465 = BgL_g1150z00_2828
; }  else 
{ 
 obj_t BgL_auxz00_13468;
BgL_auxz00_13468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56493L), BGl_string4945z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_g1150z00_2828); 
FAILURE(BgL_auxz00_13468,BFALSE,BFALSE);} 
BgL_auxz00_13464 = 
(long)CINT(BgL_tmpz00_13465); } 
if(
STRINGP(BgL_strz00_2827))
{ /* Llib/unicode.scm 1394 */
BgL_auxz00_13457 = BgL_strz00_2827
; }  else 
{ 
 obj_t BgL_auxz00_13460;
BgL_auxz00_13460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56493L), BGl_string4945z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_2827); 
FAILURE(BgL_auxz00_13460,BFALSE,BFALSE);} 
return 
BGl_utf8zd2substringzd2zz__unicodez00(BgL_auxz00_13457, BgL_auxz00_13464, BgL_auxz00_13473);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4942z00zz__unicodez00, BGl_string4944z00zz__unicodez00, 
BINT(
VECTOR_LENGTH(BgL_opt1148z00_115)));} } } } 

}



/* utf8-substring */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2substringzd2zz__unicodez00(obj_t BgL_strz00_112, long BgL_startz00_113, long BgL_endz00_114)
{
{ /* Llib/unicode.scm 1394 */
{ /* Llib/unicode.scm 1396 */
 long BgL_lenz00_2834;
BgL_lenz00_2834 = 
STRING_LENGTH(BgL_strz00_112); 
{ /* Llib/unicode.scm 1398 */
 bool_t BgL_test5904z00_13489;
if(
(BgL_startz00_113<0L))
{ /* Llib/unicode.scm 1398 */
BgL_test5904z00_13489 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1398 */
BgL_test5904z00_13489 = 
(BgL_startz00_113>BgL_lenz00_2834)
; } 
if(BgL_test5904z00_13489)
{ /* Llib/unicode.scm 1400 */
 obj_t BgL_arg2659z00_2837;
BgL_arg2659z00_2837 = 
string_append_3(BGl_string4946z00zz__unicodez00, BgL_strz00_112, BGl_string4947z00zz__unicodez00); 
{ /* Llib/unicode.scm 1399 */
 obj_t BgL_aux4679z00_7571;
BgL_aux4679z00_7571 = 
BGl_errorz00zz__errorz00(BGl_string4943z00zz__unicodez00, BgL_arg2659z00_2837, 
BINT(BgL_startz00_113)); 
if(
STRINGP(BgL_aux4679z00_7571))
{ /* Llib/unicode.scm 1399 */
return BgL_aux4679z00_7571;}  else 
{ 
 obj_t BgL_auxz00_13498;
BgL_auxz00_13498 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56684L), BGl_string4943z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4679z00_7571); 
FAILURE(BgL_auxz00_13498,BFALSE,BFALSE);} } }  else 
{ /* Llib/unicode.scm 1398 */
if(
(BgL_endz00_114<0L))
{ /* Llib/unicode.scm 1404 */
 obj_t BgL_arg2662z00_2839;
BgL_arg2662z00_2839 = 
string_append_3(BGl_string4948z00zz__unicodez00, BgL_strz00_112, BGl_string4947z00zz__unicodez00); 
{ /* Llib/unicode.scm 1403 */
 obj_t BgL_aux4681z00_7573;
BgL_aux4681z00_7573 = 
BGl_errorz00zz__errorz00(BGl_string4943z00zz__unicodez00, BgL_arg2662z00_2839, 
BINT(BgL_endz00_114)); 
if(
STRINGP(BgL_aux4681z00_7573))
{ /* Llib/unicode.scm 1403 */
return BgL_aux4681z00_7573;}  else 
{ 
 obj_t BgL_auxz00_13509;
BgL_auxz00_13509 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56796L), BGl_string4943z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4681z00_7573); 
FAILURE(BgL_auxz00_13509,BFALSE,BFALSE);} } }  else 
{ /* Llib/unicode.scm 1406 */
 bool_t BgL_test5909z00_13513;
if(
(BgL_endz00_114<BgL_startz00_113))
{ /* Llib/unicode.scm 1406 */
BgL_test5909z00_13513 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1406 */
BgL_test5909z00_13513 = 
(BgL_endz00_114>BgL_lenz00_2834)
; } 
if(BgL_test5909z00_13513)
{ /* Llib/unicode.scm 1408 */
 obj_t BgL_arg2665z00_2842;
BgL_arg2665z00_2842 = 
string_append_3(BGl_string4948z00zz__unicodez00, BgL_strz00_112, BGl_string4947z00zz__unicodez00); 
{ /* Llib/unicode.scm 1407 */
 obj_t BgL_aux4683z00_7575;
BgL_aux4683z00_7575 = 
BGl_errorz00zz__errorz00(BGl_string4943z00zz__unicodez00, BgL_arg2665z00_2842, 
BINT(BgL_endz00_114)); 
if(
STRINGP(BgL_aux4683z00_7575))
{ /* Llib/unicode.scm 1407 */
return BgL_aux4683z00_7575;}  else 
{ 
 obj_t BgL_auxz00_13522;
BgL_auxz00_13522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(56927L), BGl_string4943z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4683z00_7575); 
FAILURE(BgL_auxz00_13522,BFALSE,BFALSE);} } }  else 
{ /* Llib/unicode.scm 1406 */
if(
(BgL_startz00_113==BgL_endz00_114))
{ /* Llib/unicode.scm 1410 */
return BGl_string4817z00zz__unicodez00;}  else 
{ 
 long BgL_rz00_2845; long BgL_nz00_2846; long BgL_iz00_2847;
BgL_rz00_2845 = 0L; 
BgL_nz00_2846 = 0L; 
BgL_iz00_2847 = 0L; 
BgL_zc3z04anonymousza32667ze3z87_2848:
if(
(BgL_rz00_2845==BgL_lenz00_2834))
{ /* Llib/unicode.scm 1416 */
BGL_TAIL return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_112, BgL_iz00_2847, BgL_rz00_2845);}  else 
{ /* Llib/unicode.scm 1418 */
 unsigned char BgL_cz00_2850;
{ /* Llib/unicode.scm 1418 */
 long BgL_l4326z00_7218;
BgL_l4326z00_7218 = 
STRING_LENGTH(BgL_strz00_112); 
if(
BOUND_CHECK(BgL_rz00_2845, BgL_l4326z00_7218))
{ /* Llib/unicode.scm 1418 */
BgL_cz00_2850 = 
STRING_REF(BgL_strz00_112, BgL_rz00_2845); }  else 
{ 
 obj_t BgL_auxz00_13535;
BgL_auxz00_13535 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(57156L), BGl_string4897z00zz__unicodez00, BgL_strz00_112, 
(int)(BgL_l4326z00_7218), 
(int)(BgL_rz00_2845)); 
FAILURE(BgL_auxz00_13535,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1418 */
 long BgL_sz00_2851;
{ /* Llib/unicode.scm 1419 */
 long BgL_res3371z00_5632;
{ /* Llib/unicode.scm 1419 */
 unsigned char BgL_cz00_5625;
BgL_cz00_5625 = 
(char)(BgL_cz00_2850); 
{ /* Llib/unicode.scm 1203 */
 long BgL_arg2543z00_5626;
BgL_arg2543z00_5626 = 
(
(
(unsigned char)(BgL_cz00_5625)) >> 
(int)(4L)); 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_vectorz00_5630;
BgL_vectorz00_5630 = BGl_vector4921z00zz__unicodez00; 
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_tmpz00_13546;
{ /* Llib/unicode.scm 1202 */
 obj_t BgL_aux4685z00_7577;
BgL_aux4685z00_7577 = 
VECTOR_REF(BgL_vectorz00_5630,BgL_arg2543z00_5626); 
if(
INTEGERP(BgL_aux4685z00_7577))
{ /* Llib/unicode.scm 1202 */
BgL_tmpz00_13546 = BgL_aux4685z00_7577
; }  else 
{ 
 obj_t BgL_auxz00_13550;
BgL_auxz00_13550 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(48257L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_aux4685z00_7577); 
FAILURE(BgL_auxz00_13550,BFALSE,BFALSE);} } 
BgL_res3371z00_5632 = 
(long)CINT(BgL_tmpz00_13546); } } } } 
BgL_sz00_2851 = BgL_res3371z00_5632; } 
{ /* Llib/unicode.scm 1419 */

if(
(BgL_nz00_2846==BgL_startz00_113))
{ 
 long BgL_iz00_13561; long BgL_nz00_13559; long BgL_rz00_13557;
BgL_rz00_13557 = 
(BgL_rz00_2845+BgL_sz00_2851); 
BgL_nz00_13559 = 
(BgL_nz00_2846+1L); 
BgL_iz00_13561 = BgL_rz00_2845; 
BgL_iz00_2847 = BgL_iz00_13561; 
BgL_nz00_2846 = BgL_nz00_13559; 
BgL_rz00_2845 = BgL_rz00_13557; 
goto BgL_zc3z04anonymousza32667ze3z87_2848;}  else 
{ /* Llib/unicode.scm 1421 */
if(
(BgL_nz00_2846==BgL_endz00_114))
{ /* Llib/unicode.scm 1423 */
BGL_TAIL return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_112, BgL_iz00_2847, BgL_rz00_2845);}  else 
{ 
 long BgL_nz00_13567; long BgL_rz00_13565;
BgL_rz00_13565 = 
(BgL_rz00_2845+BgL_sz00_2851); 
BgL_nz00_13567 = 
(BgL_nz00_2846+1L); 
BgL_nz00_2846 = BgL_nz00_13567; 
BgL_rz00_2845 = BgL_rz00_13565; 
goto BgL_zc3z04anonymousza32667ze3z87_2848;} } } } } } } } } } } } 

}



/* utf8->8bits-fill! */
obj_t BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(obj_t BgL_nstrz00_117, obj_t BgL_strz00_118, int BgL_lenz00_119, obj_t BgL_tablez00_120)
{
{ /* Llib/unicode.scm 1431 */
{ 
 obj_t BgL_rz00_2908;
{ 
 obj_t BgL_rz00_2866; long BgL_wz00_2867;
BgL_rz00_2866 = 
BINT(0L); 
BgL_wz00_2867 = 0L; 
BgL_zc3z04anonymousza32676ze3z87_2868:
{ /* Llib/unicode.scm 1467 */
 bool_t BgL_test5918z00_13569;
{ /* Llib/unicode.scm 1467 */
 long BgL_n1z00_5671; long BgL_n2z00_5672;
{ /* Llib/unicode.scm 1467 */
 obj_t BgL_tmpz00_13570;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1467 */
BgL_tmpz00_13570 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13573;
BgL_auxz00_13573 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58668L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13573,BFALSE,BFALSE);} 
BgL_n1z00_5671 = 
(long)CINT(BgL_tmpz00_13570); } 
BgL_n2z00_5672 = 
(long)(BgL_lenz00_119); 
BgL_test5918z00_13569 = 
(BgL_n1z00_5671==BgL_n2z00_5672); } 
if(BgL_test5918z00_13569)
{ /* Llib/unicode.scm 1467 */
return BgL_nstrz00_117;}  else 
{ /* Llib/unicode.scm 1469 */
 unsigned char BgL_cz00_2870;
{ /* Llib/unicode.scm 1469 */
 long BgL_kz00_5674;
{ /* Llib/unicode.scm 1469 */
 obj_t BgL_tmpz00_13580;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1469 */
BgL_tmpz00_13580 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13583;
BgL_auxz00_13583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58712L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13583,BFALSE,BFALSE);} 
BgL_kz00_5674 = 
(long)CINT(BgL_tmpz00_13580); } 
{ /* Llib/unicode.scm 1469 */
 long BgL_l4334z00_7226;
BgL_l4334z00_7226 = 
STRING_LENGTH(BgL_strz00_118); 
if(
BOUND_CHECK(BgL_kz00_5674, BgL_l4334z00_7226))
{ /* Llib/unicode.scm 1469 */
BgL_cz00_2870 = 
STRING_REF(BgL_strz00_118, BgL_kz00_5674); }  else 
{ 
 obj_t BgL_auxz00_13592;
BgL_auxz00_13592 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58696L), BGl_string4897z00zz__unicodez00, BgL_strz00_118, 
(int)(BgL_l4334z00_7226), 
(int)(BgL_kz00_5674)); 
FAILURE(BgL_auxz00_13592,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1469 */
 long BgL_nz00_2871;
BgL_nz00_2871 = 
(BgL_cz00_2870); 
{ /* Llib/unicode.scm 1470 */

if(
(BgL_nz00_2871<=127L))
{ /* Llib/unicode.scm 1472 */
{ /* Llib/unicode.scm 1473 */
 long BgL_l4338z00_7230;
BgL_l4338z00_7230 = 
STRING_LENGTH(BgL_nstrz00_117); 
if(
BOUND_CHECK(BgL_wz00_2867, BgL_l4338z00_7230))
{ /* Llib/unicode.scm 1473 */
STRING_SET(BgL_nstrz00_117, BgL_wz00_2867, BgL_cz00_2870); }  else 
{ 
 obj_t BgL_auxz00_13605;
BgL_auxz00_13605 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58774L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_117, 
(int)(BgL_l4338z00_7230), 
(int)(BgL_wz00_2867)); 
FAILURE(BgL_auxz00_13605,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1474 */
 long BgL_arg2680z00_2873; long BgL_arg2681z00_2874;
{ /* Llib/unicode.scm 1474 */
 long BgL_za71za7_5680;
{ /* Llib/unicode.scm 1474 */
 obj_t BgL_tmpz00_13611;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1474 */
BgL_tmpz00_13611 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13614;
BgL_auxz00_13614 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58811L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13614,BFALSE,BFALSE);} 
BgL_za71za7_5680 = 
(long)CINT(BgL_tmpz00_13611); } 
BgL_arg2680z00_2873 = 
(BgL_za71za7_5680+1L); } 
BgL_arg2681z00_2874 = 
(BgL_wz00_2867+1L); 
{ 
 long BgL_wz00_13623; obj_t BgL_rz00_13621;
BgL_rz00_13621 = 
BINT(BgL_arg2680z00_2873); 
BgL_wz00_13623 = BgL_arg2681z00_2874; 
BgL_wz00_2867 = BgL_wz00_13623; 
BgL_rz00_2866 = BgL_rz00_13621; 
goto BgL_zc3z04anonymousza32676ze3z87_2868;} } }  else 
{ /* Llib/unicode.scm 1472 */
if(
(BgL_nz00_2871<194L))
{ /* Llib/unicode.scm 1475 */
BgL_rz00_2908 = BgL_rz00_2866; 
BgL_zc3z04anonymousza32705ze3z87_2909:
{ /* Llib/unicode.scm 1441 */
 obj_t BgL_arg2706z00_2910; obj_t BgL_arg2707z00_2911;
{ /* Llib/unicode.scm 1441 */
 long BgL_arg2708z00_2912;
{ /* Llib/unicode.scm 1441 */
 unsigned char BgL_tmpz00_13626;
{ /* Llib/unicode.scm 1441 */
 long BgL_kz00_5648;
{ /* Llib/unicode.scm 1441 */
 obj_t BgL_tmpz00_13627;
if(
INTEGERP(BgL_rz00_2908))
{ /* Llib/unicode.scm 1441 */
BgL_tmpz00_13627 = BgL_rz00_2908
; }  else 
{ 
 obj_t BgL_auxz00_13630;
BgL_auxz00_13630 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(57954L), BGl_string4949z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2908); 
FAILURE(BgL_auxz00_13630,BFALSE,BFALSE);} 
BgL_kz00_5648 = 
(long)CINT(BgL_tmpz00_13627); } 
{ /* Llib/unicode.scm 1441 */
 long BgL_l4330z00_7222;
BgL_l4330z00_7222 = 
STRING_LENGTH(BgL_strz00_118); 
if(
BOUND_CHECK(BgL_kz00_5648, BgL_l4330z00_7222))
{ /* Llib/unicode.scm 1441 */
BgL_tmpz00_13626 = 
STRING_REF(BgL_strz00_118, BgL_kz00_5648)
; }  else 
{ 
 obj_t BgL_auxz00_13639;
BgL_auxz00_13639 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(57938L), BGl_string4897z00zz__unicodez00, BgL_strz00_118, 
(int)(BgL_l4330z00_7222), 
(int)(BgL_kz00_5648)); 
FAILURE(BgL_auxz00_13639,BFALSE,BFALSE);} } } 
BgL_arg2708z00_2912 = 
(BgL_tmpz00_13626); } 
{ /* Llib/unicode.scm 1440 */
 obj_t BgL_list2709z00_2913;
{ /* Llib/unicode.scm 1440 */
 obj_t BgL_arg2710z00_2914;
BgL_arg2710z00_2914 = 
MAKE_YOUNG_PAIR(BgL_rz00_2908, BNIL); 
BgL_list2709z00_2913 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2708z00_2912), BgL_arg2710z00_2914); } 
BgL_arg2706z00_2910 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string4950z00zz__unicodez00, BgL_list2709z00_2913); } } 
{ /* Llib/unicode.scm 1443 */
 obj_t BgL_arg2712z00_2916;
{ /* Llib/unicode.scm 1443 */
 int BgL_arg2714z00_2917;
{ /* Llib/unicode.scm 1443 */
 long BgL_bz00_2919;
{ /* Llib/unicode.scm 1443 */
 long BgL_za71za7_5650;
{ /* Llib/unicode.scm 1443 */
 obj_t BgL_tmpz00_13650;
if(
INTEGERP(BgL_rz00_2908))
{ /* Llib/unicode.scm 1443 */
BgL_tmpz00_13650 = BgL_rz00_2908
; }  else 
{ 
 obj_t BgL_auxz00_13653;
BgL_auxz00_13653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58018L), BGl_string4949z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2908); 
FAILURE(BgL_auxz00_13653,BFALSE,BFALSE);} 
BgL_za71za7_5650 = 
(long)CINT(BgL_tmpz00_13650); } 
BgL_bz00_2919 = 
(BgL_za71za7_5650+10L); } 
if(
(
(long)(BgL_lenz00_119)<BgL_bz00_2919))
{ /* Llib/unicode.scm 1443 */
BgL_arg2714z00_2917 = BgL_lenz00_119; }  else 
{ /* Llib/unicode.scm 1443 */
BgL_arg2714z00_2917 = 
(int)(BgL_bz00_2919); } } 
{ /* Llib/unicode.scm 1443 */
 long BgL_auxz00_13663;
{ /* Llib/unicode.scm 1443 */
 obj_t BgL_tmpz00_13664;
if(
INTEGERP(BgL_rz00_2908))
{ /* Llib/unicode.scm 1443 */
BgL_tmpz00_13664 = BgL_rz00_2908
; }  else 
{ 
 obj_t BgL_auxz00_13667;
BgL_auxz00_13667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(57985L), BGl_string4949z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2908); 
FAILURE(BgL_auxz00_13667,BFALSE,BFALSE);} 
BgL_auxz00_13663 = 
(long)CINT(BgL_tmpz00_13664); } 
BgL_arg2712z00_2916 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_118, BgL_auxz00_13663, 
(long)(BgL_arg2714z00_2917)); } } 
BgL_arg2707z00_2911 = 
string_for_read(BgL_arg2712z00_2916); } 
return 
BGl_errorz00zz__errorz00(BGl_string4951z00zz__unicodez00, BgL_arg2706z00_2910, BgL_arg2707z00_2911);} }  else 
{ /* Llib/unicode.scm 1477 */
 obj_t BgL_g1063z00_2876;
if(
CBOOL(BgL_tablez00_120))
{ /* Llib/unicode.scm 1477 */
 obj_t BgL_auxz00_13678;
{ /* Llib/unicode.scm 1477 */
 bool_t BgL_test5932z00_13680;
if(
PAIRP(BgL_tablez00_120))
{ /* Llib/unicode.scm 1477 */
BgL_test5932z00_13680 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1477 */
BgL_test5932z00_13680 = 
NULLP(BgL_tablez00_120)
; } 
if(BgL_test5932z00_13680)
{ /* Llib/unicode.scm 1477 */
BgL_auxz00_13678 = BgL_tablez00_120
; }  else 
{ 
 obj_t BgL_auxz00_13684;
BgL_auxz00_13684 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58884L), BGl_string4885z00zz__unicodez00, BGl_string4884z00zz__unicodez00, BgL_tablez00_120); 
FAILURE(BgL_auxz00_13684,BFALSE,BFALSE);} } 
BgL_g1063z00_2876 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
BINT(BgL_nz00_2871), BgL_auxz00_13678); }  else 
{ /* Llib/unicode.scm 1477 */
BgL_g1063z00_2876 = BFALSE; } 
if(
CBOOL(BgL_g1063z00_2876))
{ 
 long BgL_wz00_13693; obj_t BgL_rz00_13691;
BgL_rz00_13691 = 
BGl_tablezd2ze38bitsze70zd6zz__unicodez00(BgL_nstrz00_117, BgL_tablez00_120, BgL_strz00_118, BgL_lenz00_119, BgL_g1063z00_2876, BgL_rz00_2866, BgL_wz00_2867, BgL_nz00_2871); 
BgL_wz00_13693 = 
(BgL_wz00_2867+1L); 
BgL_wz00_2867 = BgL_wz00_13693; 
BgL_rz00_2866 = BgL_rz00_13691; 
goto BgL_zc3z04anonymousza32676ze3z87_2868;}  else 
{ /* Llib/unicode.scm 1477 */
if(
(BgL_nz00_2871<=223L))
{ /* Llib/unicode.scm 1482 */
 bool_t BgL_test5936z00_13697;
{ /* Llib/unicode.scm 1482 */
 long BgL_n1z00_5686;
{ /* Llib/unicode.scm 1482 */
 obj_t BgL_tmpz00_13698;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1482 */
BgL_tmpz00_13698 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13701;
BgL_auxz00_13701 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59004L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13701,BFALSE,BFALSE);} 
BgL_n1z00_5686 = 
(long)CINT(BgL_tmpz00_13698); } 
BgL_test5936z00_13697 = 
(BgL_n1z00_5686==
(
(long)(BgL_lenz00_119)-1L)); } 
if(BgL_test5936z00_13697)
{ /* Llib/unicode.scm 1482 */
return 
BGl_errorzd2toozd2shortze70ze7zz__unicodez00(BgL_lenz00_119, BgL_strz00_118, BgL_rz00_2866);}  else 
{ /* Llib/unicode.scm 1484 */
 unsigned char BgL_ncz00_2884;
{ /* Llib/unicode.scm 1484 */
 long BgL_i4341z00_7233;
{ /* Llib/unicode.scm 1484 */
 long BgL_za71za7_5688;
{ /* Llib/unicode.scm 1484 */
 obj_t BgL_tmpz00_13710;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1484 */
BgL_tmpz00_13710 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13713;
BgL_auxz00_13713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59085L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13713,BFALSE,BFALSE);} 
BgL_za71za7_5688 = 
(long)CINT(BgL_tmpz00_13710); } 
BgL_i4341z00_7233 = 
(BgL_za71za7_5688+1L); } 
{ /* Llib/unicode.scm 1484 */
 long BgL_l4342z00_7234;
BgL_l4342z00_7234 = 
STRING_LENGTH(BgL_strz00_118); 
if(
BOUND_CHECK(BgL_i4341z00_7233, BgL_l4342z00_7234))
{ /* Llib/unicode.scm 1484 */
BgL_ncz00_2884 = 
STRING_REF(BgL_strz00_118, BgL_i4341z00_7233); }  else 
{ 
 obj_t BgL_auxz00_13723;
BgL_auxz00_13723 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59064L), BGl_string4897z00zz__unicodez00, BgL_strz00_118, 
(int)(BgL_l4342z00_7234), 
(int)(BgL_i4341z00_7233)); 
FAILURE(BgL_auxz00_13723,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1484 */
 long BgL_nnz00_2885;
BgL_nnz00_2885 = 
(BgL_ncz00_2884); 
{ /* Llib/unicode.scm 1485 */

{ /* Llib/unicode.scm 1486 */
 long BgL_mz00_2886;
BgL_mz00_2886 = 
(
(
(BgL_nz00_2871 & 31L) << 
(int)(6L)) | 
(63L & BgL_nnz00_2885)); 
if(
(BgL_mz00_2886>255L))
{ /* Llib/unicode.scm 1488 */
{ /* Llib/unicode.scm 1493 */
 long BgL_l4346z00_7238;
BgL_l4346z00_7238 = 
STRING_LENGTH(BgL_nstrz00_117); 
if(
BOUND_CHECK(BgL_wz00_2867, BgL_l4346z00_7238))
{ /* Llib/unicode.scm 1493 */
STRING_SET(BgL_nstrz00_117, BgL_wz00_2867, ((unsigned char)'.')); }  else 
{ 
 obj_t BgL_auxz00_13741;
BgL_auxz00_13741 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59379L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_117, 
(int)(BgL_l4346z00_7238), 
(int)(BgL_wz00_2867)); 
FAILURE(BgL_auxz00_13741,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1494 */
 long BgL_arg2690z00_2888; long BgL_arg2691z00_2889;
{ /* Llib/unicode.scm 1494 */
 long BgL_za71za7_5701;
{ /* Llib/unicode.scm 1494 */
 obj_t BgL_tmpz00_13747;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1494 */
BgL_tmpz00_13747 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13750;
BgL_auxz00_13750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59421L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13750,BFALSE,BFALSE);} 
BgL_za71za7_5701 = 
(long)CINT(BgL_tmpz00_13747); } 
BgL_arg2690z00_2888 = 
(BgL_za71za7_5701+2L); } 
BgL_arg2691z00_2889 = 
(BgL_wz00_2867+1L); 
{ 
 long BgL_wz00_13759; obj_t BgL_rz00_13757;
BgL_rz00_13757 = 
BINT(BgL_arg2690z00_2888); 
BgL_wz00_13759 = BgL_arg2691z00_2889; 
BgL_wz00_2867 = BgL_wz00_13759; 
BgL_rz00_2866 = BgL_rz00_13757; 
goto BgL_zc3z04anonymousza32676ze3z87_2868;} } }  else 
{ /* Llib/unicode.scm 1488 */
{ /* Llib/unicode.scm 1496 */
 unsigned char BgL_arg2692z00_2890;
BgL_arg2692z00_2890 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_mz00_2886); 
{ /* Llib/unicode.scm 1496 */
 long BgL_l4350z00_7242;
BgL_l4350z00_7242 = 
STRING_LENGTH(BgL_nstrz00_117); 
if(
BOUND_CHECK(BgL_wz00_2867, BgL_l4350z00_7242))
{ /* Llib/unicode.scm 1496 */
STRING_SET(BgL_nstrz00_117, BgL_wz00_2867, BgL_arg2692z00_2890); }  else 
{ 
 obj_t BgL_auxz00_13765;
BgL_auxz00_13765 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59461L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_117, 
(int)(BgL_l4350z00_7242), 
(int)(BgL_wz00_2867)); 
FAILURE(BgL_auxz00_13765,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1497 */
 long BgL_arg2693z00_2891; long BgL_arg2694z00_2892;
{ /* Llib/unicode.scm 1497 */
 long BgL_za71za7_5706;
{ /* Llib/unicode.scm 1497 */
 obj_t BgL_tmpz00_13771;
if(
INTEGERP(BgL_rz00_2866))
{ /* Llib/unicode.scm 1497 */
BgL_tmpz00_13771 = BgL_rz00_2866
; }  else 
{ 
 obj_t BgL_auxz00_13774;
BgL_auxz00_13774 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59517L), BGl_string4885z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2866); 
FAILURE(BgL_auxz00_13774,BFALSE,BFALSE);} 
BgL_za71za7_5706 = 
(long)CINT(BgL_tmpz00_13771); } 
BgL_arg2693z00_2891 = 
(BgL_za71za7_5706+2L); } 
BgL_arg2694z00_2892 = 
(BgL_wz00_2867+1L); 
{ 
 long BgL_wz00_13783; obj_t BgL_rz00_13781;
BgL_rz00_13781 = 
BINT(BgL_arg2693z00_2891); 
BgL_wz00_13783 = BgL_arg2694z00_2892; 
BgL_wz00_2867 = BgL_wz00_13783; 
BgL_rz00_2866 = BgL_rz00_13781; 
goto BgL_zc3z04anonymousza32676ze3z87_2868;} } } } } } } }  else 
{ 
 obj_t BgL_rz00_13784;
BgL_rz00_13784 = BgL_rz00_2866; 
BgL_rz00_2908 = BgL_rz00_13784; 
goto BgL_zc3z04anonymousza32705ze3z87_2909;} } } } } } } } } } } 

}



/* error-too-short~0 */
obj_t BGl_errorzd2toozd2shortze70ze7zz__unicodez00(int BgL_lenz00_6259, obj_t BgL_strz00_6258, obj_t BgL_rz00_2900)
{
{ /* Llib/unicode.scm 1436 */
{ /* Llib/unicode.scm 1436 */
 obj_t BgL_arg2701z00_2902;
{ /* Llib/unicode.scm 1436 */
 obj_t BgL_arg2702z00_2903;
{ /* Llib/unicode.scm 1436 */
 long BgL_arg2703z00_2904;
{ /* Llib/unicode.scm 1436 */
 long BgL_bz00_2906;
{ /* Llib/unicode.scm 1436 */
 long BgL_za71za7_5643;
{ /* Llib/unicode.scm 1436 */
 obj_t BgL_tmpz00_13786;
if(
INTEGERP(BgL_rz00_2900))
{ /* Llib/unicode.scm 1436 */
BgL_tmpz00_13786 = BgL_rz00_2900
; }  else 
{ 
 obj_t BgL_auxz00_13789;
BgL_auxz00_13789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(57798L), BGl_string4952z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2900); 
FAILURE(BgL_auxz00_13789,BFALSE,BFALSE);} 
BgL_za71za7_5643 = 
(long)CINT(BgL_tmpz00_13786); } 
BgL_bz00_2906 = 
(BgL_za71za7_5643-10L); } 
if(
(0L>BgL_bz00_2906))
{ /* Llib/unicode.scm 1436 */
BgL_arg2703z00_2904 = 0L; }  else 
{ /* Llib/unicode.scm 1436 */
BgL_arg2703z00_2904 = BgL_bz00_2906; } } 
BgL_arg2702z00_2903 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_6258, BgL_arg2703z00_2904, 
(long)(BgL_lenz00_6259)); } 
BgL_arg2701z00_2902 = 
string_for_read(BgL_arg2702z00_2903); } 
return 
BGl_errorz00zz__errorz00(BGl_string4951z00zz__unicodez00, BGl_string4953z00zz__unicodez00, BgL_arg2701z00_2902);} } 

}



/* table->8bits~0 */
obj_t BGl_tablezd2ze38bitsze70zd6zz__unicodez00(obj_t BgL_nstrz00_6263, obj_t BgL_tablez00_6262, obj_t BgL_strz00_6261, int BgL_lenz00_6260, obj_t BgL_subtablez00_2933, obj_t BgL_rz00_2934, long BgL_wz00_2935, long BgL_nz00_2936)
{
{ /* Llib/unicode.scm 1463 */
{ 
 obj_t BgL_rz00_2921;
{ /* Llib/unicode.scm 1451 */
 obj_t BgL_g1060z00_2938; long BgL_g1061z00_2939;
{ /* Llib/unicode.scm 1451 */
 obj_t BgL_auxz00_13801;
{ /* Llib/unicode.scm 1451 */
 bool_t BgL_test5947z00_13803;
if(
PAIRP(BgL_tablez00_6262))
{ /* Llib/unicode.scm 1451 */
BgL_test5947z00_13803 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1451 */
BgL_test5947z00_13803 = 
NULLP(BgL_tablez00_6262)
; } 
if(BgL_test5947z00_13803)
{ /* Llib/unicode.scm 1451 */
BgL_auxz00_13801 = BgL_tablez00_6262
; }  else 
{ 
 obj_t BgL_auxz00_13807;
BgL_auxz00_13807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58294L), BGl_string4956z00zz__unicodez00, BGl_string4884z00zz__unicodez00, BgL_tablez00_6262); 
FAILURE(BgL_auxz00_13807,BFALSE,BFALSE);} } 
BgL_g1060z00_2938 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
BINT(BgL_nz00_2936), BgL_auxz00_13801); } 
{ /* Llib/unicode.scm 1452 */
 long BgL_za71za7_5658;
{ /* Llib/unicode.scm 1452 */
 obj_t BgL_tmpz00_13812;
if(
INTEGERP(BgL_rz00_2934))
{ /* Llib/unicode.scm 1452 */
BgL_tmpz00_13812 = BgL_rz00_2934
; }  else 
{ 
 obj_t BgL_auxz00_13815;
BgL_auxz00_13815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58314L), BGl_string4956z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2934); 
FAILURE(BgL_auxz00_13815,BFALSE,BFALSE);} 
BgL_za71za7_5658 = 
(long)CINT(BgL_tmpz00_13812); } 
BgL_g1061z00_2939 = 
(BgL_za71za7_5658+1L); } 
{ 
 obj_t BgL_subtablez00_2941; long BgL_nrz00_2942;
BgL_subtablez00_2941 = BgL_g1060z00_2938; 
BgL_nrz00_2942 = BgL_g1061z00_2939; 
BgL_zc3z04anonymousza32726ze3z87_2943:
if(
CBOOL(BgL_subtablez00_2941))
{ /* Llib/unicode.scm 1456 */
 bool_t BgL_test5951z00_13823;
{ /* Llib/unicode.scm 1456 */
 obj_t BgL_tmpz00_13824;
{ /* Llib/unicode.scm 1456 */
 obj_t BgL_pairz00_5659;
if(
PAIRP(BgL_subtablez00_2941))
{ /* Llib/unicode.scm 1456 */
BgL_pairz00_5659 = BgL_subtablez00_2941; }  else 
{ 
 obj_t BgL_auxz00_13827;
BgL_auxz00_13827 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58394L), BGl_string4957z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_subtablez00_2941); 
FAILURE(BgL_auxz00_13827,BFALSE,BFALSE);} 
BgL_tmpz00_13824 = 
CDR(BgL_pairz00_5659); } 
BgL_test5951z00_13823 = 
CHARP(BgL_tmpz00_13824); } 
if(BgL_test5951z00_13823)
{ /* Llib/unicode.scm 1456 */
{ /* Llib/unicode.scm 1457 */
 obj_t BgL_arg2729z00_2946;
{ /* Llib/unicode.scm 1457 */
 obj_t BgL_pairz00_5660;
if(
PAIRP(BgL_subtablez00_2941))
{ /* Llib/unicode.scm 1457 */
BgL_pairz00_5660 = BgL_subtablez00_2941; }  else 
{ 
 obj_t BgL_auxz00_13835;
BgL_auxz00_13835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58436L), BGl_string4957z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_subtablez00_2941); 
FAILURE(BgL_auxz00_13835,BFALSE,BFALSE);} 
BgL_arg2729z00_2946 = 
CDR(BgL_pairz00_5660); } 
{ /* Llib/unicode.scm 1457 */
 unsigned char BgL_charz00_5663;
{ /* Llib/unicode.scm 1457 */
 obj_t BgL_tmpz00_13840;
if(
CHARP(BgL_arg2729z00_2946))
{ /* Llib/unicode.scm 1457 */
BgL_tmpz00_13840 = BgL_arg2729z00_2946
; }  else 
{ 
 obj_t BgL_auxz00_13843;
BgL_auxz00_13843 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58444L), BGl_string4957z00zz__unicodez00, BGl_string4925z00zz__unicodez00, BgL_arg2729z00_2946); 
FAILURE(BgL_auxz00_13843,BFALSE,BFALSE);} 
BgL_charz00_5663 = 
CCHAR(BgL_tmpz00_13840); } 
{ /* Llib/unicode.scm 1457 */
 long BgL_l4354z00_7246;
BgL_l4354z00_7246 = 
STRING_LENGTH(BgL_nstrz00_6263); 
if(
BOUND_CHECK(BgL_wz00_2935, BgL_l4354z00_7246))
{ /* Llib/unicode.scm 1457 */
STRING_SET(BgL_nstrz00_6263, BgL_wz00_2935, BgL_charz00_5663); }  else 
{ 
 obj_t BgL_auxz00_13852;
BgL_auxz00_13852 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58411L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_6263, 
(int)(BgL_l4354z00_7246), 
(int)(BgL_wz00_2935)); 
FAILURE(BgL_auxz00_13852,BFALSE,BFALSE);} } } } 
return 
BINT(BgL_nrz00_2942);}  else 
{ /* Llib/unicode.scm 1456 */
if(
(BgL_nrz00_2942==
(long)(BgL_lenz00_6260)))
{ /* Llib/unicode.scm 1459 */
return 
BGl_errorzd2toozd2shortze70ze7zz__unicodez00(BgL_lenz00_6260, BgL_strz00_6261, BgL_rz00_2934);}  else 
{ /* Llib/unicode.scm 1462 */
 long BgL_ncz00_2948;
{ /* Llib/unicode.scm 1462 */
 unsigned char BgL_tmpz00_13863;
{ /* Llib/unicode.scm 1462 */
 long BgL_l4358z00_7250;
BgL_l4358z00_7250 = 
STRING_LENGTH(BgL_strz00_6261); 
if(
BOUND_CHECK(BgL_nrz00_2942, BgL_l4358z00_7250))
{ /* Llib/unicode.scm 1462 */
BgL_tmpz00_13863 = 
STRING_REF(BgL_strz00_6261, BgL_nrz00_2942)
; }  else 
{ 
 obj_t BgL_auxz00_13868;
BgL_auxz00_13868 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58545L), BGl_string4897z00zz__unicodez00, BgL_strz00_6261, 
(int)(BgL_l4358z00_7250), 
(int)(BgL_nrz00_2942)); 
FAILURE(BgL_auxz00_13868,BFALSE,BFALSE);} } 
BgL_ncz00_2948 = 
(BgL_tmpz00_13863); } 
{ /* Llib/unicode.scm 1463 */
 obj_t BgL_arg2731z00_2949; long BgL_arg2733z00_2950;
{ /* Llib/unicode.scm 1463 */
 obj_t BgL_auxz00_13875;
{ /* Llib/unicode.scm 1463 */
 obj_t BgL_pairz00_5669;
if(
PAIRP(BgL_subtablez00_2941))
{ /* Llib/unicode.scm 1463 */
BgL_pairz00_5669 = BgL_subtablez00_2941; }  else 
{ 
 obj_t BgL_auxz00_13879;
BgL_auxz00_13879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58590L), BGl_string4957z00zz__unicodez00, BGl_string4882z00zz__unicodez00, BgL_subtablez00_2941); 
FAILURE(BgL_auxz00_13879,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 1463 */
 obj_t BgL_aux4713z00_7605;
BgL_aux4713z00_7605 = 
CDR(BgL_pairz00_5669); 
{ /* Llib/unicode.scm 1463 */
 bool_t BgL_test5959z00_13884;
if(
PAIRP(BgL_aux4713z00_7605))
{ /* Llib/unicode.scm 1463 */
BgL_test5959z00_13884 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1463 */
BgL_test5959z00_13884 = 
NULLP(BgL_aux4713z00_7605)
; } 
if(BgL_test5959z00_13884)
{ /* Llib/unicode.scm 1463 */
BgL_auxz00_13875 = BgL_aux4713z00_7605
; }  else 
{ 
 obj_t BgL_auxz00_13888;
BgL_auxz00_13888 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58585L), BGl_string4957z00zz__unicodez00, BGl_string4884z00zz__unicodez00, BgL_aux4713z00_7605); 
FAILURE(BgL_auxz00_13888,BFALSE,BFALSE);} } } } 
BgL_arg2731z00_2949 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
BINT(BgL_ncz00_2948), BgL_auxz00_13875); } 
BgL_arg2733z00_2950 = 
(BgL_nrz00_2942+1L); 
{ 
 long BgL_nrz00_13895; obj_t BgL_subtablez00_13894;
BgL_subtablez00_13894 = BgL_arg2731z00_2949; 
BgL_nrz00_13895 = BgL_arg2733z00_2950; 
BgL_nrz00_2942 = BgL_nrz00_13895; 
BgL_subtablez00_2941 = BgL_subtablez00_13894; 
goto BgL_zc3z04anonymousza32726ze3z87_2943;} } } } }  else 
{ /* Llib/unicode.scm 1454 */
BgL_rz00_2921 = BgL_rz00_2934; 
{ /* Llib/unicode.scm 1447 */
 obj_t BgL_arg2717z00_2923; obj_t BgL_arg2719z00_2924;
{ /* Llib/unicode.scm 1447 */
 obj_t BgL_arg2721z00_2925;
{ /* Ieee/fixnum.scm 1001 */

{ /* Ieee/fixnum.scm 1001 */
 long BgL_auxz00_13896;
{ /* Llib/unicode.scm 1447 */
 obj_t BgL_tmpz00_13897;
if(
INTEGERP(BgL_rz00_2921))
{ /* Llib/unicode.scm 1447 */
BgL_tmpz00_13897 = BgL_rz00_2921
; }  else 
{ 
 obj_t BgL_auxz00_13900;
BgL_auxz00_13900 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58150L), BGl_string4954z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2921); 
FAILURE(BgL_auxz00_13900,BFALSE,BFALSE);} 
BgL_auxz00_13896 = 
(long)CINT(BgL_tmpz00_13897); } 
BgL_arg2721z00_2925 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_auxz00_13896, 10L); } } 
BgL_arg2717z00_2923 = 
string_append(BGl_string4955z00zz__unicodez00, BgL_arg2721z00_2925); } 
{ /* Llib/unicode.scm 1448 */
 obj_t BgL_arg2722z00_2928;
{ /* Llib/unicode.scm 1448 */
 int BgL_arg2723z00_2929;
{ /* Llib/unicode.scm 1448 */
 long BgL_bz00_2931;
{ /* Llib/unicode.scm 1448 */
 long BgL_za71za7_5654;
{ /* Llib/unicode.scm 1448 */
 obj_t BgL_tmpz00_13907;
if(
INTEGERP(BgL_rz00_2921))
{ /* Llib/unicode.scm 1448 */
BgL_tmpz00_13907 = BgL_rz00_2921
; }  else 
{ 
 obj_t BgL_auxz00_13910;
BgL_auxz00_13910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58206L), BGl_string4954z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2921); 
FAILURE(BgL_auxz00_13910,BFALSE,BFALSE);} 
BgL_za71za7_5654 = 
(long)CINT(BgL_tmpz00_13907); } 
BgL_bz00_2931 = 
(BgL_za71za7_5654+10L); } 
if(
(
(long)(BgL_lenz00_6260)<BgL_bz00_2931))
{ /* Llib/unicode.scm 1448 */
BgL_arg2723z00_2929 = BgL_lenz00_6260; }  else 
{ /* Llib/unicode.scm 1448 */
BgL_arg2723z00_2929 = 
(int)(BgL_bz00_2931); } } 
{ /* Llib/unicode.scm 1448 */
 long BgL_auxz00_13920;
{ /* Llib/unicode.scm 1448 */
 obj_t BgL_tmpz00_13921;
if(
INTEGERP(BgL_rz00_2921))
{ /* Llib/unicode.scm 1448 */
BgL_tmpz00_13921 = BgL_rz00_2921
; }  else 
{ 
 obj_t BgL_auxz00_13924;
BgL_auxz00_13924 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(58173L), BGl_string4954z00zz__unicodez00, BGl_string4849z00zz__unicodez00, BgL_rz00_2921); 
FAILURE(BgL_auxz00_13924,BFALSE,BFALSE);} 
BgL_auxz00_13920 = 
(long)CINT(BgL_tmpz00_13921); } 
BgL_arg2722z00_2928 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_6261, BgL_auxz00_13920, 
(long)(BgL_arg2723z00_2929)); } } 
BgL_arg2719z00_2924 = 
string_for_read(BgL_arg2722z00_2928); } 
return 
BGl_errorz00zz__errorz00(BGl_string4951z00zz__unicodez00, BgL_arg2717z00_2923, BgL_arg2719z00_2924);} } } } } } 

}



/* utf8->8bits */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze38bitsz31zz__unicodez00(obj_t BgL_strz00_121, obj_t BgL_tablez00_122)
{
{ /* Llib/unicode.scm 1504 */
{ /* Llib/unicode.scm 1505 */
 long BgL_lenz00_2959;
BgL_lenz00_2959 = 
STRING_LENGTH(BgL_strz00_121); 
{ /* Llib/unicode.scm 1505 */
 long BgL_nlenz00_2960;
BgL_nlenz00_2960 = 
BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_121, BgL_lenz00_2959); 
{ /* Llib/unicode.scm 1506 */

if(
(BgL_lenz00_2959==BgL_nlenz00_2960))
{ /* Llib/unicode.scm 1507 */
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_121);}  else 
{ /* Llib/unicode.scm 1509 */
 obj_t BgL_arg2739z00_2962;
BgL_arg2739z00_2962 = 
make_string_sans_fill(BgL_nlenz00_2960); 
{ /* Llib/unicode.scm 1509 */
 obj_t BgL_aux4715z00_7607;
BgL_aux4715z00_7607 = 
BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(BgL_arg2739z00_2962, BgL_strz00_121, 
(int)(BgL_lenz00_2959), BgL_tablez00_122); 
if(
STRINGP(BgL_aux4715z00_7607))
{ /* Llib/unicode.scm 1509 */
return BgL_aux4715z00_7607;}  else 
{ 
 obj_t BgL_auxz00_13943;
BgL_auxz00_13943 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59950L), BGl_string4951z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4715z00_7607); 
FAILURE(BgL_auxz00_13943,BFALSE,BFALSE);} } } } } } } 

}



/* &utf8->8bits */
obj_t BGl_z62utf8zd2ze38bitsz53zz__unicodez00(obj_t BgL_envz00_6220, obj_t BgL_strz00_6221, obj_t BgL_tablez00_6222)
{
{ /* Llib/unicode.scm 1504 */
{ /* Llib/unicode.scm 1505 */
 obj_t BgL_auxz00_13947;
if(
STRINGP(BgL_strz00_6221))
{ /* Llib/unicode.scm 1505 */
BgL_auxz00_13947 = BgL_strz00_6221
; }  else 
{ 
 obj_t BgL_auxz00_13950;
BgL_auxz00_13950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59828L), BGl_string4958z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6221); 
FAILURE(BgL_auxz00_13950,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze38bitsz31zz__unicodez00(BgL_auxz00_13947, BgL_tablez00_6222);} } 

}



/* utf8->8bits! */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze38bitsz12z23zz__unicodez00(obj_t BgL_strz00_123, obj_t BgL_tablez00_124)
{
{ /* Llib/unicode.scm 1514 */
{ /* Llib/unicode.scm 1515 */
 long BgL_lenz00_2963;
BgL_lenz00_2963 = 
STRING_LENGTH(BgL_strz00_123); 
{ /* Llib/unicode.scm 1515 */
 long BgL_nlenz00_2964;
BgL_nlenz00_2964 = 
BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_123, BgL_lenz00_2963); 
{ /* Llib/unicode.scm 1516 */

if(
(BgL_lenz00_2963==BgL_nlenz00_2964))
{ /* Llib/unicode.scm 1517 */
return BgL_strz00_123;}  else 
{ /* Llib/unicode.scm 1519 */
 obj_t BgL_arg2741z00_2966;
BgL_arg2741z00_2966 = 
make_string_sans_fill(BgL_nlenz00_2964); 
{ /* Llib/unicode.scm 1519 */
 obj_t BgL_aux4719z00_7611;
BgL_aux4719z00_7611 = 
BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(BgL_arg2741z00_2966, BgL_strz00_123, 
(int)(BgL_lenz00_2963), BgL_tablez00_124); 
if(
STRINGP(BgL_aux4719z00_7611))
{ /* Llib/unicode.scm 1519 */
return BgL_aux4719z00_7611;}  else 
{ 
 obj_t BgL_auxz00_13964;
BgL_auxz00_13964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(60382L), BGl_string4959z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4719z00_7611); 
FAILURE(BgL_auxz00_13964,BFALSE,BFALSE);} } } } } } } 

}



/* &utf8->8bits! */
obj_t BGl_z62utf8zd2ze38bitsz12z41zz__unicodez00(obj_t BgL_envz00_6223, obj_t BgL_strz00_6224, obj_t BgL_tablez00_6225)
{
{ /* Llib/unicode.scm 1514 */
{ /* Llib/unicode.scm 1515 */
 obj_t BgL_auxz00_13968;
if(
STRINGP(BgL_strz00_6224))
{ /* Llib/unicode.scm 1515 */
BgL_auxz00_13968 = BgL_strz00_6224
; }  else 
{ 
 obj_t BgL_auxz00_13971;
BgL_auxz00_13971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(60274L), BGl_string4960z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6224); 
FAILURE(BgL_auxz00_13971,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze38bitsz12z23zz__unicodez00(BgL_auxz00_13968, BgL_tablez00_6225);} } 

}



/* utf8->iso-latin */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinze3zz__unicodez00(obj_t BgL_strz00_125)
{
{ /* Llib/unicode.scm 1524 */
{ /* Llib/unicode.scm 1525 */
 obj_t BgL_res3372z00_5723;
{ /* Llib/unicode.scm 1525 */
 obj_t BgL_tablez00_5715;
BgL_tablez00_5715 = BGl_list4775z00zz__unicodez00; 
{ /* Llib/unicode.scm 1505 */
 long BgL_lenz00_5716;
BgL_lenz00_5716 = 
STRING_LENGTH(BgL_strz00_125); 
{ /* Llib/unicode.scm 1505 */
 long BgL_nlenz00_5718;
BgL_nlenz00_5718 = 
BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_125, BgL_lenz00_5716); 
{ /* Llib/unicode.scm 1506 */

if(
(BgL_lenz00_5716==BgL_nlenz00_5718))
{ /* Llib/unicode.scm 1507 */
BgL_res3372z00_5723 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_125); }  else 
{ /* Llib/unicode.scm 1509 */
 obj_t BgL_arg2739z00_5722;
BgL_arg2739z00_5722 = 
make_string_sans_fill(BgL_nlenz00_5718); 
{ /* Llib/unicode.scm 1509 */
 obj_t BgL_aux4723z00_7615;
BgL_aux4723z00_7615 = 
BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(BgL_arg2739z00_5722, BgL_strz00_125, 
(int)(BgL_lenz00_5716), BgL_tablez00_5715); 
if(
STRINGP(BgL_aux4723z00_7615))
{ /* Llib/unicode.scm 1509 */
BgL_res3372z00_5723 = BgL_aux4723z00_7615; }  else 
{ 
 obj_t BgL_auxz00_13986;
BgL_auxz00_13986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59950L), BGl_string4961z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4723z00_7615); 
FAILURE(BgL_auxz00_13986,BFALSE,BFALSE);} } } } } } } 
return BgL_res3372z00_5723;} } 

}



/* &utf8->iso-latin */
obj_t BGl_z62utf8zd2ze3isozd2latinz81zz__unicodez00(obj_t BgL_envz00_6226, obj_t BgL_strz00_6227)
{
{ /* Llib/unicode.scm 1524 */
{ /* Llib/unicode.scm 1525 */
 obj_t BgL_auxz00_13990;
if(
STRINGP(BgL_strz00_6227))
{ /* Llib/unicode.scm 1525 */
BgL_auxz00_13990 = BgL_strz00_6227
; }  else 
{ 
 obj_t BgL_auxz00_13993;
BgL_auxz00_13993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(60703L), BGl_string4962z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6227); 
FAILURE(BgL_auxz00_13993,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze3isozd2latinze3zz__unicodez00(BgL_auxz00_13990);} } 

}



/* utf8->iso-latin! */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinz12zf1zz__unicodez00(obj_t BgL_strz00_126)
{
{ /* Llib/unicode.scm 1530 */
{ /* Llib/unicode.scm 1531 */
 obj_t BgL_res3373z00_5733;
{ /* Llib/unicode.scm 1531 */
 obj_t BgL_tablez00_5725;
BgL_tablez00_5725 = BGl_list4775z00zz__unicodez00; 
{ /* Llib/unicode.scm 1515 */
 long BgL_lenz00_5726;
BgL_lenz00_5726 = 
STRING_LENGTH(BgL_strz00_126); 
{ /* Llib/unicode.scm 1515 */
 long BgL_nlenz00_5728;
BgL_nlenz00_5728 = 
BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_126, BgL_lenz00_5726); 
{ /* Llib/unicode.scm 1516 */

if(
(BgL_lenz00_5726==BgL_nlenz00_5728))
{ /* Llib/unicode.scm 1517 */
BgL_res3373z00_5733 = BgL_strz00_126; }  else 
{ /* Llib/unicode.scm 1519 */
 obj_t BgL_arg2741z00_5732;
BgL_arg2741z00_5732 = 
make_string_sans_fill(BgL_nlenz00_5728); 
{ /* Llib/unicode.scm 1519 */
 obj_t BgL_aux4727z00_7619;
BgL_aux4727z00_7619 = 
BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(BgL_arg2741z00_5732, BgL_strz00_126, 
(int)(BgL_lenz00_5726), BgL_tablez00_5725); 
if(
STRINGP(BgL_aux4727z00_7619))
{ /* Llib/unicode.scm 1519 */
BgL_res3373z00_5733 = BgL_aux4727z00_7619; }  else 
{ 
 obj_t BgL_auxz00_14007;
BgL_auxz00_14007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(60382L), BGl_string4963z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4727z00_7619); 
FAILURE(BgL_auxz00_14007,BFALSE,BFALSE);} } } } } } } 
return BgL_res3373z00_5733;} } 

}



/* &utf8->iso-latin! */
obj_t BGl_z62utf8zd2ze3isozd2latinz12z93zz__unicodez00(obj_t BgL_envz00_6228, obj_t BgL_strz00_6229)
{
{ /* Llib/unicode.scm 1530 */
{ /* Llib/unicode.scm 1531 */
 obj_t BgL_auxz00_14011;
if(
STRINGP(BgL_strz00_6229))
{ /* Llib/unicode.scm 1531 */
BgL_auxz00_14011 = BgL_strz00_6229
; }  else 
{ 
 obj_t BgL_auxz00_14014;
BgL_auxz00_14014 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(60989L), BGl_string4964z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6229); 
FAILURE(BgL_auxz00_14014,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze3isozd2latinz12zf1zz__unicodez00(BgL_auxz00_14011);} } 

}



/* utf8->iso-latin-15 */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinzd215z31zz__unicodez00(obj_t BgL_strz00_127)
{
{ /* Llib/unicode.scm 1536 */
{ /* Llib/unicode.scm 1537 */
 obj_t BgL_res3374z00_5743;
{ /* Llib/unicode.scm 1537 */
 obj_t BgL_tablez00_5735;
BgL_tablez00_5735 = BGl_list4802z00zz__unicodez00; 
{ /* Llib/unicode.scm 1505 */
 long BgL_lenz00_5736;
BgL_lenz00_5736 = 
STRING_LENGTH(BgL_strz00_127); 
{ /* Llib/unicode.scm 1505 */
 long BgL_nlenz00_5738;
BgL_nlenz00_5738 = 
BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_127, BgL_lenz00_5736); 
{ /* Llib/unicode.scm 1506 */

if(
(BgL_lenz00_5736==BgL_nlenz00_5738))
{ /* Llib/unicode.scm 1507 */
BgL_res3374z00_5743 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_127); }  else 
{ /* Llib/unicode.scm 1509 */
 obj_t BgL_arg2739z00_5742;
BgL_arg2739z00_5742 = 
make_string_sans_fill(BgL_nlenz00_5738); 
{ /* Llib/unicode.scm 1509 */
 obj_t BgL_aux4731z00_7623;
BgL_aux4731z00_7623 = 
BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(BgL_arg2739z00_5742, BgL_strz00_127, 
(int)(BgL_lenz00_5736), BgL_tablez00_5735); 
if(
STRINGP(BgL_aux4731z00_7623))
{ /* Llib/unicode.scm 1509 */
BgL_res3374z00_5743 = BgL_aux4731z00_7623; }  else 
{ 
 obj_t BgL_auxz00_14029;
BgL_auxz00_14029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(59950L), BGl_string4965z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4731z00_7623); 
FAILURE(BgL_auxz00_14029,BFALSE,BFALSE);} } } } } } } 
return BgL_res3374z00_5743;} } 

}



/* &utf8->iso-latin-15 */
obj_t BGl_z62utf8zd2ze3isozd2latinzd215z53zz__unicodez00(obj_t BgL_envz00_6230, obj_t BgL_strz00_6231)
{
{ /* Llib/unicode.scm 1536 */
{ /* Llib/unicode.scm 1537 */
 obj_t BgL_auxz00_14033;
if(
STRINGP(BgL_strz00_6231))
{ /* Llib/unicode.scm 1537 */
BgL_auxz00_14033 = BgL_strz00_6231
; }  else 
{ 
 obj_t BgL_auxz00_14036;
BgL_auxz00_14036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(61278L), BGl_string4966z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6231); 
FAILURE(BgL_auxz00_14036,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze3isozd2latinzd215z31zz__unicodez00(BgL_auxz00_14033);} } 

}



/* utf8->iso-latin-15! */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3isozd2latinzd215z12z23zz__unicodez00(obj_t BgL_strz00_128)
{
{ /* Llib/unicode.scm 1542 */
{ /* Llib/unicode.scm 1543 */
 obj_t BgL_res3375z00_5753;
{ /* Llib/unicode.scm 1543 */
 obj_t BgL_tablez00_5745;
BgL_tablez00_5745 = BGl_list4802z00zz__unicodez00; 
{ /* Llib/unicode.scm 1515 */
 long BgL_lenz00_5746;
BgL_lenz00_5746 = 
STRING_LENGTH(BgL_strz00_128); 
{ /* Llib/unicode.scm 1515 */
 long BgL_nlenz00_5748;
BgL_nlenz00_5748 = 
BGl_utf8zd2ze38bitszd2lengthze3zz__unicodez00(BgL_strz00_128, BgL_lenz00_5746); 
{ /* Llib/unicode.scm 1516 */

if(
(BgL_lenz00_5746==BgL_nlenz00_5748))
{ /* Llib/unicode.scm 1517 */
BgL_res3375z00_5753 = BgL_strz00_128; }  else 
{ /* Llib/unicode.scm 1519 */
 obj_t BgL_arg2741z00_5752;
BgL_arg2741z00_5752 = 
make_string_sans_fill(BgL_nlenz00_5748); 
{ /* Llib/unicode.scm 1519 */
 obj_t BgL_aux4735z00_7627;
BgL_aux4735z00_7627 = 
BGl_utf8zd2ze38bitszd2fillz12zf1zz__unicodez00(BgL_arg2741z00_5752, BgL_strz00_128, 
(int)(BgL_lenz00_5746), BgL_tablez00_5745); 
if(
STRINGP(BgL_aux4735z00_7627))
{ /* Llib/unicode.scm 1519 */
BgL_res3375z00_5753 = BgL_aux4735z00_7627; }  else 
{ 
 obj_t BgL_auxz00_14050;
BgL_auxz00_14050 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(60382L), BGl_string4967z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_aux4735z00_7627); 
FAILURE(BgL_auxz00_14050,BFALSE,BFALSE);} } } } } } } 
return BgL_res3375z00_5753;} } 

}



/* &utf8->iso-latin-15! */
obj_t BGl_z62utf8zd2ze3isozd2latinzd215z12z41zz__unicodez00(obj_t BgL_envz00_6232, obj_t BgL_strz00_6233)
{
{ /* Llib/unicode.scm 1542 */
{ /* Llib/unicode.scm 1543 */
 obj_t BgL_auxz00_14054;
if(
STRINGP(BgL_strz00_6233))
{ /* Llib/unicode.scm 1543 */
BgL_auxz00_14054 = BgL_strz00_6233
; }  else 
{ 
 obj_t BgL_auxz00_14057;
BgL_auxz00_14057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(61576L), BGl_string4968z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6233); 
FAILURE(BgL_auxz00_14057,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze3isozd2latinzd215z12z23zz__unicodez00(BgL_auxz00_14054);} } 

}



/* utf8->cp1252 */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3cp1252z31zz__unicodez00(obj_t BgL_strz00_129)
{
{ /* Llib/unicode.scm 1548 */
if(
CBOOL(BGl_cp1252zd2invzd2zz__unicodez00))
{ /* Llib/unicode.scm 1549 */BFALSE; }  else 
{ /* Llib/unicode.scm 1549 */
BGl_cp1252zd2invzd2zz__unicodez00 = 
BGl_inversezd2utf8zd2tablez00zz__unicodez00(BGl_vector4814z00zz__unicodez00); } 
return 
BGl_utf8zd2ze38bitsz31zz__unicodez00(BgL_strz00_129, BGl_cp1252zd2invzd2zz__unicodez00);} 

}



/* &utf8->cp1252 */
obj_t BGl_z62utf8zd2ze3cp1252z53zz__unicodez00(obj_t BgL_envz00_6234, obj_t BgL_strz00_6235)
{
{ /* Llib/unicode.scm 1548 */
{ /* Llib/unicode.scm 1549 */
 obj_t BgL_auxz00_14066;
if(
STRINGP(BgL_strz00_6235))
{ /* Llib/unicode.scm 1549 */
BgL_auxz00_14066 = BgL_strz00_6235
; }  else 
{ 
 obj_t BgL_auxz00_14069;
BgL_auxz00_14069 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(61932L), BGl_string4969z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6235); 
FAILURE(BgL_auxz00_14069,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze3cp1252z31zz__unicodez00(BgL_auxz00_14066);} } 

}



/* utf8->cp1252! */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2ze3cp1252z12z23zz__unicodez00(obj_t BgL_strz00_130)
{
{ /* Llib/unicode.scm 1555 */
if(
CBOOL(BGl_cp1252zd2invzd2zz__unicodez00))
{ /* Llib/unicode.scm 1556 */BFALSE; }  else 
{ /* Llib/unicode.scm 1556 */
BGl_cp1252zd2invzd2zz__unicodez00 = 
BGl_inversezd2utf8zd2tablez00zz__unicodez00(BGl_vector4814z00zz__unicodez00); } 
return 
BGl_utf8zd2ze38bitsz12z23zz__unicodez00(BgL_strz00_130, BGl_cp1252zd2invzd2zz__unicodez00);} 

}



/* &utf8->cp1252! */
obj_t BGl_z62utf8zd2ze3cp1252z12z41zz__unicodez00(obj_t BgL_envz00_6236, obj_t BgL_strz00_6237)
{
{ /* Llib/unicode.scm 1555 */
{ /* Llib/unicode.scm 1556 */
 obj_t BgL_auxz00_14078;
if(
STRINGP(BgL_strz00_6237))
{ /* Llib/unicode.scm 1556 */
BgL_auxz00_14078 = BgL_strz00_6237
; }  else 
{ 
 obj_t BgL_auxz00_14081;
BgL_auxz00_14081 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(62285L), BGl_string4970z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6237); 
FAILURE(BgL_auxz00_14081,BFALSE,BFALSE);} 
return 
BGl_utf8zd2ze3cp1252z12z23zz__unicodez00(BgL_auxz00_14078);} } 

}



/* 8bits->utf8-length */
long BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(obj_t BgL_strz00_131, long BgL_lenz00_132, obj_t BgL_tablez00_133)
{
{ /* Llib/unicode.scm 1562 */
{ 
 long BgL_cz00_2984;
{ 
 long BgL_siza7eza7_2969; long BgL_iz00_2970;
BgL_siza7eza7_2969 = 0L; 
BgL_iz00_2970 = 0L; 
BgL_zc3z04anonymousza32742ze3z87_2971:
if(
(BgL_iz00_2970==BgL_lenz00_132))
{ /* Llib/unicode.scm 1572 */
return BgL_siza7eza7_2969;}  else 
{ /* Llib/unicode.scm 1574 */
 long BgL_cz00_2973;
{ /* Llib/unicode.scm 1574 */
 unsigned char BgL_tmpz00_14088;
{ /* Llib/unicode.scm 1574 */
 long BgL_l4366z00_7258;
BgL_l4366z00_7258 = 
STRING_LENGTH(BgL_strz00_131); 
if(
BOUND_CHECK(BgL_iz00_2970, BgL_l4366z00_7258))
{ /* Llib/unicode.scm 1574 */
BgL_tmpz00_14088 = 
STRING_REF(BgL_strz00_131, BgL_iz00_2970)
; }  else 
{ 
 obj_t BgL_auxz00_14093;
BgL_auxz00_14093 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(62838L), BGl_string4897z00zz__unicodez00, BgL_strz00_131, 
(int)(BgL_l4366z00_7258), 
(int)(BgL_iz00_2970)); 
FAILURE(BgL_auxz00_14093,BFALSE,BFALSE);} } 
BgL_cz00_2973 = 
(BgL_tmpz00_14088); } 
if(
(BgL_cz00_2973>=128L))
{ /* Llib/unicode.scm 1575 */
if(
CBOOL(BgL_tablez00_133))
{ 
 long BgL_iz00_14142; long BgL_siza7eza7_14104;
{ /* Llib/unicode.scm 1577 */
 long BgL_tmpz00_14105;
BgL_cz00_2984 = BgL_cz00_2973; 
{ /* Llib/unicode.scm 1565 */
 long BgL_vz00_2986;
BgL_vz00_2986 = 
(BgL_cz00_2984-128L); 
{ /* Llib/unicode.scm 1566 */
 bool_t BgL_test5991z00_14107;
{ /* Llib/unicode.scm 1566 */
 long BgL_arg2759z00_2990;
{ /* Llib/unicode.scm 1566 */
 obj_t BgL_vectorz00_5755;
if(
VECTORP(BgL_tablez00_133))
{ /* Llib/unicode.scm 1566 */
BgL_vectorz00_5755 = BgL_tablez00_133; }  else 
{ 
 obj_t BgL_auxz00_14110;
BgL_auxz00_14110 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(62678L), BGl_string4971z00zz__unicodez00, BGl_string4902z00zz__unicodez00, BgL_tablez00_133); 
FAILURE(BgL_auxz00_14110,BFALSE,BFALSE);} 
BgL_arg2759z00_2990 = 
VECTOR_LENGTH(BgL_vectorz00_5755); } 
BgL_test5991z00_14107 = 
(BgL_vz00_2986<BgL_arg2759z00_2990); } 
if(BgL_test5991z00_14107)
{ /* Llib/unicode.scm 1567 */
 obj_t BgL_arg2758z00_2989;
{ /* Llib/unicode.scm 1567 */
 obj_t BgL_vectorz00_5758;
if(
VECTORP(BgL_tablez00_133))
{ /* Llib/unicode.scm 1567 */
BgL_vectorz00_5758 = BgL_tablez00_133; }  else 
{ 
 obj_t BgL_auxz00_14118;
BgL_auxz00_14118 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(62719L), BGl_string4971z00zz__unicodez00, BGl_string4902z00zz__unicodez00, BgL_tablez00_133); 
FAILURE(BgL_auxz00_14118,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 1567 */
 bool_t BgL_test5994z00_14122;
{ /* Llib/unicode.scm 1567 */
 long BgL_tmpz00_14123;
BgL_tmpz00_14123 = 
VECTOR_LENGTH(BgL_vectorz00_5758); 
BgL_test5994z00_14122 = 
BOUND_CHECK(BgL_vz00_2986, BgL_tmpz00_14123); } 
if(BgL_test5994z00_14122)
{ /* Llib/unicode.scm 1567 */
BgL_arg2758z00_2989 = 
VECTOR_REF(BgL_vectorz00_5758,BgL_vz00_2986); }  else 
{ 
 obj_t BgL_auxz00_14127;
BgL_auxz00_14127 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(62707L), BGl_string4894z00zz__unicodez00, BgL_vectorz00_5758, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_5758)), 
(int)(BgL_vz00_2986)); 
FAILURE(BgL_auxz00_14127,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1567 */
 obj_t BgL_stringz00_5760;
if(
STRINGP(BgL_arg2758z00_2989))
{ /* Llib/unicode.scm 1567 */
BgL_stringz00_5760 = BgL_arg2758z00_2989; }  else 
{ 
 obj_t BgL_auxz00_14136;
BgL_auxz00_14136 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(62692L), BGl_string4971z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_arg2758z00_2989); 
FAILURE(BgL_auxz00_14136,BFALSE,BFALSE);} 
BgL_tmpz00_14105 = 
STRING_LENGTH(BgL_stringz00_5760); } }  else 
{ /* Llib/unicode.scm 1566 */
BgL_tmpz00_14105 = 2L
; } } } 
BgL_siza7eza7_14104 = 
(BgL_siza7eza7_2969+BgL_tmpz00_14105); } 
BgL_iz00_14142 = 
(BgL_iz00_2970+1L); 
BgL_iz00_2970 = BgL_iz00_14142; 
BgL_siza7eza7_2969 = BgL_siza7eza7_14104; 
goto BgL_zc3z04anonymousza32742ze3z87_2971;}  else 
{ 
 long BgL_iz00_14146; long BgL_siza7eza7_14144;
BgL_siza7eza7_14144 = 
(BgL_siza7eza7_2969+2L); 
BgL_iz00_14146 = 
(BgL_iz00_2970+1L); 
BgL_iz00_2970 = BgL_iz00_14146; 
BgL_siza7eza7_2969 = BgL_siza7eza7_14144; 
goto BgL_zc3z04anonymousza32742ze3z87_2971;} }  else 
{ 
 long BgL_iz00_14150; long BgL_siza7eza7_14148;
BgL_siza7eza7_14148 = 
(BgL_siza7eza7_2969+1L); 
BgL_iz00_14150 = 
(BgL_iz00_2970+1L); 
BgL_iz00_2970 = BgL_iz00_14150; 
BgL_siza7eza7_2969 = BgL_siza7eza7_14148; 
goto BgL_zc3z04anonymousza32742ze3z87_2971;} } } } } 

}



/* 8bits->utf8-fill! */
obj_t BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(obj_t BgL_nstrz00_134, obj_t BgL_strz00_135, long BgL_lenz00_136, obj_t BgL_tablez00_137)
{
{ /* Llib/unicode.scm 1584 */
{ 
 long BgL_rz00_2993; long BgL_wz00_2994;
BgL_rz00_2993 = 0L; 
BgL_wz00_2994 = 0L; 
BgL_zc3z04anonymousza32760ze3z87_2995:
if(
(BgL_rz00_2993==BgL_lenz00_136))
{ /* Llib/unicode.scm 1587 */
return BgL_nstrz00_134;}  else 
{ /* Llib/unicode.scm 1589 */
 long BgL_cz00_2997;
{ /* Llib/unicode.scm 1589 */
 unsigned char BgL_tmpz00_14154;
{ /* Llib/unicode.scm 1589 */
 long BgL_l4370z00_7262;
BgL_l4370z00_7262 = 
STRING_LENGTH(BgL_strz00_135); 
if(
BOUND_CHECK(BgL_rz00_2993, BgL_l4370z00_7262))
{ /* Llib/unicode.scm 1589 */
BgL_tmpz00_14154 = 
STRING_REF(BgL_strz00_135, BgL_rz00_2993)
; }  else 
{ 
 obj_t BgL_auxz00_14159;
BgL_auxz00_14159 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63390L), BGl_string4897z00zz__unicodez00, BgL_strz00_135, 
(int)(BgL_l4370z00_7262), 
(int)(BgL_rz00_2993)); 
FAILURE(BgL_auxz00_14159,BFALSE,BFALSE);} } 
BgL_cz00_2997 = 
(BgL_tmpz00_14154); } 
if(
(BgL_cz00_2997>=192L))
{ /* Llib/unicode.scm 1591 */
{ /* Llib/unicode.scm 1592 */
 long BgL_l4374z00_7266;
BgL_l4374z00_7266 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_wz00_2994, BgL_l4374z00_7266))
{ /* Llib/unicode.scm 1592 */
STRING_SET(BgL_nstrz00_134, BgL_wz00_2994, ((unsigned char)195)); }  else 
{ 
 obj_t BgL_auxz00_14172;
BgL_auxz00_14172 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63447L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4374z00_7266), 
(int)(BgL_wz00_2994)); 
FAILURE(BgL_auxz00_14172,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1593 */
 long BgL_arg2764z00_2999; unsigned char BgL_arg2765z00_3000;
BgL_arg2764z00_2999 = 
(BgL_wz00_2994+1L); 
BgL_arg2765z00_3000 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(BgL_cz00_2997-64L)); 
{ /* Llib/unicode.scm 1593 */
 long BgL_l4378z00_7270;
BgL_l4378z00_7270 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_arg2764z00_2999, BgL_l4378z00_7270))
{ /* Llib/unicode.scm 1593 */
STRING_SET(BgL_nstrz00_134, BgL_arg2764z00_2999, BgL_arg2765z00_3000); }  else 
{ 
 obj_t BgL_auxz00_14185;
BgL_auxz00_14185 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63495L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4378z00_7270), 
(int)(BgL_arg2764z00_2999)); 
FAILURE(BgL_auxz00_14185,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_14193; long BgL_rz00_14191;
BgL_rz00_14191 = 
(BgL_rz00_2993+1L); 
BgL_wz00_14193 = 
(BgL_wz00_2994+2L); 
BgL_wz00_2994 = BgL_wz00_14193; 
BgL_rz00_2993 = BgL_rz00_14191; 
goto BgL_zc3z04anonymousza32760ze3z87_2995;} }  else 
{ /* Llib/unicode.scm 1591 */
if(
(BgL_cz00_2997>=128L))
{ /* Llib/unicode.scm 1595 */
if(
CBOOL(BgL_tablez00_137))
{ /* Llib/unicode.scm 1597 */
 long BgL_vz00_3005;
BgL_vz00_3005 = 
(BgL_cz00_2997-128L); 
{ /* Llib/unicode.scm 1598 */
 bool_t BgL_test6003z00_14200;
{ /* Llib/unicode.scm 1598 */
 long BgL_arg2784z00_3016;
{ /* Llib/unicode.scm 1598 */
 obj_t BgL_vectorz00_5792;
if(
VECTORP(BgL_tablez00_137))
{ /* Llib/unicode.scm 1598 */
BgL_vectorz00_5792 = BgL_tablez00_137; }  else 
{ 
 obj_t BgL_auxz00_14203;
BgL_auxz00_14203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63677L), BGl_string4885z00zz__unicodez00, BGl_string4902z00zz__unicodez00, BgL_tablez00_137); 
FAILURE(BgL_auxz00_14203,BFALSE,BFALSE);} 
BgL_arg2784z00_3016 = 
VECTOR_LENGTH(BgL_vectorz00_5792); } 
BgL_test6003z00_14200 = 
(BgL_vz00_3005<BgL_arg2784z00_3016); } 
if(BgL_test6003z00_14200)
{ /* Llib/unicode.scm 1599 */
 obj_t BgL_encz00_3008;
{ /* Llib/unicode.scm 1599 */
 obj_t BgL_vectorz00_5795;
if(
VECTORP(BgL_tablez00_137))
{ /* Llib/unicode.scm 1599 */
BgL_vectorz00_5795 = BgL_tablez00_137; }  else 
{ 
 obj_t BgL_auxz00_14211;
BgL_auxz00_14211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63716L), BGl_string4885z00zz__unicodez00, BGl_string4902z00zz__unicodez00, BgL_tablez00_137); 
FAILURE(BgL_auxz00_14211,BFALSE,BFALSE);} 
{ /* Llib/unicode.scm 1599 */
 bool_t BgL_test6006z00_14215;
{ /* Llib/unicode.scm 1599 */
 long BgL_tmpz00_14216;
BgL_tmpz00_14216 = 
VECTOR_LENGTH(BgL_vectorz00_5795); 
BgL_test6006z00_14215 = 
BOUND_CHECK(BgL_vz00_3005, BgL_tmpz00_14216); } 
if(BgL_test6006z00_14215)
{ /* Llib/unicode.scm 1599 */
BgL_encz00_3008 = 
VECTOR_REF(BgL_vectorz00_5795,BgL_vz00_3005); }  else 
{ 
 obj_t BgL_auxz00_14220;
BgL_auxz00_14220 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63704L), BGl_string4894z00zz__unicodez00, BgL_vectorz00_5795, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_5795)), 
(int)(BgL_vz00_3005)); 
FAILURE(BgL_auxz00_14220,BFALSE,BFALSE);} } } 
{ /* Llib/unicode.scm 1599 */
 long BgL_lenz00_3009;
{ /* Llib/unicode.scm 1600 */
 obj_t BgL_stringz00_5797;
if(
STRINGP(BgL_encz00_3008))
{ /* Llib/unicode.scm 1600 */
BgL_stringz00_5797 = BgL_encz00_3008; }  else 
{ 
 obj_t BgL_auxz00_14229;
BgL_auxz00_14229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63738L), BGl_string4885z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_encz00_3008); 
FAILURE(BgL_auxz00_14229,BFALSE,BFALSE);} 
BgL_lenz00_3009 = 
STRING_LENGTH(BgL_stringz00_5797); } 
{ /* Llib/unicode.scm 1600 */

{ /* Llib/unicode.scm 1601 */
 obj_t BgL_auxz00_14234;
if(
STRINGP(BgL_encz00_3008))
{ /* Llib/unicode.scm 1601 */
BgL_auxz00_14234 = BgL_encz00_3008
; }  else 
{ 
 obj_t BgL_auxz00_14237;
BgL_auxz00_14237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63770L), BGl_string4885z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_encz00_3008); 
FAILURE(BgL_auxz00_14237,BFALSE,BFALSE);} 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_auxz00_14234, 0L, BgL_nstrz00_134, BgL_wz00_2994, BgL_lenz00_3009); } 
{ 
 long BgL_wz00_14244; long BgL_rz00_14242;
BgL_rz00_14242 = 
(BgL_rz00_2993+1L); 
BgL_wz00_14244 = 
(BgL_wz00_2994+BgL_lenz00_3009); 
BgL_wz00_2994 = BgL_wz00_14244; 
BgL_rz00_2993 = BgL_rz00_14242; 
goto BgL_zc3z04anonymousza32760ze3z87_2995;} } } }  else 
{ /* Llib/unicode.scm 1598 */
{ /* Llib/unicode.scm 1604 */
 long BgL_l4386z00_7278;
BgL_l4386z00_7278 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_wz00_2994, BgL_l4386z00_7278))
{ /* Llib/unicode.scm 1604 */
STRING_SET(BgL_nstrz00_134, BgL_wz00_2994, ((unsigned char)194)); }  else 
{ 
 obj_t BgL_auxz00_14250;
BgL_auxz00_14250 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63866L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4386z00_7278), 
(int)(BgL_wz00_2994)); 
FAILURE(BgL_auxz00_14250,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1605 */
 long BgL_arg2778z00_3012; unsigned char BgL_arg2780z00_3013;
BgL_arg2778z00_3012 = 
(BgL_wz00_2994+1L); 
BgL_arg2780z00_3013 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cz00_2997); 
{ /* Llib/unicode.scm 1605 */
 long BgL_l4390z00_7282;
BgL_l4390z00_7282 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_arg2778z00_3012, BgL_l4390z00_7282))
{ /* Llib/unicode.scm 1605 */
STRING_SET(BgL_nstrz00_134, BgL_arg2778z00_3012, BgL_arg2780z00_3013); }  else 
{ 
 obj_t BgL_auxz00_14262;
BgL_auxz00_14262 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(63921L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4390z00_7282), 
(int)(BgL_arg2778z00_3012)); 
FAILURE(BgL_auxz00_14262,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_14270; long BgL_rz00_14268;
BgL_rz00_14268 = 
(BgL_rz00_2993+1L); 
BgL_wz00_14270 = 
(BgL_wz00_2994+2L); 
BgL_wz00_2994 = BgL_wz00_14270; 
BgL_rz00_2993 = BgL_rz00_14268; 
goto BgL_zc3z04anonymousza32760ze3z87_2995;} } } }  else 
{ /* Llib/unicode.scm 1596 */
{ /* Llib/unicode.scm 1608 */
 long BgL_l4394z00_7286;
BgL_l4394z00_7286 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_wz00_2994, BgL_l4394z00_7286))
{ /* Llib/unicode.scm 1608 */
STRING_SET(BgL_nstrz00_134, BgL_wz00_2994, ((unsigned char)194)); }  else 
{ 
 obj_t BgL_auxz00_14276;
BgL_auxz00_14276 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(64028L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4394z00_7286), 
(int)(BgL_wz00_2994)); 
FAILURE(BgL_auxz00_14276,BFALSE,BFALSE);} } 
{ /* Llib/unicode.scm 1609 */
 long BgL_arg2786z00_3017; unsigned char BgL_arg2787z00_3018;
BgL_arg2786z00_3017 = 
(BgL_wz00_2994+1L); 
BgL_arg2787z00_3018 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cz00_2997); 
{ /* Llib/unicode.scm 1609 */
 long BgL_l4398z00_7290;
BgL_l4398z00_7290 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_arg2786z00_3017, BgL_l4398z00_7290))
{ /* Llib/unicode.scm 1609 */
STRING_SET(BgL_nstrz00_134, BgL_arg2786z00_3017, BgL_arg2787z00_3018); }  else 
{ 
 obj_t BgL_auxz00_14288;
BgL_auxz00_14288 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(64076L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4398z00_7290), 
(int)(BgL_arg2786z00_3017)); 
FAILURE(BgL_auxz00_14288,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_14296; long BgL_rz00_14294;
BgL_rz00_14294 = 
(BgL_rz00_2993+1L); 
BgL_wz00_14296 = 
(BgL_wz00_2994+2L); 
BgL_wz00_2994 = BgL_wz00_14296; 
BgL_rz00_2993 = BgL_rz00_14294; 
goto BgL_zc3z04anonymousza32760ze3z87_2995;} } }  else 
{ /* Llib/unicode.scm 1595 */
{ /* Llib/unicode.scm 1612 */
 unsigned char BgL_arg2794z00_3021;
BgL_arg2794z00_3021 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_cz00_2997); 
{ /* Llib/unicode.scm 1612 */
 long BgL_l4402z00_7294;
BgL_l4402z00_7294 = 
STRING_LENGTH(BgL_nstrz00_134); 
if(
BOUND_CHECK(BgL_wz00_2994, BgL_l4402z00_7294))
{ /* Llib/unicode.scm 1612 */
STRING_SET(BgL_nstrz00_134, BgL_wz00_2994, BgL_arg2794z00_3021); }  else 
{ 
 obj_t BgL_auxz00_14303;
BgL_auxz00_14303 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(64170L), BGl_string4915z00zz__unicodez00, BgL_nstrz00_134, 
(int)(BgL_l4402z00_7294), 
(int)(BgL_wz00_2994)); 
FAILURE(BgL_auxz00_14303,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_14311; long BgL_rz00_14309;
BgL_rz00_14309 = 
(BgL_rz00_2993+1L); 
BgL_wz00_14311 = 
(BgL_wz00_2994+1L); 
BgL_wz00_2994 = BgL_wz00_14311; 
BgL_rz00_2993 = BgL_rz00_14309; 
goto BgL_zc3z04anonymousza32760ze3z87_2995;} } } } } } 

}



/* 8bits->utf8 */
BGL_EXPORTED_DEF obj_t BGl_8bitszd2ze3utf8z31zz__unicodez00(obj_t BgL_strz00_138, obj_t BgL_tablez00_139)
{
{ /* Llib/unicode.scm 1618 */
{ /* Llib/unicode.scm 1619 */
 long BgL_lenz00_3026;
BgL_lenz00_3026 = 
STRING_LENGTH(BgL_strz00_138); 
{ /* Llib/unicode.scm 1619 */
 long BgL_nlenz00_3027;
BgL_nlenz00_3027 = 
BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_138, BgL_lenz00_3026, BgL_tablez00_139); 
{ /* Llib/unicode.scm 1620 */

if(
(BgL_lenz00_3026==BgL_nlenz00_3027))
{ /* Llib/unicode.scm 1621 */
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_138);}  else 
{ /* Llib/unicode.scm 1621 */
return 
BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(
make_string_sans_fill(BgL_nlenz00_3027), BgL_strz00_138, BgL_lenz00_3026, BgL_tablez00_139);} } } } } 

}



/* &8bits->utf8 */
obj_t BGl_z628bitszd2ze3utf8z53zz__unicodez00(obj_t BgL_envz00_6238, obj_t BgL_strz00_6239, obj_t BgL_tablez00_6240)
{
{ /* Llib/unicode.scm 1618 */
{ /* Llib/unicode.scm 1619 */
 obj_t BgL_auxz00_14320;
if(
STRINGP(BgL_strz00_6239))
{ /* Llib/unicode.scm 1619 */
BgL_auxz00_14320 = BgL_strz00_6239
; }  else 
{ 
 obj_t BgL_auxz00_14323;
BgL_auxz00_14323 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(64506L), BGl_string4972z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6239); 
FAILURE(BgL_auxz00_14323,BFALSE,BFALSE);} 
return 
BGl_8bitszd2ze3utf8z31zz__unicodez00(BgL_auxz00_14320, BgL_tablez00_6240);} } 

}



/* 8bits->utf8! */
BGL_EXPORTED_DEF obj_t BGl_8bitszd2ze3utf8z12z23zz__unicodez00(obj_t BgL_strz00_140, obj_t BgL_tablez00_141)
{
{ /* Llib/unicode.scm 1628 */
{ /* Llib/unicode.scm 1629 */
 long BgL_lenz00_3030;
BgL_lenz00_3030 = 
STRING_LENGTH(BgL_strz00_140); 
{ /* Llib/unicode.scm 1629 */
 long BgL_nlenz00_3031;
BgL_nlenz00_3031 = 
BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_140, BgL_lenz00_3030, BgL_tablez00_141); 
{ /* Llib/unicode.scm 1630 */

if(
(BgL_lenz00_3030==BgL_nlenz00_3031))
{ /* Llib/unicode.scm 1631 */
return BgL_strz00_140;}  else 
{ /* Llib/unicode.scm 1631 */
return 
BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(
make_string_sans_fill(BgL_nlenz00_3031), BgL_strz00_140, BgL_lenz00_3030, BgL_tablez00_141);} } } } } 

}



/* &8bits->utf8! */
obj_t BGl_z628bitszd2ze3utf8z12z41zz__unicodez00(obj_t BgL_envz00_6241, obj_t BgL_strz00_6242, obj_t BgL_tablez00_6243)
{
{ /* Llib/unicode.scm 1628 */
{ /* Llib/unicode.scm 1629 */
 obj_t BgL_auxz00_14334;
if(
STRINGP(BgL_strz00_6242))
{ /* Llib/unicode.scm 1629 */
BgL_auxz00_14334 = BgL_strz00_6242
; }  else 
{ 
 obj_t BgL_auxz00_14337;
BgL_auxz00_14337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(64958L), BGl_string4973z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6242); 
FAILURE(BgL_auxz00_14337,BFALSE,BFALSE);} 
return 
BGl_8bitszd2ze3utf8z12z23zz__unicodez00(BgL_auxz00_14334, BgL_tablez00_6243);} } 

}



/* iso-latin->utf8 */
BGL_EXPORTED_DEF obj_t BGl_isozd2latinzd2ze3utf8ze3zz__unicodez00(obj_t BgL_strz00_142)
{
{ /* Llib/unicode.scm 1638 */
{ /* Llib/unicode.scm 1639 */
 obj_t BgL_res3376z00_5838;
{ /* Llib/unicode.scm 1619 */
 long BgL_lenz00_5831;
BgL_lenz00_5831 = 
STRING_LENGTH(BgL_strz00_142); 
{ /* Llib/unicode.scm 1619 */
 long BgL_nlenz00_5833;
BgL_nlenz00_5833 = 
BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_142, BgL_lenz00_5831, BFALSE); 
{ /* Llib/unicode.scm 1620 */

if(
(BgL_lenz00_5831==BgL_nlenz00_5833))
{ /* Llib/unicode.scm 1621 */
BgL_res3376z00_5838 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_142); }  else 
{ /* Llib/unicode.scm 1621 */
BgL_res3376z00_5838 = 
BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(
make_string_sans_fill(BgL_nlenz00_5833), BgL_strz00_142, BgL_lenz00_5831, BFALSE); } } } } 
return BgL_res3376z00_5838;} } 

}



/* &iso-latin->utf8 */
obj_t BGl_z62isozd2latinzd2ze3utf8z81zz__unicodez00(obj_t BgL_envz00_6244, obj_t BgL_strz00_6245)
{
{ /* Llib/unicode.scm 1638 */
{ /* Llib/unicode.scm 1639 */
 obj_t BgL_auxz00_14349;
if(
STRINGP(BgL_strz00_6245))
{ /* Llib/unicode.scm 1639 */
BgL_auxz00_14349 = BgL_strz00_6245
; }  else 
{ 
 obj_t BgL_auxz00_14352;
BgL_auxz00_14352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(65393L), BGl_string4974z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6245); 
FAILURE(BgL_auxz00_14352,BFALSE,BFALSE);} 
return 
BGl_isozd2latinzd2ze3utf8ze3zz__unicodez00(BgL_auxz00_14349);} } 

}



/* iso-latin->utf8! */
BGL_EXPORTED_DEF obj_t BGl_isozd2latinzd2ze3utf8z12zf1zz__unicodez00(obj_t BgL_strz00_143)
{
{ /* Llib/unicode.scm 1644 */
{ /* Llib/unicode.scm 1645 */
 obj_t BgL_res3377z00_5847;
{ /* Llib/unicode.scm 1629 */
 long BgL_lenz00_5840;
BgL_lenz00_5840 = 
STRING_LENGTH(BgL_strz00_143); 
{ /* Llib/unicode.scm 1629 */
 long BgL_nlenz00_5842;
BgL_nlenz00_5842 = 
BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_143, BgL_lenz00_5840, BFALSE); 
{ /* Llib/unicode.scm 1630 */

if(
(BgL_lenz00_5840==BgL_nlenz00_5842))
{ /* Llib/unicode.scm 1631 */
BgL_res3377z00_5847 = BgL_strz00_143; }  else 
{ /* Llib/unicode.scm 1631 */
BgL_res3377z00_5847 = 
BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(
make_string_sans_fill(BgL_nlenz00_5842), BgL_strz00_143, BgL_lenz00_5840, BFALSE); } } } } 
return BgL_res3377z00_5847;} } 

}



/* &iso-latin->utf8! */
obj_t BGl_z62isozd2latinzd2ze3utf8z12z93zz__unicodez00(obj_t BgL_envz00_6246, obj_t BgL_strz00_6247)
{
{ /* Llib/unicode.scm 1644 */
{ /* Llib/unicode.scm 1645 */
 obj_t BgL_auxz00_14363;
if(
STRINGP(BgL_strz00_6247))
{ /* Llib/unicode.scm 1645 */
BgL_auxz00_14363 = BgL_strz00_6247
; }  else 
{ 
 obj_t BgL_auxz00_14366;
BgL_auxz00_14366 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(65675L), BGl_string4975z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6247); 
FAILURE(BgL_auxz00_14366,BFALSE,BFALSE);} 
return 
BGl_isozd2latinzd2ze3utf8z12zf1zz__unicodez00(BgL_auxz00_14363);} } 

}



/* cp1252->utf8 */
BGL_EXPORTED_DEF obj_t BGl_cp1252zd2ze3utf8z31zz__unicodez00(obj_t BgL_strz00_144)
{
{ /* Llib/unicode.scm 1650 */
{ /* Llib/unicode.scm 1651 */
 obj_t BgL_res3378z00_5857;
{ /* Llib/unicode.scm 1651 */
 obj_t BgL_tablez00_5849;
BgL_tablez00_5849 = BGl_vector4814z00zz__unicodez00; 
{ /* Llib/unicode.scm 1619 */
 long BgL_lenz00_5850;
BgL_lenz00_5850 = 
STRING_LENGTH(BgL_strz00_144); 
{ /* Llib/unicode.scm 1619 */
 long BgL_nlenz00_5852;
BgL_nlenz00_5852 = 
BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_144, BgL_lenz00_5850, BgL_tablez00_5849); 
{ /* Llib/unicode.scm 1620 */

if(
(BgL_lenz00_5850==BgL_nlenz00_5852))
{ /* Llib/unicode.scm 1621 */
BgL_res3378z00_5857 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_strz00_144); }  else 
{ /* Llib/unicode.scm 1621 */
BgL_res3378z00_5857 = 
BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(
make_string_sans_fill(BgL_nlenz00_5852), BgL_strz00_144, BgL_lenz00_5850, BgL_tablez00_5849); } } } } } 
return BgL_res3378z00_5857;} } 

}



/* &cp1252->utf8 */
obj_t BGl_z62cp1252zd2ze3utf8z53zz__unicodez00(obj_t BgL_envz00_6248, obj_t BgL_strz00_6249)
{
{ /* Llib/unicode.scm 1650 */
{ /* Llib/unicode.scm 1651 */
 obj_t BgL_auxz00_14378;
if(
STRINGP(BgL_strz00_6249))
{ /* Llib/unicode.scm 1651 */
BgL_auxz00_14378 = BgL_strz00_6249
; }  else 
{ 
 obj_t BgL_auxz00_14381;
BgL_auxz00_14381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(65951L), BGl_string4976z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6249); 
FAILURE(BgL_auxz00_14381,BFALSE,BFALSE);} 
return 
BGl_cp1252zd2ze3utf8z31zz__unicodez00(BgL_auxz00_14378);} } 

}



/* cp1252->utf8! */
BGL_EXPORTED_DEF obj_t BGl_cp1252zd2ze3utf8z12z23zz__unicodez00(obj_t BgL_strz00_145)
{
{ /* Llib/unicode.scm 1656 */
{ /* Llib/unicode.scm 1657 */
 obj_t BgL_res3379z00_5867;
{ /* Llib/unicode.scm 1657 */
 obj_t BgL_tablez00_5859;
BgL_tablez00_5859 = BGl_vector4814z00zz__unicodez00; 
{ /* Llib/unicode.scm 1629 */
 long BgL_lenz00_5860;
BgL_lenz00_5860 = 
STRING_LENGTH(BgL_strz00_145); 
{ /* Llib/unicode.scm 1629 */
 long BgL_nlenz00_5862;
BgL_nlenz00_5862 = 
BGl_8bitszd2ze3utf8zd2lengthze3zz__unicodez00(BgL_strz00_145, BgL_lenz00_5860, BgL_tablez00_5859); 
{ /* Llib/unicode.scm 1630 */

if(
(BgL_lenz00_5860==BgL_nlenz00_5862))
{ /* Llib/unicode.scm 1631 */
BgL_res3379z00_5867 = BgL_strz00_145; }  else 
{ /* Llib/unicode.scm 1631 */
BgL_res3379z00_5867 = 
BGl_8bitszd2ze3utf8zd2fillz12zf1zz__unicodez00(
make_string_sans_fill(BgL_nlenz00_5862), BgL_strz00_145, BgL_lenz00_5860, BgL_tablez00_5859); } } } } } 
return BgL_res3379z00_5867;} } 

}



/* &cp1252->utf8! */
obj_t BGl_z62cp1252zd2ze3utf8z12z41zz__unicodez00(obj_t BgL_envz00_6250, obj_t BgL_strz00_6251)
{
{ /* Llib/unicode.scm 1656 */
{ /* Llib/unicode.scm 1657 */
 obj_t BgL_auxz00_14392;
if(
STRINGP(BgL_strz00_6251))
{ /* Llib/unicode.scm 1657 */
BgL_auxz00_14392 = BgL_strz00_6251
; }  else 
{ 
 obj_t BgL_auxz00_14395;
BgL_auxz00_14395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(66234L), BGl_string4977z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6251); 
FAILURE(BgL_auxz00_14395,BFALSE,BFALSE);} 
return 
BGl_cp1252zd2ze3utf8z12z23zz__unicodez00(BgL_auxz00_14392);} } 

}



/* string-minimal-charset */
BGL_EXPORTED_DEF obj_t BGl_stringzd2minimalzd2charsetz00zz__unicodez00(obj_t BgL_strz00_146)
{
{ /* Llib/unicode.scm 1665 */
{ 
 long BgL_iz00_3036;
BgL_iz00_3036 = 
(
STRING_LENGTH(BgL_strz00_146)-1L); 
BgL_zc3z04anonymousza32812ze3z87_3037:
if(
(BgL_iz00_3036==-1L))
{ /* Llib/unicode.scm 1668 */
return BGl_symbol4904z00zz__unicodez00;}  else 
{ /* Llib/unicode.scm 1669 */
 bool_t BgL_test6027z00_14402;
{ /* Llib/unicode.scm 1669 */
 unsigned char BgL_tmpz00_14403;
{ /* Llib/unicode.scm 1669 */
 long BgL_l4406z00_7298;
BgL_l4406z00_7298 = 
STRING_LENGTH(BgL_strz00_146); 
if(
BOUND_CHECK(BgL_iz00_3036, BgL_l4406z00_7298))
{ /* Llib/unicode.scm 1669 */
BgL_tmpz00_14403 = 
STRING_REF(BgL_strz00_146, BgL_iz00_3036)
; }  else 
{ 
 obj_t BgL_auxz00_14408;
BgL_auxz00_14408 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(66836L), BGl_string4897z00zz__unicodez00, BgL_strz00_146, 
(int)(BgL_l4406z00_7298), 
(int)(BgL_iz00_3036)); 
FAILURE(BgL_auxz00_14408,BFALSE,BFALSE);} } 
BgL_test6027z00_14402 = 
(BgL_tmpz00_14403<=((unsigned char)'')); } 
if(BgL_test6027z00_14402)
{ 
 long BgL_iz00_14415;
BgL_iz00_14415 = 
(BgL_iz00_3036-1L); 
BgL_iz00_3036 = BgL_iz00_14415; 
goto BgL_zc3z04anonymousza32812ze3z87_3037;}  else 
{ /* Llib/unicode.scm 1669 */
return BGl_symbol4978z00zz__unicodez00;} } } } 

}



/* &string-minimal-charset */
obj_t BGl_z62stringzd2minimalzd2charsetz62zz__unicodez00(obj_t BgL_envz00_6252, obj_t BgL_strz00_6253)
{
{ /* Llib/unicode.scm 1665 */
{ /* Llib/unicode.scm 1666 */
 obj_t BgL_auxz00_14419;
if(
STRINGP(BgL_strz00_6253))
{ /* Llib/unicode.scm 1666 */
BgL_auxz00_14419 = BgL_strz00_6253
; }  else 
{ 
 obj_t BgL_auxz00_14422;
BgL_auxz00_14422 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(66746L), BGl_string4980z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6253); 
FAILURE(BgL_auxz00_14422,BFALSE,BFALSE);} 
return 
BGl_stringzd2minimalzd2charsetz00zz__unicodez00(BgL_auxz00_14419);} } 

}



/* utf8-string-minimal-charset */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t BgL_strz00_147)
{
{ /* Llib/unicode.scm 1678 */
{ /* Llib/unicode.scm 1680 */
 long BgL_lenz00_3045;
BgL_lenz00_3045 = 
(
STRING_LENGTH(BgL_strz00_147)-1L); 
{ 
 long BgL_iz00_3048; obj_t BgL_charsetz00_3049;
BgL_iz00_3048 = 0L; 
BgL_charsetz00_3049 = BGl_symbol4904z00zz__unicodez00; 
BgL_zc3z04anonymousza32822ze3z87_3050:
if(
(BgL_iz00_3048>=BgL_lenz00_3045))
{ /* Llib/unicode.scm 1683 */
return BgL_charsetz00_3049;}  else 
{ /* Llib/unicode.scm 1685 */
 long BgL_nz00_3052;
{ /* Llib/unicode.scm 1685 */
 unsigned char BgL_tmpz00_14431;
{ /* Llib/unicode.scm 1685 */
 long BgL_l4410z00_7302;
BgL_l4410z00_7302 = 
STRING_LENGTH(BgL_strz00_147); 
if(
BOUND_CHECK(BgL_iz00_3048, BgL_l4410z00_7302))
{ /* Llib/unicode.scm 1685 */
BgL_tmpz00_14431 = 
STRING_REF(BgL_strz00_147, BgL_iz00_3048)
; }  else 
{ 
 obj_t BgL_auxz00_14436;
BgL_auxz00_14436 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(67595L), BGl_string4897z00zz__unicodez00, BgL_strz00_147, 
(int)(BgL_l4410z00_7302), 
(int)(BgL_iz00_3048)); 
FAILURE(BgL_auxz00_14436,BFALSE,BFALSE);} } 
BgL_nz00_3052 = 
(BgL_tmpz00_14431); } 
if(
(BgL_nz00_3052<=127L))
{ 
 long BgL_iz00_14445;
BgL_iz00_14445 = 
(BgL_iz00_3048+1L); 
BgL_iz00_3048 = BgL_iz00_14445; 
goto BgL_zc3z04anonymousza32822ze3z87_3050;}  else 
{ /* Llib/unicode.scm 1689 */
 bool_t BgL_test6033z00_14447;
if(
(BgL_nz00_3052==194L))
{ /* Llib/unicode.scm 1689 */
BgL_test6033z00_14447 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1689 */
BgL_test6033z00_14447 = 
(BgL_nz00_3052==195L)
; } 
if(BgL_test6033z00_14447)
{ /* Llib/unicode.scm 1690 */
 long BgL_mz00_3057;
{ /* Llib/unicode.scm 1690 */
 unsigned char BgL_tmpz00_14451;
{ /* Llib/unicode.scm 1690 */
 long BgL_i4413z00_7305;
BgL_i4413z00_7305 = 
(BgL_iz00_3048+1L); 
{ /* Llib/unicode.scm 1690 */
 long BgL_l4414z00_7306;
BgL_l4414z00_7306 = 
STRING_LENGTH(BgL_strz00_147); 
if(
BOUND_CHECK(BgL_i4413z00_7305, BgL_l4414z00_7306))
{ /* Llib/unicode.scm 1690 */
BgL_tmpz00_14451 = 
STRING_REF(BgL_strz00_147, BgL_i4413z00_7305)
; }  else 
{ 
 obj_t BgL_auxz00_14457;
BgL_auxz00_14457 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(67747L), BGl_string4897z00zz__unicodez00, BgL_strz00_147, 
(int)(BgL_l4414z00_7306), 
(int)(BgL_i4413z00_7305)); 
FAILURE(BgL_auxz00_14457,BFALSE,BFALSE);} } } 
BgL_mz00_3057 = 
(BgL_tmpz00_14451); } 
{ /* Llib/unicode.scm 1691 */
 bool_t BgL_test6036z00_14464;
if(
(BgL_mz00_3057>=128L))
{ /* Llib/unicode.scm 1691 */
BgL_test6036z00_14464 = 
(BgL_mz00_3057<=191L)
; }  else 
{ /* Llib/unicode.scm 1691 */
BgL_test6036z00_14464 = ((bool_t)0)
; } 
if(BgL_test6036z00_14464)
{ 
 obj_t BgL_charsetz00_14470; long BgL_iz00_14468;
BgL_iz00_14468 = 
(BgL_iz00_3048+2L); 
BgL_charsetz00_14470 = BGl_symbol4978z00zz__unicodez00; 
BgL_charsetz00_3049 = BgL_charsetz00_14470; 
BgL_iz00_3048 = BgL_iz00_14468; 
goto BgL_zc3z04anonymousza32822ze3z87_3050;}  else 
{ /* Llib/unicode.scm 1691 */
return BGl_symbol4919z00zz__unicodez00;} } }  else 
{ /* Llib/unicode.scm 1689 */
return BGl_symbol4919z00zz__unicodez00;} } } } } } 

}



/* &utf8-string-minimal-charset */
obj_t BGl_z62utf8zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t BgL_envz00_6254, obj_t BgL_strz00_6255)
{
{ /* Llib/unicode.scm 1678 */
{ /* Llib/unicode.scm 1680 */
 obj_t BgL_auxz00_14471;
if(
STRINGP(BgL_strz00_6255))
{ /* Llib/unicode.scm 1680 */
BgL_auxz00_14471 = BgL_strz00_6255
; }  else 
{ 
 obj_t BgL_auxz00_14474;
BgL_auxz00_14474 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(67447L), BGl_string4981z00zz__unicodez00, BGl_string4895z00zz__unicodez00, BgL_strz00_6255); 
FAILURE(BgL_auxz00_14474,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2minimalzd2charsetzd2zz__unicodez00(BgL_auxz00_14471);} } 

}



/* ucs2-string-minimal-charset */
BGL_EXPORTED_DEF obj_t BGl_ucs2zd2stringzd2minimalzd2charsetzd2zz__unicodez00(obj_t BgL_strz00_148)
{
{ /* Llib/unicode.scm 1703 */
{ /* Llib/unicode.scm 1705 */
 long BgL_lenz00_3068;
BgL_lenz00_3068 = 
(
(long)(
UCS2_STRING_LENGTH(BgL_strz00_148))-1L); 
{ 
 long BgL_iz00_3071; obj_t BgL_charsetz00_3072;
BgL_iz00_3071 = 0L; 
BgL_charsetz00_3072 = BGl_symbol4904z00zz__unicodez00; 
BgL_zc3z04anonymousza32836ze3z87_3073:
if(
(BgL_iz00_3071>=BgL_lenz00_3068))
{ /* Llib/unicode.scm 1708 */
return BgL_charsetz00_3072;}  else 
{ /* Llib/unicode.scm 1710 */
 int BgL_nz00_3075;
{ /* Llib/unicode.scm 1710 */
 ucs2_t BgL_arg2858z00_3085;
{ /* Llib/unicode.scm 277 */
 int BgL_tmpz00_14484;
BgL_tmpz00_14484 = 
(int)(BgL_iz00_3071); 
BgL_arg2858z00_3085 = 
UCS2_STRING_REF(BgL_strz00_148, BgL_tmpz00_14484); } 
{ /* Llib/unicode.scm 1710 */
 obj_t BgL_tmpz00_14487;
BgL_tmpz00_14487 = 
BUCS2(BgL_arg2858z00_3085); 
BgL_nz00_3075 = 
CUCS2(BgL_tmpz00_14487); } } 
if(
(
(long)(BgL_nz00_3075)<=127L))
{ 
 long BgL_iz00_14493;
BgL_iz00_14493 = 
(BgL_iz00_3071+1L); 
BgL_iz00_3071 = BgL_iz00_14493; 
goto BgL_zc3z04anonymousza32836ze3z87_3073;}  else 
{ /* Llib/unicode.scm 1712 */
if(
(
(long)(BgL_nz00_3075)<=255L))
{ 
 obj_t BgL_charsetz00_14500; long BgL_iz00_14498;
BgL_iz00_14498 = 
(BgL_iz00_3071+1L); 
if(
(BgL_charsetz00_3072==BGl_symbol4982z00zz__unicodez00))
{ /* Llib/unicode.scm 1715 */
BgL_charsetz00_14500 = BGl_symbol4982z00zz__unicodez00; }  else 
{ /* Llib/unicode.scm 1715 */
BgL_charsetz00_14500 = BGl_symbol4978z00zz__unicodez00; } 
BgL_charsetz00_3072 = BgL_charsetz00_14500; 
BgL_iz00_3071 = BgL_iz00_14498; 
goto BgL_zc3z04anonymousza32836ze3z87_3073;}  else 
{ /* Llib/unicode.scm 1716 */
 bool_t BgL_test6043z00_14503;
if(
(
(long)(BgL_nz00_3075)<56320L))
{ /* Llib/unicode.scm 1716 */
BgL_test6043z00_14503 = ((bool_t)1)
; }  else 
{ /* Llib/unicode.scm 1716 */
BgL_test6043z00_14503 = 
(
(long)(BgL_nz00_3075)>56319L)
; } 
if(BgL_test6043z00_14503)
{ 
 obj_t BgL_charsetz00_14511; long BgL_iz00_14509;
BgL_iz00_14509 = 
(BgL_iz00_3071+1L); 
BgL_charsetz00_14511 = BGl_symbol4982z00zz__unicodez00; 
BgL_charsetz00_3072 = BgL_charsetz00_14511; 
BgL_iz00_3071 = BgL_iz00_14509; 
goto BgL_zc3z04anonymousza32836ze3z87_3073;}  else 
{ /* Llib/unicode.scm 1716 */
return BGl_symbol4984z00zz__unicodez00;} } } } } } } 

}



/* &ucs2-string-minimal-charset */
obj_t BGl_z62ucs2zd2stringzd2minimalzd2charsetzb0zz__unicodez00(obj_t BgL_envz00_6256, obj_t BgL_strz00_6257)
{
{ /* Llib/unicode.scm 1703 */
{ /* Llib/unicode.scm 1705 */
 obj_t BgL_auxz00_14512;
if(
UCS2_STRINGP(BgL_strz00_6257))
{ /* Llib/unicode.scm 1705 */
BgL_auxz00_14512 = BgL_strz00_6257
; }  else 
{ 
 obj_t BgL_auxz00_14515;
BgL_auxz00_14515 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string4847z00zz__unicodez00, 
BINT(68449L), BGl_string4986z00zz__unicodez00, BGl_string4852z00zz__unicodez00, BgL_strz00_6257); 
FAILURE(BgL_auxz00_14515,BFALSE,BFALSE);} 
return 
BGl_ucs2zd2stringzd2minimalzd2charsetzd2zz__unicodez00(BgL_auxz00_14512);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__unicodez00(void)
{
{ /* Llib/unicode.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string4987z00zz__unicodez00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string4987z00zz__unicodez00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string4987z00zz__unicodez00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string4987z00zz__unicodez00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string4987z00zz__unicodez00));} 

}

#ifdef __cplusplus
}
#endif
