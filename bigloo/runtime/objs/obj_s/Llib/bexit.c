/*===========================================================================*/
/*   (Llib/bexit.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/bexit.scm -indent -o objs/obj_s/Llib/bexit.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BEXIT_TYPE_DEFINITIONS
#define BGL___BEXIT_TYPE_DEFINITIONS
#endif // BGL___BEXIT_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_requirezd2initializa7ationz75zz__bexitz00 = BUNSPEC;
static obj_t BGl_z62envzd2getzd2exitdzd2valzb0zz__bexitz00(obj_t, obj_t);
static obj_t BGl_z62valzd2fromzd2exitzf3z91zz__bexitz00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_unwindzd2untilz12zc0zz__bexitz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_uncaught_exception_handler(obj_t);
BGL_EXPORTED_DECL obj_t bgl_failsafe_mutex_profile(void);
static obj_t BGl_z62z42failsafezd2mutexzd2profilez20zz__bexitz00(obj_t);
extern obj_t BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00(obj_t);
BGL_EXPORTED_DECL obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_exitdzd2execzd2protectz00zz__bexitz00(obj_t);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_envzd2getzd2exitdzd2valzd2zz__bexitz00(obj_t);
static obj_t BGl_cnstzd2initzd2zz__bexitz00(void);
BGL_EXPORTED_DECL obj_t BGl_exitdzd2pushzd2protectz12z12zz__bexitz00(obj_t, obj_t);
static obj_t BGl_z62defaultzd2uncaughtzd2exceptionzd2handlerzb0zz__bexitz00(obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__bexitz00(void);
static obj_t BGl_importedzd2moduleszd2initz00zz__bexitz00(void);
static bool_t BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00(obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__bexitz00(void);
static obj_t BGl_objectzd2initzd2zz__bexitz00(void);
static obj_t BGl_z62unwindzd2stackzd2valuezf3z91zz__bexitz00(obj_t, obj_t);
static obj_t BGl_z62unwindzd2untilz12za2zz__bexitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_valzd2fromzd2exitzf3zf3zz__bexitz00(obj_t);
static obj_t BGl_symbol1601z00zz__bexitz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_envzd2setzd2exitdzd2valz12zc0zz__bexitz00(obj_t, obj_t);
static obj_t BGl_symbol1603z00zz__bexitz00 = BUNSPEC;
static obj_t BGl_symbol1605z00zz__bexitz00 = BUNSPEC;
static obj_t BGl_symbol1608z00zz__bexitz00 = BUNSPEC;
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_symbol1617z00zz__bexitz00 = BUNSPEC;
extern obj_t BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(int);
static obj_t BGl_methodzd2initzd2zz__bexitz00(void);
static obj_t BGl_z62envzd2setzd2exitdzd2valz12za2zz__bexitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_exitdzd2popzd2protectz12z12zz__bexitz00(obj_t);
static obj_t BGl_list1600z00zz__bexitz00 = BUNSPEC;
static obj_t BGl_z62z42exitdzd2mutexzd2profilez20zz__bexitz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_exitdzd2protectzd2setz12z12zz__bexitz00(obj_t, obj_t);
static obj_t BGl_loopze70ze7zz__bexitz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list1607z00zz__bexitz00 = BUNSPEC;
static obj_t BGl_z62unwindzd2stackzd2untilz12z70zz__bexitz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list1616z00zz__bexitz00 = BUNSPEC;
static obj_t BGl_z62exitdzd2popzd2protectz12z70zz__bexitz00(obj_t, obj_t);
static obj_t BGl_z62exitdzd2pushzd2protectz12z70zz__bexitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_exitd_mutex_profile(void);
static obj_t BGl_z62exitdzd2protectzd2setz12z70zz__bexitz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t unwind_stack_value_p(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_unwindzd2stackzd2valuezf3zd2envz21zz__bexitz00, BgL_bgl_za762unwindza7d2stac1625z00, BGl_z62unwindzd2stackzd2valuezf3z91zz__bexitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_defaultzd2uncaughtzd2exceptionzd2handlerzd2envz00zz__bexitz00, BgL_bgl_za762defaultza7d2unc1626z00, BGl_z62defaultzd2uncaughtzd2exceptionzd2handlerzb0zz__bexitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exitdzd2protectzd2setz12zd2envzc0zz__bexitz00, BgL_bgl_za762exitdza7d2prote1627z00, BGl_z62exitdzd2protectzd2setz12z70zz__bexitz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1602z00zz__bexitz00, BgL_bgl_string1602za700za7za7_1628za7, "funcall", 7 );
DEFINE_STRING( BGl_string1604z00zz__bexitz00, BgL_bgl_string1604za700za7za7_1629za7, "proc-bottom", 11 );
DEFINE_STRING( BGl_string1606z00zz__bexitz00, BgL_bgl_string1606za700za7za7_1630za7, "val", 3 );
DEFINE_STRING( BGl_string1609z00zz__bexitz00, BgL_bgl_string1609za700za7za7_1631za7, "hdl", 3 );
DEFINE_STRING( BGl_string1610z00zz__bexitz00, BgL_bgl_string1610za700za7za7_1632za7, "unwind-protect", 14 );
DEFINE_STRING( BGl_string1611z00zz__bexitz00, BgL_bgl_string1611za700za7za7_1633za7, "exit out of dynamic scope", 25 );
DEFINE_STRING( BGl_string1612z00zz__bexitz00, BgL_bgl_string1612za700za7za7_1634za7, "bint", 4 );
DEFINE_STRING( BGl_string1613z00zz__bexitz00, BgL_bgl_string1613za700za7za7_1635za7, "exitd-exec-protect", 18 );
DEFINE_STRING( BGl_string1614z00zz__bexitz00, BgL_bgl_string1614za700za7za7_1636za7, "mutex", 5 );
DEFINE_STRING( BGl_string1615z00zz__bexitz00, BgL_bgl_string1615za700za7za7_1637za7, "exitd-exec-protect:Wrong number of arguments", 44 );
DEFINE_STRING( BGl_string1618z00zz__bexitz00, BgL_bgl_string1618za700za7za7_1638za7, "p", 1 );
DEFINE_STRING( BGl_string1619z00zz__bexitz00, BgL_bgl_string1619za700za7za7_1639za7, "exitd-pop-protect!", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z42failsafezd2mutexzd2profilezd2envz90zz__bexitz00, BgL_bgl_za762za742failsafeza7d1640za7, BGl_z62z42failsafezd2mutexzd2profilez20zz__bexitz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string1620z00zz__bexitz00, BgL_bgl_string1620za700za7za7_1641za7, "pair", 4 );
DEFINE_STRING( BGl_string1621z00zz__bexitz00, BgL_bgl_string1621za700za7za7_1642za7, "&env-get-exitd-val", 18 );
DEFINE_STRING( BGl_string1622z00zz__bexitz00, BgL_bgl_string1622za700za7za7_1643za7, "dynamic-env", 11 );
DEFINE_STRING( BGl_string1623z00zz__bexitz00, BgL_bgl_string1623za700za7za7_1644za7, "&env-set-exitd-val!", 19 );
DEFINE_STRING( BGl_string1624z00zz__bexitz00, BgL_bgl_string1624za700za7za7_1645za7, "__bexit", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_envzd2getzd2exitdzd2valzd2envz00zz__bexitz00, BgL_bgl_za762envza7d2getza7d2e1646za7, BGl_z62envzd2getzd2exitdzd2valzb0zz__bexitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unwindzd2stackzd2untilz12zd2envzc0zz__bexitz00, BgL_bgl_za762unwindza7d2stac1647z00, BGl_z62unwindzd2stackzd2untilz12z70zz__bexitz00, 0L, BUNSPEC, 5 );
DEFINE_STRING( BGl_string1596z00zz__bexitz00, BgL_bgl_string1596za700za7za7_1648za7, "/tmp/bigloo/runtime/Llib/bexit.scm", 34 );
DEFINE_STRING( BGl_string1597z00zz__bexitz00, BgL_bgl_string1597za700za7za7_1649za7, "loop~0", 6 );
DEFINE_STRING( BGl_string1598z00zz__bexitz00, BgL_bgl_string1598za700za7za7_1650za7, "procedure", 9 );
DEFINE_STRING( BGl_string1599z00zz__bexitz00, BgL_bgl_string1599za700za7za7_1651za7, "loop~0:Wrong number of arguments", 32 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exitdzd2pushzd2protectz12zd2envzc0zz__bexitz00, BgL_bgl_za762exitdza7d2pushza71652za7, BGl_z62exitdzd2pushzd2protectz12z70zz__bexitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z42exitdzd2mutexzd2profilezd2envz90zz__bexitz00, BgL_bgl_za762za742exitdza7d2mu1653za7, BGl_z62z42exitdzd2mutexzd2profilez20zz__bexitz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exitdzd2popzd2protectz12zd2envzc0zz__bexitz00, BgL_bgl_za762exitdza7d2popza7d1654za7, BGl_z62exitdzd2popzd2protectz12z70zz__bexitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unwindzd2untilz12zd2envz12zz__bexitz00, BgL_bgl_za762unwindza7d2unti1655z00, BGl_z62unwindzd2untilz12za2zz__bexitz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_valzd2fromzd2exitzf3zd2envz21zz__bexitz00, BgL_bgl_za762valza7d2fromza7d21656za7, BGl_z62valzd2fromzd2exitzf3z91zz__bexitz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_envzd2setzd2exitdzd2valz12zd2envz12zz__bexitz00, BgL_bgl_za762envza7d2setza7d2e1657za7, BGl_z62envzd2setzd2exitdzd2valz12za2zz__bexitz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_symbol1601z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_symbol1603z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_symbol1605z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_symbol1608z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_symbol1617z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_list1600z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_list1607z00zz__bexitz00) );
ADD_ROOT( (void *)(&BGl_list1616z00zz__bexitz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long BgL_checksumz00_1779, char * BgL_fromz00_1780)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__bexitz00))
{ 
BGl_requirezd2initializa7ationz75zz__bexitz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__bexitz00(); 
BGl_cnstzd2initzd2zz__bexitz00(); 
BGl_importedzd2moduleszd2initz00zz__bexitz00(); 
return 
BGl_methodzd2initzd2zz__bexitz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__bexitz00(void)
{
{ /* Llib/bexit.scm 14 */
BGl_symbol1601z00zz__bexitz00 = 
bstring_to_symbol(BGl_string1602z00zz__bexitz00); 
BGl_symbol1603z00zz__bexitz00 = 
bstring_to_symbol(BGl_string1604z00zz__bexitz00); 
BGl_symbol1605z00zz__bexitz00 = 
bstring_to_symbol(BGl_string1606z00zz__bexitz00); 
BGl_list1600z00zz__bexitz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1601z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1603z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1603z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1605z00zz__bexitz00, BNIL)))); 
BGl_symbol1608z00zz__bexitz00 = 
bstring_to_symbol(BGl_string1609z00zz__bexitz00); 
BGl_list1607z00zz__bexitz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1601z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1608z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1608z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1605z00zz__bexitz00, BNIL)))); 
BGl_symbol1617z00zz__bexitz00 = 
bstring_to_symbol(BGl_string1618z00zz__bexitz00); 
return ( 
BGl_list1616z00zz__bexitz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1601z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1617z00zz__bexitz00, 
MAKE_YOUNG_PAIR(BGl_symbol1617z00zz__bexitz00, BNIL))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__bexitz00(void)
{
{ /* Llib/bexit.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* val-from-exit? */
BGL_EXPORTED_DEF obj_t BGl_valzd2fromzd2exitzf3zf3zz__bexitz00(obj_t BgL_valz00_3)
{
{ /* Llib/bexit.scm 144 */
{ /* Llib/bexit.scm 145 */
 obj_t BgL_arg1154z00_1509;
BgL_arg1154z00_1509 = 
BGL_EXITD_VAL(); 
return 
BBOOL(
(BgL_valz00_3==BgL_arg1154z00_1509));} } 

}



/* &val-from-exit? */
obj_t BGl_z62valzd2fromzd2exitzf3z91zz__bexitz00(obj_t BgL_envz00_1715, obj_t BgL_valz00_1716)
{
{ /* Llib/bexit.scm 144 */
return 
BGl_valzd2fromzd2exitzf3zf3zz__bexitz00(BgL_valz00_1716);} 

}



/* unwind-stack-value? */
BGL_EXPORTED_DEF bool_t unwind_stack_value_p(obj_t BgL_valz00_4)
{
{ /* Llib/bexit.scm 155 */
{ /* Llib/bexit.scm 145 */
 obj_t BgL_arg1154z00_1510;
BgL_arg1154z00_1510 = 
BGL_EXITD_VAL(); 
return 
(BgL_valz00_4==BgL_arg1154z00_1510);} } 

}



/* &unwind-stack-value? */
obj_t BGl_z62unwindzd2stackzd2valuezf3z91zz__bexitz00(obj_t BgL_envz00_1717, obj_t BgL_valz00_1718)
{
{ /* Llib/bexit.scm 155 */
return 
BBOOL(
unwind_stack_value_p(BgL_valz00_1718));} 

}



/* unwind-until! */
BGL_EXPORTED_DEF obj_t BGl_unwindzd2untilz12zc0zz__bexitz00(obj_t BgL_exitdz00_5, obj_t BgL_valz00_6)
{
{ /* Llib/bexit.scm 165 */
return 
BGl_loopze70ze7zz__bexitz00(BFALSE, BgL_exitdz00_5, BFALSE, BgL_valz00_6, BFALSE);} 

}



/* &unwind-until! */
obj_t BGl_z62unwindzd2untilz12za2zz__bexitz00(obj_t BgL_envz00_1719, obj_t BgL_exitdz00_1720, obj_t BgL_valz00_1721)
{
{ /* Llib/bexit.scm 165 */
return 
BGl_unwindzd2untilz12zc0zz__bexitz00(BgL_exitdz00_1720, BgL_valz00_1721);} 

}



/* unwind-stack-until! */
BGL_EXPORTED_DEF obj_t unwind_stack_until(obj_t BgL_exitdz00_7, obj_t BgL_estampz00_8, obj_t BgL_valz00_9, obj_t BgL_proczd2bottomzd2_10, obj_t BgL_tracestkz00_11)
{
{ /* Llib/bexit.scm 176 */
BGL_TAIL return 
BGl_loopze70ze7zz__bexitz00(BgL_proczd2bottomzd2_10, BgL_exitdz00_7, BgL_estampz00_8, BgL_valz00_9, BgL_tracestkz00_11);} 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__bexitz00(obj_t BgL_proczd2bottomzd2_1749, obj_t BgL_exitdz00_1748, obj_t BgL_estampz00_1747, obj_t BgL_valz00_1746, obj_t BgL_tracestkz00_1745)
{
{ /* Llib/bexit.scm 177 */
BGl_loopze70ze7zz__bexitz00:
{ /* Llib/bexit.scm 178 */
 obj_t BgL_exitdzd2topzd2_1074;
BgL_exitdzd2topzd2_1074 = 
BGL_EXITD_TOP_AS_OBJ(); 
if(
BGL_EXITD_BOTTOMP(BgL_exitdzd2topzd2_1074))
{ /* Llib/bexit.scm 179 */
BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00(BgL_exitdzd2topzd2_1074); 
if(
PROCEDUREP(BgL_proczd2bottomzd2_1749))
{ /* Llib/bexit.scm 183 */
 obj_t BgL_funz00_1752;
{ /* Llib/bexit.scm 183 */
 obj_t BgL_aux1580z00_1750;
BgL_aux1580z00_1750 = BgL_proczd2bottomzd2_1749; 
if(
PROCEDUREP(BgL_aux1580z00_1750))
{ /* Llib/bexit.scm 183 */
BgL_funz00_1752 = BgL_aux1580z00_1750; }  else 
{ 
 obj_t BgL_auxz00_1824;
BgL_auxz00_1824 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(7892L), BGl_string1597z00zz__bexitz00, BGl_string1598z00zz__bexitz00, BgL_aux1580z00_1750); 
FAILURE(BgL_auxz00_1824,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_1752, 1))
{ /* Llib/bexit.scm 183 */
return 
(VA_PROCEDUREP( BgL_funz00_1752 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_1752))(BgL_proczd2bottomzd2_1749, BgL_valz00_1746, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_1752))(BgL_proczd2bottomzd2_1749, BgL_valz00_1746) );}  else 
{ /* Llib/bexit.scm 183 */
FAILURE(BGl_string1599z00zz__bexitz00,BGl_list1600z00zz__bexitz00,BgL_funz00_1752);} }  else 
{ /* Llib/bexit.scm 184 */
 obj_t BgL_hdlz00_1077;
BgL_hdlz00_1077 = 
BGL_UNCAUGHT_EXCEPTION_HANDLER_GET(); 
if(
PROCEDUREP(BgL_hdlz00_1077))
{ /* Llib/bexit.scm 185 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_hdlz00_1077, 1))
{ /* Llib/bexit.scm 186 */
return 
BGL_PROCEDURE_CALL1(BgL_hdlz00_1077, BgL_valz00_1746);}  else 
{ /* Llib/bexit.scm 186 */
FAILURE(BGl_string1599z00zz__bexitz00,BGl_list1607z00zz__bexitz00,BgL_hdlz00_1077);} }  else 
{ /* Llib/bexit.scm 185 */
return 
BGl_errorz00zz__errorz00(BGl_string1610z00zz__bexitz00, BGl_string1611z00zz__bexitz00, BgL_valz00_1746);} } }  else 
{ /* Llib/bexit.scm 179 */
BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00(BgL_exitdzd2topzd2_1074); 
POP_EXIT(); 
if(
CBOOL(BgL_tracestkz00_1745))
{ /* Llib/bexit.scm 193 */
BGL_SET_TRACE_STACKSP(BgL_tracestkz00_1745); }  else 
{ /* Llib/bexit.scm 195 */
 obj_t BgL_tmpz00_1851;
BgL_tmpz00_1851 = 
BGL_CURRENT_DYNAMIC_ENV(); 
bgl_init_trace(BgL_tmpz00_1851); } 
{ /* Llib/bexit.scm 198 */
 bool_t BgL_test1666z00_1854;
if(
(BgL_exitdzd2topzd2_1074==BgL_exitdz00_1748))
{ /* Llib/bexit.scm 199 */
 bool_t BgL__ortest_1039z00_1086;
if(
INTEGERP(BgL_estampz00_1747))
{ /* Llib/bexit.scm 199 */
BgL__ortest_1039z00_1086 = ((bool_t)0); }  else 
{ /* Llib/bexit.scm 199 */
BgL__ortest_1039z00_1086 = ((bool_t)1); } 
if(BgL__ortest_1039z00_1086)
{ /* Llib/bexit.scm 199 */
BgL_test1666z00_1854 = BgL__ortest_1039z00_1086
; }  else 
{ /* Llib/bexit.scm 200 */
 obj_t BgL_arg1172z00_1087;
BgL_arg1172z00_1087 = 
EXITD_STAMP(BgL_exitdzd2topzd2_1074); 
{ /* Llib/bexit.scm 200 */
 long BgL_n1z00_1511; long BgL_n2z00_1512;
BgL_n1z00_1511 = 
(long)CINT(BgL_arg1172z00_1087); 
{ /* Llib/bexit.scm 200 */
 obj_t BgL_tmpz00_1862;
if(
INTEGERP(BgL_estampz00_1747))
{ /* Llib/bexit.scm 200 */
BgL_tmpz00_1862 = BgL_estampz00_1747
; }  else 
{ 
 obj_t BgL_auxz00_1865;
BgL_auxz00_1865 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(8478L), BGl_string1597z00zz__bexitz00, BGl_string1612z00zz__bexitz00, BgL_estampz00_1747); 
FAILURE(BgL_auxz00_1865,BFALSE,BFALSE);} 
BgL_n2z00_1512 = 
(long)CINT(BgL_tmpz00_1862); } 
BgL_test1666z00_1854 = 
(BgL_n1z00_1511==BgL_n2z00_1512); } } }  else 
{ /* Llib/bexit.scm 198 */
BgL_test1666z00_1854 = ((bool_t)0)
; } 
if(BgL_test1666z00_1854)
{ /* Llib/bexit.scm 198 */
if(
EXITD_CALLCCP(BgL_exitdzd2topzd2_1074))
{ /* Llib/bexit.scm 203 */
 void * BgL_arg1171z00_1085;
BgL_arg1171z00_1085 = 
EXITD_TO_EXIT(BgL_exitdzd2topzd2_1074); 
CALLCC_JUMP_EXIT(BgL_arg1171z00_1085, BgL_valz00_1746); }  else 
{ /* Llib/bexit.scm 201 */
JUMP_EXIT( 
EXITD_TO_EXIT(BgL_exitdzd2topzd2_1074),BgL_valz00_1746)
; } 
return BUNSPEC;}  else 
{ 

goto BGl_loopze70ze7zz__bexitz00;} } } } } 

}



/* &unwind-stack-until! */
obj_t BGl_z62unwindzd2stackzd2untilz12z70zz__bexitz00(obj_t BgL_envz00_1722, obj_t BgL_exitdz00_1723, obj_t BgL_estampz00_1724, obj_t BgL_valz00_1725, obj_t BgL_proczd2bottomzd2_1726, obj_t BgL_tracestkz00_1727)
{
{ /* Llib/bexit.scm 176 */
return 
unwind_stack_until(BgL_exitdz00_1723, BgL_estampz00_1724, BgL_valz00_1725, BgL_proczd2bottomzd2_1726, BgL_tracestkz00_1727);} 

}



/* exitd-exec-and-pop-protects! */
bool_t BGl_exitdzd2execzd2andzd2popzd2protectsz12z12zz__bexitz00(obj_t BgL_exitdz00_12)
{
{ /* Llib/bexit.scm 216 */
{ /* Llib/bexit.scm 217 */
 obj_t BgL_g1040z00_1090;
BgL_g1040z00_1090 = 
BGL_EXITD_PROTECT(BgL_exitdz00_12); 
{ 
 obj_t BgL_lz00_1092;
BgL_lz00_1092 = BgL_g1040z00_1090; 
BgL_zc3z04anonymousza31173ze3z87_1093:
if(
PAIRP(BgL_lz00_1092))
{ /* Llib/bexit.scm 219 */
 obj_t BgL_pz00_1095;
BgL_pz00_1095 = 
CAR(BgL_lz00_1092); 
{ /* Llib/bexit.scm 220 */
 obj_t BgL_arg1182z00_1096;
BgL_arg1182z00_1096 = 
CDR(BgL_lz00_1092); 
BGL_EXITD_PROTECT_SET(BgL_exitdz00_12, BgL_arg1182z00_1096); BUNSPEC; } 
BGl_exitdzd2execzd2protectz00zz__bexitz00(BgL_pz00_1095); 
{ 
 obj_t BgL_lz00_1886;
BgL_lz00_1886 = 
CDR(BgL_lz00_1092); 
BgL_lz00_1092 = BgL_lz00_1886; 
goto BgL_zc3z04anonymousza31173ze3z87_1093;} }  else 
{ /* Llib/bexit.scm 218 */
return ((bool_t)0);} } } } 

}



/* exitd-exec-protect */
obj_t BGl_exitdzd2execzd2protectz00zz__bexitz00(obj_t BgL_pz00_13)
{
{ /* Llib/bexit.scm 227 */
if(
BGL_MUTEXP(BgL_pz00_13))
{ /* Llib/bexit.scm 229 */
 obj_t BgL_mz00_1516;
if(
BGL_MUTEXP(BgL_pz00_13))
{ /* Llib/bexit.scm 229 */
BgL_mz00_1516 = BgL_pz00_13; }  else 
{ 
 obj_t BgL_auxz00_1892;
BgL_auxz00_1892 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(9709L), BGl_string1613z00zz__bexitz00, BGl_string1614z00zz__bexitz00, BgL_pz00_13); 
FAILURE(BgL_auxz00_1892,BFALSE,BFALSE);} 
{ /* Llib/bexit.scm 229 */
 int BgL_arg1529z00_1517;
BgL_arg1529z00_1517 = 
BGL_MUTEX_UNLOCK(BgL_mz00_1516); 
return 
BBOOL(
(
(long)(BgL_arg1529z00_1517)==0L));} }  else 
{ /* Llib/bexit.scm 229 */
if(
PROCEDUREP(BgL_pz00_13))
{ /* Llib/bexit.scm 230 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_pz00_13, 0))
{ /* Llib/bexit.scm 230 */
return 
BGL_PROCEDURE_CALL0(BgL_pz00_13);}  else 
{ /* Llib/bexit.scm 230 */
FAILURE(BGl_string1615z00zz__bexitz00,BGl_list1616z00zz__bexitz00,BgL_pz00_13);} }  else 
{ /* Llib/bexit.scm 230 */
if(
PAIRP(BgL_pz00_13))
{ /* Llib/bexit.scm 231 */
BGL_ERROR_HANDLER_SET(BgL_pz00_13); 
return BUNSPEC;}  else 
{ /* Llib/bexit.scm 231 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_pz00_13))
{ /* Llib/bexit.scm 232 */
 int BgL_auxz00_1913;
{ /* Llib/bexit.scm 232 */
 obj_t BgL_tmpz00_1914;
if(
INTEGERP(BgL_pz00_13))
{ /* Llib/bexit.scm 232 */
BgL_tmpz00_1914 = BgL_pz00_13
; }  else 
{ 
 obj_t BgL_auxz00_1917;
BgL_auxz00_1917 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(9825L), BGl_string1613z00zz__bexitz00, BGl_string1612z00zz__bexitz00, BgL_pz00_13); 
FAILURE(BgL_auxz00_1917,BFALSE,BFALSE);} 
BgL_auxz00_1913 = 
CINT(BgL_tmpz00_1914); } 
return 
BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(BgL_auxz00_1913);}  else 
{ /* Llib/bexit.scm 232 */
if(
VECTORP(BgL_pz00_13))
{ /* Llib/bexit.scm 233 */
return 
BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00(BgL_pz00_13);}  else 
{ /* Llib/bexit.scm 233 */
return BFALSE;} } } } } } 

}



/* exitd-protect-set! */
BGL_EXPORTED_DEF obj_t BGl_exitdzd2protectzd2setz12z12zz__bexitz00(obj_t BgL_exitdz00_14, obj_t BgL_pz00_15)
{
{ /* Llib/bexit.scm 238 */
BGL_EXITD_PROTECT_SET(BgL_exitdz00_14, BgL_pz00_15); 
return BUNSPEC;} 

}



/* &exitd-protect-set! */
obj_t BGl_z62exitdzd2protectzd2setz12z70zz__bexitz00(obj_t BgL_envz00_1728, obj_t BgL_exitdz00_1729, obj_t BgL_pz00_1730)
{
{ /* Llib/bexit.scm 238 */
return 
BGl_exitdzd2protectzd2setz12z12zz__bexitz00(BgL_exitdz00_1729, BgL_pz00_1730);} 

}



/* exitd-push-protect! */
BGL_EXPORTED_DEF obj_t BGl_exitdzd2pushzd2protectz12z12zz__bexitz00(obj_t BgL_exitdz00_16, obj_t BgL_mz00_17)
{
{ /* Llib/bexit.scm 244 */
{ /* Llib/bexit.scm 245 */
 obj_t BgL_arg1189z00_1773;
{ /* Llib/bexit.scm 245 */
 obj_t BgL_arg1190z00_1774;
BgL_arg1190z00_1774 = 
BGL_EXITD_PROTECT(BgL_exitdz00_16); 
BgL_arg1189z00_1773 = 
MAKE_YOUNG_PAIR(BgL_mz00_17, BgL_arg1190z00_1774); } 
BGL_EXITD_PROTECT_SET(BgL_exitdz00_16, BgL_arg1189z00_1773); 
return BUNSPEC;} } 

}



/* &exitd-push-protect! */
obj_t BGl_z62exitdzd2pushzd2protectz12z70zz__bexitz00(obj_t BgL_envz00_1731, obj_t BgL_exitdz00_1732, obj_t BgL_mz00_1733)
{
{ /* Llib/bexit.scm 244 */
return 
BGl_exitdzd2pushzd2protectz12z12zz__bexitz00(BgL_exitdz00_1732, BgL_mz00_1733);} 

}



/* exitd-pop-protect! */
BGL_EXPORTED_DEF obj_t BGl_exitdzd2popzd2protectz12z12zz__bexitz00(obj_t BgL_exitdz00_18)
{
{ /* Llib/bexit.scm 250 */
{ /* Llib/bexit.scm 251 */
 bool_t BgL_test1681z00_1932;
{ /* Llib/bexit.scm 251 */
 obj_t BgL_arg1196z00_1775;
BgL_arg1196z00_1775 = 
BGL_EXITD_PROTECT(BgL_exitdz00_18); 
BgL_test1681z00_1932 = 
PAIRP(BgL_arg1196z00_1775); } 
if(BgL_test1681z00_1932)
{ /* Llib/bexit.scm 252 */
 obj_t BgL_arg1193z00_1776;
{ /* Llib/bexit.scm 252 */
 obj_t BgL_arg1194z00_1777;
BgL_arg1194z00_1777 = 
BGL_EXITD_PROTECT(BgL_exitdz00_18); 
{ /* Llib/bexit.scm 252 */
 obj_t BgL_pairz00_1778;
if(
PAIRP(BgL_arg1194z00_1777))
{ /* Llib/bexit.scm 252 */
BgL_pairz00_1778 = BgL_arg1194z00_1777; }  else 
{ 
 obj_t BgL_auxz00_1938;
BgL_auxz00_1938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(10883L), BGl_string1619z00zz__bexitz00, BGl_string1620z00zz__bexitz00, BgL_arg1194z00_1777); 
FAILURE(BgL_auxz00_1938,BFALSE,BFALSE);} 
BgL_arg1193z00_1776 = 
CDR(BgL_pairz00_1778); } } 
BGL_EXITD_PROTECT_SET(BgL_exitdz00_18, BgL_arg1193z00_1776); 
return BUNSPEC;}  else 
{ /* Llib/bexit.scm 251 */
return BFALSE;} } } 

}



/* &exitd-pop-protect! */
obj_t BGl_z62exitdzd2popzd2protectz12z70zz__bexitz00(obj_t BgL_envz00_1734, obj_t BgL_exitdz00_1735)
{
{ /* Llib/bexit.scm 250 */
return 
BGl_exitdzd2popzd2protectz12z12zz__bexitz00(BgL_exitdz00_1735);} 

}



/* default-uncaught-exception-handler */
BGL_EXPORTED_DEF obj_t bgl_uncaught_exception_handler(obj_t BgL_valz00_19)
{
{ /* Llib/bexit.scm 257 */
return 
BGl_errorz00zz__errorz00(BGl_string1610z00zz__bexitz00, BGl_string1611z00zz__bexitz00, BgL_valz00_19);} 

}



/* &default-uncaught-exception-handler */
obj_t BGl_z62defaultzd2uncaughtzd2exceptionzd2handlerzb0zz__bexitz00(obj_t BgL_envz00_1736, obj_t BgL_valz00_1737)
{
{ /* Llib/bexit.scm 257 */
return 
bgl_uncaught_exception_handler(BgL_valz00_1737);} 

}



/* $exitd-mutex-profile */
BGL_EXPORTED_DEF obj_t bgl_exitd_mutex_profile(void)
{
{ /* Llib/bexit.scm 268 */
return BUNSPEC;} 

}



/* &$exitd-mutex-profile */
obj_t BGl_z62z42exitdzd2mutexzd2profilez20zz__bexitz00(obj_t BgL_envz00_1738)
{
{ /* Llib/bexit.scm 268 */
return 
bgl_exitd_mutex_profile();} 

}



/* $failsafe-mutex-profile */
BGL_EXPORTED_DEF obj_t bgl_failsafe_mutex_profile(void)
{
{ /* Llib/bexit.scm 269 */
return BUNSPEC;} 

}



/* &$failsafe-mutex-profile */
obj_t BGl_z62z42failsafezd2mutexzd2profilez20zz__bexitz00(obj_t BgL_envz00_1739)
{
{ /* Llib/bexit.scm 269 */
return 
bgl_failsafe_mutex_profile();} 

}



/* env-get-exitd-val */
BGL_EXPORTED_DEF obj_t BGl_envzd2getzd2exitdzd2valzd2zz__bexitz00(obj_t BgL_envz00_20)
{
{ /* Llib/bexit.scm 276 */
return 
BGL_ENV_EXITD_VAL(BgL_envz00_20);} 

}



/* &env-get-exitd-val */
obj_t BGl_z62envzd2getzd2exitdzd2valzb0zz__bexitz00(obj_t BgL_envz00_1740, obj_t BgL_envz00_1741)
{
{ /* Llib/bexit.scm 276 */
{ /* Llib/bexit.scm 276 */
 obj_t BgL_auxz00_1950;
if(
BGL_DYNAMIC_ENVP(BgL_envz00_1741))
{ /* Llib/bexit.scm 276 */
BgL_auxz00_1950 = BgL_envz00_1741
; }  else 
{ 
 obj_t BgL_auxz00_1953;
BgL_auxz00_1953 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(12088L), BGl_string1621z00zz__bexitz00, BGl_string1622z00zz__bexitz00, BgL_envz00_1741); 
FAILURE(BgL_auxz00_1953,BFALSE,BFALSE);} 
return 
BGl_envzd2getzd2exitdzd2valzd2zz__bexitz00(BgL_auxz00_1950);} } 

}



/* env-set-exitd-val! */
BGL_EXPORTED_DEF obj_t BGl_envzd2setzd2exitdzd2valz12zc0zz__bexitz00(obj_t BgL_envz00_21, obj_t BgL_objz00_22)
{
{ /* Llib/bexit.scm 277 */
return 
BGL_ENV_EXITD_VAL_SET(BgL_envz00_21, BgL_objz00_22);} 

}



/* &env-set-exitd-val! */
obj_t BGl_z62envzd2setzd2exitdzd2valz12za2zz__bexitz00(obj_t BgL_envz00_1742, obj_t BgL_envz00_1743, obj_t BgL_objz00_1744)
{
{ /* Llib/bexit.scm 277 */
{ /* Llib/bexit.scm 277 */
 obj_t BgL_auxz00_1959;
if(
BGL_DYNAMIC_ENVP(BgL_envz00_1743))
{ /* Llib/bexit.scm 277 */
BgL_auxz00_1959 = BgL_envz00_1743
; }  else 
{ 
 obj_t BgL_auxz00_1962;
BgL_auxz00_1962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1596z00zz__bexitz00, 
BINT(12162L), BGl_string1623z00zz__bexitz00, BGl_string1622z00zz__bexitz00, BgL_envz00_1743); 
FAILURE(BgL_auxz00_1962,BFALSE,BFALSE);} 
return 
BGl_envzd2setzd2exitdzd2valz12zc0zz__bexitz00(BgL_auxz00_1959, BgL_objz00_1744);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__bexitz00(void)
{
{ /* Llib/bexit.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__bexitz00(void)
{
{ /* Llib/bexit.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__bexitz00(void)
{
{ /* Llib/bexit.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__bexitz00(void)
{
{ /* Llib/bexit.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1624z00zz__bexitz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1624z00zz__bexitz00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1624z00zz__bexitz00));} 

}

#ifdef __cplusplus
}
#endif
