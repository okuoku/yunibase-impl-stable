/*===========================================================================*/
/*   (Llib/dsssl.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/dsssl.scm -indent -o objs/obj_s/Llib/dsssl.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___DSSSL_TYPE_DEFINITIONS
#define BGL___DSSSL_TYPE_DEFINITIONS
#endif // BGL___DSSSL_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__dssslz00 = BUNSPEC;
static obj_t BGl_z62dssslzd2getzd2keyzd2argzb0zz__dssslz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62dssslzd2formalszd2ze3schemezd2typedzd2formalsz81zz__dssslz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62restzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_dssslzd2checkzd2keyzd2argsz12zc0zz__dssslz00(obj_t, obj_t);
static obj_t BGl_z62dssslzd2namedzd2constantzf3z91zz__dssslz00(obj_t, obj_t);
static obj_t BGl_z62optionalzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t);
extern obj_t BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_dssslzd2formalszd2ze3schemezd2formalsz31zz__dssslz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(obj_t, obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__dssslz00(void);
static obj_t BGl_symbol2303z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_genericzd2initzd2zz__dssslz00(void);
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_symbol2309z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__dssslz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__dssslz00(void);
static obj_t BGl_objectzd2initzd2zz__dssslz00(void);
static obj_t BGl_symbol2311z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2313z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2315z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2317z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_z62dssslzd2getzd2keyzd2restzd2argz62zz__dssslz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2319z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_z62dssslzd2checkzd2keyzd2argsz12za2zz__dssslz00(obj_t, obj_t, obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t bgl_reverse_bang(obj_t);
static obj_t BGl_symbol2245z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2247z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2249z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_z62dssslzd2formalszd2ze3schemezd2formalsz53zz__dssslz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2251z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2253z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2302z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2255z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2306z00zz__dssslz00 = BUNSPEC;
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t, obj_t, bool_t);
static obj_t BGl_symbol2263z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2268z00zz__dssslz00 = BUNSPEC;
extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
static obj_t BGl_methodzd2initzd2zz__dssslz00(void);
static obj_t BGl_symbol2272z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2274z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2241z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2323z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2276z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2244z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2327z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2279z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_optionalzd2argsze70z35zz__dssslz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_symbol2281z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2284z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2286z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_loopze70ze7zz__dssslz00(bool_t, obj_t, obj_t, obj_t, bool_t);
static obj_t BGl_loopze71ze7zz__dssslz00(obj_t, obj_t);
static obj_t BGl_symbol2288z00zz__dssslz00 = BUNSPEC;
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_idzd2sanszd2typez00zz__dssslz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(obj_t, obj_t);
static obj_t BGl_symbol2291z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2293z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2295z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2262z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_symbol2297z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_z62makezd2dssslzd2functionzd2preludezb0zz__dssslz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list2347z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2267z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_z62keyzd2statezb0zz__dssslz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t);
static obj_t BGl_list2351z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2353z00zz__dssslz00 = BUNSPEC;
static obj_t BGl_list2356z00zz__dssslz00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_dssslzd2getzd2keyzd2argzd2envz00zz__dssslz00, BgL_bgl_za762dssslza7d2getza7d2359za7, BGl_z62dssslzd2getzd2keyzd2argzb0zz__dssslz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalszd2envz31zz__dssslz00, BgL_bgl_za762dssslza7d2forma2360z00, BGl_z62dssslzd2formalszd2ze3schemezd2typedzd2formalsz81zz__dssslz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2300z00zz__dssslz00, BgL_bgl_string2300za700za7za7_2361za7, "Illegal DSSSL formal list (#!rest)", 34 );
DEFINE_STRING( BGl_string2301z00zz__dssslz00, BgL_bgl_string2301za700za7za7_2362za7, "&key-state:Wrong number of arguments", 36 );
DEFINE_STRING( BGl_string2304z00zz__dssslz00, BgL_bgl_string2304za700za7za7_2363za7, "formals", 7 );
DEFINE_STRING( BGl_string2305z00zz__dssslz00, BgL_bgl_string2305za700za7za7_2364za7, "Illegal DSSSL formal list (#!key)", 33 );
DEFINE_STRING( BGl_string2307z00zz__dssslz00, BgL_bgl_string2307za700za7za7_2365za7, "loop", 4 );
DEFINE_STRING( BGl_string2308z00zz__dssslz00, BgL_bgl_string2308za700za7za7_2366za7, "symbol", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dssslzd2namedzd2constantzf3zd2envz21zz__dssslz00, BgL_bgl_za762dssslza7d2named2367z00, BGl_z62dssslzd2namedzd2constantzf3z91zz__dssslz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2310z00zz__dssslz00, BgL_bgl_string2310za700za7za7_2368za7, "tmp", 3 );
DEFINE_STRING( BGl_string2312z00zz__dssslz00, BgL_bgl_string2312za700za7za7_2369za7, "car", 3 );
DEFINE_STRING( BGl_string2314z00zz__dssslz00, BgL_bgl_string2314za700za7za7_2370za7, "memq", 4 );
DEFINE_STRING( BGl_string2316z00zz__dssslz00, BgL_bgl_string2316za700za7za7_2371za7, "cdr", 3 );
DEFINE_STRING( BGl_string2318z00zz__dssslz00, BgL_bgl_string2318za700za7za7_2372za7, "set!", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dssslzd2getzd2keyzd2restzd2argzd2envzd2zz__dssslz00, BgL_bgl_za762dssslza7d2getza7d2373za7, BGl_z62dssslzd2getzd2keyzd2restzd2argz62zz__dssslz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2320z00zz__dssslz00, BgL_bgl_string2320za700za7za7_2374za7, "begin", 5 );
DEFINE_STRING( BGl_string2321z00zz__dssslz00, BgL_bgl_string2321za700za7za7_2375za7, "Illegal DSSSL formal list (#!optional)", 38 );
DEFINE_STRING( BGl_string2322z00zz__dssslz00, BgL_bgl_string2322za700za7za7_2376za7, "optional-args~0:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string2242z00zz__dssslz00, BgL_bgl_string2242za700za7za7_2377za7, "Illegal formal list.1", 21 );
DEFINE_STRING( BGl_string2324z00zz__dssslz00, BgL_bgl_string2324za700za7za7_2378za7, "<@anonymous:1443>", 17 );
DEFINE_STRING( BGl_string2243z00zz__dssslz00, BgL_bgl_string2243za700za7za7_2379za7, "scheme-state:Wrong number of arguments", 38 );
DEFINE_STRING( BGl_string2325z00zz__dssslz00, BgL_bgl_string2325za700za7za7_2380za7, "Illegal #!keys parameters", 25 );
DEFINE_STRING( BGl_string2326z00zz__dssslz00, BgL_bgl_string2326za700za7za7_2381za7, "<@anonymous:1443>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2246z00zz__dssslz00, BgL_bgl_string2246za700za7za7_2382za7, "funcall", 7 );
DEFINE_STRING( BGl_string2328z00zz__dssslz00, BgL_bgl_string2328za700za7za7_2383za7, "list", 4 );
DEFINE_STRING( BGl_string2329z00zz__dssslz00, BgL_bgl_string2329za700za7za7_2384za7, "rest-key-state:Wrong number of arguments", 40 );
DEFINE_STRING( BGl_string2248z00zz__dssslz00, BgL_bgl_string2248za700za7za7_2385za7, "err", 3 );
DEFINE_STRING( BGl_string2330z00zz__dssslz00, BgL_bgl_string2330za700za7za7_2386za7, "exit-rest-state:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string2331z00zz__dssslz00, BgL_bgl_string2331za700za7za7_2387za7, "rest-state:Wrong number of arguments", 36 );
DEFINE_STRING( BGl_string2250z00zz__dssslz00, BgL_bgl_string2250za700za7za7_2388za7, "where", 5 );
DEFINE_STRING( BGl_string2332z00zz__dssslz00, BgL_bgl_string2332za700za7za7_2389za7, "no-rest-key-state:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string2333z00zz__dssslz00, BgL_bgl_string2333za700za7za7_2390za7, "dsssl formal parsing", 20 );
DEFINE_STRING( BGl_string2252z00zz__dssslz00, BgL_bgl_string2252za700za7za7_2391za7, "arg1200", 7 );
DEFINE_STRING( BGl_string2334z00zz__dssslz00, BgL_bgl_string2334za700za7za7_2392za7, "Unexpected #!keys parameters", 28 );
DEFINE_STRING( BGl_string2335z00zz__dssslz00, BgL_bgl_string2335za700za7za7_2393za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2254z00zz__dssslz00, BgL_bgl_string2254za700za7za7_2394za7, "dsssl", 5 );
DEFINE_STRING( BGl_string2336z00zz__dssslz00, BgL_bgl_string2336za700za7za7_2395za7, "Keyword argument misses value", 29 );
DEFINE_STRING( BGl_string2337z00zz__dssslz00, BgL_bgl_string2337za700za7za7_2396za7, "Illegal keyword actual value", 28 );
DEFINE_STRING( BGl_string2256z00zz__dssslz00, BgL_bgl_string2256za700za7za7_2397za7, "let", 3 );
DEFINE_STRING( BGl_string2338z00zz__dssslz00, BgL_bgl_string2338za700za7za7_2398za7, "Illegal DSSSL arguments", 23 );
DEFINE_STRING( BGl_string2257z00zz__dssslz00, BgL_bgl_string2257za700za7za7_2399za7, "/tmp/bigloo/runtime/Llib/dsssl.scm", 34 );
DEFINE_STRING( BGl_string2339z00zz__dssslz00, BgL_bgl_string2339za700za7za7_2400za7, "&dsssl-get-key-arg", 18 );
DEFINE_STRING( BGl_string2258z00zz__dssslz00, BgL_bgl_string2258za700za7za7_2401za7, "TAG-102", 7 );
DEFINE_STRING( BGl_string2259z00zz__dssslz00, BgL_bgl_string2259za700za7za7_2402za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dssslzd2checkzd2keyzd2argsz12zd2envz12zz__dssslz00, BgL_bgl_za762dssslza7d2check2403z00, BGl_z62dssslzd2checkzd2keyzd2argsz12za2zz__dssslz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2340z00zz__dssslz00, BgL_bgl_string2340za700za7za7_2404za7, "keyword", 7 );
DEFINE_STRING( BGl_string2341z00zz__dssslz00, BgL_bgl_string2341za700za7za7_2405za7, "loop~1", 6 );
DEFINE_STRING( BGl_string2260z00zz__dssslz00, BgL_bgl_string2260za700za7za7_2406za7, "Illegal formal list.3", 21 );
DEFINE_STRING( BGl_string2342z00zz__dssslz00, BgL_bgl_string2342za700za7za7_2407za7, "&dsssl-get-key-rest-arg", 23 );
DEFINE_STRING( BGl_string2261z00zz__dssslz00, BgL_bgl_string2261za700za7za7_2408za7, "TAG-103:Wrong number of arguments", 33 );
DEFINE_STRING( BGl_string2343z00zz__dssslz00, BgL_bgl_string2343za700za7za7_2409za7, "string-ref", 10 );
DEFINE_STRING( BGl_string2344z00zz__dssslz00, BgL_bgl_string2344za700za7za7_2410za7, "&dsssl-formals->scheme-formals", 30 );
DEFINE_STRING( BGl_string2345z00zz__dssslz00, BgL_bgl_string2345za700za7za7_2411za7, "dsssl-defaulted-formal?", 23 );
DEFINE_STRING( BGl_string2264z00zz__dssslz00, BgL_bgl_string2264za700za7za7_2412za7, "arg1248", 7 );
DEFINE_STRING( BGl_string2346z00zz__dssslz00, BgL_bgl_string2346za700za7za7_2413za7, "loop~0", 6 );
DEFINE_STRING( BGl_string2265z00zz__dssslz00, BgL_bgl_string2265za700za7za7_2414za7, "Illegal formal list.2", 21 );
DEFINE_STRING( BGl_string2266z00zz__dssslz00, BgL_bgl_string2266za700za7za7_2415za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string2348z00zz__dssslz00, BgL_bgl_string2348za700za7za7_2416za7, "Illegal formal parameter", 24 );
DEFINE_STRING( BGl_string2349z00zz__dssslz00, BgL_bgl_string2349za700za7za7_2417za7, "symbol or named constant expected", 33 );
DEFINE_STRING( BGl_string2269z00zz__dssslz00, BgL_bgl_string2269za700za7za7_2418za7, "arg1252", 7 );
DEFINE_STRING( BGl_string2350z00zz__dssslz00, BgL_bgl_string2350za700za7za7_2419za7, "loop~0:Wrong number of arguments", 32 );
DEFINE_STRING( BGl_string2270z00zz__dssslz00, BgL_bgl_string2270za700za7za7_2420za7, "&make-dsssl-function-prelude", 28 );
DEFINE_STRING( BGl_string2352z00zz__dssslz00, BgL_bgl_string2352za700za7za7_2421za7, "symbol expected", 15 );
DEFINE_STRING( BGl_string2271z00zz__dssslz00, BgL_bgl_string2271za700za7za7_2422za7, "procedure", 9 );
DEFINE_STRING( BGl_string2354z00zz__dssslz00, BgL_bgl_string2354za700za7za7_2423za7, "Can't use both DSSSL named constant", 35 );
DEFINE_STRING( BGl_string2273z00zz__dssslz00, BgL_bgl_string2273za700za7za7_2424za7, "quote", 5 );
DEFINE_STRING( BGl_string2355z00zz__dssslz00, BgL_bgl_string2355za700za7za7_2425za7, "and `.' notation", 16 );
DEFINE_STRING( BGl_string2275z00zz__dssslz00, BgL_bgl_string2275za700za7za7_2426za7, "dsssl-get-key-rest-arg", 22 );
DEFINE_STRING( BGl_string2357z00zz__dssslz00, BgL_bgl_string2357za700za7za7_2427za7, "&dsssl-formals->scheme-typed-formals", 36 );
DEFINE_STRING( BGl_string2358z00zz__dssslz00, BgL_bgl_string2358za700za7za7_2428za7, "__dsssl", 7 );
DEFINE_STRING( BGl_string2277z00zz__dssslz00, BgL_bgl_string2277za700za7za7_2429za7, "dsssl-get-key-arg", 17 );
DEFINE_STRING( BGl_string2278z00zz__dssslz00, BgL_bgl_string2278za700za7za7_2430za7, "one-key-arg", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2dssslzd2functionzd2preludezd2envz00zz__dssslz00, BgL_bgl_za762makeza7d2dssslza72431za7, BGl_z62makezd2dssslzd2functionzd2preludezb0zz__dssslz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string2280z00zz__dssslz00, BgL_bgl_string2280za700za7za7_2432za7, "null?", 5 );
DEFINE_STRING( BGl_string2282z00zz__dssslz00, BgL_bgl_string2282za700za7za7_2433za7, "v", 1 );
DEFINE_STRING( BGl_string2283z00zz__dssslz00, BgL_bgl_string2283za700za7za7_2434za7, "~a ", 3 );
DEFINE_STRING( BGl_string2285z00zz__dssslz00, BgL_bgl_string2285za700za7za7_2435za7, "format", 6 );
DEFINE_STRING( BGl_string2287z00zz__dssslz00, BgL_bgl_string2287za700za7za7_2436za7, "lambda", 6 );
DEFINE_STRING( BGl_string2289z00zz__dssslz00, BgL_bgl_string2289za700za7za7_2437za7, "map", 3 );
DEFINE_STRING( BGl_string2290z00zz__dssslz00, BgL_bgl_string2290za700za7za7_2438za7, "Illegal extra key arguments: ", 29 );
DEFINE_STRING( BGl_string2292z00zz__dssslz00, BgL_bgl_string2292za700za7za7_2439za7, "string-append", 13 );
DEFINE_STRING( BGl_string2294z00zz__dssslz00, BgL_bgl_string2294za700za7za7_2440za7, "apply", 5 );
DEFINE_STRING( BGl_string2296z00zz__dssslz00, BgL_bgl_string2296za700za7za7_2441za7, "error", 5 );
DEFINE_STRING( BGl_string2298z00zz__dssslz00, BgL_bgl_string2298za700za7za7_2442za7, "if", 2 );
DEFINE_STRING( BGl_string2299z00zz__dssslz00, BgL_bgl_string2299za700za7za7_2443za7, "&key-state", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dssslzd2formalszd2ze3schemezd2formalszd2envze3zz__dssslz00, BgL_bgl_za762dssslza7d2forma2444z00, BGl_z62dssslzd2formalszd2ze3schemezd2formalsz53zz__dssslz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2303z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2309z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2311z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2313z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2315z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2317z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2319z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2245z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2247z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2249z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2251z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2253z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2302z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2255z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2306z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2263z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2268z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2272z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2274z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2241z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2323z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2276z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2244z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2327z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2279z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2281z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2284z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2286z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2288z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2291z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2293z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2295z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2262z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_symbol2297z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2347z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2267z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2351z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2353z00zz__dssslz00) );
ADD_ROOT( (void *)(&BGl_list2356z00zz__dssslz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long BgL_checksumz00_2865, char * BgL_fromz00_2866)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__dssslz00))
{ 
BGl_requirezd2initializa7ationz75zz__dssslz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__dssslz00(); 
BGl_cnstzd2initzd2zz__dssslz00(); 
BGl_importedzd2moduleszd2initz00zz__dssslz00(); 
return 
BGl_methodzd2initzd2zz__dssslz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__dssslz00(void)
{
{ /* Llib/dsssl.scm 14 */
BGl_list2241z00zz__dssslz00 = 
MAKE_YOUNG_PAIR((BREST), 
MAKE_YOUNG_PAIR((BOPTIONAL), 
MAKE_YOUNG_PAIR((BKEY), BNIL))); 
BGl_symbol2245z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2246z00zz__dssslz00); 
BGl_symbol2247z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2248z00zz__dssslz00); 
BGl_symbol2249z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2250z00zz__dssslz00); 
BGl_symbol2251z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2252z00zz__dssslz00); 
BGl_list2244z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2242z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2251z00zz__dssslz00, BNIL)))))); 
BGl_symbol2253z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2254z00zz__dssslz00); 
BGl_symbol2255z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2256z00zz__dssslz00); 
BGl_symbol2263z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2264z00zz__dssslz00); 
BGl_list2262z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2260z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2263z00zz__dssslz00, BNIL)))))); 
BGl_symbol2268z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2269z00zz__dssslz00); 
BGl_list2267z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2265z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2268z00zz__dssslz00, BNIL)))))); 
BGl_symbol2272z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2273z00zz__dssslz00); 
BGl_symbol2274z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2275z00zz__dssslz00); 
BGl_symbol2276z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2277z00zz__dssslz00); 
BGl_symbol2279z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2280z00zz__dssslz00); 
BGl_symbol2281z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2282z00zz__dssslz00); 
BGl_symbol2284z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2285z00zz__dssslz00); 
BGl_symbol2286z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2287z00zz__dssslz00); 
BGl_symbol2288z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2289z00zz__dssslz00); 
BGl_symbol2291z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2292z00zz__dssslz00); 
BGl_symbol2293z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2294z00zz__dssslz00); 
BGl_symbol2295z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2296z00zz__dssslz00); 
BGl_symbol2297z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2298z00zz__dssslz00); 
BGl_symbol2303z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2304z00zz__dssslz00); 
BGl_list2302z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2300z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))); 
BGl_list2306z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2305z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))); 
BGl_symbol2309z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2310z00zz__dssslz00); 
BGl_symbol2311z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2312z00zz__dssslz00); 
BGl_symbol2313z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2314z00zz__dssslz00); 
BGl_symbol2315z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2316z00zz__dssslz00); 
BGl_symbol2317z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2318z00zz__dssslz00); 
BGl_symbol2319z00zz__dssslz00 = 
bstring_to_symbol(BGl_string2320z00zz__dssslz00); 
BGl_list2323z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2321z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))); 
BGl_list2327z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2325z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))); 
BGl_list2347z00zz__dssslz00 = 
MAKE_YOUNG_PAIR((BOPTIONAL), 
MAKE_YOUNG_PAIR((BREST), 
MAKE_YOUNG_PAIR((BKEY), BNIL))); 
BGl_list2351z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2348z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2349z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))); 
BGl_list2353z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2348z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2352z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))); 
return ( 
BGl_list2356z00zz__dssslz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2354z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_string2355z00zz__dssslz00, 
MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__dssslz00, BNIL)))))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__dssslz00(void)
{
{ /* Llib/dsssl.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* dsssl-named-constant? */
BGL_EXPORTED_DEF bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t BgL_objz00_3)
{
{ /* Llib/dsssl.scm 59 */
if(
CNSTP(BgL_objz00_3))
{ /* Llib/dsssl.scm 60 */
return 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_objz00_3, BGl_list2241z00zz__dssslz00));}  else 
{ /* Llib/dsssl.scm 60 */
return ((bool_t)0);} } 

}



/* &dsssl-named-constant? */
obj_t BGl_z62dssslzd2namedzd2constantzf3z91zz__dssslz00(obj_t BgL_envz00_2524, obj_t BgL_objz00_2525)
{
{ /* Llib/dsssl.scm 59 */
return 
BBOOL(
BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_objz00_2525));} 

}



/* make-dsssl-function-prelude */
BGL_EXPORTED_DEF obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t BgL_wherez00_4, obj_t BgL_formalsz00_5, obj_t BgL_bodyz00_6, obj_t BgL_errz00_7)
{
{ /* Llib/dsssl.scm 71 */
{ /* Llib/dsssl.scm 75 */
 obj_t BgL_nozd2restzd2keyzd2statezd2_2527; obj_t BgL_restzd2statezd2_2528; obj_t BgL_optionalzd2statezd2_2529;
{ 
 int BgL_tmpz00_2974;
BgL_tmpz00_2974 = 
(int)(4L); 
BgL_nozd2restzd2keyzd2statezd2_2527 = 
MAKE_L_PROCEDURE(BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00, BgL_tmpz00_2974); } 
{ 
 int BgL_tmpz00_2977;
BgL_tmpz00_2977 = 
(int)(4L); 
BgL_restzd2statezd2_2528 = 
MAKE_L_PROCEDURE(BGl_z62restzd2statezb0zz__dssslz00, BgL_tmpz00_2977); } 
{ 
 int BgL_tmpz00_2980;
BgL_tmpz00_2980 = 
(int)(6L); 
BgL_optionalzd2statezd2_2529 = 
MAKE_L_PROCEDURE(BGl_z62optionalzd2statezb0zz__dssslz00, BgL_tmpz00_2980); } 
PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2527, 
(int)(0L), BgL_bodyz00_6); 
PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2527, 
(int)(1L), BgL_errz00_7); 
PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2527, 
(int)(2L), BgL_wherez00_4); 
PROCEDURE_L_SET(BgL_nozd2restzd2keyzd2statezd2_2527, 
(int)(3L), BgL_formalsz00_5); 
PROCEDURE_L_SET(BgL_restzd2statezd2_2528, 
(int)(0L), BgL_errz00_7); 
PROCEDURE_L_SET(BgL_restzd2statezd2_2528, 
(int)(1L), BgL_wherez00_4); 
PROCEDURE_L_SET(BgL_restzd2statezd2_2528, 
(int)(2L), BgL_formalsz00_5); 
PROCEDURE_L_SET(BgL_restzd2statezd2_2528, 
(int)(3L), BgL_bodyz00_6); 
PROCEDURE_L_SET(BgL_optionalzd2statezd2_2529, 
(int)(0L), BgL_errz00_7); 
PROCEDURE_L_SET(BgL_optionalzd2statezd2_2529, 
(int)(1L), BgL_wherez00_4); 
PROCEDURE_L_SET(BgL_optionalzd2statezd2_2529, 
(int)(2L), BgL_formalsz00_5); 
PROCEDURE_L_SET(BgL_optionalzd2statezd2_2529, 
(int)(3L), BgL_nozd2restzd2keyzd2statezd2_2527); 
PROCEDURE_L_SET(BgL_optionalzd2statezd2_2529, 
(int)(4L), BgL_restzd2statezd2_2528); 
PROCEDURE_L_SET(BgL_optionalzd2statezd2_2529, 
(int)(5L), BgL_bodyz00_6); 
{ 
 obj_t BgL_argsz00_1136; obj_t BgL_nextzd2statezd2_1137; obj_t BgL_argsz00_1107;
BgL_argsz00_1107 = BgL_formalsz00_5; 
BgL_zc3z04anonymousza31165ze3z87_1108:
if(
PAIRP(BgL_argsz00_1107))
{ /* Llib/dsssl.scm 77 */
 bool_t BgL_test2448z00_3013;
{ /* Llib/dsssl.scm 77 */
 bool_t BgL_test2449z00_3014;
{ /* Llib/dsssl.scm 77 */
 obj_t BgL_tmpz00_3015;
BgL_tmpz00_3015 = 
CAR(BgL_argsz00_1107); 
BgL_test2449z00_3014 = 
SYMBOLP(BgL_tmpz00_3015); } 
if(BgL_test2449z00_3014)
{ /* Llib/dsssl.scm 77 */
BgL_test2448z00_3013 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 77 */
 bool_t BgL_test2450z00_3018;
{ /* Llib/dsssl.scm 77 */
 obj_t BgL_tmpz00_3019;
BgL_tmpz00_3019 = 
CAR(BgL_argsz00_1107); 
BgL_test2450z00_3018 = 
PAIRP(BgL_tmpz00_3019); } 
if(BgL_test2450z00_3018)
{ /* Llib/dsssl.scm 77 */
BgL_test2448z00_3013 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 77 */
BgL_test2448z00_3013 = ((bool_t)1)
; } } } 
if(BgL_test2448z00_3013)
{ /* Llib/dsssl.scm 77 */
if(
(
CAR(BgL_argsz00_1107)==(BOPTIONAL)))
{ /* Llib/dsssl.scm 81 */
BgL_argsz00_1136 = 
CDR(BgL_argsz00_1107); 
BgL_nextzd2statezd2_1137 = BgL_optionalzd2statezd2_2529; 
BgL_zc3z04anonymousza31211ze3z87_1138:
{ 
 obj_t BgL_asz00_1140;
BgL_asz00_1140 = BgL_argsz00_1136; 
if(
PAIRP(BgL_asz00_1140))
{ 

{ /* Llib/dsssl.scm 99 */
 obj_t BgL_ezd2104zd2_1146;
BgL_ezd2104zd2_1146 = 
CAR(BgL_asz00_1140); 
if(
SYMBOLP(BgL_ezd2104zd2_1146))
{ /* Llib/dsssl.scm 99 */
{ /* Llib/dsssl.scm 101 */
 obj_t BgL_dssslzd2argzd2_1157;
BgL_dssslzd2argzd2_1157 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2253z00zz__dssslz00); 
{ /* Llib/dsssl.scm 102 */
 obj_t BgL_arg1225z00_1158;
{ /* Llib/dsssl.scm 102 */
 obj_t BgL_arg1226z00_1159; obj_t BgL_arg1227z00_1160;
{ /* Llib/dsssl.scm 102 */
 obj_t BgL_arg1228z00_1161;
{ /* Llib/dsssl.scm 102 */
 obj_t BgL_arg1229z00_1162;
{ /* Llib/dsssl.scm 102 */
 obj_t BgL_arg1230z00_1163;
BgL_arg1230z00_1163 = 
CAR(
((obj_t)BgL_asz00_1140)); 
BgL_arg1229z00_1162 = 
MAKE_YOUNG_PAIR(BgL_arg1230z00_1163, BNIL); } 
BgL_arg1228z00_1161 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1157, BgL_arg1229z00_1162); } 
BgL_arg1226z00_1159 = 
MAKE_YOUNG_PAIR(BgL_arg1228z00_1161, BNIL); } 
{ /* Llib/dsssl.scm 103 */
 obj_t BgL_arg1231z00_1164;
BgL_arg1231z00_1164 = 
((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_L_ENTRY(BgL_nextzd2statezd2_1137))(BgL_nextzd2statezd2_1137, BgL_argsz00_1136, BgL_dssslzd2argzd2_1157); 
BgL_arg1227z00_1160 = 
MAKE_YOUNG_PAIR(BgL_arg1231z00_1164, BNIL); } 
BgL_arg1225z00_1158 = 
MAKE_YOUNG_PAIR(BgL_arg1226z00_1159, BgL_arg1227z00_1160); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1225z00_1158);} } }  else 
{ /* Llib/dsssl.scm 99 */
if(
PAIRP(BgL_ezd2104zd2_1146))
{ /* Llib/dsssl.scm 99 */
 obj_t BgL_cdrzd2106zd2_1149;
BgL_cdrzd2106zd2_1149 = 
CDR(BgL_ezd2104zd2_1146); 
{ /* Llib/dsssl.scm 99 */
 bool_t BgL_test2455z00_3047;
{ /* Llib/dsssl.scm 99 */
 obj_t BgL_tmpz00_3048;
BgL_tmpz00_3048 = 
CAR(BgL_ezd2104zd2_1146); 
BgL_test2455z00_3047 = 
SYMBOLP(BgL_tmpz00_3048); } 
if(BgL_test2455z00_3047)
{ /* Llib/dsssl.scm 99 */
if(
PAIRP(BgL_cdrzd2106zd2_1149))
{ /* Llib/dsssl.scm 99 */
if(
NULLP(
CDR(BgL_cdrzd2106zd2_1149)))
{ /* Llib/dsssl.scm 99 */
{ /* Llib/dsssl.scm 105 */
 obj_t BgL_dssslzd2argzd2_1165;
BgL_dssslzd2argzd2_1165 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2253z00zz__dssslz00); 
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_arg1232z00_1166;
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_arg1233z00_1167; obj_t BgL_arg1234z00_1168;
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_arg1236z00_1169;
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_arg1238z00_1170;
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_arg1239z00_1171;
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_pairz00_2150;
{ /* Llib/dsssl.scm 106 */
 obj_t BgL_aux2107z00_2605;
BgL_aux2107z00_2605 = 
CAR(
((obj_t)BgL_asz00_1140)); 
if(
PAIRP(BgL_aux2107z00_2605))
{ /* Llib/dsssl.scm 106 */
BgL_pairz00_2150 = BgL_aux2107z00_2605; }  else 
{ 
 obj_t BgL_auxz00_3061;
BgL_auxz00_3061 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4202L), BGl_string2258z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2107z00_2605); 
FAILURE(BgL_auxz00_3061,BFALSE,BFALSE);} } 
BgL_arg1239z00_1171 = 
CAR(BgL_pairz00_2150); } 
BgL_arg1238z00_1170 = 
MAKE_YOUNG_PAIR(BgL_arg1239z00_1171, BNIL); } 
BgL_arg1236z00_1169 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1165, BgL_arg1238z00_1170); } 
BgL_arg1233z00_1167 = 
MAKE_YOUNG_PAIR(BgL_arg1236z00_1169, BNIL); } 
{ /* Llib/dsssl.scm 107 */
 obj_t BgL_arg1244z00_1173;
BgL_arg1244z00_1173 = 
((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_L_ENTRY(BgL_nextzd2statezd2_1137))(BgL_nextzd2statezd2_1137, BgL_argsz00_1136, BgL_dssslzd2argzd2_1165); 
BgL_arg1234z00_1168 = 
MAKE_YOUNG_PAIR(BgL_arg1244z00_1173, BNIL); } 
BgL_arg1232z00_1166 = 
MAKE_YOUNG_PAIR(BgL_arg1233z00_1167, BgL_arg1234z00_1168); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1232z00_1166);} } }  else 
{ /* Llib/dsssl.scm 99 */
BgL_tagzd2103zd2_1145:
{ /* Llib/dsssl.scm 109 */
 obj_t BgL_arg1248z00_1174;
{ /* Llib/dsssl.scm 109 */
 obj_t BgL_arg1249z00_1175;
BgL_arg1249z00_1175 = 
CAR(
((obj_t)BgL_asz00_1140)); 
BgL_arg1248z00_1174 = 
MAKE_YOUNG_PAIR(BgL_arg1249z00_1175, BgL_formalsz00_5); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_7, 3))
{ /* Llib/dsssl.scm 109 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_7, BgL_wherez00_4, BGl_string2260z00zz__dssslz00, BgL_arg1248z00_1174);}  else 
{ /* Llib/dsssl.scm 109 */
FAILURE(BGl_string2261z00zz__dssslz00,BGl_list2262z00zz__dssslz00,BgL_errz00_7);} } } }  else 
{ /* Llib/dsssl.scm 99 */
goto BgL_tagzd2103zd2_1145;} }  else 
{ /* Llib/dsssl.scm 99 */
goto BgL_tagzd2103zd2_1145;} } }  else 
{ /* Llib/dsssl.scm 99 */
goto BgL_tagzd2103zd2_1145;} } } }  else 
{ /* Llib/dsssl.scm 97 */
 obj_t BgL_arg1252z00_1176;
BgL_arg1252z00_1176 = 
MAKE_YOUNG_PAIR(BgL_asz00_1140, BgL_formalsz00_5); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_7, 3))
{ /* Llib/dsssl.scm 97 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_7, BgL_wherez00_4, BGl_string2265z00zz__dssslz00, BgL_arg1252z00_1176);}  else 
{ /* Llib/dsssl.scm 97 */
FAILURE(BGl_string2266z00zz__dssslz00,BGl_list2267z00zz__dssslz00,BgL_errz00_7);} } } }  else 
{ /* Llib/dsssl.scm 81 */
if(
(
CAR(BgL_argsz00_1107)==(BREST)))
{ 
 obj_t BgL_nextzd2statezd2_3105; obj_t BgL_argsz00_3103;
BgL_argsz00_3103 = 
CDR(BgL_argsz00_1107); 
BgL_nextzd2statezd2_3105 = BgL_restzd2statezd2_2528; 
BgL_nextzd2statezd2_1137 = BgL_nextzd2statezd2_3105; 
BgL_argsz00_1136 = BgL_argsz00_3103; 
goto BgL_zc3z04anonymousza31211ze3z87_1138;}  else 
{ /* Llib/dsssl.scm 83 */
if(
(
CAR(BgL_argsz00_1107)==(BKEY)))
{ 
 obj_t BgL_nextzd2statezd2_3111; obj_t BgL_argsz00_3109;
BgL_argsz00_3109 = 
CDR(BgL_argsz00_1107); 
BgL_nextzd2statezd2_3111 = BgL_nozd2restzd2keyzd2statezd2_2527; 
BgL_nextzd2statezd2_1137 = BgL_nextzd2statezd2_3111; 
BgL_argsz00_1136 = BgL_argsz00_3109; 
goto BgL_zc3z04anonymousza31211ze3z87_1138;}  else 
{ /* Llib/dsssl.scm 88 */
 obj_t BgL_arg1200z00_1126;
BgL_arg1200z00_1126 = 
MAKE_YOUNG_PAIR(
CAR(BgL_argsz00_1107), BgL_formalsz00_5); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_7, 3))
{ /* Llib/dsssl.scm 88 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_7, BgL_wherez00_4, BGl_string2242z00zz__dssslz00, BgL_arg1200z00_1126);}  else 
{ /* Llib/dsssl.scm 88 */
FAILURE(BGl_string2243z00zz__dssslz00,BGl_list2244z00zz__dssslz00,BgL_errz00_7);} } } } }  else 
{ 
 obj_t BgL_argsz00_3123;
BgL_argsz00_3123 = 
CDR(BgL_argsz00_1107); 
BgL_argsz00_1107 = BgL_argsz00_3123; 
goto BgL_zc3z04anonymousza31165ze3z87_1108;} }  else 
{ /* Llib/dsssl.scm 75 */
return BgL_bodyz00_6;} } } } 

}



/* &make-dsssl-function-prelude */
obj_t BGl_z62makezd2dssslzd2functionzd2preludezb0zz__dssslz00(obj_t BgL_envz00_2530, obj_t BgL_wherez00_2531, obj_t BgL_formalsz00_2532, obj_t BgL_bodyz00_2533, obj_t BgL_errz00_2534)
{
{ /* Llib/dsssl.scm 71 */
{ /* Llib/dsssl.scm 75 */
 obj_t BgL_auxz00_3125;
if(
PROCEDUREP(BgL_errz00_2534))
{ /* Llib/dsssl.scm 75 */
BgL_auxz00_3125 = BgL_errz00_2534
; }  else 
{ 
 obj_t BgL_auxz00_3128;
BgL_auxz00_3128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(3130L), BGl_string2270z00zz__dssslz00, BGl_string2271z00zz__dssslz00, BgL_errz00_2534); 
FAILURE(BgL_auxz00_3128,BFALSE,BFALSE);} 
return 
BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(BgL_wherez00_2531, BgL_formalsz00_2532, BgL_bodyz00_2533, BgL_auxz00_3125);} } 

}



/* &key-state */
obj_t BGl_z62keyzd2statezb0zz__dssslz00(obj_t BgL_bodyz00_2538, obj_t BgL_formalsz00_2537, obj_t BgL_wherez00_2536, obj_t BgL_errz00_2535, obj_t BgL_argsz00_1424, obj_t BgL_dssslzd2argzd2_1425, obj_t BgL_collectedzd2keyszd2_1426, bool_t BgL_allowzd2restpzd2_1427)
{
{ /* Llib/dsssl.scm 264 */
{ 
 obj_t BgL_argz00_1514; obj_t BgL_initializa7erza7_1515; obj_t BgL_collectedzd2keyszd2_1516; obj_t BgL_argz00_1532; obj_t BgL_bodyz00_1533;
if(
NULLP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 271 */
if(BgL_allowzd2restpzd2_1427)
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1481z00_1432;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1482z00_1433; obj_t BgL_arg1483z00_1434;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1484z00_1435;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1485z00_1436;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1486z00_1437;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1487z00_1438;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1488z00_1439;
{ /* Llib/dsssl.scm 274 */
 obj_t BgL_arg1489z00_1440;
BgL_arg1489z00_1440 = 
MAKE_YOUNG_PAIR(BgL_collectedzd2keyszd2_1426, BNIL); 
BgL_arg1488z00_1439 = 
MAKE_YOUNG_PAIR(BGl_symbol2272z00zz__dssslz00, BgL_arg1489z00_1440); } 
BgL_arg1487z00_1438 = 
MAKE_YOUNG_PAIR(BgL_arg1488z00_1439, BNIL); } 
BgL_arg1486z00_1437 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1425, BgL_arg1487z00_1438); } 
BgL_arg1485z00_1436 = 
MAKE_YOUNG_PAIR(BGl_symbol2274z00zz__dssslz00, BgL_arg1486z00_1437); } 
BgL_arg1484z00_1435 = 
MAKE_YOUNG_PAIR(BgL_arg1485z00_1436, BNIL); } 
BgL_arg1482z00_1433 = 
MAKE_YOUNG_PAIR(BGl_symbol2279z00zz__dssslz00, BgL_arg1484z00_1435); } 
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1490z00_1441;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1492z00_1442;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1494z00_1443;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1495z00_1444;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1497z00_1445; obj_t BgL_arg1498z00_1446;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1499z00_1447;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1500z00_1448;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1501z00_1449;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1502z00_1450;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1503z00_1451;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1504z00_1452; obj_t BgL_arg1505z00_1453;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1506z00_1454;
{ /* Llib/dsssl.scm 278 */
 obj_t BgL_arg1507z00_1455; obj_t BgL_arg1508z00_1456;
BgL_arg1507z00_1455 = 
MAKE_YOUNG_PAIR(BGl_symbol2281z00zz__dssslz00, BNIL); 
{ /* Llib/dsssl.scm 279 */
 obj_t BgL_arg1509z00_1457;
{ /* Llib/dsssl.scm 279 */
 obj_t BgL_arg1510z00_1458;
{ /* Llib/dsssl.scm 279 */
 obj_t BgL_arg1511z00_1459;
BgL_arg1511z00_1459 = 
MAKE_YOUNG_PAIR(BGl_symbol2281z00zz__dssslz00, BNIL); 
BgL_arg1510z00_1458 = 
MAKE_YOUNG_PAIR(BGl_string2283z00zz__dssslz00, BgL_arg1511z00_1459); } 
BgL_arg1509z00_1457 = 
MAKE_YOUNG_PAIR(BGl_symbol2284z00zz__dssslz00, BgL_arg1510z00_1458); } 
BgL_arg1508z00_1456 = 
MAKE_YOUNG_PAIR(BgL_arg1509z00_1457, BNIL); } 
BgL_arg1506z00_1454 = 
MAKE_YOUNG_PAIR(BgL_arg1507z00_1455, BgL_arg1508z00_1456); } 
BgL_arg1504z00_1452 = 
MAKE_YOUNG_PAIR(BGl_symbol2286z00zz__dssslz00, BgL_arg1506z00_1454); } 
{ /* Llib/dsssl.scm 280 */
 obj_t BgL_arg1513z00_1460;
{ /* Llib/dsssl.scm 280 */
 obj_t BgL_arg1514z00_1461;
{ /* Llib/dsssl.scm 280 */
 obj_t BgL_arg1516z00_1462;
{ /* Llib/dsssl.scm 280 */
 obj_t BgL_arg1517z00_1463;
{ /* Llib/dsssl.scm 280 */
 obj_t BgL_arg1521z00_1464;
BgL_arg1521z00_1464 = 
MAKE_YOUNG_PAIR(BgL_collectedzd2keyszd2_1426, BNIL); 
BgL_arg1517z00_1463 = 
MAKE_YOUNG_PAIR(BGl_symbol2272z00zz__dssslz00, BgL_arg1521z00_1464); } 
BgL_arg1516z00_1462 = 
MAKE_YOUNG_PAIR(BgL_arg1517z00_1463, BNIL); } 
BgL_arg1514z00_1461 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1425, BgL_arg1516z00_1462); } 
BgL_arg1513z00_1460 = 
MAKE_YOUNG_PAIR(BGl_symbol2274z00zz__dssslz00, BgL_arg1514z00_1461); } 
BgL_arg1505z00_1453 = 
MAKE_YOUNG_PAIR(BgL_arg1513z00_1460, BNIL); } 
BgL_arg1503z00_1451 = 
MAKE_YOUNG_PAIR(BgL_arg1504z00_1452, BgL_arg1505z00_1453); } 
BgL_arg1502z00_1450 = 
MAKE_YOUNG_PAIR(BGl_symbol2288z00zz__dssslz00, BgL_arg1503z00_1451); } 
BgL_arg1501z00_1449 = 
MAKE_YOUNG_PAIR(BgL_arg1502z00_1450, BNIL); } 
BgL_arg1500z00_1448 = 
MAKE_YOUNG_PAIR(BGl_string2290z00zz__dssslz00, BgL_arg1501z00_1449); } 
BgL_arg1499z00_1447 = 
MAKE_YOUNG_PAIR(BGl_symbol2291z00zz__dssslz00, BgL_arg1500z00_1448); } 
BgL_arg1497z00_1445 = 
MAKE_YOUNG_PAIR(BGl_symbol2293z00zz__dssslz00, BgL_arg1499z00_1447); } 
BgL_arg1498z00_1446 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1425, BNIL); 
BgL_arg1495z00_1444 = 
MAKE_YOUNG_PAIR(BgL_arg1497z00_1445, BgL_arg1498z00_1446); } 
BgL_arg1494z00_1443 = 
MAKE_YOUNG_PAIR(BGl_string2277z00zz__dssslz00, BgL_arg1495z00_1444); } 
BgL_arg1492z00_1442 = 
MAKE_YOUNG_PAIR(BGl_symbol2295z00zz__dssslz00, BgL_arg1494z00_1443); } 
BgL_arg1490z00_1441 = 
MAKE_YOUNG_PAIR(BgL_arg1492z00_1442, BNIL); } 
BgL_arg1483z00_1434 = 
MAKE_YOUNG_PAIR(BgL_bodyz00_2538, BgL_arg1490z00_1441); } 
BgL_arg1481z00_1432 = 
MAKE_YOUNG_PAIR(BgL_arg1482z00_1433, BgL_arg1483z00_1434); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2297z00zz__dssslz00, BgL_arg1481z00_1432);}  else 
{ /* Llib/dsssl.scm 272 */
return BgL_bodyz00_2538;} }  else 
{ /* Llib/dsssl.scm 284 */
 bool_t BgL_test2467z00_3170;
{ /* Llib/dsssl.scm 284 */
 obj_t BgL_tmpz00_3171;
{ /* Llib/dsssl.scm 284 */
 obj_t BgL_pairz00_2229;
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 284 */
BgL_pairz00_2229 = BgL_argsz00_1424; }  else 
{ 
 obj_t BgL_auxz00_3174;
BgL_auxz00_3174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9565L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1424); 
FAILURE(BgL_auxz00_3174,BFALSE,BFALSE);} 
BgL_tmpz00_3171 = 
CAR(BgL_pairz00_2229); } 
BgL_test2467z00_3170 = 
(BgL_tmpz00_3171==(BREST)); } 
if(BgL_test2467z00_3170)
{ /* Llib/dsssl.scm 285 */
 bool_t BgL_test2469z00_3180;
if(BgL_allowzd2restpzd2_1427)
{ /* Llib/dsssl.scm 286 */
 bool_t BgL_test2471z00_3182;
{ /* Llib/dsssl.scm 286 */
 obj_t BgL_tmpz00_3183;
{ /* Llib/dsssl.scm 286 */
 obj_t BgL_pairz00_2230;
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 286 */
BgL_pairz00_2230 = BgL_argsz00_1424; }  else 
{ 
 obj_t BgL_auxz00_3186;
BgL_auxz00_3186 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9624L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1424); 
FAILURE(BgL_auxz00_3186,BFALSE,BFALSE);} 
BgL_tmpz00_3183 = 
CDR(BgL_pairz00_2230); } 
BgL_test2471z00_3182 = 
NULLP(BgL_tmpz00_3183); } 
if(BgL_test2471z00_3182)
{ /* Llib/dsssl.scm 286 */
BgL_test2469z00_3180 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 287 */
 bool_t BgL_test2473z00_3192;
{ /* Llib/dsssl.scm 287 */
 obj_t BgL_tmpz00_3193;
{ /* Llib/dsssl.scm 287 */
 obj_t BgL_pairz00_2231;
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 287 */
BgL_pairz00_2231 = BgL_argsz00_1424; }  else 
{ 
 obj_t BgL_auxz00_3196;
BgL_auxz00_3196 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9655L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1424); 
FAILURE(BgL_auxz00_3196,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 287 */
 obj_t BgL_pairz00_2234;
{ /* Llib/dsssl.scm 287 */
 obj_t BgL_aux2123z00_2623;
BgL_aux2123z00_2623 = 
CDR(BgL_pairz00_2231); 
if(
PAIRP(BgL_aux2123z00_2623))
{ /* Llib/dsssl.scm 287 */
BgL_pairz00_2234 = BgL_aux2123z00_2623; }  else 
{ 
 obj_t BgL_auxz00_3203;
BgL_auxz00_3203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9649L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2123z00_2623); 
FAILURE(BgL_auxz00_3203,BFALSE,BFALSE);} } 
BgL_tmpz00_3193 = 
CAR(BgL_pairz00_2234); } } 
BgL_test2473z00_3192 = 
SYMBOLP(BgL_tmpz00_3193); } 
if(BgL_test2473z00_3192)
{ /* Llib/dsssl.scm 288 */
 obj_t BgL_tmpz00_3209;
{ /* Llib/dsssl.scm 288 */
 obj_t BgL_pairz00_2235;
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 288 */
BgL_pairz00_2235 = BgL_argsz00_1424; }  else 
{ 
 obj_t BgL_auxz00_3212;
BgL_auxz00_3212 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9680L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1424); 
FAILURE(BgL_auxz00_3212,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 288 */
 obj_t BgL_pairz00_2238;
{ /* Llib/dsssl.scm 288 */
 obj_t BgL_aux2127z00_2627;
BgL_aux2127z00_2627 = 
CDR(BgL_pairz00_2235); 
if(
PAIRP(BgL_aux2127z00_2627))
{ /* Llib/dsssl.scm 288 */
BgL_pairz00_2238 = BgL_aux2127z00_2627; }  else 
{ 
 obj_t BgL_auxz00_3219;
BgL_auxz00_3219 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9674L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2127z00_2627); 
FAILURE(BgL_auxz00_3219,BFALSE,BFALSE);} } 
BgL_tmpz00_3209 = 
CDR(BgL_pairz00_2238); } } 
BgL_test2469z00_3180 = 
PAIRP(BgL_tmpz00_3209); }  else 
{ /* Llib/dsssl.scm 287 */
BgL_test2469z00_3180 = ((bool_t)1)
; } } }  else 
{ /* Llib/dsssl.scm 285 */
BgL_test2469z00_3180 = ((bool_t)1)
; } 
if(BgL_test2469z00_3180)
{ /* Llib/dsssl.scm 285 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 289 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2300z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 289 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2302z00zz__dssslz00,BgL_errz00_2535);} }  else 
{ /* Llib/dsssl.scm 290 */
 obj_t BgL_arg1535z00_1475;
{ /* Llib/dsssl.scm 290 */
 obj_t BgL_pairz00_2239;
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 290 */
BgL_pairz00_2239 = BgL_argsz00_1424; }  else 
{ 
 obj_t BgL_auxz00_3236;
BgL_auxz00_3236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9779L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1424); 
FAILURE(BgL_auxz00_3236,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 290 */
 obj_t BgL_pairz00_2242;
{ /* Llib/dsssl.scm 290 */
 obj_t BgL_aux2132z00_2633;
BgL_aux2132z00_2633 = 
CDR(BgL_pairz00_2239); 
if(
PAIRP(BgL_aux2132z00_2633))
{ /* Llib/dsssl.scm 290 */
BgL_pairz00_2242 = BgL_aux2132z00_2633; }  else 
{ 
 obj_t BgL_auxz00_3243;
BgL_auxz00_3243 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(9773L), BGl_string2299z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2132z00_2633); 
FAILURE(BgL_auxz00_3243,BFALSE,BFALSE);} } 
BgL_arg1535z00_1475 = 
CAR(BgL_pairz00_2242); } } 
BgL_argz00_1532 = BgL_arg1535z00_1475; 
BgL_bodyz00_1533 = BgL_bodyz00_2538; 
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1593z00_1535;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1594z00_1536; obj_t BgL_arg1595z00_1537;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1598z00_1538;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1601z00_1539;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1602z00_1540;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1603z00_1541;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1605z00_1542;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1606z00_1543;
{ /* Llib/dsssl.scm 267 */
 obj_t BgL_arg1607z00_1544;
BgL_arg1607z00_1544 = 
MAKE_YOUNG_PAIR(BgL_collectedzd2keyszd2_1426, BNIL); 
BgL_arg1606z00_1543 = 
MAKE_YOUNG_PAIR(BGl_symbol2272z00zz__dssslz00, BgL_arg1607z00_1544); } 
BgL_arg1605z00_1542 = 
MAKE_YOUNG_PAIR(BgL_arg1606z00_1543, BNIL); } 
BgL_arg1603z00_1541 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1425, BgL_arg1605z00_1542); } 
BgL_arg1602z00_1540 = 
MAKE_YOUNG_PAIR(BGl_symbol2274z00zz__dssslz00, BgL_arg1603z00_1541); } 
BgL_arg1601z00_1539 = 
MAKE_YOUNG_PAIR(BgL_arg1602z00_1540, BNIL); } 
BgL_arg1598z00_1538 = 
MAKE_YOUNG_PAIR(BgL_argz00_1532, BgL_arg1601z00_1539); } 
BgL_arg1594z00_1536 = 
MAKE_YOUNG_PAIR(BgL_arg1598z00_1538, BNIL); } 
BgL_arg1595z00_1537 = 
MAKE_YOUNG_PAIR(BgL_bodyz00_1533, BNIL); 
BgL_arg1593z00_1535 = 
MAKE_YOUNG_PAIR(BgL_arg1594z00_1536, BgL_arg1595z00_1537); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1593z00_1535);} } }  else 
{ /* Llib/dsssl.scm 284 */
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 293 */
 bool_t BgL_test2482z00_3261;
{ /* Llib/dsssl.scm 293 */
 bool_t BgL_test2483z00_3262;
{ /* Llib/dsssl.scm 293 */
 obj_t BgL_tmpz00_3263;
BgL_tmpz00_3263 = 
CAR(BgL_argsz00_1424); 
BgL_test2483z00_3262 = 
SYMBOLP(BgL_tmpz00_3263); } 
if(BgL_test2483z00_3262)
{ /* Llib/dsssl.scm 293 */
BgL_test2482z00_3261 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 293 */
 bool_t BgL_test2484z00_3266;
{ /* Llib/dsssl.scm 293 */
 obj_t BgL_tmpz00_3267;
BgL_tmpz00_3267 = 
CAR(BgL_argsz00_1424); 
BgL_test2484z00_3266 = 
PAIRP(BgL_tmpz00_3267); } 
if(BgL_test2484z00_3266)
{ /* Llib/dsssl.scm 293 */
BgL_test2482z00_3261 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 293 */
BgL_test2482z00_3261 = ((bool_t)1)
; } } } 
if(BgL_test2482z00_3261)
{ /* Llib/dsssl.scm 293 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 294 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 294 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2535);} }  else 
{ /* Llib/dsssl.scm 297 */
 obj_t BgL_ezd2162zd2_1495;
BgL_ezd2162zd2_1495 = 
CAR(BgL_argsz00_1424); 
if(
PAIRP(BgL_ezd2162zd2_1495))
{ /* Llib/dsssl.scm 297 */
 obj_t BgL_carzd2167zd2_1497; obj_t BgL_cdrzd2168zd2_1498;
BgL_carzd2167zd2_1497 = 
CAR(BgL_ezd2162zd2_1495); 
BgL_cdrzd2168zd2_1498 = 
CDR(BgL_ezd2162zd2_1495); 
if(
SYMBOLP(BgL_carzd2167zd2_1497))
{ /* Llib/dsssl.scm 297 */
if(
PAIRP(BgL_cdrzd2168zd2_1498))
{ /* Llib/dsssl.scm 297 */
if(
NULLP(
CDR(BgL_cdrzd2168zd2_1498)))
{ /* Llib/dsssl.scm 297 */
BgL_argz00_1514 = BgL_carzd2167zd2_1497; 
BgL_initializa7erza7_1515 = 
CAR(BgL_cdrzd2168zd2_1498); 
BgL_collectedzd2keyszd2_1516 = BgL_collectedzd2keyszd2_1426; 
BgL_zc3z04anonymousza31572ze3z87_1517:
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1573z00_1518;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1575z00_1519; obj_t BgL_arg1576z00_1520;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1578z00_1521;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1579z00_1522;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1580z00_1523;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1582z00_1524;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1583z00_1525;
{ /* Llib/dsssl.scm 260 */
 obj_t BgL_arg1584z00_1526; obj_t BgL_arg1585z00_1527;
BgL_arg1584z00_1526 = 
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_argz00_1514); 
BgL_arg1585z00_1527 = 
MAKE_YOUNG_PAIR(BgL_initializa7erza7_1515, BNIL); 
BgL_arg1583z00_1525 = 
MAKE_YOUNG_PAIR(BgL_arg1584z00_1526, BgL_arg1585z00_1527); } 
BgL_arg1582z00_1524 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_1425, BgL_arg1583z00_1525); } 
BgL_arg1580z00_1523 = 
MAKE_YOUNG_PAIR(BGl_symbol2276z00zz__dssslz00, BgL_arg1582z00_1524); } 
BgL_arg1579z00_1522 = 
MAKE_YOUNG_PAIR(BgL_arg1580z00_1523, BNIL); } 
BgL_arg1578z00_1521 = 
MAKE_YOUNG_PAIR(BgL_argz00_1514, BgL_arg1579z00_1522); } 
BgL_arg1575z00_1519 = 
MAKE_YOUNG_PAIR(BgL_arg1578z00_1521, BNIL); } 
{ /* Llib/dsssl.scm 261 */
 obj_t BgL_arg1586z00_1528;
{ /* Llib/dsssl.scm 261 */
 obj_t BgL_arg1587z00_1529; obj_t BgL_arg1589z00_1530;
{ /* Llib/dsssl.scm 261 */
 obj_t BgL_pairz00_2228;
if(
PAIRP(BgL_argsz00_1424))
{ /* Llib/dsssl.scm 261 */
BgL_pairz00_2228 = BgL_argsz00_1424; }  else 
{ 
 obj_t BgL_auxz00_3301;
BgL_auxz00_3301 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(8834L), BGl_string2278z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1424); 
FAILURE(BgL_auxz00_3301,BFALSE,BFALSE);} 
BgL_arg1587z00_1529 = 
CDR(BgL_pairz00_2228); } 
BgL_arg1589z00_1530 = 
MAKE_YOUNG_PAIR(
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_argz00_1514), BgL_collectedzd2keyszd2_1516); 
BgL_arg1586z00_1528 = 
BGl_z62keyzd2statezb0zz__dssslz00(BgL_bodyz00_2538, BgL_formalsz00_2537, BgL_wherez00_2536, BgL_errz00_2535, BgL_arg1587z00_1529, BgL_dssslzd2argzd2_1425, BgL_arg1589z00_1530, BgL_allowzd2restpzd2_1427); } 
BgL_arg1576z00_1520 = 
MAKE_YOUNG_PAIR(BgL_arg1586z00_1528, BNIL); } 
BgL_arg1573z00_1518 = 
MAKE_YOUNG_PAIR(BgL_arg1575z00_1519, BgL_arg1576z00_1520); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1573z00_1518);} }  else 
{ /* Llib/dsssl.scm 297 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 303 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 303 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2535);} } }  else 
{ /* Llib/dsssl.scm 297 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 303 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 303 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2535);} } }  else 
{ /* Llib/dsssl.scm 297 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 303 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 303 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2535);} } }  else 
{ /* Llib/dsssl.scm 297 */
if(
SYMBOLP(BgL_ezd2162zd2_1495))
{ 
 obj_t BgL_collectedzd2keyszd2_3344; obj_t BgL_initializa7erza7_3343; obj_t BgL_argz00_3342;
BgL_argz00_3342 = BgL_ezd2162zd2_1495; 
BgL_initializa7erza7_3343 = BFALSE; 
BgL_collectedzd2keyszd2_3344 = BgL_collectedzd2keyszd2_1426; 
BgL_collectedzd2keyszd2_1516 = BgL_collectedzd2keyszd2_3344; 
BgL_initializa7erza7_1515 = BgL_initializa7erza7_3343; 
BgL_argz00_1514 = BgL_argz00_3342; 
goto BgL_zc3z04anonymousza31572ze3z87_1517;}  else 
{ /* Llib/dsssl.scm 297 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 303 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 303 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2535);} } } } }  else 
{ /* Llib/dsssl.scm 291 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2535, 3))
{ /* Llib/dsssl.scm 292 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2535, BgL_wherez00_2536, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2537);}  else 
{ /* Llib/dsssl.scm 292 */
FAILURE(BGl_string2301z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2535);} } } } } } 

}



/* &optional-state */
obj_t BGl_z62optionalzd2statezb0zz__dssslz00(obj_t BgL_envz00_2539, obj_t BgL_argsz00_2546, obj_t BgL_dssslzd2argzd2_2547)
{
{ /* Llib/dsssl.scm 135 */
{ /* Llib/dsssl.scm 116 */
 obj_t BgL_errz00_2540; obj_t BgL_wherez00_2541; obj_t BgL_formalsz00_2542; obj_t BgL_nozd2restzd2keyzd2statezd2_2543; obj_t BgL_restzd2statezd2_2544; obj_t BgL_bodyz00_2545;
BgL_errz00_2540 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_2539, 
(int)(0L))); 
BgL_wherez00_2541 = 
PROCEDURE_L_REF(BgL_envz00_2539, 
(int)(1L)); 
BgL_formalsz00_2542 = 
PROCEDURE_L_REF(BgL_envz00_2539, 
(int)(2L)); 
BgL_nozd2restzd2keyzd2statezd2_2543 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_2539, 
(int)(3L))); 
BgL_restzd2statezd2_2544 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_2539, 
(int)(4L))); 
BgL_bodyz00_2545 = 
PROCEDURE_L_REF(BgL_envz00_2539, 
(int)(5L)); 
{ 
 obj_t BgL_argsz00_2800;
{ /* Llib/dsssl.scm 183 */
 obj_t BgL_auxz00_3378;
BgL_argsz00_2800 = BgL_argsz00_2546; 
{ 
 obj_t BgL_argsz00_2802;
BgL_argsz00_2802 = BgL_argsz00_2800; 
BgL_loopz00_2801:
if(
PAIRP(BgL_argsz00_2802))
{ /* Llib/dsssl.scm 116 */
if(
(
CAR(BgL_argsz00_2802)==(BKEY)))
{ /* Llib/dsssl.scm 119 */
 obj_t BgL_g1040z00_2803;
BgL_g1040z00_2803 = 
CDR(BgL_argsz00_2802); 
{ 
 obj_t BgL_argsz00_2805; obj_t BgL_resz00_2806;
BgL_argsz00_2805 = BgL_g1040z00_2803; 
BgL_resz00_2806 = BNIL; 
BgL_loopz00_2804:
{ /* Llib/dsssl.scm 122 */
 bool_t BgL_test2499z00_3385;
if(
PAIRP(BgL_argsz00_2805))
{ /* Llib/dsssl.scm 123 */
 bool_t BgL_test2501z00_3388;
{ /* Llib/dsssl.scm 123 */
 bool_t BgL_test2502z00_3389;
{ /* Llib/dsssl.scm 123 */
 obj_t BgL_tmpz00_3390;
BgL_tmpz00_3390 = 
CAR(BgL_argsz00_2805); 
BgL_test2502z00_3389 = 
PAIRP(BgL_tmpz00_3390); } 
if(BgL_test2502z00_3389)
{ /* Llib/dsssl.scm 123 */
BgL_test2501z00_3388 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 124 */
 obj_t BgL_tmpz00_3393;
BgL_tmpz00_3393 = 
CAR(BgL_argsz00_2805); 
BgL_test2501z00_3388 = 
SYMBOLP(BgL_tmpz00_3393); } } 
if(BgL_test2501z00_3388)
{ /* Llib/dsssl.scm 123 */
if(
(
CAR(BgL_argsz00_2805)==(BOPTIONAL)))
{ /* Llib/dsssl.scm 125 */
BgL_test2499z00_3385 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 125 */
BgL_test2499z00_3385 = 
(
CAR(BgL_argsz00_2805)==(BREST))
; } }  else 
{ /* Llib/dsssl.scm 123 */
BgL_test2499z00_3385 = ((bool_t)1)
; } }  else 
{ /* Llib/dsssl.scm 122 */
BgL_test2499z00_3385 = ((bool_t)1)
; } 
if(BgL_test2499z00_3385)
{ /* Llib/dsssl.scm 122 */
BgL_auxz00_3378 = BgL_resz00_2806
; }  else 
{ /* Llib/dsssl.scm 128 */
 bool_t BgL_test2504z00_3401;
{ /* Llib/dsssl.scm 128 */
 obj_t BgL_tmpz00_3402;
{ /* Llib/dsssl.scm 128 */
 obj_t BgL_pairz00_2807;
if(
PAIRP(BgL_argsz00_2805))
{ /* Llib/dsssl.scm 128 */
BgL_pairz00_2807 = BgL_argsz00_2805; }  else 
{ 
 obj_t BgL_auxz00_3405;
BgL_auxz00_3405 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4785L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2805); 
FAILURE(BgL_auxz00_3405,BFALSE,BFALSE);} 
BgL_tmpz00_3402 = 
CAR(BgL_pairz00_2807); } 
BgL_test2504z00_3401 = 
SYMBOLP(BgL_tmpz00_3402); } 
if(BgL_test2504z00_3401)
{ /* Llib/dsssl.scm 129 */
 obj_t BgL_arg1401z00_2808; obj_t BgL_arg1402z00_2809;
{ /* Llib/dsssl.scm 129 */
 obj_t BgL_pairz00_2810;
if(
PAIRP(BgL_argsz00_2805))
{ /* Llib/dsssl.scm 129 */
BgL_pairz00_2810 = BgL_argsz00_2805; }  else 
{ 
 obj_t BgL_auxz00_3413;
BgL_auxz00_3413 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4812L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2805); 
FAILURE(BgL_auxz00_3413,BFALSE,BFALSE);} 
BgL_arg1401z00_2808 = 
CDR(BgL_pairz00_2810); } 
{ /* Llib/dsssl.scm 130 */
 obj_t BgL_arg1403z00_2811;
{ /* Llib/dsssl.scm 130 */
 obj_t BgL_arg1404z00_2812;
{ /* Llib/dsssl.scm 130 */
 obj_t BgL_pairz00_2813;
if(
PAIRP(BgL_argsz00_2805))
{ /* Llib/dsssl.scm 130 */
BgL_pairz00_2813 = BgL_argsz00_2805; }  else 
{ 
 obj_t BgL_auxz00_3420;
BgL_auxz00_3420 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4854L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2805); 
FAILURE(BgL_auxz00_3420,BFALSE,BFALSE);} 
BgL_arg1404z00_2812 = 
CAR(BgL_pairz00_2813); } 
{ /* Llib/dsssl.scm 130 */
 obj_t BgL_auxz00_3425;
if(
SYMBOLP(BgL_arg1404z00_2812))
{ /* Llib/dsssl.scm 130 */
BgL_auxz00_3425 = BgL_arg1404z00_2812
; }  else 
{ 
 obj_t BgL_auxz00_3428;
BgL_auxz00_3428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4858L), BGl_string2307z00zz__dssslz00, BGl_string2308z00zz__dssslz00, BgL_arg1404z00_2812); 
FAILURE(BgL_auxz00_3428,BFALSE,BFALSE);} 
BgL_arg1403z00_2811 = 
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_3425); } } 
BgL_arg1402z00_2809 = 
MAKE_YOUNG_PAIR(BgL_arg1403z00_2811, BgL_resz00_2806); } 
{ 
 obj_t BgL_resz00_3435; obj_t BgL_argsz00_3434;
BgL_argsz00_3434 = BgL_arg1401z00_2808; 
BgL_resz00_3435 = BgL_arg1402z00_2809; 
BgL_resz00_2806 = BgL_resz00_3435; 
BgL_argsz00_2805 = BgL_argsz00_3434; 
goto BgL_loopz00_2804;} }  else 
{ /* Llib/dsssl.scm 132 */
 obj_t BgL_arg1405z00_2814; obj_t BgL_arg1406z00_2815;
{ /* Llib/dsssl.scm 132 */
 obj_t BgL_pairz00_2816;
if(
PAIRP(BgL_argsz00_2805))
{ /* Llib/dsssl.scm 132 */
BgL_pairz00_2816 = BgL_argsz00_2805; }  else 
{ 
 obj_t BgL_auxz00_3438;
BgL_auxz00_3438 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4902L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2805); 
FAILURE(BgL_auxz00_3438,BFALSE,BFALSE);} 
BgL_arg1405z00_2814 = 
CDR(BgL_pairz00_2816); } 
{ /* Llib/dsssl.scm 133 */
 obj_t BgL_arg1407z00_2817;
{ /* Llib/dsssl.scm 133 */
 obj_t BgL_arg1408z00_2818;
{ /* Llib/dsssl.scm 133 */
 obj_t BgL_pairz00_2819;
if(
PAIRP(BgL_argsz00_2805))
{ /* Llib/dsssl.scm 133 */
BgL_pairz00_2819 = BgL_argsz00_2805; }  else 
{ 
 obj_t BgL_auxz00_3445;
BgL_auxz00_3445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4945L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2805); 
FAILURE(BgL_auxz00_3445,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 133 */
 obj_t BgL_pairz00_2820;
{ /* Llib/dsssl.scm 133 */
 obj_t BgL_aux2152z00_2821;
BgL_aux2152z00_2821 = 
CAR(BgL_pairz00_2819); 
if(
PAIRP(BgL_aux2152z00_2821))
{ /* Llib/dsssl.scm 133 */
BgL_pairz00_2820 = BgL_aux2152z00_2821; }  else 
{ 
 obj_t BgL_auxz00_3452;
BgL_auxz00_3452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4939L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2152z00_2821); 
FAILURE(BgL_auxz00_3452,BFALSE,BFALSE);} } 
BgL_arg1408z00_2818 = 
CAR(BgL_pairz00_2820); } } 
{ /* Llib/dsssl.scm 133 */
 obj_t BgL_auxz00_3457;
if(
SYMBOLP(BgL_arg1408z00_2818))
{ /* Llib/dsssl.scm 133 */
BgL_auxz00_3457 = BgL_arg1408z00_2818
; }  else 
{ 
 obj_t BgL_auxz00_3460;
BgL_auxz00_3460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(4949L), BGl_string2307z00zz__dssslz00, BGl_string2308z00zz__dssslz00, BgL_arg1408z00_2818); 
FAILURE(BgL_auxz00_3460,BFALSE,BFALSE);} 
BgL_arg1407z00_2817 = 
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_3457); } } 
BgL_arg1406z00_2815 = 
MAKE_YOUNG_PAIR(BgL_arg1407z00_2817, BgL_resz00_2806); } 
{ 
 obj_t BgL_resz00_3467; obj_t BgL_argsz00_3466;
BgL_argsz00_3466 = BgL_arg1405z00_2814; 
BgL_resz00_3467 = BgL_arg1406z00_2815; 
BgL_resz00_2806 = BgL_resz00_3467; 
BgL_argsz00_2805 = BgL_argsz00_3466; 
goto BgL_loopz00_2804;} } } } } }  else 
{ 
 obj_t BgL_argsz00_3468;
BgL_argsz00_3468 = 
CDR(BgL_argsz00_2802); 
BgL_argsz00_2802 = BgL_argsz00_3468; 
goto BgL_loopz00_2801;} }  else 
{ /* Llib/dsssl.scm 116 */
BgL_auxz00_3378 = BNIL
; } } 
return 
BGl_optionalzd2argsze70z35zz__dssslz00(BgL_bodyz00_2545, BgL_restzd2statezd2_2544, BgL_nozd2restzd2keyzd2statezd2_2543, BgL_formalsz00_2542, BgL_wherez00_2541, BgL_errz00_2540, BgL_auxz00_3378, BgL_dssslzd2argzd2_2547, BgL_argsz00_2546);} } } } 

}



/* optional-args~0 */
obj_t BGl_optionalzd2argsze70z35zz__dssslz00(obj_t BgL_bodyz00_2590, obj_t BgL_restzd2statezd2_2589, obj_t BgL_nozd2restzd2keyzd2statezd2_2588, obj_t BgL_formalsz00_2587, obj_t BgL_wherez00_2586, obj_t BgL_errz00_2585, obj_t BgL_keywordzd2argumentszd2_2584, obj_t BgL_dssslzd2argzd2_2583, obj_t BgL_argsz00_1229)
{
{ /* Llib/dsssl.scm 181 */
{ 
 obj_t BgL_argz00_1185; obj_t BgL_initializa7erza7_1186; obj_t BgL_restz00_1187;
if(
NULLP(BgL_argsz00_1229))
{ /* Llib/dsssl.scm 159 */
return BgL_bodyz00_2590;}  else 
{ /* Llib/dsssl.scm 159 */
if(
PAIRP(BgL_argsz00_1229))
{ /* Llib/dsssl.scm 163 */
 bool_t BgL_test2515z00_3475;
{ /* Llib/dsssl.scm 163 */
 bool_t BgL_test2516z00_3476;
{ /* Llib/dsssl.scm 163 */
 obj_t BgL_tmpz00_3477;
BgL_tmpz00_3477 = 
CAR(BgL_argsz00_1229); 
BgL_test2516z00_3476 = 
SYMBOLP(BgL_tmpz00_3477); } 
if(BgL_test2516z00_3476)
{ /* Llib/dsssl.scm 163 */
BgL_test2515z00_3475 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 163 */
 bool_t BgL_test2517z00_3480;
{ /* Llib/dsssl.scm 163 */
 obj_t BgL_tmpz00_3481;
BgL_tmpz00_3481 = 
CAR(BgL_argsz00_1229); 
BgL_test2517z00_3480 = 
PAIRP(BgL_tmpz00_3481); } 
if(BgL_test2517z00_3480)
{ /* Llib/dsssl.scm 163 */
BgL_test2515z00_3475 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 163 */
BgL_test2515z00_3475 = ((bool_t)1)
; } } } 
if(BgL_test2515z00_3475)
{ /* Llib/dsssl.scm 163 */
if(
(
CAR(BgL_argsz00_1229)==(BREST)))
{ /* Llib/dsssl.scm 167 */
return 
BGl_z62restzd2statezb0zz__dssslz00(BgL_restzd2statezd2_2589, 
CDR(BgL_argsz00_1229), BgL_dssslzd2argzd2_2583);}  else 
{ /* Llib/dsssl.scm 167 */
if(
(
CAR(BgL_argsz00_1229)==(BKEY)))
{ /* Llib/dsssl.scm 169 */
return 
BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00(BgL_nozd2restzd2keyzd2statezd2_2588, 
CDR(BgL_argsz00_1229), BgL_dssslzd2argzd2_2583);}  else 
{ /* Llib/dsssl.scm 169 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2585, 3))
{ /* Llib/dsssl.scm 172 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2585, BgL_wherez00_2586, BGl_string2321z00zz__dssslz00, BgL_formalsz00_2587);}  else 
{ /* Llib/dsssl.scm 172 */
FAILURE(BGl_string2322z00zz__dssslz00,BGl_list2323z00zz__dssslz00,BgL_errz00_2585);} } } }  else 
{ /* Llib/dsssl.scm 175 */
 obj_t BgL_ezd2112zd2_1254;
BgL_ezd2112zd2_1254 = 
CAR(BgL_argsz00_1229); 
if(
PAIRP(BgL_ezd2112zd2_1254))
{ /* Llib/dsssl.scm 175 */
 obj_t BgL_carzd2117zd2_1256; obj_t BgL_cdrzd2118zd2_1257;
BgL_carzd2117zd2_1256 = 
CAR(BgL_ezd2112zd2_1254); 
BgL_cdrzd2118zd2_1257 = 
CDR(BgL_ezd2112zd2_1254); 
if(
SYMBOLP(BgL_carzd2117zd2_1256))
{ /* Llib/dsssl.scm 175 */
if(
PAIRP(BgL_cdrzd2118zd2_1257))
{ /* Llib/dsssl.scm 175 */
if(
NULLP(
CDR(BgL_cdrzd2118zd2_1257)))
{ /* Llib/dsssl.scm 175 */
BgL_argz00_1185 = BgL_carzd2117zd2_1256; 
BgL_initializa7erza7_1186 = 
CAR(BgL_cdrzd2118zd2_1257); 
BgL_restz00_1187 = 
CDR(BgL_argsz00_1229); 
BgL_zc3z04anonymousza31254ze3z87_1188:
{ /* Llib/dsssl.scm 140 */
 obj_t BgL_tmpz00_1189;
BgL_tmpz00_1189 = 
BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2309z00zz__dssslz00); 
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1268z00_1190;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1272z00_1191; obj_t BgL_arg1284z00_1192;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1304z00_1193;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1305z00_1194;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1306z00_1195;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1307z00_1196;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1308z00_1197; obj_t BgL_arg1309z00_1198;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1310z00_1199;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1311z00_1200; obj_t BgL_arg1312z00_1201;
{ /* Llib/dsssl.scm 141 */
 obj_t BgL_arg1314z00_1202;
BgL_arg1314z00_1202 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2583, BNIL); 
BgL_arg1311z00_1200 = 
MAKE_YOUNG_PAIR(BGl_symbol2279z00zz__dssslz00, BgL_arg1314z00_1202); } 
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1315z00_1203;
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1316z00_1204;
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1317z00_1205;
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1318z00_1206; obj_t BgL_arg1319z00_1207;
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1320z00_1208;
BgL_arg1320z00_1208 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2583, BNIL); 
BgL_arg1318z00_1206 = 
MAKE_YOUNG_PAIR(BGl_symbol2311z00zz__dssslz00, BgL_arg1320z00_1208); } 
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1321z00_1209;
{ /* Llib/dsssl.scm 143 */
 obj_t BgL_arg1322z00_1210;
BgL_arg1322z00_1210 = 
MAKE_YOUNG_PAIR(BgL_keywordzd2argumentszd2_2584, BNIL); 
BgL_arg1321z00_1209 = 
MAKE_YOUNG_PAIR(BGl_symbol2272z00zz__dssslz00, BgL_arg1322z00_1210); } 
BgL_arg1319z00_1207 = 
MAKE_YOUNG_PAIR(BgL_arg1321z00_1209, BNIL); } 
BgL_arg1317z00_1205 = 
MAKE_YOUNG_PAIR(BgL_arg1318z00_1206, BgL_arg1319z00_1207); } 
BgL_arg1316z00_1204 = 
MAKE_YOUNG_PAIR(BGl_symbol2313z00zz__dssslz00, BgL_arg1317z00_1205); } 
BgL_arg1315z00_1203 = 
MAKE_YOUNG_PAIR(BgL_arg1316z00_1204, BNIL); } 
BgL_arg1312z00_1201 = 
MAKE_YOUNG_PAIR(BTRUE, BgL_arg1315z00_1203); } 
BgL_arg1310z00_1199 = 
MAKE_YOUNG_PAIR(BgL_arg1311z00_1200, BgL_arg1312z00_1201); } 
BgL_arg1308z00_1197 = 
MAKE_YOUNG_PAIR(BGl_symbol2297z00zz__dssslz00, BgL_arg1310z00_1199); } 
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1323z00_1211;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1325z00_1212;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1326z00_1213;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1327z00_1214; obj_t BgL_arg1328z00_1215;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1329z00_1216;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1331z00_1217;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1332z00_1218;
{ /* Llib/dsssl.scm 145 */
 obj_t BgL_arg1333z00_1219;
BgL_arg1333z00_1219 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2583, BNIL); 
BgL_arg1332z00_1218 = 
MAKE_YOUNG_PAIR(BGl_symbol2311z00zz__dssslz00, BgL_arg1333z00_1219); } 
BgL_arg1331z00_1217 = 
MAKE_YOUNG_PAIR(BgL_arg1332z00_1218, BNIL); } 
BgL_arg1329z00_1216 = 
MAKE_YOUNG_PAIR(BgL_tmpz00_1189, BgL_arg1331z00_1217); } 
BgL_arg1327z00_1214 = 
MAKE_YOUNG_PAIR(BgL_arg1329z00_1216, BNIL); } 
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1334z00_1220;
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1335z00_1221;
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1336z00_1222; obj_t BgL_arg1337z00_1223;
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1338z00_1224;
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1339z00_1225;
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1340z00_1226;
{ /* Llib/dsssl.scm 153 */
 obj_t BgL_arg1341z00_1227;
BgL_arg1341z00_1227 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2583, BNIL); 
BgL_arg1340z00_1226 = 
MAKE_YOUNG_PAIR(BGl_symbol2315z00zz__dssslz00, BgL_arg1341z00_1227); } 
BgL_arg1339z00_1225 = 
MAKE_YOUNG_PAIR(BgL_arg1340z00_1226, BNIL); } 
BgL_arg1338z00_1224 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2583, BgL_arg1339z00_1225); } 
BgL_arg1336z00_1222 = 
MAKE_YOUNG_PAIR(BGl_symbol2317z00zz__dssslz00, BgL_arg1338z00_1224); } 
BgL_arg1337z00_1223 = 
MAKE_YOUNG_PAIR(BgL_tmpz00_1189, BNIL); 
BgL_arg1335z00_1221 = 
MAKE_YOUNG_PAIR(BgL_arg1336z00_1222, BgL_arg1337z00_1223); } 
BgL_arg1334z00_1220 = 
MAKE_YOUNG_PAIR(BGl_symbol2319z00zz__dssslz00, BgL_arg1335z00_1221); } 
BgL_arg1328z00_1215 = 
MAKE_YOUNG_PAIR(BgL_arg1334z00_1220, BNIL); } 
BgL_arg1326z00_1213 = 
MAKE_YOUNG_PAIR(BgL_arg1327z00_1214, BgL_arg1328z00_1215); } 
BgL_arg1325z00_1212 = 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1326z00_1213); } 
BgL_arg1323z00_1211 = 
MAKE_YOUNG_PAIR(BgL_arg1325z00_1212, BNIL); } 
BgL_arg1309z00_1198 = 
MAKE_YOUNG_PAIR(BgL_initializa7erza7_1186, BgL_arg1323z00_1211); } 
BgL_arg1307z00_1196 = 
MAKE_YOUNG_PAIR(BgL_arg1308z00_1197, BgL_arg1309z00_1198); } 
BgL_arg1306z00_1195 = 
MAKE_YOUNG_PAIR(BGl_symbol2297z00zz__dssslz00, BgL_arg1307z00_1196); } 
BgL_arg1305z00_1194 = 
MAKE_YOUNG_PAIR(BgL_arg1306z00_1195, BNIL); } 
BgL_arg1304z00_1193 = 
MAKE_YOUNG_PAIR(BgL_argz00_1185, BgL_arg1305z00_1194); } 
BgL_arg1272z00_1191 = 
MAKE_YOUNG_PAIR(BgL_arg1304z00_1193, BNIL); } 
BgL_arg1284z00_1192 = 
MAKE_YOUNG_PAIR(
BGl_optionalzd2argsze70z35zz__dssslz00(BgL_bodyz00_2590, BgL_restzd2statezd2_2589, BgL_nozd2restzd2keyzd2statezd2_2588, BgL_formalsz00_2587, BgL_wherez00_2586, BgL_errz00_2585, BgL_keywordzd2argumentszd2_2584, BgL_dssslzd2argzd2_2583, BgL_restz00_1187), BNIL); 
BgL_arg1268z00_1190 = 
MAKE_YOUNG_PAIR(BgL_arg1272z00_1191, BgL_arg1284z00_1192); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1268z00_1190);} } }  else 
{ /* Llib/dsssl.scm 175 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2585, 3))
{ /* Llib/dsssl.scm 181 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2585, BgL_wherez00_2586, BGl_string2321z00zz__dssslz00, BgL_formalsz00_2587);}  else 
{ /* Llib/dsssl.scm 181 */
FAILURE(BGl_string2322z00zz__dssslz00,BGl_list2323z00zz__dssslz00,BgL_errz00_2585);} } }  else 
{ /* Llib/dsssl.scm 175 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2585, 3))
{ /* Llib/dsssl.scm 181 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2585, BgL_wherez00_2586, BGl_string2321z00zz__dssslz00, BgL_formalsz00_2587);}  else 
{ /* Llib/dsssl.scm 181 */
FAILURE(BGl_string2322z00zz__dssslz00,BGl_list2323z00zz__dssslz00,BgL_errz00_2585);} } }  else 
{ /* Llib/dsssl.scm 175 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2585, 3))
{ /* Llib/dsssl.scm 181 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2585, BgL_wherez00_2586, BGl_string2321z00zz__dssslz00, BgL_formalsz00_2587);}  else 
{ /* Llib/dsssl.scm 181 */
FAILURE(BGl_string2322z00zz__dssslz00,BGl_list2323z00zz__dssslz00,BgL_errz00_2585);} } }  else 
{ /* Llib/dsssl.scm 175 */
if(
SYMBOLP(BgL_ezd2112zd2_1254))
{ 
 obj_t BgL_restz00_3589; obj_t BgL_initializa7erza7_3588; obj_t BgL_argz00_3587;
BgL_argz00_3587 = BgL_ezd2112zd2_1254; 
BgL_initializa7erza7_3588 = BFALSE; 
BgL_restz00_3589 = 
CDR(BgL_argsz00_1229); 
BgL_restz00_1187 = BgL_restz00_3589; 
BgL_initializa7erza7_1186 = BgL_initializa7erza7_3588; 
BgL_argz00_1185 = BgL_argz00_3587; 
goto BgL_zc3z04anonymousza31254ze3z87_1188;}  else 
{ /* Llib/dsssl.scm 175 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2585, 3))
{ /* Llib/dsssl.scm 181 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2585, BgL_wherez00_2586, BGl_string2321z00zz__dssslz00, BgL_formalsz00_2587);}  else 
{ /* Llib/dsssl.scm 181 */
FAILURE(BGl_string2322z00zz__dssslz00,BGl_list2323z00zz__dssslz00,BgL_errz00_2585);} } } } }  else 
{ /* Llib/dsssl.scm 161 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2585, 3))
{ /* Llib/dsssl.scm 162 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2585, BgL_wherez00_2586, BGl_string2321z00zz__dssslz00, BgL_formalsz00_2587);}  else 
{ /* Llib/dsssl.scm 162 */
FAILURE(BGl_string2322z00zz__dssslz00,BGl_list2323z00zz__dssslz00,BgL_errz00_2585);} } } } } 

}



/* &rest-state */
obj_t BGl_z62restzd2statezb0zz__dssslz00(obj_t BgL_envz00_2548, obj_t BgL_argsz00_2553, obj_t BgL_dssslzd2argzd2_2554)
{
{ /* Llib/dsssl.scm 193 */
{ /* Llib/dsssl.scm 187 */
 obj_t BgL_errz00_2549; obj_t BgL_wherez00_2550; obj_t BgL_formalsz00_2551; obj_t BgL_bodyz00_2552;
BgL_errz00_2549 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_2548, 
(int)(0L))); 
BgL_wherez00_2550 = 
PROCEDURE_L_REF(BgL_envz00_2548, 
(int)(1L)); 
BgL_formalsz00_2551 = 
PROCEDURE_L_REF(BgL_envz00_2548, 
(int)(2L)); 
BgL_bodyz00_2552 = 
PROCEDURE_L_REF(BgL_envz00_2548, 
(int)(3L)); 
{ 
 obj_t BgL_argsz00_2838; obj_t BgL_dssslzd2argzd2_2839; obj_t BgL_argsz00_2825; obj_t BgL_dssslzd2argzd2_2826;
if(
PAIRP(BgL_argsz00_2553))
{ /* Llib/dsssl.scm 189 */
 bool_t BgL_test2532z00_3620;
{ /* Llib/dsssl.scm 189 */
 obj_t BgL_tmpz00_3621;
BgL_tmpz00_3621 = 
CAR(BgL_argsz00_2553); 
BgL_test2532z00_3620 = 
SYMBOLP(BgL_tmpz00_3621); } 
if(BgL_test2532z00_3620)
{ /* Llib/dsssl.scm 192 */
 obj_t BgL_arg1421z00_2840;
{ /* Llib/dsssl.scm 192 */
 obj_t BgL_arg1422z00_2841; obj_t BgL_arg1423z00_2842;
{ /* Llib/dsssl.scm 192 */
 obj_t BgL_arg1424z00_2843;
{ /* Llib/dsssl.scm 192 */
 obj_t BgL_arg1425z00_2844; obj_t BgL_arg1426z00_2845;
BgL_arg1425z00_2844 = 
CAR(BgL_argsz00_2553); 
BgL_arg1426z00_2845 = 
MAKE_YOUNG_PAIR(BgL_dssslzd2argzd2_2554, BNIL); 
BgL_arg1424z00_2843 = 
MAKE_YOUNG_PAIR(BgL_arg1425z00_2844, BgL_arg1426z00_2845); } 
BgL_arg1422z00_2841 = 
MAKE_YOUNG_PAIR(BgL_arg1424z00_2843, BNIL); } 
{ /* Llib/dsssl.scm 192 */
 obj_t BgL_tmpz00_3628;
BgL_argsz00_2838 = 
CDR(BgL_argsz00_2553); 
BgL_dssslzd2argzd2_2839 = BgL_dssslzd2argzd2_2554; 
if(
NULLP(BgL_argsz00_2838))
{ /* Llib/dsssl.scm 197 */
BgL_tmpz00_3628 = BgL_bodyz00_2552
; }  else 
{ /* Llib/dsssl.scm 197 */
if(
PAIRP(BgL_argsz00_2838))
{ /* Llib/dsssl.scm 199 */
if(
(
CAR(BgL_argsz00_2838)==(BKEY)))
{ /* Llib/dsssl.scm 201 */
BgL_argsz00_2825 = 
CDR(BgL_argsz00_2838); 
BgL_dssslzd2argzd2_2826 = BgL_dssslzd2argzd2_2839; 
{ 
 obj_t BgL_argsz00_2828;
if(
NULLP(BgL_argsz00_2825))
{ /* Llib/dsssl.scm 220 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 221 */
BgL_tmpz00_3628 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2551)
; }  else 
{ /* Llib/dsssl.scm 221 */
FAILURE(BGl_string2329z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2549);} }  else 
{ /* Llib/dsssl.scm 223 */
 obj_t BgL_keysz00_2837;
BgL_argsz00_2828 = BgL_argsz00_2825; 
if(
NULLP(BgL_argsz00_2828))
{ /* Llib/dsssl.scm 209 */
BgL_keysz00_2837 = BNIL; }  else 
{ /* Llib/dsssl.scm 209 */
 obj_t BgL_head1078z00_2829;
BgL_head1078z00_2829 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
{ 
 obj_t BgL_l1076z00_2831; obj_t BgL_tail1079z00_2832;
BgL_l1076z00_2831 = BgL_argsz00_2828; 
BgL_tail1079z00_2832 = BgL_head1078z00_2829; 
BgL_zc3z04anonymousza31443ze3z87_2830:
if(
PAIRP(BgL_l1076z00_2831))
{ /* Llib/dsssl.scm 209 */
 obj_t BgL_newtail1080z00_2833;
{ /* Llib/dsssl.scm 209 */
 obj_t BgL_arg1446z00_2834;
{ /* Llib/dsssl.scm 209 */
 obj_t BgL_xz00_2835;
BgL_xz00_2835 = 
CAR(BgL_l1076z00_2831); 
{ /* Llib/dsssl.scm 211 */
 bool_t BgL_test2540z00_3653;
if(
PAIRP(BgL_xz00_2835))
{ /* Llib/dsssl.scm 211 */
 obj_t BgL_tmpz00_3656;
BgL_tmpz00_3656 = 
CAR(BgL_xz00_2835); 
BgL_test2540z00_3653 = 
SYMBOLP(BgL_tmpz00_3656); }  else 
{ /* Llib/dsssl.scm 211 */
BgL_test2540z00_3653 = ((bool_t)0)
; } 
if(BgL_test2540z00_3653)
{ /* Llib/dsssl.scm 212 */
 obj_t BgL_arg1450z00_2836;
BgL_arg1450z00_2836 = 
CAR(BgL_xz00_2835); 
{ /* Llib/dsssl.scm 212 */
 obj_t BgL_auxz00_3660;
if(
SYMBOLP(BgL_arg1450z00_2836))
{ /* Llib/dsssl.scm 212 */
BgL_auxz00_3660 = BgL_arg1450z00_2836
; }  else 
{ 
 obj_t BgL_auxz00_3663;
BgL_auxz00_3663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(7368L), BGl_string2324z00zz__dssslz00, BGl_string2308z00zz__dssslz00, BgL_arg1450z00_2836); 
FAILURE(BgL_auxz00_3663,BFALSE,BFALSE);} 
BgL_arg1446z00_2834 = 
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_auxz00_3660); } }  else 
{ /* Llib/dsssl.scm 211 */
if(
SYMBOLP(BgL_xz00_2835))
{ /* Llib/dsssl.scm 213 */
BgL_arg1446z00_2834 = 
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_xz00_2835); }  else 
{ /* Llib/dsssl.scm 213 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 216 */
BgL_arg1446z00_2834 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2325z00zz__dssslz00, BgL_formalsz00_2551); }  else 
{ /* Llib/dsssl.scm 216 */
FAILURE(BGl_string2326z00zz__dssslz00,BGl_list2327z00zz__dssslz00,BgL_errz00_2549);} } } } } 
BgL_newtail1080z00_2833 = 
MAKE_YOUNG_PAIR(BgL_arg1446z00_2834, BNIL); } 
SET_CDR(BgL_tail1079z00_2832, BgL_newtail1080z00_2833); 
{ 
 obj_t BgL_tail1079z00_3684; obj_t BgL_l1076z00_3682;
BgL_l1076z00_3682 = 
CDR(BgL_l1076z00_2831); 
BgL_tail1079z00_3684 = BgL_newtail1080z00_2833; 
BgL_tail1079z00_2832 = BgL_tail1079z00_3684; 
BgL_l1076z00_2831 = BgL_l1076z00_3682; 
goto BgL_zc3z04anonymousza31443ze3z87_2830;} }  else 
{ /* Llib/dsssl.scm 209 */
if(
NULLP(BgL_l1076z00_2831))
{ /* Llib/dsssl.scm 209 */
BgL_keysz00_2837 = 
CDR(BgL_head1078z00_2829); }  else 
{ /* Llib/dsssl.scm 209 */
BgL_keysz00_2837 = 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string2289z00zz__dssslz00, BGl_string2328z00zz__dssslz00, BgL_l1076z00_2831, BGl_string2257z00zz__dssslz00, 
BINT(7271L)); } } } } 
if(
NULLP(BgL_keysz00_2837))
{ /* Llib/dsssl.scm 224 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 225 */
BgL_tmpz00_3628 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2551)
; }  else 
{ /* Llib/dsssl.scm 225 */
FAILURE(BGl_string2329z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2549);} }  else 
{ /* Llib/dsssl.scm 224 */
BgL_tmpz00_3628 = 
BGl_z62keyzd2statezb0zz__dssslz00(BgL_bodyz00_2552, BgL_formalsz00_2551, BgL_wherez00_2550, BgL_errz00_2549, BgL_argsz00_2825, BgL_dssslzd2argzd2_2826, BNIL, ((bool_t)0))
; } } } }  else 
{ /* Llib/dsssl.scm 201 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 204 */
BgL_tmpz00_3628 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2300z00zz__dssslz00, BgL_formalsz00_2551)
; }  else 
{ /* Llib/dsssl.scm 204 */
FAILURE(BGl_string2330z00zz__dssslz00,BGl_list2302z00zz__dssslz00,BgL_errz00_2549);} } }  else 
{ /* Llib/dsssl.scm 199 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 200 */
BgL_tmpz00_3628 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2300z00zz__dssslz00, BgL_formalsz00_2551)
; }  else 
{ /* Llib/dsssl.scm 200 */
FAILURE(BGl_string2330z00zz__dssslz00,BGl_list2302z00zz__dssslz00,BgL_errz00_2549);} } } 
BgL_arg1423z00_2842 = 
MAKE_YOUNG_PAIR(BgL_tmpz00_3628, BNIL); } 
BgL_arg1421z00_2840 = 
MAKE_YOUNG_PAIR(BgL_arg1422z00_2841, BgL_arg1423z00_2842); } 
return 
MAKE_YOUNG_PAIR(BGl_symbol2255z00zz__dssslz00, BgL_arg1421z00_2840);}  else 
{ /* Llib/dsssl.scm 189 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 190 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2300z00zz__dssslz00, BgL_formalsz00_2551);}  else 
{ /* Llib/dsssl.scm 190 */
FAILURE(BGl_string2331z00zz__dssslz00,BGl_list2302z00zz__dssslz00,BgL_errz00_2549);} } }  else 
{ /* Llib/dsssl.scm 187 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2549, 3))
{ /* Llib/dsssl.scm 188 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2549, BgL_wherez00_2550, BGl_string2300z00zz__dssslz00, BgL_formalsz00_2551);}  else 
{ /* Llib/dsssl.scm 188 */
FAILURE(BGl_string2331z00zz__dssslz00,BGl_list2302z00zz__dssslz00,BgL_errz00_2549);} } } } } 

}



/* &no-rest-key-state */
obj_t BGl_z62nozd2restzd2keyzd2statezb0zz__dssslz00(obj_t BgL_envz00_2555, obj_t BgL_argsz00_2560, obj_t BgL_dssslzd2argzd2_2561)
{
{ /* Llib/dsssl.scm 245 */
{ /* Llib/dsssl.scm 231 */
 obj_t BgL_bodyz00_2556; obj_t BgL_errz00_2557; obj_t BgL_wherez00_2558; obj_t BgL_formalsz00_2559;
BgL_bodyz00_2556 = 
PROCEDURE_L_REF(BgL_envz00_2555, 
(int)(0L)); 
BgL_errz00_2557 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_2555, 
(int)(1L))); 
BgL_wherez00_2558 = 
PROCEDURE_L_REF(BgL_envz00_2555, 
(int)(2L)); 
BgL_formalsz00_2559 = 
PROCEDURE_L_REF(BgL_envz00_2555, 
(int)(3L)); 
{ 
 obj_t BgL_argsz00_2848;
if(
NULLP(BgL_argsz00_2560))
{ /* Llib/dsssl.scm 248 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2557, 3))
{ /* Llib/dsssl.scm 249 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2557, BgL_wherez00_2558, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2559);}  else 
{ /* Llib/dsssl.scm 249 */
FAILURE(BGl_string2332z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2557);} }  else 
{ /* Llib/dsssl.scm 251 */
 obj_t BgL_keysz00_2863;
BgL_argsz00_2848 = BgL_argsz00_2560; 
{ 
 obj_t BgL_argsz00_2850; obj_t BgL_auxz00_2851;
BgL_argsz00_2850 = BgL_argsz00_2848; 
BgL_auxz00_2851 = BNIL; 
BgL_loopz00_2849:
if(
NULLP(BgL_argsz00_2850))
{ /* Llib/dsssl.scm 234 */
BgL_keysz00_2863 = 
bgl_reverse_bang(BgL_auxz00_2851); }  else 
{ /* Llib/dsssl.scm 236 */
 bool_t BgL_test2555z00_3766;
{ /* Llib/dsssl.scm 236 */
 obj_t BgL_tmpz00_3767;
{ /* Llib/dsssl.scm 236 */
 obj_t BgL_pairz00_2852;
if(
PAIRP(BgL_argsz00_2850))
{ /* Llib/dsssl.scm 236 */
BgL_pairz00_2852 = BgL_argsz00_2850; }  else 
{ 
 obj_t BgL_auxz00_3770;
BgL_auxz00_3770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(7986L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2850); 
FAILURE(BgL_auxz00_3770,BFALSE,BFALSE);} 
BgL_tmpz00_3767 = 
CAR(BgL_pairz00_2852); } 
BgL_test2555z00_3766 = 
(BgL_tmpz00_3767==(BREST)); } 
if(BgL_test2555z00_3766)
{ /* Llib/dsssl.scm 236 */
BgL_keysz00_2863 = 
bgl_reverse_bang(BgL_auxz00_2851); }  else 
{ /* Llib/dsssl.scm 239 */
 obj_t BgL_ezd2145zd2_2853;
{ /* Llib/dsssl.scm 239 */
 obj_t BgL_pairz00_2854;
if(
PAIRP(BgL_argsz00_2850))
{ /* Llib/dsssl.scm 239 */
BgL_pairz00_2854 = BgL_argsz00_2850; }  else 
{ 
 obj_t BgL_auxz00_3779;
BgL_auxz00_3779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(8051L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2850); 
FAILURE(BgL_auxz00_3779,BFALSE,BFALSE);} 
BgL_ezd2145zd2_2853 = 
CAR(BgL_pairz00_2854); } 
if(
SYMBOLP(BgL_ezd2145zd2_2853))
{ /* Llib/dsssl.scm 241 */
 obj_t BgL_arg1469z00_2855; obj_t BgL_arg1472z00_2856;
{ /* Llib/dsssl.scm 241 */
 obj_t BgL_pairz00_2857;
if(
PAIRP(BgL_argsz00_2850))
{ /* Llib/dsssl.scm 241 */
BgL_pairz00_2857 = BgL_argsz00_2850; }  else 
{ 
 obj_t BgL_auxz00_3788;
BgL_auxz00_3788 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(8103L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2850); 
FAILURE(BgL_auxz00_3788,BFALSE,BFALSE);} 
BgL_arg1469z00_2855 = 
CDR(BgL_pairz00_2857); } 
BgL_arg1472z00_2856 = 
MAKE_YOUNG_PAIR(
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_ezd2145zd2_2853), BgL_auxz00_2851); 
{ 
 obj_t BgL_auxz00_3796; obj_t BgL_argsz00_3795;
BgL_argsz00_3795 = BgL_arg1469z00_2855; 
BgL_auxz00_3796 = BgL_arg1472z00_2856; 
BgL_auxz00_2851 = BgL_auxz00_3796; 
BgL_argsz00_2850 = BgL_argsz00_3795; 
goto BgL_loopz00_2849;} }  else 
{ /* Llib/dsssl.scm 239 */
if(
PAIRP(BgL_ezd2145zd2_2853))
{ /* Llib/dsssl.scm 239 */
 obj_t BgL_carzd2152zd2_2858; obj_t BgL_cdrzd2153zd2_2859;
BgL_carzd2152zd2_2858 = 
CAR(BgL_ezd2145zd2_2853); 
BgL_cdrzd2153zd2_2859 = 
CDR(BgL_ezd2145zd2_2853); 
if(
SYMBOLP(BgL_carzd2152zd2_2858))
{ /* Llib/dsssl.scm 239 */
if(
PAIRP(BgL_cdrzd2153zd2_2859))
{ /* Llib/dsssl.scm 239 */
if(
NULLP(
CDR(BgL_cdrzd2153zd2_2859)))
{ /* Llib/dsssl.scm 243 */
 obj_t BgL_arg1474z00_2860; obj_t BgL_arg1476z00_2861;
{ /* Llib/dsssl.scm 243 */
 obj_t BgL_pairz00_2862;
if(
PAIRP(BgL_argsz00_2850))
{ /* Llib/dsssl.scm 243 */
BgL_pairz00_2862 = BgL_argsz00_2850; }  else 
{ 
 obj_t BgL_auxz00_3810;
BgL_auxz00_3810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(8195L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_2850); 
FAILURE(BgL_auxz00_3810,BFALSE,BFALSE);} 
BgL_arg1474z00_2860 = 
CDR(BgL_pairz00_2862); } 
BgL_arg1476z00_2861 = 
MAKE_YOUNG_PAIR(
BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(BgL_carzd2152zd2_2858), BgL_auxz00_2851); 
{ 
 obj_t BgL_auxz00_3818; obj_t BgL_argsz00_3817;
BgL_argsz00_3817 = BgL_arg1474z00_2860; 
BgL_auxz00_3818 = BgL_arg1476z00_2861; 
BgL_auxz00_2851 = BgL_auxz00_3818; 
BgL_argsz00_2850 = BgL_argsz00_3817; 
goto BgL_loopz00_2849;} }  else 
{ /* Llib/dsssl.scm 239 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2557, 3))
{ /* Llib/dsssl.scm 245 */
BgL_keysz00_2863 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2557, BgL_wherez00_2558, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2559); }  else 
{ /* Llib/dsssl.scm 245 */
FAILURE(BGl_string2266z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2557);} } }  else 
{ /* Llib/dsssl.scm 239 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2557, 3))
{ /* Llib/dsssl.scm 245 */
BgL_keysz00_2863 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2557, BgL_wherez00_2558, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2559); }  else 
{ /* Llib/dsssl.scm 245 */
FAILURE(BGl_string2266z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2557);} } }  else 
{ /* Llib/dsssl.scm 239 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2557, 3))
{ /* Llib/dsssl.scm 245 */
BgL_keysz00_2863 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2557, BgL_wherez00_2558, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2559); }  else 
{ /* Llib/dsssl.scm 245 */
FAILURE(BGl_string2266z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2557);} } }  else 
{ /* Llib/dsssl.scm 239 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2557, 3))
{ /* Llib/dsssl.scm 245 */
BgL_keysz00_2863 = 
BGL_PROCEDURE_CALL3(BgL_errz00_2557, BgL_wherez00_2558, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2559); }  else 
{ /* Llib/dsssl.scm 245 */
FAILURE(BGl_string2266z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2557);} } } } } } 
if(
NULLP(BgL_keysz00_2863))
{ /* Llib/dsssl.scm 252 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2557, 3))
{ /* Llib/dsssl.scm 253 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2557, BgL_wherez00_2558, BGl_string2305z00zz__dssslz00, BgL_formalsz00_2559);}  else 
{ /* Llib/dsssl.scm 253 */
FAILURE(BGl_string2332z00zz__dssslz00,BGl_list2306z00zz__dssslz00,BgL_errz00_2557);} }  else 
{ /* Llib/dsssl.scm 252 */
return 
BGl_z62keyzd2statezb0zz__dssslz00(BgL_bodyz00_2556, BgL_formalsz00_2559, BgL_wherez00_2558, BgL_errz00_2557, BgL_argsz00_2560, BgL_dssslzd2argzd2_2561, BNIL, ((bool_t)1));} } } } } 

}



/* dsssl-check-key-args! */
BGL_EXPORTED_DEF obj_t BGl_dssslzd2checkzd2keyzd2argsz12zc0zz__dssslz00(obj_t BgL_dssslzd2argszd2_8, obj_t BgL_keyzd2listzd2_9)
{
{ /* Llib/dsssl.scm 316 */
if(
NULLP(BgL_keyzd2listzd2_9))
{ 
 obj_t BgL_argsz00_1557;
BgL_argsz00_1557 = BgL_dssslzd2argszd2_8; 
BgL_zc3z04anonymousza31609ze3z87_1558:
if(
NULLP(BgL_argsz00_1557))
{ /* Llib/dsssl.scm 320 */
return BgL_dssslzd2argszd2_8;}  else 
{ /* Llib/dsssl.scm 322 */
 bool_t BgL_test2573z00_3871;
if(
PAIRP(BgL_argsz00_1557))
{ /* Llib/dsssl.scm 322 */
if(
NULLP(
CDR(BgL_argsz00_1557)))
{ /* Llib/dsssl.scm 323 */
BgL_test2573z00_3871 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 324 */
 bool_t BgL_test2576z00_3877;
{ /* Llib/dsssl.scm 324 */
 obj_t BgL_tmpz00_3878;
BgL_tmpz00_3878 = 
CAR(BgL_argsz00_1557); 
BgL_test2576z00_3877 = 
KEYWORDP(BgL_tmpz00_3878); } 
if(BgL_test2576z00_3877)
{ /* Llib/dsssl.scm 324 */
BgL_test2573z00_3871 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 324 */
BgL_test2573z00_3871 = ((bool_t)1)
; } } }  else 
{ /* Llib/dsssl.scm 322 */
BgL_test2573z00_3871 = ((bool_t)1)
; } 
if(BgL_test2573z00_3871)
{ /* Llib/dsssl.scm 322 */
return 
BGl_errorz00zz__errorz00(BGl_string2333z00zz__dssslz00, BGl_string2334z00zz__dssslz00, BgL_argsz00_1557);}  else 
{ /* Llib/dsssl.scm 329 */
 obj_t BgL_arg1620z00_1568;
{ /* Llib/dsssl.scm 329 */
 obj_t BgL_pairz00_2252;
if(
PAIRP(BgL_argsz00_1557))
{ /* Llib/dsssl.scm 329 */
BgL_pairz00_2252 = BgL_argsz00_1557; }  else 
{ 
 obj_t BgL_auxz00_3884;
BgL_auxz00_3884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11381L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1557); 
FAILURE(BgL_auxz00_3884,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 329 */
 obj_t BgL_pairz00_2255;
{ /* Llib/dsssl.scm 329 */
 obj_t BgL_aux2187z00_2713;
BgL_aux2187z00_2713 = 
CDR(BgL_pairz00_2252); 
if(
PAIRP(BgL_aux2187z00_2713))
{ /* Llib/dsssl.scm 329 */
BgL_pairz00_2255 = BgL_aux2187z00_2713; }  else 
{ 
 obj_t BgL_auxz00_3891;
BgL_auxz00_3891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11375L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2187z00_2713); 
FAILURE(BgL_auxz00_3891,BFALSE,BFALSE);} } 
BgL_arg1620z00_1568 = 
CDR(BgL_pairz00_2255); } } 
{ 
 obj_t BgL_argsz00_3896;
BgL_argsz00_3896 = BgL_arg1620z00_1568; 
BgL_argsz00_1557 = BgL_argsz00_3896; 
goto BgL_zc3z04anonymousza31609ze3z87_1558;} } } }  else 
{ 
 obj_t BgL_argsz00_1577; bool_t BgL_armedz00_1578; obj_t BgL_optsz00_1579;
BgL_argsz00_1577 = BgL_dssslzd2argszd2_8; 
BgL_armedz00_1578 = ((bool_t)0); 
BgL_optsz00_1579 = BNIL; 
BgL_zc3z04anonymousza31623ze3z87_1580:
if(
NULLP(BgL_argsz00_1577))
{ /* Llib/dsssl.scm 334 */
return 
bgl_reverse_bang(BgL_optsz00_1579);}  else 
{ /* Llib/dsssl.scm 336 */
 bool_t BgL_test2580z00_3900;
if(
PAIRP(BgL_argsz00_1577))
{ /* Llib/dsssl.scm 336 */
if(
NULLP(
CDR(BgL_argsz00_1577)))
{ /* Llib/dsssl.scm 337 */
BgL_test2580z00_3900 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 338 */
 bool_t BgL_test2583z00_3906;
{ /* Llib/dsssl.scm 338 */
 obj_t BgL_tmpz00_3907;
BgL_tmpz00_3907 = 
CAR(BgL_argsz00_1577); 
BgL_test2583z00_3906 = 
KEYWORDP(BgL_tmpz00_3907); } 
if(BgL_test2583z00_3906)
{ /* Llib/dsssl.scm 339 */
 bool_t BgL_test2584z00_3910;
{ /* Llib/dsssl.scm 339 */
 obj_t BgL_tmpz00_3911;
{ /* Llib/dsssl.scm 339 */
 obj_t BgL_auxz00_3912;
{ /* Llib/dsssl.scm 339 */
 bool_t BgL_test2585z00_3914;
if(
PAIRP(BgL_keyzd2listzd2_9))
{ /* Llib/dsssl.scm 339 */
BgL_test2585z00_3914 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 339 */
BgL_test2585z00_3914 = 
NULLP(BgL_keyzd2listzd2_9)
; } 
if(BgL_test2585z00_3914)
{ /* Llib/dsssl.scm 339 */
BgL_auxz00_3912 = BgL_keyzd2listzd2_9
; }  else 
{ 
 obj_t BgL_auxz00_3918;
BgL_auxz00_3918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11622L), BGl_string2307z00zz__dssslz00, BGl_string2335z00zz__dssslz00, BgL_keyzd2listzd2_9); 
FAILURE(BgL_auxz00_3918,BFALSE,BFALSE);} } 
BgL_tmpz00_3911 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
CAR(BgL_argsz00_1577), BgL_auxz00_3912); } 
BgL_test2584z00_3910 = 
CBOOL(BgL_tmpz00_3911); } 
if(BgL_test2584z00_3910)
{ /* Llib/dsssl.scm 339 */
BgL_test2580z00_3900 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 339 */
BgL_test2580z00_3900 = ((bool_t)1)
; } }  else 
{ /* Llib/dsssl.scm 338 */
BgL_test2580z00_3900 = ((bool_t)1)
; } } }  else 
{ /* Llib/dsssl.scm 336 */
BgL_test2580z00_3900 = ((bool_t)1)
; } 
if(BgL_test2580z00_3900)
{ /* Llib/dsssl.scm 336 */
if(BgL_armedz00_1578)
{ /* Llib/dsssl.scm 342 */
 obj_t BgL_arg1639z00_1593; obj_t BgL_arg1640z00_1594;
{ /* Llib/dsssl.scm 342 */
 obj_t BgL_pairz00_2259;
if(
PAIRP(BgL_argsz00_1577))
{ /* Llib/dsssl.scm 342 */
BgL_pairz00_2259 = BgL_argsz00_1577; }  else 
{ 
 obj_t BgL_auxz00_3927;
BgL_auxz00_3927 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11705L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1577); 
FAILURE(BgL_auxz00_3927,BFALSE,BFALSE);} 
BgL_arg1639z00_1593 = 
CDR(BgL_pairz00_2259); } 
{ /* Llib/dsssl.scm 344 */
 obj_t BgL_arg1641z00_1595;
{ /* Llib/dsssl.scm 344 */
 obj_t BgL_pairz00_2260;
if(
PAIRP(BgL_argsz00_1577))
{ /* Llib/dsssl.scm 344 */
BgL_pairz00_2260 = BgL_argsz00_1577; }  else 
{ 
 obj_t BgL_auxz00_3934;
BgL_auxz00_3934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11731L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1577); 
FAILURE(BgL_auxz00_3934,BFALSE,BFALSE);} 
BgL_arg1641z00_1595 = 
CAR(BgL_pairz00_2260); } 
BgL_arg1640z00_1594 = 
MAKE_YOUNG_PAIR(BgL_arg1641z00_1595, BgL_optsz00_1579); } 
{ 
 obj_t BgL_optsz00_3942; bool_t BgL_armedz00_3941; obj_t BgL_argsz00_3940;
BgL_argsz00_3940 = BgL_arg1639z00_1593; 
BgL_armedz00_3941 = ((bool_t)0); 
BgL_optsz00_3942 = BgL_arg1640z00_1594; 
BgL_optsz00_1579 = BgL_optsz00_3942; 
BgL_armedz00_1578 = BgL_armedz00_3941; 
BgL_argsz00_1577 = BgL_argsz00_3940; 
goto BgL_zc3z04anonymousza31623ze3z87_1580;} }  else 
{ /* Llib/dsssl.scm 341 */
 obj_t BgL_arg1642z00_1596;
{ /* Llib/dsssl.scm 341 */
 obj_t BgL_pairz00_2261;
if(
PAIRP(BgL_argsz00_1577))
{ /* Llib/dsssl.scm 341 */
BgL_pairz00_2261 = BgL_argsz00_1577; }  else 
{ 
 obj_t BgL_auxz00_3945;
BgL_auxz00_3945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11672L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1577); 
FAILURE(BgL_auxz00_3945,BFALSE,BFALSE);} 
BgL_arg1642z00_1596 = 
CDR(BgL_pairz00_2261); } 
{ 
 obj_t BgL_argsz00_3950;
BgL_argsz00_3950 = BgL_arg1642z00_1596; 
BgL_argsz00_1577 = BgL_argsz00_3950; 
goto BgL_zc3z04anonymousza31623ze3z87_1580;} } }  else 
{ /* Llib/dsssl.scm 346 */
 obj_t BgL_arg1643z00_1597;
{ /* Llib/dsssl.scm 346 */
 obj_t BgL_pairz00_2262;
if(
PAIRP(BgL_argsz00_1577))
{ /* Llib/dsssl.scm 346 */
BgL_pairz00_2262 = BgL_argsz00_1577; }  else 
{ 
 obj_t BgL_auxz00_3953;
BgL_auxz00_3953 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11777L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1577); 
FAILURE(BgL_auxz00_3953,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 346 */
 obj_t BgL_pairz00_2265;
{ /* Llib/dsssl.scm 346 */
 obj_t BgL_aux2199z00_2725;
BgL_aux2199z00_2725 = 
CDR(BgL_pairz00_2262); 
if(
PAIRP(BgL_aux2199z00_2725))
{ /* Llib/dsssl.scm 346 */
BgL_pairz00_2265 = BgL_aux2199z00_2725; }  else 
{ 
 obj_t BgL_auxz00_3960;
BgL_auxz00_3960 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(11771L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2199z00_2725); 
FAILURE(BgL_auxz00_3960,BFALSE,BFALSE);} } 
BgL_arg1643z00_1597 = 
CDR(BgL_pairz00_2265); } } 
{ 
 bool_t BgL_armedz00_3966; obj_t BgL_argsz00_3965;
BgL_argsz00_3965 = BgL_arg1643z00_1597; 
BgL_armedz00_3966 = ((bool_t)1); 
BgL_armedz00_1578 = BgL_armedz00_3966; 
BgL_argsz00_1577 = BgL_argsz00_3965; 
goto BgL_zc3z04anonymousza31623ze3z87_1580;} } } } } 

}



/* &dsssl-check-key-args! */
obj_t BGl_z62dssslzd2checkzd2keyzd2argsz12za2zz__dssslz00(obj_t BgL_envz00_2562, obj_t BgL_dssslzd2argszd2_2563, obj_t BgL_keyzd2listzd2_2564)
{
{ /* Llib/dsssl.scm 316 */
return 
BGl_dssslzd2checkzd2keyzd2argsz12zc0zz__dssslz00(BgL_dssslzd2argszd2_2563, BgL_keyzd2listzd2_2564);} 

}



/* dsssl-get-key-arg */
BGL_EXPORTED_DEF obj_t BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(obj_t BgL_dssslzd2argszd2_10, obj_t BgL_keywordz00_11, obj_t BgL_initializa7erza7_12)
{
{ /* Llib/dsssl.scm 354 */
{ 
 obj_t BgL_argsz00_1607;
BgL_argsz00_1607 = BgL_dssslzd2argszd2_10; 
BgL_zc3z04anonymousza31647ze3z87_1608:
if(
PAIRP(BgL_argsz00_1607))
{ /* Llib/dsssl.scm 362 */
 bool_t BgL_test2594z00_3970;
{ /* Llib/dsssl.scm 362 */
 obj_t BgL_tmpz00_3971;
BgL_tmpz00_3971 = 
CAR(BgL_argsz00_1607); 
BgL_test2594z00_3970 = 
KEYWORDP(BgL_tmpz00_3971); } 
if(BgL_test2594z00_3970)
{ /* Llib/dsssl.scm 362 */
if(
(
CAR(BgL_argsz00_1607)==BgL_keywordz00_11))
{ /* Llib/dsssl.scm 365 */
 bool_t BgL_test2596z00_3977;
{ /* Llib/dsssl.scm 365 */
 obj_t BgL_tmpz00_3978;
BgL_tmpz00_3978 = 
CDR(BgL_argsz00_1607); 
BgL_test2596z00_3977 = 
PAIRP(BgL_tmpz00_3978); } 
if(BgL_test2596z00_3977)
{ /* Llib/dsssl.scm 369 */
 obj_t BgL_pairz00_2272;
{ /* Llib/dsssl.scm 369 */
 obj_t BgL_aux2201z00_2727;
BgL_aux2201z00_2727 = 
CDR(BgL_argsz00_1607); 
if(
PAIRP(BgL_aux2201z00_2727))
{ /* Llib/dsssl.scm 369 */
BgL_pairz00_2272 = BgL_aux2201z00_2727; }  else 
{ 
 obj_t BgL_auxz00_3984;
BgL_auxz00_3984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(12699L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2201z00_2727); 
FAILURE(BgL_auxz00_3984,BFALSE,BFALSE);} } 
return 
CAR(BgL_pairz00_2272);}  else 
{ /* Llib/dsssl.scm 365 */
return 
BGl_errorz00zz__errorz00(BGl_string2277z00zz__dssslz00, BGl_string2336z00zz__dssslz00, 
CAR(BgL_argsz00_1607));} }  else 
{ /* Llib/dsssl.scm 370 */
 bool_t BgL_test2598z00_3991;
{ /* Llib/dsssl.scm 370 */
 obj_t BgL_tmpz00_3992;
BgL_tmpz00_3992 = 
CAR(BgL_argsz00_1607); 
BgL_test2598z00_3991 = 
KEYWORDP(BgL_tmpz00_3992); } 
if(BgL_test2598z00_3991)
{ /* Llib/dsssl.scm 375 */
 bool_t BgL_test2599z00_3995;
{ /* Llib/dsssl.scm 375 */
 obj_t BgL_tmpz00_3996;
BgL_tmpz00_3996 = 
CDR(BgL_argsz00_1607); 
BgL_test2599z00_3995 = 
PAIRP(BgL_tmpz00_3996); } 
if(BgL_test2599z00_3995)
{ /* Llib/dsssl.scm 379 */
 obj_t BgL_arg1663z00_1622;
{ /* Llib/dsssl.scm 379 */
 obj_t BgL_pairz00_2279;
{ /* Llib/dsssl.scm 379 */
 obj_t BgL_aux2203z00_2729;
BgL_aux2203z00_2729 = 
CDR(BgL_argsz00_1607); 
if(
PAIRP(BgL_aux2203z00_2729))
{ /* Llib/dsssl.scm 379 */
BgL_pairz00_2279 = BgL_aux2203z00_2729; }  else 
{ 
 obj_t BgL_auxz00_4002;
BgL_auxz00_4002 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(12975L), BGl_string2307z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2203z00_2729); 
FAILURE(BgL_auxz00_4002,BFALSE,BFALSE);} } 
BgL_arg1663z00_1622 = 
CDR(BgL_pairz00_2279); } 
{ 
 obj_t BgL_argsz00_4007;
BgL_argsz00_4007 = BgL_arg1663z00_1622; 
BgL_argsz00_1607 = BgL_argsz00_4007; 
goto BgL_zc3z04anonymousza31647ze3z87_1608;} }  else 
{ /* Llib/dsssl.scm 375 */
return 
BGl_errorz00zz__errorz00(BGl_string2277z00zz__dssslz00, BGl_string2336z00zz__dssslz00, 
CAR(BgL_argsz00_1607));} }  else 
{ /* Llib/dsssl.scm 370 */
return 
BGl_errorz00zz__errorz00(BGl_string2277z00zz__dssslz00, BGl_string2337z00zz__dssslz00, 
CAR(BgL_argsz00_1607));} } }  else 
{ 
 obj_t BgL_argsz00_4012;
BgL_argsz00_4012 = 
CDR(BgL_argsz00_1607); 
BgL_argsz00_1607 = BgL_argsz00_4012; 
goto BgL_zc3z04anonymousza31647ze3z87_1608;} }  else 
{ /* Llib/dsssl.scm 357 */
if(
NULLP(BgL_argsz00_1607))
{ /* Llib/dsssl.scm 358 */
return BgL_initializa7erza7_12;}  else 
{ /* Llib/dsssl.scm 358 */
return 
BGl_errorz00zz__errorz00(BGl_string2277z00zz__dssslz00, BGl_string2338z00zz__dssslz00, BgL_dssslzd2argszd2_10);} } } } 

}



/* &dsssl-get-key-arg */
obj_t BGl_z62dssslzd2getzd2keyzd2argzb0zz__dssslz00(obj_t BgL_envz00_2565, obj_t BgL_dssslzd2argszd2_2566, obj_t BgL_keywordz00_2567, obj_t BgL_initializa7erza7_2568)
{
{ /* Llib/dsssl.scm 354 */
{ /* Llib/dsssl.scm 357 */
 obj_t BgL_auxz00_4017;
if(
KEYWORDP(BgL_keywordz00_2567))
{ /* Llib/dsssl.scm 357 */
BgL_auxz00_4017 = BgL_keywordz00_2567
; }  else 
{ 
 obj_t BgL_auxz00_4020;
BgL_auxz00_4020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(12351L), BGl_string2339z00zz__dssslz00, BGl_string2340z00zz__dssslz00, BgL_keywordz00_2567); 
FAILURE(BgL_auxz00_4020,BFALSE,BFALSE);} 
return 
BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(BgL_dssslzd2argszd2_2566, BgL_auxz00_4017, BgL_initializa7erza7_2568);} } 

}



/* dsssl-get-key-rest-arg */
BGL_EXPORTED_DEF obj_t BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(obj_t BgL_dssslzd2argszd2_13, obj_t BgL_keysz00_14)
{
{ /* Llib/dsssl.scm 384 */
return 
BGl_loopze71ze7zz__dssslz00(BgL_keysz00_14, BgL_dssslzd2argszd2_13);} 

}



/* loop~1 */
obj_t BGl_loopze71ze7zz__dssslz00(obj_t BgL_keysz00_2582, obj_t BgL_argsz00_1633)
{
{ /* Llib/dsssl.scm 385 */
BGl_loopze71ze7zz__dssslz00:
if(
NULLP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 387 */
return BNIL;}  else 
{ /* Llib/dsssl.scm 389 */
 bool_t BgL_test2604z00_4028;
{ /* Llib/dsssl.scm 389 */
 bool_t BgL_test2605z00_4029;
{ /* Llib/dsssl.scm 389 */
 obj_t BgL_tmpz00_4030;
{ /* Llib/dsssl.scm 389 */
 obj_t BgL_pairz00_2283;
if(
PAIRP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 389 */
BgL_pairz00_2283 = BgL_argsz00_1633; }  else 
{ 
 obj_t BgL_auxz00_4033;
BgL_auxz00_4033 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13364L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1633); 
FAILURE(BgL_auxz00_4033,BFALSE,BFALSE);} 
BgL_tmpz00_4030 = 
CAR(BgL_pairz00_2283); } 
BgL_test2605z00_4029 = 
KEYWORDP(BgL_tmpz00_4030); } 
if(BgL_test2605z00_4029)
{ /* Llib/dsssl.scm 390 */
 bool_t BgL_test2607z00_4039;
{ /* Llib/dsssl.scm 390 */
 obj_t BgL_tmpz00_4040;
{ /* Llib/dsssl.scm 390 */
 obj_t BgL_pairz00_2284;
if(
PAIRP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 390 */
BgL_pairz00_2284 = BgL_argsz00_1633; }  else 
{ 
 obj_t BgL_auxz00_4043;
BgL_auxz00_4043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13391L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1633); 
FAILURE(BgL_auxz00_4043,BFALSE,BFALSE);} 
BgL_tmpz00_4040 = 
CDR(BgL_pairz00_2284); } 
BgL_test2607z00_4039 = 
NULLP(BgL_tmpz00_4040); } 
if(BgL_test2607z00_4039)
{ /* Llib/dsssl.scm 390 */
BgL_test2604z00_4028 = ((bool_t)1)
; }  else 
{ /* Llib/dsssl.scm 391 */
 bool_t BgL_test2609z00_4049;
{ /* Llib/dsssl.scm 391 */
 obj_t BgL_tmpz00_4050;
{ /* Llib/dsssl.scm 391 */
 obj_t BgL_auxz00_4051;
{ /* Llib/dsssl.scm 391 */
 obj_t BgL_pairz00_2285;
if(
PAIRP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 391 */
BgL_pairz00_2285 = BgL_argsz00_1633; }  else 
{ 
 obj_t BgL_auxz00_4054;
BgL_auxz00_4054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13421L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1633); 
FAILURE(BgL_auxz00_4054,BFALSE,BFALSE);} 
BgL_auxz00_4051 = 
CAR(BgL_pairz00_2285); } 
BgL_tmpz00_4050 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_4051, BgL_keysz00_2582); } 
BgL_test2609z00_4049 = 
CBOOL(BgL_tmpz00_4050); } 
if(BgL_test2609z00_4049)
{ /* Llib/dsssl.scm 391 */
BgL_test2604z00_4028 = ((bool_t)0)
; }  else 
{ /* Llib/dsssl.scm 391 */
BgL_test2604z00_4028 = ((bool_t)1)
; } } }  else 
{ /* Llib/dsssl.scm 389 */
BgL_test2604z00_4028 = ((bool_t)1)
; } } 
if(BgL_test2604z00_4028)
{ /* Llib/dsssl.scm 392 */
 obj_t BgL_arg1701z00_1646; obj_t BgL_arg1702z00_1647;
{ /* Llib/dsssl.scm 392 */
 obj_t BgL_pairz00_2286;
if(
PAIRP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 392 */
BgL_pairz00_2286 = BgL_argsz00_1633; }  else 
{ 
 obj_t BgL_auxz00_4063;
BgL_auxz00_4063 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13449L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1633); 
FAILURE(BgL_auxz00_4063,BFALSE,BFALSE);} 
BgL_arg1701z00_1646 = 
CAR(BgL_pairz00_2286); } 
{ /* Llib/dsssl.scm 392 */
 obj_t BgL_arg1703z00_1648;
{ /* Llib/dsssl.scm 392 */
 obj_t BgL_pairz00_2287;
if(
PAIRP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 392 */
BgL_pairz00_2287 = BgL_argsz00_1633; }  else 
{ 
 obj_t BgL_auxz00_4070;
BgL_auxz00_4070 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13466L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1633); 
FAILURE(BgL_auxz00_4070,BFALSE,BFALSE);} 
BgL_arg1703z00_1648 = 
CDR(BgL_pairz00_2287); } 
BgL_arg1702z00_1647 = 
BGl_loopze71ze7zz__dssslz00(BgL_keysz00_2582, BgL_arg1703z00_1648); } 
return 
MAKE_YOUNG_PAIR(BgL_arg1701z00_1646, BgL_arg1702z00_1647);}  else 
{ /* Llib/dsssl.scm 394 */
 obj_t BgL_arg1704z00_1649;
{ /* Llib/dsssl.scm 394 */
 obj_t BgL_pairz00_2288;
if(
PAIRP(BgL_argsz00_1633))
{ /* Llib/dsssl.scm 394 */
BgL_pairz00_2288 = BgL_argsz00_1633; }  else 
{ 
 obj_t BgL_auxz00_4079;
BgL_auxz00_4079 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13498L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_argsz00_1633); 
FAILURE(BgL_auxz00_4079,BFALSE,BFALSE);} 
{ /* Llib/dsssl.scm 394 */
 obj_t BgL_pairz00_2291;
{ /* Llib/dsssl.scm 394 */
 obj_t BgL_aux2219z00_2745;
BgL_aux2219z00_2745 = 
CDR(BgL_pairz00_2288); 
if(
PAIRP(BgL_aux2219z00_2745))
{ /* Llib/dsssl.scm 394 */
BgL_pairz00_2291 = BgL_aux2219z00_2745; }  else 
{ 
 obj_t BgL_auxz00_4086;
BgL_auxz00_4086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13492L), BGl_string2341z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2219z00_2745); 
FAILURE(BgL_auxz00_4086,BFALSE,BFALSE);} } 
BgL_arg1704z00_1649 = 
CDR(BgL_pairz00_2291); } } 
{ 
 obj_t BgL_argsz00_4091;
BgL_argsz00_4091 = BgL_arg1704z00_1649; 
BgL_argsz00_1633 = BgL_argsz00_4091; 
goto BGl_loopze71ze7zz__dssslz00;} } } } 

}



/* &dsssl-get-key-rest-arg */
obj_t BGl_z62dssslzd2getzd2keyzd2restzd2argz62zz__dssslz00(obj_t BgL_envz00_2569, obj_t BgL_dssslzd2argszd2_2570, obj_t BgL_keysz00_2571)
{
{ /* Llib/dsssl.scm 384 */
{ /* Llib/dsssl.scm 387 */
 obj_t BgL_auxz00_4092;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_keysz00_2571))
{ /* Llib/dsssl.scm 387 */
BgL_auxz00_4092 = BgL_keysz00_2571
; }  else 
{ 
 obj_t BgL_auxz00_4095;
BgL_auxz00_4095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(13315L), BGl_string2342z00zz__dssslz00, BGl_string2335z00zz__dssslz00, BgL_keysz00_2571); 
FAILURE(BgL_auxz00_4095,BFALSE,BFALSE);} 
return 
BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(BgL_dssslzd2argszd2_2570, BgL_auxz00_4092);} } 

}



/* id-sans-type */
obj_t BGl_idzd2sanszd2typez00zz__dssslz00(obj_t BgL_idz00_15)
{
{ /* Llib/dsssl.scm 402 */
{ /* Llib/dsssl.scm 403 */
 obj_t BgL_stringz00_1657;
{ /* Llib/dsssl.scm 403 */
 obj_t BgL_arg1762z00_2293;
BgL_arg1762z00_2293 = 
SYMBOL_TO_STRING(BgL_idz00_15); 
BgL_stringz00_1657 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1762z00_2293); } 
{ /* Llib/dsssl.scm 403 */
 long BgL_lenz00_1658;
BgL_lenz00_1658 = 
STRING_LENGTH(BgL_stringz00_1657); 
{ /* Llib/dsssl.scm 404 */

{ 
 long BgL_walkerz00_1660;
BgL_walkerz00_1660 = 0L; 
BgL_zc3z04anonymousza31708ze3z87_1661:
if(
(BgL_walkerz00_1660==BgL_lenz00_1658))
{ /* Llib/dsssl.scm 407 */
return BgL_idz00_15;}  else 
{ /* Llib/dsssl.scm 409 */
 bool_t BgL_test2617z00_4105;
{ /* Llib/dsssl.scm 409 */
 bool_t BgL_test2618z00_4106;
{ /* Llib/dsssl.scm 409 */
 unsigned char BgL_tmpz00_4107;
{ /* Llib/dsssl.scm 409 */
 long BgL_l2096z00_2593;
BgL_l2096z00_2593 = 
STRING_LENGTH(BgL_stringz00_1657); 
if(
BOUND_CHECK(BgL_walkerz00_1660, BgL_l2096z00_2593))
{ /* Llib/dsssl.scm 409 */
BgL_tmpz00_4107 = 
STRING_REF(BgL_stringz00_1657, BgL_walkerz00_1660)
; }  else 
{ 
 obj_t BgL_auxz00_4112;
BgL_auxz00_4112 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(14164L), BGl_string2343z00zz__dssslz00, BgL_stringz00_1657, 
(int)(BgL_l2096z00_2593), 
(int)(BgL_walkerz00_1660)); 
FAILURE(BgL_auxz00_4112,BFALSE,BFALSE);} } 
BgL_test2618z00_4106 = 
(BgL_tmpz00_4107==((unsigned char)':')); } 
if(BgL_test2618z00_4106)
{ /* Llib/dsssl.scm 409 */
if(
(BgL_walkerz00_1660<
(BgL_lenz00_1658-1L)))
{ /* Llib/dsssl.scm 411 */
 unsigned char BgL_tmpz00_4122;
{ /* Llib/dsssl.scm 411 */
 long BgL_i2099z00_2596;
BgL_i2099z00_2596 = 
(BgL_walkerz00_1660+1L); 
{ /* Llib/dsssl.scm 411 */
 long BgL_l2100z00_2597;
BgL_l2100z00_2597 = 
STRING_LENGTH(BgL_stringz00_1657); 
if(
BOUND_CHECK(BgL_i2099z00_2596, BgL_l2100z00_2597))
{ /* Llib/dsssl.scm 411 */
BgL_tmpz00_4122 = 
STRING_REF(BgL_stringz00_1657, BgL_i2099z00_2596)
; }  else 
{ 
 obj_t BgL_auxz00_4128;
BgL_auxz00_4128 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(14237L), BGl_string2343z00zz__dssslz00, BgL_stringz00_1657, 
(int)(BgL_l2100z00_2597), 
(int)(BgL_i2099z00_2596)); 
FAILURE(BgL_auxz00_4128,BFALSE,BFALSE);} } } 
BgL_test2617z00_4105 = 
(BgL_tmpz00_4122==((unsigned char)':')); }  else 
{ /* Llib/dsssl.scm 410 */
BgL_test2617z00_4105 = ((bool_t)0)
; } }  else 
{ /* Llib/dsssl.scm 409 */
BgL_test2617z00_4105 = ((bool_t)0)
; } } 
if(BgL_test2617z00_4105)
{ /* Llib/dsssl.scm 409 */
return 
bstring_to_symbol(
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_1657, 0L, BgL_walkerz00_1660));}  else 
{ 
 long BgL_walkerz00_4137;
BgL_walkerz00_4137 = 
(BgL_walkerz00_1660+1L); 
BgL_walkerz00_1660 = BgL_walkerz00_4137; 
goto BgL_zc3z04anonymousza31708ze3z87_1661;} } } } } } } 

}



/* dsssl-formals->scheme-formals */
BGL_EXPORTED_DEF obj_t BGl_dssslzd2formalszd2ze3schemezd2formalsz31zz__dssslz00(obj_t BgL_formalsz00_16, obj_t BgL_errz00_17)
{
{ /* Llib/dsssl.scm 419 */
return 
BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(BgL_formalsz00_16, BgL_errz00_17, ((bool_t)0));} 

}



/* &dsssl-formals->scheme-formals */
obj_t BGl_z62dssslzd2formalszd2ze3schemezd2formalsz53zz__dssslz00(obj_t BgL_envz00_2572, obj_t BgL_formalsz00_2573, obj_t BgL_errz00_2574)
{
{ /* Llib/dsssl.scm 419 */
{ /* Llib/dsssl.scm 420 */
 obj_t BgL_auxz00_4140;
if(
PROCEDUREP(BgL_errz00_2574))
{ /* Llib/dsssl.scm 420 */
BgL_auxz00_4140 = BgL_errz00_2574
; }  else 
{ 
 obj_t BgL_auxz00_4143;
BgL_auxz00_4143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(14652L), BGl_string2344z00zz__dssslz00, BGl_string2271z00zz__dssslz00, BgL_errz00_2574); 
FAILURE(BgL_auxz00_4143,BFALSE,BFALSE);} 
return 
BGl_dssslzd2formalszd2ze3schemezd2formalsz31zz__dssslz00(BgL_formalsz00_2573, BgL_auxz00_4140);} } 

}



/* dsssl-formals->scheme-typed-formals */
BGL_EXPORTED_DEF obj_t BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t BgL_formalsz00_18, obj_t BgL_errz00_19, bool_t BgL_typedz00_20)
{
{ /* Llib/dsssl.scm 436 */
return 
BGl_loopze70ze7zz__dssslz00(BgL_typedz00_20, BgL_formalsz00_18, BgL_errz00_19, BgL_formalsz00_18, ((bool_t)0));} 

}



/* loop~0 */
obj_t BGl_loopze70ze7zz__dssslz00(bool_t BgL_typedz00_2581, obj_t BgL_formalsz00_2580, obj_t BgL_errz00_2579, obj_t BgL_argsz00_1685, bool_t BgL_dssslz00_1686)
{
{ /* Llib/dsssl.scm 449 */
BGl_loopze70ze7zz__dssslz00:
{ 
 obj_t BgL_objz00_1711;
if(
NULLP(BgL_argsz00_1685))
{ /* Llib/dsssl.scm 452 */
return BNIL;}  else 
{ /* Llib/dsssl.scm 452 */
if(
PAIRP(BgL_argsz00_1685))
{ /* Llib/dsssl.scm 464 */
 bool_t BgL_test2625z00_4153;
{ /* Llib/dsssl.scm 464 */
 obj_t BgL_tmpz00_4154;
BgL_tmpz00_4154 = 
CAR(BgL_argsz00_1685); 
BgL_test2625z00_4153 = 
SYMBOLP(BgL_tmpz00_4154); } 
if(BgL_test2625z00_4153)
{ /* Llib/dsssl.scm 464 */
if(BgL_dssslz00_1686)
{ /* Llib/dsssl.scm 477 */
 obj_t BgL_arg1735z00_1692;
BgL_arg1735z00_1692 = 
CAR(BgL_argsz00_1685); 
{ /* Llib/dsssl.scm 477 */
 obj_t BgL_auxz00_4159;
if(
SYMBOLP(BgL_arg1735z00_1692))
{ /* Llib/dsssl.scm 477 */
BgL_auxz00_4159 = BgL_arg1735z00_1692
; }  else 
{ 
 obj_t BgL_auxz00_4162;
BgL_auxz00_4162 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(16861L), BGl_string2346z00zz__dssslz00, BGl_string2308z00zz__dssslz00, BgL_arg1735z00_1692); 
FAILURE(BgL_auxz00_4162,BFALSE,BFALSE);} 
return 
BGl_idzd2sanszd2typez00zz__dssslz00(BgL_auxz00_4159);} }  else 
{ /* Llib/dsssl.scm 479 */
 obj_t BgL_arg1736z00_1693; obj_t BgL_arg1737z00_1694;
if(BgL_typedz00_2581)
{ /* Llib/dsssl.scm 479 */
BgL_arg1736z00_1693 = 
CAR(BgL_argsz00_1685); }  else 
{ /* Llib/dsssl.scm 479 */
 obj_t BgL_arg1738z00_1695;
BgL_arg1738z00_1695 = 
CAR(BgL_argsz00_1685); 
{ /* Llib/dsssl.scm 479 */
 obj_t BgL_auxz00_4170;
if(
SYMBOLP(BgL_arg1738z00_1695))
{ /* Llib/dsssl.scm 479 */
BgL_auxz00_4170 = BgL_arg1738z00_1695
; }  else 
{ 
 obj_t BgL_auxz00_4173;
BgL_auxz00_4173 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(16926L), BGl_string2346z00zz__dssslz00, BGl_string2308z00zz__dssslz00, BgL_arg1738z00_1695); 
FAILURE(BgL_auxz00_4173,BFALSE,BFALSE);} 
BgL_arg1736z00_1693 = 
BGl_idzd2sanszd2typez00zz__dssslz00(BgL_auxz00_4170); } } 
BgL_arg1737z00_1694 = 
BGl_loopze70ze7zz__dssslz00(BgL_typedz00_2581, BgL_formalsz00_2580, BgL_errz00_2579, 
CDR(BgL_argsz00_1685), ((bool_t)0)); 
return 
MAKE_YOUNG_PAIR(BgL_arg1736z00_1693, BgL_arg1737z00_1694);} }  else 
{ /* Llib/dsssl.scm 464 */
if(
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
CAR(BgL_argsz00_1685), BGl_list2347z00zz__dssslz00)))
{ 
 bool_t BgL_dssslz00_4187; obj_t BgL_argsz00_4185;
BgL_argsz00_4185 = 
CDR(BgL_argsz00_1685); 
BgL_dssslz00_4187 = ((bool_t)1); 
BgL_dssslz00_1686 = BgL_dssslz00_4187; 
BgL_argsz00_1685 = BgL_argsz00_4185; 
goto BGl_loopze70ze7zz__dssslz00;}  else 
{ /* Llib/dsssl.scm 466 */
if(BgL_dssslz00_1686)
{ /* Llib/dsssl.scm 470 */
 bool_t BgL_test2632z00_4189;
BgL_objz00_1711 = 
CAR(BgL_argsz00_1685); 
if(
PAIRP(BgL_objz00_1711))
{ /* Llib/dsssl.scm 443 */
 bool_t BgL_test2634z00_4192;
{ /* Llib/dsssl.scm 443 */
 obj_t BgL_tmpz00_4193;
BgL_tmpz00_4193 = 
CDR(BgL_objz00_1711); 
BgL_test2634z00_4192 = 
PAIRP(BgL_tmpz00_4193); } 
if(BgL_test2634z00_4192)
{ /* Llib/dsssl.scm 444 */
 obj_t BgL_tmpz00_4196;
{ /* Llib/dsssl.scm 444 */
 obj_t BgL_pairz00_2315;
{ /* Llib/dsssl.scm 444 */
 obj_t BgL_aux2225z00_2751;
BgL_aux2225z00_2751 = 
CDR(BgL_objz00_1711); 
if(
PAIRP(BgL_aux2225z00_2751))
{ /* Llib/dsssl.scm 444 */
BgL_pairz00_2315 = BgL_aux2225z00_2751; }  else 
{ 
 obj_t BgL_auxz00_4200;
BgL_auxz00_4200 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(15993L), BGl_string2345z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2225z00_2751); 
FAILURE(BgL_auxz00_4200,BFALSE,BFALSE);} } 
BgL_tmpz00_4196 = 
CDR(BgL_pairz00_2315); } 
BgL_test2632z00_4189 = 
NULLP(BgL_tmpz00_4196); }  else 
{ /* Llib/dsssl.scm 443 */
BgL_test2632z00_4189 = ((bool_t)0)
; } }  else 
{ /* Llib/dsssl.scm 442 */
BgL_test2632z00_4189 = ((bool_t)0)
; } 
if(BgL_test2632z00_4189)
{ /* Llib/dsssl.scm 471 */
 obj_t BgL_arg1746z00_1702;
{ /* Llib/dsssl.scm 447 */
 obj_t BgL_pairz00_2326;
{ /* Llib/dsssl.scm 471 */
 obj_t BgL_aux2231z00_2757;
BgL_aux2231z00_2757 = 
CAR(BgL_argsz00_1685); 
if(
PAIRP(BgL_aux2231z00_2757))
{ /* Llib/dsssl.scm 471 */
BgL_pairz00_2326 = BgL_aux2231z00_2757; }  else 
{ 
 obj_t BgL_auxz00_4210;
BgL_auxz00_4210 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(16703L), BGl_string2346z00zz__dssslz00, BGl_string2259z00zz__dssslz00, BgL_aux2231z00_2757); 
FAILURE(BgL_auxz00_4210,BFALSE,BFALSE);} } 
BgL_arg1746z00_1702 = 
CAR(BgL_pairz00_2326); } 
{ /* Llib/dsssl.scm 471 */
 obj_t BgL_auxz00_4215;
if(
SYMBOLP(BgL_arg1746z00_1702))
{ /* Llib/dsssl.scm 471 */
BgL_auxz00_4215 = BgL_arg1746z00_1702
; }  else 
{ 
 obj_t BgL_auxz00_4218;
BgL_auxz00_4218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(16713L), BGl_string2346z00zz__dssslz00, BGl_string2308z00zz__dssslz00, BgL_arg1746z00_1702); 
FAILURE(BgL_auxz00_4218,BFALSE,BFALSE);} 
return 
BGl_idzd2sanszd2typez00zz__dssslz00(BgL_auxz00_4215);} }  else 
{ /* Llib/dsssl.scm 470 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2579, 3))
{ /* Llib/dsssl.scm 473 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2579, BGl_string2348z00zz__dssslz00, BGl_string2349z00zz__dssslz00, BgL_formalsz00_2580);}  else 
{ /* Llib/dsssl.scm 473 */
FAILURE(BGl_string2350z00zz__dssslz00,BGl_list2351z00zz__dssslz00,BgL_errz00_2579);} } }  else 
{ /* Llib/dsssl.scm 468 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2579, 3))
{ /* Llib/dsssl.scm 469 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2579, BGl_string2348z00zz__dssslz00, BGl_string2352z00zz__dssslz00, BgL_formalsz00_2580);}  else 
{ /* Llib/dsssl.scm 469 */
FAILURE(BGl_string2350z00zz__dssslz00,BGl_list2353z00zz__dssslz00,BgL_errz00_2579);} } } } }  else 
{ /* Llib/dsssl.scm 454 */
if(BgL_dssslz00_1686)
{ /* Llib/dsssl.scm 456 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2579, 3))
{ /* Llib/dsssl.scm 457 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2579, BGl_string2354z00zz__dssslz00, BGl_string2355z00zz__dssslz00, BgL_formalsz00_2580);}  else 
{ /* Llib/dsssl.scm 457 */
FAILURE(BGl_string2350z00zz__dssslz00,BGl_list2356z00zz__dssslz00,BgL_errz00_2579);} }  else 
{ /* Llib/dsssl.scm 456 */
if(
SYMBOLP(BgL_argsz00_1685))
{ /* Llib/dsssl.scm 460 */
return 
BGl_idzd2sanszd2typez00zz__dssslz00(BgL_argsz00_1685);}  else 
{ /* Llib/dsssl.scm 460 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_errz00_2579, 3))
{ /* Llib/dsssl.scm 461 */
return 
BGL_PROCEDURE_CALL3(BgL_errz00_2579, BGl_string2348z00zz__dssslz00, BGl_string2352z00zz__dssslz00, BgL_formalsz00_2580);}  else 
{ /* Llib/dsssl.scm 461 */
FAILURE(BGl_string2350z00zz__dssslz00,BGl_list2353z00zz__dssslz00,BgL_errz00_2579);} } } } } } } 

}



/* &dsssl-formals->scheme-typed-formals */
obj_t BGl_z62dssslzd2formalszd2ze3schemezd2typedzd2formalsz81zz__dssslz00(obj_t BgL_envz00_2575, obj_t BgL_formalsz00_2576, obj_t BgL_errz00_2577, obj_t BgL_typedz00_2578)
{
{ /* Llib/dsssl.scm 436 */
{ /* Llib/dsssl.scm 439 */
 obj_t BgL_auxz00_4263;
if(
PROCEDUREP(BgL_errz00_2577))
{ /* Llib/dsssl.scm 439 */
BgL_auxz00_4263 = BgL_errz00_2577
; }  else 
{ 
 obj_t BgL_auxz00_4266;
BgL_auxz00_4266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2257z00zz__dssslz00, 
BINT(15856L), BGl_string2357z00zz__dssslz00, BGl_string2271z00zz__dssslz00, BgL_errz00_2577); 
FAILURE(BgL_auxz00_4266,BFALSE,BFALSE);} 
return 
BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(BgL_formalsz00_2576, BgL_auxz00_4263, 
CBOOL(BgL_typedz00_2578));} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__dssslz00(void)
{
{ /* Llib/dsssl.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__dssslz00(void)
{
{ /* Llib/dsssl.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__dssslz00(void)
{
{ /* Llib/dsssl.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__dssslz00(void)
{
{ /* Llib/dsssl.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2358z00zz__dssslz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string2358z00zz__dssslz00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string2358z00zz__dssslz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string2358z00zz__dssslz00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string2358z00zz__dssslz00));} 

}

#ifdef __cplusplus
}
#endif
