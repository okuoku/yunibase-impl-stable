/*===========================================================================*/
/*   (Llib/date.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/date.scm -indent -o objs/obj_s/Llib/date.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___DATE_TYPE_DEFINITIONS
#define BGL___DATE_TYPE_DEFINITIONS

/* object type definitions */
typedef struct BgL_z62exceptionz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
} *BgL_z62exceptionz62_bglt;

typedef struct BgL_z62errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62errorz62_bglt;

typedef struct BgL_z62iozd2closedzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2closedzd2errorz62_bglt;

typedef struct BgL_z62iozd2parsezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2parsezd2errorz62_bglt;


#endif // BGL___DATE_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62datezd2timeza7onez17zz__datez00(obj_t, obj_t);
extern obj_t BGl_z62iozd2closedzd2errorz62zz__objectz00;
BGL_EXPORTED_DECL bool_t BGl_datezf3zf3zz__datez00(obj_t);
static obj_t BGl_z62datezd2weekzd2dayz62zz__datez00(obj_t, obj_t);
extern BGL_LONGLONG_T bgl_current_microseconds(void);
BGL_EXPORTED_DECL obj_t BGl_makezd2datezd2zz__datez00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol5870z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5872z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5874z00zz__datez00 = BUNSPEC;
extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
BGL_EXPORTED_DECL obj_t BGl_currentzd2datezd2zz__datez00(void);
BGL_EXPORTED_DECL obj_t BGl_datezd2updatezd2millisecondz12z12zz__datez00(obj_t, long);
static obj_t BGl_list5762z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5877z00zz__datez00 = BUNSPEC;
extern obj_t bgl_update_date(obj_t, BGL_LONGLONG_T, int, int, int, int, int, int, long, bool_t, int);
static obj_t BGl_symbol5879z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5765z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5768z00zz__datez00 = BUNSPEC;
extern obj_t bgl_make_date(BGL_LONGLONG_T, int, int, int, int, int, int, long, bool_t, int);
BGL_EXPORTED_DECL int BGl_datezd2minutezd2zz__datez00(obj_t);
static obj_t BGl_z62currentzd2microsecondszb0zz__datez00(obj_t);
BGL_EXPORTED_DECL int BGl_datezd2ydayzd2zz__datez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_nanosecondszd2ze3datez31zz__datez00(BGL_LONGLONG_T);
static obj_t BGl_symbol5882z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31702ze3ze5zz__datez00(obj_t, obj_t);
static obj_t BGl_dayzd2ofzd2weekzd2grammarzd2zz__datez00 = BUNSPEC;
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__datez00(obj_t, obj_t);
static obj_t BGl_symbol5884z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_millisecondszd2ze3datez31zz__datez00(BGL_LONGLONG_T);
static obj_t BGl_list5771z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5934z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_datezd2monthzd2zz__datez00(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5887z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5774z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62iso8601zd2parsezd2datez62zz__datez00(obj_t, obj_t);
extern obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol5889z00zz__datez00 = BUNSPEC;
extern obj_t bgl_seconds_to_gmtdate(long);
extern obj_t BGl_raisez00zz__errorz00(obj_t);
static obj_t BGl_list5777z00zz__datez00 = BUNSPEC;
extern obj_t bgl_milliseconds_to_date(BGL_LONGLONG_T);
static obj_t BGl_z62dayzd2secondszb0zz__datez00(obj_t);
static obj_t BGl_timezd2grammarzd2zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_datezd2ze3nanosecondsz31zz__datez00(obj_t);
static obj_t BGl_z62secondszd2ze3gmtdatez53zz__datez00(obj_t, obj_t);
extern bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t);
BGL_EXPORTED_DECL int BGl_datezd2yearzd2dayz00zz__datez00(obj_t);
BGL_EXPORTED_DECL int BGl_datezd2dayzd2zz__datez00(obj_t);
static obj_t BGl_symbol5892z00zz__datez00 = BUNSPEC;
static obj_t BGl_search1333ze70ze7zz__datez00(long, obj_t, obj_t, long);
static obj_t BGl_z62zc3z04anonymousza32351ze3ze5zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datezd2updatez12zc0zz__datez00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list5942z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5780z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62secondszd2ze3utczd2stringz81zz__datez00(obj_t, obj_t);
static obj_t BGl_symbol5895z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_secondszd2ze3stringz31zz__datez00(long);
static obj_t BGl_symbol5898z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62secondszd2ze3stringz53zz__datez00(obj_t, obj_t);
static obj_t BGl_list5869z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_datezd2ze3utczd2stringze3zz__datez00(obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_currentzd2microsecondszd2zz__datez00(void);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__datez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL int BGl_datezd2yearzd2zz__datez00(obj_t);
static obj_t BGl_z62datezd2ze3secondsz53zz__datez00(obj_t, obj_t);
static obj_t BGl__datezd2copyzd2zz__datez00(obj_t, obj_t);
extern obj_t bgl_nanoseconds_to_date(BGL_LONGLONG_T);
static obj_t BGl_symbol5981z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62datezd2secondzb0zz__datez00(obj_t, obj_t);
static obj_t BGl_z62currentzd2nanosecondszb0zz__datez00(obj_t);
static obj_t BGl_symbol5986z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5989z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62millisecondszd2ze3datez53zz__datez00(obj_t, obj_t);
static obj_t BGl_list5876z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62datezd2monthzb0zz__datez00(obj_t, obj_t);
extern obj_t BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
extern obj_t bgl_day_aname(int);
static obj_t BGl_z62datezd2updatezd2secondz12z70zz__datez00(obj_t, obj_t, obj_t);
extern obj_t bgl_seconds_to_date(long);
static obj_t BGl_z62datezf3z91zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datezd2copyzd2zz__datez00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__datez00(void);
BGL_EXPORTED_DECL obj_t BGl_dayzd2namezd2zz__datez00(int);
static obj_t BGl_z62datezd2hourzb0zz__datez00(obj_t, obj_t);
static obj_t BGl_list5881z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_monthzd2anamezd2zz__datez00(int);
static obj_t BGl_list5886z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_rfc2822zd2datezd2ze3dateze3zz__datez00(obj_t);
static obj_t BGl_z62datezd2za7onezd2offsetzc5zz__datez00(obj_t, obj_t);
static obj_t BGl_z62datezd2iszd2dstz62zz__datez00(obj_t, obj_t);
extern BGL_LONGLONG_T bgl_date_to_milliseconds(obj_t);
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datezd2ze3iso8601zd2dateze3zz__datez00(obj_t);
extern obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
static obj_t BGl_list5891z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_monthzd2namezd2zz__datez00(int);
static obj_t BGl_list5894z00zz__datez00 = BUNSPEC;
extern unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__datez00(void);
static obj_t BGl_list5897z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31859ze3ze5zz__datez00(obj_t, obj_t);
extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
static obj_t BGl_genericzd2initzd2zz__datez00(void);
static obj_t BGl_importedzd2moduleszd2initz00zz__datez00(void);
static obj_t BGl_z62datezd2yearzd2dayz62zz__datez00(obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__datez00(void);
static obj_t BGl_search1318ze70ze7zz__datez00(long, obj_t, obj_t, long);
static obj_t BGl_z62datezd2updatezd2millisecondz12z70zz__datez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_integerzd2ze3secondz31zz__datez00(long);
static obj_t BGl_z62datezd2ze3gmtdatez12z41zz__datez00(obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__datez00(void);
static obj_t BGl_z62integerzd2ze3secondz53zz__datez00(obj_t, obj_t);
extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_z62rfc2822zd2datezd2ze3datez81zz__datez00(obj_t, obj_t);
extern BGL_LONGLONG_T bgl_current_milliseconds(void);
static obj_t BGl_z62datezd2monthzd2lengthz62zz__datez00(obj_t, obj_t);
static obj_t BGl_z62datezd2wdayzb0zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_iso8601zd2datezd2ze3dateze3zz__datez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datezd2updatezd2minutez12z12zz__datez00(obj_t, long);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62dayzd2anamezb0zz__datez00(obj_t, obj_t);
static obj_t BGl_keyword5910z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62datezd2ze3iso8601zd2datez81zz__datez00(obj_t, obj_t);
static obj_t BGl_keyword5912z00zz__datez00 = BUNSPEC;
static long BGl_blitzd2int2z12zc0zz__datez00(obj_t, long, int);
static obj_t BGl_keyword5914z00zz__datez00 = BUNSPEC;
static obj_t BGl_thezd2fixnumzd2grammarz00zz__datez00 = BUNSPEC;
static obj_t BGl_keyword5916z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62monthzd2namezb0zz__datez00(obj_t, obj_t);
extern BGL_LONGLONG_T bgl_date_to_nanoseconds(obj_t);
static obj_t BGl_keyword5918z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62currentzd2millisecondszb0zz__datez00(obj_t);
BGL_EXPORTED_DECL int BGl_datezd2hourzd2zz__datez00(obj_t);
extern long rgc_buffer_fixnum(obj_t);
static obj_t BGl_z62datezd2dayzb0zz__datez00(obj_t, obj_t);
static obj_t BGl_keyword5920z00zz__datez00 = BUNSPEC;
static obj_t BGl_keyword5922z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_millisecondszd2ze3gmtdatez31zz__datez00(BGL_LONGLONG_T);
static obj_t BGl_keyword5924z00zz__datez00 = BUNSPEC;
static obj_t BGl_monthzd2grammarzd2zz__datez00 = BUNSPEC;
static obj_t BGl_keyword5926z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_datezd2ze3rfc2822zd2dateze3zz__datez00(obj_t);
BGL_EXPORTED_DECL long BGl_datezd2millisecondzd2zz__datez00(obj_t);
static obj_t BGl_z62datezd2ze3utczd2stringz81zz__datez00(obj_t, obj_t);
static obj_t BGl_z62datezd2minutezb0zz__datez00(obj_t, obj_t);
extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_datezd2ze3millisecondsz31zz__datez00(obj_t);
static obj_t BGl_za7onezd2grammarz75zz__datez00 = BUNSPEC;
static obj_t BGl_z62iso8601zd2datezd2ze3datez81zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_currentzd2millisecondszd2zz__datez00(void);
BGL_EXPORTED_DECL obj_t BGl_secondszd2ze3datez31zz__datez00(long);
BGL_EXPORTED_DECL long BGl_dayzd2secondszd2zz__datez00(void);
extern BGL_LONGLONG_T bgl_current_nanoseconds(void);
static obj_t BGl_keyword5935z00zz__datez00 = BUNSPEC;
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_secondszd2ze3utczd2stringze3zz__datez00(long);
extern obj_t rgc_buffer_symbol(obj_t);
extern obj_t bgl_seconds_to_utc_string(long);
static obj_t BGl_z62dayzd2namezb0zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_datezd2nanosecondzd2zz__datez00(obj_t);
extern obj_t bgl_date_to_gmtdate(obj_t);
static obj_t BGl_z62zc3z04anonymousza32973ze3ze5zz__datez00(obj_t);
static obj_t BGl_z62datezd2nanosecondzb0zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_leapzd2yearzf3z21zz__datez00(int);
BGL_EXPORTED_DECL int BGl_datezd2monthzd2lengthz00zz__datez00(obj_t);
BGL_EXPORTED_DECL int BGl_datezd2wdayzd2zz__datez00(obj_t);
static obj_t BGl_methodzd2initzd2zz__datez00(void);
static obj_t BGl_z62datezd2ze3rfc2822zd2datez81zz__datez00(obj_t, obj_t);
static obj_t BGl_thezd2failureze70z35zz__datez00(obj_t);
extern obj_t bgl_month_aname(int);
static obj_t BGl_symbol5901z00zz__datez00 = BUNSPEC;
static obj_t BGl_thezd2failureze71z35zz__datez00(obj_t);
static obj_t BGl_thezd2failureze72z35zz__datez00(obj_t);
static obj_t BGl_thezd2failureze73z35zz__datez00(obj_t);
BGL_EXPORTED_DECL int BGl_datezd2secondzd2zz__datez00(obj_t);
static obj_t BGl_symbol5904z00zz__datez00 = BUNSPEC;
static obj_t BGl_thezd2failureze74z35zz__datez00(obj_t);
static obj_t BGl_thezd2failureze75z35zz__datez00(obj_t);
static obj_t BGl_thezd2failureze76z35zz__datez00(obj_t);
static obj_t BGl_thezd2failureze77z35zz__datez00(obj_t);
BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_currentzd2nanosecondszd2zz__datez00(void);
extern obj_t bgl_milliseconds_to_gmtdate(BGL_LONGLONG_T);
BGL_EXPORTED_DECL obj_t BGl_dayzd2anamezd2zz__datez00(int);
static obj_t BGl_symbol5829z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5748z00zz__datez00 = BUNSPEC;
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_z62datezd2ze3millisecondsz53zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_datezd2timeza7onez75zz__datez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datezd2updatezd2secondz12z12zz__datez00(obj_t, long);
static obj_t BGl_za2timezd2za7onesza2z75zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_currentzd2secondszd2zz__datez00(void);
BGL_EXPORTED_DECL obj_t BGl_secondszd2ze3gmtdatez31zz__datez00(long);
BGL_EXPORTED_DECL int BGl_datezd2weekzd2dayz00zz__datez00(obj_t);
static long BGl_blitzd2intz12zc0zz__datez00(obj_t, long, int);
static obj_t BGl_z62currentzd2secondszb0zz__datez00(obj_t);
static obj_t BGl_symbol5831z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5751z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5833z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5835z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5754z00zz__datez00 = BUNSPEC;
extern obj_t BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(unsigned char, obj_t);
static obj_t BGl_symbol5837z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__datez00(obj_t, obj_t);
static obj_t BGl_symbol5757z00zz__datez00 = BUNSPEC;
extern long bgl_date_to_seconds(obj_t);
static obj_t BGl_symbol5839z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_datezd2ze3gmtdatez12z23zz__datez00(obj_t);
static obj_t BGl_parsezd2errorzd2zz__datez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62monthzd2anamezb0zz__datez00(obj_t, obj_t);
extern bool_t rgc_fill_buffer(obj_t);
BGL_EXPORTED_DECL obj_t BGl_rfc2822zd2parsezd2datez00zz__datez00(obj_t);
static obj_t BGl_z62millisecondszd2ze3gmtdatez53zz__datez00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_datezd2iszd2dstz00zz__datez00(obj_t);
extern obj_t bgl_month_name(int);
static obj_t BGl_z62leapzd2yearzf3z43zz__datez00(obj_t, obj_t);
static obj_t BGl_z62datezd2ze3stringz53zz__datez00(obj_t, obj_t);
extern obj_t make_string(long, unsigned char);
static obj_t BGl_symbol5841z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5760z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62datezd2ydayzb0zz__datez00(obj_t, obj_t);
extern obj_t bgl_close_input_port(obj_t);
static obj_t BGl_symbol5843z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62nanosecondszd2ze3datez53zz__datez00(obj_t, obj_t);
static obj_t BGl_symbol5763z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5845z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5928z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5847z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5766z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5849z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5769z00zz__datez00 = BUNSPEC;
extern obj_t bgl_day_name(int);
static obj_t BGl_za2monthzd2lengthsza2zd2zz__datez00 = BUNSPEC;
static obj_t BGl_z62secondszd2ze3datez53zz__datez00(obj_t, obj_t);
static obj_t BGl_symbol5851z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5900z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62datezd2ze3nanosecondsz53zz__datez00(obj_t, obj_t);
static obj_t BGl_symbol5772z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5903z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5937z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5775z00zz__datez00 = BUNSPEC;
static obj_t BGl__datezd2updatez12zc0zz__datez00(obj_t, obj_t);
extern long bgl_current_seconds(void);
extern obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
static obj_t BGl_symbol5778z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5746z00zz__datez00 = BUNSPEC;
extern obj_t bgl_seconds_to_string(long);
static obj_t BGl_list5909z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5747z00zz__datez00 = BUNSPEC;
static obj_t BGl_z62currentzd2datezb0zz__datez00(obj_t);
extern obj_t rgc_buffer_substring(obj_t, long, long);
BGL_EXPORTED_DECL long BGl_datezd2ze3secondsz31zz__datez00(obj_t);
BGL_EXPORTED_DECL long BGl_datezd2za7onezd2offsetza7zz__datez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datezd2ze3stringz31zz__datez00(obj_t);
static obj_t BGl_z62rfc2822zd2parsezd2datez62zz__datez00(obj_t, obj_t);
static obj_t BGl_z62datezd2yearzb0zz__datez00(obj_t, obj_t);
static obj_t BGl_z62datezd2millisecondzb0zz__datez00(obj_t, obj_t);
static obj_t BGl__makezd2datezd2zz__datez00(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol5943z00zz__datez00 = BUNSPEC;
static obj_t BGl_symbol5781z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5750z00zz__datez00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_iso8601zd2parsezd2datez00zz__datez00(obj_t);
static obj_t BGl_list5753z00zz__datez00 = BUNSPEC;
static obj_t BGl_list5756z00zz__datez00 = BUNSPEC;
static obj_t BGl_search1348ze70ze7zz__datez00(long, obj_t, obj_t, long);
static obj_t BGl_z62datezd2updatezd2minutez12z70zz__datez00(obj_t, obj_t, obj_t);
static obj_t BGl_list5759z00zz__datez00 = BUNSPEC;
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_nanosecondszd2ze3datezd2envze3zz__datez00, BgL_bgl_za762nanosecondsza7d6028z00, BGl_z62nanosecondszd2ze3datez53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2millisecondszd2envz00zz__datez00, BgL_bgl_za762currentza7d2mil6029z00, BGl_z62currentzd2millisecondszb0zz__datez00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string5990z00zz__datez00, BgL_bgl_string5990za700za7za7_6030za7, "month-aname", 11 );
DEFINE_STRING( BGl_string5991z00zz__datez00, BgL_bgl_string5991za700za7za7_6031za7, "Illegal month number", 20 );
DEFINE_STRING( BGl_string5992z00zz__datez00, BgL_bgl_string5992za700za7za7_6032za7, "month-name", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dayzd2secondszd2envz00zz__datez00, BgL_bgl_za762dayza7d2seconds6033z00, BGl_z62dayzd2secondszb0zz__datez00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string5993z00zz__datez00, BgL_bgl_string5993za700za7za7_6034za7, "&month-name", 11 );
DEFINE_STRING( BGl_string5994z00zz__datez00, BgL_bgl_string5994za700za7za7_6035za7, "&month-aname", 12 );
DEFINE_STRING( BGl_string5995z00zz__datez00, BgL_bgl_string5995za700za7za7_6036za7, "date-month-length", 17 );
DEFINE_STRING( BGl_string5996z00zz__datez00, BgL_bgl_string5996za700za7za7_6037za7, "&date-month-length", 18 );
DEFINE_STRING( BGl_string5997z00zz__datez00, BgL_bgl_string5997za700za7za7_6038za7, "&leap-year?", 11 );
DEFINE_STRING( BGl_string5998z00zz__datez00, BgL_bgl_string5998za700za7za7_6039za7, "rfc2822-date->date", 18 );
DEFINE_STRING( BGl_string5999z00zz__datez00, BgL_bgl_string5999za700za7za7_6040za7, "&rfc2822-date->date", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3nanosecondszd2envze3zz__datez00, BgL_bgl_za762dateza7d2za7e3nan6041za7, BGl_z62datezd2ze3nanosecondsz53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_secondszd2ze3utczd2stringzd2envz31zz__datez00, BgL_bgl_za762secondsza7d2za7e36042za7, BGl_z62secondszd2ze3utczd2stringz81zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2yearzd2envz00zz__datez00, BgL_bgl_za762dateza7d2yearza7b6043za7, BGl_z62datezd2yearzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_iso8601zd2parsezd2datezd2envzd2zz__datez00, BgL_bgl_za762iso8601za7d2par6044z00, BGl_z62iso8601zd2parsezd2datez62zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2timeza7onezd2envza7zz__datez00, BgL_bgl_za762dateza7d2timeza7a6045za7, BGl_z62datezd2timeza7onez17zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezf3zd2envz21zz__datez00, BgL_bgl_za762dateza7f3za791za7za7_6046za7, BGl_z62datezf3z91zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2weekzd2dayzd2envzd2zz__datez00, BgL_bgl_za762dateza7d2weekza7d6047za7, BGl_z62datezd2weekzd2dayz62zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2datezd2envz00zz__datez00, BgL_bgl__makeza7d2dateza7d2za76048za7, opt_generic_entry, BGl__makezd2datezd2zz__datez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dayzd2namezd2envz00zz__datez00, BgL_bgl_za762dayza7d2nameza7b06049za7, BGl_z62dayzd2namezb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_millisecondszd2ze3gmtdatezd2envze3zz__datez00, BgL_bgl_za762millisecondsza76050z00, BGl_z62millisecondszd2ze3gmtdatez53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2minutezd2envz00zz__datez00, BgL_bgl_za762dateza7d2minute6051z00, BGl_z62datezd2minutezb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3millisecondszd2envze3zz__datez00, BgL_bgl_za762dateza7d2za7e3mil6052za7, BGl_z62datezd2ze3millisecondsz53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2monthzd2envz00zz__datez00, BgL_bgl_za762dateza7d2monthza76053za7, BGl_z62datezd2monthzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_monthzd2namezd2envz00zz__datez00, BgL_bgl_za762monthza7d2nameza76054za7, BGl_z62monthzd2namezb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2dayzd2envz00zz__datez00, BgL_bgl_za762dateza7d2dayza7b06055za7, BGl_z62datezd2dayzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5800z00zz__datez00, BgL_bgl_string5800za700za7za7_6056za7, "STATE-0-1210", 12 );
DEFINE_STRING( BGl_string5801z00zz__datez00, BgL_bgl_string5801za700za7za7_6057za7, "STATE-2-1212", 12 );
DEFINE_STRING( BGl_string5802z00zz__datez00, BgL_bgl_string5802za700za7za7_6058za7, "STATE-6-1216", 12 );
DEFINE_STRING( BGl_string5803z00zz__datez00, BgL_bgl_string5803za700za7za7_6059za7, "STATE-3-1213", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_secondszd2ze3stringzd2envze3zz__datez00, BgL_bgl_za762secondsza7d2za7e36060za7, BGl_z62secondszd2ze3stringz53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5804z00zz__datez00, BgL_bgl_string5804za700za7za7_6061za7, "STATE-5-1215", 12 );
DEFINE_STRING( BGl_string5805z00zz__datez00, BgL_bgl_string5805za700za7za7_6062za7, "STATE-8-1218", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2nanosecondzd2envz00zz__datez00, BgL_bgl_za762dateza7d2nanose6063z00, BGl_z62datezd2nanosecondzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5806z00zz__datez00, BgL_bgl_string5806za700za7za7_6064za7, "<@anonymous:2351>", 17 );
DEFINE_STRING( BGl_string5807z00zz__datez00, BgL_bgl_string5807za700za7za7_6065za7, "class-field", 11 );
DEFINE_STRING( BGl_string5808z00zz__datez00, BgL_bgl_string5808za700za7za7_6066za7, "Can't read on a closed input port", 33 );
DEFINE_STRING( BGl_string5809z00zz__datez00, BgL_bgl_string5809za700za7za7_6067za7, "Illegal time", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3utczd2stringzd2envz31zz__datez00, BgL_bgl_za762dateza7d2za7e3utc6068za7, BGl_z62datezd2ze3utczd2stringz81zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2microsecondszd2envz00zz__datez00, BgL_bgl_za762currentza7d2mic6069z00, BGl_z62currentzd2microsecondszb0zz__datez00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_leapzd2yearzf3zd2envzf3zz__datez00, BgL_bgl_za762leapza7d2yearza7f6070za7, BGl_z62leapzd2yearzf3z43zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2secondzd2envz00zz__datez00, BgL_bgl_za762dateza7d2second6071z00, BGl_z62datezd2secondzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2nanosecondszd2envz00zz__datez00, BgL_bgl_za762currentza7d2nan6072z00, BGl_z62currentzd2nanosecondszb0zz__datez00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string5810z00zz__datez00, BgL_bgl_string5810za700za7za7_6073za7, "STATE-21-1207", 13 );
DEFINE_STRING( BGl_string5811z00zz__datez00, BgL_bgl_string5811za700za7za7_6074za7, "STATE-19-1205", 13 );
DEFINE_STRING( BGl_string5812z00zz__datez00, BgL_bgl_string5812za700za7za7_6075za7, "STATE-18-1204", 13 );
DEFINE_STRING( BGl_string5813z00zz__datez00, BgL_bgl_string5813za700za7za7_6076za7, "STATE-17-1203", 13 );
DEFINE_STRING( BGl_string5814z00zz__datez00, BgL_bgl_string5814za700za7za7_6077za7, "STATE-0-1186", 12 );
DEFINE_STRING( BGl_string5815z00zz__datez00, BgL_bgl_string5815za700za7za7_6078za7, "STATE-3-1189", 12 );
DEFINE_STRING( BGl_string5816z00zz__datez00, BgL_bgl_string5816za700za7za7_6079za7, "STATE-10-1196", 13 );
DEFINE_STRING( BGl_string5817z00zz__datez00, BgL_bgl_string5817za700za7za7_6080za7, "STATE-13-1199", 13 );
DEFINE_STRING( BGl_string5818z00zz__datez00, BgL_bgl_string5818za700za7za7_6081za7, "STATE-12-1198", 13 );
DEFINE_STRING( BGl_string5819z00zz__datez00, BgL_bgl_string5819za700za7za7_6082za7, "STATE-11-1197", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2secondszd2envz00zz__datez00, BgL_bgl_za762currentza7d2sec6083z00, BGl_z62currentzd2secondszb0zz__datez00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2datezd2envz00zz__datez00, BgL_bgl_za762currentza7d2dat6084z00, BGl_z62currentzd2datezb0zz__datez00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string5820z00zz__datez00, BgL_bgl_string5820za700za7za7_6085za7, "STATE-8-1194", 12 );
DEFINE_STRING( BGl_string5902z00zz__datez00, BgL_bgl_string5902za700za7za7_6086za7, "arg1689", 7 );
DEFINE_STRING( BGl_string5821z00zz__datez00, BgL_bgl_string5821za700za7za7_6087za7, "STATE-15-1201", 13 );
DEFINE_STRING( BGl_string5822z00zz__datez00, BgL_bgl_string5822za700za7za7_6088za7, "STATE-5-1191", 12 );
DEFINE_STRING( BGl_string5823z00zz__datez00, BgL_bgl_string5823za700za7za7_6089za7, "STATE-4-1190", 12 );
DEFINE_STRING( BGl_string5905z00zz__datez00, BgL_bgl_string5905za700za7za7_6090za7, "arg1688", 7 );
DEFINE_STRING( BGl_string5824z00zz__datez00, BgL_bgl_string5824za700za7za7_6091za7, "STATE-9-1195", 12 );
DEFINE_STRING( BGl_string5906z00zz__datez00, BgL_bgl_string5906za700za7za7_6092za7, "<@anonymous:1451>", 17 );
DEFINE_STRING( BGl_string5825z00zz__datez00, BgL_bgl_string5825za700za7za7_6093za7, "STATE-2-1188", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_monthzd2anamezd2envz00zz__datez00, BgL_bgl_za762monthza7d2aname6094z00, BGl_z62monthzd2anamezb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5826z00zz__datez00, BgL_bgl_string5826za700za7za7_6095za7, "STATE-16-1202", 13 );
DEFINE_STRING( BGl_string5908z00zz__datez00, BgL_bgl_string5908za700za7za7_6096za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string5827z00zz__datez00, BgL_bgl_string5827za700za7za7_6097za7, "<@anonymous:2062>", 17 );
DEFINE_STRING( BGl_string5828z00zz__datez00, BgL_bgl_string5828za700za7za7_6098za7, "Illegal month", 13 );
DEFINE_STRING( BGl_string5749z00zz__datez00, BgL_bgl_string5749za700za7za7_6099za7, "EDT", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_rfc2822zd2datezd2ze3datezd2envz31zz__datez00, BgL_bgl_za762rfc2822za7d2dat6100z00, BGl_z62rfc2822zd2datezd2ze3datez81zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2iszd2dstzd2envzd2zz__datez00, BgL_bgl_za762dateza7d2isza7d2d6101za7, BGl_z62datezd2iszd2dstz62zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ydayzd2envz00zz__datez00, BgL_bgl_za762dateza7d2ydayza7b6102za7, BGl_z62datezd2ydayzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string6000z00zz__datez00, BgL_bgl_string6000za700za7za7_6103za7, "&rfc2822-parse-date", 19 );
DEFINE_STRING( BGl_string6001z00zz__datez00, BgL_bgl_string6001za700za7za7_6104za7, "&date->rfc2822-date", 19 );
DEFINE_STRING( BGl_string6002z00zz__datez00, BgL_bgl_string6002za700za7za7_6105za7, "~a-~2,0d-~2,0dT~2,0d:~2,0d:~2,0dZ", 33 );
DEFINE_STRING( BGl_string6003z00zz__datez00, BgL_bgl_string6003za700za7za7_6106za7, "-", 1 );
DEFINE_STRING( BGl_string6004z00zz__datez00, BgL_bgl_string6004za700za7za7_6107za7, "+", 1 );
DEFINE_STRING( BGl_string6005z00zz__datez00, BgL_bgl_string6005za700za7za7_6108za7, "date->iso8601-date", 18 );
DEFINE_STRING( BGl_string6006z00zz__datez00, BgL_bgl_string6006za700za7za7_6109za7, "~a-~2,0d-~2,0dT~2,0d:~2,0d:~2,0d~a~2,0d:~2,0d", 45 );
DEFINE_STRING( BGl_string6007z00zz__datez00, BgL_bgl_string6007za700za7za7_6110za7, "&date->iso8601-date", 19 );
DEFINE_STRING( BGl_string6008z00zz__datez00, BgL_bgl_string6008za700za7za7_6111za7, "iso8601-date->date", 18 );
DEFINE_STRING( BGl_string6009z00zz__datez00, BgL_bgl_string6009za700za7za7_6112za7, "pair", 4 );
DEFINE_STRING( BGl_string5911z00zz__datez00, BgL_bgl_string5911za700za7za7_6113za7, "day", 3 );
DEFINE_STRING( BGl_string5830z00zz__datez00, BgL_bgl_string5830za700za7za7_6114za7, "Jan", 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc5743z00zz__datez00, BgL_bgl_za762za7c3za704anonymo6115za7, BGl_z62zc3z04anonymousza31702ze3ze5zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc5744z00zz__datez00, BgL_bgl_za762za7c3za704anonymo6116za7, BGl_z62zc3z04anonymousza31859ze3ze5zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5913z00zz__datez00, BgL_bgl_string5913za700za7za7_6117za7, "dst", 3 );
DEFINE_STRING( BGl_string5832z00zz__datez00, BgL_bgl_string5832za700za7za7_6118za7, "Feb", 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc5745z00zz__datez00, BgL_bgl_za762za7c3za704anonymo6119za7, BGl_z62zc3z04anonymousza32062ze3ze5zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5752z00zz__datez00, BgL_bgl_string5752za700za7za7_6120za7, "EST", 3 );
DEFINE_STRING( BGl_string5915z00zz__datez00, BgL_bgl_string5915za700za7za7_6121za7, "hour", 4 );
DEFINE_STRING( BGl_string5834z00zz__datez00, BgL_bgl_string5834za700za7za7_6122za7, "Mar", 3 );
DEFINE_STRING( BGl_string5917z00zz__datez00, BgL_bgl_string5917za700za7za7_6123za7, "min", 3 );
DEFINE_STRING( BGl_string5836z00zz__datez00, BgL_bgl_string5836za700za7za7_6124za7, "Apr", 3 );
DEFINE_STRING( BGl_string5755z00zz__datez00, BgL_bgl_string5755za700za7za7_6125za7, "CDT", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_secondszd2ze3datezd2envze3zz__datez00, BgL_bgl_za762secondsza7d2za7e36126za7, BGl_z62secondszd2ze3datez53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5919z00zz__datez00, BgL_bgl_string5919za700za7za7_6127za7, "month", 5 );
DEFINE_STRING( BGl_string5838z00zz__datez00, BgL_bgl_string5838za700za7za7_6128za7, "May", 3 );
DEFINE_STRING( BGl_string5758z00zz__datez00, BgL_bgl_string5758za700za7za7_6129za7, "CST", 3 );
DEFINE_STRING( BGl_string6010z00zz__datez00, BgL_bgl_string6010za700za7za7_6130za7, "&iso8601-date->date", 19 );
DEFINE_STRING( BGl_string6011z00zz__datez00, BgL_bgl_string6011za700za7za7_6131za7, "iso8601-parse-date", 18 );
DEFINE_STRING( BGl_string6012z00zz__datez00, BgL_bgl_string6012za700za7za7_6132za7, "iso8601-ZMM-grammar", 19 );
DEFINE_STRING( BGl_string6013z00zz__datez00, BgL_bgl_string6013za700za7za7_6133za7, "long", 4 );
DEFINE_STRING( BGl_string6014z00zz__datez00, BgL_bgl_string6014za700za7za7_6134za7, "iso8601-Z-grammar", 17 );
DEFINE_STRING( BGl_string6015z00zz__datez00, BgL_bgl_string6015za700za7za7_6135za7, "bchar", 5 );
DEFINE_STRING( BGl_string6016z00zz__datez00, BgL_bgl_string6016za700za7za7_6136za7, "iso8601-sss-grammar", 19 );
DEFINE_STRING( BGl_string6017z00zz__datez00, BgL_bgl_string6017za700za7za7_6137za7, "iso8601-ss-grammar", 18 );
DEFINE_STRING( BGl_string6018z00zz__datez00, BgL_bgl_string6018za700za7za7_6138za7, "iso8601-mm-grammar", 18 );
DEFINE_STRING( BGl_string6019z00zz__datez00, BgL_bgl_string6019za700za7za7_6139za7, "iso8601-HH-grammar", 18 );
DEFINE_STRING( BGl_string5921z00zz__datez00, BgL_bgl_string5921za700za7za7_6140za7, "nsec", 4 );
DEFINE_STRING( BGl_string5840z00zz__datez00, BgL_bgl_string5840za700za7za7_6141za7, "Jun", 3 );
DEFINE_STRING( BGl_string5923z00zz__datez00, BgL_bgl_string5923za700za7za7_6142za7, "sec", 3 );
DEFINE_STRING( BGl_string5842z00zz__datez00, BgL_bgl_string5842za700za7za7_6143za7, "Jul", 3 );
DEFINE_STRING( BGl_string5761z00zz__datez00, BgL_bgl_string5761za700za7za7_6144za7, "MDT", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_integerzd2ze3secondzd2envze3zz__datez00, BgL_bgl_za762integerza7d2za7e36145za7, BGl_z62integerzd2ze3secondz53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5925z00zz__datez00, BgL_bgl_string5925za700za7za7_6146za7, "timezone", 8 );
DEFINE_STRING( BGl_string5844z00zz__datez00, BgL_bgl_string5844za700za7za7_6147za7, "Aug", 3 );
DEFINE_STRING( BGl_string5764z00zz__datez00, BgL_bgl_string5764za700za7za7_6148za7, "MST", 3 );
DEFINE_STRING( BGl_string5927z00zz__datez00, BgL_bgl_string5927za700za7za7_6149za7, "year", 4 );
DEFINE_STRING( BGl_string5846z00zz__datez00, BgL_bgl_string5846za700za7za7_6150za7, "Sep", 3 );
DEFINE_STRING( BGl_string5929z00zz__datez00, BgL_bgl_string5929za700za7za7_6151za7, "make-date", 9 );
DEFINE_STRING( BGl_string5848z00zz__datez00, BgL_bgl_string5848za700za7za7_6152za7, "Oct", 3 );
DEFINE_STRING( BGl_string5767z00zz__datez00, BgL_bgl_string5767za700za7za7_6153za7, "PDT", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2za7onezd2offsetzd2envz75zz__datez00, BgL_bgl_za762dateza7d2za7a7one6154za7, BGl_z62datezd2za7onezd2offsetzc5zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2monthzd2lengthzd2envzd2zz__datez00, BgL_bgl_za762dateza7d2monthza76155za7, BGl_z62datezd2monthzd2lengthz62zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2wdayzd2envz00zz__datez00, BgL_bgl_za762dateza7d2wdayza7b6156za7, BGl_z62datezd2wdayzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string6020z00zz__datez00, BgL_bgl_string6020za700za7za7_6157za7, "iso8601-DD-grammar", 18 );
DEFINE_STRING( BGl_string6021z00zz__datez00, BgL_bgl_string6021za700za7za7_6158za7, "iso8601-MM-grammar", 18 );
DEFINE_STRING( BGl_string6022z00zz__datez00, BgL_bgl_string6022za700za7za7_6159za7, "iso8601-grammar", 15 );
DEFINE_STRING( BGl_string6023z00zz__datez00, BgL_bgl_string6023za700za7za7_6160za7, "&iso8601-parse-date", 19 );
DEFINE_STRING( BGl_string6024z00zz__datez00, BgL_bgl_string6024za700za7za7_6161za7, "parse-error", 11 );
DEFINE_STRING( BGl_string6025z00zz__datez00, BgL_bgl_string6025za700za7za7_6162za7, "}", 1 );
DEFINE_STRING( BGl_string6026z00zz__datez00, BgL_bgl_string6026za700za7za7_6163za7, "{", 1 );
DEFINE_STRING( BGl_string6027z00zz__datez00, BgL_bgl_string6027za700za7za7_6164za7, "__date", 6 );
DEFINE_STRING( BGl_string5930z00zz__datez00, BgL_bgl_string5930za700za7za7_6165za7, "Illegal keyword argument", 24 );
DEFINE_STRING( BGl_string5931z00zz__datez00, BgL_bgl_string5931za700za7za7_6166za7, "_make-date", 10 );
DEFINE_STRING( BGl_string5850z00zz__datez00, BgL_bgl_string5850za700za7za7_6167za7, "Nov", 3 );
DEFINE_STRING( BGl_string5932z00zz__datez00, BgL_bgl_string5932za700za7za7_6168za7, "bllong", 6 );
DEFINE_STRING( BGl_string5770z00zz__datez00, BgL_bgl_string5770za700za7za7_6169za7, "PST", 3 );
DEFINE_STRING( BGl_string5933z00zz__datez00, BgL_bgl_string5933za700za7za7_6170za7, "wrong number of arguments: [0..9] expected, provided", 52 );
DEFINE_STRING( BGl_string5852z00zz__datez00, BgL_bgl_string5852za700za7za7_6171za7, "Dec", 3 );
DEFINE_STRING( BGl_string5853z00zz__datez00, BgL_bgl_string5853za700za7za7_6172za7, "STATE-4-1180", 12 );
DEFINE_STRING( BGl_string5854z00zz__datez00, BgL_bgl_string5854za700za7za7_6173za7, "STATE-3-1179", 12 );
DEFINE_STRING( BGl_string5773z00zz__datez00, BgL_bgl_string5773za700za7za7_6174za7, "CEST", 4 );
DEFINE_STRING( BGl_string5936z00zz__datez00, BgL_bgl_string5936za700za7za7_6175za7, "isdst", 5 );
DEFINE_STRING( BGl_string5855z00zz__datez00, BgL_bgl_string5855za700za7za7_6176za7, "STATE-7-1183", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2updatezd2secondz12zd2envzc0zz__datez00, BgL_bgl_za762dateza7d2update6177z00, BGl_z62datezd2updatezd2secondz12z70zz__datez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string5856z00zz__datez00, BgL_bgl_string5856za700za7za7_6178za7, "STATE-2-1178", 12 );
DEFINE_STRING( BGl_string5938z00zz__datez00, BgL_bgl_string5938za700za7za7_6179za7, "date-copy", 9 );
DEFINE_STRING( BGl_string5857z00zz__datez00, BgL_bgl_string5857za700za7za7_6180za7, "STATE-0-1176", 12 );
DEFINE_STRING( BGl_string5776z00zz__datez00, BgL_bgl_string5776za700za7za7_6181za7, "UT", 2 );
DEFINE_STRING( BGl_string5939z00zz__datez00, BgL_bgl_string5939za700za7za7_6182za7, "_date-copy", 10 );
DEFINE_STRING( BGl_string5858z00zz__datez00, BgL_bgl_string5858za700za7za7_6183za7, "<@anonymous:1859>", 17 );
DEFINE_STRING( BGl_string5859z00zz__datez00, BgL_bgl_string5859za700za7za7_6184za7, "Illegal integer", 15 );
DEFINE_STRING( BGl_string5779z00zz__datez00, BgL_bgl_string5779za700za7za7_6185za7, "GMT", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_secondszd2ze3gmtdatezd2envze3zz__datez00, BgL_bgl_za762secondsza7d2za7e36186za7, BGl_z62secondszd2ze3gmtdatez53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2copyzd2envz00zz__datez00, BgL_bgl__dateza7d2copyza7d2za76187za7, opt_generic_entry, BGl__datezd2copyzd2zz__datez00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2hourzd2envz00zz__datez00, BgL_bgl_za762dateza7d2hourza7b6188za7, BGl_z62datezd2hourzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2updatezd2millisecondz12zd2envzc0zz__datez00, BgL_bgl_za762dateza7d2update6189z00, BGl_z62datezd2updatezd2millisecondz12z70zz__datez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3gmtdatez12zd2envzf1zz__datez00, BgL_bgl_za762dateza7d2za7e3gmt6190za7, BGl_z62datezd2ze3gmtdatez12z41zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5940z00zz__datez00, BgL_bgl_string5940za700za7za7_6191za7, "date", 4 );
DEFINE_STRING( BGl_string5941z00zz__datez00, BgL_bgl_string5941za700za7za7_6192za7, "wrong number of arguments: [1..10] expected, provided", 53 );
DEFINE_STRING( BGl_string5860z00zz__datez00, BgL_bgl_string5860za700za7za7_6193za7, "STATE-6-1173", 12 );
DEFINE_STRING( BGl_string5861z00zz__datez00, BgL_bgl_string5861za700za7za7_6194za7, "STATE-4-1171", 12 );
DEFINE_STRING( BGl_string5862z00zz__datez00, BgL_bgl_string5862za700za7za7_6195za7, "STATE-3-1170", 12 );
DEFINE_STRING( BGl_string5944z00zz__datez00, BgL_bgl_string5944za700za7za7_6196za7, "date-update!", 12 );
DEFINE_STRING( BGl_string5863z00zz__datez00, BgL_bgl_string5863za700za7za7_6197za7, "STATE-2-1169", 12 );
DEFINE_STRING( BGl_string5782z00zz__datez00, BgL_bgl_string5782za700za7za7_6198za7, "BST", 3 );
DEFINE_STRING( BGl_string5945z00zz__datez00, BgL_bgl_string5945za700za7za7_6199za7, "_date-update!", 13 );
DEFINE_STRING( BGl_string5864z00zz__datez00, BgL_bgl_string5864za700za7za7_6200za7, "STATE-0-1167", 12 );
DEFINE_STRING( BGl_string5946z00zz__datez00, BgL_bgl_string5946za700za7za7_6201za7, "wrong number of arguments: [1..8] expected, provided", 52 );
DEFINE_STRING( BGl_string5865z00zz__datez00, BgL_bgl_string5865za700za7za7_6202za7, "<@anonymous:1702>", 17 );
DEFINE_STRING( BGl_string5784z00zz__datez00, BgL_bgl_string5784za700za7za7_6203za7, "/tmp/bigloo/runtime/Llib/date.scm", 33 );
DEFINE_STRING( BGl_string5947z00zz__datez00, BgL_bgl_string5947za700za7za7_6204za7, "&date->gmtdate!", 15 );
DEFINE_STRING( BGl_string5866z00zz__datez00, BgL_bgl_string5866za700za7za7_6205za7, "Illegal day of week", 19 );
DEFINE_STRING( BGl_string5785z00zz__datez00, BgL_bgl_string5785za700za7za7_6206za7, "ignore", 6 );
DEFINE_STRING( BGl_string5948z00zz__datez00, BgL_bgl_string5948za700za7za7_6207za7, "&date-update-millisecond!", 25 );
DEFINE_STRING( BGl_string5867z00zz__datez00, BgL_bgl_string5867za700za7za7_6208za7, "procedure", 9 );
DEFINE_STRING( BGl_string5786z00zz__datez00, BgL_bgl_string5786za700za7za7_6209za7, "input-port", 10 );
DEFINE_STRING( BGl_string5949z00zz__datez00, BgL_bgl_string5949za700za7za7_6210za7, "&date-update-second!", 20 );
DEFINE_STRING( BGl_string5868z00zz__datez00, BgL_bgl_string5868za700za7za7_6211za7, "ignore:Wrong number of arguments", 32 );
DEFINE_STRING( BGl_string5787z00zz__datez00, BgL_bgl_string5787za700za7za7_6212za7, "rfc2822-parse-date", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_rfc2822zd2parsezd2datezd2envzd2zz__datez00, BgL_bgl_za762rfc2822za7d2par6213z00, BGl_z62rfc2822zd2parsezd2datez62zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5788z00zz__datez00, BgL_bgl_string5788za700za7za7_6214za7, "Illegal zone", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3rfc2822zd2datezd2envz31zz__datez00, BgL_bgl_za762dateza7d2za7e3rfc6215za7, BGl_z62datezd2ze3rfc2822zd2datez81zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5789z00zz__datez00, BgL_bgl_string5789za700za7za7_6216za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3iso8601zd2datezd2envz31zz__datez00, BgL_bgl_za762dateza7d2za7e3iso6217za7, BGl_z62datezd2ze3iso8601zd2datez81zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_millisecondszd2ze3datezd2envze3zz__datez00, BgL_bgl_za762millisecondsza76218z00, BGl_z62millisecondszd2ze3datez53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5950z00zz__datez00, BgL_bgl_string5950za700za7za7_6219za7, "&date-update-minute!", 20 );
DEFINE_STRING( BGl_string5951z00zz__datez00, BgL_bgl_string5951za700za7za7_6220za7, "&integer->second", 16 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc5783z00zz__datez00, BgL_bgl_za762za7c3za704anonymo6221za7, BGl_z62zc3z04anonymousza32351ze3ze5zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5952z00zz__datez00, BgL_bgl_string5952za700za7za7_6222za7, "&date-nanosecond", 16 );
DEFINE_STRING( BGl_string5871z00zz__datez00, BgL_bgl_string5871za700za7za7_6223za7, "funcall", 7 );
DEFINE_STRING( BGl_string5790z00zz__datez00, BgL_bgl_string5790za700za7za7_6224za7, "regular-grammar", 15 );
DEFINE_STRING( BGl_string5953z00zz__datez00, BgL_bgl_string5953za700za7za7_6225za7, "&date-millisecond", 17 );
DEFINE_STRING( BGl_string5791z00zz__datez00, BgL_bgl_string5791za700za7za7_6226za7, "Illegal match", 13 );
DEFINE_STRING( BGl_string5954z00zz__datez00, BgL_bgl_string5954za700za7za7_6227za7, "&date-second", 12 );
DEFINE_STRING( BGl_string5873z00zz__datez00, BgL_bgl_string5873za700za7za7_6228za7, "month-grammar", 13 );
DEFINE_STRING( BGl_string5792z00zz__datez00, BgL_bgl_string5792za700za7za7_6229za7, "the-failure", 11 );
DEFINE_STRING( BGl_string5955z00zz__datez00, BgL_bgl_string5955za700za7za7_6230za7, "&date-minute", 12 );
DEFINE_STRING( BGl_string5793z00zz__datez00, BgL_bgl_string5793za700za7za7_6231za7, "STATE-16-1226", 13 );
DEFINE_STRING( BGl_string5956z00zz__datez00, BgL_bgl_string5956za700za7za7_6232za7, "&date-hour", 10 );
DEFINE_STRING( BGl_string5875z00zz__datez00, BgL_bgl_string5875za700za7za7_6233za7, "arg1681", 7 );
DEFINE_STRING( BGl_string5794z00zz__datez00, BgL_bgl_string5794za700za7za7_6234za7, "STATE-14-1224", 13 );
DEFINE_STRING( BGl_string5957z00zz__datez00, BgL_bgl_string5957za700za7za7_6235za7, "&date-day", 9 );
DEFINE_STRING( BGl_string5795z00zz__datez00, BgL_bgl_string5795za700za7za7_6236za7, "STATE-11-1221", 13 );
DEFINE_STRING( BGl_string5958z00zz__datez00, BgL_bgl_string5958za700za7za7_6237za7, "&date-week-day", 14 );
DEFINE_STRING( BGl_string5796z00zz__datez00, BgL_bgl_string5796za700za7za7_6238za7, "STATE-10-1220", 13 );
DEFINE_STRING( BGl_string5959z00zz__datez00, BgL_bgl_string5959za700za7za7_6239za7, "&date-wday", 10 );
DEFINE_STRING( BGl_string5878z00zz__datez00, BgL_bgl_string5878za700za7za7_6240za7, "the-fixnum-grammar", 18 );
DEFINE_STRING( BGl_string5797z00zz__datez00, BgL_bgl_string5797za700za7za7_6241za7, "STATE-9-1219", 12 );
DEFINE_STRING( BGl_string5798z00zz__datez00, BgL_bgl_string5798za700za7za7_6242za7, "STATE-4-1214", 12 );
DEFINE_STRING( BGl_string5799z00zz__datez00, BgL_bgl_string5799za700za7za7_6243za7, "STATE-13-1223", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2yearzd2dayzd2envzd2zz__datez00, BgL_bgl_za762dateza7d2yearza7d6244za7, BGl_z62datezd2yearzd2dayz62zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2updatez12zd2envz12zz__datez00, BgL_bgl__dateza7d2updateza716245z00, opt_generic_entry, BGl__datezd2updatez12zc0zz__datez00, BFALSE, -1 );
DEFINE_STRING( BGl_string5960z00zz__datez00, BgL_bgl_string5960za700za7za7_6246za7, "&date-year-day", 14 );
DEFINE_STRING( BGl_string5961z00zz__datez00, BgL_bgl_string5961za700za7za7_6247za7, "&date-yday", 10 );
DEFINE_STRING( BGl_string5880z00zz__datez00, BgL_bgl_string5880za700za7za7_6248za7, "arg1678", 7 );
DEFINE_STRING( BGl_string5962z00zz__datez00, BgL_bgl_string5962za700za7za7_6249za7, "&date-month", 11 );
DEFINE_STRING( BGl_string5963z00zz__datez00, BgL_bgl_string5963za700za7za7_6250za7, "&date-year", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3secondszd2envze3zz__datez00, BgL_bgl_za762dateza7d2za7e3sec6251za7, BGl_z62datezd2ze3secondsz53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5964z00zz__datez00, BgL_bgl_string5964za700za7za7_6252za7, "&date-timezone", 14 );
DEFINE_STRING( BGl_string5883z00zz__datez00, BgL_bgl_string5883za700za7za7_6253za7, "time-grammar", 12 );
DEFINE_STRING( BGl_string5965z00zz__datez00, BgL_bgl_string5965za700za7za7_6254za7, "&date-zone-offset", 17 );
DEFINE_STRING( BGl_string5966z00zz__datez00, BgL_bgl_string5966za700za7za7_6255za7, "&date-is-dst", 12 );
DEFINE_LLONG( BGl_llong5907z00zz__datez00, BgL_bgl_llong5907za700za7za7__6256za7, ((BGL_LONGLONG_T)0) );
DEFINE_STRING( BGl_string5885z00zz__datez00, BgL_bgl_string5885za700za7za7_6257za7, "arg1676", 7 );
DEFINE_STRING( BGl_string5967z00zz__datez00, BgL_bgl_string5967za700za7za7_6258za7, "&seconds->date", 14 );
DEFINE_STRING( BGl_string5968z00zz__datez00, BgL_bgl_string5968za700za7za7_6259za7, "belong", 6 );
DEFINE_STRING( BGl_string5969z00zz__datez00, BgL_bgl_string5969za700za7za7_6260za7, "&seconds->gmtdate", 17 );
DEFINE_STRING( BGl_string5888z00zz__datez00, BgL_bgl_string5888za700za7za7_6261za7, "zone-grammar", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2ze3stringzd2envze3zz__datez00, BgL_bgl_za762dateza7d2za7e3str6262za7, BGl_z62datezd2ze3stringz53zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dayzd2anamezd2envz00zz__datez00, BgL_bgl_za762dayza7d2anameza7b6263za7, BGl_z62dayzd2anamezb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_iso8601zd2datezd2ze3datezd2envz31zz__datez00, BgL_bgl_za762iso8601za7d2dat6264z00, BGl_z62iso8601zd2datezd2ze3datez81zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2updatezd2minutez12zd2envzc0zz__datez00, BgL_bgl_za762dateza7d2update6265z00, BGl_z62datezd2updatezd2minutez12z70zz__datez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string5970z00zz__datez00, BgL_bgl_string5970za700za7za7_6266za7, "&milliseconds->gmtdate", 22 );
DEFINE_STRING( BGl_string5971z00zz__datez00, BgL_bgl_string5971za700za7za7_6267za7, "&nanoseconds->date", 18 );
DEFINE_STRING( BGl_string5890z00zz__datez00, BgL_bgl_string5890za700za7za7_6268za7, "arg1675", 7 );
DEFINE_STRING( BGl_string5972z00zz__datez00, BgL_bgl_string5972za700za7za7_6269za7, "&milliseconds->date", 19 );
DEFINE_STRING( BGl_string5973z00zz__datez00, BgL_bgl_string5973za700za7za7_6270za7, "&date->seconds", 14 );
DEFINE_STRING( BGl_string5974z00zz__datez00, BgL_bgl_string5974za700za7za7_6271za7, "&date->nanoseconds", 18 );
DEFINE_STRING( BGl_string5893z00zz__datez00, BgL_bgl_string5893za700za7za7_6272za7, "arg1699", 7 );
DEFINE_STRING( BGl_string5975z00zz__datez00, BgL_bgl_string5975za700za7za7_6273za7, "&date->milliseconds", 19 );
DEFINE_STRING( BGl_string5976z00zz__datez00, BgL_bgl_string5976za700za7za7_6274za7, "&date->string", 13 );
DEFINE_STRING( BGl_string5977z00zz__datez00, BgL_bgl_string5977za700za7za7_6275za7, "string-set!", 11 );
DEFINE_STRING( BGl_string5896z00zz__datez00, BgL_bgl_string5896za700za7za7_6276za7, "arg1692", 7 );
DEFINE_STRING( BGl_string5978z00zz__datez00, BgL_bgl_string5978za700za7za7_6277za7, "&date->utc-string", 17 );
DEFINE_STRING( BGl_string5979z00zz__datez00, BgL_bgl_string5979za700za7za7_6278za7, "&seconds->string", 16 );
DEFINE_STRING( BGl_string5899z00zz__datez00, BgL_bgl_string5899za700za7za7_6279za7, "arg1691", 7 );
DEFINE_STRING( BGl_string5980z00zz__datez00, BgL_bgl_string5980za700za7za7_6280za7, "&seconds->utc-string", 20 );
DEFINE_STRING( BGl_string5982z00zz__datez00, BgL_bgl_string5982za700za7za7_6281za7, "day-name", 8 );
DEFINE_STRING( BGl_string5983z00zz__datez00, BgL_bgl_string5983za700za7za7_6282za7, "Illegal day number", 18 );
DEFINE_STRING( BGl_string5984z00zz__datez00, BgL_bgl_string5984za700za7za7_6283za7, "bstring", 7 );
DEFINE_STRING( BGl_string5985z00zz__datez00, BgL_bgl_string5985za700za7za7_6284za7, "&day-name", 9 );
DEFINE_STRING( BGl_string5987z00zz__datez00, BgL_bgl_string5987za700za7za7_6285za7, "day-aname", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datezd2millisecondzd2envz00zz__datez00, BgL_bgl_za762dateza7d2millis6286z00, BGl_z62datezd2millisecondzb0zz__datez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string5988z00zz__datez00, BgL_bgl_string5988za700za7za7_6287za7, "&day-aname", 10 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol5870z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5872z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5874z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5762z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5877z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5879z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5765z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5768z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5882z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_dayzd2ofzd2weekzd2grammarzd2zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5884z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5771z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5934z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5887z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5774z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5889z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5777z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_timezd2grammarzd2zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5892z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5942z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5780z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5895z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5898z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5869z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5981z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5986z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5989z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5876z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5881z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5886z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5891z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5894z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5897z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5910z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5912z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5914z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_thezd2fixnumzd2grammarz00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5916z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5918z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5920z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5922z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5924z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_monthzd2grammarzd2zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5926z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_za7onezd2grammarz75zz__datez00) );
ADD_ROOT( (void *)(&BGl_keyword5935z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5901z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5904z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5829z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5748z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_za2timezd2za7onesza2z75zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5831z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5751z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5833z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5835z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5754z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5837z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5757z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5839z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5841z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5760z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5843z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5763z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5845z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5928z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5847z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5766z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5849z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5769z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_za2monthzd2lengthsza2zd2zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5851z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5900z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5772z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5903z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5937z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5775z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5778z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5746z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5909z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5747z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5943z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_symbol5781z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5750z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5753z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5756z00zz__datez00) );
ADD_ROOT( (void *)(&BGl_list5759z00zz__datez00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__datez00(long BgL_checksumz00_11844, char * BgL_fromz00_11845)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__datez00))
{ 
BGl_requirezd2initializa7ationz75zz__datez00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__datez00(); 
BGl_cnstzd2initzd2zz__datez00(); 
BGl_importedzd2moduleszd2initz00zz__datez00(); 
return 
BGl_toplevelzd2initzd2zz__datez00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__datez00(void)
{
{ /* Llib/date.scm 19 */
BGl_symbol5748z00zz__datez00 = 
bstring_to_symbol(BGl_string5749z00zz__datez00); 
BGl_list5747z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5748z00zz__datez00, 
BINT(-4L)); 
BGl_symbol5751z00zz__datez00 = 
bstring_to_symbol(BGl_string5752z00zz__datez00); 
BGl_list5750z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5751z00zz__datez00, 
BINT(-5L)); 
BGl_symbol5754z00zz__datez00 = 
bstring_to_symbol(BGl_string5755z00zz__datez00); 
BGl_list5753z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5754z00zz__datez00, 
BINT(-5L)); 
BGl_symbol5757z00zz__datez00 = 
bstring_to_symbol(BGl_string5758z00zz__datez00); 
BGl_list5756z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5757z00zz__datez00, 
BINT(-6L)); 
BGl_symbol5760z00zz__datez00 = 
bstring_to_symbol(BGl_string5761z00zz__datez00); 
BGl_list5759z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5760z00zz__datez00, 
BINT(-6L)); 
BGl_symbol5763z00zz__datez00 = 
bstring_to_symbol(BGl_string5764z00zz__datez00); 
BGl_list5762z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5763z00zz__datez00, 
BINT(-7L)); 
BGl_symbol5766z00zz__datez00 = 
bstring_to_symbol(BGl_string5767z00zz__datez00); 
BGl_list5765z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5766z00zz__datez00, 
BINT(-7L)); 
BGl_symbol5769z00zz__datez00 = 
bstring_to_symbol(BGl_string5770z00zz__datez00); 
BGl_list5768z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5769z00zz__datez00, 
BINT(-8L)); 
BGl_symbol5772z00zz__datez00 = 
bstring_to_symbol(BGl_string5773z00zz__datez00); 
BGl_list5771z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5772z00zz__datez00, 
BINT(2L)); 
BGl_symbol5775z00zz__datez00 = 
bstring_to_symbol(BGl_string5776z00zz__datez00); 
BGl_list5774z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5775z00zz__datez00, 
BINT(0L)); 
BGl_symbol5778z00zz__datez00 = 
bstring_to_symbol(BGl_string5779z00zz__datez00); 
BGl_list5777z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5778z00zz__datez00, 
BINT(0L)); 
BGl_symbol5781z00zz__datez00 = 
bstring_to_symbol(BGl_string5782z00zz__datez00); 
BGl_list5780z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5781z00zz__datez00, 
BINT(1L)); 
BGl_list5746z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_list5747z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5750z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5753z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5756z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5759z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5762z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5765z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5768z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5771z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5774z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5777z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_list5780z00zz__datez00, BNIL)))))))))))); 
BGl_symbol5829z00zz__datez00 = 
bstring_to_symbol(BGl_string5830z00zz__datez00); 
BGl_symbol5831z00zz__datez00 = 
bstring_to_symbol(BGl_string5832z00zz__datez00); 
BGl_symbol5833z00zz__datez00 = 
bstring_to_symbol(BGl_string5834z00zz__datez00); 
BGl_symbol5835z00zz__datez00 = 
bstring_to_symbol(BGl_string5836z00zz__datez00); 
BGl_symbol5837z00zz__datez00 = 
bstring_to_symbol(BGl_string5838z00zz__datez00); 
BGl_symbol5839z00zz__datez00 = 
bstring_to_symbol(BGl_string5840z00zz__datez00); 
BGl_symbol5841z00zz__datez00 = 
bstring_to_symbol(BGl_string5842z00zz__datez00); 
BGl_symbol5843z00zz__datez00 = 
bstring_to_symbol(BGl_string5844z00zz__datez00); 
BGl_symbol5845z00zz__datez00 = 
bstring_to_symbol(BGl_string5846z00zz__datez00); 
BGl_symbol5847z00zz__datez00 = 
bstring_to_symbol(BGl_string5848z00zz__datez00); 
BGl_symbol5849z00zz__datez00 = 
bstring_to_symbol(BGl_string5850z00zz__datez00); 
BGl_symbol5851z00zz__datez00 = 
bstring_to_symbol(BGl_string5852z00zz__datez00); 
BGl_symbol5870z00zz__datez00 = 
bstring_to_symbol(BGl_string5871z00zz__datez00); 
BGl_symbol5872z00zz__datez00 = 
bstring_to_symbol(BGl_string5873z00zz__datez00); 
BGl_symbol5874z00zz__datez00 = 
bstring_to_symbol(BGl_string5875z00zz__datez00); 
BGl_list5869z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5872z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5872z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5874z00zz__datez00, BNIL)))); 
BGl_symbol5877z00zz__datez00 = 
bstring_to_symbol(BGl_string5878z00zz__datez00); 
BGl_symbol5879z00zz__datez00 = 
bstring_to_symbol(BGl_string5880z00zz__datez00); 
BGl_list5876z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5877z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5877z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5879z00zz__datez00, BNIL)))); 
BGl_symbol5882z00zz__datez00 = 
bstring_to_symbol(BGl_string5883z00zz__datez00); 
BGl_symbol5884z00zz__datez00 = 
bstring_to_symbol(BGl_string5885z00zz__datez00); 
BGl_list5881z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5882z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5882z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5884z00zz__datez00, BNIL)))); 
BGl_symbol5887z00zz__datez00 = 
bstring_to_symbol(BGl_string5888z00zz__datez00); 
BGl_symbol5889z00zz__datez00 = 
bstring_to_symbol(BGl_string5890z00zz__datez00); 
BGl_list5886z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5887z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5887z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5889z00zz__datez00, BNIL)))); 
BGl_symbol5892z00zz__datez00 = 
bstring_to_symbol(BGl_string5893z00zz__datez00); 
BGl_list5891z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5877z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5877z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5892z00zz__datez00, BNIL)))); 
BGl_symbol5895z00zz__datez00 = 
bstring_to_symbol(BGl_string5896z00zz__datez00); 
BGl_list5894z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5872z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5872z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5895z00zz__datez00, BNIL)))); 
BGl_symbol5898z00zz__datez00 = 
bstring_to_symbol(BGl_string5899z00zz__datez00); 
BGl_list5897z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5877z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5877z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5898z00zz__datez00, BNIL)))); 
BGl_symbol5901z00zz__datez00 = 
bstring_to_symbol(BGl_string5902z00zz__datez00); 
BGl_list5900z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5882z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5882z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5901z00zz__datez00, BNIL)))); 
BGl_symbol5904z00zz__datez00 = 
bstring_to_symbol(BGl_string5905z00zz__datez00); 
BGl_list5903z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_symbol5870z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5887z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5887z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_symbol5904z00zz__datez00, BNIL)))); 
BGl_keyword5910z00zz__datez00 = 
bstring_to_keyword(BGl_string5911z00zz__datez00); 
BGl_keyword5912z00zz__datez00 = 
bstring_to_keyword(BGl_string5913z00zz__datez00); 
BGl_keyword5914z00zz__datez00 = 
bstring_to_keyword(BGl_string5915z00zz__datez00); 
BGl_keyword5916z00zz__datez00 = 
bstring_to_keyword(BGl_string5917z00zz__datez00); 
BGl_keyword5918z00zz__datez00 = 
bstring_to_keyword(BGl_string5919z00zz__datez00); 
BGl_keyword5920z00zz__datez00 = 
bstring_to_keyword(BGl_string5921z00zz__datez00); 
BGl_keyword5922z00zz__datez00 = 
bstring_to_keyword(BGl_string5923z00zz__datez00); 
BGl_keyword5924z00zz__datez00 = 
bstring_to_keyword(BGl_string5925z00zz__datez00); 
BGl_keyword5926z00zz__datez00 = 
bstring_to_keyword(BGl_string5927z00zz__datez00); 
BGl_list5909z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_keyword5910z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5912z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5914z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5916z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5918z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5920z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5922z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5924z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5926z00zz__datez00, BNIL))))))))); 
BGl_symbol5928z00zz__datez00 = 
bstring_to_symbol(BGl_string5929z00zz__datez00); 
BGl_keyword5935z00zz__datez00 = 
bstring_to_keyword(BGl_string5936z00zz__datez00); 
BGl_list5934z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_keyword5910z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5914z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5935z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5916z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5918z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5920z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5922z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5924z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5926z00zz__datez00, BNIL))))))))); 
BGl_symbol5937z00zz__datez00 = 
bstring_to_symbol(BGl_string5938z00zz__datez00); 
BGl_list5942z00zz__datez00 = 
MAKE_YOUNG_PAIR(BGl_keyword5910z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5914z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5916z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5918z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5920z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5922z00zz__datez00, 
MAKE_YOUNG_PAIR(BGl_keyword5926z00zz__datez00, BNIL))))))); 
BGl_symbol5943z00zz__datez00 = 
bstring_to_symbol(BGl_string5944z00zz__datez00); 
BGl_symbol5981z00zz__datez00 = 
bstring_to_symbol(BGl_string5982z00zz__datez00); 
BGl_symbol5986z00zz__datez00 = 
bstring_to_symbol(BGl_string5987z00zz__datez00); 
return ( 
BGl_symbol5989z00zz__datez00 = 
bstring_to_symbol(BGl_string5990z00zz__datez00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__datez00(void)
{
{ /* Llib/date.scm 19 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__datez00(void)
{
{ /* Llib/date.scm 19 */
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1439z00_1273;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1440z00_1274;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1441z00_1275;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1442z00_1276;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1443z00_1277;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1444z00_1278;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1445z00_1279;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1446z00_1280;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1447z00_1281;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1448z00_1282;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1449z00_1283;
{ /* Llib/date.scm 676 */
 obj_t BgL_arg1450z00_1284;
BgL_arg1450z00_1284 = 
MAKE_YOUNG_PAIR(
BINT(31L), BNIL); 
BgL_arg1449z00_1283 = 
MAKE_YOUNG_PAIR(
BINT(30L), BgL_arg1450z00_1284); } 
BgL_arg1448z00_1282 = 
MAKE_YOUNG_PAIR(
BINT(31L), BgL_arg1449z00_1283); } 
BgL_arg1447z00_1281 = 
MAKE_YOUNG_PAIR(
BINT(30L), BgL_arg1448z00_1282); } 
BgL_arg1446z00_1280 = 
MAKE_YOUNG_PAIR(
BINT(31L), BgL_arg1447z00_1281); } 
BgL_arg1445z00_1279 = 
MAKE_YOUNG_PAIR(
BINT(31L), BgL_arg1446z00_1280); } 
BgL_arg1444z00_1278 = 
MAKE_YOUNG_PAIR(
BINT(30L), BgL_arg1445z00_1279); } 
BgL_arg1443z00_1277 = 
MAKE_YOUNG_PAIR(
BINT(31L), BgL_arg1444z00_1278); } 
BgL_arg1442z00_1276 = 
MAKE_YOUNG_PAIR(
BINT(30L), BgL_arg1443z00_1277); } 
BgL_arg1441z00_1275 = 
MAKE_YOUNG_PAIR(
BINT(31L), BgL_arg1442z00_1276); } 
BgL_arg1440z00_1274 = 
MAKE_YOUNG_PAIR(
BINT(28L), BgL_arg1441z00_1275); } 
BgL_arg1439z00_1273 = 
MAKE_YOUNG_PAIR(
BINT(31L), BgL_arg1440z00_1274); } 
BGl_za2monthzd2lengthsza2zd2zz__datez00 = 
BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_arg1439z00_1273); } 
{ /* Llib/date.scm 979 */
 obj_t BgL_zc3z04anonymousza31451ze3z87_9758;
{ 
 int BgL_tmpz00_12030;
BgL_tmpz00_12030 = 
(int)(0L); 
BgL_zc3z04anonymousza31451ze3z87_9758 = 
MAKE_EL_PROCEDURE(BgL_tmpz00_12030); } 
BGl_dayzd2ofzd2weekzd2grammarzd2zz__datez00 = BgL_zc3z04anonymousza31451ze3z87_9758; } 
BGl_thezd2fixnumzd2grammarz00zz__datez00 = BGl_proc5743z00zz__datez00; 
BGl_monthzd2grammarzd2zz__datez00 = BGl_proc5744z00zz__datez00; 
BGl_timezd2grammarzd2zz__datez00 = BGl_proc5745z00zz__datez00; 
BGl_za2timezd2za7onesza2z75zz__datez00 = BGl_list5746z00zz__datez00; 
return ( 
BGl_za7onezd2grammarz75zz__datez00 = BGl_proc5783z00zz__datez00, BUNSPEC) ;} 

}



/* &<@anonymous:2351> */
obj_t BGl_z62zc3z04anonymousza32351ze3ze5zz__datez00(obj_t BgL_envz00_9759, obj_t BgL_iportz00_9760)
{
{ /* Llib/date.scm 1121 */
{ 
 obj_t BgL_iportz00_11136; long BgL_lastzd2matchzd2_11137; long BgL_forwardz00_11138; long BgL_bufposz00_11139; obj_t BgL_iportz00_11123; long BgL_lastzd2matchzd2_11124; long BgL_forwardz00_11125; long BgL_bufposz00_11126; obj_t BgL_iportz00_11110; long BgL_lastzd2matchzd2_11111; long BgL_forwardz00_11112; long BgL_bufposz00_11113; obj_t BgL_iportz00_11097; long BgL_lastzd2matchzd2_11098; long BgL_forwardz00_11099; long BgL_bufposz00_11100; obj_t BgL_iportz00_11084; long BgL_lastzd2matchzd2_11085; long BgL_forwardz00_11086; long BgL_bufposz00_11087; obj_t BgL_iportz00_11070; long BgL_lastzd2matchzd2_11071; long BgL_forwardz00_11072; long BgL_bufposz00_11073; obj_t BgL_iportz00_11059; long BgL_lastzd2matchzd2_11060; long BgL_forwardz00_11061; long BgL_bufposz00_11062; obj_t BgL_iportz00_11046; long BgL_lastzd2matchzd2_11047; long BgL_forwardz00_11048; long BgL_bufposz00_11049; obj_t BgL_iportz00_11035; long BgL_lastzd2matchzd2_11036; long BgL_forwardz00_11037; long BgL_bufposz00_11038; obj_t BgL_iportz00_11024; long BgL_lastzd2matchzd2_11025; long BgL_forwardz00_11026; long BgL_bufposz00_11027; obj_t BgL_iportz00_11008; long BgL_lastzd2matchzd2_11009; long BgL_forwardz00_11010; long BgL_bufposz00_11011; obj_t BgL_iportz00_10994; long BgL_lastzd2matchzd2_10995; long BgL_forwardz00_10996; long BgL_bufposz00_10997; obj_t BgL_iportz00_10981; long BgL_lastzd2matchzd2_10982; long BgL_forwardz00_10983; long BgL_bufposz00_10984;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6289z00_12033;
{ /* Llib/date.scm 1121 */
 obj_t BgL_arg2356z00_11147;
{ /* Llib/date.scm 1121 */
 obj_t BgL_res4742z00_11148;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_res4742z00_11148 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12036;
BgL_auxz00_12036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5806z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12036,BFALSE,BFALSE);} 
BgL_arg2356z00_11147 = BgL_res4742z00_11148; } 
BgL_test6289z00_12033 = 
INPUT_PORT_CLOSEP(BgL_arg2356z00_11147); } 
if(BgL_test6289z00_12033)
{ /* Llib/date.scm 1121 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2352z00_11149;
{ /* Llib/date.scm 1121 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_11150;
{ /* Llib/date.scm 1121 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_11151;
BgL_new1077z00_11151 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 1121 */
 long BgL_arg2355z00_11152;
BgL_arg2355z00_11152 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_11151), BgL_arg2355z00_11152); } 
BgL_new1078z00_11150 = BgL_new1077z00_11151; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11150)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11150)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_12049;
{ /* Llib/date.scm 1121 */
 obj_t BgL_arg2353z00_11153;
{ /* Llib/date.scm 1121 */
 obj_t BgL_arg2354z00_11154;
{ /* Llib/date.scm 1121 */
 obj_t BgL_classz00_11155;
BgL_classz00_11155 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2354z00_11154 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_11155); } 
BgL_arg2353z00_11153 = 
VECTOR_REF(BgL_arg2354z00_11154,2L); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_auxz00_12053;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2353z00_11153))
{ /* Llib/date.scm 1121 */
BgL_auxz00_12053 = BgL_arg2353z00_11153
; }  else 
{ 
 obj_t BgL_auxz00_12056;
BgL_auxz00_12056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5806z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg2353z00_11153); 
FAILURE(BgL_auxz00_12056,BFALSE,BFALSE);} 
BgL_auxz00_12049 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_12053); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11150)))->BgL_stackz00)=((obj_t)BgL_auxz00_12049),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11150)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11150)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_12066;
{ /* Llib/date.scm 1121 */
 obj_t BgL_res4743z00_11156;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_res4743z00_11156 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12070;
BgL_auxz00_12070 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5806z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12070,BFALSE,BFALSE);} 
BgL_auxz00_12066 = BgL_res4743z00_11156; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11150)))->BgL_objz00)=((obj_t)BgL_auxz00_12066),BUNSPEC); } 
BgL_arg2352z00_11149 = BgL_new1078z00_11150; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2352z00_11149));}  else 
{ /* Llib/date.scm 1121 */
BgL_ignorez00_10906:
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10921;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10921 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12079;
BgL_auxz00_12079 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12079,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_10921); } 
{ /* Llib/date.scm 1121 */
 long BgL_matchz00_10922;
{ /* Llib/date.scm 1121 */
 long BgL_arg2650z00_10923; long BgL_arg2651z00_10924;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10925;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10925 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12086;
BgL_auxz00_12086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12086,BFALSE,BFALSE);} 
BgL_arg2650z00_10923 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_10925); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10926;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10926 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12093;
BgL_auxz00_12093 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12093,BFALSE,BFALSE);} 
BgL_arg2651z00_10924 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_10926); } 
BgL_iportz00_11070 = BgL_iportz00_9760; 
BgL_lastzd2matchzd2_11071 = 5L; 
BgL_forwardz00_11072 = BgL_arg2650z00_10923; 
BgL_bufposz00_11073 = BgL_arg2651z00_10924; 
BgL_statezd20zd21210z00_10915:
if(
(BgL_forwardz00_11072==BgL_bufposz00_11073))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6297z00_12100;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11074;
if(
INPUT_PORTP(BgL_iportz00_11070))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11074 = BgL_iportz00_11070; }  else 
{ 
 obj_t BgL_auxz00_12103;
BgL_auxz00_12103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5800z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11070); 
FAILURE(BgL_auxz00_12103,BFALSE,BFALSE);} 
BgL_test6297z00_12100 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11074); } 
if(BgL_test6297z00_12100)
{ /* Llib/date.scm 1121 */
 long BgL_arg2407z00_11075; long BgL_arg2408z00_11076;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11077;
if(
INPUT_PORTP(BgL_iportz00_11070))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11077 = BgL_iportz00_11070; }  else 
{ 
 obj_t BgL_auxz00_12110;
BgL_auxz00_12110 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5800z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11070); 
FAILURE(BgL_auxz00_12110,BFALSE,BFALSE);} 
BgL_arg2407z00_11075 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11077); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11078;
if(
INPUT_PORTP(BgL_iportz00_11070))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11078 = BgL_iportz00_11070; }  else 
{ 
 obj_t BgL_auxz00_12117;
BgL_auxz00_12117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5800z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11070); 
FAILURE(BgL_auxz00_12117,BFALSE,BFALSE);} 
BgL_arg2408z00_11076 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11078); } 
{ 
 long BgL_bufposz00_12123; long BgL_forwardz00_12122;
BgL_forwardz00_12122 = BgL_arg2407z00_11075; 
BgL_bufposz00_12123 = BgL_arg2408z00_11076; 
BgL_bufposz00_11073 = BgL_bufposz00_12123; 
BgL_forwardz00_11072 = BgL_forwardz00_12122; 
goto BgL_statezd20zd21210z00_10915;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11071; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11079;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11080;
if(
INPUT_PORTP(BgL_iportz00_11070))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11080 = BgL_iportz00_11070; }  else 
{ 
 obj_t BgL_auxz00_12126;
BgL_auxz00_12126 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5800z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11070); 
FAILURE(BgL_auxz00_12126,BFALSE,BFALSE);} 
BgL_curz00_11079 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11080, BgL_forwardz00_11072); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6302z00_12131;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6303z00_12132;
if(
(
(long)(BgL_curz00_11079)>=65L))
{ /* Llib/date.scm 1121 */
BgL_test6303z00_12132 = 
(
(long)(BgL_curz00_11079)<91L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6303z00_12132 = ((bool_t)0)
; } 
if(BgL_test6303z00_12132)
{ /* Llib/date.scm 1121 */
BgL_test6302z00_12131 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11079)>=97L))
{ /* Llib/date.scm 1121 */
BgL_test6302z00_12131 = 
(
(long)(BgL_curz00_11079)<123L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6302z00_12131 = ((bool_t)0)
; } } } 
if(BgL_test6302z00_12131)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11123 = BgL_iportz00_11070; 
BgL_lastzd2matchzd2_11124 = BgL_lastzd2matchzd2_11071; 
BgL_forwardz00_11125 = 
(1L+BgL_forwardz00_11072); 
BgL_bufposz00_11126 = BgL_bufposz00_11073; 
BgL_statezd25zd21215z00_10919:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11127;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11128;
if(
INPUT_PORTP(BgL_iportz00_11123))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11128 = BgL_iportz00_11123; }  else 
{ 
 obj_t BgL_auxz00_12145;
BgL_auxz00_12145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5804z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11123); 
FAILURE(BgL_auxz00_12145,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11128, BgL_forwardz00_11125); } 
BgL_newzd2matchzd2_11127 = 5L; 
if(
(BgL_forwardz00_11125==BgL_bufposz00_11126))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6308z00_12152;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11129;
if(
INPUT_PORTP(BgL_iportz00_11123))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11129 = BgL_iportz00_11123; }  else 
{ 
 obj_t BgL_auxz00_12155;
BgL_auxz00_12155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5804z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11123); 
FAILURE(BgL_auxz00_12155,BFALSE,BFALSE);} 
BgL_test6308z00_12152 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11129); } 
if(BgL_test6308z00_12152)
{ /* Llib/date.scm 1121 */
 long BgL_arg2370z00_11130; long BgL_arg2371z00_11131;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11132;
if(
INPUT_PORTP(BgL_iportz00_11123))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11132 = BgL_iportz00_11123; }  else 
{ 
 obj_t BgL_auxz00_12162;
BgL_auxz00_12162 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5804z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11123); 
FAILURE(BgL_auxz00_12162,BFALSE,BFALSE);} 
BgL_arg2370z00_11130 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11132); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11133;
if(
INPUT_PORTP(BgL_iportz00_11123))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11133 = BgL_iportz00_11123; }  else 
{ 
 obj_t BgL_auxz00_12169;
BgL_auxz00_12169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5804z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11123); 
FAILURE(BgL_auxz00_12169,BFALSE,BFALSE);} 
BgL_arg2371z00_11131 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11133); } 
{ 
 long BgL_bufposz00_12175; long BgL_forwardz00_12174;
BgL_forwardz00_12174 = BgL_arg2370z00_11130; 
BgL_bufposz00_12175 = BgL_arg2371z00_11131; 
BgL_bufposz00_11126 = BgL_bufposz00_12175; 
BgL_forwardz00_11125 = BgL_forwardz00_12174; 
goto BgL_statezd25zd21215z00_10919;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11127; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11134;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11135;
if(
INPUT_PORTP(BgL_iportz00_11123))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11135 = BgL_iportz00_11123; }  else 
{ 
 obj_t BgL_auxz00_12178;
BgL_auxz00_12178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5804z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11123); 
FAILURE(BgL_auxz00_12178,BFALSE,BFALSE);} 
BgL_curz00_11134 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11135, BgL_forwardz00_11125); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6313z00_12183;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6314z00_12184;
if(
(
(long)(BgL_curz00_11134)>=65L))
{ /* Llib/date.scm 1121 */
BgL_test6314z00_12184 = 
(
(long)(BgL_curz00_11134)<91L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6314z00_12184 = ((bool_t)0)
; } 
if(BgL_test6314z00_12184)
{ /* Llib/date.scm 1121 */
BgL_test6313z00_12183 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11134)>=97L))
{ /* Llib/date.scm 1121 */
BgL_test6313z00_12183 = 
(
(long)(BgL_curz00_11134)<123L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6313z00_12183 = ((bool_t)0)
; } } } 
if(BgL_test6313z00_12183)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11097 = BgL_iportz00_11123; 
BgL_lastzd2matchzd2_11098 = BgL_newzd2matchzd2_11127; 
BgL_forwardz00_11099 = 
(1L+BgL_forwardz00_11125); 
BgL_bufposz00_11100 = BgL_bufposz00_11126; 
BgL_statezd26zd21216z00_10917:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11101;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11102;
if(
INPUT_PORTP(BgL_iportz00_11097))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11102 = BgL_iportz00_11097; }  else 
{ 
 obj_t BgL_auxz00_12197;
BgL_auxz00_12197 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5802z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11097); 
FAILURE(BgL_auxz00_12197,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11102, BgL_forwardz00_11099); } 
BgL_newzd2matchzd2_11101 = 4L; 
if(
(BgL_forwardz00_11099==BgL_bufposz00_11100))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6319z00_12204;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11103;
if(
INPUT_PORTP(BgL_iportz00_11097))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11103 = BgL_iportz00_11097; }  else 
{ 
 obj_t BgL_auxz00_12207;
BgL_auxz00_12207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5802z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11097); 
FAILURE(BgL_auxz00_12207,BFALSE,BFALSE);} 
BgL_test6319z00_12204 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11103); } 
if(BgL_test6319z00_12204)
{ /* Llib/date.scm 1121 */
 long BgL_arg2387z00_11104; long BgL_arg2388z00_11105;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11106;
if(
INPUT_PORTP(BgL_iportz00_11097))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11106 = BgL_iportz00_11097; }  else 
{ 
 obj_t BgL_auxz00_12214;
BgL_auxz00_12214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5802z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11097); 
FAILURE(BgL_auxz00_12214,BFALSE,BFALSE);} 
BgL_arg2387z00_11104 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11106); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11107;
if(
INPUT_PORTP(BgL_iportz00_11097))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11107 = BgL_iportz00_11097; }  else 
{ 
 obj_t BgL_auxz00_12221;
BgL_auxz00_12221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5802z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11097); 
FAILURE(BgL_auxz00_12221,BFALSE,BFALSE);} 
BgL_arg2388z00_11105 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11107); } 
{ 
 long BgL_bufposz00_12227; long BgL_forwardz00_12226;
BgL_forwardz00_12226 = BgL_arg2387z00_11104; 
BgL_bufposz00_12227 = BgL_arg2388z00_11105; 
BgL_bufposz00_11100 = BgL_bufposz00_12227; 
BgL_forwardz00_11099 = BgL_forwardz00_12226; 
goto BgL_statezd26zd21216z00_10917;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11101; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11108;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11109;
if(
INPUT_PORTP(BgL_iportz00_11097))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11109 = BgL_iportz00_11097; }  else 
{ 
 obj_t BgL_auxz00_12230;
BgL_auxz00_12230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5802z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11097); 
FAILURE(BgL_auxz00_12230,BFALSE,BFALSE);} 
BgL_curz00_11108 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11109, BgL_forwardz00_11099); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6324z00_12235;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6325z00_12236;
if(
(
(long)(BgL_curz00_11108)>=65L))
{ /* Llib/date.scm 1121 */
BgL_test6325z00_12236 = 
(
(long)(BgL_curz00_11108)<91L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6325z00_12236 = ((bool_t)0)
; } 
if(BgL_test6325z00_12236)
{ /* Llib/date.scm 1121 */
BgL_test6324z00_12235 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11108)>=97L))
{ /* Llib/date.scm 1121 */
BgL_test6324z00_12235 = 
(
(long)(BgL_curz00_11108)<123L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6324z00_12235 = ((bool_t)0)
; } } } 
if(BgL_test6324z00_12235)
{ 
 long BgL_forwardz00_12248; long BgL_lastzd2matchzd2_12247;
BgL_lastzd2matchzd2_12247 = BgL_newzd2matchzd2_11101; 
BgL_forwardz00_12248 = 
(1L+BgL_forwardz00_11099); 
BgL_forwardz00_11099 = BgL_forwardz00_12248; 
BgL_lastzd2matchzd2_11098 = BgL_lastzd2matchzd2_12247; 
goto BgL_statezd26zd21216z00_10917;}  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11101; } } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11127; } } } } } }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11079)==45L))
{ /* Llib/date.scm 1121 */
BgL_iportz00_11046 = BgL_iportz00_11070; 
BgL_lastzd2matchzd2_11047 = BgL_lastzd2matchzd2_11071; 
BgL_forwardz00_11048 = 
(1L+BgL_forwardz00_11072); 
BgL_bufposz00_11049 = BgL_bufposz00_11073; 
BgL_statezd24zd21214z00_10913:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11050;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11051;
if(
INPUT_PORTP(BgL_iportz00_11046))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11051 = BgL_iportz00_11046; }  else 
{ 
 obj_t BgL_auxz00_12257;
BgL_auxz00_12257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5798z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11046); 
FAILURE(BgL_auxz00_12257,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11051, BgL_forwardz00_11048); } 
BgL_newzd2matchzd2_11050 = 5L; 
if(
(BgL_forwardz00_11048==BgL_bufposz00_11049))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6331z00_12264;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11052;
if(
INPUT_PORTP(BgL_iportz00_11046))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11052 = BgL_iportz00_11046; }  else 
{ 
 obj_t BgL_auxz00_12267;
BgL_auxz00_12267 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5798z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11046); 
FAILURE(BgL_auxz00_12267,BFALSE,BFALSE);} 
BgL_test6331z00_12264 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11052); } 
if(BgL_test6331z00_12264)
{ /* Llib/date.scm 1121 */
 long BgL_arg2437z00_11053; long BgL_arg2438z00_11054;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11055;
if(
INPUT_PORTP(BgL_iportz00_11046))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11055 = BgL_iportz00_11046; }  else 
{ 
 obj_t BgL_auxz00_12274;
BgL_auxz00_12274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5798z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11046); 
FAILURE(BgL_auxz00_12274,BFALSE,BFALSE);} 
BgL_arg2437z00_11053 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11055); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11056;
if(
INPUT_PORTP(BgL_iportz00_11046))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11056 = BgL_iportz00_11046; }  else 
{ 
 obj_t BgL_auxz00_12281;
BgL_auxz00_12281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5798z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11046); 
FAILURE(BgL_auxz00_12281,BFALSE,BFALSE);} 
BgL_arg2438z00_11054 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11056); } 
{ 
 long BgL_bufposz00_12287; long BgL_forwardz00_12286;
BgL_forwardz00_12286 = BgL_arg2437z00_11053; 
BgL_bufposz00_12287 = BgL_arg2438z00_11054; 
BgL_bufposz00_11049 = BgL_bufposz00_12287; 
BgL_forwardz00_11048 = BgL_forwardz00_12286; 
goto BgL_statezd24zd21214z00_10913;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11050; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11057;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11058;
if(
INPUT_PORTP(BgL_iportz00_11046))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11058 = BgL_iportz00_11046; }  else 
{ 
 obj_t BgL_auxz00_12290;
BgL_auxz00_12290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5798z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11046); 
FAILURE(BgL_auxz00_12290,BFALSE,BFALSE);} 
BgL_curz00_11057 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11058, BgL_forwardz00_11048); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6336z00_12295;
if(
(
(long)(BgL_curz00_11057)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6336z00_12295 = 
(
(long)(BgL_curz00_11057)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6336z00_12295 = ((bool_t)0)
; } 
if(BgL_test6336z00_12295)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11035 = BgL_iportz00_11046; 
BgL_lastzd2matchzd2_11036 = BgL_newzd2matchzd2_11050; 
BgL_forwardz00_11037 = 
(1L+BgL_forwardz00_11048); 
BgL_bufposz00_11038 = BgL_bufposz00_11049; 
BgL_statezd29zd21219z00_10912:
if(
(BgL_forwardz00_11037==BgL_bufposz00_11038))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6339z00_12303;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11039;
if(
INPUT_PORTP(BgL_iportz00_11035))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11039 = BgL_iportz00_11035; }  else 
{ 
 obj_t BgL_auxz00_12306;
BgL_auxz00_12306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5797z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11035); 
FAILURE(BgL_auxz00_12306,BFALSE,BFALSE);} 
BgL_test6339z00_12303 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11039); } 
if(BgL_test6339z00_12303)
{ /* Llib/date.scm 1121 */
 long BgL_arg2449z00_11040; long BgL_arg2450z00_11041;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11042;
if(
INPUT_PORTP(BgL_iportz00_11035))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11042 = BgL_iportz00_11035; }  else 
{ 
 obj_t BgL_auxz00_12313;
BgL_auxz00_12313 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5797z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11035); 
FAILURE(BgL_auxz00_12313,BFALSE,BFALSE);} 
BgL_arg2449z00_11040 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11042); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11043;
if(
INPUT_PORTP(BgL_iportz00_11035))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11043 = BgL_iportz00_11035; }  else 
{ 
 obj_t BgL_auxz00_12320;
BgL_auxz00_12320 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5797z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11035); 
FAILURE(BgL_auxz00_12320,BFALSE,BFALSE);} 
BgL_arg2450z00_11041 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11043); } 
{ 
 long BgL_bufposz00_12326; long BgL_forwardz00_12325;
BgL_forwardz00_12325 = BgL_arg2449z00_11040; 
BgL_bufposz00_12326 = BgL_arg2450z00_11041; 
BgL_bufposz00_11038 = BgL_bufposz00_12326; 
BgL_forwardz00_11037 = BgL_forwardz00_12325; 
goto BgL_statezd29zd21219z00_10912;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11036; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11044;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11045;
if(
INPUT_PORTP(BgL_iportz00_11035))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11045 = BgL_iportz00_11035; }  else 
{ 
 obj_t BgL_auxz00_12329;
BgL_auxz00_12329 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5797z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11035); 
FAILURE(BgL_auxz00_12329,BFALSE,BFALSE);} 
BgL_curz00_11044 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11045, BgL_forwardz00_11037); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6344z00_12334;
if(
(
(long)(BgL_curz00_11044)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6344z00_12334 = 
(
(long)(BgL_curz00_11044)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6344z00_12334 = ((bool_t)0)
; } 
if(BgL_test6344z00_12334)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11024 = BgL_iportz00_11035; 
BgL_lastzd2matchzd2_11025 = BgL_lastzd2matchzd2_11036; 
BgL_forwardz00_11026 = 
(1L+BgL_forwardz00_11037); 
BgL_bufposz00_11027 = BgL_bufposz00_11038; 
BgL_statezd210zd21220z00_10911:
if(
(BgL_forwardz00_11026==BgL_bufposz00_11027))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6347z00_12342;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11028;
if(
INPUT_PORTP(BgL_iportz00_11024))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11028 = BgL_iportz00_11024; }  else 
{ 
 obj_t BgL_auxz00_12345;
BgL_auxz00_12345 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5796z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11024); 
FAILURE(BgL_auxz00_12345,BFALSE,BFALSE);} 
BgL_test6347z00_12342 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11028); } 
if(BgL_test6347z00_12342)
{ /* Llib/date.scm 1121 */
 long BgL_arg2457z00_11029; long BgL_arg2458z00_11030;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11031;
if(
INPUT_PORTP(BgL_iportz00_11024))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11031 = BgL_iportz00_11024; }  else 
{ 
 obj_t BgL_auxz00_12352;
BgL_auxz00_12352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5796z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11024); 
FAILURE(BgL_auxz00_12352,BFALSE,BFALSE);} 
BgL_arg2457z00_11029 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11031); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11032;
if(
INPUT_PORTP(BgL_iportz00_11024))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11032 = BgL_iportz00_11024; }  else 
{ 
 obj_t BgL_auxz00_12359;
BgL_auxz00_12359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5796z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11024); 
FAILURE(BgL_auxz00_12359,BFALSE,BFALSE);} 
BgL_arg2458z00_11030 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11032); } 
{ 
 long BgL_bufposz00_12365; long BgL_forwardz00_12364;
BgL_forwardz00_12364 = BgL_arg2457z00_11029; 
BgL_bufposz00_12365 = BgL_arg2458z00_11030; 
BgL_bufposz00_11027 = BgL_bufposz00_12365; 
BgL_forwardz00_11026 = BgL_forwardz00_12364; 
goto BgL_statezd210zd21220z00_10911;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11025; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11033;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11034;
if(
INPUT_PORTP(BgL_iportz00_11024))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11034 = BgL_iportz00_11024; }  else 
{ 
 obj_t BgL_auxz00_12368;
BgL_auxz00_12368 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5796z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11024); 
FAILURE(BgL_auxz00_12368,BFALSE,BFALSE);} 
BgL_curz00_11033 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11034, BgL_forwardz00_11026); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6352z00_12373;
if(
(
(long)(BgL_curz00_11033)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6352z00_12373 = 
(
(long)(BgL_curz00_11033)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6352z00_12373 = ((bool_t)0)
; } 
if(BgL_test6352z00_12373)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11008 = BgL_iportz00_11024; 
BgL_lastzd2matchzd2_11009 = BgL_lastzd2matchzd2_11025; 
BgL_forwardz00_11010 = 
(1L+BgL_forwardz00_11026); 
BgL_bufposz00_11011 = BgL_bufposz00_11027; 
BgL_statezd211zd21221z00_10910:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11012;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11013;
if(
INPUT_PORTP(BgL_iportz00_11008))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11013 = BgL_iportz00_11008; }  else 
{ 
 obj_t BgL_auxz00_12381;
BgL_auxz00_12381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5795z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11008); 
FAILURE(BgL_auxz00_12381,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11013, BgL_forwardz00_11010); } 
BgL_newzd2matchzd2_11012 = 2L; 
if(
(BgL_forwardz00_11010==BgL_bufposz00_11011))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6356z00_12388;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11014;
if(
INPUT_PORTP(BgL_iportz00_11008))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11014 = BgL_iportz00_11008; }  else 
{ 
 obj_t BgL_auxz00_12391;
BgL_auxz00_12391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5795z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11008); 
FAILURE(BgL_auxz00_12391,BFALSE,BFALSE);} 
BgL_test6356z00_12388 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11014); } 
if(BgL_test6356z00_12388)
{ /* Llib/date.scm 1121 */
 long BgL_arg2465z00_11015; long BgL_arg2466z00_11016;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11017;
if(
INPUT_PORTP(BgL_iportz00_11008))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11017 = BgL_iportz00_11008; }  else 
{ 
 obj_t BgL_auxz00_12398;
BgL_auxz00_12398 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5795z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11008); 
FAILURE(BgL_auxz00_12398,BFALSE,BFALSE);} 
BgL_arg2465z00_11015 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11017); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11018;
if(
INPUT_PORTP(BgL_iportz00_11008))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11018 = BgL_iportz00_11008; }  else 
{ 
 obj_t BgL_auxz00_12405;
BgL_auxz00_12405 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5795z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11008); 
FAILURE(BgL_auxz00_12405,BFALSE,BFALSE);} 
BgL_arg2466z00_11016 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11018); } 
{ 
 long BgL_bufposz00_12411; long BgL_forwardz00_12410;
BgL_forwardz00_12410 = BgL_arg2465z00_11015; 
BgL_bufposz00_12411 = BgL_arg2466z00_11016; 
BgL_bufposz00_11011 = BgL_bufposz00_12411; 
BgL_forwardz00_11010 = BgL_forwardz00_12410; 
goto BgL_statezd211zd21221z00_10910;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11012; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11019;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11020;
if(
INPUT_PORTP(BgL_iportz00_11008))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11020 = BgL_iportz00_11008; }  else 
{ 
 obj_t BgL_auxz00_12414;
BgL_auxz00_12414 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5795z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11008); 
FAILURE(BgL_auxz00_12414,BFALSE,BFALSE);} 
BgL_curz00_11019 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11020, BgL_forwardz00_11010); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6361z00_12419;
if(
(
(long)(BgL_curz00_11019)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6361z00_12419 = 
(
(long)(BgL_curz00_11019)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6361z00_12419 = ((bool_t)0)
; } 
if(BgL_test6361z00_12419)
{ /* Llib/date.scm 1121 */
 long BgL_arg2469z00_11021;
BgL_arg2469z00_11021 = 
(1L+BgL_forwardz00_11010); 
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11022;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11023;
if(
INPUT_PORTP(BgL_iportz00_11008))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11023 = BgL_iportz00_11008; }  else 
{ 
 obj_t BgL_auxz00_12428;
BgL_auxz00_12428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5795z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11008); 
FAILURE(BgL_auxz00_12428,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11023, BgL_arg2469z00_11021); } 
BgL_newzd2matchzd2_11022 = 1L; 
BgL_matchz00_10922 = BgL_newzd2matchzd2_11022; } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11012; } } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11025; } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11036; } } } } }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11057)==45L))
{ /* Llib/date.scm 1121 */
BgL_iportz00_11136 = BgL_iportz00_11046; 
BgL_lastzd2matchzd2_11137 = BgL_newzd2matchzd2_11050; 
BgL_forwardz00_11138 = 
(1L+BgL_forwardz00_11048); 
BgL_bufposz00_11139 = BgL_bufposz00_11049; 
BgL_statezd28zd21218z00_10920:
if(
(BgL_forwardz00_11138==BgL_bufposz00_11139))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6366z00_12441;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11140;
if(
INPUT_PORTP(BgL_iportz00_11136))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11140 = BgL_iportz00_11136; }  else 
{ 
 obj_t BgL_auxz00_12444;
BgL_auxz00_12444 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5805z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11136); 
FAILURE(BgL_auxz00_12444,BFALSE,BFALSE);} 
BgL_test6366z00_12441 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11140); } 
if(BgL_test6366z00_12441)
{ /* Llib/date.scm 1121 */
 long BgL_arg2361z00_11141; long BgL_arg2363z00_11142;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11143;
if(
INPUT_PORTP(BgL_iportz00_11136))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11143 = BgL_iportz00_11136; }  else 
{ 
 obj_t BgL_auxz00_12451;
BgL_auxz00_12451 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5805z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11136); 
FAILURE(BgL_auxz00_12451,BFALSE,BFALSE);} 
BgL_arg2361z00_11141 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11143); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11144;
if(
INPUT_PORTP(BgL_iportz00_11136))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11144 = BgL_iportz00_11136; }  else 
{ 
 obj_t BgL_auxz00_12458;
BgL_auxz00_12458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5805z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11136); 
FAILURE(BgL_auxz00_12458,BFALSE,BFALSE);} 
BgL_arg2363z00_11142 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11144); } 
{ 
 long BgL_bufposz00_12464; long BgL_forwardz00_12463;
BgL_forwardz00_12463 = BgL_arg2361z00_11141; 
BgL_bufposz00_12464 = BgL_arg2363z00_11142; 
BgL_bufposz00_11139 = BgL_bufposz00_12464; 
BgL_forwardz00_11138 = BgL_forwardz00_12463; 
goto BgL_statezd28zd21218z00_10920;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11137; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11145;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11146;
if(
INPUT_PORTP(BgL_iportz00_11136))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11146 = BgL_iportz00_11136; }  else 
{ 
 obj_t BgL_auxz00_12467;
BgL_auxz00_12467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5805z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11136); 
FAILURE(BgL_auxz00_12467,BFALSE,BFALSE);} 
BgL_curz00_11145 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11146, BgL_forwardz00_11138); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6371z00_12472;
if(
(
(long)(BgL_curz00_11145)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6371z00_12472 = 
(
(long)(BgL_curz00_11145)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6371z00_12472 = ((bool_t)0)
; } 
if(BgL_test6371z00_12472)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11059 = BgL_iportz00_11136; 
BgL_lastzd2matchzd2_11060 = BgL_lastzd2matchzd2_11137; 
BgL_forwardz00_11061 = 
(1L+BgL_forwardz00_11138); 
BgL_bufposz00_11062 = BgL_bufposz00_11139; 
BgL_statezd213zd21223z00_10914:
if(
(BgL_forwardz00_11061==BgL_bufposz00_11062))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6374z00_12480;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11063;
if(
INPUT_PORTP(BgL_iportz00_11059))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11063 = BgL_iportz00_11059; }  else 
{ 
 obj_t BgL_auxz00_12483;
BgL_auxz00_12483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5799z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11059); 
FAILURE(BgL_auxz00_12483,BFALSE,BFALSE);} 
BgL_test6374z00_12480 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11063); } 
if(BgL_test6374z00_12480)
{ /* Llib/date.scm 1121 */
 long BgL_arg2428z00_11064; long BgL_arg2429z00_11065;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11066;
if(
INPUT_PORTP(BgL_iportz00_11059))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11066 = BgL_iportz00_11059; }  else 
{ 
 obj_t BgL_auxz00_12490;
BgL_auxz00_12490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5799z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11059); 
FAILURE(BgL_auxz00_12490,BFALSE,BFALSE);} 
BgL_arg2428z00_11064 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11066); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11067;
if(
INPUT_PORTP(BgL_iportz00_11059))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11067 = BgL_iportz00_11059; }  else 
{ 
 obj_t BgL_auxz00_12497;
BgL_auxz00_12497 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5799z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11059); 
FAILURE(BgL_auxz00_12497,BFALSE,BFALSE);} 
BgL_arg2429z00_11065 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11067); } 
{ 
 long BgL_bufposz00_12503; long BgL_forwardz00_12502;
BgL_forwardz00_12502 = BgL_arg2428z00_11064; 
BgL_bufposz00_12503 = BgL_arg2429z00_11065; 
BgL_bufposz00_11062 = BgL_bufposz00_12503; 
BgL_forwardz00_11061 = BgL_forwardz00_12502; 
goto BgL_statezd213zd21223z00_10914;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11060; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11068;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11069;
if(
INPUT_PORTP(BgL_iportz00_11059))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11069 = BgL_iportz00_11059; }  else 
{ 
 obj_t BgL_auxz00_12506;
BgL_auxz00_12506 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5799z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11059); 
FAILURE(BgL_auxz00_12506,BFALSE,BFALSE);} 
BgL_curz00_11068 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11069, BgL_forwardz00_11061); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6379z00_12511;
if(
(
(long)(BgL_curz00_11068)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6379z00_12511 = 
(
(long)(BgL_curz00_11068)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6379z00_12511 = ((bool_t)0)
; } 
if(BgL_test6379z00_12511)
{ /* Llib/date.scm 1121 */
BgL_iportz00_10994 = BgL_iportz00_11059; 
BgL_lastzd2matchzd2_10995 = BgL_lastzd2matchzd2_11060; 
BgL_forwardz00_10996 = 
(1L+BgL_forwardz00_11061); 
BgL_bufposz00_10997 = BgL_bufposz00_11062; 
BgL_statezd214zd21224z00_10909:
if(
(BgL_forwardz00_10996==BgL_bufposz00_10997))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6382z00_12519;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10998;
if(
INPUT_PORTP(BgL_iportz00_10994))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10998 = BgL_iportz00_10994; }  else 
{ 
 obj_t BgL_auxz00_12522;
BgL_auxz00_12522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5794z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10994); 
FAILURE(BgL_auxz00_12522,BFALSE,BFALSE);} 
BgL_test6382z00_12519 = 
rgc_fill_buffer(BgL_inputzd2portzd2_10998); } 
if(BgL_test6382z00_12519)
{ /* Llib/date.scm 1121 */
 long BgL_arg2473z00_10999; long BgL_arg2474z00_11000;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11001;
if(
INPUT_PORTP(BgL_iportz00_10994))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11001 = BgL_iportz00_10994; }  else 
{ 
 obj_t BgL_auxz00_12529;
BgL_auxz00_12529 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5794z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10994); 
FAILURE(BgL_auxz00_12529,BFALSE,BFALSE);} 
BgL_arg2473z00_10999 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11001); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11002;
if(
INPUT_PORTP(BgL_iportz00_10994))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11002 = BgL_iportz00_10994; }  else 
{ 
 obj_t BgL_auxz00_12536;
BgL_auxz00_12536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5794z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10994); 
FAILURE(BgL_auxz00_12536,BFALSE,BFALSE);} 
BgL_arg2474z00_11000 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11002); } 
{ 
 long BgL_bufposz00_12542; long BgL_forwardz00_12541;
BgL_forwardz00_12541 = BgL_arg2473z00_10999; 
BgL_bufposz00_12542 = BgL_arg2474z00_11000; 
BgL_bufposz00_10997 = BgL_bufposz00_12542; 
BgL_forwardz00_10996 = BgL_forwardz00_12541; 
goto BgL_statezd214zd21224z00_10909;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_10995; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11003;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11004;
if(
INPUT_PORTP(BgL_iportz00_10994))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11004 = BgL_iportz00_10994; }  else 
{ 
 obj_t BgL_auxz00_12545;
BgL_auxz00_12545 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5794z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10994); 
FAILURE(BgL_auxz00_12545,BFALSE,BFALSE);} 
BgL_curz00_11003 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11004, BgL_forwardz00_10996); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6387z00_12550;
if(
(
(long)(BgL_curz00_11003)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6387z00_12550 = 
(
(long)(BgL_curz00_11003)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6387z00_12550 = ((bool_t)0)
; } 
if(BgL_test6387z00_12550)
{ /* Llib/date.scm 1121 */
 long BgL_arg2479z00_11005;
BgL_arg2479z00_11005 = 
(1L+BgL_forwardz00_10996); 
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11006;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11007;
if(
INPUT_PORTP(BgL_iportz00_10994))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11007 = BgL_iportz00_10994; }  else 
{ 
 obj_t BgL_auxz00_12559;
BgL_auxz00_12559 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5794z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10994); 
FAILURE(BgL_auxz00_12559,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11007, BgL_arg2479z00_11005); } 
BgL_newzd2matchzd2_11006 = 3L; 
BgL_matchz00_10922 = BgL_newzd2matchzd2_11006; } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_10995; } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11060; } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_lastzd2matchzd2_11137; } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11050; } } } } } } }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11079)==43L))
{ /* Llib/date.scm 1121 */
BgL_iportz00_11110 = BgL_iportz00_11070; 
BgL_lastzd2matchzd2_11111 = BgL_lastzd2matchzd2_11071; 
BgL_forwardz00_11112 = 
(1L+BgL_forwardz00_11072); 
BgL_bufposz00_11113 = BgL_bufposz00_11073; 
BgL_statezd23zd21213z00_10918:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11114;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11115;
if(
INPUT_PORTP(BgL_iportz00_11110))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11115 = BgL_iportz00_11110; }  else 
{ 
 obj_t BgL_auxz00_12573;
BgL_auxz00_12573 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5803z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11110); 
FAILURE(BgL_auxz00_12573,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11115, BgL_forwardz00_11112); } 
BgL_newzd2matchzd2_11114 = 5L; 
if(
(BgL_forwardz00_11112==BgL_bufposz00_11113))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6393z00_12580;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11116;
if(
INPUT_PORTP(BgL_iportz00_11110))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11116 = BgL_iportz00_11110; }  else 
{ 
 obj_t BgL_auxz00_12583;
BgL_auxz00_12583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5803z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11110); 
FAILURE(BgL_auxz00_12583,BFALSE,BFALSE);} 
BgL_test6393z00_12580 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11116); } 
if(BgL_test6393z00_12580)
{ /* Llib/date.scm 1121 */
 long BgL_arg2379z00_11117; long BgL_arg2380z00_11118;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11119;
if(
INPUT_PORTP(BgL_iportz00_11110))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11119 = BgL_iportz00_11110; }  else 
{ 
 obj_t BgL_auxz00_12590;
BgL_auxz00_12590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5803z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11110); 
FAILURE(BgL_auxz00_12590,BFALSE,BFALSE);} 
BgL_arg2379z00_11117 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11119); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11120;
if(
INPUT_PORTP(BgL_iportz00_11110))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11120 = BgL_iportz00_11110; }  else 
{ 
 obj_t BgL_auxz00_12597;
BgL_auxz00_12597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5803z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11110); 
FAILURE(BgL_auxz00_12597,BFALSE,BFALSE);} 
BgL_arg2380z00_11118 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11120); } 
{ 
 long BgL_bufposz00_12603; long BgL_forwardz00_12602;
BgL_forwardz00_12602 = BgL_arg2379z00_11117; 
BgL_bufposz00_12603 = BgL_arg2380z00_11118; 
BgL_bufposz00_11113 = BgL_bufposz00_12603; 
BgL_forwardz00_11112 = BgL_forwardz00_12602; 
goto BgL_statezd23zd21213z00_10918;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11114; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11121;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11122;
if(
INPUT_PORTP(BgL_iportz00_11110))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11122 = BgL_iportz00_11110; }  else 
{ 
 obj_t BgL_auxz00_12606;
BgL_auxz00_12606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5803z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11110); 
FAILURE(BgL_auxz00_12606,BFALSE,BFALSE);} 
BgL_curz00_11121 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11122, BgL_forwardz00_11112); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6398z00_12611;
if(
(
(long)(BgL_curz00_11121)>=48L))
{ /* Llib/date.scm 1121 */
BgL_test6398z00_12611 = 
(
(long)(BgL_curz00_11121)<58L)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6398z00_12611 = ((bool_t)0)
; } 
if(BgL_test6398z00_12611)
{ 
 long BgL_bufposz00_12621; long BgL_forwardz00_12619; long BgL_lastzd2matchzd2_12618; obj_t BgL_iportz00_12617;
BgL_iportz00_12617 = BgL_iportz00_11110; 
BgL_lastzd2matchzd2_12618 = BgL_newzd2matchzd2_11114; 
BgL_forwardz00_12619 = 
(1L+BgL_forwardz00_11112); 
BgL_bufposz00_12621 = BgL_bufposz00_11113; 
BgL_bufposz00_11038 = BgL_bufposz00_12621; 
BgL_forwardz00_11037 = BgL_forwardz00_12619; 
BgL_lastzd2matchzd2_11036 = BgL_lastzd2matchzd2_12618; 
BgL_iportz00_11035 = BgL_iportz00_12617; 
goto BgL_statezd29zd21219z00_10912;}  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11114; } } } } } }  else 
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6400z00_12623;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6401z00_12624;
if(
(
(long)(BgL_curz00_11079)==10L))
{ /* Llib/date.scm 1121 */
BgL_test6401z00_12624 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6401z00_12624 = 
(
(long)(BgL_curz00_11079)==9L)
; } 
if(BgL_test6401z00_12624)
{ /* Llib/date.scm 1121 */
BgL_test6400z00_12623 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11079)==13L))
{ /* Llib/date.scm 1121 */
BgL_test6400z00_12623 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6400z00_12623 = 
(
(long)(BgL_curz00_11079)==32L)
; } } } 
if(BgL_test6400z00_12623)
{ /* Llib/date.scm 1121 */
BgL_iportz00_11084 = BgL_iportz00_11070; 
BgL_lastzd2matchzd2_11085 = BgL_lastzd2matchzd2_11071; 
BgL_forwardz00_11086 = 
(1L+BgL_forwardz00_11072); 
BgL_bufposz00_11087 = BgL_bufposz00_11073; 
BgL_statezd22zd21212z00_10916:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11088;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11089;
if(
INPUT_PORTP(BgL_iportz00_11084))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11089 = BgL_iportz00_11084; }  else 
{ 
 obj_t BgL_auxz00_12637;
BgL_auxz00_12637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5801z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11084); 
FAILURE(BgL_auxz00_12637,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11089, BgL_forwardz00_11086); } 
BgL_newzd2matchzd2_11088 = 0L; 
if(
(BgL_forwardz00_11086==BgL_bufposz00_11087))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6406z00_12644;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11090;
if(
INPUT_PORTP(BgL_iportz00_11084))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11090 = BgL_iportz00_11084; }  else 
{ 
 obj_t BgL_auxz00_12647;
BgL_auxz00_12647 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5801z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11084); 
FAILURE(BgL_auxz00_12647,BFALSE,BFALSE);} 
BgL_test6406z00_12644 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11090); } 
if(BgL_test6406z00_12644)
{ /* Llib/date.scm 1121 */
 long BgL_arg2396z00_11091; long BgL_arg2397z00_11092;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11093;
if(
INPUT_PORTP(BgL_iportz00_11084))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11093 = BgL_iportz00_11084; }  else 
{ 
 obj_t BgL_auxz00_12654;
BgL_auxz00_12654 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5801z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11084); 
FAILURE(BgL_auxz00_12654,BFALSE,BFALSE);} 
BgL_arg2396z00_11091 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11093); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11094;
if(
INPUT_PORTP(BgL_iportz00_11084))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11094 = BgL_iportz00_11084; }  else 
{ 
 obj_t BgL_auxz00_12661;
BgL_auxz00_12661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5801z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11084); 
FAILURE(BgL_auxz00_12661,BFALSE,BFALSE);} 
BgL_arg2397z00_11092 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11094); } 
{ 
 long BgL_bufposz00_12667; long BgL_forwardz00_12666;
BgL_forwardz00_12666 = BgL_arg2396z00_11091; 
BgL_bufposz00_12667 = BgL_arg2397z00_11092; 
BgL_bufposz00_11087 = BgL_bufposz00_12667; 
BgL_forwardz00_11086 = BgL_forwardz00_12666; 
goto BgL_statezd22zd21212z00_10916;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11088; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_11095;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11096;
if(
INPUT_PORTP(BgL_iportz00_11084))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11096 = BgL_iportz00_11084; }  else 
{ 
 obj_t BgL_auxz00_12670;
BgL_auxz00_12670 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5801z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11084); 
FAILURE(BgL_auxz00_12670,BFALSE,BFALSE);} 
BgL_curz00_11095 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11096, BgL_forwardz00_11086); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6411z00_12675;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6412z00_12676;
if(
(
(long)(BgL_curz00_11095)==10L))
{ /* Llib/date.scm 1121 */
BgL_test6412z00_12676 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6412z00_12676 = 
(
(long)(BgL_curz00_11095)==9L)
; } 
if(BgL_test6412z00_12676)
{ /* Llib/date.scm 1121 */
BgL_test6411z00_12675 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_11095)==13L))
{ /* Llib/date.scm 1121 */
BgL_test6411z00_12675 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6411z00_12675 = 
(
(long)(BgL_curz00_11095)==32L)
; } } } 
if(BgL_test6411z00_12675)
{ /* Llib/date.scm 1121 */
BgL_iportz00_10981 = BgL_iportz00_11084; 
BgL_lastzd2matchzd2_10982 = BgL_newzd2matchzd2_11088; 
BgL_forwardz00_10983 = 
(1L+BgL_forwardz00_11086); 
BgL_bufposz00_10984 = BgL_bufposz00_11087; 
BgL_statezd216zd21226z00_10908:
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_10985;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10986;
if(
INPUT_PORTP(BgL_iportz00_10981))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10986 = BgL_iportz00_10981; }  else 
{ 
 obj_t BgL_auxz00_12689;
BgL_auxz00_12689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5793z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10981); 
FAILURE(BgL_auxz00_12689,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_10986, BgL_forwardz00_10983); } 
BgL_newzd2matchzd2_10985 = 0L; 
if(
(BgL_forwardz00_10983==BgL_bufposz00_10984))
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6417z00_12696;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10987;
if(
INPUT_PORTP(BgL_iportz00_10981))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10987 = BgL_iportz00_10981; }  else 
{ 
 obj_t BgL_auxz00_12699;
BgL_auxz00_12699 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5793z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10981); 
FAILURE(BgL_auxz00_12699,BFALSE,BFALSE);} 
BgL_test6417z00_12696 = 
rgc_fill_buffer(BgL_inputzd2portzd2_10987); } 
if(BgL_test6417z00_12696)
{ /* Llib/date.scm 1121 */
 long BgL_arg2486z00_10988; long BgL_arg2487z00_10989;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10990;
if(
INPUT_PORTP(BgL_iportz00_10981))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10990 = BgL_iportz00_10981; }  else 
{ 
 obj_t BgL_auxz00_12706;
BgL_auxz00_12706 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5793z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10981); 
FAILURE(BgL_auxz00_12706,BFALSE,BFALSE);} 
BgL_arg2486z00_10988 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_10990); } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10991;
if(
INPUT_PORTP(BgL_iportz00_10981))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10991 = BgL_iportz00_10981; }  else 
{ 
 obj_t BgL_auxz00_12713;
BgL_auxz00_12713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5793z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10981); 
FAILURE(BgL_auxz00_12713,BFALSE,BFALSE);} 
BgL_arg2487z00_10989 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_10991); } 
{ 
 long BgL_bufposz00_12719; long BgL_forwardz00_12718;
BgL_forwardz00_12718 = BgL_arg2486z00_10988; 
BgL_bufposz00_12719 = BgL_arg2487z00_10989; 
BgL_bufposz00_10984 = BgL_bufposz00_12719; 
BgL_forwardz00_10983 = BgL_forwardz00_12718; 
goto BgL_statezd216zd21226z00_10908;} }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_10985; } }  else 
{ /* Llib/date.scm 1121 */
 int BgL_curz00_10992;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10993;
if(
INPUT_PORTP(BgL_iportz00_10981))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10993 = BgL_iportz00_10981; }  else 
{ 
 obj_t BgL_auxz00_12722;
BgL_auxz00_12722 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5793z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_10981); 
FAILURE(BgL_auxz00_12722,BFALSE,BFALSE);} 
BgL_curz00_10992 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_10993, BgL_forwardz00_10983); } 
{ /* Llib/date.scm 1121 */

{ /* Llib/date.scm 1121 */
 bool_t BgL_test6422z00_12727;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6423z00_12728;
if(
(
(long)(BgL_curz00_10992)==10L))
{ /* Llib/date.scm 1121 */
BgL_test6423z00_12728 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6423z00_12728 = 
(
(long)(BgL_curz00_10992)==9L)
; } 
if(BgL_test6423z00_12728)
{ /* Llib/date.scm 1121 */
BgL_test6422z00_12727 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
if(
(
(long)(BgL_curz00_10992)==13L))
{ /* Llib/date.scm 1121 */
BgL_test6422z00_12727 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1121 */
BgL_test6422z00_12727 = 
(
(long)(BgL_curz00_10992)==32L)
; } } } 
if(BgL_test6422z00_12727)
{ 
 long BgL_forwardz00_12740; long BgL_lastzd2matchzd2_12739;
BgL_lastzd2matchzd2_12739 = BgL_newzd2matchzd2_10985; 
BgL_forwardz00_12740 = 
(1L+BgL_forwardz00_10983); 
BgL_forwardz00_10983 = BgL_forwardz00_12740; 
BgL_lastzd2matchzd2_10982 = BgL_lastzd2matchzd2_12739; 
goto BgL_statezd216zd21226z00_10908;}  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_10985; } } } } } }  else 
{ /* Llib/date.scm 1121 */
BgL_matchz00_10922 = BgL_newzd2matchzd2_11088; } } } } } }  else 
{ /* Llib/date.scm 1121 */
 long BgL_arg2424z00_11081;
BgL_arg2424z00_11081 = 
(1L+BgL_forwardz00_11072); 
{ /* Llib/date.scm 1121 */
 long BgL_newzd2matchzd2_11082;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_11083;
if(
INPUT_PORTP(BgL_iportz00_11070))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_11083 = BgL_iportz00_11070; }  else 
{ 
 obj_t BgL_auxz00_12747;
BgL_auxz00_12747 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5800z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11070); 
FAILURE(BgL_auxz00_12747,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11083, BgL_arg2424z00_11081); } 
BgL_newzd2matchzd2_11082 = 5L; 
BgL_matchz00_10922 = BgL_newzd2matchzd2_11082; } } } } } } } } } 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10927;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10927 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12754;
BgL_auxz00_12754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12754,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_10927); } 
switch( BgL_matchz00_10922) { case 5L : 

{ /* Llib/date.scm 1149 */
 obj_t BgL_arg2599z00_10928; obj_t BgL_arg2600z00_10929;
{ /* Llib/date.scm 1121 */
 bool_t BgL_test6428z00_12759;
{ /* Llib/date.scm 1121 */
 long BgL_arg2592z00_10978;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10979;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10979 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12762;
BgL_auxz00_12762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12762,BFALSE,BFALSE);} 
BgL_arg2592z00_10978 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_10979); } 
BgL_test6428z00_12759 = 
(BgL_arg2592z00_10978==0L); } 
if(BgL_test6428z00_12759)
{ /* Llib/date.scm 1121 */
BgL_arg2599z00_10928 = BEOF; }  else 
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10980;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10980 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12770;
BgL_auxz00_12770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12770,BFALSE,BFALSE);} 
BgL_arg2599z00_10928 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_10980)); } } 
{ /* Llib/date.scm 1149 */
 obj_t BgL_res4741z00_10930;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_res4741z00_10930 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12778;
BgL_auxz00_12778 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12778,BFALSE,BFALSE);} 
BgL_arg2600z00_10929 = BgL_res4741z00_10930; } 
return 
BGl_parsezd2errorzd2zz__datez00(BGl_string5787z00zz__datez00, BGl_string5788z00zz__datez00, BgL_arg2599z00_10928, BgL_arg2600z00_10929);} break;case 4L : 

{ /* Llib/date.scm 1143 */
 obj_t BgL_cz00_10931;
{ /* Llib/date.scm 1143 */
 obj_t BgL_arg2605z00_10932;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10933;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10933 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12785;
BgL_auxz00_12785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12785,BFALSE,BFALSE);} 
BgL_arg2605z00_10932 = 
rgc_buffer_symbol(BgL_inputzd2portzd2_10933); } 
BgL_cz00_10931 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg2605z00_10932, BGl_list5746z00zz__datez00); } 
if(
PAIRP(BgL_cz00_10931))
{ /* Llib/date.scm 1145 */
 long BgL_za72za7_10934;
{ /* Llib/date.scm 1145 */
 obj_t BgL_tmpz00_12793;
{ /* Llib/date.scm 1145 */
 obj_t BgL_aux4909z00_10935;
BgL_aux4909z00_10935 = 
CDR(BgL_cz00_10931); 
if(
INTEGERP(BgL_aux4909z00_10935))
{ /* Llib/date.scm 1145 */
BgL_tmpz00_12793 = BgL_aux4909z00_10935
; }  else 
{ 
 obj_t BgL_auxz00_12797;
BgL_auxz00_12797 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(46625L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_aux4909z00_10935); 
FAILURE(BgL_auxz00_12797,BFALSE,BFALSE);} } 
BgL_za72za7_10934 = 
(long)CINT(BgL_tmpz00_12793); } 
return 
BINT(
(3600L*BgL_za72za7_10934));}  else 
{ /* Llib/date.scm 1144 */
return 
BINT(0L);} } break;case 3L : 

{ /* Llib/date.scm 1138 */
 long BgL_hz00_10936; long BgL_mz00_10937;
{ /* Llib/date.scm 1138 */
 int BgL_arg2609z00_10938;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10939;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10939 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12807;
BgL_auxz00_12807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12807,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12811;
BgL_tmpz00_12811 = 
(int)(2L); 
BgL_arg2609z00_10938 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10939, BgL_tmpz00_12811); } } 
BgL_hz00_10936 = 
(
(long)(BgL_arg2609z00_10938)-48L); } 
{ /* Llib/date.scm 1139 */
 long BgL_arg2610z00_10940; long BgL_arg2611z00_10941;
{ /* Llib/date.scm 1139 */
 long BgL_arg2612z00_10942;
{ /* Llib/date.scm 1139 */
 int BgL_arg2613z00_10943;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10944;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10944 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12818;
BgL_auxz00_12818 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12818,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12822;
BgL_tmpz00_12822 = 
(int)(3L); 
BgL_arg2613z00_10943 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10944, BgL_tmpz00_12822); } } 
BgL_arg2612z00_10942 = 
(
(long)(BgL_arg2613z00_10943)-48L); } 
BgL_arg2610z00_10940 = 
(10L*BgL_arg2612z00_10942); } 
{ /* Llib/date.scm 1139 */
 int BgL_arg2614z00_10945;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10946;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10946 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12830;
BgL_auxz00_12830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12830,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12834;
BgL_tmpz00_12834 = 
(int)(4L); 
BgL_arg2614z00_10945 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10946, BgL_tmpz00_12834); } } 
BgL_arg2611z00_10941 = 
(
(long)(BgL_arg2614z00_10945)-48L); } 
BgL_mz00_10937 = 
(BgL_arg2610z00_10940+BgL_arg2611z00_10941); } 
return 
BINT(
(60L*
(
(BgL_hz00_10936*60L)+BgL_mz00_10937)));} break;case 2L : 

{ /* Llib/date.scm 1131 */
 long BgL_hz00_10947; long BgL_mz00_10948;
{ /* Llib/date.scm 1131 */
 int BgL_arg2623z00_10949;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10950;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10950 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12846;
BgL_auxz00_12846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12846,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12850;
BgL_tmpz00_12850 = 
(int)(1L); 
BgL_arg2623z00_10949 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10950, BgL_tmpz00_12850); } } 
BgL_hz00_10947 = 
(
(long)(BgL_arg2623z00_10949)-48L); } 
{ /* Llib/date.scm 1132 */
 long BgL_arg2624z00_10951; long BgL_arg2626z00_10952;
{ /* Llib/date.scm 1132 */
 long BgL_arg2627z00_10953;
{ /* Llib/date.scm 1132 */
 int BgL_arg2628z00_10954;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10955;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10955 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12857;
BgL_auxz00_12857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12857,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12861;
BgL_tmpz00_12861 = 
(int)(2L); 
BgL_arg2628z00_10954 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10955, BgL_tmpz00_12861); } } 
BgL_arg2627z00_10953 = 
(
(long)(BgL_arg2628z00_10954)-48L); } 
BgL_arg2624z00_10951 = 
(10L*BgL_arg2627z00_10953); } 
{ /* Llib/date.scm 1132 */
 int BgL_arg2629z00_10956;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10957;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10957 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12869;
BgL_auxz00_12869 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12869,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12873;
BgL_tmpz00_12873 = 
(int)(3L); 
BgL_arg2629z00_10956 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10957, BgL_tmpz00_12873); } } 
BgL_arg2626z00_10952 = 
(
(long)(BgL_arg2629z00_10956)-48L); } 
BgL_mz00_10948 = 
(BgL_arg2624z00_10951+BgL_arg2626z00_10952); } 
{ /* Llib/date.scm 1133 */
 bool_t BgL_test6441z00_12879;
{ /* Llib/date.scm 1133 */
 int BgL_arg2622z00_10958;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10959;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10959 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12882;
BgL_auxz00_12882 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12882,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12886;
BgL_tmpz00_12886 = 
(int)(0L); 
BgL_arg2622z00_10958 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10959, BgL_tmpz00_12886); } } 
BgL_test6441z00_12879 = 
(
(long)(BgL_arg2622z00_10958)==45L); } 
if(BgL_test6441z00_12879)
{ /* Llib/date.scm 1133 */
return 
BINT(
NEG(
(60L*
(
(BgL_hz00_10947*60L)+BgL_mz00_10948))));}  else 
{ /* Llib/date.scm 1133 */
return 
BINT(
(60L*
(
(BgL_hz00_10947*60L)+BgL_mz00_10948)));} } } break;case 1L : 

{ /* Llib/date.scm 1125 */
 long BgL_hz00_10960; long BgL_mz00_10961;
{ /* Llib/date.scm 1125 */
 long BgL_arg2639z00_10962; long BgL_arg2640z00_10963;
{ /* Llib/date.scm 1125 */
 long BgL_arg2641z00_10964;
{ /* Llib/date.scm 1125 */
 int BgL_arg2642z00_10965;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10966;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10966 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12902;
BgL_auxz00_12902 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12902,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12906;
BgL_tmpz00_12906 = 
(int)(1L); 
BgL_arg2642z00_10965 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10966, BgL_tmpz00_12906); } } 
BgL_arg2641z00_10964 = 
(
(long)(BgL_arg2642z00_10965)-48L); } 
BgL_arg2639z00_10962 = 
(10L*BgL_arg2641z00_10964); } 
{ /* Llib/date.scm 1125 */
 int BgL_arg2643z00_10967;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10968;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10968 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12914;
BgL_auxz00_12914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12914,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12918;
BgL_tmpz00_12918 = 
(int)(2L); 
BgL_arg2643z00_10967 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10968, BgL_tmpz00_12918); } } 
BgL_arg2640z00_10963 = 
(
(long)(BgL_arg2643z00_10967)-48L); } 
BgL_hz00_10960 = 
(BgL_arg2639z00_10962+BgL_arg2640z00_10963); } 
{ /* Llib/date.scm 1126 */
 long BgL_arg2644z00_10969; long BgL_arg2645z00_10970;
{ /* Llib/date.scm 1126 */
 long BgL_arg2647z00_10971;
{ /* Llib/date.scm 1126 */
 int BgL_arg2648z00_10972;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10973;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10973 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12926;
BgL_auxz00_12926 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12926,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12930;
BgL_tmpz00_12930 = 
(int)(3L); 
BgL_arg2648z00_10972 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10973, BgL_tmpz00_12930); } } 
BgL_arg2647z00_10971 = 
(
(long)(BgL_arg2648z00_10972)-48L); } 
BgL_arg2644z00_10969 = 
(10L*BgL_arg2647z00_10971); } 
{ /* Llib/date.scm 1126 */
 int BgL_arg2649z00_10974;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10975;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10975 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12938;
BgL_auxz00_12938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12938,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12942;
BgL_tmpz00_12942 = 
(int)(4L); 
BgL_arg2649z00_10974 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10975, BgL_tmpz00_12942); } } 
BgL_arg2645z00_10970 = 
(
(long)(BgL_arg2649z00_10974)-48L); } 
BgL_mz00_10961 = 
(BgL_arg2644z00_10969+BgL_arg2645z00_10970); } 
{ /* Llib/date.scm 1127 */
 bool_t BgL_test6447z00_12948;
{ /* Llib/date.scm 1127 */
 int BgL_arg2638z00_10976;
{ /* Llib/date.scm 1121 */
 obj_t BgL_inputzd2portzd2_10977;
if(
INPUT_PORTP(BgL_iportz00_9760))
{ /* Llib/date.scm 1121 */
BgL_inputzd2portzd2_10977 = BgL_iportz00_9760; }  else 
{ 
 obj_t BgL_auxz00_12951;
BgL_auxz00_12951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(45672L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9760); 
FAILURE(BgL_auxz00_12951,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1121 */
 int BgL_tmpz00_12955;
BgL_tmpz00_12955 = 
(int)(0L); 
BgL_arg2638z00_10976 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_10977, BgL_tmpz00_12955); } } 
BgL_test6447z00_12948 = 
(
(long)(BgL_arg2638z00_10976)==45L); } 
if(BgL_test6447z00_12948)
{ /* Llib/date.scm 1127 */
return 
BINT(
NEG(
(60L*
(
(BgL_hz00_10960*60L)+BgL_mz00_10961))));}  else 
{ /* Llib/date.scm 1127 */
return 
BINT(
(60L*
(
(BgL_hz00_10960*60L)+BgL_mz00_10961)));} } } break;case 0L : 

goto BgL_ignorez00_10906;break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_10922));} } } } } } 

}



/* &<@anonymous:2062> */
obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__datez00(obj_t BgL_envz00_9761, obj_t BgL_iportz00_9762)
{
{ /* Llib/date.scm 1063 */
{ 
 obj_t BgL_iportz00_11458; long BgL_lastzd2matchzd2_11459; long BgL_forwardz00_11460; long BgL_bufposz00_11461; obj_t BgL_iportz00_11445; long BgL_lastzd2matchzd2_11446; long BgL_forwardz00_11447; long BgL_bufposz00_11448; obj_t BgL_iportz00_11434; long BgL_lastzd2matchzd2_11435; long BgL_forwardz00_11436; long BgL_bufposz00_11437; obj_t BgL_iportz00_11421; long BgL_lastzd2matchzd2_11422; long BgL_forwardz00_11423; long BgL_bufposz00_11424; obj_t BgL_iportz00_11407; long BgL_lastzd2matchzd2_11408; long BgL_forwardz00_11409; long BgL_bufposz00_11410; obj_t BgL_iportz00_11396; long BgL_lastzd2matchzd2_11397; long BgL_forwardz00_11398; long BgL_bufposz00_11399; obj_t BgL_iportz00_11385; long BgL_lastzd2matchzd2_11386; long BgL_forwardz00_11387; long BgL_bufposz00_11388; obj_t BgL_iportz00_11372; long BgL_lastzd2matchzd2_11373; long BgL_forwardz00_11374; long BgL_bufposz00_11375; obj_t BgL_iportz00_11361; long BgL_lastzd2matchzd2_11362; long BgL_forwardz00_11363; long BgL_bufposz00_11364; obj_t BgL_iportz00_11347; long BgL_lastzd2matchzd2_11348; long BgL_forwardz00_11349; long BgL_bufposz00_11350; obj_t BgL_iportz00_11336; long BgL_lastzd2matchzd2_11337; long BgL_forwardz00_11338; long BgL_bufposz00_11339; obj_t BgL_iportz00_11323; long BgL_lastzd2matchzd2_11324; long BgL_forwardz00_11325; long BgL_bufposz00_11326; obj_t BgL_iportz00_11309; long BgL_lastzd2matchzd2_11310; long BgL_forwardz00_11311; long BgL_bufposz00_11312; obj_t BgL_iportz00_11296; long BgL_lastzd2matchzd2_11297; long BgL_forwardz00_11298; long BgL_bufposz00_11299; obj_t BgL_iportz00_11285; long BgL_lastzd2matchzd2_11286; long BgL_forwardz00_11287; long BgL_bufposz00_11288; obj_t BgL_iportz00_11271; long BgL_lastzd2matchzd2_11272; long BgL_forwardz00_11273; long BgL_bufposz00_11274; obj_t BgL_iportz00_11258; long BgL_lastzd2matchzd2_11259; long BgL_forwardz00_11260; long BgL_bufposz00_11261;
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6449z00_12972;
{ /* Llib/date.scm 1063 */
 obj_t BgL_arg2068z00_11469;
{ /* Llib/date.scm 1063 */
 obj_t BgL_res4739z00_11470;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_res4739z00_11470 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_12975;
BgL_auxz00_12975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5827z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_12975,BFALSE,BFALSE);} 
BgL_arg2068z00_11469 = BgL_res4739z00_11470; } 
BgL_test6449z00_12972 = 
INPUT_PORT_CLOSEP(BgL_arg2068z00_11469); } 
if(BgL_test6449z00_12972)
{ /* Llib/date.scm 1063 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2063z00_11471;
{ /* Llib/date.scm 1063 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_11472;
{ /* Llib/date.scm 1063 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_11473;
BgL_new1077z00_11473 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 1063 */
 long BgL_arg2067z00_11474;
BgL_arg2067z00_11474 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_11473), BgL_arg2067z00_11474); } 
BgL_new1078z00_11472 = BgL_new1077z00_11473; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11472)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11472)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_12988;
{ /* Llib/date.scm 1063 */
 obj_t BgL_arg2064z00_11475;
{ /* Llib/date.scm 1063 */
 obj_t BgL_arg2065z00_11476;
{ /* Llib/date.scm 1063 */
 obj_t BgL_classz00_11477;
BgL_classz00_11477 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2065z00_11476 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_11477); } 
BgL_arg2064z00_11475 = 
VECTOR_REF(BgL_arg2065z00_11476,2L); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_auxz00_12992;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2064z00_11475))
{ /* Llib/date.scm 1063 */
BgL_auxz00_12992 = BgL_arg2064z00_11475
; }  else 
{ 
 obj_t BgL_auxz00_12995;
BgL_auxz00_12995 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5827z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg2064z00_11475); 
FAILURE(BgL_auxz00_12995,BFALSE,BFALSE);} 
BgL_auxz00_12988 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_12992); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11472)))->BgL_stackz00)=((obj_t)BgL_auxz00_12988),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11472)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11472)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_13005;
{ /* Llib/date.scm 1063 */
 obj_t BgL_res4740z00_11478;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_res4740z00_11478 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13009;
BgL_auxz00_13009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5827z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13009,BFALSE,BFALSE);} 
BgL_auxz00_13005 = BgL_res4740z00_11478; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11472)))->BgL_objz00)=((obj_t)BgL_auxz00_13005),BUNSPEC); } 
BgL_arg2063z00_11471 = BgL_new1078z00_11472; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2063z00_11471));}  else 
{ /* Llib/date.scm 1063 */
BgL_ignorez00_11157:
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11176;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11176 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13018;
BgL_auxz00_13018 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13018,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_11176); } 
{ /* Llib/date.scm 1063 */
 long BgL_matchz00_11177;
{ /* Llib/date.scm 1063 */
 long BgL_arg2349z00_11178; long BgL_arg2350z00_11179;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11180;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11180 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13025;
BgL_auxz00_13025 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13025,BFALSE,BFALSE);} 
BgL_arg2349z00_11178 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11180); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11181;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11181 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13032;
BgL_auxz00_13032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13032,BFALSE,BFALSE);} 
BgL_arg2350z00_11179 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11181); } 
BgL_iportz00_11309 = BgL_iportz00_9762; 
BgL_lastzd2matchzd2_11310 = 6L; 
BgL_forwardz00_11311 = BgL_arg2349z00_11178; 
BgL_bufposz00_11312 = BgL_arg2350z00_11179; 
BgL_statezd20zd21186z00_11163:
if(
(BgL_forwardz00_11311==BgL_bufposz00_11312))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6457z00_13039;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11313;
if(
INPUT_PORTP(BgL_iportz00_11309))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11313 = BgL_iportz00_11309; }  else 
{ 
 obj_t BgL_auxz00_13042;
BgL_auxz00_13042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5814z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11309); 
FAILURE(BgL_auxz00_13042,BFALSE,BFALSE);} 
BgL_test6457z00_13039 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11313); } 
if(BgL_test6457z00_13039)
{ /* Llib/date.scm 1063 */
 long BgL_arg2177z00_11314; long BgL_arg2178z00_11315;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11316;
if(
INPUT_PORTP(BgL_iportz00_11309))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11316 = BgL_iportz00_11309; }  else 
{ 
 obj_t BgL_auxz00_13049;
BgL_auxz00_13049 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5814z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11309); 
FAILURE(BgL_auxz00_13049,BFALSE,BFALSE);} 
BgL_arg2177z00_11314 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11316); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11317;
if(
INPUT_PORTP(BgL_iportz00_11309))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11317 = BgL_iportz00_11309; }  else 
{ 
 obj_t BgL_auxz00_13056;
BgL_auxz00_13056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5814z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11309); 
FAILURE(BgL_auxz00_13056,BFALSE,BFALSE);} 
BgL_arg2178z00_11315 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11317); } 
{ 
 long BgL_bufposz00_13062; long BgL_forwardz00_13061;
BgL_forwardz00_13061 = BgL_arg2177z00_11314; 
BgL_bufposz00_13062 = BgL_arg2178z00_11315; 
BgL_bufposz00_11312 = BgL_bufposz00_13062; 
BgL_forwardz00_11311 = BgL_forwardz00_13061; 
goto BgL_statezd20zd21186z00_11163;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11310; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11318;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11319;
if(
INPUT_PORTP(BgL_iportz00_11309))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11319 = BgL_iportz00_11309; }  else 
{ 
 obj_t BgL_auxz00_13065;
BgL_auxz00_13065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5814z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11309); 
FAILURE(BgL_auxz00_13065,BFALSE,BFALSE);} 
BgL_curz00_11318 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11319, BgL_forwardz00_11311); } 
{ /* Llib/date.scm 1063 */

if(
(
(long)(BgL_curz00_11318)==58L))
{ /* Llib/date.scm 1063 */
BgL_iportz00_11421 = BgL_iportz00_11309; 
BgL_lastzd2matchzd2_11422 = BgL_lastzd2matchzd2_11310; 
BgL_forwardz00_11423 = 
(1L+BgL_forwardz00_11311); 
BgL_bufposz00_11424 = BgL_bufposz00_11312; 
BgL_statezd24zd21190z00_11172:
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11425;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11426;
if(
INPUT_PORTP(BgL_iportz00_11421))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11426 = BgL_iportz00_11421; }  else 
{ 
 obj_t BgL_auxz00_13075;
BgL_auxz00_13075 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5823z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11421); 
FAILURE(BgL_auxz00_13075,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11426, BgL_forwardz00_11423); } 
BgL_newzd2matchzd2_11425 = 6L; 
if(
(BgL_forwardz00_11423==BgL_bufposz00_11424))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6465z00_13082;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11427;
if(
INPUT_PORTP(BgL_iportz00_11421))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11427 = BgL_iportz00_11421; }  else 
{ 
 obj_t BgL_auxz00_13085;
BgL_auxz00_13085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5823z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11421); 
FAILURE(BgL_auxz00_13085,BFALSE,BFALSE);} 
BgL_test6465z00_13082 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11427); } 
if(BgL_test6465z00_13082)
{ /* Llib/date.scm 1063 */
 long BgL_arg2101z00_11428; long BgL_arg2102z00_11429;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11430;
if(
INPUT_PORTP(BgL_iportz00_11421))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11430 = BgL_iportz00_11421; }  else 
{ 
 obj_t BgL_auxz00_13092;
BgL_auxz00_13092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5823z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11421); 
FAILURE(BgL_auxz00_13092,BFALSE,BFALSE);} 
BgL_arg2101z00_11428 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11430); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11431;
if(
INPUT_PORTP(BgL_iportz00_11421))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11431 = BgL_iportz00_11421; }  else 
{ 
 obj_t BgL_auxz00_13099;
BgL_auxz00_13099 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5823z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11421); 
FAILURE(BgL_auxz00_13099,BFALSE,BFALSE);} 
BgL_arg2102z00_11429 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11431); } 
{ 
 long BgL_bufposz00_13105; long BgL_forwardz00_13104;
BgL_forwardz00_13104 = BgL_arg2101z00_11428; 
BgL_bufposz00_13105 = BgL_arg2102z00_11429; 
BgL_bufposz00_11424 = BgL_bufposz00_13105; 
BgL_forwardz00_11423 = BgL_forwardz00_13104; 
goto BgL_statezd24zd21190z00_11172;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11425; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11432;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11433;
if(
INPUT_PORTP(BgL_iportz00_11421))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11433 = BgL_iportz00_11421; }  else 
{ 
 obj_t BgL_auxz00_13108;
BgL_auxz00_13108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5823z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11421); 
FAILURE(BgL_auxz00_13108,BFALSE,BFALSE);} 
BgL_curz00_11432 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11433, BgL_forwardz00_11423); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6470z00_13113;
if(
(
(long)(BgL_curz00_11432)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6470z00_13113 = 
(
(long)(BgL_curz00_11432)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6470z00_13113 = ((bool_t)0)
; } 
if(BgL_test6470z00_13113)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11407 = BgL_iportz00_11421; 
BgL_lastzd2matchzd2_11408 = BgL_newzd2matchzd2_11425; 
BgL_forwardz00_11409 = 
(1L+BgL_forwardz00_11423); 
BgL_bufposz00_11410 = BgL_bufposz00_11424; 
BgL_statezd25zd21191z00_11171:
if(
(BgL_forwardz00_11409==BgL_bufposz00_11410))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6473z00_13121;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11411;
if(
INPUT_PORTP(BgL_iportz00_11407))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11411 = BgL_iportz00_11407; }  else 
{ 
 obj_t BgL_auxz00_13124;
BgL_auxz00_13124 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5822z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11407); 
FAILURE(BgL_auxz00_13124,BFALSE,BFALSE);} 
BgL_test6473z00_13121 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11411); } 
if(BgL_test6473z00_13121)
{ /* Llib/date.scm 1063 */
 long BgL_arg2109z00_11412; long BgL_arg2110z00_11413;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11414;
if(
INPUT_PORTP(BgL_iportz00_11407))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11414 = BgL_iportz00_11407; }  else 
{ 
 obj_t BgL_auxz00_13131;
BgL_auxz00_13131 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5822z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11407); 
FAILURE(BgL_auxz00_13131,BFALSE,BFALSE);} 
BgL_arg2109z00_11412 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11414); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11415;
if(
INPUT_PORTP(BgL_iportz00_11407))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11415 = BgL_iportz00_11407; }  else 
{ 
 obj_t BgL_auxz00_13138;
BgL_auxz00_13138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5822z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11407); 
FAILURE(BgL_auxz00_13138,BFALSE,BFALSE);} 
BgL_arg2110z00_11413 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11415); } 
{ 
 long BgL_bufposz00_13144; long BgL_forwardz00_13143;
BgL_forwardz00_13143 = BgL_arg2109z00_11412; 
BgL_bufposz00_13144 = BgL_arg2110z00_11413; 
BgL_bufposz00_11410 = BgL_bufposz00_13144; 
BgL_forwardz00_11409 = BgL_forwardz00_13143; 
goto BgL_statezd25zd21191z00_11171;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11408; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11416;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11417;
if(
INPUT_PORTP(BgL_iportz00_11407))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11417 = BgL_iportz00_11407; }  else 
{ 
 obj_t BgL_auxz00_13147;
BgL_auxz00_13147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5822z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11407); 
FAILURE(BgL_auxz00_13147,BFALSE,BFALSE);} 
BgL_curz00_11416 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11417, BgL_forwardz00_11409); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6478z00_13152;
if(
(
(long)(BgL_curz00_11416)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6478z00_13152 = 
(
(long)(BgL_curz00_11416)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6478z00_13152 = ((bool_t)0)
; } 
if(BgL_test6478z00_13152)
{ /* Llib/date.scm 1063 */
 long BgL_arg2113z00_11418;
BgL_arg2113z00_11418 = 
(1L+BgL_forwardz00_11409); 
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11419;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11420;
if(
INPUT_PORTP(BgL_iportz00_11407))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11420 = BgL_iportz00_11407; }  else 
{ 
 obj_t BgL_auxz00_13161;
BgL_auxz00_13161 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5822z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11407); 
FAILURE(BgL_auxz00_13161,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11420, BgL_arg2113z00_11418); } 
BgL_newzd2matchzd2_11419 = 5L; 
BgL_matchz00_11177 = BgL_newzd2matchzd2_11419; } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11408; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11425; } } } } } }  else 
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6481z00_13168;
if(
(
(long)(BgL_curz00_11318)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6481z00_13168 = 
(
(long)(BgL_curz00_11318)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6481z00_13168 = ((bool_t)0)
; } 
if(BgL_test6481z00_13168)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11323 = BgL_iportz00_11309; 
BgL_lastzd2matchzd2_11324 = BgL_lastzd2matchzd2_11310; 
BgL_forwardz00_11325 = 
(1L+BgL_forwardz00_11311); 
BgL_bufposz00_11326 = BgL_bufposz00_11312; 
BgL_statezd23zd21189z00_11164:
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11327;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11328;
if(
INPUT_PORTP(BgL_iportz00_11323))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11328 = BgL_iportz00_11323; }  else 
{ 
 obj_t BgL_auxz00_13176;
BgL_auxz00_13176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5815z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11323); 
FAILURE(BgL_auxz00_13176,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11328, BgL_forwardz00_11325); } 
BgL_newzd2matchzd2_11327 = 6L; 
if(
(BgL_forwardz00_11325==BgL_bufposz00_11326))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6485z00_13183;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11329;
if(
INPUT_PORTP(BgL_iportz00_11323))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11329 = BgL_iportz00_11323; }  else 
{ 
 obj_t BgL_auxz00_13186;
BgL_auxz00_13186 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5815z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11323); 
FAILURE(BgL_auxz00_13186,BFALSE,BFALSE);} 
BgL_test6485z00_13183 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11329); } 
if(BgL_test6485z00_13183)
{ /* Llib/date.scm 1063 */
 long BgL_arg2167z00_11330; long BgL_arg2168z00_11331;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11332;
if(
INPUT_PORTP(BgL_iportz00_11323))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11332 = BgL_iportz00_11323; }  else 
{ 
 obj_t BgL_auxz00_13193;
BgL_auxz00_13193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5815z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11323); 
FAILURE(BgL_auxz00_13193,BFALSE,BFALSE);} 
BgL_arg2167z00_11330 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11332); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11333;
if(
INPUT_PORTP(BgL_iportz00_11323))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11333 = BgL_iportz00_11323; }  else 
{ 
 obj_t BgL_auxz00_13200;
BgL_auxz00_13200 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5815z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11323); 
FAILURE(BgL_auxz00_13200,BFALSE,BFALSE);} 
BgL_arg2168z00_11331 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11333); } 
{ 
 long BgL_bufposz00_13206; long BgL_forwardz00_13205;
BgL_forwardz00_13205 = BgL_arg2167z00_11330; 
BgL_bufposz00_13206 = BgL_arg2168z00_11331; 
BgL_bufposz00_11326 = BgL_bufposz00_13206; 
BgL_forwardz00_11325 = BgL_forwardz00_13205; 
goto BgL_statezd23zd21189z00_11164;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11327; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11334;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11335;
if(
INPUT_PORTP(BgL_iportz00_11323))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11335 = BgL_iportz00_11323; }  else 
{ 
 obj_t BgL_auxz00_13209;
BgL_auxz00_13209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5815z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11323); 
FAILURE(BgL_auxz00_13209,BFALSE,BFALSE);} 
BgL_curz00_11334 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11335, BgL_forwardz00_11325); } 
{ /* Llib/date.scm 1063 */

if(
(
(long)(BgL_curz00_11334)==58L))
{ /* Llib/date.scm 1063 */
BgL_iportz00_11434 = BgL_iportz00_11323; 
BgL_lastzd2matchzd2_11435 = BgL_newzd2matchzd2_11327; 
BgL_forwardz00_11436 = 
(1L+BgL_forwardz00_11325); 
BgL_bufposz00_11437 = BgL_bufposz00_11326; 
BgL_statezd29zd21195z00_11173:
if(
(BgL_forwardz00_11436==BgL_bufposz00_11437))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6492z00_13219;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11438;
if(
INPUT_PORTP(BgL_iportz00_11434))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11438 = BgL_iportz00_11434; }  else 
{ 
 obj_t BgL_auxz00_13222;
BgL_auxz00_13222 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5824z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11434); 
FAILURE(BgL_auxz00_13222,BFALSE,BFALSE);} 
BgL_test6492z00_13219 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11438); } 
if(BgL_test6492z00_13219)
{ /* Llib/date.scm 1063 */
 long BgL_arg2091z00_11439; long BgL_arg2093z00_11440;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11441;
if(
INPUT_PORTP(BgL_iportz00_11434))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11441 = BgL_iportz00_11434; }  else 
{ 
 obj_t BgL_auxz00_13229;
BgL_auxz00_13229 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5824z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11434); 
FAILURE(BgL_auxz00_13229,BFALSE,BFALSE);} 
BgL_arg2091z00_11439 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11441); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11442;
if(
INPUT_PORTP(BgL_iportz00_11434))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11442 = BgL_iportz00_11434; }  else 
{ 
 obj_t BgL_auxz00_13236;
BgL_auxz00_13236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5824z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11434); 
FAILURE(BgL_auxz00_13236,BFALSE,BFALSE);} 
BgL_arg2093z00_11440 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11442); } 
{ 
 long BgL_bufposz00_13242; long BgL_forwardz00_13241;
BgL_forwardz00_13241 = BgL_arg2091z00_11439; 
BgL_bufposz00_13242 = BgL_arg2093z00_11440; 
BgL_bufposz00_11437 = BgL_bufposz00_13242; 
BgL_forwardz00_11436 = BgL_forwardz00_13241; 
goto BgL_statezd29zd21195z00_11173;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11435; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11443;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11444;
if(
INPUT_PORTP(BgL_iportz00_11434))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11444 = BgL_iportz00_11434; }  else 
{ 
 obj_t BgL_auxz00_13245;
BgL_auxz00_13245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5824z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11434); 
FAILURE(BgL_auxz00_13245,BFALSE,BFALSE);} 
BgL_curz00_11443 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11444, BgL_forwardz00_11436); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6497z00_13250;
if(
(
(long)(BgL_curz00_11443)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6497z00_13250 = 
(
(long)(BgL_curz00_11443)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6497z00_13250 = ((bool_t)0)
; } 
if(BgL_test6497z00_13250)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11336 = BgL_iportz00_11434; 
BgL_lastzd2matchzd2_11337 = BgL_lastzd2matchzd2_11435; 
BgL_forwardz00_11338 = 
(1L+BgL_forwardz00_11436); 
BgL_bufposz00_11339 = BgL_bufposz00_11437; 
BgL_statezd210zd21196z00_11165:
if(
(BgL_forwardz00_11338==BgL_bufposz00_11339))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6500z00_13258;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11340;
if(
INPUT_PORTP(BgL_iportz00_11336))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11340 = BgL_iportz00_11336; }  else 
{ 
 obj_t BgL_auxz00_13261;
BgL_auxz00_13261 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5816z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11336); 
FAILURE(BgL_auxz00_13261,BFALSE,BFALSE);} 
BgL_test6500z00_13258 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11340); } 
if(BgL_test6500z00_13258)
{ /* Llib/date.scm 1063 */
 long BgL_arg2159z00_11341; long BgL_arg2160z00_11342;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11343;
if(
INPUT_PORTP(BgL_iportz00_11336))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11343 = BgL_iportz00_11336; }  else 
{ 
 obj_t BgL_auxz00_13268;
BgL_auxz00_13268 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5816z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11336); 
FAILURE(BgL_auxz00_13268,BFALSE,BFALSE);} 
BgL_arg2159z00_11341 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11343); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11344;
if(
INPUT_PORTP(BgL_iportz00_11336))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11344 = BgL_iportz00_11336; }  else 
{ 
 obj_t BgL_auxz00_13275;
BgL_auxz00_13275 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5816z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11336); 
FAILURE(BgL_auxz00_13275,BFALSE,BFALSE);} 
BgL_arg2160z00_11342 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11344); } 
{ 
 long BgL_bufposz00_13281; long BgL_forwardz00_13280;
BgL_forwardz00_13280 = BgL_arg2159z00_11341; 
BgL_bufposz00_13281 = BgL_arg2160z00_11342; 
BgL_bufposz00_11339 = BgL_bufposz00_13281; 
BgL_forwardz00_11338 = BgL_forwardz00_13280; 
goto BgL_statezd210zd21196z00_11165;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11337; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11345;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11346;
if(
INPUT_PORTP(BgL_iportz00_11336))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11346 = BgL_iportz00_11336; }  else 
{ 
 obj_t BgL_auxz00_13284;
BgL_auxz00_13284 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5816z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11336); 
FAILURE(BgL_auxz00_13284,BFALSE,BFALSE);} 
BgL_curz00_11345 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11346, BgL_forwardz00_11338); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6505z00_13289;
if(
(
(long)(BgL_curz00_11345)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6505z00_13289 = 
(
(long)(BgL_curz00_11345)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6505z00_13289 = ((bool_t)0)
; } 
if(BgL_test6505z00_13289)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11372 = BgL_iportz00_11336; 
BgL_lastzd2matchzd2_11373 = BgL_lastzd2matchzd2_11337; 
BgL_forwardz00_11374 = 
(1L+BgL_forwardz00_11338); 
BgL_bufposz00_11375 = BgL_bufposz00_11339; 
BgL_statezd211zd21197z00_11168:
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11376;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11377;
if(
INPUT_PORTP(BgL_iportz00_11372))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11377 = BgL_iportz00_11372; }  else 
{ 
 obj_t BgL_auxz00_13297;
BgL_auxz00_13297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5819z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11372); 
FAILURE(BgL_auxz00_13297,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11377, BgL_forwardz00_11374); } 
BgL_newzd2matchzd2_11376 = 1L; 
if(
(BgL_forwardz00_11374==BgL_bufposz00_11375))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6509z00_13304;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11378;
if(
INPUT_PORTP(BgL_iportz00_11372))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11378 = BgL_iportz00_11372; }  else 
{ 
 obj_t BgL_auxz00_13307;
BgL_auxz00_13307 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5819z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11372); 
FAILURE(BgL_auxz00_13307,BFALSE,BFALSE);} 
BgL_test6509z00_13304 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11378); } 
if(BgL_test6509z00_13304)
{ /* Llib/date.scm 1063 */
 long BgL_arg2133z00_11379; long BgL_arg2134z00_11380;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11381;
if(
INPUT_PORTP(BgL_iportz00_11372))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11381 = BgL_iportz00_11372; }  else 
{ 
 obj_t BgL_auxz00_13314;
BgL_auxz00_13314 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5819z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11372); 
FAILURE(BgL_auxz00_13314,BFALSE,BFALSE);} 
BgL_arg2133z00_11379 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11381); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11382;
if(
INPUT_PORTP(BgL_iportz00_11372))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11382 = BgL_iportz00_11372; }  else 
{ 
 obj_t BgL_auxz00_13321;
BgL_auxz00_13321 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5819z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11372); 
FAILURE(BgL_auxz00_13321,BFALSE,BFALSE);} 
BgL_arg2134z00_11380 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11382); } 
{ 
 long BgL_bufposz00_13327; long BgL_forwardz00_13326;
BgL_forwardz00_13326 = BgL_arg2133z00_11379; 
BgL_bufposz00_13327 = BgL_arg2134z00_11380; 
BgL_bufposz00_11375 = BgL_bufposz00_13327; 
BgL_forwardz00_11374 = BgL_forwardz00_13326; 
goto BgL_statezd211zd21197z00_11168;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11376; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11383;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11384;
if(
INPUT_PORTP(BgL_iportz00_11372))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11384 = BgL_iportz00_11372; }  else 
{ 
 obj_t BgL_auxz00_13330;
BgL_auxz00_13330 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5819z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11372); 
FAILURE(BgL_auxz00_13330,BFALSE,BFALSE);} 
BgL_curz00_11383 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11384, BgL_forwardz00_11374); } 
{ /* Llib/date.scm 1063 */

if(
(
(long)(BgL_curz00_11383)==58L))
{ /* Llib/date.scm 1063 */
BgL_iportz00_11361 = BgL_iportz00_11372; 
BgL_lastzd2matchzd2_11362 = BgL_newzd2matchzd2_11376; 
BgL_forwardz00_11363 = 
(1L+BgL_forwardz00_11374); 
BgL_bufposz00_11364 = BgL_bufposz00_11375; 
BgL_statezd212zd21198z00_11167:
if(
(BgL_forwardz00_11363==BgL_bufposz00_11364))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6516z00_13340;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11365;
if(
INPUT_PORTP(BgL_iportz00_11361))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11365 = BgL_iportz00_11361; }  else 
{ 
 obj_t BgL_auxz00_13343;
BgL_auxz00_13343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5818z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11361); 
FAILURE(BgL_auxz00_13343,BFALSE,BFALSE);} 
BgL_test6516z00_13340 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11365); } 
if(BgL_test6516z00_13340)
{ /* Llib/date.scm 1063 */
 long BgL_arg2141z00_11366; long BgL_arg2142z00_11367;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11368;
if(
INPUT_PORTP(BgL_iportz00_11361))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11368 = BgL_iportz00_11361; }  else 
{ 
 obj_t BgL_auxz00_13350;
BgL_auxz00_13350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5818z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11361); 
FAILURE(BgL_auxz00_13350,BFALSE,BFALSE);} 
BgL_arg2141z00_11366 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11368); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11369;
if(
INPUT_PORTP(BgL_iportz00_11361))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11369 = BgL_iportz00_11361; }  else 
{ 
 obj_t BgL_auxz00_13357;
BgL_auxz00_13357 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5818z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11361); 
FAILURE(BgL_auxz00_13357,BFALSE,BFALSE);} 
BgL_arg2142z00_11367 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11369); } 
{ 
 long BgL_bufposz00_13363; long BgL_forwardz00_13362;
BgL_forwardz00_13362 = BgL_arg2141z00_11366; 
BgL_bufposz00_13363 = BgL_arg2142z00_11367; 
BgL_bufposz00_11364 = BgL_bufposz00_13363; 
BgL_forwardz00_11363 = BgL_forwardz00_13362; 
goto BgL_statezd212zd21198z00_11167;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11362; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11370;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11371;
if(
INPUT_PORTP(BgL_iportz00_11361))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11371 = BgL_iportz00_11361; }  else 
{ 
 obj_t BgL_auxz00_13366;
BgL_auxz00_13366 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5818z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11361); 
FAILURE(BgL_auxz00_13366,BFALSE,BFALSE);} 
BgL_curz00_11370 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11371, BgL_forwardz00_11363); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6521z00_13371;
if(
(
(long)(BgL_curz00_11370)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6521z00_13371 = 
(
(long)(BgL_curz00_11370)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6521z00_13371 = ((bool_t)0)
; } 
if(BgL_test6521z00_13371)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11347 = BgL_iportz00_11361; 
BgL_lastzd2matchzd2_11348 = BgL_lastzd2matchzd2_11362; 
BgL_forwardz00_11349 = 
(1L+BgL_forwardz00_11363); 
BgL_bufposz00_11350 = BgL_bufposz00_11364; 
BgL_statezd213zd21199z00_11166:
if(
(BgL_forwardz00_11349==BgL_bufposz00_11350))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6524z00_13379;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11351;
if(
INPUT_PORTP(BgL_iportz00_11347))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11351 = BgL_iportz00_11347; }  else 
{ 
 obj_t BgL_auxz00_13382;
BgL_auxz00_13382 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5817z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11347); 
FAILURE(BgL_auxz00_13382,BFALSE,BFALSE);} 
BgL_test6524z00_13379 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11351); } 
if(BgL_test6524z00_13379)
{ /* Llib/date.scm 1063 */
 long BgL_arg2149z00_11352; long BgL_arg2150z00_11353;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11354;
if(
INPUT_PORTP(BgL_iportz00_11347))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11354 = BgL_iportz00_11347; }  else 
{ 
 obj_t BgL_auxz00_13389;
BgL_auxz00_13389 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5817z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11347); 
FAILURE(BgL_auxz00_13389,BFALSE,BFALSE);} 
BgL_arg2149z00_11352 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11354); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11355;
if(
INPUT_PORTP(BgL_iportz00_11347))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11355 = BgL_iportz00_11347; }  else 
{ 
 obj_t BgL_auxz00_13396;
BgL_auxz00_13396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5817z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11347); 
FAILURE(BgL_auxz00_13396,BFALSE,BFALSE);} 
BgL_arg2150z00_11353 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11355); } 
{ 
 long BgL_bufposz00_13402; long BgL_forwardz00_13401;
BgL_forwardz00_13401 = BgL_arg2149z00_11352; 
BgL_bufposz00_13402 = BgL_arg2150z00_11353; 
BgL_bufposz00_11350 = BgL_bufposz00_13402; 
BgL_forwardz00_11349 = BgL_forwardz00_13401; 
goto BgL_statezd213zd21199z00_11166;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11348; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11356;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11357;
if(
INPUT_PORTP(BgL_iportz00_11347))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11357 = BgL_iportz00_11347; }  else 
{ 
 obj_t BgL_auxz00_13405;
BgL_auxz00_13405 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5817z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11347); 
FAILURE(BgL_auxz00_13405,BFALSE,BFALSE);} 
BgL_curz00_11356 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11357, BgL_forwardz00_11349); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6529z00_13410;
if(
(
(long)(BgL_curz00_11356)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6529z00_13410 = 
(
(long)(BgL_curz00_11356)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6529z00_13410 = ((bool_t)0)
; } 
if(BgL_test6529z00_13410)
{ /* Llib/date.scm 1063 */
 long BgL_arg2154z00_11358;
BgL_arg2154z00_11358 = 
(1L+BgL_forwardz00_11349); 
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11359;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11360;
if(
INPUT_PORTP(BgL_iportz00_11347))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11360 = BgL_iportz00_11347; }  else 
{ 
 obj_t BgL_auxz00_13419;
BgL_auxz00_13419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5817z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11347); 
FAILURE(BgL_auxz00_13419,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11360, BgL_arg2154z00_11358); } 
BgL_newzd2matchzd2_11359 = 4L; 
BgL_matchz00_11177 = BgL_newzd2matchzd2_11359; } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11348; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11362; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11376; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11337; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11435; } } } } }  else 
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6532z00_13429;
if(
(
(long)(BgL_curz00_11334)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6532z00_13429 = 
(
(long)(BgL_curz00_11334)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6532z00_13429 = ((bool_t)0)
; } 
if(BgL_test6532z00_13429)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11385 = BgL_iportz00_11323; 
BgL_lastzd2matchzd2_11386 = BgL_newzd2matchzd2_11327; 
BgL_forwardz00_11387 = 
(1L+BgL_forwardz00_11325); 
BgL_bufposz00_11388 = BgL_bufposz00_11326; 
BgL_statezd28zd21194z00_11169:
if(
(BgL_forwardz00_11387==BgL_bufposz00_11388))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6535z00_13437;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11389;
if(
INPUT_PORTP(BgL_iportz00_11385))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11389 = BgL_iportz00_11385; }  else 
{ 
 obj_t BgL_auxz00_13440;
BgL_auxz00_13440 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5820z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11385); 
FAILURE(BgL_auxz00_13440,BFALSE,BFALSE);} 
BgL_test6535z00_13437 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11389); } 
if(BgL_test6535z00_13437)
{ /* Llib/date.scm 1063 */
 long BgL_arg2126z00_11390; long BgL_arg2127z00_11391;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11392;
if(
INPUT_PORTP(BgL_iportz00_11385))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11392 = BgL_iportz00_11385; }  else 
{ 
 obj_t BgL_auxz00_13447;
BgL_auxz00_13447 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5820z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11385); 
FAILURE(BgL_auxz00_13447,BFALSE,BFALSE);} 
BgL_arg2126z00_11390 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11392); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11393;
if(
INPUT_PORTP(BgL_iportz00_11385))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11393 = BgL_iportz00_11385; }  else 
{ 
 obj_t BgL_auxz00_13454;
BgL_auxz00_13454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5820z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11385); 
FAILURE(BgL_auxz00_13454,BFALSE,BFALSE);} 
BgL_arg2127z00_11391 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11393); } 
{ 
 long BgL_bufposz00_13460; long BgL_forwardz00_13459;
BgL_forwardz00_13459 = BgL_arg2126z00_11390; 
BgL_bufposz00_13460 = BgL_arg2127z00_11391; 
BgL_bufposz00_11388 = BgL_bufposz00_13460; 
BgL_forwardz00_11387 = BgL_forwardz00_13459; 
goto BgL_statezd28zd21194z00_11169;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11386; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11394;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11395;
if(
INPUT_PORTP(BgL_iportz00_11385))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11395 = BgL_iportz00_11385; }  else 
{ 
 obj_t BgL_auxz00_13463;
BgL_auxz00_13463 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5820z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11385); 
FAILURE(BgL_auxz00_13463,BFALSE,BFALSE);} 
BgL_curz00_11394 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11395, BgL_forwardz00_11387); } 
{ /* Llib/date.scm 1063 */

if(
(
(long)(BgL_curz00_11394)==58L))
{ /* Llib/date.scm 1063 */
BgL_iportz00_11396 = BgL_iportz00_11385; 
BgL_lastzd2matchzd2_11397 = BgL_lastzd2matchzd2_11386; 
BgL_forwardz00_11398 = 
(1L+BgL_forwardz00_11387); 
BgL_bufposz00_11399 = BgL_bufposz00_11388; 
BgL_statezd215zd21201z00_11170:
if(
(BgL_forwardz00_11398==BgL_bufposz00_11399))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6542z00_13473;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11400;
if(
INPUT_PORTP(BgL_iportz00_11396))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11400 = BgL_iportz00_11396; }  else 
{ 
 obj_t BgL_auxz00_13476;
BgL_auxz00_13476 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5821z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11396); 
FAILURE(BgL_auxz00_13476,BFALSE,BFALSE);} 
BgL_test6542z00_13473 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11400); } 
if(BgL_test6542z00_13473)
{ /* Llib/date.scm 1063 */
 long BgL_arg2118z00_11401; long BgL_arg2119z00_11402;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11403;
if(
INPUT_PORTP(BgL_iportz00_11396))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11403 = BgL_iportz00_11396; }  else 
{ 
 obj_t BgL_auxz00_13483;
BgL_auxz00_13483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5821z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11396); 
FAILURE(BgL_auxz00_13483,BFALSE,BFALSE);} 
BgL_arg2118z00_11401 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11403); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11404;
if(
INPUT_PORTP(BgL_iportz00_11396))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11404 = BgL_iportz00_11396; }  else 
{ 
 obj_t BgL_auxz00_13490;
BgL_auxz00_13490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5821z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11396); 
FAILURE(BgL_auxz00_13490,BFALSE,BFALSE);} 
BgL_arg2119z00_11402 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11404); } 
{ 
 long BgL_bufposz00_13496; long BgL_forwardz00_13495;
BgL_forwardz00_13495 = BgL_arg2118z00_11401; 
BgL_bufposz00_13496 = BgL_arg2119z00_11402; 
BgL_bufposz00_11399 = BgL_bufposz00_13496; 
BgL_forwardz00_11398 = BgL_forwardz00_13495; 
goto BgL_statezd215zd21201z00_11170;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11397; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11405;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11406;
if(
INPUT_PORTP(BgL_iportz00_11396))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11406 = BgL_iportz00_11396; }  else 
{ 
 obj_t BgL_auxz00_13499;
BgL_auxz00_13499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5821z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11396); 
FAILURE(BgL_auxz00_13499,BFALSE,BFALSE);} 
BgL_curz00_11405 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11406, BgL_forwardz00_11398); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6547z00_13504;
if(
(
(long)(BgL_curz00_11405)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6547z00_13504 = 
(
(long)(BgL_curz00_11405)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6547z00_13504 = ((bool_t)0)
; } 
if(BgL_test6547z00_13504)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11458 = BgL_iportz00_11396; 
BgL_lastzd2matchzd2_11459 = BgL_lastzd2matchzd2_11397; 
BgL_forwardz00_11460 = 
(1L+BgL_forwardz00_11398); 
BgL_bufposz00_11461 = BgL_bufposz00_11399; 
BgL_statezd216zd21202z00_11175:
if(
(BgL_forwardz00_11460==BgL_bufposz00_11461))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6550z00_13512;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11462;
if(
INPUT_PORTP(BgL_iportz00_11458))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11462 = BgL_iportz00_11458; }  else 
{ 
 obj_t BgL_auxz00_13515;
BgL_auxz00_13515 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5826z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11458); 
FAILURE(BgL_auxz00_13515,BFALSE,BFALSE);} 
BgL_test6550z00_13512 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11462); } 
if(BgL_test6550z00_13512)
{ /* Llib/date.scm 1063 */
 long BgL_arg2072z00_11463; long BgL_arg2074z00_11464;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11465;
if(
INPUT_PORTP(BgL_iportz00_11458))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11465 = BgL_iportz00_11458; }  else 
{ 
 obj_t BgL_auxz00_13522;
BgL_auxz00_13522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5826z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11458); 
FAILURE(BgL_auxz00_13522,BFALSE,BFALSE);} 
BgL_arg2072z00_11463 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11465); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11466;
if(
INPUT_PORTP(BgL_iportz00_11458))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11466 = BgL_iportz00_11458; }  else 
{ 
 obj_t BgL_auxz00_13529;
BgL_auxz00_13529 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5826z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11458); 
FAILURE(BgL_auxz00_13529,BFALSE,BFALSE);} 
BgL_arg2074z00_11464 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11466); } 
{ 
 long BgL_bufposz00_13535; long BgL_forwardz00_13534;
BgL_forwardz00_13534 = BgL_arg2072z00_11463; 
BgL_bufposz00_13535 = BgL_arg2074z00_11464; 
BgL_bufposz00_11461 = BgL_bufposz00_13535; 
BgL_forwardz00_11460 = BgL_forwardz00_13534; 
goto BgL_statezd216zd21202z00_11175;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11459; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11467;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11468;
if(
INPUT_PORTP(BgL_iportz00_11458))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11468 = BgL_iportz00_11458; }  else 
{ 
 obj_t BgL_auxz00_13538;
BgL_auxz00_13538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5826z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11458); 
FAILURE(BgL_auxz00_13538,BFALSE,BFALSE);} 
BgL_curz00_11467 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11468, BgL_forwardz00_11460); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6555z00_13543;
if(
(
(long)(BgL_curz00_11467)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6555z00_13543 = 
(
(long)(BgL_curz00_11467)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6555z00_13543 = ((bool_t)0)
; } 
if(BgL_test6555z00_13543)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11296 = BgL_iportz00_11458; 
BgL_lastzd2matchzd2_11297 = BgL_lastzd2matchzd2_11459; 
BgL_forwardz00_11298 = 
(1L+BgL_forwardz00_11460); 
BgL_bufposz00_11299 = BgL_bufposz00_11461; 
BgL_statezd217zd21203z00_11162:
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11300;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11301;
if(
INPUT_PORTP(BgL_iportz00_11296))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11301 = BgL_iportz00_11296; }  else 
{ 
 obj_t BgL_auxz00_13551;
BgL_auxz00_13551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5813z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11296); 
FAILURE(BgL_auxz00_13551,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11301, BgL_forwardz00_11298); } 
BgL_newzd2matchzd2_11300 = 2L; 
if(
(BgL_forwardz00_11298==BgL_bufposz00_11299))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6559z00_13558;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11302;
if(
INPUT_PORTP(BgL_iportz00_11296))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11302 = BgL_iportz00_11296; }  else 
{ 
 obj_t BgL_auxz00_13561;
BgL_auxz00_13561 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5813z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11296); 
FAILURE(BgL_auxz00_13561,BFALSE,BFALSE);} 
BgL_test6559z00_13558 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11302); } 
if(BgL_test6559z00_13558)
{ /* Llib/date.scm 1063 */
 long BgL_arg2193z00_11303; long BgL_arg2194z00_11304;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11305;
if(
INPUT_PORTP(BgL_iportz00_11296))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11305 = BgL_iportz00_11296; }  else 
{ 
 obj_t BgL_auxz00_13568;
BgL_auxz00_13568 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5813z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11296); 
FAILURE(BgL_auxz00_13568,BFALSE,BFALSE);} 
BgL_arg2193z00_11303 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11305); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11306;
if(
INPUT_PORTP(BgL_iportz00_11296))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11306 = BgL_iportz00_11296; }  else 
{ 
 obj_t BgL_auxz00_13575;
BgL_auxz00_13575 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5813z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11296); 
FAILURE(BgL_auxz00_13575,BFALSE,BFALSE);} 
BgL_arg2194z00_11304 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11306); } 
{ 
 long BgL_bufposz00_13581; long BgL_forwardz00_13580;
BgL_forwardz00_13580 = BgL_arg2193z00_11303; 
BgL_bufposz00_13581 = BgL_arg2194z00_11304; 
BgL_bufposz00_11299 = BgL_bufposz00_13581; 
BgL_forwardz00_11298 = BgL_forwardz00_13580; 
goto BgL_statezd217zd21203z00_11162;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11300; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11307;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11308;
if(
INPUT_PORTP(BgL_iportz00_11296))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11308 = BgL_iportz00_11296; }  else 
{ 
 obj_t BgL_auxz00_13584;
BgL_auxz00_13584 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5813z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11296); 
FAILURE(BgL_auxz00_13584,BFALSE,BFALSE);} 
BgL_curz00_11307 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11308, BgL_forwardz00_11298); } 
{ /* Llib/date.scm 1063 */

if(
(
(long)(BgL_curz00_11307)==58L))
{ /* Llib/date.scm 1063 */
BgL_iportz00_11285 = BgL_iportz00_11296; 
BgL_lastzd2matchzd2_11286 = BgL_newzd2matchzd2_11300; 
BgL_forwardz00_11287 = 
(1L+BgL_forwardz00_11298); 
BgL_bufposz00_11288 = BgL_bufposz00_11299; 
BgL_statezd218zd21204z00_11161:
if(
(BgL_forwardz00_11287==BgL_bufposz00_11288))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6566z00_13594;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11289;
if(
INPUT_PORTP(BgL_iportz00_11285))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11289 = BgL_iportz00_11285; }  else 
{ 
 obj_t BgL_auxz00_13597;
BgL_auxz00_13597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5812z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11285); 
FAILURE(BgL_auxz00_13597,BFALSE,BFALSE);} 
BgL_test6566z00_13594 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11289); } 
if(BgL_test6566z00_13594)
{ /* Llib/date.scm 1063 */
 long BgL_arg2200z00_11290; long BgL_arg2201z00_11291;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11292;
if(
INPUT_PORTP(BgL_iportz00_11285))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11292 = BgL_iportz00_11285; }  else 
{ 
 obj_t BgL_auxz00_13604;
BgL_auxz00_13604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5812z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11285); 
FAILURE(BgL_auxz00_13604,BFALSE,BFALSE);} 
BgL_arg2200z00_11290 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11292); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11293;
if(
INPUT_PORTP(BgL_iportz00_11285))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11293 = BgL_iportz00_11285; }  else 
{ 
 obj_t BgL_auxz00_13611;
BgL_auxz00_13611 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5812z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11285); 
FAILURE(BgL_auxz00_13611,BFALSE,BFALSE);} 
BgL_arg2201z00_11291 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11293); } 
{ 
 long BgL_bufposz00_13617; long BgL_forwardz00_13616;
BgL_forwardz00_13616 = BgL_arg2200z00_11290; 
BgL_bufposz00_13617 = BgL_arg2201z00_11291; 
BgL_bufposz00_11288 = BgL_bufposz00_13617; 
BgL_forwardz00_11287 = BgL_forwardz00_13616; 
goto BgL_statezd218zd21204z00_11161;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11286; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11294;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11295;
if(
INPUT_PORTP(BgL_iportz00_11285))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11295 = BgL_iportz00_11285; }  else 
{ 
 obj_t BgL_auxz00_13620;
BgL_auxz00_13620 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5812z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11285); 
FAILURE(BgL_auxz00_13620,BFALSE,BFALSE);} 
BgL_curz00_11294 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11295, BgL_forwardz00_11287); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6571z00_13625;
if(
(
(long)(BgL_curz00_11294)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6571z00_13625 = 
(
(long)(BgL_curz00_11294)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6571z00_13625 = ((bool_t)0)
; } 
if(BgL_test6571z00_13625)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11271 = BgL_iportz00_11285; 
BgL_lastzd2matchzd2_11272 = BgL_lastzd2matchzd2_11286; 
BgL_forwardz00_11273 = 
(1L+BgL_forwardz00_11287); 
BgL_bufposz00_11274 = BgL_bufposz00_11288; 
BgL_statezd219zd21205z00_11160:
if(
(BgL_forwardz00_11273==BgL_bufposz00_11274))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6574z00_13633;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11275;
if(
INPUT_PORTP(BgL_iportz00_11271))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11275 = BgL_iportz00_11271; }  else 
{ 
 obj_t BgL_auxz00_13636;
BgL_auxz00_13636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5811z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11271); 
FAILURE(BgL_auxz00_13636,BFALSE,BFALSE);} 
BgL_test6574z00_13633 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11275); } 
if(BgL_test6574z00_13633)
{ /* Llib/date.scm 1063 */
 long BgL_arg2208z00_11276; long BgL_arg2209z00_11277;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11278;
if(
INPUT_PORTP(BgL_iportz00_11271))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11278 = BgL_iportz00_11271; }  else 
{ 
 obj_t BgL_auxz00_13643;
BgL_auxz00_13643 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5811z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11271); 
FAILURE(BgL_auxz00_13643,BFALSE,BFALSE);} 
BgL_arg2208z00_11276 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11278); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11279;
if(
INPUT_PORTP(BgL_iportz00_11271))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11279 = BgL_iportz00_11271; }  else 
{ 
 obj_t BgL_auxz00_13650;
BgL_auxz00_13650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5811z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11271); 
FAILURE(BgL_auxz00_13650,BFALSE,BFALSE);} 
BgL_arg2209z00_11277 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11279); } 
{ 
 long BgL_bufposz00_13656; long BgL_forwardz00_13655;
BgL_forwardz00_13655 = BgL_arg2208z00_11276; 
BgL_bufposz00_13656 = BgL_arg2209z00_11277; 
BgL_bufposz00_11274 = BgL_bufposz00_13656; 
BgL_forwardz00_11273 = BgL_forwardz00_13655; 
goto BgL_statezd219zd21205z00_11160;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11272; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11280;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11281;
if(
INPUT_PORTP(BgL_iportz00_11271))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11281 = BgL_iportz00_11271; }  else 
{ 
 obj_t BgL_auxz00_13659;
BgL_auxz00_13659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5811z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11271); 
FAILURE(BgL_auxz00_13659,BFALSE,BFALSE);} 
BgL_curz00_11280 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11281, BgL_forwardz00_11273); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6579z00_13664;
if(
(
(long)(BgL_curz00_11280)>=48L))
{ /* Llib/date.scm 1063 */
BgL_test6579z00_13664 = 
(
(long)(BgL_curz00_11280)<58L)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6579z00_13664 = ((bool_t)0)
; } 
if(BgL_test6579z00_13664)
{ /* Llib/date.scm 1063 */
 long BgL_arg2212z00_11282;
BgL_arg2212z00_11282 = 
(1L+BgL_forwardz00_11273); 
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11283;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11284;
if(
INPUT_PORTP(BgL_iportz00_11271))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11284 = BgL_iportz00_11271; }  else 
{ 
 obj_t BgL_auxz00_13673;
BgL_auxz00_13673 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5811z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11271); 
FAILURE(BgL_auxz00_13673,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11284, BgL_arg2212z00_11282); } 
BgL_newzd2matchzd2_11283 = 3L; 
BgL_matchz00_11177 = BgL_newzd2matchzd2_11283; } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11272; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11286; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11300; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11459; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11397; } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_lastzd2matchzd2_11386; } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11327; } } } } } }  else 
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6582z00_13685;
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6583z00_13686;
if(
(
(long)(BgL_curz00_11318)==10L))
{ /* Llib/date.scm 1063 */
BgL_test6583z00_13686 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6583z00_13686 = 
(
(long)(BgL_curz00_11318)==9L)
; } 
if(BgL_test6583z00_13686)
{ /* Llib/date.scm 1063 */
BgL_test6582z00_13685 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
if(
(
(long)(BgL_curz00_11318)==13L))
{ /* Llib/date.scm 1063 */
BgL_test6582z00_13685 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6582z00_13685 = 
(
(long)(BgL_curz00_11318)==32L)
; } } } 
if(BgL_test6582z00_13685)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11445 = BgL_iportz00_11309; 
BgL_lastzd2matchzd2_11446 = BgL_lastzd2matchzd2_11310; 
BgL_forwardz00_11447 = 
(1L+BgL_forwardz00_11311); 
BgL_bufposz00_11448 = BgL_bufposz00_11312; 
BgL_statezd22zd21188z00_11174:
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11449;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11450;
if(
INPUT_PORTP(BgL_iportz00_11445))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11450 = BgL_iportz00_11445; }  else 
{ 
 obj_t BgL_auxz00_13699;
BgL_auxz00_13699 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5825z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11445); 
FAILURE(BgL_auxz00_13699,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11450, BgL_forwardz00_11447); } 
BgL_newzd2matchzd2_11449 = 0L; 
if(
(BgL_forwardz00_11447==BgL_bufposz00_11448))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6588z00_13706;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11451;
if(
INPUT_PORTP(BgL_iportz00_11445))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11451 = BgL_iportz00_11445; }  else 
{ 
 obj_t BgL_auxz00_13709;
BgL_auxz00_13709 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5825z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11445); 
FAILURE(BgL_auxz00_13709,BFALSE,BFALSE);} 
BgL_test6588z00_13706 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11451); } 
if(BgL_test6588z00_13706)
{ /* Llib/date.scm 1063 */
 long BgL_arg2081z00_11452; long BgL_arg2082z00_11453;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11454;
if(
INPUT_PORTP(BgL_iportz00_11445))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11454 = BgL_iportz00_11445; }  else 
{ 
 obj_t BgL_auxz00_13716;
BgL_auxz00_13716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5825z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11445); 
FAILURE(BgL_auxz00_13716,BFALSE,BFALSE);} 
BgL_arg2081z00_11452 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11454); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11455;
if(
INPUT_PORTP(BgL_iportz00_11445))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11455 = BgL_iportz00_11445; }  else 
{ 
 obj_t BgL_auxz00_13723;
BgL_auxz00_13723 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5825z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11445); 
FAILURE(BgL_auxz00_13723,BFALSE,BFALSE);} 
BgL_arg2082z00_11453 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11455); } 
{ 
 long BgL_bufposz00_13729; long BgL_forwardz00_13728;
BgL_forwardz00_13728 = BgL_arg2081z00_11452; 
BgL_bufposz00_13729 = BgL_arg2082z00_11453; 
BgL_bufposz00_11448 = BgL_bufposz00_13729; 
BgL_forwardz00_11447 = BgL_forwardz00_13728; 
goto BgL_statezd22zd21188z00_11174;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11449; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11456;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11457;
if(
INPUT_PORTP(BgL_iportz00_11445))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11457 = BgL_iportz00_11445; }  else 
{ 
 obj_t BgL_auxz00_13732;
BgL_auxz00_13732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5825z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11445); 
FAILURE(BgL_auxz00_13732,BFALSE,BFALSE);} 
BgL_curz00_11456 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11457, BgL_forwardz00_11447); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6593z00_13737;
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6594z00_13738;
if(
(
(long)(BgL_curz00_11456)==10L))
{ /* Llib/date.scm 1063 */
BgL_test6594z00_13738 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6594z00_13738 = 
(
(long)(BgL_curz00_11456)==9L)
; } 
if(BgL_test6594z00_13738)
{ /* Llib/date.scm 1063 */
BgL_test6593z00_13737 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
if(
(
(long)(BgL_curz00_11456)==13L))
{ /* Llib/date.scm 1063 */
BgL_test6593z00_13737 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6593z00_13737 = 
(
(long)(BgL_curz00_11456)==32L)
; } } } 
if(BgL_test6593z00_13737)
{ /* Llib/date.scm 1063 */
BgL_iportz00_11258 = BgL_iportz00_11445; 
BgL_lastzd2matchzd2_11259 = BgL_newzd2matchzd2_11449; 
BgL_forwardz00_11260 = 
(1L+BgL_forwardz00_11447); 
BgL_bufposz00_11261 = BgL_bufposz00_11448; 
BgL_statezd221zd21207z00_11159:
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11262;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11263;
if(
INPUT_PORTP(BgL_iportz00_11258))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11263 = BgL_iportz00_11258; }  else 
{ 
 obj_t BgL_auxz00_13751;
BgL_auxz00_13751 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5810z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11258); 
FAILURE(BgL_auxz00_13751,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11263, BgL_forwardz00_11260); } 
BgL_newzd2matchzd2_11262 = 0L; 
if(
(BgL_forwardz00_11260==BgL_bufposz00_11261))
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6599z00_13758;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11264;
if(
INPUT_PORTP(BgL_iportz00_11258))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11264 = BgL_iportz00_11258; }  else 
{ 
 obj_t BgL_auxz00_13761;
BgL_auxz00_13761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5810z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11258); 
FAILURE(BgL_auxz00_13761,BFALSE,BFALSE);} 
BgL_test6599z00_13758 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11264); } 
if(BgL_test6599z00_13758)
{ /* Llib/date.scm 1063 */
 long BgL_arg2217z00_11265; long BgL_arg2218z00_11266;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11267;
if(
INPUT_PORTP(BgL_iportz00_11258))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11267 = BgL_iportz00_11258; }  else 
{ 
 obj_t BgL_auxz00_13768;
BgL_auxz00_13768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5810z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11258); 
FAILURE(BgL_auxz00_13768,BFALSE,BFALSE);} 
BgL_arg2217z00_11265 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11267); } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11268;
if(
INPUT_PORTP(BgL_iportz00_11258))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11268 = BgL_iportz00_11258; }  else 
{ 
 obj_t BgL_auxz00_13775;
BgL_auxz00_13775 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5810z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11258); 
FAILURE(BgL_auxz00_13775,BFALSE,BFALSE);} 
BgL_arg2218z00_11266 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11268); } 
{ 
 long BgL_bufposz00_13781; long BgL_forwardz00_13780;
BgL_forwardz00_13780 = BgL_arg2217z00_11265; 
BgL_bufposz00_13781 = BgL_arg2218z00_11266; 
BgL_bufposz00_11261 = BgL_bufposz00_13781; 
BgL_forwardz00_11260 = BgL_forwardz00_13780; 
goto BgL_statezd221zd21207z00_11159;} }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11262; } }  else 
{ /* Llib/date.scm 1063 */
 int BgL_curz00_11269;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11270;
if(
INPUT_PORTP(BgL_iportz00_11258))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11270 = BgL_iportz00_11258; }  else 
{ 
 obj_t BgL_auxz00_13784;
BgL_auxz00_13784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5810z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11258); 
FAILURE(BgL_auxz00_13784,BFALSE,BFALSE);} 
BgL_curz00_11269 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11270, BgL_forwardz00_11260); } 
{ /* Llib/date.scm 1063 */

{ /* Llib/date.scm 1063 */
 bool_t BgL_test6604z00_13789;
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6605z00_13790;
if(
(
(long)(BgL_curz00_11269)==10L))
{ /* Llib/date.scm 1063 */
BgL_test6605z00_13790 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6605z00_13790 = 
(
(long)(BgL_curz00_11269)==9L)
; } 
if(BgL_test6605z00_13790)
{ /* Llib/date.scm 1063 */
BgL_test6604z00_13789 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
if(
(
(long)(BgL_curz00_11269)==13L))
{ /* Llib/date.scm 1063 */
BgL_test6604z00_13789 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1063 */
BgL_test6604z00_13789 = 
(
(long)(BgL_curz00_11269)==32L)
; } } } 
if(BgL_test6604z00_13789)
{ 
 long BgL_forwardz00_13802; long BgL_lastzd2matchzd2_13801;
BgL_lastzd2matchzd2_13801 = BgL_newzd2matchzd2_11262; 
BgL_forwardz00_13802 = 
(1L+BgL_forwardz00_11260); 
BgL_forwardz00_11260 = BgL_forwardz00_13802; 
BgL_lastzd2matchzd2_11259 = BgL_lastzd2matchzd2_13801; 
goto BgL_statezd221zd21207z00_11159;}  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11262; } } } } } }  else 
{ /* Llib/date.scm 1063 */
BgL_matchz00_11177 = BgL_newzd2matchzd2_11449; } } } } } }  else 
{ /* Llib/date.scm 1063 */
 long BgL_arg2189z00_11320;
BgL_arg2189z00_11320 = 
(1L+BgL_forwardz00_11311); 
{ /* Llib/date.scm 1063 */
 long BgL_newzd2matchzd2_11321;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11322;
if(
INPUT_PORTP(BgL_iportz00_11309))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11322 = BgL_iportz00_11309; }  else 
{ 
 obj_t BgL_auxz00_13809;
BgL_auxz00_13809 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5814z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11309); 
FAILURE(BgL_auxz00_13809,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11322, BgL_arg2189z00_11320); } 
BgL_newzd2matchzd2_11321 = 6L; 
BgL_matchz00_11177 = BgL_newzd2matchzd2_11321; } } } } } } } 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11182;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11182 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13816;
BgL_auxz00_13816 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13816,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_11182); } 
switch( BgL_matchz00_11177) { case 6L : 

{ /* Llib/date.scm 1098 */
 obj_t BgL_arg2311z00_11183; obj_t BgL_arg2312z00_11184;
{ /* Llib/date.scm 1063 */
 bool_t BgL_test6610z00_13821;
{ /* Llib/date.scm 1063 */
 long BgL_arg2304z00_11255;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11256;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11256 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13824;
BgL_auxz00_13824 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13824,BFALSE,BFALSE);} 
BgL_arg2304z00_11255 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_11256); } 
BgL_test6610z00_13821 = 
(BgL_arg2304z00_11255==0L); } 
if(BgL_test6610z00_13821)
{ /* Llib/date.scm 1063 */
BgL_arg2311z00_11183 = BEOF; }  else 
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11257;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11257 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13832;
BgL_auxz00_13832 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13832,BFALSE,BFALSE);} 
BgL_arg2311z00_11183 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_11257)); } } 
{ /* Llib/date.scm 1098 */
 obj_t BgL_res4738z00_11185;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_res4738z00_11185 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13840;
BgL_auxz00_13840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13840,BFALSE,BFALSE);} 
BgL_arg2312z00_11184 = BgL_res4738z00_11185; } 
return 
BGl_parsezd2errorzd2zz__datez00(BGl_string5787z00zz__datez00, BGl_string5809z00zz__datez00, BgL_arg2311z00_11183, BgL_arg2312z00_11184);} break;case 5L : 

{ /* Llib/date.scm 1093 */
 long BgL_b1z00_11186; long BgL_b2z00_11187;
{ /* Llib/date.scm 1093 */
 int BgL_arg2314z00_11188;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11189;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11189 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13847;
BgL_auxz00_13847 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13847,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13851;
BgL_tmpz00_13851 = 
(int)(0L); 
BgL_arg2314z00_11188 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11189, BgL_tmpz00_13851); } } 
BgL_b1z00_11186 = 
(
(long)(BgL_arg2314z00_11188)-48L); } 
{ /* Llib/date.scm 1094 */
 int BgL_arg2315z00_11190;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11191;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11191 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13858;
BgL_auxz00_13858 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13858,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13862;
BgL_tmpz00_13862 = 
(int)(2L); 
BgL_arg2315z00_11190 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11191, BgL_tmpz00_13862); } } 
BgL_b2z00_11187 = 
(
(long)(BgL_arg2315z00_11190)-48L); } 
{ /* Llib/date.scm 1095 */
 long BgL_val2_1286z00_11192;
BgL_val2_1286z00_11192 = 
(
(BgL_b1z00_11186*10L)+BgL_b2z00_11187); 
{ /* Llib/date.scm 1095 */
 int BgL_tmpz00_13869;
BgL_tmpz00_13869 = 
(int)(3L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_13869); } 
{ /* Llib/date.scm 1095 */
 obj_t BgL_auxz00_13874; int BgL_tmpz00_13872;
BgL_auxz00_13874 = 
BINT(0L); 
BgL_tmpz00_13872 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13872, BgL_auxz00_13874); } 
{ /* Llib/date.scm 1095 */
 obj_t BgL_auxz00_13879; int BgL_tmpz00_13877;
BgL_auxz00_13879 = 
BINT(BgL_val2_1286z00_11192); 
BgL_tmpz00_13877 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13877, BgL_auxz00_13879); } 
return 
BINT(0L);} } break;case 4L : 

{ /* Llib/date.scm 1086 */
 long BgL_b1z00_11193; long BgL_b3z00_11194; long BgL_b4z00_11195; long BgL_b5z00_11196; long BgL_b6z00_11197;
{ /* Llib/date.scm 1086 */
 int BgL_arg2318z00_11198;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11199;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11199 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13885;
BgL_auxz00_13885 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13885,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13889;
BgL_tmpz00_13889 = 
(int)(0L); 
BgL_arg2318z00_11198 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11199, BgL_tmpz00_13889); } } 
BgL_b1z00_11193 = 
(
(long)(BgL_arg2318z00_11198)-48L); } 
{ /* Llib/date.scm 1087 */
 int BgL_arg2319z00_11200;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11201;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11201 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13896;
BgL_auxz00_13896 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13896,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13900;
BgL_tmpz00_13900 = 
(int)(2L); 
BgL_arg2319z00_11200 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11201, BgL_tmpz00_13900); } } 
BgL_b3z00_11194 = 
(
(long)(BgL_arg2319z00_11200)-48L); } 
{ /* Llib/date.scm 1088 */
 int BgL_arg2320z00_11202;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11203;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11203 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13907;
BgL_auxz00_13907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13907,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13911;
BgL_tmpz00_13911 = 
(int)(3L); 
BgL_arg2320z00_11202 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11203, BgL_tmpz00_13911); } } 
BgL_b4z00_11195 = 
(
(long)(BgL_arg2320z00_11202)-48L); } 
{ /* Llib/date.scm 1089 */
 int BgL_arg2321z00_11204;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11205;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11205 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13918;
BgL_auxz00_13918 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13918,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13922;
BgL_tmpz00_13922 = 
(int)(5L); 
BgL_arg2321z00_11204 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11205, BgL_tmpz00_13922); } } 
BgL_b5z00_11196 = 
(
(long)(BgL_arg2321z00_11204)-48L); } 
{ /* Llib/date.scm 1090 */
 int BgL_arg2323z00_11206;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11207;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11207 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13929;
BgL_auxz00_13929 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13929,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13933;
BgL_tmpz00_13933 = 
(int)(6L); 
BgL_arg2323z00_11206 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11207, BgL_tmpz00_13933); } } 
BgL_b6z00_11197 = 
(
(long)(BgL_arg2323z00_11206)-48L); } 
{ /* Llib/date.scm 1091 */
 long BgL_val1_1288z00_11208; long BgL_val2_1289z00_11209;
BgL_val1_1288z00_11208 = 
(
(BgL_b3z00_11194*10L)+BgL_b4z00_11195); 
BgL_val2_1289z00_11209 = 
(
(BgL_b5z00_11196*10L)+BgL_b6z00_11197); 
{ /* Llib/date.scm 1091 */
 int BgL_tmpz00_13942;
BgL_tmpz00_13942 = 
(int)(3L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_13942); } 
{ /* Llib/date.scm 1091 */
 obj_t BgL_auxz00_13947; int BgL_tmpz00_13945;
BgL_auxz00_13947 = 
BINT(BgL_val1_1288z00_11208); 
BgL_tmpz00_13945 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13945, BgL_auxz00_13947); } 
{ /* Llib/date.scm 1091 */
 obj_t BgL_auxz00_13952; int BgL_tmpz00_13950;
BgL_auxz00_13952 = 
BINT(BgL_val2_1289z00_11209); 
BgL_tmpz00_13950 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13950, BgL_auxz00_13952); } 
return 
BINT(BgL_b1z00_11193);} } break;case 3L : 

{ /* Llib/date.scm 1078 */
 long BgL_b1z00_11210; long BgL_b2z00_11211; long BgL_b3z00_11212; long BgL_b4z00_11213; long BgL_b5z00_11214; long BgL_b6z00_11215;
{ /* Llib/date.scm 1078 */
 int BgL_arg2327z00_11216;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11217;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11217 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13958;
BgL_auxz00_13958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13958,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13962;
BgL_tmpz00_13962 = 
(int)(0L); 
BgL_arg2327z00_11216 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11217, BgL_tmpz00_13962); } } 
BgL_b1z00_11210 = 
(
(long)(BgL_arg2327z00_11216)-48L); } 
{ /* Llib/date.scm 1079 */
 int BgL_arg2328z00_11218;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11219;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11219 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13969;
BgL_auxz00_13969 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13969,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13973;
BgL_tmpz00_13973 = 
(int)(1L); 
BgL_arg2328z00_11218 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11219, BgL_tmpz00_13973); } } 
BgL_b2z00_11211 = 
(
(long)(BgL_arg2328z00_11218)-48L); } 
{ /* Llib/date.scm 1080 */
 int BgL_arg2330z00_11220;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11221;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11221 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13980;
BgL_auxz00_13980 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13980,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13984;
BgL_tmpz00_13984 = 
(int)(3L); 
BgL_arg2330z00_11220 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11221, BgL_tmpz00_13984); } } 
BgL_b3z00_11212 = 
(
(long)(BgL_arg2330z00_11220)-48L); } 
{ /* Llib/date.scm 1081 */
 int BgL_arg2331z00_11222;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11223;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11223 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_13991;
BgL_auxz00_13991 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_13991,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_13995;
BgL_tmpz00_13995 = 
(int)(4L); 
BgL_arg2331z00_11222 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11223, BgL_tmpz00_13995); } } 
BgL_b4z00_11213 = 
(
(long)(BgL_arg2331z00_11222)-48L); } 
{ /* Llib/date.scm 1082 */
 int BgL_arg2333z00_11224;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11225;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11225 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14002;
BgL_auxz00_14002 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14002,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14006;
BgL_tmpz00_14006 = 
(int)(6L); 
BgL_arg2333z00_11224 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11225, BgL_tmpz00_14006); } } 
BgL_b5z00_11214 = 
(
(long)(BgL_arg2333z00_11224)-48L); } 
{ /* Llib/date.scm 1083 */
 int BgL_arg2335z00_11226;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11227;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11227 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14013;
BgL_auxz00_14013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14013,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14017;
BgL_tmpz00_14017 = 
(int)(7L); 
BgL_arg2335z00_11226 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11227, BgL_tmpz00_14017); } } 
BgL_b6z00_11215 = 
(
(long)(BgL_arg2335z00_11226)-48L); } 
{ /* Llib/date.scm 1084 */
 long BgL_val0_1290z00_11228; long BgL_val1_1291z00_11229; long BgL_val2_1292z00_11230;
BgL_val0_1290z00_11228 = 
(
(BgL_b1z00_11210*10L)+BgL_b2z00_11211); 
BgL_val1_1291z00_11229 = 
(
(BgL_b3z00_11212*10L)+BgL_b4z00_11213); 
BgL_val2_1292z00_11230 = 
(
(BgL_b5z00_11214*10L)+BgL_b6z00_11215); 
{ /* Llib/date.scm 1084 */
 int BgL_tmpz00_14028;
BgL_tmpz00_14028 = 
(int)(3L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_14028); } 
{ /* Llib/date.scm 1084 */
 obj_t BgL_auxz00_14033; int BgL_tmpz00_14031;
BgL_auxz00_14033 = 
BINT(BgL_val1_1291z00_11229); 
BgL_tmpz00_14031 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_14031, BgL_auxz00_14033); } 
{ /* Llib/date.scm 1084 */
 obj_t BgL_auxz00_14038; int BgL_tmpz00_14036;
BgL_auxz00_14038 = 
BINT(BgL_val2_1292z00_11230); 
BgL_tmpz00_14036 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_14036, BgL_auxz00_14038); } 
return 
BINT(BgL_val0_1290z00_11228);} } break;case 2L : 

{ /* Llib/date.scm 1072 */
 long BgL_b1z00_11231; long BgL_b2z00_11232; long BgL_b3z00_11233; long BgL_b4z00_11234;
{ /* Llib/date.scm 1072 */
 int BgL_arg2338z00_11235;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11236;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11236 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14044;
BgL_auxz00_14044 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14044,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14048;
BgL_tmpz00_14048 = 
(int)(0L); 
BgL_arg2338z00_11235 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11236, BgL_tmpz00_14048); } } 
BgL_b1z00_11231 = 
(
(long)(BgL_arg2338z00_11235)-48L); } 
{ /* Llib/date.scm 1073 */
 int BgL_arg2339z00_11237;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11238;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11238 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14055;
BgL_auxz00_14055 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14055,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14059;
BgL_tmpz00_14059 = 
(int)(1L); 
BgL_arg2339z00_11237 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11238, BgL_tmpz00_14059); } } 
BgL_b2z00_11232 = 
(
(long)(BgL_arg2339z00_11237)-48L); } 
{ /* Llib/date.scm 1074 */
 int BgL_arg2340z00_11239;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11240;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11240 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14066;
BgL_auxz00_14066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14066,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14070;
BgL_tmpz00_14070 = 
(int)(3L); 
BgL_arg2340z00_11239 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11240, BgL_tmpz00_14070); } } 
BgL_b3z00_11233 = 
(
(long)(BgL_arg2340z00_11239)-48L); } 
{ /* Llib/date.scm 1075 */
 int BgL_arg2341z00_11241;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11242;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11242 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14077;
BgL_auxz00_14077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14077,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14081;
BgL_tmpz00_14081 = 
(int)(4L); 
BgL_arg2341z00_11241 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11242, BgL_tmpz00_14081); } } 
BgL_b4z00_11234 = 
(
(long)(BgL_arg2341z00_11241)-48L); } 
{ /* Llib/date.scm 1076 */
 long BgL_val0_1293z00_11243; long BgL_val1_1294z00_11244;
BgL_val0_1293z00_11243 = 
(
(BgL_b1z00_11231*10L)+BgL_b2z00_11232); 
BgL_val1_1294z00_11244 = 
(
(BgL_b3z00_11233*10L)+BgL_b4z00_11234); 
{ /* Llib/date.scm 1076 */
 int BgL_tmpz00_14090;
BgL_tmpz00_14090 = 
(int)(3L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_14090); } 
{ /* Llib/date.scm 1076 */
 obj_t BgL_auxz00_14095; int BgL_tmpz00_14093;
BgL_auxz00_14095 = 
BINT(BgL_val1_1294z00_11244); 
BgL_tmpz00_14093 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_14093, BgL_auxz00_14095); } 
{ /* Llib/date.scm 1076 */
 obj_t BgL_auxz00_14100; int BgL_tmpz00_14098;
BgL_auxz00_14100 = 
BINT(0L); 
BgL_tmpz00_14098 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_14098, BgL_auxz00_14100); } 
return 
BINT(BgL_val0_1293z00_11243);} } break;case 1L : 

{ /* Llib/date.scm 1067 */
 long BgL_b1z00_11245; long BgL_b3z00_11246; long BgL_b4z00_11247;
{ /* Llib/date.scm 1067 */
 int BgL_arg2345z00_11248;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11249;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11249 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14106;
BgL_auxz00_14106 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14106,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14110;
BgL_tmpz00_14110 = 
(int)(0L); 
BgL_arg2345z00_11248 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11249, BgL_tmpz00_14110); } } 
BgL_b1z00_11245 = 
(
(long)(BgL_arg2345z00_11248)-48L); } 
{ /* Llib/date.scm 1068 */
 int BgL_arg2346z00_11250;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11251;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11251 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14117;
BgL_auxz00_14117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14117,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14121;
BgL_tmpz00_14121 = 
(int)(2L); 
BgL_arg2346z00_11250 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11251, BgL_tmpz00_14121); } } 
BgL_b3z00_11246 = 
(
(long)(BgL_arg2346z00_11250)-48L); } 
{ /* Llib/date.scm 1069 */
 int BgL_arg2348z00_11252;
{ /* Llib/date.scm 1063 */
 obj_t BgL_inputzd2portzd2_11253;
if(
INPUT_PORTP(BgL_iportz00_9762))
{ /* Llib/date.scm 1063 */
BgL_inputzd2portzd2_11253 = BgL_iportz00_9762; }  else 
{ 
 obj_t BgL_auxz00_14128;
BgL_auxz00_14128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43846L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9762); 
FAILURE(BgL_auxz00_14128,BFALSE,BFALSE);} 
{ /* Llib/date.scm 1063 */
 int BgL_tmpz00_14132;
BgL_tmpz00_14132 = 
(int)(3L); 
BgL_arg2348z00_11252 = 
RGC_BUFFER_BYTE_REF(BgL_inputzd2portzd2_11253, BgL_tmpz00_14132); } } 
BgL_b4z00_11247 = 
(
(long)(BgL_arg2348z00_11252)-48L); } 
{ /* Llib/date.scm 1070 */
 long BgL_val1_1297z00_11254;
BgL_val1_1297z00_11254 = 
(
(BgL_b3z00_11246*10L)+BgL_b4z00_11247); 
{ /* Llib/date.scm 1070 */
 int BgL_tmpz00_14139;
BgL_tmpz00_14139 = 
(int)(3L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_14139); } 
{ /* Llib/date.scm 1070 */
 obj_t BgL_auxz00_14144; int BgL_tmpz00_14142;
BgL_auxz00_14144 = 
BINT(BgL_val1_1297z00_11254); 
BgL_tmpz00_14142 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_14142, BgL_auxz00_14144); } 
{ /* Llib/date.scm 1070 */
 obj_t BgL_auxz00_14149; int BgL_tmpz00_14147;
BgL_auxz00_14149 = 
BINT(0L); 
BgL_tmpz00_14147 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_14147, BgL_auxz00_14149); } 
return 
BINT(BgL_b1z00_11245);} } break;case 0L : 

goto BgL_ignorez00_11157;break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_11177));} } } } } } 

}



/* &<@anonymous:1859> */
obj_t BGl_z62zc3z04anonymousza31859ze3ze5zz__datez00(obj_t BgL_envz00_9763, obj_t BgL_iportz00_9764)
{
{ /* Llib/date.scm 1035 */
{ 
 obj_t BgL_iportz00_11560; long BgL_lastzd2matchzd2_11561; long BgL_forwardz00_11562; long BgL_bufposz00_11563; obj_t BgL_iportz00_11547; long BgL_lastzd2matchzd2_11548; long BgL_forwardz00_11549; long BgL_bufposz00_11550; obj_t BgL_iportz00_11534; long BgL_lastzd2matchzd2_11535; long BgL_forwardz00_11536; long BgL_bufposz00_11537; obj_t BgL_iportz00_11521; long BgL_lastzd2matchzd2_11522; long BgL_forwardz00_11523; long BgL_bufposz00_11524; obj_t BgL_iportz00_11507; long BgL_lastzd2matchzd2_11508; long BgL_forwardz00_11509; long BgL_bufposz00_11510;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6634z00_14156;
{ /* Llib/date.scm 1035 */
 obj_t BgL_arg1866z00_11574;
{ /* Llib/date.scm 1035 */
 obj_t BgL_res4736z00_11575;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_res4736z00_11575 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14159;
BgL_auxz00_14159 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5858z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14159,BFALSE,BFALSE);} 
BgL_arg1866z00_11574 = BgL_res4736z00_11575; } 
BgL_test6634z00_14156 = 
INPUT_PORT_CLOSEP(BgL_arg1866z00_11574); } 
if(BgL_test6634z00_14156)
{ /* Llib/date.scm 1035 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1860z00_11576;
{ /* Llib/date.scm 1035 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_11577;
{ /* Llib/date.scm 1035 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_11578;
BgL_new1077z00_11578 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 1035 */
 long BgL_arg1864z00_11579;
BgL_arg1864z00_11579 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_11578), BgL_arg1864z00_11579); } 
BgL_new1078z00_11577 = BgL_new1077z00_11578; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11577)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11577)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_14172;
{ /* Llib/date.scm 1035 */
 obj_t BgL_arg1862z00_11580;
{ /* Llib/date.scm 1035 */
 obj_t BgL_arg1863z00_11581;
{ /* Llib/date.scm 1035 */
 obj_t BgL_classz00_11582;
BgL_classz00_11582 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1863z00_11581 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_11582); } 
BgL_arg1862z00_11580 = 
VECTOR_REF(BgL_arg1863z00_11581,2L); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_auxz00_14176;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1862z00_11580))
{ /* Llib/date.scm 1035 */
BgL_auxz00_14176 = BgL_arg1862z00_11580
; }  else 
{ 
 obj_t BgL_auxz00_14179;
BgL_auxz00_14179 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5858z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg1862z00_11580); 
FAILURE(BgL_auxz00_14179,BFALSE,BFALSE);} 
BgL_auxz00_14172 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_14176); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11577)))->BgL_stackz00)=((obj_t)BgL_auxz00_14172),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11577)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11577)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_14189;
{ /* Llib/date.scm 1035 */
 obj_t BgL_res4737z00_11583;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_res4737z00_11583 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14193;
BgL_auxz00_14193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5858z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14193,BFALSE,BFALSE);} 
BgL_auxz00_14189 = BgL_res4737z00_11583; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11577)))->BgL_objz00)=((obj_t)BgL_auxz00_14189),BUNSPEC); } 
BgL_arg1860z00_11576 = BgL_new1078z00_11577; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1860z00_11576));}  else 
{ /* Llib/date.scm 1035 */
BgL_ignorez00_11479:
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11486;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11486 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14202;
BgL_auxz00_14202 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14202,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_11486); } 
{ /* Llib/date.scm 1035 */
 long BgL_matchz00_11487;
{ /* Llib/date.scm 1035 */
 long BgL_arg2060z00_11488; long BgL_arg2061z00_11489;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11490;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11490 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14209;
BgL_auxz00_14209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14209,BFALSE,BFALSE);} 
BgL_arg2060z00_11488 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11490); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11491;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11491 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14216;
BgL_auxz00_14216 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14216,BFALSE,BFALSE);} 
BgL_arg2061z00_11489 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11491); } 
BgL_iportz00_11560 = BgL_iportz00_9764; 
BgL_lastzd2matchzd2_11561 = 2L; 
BgL_forwardz00_11562 = BgL_arg2060z00_11488; 
BgL_bufposz00_11563 = BgL_arg2061z00_11489; 
BgL_statezd20zd21176z00_11485:
if(
(BgL_forwardz00_11562==BgL_bufposz00_11563))
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6642z00_14223;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11564;
if(
INPUT_PORTP(BgL_iportz00_11560))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11564 = BgL_iportz00_11560; }  else 
{ 
 obj_t BgL_auxz00_14226;
BgL_auxz00_14226 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5857z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11560); 
FAILURE(BgL_auxz00_14226,BFALSE,BFALSE);} 
BgL_test6642z00_14223 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11564); } 
if(BgL_test6642z00_14223)
{ /* Llib/date.scm 1035 */
 long BgL_arg1870z00_11565; long BgL_arg1872z00_11566;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11567;
if(
INPUT_PORTP(BgL_iportz00_11560))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11567 = BgL_iportz00_11560; }  else 
{ 
 obj_t BgL_auxz00_14233;
BgL_auxz00_14233 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5857z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11560); 
FAILURE(BgL_auxz00_14233,BFALSE,BFALSE);} 
BgL_arg1870z00_11565 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11567); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11568;
if(
INPUT_PORTP(BgL_iportz00_11560))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11568 = BgL_iportz00_11560; }  else 
{ 
 obj_t BgL_auxz00_14240;
BgL_auxz00_14240 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5857z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11560); 
FAILURE(BgL_auxz00_14240,BFALSE,BFALSE);} 
BgL_arg1872z00_11566 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11568); } 
{ 
 long BgL_bufposz00_14246; long BgL_forwardz00_14245;
BgL_forwardz00_14245 = BgL_arg1870z00_11565; 
BgL_bufposz00_14246 = BgL_arg1872z00_11566; 
BgL_bufposz00_11563 = BgL_bufposz00_14246; 
BgL_forwardz00_11562 = BgL_forwardz00_14245; 
goto BgL_statezd20zd21176z00_11485;} }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_lastzd2matchzd2_11561; } }  else 
{ /* Llib/date.scm 1035 */
 int BgL_curz00_11569;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11570;
if(
INPUT_PORTP(BgL_iportz00_11560))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11570 = BgL_iportz00_11560; }  else 
{ 
 obj_t BgL_auxz00_14249;
BgL_auxz00_14249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5857z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11560); 
FAILURE(BgL_auxz00_14249,BFALSE,BFALSE);} 
BgL_curz00_11569 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11570, BgL_forwardz00_11562); } 
{ /* Llib/date.scm 1035 */

{ /* Llib/date.scm 1035 */
 bool_t BgL_test6647z00_14254;
if(
(
(long)(BgL_curz00_11569)==65L))
{ /* Llib/date.scm 1035 */
BgL_test6647z00_14254 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11569)==68L))
{ /* Llib/date.scm 1035 */
BgL_test6647z00_14254 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11569)==70L))
{ /* Llib/date.scm 1035 */
BgL_test6647z00_14254 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11569)==74L))
{ /* Llib/date.scm 1035 */
BgL_test6647z00_14254 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6652z00_14267;
if(
(
(long)(BgL_curz00_11569)==79L))
{ /* Llib/date.scm 1035 */
BgL_test6652z00_14267 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11569)==78L))
{ /* Llib/date.scm 1035 */
BgL_test6652z00_14267 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6652z00_14267 = 
(
(long)(BgL_curz00_11569)==77L)
; } } 
if(BgL_test6652z00_14267)
{ /* Llib/date.scm 1035 */
BgL_test6647z00_14254 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6647z00_14254 = 
(
(long)(BgL_curz00_11569)==83L)
; } } } } } 
if(BgL_test6647z00_14254)
{ /* Llib/date.scm 1035 */
BgL_iportz00_11521 = BgL_iportz00_11560; 
BgL_lastzd2matchzd2_11522 = BgL_lastzd2matchzd2_11561; 
BgL_forwardz00_11523 = 
(1L+BgL_forwardz00_11562); 
BgL_bufposz00_11524 = BgL_bufposz00_11563; 
BgL_statezd23zd21179z00_11482:
{ /* Llib/date.scm 1035 */
 long BgL_newzd2matchzd2_11525;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11526;
if(
INPUT_PORTP(BgL_iportz00_11521))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11526 = BgL_iportz00_11521; }  else 
{ 
 obj_t BgL_auxz00_14280;
BgL_auxz00_14280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5854z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11521); 
FAILURE(BgL_auxz00_14280,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11526, BgL_forwardz00_11523); } 
BgL_newzd2matchzd2_11525 = 2L; 
if(
(BgL_forwardz00_11523==BgL_bufposz00_11524))
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6657z00_14287;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11527;
if(
INPUT_PORTP(BgL_iportz00_11521))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11527 = BgL_iportz00_11521; }  else 
{ 
 obj_t BgL_auxz00_14290;
BgL_auxz00_14290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5854z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11521); 
FAILURE(BgL_auxz00_14290,BFALSE,BFALSE);} 
BgL_test6657z00_14287 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11527); } 
if(BgL_test6657z00_14287)
{ /* Llib/date.scm 1035 */
 long BgL_arg1916z00_11528; long BgL_arg1917z00_11529;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11530;
if(
INPUT_PORTP(BgL_iportz00_11521))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11530 = BgL_iportz00_11521; }  else 
{ 
 obj_t BgL_auxz00_14297;
BgL_auxz00_14297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5854z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11521); 
FAILURE(BgL_auxz00_14297,BFALSE,BFALSE);} 
BgL_arg1916z00_11528 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11530); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11531;
if(
INPUT_PORTP(BgL_iportz00_11521))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11531 = BgL_iportz00_11521; }  else 
{ 
 obj_t BgL_auxz00_14304;
BgL_auxz00_14304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5854z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11521); 
FAILURE(BgL_auxz00_14304,BFALSE,BFALSE);} 
BgL_arg1917z00_11529 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11531); } 
{ 
 long BgL_bufposz00_14310; long BgL_forwardz00_14309;
BgL_forwardz00_14309 = BgL_arg1916z00_11528; 
BgL_bufposz00_14310 = BgL_arg1917z00_11529; 
BgL_bufposz00_11524 = BgL_bufposz00_14310; 
BgL_forwardz00_11523 = BgL_forwardz00_14309; 
goto BgL_statezd23zd21179z00_11482;} }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_newzd2matchzd2_11525; } }  else 
{ /* Llib/date.scm 1035 */
 int BgL_curz00_11532;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11533;
if(
INPUT_PORTP(BgL_iportz00_11521))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11533 = BgL_iportz00_11521; }  else 
{ 
 obj_t BgL_auxz00_14313;
BgL_auxz00_14313 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5854z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11521); 
FAILURE(BgL_auxz00_14313,BFALSE,BFALSE);} 
BgL_curz00_11532 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11533, BgL_forwardz00_11523); } 
{ /* Llib/date.scm 1035 */

{ /* Llib/date.scm 1035 */
 bool_t BgL_test6662z00_14318;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6663z00_14319;
if(
(
(long)(BgL_curz00_11532)==99L))
{ /* Llib/date.scm 1035 */
BgL_test6663z00_14319 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==98L))
{ /* Llib/date.scm 1035 */
BgL_test6663z00_14319 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6663z00_14319 = 
(
(long)(BgL_curz00_11532)==97L)
; } } 
if(BgL_test6663z00_14319)
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==101L))
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==103L))
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==108L))
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6669z00_14337;
if(
(
(long)(BgL_curz00_11532)==112L))
{ /* Llib/date.scm 1035 */
BgL_test6669z00_14337 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==111L))
{ /* Llib/date.scm 1035 */
BgL_test6669z00_14337 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6669z00_14337 = 
(
(long)(BgL_curz00_11532)==110L)
; } } 
if(BgL_test6669z00_14337)
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==114L))
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6673z00_14349;
if(
(
(long)(BgL_curz00_11532)==118L))
{ /* Llib/date.scm 1035 */
BgL_test6673z00_14349 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11532)==117L))
{ /* Llib/date.scm 1035 */
BgL_test6673z00_14349 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6673z00_14349 = 
(
(long)(BgL_curz00_11532)==116L)
; } } 
if(BgL_test6673z00_14349)
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6662z00_14318 = 
(
(long)(BgL_curz00_11532)==121L)
; } } } } } } } } 
if(BgL_test6662z00_14318)
{ /* Llib/date.scm 1035 */
BgL_iportz00_11507 = BgL_iportz00_11521; 
BgL_lastzd2matchzd2_11508 = BgL_newzd2matchzd2_11525; 
BgL_forwardz00_11509 = 
(1L+BgL_forwardz00_11523); 
BgL_bufposz00_11510 = BgL_bufposz00_11524; 
BgL_statezd24zd21180z00_11481:
if(
(BgL_forwardz00_11509==BgL_bufposz00_11510))
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6677z00_14362;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11511;
if(
INPUT_PORTP(BgL_iportz00_11507))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11511 = BgL_iportz00_11507; }  else 
{ 
 obj_t BgL_auxz00_14365;
BgL_auxz00_14365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5853z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11507); 
FAILURE(BgL_auxz00_14365,BFALSE,BFALSE);} 
BgL_test6677z00_14362 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11511); } 
if(BgL_test6677z00_14362)
{ /* Llib/date.scm 1035 */
 long BgL_arg1936z00_11512; long BgL_arg1937z00_11513;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11514;
if(
INPUT_PORTP(BgL_iportz00_11507))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11514 = BgL_iportz00_11507; }  else 
{ 
 obj_t BgL_auxz00_14372;
BgL_auxz00_14372 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5853z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11507); 
FAILURE(BgL_auxz00_14372,BFALSE,BFALSE);} 
BgL_arg1936z00_11512 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11514); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11515;
if(
INPUT_PORTP(BgL_iportz00_11507))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11515 = BgL_iportz00_11507; }  else 
{ 
 obj_t BgL_auxz00_14379;
BgL_auxz00_14379 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5853z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11507); 
FAILURE(BgL_auxz00_14379,BFALSE,BFALSE);} 
BgL_arg1937z00_11513 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11515); } 
{ 
 long BgL_bufposz00_14385; long BgL_forwardz00_14384;
BgL_forwardz00_14384 = BgL_arg1936z00_11512; 
BgL_bufposz00_14385 = BgL_arg1937z00_11513; 
BgL_bufposz00_11510 = BgL_bufposz00_14385; 
BgL_forwardz00_11509 = BgL_forwardz00_14384; 
goto BgL_statezd24zd21180z00_11481;} }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_lastzd2matchzd2_11508; } }  else 
{ /* Llib/date.scm 1035 */
 int BgL_curz00_11516;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11517;
if(
INPUT_PORTP(BgL_iportz00_11507))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11517 = BgL_iportz00_11507; }  else 
{ 
 obj_t BgL_auxz00_14388;
BgL_auxz00_14388 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5853z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11507); 
FAILURE(BgL_auxz00_14388,BFALSE,BFALSE);} 
BgL_curz00_11516 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11517, BgL_forwardz00_11509); } 
{ /* Llib/date.scm 1035 */

{ /* Llib/date.scm 1035 */
 bool_t BgL_test6682z00_14393;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6683z00_14394;
if(
(
(long)(BgL_curz00_11516)==99L))
{ /* Llib/date.scm 1035 */
BgL_test6683z00_14394 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==98L))
{ /* Llib/date.scm 1035 */
BgL_test6683z00_14394 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6683z00_14394 = 
(
(long)(BgL_curz00_11516)==97L)
; } } 
if(BgL_test6683z00_14394)
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==101L))
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==103L))
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==108L))
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6689z00_14412;
if(
(
(long)(BgL_curz00_11516)==112L))
{ /* Llib/date.scm 1035 */
BgL_test6689z00_14412 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==111L))
{ /* Llib/date.scm 1035 */
BgL_test6689z00_14412 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6689z00_14412 = 
(
(long)(BgL_curz00_11516)==110L)
; } } 
if(BgL_test6689z00_14412)
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==114L))
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6693z00_14424;
if(
(
(long)(BgL_curz00_11516)==118L))
{ /* Llib/date.scm 1035 */
BgL_test6693z00_14424 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11516)==117L))
{ /* Llib/date.scm 1035 */
BgL_test6693z00_14424 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6693z00_14424 = 
(
(long)(BgL_curz00_11516)==116L)
; } } 
if(BgL_test6693z00_14424)
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6682z00_14393 = 
(
(long)(BgL_curz00_11516)==121L)
; } } } } } } } } 
if(BgL_test6682z00_14393)
{ /* Llib/date.scm 1035 */
 long BgL_arg1952z00_11518;
BgL_arg1952z00_11518 = 
(1L+BgL_forwardz00_11509); 
{ /* Llib/date.scm 1035 */
 long BgL_newzd2matchzd2_11519;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11520;
if(
INPUT_PORTP(BgL_iportz00_11507))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11520 = BgL_iportz00_11507; }  else 
{ 
 obj_t BgL_auxz00_14438;
BgL_auxz00_14438 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5853z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11507); 
FAILURE(BgL_auxz00_14438,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11520, BgL_arg1952z00_11518); } 
BgL_newzd2matchzd2_11519 = 1L; 
BgL_matchz00_11487 = BgL_newzd2matchzd2_11519; } }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_lastzd2matchzd2_11508; } } } } }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_newzd2matchzd2_11525; } } } } } }  else 
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6697z00_14445;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6698z00_14446;
if(
(
(long)(BgL_curz00_11569)==10L))
{ /* Llib/date.scm 1035 */
BgL_test6698z00_14446 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6698z00_14446 = 
(
(long)(BgL_curz00_11569)==9L)
; } 
if(BgL_test6698z00_14446)
{ /* Llib/date.scm 1035 */
BgL_test6697z00_14445 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11569)==13L))
{ /* Llib/date.scm 1035 */
BgL_test6697z00_14445 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6697z00_14445 = 
(
(long)(BgL_curz00_11569)==32L)
; } } } 
if(BgL_test6697z00_14445)
{ /* Llib/date.scm 1035 */
BgL_iportz00_11547 = BgL_iportz00_11560; 
BgL_lastzd2matchzd2_11548 = BgL_lastzd2matchzd2_11561; 
BgL_forwardz00_11549 = 
(1L+BgL_forwardz00_11562); 
BgL_bufposz00_11550 = BgL_bufposz00_11563; 
BgL_statezd22zd21178z00_11484:
{ /* Llib/date.scm 1035 */
 long BgL_newzd2matchzd2_11551;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11552;
if(
INPUT_PORTP(BgL_iportz00_11547))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11552 = BgL_iportz00_11547; }  else 
{ 
 obj_t BgL_auxz00_14459;
BgL_auxz00_14459 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5856z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11547); 
FAILURE(BgL_auxz00_14459,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11552, BgL_forwardz00_11549); } 
BgL_newzd2matchzd2_11551 = 0L; 
if(
(BgL_forwardz00_11549==BgL_bufposz00_11550))
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6703z00_14466;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11553;
if(
INPUT_PORTP(BgL_iportz00_11547))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11553 = BgL_iportz00_11547; }  else 
{ 
 obj_t BgL_auxz00_14469;
BgL_auxz00_14469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5856z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11547); 
FAILURE(BgL_auxz00_14469,BFALSE,BFALSE);} 
BgL_test6703z00_14466 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11553); } 
if(BgL_test6703z00_14466)
{ /* Llib/date.scm 1035 */
 long BgL_arg1892z00_11554; long BgL_arg1893z00_11555;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11556;
if(
INPUT_PORTP(BgL_iportz00_11547))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11556 = BgL_iportz00_11547; }  else 
{ 
 obj_t BgL_auxz00_14476;
BgL_auxz00_14476 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5856z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11547); 
FAILURE(BgL_auxz00_14476,BFALSE,BFALSE);} 
BgL_arg1892z00_11554 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11556); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11557;
if(
INPUT_PORTP(BgL_iportz00_11547))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11557 = BgL_iportz00_11547; }  else 
{ 
 obj_t BgL_auxz00_14483;
BgL_auxz00_14483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5856z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11547); 
FAILURE(BgL_auxz00_14483,BFALSE,BFALSE);} 
BgL_arg1893z00_11555 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11557); } 
{ 
 long BgL_bufposz00_14489; long BgL_forwardz00_14488;
BgL_forwardz00_14488 = BgL_arg1892z00_11554; 
BgL_bufposz00_14489 = BgL_arg1893z00_11555; 
BgL_bufposz00_11550 = BgL_bufposz00_14489; 
BgL_forwardz00_11549 = BgL_forwardz00_14488; 
goto BgL_statezd22zd21178z00_11484;} }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_newzd2matchzd2_11551; } }  else 
{ /* Llib/date.scm 1035 */
 int BgL_curz00_11558;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11559;
if(
INPUT_PORTP(BgL_iportz00_11547))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11559 = BgL_iportz00_11547; }  else 
{ 
 obj_t BgL_auxz00_14492;
BgL_auxz00_14492 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5856z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11547); 
FAILURE(BgL_auxz00_14492,BFALSE,BFALSE);} 
BgL_curz00_11558 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11559, BgL_forwardz00_11549); } 
{ /* Llib/date.scm 1035 */

{ /* Llib/date.scm 1035 */
 bool_t BgL_test6708z00_14497;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6709z00_14498;
if(
(
(long)(BgL_curz00_11558)==10L))
{ /* Llib/date.scm 1035 */
BgL_test6709z00_14498 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6709z00_14498 = 
(
(long)(BgL_curz00_11558)==9L)
; } 
if(BgL_test6709z00_14498)
{ /* Llib/date.scm 1035 */
BgL_test6708z00_14497 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11558)==13L))
{ /* Llib/date.scm 1035 */
BgL_test6708z00_14497 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6708z00_14497 = 
(
(long)(BgL_curz00_11558)==32L)
; } } } 
if(BgL_test6708z00_14497)
{ /* Llib/date.scm 1035 */
BgL_iportz00_11534 = BgL_iportz00_11547; 
BgL_lastzd2matchzd2_11535 = BgL_newzd2matchzd2_11551; 
BgL_forwardz00_11536 = 
(1L+BgL_forwardz00_11549); 
BgL_bufposz00_11537 = BgL_bufposz00_11550; 
BgL_statezd27zd21183z00_11483:
{ /* Llib/date.scm 1035 */
 long BgL_newzd2matchzd2_11538;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11539;
if(
INPUT_PORTP(BgL_iportz00_11534))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11539 = BgL_iportz00_11534; }  else 
{ 
 obj_t BgL_auxz00_14511;
BgL_auxz00_14511 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5855z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11534); 
FAILURE(BgL_auxz00_14511,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11539, BgL_forwardz00_11536); } 
BgL_newzd2matchzd2_11538 = 0L; 
if(
(BgL_forwardz00_11536==BgL_bufposz00_11537))
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6714z00_14518;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11540;
if(
INPUT_PORTP(BgL_iportz00_11534))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11540 = BgL_iportz00_11534; }  else 
{ 
 obj_t BgL_auxz00_14521;
BgL_auxz00_14521 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5855z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11534); 
FAILURE(BgL_auxz00_14521,BFALSE,BFALSE);} 
BgL_test6714z00_14518 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11540); } 
if(BgL_test6714z00_14518)
{ /* Llib/date.scm 1035 */
 long BgL_arg1902z00_11541; long BgL_arg1903z00_11542;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11543;
if(
INPUT_PORTP(BgL_iportz00_11534))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11543 = BgL_iportz00_11534; }  else 
{ 
 obj_t BgL_auxz00_14528;
BgL_auxz00_14528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5855z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11534); 
FAILURE(BgL_auxz00_14528,BFALSE,BFALSE);} 
BgL_arg1902z00_11541 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11543); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11544;
if(
INPUT_PORTP(BgL_iportz00_11534))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11544 = BgL_iportz00_11534; }  else 
{ 
 obj_t BgL_auxz00_14535;
BgL_auxz00_14535 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5855z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11534); 
FAILURE(BgL_auxz00_14535,BFALSE,BFALSE);} 
BgL_arg1903z00_11542 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11544); } 
{ 
 long BgL_bufposz00_14541; long BgL_forwardz00_14540;
BgL_forwardz00_14540 = BgL_arg1902z00_11541; 
BgL_bufposz00_14541 = BgL_arg1903z00_11542; 
BgL_bufposz00_11537 = BgL_bufposz00_14541; 
BgL_forwardz00_11536 = BgL_forwardz00_14540; 
goto BgL_statezd27zd21183z00_11483;} }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_newzd2matchzd2_11538; } }  else 
{ /* Llib/date.scm 1035 */
 int BgL_curz00_11545;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11546;
if(
INPUT_PORTP(BgL_iportz00_11534))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11546 = BgL_iportz00_11534; }  else 
{ 
 obj_t BgL_auxz00_14544;
BgL_auxz00_14544 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5855z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11534); 
FAILURE(BgL_auxz00_14544,BFALSE,BFALSE);} 
BgL_curz00_11545 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11546, BgL_forwardz00_11536); } 
{ /* Llib/date.scm 1035 */

{ /* Llib/date.scm 1035 */
 bool_t BgL_test6719z00_14549;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6720z00_14550;
if(
(
(long)(BgL_curz00_11545)==10L))
{ /* Llib/date.scm 1035 */
BgL_test6720z00_14550 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6720z00_14550 = 
(
(long)(BgL_curz00_11545)==9L)
; } 
if(BgL_test6720z00_14550)
{ /* Llib/date.scm 1035 */
BgL_test6719z00_14549 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
if(
(
(long)(BgL_curz00_11545)==13L))
{ /* Llib/date.scm 1035 */
BgL_test6719z00_14549 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1035 */
BgL_test6719z00_14549 = 
(
(long)(BgL_curz00_11545)==32L)
; } } } 
if(BgL_test6719z00_14549)
{ 
 long BgL_forwardz00_14562; long BgL_lastzd2matchzd2_14561;
BgL_lastzd2matchzd2_14561 = BgL_newzd2matchzd2_11538; 
BgL_forwardz00_14562 = 
(1L+BgL_forwardz00_11536); 
BgL_forwardz00_11536 = BgL_forwardz00_14562; 
BgL_lastzd2matchzd2_11535 = BgL_lastzd2matchzd2_14561; 
goto BgL_statezd27zd21183z00_11483;}  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_newzd2matchzd2_11538; } } } } } }  else 
{ /* Llib/date.scm 1035 */
BgL_matchz00_11487 = BgL_newzd2matchzd2_11551; } } } } } }  else 
{ /* Llib/date.scm 1035 */
 long BgL_arg1888z00_11571;
BgL_arg1888z00_11571 = 
(1L+BgL_forwardz00_11562); 
{ /* Llib/date.scm 1035 */
 long BgL_newzd2matchzd2_11572;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11573;
if(
INPUT_PORTP(BgL_iportz00_11560))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11573 = BgL_iportz00_11560; }  else 
{ 
 obj_t BgL_auxz00_14569;
BgL_auxz00_14569 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5857z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11560); 
FAILURE(BgL_auxz00_14569,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11573, BgL_arg1888z00_11571); } 
BgL_newzd2matchzd2_11572 = 2L; 
BgL_matchz00_11487 = BgL_newzd2matchzd2_11572; } } } } } } } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11492;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11492 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14576;
BgL_auxz00_14576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14576,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_11492); } 
switch( BgL_matchz00_11487) { case 2L : 

{ /* Llib/date.scm 1057 */
 obj_t BgL_arg2044z00_11493; obj_t BgL_arg2045z00_11494;
{ /* Llib/date.scm 1035 */
 bool_t BgL_test6725z00_14581;
{ /* Llib/date.scm 1035 */
 long BgL_arg2036z00_11504;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11505;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11505 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14584;
BgL_auxz00_14584 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14584,BFALSE,BFALSE);} 
BgL_arg2036z00_11504 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_11505); } 
BgL_test6725z00_14581 = 
(BgL_arg2036z00_11504==0L); } 
if(BgL_test6725z00_14581)
{ /* Llib/date.scm 1035 */
BgL_arg2044z00_11493 = BEOF; }  else 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11506;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11506 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14592;
BgL_auxz00_14592 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14592,BFALSE,BFALSE);} 
BgL_arg2044z00_11493 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_11506)); } } 
{ /* Llib/date.scm 1057 */
 obj_t BgL_res4734z00_11495;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_res4734z00_11495 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14600;
BgL_auxz00_14600 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14600,BFALSE,BFALSE);} 
BgL_arg2045z00_11494 = BgL_res4734z00_11495; } 
return 
BGl_parsezd2errorzd2zz__datez00(BGl_string5787z00zz__datez00, BGl_string5828z00zz__datez00, BgL_arg2044z00_11493, BgL_arg2045z00_11494);} break;case 1L : 

{ /* Llib/date.scm 1039 */
 obj_t BgL_casezd2valuezd2_11496;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11497;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11497 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14607;
BgL_auxz00_14607 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14607,BFALSE,BFALSE);} 
BgL_casezd2valuezd2_11496 = 
rgc_buffer_symbol(BgL_inputzd2portzd2_11497); } 
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5829z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(1L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5831z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(2L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5833z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(3L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5835z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(4L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5837z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(5L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5839z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(6L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5841z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(7L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5843z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(8L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5845z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(9L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5847z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(10L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5849z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(11L);}  else 
{ /* Llib/date.scm 1039 */
if(
(BgL_casezd2valuezd2_11496==BGl_symbol5851z00zz__datez00))
{ /* Llib/date.scm 1039 */
return 
BINT(12L);}  else 
{ /* Llib/date.scm 1054 */
 obj_t BgL_arg2058z00_11498; obj_t BgL_arg2059z00_11499;
{ /* Llib/date.scm 1035 */
 long BgL_arg1960z00_11500;
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11501;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11501 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14650;
BgL_auxz00_14650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14650,BFALSE,BFALSE);} 
BgL_arg1960z00_11500 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_11501); } 
{ /* Llib/date.scm 1035 */
 obj_t BgL_inputzd2portzd2_11502;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_inputzd2portzd2_11502 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14657;
BgL_auxz00_14657 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14657,BFALSE,BFALSE);} 
BgL_arg2058z00_11498 = 
rgc_buffer_substring(BgL_inputzd2portzd2_11502, 0L, BgL_arg1960z00_11500); } } 
{ /* Llib/date.scm 1054 */
 obj_t BgL_res4735z00_11503;
if(
INPUT_PORTP(BgL_iportz00_9764))
{ /* Llib/date.scm 1035 */
BgL_res4735z00_11503 = BgL_iportz00_9764; }  else 
{ 
 obj_t BgL_auxz00_14664;
BgL_auxz00_14664 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(43080L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9764); 
FAILURE(BgL_auxz00_14664,BFALSE,BFALSE);} 
BgL_arg2059z00_11499 = BgL_res4735z00_11503; } 
return 
BGl_parsezd2errorzd2zz__datez00(BGl_string5787z00zz__datez00, BGl_string5828z00zz__datez00, BgL_arg2058z00_11498, BgL_arg2059z00_11499);} } } } } } } } } } } } } break;case 0L : 

goto BgL_ignorez00_11479;break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_11487));} } } } } } 

}



/* &<@anonymous:1702> */
obj_t BGl_z62zc3z04anonymousza31702ze3ze5zz__datez00(obj_t BgL_envz00_9765, obj_t BgL_iportz00_9766)
{
{ /* Llib/date.scm 1021 */
{ 
 obj_t BgL_iportz00_11657; long BgL_lastzd2matchzd2_11658; long BgL_forwardz00_11659; long BgL_bufposz00_11660; obj_t BgL_iportz00_11644; long BgL_lastzd2matchzd2_11645; long BgL_forwardz00_11646; long BgL_bufposz00_11647; obj_t BgL_iportz00_11631; long BgL_lastzd2matchzd2_11632; long BgL_forwardz00_11633; long BgL_bufposz00_11634; obj_t BgL_iportz00_11618; long BgL_lastzd2matchzd2_11619; long BgL_forwardz00_11620; long BgL_bufposz00_11621; obj_t BgL_iportz00_11605; long BgL_lastzd2matchzd2_11606; long BgL_forwardz00_11607; long BgL_bufposz00_11608;
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6745z00_14672;
{ /* Llib/date.scm 1021 */
 obj_t BgL_arg1707z00_11671;
{ /* Llib/date.scm 1021 */
 obj_t BgL_res4732z00_11672;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_res4732z00_11672 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_14675;
BgL_auxz00_14675 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5865z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_14675,BFALSE,BFALSE);} 
BgL_arg1707z00_11671 = BgL_res4732z00_11672; } 
BgL_test6745z00_14672 = 
INPUT_PORT_CLOSEP(BgL_arg1707z00_11671); } 
if(BgL_test6745z00_14672)
{ /* Llib/date.scm 1021 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1703z00_11673;
{ /* Llib/date.scm 1021 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_11674;
{ /* Llib/date.scm 1021 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_11675;
BgL_new1077z00_11675 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 1021 */
 long BgL_arg1706z00_11676;
BgL_arg1706z00_11676 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_11675), BgL_arg1706z00_11676); } 
BgL_new1078z00_11674 = BgL_new1077z00_11675; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11674)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11674)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_14688;
{ /* Llib/date.scm 1021 */
 obj_t BgL_arg1704z00_11677;
{ /* Llib/date.scm 1021 */
 obj_t BgL_arg1705z00_11678;
{ /* Llib/date.scm 1021 */
 obj_t BgL_classz00_11679;
BgL_classz00_11679 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1705z00_11678 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_11679); } 
BgL_arg1704z00_11677 = 
VECTOR_REF(BgL_arg1705z00_11678,2L); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_auxz00_14692;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1704z00_11677))
{ /* Llib/date.scm 1021 */
BgL_auxz00_14692 = BgL_arg1704z00_11677
; }  else 
{ 
 obj_t BgL_auxz00_14695;
BgL_auxz00_14695 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5865z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg1704z00_11677); 
FAILURE(BgL_auxz00_14695,BFALSE,BFALSE);} 
BgL_auxz00_14688 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_14692); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11674)))->BgL_stackz00)=((obj_t)BgL_auxz00_14688),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11674)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11674)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
{ 
 obj_t BgL_auxz00_14705;
{ /* Llib/date.scm 1021 */
 obj_t BgL_res4733z00_11680;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_res4733z00_11680 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_14709;
BgL_auxz00_14709 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5865z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_14709,BFALSE,BFALSE);} 
BgL_auxz00_14705 = BgL_res4733z00_11680; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11674)))->BgL_objz00)=((obj_t)BgL_auxz00_14705),BUNSPEC); } 
BgL_arg1703z00_11673 = BgL_new1078z00_11674; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1703z00_11673));}  else 
{ /* Llib/date.scm 1021 */
BgL_ignorez00_11584:
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11591;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11591 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_14718;
BgL_auxz00_14718 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_14718,BFALSE,BFALSE);} 
RGC_START_MATCH(BgL_inputzd2portzd2_11591); } 
{ /* Llib/date.scm 1021 */
 long BgL_matchz00_11592;
{ /* Llib/date.scm 1021 */
 long BgL_arg1857z00_11593; long BgL_arg1858z00_11594;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11595;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11595 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_14725;
BgL_auxz00_14725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_14725,BFALSE,BFALSE);} 
BgL_arg1857z00_11593 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11595); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11596;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11596 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_14732;
BgL_auxz00_14732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_14732,BFALSE,BFALSE);} 
BgL_arg1858z00_11594 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11596); } 
BgL_iportz00_11657 = BgL_iportz00_9766; 
BgL_lastzd2matchzd2_11658 = 2L; 
BgL_forwardz00_11659 = BgL_arg1857z00_11593; 
BgL_bufposz00_11660 = BgL_arg1858z00_11594; 
BgL_statezd20zd21167z00_11590:
if(
(BgL_forwardz00_11659==BgL_bufposz00_11660))
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6753z00_14739;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11661;
if(
INPUT_PORTP(BgL_iportz00_11657))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11661 = BgL_iportz00_11657; }  else 
{ 
 obj_t BgL_auxz00_14742;
BgL_auxz00_14742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5864z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11657); 
FAILURE(BgL_auxz00_14742,BFALSE,BFALSE);} 
BgL_test6753z00_14739 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11661); } 
if(BgL_test6753z00_14739)
{ /* Llib/date.scm 1021 */
 long BgL_arg1711z00_11662; long BgL_arg1714z00_11663;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11664;
if(
INPUT_PORTP(BgL_iportz00_11657))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11664 = BgL_iportz00_11657; }  else 
{ 
 obj_t BgL_auxz00_14749;
BgL_auxz00_14749 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5864z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11657); 
FAILURE(BgL_auxz00_14749,BFALSE,BFALSE);} 
BgL_arg1711z00_11662 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11664); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11665;
if(
INPUT_PORTP(BgL_iportz00_11657))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11665 = BgL_iportz00_11657; }  else 
{ 
 obj_t BgL_auxz00_14756;
BgL_auxz00_14756 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5864z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11657); 
FAILURE(BgL_auxz00_14756,BFALSE,BFALSE);} 
BgL_arg1714z00_11663 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11665); } 
{ 
 long BgL_bufposz00_14762; long BgL_forwardz00_14761;
BgL_forwardz00_14761 = BgL_arg1711z00_11662; 
BgL_bufposz00_14762 = BgL_arg1714z00_11663; 
BgL_bufposz00_11660 = BgL_bufposz00_14762; 
BgL_forwardz00_11659 = BgL_forwardz00_14761; 
goto BgL_statezd20zd21167z00_11590;} }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_lastzd2matchzd2_11658; } }  else 
{ /* Llib/date.scm 1021 */
 int BgL_curz00_11666;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11667;
if(
INPUT_PORTP(BgL_iportz00_11657))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11667 = BgL_iportz00_11657; }  else 
{ 
 obj_t BgL_auxz00_14765;
BgL_auxz00_14765 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5864z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11657); 
FAILURE(BgL_auxz00_14765,BFALSE,BFALSE);} 
BgL_curz00_11666 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11667, BgL_forwardz00_11659); } 
{ /* Llib/date.scm 1021 */

{ /* Llib/date.scm 1021 */
 bool_t BgL_test6758z00_14770;
if(
(
(long)(BgL_curz00_11666)>=48L))
{ /* Llib/date.scm 1021 */
BgL_test6758z00_14770 = 
(
(long)(BgL_curz00_11666)<58L)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6758z00_14770 = ((bool_t)0)
; } 
if(BgL_test6758z00_14770)
{ /* Llib/date.scm 1021 */
BgL_iportz00_11631 = BgL_iportz00_11657; 
BgL_lastzd2matchzd2_11632 = BgL_lastzd2matchzd2_11658; 
BgL_forwardz00_11633 = 
(1L+BgL_forwardz00_11659); 
BgL_bufposz00_11634 = BgL_bufposz00_11660; 
BgL_statezd23zd21170z00_11588:
{ /* Llib/date.scm 1021 */
 long BgL_newzd2matchzd2_11635;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11636;
if(
INPUT_PORTP(BgL_iportz00_11631))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11636 = BgL_iportz00_11631; }  else 
{ 
 obj_t BgL_auxz00_14778;
BgL_auxz00_14778 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5862z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11631); 
FAILURE(BgL_auxz00_14778,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11636, BgL_forwardz00_11633); } 
BgL_newzd2matchzd2_11635 = 1L; 
if(
(BgL_forwardz00_11633==BgL_bufposz00_11634))
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6762z00_14785;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11637;
if(
INPUT_PORTP(BgL_iportz00_11631))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11637 = BgL_iportz00_11631; }  else 
{ 
 obj_t BgL_auxz00_14788;
BgL_auxz00_14788 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5862z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11631); 
FAILURE(BgL_auxz00_14788,BFALSE,BFALSE);} 
BgL_test6762z00_14785 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11637); } 
if(BgL_test6762z00_14785)
{ /* Llib/date.scm 1021 */
 long BgL_arg1738z00_11638; long BgL_arg1739z00_11639;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11640;
if(
INPUT_PORTP(BgL_iportz00_11631))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11640 = BgL_iportz00_11631; }  else 
{ 
 obj_t BgL_auxz00_14795;
BgL_auxz00_14795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5862z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11631); 
FAILURE(BgL_auxz00_14795,BFALSE,BFALSE);} 
BgL_arg1738z00_11638 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11640); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11641;
if(
INPUT_PORTP(BgL_iportz00_11631))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11641 = BgL_iportz00_11631; }  else 
{ 
 obj_t BgL_auxz00_14802;
BgL_auxz00_14802 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5862z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11631); 
FAILURE(BgL_auxz00_14802,BFALSE,BFALSE);} 
BgL_arg1739z00_11639 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11641); } 
{ 
 long BgL_bufposz00_14808; long BgL_forwardz00_14807;
BgL_forwardz00_14807 = BgL_arg1738z00_11638; 
BgL_bufposz00_14808 = BgL_arg1739z00_11639; 
BgL_bufposz00_11634 = BgL_bufposz00_14808; 
BgL_forwardz00_11633 = BgL_forwardz00_14807; 
goto BgL_statezd23zd21170z00_11588;} }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11635; } }  else 
{ /* Llib/date.scm 1021 */
 int BgL_curz00_11642;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11643;
if(
INPUT_PORTP(BgL_iportz00_11631))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11643 = BgL_iportz00_11631; }  else 
{ 
 obj_t BgL_auxz00_14811;
BgL_auxz00_14811 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5862z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11631); 
FAILURE(BgL_auxz00_14811,BFALSE,BFALSE);} 
BgL_curz00_11642 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11643, BgL_forwardz00_11633); } 
{ /* Llib/date.scm 1021 */

{ /* Llib/date.scm 1021 */
 bool_t BgL_test6767z00_14816;
if(
(
(long)(BgL_curz00_11642)>=48L))
{ /* Llib/date.scm 1021 */
BgL_test6767z00_14816 = 
(
(long)(BgL_curz00_11642)<58L)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6767z00_14816 = ((bool_t)0)
; } 
if(BgL_test6767z00_14816)
{ /* Llib/date.scm 1021 */
BgL_iportz00_11618 = BgL_iportz00_11631; 
BgL_lastzd2matchzd2_11619 = BgL_newzd2matchzd2_11635; 
BgL_forwardz00_11620 = 
(1L+BgL_forwardz00_11633); 
BgL_bufposz00_11621 = BgL_bufposz00_11634; 
BgL_statezd24zd21171z00_11587:
{ /* Llib/date.scm 1021 */
 long BgL_newzd2matchzd2_11622;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11623;
if(
INPUT_PORTP(BgL_iportz00_11618))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11623 = BgL_iportz00_11618; }  else 
{ 
 obj_t BgL_auxz00_14824;
BgL_auxz00_14824 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5861z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11618); 
FAILURE(BgL_auxz00_14824,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11623, BgL_forwardz00_11620); } 
BgL_newzd2matchzd2_11622 = 1L; 
if(
(BgL_forwardz00_11620==BgL_bufposz00_11621))
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6771z00_14831;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11624;
if(
INPUT_PORTP(BgL_iportz00_11618))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11624 = BgL_iportz00_11618; }  else 
{ 
 obj_t BgL_auxz00_14834;
BgL_auxz00_14834 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5861z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11618); 
FAILURE(BgL_auxz00_14834,BFALSE,BFALSE);} 
BgL_test6771z00_14831 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11624); } 
if(BgL_test6771z00_14831)
{ /* Llib/date.scm 1021 */
 long BgL_arg1747z00_11625; long BgL_arg1748z00_11626;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11627;
if(
INPUT_PORTP(BgL_iportz00_11618))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11627 = BgL_iportz00_11618; }  else 
{ 
 obj_t BgL_auxz00_14841;
BgL_auxz00_14841 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5861z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11618); 
FAILURE(BgL_auxz00_14841,BFALSE,BFALSE);} 
BgL_arg1747z00_11625 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11627); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11628;
if(
INPUT_PORTP(BgL_iportz00_11618))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11628 = BgL_iportz00_11618; }  else 
{ 
 obj_t BgL_auxz00_14848;
BgL_auxz00_14848 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5861z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11618); 
FAILURE(BgL_auxz00_14848,BFALSE,BFALSE);} 
BgL_arg1748z00_11626 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11628); } 
{ 
 long BgL_bufposz00_14854; long BgL_forwardz00_14853;
BgL_forwardz00_14853 = BgL_arg1747z00_11625; 
BgL_bufposz00_14854 = BgL_arg1748z00_11626; 
BgL_bufposz00_11621 = BgL_bufposz00_14854; 
BgL_forwardz00_11620 = BgL_forwardz00_14853; 
goto BgL_statezd24zd21171z00_11587;} }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11622; } }  else 
{ /* Llib/date.scm 1021 */
 int BgL_curz00_11629;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11630;
if(
INPUT_PORTP(BgL_iportz00_11618))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11630 = BgL_iportz00_11618; }  else 
{ 
 obj_t BgL_auxz00_14857;
BgL_auxz00_14857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5861z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11618); 
FAILURE(BgL_auxz00_14857,BFALSE,BFALSE);} 
BgL_curz00_11629 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11630, BgL_forwardz00_11620); } 
{ /* Llib/date.scm 1021 */

{ /* Llib/date.scm 1021 */
 bool_t BgL_test6776z00_14862;
if(
(
(long)(BgL_curz00_11629)>=48L))
{ /* Llib/date.scm 1021 */
BgL_test6776z00_14862 = 
(
(long)(BgL_curz00_11629)<58L)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6776z00_14862 = ((bool_t)0)
; } 
if(BgL_test6776z00_14862)
{ 
 long BgL_forwardz00_14869; long BgL_lastzd2matchzd2_14868;
BgL_lastzd2matchzd2_14868 = BgL_newzd2matchzd2_11622; 
BgL_forwardz00_14869 = 
(1L+BgL_forwardz00_11620); 
BgL_forwardz00_11620 = BgL_forwardz00_14869; 
BgL_lastzd2matchzd2_11619 = BgL_lastzd2matchzd2_14868; 
goto BgL_statezd24zd21171z00_11587;}  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11622; } } } } } }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11635; } } } } } }  else 
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6778z00_14873;
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6779z00_14874;
if(
(
(long)(BgL_curz00_11666)==10L))
{ /* Llib/date.scm 1021 */
BgL_test6779z00_14874 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6779z00_14874 = 
(
(long)(BgL_curz00_11666)==9L)
; } 
if(BgL_test6779z00_14874)
{ /* Llib/date.scm 1021 */
BgL_test6778z00_14873 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
if(
(
(long)(BgL_curz00_11666)==13L))
{ /* Llib/date.scm 1021 */
BgL_test6778z00_14873 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6778z00_14873 = 
(
(long)(BgL_curz00_11666)==32L)
; } } } 
if(BgL_test6778z00_14873)
{ /* Llib/date.scm 1021 */
BgL_iportz00_11644 = BgL_iportz00_11657; 
BgL_lastzd2matchzd2_11645 = BgL_lastzd2matchzd2_11658; 
BgL_forwardz00_11646 = 
(1L+BgL_forwardz00_11659); 
BgL_bufposz00_11647 = BgL_bufposz00_11660; 
BgL_statezd22zd21169z00_11589:
{ /* Llib/date.scm 1021 */
 long BgL_newzd2matchzd2_11648;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11649;
if(
INPUT_PORTP(BgL_iportz00_11644))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11649 = BgL_iportz00_11644; }  else 
{ 
 obj_t BgL_auxz00_14887;
BgL_auxz00_14887 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5863z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11644); 
FAILURE(BgL_auxz00_14887,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11649, BgL_forwardz00_11646); } 
BgL_newzd2matchzd2_11648 = 0L; 
if(
(BgL_forwardz00_11646==BgL_bufposz00_11647))
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6784z00_14894;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11650;
if(
INPUT_PORTP(BgL_iportz00_11644))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11650 = BgL_iportz00_11644; }  else 
{ 
 obj_t BgL_auxz00_14897;
BgL_auxz00_14897 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5863z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11644); 
FAILURE(BgL_auxz00_14897,BFALSE,BFALSE);} 
BgL_test6784z00_14894 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11650); } 
if(BgL_test6784z00_14894)
{ /* Llib/date.scm 1021 */
 long BgL_arg1728z00_11651; long BgL_arg1729z00_11652;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11653;
if(
INPUT_PORTP(BgL_iportz00_11644))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11653 = BgL_iportz00_11644; }  else 
{ 
 obj_t BgL_auxz00_14904;
BgL_auxz00_14904 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5863z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11644); 
FAILURE(BgL_auxz00_14904,BFALSE,BFALSE);} 
BgL_arg1728z00_11651 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11653); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11654;
if(
INPUT_PORTP(BgL_iportz00_11644))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11654 = BgL_iportz00_11644; }  else 
{ 
 obj_t BgL_auxz00_14911;
BgL_auxz00_14911 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5863z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11644); 
FAILURE(BgL_auxz00_14911,BFALSE,BFALSE);} 
BgL_arg1729z00_11652 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11654); } 
{ 
 long BgL_bufposz00_14917; long BgL_forwardz00_14916;
BgL_forwardz00_14916 = BgL_arg1728z00_11651; 
BgL_bufposz00_14917 = BgL_arg1729z00_11652; 
BgL_bufposz00_11647 = BgL_bufposz00_14917; 
BgL_forwardz00_11646 = BgL_forwardz00_14916; 
goto BgL_statezd22zd21169z00_11589;} }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11648; } }  else 
{ /* Llib/date.scm 1021 */
 int BgL_curz00_11655;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11656;
if(
INPUT_PORTP(BgL_iportz00_11644))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11656 = BgL_iportz00_11644; }  else 
{ 
 obj_t BgL_auxz00_14920;
BgL_auxz00_14920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5863z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11644); 
FAILURE(BgL_auxz00_14920,BFALSE,BFALSE);} 
BgL_curz00_11655 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11656, BgL_forwardz00_11646); } 
{ /* Llib/date.scm 1021 */

{ /* Llib/date.scm 1021 */
 bool_t BgL_test6789z00_14925;
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6790z00_14926;
if(
(
(long)(BgL_curz00_11655)==10L))
{ /* Llib/date.scm 1021 */
BgL_test6790z00_14926 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6790z00_14926 = 
(
(long)(BgL_curz00_11655)==9L)
; } 
if(BgL_test6790z00_14926)
{ /* Llib/date.scm 1021 */
BgL_test6789z00_14925 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
if(
(
(long)(BgL_curz00_11655)==13L))
{ /* Llib/date.scm 1021 */
BgL_test6789z00_14925 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6789z00_14925 = 
(
(long)(BgL_curz00_11655)==32L)
; } } } 
if(BgL_test6789z00_14925)
{ /* Llib/date.scm 1021 */
BgL_iportz00_11605 = BgL_iportz00_11644; 
BgL_lastzd2matchzd2_11606 = BgL_newzd2matchzd2_11648; 
BgL_forwardz00_11607 = 
(1L+BgL_forwardz00_11646); 
BgL_bufposz00_11608 = BgL_bufposz00_11647; 
BgL_statezd26zd21173z00_11586:
{ /* Llib/date.scm 1021 */
 long BgL_newzd2matchzd2_11609;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11610;
if(
INPUT_PORTP(BgL_iportz00_11605))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11610 = BgL_iportz00_11605; }  else 
{ 
 obj_t BgL_auxz00_14939;
BgL_auxz00_14939 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5860z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11605); 
FAILURE(BgL_auxz00_14939,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11610, BgL_forwardz00_11607); } 
BgL_newzd2matchzd2_11609 = 0L; 
if(
(BgL_forwardz00_11607==BgL_bufposz00_11608))
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6795z00_14946;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11611;
if(
INPUT_PORTP(BgL_iportz00_11605))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11611 = BgL_iportz00_11605; }  else 
{ 
 obj_t BgL_auxz00_14949;
BgL_auxz00_14949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5860z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11605); 
FAILURE(BgL_auxz00_14949,BFALSE,BFALSE);} 
BgL_test6795z00_14946 = 
rgc_fill_buffer(BgL_inputzd2portzd2_11611); } 
if(BgL_test6795z00_14946)
{ /* Llib/date.scm 1021 */
 long BgL_arg1755z00_11612; long BgL_arg1756z00_11613;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11614;
if(
INPUT_PORTP(BgL_iportz00_11605))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11614 = BgL_iportz00_11605; }  else 
{ 
 obj_t BgL_auxz00_14956;
BgL_auxz00_14956 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5860z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11605); 
FAILURE(BgL_auxz00_14956,BFALSE,BFALSE);} 
BgL_arg1755z00_11612 = 
RGC_BUFFER_FORWARD(BgL_inputzd2portzd2_11614); } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11615;
if(
INPUT_PORTP(BgL_iportz00_11605))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11615 = BgL_iportz00_11605; }  else 
{ 
 obj_t BgL_auxz00_14963;
BgL_auxz00_14963 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5860z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11605); 
FAILURE(BgL_auxz00_14963,BFALSE,BFALSE);} 
BgL_arg1756z00_11613 = 
RGC_BUFFER_BUFPOS(BgL_inputzd2portzd2_11615); } 
{ 
 long BgL_bufposz00_14969; long BgL_forwardz00_14968;
BgL_forwardz00_14968 = BgL_arg1755z00_11612; 
BgL_bufposz00_14969 = BgL_arg1756z00_11613; 
BgL_bufposz00_11608 = BgL_bufposz00_14969; 
BgL_forwardz00_11607 = BgL_forwardz00_14968; 
goto BgL_statezd26zd21173z00_11586;} }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11609; } }  else 
{ /* Llib/date.scm 1021 */
 int BgL_curz00_11616;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11617;
if(
INPUT_PORTP(BgL_iportz00_11605))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11617 = BgL_iportz00_11605; }  else 
{ 
 obj_t BgL_auxz00_14972;
BgL_auxz00_14972 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5860z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11605); 
FAILURE(BgL_auxz00_14972,BFALSE,BFALSE);} 
BgL_curz00_11616 = 
RGC_BUFFER_GET_CHAR(BgL_inputzd2portzd2_11617, BgL_forwardz00_11607); } 
{ /* Llib/date.scm 1021 */

{ /* Llib/date.scm 1021 */
 bool_t BgL_test6800z00_14977;
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6801z00_14978;
if(
(
(long)(BgL_curz00_11616)==10L))
{ /* Llib/date.scm 1021 */
BgL_test6801z00_14978 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6801z00_14978 = 
(
(long)(BgL_curz00_11616)==9L)
; } 
if(BgL_test6801z00_14978)
{ /* Llib/date.scm 1021 */
BgL_test6800z00_14977 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
if(
(
(long)(BgL_curz00_11616)==13L))
{ /* Llib/date.scm 1021 */
BgL_test6800z00_14977 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 1021 */
BgL_test6800z00_14977 = 
(
(long)(BgL_curz00_11616)==32L)
; } } } 
if(BgL_test6800z00_14977)
{ 
 long BgL_forwardz00_14990; long BgL_lastzd2matchzd2_14989;
BgL_lastzd2matchzd2_14989 = BgL_newzd2matchzd2_11609; 
BgL_forwardz00_14990 = 
(1L+BgL_forwardz00_11607); 
BgL_forwardz00_11607 = BgL_forwardz00_14990; 
BgL_lastzd2matchzd2_11606 = BgL_lastzd2matchzd2_14989; 
goto BgL_statezd26zd21173z00_11586;}  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11609; } } } } } }  else 
{ /* Llib/date.scm 1021 */
BgL_matchz00_11592 = BgL_newzd2matchzd2_11648; } } } } } }  else 
{ /* Llib/date.scm 1021 */
 long BgL_arg1723z00_11668;
BgL_arg1723z00_11668 = 
(1L+BgL_forwardz00_11659); 
{ /* Llib/date.scm 1021 */
 long BgL_newzd2matchzd2_11669;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11670;
if(
INPUT_PORTP(BgL_iportz00_11657))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11670 = BgL_iportz00_11657; }  else 
{ 
 obj_t BgL_auxz00_14997;
BgL_auxz00_14997 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5864z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_11657); 
FAILURE(BgL_auxz00_14997,BFALSE,BFALSE);} 
RGC_STOP_MATCH(BgL_inputzd2portzd2_11670, BgL_arg1723z00_11668); } 
BgL_newzd2matchzd2_11669 = 2L; 
BgL_matchz00_11592 = BgL_newzd2matchzd2_11669; } } } } } } } 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11597;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11597 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_15004;
BgL_auxz00_15004 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_15004,BFALSE,BFALSE);} 
RGC_SET_FILEPOS(BgL_inputzd2portzd2_11597); } 
switch( BgL_matchz00_11592) { case 2L : 

{ /* Llib/date.scm 1029 */
 obj_t BgL_arg1854z00_11598; obj_t BgL_arg1856z00_11599;
{ /* Llib/date.scm 1021 */
 bool_t BgL_test6806z00_15009;
{ /* Llib/date.scm 1021 */
 long BgL_arg1847z00_11602;
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11603;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11603 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_15012;
BgL_auxz00_15012 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_15012,BFALSE,BFALSE);} 
BgL_arg1847z00_11602 = 
RGC_BUFFER_MATCH_LENGTH(BgL_inputzd2portzd2_11603); } 
BgL_test6806z00_15009 = 
(BgL_arg1847z00_11602==0L); } 
if(BgL_test6806z00_15009)
{ /* Llib/date.scm 1021 */
BgL_arg1854z00_11598 = BEOF; }  else 
{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11604;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11604 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_15020;
BgL_auxz00_15020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5792z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_15020,BFALSE,BFALSE);} 
BgL_arg1854z00_11598 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_inputzd2portzd2_11604)); } } 
{ /* Llib/date.scm 1029 */
 obj_t BgL_res4731z00_11600;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_res4731z00_11600 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_15028;
BgL_auxz00_15028 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_15028,BFALSE,BFALSE);} 
BgL_arg1856z00_11599 = BgL_res4731z00_11600; } 
return 
BGl_parsezd2errorzd2zz__datez00(BGl_string5787z00zz__datez00, BGl_string5859z00zz__datez00, BgL_arg1854z00_11598, BgL_arg1856z00_11599);} break;case 1L : 

{ /* Llib/date.scm 1021 */
 obj_t BgL_inputzd2portzd2_11601;
if(
INPUT_PORTP(BgL_iportz00_9766))
{ /* Llib/date.scm 1021 */
BgL_inputzd2portzd2_11601 = BgL_iportz00_9766; }  else 
{ 
 obj_t BgL_auxz00_15035;
BgL_auxz00_15035 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42604L), BGl_string5785z00zz__datez00, BGl_string5786z00zz__datez00, BgL_iportz00_9766); 
FAILURE(BgL_auxz00_15035,BFALSE,BFALSE);} 
return 
BINT(
rgc_buffer_fixnum(BgL_inputzd2portzd2_11601));} break;case 0L : 

goto BgL_ignorez00_11584;break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_11592));} } } } } } 

}



/* &<@anonymous:1451> */
obj_t BGl_z62zc3z04anonymousza31451ze3ze5zz__datez00(obj_t BgL_envz00_9767, obj_t BgL_iportz00_9768)
{
{ /* Llib/date.scm 979 */
{ 
 obj_t BgL_iportz00_11802; long BgL_lastzd2matchzd2_11803; long BgL_forwardz00_11804; long BgL_bufposz00_11805; obj_t BgL_iportz00_11793; long BgL_lastzd2matchzd2_11794; long BgL_forwardz00_11795; long BgL_bufposz00_11796; obj_t BgL_iportz00_11785; long BgL_lastzd2matchzd2_11786; long BgL_forwardz00_11787; long BgL_bufposz00_11788; obj_t BgL_iportz00_11777; long BgL_lastzd2matchzd2_11778; long BgL_forwardz00_11779; long BgL_bufposz00_11780; obj_t BgL_iportz00_11769; long BgL_lastzd2matchzd2_11770; long BgL_forwardz00_11771; long BgL_bufposz00_11772; obj_t BgL_iportz00_11760; long BgL_lastzd2matchzd2_11761; long BgL_forwardz00_11762; long BgL_bufposz00_11763; obj_t BgL_iportz00_11752; long BgL_lastzd2matchzd2_11753; long BgL_forwardz00_11754; long BgL_bufposz00_11755; obj_t BgL_iportz00_11745; long BgL_lastzd2matchzd2_11746; long BgL_forwardz00_11747; long BgL_bufposz00_11748; obj_t BgL_iportz00_11737; long BgL_lastzd2matchzd2_11738; long BgL_forwardz00_11739; long BgL_bufposz00_11740;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_9768))
{ /* Llib/date.scm 979 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg1454z00_11811;
{ /* Llib/date.scm 979 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_11812;
{ /* Llib/date.scm 979 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_11813;
BgL_new1077z00_11813 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 979 */
 long BgL_arg1457z00_11814;
BgL_arg1457z00_11814 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_11813), BgL_arg1457z00_11814); } 
BgL_new1078z00_11812 = BgL_new1077z00_11813; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11812)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11812)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_15054;
{ /* Llib/date.scm 979 */
 obj_t BgL_arg1455z00_11815;
{ /* Llib/date.scm 979 */
 obj_t BgL_arg1456z00_11816;
{ /* Llib/date.scm 979 */
 obj_t BgL_classz00_11817;
BgL_classz00_11817 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg1456z00_11816 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_11817); } 
BgL_arg1455z00_11815 = 
VECTOR_REF(BgL_arg1456z00_11816,2L); } 
{ /* Llib/date.scm 979 */
 obj_t BgL_auxz00_15058;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg1455z00_11815))
{ /* Llib/date.scm 979 */
BgL_auxz00_15058 = BgL_arg1455z00_11815
; }  else 
{ 
 obj_t BgL_auxz00_15061;
BgL_auxz00_15061 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41155L), BGl_string5906z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg1455z00_11815); 
FAILURE(BgL_auxz00_15061,BFALSE,BFALSE);} 
BgL_auxz00_15054 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_15058); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_11812)))->BgL_stackz00)=((obj_t)BgL_auxz00_15054),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11812)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11812)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_11812)))->BgL_objz00)=((obj_t)BgL_iportz00_9768),BUNSPEC); 
BgL_arg1454z00_11811 = BgL_new1078z00_11812; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1454z00_11811));}  else 
{ /* Llib/date.scm 979 */
BgL_ignorez00_11682:
RGC_START_MATCH(BgL_iportz00_9768); 
{ /* Llib/date.scm 979 */
 long BgL_matchz00_11693;
{ /* Llib/date.scm 979 */
 long BgL_arg1700z00_11694; long BgL_arg1701z00_11695;
BgL_arg1700z00_11694 = 
RGC_BUFFER_FORWARD(BgL_iportz00_9768); 
BgL_arg1701z00_11695 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_9768); 
BgL_iportz00_11802 = BgL_iportz00_9768; 
BgL_lastzd2matchzd2_11803 = 3L; 
BgL_forwardz00_11804 = BgL_arg1700z00_11694; 
BgL_bufposz00_11805 = BgL_arg1701z00_11695; 
BgL_statezd20zd21151z00_11692:
if(
(BgL_forwardz00_11804==BgL_bufposz00_11805))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11802))
{ /* Llib/date.scm 979 */
 long BgL_arg1462z00_11806; long BgL_arg1463z00_11807;
BgL_arg1462z00_11806 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11802); 
BgL_arg1463z00_11807 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11802); 
{ 
 long BgL_bufposz00_15085; long BgL_forwardz00_15084;
BgL_forwardz00_15084 = BgL_arg1462z00_11806; 
BgL_bufposz00_15085 = BgL_arg1463z00_11807; 
BgL_bufposz00_11805 = BgL_bufposz00_15085; 
BgL_forwardz00_11804 = BgL_forwardz00_15084; 
goto BgL_statezd20zd21151z00_11692;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11803; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11808;
BgL_curz00_11808 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11802, BgL_forwardz00_11804); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6815z00_15087;
if(
(
(long)(BgL_curz00_11808)==70L))
{ /* Llib/date.scm 979 */
BgL_test6815z00_15087 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11808)==77L))
{ /* Llib/date.scm 979 */
BgL_test6815z00_15087 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6818z00_15094;
if(
(
(long)(BgL_curz00_11808)==84L))
{ /* Llib/date.scm 979 */
BgL_test6818z00_15094 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6818z00_15094 = 
(
(long)(BgL_curz00_11808)==83L)
; } 
if(BgL_test6818z00_15094)
{ /* Llib/date.scm 979 */
BgL_test6815z00_15087 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6815z00_15087 = 
(
(long)(BgL_curz00_11808)==87L)
; } } } 
if(BgL_test6815z00_15087)
{ /* Llib/date.scm 979 */
BgL_iportz00_11760 = BgL_iportz00_11802; 
BgL_lastzd2matchzd2_11761 = BgL_lastzd2matchzd2_11803; 
BgL_forwardz00_11762 = 
(1L+BgL_forwardz00_11804); 
BgL_bufposz00_11763 = BgL_bufposz00_11805; 
BgL_statezd24zd21155z00_11687:
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11764;
RGC_STOP_MATCH(BgL_iportz00_11760, BgL_forwardz00_11762); 
BgL_newzd2matchzd2_11764 = 3L; 
if(
(BgL_forwardz00_11762==BgL_bufposz00_11763))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11760))
{ /* Llib/date.scm 979 */
 long BgL_arg1529z00_11765; long BgL_arg1530z00_11766;
BgL_arg1529z00_11765 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11760); 
BgL_arg1530z00_11766 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11760); 
{ 
 long BgL_bufposz00_15110; long BgL_forwardz00_15109;
BgL_forwardz00_15109 = BgL_arg1529z00_11765; 
BgL_bufposz00_15110 = BgL_arg1530z00_11766; 
BgL_bufposz00_11763 = BgL_bufposz00_15110; 
BgL_forwardz00_11762 = BgL_forwardz00_15109; 
goto BgL_statezd24zd21155z00_11687;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11764; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11767;
BgL_curz00_11767 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11760, BgL_forwardz00_11762); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6822z00_15112;
if(
(
(long)(BgL_curz00_11767)==97L))
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6824z00_15116;
if(
(
(long)(BgL_curz00_11767)==101L))
{ /* Llib/date.scm 979 */
BgL_test6824z00_15116 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6824z00_15116 = 
(
(long)(BgL_curz00_11767)==100L)
; } 
if(BgL_test6824z00_15116)
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6826z00_15122;
if(
(
(long)(BgL_curz00_11767)==105L))
{ /* Llib/date.scm 979 */
BgL_test6826z00_15122 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6826z00_15122 = 
(
(long)(BgL_curz00_11767)==104L)
; } 
if(BgL_test6826z00_15122)
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6828z00_15128;
if(
(
(long)(BgL_curz00_11767)==111L))
{ /* Llib/date.scm 979 */
BgL_test6828z00_15128 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6828z00_15128 = 
(
(long)(BgL_curz00_11767)==110L)
; } 
if(BgL_test6828z00_15128)
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11767)==114L))
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL__ortest_1164z00_11768;
BgL__ortest_1164z00_11768 = 
(
(long)(BgL_curz00_11767)==117L); 
if(BgL__ortest_1164z00_11768)
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = BgL__ortest_1164z00_11768
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6822z00_15112 = 
(
(long)(BgL_curz00_11767)==116L)
; } } } } } } 
if(BgL_test6822z00_15112)
{ /* Llib/date.scm 979 */
BgL_iportz00_11785 = BgL_iportz00_11760; 
BgL_lastzd2matchzd2_11786 = BgL_newzd2matchzd2_11764; 
BgL_forwardz00_11787 = 
(1L+BgL_forwardz00_11762); 
BgL_bufposz00_11788 = BgL_bufposz00_11763; 
BgL_statezd25zd21156z00_11690:
if(
(BgL_forwardz00_11787==BgL_bufposz00_11788))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11785))
{ /* Llib/date.scm 979 */
 long BgL_arg1494z00_11789; long BgL_arg1495z00_11790;
BgL_arg1494z00_11789 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11785); 
BgL_arg1495z00_11790 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11785); 
{ 
 long BgL_bufposz00_15149; long BgL_forwardz00_15148;
BgL_forwardz00_15148 = BgL_arg1494z00_11789; 
BgL_bufposz00_15149 = BgL_arg1495z00_11790; 
BgL_bufposz00_11788 = BgL_bufposz00_15149; 
BgL_forwardz00_11787 = BgL_forwardz00_15148; 
goto BgL_statezd25zd21156z00_11690;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11786; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11791;
BgL_curz00_11791 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11785, BgL_forwardz00_11787); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6834z00_15151;
if(
(
(long)(BgL_curz00_11791)==97L))
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6836z00_15155;
if(
(
(long)(BgL_curz00_11791)==101L))
{ /* Llib/date.scm 979 */
BgL_test6836z00_15155 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6836z00_15155 = 
(
(long)(BgL_curz00_11791)==100L)
; } 
if(BgL_test6836z00_15155)
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6838z00_15161;
if(
(
(long)(BgL_curz00_11791)==105L))
{ /* Llib/date.scm 979 */
BgL_test6838z00_15161 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6838z00_15161 = 
(
(long)(BgL_curz00_11791)==104L)
; } 
if(BgL_test6838z00_15161)
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6840z00_15167;
if(
(
(long)(BgL_curz00_11791)==111L))
{ /* Llib/date.scm 979 */
BgL_test6840z00_15167 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6840z00_15167 = 
(
(long)(BgL_curz00_11791)==110L)
; } 
if(BgL_test6840z00_15167)
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11791)==114L))
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL__ortest_1163z00_11792;
BgL__ortest_1163z00_11792 = 
(
(long)(BgL_curz00_11791)==117L); 
if(BgL__ortest_1163z00_11792)
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = BgL__ortest_1163z00_11792
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6834z00_15151 = 
(
(long)(BgL_curz00_11791)==116L)
; } } } } } } 
if(BgL_test6834z00_15151)
{ /* Llib/date.scm 979 */
BgL_iportz00_11745 = BgL_iportz00_11785; 
BgL_lastzd2matchzd2_11746 = BgL_lastzd2matchzd2_11786; 
BgL_forwardz00_11747 = 
(1L+BgL_forwardz00_11787); 
BgL_bufposz00_11748 = BgL_bufposz00_11788; 
BgL_statezd27zd21158z00_11685:
if(
(BgL_forwardz00_11747==BgL_bufposz00_11748))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11745))
{ /* Llib/date.scm 979 */
 long BgL_arg1553z00_11749; long BgL_arg1554z00_11750;
BgL_arg1553z00_11749 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11745); 
BgL_arg1554z00_11750 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11745); 
{ 
 long BgL_bufposz00_15188; long BgL_forwardz00_15187;
BgL_forwardz00_15187 = BgL_arg1553z00_11749; 
BgL_bufposz00_15188 = BgL_arg1554z00_11750; 
BgL_bufposz00_11748 = BgL_bufposz00_15188; 
BgL_forwardz00_11747 = BgL_forwardz00_15187; 
goto BgL_statezd27zd21158z00_11685;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11746; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11751;
BgL_curz00_11751 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11745, BgL_forwardz00_11747); 
{ /* Llib/date.scm 979 */

if(
(
(long)(BgL_curz00_11751)==44L))
{ /* Llib/date.scm 979 */
BgL_iportz00_11793 = BgL_iportz00_11745; 
BgL_lastzd2matchzd2_11794 = BgL_lastzd2matchzd2_11746; 
BgL_forwardz00_11795 = 
(1L+BgL_forwardz00_11747); 
BgL_bufposz00_11796 = BgL_bufposz00_11748; 
BgL_statezd28zd21159z00_11691:
if(
(BgL_forwardz00_11795==BgL_bufposz00_11796))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11793))
{ /* Llib/date.scm 979 */
 long BgL_arg1483z00_11797; long BgL_arg1484z00_11798;
BgL_arg1483z00_11797 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11793); 
BgL_arg1484z00_11798 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11793); 
{ 
 long BgL_bufposz00_15200; long BgL_forwardz00_15199;
BgL_forwardz00_15199 = BgL_arg1483z00_11797; 
BgL_bufposz00_15200 = BgL_arg1484z00_11798; 
BgL_bufposz00_11796 = BgL_bufposz00_15200; 
BgL_forwardz00_11795 = BgL_forwardz00_15199; 
goto BgL_statezd28zd21159z00_11691;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11794; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11799;
BgL_curz00_11799 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11793, BgL_forwardz00_11795); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6849z00_15202;
{ /* Llib/date.scm 979 */
 bool_t BgL_test6850z00_15203;
if(
(
(long)(BgL_curz00_11799)==10L))
{ /* Llib/date.scm 979 */
BgL_test6850z00_15203 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6850z00_15203 = 
(
(long)(BgL_curz00_11799)==9L)
; } 
if(BgL_test6850z00_15203)
{ /* Llib/date.scm 979 */
BgL_test6849z00_15202 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11799)==13L))
{ /* Llib/date.scm 979 */
BgL_test6849z00_15202 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6849z00_15202 = 
(
(long)(BgL_curz00_11799)==32L)
; } } } 
if(BgL_test6849z00_15202)
{ /* Llib/date.scm 979 */
 long BgL_arg1489z00_11800;
BgL_arg1489z00_11800 = 
(1L+BgL_forwardz00_11795); 
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11801;
RGC_STOP_MATCH(BgL_iportz00_11793, BgL_arg1489z00_11800); 
BgL_newzd2matchzd2_11801 = 1L; 
BgL_matchz00_11693 = BgL_newzd2matchzd2_11801; } }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11794; } } } } }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11746; } } } }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_lastzd2matchzd2_11786; } } } } }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11764; } } } } } }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6853z00_15220;
if(
(
(long)(BgL_curz00_11808)>=48L))
{ /* Llib/date.scm 979 */
BgL_test6853z00_15220 = 
(
(long)(BgL_curz00_11808)<58L)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6853z00_15220 = ((bool_t)0)
; } 
if(BgL_test6853z00_15220)
{ /* Llib/date.scm 979 */
BgL_iportz00_11769 = BgL_iportz00_11802; 
BgL_lastzd2matchzd2_11770 = BgL_lastzd2matchzd2_11803; 
BgL_forwardz00_11771 = 
(1L+BgL_forwardz00_11804); 
BgL_bufposz00_11772 = BgL_bufposz00_11805; 
BgL_statezd23zd21154z00_11688:
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11773;
RGC_STOP_MATCH(BgL_iportz00_11769, BgL_forwardz00_11771); 
BgL_newzd2matchzd2_11773 = 2L; 
if(
(BgL_forwardz00_11771==BgL_bufposz00_11772))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11769))
{ /* Llib/date.scm 979 */
 long BgL_arg1521z00_11774; long BgL_arg1522z00_11775;
BgL_arg1521z00_11774 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11769); 
BgL_arg1522z00_11775 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11769); 
{ 
 long BgL_bufposz00_15234; long BgL_forwardz00_15233;
BgL_forwardz00_15233 = BgL_arg1521z00_11774; 
BgL_bufposz00_15234 = BgL_arg1522z00_11775; 
BgL_bufposz00_11772 = BgL_bufposz00_15234; 
BgL_forwardz00_11771 = BgL_forwardz00_15233; 
goto BgL_statezd23zd21154z00_11688;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11773; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11776;
BgL_curz00_11776 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11769, BgL_forwardz00_11771); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6857z00_15236;
if(
(
(long)(BgL_curz00_11776)>=48L))
{ /* Llib/date.scm 979 */
BgL_test6857z00_15236 = 
(
(long)(BgL_curz00_11776)<58L)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6857z00_15236 = ((bool_t)0)
; } 
if(BgL_test6857z00_15236)
{ /* Llib/date.scm 979 */
BgL_iportz00_11752 = BgL_iportz00_11769; 
BgL_lastzd2matchzd2_11753 = BgL_newzd2matchzd2_11773; 
BgL_forwardz00_11754 = 
(1L+BgL_forwardz00_11771); 
BgL_bufposz00_11755 = BgL_bufposz00_11772; 
BgL_statezd210zd21161z00_11686:
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11756;
RGC_STOP_MATCH(BgL_iportz00_11752, BgL_forwardz00_11754); 
BgL_newzd2matchzd2_11756 = 2L; 
if(
(BgL_forwardz00_11754==BgL_bufposz00_11755))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11752))
{ /* Llib/date.scm 979 */
 long BgL_arg1544z00_11757; long BgL_arg1546z00_11758;
BgL_arg1544z00_11757 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11752); 
BgL_arg1546z00_11758 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11752); 
{ 
 long BgL_bufposz00_15250; long BgL_forwardz00_15249;
BgL_forwardz00_15249 = BgL_arg1544z00_11757; 
BgL_bufposz00_15250 = BgL_arg1546z00_11758; 
BgL_bufposz00_11755 = BgL_bufposz00_15250; 
BgL_forwardz00_11754 = BgL_forwardz00_15249; 
goto BgL_statezd210zd21161z00_11686;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11756; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11759;
BgL_curz00_11759 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11752, BgL_forwardz00_11754); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6861z00_15252;
if(
(
(long)(BgL_curz00_11759)>=48L))
{ /* Llib/date.scm 979 */
BgL_test6861z00_15252 = 
(
(long)(BgL_curz00_11759)<58L)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6861z00_15252 = ((bool_t)0)
; } 
if(BgL_test6861z00_15252)
{ 
 long BgL_forwardz00_15259; long BgL_lastzd2matchzd2_15258;
BgL_lastzd2matchzd2_15258 = BgL_newzd2matchzd2_11756; 
BgL_forwardz00_15259 = 
(1L+BgL_forwardz00_11754); 
BgL_forwardz00_11754 = BgL_forwardz00_15259; 
BgL_lastzd2matchzd2_11753 = BgL_lastzd2matchzd2_15258; 
goto BgL_statezd210zd21161z00_11686;}  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11756; } } } } } }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11773; } } } } } }  else 
{ /* Llib/date.scm 979 */
 bool_t BgL_test6863z00_15263;
{ /* Llib/date.scm 979 */
 bool_t BgL_test6864z00_15264;
if(
(
(long)(BgL_curz00_11808)==10L))
{ /* Llib/date.scm 979 */
BgL_test6864z00_15264 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6864z00_15264 = 
(
(long)(BgL_curz00_11808)==9L)
; } 
if(BgL_test6864z00_15264)
{ /* Llib/date.scm 979 */
BgL_test6863z00_15263 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11808)==13L))
{ /* Llib/date.scm 979 */
BgL_test6863z00_15263 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6863z00_15263 = 
(
(long)(BgL_curz00_11808)==32L)
; } } } 
if(BgL_test6863z00_15263)
{ /* Llib/date.scm 979 */
BgL_iportz00_11777 = BgL_iportz00_11802; 
BgL_lastzd2matchzd2_11778 = BgL_lastzd2matchzd2_11803; 
BgL_forwardz00_11779 = 
(1L+BgL_forwardz00_11804); 
BgL_bufposz00_11780 = BgL_bufposz00_11805; 
BgL_statezd22zd21153z00_11689:
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11781;
RGC_STOP_MATCH(BgL_iportz00_11777, BgL_forwardz00_11779); 
BgL_newzd2matchzd2_11781 = 0L; 
if(
(BgL_forwardz00_11779==BgL_bufposz00_11780))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11777))
{ /* Llib/date.scm 979 */
 long BgL_arg1509z00_11782; long BgL_arg1510z00_11783;
BgL_arg1509z00_11782 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11777); 
BgL_arg1510z00_11783 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11777); 
{ 
 long BgL_bufposz00_15283; long BgL_forwardz00_15282;
BgL_forwardz00_15282 = BgL_arg1509z00_11782; 
BgL_bufposz00_15283 = BgL_arg1510z00_11783; 
BgL_bufposz00_11780 = BgL_bufposz00_15283; 
BgL_forwardz00_11779 = BgL_forwardz00_15282; 
goto BgL_statezd22zd21153z00_11689;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11781; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11784;
BgL_curz00_11784 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11777, BgL_forwardz00_11779); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6869z00_15285;
{ /* Llib/date.scm 979 */
 bool_t BgL_test6870z00_15286;
if(
(
(long)(BgL_curz00_11784)==10L))
{ /* Llib/date.scm 979 */
BgL_test6870z00_15286 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6870z00_15286 = 
(
(long)(BgL_curz00_11784)==9L)
; } 
if(BgL_test6870z00_15286)
{ /* Llib/date.scm 979 */
BgL_test6869z00_15285 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11784)==13L))
{ /* Llib/date.scm 979 */
BgL_test6869z00_15285 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6869z00_15285 = 
(
(long)(BgL_curz00_11784)==32L)
; } } } 
if(BgL_test6869z00_15285)
{ /* Llib/date.scm 979 */
BgL_iportz00_11737 = BgL_iportz00_11777; 
BgL_lastzd2matchzd2_11738 = BgL_newzd2matchzd2_11781; 
BgL_forwardz00_11739 = 
(1L+BgL_forwardz00_11779); 
BgL_bufposz00_11740 = BgL_bufposz00_11780; 
BgL_statezd211zd21162z00_11684:
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11741;
RGC_STOP_MATCH(BgL_iportz00_11737, BgL_forwardz00_11739); 
BgL_newzd2matchzd2_11741 = 0L; 
if(
(BgL_forwardz00_11739==BgL_bufposz00_11740))
{ /* Llib/date.scm 979 */
if(
rgc_fill_buffer(BgL_iportz00_11737))
{ /* Llib/date.scm 979 */
 long BgL_arg1561z00_11742; long BgL_arg1562z00_11743;
BgL_arg1561z00_11742 = 
RGC_BUFFER_FORWARD(BgL_iportz00_11737); 
BgL_arg1562z00_11743 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_11737); 
{ 
 long BgL_bufposz00_15305; long BgL_forwardz00_15304;
BgL_forwardz00_15304 = BgL_arg1561z00_11742; 
BgL_bufposz00_15305 = BgL_arg1562z00_11743; 
BgL_bufposz00_11740 = BgL_bufposz00_15305; 
BgL_forwardz00_11739 = BgL_forwardz00_15304; 
goto BgL_statezd211zd21162z00_11684;} }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11741; } }  else 
{ /* Llib/date.scm 979 */
 int BgL_curz00_11744;
BgL_curz00_11744 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_11737, BgL_forwardz00_11739); 
{ /* Llib/date.scm 979 */

{ /* Llib/date.scm 979 */
 bool_t BgL_test6875z00_15307;
{ /* Llib/date.scm 979 */
 bool_t BgL_test6876z00_15308;
if(
(
(long)(BgL_curz00_11744)==10L))
{ /* Llib/date.scm 979 */
BgL_test6876z00_15308 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6876z00_15308 = 
(
(long)(BgL_curz00_11744)==9L)
; } 
if(BgL_test6876z00_15308)
{ /* Llib/date.scm 979 */
BgL_test6875z00_15307 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
if(
(
(long)(BgL_curz00_11744)==13L))
{ /* Llib/date.scm 979 */
BgL_test6875z00_15307 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 979 */
BgL_test6875z00_15307 = 
(
(long)(BgL_curz00_11744)==32L)
; } } } 
if(BgL_test6875z00_15307)
{ 
 long BgL_forwardz00_15320; long BgL_lastzd2matchzd2_15319;
BgL_lastzd2matchzd2_15319 = BgL_newzd2matchzd2_11741; 
BgL_forwardz00_15320 = 
(1L+BgL_forwardz00_11739); 
BgL_forwardz00_11739 = BgL_forwardz00_15320; 
BgL_lastzd2matchzd2_11738 = BgL_lastzd2matchzd2_15319; 
goto BgL_statezd211zd21162z00_11684;}  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11741; } } } } } }  else 
{ /* Llib/date.scm 979 */
BgL_matchz00_11693 = BgL_newzd2matchzd2_11781; } } } } } }  else 
{ /* Llib/date.scm 979 */
 long BgL_arg1478z00_11809;
BgL_arg1478z00_11809 = 
(1L+BgL_forwardz00_11804); 
{ /* Llib/date.scm 979 */
 long BgL_newzd2matchzd2_11810;
RGC_STOP_MATCH(BgL_iportz00_11802, BgL_arg1478z00_11809); 
BgL_newzd2matchzd2_11810 = 3L; 
BgL_matchz00_11693 = BgL_newzd2matchzd2_11810; } } } } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_9768); 
switch( BgL_matchz00_11693) { case 3L : 

{ /* Llib/date.scm 1013 */
 obj_t BgL_auxz00_15327;
{ /* Llib/date.scm 979 */
 bool_t BgL_test6879z00_15328;
{ /* Llib/date.scm 979 */
 long BgL_arg1658z00_11736;
BgL_arg1658z00_11736 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9768); 
BgL_test6879z00_15328 = 
(BgL_arg1658z00_11736==0L); } 
if(BgL_test6879z00_15328)
{ /* Llib/date.scm 979 */
BgL_auxz00_15327 = BEOF
; }  else 
{ /* Llib/date.scm 979 */
BgL_auxz00_15327 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9768))
; } } 
return 
BGl_parsezd2errorzd2zz__datez00(BGl_string5787z00zz__datez00, BGl_string5866z00zz__datez00, BgL_auxz00_15327, BgL_iportz00_9768);} break;case 2L : 

{ /* Llib/date.scm 998 */
 long BgL_dayz00_11696;
BgL_dayz00_11696 = 
rgc_buffer_fixnum(BgL_iportz00_9768); 
{ /* Llib/date.scm 998 */
 obj_t BgL_monthz00_11697;
{ /* Llib/date.scm 999 */
 obj_t BgL_funz00_11698;
{ /* Llib/date.scm 999 */
 obj_t BgL_aux5434z00_11699;
BgL_aux5434z00_11699 = BGl_monthzd2grammarzd2zz__datez00; 
if(
PROCEDUREP(BgL_aux5434z00_11699))
{ /* Llib/date.scm 999 */
BgL_funz00_11698 = BgL_aux5434z00_11699; }  else 
{ 
 obj_t BgL_auxz00_15337;
BgL_auxz00_15337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41831L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5434z00_11699); 
FAILURE(BgL_auxz00_15337,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11698, 1))
{ /* Llib/date.scm 999 */
BgL_monthz00_11697 = 
(VA_PROCEDUREP( BgL_funz00_11698 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11698))(BGl_monthzd2grammarzd2zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11698))(BGl_monthzd2grammarzd2zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 999 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5869z00zz__datez00,BgL_funz00_11698);} } 
{ /* Llib/date.scm 999 */
 obj_t BgL_yearz00_11700;
{ /* Llib/date.scm 1000 */
 obj_t BgL_funz00_11701;
{ /* Llib/date.scm 1000 */
 obj_t BgL_aux5437z00_11702;
BgL_aux5437z00_11702 = BGl_thezd2fixnumzd2grammarz00zz__datez00; 
if(
PROCEDUREP(BgL_aux5437z00_11702))
{ /* Llib/date.scm 1000 */
BgL_funz00_11701 = BgL_aux5437z00_11702; }  else 
{ 
 obj_t BgL_auxz00_15350;
BgL_auxz00_15350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41880L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5437z00_11702); 
FAILURE(BgL_auxz00_15350,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11701, 1))
{ /* Llib/date.scm 1000 */
BgL_yearz00_11700 = 
(VA_PROCEDUREP( BgL_funz00_11701 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11701))(BGl_thezd2fixnumzd2grammarz00zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11701))(BGl_thezd2fixnumzd2grammarz00zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 1000 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5876z00zz__datez00,BgL_funz00_11701);} } 
{ /* Llib/date.scm 1000 */

{ /* Llib/date.scm 1001 */
 obj_t BgL_hourz00_11703;
{ /* Llib/date.scm 1002 */
 obj_t BgL_funz00_11704;
{ /* Llib/date.scm 1002 */
 obj_t BgL_aux5440z00_11705;
BgL_aux5440z00_11705 = BGl_timezd2grammarzd2zz__datez00; 
if(
PROCEDUREP(BgL_aux5440z00_11705))
{ /* Llib/date.scm 1002 */
BgL_funz00_11704 = BgL_aux5440z00_11705; }  else 
{ 
 obj_t BgL_auxz00_15363;
BgL_auxz00_15363 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41973L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5440z00_11705); 
FAILURE(BgL_auxz00_15363,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11704, 1))
{ /* Llib/date.scm 1002 */
BgL_hourz00_11703 = 
(VA_PROCEDUREP( BgL_funz00_11704 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11704))(BGl_timezd2grammarzd2zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11704))(BGl_timezd2grammarzd2zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 1002 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5881z00zz__datez00,BgL_funz00_11704);} } 
{ /* Llib/date.scm 1002 */
 obj_t BgL_minutez00_11706; obj_t BgL_secondz00_11707;
{ /* Llib/date.scm 1003 */
 obj_t BgL_tmpz00_11708;
{ /* Llib/date.scm 1003 */
 int BgL_tmpz00_15374;
BgL_tmpz00_15374 = 
(int)(1L); 
BgL_tmpz00_11708 = 
BGL_MVALUES_VAL(BgL_tmpz00_15374); } 
{ /* Llib/date.scm 1003 */
 int BgL_tmpz00_15377;
BgL_tmpz00_15377 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_15377, BUNSPEC); } 
BgL_minutez00_11706 = BgL_tmpz00_11708; } 
{ /* Llib/date.scm 1003 */
 obj_t BgL_tmpz00_11709;
{ /* Llib/date.scm 1003 */
 int BgL_tmpz00_15380;
BgL_tmpz00_15380 = 
(int)(2L); 
BgL_tmpz00_11709 = 
BGL_MVALUES_VAL(BgL_tmpz00_15380); } 
{ /* Llib/date.scm 1003 */
 int BgL_tmpz00_15383;
BgL_tmpz00_15383 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_15383, BUNSPEC); } 
BgL_secondz00_11707 = BgL_tmpz00_11709; } 
{ /* Llib/date.scm 1003 */
 obj_t BgL_za7oneza7_11710;
{ /* Llib/date.scm 1003 */
 obj_t BgL_funz00_11711;
{ /* Llib/date.scm 1003 */
 obj_t BgL_aux5443z00_11712;
BgL_aux5443z00_11712 = BGl_za7onezd2grammarz75zz__datez00; 
if(
PROCEDUREP(BgL_aux5443z00_11712))
{ /* Llib/date.scm 1003 */
BgL_funz00_11711 = BgL_aux5443z00_11712; }  else 
{ 
 obj_t BgL_auxz00_15388;
BgL_auxz00_15388 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42025L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5443z00_11712); 
FAILURE(BgL_auxz00_15388,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11711, 1))
{ /* Llib/date.scm 1003 */
BgL_za7oneza7_11710 = 
(VA_PROCEDUREP( BgL_funz00_11711 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11711))(BGl_za7onezd2grammarz75zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11711))(BGl_za7onezd2grammarz75zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 1003 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5886z00zz__datez00,BgL_funz00_11711);} } 
{ /* Llib/date.scm 1008 */
 obj_t BgL_arg1669z00_11713;
{ /* Llib/date.scm 1008 */
 bool_t BgL_test6888z00_15399;
{ /* Llib/date.scm 1008 */
 long BgL_n1z00_11714;
{ /* Llib/date.scm 1008 */
 obj_t BgL_tmpz00_15400;
if(
INTEGERP(BgL_yearz00_11700))
{ /* Llib/date.scm 1008 */
BgL_tmpz00_15400 = BgL_yearz00_11700
; }  else 
{ 
 obj_t BgL_auxz00_15403;
BgL_auxz00_15403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42157L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_11700); 
FAILURE(BgL_auxz00_15403,BFALSE,BFALSE);} 
BgL_n1z00_11714 = 
(long)CINT(BgL_tmpz00_15400); } 
BgL_test6888z00_15399 = 
(BgL_n1z00_11714<100L); } 
if(BgL_test6888z00_15399)
{ /* Llib/date.scm 1003 */
 obj_t BgL_tmpz00_15409;
if(
INTEGERP(BgL_yearz00_11700))
{ /* Llib/date.scm 1008 */
BgL_tmpz00_15409 = BgL_yearz00_11700
; }  else 
{ 
 obj_t BgL_auxz00_15412;
BgL_auxz00_15412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42172L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_11700); 
FAILURE(BgL_auxz00_15412,BFALSE,BFALSE);} 
BgL_arg1669z00_11713 = 
ADDFX(BgL_tmpz00_15409, 
BINT(2000L)); }  else 
{ /* Llib/date.scm 1008 */
BgL_arg1669z00_11713 = BgL_yearz00_11700; } } 
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_za7oneza7_11710))
{ /* Llib/date.scm 233 */
 long BgL_auxz00_15466; int BgL_auxz00_15457; int BgL_auxz00_15447; int BgL_auxz00_15438; int BgL_auxz00_15429; int BgL_tmpz00_15420;
{ /* Llib/date.scm 1010 */
 obj_t BgL_tmpz00_15467;
if(
INTEGERP(BgL_za7oneza7_11710))
{ /* Llib/date.scm 1010 */
BgL_tmpz00_15467 = BgL_za7oneza7_11710
; }  else 
{ 
 obj_t BgL_auxz00_15470;
BgL_auxz00_15470 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42218L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_za7oneza7_11710); 
FAILURE(BgL_auxz00_15470,BFALSE,BFALSE);} 
BgL_auxz00_15466 = 
(long)CINT(BgL_tmpz00_15467); } 
{ /* Llib/date.scm 1008 */
 obj_t BgL_tmpz00_15458;
if(
INTEGERP(BgL_arg1669z00_11713))
{ /* Llib/date.scm 1008 */
BgL_tmpz00_15458 = BgL_arg1669z00_11713
; }  else 
{ 
 obj_t BgL_auxz00_15461;
BgL_auxz00_15461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42187L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg1669z00_11713); 
FAILURE(BgL_auxz00_15461,BFALSE,BFALSE);} 
BgL_auxz00_15457 = 
CINT(BgL_tmpz00_15458); } 
{ /* Llib/date.scm 1007 */
 obj_t BgL_tmpz00_15449;
if(
INTEGERP(BgL_monthz00_11697))
{ /* Llib/date.scm 1007 */
BgL_tmpz00_15449 = BgL_monthz00_11697
; }  else 
{ 
 obj_t BgL_auxz00_15452;
BgL_auxz00_15452 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42131L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_11697); 
FAILURE(BgL_auxz00_15452,BFALSE,BFALSE);} 
BgL_auxz00_15447 = 
CINT(BgL_tmpz00_15449); } 
{ /* Llib/date.scm 1006 */
 obj_t BgL_tmpz00_15439;
if(
INTEGERP(BgL_hourz00_11703))
{ /* Llib/date.scm 1006 */
BgL_tmpz00_15439 = BgL_hourz00_11703
; }  else 
{ 
 obj_t BgL_auxz00_15442;
BgL_auxz00_15442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42114L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_11703); 
FAILURE(BgL_auxz00_15442,BFALSE,BFALSE);} 
BgL_auxz00_15438 = 
CINT(BgL_tmpz00_15439); } 
{ /* Llib/date.scm 1005 */
 obj_t BgL_tmpz00_15430;
if(
INTEGERP(BgL_minutez00_11706))
{ /* Llib/date.scm 1005 */
BgL_tmpz00_15430 = BgL_minutez00_11706
; }  else 
{ 
 obj_t BgL_auxz00_15433;
BgL_auxz00_15433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42096L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minutez00_11706); 
FAILURE(BgL_auxz00_15433,BFALSE,BFALSE);} 
BgL_auxz00_15429 = 
CINT(BgL_tmpz00_15430); } 
{ /* Llib/date.scm 1004 */
 obj_t BgL_tmpz00_15421;
if(
INTEGERP(BgL_secondz00_11707))
{ /* Llib/date.scm 1004 */
BgL_tmpz00_15421 = BgL_secondz00_11707
; }  else 
{ 
 obj_t BgL_auxz00_15424;
BgL_auxz00_15424 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42079L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secondz00_11707); 
FAILURE(BgL_auxz00_15424,BFALSE,BFALSE);} 
BgL_tmpz00_15420 = 
CINT(BgL_tmpz00_15421); } 
return 
bgl_make_date(((BGL_LONGLONG_T)0), BgL_tmpz00_15420, BgL_auxz00_15429, BgL_auxz00_15438, 
(int)(BgL_dayz00_11696), BgL_auxz00_15447, BgL_auxz00_15457, BgL_auxz00_15466, ((bool_t)1), 
(int)(-1L));}  else 
{ /* Llib/date.scm 234 */
 int BgL_auxz00_15514; int BgL_auxz00_15504; int BgL_auxz00_15495; int BgL_auxz00_15486; int BgL_tmpz00_15477;
{ /* Llib/date.scm 1008 */
 obj_t BgL_tmpz00_15515;
if(
INTEGERP(BgL_arg1669z00_11713))
{ /* Llib/date.scm 1008 */
BgL_tmpz00_15515 = BgL_arg1669z00_11713
; }  else 
{ 
 obj_t BgL_auxz00_15518;
BgL_auxz00_15518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42187L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg1669z00_11713); 
FAILURE(BgL_auxz00_15518,BFALSE,BFALSE);} 
BgL_auxz00_15514 = 
CINT(BgL_tmpz00_15515); } 
{ /* Llib/date.scm 1007 */
 obj_t BgL_tmpz00_15506;
if(
INTEGERP(BgL_monthz00_11697))
{ /* Llib/date.scm 1007 */
BgL_tmpz00_15506 = BgL_monthz00_11697
; }  else 
{ 
 obj_t BgL_auxz00_15509;
BgL_auxz00_15509 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42131L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_11697); 
FAILURE(BgL_auxz00_15509,BFALSE,BFALSE);} 
BgL_auxz00_15504 = 
CINT(BgL_tmpz00_15506); } 
{ /* Llib/date.scm 1006 */
 obj_t BgL_tmpz00_15496;
if(
INTEGERP(BgL_hourz00_11703))
{ /* Llib/date.scm 1006 */
BgL_tmpz00_15496 = BgL_hourz00_11703
; }  else 
{ 
 obj_t BgL_auxz00_15499;
BgL_auxz00_15499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42114L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_11703); 
FAILURE(BgL_auxz00_15499,BFALSE,BFALSE);} 
BgL_auxz00_15495 = 
CINT(BgL_tmpz00_15496); } 
{ /* Llib/date.scm 1005 */
 obj_t BgL_tmpz00_15487;
if(
INTEGERP(BgL_minutez00_11706))
{ /* Llib/date.scm 1005 */
BgL_tmpz00_15487 = BgL_minutez00_11706
; }  else 
{ 
 obj_t BgL_auxz00_15490;
BgL_auxz00_15490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42096L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minutez00_11706); 
FAILURE(BgL_auxz00_15490,BFALSE,BFALSE);} 
BgL_auxz00_15486 = 
CINT(BgL_tmpz00_15487); } 
{ /* Llib/date.scm 1004 */
 obj_t BgL_tmpz00_15478;
if(
INTEGERP(BgL_secondz00_11707))
{ /* Llib/date.scm 1004 */
BgL_tmpz00_15478 = BgL_secondz00_11707
; }  else 
{ 
 obj_t BgL_auxz00_15481;
BgL_auxz00_15481 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(42079L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secondz00_11707); 
FAILURE(BgL_auxz00_15481,BFALSE,BFALSE);} 
BgL_tmpz00_15477 = 
CINT(BgL_tmpz00_15478); } 
return 
bgl_make_date(((BGL_LONGLONG_T)0), BgL_tmpz00_15477, BgL_auxz00_15486, BgL_auxz00_15495, 
(int)(BgL_dayz00_11696), BgL_auxz00_15504, BgL_auxz00_15514, 0L, ((bool_t)0), 
(int)(-1L));} } } } } } } } } break;case 1L : 

{ /* Llib/date.scm 983 */
 obj_t BgL_dayz00_11715;
{ /* Llib/date.scm 983 */
 obj_t BgL_funz00_11716;
{ /* Llib/date.scm 983 */
 obj_t BgL_aux5459z00_11717;
BgL_aux5459z00_11717 = BGl_thezd2fixnumzd2grammarz00zz__datez00; 
if(
PROCEDUREP(BgL_aux5459z00_11717))
{ /* Llib/date.scm 983 */
BgL_funz00_11716 = BgL_aux5459z00_11717; }  else 
{ 
 obj_t BgL_auxz00_15527;
BgL_auxz00_15527 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41302L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5459z00_11717); 
FAILURE(BgL_auxz00_15527,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11716, 1))
{ /* Llib/date.scm 983 */
BgL_dayz00_11715 = 
(VA_PROCEDUREP( BgL_funz00_11716 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11716))(BGl_thezd2fixnumzd2grammarz00zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11716))(BGl_thezd2fixnumzd2grammarz00zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 983 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5891z00zz__datez00,BgL_funz00_11716);} } 
{ /* Llib/date.scm 983 */
 obj_t BgL_monthz00_11718;
{ /* Llib/date.scm 984 */
 obj_t BgL_funz00_11719;
{ /* Llib/date.scm 984 */
 obj_t BgL_aux5462z00_11720;
BgL_aux5462z00_11720 = BGl_monthzd2grammarzd2zz__datez00; 
if(
PROCEDUREP(BgL_aux5462z00_11720))
{ /* Llib/date.scm 984 */
BgL_funz00_11719 = BgL_aux5462z00_11720; }  else 
{ 
 obj_t BgL_auxz00_15540;
BgL_auxz00_15540 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41357L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5462z00_11720); 
FAILURE(BgL_auxz00_15540,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11719, 1))
{ /* Llib/date.scm 984 */
BgL_monthz00_11718 = 
(VA_PROCEDUREP( BgL_funz00_11719 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11719))(BGl_monthzd2grammarzd2zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11719))(BGl_monthzd2grammarzd2zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 984 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5894z00zz__datez00,BgL_funz00_11719);} } 
{ /* Llib/date.scm 984 */
 obj_t BgL_yearz00_11721;
{ /* Llib/date.scm 985 */
 obj_t BgL_funz00_11722;
{ /* Llib/date.scm 985 */
 obj_t BgL_aux5465z00_11723;
BgL_aux5465z00_11723 = BGl_thezd2fixnumzd2grammarz00zz__datez00; 
if(
PROCEDUREP(BgL_aux5465z00_11723))
{ /* Llib/date.scm 985 */
BgL_funz00_11722 = BgL_aux5465z00_11723; }  else 
{ 
 obj_t BgL_auxz00_15553;
BgL_auxz00_15553 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41406L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5465z00_11723); 
FAILURE(BgL_auxz00_15553,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11722, 1))
{ /* Llib/date.scm 985 */
BgL_yearz00_11721 = 
(VA_PROCEDUREP( BgL_funz00_11722 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11722))(BGl_thezd2fixnumzd2grammarz00zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11722))(BGl_thezd2fixnumzd2grammarz00zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 985 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5897z00zz__datez00,BgL_funz00_11722);} } 
{ /* Llib/date.scm 985 */

{ /* Llib/date.scm 986 */
 obj_t BgL_hourz00_11724;
{ /* Llib/date.scm 987 */
 obj_t BgL_funz00_11725;
{ /* Llib/date.scm 987 */
 obj_t BgL_aux5468z00_11726;
BgL_aux5468z00_11726 = BGl_timezd2grammarzd2zz__datez00; 
if(
PROCEDUREP(BgL_aux5468z00_11726))
{ /* Llib/date.scm 987 */
BgL_funz00_11725 = BgL_aux5468z00_11726; }  else 
{ 
 obj_t BgL_auxz00_15566;
BgL_auxz00_15566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41499L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5468z00_11726); 
FAILURE(BgL_auxz00_15566,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11725, 1))
{ /* Llib/date.scm 987 */
BgL_hourz00_11724 = 
(VA_PROCEDUREP( BgL_funz00_11725 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11725))(BGl_timezd2grammarzd2zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11725))(BGl_timezd2grammarzd2zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 987 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5900z00zz__datez00,BgL_funz00_11725);} } 
{ /* Llib/date.scm 987 */
 obj_t BgL_minutez00_11727; obj_t BgL_secondz00_11728;
{ /* Llib/date.scm 988 */
 obj_t BgL_tmpz00_11729;
{ /* Llib/date.scm 988 */
 int BgL_tmpz00_15577;
BgL_tmpz00_15577 = 
(int)(1L); 
BgL_tmpz00_11729 = 
BGL_MVALUES_VAL(BgL_tmpz00_15577); } 
{ /* Llib/date.scm 988 */
 int BgL_tmpz00_15580;
BgL_tmpz00_15580 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_15580, BUNSPEC); } 
BgL_minutez00_11727 = BgL_tmpz00_11729; } 
{ /* Llib/date.scm 988 */
 obj_t BgL_tmpz00_11730;
{ /* Llib/date.scm 988 */
 int BgL_tmpz00_15583;
BgL_tmpz00_15583 = 
(int)(2L); 
BgL_tmpz00_11730 = 
BGL_MVALUES_VAL(BgL_tmpz00_15583); } 
{ /* Llib/date.scm 988 */
 int BgL_tmpz00_15586;
BgL_tmpz00_15586 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_15586, BUNSPEC); } 
BgL_secondz00_11728 = BgL_tmpz00_11730; } 
{ /* Llib/date.scm 988 */
 obj_t BgL_za7oneza7_11731;
{ /* Llib/date.scm 988 */
 obj_t BgL_funz00_11732;
{ /* Llib/date.scm 988 */
 obj_t BgL_aux5471z00_11733;
BgL_aux5471z00_11733 = BGl_za7onezd2grammarz75zz__datez00; 
if(
PROCEDUREP(BgL_aux5471z00_11733))
{ /* Llib/date.scm 988 */
BgL_funz00_11732 = BgL_aux5471z00_11733; }  else 
{ 
 obj_t BgL_auxz00_15591;
BgL_auxz00_15591 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41551L), BGl_string5785z00zz__datez00, BGl_string5867z00zz__datez00, BgL_aux5471z00_11733); 
FAILURE(BgL_auxz00_15591,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_11732, 1))
{ /* Llib/date.scm 988 */
BgL_za7oneza7_11731 = 
(VA_PROCEDUREP( BgL_funz00_11732 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_11732))(BGl_za7onezd2grammarz75zz__datez00, BgL_iportz00_9768, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_11732))(BGl_za7onezd2grammarz75zz__datez00, BgL_iportz00_9768) ); }  else 
{ /* Llib/date.scm 988 */
FAILURE(BGl_string5868z00zz__datez00,BGl_list5903z00zz__datez00,BgL_funz00_11732);} } 
{ /* Llib/date.scm 993 */
 obj_t BgL_arg1684z00_11734;
{ /* Llib/date.scm 993 */
 bool_t BgL_test6913z00_15602;
{ /* Llib/date.scm 993 */
 long BgL_n1z00_11735;
{ /* Llib/date.scm 993 */
 obj_t BgL_tmpz00_15603;
if(
INTEGERP(BgL_yearz00_11721))
{ /* Llib/date.scm 993 */
BgL_tmpz00_15603 = BgL_yearz00_11721
; }  else 
{ 
 obj_t BgL_auxz00_15606;
BgL_auxz00_15606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41683L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_11721); 
FAILURE(BgL_auxz00_15606,BFALSE,BFALSE);} 
BgL_n1z00_11735 = 
(long)CINT(BgL_tmpz00_15603); } 
BgL_test6913z00_15602 = 
(BgL_n1z00_11735<100L); } 
if(BgL_test6913z00_15602)
{ /* Llib/date.scm 988 */
 obj_t BgL_tmpz00_15612;
if(
INTEGERP(BgL_yearz00_11721))
{ /* Llib/date.scm 993 */
BgL_tmpz00_15612 = BgL_yearz00_11721
; }  else 
{ 
 obj_t BgL_auxz00_15615;
BgL_auxz00_15615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41698L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_11721); 
FAILURE(BgL_auxz00_15615,BFALSE,BFALSE);} 
BgL_arg1684z00_11734 = 
ADDFX(BgL_tmpz00_15612, 
BINT(2000L)); }  else 
{ /* Llib/date.scm 993 */
BgL_arg1684z00_11734 = BgL_yearz00_11721; } } 
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_za7oneza7_11731))
{ /* Llib/date.scm 233 */
 long BgL_auxz00_15677; int BgL_auxz00_15668; int BgL_auxz00_15659; int BgL_auxz00_15650; int BgL_auxz00_15641; int BgL_auxz00_15632; int BgL_tmpz00_15623;
{ /* Llib/date.scm 995 */
 obj_t BgL_tmpz00_15678;
if(
INTEGERP(BgL_za7oneza7_11731))
{ /* Llib/date.scm 995 */
BgL_tmpz00_15678 = BgL_za7oneza7_11731
; }  else 
{ 
 obj_t BgL_auxz00_15681;
BgL_auxz00_15681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41744L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_za7oneza7_11731); 
FAILURE(BgL_auxz00_15681,BFALSE,BFALSE);} 
BgL_auxz00_15677 = 
(long)CINT(BgL_tmpz00_15678); } 
{ /* Llib/date.scm 993 */
 obj_t BgL_tmpz00_15669;
if(
INTEGERP(BgL_arg1684z00_11734))
{ /* Llib/date.scm 993 */
BgL_tmpz00_15669 = BgL_arg1684z00_11734
; }  else 
{ 
 obj_t BgL_auxz00_15672;
BgL_auxz00_15672 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41713L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg1684z00_11734); 
FAILURE(BgL_auxz00_15672,BFALSE,BFALSE);} 
BgL_auxz00_15668 = 
CINT(BgL_tmpz00_15669); } 
{ /* Llib/date.scm 992 */
 obj_t BgL_tmpz00_15660;
if(
INTEGERP(BgL_monthz00_11718))
{ /* Llib/date.scm 992 */
BgL_tmpz00_15660 = BgL_monthz00_11718
; }  else 
{ 
 obj_t BgL_auxz00_15663;
BgL_auxz00_15663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41657L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_11718); 
FAILURE(BgL_auxz00_15663,BFALSE,BFALSE);} 
BgL_auxz00_15659 = 
CINT(BgL_tmpz00_15660); } 
{ /* Llib/date.scm 994 */
 obj_t BgL_tmpz00_15651;
if(
INTEGERP(BgL_dayz00_11715))
{ /* Llib/date.scm 994 */
BgL_tmpz00_15651 = BgL_dayz00_11715
; }  else 
{ 
 obj_t BgL_auxz00_15654;
BgL_auxz00_15654 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41725L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_11715); 
FAILURE(BgL_auxz00_15654,BFALSE,BFALSE);} 
BgL_auxz00_15650 = 
CINT(BgL_tmpz00_15651); } 
{ /* Llib/date.scm 991 */
 obj_t BgL_tmpz00_15642;
if(
INTEGERP(BgL_hourz00_11724))
{ /* Llib/date.scm 991 */
BgL_tmpz00_15642 = BgL_hourz00_11724
; }  else 
{ 
 obj_t BgL_auxz00_15645;
BgL_auxz00_15645 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41640L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_11724); 
FAILURE(BgL_auxz00_15645,BFALSE,BFALSE);} 
BgL_auxz00_15641 = 
CINT(BgL_tmpz00_15642); } 
{ /* Llib/date.scm 990 */
 obj_t BgL_tmpz00_15633;
if(
INTEGERP(BgL_minutez00_11727))
{ /* Llib/date.scm 990 */
BgL_tmpz00_15633 = BgL_minutez00_11727
; }  else 
{ 
 obj_t BgL_auxz00_15636;
BgL_auxz00_15636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41622L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minutez00_11727); 
FAILURE(BgL_auxz00_15636,BFALSE,BFALSE);} 
BgL_auxz00_15632 = 
CINT(BgL_tmpz00_15633); } 
{ /* Llib/date.scm 989 */
 obj_t BgL_tmpz00_15624;
if(
INTEGERP(BgL_secondz00_11728))
{ /* Llib/date.scm 989 */
BgL_tmpz00_15624 = BgL_secondz00_11728
; }  else 
{ 
 obj_t BgL_auxz00_15627;
BgL_auxz00_15627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41605L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secondz00_11728); 
FAILURE(BgL_auxz00_15627,BFALSE,BFALSE);} 
BgL_tmpz00_15623 = 
CINT(BgL_tmpz00_15624); } 
return 
bgl_make_date(((BGL_LONGLONG_T)0), BgL_tmpz00_15623, BgL_auxz00_15632, BgL_auxz00_15641, BgL_auxz00_15650, BgL_auxz00_15659, BgL_auxz00_15668, BgL_auxz00_15677, ((bool_t)1), 
(int)(-1L));}  else 
{ /* Llib/date.scm 234 */
 int BgL_auxz00_15733; int BgL_auxz00_15724; int BgL_auxz00_15715; int BgL_auxz00_15706; int BgL_auxz00_15697; int BgL_tmpz00_15688;
{ /* Llib/date.scm 993 */
 obj_t BgL_tmpz00_15734;
if(
INTEGERP(BgL_arg1684z00_11734))
{ /* Llib/date.scm 993 */
BgL_tmpz00_15734 = BgL_arg1684z00_11734
; }  else 
{ 
 obj_t BgL_auxz00_15737;
BgL_auxz00_15737 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41713L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg1684z00_11734); 
FAILURE(BgL_auxz00_15737,BFALSE,BFALSE);} 
BgL_auxz00_15733 = 
CINT(BgL_tmpz00_15734); } 
{ /* Llib/date.scm 992 */
 obj_t BgL_tmpz00_15725;
if(
INTEGERP(BgL_monthz00_11718))
{ /* Llib/date.scm 992 */
BgL_tmpz00_15725 = BgL_monthz00_11718
; }  else 
{ 
 obj_t BgL_auxz00_15728;
BgL_auxz00_15728 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41657L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_11718); 
FAILURE(BgL_auxz00_15728,BFALSE,BFALSE);} 
BgL_auxz00_15724 = 
CINT(BgL_tmpz00_15725); } 
{ /* Llib/date.scm 994 */
 obj_t BgL_tmpz00_15716;
if(
INTEGERP(BgL_dayz00_11715))
{ /* Llib/date.scm 994 */
BgL_tmpz00_15716 = BgL_dayz00_11715
; }  else 
{ 
 obj_t BgL_auxz00_15719;
BgL_auxz00_15719 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41725L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_11715); 
FAILURE(BgL_auxz00_15719,BFALSE,BFALSE);} 
BgL_auxz00_15715 = 
CINT(BgL_tmpz00_15716); } 
{ /* Llib/date.scm 991 */
 obj_t BgL_tmpz00_15707;
if(
INTEGERP(BgL_hourz00_11724))
{ /* Llib/date.scm 991 */
BgL_tmpz00_15707 = BgL_hourz00_11724
; }  else 
{ 
 obj_t BgL_auxz00_15710;
BgL_auxz00_15710 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41640L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_11724); 
FAILURE(BgL_auxz00_15710,BFALSE,BFALSE);} 
BgL_auxz00_15706 = 
CINT(BgL_tmpz00_15707); } 
{ /* Llib/date.scm 990 */
 obj_t BgL_tmpz00_15698;
if(
INTEGERP(BgL_minutez00_11727))
{ /* Llib/date.scm 990 */
BgL_tmpz00_15698 = BgL_minutez00_11727
; }  else 
{ 
 obj_t BgL_auxz00_15701;
BgL_auxz00_15701 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41622L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minutez00_11727); 
FAILURE(BgL_auxz00_15701,BFALSE,BFALSE);} 
BgL_auxz00_15697 = 
CINT(BgL_tmpz00_15698); } 
{ /* Llib/date.scm 989 */
 obj_t BgL_tmpz00_15689;
if(
INTEGERP(BgL_secondz00_11728))
{ /* Llib/date.scm 989 */
BgL_tmpz00_15689 = BgL_secondz00_11728
; }  else 
{ 
 obj_t BgL_auxz00_15692;
BgL_auxz00_15692 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(41605L), BGl_string5785z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secondz00_11728); 
FAILURE(BgL_auxz00_15692,BFALSE,BFALSE);} 
BgL_tmpz00_15688 = 
CINT(BgL_tmpz00_15689); } 
return 
bgl_make_date(((BGL_LONGLONG_T)0), BgL_tmpz00_15688, BgL_auxz00_15697, BgL_auxz00_15706, BgL_auxz00_15715, BgL_auxz00_15724, BgL_auxz00_15733, 0L, ((bool_t)0), 
(int)(-1L));} } } } } } } } } break;case 0L : 

goto BgL_ignorez00_11682;break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_11693));} } } } } 

}



/* date? */
BGL_EXPORTED_DEF bool_t BGl_datezf3zf3zz__datez00(obj_t BgL_objz00_3)
{
{ /* Llib/date.scm 221 */
return 
BGL_DATEP(BgL_objz00_3);} 

}



/* &date? */
obj_t BGl_z62datezf3z91zz__datez00(obj_t BgL_envz00_9769, obj_t BgL_objz00_9770)
{
{ /* Llib/date.scm 221 */
return 
BBOOL(
BGl_datezf3zf3zz__datez00(BgL_objz00_9770));} 

}



/* _make-date */
obj_t BGl__makezd2datezd2zz__datez00(obj_t BgL_env1316z00_14, obj_t BgL_opt1315z00_13)
{
{ /* Llib/date.scm 227 */
{ /* Llib/date.scm 227 */

{ /* Llib/date.scm 227 */
 obj_t BgL_dayz00_3577;
BgL_dayz00_3577 = 
BINT(1L); 
{ /* Llib/date.scm 227 */
 obj_t BgL_dstz00_3578;
BgL_dstz00_3578 = 
BINT(-1L); 
{ /* Llib/date.scm 227 */
 obj_t BgL_hourz00_3579;
BgL_hourz00_3579 = 
BINT(0L); 
{ /* Llib/date.scm 227 */
 obj_t BgL_minz00_3580;
BgL_minz00_3580 = 
BINT(0L); 
{ /* Llib/date.scm 227 */
 obj_t BgL_monthz00_3581;
BgL_monthz00_3581 = 
BINT(1L); 
{ /* Llib/date.scm 227 */
 obj_t BgL_nsecz00_3582;
BgL_nsecz00_3582 = BGl_llong5907z00zz__datez00; 
{ /* Llib/date.scm 227 */
 obj_t BgL_secz00_3583;
BgL_secz00_3583 = 
BINT(0L); 
{ /* Llib/date.scm 227 */
 obj_t BgL_timeza7oneza7_3584;
BgL_timeza7oneza7_3584 = BFALSE; 
{ /* Llib/date.scm 227 */
 obj_t BgL_yearz00_3585;
BgL_yearz00_3585 = 
BINT(1970L); 
{ /* Llib/date.scm 227 */

{ 
 long BgL_iz00_3586;
BgL_iz00_3586 = 0L; 
BgL_check1319z00_3587:
if(
(BgL_iz00_3586==
VECTOR_LENGTH(BgL_opt1315z00_13)))
{ /* Llib/date.scm 227 */BNIL; }  else 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6931z00_15760;
{ /* Llib/date.scm 227 */
 obj_t BgL_arg2660z00_3593;
{ /* Llib/date.scm 227 */
 bool_t BgL_test6932z00_15761;
{ /* Llib/date.scm 227 */
 long BgL_tmpz00_15762;
BgL_tmpz00_15762 = 
VECTOR_LENGTH(BgL_opt1315z00_13); 
BgL_test6932z00_15761 = 
BOUND_CHECK(BgL_iz00_3586, BgL_tmpz00_15762); } 
if(BgL_test6932z00_15761)
{ /* Llib/date.scm 227 */
BgL_arg2660z00_3593 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_iz00_3586); }  else 
{ 
 obj_t BgL_auxz00_15766;
BgL_auxz00_15766 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5908z00zz__datez00, BgL_opt1315z00_13, 
(int)(
VECTOR_LENGTH(BgL_opt1315z00_13)), 
(int)(BgL_iz00_3586)); 
FAILURE(BgL_auxz00_15766,BFALSE,BFALSE);} } 
BgL_test6931z00_15760 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg2660z00_3593, BGl_list5909z00zz__datez00)); } 
if(BgL_test6931z00_15760)
{ 
 long BgL_iz00_15775;
BgL_iz00_15775 = 
(BgL_iz00_3586+2L); 
BgL_iz00_3586 = BgL_iz00_15775; 
goto BgL_check1319z00_3587;}  else 
{ /* Llib/date.scm 227 */
 obj_t BgL_arg2659z00_3592;
{ /* Llib/date.scm 227 */
 bool_t BgL_test6933z00_15777;
{ /* Llib/date.scm 227 */
 long BgL_tmpz00_15778;
BgL_tmpz00_15778 = 
VECTOR_LENGTH(BgL_opt1315z00_13); 
BgL_test6933z00_15777 = 
BOUND_CHECK(BgL_iz00_3586, BgL_tmpz00_15778); } 
if(BgL_test6933z00_15777)
{ /* Llib/date.scm 227 */
BgL_arg2659z00_3592 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_iz00_3586); }  else 
{ 
 obj_t BgL_auxz00_15782;
BgL_auxz00_15782 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5908z00zz__datez00, BgL_opt1315z00_13, 
(int)(
VECTOR_LENGTH(BgL_opt1315z00_13)), 
(int)(BgL_iz00_3586)); 
FAILURE(BgL_auxz00_15782,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol5928z00zz__datez00, BGl_string5930z00zz__datez00, BgL_arg2659z00_3592); } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1321z00_3594;
BgL_index1321z00_3594 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5910z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6934z00_15792;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8104;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15793;
if(
INTEGERP(BgL_index1321z00_3594))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15793 = BgL_index1321z00_3594
; }  else 
{ 
 obj_t BgL_auxz00_15796;
BgL_auxz00_15796 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1321z00_3594); 
FAILURE(BgL_auxz00_15796,BFALSE,BFALSE);} 
BgL_n1z00_8104 = 
(long)CINT(BgL_tmpz00_15793); } 
BgL_test6934z00_15792 = 
(BgL_n1z00_8104>=0L); } 
if(BgL_test6934z00_15792)
{ 
 long BgL_auxz00_15802;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15803;
if(
INTEGERP(BgL_index1321z00_3594))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15803 = BgL_index1321z00_3594
; }  else 
{ 
 obj_t BgL_auxz00_15806;
BgL_auxz00_15806 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1321z00_3594); 
FAILURE(BgL_auxz00_15806,BFALSE,BFALSE);} 
BgL_auxz00_15802 = 
(long)CINT(BgL_tmpz00_15803); } 
BgL_dayz00_3577 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15802); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1322z00_3596;
BgL_index1322z00_3596 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5912z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6937z00_15814;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8105;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15815;
if(
INTEGERP(BgL_index1322z00_3596))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15815 = BgL_index1322z00_3596
; }  else 
{ 
 obj_t BgL_auxz00_15818;
BgL_auxz00_15818 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1322z00_3596); 
FAILURE(BgL_auxz00_15818,BFALSE,BFALSE);} 
BgL_n1z00_8105 = 
(long)CINT(BgL_tmpz00_15815); } 
BgL_test6937z00_15814 = 
(BgL_n1z00_8105>=0L); } 
if(BgL_test6937z00_15814)
{ 
 long BgL_auxz00_15824;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15825;
if(
INTEGERP(BgL_index1322z00_3596))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15825 = BgL_index1322z00_3596
; }  else 
{ 
 obj_t BgL_auxz00_15828;
BgL_auxz00_15828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1322z00_3596); 
FAILURE(BgL_auxz00_15828,BFALSE,BFALSE);} 
BgL_auxz00_15824 = 
(long)CINT(BgL_tmpz00_15825); } 
BgL_dstz00_3578 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15824); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1323z00_3598;
BgL_index1323z00_3598 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5914z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6940z00_15836;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8106;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15837;
if(
INTEGERP(BgL_index1323z00_3598))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15837 = BgL_index1323z00_3598
; }  else 
{ 
 obj_t BgL_auxz00_15840;
BgL_auxz00_15840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1323z00_3598); 
FAILURE(BgL_auxz00_15840,BFALSE,BFALSE);} 
BgL_n1z00_8106 = 
(long)CINT(BgL_tmpz00_15837); } 
BgL_test6940z00_15836 = 
(BgL_n1z00_8106>=0L); } 
if(BgL_test6940z00_15836)
{ 
 long BgL_auxz00_15846;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15847;
if(
INTEGERP(BgL_index1323z00_3598))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15847 = BgL_index1323z00_3598
; }  else 
{ 
 obj_t BgL_auxz00_15850;
BgL_auxz00_15850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1323z00_3598); 
FAILURE(BgL_auxz00_15850,BFALSE,BFALSE);} 
BgL_auxz00_15846 = 
(long)CINT(BgL_tmpz00_15847); } 
BgL_hourz00_3579 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15846); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1324z00_3600;
BgL_index1324z00_3600 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5916z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6943z00_15858;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8107;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15859;
if(
INTEGERP(BgL_index1324z00_3600))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15859 = BgL_index1324z00_3600
; }  else 
{ 
 obj_t BgL_auxz00_15862;
BgL_auxz00_15862 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1324z00_3600); 
FAILURE(BgL_auxz00_15862,BFALSE,BFALSE);} 
BgL_n1z00_8107 = 
(long)CINT(BgL_tmpz00_15859); } 
BgL_test6943z00_15858 = 
(BgL_n1z00_8107>=0L); } 
if(BgL_test6943z00_15858)
{ 
 long BgL_auxz00_15868;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15869;
if(
INTEGERP(BgL_index1324z00_3600))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15869 = BgL_index1324z00_3600
; }  else 
{ 
 obj_t BgL_auxz00_15872;
BgL_auxz00_15872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1324z00_3600); 
FAILURE(BgL_auxz00_15872,BFALSE,BFALSE);} 
BgL_auxz00_15868 = 
(long)CINT(BgL_tmpz00_15869); } 
BgL_minz00_3580 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15868); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1325z00_3602;
BgL_index1325z00_3602 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5918z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6946z00_15880;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8108;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15881;
if(
INTEGERP(BgL_index1325z00_3602))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15881 = BgL_index1325z00_3602
; }  else 
{ 
 obj_t BgL_auxz00_15884;
BgL_auxz00_15884 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1325z00_3602); 
FAILURE(BgL_auxz00_15884,BFALSE,BFALSE);} 
BgL_n1z00_8108 = 
(long)CINT(BgL_tmpz00_15881); } 
BgL_test6946z00_15880 = 
(BgL_n1z00_8108>=0L); } 
if(BgL_test6946z00_15880)
{ 
 long BgL_auxz00_15890;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15891;
if(
INTEGERP(BgL_index1325z00_3602))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15891 = BgL_index1325z00_3602
; }  else 
{ 
 obj_t BgL_auxz00_15894;
BgL_auxz00_15894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1325z00_3602); 
FAILURE(BgL_auxz00_15894,BFALSE,BFALSE);} 
BgL_auxz00_15890 = 
(long)CINT(BgL_tmpz00_15891); } 
BgL_monthz00_3581 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15890); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1326z00_3604;
BgL_index1326z00_3604 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5920z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6949z00_15902;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8109;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15903;
if(
INTEGERP(BgL_index1326z00_3604))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15903 = BgL_index1326z00_3604
; }  else 
{ 
 obj_t BgL_auxz00_15906;
BgL_auxz00_15906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1326z00_3604); 
FAILURE(BgL_auxz00_15906,BFALSE,BFALSE);} 
BgL_n1z00_8109 = 
(long)CINT(BgL_tmpz00_15903); } 
BgL_test6949z00_15902 = 
(BgL_n1z00_8109>=0L); } 
if(BgL_test6949z00_15902)
{ 
 long BgL_auxz00_15912;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15913;
if(
INTEGERP(BgL_index1326z00_3604))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15913 = BgL_index1326z00_3604
; }  else 
{ 
 obj_t BgL_auxz00_15916;
BgL_auxz00_15916 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1326z00_3604); 
FAILURE(BgL_auxz00_15916,BFALSE,BFALSE);} 
BgL_auxz00_15912 = 
(long)CINT(BgL_tmpz00_15913); } 
BgL_nsecz00_3582 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15912); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1327z00_3606;
BgL_index1327z00_3606 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5922z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6952z00_15924;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8110;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15925;
if(
INTEGERP(BgL_index1327z00_3606))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15925 = BgL_index1327z00_3606
; }  else 
{ 
 obj_t BgL_auxz00_15928;
BgL_auxz00_15928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1327z00_3606); 
FAILURE(BgL_auxz00_15928,BFALSE,BFALSE);} 
BgL_n1z00_8110 = 
(long)CINT(BgL_tmpz00_15925); } 
BgL_test6952z00_15924 = 
(BgL_n1z00_8110>=0L); } 
if(BgL_test6952z00_15924)
{ 
 long BgL_auxz00_15934;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15935;
if(
INTEGERP(BgL_index1327z00_3606))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15935 = BgL_index1327z00_3606
; }  else 
{ 
 obj_t BgL_auxz00_15938;
BgL_auxz00_15938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1327z00_3606); 
FAILURE(BgL_auxz00_15938,BFALSE,BFALSE);} 
BgL_auxz00_15934 = 
(long)CINT(BgL_tmpz00_15935); } 
BgL_secz00_3583 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15934); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1328z00_3608;
BgL_index1328z00_3608 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5924z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6955z00_15946;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8111;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15947;
if(
INTEGERP(BgL_index1328z00_3608))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15947 = BgL_index1328z00_3608
; }  else 
{ 
 obj_t BgL_auxz00_15950;
BgL_auxz00_15950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1328z00_3608); 
FAILURE(BgL_auxz00_15950,BFALSE,BFALSE);} 
BgL_n1z00_8111 = 
(long)CINT(BgL_tmpz00_15947); } 
BgL_test6955z00_15946 = 
(BgL_n1z00_8111>=0L); } 
if(BgL_test6955z00_15946)
{ 
 long BgL_auxz00_15956;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15957;
if(
INTEGERP(BgL_index1328z00_3608))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15957 = BgL_index1328z00_3608
; }  else 
{ 
 obj_t BgL_auxz00_15960;
BgL_auxz00_15960 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1328z00_3608); 
FAILURE(BgL_auxz00_15960,BFALSE,BFALSE);} 
BgL_auxz00_15956 = 
(long)CINT(BgL_tmpz00_15957); } 
BgL_timeza7oneza7_3584 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15956); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_index1329z00_3610;
BgL_index1329z00_3610 = 
BGl_search1318ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1315z00_13), BgL_opt1315z00_13, BGl_keyword5926z00zz__datez00, 0L); 
{ /* Llib/date.scm 227 */
 bool_t BgL_test6958z00_15968;
{ /* Llib/date.scm 227 */
 long BgL_n1z00_8112;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15969;
if(
INTEGERP(BgL_index1329z00_3610))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15969 = BgL_index1329z00_3610
; }  else 
{ 
 obj_t BgL_auxz00_15972;
BgL_auxz00_15972 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1329z00_3610); 
FAILURE(BgL_auxz00_15972,BFALSE,BFALSE);} 
BgL_n1z00_8112 = 
(long)CINT(BgL_tmpz00_15969); } 
BgL_test6958z00_15968 = 
(BgL_n1z00_8112>=0L); } 
if(BgL_test6958z00_15968)
{ 
 long BgL_auxz00_15978;
{ /* Llib/date.scm 227 */
 obj_t BgL_tmpz00_15979;
if(
INTEGERP(BgL_index1329z00_3610))
{ /* Llib/date.scm 227 */
BgL_tmpz00_15979 = BgL_index1329z00_3610
; }  else 
{ 
 obj_t BgL_auxz00_15982;
BgL_auxz00_15982 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(10879L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1329z00_3610); 
FAILURE(BgL_auxz00_15982,BFALSE,BFALSE);} 
BgL_auxz00_15978 = 
(long)CINT(BgL_tmpz00_15979); } 
BgL_yearz00_3585 = 
VECTOR_REF(BgL_opt1315z00_13,BgL_auxz00_15978); }  else 
{ /* Llib/date.scm 227 */BFALSE; } } } 
{ /* Llib/date.scm 227 */
 obj_t BgL_dayz00_3612;
BgL_dayz00_3612 = BgL_dayz00_3577; 
{ /* Llib/date.scm 227 */
 obj_t BgL_dstz00_3613;
BgL_dstz00_3613 = BgL_dstz00_3578; 
{ /* Llib/date.scm 227 */
 obj_t BgL_hourz00_3614;
BgL_hourz00_3614 = BgL_hourz00_3579; 
{ /* Llib/date.scm 227 */
 obj_t BgL_minz00_3615;
BgL_minz00_3615 = BgL_minz00_3580; 
{ /* Llib/date.scm 227 */
 obj_t BgL_monthz00_3616;
BgL_monthz00_3616 = BgL_monthz00_3581; 
{ /* Llib/date.scm 227 */
 obj_t BgL_nsecz00_3617;
BgL_nsecz00_3617 = BgL_nsecz00_3582; 
{ /* Llib/date.scm 227 */
 obj_t BgL_secz00_3618;
BgL_secz00_3618 = BgL_secz00_3583; 
{ /* Llib/date.scm 227 */
 obj_t BgL_timeza7oneza7_3619;
BgL_timeza7oneza7_3619 = BgL_timeza7oneza7_3584; 
{ /* Llib/date.scm 227 */
 obj_t BgL_yearz00_3620;
BgL_yearz00_3620 = BgL_yearz00_3585; 
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_timeza7oneza7_3619))
{ /* Llib/date.scm 233 */
 int BgL_auxz00_16062; long BgL_auxz00_16053; int BgL_auxz00_16044; int BgL_auxz00_16035; int BgL_auxz00_16026; int BgL_auxz00_16017; int BgL_auxz00_16008; int BgL_auxz00_15999; BGL_LONGLONG_T BgL_tmpz00_15990;
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16063;
if(
INTEGERP(BgL_dstz00_3613))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16063 = BgL_dstz00_3613
; }  else 
{ 
 obj_t BgL_auxz00_16066;
BgL_auxz00_16066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11095L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dstz00_3613); 
FAILURE(BgL_auxz00_16066,BFALSE,BFALSE);} 
BgL_auxz00_16062 = 
CINT(BgL_tmpz00_16063); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16054;
if(
INTEGERP(BgL_timeza7oneza7_3619))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16054 = BgL_timeza7oneza7_3619
; }  else 
{ 
 obj_t BgL_auxz00_16057;
BgL_auxz00_16057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11083L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_timeza7oneza7_3619); 
FAILURE(BgL_auxz00_16057,BFALSE,BFALSE);} 
BgL_auxz00_16053 = 
(long)CINT(BgL_tmpz00_16054); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16045;
if(
INTEGERP(BgL_yearz00_3620))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16045 = BgL_yearz00_3620
; }  else 
{ 
 obj_t BgL_auxz00_16048;
BgL_auxz00_16048 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11078L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_3620); 
FAILURE(BgL_auxz00_16048,BFALSE,BFALSE);} 
BgL_auxz00_16044 = 
CINT(BgL_tmpz00_16045); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16036;
if(
INTEGERP(BgL_monthz00_3616))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16036 = BgL_monthz00_3616
; }  else 
{ 
 obj_t BgL_auxz00_16039;
BgL_auxz00_16039 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11072L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_3616); 
FAILURE(BgL_auxz00_16039,BFALSE,BFALSE);} 
BgL_auxz00_16035 = 
CINT(BgL_tmpz00_16036); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16027;
if(
INTEGERP(BgL_dayz00_3612))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16027 = BgL_dayz00_3612
; }  else 
{ 
 obj_t BgL_auxz00_16030;
BgL_auxz00_16030 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11068L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_3612); 
FAILURE(BgL_auxz00_16030,BFALSE,BFALSE);} 
BgL_auxz00_16026 = 
CINT(BgL_tmpz00_16027); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16018;
if(
INTEGERP(BgL_hourz00_3614))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16018 = BgL_hourz00_3614
; }  else 
{ 
 obj_t BgL_auxz00_16021;
BgL_auxz00_16021 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11063L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_3614); 
FAILURE(BgL_auxz00_16021,BFALSE,BFALSE);} 
BgL_auxz00_16017 = 
CINT(BgL_tmpz00_16018); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16009;
if(
INTEGERP(BgL_minz00_3615))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16009 = BgL_minz00_3615
; }  else 
{ 
 obj_t BgL_auxz00_16012;
BgL_auxz00_16012 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11059L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minz00_3615); 
FAILURE(BgL_auxz00_16012,BFALSE,BFALSE);} 
BgL_auxz00_16008 = 
CINT(BgL_tmpz00_16009); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16000;
if(
INTEGERP(BgL_secz00_3618))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16000 = BgL_secz00_3618
; }  else 
{ 
 obj_t BgL_auxz00_16003;
BgL_auxz00_16003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11055L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secz00_3618); 
FAILURE(BgL_auxz00_16003,BFALSE,BFALSE);} 
BgL_auxz00_15999 = 
CINT(BgL_tmpz00_16000); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_15991;
if(
LLONGP(BgL_nsecz00_3617))
{ /* Llib/date.scm 233 */
BgL_tmpz00_15991 = BgL_nsecz00_3617
; }  else 
{ 
 obj_t BgL_auxz00_15994;
BgL_auxz00_15994 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11050L), BGl_string5931z00zz__datez00, BGl_string5932z00zz__datez00, BgL_nsecz00_3617); 
FAILURE(BgL_auxz00_15994,BFALSE,BFALSE);} 
BgL_tmpz00_15990 = 
BLLONG_TO_LLONG(BgL_tmpz00_15991); } 
return 
bgl_make_date(BgL_tmpz00_15990, BgL_auxz00_15999, BgL_auxz00_16008, BgL_auxz00_16017, BgL_auxz00_16026, BgL_auxz00_16035, BgL_auxz00_16044, BgL_auxz00_16053, ((bool_t)1), BgL_auxz00_16062);}  else 
{ /* Llib/date.scm 234 */
 int BgL_auxz00_16135; int BgL_auxz00_16126; int BgL_auxz00_16117; int BgL_auxz00_16108; int BgL_auxz00_16099; int BgL_auxz00_16090; int BgL_auxz00_16081; BGL_LONGLONG_T BgL_tmpz00_16072;
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16136;
if(
INTEGERP(BgL_dstz00_3613))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16136 = BgL_dstz00_3613
; }  else 
{ 
 obj_t BgL_auxz00_16139;
BgL_auxz00_16139 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11156L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dstz00_3613); 
FAILURE(BgL_auxz00_16139,BFALSE,BFALSE);} 
BgL_auxz00_16135 = 
CINT(BgL_tmpz00_16136); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16127;
if(
INTEGERP(BgL_yearz00_3620))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16127 = BgL_yearz00_3620
; }  else 
{ 
 obj_t BgL_auxz00_16130;
BgL_auxz00_16130 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11146L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_3620); 
FAILURE(BgL_auxz00_16130,BFALSE,BFALSE);} 
BgL_auxz00_16126 = 
CINT(BgL_tmpz00_16127); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16118;
if(
INTEGERP(BgL_monthz00_3616))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16118 = BgL_monthz00_3616
; }  else 
{ 
 obj_t BgL_auxz00_16121;
BgL_auxz00_16121 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11140L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_3616); 
FAILURE(BgL_auxz00_16121,BFALSE,BFALSE);} 
BgL_auxz00_16117 = 
CINT(BgL_tmpz00_16118); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16109;
if(
INTEGERP(BgL_dayz00_3612))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16109 = BgL_dayz00_3612
; }  else 
{ 
 obj_t BgL_auxz00_16112;
BgL_auxz00_16112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11136L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_3612); 
FAILURE(BgL_auxz00_16112,BFALSE,BFALSE);} 
BgL_auxz00_16108 = 
CINT(BgL_tmpz00_16109); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16100;
if(
INTEGERP(BgL_hourz00_3614))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16100 = BgL_hourz00_3614
; }  else 
{ 
 obj_t BgL_auxz00_16103;
BgL_auxz00_16103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11131L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_3614); 
FAILURE(BgL_auxz00_16103,BFALSE,BFALSE);} 
BgL_auxz00_16099 = 
CINT(BgL_tmpz00_16100); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16091;
if(
INTEGERP(BgL_minz00_3615))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16091 = BgL_minz00_3615
; }  else 
{ 
 obj_t BgL_auxz00_16094;
BgL_auxz00_16094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11127L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minz00_3615); 
FAILURE(BgL_auxz00_16094,BFALSE,BFALSE);} 
BgL_auxz00_16090 = 
CINT(BgL_tmpz00_16091); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16082;
if(
INTEGERP(BgL_secz00_3618))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16082 = BgL_secz00_3618
; }  else 
{ 
 obj_t BgL_auxz00_16085;
BgL_auxz00_16085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11123L), BGl_string5931z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secz00_3618); 
FAILURE(BgL_auxz00_16085,BFALSE,BFALSE);} 
BgL_auxz00_16081 = 
CINT(BgL_tmpz00_16082); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16073;
if(
LLONGP(BgL_nsecz00_3617))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16073 = BgL_nsecz00_3617
; }  else 
{ 
 obj_t BgL_auxz00_16076;
BgL_auxz00_16076 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11118L), BGl_string5931z00zz__datez00, BGl_string5932z00zz__datez00, BgL_nsecz00_3617); 
FAILURE(BgL_auxz00_16076,BFALSE,BFALSE);} 
BgL_tmpz00_16072 = 
BLLONG_TO_LLONG(BgL_tmpz00_16073); } 
return 
bgl_make_date(BgL_tmpz00_16072, BgL_auxz00_16081, BgL_auxz00_16090, BgL_auxz00_16099, BgL_auxz00_16108, BgL_auxz00_16117, BgL_auxz00_16126, 0L, ((bool_t)0), BgL_auxz00_16135);} } } } } } } } } } } } } } } } } } } } } } 

}



/* search1318~0 */
obj_t BGl_search1318ze70ze7zz__datez00(long BgL_l1317z00_9884, obj_t BgL_opt1315z00_9883, obj_t BgL_k1z00_3574, long BgL_iz00_3575)
{
{ /* Llib/date.scm 227 */
BGl_search1318ze70ze7zz__datez00:
if(
(BgL_iz00_3575==BgL_l1317z00_9884))
{ /* Llib/date.scm 227 */
return 
BINT(-1L);}  else 
{ /* Llib/date.scm 227 */
if(
(BgL_iz00_3575==
(BgL_l1317z00_9884-1L)))
{ /* Llib/date.scm 227 */
return 
BGl_errorz00zz__errorz00(BGl_symbol5928z00zz__datez00, BGl_string5933z00zz__datez00, 
BINT(
VECTOR_LENGTH(BgL_opt1315z00_9883)));}  else 
{ /* Llib/date.scm 227 */
 obj_t BgL_vz00_3625;
BgL_vz00_3625 = 
VECTOR_REF(BgL_opt1315z00_9883,BgL_iz00_3575); 
if(
(BgL_vz00_3625==BgL_k1z00_3574))
{ /* Llib/date.scm 227 */
return 
BINT(
(BgL_iz00_3575+1L));}  else 
{ 
 long BgL_iz00_16159;
BgL_iz00_16159 = 
(BgL_iz00_3575+2L); 
BgL_iz00_3575 = BgL_iz00_16159; 
goto BGl_search1318ze70ze7zz__datez00;} } } } 

}



/* make-date */
BGL_EXPORTED_DEF obj_t BGl_makezd2datezd2zz__datez00(obj_t BgL_dayz00_4, obj_t BgL_dstz00_5, obj_t BgL_hourz00_6, obj_t BgL_minz00_7, obj_t BgL_monthz00_8, obj_t BgL_nsecz00_9, obj_t BgL_secz00_10, obj_t BgL_timeza7oneza7_11, obj_t BgL_yearz00_12)
{
{ /* Llib/date.scm 227 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_timeza7oneza7_11))
{ /* Llib/date.scm 233 */
 int BgL_auxz00_16235; long BgL_auxz00_16226; int BgL_auxz00_16217; int BgL_auxz00_16208; int BgL_auxz00_16199; int BgL_auxz00_16190; int BgL_auxz00_16181; int BgL_auxz00_16172; BGL_LONGLONG_T BgL_tmpz00_16163;
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16236;
if(
INTEGERP(BgL_dstz00_5))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16236 = BgL_dstz00_5
; }  else 
{ 
 obj_t BgL_auxz00_16239;
BgL_auxz00_16239 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11095L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dstz00_5); 
FAILURE(BgL_auxz00_16239,BFALSE,BFALSE);} 
BgL_auxz00_16235 = 
CINT(BgL_tmpz00_16236); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16227;
if(
INTEGERP(BgL_timeza7oneza7_11))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16227 = BgL_timeza7oneza7_11
; }  else 
{ 
 obj_t BgL_auxz00_16230;
BgL_auxz00_16230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11083L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_timeza7oneza7_11); 
FAILURE(BgL_auxz00_16230,BFALSE,BFALSE);} 
BgL_auxz00_16226 = 
(long)CINT(BgL_tmpz00_16227); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16218;
if(
INTEGERP(BgL_yearz00_12))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16218 = BgL_yearz00_12
; }  else 
{ 
 obj_t BgL_auxz00_16221;
BgL_auxz00_16221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11078L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_12); 
FAILURE(BgL_auxz00_16221,BFALSE,BFALSE);} 
BgL_auxz00_16217 = 
CINT(BgL_tmpz00_16218); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16209;
if(
INTEGERP(BgL_monthz00_8))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16209 = BgL_monthz00_8
; }  else 
{ 
 obj_t BgL_auxz00_16212;
BgL_auxz00_16212 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11072L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_8); 
FAILURE(BgL_auxz00_16212,BFALSE,BFALSE);} 
BgL_auxz00_16208 = 
CINT(BgL_tmpz00_16209); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16200;
if(
INTEGERP(BgL_dayz00_4))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16200 = BgL_dayz00_4
; }  else 
{ 
 obj_t BgL_auxz00_16203;
BgL_auxz00_16203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11068L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_4); 
FAILURE(BgL_auxz00_16203,BFALSE,BFALSE);} 
BgL_auxz00_16199 = 
CINT(BgL_tmpz00_16200); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16191;
if(
INTEGERP(BgL_hourz00_6))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16191 = BgL_hourz00_6
; }  else 
{ 
 obj_t BgL_auxz00_16194;
BgL_auxz00_16194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11063L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_6); 
FAILURE(BgL_auxz00_16194,BFALSE,BFALSE);} 
BgL_auxz00_16190 = 
CINT(BgL_tmpz00_16191); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16182;
if(
INTEGERP(BgL_minz00_7))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16182 = BgL_minz00_7
; }  else 
{ 
 obj_t BgL_auxz00_16185;
BgL_auxz00_16185 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11059L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minz00_7); 
FAILURE(BgL_auxz00_16185,BFALSE,BFALSE);} 
BgL_auxz00_16181 = 
CINT(BgL_tmpz00_16182); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16173;
if(
INTEGERP(BgL_secz00_10))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16173 = BgL_secz00_10
; }  else 
{ 
 obj_t BgL_auxz00_16176;
BgL_auxz00_16176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11055L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secz00_10); 
FAILURE(BgL_auxz00_16176,BFALSE,BFALSE);} 
BgL_auxz00_16172 = 
CINT(BgL_tmpz00_16173); } 
{ /* Llib/date.scm 233 */
 obj_t BgL_tmpz00_16164;
if(
LLONGP(BgL_nsecz00_9))
{ /* Llib/date.scm 233 */
BgL_tmpz00_16164 = BgL_nsecz00_9
; }  else 
{ 
 obj_t BgL_auxz00_16167;
BgL_auxz00_16167 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11050L), BGl_string5929z00zz__datez00, BGl_string5932z00zz__datez00, BgL_nsecz00_9); 
FAILURE(BgL_auxz00_16167,BFALSE,BFALSE);} 
BgL_tmpz00_16163 = 
BLLONG_TO_LLONG(BgL_tmpz00_16164); } 
return 
bgl_make_date(BgL_tmpz00_16163, BgL_auxz00_16172, BgL_auxz00_16181, BgL_auxz00_16190, BgL_auxz00_16199, BgL_auxz00_16208, BgL_auxz00_16217, BgL_auxz00_16226, ((bool_t)1), BgL_auxz00_16235);}  else 
{ /* Llib/date.scm 234 */
 int BgL_auxz00_16308; int BgL_auxz00_16299; int BgL_auxz00_16290; int BgL_auxz00_16281; int BgL_auxz00_16272; int BgL_auxz00_16263; int BgL_auxz00_16254; BGL_LONGLONG_T BgL_tmpz00_16245;
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16309;
if(
INTEGERP(BgL_dstz00_5))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16309 = BgL_dstz00_5
; }  else 
{ 
 obj_t BgL_auxz00_16312;
BgL_auxz00_16312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11156L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dstz00_5); 
FAILURE(BgL_auxz00_16312,BFALSE,BFALSE);} 
BgL_auxz00_16308 = 
CINT(BgL_tmpz00_16309); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16300;
if(
INTEGERP(BgL_yearz00_12))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16300 = BgL_yearz00_12
; }  else 
{ 
 obj_t BgL_auxz00_16303;
BgL_auxz00_16303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11146L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_12); 
FAILURE(BgL_auxz00_16303,BFALSE,BFALSE);} 
BgL_auxz00_16299 = 
CINT(BgL_tmpz00_16300); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16291;
if(
INTEGERP(BgL_monthz00_8))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16291 = BgL_monthz00_8
; }  else 
{ 
 obj_t BgL_auxz00_16294;
BgL_auxz00_16294 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11140L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_8); 
FAILURE(BgL_auxz00_16294,BFALSE,BFALSE);} 
BgL_auxz00_16290 = 
CINT(BgL_tmpz00_16291); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16282;
if(
INTEGERP(BgL_dayz00_4))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16282 = BgL_dayz00_4
; }  else 
{ 
 obj_t BgL_auxz00_16285;
BgL_auxz00_16285 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11136L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_4); 
FAILURE(BgL_auxz00_16285,BFALSE,BFALSE);} 
BgL_auxz00_16281 = 
CINT(BgL_tmpz00_16282); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16273;
if(
INTEGERP(BgL_hourz00_6))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16273 = BgL_hourz00_6
; }  else 
{ 
 obj_t BgL_auxz00_16276;
BgL_auxz00_16276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11131L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_hourz00_6); 
FAILURE(BgL_auxz00_16276,BFALSE,BFALSE);} 
BgL_auxz00_16272 = 
CINT(BgL_tmpz00_16273); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16264;
if(
INTEGERP(BgL_minz00_7))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16264 = BgL_minz00_7
; }  else 
{ 
 obj_t BgL_auxz00_16267;
BgL_auxz00_16267 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11127L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minz00_7); 
FAILURE(BgL_auxz00_16267,BFALSE,BFALSE);} 
BgL_auxz00_16263 = 
CINT(BgL_tmpz00_16264); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16255;
if(
INTEGERP(BgL_secz00_10))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16255 = BgL_secz00_10
; }  else 
{ 
 obj_t BgL_auxz00_16258;
BgL_auxz00_16258 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11123L), BGl_string5929z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secz00_10); 
FAILURE(BgL_auxz00_16258,BFALSE,BFALSE);} 
BgL_auxz00_16254 = 
CINT(BgL_tmpz00_16255); } 
{ /* Llib/date.scm 234 */
 obj_t BgL_tmpz00_16246;
if(
LLONGP(BgL_nsecz00_9))
{ /* Llib/date.scm 234 */
BgL_tmpz00_16246 = BgL_nsecz00_9
; }  else 
{ 
 obj_t BgL_auxz00_16249;
BgL_auxz00_16249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11118L), BGl_string5929z00zz__datez00, BGl_string5932z00zz__datez00, BgL_nsecz00_9); 
FAILURE(BgL_auxz00_16249,BFALSE,BFALSE);} 
BgL_tmpz00_16245 = 
BLLONG_TO_LLONG(BgL_tmpz00_16246); } 
return 
bgl_make_date(BgL_tmpz00_16245, BgL_auxz00_16254, BgL_auxz00_16263, BgL_auxz00_16272, BgL_auxz00_16281, BgL_auxz00_16290, BgL_auxz00_16299, 0L, ((bool_t)0), BgL_auxz00_16308);} } 

}



/* _date-copy */
obj_t BGl__datezd2copyzd2zz__datez00(obj_t BgL_env1331z00_26, obj_t BgL_opt1330z00_25)
{
{ /* Llib/date.scm 239 */
{ /* Llib/date.scm 239 */
 obj_t BgL_datez00_3633;
BgL_datez00_3633 = 
VECTOR_REF(BgL_opt1330z00_25,0L); 
{ /* Llib/date.scm 239 */
 obj_t BgL_dayz00_3634;
BgL_dayz00_3634 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_hourz00_3635;
BgL_hourz00_3635 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_isdstz00_3636;
BgL_isdstz00_3636 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_minz00_3637;
BgL_minz00_3637 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_monthz00_3638;
BgL_monthz00_3638 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_nsecz00_3639;
BgL_nsecz00_3639 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_secz00_3640;
BgL_secz00_3640 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_timeza7oneza7_3641;
BgL_timeza7oneza7_3641 = BFALSE; 
{ /* Llib/date.scm 239 */
 obj_t BgL_yearz00_3642;
BgL_yearz00_3642 = BFALSE; 
{ /* Llib/date.scm 239 */

{ 
 long BgL_iz00_3643;
BgL_iz00_3643 = 1L; 
BgL_check1334z00_3644:
if(
(BgL_iz00_3643==
VECTOR_LENGTH(BgL_opt1330z00_25)))
{ /* Llib/date.scm 239 */BNIL; }  else 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7001z00_16322;
{ /* Llib/date.scm 239 */
 obj_t BgL_arg2685z00_3650;
{ /* Llib/date.scm 239 */
 bool_t BgL_test7002z00_16323;
{ /* Llib/date.scm 239 */
 long BgL_tmpz00_16324;
BgL_tmpz00_16324 = 
VECTOR_LENGTH(BgL_opt1330z00_25); 
BgL_test7002z00_16323 = 
BOUND_CHECK(BgL_iz00_3643, BgL_tmpz00_16324); } 
if(BgL_test7002z00_16323)
{ /* Llib/date.scm 239 */
BgL_arg2685z00_3650 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_iz00_3643); }  else 
{ 
 obj_t BgL_auxz00_16328;
BgL_auxz00_16328 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5908z00zz__datez00, BgL_opt1330z00_25, 
(int)(
VECTOR_LENGTH(BgL_opt1330z00_25)), 
(int)(BgL_iz00_3643)); 
FAILURE(BgL_auxz00_16328,BFALSE,BFALSE);} } 
BgL_test7001z00_16322 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg2685z00_3650, BGl_list5934z00zz__datez00)); } 
if(BgL_test7001z00_16322)
{ 
 long BgL_iz00_16337;
BgL_iz00_16337 = 
(BgL_iz00_3643+2L); 
BgL_iz00_3643 = BgL_iz00_16337; 
goto BgL_check1334z00_3644;}  else 
{ /* Llib/date.scm 239 */
 obj_t BgL_arg2684z00_3649;
{ /* Llib/date.scm 239 */
 bool_t BgL_test7003z00_16339;
{ /* Llib/date.scm 239 */
 long BgL_tmpz00_16340;
BgL_tmpz00_16340 = 
VECTOR_LENGTH(BgL_opt1330z00_25); 
BgL_test7003z00_16339 = 
BOUND_CHECK(BgL_iz00_3643, BgL_tmpz00_16340); } 
if(BgL_test7003z00_16339)
{ /* Llib/date.scm 239 */
BgL_arg2684z00_3649 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_iz00_3643); }  else 
{ 
 obj_t BgL_auxz00_16344;
BgL_auxz00_16344 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5908z00zz__datez00, BgL_opt1330z00_25, 
(int)(
VECTOR_LENGTH(BgL_opt1330z00_25)), 
(int)(BgL_iz00_3643)); 
FAILURE(BgL_auxz00_16344,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol5937z00zz__datez00, BGl_string5930z00zz__datez00, BgL_arg2684z00_3649); } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1336z00_3651;
BgL_index1336z00_3651 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5910z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7004z00_16354;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8130;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16355;
if(
INTEGERP(BgL_index1336z00_3651))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16355 = BgL_index1336z00_3651
; }  else 
{ 
 obj_t BgL_auxz00_16358;
BgL_auxz00_16358 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1336z00_3651); 
FAILURE(BgL_auxz00_16358,BFALSE,BFALSE);} 
BgL_n1z00_8130 = 
(long)CINT(BgL_tmpz00_16355); } 
BgL_test7004z00_16354 = 
(BgL_n1z00_8130>=0L); } 
if(BgL_test7004z00_16354)
{ 
 long BgL_auxz00_16364;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16365;
if(
INTEGERP(BgL_index1336z00_3651))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16365 = BgL_index1336z00_3651
; }  else 
{ 
 obj_t BgL_auxz00_16368;
BgL_auxz00_16368 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1336z00_3651); 
FAILURE(BgL_auxz00_16368,BFALSE,BFALSE);} 
BgL_auxz00_16364 = 
(long)CINT(BgL_tmpz00_16365); } 
BgL_dayz00_3634 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16364); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1337z00_3653;
BgL_index1337z00_3653 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5914z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7007z00_16376;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8131;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16377;
if(
INTEGERP(BgL_index1337z00_3653))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16377 = BgL_index1337z00_3653
; }  else 
{ 
 obj_t BgL_auxz00_16380;
BgL_auxz00_16380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1337z00_3653); 
FAILURE(BgL_auxz00_16380,BFALSE,BFALSE);} 
BgL_n1z00_8131 = 
(long)CINT(BgL_tmpz00_16377); } 
BgL_test7007z00_16376 = 
(BgL_n1z00_8131>=0L); } 
if(BgL_test7007z00_16376)
{ 
 long BgL_auxz00_16386;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16387;
if(
INTEGERP(BgL_index1337z00_3653))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16387 = BgL_index1337z00_3653
; }  else 
{ 
 obj_t BgL_auxz00_16390;
BgL_auxz00_16390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1337z00_3653); 
FAILURE(BgL_auxz00_16390,BFALSE,BFALSE);} 
BgL_auxz00_16386 = 
(long)CINT(BgL_tmpz00_16387); } 
BgL_hourz00_3635 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16386); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1338z00_3655;
BgL_index1338z00_3655 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5935z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7010z00_16398;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8132;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16399;
if(
INTEGERP(BgL_index1338z00_3655))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16399 = BgL_index1338z00_3655
; }  else 
{ 
 obj_t BgL_auxz00_16402;
BgL_auxz00_16402 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1338z00_3655); 
FAILURE(BgL_auxz00_16402,BFALSE,BFALSE);} 
BgL_n1z00_8132 = 
(long)CINT(BgL_tmpz00_16399); } 
BgL_test7010z00_16398 = 
(BgL_n1z00_8132>=0L); } 
if(BgL_test7010z00_16398)
{ 
 long BgL_auxz00_16408;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16409;
if(
INTEGERP(BgL_index1338z00_3655))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16409 = BgL_index1338z00_3655
; }  else 
{ 
 obj_t BgL_auxz00_16412;
BgL_auxz00_16412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1338z00_3655); 
FAILURE(BgL_auxz00_16412,BFALSE,BFALSE);} 
BgL_auxz00_16408 = 
(long)CINT(BgL_tmpz00_16409); } 
BgL_isdstz00_3636 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16408); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1339z00_3657;
BgL_index1339z00_3657 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5916z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7013z00_16420;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8133;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16421;
if(
INTEGERP(BgL_index1339z00_3657))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16421 = BgL_index1339z00_3657
; }  else 
{ 
 obj_t BgL_auxz00_16424;
BgL_auxz00_16424 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1339z00_3657); 
FAILURE(BgL_auxz00_16424,BFALSE,BFALSE);} 
BgL_n1z00_8133 = 
(long)CINT(BgL_tmpz00_16421); } 
BgL_test7013z00_16420 = 
(BgL_n1z00_8133>=0L); } 
if(BgL_test7013z00_16420)
{ 
 long BgL_auxz00_16430;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16431;
if(
INTEGERP(BgL_index1339z00_3657))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16431 = BgL_index1339z00_3657
; }  else 
{ 
 obj_t BgL_auxz00_16434;
BgL_auxz00_16434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1339z00_3657); 
FAILURE(BgL_auxz00_16434,BFALSE,BFALSE);} 
BgL_auxz00_16430 = 
(long)CINT(BgL_tmpz00_16431); } 
BgL_minz00_3637 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16430); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1340z00_3659;
BgL_index1340z00_3659 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5918z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7016z00_16442;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8134;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16443;
if(
INTEGERP(BgL_index1340z00_3659))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16443 = BgL_index1340z00_3659
; }  else 
{ 
 obj_t BgL_auxz00_16446;
BgL_auxz00_16446 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1340z00_3659); 
FAILURE(BgL_auxz00_16446,BFALSE,BFALSE);} 
BgL_n1z00_8134 = 
(long)CINT(BgL_tmpz00_16443); } 
BgL_test7016z00_16442 = 
(BgL_n1z00_8134>=0L); } 
if(BgL_test7016z00_16442)
{ 
 long BgL_auxz00_16452;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16453;
if(
INTEGERP(BgL_index1340z00_3659))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16453 = BgL_index1340z00_3659
; }  else 
{ 
 obj_t BgL_auxz00_16456;
BgL_auxz00_16456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1340z00_3659); 
FAILURE(BgL_auxz00_16456,BFALSE,BFALSE);} 
BgL_auxz00_16452 = 
(long)CINT(BgL_tmpz00_16453); } 
BgL_monthz00_3638 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16452); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1341z00_3661;
BgL_index1341z00_3661 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5920z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7019z00_16464;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8135;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16465;
if(
INTEGERP(BgL_index1341z00_3661))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16465 = BgL_index1341z00_3661
; }  else 
{ 
 obj_t BgL_auxz00_16468;
BgL_auxz00_16468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1341z00_3661); 
FAILURE(BgL_auxz00_16468,BFALSE,BFALSE);} 
BgL_n1z00_8135 = 
(long)CINT(BgL_tmpz00_16465); } 
BgL_test7019z00_16464 = 
(BgL_n1z00_8135>=0L); } 
if(BgL_test7019z00_16464)
{ 
 long BgL_auxz00_16474;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16475;
if(
INTEGERP(BgL_index1341z00_3661))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16475 = BgL_index1341z00_3661
; }  else 
{ 
 obj_t BgL_auxz00_16478;
BgL_auxz00_16478 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1341z00_3661); 
FAILURE(BgL_auxz00_16478,BFALSE,BFALSE);} 
BgL_auxz00_16474 = 
(long)CINT(BgL_tmpz00_16475); } 
BgL_nsecz00_3639 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16474); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1342z00_3663;
BgL_index1342z00_3663 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5922z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7022z00_16486;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8136;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16487;
if(
INTEGERP(BgL_index1342z00_3663))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16487 = BgL_index1342z00_3663
; }  else 
{ 
 obj_t BgL_auxz00_16490;
BgL_auxz00_16490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1342z00_3663); 
FAILURE(BgL_auxz00_16490,BFALSE,BFALSE);} 
BgL_n1z00_8136 = 
(long)CINT(BgL_tmpz00_16487); } 
BgL_test7022z00_16486 = 
(BgL_n1z00_8136>=0L); } 
if(BgL_test7022z00_16486)
{ 
 long BgL_auxz00_16496;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16497;
if(
INTEGERP(BgL_index1342z00_3663))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16497 = BgL_index1342z00_3663
; }  else 
{ 
 obj_t BgL_auxz00_16500;
BgL_auxz00_16500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1342z00_3663); 
FAILURE(BgL_auxz00_16500,BFALSE,BFALSE);} 
BgL_auxz00_16496 = 
(long)CINT(BgL_tmpz00_16497); } 
BgL_secz00_3640 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16496); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1343z00_3665;
BgL_index1343z00_3665 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5924z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7025z00_16508;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8137;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16509;
if(
INTEGERP(BgL_index1343z00_3665))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16509 = BgL_index1343z00_3665
; }  else 
{ 
 obj_t BgL_auxz00_16512;
BgL_auxz00_16512 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1343z00_3665); 
FAILURE(BgL_auxz00_16512,BFALSE,BFALSE);} 
BgL_n1z00_8137 = 
(long)CINT(BgL_tmpz00_16509); } 
BgL_test7025z00_16508 = 
(BgL_n1z00_8137>=0L); } 
if(BgL_test7025z00_16508)
{ 
 long BgL_auxz00_16518;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16519;
if(
INTEGERP(BgL_index1343z00_3665))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16519 = BgL_index1343z00_3665
; }  else 
{ 
 obj_t BgL_auxz00_16522;
BgL_auxz00_16522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1343z00_3665); 
FAILURE(BgL_auxz00_16522,BFALSE,BFALSE);} 
BgL_auxz00_16518 = 
(long)CINT(BgL_tmpz00_16519); } 
BgL_timeza7oneza7_3641 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16518); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_index1344z00_3667;
BgL_index1344z00_3667 = 
BGl_search1333ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1330z00_25), BgL_opt1330z00_25, BGl_keyword5926z00zz__datez00, 1L); 
{ /* Llib/date.scm 239 */
 bool_t BgL_test7028z00_16530;
{ /* Llib/date.scm 239 */
 long BgL_n1z00_8138;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16531;
if(
INTEGERP(BgL_index1344z00_3667))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16531 = BgL_index1344z00_3667
; }  else 
{ 
 obj_t BgL_auxz00_16534;
BgL_auxz00_16534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1344z00_3667); 
FAILURE(BgL_auxz00_16534,BFALSE,BFALSE);} 
BgL_n1z00_8138 = 
(long)CINT(BgL_tmpz00_16531); } 
BgL_test7028z00_16530 = 
(BgL_n1z00_8138>=0L); } 
if(BgL_test7028z00_16530)
{ 
 long BgL_auxz00_16540;
{ /* Llib/date.scm 239 */
 obj_t BgL_tmpz00_16541;
if(
INTEGERP(BgL_index1344z00_3667))
{ /* Llib/date.scm 239 */
BgL_tmpz00_16541 = BgL_index1344z00_3667
; }  else 
{ 
 obj_t BgL_auxz00_16544;
BgL_auxz00_16544 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1344z00_3667); 
FAILURE(BgL_auxz00_16544,BFALSE,BFALSE);} 
BgL_auxz00_16540 = 
(long)CINT(BgL_tmpz00_16541); } 
BgL_yearz00_3642 = 
VECTOR_REF(BgL_opt1330z00_25,BgL_auxz00_16540); }  else 
{ /* Llib/date.scm 239 */BFALSE; } } } 
{ /* Llib/date.scm 239 */
 obj_t BgL_arg2695z00_3669;
BgL_arg2695z00_3669 = 
VECTOR_REF(BgL_opt1330z00_25,0L); 
{ /* Llib/date.scm 239 */
 obj_t BgL_dayz00_3670;
BgL_dayz00_3670 = BgL_dayz00_3634; 
{ /* Llib/date.scm 239 */
 obj_t BgL_hourz00_3671;
BgL_hourz00_3671 = BgL_hourz00_3635; 
{ /* Llib/date.scm 239 */
 obj_t BgL_isdstz00_3672;
BgL_isdstz00_3672 = BgL_isdstz00_3636; 
{ /* Llib/date.scm 239 */
 obj_t BgL_minz00_3673;
BgL_minz00_3673 = BgL_minz00_3637; 
{ /* Llib/date.scm 239 */
 obj_t BgL_monthz00_3674;
BgL_monthz00_3674 = BgL_monthz00_3638; 
{ /* Llib/date.scm 239 */
 obj_t BgL_nsecz00_3675;
BgL_nsecz00_3675 = BgL_nsecz00_3639; 
{ /* Llib/date.scm 239 */
 obj_t BgL_secz00_3676;
BgL_secz00_3676 = BgL_secz00_3640; 
{ /* Llib/date.scm 239 */
 obj_t BgL_timeza7oneza7_3677;
BgL_timeza7oneza7_3677 = BgL_timeza7oneza7_3641; 
{ /* Llib/date.scm 239 */
 obj_t BgL_yearz00_3678;
BgL_yearz00_3678 = BgL_yearz00_3642; 
{ /* Llib/date.scm 239 */
 obj_t BgL_datez00_8139;
if(
BGL_DATEP(BgL_arg2695z00_3669))
{ /* Llib/date.scm 239 */
BgL_datez00_8139 = BgL_arg2695z00_3669; }  else 
{ 
 obj_t BgL_auxz00_16553;
BgL_auxz00_16553 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11386L), BGl_string5939z00zz__datez00, BGl_string5940z00zz__datez00, BgL_arg2695z00_3669); 
FAILURE(BgL_auxz00_16553,BFALSE,BFALSE);} 
{ /* Llib/date.scm 241 */
 obj_t BgL_arg2702z00_8140; obj_t BgL_arg2703z00_8141; obj_t BgL_arg2704z00_8142; obj_t BgL_arg2705z00_8143; obj_t BgL_arg2706z00_8144; obj_t BgL_arg2707z00_8145; obj_t BgL_arg2708z00_8146; long BgL_arg2709z00_8147; bool_t BgL_arg2710z00_8148; obj_t BgL_arg2711z00_8149;
if(
CBOOL(BgL_nsecz00_3675))
{ /* Llib/date.scm 241 */
BgL_arg2702z00_8140 = BgL_nsecz00_3675; }  else 
{ /* Llib/date.scm 241 */
BgL_arg2702z00_8140 = 
make_bllong(
BGL_DATE_NANOSECOND(BgL_datez00_8139)); } 
if(
CBOOL(BgL_secz00_3676))
{ /* Llib/date.scm 242 */
BgL_arg2703z00_8141 = BgL_secz00_3676; }  else 
{ /* Llib/date.scm 242 */
BgL_arg2703z00_8141 = 
BINT(
BGL_DATE_SECOND(BgL_datez00_8139)); } 
if(
CBOOL(BgL_minz00_3673))
{ /* Llib/date.scm 243 */
BgL_arg2704z00_8142 = BgL_minz00_3673; }  else 
{ /* Llib/date.scm 243 */
BgL_arg2704z00_8142 = 
BINT(
BGL_DATE_MINUTE(BgL_datez00_8139)); } 
if(
CBOOL(BgL_hourz00_3671))
{ /* Llib/date.scm 244 */
BgL_arg2705z00_8143 = BgL_hourz00_3671; }  else 
{ /* Llib/date.scm 244 */
BgL_arg2705z00_8143 = 
BINT(
BGL_DATE_HOUR(BgL_datez00_8139)); } 
if(
CBOOL(BgL_dayz00_3670))
{ /* Llib/date.scm 245 */
BgL_arg2706z00_8144 = BgL_dayz00_3670; }  else 
{ /* Llib/date.scm 245 */
BgL_arg2706z00_8144 = 
BINT(
BGL_DATE_DAY(BgL_datez00_8139)); } 
if(
CBOOL(BgL_monthz00_3674))
{ /* Llib/date.scm 246 */
BgL_arg2707z00_8145 = BgL_monthz00_3674; }  else 
{ /* Llib/date.scm 246 */
BgL_arg2707z00_8145 = 
BINT(
BGL_DATE_MONTH(BgL_datez00_8139)); } 
if(
CBOOL(BgL_yearz00_3678))
{ /* Llib/date.scm 247 */
BgL_arg2708z00_8146 = BgL_yearz00_3678; }  else 
{ /* Llib/date.scm 247 */
BgL_arg2708z00_8146 = 
BINT(
BGL_DATE_YEAR(BgL_datez00_8139)); } 
BgL_arg2709z00_8147 = 
BGL_DATE_TIMEZONE(BgL_datez00_8139); 
BgL_arg2710z00_8148 = 
BGL_DATE_ISGMT(BgL_datez00_8139); 
if(
CBOOL(BgL_isdstz00_3672))
{ /* Llib/date.scm 250 */
BgL_arg2711z00_8149 = BgL_isdstz00_3672; }  else 
{ /* Llib/date.scm 250 */
BgL_arg2711z00_8149 = 
BINT(-1L); } 
{ /* Llib/date.scm 240 */
 int BgL_auxz00_16653; int BgL_auxz00_16644; int BgL_auxz00_16635; int BgL_auxz00_16626; int BgL_auxz00_16617; int BgL_auxz00_16608; int BgL_auxz00_16599; BGL_LONGLONG_T BgL_tmpz00_16590;
{ /* Llib/date.scm 250 */
 obj_t BgL_tmpz00_16654;
if(
INTEGERP(BgL_arg2711z00_8149))
{ /* Llib/date.scm 250 */
BgL_tmpz00_16654 = BgL_arg2711z00_8149
; }  else 
{ 
 obj_t BgL_auxz00_16657;
BgL_auxz00_16657 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11789L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2711z00_8149); 
FAILURE(BgL_auxz00_16657,BFALSE,BFALSE);} 
BgL_auxz00_16653 = 
CINT(BgL_tmpz00_16654); } 
{ /* Llib/date.scm 247 */
 obj_t BgL_tmpz00_16645;
if(
INTEGERP(BgL_arg2708z00_8146))
{ /* Llib/date.scm 247 */
BgL_tmpz00_16645 = BgL_arg2708z00_8146
; }  else 
{ 
 obj_t BgL_auxz00_16648;
BgL_auxz00_16648 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11716L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2708z00_8146); 
FAILURE(BgL_auxz00_16648,BFALSE,BFALSE);} 
BgL_auxz00_16644 = 
CINT(BgL_tmpz00_16645); } 
{ /* Llib/date.scm 246 */
 obj_t BgL_tmpz00_16636;
if(
INTEGERP(BgL_arg2707z00_8145))
{ /* Llib/date.scm 246 */
BgL_tmpz00_16636 = BgL_arg2707z00_8145
; }  else 
{ 
 obj_t BgL_auxz00_16639;
BgL_auxz00_16639 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11683L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2707z00_8145); 
FAILURE(BgL_auxz00_16639,BFALSE,BFALSE);} 
BgL_auxz00_16635 = 
CINT(BgL_tmpz00_16636); } 
{ /* Llib/date.scm 245 */
 obj_t BgL_tmpz00_16627;
if(
INTEGERP(BgL_arg2706z00_8144))
{ /* Llib/date.scm 245 */
BgL_tmpz00_16627 = BgL_arg2706z00_8144
; }  else 
{ 
 obj_t BgL_auxz00_16630;
BgL_auxz00_16630 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11648L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2706z00_8144); 
FAILURE(BgL_auxz00_16630,BFALSE,BFALSE);} 
BgL_auxz00_16626 = 
CINT(BgL_tmpz00_16627); } 
{ /* Llib/date.scm 244 */
 obj_t BgL_tmpz00_16618;
if(
INTEGERP(BgL_arg2705z00_8143))
{ /* Llib/date.scm 244 */
BgL_tmpz00_16618 = BgL_arg2705z00_8143
; }  else 
{ 
 obj_t BgL_auxz00_16621;
BgL_auxz00_16621 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11617L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2705z00_8143); 
FAILURE(BgL_auxz00_16621,BFALSE,BFALSE);} 
BgL_auxz00_16617 = 
CINT(BgL_tmpz00_16618); } 
{ /* Llib/date.scm 243 */
 obj_t BgL_tmpz00_16609;
if(
INTEGERP(BgL_arg2704z00_8142))
{ /* Llib/date.scm 243 */
BgL_tmpz00_16609 = BgL_arg2704z00_8142
; }  else 
{ 
 obj_t BgL_auxz00_16612;
BgL_auxz00_16612 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11584L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2704z00_8142); 
FAILURE(BgL_auxz00_16612,BFALSE,BFALSE);} 
BgL_auxz00_16608 = 
CINT(BgL_tmpz00_16609); } 
{ /* Llib/date.scm 242 */
 obj_t BgL_tmpz00_16600;
if(
INTEGERP(BgL_arg2703z00_8141))
{ /* Llib/date.scm 242 */
BgL_tmpz00_16600 = BgL_arg2703z00_8141
; }  else 
{ 
 obj_t BgL_auxz00_16603;
BgL_auxz00_16603 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11550L), BGl_string5939z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2703z00_8141); 
FAILURE(BgL_auxz00_16603,BFALSE,BFALSE);} 
BgL_auxz00_16599 = 
CINT(BgL_tmpz00_16600); } 
{ /* Llib/date.scm 241 */
 obj_t BgL_tmpz00_16591;
if(
LLONGP(BgL_arg2702z00_8140))
{ /* Llib/date.scm 241 */
BgL_tmpz00_16591 = BgL_arg2702z00_8140
; }  else 
{ 
 obj_t BgL_auxz00_16594;
BgL_auxz00_16594 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11516L), BGl_string5939z00zz__datez00, BGl_string5932z00zz__datez00, BgL_arg2702z00_8140); 
FAILURE(BgL_auxz00_16594,BFALSE,BFALSE);} 
BgL_tmpz00_16590 = 
BLLONG_TO_LLONG(BgL_tmpz00_16591); } 
return 
bgl_make_date(BgL_tmpz00_16590, BgL_auxz00_16599, BgL_auxz00_16608, BgL_auxz00_16617, BgL_auxz00_16626, BgL_auxz00_16635, BgL_auxz00_16644, BgL_arg2709z00_8147, BgL_arg2710z00_8148, BgL_auxz00_16653);} } } } } } } } } } } } } } } } } } } } } } } } } 

}



/* search1333~0 */
obj_t BGl_search1333ze70ze7zz__datez00(long BgL_l1332z00_9882, obj_t BgL_opt1330z00_9881, obj_t BgL_k1z00_3630, long BgL_iz00_3631)
{
{ /* Llib/date.scm 239 */
BGl_search1333ze70ze7zz__datez00:
if(
(BgL_iz00_3631==BgL_l1332z00_9882))
{ /* Llib/date.scm 239 */
return 
BINT(-1L);}  else 
{ /* Llib/date.scm 239 */
if(
(BgL_iz00_3631==
(BgL_l1332z00_9882-1L)))
{ /* Llib/date.scm 239 */
return 
BGl_errorz00zz__errorz00(BGl_symbol5937z00zz__datez00, BGl_string5941z00zz__datez00, 
BINT(
VECTOR_LENGTH(BgL_opt1330z00_9881)));}  else 
{ /* Llib/date.scm 239 */
 obj_t BgL_vz00_3683;
BgL_vz00_3683 = 
VECTOR_REF(BgL_opt1330z00_9881,BgL_iz00_3631); 
if(
(BgL_vz00_3683==BgL_k1z00_3630))
{ /* Llib/date.scm 239 */
return 
BINT(
(BgL_iz00_3631+1L));}  else 
{ 
 long BgL_iz00_16677;
BgL_iz00_16677 = 
(BgL_iz00_3631+2L); 
BgL_iz00_3631 = BgL_iz00_16677; 
goto BGl_search1333ze70ze7zz__datez00;} } } } 

}



/* date-copy */
BGL_EXPORTED_DEF obj_t BGl_datezd2copyzd2zz__datez00(obj_t BgL_datez00_15, obj_t BgL_dayz00_16, obj_t BgL_hourz00_17, obj_t BgL_isdstz00_18, obj_t BgL_minz00_19, obj_t BgL_monthz00_20, obj_t BgL_nsecz00_21, obj_t BgL_secz00_22, obj_t BgL_timeza7oneza7_23, obj_t BgL_yearz00_24)
{
{ /* Llib/date.scm 239 */
{ /* Llib/date.scm 241 */
 obj_t BgL_arg2702z00_8166; obj_t BgL_arg2703z00_8167; obj_t BgL_arg2704z00_8168; obj_t BgL_arg2705z00_8169; obj_t BgL_arg2706z00_8170; obj_t BgL_arg2707z00_8171; obj_t BgL_arg2708z00_8172; long BgL_arg2709z00_8173; bool_t BgL_arg2710z00_8174; obj_t BgL_arg2711z00_8175;
if(
CBOOL(BgL_nsecz00_21))
{ /* Llib/date.scm 241 */
BgL_arg2702z00_8166 = BgL_nsecz00_21; }  else 
{ /* Llib/date.scm 241 */
BgL_arg2702z00_8166 = 
make_bllong(
BGL_DATE_NANOSECOND(BgL_datez00_15)); } 
if(
CBOOL(BgL_secz00_22))
{ /* Llib/date.scm 242 */
BgL_arg2703z00_8167 = BgL_secz00_22; }  else 
{ /* Llib/date.scm 242 */
BgL_arg2703z00_8167 = 
BINT(
BGL_DATE_SECOND(BgL_datez00_15)); } 
if(
CBOOL(BgL_minz00_19))
{ /* Llib/date.scm 243 */
BgL_arg2704z00_8168 = BgL_minz00_19; }  else 
{ /* Llib/date.scm 243 */
BgL_arg2704z00_8168 = 
BINT(
BGL_DATE_MINUTE(BgL_datez00_15)); } 
if(
CBOOL(BgL_hourz00_17))
{ /* Llib/date.scm 244 */
BgL_arg2705z00_8169 = BgL_hourz00_17; }  else 
{ /* Llib/date.scm 244 */
BgL_arg2705z00_8169 = 
BINT(
BGL_DATE_HOUR(BgL_datez00_15)); } 
if(
CBOOL(BgL_dayz00_16))
{ /* Llib/date.scm 245 */
BgL_arg2706z00_8170 = BgL_dayz00_16; }  else 
{ /* Llib/date.scm 245 */
BgL_arg2706z00_8170 = 
BINT(
BGL_DATE_DAY(BgL_datez00_15)); } 
if(
CBOOL(BgL_monthz00_20))
{ /* Llib/date.scm 246 */
BgL_arg2707z00_8171 = BgL_monthz00_20; }  else 
{ /* Llib/date.scm 246 */
BgL_arg2707z00_8171 = 
BINT(
BGL_DATE_MONTH(BgL_datez00_15)); } 
if(
CBOOL(BgL_yearz00_24))
{ /* Llib/date.scm 247 */
BgL_arg2708z00_8172 = BgL_yearz00_24; }  else 
{ /* Llib/date.scm 247 */
BgL_arg2708z00_8172 = 
BINT(
BGL_DATE_YEAR(BgL_datez00_15)); } 
BgL_arg2709z00_8173 = 
BGL_DATE_TIMEZONE(BgL_datez00_15); 
BgL_arg2710z00_8174 = 
BGL_DATE_ISGMT(BgL_datez00_15); 
if(
CBOOL(BgL_isdstz00_18))
{ /* Llib/date.scm 250 */
BgL_arg2711z00_8175 = BgL_isdstz00_18; }  else 
{ /* Llib/date.scm 250 */
BgL_arg2711z00_8175 = 
BINT(-1L); } 
{ /* Llib/date.scm 240 */
 int BgL_auxz00_16775; int BgL_auxz00_16766; int BgL_auxz00_16757; int BgL_auxz00_16748; int BgL_auxz00_16739; int BgL_auxz00_16730; int BgL_auxz00_16721; BGL_LONGLONG_T BgL_tmpz00_16712;
{ /* Llib/date.scm 250 */
 obj_t BgL_tmpz00_16776;
if(
INTEGERP(BgL_arg2711z00_8175))
{ /* Llib/date.scm 250 */
BgL_tmpz00_16776 = BgL_arg2711z00_8175
; }  else 
{ 
 obj_t BgL_auxz00_16779;
BgL_auxz00_16779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11789L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2711z00_8175); 
FAILURE(BgL_auxz00_16779,BFALSE,BFALSE);} 
BgL_auxz00_16775 = 
CINT(BgL_tmpz00_16776); } 
{ /* Llib/date.scm 247 */
 obj_t BgL_tmpz00_16767;
if(
INTEGERP(BgL_arg2708z00_8172))
{ /* Llib/date.scm 247 */
BgL_tmpz00_16767 = BgL_arg2708z00_8172
; }  else 
{ 
 obj_t BgL_auxz00_16770;
BgL_auxz00_16770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11716L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2708z00_8172); 
FAILURE(BgL_auxz00_16770,BFALSE,BFALSE);} 
BgL_auxz00_16766 = 
CINT(BgL_tmpz00_16767); } 
{ /* Llib/date.scm 246 */
 obj_t BgL_tmpz00_16758;
if(
INTEGERP(BgL_arg2707z00_8171))
{ /* Llib/date.scm 246 */
BgL_tmpz00_16758 = BgL_arg2707z00_8171
; }  else 
{ 
 obj_t BgL_auxz00_16761;
BgL_auxz00_16761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11683L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2707z00_8171); 
FAILURE(BgL_auxz00_16761,BFALSE,BFALSE);} 
BgL_auxz00_16757 = 
CINT(BgL_tmpz00_16758); } 
{ /* Llib/date.scm 245 */
 obj_t BgL_tmpz00_16749;
if(
INTEGERP(BgL_arg2706z00_8170))
{ /* Llib/date.scm 245 */
BgL_tmpz00_16749 = BgL_arg2706z00_8170
; }  else 
{ 
 obj_t BgL_auxz00_16752;
BgL_auxz00_16752 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11648L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2706z00_8170); 
FAILURE(BgL_auxz00_16752,BFALSE,BFALSE);} 
BgL_auxz00_16748 = 
CINT(BgL_tmpz00_16749); } 
{ /* Llib/date.scm 244 */
 obj_t BgL_tmpz00_16740;
if(
INTEGERP(BgL_arg2705z00_8169))
{ /* Llib/date.scm 244 */
BgL_tmpz00_16740 = BgL_arg2705z00_8169
; }  else 
{ 
 obj_t BgL_auxz00_16743;
BgL_auxz00_16743 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11617L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2705z00_8169); 
FAILURE(BgL_auxz00_16743,BFALSE,BFALSE);} 
BgL_auxz00_16739 = 
CINT(BgL_tmpz00_16740); } 
{ /* Llib/date.scm 243 */
 obj_t BgL_tmpz00_16731;
if(
INTEGERP(BgL_arg2704z00_8168))
{ /* Llib/date.scm 243 */
BgL_tmpz00_16731 = BgL_arg2704z00_8168
; }  else 
{ 
 obj_t BgL_auxz00_16734;
BgL_auxz00_16734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11584L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2704z00_8168); 
FAILURE(BgL_auxz00_16734,BFALSE,BFALSE);} 
BgL_auxz00_16730 = 
CINT(BgL_tmpz00_16731); } 
{ /* Llib/date.scm 242 */
 obj_t BgL_tmpz00_16722;
if(
INTEGERP(BgL_arg2703z00_8167))
{ /* Llib/date.scm 242 */
BgL_tmpz00_16722 = BgL_arg2703z00_8167
; }  else 
{ 
 obj_t BgL_auxz00_16725;
BgL_auxz00_16725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11550L), BGl_string5938z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2703z00_8167); 
FAILURE(BgL_auxz00_16725,BFALSE,BFALSE);} 
BgL_auxz00_16721 = 
CINT(BgL_tmpz00_16722); } 
{ /* Llib/date.scm 241 */
 obj_t BgL_tmpz00_16713;
if(
LLONGP(BgL_arg2702z00_8166))
{ /* Llib/date.scm 241 */
BgL_tmpz00_16713 = BgL_arg2702z00_8166
; }  else 
{ 
 obj_t BgL_auxz00_16716;
BgL_auxz00_16716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(11516L), BGl_string5938z00zz__datez00, BGl_string5932z00zz__datez00, BgL_arg2702z00_8166); 
FAILURE(BgL_auxz00_16716,BFALSE,BFALSE);} 
BgL_tmpz00_16712 = 
BLLONG_TO_LLONG(BgL_tmpz00_16713); } 
return 
bgl_make_date(BgL_tmpz00_16712, BgL_auxz00_16721, BgL_auxz00_16730, BgL_auxz00_16739, BgL_auxz00_16748, BgL_auxz00_16757, BgL_auxz00_16766, BgL_arg2709z00_8173, BgL_arg2710z00_8174, BgL_auxz00_16775);} } } 

}



/* _date-update! */
obj_t BGl__datezd2updatez12zc0zz__datez00(obj_t BgL_env1346z00_36, obj_t BgL_opt1345z00_35)
{
{ /* Llib/date.scm 255 */
{ /* Llib/date.scm 255 */
 obj_t BgL_datez00_3708;
BgL_datez00_3708 = 
VECTOR_REF(BgL_opt1345z00_35,0L); 
{ /* Llib/date.scm 255 */
 obj_t BgL_dayz00_3709;
BgL_dayz00_3709 = BFALSE; 
{ /* Llib/date.scm 255 */
 obj_t BgL_hourz00_3710;
BgL_hourz00_3710 = BFALSE; 
{ /* Llib/date.scm 255 */
 obj_t BgL_minz00_3711;
BgL_minz00_3711 = BFALSE; 
{ /* Llib/date.scm 255 */
 obj_t BgL_monthz00_3712;
BgL_monthz00_3712 = BFALSE; 
{ /* Llib/date.scm 255 */
 obj_t BgL_nsecz00_3713;
BgL_nsecz00_3713 = BFALSE; 
{ /* Llib/date.scm 255 */
 obj_t BgL_secz00_3714;
BgL_secz00_3714 = BFALSE; 
{ /* Llib/date.scm 255 */
 obj_t BgL_yearz00_3715;
BgL_yearz00_3715 = BFALSE; 
{ /* Llib/date.scm 255 */

{ 
 long BgL_iz00_3716;
BgL_iz00_3716 = 1L; 
BgL_check1349z00_3717:
if(
(BgL_iz00_3716==
VECTOR_LENGTH(BgL_opt1345z00_35)))
{ /* Llib/date.scm 255 */BNIL; }  else 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7068z00_16789;
{ /* Llib/date.scm 255 */
 obj_t BgL_arg2719z00_3723;
{ /* Llib/date.scm 255 */
 bool_t BgL_test7069z00_16790;
{ /* Llib/date.scm 255 */
 long BgL_tmpz00_16791;
BgL_tmpz00_16791 = 
VECTOR_LENGTH(BgL_opt1345z00_35); 
BgL_test7069z00_16790 = 
BOUND_CHECK(BgL_iz00_3716, BgL_tmpz00_16791); } 
if(BgL_test7069z00_16790)
{ /* Llib/date.scm 255 */
BgL_arg2719z00_3723 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_iz00_3716); }  else 
{ 
 obj_t BgL_auxz00_16795;
BgL_auxz00_16795 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5908z00zz__datez00, BgL_opt1345z00_35, 
(int)(
VECTOR_LENGTH(BgL_opt1345z00_35)), 
(int)(BgL_iz00_3716)); 
FAILURE(BgL_auxz00_16795,BFALSE,BFALSE);} } 
BgL_test7068z00_16789 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg2719z00_3723, BGl_list5942z00zz__datez00)); } 
if(BgL_test7068z00_16789)
{ 
 long BgL_iz00_16804;
BgL_iz00_16804 = 
(BgL_iz00_3716+2L); 
BgL_iz00_3716 = BgL_iz00_16804; 
goto BgL_check1349z00_3717;}  else 
{ /* Llib/date.scm 255 */
 obj_t BgL_arg2717z00_3722;
{ /* Llib/date.scm 255 */
 bool_t BgL_test7070z00_16806;
{ /* Llib/date.scm 255 */
 long BgL_tmpz00_16807;
BgL_tmpz00_16807 = 
VECTOR_LENGTH(BgL_opt1345z00_35); 
BgL_test7070z00_16806 = 
BOUND_CHECK(BgL_iz00_3716, BgL_tmpz00_16807); } 
if(BgL_test7070z00_16806)
{ /* Llib/date.scm 255 */
BgL_arg2717z00_3722 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_iz00_3716); }  else 
{ 
 obj_t BgL_auxz00_16811;
BgL_auxz00_16811 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5908z00zz__datez00, BgL_opt1345z00_35, 
(int)(
VECTOR_LENGTH(BgL_opt1345z00_35)), 
(int)(BgL_iz00_3716)); 
FAILURE(BgL_auxz00_16811,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol5943z00zz__datez00, BGl_string5930z00zz__datez00, BgL_arg2717z00_3722); } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1351z00_3724;
BgL_index1351z00_3724 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5910z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7071z00_16821;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8207;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16822;
if(
INTEGERP(BgL_index1351z00_3724))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16822 = BgL_index1351z00_3724
; }  else 
{ 
 obj_t BgL_auxz00_16825;
BgL_auxz00_16825 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1351z00_3724); 
FAILURE(BgL_auxz00_16825,BFALSE,BFALSE);} 
BgL_n1z00_8207 = 
(long)CINT(BgL_tmpz00_16822); } 
BgL_test7071z00_16821 = 
(BgL_n1z00_8207>=0L); } 
if(BgL_test7071z00_16821)
{ 
 long BgL_auxz00_16831;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16832;
if(
INTEGERP(BgL_index1351z00_3724))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16832 = BgL_index1351z00_3724
; }  else 
{ 
 obj_t BgL_auxz00_16835;
BgL_auxz00_16835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1351z00_3724); 
FAILURE(BgL_auxz00_16835,BFALSE,BFALSE);} 
BgL_auxz00_16831 = 
(long)CINT(BgL_tmpz00_16832); } 
BgL_dayz00_3709 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16831); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1352z00_3726;
BgL_index1352z00_3726 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5914z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7074z00_16843;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8208;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16844;
if(
INTEGERP(BgL_index1352z00_3726))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16844 = BgL_index1352z00_3726
; }  else 
{ 
 obj_t BgL_auxz00_16847;
BgL_auxz00_16847 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1352z00_3726); 
FAILURE(BgL_auxz00_16847,BFALSE,BFALSE);} 
BgL_n1z00_8208 = 
(long)CINT(BgL_tmpz00_16844); } 
BgL_test7074z00_16843 = 
(BgL_n1z00_8208>=0L); } 
if(BgL_test7074z00_16843)
{ 
 long BgL_auxz00_16853;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16854;
if(
INTEGERP(BgL_index1352z00_3726))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16854 = BgL_index1352z00_3726
; }  else 
{ 
 obj_t BgL_auxz00_16857;
BgL_auxz00_16857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1352z00_3726); 
FAILURE(BgL_auxz00_16857,BFALSE,BFALSE);} 
BgL_auxz00_16853 = 
(long)CINT(BgL_tmpz00_16854); } 
BgL_hourz00_3710 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16853); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1353z00_3728;
BgL_index1353z00_3728 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5916z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7077z00_16865;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8209;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16866;
if(
INTEGERP(BgL_index1353z00_3728))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16866 = BgL_index1353z00_3728
; }  else 
{ 
 obj_t BgL_auxz00_16869;
BgL_auxz00_16869 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1353z00_3728); 
FAILURE(BgL_auxz00_16869,BFALSE,BFALSE);} 
BgL_n1z00_8209 = 
(long)CINT(BgL_tmpz00_16866); } 
BgL_test7077z00_16865 = 
(BgL_n1z00_8209>=0L); } 
if(BgL_test7077z00_16865)
{ 
 long BgL_auxz00_16875;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16876;
if(
INTEGERP(BgL_index1353z00_3728))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16876 = BgL_index1353z00_3728
; }  else 
{ 
 obj_t BgL_auxz00_16879;
BgL_auxz00_16879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1353z00_3728); 
FAILURE(BgL_auxz00_16879,BFALSE,BFALSE);} 
BgL_auxz00_16875 = 
(long)CINT(BgL_tmpz00_16876); } 
BgL_minz00_3711 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16875); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1354z00_3730;
BgL_index1354z00_3730 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5918z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7080z00_16887;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8210;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16888;
if(
INTEGERP(BgL_index1354z00_3730))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16888 = BgL_index1354z00_3730
; }  else 
{ 
 obj_t BgL_auxz00_16891;
BgL_auxz00_16891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1354z00_3730); 
FAILURE(BgL_auxz00_16891,BFALSE,BFALSE);} 
BgL_n1z00_8210 = 
(long)CINT(BgL_tmpz00_16888); } 
BgL_test7080z00_16887 = 
(BgL_n1z00_8210>=0L); } 
if(BgL_test7080z00_16887)
{ 
 long BgL_auxz00_16897;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16898;
if(
INTEGERP(BgL_index1354z00_3730))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16898 = BgL_index1354z00_3730
; }  else 
{ 
 obj_t BgL_auxz00_16901;
BgL_auxz00_16901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1354z00_3730); 
FAILURE(BgL_auxz00_16901,BFALSE,BFALSE);} 
BgL_auxz00_16897 = 
(long)CINT(BgL_tmpz00_16898); } 
BgL_monthz00_3712 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16897); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1355z00_3732;
BgL_index1355z00_3732 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5920z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7083z00_16909;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8211;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16910;
if(
INTEGERP(BgL_index1355z00_3732))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16910 = BgL_index1355z00_3732
; }  else 
{ 
 obj_t BgL_auxz00_16913;
BgL_auxz00_16913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1355z00_3732); 
FAILURE(BgL_auxz00_16913,BFALSE,BFALSE);} 
BgL_n1z00_8211 = 
(long)CINT(BgL_tmpz00_16910); } 
BgL_test7083z00_16909 = 
(BgL_n1z00_8211>=0L); } 
if(BgL_test7083z00_16909)
{ 
 long BgL_auxz00_16919;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16920;
if(
INTEGERP(BgL_index1355z00_3732))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16920 = BgL_index1355z00_3732
; }  else 
{ 
 obj_t BgL_auxz00_16923;
BgL_auxz00_16923 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1355z00_3732); 
FAILURE(BgL_auxz00_16923,BFALSE,BFALSE);} 
BgL_auxz00_16919 = 
(long)CINT(BgL_tmpz00_16920); } 
BgL_nsecz00_3713 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16919); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1356z00_3734;
BgL_index1356z00_3734 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5922z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7086z00_16931;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8212;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16932;
if(
INTEGERP(BgL_index1356z00_3734))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16932 = BgL_index1356z00_3734
; }  else 
{ 
 obj_t BgL_auxz00_16935;
BgL_auxz00_16935 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1356z00_3734); 
FAILURE(BgL_auxz00_16935,BFALSE,BFALSE);} 
BgL_n1z00_8212 = 
(long)CINT(BgL_tmpz00_16932); } 
BgL_test7086z00_16931 = 
(BgL_n1z00_8212>=0L); } 
if(BgL_test7086z00_16931)
{ 
 long BgL_auxz00_16941;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16942;
if(
INTEGERP(BgL_index1356z00_3734))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16942 = BgL_index1356z00_3734
; }  else 
{ 
 obj_t BgL_auxz00_16945;
BgL_auxz00_16945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1356z00_3734); 
FAILURE(BgL_auxz00_16945,BFALSE,BFALSE);} 
BgL_auxz00_16941 = 
(long)CINT(BgL_tmpz00_16942); } 
BgL_secz00_3714 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16941); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_index1357z00_3736;
BgL_index1357z00_3736 = 
BGl_search1348ze70ze7zz__datez00(
VECTOR_LENGTH(BgL_opt1345z00_35), BgL_opt1345z00_35, BGl_keyword5926z00zz__datez00, 1L); 
{ /* Llib/date.scm 255 */
 bool_t BgL_test7089z00_16953;
{ /* Llib/date.scm 255 */
 long BgL_n1z00_8213;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16954;
if(
INTEGERP(BgL_index1357z00_3736))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16954 = BgL_index1357z00_3736
; }  else 
{ 
 obj_t BgL_auxz00_16957;
BgL_auxz00_16957 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1357z00_3736); 
FAILURE(BgL_auxz00_16957,BFALSE,BFALSE);} 
BgL_n1z00_8213 = 
(long)CINT(BgL_tmpz00_16954); } 
BgL_test7089z00_16953 = 
(BgL_n1z00_8213>=0L); } 
if(BgL_test7089z00_16953)
{ 
 long BgL_auxz00_16963;
{ /* Llib/date.scm 255 */
 obj_t BgL_tmpz00_16964;
if(
INTEGERP(BgL_index1357z00_3736))
{ /* Llib/date.scm 255 */
BgL_tmpz00_16964 = BgL_index1357z00_3736
; }  else 
{ 
 obj_t BgL_auxz00_16967;
BgL_auxz00_16967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5789z00zz__datez00, BgL_index1357z00_3736); 
FAILURE(BgL_auxz00_16967,BFALSE,BFALSE);} 
BgL_auxz00_16963 = 
(long)CINT(BgL_tmpz00_16964); } 
BgL_yearz00_3715 = 
VECTOR_REF(BgL_opt1345z00_35,BgL_auxz00_16963); }  else 
{ /* Llib/date.scm 255 */BFALSE; } } } 
{ /* Llib/date.scm 255 */
 obj_t BgL_arg2727z00_3738;
BgL_arg2727z00_3738 = 
VECTOR_REF(BgL_opt1345z00_35,0L); 
{ /* Llib/date.scm 255 */
 obj_t BgL_dayz00_3739;
BgL_dayz00_3739 = BgL_dayz00_3709; 
{ /* Llib/date.scm 255 */
 obj_t BgL_hourz00_3740;
BgL_hourz00_3740 = BgL_hourz00_3710; 
{ /* Llib/date.scm 255 */
 obj_t BgL_minz00_3741;
BgL_minz00_3741 = BgL_minz00_3711; 
{ /* Llib/date.scm 255 */
 obj_t BgL_monthz00_3742;
BgL_monthz00_3742 = BgL_monthz00_3712; 
{ /* Llib/date.scm 255 */
 obj_t BgL_nsecz00_3743;
BgL_nsecz00_3743 = BgL_nsecz00_3713; 
{ /* Llib/date.scm 255 */
 obj_t BgL_secz00_3744;
BgL_secz00_3744 = BgL_secz00_3714; 
{ /* Llib/date.scm 255 */
 obj_t BgL_yearz00_3745;
BgL_yearz00_3745 = BgL_yearz00_3715; 
{ /* Llib/date.scm 255 */
 obj_t BgL_auxz00_16974;
if(
BGL_DATEP(BgL_arg2727z00_3738))
{ /* Llib/date.scm 255 */
BgL_auxz00_16974 = BgL_arg2727z00_3738
; }  else 
{ 
 obj_t BgL_auxz00_16977;
BgL_auxz00_16977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12016L), BGl_string5945z00zz__datez00, BGl_string5940z00zz__datez00, BgL_arg2727z00_3738); 
FAILURE(BgL_auxz00_16977,BFALSE,BFALSE);} 
return 
BGl_datezd2updatez12zc0zz__datez00(BgL_auxz00_16974, BgL_dayz00_3739, BgL_hourz00_3740, BgL_minz00_3741, BgL_monthz00_3742, BgL_nsecz00_3743, BgL_secz00_3744, BgL_yearz00_3745);} } } } } } } } } } } } } } } } } } } 

}



/* search1348~0 */
obj_t BGl_search1348ze70ze7zz__datez00(long BgL_l1347z00_9880, obj_t BgL_opt1345z00_9879, obj_t BgL_k1z00_3705, long BgL_iz00_3706)
{
{ /* Llib/date.scm 255 */
BGl_search1348ze70ze7zz__datez00:
if(
(BgL_iz00_3706==BgL_l1347z00_9880))
{ /* Llib/date.scm 255 */
return 
BINT(-1L);}  else 
{ /* Llib/date.scm 255 */
if(
(BgL_iz00_3706==
(BgL_l1347z00_9880-1L)))
{ /* Llib/date.scm 255 */
return 
BGl_errorz00zz__errorz00(BGl_symbol5943z00zz__datez00, BGl_string5946z00zz__datez00, 
BINT(
VECTOR_LENGTH(BgL_opt1345z00_9879)));}  else 
{ /* Llib/date.scm 255 */
 obj_t BgL_vz00_3750;
BgL_vz00_3750 = 
VECTOR_REF(BgL_opt1345z00_9879,BgL_iz00_3706); 
if(
(BgL_vz00_3750==BgL_k1z00_3705))
{ /* Llib/date.scm 255 */
return 
BINT(
(BgL_iz00_3706+1L));}  else 
{ 
 long BgL_iz00_16996;
BgL_iz00_16996 = 
(BgL_iz00_3706+2L); 
BgL_iz00_3706 = BgL_iz00_16996; 
goto BGl_search1348ze70ze7zz__datez00;} } } } 

}



/* date-update! */
BGL_EXPORTED_DEF obj_t BGl_datezd2updatez12zc0zz__datez00(obj_t BgL_datez00_27, obj_t BgL_dayz00_28, obj_t BgL_hourz00_29, obj_t BgL_minz00_30, obj_t BgL_monthz00_31, obj_t BgL_nsecz00_32, obj_t BgL_secz00_33, obj_t BgL_yearz00_34)
{
{ /* Llib/date.scm 255 */
{ /* Llib/date.scm 257 */
 obj_t BgL_arg2736z00_3753; obj_t BgL_arg2737z00_3754; obj_t BgL_arg2738z00_3755; obj_t BgL_arg2739z00_3756; obj_t BgL_arg2740z00_3757; obj_t BgL_arg2741z00_3758; obj_t BgL_arg2742z00_3759; long BgL_arg2743z00_3760; bool_t BgL_arg2744z00_3761;
if(
CBOOL(BgL_nsecz00_32))
{ /* Llib/date.scm 257 */
BgL_arg2736z00_3753 = BgL_nsecz00_32; }  else 
{ /* Llib/date.scm 257 */
BgL_arg2736z00_3753 = 
make_bllong(
BGL_DATE_NANOSECOND(BgL_datez00_27)); } 
if(
CBOOL(BgL_secz00_33))
{ /* Llib/date.scm 258 */
BgL_arg2737z00_3754 = BgL_secz00_33; }  else 
{ /* Llib/date.scm 258 */
BgL_arg2737z00_3754 = 
BINT(
BGL_DATE_SECOND(BgL_datez00_27)); } 
if(
CBOOL(BgL_minz00_30))
{ /* Llib/date.scm 259 */
BgL_arg2738z00_3755 = BgL_minz00_30; }  else 
{ /* Llib/date.scm 259 */
BgL_arg2738z00_3755 = 
BINT(
BGL_DATE_MINUTE(BgL_datez00_27)); } 
if(
CBOOL(BgL_hourz00_29))
{ /* Llib/date.scm 260 */
BgL_arg2739z00_3756 = BgL_hourz00_29; }  else 
{ /* Llib/date.scm 260 */
BgL_arg2739z00_3756 = 
BINT(
BGL_DATE_HOUR(BgL_datez00_27)); } 
if(
CBOOL(BgL_dayz00_28))
{ /* Llib/date.scm 261 */
BgL_arg2740z00_3757 = BgL_dayz00_28; }  else 
{ /* Llib/date.scm 261 */
BgL_arg2740z00_3757 = 
BINT(
BGL_DATE_DAY(BgL_datez00_27)); } 
if(
CBOOL(BgL_monthz00_31))
{ /* Llib/date.scm 262 */
BgL_arg2741z00_3758 = BgL_monthz00_31; }  else 
{ /* Llib/date.scm 262 */
BgL_arg2741z00_3758 = 
BINT(
BGL_DATE_MONTH(BgL_datez00_27)); } 
if(
CBOOL(BgL_yearz00_34))
{ /* Llib/date.scm 263 */
BgL_arg2742z00_3759 = BgL_yearz00_34; }  else 
{ /* Llib/date.scm 263 */
BgL_arg2742z00_3759 = 
BINT(
BGL_DATE_YEAR(BgL_datez00_27)); } 
BgL_arg2743z00_3760 = 
BGL_DATE_TIMEZONE(BgL_datez00_27); 
BgL_arg2744z00_3761 = 
BGL_DATE_ISGMT(BgL_datez00_27); 
{ /* Llib/date.scm 256 */
 int BgL_auxz00_17082; int BgL_auxz00_17073; int BgL_auxz00_17064; int BgL_auxz00_17055; int BgL_auxz00_17046; int BgL_auxz00_17037; BGL_LONGLONG_T BgL_tmpz00_17028;
{ /* Llib/date.scm 263 */
 obj_t BgL_tmpz00_17083;
if(
INTEGERP(BgL_arg2742z00_3759))
{ /* Llib/date.scm 263 */
BgL_tmpz00_17083 = BgL_arg2742z00_3759
; }  else 
{ 
 obj_t BgL_auxz00_17086;
BgL_auxz00_17086 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12342L), BGl_string5944z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2742z00_3759); 
FAILURE(BgL_auxz00_17086,BFALSE,BFALSE);} 
BgL_auxz00_17082 = 
CINT(BgL_tmpz00_17083); } 
{ /* Llib/date.scm 262 */
 obj_t BgL_tmpz00_17074;
if(
INTEGERP(BgL_arg2741z00_3758))
{ /* Llib/date.scm 262 */
BgL_tmpz00_17074 = BgL_arg2741z00_3758
; }  else 
{ 
 obj_t BgL_auxz00_17077;
BgL_auxz00_17077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12309L), BGl_string5944z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2741z00_3758); 
FAILURE(BgL_auxz00_17077,BFALSE,BFALSE);} 
BgL_auxz00_17073 = 
CINT(BgL_tmpz00_17074); } 
{ /* Llib/date.scm 261 */
 obj_t BgL_tmpz00_17065;
if(
INTEGERP(BgL_arg2740z00_3757))
{ /* Llib/date.scm 261 */
BgL_tmpz00_17065 = BgL_arg2740z00_3757
; }  else 
{ 
 obj_t BgL_auxz00_17068;
BgL_auxz00_17068 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12274L), BGl_string5944z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2740z00_3757); 
FAILURE(BgL_auxz00_17068,BFALSE,BFALSE);} 
BgL_auxz00_17064 = 
CINT(BgL_tmpz00_17065); } 
{ /* Llib/date.scm 260 */
 obj_t BgL_tmpz00_17056;
if(
INTEGERP(BgL_arg2739z00_3756))
{ /* Llib/date.scm 260 */
BgL_tmpz00_17056 = BgL_arg2739z00_3756
; }  else 
{ 
 obj_t BgL_auxz00_17059;
BgL_auxz00_17059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12243L), BGl_string5944z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2739z00_3756); 
FAILURE(BgL_auxz00_17059,BFALSE,BFALSE);} 
BgL_auxz00_17055 = 
CINT(BgL_tmpz00_17056); } 
{ /* Llib/date.scm 259 */
 obj_t BgL_tmpz00_17047;
if(
INTEGERP(BgL_arg2738z00_3755))
{ /* Llib/date.scm 259 */
BgL_tmpz00_17047 = BgL_arg2738z00_3755
; }  else 
{ 
 obj_t BgL_auxz00_17050;
BgL_auxz00_17050 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12210L), BGl_string5944z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2738z00_3755); 
FAILURE(BgL_auxz00_17050,BFALSE,BFALSE);} 
BgL_auxz00_17046 = 
CINT(BgL_tmpz00_17047); } 
{ /* Llib/date.scm 258 */
 obj_t BgL_tmpz00_17038;
if(
INTEGERP(BgL_arg2737z00_3754))
{ /* Llib/date.scm 258 */
BgL_tmpz00_17038 = BgL_arg2737z00_3754
; }  else 
{ 
 obj_t BgL_auxz00_17041;
BgL_auxz00_17041 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12176L), BGl_string5944z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2737z00_3754); 
FAILURE(BgL_auxz00_17041,BFALSE,BFALSE);} 
BgL_auxz00_17037 = 
CINT(BgL_tmpz00_17038); } 
{ /* Llib/date.scm 257 */
 obj_t BgL_tmpz00_17029;
if(
LLONGP(BgL_arg2736z00_3753))
{ /* Llib/date.scm 257 */
BgL_tmpz00_17029 = BgL_arg2736z00_3753
; }  else 
{ 
 obj_t BgL_auxz00_17032;
BgL_auxz00_17032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12142L), BGl_string5944z00zz__datez00, BGl_string5932z00zz__datez00, BgL_arg2736z00_3753); 
FAILURE(BgL_auxz00_17032,BFALSE,BFALSE);} 
BgL_tmpz00_17028 = 
BLLONG_TO_LLONG(BgL_tmpz00_17029); } 
return 
bgl_update_date(BgL_datez00_27, BgL_tmpz00_17028, BgL_auxz00_17037, BgL_auxz00_17046, BgL_auxz00_17055, BgL_auxz00_17064, BgL_auxz00_17073, BgL_auxz00_17082, BgL_arg2743z00_3760, BgL_arg2744z00_3761, 
(int)(-1L));} } } 

}



/* date->gmtdate! */
BGL_EXPORTED_DEF obj_t BGl_datezd2ze3gmtdatez12z23zz__datez00(obj_t BgL_datez00_37)
{
{ /* Llib/date.scm 271 */
BGL_TAIL return 
bgl_date_to_gmtdate(BgL_datez00_37);} 

}



/* &date->gmtdate! */
obj_t BGl_z62datezd2ze3gmtdatez12z41zz__datez00(obj_t BgL_envz00_9771, obj_t BgL_datez00_9772)
{
{ /* Llib/date.scm 271 */
{ /* Llib/date.scm 274 */
 obj_t BgL_auxz00_17094;
if(
BGL_DATEP(BgL_datez00_9772))
{ /* Llib/date.scm 274 */
BgL_auxz00_17094 = BgL_datez00_9772
; }  else 
{ 
 obj_t BgL_auxz00_17097;
BgL_auxz00_17097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(12707L), BGl_string5947z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9772); 
FAILURE(BgL_auxz00_17097,BFALSE,BFALSE);} 
return 
BGl_datezd2ze3gmtdatez12z23zz__datez00(BgL_auxz00_17094);} } 

}



/* date-update-millisecond! */
BGL_EXPORTED_DEF obj_t BGl_datezd2updatezd2millisecondz12z12zz__datez00(obj_t BgL_datez00_38, long BgL_msz00_39)
{
{ /* Llib/date.scm 282 */
{ /* Llib/date.scm 285 */
 bool_t BgL_test7111z00_17102;
if(
(BgL_msz00_39>=0L))
{ /* Llib/date.scm 285 */
BgL_test7111z00_17102 = 
(BgL_msz00_39<1000L)
; }  else 
{ /* Llib/date.scm 285 */
BgL_test7111z00_17102 = ((bool_t)0)
; } 
if(BgL_test7111z00_17102)
{ /* Llib/date.scm 286 */
 int BgL_osecz00_3772;
BgL_osecz00_3772 = 
BGL_DATE_SECOND(BgL_datez00_38); 
BGL_DATE_UPDATE_MILLISECOND(BgL_datez00_38, BgL_msz00_39); 
return BgL_datez00_38;}  else 
{ /* Llib/date.scm 289 */
 BGL_LONGLONG_T BgL_arg2747z00_3773;
{ /* Llib/date.scm 289 */
 BGL_LONGLONG_T BgL_arg2748z00_3781;
BgL_arg2748z00_3781 = 
LONG_TO_LLONG(BgL_msz00_39); 
BgL_arg2747z00_3773 = 
((16960 + ((BGL_LONGLONG_T)65536 * (((BGL_LONGLONG_T)15))))*BgL_arg2748z00_3781); } 
return 
BGl_datezd2updatez12zc0zz__datez00(BgL_datez00_38, BFALSE, BFALSE, BFALSE, BFALSE, 
make_bllong(BgL_arg2747z00_3773), BFALSE, BFALSE);} } } 

}



/* &date-update-millisecond! */
obj_t BGl_z62datezd2updatezd2millisecondz12z70zz__datez00(obj_t BgL_envz00_9773, obj_t BgL_datez00_9774, obj_t BgL_msz00_9775)
{
{ /* Llib/date.scm 282 */
{ /* Llib/date.scm 285 */
 long BgL_auxz00_17119; obj_t BgL_auxz00_17112;
{ /* Llib/date.scm 285 */
 obj_t BgL_tmpz00_17120;
if(
INTEGERP(BgL_msz00_9775))
{ /* Llib/date.scm 285 */
BgL_tmpz00_17120 = BgL_msz00_9775
; }  else 
{ 
 obj_t BgL_auxz00_17123;
BgL_auxz00_17123 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(13149L), BGl_string5948z00zz__datez00, BGl_string5789z00zz__datez00, BgL_msz00_9775); 
FAILURE(BgL_auxz00_17123,BFALSE,BFALSE);} 
BgL_auxz00_17119 = 
(long)CINT(BgL_tmpz00_17120); } 
if(
BGL_DATEP(BgL_datez00_9774))
{ /* Llib/date.scm 285 */
BgL_auxz00_17112 = BgL_datez00_9774
; }  else 
{ 
 obj_t BgL_auxz00_17115;
BgL_auxz00_17115 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(13149L), BGl_string5948z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9774); 
FAILURE(BgL_auxz00_17115,BFALSE,BFALSE);} 
return 
BGl_datezd2updatezd2millisecondz12z12zz__datez00(BgL_auxz00_17112, BgL_auxz00_17119);} } 

}



/* date-update-second! */
BGL_EXPORTED_DEF obj_t BGl_datezd2updatezd2secondz12z12zz__datez00(obj_t BgL_datez00_40, long BgL_secz00_41)
{
{ /* Llib/date.scm 296 */
{ /* Llib/date.scm 299 */
 bool_t BgL_test7115z00_17129;
if(
(BgL_secz00_41>=0L))
{ /* Llib/date.scm 299 */
BgL_test7115z00_17129 = 
(BgL_secz00_41<60L)
; }  else 
{ /* Llib/date.scm 299 */
BgL_test7115z00_17129 = ((bool_t)0)
; } 
if(BgL_test7115z00_17129)
{ /* Llib/date.scm 300 */
 int BgL_osecz00_3785;
BgL_osecz00_3785 = 
BGL_DATE_SECOND(BgL_datez00_40); 
BGL_DATE_UPDATE_SECOND(BgL_datez00_40, BgL_secz00_41); 
{ /* Llib/date.scm 302 */
 long BgL_arg2751z00_3786;
{ /* Llib/date.scm 302 */
 long BgL_arg2753z00_3787; long BgL_arg2754z00_3788;
BgL_arg2753z00_3787 = 
BGL_DATE_TIME(BgL_datez00_40); 
BgL_arg2754z00_3788 = 
(BgL_secz00_41-
(long)(BgL_osecz00_3785)); 
BgL_arg2751z00_3786 = 
(BgL_arg2753z00_3787+BgL_arg2754z00_3788); } 
BGL_DATE_UPDATE_TIME(BgL_datez00_40, BgL_arg2751z00_3786); } 
return BgL_datez00_40;}  else 
{ /* Llib/date.scm 299 */
return 
BGl_datezd2updatez12zc0zz__datez00(BgL_datez00_40, BFALSE, BFALSE, BFALSE, BFALSE, BFALSE, 
BINT(BgL_secz00_41), BFALSE);} } } 

}



/* &date-update-second! */
obj_t BGl_z62datezd2updatezd2secondz12z70zz__datez00(obj_t BgL_envz00_9776, obj_t BgL_datez00_9777, obj_t BgL_secz00_9778)
{
{ /* Llib/date.scm 296 */
{ /* Llib/date.scm 299 */
 long BgL_auxz00_17149; obj_t BgL_auxz00_17142;
{ /* Llib/date.scm 299 */
 obj_t BgL_tmpz00_17150;
if(
INTEGERP(BgL_secz00_9778))
{ /* Llib/date.scm 299 */
BgL_tmpz00_17150 = BgL_secz00_9778
; }  else 
{ 
 obj_t BgL_auxz00_17153;
BgL_auxz00_17153 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(13737L), BGl_string5949z00zz__datez00, BGl_string5789z00zz__datez00, BgL_secz00_9778); 
FAILURE(BgL_auxz00_17153,BFALSE,BFALSE);} 
BgL_auxz00_17149 = 
(long)CINT(BgL_tmpz00_17150); } 
if(
BGL_DATEP(BgL_datez00_9777))
{ /* Llib/date.scm 299 */
BgL_auxz00_17142 = BgL_datez00_9777
; }  else 
{ 
 obj_t BgL_auxz00_17145;
BgL_auxz00_17145 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(13737L), BGl_string5949z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9777); 
FAILURE(BgL_auxz00_17145,BFALSE,BFALSE);} 
return 
BGl_datezd2updatezd2secondz12z12zz__datez00(BgL_auxz00_17142, BgL_auxz00_17149);} } 

}



/* date-update-minute! */
BGL_EXPORTED_DEF obj_t BGl_datezd2updatezd2minutez12z12zz__datez00(obj_t BgL_datez00_42, long BgL_minz00_43)
{
{ /* Llib/date.scm 311 */
{ /* Llib/date.scm 314 */
 bool_t BgL_test7119z00_17159;
if(
(BgL_minz00_43>=0L))
{ /* Llib/date.scm 314 */
BgL_test7119z00_17159 = 
(BgL_minz00_43<60L)
; }  else 
{ /* Llib/date.scm 314 */
BgL_test7119z00_17159 = ((bool_t)0)
; } 
if(BgL_test7119z00_17159)
{ /* Llib/date.scm 315 */
 int BgL_ominz00_3799;
BgL_ominz00_3799 = 
BGL_DATE_MINUTE(BgL_datez00_42); 
BGL_DATE_UPDATE_MINUTE(BgL_datez00_42, BgL_minz00_43); 
{ /* Llib/date.scm 318 */
 long BgL_arg2757z00_3800;
{ /* Llib/date.scm 318 */
 long BgL_arg2758z00_3801; long BgL_arg2759z00_3802;
BgL_arg2758z00_3801 = 
BGL_DATE_TIME(BgL_datez00_42); 
BgL_arg2759z00_3802 = 
(60L*
(BgL_minz00_43-
(long)(BgL_ominz00_3799))); 
BgL_arg2757z00_3800 = 
(BgL_arg2758z00_3801+BgL_arg2759z00_3802); } 
BGL_DATE_UPDATE_TIME(BgL_datez00_42, BgL_arg2757z00_3800); } 
return BgL_datez00_42;}  else 
{ /* Llib/date.scm 314 */
return 
BGl_datezd2updatez12zc0zz__datez00(BgL_datez00_42, BFALSE, BFALSE, 
BINT(BgL_minz00_43), BFALSE, BFALSE, BFALSE, BFALSE);} } } 

}



/* &date-update-minute! */
obj_t BGl_z62datezd2updatezd2minutez12z70zz__datez00(obj_t BgL_envz00_9779, obj_t BgL_datez00_9780, obj_t BgL_minz00_9781)
{
{ /* Llib/date.scm 311 */
{ /* Llib/date.scm 314 */
 long BgL_auxz00_17180; obj_t BgL_auxz00_17173;
{ /* Llib/date.scm 314 */
 obj_t BgL_tmpz00_17181;
if(
INTEGERP(BgL_minz00_9781))
{ /* Llib/date.scm 314 */
BgL_tmpz00_17181 = BgL_minz00_9781
; }  else 
{ 
 obj_t BgL_auxz00_17184;
BgL_auxz00_17184 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(14322L), BGl_string5950z00zz__datez00, BGl_string5789z00zz__datez00, BgL_minz00_9781); 
FAILURE(BgL_auxz00_17184,BFALSE,BFALSE);} 
BgL_auxz00_17180 = 
(long)CINT(BgL_tmpz00_17181); } 
if(
BGL_DATEP(BgL_datez00_9780))
{ /* Llib/date.scm 314 */
BgL_auxz00_17173 = BgL_datez00_9780
; }  else 
{ 
 obj_t BgL_auxz00_17176;
BgL_auxz00_17176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(14322L), BGl_string5950z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9780); 
FAILURE(BgL_auxz00_17176,BFALSE,BFALSE);} 
return 
BGl_datezd2updatezd2minutez12z12zz__datez00(BgL_auxz00_17173, BgL_auxz00_17180);} } 

}



/* integer->second */
BGL_EXPORTED_DEF long BGl_integerzd2ze3secondz31zz__datez00(long BgL_iz00_44)
{
{ /* Llib/date.scm 327 */
return 
(long)(BgL_iz00_44);} 

}



/* &integer->second */
obj_t BGl_z62integerzd2ze3secondz53zz__datez00(obj_t BgL_envz00_9782, obj_t BgL_iz00_9783)
{
{ /* Llib/date.scm 327 */
{ /* Llib/date.scm 328 */
 long BgL_tmpz00_17191;
{ /* Llib/date.scm 328 */
 long BgL_auxz00_17192;
{ /* Llib/date.scm 328 */
 obj_t BgL_tmpz00_17193;
if(
INTEGERP(BgL_iz00_9783))
{ /* Llib/date.scm 328 */
BgL_tmpz00_17193 = BgL_iz00_9783
; }  else 
{ 
 obj_t BgL_auxz00_17196;
BgL_auxz00_17196 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(14879L), BGl_string5951z00zz__datez00, BGl_string5789z00zz__datez00, BgL_iz00_9783); 
FAILURE(BgL_auxz00_17196,BFALSE,BFALSE);} 
BgL_auxz00_17192 = 
(long)CINT(BgL_tmpz00_17193); } 
BgL_tmpz00_17191 = 
BGl_integerzd2ze3secondz31zz__datez00(BgL_auxz00_17192); } 
return 
make_belong(BgL_tmpz00_17191);} } 

}



/* date-nanosecond */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_datezd2nanosecondzd2zz__datez00(obj_t BgL_dz00_45)
{
{ /* Llib/date.scm 333 */
return 
BGL_DATE_NANOSECOND(BgL_dz00_45);} 

}



/* &date-nanosecond */
obj_t BGl_z62datezd2nanosecondzb0zz__datez00(obj_t BgL_envz00_9784, obj_t BgL_dz00_9785)
{
{ /* Llib/date.scm 333 */
{ /* Llib/date.scm 334 */
 BGL_LONGLONG_T BgL_tmpz00_17204;
{ /* Llib/date.scm 334 */
 obj_t BgL_auxz00_17205;
if(
BGL_DATEP(BgL_dz00_9785))
{ /* Llib/date.scm 334 */
BgL_auxz00_17205 = BgL_dz00_9785
; }  else 
{ 
 obj_t BgL_auxz00_17208;
BgL_auxz00_17208 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(15173L), BGl_string5952z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9785); 
FAILURE(BgL_auxz00_17208,BFALSE,BFALSE);} 
BgL_tmpz00_17204 = 
BGl_datezd2nanosecondzd2zz__datez00(BgL_auxz00_17205); } 
return 
make_bllong(BgL_tmpz00_17204);} } 

}



/* date-millisecond */
BGL_EXPORTED_DEF long BGl_datezd2millisecondzd2zz__datez00(obj_t BgL_dz00_46)
{
{ /* Llib/date.scm 339 */
return 
BGL_DATE_MILLISECOND(BgL_dz00_46);} 

}



/* &date-millisecond */
obj_t BGl_z62datezd2millisecondzb0zz__datez00(obj_t BgL_envz00_9786, obj_t BgL_dz00_9787)
{
{ /* Llib/date.scm 339 */
{ /* Llib/date.scm 340 */
 long BgL_tmpz00_17215;
{ /* Llib/date.scm 340 */
 obj_t BgL_auxz00_17216;
if(
BGL_DATEP(BgL_dz00_9787))
{ /* Llib/date.scm 340 */
BgL_auxz00_17216 = BgL_dz00_9787
; }  else 
{ 
 obj_t BgL_auxz00_17219;
BgL_auxz00_17219 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(15463L), BGl_string5953z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9787); 
FAILURE(BgL_auxz00_17219,BFALSE,BFALSE);} 
BgL_tmpz00_17215 = 
BGl_datezd2millisecondzd2zz__datez00(BgL_auxz00_17216); } 
return 
BINT(BgL_tmpz00_17215);} } 

}



/* date-second */
BGL_EXPORTED_DEF int BGl_datezd2secondzd2zz__datez00(obj_t BgL_dz00_47)
{
{ /* Llib/date.scm 345 */
return 
BGL_DATE_SECOND(BgL_dz00_47);} 

}



/* &date-second */
obj_t BGl_z62datezd2secondzb0zz__datez00(obj_t BgL_envz00_9788, obj_t BgL_dz00_9789)
{
{ /* Llib/date.scm 345 */
{ /* Llib/date.scm 346 */
 int BgL_tmpz00_17226;
{ /* Llib/date.scm 346 */
 obj_t BgL_auxz00_17227;
if(
BGL_DATEP(BgL_dz00_9789))
{ /* Llib/date.scm 346 */
BgL_auxz00_17227 = BgL_dz00_9789
; }  else 
{ 
 obj_t BgL_auxz00_17230;
BgL_auxz00_17230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(15749L), BGl_string5954z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9789); 
FAILURE(BgL_auxz00_17230,BFALSE,BFALSE);} 
BgL_tmpz00_17226 = 
BGl_datezd2secondzd2zz__datez00(BgL_auxz00_17227); } 
return 
BINT(BgL_tmpz00_17226);} } 

}



/* date-minute */
BGL_EXPORTED_DEF int BGl_datezd2minutezd2zz__datez00(obj_t BgL_dz00_48)
{
{ /* Llib/date.scm 351 */
return 
BGL_DATE_MINUTE(BgL_dz00_48);} 

}



/* &date-minute */
obj_t BGl_z62datezd2minutezb0zz__datez00(obj_t BgL_envz00_9790, obj_t BgL_dz00_9791)
{
{ /* Llib/date.scm 351 */
{ /* Llib/date.scm 352 */
 int BgL_tmpz00_17237;
{ /* Llib/date.scm 352 */
 obj_t BgL_auxz00_17238;
if(
BGL_DATEP(BgL_dz00_9791))
{ /* Llib/date.scm 352 */
BgL_auxz00_17238 = BgL_dz00_9791
; }  else 
{ 
 obj_t BgL_auxz00_17241;
BgL_auxz00_17241 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(16030L), BGl_string5955z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9791); 
FAILURE(BgL_auxz00_17241,BFALSE,BFALSE);} 
BgL_tmpz00_17237 = 
BGl_datezd2minutezd2zz__datez00(BgL_auxz00_17238); } 
return 
BINT(BgL_tmpz00_17237);} } 

}



/* date-hour */
BGL_EXPORTED_DEF int BGl_datezd2hourzd2zz__datez00(obj_t BgL_dz00_49)
{
{ /* Llib/date.scm 357 */
return 
BGL_DATE_HOUR(BgL_dz00_49);} 

}



/* &date-hour */
obj_t BGl_z62datezd2hourzb0zz__datez00(obj_t BgL_envz00_9792, obj_t BgL_dz00_9793)
{
{ /* Llib/date.scm 357 */
{ /* Llib/date.scm 358 */
 int BgL_tmpz00_17248;
{ /* Llib/date.scm 358 */
 obj_t BgL_auxz00_17249;
if(
BGL_DATEP(BgL_dz00_9793))
{ /* Llib/date.scm 358 */
BgL_auxz00_17249 = BgL_dz00_9793
; }  else 
{ 
 obj_t BgL_auxz00_17252;
BgL_auxz00_17252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(16309L), BGl_string5956z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9793); 
FAILURE(BgL_auxz00_17252,BFALSE,BFALSE);} 
BgL_tmpz00_17248 = 
BGl_datezd2hourzd2zz__datez00(BgL_auxz00_17249); } 
return 
BINT(BgL_tmpz00_17248);} } 

}



/* date-day */
BGL_EXPORTED_DEF int BGl_datezd2dayzd2zz__datez00(obj_t BgL_dz00_50)
{
{ /* Llib/date.scm 363 */
return 
BGL_DATE_DAY(BgL_dz00_50);} 

}



/* &date-day */
obj_t BGl_z62datezd2dayzb0zz__datez00(obj_t BgL_envz00_9794, obj_t BgL_dz00_9795)
{
{ /* Llib/date.scm 363 */
{ /* Llib/date.scm 364 */
 int BgL_tmpz00_17259;
{ /* Llib/date.scm 364 */
 obj_t BgL_auxz00_17260;
if(
BGL_DATEP(BgL_dz00_9795))
{ /* Llib/date.scm 364 */
BgL_auxz00_17260 = BgL_dz00_9795
; }  else 
{ 
 obj_t BgL_auxz00_17263;
BgL_auxz00_17263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(16585L), BGl_string5957z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9795); 
FAILURE(BgL_auxz00_17263,BFALSE,BFALSE);} 
BgL_tmpz00_17259 = 
BGl_datezd2dayzd2zz__datez00(BgL_auxz00_17260); } 
return 
BINT(BgL_tmpz00_17259);} } 

}



/* date-week-day */
BGL_EXPORTED_DEF int BGl_datezd2weekzd2dayz00zz__datez00(obj_t BgL_dz00_51)
{
{ /* Llib/date.scm 369 */
return 
BGL_DATE_WDAY(BgL_dz00_51);} 

}



/* &date-week-day */
obj_t BGl_z62datezd2weekzd2dayz62zz__datez00(obj_t BgL_envz00_9796, obj_t BgL_dz00_9797)
{
{ /* Llib/date.scm 369 */
{ /* Llib/date.scm 370 */
 int BgL_tmpz00_17270;
{ /* Llib/date.scm 370 */
 obj_t BgL_auxz00_17271;
if(
BGL_DATEP(BgL_dz00_9797))
{ /* Llib/date.scm 370 */
BgL_auxz00_17271 = BgL_dz00_9797
; }  else 
{ 
 obj_t BgL_auxz00_17274;
BgL_auxz00_17274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(16865L), BGl_string5958z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9797); 
FAILURE(BgL_auxz00_17274,BFALSE,BFALSE);} 
BgL_tmpz00_17270 = 
BGl_datezd2weekzd2dayz00zz__datez00(BgL_auxz00_17271); } 
return 
BINT(BgL_tmpz00_17270);} } 

}



/* date-wday */
BGL_EXPORTED_DEF int BGl_datezd2wdayzd2zz__datez00(obj_t BgL_dz00_52)
{
{ /* Llib/date.scm 375 */
return 
BGL_DATE_WDAY(BgL_dz00_52);} 

}



/* &date-wday */
obj_t BGl_z62datezd2wdayzb0zz__datez00(obj_t BgL_envz00_9798, obj_t BgL_dz00_9799)
{
{ /* Llib/date.scm 375 */
{ /* Llib/date.scm 376 */
 int BgL_tmpz00_17281;
{ /* Llib/date.scm 376 */
 obj_t BgL_auxz00_17282;
if(
BGL_DATEP(BgL_dz00_9799))
{ /* Llib/date.scm 376 */
BgL_auxz00_17282 = BgL_dz00_9799
; }  else 
{ 
 obj_t BgL_auxz00_17285;
BgL_auxz00_17285 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(17142L), BGl_string5959z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9799); 
FAILURE(BgL_auxz00_17285,BFALSE,BFALSE);} 
BgL_tmpz00_17281 = 
BGl_datezd2wdayzd2zz__datez00(BgL_auxz00_17282); } 
return 
BINT(BgL_tmpz00_17281);} } 

}



/* date-year-day */
BGL_EXPORTED_DEF int BGl_datezd2yearzd2dayz00zz__datez00(obj_t BgL_dz00_53)
{
{ /* Llib/date.scm 381 */
return 
BGL_DATE_YDAY(BgL_dz00_53);} 

}



/* &date-year-day */
obj_t BGl_z62datezd2yearzd2dayz62zz__datez00(obj_t BgL_envz00_9800, obj_t BgL_dz00_9801)
{
{ /* Llib/date.scm 381 */
{ /* Llib/date.scm 382 */
 int BgL_tmpz00_17292;
{ /* Llib/date.scm 382 */
 obj_t BgL_auxz00_17293;
if(
BGL_DATEP(BgL_dz00_9801))
{ /* Llib/date.scm 382 */
BgL_auxz00_17293 = BgL_dz00_9801
; }  else 
{ 
 obj_t BgL_auxz00_17296;
BgL_auxz00_17296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(17423L), BGl_string5960z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9801); 
FAILURE(BgL_auxz00_17296,BFALSE,BFALSE);} 
BgL_tmpz00_17292 = 
BGl_datezd2yearzd2dayz00zz__datez00(BgL_auxz00_17293); } 
return 
BINT(BgL_tmpz00_17292);} } 

}



/* date-yday */
BGL_EXPORTED_DEF int BGl_datezd2ydayzd2zz__datez00(obj_t BgL_dz00_54)
{
{ /* Llib/date.scm 387 */
return 
BGL_DATE_YDAY(BgL_dz00_54);} 

}



/* &date-yday */
obj_t BGl_z62datezd2ydayzb0zz__datez00(obj_t BgL_envz00_9802, obj_t BgL_dz00_9803)
{
{ /* Llib/date.scm 387 */
{ /* Llib/date.scm 388 */
 int BgL_tmpz00_17303;
{ /* Llib/date.scm 388 */
 obj_t BgL_auxz00_17304;
if(
BGL_DATEP(BgL_dz00_9803))
{ /* Llib/date.scm 388 */
BgL_auxz00_17304 = BgL_dz00_9803
; }  else 
{ 
 obj_t BgL_auxz00_17307;
BgL_auxz00_17307 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(17700L), BGl_string5961z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9803); 
FAILURE(BgL_auxz00_17307,BFALSE,BFALSE);} 
BgL_tmpz00_17303 = 
BGl_datezd2ydayzd2zz__datez00(BgL_auxz00_17304); } 
return 
BINT(BgL_tmpz00_17303);} } 

}



/* date-month */
BGL_EXPORTED_DEF int BGl_datezd2monthzd2zz__datez00(obj_t BgL_dz00_55)
{
{ /* Llib/date.scm 393 */
return 
BGL_DATE_MONTH(BgL_dz00_55);} 

}



/* &date-month */
obj_t BGl_z62datezd2monthzb0zz__datez00(obj_t BgL_envz00_9804, obj_t BgL_dz00_9805)
{
{ /* Llib/date.scm 393 */
{ /* Llib/date.scm 394 */
 int BgL_tmpz00_17314;
{ /* Llib/date.scm 394 */
 obj_t BgL_auxz00_17315;
if(
BGL_DATEP(BgL_dz00_9805))
{ /* Llib/date.scm 394 */
BgL_auxz00_17315 = BgL_dz00_9805
; }  else 
{ 
 obj_t BgL_auxz00_17318;
BgL_auxz00_17318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(17978L), BGl_string5962z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9805); 
FAILURE(BgL_auxz00_17318,BFALSE,BFALSE);} 
BgL_tmpz00_17314 = 
BGl_datezd2monthzd2zz__datez00(BgL_auxz00_17315); } 
return 
BINT(BgL_tmpz00_17314);} } 

}



/* date-year */
BGL_EXPORTED_DEF int BGl_datezd2yearzd2zz__datez00(obj_t BgL_dz00_56)
{
{ /* Llib/date.scm 399 */
return 
BGL_DATE_YEAR(BgL_dz00_56);} 

}



/* &date-year */
obj_t BGl_z62datezd2yearzb0zz__datez00(obj_t BgL_envz00_9806, obj_t BgL_dz00_9807)
{
{ /* Llib/date.scm 399 */
{ /* Llib/date.scm 400 */
 int BgL_tmpz00_17325;
{ /* Llib/date.scm 400 */
 obj_t BgL_auxz00_17326;
if(
BGL_DATEP(BgL_dz00_9807))
{ /* Llib/date.scm 400 */
BgL_auxz00_17326 = BgL_dz00_9807
; }  else 
{ 
 obj_t BgL_auxz00_17329;
BgL_auxz00_17329 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(18256L), BGl_string5963z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9807); 
FAILURE(BgL_auxz00_17329,BFALSE,BFALSE);} 
BgL_tmpz00_17325 = 
BGl_datezd2yearzd2zz__datez00(BgL_auxz00_17326); } 
return 
BINT(BgL_tmpz00_17325);} } 

}



/* date-timezone */
BGL_EXPORTED_DEF long BGl_datezd2timeza7onez75zz__datez00(obj_t BgL_dz00_57)
{
{ /* Llib/date.scm 405 */
return 
BGL_DATE_TIMEZONE(BgL_dz00_57);} 

}



/* &date-timezone */
obj_t BGl_z62datezd2timeza7onez17zz__datez00(obj_t BgL_envz00_9808, obj_t BgL_dz00_9809)
{
{ /* Llib/date.scm 405 */
{ /* Llib/date.scm 406 */
 long BgL_tmpz00_17336;
{ /* Llib/date.scm 406 */
 obj_t BgL_auxz00_17337;
if(
BGL_DATEP(BgL_dz00_9809))
{ /* Llib/date.scm 406 */
BgL_auxz00_17337 = BgL_dz00_9809
; }  else 
{ 
 obj_t BgL_auxz00_17340;
BgL_auxz00_17340 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(18537L), BGl_string5964z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9809); 
FAILURE(BgL_auxz00_17340,BFALSE,BFALSE);} 
BgL_tmpz00_17336 = 
BGl_datezd2timeza7onez75zz__datez00(BgL_auxz00_17337); } 
return 
BINT(BgL_tmpz00_17336);} } 

}



/* date-zone-offset */
BGL_EXPORTED_DEF long BGl_datezd2za7onezd2offsetza7zz__datez00(obj_t BgL_dz00_58)
{
{ /* Llib/date.scm 411 */
{ /* Llib/date.scm 412 */
 long BgL_arg2761z00_11818;
BgL_arg2761z00_11818 = 
BGL_DATE_TIMEZONE(BgL_dz00_58); 
return 
(3600L*BgL_arg2761z00_11818);} } 

}



/* &date-zone-offset */
obj_t BGl_z62datezd2za7onezd2offsetzc5zz__datez00(obj_t BgL_envz00_9810, obj_t BgL_dz00_9811)
{
{ /* Llib/date.scm 411 */
{ /* Llib/date.scm 412 */
 long BgL_tmpz00_17348;
{ /* Llib/date.scm 412 */
 obj_t BgL_auxz00_17349;
if(
BGL_DATEP(BgL_dz00_9811))
{ /* Llib/date.scm 412 */
BgL_auxz00_17349 = BgL_dz00_9811
; }  else 
{ 
 obj_t BgL_auxz00_17352;
BgL_auxz00_17352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(18835L), BGl_string5965z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9811); 
FAILURE(BgL_auxz00_17352,BFALSE,BFALSE);} 
BgL_tmpz00_17348 = 
BGl_datezd2za7onezd2offsetza7zz__datez00(BgL_auxz00_17349); } 
return 
BINT(BgL_tmpz00_17348);} } 

}



/* date-is-dst */
BGL_EXPORTED_DEF int BGl_datezd2iszd2dstz00zz__datez00(obj_t BgL_dz00_59)
{
{ /* Llib/date.scm 417 */
return 
BGL_DATE_ISDST(BgL_dz00_59);} 

}



/* &date-is-dst */
obj_t BGl_z62datezd2iszd2dstz62zz__datez00(obj_t BgL_envz00_9812, obj_t BgL_dz00_9813)
{
{ /* Llib/date.scm 417 */
{ /* Llib/date.scm 418 */
 int BgL_tmpz00_17359;
{ /* Llib/date.scm 418 */
 obj_t BgL_auxz00_17360;
if(
BGL_DATEP(BgL_dz00_9813))
{ /* Llib/date.scm 418 */
BgL_auxz00_17360 = BgL_dz00_9813
; }  else 
{ 
 obj_t BgL_auxz00_17363;
BgL_auxz00_17363 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(19119L), BGl_string5966z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9813); 
FAILURE(BgL_auxz00_17363,BFALSE,BFALSE);} 
BgL_tmpz00_17359 = 
BGl_datezd2iszd2dstz00zz__datez00(BgL_auxz00_17360); } 
return 
BINT(BgL_tmpz00_17359);} } 

}



/* current-seconds */
BGL_EXPORTED_DEF long BGl_currentzd2secondszd2zz__datez00(void)
{
{ /* Llib/date.scm 423 */
BGL_TAIL return 
bgl_current_seconds();} 

}



/* &current-seconds */
obj_t BGl_z62currentzd2secondszb0zz__datez00(obj_t BgL_envz00_9814)
{
{ /* Llib/date.scm 423 */
{ /* Llib/date.scm 424 */
 long BgL_tmpz00_17370;
BgL_tmpz00_17370 = 
BGl_currentzd2secondszd2zz__datez00(); 
return 
make_belong(BgL_tmpz00_17370);} } 

}



/* current-milliseconds */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_currentzd2millisecondszd2zz__datez00(void)
{
{ /* Llib/date.scm 429 */
BGL_TAIL return 
bgl_current_milliseconds();} 

}



/* &current-milliseconds */
obj_t BGl_z62currentzd2millisecondszb0zz__datez00(obj_t BgL_envz00_9815)
{
{ /* Llib/date.scm 429 */
return 
make_bllong(
BGl_currentzd2millisecondszd2zz__datez00());} 

}



/* current-microseconds */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_currentzd2microsecondszd2zz__datez00(void)
{
{ /* Llib/date.scm 435 */
BGL_TAIL return 
bgl_current_microseconds();} 

}



/* &current-microseconds */
obj_t BGl_z62currentzd2microsecondszb0zz__datez00(obj_t BgL_envz00_9816)
{
{ /* Llib/date.scm 435 */
return 
make_bllong(
BGl_currentzd2microsecondszd2zz__datez00());} 

}



/* current-nanoseconds */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_currentzd2nanosecondszd2zz__datez00(void)
{
{ /* Llib/date.scm 441 */
BGL_TAIL return 
bgl_current_nanoseconds();} 

}



/* &current-nanoseconds */
obj_t BGl_z62currentzd2nanosecondszb0zz__datez00(obj_t BgL_envz00_9817)
{
{ /* Llib/date.scm 441 */
return 
make_bllong(
BGl_currentzd2nanosecondszd2zz__datez00());} 

}



/* current-date */
BGL_EXPORTED_DEF obj_t BGl_currentzd2datezd2zz__datez00(void)
{
{ /* Llib/date.scm 447 */
return 
bgl_nanoseconds_to_date(
bgl_current_nanoseconds());} 

}



/* &current-date */
obj_t BGl_z62currentzd2datezb0zz__datez00(obj_t BgL_envz00_9818)
{
{ /* Llib/date.scm 447 */
return 
BGl_currentzd2datezd2zz__datez00();} 

}



/* seconds->date */
BGL_EXPORTED_DEF obj_t BGl_secondszd2ze3datez31zz__datez00(long BgL_elongz00_60)
{
{ /* Llib/date.scm 453 */
BGL_TAIL return 
bgl_seconds_to_date(BgL_elongz00_60);} 

}



/* &seconds->date */
obj_t BGl_z62secondszd2ze3datez53zz__datez00(obj_t BgL_envz00_9819, obj_t BgL_elongz00_9820)
{
{ /* Llib/date.scm 453 */
{ /* Llib/date.scm 454 */
 long BgL_auxz00_17386;
{ /* Llib/date.scm 454 */
 obj_t BgL_tmpz00_17387;
if(
ELONGP(BgL_elongz00_9820))
{ /* Llib/date.scm 454 */
BgL_tmpz00_17387 = BgL_elongz00_9820
; }  else 
{ 
 obj_t BgL_auxz00_17390;
BgL_auxz00_17390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(20874L), BGl_string5967z00zz__datez00, BGl_string5968z00zz__datez00, BgL_elongz00_9820); 
FAILURE(BgL_auxz00_17390,BFALSE,BFALSE);} 
BgL_auxz00_17386 = 
BELONG_TO_LONG(BgL_tmpz00_17387); } 
return 
BGl_secondszd2ze3datez31zz__datez00(BgL_auxz00_17386);} } 

}



/* seconds->gmtdate */
BGL_EXPORTED_DEF obj_t BGl_secondszd2ze3gmtdatez31zz__datez00(long BgL_elongz00_61)
{
{ /* Llib/date.scm 459 */
BGL_TAIL return 
bgl_seconds_to_gmtdate(BgL_elongz00_61);} 

}



/* &seconds->gmtdate */
obj_t BGl_z62secondszd2ze3gmtdatez53zz__datez00(obj_t BgL_envz00_9821, obj_t BgL_elongz00_9822)
{
{ /* Llib/date.scm 459 */
{ /* Llib/date.scm 460 */
 long BgL_auxz00_17397;
{ /* Llib/date.scm 460 */
 obj_t BgL_tmpz00_17398;
if(
ELONGP(BgL_elongz00_9822))
{ /* Llib/date.scm 460 */
BgL_tmpz00_17398 = BgL_elongz00_9822
; }  else 
{ 
 obj_t BgL_auxz00_17401;
BgL_auxz00_17401 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(21168L), BGl_string5969z00zz__datez00, BGl_string5968z00zz__datez00, BgL_elongz00_9822); 
FAILURE(BgL_auxz00_17401,BFALSE,BFALSE);} 
BgL_auxz00_17397 = 
BELONG_TO_LONG(BgL_tmpz00_17398); } 
return 
BGl_secondszd2ze3gmtdatez31zz__datez00(BgL_auxz00_17397);} } 

}



/* milliseconds->gmtdate */
BGL_EXPORTED_DEF obj_t BGl_millisecondszd2ze3gmtdatez31zz__datez00(BGL_LONGLONG_T BgL_llongz00_62)
{
{ /* Llib/date.scm 465 */
BGL_TAIL return 
bgl_milliseconds_to_gmtdate(BgL_llongz00_62);} 

}



/* &milliseconds->gmtdate */
obj_t BGl_z62millisecondszd2ze3gmtdatez53zz__datez00(obj_t BgL_envz00_9823, obj_t BgL_llongz00_9824)
{
{ /* Llib/date.scm 465 */
{ /* Llib/date.scm 466 */
 BGL_LONGLONG_T BgL_auxz00_17408;
{ /* Llib/date.scm 466 */
 obj_t BgL_tmpz00_17409;
if(
LLONGP(BgL_llongz00_9824))
{ /* Llib/date.scm 466 */
BgL_tmpz00_17409 = BgL_llongz00_9824
; }  else 
{ 
 obj_t BgL_auxz00_17412;
BgL_auxz00_17412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(21471L), BGl_string5970z00zz__datez00, BGl_string5932z00zz__datez00, BgL_llongz00_9824); 
FAILURE(BgL_auxz00_17412,BFALSE,BFALSE);} 
BgL_auxz00_17408 = 
BLLONG_TO_LLONG(BgL_tmpz00_17409); } 
return 
BGl_millisecondszd2ze3gmtdatez31zz__datez00(BgL_auxz00_17408);} } 

}



/* nanoseconds->date */
BGL_EXPORTED_DEF obj_t BGl_nanosecondszd2ze3datez31zz__datez00(BGL_LONGLONG_T BgL_llongz00_63)
{
{ /* Llib/date.scm 471 */
BGL_TAIL return 
bgl_nanoseconds_to_date(BgL_llongz00_63);} 

}



/* &nanoseconds->date */
obj_t BGl_z62nanosecondszd2ze3datez53zz__datez00(obj_t BgL_envz00_9825, obj_t BgL_llongz00_9826)
{
{ /* Llib/date.scm 471 */
{ /* Llib/date.scm 472 */
 BGL_LONGLONG_T BgL_auxz00_17419;
{ /* Llib/date.scm 472 */
 obj_t BgL_tmpz00_17420;
if(
LLONGP(BgL_llongz00_9826))
{ /* Llib/date.scm 472 */
BgL_tmpz00_17420 = BgL_llongz00_9826
; }  else 
{ 
 obj_t BgL_auxz00_17423;
BgL_auxz00_17423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(21775L), BGl_string5971z00zz__datez00, BGl_string5932z00zz__datez00, BgL_llongz00_9826); 
FAILURE(BgL_auxz00_17423,BFALSE,BFALSE);} 
BgL_auxz00_17419 = 
BLLONG_TO_LLONG(BgL_tmpz00_17420); } 
return 
BGl_nanosecondszd2ze3datez31zz__datez00(BgL_auxz00_17419);} } 

}



/* milliseconds->date */
BGL_EXPORTED_DEF obj_t BGl_millisecondszd2ze3datez31zz__datez00(BGL_LONGLONG_T BgL_llongz00_64)
{
{ /* Llib/date.scm 477 */
BGL_TAIL return 
bgl_milliseconds_to_date(BgL_llongz00_64);} 

}



/* &milliseconds->date */
obj_t BGl_z62millisecondszd2ze3datez53zz__datez00(obj_t BgL_envz00_9827, obj_t BgL_llongz00_9828)
{
{ /* Llib/date.scm 477 */
{ /* Llib/date.scm 478 */
 BGL_LONGLONG_T BgL_auxz00_17430;
{ /* Llib/date.scm 478 */
 obj_t BgL_tmpz00_17431;
if(
LLONGP(BgL_llongz00_9828))
{ /* Llib/date.scm 478 */
BgL_tmpz00_17431 = BgL_llongz00_9828
; }  else 
{ 
 obj_t BgL_auxz00_17434;
BgL_auxz00_17434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(22075L), BGl_string5972z00zz__datez00, BGl_string5932z00zz__datez00, BgL_llongz00_9828); 
FAILURE(BgL_auxz00_17434,BFALSE,BFALSE);} 
BgL_auxz00_17430 = 
BLLONG_TO_LLONG(BgL_tmpz00_17431); } 
return 
BGl_millisecondszd2ze3datez31zz__datez00(BgL_auxz00_17430);} } 

}



/* date->seconds */
BGL_EXPORTED_DEF long BGl_datezd2ze3secondsz31zz__datez00(obj_t BgL_datez00_65)
{
{ /* Llib/date.scm 483 */
BGL_TAIL return 
bgl_date_to_seconds(BgL_datez00_65);} 

}



/* &date->seconds */
obj_t BGl_z62datezd2ze3secondsz53zz__datez00(obj_t BgL_envz00_9829, obj_t BgL_datez00_9830)
{
{ /* Llib/date.scm 483 */
{ /* Llib/date.scm 484 */
 long BgL_tmpz00_17441;
{ /* Llib/date.scm 484 */
 obj_t BgL_auxz00_17442;
if(
BGL_DATEP(BgL_datez00_9830))
{ /* Llib/date.scm 484 */
BgL_auxz00_17442 = BgL_datez00_9830
; }  else 
{ 
 obj_t BgL_auxz00_17445;
BgL_auxz00_17445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(22370L), BGl_string5973z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9830); 
FAILURE(BgL_auxz00_17445,BFALSE,BFALSE);} 
BgL_tmpz00_17441 = 
BGl_datezd2ze3secondsz31zz__datez00(BgL_auxz00_17442); } 
return 
make_belong(BgL_tmpz00_17441);} } 

}



/* date->nanoseconds */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_datezd2ze3nanosecondsz31zz__datez00(obj_t BgL_datez00_66)
{
{ /* Llib/date.scm 489 */
BGL_TAIL return 
bgl_date_to_nanoseconds(BgL_datez00_66);} 

}



/* &date->nanoseconds */
obj_t BGl_z62datezd2ze3nanosecondsz53zz__datez00(obj_t BgL_envz00_9831, obj_t BgL_datez00_9832)
{
{ /* Llib/date.scm 489 */
{ /* Llib/date.scm 490 */
 BGL_LONGLONG_T BgL_tmpz00_17452;
{ /* Llib/date.scm 490 */
 obj_t BgL_auxz00_17453;
if(
BGL_DATEP(BgL_datez00_9832))
{ /* Llib/date.scm 490 */
BgL_auxz00_17453 = BgL_datez00_9832
; }  else 
{ 
 obj_t BgL_auxz00_17456;
BgL_auxz00_17456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(22661L), BGl_string5974z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9832); 
FAILURE(BgL_auxz00_17456,BFALSE,BFALSE);} 
BgL_tmpz00_17452 = 
BGl_datezd2ze3nanosecondsz31zz__datez00(BgL_auxz00_17453); } 
return 
make_bllong(BgL_tmpz00_17452);} } 

}



/* date->milliseconds */
BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_datezd2ze3millisecondsz31zz__datez00(obj_t BgL_datez00_67)
{
{ /* Llib/date.scm 495 */
BGL_TAIL return 
bgl_date_to_milliseconds(BgL_datez00_67);} 

}



/* &date->milliseconds */
obj_t BGl_z62datezd2ze3millisecondsz53zz__datez00(obj_t BgL_envz00_9833, obj_t BgL_datez00_9834)
{
{ /* Llib/date.scm 495 */
{ /* Llib/date.scm 496 */
 BGL_LONGLONG_T BgL_tmpz00_17463;
{ /* Llib/date.scm 496 */
 obj_t BgL_auxz00_17464;
if(
BGL_DATEP(BgL_datez00_9834))
{ /* Llib/date.scm 496 */
BgL_auxz00_17464 = BgL_datez00_9834
; }  else 
{ 
 obj_t BgL_auxz00_17467;
BgL_auxz00_17467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(22957L), BGl_string5975z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9834); 
FAILURE(BgL_auxz00_17467,BFALSE,BFALSE);} 
BgL_tmpz00_17463 = 
BGl_datezd2ze3millisecondsz31zz__datez00(BgL_auxz00_17464); } 
return 
make_bllong(BgL_tmpz00_17463);} } 

}



/* date->string */
BGL_EXPORTED_DEF obj_t BGl_datezd2ze3stringz31zz__datez00(obj_t BgL_datez00_68)
{
{ /* Llib/date.scm 501 */
BGL_TAIL return 
BGl_datezd2ze3rfc2822zd2dateze3zz__datez00(BgL_datez00_68);} 

}



/* &date->string */
obj_t BGl_z62datezd2ze3stringz53zz__datez00(obj_t BgL_envz00_9835, obj_t BgL_datez00_9836)
{
{ /* Llib/date.scm 501 */
{ /* Llib/date.scm 502 */
 obj_t BgL_auxz00_17474;
if(
BGL_DATEP(BgL_datez00_9836))
{ /* Llib/date.scm 502 */
BgL_auxz00_17474 = BgL_datez00_9836
; }  else 
{ 
 obj_t BgL_auxz00_17477;
BgL_auxz00_17477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23241L), BGl_string5976z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9836); 
FAILURE(BgL_auxz00_17477,BFALSE,BFALSE);} 
return 
BGl_datezd2ze3stringz31zz__datez00(BgL_auxz00_17474);} } 

}



/* blit-int2! */
long BGl_blitzd2int2z12zc0zz__datez00(obj_t BgL_bufz00_72, long BgL_wz00_73, int BgL_intz00_74)
{
{ /* Llib/date.scm 513 */
if(
(
(long)(BgL_intz00_74)<10L))
{ /* Llib/date.scm 514 */
{ /* Llib/date.scm 516 */
 long BgL_l4810z00_9911;
BgL_l4810z00_9911 = 
STRING_LENGTH(BgL_bufz00_72); 
if(
BOUND_CHECK(BgL_wz00_73, BgL_l4810z00_9911))
{ /* Llib/date.scm 516 */
STRING_SET(BgL_bufz00_72, BgL_wz00_73, ((unsigned char)'0')); }  else 
{ 
 obj_t BgL_auxz00_17489;
BgL_auxz00_17489 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23888L), BGl_string5977z00zz__datez00, BgL_bufz00_72, 
(int)(BgL_l4810z00_9911), 
(int)(BgL_wz00_73)); 
FAILURE(BgL_auxz00_17489,BFALSE,BFALSE);} } 
{ /* Llib/date.scm 517 */
 long BgL_arg2767z00_3817;
BgL_arg2767z00_3817 = 
(BgL_wz00_73+1L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8258;
BgL_arg2764z00_8258 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+
(long)(BgL_intz00_74))); 
{ /* Llib/date.scm 508 */
 long BgL_l4814z00_9915;
BgL_l4814z00_9915 = 
STRING_LENGTH(BgL_bufz00_72); 
if(
BOUND_CHECK(BgL_arg2767z00_3817, BgL_l4814z00_9915))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_72, BgL_arg2767z00_3817, BgL_arg2764z00_8258); }  else 
{ 
 obj_t BgL_auxz00_17503;
BgL_auxz00_17503 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_72, 
(int)(BgL_l4814z00_9915), 
(int)(BgL_arg2767z00_3817)); 
FAILURE(BgL_auxz00_17503,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/date.scm 514 */
{ /* Llib/date.scm 519 */
 long BgL_arg2771z00_3818;
BgL_arg2771z00_3818 = 
(
(long)(BgL_intz00_74)/10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8265;
BgL_arg2764z00_8265 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2771z00_3818)); 
{ /* Llib/date.scm 508 */
 long BgL_l4818z00_9919;
BgL_l4818z00_9919 = 
STRING_LENGTH(BgL_bufz00_72); 
if(
BOUND_CHECK(BgL_wz00_73, BgL_l4818z00_9919))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_72, BgL_wz00_73, BgL_arg2764z00_8265); }  else 
{ 
 obj_t BgL_auxz00_17517;
BgL_auxz00_17517 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_72, 
(int)(BgL_l4818z00_9919), 
(int)(BgL_wz00_73)); 
FAILURE(BgL_auxz00_17517,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 520 */
 long BgL_arg2772z00_3819; long BgL_arg2773z00_3820;
BgL_arg2772z00_3819 = 
(BgL_wz00_73+1L); 
BgL_arg2773z00_3820 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_intz00_74), 10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8272;
BgL_arg2764z00_8272 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2773z00_3820)); 
{ /* Llib/date.scm 508 */
 long BgL_l4822z00_9923;
BgL_l4822z00_9923 = 
STRING_LENGTH(BgL_bufz00_72); 
if(
BOUND_CHECK(BgL_arg2772z00_3819, BgL_l4822z00_9923))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_72, BgL_arg2772z00_3819, BgL_arg2764z00_8272); }  else 
{ 
 obj_t BgL_auxz00_17532;
BgL_auxz00_17532 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_72, 
(int)(BgL_l4822z00_9923), 
(int)(BgL_arg2772z00_3819)); 
FAILURE(BgL_auxz00_17532,BFALSE,BFALSE);} } } } } 
return 2L;} 

}



/* blit-int! */
long BGl_blitzd2intz12zc0zz__datez00(obj_t BgL_bufz00_75, long BgL_wz00_76, int BgL_intz00_77)
{
{ /* Llib/date.scm 526 */
if(
(
(long)(BgL_intz00_77)<10L))
{ /* Llib/date.scm 528 */
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8279;
BgL_arg2764z00_8279 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+
(long)(BgL_intz00_77))); 
{ /* Llib/date.scm 508 */
 long BgL_l4826z00_9927;
BgL_l4826z00_9927 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_wz00_76, BgL_l4826z00_9927))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_wz00_76, BgL_arg2764z00_8279); }  else 
{ 
 obj_t BgL_auxz00_17548;
BgL_auxz00_17548 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4826z00_9927), 
(int)(BgL_wz00_76)); 
FAILURE(BgL_auxz00_17548,BFALSE,BFALSE);} } } 
return 1L;}  else 
{ /* Llib/date.scm 528 */
if(
(
(long)(BgL_intz00_77)<100L))
{ /* Llib/date.scm 531 */
{ /* Llib/date.scm 532 */
 long BgL_arg2776z00_3823;
BgL_arg2776z00_3823 = 
(
(long)(BgL_intz00_77)/10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8287;
BgL_arg2764z00_8287 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2776z00_3823)); 
{ /* Llib/date.scm 508 */
 long BgL_l4830z00_9931;
BgL_l4830z00_9931 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_wz00_76, BgL_l4830z00_9931))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_wz00_76, BgL_arg2764z00_8287); }  else 
{ 
 obj_t BgL_auxz00_17565;
BgL_auxz00_17565 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4830z00_9931), 
(int)(BgL_wz00_76)); 
FAILURE(BgL_auxz00_17565,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 533 */
 long BgL_arg2777z00_3824; long BgL_arg2778z00_3825;
BgL_arg2777z00_3824 = 
(BgL_wz00_76+1L); 
BgL_arg2778z00_3825 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_intz00_77), 10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8294;
BgL_arg2764z00_8294 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2778z00_3825)); 
{ /* Llib/date.scm 508 */
 long BgL_l4834z00_9935;
BgL_l4834z00_9935 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_arg2777z00_3824, BgL_l4834z00_9935))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_arg2777z00_3824, BgL_arg2764z00_8294); }  else 
{ 
 obj_t BgL_auxz00_17580;
BgL_auxz00_17580 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4834z00_9935), 
(int)(BgL_arg2777z00_3824)); 
FAILURE(BgL_auxz00_17580,BFALSE,BFALSE);} } } } 
return 2L;}  else 
{ /* Llib/date.scm 531 */
if(
(
(long)(BgL_intz00_77)<1000L))
{ /* Llib/date.scm 535 */
{ /* Llib/date.scm 536 */
 long BgL_arg2780z00_3827;
BgL_arg2780z00_3827 = 
(
(long)(BgL_intz00_77)/100L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8302;
BgL_arg2764z00_8302 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2780z00_3827)); 
{ /* Llib/date.scm 508 */
 long BgL_l4838z00_9939;
BgL_l4838z00_9939 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_wz00_76, BgL_l4838z00_9939))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_wz00_76, BgL_arg2764z00_8302); }  else 
{ 
 obj_t BgL_auxz00_17597;
BgL_auxz00_17597 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4838z00_9939), 
(int)(BgL_wz00_76)); 
FAILURE(BgL_auxz00_17597,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 537 */
 long BgL_rz00_3828;
BgL_rz00_3828 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_intz00_77), 100L); 
{ /* Llib/date.scm 538 */
 long BgL_arg2781z00_3829; long BgL_arg2783z00_3830;
BgL_arg2781z00_3829 = 
(BgL_wz00_76+1L); 
BgL_arg2783z00_3830 = 
(BgL_rz00_3828/10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8310;
BgL_arg2764z00_8310 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2783z00_3830)); 
{ /* Llib/date.scm 508 */
 long BgL_l4842z00_9943;
BgL_l4842z00_9943 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_arg2781z00_3829, BgL_l4842z00_9943))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_arg2781z00_3829, BgL_arg2764z00_8310); }  else 
{ 
 obj_t BgL_auxz00_17613;
BgL_auxz00_17613 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4842z00_9943), 
(int)(BgL_arg2781z00_3829)); 
FAILURE(BgL_auxz00_17613,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 539 */
 long BgL_arg2784z00_3831; long BgL_arg2786z00_3832;
BgL_arg2784z00_3831 = 
(BgL_wz00_76+2L); 
BgL_arg2786z00_3832 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(BgL_rz00_3828, 10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8317;
BgL_arg2764z00_8317 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2786z00_3832)); 
{ /* Llib/date.scm 508 */
 long BgL_l4846z00_9947;
BgL_l4846z00_9947 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_arg2784z00_3831, BgL_l4846z00_9947))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_arg2784z00_3831, BgL_arg2764z00_8317); }  else 
{ 
 obj_t BgL_auxz00_17627;
BgL_auxz00_17627 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4846z00_9947), 
(int)(BgL_arg2784z00_3831)); 
FAILURE(BgL_auxz00_17627,BFALSE,BFALSE);} } } } 
return 3L;} }  else 
{ /* Llib/date.scm 535 */
{ /* Llib/date.scm 542 */
 long BgL_arg2787z00_3833;
BgL_arg2787z00_3833 = 
(
(long)(BgL_intz00_77)/1000L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8324;
BgL_arg2764z00_8324 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2787z00_3833)); 
{ /* Llib/date.scm 508 */
 long BgL_l4850z00_9951;
BgL_l4850z00_9951 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_wz00_76, BgL_l4850z00_9951))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_wz00_76, BgL_arg2764z00_8324); }  else 
{ 
 obj_t BgL_auxz00_17641;
BgL_auxz00_17641 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4850z00_9951), 
(int)(BgL_wz00_76)); 
FAILURE(BgL_auxz00_17641,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 543 */
 long BgL_rz00_3834;
BgL_rz00_3834 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_intz00_77), 1000L); 
{ /* Llib/date.scm 544 */
 long BgL_arg2789z00_3835; long BgL_arg2793z00_3836;
BgL_arg2789z00_3835 = 
(BgL_wz00_76+1L); 
BgL_arg2793z00_3836 = 
(BgL_rz00_3834/100L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8332;
BgL_arg2764z00_8332 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2793z00_3836)); 
{ /* Llib/date.scm 508 */
 long BgL_l4854z00_9955;
BgL_l4854z00_9955 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_arg2789z00_3835, BgL_l4854z00_9955))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_arg2789z00_3835, BgL_arg2764z00_8332); }  else 
{ 
 obj_t BgL_auxz00_17657;
BgL_auxz00_17657 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4854z00_9955), 
(int)(BgL_arg2789z00_3835)); 
FAILURE(BgL_auxz00_17657,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 545 */
 long BgL_rz00_3837;
BgL_rz00_3837 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_intz00_77), 100L); 
{ /* Llib/date.scm 546 */
 long BgL_arg2794z00_3838; long BgL_arg2799z00_3839;
BgL_arg2794z00_3838 = 
(BgL_wz00_76+2L); 
BgL_arg2799z00_3839 = 
(BgL_rz00_3837/10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8340;
BgL_arg2764z00_8340 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2799z00_3839)); 
{ /* Llib/date.scm 508 */
 long BgL_l4858z00_9959;
BgL_l4858z00_9959 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_arg2794z00_3838, BgL_l4858z00_9959))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_arg2794z00_3838, BgL_arg2764z00_8340); }  else 
{ 
 obj_t BgL_auxz00_17673;
BgL_auxz00_17673 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4858z00_9959), 
(int)(BgL_arg2794z00_3838)); 
FAILURE(BgL_auxz00_17673,BFALSE,BFALSE);} } } } 
{ /* Llib/date.scm 547 */
 long BgL_arg2800z00_3840; long BgL_arg2804z00_3841;
BgL_arg2800z00_3840 = 
(BgL_wz00_76+3L); 
BgL_arg2804z00_3841 = 
BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(BgL_rz00_3837, 10L); 
{ /* Llib/date.scm 508 */
 unsigned char BgL_arg2764z00_8347;
BgL_arg2764z00_8347 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(48L+BgL_arg2804z00_3841)); 
{ /* Llib/date.scm 508 */
 long BgL_l4862z00_9963;
BgL_l4862z00_9963 = 
STRING_LENGTH(BgL_bufz00_75); 
if(
BOUND_CHECK(BgL_arg2800z00_3840, BgL_l4862z00_9963))
{ /* Llib/date.scm 508 */
STRING_SET(BgL_bufz00_75, BgL_arg2800z00_3840, BgL_arg2764z00_8347); }  else 
{ 
 obj_t BgL_auxz00_17687;
BgL_auxz00_17687 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(23528L), BGl_string5977z00zz__datez00, BgL_bufz00_75, 
(int)(BgL_l4862z00_9963), 
(int)(BgL_arg2800z00_3840)); 
FAILURE(BgL_auxz00_17687,BFALSE,BFALSE);} } } } 
return 4L;} } } } } } 

}



/* date->utc-string */
BGL_EXPORTED_DEF obj_t BGl_datezd2ze3utczd2stringze3zz__datez00(obj_t BgL_datez00_81)
{
{ /* Llib/date.scm 561 */
{ 
 obj_t BgL_datez00_3848;
{ /* Llib/date.scm 602 */
 long BgL_tza7za7_3844;
BgL_tza7za7_3844 = 
BGL_DATE_TIMEZONE(BgL_datez00_81); 
if(
(BgL_tza7za7_3844==0L))
{ /* Llib/date.scm 603 */
BgL_datez00_3848 = BgL_datez00_81; 
BgL_zc3z04anonymousza32810ze3z87_3849:
{ /* Llib/date.scm 564 */
 obj_t BgL_bufz00_3850; long BgL_wz00_3851;
BgL_bufz00_3850 = 
make_string(29L, ((unsigned char)' ')); 
BgL_wz00_3851 = 0L; 
{ /* Llib/date.scm 575 */
 long BgL_arg2811z00_3852;
{ /* Llib/date.scm 575 */
 obj_t BgL_arg2812z00_3853;
{ /* Llib/date.scm 575 */
 int BgL_arg2815z00_3854;
BgL_arg2815z00_3854 = 
BGL_DATE_WDAY(BgL_datez00_3848); 
BgL_arg2812z00_3853 = 
BGl_dayzd2anamezd2zz__datez00(BgL_arg2815z00_3854); } 
{ /* Llib/date.scm 575 */
 long BgL_wz00_8356;
BgL_wz00_8356 = BgL_wz00_3851; 
{ /* Llib/date.scm 554 */
 long BgL_lz00_8357;
BgL_lz00_8357 = 
STRING_LENGTH(BgL_arg2812z00_3853); 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_arg2812z00_3853, 0L, BgL_bufz00_3850, BgL_wz00_8356, BgL_lz00_8357); 
BgL_arg2811z00_3852 = BgL_lz00_8357; } } } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2811z00_3852); } 
{ /* Llib/date.scm 576 */
 long BgL_kz00_8362;
BgL_kz00_8362 = BgL_wz00_3851; 
{ /* Llib/date.scm 576 */
 long BgL_l4866z00_9967;
BgL_l4866z00_9967 = 
STRING_LENGTH(BgL_bufz00_3850); 
if(
BOUND_CHECK(BgL_kz00_8362, BgL_l4866z00_9967))
{ /* Llib/date.scm 576 */
STRING_SET(BgL_bufz00_3850, BgL_kz00_8362, ((unsigned char)',')); }  else 
{ 
 obj_t BgL_auxz00_17706;
BgL_auxz00_17706 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(26294L), BGl_string5977z00zz__datez00, BgL_bufz00_3850, 
(int)(BgL_l4866z00_9967), 
(int)(BgL_kz00_8362)); 
FAILURE(BgL_auxz00_17706,BFALSE,BFALSE);} } } 
BgL_wz00_3851 = 
(BgL_wz00_3851+2L); 
{ /* Llib/date.scm 579 */
 long BgL_arg2816z00_3855;
{ /* Llib/date.scm 579 */
 int BgL_arg2818z00_3856;
BgL_arg2818z00_3856 = 
BGL_DATE_DAY(BgL_datez00_3848); 
BgL_arg2816z00_3855 = 
BGl_blitzd2intz12zc0zz__datez00(BgL_bufz00_3850, BgL_wz00_3851, BgL_arg2818z00_3856); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2816z00_3855); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+1L); 
{ /* Llib/date.scm 582 */
 long BgL_arg2820z00_3857;
{ /* Llib/date.scm 582 */
 obj_t BgL_arg2821z00_3858;
{ /* Llib/date.scm 582 */
 int BgL_arg2823z00_3859;
BgL_arg2823z00_3859 = 
BGL_DATE_MONTH(BgL_datez00_3848); 
BgL_arg2821z00_3858 = 
BGl_monthzd2anamezd2zz__datez00(BgL_arg2823z00_3859); } 
{ /* Llib/date.scm 582 */
 long BgL_wz00_8370;
BgL_wz00_8370 = BgL_wz00_3851; 
{ /* Llib/date.scm 554 */
 long BgL_lz00_8371;
BgL_lz00_8371 = 
STRING_LENGTH(BgL_arg2821z00_3858); 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_arg2821z00_3858, 0L, BgL_bufz00_3850, BgL_wz00_8370, BgL_lz00_8371); 
BgL_arg2820z00_3857 = BgL_lz00_8371; } } } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2820z00_3857); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+1L); 
{ /* Llib/date.scm 585 */
 long BgL_arg2824z00_3860;
{ /* Llib/date.scm 585 */
 int BgL_arg2826z00_3861;
BgL_arg2826z00_3861 = 
BGL_DATE_YEAR(BgL_datez00_3848); 
BgL_arg2824z00_3860 = 
BGl_blitzd2intz12zc0zz__datez00(BgL_bufz00_3850, BgL_wz00_3851, BgL_arg2826z00_3861); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2824z00_3860); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+1L); 
{ /* Llib/date.scm 588 */
 long BgL_arg2827z00_3862;
{ /* Llib/date.scm 588 */
 int BgL_arg2828z00_3863;
BgL_arg2828z00_3863 = 
BGL_DATE_HOUR(BgL_datez00_3848); 
BgL_arg2827z00_3862 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3850, BgL_wz00_3851, BgL_arg2828z00_3863); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2827z00_3862); } 
{ /* Llib/date.scm 589 */
 long BgL_kz00_8384;
BgL_kz00_8384 = BgL_wz00_3851; 
{ /* Llib/date.scm 589 */
 long BgL_l4870z00_9971;
BgL_l4870z00_9971 = 
STRING_LENGTH(BgL_bufz00_3850); 
if(
BOUND_CHECK(BgL_kz00_8384, BgL_l4870z00_9971))
{ /* Llib/date.scm 589 */
STRING_SET(BgL_bufz00_3850, BgL_kz00_8384, ((unsigned char)':')); }  else 
{ 
 obj_t BgL_auxz00_17734;
BgL_auxz00_17734 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(26696L), BGl_string5977z00zz__datez00, BgL_bufz00_3850, 
(int)(BgL_l4870z00_9971), 
(int)(BgL_kz00_8384)); 
FAILURE(BgL_auxz00_17734,BFALSE,BFALSE);} } } 
BgL_wz00_3851 = 
(BgL_wz00_3851+1L); 
{ /* Llib/date.scm 592 */
 long BgL_arg2829z00_3864;
{ /* Llib/date.scm 592 */
 int BgL_arg2830z00_3865;
BgL_arg2830z00_3865 = 
BGL_DATE_MINUTE(BgL_datez00_3848); 
BgL_arg2829z00_3864 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3850, BgL_wz00_3851, BgL_arg2830z00_3865); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2829z00_3864); } 
{ /* Llib/date.scm 593 */
 long BgL_kz00_8391;
BgL_kz00_8391 = BgL_wz00_3851; 
{ /* Llib/date.scm 593 */
 long BgL_l4874z00_9975;
BgL_l4874z00_9975 = 
STRING_LENGTH(BgL_bufz00_3850); 
if(
BOUND_CHECK(BgL_kz00_8391, BgL_l4874z00_9975))
{ /* Llib/date.scm 593 */
STRING_SET(BgL_bufz00_3850, BgL_kz00_8391, ((unsigned char)':')); }  else 
{ 
 obj_t BgL_auxz00_17748;
BgL_auxz00_17748 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(26817L), BGl_string5977z00zz__datez00, BgL_bufz00_3850, 
(int)(BgL_l4874z00_9975), 
(int)(BgL_kz00_8391)); 
FAILURE(BgL_auxz00_17748,BFALSE,BFALSE);} } } 
BgL_wz00_3851 = 
(BgL_wz00_3851+1L); 
{ /* Llib/date.scm 596 */
 long BgL_arg2831z00_3866;
{ /* Llib/date.scm 596 */
 int BgL_arg2832z00_3867;
BgL_arg2832z00_3867 = 
BGL_DATE_SECOND(BgL_datez00_3848); 
BgL_arg2831z00_3866 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3850, BgL_wz00_3851, BgL_arg2832z00_3867); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2831z00_3866); } 
BgL_wz00_3851 = 
(BgL_wz00_3851+1L); 
{ /* Llib/date.scm 599 */
 long BgL_arg2833z00_3868;
{ /* Llib/date.scm 599 */
 long BgL_wz00_8398;
BgL_wz00_8398 = BgL_wz00_3851; 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BGl_string5779z00zz__datez00, 0L, BgL_bufz00_3850, BgL_wz00_8398, 3L); 
BgL_arg2833z00_3868 = 3L; } 
BgL_wz00_3851 = 
(BgL_wz00_3851+BgL_arg2833z00_3868); } 
return 
bgl_string_shrink(BgL_bufz00_3850, BgL_wz00_3851);} }  else 
{ 
 obj_t BgL_datez00_17762;
BgL_datez00_17762 = 
bgl_seconds_to_gmtdate(
bgl_date_to_seconds(BgL_datez00_81)); 
BgL_datez00_3848 = BgL_datez00_17762; 
goto BgL_zc3z04anonymousza32810ze3z87_3849;} } } } 

}



/* &date->utc-string */
obj_t BGl_z62datezd2ze3utczd2stringz81zz__datez00(obj_t BgL_envz00_9837, obj_t BgL_datez00_9838)
{
{ /* Llib/date.scm 561 */
{ /* Llib/date.scm 564 */
 obj_t BgL_auxz00_17765;
if(
BGL_DATEP(BgL_datez00_9838))
{ /* Llib/date.scm 564 */
BgL_auxz00_17765 = BgL_datez00_9838
; }  else 
{ 
 obj_t BgL_auxz00_17768;
BgL_auxz00_17768 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(25597L), BGl_string5978z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9838); 
FAILURE(BgL_auxz00_17768,BFALSE,BFALSE);} 
return 
BGl_datezd2ze3utczd2stringze3zz__datez00(BgL_auxz00_17765);} } 

}



/* seconds->string */
BGL_EXPORTED_DEF obj_t BGl_secondszd2ze3stringz31zz__datez00(long BgL_secz00_82)
{
{ /* Llib/date.scm 610 */
BGL_TAIL return 
bgl_seconds_to_string(BgL_secz00_82);} 

}



/* &seconds->string */
obj_t BGl_z62secondszd2ze3stringz53zz__datez00(obj_t BgL_envz00_9839, obj_t BgL_secz00_9840)
{
{ /* Llib/date.scm 610 */
{ /* Llib/date.scm 611 */
 long BgL_auxz00_17774;
{ /* Llib/date.scm 611 */
 obj_t BgL_tmpz00_17775;
if(
ELONGP(BgL_secz00_9840))
{ /* Llib/date.scm 611 */
BgL_tmpz00_17775 = BgL_secz00_9840
; }  else 
{ 
 obj_t BgL_auxz00_17778;
BgL_auxz00_17778 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(27440L), BGl_string5979z00zz__datez00, BGl_string5968z00zz__datez00, BgL_secz00_9840); 
FAILURE(BgL_auxz00_17778,BFALSE,BFALSE);} 
BgL_auxz00_17774 = 
BELONG_TO_LONG(BgL_tmpz00_17775); } 
return 
BGl_secondszd2ze3stringz31zz__datez00(BgL_auxz00_17774);} } 

}



/* seconds->utc-string */
BGL_EXPORTED_DEF obj_t BGl_secondszd2ze3utczd2stringze3zz__datez00(long BgL_secz00_83)
{
{ /* Llib/date.scm 616 */
BGL_TAIL return 
bgl_seconds_to_utc_string(BgL_secz00_83);} 

}



/* &seconds->utc-string */
obj_t BGl_z62secondszd2ze3utczd2stringz81zz__datez00(obj_t BgL_envz00_9841, obj_t BgL_secz00_9842)
{
{ /* Llib/date.scm 616 */
{ /* Llib/date.scm 617 */
 long BgL_auxz00_17785;
{ /* Llib/date.scm 617 */
 obj_t BgL_tmpz00_17786;
if(
ELONGP(BgL_secz00_9842))
{ /* Llib/date.scm 617 */
BgL_tmpz00_17786 = BgL_secz00_9842
; }  else 
{ 
 obj_t BgL_auxz00_17789;
BgL_auxz00_17789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(27738L), BGl_string5980z00zz__datez00, BGl_string5968z00zz__datez00, BgL_secz00_9842); 
FAILURE(BgL_auxz00_17789,BFALSE,BFALSE);} 
BgL_auxz00_17785 = 
BELONG_TO_LONG(BgL_tmpz00_17786); } 
return 
BGl_secondszd2ze3utczd2stringze3zz__datez00(BgL_auxz00_17785);} } 

}



/* day-seconds */
BGL_EXPORTED_DEF long BGl_dayzd2secondszd2zz__datez00(void)
{
{ /* Llib/date.scm 622 */
return ((long)86400);} 

}



/* &day-seconds */
obj_t BGl_z62dayzd2secondszb0zz__datez00(obj_t BgL_envz00_9843)
{
{ /* Llib/date.scm 622 */
{ /* Llib/date.scm 622 */
 long BgL_tmpz00_17795;
BgL_tmpz00_17795 = 
BGl_dayzd2secondszd2zz__datez00(); 
return 
make_belong(BgL_tmpz00_17795);} } 

}



/* day-name */
BGL_EXPORTED_DEF obj_t BGl_dayzd2namezd2zz__datez00(int BgL_dayz00_84)
{
{ /* Llib/date.scm 628 */
if(
(
(long)(BgL_dayz00_84)<1L))
{ /* Llib/date.scm 631 */
 obj_t BgL_aux5675z00_10784;
BgL_aux5675z00_10784 = 
BGl_errorz00zz__errorz00(BGl_symbol5981z00zz__datez00, BGl_string5983z00zz__datez00, 
BINT(BgL_dayz00_84)); 
if(
STRINGP(BgL_aux5675z00_10784))
{ /* Llib/date.scm 631 */
return BgL_aux5675z00_10784;}  else 
{ 
 obj_t BgL_auxz00_17805;
BgL_auxz00_17805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(28318L), BGl_string5982z00zz__datez00, BGl_string5984z00zz__datez00, BgL_aux5675z00_10784); 
FAILURE(BgL_auxz00_17805,BFALSE,BFALSE);} }  else 
{ /* Llib/date.scm 630 */
if(
(
(long)(BgL_dayz00_84)>7L))
{ /* Llib/date.scm 633 */
 long BgL_arg2836z00_3872;
{ /* Llib/date.scm 633 */
 long BgL_arg2837z00_3873;
{ /* Llib/date.scm 633 */
 long BgL_n1z00_8411; long BgL_n2z00_8412;
BgL_n1z00_8411 = 
(long)(BgL_dayz00_84); 
BgL_n2z00_8412 = 7L; 
{ /* Llib/date.scm 633 */
 bool_t BgL_test7176z00_17813;
{ /* Llib/date.scm 633 */
 long BgL_arg4568z00_8414;
BgL_arg4568z00_8414 = 
(((BgL_n1z00_8411) | (BgL_n2z00_8412)) & -2147483648); 
BgL_test7176z00_17813 = 
(BgL_arg4568z00_8414==0L); } 
if(BgL_test7176z00_17813)
{ /* Llib/date.scm 633 */
 int32_t BgL_arg4565z00_8415;
{ /* Llib/date.scm 633 */
 int32_t BgL_arg4566z00_8416; int32_t BgL_arg4567z00_8417;
BgL_arg4566z00_8416 = 
(int32_t)(BgL_n1z00_8411); 
BgL_arg4567z00_8417 = 
(int32_t)(BgL_n2z00_8412); 
BgL_arg4565z00_8415 = 
(BgL_arg4566z00_8416%BgL_arg4567z00_8417); } 
{ /* Llib/date.scm 633 */
 long BgL_arg4669z00_8422;
BgL_arg4669z00_8422 = 
(long)(BgL_arg4565z00_8415); 
BgL_arg2837z00_3873 = 
(long)(BgL_arg4669z00_8422); } }  else 
{ /* Llib/date.scm 633 */
BgL_arg2837z00_3873 = 
(BgL_n1z00_8411%BgL_n2z00_8412); } } } 
BgL_arg2836z00_3872 = 
(1L+BgL_arg2837z00_3873); } 
BGL_TAIL return 
bgl_day_name(
(int)(BgL_arg2836z00_3872));}  else 
{ /* Llib/date.scm 632 */
BGL_TAIL return 
bgl_day_name(BgL_dayz00_84);} } } 

}



/* &day-name */
obj_t BGl_z62dayzd2namezb0zz__datez00(obj_t BgL_envz00_9844, obj_t BgL_dayz00_9845)
{
{ /* Llib/date.scm 628 */
{ /* Llib/date.scm 630 */
 int BgL_auxz00_17826;
{ /* Llib/date.scm 630 */
 obj_t BgL_tmpz00_17827;
if(
INTEGERP(BgL_dayz00_9845))
{ /* Llib/date.scm 630 */
BgL_tmpz00_17827 = BgL_dayz00_9845
; }  else 
{ 
 obj_t BgL_auxz00_17830;
BgL_auxz00_17830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(28298L), BGl_string5985z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_9845); 
FAILURE(BgL_auxz00_17830,BFALSE,BFALSE);} 
BgL_auxz00_17826 = 
CINT(BgL_tmpz00_17827); } 
return 
BGl_dayzd2namezd2zz__datez00(BgL_auxz00_17826);} } 

}



/* day-aname */
BGL_EXPORTED_DEF obj_t BGl_dayzd2anamezd2zz__datez00(int BgL_dayz00_85)
{
{ /* Llib/date.scm 640 */
if(
(
(long)(BgL_dayz00_85)<1L))
{ /* Llib/date.scm 643 */
 obj_t BgL_aux5678z00_10787;
BgL_aux5678z00_10787 = 
BGl_errorz00zz__errorz00(BGl_symbol5986z00zz__datez00, BGl_string5983z00zz__datez00, 
BINT(BgL_dayz00_85)); 
if(
STRINGP(BgL_aux5678z00_10787))
{ /* Llib/date.scm 643 */
return BgL_aux5678z00_10787;}  else 
{ 
 obj_t BgL_auxz00_17843;
BgL_auxz00_17843 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(28759L), BGl_string5987z00zz__datez00, BGl_string5984z00zz__datez00, BgL_aux5678z00_10787); 
FAILURE(BgL_auxz00_17843,BFALSE,BFALSE);} }  else 
{ /* Llib/date.scm 642 */
if(
(
(long)(BgL_dayz00_85)>7L))
{ /* Llib/date.scm 645 */
 long BgL_arg2844z00_3876;
{ /* Llib/date.scm 645 */
 long BgL_arg2846z00_3877;
{ /* Llib/date.scm 645 */
 long BgL_n1z00_8427; long BgL_n2z00_8428;
BgL_n1z00_8427 = 
(long)(BgL_dayz00_85); 
BgL_n2z00_8428 = 7L; 
{ /* Llib/date.scm 645 */
 bool_t BgL_test7181z00_17851;
{ /* Llib/date.scm 645 */
 long BgL_arg4568z00_8430;
BgL_arg4568z00_8430 = 
(((BgL_n1z00_8427) | (BgL_n2z00_8428)) & -2147483648); 
BgL_test7181z00_17851 = 
(BgL_arg4568z00_8430==0L); } 
if(BgL_test7181z00_17851)
{ /* Llib/date.scm 645 */
 int32_t BgL_arg4565z00_8431;
{ /* Llib/date.scm 645 */
 int32_t BgL_arg4566z00_8432; int32_t BgL_arg4567z00_8433;
BgL_arg4566z00_8432 = 
(int32_t)(BgL_n1z00_8427); 
BgL_arg4567z00_8433 = 
(int32_t)(BgL_n2z00_8428); 
BgL_arg4565z00_8431 = 
(BgL_arg4566z00_8432%BgL_arg4567z00_8433); } 
{ /* Llib/date.scm 645 */
 long BgL_arg4669z00_8438;
BgL_arg4669z00_8438 = 
(long)(BgL_arg4565z00_8431); 
BgL_arg2846z00_3877 = 
(long)(BgL_arg4669z00_8438); } }  else 
{ /* Llib/date.scm 645 */
BgL_arg2846z00_3877 = 
(BgL_n1z00_8427%BgL_n2z00_8428); } } } 
BgL_arg2844z00_3876 = 
(1L+BgL_arg2846z00_3877); } 
BGL_TAIL return 
bgl_day_aname(
(int)(BgL_arg2844z00_3876));}  else 
{ /* Llib/date.scm 644 */
BGL_TAIL return 
bgl_day_aname(BgL_dayz00_85);} } } 

}



/* &day-aname */
obj_t BGl_z62dayzd2anamezb0zz__datez00(obj_t BgL_envz00_9846, obj_t BgL_dayz00_9847)
{
{ /* Llib/date.scm 640 */
{ /* Llib/date.scm 642 */
 int BgL_auxz00_17864;
{ /* Llib/date.scm 642 */
 obj_t BgL_tmpz00_17865;
if(
INTEGERP(BgL_dayz00_9847))
{ /* Llib/date.scm 642 */
BgL_tmpz00_17865 = BgL_dayz00_9847
; }  else 
{ 
 obj_t BgL_auxz00_17868;
BgL_auxz00_17868 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(28739L), BGl_string5988z00zz__datez00, BGl_string5789z00zz__datez00, BgL_dayz00_9847); 
FAILURE(BgL_auxz00_17868,BFALSE,BFALSE);} 
BgL_auxz00_17864 = 
CINT(BgL_tmpz00_17865); } 
return 
BGl_dayzd2anamezd2zz__datez00(BgL_auxz00_17864);} } 

}



/* month-name */
BGL_EXPORTED_DEF obj_t BGl_monthzd2namezd2zz__datez00(int BgL_monthz00_86)
{
{ /* Llib/date.scm 652 */
if(
(
(long)(BgL_monthz00_86)<1L))
{ /* Llib/date.scm 655 */
 obj_t BgL_aux5681z00_10790;
BgL_aux5681z00_10790 = 
BGl_errorz00zz__errorz00(BGl_symbol5989z00zz__datez00, BGl_string5991z00zz__datez00, 
BINT(BgL_monthz00_86)); 
if(
STRINGP(BgL_aux5681z00_10790))
{ /* Llib/date.scm 655 */
return BgL_aux5681z00_10790;}  else 
{ 
 obj_t BgL_auxz00_17881;
BgL_auxz00_17881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(29208L), BGl_string5992z00zz__datez00, BGl_string5984z00zz__datez00, BgL_aux5681z00_10790); 
FAILURE(BgL_auxz00_17881,BFALSE,BFALSE);} }  else 
{ /* Llib/date.scm 654 */
if(
(
(long)(BgL_monthz00_86)>12L))
{ /* Llib/date.scm 657 */
 long BgL_arg2856z00_3880;
{ /* Llib/date.scm 657 */
 long BgL_arg2858z00_3881;
{ /* Llib/date.scm 657 */
 long BgL_n1z00_8443; long BgL_n2z00_8444;
BgL_n1z00_8443 = 
(long)(BgL_monthz00_86); 
BgL_n2z00_8444 = 12L; 
{ /* Llib/date.scm 657 */
 bool_t BgL_test7186z00_17889;
{ /* Llib/date.scm 657 */
 long BgL_arg4568z00_8446;
BgL_arg4568z00_8446 = 
(((BgL_n1z00_8443) | (BgL_n2z00_8444)) & -2147483648); 
BgL_test7186z00_17889 = 
(BgL_arg4568z00_8446==0L); } 
if(BgL_test7186z00_17889)
{ /* Llib/date.scm 657 */
 int32_t BgL_arg4565z00_8447;
{ /* Llib/date.scm 657 */
 int32_t BgL_arg4566z00_8448; int32_t BgL_arg4567z00_8449;
BgL_arg4566z00_8448 = 
(int32_t)(BgL_n1z00_8443); 
BgL_arg4567z00_8449 = 
(int32_t)(BgL_n2z00_8444); 
BgL_arg4565z00_8447 = 
(BgL_arg4566z00_8448%BgL_arg4567z00_8449); } 
{ /* Llib/date.scm 657 */
 long BgL_arg4669z00_8454;
BgL_arg4669z00_8454 = 
(long)(BgL_arg4565z00_8447); 
BgL_arg2858z00_3881 = 
(long)(BgL_arg4669z00_8454); } }  else 
{ /* Llib/date.scm 657 */
BgL_arg2858z00_3881 = 
(BgL_n1z00_8443%BgL_n2z00_8444); } } } 
BgL_arg2856z00_3880 = 
(1L+BgL_arg2858z00_3881); } 
BGL_TAIL return 
bgl_month_name(
(int)(BgL_arg2856z00_3880));}  else 
{ /* Llib/date.scm 656 */
BGL_TAIL return 
bgl_month_name(BgL_monthz00_86);} } } 

}



/* &month-name */
obj_t BGl_z62monthzd2namezb0zz__datez00(obj_t BgL_envz00_9848, obj_t BgL_monthz00_9849)
{
{ /* Llib/date.scm 652 */
{ /* Llib/date.scm 654 */
 int BgL_auxz00_17902;
{ /* Llib/date.scm 654 */
 obj_t BgL_tmpz00_17903;
if(
INTEGERP(BgL_monthz00_9849))
{ /* Llib/date.scm 654 */
BgL_tmpz00_17903 = BgL_monthz00_9849
; }  else 
{ 
 obj_t BgL_auxz00_17906;
BgL_auxz00_17906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(29186L), BGl_string5993z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_9849); 
FAILURE(BgL_auxz00_17906,BFALSE,BFALSE);} 
BgL_auxz00_17902 = 
CINT(BgL_tmpz00_17903); } 
return 
BGl_monthzd2namezd2zz__datez00(BgL_auxz00_17902);} } 

}



/* month-aname */
BGL_EXPORTED_DEF obj_t BGl_monthzd2anamezd2zz__datez00(int BgL_monthz00_87)
{
{ /* Llib/date.scm 664 */
if(
(
(long)(BgL_monthz00_87)<1L))
{ /* Llib/date.scm 667 */
 obj_t BgL_aux5684z00_10793;
BgL_aux5684z00_10793 = 
BGl_errorz00zz__errorz00(BGl_symbol5989z00zz__datez00, BGl_string5991z00zz__datez00, 
BINT(BgL_monthz00_87)); 
if(
STRINGP(BgL_aux5684z00_10793))
{ /* Llib/date.scm 667 */
return BgL_aux5684z00_10793;}  else 
{ 
 obj_t BgL_auxz00_17919;
BgL_auxz00_17919 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(29674L), BGl_string5990z00zz__datez00, BGl_string5984z00zz__datez00, BgL_aux5684z00_10793); 
FAILURE(BgL_auxz00_17919,BFALSE,BFALSE);} }  else 
{ /* Llib/date.scm 666 */
if(
(
(long)(BgL_monthz00_87)>12L))
{ /* Llib/date.scm 669 */
 long BgL_arg2864z00_3884;
{ /* Llib/date.scm 669 */
 long BgL_arg2870z00_3885;
{ /* Llib/date.scm 669 */
 long BgL_n1z00_8459; long BgL_n2z00_8460;
BgL_n1z00_8459 = 
(long)(BgL_monthz00_87); 
BgL_n2z00_8460 = 12L; 
{ /* Llib/date.scm 669 */
 bool_t BgL_test7191z00_17927;
{ /* Llib/date.scm 669 */
 long BgL_arg4568z00_8462;
BgL_arg4568z00_8462 = 
(((BgL_n1z00_8459) | (BgL_n2z00_8460)) & -2147483648); 
BgL_test7191z00_17927 = 
(BgL_arg4568z00_8462==0L); } 
if(BgL_test7191z00_17927)
{ /* Llib/date.scm 669 */
 int32_t BgL_arg4565z00_8463;
{ /* Llib/date.scm 669 */
 int32_t BgL_arg4566z00_8464; int32_t BgL_arg4567z00_8465;
BgL_arg4566z00_8464 = 
(int32_t)(BgL_n1z00_8459); 
BgL_arg4567z00_8465 = 
(int32_t)(BgL_n2z00_8460); 
BgL_arg4565z00_8463 = 
(BgL_arg4566z00_8464%BgL_arg4567z00_8465); } 
{ /* Llib/date.scm 669 */
 long BgL_arg4669z00_8470;
BgL_arg4669z00_8470 = 
(long)(BgL_arg4565z00_8463); 
BgL_arg2870z00_3885 = 
(long)(BgL_arg4669z00_8470); } }  else 
{ /* Llib/date.scm 669 */
BgL_arg2870z00_3885 = 
(BgL_n1z00_8459%BgL_n2z00_8460); } } } 
BgL_arg2864z00_3884 = 
(1L+BgL_arg2870z00_3885); } 
BGL_TAIL return 
bgl_month_aname(
(int)(BgL_arg2864z00_3884));}  else 
{ /* Llib/date.scm 668 */
BGL_TAIL return 
bgl_month_aname(BgL_monthz00_87);} } } 

}



/* &month-aname */
obj_t BGl_z62monthzd2anamezb0zz__datez00(obj_t BgL_envz00_9850, obj_t BgL_monthz00_9851)
{
{ /* Llib/date.scm 664 */
{ /* Llib/date.scm 666 */
 int BgL_auxz00_17940;
{ /* Llib/date.scm 666 */
 obj_t BgL_tmpz00_17941;
if(
INTEGERP(BgL_monthz00_9851))
{ /* Llib/date.scm 666 */
BgL_tmpz00_17941 = BgL_monthz00_9851
; }  else 
{ 
 obj_t BgL_auxz00_17944;
BgL_auxz00_17944 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(29652L), BGl_string5994z00zz__datez00, BGl_string5789z00zz__datez00, BgL_monthz00_9851); 
FAILURE(BgL_auxz00_17944,BFALSE,BFALSE);} 
BgL_auxz00_17940 = 
CINT(BgL_tmpz00_17941); } 
return 
BGl_monthzd2anamezd2zz__datez00(BgL_auxz00_17940);} } 

}



/* date-month-length */
BGL_EXPORTED_DEF int BGl_datezd2monthzd2lengthz00zz__datez00(obj_t BgL_dz00_88)
{
{ /* Llib/date.scm 681 */
{ /* Llib/date.scm 682 */
 int BgL_mz00_3886;
BgL_mz00_3886 = 
BGL_DATE_MONTH(BgL_dz00_88); 
if(
(
(long)(BgL_mz00_3886)==2L))
{ /* Llib/date.scm 684 */
 bool_t BgL_test7194z00_17954;
{ /* Llib/date.scm 684 */
 int BgL_arg2874z00_3890;
BgL_arg2874z00_3890 = 
BGL_DATE_YEAR(BgL_dz00_88); 
{ /* Llib/date.scm 684 */
 bool_t BgL_res4744z00_8525;
{ /* Llib/date.scm 691 */
 bool_t BgL_test7195z00_17956;
{ /* Llib/date.scm 691 */
 long BgL_arg2880z00_8478;
{ /* Llib/date.scm 691 */
 long BgL_n1z00_8483; long BgL_n2z00_8484;
BgL_n1z00_8483 = 
(long)(BgL_arg2874z00_3890); 
BgL_n2z00_8484 = 4L; 
{ /* Llib/date.scm 691 */
 bool_t BgL_test7196z00_17958;
{ /* Llib/date.scm 691 */
 long BgL_arg4568z00_8486;
BgL_arg4568z00_8486 = 
(((BgL_n1z00_8483) | (BgL_n2z00_8484)) & -2147483648); 
BgL_test7196z00_17958 = 
(BgL_arg4568z00_8486==0L); } 
if(BgL_test7196z00_17958)
{ /* Llib/date.scm 691 */
 int32_t BgL_arg4565z00_8487;
{ /* Llib/date.scm 691 */
 int32_t BgL_arg4566z00_8488; int32_t BgL_arg4567z00_8489;
BgL_arg4566z00_8488 = 
(int32_t)(BgL_n1z00_8483); 
BgL_arg4567z00_8489 = 
(int32_t)(BgL_n2z00_8484); 
BgL_arg4565z00_8487 = 
(BgL_arg4566z00_8488%BgL_arg4567z00_8489); } 
{ /* Llib/date.scm 691 */
 long BgL_arg4669z00_8494;
BgL_arg4669z00_8494 = 
(long)(BgL_arg4565z00_8487); 
BgL_arg2880z00_8478 = 
(long)(BgL_arg4669z00_8494); } }  else 
{ /* Llib/date.scm 691 */
BgL_arg2880z00_8478 = 
(BgL_n1z00_8483%BgL_n2z00_8484); } } } 
BgL_test7195z00_17956 = 
(BgL_arg2880z00_8478==0L); } 
if(BgL_test7195z00_17956)
{ /* Llib/date.scm 692 */
 bool_t BgL__ortest_1058z00_8479;
{ /* Llib/date.scm 692 */
 bool_t BgL_test7197z00_17968;
{ /* Llib/date.scm 692 */
 long BgL_arg2879z00_8481;
{ /* Llib/date.scm 692 */
 long BgL_n1z00_8497; long BgL_n2z00_8498;
BgL_n1z00_8497 = 
(long)(BgL_arg2874z00_3890); 
BgL_n2z00_8498 = 100L; 
{ /* Llib/date.scm 692 */
 bool_t BgL_test7198z00_17970;
{ /* Llib/date.scm 692 */
 long BgL_arg4568z00_8500;
BgL_arg4568z00_8500 = 
(((BgL_n1z00_8497) | (BgL_n2z00_8498)) & -2147483648); 
BgL_test7198z00_17970 = 
(BgL_arg4568z00_8500==0L); } 
if(BgL_test7198z00_17970)
{ /* Llib/date.scm 692 */
 int32_t BgL_arg4565z00_8501;
{ /* Llib/date.scm 692 */
 int32_t BgL_arg4566z00_8502; int32_t BgL_arg4567z00_8503;
BgL_arg4566z00_8502 = 
(int32_t)(BgL_n1z00_8497); 
BgL_arg4567z00_8503 = 
(int32_t)(BgL_n2z00_8498); 
BgL_arg4565z00_8501 = 
(BgL_arg4566z00_8502%BgL_arg4567z00_8503); } 
{ /* Llib/date.scm 692 */
 long BgL_arg4669z00_8508;
BgL_arg4669z00_8508 = 
(long)(BgL_arg4565z00_8501); 
BgL_arg2879z00_8481 = 
(long)(BgL_arg4669z00_8508); } }  else 
{ /* Llib/date.scm 692 */
BgL_arg2879z00_8481 = 
(BgL_n1z00_8497%BgL_n2z00_8498); } } } 
BgL_test7197z00_17968 = 
(BgL_arg2879z00_8481==0L); } 
if(BgL_test7197z00_17968)
{ /* Llib/date.scm 692 */
BgL__ortest_1058z00_8479 = ((bool_t)0); }  else 
{ /* Llib/date.scm 692 */
BgL__ortest_1058z00_8479 = ((bool_t)1); } } 
if(BgL__ortest_1058z00_8479)
{ /* Llib/date.scm 692 */
BgL_res4744z00_8525 = BgL__ortest_1058z00_8479; }  else 
{ /* Llib/date.scm 693 */
 long BgL_arg2876z00_8482;
{ /* Llib/date.scm 693 */
 long BgL_n1z00_8511; long BgL_n2z00_8512;
BgL_n1z00_8511 = 
(long)(BgL_arg2874z00_3890); 
BgL_n2z00_8512 = 400L; 
{ /* Llib/date.scm 693 */
 bool_t BgL_test7200z00_17982;
{ /* Llib/date.scm 693 */
 long BgL_arg4568z00_8514;
BgL_arg4568z00_8514 = 
(((BgL_n1z00_8511) | (BgL_n2z00_8512)) & -2147483648); 
BgL_test7200z00_17982 = 
(BgL_arg4568z00_8514==0L); } 
if(BgL_test7200z00_17982)
{ /* Llib/date.scm 693 */
 int32_t BgL_arg4565z00_8515;
{ /* Llib/date.scm 693 */
 int32_t BgL_arg4566z00_8516; int32_t BgL_arg4567z00_8517;
BgL_arg4566z00_8516 = 
(int32_t)(BgL_n1z00_8511); 
BgL_arg4567z00_8517 = 
(int32_t)(BgL_n2z00_8512); 
BgL_arg4565z00_8515 = 
(BgL_arg4566z00_8516%BgL_arg4567z00_8517); } 
{ /* Llib/date.scm 693 */
 long BgL_arg4669z00_8522;
BgL_arg4669z00_8522 = 
(long)(BgL_arg4565z00_8515); 
BgL_arg2876z00_8482 = 
(long)(BgL_arg4669z00_8522); } }  else 
{ /* Llib/date.scm 693 */
BgL_arg2876z00_8482 = 
(BgL_n1z00_8511%BgL_n2z00_8512); } } } 
BgL_res4744z00_8525 = 
(BgL_arg2876z00_8482==0L); } }  else 
{ /* Llib/date.scm 691 */
BgL_res4744z00_8525 = ((bool_t)0); } } 
BgL_test7194z00_17954 = BgL_res4744z00_8525; } } 
if(BgL_test7194z00_17954)
{ /* Llib/date.scm 684 */
return 
(int)(29L);}  else 
{ /* Llib/date.scm 684 */
return 
(int)(28L);} }  else 
{ /* Llib/date.scm 685 */
 long BgL_arg2875z00_3891;
BgL_arg2875z00_3891 = 
(
(long)(BgL_mz00_3886)-1L); 
{ /* Llib/date.scm 685 */
 obj_t BgL_vectorz00_8527;
BgL_vectorz00_8527 = BGl_za2monthzd2lengthsza2zd2zz__datez00; 
{ /* Llib/date.scm 685 */
 bool_t BgL_test7201z00_17996;
{ /* Llib/date.scm 685 */
 long BgL_tmpz00_17997;
BgL_tmpz00_17997 = 
VECTOR_LENGTH(BgL_vectorz00_8527); 
BgL_test7201z00_17996 = 
BOUND_CHECK(BgL_arg2875z00_3891, BgL_tmpz00_17997); } 
if(BgL_test7201z00_17996)
{ /* Llib/date.scm 685 */
 obj_t BgL_tmpz00_18000;
{ /* Llib/date.scm 685 */
 obj_t BgL_aux5687z00_10796;
BgL_aux5687z00_10796 = 
VECTOR_REF(BgL_vectorz00_8527,BgL_arg2875z00_3891); 
if(
INTEGERP(BgL_aux5687z00_10796))
{ /* Llib/date.scm 685 */
BgL_tmpz00_18000 = BgL_aux5687z00_10796
; }  else 
{ 
 obj_t BgL_auxz00_18004;
BgL_auxz00_18004 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(30488L), BGl_string5995z00zz__datez00, BGl_string5789z00zz__datez00, BgL_aux5687z00_10796); 
FAILURE(BgL_auxz00_18004,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_18000);}  else 
{ 
 obj_t BgL_auxz00_18009;
BgL_auxz00_18009 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(30488L), BGl_string5908z00zz__datez00, BgL_vectorz00_8527, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_8527)), 
(int)(BgL_arg2875z00_3891)); 
FAILURE(BgL_auxz00_18009,BFALSE,BFALSE);} } } } } } 

}



/* &date-month-length */
obj_t BGl_z62datezd2monthzd2lengthz62zz__datez00(obj_t BgL_envz00_9852, obj_t BgL_dz00_9853)
{
{ /* Llib/date.scm 681 */
{ /* Llib/date.scm 682 */
 int BgL_tmpz00_18016;
{ /* Llib/date.scm 682 */
 obj_t BgL_auxz00_18017;
if(
BGL_DATEP(BgL_dz00_9853))
{ /* Llib/date.scm 682 */
BgL_auxz00_18017 = BgL_dz00_9853
; }  else 
{ 
 obj_t BgL_auxz00_18020;
BgL_auxz00_18020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(30398L), BGl_string5996z00zz__datez00, BGl_string5940z00zz__datez00, BgL_dz00_9853); 
FAILURE(BgL_auxz00_18020,BFALSE,BFALSE);} 
BgL_tmpz00_18016 = 
BGl_datezd2monthzd2lengthz00zz__datez00(BgL_auxz00_18017); } 
return 
BINT(BgL_tmpz00_18016);} } 

}



/* leap-year? */
BGL_EXPORTED_DEF bool_t BGl_leapzd2yearzf3z21zz__datez00(int BgL_yearz00_89)
{
{ /* Llib/date.scm 690 */
{ /* Llib/date.scm 691 */
 bool_t BgL_test7204z00_18026;
{ /* Llib/date.scm 691 */
 long BgL_arg2880z00_11819;
{ /* Llib/date.scm 691 */
 long BgL_n1z00_11820; long BgL_n2z00_11821;
BgL_n1z00_11820 = 
(long)(BgL_yearz00_89); 
BgL_n2z00_11821 = 4L; 
{ /* Llib/date.scm 691 */
 bool_t BgL_test7205z00_18028;
{ /* Llib/date.scm 691 */
 long BgL_arg4568z00_11822;
BgL_arg4568z00_11822 = 
(((BgL_n1z00_11820) | (BgL_n2z00_11821)) & -2147483648); 
BgL_test7205z00_18028 = 
(BgL_arg4568z00_11822==0L); } 
if(BgL_test7205z00_18028)
{ /* Llib/date.scm 691 */
 int32_t BgL_arg4565z00_11823;
{ /* Llib/date.scm 691 */
 int32_t BgL_arg4566z00_11824; int32_t BgL_arg4567z00_11825;
BgL_arg4566z00_11824 = 
(int32_t)(BgL_n1z00_11820); 
BgL_arg4567z00_11825 = 
(int32_t)(BgL_n2z00_11821); 
BgL_arg4565z00_11823 = 
(BgL_arg4566z00_11824%BgL_arg4567z00_11825); } 
{ /* Llib/date.scm 691 */
 long BgL_arg4669z00_11826;
BgL_arg4669z00_11826 = 
(long)(BgL_arg4565z00_11823); 
BgL_arg2880z00_11819 = 
(long)(BgL_arg4669z00_11826); } }  else 
{ /* Llib/date.scm 691 */
BgL_arg2880z00_11819 = 
(BgL_n1z00_11820%BgL_n2z00_11821); } } } 
BgL_test7204z00_18026 = 
(BgL_arg2880z00_11819==0L); } 
if(BgL_test7204z00_18026)
{ /* Llib/date.scm 692 */
 bool_t BgL__ortest_1058z00_11827;
{ /* Llib/date.scm 692 */
 bool_t BgL_test7206z00_18038;
{ /* Llib/date.scm 692 */
 long BgL_arg2879z00_11828;
{ /* Llib/date.scm 692 */
 long BgL_n1z00_11829; long BgL_n2z00_11830;
BgL_n1z00_11829 = 
(long)(BgL_yearz00_89); 
BgL_n2z00_11830 = 100L; 
{ /* Llib/date.scm 692 */
 bool_t BgL_test7207z00_18040;
{ /* Llib/date.scm 692 */
 long BgL_arg4568z00_11831;
BgL_arg4568z00_11831 = 
(((BgL_n1z00_11829) | (BgL_n2z00_11830)) & -2147483648); 
BgL_test7207z00_18040 = 
(BgL_arg4568z00_11831==0L); } 
if(BgL_test7207z00_18040)
{ /* Llib/date.scm 692 */
 int32_t BgL_arg4565z00_11832;
{ /* Llib/date.scm 692 */
 int32_t BgL_arg4566z00_11833; int32_t BgL_arg4567z00_11834;
BgL_arg4566z00_11833 = 
(int32_t)(BgL_n1z00_11829); 
BgL_arg4567z00_11834 = 
(int32_t)(BgL_n2z00_11830); 
BgL_arg4565z00_11832 = 
(BgL_arg4566z00_11833%BgL_arg4567z00_11834); } 
{ /* Llib/date.scm 692 */
 long BgL_arg4669z00_11835;
BgL_arg4669z00_11835 = 
(long)(BgL_arg4565z00_11832); 
BgL_arg2879z00_11828 = 
(long)(BgL_arg4669z00_11835); } }  else 
{ /* Llib/date.scm 692 */
BgL_arg2879z00_11828 = 
(BgL_n1z00_11829%BgL_n2z00_11830); } } } 
BgL_test7206z00_18038 = 
(BgL_arg2879z00_11828==0L); } 
if(BgL_test7206z00_18038)
{ /* Llib/date.scm 692 */
BgL__ortest_1058z00_11827 = ((bool_t)0); }  else 
{ /* Llib/date.scm 692 */
BgL__ortest_1058z00_11827 = ((bool_t)1); } } 
if(BgL__ortest_1058z00_11827)
{ /* Llib/date.scm 692 */
return BgL__ortest_1058z00_11827;}  else 
{ /* Llib/date.scm 693 */
 long BgL_arg2876z00_11836;
{ /* Llib/date.scm 693 */
 long BgL_n1z00_11837; long BgL_n2z00_11838;
BgL_n1z00_11837 = 
(long)(BgL_yearz00_89); 
BgL_n2z00_11838 = 400L; 
{ /* Llib/date.scm 693 */
 bool_t BgL_test7209z00_18052;
{ /* Llib/date.scm 693 */
 long BgL_arg4568z00_11839;
BgL_arg4568z00_11839 = 
(((BgL_n1z00_11837) | (BgL_n2z00_11838)) & -2147483648); 
BgL_test7209z00_18052 = 
(BgL_arg4568z00_11839==0L); } 
if(BgL_test7209z00_18052)
{ /* Llib/date.scm 693 */
 int32_t BgL_arg4565z00_11840;
{ /* Llib/date.scm 693 */
 int32_t BgL_arg4566z00_11841; int32_t BgL_arg4567z00_11842;
BgL_arg4566z00_11841 = 
(int32_t)(BgL_n1z00_11837); 
BgL_arg4567z00_11842 = 
(int32_t)(BgL_n2z00_11838); 
BgL_arg4565z00_11840 = 
(BgL_arg4566z00_11841%BgL_arg4567z00_11842); } 
{ /* Llib/date.scm 693 */
 long BgL_arg4669z00_11843;
BgL_arg4669z00_11843 = 
(long)(BgL_arg4565z00_11840); 
BgL_arg2876z00_11836 = 
(long)(BgL_arg4669z00_11843); } }  else 
{ /* Llib/date.scm 693 */
BgL_arg2876z00_11836 = 
(BgL_n1z00_11837%BgL_n2z00_11838); } } } 
return 
(BgL_arg2876z00_11836==0L);} }  else 
{ /* Llib/date.scm 691 */
return ((bool_t)0);} } } 

}



/* &leap-year? */
obj_t BGl_z62leapzd2yearzf3z43zz__datez00(obj_t BgL_envz00_9854, obj_t BgL_yearz00_9855)
{
{ /* Llib/date.scm 690 */
{ /* Llib/date.scm 691 */
 bool_t BgL_tmpz00_18062;
{ /* Llib/date.scm 691 */
 int BgL_auxz00_18063;
{ /* Llib/date.scm 691 */
 obj_t BgL_tmpz00_18064;
if(
INTEGERP(BgL_yearz00_9855))
{ /* Llib/date.scm 691 */
BgL_tmpz00_18064 = BgL_yearz00_9855
; }  else 
{ 
 obj_t BgL_auxz00_18067;
BgL_auxz00_18067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(30794L), BGl_string5997z00zz__datez00, BGl_string5789z00zz__datez00, BgL_yearz00_9855); 
FAILURE(BgL_auxz00_18067,BFALSE,BFALSE);} 
BgL_auxz00_18063 = 
CINT(BgL_tmpz00_18064); } 
BgL_tmpz00_18062 = 
BGl_leapzd2yearzf3z21zz__datez00(BgL_auxz00_18063); } 
return 
BBOOL(BgL_tmpz00_18062);} } 

}



/* rfc2822-date->date */
BGL_EXPORTED_DEF obj_t BGl_rfc2822zd2datezd2ze3dateze3zz__datez00(obj_t BgL_stringz00_90)
{
{ /* Llib/date.scm 698 */
{ /* Llib/date.scm 699 */
 obj_t BgL_portz00_3899;
{ /* Ieee/port.scm 469 */
 long BgL_endz00_3903;
BgL_endz00_3903 = 
STRING_LENGTH(BgL_stringz00_90); 
{ /* Ieee/port.scm 469 */

BgL_portz00_3899 = 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_stringz00_90, 
BINT(0L), 
BINT(BgL_endz00_3903)); } } 
{ /* Llib/date.scm 700 */
 obj_t BgL_dz00_3900;
{ /* Llib/date.scm 700 */
 obj_t BgL_res4745z00_8579;
{ /* Llib/date.scm 708 */
 obj_t BgL_aux5691z00_10800;
BgL_aux5691z00_10800 = 
BGl_z62zc3z04anonymousza31451ze3ze5zz__datez00(BGl_dayzd2ofzd2weekzd2grammarzd2zz__datez00, BgL_portz00_3899); 
if(
BGL_DATEP(BgL_aux5691z00_10800))
{ /* Llib/date.scm 708 */
BgL_res4745z00_8579 = BgL_aux5691z00_10800; }  else 
{ 
 obj_t BgL_auxz00_18084;
BgL_auxz00_18084 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(31553L), BGl_string5998z00zz__datez00, BGl_string5940z00zz__datez00, BgL_aux5691z00_10800); 
FAILURE(BgL_auxz00_18084,BFALSE,BFALSE);} } 
BgL_dz00_3900 = BgL_res4745z00_8579; } 
bgl_close_input_port(BgL_portz00_3899); 
return BgL_dz00_3900;} } } 

}



/* &rfc2822-date->date */
obj_t BGl_z62rfc2822zd2datezd2ze3datez81zz__datez00(obj_t BgL_envz00_9856, obj_t BgL_stringz00_9857)
{
{ /* Llib/date.scm 698 */
{ /* Llib/date.scm 699 */
 obj_t BgL_auxz00_18089;
if(
STRINGP(BgL_stringz00_9857))
{ /* Llib/date.scm 699 */
BgL_auxz00_18089 = BgL_stringz00_9857
; }  else 
{ 
 obj_t BgL_auxz00_18092;
BgL_auxz00_18092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(31166L), BGl_string5999z00zz__datez00, BGl_string5984z00zz__datez00, BgL_stringz00_9857); 
FAILURE(BgL_auxz00_18092,BFALSE,BFALSE);} 
return 
BGl_rfc2822zd2datezd2ze3dateze3zz__datez00(BgL_auxz00_18089);} } 

}



/* rfc2822-parse-date */
BGL_EXPORTED_DEF obj_t BGl_rfc2822zd2parsezd2datez00zz__datez00(obj_t BgL_ipz00_91)
{
{ /* Llib/date.scm 707 */
{ /* Llib/date.scm 708 */
 obj_t BgL_aux5695z00_10804;
BgL_aux5695z00_10804 = 
BGl_z62zc3z04anonymousza31451ze3ze5zz__datez00(BGl_dayzd2ofzd2weekzd2grammarzd2zz__datez00, BgL_ipz00_91); 
if(
BGL_DATEP(BgL_aux5695z00_10804))
{ /* Llib/date.scm 708 */
return BgL_aux5695z00_10804;}  else 
{ 
 obj_t BgL_auxz00_18103;
BgL_auxz00_18103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(31553L), BGl_string5787z00zz__datez00, BGl_string5940z00zz__datez00, BgL_aux5695z00_10804); 
FAILURE(BgL_auxz00_18103,BFALSE,BFALSE);} } } 

}



/* &rfc2822-parse-date */
obj_t BGl_z62rfc2822zd2parsezd2datez62zz__datez00(obj_t BgL_envz00_9858, obj_t BgL_ipz00_9859)
{
{ /* Llib/date.scm 707 */
{ /* Llib/date.scm 708 */
 obj_t BgL_auxz00_18107;
if(
INPUT_PORTP(BgL_ipz00_9859))
{ /* Llib/date.scm 708 */
BgL_auxz00_18107 = BgL_ipz00_9859
; }  else 
{ 
 obj_t BgL_auxz00_18110;
BgL_auxz00_18110 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(31553L), BGl_string6000z00zz__datez00, BGl_string5786z00zz__datez00, BgL_ipz00_9859); 
FAILURE(BgL_auxz00_18110,BFALSE,BFALSE);} 
return 
BGl_rfc2822zd2parsezd2datez00zz__datez00(BgL_auxz00_18107);} } 

}



/* date->rfc2822-date */
BGL_EXPORTED_DEF obj_t BGl_datezd2ze3rfc2822zd2dateze3zz__datez00(obj_t BgL_datez00_92)
{
{ /* Llib/date.scm 713 */
{ 
 obj_t BgL_datez00_3907; long BgL_tza7za7_3908;
{ /* Llib/date.scm 762 */
 long BgL_tza7za7_3905;
BgL_tza7za7_3905 = 
BGL_DATE_TIMEZONE(BgL_datez00_92); 
if(
(BgL_tza7za7_3905==0L))
{ /* Llib/date.scm 763 */
BGL_TAIL return 
BGl_datezd2ze3utczd2stringze3zz__datez00(BgL_datez00_92);}  else 
{ /* Llib/date.scm 763 */
BgL_datez00_3907 = BgL_datez00_92; 
BgL_tza7za7_3908 = BgL_tza7za7_3905; 
{ /* Llib/date.scm 716 */
 obj_t BgL_bufz00_3910; long BgL_wz00_3911;
BgL_bufz00_3910 = 
make_string(32L, ((unsigned char)' ')); 
BgL_wz00_3911 = 0L; 
{ /* Llib/date.scm 730 */
 long BgL_arg2883z00_3912;
{ /* Llib/date.scm 730 */
 obj_t BgL_arg2887z00_3913;
{ /* Llib/date.scm 730 */
 int BgL_arg2888z00_3914;
BgL_arg2888z00_3914 = 
BGL_DATE_WDAY(BgL_datez00_3907); 
BgL_arg2887z00_3913 = 
BGl_dayzd2anamezd2zz__datez00(BgL_arg2888z00_3914); } 
{ /* Llib/date.scm 730 */
 long BgL_wz00_8582;
BgL_wz00_8582 = BgL_wz00_3911; 
{ /* Llib/date.scm 554 */
 long BgL_lz00_8583;
BgL_lz00_8583 = 
STRING_LENGTH(BgL_arg2887z00_3913); 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_arg2887z00_3913, 0L, BgL_bufz00_3910, BgL_wz00_8582, BgL_lz00_8583); 
BgL_arg2883z00_3912 = BgL_lz00_8583; } } } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2883z00_3912); } 
{ /* Llib/date.scm 731 */
 long BgL_kz00_8588;
BgL_kz00_8588 = BgL_wz00_3911; 
{ /* Llib/date.scm 731 */
 long BgL_l4882z00_9983;
BgL_l4882z00_9983 = 
STRING_LENGTH(BgL_bufz00_3910); 
if(
BOUND_CHECK(BgL_kz00_8588, BgL_l4882z00_9983))
{ /* Llib/date.scm 731 */
STRING_SET(BgL_bufz00_3910, BgL_kz00_8588, ((unsigned char)',')); }  else 
{ 
 obj_t BgL_auxz00_18129;
BgL_auxz00_18129 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(32789L), BGl_string5977z00zz__datez00, BgL_bufz00_3910, 
(int)(BgL_l4882z00_9983), 
(int)(BgL_kz00_8588)); 
FAILURE(BgL_auxz00_18129,BFALSE,BFALSE);} } } 
BgL_wz00_3911 = 
(BgL_wz00_3911+2L); 
{ /* Llib/date.scm 734 */
 long BgL_arg2889z00_3915;
{ /* Llib/date.scm 734 */
 int BgL_arg2890z00_3916;
BgL_arg2890z00_3916 = 
BGL_DATE_DAY(BgL_datez00_3907); 
BgL_arg2889z00_3915 = 
BGl_blitzd2intz12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, BgL_arg2890z00_3916); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2889z00_3915); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 737 */
 long BgL_arg2891z00_3917;
{ /* Llib/date.scm 737 */
 obj_t BgL_arg2892z00_3918;
{ /* Llib/date.scm 737 */
 int BgL_arg2893z00_3919;
BgL_arg2893z00_3919 = 
BGL_DATE_MONTH(BgL_datez00_3907); 
BgL_arg2892z00_3918 = 
BGl_monthzd2anamezd2zz__datez00(BgL_arg2893z00_3919); } 
{ /* Llib/date.scm 737 */
 long BgL_wz00_8596;
BgL_wz00_8596 = BgL_wz00_3911; 
{ /* Llib/date.scm 554 */
 long BgL_lz00_8597;
BgL_lz00_8597 = 
STRING_LENGTH(BgL_arg2892z00_3918); 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BgL_arg2892z00_3918, 0L, BgL_bufz00_3910, BgL_wz00_8596, BgL_lz00_8597); 
BgL_arg2891z00_3917 = BgL_lz00_8597; } } } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2891z00_3917); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 740 */
 long BgL_arg2894z00_3920;
{ /* Llib/date.scm 740 */
 int BgL_arg2895z00_3921;
BgL_arg2895z00_3921 = 
BGL_DATE_YEAR(BgL_datez00_3907); 
BgL_arg2894z00_3920 = 
BGl_blitzd2intz12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, BgL_arg2895z00_3921); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2894z00_3920); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 743 */
 long BgL_arg2896z00_3922;
{ /* Llib/date.scm 743 */
 int BgL_arg2897z00_3923;
BgL_arg2897z00_3923 = 
BGL_DATE_HOUR(BgL_datez00_3907); 
BgL_arg2896z00_3922 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, BgL_arg2897z00_3923); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2896z00_3922); } 
{ /* Llib/date.scm 744 */
 long BgL_kz00_8610;
BgL_kz00_8610 = BgL_wz00_3911; 
{ /* Llib/date.scm 744 */
 long BgL_l4886z00_9987;
BgL_l4886z00_9987 = 
STRING_LENGTH(BgL_bufz00_3910); 
if(
BOUND_CHECK(BgL_kz00_8610, BgL_l4886z00_9987))
{ /* Llib/date.scm 744 */
STRING_SET(BgL_bufz00_3910, BgL_kz00_8610, ((unsigned char)':')); }  else 
{ 
 obj_t BgL_auxz00_18157;
BgL_auxz00_18157 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(33191L), BGl_string5977z00zz__datez00, BgL_bufz00_3910, 
(int)(BgL_l4886z00_9987), 
(int)(BgL_kz00_8610)); 
FAILURE(BgL_auxz00_18157,BFALSE,BFALSE);} } } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 747 */
 long BgL_arg2898z00_3924;
{ /* Llib/date.scm 747 */
 int BgL_arg2899z00_3925;
BgL_arg2899z00_3925 = 
BGL_DATE_MINUTE(BgL_datez00_3907); 
BgL_arg2898z00_3924 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, BgL_arg2899z00_3925); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2898z00_3924); } 
{ /* Llib/date.scm 748 */
 long BgL_kz00_8617;
BgL_kz00_8617 = BgL_wz00_3911; 
{ /* Llib/date.scm 748 */
 long BgL_l4891z00_9991;
BgL_l4891z00_9991 = 
STRING_LENGTH(BgL_bufz00_3910); 
if(
BOUND_CHECK(BgL_kz00_8617, BgL_l4891z00_9991))
{ /* Llib/date.scm 748 */
STRING_SET(BgL_bufz00_3910, BgL_kz00_8617, ((unsigned char)':')); }  else 
{ 
 obj_t BgL_auxz00_18171;
BgL_auxz00_18171 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(33312L), BGl_string5977z00zz__datez00, BgL_bufz00_3910, 
(int)(BgL_l4891z00_9991), 
(int)(BgL_kz00_8617)); 
FAILURE(BgL_auxz00_18171,BFALSE,BFALSE);} } } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 751 */
 long BgL_arg2903z00_3926;
{ /* Llib/date.scm 751 */
 int BgL_arg2904z00_3927;
BgL_arg2904z00_3927 = 
BGL_DATE_SECOND(BgL_datez00_3907); 
BgL_arg2903z00_3926 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, BgL_arg2904z00_3927); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2903z00_3926); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 754 */
 unsigned char BgL_arg2905z00_3928;
if(
(BgL_tza7za7_3908<0L))
{ /* Llib/date.scm 754 */
BgL_arg2905z00_3928 = ((unsigned char)'-'); }  else 
{ /* Llib/date.scm 754 */
BgL_arg2905z00_3928 = ((unsigned char)'+'); } 
{ /* Llib/date.scm 754 */
 long BgL_kz00_8626;
BgL_kz00_8626 = BgL_wz00_3911; 
{ /* Llib/date.scm 754 */
 long BgL_l4895z00_9995;
BgL_l4895z00_9995 = 
STRING_LENGTH(BgL_bufz00_3910); 
if(
BOUND_CHECK(BgL_kz00_8626, BgL_l4895z00_9995))
{ /* Llib/date.scm 754 */
STRING_SET(BgL_bufz00_3910, BgL_kz00_8626, BgL_arg2905z00_3928); }  else 
{ 
 obj_t BgL_auxz00_18188;
BgL_auxz00_18188 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(33463L), BGl_string5977z00zz__datez00, BgL_bufz00_3910, 
(int)(BgL_l4895z00_9995), 
(int)(BgL_kz00_8626)); 
FAILURE(BgL_auxz00_18188,BFALSE,BFALSE);} } } } 
BgL_wz00_3911 = 
(BgL_wz00_3911+1L); 
{ /* Llib/date.scm 757 */
 long BgL_arg2907z00_3930;
{ /* Llib/date.scm 757 */
 long BgL_arg2908z00_3931;
BgL_arg2908z00_3931 = 
(BgL_tza7za7_3908/3600L); 
BgL_arg2907z00_3930 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, 
(int)(BgL_arg2908z00_3931)); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2907z00_3930); } 
{ /* Llib/date.scm 759 */
 long BgL_arg2912z00_3932;
{ /* Llib/date.scm 759 */
 long BgL_arg2913z00_3933;
{ /* Llib/date.scm 759 */
 long BgL_n1z00_8632; long BgL_n2z00_8633;
BgL_n1z00_8632 = BgL_tza7za7_3908; 
BgL_n2z00_8633 = 3600L; 
{ /* Llib/date.scm 759 */
 bool_t BgL_test7221z00_18199;
{ /* Llib/date.scm 759 */
 long BgL_arg4568z00_8635;
BgL_arg4568z00_8635 = 
(((BgL_n1z00_8632) | (BgL_n2z00_8633)) & -2147483648); 
BgL_test7221z00_18199 = 
(BgL_arg4568z00_8635==0L); } 
if(BgL_test7221z00_18199)
{ /* Llib/date.scm 759 */
 int32_t BgL_arg4565z00_8636;
{ /* Llib/date.scm 759 */
 int32_t BgL_arg4566z00_8637; int32_t BgL_arg4567z00_8638;
BgL_arg4566z00_8637 = 
(int32_t)(BgL_n1z00_8632); 
BgL_arg4567z00_8638 = 
(int32_t)(BgL_n2z00_8633); 
BgL_arg4565z00_8636 = 
(BgL_arg4566z00_8637%BgL_arg4567z00_8638); } 
{ /* Llib/date.scm 759 */
 long BgL_arg4669z00_8643;
BgL_arg4669z00_8643 = 
(long)(BgL_arg4565z00_8636); 
BgL_arg2913z00_3933 = 
(long)(BgL_arg4669z00_8643); } }  else 
{ /* Llib/date.scm 759 */
BgL_arg2913z00_3933 = 
(BgL_n1z00_8632%BgL_n2z00_8633); } } } 
BgL_arg2912z00_3932 = 
BGl_blitzd2int2z12zc0zz__datez00(BgL_bufz00_3910, BgL_wz00_3911, 
(int)(BgL_arg2913z00_3933)); } 
BgL_wz00_3911 = 
(BgL_wz00_3911+BgL_arg2912z00_3932); } 
return 
bgl_string_shrink(BgL_bufz00_3910, BgL_wz00_3911);} } } } } 

}



/* &date->rfc2822-date */
obj_t BGl_z62datezd2ze3rfc2822zd2datez81zz__datez00(obj_t BgL_envz00_9860, obj_t BgL_datez00_9861)
{
{ /* Llib/date.scm 713 */
{ /* Llib/date.scm 716 */
 obj_t BgL_auxz00_18212;
if(
BGL_DATEP(BgL_datez00_9861))
{ /* Llib/date.scm 716 */
BgL_auxz00_18212 = BgL_datez00_9861
; }  else 
{ 
 obj_t BgL_auxz00_18215;
BgL_auxz00_18215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(31886L), BGl_string6001z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9861); 
FAILURE(BgL_auxz00_18215,BFALSE,BFALSE);} 
return 
BGl_datezd2ze3rfc2822zd2dateze3zz__datez00(BgL_auxz00_18212);} } 

}



/* date->iso8601-date */
BGL_EXPORTED_DEF obj_t BGl_datezd2ze3iso8601zd2dateze3zz__datez00(obj_t BgL_datez00_93)
{
{ /* Llib/date.scm 770 */
{ /* Llib/date.scm 771 */
 long BgL_tza7za7_3935;
BgL_tza7za7_3935 = 
BGL_DATE_TIMEZONE(BgL_datez00_93); 
if(
(BgL_tza7za7_3935==0L))
{ /* Llib/date.scm 774 */
 int BgL_arg2915z00_3937; int BgL_arg2916z00_3938; int BgL_arg2917z00_3939; int BgL_arg2918z00_3940; int BgL_arg2919z00_3941; int BgL_arg2920z00_3942;
BgL_arg2915z00_3937 = 
BGL_DATE_YEAR(BgL_datez00_93); 
BgL_arg2916z00_3938 = 
BGL_DATE_MONTH(BgL_datez00_93); 
BgL_arg2917z00_3939 = 
BGL_DATE_DAY(BgL_datez00_93); 
BgL_arg2918z00_3940 = 
BGL_DATE_HOUR(BgL_datez00_93); 
BgL_arg2919z00_3941 = 
BGL_DATE_MINUTE(BgL_datez00_93); 
BgL_arg2920z00_3942 = 
BGL_DATE_SECOND(BgL_datez00_93); 
{ /* Llib/date.scm 773 */
 obj_t BgL_list2921z00_3943;
{ /* Llib/date.scm 773 */
 obj_t BgL_arg2923z00_3944;
{ /* Llib/date.scm 773 */
 obj_t BgL_arg2924z00_3945;
{ /* Llib/date.scm 773 */
 obj_t BgL_arg2925z00_3946;
{ /* Llib/date.scm 773 */
 obj_t BgL_arg2926z00_3947;
{ /* Llib/date.scm 773 */
 obj_t BgL_arg2927z00_3948;
BgL_arg2927z00_3948 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2920z00_3942), BNIL); 
BgL_arg2926z00_3947 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2919z00_3941), BgL_arg2927z00_3948); } 
BgL_arg2925z00_3946 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2918z00_3940), BgL_arg2926z00_3947); } 
BgL_arg2924z00_3945 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2917z00_3939), BgL_arg2925z00_3946); } 
BgL_arg2923z00_3944 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2916z00_3938), BgL_arg2924z00_3945); } 
BgL_list2921z00_3943 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2915z00_3937), BgL_arg2923z00_3944); } 
return 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string6002z00zz__datez00, BgL_list2921z00_3943);} }  else 
{ /* Llib/date.scm 781 */
 int BgL_arg2928z00_3949; int BgL_arg2929z00_3950; int BgL_arg2930z00_3951; int BgL_arg2931z00_3952; int BgL_arg2932z00_3953; int BgL_arg2933z00_3954; obj_t BgL_arg2934z00_3955; long BgL_arg2935z00_3956; long BgL_arg2936z00_3957;
BgL_arg2928z00_3949 = 
BGL_DATE_YEAR(BgL_datez00_93); 
BgL_arg2929z00_3950 = 
BGL_DATE_MONTH(BgL_datez00_93); 
BgL_arg2930z00_3951 = 
BGL_DATE_DAY(BgL_datez00_93); 
BgL_arg2931z00_3952 = 
BGL_DATE_HOUR(BgL_datez00_93); 
BgL_arg2932z00_3953 = 
BGL_DATE_MINUTE(BgL_datez00_93); 
BgL_arg2933z00_3954 = 
BGL_DATE_SECOND(BgL_datez00_93); 
if(
(BgL_tza7za7_3935<0L))
{ /* Llib/date.scm 787 */
BgL_arg2934z00_3955 = BGl_string6003z00zz__datez00; }  else 
{ /* Llib/date.scm 787 */
BgL_arg2934z00_3955 = BGl_string6004z00zz__datez00; } 
{ /* Llib/date.scm 788 */
 long BgL_nz00_8667;
BgL_nz00_8667 = 
(BgL_tza7za7_3935/3600L); 
if(
(BgL_nz00_8667<0L))
{ /* Llib/date.scm 788 */
BgL_arg2935z00_3956 = 
NEG(BgL_nz00_8667); }  else 
{ /* Llib/date.scm 788 */
BgL_arg2935z00_3956 = BgL_nz00_8667; } } 
{ /* Llib/date.scm 789 */
 obj_t BgL_arg2972z00_3969;
BgL_arg2972z00_3969 = 
BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(
BINT(BgL_tza7za7_3935), 
BINT(3600L)); 
{ /* Llib/date.scm 789 */
 long BgL_nz00_8671;
{ /* Llib/date.scm 789 */
 obj_t BgL_tmpz00_18257;
if(
INTEGERP(BgL_arg2972z00_3969))
{ /* Llib/date.scm 789 */
BgL_tmpz00_18257 = BgL_arg2972z00_3969
; }  else 
{ 
 obj_t BgL_auxz00_18260;
BgL_auxz00_18260 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(34707L), BGl_string6005z00zz__datez00, BGl_string5789z00zz__datez00, BgL_arg2972z00_3969); 
FAILURE(BgL_auxz00_18260,BFALSE,BFALSE);} 
BgL_nz00_8671 = 
(long)CINT(BgL_tmpz00_18257); } 
if(
(BgL_nz00_8671<0L))
{ /* Llib/date.scm 789 */
BgL_arg2936z00_3957 = 
NEG(BgL_nz00_8671); }  else 
{ /* Llib/date.scm 789 */
BgL_arg2936z00_3957 = BgL_nz00_8671; } } } 
{ /* Llib/date.scm 780 */
 obj_t BgL_list2937z00_3958;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2940z00_3959;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2941z00_3960;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2942z00_3961;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2943z00_3962;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2954z00_3963;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2955z00_3964;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2956z00_3965;
{ /* Llib/date.scm 780 */
 obj_t BgL_arg2966z00_3966;
BgL_arg2966z00_3966 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2936z00_3957), BNIL); 
BgL_arg2956z00_3965 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2935z00_3956), BgL_arg2966z00_3966); } 
BgL_arg2955z00_3964 = 
MAKE_YOUNG_PAIR(BgL_arg2934z00_3955, BgL_arg2956z00_3965); } 
BgL_arg2954z00_3963 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2933z00_3954), BgL_arg2955z00_3964); } 
BgL_arg2943z00_3962 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2932z00_3953), BgL_arg2954z00_3963); } 
BgL_arg2942z00_3961 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2931z00_3952), BgL_arg2943z00_3962); } 
BgL_arg2941z00_3960 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2930z00_3951), BgL_arg2942z00_3961); } 
BgL_arg2940z00_3959 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2929z00_3950), BgL_arg2941z00_3960); } 
BgL_list2937z00_3958 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg2928z00_3949), BgL_arg2940z00_3959); } 
return 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string6006z00zz__datez00, BgL_list2937z00_3958);} } } } 

}



/* &date->iso8601-date */
obj_t BGl_z62datezd2ze3iso8601zd2datez81zz__datez00(obj_t BgL_envz00_9862, obj_t BgL_datez00_9863)
{
{ /* Llib/date.scm 770 */
{ /* Llib/date.scm 771 */
 obj_t BgL_auxz00_18286;
if(
BGL_DATEP(BgL_datez00_9863))
{ /* Llib/date.scm 771 */
BgL_auxz00_18286 = BgL_datez00_9863
; }  else 
{ 
 obj_t BgL_auxz00_18289;
BgL_auxz00_18289 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(34068L), BGl_string6007z00zz__datez00, BGl_string5940z00zz__datez00, BgL_datez00_9863); 
FAILURE(BgL_auxz00_18289,BFALSE,BFALSE);} 
return 
BGl_datezd2ze3iso8601zd2dateze3zz__datez00(BgL_auxz00_18286);} } 

}



/* iso8601-date->date */
BGL_EXPORTED_DEF obj_t BGl_iso8601zd2datezd2ze3dateze3zz__datez00(obj_t BgL_stringz00_94)
{
{ /* Llib/date.scm 806 */
{ /* Llib/date.scm 807 */
 obj_t BgL_portz00_3970;
{ /* Ieee/port.scm 469 */
 long BgL_endz00_3978;
BgL_endz00_3978 = 
STRING_LENGTH(BgL_stringz00_94); 
{ /* Ieee/port.scm 469 */

BgL_portz00_3970 = 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_stringz00_94, 
BINT(0L), 
BINT(BgL_endz00_3978)); } } 
{ /* Llib/date.scm 808 */
 obj_t BgL_exitd1065z00_3971;
BgL_exitd1065z00_3971 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/date.scm 810 */
 obj_t BgL_zc3z04anonymousza32973ze3z87_9864;
BgL_zc3z04anonymousza32973ze3z87_9864 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32973ze3ze5zz__datez00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza32973ze3z87_9864, 
(int)(0L), BgL_portz00_3970); 
{ /* Llib/date.scm 808 */
 obj_t BgL_arg4717z00_8677;
{ /* Llib/date.scm 808 */
 obj_t BgL_arg4718z00_8678;
BgL_arg4718z00_8678 = 
BGL_EXITD_PROTECT(BgL_exitd1065z00_3971); 
BgL_arg4717z00_8677 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32973ze3z87_9864, BgL_arg4718z00_8678); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1065z00_3971, BgL_arg4717z00_8677); BUNSPEC; } 
{ /* Llib/date.scm 809 */
 obj_t BgL_tmp1067z00_3973;
BgL_tmp1067z00_3973 = 
BGl_iso8601zd2parsezd2datez00zz__datez00(BgL_portz00_3970); 
{ /* Llib/date.scm 808 */
 bool_t BgL_test7229z00_18308;
{ /* Llib/date.scm 808 */
 obj_t BgL_arg4716z00_8680;
BgL_arg4716z00_8680 = 
BGL_EXITD_PROTECT(BgL_exitd1065z00_3971); 
BgL_test7229z00_18308 = 
PAIRP(BgL_arg4716z00_8680); } 
if(BgL_test7229z00_18308)
{ /* Llib/date.scm 808 */
 obj_t BgL_arg4714z00_8681;
{ /* Llib/date.scm 808 */
 obj_t BgL_arg4715z00_8682;
BgL_arg4715z00_8682 = 
BGL_EXITD_PROTECT(BgL_exitd1065z00_3971); 
{ /* Llib/date.scm 808 */
 obj_t BgL_pairz00_8683;
if(
PAIRP(BgL_arg4715z00_8682))
{ /* Llib/date.scm 808 */
BgL_pairz00_8683 = BgL_arg4715z00_8682; }  else 
{ 
 obj_t BgL_auxz00_18314;
BgL_auxz00_18314 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(35604L), BGl_string6008z00zz__datez00, BGl_string6009z00zz__datez00, BgL_arg4715z00_8682); 
FAILURE(BgL_auxz00_18314,BFALSE,BFALSE);} 
BgL_arg4714z00_8681 = 
CDR(BgL_pairz00_8683); } } 
BGL_EXITD_PROTECT_SET(BgL_exitd1065z00_3971, BgL_arg4714z00_8681); BUNSPEC; }  else 
{ /* Llib/date.scm 808 */BFALSE; } } 
bgl_close_input_port(BgL_portz00_3970); 
return BgL_tmp1067z00_3973;} } } } } 

}



/* &iso8601-date->date */
obj_t BGl_z62iso8601zd2datezd2ze3datez81zz__datez00(obj_t BgL_envz00_9865, obj_t BgL_stringz00_9866)
{
{ /* Llib/date.scm 806 */
{ /* Llib/date.scm 807 */
 obj_t BgL_auxz00_18321;
if(
STRINGP(BgL_stringz00_9866))
{ /* Llib/date.scm 807 */
BgL_auxz00_18321 = BgL_stringz00_9866
; }  else 
{ 
 obj_t BgL_auxz00_18324;
BgL_auxz00_18324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(35557L), BGl_string6010z00zz__datez00, BGl_string5984z00zz__datez00, BgL_stringz00_9866); 
FAILURE(BgL_auxz00_18324,BFALSE,BFALSE);} 
return 
BGl_iso8601zd2datezd2ze3dateze3zz__datez00(BgL_auxz00_18321);} } 

}



/* &<@anonymous:2973> */
obj_t BGl_z62zc3z04anonymousza32973ze3ze5zz__datez00(obj_t BgL_envz00_9867)
{
{ /* Llib/date.scm 808 */
{ /* Llib/date.scm 810 */
 obj_t BgL_portz00_9868;
BgL_portz00_9868 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_9867, 
(int)(0L))); 
return 
bgl_close_input_port(BgL_portz00_9868);} } 

}



/* iso8601-parse-date */
BGL_EXPORTED_DEF obj_t BGl_iso8601zd2parsezd2datez00zz__datez00(obj_t BgL_ipz00_95)
{
{ /* Llib/date.scm 815 */
{ 
 obj_t BgL_iportz00_6326; long BgL_yyyyz00_6327; long BgL_mmz00_6328; long BgL_ddz00_6329; long BgL_hhz00_6330; long BgL_mmz00_6331; long BgL_ssz00_6332; BGL_LONGLONG_T BgL_sssz00_6333; long BgL_za7hhza7_6334;
{ 
 obj_t BgL_iportz00_5970; long BgL_yyyyz00_5971; long BgL_mmz00_5972; long BgL_ddz00_5973; long BgL_hhz00_5974; long BgL_mmz00_5975; long BgL_ssz00_5976; BGL_LONGLONG_T BgL_sssz00_5977;
{ 
 obj_t BgL_iportz00_5666; long BgL_yyyyz00_5667; long BgL_mmz00_5668; long BgL_ddz00_5669; long BgL_hhz00_5670; long BgL_mmz00_5671; long BgL_ssz00_5672;
{ 
 obj_t BgL_iportz00_5385; long BgL_yyyyz00_5386; long BgL_mmz00_5387; long BgL_ddz00_5388; long BgL_hhz00_5389; long BgL_mmz00_5390;
{ 
 obj_t BgL_iportz00_5106; long BgL_yyyyz00_5107; long BgL_mmz00_5108; long BgL_ddz00_5109; long BgL_hhz00_5110;
{ 
 obj_t BgL_iportz00_4826; long BgL_yyyyz00_4827; long BgL_mmz00_4828; long BgL_ddz00_4829;
{ 
 obj_t BgL_iportz00_4549; long BgL_yyyyz00_4550; long BgL_mmz00_4551;
{ 
 obj_t BgL_iportz00_4273; long BgL_yyyyz00_4274;
{ 
 obj_t BgL_iportz00_3989;
{ /* Llib/date.scm 959 */
 obj_t BgL_aux5737z00_10846;
BgL_iportz00_3989 = BgL_ipz00_95; 
{ 
 obj_t BgL_iportz00_4032; long BgL_lastzd2matchzd2_4033; long BgL_forwardz00_4034; long BgL_bufposz00_4035; obj_t BgL_iportz00_4053; long BgL_lastzd2matchzd2_4054; long BgL_forwardz00_4055; long BgL_bufposz00_4056; obj_t BgL_iportz00_4068; long BgL_lastzd2matchzd2_4069; long BgL_forwardz00_4070; long BgL_bufposz00_4071; obj_t BgL_iportz00_4082; long BgL_lastzd2matchzd2_4083; long BgL_forwardz00_4084; long BgL_bufposz00_4085;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_3989))
{ /* Llib/date.scm 948 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg2975z00_4025;
{ /* Llib/date.scm 948 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_4026;
{ /* Llib/date.scm 948 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_4029;
BgL_new1077z00_4029 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 948 */
 long BgL_arg2978z00_4030;
BgL_arg2978z00_4030 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_4029), BgL_arg2978z00_4030); } 
BgL_new1078z00_4026 = BgL_new1077z00_4029; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4026)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4026)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_18343;
{ /* Llib/date.scm 948 */
 obj_t BgL_arg2976z00_4027;
{ /* Llib/date.scm 948 */
 obj_t BgL_arg2977z00_4028;
{ /* Llib/date.scm 948 */
 obj_t BgL_classz00_9416;
BgL_classz00_9416 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg2977z00_4028 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9416); } 
BgL_arg2976z00_4027 = 
VECTOR_REF(BgL_arg2977z00_4028,2L); } 
{ /* Llib/date.scm 948 */
 obj_t BgL_auxz00_18347;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg2976z00_4027))
{ /* Llib/date.scm 948 */
BgL_auxz00_18347 = BgL_arg2976z00_4027
; }  else 
{ 
 obj_t BgL_auxz00_18350;
BgL_auxz00_18350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(40048L), BGl_string6022z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg2976z00_4027); 
FAILURE(BgL_auxz00_18350,BFALSE,BFALSE);} 
BgL_auxz00_18343 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_18347); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4026)))->BgL_stackz00)=((obj_t)BgL_auxz00_18343),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4026)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4026)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4026)))->BgL_objz00)=((obj_t)BgL_iportz00_3989),BUNSPEC); 
BgL_arg2975z00_4025 = BgL_new1078z00_4026; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg2975z00_4025)); }  else 
{ /* Llib/date.scm 948 */
RGC_START_MATCH(BgL_iportz00_3989); 
{ /* Llib/date.scm 948 */
 long BgL_matchz00_4221;
{ /* Llib/date.scm 948 */
 long BgL_arg3161z00_4236; long BgL_arg3162z00_4237;
BgL_arg3161z00_4236 = 
RGC_BUFFER_FORWARD(BgL_iportz00_3989); 
BgL_arg3162z00_4237 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_3989); 
BgL_iportz00_4032 = BgL_iportz00_3989; 
BgL_lastzd2matchzd2_4033 = 1L; 
BgL_forwardz00_4034 = BgL_arg3161z00_4236; 
BgL_bufposz00_4035 = BgL_arg3162z00_4237; 
BgL_zc3z04anonymousza32980ze3z87_4036:
if(
(BgL_forwardz00_4034==BgL_bufposz00_4035))
{ /* Llib/date.scm 948 */
if(
rgc_fill_buffer(BgL_iportz00_4032))
{ /* Llib/date.scm 948 */
 long BgL_arg2985z00_4039; long BgL_arg2986z00_4040;
BgL_arg2985z00_4039 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4032); 
BgL_arg2986z00_4040 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4032); 
{ 
 long BgL_bufposz00_18374; long BgL_forwardz00_18373;
BgL_forwardz00_18373 = BgL_arg2985z00_4039; 
BgL_bufposz00_18374 = BgL_arg2986z00_4040; 
BgL_bufposz00_4035 = BgL_bufposz00_18374; 
BgL_forwardz00_4034 = BgL_forwardz00_18373; 
goto BgL_zc3z04anonymousza32980ze3z87_4036;} }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_lastzd2matchzd2_4033; } }  else 
{ /* Llib/date.scm 948 */
 int BgL_curz00_4041;
BgL_curz00_4041 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4032, BgL_forwardz00_4034); 
{ /* Llib/date.scm 948 */

{ /* Llib/date.scm 948 */
 bool_t BgL_test7236z00_18376;
if(
(
(long)(BgL_curz00_4041)>=48L))
{ /* Llib/date.scm 948 */
BgL_test7236z00_18376 = 
(
(long)(BgL_curz00_4041)<58L)
; }  else 
{ /* Llib/date.scm 948 */
BgL_test7236z00_18376 = ((bool_t)0)
; } 
if(BgL_test7236z00_18376)
{ /* Llib/date.scm 948 */
BgL_iportz00_4053 = BgL_iportz00_4032; 
BgL_lastzd2matchzd2_4054 = BgL_lastzd2matchzd2_4033; 
BgL_forwardz00_4055 = 
(1L+BgL_forwardz00_4034); 
BgL_bufposz00_4056 = BgL_bufposz00_4035; 
BgL_zc3z04anonymousza32992ze3z87_4057:
{ /* Llib/date.scm 948 */
 long BgL_newzd2matchzd2_4058;
RGC_STOP_MATCH(BgL_iportz00_4053, BgL_forwardz00_4055); 
BgL_newzd2matchzd2_4058 = 1L; 
if(
(BgL_forwardz00_4055==BgL_bufposz00_4056))
{ /* Llib/date.scm 948 */
if(
rgc_fill_buffer(BgL_iportz00_4053))
{ /* Llib/date.scm 948 */
 long BgL_arg2995z00_4061; long BgL_arg2999z00_4062;
BgL_arg2995z00_4061 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4053); 
BgL_arg2999z00_4062 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4053); 
{ 
 long BgL_bufposz00_18390; long BgL_forwardz00_18389;
BgL_forwardz00_18389 = BgL_arg2995z00_4061; 
BgL_bufposz00_18390 = BgL_arg2999z00_4062; 
BgL_bufposz00_4056 = BgL_bufposz00_18390; 
BgL_forwardz00_4055 = BgL_forwardz00_18389; 
goto BgL_zc3z04anonymousza32992ze3z87_4057;} }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_newzd2matchzd2_4058; } }  else 
{ /* Llib/date.scm 948 */
 int BgL_curz00_4063;
BgL_curz00_4063 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4053, BgL_forwardz00_4055); 
{ /* Llib/date.scm 948 */

{ /* Llib/date.scm 948 */
 bool_t BgL_test7240z00_18392;
if(
(
(long)(BgL_curz00_4063)>=48L))
{ /* Llib/date.scm 948 */
BgL_test7240z00_18392 = 
(
(long)(BgL_curz00_4063)<58L)
; }  else 
{ /* Llib/date.scm 948 */
BgL_test7240z00_18392 = ((bool_t)0)
; } 
if(BgL_test7240z00_18392)
{ /* Llib/date.scm 948 */
BgL_iportz00_4068 = BgL_iportz00_4053; 
BgL_lastzd2matchzd2_4069 = BgL_newzd2matchzd2_4058; 
BgL_forwardz00_4070 = 
(1L+BgL_forwardz00_4055); 
BgL_bufposz00_4071 = BgL_bufposz00_4056; 
BgL_zc3z04anonymousza33003ze3z87_4072:
if(
(BgL_forwardz00_4070==BgL_bufposz00_4071))
{ /* Llib/date.scm 948 */
if(
rgc_fill_buffer(BgL_iportz00_4068))
{ /* Llib/date.scm 948 */
 long BgL_arg3008z00_4075; long BgL_arg3009z00_4076;
BgL_arg3008z00_4075 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4068); 
BgL_arg3009z00_4076 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4068); 
{ 
 long BgL_bufposz00_18405; long BgL_forwardz00_18404;
BgL_forwardz00_18404 = BgL_arg3008z00_4075; 
BgL_bufposz00_18405 = BgL_arg3009z00_4076; 
BgL_bufposz00_4071 = BgL_bufposz00_18405; 
BgL_forwardz00_4070 = BgL_forwardz00_18404; 
goto BgL_zc3z04anonymousza33003ze3z87_4072;} }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_lastzd2matchzd2_4069; } }  else 
{ /* Llib/date.scm 948 */
 int BgL_curz00_4077;
BgL_curz00_4077 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4068, BgL_forwardz00_4070); 
{ /* Llib/date.scm 948 */

{ /* Llib/date.scm 948 */
 bool_t BgL_test7244z00_18407;
if(
(
(long)(BgL_curz00_4077)>=48L))
{ /* Llib/date.scm 948 */
BgL_test7244z00_18407 = 
(
(long)(BgL_curz00_4077)<58L)
; }  else 
{ /* Llib/date.scm 948 */
BgL_test7244z00_18407 = ((bool_t)0)
; } 
if(BgL_test7244z00_18407)
{ /* Llib/date.scm 948 */
BgL_iportz00_4082 = BgL_iportz00_4068; 
BgL_lastzd2matchzd2_4083 = BgL_lastzd2matchzd2_4069; 
BgL_forwardz00_4084 = 
(1L+BgL_forwardz00_4070); 
BgL_bufposz00_4085 = BgL_bufposz00_4071; 
BgL_zc3z04anonymousza33013ze3z87_4086:
if(
(BgL_forwardz00_4084==BgL_bufposz00_4085))
{ /* Llib/date.scm 948 */
if(
rgc_fill_buffer(BgL_iportz00_4082))
{ /* Llib/date.scm 948 */
 long BgL_arg3016z00_4089; long BgL_arg3017z00_4090;
BgL_arg3016z00_4089 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4082); 
BgL_arg3017z00_4090 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4082); 
{ 
 long BgL_bufposz00_18420; long BgL_forwardz00_18419;
BgL_forwardz00_18419 = BgL_arg3016z00_4089; 
BgL_bufposz00_18420 = BgL_arg3017z00_4090; 
BgL_bufposz00_4085 = BgL_bufposz00_18420; 
BgL_forwardz00_4084 = BgL_forwardz00_18419; 
goto BgL_zc3z04anonymousza33013ze3z87_4086;} }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_lastzd2matchzd2_4083; } }  else 
{ /* Llib/date.scm 948 */
 int BgL_curz00_4091;
BgL_curz00_4091 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4082, BgL_forwardz00_4084); 
{ /* Llib/date.scm 948 */

{ /* Llib/date.scm 948 */
 bool_t BgL_test7248z00_18422;
if(
(
(long)(BgL_curz00_4091)>=48L))
{ /* Llib/date.scm 948 */
BgL_test7248z00_18422 = 
(
(long)(BgL_curz00_4091)<58L)
; }  else 
{ /* Llib/date.scm 948 */
BgL_test7248z00_18422 = ((bool_t)0)
; } 
if(BgL_test7248z00_18422)
{ /* Llib/date.scm 948 */
 long BgL_arg3020z00_4094;
BgL_arg3020z00_4094 = 
(1L+BgL_forwardz00_4084); 
{ /* Llib/date.scm 948 */
 long BgL_newzd2matchzd2_9384;
RGC_STOP_MATCH(BgL_iportz00_4082, BgL_arg3020z00_4094); 
BgL_newzd2matchzd2_9384 = 0L; 
BgL_matchz00_4221 = BgL_newzd2matchzd2_9384; } }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_lastzd2matchzd2_4083; } } } } }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_lastzd2matchzd2_4069; } } } } }  else 
{ /* Llib/date.scm 948 */
BgL_matchz00_4221 = BgL_newzd2matchzd2_4058; } } } } } }  else 
{ /* Llib/date.scm 948 */
 long BgL_arg2990z00_4045;
BgL_arg2990z00_4045 = 
(1L+BgL_forwardz00_4034); 
{ /* Llib/date.scm 948 */
 long BgL_newzd2matchzd2_9346;
RGC_STOP_MATCH(BgL_iportz00_4032, BgL_arg2990z00_4045); 
BgL_newzd2matchzd2_9346 = 1L; 
BgL_matchz00_4221 = BgL_newzd2matchzd2_9346; } } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_3989); 
switch( BgL_matchz00_4221) { case 1L : 

{ /* Llib/date.scm 956 */
 obj_t BgL_auxz00_18436;
{ /* Llib/date.scm 948 */
 bool_t BgL_test7250z00_18437;
{ /* Llib/date.scm 948 */
 long BgL_arg3139z00_4211;
BgL_arg3139z00_4211 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_3989); 
BgL_test7250z00_18437 = 
(BgL_arg3139z00_4211==0L); } 
if(BgL_test7250z00_18437)
{ /* Llib/date.scm 948 */
BgL_auxz00_18436 = BEOF
; }  else 
{ /* Llib/date.scm 948 */
BgL_auxz00_18436 = 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_3989))
; } } 
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, BgL_auxz00_18436, BgL_iportz00_3989); } break;case 0L : 

{ /* Llib/date.scm 950 */
 long BgL_b1z00_4226; long BgL_b2z00_4227; long BgL_b3z00_4228; long BgL_b4z00_4229;
{ /* Llib/date.scm 950 */
 int BgL_arg3156z00_4232;
{ /* Llib/date.scm 948 */
 int BgL_tmpz00_18443;
BgL_tmpz00_18443 = 
(int)(0L); 
BgL_arg3156z00_4232 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_3989, BgL_tmpz00_18443); } 
BgL_b1z00_4226 = 
(
(long)(BgL_arg3156z00_4232)-48L); } 
{ /* Llib/date.scm 951 */
 int BgL_arg3157z00_4233;
{ /* Llib/date.scm 948 */
 int BgL_tmpz00_18448;
BgL_tmpz00_18448 = 
(int)(1L); 
BgL_arg3157z00_4233 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_3989, BgL_tmpz00_18448); } 
BgL_b2z00_4227 = 
(
(long)(BgL_arg3157z00_4233)-48L); } 
{ /* Llib/date.scm 952 */
 int BgL_arg3158z00_4234;
{ /* Llib/date.scm 948 */
 int BgL_tmpz00_18453;
BgL_tmpz00_18453 = 
(int)(3L); 
BgL_arg3158z00_4234 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_3989, BgL_tmpz00_18453); } 
BgL_b3z00_4228 = 
(
(long)(BgL_arg3158z00_4234)-48L); } 
{ /* Llib/date.scm 953 */
 int BgL_arg3160z00_4235;
{ /* Llib/date.scm 948 */
 int BgL_tmpz00_18458;
BgL_tmpz00_18458 = 
(int)(4L); 
BgL_arg3160z00_4235 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_3989, BgL_tmpz00_18458); } 
BgL_b4z00_4229 = 
(
(long)(BgL_arg3160z00_4235)-48L); } 
BgL_iportz00_4273 = BgL_iportz00_3989; 
BgL_yyyyz00_4274 = 
rgc_buffer_fixnum(BgL_iportz00_3989); 
{ 
 obj_t BgL_iportz00_4316; long BgL_lastzd2matchzd2_4317; long BgL_forwardz00_4318; long BgL_bufposz00_4319; obj_t BgL_iportz00_4335; long BgL_lastzd2matchzd2_4336; long BgL_forwardz00_4337; long BgL_bufposz00_4338; obj_t BgL_iportz00_4350; long BgL_lastzd2matchzd2_4351; long BgL_forwardz00_4352; long BgL_bufposz00_4353;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_4273))
{ /* Llib/date.scm 935 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg3171z00_4309;
{ /* Llib/date.scm 935 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_4310;
{ /* Llib/date.scm 935 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_4313;
BgL_new1077z00_4313 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 935 */
 long BgL_arg3175z00_4314;
BgL_arg3175z00_4314 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_4313), BgL_arg3175z00_4314); } 
BgL_new1078z00_4310 = BgL_new1077z00_4313; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4310)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4310)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_18473;
{ /* Llib/date.scm 935 */
 obj_t BgL_arg3172z00_4311;
{ /* Llib/date.scm 935 */
 obj_t BgL_arg3174z00_4312;
{ /* Llib/date.scm 935 */
 obj_t BgL_classz00_9332;
BgL_classz00_9332 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg3174z00_4312 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9332); } 
BgL_arg3172z00_4311 = 
VECTOR_REF(BgL_arg3174z00_4312,2L); } 
{ /* Llib/date.scm 935 */
 obj_t BgL_auxz00_18477;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg3172z00_4311))
{ /* Llib/date.scm 935 */
BgL_auxz00_18477 = BgL_arg3172z00_4311
; }  else 
{ 
 obj_t BgL_auxz00_18480;
BgL_auxz00_18480 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(39676L), BGl_string6021z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg3172z00_4311); 
FAILURE(BgL_auxz00_18480,BFALSE,BFALSE);} 
BgL_auxz00_18473 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_18477); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4310)))->BgL_stackz00)=((obj_t)BgL_auxz00_18473),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4310)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4310)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4310)))->BgL_objz00)=((obj_t)BgL_iportz00_4273),BUNSPEC); 
BgL_arg3171z00_4309 = BgL_new1078z00_4310; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg3171z00_4309)); }  else 
{ /* Llib/date.scm 935 */
RGC_START_MATCH(BgL_iportz00_4273); 
{ /* Llib/date.scm 935 */
 long BgL_matchz00_4489;
{ /* Llib/date.scm 935 */
 long BgL_arg3356z00_4513; long BgL_arg3357z00_4514;
BgL_arg3356z00_4513 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4273); 
BgL_arg3357z00_4514 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4273); 
BgL_iportz00_4316 = BgL_iportz00_4273; 
BgL_lastzd2matchzd2_4317 = 1L; 
BgL_forwardz00_4318 = BgL_arg3356z00_4513; 
BgL_bufposz00_4319 = BgL_arg3357z00_4514; 
BgL_zc3z04anonymousza33178ze3z87_4320:
if(
(BgL_forwardz00_4318==BgL_bufposz00_4319))
{ /* Llib/date.scm 935 */
if(
rgc_fill_buffer(BgL_iportz00_4316))
{ /* Llib/date.scm 935 */
 long BgL_arg3181z00_4323; long BgL_arg3182z00_4324;
BgL_arg3181z00_4323 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4316); 
BgL_arg3182z00_4324 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4316); 
{ 
 long BgL_bufposz00_18504; long BgL_forwardz00_18503;
BgL_forwardz00_18503 = BgL_arg3181z00_4323; 
BgL_bufposz00_18504 = BgL_arg3182z00_4324; 
BgL_bufposz00_4319 = BgL_bufposz00_18504; 
BgL_forwardz00_4318 = BgL_forwardz00_18503; 
goto BgL_zc3z04anonymousza33178ze3z87_4320;} }  else 
{ /* Llib/date.scm 935 */
BgL_matchz00_4489 = BgL_lastzd2matchzd2_4317; } }  else 
{ /* Llib/date.scm 935 */
 int BgL_curz00_4325;
BgL_curz00_4325 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4316, BgL_forwardz00_4318); 
{ /* Llib/date.scm 935 */

if(
(
(long)(BgL_curz00_4325)==45L))
{ /* Llib/date.scm 935 */
BgL_iportz00_4335 = BgL_iportz00_4316; 
BgL_lastzd2matchzd2_4336 = BgL_lastzd2matchzd2_4317; 
BgL_forwardz00_4337 = 
(1L+BgL_forwardz00_4318); 
BgL_bufposz00_4338 = BgL_bufposz00_4319; 
BgL_zc3z04anonymousza33188ze3z87_4339:
{ /* Llib/date.scm 935 */
 long BgL_newzd2matchzd2_4340;
RGC_STOP_MATCH(BgL_iportz00_4335, BgL_forwardz00_4337); 
BgL_newzd2matchzd2_4340 = 1L; 
if(
(BgL_forwardz00_4337==BgL_bufposz00_4338))
{ /* Llib/date.scm 935 */
if(
rgc_fill_buffer(BgL_iportz00_4335))
{ /* Llib/date.scm 935 */
 long BgL_arg3192z00_4343; long BgL_arg3195z00_4344;
BgL_arg3192z00_4343 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4335); 
BgL_arg3195z00_4344 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4335); 
{ 
 long BgL_bufposz00_18517; long BgL_forwardz00_18516;
BgL_forwardz00_18516 = BgL_arg3192z00_4343; 
BgL_bufposz00_18517 = BgL_arg3195z00_4344; 
BgL_bufposz00_4338 = BgL_bufposz00_18517; 
BgL_forwardz00_4337 = BgL_forwardz00_18516; 
goto BgL_zc3z04anonymousza33188ze3z87_4339;} }  else 
{ /* Llib/date.scm 935 */
BgL_matchz00_4489 = BgL_newzd2matchzd2_4340; } }  else 
{ /* Llib/date.scm 935 */
 int BgL_curz00_4345;
BgL_curz00_4345 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4335, BgL_forwardz00_4337); 
{ /* Llib/date.scm 935 */

{ /* Llib/date.scm 935 */
 bool_t BgL_test7258z00_18519;
if(
(
(long)(BgL_curz00_4345)>=48L))
{ /* Llib/date.scm 935 */
BgL_test7258z00_18519 = 
(
(long)(BgL_curz00_4345)<58L)
; }  else 
{ /* Llib/date.scm 935 */
BgL_test7258z00_18519 = ((bool_t)0)
; } 
if(BgL_test7258z00_18519)
{ /* Llib/date.scm 935 */
BgL_iportz00_4350 = BgL_iportz00_4335; 
BgL_lastzd2matchzd2_4351 = BgL_newzd2matchzd2_4340; 
BgL_forwardz00_4352 = 
(1L+BgL_forwardz00_4337); 
BgL_bufposz00_4353 = BgL_bufposz00_4338; 
BgL_zc3z04anonymousza33200ze3z87_4354:
if(
(BgL_forwardz00_4352==BgL_bufposz00_4353))
{ /* Llib/date.scm 935 */
if(
rgc_fill_buffer(BgL_iportz00_4350))
{ /* Llib/date.scm 935 */
 long BgL_arg3203z00_4357; long BgL_arg3204z00_4358;
BgL_arg3203z00_4357 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4350); 
BgL_arg3204z00_4358 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4350); 
{ 
 long BgL_bufposz00_18532; long BgL_forwardz00_18531;
BgL_forwardz00_18531 = BgL_arg3203z00_4357; 
BgL_bufposz00_18532 = BgL_arg3204z00_4358; 
BgL_bufposz00_4353 = BgL_bufposz00_18532; 
BgL_forwardz00_4352 = BgL_forwardz00_18531; 
goto BgL_zc3z04anonymousza33200ze3z87_4354;} }  else 
{ /* Llib/date.scm 935 */
BgL_matchz00_4489 = BgL_lastzd2matchzd2_4351; } }  else 
{ /* Llib/date.scm 935 */
 int BgL_curz00_4359;
BgL_curz00_4359 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4350, BgL_forwardz00_4352); 
{ /* Llib/date.scm 935 */

{ /* Llib/date.scm 935 */
 bool_t BgL_test7262z00_18534;
if(
(
(long)(BgL_curz00_4359)>=48L))
{ /* Llib/date.scm 935 */
BgL_test7262z00_18534 = 
(
(long)(BgL_curz00_4359)<58L)
; }  else 
{ /* Llib/date.scm 935 */
BgL_test7262z00_18534 = ((bool_t)0)
; } 
if(BgL_test7262z00_18534)
{ /* Llib/date.scm 935 */
 long BgL_arg3207z00_4362;
BgL_arg3207z00_4362 = 
(1L+BgL_forwardz00_4352); 
{ /* Llib/date.scm 935 */
 long BgL_newzd2matchzd2_9302;
RGC_STOP_MATCH(BgL_iportz00_4350, BgL_arg3207z00_4362); 
BgL_newzd2matchzd2_9302 = 0L; 
BgL_matchz00_4489 = BgL_newzd2matchzd2_9302; } }  else 
{ /* Llib/date.scm 935 */
BgL_matchz00_4489 = BgL_lastzd2matchzd2_4351; } } } } }  else 
{ /* Llib/date.scm 935 */
BgL_matchz00_4489 = BgL_newzd2matchzd2_4340; } } } } } }  else 
{ /* Llib/date.scm 935 */
 long BgL_arg3186z00_4328;
BgL_arg3186z00_4328 = 
(1L+BgL_forwardz00_4318); 
{ /* Llib/date.scm 935 */
 long BgL_newzd2matchzd2_9274;
RGC_STOP_MATCH(BgL_iportz00_4316, BgL_arg3186z00_4328); 
BgL_newzd2matchzd2_9274 = 1L; 
BgL_matchz00_4489 = BgL_newzd2matchzd2_9274; } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_4273); 
switch( BgL_matchz00_4489) { case 1L : 

{ /* Llib/date.scm 942 */
 bool_t BgL_test7264z00_18547;
{ /* Llib/date.scm 942 */
 obj_t BgL_tmpz00_18548;
BgL_tmpz00_18548 = 
BGl_thezd2failureze77z35zz__datez00(BgL_iportz00_4273); 
BgL_test7264z00_18547 = 
EOF_OBJECTP(BgL_tmpz00_18548); } 
if(BgL_test7264z00_18547)
{ /* Llib/date.scm 942 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_18553;
{ /* Llib/date.scm 943 */
 obj_t BgL_aux5732z00_10841;
BgL_aux5732z00_10841 = BFALSE; 
{ 
 obj_t BgL_auxz00_18560;
BgL_auxz00_18560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(39894L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5732z00_10841); 
FAILURE(BgL_auxz00_18560,BFALSE,BFALSE);} } 
BgL_tmpz00_18553 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(1L), 
(int)(1L), 
(int)(1L), 
(int)(BgL_yyyyz00_4274), BgL_tmpz00_18553, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(1L), 
(int)(1L), 
(int)(1L), 
(int)(BgL_yyyyz00_4274), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 942 */
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, 
BGl_thezd2failureze77z35zz__datez00(BgL_iportz00_4273), BgL_iportz00_4273); } } break;case 0L : 

{ /* Llib/date.scm 937 */
 long BgL_b1z00_4506; long BgL_b2z00_4507;
{ /* Llib/date.scm 937 */
 int BgL_arg3350z00_4511;
{ /* Llib/date.scm 935 */
 int BgL_tmpz00_18576;
BgL_tmpz00_18576 = 
(int)(1L); 
BgL_arg3350z00_4511 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_4273, BgL_tmpz00_18576); } 
BgL_b1z00_4506 = 
(
(long)(BgL_arg3350z00_4511)-48L); } 
{ /* Llib/date.scm 938 */
 int BgL_arg3352z00_4512;
{ /* Llib/date.scm 935 */
 int BgL_tmpz00_18581;
BgL_tmpz00_18581 = 
(int)(2L); 
BgL_arg3352z00_4512 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_4273, BgL_tmpz00_18581); } 
BgL_b2z00_4507 = 
(
(long)(BgL_arg3352z00_4512)-48L); } 
BgL_iportz00_4549 = BgL_iportz00_4273; 
BgL_yyyyz00_4550 = BgL_yyyyz00_4274; 
BgL_mmz00_4551 = 
(
(BgL_b1z00_4506*10L)+BgL_b2z00_4507); 
{ 
 obj_t BgL_iportz00_4593; long BgL_lastzd2matchzd2_4594; long BgL_forwardz00_4595; long BgL_bufposz00_4596; obj_t BgL_iportz00_4612; long BgL_lastzd2matchzd2_4613; long BgL_forwardz00_4614; long BgL_bufposz00_4615; obj_t BgL_iportz00_4627; long BgL_lastzd2matchzd2_4628; long BgL_forwardz00_4629; long BgL_bufposz00_4630;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_4549))
{ /* Llib/date.scm 921 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg3359z00_4586;
{ /* Llib/date.scm 921 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_4587;
{ /* Llib/date.scm 921 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_4590;
BgL_new1077z00_4590 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 921 */
 long BgL_arg3362z00_4591;
BgL_arg3362z00_4591 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_4590), BgL_arg3362z00_4591); } 
BgL_new1078z00_4587 = BgL_new1077z00_4590; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4587)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4587)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_18596;
{ /* Llib/date.scm 921 */
 obj_t BgL_arg3360z00_4588;
{ /* Llib/date.scm 921 */
 obj_t BgL_arg3361z00_4589;
{ /* Llib/date.scm 921 */
 obj_t BgL_classz00_9261;
BgL_classz00_9261 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg3361z00_4589 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9261); } 
BgL_arg3360z00_4588 = 
VECTOR_REF(BgL_arg3361z00_4589,2L); } 
{ /* Llib/date.scm 921 */
 obj_t BgL_auxz00_18600;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg3360z00_4588))
{ /* Llib/date.scm 921 */
BgL_auxz00_18600 = BgL_arg3360z00_4588
; }  else 
{ 
 obj_t BgL_auxz00_18603;
BgL_auxz00_18603 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(39279L), BGl_string6020z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg3360z00_4588); 
FAILURE(BgL_auxz00_18603,BFALSE,BFALSE);} 
BgL_auxz00_18596 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_18600); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4587)))->BgL_stackz00)=((obj_t)BgL_auxz00_18596),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4587)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4587)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4587)))->BgL_objz00)=((obj_t)BgL_iportz00_4549),BUNSPEC); 
BgL_arg3359z00_4586 = BgL_new1078z00_4587; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg3359z00_4586)); }  else 
{ /* Llib/date.scm 921 */
RGC_START_MATCH(BgL_iportz00_4549); 
{ /* Llib/date.scm 921 */
 long BgL_matchz00_4766;
{ /* Llib/date.scm 921 */
 long BgL_arg3515z00_4790; long BgL_arg3517z00_4791;
BgL_arg3515z00_4790 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4549); 
BgL_arg3517z00_4791 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4549); 
BgL_iportz00_4593 = BgL_iportz00_4549; 
BgL_lastzd2matchzd2_4594 = 1L; 
BgL_forwardz00_4595 = BgL_arg3515z00_4790; 
BgL_bufposz00_4596 = BgL_arg3517z00_4791; 
BgL_zc3z04anonymousza33365ze3z87_4597:
if(
(BgL_forwardz00_4595==BgL_bufposz00_4596))
{ /* Llib/date.scm 921 */
if(
rgc_fill_buffer(BgL_iportz00_4593))
{ /* Llib/date.scm 921 */
 long BgL_arg3369z00_4600; long BgL_arg3370z00_4601;
BgL_arg3369z00_4600 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4593); 
BgL_arg3370z00_4601 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4593); 
{ 
 long BgL_bufposz00_18627; long BgL_forwardz00_18626;
BgL_forwardz00_18626 = BgL_arg3369z00_4600; 
BgL_bufposz00_18627 = BgL_arg3370z00_4601; 
BgL_bufposz00_4596 = BgL_bufposz00_18627; 
BgL_forwardz00_4595 = BgL_forwardz00_18626; 
goto BgL_zc3z04anonymousza33365ze3z87_4597;} }  else 
{ /* Llib/date.scm 921 */
BgL_matchz00_4766 = BgL_lastzd2matchzd2_4594; } }  else 
{ /* Llib/date.scm 921 */
 int BgL_curz00_4602;
BgL_curz00_4602 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4593, BgL_forwardz00_4595); 
{ /* Llib/date.scm 921 */

if(
(
(long)(BgL_curz00_4602)==45L))
{ /* Llib/date.scm 921 */
BgL_iportz00_4612 = BgL_iportz00_4593; 
BgL_lastzd2matchzd2_4613 = BgL_lastzd2matchzd2_4594; 
BgL_forwardz00_4614 = 
(1L+BgL_forwardz00_4595); 
BgL_bufposz00_4615 = BgL_bufposz00_4596; 
BgL_zc3z04anonymousza33375ze3z87_4616:
{ /* Llib/date.scm 921 */
 long BgL_newzd2matchzd2_4617;
RGC_STOP_MATCH(BgL_iportz00_4612, BgL_forwardz00_4614); 
BgL_newzd2matchzd2_4617 = 1L; 
if(
(BgL_forwardz00_4614==BgL_bufposz00_4615))
{ /* Llib/date.scm 921 */
if(
rgc_fill_buffer(BgL_iportz00_4612))
{ /* Llib/date.scm 921 */
 long BgL_arg3379z00_4620; long BgL_arg3381z00_4621;
BgL_arg3379z00_4620 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4612); 
BgL_arg3381z00_4621 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4612); 
{ 
 long BgL_bufposz00_18640; long BgL_forwardz00_18639;
BgL_forwardz00_18639 = BgL_arg3379z00_4620; 
BgL_bufposz00_18640 = BgL_arg3381z00_4621; 
BgL_bufposz00_4615 = BgL_bufposz00_18640; 
BgL_forwardz00_4614 = BgL_forwardz00_18639; 
goto BgL_zc3z04anonymousza33375ze3z87_4616;} }  else 
{ /* Llib/date.scm 921 */
BgL_matchz00_4766 = BgL_newzd2matchzd2_4617; } }  else 
{ /* Llib/date.scm 921 */
 int BgL_curz00_4622;
BgL_curz00_4622 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4612, BgL_forwardz00_4614); 
{ /* Llib/date.scm 921 */

{ /* Llib/date.scm 921 */
 bool_t BgL_test7273z00_18642;
if(
(
(long)(BgL_curz00_4622)>=48L))
{ /* Llib/date.scm 921 */
BgL_test7273z00_18642 = 
(
(long)(BgL_curz00_4622)<58L)
; }  else 
{ /* Llib/date.scm 921 */
BgL_test7273z00_18642 = ((bool_t)0)
; } 
if(BgL_test7273z00_18642)
{ /* Llib/date.scm 921 */
BgL_iportz00_4627 = BgL_iportz00_4612; 
BgL_lastzd2matchzd2_4628 = BgL_newzd2matchzd2_4617; 
BgL_forwardz00_4629 = 
(1L+BgL_forwardz00_4614); 
BgL_bufposz00_4630 = BgL_bufposz00_4615; 
BgL_zc3z04anonymousza33385ze3z87_4631:
if(
(BgL_forwardz00_4629==BgL_bufposz00_4630))
{ /* Llib/date.scm 921 */
if(
rgc_fill_buffer(BgL_iportz00_4627))
{ /* Llib/date.scm 921 */
 long BgL_arg3388z00_4634; long BgL_arg3391z00_4635;
BgL_arg3388z00_4634 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4627); 
BgL_arg3391z00_4635 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4627); 
{ 
 long BgL_bufposz00_18655; long BgL_forwardz00_18654;
BgL_forwardz00_18654 = BgL_arg3388z00_4634; 
BgL_bufposz00_18655 = BgL_arg3391z00_4635; 
BgL_bufposz00_4630 = BgL_bufposz00_18655; 
BgL_forwardz00_4629 = BgL_forwardz00_18654; 
goto BgL_zc3z04anonymousza33385ze3z87_4631;} }  else 
{ /* Llib/date.scm 921 */
BgL_matchz00_4766 = BgL_lastzd2matchzd2_4628; } }  else 
{ /* Llib/date.scm 921 */
 int BgL_curz00_4636;
BgL_curz00_4636 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4627, BgL_forwardz00_4629); 
{ /* Llib/date.scm 921 */

{ /* Llib/date.scm 921 */
 bool_t BgL_test7277z00_18657;
if(
(
(long)(BgL_curz00_4636)>=48L))
{ /* Llib/date.scm 921 */
BgL_test7277z00_18657 = 
(
(long)(BgL_curz00_4636)<58L)
; }  else 
{ /* Llib/date.scm 921 */
BgL_test7277z00_18657 = ((bool_t)0)
; } 
if(BgL_test7277z00_18657)
{ /* Llib/date.scm 921 */
 long BgL_arg3394z00_4639;
BgL_arg3394z00_4639 = 
(1L+BgL_forwardz00_4629); 
{ /* Llib/date.scm 921 */
 long BgL_newzd2matchzd2_9231;
RGC_STOP_MATCH(BgL_iportz00_4627, BgL_arg3394z00_4639); 
BgL_newzd2matchzd2_9231 = 0L; 
BgL_matchz00_4766 = BgL_newzd2matchzd2_9231; } }  else 
{ /* Llib/date.scm 921 */
BgL_matchz00_4766 = BgL_lastzd2matchzd2_4628; } } } } }  else 
{ /* Llib/date.scm 921 */
BgL_matchz00_4766 = BgL_newzd2matchzd2_4617; } } } } } }  else 
{ /* Llib/date.scm 921 */
 long BgL_arg3373z00_4605;
BgL_arg3373z00_4605 = 
(1L+BgL_forwardz00_4595); 
{ /* Llib/date.scm 921 */
 long BgL_newzd2matchzd2_9203;
RGC_STOP_MATCH(BgL_iportz00_4593, BgL_arg3373z00_4605); 
BgL_newzd2matchzd2_9203 = 1L; 
BgL_matchz00_4766 = BgL_newzd2matchzd2_9203; } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_4549); 
switch( BgL_matchz00_4766) { case 1L : 

{ /* Llib/date.scm 928 */
 bool_t BgL_test7279z00_18670;
{ /* Llib/date.scm 928 */
 obj_t BgL_tmpz00_18671;
BgL_tmpz00_18671 = 
BGl_thezd2failureze76z35zz__datez00(BgL_iportz00_4549); 
BgL_test7279z00_18670 = 
EOF_OBJECTP(BgL_tmpz00_18671); } 
if(BgL_test7279z00_18670)
{ /* Llib/date.scm 928 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_18676;
{ /* Llib/date.scm 929 */
 obj_t BgL_aux5729z00_10838;
BgL_aux5729z00_10838 = BFALSE; 
{ 
 obj_t BgL_auxz00_18683;
BgL_auxz00_18683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(39503L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5729z00_10838); 
FAILURE(BgL_auxz00_18683,BFALSE,BFALSE);} } 
BgL_tmpz00_18676 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(1L), 
(int)(1L), 
(int)(BgL_mmz00_4551), 
(int)(BgL_yyyyz00_4550), BgL_tmpz00_18676, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(1L), 
(int)(1L), 
(int)(BgL_mmz00_4551), 
(int)(BgL_yyyyz00_4550), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 928 */
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, 
BGl_thezd2failureze76z35zz__datez00(BgL_iportz00_4549), BgL_iportz00_4549); } } break;case 0L : 

{ /* Llib/date.scm 923 */
 long BgL_b1z00_4783; long BgL_b2z00_4784;
{ /* Llib/date.scm 923 */
 int BgL_arg3513z00_4788;
{ /* Llib/date.scm 921 */
 int BgL_tmpz00_18699;
BgL_tmpz00_18699 = 
(int)(1L); 
BgL_arg3513z00_4788 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_4549, BgL_tmpz00_18699); } 
BgL_b1z00_4783 = 
(
(long)(BgL_arg3513z00_4788)-48L); } 
{ /* Llib/date.scm 924 */
 int BgL_arg3514z00_4789;
{ /* Llib/date.scm 921 */
 int BgL_tmpz00_18704;
BgL_tmpz00_18704 = 
(int)(2L); 
BgL_arg3514z00_4789 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_4549, BgL_tmpz00_18704); } 
BgL_b2z00_4784 = 
(
(long)(BgL_arg3514z00_4789)-48L); } 
BgL_iportz00_4826 = BgL_iportz00_4549; 
BgL_yyyyz00_4827 = BgL_yyyyz00_4550; 
BgL_mmz00_4828 = BgL_mmz00_4551; 
BgL_ddz00_4829 = 
(
(BgL_b1z00_4783*10L)+BgL_b2z00_4784); 
{ 
 obj_t BgL_iportz00_4871; long BgL_lastzd2matchzd2_4872; long BgL_forwardz00_4873; long BgL_bufposz00_4874; obj_t BgL_iportz00_4892; long BgL_lastzd2matchzd2_4893; long BgL_forwardz00_4894; long BgL_bufposz00_4895; obj_t BgL_iportz00_4907; long BgL_lastzd2matchzd2_4908; long BgL_forwardz00_4909; long BgL_bufposz00_4910;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_4826))
{ /* Llib/date.scm 906 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg3520z00_4864;
{ /* Llib/date.scm 906 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_4865;
{ /* Llib/date.scm 906 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_4868;
BgL_new1077z00_4868 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 906 */
 long BgL_arg3523z00_4869;
BgL_arg3523z00_4869 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_4868), BgL_arg3523z00_4869); } 
BgL_new1078z00_4865 = BgL_new1077z00_4868; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4865)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4865)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_18719;
{ /* Llib/date.scm 906 */
 obj_t BgL_arg3521z00_4866;
{ /* Llib/date.scm 906 */
 obj_t BgL_arg3522z00_4867;
{ /* Llib/date.scm 906 */
 obj_t BgL_classz00_9190;
BgL_classz00_9190 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg3522z00_4867 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9190); } 
BgL_arg3521z00_4866 = 
VECTOR_REF(BgL_arg3522z00_4867,2L); } 
{ /* Llib/date.scm 906 */
 obj_t BgL_auxz00_18723;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg3521z00_4866))
{ /* Llib/date.scm 906 */
BgL_auxz00_18723 = BgL_arg3521z00_4866
; }  else 
{ 
 obj_t BgL_auxz00_18726;
BgL_auxz00_18726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(38800L), BGl_string6019z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg3521z00_4866); 
FAILURE(BgL_auxz00_18726,BFALSE,BFALSE);} 
BgL_auxz00_18719 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_18723); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_4865)))->BgL_stackz00)=((obj_t)BgL_auxz00_18719),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4865)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4865)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_4865)))->BgL_objz00)=((obj_t)BgL_iportz00_4826),BUNSPEC); 
BgL_arg3520z00_4864 = BgL_new1078z00_4865; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg3520z00_4864)); }  else 
{ /* Llib/date.scm 906 */
RGC_START_MATCH(BgL_iportz00_4826); 
{ /* Llib/date.scm 906 */
 long BgL_matchz00_5046;
{ /* Llib/date.scm 906 */
 long BgL_arg3660z00_5070; long BgL_arg3661z00_5071;
BgL_arg3660z00_5070 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4826); 
BgL_arg3661z00_5071 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4826); 
BgL_iportz00_4871 = BgL_iportz00_4826; 
BgL_lastzd2matchzd2_4872 = 1L; 
BgL_forwardz00_4873 = BgL_arg3660z00_5070; 
BgL_bufposz00_4874 = BgL_arg3661z00_5071; 
BgL_zc3z04anonymousza33525ze3z87_4875:
if(
(BgL_forwardz00_4873==BgL_bufposz00_4874))
{ /* Llib/date.scm 906 */
if(
rgc_fill_buffer(BgL_iportz00_4871))
{ /* Llib/date.scm 906 */
 long BgL_arg3528z00_4878; long BgL_arg3529z00_4879;
BgL_arg3528z00_4878 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4871); 
BgL_arg3529z00_4879 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4871); 
{ 
 long BgL_bufposz00_18750; long BgL_forwardz00_18749;
BgL_forwardz00_18749 = BgL_arg3528z00_4878; 
BgL_bufposz00_18750 = BgL_arg3529z00_4879; 
BgL_bufposz00_4874 = BgL_bufposz00_18750; 
BgL_forwardz00_4873 = BgL_forwardz00_18749; 
goto BgL_zc3z04anonymousza33525ze3z87_4875;} }  else 
{ /* Llib/date.scm 906 */
BgL_matchz00_5046 = BgL_lastzd2matchzd2_4872; } }  else 
{ /* Llib/date.scm 906 */
 int BgL_curz00_4880;
BgL_curz00_4880 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4871, BgL_forwardz00_4873); 
{ /* Llib/date.scm 906 */

{ /* Llib/date.scm 906 */
 bool_t BgL_test7285z00_18752;
if(
(
(long)(BgL_curz00_4880)==32L))
{ /* Llib/date.scm 906 */
BgL_test7285z00_18752 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 906 */
BgL_test7285z00_18752 = 
(
(long)(BgL_curz00_4880)==84L)
; } 
if(BgL_test7285z00_18752)
{ /* Llib/date.scm 906 */
BgL_iportz00_4892 = BgL_iportz00_4871; 
BgL_lastzd2matchzd2_4893 = BgL_lastzd2matchzd2_4872; 
BgL_forwardz00_4894 = 
(1L+BgL_forwardz00_4873); 
BgL_bufposz00_4895 = BgL_bufposz00_4874; 
BgL_zc3z04anonymousza33535ze3z87_4896:
{ /* Llib/date.scm 906 */
 long BgL_newzd2matchzd2_4897;
RGC_STOP_MATCH(BgL_iportz00_4892, BgL_forwardz00_4894); 
BgL_newzd2matchzd2_4897 = 1L; 
if(
(BgL_forwardz00_4894==BgL_bufposz00_4895))
{ /* Llib/date.scm 906 */
if(
rgc_fill_buffer(BgL_iportz00_4892))
{ /* Llib/date.scm 906 */
 long BgL_arg3538z00_4900; long BgL_arg3539z00_4901;
BgL_arg3538z00_4900 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4892); 
BgL_arg3539z00_4901 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4892); 
{ 
 long BgL_bufposz00_18766; long BgL_forwardz00_18765;
BgL_forwardz00_18765 = BgL_arg3538z00_4900; 
BgL_bufposz00_18766 = BgL_arg3539z00_4901; 
BgL_bufposz00_4895 = BgL_bufposz00_18766; 
BgL_forwardz00_4894 = BgL_forwardz00_18765; 
goto BgL_zc3z04anonymousza33535ze3z87_4896;} }  else 
{ /* Llib/date.scm 906 */
BgL_matchz00_5046 = BgL_newzd2matchzd2_4897; } }  else 
{ /* Llib/date.scm 906 */
 int BgL_curz00_4902;
BgL_curz00_4902 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4892, BgL_forwardz00_4894); 
{ /* Llib/date.scm 906 */

{ /* Llib/date.scm 906 */
 bool_t BgL_test7289z00_18768;
if(
(
(long)(BgL_curz00_4902)>=48L))
{ /* Llib/date.scm 906 */
BgL_test7289z00_18768 = 
(
(long)(BgL_curz00_4902)<58L)
; }  else 
{ /* Llib/date.scm 906 */
BgL_test7289z00_18768 = ((bool_t)0)
; } 
if(BgL_test7289z00_18768)
{ /* Llib/date.scm 906 */
BgL_iportz00_4907 = BgL_iportz00_4892; 
BgL_lastzd2matchzd2_4908 = BgL_newzd2matchzd2_4897; 
BgL_forwardz00_4909 = 
(1L+BgL_forwardz00_4894); 
BgL_bufposz00_4910 = BgL_bufposz00_4895; 
BgL_zc3z04anonymousza33543ze3z87_4911:
if(
(BgL_forwardz00_4909==BgL_bufposz00_4910))
{ /* Llib/date.scm 906 */
if(
rgc_fill_buffer(BgL_iportz00_4907))
{ /* Llib/date.scm 906 */
 long BgL_arg3546z00_4914; long BgL_arg3548z00_4915;
BgL_arg3546z00_4914 = 
RGC_BUFFER_FORWARD(BgL_iportz00_4907); 
BgL_arg3548z00_4915 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_4907); 
{ 
 long BgL_bufposz00_18781; long BgL_forwardz00_18780;
BgL_forwardz00_18780 = BgL_arg3546z00_4914; 
BgL_bufposz00_18781 = BgL_arg3548z00_4915; 
BgL_bufposz00_4910 = BgL_bufposz00_18781; 
BgL_forwardz00_4909 = BgL_forwardz00_18780; 
goto BgL_zc3z04anonymousza33543ze3z87_4911;} }  else 
{ /* Llib/date.scm 906 */
BgL_matchz00_5046 = BgL_lastzd2matchzd2_4908; } }  else 
{ /* Llib/date.scm 906 */
 int BgL_curz00_4916;
BgL_curz00_4916 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_4907, BgL_forwardz00_4909); 
{ /* Llib/date.scm 906 */

{ /* Llib/date.scm 906 */
 bool_t BgL_test7293z00_18783;
if(
(
(long)(BgL_curz00_4916)>=48L))
{ /* Llib/date.scm 906 */
BgL_test7293z00_18783 = 
(
(long)(BgL_curz00_4916)<58L)
; }  else 
{ /* Llib/date.scm 906 */
BgL_test7293z00_18783 = ((bool_t)0)
; } 
if(BgL_test7293z00_18783)
{ /* Llib/date.scm 906 */
 long BgL_arg3551z00_4919;
BgL_arg3551z00_4919 = 
(1L+BgL_forwardz00_4909); 
{ /* Llib/date.scm 906 */
 long BgL_newzd2matchzd2_9160;
RGC_STOP_MATCH(BgL_iportz00_4907, BgL_arg3551z00_4919); 
BgL_newzd2matchzd2_9160 = 0L; 
BgL_matchz00_5046 = BgL_newzd2matchzd2_9160; } }  else 
{ /* Llib/date.scm 906 */
BgL_matchz00_5046 = BgL_lastzd2matchzd2_4908; } } } } }  else 
{ /* Llib/date.scm 906 */
BgL_matchz00_5046 = BgL_newzd2matchzd2_4897; } } } } } }  else 
{ /* Llib/date.scm 906 */
 long BgL_arg3533z00_4884;
BgL_arg3533z00_4884 = 
(1L+BgL_forwardz00_4873); 
{ /* Llib/date.scm 906 */
 long BgL_newzd2matchzd2_9132;
RGC_STOP_MATCH(BgL_iportz00_4871, BgL_arg3533z00_4884); 
BgL_newzd2matchzd2_9132 = 1L; 
BgL_matchz00_5046 = BgL_newzd2matchzd2_9132; } } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_4826); 
switch( BgL_matchz00_5046) { case 1L : 

{ /* Llib/date.scm 914 */
 bool_t BgL_test7295z00_18796;
{ /* Llib/date.scm 914 */
 obj_t BgL_tmpz00_18797;
BgL_tmpz00_18797 = 
BGl_thezd2failureze75z35zz__datez00(BgL_iportz00_4826); 
BgL_test7295z00_18796 = 
EOF_OBJECTP(BgL_tmpz00_18797); } 
if(BgL_test7295z00_18796)
{ /* Llib/date.scm 914 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_18802;
{ /* Llib/date.scm 915 */
 obj_t BgL_aux5726z00_10835;
BgL_aux5726z00_10835 = BFALSE; 
{ 
 obj_t BgL_auxz00_18809;
BgL_auxz00_18809 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(39101L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5726z00_10835); 
FAILURE(BgL_auxz00_18809,BFALSE,BFALSE);} } 
BgL_tmpz00_18802 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(1L), 
(int)(BgL_ddz00_4829), 
(int)(BgL_mmz00_4828), 
(int)(BgL_yyyyz00_4827), BgL_tmpz00_18802, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(1L), 
(int)(BgL_ddz00_4829), 
(int)(BgL_mmz00_4828), 
(int)(BgL_yyyyz00_4827), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 914 */
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, 
BGl_thezd2failureze75z35zz__datez00(BgL_iportz00_4826), BgL_iportz00_4826); } } break;case 0L : 

{ /* Llib/date.scm 909 */
 long BgL_b1z00_5063; long BgL_b2z00_5064;
{ /* Llib/date.scm 909 */
 int BgL_arg3658z00_5068;
{ /* Llib/date.scm 906 */
 int BgL_tmpz00_18825;
BgL_tmpz00_18825 = 
(int)(1L); 
BgL_arg3658z00_5068 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_4826, BgL_tmpz00_18825); } 
BgL_b1z00_5063 = 
(
(long)(BgL_arg3658z00_5068)-48L); } 
{ /* Llib/date.scm 910 */
 int BgL_arg3659z00_5069;
{ /* Llib/date.scm 906 */
 int BgL_tmpz00_18830;
BgL_tmpz00_18830 = 
(int)(2L); 
BgL_arg3659z00_5069 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_4826, BgL_tmpz00_18830); } 
BgL_b2z00_5064 = 
(
(long)(BgL_arg3659z00_5069)-48L); } 
BgL_iportz00_5106 = BgL_iportz00_4826; 
BgL_yyyyz00_5107 = BgL_yyyyz00_4827; 
BgL_mmz00_5108 = BgL_mmz00_4828; 
BgL_ddz00_5109 = BgL_ddz00_4829; 
BgL_hhz00_5110 = 
(
(BgL_b1z00_5063*10L)+BgL_b2z00_5064); 
{ 
 obj_t BgL_iportz00_5152; long BgL_lastzd2matchzd2_5153; long BgL_forwardz00_5154; long BgL_bufposz00_5155; obj_t BgL_iportz00_5171; long BgL_lastzd2matchzd2_5172; long BgL_forwardz00_5173; long BgL_bufposz00_5174; obj_t BgL_iportz00_5186; long BgL_lastzd2matchzd2_5187; long BgL_forwardz00_5188; long BgL_bufposz00_5189;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_5106))
{ /* Llib/date.scm 892 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg3664z00_5145;
{ /* Llib/date.scm 892 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_5146;
{ /* Llib/date.scm 892 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_5149;
BgL_new1077z00_5149 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 892 */
 long BgL_arg3669z00_5150;
BgL_arg3669z00_5150 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_5149), BgL_arg3669z00_5150); } 
BgL_new1078z00_5146 = BgL_new1077z00_5149; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5146)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5146)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_18845;
{ /* Llib/date.scm 892 */
 obj_t BgL_arg3665z00_5147;
{ /* Llib/date.scm 892 */
 obj_t BgL_arg3668z00_5148;
{ /* Llib/date.scm 892 */
 obj_t BgL_classz00_9118;
BgL_classz00_9118 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg3668z00_5148 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9118); } 
BgL_arg3665z00_5147 = 
VECTOR_REF(BgL_arg3668z00_5148,2L); } 
{ /* Llib/date.scm 892 */
 obj_t BgL_auxz00_18849;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg3665z00_5147))
{ /* Llib/date.scm 892 */
BgL_auxz00_18849 = BgL_arg3665z00_5147
; }  else 
{ 
 obj_t BgL_auxz00_18852;
BgL_auxz00_18852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(38381L), BGl_string6018z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg3665z00_5147); 
FAILURE(BgL_auxz00_18852,BFALSE,BFALSE);} 
BgL_auxz00_18845 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_18849); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5146)))->BgL_stackz00)=((obj_t)BgL_auxz00_18845),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5146)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5146)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5146)))->BgL_objz00)=((obj_t)BgL_iportz00_5106),BUNSPEC); 
BgL_arg3664z00_5145 = BgL_new1078z00_5146; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg3664z00_5145)); }  else 
{ /* Llib/date.scm 892 */
RGC_START_MATCH(BgL_iportz00_5106); 
{ /* Llib/date.scm 892 */
 long BgL_matchz00_5325;
{ /* Llib/date.scm 892 */
 long BgL_arg3805z00_5349; long BgL_arg3806z00_5350;
BgL_arg3805z00_5349 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5106); 
BgL_arg3806z00_5350 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5106); 
BgL_iportz00_5152 = BgL_iportz00_5106; 
BgL_lastzd2matchzd2_5153 = 1L; 
BgL_forwardz00_5154 = BgL_arg3805z00_5349; 
BgL_bufposz00_5155 = BgL_arg3806z00_5350; 
BgL_zc3z04anonymousza33672ze3z87_5156:
if(
(BgL_forwardz00_5154==BgL_bufposz00_5155))
{ /* Llib/date.scm 892 */
if(
rgc_fill_buffer(BgL_iportz00_5152))
{ /* Llib/date.scm 892 */
 long BgL_arg3675z00_5159; long BgL_arg3677z00_5160;
BgL_arg3675z00_5159 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5152); 
BgL_arg3677z00_5160 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5152); 
{ 
 long BgL_bufposz00_18876; long BgL_forwardz00_18875;
BgL_forwardz00_18875 = BgL_arg3675z00_5159; 
BgL_bufposz00_18876 = BgL_arg3677z00_5160; 
BgL_bufposz00_5155 = BgL_bufposz00_18876; 
BgL_forwardz00_5154 = BgL_forwardz00_18875; 
goto BgL_zc3z04anonymousza33672ze3z87_5156;} }  else 
{ /* Llib/date.scm 892 */
BgL_matchz00_5325 = BgL_lastzd2matchzd2_5153; } }  else 
{ /* Llib/date.scm 892 */
 int BgL_curz00_5161;
BgL_curz00_5161 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5152, BgL_forwardz00_5154); 
{ /* Llib/date.scm 892 */

if(
(
(long)(BgL_curz00_5161)==58L))
{ /* Llib/date.scm 892 */
BgL_iportz00_5171 = BgL_iportz00_5152; 
BgL_lastzd2matchzd2_5172 = BgL_lastzd2matchzd2_5153; 
BgL_forwardz00_5173 = 
(1L+BgL_forwardz00_5154); 
BgL_bufposz00_5174 = BgL_bufposz00_5155; 
BgL_zc3z04anonymousza33683ze3z87_5175:
{ /* Llib/date.scm 892 */
 long BgL_newzd2matchzd2_5176;
RGC_STOP_MATCH(BgL_iportz00_5171, BgL_forwardz00_5173); 
BgL_newzd2matchzd2_5176 = 1L; 
if(
(BgL_forwardz00_5173==BgL_bufposz00_5174))
{ /* Llib/date.scm 892 */
if(
rgc_fill_buffer(BgL_iportz00_5171))
{ /* Llib/date.scm 892 */
 long BgL_arg3686z00_5179; long BgL_arg3687z00_5180;
BgL_arg3686z00_5179 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5171); 
BgL_arg3687z00_5180 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5171); 
{ 
 long BgL_bufposz00_18889; long BgL_forwardz00_18888;
BgL_forwardz00_18888 = BgL_arg3686z00_5179; 
BgL_bufposz00_18889 = BgL_arg3687z00_5180; 
BgL_bufposz00_5174 = BgL_bufposz00_18889; 
BgL_forwardz00_5173 = BgL_forwardz00_18888; 
goto BgL_zc3z04anonymousza33683ze3z87_5175;} }  else 
{ /* Llib/date.scm 892 */
BgL_matchz00_5325 = BgL_newzd2matchzd2_5176; } }  else 
{ /* Llib/date.scm 892 */
 int BgL_curz00_5181;
BgL_curz00_5181 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5171, BgL_forwardz00_5173); 
{ /* Llib/date.scm 892 */

{ /* Llib/date.scm 892 */
 bool_t BgL_test7304z00_18891;
if(
(
(long)(BgL_curz00_5181)>=48L))
{ /* Llib/date.scm 892 */
BgL_test7304z00_18891 = 
(
(long)(BgL_curz00_5181)<58L)
; }  else 
{ /* Llib/date.scm 892 */
BgL_test7304z00_18891 = ((bool_t)0)
; } 
if(BgL_test7304z00_18891)
{ /* Llib/date.scm 892 */
BgL_iportz00_5186 = BgL_iportz00_5171; 
BgL_lastzd2matchzd2_5187 = BgL_newzd2matchzd2_5176; 
BgL_forwardz00_5188 = 
(1L+BgL_forwardz00_5173); 
BgL_bufposz00_5189 = BgL_bufposz00_5174; 
BgL_zc3z04anonymousza33691ze3z87_5190:
if(
(BgL_forwardz00_5188==BgL_bufposz00_5189))
{ /* Llib/date.scm 892 */
if(
rgc_fill_buffer(BgL_iportz00_5186))
{ /* Llib/date.scm 892 */
 long BgL_arg3694z00_5193; long BgL_arg3695z00_5194;
BgL_arg3694z00_5193 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5186); 
BgL_arg3695z00_5194 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5186); 
{ 
 long BgL_bufposz00_18904; long BgL_forwardz00_18903;
BgL_forwardz00_18903 = BgL_arg3694z00_5193; 
BgL_bufposz00_18904 = BgL_arg3695z00_5194; 
BgL_bufposz00_5189 = BgL_bufposz00_18904; 
BgL_forwardz00_5188 = BgL_forwardz00_18903; 
goto BgL_zc3z04anonymousza33691ze3z87_5190;} }  else 
{ /* Llib/date.scm 892 */
BgL_matchz00_5325 = BgL_lastzd2matchzd2_5187; } }  else 
{ /* Llib/date.scm 892 */
 int BgL_curz00_5195;
BgL_curz00_5195 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5186, BgL_forwardz00_5188); 
{ /* Llib/date.scm 892 */

{ /* Llib/date.scm 892 */
 bool_t BgL_test7308z00_18906;
if(
(
(long)(BgL_curz00_5195)>=48L))
{ /* Llib/date.scm 892 */
BgL_test7308z00_18906 = 
(
(long)(BgL_curz00_5195)<58L)
; }  else 
{ /* Llib/date.scm 892 */
BgL_test7308z00_18906 = ((bool_t)0)
; } 
if(BgL_test7308z00_18906)
{ /* Llib/date.scm 892 */
 long BgL_arg3699z00_5198;
BgL_arg3699z00_5198 = 
(1L+BgL_forwardz00_5188); 
{ /* Llib/date.scm 892 */
 long BgL_newzd2matchzd2_9088;
RGC_STOP_MATCH(BgL_iportz00_5186, BgL_arg3699z00_5198); 
BgL_newzd2matchzd2_9088 = 0L; 
BgL_matchz00_5325 = BgL_newzd2matchzd2_9088; } }  else 
{ /* Llib/date.scm 892 */
BgL_matchz00_5325 = BgL_lastzd2matchzd2_5187; } } } } }  else 
{ /* Llib/date.scm 892 */
BgL_matchz00_5325 = BgL_newzd2matchzd2_5176; } } } } } }  else 
{ /* Llib/date.scm 892 */
 long BgL_arg3681z00_5164;
BgL_arg3681z00_5164 = 
(1L+BgL_forwardz00_5154); 
{ /* Llib/date.scm 892 */
 long BgL_newzd2matchzd2_9060;
RGC_STOP_MATCH(BgL_iportz00_5152, BgL_arg3681z00_5164); 
BgL_newzd2matchzd2_9060 = 1L; 
BgL_matchz00_5325 = BgL_newzd2matchzd2_9060; } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_5106); 
switch( BgL_matchz00_5325) { case 1L : 

{ /* Llib/date.scm 899 */
 bool_t BgL_test7310z00_18919;
{ /* Llib/date.scm 899 */
 obj_t BgL_tmpz00_18920;
BgL_tmpz00_18920 = 
BGl_thezd2failureze74z35zz__datez00(BgL_iportz00_5106); 
BgL_test7310z00_18919 = 
EOF_OBJECTP(BgL_tmpz00_18920); } 
if(BgL_test7310z00_18919)
{ /* Llib/date.scm 899 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_18925;
{ /* Llib/date.scm 900 */
 obj_t BgL_aux5723z00_10832;
BgL_aux5723z00_10832 = BFALSE; 
{ 
 obj_t BgL_auxz00_18932;
BgL_auxz00_18932 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(38617L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5723z00_10832); 
FAILURE(BgL_auxz00_18932,BFALSE,BFALSE);} } 
BgL_tmpz00_18925 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(BgL_hhz00_5110), 
(int)(BgL_ddz00_5109), 
(int)(BgL_mmz00_5108), 
(int)(BgL_yyyyz00_5107), BgL_tmpz00_18925, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(0L), 
(int)(BgL_hhz00_5110), 
(int)(BgL_ddz00_5109), 
(int)(BgL_mmz00_5108), 
(int)(BgL_yyyyz00_5107), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 899 */
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, 
BGl_thezd2failureze74z35zz__datez00(BgL_iportz00_5106), BgL_iportz00_5106); } } break;case 0L : 

{ /* Llib/date.scm 894 */
 long BgL_b1z00_5342; long BgL_b2z00_5343;
{ /* Llib/date.scm 894 */
 int BgL_arg3803z00_5347;
{ /* Llib/date.scm 892 */
 int BgL_tmpz00_18948;
BgL_tmpz00_18948 = 
(int)(1L); 
BgL_arg3803z00_5347 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5106, BgL_tmpz00_18948); } 
BgL_b1z00_5342 = 
(
(long)(BgL_arg3803z00_5347)-48L); } 
{ /* Llib/date.scm 895 */
 int BgL_arg3804z00_5348;
{ /* Llib/date.scm 892 */
 int BgL_tmpz00_18953;
BgL_tmpz00_18953 = 
(int)(2L); 
BgL_arg3804z00_5348 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5106, BgL_tmpz00_18953); } 
BgL_b2z00_5343 = 
(
(long)(BgL_arg3804z00_5348)-48L); } 
BgL_iportz00_5385 = BgL_iportz00_5106; 
BgL_yyyyz00_5386 = BgL_yyyyz00_5107; 
BgL_mmz00_5387 = BgL_mmz00_5108; 
BgL_ddz00_5388 = BgL_ddz00_5109; 
BgL_hhz00_5389 = BgL_hhz00_5110; 
BgL_mmz00_5390 = 
(
(BgL_b1z00_5342*10L)+BgL_b2z00_5343); 
{ 
 obj_t BgL_iportz00_5432; long BgL_lastzd2matchzd2_5433; long BgL_forwardz00_5434; long BgL_bufposz00_5435; obj_t BgL_iportz00_5451; long BgL_lastzd2matchzd2_5452; long BgL_forwardz00_5453; long BgL_bufposz00_5454; obj_t BgL_iportz00_5466; long BgL_lastzd2matchzd2_5467; long BgL_forwardz00_5468; long BgL_bufposz00_5469;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_5385))
{ /* Llib/date.scm 876 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg3808z00_5425;
{ /* Llib/date.scm 876 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_5426;
{ /* Llib/date.scm 876 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_5429;
BgL_new1077z00_5429 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 876 */
 long BgL_arg3811z00_5430;
BgL_arg3811z00_5430 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_5429), BgL_arg3811z00_5430); } 
BgL_new1078z00_5426 = BgL_new1077z00_5429; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5426)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5426)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_18968;
{ /* Llib/date.scm 876 */
 obj_t BgL_arg3809z00_5427;
{ /* Llib/date.scm 876 */
 obj_t BgL_arg3810z00_5428;
{ /* Llib/date.scm 876 */
 obj_t BgL_classz00_9047;
BgL_classz00_9047 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg3810z00_5428 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9047); } 
BgL_arg3809z00_5427 = 
VECTOR_REF(BgL_arg3810z00_5428,2L); } 
{ /* Llib/date.scm 876 */
 obj_t BgL_auxz00_18972;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg3809z00_5427))
{ /* Llib/date.scm 876 */
BgL_auxz00_18972 = BgL_arg3809z00_5427
; }  else 
{ 
 obj_t BgL_auxz00_18975;
BgL_auxz00_18975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(37906L), BGl_string6017z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg3809z00_5427); 
FAILURE(BgL_auxz00_18975,BFALSE,BFALSE);} 
BgL_auxz00_18968 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_18972); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5426)))->BgL_stackz00)=((obj_t)BgL_auxz00_18968),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5426)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5426)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5426)))->BgL_objz00)=((obj_t)BgL_iportz00_5385),BUNSPEC); 
BgL_arg3808z00_5425 = BgL_new1078z00_5426; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg3808z00_5425)); }  else 
{ /* Llib/date.scm 876 */
RGC_START_MATCH(BgL_iportz00_5385); 
{ /* Llib/date.scm 876 */
 long BgL_matchz00_5605;
{ /* Llib/date.scm 876 */
 long BgL_arg3937z00_5630; long BgL_arg3938z00_5631;
BgL_arg3937z00_5630 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5385); 
BgL_arg3938z00_5631 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5385); 
BgL_iportz00_5432 = BgL_iportz00_5385; 
BgL_lastzd2matchzd2_5433 = 1L; 
BgL_forwardz00_5434 = BgL_arg3937z00_5630; 
BgL_bufposz00_5435 = BgL_arg3938z00_5631; 
BgL_zc3z04anonymousza33813ze3z87_5436:
if(
(BgL_forwardz00_5434==BgL_bufposz00_5435))
{ /* Llib/date.scm 876 */
if(
rgc_fill_buffer(BgL_iportz00_5432))
{ /* Llib/date.scm 876 */
 long BgL_arg3816z00_5439; long BgL_arg3817z00_5440;
BgL_arg3816z00_5439 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5432); 
BgL_arg3817z00_5440 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5432); 
{ 
 long BgL_bufposz00_18999; long BgL_forwardz00_18998;
BgL_forwardz00_18998 = BgL_arg3816z00_5439; 
BgL_bufposz00_18999 = BgL_arg3817z00_5440; 
BgL_bufposz00_5435 = BgL_bufposz00_18999; 
BgL_forwardz00_5434 = BgL_forwardz00_18998; 
goto BgL_zc3z04anonymousza33813ze3z87_5436;} }  else 
{ /* Llib/date.scm 876 */
BgL_matchz00_5605 = BgL_lastzd2matchzd2_5433; } }  else 
{ /* Llib/date.scm 876 */
 int BgL_curz00_5441;
BgL_curz00_5441 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5432, BgL_forwardz00_5434); 
{ /* Llib/date.scm 876 */

if(
(
(long)(BgL_curz00_5441)==58L))
{ /* Llib/date.scm 876 */
BgL_iportz00_5451 = BgL_iportz00_5432; 
BgL_lastzd2matchzd2_5452 = BgL_lastzd2matchzd2_5433; 
BgL_forwardz00_5453 = 
(1L+BgL_forwardz00_5434); 
BgL_bufposz00_5454 = BgL_bufposz00_5435; 
BgL_zc3z04anonymousza33822ze3z87_5455:
{ /* Llib/date.scm 876 */
 long BgL_newzd2matchzd2_5456;
RGC_STOP_MATCH(BgL_iportz00_5451, BgL_forwardz00_5453); 
BgL_newzd2matchzd2_5456 = 1L; 
if(
(BgL_forwardz00_5453==BgL_bufposz00_5454))
{ /* Llib/date.scm 876 */
if(
rgc_fill_buffer(BgL_iportz00_5451))
{ /* Llib/date.scm 876 */
 long BgL_arg3825z00_5459; long BgL_arg3826z00_5460;
BgL_arg3825z00_5459 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5451); 
BgL_arg3826z00_5460 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5451); 
{ 
 long BgL_bufposz00_19012; long BgL_forwardz00_19011;
BgL_forwardz00_19011 = BgL_arg3825z00_5459; 
BgL_bufposz00_19012 = BgL_arg3826z00_5460; 
BgL_bufposz00_5454 = BgL_bufposz00_19012; 
BgL_forwardz00_5453 = BgL_forwardz00_19011; 
goto BgL_zc3z04anonymousza33822ze3z87_5455;} }  else 
{ /* Llib/date.scm 876 */
BgL_matchz00_5605 = BgL_newzd2matchzd2_5456; } }  else 
{ /* Llib/date.scm 876 */
 int BgL_curz00_5461;
BgL_curz00_5461 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5451, BgL_forwardz00_5453); 
{ /* Llib/date.scm 876 */

{ /* Llib/date.scm 876 */
 bool_t BgL_test7319z00_19014;
if(
(
(long)(BgL_curz00_5461)>=48L))
{ /* Llib/date.scm 876 */
BgL_test7319z00_19014 = 
(
(long)(BgL_curz00_5461)<58L)
; }  else 
{ /* Llib/date.scm 876 */
BgL_test7319z00_19014 = ((bool_t)0)
; } 
if(BgL_test7319z00_19014)
{ /* Llib/date.scm 876 */
BgL_iportz00_5466 = BgL_iportz00_5451; 
BgL_lastzd2matchzd2_5467 = BgL_newzd2matchzd2_5456; 
BgL_forwardz00_5468 = 
(1L+BgL_forwardz00_5453); 
BgL_bufposz00_5469 = BgL_bufposz00_5454; 
BgL_zc3z04anonymousza33830ze3z87_5470:
if(
(BgL_forwardz00_5468==BgL_bufposz00_5469))
{ /* Llib/date.scm 876 */
if(
rgc_fill_buffer(BgL_iportz00_5466))
{ /* Llib/date.scm 876 */
 long BgL_arg3833z00_5473; long BgL_arg3834z00_5474;
BgL_arg3833z00_5473 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5466); 
BgL_arg3834z00_5474 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5466); 
{ 
 long BgL_bufposz00_19027; long BgL_forwardz00_19026;
BgL_forwardz00_19026 = BgL_arg3833z00_5473; 
BgL_bufposz00_19027 = BgL_arg3834z00_5474; 
BgL_bufposz00_5469 = BgL_bufposz00_19027; 
BgL_forwardz00_5468 = BgL_forwardz00_19026; 
goto BgL_zc3z04anonymousza33830ze3z87_5470;} }  else 
{ /* Llib/date.scm 876 */
BgL_matchz00_5605 = BgL_lastzd2matchzd2_5467; } }  else 
{ /* Llib/date.scm 876 */
 int BgL_curz00_5475;
BgL_curz00_5475 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5466, BgL_forwardz00_5468); 
{ /* Llib/date.scm 876 */

{ /* Llib/date.scm 876 */
 bool_t BgL_test7323z00_19029;
if(
(
(long)(BgL_curz00_5475)>=48L))
{ /* Llib/date.scm 876 */
BgL_test7323z00_19029 = 
(
(long)(BgL_curz00_5475)<58L)
; }  else 
{ /* Llib/date.scm 876 */
BgL_test7323z00_19029 = ((bool_t)0)
; } 
if(BgL_test7323z00_19029)
{ /* Llib/date.scm 876 */
 long BgL_arg3837z00_5478;
BgL_arg3837z00_5478 = 
(1L+BgL_forwardz00_5468); 
{ /* Llib/date.scm 876 */
 long BgL_newzd2matchzd2_9016;
RGC_STOP_MATCH(BgL_iportz00_5466, BgL_arg3837z00_5478); 
BgL_newzd2matchzd2_9016 = 0L; 
BgL_matchz00_5605 = BgL_newzd2matchzd2_9016; } }  else 
{ /* Llib/date.scm 876 */
BgL_matchz00_5605 = BgL_lastzd2matchzd2_5467; } } } } }  else 
{ /* Llib/date.scm 876 */
BgL_matchz00_5605 = BgL_newzd2matchzd2_5456; } } } } } }  else 
{ /* Llib/date.scm 876 */
 long BgL_arg3820z00_5444;
BgL_arg3820z00_5444 = 
(1L+BgL_forwardz00_5434); 
{ /* Llib/date.scm 876 */
 long BgL_newzd2matchzd2_8988;
RGC_STOP_MATCH(BgL_iportz00_5432, BgL_arg3820z00_5444); 
BgL_newzd2matchzd2_8988 = 1L; 
BgL_matchz00_5605 = BgL_newzd2matchzd2_8988; } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_5385); 
switch( BgL_matchz00_5605) { case 1L : 

{ /* Llib/date.scm 883 */
 bool_t BgL_test7325z00_19042;
{ /* Llib/date.scm 883 */
 obj_t BgL_tmpz00_19043;
BgL_tmpz00_19043 = 
BGl_thezd2failureze73z35zz__datez00(BgL_iportz00_5385); 
BgL_test7325z00_19042 = 
EOF_OBJECTP(BgL_tmpz00_19043); } 
if(BgL_test7325z00_19042)
{ /* Llib/date.scm 883 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_19048;
{ /* Llib/date.scm 884 */
 obj_t BgL_aux5718z00_10827;
BgL_aux5718z00_10827 = BFALSE; 
{ 
 obj_t BgL_auxz00_19055;
BgL_auxz00_19055 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(38149L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5718z00_10827); 
FAILURE(BgL_auxz00_19055,BFALSE,BFALSE);} } 
BgL_tmpz00_19048 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(BgL_mmz00_5390), 
(int)(BgL_hhz00_5389), 
(int)(BgL_ddz00_5388), 
(int)(BgL_mmz00_5387), 
(int)(BgL_yyyyz00_5386), BgL_tmpz00_19048, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(0L), 
(int)(BgL_mmz00_5390), 
(int)(BgL_hhz00_5389), 
(int)(BgL_ddz00_5388), 
(int)(BgL_mmz00_5387), 
(int)(BgL_yyyyz00_5386), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 883 */
{ /* Llib/date.scm 887 */
 obj_t BgL_arg3928z00_5619;
BgL_arg3928z00_5619 = 
BGl_thezd2failureze73z35zz__datez00(BgL_iportz00_5385); 
{ /* Llib/date.scm 887 */
 unsigned char BgL_auxz00_19070;
{ /* Llib/date.scm 887 */
 obj_t BgL_tmpz00_19071;
if(
CHARP(BgL_arg3928z00_5619))
{ /* Llib/date.scm 887 */
BgL_tmpz00_19071 = BgL_arg3928z00_5619
; }  else 
{ 
 obj_t BgL_auxz00_19074;
BgL_auxz00_19074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(38254L), BGl_string5785z00zz__datez00, BGl_string6015z00zz__datez00, BgL_arg3928z00_5619); 
FAILURE(BgL_auxz00_19074,BFALSE,BFALSE);} 
BgL_auxz00_19070 = 
CCHAR(BgL_tmpz00_19071); } 
BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_19070, BgL_iportz00_5385); } } 
BgL_iportz00_5970 = BgL_iportz00_5385; 
BgL_yyyyz00_5971 = BgL_yyyyz00_5386; 
BgL_mmz00_5972 = BgL_mmz00_5387; 
BgL_ddz00_5973 = BgL_ddz00_5388; 
BgL_hhz00_5974 = BgL_hhz00_5389; 
BgL_mmz00_5975 = BgL_mmz00_5390; 
BgL_ssz00_5976 = 0L; 
BgL_sssz00_5977 = ((BGL_LONGLONG_T)0); 
BgL_zc3z04anonymousza34084ze3z87_5978:
{ 
 obj_t BgL_iportz00_6023; long BgL_lastzd2matchzd2_6024; long BgL_forwardz00_6025; long BgL_bufposz00_6026; obj_t BgL_iportz00_6054; long BgL_lastzd2matchzd2_6055; long BgL_forwardz00_6056; long BgL_bufposz00_6057; obj_t BgL_iportz00_6069; long BgL_lastzd2matchzd2_6070; long BgL_forwardz00_6071; long BgL_bufposz00_6072; obj_t BgL_iportz00_6084; long BgL_lastzd2matchzd2_6085; long BgL_forwardz00_6086; long BgL_bufposz00_6087; obj_t BgL_iportz00_6104; long BgL_lastzd2matchzd2_6105; long BgL_forwardz00_6106; long BgL_bufposz00_6107;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_5970))
{ /* Llib/date.scm 835 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg4085z00_6016;
{ /* Llib/date.scm 835 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_6017;
{ /* Llib/date.scm 835 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_6020;
BgL_new1077z00_6020 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 835 */
 long BgL_arg4088z00_6021;
BgL_arg4088z00_6021 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_6020), BgL_arg4088z00_6021); } 
BgL_new1078z00_6017 = BgL_new1077z00_6020; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_6017)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_6017)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_19090;
{ /* Llib/date.scm 835 */
 obj_t BgL_arg4086z00_6018;
{ /* Llib/date.scm 835 */
 obj_t BgL_arg4087z00_6019;
{ /* Llib/date.scm 835 */
 obj_t BgL_classz00_8885;
BgL_classz00_8885 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg4087z00_6019 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_8885); } 
BgL_arg4086z00_6018 = 
VECTOR_REF(BgL_arg4087z00_6019,2L); } 
{ /* Llib/date.scm 835 */
 obj_t BgL_auxz00_19094;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg4086z00_6018))
{ /* Llib/date.scm 835 */
BgL_auxz00_19094 = BgL_arg4086z00_6018
; }  else 
{ 
 obj_t BgL_auxz00_19097;
BgL_auxz00_19097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(36574L), BGl_string6014z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg4086z00_6018); 
FAILURE(BgL_auxz00_19097,BFALSE,BFALSE);} 
BgL_auxz00_19090 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_19094); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_6017)))->BgL_stackz00)=((obj_t)BgL_auxz00_19090),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_6017)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_6017)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_6017)))->BgL_objz00)=((obj_t)BgL_iportz00_5970),BUNSPEC); 
BgL_arg4085z00_6016 = BgL_new1078z00_6017; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg4085z00_6016)); }  else 
{ /* Llib/date.scm 835 */
RGC_START_MATCH(BgL_iportz00_5970); 
{ /* Llib/date.scm 835 */
 long BgL_matchz00_6243;
{ /* Llib/date.scm 835 */
 long BgL_arg4244z00_6286; long BgL_arg4245z00_6287;
BgL_arg4244z00_6286 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5970); 
BgL_arg4245z00_6287 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5970); 
BgL_iportz00_6023 = BgL_iportz00_5970; 
BgL_lastzd2matchzd2_6024 = 3L; 
BgL_forwardz00_6025 = BgL_arg4244z00_6286; 
BgL_bufposz00_6026 = BgL_arg4245z00_6287; 
BgL_zc3z04anonymousza34090ze3z87_6027:
if(
(BgL_forwardz00_6025==BgL_bufposz00_6026))
{ /* Llib/date.scm 835 */
if(
rgc_fill_buffer(BgL_iportz00_6023))
{ /* Llib/date.scm 835 */
 long BgL_arg4093z00_6030; long BgL_arg4094z00_6031;
BgL_arg4093z00_6030 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6023); 
BgL_arg4094z00_6031 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6023); 
{ 
 long BgL_bufposz00_19121; long BgL_forwardz00_19120;
BgL_forwardz00_19120 = BgL_arg4093z00_6030; 
BgL_bufposz00_19121 = BgL_arg4094z00_6031; 
BgL_bufposz00_6026 = BgL_bufposz00_19121; 
BgL_forwardz00_6025 = BgL_forwardz00_19120; 
goto BgL_zc3z04anonymousza34090ze3z87_6027;} }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_lastzd2matchzd2_6024; } }  else 
{ /* Llib/date.scm 835 */
 int BgL_curz00_6032;
BgL_curz00_6032 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6023, BgL_forwardz00_6025); 
{ /* Llib/date.scm 835 */

{ /* Llib/date.scm 835 */
 bool_t BgL_test7332z00_19123;
if(
(
(long)(BgL_curz00_6032)==90L))
{ /* Llib/date.scm 835 */
BgL_test7332z00_19123 = ((bool_t)1)
; }  else 
{ /* Llib/date.scm 835 */
BgL_test7332z00_19123 = 
(
(long)(BgL_curz00_6032)==122L)
; } 
if(BgL_test7332z00_19123)
{ /* Llib/date.scm 835 */
 long BgL_arg4097z00_6035;
BgL_arg4097z00_6035 = 
(1L+BgL_forwardz00_6025); 
{ /* Llib/date.scm 835 */
 long BgL_newzd2matchzd2_8776;
RGC_STOP_MATCH(BgL_iportz00_6023, BgL_arg4097z00_6035); 
BgL_newzd2matchzd2_8776 = 0L; 
BgL_matchz00_6243 = BgL_newzd2matchzd2_8776; } }  else 
{ /* Llib/date.scm 835 */
if(
(
(long)(BgL_curz00_6032)==45L))
{ /* Llib/date.scm 835 */
BgL_iportz00_6069 = BgL_iportz00_6023; 
BgL_lastzd2matchzd2_6070 = BgL_lastzd2matchzd2_6024; 
BgL_forwardz00_6071 = 
(1L+BgL_forwardz00_6025); 
BgL_bufposz00_6072 = BgL_bufposz00_6026; 
BgL_zc3z04anonymousza34113ze3z87_6073:
{ /* Llib/date.scm 835 */
 long BgL_newzd2matchzd2_6074;
RGC_STOP_MATCH(BgL_iportz00_6069, BgL_forwardz00_6071); 
BgL_newzd2matchzd2_6074 = 3L; 
if(
(BgL_forwardz00_6071==BgL_bufposz00_6072))
{ /* Llib/date.scm 835 */
if(
rgc_fill_buffer(BgL_iportz00_6069))
{ /* Llib/date.scm 835 */
 long BgL_arg4116z00_6077; long BgL_arg4117z00_6078;
BgL_arg4116z00_6077 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6069); 
BgL_arg4117z00_6078 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6069); 
{ 
 long BgL_bufposz00_19142; long BgL_forwardz00_19141;
BgL_forwardz00_19141 = BgL_arg4116z00_6077; 
BgL_bufposz00_19142 = BgL_arg4117z00_6078; 
BgL_bufposz00_6072 = BgL_bufposz00_19142; 
BgL_forwardz00_6071 = BgL_forwardz00_19141; 
goto BgL_zc3z04anonymousza34113ze3z87_6073;} }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_newzd2matchzd2_6074; } }  else 
{ /* Llib/date.scm 835 */
 int BgL_curz00_6079;
BgL_curz00_6079 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6069, BgL_forwardz00_6071); 
{ /* Llib/date.scm 835 */

{ /* Llib/date.scm 835 */
 bool_t BgL_test7337z00_19144;
if(
(
(long)(BgL_curz00_6079)>=48L))
{ /* Llib/date.scm 835 */
BgL_test7337z00_19144 = 
(
(long)(BgL_curz00_6079)<58L)
; }  else 
{ /* Llib/date.scm 835 */
BgL_test7337z00_19144 = ((bool_t)0)
; } 
if(BgL_test7337z00_19144)
{ /* Llib/date.scm 835 */
BgL_iportz00_6084 = BgL_iportz00_6069; 
BgL_lastzd2matchzd2_6085 = BgL_newzd2matchzd2_6074; 
BgL_forwardz00_6086 = 
(1L+BgL_forwardz00_6071); 
BgL_bufposz00_6087 = BgL_bufposz00_6072; 
BgL_zc3z04anonymousza34121ze3z87_6088:
if(
(BgL_forwardz00_6086==BgL_bufposz00_6087))
{ /* Llib/date.scm 835 */
if(
rgc_fill_buffer(BgL_iportz00_6084))
{ /* Llib/date.scm 835 */
 long BgL_arg4124z00_6091; long BgL_arg4125z00_6092;
BgL_arg4124z00_6091 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6084); 
BgL_arg4125z00_6092 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6084); 
{ 
 long BgL_bufposz00_19157; long BgL_forwardz00_19156;
BgL_forwardz00_19156 = BgL_arg4124z00_6091; 
BgL_bufposz00_19157 = BgL_arg4125z00_6092; 
BgL_bufposz00_6087 = BgL_bufposz00_19157; 
BgL_forwardz00_6086 = BgL_forwardz00_19156; 
goto BgL_zc3z04anonymousza34121ze3z87_6088;} }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_lastzd2matchzd2_6085; } }  else 
{ /* Llib/date.scm 835 */
 int BgL_curz00_6093;
BgL_curz00_6093 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6084, BgL_forwardz00_6086); 
{ /* Llib/date.scm 835 */

{ /* Llib/date.scm 835 */
 bool_t BgL_test7341z00_19159;
if(
(
(long)(BgL_curz00_6093)>=48L))
{ /* Llib/date.scm 835 */
BgL_test7341z00_19159 = 
(
(long)(BgL_curz00_6093)<58L)
; }  else 
{ /* Llib/date.scm 835 */
BgL_test7341z00_19159 = ((bool_t)0)
; } 
if(BgL_test7341z00_19159)
{ /* Llib/date.scm 835 */
 long BgL_arg4128z00_6096;
BgL_arg4128z00_6096 = 
(1L+BgL_forwardz00_6086); 
{ /* Llib/date.scm 835 */
 long BgL_newzd2matchzd2_8827;
RGC_STOP_MATCH(BgL_iportz00_6084, BgL_arg4128z00_6096); 
BgL_newzd2matchzd2_8827 = 2L; 
BgL_matchz00_6243 = BgL_newzd2matchzd2_8827; } }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_lastzd2matchzd2_6085; } } } } }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_newzd2matchzd2_6074; } } } } } }  else 
{ /* Llib/date.scm 835 */
if(
(
(long)(BgL_curz00_6032)==43L))
{ /* Llib/date.scm 835 */
BgL_iportz00_6054 = BgL_iportz00_6023; 
BgL_lastzd2matchzd2_6055 = BgL_lastzd2matchzd2_6024; 
BgL_forwardz00_6056 = 
(1L+BgL_forwardz00_6025); 
BgL_bufposz00_6057 = BgL_bufposz00_6026; 
BgL_zc3z04anonymousza34105ze3z87_6058:
{ /* Llib/date.scm 835 */
 long BgL_newzd2matchzd2_6059;
RGC_STOP_MATCH(BgL_iportz00_6054, BgL_forwardz00_6056); 
BgL_newzd2matchzd2_6059 = 3L; 
if(
(BgL_forwardz00_6056==BgL_bufposz00_6057))
{ /* Llib/date.scm 835 */
if(
rgc_fill_buffer(BgL_iportz00_6054))
{ /* Llib/date.scm 835 */
 long BgL_arg4108z00_6062; long BgL_arg4109z00_6063;
BgL_arg4108z00_6062 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6054); 
BgL_arg4109z00_6063 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6054); 
{ 
 long BgL_bufposz00_19180; long BgL_forwardz00_19179;
BgL_forwardz00_19179 = BgL_arg4108z00_6062; 
BgL_bufposz00_19180 = BgL_arg4109z00_6063; 
BgL_bufposz00_6057 = BgL_bufposz00_19180; 
BgL_forwardz00_6056 = BgL_forwardz00_19179; 
goto BgL_zc3z04anonymousza34105ze3z87_6058;} }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_newzd2matchzd2_6059; } }  else 
{ /* Llib/date.scm 835 */
 int BgL_curz00_6064;
BgL_curz00_6064 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6054, BgL_forwardz00_6056); 
{ /* Llib/date.scm 835 */

{ /* Llib/date.scm 835 */
 bool_t BgL_test7346z00_19182;
if(
(
(long)(BgL_curz00_6064)>=48L))
{ /* Llib/date.scm 835 */
BgL_test7346z00_19182 = 
(
(long)(BgL_curz00_6064)<58L)
; }  else 
{ /* Llib/date.scm 835 */
BgL_test7346z00_19182 = ((bool_t)0)
; } 
if(BgL_test7346z00_19182)
{ /* Llib/date.scm 835 */
BgL_iportz00_6104 = BgL_iportz00_6054; 
BgL_lastzd2matchzd2_6105 = BgL_newzd2matchzd2_6059; 
BgL_forwardz00_6106 = 
(1L+BgL_forwardz00_6056); 
BgL_bufposz00_6107 = BgL_bufposz00_6057; 
BgL_zc3z04anonymousza34130ze3z87_6108:
if(
(BgL_forwardz00_6106==BgL_bufposz00_6107))
{ /* Llib/date.scm 835 */
if(
rgc_fill_buffer(BgL_iportz00_6104))
{ /* Llib/date.scm 835 */
 long BgL_arg4133z00_6111; long BgL_arg4134z00_6112;
BgL_arg4133z00_6111 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6104); 
BgL_arg4134z00_6112 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6104); 
{ 
 long BgL_bufposz00_19195; long BgL_forwardz00_19194;
BgL_forwardz00_19194 = BgL_arg4133z00_6111; 
BgL_bufposz00_19195 = BgL_arg4134z00_6112; 
BgL_bufposz00_6107 = BgL_bufposz00_19195; 
BgL_forwardz00_6106 = BgL_forwardz00_19194; 
goto BgL_zc3z04anonymousza34130ze3z87_6108;} }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_lastzd2matchzd2_6105; } }  else 
{ /* Llib/date.scm 835 */
 int BgL_curz00_6113;
BgL_curz00_6113 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6104, BgL_forwardz00_6106); 
{ /* Llib/date.scm 835 */

{ /* Llib/date.scm 835 */
 bool_t BgL_test7350z00_19197;
if(
(
(long)(BgL_curz00_6113)>=48L))
{ /* Llib/date.scm 835 */
BgL_test7350z00_19197 = 
(
(long)(BgL_curz00_6113)<58L)
; }  else 
{ /* Llib/date.scm 835 */
BgL_test7350z00_19197 = ((bool_t)0)
; } 
if(BgL_test7350z00_19197)
{ /* Llib/date.scm 835 */
 long BgL_arg4137z00_6116;
BgL_arg4137z00_6116 = 
(1L+BgL_forwardz00_6106); 
{ /* Llib/date.scm 835 */
 long BgL_newzd2matchzd2_8843;
RGC_STOP_MATCH(BgL_iportz00_6104, BgL_arg4137z00_6116); 
BgL_newzd2matchzd2_8843 = 1L; 
BgL_matchz00_6243 = BgL_newzd2matchzd2_8843; } }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_lastzd2matchzd2_6105; } } } } }  else 
{ /* Llib/date.scm 835 */
BgL_matchz00_6243 = BgL_newzd2matchzd2_6059; } } } } } }  else 
{ /* Llib/date.scm 835 */
 long BgL_arg4102z00_6040;
BgL_arg4102z00_6040 = 
(1L+BgL_forwardz00_6025); 
{ /* Llib/date.scm 835 */
 long BgL_newzd2matchzd2_8784;
RGC_STOP_MATCH(BgL_iportz00_6023, BgL_arg4102z00_6040); 
BgL_newzd2matchzd2_8784 = 3L; 
BgL_matchz00_6243 = BgL_newzd2matchzd2_8784; } } } } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_5970); 
switch( BgL_matchz00_6243) { case 3L : 

{ /* Llib/date.scm 850 */
 bool_t BgL_test7352z00_19210;
{ /* Llib/date.scm 850 */
 obj_t BgL_tmpz00_19211;
BgL_tmpz00_19211 = 
BGl_thezd2failureze71z35zz__datez00(BgL_iportz00_5970); 
BgL_test7352z00_19210 = 
EOF_OBJECTP(BgL_tmpz00_19211); } 
if(BgL_test7352z00_19210)
{ /* Llib/date.scm 850 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_19216;
{ /* Llib/date.scm 851 */
 obj_t BgL_aux5710z00_10819;
BgL_aux5710z00_10819 = BFALSE; 
{ 
 obj_t BgL_auxz00_19223;
BgL_auxz00_19223 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(37136L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5710z00_10819); 
FAILURE(BgL_auxz00_19223,BFALSE,BFALSE);} } 
BgL_tmpz00_19216 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_5977, 
(int)(BgL_ssz00_5976), 
(int)(BgL_mmz00_5975), 
(int)(BgL_hhz00_5974), 
(int)(BgL_ddz00_5973), 
(int)(BgL_mmz00_5972), 
(int)(BgL_yyyyz00_5971), BgL_tmpz00_19216, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_5977, 
(int)(BgL_ssz00_5976), 
(int)(BgL_mmz00_5975), 
(int)(BgL_hhz00_5974), 
(int)(BgL_ddz00_5973), 
(int)(BgL_mmz00_5972), 
(int)(BgL_yyyyz00_5971), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 850 */
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, 
BGl_thezd2failureze71z35zz__datez00(BgL_iportz00_5970), BgL_iportz00_5970); } } break;case 2L : 

{ /* Llib/date.scm 845 */
 long BgL_b1z00_6260; long BgL_b2z00_6261;
{ /* Llib/date.scm 845 */
 int BgL_arg4236z00_6267;
{ /* Llib/date.scm 835 */
 int BgL_tmpz00_19239;
BgL_tmpz00_19239 = 
(int)(1L); 
BgL_arg4236z00_6267 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5970, BgL_tmpz00_19239); } 
BgL_b1z00_6260 = 
(
(long)(BgL_arg4236z00_6267)-48L); } 
{ /* Llib/date.scm 846 */
 int BgL_arg4237z00_6268;
{ /* Llib/date.scm 835 */
 int BgL_tmpz00_19244;
BgL_tmpz00_19244 = 
(int)(2L); 
BgL_arg4237z00_6268 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5970, BgL_tmpz00_19244); } 
BgL_b2z00_6261 = 
(
(long)(BgL_arg4237z00_6268)-48L); } 
BgL_iportz00_6326 = BgL_iportz00_5970; 
BgL_yyyyz00_6327 = BgL_yyyyz00_5971; 
BgL_mmz00_6328 = BgL_mmz00_5972; 
BgL_ddz00_6329 = BgL_ddz00_5973; 
BgL_hhz00_6330 = BgL_hhz00_5974; 
BgL_mmz00_6331 = BgL_mmz00_5975; 
BgL_ssz00_6332 = BgL_ssz00_5976; 
BgL_sssz00_6333 = BgL_sssz00_5977; 
BgL_za7hhza7_6334 = 
NEG(
(3600L*
(
(BgL_b1z00_6260*10L)+BgL_b2z00_6261))); 
BgL_zc3z04anonymousza34246ze3z87_6335:
{ 
 obj_t BgL_iportz00_6376; long BgL_lastzd2matchzd2_6377; long BgL_forwardz00_6378; long BgL_bufposz00_6379; obj_t BgL_iportz00_6395; long BgL_lastzd2matchzd2_6396; long BgL_forwardz00_6397; long BgL_bufposz00_6398; obj_t BgL_iportz00_6410; long BgL_lastzd2matchzd2_6411; long BgL_forwardz00_6412; long BgL_bufposz00_6413;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_6326))
{ /* Llib/date.scm 818 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg4247z00_6369;
{ /* Llib/date.scm 818 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_6370;
{ /* Llib/date.scm 818 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_6373;
BgL_new1077z00_6373 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 818 */
 long BgL_arg4250z00_6374;
BgL_arg4250z00_6374 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_6373), BgL_arg4250z00_6374); } 
BgL_new1078z00_6370 = BgL_new1077z00_6373; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_6370)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_6370)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_19259;
{ /* Llib/date.scm 818 */
 obj_t BgL_arg4248z00_6371;
{ /* Llib/date.scm 818 */
 obj_t BgL_arg4249z00_6372;
{ /* Llib/date.scm 818 */
 obj_t BgL_classz00_8763;
BgL_classz00_8763 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg4249z00_6372 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_8763); } 
BgL_arg4248z00_6371 = 
VECTOR_REF(BgL_arg4249z00_6372,2L); } 
{ /* Llib/date.scm 818 */
 obj_t BgL_auxz00_19263;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg4248z00_6371))
{ /* Llib/date.scm 818 */
BgL_auxz00_19263 = BgL_arg4248z00_6371
; }  else 
{ 
 obj_t BgL_auxz00_19266;
BgL_auxz00_19266 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(35982L), BGl_string6012z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg4248z00_6371); 
FAILURE(BgL_auxz00_19266,BFALSE,BFALSE);} 
BgL_auxz00_19259 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_19263); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_6370)))->BgL_stackz00)=((obj_t)BgL_auxz00_19259),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_6370)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_6370)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_6370)))->BgL_objz00)=((obj_t)BgL_iportz00_6326),BUNSPEC); 
BgL_arg4247z00_6369 = BgL_new1078z00_6370; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg4247z00_6369)); }  else 
{ /* Llib/date.scm 818 */
RGC_START_MATCH(BgL_iportz00_6326); 
{ /* Llib/date.scm 818 */
 long BgL_matchz00_6549;
{ /* Llib/date.scm 818 */
 long BgL_arg4379z00_6587; long BgL_arg4380z00_6588;
BgL_arg4379z00_6587 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6326); 
BgL_arg4380z00_6588 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6326); 
BgL_iportz00_6376 = BgL_iportz00_6326; 
BgL_lastzd2matchzd2_6377 = 1L; 
BgL_forwardz00_6378 = BgL_arg4379z00_6587; 
BgL_bufposz00_6379 = BgL_arg4380z00_6588; 
BgL_zc3z04anonymousza34252ze3z87_6380:
if(
(BgL_forwardz00_6378==BgL_bufposz00_6379))
{ /* Llib/date.scm 818 */
if(
rgc_fill_buffer(BgL_iportz00_6376))
{ /* Llib/date.scm 818 */
 long BgL_arg4255z00_6383; long BgL_arg4256z00_6384;
BgL_arg4255z00_6383 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6376); 
BgL_arg4256z00_6384 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6376); 
{ 
 long BgL_bufposz00_19290; long BgL_forwardz00_19289;
BgL_forwardz00_19289 = BgL_arg4255z00_6383; 
BgL_bufposz00_19290 = BgL_arg4256z00_6384; 
BgL_bufposz00_6379 = BgL_bufposz00_19290; 
BgL_forwardz00_6378 = BgL_forwardz00_19289; 
goto BgL_zc3z04anonymousza34252ze3z87_6380;} }  else 
{ /* Llib/date.scm 818 */
BgL_matchz00_6549 = BgL_lastzd2matchzd2_6377; } }  else 
{ /* Llib/date.scm 818 */
 int BgL_curz00_6385;
BgL_curz00_6385 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6376, BgL_forwardz00_6378); 
{ /* Llib/date.scm 818 */

if(
(
(long)(BgL_curz00_6385)==58L))
{ /* Llib/date.scm 818 */
BgL_iportz00_6395 = BgL_iportz00_6376; 
BgL_lastzd2matchzd2_6396 = BgL_lastzd2matchzd2_6377; 
BgL_forwardz00_6397 = 
(1L+BgL_forwardz00_6378); 
BgL_bufposz00_6398 = BgL_bufposz00_6379; 
BgL_zc3z04anonymousza34261ze3z87_6399:
{ /* Llib/date.scm 818 */
 long BgL_newzd2matchzd2_6400;
RGC_STOP_MATCH(BgL_iportz00_6395, BgL_forwardz00_6397); 
BgL_newzd2matchzd2_6400 = 1L; 
if(
(BgL_forwardz00_6397==BgL_bufposz00_6398))
{ /* Llib/date.scm 818 */
if(
rgc_fill_buffer(BgL_iportz00_6395))
{ /* Llib/date.scm 818 */
 long BgL_arg4264z00_6403; long BgL_arg4265z00_6404;
BgL_arg4264z00_6403 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6395); 
BgL_arg4265z00_6404 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6395); 
{ 
 long BgL_bufposz00_19303; long BgL_forwardz00_19302;
BgL_forwardz00_19302 = BgL_arg4264z00_6403; 
BgL_bufposz00_19303 = BgL_arg4265z00_6404; 
BgL_bufposz00_6398 = BgL_bufposz00_19303; 
BgL_forwardz00_6397 = BgL_forwardz00_19302; 
goto BgL_zc3z04anonymousza34261ze3z87_6399;} }  else 
{ /* Llib/date.scm 818 */
BgL_matchz00_6549 = BgL_newzd2matchzd2_6400; } }  else 
{ /* Llib/date.scm 818 */
 int BgL_curz00_6405;
BgL_curz00_6405 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6395, BgL_forwardz00_6397); 
{ /* Llib/date.scm 818 */

{ /* Llib/date.scm 818 */
 bool_t BgL_test7361z00_19305;
if(
(
(long)(BgL_curz00_6405)>=48L))
{ /* Llib/date.scm 818 */
BgL_test7361z00_19305 = 
(
(long)(BgL_curz00_6405)<58L)
; }  else 
{ /* Llib/date.scm 818 */
BgL_test7361z00_19305 = ((bool_t)0)
; } 
if(BgL_test7361z00_19305)
{ /* Llib/date.scm 818 */
BgL_iportz00_6410 = BgL_iportz00_6395; 
BgL_lastzd2matchzd2_6411 = BgL_newzd2matchzd2_6400; 
BgL_forwardz00_6412 = 
(1L+BgL_forwardz00_6397); 
BgL_bufposz00_6413 = BgL_bufposz00_6398; 
BgL_zc3z04anonymousza34269ze3z87_6414:
if(
(BgL_forwardz00_6412==BgL_bufposz00_6413))
{ /* Llib/date.scm 818 */
if(
rgc_fill_buffer(BgL_iportz00_6410))
{ /* Llib/date.scm 818 */
 long BgL_arg4272z00_6417; long BgL_arg4273z00_6418;
BgL_arg4272z00_6417 = 
RGC_BUFFER_FORWARD(BgL_iportz00_6410); 
BgL_arg4273z00_6418 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_6410); 
{ 
 long BgL_bufposz00_19318; long BgL_forwardz00_19317;
BgL_forwardz00_19317 = BgL_arg4272z00_6417; 
BgL_bufposz00_19318 = BgL_arg4273z00_6418; 
BgL_bufposz00_6413 = BgL_bufposz00_19318; 
BgL_forwardz00_6412 = BgL_forwardz00_19317; 
goto BgL_zc3z04anonymousza34269ze3z87_6414;} }  else 
{ /* Llib/date.scm 818 */
BgL_matchz00_6549 = BgL_lastzd2matchzd2_6411; } }  else 
{ /* Llib/date.scm 818 */
 int BgL_curz00_6419;
BgL_curz00_6419 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_6410, BgL_forwardz00_6412); 
{ /* Llib/date.scm 818 */

{ /* Llib/date.scm 818 */
 bool_t BgL_test7365z00_19320;
if(
(
(long)(BgL_curz00_6419)>=48L))
{ /* Llib/date.scm 818 */
BgL_test7365z00_19320 = 
(
(long)(BgL_curz00_6419)<58L)
; }  else 
{ /* Llib/date.scm 818 */
BgL_test7365z00_19320 = ((bool_t)0)
; } 
if(BgL_test7365z00_19320)
{ /* Llib/date.scm 818 */
 long BgL_arg4276z00_6422;
BgL_arg4276z00_6422 = 
(1L+BgL_forwardz00_6412); 
{ /* Llib/date.scm 818 */
 long BgL_newzd2matchzd2_8723;
RGC_STOP_MATCH(BgL_iportz00_6410, BgL_arg4276z00_6422); 
BgL_newzd2matchzd2_8723 = 0L; 
BgL_matchz00_6549 = BgL_newzd2matchzd2_8723; } }  else 
{ /* Llib/date.scm 818 */
BgL_matchz00_6549 = BgL_lastzd2matchzd2_6411; } } } } }  else 
{ /* Llib/date.scm 818 */
BgL_matchz00_6549 = BgL_newzd2matchzd2_6400; } } } } } }  else 
{ /* Llib/date.scm 818 */
 long BgL_arg4259z00_6388;
BgL_arg4259z00_6388 = 
(1L+BgL_forwardz00_6378); 
{ /* Llib/date.scm 818 */
 long BgL_newzd2matchzd2_8695;
RGC_STOP_MATCH(BgL_iportz00_6376, BgL_arg4259z00_6388); 
BgL_newzd2matchzd2_8695 = 1L; 
BgL_matchz00_6549 = BgL_newzd2matchzd2_8695; } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_6326); 
switch( BgL_matchz00_6549) { case 1L : 

{ /* Llib/date.scm 828 */
 bool_t BgL_test7367z00_19333;
{ /* Llib/date.scm 828 */
 obj_t BgL_tmpz00_19334;
BgL_tmpz00_19334 = 
BGl_thezd2failureze70z35zz__datez00(BgL_iportz00_6326); 
BgL_test7367z00_19333 = 
EOF_OBJECTP(BgL_tmpz00_19334); } 
if(BgL_test7367z00_19333)
{ /* Llib/date.scm 828 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(
BINT(BgL_za7hhza7_6334)))
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_6333, 
(int)(BgL_ssz00_6332), 
(int)(BgL_mmz00_6331), 
(int)(BgL_hhz00_6330), 
(int)(BgL_ddz00_6329), 
(int)(BgL_mmz00_6328), 
(int)(BgL_yyyyz00_6327), BgL_za7hhza7_6334, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_6333, 
(int)(BgL_ssz00_6332), 
(int)(BgL_mmz00_6331), 
(int)(BgL_hhz00_6330), 
(int)(BgL_ddz00_6329), 
(int)(BgL_mmz00_6328), 
(int)(BgL_yyyyz00_6327), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 828 */
BgL_aux5737z00_10846 = 
BGl_parsezd2errorzd2zz__datez00(BGl_string6011z00zz__datez00, BGl_string5809z00zz__datez00, 
BGl_thezd2failureze70z35zz__datez00(BgL_iportz00_6326), BgL_iportz00_6326); } } break;case 0L : 

{ /* Llib/date.scm 820 */
 long BgL_b1z00_6566;
{ /* Llib/date.scm 820 */
 int BgL_arg4378z00_6586;
{ /* Llib/date.scm 818 */
 int BgL_tmpz00_19358;
BgL_tmpz00_19358 = 
(int)(1L); 
BgL_arg4378z00_6586 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_6326, BgL_tmpz00_19358); } 
BgL_b1z00_6566 = 
(
(long)(BgL_arg4378z00_6586)-48L); } 
{ /* Llib/date.scm 820 */
 long BgL_b2z00_6567;
{ /* Llib/date.scm 821 */
 int BgL_arg4377z00_6585;
{ /* Llib/date.scm 818 */
 int BgL_tmpz00_19363;
BgL_tmpz00_19363 = 
(int)(2L); 
BgL_arg4377z00_6585 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_6326, BgL_tmpz00_19363); } 
BgL_b2z00_6567 = 
(
(long)(BgL_arg4377z00_6585)-48L); } 
{ /* Llib/date.scm 821 */
 long BgL_tza7za7_6568;
if(
(BgL_za7hhza7_6334<0L))
{ /* Llib/date.scm 822 */
BgL_tza7za7_6568 = 
(BgL_za7hhza7_6334-
(60L*
(
(BgL_b1z00_6566*10L)+BgL_b2z00_6567))); }  else 
{ /* Llib/date.scm 822 */
BgL_tza7za7_6568 = 
(BgL_za7hhza7_6334+
(60L*
(
(BgL_b1z00_6566*10L)+BgL_b2z00_6567))); } 
{ /* Llib/date.scm 822 */

if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(
BINT(BgL_tza7za7_6568)))
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_6333, 
(int)(BgL_ssz00_6332), 
(int)(BgL_mmz00_6331), 
(int)(BgL_hhz00_6330), 
(int)(BgL_ddz00_6329), 
(int)(BgL_mmz00_6328), 
(int)(BgL_yyyyz00_6327), BgL_tza7za7_6568, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_6333, 
(int)(BgL_ssz00_6332), 
(int)(BgL_mmz00_6331), 
(int)(BgL_hhz00_6330), 
(int)(BgL_ddz00_6329), 
(int)(BgL_mmz00_6328), 
(int)(BgL_yyyyz00_6327), 0L, ((bool_t)0), 
(int)(-1L)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_6549)); } } } } } break;case 1L : 

{ /* Llib/date.scm 840 */
 long BgL_b1z00_6269; long BgL_b2z00_6270;
{ /* Llib/date.scm 840 */
 int BgL_arg4242z00_6275;
{ /* Llib/date.scm 835 */
 int BgL_tmpz00_19404;
BgL_tmpz00_19404 = 
(int)(1L); 
BgL_arg4242z00_6275 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5970, BgL_tmpz00_19404); } 
BgL_b1z00_6269 = 
(
(long)(BgL_arg4242z00_6275)-48L); } 
{ /* Llib/date.scm 841 */
 int BgL_arg4243z00_6276;
{ /* Llib/date.scm 835 */
 int BgL_tmpz00_19409;
BgL_tmpz00_19409 = 
(int)(2L); 
BgL_arg4243z00_6276 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5970, BgL_tmpz00_19409); } 
BgL_b2z00_6270 = 
(
(long)(BgL_arg4243z00_6276)-48L); } 
{ 
 long BgL_za7hhza7_19422; BGL_LONGLONG_T BgL_sssz00_19421; long BgL_ssz00_19420; long BgL_mmz00_19419; long BgL_hhz00_19418; long BgL_ddz00_19417; long BgL_mmz00_19416; long BgL_yyyyz00_19415; obj_t BgL_iportz00_19414;
BgL_iportz00_19414 = BgL_iportz00_5970; 
BgL_yyyyz00_19415 = BgL_yyyyz00_5971; 
BgL_mmz00_19416 = BgL_mmz00_5972; 
BgL_ddz00_19417 = BgL_ddz00_5973; 
BgL_hhz00_19418 = BgL_hhz00_5974; 
BgL_mmz00_19419 = BgL_mmz00_5975; 
BgL_ssz00_19420 = BgL_ssz00_5976; 
BgL_sssz00_19421 = BgL_sssz00_5977; 
BgL_za7hhza7_19422 = 
(3600L*
(
(BgL_b1z00_6269*10L)+BgL_b2z00_6270)); 
BgL_za7hhza7_6334 = BgL_za7hhza7_19422; 
BgL_sssz00_6333 = BgL_sssz00_19421; 
BgL_ssz00_6332 = BgL_ssz00_19420; 
BgL_mmz00_6331 = BgL_mmz00_19419; 
BgL_hhz00_6330 = BgL_hhz00_19418; 
BgL_ddz00_6329 = BgL_ddz00_19417; 
BgL_mmz00_6328 = BgL_mmz00_19416; 
BgL_yyyyz00_6327 = BgL_yyyyz00_19415; 
BgL_iportz00_6326 = BgL_iportz00_19414; 
goto BgL_zc3z04anonymousza34246ze3z87_6335;} } break;case 0L : 

if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(
BINT(0L)))
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_5977, 
(int)(BgL_ssz00_5976), 
(int)(BgL_mmz00_5975), 
(int)(BgL_hhz00_5974), 
(int)(BgL_ddz00_5973), 
(int)(BgL_mmz00_5972), 
(int)(BgL_yyyyz00_5971), 0L, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(BgL_sssz00_5977, 
(int)(BgL_ssz00_5976), 
(int)(BgL_mmz00_5975), 
(int)(BgL_hhz00_5974), 
(int)(BgL_ddz00_5973), 
(int)(BgL_mmz00_5972), 
(int)(BgL_yyyyz00_5971), 0L, ((bool_t)0), 
(int)(-1L)); } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_6243)); } } } } } } break;case 0L : 

{ /* Llib/date.scm 878 */
 long BgL_b1z00_5623; long BgL_b2z00_5624;
{ /* Llib/date.scm 878 */
 int BgL_arg3935z00_5628;
{ /* Llib/date.scm 876 */
 int BgL_tmpz00_19448;
BgL_tmpz00_19448 = 
(int)(1L); 
BgL_arg3935z00_5628 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5385, BgL_tmpz00_19448); } 
BgL_b1z00_5623 = 
(
(long)(BgL_arg3935z00_5628)-48L); } 
{ /* Llib/date.scm 879 */
 int BgL_arg3936z00_5629;
{ /* Llib/date.scm 876 */
 int BgL_tmpz00_19453;
BgL_tmpz00_19453 = 
(int)(2L); 
BgL_arg3936z00_5629 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5385, BgL_tmpz00_19453); } 
BgL_b2z00_5624 = 
(
(long)(BgL_arg3936z00_5629)-48L); } 
BgL_iportz00_5666 = BgL_iportz00_5385; 
BgL_yyyyz00_5667 = BgL_yyyyz00_5386; 
BgL_mmz00_5668 = BgL_mmz00_5387; 
BgL_ddz00_5669 = BgL_ddz00_5388; 
BgL_hhz00_5670 = BgL_hhz00_5389; 
BgL_mmz00_5671 = BgL_mmz00_5390; 
BgL_ssz00_5672 = 
(
(BgL_b1z00_5623*10L)+BgL_b2z00_5624); 
{ 
 obj_t BgL_iportz00_5715; long BgL_lastzd2matchzd2_5716; long BgL_forwardz00_5717; long BgL_bufposz00_5718; obj_t BgL_iportz00_5734; long BgL_lastzd2matchzd2_5735; long BgL_forwardz00_5736; long BgL_bufposz00_5737; obj_t BgL_iportz00_5749; long BgL_lastzd2matchzd2_5750; long BgL_forwardz00_5751; long BgL_bufposz00_5752; obj_t BgL_iportz00_5763; long BgL_lastzd2matchzd2_5764; long BgL_forwardz00_5765; long BgL_bufposz00_5766;
if(
INPUT_PORT_CLOSEP(BgL_iportz00_5666))
{ /* Llib/date.scm 858 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_arg3940z00_5708;
{ /* Llib/date.scm 858 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1078z00_5709;
{ /* Llib/date.scm 858 */
 BgL_z62iozd2closedzd2errorz62_bglt BgL_new1077z00_5712;
BgL_new1077z00_5712 = 
((BgL_z62iozd2closedzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2closedzd2errorz62_bgl) ))); 
{ /* Llib/date.scm 858 */
 long BgL_arg3943z00_5713;
BgL_arg3943z00_5713 = 
BGL_CLASS_NUM(BGl_z62iozd2closedzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1077z00_5712), BgL_arg3943z00_5713); } 
BgL_new1078z00_5709 = BgL_new1077z00_5712; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5709)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5709)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_19468;
{ /* Llib/date.scm 858 */
 obj_t BgL_arg3941z00_5710;
{ /* Llib/date.scm 858 */
 obj_t BgL_arg3942z00_5711;
{ /* Llib/date.scm 858 */
 obj_t BgL_classz00_8975;
BgL_classz00_8975 = BGl_z62iozd2closedzd2errorz62zz__objectz00; 
BgL_arg3942z00_5711 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_8975); } 
BgL_arg3941z00_5710 = 
VECTOR_REF(BgL_arg3942z00_5711,2L); } 
{ /* Llib/date.scm 858 */
 obj_t BgL_auxz00_19472;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg3941z00_5710))
{ /* Llib/date.scm 858 */
BgL_auxz00_19472 = BgL_arg3941z00_5710
; }  else 
{ 
 obj_t BgL_auxz00_19475;
BgL_auxz00_19475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(37348L), BGl_string6016z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg3941z00_5710); 
FAILURE(BgL_auxz00_19475,BFALSE,BFALSE);} 
BgL_auxz00_19468 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_19472); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1078z00_5709)))->BgL_stackz00)=((obj_t)BgL_auxz00_19468),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5709)))->BgL_procz00)=((obj_t)BGl_string5790z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5709)))->BgL_msgz00)=((obj_t)BGl_string5808z00zz__datez00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1078z00_5709)))->BgL_objz00)=((obj_t)BgL_iportz00_5666),BUNSPEC); 
BgL_arg3940z00_5708 = BgL_new1078z00_5709; } 
BgL_aux5737z00_10846 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg3940z00_5708)); }  else 
{ /* Llib/date.scm 858 */
RGC_START_MATCH(BgL_iportz00_5666); 
{ /* Llib/date.scm 858 */
 long BgL_matchz00_5902;
{ /* Llib/date.scm 858 */
 long BgL_arg4082z00_5933; long BgL_arg4083z00_5934;
BgL_arg4082z00_5933 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5666); 
BgL_arg4083z00_5934 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5666); 
BgL_iportz00_5715 = BgL_iportz00_5666; 
BgL_lastzd2matchzd2_5716 = 1L; 
BgL_forwardz00_5717 = BgL_arg4082z00_5933; 
BgL_bufposz00_5718 = BgL_arg4083z00_5934; 
BgL_zc3z04anonymousza33945ze3z87_5719:
if(
(BgL_forwardz00_5717==BgL_bufposz00_5718))
{ /* Llib/date.scm 858 */
if(
rgc_fill_buffer(BgL_iportz00_5715))
{ /* Llib/date.scm 858 */
 long BgL_arg3948z00_5722; long BgL_arg3949z00_5723;
BgL_arg3948z00_5722 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5715); 
BgL_arg3949z00_5723 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5715); 
{ 
 long BgL_bufposz00_19499; long BgL_forwardz00_19498;
BgL_forwardz00_19498 = BgL_arg3948z00_5722; 
BgL_bufposz00_19499 = BgL_arg3949z00_5723; 
BgL_bufposz00_5718 = BgL_bufposz00_19499; 
BgL_forwardz00_5717 = BgL_forwardz00_19498; 
goto BgL_zc3z04anonymousza33945ze3z87_5719;} }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_lastzd2matchzd2_5716; } }  else 
{ /* Llib/date.scm 858 */
 int BgL_curz00_5724;
BgL_curz00_5724 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5715, BgL_forwardz00_5717); 
{ /* Llib/date.scm 858 */

if(
(
(long)(BgL_curz00_5724)==46L))
{ /* Llib/date.scm 858 */
BgL_iportz00_5734 = BgL_iportz00_5715; 
BgL_lastzd2matchzd2_5735 = BgL_lastzd2matchzd2_5716; 
BgL_forwardz00_5736 = 
(1L+BgL_forwardz00_5717); 
BgL_bufposz00_5737 = BgL_bufposz00_5718; 
BgL_zc3z04anonymousza33954ze3z87_5738:
{ /* Llib/date.scm 858 */
 long BgL_newzd2matchzd2_5739;
RGC_STOP_MATCH(BgL_iportz00_5734, BgL_forwardz00_5736); 
BgL_newzd2matchzd2_5739 = 1L; 
if(
(BgL_forwardz00_5736==BgL_bufposz00_5737))
{ /* Llib/date.scm 858 */
if(
rgc_fill_buffer(BgL_iportz00_5734))
{ /* Llib/date.scm 858 */
 long BgL_arg3957z00_5742; long BgL_arg3958z00_5743;
BgL_arg3957z00_5742 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5734); 
BgL_arg3958z00_5743 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5734); 
{ 
 long BgL_bufposz00_19512; long BgL_forwardz00_19511;
BgL_forwardz00_19511 = BgL_arg3957z00_5742; 
BgL_bufposz00_19512 = BgL_arg3958z00_5743; 
BgL_bufposz00_5737 = BgL_bufposz00_19512; 
BgL_forwardz00_5736 = BgL_forwardz00_19511; 
goto BgL_zc3z04anonymousza33954ze3z87_5738;} }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_newzd2matchzd2_5739; } }  else 
{ /* Llib/date.scm 858 */
 int BgL_curz00_5744;
BgL_curz00_5744 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5734, BgL_forwardz00_5736); 
{ /* Llib/date.scm 858 */

{ /* Llib/date.scm 858 */
 bool_t BgL_test7379z00_19514;
if(
(
(long)(BgL_curz00_5744)>=48L))
{ /* Llib/date.scm 858 */
BgL_test7379z00_19514 = 
(
(long)(BgL_curz00_5744)<58L)
; }  else 
{ /* Llib/date.scm 858 */
BgL_test7379z00_19514 = ((bool_t)0)
; } 
if(BgL_test7379z00_19514)
{ /* Llib/date.scm 858 */
BgL_iportz00_5749 = BgL_iportz00_5734; 
BgL_lastzd2matchzd2_5750 = BgL_newzd2matchzd2_5739; 
BgL_forwardz00_5751 = 
(1L+BgL_forwardz00_5736); 
BgL_bufposz00_5752 = BgL_bufposz00_5737; 
BgL_zc3z04anonymousza33962ze3z87_5753:
if(
(BgL_forwardz00_5751==BgL_bufposz00_5752))
{ /* Llib/date.scm 858 */
if(
rgc_fill_buffer(BgL_iportz00_5749))
{ /* Llib/date.scm 858 */
 long BgL_arg3965z00_5756; long BgL_arg3966z00_5757;
BgL_arg3965z00_5756 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5749); 
BgL_arg3966z00_5757 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5749); 
{ 
 long BgL_bufposz00_19527; long BgL_forwardz00_19526;
BgL_forwardz00_19526 = BgL_arg3965z00_5756; 
BgL_bufposz00_19527 = BgL_arg3966z00_5757; 
BgL_bufposz00_5752 = BgL_bufposz00_19527; 
BgL_forwardz00_5751 = BgL_forwardz00_19526; 
goto BgL_zc3z04anonymousza33962ze3z87_5753;} }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_lastzd2matchzd2_5750; } }  else 
{ /* Llib/date.scm 858 */
 int BgL_curz00_5758;
BgL_curz00_5758 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5749, BgL_forwardz00_5751); 
{ /* Llib/date.scm 858 */

{ /* Llib/date.scm 858 */
 bool_t BgL_test7383z00_19529;
if(
(
(long)(BgL_curz00_5758)>=48L))
{ /* Llib/date.scm 858 */
BgL_test7383z00_19529 = 
(
(long)(BgL_curz00_5758)<58L)
; }  else 
{ /* Llib/date.scm 858 */
BgL_test7383z00_19529 = ((bool_t)0)
; } 
if(BgL_test7383z00_19529)
{ /* Llib/date.scm 858 */
BgL_iportz00_5763 = BgL_iportz00_5749; 
BgL_lastzd2matchzd2_5764 = BgL_lastzd2matchzd2_5750; 
BgL_forwardz00_5765 = 
(1L+BgL_forwardz00_5751); 
BgL_bufposz00_5766 = BgL_bufposz00_5752; 
BgL_zc3z04anonymousza33970ze3z87_5767:
if(
(BgL_forwardz00_5765==BgL_bufposz00_5766))
{ /* Llib/date.scm 858 */
if(
rgc_fill_buffer(BgL_iportz00_5763))
{ /* Llib/date.scm 858 */
 long BgL_arg3973z00_5770; long BgL_arg3974z00_5771;
BgL_arg3973z00_5770 = 
RGC_BUFFER_FORWARD(BgL_iportz00_5763); 
BgL_arg3974z00_5771 = 
RGC_BUFFER_BUFPOS(BgL_iportz00_5763); 
{ 
 long BgL_bufposz00_19542; long BgL_forwardz00_19541;
BgL_forwardz00_19541 = BgL_arg3973z00_5770; 
BgL_bufposz00_19542 = BgL_arg3974z00_5771; 
BgL_bufposz00_5766 = BgL_bufposz00_19542; 
BgL_forwardz00_5765 = BgL_forwardz00_19541; 
goto BgL_zc3z04anonymousza33970ze3z87_5767;} }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_lastzd2matchzd2_5764; } }  else 
{ /* Llib/date.scm 858 */
 int BgL_curz00_5772;
BgL_curz00_5772 = 
RGC_BUFFER_GET_CHAR(BgL_iportz00_5763, BgL_forwardz00_5765); 
{ /* Llib/date.scm 858 */

{ /* Llib/date.scm 858 */
 bool_t BgL_test7387z00_19544;
if(
(
(long)(BgL_curz00_5772)>=48L))
{ /* Llib/date.scm 858 */
BgL_test7387z00_19544 = 
(
(long)(BgL_curz00_5772)<58L)
; }  else 
{ /* Llib/date.scm 858 */
BgL_test7387z00_19544 = ((bool_t)0)
; } 
if(BgL_test7387z00_19544)
{ /* Llib/date.scm 858 */
 long BgL_arg3977z00_5775;
BgL_arg3977z00_5775 = 
(1L+BgL_forwardz00_5765); 
{ /* Llib/date.scm 858 */
 long BgL_newzd2matchzd2_8936;
RGC_STOP_MATCH(BgL_iportz00_5763, BgL_arg3977z00_5775); 
BgL_newzd2matchzd2_8936 = 0L; 
BgL_matchz00_5902 = BgL_newzd2matchzd2_8936; } }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_lastzd2matchzd2_5764; } } } } }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_lastzd2matchzd2_5750; } } } } }  else 
{ /* Llib/date.scm 858 */
BgL_matchz00_5902 = BgL_newzd2matchzd2_5739; } } } } } }  else 
{ /* Llib/date.scm 858 */
 long BgL_arg3952z00_5727;
BgL_arg3952z00_5727 = 
(1L+BgL_forwardz00_5717); 
{ /* Llib/date.scm 858 */
 long BgL_newzd2matchzd2_8898;
RGC_STOP_MATCH(BgL_iportz00_5715, BgL_arg3952z00_5727); 
BgL_newzd2matchzd2_8898 = 1L; 
BgL_matchz00_5902 = BgL_newzd2matchzd2_8898; } } } } } 
RGC_SET_FILEPOS(BgL_iportz00_5666); 
switch( BgL_matchz00_5902) { case 1L : 

{ /* Llib/date.scm 867 */
 bool_t BgL_test7389z00_19558;
{ /* Llib/date.scm 867 */
 obj_t BgL_tmpz00_19559;
BgL_tmpz00_19559 = 
BGl_thezd2failureze72z35zz__datez00(BgL_iportz00_5666); 
BgL_test7389z00_19558 = 
EOF_OBJECTP(BgL_tmpz00_19559); } 
if(BgL_test7389z00_19558)
{ /* Llib/date.scm 867 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BFALSE))
{ /* Llib/date.scm 233 */
 long BgL_tmpz00_19564;
{ /* Llib/date.scm 868 */
 obj_t BgL_aux5713z00_10822;
BgL_aux5713z00_10822 = BFALSE; 
{ 
 obj_t BgL_auxz00_19571;
BgL_auxz00_19571 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(37666L), BGl_string5785z00zz__datez00, BGl_string6013z00zz__datez00, BgL_aux5713z00_10822); 
FAILURE(BgL_auxz00_19571,BFALSE,BFALSE);} } 
BgL_tmpz00_19564 = 0L; 
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(BgL_ssz00_5672), 
(int)(BgL_mmz00_5671), 
(int)(BgL_hhz00_5670), 
(int)(BgL_ddz00_5669), 
(int)(BgL_mmz00_5668), 
(int)(BgL_yyyyz00_5667), BgL_tmpz00_19564, ((bool_t)1), 
(int)(-1L)); }  else 
{ /* Llib/date.scm 232 */
BgL_aux5737z00_10846 = 
bgl_make_date(((BGL_LONGLONG_T)0), 
(int)(BgL_ssz00_5672), 
(int)(BgL_mmz00_5671), 
(int)(BgL_hhz00_5670), 
(int)(BgL_ddz00_5669), 
(int)(BgL_mmz00_5668), 
(int)(BgL_yyyyz00_5667), 0L, ((bool_t)0), 
(int)(-1L)); } }  else 
{ /* Llib/date.scm 867 */
{ /* Llib/date.scm 871 */
 obj_t BgL_arg4068z00_5916;
BgL_arg4068z00_5916 = 
BGl_thezd2failureze72z35zz__datez00(BgL_iportz00_5666); 
{ /* Llib/date.scm 871 */
 unsigned char BgL_auxz00_19586;
{ /* Llib/date.scm 871 */
 obj_t BgL_tmpz00_19587;
if(
CHARP(BgL_arg4068z00_5916))
{ /* Llib/date.scm 871 */
BgL_tmpz00_19587 = BgL_arg4068z00_5916
; }  else 
{ 
 obj_t BgL_auxz00_19590;
BgL_auxz00_19590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(37778L), BGl_string5785z00zz__datez00, BGl_string6015z00zz__datez00, BgL_arg4068z00_5916); 
FAILURE(BgL_auxz00_19590,BFALSE,BFALSE);} 
BgL_auxz00_19586 = 
CCHAR(BgL_tmpz00_19587); } 
BGl_unreadzd2charz12zc0zz__r4_input_6_10_2z00(BgL_auxz00_19586, BgL_iportz00_5666); } } 
{ 
 BGL_LONGLONG_T BgL_sssz00_19603; long BgL_ssz00_19602; long BgL_mmz00_19601; long BgL_hhz00_19600; long BgL_ddz00_19599; long BgL_mmz00_19598; long BgL_yyyyz00_19597; obj_t BgL_iportz00_19596;
BgL_iportz00_19596 = BgL_iportz00_5666; 
BgL_yyyyz00_19597 = BgL_yyyyz00_5667; 
BgL_mmz00_19598 = BgL_mmz00_5668; 
BgL_ddz00_19599 = BgL_ddz00_5669; 
BgL_hhz00_19600 = BgL_hhz00_5670; 
BgL_mmz00_19601 = BgL_mmz00_5671; 
BgL_ssz00_19602 = BgL_ssz00_5672; 
BgL_sssz00_19603 = ((BGL_LONGLONG_T)0); 
BgL_sssz00_5977 = BgL_sssz00_19603; 
BgL_ssz00_5976 = BgL_ssz00_19602; 
BgL_mmz00_5975 = BgL_mmz00_19601; 
BgL_hhz00_5974 = BgL_hhz00_19600; 
BgL_ddz00_5973 = BgL_ddz00_19599; 
BgL_mmz00_5972 = BgL_mmz00_19598; 
BgL_yyyyz00_5971 = BgL_yyyyz00_19597; 
BgL_iportz00_5970 = BgL_iportz00_19596; 
goto BgL_zc3z04anonymousza34084ze3z87_5978;} } } break;case 0L : 

{ /* Llib/date.scm 860 */
 long BgL_b1z00_5920; long BgL_b2z00_5921; long BgL_b3z00_5922;
{ /* Llib/date.scm 860 */
 int BgL_arg4079z00_5930;
{ /* Llib/date.scm 858 */
 int BgL_tmpz00_19604;
BgL_tmpz00_19604 = 
(int)(1L); 
BgL_arg4079z00_5930 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5666, BgL_tmpz00_19604); } 
BgL_b1z00_5920 = 
(
(long)(BgL_arg4079z00_5930)-48L); } 
{ /* Llib/date.scm 861 */
 int BgL_arg4080z00_5931;
{ /* Llib/date.scm 858 */
 int BgL_tmpz00_19609;
BgL_tmpz00_19609 = 
(int)(2L); 
BgL_arg4080z00_5931 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5666, BgL_tmpz00_19609); } 
BgL_b2z00_5921 = 
(
(long)(BgL_arg4080z00_5931)-48L); } 
{ /* Llib/date.scm 862 */
 int BgL_arg4081z00_5932;
{ /* Llib/date.scm 858 */
 int BgL_tmpz00_19614;
BgL_tmpz00_19614 = 
(int)(3L); 
BgL_arg4081z00_5932 = 
RGC_BUFFER_BYTE_REF(BgL_iportz00_5666, BgL_tmpz00_19614); } 
BgL_b3z00_5922 = 
(
(long)(BgL_arg4081z00_5932)-48L); } 
{ /* Llib/date.scm 863 */
 BGL_LONGLONG_T BgL_arg4073z00_5924;
{ /* Llib/date.scm 865 */
 BGL_LONGLONG_T BgL_arg4074z00_5925;
{ /* Llib/date.scm 865 */
 long BgL_tmpz00_19619;
BgL_tmpz00_19619 = 
(
(
(
(BgL_b1z00_5920*10L)+BgL_b2z00_5921)*10L)+BgL_b3z00_5922); 
BgL_arg4074z00_5925 = 
LONG_TO_LLONG(BgL_tmpz00_19619); } 
BgL_arg4073z00_5924 = 
((16960 + ((BGL_LONGLONG_T)65536 * (((BGL_LONGLONG_T)15))))*BgL_arg4074z00_5925); } 
{ 
 BGL_LONGLONG_T BgL_sssz00_19633; long BgL_ssz00_19632; long BgL_mmz00_19631; long BgL_hhz00_19630; long BgL_ddz00_19629; long BgL_mmz00_19628; long BgL_yyyyz00_19627; obj_t BgL_iportz00_19626;
BgL_iportz00_19626 = BgL_iportz00_5666; 
BgL_yyyyz00_19627 = BgL_yyyyz00_5667; 
BgL_mmz00_19628 = BgL_mmz00_5668; 
BgL_ddz00_19629 = BgL_ddz00_5669; 
BgL_hhz00_19630 = BgL_hhz00_5670; 
BgL_mmz00_19631 = BgL_mmz00_5671; 
BgL_ssz00_19632 = BgL_ssz00_5672; 
BgL_sssz00_19633 = BgL_arg4073z00_5924; 
BgL_sssz00_5977 = BgL_sssz00_19633; 
BgL_ssz00_5976 = BgL_ssz00_19632; 
BgL_mmz00_5975 = BgL_mmz00_19631; 
BgL_hhz00_5974 = BgL_hhz00_19630; 
BgL_ddz00_5973 = BgL_ddz00_19629; 
BgL_mmz00_5972 = BgL_mmz00_19628; 
BgL_yyyyz00_5971 = BgL_yyyyz00_19627; 
BgL_iportz00_5970 = BgL_iportz00_19626; 
goto BgL_zc3z04anonymousza34084ze3z87_5978;} } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_5902)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_5605)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_5325)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_5046)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_4766)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_4489)); } } } } } break;
default: 
BgL_aux5737z00_10846 = 
BGl_errorz00zz__errorz00(BGl_string5790z00zz__datez00, BGl_string5791z00zz__datez00, 
BINT(BgL_matchz00_4221)); } } } } 
if(
BGL_DATEP(BgL_aux5737z00_10846))
{ /* Llib/date.scm 959 */
return BgL_aux5737z00_10846;}  else 
{ 
 obj_t BgL_auxz00_19668;
BgL_auxz00_19668 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(40342L), BGl_string6011z00zz__datez00, BGl_string5940z00zz__datez00, BgL_aux5737z00_10846); 
FAILURE(BgL_auxz00_19668,BFALSE,BFALSE);} } } } } } } } } } } } 

}



/* the-failure~0 */
obj_t BGl_thezd2failureze70z35zz__datez00(obj_t BgL_iportz00_9871)
{
{ /* Llib/date.scm 818 */
{ /* Llib/date.scm 818 */
 bool_t BgL_test7393z00_19672;
{ /* Llib/date.scm 818 */
 long BgL_arg4358z00_6539;
BgL_arg4358z00_6539 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9871); 
BgL_test7393z00_19672 = 
(BgL_arg4358z00_6539==0L); } 
if(BgL_test7393z00_19672)
{ /* Llib/date.scm 818 */
return BEOF;}  else 
{ /* Llib/date.scm 818 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9871));} } } 

}



/* the-failure~1 */
obj_t BGl_thezd2failureze71z35zz__datez00(obj_t BgL_iportz00_9872)
{
{ /* Llib/date.scm 835 */
{ /* Llib/date.scm 835 */
 bool_t BgL_test7394z00_19677;
{ /* Llib/date.scm 835 */
 long BgL_arg4219z00_6233;
BgL_arg4219z00_6233 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9872); 
BgL_test7394z00_19677 = 
(BgL_arg4219z00_6233==0L); } 
if(BgL_test7394z00_19677)
{ /* Llib/date.scm 835 */
return BEOF;}  else 
{ /* Llib/date.scm 835 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9872));} } } 

}



/* the-failure~2 */
obj_t BGl_thezd2failureze72z35zz__datez00(obj_t BgL_iportz00_9873)
{
{ /* Llib/date.scm 858 */
{ /* Llib/date.scm 858 */
 bool_t BgL_test7395z00_19682;
{ /* Llib/date.scm 858 */
 long BgL_arg4059z00_5892;
BgL_arg4059z00_5892 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9873); 
BgL_test7395z00_19682 = 
(BgL_arg4059z00_5892==0L); } 
if(BgL_test7395z00_19682)
{ /* Llib/date.scm 858 */
return BEOF;}  else 
{ /* Llib/date.scm 858 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9873));} } } 

}



/* the-failure~3 */
obj_t BGl_thezd2failureze73z35zz__datez00(obj_t BgL_iportz00_9874)
{
{ /* Llib/date.scm 876 */
{ /* Llib/date.scm 876 */
 bool_t BgL_test7396z00_19687;
{ /* Llib/date.scm 876 */
 long BgL_arg3919z00_5595;
BgL_arg3919z00_5595 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9874); 
BgL_test7396z00_19687 = 
(BgL_arg3919z00_5595==0L); } 
if(BgL_test7396z00_19687)
{ /* Llib/date.scm 876 */
return BEOF;}  else 
{ /* Llib/date.scm 876 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9874));} } } 

}



/* the-failure~4 */
obj_t BGl_thezd2failureze74z35zz__datez00(obj_t BgL_iportz00_9875)
{
{ /* Llib/date.scm 892 */
{ /* Llib/date.scm 892 */
 bool_t BgL_test7397z00_19692;
{ /* Llib/date.scm 892 */
 long BgL_arg3788z00_5315;
BgL_arg3788z00_5315 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9875); 
BgL_test7397z00_19692 = 
(BgL_arg3788z00_5315==0L); } 
if(BgL_test7397z00_19692)
{ /* Llib/date.scm 892 */
return BEOF;}  else 
{ /* Llib/date.scm 892 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9875));} } } 

}



/* the-failure~5 */
obj_t BGl_thezd2failureze75z35zz__datez00(obj_t BgL_iportz00_9876)
{
{ /* Llib/date.scm 906 */
{ /* Llib/date.scm 906 */
 bool_t BgL_test7398z00_19697;
{ /* Llib/date.scm 906 */
 long BgL_arg3641z00_5036;
BgL_arg3641z00_5036 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9876); 
BgL_test7398z00_19697 = 
(BgL_arg3641z00_5036==0L); } 
if(BgL_test7398z00_19697)
{ /* Llib/date.scm 906 */
return BEOF;}  else 
{ /* Llib/date.scm 906 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9876));} } } 

}



/* the-failure~6 */
obj_t BGl_thezd2failureze76z35zz__datez00(obj_t BgL_iportz00_9877)
{
{ /* Llib/date.scm 921 */
{ /* Llib/date.scm 921 */
 bool_t BgL_test7399z00_19702;
{ /* Llib/date.scm 921 */
 long BgL_arg3494z00_4756;
BgL_arg3494z00_4756 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9877); 
BgL_test7399z00_19702 = 
(BgL_arg3494z00_4756==0L); } 
if(BgL_test7399z00_19702)
{ /* Llib/date.scm 921 */
return BEOF;}  else 
{ /* Llib/date.scm 921 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9877));} } } 

}



/* the-failure~7 */
obj_t BGl_thezd2failureze77z35zz__datez00(obj_t BgL_iportz00_9878)
{
{ /* Llib/date.scm 935 */
{ /* Llib/date.scm 935 */
 bool_t BgL_test7400z00_19707;
{ /* Llib/date.scm 935 */
 long BgL_arg3315z00_4479;
BgL_arg3315z00_4479 = 
RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_9878); 
BgL_test7400z00_19707 = 
(BgL_arg3315z00_4479==0L); } 
if(BgL_test7400z00_19707)
{ /* Llib/date.scm 935 */
return BEOF;}  else 
{ /* Llib/date.scm 935 */
return 
BCHAR(
RGC_BUFFER_CHARACTER(BgL_iportz00_9878));} } } 

}



/* &iso8601-parse-date */
obj_t BGl_z62iso8601zd2parsezd2datez62zz__datez00(obj_t BgL_envz00_9869, obj_t BgL_ipz00_9870)
{
{ /* Llib/date.scm 815 */
{ /* Llib/date.scm 818 */
 obj_t BgL_auxz00_19712;
if(
INPUT_PORTP(BgL_ipz00_9870))
{ /* Llib/date.scm 818 */
BgL_auxz00_19712 = BgL_ipz00_9870
; }  else 
{ 
 obj_t BgL_auxz00_19715;
BgL_auxz00_19715 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(35982L), BGl_string6023z00zz__datez00, BGl_string5786z00zz__datez00, BgL_ipz00_9870); 
FAILURE(BgL_auxz00_19715,BFALSE,BFALSE);} 
return 
BGl_iso8601zd2parsezd2datez00zz__datez00(BgL_auxz00_19712);} } 

}



/* parse-error */
obj_t BGl_parsezd2errorzd2zz__datez00(obj_t BgL_procz00_96, obj_t BgL_messagez00_97, obj_t BgL_objz00_98, obj_t BgL_portz00_99)
{
{ /* Llib/date.scm 964 */
{ /* Llib/date.scm 966 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_arg4381z00_6622;
{ /* Llib/date.scm 966 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1150z00_6623;
{ /* Llib/date.scm 972 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1149z00_6634;
BgL_new1149z00_6634 = 
((BgL_z62iozd2parsezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2parsezd2errorz62_bgl) ))); 
{ /* Llib/date.scm 972 */
 long BgL_arg4392z00_6635;
BgL_arg4392z00_6635 = 
BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1149z00_6634), BgL_arg4392z00_6635); } 
BgL_new1150z00_6623 = BgL_new1149z00_6634; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1150z00_6623)))->BgL_fnamez00)=((obj_t)
INPUT_PORT_NAME(BgL_portz00_99)),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1150z00_6623)))->BgL_locationz00)=((obj_t)
BINT(
INPUT_PORT_FILEPOS(BgL_portz00_99))),BUNSPEC); 
{ 
 obj_t BgL_auxz00_19731;
{ /* Llib/date.scm 973 */
 obj_t BgL_arg4382z00_6624;
{ /* Llib/date.scm 973 */
 obj_t BgL_arg4383z00_6625;
{ /* Llib/date.scm 973 */
 obj_t BgL_classz00_9424;
BgL_classz00_9424 = BGl_z62iozd2parsezd2errorz62zz__objectz00; 
BgL_arg4383z00_6625 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_9424); } 
BgL_arg4382z00_6624 = 
VECTOR_REF(BgL_arg4383z00_6625,2L); } 
{ /* Llib/date.scm 973 */
 obj_t BgL_auxz00_19735;
if(
BGl_classzd2fieldzf3z21zz__objectz00(BgL_arg4382z00_6624))
{ /* Llib/date.scm 973 */
BgL_auxz00_19735 = BgL_arg4382z00_6624
; }  else 
{ 
 obj_t BgL_auxz00_19738;
BgL_auxz00_19738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string5784z00zz__datez00, 
BINT(40870L), BGl_string6024z00zz__datez00, BGl_string5807z00zz__datez00, BgL_arg4382z00_6624); 
FAILURE(BgL_auxz00_19738,BFALSE,BFALSE);} 
BgL_auxz00_19731 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_auxz00_19735); } } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1150z00_6623)))->BgL_stackz00)=((obj_t)BgL_auxz00_19731),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1150z00_6623)))->BgL_procz00)=((obj_t)BgL_procz00_96),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1150z00_6623)))->BgL_msgz00)=((obj_t)BgL_messagez00_97),BUNSPEC); 
{ 
 obj_t BgL_auxz00_19748;
if(
CHARP(BgL_objz00_98))
{ /* Llib/date.scm 970 */
 obj_t BgL_arg4385z00_6627; obj_t BgL_arg4386z00_6628;
{ /* Llib/date.scm 970 */
 obj_t BgL_list4391z00_6633;
BgL_list4391z00_6633 = 
MAKE_YOUNG_PAIR(BgL_objz00_98, BNIL); 
BgL_arg4385z00_6627 = 
BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_list4391z00_6633); } 
BgL_arg4386z00_6628 = 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_portz00_99); 
{ /* Llib/date.scm 970 */
 obj_t BgL_list4387z00_6629;
{ /* Llib/date.scm 970 */
 obj_t BgL_arg4388z00_6630;
{ /* Llib/date.scm 970 */
 obj_t BgL_arg4389z00_6631;
{ /* Llib/date.scm 970 */
 obj_t BgL_arg4390z00_6632;
BgL_arg4390z00_6632 = 
MAKE_YOUNG_PAIR(BgL_arg4386z00_6628, BNIL); 
BgL_arg4389z00_6631 = 
MAKE_YOUNG_PAIR(BGl_string6025z00zz__datez00, BgL_arg4390z00_6632); } 
BgL_arg4388z00_6630 = 
MAKE_YOUNG_PAIR(BgL_arg4385z00_6627, BgL_arg4389z00_6631); } 
BgL_list4387z00_6629 = 
MAKE_YOUNG_PAIR(BGl_string6026z00zz__datez00, BgL_arg4388z00_6630); } 
BgL_auxz00_19748 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list4387z00_6629); } }  else 
{ /* Llib/date.scm 969 */
BgL_auxz00_19748 = BgL_objz00_98
; } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1150z00_6623)))->BgL_objz00)=((obj_t)BgL_auxz00_19748),BUNSPEC); } 
BgL_arg4381z00_6622 = BgL_new1150z00_6623; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg4381z00_6622));} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__datez00(void)
{
{ /* Llib/date.scm 19 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__datez00(void)
{
{ /* Llib/date.scm 19 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__datez00(void)
{
{ /* Llib/date.scm 19 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__datez00(void)
{
{ /* Llib/date.scm 19 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string6027z00zz__datez00));} 

}

#ifdef __cplusplus
}
#endif
