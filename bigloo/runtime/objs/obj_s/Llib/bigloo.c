/*===========================================================================*/
/*   (Llib/bigloo.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/bigloo.scm -indent -o objs/obj_s/Llib/bigloo.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BIGLOO_TYPE_DEFINITIONS
#define BGL___BIGLOO_TYPE_DEFINITIONS
#endif // BGL___BIGLOO_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_symbol2008z00zz__biglooz00 = BUNSPEC;
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
static obj_t BGl_z62bigloozd2classzd2demanglez62zz__biglooz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_cnstzf3zf3zz__biglooz00(obj_t);
static obj_t BGl_z62bigloozd2classzd2mangledzf3z91zz__biglooz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_opaquezf3zf3zz__biglooz00(obj_t);
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_z62opaquezf3z91zz__biglooz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__biglooz00 = BUNSPEC;
static obj_t BGl_keyword2069z00zz__biglooz00 = BUNSPEC;
extern obj_t bgl_remq_bang(obj_t, obj_t);
static obj_t BGl_z62bigloozd2modulezd2demanglez62zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62unregisterzd2exitzd2functionz12z70zz__biglooz00(obj_t, obj_t);
static obj_t BGl_za2levelza2z00zz__biglooz00 = BUNSPEC;
static obj_t BGl_z62timez62zz__biglooz00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static long BGl_getzd28bitszd2integerze70ze7zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2needzd2manglingzf3z91zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62cellzf3z91zz__biglooz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bigloo_demangle(obj_t);
static long BGl_charzd2ze3digitze70zd6zz__biglooz00(unsigned char);
static obj_t BGl_z62bigloozd2demanglezb0zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2modulezd2manglez62zz__biglooz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62bigloozd2exitzd2applyz62zz__biglooz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t bigloo_mangledp(obj_t);
static obj_t BGl_z62bigloozd2mangledzf3z43zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62bigloozd2exitzd2mutexz62zz__biglooz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_gczd2finaliza7ez75zz__biglooz00(void);
static obj_t BGl_z62nullzd2orzd2unspecifiedzf3z91zz__biglooz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_procedurezd2attrzd2setz12z12zz__biglooz00(obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__biglooz00(void);
static obj_t BGl_za2exitzd2mutexza2zd2zz__biglooz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_timez00zz__biglooz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_checkzd2versionz12zc0zz__biglooz00(obj_t, char *, obj_t);
static obj_t BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(obj_t, long, obj_t);
static obj_t BGl_z62cnstzf3z91zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62cellzd2setz12za2zz__biglooz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2055z00zz__biglooz00 = BUNSPEC;
static obj_t BGl_symbol2057z00zz__biglooz00 = BUNSPEC;
static obj_t BGl_symbol2059z00zz__biglooz00 = BUNSPEC;
static obj_t BGl__gcz00zz__biglooz00(obj_t, obj_t);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_unspecifiedz00zz__biglooz00(void);
BGL_EXPORTED_DECL obj_t bigloo_module_mangle(obj_t, obj_t);
extern unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
static obj_t BGl_cnstzd2initzd2zz__biglooz00(void);
static obj_t BGl_genericzd2initzd2zz__biglooz00(void);
static obj_t BGl_symbol2061z00zz__biglooz00 = BUNSPEC;
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_symbol2064z00zz__biglooz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_unregisterzd2exitzd2functionz12z12zz__biglooz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_nullzd2orzd2unspecifiedzf3zf3zz__biglooz00(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__biglooz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__biglooz00(void);
static obj_t BGl_z62bmemzd2resetz12za2zz__biglooz00(obj_t);
static obj_t BGl_objectzd2initzd2zz__biglooz00(void);
BGL_EXPORTED_DECL obj_t BGl_opaquezd2nilzd2zz__biglooz00(void);
BGL_EXPORTED_DECL obj_t bigloo_mangle(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cellzd2refzd2zz__biglooz00(obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bigloo_exit_apply(obj_t);
BGL_EXPORTED_DECL int BGl_procedurezd2lengthzd2zz__biglooz00(obj_t);
BGL_EXPORTED_DECL obj_t bigloo_module_demangle(obj_t);
static obj_t BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 = BUNSPEC;
extern obj_t string_append_3(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_exit_mutex(void);
static obj_t BGl_list2054z00zz__biglooz00 = BUNSPEC;
extern void bgl_gc_verbose_set(bool_t);
static obj_t BGl_za2releaseza2z00zz__biglooz00 = BUNSPEC;
static obj_t BGl_z62procedurezd2attrzb0zz__biglooz00(obj_t, obj_t);
extern obj_t bgl_bmem_reset(void);
BGL_EXPORTED_DECL obj_t BGl_cellzd2setz12zc0zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62opaquezd2nilzb0zz__biglooz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_gczd2verbosezd2setz12z12zz__biglooz00(bool_t);
static obj_t BGl_list2068z00zz__biglooz00 = BUNSPEC;
static obj_t BGl_z62gczd2verbosezd2setz12z70zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62makezd2cellzb0zz__biglooz00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_registerzd2exitzd2functionz12z12zz__biglooz00(obj_t);
static obj_t BGl_methodzd2initzd2zz__biglooz00(void);
static obj_t BGl_z62bigloozd2manglezb0zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62registerzd2exitzd2functionz12z70zz__biglooz00(obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL obj_t bigloo_class_demangle(obj_t);
static obj_t BGl_z62gczd2finaliza7ez17zz__biglooz00(obj_t);
static obj_t BGl_z62procedurezd2attrzd2setz12z70zz__biglooz00(obj_t, obj_t, obj_t);
static long BGl_manglezd2atz12zc0zz__biglooz00(obj_t, obj_t, long, long);
BGL_EXPORTED_DECL bool_t bigloo_class_mangledp(obj_t);
extern obj_t make_string(long, unsigned char);
BGL_EXPORTED_DECL obj_t BGl_makezd2cellzd2zz__biglooz00(obj_t);
static obj_t BGl_za2modulesza2z00zz__biglooz00 = BUNSPEC;
static obj_t BGl_z62cellzd2refzb0zz__biglooz00(obj_t, obj_t);
static obj_t BGl_z62unspecifiedz62zz__biglooz00(obj_t);
static obj_t BGl_z62procedurezd2lengthzb0zz__biglooz00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_procedurezd2arityzd2zz__biglooz00(obj_t);
extern obj_t string_append(obj_t, obj_t);
static obj_t BGl_z62procedurezd2arityzb0zz__biglooz00(obj_t, obj_t);
extern obj_t BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(obj_t, long, obj_t, long, long);
BGL_EXPORTED_DECL obj_t BGl_procedurezd2attrzd2zz__biglooz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_bmemzd2resetz12zc0zz__biglooz00(void);
static obj_t BGl_z62checkzd2versionz12za2zz__biglooz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_cellzf3zf3zz__biglooz00(obj_t);
extern bool_t bigloo_strncmp(obj_t, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_gcz00zz__biglooz00(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string2040z00zz__biglooz00, BgL_bgl_string2040za700za7za7_2074za7, "bigloo-module-demangle", 22 );
DEFINE_STRING( BGl_string2041z00zz__biglooz00, BgL_bgl_string2041za700za7za7_2075za7, "@", 1 );
DEFINE_STRING( BGl_string2042z00zz__biglooz00, BgL_bgl_string2042za700za7za7_2076za7, "&bigloo-module-demangle", 23 );
DEFINE_STRING( BGl_string2043z00zz__biglooz00, BgL_bgl_string2043za700za7za7_2077za7, "&bigloo-class-mangled?", 22 );
DEFINE_STRING( BGl_string2044z00zz__biglooz00, BgL_bgl_string2044za700za7za7_2078za7, "bigloo-class-demangle", 21 );
DEFINE_STRING( BGl_string2045z00zz__biglooz00, BgL_bgl_string2045za700za7za7_2079za7, "_bglt", 5 );
DEFINE_STRING( BGl_string2046z00zz__biglooz00, BgL_bgl_string2046za700za7za7_2080za7, "&bigloo-class-demangle", 22 );
DEFINE_STRING( BGl_string2047z00zz__biglooz00, BgL_bgl_string2047za700za7za7_2081za7, "bigloo-exit-register!", 21 );
DEFINE_STRING( BGl_string2048z00zz__biglooz00, BgL_bgl_string2048za700za7za7_2082za7, "Wrong procedure arity", 21 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unspecifiedzd2envzd2zz__biglooz00, BgL_bgl_za762unspecifiedza762083z00, BGl_z62unspecifiedz62zz__biglooz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2049z00zz__biglooz00, BgL_bgl_string2049za700za7za7_2084za7, "&register-exit-function!", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_unregisterzd2exitzd2functionz12zd2envzc0zz__biglooz00, BgL_bgl_za762unregisterza7d22085z00, BGl_z62unregisterzd2exitzd2functionz12z70zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2050z00zz__biglooz00, BgL_bgl_string2050za700za7za7_2086za7, "unregister-exit-function!", 25 );
DEFINE_STRING( BGl_string2051z00zz__biglooz00, BgL_bgl_string2051za700za7za7_2087za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string2052z00zz__biglooz00, BgL_bgl_string2052za700za7za7_2088za7, "&unregister-exit-function!", 26 );
DEFINE_STRING( BGl_string2053z00zz__biglooz00, BgL_bgl_string2053za700za7za7_2089za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string2056z00zz__biglooz00, BgL_bgl_string2056za700za7za7_2090za7, "funcall", 7 );
DEFINE_STRING( BGl_string2058z00zz__biglooz00, BgL_bgl_string2058za700za7za7_2091za7, "fun", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_procedurezd2attrzd2envz00zz__biglooz00, BgL_bgl_za762procedureza7d2a2092z00, BGl_z62procedurezd2attrzb0zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_opaquezd2nilzd2envz00zz__biglooz00, BgL_bgl_za762opaqueza7d2nilza72093za7, BGl_z62opaquezd2nilzb0zz__biglooz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2060z00zz__biglooz00, BgL_bgl_string2060za700za7za7_2094za7, "val", 3 );
DEFINE_STRING( BGl_string2062z00zz__biglooz00, BgL_bgl_string2062za700za7za7_2095za7, "time", 4 );
DEFINE_STRING( BGl_string2063z00zz__biglooz00, BgL_bgl_string2063za700za7za7_2096za7, "&time", 5 );
DEFINE_STRING( BGl_string2065z00zz__biglooz00, BgL_bgl_string2065za700za7za7_2097za7, "gc", 2 );
DEFINE_STRING( BGl_string2066z00zz__biglooz00, BgL_bgl_string2066za700za7za7_2098za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string2067z00zz__biglooz00, BgL_bgl_string2067za700za7za7_2099za7, "vector-ref", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2mangledzf3zd2envzf3zz__biglooz00, BgL_bgl_za762biglooza7d2mang2100z00, BGl_z62bigloozd2mangledzf3z43zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2exitzd2mutexzd2envzd2zz__biglooz00, BgL_bgl_za762biglooza7d2exit2101z00, BGl_z62bigloozd2exitzd2mutexz62zz__biglooz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2070z00zz__biglooz00, BgL_bgl_string2070za700za7za7_2102za7, "finalize", 8 );
DEFINE_STRING( BGl_string2071z00zz__biglooz00, BgL_bgl_string2071za700za7za7_2103za7, "Illegal keyword argument", 24 );
DEFINE_STRING( BGl_string2072z00zz__biglooz00, BgL_bgl_string2072za700za7za7_2104za7, "_gc", 3 );
DEFINE_STRING( BGl_string2073z00zz__biglooz00, BgL_bgl_string2073za700za7za7_2105za7, "__bigloo", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_gczd2verbosezd2setz12zd2envzc0zz__biglooz00, BgL_bgl_za762gcza7d2verboseza72106za7, BGl_z62gczd2verbosezd2setz12z70zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2modulezd2manglezd2envzd2zz__biglooz00, BgL_bgl_za762biglooza7d2modu2107z00, BGl_z62bigloozd2modulezd2manglez62zz__biglooz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_nullzd2orzd2unspecifiedzf3zd2envz21zz__biglooz00, BgL_bgl_za762nullza7d2orza7d2u2108za7, BGl_z62nullzd2orzd2unspecifiedzf3z91zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bmemzd2resetz12zd2envz12zz__biglooz00, BgL_bgl_za762bmemza7d2resetza72109za7, BGl_z62bmemzd2resetz12za2zz__biglooz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2needzd2manglingzf3zd2envz21zz__biglooz00, BgL_bgl_za762biglooza7d2need2110z00, BGl_z62bigloozd2needzd2manglingzf3z91zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_gczd2envzd2zz__biglooz00, BgL_bgl__gcza700za7za7__bigloo2111za7, opt_generic_entry, BGl__gcz00zz__biglooz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_procedurezd2lengthzd2envz00zz__biglooz00, BgL_bgl_za762procedureza7d2l2112z00, BGl_z62procedurezd2lengthzb0zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_gczd2finaliza7ezd2envza7zz__biglooz00, BgL_bgl_za762gcza7d2finaliza7a2113za7, BGl_z62gczd2finaliza7ez17zz__biglooz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2classzd2mangledzf3zd2envz21zz__biglooz00, BgL_bgl_za762biglooza7d2clas2114z00, BGl_z62bigloozd2classzd2mangledzf3z91zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_opaquezf3zd2envz21zz__biglooz00, BgL_bgl_za762opaqueza7f3za791za72115z00, BGl_z62opaquezf3z91zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2cellzd2envz00zz__biglooz00, BgL_bgl_za762makeza7d2cellza7b2116za7, BGl_z62makezd2cellzb0zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_procedurezd2arityzd2envz00zz__biglooz00, BgL_bgl_za762procedureza7d2a2117z00, BGl_z62procedurezd2arityzb0zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2manglezd2envz00zz__biglooz00, BgL_bgl_za762biglooza7d2mang2118z00, BGl_z62bigloozd2manglezb0zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cellzf3zd2envz21zz__biglooz00, BgL_bgl_za762cellza7f3za791za7za7_2119za7, BGl_z62cellzf3z91zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_registerzd2exitzd2functionz12zd2envzc0zz__biglooz00, BgL_bgl_za762registerza7d2ex2120z00, BGl_z62registerzd2exitzd2functionz12z70zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2demanglezd2envz00zz__biglooz00, BgL_bgl_za762biglooza7d2dema2121z00, BGl_z62bigloozd2demanglezb0zz__biglooz00, 0L, BUNSPEC, 1 );
extern obj_t BGl_stringzd2envzd2zz__r4_strings_6_7z00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2modulezd2demanglezd2envzd2zz__biglooz00, BgL_bgl_za762biglooza7d2modu2122z00, BGl_z62bigloozd2modulezd2demanglez62zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_checkzd2versionz12zd2envz12zz__biglooz00, BgL_bgl_za762checkza7d2versi2123z00, BGl_z62checkzd2versionz12za2zz__biglooz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cnstzf3zd2envz21zz__biglooz00, BgL_bgl_za762cnstza7f3za791za7za7_2124za7, BGl_z62cnstzf3z91zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2009z00zz__biglooz00, BgL_bgl_string2009za700za7za7_2125za7, "bigloo-exit", 11 );
DEFINE_STRING( BGl_string2010z00zz__biglooz00, BgL_bgl_string2010za700za7za7_2126za7, " (level 0)", 10 );
DEFINE_STRING( BGl_string2011z00zz__biglooz00, BgL_bgl_string2011za700za7za7_2127za7, "/tmp/bigloo/runtime/Llib/bigloo.scm", 35 );
DEFINE_STRING( BGl_string2012z00zz__biglooz00, BgL_bgl_string2012za700za7za7_2128za7, "string-set!", 11 );
DEFINE_STRING( BGl_string2013z00zz__biglooz00, BgL_bgl_string2013za700za7za7_2129za7, "Some modules have been compiled by: ", 36 );
DEFINE_STRING( BGl_string2014z00zz__biglooz00, BgL_bgl_string2014za700za7za7_2130za7, "and other by: ", 14 );
DEFINE_STRING( BGl_string2015z00zz__biglooz00, BgL_bgl_string2015za700za7za7_2131za7, "&check-version!", 15 );
DEFINE_STRING( BGl_string2016z00zz__biglooz00, BgL_bgl_string2016za700za7za7_2132za7, "bstring", 7 );
DEFINE_STRING( BGl_string2017z00zz__biglooz00, BgL_bgl_string2017za700za7za7_2133za7, "&procedure-arity", 16 );
DEFINE_STRING( BGl_string2018z00zz__biglooz00, BgL_bgl_string2018za700za7za7_2134za7, "procedure", 9 );
DEFINE_STRING( BGl_string2019z00zz__biglooz00, BgL_bgl_string2019za700za7za7_2135za7, "&procedure-length", 17 );
DEFINE_STRING( BGl_string2020z00zz__biglooz00, BgL_bgl_string2020za700za7za7_2136za7, "&procedure-attr", 15 );
DEFINE_STRING( BGl_string2021z00zz__biglooz00, BgL_bgl_string2021za700za7za7_2137za7, "&procedure-attr-set!", 20 );
DEFINE_STRING( BGl_string2022z00zz__biglooz00, BgL_bgl_string2022za700za7za7_2138za7, "0123456789abcdef", 16 );
DEFINE_STRING( BGl_string2023z00zz__biglooz00, BgL_bgl_string2023za700za7za7_2139za7, "string-ref", 10 );
DEFINE_STRING( BGl_string2024z00zz__biglooz00, BgL_bgl_string2024za700za7za7_2140za7, "bigloo-mangle-string", 20 );
DEFINE_STRING( BGl_string2025z00zz__biglooz00, BgL_bgl_string2025za700za7za7_2141za7, "Can't mangle empty string", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cellzd2refzd2envz00zz__biglooz00, BgL_bgl_za762cellza7d2refza7b02142za7, BGl_z62cellzd2refzb0zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2026z00zz__biglooz00, BgL_bgl_string2026za700za7za7_2143za7, "bigloo-mangle", 13 );
DEFINE_STRING( BGl_string2027z00zz__biglooz00, BgL_bgl_string2027za700za7za7_2144za7, "BgL_", 4 );
DEFINE_STRING( BGl_string2028z00zz__biglooz00, BgL_bgl_string2028za700za7za7_2145za7, "&bigloo-mangle", 14 );
DEFINE_STRING( BGl_string2029z00zz__biglooz00, BgL_bgl_string2029za700za7za7_2146za7, "bigloo-module-mangle", 20 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2exitzd2applyzd2envzd2zz__biglooz00, BgL_bgl_za762biglooza7d2exit2147z00, BGl_z62bigloozd2exitzd2applyz62zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2030z00zz__biglooz00, BgL_bgl_string2030za700za7za7_2148za7, "BGl_", 4 );
DEFINE_STRING( BGl_string2031z00zz__biglooz00, BgL_bgl_string2031za700za7za7_2149za7, "&bigloo-module-mangle", 21 );
DEFINE_STRING( BGl_string2032z00zz__biglooz00, BgL_bgl_string2032za700za7za7_2150za7, "&bigloo-mangled?", 16 );
DEFINE_STRING( BGl_string2033z00zz__biglooz00, BgL_bgl_string2033za700za7za7_2151za7, "&bigloo-need-mangling?", 22 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2classzd2demanglezd2envzd2zz__biglooz00, BgL_bgl_za762biglooza7d2clas2152z00, BGl_z62bigloozd2classzd2demanglez62zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2034z00zz__biglooz00, BgL_bgl_string2034za700za7za7_2153za7, "get-8bits-integer~0", 19 );
DEFINE_STRING( BGl_string2035z00zz__biglooz00, BgL_bgl_string2035za700za7za7_2154za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_procedurezd2attrzd2setz12zd2envzc0zz__biglooz00, BgL_bgl_za762procedureza7d2a2155z00, BGl_z62procedurezd2attrzd2setz12z70zz__biglooz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2036z00zz__biglooz00, BgL_bgl_string2036za700za7za7_2156za7, "loop", 4 );
DEFINE_STRING( BGl_string2037z00zz__biglooz00, BgL_bgl_string2037za700za7za7_2157za7, "bigloo-demangle", 15 );
DEFINE_STRING( BGl_string2038z00zz__biglooz00, BgL_bgl_string2038za700za7za7_2158za7, "Illegal mangling on", 19 );
DEFINE_STRING( BGl_string2039z00zz__biglooz00, BgL_bgl_string2039za700za7za7_2159za7, "&bigloo-demangle", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_timezd2envzd2zz__biglooz00, BgL_bgl_za762timeza762za7za7__bi2160z00, BGl_z62timez62zz__biglooz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_cellzd2setz12zd2envz12zz__biglooz00, BgL_bgl_za762cellza7d2setza7122161za7, BGl_z62cellzd2setz12za2zz__biglooz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol2008z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_keyword2069z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_za2levelza2z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_za2exitzd2mutexza2zd2zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_symbol2055z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_symbol2057z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_symbol2059z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_symbol2061z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_symbol2064z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_list2054z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_za2releaseza2z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_list2068z00zz__biglooz00) );
ADD_ROOT( (void *)(&BGl_za2modulesza2z00zz__biglooz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long BgL_checksumz00_2578, char * BgL_fromz00_2579)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__biglooz00))
{ 
BGl_requirezd2initializa7ationz75zz__biglooz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__biglooz00(); 
BGl_cnstzd2initzd2zz__biglooz00(); 
BGl_importedzd2moduleszd2initz00zz__biglooz00(); 
return 
BGl_toplevelzd2initzd2zz__biglooz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
BGl_symbol2008z00zz__biglooz00 = 
bstring_to_symbol(BGl_string2009z00zz__biglooz00); 
BGl_symbol2055z00zz__biglooz00 = 
bstring_to_symbol(BGl_string2056z00zz__biglooz00); 
BGl_symbol2057z00zz__biglooz00 = 
bstring_to_symbol(BGl_string2058z00zz__biglooz00); 
BGl_symbol2059z00zz__biglooz00 = 
bstring_to_symbol(BGl_string2060z00zz__biglooz00); 
BGl_list2054z00zz__biglooz00 = 
MAKE_YOUNG_PAIR(BGl_symbol2055z00zz__biglooz00, 
MAKE_YOUNG_PAIR(BGl_symbol2057z00zz__biglooz00, 
MAKE_YOUNG_PAIR(BGl_symbol2057z00zz__biglooz00, 
MAKE_YOUNG_PAIR(BGl_symbol2059z00zz__biglooz00, BNIL)))); 
BGl_symbol2061z00zz__biglooz00 = 
bstring_to_symbol(BGl_string2062z00zz__biglooz00); 
BGl_symbol2064z00zz__biglooz00 = 
bstring_to_symbol(BGl_string2065z00zz__biglooz00); 
BGl_keyword2069z00zz__biglooz00 = 
bstring_to_keyword(BGl_string2070z00zz__biglooz00); 
return ( 
BGl_list2068z00zz__biglooz00 = 
MAKE_YOUNG_PAIR(BGl_keyword2069z00zz__biglooz00, BNIL), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
BGl_za2releaseza2z00zz__biglooz00 = BFALSE; 
BGl_za2levelza2z00zz__biglooz00 = BFALSE; 
BGl_za2modulesza2z00zz__biglooz00 = BNIL; 
BGl_za2exitzd2mutexza2zd2zz__biglooz00 = 
bgl_make_mutex(BGl_symbol2008z00zz__biglooz00); 
return ( 
BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 = BNIL, BUNSPEC) ;} 

}



/* check-version! */
BGL_EXPORTED_DEF obj_t BGl_checkzd2versionz12zc0zz__biglooz00(obj_t BgL_modulez00_3, char * BgL_releasez00_4, obj_t BgL_levelz00_5)
{
{ /* Llib/bigloo.scm 339 */
if(
STRINGP(BGl_za2releaseza2z00zz__biglooz00))
{ /* Llib/bigloo.scm 345 */
 bool_t BgL_test2164z00_2603;
{ /* Llib/bigloo.scm 345 */
 bool_t BgL_test2165z00_2604;
{ /* Llib/bigloo.scm 345 */
 long BgL_minz00_1145;
{ /* Llib/bigloo.scm 345 */
 long BgL_arg1212z00_1148;
{ /* Llib/bigloo.scm 345 */
 long BgL_az00_1149; long BgL_bz00_1150;
BgL_az00_1149 = 
STRING_LENGTH(
string_to_bstring(BgL_releasez00_4)); 
BgL_bz00_1150 = 
STRING_LENGTH(BGl_za2releaseza2z00zz__biglooz00); 
if(
(BgL_az00_1149<BgL_bz00_1150))
{ /* Llib/bigloo.scm 346 */
BgL_arg1212z00_1148 = BgL_az00_1149; }  else 
{ /* Llib/bigloo.scm 346 */
BgL_arg1212z00_1148 = BgL_bz00_1150; } } 
BgL_minz00_1145 = 
(BgL_arg1212z00_1148-1L); } 
{ /* Llib/bigloo.scm 348 */
 obj_t BgL_arg1209z00_1146; obj_t BgL_arg1210z00_1147;
BgL_arg1209z00_1146 = 
BGl_substringz00zz__r4_strings_6_7z00(
string_to_bstring(BgL_releasez00_4), 0L, BgL_minz00_1145); 
BgL_arg1210z00_1147 = 
BGl_substringz00zz__r4_strings_6_7z00(BGl_za2releaseza2z00zz__biglooz00, 0L, BgL_minz00_1145); 
{ /* Llib/bigloo.scm 348 */
 long BgL_l1z00_1831;
BgL_l1z00_1831 = 
STRING_LENGTH(BgL_arg1209z00_1146); 
if(
(BgL_l1z00_1831==
STRING_LENGTH(BgL_arg1210z00_1147)))
{ /* Llib/bigloo.scm 348 */
 int BgL_arg1474z00_1834;
{ /* Llib/bigloo.scm 348 */
 char * BgL_auxz00_2620; char * BgL_tmpz00_2618;
BgL_auxz00_2620 = 
BSTRING_TO_STRING(BgL_arg1210z00_1147); 
BgL_tmpz00_2618 = 
BSTRING_TO_STRING(BgL_arg1209z00_1146); 
BgL_arg1474z00_1834 = 
memcmp(BgL_tmpz00_2618, BgL_auxz00_2620, BgL_l1z00_1831); } 
BgL_test2165z00_2604 = 
(
(long)(BgL_arg1474z00_1834)==0L); }  else 
{ /* Llib/bigloo.scm 348 */
BgL_test2165z00_2604 = ((bool_t)0)
; } } } } 
if(BgL_test2165z00_2604)
{ /* Llib/bigloo.scm 345 */
if(
CHARP(BgL_levelz00_5))
{ /* Llib/bigloo.scm 350 */
if(
CHARP(BGl_za2levelza2z00zz__biglooz00))
{ /* Llib/bigloo.scm 350 */
if(
(
CCHAR(BGl_za2levelza2z00zz__biglooz00)==
CCHAR(BgL_levelz00_5)))
{ /* Llib/bigloo.scm 350 */
BgL_test2164z00_2603 = ((bool_t)0)
; }  else 
{ /* Llib/bigloo.scm 350 */
BgL_test2164z00_2603 = ((bool_t)1)
; } }  else 
{ /* Llib/bigloo.scm 350 */
BgL_test2164z00_2603 = ((bool_t)0)
; } }  else 
{ /* Llib/bigloo.scm 350 */
BgL_test2164z00_2603 = ((bool_t)0)
; } }  else 
{ /* Llib/bigloo.scm 345 */
BgL_test2164z00_2603 = ((bool_t)1)
; } } 
if(BgL_test2164z00_2603)
{ /* Llib/bigloo.scm 358 */
 obj_t BgL_arg1200z00_1130; obj_t BgL_arg1201z00_1131; obj_t BgL_arg1202z00_1132;
{ /* Llib/bigloo.scm 358 */
 obj_t BgL_arg1203z00_1133;
{ /* Llib/bigloo.scm 358 */
 obj_t BgL_releasez00_1847; obj_t BgL_levelz00_1848;
BgL_releasez00_1847 = BGl_za2releaseza2z00zz__biglooz00; 
BgL_levelz00_1848 = BGl_za2levelza2z00zz__biglooz00; 
if(
CHARP(BgL_levelz00_1848))
{ /* Llib/bigloo.scm 353 */
 obj_t BgL_sz00_1850;
BgL_sz00_1850 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BGl_string2010z00zz__biglooz00); 
{ /* Llib/bigloo.scm 354 */
 unsigned char BgL_charz00_1852;
BgL_charz00_1852 = 
CCHAR(BgL_levelz00_1848); 
{ /* Llib/bigloo.scm 354 */
 long BgL_l1802z00_2362;
BgL_l1802z00_2362 = 
STRING_LENGTH(BgL_sz00_1850); 
if(
BOUND_CHECK(8L, BgL_l1802z00_2362))
{ /* Llib/bigloo.scm 354 */
STRING_SET(BgL_sz00_1850, 8L, BgL_charz00_1852); }  else 
{ 
 obj_t BgL_auxz00_2641;
BgL_auxz00_2641 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(13053L), BGl_string2012z00zz__biglooz00, BgL_sz00_1850, 
(int)(BgL_l1802z00_2362), 
(int)(8L)); 
FAILURE(BgL_auxz00_2641,BFALSE,BFALSE);} } } 
BgL_arg1203z00_1133 = 
string_append(BgL_releasez00_1847, BgL_sz00_1850); }  else 
{ /* Llib/bigloo.scm 352 */
BgL_arg1203z00_1133 = BgL_releasez00_1847; } } 
BgL_arg1200z00_1130 = 
string_append(BGl_string2013z00zz__biglooz00, BgL_arg1203z00_1133); } 
{ /* Llib/bigloo.scm 360 */
 obj_t BgL_arg1206z00_1134;
if(
CHARP(BgL_levelz00_5))
{ /* Llib/bigloo.scm 353 */
 obj_t BgL_sz00_1854;
BgL_sz00_1854 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BGl_string2010z00zz__biglooz00); 
{ /* Llib/bigloo.scm 354 */
 unsigned char BgL_charz00_1856;
BgL_charz00_1856 = 
CCHAR(BgL_levelz00_5); 
{ /* Llib/bigloo.scm 354 */
 long BgL_l1806z00_2366;
BgL_l1806z00_2366 = 
STRING_LENGTH(BgL_sz00_1854); 
if(
BOUND_CHECK(8L, BgL_l1806z00_2366))
{ /* Llib/bigloo.scm 354 */
STRING_SET(BgL_sz00_1854, 8L, BgL_charz00_1856); }  else 
{ 
 obj_t BgL_auxz00_2657;
BgL_auxz00_2657 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(13053L), BGl_string2012z00zz__biglooz00, BgL_sz00_1854, 
(int)(BgL_l1806z00_2366), 
(int)(8L)); 
FAILURE(BgL_auxz00_2657,BFALSE,BFALSE);} } } 
BgL_arg1206z00_1134 = 
string_append(
string_to_bstring(BgL_releasez00_4), BgL_sz00_1854); }  else 
{ /* Llib/bigloo.scm 352 */
BgL_arg1206z00_1134 = 
string_to_bstring(BgL_releasez00_4); } 
BgL_arg1201z00_1131 = 
string_append(BGl_string2014z00zz__biglooz00, BgL_arg1206z00_1134); } 
BgL_arg1202z00_1132 = 
MAKE_YOUNG_PAIR(BgL_modulez00_3, BGl_za2modulesza2z00zz__biglooz00); 
return 
BGl_errorz00zz__errorz00(BgL_arg1200z00_1130, BgL_arg1201z00_1131, BgL_arg1202z00_1132);}  else 
{ /* Llib/bigloo.scm 345 */
return ( 
BGl_za2modulesza2z00zz__biglooz00 = 
MAKE_YOUNG_PAIR(BgL_modulez00_3, BGl_za2modulesza2z00zz__biglooz00), BUNSPEC) ;} }  else 
{ /* Llib/bigloo.scm 341 */
{ /* Llib/bigloo.scm 342 */
 obj_t BgL_list1213z00_1152;
BgL_list1213z00_1152 = 
MAKE_YOUNG_PAIR(BgL_modulez00_3, BNIL); 
BGl_za2modulesza2z00zz__biglooz00 = BgL_list1213z00_1152; } 
BGl_za2releaseza2z00zz__biglooz00 = 
string_to_bstring(BgL_releasez00_4); 
return ( 
BGl_za2levelza2z00zz__biglooz00 = BgL_levelz00_5, BUNSPEC) ;} } 

}



/* &check-version! */
obj_t BGl_z62checkzd2versionz12za2zz__biglooz00(obj_t BgL_envz00_2295, obj_t BgL_modulez00_2296, obj_t BgL_releasez00_2297, obj_t BgL_levelz00_2298)
{
{ /* Llib/bigloo.scm 339 */
{ /* Llib/bigloo.scm 341 */
 char * BgL_auxz00_2672;
{ /* Llib/bigloo.scm 341 */
 obj_t BgL_tmpz00_2673;
if(
STRINGP(BgL_releasez00_2297))
{ /* Llib/bigloo.scm 341 */
BgL_tmpz00_2673 = BgL_releasez00_2297
; }  else 
{ 
 obj_t BgL_auxz00_2676;
BgL_auxz00_2676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(12535L), BGl_string2015z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_releasez00_2297); 
FAILURE(BgL_auxz00_2676,BFALSE,BFALSE);} 
BgL_auxz00_2672 = 
BSTRING_TO_STRING(BgL_tmpz00_2673); } 
return 
BGl_checkzd2versionz12zc0zz__biglooz00(BgL_modulez00_2296, BgL_auxz00_2672, BgL_levelz00_2298);} } 

}



/* procedure-arity */
BGL_EXPORTED_DEF int BGl_procedurezd2arityzd2zz__biglooz00(obj_t BgL_procz00_6)
{
{ /* Llib/bigloo.scm 375 */
return 
PROCEDURE_ARITY(BgL_procz00_6);} 

}



/* &procedure-arity */
obj_t BGl_z62procedurezd2arityzb0zz__biglooz00(obj_t BgL_envz00_2299, obj_t BgL_procz00_2300)
{
{ /* Llib/bigloo.scm 375 */
{ /* Llib/bigloo.scm 376 */
 int BgL_tmpz00_2683;
{ /* Llib/bigloo.scm 376 */
 obj_t BgL_auxz00_2684;
if(
PROCEDUREP(BgL_procz00_2300))
{ /* Llib/bigloo.scm 376 */
BgL_auxz00_2684 = BgL_procz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_2687;
BgL_auxz00_2687 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(13952L), BGl_string2017z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_procz00_2300); 
FAILURE(BgL_auxz00_2687,BFALSE,BFALSE);} 
BgL_tmpz00_2683 = 
BGl_procedurezd2arityzd2zz__biglooz00(BgL_auxz00_2684); } 
return 
BINT(BgL_tmpz00_2683);} } 

}



/* procedure-length */
BGL_EXPORTED_DEF int BGl_procedurezd2lengthzd2zz__biglooz00(obj_t BgL_procz00_7)
{
{ /* Llib/bigloo.scm 381 */
return 
PROCEDURE_LENGTH(BgL_procz00_7);} 

}



/* &procedure-length */
obj_t BGl_z62procedurezd2lengthzb0zz__biglooz00(obj_t BgL_envz00_2301, obj_t BgL_procz00_2302)
{
{ /* Llib/bigloo.scm 381 */
{ /* Llib/bigloo.scm 382 */
 int BgL_tmpz00_2694;
{ /* Llib/bigloo.scm 382 */
 obj_t BgL_auxz00_2695;
if(
PROCEDUREP(BgL_procz00_2302))
{ /* Llib/bigloo.scm 382 */
BgL_auxz00_2695 = BgL_procz00_2302
; }  else 
{ 
 obj_t BgL_auxz00_2698;
BgL_auxz00_2698 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(14242L), BGl_string2019z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_procz00_2302); 
FAILURE(BgL_auxz00_2698,BFALSE,BFALSE);} 
BgL_tmpz00_2694 = 
BGl_procedurezd2lengthzd2zz__biglooz00(BgL_auxz00_2695); } 
return 
BINT(BgL_tmpz00_2694);} } 

}



/* procedure-attr */
BGL_EXPORTED_DEF obj_t BGl_procedurezd2attrzd2zz__biglooz00(obj_t BgL_procz00_8)
{
{ /* Llib/bigloo.scm 387 */
return 
PROCEDURE_ATTR(BgL_procz00_8);} 

}



/* &procedure-attr */
obj_t BGl_z62procedurezd2attrzb0zz__biglooz00(obj_t BgL_envz00_2303, obj_t BgL_procz00_2304)
{
{ /* Llib/bigloo.scm 387 */
{ /* Llib/bigloo.scm 388 */
 obj_t BgL_auxz00_2705;
if(
PROCEDUREP(BgL_procz00_2304))
{ /* Llib/bigloo.scm 388 */
BgL_auxz00_2705 = BgL_procz00_2304
; }  else 
{ 
 obj_t BgL_auxz00_2708;
BgL_auxz00_2708 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(14531L), BGl_string2020z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_procz00_2304); 
FAILURE(BgL_auxz00_2708,BFALSE,BFALSE);} 
return 
BGl_procedurezd2attrzd2zz__biglooz00(BgL_auxz00_2705);} } 

}



/* procedure-attr-set! */
BGL_EXPORTED_DEF obj_t BGl_procedurezd2attrzd2setz12z12zz__biglooz00(obj_t BgL_procz00_9, obj_t BgL_objz00_10)
{
{ /* Llib/bigloo.scm 393 */
PROCEDURE_ATTR_SET(BgL_procz00_9, BgL_objz00_10); 
return BgL_objz00_10;} 

}



/* &procedure-attr-set! */
obj_t BGl_z62procedurezd2attrzd2setz12z70zz__biglooz00(obj_t BgL_envz00_2305, obj_t BgL_procz00_2306, obj_t BgL_objz00_2307)
{
{ /* Llib/bigloo.scm 393 */
{ /* Llib/bigloo.scm 394 */
 obj_t BgL_auxz00_2714;
if(
PROCEDUREP(BgL_procz00_2306))
{ /* Llib/bigloo.scm 394 */
BgL_auxz00_2714 = BgL_procz00_2306
; }  else 
{ 
 obj_t BgL_auxz00_2717;
BgL_auxz00_2717 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(14857L), BGl_string2021z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_procz00_2306); 
FAILURE(BgL_auxz00_2717,BFALSE,BFALSE);} 
return 
BGl_procedurezd2attrzd2setz12z12zz__biglooz00(BgL_auxz00_2714, BgL_objz00_2307);} } 

}



/* unspecified */
BGL_EXPORTED_DEF obj_t BGl_unspecifiedz00zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 400 */
return BUNSPEC;} 

}



/* &unspecified */
obj_t BGl_z62unspecifiedz62zz__biglooz00(obj_t BgL_envz00_2308)
{
{ /* Llib/bigloo.scm 400 */
return 
BGl_unspecifiedz00zz__biglooz00();} 

}



/* null-or-unspecified? */
BGL_EXPORTED_DEF bool_t BGl_nullzd2orzd2unspecifiedzf3zf3zz__biglooz00(obj_t BgL_objz00_11)
{
{ /* Llib/bigloo.scm 406 */
return 
BGL_NULL_OR_UNSPECIFIEDP(BgL_objz00_11);} 

}



/* &null-or-unspecified? */
obj_t BGl_z62nullzd2orzd2unspecifiedzf3z91zz__biglooz00(obj_t BgL_envz00_2309, obj_t BgL_objz00_2310)
{
{ /* Llib/bigloo.scm 406 */
return 
BBOOL(
BGl_nullzd2orzd2unspecifiedzf3zf3zz__biglooz00(BgL_objz00_2310));} 

}



/* cnst? */
BGL_EXPORTED_DEF bool_t BGl_cnstzf3zf3zz__biglooz00(obj_t BgL_objz00_12)
{
{ /* Llib/bigloo.scm 418 */
return 
CNSTP(BgL_objz00_12);} 

}



/* &cnst? */
obj_t BGl_z62cnstzf3z91zz__biglooz00(obj_t BgL_envz00_2311, obj_t BgL_objz00_2312)
{
{ /* Llib/bigloo.scm 418 */
return 
BBOOL(
BGl_cnstzf3zf3zz__biglooz00(BgL_objz00_2312));} 

}



/* opaque? */
BGL_EXPORTED_DEF bool_t BGl_opaquezf3zf3zz__biglooz00(obj_t BgL_objz00_13)
{
{ /* Llib/bigloo.scm 424 */
return 
OPAQUEP(BgL_objz00_13);} 

}



/* &opaque? */
obj_t BGl_z62opaquezf3z91zz__biglooz00(obj_t BgL_envz00_2313, obj_t BgL_objz00_2314)
{
{ /* Llib/bigloo.scm 424 */
return 
BBOOL(
BGl_opaquezf3zf3zz__biglooz00(BgL_objz00_2314));} 

}



/* opaque-nil */
BGL_EXPORTED_DEF obj_t BGl_opaquezd2nilzd2zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 430 */
return 
BGL_OPAQUE_NIL();} 

}



/* &opaque-nil */
obj_t BGl_z62opaquezd2nilzb0zz__biglooz00(obj_t BgL_envz00_2315)
{
{ /* Llib/bigloo.scm 430 */
return 
BGl_opaquezd2nilzd2zz__biglooz00();} 

}



/* mangle-at! */
long BGl_manglezd2atz12zc0zz__biglooz00(obj_t BgL_newz00_15, obj_t BgL_oldz00_16, long BgL_lenz00_17, long BgL_offsetz00_18)
{
{ /* Llib/bigloo.scm 443 */
{ 
 long BgL_rz00_1155; long BgL_wz00_1156; long BgL_newzd2lenzd2_1157; long BgL_checksumz00_1158;
BgL_rz00_1155 = 0L; 
BgL_wz00_1156 = BgL_offsetz00_18; 
BgL_newzd2lenzd2_1157 = BgL_offsetz00_18; 
BgL_checksumz00_1158 = 0L; 
BgL_zc3z04anonymousza31214ze3z87_1159:
if(
(BgL_rz00_1155==BgL_lenz00_17))
{ /* Llib/bigloo.scm 448 */
{ /* Llib/bigloo.scm 450 */
 long BgL_l1810z00_2370;
BgL_l1810z00_2370 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_wz00_1156, BgL_l1810z00_2370))
{ /* Llib/bigloo.scm 450 */
STRING_SET(BgL_newz00_15, BgL_wz00_1156, ((unsigned char)'z')); }  else 
{ 
 obj_t BgL_auxz00_2740;
BgL_auxz00_2740 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17159L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1810z00_2370), 
(int)(BgL_wz00_1156)); 
FAILURE(BgL_auxz00_2740,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 452 */
 long BgL_arg1216z00_1161; unsigned char BgL_arg1218z00_1162;
BgL_arg1216z00_1161 = 
(BgL_wz00_1156+1L); 
{ /* Llib/bigloo.scm 437 */
 long BgL_i1813z00_2373;
BgL_i1813z00_2373 = 
(BgL_checksumz00_1158 & 15L); 
if(
BOUND_CHECK(BgL_i1813z00_2373, 16L))
{ /* Llib/bigloo.scm 438 */
BgL_arg1218z00_1162 = 
STRING_REF(BGl_string2022z00zz__biglooz00, BgL_i1813z00_2373); }  else 
{ 
 obj_t BgL_auxz00_2751;
BgL_auxz00_2751 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(16751L), BGl_string2023z00zz__biglooz00, BGl_string2022z00zz__biglooz00, 
(int)(16L), 
(int)(BgL_i1813z00_2373)); 
FAILURE(BgL_auxz00_2751,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 451 */
 long BgL_l1818z00_2378;
BgL_l1818z00_2378 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_arg1216z00_1161, BgL_l1818z00_2378))
{ /* Llib/bigloo.scm 451 */
STRING_SET(BgL_newz00_15, BgL_arg1216z00_1161, BgL_arg1218z00_1162); }  else 
{ 
 obj_t BgL_auxz00_2761;
BgL_auxz00_2761 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17189L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1818z00_2378), 
(int)(BgL_arg1216z00_1161)); 
FAILURE(BgL_auxz00_2761,BFALSE,BFALSE);} } } 
{ /* Llib/bigloo.scm 455 */
 long BgL_arg1220z00_1164; unsigned char BgL_arg1221z00_1165;
BgL_arg1220z00_1164 = 
(BgL_wz00_1156+2L); 
{ /* Llib/bigloo.scm 437 */
 long BgL_i1821z00_2381;
BgL_i1821z00_2381 = 
(
(BgL_checksumz00_1158 >> 
(int)(4L)) & 15L); 
if(
BOUND_CHECK(BgL_i1821z00_2381, 16L))
{ /* Llib/bigloo.scm 438 */
BgL_arg1221z00_1165 = 
STRING_REF(BGl_string2022z00zz__biglooz00, BgL_i1821z00_2381); }  else 
{ 
 obj_t BgL_auxz00_2774;
BgL_auxz00_2774 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(16751L), BGl_string2023z00zz__biglooz00, BGl_string2022z00zz__biglooz00, 
(int)(16L), 
(int)(BgL_i1821z00_2381)); 
FAILURE(BgL_auxz00_2774,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 454 */
 long BgL_l1826z00_2386;
BgL_l1826z00_2386 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_arg1220z00_1164, BgL_l1826z00_2386))
{ /* Llib/bigloo.scm 454 */
STRING_SET(BgL_newz00_15, BgL_arg1220z00_1164, BgL_arg1221z00_1165); }  else 
{ 
 obj_t BgL_auxz00_2784;
BgL_auxz00_2784 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17269L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1826z00_2386), 
(int)(BgL_arg1220z00_1164)); 
FAILURE(BgL_auxz00_2784,BFALSE,BFALSE);} } } 
return 
(BgL_wz00_1156+3L);}  else 
{ /* Llib/bigloo.scm 458 */
 unsigned char BgL_cz00_1168;
{ /* Llib/bigloo.scm 458 */
 long BgL_l1830z00_2390;
BgL_l1830z00_2390 = 
STRING_LENGTH(BgL_oldz00_16); 
if(
BOUND_CHECK(BgL_rz00_1155, BgL_l1830z00_2390))
{ /* Llib/bigloo.scm 458 */
BgL_cz00_1168 = 
STRING_REF(BgL_oldz00_16, BgL_rz00_1155); }  else 
{ 
 obj_t BgL_auxz00_2795;
BgL_auxz00_2795 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17384L), BGl_string2023z00zz__biglooz00, BgL_oldz00_16, 
(int)(BgL_l1830z00_2390), 
(int)(BgL_rz00_1155)); 
FAILURE(BgL_auxz00_2795,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 459 */
 bool_t BgL_test2187z00_2801;
{ /* Llib/bigloo.scm 459 */
 bool_t BgL_test2188z00_2802;
if(
isalpha(BgL_cz00_1168))
{ /* Llib/bigloo.scm 459 */
if(
(BgL_cz00_1168==((unsigned char)'z')))
{ /* Llib/bigloo.scm 459 */
BgL_test2188z00_2802 = ((bool_t)0)
; }  else 
{ /* Llib/bigloo.scm 459 */
BgL_test2188z00_2802 = ((bool_t)1)
; } }  else 
{ /* Llib/bigloo.scm 459 */
BgL_test2188z00_2802 = ((bool_t)0)
; } 
if(BgL_test2188z00_2802)
{ /* Llib/bigloo.scm 459 */
BgL_test2187z00_2801 = ((bool_t)1)
; }  else 
{ /* Llib/bigloo.scm 459 */
if(
isdigit(BgL_cz00_1168))
{ /* Llib/bigloo.scm 460 */
BgL_test2187z00_2801 = ((bool_t)1)
; }  else 
{ /* Llib/bigloo.scm 460 */
BgL_test2187z00_2801 = 
(BgL_cz00_1168==((unsigned char)'_'))
; } } } 
if(BgL_test2187z00_2801)
{ /* Llib/bigloo.scm 459 */
{ /* Llib/bigloo.scm 463 */
 long BgL_l1834z00_2394;
BgL_l1834z00_2394 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_wz00_1156, BgL_l1834z00_2394))
{ /* Llib/bigloo.scm 463 */
STRING_SET(BgL_newz00_15, BgL_wz00_1156, BgL_cz00_1168); }  else 
{ 
 obj_t BgL_auxz00_2814;
BgL_auxz00_2814 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17532L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1834z00_2394), 
(int)(BgL_wz00_1156)); 
FAILURE(BgL_auxz00_2814,BFALSE,BFALSE);} } 
{ 
 long BgL_newzd2lenzd2_2824; long BgL_wz00_2822; long BgL_rz00_2820;
BgL_rz00_2820 = 
(BgL_rz00_1155+1L); 
BgL_wz00_2822 = 
(BgL_wz00_1156+1L); 
BgL_newzd2lenzd2_2824 = 
(BgL_newzd2lenzd2_1157+1L); 
BgL_newzd2lenzd2_1157 = BgL_newzd2lenzd2_2824; 
BgL_wz00_1156 = BgL_wz00_2822; 
BgL_rz00_1155 = BgL_rz00_2820; 
goto BgL_zc3z04anonymousza31214ze3z87_1159;} }  else 
{ /* Llib/bigloo.scm 468 */
 long BgL_icz00_1179;
BgL_icz00_1179 = 
(BgL_cz00_1168); 
{ /* Llib/bigloo.scm 469 */
 long BgL_l1838z00_2398;
BgL_l1838z00_2398 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_wz00_1156, BgL_l1838z00_2398))
{ /* Llib/bigloo.scm 469 */
STRING_SET(BgL_newz00_15, BgL_wz00_1156, ((unsigned char)'z')); }  else 
{ 
 obj_t BgL_auxz00_2831;
BgL_auxz00_2831 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17667L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1838z00_2398), 
(int)(BgL_wz00_1156)); 
FAILURE(BgL_auxz00_2831,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 471 */
 long BgL_arg1234z00_1180; unsigned char BgL_arg1236z00_1181;
BgL_arg1234z00_1180 = 
(BgL_wz00_1156+1L); 
{ /* Llib/bigloo.scm 437 */
 long BgL_i1841z00_2401;
BgL_i1841z00_2401 = 
(BgL_icz00_1179 & 15L); 
if(
BOUND_CHECK(BgL_i1841z00_2401, 16L))
{ /* Llib/bigloo.scm 438 */
BgL_arg1236z00_1181 = 
STRING_REF(BGl_string2022z00zz__biglooz00, BgL_i1841z00_2401); }  else 
{ 
 obj_t BgL_auxz00_2842;
BgL_auxz00_2842 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(16751L), BGl_string2023z00zz__biglooz00, BGl_string2022z00zz__biglooz00, 
(int)(16L), 
(int)(BgL_i1841z00_2401)); 
FAILURE(BgL_auxz00_2842,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 470 */
 long BgL_l1846z00_2406;
BgL_l1846z00_2406 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_arg1234z00_1180, BgL_l1846z00_2406))
{ /* Llib/bigloo.scm 470 */
STRING_SET(BgL_newz00_15, BgL_arg1234z00_1180, BgL_arg1236z00_1181); }  else 
{ 
 obj_t BgL_auxz00_2852;
BgL_auxz00_2852 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17697L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1846z00_2406), 
(int)(BgL_arg1234z00_1180)); 
FAILURE(BgL_auxz00_2852,BFALSE,BFALSE);} } } 
{ /* Llib/bigloo.scm 474 */
 long BgL_arg1239z00_1183; unsigned char BgL_arg1242z00_1184;
BgL_arg1239z00_1183 = 
(BgL_wz00_1156+2L); 
{ /* Llib/bigloo.scm 437 */
 long BgL_i1849z00_2409;
BgL_i1849z00_2409 = 
(
(BgL_icz00_1179 >> 
(int)(4L)) & 15L); 
if(
BOUND_CHECK(BgL_i1849z00_2409, 16L))
{ /* Llib/bigloo.scm 438 */
BgL_arg1242z00_1184 = 
STRING_REF(BGl_string2022z00zz__biglooz00, BgL_i1849z00_2409); }  else 
{ 
 obj_t BgL_auxz00_2865;
BgL_auxz00_2865 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(16751L), BGl_string2023z00zz__biglooz00, BGl_string2022z00zz__biglooz00, 
(int)(16L), 
(int)(BgL_i1849z00_2409)); 
FAILURE(BgL_auxz00_2865,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 473 */
 long BgL_l1854z00_2414;
BgL_l1854z00_2414 = 
STRING_LENGTH(BgL_newz00_15); 
if(
BOUND_CHECK(BgL_arg1239z00_1183, BgL_l1854z00_2414))
{ /* Llib/bigloo.scm 473 */
STRING_SET(BgL_newz00_15, BgL_arg1239z00_1183, BgL_arg1242z00_1184); }  else 
{ 
 obj_t BgL_auxz00_2875;
BgL_auxz00_2875 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(17771L), BGl_string2012z00zz__biglooz00, BgL_newz00_15, 
(int)(BgL_l1854z00_2414), 
(int)(BgL_arg1239z00_1183)); 
FAILURE(BgL_auxz00_2875,BFALSE,BFALSE);} } } 
{ 
 long BgL_checksumz00_2887; long BgL_newzd2lenzd2_2885; long BgL_wz00_2883; long BgL_rz00_2881;
BgL_rz00_2881 = 
(BgL_rz00_1155+1L); 
BgL_wz00_2883 = 
(BgL_wz00_1156+3L); 
BgL_newzd2lenzd2_2885 = 
(BgL_newzd2lenzd2_1157+3L); 
BgL_checksumz00_2887 = 
(BgL_checksumz00_1158 ^ 
(BgL_cz00_1168)); 
BgL_checksumz00_1158 = BgL_checksumz00_2887; 
BgL_newzd2lenzd2_1157 = BgL_newzd2lenzd2_2885; 
BgL_wz00_1156 = BgL_wz00_2883; 
BgL_rz00_1155 = BgL_rz00_2881; 
goto BgL_zc3z04anonymousza31214ze3z87_1159;} } } } } } 

}



/* bigloo-mangle */
BGL_EXPORTED_DEF obj_t bigloo_mangle(obj_t BgL_stringz00_19)
{
{ /* Llib/bigloo.scm 484 */
{ /* Llib/bigloo.scm 485 */
 long BgL_lenz00_1197;
BgL_lenz00_1197 = 
STRING_LENGTH(BgL_stringz00_19); 
{ /* Llib/bigloo.scm 485 */
 obj_t BgL_newz00_1198;
{ /* Llib/bigloo.scm 486 */
 long BgL_arg1304z00_1201;
BgL_arg1304z00_1201 = 
(
(BgL_lenz00_1197*3L)+7L); 
{ /* Ieee/string.scm 172 */

BgL_newz00_1198 = 
make_string(BgL_arg1304z00_1201, ((unsigned char)' ')); } } 
{ /* Llib/bigloo.scm 486 */

if(
(BgL_lenz00_1197==0L))
{ /* Llib/bigloo.scm 488 */
 obj_t BgL_aux1958z00_2519;
BgL_aux1958z00_2519 = 
BGl_errorz00zz__errorz00(BGl_string2024z00zz__biglooz00, BGl_string2025z00zz__biglooz00, BgL_stringz00_19); 
if(
STRINGP(BgL_aux1958z00_2519))
{ /* Llib/bigloo.scm 488 */
return BgL_aux1958z00_2519;}  else 
{ 
 obj_t BgL_auxz00_2899;
BgL_auxz00_2899 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(18320L), BGl_string2026z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_aux1958z00_2519); 
FAILURE(BgL_auxz00_2899,BFALSE,BFALSE);} }  else 
{ /* Llib/bigloo.scm 489 */
 long BgL_stopz00_1200;
BgL_stopz00_1200 = 
BGl_manglezd2atz12zc0zz__biglooz00(BgL_newz00_1198, BgL_stringz00_19, BgL_lenz00_1197, 4L); 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BGl_string2027z00zz__biglooz00, 0L, BgL_newz00_1198, 0L, 4L); 
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_newz00_1198, 0L, BgL_stopz00_1200);} } } } } 

}



/* &bigloo-mangle */
obj_t BGl_z62bigloozd2manglezb0zz__biglooz00(obj_t BgL_envz00_2316, obj_t BgL_stringz00_2317)
{
{ /* Llib/bigloo.scm 484 */
{ /* Llib/bigloo.scm 485 */
 obj_t BgL_auxz00_2906;
if(
STRINGP(BgL_stringz00_2317))
{ /* Llib/bigloo.scm 485 */
BgL_auxz00_2906 = BgL_stringz00_2317
; }  else 
{ 
 obj_t BgL_auxz00_2909;
BgL_auxz00_2909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(18215L), BGl_string2028z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2317); 
FAILURE(BgL_auxz00_2909,BFALSE,BFALSE);} 
return 
bigloo_mangle(BgL_auxz00_2906);} } 

}



/* bigloo-module-mangle */
BGL_EXPORTED_DEF obj_t bigloo_module_mangle(obj_t BgL_idz00_20, obj_t BgL_modulez00_21)
{
{ /* Llib/bigloo.scm 496 */
{ /* Llib/bigloo.scm 497 */
 long BgL_lenz00_1205;
BgL_lenz00_1205 = 
(
STRING_LENGTH(BgL_idz00_20)+
STRING_LENGTH(BgL_modulez00_21)); 
{ /* Llib/bigloo.scm 497 */
 obj_t BgL_newz00_1206;
{ /* Llib/bigloo.scm 498 */
 long BgL_arg1311z00_1214;
BgL_arg1311z00_1214 = 
(
(BgL_lenz00_1205*3L)+12L); 
{ /* Ieee/string.scm 172 */

BgL_newz00_1206 = 
make_string(BgL_arg1311z00_1214, ((unsigned char)' ')); } } 
{ /* Llib/bigloo.scm 498 */

if(
(BgL_lenz00_1205==0L))
{ /* Llib/bigloo.scm 500 */
 obj_t BgL_aux1962z00_2523;
BgL_aux1962z00_2523 = 
BGl_errorz00zz__errorz00(BGl_string2024z00zz__biglooz00, BGl_string2025z00zz__biglooz00, BGl_stringzd2envzd2zz__r4_strings_6_7z00); 
if(
STRINGP(BgL_aux1962z00_2523))
{ /* Llib/bigloo.scm 500 */
return BgL_aux1962z00_2523;}  else 
{ 
 obj_t BgL_auxz00_2925;
BgL_auxz00_2925 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(18902L), BGl_string2029z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_aux1962z00_2523); 
FAILURE(BgL_auxz00_2925,BFALSE,BFALSE);} }  else 
{ /* Llib/bigloo.scm 501 */
 long BgL_modzd2startzd2_1208;
BgL_modzd2startzd2_1208 = 
BGl_manglezd2atz12zc0zz__biglooz00(BgL_newz00_1206, BgL_idz00_20, 
STRING_LENGTH(BgL_idz00_20), 4L); 
{ /* Llib/bigloo.scm 502 */
 long BgL_l1858z00_2418;
BgL_l1858z00_2418 = 
STRING_LENGTH(BgL_newz00_1206); 
if(
BOUND_CHECK(BgL_modzd2startzd2_1208, BgL_l1858z00_2418))
{ /* Llib/bigloo.scm 502 */
STRING_SET(BgL_newz00_1206, BgL_modzd2startzd2_1208, ((unsigned char)'z')); }  else 
{ 
 obj_t BgL_auxz00_2935;
BgL_auxz00_2935 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19037L), BGl_string2012z00zz__biglooz00, BgL_newz00_1206, 
(int)(BgL_l1858z00_2418), 
(int)(BgL_modzd2startzd2_1208)); 
FAILURE(BgL_auxz00_2935,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 503 */
 long BgL_arg1307z00_1209;
BgL_arg1307z00_1209 = 
(1L+BgL_modzd2startzd2_1208); 
{ /* Llib/bigloo.scm 503 */
 long BgL_l1862z00_2422;
BgL_l1862z00_2422 = 
STRING_LENGTH(BgL_newz00_1206); 
if(
BOUND_CHECK(BgL_arg1307z00_1209, BgL_l1862z00_2422))
{ /* Llib/bigloo.scm 503 */
STRING_SET(BgL_newz00_1206, BgL_arg1307z00_1209, ((unsigned char)'z')); }  else 
{ 
 obj_t BgL_auxz00_2946;
BgL_auxz00_2946 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19075L), BGl_string2012z00zz__biglooz00, BgL_newz00_1206, 
(int)(BgL_l1862z00_2422), 
(int)(BgL_arg1307z00_1209)); 
FAILURE(BgL_auxz00_2946,BFALSE,BFALSE);} } } 
{ /* Llib/bigloo.scm 504 */
 long BgL_stopz00_1210;
BgL_stopz00_1210 = 
BGl_manglezd2atz12zc0zz__biglooz00(BgL_newz00_1206, BgL_modulez00_21, 
STRING_LENGTH(BgL_modulez00_21), 
(BgL_modzd2startzd2_1208+2L)); 
BGl_blitzd2stringz12zc0zz__r4_strings_6_7z00(BGl_string2030z00zz__biglooz00, 0L, BgL_newz00_1206, 0L, 4L); 
return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_newz00_1206, 0L, BgL_stopz00_1210);} } } } } } 

}



/* &bigloo-module-mangle */
obj_t BGl_z62bigloozd2modulezd2manglez62zz__biglooz00(obj_t BgL_envz00_2318, obj_t BgL_idz00_2319, obj_t BgL_modulez00_2320)
{
{ /* Llib/bigloo.scm 496 */
{ /* Llib/bigloo.scm 497 */
 obj_t BgL_auxz00_2964; obj_t BgL_auxz00_2957;
if(
STRINGP(BgL_modulez00_2320))
{ /* Llib/bigloo.scm 497 */
BgL_auxz00_2964 = BgL_modulez00_2320
; }  else 
{ 
 obj_t BgL_auxz00_2967;
BgL_auxz00_2967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(18771L), BGl_string2031z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_modulez00_2320); 
FAILURE(BgL_auxz00_2967,BFALSE,BFALSE);} 
if(
STRINGP(BgL_idz00_2319))
{ /* Llib/bigloo.scm 497 */
BgL_auxz00_2957 = BgL_idz00_2319
; }  else 
{ 
 obj_t BgL_auxz00_2960;
BgL_auxz00_2960 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(18771L), BGl_string2031z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_idz00_2319); 
FAILURE(BgL_auxz00_2960,BFALSE,BFALSE);} 
return 
bigloo_module_mangle(BgL_auxz00_2957, BgL_auxz00_2964);} } 

}



/* bigloo-mangled? */
BGL_EXPORTED_DEF bool_t bigloo_mangledp(obj_t BgL_stringz00_22)
{
{ /* Llib/bigloo.scm 513 */
{ /* Llib/bigloo.scm 514 */
 long BgL_lenz00_1220;
BgL_lenz00_1220 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
(BgL_lenz00_1220>7L))
{ /* Llib/bigloo.scm 516 */
 bool_t BgL_test2208z00_2975;
{ /* Llib/bigloo.scm 516 */
 bool_t BgL__ortest_1045z00_1237;
BgL__ortest_1045z00_1237 = 
bigloo_strncmp(BgL_stringz00_22, BGl_string2027z00zz__biglooz00, 4L); 
if(BgL__ortest_1045z00_1237)
{ /* Llib/bigloo.scm 516 */
BgL_test2208z00_2975 = BgL__ortest_1045z00_1237
; }  else 
{ /* Llib/bigloo.scm 516 */
BgL_test2208z00_2975 = 
bigloo_strncmp(BgL_stringz00_22, BGl_string2030z00zz__biglooz00, 4L)
; } } 
if(BgL_test2208z00_2975)
{ /* Llib/bigloo.scm 518 */
 bool_t BgL_test2210z00_2979;
{ /* Llib/bigloo.scm 518 */
 unsigned char BgL_tmpz00_2980;
{ /* Llib/bigloo.scm 518 */
 long BgL_i1865z00_2425;
BgL_i1865z00_2425 = 
(BgL_lenz00_1220-3L); 
if(
BOUND_CHECK(BgL_i1865z00_2425, BgL_lenz00_1220))
{ /* Llib/bigloo.scm 518 */
BgL_tmpz00_2980 = 
STRING_REF(BgL_stringz00_22, BgL_i1865z00_2425)
; }  else 
{ 
 obj_t BgL_auxz00_2985;
BgL_auxz00_2985 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19689L), BGl_string2023z00zz__biglooz00, BgL_stringz00_22, 
(int)(BgL_lenz00_1220), 
(int)(BgL_i1865z00_2425)); 
FAILURE(BgL_auxz00_2985,BFALSE,BFALSE);} } 
BgL_test2210z00_2979 = 
(BgL_tmpz00_2980==((unsigned char)'z')); } 
if(BgL_test2210z00_2979)
{ /* Llib/bigloo.scm 519 */
 bool_t BgL_test2212z00_2992;
{ /* Llib/bigloo.scm 519 */
 bool_t BgL__ortest_1046z00_1230;
{ /* Llib/bigloo.scm 519 */
 unsigned char BgL_arg1322z00_1233;
{ /* Llib/bigloo.scm 519 */
 long BgL_i1869z00_2429;
BgL_i1869z00_2429 = 
(BgL_lenz00_1220-2L); 
{ /* Llib/bigloo.scm 519 */
 long BgL_l1870z00_2430;
BgL_l1870z00_2430 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
BOUND_CHECK(BgL_i1869z00_2429, BgL_l1870z00_2430))
{ /* Llib/bigloo.scm 519 */
BgL_arg1322z00_1233 = 
STRING_REF(BgL_stringz00_22, BgL_i1869z00_2429); }  else 
{ 
 obj_t BgL_auxz00_2998;
BgL_auxz00_2998 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19752L), BGl_string2023z00zz__biglooz00, BgL_stringz00_22, 
(int)(BgL_l1870z00_2430), 
(int)(BgL_i1869z00_2429)); 
FAILURE(BgL_auxz00_2998,BFALSE,BFALSE);} } } 
BgL__ortest_1046z00_1230 = 
isalpha(BgL_arg1322z00_1233); } 
if(BgL__ortest_1046z00_1230)
{ /* Llib/bigloo.scm 519 */
BgL_test2212z00_2992 = BgL__ortest_1046z00_1230
; }  else 
{ /* Llib/bigloo.scm 520 */
 unsigned char BgL_arg1320z00_1231;
{ /* Llib/bigloo.scm 520 */
 long BgL_i1873z00_2433;
BgL_i1873z00_2433 = 
(BgL_lenz00_1220-2L); 
{ /* Llib/bigloo.scm 520 */
 long BgL_l1874z00_2434;
BgL_l1874z00_2434 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
BOUND_CHECK(BgL_i1873z00_2433, BgL_l1874z00_2434))
{ /* Llib/bigloo.scm 520 */
BgL_arg1320z00_1231 = 
STRING_REF(BgL_stringz00_22, BgL_i1873z00_2433); }  else 
{ 
 obj_t BgL_auxz00_3011;
BgL_auxz00_3011 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19808L), BGl_string2023z00zz__biglooz00, BgL_stringz00_22, 
(int)(BgL_l1874z00_2434), 
(int)(BgL_i1873z00_2433)); 
FAILURE(BgL_auxz00_3011,BFALSE,BFALSE);} } } 
BgL_test2212z00_2992 = 
isdigit(BgL_arg1320z00_1231); } } 
if(BgL_test2212z00_2992)
{ /* Llib/bigloo.scm 521 */
 bool_t BgL__ortest_1047z00_1225;
{ /* Llib/bigloo.scm 521 */
 unsigned char BgL_arg1318z00_1228;
{ /* Llib/bigloo.scm 521 */
 long BgL_i1877z00_2437;
BgL_i1877z00_2437 = 
(BgL_lenz00_1220-1L); 
{ /* Llib/bigloo.scm 521 */
 long BgL_l1878z00_2438;
BgL_l1878z00_2438 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
BOUND_CHECK(BgL_i1877z00_2437, BgL_l1878z00_2438))
{ /* Llib/bigloo.scm 521 */
BgL_arg1318z00_1228 = 
STRING_REF(BgL_stringz00_22, BgL_i1877z00_2437); }  else 
{ 
 obj_t BgL_auxz00_3023;
BgL_auxz00_3023 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19868L), BGl_string2023z00zz__biglooz00, BgL_stringz00_22, 
(int)(BgL_l1878z00_2438), 
(int)(BgL_i1877z00_2437)); 
FAILURE(BgL_auxz00_3023,BFALSE,BFALSE);} } } 
BgL__ortest_1047z00_1225 = 
isalpha(BgL_arg1318z00_1228); } 
if(BgL__ortest_1047z00_1225)
{ /* Llib/bigloo.scm 521 */
return BgL__ortest_1047z00_1225;}  else 
{ /* Llib/bigloo.scm 522 */
 unsigned char BgL_arg1316z00_1226;
{ /* Llib/bigloo.scm 522 */
 long BgL_i1881z00_2441;
BgL_i1881z00_2441 = 
(BgL_lenz00_1220-1L); 
{ /* Llib/bigloo.scm 522 */
 long BgL_l1882z00_2442;
BgL_l1882z00_2442 = 
STRING_LENGTH(BgL_stringz00_22); 
if(
BOUND_CHECK(BgL_i1881z00_2441, BgL_l1882z00_2442))
{ /* Llib/bigloo.scm 522 */
BgL_arg1316z00_1226 = 
STRING_REF(BgL_stringz00_22, BgL_i1881z00_2441); }  else 
{ 
 obj_t BgL_auxz00_3036;
BgL_auxz00_3036 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19924L), BGl_string2023z00zz__biglooz00, BgL_stringz00_22, 
(int)(BgL_l1882z00_2442), 
(int)(BgL_i1881z00_2441)); 
FAILURE(BgL_auxz00_3036,BFALSE,BFALSE);} } } 
return 
isdigit(BgL_arg1316z00_1226);} }  else 
{ /* Llib/bigloo.scm 519 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 518 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 516 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 515 */
return ((bool_t)0);} } } 

}



/* &bigloo-mangled? */
obj_t BGl_z62bigloozd2mangledzf3z43zz__biglooz00(obj_t BgL_envz00_2323, obj_t BgL_stringz00_2324)
{
{ /* Llib/bigloo.scm 513 */
{ /* Llib/bigloo.scm 514 */
 bool_t BgL_tmpz00_3043;
{ /* Llib/bigloo.scm 514 */
 obj_t BgL_auxz00_3044;
if(
STRINGP(BgL_stringz00_2324))
{ /* Llib/bigloo.scm 514 */
BgL_auxz00_3044 = BgL_stringz00_2324
; }  else 
{ 
 obj_t BgL_auxz00_3047;
BgL_auxz00_3047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(19541L), BGl_string2032z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2324); 
FAILURE(BgL_auxz00_3047,BFALSE,BFALSE);} 
BgL_tmpz00_3043 = 
bigloo_mangledp(BgL_auxz00_3044); } 
return 
BBOOL(BgL_tmpz00_3043);} } 

}



/* bigloo-need-mangling? */
BGL_EXPORTED_DEF bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t BgL_stringz00_23)
{
{ /* Llib/bigloo.scm 527 */
{ /* Llib/bigloo.scm 528 */
 long BgL_lenz00_1238;
BgL_lenz00_1238 = 
STRING_LENGTH(BgL_stringz00_23); 
if(
(BgL_lenz00_1238>0L))
{ /* Llib/bigloo.scm 530 */
 bool_t BgL__ortest_1049z00_1240;
{ /* Llib/bigloo.scm 530 */
 bool_t BgL_test2221z00_3056;
{ /* Llib/bigloo.scm 530 */
 bool_t BgL_test2222z00_3057;
{ /* Llib/bigloo.scm 530 */
 unsigned char BgL_arg1339z00_1260;
if(
BOUND_CHECK(0L, BgL_lenz00_1238))
{ /* Llib/bigloo.scm 530 */
BgL_arg1339z00_1260 = 
STRING_REF(BgL_stringz00_23, 0L); }  else 
{ 
 obj_t BgL_auxz00_3061;
BgL_auxz00_3061 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(20320L), BGl_string2023z00zz__biglooz00, BgL_stringz00_23, 
(int)(BgL_lenz00_1238), 
(int)(0L)); 
FAILURE(BgL_auxz00_3061,BFALSE,BFALSE);} 
BgL_test2222z00_3057 = 
isalpha(BgL_arg1339z00_1260); } 
if(BgL_test2222z00_3057)
{ /* Llib/bigloo.scm 530 */
BgL_test2221z00_3056 = ((bool_t)1)
; }  else 
{ /* Llib/bigloo.scm 531 */
 unsigned char BgL_tmpz00_3068;
{ /* Llib/bigloo.scm 531 */
 long BgL_l1890z00_2450;
BgL_l1890z00_2450 = 
STRING_LENGTH(BgL_stringz00_23); 
if(
BOUND_CHECK(0L, BgL_l1890z00_2450))
{ /* Llib/bigloo.scm 531 */
BgL_tmpz00_3068 = 
STRING_REF(BgL_stringz00_23, 0L)
; }  else 
{ 
 obj_t BgL_auxz00_3073;
BgL_auxz00_3073 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(20354L), BGl_string2023z00zz__biglooz00, BgL_stringz00_23, 
(int)(BgL_l1890z00_2450), 
(int)(0L)); 
FAILURE(BgL_auxz00_3073,BFALSE,BFALSE);} } 
BgL_test2221z00_3056 = 
(BgL_tmpz00_3068==((unsigned char)'_')); } } 
if(BgL_test2221z00_3056)
{ /* Llib/bigloo.scm 530 */
BgL__ortest_1049z00_1240 = ((bool_t)0); }  else 
{ /* Llib/bigloo.scm 530 */
BgL__ortest_1049z00_1240 = ((bool_t)1); } } 
if(BgL__ortest_1049z00_1240)
{ /* Llib/bigloo.scm 530 */
return BgL__ortest_1049z00_1240;}  else 
{ 
 long BgL_iz00_1242;
BgL_iz00_1242 = 1L; 
BgL_zc3z04anonymousza31327ze3z87_1243:
if(
(BgL_iz00_1242>=BgL_lenz00_1238))
{ /* Llib/bigloo.scm 533 */
return ((bool_t)0);}  else 
{ /* Llib/bigloo.scm 535 */
 unsigned char BgL_cz00_1245;
{ /* Llib/bigloo.scm 535 */
 long BgL_l1894z00_2454;
BgL_l1894z00_2454 = 
STRING_LENGTH(BgL_stringz00_23); 
if(
BOUND_CHECK(BgL_iz00_1242, BgL_l1894z00_2454))
{ /* Llib/bigloo.scm 535 */
BgL_cz00_1245 = 
STRING_REF(BgL_stringz00_23, BgL_iz00_1242); }  else 
{ 
 obj_t BgL_auxz00_3087;
BgL_auxz00_3087 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(20458L), BGl_string2023z00zz__biglooz00, BgL_stringz00_23, 
(int)(BgL_l1894z00_2454), 
(int)(BgL_iz00_1242)); 
FAILURE(BgL_auxz00_3087,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 536 */
 bool_t BgL_test2228z00_3093;
if(
isalpha(BgL_cz00_1245))
{ /* Llib/bigloo.scm 536 */
BgL_test2228z00_3093 = ((bool_t)1)
; }  else 
{ /* Llib/bigloo.scm 536 */
if(
isdigit(BgL_cz00_1245))
{ /* Llib/bigloo.scm 537 */
BgL_test2228z00_3093 = ((bool_t)1)
; }  else 
{ /* Llib/bigloo.scm 537 */
BgL_test2228z00_3093 = 
(BgL_cz00_1245==((unsigned char)'_'))
; } } 
if(BgL_test2228z00_3093)
{ 
 long BgL_iz00_3099;
BgL_iz00_3099 = 
(BgL_iz00_1242+1L); 
BgL_iz00_1242 = BgL_iz00_3099; 
goto BgL_zc3z04anonymousza31327ze3z87_1243;}  else 
{ /* Llib/bigloo.scm 536 */
return ((bool_t)1);} } } } }  else 
{ /* Llib/bigloo.scm 529 */
return ((bool_t)0);} } } 

}



/* &bigloo-need-mangling? */
obj_t BGl_z62bigloozd2needzd2manglingzf3z91zz__biglooz00(obj_t BgL_envz00_2325, obj_t BgL_stringz00_2326)
{
{ /* Llib/bigloo.scm 527 */
{ /* Llib/bigloo.scm 528 */
 bool_t BgL_tmpz00_3101;
{ /* Llib/bigloo.scm 528 */
 obj_t BgL_auxz00_3102;
if(
STRINGP(BgL_stringz00_2326))
{ /* Llib/bigloo.scm 528 */
BgL_auxz00_3102 = BgL_stringz00_2326
; }  else 
{ 
 obj_t BgL_auxz00_3105;
BgL_auxz00_3105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(20226L), BGl_string2033z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2326); 
FAILURE(BgL_auxz00_3105,BFALSE,BFALSE);} 
BgL_tmpz00_3101 = 
BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(BgL_auxz00_3102); } 
return 
BBOOL(BgL_tmpz00_3101);} } 

}



/* bigloo-demangle */
BGL_EXPORTED_DEF obj_t bigloo_demangle(obj_t BgL_stringz00_24)
{
{ /* Llib/bigloo.scm 545 */
{ /* Llib/bigloo.scm 546 */
 long BgL_lenz00_1261;
BgL_lenz00_1261 = 
STRING_LENGTH(BgL_stringz00_24); 
{ /* Llib/bigloo.scm 546 */
 long BgL_clenz00_1262;
BgL_clenz00_1262 = 
(BgL_lenz00_1261-3L); 
{ /* Llib/bigloo.scm 547 */

{ 

if(
(BgL_lenz00_1261<8L))
{ /* Llib/bigloo.scm 596 */
return BgL_stringz00_24;}  else 
{ /* Llib/bigloo.scm 596 */
if(
bigloo_strncmp(BgL_stringz00_24, BGl_string2027z00zz__biglooz00, 4L))
{ /* Llib/bigloo.scm 598 */
{ /* Llib/bigloo.scm 586 */
 obj_t BgL_strz00_1323;
BgL_strz00_1323 = 
BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(BgL_stringz00_24, BgL_clenz00_1262, 
BINT(4L)); 
{ /* Llib/bigloo.scm 587 */
 obj_t BgL_offsetz00_1324;
{ /* Llib/bigloo.scm 588 */
 obj_t BgL_tmpz00_2037;
{ /* Llib/bigloo.scm 588 */
 int BgL_tmpz00_3119;
BgL_tmpz00_3119 = 
(int)(1L); 
BgL_tmpz00_2037 = 
BGL_MVALUES_VAL(BgL_tmpz00_3119); } 
{ /* Llib/bigloo.scm 588 */
 int BgL_tmpz00_3122;
BgL_tmpz00_3122 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3122, BUNSPEC); } 
BgL_offsetz00_1324 = BgL_tmpz00_2037; } 
{ /* Llib/bigloo.scm 588 */
 int BgL_tmpz00_3125;
BgL_tmpz00_3125 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3125); } 
{ /* Llib/bigloo.scm 588 */
 int BgL_tmpz00_3128;
BgL_tmpz00_3128 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3128, BUNSPEC); } 
return BgL_strz00_1323;} } }  else 
{ /* Llib/bigloo.scm 598 */
if(
bigloo_strncmp(BgL_stringz00_24, BGl_string2030z00zz__biglooz00, 4L))
{ /* Llib/bigloo.scm 600 */
{ /* Llib/bigloo.scm 590 */
 obj_t BgL_idz00_1328;
BgL_idz00_1328 = 
BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(BgL_stringz00_24, BgL_clenz00_1262, 
BINT(4L)); 
{ /* Llib/bigloo.scm 591 */
 obj_t BgL_offsetz00_1329;
{ /* Llib/bigloo.scm 592 */
 obj_t BgL_tmpz00_2038;
{ /* Llib/bigloo.scm 592 */
 int BgL_tmpz00_3135;
BgL_tmpz00_3135 = 
(int)(1L); 
BgL_tmpz00_2038 = 
BGL_MVALUES_VAL(BgL_tmpz00_3135); } 
{ /* Llib/bigloo.scm 592 */
 int BgL_tmpz00_3138;
BgL_tmpz00_3138 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3138, BUNSPEC); } 
BgL_offsetz00_1329 = BgL_tmpz00_2038; } 
{ /* Llib/bigloo.scm 592 */
 obj_t BgL_modulez00_1330;
BgL_modulez00_1330 = 
BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(BgL_stringz00_24, BgL_clenz00_1262, BgL_offsetz00_1329); 
{ /* Llib/bigloo.scm 593 */
 obj_t BgL_offsetz00_1331;
{ /* Llib/bigloo.scm 594 */
 obj_t BgL_tmpz00_2039;
{ /* Llib/bigloo.scm 594 */
 int BgL_tmpz00_3142;
BgL_tmpz00_3142 = 
(int)(1L); 
BgL_tmpz00_2039 = 
BGL_MVALUES_VAL(BgL_tmpz00_3142); } 
{ /* Llib/bigloo.scm 594 */
 int BgL_tmpz00_3145;
BgL_tmpz00_3145 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3145, BUNSPEC); } 
BgL_offsetz00_1331 = BgL_tmpz00_2039; } 
{ /* Llib/bigloo.scm 594 */
 int BgL_tmpz00_3148;
BgL_tmpz00_3148 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3148); } 
{ /* Llib/bigloo.scm 594 */
 int BgL_tmpz00_3151;
BgL_tmpz00_3151 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3151, BgL_modulez00_1330); } 
return BgL_idz00_1328;} } } } }  else 
{ /* Llib/bigloo.scm 600 */
return BgL_stringz00_24;} } } } } } } } 

}



/* char->digit~0 */
long BGl_charzd2ze3digitze70zd6zz__biglooz00(unsigned char BgL_cz00_1273)
{
{ /* Llib/bigloo.scm 553 */
if(
isdigit(BgL_cz00_1273))
{ /* Llib/bigloo.scm 551 */
return 
(
(BgL_cz00_1273)-48L);}  else 
{ /* Llib/bigloo.scm 551 */
return 
(10L+
(
(BgL_cz00_1273)-97L));} } 

}



/* get-8bits-integer~0 */
long BGl_getzd28bitszd2integerze70ze7zz__biglooz00(obj_t BgL_stringz00_2357, obj_t BgL_rz00_1279)
{
{ /* Llib/bigloo.scm 559 */
{ /* Llib/bigloo.scm 558 */

{ /* Llib/bigloo.scm 559 */
 long BgL_auxz00_3183; long BgL_tmpz00_3161;
{ /* Llib/bigloo.scm 559 */
 long BgL_tmpz00_3184;
{ /* Llib/bigloo.scm 558 */
 unsigned char BgL_auxz00_3185;
{ /* Llib/bigloo.scm 556 */
 long BgL_i1901z00_2461;
{ /* Llib/bigloo.scm 556 */
 long BgL_za71za7_2002;
{ /* Llib/bigloo.scm 556 */
 obj_t BgL_tmpz00_3186;
if(
INTEGERP(BgL_rz00_1279))
{ /* Llib/bigloo.scm 556 */
BgL_tmpz00_3186 = BgL_rz00_1279
; }  else 
{ 
 obj_t BgL_auxz00_3189;
BgL_auxz00_3189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21281L), BGl_string2034z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1279); 
FAILURE(BgL_auxz00_3189,BFALSE,BFALSE);} 
BgL_za71za7_2002 = 
(long)CINT(BgL_tmpz00_3186); } 
BgL_i1901z00_2461 = 
(BgL_za71za7_2002+2L); } 
{ /* Llib/bigloo.scm 556 */
 long BgL_l1902z00_2462;
BgL_l1902z00_2462 = 
STRING_LENGTH(BgL_stringz00_2357); 
if(
BOUND_CHECK(BgL_i1901z00_2461, BgL_l1902z00_2462))
{ /* Llib/bigloo.scm 556 */
BgL_auxz00_3185 = 
STRING_REF(BgL_stringz00_2357, BgL_i1901z00_2461)
; }  else 
{ 
 obj_t BgL_auxz00_3199;
BgL_auxz00_3199 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21257L), BGl_string2023z00zz__biglooz00, BgL_stringz00_2357, 
(int)(BgL_l1902z00_2462), 
(int)(BgL_i1901z00_2461)); 
FAILURE(BgL_auxz00_3199,BFALSE,BFALSE);} } } 
BgL_tmpz00_3184 = 
BGl_charzd2ze3digitze70zd6zz__biglooz00(BgL_auxz00_3185); } 
BgL_auxz00_3183 = 
(BgL_tmpz00_3184 << 
(int)(4L)); } 
{ /* Llib/bigloo.scm 557 */
 unsigned char BgL_auxz00_3162;
{ /* Llib/bigloo.scm 555 */
 long BgL_i1897z00_2457;
{ /* Llib/bigloo.scm 555 */
 long BgL_za71za7_1999;
{ /* Llib/bigloo.scm 555 */
 obj_t BgL_tmpz00_3163;
if(
INTEGERP(BgL_rz00_1279))
{ /* Llib/bigloo.scm 555 */
BgL_tmpz00_3163 = BgL_rz00_1279
; }  else 
{ 
 obj_t BgL_auxz00_3166;
BgL_auxz00_3166 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21244L), BGl_string2034z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1279); 
FAILURE(BgL_auxz00_3166,BFALSE,BFALSE);} 
BgL_za71za7_1999 = 
(long)CINT(BgL_tmpz00_3163); } 
BgL_i1897z00_2457 = 
(BgL_za71za7_1999+1L); } 
{ /* Llib/bigloo.scm 555 */
 long BgL_l1898z00_2458;
BgL_l1898z00_2458 = 
STRING_LENGTH(BgL_stringz00_2357); 
if(
BOUND_CHECK(BgL_i1897z00_2457, BgL_l1898z00_2458))
{ /* Llib/bigloo.scm 555 */
BgL_auxz00_3162 = 
STRING_REF(BgL_stringz00_2357, BgL_i1897z00_2457)
; }  else 
{ 
 obj_t BgL_auxz00_3176;
BgL_auxz00_3176 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21220L), BGl_string2023z00zz__biglooz00, BgL_stringz00_2357, 
(int)(BgL_l1898z00_2458), 
(int)(BgL_i1897z00_2457)); 
FAILURE(BgL_auxz00_3176,BFALSE,BFALSE);} } } 
BgL_tmpz00_3161 = 
BGl_charzd2ze3digitze70zd6zz__biglooz00(BgL_auxz00_3162); } 
return 
(BgL_tmpz00_3161+BgL_auxz00_3183);} } } 

}



/* bigloo-demangle-at~0 */
obj_t BGl_bigloozd2demanglezd2atze70ze7zz__biglooz00(obj_t BgL_stringz00_2359, long BgL_clenz00_2358, obj_t BgL_offsetz00_1288)
{
{ /* Llib/bigloo.scm 584 */
{ /* Llib/bigloo.scm 561 */
 obj_t BgL_newz00_1290;
{ /* Ieee/string.scm 172 */

BgL_newz00_1290 = 
make_string(BgL_clenz00_2358, ((unsigned char)' ')); } 
{ 
 obj_t BgL_rz00_1292; long BgL_wz00_1293; long BgL_checksumz00_1294;
BgL_rz00_1292 = BgL_offsetz00_1288; 
BgL_wz00_1293 = 0L; 
BgL_checksumz00_1294 = 0L; 
BgL_zc3z04anonymousza31354ze3z87_1295:
{ /* Llib/bigloo.scm 565 */
 bool_t BgL_test2240z00_3210;
{ /* Llib/bigloo.scm 565 */
 long BgL_n1z00_2009;
{ /* Llib/bigloo.scm 565 */
 obj_t BgL_tmpz00_3211;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 565 */
BgL_tmpz00_3211 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3214;
BgL_auxz00_3214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21526L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3214,BFALSE,BFALSE);} 
BgL_n1z00_2009 = 
(long)CINT(BgL_tmpz00_3211); } 
BgL_test2240z00_3210 = 
(BgL_n1z00_2009==BgL_clenz00_2358); } 
if(BgL_test2240z00_3210)
{ /* Llib/bigloo.scm 565 */
if(
(BgL_checksumz00_1294==
BGl_getzd28bitszd2integerze70ze7zz__biglooz00(BgL_stringz00_2359, BgL_rz00_1292)))
{ /* Llib/bigloo.scm 568 */
 obj_t BgL_val0_1087z00_1299; long BgL_val1_1088z00_1300;
BgL_val0_1087z00_1299 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_newz00_1290, 0L, BgL_wz00_1293); 
{ /* Llib/bigloo.scm 568 */
 long BgL_za71za7_2013;
{ /* Llib/bigloo.scm 568 */
 obj_t BgL_tmpz00_3224;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 568 */
BgL_tmpz00_3224 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3227;
BgL_auxz00_3227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21666L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3227,BFALSE,BFALSE);} 
BgL_za71za7_2013 = 
(long)CINT(BgL_tmpz00_3224); } 
BgL_val1_1088z00_1300 = 
(BgL_za71za7_2013+3L); } 
{ /* Llib/bigloo.scm 568 */
 int BgL_tmpz00_3233;
BgL_tmpz00_3233 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3233); } 
{ /* Llib/bigloo.scm 568 */
 obj_t BgL_auxz00_3238; int BgL_tmpz00_3236;
BgL_auxz00_3238 = 
BINT(BgL_val1_1088z00_1300); 
BgL_tmpz00_3236 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3236, BgL_auxz00_3238); } 
return BgL_val0_1087z00_1299;}  else 
{ /* Llib/bigloo.scm 567 */
return 
BGl_errorz00zz__errorz00(BGl_string2037z00zz__biglooz00, BGl_string2038z00zz__biglooz00, BgL_stringz00_2359);} }  else 
{ /* Llib/bigloo.scm 570 */
 unsigned char BgL_cz00_1302;
{ /* Llib/bigloo.scm 570 */
 long BgL_kz00_2015;
{ /* Llib/bigloo.scm 570 */
 obj_t BgL_tmpz00_3242;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 570 */
BgL_tmpz00_3242 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3245;
BgL_auxz00_3245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21722L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3245,BFALSE,BFALSE);} 
BgL_kz00_2015 = 
(long)CINT(BgL_tmpz00_3242); } 
{ /* Llib/bigloo.scm 570 */
 long BgL_l1906z00_2466;
BgL_l1906z00_2466 = 
STRING_LENGTH(BgL_stringz00_2359); 
if(
BOUND_CHECK(BgL_kz00_2015, BgL_l1906z00_2466))
{ /* Llib/bigloo.scm 570 */
BgL_cz00_1302 = 
STRING_REF(BgL_stringz00_2359, BgL_kz00_2015); }  else 
{ 
 obj_t BgL_auxz00_3254;
BgL_auxz00_3254 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21703L), BGl_string2023z00zz__biglooz00, BgL_stringz00_2359, 
(int)(BgL_l1906z00_2466), 
(int)(BgL_kz00_2015)); 
FAILURE(BgL_auxz00_3254,BFALSE,BFALSE);} } } 
if(
(BgL_cz00_1302==((unsigned char)'z')))
{ /* Llib/bigloo.scm 572 */
 bool_t BgL_test2247z00_3262;
{ /* Llib/bigloo.scm 572 */
 unsigned char BgL_tmpz00_3263;
{ /* Llib/bigloo.scm 572 */
 long BgL_i1909z00_2469;
{ /* Llib/bigloo.scm 572 */
 long BgL_za71za7_2018;
{ /* Llib/bigloo.scm 572 */
 obj_t BgL_tmpz00_3264;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 572 */
BgL_tmpz00_3264 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3267;
BgL_auxz00_3267 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21795L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3267,BFALSE,BFALSE);} 
BgL_za71za7_2018 = 
(long)CINT(BgL_tmpz00_3264); } 
BgL_i1909z00_2469 = 
(BgL_za71za7_2018+1L); } 
{ /* Llib/bigloo.scm 572 */
 long BgL_l1910z00_2470;
BgL_l1910z00_2470 = 
STRING_LENGTH(BgL_stringz00_2359); 
if(
BOUND_CHECK(BgL_i1909z00_2469, BgL_l1910z00_2470))
{ /* Llib/bigloo.scm 572 */
BgL_tmpz00_3263 = 
STRING_REF(BgL_stringz00_2359, BgL_i1909z00_2469)
; }  else 
{ 
 obj_t BgL_auxz00_3277;
BgL_auxz00_3277 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21771L), BGl_string2023z00zz__biglooz00, BgL_stringz00_2359, 
(int)(BgL_l1910z00_2470), 
(int)(BgL_i1909z00_2469)); 
FAILURE(BgL_auxz00_3277,BFALSE,BFALSE);} } } 
BgL_test2247z00_3262 = 
(BgL_tmpz00_3263==((unsigned char)'z')); } 
if(BgL_test2247z00_3262)
{ /* Llib/bigloo.scm 573 */
 obj_t BgL_val0_1089z00_1307; long BgL_val1_1090z00_1308;
BgL_val0_1089z00_1307 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_newz00_1290, 0L, 
(BgL_wz00_1293-1L)); 
{ /* Llib/bigloo.scm 573 */
 long BgL_za71za7_2024;
{ /* Llib/bigloo.scm 573 */
 obj_t BgL_tmpz00_3286;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 573 */
BgL_tmpz00_3286 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3289;
BgL_auxz00_3289 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21856L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3289,BFALSE,BFALSE);} 
BgL_za71za7_2024 = 
(long)CINT(BgL_tmpz00_3286); } 
BgL_val1_1090z00_1308 = 
(BgL_za71za7_2024+2L); } 
{ /* Llib/bigloo.scm 573 */
 int BgL_tmpz00_3295;
BgL_tmpz00_3295 = 
(int)(2L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3295); } 
{ /* Llib/bigloo.scm 573 */
 obj_t BgL_auxz00_3300; int BgL_tmpz00_3298;
BgL_auxz00_3300 = 
BINT(BgL_val1_1090z00_1308); 
BgL_tmpz00_3298 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3298, BgL_auxz00_3300); } 
return BgL_val0_1089z00_1307;}  else 
{ /* Llib/bigloo.scm 574 */
 long BgL_iz00_1310;
BgL_iz00_1310 = 
BGl_getzd28bitszd2integerze70ze7zz__biglooz00(BgL_stringz00_2359, BgL_rz00_1292); 
{ /* Llib/bigloo.scm 574 */
 unsigned char BgL_ncz00_1311;
BgL_ncz00_1311 = 
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(BgL_iz00_1310); 
{ /* Llib/bigloo.scm 575 */

{ /* Llib/bigloo.scm 576 */
 long BgL_l1914z00_2474;
BgL_l1914z00_2474 = 
STRING_LENGTH(BgL_newz00_1290); 
if(
BOUND_CHECK(BgL_wz00_1293, BgL_l1914z00_2474))
{ /* Llib/bigloo.scm 576 */
STRING_SET(BgL_newz00_1290, BgL_wz00_1293, BgL_ncz00_1311); }  else 
{ 
 obj_t BgL_auxz00_3309;
BgL_auxz00_3309 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21942L), BGl_string2012z00zz__biglooz00, BgL_newz00_1290, 
(int)(BgL_l1914z00_2474), 
(int)(BgL_wz00_1293)); 
FAILURE(BgL_auxz00_3309,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 577 */
 long BgL_arg1364z00_1312; long BgL_arg1365z00_1313; long BgL_arg1366z00_1314;
{ /* Llib/bigloo.scm 577 */
 long BgL_za71za7_2028;
{ /* Llib/bigloo.scm 577 */
 obj_t BgL_tmpz00_3315;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 577 */
BgL_tmpz00_3315 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3318;
BgL_auxz00_3318 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(21981L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3318,BFALSE,BFALSE);} 
BgL_za71za7_2028 = 
(long)CINT(BgL_tmpz00_3315); } 
BgL_arg1364z00_1312 = 
(BgL_za71za7_2028+3L); } 
BgL_arg1365z00_1313 = 
(BgL_wz00_1293+1L); 
BgL_arg1366z00_1314 = 
(BgL_checksumz00_1294 ^ BgL_iz00_1310); 
{ 
 long BgL_checksumz00_3329; long BgL_wz00_3328; obj_t BgL_rz00_3326;
BgL_rz00_3326 = 
BINT(BgL_arg1364z00_1312); 
BgL_wz00_3328 = BgL_arg1365z00_1313; 
BgL_checksumz00_3329 = BgL_arg1366z00_1314; 
BgL_checksumz00_1294 = BgL_checksumz00_3329; 
BgL_wz00_1293 = BgL_wz00_3328; 
BgL_rz00_1292 = BgL_rz00_3326; 
goto BgL_zc3z04anonymousza31354ze3z87_1295;} } } } } }  else 
{ /* Llib/bigloo.scm 571 */
{ /* Llib/bigloo.scm 581 */
 long BgL_l1918z00_2478;
BgL_l1918z00_2478 = 
STRING_LENGTH(BgL_newz00_1290); 
if(
BOUND_CHECK(BgL_wz00_1293, BgL_l1918z00_2478))
{ /* Llib/bigloo.scm 581 */
STRING_SET(BgL_newz00_1290, BgL_wz00_1293, BgL_cz00_1302); }  else 
{ 
 obj_t BgL_auxz00_3334;
BgL_auxz00_3334 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(22062L), BGl_string2012z00zz__biglooz00, BgL_newz00_1290, 
(int)(BgL_l1918z00_2478), 
(int)(BgL_wz00_1293)); 
FAILURE(BgL_auxz00_3334,BFALSE,BFALSE);} } 
{ /* Llib/bigloo.scm 582 */
 long BgL_arg1369z00_1317; long BgL_arg1370z00_1318;
{ /* Llib/bigloo.scm 582 */
 long BgL_za71za7_2035;
{ /* Llib/bigloo.scm 582 */
 obj_t BgL_tmpz00_3340;
if(
INTEGERP(BgL_rz00_1292))
{ /* Llib/bigloo.scm 582 */
BgL_tmpz00_3340 = BgL_rz00_1292
; }  else 
{ 
 obj_t BgL_auxz00_3343;
BgL_auxz00_3343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(22103L), BGl_string2036z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_rz00_1292); 
FAILURE(BgL_auxz00_3343,BFALSE,BFALSE);} 
BgL_za71za7_2035 = 
(long)CINT(BgL_tmpz00_3340); } 
BgL_arg1369z00_1317 = 
(BgL_za71za7_2035+1L); } 
BgL_arg1370z00_1318 = 
(BgL_wz00_1293+1L); 
{ 
 long BgL_wz00_3352; obj_t BgL_rz00_3350;
BgL_rz00_3350 = 
BINT(BgL_arg1369z00_1317); 
BgL_wz00_3352 = BgL_arg1370z00_1318; 
BgL_wz00_1293 = BgL_wz00_3352; 
BgL_rz00_1292 = BgL_rz00_3350; 
goto BgL_zc3z04anonymousza31354ze3z87_1295;} } } } } } } } 

}



/* &bigloo-demangle */
obj_t BGl_z62bigloozd2demanglezb0zz__biglooz00(obj_t BgL_envz00_2327, obj_t BgL_stringz00_2328)
{
{ /* Llib/bigloo.scm 545 */
{ /* Llib/bigloo.scm 546 */
 obj_t BgL_auxz00_3353;
if(
STRINGP(BgL_stringz00_2328))
{ /* Llib/bigloo.scm 546 */
BgL_auxz00_3353 = BgL_stringz00_2328
; }  else 
{ 
 obj_t BgL_auxz00_3356;
BgL_auxz00_3356 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(20869L), BGl_string2039z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2328); 
FAILURE(BgL_auxz00_3356,BFALSE,BFALSE);} 
return 
bigloo_demangle(BgL_auxz00_3353);} } 

}



/* bigloo-module-demangle */
BGL_EXPORTED_DEF obj_t bigloo_module_demangle(obj_t BgL_stringz00_25)
{
{ /* Llib/bigloo.scm 608 */
{ /* Llib/bigloo.scm 609 */
 obj_t BgL_idz00_1340;
BgL_idz00_1340 = 
bigloo_demangle(BgL_stringz00_25); 
{ /* Llib/bigloo.scm 610 */
 obj_t BgL_modulez00_1341;
{ /* Llib/bigloo.scm 611 */
 obj_t BgL_tmpz00_2045;
{ /* Llib/bigloo.scm 611 */
 int BgL_tmpz00_3362;
BgL_tmpz00_3362 = 
(int)(1L); 
BgL_tmpz00_2045 = 
BGL_MVALUES_VAL(BgL_tmpz00_3362); } 
{ /* Llib/bigloo.scm 611 */
 int BgL_tmpz00_3365;
BgL_tmpz00_3365 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_3365, BUNSPEC); } 
BgL_modulez00_1341 = BgL_tmpz00_2045; } 
if(
STRINGP(BgL_modulez00_1341))
{ /* Llib/bigloo.scm 612 */
 obj_t BgL_tmpz00_3370;
if(
STRINGP(BgL_idz00_1340))
{ /* Llib/bigloo.scm 612 */
BgL_tmpz00_3370 = BgL_idz00_1340
; }  else 
{ 
 obj_t BgL_auxz00_3373;
BgL_auxz00_3373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23039L), BGl_string2040z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_idz00_1340); 
FAILURE(BgL_auxz00_3373,BFALSE,BFALSE);} 
return 
string_append_3(BgL_tmpz00_3370, BGl_string2041z00zz__biglooz00, BgL_modulez00_1341);}  else 
{ /* Llib/bigloo.scm 611 */
if(
STRINGP(BgL_idz00_1340))
{ /* Llib/bigloo.scm 613 */
return BgL_idz00_1340;}  else 
{ 
 obj_t BgL_auxz00_3380;
BgL_auxz00_3380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23072L), BGl_string2040z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_idz00_1340); 
FAILURE(BgL_auxz00_3380,BFALSE,BFALSE);} } } } } 

}



/* &bigloo-module-demangle */
obj_t BGl_z62bigloozd2modulezd2demanglez62zz__biglooz00(obj_t BgL_envz00_2329, obj_t BgL_stringz00_2330)
{
{ /* Llib/bigloo.scm 608 */
{ /* Llib/bigloo.scm 609 */
 obj_t BgL_auxz00_3384;
if(
STRINGP(BgL_stringz00_2330))
{ /* Llib/bigloo.scm 609 */
BgL_auxz00_3384 = BgL_stringz00_2330
; }  else 
{ 
 obj_t BgL_auxz00_3387;
BgL_auxz00_3387 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(22945L), BGl_string2042z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2330); 
FAILURE(BgL_auxz00_3387,BFALSE,BFALSE);} 
return 
bigloo_module_demangle(BgL_auxz00_3384);} } 

}



/* bigloo-class-mangled? */
BGL_EXPORTED_DEF bool_t bigloo_class_mangledp(obj_t BgL_stringz00_26)
{
{ /* Llib/bigloo.scm 618 */
{ /* Llib/bigloo.scm 619 */
 long BgL_lenz00_1343;
BgL_lenz00_1343 = 
STRING_LENGTH(BgL_stringz00_26); 
if(
(BgL_lenz00_1343>8L))
{ /* Llib/bigloo.scm 621 */
 bool_t BgL_test2261z00_3395;
{ /* Llib/bigloo.scm 621 */
 unsigned char BgL_tmpz00_3396;
{ /* Llib/bigloo.scm 621 */
 long BgL_i1921z00_2481;
BgL_i1921z00_2481 = 
(BgL_lenz00_1343-1L); 
if(
BOUND_CHECK(BgL_i1921z00_2481, BgL_lenz00_1343))
{ /* Llib/bigloo.scm 621 */
BgL_tmpz00_3396 = 
STRING_REF(BgL_stringz00_26, BgL_i1921z00_2481)
; }  else 
{ 
 obj_t BgL_auxz00_3401;
BgL_auxz00_3401 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23414L), BGl_string2023z00zz__biglooz00, BgL_stringz00_26, 
(int)(BgL_lenz00_1343), 
(int)(BgL_i1921z00_2481)); 
FAILURE(BgL_auxz00_3401,BFALSE,BFALSE);} } 
BgL_test2261z00_3395 = 
(BgL_tmpz00_3396==((unsigned char)'t')); } 
if(BgL_test2261z00_3395)
{ /* Llib/bigloo.scm 622 */
 bool_t BgL_test2263z00_3408;
{ /* Llib/bigloo.scm 622 */
 unsigned char BgL_tmpz00_3409;
{ /* Llib/bigloo.scm 622 */
 long BgL_i1925z00_2485;
BgL_i1925z00_2485 = 
(BgL_lenz00_1343-2L); 
{ /* Llib/bigloo.scm 622 */
 long BgL_l1926z00_2486;
BgL_l1926z00_2486 = 
STRING_LENGTH(BgL_stringz00_26); 
if(
BOUND_CHECK(BgL_i1925z00_2485, BgL_l1926z00_2486))
{ /* Llib/bigloo.scm 622 */
BgL_tmpz00_3409 = 
STRING_REF(BgL_stringz00_26, BgL_i1925z00_2485)
; }  else 
{ 
 obj_t BgL_auxz00_3415;
BgL_auxz00_3415 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23463L), BGl_string2023z00zz__biglooz00, BgL_stringz00_26, 
(int)(BgL_l1926z00_2486), 
(int)(BgL_i1925z00_2485)); 
FAILURE(BgL_auxz00_3415,BFALSE,BFALSE);} } } 
BgL_test2263z00_3408 = 
(BgL_tmpz00_3409==((unsigned char)'l')); } 
if(BgL_test2263z00_3408)
{ /* Llib/bigloo.scm 623 */
 bool_t BgL_test2265z00_3422;
{ /* Llib/bigloo.scm 623 */
 unsigned char BgL_tmpz00_3423;
{ /* Llib/bigloo.scm 623 */
 long BgL_i1929z00_2489;
BgL_i1929z00_2489 = 
(BgL_lenz00_1343-3L); 
{ /* Llib/bigloo.scm 623 */
 long BgL_l1930z00_2490;
BgL_l1930z00_2490 = 
STRING_LENGTH(BgL_stringz00_26); 
if(
BOUND_CHECK(BgL_i1929z00_2489, BgL_l1930z00_2490))
{ /* Llib/bigloo.scm 623 */
BgL_tmpz00_3423 = 
STRING_REF(BgL_stringz00_26, BgL_i1929z00_2489)
; }  else 
{ 
 obj_t BgL_auxz00_3429;
BgL_auxz00_3429 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23512L), BGl_string2023z00zz__biglooz00, BgL_stringz00_26, 
(int)(BgL_l1930z00_2490), 
(int)(BgL_i1929z00_2489)); 
FAILURE(BgL_auxz00_3429,BFALSE,BFALSE);} } } 
BgL_test2265z00_3422 = 
(BgL_tmpz00_3423==((unsigned char)'g')); } 
if(BgL_test2265z00_3422)
{ /* Llib/bigloo.scm 624 */
 bool_t BgL_test2267z00_3436;
{ /* Llib/bigloo.scm 624 */
 unsigned char BgL_tmpz00_3437;
{ /* Llib/bigloo.scm 624 */
 long BgL_i1933z00_2493;
BgL_i1933z00_2493 = 
(BgL_lenz00_1343-4L); 
{ /* Llib/bigloo.scm 624 */
 long BgL_l1934z00_2494;
BgL_l1934z00_2494 = 
STRING_LENGTH(BgL_stringz00_26); 
if(
BOUND_CHECK(BgL_i1933z00_2493, BgL_l1934z00_2494))
{ /* Llib/bigloo.scm 624 */
BgL_tmpz00_3437 = 
STRING_REF(BgL_stringz00_26, BgL_i1933z00_2493)
; }  else 
{ 
 obj_t BgL_auxz00_3443;
BgL_auxz00_3443 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23561L), BGl_string2023z00zz__biglooz00, BgL_stringz00_26, 
(int)(BgL_l1934z00_2494), 
(int)(BgL_i1933z00_2493)); 
FAILURE(BgL_auxz00_3443,BFALSE,BFALSE);} } } 
BgL_test2267z00_3436 = 
(BgL_tmpz00_3437==((unsigned char)'b')); } 
if(BgL_test2267z00_3436)
{ /* Llib/bigloo.scm 625 */
 bool_t BgL_test2269z00_3450;
{ /* Llib/bigloo.scm 625 */
 unsigned char BgL_tmpz00_3451;
{ /* Llib/bigloo.scm 625 */
 long BgL_i1937z00_2497;
BgL_i1937z00_2497 = 
(BgL_lenz00_1343-5L); 
{ /* Llib/bigloo.scm 625 */
 long BgL_l1938z00_2498;
BgL_l1938z00_2498 = 
STRING_LENGTH(BgL_stringz00_26); 
if(
BOUND_CHECK(BgL_i1937z00_2497, BgL_l1938z00_2498))
{ /* Llib/bigloo.scm 625 */
BgL_tmpz00_3451 = 
STRING_REF(BgL_stringz00_26, BgL_i1937z00_2497)
; }  else 
{ 
 obj_t BgL_auxz00_3457;
BgL_auxz00_3457 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23610L), BGl_string2023z00zz__biglooz00, BgL_stringz00_26, 
(int)(BgL_l1938z00_2498), 
(int)(BgL_i1937z00_2497)); 
FAILURE(BgL_auxz00_3457,BFALSE,BFALSE);} } } 
BgL_test2269z00_3450 = 
(BgL_tmpz00_3451==((unsigned char)'_')); } 
if(BgL_test2269z00_3450)
{ /* Llib/bigloo.scm 625 */
BGL_TAIL return 
bigloo_mangledp(
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_26, 0L, 
(BgL_lenz00_1343-5L)));}  else 
{ /* Llib/bigloo.scm 625 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 624 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 623 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 622 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 621 */
return ((bool_t)0);} }  else 
{ /* Llib/bigloo.scm 620 */
return ((bool_t)0);} } } 

}



/* &bigloo-class-mangled? */
obj_t BGl_z62bigloozd2classzd2mangledzf3z91zz__biglooz00(obj_t BgL_envz00_2331, obj_t BgL_stringz00_2332)
{
{ /* Llib/bigloo.scm 618 */
{ /* Llib/bigloo.scm 619 */
 bool_t BgL_tmpz00_3467;
{ /* Llib/bigloo.scm 619 */
 obj_t BgL_auxz00_3468;
if(
STRINGP(BgL_stringz00_2332))
{ /* Llib/bigloo.scm 619 */
BgL_auxz00_3468 = BgL_stringz00_2332
; }  else 
{ 
 obj_t BgL_auxz00_3471;
BgL_auxz00_3471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23343L), BGl_string2043z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2332); 
FAILURE(BgL_auxz00_3471,BFALSE,BFALSE);} 
BgL_tmpz00_3467 = 
bigloo_class_mangledp(BgL_auxz00_3468); } 
return 
BBOOL(BgL_tmpz00_3467);} } 

}



/* bigloo-class-demangle */
BGL_EXPORTED_DEF obj_t bigloo_class_demangle(obj_t BgL_stringz00_27)
{
{ /* Llib/bigloo.scm 631 */
{ /* Llib/bigloo.scm 633 */
 obj_t BgL_arg1390z00_1362;
BgL_arg1390z00_1362 = 
bigloo_demangle(
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_27, 0L, 
(
STRING_LENGTH(BgL_stringz00_27)-5L))); 
{ /* Llib/bigloo.scm 632 */
 obj_t BgL_tmpz00_3481;
if(
STRINGP(BgL_arg1390z00_1362))
{ /* Llib/bigloo.scm 632 */
BgL_tmpz00_3481 = BgL_arg1390z00_1362
; }  else 
{ 
 obj_t BgL_auxz00_3484;
BgL_auxz00_3484 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(23973L), BGl_string2044z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_arg1390z00_1362); 
FAILURE(BgL_auxz00_3484,BFALSE,BFALSE);} 
return 
string_append(BgL_tmpz00_3481, BGl_string2045z00zz__biglooz00);} } } 

}



/* &bigloo-class-demangle */
obj_t BGl_z62bigloozd2classzd2demanglez62zz__biglooz00(obj_t BgL_envz00_2333, obj_t BgL_stringz00_2334)
{
{ /* Llib/bigloo.scm 631 */
{ /* Llib/bigloo.scm 633 */
 obj_t BgL_auxz00_3489;
if(
STRINGP(BgL_stringz00_2334))
{ /* Llib/bigloo.scm 633 */
BgL_auxz00_3489 = BgL_stringz00_2334
; }  else 
{ 
 obj_t BgL_auxz00_3492;
BgL_auxz00_3492 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(24035L), BGl_string2046z00zz__biglooz00, BGl_string2016z00zz__biglooz00, BgL_stringz00_2334); 
FAILURE(BgL_auxz00_3492,BFALSE,BFALSE);} 
return 
bigloo_class_demangle(BgL_auxz00_3489);} } 

}



/* bigloo-exit-mutex */
BGL_EXPORTED_DEF obj_t bgl_exit_mutex(void)
{
{ /* Llib/bigloo.scm 644 */
return BGl_za2exitzd2mutexza2zd2zz__biglooz00;} 

}



/* &bigloo-exit-mutex */
obj_t BGl_z62bigloozd2exitzd2mutexz62zz__biglooz00(obj_t BgL_envz00_2335)
{
{ /* Llib/bigloo.scm 644 */
return 
bgl_exit_mutex();} 

}



/* register-exit-function! */
BGL_EXPORTED_DEF obj_t BGl_registerzd2exitzd2functionz12z12zz__biglooz00(obj_t BgL_funz00_28)
{
{ /* Llib/bigloo.scm 655 */
{ /* Llib/bigloo.scm 656 */
 obj_t BgL_mutex1394z00_1366;
BgL_mutex1394z00_1366 = BGl_za2exitzd2mutexza2zd2zz__biglooz00; 
{ /* Llib/bigloo.scm 656 */
 obj_t BgL_top2275z00_3499;
BgL_top2275z00_3499 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BgL_mutex1394z00_1366); 
BGL_EXITD_PUSH_PROTECT(BgL_top2275z00_3499, BgL_mutex1394z00_1366); BUNSPEC; 
{ /* Llib/bigloo.scm 656 */
 obj_t BgL_tmp2274z00_3498;
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_28, 
(int)(1L)))
{ /* Llib/bigloo.scm 657 */
BgL_tmp2274z00_3498 = ( 
BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 = 
MAKE_YOUNG_PAIR(BgL_funz00_28, BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00), BUNSPEC) ; }  else 
{ /* Llib/bigloo.scm 657 */
BgL_tmp2274z00_3498 = 
BGl_errorz00zz__errorz00(BGl_string2047z00zz__biglooz00, BGl_string2048z00zz__biglooz00, BgL_funz00_28); } 
BGL_EXITD_POP_PROTECT(BgL_top2275z00_3499); BUNSPEC; 
BGL_MUTEX_UNLOCK(BgL_mutex1394z00_1366); 
return BgL_tmp2274z00_3498;} } } } 

}



/* &register-exit-function! */
obj_t BGl_z62registerzd2exitzd2functionz12z70zz__biglooz00(obj_t BgL_envz00_2336, obj_t BgL_funz00_2337)
{
{ /* Llib/bigloo.scm 655 */
{ /* Llib/bigloo.scm 656 */
 obj_t BgL_auxz00_3510;
if(
PROCEDUREP(BgL_funz00_2337))
{ /* Llib/bigloo.scm 656 */
BgL_auxz00_3510 = BgL_funz00_2337
; }  else 
{ 
 obj_t BgL_auxz00_3513;
BgL_auxz00_3513 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(25140L), BGl_string2049z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_funz00_2337); 
FAILURE(BgL_auxz00_3513,BFALSE,BFALSE);} 
return 
BGl_registerzd2exitzd2functionz12z12zz__biglooz00(BgL_auxz00_3510);} } 

}



/* unregister-exit-function! */
BGL_EXPORTED_DEF obj_t BGl_unregisterzd2exitzd2functionz12z12zz__biglooz00(obj_t BgL_funz00_29)
{
{ /* Llib/bigloo.scm 666 */
{ /* Llib/bigloo.scm 667 */
 obj_t BgL_mutex1395z00_2076;
BgL_mutex1395z00_2076 = BGl_za2exitzd2mutexza2zd2zz__biglooz00; 
{ /* Llib/bigloo.scm 667 */
 obj_t BgL_top2279z00_3519;
BgL_top2279z00_3519 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BgL_mutex1395z00_2076); 
BGL_EXITD_PUSH_PROTECT(BgL_top2279z00_3519, BgL_mutex1395z00_2076); BUNSPEC; 
{ /* Llib/bigloo.scm 667 */
 obj_t BgL_tmp2278z00_3518;
{ /* Llib/bigloo.scm 668 */
 obj_t BgL_auxz00_3523;
{ /* Llib/bigloo.scm 668 */
 obj_t BgL_aux1997z00_2558;
BgL_aux1997z00_2558 = BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00; 
{ /* Llib/bigloo.scm 668 */
 bool_t BgL_test2280z00_3524;
if(
PAIRP(BgL_aux1997z00_2558))
{ /* Llib/bigloo.scm 668 */
BgL_test2280z00_3524 = ((bool_t)1)
; }  else 
{ /* Llib/bigloo.scm 668 */
BgL_test2280z00_3524 = 
NULLP(BgL_aux1997z00_2558)
; } 
if(BgL_test2280z00_3524)
{ /* Llib/bigloo.scm 668 */
BgL_auxz00_3523 = BgL_aux1997z00_2558
; }  else 
{ 
 obj_t BgL_auxz00_3528;
BgL_auxz00_3528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(25705L), BGl_string2050z00zz__biglooz00, BGl_string2051z00zz__biglooz00, BgL_aux1997z00_2558); 
FAILURE(BgL_auxz00_3528,BFALSE,BFALSE);} } } 
BgL_tmp2278z00_3518 = ( 
BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 = 
bgl_remq_bang(BgL_funz00_29, BgL_auxz00_3523), BUNSPEC) ; } 
BGL_EXITD_POP_PROTECT(BgL_top2279z00_3519); BUNSPEC; 
BGL_MUTEX_UNLOCK(BgL_mutex1395z00_2076); 
return BgL_tmp2278z00_3518;} } } } 

}



/* &unregister-exit-function! */
obj_t BGl_z62unregisterzd2exitzd2functionz12z70zz__biglooz00(obj_t BgL_envz00_2338, obj_t BgL_funz00_2339)
{
{ /* Llib/bigloo.scm 666 */
{ /* Llib/bigloo.scm 667 */
 obj_t BgL_auxz00_3535;
if(
PROCEDUREP(BgL_funz00_2339))
{ /* Llib/bigloo.scm 667 */
BgL_auxz00_3535 = BgL_funz00_2339
; }  else 
{ 
 obj_t BgL_auxz00_3538;
BgL_auxz00_3538 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(25625L), BGl_string2052z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_funz00_2339); 
FAILURE(BgL_auxz00_3538,BFALSE,BFALSE);} 
return 
BGl_unregisterzd2exitzd2functionz12z12zz__biglooz00(BgL_auxz00_3535);} } 

}



/* bigloo-exit-apply */
BGL_EXPORTED_DEF obj_t bigloo_exit_apply(obj_t BgL_valz00_30)
{
{ /* Llib/bigloo.scm 673 */
{ /* Llib/bigloo.scm 674 */
 obj_t BgL_mutz00_1368;
if(
BGL_MUTEXP(BGl_za2exitzd2mutexza2zd2zz__biglooz00))
{ /* Llib/bigloo.scm 674 */
BgL_mutz00_1368 = BGl_za2exitzd2mutexza2zd2zz__biglooz00; }  else 
{ /* Llib/bigloo.scm 674 */
BgL_mutz00_1368 = 
bgl_make_mutex(BGl_symbol2008z00zz__biglooz00); } 
{ /* Llib/bigloo.scm 677 */
 obj_t BgL_top2285z00_3547;
BgL_top2285z00_3547 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BgL_mutz00_1368); 
BGL_EXITD_PUSH_PROTECT(BgL_top2285z00_3547, BgL_mutz00_1368); BUNSPEC; 
{ /* Llib/bigloo.scm 677 */
 obj_t BgL_tmp2284z00_3546;
{ 
 obj_t BgL_valz00_1370;
BgL_valz00_1370 = BgL_valz00_30; 
BgL_zc3z04anonymousza31396ze3z87_1371:
{ /* Llib/bigloo.scm 679 */
 obj_t BgL_valz00_1372;
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_valz00_1370))
{ /* Llib/bigloo.scm 679 */
BgL_valz00_1372 = BgL_valz00_1370; }  else 
{ /* Llib/bigloo.scm 679 */
BgL_valz00_1372 = 
BINT(0L); } 
if(
PAIRP(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00))
{ /* Llib/bigloo.scm 683 */
 obj_t BgL_funz00_1374;
BgL_funz00_1374 = 
CAR(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00); 
BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00 = 
CDR(BGl_za2bigloozd2exitzd2functionsza2z00zz__biglooz00); 
{ /* Llib/bigloo.scm 686 */
 obj_t BgL_nvalz00_1375;
{ /* Llib/bigloo.scm 686 */
 obj_t BgL_funz00_2564;
if(
PROCEDUREP(BgL_funz00_1374))
{ /* Llib/bigloo.scm 686 */
BgL_funz00_2564 = BgL_funz00_1374; }  else 
{ 
 obj_t BgL_auxz00_3560;
BgL_auxz00_3560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(26380L), BGl_string2036z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_funz00_1374); 
FAILURE(BgL_auxz00_3560,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_2564, 1))
{ /* Llib/bigloo.scm 686 */
BgL_nvalz00_1375 = 
(VA_PROCEDUREP( BgL_funz00_2564 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_2564))(BgL_funz00_1374, BgL_valz00_1372, BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_2564))(BgL_funz00_1374, BgL_valz00_1372) ); }  else 
{ /* Llib/bigloo.scm 686 */
FAILURE(BGl_string2053z00zz__biglooz00,BGl_list2054z00zz__biglooz00,BgL_funz00_2564);} } 
{ /* Llib/bigloo.scm 687 */
 obj_t BgL_arg1399z00_1376;
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_nvalz00_1375))
{ /* Llib/bigloo.scm 687 */
BgL_arg1399z00_1376 = BgL_nvalz00_1375; }  else 
{ /* Llib/bigloo.scm 687 */
BgL_arg1399z00_1376 = BgL_valz00_1372; } 
{ 
 obj_t BgL_valz00_3573;
BgL_valz00_3573 = BgL_arg1399z00_1376; 
BgL_valz00_1370 = BgL_valz00_3573; 
goto BgL_zc3z04anonymousza31396ze3z87_1371;} } } }  else 
{ /* Llib/bigloo.scm 682 */
BgL_tmp2284z00_3546 = BgL_valz00_1372; } } } 
BGL_EXITD_POP_PROTECT(BgL_top2285z00_3547); BUNSPEC; 
BGL_MUTEX_UNLOCK(BgL_mutz00_1368); 
return BgL_tmp2284z00_3546;} } } } 

}



/* &bigloo-exit-apply */
obj_t BGl_z62bigloozd2exitzd2applyz62zz__biglooz00(obj_t BgL_envz00_2340, obj_t BgL_valz00_2341)
{
{ /* Llib/bigloo.scm 673 */
return 
bigloo_exit_apply(BgL_valz00_2341);} 

}



/* time */
BGL_EXPORTED_DEF obj_t BGl_timez00zz__biglooz00(obj_t BgL_procz00_31)
{
{ /* Llib/bigloo.scm 693 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_31, 
(int)(0L)))
{ /* Llib/bigloo.scm 694 */
return 
bgl_time(BgL_procz00_31);}  else 
{ /* Llib/bigloo.scm 694 */
return 
BGl_errorz00zz__errorz00(BGl_symbol2061z00zz__biglooz00, BGl_string2048z00zz__biglooz00, BgL_procz00_31);} } 

}



/* &time */
obj_t BGl_z62timez62zz__biglooz00(obj_t BgL_envz00_2342, obj_t BgL_procz00_2343)
{
{ /* Llib/bigloo.scm 693 */
{ /* Llib/bigloo.scm 694 */
 obj_t BgL_auxz00_3582;
if(
PROCEDUREP(BgL_procz00_2343))
{ /* Llib/bigloo.scm 694 */
BgL_auxz00_3582 = BgL_procz00_2343
; }  else 
{ 
 obj_t BgL_auxz00_3585;
BgL_auxz00_3585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(26696L), BGl_string2063z00zz__biglooz00, BGl_string2018z00zz__biglooz00, BgL_procz00_2343); 
FAILURE(BgL_auxz00_3585,BFALSE,BFALSE);} 
return 
BGl_timez00zz__biglooz00(BgL_auxz00_3582);} } 

}



/* bmem-reset! */
BGL_EXPORTED_DEF obj_t BGl_bmemzd2resetz12zc0zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 709 */
BGL_TAIL return 
bgl_bmem_reset();} 

}



/* &bmem-reset! */
obj_t BGl_z62bmemzd2resetz12za2zz__biglooz00(obj_t BgL_envz00_2344)
{
{ /* Llib/bigloo.scm 709 */
return 
BGl_bmemzd2resetz12zc0zz__biglooz00();} 

}



/* gc-verbose-set! */
BGL_EXPORTED_DEF obj_t BGl_gczd2verbosezd2setz12z12zz__biglooz00(bool_t BgL_boolz00_32)
{
{ /* Llib/bigloo.scm 717 */
bgl_gc_verbose_set(BgL_boolz00_32); 
return BUNSPEC;} 

}



/* &gc-verbose-set! */
obj_t BGl_z62gczd2verbosezd2setz12z70zz__biglooz00(obj_t BgL_envz00_2345, obj_t BgL_boolz00_2346)
{
{ /* Llib/bigloo.scm 717 */
return 
BGl_gczd2verbosezd2setz12z12zz__biglooz00(
CBOOL(BgL_boolz00_2346));} 

}



/* _gc */
obj_t BGl__gcz00zz__biglooz00(obj_t BgL_env1110z00_35, obj_t BgL_opt1109z00_34)
{
{ /* Llib/bigloo.scm 725 */
{ 
 obj_t BgL_k1z00_1384; long BgL_iz00_1385;
{ /* Llib/bigloo.scm 725 */

{ /* Llib/bigloo.scm 725 */
 obj_t BgL_finaliza7eza7_1387;
BgL_finaliza7eza7_1387 = BTRUE; 
{ /* Llib/bigloo.scm 725 */

{ 
 long BgL_iz00_1388;
BgL_iz00_1388 = 0L; 
BgL_check1113z00_1389:
if(
(BgL_iz00_1388==
VECTOR_LENGTH(BgL_opt1109z00_34)))
{ /* Llib/bigloo.scm 725 */BNIL; }  else 
{ /* Llib/bigloo.scm 725 */
 bool_t BgL_test2294z00_3598;
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_arg1411z00_1395;
{ /* Llib/bigloo.scm 725 */
 bool_t BgL_test2295z00_3599;
{ /* Llib/bigloo.scm 725 */
 long BgL_tmpz00_3600;
BgL_tmpz00_3600 = 
VECTOR_LENGTH(BgL_opt1109z00_34); 
BgL_test2295z00_3599 = 
BOUND_CHECK(BgL_iz00_1388, BgL_tmpz00_3600); } 
if(BgL_test2295z00_3599)
{ /* Llib/bigloo.scm 725 */
BgL_arg1411z00_1395 = 
VECTOR_REF(BgL_opt1109z00_34,BgL_iz00_1388); }  else 
{ 
 obj_t BgL_auxz00_3604;
BgL_auxz00_3604 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(28046L), BGl_string2067z00zz__biglooz00, BgL_opt1109z00_34, 
(int)(
VECTOR_LENGTH(BgL_opt1109z00_34)), 
(int)(BgL_iz00_1388)); 
FAILURE(BgL_auxz00_3604,BFALSE,BFALSE);} } 
BgL_test2294z00_3598 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1411z00_1395, BGl_list2068z00zz__biglooz00)); } 
if(BgL_test2294z00_3598)
{ 
 long BgL_iz00_3613;
BgL_iz00_3613 = 
(BgL_iz00_1388+2L); 
BgL_iz00_1388 = BgL_iz00_3613; 
goto BgL_check1113z00_1389;}  else 
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_arg1410z00_1394;
{ /* Llib/bigloo.scm 725 */
 bool_t BgL_test2296z00_3615;
{ /* Llib/bigloo.scm 725 */
 long BgL_tmpz00_3616;
BgL_tmpz00_3616 = 
VECTOR_LENGTH(BgL_opt1109z00_34); 
BgL_test2296z00_3615 = 
BOUND_CHECK(BgL_iz00_1388, BgL_tmpz00_3616); } 
if(BgL_test2296z00_3615)
{ /* Llib/bigloo.scm 725 */
BgL_arg1410z00_1394 = 
VECTOR_REF(BgL_opt1109z00_34,BgL_iz00_1388); }  else 
{ 
 obj_t BgL_auxz00_3620;
BgL_auxz00_3620 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(28046L), BGl_string2067z00zz__biglooz00, BgL_opt1109z00_34, 
(int)(
VECTOR_LENGTH(BgL_opt1109z00_34)), 
(int)(BgL_iz00_1388)); 
FAILURE(BgL_auxz00_3620,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol2064z00zz__biglooz00, BGl_string2071z00zz__biglooz00, BgL_arg1410z00_1394); } } } 
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_index1115z00_1396;
BgL_k1z00_1384 = BGl_keyword2069z00zz__biglooz00; 
BgL_iz00_1385 = 0L; 
BgL_search1112z00_1386:
if(
(BgL_iz00_1385==
VECTOR_LENGTH(BgL_opt1109z00_34)))
{ /* Llib/bigloo.scm 725 */
BgL_index1115z00_1396 = 
BINT(-1L); }  else 
{ /* Llib/bigloo.scm 725 */
if(
(BgL_iz00_1385==
(
VECTOR_LENGTH(BgL_opt1109z00_34)-1L)))
{ /* Llib/bigloo.scm 725 */
BgL_index1115z00_1396 = 
BGl_errorz00zz__errorz00(BGl_symbol2064z00zz__biglooz00, BGl_string2066z00zz__biglooz00, 
BINT(
VECTOR_LENGTH(BgL_opt1109z00_34))); }  else 
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_vz00_1403;
BgL_vz00_1403 = 
VECTOR_REF(BgL_opt1109z00_34,BgL_iz00_1385); 
if(
(BgL_vz00_1403==BgL_k1z00_1384))
{ /* Llib/bigloo.scm 725 */
BgL_index1115z00_1396 = 
BINT(
(BgL_iz00_1385+1L)); }  else 
{ 
 long BgL_iz00_3644;
BgL_iz00_3644 = 
(BgL_iz00_1385+2L); 
BgL_iz00_1385 = BgL_iz00_3644; 
goto BgL_search1112z00_1386;} } } 
{ /* Llib/bigloo.scm 725 */
 bool_t BgL_test2300z00_3646;
{ /* Llib/bigloo.scm 725 */
 long BgL_n1z00_2096;
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_tmpz00_3647;
if(
INTEGERP(BgL_index1115z00_1396))
{ /* Llib/bigloo.scm 725 */
BgL_tmpz00_3647 = BgL_index1115z00_1396
; }  else 
{ 
 obj_t BgL_auxz00_3650;
BgL_auxz00_3650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(28046L), BGl_string2072z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_index1115z00_1396); 
FAILURE(BgL_auxz00_3650,BFALSE,BFALSE);} 
BgL_n1z00_2096 = 
(long)CINT(BgL_tmpz00_3647); } 
BgL_test2300z00_3646 = 
(BgL_n1z00_2096>=0L); } 
if(BgL_test2300z00_3646)
{ 
 long BgL_auxz00_3656;
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_tmpz00_3657;
if(
INTEGERP(BgL_index1115z00_1396))
{ /* Llib/bigloo.scm 725 */
BgL_tmpz00_3657 = BgL_index1115z00_1396
; }  else 
{ 
 obj_t BgL_auxz00_3660;
BgL_auxz00_3660 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2011z00zz__biglooz00, 
BINT(28046L), BGl_string2072z00zz__biglooz00, BGl_string2035z00zz__biglooz00, BgL_index1115z00_1396); 
FAILURE(BgL_auxz00_3660,BFALSE,BFALSE);} 
BgL_auxz00_3656 = 
(long)CINT(BgL_tmpz00_3657); } 
BgL_finaliza7eza7_1387 = 
VECTOR_REF(BgL_opt1109z00_34,BgL_auxz00_3656); }  else 
{ /* Llib/bigloo.scm 725 */BFALSE; } } } 
{ /* Llib/bigloo.scm 725 */
 obj_t BgL_finaliza7eza7_1398;
BgL_finaliza7eza7_1398 = BgL_finaliza7eza7_1387; 
GC_COLLECT(); BUNSPEC; 
if(
CBOOL(BgL_finaliza7eza7_1398))
{ /* Llib/bigloo.scm 727 */
GC_invoke_finalizers(); 
return BUNSPEC;}  else 
{ /* Llib/bigloo.scm 727 */
return BFALSE;} } } } } } } 

}



/* gc */
BGL_EXPORTED_DEF obj_t BGl_gcz00zz__biglooz00(obj_t BgL_finaliza7eza7_33)
{
{ /* Llib/bigloo.scm 725 */
GC_COLLECT(); BUNSPEC; 
if(
CBOOL(BgL_finaliza7eza7_33))
{ /* Llib/bigloo.scm 727 */
GC_invoke_finalizers(); 
return BUNSPEC;}  else 
{ /* Llib/bigloo.scm 727 */
return BFALSE;} } 

}



/* gc-finalize */
BGL_EXPORTED_DEF obj_t BGl_gczd2finaliza7ez75zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 733 */
GC_invoke_finalizers(); 
return BUNSPEC;} 

}



/* &gc-finalize */
obj_t BGl_z62gczd2finaliza7ez17zz__biglooz00(obj_t BgL_envz00_2347)
{
{ /* Llib/bigloo.scm 733 */
return 
BGl_gczd2finaliza7ez75zz__biglooz00();} 

}



/* make-cell */
BGL_EXPORTED_DEF obj_t BGl_makezd2cellzd2zz__biglooz00(obj_t BgL_valz00_36)
{
{ /* Llib/bigloo.scm 741 */
return 
MAKE_YOUNG_CELL(BgL_valz00_36);} 

}



/* &make-cell */
obj_t BGl_z62makezd2cellzb0zz__biglooz00(obj_t BgL_envz00_2348, obj_t BgL_valz00_2349)
{
{ /* Llib/bigloo.scm 741 */
return 
BGl_makezd2cellzd2zz__biglooz00(BgL_valz00_2349);} 

}



/* cell? */
BGL_EXPORTED_DEF obj_t BGl_cellzf3zf3zz__biglooz00(obj_t BgL_objz00_37)
{
{ /* Llib/bigloo.scm 747 */
return 
BBOOL(
CELLP(BgL_objz00_37));} 

}



/* &cell? */
obj_t BGl_z62cellzf3z91zz__biglooz00(obj_t BgL_envz00_2350, obj_t BgL_objz00_2351)
{
{ /* Llib/bigloo.scm 747 */
return 
BGl_cellzf3zf3zz__biglooz00(BgL_objz00_2351);} 

}



/* cell-ref */
BGL_EXPORTED_DEF obj_t BGl_cellzd2refzd2zz__biglooz00(obj_t BgL_cellz00_38)
{
{ /* Llib/bigloo.scm 753 */
return 
CELL_REF(BgL_cellz00_38);} 

}



/* &cell-ref */
obj_t BGl_z62cellzd2refzb0zz__biglooz00(obj_t BgL_envz00_2352, obj_t BgL_cellz00_2353)
{
{ /* Llib/bigloo.scm 753 */
return 
BGl_cellzd2refzd2zz__biglooz00(BgL_cellz00_2353);} 

}



/* cell-set! */
BGL_EXPORTED_DEF obj_t BGl_cellzd2setz12zc0zz__biglooz00(obj_t BgL_cellz00_39, obj_t BgL_valz00_40)
{
{ /* Llib/bigloo.scm 759 */
return 
CELL_SET(BgL_cellz00_39, BgL_valz00_40);} 

}



/* &cell-set! */
obj_t BGl_z62cellzd2setz12za2zz__biglooz00(obj_t BgL_envz00_2354, obj_t BgL_cellz00_2355, obj_t BgL_valz00_2356)
{
{ /* Llib/bigloo.scm 759 */
return 
BGl_cellzd2setz12zc0zz__biglooz00(BgL_cellz00_2355, BgL_valz00_2356);} 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__biglooz00(void)
{
{ /* Llib/bigloo.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2073z00zz__biglooz00)); 
BGl_modulezd2initializa7ationz75zz__configurez00(35034923L, 
BSTRING_TO_STRING(BGl_string2073z00zz__biglooz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string2073z00zz__biglooz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string2073z00zz__biglooz00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string2073z00zz__biglooz00)); 
return 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string2073z00zz__biglooz00));} 

}

#ifdef __cplusplus
}
#endif
