/*===========================================================================*/
/*   (Llib/i18n.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/i18n.scm -indent -o objs/obj_s/Llib/i18n.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___I18N_TYPE_DEFINITIONS
#define BGL___I18N_TYPE_DEFINITIONS
#endif // BGL___I18N_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_requirezd2initializa7ationz75zz__i18nz00 = BUNSPEC;
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__i18nz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
extern obj_t bgl_utf8_string_locale_capitalize(obj_t);
extern obj_t bgl_utf8_string_locale_upcase(obj_t);
static obj_t BGl_z62utf8zd2stringzd2localezd2downcasezb0zz__i18nz00(obj_t, obj_t);
static obj_t BGl_z62utf8zd2stringzd2localezd2upcasezb0zz__i18nz00(obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__i18nz00(void);
static obj_t BGl_importedzd2moduleszd2initz00zz__i18nz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__i18nz00(void);
static obj_t BGl_objectzd2initzd2zz__i18nz00(void);
BGL_EXPORTED_DECL int BGl_utf8zd2stringzd2localezd2compare3zd2zz__i18nz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2localezd2capitaliza7ez75zz__i18nz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2localezd2upcasezd2zz__i18nz00(obj_t);
static obj_t BGl_methodzd2initzd2zz__i18nz00(void);
BGL_EXPORTED_DECL obj_t BGl_utf8zd2stringzd2localezd2downcasezd2zz__i18nz00(obj_t);
static obj_t BGl_z62utf8zd2stringzd2localezd2capitaliza7ez17zz__i18nz00(obj_t, obj_t);
extern obj_t bgl_utf8_string_locale_downcase(obj_t);
static obj_t BGl_z62utf8zd2stringzd2localezd2compare3zb0zz__i18nz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2localezd2upcasezd2envz00zz__i18nz00, BgL_bgl_za762utf8za7d2string1613z00, BGl_z62utf8zd2stringzd2localezd2upcasezb0zz__i18nz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2localezd2compare3zd2envz00zz__i18nz00, BgL_bgl_za762utf8za7d2string1614z00, BGl_z62utf8zd2stringzd2localezd2compare3zb0zz__i18nz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2localezd2capitaliza7ezd2envza7zz__i18nz00, BgL_bgl_za762utf8za7d2string1615z00, BGl_z62utf8zd2stringzd2localezd2capitaliza7ez17zz__i18nz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1606z00zz__i18nz00, BgL_bgl_string1606za700za7za7_1616za7, "/tmp/bigloo/runtime/Llib/i18n.scm", 33 );
DEFINE_STRING( BGl_string1607z00zz__i18nz00, BgL_bgl_string1607za700za7za7_1617za7, "&utf8-string-locale-compare3", 28 );
DEFINE_STRING( BGl_string1608z00zz__i18nz00, BgL_bgl_string1608za700za7za7_1618za7, "bstring", 7 );
DEFINE_STRING( BGl_string1609z00zz__i18nz00, BgL_bgl_string1609za700za7za7_1619za7, "&utf8-string-locale-upcase", 26 );
DEFINE_STRING( BGl_string1610z00zz__i18nz00, BgL_bgl_string1610za700za7za7_1620za7, "&utf8-string-locale-downcase", 28 );
DEFINE_STRING( BGl_string1611z00zz__i18nz00, BgL_bgl_string1611za700za7za7_1621za7, "&utf8-string-locale-capitalize", 30 );
DEFINE_STRING( BGl_string1612z00zz__i18nz00, BgL_bgl_string1612za700za7za7_1622za7, "__i18n", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_utf8zd2stringzd2localezd2downcasezd2envz00zz__i18nz00, BgL_bgl_za762utf8za7d2string1623z00, BGl_z62utf8zd2stringzd2localezd2downcasezb0zz__i18nz00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__i18nz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__i18nz00(long BgL_checksumz00_1745, char * BgL_fromz00_1746)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__i18nz00))
{ 
BGl_requirezd2initializa7ationz75zz__i18nz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__i18nz00(); 
BGl_importedzd2moduleszd2initz00zz__i18nz00(); 
return 
BGl_methodzd2initzd2zz__i18nz00();}  else 
{ 
return BUNSPEC;} } 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__i18nz00(void)
{
{ /* Llib/i18n.scm 15 */
return 
bgl_gc_roots_register();} 

}



/* utf8-string-locale-compare3 */
BGL_EXPORTED_DEF int BGl_utf8zd2stringzd2localezd2compare3zd2zz__i18nz00(obj_t BgL_leftz00_3, obj_t BgL_rightz00_4)
{
{ /* Llib/i18n.scm 77 */
return 
BGL_UTF8_STRING_LOCALE_COMPARE3(BgL_leftz00_3, BgL_rightz00_4);} 

}



/* &utf8-string-locale-compare3 */
obj_t BGl_z62utf8zd2stringzd2localezd2compare3zb0zz__i18nz00(obj_t BgL_envz00_1726, obj_t BgL_leftz00_1727, obj_t BgL_rightz00_1728)
{
{ /* Llib/i18n.scm 77 */
{ /* Llib/i18n.scm 78 */
 int BgL_tmpz00_1755;
{ /* Llib/i18n.scm 78 */
 obj_t BgL_auxz00_1763; obj_t BgL_auxz00_1756;
if(
STRINGP(BgL_rightz00_1728))
{ /* Llib/i18n.scm 78 */
BgL_auxz00_1763 = BgL_rightz00_1728
; }  else 
{ 
 obj_t BgL_auxz00_1766;
BgL_auxz00_1766 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1606z00zz__i18nz00, 
BINT(2946L), BGl_string1607z00zz__i18nz00, BGl_string1608z00zz__i18nz00, BgL_rightz00_1728); 
FAILURE(BgL_auxz00_1766,BFALSE,BFALSE);} 
if(
STRINGP(BgL_leftz00_1727))
{ /* Llib/i18n.scm 78 */
BgL_auxz00_1756 = BgL_leftz00_1727
; }  else 
{ 
 obj_t BgL_auxz00_1759;
BgL_auxz00_1759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1606z00zz__i18nz00, 
BINT(2946L), BGl_string1607z00zz__i18nz00, BGl_string1608z00zz__i18nz00, BgL_leftz00_1727); 
FAILURE(BgL_auxz00_1759,BFALSE,BFALSE);} 
BgL_tmpz00_1755 = 
BGl_utf8zd2stringzd2localezd2compare3zd2zz__i18nz00(BgL_auxz00_1756, BgL_auxz00_1763); } 
return 
BINT(BgL_tmpz00_1755);} } 

}



/* utf8-string-locale-upcase */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2localezd2upcasezd2zz__i18nz00(obj_t BgL_strz00_5)
{
{ /* Llib/i18n.scm 83 */
BGL_TAIL return 
bgl_utf8_string_locale_upcase(BgL_strz00_5);} 

}



/* &utf8-string-locale-upcase */
obj_t BGl_z62utf8zd2stringzd2localezd2upcasezb0zz__i18nz00(obj_t BgL_envz00_1729, obj_t BgL_strz00_1730)
{
{ /* Llib/i18n.scm 83 */
{ /* Llib/i18n.scm 86 */
 obj_t BgL_auxz00_1773;
if(
STRINGP(BgL_strz00_1730))
{ /* Llib/i18n.scm 86 */
BgL_auxz00_1773 = BgL_strz00_1730
; }  else 
{ 
 obj_t BgL_auxz00_1776;
BgL_auxz00_1776 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1606z00zz__i18nz00, 
BINT(3326L), BGl_string1609z00zz__i18nz00, BGl_string1608z00zz__i18nz00, BgL_strz00_1730); 
FAILURE(BgL_auxz00_1776,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2localezd2upcasezd2zz__i18nz00(BgL_auxz00_1773);} } 

}



/* utf8-string-locale-downcase */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2localezd2downcasezd2zz__i18nz00(obj_t BgL_strz00_6)
{
{ /* Llib/i18n.scm 91 */
BGL_TAIL return 
bgl_utf8_string_locale_downcase(BgL_strz00_6);} 

}



/* &utf8-string-locale-downcase */
obj_t BGl_z62utf8zd2stringzd2localezd2downcasezb0zz__i18nz00(obj_t BgL_envz00_1731, obj_t BgL_strz00_1732)
{
{ /* Llib/i18n.scm 91 */
{ /* Llib/i18n.scm 94 */
 obj_t BgL_auxz00_1782;
if(
STRINGP(BgL_strz00_1732))
{ /* Llib/i18n.scm 94 */
BgL_auxz00_1782 = BgL_strz00_1732
; }  else 
{ 
 obj_t BgL_auxz00_1785;
BgL_auxz00_1785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1606z00zz__i18nz00, 
BINT(3703L), BGl_string1610z00zz__i18nz00, BGl_string1608z00zz__i18nz00, BgL_strz00_1732); 
FAILURE(BgL_auxz00_1785,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2localezd2downcasezd2zz__i18nz00(BgL_auxz00_1782);} } 

}



/* utf8-string-locale-capitalize */
BGL_EXPORTED_DEF obj_t BGl_utf8zd2stringzd2localezd2capitaliza7ez75zz__i18nz00(obj_t BgL_strz00_7)
{
{ /* Llib/i18n.scm 99 */
BGL_TAIL return 
bgl_utf8_string_locale_capitalize(BgL_strz00_7);} 

}



/* &utf8-string-locale-capitalize */
obj_t BGl_z62utf8zd2stringzd2localezd2capitaliza7ez17zz__i18nz00(obj_t BgL_envz00_1733, obj_t BgL_strz00_1734)
{
{ /* Llib/i18n.scm 99 */
{ /* Llib/i18n.scm 102 */
 obj_t BgL_auxz00_1791;
if(
STRINGP(BgL_strz00_1734))
{ /* Llib/i18n.scm 102 */
BgL_auxz00_1791 = BgL_strz00_1734
; }  else 
{ 
 obj_t BgL_auxz00_1794;
BgL_auxz00_1794 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1606z00zz__i18nz00, 
BINT(4086L), BGl_string1611z00zz__i18nz00, BGl_string1608z00zz__i18nz00, BgL_strz00_1734); 
FAILURE(BgL_auxz00_1794,BFALSE,BFALSE);} 
return 
BGl_utf8zd2stringzd2localezd2capitaliza7ez75zz__i18nz00(BgL_auxz00_1791);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__i18nz00(void)
{
{ /* Llib/i18n.scm 15 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__i18nz00(void)
{
{ /* Llib/i18n.scm 15 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__i18nz00(void)
{
{ /* Llib/i18n.scm 15 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__i18nz00(void)
{
{ /* Llib/i18n.scm 15 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1612z00zz__i18nz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1612z00zz__i18nz00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string1612z00zz__i18nz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1612z00zz__i18nz00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1612z00zz__i18nz00));} 

}

#ifdef __cplusplus
}
#endif
