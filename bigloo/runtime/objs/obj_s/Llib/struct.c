/*===========================================================================*/
/*   (Llib/struct.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/struct.scm -indent -o objs/obj_s/Llib/struct.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___STRUCTURE_TYPE_DEFINITIONS
#define BGL___STRUCTURE_TYPE_DEFINITIONS
#endif // BGL___STRUCTURE_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_structzd2refzd2zz__structurez00(obj_t, int);
static obj_t BGl_z62makezd2structzb0zz__structurez00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__structurez00 = BUNSPEC;
static obj_t BGl_z62listzd2ze3structz53zz__structurez00(obj_t, obj_t);
BGL_EXPORTED_DECL int BGl_structzd2lengthzd2zz__structurez00(obj_t);
static obj_t BGl_z62structzd2refzb0zz__structurez00(obj_t, obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_structzd2keyzd2zz__structurez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3structz31zz__structurez00(obj_t);
static obj_t BGl_z62structzd2keyzb0zz__structurez00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__structurez00(void);
extern long bgl_list_length(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__structurez00(void);
static obj_t BGl_gczd2rootszd2initz00zz__structurez00(void);
static obj_t BGl_z62structzd2lengthzb0zz__structurez00(obj_t, obj_t);
static obj_t BGl_z62structzd2updatez12za2zz__structurez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_recordzf3zf3zz__structurez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_structzd2setz12zc0zz__structurez00(obj_t, int, obj_t);
static obj_t BGl_z62recordzf3z91zz__structurez00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2structzd2zz__structurez00(obj_t, int, obj_t);
static obj_t BGl_symbol1526z00zz__structurez00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_structzf3zf3zz__structurez00(obj_t);
static obj_t BGl_z62structzf3z91zz__structurez00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62structzd2ze3listz53zz__structurez00(obj_t, obj_t);
extern obj_t make_struct(obj_t, int, obj_t);
BGL_EXPORTED_DECL obj_t BGl_structzd2keyzd2setz12z12zz__structurez00(obj_t, obj_t);
static obj_t BGl_z62structzd2keyzd2setz12z70zz__structurez00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_structzd2ze3listz31zz__structurez00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_structzd2updatez12zc0zz__structurez00(obj_t, obj_t);
static obj_t BGl_z62structzd2setz12za2zz__structurez00(obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2lengthzd2envz00zz__structurez00, BgL_bgl_za762structza7d2leng1530z00, BGl_z62structzd2lengthzb0zz__structurez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2setz12zd2envz12zz__structurez00, BgL_bgl_za762structza7d2setza71531za7, BGl_z62structzd2setz12za2zz__structurez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3structzd2envze3zz__structurez00, BgL_bgl_za762listza7d2za7e3str1532za7, BGl_z62listzd2ze3structz53zz__structurez00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1507z00zz__structurez00, BgL_bgl_string1507za700za7za7_1533za7, "/tmp/bigloo/runtime/Llib/struct.scm", 35 );
DEFINE_STRING( BGl_string1508z00zz__structurez00, BgL_bgl_string1508za700za7za7_1534za7, "&make-struct", 12 );
DEFINE_STRING( BGl_string1509z00zz__structurez00, BgL_bgl_string1509za700za7za7_1535za7, "symbol", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2updatez12zd2envz12zz__structurez00, BgL_bgl_za762structza7d2upda1536z00, BGl_z62structzd2updatez12za2zz__structurez00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1510z00zz__structurez00, BgL_bgl_string1510za700za7za7_1537za7, "bint", 4 );
DEFINE_STRING( BGl_string1511z00zz__structurez00, BgL_bgl_string1511za700za7za7_1538za7, "struct-key", 10 );
DEFINE_STRING( BGl_string1512z00zz__structurez00, BgL_bgl_string1512za700za7za7_1539za7, "&struct-key", 11 );
DEFINE_STRING( BGl_string1513z00zz__structurez00, BgL_bgl_string1513za700za7za7_1540za7, "struct", 6 );
DEFINE_STRING( BGl_string1514z00zz__structurez00, BgL_bgl_string1514za700za7za7_1541za7, "&struct-key-set!", 16 );
DEFINE_STRING( BGl_string1515z00zz__structurez00, BgL_bgl_string1515za700za7za7_1542za7, "&struct-length", 14 );
DEFINE_STRING( BGl_string1516z00zz__structurez00, BgL_bgl_string1516za700za7za7_1543za7, "&struct-ref", 11 );
DEFINE_STRING( BGl_string1517z00zz__structurez00, BgL_bgl_string1517za700za7za7_1544za7, "&struct-set!", 12 );
DEFINE_STRING( BGl_string1518z00zz__structurez00, BgL_bgl_string1518za700za7za7_1545za7, "struct-update!", 14 );
DEFINE_STRING( BGl_string1519z00zz__structurez00, BgL_bgl_string1519za700za7za7_1546za7, "Incompatible structures", 23 );
DEFINE_STRING( BGl_string1520z00zz__structurez00, BgL_bgl_string1520za700za7za7_1547za7, "&struct-update!", 15 );
DEFINE_STRING( BGl_string1521z00zz__structurez00, BgL_bgl_string1521za700za7za7_1548za7, "loop", 4 );
DEFINE_STRING( BGl_string1522z00zz__structurez00, BgL_bgl_string1522za700za7za7_1549za7, "&struct->list", 13 );
DEFINE_STRING( BGl_string1523z00zz__structurez00, BgL_bgl_string1523za700za7za7_1550za7, "list->struct", 12 );
DEFINE_STRING( BGl_string1524z00zz__structurez00, BgL_bgl_string1524za700za7za7_1551za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string1525z00zz__structurez00, BgL_bgl_string1525za700za7za7_1552za7, "pair", 4 );
DEFINE_STRING( BGl_string1527z00zz__structurez00, BgL_bgl_string1527za700za7za7_1553za7, "Illegal struct key", 18 );
DEFINE_STRING( BGl_string1528z00zz__structurez00, BgL_bgl_string1528za700za7za7_1554za7, "&list->struct", 13 );
DEFINE_STRING( BGl_string1529z00zz__structurez00, BgL_bgl_string1529za700za7za7_1555za7, "__structure", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2structzd2envz00zz__structurez00, BgL_bgl_za762makeza7d2struct1556z00, BGl_z62makezd2structzb0zz__structurez00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzf3zd2envz21zz__structurez00, BgL_bgl_za762structza7f3za791za71557z00, BGl_z62structzf3z91zz__structurez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2ze3listzd2envze3zz__structurez00, BgL_bgl_za762structza7d2za7e3l1558za7, BGl_z62structzd2ze3listz53zz__structurez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2refzd2envz00zz__structurez00, BgL_bgl_za762structza7d2refza71559za7, BGl_z62structzd2refzb0zz__structurez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2keyzd2setz12zd2envzc0zz__structurez00, BgL_bgl_za762structza7d2keyza71560za7, BGl_z62structzd2keyzd2setz12z70zz__structurez00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_structzd2keyzd2envz00zz__structurez00, BgL_bgl_za762structza7d2keyza71561za7, BGl_z62structzd2keyzb0zz__structurez00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_recordzf3zd2envz21zz__structurez00, BgL_bgl_za762recordza7f3za791za71562z00, BGl_z62recordzf3z91zz__structurez00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__structurez00) );
ADD_ROOT( (void *)(&BGl_symbol1526z00zz__structurez00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long BgL_checksumz00_1254, char * BgL_fromz00_1255)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__structurez00))
{ 
BGl_requirezd2initializa7ationz75zz__structurez00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__structurez00(); 
BGl_cnstzd2initzd2zz__structurez00(); 
return 
BGl_importedzd2moduleszd2initz00zz__structurez00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__structurez00(void)
{
{ /* Llib/struct.scm 18 */
return ( 
BGl_symbol1526z00zz__structurez00 = 
bstring_to_symbol(BGl_string1523z00zz__structurez00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__structurez00(void)
{
{ /* Llib/struct.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* make-struct */
BGL_EXPORTED_DEF obj_t BGl_makezd2structzd2zz__structurez00(obj_t BgL_keyz00_3, int BgL_lenz00_4, obj_t BgL_initz00_5)
{
{ /* Llib/struct.scm 119 */
BGL_TAIL return 
make_struct(BgL_keyz00_3, BgL_lenz00_4, BgL_initz00_5);} 

}



/* &make-struct */
obj_t BGl_z62makezd2structzb0zz__structurez00(obj_t BgL_envz00_1179, obj_t BgL_keyz00_1180, obj_t BgL_lenz00_1181, obj_t BgL_initz00_1182)
{
{ /* Llib/struct.scm 119 */
{ /* Llib/struct.scm 120 */
 int BgL_auxz00_1272; obj_t BgL_auxz00_1265;
{ /* Llib/struct.scm 120 */
 obj_t BgL_tmpz00_1273;
if(
INTEGERP(BgL_lenz00_1181))
{ /* Llib/struct.scm 120 */
BgL_tmpz00_1273 = BgL_lenz00_1181
; }  else 
{ 
 obj_t BgL_auxz00_1276;
BgL_auxz00_1276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5074L), BGl_string1508z00zz__structurez00, BGl_string1510z00zz__structurez00, BgL_lenz00_1181); 
FAILURE(BgL_auxz00_1276,BFALSE,BFALSE);} 
BgL_auxz00_1272 = 
CINT(BgL_tmpz00_1273); } 
if(
SYMBOLP(BgL_keyz00_1180))
{ /* Llib/struct.scm 120 */
BgL_auxz00_1265 = BgL_keyz00_1180
; }  else 
{ 
 obj_t BgL_auxz00_1268;
BgL_auxz00_1268 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5074L), BGl_string1508z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_keyz00_1180); 
FAILURE(BgL_auxz00_1268,BFALSE,BFALSE);} 
return 
BGl_makezd2structzd2zz__structurez00(BgL_auxz00_1265, BgL_auxz00_1272, BgL_initz00_1182);} } 

}



/* struct? */
BGL_EXPORTED_DEF bool_t BGl_structzf3zf3zz__structurez00(obj_t BgL_oz00_6)
{
{ /* Llib/struct.scm 125 */
return 
STRUCTP(BgL_oz00_6);} 

}



/* &struct? */
obj_t BGl_z62structzf3z91zz__structurez00(obj_t BgL_envz00_1183, obj_t BgL_oz00_1184)
{
{ /* Llib/struct.scm 125 */
return 
BBOOL(
BGl_structzf3zf3zz__structurez00(BgL_oz00_1184));} 

}



/* record? */
BGL_EXPORTED_DEF bool_t BGl_recordzf3zf3zz__structurez00(obj_t BgL_oz00_7)
{
{ /* Llib/struct.scm 131 */
return 
STRUCTP(BgL_oz00_7);} 

}



/* &record? */
obj_t BGl_z62recordzf3z91zz__structurez00(obj_t BgL_envz00_1185, obj_t BgL_oz00_1186)
{
{ /* Llib/struct.scm 131 */
return 
BBOOL(
BGl_recordzf3zf3zz__structurez00(BgL_oz00_1186));} 

}



/* struct-key */
BGL_EXPORTED_DEF obj_t BGl_structzd2keyzd2zz__structurez00(obj_t BgL_sz00_8)
{
{ /* Llib/struct.scm 137 */
{ /* Llib/struct.scm 138 */
 obj_t BgL_aux1467z00_1252;
BgL_aux1467z00_1252 = 
STRUCT_KEY(BgL_sz00_8); 
if(
SYMBOLP(BgL_aux1467z00_1252))
{ /* Llib/struct.scm 138 */
return BgL_aux1467z00_1252;}  else 
{ 
 obj_t BgL_auxz00_1291;
BgL_auxz00_1291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5893L), BGl_string1511z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_aux1467z00_1252); 
FAILURE(BgL_auxz00_1291,BFALSE,BFALSE);} } } 

}



/* &struct-key */
obj_t BGl_z62structzd2keyzb0zz__structurez00(obj_t BgL_envz00_1187, obj_t BgL_sz00_1188)
{
{ /* Llib/struct.scm 137 */
{ /* Llib/struct.scm 138 */
 obj_t BgL_auxz00_1295;
if(
STRUCTP(BgL_sz00_1188))
{ /* Llib/struct.scm 138 */
BgL_auxz00_1295 = BgL_sz00_1188
; }  else 
{ 
 obj_t BgL_auxz00_1298;
BgL_auxz00_1298 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5893L), BGl_string1512z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_sz00_1188); 
FAILURE(BgL_auxz00_1298,BFALSE,BFALSE);} 
return 
BGl_structzd2keyzd2zz__structurez00(BgL_auxz00_1295);} } 

}



/* struct-key-set! */
BGL_EXPORTED_DEF obj_t BGl_structzd2keyzd2setz12z12zz__structurez00(obj_t BgL_sz00_9, obj_t BgL_kz00_10)
{
{ /* Llib/struct.scm 143 */
return 
STRUCT_KEY_SET(BgL_sz00_9, BgL_kz00_10);} 

}



/* &struct-key-set! */
obj_t BGl_z62structzd2keyzd2setz12z70zz__structurez00(obj_t BgL_envz00_1189, obj_t BgL_sz00_1190, obj_t BgL_kz00_1191)
{
{ /* Llib/struct.scm 143 */
{ /* Llib/struct.scm 144 */
 obj_t BgL_auxz00_1311; obj_t BgL_auxz00_1304;
if(
SYMBOLP(BgL_kz00_1191))
{ /* Llib/struct.scm 144 */
BgL_auxz00_1311 = BgL_kz00_1191
; }  else 
{ 
 obj_t BgL_auxz00_1314;
BgL_auxz00_1314 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(6173L), BGl_string1514z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_kz00_1191); 
FAILURE(BgL_auxz00_1314,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_sz00_1190))
{ /* Llib/struct.scm 144 */
BgL_auxz00_1304 = BgL_sz00_1190
; }  else 
{ 
 obj_t BgL_auxz00_1307;
BgL_auxz00_1307 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(6173L), BGl_string1514z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_sz00_1190); 
FAILURE(BgL_auxz00_1307,BFALSE,BFALSE);} 
return 
BGl_structzd2keyzd2setz12z12zz__structurez00(BgL_auxz00_1304, BgL_auxz00_1311);} } 

}



/* struct-length */
BGL_EXPORTED_DEF int BGl_structzd2lengthzd2zz__structurez00(obj_t BgL_sz00_11)
{
{ /* Llib/struct.scm 149 */
return 
STRUCT_LENGTH(BgL_sz00_11);} 

}



/* &struct-length */
obj_t BGl_z62structzd2lengthzb0zz__structurez00(obj_t BgL_envz00_1192, obj_t BgL_sz00_1193)
{
{ /* Llib/struct.scm 149 */
{ /* Llib/struct.scm 150 */
 int BgL_tmpz00_1320;
{ /* Llib/struct.scm 150 */
 obj_t BgL_auxz00_1321;
if(
STRUCTP(BgL_sz00_1193))
{ /* Llib/struct.scm 150 */
BgL_auxz00_1321 = BgL_sz00_1193
; }  else 
{ 
 obj_t BgL_auxz00_1324;
BgL_auxz00_1324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(6457L), BGl_string1515z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_sz00_1193); 
FAILURE(BgL_auxz00_1324,BFALSE,BFALSE);} 
BgL_tmpz00_1320 = 
BGl_structzd2lengthzd2zz__structurez00(BgL_auxz00_1321); } 
return 
BINT(BgL_tmpz00_1320);} } 

}



/* struct-ref */
BGL_EXPORTED_DEF obj_t BGl_structzd2refzd2zz__structurez00(obj_t BgL_sz00_12, int BgL_kz00_13)
{
{ /* Llib/struct.scm 155 */
return 
STRUCT_REF(BgL_sz00_12, BgL_kz00_13);} 

}



/* &struct-ref */
obj_t BGl_z62structzd2refzb0zz__structurez00(obj_t BgL_envz00_1194, obj_t BgL_sz00_1195, obj_t BgL_kz00_1196)
{
{ /* Llib/struct.scm 155 */
{ /* Llib/struct.scm 156 */
 int BgL_auxz00_1338; obj_t BgL_auxz00_1331;
{ /* Llib/struct.scm 156 */
 obj_t BgL_tmpz00_1339;
if(
INTEGERP(BgL_kz00_1196))
{ /* Llib/struct.scm 156 */
BgL_tmpz00_1339 = BgL_kz00_1196
; }  else 
{ 
 obj_t BgL_auxz00_1342;
BgL_auxz00_1342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(6739L), BGl_string1516z00zz__structurez00, BGl_string1510z00zz__structurez00, BgL_kz00_1196); 
FAILURE(BgL_auxz00_1342,BFALSE,BFALSE);} 
BgL_auxz00_1338 = 
CINT(BgL_tmpz00_1339); } 
if(
STRUCTP(BgL_sz00_1195))
{ /* Llib/struct.scm 156 */
BgL_auxz00_1331 = BgL_sz00_1195
; }  else 
{ 
 obj_t BgL_auxz00_1334;
BgL_auxz00_1334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(6739L), BGl_string1516z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_sz00_1195); 
FAILURE(BgL_auxz00_1334,BFALSE,BFALSE);} 
return 
BGl_structzd2refzd2zz__structurez00(BgL_auxz00_1331, BgL_auxz00_1338);} } 

}



/* struct-set! */
BGL_EXPORTED_DEF obj_t BGl_structzd2setz12zc0zz__structurez00(obj_t BgL_sz00_14, int BgL_kz00_15, obj_t BgL_oz00_16)
{
{ /* Llib/struct.scm 161 */
return 
STRUCT_SET(BgL_sz00_14, BgL_kz00_15, BgL_oz00_16);} 

}



/* &struct-set! */
obj_t BGl_z62structzd2setz12za2zz__structurez00(obj_t BgL_envz00_1197, obj_t BgL_sz00_1198, obj_t BgL_kz00_1199, obj_t BgL_oz00_1200)
{
{ /* Llib/struct.scm 161 */
{ /* Llib/struct.scm 162 */
 int BgL_auxz00_1356; obj_t BgL_auxz00_1349;
{ /* Llib/struct.scm 162 */
 obj_t BgL_tmpz00_1357;
if(
INTEGERP(BgL_kz00_1199))
{ /* Llib/struct.scm 162 */
BgL_tmpz00_1357 = BgL_kz00_1199
; }  else 
{ 
 obj_t BgL_auxz00_1360;
BgL_auxz00_1360 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(7020L), BGl_string1517z00zz__structurez00, BGl_string1510z00zz__structurez00, BgL_kz00_1199); 
FAILURE(BgL_auxz00_1360,BFALSE,BFALSE);} 
BgL_auxz00_1356 = 
CINT(BgL_tmpz00_1357); } 
if(
STRUCTP(BgL_sz00_1198))
{ /* Llib/struct.scm 162 */
BgL_auxz00_1349 = BgL_sz00_1198
; }  else 
{ 
 obj_t BgL_auxz00_1352;
BgL_auxz00_1352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(7020L), BGl_string1517z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_sz00_1198); 
FAILURE(BgL_auxz00_1352,BFALSE,BFALSE);} 
return 
BGl_structzd2setz12zc0zz__structurez00(BgL_auxz00_1349, BgL_auxz00_1356, BgL_oz00_1200);} } 

}



/* struct-update! */
BGL_EXPORTED_DEF obj_t BGl_structzd2updatez12zc0zz__structurez00(obj_t BgL_dstz00_17, obj_t BgL_srcz00_18)
{
{ /* Llib/struct.scm 167 */
{ /* Llib/struct.scm 168 */
 bool_t BgL_test1575z00_1366;
{ /* Llib/struct.scm 168 */
 bool_t BgL_test1576z00_1367;
{ /* Llib/struct.scm 168 */
 obj_t BgL_auxz00_1376; obj_t BgL_tmpz00_1368;
{ /* Llib/struct.scm 168 */
 obj_t BgL_res1461z00_1076;
{ /* Llib/struct.scm 138 */
 obj_t BgL_aux1485z00_1229;
BgL_aux1485z00_1229 = 
STRUCT_KEY(BgL_srcz00_18); 
if(
SYMBOLP(BgL_aux1485z00_1229))
{ /* Llib/struct.scm 138 */
BgL_res1461z00_1076 = BgL_aux1485z00_1229; }  else 
{ 
 obj_t BgL_auxz00_1380;
BgL_auxz00_1380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5893L), BGl_string1518z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_aux1485z00_1229); 
FAILURE(BgL_auxz00_1380,BFALSE,BFALSE);} } 
BgL_auxz00_1376 = BgL_res1461z00_1076; } 
{ /* Llib/struct.scm 168 */
 obj_t BgL_res1460z00_1074;
{ /* Llib/struct.scm 138 */
 obj_t BgL_aux1483z00_1227;
BgL_aux1483z00_1227 = 
STRUCT_KEY(BgL_dstz00_17); 
if(
SYMBOLP(BgL_aux1483z00_1227))
{ /* Llib/struct.scm 138 */
BgL_res1460z00_1074 = BgL_aux1483z00_1227; }  else 
{ 
 obj_t BgL_auxz00_1372;
BgL_auxz00_1372 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5893L), BGl_string1518z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_aux1483z00_1227); 
FAILURE(BgL_auxz00_1372,BFALSE,BFALSE);} } 
BgL_tmpz00_1368 = BgL_res1460z00_1074; } 
BgL_test1576z00_1367 = 
(BgL_tmpz00_1368==BgL_auxz00_1376); } 
if(BgL_test1576z00_1367)
{ /* Llib/struct.scm 168 */
BgL_test1575z00_1366 = 
(
(long)(
STRUCT_LENGTH(BgL_dstz00_17))==
(long)(
STRUCT_LENGTH(BgL_srcz00_18)))
; }  else 
{ /* Llib/struct.scm 168 */
BgL_test1575z00_1366 = ((bool_t)0)
; } } 
if(BgL_test1575z00_1366)
{ /* Llib/struct.scm 170 */
 long BgL_g1012z00_759;
BgL_g1012z00_759 = 
(
(long)(
STRUCT_LENGTH(BgL_dstz00_17))-1L); 
{ 
 long BgL_iz00_761;
BgL_iz00_761 = BgL_g1012z00_759; 
BgL_zc3z04anonymousza31068ze3z87_762:
if(
(BgL_iz00_761==-1L))
{ /* Llib/struct.scm 171 */
return BgL_dstz00_17;}  else 
{ /* Llib/struct.scm 171 */
{ /* Llib/struct.scm 162 */
 obj_t BgL_auxz00_1397; int BgL_tmpz00_1395;
BgL_auxz00_1397 = 
STRUCT_REF(BgL_srcz00_18, 
(int)(BgL_iz00_761)); 
BgL_tmpz00_1395 = 
(int)(BgL_iz00_761); 
STRUCT_SET(BgL_dstz00_17, BgL_tmpz00_1395, BgL_auxz00_1397); } 
{ 
 long BgL_iz00_1401;
BgL_iz00_1401 = 
(BgL_iz00_761-1L); 
BgL_iz00_761 = BgL_iz00_1401; 
goto BgL_zc3z04anonymousza31068ze3z87_762;} } } }  else 
{ /* Llib/struct.scm 176 */
 obj_t BgL_arg1078z00_768;
{ /* Llib/struct.scm 176 */
 obj_t BgL_list1079z00_769;
{ /* Llib/struct.scm 176 */
 obj_t BgL_arg1080z00_770;
BgL_arg1080z00_770 = 
MAKE_YOUNG_PAIR(BgL_srcz00_18, BNIL); 
BgL_list1079z00_769 = 
MAKE_YOUNG_PAIR(BgL_dstz00_17, BgL_arg1080z00_770); } 
BgL_arg1078z00_768 = BgL_list1079z00_769; } 
return 
BGl_errorz00zz__errorz00(BGl_string1518z00zz__structurez00, BGl_string1519z00zz__structurez00, BgL_arg1078z00_768);} } } 

}



/* &struct-update! */
obj_t BGl_z62structzd2updatez12za2zz__structurez00(obj_t BgL_envz00_1201, obj_t BgL_dstz00_1202, obj_t BgL_srcz00_1203)
{
{ /* Llib/struct.scm 167 */
{ /* Llib/struct.scm 168 */
 obj_t BgL_auxz00_1413; obj_t BgL_auxz00_1406;
if(
STRUCTP(BgL_srcz00_1203))
{ /* Llib/struct.scm 168 */
BgL_auxz00_1413 = BgL_srcz00_1203
; }  else 
{ 
 obj_t BgL_auxz00_1416;
BgL_auxz00_1416 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(7301L), BGl_string1520z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_srcz00_1203); 
FAILURE(BgL_auxz00_1416,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_dstz00_1202))
{ /* Llib/struct.scm 168 */
BgL_auxz00_1406 = BgL_dstz00_1202
; }  else 
{ 
 obj_t BgL_auxz00_1409;
BgL_auxz00_1409 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(7301L), BGl_string1520z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_dstz00_1202); 
FAILURE(BgL_auxz00_1409,BFALSE,BFALSE);} 
return 
BGl_structzd2updatez12zc0zz__structurez00(BgL_auxz00_1406, BgL_auxz00_1413);} } 

}



/* struct->list */
BGL_EXPORTED_DEF obj_t BGl_structzd2ze3listz31zz__structurez00(obj_t BgL_structz00_19)
{
{ /* Llib/struct.scm 181 */
{ /* Llib/struct.scm 182 */
 long BgL_g1013z00_776;
BgL_g1013z00_776 = 
(
(long)(
STRUCT_LENGTH(BgL_structz00_19))-1L); 
{ 
 long BgL_iz00_779; obj_t BgL_rz00_780;
BgL_iz00_779 = BgL_g1013z00_776; 
BgL_rz00_780 = BNIL; 
BgL_zc3z04anonymousza31086ze3z87_781:
if(
(BgL_iz00_779==-1L))
{ /* Llib/struct.scm 185 */
 obj_t BgL_arg1088z00_783;
{ /* Llib/struct.scm 185 */
 obj_t BgL_res1463z00_1094;
{ /* Llib/struct.scm 138 */
 obj_t BgL_aux1491z00_1235;
BgL_aux1491z00_1235 = 
STRUCT_KEY(BgL_structz00_19); 
if(
SYMBOLP(BgL_aux1491z00_1235))
{ /* Llib/struct.scm 138 */
BgL_res1463z00_1094 = BgL_aux1491z00_1235; }  else 
{ 
 obj_t BgL_auxz00_1429;
BgL_auxz00_1429 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(5893L), BGl_string1521z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_aux1491z00_1235); 
FAILURE(BgL_auxz00_1429,BFALSE,BFALSE);} } 
BgL_arg1088z00_783 = BgL_res1463z00_1094; } 
return 
MAKE_YOUNG_PAIR(BgL_arg1088z00_783, BgL_rz00_780);}  else 
{ /* Llib/struct.scm 186 */
 long BgL_arg1090z00_784; obj_t BgL_arg1092z00_785;
BgL_arg1090z00_784 = 
(BgL_iz00_779-1L); 
BgL_arg1092z00_785 = 
MAKE_YOUNG_PAIR(
STRUCT_REF(BgL_structz00_19, 
(int)(BgL_iz00_779)), BgL_rz00_780); 
{ 
 obj_t BgL_rz00_1439; long BgL_iz00_1438;
BgL_iz00_1438 = BgL_arg1090z00_784; 
BgL_rz00_1439 = BgL_arg1092z00_785; 
BgL_rz00_780 = BgL_rz00_1439; 
BgL_iz00_779 = BgL_iz00_1438; 
goto BgL_zc3z04anonymousza31086ze3z87_781;} } } } } 

}



/* &struct->list */
obj_t BGl_z62structzd2ze3listz53zz__structurez00(obj_t BgL_envz00_1204, obj_t BgL_structz00_1205)
{
{ /* Llib/struct.scm 181 */
{ /* Llib/struct.scm 182 */
 obj_t BgL_auxz00_1440;
if(
STRUCTP(BgL_structz00_1205))
{ /* Llib/struct.scm 182 */
BgL_auxz00_1440 = BgL_structz00_1205
; }  else 
{ 
 obj_t BgL_auxz00_1443;
BgL_auxz00_1443 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(7892L), BGl_string1522z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_structz00_1205); 
FAILURE(BgL_auxz00_1443,BFALSE,BFALSE);} 
return 
BGl_structzd2ze3listz31zz__structurez00(BgL_auxz00_1440);} } 

}



/* list->struct */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3structz31zz__structurez00(obj_t BgL_lstz00_20)
{
{ /* Llib/struct.scm 191 */
{ /* Llib/struct.scm 195 */
 bool_t BgL_test1586z00_1448;
{ /* Llib/struct.scm 195 */
 obj_t BgL_tmpz00_1449;
BgL_tmpz00_1449 = 
CAR(BgL_lstz00_20); 
BgL_test1586z00_1448 = 
SYMBOLP(BgL_tmpz00_1449); } 
if(BgL_test1586z00_1448)
{ /* Llib/struct.scm 198 */
 long BgL_lenz00_792;
{ /* Llib/struct.scm 198 */
 obj_t BgL_auxz00_1452;
{ /* Llib/struct.scm 198 */
 obj_t BgL_aux1495z00_1239;
BgL_aux1495z00_1239 = 
CDR(BgL_lstz00_20); 
{ /* Llib/struct.scm 198 */
 bool_t BgL_test1587z00_1454;
if(
PAIRP(BgL_aux1495z00_1239))
{ /* Llib/struct.scm 198 */
BgL_test1587z00_1454 = ((bool_t)1)
; }  else 
{ /* Llib/struct.scm 198 */
BgL_test1587z00_1454 = 
NULLP(BgL_aux1495z00_1239)
; } 
if(BgL_test1587z00_1454)
{ /* Llib/struct.scm 198 */
BgL_auxz00_1452 = BgL_aux1495z00_1239
; }  else 
{ 
 obj_t BgL_auxz00_1458;
BgL_auxz00_1458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(8527L), BGl_string1523z00zz__structurez00, BGl_string1524z00zz__structurez00, BgL_aux1495z00_1239); 
FAILURE(BgL_auxz00_1458,BFALSE,BFALSE);} } } 
BgL_lenz00_792 = 
bgl_list_length(BgL_auxz00_1452); } 
{ /* Llib/struct.scm 198 */
 obj_t BgL_structz00_793;
{ /* Llib/struct.scm 199 */
 obj_t BgL_arg1123z00_804;
BgL_arg1123z00_804 = 
CAR(BgL_lstz00_20); 
{ /* Llib/struct.scm 199 */
 obj_t BgL_keyz00_1101; int BgL_lenz00_1102;
if(
SYMBOLP(BgL_arg1123z00_804))
{ /* Llib/struct.scm 199 */
BgL_keyz00_1101 = BgL_arg1123z00_804; }  else 
{ 
 obj_t BgL_auxz00_1466;
BgL_auxz00_1466 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(8575L), BGl_string1523z00zz__structurez00, BGl_string1509z00zz__structurez00, BgL_arg1123z00_804); 
FAILURE(BgL_auxz00_1466,BFALSE,BFALSE);} 
BgL_lenz00_1102 = 
(int)(BgL_lenz00_792); 
BgL_structz00_793 = 
make_struct(BgL_keyz00_1101, BgL_lenz00_1102, BUNSPEC); } } 
{ /* Llib/struct.scm 199 */

{ /* Llib/struct.scm 200 */
 obj_t BgL_g1015z00_794;
BgL_g1015z00_794 = 
CDR(BgL_lstz00_20); 
{ 
 long BgL_iz00_1110; obj_t BgL_lz00_1111;
BgL_iz00_1110 = 0L; 
BgL_lz00_1111 = BgL_g1015z00_794; 
BgL_loopz00_1109:
if(
NULLP(BgL_lz00_1111))
{ /* Llib/struct.scm 202 */
return BgL_structz00_793;}  else 
{ /* Llib/struct.scm 202 */
{ /* Llib/struct.scm 205 */
 obj_t BgL_arg1114z00_1113;
{ /* Llib/struct.scm 205 */
 obj_t BgL_pairz00_1114;
if(
PAIRP(BgL_lz00_1111))
{ /* Llib/struct.scm 205 */
BgL_pairz00_1114 = BgL_lz00_1111; }  else 
{ 
 obj_t BgL_auxz00_1477;
BgL_auxz00_1477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(8712L), BGl_string1521z00zz__structurez00, BGl_string1525z00zz__structurez00, BgL_lz00_1111); 
FAILURE(BgL_auxz00_1477,BFALSE,BFALSE);} 
BgL_arg1114z00_1113 = 
CAR(BgL_pairz00_1114); } 
{ /* Llib/struct.scm 162 */
 int BgL_tmpz00_1482;
BgL_tmpz00_1482 = 
(int)(BgL_iz00_1110); 
STRUCT_SET(BgL_structz00_793, BgL_tmpz00_1482, BgL_arg1114z00_1113); } } 
{ /* Llib/struct.scm 206 */
 long BgL_arg1115z00_1117; obj_t BgL_arg1122z00_1118;
BgL_arg1115z00_1117 = 
(BgL_iz00_1110+1L); 
{ /* Llib/struct.scm 206 */
 obj_t BgL_pairz00_1120;
if(
PAIRP(BgL_lz00_1111))
{ /* Llib/struct.scm 206 */
BgL_pairz00_1120 = BgL_lz00_1111; }  else 
{ 
 obj_t BgL_auxz00_1488;
BgL_auxz00_1488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(8743L), BGl_string1521z00zz__structurez00, BGl_string1525z00zz__structurez00, BgL_lz00_1111); 
FAILURE(BgL_auxz00_1488,BFALSE,BFALSE);} 
BgL_arg1122z00_1118 = 
CDR(BgL_pairz00_1120); } 
{ 
 obj_t BgL_lz00_1494; long BgL_iz00_1493;
BgL_iz00_1493 = BgL_arg1115z00_1117; 
BgL_lz00_1494 = BgL_arg1122z00_1118; 
BgL_lz00_1111 = BgL_lz00_1494; 
BgL_iz00_1110 = BgL_iz00_1493; 
goto BgL_loopz00_1109;} } } } } } } }  else 
{ /* Llib/struct.scm 196 */
 obj_t BgL_arg1126z00_806;
BgL_arg1126z00_806 = 
CAR(BgL_lstz00_20); 
{ /* Llib/struct.scm 196 */
 obj_t BgL_aux1503z00_1247;
BgL_aux1503z00_1247 = 
BGl_errorz00zz__errorz00(BGl_symbol1526z00zz__structurez00, BGl_string1527z00zz__structurez00, BgL_arg1126z00_806); 
if(
STRUCTP(BgL_aux1503z00_1247))
{ /* Llib/struct.scm 196 */
return BgL_aux1503z00_1247;}  else 
{ 
 obj_t BgL_auxz00_1499;
BgL_auxz00_1499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(8434L), BGl_string1523z00zz__structurez00, BGl_string1513z00zz__structurez00, BgL_aux1503z00_1247); 
FAILURE(BgL_auxz00_1499,BFALSE,BFALSE);} } } } } 

}



/* &list->struct */
obj_t BGl_z62listzd2ze3structz53zz__structurez00(obj_t BgL_envz00_1206, obj_t BgL_lstz00_1207)
{
{ /* Llib/struct.scm 191 */
{ /* Llib/struct.scm 195 */
 obj_t BgL_auxz00_1503;
if(
PAIRP(BgL_lstz00_1207))
{ /* Llib/struct.scm 195 */
BgL_auxz00_1503 = BgL_lstz00_1207
; }  else 
{ 
 obj_t BgL_auxz00_1506;
BgL_auxz00_1506 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1507z00zz__structurez00, 
BINT(8400L), BGl_string1528z00zz__structurez00, BGl_string1525z00zz__structurez00, BgL_lstz00_1207); 
FAILURE(BgL_auxz00_1506,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3structz31zz__structurez00(BgL_auxz00_1503);} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__structurez00(void)
{
{ /* Llib/struct.scm 18 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1529z00zz__structurez00));} 

}

#ifdef __cplusplus
}
#endif
