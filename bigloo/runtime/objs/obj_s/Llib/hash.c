/*===========================================================================*/
/*   (Llib/hash.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/hash.scm -indent -o objs/obj_s/Llib/hash.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___HASH_TYPE_DEFINITIONS
#define BGL___HASH_TYPE_DEFINITIONS

/* object type definitions */

#endif // BGL___HASH_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_plainzd2hashtablezd2getz00zz__hashz00(obj_t, obj_t);
extern long bgl_symbol_hash_number(obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2clearz12zc0zz__hashz00(obj_t);
extern obj_t BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_list4073z00zz__hashz00 = BUNSPEC;
extern long bgl_pointer_hashnumber(obj_t, long);
static obj_t BGl_z62openzd2stringzd2hashtablezd2addz12za2zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list4076z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_weakzd2hashtablezd2getz00zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_list3982z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(obj_t, obj_t);
static bool_t BGl_plainzd2hashtablezd2containszf3zf3zz__hashz00(obj_t, obj_t);
extern long bgl_symbol_hash_number_persistent(obj_t);
static obj_t BGl_z62openzd2stringzd2hashtablezd2putz12za2zz__hashz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
extern obj_t BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(obj_t);
static obj_t BGl_z62openzd2stringzd2hashtablezd2forzd2eachz62zz__hashz00(obj_t, obj_t, obj_t);
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_search1162ze70ze7zz__hashz00(long, obj_t, obj_t, long);
static obj_t BGl_requirezd2initializa7ationz75zz__hashz00 = BUNSPEC;
static long BGl_defaultzd2maxzd2bucketzd2lengthzd2zz__hashz00 = 0L;
static obj_t BGl_list3992z00zz__hashz00 = BUNSPEC;
static obj_t BGl_plainzd2hashtablezd2ze3listze3zz__hashz00(obj_t);
static obj_t BGl_plainzd2hashtablezd2updatez12z12zz__hashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list4091z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62hashtablezd2weakzd2keyszf3z91zz__hashz00(obj_t, obj_t);
extern obj_t BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
static bool_t BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00(obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__weakhashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62hashtablezd2containszf3z43zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2clearz12z12zz__hashz00(obj_t);
extern long bgl_string_hash_persistent(char *, int, int);
extern long bgl_keyword_hash_number(obj_t);
static obj_t BGl_z62hashtablezd2removez12za2zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62hashtablezd2ze3vectorz53zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t);
static obj_t BGl_z62hashtablezd2collisionszb0zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_stringzd2hashzd2numberz00zz__hashz00(obj_t);
static obj_t BGl_z62hashtablezd2getzb0zz__hashz00(obj_t, obj_t, obj_t);
extern obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t, long, long);
static obj_t BGl_z62hashtablezd2addz12za2zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00(obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__hashz00(void);
static obj_t BGl_openzd2stringzd2hashtablezd2ze3vectorz31zz__hashz00(obj_t);
extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
static obj_t BGl_z62hashtablezd2putz12za2zz__hashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62openzd2stringzd2hashtablezd2mapzb0zz__hashz00(obj_t, obj_t, obj_t);
extern long bgl_obj_hash_number(obj_t);
static obj_t BGl_symbol3903z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62hashtablezd2filterz12za2zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3905z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3907z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t create_struct(obj_t, int);
static obj_t BGl_symbol3909z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62getzd2pointerzd2hashnumberz62zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2collisionszd2zz__hashz00(obj_t);
static obj_t BGl_cnstzd2initzd2zz__hashz00(void);
static long BGl_objzd2hashze70z35zz__hashz00(obj_t);
static obj_t BGl_genericzd2initzd2zz__hashz00(void);
static bool_t BGl_hashtablezd2weakzf3z21zz__hashz00(obj_t);
static obj_t BGl_symbol4007z00zz__hashz00 = BUNSPEC;
extern long bgl_list_length(obj_t);
static obj_t BGl_symbol4009z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00(obj_t, obj_t);
static obj_t BGl_symbol3911z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62stringzd2hashzd2numberz62zz__hashz00(obj_t, obj_t);
static obj_t BGl_z62hashtablezd2clearz12za2zz__hashz00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__hashz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__hashz00(void);
static obj_t BGl_z62zc3z04anonymousza31618ze3ze5zz__hashz00(obj_t, obj_t);
static obj_t BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00(obj_t);
static obj_t BGl_objectzd2initzd2zz__hashz00(void);
static obj_t BGl_keyword3883z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_keyword3885z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
static obj_t BGl_keyword3887z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol4015z00zz__hashz00 = BUNSPEC;
static obj_t BGl_keyword3889z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
static obj_t BGl_z62stringzd2hashtablezd2getz62zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_hashtablezd2removez12zc0zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2ze3vectorz31zz__hashz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2keyzd2listz00zz__hashz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_openzd2stringzd2hashtablezd2keyzd2listz00zz__hashz00(obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2mapzd2zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2hashtablezd2putz12z12zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2mapz00zz__hashz00(obj_t, obj_t);
static obj_t BGl_keyword3891z00zz__hashz00 = BUNSPEC;
static obj_t BGl_keyword3893z00zz__hashz00 = BUNSPEC;
static obj_t BGl_keyword3894z00zz__hashz00 = BUNSPEC;
static obj_t BGl_keyword3896z00zz__hashz00 = BUNSPEC;
extern obj_t string_append_3(obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2hashzd2zz__hashz00(obj_t, obj_t);
extern obj_t BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_symbol3857z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62openzd2stringzd2hashtablezd2removez12za2zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2mapzd2zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_stringzd2hashtablezd2getz00zz__hashz00(obj_t, obj_t);
static obj_t BGl_symbol4031z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62getzd2hashnumberzb0zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2filterz12zc0zz__hashz00(obj_t, obj_t);
static obj_t BGl_symbol4037z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62hashtablezd2forzd2eachz62zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol4039z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4006z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_symbol3862z00zz__hashz00 = BUNSPEC;
extern long bgl_keyword_hash_number_persistent(obj_t);
static obj_t BGl_symbol3864z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t);
extern bool_t BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_symbol3866z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3948z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3869z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62hashtablezd2keyzd2listz62zz__hashz00(obj_t, obj_t);
extern long bgl_foreign_hash_number(obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_symbol4044z00zz__hashz00 = BUNSPEC;
extern obj_t make_vector(long, obj_t);
static obj_t BGl_z62hashtablezd2updatez12za2zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2addz12zc0zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_openzd2stringzd2hashtablezd2clearz12zc0zz__hashz00(obj_t);
static obj_t BGl_symbol4047z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4014z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3950z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(obj_t);
static obj_t BGl_symbol3952z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62openzd2stringzd2hashtablezd2filterz12za2zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31420ze3ze5zz__hashz00(obj_t, obj_t);
static obj_t BGl_symbol3958z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3879z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_hashtablezd2containszf3z21zz__hashz00(obj_t, obj_t);
static obj_t BGl_z62getzd2hashnumberzd2persistentz62zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z42openzd2stringzd2hashtablezd2getz90zz__hashz00(obj_t, char *);
static obj_t BGl_symbol4053z00zz__hashz00 = BUNSPEC;
static obj_t BGl_methodzd2initzd2zz__hashz00(void);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2addz12z12zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol4056z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3960z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_stringzd2hashzd2zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_getzd2pointerzd2hashnumberz00zz__hashz00(obj_t, long);
static bool_t BGl_plainzd2hashtablezd2removez12z12zz__hashz00(obj_t, obj_t);
static obj_t BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2ze3vectorze3zz__hashz00(obj_t);
static obj_t BGl_symbol3965z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_symbol3967z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62hashtablezf3z91zz__hashz00(obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2putz12z12zz__hashz00(obj_t, obj_t, obj_t);
static long BGl_defaultzd2hashtablezd2bucketzd2lengthzd2zz__hashz00 = 0L;
static obj_t BGl_z62stringzd2hashtablezd2putz12z70zz__hashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62hashtablezd2mapzb0zz__hashz00(obj_t, obj_t, obj_t);
extern obj_t BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(obj_t, obj_t);
static bool_t BGl_plainzd2hashtablezd2forzd2eachzd2zz__hashz00(obj_t, obj_t);
extern obj_t BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(obj_t);
static obj_t BGl_symbol4062z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4030z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol4065z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4036z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2keyzd2listzd2zz__hashz00(obj_t);
static obj_t BGl_symbol3974z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list3861z00zz__hashz00 = BUNSPEC;
extern long bgl_date_to_seconds(obj_t);
static obj_t BGl_symbol3976z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3898z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list3947z00zz__hashz00 = BUNSPEC;
static obj_t BGl_plainzd2hashtablezd2collisionsz00zz__hashz00(obj_t);
static obj_t BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(obj_t);
static obj_t BGl_z62makezd2hashtablezb0zz__hashz00(obj_t, obj_t);
extern obj_t obj_to_string(obj_t, obj_t);
static obj_t BGl_plainzd2hashtablezd2filterz12z12zz__hashz00(obj_t, obj_t);
static obj_t BGl_z62openzd2stringzd2hashtablezd2getzb0zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol4074z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62z42openzd2stringzd2hashtablezd2getzf2zz__hashz00(obj_t, obj_t, obj_t);
static obj_t BGl_list4043z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol4077z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4046z00zz__hashz00 = BUNSPEC;
extern long bgl_string_hash(char *, int, int);
static obj_t BGl_symbol3983z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62hashtablezd2weakzd2datazf3z91zz__hashz00(obj_t, obj_t);
static obj_t BGl_symbol3985z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list3957z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62hashtablezd2siza7ez17zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2updatez12zc0zz__hashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_openzd2stringzd2hashtablezd2siza7ezd2incz12zb5zz__hashz00(obj_t);
static obj_t BGl_z62openzd2stringzd2hashtablezd2containszf3z43zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_list4052z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol4088z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4055z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3993z00zz__hashz00 = BUNSPEC;
static obj_t BGl_symbol3995z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list3882z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list3964z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_openzd2stringzd2hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t);
BGL_EXPORTED_DECL long BGl_getzd2hashnumberzd2zz__hashz00(obj_t);
static obj_t BGl_symbol4092z00zz__hashz00 = BUNSPEC;
static obj_t BGl_z62openzd2stringzd2hashtablezd2updatez12za2zz__hashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol4094z00zz__hashz00 = BUNSPEC;
static obj_t BGl_list4061z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(obj_t);
static obj_t BGl_list4064z00zz__hashz00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol4098z00zz__hashz00 = BUNSPEC;
static obj_t BGl__createzd2hashtablezd2zz__hashz00(obj_t, obj_t);
static bool_t BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(obj_t);
static long BGl_tablezd2getzd2hashnumberz00zz__hashz00(obj_t, obj_t);
extern long BGl_objectzd2hashnumberzd2zz__objectz00(BgL_objectz00_bglt);
static obj_t BGl_list3973z00zz__hashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t);
extern obj_t BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(obj_t);
extern obj_t BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62hashtablezd2ze3listz53zz__hashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2updatez12zd2envz12zz__hashz00, BgL_bgl_za762hashtableza7d2u4106z00, BGl_z62hashtablezd2updatez12za2zz__hashz00, 0L, BUNSPEC, 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2filterz12zd2envz12zz__hashz00, BgL_bgl_za762openza7d2string4107z00, BGl_z62openzd2stringzd2hashtablezd2filterz12za2zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3900z00zz__hashz00, BgL_bgl_string3900za700za7za7_4108za7, "Illegal keyword argument", 24 );
DEFINE_STRING( BGl_string3901z00zz__hashz00, BgL_bgl_string3901za700za7za7_4109za7, "_create-hashtable", 17 );
DEFINE_STRING( BGl_string3902z00zz__hashz00, BgL_bgl_string3902za700za7za7_4110za7, "wrong number of arguments: [0..8] expected, provided", 52 );
DEFINE_STRING( BGl_string3904z00zz__hashz00, BgL_bgl_string3904za700za7za7_4111za7, "keys", 4 );
DEFINE_STRING( BGl_string3906z00zz__hashz00, BgL_bgl_string3906za700za7za7_4112za7, "data", 4 );
DEFINE_STRING( BGl_string3908z00zz__hashz00, BgL_bgl_string3908za700za7za7_4113za7, "both", 4 );
extern obj_t BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00;
DEFINE_STRING( BGl_string4000z00zz__hashz00, BgL_bgl_string4000za700za7za7_4114za7, "plain-hashtable-clear!", 22 );
DEFINE_STRING( BGl_string4001z00zz__hashz00, BgL_bgl_string4001za700za7za7_4115za7, "hashtable-contains?", 19 );
DEFINE_STRING( BGl_string4002z00zz__hashz00, BgL_bgl_string4002za700za7za7_4116za7, "&hashtable-contains?", 20 );
DEFINE_STRING( BGl_string4003z00zz__hashz00, BgL_bgl_string4003za700za7za7_4117za7, "open-string-hashtable-contains?", 31 );
DEFINE_STRING( BGl_string4004z00zz__hashz00, BgL_bgl_string4004za700za7za7_4118za7, "&open-string-hashtable-contains?", 32 );
DEFINE_STRING( BGl_string4005z00zz__hashz00, BgL_bgl_string4005za700za7za7_4119za7, "plain-hashtable-contains?", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2keyzd2listzd2envzd2zz__hashz00, BgL_bgl_za762hashtableza7d2k4120z00, BGl_z62hashtablezd2keyzd2listz62zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4008z00zz__hashz00, BgL_bgl_string4008za700za7za7_4121za7, "eqt", 3 );
DEFINE_STRING( BGl_string3910z00zz__hashz00, BgL_bgl_string3910za700za7za7_4122za7, "open-string", 11 );
DEFINE_STRING( BGl_string3912z00zz__hashz00, BgL_bgl_string3912za700za7za7_4123za7, "string", 6 );
DEFINE_STRING( BGl_string3913z00zz__hashz00, BgL_bgl_string3913za700za7za7_4124za7, "Persistent hashtable cannot use custom hash function", 52 );
DEFINE_STRING( BGl_string3914z00zz__hashz00, BgL_bgl_string3914za700za7za7_4125za7, "Cannot provide eqtest for string hashtable", 42 );
DEFINE_STRING( BGl_string3915z00zz__hashz00, BgL_bgl_string3915za700za7za7_4126za7, "Cannot provide hash for string hashtable", 40 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2siza7ezd2envza7zz__hashz00, BgL_bgl_za762hashtableza7d2s4127z00, BGl_z62hashtablezd2siza7ez17zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3917z00zz__hashz00, BgL_bgl_string3917za700za7za7_4128za7, "<@anonymous:1420>", 17 );
DEFINE_STRING( BGl_string3918z00zz__hashz00, BgL_bgl_string3918za700za7za7_4129za7, "bstring", 7 );
DEFINE_STRING( BGl_string3919z00zz__hashz00, BgL_bgl_string3919za700za7za7_4130za7, "hashtable?", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hashtablezd2putz12zd2envzc0zz__hashz00, BgL_bgl_za762stringza7d2hash4131z00, BGl_z62stringzd2hashtablezd2putz12z70zz__hashz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2mapzd2envz00zz__hashz00, BgL_bgl_za762hashtableza7d2m4132z00, BGl_z62hashtablezd2mapzb0zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2getzd2envz00zz__hashz00, BgL_bgl_za762hashtableza7d2g4133z00, BGl_z62hashtablezd2getzb0zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2clearz12zd2envz12zz__hashz00, BgL_bgl_za762hashtableza7d2c4134z00, BGl_z62hashtablezd2clearz12za2zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4010z00zz__hashz00, BgL_bgl_string4010za700za7za7_4135za7, "arg1652", 7 );
DEFINE_STRING( BGl_string4011z00zz__hashz00, BgL_bgl_string4011za700za7za7_4136za7, "hashtable-get", 13 );
DEFINE_STRING( BGl_string4012z00zz__hashz00, BgL_bgl_string4012za700za7za7_4137za7, "&hashtable-get", 14 );
DEFINE_STRING( BGl_string4013z00zz__hashz00, BgL_bgl_string4013za700za7za7_4138za7, "plain-hashtable-get", 19 );
DEFINE_STRING( BGl_string4016z00zz__hashz00, BgL_bgl_string4016za700za7za7_4139za7, "arg1664", 7 );
DEFINE_STRING( BGl_string4017z00zz__hashz00, BgL_bgl_string4017za700za7za7_4140za7, "string-hashtable-get", 20 );
DEFINE_STRING( BGl_string4018z00zz__hashz00, BgL_bgl_string4018za700za7za7_4141za7, "&string-hashtable-get", 21 );
DEFINE_STRING( BGl_string4019z00zz__hashz00, BgL_bgl_string4019za700za7za7_4142za7, "open-string-hashtable-get", 25 );
DEFINE_STRING( BGl_string3920z00zz__hashz00, BgL_bgl_string3920za700za7za7_4143za7, "hashtable-weak?", 15 );
DEFINE_STRING( BGl_string3921z00zz__hashz00, BgL_bgl_string3921za700za7za7_4144za7, "hashtable-open-string?", 22 );
DEFINE_STRING( BGl_string3922z00zz__hashz00, BgL_bgl_string3922za700za7za7_4145za7, "hashtable-weak-keys?", 20 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3916z00zz__hashz00, BgL_bgl_za762za7c3za704anonymo4146za7, BGl_z62zc3z04anonymousza31420ze3ze5zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3923z00zz__hashz00, BgL_bgl_string3923za700za7za7_4147za7, "&hashtable-weak-keys?", 21 );
DEFINE_STRING( BGl_string3924z00zz__hashz00, BgL_bgl_string3924za700za7za7_4148za7, "struct", 6 );
extern obj_t BGl_stringzd3zf3zd2envzf2zz__r4_strings_6_7z00;
DEFINE_STRING( BGl_string3925z00zz__hashz00, BgL_bgl_string3925za700za7za7_4149za7, "hashtable-weak-data?", 20 );
DEFINE_STRING( BGl_string3926z00zz__hashz00, BgL_bgl_string3926za700za7za7_4150za7, "&hashtable-weak-data?", 21 );
DEFINE_STRING( BGl_string3927z00zz__hashz00, BgL_bgl_string3927za700za7za7_4151za7, "hashtable-size", 14 );
DEFINE_STRING( BGl_string3928z00zz__hashz00, BgL_bgl_string3928za700za7za7_4152za7, "&hashtable-size", 15 );
DEFINE_STRING( BGl_string3929z00zz__hashz00, BgL_bgl_string3929za700za7za7_4153za7, "&hashtable->vector", 18 );
DEFINE_STRING( BGl_string4100z00zz__hashz00, BgL_bgl_string4100za700za7za7_4154za7, "wrong number of arguments: [1..3] expected, provided", 52 );
DEFINE_STRING( BGl_string4101z00zz__hashz00, BgL_bgl_string4101za700za7za7_4155za7, "_string-hash", 12 );
DEFINE_STRING( BGl_string4020z00zz__hashz00, BgL_bgl_string4020za700za7za7_4156za7, "&open-string-hashtable-get", 26 );
DEFINE_STRING( BGl_string4102z00zz__hashz00, BgL_bgl_string4102za700za7za7_4157za7, "&string-hash-number", 19 );
DEFINE_STRING( BGl_string4021z00zz__hashz00, BgL_bgl_string4021za700za7za7_4158za7, "$open-string-hashtable-get", 26 );
DEFINE_STRING( BGl_string4103z00zz__hashz00, BgL_bgl_string4103za700za7za7_4159za7, "open-string-hashtable-rehash!", 29 );
DEFINE_STRING( BGl_string4022z00zz__hashz00, BgL_bgl_string4022za700za7za7_4160za7, "&$open-string-hashtable-get", 27 );
DEFINE_STRING( BGl_string4104z00zz__hashz00, BgL_bgl_string4104za700za7za7_4161za7, "open-string-hashtable-size-inc!", 31 );
DEFINE_STRING( BGl_string4023z00zz__hashz00, BgL_bgl_string4023za700za7za7_4162za7, "hashtable-put!", 14 );
DEFINE_STRING( BGl_string4105z00zz__hashz00, BgL_bgl_string4105za700za7za7_4163za7, "__hash", 6 );
DEFINE_STRING( BGl_string4024z00zz__hashz00, BgL_bgl_string4024za700za7za7_4164za7, "&hashtable-put!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezf3zd2envz21zz__hashz00, BgL_bgl_za762hashtableza7f3za74165za7, BGl_z62hashtablezf3z91zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4025z00zz__hashz00, BgL_bgl_string4025za700za7za7_4166za7, "&open-string-hashtable-put!", 27 );
DEFINE_STRING( BGl_string4026z00zz__hashz00, BgL_bgl_string4026za700za7za7_4167za7, "open-string-hashtable-put/hash!", 31 );
DEFINE_STRING( BGl_string4027z00zz__hashz00, BgL_bgl_string4027za700za7za7_4168za7, "string-hashtable-put!", 21 );
DEFINE_STRING( BGl_string4028z00zz__hashz00, BgL_bgl_string4028za700za7za7_4169za7, "&string-hashtable-put!", 22 );
DEFINE_STRING( BGl_string4029z00zz__hashz00, BgL_bgl_string4029za700za7za7_4170za7, "plain-hashtable-put!", 20 );
DEFINE_STRING( BGl_string3930z00zz__hashz00, BgL_bgl_string3930za700za7za7_4171za7, "open-string-hashtable->vector", 29 );
DEFINE_STRING( BGl_string3931z00zz__hashz00, BgL_bgl_string3931za700za7za7_4172za7, "loop", 4 );
DEFINE_STRING( BGl_string3932z00zz__hashz00, BgL_bgl_string3932za700za7za7_4173za7, "vector", 6 );
DEFINE_STRING( BGl_string3933z00zz__hashz00, BgL_bgl_string3933za700za7za7_4174za7, "vector-set!", 11 );
DEFINE_STRING( BGl_string3934z00zz__hashz00, BgL_bgl_string3934za700za7za7_4175za7, "plain-hashtable->vector", 23 );
DEFINE_STRING( BGl_string3935z00zz__hashz00, BgL_bgl_string3935za700za7za7_4176za7, "liip", 4 );
DEFINE_STRING( BGl_string3854z00zz__hashz00, BgL_bgl_string3854za700za7za7_4177za7, "/tmp/bigloo/runtime/Llib/hash.sch", 33 );
DEFINE_STRING( BGl_string3936z00zz__hashz00, BgL_bgl_string3936za700za7za7_4178za7, "pair", 4 );
DEFINE_STRING( BGl_string3855z00zz__hashz00, BgL_bgl_string3855za700za7za7_4179za7, "table-get-hashnumber", 20 );
DEFINE_STRING( BGl_string3937z00zz__hashz00, BgL_bgl_string3937za700za7za7_4180za7, "&hashtable->list", 16 );
DEFINE_STRING( BGl_string3856z00zz__hashz00, BgL_bgl_string3856za700za7za7_4181za7, "symbol", 6 );
DEFINE_STRING( BGl_string3938z00zz__hashz00, BgL_bgl_string3938za700za7za7_4182za7, "open-string-hashtable->list", 27 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2collisionszd2envz00zz__hashz00, BgL_bgl_za762hashtableza7d2c4183z00, BGl_z62hashtablezd2collisionszb0zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3939z00zz__hashz00, BgL_bgl_string3939za700za7za7_4184za7, "plain-hashtable->list", 21 );
DEFINE_STRING( BGl_string3858z00zz__hashz00, BgL_bgl_string3858za700za7za7_4185za7, "%hashtable", 10 );
DEFINE_STRING( BGl_string3859z00zz__hashz00, BgL_bgl_string3859za700za7za7_4186za7, "struct-ref:not an instance of", 29 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2containszf3zd2envzf3zz__hashz00, BgL_bgl_za762openza7d2string4187z00, BGl_z62openzd2stringzd2hashtablezd2containszf3z43zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2addz12zd2envz12zz__hashz00, BgL_bgl_za762hashtableza7d2a4188z00, BGl_z62hashtablezd2addz12za2zz__hashz00, 0L, BUNSPEC, 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2addz12zd2envz12zz__hashz00, BgL_bgl_za762openza7d2string4189z00, BGl_z62openzd2stringzd2hashtablezd2addz12za2zz__hashz00, 0L, BUNSPEC, 5 );
DEFINE_REAL( BGl_real3878z00zz__hashz00, BgL_bgl_real3878za700za7za7__h4190za7, 1.2 );
DEFINE_STRING( BGl_string4032z00zz__hashz00, BgL_bgl_string4032za700za7za7_4191za7, "arg1795", 7 );
DEFINE_STRING( BGl_string4033z00zz__hashz00, BgL_bgl_string4033za700za7za7_4192za7, "hashtable-update!", 17 );
DEFINE_STRING( BGl_string4034z00zz__hashz00, BgL_bgl_string4034za700za7za7_4193za7, "&hashtable-update!", 18 );
DEFINE_STRING( BGl_string4035z00zz__hashz00, BgL_bgl_string4035za700za7za7_4194za7, "open-string-hashtable-update!", 29 );
DEFINE_STRING( BGl_string4038z00zz__hashz00, BgL_bgl_string4038za700za7za7_4195za7, "proc", 4 );
DEFINE_STRING( BGl_string3940z00zz__hashz00, BgL_bgl_string3940za700za7za7_4196za7, "&hashtable-key-list", 19 );
DEFINE_STRING( BGl_string3941z00zz__hashz00, BgL_bgl_string3941za700za7za7_4197za7, "open-string-hashtable-key-list", 30 );
DEFINE_STRING( BGl_string3860z00zz__hashz00, BgL_bgl_string3860za700za7za7_4198za7, "table-get-hashnumber:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string3942z00zz__hashz00, BgL_bgl_string3942za700za7za7_4199za7, "plain-hashtable-key-list", 24 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2putz12zd2envz12zz__hashz00, BgL_bgl_za762hashtableza7d2p4200z00, BGl_z62hashtablezd2putz12za2zz__hashz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3943z00zz__hashz00, BgL_bgl_string3943za700za7za7_4201za7, "&hashtable-map", 14 );
DEFINE_STRING( BGl_string3944z00zz__hashz00, BgL_bgl_string3944za700za7za7_4202za7, "procedure", 9 );
DEFINE_STRING( BGl_string3863z00zz__hashz00, BgL_bgl_string3863za700za7za7_4203za7, "funcall", 7 );
DEFINE_STRING( BGl_string3945z00zz__hashz00, BgL_bgl_string3945za700za7za7_4204za7, "open-string-hashtable-map", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2putz12zd2envz12zz__hashz00, BgL_bgl_za762openza7d2string4205z00, BGl_z62openzd2stringzd2hashtablezd2putz12za2zz__hashz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3946z00zz__hashz00, BgL_bgl_string3946za700za7za7_4206za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string3865z00zz__hashz00, BgL_bgl_string3865za700za7za7_4207za7, "hashn", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2containszf3zd2envzf3zz__hashz00, BgL_bgl_za762hashtableza7d2c4208z00, BGl_z62hashtablezd2containszf3z43zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3867z00zz__hashz00, BgL_bgl_string3867za700za7za7_4209za7, "key", 3 );
DEFINE_STRING( BGl_string3949z00zz__hashz00, BgL_bgl_string3949za700za7za7_4210za7, "fun", 3 );
DEFINE_STRING( BGl_string3868z00zz__hashz00, BgL_bgl_string3868za700za7za7_4211za7, "bint", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2hashnumberzd2envz00zz__hashz00, BgL_bgl_za762getza7d2hashnum4212z00, BGl_z62getzd2hashnumberzb0zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2removez12zd2envz12zz__hashz00, BgL_bgl_za762hashtableza7d2r4213z00, BGl_z62hashtablezd2removez12za2zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4040z00zz__hashz00, BgL_bgl_string4040za700za7za7_4214za7, "oval", 4 );
DEFINE_STRING( BGl_string4041z00zz__hashz00, BgL_bgl_string4041za700za7za7_4215za7, "&open-string-hashtable-update!", 30 );
DEFINE_STRING( BGl_string4042z00zz__hashz00, BgL_bgl_string4042za700za7za7_4216za7, "plain-hashtable-update!", 23 );
DEFINE_STRING( BGl_string4045z00zz__hashz00, BgL_bgl_string4045za700za7za7_4217za7, "arg1837", 7 );
DEFINE_STRING( BGl_string4048z00zz__hashz00, BgL_bgl_string4048za700za7za7_4218za7, "arg1834", 7 );
DEFINE_STRING( BGl_string4049z00zz__hashz00, BgL_bgl_string4049za700za7za7_4219za7, "hashtable-add!", 14 );
DEFINE_STRING( BGl_string3951z00zz__hashz00, BgL_bgl_string3951za700za7za7_4220za7, "arg1543", 7 );
DEFINE_STRING( BGl_string3870z00zz__hashz00, BgL_bgl_string3870za700za7za7_4221za7, "persistent", 10 );
DEFINE_STRING( BGl_string3871z00zz__hashz00, BgL_bgl_string3871za700za7za7_4222za7, "make-hashtable", 14 );
DEFINE_STRING( BGl_string3953z00zz__hashz00, BgL_bgl_string3953za700za7za7_4223za7, "arg1544", 7 );
DEFINE_STRING( BGl_string3872z00zz__hashz00, BgL_bgl_string3872za700za7za7_4224za7, "Illegal default bucket length", 29 );
DEFINE_STRING( BGl_string3954z00zz__hashz00, BgL_bgl_string3954za700za7za7_4225za7, "&open-string-hashtable-map", 26 );
DEFINE_STRING( BGl_string3873z00zz__hashz00, BgL_bgl_string3873za700za7za7_4226za7, "Illegal max bucket length", 25 );
DEFINE_STRING( BGl_string3955z00zz__hashz00, BgL_bgl_string3955za700za7za7_4227za7, "plain-hashtable-map", 19 );
DEFINE_STRING( BGl_string3874z00zz__hashz00, BgL_bgl_string3874za700za7za7_4228za7, "Illegal equality test", 21 );
DEFINE_STRING( BGl_string3956z00zz__hashz00, BgL_bgl_string3956za700za7za7_4229za7, "liip:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string3875z00zz__hashz00, BgL_bgl_string3875za700za7za7_4230za7, "Illegal hashnumber function", 27 );
DEFINE_STRING( BGl_string3876z00zz__hashz00, BgL_bgl_string3876za700za7za7_4231za7, "/tmp/bigloo/runtime/Llib/hash.scm", 33 );
DEFINE_STRING( BGl_string3877z00zz__hashz00, BgL_bgl_string3877za700za7za7_4232za7, "struct-set!:not an instance of", 30 );
DEFINE_STRING( BGl_string3959z00zz__hashz00, BgL_bgl_string3959za700za7za7_4233za7, "arg1558", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2weakzd2keyszf3zd2envz21zz__hashz00, BgL_bgl_za762hashtableza7d2w4234z00, BGl_z62hashtablezd2weakzd2keyszf3z91zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4050z00zz__hashz00, BgL_bgl_string4050za700za7za7_4235za7, "&hashtable-add!", 15 );
DEFINE_STRING( BGl_string4051z00zz__hashz00, BgL_bgl_string4051za700za7za7_4236za7, "open-string-hashtable-add!", 26 );
DEFINE_STRING( BGl_string4054z00zz__hashz00, BgL_bgl_string4054za700za7za7_4237za7, "init", 4 );
DEFINE_STRING( BGl_string4057z00zz__hashz00, BgL_bgl_string4057za700za7za7_4238za7, "obj", 3 );
DEFINE_STRING( BGl_string4058z00zz__hashz00, BgL_bgl_string4058za700za7za7_4239za7, "&open-string-hashtable-add!", 27 );
DEFINE_STRING( BGl_string4059z00zz__hashz00, BgL_bgl_string4059za700za7za7_4240za7, "plain-hashtable-add!", 20 );
DEFINE_STRING( BGl_string3961z00zz__hashz00, BgL_bgl_string3961za700za7za7_4241za7, "arg1559", 7 );
DEFINE_STRING( BGl_string3880z00zz__hashz00, BgL_bgl_string3880za700za7za7_4242za7, "none", 4 );
DEFINE_STRING( BGl_string3962z00zz__hashz00, BgL_bgl_string3962za700za7za7_4243za7, "&hashtable-for-each", 19 );
DEFINE_STRING( BGl_string3881z00zz__hashz00, BgL_bgl_string3881za700za7za7_4244za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string3963z00zz__hashz00, BgL_bgl_string3963za700za7za7_4245za7, "open-string-hashtable-for-each", 30 );
DEFINE_STRING( BGl_string3884z00zz__hashz00, BgL_bgl_string3884za700za7za7_4246za7, "bucket-expansion", 16 );
DEFINE_STRING( BGl_string3966z00zz__hashz00, BgL_bgl_string3966za700za7za7_4247za7, "arg1567", 7 );
DEFINE_STRING( BGl_string3886z00zz__hashz00, BgL_bgl_string3886za700za7za7_4248za7, "eqtest", 6 );
DEFINE_STRING( BGl_string3968z00zz__hashz00, BgL_bgl_string3968za700za7za7_4249za7, "arg1571", 7 );
DEFINE_STRING( BGl_string3969z00zz__hashz00, BgL_bgl_string3969za700za7za7_4250za7, "&open-string-hashtable-for-each", 31 );
DEFINE_STRING( BGl_string3888z00zz__hashz00, BgL_bgl_string3888za700za7za7_4251za7, "hash", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2filterz12zd2envz12zz__hashz00, BgL_bgl_za762hashtableza7d2f4252z00, BGl_z62hashtablezd2filterz12za2zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4060z00zz__hashz00, BgL_bgl_string4060za700za7za7_4253za7, "plain-hashtable-add!:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string4063z00zz__hashz00, BgL_bgl_string4063za700za7za7_4254za7, "arg1885", 7 );
DEFINE_STRING( BGl_string4066z00zz__hashz00, BgL_bgl_string4066za700za7za7_4255za7, "arg1882", 7 );
DEFINE_STRING( BGl_string4067z00zz__hashz00, BgL_bgl_string4067za700za7za7_4256za7, "hashtable-remove!", 17 );
DEFINE_STRING( BGl_string4068z00zz__hashz00, BgL_bgl_string4068za700za7za7_4257za7, "&hashtable-remove!", 18 );
DEFINE_STRING( BGl_string4069z00zz__hashz00, BgL_bgl_string4069za700za7za7_4258za7, "open-string-hashtable-remove!", 29 );
DEFINE_STRING( BGl_string3970z00zz__hashz00, BgL_bgl_string3970za700za7za7_4259za7, "plain-hashtable-for-each", 24 );
DEFINE_STRING( BGl_string3971z00zz__hashz00, BgL_bgl_string3971za700za7za7_4260za7, "<@anonymous:1579>", 17 );
DEFINE_STRING( BGl_string3890z00zz__hashz00, BgL_bgl_string3890za700za7za7_4261za7, "max-bucket-length", 17 );
DEFINE_STRING( BGl_string3972z00zz__hashz00, BgL_bgl_string3972za700za7za7_4262za7, "<@anonymous:1579>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3892z00zz__hashz00, BgL_bgl_string3892za700za7za7_4263za7, "max-length", 10 );
DEFINE_STRING( BGl_string3975z00zz__hashz00, BgL_bgl_string3975za700za7za7_4264za7, "arg1582", 7 );
DEFINE_STRING( BGl_string3895z00zz__hashz00, BgL_bgl_string3895za700za7za7_4265za7, "size", 4 );
DEFINE_STRING( BGl_string3977z00zz__hashz00, BgL_bgl_string3977za700za7za7_4266za7, "arg1583", 7 );
DEFINE_STRING( BGl_string3978z00zz__hashz00, BgL_bgl_string3978za700za7za7_4267za7, "for-each", 8 );
DEFINE_STRING( BGl_string3897z00zz__hashz00, BgL_bgl_string3897za700za7za7_4268za7, "weak", 4 );
DEFINE_STRING( BGl_string3979z00zz__hashz00, BgL_bgl_string3979za700za7za7_4269za7, "list", 4 );
DEFINE_STRING( BGl_string3899z00zz__hashz00, BgL_bgl_string3899za700za7za7_4270za7, "create-hashtable", 16 );
DEFINE_STRING( BGl_string4070z00zz__hashz00, BgL_bgl_string4070za700za7za7_4271za7, "&open-string-hashtable-remove!", 30 );
DEFINE_STRING( BGl_string4071z00zz__hashz00, BgL_bgl_string4071za700za7za7_4272za7, "plain-hashtable-remove!", 23 );
DEFINE_STRING( BGl_string4072z00zz__hashz00, BgL_bgl_string4072za700za7za7_4273za7, "plain-hashtable-remove!:Wrong number of arguments", 49 );
DEFINE_STRING( BGl_string4075z00zz__hashz00, BgL_bgl_string4075za700za7za7_4274za7, "arg1925", 7 );
DEFINE_STRING( BGl_string4078z00zz__hashz00, BgL_bgl_string4078za700za7za7_4275za7, "arg1924", 7 );
DEFINE_STRING( BGl_string4079z00zz__hashz00, BgL_bgl_string4079za700za7za7_4276za7, "plain-hashtable-expand!", 23 );
DEFINE_STRING( BGl_string3980z00zz__hashz00, BgL_bgl_string3980za700za7za7_4277za7, "&hashtable-filter!", 18 );
DEFINE_STRING( BGl_string3981z00zz__hashz00, BgL_bgl_string3981za700za7za7_4278za7, "open-string-hashtable-filter!", 29 );
DEFINE_STRING( BGl_string3984z00zz__hashz00, BgL_bgl_string3984za700za7za7_4279za7, "arg1606", 7 );
DEFINE_STRING( BGl_string3986z00zz__hashz00, BgL_bgl_string3986za700za7za7_4280za7, "arg1607", 7 );
DEFINE_STRING( BGl_string3987z00zz__hashz00, BgL_bgl_string3987za700za7za7_4281za7, "&open-string-hashtable-filter!", 30 );
DEFINE_STRING( BGl_string3988z00zz__hashz00, BgL_bgl_string3988za700za7za7_4282za7, "plain-hashtable-filter!", 23 );
DEFINE_STRING( BGl_string3989z00zz__hashz00, BgL_bgl_string3989za700za7za7_4283za7, "pair-nil", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2updatez12zd2envz12zz__hashz00, BgL_bgl_za762openza7d2string4284z00, BGl_z62openzd2stringzd2hashtablezd2updatez12za2zz__hashz00, 0L, BUNSPEC, 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z42openzd2stringzd2hashtablezd2getzd2envz42zz__hashz00, BgL_bgl_za762za742openza7d2str4285za7, BGl_z62z42openzd2stringzd2hashtablezd2getzf2zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2forzd2eachzd2envzd2zz__hashz00, BgL_bgl_za762openza7d2string4286z00, BGl_z62openzd2stringzd2hashtablezd2forzd2eachz62zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2weakzd2datazf3zd2envz21zz__hashz00, BgL_bgl_za762hashtableza7d2w4287z00, BGl_z62hashtablezd2weakzd2datazf3z91zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hashzd2envz00zz__hashz00, BgL_bgl__stringza7d2hashza7d4288z00, opt_generic_entry, BGl__stringzd2hashzd2zz__hashz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2pointerzd2hashnumberzd2envzd2zz__hashz00, BgL_bgl_za762getza7d2pointer4289z00, BGl_z62getzd2pointerzd2hashnumberz62zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string4080z00zz__hashz00, BgL_bgl_string4080za700za7za7_4290za7, "<@anonymous:1935>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_createzd2hashtablezd2envz00zz__hashz00, BgL_bgl__createza7d2hashta4291za7, opt_generic_entry, BGl__createzd2hashtablezd2zz__hashz00, BFALSE, -1 );
DEFINE_STRING( BGl_string4081z00zz__hashz00, BgL_bgl_string4081za700za7za7_4292za7, "Hashtable too large (new-len=~a/~a, size=~a)", 44 );
DEFINE_STRING( BGl_string4082z00zz__hashz00, BgL_bgl_string4082za700za7za7_4293za7, "&hashtable-collisions", 21 );
DEFINE_STRING( BGl_string4083z00zz__hashz00, BgL_bgl_string4083za700za7za7_4294za7, "plain-hashtable-collisions", 26 );
DEFINE_STRING( BGl_string4084z00zz__hashz00, BgL_bgl_string4084za700za7za7_4295za7, "get-hashnumber", 14 );
DEFINE_STRING( BGl_string4085z00zz__hashz00, BgL_bgl_string4085za700za7za7_4296za7, "real", 4 );
DEFINE_STRING( BGl_string4086z00zz__hashz00, BgL_bgl_string4086za700za7za7_4297za7, "index out of range [0..", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2ze3listzd2envze3zz__hashz00, BgL_bgl_za762hashtableza7d2za74298za7, BGl_z62hashtablezd2ze3listz53zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string4087z00zz__hashz00, BgL_bgl_string4087za700za7za7_4299za7, "]", 1 );
DEFINE_STRING( BGl_string4089z00zz__hashz00, BgL_bgl_string4089za700za7za7_4300za7, "ucs2-string-ref", 15 );
DEFINE_STRING( BGl_string3990z00zz__hashz00, BgL_bgl_string3990za700za7za7_4301za7, "<@anonymous:1618>", 17 );
DEFINE_STRING( BGl_string3991z00zz__hashz00, BgL_bgl_string3991za700za7za7_4302za7, "<@anonymous:1618>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3994z00zz__hashz00, BgL_bgl_string3994za700za7za7_4303za7, "arg1619", 7 );
DEFINE_STRING( BGl_string3996z00zz__hashz00, BgL_bgl_string3996za700za7za7_4304za7, "arg1620", 7 );
DEFINE_STRING( BGl_string3997z00zz__hashz00, BgL_bgl_string3997za700za7za7_4305za7, "&hashtable-clear!", 17 );
DEFINE_STRING( BGl_string3998z00zz__hashz00, BgL_bgl_string3998za700za7za7_4306za7, "open-string-hashtable-clear!", 28 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hashzd2numberzd2envzd2zz__hashz00, BgL_bgl_za762stringza7d2hash4307z00, BGl_z62stringzd2hashzd2numberz62zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3999z00zz__hashz00, BgL_bgl_string3999za700za7za7_4308za7, "/tmp/bigloo/runtime/Ieee/vector.scm", 35 );
DEFINE_STRING( BGl_string4090z00zz__hashz00, BgL_bgl_string4090za700za7za7_4309za7, "bucs2", 5 );
DEFINE_STRING( BGl_string4093z00zz__hashz00, BgL_bgl_string4093za700za7za7_4310za7, "get", 3 );
DEFINE_STRING( BGl_string4095z00zz__hashz00, BgL_bgl_string4095za700za7za7_4311za7, "i", 1 );
DEFINE_STRING( BGl_string4096z00zz__hashz00, BgL_bgl_string4096za700za7za7_4312za7, "obj-hash~0", 10 );
DEFINE_STRING( BGl_string4097z00zz__hashz00, BgL_bgl_string4097za700za7za7_4313za7, "&get-pointer-hashnumber", 23 );
DEFINE_STRING( BGl_string4099z00zz__hashz00, BgL_bgl_string4099za700za7za7_4314za7, "string-hash", 11 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2hashtablezd2getzd2envzd2zz__hashz00, BgL_bgl_za762stringza7d2hash4315z00, BGl_z62stringzd2hashtablezd2getz62zz__hashz00, 0L, BUNSPEC, 2 );
extern obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2hashtablezd2envz00zz__hashz00, BgL_bgl_za762makeza7d2hashta4316z00, va_generic_entry, BGl_z62makezd2hashtablezb0zz__hashz00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2hashnumberzd2persistentzd2envzd2zz__hashz00, BgL_bgl_za762getza7d2hashnum4317z00, BGl_z62getzd2hashnumberzd2persistentz62zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2forzd2eachzd2envzd2zz__hashz00, BgL_bgl_za762hashtableza7d2f4318z00, BGl_z62hashtablezd2forzd2eachz62zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hashtablezd2ze3vectorzd2envze3zz__hashz00, BgL_bgl_za762hashtableza7d2za74319za7, BGl_z62hashtablezd2ze3vectorz53zz__hashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2removez12zd2envz12zz__hashz00, BgL_bgl_za762openza7d2string4320z00, BGl_z62openzd2stringzd2hashtablezd2removez12za2zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2mapzd2envz00zz__hashz00, BgL_bgl_za762openza7d2string4321z00, BGl_z62openzd2stringzd2hashtablezd2mapzb0zz__hashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2stringzd2hashtablezd2getzd2envz00zz__hashz00, BgL_bgl_za762openza7d2string4322z00, BGl_z62openzd2stringzd2hashtablezd2getzb0zz__hashz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_list4073z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4076z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3982z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3992z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4091z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3903z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3905z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3907z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3909z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4007z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4009z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3911z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3883z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3885z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3887z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4015z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3889z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3891z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3893z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3894z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_keyword3896z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3857z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4031z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4037z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4039z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4006z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3862z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3864z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3866z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3948z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3869z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4044z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4047z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4014z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3950z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3952z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3958z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3879z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4053z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4056z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3960z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3965z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3967z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4062z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4030z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4065z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4036z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3974z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3861z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3976z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3898z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3947z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4074z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4043z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4077z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4046z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3983z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3985z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3957z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4052z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4088z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4055z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3993z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol3995z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3882z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3964z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4092z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4094z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4061z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list4064z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_symbol4098z00zz__hashz00) );
ADD_ROOT( (void *)(&BGl_list3973z00zz__hashz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long BgL_checksumz00_6809, char * BgL_fromz00_6810)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__hashz00))
{ 
BGl_requirezd2initializa7ationz75zz__hashz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__hashz00(); 
BGl_cnstzd2initzd2zz__hashz00(); 
BGl_importedzd2moduleszd2initz00zz__hashz00(); 
return 
BGl_toplevelzd2initzd2zz__hashz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
BGl_symbol3857z00zz__hashz00 = 
bstring_to_symbol(BGl_string3858z00zz__hashz00); 
BGl_symbol3862z00zz__hashz00 = 
bstring_to_symbol(BGl_string3863z00zz__hashz00); 
BGl_symbol3864z00zz__hashz00 = 
bstring_to_symbol(BGl_string3865z00zz__hashz00); 
BGl_symbol3866z00zz__hashz00 = 
bstring_to_symbol(BGl_string3867z00zz__hashz00); 
BGl_list3861z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3864z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3864z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL)))); 
BGl_symbol3869z00zz__hashz00 = 
bstring_to_symbol(BGl_string3870z00zz__hashz00); 
BGl_symbol3879z00zz__hashz00 = 
bstring_to_symbol(BGl_string3880z00zz__hashz00); 
BGl_keyword3883z00zz__hashz00 = 
bstring_to_keyword(BGl_string3884z00zz__hashz00); 
BGl_keyword3885z00zz__hashz00 = 
bstring_to_keyword(BGl_string3886z00zz__hashz00); 
BGl_keyword3887z00zz__hashz00 = 
bstring_to_keyword(BGl_string3888z00zz__hashz00); 
BGl_keyword3889z00zz__hashz00 = 
bstring_to_keyword(BGl_string3890z00zz__hashz00); 
BGl_keyword3891z00zz__hashz00 = 
bstring_to_keyword(BGl_string3892z00zz__hashz00); 
BGl_keyword3893z00zz__hashz00 = 
bstring_to_keyword(BGl_string3870z00zz__hashz00); 
BGl_keyword3894z00zz__hashz00 = 
bstring_to_keyword(BGl_string3895z00zz__hashz00); 
BGl_keyword3896z00zz__hashz00 = 
bstring_to_keyword(BGl_string3897z00zz__hashz00); 
BGl_list3882z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_keyword3883z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3885z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3887z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3889z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3891z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3893z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3894z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_keyword3896z00zz__hashz00, BNIL)))))))); 
BGl_symbol3898z00zz__hashz00 = 
bstring_to_symbol(BGl_string3899z00zz__hashz00); 
BGl_symbol3903z00zz__hashz00 = 
bstring_to_symbol(BGl_string3904z00zz__hashz00); 
BGl_symbol3905z00zz__hashz00 = 
bstring_to_symbol(BGl_string3906z00zz__hashz00); 
BGl_symbol3907z00zz__hashz00 = 
bstring_to_symbol(BGl_string3908z00zz__hashz00); 
BGl_symbol3909z00zz__hashz00 = 
bstring_to_symbol(BGl_string3910z00zz__hashz00); 
BGl_symbol3911z00zz__hashz00 = 
bstring_to_symbol(BGl_string3912z00zz__hashz00); 
BGl_symbol3948z00zz__hashz00 = 
bstring_to_symbol(BGl_string3949z00zz__hashz00); 
BGl_symbol3950z00zz__hashz00 = 
bstring_to_symbol(BGl_string3951z00zz__hashz00); 
BGl_symbol3952z00zz__hashz00 = 
bstring_to_symbol(BGl_string3953z00zz__hashz00); 
BGl_list3947z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3950z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3952z00zz__hashz00, BNIL))))); 
BGl_symbol3958z00zz__hashz00 = 
bstring_to_symbol(BGl_string3959z00zz__hashz00); 
BGl_symbol3960z00zz__hashz00 = 
bstring_to_symbol(BGl_string3961z00zz__hashz00); 
BGl_list3957z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3958z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3960z00zz__hashz00, BNIL))))); 
BGl_symbol3965z00zz__hashz00 = 
bstring_to_symbol(BGl_string3966z00zz__hashz00); 
BGl_symbol3967z00zz__hashz00 = 
bstring_to_symbol(BGl_string3968z00zz__hashz00); 
BGl_list3964z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3965z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3967z00zz__hashz00, BNIL))))); 
BGl_symbol3974z00zz__hashz00 = 
bstring_to_symbol(BGl_string3975z00zz__hashz00); 
BGl_symbol3976z00zz__hashz00 = 
bstring_to_symbol(BGl_string3977z00zz__hashz00); 
BGl_list3973z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3974z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3976z00zz__hashz00, BNIL))))); 
BGl_symbol3983z00zz__hashz00 = 
bstring_to_symbol(BGl_string3984z00zz__hashz00); 
BGl_symbol3985z00zz__hashz00 = 
bstring_to_symbol(BGl_string3986z00zz__hashz00); 
BGl_list3982z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3983z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3985z00zz__hashz00, BNIL))))); 
BGl_symbol3993z00zz__hashz00 = 
bstring_to_symbol(BGl_string3994z00zz__hashz00); 
BGl_symbol3995z00zz__hashz00 = 
bstring_to_symbol(BGl_string3996z00zz__hashz00); 
BGl_list3992z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3948z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3993z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3995z00zz__hashz00, BNIL))))); 
BGl_symbol4007z00zz__hashz00 = 
bstring_to_symbol(BGl_string4008z00zz__hashz00); 
BGl_symbol4009z00zz__hashz00 = 
bstring_to_symbol(BGl_string4010z00zz__hashz00); 
BGl_list4006z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4009z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4015z00zz__hashz00 = 
bstring_to_symbol(BGl_string4016z00zz__hashz00); 
BGl_list4014z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4015z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4031z00zz__hashz00 = 
bstring_to_symbol(BGl_string4032z00zz__hashz00); 
BGl_list4030z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4031z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4037z00zz__hashz00 = 
bstring_to_symbol(BGl_string4038z00zz__hashz00); 
BGl_symbol4039z00zz__hashz00 = 
bstring_to_symbol(BGl_string4040z00zz__hashz00); 
BGl_list4036z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4039z00zz__hashz00, BNIL)))); 
BGl_symbol4044z00zz__hashz00 = 
bstring_to_symbol(BGl_string4045z00zz__hashz00); 
BGl_list4043z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4044z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4047z00zz__hashz00 = 
bstring_to_symbol(BGl_string4048z00zz__hashz00); 
BGl_list4046z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4047z00zz__hashz00, BNIL)))); 
BGl_symbol4053z00zz__hashz00 = 
bstring_to_symbol(BGl_string4054z00zz__hashz00); 
BGl_list4052z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4039z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4053z00zz__hashz00, BNIL))))); 
BGl_symbol4056z00zz__hashz00 = 
bstring_to_symbol(BGl_string4057z00zz__hashz00); 
BGl_list4055z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4056z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4053z00zz__hashz00, BNIL))))); 
BGl_symbol4062z00zz__hashz00 = 
bstring_to_symbol(BGl_string4063z00zz__hashz00); 
BGl_list4061z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4062z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4065z00zz__hashz00 = 
bstring_to_symbol(BGl_string4066z00zz__hashz00); 
BGl_list4064z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4037z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4056z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4065z00zz__hashz00, BNIL))))); 
BGl_symbol4074z00zz__hashz00 = 
bstring_to_symbol(BGl_string4075z00zz__hashz00); 
BGl_list4073z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4074z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4077z00zz__hashz00 = 
bstring_to_symbol(BGl_string4078z00zz__hashz00); 
BGl_list4076z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4007z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4077z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, BNIL))))); 
BGl_symbol4088z00zz__hashz00 = 
bstring_to_symbol(BGl_string4089z00zz__hashz00); 
BGl_symbol4092z00zz__hashz00 = 
bstring_to_symbol(BGl_string4093z00zz__hashz00); 
BGl_symbol4094z00zz__hashz00 = 
bstring_to_symbol(BGl_string4095z00zz__hashz00); 
BGl_list4091z00zz__hashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3862z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4092z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4092z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3866z00zz__hashz00, 
MAKE_YOUNG_PAIR(BGl_symbol4094z00zz__hashz00, BNIL))))); 
return ( 
BGl_symbol4098z00zz__hashz00 = 
bstring_to_symbol(BGl_string4099z00zz__hashz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
BGl_defaultzd2hashtablezd2bucketzd2lengthzd2zz__hashz00 = 128L; 
return ( 
BGl_defaultzd2maxzd2bucketzd2lengthzd2zz__hashz00 = 10L, BUNSPEC) ;} 

}



/* table-get-hashnumber */
long BGl_tablezd2getzd2hashnumberz00zz__hashz00(obj_t BgL_tablez00_37, obj_t BgL_keyz00_38)
{
{ /* Llib/hash.sch 21 */
{ /* Llib/hash.sch 22 */
 obj_t BgL_hashnz00_1575;
{ /* Llib/hash.sch 15 */
 bool_t BgL_test4324z00_6975;
{ /* Llib/hash.sch 15 */
 obj_t BgL_tmpz00_6976;
{ /* Llib/hash.sch 15 */
 obj_t BgL_res2496z00_3228;
{ /* Llib/hash.sch 15 */
 obj_t BgL_aux2927z00_5794;
BgL_aux2927z00_5794 = 
STRUCT_KEY(BgL_tablez00_37); 
if(
SYMBOLP(BgL_aux2927z00_5794))
{ /* Llib/hash.sch 15 */
BgL_res2496z00_3228 = BgL_aux2927z00_5794; }  else 
{ 
 obj_t BgL_auxz00_6980;
BgL_auxz00_6980 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3854z00zz__hashz00, 
BINT(963L), BGl_string3855z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2927z00_5794); 
FAILURE(BgL_auxz00_6980,BFALSE,BFALSE);} } 
BgL_tmpz00_6976 = BgL_res2496z00_3228; } 
BgL_test4324z00_6975 = 
(BgL_tmpz00_6976==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4324z00_6975)
{ /* Llib/hash.sch 15 */
 int BgL_tmpz00_6985;
BgL_tmpz00_6985 = 
(int)(4L); 
BgL_hashnz00_1575 = 
STRUCT_REF(BgL_tablez00_37, BgL_tmpz00_6985); }  else 
{ /* Llib/hash.sch 15 */
BgL_hashnz00_1575 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_37); } } 
if(
PROCEDUREP(BgL_hashnz00_1575))
{ /* Llib/hash.sch 25 */
 obj_t BgL_arg1370z00_1577;
if(
PROCEDURE_CORRECT_ARITYP(BgL_hashnz00_1575, 1))
{ /* Llib/hash.sch 25 */
BgL_arg1370z00_1577 = 
BGL_PROCEDURE_CALL1(BgL_hashnz00_1575, BgL_keyz00_38); }  else 
{ /* Llib/hash.sch 25 */
FAILURE(BGl_string3860z00zz__hashz00,BGl_list3861z00zz__hashz00,BgL_hashnz00_1575);} 
{ /* Llib/hash.sch 25 */
 long BgL_nz00_3229;
{ /* Llib/hash.sch 25 */
 obj_t BgL_tmpz00_6998;
if(
INTEGERP(BgL_arg1370z00_1577))
{ /* Llib/hash.sch 25 */
BgL_tmpz00_6998 = BgL_arg1370z00_1577
; }  else 
{ 
 obj_t BgL_auxz00_7001;
BgL_auxz00_7001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3854z00zz__hashz00, 
BINT(1434L), BGl_string3855z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1370z00_1577); 
FAILURE(BgL_auxz00_7001,BFALSE,BFALSE);} 
BgL_nz00_3229 = 
(long)CINT(BgL_tmpz00_6998); } 
if(
(BgL_nz00_3229<0L))
{ /* Llib/hash.sch 25 */
return 
NEG(BgL_nz00_3229);}  else 
{ /* Llib/hash.sch 25 */
return BgL_nz00_3229;} } }  else 
{ /* Llib/hash.sch 24 */
if(
(BgL_hashnz00_1575==BGl_symbol3869z00zz__hashz00))
{ /* Llib/hash.sch 26 */
return 
BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_38);}  else 
{ /* Llib/hash.sch 26 */
return 
BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_38);} } } } 

}



/* make-hashtable */
BGL_EXPORTED_DEF obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t BgL_argsz00_42)
{
{ /* Llib/hash.scm 162 */
{ /* Llib/hash.scm 163 */
 obj_t BgL_siza7eza7_1582;
if(
PAIRP(BgL_argsz00_42))
{ /* Llib/hash.scm 164 */
 obj_t BgL_siza7eza7_1614;
BgL_siza7eza7_1614 = 
CAR(BgL_argsz00_42); 
BgL_argsz00_42 = 
CDR(BgL_argsz00_42); 
{ /* Llib/hash.scm 167 */
 bool_t BgL_test4332z00_7017;
if(
INTEGERP(BgL_siza7eza7_1614))
{ /* Llib/hash.scm 167 */
BgL_test4332z00_7017 = 
(
(long)CINT(BgL_siza7eza7_1614)>=1L)
; }  else 
{ /* Llib/hash.scm 167 */
BgL_test4332z00_7017 = ((bool_t)0)
; } 
if(BgL_test4332z00_7017)
{ /* Llib/hash.scm 167 */
BgL_siza7eza7_1582 = BgL_siza7eza7_1614; }  else 
{ /* Llib/hash.scm 167 */
if(
(BgL_siza7eza7_1614==BUNSPEC))
{ /* Llib/hash.scm 169 */
BgL_siza7eza7_1582 = 
BINT(128L); }  else 
{ /* Llib/hash.scm 169 */
BgL_siza7eza7_1582 = 
BGl_errorz00zz__errorz00(BGl_string3871z00zz__hashz00, BGl_string3872z00zz__hashz00, BgL_siza7eza7_1614); } } } }  else 
{ /* Llib/hash.scm 163 */
BgL_siza7eza7_1582 = 
BINT(128L); } 
{ /* Llib/hash.scm 163 */
 obj_t BgL_mblenz00_1583;
if(
PAIRP(BgL_argsz00_42))
{ /* Llib/hash.scm 177 */
 obj_t BgL_mblenz00_1609;
BgL_mblenz00_1609 = 
CAR(BgL_argsz00_42); 
BgL_argsz00_42 = 
CDR(BgL_argsz00_42); 
{ /* Llib/hash.scm 180 */
 bool_t BgL_test4336z00_7031;
if(
INTEGERP(BgL_mblenz00_1609))
{ /* Llib/hash.scm 180 */
BgL_test4336z00_7031 = 
(
(long)CINT(BgL_mblenz00_1609)>=1L)
; }  else 
{ /* Llib/hash.scm 180 */
BgL_test4336z00_7031 = ((bool_t)0)
; } 
if(BgL_test4336z00_7031)
{ /* Llib/hash.scm 180 */
BgL_mblenz00_1583 = BgL_mblenz00_1609; }  else 
{ /* Llib/hash.scm 180 */
if(
(BgL_mblenz00_1609==BUNSPEC))
{ /* Llib/hash.scm 182 */
BgL_mblenz00_1583 = 
BINT(10L); }  else 
{ /* Llib/hash.scm 182 */
BgL_mblenz00_1583 = 
BGl_errorz00zz__errorz00(BGl_string3871z00zz__hashz00, BGl_string3873z00zz__hashz00, BgL_mblenz00_1609); } } } }  else 
{ /* Llib/hash.scm 176 */
BgL_mblenz00_1583 = 
BINT(10L); } 
{ /* Llib/hash.scm 176 */
 obj_t BgL_eqtestz00_1584;
if(
PAIRP(BgL_argsz00_42))
{ /* Llib/hash.scm 190 */
 obj_t BgL_eqtestz00_1604;
BgL_eqtestz00_1604 = 
CAR(BgL_argsz00_42); 
BgL_argsz00_42 = 
CDR(BgL_argsz00_42); 
{ /* Llib/hash.scm 193 */
 bool_t BgL_test4340z00_7045;
if(
PROCEDUREP(BgL_eqtestz00_1604))
{ /* Llib/hash.scm 193 */
BgL_test4340z00_7045 = 
PROCEDURE_CORRECT_ARITYP(BgL_eqtestz00_1604, 
(int)(2L))
; }  else 
{ /* Llib/hash.scm 193 */
BgL_test4340z00_7045 = ((bool_t)0)
; } 
if(BgL_test4340z00_7045)
{ /* Llib/hash.scm 193 */
BgL_eqtestz00_1584 = BgL_eqtestz00_1604; }  else 
{ /* Llib/hash.scm 193 */
if(
(BgL_eqtestz00_1604==BUNSPEC))
{ /* Llib/hash.scm 195 */
BgL_eqtestz00_1584 = BFALSE; }  else 
{ /* Llib/hash.scm 195 */
BgL_eqtestz00_1584 = 
BGl_errorz00zz__errorz00(BGl_string3871z00zz__hashz00, BGl_string3874z00zz__hashz00, BgL_eqtestz00_1604); } } } }  else 
{ /* Llib/hash.scm 189 */
BgL_eqtestz00_1584 = BFALSE; } 
{ /* Llib/hash.scm 189 */
 obj_t BgL_hashnz00_1585;
if(
PAIRP(BgL_argsz00_42))
{ /* Llib/hash.scm 203 */
 obj_t BgL_hnz00_1599;
BgL_hnz00_1599 = 
CAR(BgL_argsz00_42); 
BgL_argsz00_42 = 
CDR(BgL_argsz00_42); 
{ /* Llib/hash.scm 206 */
 bool_t BgL_test4344z00_7057;
if(
PROCEDUREP(BgL_hnz00_1599))
{ /* Llib/hash.scm 206 */
BgL_test4344z00_7057 = 
PROCEDURE_CORRECT_ARITYP(BgL_hnz00_1599, 
(int)(1L))
; }  else 
{ /* Llib/hash.scm 206 */
BgL_test4344z00_7057 = ((bool_t)0)
; } 
if(BgL_test4344z00_7057)
{ /* Llib/hash.scm 206 */
BgL_hashnz00_1585 = BgL_hnz00_1599; }  else 
{ /* Llib/hash.scm 206 */
if(
(BgL_hnz00_1599==BUNSPEC))
{ /* Llib/hash.scm 208 */
BgL_hashnz00_1585 = BFALSE; }  else 
{ /* Llib/hash.scm 208 */
BgL_hashnz00_1585 = 
BGl_errorz00zz__errorz00(BGl_string3871z00zz__hashz00, BGl_string3875z00zz__hashz00, BgL_hnz00_1599); } } } }  else 
{ /* Llib/hash.scm 202 */
BgL_hashnz00_1585 = BFALSE; } 
{ /* Llib/hash.scm 202 */
 long BgL_wkkz00_1586;
if(
PAIRP(BgL_argsz00_42))
{ /* Llib/hash.scm 216 */
 obj_t BgL_wkkz00_1596;
BgL_wkkz00_1596 = 
CAR(BgL_argsz00_42); 
BgL_argsz00_42 = 
CDR(BgL_argsz00_42); 
{ /* Llib/hash.scm 218 */
 bool_t BgL_test4348z00_7069;
if(
(BgL_wkkz00_1596==BUNSPEC))
{ /* Llib/hash.scm 218 */
BgL_test4348z00_7069 = ((bool_t)0)
; }  else 
{ /* Llib/hash.scm 218 */
BgL_test4348z00_7069 = 
CBOOL(BgL_wkkz00_1596)
; } 
if(BgL_test4348z00_7069)
{ /* Llib/hash.scm 218 */
BgL_wkkz00_1586 = 1L; }  else 
{ /* Llib/hash.scm 218 */
BgL_wkkz00_1586 = 0L; } } }  else 
{ /* Llib/hash.scm 215 */
BgL_wkkz00_1586 = 0L; } 
{ /* Llib/hash.scm 215 */
 long BgL_wkdz00_1587;
if(
PAIRP(BgL_argsz00_42))
{ /* Llib/hash.scm 223 */
 obj_t BgL_wkdz00_1593;
BgL_wkdz00_1593 = 
CAR(BgL_argsz00_42); 
{ /* Llib/hash.scm 224 */
 bool_t BgL_test4351z00_7076;
if(
(BgL_wkdz00_1593==BUNSPEC))
{ /* Llib/hash.scm 224 */
BgL_test4351z00_7076 = ((bool_t)0)
; }  else 
{ /* Llib/hash.scm 224 */
BgL_test4351z00_7076 = 
CBOOL(BgL_wkdz00_1593)
; } 
if(BgL_test4351z00_7076)
{ /* Llib/hash.scm 224 */
BgL_wkdz00_1587 = 2L; }  else 
{ /* Llib/hash.scm 224 */
BgL_wkdz00_1587 = 0L; } } }  else 
{ /* Llib/hash.scm 222 */
BgL_wkdz00_1587 = 0L; } 
{ /* Llib/hash.scm 222 */
 long BgL_wkz00_1588;
BgL_wkz00_1588 = 
(BgL_wkkz00_1586 | BgL_wkdz00_1587); 
{ /* Llib/hash.scm 228 */

if(
(
BINT(BgL_wkz00_1588)==
BINT(8L)))
{ /* Llib/hash.scm 230 */
 obj_t BgL_arg1375z00_1589;
{ /* Llib/hash.scm 230 */
 long BgL_arg1376z00_1590;
{ /* Llib/hash.scm 230 */
 long BgL_za72za7_3273;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7085;
if(
INTEGERP(BgL_siza7eza7_1582))
{ /* Llib/hash.scm 230 */
BgL_tmpz00_7085 = BgL_siza7eza7_1582
; }  else 
{ 
 obj_t BgL_auxz00_7088;
BgL_auxz00_7088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8483L), BGl_string3871z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1582); 
FAILURE(BgL_auxz00_7088,BFALSE,BFALSE);} 
BgL_za72za7_3273 = 
(long)CINT(BgL_tmpz00_7085); } 
BgL_arg1376z00_1590 = 
(3L*BgL_za72za7_3273); } 
BgL_arg1375z00_1589 = 
make_vector(BgL_arg1376z00_1590, BFALSE); } 
{ /* Llib/hash.scm 230 */
 obj_t BgL_newz00_3274;
BgL_newz00_3274 = 
create_struct(BGl_symbol3857z00zz__hashz00, 
(int)(8L)); 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4355z00_7097;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7098;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2498z00_3278;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2932z00_5800;
BgL_aux2932z00_5800 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2932z00_5800))
{ /* Llib/hash.scm 230 */
BgL_res2498z00_3278 = BgL_aux2932z00_5800; }  else 
{ 
 obj_t BgL_auxz00_7102;
BgL_auxz00_7102 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2932z00_5800); 
FAILURE(BgL_auxz00_7102,BFALSE,BFALSE);} } 
BgL_tmpz00_7098 = BgL_res2498z00_3278; } 
BgL_test4355z00_7097 = 
(BgL_tmpz00_7098==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4355z00_7097)
{ /* Llib/hash.scm 230 */
 obj_t BgL_auxz00_7109; int BgL_tmpz00_7107;
BgL_auxz00_7109 = 
BINT(0L); 
BgL_tmpz00_7107 = 
(int)(7L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7107, BgL_auxz00_7109); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4357z00_7113;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7114;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2499z00_3282;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2934z00_5802;
BgL_aux2934z00_5802 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2934z00_5802))
{ /* Llib/hash.scm 230 */
BgL_res2499z00_3282 = BgL_aux2934z00_5802; }  else 
{ 
 obj_t BgL_auxz00_7118;
BgL_auxz00_7118 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2934z00_5802); 
FAILURE(BgL_auxz00_7118,BFALSE,BFALSE);} } 
BgL_tmpz00_7114 = BgL_res2499z00_3282; } 
BgL_test4357z00_7113 = 
(BgL_tmpz00_7114==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4357z00_7113)
{ /* Llib/hash.scm 230 */
 obj_t BgL_auxz00_7125; int BgL_tmpz00_7123;
BgL_auxz00_7125 = 
BINT(0L); 
BgL_tmpz00_7123 = 
(int)(6L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7123, BgL_auxz00_7125); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4359z00_7129;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7130;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2500z00_3286;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2936z00_5804;
BgL_aux2936z00_5804 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2936z00_5804))
{ /* Llib/hash.scm 230 */
BgL_res2500z00_3286 = BgL_aux2936z00_5804; }  else 
{ 
 obj_t BgL_auxz00_7134;
BgL_auxz00_7134 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2936z00_5804); 
FAILURE(BgL_auxz00_7134,BFALSE,BFALSE);} } 
BgL_tmpz00_7130 = BgL_res2500z00_3286; } 
BgL_test4359z00_7129 = 
(BgL_tmpz00_7130==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4359z00_7129)
{ /* Llib/hash.scm 230 */
 obj_t BgL_auxz00_7141; int BgL_tmpz00_7139;
BgL_auxz00_7141 = 
BINT(BgL_wkz00_1588); 
BgL_tmpz00_7139 = 
(int)(5L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7139, BgL_auxz00_7141); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4361z00_7145;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7146;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2501z00_3290;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2938z00_5806;
BgL_aux2938z00_5806 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2938z00_5806))
{ /* Llib/hash.scm 230 */
BgL_res2501z00_3290 = BgL_aux2938z00_5806; }  else 
{ 
 obj_t BgL_auxz00_7150;
BgL_auxz00_7150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2938z00_5806); 
FAILURE(BgL_auxz00_7150,BFALSE,BFALSE);} } 
BgL_tmpz00_7146 = BgL_res2501z00_3290; } 
BgL_test4361z00_7145 = 
(BgL_tmpz00_7146==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4361z00_7145)
{ /* Llib/hash.scm 230 */
 int BgL_tmpz00_7155;
BgL_tmpz00_7155 = 
(int)(4L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7155, BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4363z00_7159;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7160;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2502z00_3294;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2940z00_5808;
BgL_aux2940z00_5808 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2940z00_5808))
{ /* Llib/hash.scm 230 */
BgL_res2502z00_3294 = BgL_aux2940z00_5808; }  else 
{ 
 obj_t BgL_auxz00_7164;
BgL_auxz00_7164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2940z00_5808); 
FAILURE(BgL_auxz00_7164,BFALSE,BFALSE);} } 
BgL_tmpz00_7160 = BgL_res2502z00_3294; } 
BgL_test4363z00_7159 = 
(BgL_tmpz00_7160==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4363z00_7159)
{ /* Llib/hash.scm 230 */
 int BgL_tmpz00_7169;
BgL_tmpz00_7169 = 
(int)(3L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7169, BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4365z00_7173;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7174;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2503z00_3298;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2942z00_5810;
BgL_aux2942z00_5810 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2942z00_5810))
{ /* Llib/hash.scm 230 */
BgL_res2503z00_3298 = BgL_aux2942z00_5810; }  else 
{ 
 obj_t BgL_auxz00_7178;
BgL_auxz00_7178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2942z00_5810); 
FAILURE(BgL_auxz00_7178,BFALSE,BFALSE);} } 
BgL_tmpz00_7174 = BgL_res2503z00_3298; } 
BgL_test4365z00_7173 = 
(BgL_tmpz00_7174==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4365z00_7173)
{ /* Llib/hash.scm 230 */
 int BgL_tmpz00_7183;
BgL_tmpz00_7183 = 
(int)(2L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7183, BgL_arg1375z00_1589); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4367z00_7187;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7188;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2504z00_3302;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2944z00_5812;
BgL_aux2944z00_5812 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2944z00_5812))
{ /* Llib/hash.scm 230 */
BgL_res2504z00_3302 = BgL_aux2944z00_5812; }  else 
{ 
 obj_t BgL_auxz00_7192;
BgL_auxz00_7192 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2944z00_5812); 
FAILURE(BgL_auxz00_7192,BFALSE,BFALSE);} } 
BgL_tmpz00_7188 = BgL_res2504z00_3302; } 
BgL_test4367z00_7187 = 
(BgL_tmpz00_7188==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4367z00_7187)
{ /* Llib/hash.scm 230 */
 int BgL_tmpz00_7197;
BgL_tmpz00_7197 = 
(int)(1L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7197, BgL_siza7eza7_1582); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
{ /* Llib/hash.scm 230 */
 bool_t BgL_test4369z00_7201;
{ /* Llib/hash.scm 230 */
 obj_t BgL_tmpz00_7202;
{ /* Llib/hash.scm 230 */
 obj_t BgL_res2505z00_3306;
{ /* Llib/hash.scm 230 */
 obj_t BgL_aux2946z00_5814;
BgL_aux2946z00_5814 = 
STRUCT_KEY(BgL_newz00_3274); 
if(
SYMBOLP(BgL_aux2946z00_5814))
{ /* Llib/hash.scm 230 */
BgL_res2505z00_3306 = BgL_aux2946z00_5814; }  else 
{ 
 obj_t BgL_auxz00_7206;
BgL_auxz00_7206 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8444L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2946z00_5814); 
FAILURE(BgL_auxz00_7206,BFALSE,BFALSE);} } 
BgL_tmpz00_7202 = BgL_res2505z00_3306; } 
BgL_test4369z00_7201 = 
(BgL_tmpz00_7202==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4369z00_7201)
{ /* Llib/hash.scm 230 */
 obj_t BgL_auxz00_7213; int BgL_tmpz00_7211;
BgL_auxz00_7213 = 
BINT(0L); 
BgL_tmpz00_7211 = 
(int)(0L); 
STRUCT_SET(BgL_newz00_3274, BgL_tmpz00_7211, BgL_auxz00_7213); }  else 
{ /* Llib/hash.scm 230 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3274); } } 
return BgL_newz00_3274;} }  else 
{ /* Llib/hash.scm 231 */
 obj_t BgL_arg1377z00_1591;
{ /* Llib/hash.scm 231 */
 long BgL_tmpz00_7217;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7218;
if(
INTEGERP(BgL_siza7eza7_1582))
{ /* Llib/hash.scm 231 */
BgL_tmpz00_7218 = BgL_siza7eza7_1582
; }  else 
{ 
 obj_t BgL_auxz00_7221;
BgL_auxz00_7221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8533L), BGl_string3871z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1582); 
FAILURE(BgL_auxz00_7221,BFALSE,BFALSE);} 
BgL_tmpz00_7217 = 
(long)CINT(BgL_tmpz00_7218); } 
BgL_arg1377z00_1591 = 
make_vector(BgL_tmpz00_7217, BNIL); } 
{ /* Llib/hash.scm 231 */
 obj_t BgL_newz00_3307;
BgL_newz00_3307 = 
create_struct(BGl_symbol3857z00zz__hashz00, 
(int)(8L)); 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4372z00_7229;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7230;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2506z00_3311;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2949z00_5817;
BgL_aux2949z00_5817 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2949z00_5817))
{ /* Llib/hash.scm 231 */
BgL_res2506z00_3311 = BgL_aux2949z00_5817; }  else 
{ 
 obj_t BgL_auxz00_7234;
BgL_auxz00_7234 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2949z00_5817); 
FAILURE(BgL_auxz00_7234,BFALSE,BFALSE);} } 
BgL_tmpz00_7230 = BgL_res2506z00_3311; } 
BgL_test4372z00_7229 = 
(BgL_tmpz00_7230==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4372z00_7229)
{ /* Llib/hash.scm 231 */
 int BgL_tmpz00_7239;
BgL_tmpz00_7239 = 
(int)(7L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7239, BGL_REAL_CNST( BGl_real3878z00zz__hashz00)); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4374z00_7243;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7244;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2507z00_3315;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2951z00_5819;
BgL_aux2951z00_5819 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2951z00_5819))
{ /* Llib/hash.scm 231 */
BgL_res2507z00_3315 = BgL_aux2951z00_5819; }  else 
{ 
 obj_t BgL_auxz00_7248;
BgL_auxz00_7248 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2951z00_5819); 
FAILURE(BgL_auxz00_7248,BFALSE,BFALSE);} } 
BgL_tmpz00_7244 = BgL_res2507z00_3315; } 
BgL_test4374z00_7243 = 
(BgL_tmpz00_7244==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4374z00_7243)
{ /* Llib/hash.scm 231 */
 obj_t BgL_auxz00_7255; int BgL_tmpz00_7253;
BgL_auxz00_7255 = 
BINT(-1L); 
BgL_tmpz00_7253 = 
(int)(6L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7253, BgL_auxz00_7255); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4376z00_7259;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7260;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2508z00_3319;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2953z00_5821;
BgL_aux2953z00_5821 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2953z00_5821))
{ /* Llib/hash.scm 231 */
BgL_res2508z00_3319 = BgL_aux2953z00_5821; }  else 
{ 
 obj_t BgL_auxz00_7264;
BgL_auxz00_7264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2953z00_5821); 
FAILURE(BgL_auxz00_7264,BFALSE,BFALSE);} } 
BgL_tmpz00_7260 = BgL_res2508z00_3319; } 
BgL_test4376z00_7259 = 
(BgL_tmpz00_7260==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4376z00_7259)
{ /* Llib/hash.scm 231 */
 obj_t BgL_auxz00_7271; int BgL_tmpz00_7269;
BgL_auxz00_7271 = 
BINT(BgL_wkz00_1588); 
BgL_tmpz00_7269 = 
(int)(5L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7269, BgL_auxz00_7271); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4378z00_7275;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7276;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2509z00_3323;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2955z00_5823;
BgL_aux2955z00_5823 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2955z00_5823))
{ /* Llib/hash.scm 231 */
BgL_res2509z00_3323 = BgL_aux2955z00_5823; }  else 
{ 
 obj_t BgL_auxz00_7280;
BgL_auxz00_7280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2955z00_5823); 
FAILURE(BgL_auxz00_7280,BFALSE,BFALSE);} } 
BgL_tmpz00_7276 = BgL_res2509z00_3323; } 
BgL_test4378z00_7275 = 
(BgL_tmpz00_7276==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4378z00_7275)
{ /* Llib/hash.scm 231 */
 int BgL_tmpz00_7285;
BgL_tmpz00_7285 = 
(int)(4L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7285, BgL_hashnz00_1585); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4380z00_7289;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7290;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2510z00_3327;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2957z00_5825;
BgL_aux2957z00_5825 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2957z00_5825))
{ /* Llib/hash.scm 231 */
BgL_res2510z00_3327 = BgL_aux2957z00_5825; }  else 
{ 
 obj_t BgL_auxz00_7294;
BgL_auxz00_7294 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2957z00_5825); 
FAILURE(BgL_auxz00_7294,BFALSE,BFALSE);} } 
BgL_tmpz00_7290 = BgL_res2510z00_3327; } 
BgL_test4380z00_7289 = 
(BgL_tmpz00_7290==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4380z00_7289)
{ /* Llib/hash.scm 231 */
 int BgL_tmpz00_7299;
BgL_tmpz00_7299 = 
(int)(3L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7299, BgL_eqtestz00_1584); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4382z00_7303;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7304;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2511z00_3331;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2959z00_5827;
BgL_aux2959z00_5827 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2959z00_5827))
{ /* Llib/hash.scm 231 */
BgL_res2511z00_3331 = BgL_aux2959z00_5827; }  else 
{ 
 obj_t BgL_auxz00_7308;
BgL_auxz00_7308 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2959z00_5827); 
FAILURE(BgL_auxz00_7308,BFALSE,BFALSE);} } 
BgL_tmpz00_7304 = BgL_res2511z00_3331; } 
BgL_test4382z00_7303 = 
(BgL_tmpz00_7304==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4382z00_7303)
{ /* Llib/hash.scm 231 */
 int BgL_tmpz00_7313;
BgL_tmpz00_7313 = 
(int)(2L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7313, BgL_arg1377z00_1591); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4384z00_7317;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7318;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2512z00_3335;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2961z00_5829;
BgL_aux2961z00_5829 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2961z00_5829))
{ /* Llib/hash.scm 231 */
BgL_res2512z00_3335 = BgL_aux2961z00_5829; }  else 
{ 
 obj_t BgL_auxz00_7322;
BgL_auxz00_7322 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2961z00_5829); 
FAILURE(BgL_auxz00_7322,BFALSE,BFALSE);} } 
BgL_tmpz00_7318 = BgL_res2512z00_3335; } 
BgL_test4384z00_7317 = 
(BgL_tmpz00_7318==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4384z00_7317)
{ /* Llib/hash.scm 231 */
 int BgL_tmpz00_7327;
BgL_tmpz00_7327 = 
(int)(1L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7327, BgL_mblenz00_1583); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
{ /* Llib/hash.scm 231 */
 bool_t BgL_test4386z00_7331;
{ /* Llib/hash.scm 231 */
 obj_t BgL_tmpz00_7332;
{ /* Llib/hash.scm 231 */
 obj_t BgL_res2513z00_3339;
{ /* Llib/hash.scm 231 */
 obj_t BgL_aux2963z00_5831;
BgL_aux2963z00_5831 = 
STRUCT_KEY(BgL_newz00_3307); 
if(
SYMBOLP(BgL_aux2963z00_5831))
{ /* Llib/hash.scm 231 */
BgL_res2513z00_3339 = BgL_aux2963z00_5831; }  else 
{ 
 obj_t BgL_auxz00_7336;
BgL_auxz00_7336 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8513L), BGl_string3871z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2963z00_5831); 
FAILURE(BgL_auxz00_7336,BFALSE,BFALSE);} } 
BgL_tmpz00_7332 = BgL_res2513z00_3339; } 
BgL_test4386z00_7331 = 
(BgL_tmpz00_7332==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4386z00_7331)
{ /* Llib/hash.scm 231 */
 obj_t BgL_auxz00_7343; int BgL_tmpz00_7341;
BgL_auxz00_7343 = 
BINT(0L); 
BgL_tmpz00_7341 = 
(int)(0L); 
STRUCT_SET(BgL_newz00_3307, BgL_tmpz00_7341, BgL_auxz00_7343); }  else 
{ /* Llib/hash.scm 231 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3307); } } 
return BgL_newz00_3307;} } } } } } } } } } } 

}



/* &make-hashtable */
obj_t BGl_z62makezd2hashtablezb0zz__hashz00(obj_t BgL_envz00_5411, obj_t BgL_argsz00_5412)
{
{ /* Llib/hash.scm 162 */
return 
BGl_makezd2hashtablezd2zz__hashz00(BgL_argsz00_5412);} 

}



/* _create-hashtable */
obj_t BGl__createzd2hashtablezd2zz__hashz00(obj_t BgL_env1160z00_52, obj_t BgL_opt1159z00_51)
{
{ /* Llib/hash.scm 236 */
{ /* Llib/hash.scm 236 */

{ /* Llib/hash.scm 236 */
 obj_t BgL_bucketzd2expansionzd2_1622;
BgL_bucketzd2expansionzd2_1622 = BGL_REAL_CNST( BGl_real3878z00zz__hashz00); 
{ /* Llib/hash.scm 236 */
 obj_t BgL_eqtestz00_1623;
BgL_eqtestz00_1623 = BFALSE; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_hashz00_1624;
BgL_hashz00_1624 = BFALSE; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_maxzd2bucketzd2lengthz00_1625;
BgL_maxzd2bucketzd2lengthz00_1625 = 
BINT(10L); 
{ /* Llib/hash.scm 236 */
 obj_t BgL_maxzd2lengthzd2_1626;
BgL_maxzd2lengthzd2_1626 = 
BINT(16384L); 
{ /* Llib/hash.scm 236 */
 obj_t BgL_persistentz00_1627;
BgL_persistentz00_1627 = BFALSE; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_siza7eza7_1628;
BgL_siza7eza7_1628 = 
BINT(128L); 
{ /* Llib/hash.scm 241 */
 obj_t BgL_weakz00_1629;
BgL_weakz00_1629 = BGl_symbol3879z00zz__hashz00; 
{ /* Llib/hash.scm 236 */

{ 
 long BgL_iz00_1630;
BgL_iz00_1630 = 0L; 
BgL_check1163z00_1631:
if(
(BgL_iz00_1630==
VECTOR_LENGTH(BgL_opt1159z00_51)))
{ /* Llib/hash.scm 236 */BNIL; }  else 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4389z00_7354;
{ /* Llib/hash.scm 236 */
 obj_t BgL_arg1400z00_1637;
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4390z00_7355;
{ /* Llib/hash.scm 236 */
 long BgL_tmpz00_7356;
BgL_tmpz00_7356 = 
VECTOR_LENGTH(BgL_opt1159z00_51); 
BgL_test4390z00_7355 = 
BOUND_CHECK(BgL_iz00_1630, BgL_tmpz00_7356); } 
if(BgL_test4390z00_7355)
{ /* Llib/hash.scm 236 */
BgL_arg1400z00_1637 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_iz00_1630); }  else 
{ 
 obj_t BgL_auxz00_7360;
BgL_auxz00_7360 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3881z00zz__hashz00, BgL_opt1159z00_51, 
(int)(
VECTOR_LENGTH(BgL_opt1159z00_51)), 
(int)(BgL_iz00_1630)); 
FAILURE(BgL_auxz00_7360,BFALSE,BFALSE);} } 
BgL_test4389z00_7354 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1400z00_1637, BGl_list3882z00zz__hashz00)); } 
if(BgL_test4389z00_7354)
{ 
 long BgL_iz00_7369;
BgL_iz00_7369 = 
(BgL_iz00_1630+2L); 
BgL_iz00_1630 = BgL_iz00_7369; 
goto BgL_check1163z00_1631;}  else 
{ /* Llib/hash.scm 236 */
 obj_t BgL_arg1399z00_1636;
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4391z00_7371;
{ /* Llib/hash.scm 236 */
 long BgL_tmpz00_7372;
BgL_tmpz00_7372 = 
VECTOR_LENGTH(BgL_opt1159z00_51); 
BgL_test4391z00_7371 = 
BOUND_CHECK(BgL_iz00_1630, BgL_tmpz00_7372); } 
if(BgL_test4391z00_7371)
{ /* Llib/hash.scm 236 */
BgL_arg1399z00_1636 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_iz00_1630); }  else 
{ 
 obj_t BgL_auxz00_7376;
BgL_auxz00_7376 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3881z00zz__hashz00, BgL_opt1159z00_51, 
(int)(
VECTOR_LENGTH(BgL_opt1159z00_51)), 
(int)(BgL_iz00_1630)); 
FAILURE(BgL_auxz00_7376,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol3898z00zz__hashz00, BGl_string3900z00zz__hashz00, BgL_arg1399z00_1636); } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1165z00_1638;
BgL_index1165z00_1638 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3883z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4392z00_7386;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3355;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7387;
if(
INTEGERP(BgL_index1165z00_1638))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7387 = BgL_index1165z00_1638
; }  else 
{ 
 obj_t BgL_auxz00_7390;
BgL_auxz00_7390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1165z00_1638); 
FAILURE(BgL_auxz00_7390,BFALSE,BFALSE);} 
BgL_n1z00_3355 = 
(long)CINT(BgL_tmpz00_7387); } 
BgL_test4392z00_7386 = 
(BgL_n1z00_3355>=0L); } 
if(BgL_test4392z00_7386)
{ 
 long BgL_auxz00_7396;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7397;
if(
INTEGERP(BgL_index1165z00_1638))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7397 = BgL_index1165z00_1638
; }  else 
{ 
 obj_t BgL_auxz00_7400;
BgL_auxz00_7400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1165z00_1638); 
FAILURE(BgL_auxz00_7400,BFALSE,BFALSE);} 
BgL_auxz00_7396 = 
(long)CINT(BgL_tmpz00_7397); } 
BgL_bucketzd2expansionzd2_1622 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7396); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1166z00_1640;
BgL_index1166z00_1640 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3885z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4395z00_7408;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3356;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7409;
if(
INTEGERP(BgL_index1166z00_1640))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7409 = BgL_index1166z00_1640
; }  else 
{ 
 obj_t BgL_auxz00_7412;
BgL_auxz00_7412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1166z00_1640); 
FAILURE(BgL_auxz00_7412,BFALSE,BFALSE);} 
BgL_n1z00_3356 = 
(long)CINT(BgL_tmpz00_7409); } 
BgL_test4395z00_7408 = 
(BgL_n1z00_3356>=0L); } 
if(BgL_test4395z00_7408)
{ 
 long BgL_auxz00_7418;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7419;
if(
INTEGERP(BgL_index1166z00_1640))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7419 = BgL_index1166z00_1640
; }  else 
{ 
 obj_t BgL_auxz00_7422;
BgL_auxz00_7422 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1166z00_1640); 
FAILURE(BgL_auxz00_7422,BFALSE,BFALSE);} 
BgL_auxz00_7418 = 
(long)CINT(BgL_tmpz00_7419); } 
BgL_eqtestz00_1623 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7418); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1167z00_1642;
BgL_index1167z00_1642 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3887z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4398z00_7430;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3357;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7431;
if(
INTEGERP(BgL_index1167z00_1642))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7431 = BgL_index1167z00_1642
; }  else 
{ 
 obj_t BgL_auxz00_7434;
BgL_auxz00_7434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1167z00_1642); 
FAILURE(BgL_auxz00_7434,BFALSE,BFALSE);} 
BgL_n1z00_3357 = 
(long)CINT(BgL_tmpz00_7431); } 
BgL_test4398z00_7430 = 
(BgL_n1z00_3357>=0L); } 
if(BgL_test4398z00_7430)
{ 
 long BgL_auxz00_7440;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7441;
if(
INTEGERP(BgL_index1167z00_1642))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7441 = BgL_index1167z00_1642
; }  else 
{ 
 obj_t BgL_auxz00_7444;
BgL_auxz00_7444 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1167z00_1642); 
FAILURE(BgL_auxz00_7444,BFALSE,BFALSE);} 
BgL_auxz00_7440 = 
(long)CINT(BgL_tmpz00_7441); } 
BgL_hashz00_1624 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7440); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1168z00_1644;
BgL_index1168z00_1644 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3889z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4401z00_7452;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3358;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7453;
if(
INTEGERP(BgL_index1168z00_1644))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7453 = BgL_index1168z00_1644
; }  else 
{ 
 obj_t BgL_auxz00_7456;
BgL_auxz00_7456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1168z00_1644); 
FAILURE(BgL_auxz00_7456,BFALSE,BFALSE);} 
BgL_n1z00_3358 = 
(long)CINT(BgL_tmpz00_7453); } 
BgL_test4401z00_7452 = 
(BgL_n1z00_3358>=0L); } 
if(BgL_test4401z00_7452)
{ 
 long BgL_auxz00_7462;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7463;
if(
INTEGERP(BgL_index1168z00_1644))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7463 = BgL_index1168z00_1644
; }  else 
{ 
 obj_t BgL_auxz00_7466;
BgL_auxz00_7466 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1168z00_1644); 
FAILURE(BgL_auxz00_7466,BFALSE,BFALSE);} 
BgL_auxz00_7462 = 
(long)CINT(BgL_tmpz00_7463); } 
BgL_maxzd2bucketzd2lengthz00_1625 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7462); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1169z00_1646;
BgL_index1169z00_1646 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3891z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4404z00_7474;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3359;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7475;
if(
INTEGERP(BgL_index1169z00_1646))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7475 = BgL_index1169z00_1646
; }  else 
{ 
 obj_t BgL_auxz00_7478;
BgL_auxz00_7478 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1169z00_1646); 
FAILURE(BgL_auxz00_7478,BFALSE,BFALSE);} 
BgL_n1z00_3359 = 
(long)CINT(BgL_tmpz00_7475); } 
BgL_test4404z00_7474 = 
(BgL_n1z00_3359>=0L); } 
if(BgL_test4404z00_7474)
{ 
 long BgL_auxz00_7484;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7485;
if(
INTEGERP(BgL_index1169z00_1646))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7485 = BgL_index1169z00_1646
; }  else 
{ 
 obj_t BgL_auxz00_7488;
BgL_auxz00_7488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1169z00_1646); 
FAILURE(BgL_auxz00_7488,BFALSE,BFALSE);} 
BgL_auxz00_7484 = 
(long)CINT(BgL_tmpz00_7485); } 
BgL_maxzd2lengthzd2_1626 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7484); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1170z00_1648;
BgL_index1170z00_1648 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3893z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4407z00_7496;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3360;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7497;
if(
INTEGERP(BgL_index1170z00_1648))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7497 = BgL_index1170z00_1648
; }  else 
{ 
 obj_t BgL_auxz00_7500;
BgL_auxz00_7500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1170z00_1648); 
FAILURE(BgL_auxz00_7500,BFALSE,BFALSE);} 
BgL_n1z00_3360 = 
(long)CINT(BgL_tmpz00_7497); } 
BgL_test4407z00_7496 = 
(BgL_n1z00_3360>=0L); } 
if(BgL_test4407z00_7496)
{ 
 long BgL_auxz00_7506;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7507;
if(
INTEGERP(BgL_index1170z00_1648))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7507 = BgL_index1170z00_1648
; }  else 
{ 
 obj_t BgL_auxz00_7510;
BgL_auxz00_7510 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1170z00_1648); 
FAILURE(BgL_auxz00_7510,BFALSE,BFALSE);} 
BgL_auxz00_7506 = 
(long)CINT(BgL_tmpz00_7507); } 
BgL_persistentz00_1627 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7506); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1171z00_1650;
BgL_index1171z00_1650 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3894z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4410z00_7518;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3361;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7519;
if(
INTEGERP(BgL_index1171z00_1650))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7519 = BgL_index1171z00_1650
; }  else 
{ 
 obj_t BgL_auxz00_7522;
BgL_auxz00_7522 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1171z00_1650); 
FAILURE(BgL_auxz00_7522,BFALSE,BFALSE);} 
BgL_n1z00_3361 = 
(long)CINT(BgL_tmpz00_7519); } 
BgL_test4410z00_7518 = 
(BgL_n1z00_3361>=0L); } 
if(BgL_test4410z00_7518)
{ 
 long BgL_auxz00_7528;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7529;
if(
INTEGERP(BgL_index1171z00_1650))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7529 = BgL_index1171z00_1650
; }  else 
{ 
 obj_t BgL_auxz00_7532;
BgL_auxz00_7532 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1171z00_1650); 
FAILURE(BgL_auxz00_7532,BFALSE,BFALSE);} 
BgL_auxz00_7528 = 
(long)CINT(BgL_tmpz00_7529); } 
BgL_siza7eza7_1628 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7528); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_index1172z00_1652;
BgL_index1172z00_1652 = 
BGl_search1162ze70ze7zz__hashz00(
VECTOR_LENGTH(BgL_opt1159z00_51), BgL_opt1159z00_51, BGl_keyword3896z00zz__hashz00, 0L); 
{ /* Llib/hash.scm 236 */
 bool_t BgL_test4413z00_7540;
{ /* Llib/hash.scm 236 */
 long BgL_n1z00_3362;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7541;
if(
INTEGERP(BgL_index1172z00_1652))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7541 = BgL_index1172z00_1652
; }  else 
{ 
 obj_t BgL_auxz00_7544;
BgL_auxz00_7544 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1172z00_1652); 
FAILURE(BgL_auxz00_7544,BFALSE,BFALSE);} 
BgL_n1z00_3362 = 
(long)CINT(BgL_tmpz00_7541); } 
BgL_test4413z00_7540 = 
(BgL_n1z00_3362>=0L); } 
if(BgL_test4413z00_7540)
{ 
 long BgL_auxz00_7550;
{ /* Llib/hash.scm 236 */
 obj_t BgL_tmpz00_7551;
if(
INTEGERP(BgL_index1172z00_1652))
{ /* Llib/hash.scm 236 */
BgL_tmpz00_7551 = BgL_index1172z00_1652
; }  else 
{ 
 obj_t BgL_auxz00_7554;
BgL_auxz00_7554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(8806L), BGl_string3901z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_index1172z00_1652); 
FAILURE(BgL_auxz00_7554,BFALSE,BFALSE);} 
BgL_auxz00_7550 = 
(long)CINT(BgL_tmpz00_7551); } 
BgL_weakz00_1629 = 
VECTOR_REF(BgL_opt1159z00_51,BgL_auxz00_7550); }  else 
{ /* Llib/hash.scm 236 */BFALSE; } } } 
{ /* Llib/hash.scm 236 */
 obj_t BgL_bucketzd2expansionzd2_1654;
BgL_bucketzd2expansionzd2_1654 = BgL_bucketzd2expansionzd2_1622; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_eqtestz00_1655;
BgL_eqtestz00_1655 = BgL_eqtestz00_1623; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_hashz00_1656;
BgL_hashz00_1656 = BgL_hashz00_1624; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_maxzd2bucketzd2lengthz00_1657;
BgL_maxzd2bucketzd2lengthz00_1657 = BgL_maxzd2bucketzd2lengthz00_1625; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_maxzd2lengthzd2_1658;
BgL_maxzd2lengthzd2_1658 = BgL_maxzd2lengthzd2_1626; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_persistentz00_1659;
BgL_persistentz00_1659 = BgL_persistentz00_1627; 
{ /* Llib/hash.scm 236 */
 obj_t BgL_siza7eza7_1660;
BgL_siza7eza7_1660 = BgL_siza7eza7_1628; 
return 
BGl_createzd2hashtablezd2zz__hashz00(BgL_bucketzd2expansionzd2_1654, BgL_eqtestz00_1655, BgL_hashz00_1656, BgL_maxzd2bucketzd2lengthz00_1657, BgL_maxzd2lengthzd2_1658, BgL_persistentz00_1659, BgL_siza7eza7_1660, BgL_weakz00_1629);} } } } } } } } } } } } } } } } } } 

}



/* search1162~0 */
obj_t BGl_search1162ze70ze7zz__hashz00(long BgL_l1161z00_5532, obj_t BgL_opt1159z00_5531, obj_t BgL_k1z00_1619, long BgL_iz00_1620)
{
{ /* Llib/hash.scm 236 */
BGl_search1162ze70ze7zz__hashz00:
if(
(BgL_iz00_1620==BgL_l1161z00_5532))
{ /* Llib/hash.scm 236 */
return 
BINT(-1L);}  else 
{ /* Llib/hash.scm 236 */
if(
(BgL_iz00_1620==
(BgL_l1161z00_5532-1L)))
{ /* Llib/hash.scm 236 */
return 
BGl_errorz00zz__errorz00(BGl_symbol3898z00zz__hashz00, BGl_string3902z00zz__hashz00, 
BINT(
VECTOR_LENGTH(BgL_opt1159z00_5531)));}  else 
{ /* Llib/hash.scm 236 */
 obj_t BgL_vz00_1666;
BgL_vz00_1666 = 
VECTOR_REF(BgL_opt1159z00_5531,BgL_iz00_1620); 
if(
(BgL_vz00_1666==BgL_k1z00_1619))
{ /* Llib/hash.scm 236 */
return 
BINT(
(BgL_iz00_1620+1L));}  else 
{ 
 long BgL_iz00_7575;
BgL_iz00_7575 = 
(BgL_iz00_1620+2L); 
BgL_iz00_1620 = BgL_iz00_7575; 
goto BGl_search1162ze70ze7zz__hashz00;} } } } 

}



/* create-hashtable */
BGL_EXPORTED_DEF obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t BgL_bucketzd2expansionzd2_43, obj_t BgL_eqtestz00_44, obj_t BgL_hashz00_45, obj_t BgL_maxzd2bucketzd2lengthz00_46, obj_t BgL_maxzd2lengthzd2_47, obj_t BgL_persistentz00_48, obj_t BgL_siza7eza7_49, obj_t BgL_weakz00_50)
{
{ /* Llib/hash.scm 236 */
{ /* Llib/hash.scm 245 */
 long BgL_weakz00_1669;
if(
(BgL_weakz00_50==BGl_symbol3903z00zz__hashz00))
{ /* Llib/hash.scm 245 */
BgL_weakz00_1669 = 1L; }  else 
{ /* Llib/hash.scm 245 */
if(
(BgL_weakz00_50==BGl_symbol3905z00zz__hashz00))
{ /* Llib/hash.scm 245 */
BgL_weakz00_1669 = 2L; }  else 
{ /* Llib/hash.scm 245 */
if(
(BgL_weakz00_50==BGl_symbol3907z00zz__hashz00))
{ /* Llib/hash.scm 245 */
BgL_weakz00_1669 = 3L; }  else 
{ /* Llib/hash.scm 245 */
if(
(BgL_weakz00_50==BGl_symbol3879z00zz__hashz00))
{ /* Llib/hash.scm 245 */
BgL_weakz00_1669 = 0L; }  else 
{ /* Llib/hash.scm 245 */
if(
(BgL_weakz00_50==BGl_symbol3909z00zz__hashz00))
{ /* Llib/hash.scm 245 */
BgL_weakz00_1669 = 8L; }  else 
{ /* Llib/hash.scm 245 */
if(
(BgL_weakz00_50==BGl_symbol3911z00zz__hashz00))
{ /* Llib/hash.scm 245 */
BgL_weakz00_1669 = 4L; }  else 
{ /* Llib/hash.scm 245 */
if(
CBOOL(BgL_weakz00_50))
{ /* Llib/hash.scm 252 */
BgL_weakz00_1669 = 2L; }  else 
{ /* Llib/hash.scm 252 */
BgL_weakz00_1669 = 0L; } } } } } } } 
if(
CBOOL(BgL_persistentz00_48))
{ /* Llib/hash.scm 253 */
if(
CBOOL(BgL_hashz00_45))
{ /* Llib/hash.scm 254 */
BGl_errorz00zz__errorz00(BGl_string3899z00zz__hashz00, BGl_string3913z00zz__hashz00, BgL_hashz00_45); }  else 
{ /* Llib/hash.scm 254 */
BgL_hashz00_45 = BGl_symbol3869z00zz__hashz00; } }  else 
{ /* Llib/hash.scm 253 */BFALSE; } 
{ /* Llib/hash.scm 259 */
 bool_t BgL_test4428z00_7596;
if(
(
BINT(BgL_weakz00_1669)==
BINT(8L)))
{ /* Llib/hash.scm 259 */
BgL_test4428z00_7596 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 259 */
BgL_test4428z00_7596 = 
(
BINT(BgL_weakz00_1669)==
BINT(4L))
; } 
if(BgL_test4428z00_7596)
{ /* Llib/hash.scm 259 */
if(
CBOOL(BgL_eqtestz00_44))
{ /* Llib/hash.scm 261 */
return 
BGl_errorz00zz__errorz00(BGl_string3899z00zz__hashz00, BGl_string3914z00zz__hashz00, BgL_eqtestz00_44);}  else 
{ /* Llib/hash.scm 261 */
if(
CBOOL(BgL_hashz00_45))
{ /* Llib/hash.scm 264 */
return 
BGl_errorz00zz__errorz00(BGl_string3899z00zz__hashz00, BGl_string3915z00zz__hashz00, BgL_hashz00_45);}  else 
{ /* Llib/hash.scm 264 */
if(
(
BINT(BgL_weakz00_1669)==
BINT(8L)))
{ /* Llib/hash.scm 268 */
 obj_t BgL_arg1416z00_1671;
{ /* Llib/hash.scm 268 */
 long BgL_arg1417z00_1672;
{ /* Llib/hash.scm 268 */
 long BgL_za72za7_3369;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7614;
if(
INTEGERP(BgL_siza7eza7_49))
{ /* Llib/hash.scm 268 */
BgL_tmpz00_7614 = BgL_siza7eza7_49
; }  else 
{ 
 obj_t BgL_auxz00_7617;
BgL_auxz00_7617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9777L), BGl_string3899z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_49); 
FAILURE(BgL_auxz00_7617,BFALSE,BFALSE);} 
BgL_za72za7_3369 = 
(long)CINT(BgL_tmpz00_7614); } 
BgL_arg1417z00_1672 = 
(3L*BgL_za72za7_3369); } 
BgL_arg1416z00_1671 = 
make_vector(BgL_arg1417z00_1672, BFALSE); } 
{ /* Llib/hash.scm 268 */
 obj_t BgL_newz00_3370;
BgL_newz00_3370 = 
create_struct(BGl_symbol3857z00zz__hashz00, 
(int)(8L)); 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4434z00_7626;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7627;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2514z00_3374;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2982z00_5850;
BgL_aux2982z00_5850 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2982z00_5850))
{ /* Llib/hash.scm 268 */
BgL_res2514z00_3374 = BgL_aux2982z00_5850; }  else 
{ 
 obj_t BgL_auxz00_7631;
BgL_auxz00_7631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2982z00_5850); 
FAILURE(BgL_auxz00_7631,BFALSE,BFALSE);} } 
BgL_tmpz00_7627 = BgL_res2514z00_3374; } 
BgL_test4434z00_7626 = 
(BgL_tmpz00_7627==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4434z00_7626)
{ /* Llib/hash.scm 268 */
 obj_t BgL_auxz00_7638; int BgL_tmpz00_7636;
BgL_auxz00_7638 = 
BINT(0L); 
BgL_tmpz00_7636 = 
(int)(7L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7636, BgL_auxz00_7638); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4436z00_7642;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7643;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2515z00_3378;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2984z00_5852;
BgL_aux2984z00_5852 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2984z00_5852))
{ /* Llib/hash.scm 268 */
BgL_res2515z00_3378 = BgL_aux2984z00_5852; }  else 
{ 
 obj_t BgL_auxz00_7647;
BgL_auxz00_7647 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2984z00_5852); 
FAILURE(BgL_auxz00_7647,BFALSE,BFALSE);} } 
BgL_tmpz00_7643 = BgL_res2515z00_3378; } 
BgL_test4436z00_7642 = 
(BgL_tmpz00_7643==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4436z00_7642)
{ /* Llib/hash.scm 268 */
 obj_t BgL_auxz00_7654; int BgL_tmpz00_7652;
BgL_auxz00_7654 = 
BINT(0L); 
BgL_tmpz00_7652 = 
(int)(6L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7652, BgL_auxz00_7654); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4438z00_7658;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7659;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2516z00_3382;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2986z00_5854;
BgL_aux2986z00_5854 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2986z00_5854))
{ /* Llib/hash.scm 268 */
BgL_res2516z00_3382 = BgL_aux2986z00_5854; }  else 
{ 
 obj_t BgL_auxz00_7663;
BgL_auxz00_7663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2986z00_5854); 
FAILURE(BgL_auxz00_7663,BFALSE,BFALSE);} } 
BgL_tmpz00_7659 = BgL_res2516z00_3382; } 
BgL_test4438z00_7658 = 
(BgL_tmpz00_7659==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4438z00_7658)
{ /* Llib/hash.scm 268 */
 obj_t BgL_auxz00_7670; int BgL_tmpz00_7668;
BgL_auxz00_7670 = 
BINT(BgL_weakz00_1669); 
BgL_tmpz00_7668 = 
(int)(5L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7668, BgL_auxz00_7670); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4440z00_7674;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7675;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2517z00_3386;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2988z00_5856;
BgL_aux2988z00_5856 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2988z00_5856))
{ /* Llib/hash.scm 268 */
BgL_res2517z00_3386 = BgL_aux2988z00_5856; }  else 
{ 
 obj_t BgL_auxz00_7679;
BgL_auxz00_7679 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2988z00_5856); 
FAILURE(BgL_auxz00_7679,BFALSE,BFALSE);} } 
BgL_tmpz00_7675 = BgL_res2517z00_3386; } 
BgL_test4440z00_7674 = 
(BgL_tmpz00_7675==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4440z00_7674)
{ /* Llib/hash.scm 268 */
 int BgL_tmpz00_7684;
BgL_tmpz00_7684 = 
(int)(4L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7684, BGl_listzd2envzd2zz__r4_pairs_and_lists_6_3z00); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4442z00_7688;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7689;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2518z00_3390;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2990z00_5858;
BgL_aux2990z00_5858 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2990z00_5858))
{ /* Llib/hash.scm 268 */
BgL_res2518z00_3390 = BgL_aux2990z00_5858; }  else 
{ 
 obj_t BgL_auxz00_7693;
BgL_auxz00_7693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2990z00_5858); 
FAILURE(BgL_auxz00_7693,BFALSE,BFALSE);} } 
BgL_tmpz00_7689 = BgL_res2518z00_3390; } 
BgL_test4442z00_7688 = 
(BgL_tmpz00_7689==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4442z00_7688)
{ /* Llib/hash.scm 268 */
 int BgL_tmpz00_7698;
BgL_tmpz00_7698 = 
(int)(3L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7698, BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4444z00_7702;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7703;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2519z00_3394;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2992z00_5860;
BgL_aux2992z00_5860 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2992z00_5860))
{ /* Llib/hash.scm 268 */
BgL_res2519z00_3394 = BgL_aux2992z00_5860; }  else 
{ 
 obj_t BgL_auxz00_7707;
BgL_auxz00_7707 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2992z00_5860); 
FAILURE(BgL_auxz00_7707,BFALSE,BFALSE);} } 
BgL_tmpz00_7703 = BgL_res2519z00_3394; } 
BgL_test4444z00_7702 = 
(BgL_tmpz00_7703==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4444z00_7702)
{ /* Llib/hash.scm 268 */
 int BgL_tmpz00_7712;
BgL_tmpz00_7712 = 
(int)(2L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7712, BgL_arg1416z00_1671); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4446z00_7716;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7717;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2520z00_3398;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2994z00_5862;
BgL_aux2994z00_5862 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2994z00_5862))
{ /* Llib/hash.scm 268 */
BgL_res2520z00_3398 = BgL_aux2994z00_5862; }  else 
{ 
 obj_t BgL_auxz00_7721;
BgL_auxz00_7721 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2994z00_5862); 
FAILURE(BgL_auxz00_7721,BFALSE,BFALSE);} } 
BgL_tmpz00_7717 = BgL_res2520z00_3398; } 
BgL_test4446z00_7716 = 
(BgL_tmpz00_7717==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4446z00_7716)
{ /* Llib/hash.scm 268 */
 int BgL_tmpz00_7726;
BgL_tmpz00_7726 = 
(int)(1L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7726, BgL_siza7eza7_49); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
{ /* Llib/hash.scm 268 */
 bool_t BgL_test4448z00_7730;
{ /* Llib/hash.scm 268 */
 obj_t BgL_tmpz00_7731;
{ /* Llib/hash.scm 268 */
 obj_t BgL_res2521z00_3402;
{ /* Llib/hash.scm 268 */
 obj_t BgL_aux2996z00_5864;
BgL_aux2996z00_5864 = 
STRUCT_KEY(BgL_newz00_3370); 
if(
SYMBOLP(BgL_aux2996z00_5864))
{ /* Llib/hash.scm 268 */
BgL_res2521z00_3402 = BgL_aux2996z00_5864; }  else 
{ 
 obj_t BgL_auxz00_7735;
BgL_auxz00_7735 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9738L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2996z00_5864); 
FAILURE(BgL_auxz00_7735,BFALSE,BFALSE);} } 
BgL_tmpz00_7731 = BgL_res2521z00_3402; } 
BgL_test4448z00_7730 = 
(BgL_tmpz00_7731==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4448z00_7730)
{ /* Llib/hash.scm 268 */
 obj_t BgL_auxz00_7742; int BgL_tmpz00_7740;
BgL_auxz00_7742 = 
BINT(0L); 
BgL_tmpz00_7740 = 
(int)(0L); 
STRUCT_SET(BgL_newz00_3370, BgL_tmpz00_7740, BgL_auxz00_7742); }  else 
{ /* Llib/hash.scm 268 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3370); } } 
return BgL_newz00_3370;} }  else 
{ /* Llib/hash.scm 270 */
 obj_t BgL_arg1418z00_1673;
{ /* Llib/hash.scm 270 */
 long BgL_tmpz00_7746;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7747;
if(
INTEGERP(BgL_siza7eza7_49))
{ /* Llib/hash.scm 270 */
BgL_tmpz00_7747 = BgL_siza7eza7_49
; }  else 
{ 
 obj_t BgL_auxz00_7750;
BgL_auxz00_7750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9858L), BGl_string3899z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_49); 
FAILURE(BgL_auxz00_7750,BFALSE,BFALSE);} 
BgL_tmpz00_7746 = 
(long)CINT(BgL_tmpz00_7747); } 
BgL_arg1418z00_1673 = 
make_vector(BgL_tmpz00_7746, BNIL); } 
{ /* Llib/hash.scm 270 */
 obj_t BgL_newz00_3405;
BgL_newz00_3405 = 
create_struct(BGl_symbol3857z00zz__hashz00, 
(int)(8L)); 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4451z00_7758;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7759;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2522z00_3409;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux2999z00_5867;
BgL_aux2999z00_5867 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux2999z00_5867))
{ /* Llib/hash.scm 270 */
BgL_res2522z00_3409 = BgL_aux2999z00_5867; }  else 
{ 
 obj_t BgL_auxz00_7763;
BgL_auxz00_7763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux2999z00_5867); 
FAILURE(BgL_auxz00_7763,BFALSE,BFALSE);} } 
BgL_tmpz00_7759 = BgL_res2522z00_3409; } 
BgL_test4451z00_7758 = 
(BgL_tmpz00_7759==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4451z00_7758)
{ /* Llib/hash.scm 270 */
 int BgL_tmpz00_7768;
BgL_tmpz00_7768 = 
(int)(7L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7768, BgL_bucketzd2expansionzd2_43); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4453z00_7772;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7773;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2523z00_3413;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3001z00_5869;
BgL_aux3001z00_5869 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3001z00_5869))
{ /* Llib/hash.scm 270 */
BgL_res2523z00_3413 = BgL_aux3001z00_5869; }  else 
{ 
 obj_t BgL_auxz00_7777;
BgL_auxz00_7777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3001z00_5869); 
FAILURE(BgL_auxz00_7777,BFALSE,BFALSE);} } 
BgL_tmpz00_7773 = BgL_res2523z00_3413; } 
BgL_test4453z00_7772 = 
(BgL_tmpz00_7773==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4453z00_7772)
{ /* Llib/hash.scm 270 */
 int BgL_tmpz00_7782;
BgL_tmpz00_7782 = 
(int)(6L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7782, BgL_maxzd2lengthzd2_47); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4455z00_7786;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7787;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2524z00_3417;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3003z00_5871;
BgL_aux3003z00_5871 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3003z00_5871))
{ /* Llib/hash.scm 270 */
BgL_res2524z00_3417 = BgL_aux3003z00_5871; }  else 
{ 
 obj_t BgL_auxz00_7791;
BgL_auxz00_7791 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3003z00_5871); 
FAILURE(BgL_auxz00_7791,BFALSE,BFALSE);} } 
BgL_tmpz00_7787 = BgL_res2524z00_3417; } 
BgL_test4455z00_7786 = 
(BgL_tmpz00_7787==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4455z00_7786)
{ /* Llib/hash.scm 270 */
 obj_t BgL_auxz00_7798; int BgL_tmpz00_7796;
BgL_auxz00_7798 = 
BINT(BgL_weakz00_1669); 
BgL_tmpz00_7796 = 
(int)(5L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7796, BgL_auxz00_7798); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4457z00_7802;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7803;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2525z00_3421;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3005z00_5873;
BgL_aux3005z00_5873 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3005z00_5873))
{ /* Llib/hash.scm 270 */
BgL_res2525z00_3421 = BgL_aux3005z00_5873; }  else 
{ 
 obj_t BgL_auxz00_7807;
BgL_auxz00_7807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3005z00_5873); 
FAILURE(BgL_auxz00_7807,BFALSE,BFALSE);} } 
BgL_tmpz00_7803 = BgL_res2525z00_3421; } 
BgL_test4457z00_7802 = 
(BgL_tmpz00_7803==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4457z00_7802)
{ /* Llib/hash.scm 270 */
 int BgL_tmpz00_7812;
BgL_tmpz00_7812 = 
(int)(4L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7812, BGl_proc3916z00zz__hashz00); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4459z00_7816;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7817;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2526z00_3425;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3007z00_5875;
BgL_aux3007z00_5875 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3007z00_5875))
{ /* Llib/hash.scm 270 */
BgL_res2526z00_3425 = BgL_aux3007z00_5875; }  else 
{ 
 obj_t BgL_auxz00_7821;
BgL_auxz00_7821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3007z00_5875); 
FAILURE(BgL_auxz00_7821,BFALSE,BFALSE);} } 
BgL_tmpz00_7817 = BgL_res2526z00_3425; } 
BgL_test4459z00_7816 = 
(BgL_tmpz00_7817==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4459z00_7816)
{ /* Llib/hash.scm 270 */
 int BgL_tmpz00_7826;
BgL_tmpz00_7826 = 
(int)(3L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7826, BGl_stringzd3zf3zd2envzf2zz__r4_strings_6_7z00); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4461z00_7830;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7831;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2527z00_3429;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3009z00_5877;
BgL_aux3009z00_5877 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3009z00_5877))
{ /* Llib/hash.scm 270 */
BgL_res2527z00_3429 = BgL_aux3009z00_5877; }  else 
{ 
 obj_t BgL_auxz00_7835;
BgL_auxz00_7835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3009z00_5877); 
FAILURE(BgL_auxz00_7835,BFALSE,BFALSE);} } 
BgL_tmpz00_7831 = BgL_res2527z00_3429; } 
BgL_test4461z00_7830 = 
(BgL_tmpz00_7831==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4461z00_7830)
{ /* Llib/hash.scm 270 */
 int BgL_tmpz00_7840;
BgL_tmpz00_7840 = 
(int)(2L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7840, BgL_arg1418z00_1673); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4463z00_7844;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7845;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2528z00_3433;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3011z00_5879;
BgL_aux3011z00_5879 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3011z00_5879))
{ /* Llib/hash.scm 270 */
BgL_res2528z00_3433 = BgL_aux3011z00_5879; }  else 
{ 
 obj_t BgL_auxz00_7849;
BgL_auxz00_7849 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3011z00_5879); 
FAILURE(BgL_auxz00_7849,BFALSE,BFALSE);} } 
BgL_tmpz00_7845 = BgL_res2528z00_3433; } 
BgL_test4463z00_7844 = 
(BgL_tmpz00_7845==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4463z00_7844)
{ /* Llib/hash.scm 270 */
 int BgL_tmpz00_7854;
BgL_tmpz00_7854 = 
(int)(1L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7854, BgL_maxzd2bucketzd2lengthz00_46); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
{ /* Llib/hash.scm 270 */
 bool_t BgL_test4465z00_7858;
{ /* Llib/hash.scm 270 */
 obj_t BgL_tmpz00_7859;
{ /* Llib/hash.scm 270 */
 obj_t BgL_res2529z00_3437;
{ /* Llib/hash.scm 270 */
 obj_t BgL_aux3013z00_5881;
BgL_aux3013z00_5881 = 
STRUCT_KEY(BgL_newz00_3405); 
if(
SYMBOLP(BgL_aux3013z00_5881))
{ /* Llib/hash.scm 270 */
BgL_res2529z00_3437 = BgL_aux3013z00_5881; }  else 
{ 
 obj_t BgL_auxz00_7863;
BgL_auxz00_7863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9826L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3013z00_5881); 
FAILURE(BgL_auxz00_7863,BFALSE,BFALSE);} } 
BgL_tmpz00_7859 = BgL_res2529z00_3437; } 
BgL_test4465z00_7858 = 
(BgL_tmpz00_7859==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4465z00_7858)
{ /* Llib/hash.scm 270 */
 obj_t BgL_auxz00_7870; int BgL_tmpz00_7868;
BgL_auxz00_7870 = 
BINT(0L); 
BgL_tmpz00_7868 = 
(int)(0L); 
STRUCT_SET(BgL_newz00_3405, BgL_tmpz00_7868, BgL_auxz00_7870); }  else 
{ /* Llib/hash.scm 270 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3405); } } 
return BgL_newz00_3405;} } } } }  else 
{ /* Llib/hash.scm 274 */
 obj_t BgL_arg1422z00_1679;
{ /* Llib/hash.scm 274 */
 long BgL_tmpz00_7874;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7875;
if(
INTEGERP(BgL_siza7eza7_49))
{ /* Llib/hash.scm 274 */
BgL_tmpz00_7875 = BgL_siza7eza7_49
; }  else 
{ 
 obj_t BgL_auxz00_7878;
BgL_auxz00_7878 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(10020L), BGl_string3899z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_49); 
FAILURE(BgL_auxz00_7878,BFALSE,BFALSE);} 
BgL_tmpz00_7874 = 
(long)CINT(BgL_tmpz00_7875); } 
BgL_arg1422z00_1679 = 
make_vector(BgL_tmpz00_7874, BNIL); } 
{ /* Llib/hash.scm 274 */
 obj_t BgL_hashnz00_3438;
BgL_hashnz00_3438 = BgL_hashz00_45; 
{ /* Llib/hash.scm 274 */
 obj_t BgL_newz00_3439;
BgL_newz00_3439 = 
create_struct(BGl_symbol3857z00zz__hashz00, 
(int)(8L)); 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4468z00_7886;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7887;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2530z00_3443;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3016z00_5884;
BgL_aux3016z00_5884 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3016z00_5884))
{ /* Llib/hash.scm 274 */
BgL_res2530z00_3443 = BgL_aux3016z00_5884; }  else 
{ 
 obj_t BgL_auxz00_7891;
BgL_auxz00_7891 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3016z00_5884); 
FAILURE(BgL_auxz00_7891,BFALSE,BFALSE);} } 
BgL_tmpz00_7887 = BgL_res2530z00_3443; } 
BgL_test4468z00_7886 = 
(BgL_tmpz00_7887==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4468z00_7886)
{ /* Llib/hash.scm 274 */
 int BgL_tmpz00_7896;
BgL_tmpz00_7896 = 
(int)(7L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7896, BgL_bucketzd2expansionzd2_43); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4470z00_7900;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7901;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2531z00_3447;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3018z00_5886;
BgL_aux3018z00_5886 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3018z00_5886))
{ /* Llib/hash.scm 274 */
BgL_res2531z00_3447 = BgL_aux3018z00_5886; }  else 
{ 
 obj_t BgL_auxz00_7905;
BgL_auxz00_7905 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3018z00_5886); 
FAILURE(BgL_auxz00_7905,BFALSE,BFALSE);} } 
BgL_tmpz00_7901 = BgL_res2531z00_3447; } 
BgL_test4470z00_7900 = 
(BgL_tmpz00_7901==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4470z00_7900)
{ /* Llib/hash.scm 274 */
 int BgL_tmpz00_7910;
BgL_tmpz00_7910 = 
(int)(6L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7910, BgL_maxzd2lengthzd2_47); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4472z00_7914;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7915;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2532z00_3451;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3020z00_5888;
BgL_aux3020z00_5888 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3020z00_5888))
{ /* Llib/hash.scm 274 */
BgL_res2532z00_3451 = BgL_aux3020z00_5888; }  else 
{ 
 obj_t BgL_auxz00_7919;
BgL_auxz00_7919 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3020z00_5888); 
FAILURE(BgL_auxz00_7919,BFALSE,BFALSE);} } 
BgL_tmpz00_7915 = BgL_res2532z00_3451; } 
BgL_test4472z00_7914 = 
(BgL_tmpz00_7915==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4472z00_7914)
{ /* Llib/hash.scm 274 */
 obj_t BgL_auxz00_7926; int BgL_tmpz00_7924;
BgL_auxz00_7926 = 
BINT(BgL_weakz00_1669); 
BgL_tmpz00_7924 = 
(int)(5L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7924, BgL_auxz00_7926); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4474z00_7930;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7931;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2533z00_3455;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3022z00_5890;
BgL_aux3022z00_5890 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3022z00_5890))
{ /* Llib/hash.scm 274 */
BgL_res2533z00_3455 = BgL_aux3022z00_5890; }  else 
{ 
 obj_t BgL_auxz00_7935;
BgL_auxz00_7935 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3022z00_5890); 
FAILURE(BgL_auxz00_7935,BFALSE,BFALSE);} } 
BgL_tmpz00_7931 = BgL_res2533z00_3455; } 
BgL_test4474z00_7930 = 
(BgL_tmpz00_7931==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4474z00_7930)
{ /* Llib/hash.scm 274 */
 int BgL_tmpz00_7940;
BgL_tmpz00_7940 = 
(int)(4L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7940, BgL_hashnz00_3438); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4476z00_7944;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7945;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2534z00_3459;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3024z00_5892;
BgL_aux3024z00_5892 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3024z00_5892))
{ /* Llib/hash.scm 274 */
BgL_res2534z00_3459 = BgL_aux3024z00_5892; }  else 
{ 
 obj_t BgL_auxz00_7949;
BgL_auxz00_7949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3024z00_5892); 
FAILURE(BgL_auxz00_7949,BFALSE,BFALSE);} } 
BgL_tmpz00_7945 = BgL_res2534z00_3459; } 
BgL_test4476z00_7944 = 
(BgL_tmpz00_7945==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4476z00_7944)
{ /* Llib/hash.scm 274 */
 int BgL_tmpz00_7954;
BgL_tmpz00_7954 = 
(int)(3L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7954, BgL_eqtestz00_44); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4478z00_7958;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7959;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2535z00_3463;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3026z00_5894;
BgL_aux3026z00_5894 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3026z00_5894))
{ /* Llib/hash.scm 274 */
BgL_res2535z00_3463 = BgL_aux3026z00_5894; }  else 
{ 
 obj_t BgL_auxz00_7963;
BgL_auxz00_7963 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3026z00_5894); 
FAILURE(BgL_auxz00_7963,BFALSE,BFALSE);} } 
BgL_tmpz00_7959 = BgL_res2535z00_3463; } 
BgL_test4478z00_7958 = 
(BgL_tmpz00_7959==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4478z00_7958)
{ /* Llib/hash.scm 274 */
 int BgL_tmpz00_7968;
BgL_tmpz00_7968 = 
(int)(2L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7968, BgL_arg1422z00_1679); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4480z00_7972;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7973;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2536z00_3467;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3028z00_5896;
BgL_aux3028z00_5896 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3028z00_5896))
{ /* Llib/hash.scm 274 */
BgL_res2536z00_3467 = BgL_aux3028z00_5896; }  else 
{ 
 obj_t BgL_auxz00_7977;
BgL_auxz00_7977 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3028z00_5896); 
FAILURE(BgL_auxz00_7977,BFALSE,BFALSE);} } 
BgL_tmpz00_7973 = BgL_res2536z00_3467; } 
BgL_test4480z00_7972 = 
(BgL_tmpz00_7973==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4480z00_7972)
{ /* Llib/hash.scm 274 */
 int BgL_tmpz00_7982;
BgL_tmpz00_7982 = 
(int)(1L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7982, BgL_maxzd2bucketzd2lengthz00_46); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
{ /* Llib/hash.scm 274 */
 bool_t BgL_test4482z00_7986;
{ /* Llib/hash.scm 274 */
 obj_t BgL_tmpz00_7987;
{ /* Llib/hash.scm 274 */
 obj_t BgL_res2537z00_3471;
{ /* Llib/hash.scm 274 */
 obj_t BgL_aux3030z00_5898;
BgL_aux3030z00_5898 = 
STRUCT_KEY(BgL_newz00_3439); 
if(
SYMBOLP(BgL_aux3030z00_5898))
{ /* Llib/hash.scm 274 */
BgL_res2537z00_3471 = BgL_aux3030z00_5898; }  else 
{ 
 obj_t BgL_auxz00_7991;
BgL_auxz00_7991 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9988L), BGl_string3899z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3030z00_5898); 
FAILURE(BgL_auxz00_7991,BFALSE,BFALSE);} } 
BgL_tmpz00_7987 = BgL_res2537z00_3471; } 
BgL_test4482z00_7986 = 
(BgL_tmpz00_7987==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4482z00_7986)
{ /* Llib/hash.scm 274 */
 obj_t BgL_auxz00_7998; int BgL_tmpz00_7996;
BgL_auxz00_7998 = 
BINT(0L); 
BgL_tmpz00_7996 = 
(int)(0L); 
STRUCT_SET(BgL_newz00_3439, BgL_tmpz00_7996, BgL_auxz00_7998); }  else 
{ /* Llib/hash.scm 274 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_newz00_3439); } } 
return BgL_newz00_3439;} } } } } } 

}



/* &<@anonymous:1420> */
obj_t BGl_z62zc3z04anonymousza31420ze3ze5zz__hashz00(obj_t BgL_envz00_5419, obj_t BgL_sz00_5420)
{
{ /* Llib/hash.scm 272 */
{ /* Llib/hash.scm 272 */
 long BgL_tmpz00_8002;
{ /* Llib/hash.scm 272 */
 long BgL_arg1421z00_6800;
{ /* Llib/hash.scm 272 */
 obj_t BgL_stringz00_6801;
if(
STRINGP(BgL_sz00_5420))
{ /* Llib/hash.scm 272 */
BgL_stringz00_6801 = BgL_sz00_5420; }  else 
{ 
 obj_t BgL_auxz00_8005;
BgL_auxz00_8005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9926L), BGl_string3917z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_sz00_5420); 
FAILURE(BgL_auxz00_8005,BFALSE,BFALSE);} 
BgL_arg1421z00_6800 = 
STRING_LENGTH(BgL_stringz00_6801); } 
{ /* Llib/hash.scm 272 */
 char * BgL_tmpz00_8010;
{ /* Llib/hash.scm 272 */
 obj_t BgL_tmpz00_8011;
if(
STRINGP(BgL_sz00_5420))
{ /* Llib/hash.scm 272 */
BgL_tmpz00_8011 = BgL_sz00_5420
; }  else 
{ 
 obj_t BgL_auxz00_8014;
BgL_auxz00_8014 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(9922L), BGl_string3917z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_sz00_5420); 
FAILURE(BgL_auxz00_8014,BFALSE,BFALSE);} 
BgL_tmpz00_8010 = 
BSTRING_TO_STRING(BgL_tmpz00_8011); } 
BgL_tmpz00_8002 = 
bgl_string_hash(BgL_tmpz00_8010, 
(int)(0L), 
(int)(BgL_arg1421z00_6800)); } } 
return 
BINT(BgL_tmpz00_8002);} } 

}



/* hashtable? */
BGL_EXPORTED_DEF bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t BgL_objz00_53)
{
{ /* Llib/hash.scm 281 */
if(
STRUCTP(BgL_objz00_53))
{ /* Llib/hash.scm 282 */
 obj_t BgL_tmpz00_8025;
{ /* Llib/hash.scm 282 */
 obj_t BgL_res2538z00_3475;
{ /* Llib/hash.scm 282 */
 obj_t BgL_aux3036z00_5904;
BgL_aux3036z00_5904 = 
STRUCT_KEY(BgL_objz00_53); 
if(
SYMBOLP(BgL_aux3036z00_5904))
{ /* Llib/hash.scm 282 */
BgL_res2538z00_3475 = BgL_aux3036z00_5904; }  else 
{ 
 obj_t BgL_auxz00_8029;
BgL_auxz00_8029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(10366L), BGl_string3919z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3036z00_5904); 
FAILURE(BgL_auxz00_8029,BFALSE,BFALSE);} } 
BgL_tmpz00_8025 = BgL_res2538z00_3475; } 
return 
(BgL_tmpz00_8025==BGl_symbol3857z00zz__hashz00);}  else 
{ /* Llib/hash.scm 282 */
return ((bool_t)0);} } 

}



/* &hashtable? */
obj_t BGl_z62hashtablezf3z91zz__hashz00(obj_t BgL_envz00_5424, obj_t BgL_objz00_5425)
{
{ /* Llib/hash.scm 281 */
return 
BBOOL(
BGl_hashtablezf3zf3zz__hashz00(BgL_objz00_5425));} 

}



/* hashtable-weak? */
bool_t BGl_hashtablezd2weakzf3z21zz__hashz00(obj_t BgL_tablez00_54)
{
{ /* Llib/hash.scm 287 */
{ /* Llib/hash.scm 288 */
 bool_t BgL_test4488z00_8036;
{ /* Llib/hash.scm 288 */
 long BgL_arg1434z00_1690;
{ /* Llib/hash.scm 288 */
 obj_t BgL_arg1435z00_1691;
{ /* Llib/hash.scm 288 */
 bool_t BgL_test4489z00_8037;
{ /* Llib/hash.scm 288 */
 obj_t BgL_tmpz00_8038;
{ /* Llib/hash.scm 288 */
 obj_t BgL_res2539z00_3479;
{ /* Llib/hash.scm 288 */
 obj_t BgL_aux3038z00_5906;
BgL_aux3038z00_5906 = 
STRUCT_KEY(BgL_tablez00_54); 
if(
SYMBOLP(BgL_aux3038z00_5906))
{ /* Llib/hash.scm 288 */
BgL_res2539z00_3479 = BgL_aux3038z00_5906; }  else 
{ 
 obj_t BgL_auxz00_8042;
BgL_auxz00_8042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(10678L), BGl_string3920z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3038z00_5906); 
FAILURE(BgL_auxz00_8042,BFALSE,BFALSE);} } 
BgL_tmpz00_8038 = BgL_res2539z00_3479; } 
BgL_test4489z00_8037 = 
(BgL_tmpz00_8038==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4489z00_8037)
{ /* Llib/hash.scm 288 */
 int BgL_tmpz00_8047;
BgL_tmpz00_8047 = 
(int)(5L); 
BgL_arg1435z00_1691 = 
STRUCT_REF(BgL_tablez00_54, BgL_tmpz00_8047); }  else 
{ /* Llib/hash.scm 288 */
BgL_arg1435z00_1691 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_54); } } 
{ /* Llib/hash.scm 288 */
 long BgL_xz00_3480;
{ /* Llib/hash.scm 288 */
 obj_t BgL_tmpz00_8051;
if(
INTEGERP(BgL_arg1435z00_1691))
{ /* Llib/hash.scm 288 */
BgL_tmpz00_8051 = BgL_arg1435z00_1691
; }  else 
{ 
 obj_t BgL_auxz00_8054;
BgL_auxz00_8054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(10700L), BGl_string3920z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1435z00_1691); 
FAILURE(BgL_auxz00_8054,BFALSE,BFALSE);} 
BgL_xz00_3480 = 
(long)CINT(BgL_tmpz00_8051); } 
BgL_arg1434z00_1690 = 
(BgL_xz00_3480 & 3L); } } 
BgL_test4488z00_8036 = 
(0L==BgL_arg1434z00_1690); } 
if(BgL_test4488z00_8036)
{ /* Llib/hash.scm 288 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 288 */
return ((bool_t)1);} } } 

}



/* hashtable-open-string? */
bool_t BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(obj_t BgL_tablez00_55)
{
{ /* Llib/hash.scm 293 */
{ /* Llib/hash.scm 294 */
 bool_t BgL_test4492z00_8061;
{ /* Llib/hash.scm 294 */
 long BgL_arg1439z00_1695;
{ /* Llib/hash.scm 294 */
 obj_t BgL_arg1440z00_1696;
{ /* Llib/hash.scm 294 */
 bool_t BgL_test4493z00_8062;
{ /* Llib/hash.scm 294 */
 obj_t BgL_tmpz00_8063;
{ /* Llib/hash.scm 294 */
 obj_t BgL_res2540z00_3485;
{ /* Llib/hash.scm 294 */
 obj_t BgL_aux3041z00_5909;
BgL_aux3041z00_5909 = 
STRUCT_KEY(BgL_tablez00_55); 
if(
SYMBOLP(BgL_aux3041z00_5909))
{ /* Llib/hash.scm 294 */
BgL_res2540z00_3485 = BgL_aux3041z00_5909; }  else 
{ 
 obj_t BgL_auxz00_8067;
BgL_auxz00_8067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11018L), BGl_string3921z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3041z00_5909); 
FAILURE(BgL_auxz00_8067,BFALSE,BFALSE);} } 
BgL_tmpz00_8063 = BgL_res2540z00_3485; } 
BgL_test4493z00_8062 = 
(BgL_tmpz00_8063==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4493z00_8062)
{ /* Llib/hash.scm 294 */
 int BgL_tmpz00_8072;
BgL_tmpz00_8072 = 
(int)(5L); 
BgL_arg1440z00_1696 = 
STRUCT_REF(BgL_tablez00_55, BgL_tmpz00_8072); }  else 
{ /* Llib/hash.scm 294 */
BgL_arg1440z00_1696 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_55); } } 
{ /* Llib/hash.scm 294 */
 long BgL_xz00_3486;
{ /* Llib/hash.scm 294 */
 obj_t BgL_tmpz00_8076;
if(
INTEGERP(BgL_arg1440z00_1696))
{ /* Llib/hash.scm 294 */
BgL_tmpz00_8076 = BgL_arg1440z00_1696
; }  else 
{ 
 obj_t BgL_auxz00_8079;
BgL_auxz00_8079 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11040L), BGl_string3921z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1440z00_1696); 
FAILURE(BgL_auxz00_8079,BFALSE,BFALSE);} 
BgL_xz00_3486 = 
(long)CINT(BgL_tmpz00_8076); } 
BgL_arg1439z00_1695 = 
(BgL_xz00_3486 & 8L); } } 
BgL_test4492z00_8061 = 
(0L==BgL_arg1439z00_1695); } 
if(BgL_test4492z00_8061)
{ /* Llib/hash.scm 294 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 294 */
return ((bool_t)1);} } } 

}



/* hashtable-weak-keys? */
BGL_EXPORTED_DEF bool_t BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(obj_t BgL_tablez00_57)
{
{ /* Llib/hash.scm 305 */
{ /* Llib/hash.scm 306 */
 bool_t BgL_test4496z00_8086;
{ /* Llib/hash.scm 306 */
 long BgL_arg1445z00_1701;
{ /* Llib/hash.scm 306 */
 obj_t BgL_arg1446z00_1702;
{ /* Llib/hash.scm 306 */
 bool_t BgL_test4497z00_8087;
{ /* Llib/hash.scm 306 */
 obj_t BgL_tmpz00_8088;
{ /* Llib/hash.scm 306 */
 obj_t BgL_res2542z00_3497;
{ /* Llib/hash.scm 306 */
 obj_t BgL_aux3044z00_5912;
BgL_aux3044z00_5912 = 
STRUCT_KEY(BgL_tablez00_57); 
if(
SYMBOLP(BgL_aux3044z00_5912))
{ /* Llib/hash.scm 306 */
BgL_res2542z00_3497 = BgL_aux3044z00_5912; }  else 
{ 
 obj_t BgL_auxz00_8092;
BgL_auxz00_8092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11694L), BGl_string3922z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3044z00_5912); 
FAILURE(BgL_auxz00_8092,BFALSE,BFALSE);} } 
BgL_tmpz00_8088 = BgL_res2542z00_3497; } 
BgL_test4497z00_8087 = 
(BgL_tmpz00_8088==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4497z00_8087)
{ /* Llib/hash.scm 306 */
 int BgL_tmpz00_8097;
BgL_tmpz00_8097 = 
(int)(5L); 
BgL_arg1446z00_1702 = 
STRUCT_REF(BgL_tablez00_57, BgL_tmpz00_8097); }  else 
{ /* Llib/hash.scm 306 */
BgL_arg1446z00_1702 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_57); } } 
{ /* Llib/hash.scm 306 */
 long BgL_yz00_3498;
{ /* Llib/hash.scm 306 */
 obj_t BgL_tmpz00_8101;
if(
INTEGERP(BgL_arg1446z00_1702))
{ /* Llib/hash.scm 306 */
BgL_tmpz00_8101 = BgL_arg1446z00_1702
; }  else 
{ 
 obj_t BgL_auxz00_8104;
BgL_auxz00_8104 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11716L), BGl_string3922z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1446z00_1702); 
FAILURE(BgL_auxz00_8104,BFALSE,BFALSE);} 
BgL_yz00_3498 = 
(long)CINT(BgL_tmpz00_8101); } 
BgL_arg1445z00_1701 = 
(1L & BgL_yz00_3498); } } 
BgL_test4496z00_8086 = 
(0L==BgL_arg1445z00_1701); } 
if(BgL_test4496z00_8086)
{ /* Llib/hash.scm 306 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 306 */
return ((bool_t)1);} } } 

}



/* &hashtable-weak-keys? */
obj_t BGl_z62hashtablezd2weakzd2keyszf3z91zz__hashz00(obj_t BgL_envz00_5426, obj_t BgL_tablez00_5427)
{
{ /* Llib/hash.scm 305 */
{ /* Llib/hash.scm 306 */
 bool_t BgL_tmpz00_8111;
{ /* Llib/hash.scm 306 */
 obj_t BgL_auxz00_8112;
if(
STRUCTP(BgL_tablez00_5427))
{ /* Llib/hash.scm 306 */
BgL_auxz00_8112 = BgL_tablez00_5427
; }  else 
{ 
 obj_t BgL_auxz00_8115;
BgL_auxz00_8115 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11661L), BGl_string3923z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5427); 
FAILURE(BgL_auxz00_8115,BFALSE,BFALSE);} 
BgL_tmpz00_8111 = 
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_auxz00_8112); } 
return 
BBOOL(BgL_tmpz00_8111);} } 

}



/* hashtable-weak-data? */
BGL_EXPORTED_DEF bool_t BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(obj_t BgL_tablez00_58)
{
{ /* Llib/hash.scm 311 */
{ /* Llib/hash.scm 312 */
 bool_t BgL_test4501z00_8121;
{ /* Llib/hash.scm 312 */
 long BgL_arg1450z00_1706;
{ /* Llib/hash.scm 312 */
 obj_t BgL_arg1451z00_1707;
{ /* Llib/hash.scm 312 */
 bool_t BgL_test4502z00_8122;
{ /* Llib/hash.scm 312 */
 obj_t BgL_tmpz00_8123;
{ /* Llib/hash.scm 312 */
 obj_t BgL_res2543z00_3503;
{ /* Llib/hash.scm 312 */
 obj_t BgL_aux3049z00_5917;
BgL_aux3049z00_5917 = 
STRUCT_KEY(BgL_tablez00_58); 
if(
SYMBOLP(BgL_aux3049z00_5917))
{ /* Llib/hash.scm 312 */
BgL_res2543z00_3503 = BgL_aux3049z00_5917; }  else 
{ 
 obj_t BgL_auxz00_8127;
BgL_auxz00_8127 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12032L), BGl_string3925z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3049z00_5917); 
FAILURE(BgL_auxz00_8127,BFALSE,BFALSE);} } 
BgL_tmpz00_8123 = BgL_res2543z00_3503; } 
BgL_test4502z00_8122 = 
(BgL_tmpz00_8123==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4502z00_8122)
{ /* Llib/hash.scm 312 */
 int BgL_tmpz00_8132;
BgL_tmpz00_8132 = 
(int)(5L); 
BgL_arg1451z00_1707 = 
STRUCT_REF(BgL_tablez00_58, BgL_tmpz00_8132); }  else 
{ /* Llib/hash.scm 312 */
BgL_arg1451z00_1707 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_58); } } 
{ /* Llib/hash.scm 312 */
 long BgL_yz00_3504;
{ /* Llib/hash.scm 312 */
 obj_t BgL_tmpz00_8136;
if(
INTEGERP(BgL_arg1451z00_1707))
{ /* Llib/hash.scm 312 */
BgL_tmpz00_8136 = BgL_arg1451z00_1707
; }  else 
{ 
 obj_t BgL_auxz00_8139;
BgL_auxz00_8139 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12054L), BGl_string3925z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1451z00_1707); 
FAILURE(BgL_auxz00_8139,BFALSE,BFALSE);} 
BgL_yz00_3504 = 
(long)CINT(BgL_tmpz00_8136); } 
BgL_arg1450z00_1706 = 
(2L & BgL_yz00_3504); } } 
BgL_test4501z00_8121 = 
(0L==BgL_arg1450z00_1706); } 
if(BgL_test4501z00_8121)
{ /* Llib/hash.scm 312 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 312 */
return ((bool_t)1);} } } 

}



/* &hashtable-weak-data? */
obj_t BGl_z62hashtablezd2weakzd2datazf3z91zz__hashz00(obj_t BgL_envz00_5428, obj_t BgL_tablez00_5429)
{
{ /* Llib/hash.scm 311 */
{ /* Llib/hash.scm 312 */
 bool_t BgL_tmpz00_8146;
{ /* Llib/hash.scm 312 */
 obj_t BgL_auxz00_8147;
if(
STRUCTP(BgL_tablez00_5429))
{ /* Llib/hash.scm 312 */
BgL_auxz00_8147 = BgL_tablez00_5429
; }  else 
{ 
 obj_t BgL_auxz00_8150;
BgL_auxz00_8150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11999L), BGl_string3926z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5429); 
FAILURE(BgL_auxz00_8150,BFALSE,BFALSE);} 
BgL_tmpz00_8146 = 
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_auxz00_8147); } 
return 
BBOOL(BgL_tmpz00_8146);} } 

}



/* hashtable-size */
BGL_EXPORTED_DEF long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t BgL_tablez00_59)
{
{ /* Llib/hash.scm 317 */
{ /* Llib/hash.scm 318 */
 bool_t BgL_test4506z00_8156;
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8157;
{ /* Llib/hash.scm 318 */
 obj_t BgL_res2544z00_3509;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3054z00_5922;
BgL_aux3054z00_5922 = 
STRUCT_KEY(BgL_tablez00_59); 
if(
SYMBOLP(BgL_aux3054z00_5922))
{ /* Llib/hash.scm 318 */
BgL_res2544z00_3509 = BgL_aux3054z00_5922; }  else 
{ 
 obj_t BgL_auxz00_8161;
BgL_auxz00_8161 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3927z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3054z00_5922); 
FAILURE(BgL_auxz00_8161,BFALSE,BFALSE);} } 
BgL_tmpz00_8157 = BgL_res2544z00_3509; } 
BgL_test4506z00_8156 = 
(BgL_tmpz00_8157==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4506z00_8156)
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8166;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3056z00_5924;
{ /* Llib/hash.scm 318 */
 int BgL_tmpz00_8167;
BgL_tmpz00_8167 = 
(int)(0L); 
BgL_aux3056z00_5924 = 
STRUCT_REF(BgL_tablez00_59, BgL_tmpz00_8167); } 
if(
INTEGERP(BgL_aux3056z00_5924))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8166 = BgL_aux3056z00_5924
; }  else 
{ 
 obj_t BgL_auxz00_8172;
BgL_auxz00_8172 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3927z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3056z00_5924); 
FAILURE(BgL_auxz00_8172,BFALSE,BFALSE);} } 
return 
(long)CINT(BgL_tmpz00_8166);}  else 
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8177;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3057z00_5925;
BgL_aux3057z00_5925 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_59); 
if(
INTEGERP(BgL_aux3057z00_5925))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8177 = BgL_aux3057z00_5925
; }  else 
{ 
 obj_t BgL_auxz00_8181;
BgL_auxz00_8181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3927z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3057z00_5925); 
FAILURE(BgL_auxz00_8181,BFALSE,BFALSE);} } 
return 
(long)CINT(BgL_tmpz00_8177);} } } 

}



/* &hashtable-size */
obj_t BGl_z62hashtablezd2siza7ez17zz__hashz00(obj_t BgL_envz00_5430, obj_t BgL_tablez00_5431)
{
{ /* Llib/hash.scm 317 */
{ /* Llib/hash.scm 318 */
 long BgL_tmpz00_8186;
{ /* Llib/hash.scm 318 */
 obj_t BgL_auxz00_8187;
if(
STRUCTP(BgL_tablez00_5431))
{ /* Llib/hash.scm 318 */
BgL_auxz00_8187 = BgL_tablez00_5431
; }  else 
{ 
 obj_t BgL_auxz00_8190;
BgL_auxz00_8190 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3928z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5431); 
FAILURE(BgL_auxz00_8190,BFALSE,BFALSE);} 
BgL_tmpz00_8186 = 
BGl_hashtablezd2siza7ez75zz__hashz00(BgL_auxz00_8187); } 
return 
BINT(BgL_tmpz00_8186);} } 

}



/* hashtable->vector */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2ze3vectorz31zz__hashz00(obj_t BgL_tablez00_60)
{
{ /* Llib/hash.scm 323 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_60))
{ /* Llib/hash.scm 325 */
BGL_TAIL return 
BGl_openzd2stringzd2hashtablezd2ze3vectorz31zz__hashz00(BgL_tablez00_60);}  else 
{ /* Llib/hash.scm 325 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_60))
{ /* Llib/hash.scm 327 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(BgL_tablez00_60);}  else 
{ /* Llib/hash.scm 327 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2ze3vectorze3zz__hashz00(BgL_tablez00_60);} } } 

}



/* &hashtable->vector */
obj_t BGl_z62hashtablezd2ze3vectorz53zz__hashz00(obj_t BgL_envz00_5432, obj_t BgL_tablez00_5433)
{
{ /* Llib/hash.scm 323 */
{ /* Llib/hash.scm 325 */
 obj_t BgL_auxz00_8203;
if(
STRUCTP(BgL_tablez00_5433))
{ /* Llib/hash.scm 325 */
BgL_auxz00_8203 = BgL_tablez00_5433
; }  else 
{ 
 obj_t BgL_auxz00_8206;
BgL_auxz00_8206 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12636L), BGl_string3929z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5433); 
FAILURE(BgL_auxz00_8206,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2ze3vectorz31zz__hashz00(BgL_auxz00_8203);} } 

}



/* open-string-hashtable->vector */
obj_t BGl_openzd2stringzd2hashtablezd2ze3vectorz31zz__hashz00(obj_t BgL_tablez00_61)
{
{ /* Llib/hash.scm 335 */
{ /* Llib/hash.scm 336 */
 obj_t BgL_siza7eza7_1710;
{ /* Llib/hash.scm 336 */
 bool_t BgL_test4514z00_8211;
{ /* Llib/hash.scm 336 */
 obj_t BgL_tmpz00_8212;
{ /* Llib/hash.scm 336 */
 obj_t BgL_res2545z00_3513;
{ /* Llib/hash.scm 336 */
 obj_t BgL_aux3062z00_5930;
BgL_aux3062z00_5930 = 
STRUCT_KEY(BgL_tablez00_61); 
if(
SYMBOLP(BgL_aux3062z00_5930))
{ /* Llib/hash.scm 336 */
BgL_res2545z00_3513 = BgL_aux3062z00_5930; }  else 
{ 
 obj_t BgL_auxz00_8216;
BgL_auxz00_8216 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13131L), BGl_string3930z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3062z00_5930); 
FAILURE(BgL_auxz00_8216,BFALSE,BFALSE);} } 
BgL_tmpz00_8212 = BgL_res2545z00_3513; } 
BgL_test4514z00_8211 = 
(BgL_tmpz00_8212==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4514z00_8211)
{ /* Llib/hash.scm 336 */
 int BgL_tmpz00_8221;
BgL_tmpz00_8221 = 
(int)(1L); 
BgL_siza7eza7_1710 = 
STRUCT_REF(BgL_tablez00_61, BgL_tmpz00_8221); }  else 
{ /* Llib/hash.scm 336 */
BgL_siza7eza7_1710 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_61); } } 
{ /* Llib/hash.scm 336 */
 long BgL_siza7e3za7_1711;
{ /* Llib/hash.scm 337 */
 long BgL_za72za7_3514;
{ /* Llib/hash.scm 337 */
 obj_t BgL_tmpz00_8225;
if(
INTEGERP(BgL_siza7eza7_1710))
{ /* Llib/hash.scm 337 */
BgL_tmpz00_8225 = BgL_siza7eza7_1710
; }  else 
{ 
 obj_t BgL_auxz00_8228;
BgL_auxz00_8228 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13183L), BGl_string3930z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1710); 
FAILURE(BgL_auxz00_8228,BFALSE,BFALSE);} 
BgL_za72za7_3514 = 
(long)CINT(BgL_tmpz00_8225); } 
BgL_siza7e3za7_1711 = 
(3L*BgL_za72za7_3514); } 
{ /* Llib/hash.scm 337 */
 obj_t BgL_bucketsz00_1712;
{ /* Llib/hash.scm 338 */
 bool_t BgL_test4517z00_8234;
{ /* Llib/hash.scm 338 */
 obj_t BgL_tmpz00_8235;
{ /* Llib/hash.scm 338 */
 obj_t BgL_res2546z00_3518;
{ /* Llib/hash.scm 338 */
 obj_t BgL_aux3065z00_5933;
BgL_aux3065z00_5933 = 
STRUCT_KEY(BgL_tablez00_61); 
if(
SYMBOLP(BgL_aux3065z00_5933))
{ /* Llib/hash.scm 338 */
BgL_res2546z00_3518 = BgL_aux3065z00_5933; }  else 
{ 
 obj_t BgL_auxz00_8239;
BgL_auxz00_8239 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13202L), BGl_string3930z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3065z00_5933); 
FAILURE(BgL_auxz00_8239,BFALSE,BFALSE);} } 
BgL_tmpz00_8235 = BgL_res2546z00_3518; } 
BgL_test4517z00_8234 = 
(BgL_tmpz00_8235==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4517z00_8234)
{ /* Llib/hash.scm 338 */
 int BgL_tmpz00_8244;
BgL_tmpz00_8244 = 
(int)(2L); 
BgL_bucketsz00_1712 = 
STRUCT_REF(BgL_tablez00_61, BgL_tmpz00_8244); }  else 
{ /* Llib/hash.scm 338 */
BgL_bucketsz00_1712 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_61); } } 
{ /* Llib/hash.scm 338 */
 obj_t BgL_vecz00_1713;
{ /* Llib/hash.scm 339 */
 long BgL_tmpz00_8248;
{ /* Llib/hash.scm 339 */
 obj_t BgL_tmpz00_8249;
if(
INTEGERP(BgL_siza7eza7_1710))
{ /* Llib/hash.scm 339 */
BgL_tmpz00_8249 = BgL_siza7eza7_1710
; }  else 
{ 
 obj_t BgL_auxz00_8252;
BgL_auxz00_8252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13238L), BGl_string3930z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1710); 
FAILURE(BgL_auxz00_8252,BFALSE,BFALSE);} 
BgL_tmpz00_8248 = 
(long)CINT(BgL_tmpz00_8249); } 
BgL_vecz00_1713 = 
make_vector(BgL_tmpz00_8248, BUNSPEC); } 
{ /* Llib/hash.scm 339 */

{ 
 long BgL_iz00_1715; long BgL_wz00_1716;
BgL_iz00_1715 = 0L; 
BgL_wz00_1716 = 0L; 
BgL_zc3z04anonymousza31454ze3z87_1717:
if(
(BgL_iz00_1715==BgL_siza7e3za7_1711))
{ /* Llib/hash.scm 342 */
return BgL_vecz00_1713;}  else 
{ /* Llib/hash.scm 344 */
 bool_t BgL_test4521z00_8260;
{ /* Llib/hash.scm 344 */
 bool_t BgL_test4522z00_8261;
{ /* Llib/hash.scm 344 */
 obj_t BgL_vectorz00_3522;
if(
VECTORP(BgL_bucketsz00_1712))
{ /* Llib/hash.scm 344 */
BgL_vectorz00_3522 = BgL_bucketsz00_1712; }  else 
{ 
 obj_t BgL_auxz00_8264;
BgL_auxz00_8264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13349L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1712); 
FAILURE(BgL_auxz00_8264,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 344 */
 bool_t BgL_test4524z00_8268;
{ /* Llib/hash.scm 344 */
 long BgL_tmpz00_8269;
BgL_tmpz00_8269 = 
VECTOR_LENGTH(BgL_vectorz00_3522); 
BgL_test4524z00_8268 = 
BOUND_CHECK(BgL_iz00_1715, BgL_tmpz00_8269); } 
if(BgL_test4524z00_8268)
{ /* Llib/hash.scm 344 */
BgL_test4522z00_8261 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3522,BgL_iz00_1715))
; }  else 
{ 
 obj_t BgL_auxz00_8274;
BgL_auxz00_8274 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13337L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3522, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3522)), 
(int)(BgL_iz00_1715)); 
FAILURE(BgL_auxz00_8274,BFALSE,BFALSE);} } } 
if(BgL_test4522z00_8261)
{ /* Llib/hash.scm 344 */
 long BgL_arg1464z00_1728;
BgL_arg1464z00_1728 = 
(BgL_iz00_1715+2L); 
{ /* Llib/hash.scm 344 */
 obj_t BgL_vectorz00_3525;
if(
VECTORP(BgL_bucketsz00_1712))
{ /* Llib/hash.scm 344 */
BgL_vectorz00_3525 = BgL_bucketsz00_1712; }  else 
{ 
 obj_t BgL_auxz00_8284;
BgL_auxz00_8284 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13372L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1712); 
FAILURE(BgL_auxz00_8284,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 344 */
 bool_t BgL_test4526z00_8288;
{ /* Llib/hash.scm 344 */
 long BgL_tmpz00_8289;
BgL_tmpz00_8289 = 
VECTOR_LENGTH(BgL_vectorz00_3525); 
BgL_test4526z00_8288 = 
BOUND_CHECK(BgL_arg1464z00_1728, BgL_tmpz00_8289); } 
if(BgL_test4526z00_8288)
{ /* Llib/hash.scm 344 */
BgL_test4521z00_8260 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3525,BgL_arg1464z00_1728))
; }  else 
{ 
 obj_t BgL_auxz00_8294;
BgL_auxz00_8294 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13360L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3525, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3525)), 
(int)(BgL_arg1464z00_1728)); 
FAILURE(BgL_auxz00_8294,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 344 */
BgL_test4521z00_8260 = ((bool_t)0)
; } } 
if(BgL_test4521z00_8260)
{ /* Llib/hash.scm 344 */
{ /* Llib/hash.scm 346 */
 obj_t BgL_arg1459z00_1722;
{ /* Llib/hash.scm 346 */
 long BgL_arg1460z00_1723;
BgL_arg1460z00_1723 = 
(BgL_iz00_1715+1L); 
{ /* Llib/hash.scm 346 */
 obj_t BgL_vectorz00_3528;
if(
VECTORP(BgL_bucketsz00_1712))
{ /* Llib/hash.scm 346 */
BgL_vectorz00_3528 = BgL_bucketsz00_1712; }  else 
{ 
 obj_t BgL_auxz00_8304;
BgL_auxz00_8304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13439L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1712); 
FAILURE(BgL_auxz00_8304,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 346 */
 bool_t BgL_test4528z00_8308;
{ /* Llib/hash.scm 346 */
 long BgL_tmpz00_8309;
BgL_tmpz00_8309 = 
VECTOR_LENGTH(BgL_vectorz00_3528); 
BgL_test4528z00_8308 = 
BOUND_CHECK(BgL_arg1460z00_1723, BgL_tmpz00_8309); } 
if(BgL_test4528z00_8308)
{ /* Llib/hash.scm 346 */
BgL_arg1459z00_1722 = 
VECTOR_REF(BgL_vectorz00_3528,BgL_arg1460z00_1723); }  else 
{ 
 obj_t BgL_auxz00_8313;
BgL_auxz00_8313 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13427L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3528, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3528)), 
(int)(BgL_arg1460z00_1723)); 
FAILURE(BgL_auxz00_8313,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 346 */
 bool_t BgL_test4529z00_8320;
{ /* Llib/hash.scm 346 */
 long BgL_tmpz00_8321;
BgL_tmpz00_8321 = 
VECTOR_LENGTH(BgL_vecz00_1713); 
BgL_test4529z00_8320 = 
BOUND_CHECK(BgL_wz00_1716, BgL_tmpz00_8321); } 
if(BgL_test4529z00_8320)
{ /* Llib/hash.scm 346 */
VECTOR_SET(BgL_vecz00_1713,BgL_wz00_1716,BgL_arg1459z00_1722); }  else 
{ 
 obj_t BgL_auxz00_8325;
BgL_auxz00_8325 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13408L), BGl_string3933z00zz__hashz00, BgL_vecz00_1713, 
(int)(
VECTOR_LENGTH(BgL_vecz00_1713)), 
(int)(BgL_wz00_1716)); 
FAILURE(BgL_auxz00_8325,BFALSE,BFALSE);} } } 
{ 
 long BgL_wz00_8334; long BgL_iz00_8332;
BgL_iz00_8332 = 
(BgL_iz00_1715+3L); 
BgL_wz00_8334 = 
(BgL_wz00_1716+1L); 
BgL_wz00_1716 = BgL_wz00_8334; 
BgL_iz00_1715 = BgL_iz00_8332; 
goto BgL_zc3z04anonymousza31454ze3z87_1717;} }  else 
{ 
 long BgL_iz00_8336;
BgL_iz00_8336 = 
(BgL_iz00_1715+3L); 
BgL_iz00_1715 = BgL_iz00_8336; 
goto BgL_zc3z04anonymousza31454ze3z87_1717;} } } } } } } } } 

}



/* plain-hashtable->vector */
obj_t BGl_plainzd2hashtablezd2ze3vectorze3zz__hashz00(obj_t BgL_tablez00_62)
{
{ /* Llib/hash.scm 353 */
{ /* Llib/hash.scm 354 */
 obj_t BgL_vecz00_1731;
{ /* Llib/hash.scm 354 */
 long BgL_arg1477z00_1751;
{ /* Llib/hash.scm 354 */
 long BgL_res2549z00_3540;
{ /* Llib/hash.scm 318 */
 bool_t BgL_test4530z00_8338;
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8339;
{ /* Llib/hash.scm 318 */
 obj_t BgL_res2548z00_3539;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3074z00_5943;
BgL_aux3074z00_5943 = 
STRUCT_KEY(BgL_tablez00_62); 
if(
SYMBOLP(BgL_aux3074z00_5943))
{ /* Llib/hash.scm 318 */
BgL_res2548z00_3539 = BgL_aux3074z00_5943; }  else 
{ 
 obj_t BgL_auxz00_8343;
BgL_auxz00_8343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3934z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3074z00_5943); 
FAILURE(BgL_auxz00_8343,BFALSE,BFALSE);} } 
BgL_tmpz00_8339 = BgL_res2548z00_3539; } 
BgL_test4530z00_8338 = 
(BgL_tmpz00_8339==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4530z00_8338)
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8348;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3076z00_5945;
{ /* Llib/hash.scm 318 */
 int BgL_tmpz00_8349;
BgL_tmpz00_8349 = 
(int)(0L); 
BgL_aux3076z00_5945 = 
STRUCT_REF(BgL_tablez00_62, BgL_tmpz00_8349); } 
if(
INTEGERP(BgL_aux3076z00_5945))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8348 = BgL_aux3076z00_5945
; }  else 
{ 
 obj_t BgL_auxz00_8354;
BgL_auxz00_8354 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3934z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3076z00_5945); 
FAILURE(BgL_auxz00_8354,BFALSE,BFALSE);} } 
BgL_res2549z00_3540 = 
(long)CINT(BgL_tmpz00_8348); }  else 
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8359;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3077z00_5946;
BgL_aux3077z00_5946 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_62); 
if(
INTEGERP(BgL_aux3077z00_5946))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8359 = BgL_aux3077z00_5946
; }  else 
{ 
 obj_t BgL_auxz00_8363;
BgL_auxz00_8363 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3934z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3077z00_5946); 
FAILURE(BgL_auxz00_8363,BFALSE,BFALSE);} } 
BgL_res2549z00_3540 = 
(long)CINT(BgL_tmpz00_8359); } } 
BgL_arg1477z00_1751 = BgL_res2549z00_3540; } 
BgL_vecz00_1731 = 
make_vector(BgL_arg1477z00_1751, BUNSPEC); } 
{ /* Llib/hash.scm 354 */
 obj_t BgL_bucketsz00_1732;
{ /* Llib/hash.scm 355 */
 bool_t BgL_test4534z00_8369;
{ /* Llib/hash.scm 355 */
 obj_t BgL_tmpz00_8370;
{ /* Llib/hash.scm 355 */
 obj_t BgL_res2551z00_3545;
{ /* Llib/hash.scm 355 */
 obj_t BgL_aux3078z00_5948;
BgL_aux3078z00_5948 = 
STRUCT_KEY(BgL_tablez00_62); 
if(
SYMBOLP(BgL_aux3078z00_5948))
{ /* Llib/hash.scm 355 */
BgL_res2551z00_3545 = BgL_aux3078z00_5948; }  else 
{ 
 obj_t BgL_auxz00_8374;
BgL_auxz00_8374 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13856L), BGl_string3934z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3078z00_5948); 
FAILURE(BgL_auxz00_8374,BFALSE,BFALSE);} } 
BgL_tmpz00_8370 = BgL_res2551z00_3545; } 
BgL_test4534z00_8369 = 
(BgL_tmpz00_8370==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4534z00_8369)
{ /* Llib/hash.scm 355 */
 int BgL_tmpz00_8379;
BgL_tmpz00_8379 = 
(int)(2L); 
BgL_bucketsz00_1732 = 
STRUCT_REF(BgL_tablez00_62, BgL_tmpz00_8379); }  else 
{ /* Llib/hash.scm 355 */
BgL_bucketsz00_1732 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_62); } } 
{ /* Llib/hash.scm 355 */
 long BgL_bucketszd2lenzd2_1733;
{ /* Llib/hash.scm 356 */
 obj_t BgL_vectorz00_3546;
if(
VECTORP(BgL_bucketsz00_1732))
{ /* Llib/hash.scm 356 */
BgL_vectorz00_3546 = BgL_bucketsz00_1732; }  else 
{ 
 obj_t BgL_auxz00_8385;
BgL_auxz00_8385 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(13915L), BGl_string3934z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1732); 
FAILURE(BgL_auxz00_8385,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1733 = 
VECTOR_LENGTH(BgL_vectorz00_3546); } 
{ /* Llib/hash.scm 356 */

{ 
 long BgL_iz00_1735; long BgL_wz00_1736;
BgL_iz00_1735 = 0L; 
BgL_wz00_1736 = 0L; 
BgL_zc3z04anonymousza31466ze3z87_1737:
if(
(BgL_iz00_1735==BgL_bucketszd2lenzd2_1733))
{ /* Llib/hash.scm 359 */
return BgL_vecz00_1731;}  else 
{ /* Llib/hash.scm 361 */
 obj_t BgL_g1058z00_1739;
{ /* Llib/hash.scm 361 */
 obj_t BgL_vectorz00_3549;
if(
VECTORP(BgL_bucketsz00_1732))
{ /* Llib/hash.scm 361 */
BgL_vectorz00_3549 = BgL_bucketsz00_1732; }  else 
{ 
 obj_t BgL_auxz00_8394;
BgL_auxz00_8394 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(14035L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1732); 
FAILURE(BgL_auxz00_8394,BFALSE,BFALSE);} 
BgL_g1058z00_1739 = 
VECTOR_REF(BgL_vectorz00_3549,BgL_iz00_1735); } 
{ 
 obj_t BgL_bucketz00_1741; long BgL_wz00_1742;
BgL_bucketz00_1741 = BgL_g1058z00_1739; 
BgL_wz00_1742 = BgL_wz00_1736; 
BgL_zc3z04anonymousza31468ze3z87_1743:
if(
NULLP(BgL_bucketz00_1741))
{ 
 long BgL_wz00_8403; long BgL_iz00_8401;
BgL_iz00_8401 = 
(BgL_iz00_1735+1L); 
BgL_wz00_8403 = BgL_wz00_1742; 
BgL_wz00_1736 = BgL_wz00_8403; 
BgL_iz00_1735 = BgL_iz00_8401; 
goto BgL_zc3z04anonymousza31466ze3z87_1737;}  else 
{ /* Llib/hash.scm 363 */
{ /* Llib/hash.scm 366 */
 obj_t BgL_arg1473z00_1746;
{ /* Llib/hash.scm 366 */
 obj_t BgL_pairz00_3552;
if(
PAIRP(BgL_bucketz00_1741))
{ /* Llib/hash.scm 366 */
BgL_pairz00_3552 = BgL_bucketz00_1741; }  else 
{ 
 obj_t BgL_auxz00_8406;
BgL_auxz00_8406 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(14153L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_1741); 
FAILURE(BgL_auxz00_8406,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 366 */
 obj_t BgL_pairz00_3555;
{ /* Llib/hash.scm 366 */
 obj_t BgL_aux3086z00_5956;
BgL_aux3086z00_5956 = 
CAR(BgL_pairz00_3552); 
if(
PAIRP(BgL_aux3086z00_5956))
{ /* Llib/hash.scm 366 */
BgL_pairz00_3555 = BgL_aux3086z00_5956; }  else 
{ 
 obj_t BgL_auxz00_8413;
BgL_auxz00_8413 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(14147L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3086z00_5956); 
FAILURE(BgL_auxz00_8413,BFALSE,BFALSE);} } 
BgL_arg1473z00_1746 = 
CDR(BgL_pairz00_3555); } } 
VECTOR_SET(BgL_vecz00_1731,BgL_wz00_1742,BgL_arg1473z00_1746); } 
{ /* Llib/hash.scm 367 */
 obj_t BgL_arg1474z00_1747; long BgL_arg1476z00_1748;
{ /* Llib/hash.scm 367 */
 obj_t BgL_pairz00_3558;
if(
PAIRP(BgL_bucketz00_1741))
{ /* Llib/hash.scm 367 */
BgL_pairz00_3558 = BgL_bucketz00_1741; }  else 
{ 
 obj_t BgL_auxz00_8421;
BgL_auxz00_8421 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(14182L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_1741); 
FAILURE(BgL_auxz00_8421,BFALSE,BFALSE);} 
BgL_arg1474z00_1747 = 
CDR(BgL_pairz00_3558); } 
BgL_arg1476z00_1748 = 
(BgL_wz00_1742+1L); 
{ 
 long BgL_wz00_8428; obj_t BgL_bucketz00_8427;
BgL_bucketz00_8427 = BgL_arg1474z00_1747; 
BgL_wz00_8428 = BgL_arg1476z00_1748; 
BgL_wz00_1742 = BgL_wz00_8428; 
BgL_bucketz00_1741 = BgL_bucketz00_8427; 
goto BgL_zc3z04anonymousza31468ze3z87_1743;} } } } } } } } } } } 

}



/* hashtable->list */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t BgL_tablez00_63)
{
{ /* Llib/hash.scm 372 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_63))
{ /* Llib/hash.scm 374 */
BGL_TAIL return 
BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00(BgL_tablez00_63);}  else 
{ /* Llib/hash.scm 374 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_63))
{ /* Llib/hash.scm 376 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(BgL_tablez00_63);}  else 
{ /* Llib/hash.scm 376 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2ze3listze3zz__hashz00(BgL_tablez00_63);} } } 

}



/* &hashtable->list */
obj_t BGl_z62hashtablezd2ze3listz53zz__hashz00(obj_t BgL_envz00_5434, obj_t BgL_tablez00_5435)
{
{ /* Llib/hash.scm 372 */
{ /* Llib/hash.scm 374 */
 obj_t BgL_auxz00_8436;
if(
STRUCTP(BgL_tablez00_5435))
{ /* Llib/hash.scm 374 */
BgL_auxz00_8436 = BgL_tablez00_5435
; }  else 
{ 
 obj_t BgL_auxz00_8439;
BgL_auxz00_8439 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(14486L), BGl_string3937z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5435); 
FAILURE(BgL_auxz00_8439,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2ze3listz31zz__hashz00(BgL_auxz00_8436);} } 

}



/* open-string-hashtable->list */
obj_t BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00(obj_t BgL_tablez00_64)
{
{ /* Llib/hash.scm 384 */
{ /* Llib/hash.scm 385 */
 obj_t BgL_siza7eza7_1755;
{ /* Llib/hash.scm 385 */
 bool_t BgL_test4546z00_8444;
{ /* Llib/hash.scm 385 */
 obj_t BgL_tmpz00_8445;
{ /* Llib/hash.scm 385 */
 obj_t BgL_res2552z00_3563;
{ /* Llib/hash.scm 385 */
 obj_t BgL_aux3092z00_5962;
BgL_aux3092z00_5962 = 
STRUCT_KEY(BgL_tablez00_64); 
if(
SYMBOLP(BgL_aux3092z00_5962))
{ /* Llib/hash.scm 385 */
BgL_res2552z00_3563 = BgL_aux3092z00_5962; }  else 
{ 
 obj_t BgL_auxz00_8449;
BgL_auxz00_8449 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(14973L), BGl_string3938z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3092z00_5962); 
FAILURE(BgL_auxz00_8449,BFALSE,BFALSE);} } 
BgL_tmpz00_8445 = BgL_res2552z00_3563; } 
BgL_test4546z00_8444 = 
(BgL_tmpz00_8445==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4546z00_8444)
{ /* Llib/hash.scm 385 */
 int BgL_tmpz00_8454;
BgL_tmpz00_8454 = 
(int)(1L); 
BgL_siza7eza7_1755 = 
STRUCT_REF(BgL_tablez00_64, BgL_tmpz00_8454); }  else 
{ /* Llib/hash.scm 385 */
BgL_siza7eza7_1755 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_64); } } 
{ /* Llib/hash.scm 385 */
 long BgL_siza7e3za7_1756;
{ /* Llib/hash.scm 386 */
 long BgL_za72za7_3564;
{ /* Llib/hash.scm 386 */
 obj_t BgL_tmpz00_8458;
if(
INTEGERP(BgL_siza7eza7_1755))
{ /* Llib/hash.scm 386 */
BgL_tmpz00_8458 = BgL_siza7eza7_1755
; }  else 
{ 
 obj_t BgL_auxz00_8461;
BgL_auxz00_8461 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15025L), BGl_string3938z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1755); 
FAILURE(BgL_auxz00_8461,BFALSE,BFALSE);} 
BgL_za72za7_3564 = 
(long)CINT(BgL_tmpz00_8458); } 
BgL_siza7e3za7_1756 = 
(3L*BgL_za72za7_3564); } 
{ /* Llib/hash.scm 386 */
 obj_t BgL_bucketsz00_1757;
{ /* Llib/hash.scm 387 */
 bool_t BgL_test4549z00_8467;
{ /* Llib/hash.scm 387 */
 obj_t BgL_tmpz00_8468;
{ /* Llib/hash.scm 387 */
 obj_t BgL_res2553z00_3568;
{ /* Llib/hash.scm 387 */
 obj_t BgL_aux3095z00_5965;
BgL_aux3095z00_5965 = 
STRUCT_KEY(BgL_tablez00_64); 
if(
SYMBOLP(BgL_aux3095z00_5965))
{ /* Llib/hash.scm 387 */
BgL_res2553z00_3568 = BgL_aux3095z00_5965; }  else 
{ 
 obj_t BgL_auxz00_8472;
BgL_auxz00_8472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15044L), BGl_string3938z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3095z00_5965); 
FAILURE(BgL_auxz00_8472,BFALSE,BFALSE);} } 
BgL_tmpz00_8468 = BgL_res2553z00_3568; } 
BgL_test4549z00_8467 = 
(BgL_tmpz00_8468==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4549z00_8467)
{ /* Llib/hash.scm 387 */
 int BgL_tmpz00_8477;
BgL_tmpz00_8477 = 
(int)(2L); 
BgL_bucketsz00_1757 = 
STRUCT_REF(BgL_tablez00_64, BgL_tmpz00_8477); }  else 
{ /* Llib/hash.scm 387 */
BgL_bucketsz00_1757 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_64); } } 
{ /* Llib/hash.scm 387 */

{ 
 long BgL_iz00_1760; obj_t BgL_resz00_1761;
BgL_iz00_1760 = 0L; 
BgL_resz00_1761 = BNIL; 
BgL_zc3z04anonymousza31481ze3z87_1762:
if(
(BgL_iz00_1760==BgL_siza7e3za7_1756))
{ /* Llib/hash.scm 390 */
return BgL_resz00_1761;}  else 
{ /* Llib/hash.scm 392 */
 bool_t BgL_test4552z00_8483;
{ /* Llib/hash.scm 392 */
 bool_t BgL_test4553z00_8484;
{ /* Llib/hash.scm 392 */
 obj_t BgL_vectorz00_3571;
if(
VECTORP(BgL_bucketsz00_1757))
{ /* Llib/hash.scm 392 */
BgL_vectorz00_3571 = BgL_bucketsz00_1757; }  else 
{ 
 obj_t BgL_auxz00_8487;
BgL_auxz00_8487 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15167L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1757); 
FAILURE(BgL_auxz00_8487,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 392 */
 bool_t BgL_test4555z00_8491;
{ /* Llib/hash.scm 392 */
 long BgL_tmpz00_8492;
BgL_tmpz00_8492 = 
VECTOR_LENGTH(BgL_vectorz00_3571); 
BgL_test4555z00_8491 = 
BOUND_CHECK(BgL_iz00_1760, BgL_tmpz00_8492); } 
if(BgL_test4555z00_8491)
{ /* Llib/hash.scm 392 */
BgL_test4553z00_8484 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3571,BgL_iz00_1760))
; }  else 
{ 
 obj_t BgL_auxz00_8497;
BgL_auxz00_8497 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15155L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3571, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3571)), 
(int)(BgL_iz00_1760)); 
FAILURE(BgL_auxz00_8497,BFALSE,BFALSE);} } } 
if(BgL_test4553z00_8484)
{ /* Llib/hash.scm 392 */
 long BgL_arg1492z00_1773;
BgL_arg1492z00_1773 = 
(BgL_iz00_1760+2L); 
{ /* Llib/hash.scm 392 */
 obj_t BgL_vectorz00_3574;
if(
VECTORP(BgL_bucketsz00_1757))
{ /* Llib/hash.scm 392 */
BgL_vectorz00_3574 = BgL_bucketsz00_1757; }  else 
{ 
 obj_t BgL_auxz00_8507;
BgL_auxz00_8507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15190L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1757); 
FAILURE(BgL_auxz00_8507,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 392 */
 bool_t BgL_test4557z00_8511;
{ /* Llib/hash.scm 392 */
 long BgL_tmpz00_8512;
BgL_tmpz00_8512 = 
VECTOR_LENGTH(BgL_vectorz00_3574); 
BgL_test4557z00_8511 = 
BOUND_CHECK(BgL_arg1492z00_1773, BgL_tmpz00_8512); } 
if(BgL_test4557z00_8511)
{ /* Llib/hash.scm 392 */
BgL_test4552z00_8483 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3574,BgL_arg1492z00_1773))
; }  else 
{ 
 obj_t BgL_auxz00_8517;
BgL_auxz00_8517 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15178L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3574, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3574)), 
(int)(BgL_arg1492z00_1773)); 
FAILURE(BgL_auxz00_8517,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 392 */
BgL_test4552z00_8483 = ((bool_t)0)
; } } 
if(BgL_test4552z00_8483)
{ /* Llib/hash.scm 393 */
 long BgL_arg1486z00_1767; obj_t BgL_arg1487z00_1768;
BgL_arg1486z00_1767 = 
(BgL_iz00_1760+3L); 
{ /* Llib/hash.scm 393 */
 obj_t BgL_arg1488z00_1769;
{ /* Llib/hash.scm 393 */
 long BgL_arg1489z00_1770;
BgL_arg1489z00_1770 = 
(BgL_iz00_1760+1L); 
{ /* Llib/hash.scm 393 */
 obj_t BgL_vectorz00_3578;
if(
VECTORP(BgL_bucketsz00_1757))
{ /* Llib/hash.scm 393 */
BgL_vectorz00_3578 = BgL_bucketsz00_1757; }  else 
{ 
 obj_t BgL_auxz00_8528;
BgL_auxz00_8528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15247L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1757); 
FAILURE(BgL_auxz00_8528,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 393 */
 bool_t BgL_test4559z00_8532;
{ /* Llib/hash.scm 393 */
 long BgL_tmpz00_8533;
BgL_tmpz00_8533 = 
VECTOR_LENGTH(BgL_vectorz00_3578); 
BgL_test4559z00_8532 = 
BOUND_CHECK(BgL_arg1489z00_1770, BgL_tmpz00_8533); } 
if(BgL_test4559z00_8532)
{ /* Llib/hash.scm 393 */
BgL_arg1488z00_1769 = 
VECTOR_REF(BgL_vectorz00_3578,BgL_arg1489z00_1770); }  else 
{ 
 obj_t BgL_auxz00_8537;
BgL_auxz00_8537 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15235L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3578, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3578)), 
(int)(BgL_arg1489z00_1770)); 
FAILURE(BgL_auxz00_8537,BFALSE,BFALSE);} } } } 
BgL_arg1487z00_1768 = 
MAKE_YOUNG_PAIR(BgL_arg1488z00_1769, BgL_resz00_1761); } 
{ 
 obj_t BgL_resz00_8546; long BgL_iz00_8545;
BgL_iz00_8545 = BgL_arg1486z00_1767; 
BgL_resz00_8546 = BgL_arg1487z00_1768; 
BgL_resz00_1761 = BgL_resz00_8546; 
BgL_iz00_1760 = BgL_iz00_8545; 
goto BgL_zc3z04anonymousza31481ze3z87_1762;} }  else 
{ 
 long BgL_iz00_8547;
BgL_iz00_8547 = 
(BgL_iz00_1760+3L); 
BgL_iz00_1760 = BgL_iz00_8547; 
goto BgL_zc3z04anonymousza31481ze3z87_1762;} } } } } } } } 

}



/* plain-hashtable->list */
obj_t BGl_plainzd2hashtablezd2ze3listze3zz__hashz00(obj_t BgL_tablez00_65)
{
{ /* Llib/hash.scm 399 */
{ /* Llib/hash.scm 400 */
 obj_t BgL_vecz00_1775;
{ /* Llib/hash.scm 400 */
 long BgL_arg1501z00_1796;
{ /* Llib/hash.scm 400 */
 long BgL_res2555z00_3586;
{ /* Llib/hash.scm 318 */
 bool_t BgL_test4560z00_8549;
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8550;
{ /* Llib/hash.scm 318 */
 obj_t BgL_res2554z00_3585;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3103z00_5973;
BgL_aux3103z00_5973 = 
STRUCT_KEY(BgL_tablez00_65); 
if(
SYMBOLP(BgL_aux3103z00_5973))
{ /* Llib/hash.scm 318 */
BgL_res2554z00_3585 = BgL_aux3103z00_5973; }  else 
{ 
 obj_t BgL_auxz00_8554;
BgL_auxz00_8554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3939z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3103z00_5973); 
FAILURE(BgL_auxz00_8554,BFALSE,BFALSE);} } 
BgL_tmpz00_8550 = BgL_res2554z00_3585; } 
BgL_test4560z00_8549 = 
(BgL_tmpz00_8550==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4560z00_8549)
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8559;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3105z00_5975;
{ /* Llib/hash.scm 318 */
 int BgL_tmpz00_8560;
BgL_tmpz00_8560 = 
(int)(0L); 
BgL_aux3105z00_5975 = 
STRUCT_REF(BgL_tablez00_65, BgL_tmpz00_8560); } 
if(
INTEGERP(BgL_aux3105z00_5975))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8559 = BgL_aux3105z00_5975
; }  else 
{ 
 obj_t BgL_auxz00_8565;
BgL_auxz00_8565 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3939z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3105z00_5975); 
FAILURE(BgL_auxz00_8565,BFALSE,BFALSE);} } 
BgL_res2555z00_3586 = 
(long)CINT(BgL_tmpz00_8559); }  else 
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8570;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3106z00_5976;
BgL_aux3106z00_5976 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_65); 
if(
INTEGERP(BgL_aux3106z00_5976))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8570 = BgL_aux3106z00_5976
; }  else 
{ 
 obj_t BgL_auxz00_8574;
BgL_auxz00_8574 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3939z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3106z00_5976); 
FAILURE(BgL_auxz00_8574,BFALSE,BFALSE);} } 
BgL_res2555z00_3586 = 
(long)CINT(BgL_tmpz00_8570); } } 
BgL_arg1501z00_1796 = BgL_res2555z00_3586; } 
BgL_vecz00_1775 = 
make_vector(BgL_arg1501z00_1796, BUNSPEC); } 
{ /* Llib/hash.scm 400 */
 obj_t BgL_bucketsz00_1776;
{ /* Llib/hash.scm 401 */
 bool_t BgL_test4565z00_8580;
{ /* Llib/hash.scm 401 */
 obj_t BgL_tmpz00_8581;
{ /* Llib/hash.scm 401 */
 obj_t BgL_res2557z00_3591;
{ /* Llib/hash.scm 401 */
 obj_t BgL_aux3107z00_5978;
BgL_aux3107z00_5978 = 
STRUCT_KEY(BgL_tablez00_65); 
if(
SYMBOLP(BgL_aux3107z00_5978))
{ /* Llib/hash.scm 401 */
BgL_res2557z00_3591 = BgL_aux3107z00_5978; }  else 
{ 
 obj_t BgL_auxz00_8585;
BgL_auxz00_8585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15635L), BGl_string3939z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3107z00_5978); 
FAILURE(BgL_auxz00_8585,BFALSE,BFALSE);} } 
BgL_tmpz00_8581 = BgL_res2557z00_3591; } 
BgL_test4565z00_8580 = 
(BgL_tmpz00_8581==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4565z00_8580)
{ /* Llib/hash.scm 401 */
 int BgL_tmpz00_8590;
BgL_tmpz00_8590 = 
(int)(2L); 
BgL_bucketsz00_1776 = 
STRUCT_REF(BgL_tablez00_65, BgL_tmpz00_8590); }  else 
{ /* Llib/hash.scm 401 */
BgL_bucketsz00_1776 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_65); } } 
{ /* Llib/hash.scm 401 */
 long BgL_bucketszd2lenzd2_1777;
{ /* Llib/hash.scm 402 */
 obj_t BgL_vectorz00_3592;
if(
VECTORP(BgL_bucketsz00_1776))
{ /* Llib/hash.scm 402 */
BgL_vectorz00_3592 = BgL_bucketsz00_1776; }  else 
{ 
 obj_t BgL_auxz00_8596;
BgL_auxz00_8596 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15694L), BGl_string3939z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1776); 
FAILURE(BgL_auxz00_8596,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1777 = 
VECTOR_LENGTH(BgL_vectorz00_3592); } 
{ /* Llib/hash.scm 402 */

{ 
 long BgL_iz00_1780; obj_t BgL_resz00_1781;
BgL_iz00_1780 = 0L; 
BgL_resz00_1781 = BNIL; 
BgL_zc3z04anonymousza31493ze3z87_1782:
if(
(BgL_iz00_1780==BgL_bucketszd2lenzd2_1777))
{ /* Llib/hash.scm 405 */
return BgL_resz00_1781;}  else 
{ /* Llib/hash.scm 407 */
 obj_t BgL_g1061z00_1784;
{ /* Llib/hash.scm 407 */
 obj_t BgL_vectorz00_3595;
if(
VECTORP(BgL_bucketsz00_1776))
{ /* Llib/hash.scm 407 */
BgL_vectorz00_3595 = BgL_bucketsz00_1776; }  else 
{ 
 obj_t BgL_auxz00_8605;
BgL_auxz00_8605 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15818L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1776); 
FAILURE(BgL_auxz00_8605,BFALSE,BFALSE);} 
BgL_g1061z00_1784 = 
VECTOR_REF(BgL_vectorz00_3595,BgL_iz00_1780); } 
{ 
 obj_t BgL_bucketz00_1786; obj_t BgL_resz00_1787;
BgL_bucketz00_1786 = BgL_g1061z00_1784; 
BgL_resz00_1787 = BgL_resz00_1781; 
BgL_zc3z04anonymousza31495ze3z87_1788:
if(
NULLP(BgL_bucketz00_1786))
{ 
 obj_t BgL_resz00_8614; long BgL_iz00_8612;
BgL_iz00_8612 = 
(BgL_iz00_1780+1L); 
BgL_resz00_8614 = BgL_resz00_1787; 
BgL_resz00_1781 = BgL_resz00_8614; 
BgL_iz00_1780 = BgL_iz00_8612; 
goto BgL_zc3z04anonymousza31493ze3z87_1782;}  else 
{ /* Llib/hash.scm 411 */
 obj_t BgL_arg1498z00_1791; obj_t BgL_arg1499z00_1792;
{ /* Llib/hash.scm 411 */
 obj_t BgL_pairz00_3598;
if(
PAIRP(BgL_bucketz00_1786))
{ /* Llib/hash.scm 411 */
BgL_pairz00_3598 = BgL_bucketz00_1786; }  else 
{ 
 obj_t BgL_auxz00_8617;
BgL_auxz00_8617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15909L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_1786); 
FAILURE(BgL_auxz00_8617,BFALSE,BFALSE);} 
BgL_arg1498z00_1791 = 
CDR(BgL_pairz00_3598); } 
{ /* Llib/hash.scm 411 */
 obj_t BgL_arg1500z00_1793;
{ /* Llib/hash.scm 411 */
 obj_t BgL_pairz00_3599;
if(
PAIRP(BgL_bucketz00_1786))
{ /* Llib/hash.scm 411 */
BgL_pairz00_3599 = BgL_bucketz00_1786; }  else 
{ 
 obj_t BgL_auxz00_8624;
BgL_auxz00_8624 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15929L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_1786); 
FAILURE(BgL_auxz00_8624,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 411 */
 obj_t BgL_pairz00_3602;
{ /* Llib/hash.scm 411 */
 obj_t BgL_aux3117z00_5988;
BgL_aux3117z00_5988 = 
CAR(BgL_pairz00_3599); 
if(
PAIRP(BgL_aux3117z00_5988))
{ /* Llib/hash.scm 411 */
BgL_pairz00_3602 = BgL_aux3117z00_5988; }  else 
{ 
 obj_t BgL_auxz00_8631;
BgL_auxz00_8631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(15923L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3117z00_5988); 
FAILURE(BgL_auxz00_8631,BFALSE,BFALSE);} } 
BgL_arg1500z00_1793 = 
CDR(BgL_pairz00_3602); } } 
BgL_arg1499z00_1792 = 
MAKE_YOUNG_PAIR(BgL_arg1500z00_1793, BgL_resz00_1787); } 
{ 
 obj_t BgL_resz00_8638; obj_t BgL_bucketz00_8637;
BgL_bucketz00_8637 = BgL_arg1498z00_1791; 
BgL_resz00_8638 = BgL_arg1499z00_1792; 
BgL_resz00_1787 = BgL_resz00_8638; 
BgL_bucketz00_1786 = BgL_bucketz00_8637; 
goto BgL_zc3z04anonymousza31495ze3z87_1788;} } } } } } } } } } 

}



/* hashtable-key-list */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2keyzd2listz00zz__hashz00(obj_t BgL_tablez00_66)
{
{ /* Llib/hash.scm 416 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_66))
{ /* Llib/hash.scm 418 */
BGL_TAIL return 
BGl_openzd2stringzd2hashtablezd2keyzd2listz00zz__hashz00(BgL_tablez00_66);}  else 
{ /* Llib/hash.scm 418 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_66))
{ /* Llib/hash.scm 420 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(BgL_tablez00_66);}  else 
{ /* Llib/hash.scm 420 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2keyzd2listzd2zz__hashz00(BgL_tablez00_66);} } } 

}



/* &hashtable-key-list */
obj_t BGl_z62hashtablezd2keyzd2listz62zz__hashz00(obj_t BgL_envz00_5436, obj_t BgL_tablez00_5437)
{
{ /* Llib/hash.scm 416 */
{ /* Llib/hash.scm 418 */
 obj_t BgL_auxz00_8646;
if(
STRUCTP(BgL_tablez00_5437))
{ /* Llib/hash.scm 418 */
BgL_auxz00_8646 = BgL_tablez00_5437
; }  else 
{ 
 obj_t BgL_auxz00_8649;
BgL_auxz00_8649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16230L), BGl_string3940z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5437); 
FAILURE(BgL_auxz00_8649,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2keyzd2listz00zz__hashz00(BgL_auxz00_8646);} } 

}



/* open-string-hashtable-key-list */
obj_t BGl_openzd2stringzd2hashtablezd2keyzd2listz00zz__hashz00(obj_t BgL_tablez00_67)
{
{ /* Llib/hash.scm 428 */
{ /* Llib/hash.scm 429 */
 obj_t BgL_siza7eza7_1800;
{ /* Llib/hash.scm 429 */
 bool_t BgL_test4577z00_8654;
{ /* Llib/hash.scm 429 */
 obj_t BgL_tmpz00_8655;
{ /* Llib/hash.scm 429 */
 obj_t BgL_res2558z00_3606;
{ /* Llib/hash.scm 429 */
 obj_t BgL_aux3121z00_5992;
BgL_aux3121z00_5992 = 
STRUCT_KEY(BgL_tablez00_67); 
if(
SYMBOLP(BgL_aux3121z00_5992))
{ /* Llib/hash.scm 429 */
BgL_res2558z00_3606 = BgL_aux3121z00_5992; }  else 
{ 
 obj_t BgL_auxz00_8659;
BgL_auxz00_8659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16729L), BGl_string3941z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3121z00_5992); 
FAILURE(BgL_auxz00_8659,BFALSE,BFALSE);} } 
BgL_tmpz00_8655 = BgL_res2558z00_3606; } 
BgL_test4577z00_8654 = 
(BgL_tmpz00_8655==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4577z00_8654)
{ /* Llib/hash.scm 429 */
 int BgL_tmpz00_8664;
BgL_tmpz00_8664 = 
(int)(1L); 
BgL_siza7eza7_1800 = 
STRUCT_REF(BgL_tablez00_67, BgL_tmpz00_8664); }  else 
{ /* Llib/hash.scm 429 */
BgL_siza7eza7_1800 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_67); } } 
{ /* Llib/hash.scm 429 */
 long BgL_siza7e3za7_1801;
{ /* Llib/hash.scm 430 */
 long BgL_za72za7_3607;
{ /* Llib/hash.scm 430 */
 obj_t BgL_tmpz00_8668;
if(
INTEGERP(BgL_siza7eza7_1800))
{ /* Llib/hash.scm 430 */
BgL_tmpz00_8668 = BgL_siza7eza7_1800
; }  else 
{ 
 obj_t BgL_auxz00_8671;
BgL_auxz00_8671 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16781L), BGl_string3941z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1800); 
FAILURE(BgL_auxz00_8671,BFALSE,BFALSE);} 
BgL_za72za7_3607 = 
(long)CINT(BgL_tmpz00_8668); } 
BgL_siza7e3za7_1801 = 
(3L*BgL_za72za7_3607); } 
{ /* Llib/hash.scm 430 */
 obj_t BgL_bucketsz00_1802;
{ /* Llib/hash.scm 431 */
 bool_t BgL_test4580z00_8677;
{ /* Llib/hash.scm 431 */
 obj_t BgL_tmpz00_8678;
{ /* Llib/hash.scm 431 */
 obj_t BgL_res2559z00_3611;
{ /* Llib/hash.scm 431 */
 obj_t BgL_aux3124z00_5995;
BgL_aux3124z00_5995 = 
STRUCT_KEY(BgL_tablez00_67); 
if(
SYMBOLP(BgL_aux3124z00_5995))
{ /* Llib/hash.scm 431 */
BgL_res2559z00_3611 = BgL_aux3124z00_5995; }  else 
{ 
 obj_t BgL_auxz00_8682;
BgL_auxz00_8682 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16800L), BGl_string3941z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3124z00_5995); 
FAILURE(BgL_auxz00_8682,BFALSE,BFALSE);} } 
BgL_tmpz00_8678 = BgL_res2559z00_3611; } 
BgL_test4580z00_8677 = 
(BgL_tmpz00_8678==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4580z00_8677)
{ /* Llib/hash.scm 431 */
 int BgL_tmpz00_8687;
BgL_tmpz00_8687 = 
(int)(2L); 
BgL_bucketsz00_1802 = 
STRUCT_REF(BgL_tablez00_67, BgL_tmpz00_8687); }  else 
{ /* Llib/hash.scm 431 */
BgL_bucketsz00_1802 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_67); } } 
{ /* Llib/hash.scm 431 */

{ 
 long BgL_iz00_1805; obj_t BgL_resz00_1806;
BgL_iz00_1805 = 0L; 
BgL_resz00_1806 = BNIL; 
BgL_zc3z04anonymousza31505ze3z87_1807:
if(
(BgL_iz00_1805==BgL_siza7e3za7_1801))
{ /* Llib/hash.scm 434 */
return BgL_resz00_1806;}  else 
{ /* Llib/hash.scm 436 */
 bool_t BgL_test4583z00_8693;
{ /* Llib/hash.scm 436 */
 bool_t BgL_test4584z00_8694;
{ /* Llib/hash.scm 436 */
 obj_t BgL_vectorz00_3614;
if(
VECTORP(BgL_bucketsz00_1802))
{ /* Llib/hash.scm 436 */
BgL_vectorz00_3614 = BgL_bucketsz00_1802; }  else 
{ 
 obj_t BgL_auxz00_8697;
BgL_auxz00_8697 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16923L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1802); 
FAILURE(BgL_auxz00_8697,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 436 */
 bool_t BgL_test4586z00_8701;
{ /* Llib/hash.scm 436 */
 long BgL_tmpz00_8702;
BgL_tmpz00_8702 = 
VECTOR_LENGTH(BgL_vectorz00_3614); 
BgL_test4586z00_8701 = 
BOUND_CHECK(BgL_iz00_1805, BgL_tmpz00_8702); } 
if(BgL_test4586z00_8701)
{ /* Llib/hash.scm 436 */
BgL_test4584z00_8694 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3614,BgL_iz00_1805))
; }  else 
{ 
 obj_t BgL_auxz00_8707;
BgL_auxz00_8707 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16911L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3614, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3614)), 
(int)(BgL_iz00_1805)); 
FAILURE(BgL_auxz00_8707,BFALSE,BFALSE);} } } 
if(BgL_test4584z00_8694)
{ /* Llib/hash.scm 436 */
 long BgL_arg1516z00_1817;
BgL_arg1516z00_1817 = 
(BgL_iz00_1805+2L); 
{ /* Llib/hash.scm 436 */
 obj_t BgL_vectorz00_3617;
if(
VECTORP(BgL_bucketsz00_1802))
{ /* Llib/hash.scm 436 */
BgL_vectorz00_3617 = BgL_bucketsz00_1802; }  else 
{ 
 obj_t BgL_auxz00_8717;
BgL_auxz00_8717 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16946L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1802); 
FAILURE(BgL_auxz00_8717,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 436 */
 bool_t BgL_test4588z00_8721;
{ /* Llib/hash.scm 436 */
 long BgL_tmpz00_8722;
BgL_tmpz00_8722 = 
VECTOR_LENGTH(BgL_vectorz00_3617); 
BgL_test4588z00_8721 = 
BOUND_CHECK(BgL_arg1516z00_1817, BgL_tmpz00_8722); } 
if(BgL_test4588z00_8721)
{ /* Llib/hash.scm 436 */
BgL_test4583z00_8693 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3617,BgL_arg1516z00_1817))
; }  else 
{ 
 obj_t BgL_auxz00_8727;
BgL_auxz00_8727 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16934L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3617, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3617)), 
(int)(BgL_arg1516z00_1817)); 
FAILURE(BgL_auxz00_8727,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 436 */
BgL_test4583z00_8693 = ((bool_t)0)
; } } 
if(BgL_test4583z00_8693)
{ /* Llib/hash.scm 437 */
 long BgL_arg1510z00_1812; obj_t BgL_arg1511z00_1813;
BgL_arg1510z00_1812 = 
(BgL_iz00_1805+3L); 
{ /* Llib/hash.scm 437 */
 obj_t BgL_arg1513z00_1814;
{ /* Llib/hash.scm 437 */
 obj_t BgL_vectorz00_3620;
if(
VECTORP(BgL_bucketsz00_1802))
{ /* Llib/hash.scm 437 */
BgL_vectorz00_3620 = BgL_bucketsz00_1802; }  else 
{ 
 obj_t BgL_auxz00_8737;
BgL_auxz00_8737 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17003L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1802); 
FAILURE(BgL_auxz00_8737,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 437 */
 bool_t BgL_test4590z00_8741;
{ /* Llib/hash.scm 437 */
 long BgL_tmpz00_8742;
BgL_tmpz00_8742 = 
VECTOR_LENGTH(BgL_vectorz00_3620); 
BgL_test4590z00_8741 = 
BOUND_CHECK(BgL_iz00_1805, BgL_tmpz00_8742); } 
if(BgL_test4590z00_8741)
{ /* Llib/hash.scm 437 */
BgL_arg1513z00_1814 = 
VECTOR_REF(BgL_vectorz00_3620,BgL_iz00_1805); }  else 
{ 
 obj_t BgL_auxz00_8746;
BgL_auxz00_8746 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(16991L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3620, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3620)), 
(int)(BgL_iz00_1805)); 
FAILURE(BgL_auxz00_8746,BFALSE,BFALSE);} } } 
BgL_arg1511z00_1813 = 
MAKE_YOUNG_PAIR(BgL_arg1513z00_1814, BgL_resz00_1806); } 
{ 
 obj_t BgL_resz00_8755; long BgL_iz00_8754;
BgL_iz00_8754 = BgL_arg1510z00_1812; 
BgL_resz00_8755 = BgL_arg1511z00_1813; 
BgL_resz00_1806 = BgL_resz00_8755; 
BgL_iz00_1805 = BgL_iz00_8754; 
goto BgL_zc3z04anonymousza31505ze3z87_1807;} }  else 
{ 
 long BgL_iz00_8756;
BgL_iz00_8756 = 
(BgL_iz00_1805+3L); 
BgL_iz00_1805 = BgL_iz00_8756; 
goto BgL_zc3z04anonymousza31505ze3z87_1807;} } } } } } } } 

}



/* plain-hashtable-key-list */
obj_t BGl_plainzd2hashtablezd2keyzd2listzd2zz__hashz00(obj_t BgL_tablez00_68)
{
{ /* Llib/hash.scm 443 */
{ /* Llib/hash.scm 444 */
 obj_t BgL_vecz00_1819;
{ /* Llib/hash.scm 444 */
 long BgL_arg1525z00_1840;
{ /* Llib/hash.scm 444 */
 long BgL_res2561z00_3628;
{ /* Llib/hash.scm 318 */
 bool_t BgL_test4591z00_8758;
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8759;
{ /* Llib/hash.scm 318 */
 obj_t BgL_res2560z00_3627;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3132z00_6003;
BgL_aux3132z00_6003 = 
STRUCT_KEY(BgL_tablez00_68); 
if(
SYMBOLP(BgL_aux3132z00_6003))
{ /* Llib/hash.scm 318 */
BgL_res2560z00_3627 = BgL_aux3132z00_6003; }  else 
{ 
 obj_t BgL_auxz00_8763;
BgL_auxz00_8763 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3942z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3132z00_6003); 
FAILURE(BgL_auxz00_8763,BFALSE,BFALSE);} } 
BgL_tmpz00_8759 = BgL_res2560z00_3627; } 
BgL_test4591z00_8758 = 
(BgL_tmpz00_8759==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4591z00_8758)
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8768;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3134z00_6005;
{ /* Llib/hash.scm 318 */
 int BgL_tmpz00_8769;
BgL_tmpz00_8769 = 
(int)(0L); 
BgL_aux3134z00_6005 = 
STRUCT_REF(BgL_tablez00_68, BgL_tmpz00_8769); } 
if(
INTEGERP(BgL_aux3134z00_6005))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8768 = BgL_aux3134z00_6005
; }  else 
{ 
 obj_t BgL_auxz00_8774;
BgL_auxz00_8774 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3942z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3134z00_6005); 
FAILURE(BgL_auxz00_8774,BFALSE,BFALSE);} } 
BgL_res2561z00_3628 = 
(long)CINT(BgL_tmpz00_8768); }  else 
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_8779;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3135z00_6006;
BgL_aux3135z00_6006 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_68); 
if(
INTEGERP(BgL_aux3135z00_6006))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_8779 = BgL_aux3135z00_6006
; }  else 
{ 
 obj_t BgL_auxz00_8783;
BgL_auxz00_8783 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string3942z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3135z00_6006); 
FAILURE(BgL_auxz00_8783,BFALSE,BFALSE);} } 
BgL_res2561z00_3628 = 
(long)CINT(BgL_tmpz00_8779); } } 
BgL_arg1525z00_1840 = BgL_res2561z00_3628; } 
BgL_vecz00_1819 = 
make_vector(BgL_arg1525z00_1840, BUNSPEC); } 
{ /* Llib/hash.scm 444 */
 obj_t BgL_bucketsz00_1820;
{ /* Llib/hash.scm 445 */
 bool_t BgL_test4595z00_8789;
{ /* Llib/hash.scm 445 */
 obj_t BgL_tmpz00_8790;
{ /* Llib/hash.scm 445 */
 obj_t BgL_res2563z00_3633;
{ /* Llib/hash.scm 445 */
 obj_t BgL_aux3136z00_6008;
BgL_aux3136z00_6008 = 
STRUCT_KEY(BgL_tablez00_68); 
if(
SYMBOLP(BgL_aux3136z00_6008))
{ /* Llib/hash.scm 445 */
BgL_res2563z00_3633 = BgL_aux3136z00_6008; }  else 
{ 
 obj_t BgL_auxz00_8794;
BgL_auxz00_8794 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17386L), BGl_string3942z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3136z00_6008); 
FAILURE(BgL_auxz00_8794,BFALSE,BFALSE);} } 
BgL_tmpz00_8790 = BgL_res2563z00_3633; } 
BgL_test4595z00_8789 = 
(BgL_tmpz00_8790==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4595z00_8789)
{ /* Llib/hash.scm 445 */
 int BgL_tmpz00_8799;
BgL_tmpz00_8799 = 
(int)(2L); 
BgL_bucketsz00_1820 = 
STRUCT_REF(BgL_tablez00_68, BgL_tmpz00_8799); }  else 
{ /* Llib/hash.scm 445 */
BgL_bucketsz00_1820 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_68); } } 
{ /* Llib/hash.scm 445 */
 long BgL_bucketszd2lenzd2_1821;
{ /* Llib/hash.scm 446 */
 obj_t BgL_vectorz00_3634;
if(
VECTORP(BgL_bucketsz00_1820))
{ /* Llib/hash.scm 446 */
BgL_vectorz00_3634 = BgL_bucketsz00_1820; }  else 
{ 
 obj_t BgL_auxz00_8805;
BgL_auxz00_8805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17445L), BGl_string3942z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1820); 
FAILURE(BgL_auxz00_8805,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1821 = 
VECTOR_LENGTH(BgL_vectorz00_3634); } 
{ /* Llib/hash.scm 446 */

{ 
 long BgL_iz00_1824; obj_t BgL_resz00_1825;
BgL_iz00_1824 = 0L; 
BgL_resz00_1825 = BNIL; 
BgL_zc3z04anonymousza31517ze3z87_1826:
if(
(BgL_iz00_1824==BgL_bucketszd2lenzd2_1821))
{ /* Llib/hash.scm 449 */
return BgL_resz00_1825;}  else 
{ /* Llib/hash.scm 451 */
 obj_t BgL_g1064z00_1828;
{ /* Llib/hash.scm 451 */
 obj_t BgL_vectorz00_3637;
if(
VECTORP(BgL_bucketsz00_1820))
{ /* Llib/hash.scm 451 */
BgL_vectorz00_3637 = BgL_bucketsz00_1820; }  else 
{ 
 obj_t BgL_auxz00_8814;
BgL_auxz00_8814 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17569L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1820); 
FAILURE(BgL_auxz00_8814,BFALSE,BFALSE);} 
BgL_g1064z00_1828 = 
VECTOR_REF(BgL_vectorz00_3637,BgL_iz00_1824); } 
{ 
 obj_t BgL_bucketz00_1830; obj_t BgL_resz00_1831;
BgL_bucketz00_1830 = BgL_g1064z00_1828; 
BgL_resz00_1831 = BgL_resz00_1825; 
BgL_zc3z04anonymousza31519ze3z87_1832:
if(
NULLP(BgL_bucketz00_1830))
{ 
 obj_t BgL_resz00_8823; long BgL_iz00_8821;
BgL_iz00_8821 = 
(BgL_iz00_1824+1L); 
BgL_resz00_8823 = BgL_resz00_1831; 
BgL_resz00_1825 = BgL_resz00_8823; 
BgL_iz00_1824 = BgL_iz00_8821; 
goto BgL_zc3z04anonymousza31517ze3z87_1826;}  else 
{ /* Llib/hash.scm 455 */
 obj_t BgL_arg1522z00_1835; obj_t BgL_arg1523z00_1836;
{ /* Llib/hash.scm 455 */
 obj_t BgL_pairz00_3640;
if(
PAIRP(BgL_bucketz00_1830))
{ /* Llib/hash.scm 455 */
BgL_pairz00_3640 = BgL_bucketz00_1830; }  else 
{ 
 obj_t BgL_auxz00_8826;
BgL_auxz00_8826 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17660L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_1830); 
FAILURE(BgL_auxz00_8826,BFALSE,BFALSE);} 
BgL_arg1522z00_1835 = 
CDR(BgL_pairz00_3640); } 
{ /* Llib/hash.scm 455 */
 obj_t BgL_arg1524z00_1837;
{ /* Llib/hash.scm 455 */
 obj_t BgL_pairz00_3641;
if(
PAIRP(BgL_bucketz00_1830))
{ /* Llib/hash.scm 455 */
BgL_pairz00_3641 = BgL_bucketz00_1830; }  else 
{ 
 obj_t BgL_auxz00_8833;
BgL_auxz00_8833 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17680L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_1830); 
FAILURE(BgL_auxz00_8833,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 455 */
 obj_t BgL_pairz00_3644;
{ /* Llib/hash.scm 455 */
 obj_t BgL_aux3146z00_6018;
BgL_aux3146z00_6018 = 
CAR(BgL_pairz00_3641); 
if(
PAIRP(BgL_aux3146z00_6018))
{ /* Llib/hash.scm 455 */
BgL_pairz00_3644 = BgL_aux3146z00_6018; }  else 
{ 
 obj_t BgL_auxz00_8840;
BgL_auxz00_8840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17674L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3146z00_6018); 
FAILURE(BgL_auxz00_8840,BFALSE,BFALSE);} } 
BgL_arg1524z00_1837 = 
CAR(BgL_pairz00_3644); } } 
BgL_arg1523z00_1836 = 
MAKE_YOUNG_PAIR(BgL_arg1524z00_1837, BgL_resz00_1831); } 
{ 
 obj_t BgL_resz00_8847; obj_t BgL_bucketz00_8846;
BgL_bucketz00_8846 = BgL_arg1522z00_1835; 
BgL_resz00_8847 = BgL_arg1523z00_1836; 
BgL_resz00_1831 = BgL_resz00_8847; 
BgL_bucketz00_1830 = BgL_bucketz00_8846; 
goto BgL_zc3z04anonymousza31519ze3z87_1832;} } } } } } } } } } 

}



/* hashtable-map */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2mapzd2zz__hashz00(obj_t BgL_tablez00_69, obj_t BgL_funz00_70)
{
{ /* Llib/hash.scm 460 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_69))
{ /* Llib/hash.scm 462 */
return 
BGl_openzd2stringzd2hashtablezd2ze3listz31zz__hashz00(BgL_tablez00_69);}  else 
{ /* Llib/hash.scm 462 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_69))
{ /* Llib/hash.scm 464 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(BgL_tablez00_69, BgL_funz00_70);}  else 
{ /* Llib/hash.scm 464 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2mapz00zz__hashz00(BgL_tablez00_69, BgL_funz00_70);} } } 

}



/* &hashtable-map */
obj_t BGl_z62hashtablezd2mapzb0zz__hashz00(obj_t BgL_envz00_5438, obj_t BgL_tablez00_5439, obj_t BgL_funz00_5440)
{
{ /* Llib/hash.scm 460 */
{ /* Llib/hash.scm 462 */
 obj_t BgL_auxz00_8862; obj_t BgL_auxz00_8855;
if(
PROCEDUREP(BgL_funz00_5440))
{ /* Llib/hash.scm 462 */
BgL_auxz00_8862 = BgL_funz00_5440
; }  else 
{ 
 obj_t BgL_auxz00_8865;
BgL_auxz00_8865 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17991L), BGl_string3943z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_funz00_5440); 
FAILURE(BgL_auxz00_8865,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5439))
{ /* Llib/hash.scm 462 */
BgL_auxz00_8855 = BgL_tablez00_5439
; }  else 
{ 
 obj_t BgL_auxz00_8858;
BgL_auxz00_8858 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(17991L), BGl_string3943z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5439); 
FAILURE(BgL_auxz00_8858,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2mapzd2zz__hashz00(BgL_auxz00_8855, BgL_auxz00_8862);} } 

}



/* open-string-hashtable-map */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2mapzd2zz__hashz00(obj_t BgL_tablez00_71, obj_t BgL_funz00_72)
{
{ /* Llib/hash.scm 472 */
{ /* Llib/hash.scm 473 */
 obj_t BgL_siza7eza7_1844;
{ /* Llib/hash.scm 473 */
 bool_t BgL_test4608z00_8870;
{ /* Llib/hash.scm 473 */
 obj_t BgL_tmpz00_8871;
{ /* Llib/hash.scm 473 */
 obj_t BgL_res2564z00_3650;
{ /* Llib/hash.scm 473 */
 obj_t BgL_aux3152z00_6024;
BgL_aux3152z00_6024 = 
STRUCT_KEY(BgL_tablez00_71); 
if(
SYMBOLP(BgL_aux3152z00_6024))
{ /* Llib/hash.scm 473 */
BgL_res2564z00_3650 = BgL_aux3152z00_6024; }  else 
{ 
 obj_t BgL_auxz00_8875;
BgL_auxz00_8875 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18484L), BGl_string3945z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3152z00_6024); 
FAILURE(BgL_auxz00_8875,BFALSE,BFALSE);} } 
BgL_tmpz00_8871 = BgL_res2564z00_3650; } 
BgL_test4608z00_8870 = 
(BgL_tmpz00_8871==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4608z00_8870)
{ /* Llib/hash.scm 473 */
 int BgL_tmpz00_8880;
BgL_tmpz00_8880 = 
(int)(1L); 
BgL_siza7eza7_1844 = 
STRUCT_REF(BgL_tablez00_71, BgL_tmpz00_8880); }  else 
{ /* Llib/hash.scm 473 */
BgL_siza7eza7_1844 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_71); } } 
{ /* Llib/hash.scm 473 */
 long BgL_siza7e3za7_1845;
{ /* Llib/hash.scm 474 */
 long BgL_za72za7_3651;
{ /* Llib/hash.scm 474 */
 obj_t BgL_tmpz00_8884;
if(
INTEGERP(BgL_siza7eza7_1844))
{ /* Llib/hash.scm 474 */
BgL_tmpz00_8884 = BgL_siza7eza7_1844
; }  else 
{ 
 obj_t BgL_auxz00_8887;
BgL_auxz00_8887 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18536L), BGl_string3945z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1844); 
FAILURE(BgL_auxz00_8887,BFALSE,BFALSE);} 
BgL_za72za7_3651 = 
(long)CINT(BgL_tmpz00_8884); } 
BgL_siza7e3za7_1845 = 
(3L*BgL_za72za7_3651); } 
{ /* Llib/hash.scm 474 */
 obj_t BgL_bucketsz00_1846;
{ /* Llib/hash.scm 475 */
 bool_t BgL_test4611z00_8893;
{ /* Llib/hash.scm 475 */
 obj_t BgL_tmpz00_8894;
{ /* Llib/hash.scm 475 */
 obj_t BgL_res2565z00_3655;
{ /* Llib/hash.scm 475 */
 obj_t BgL_aux3155z00_6027;
BgL_aux3155z00_6027 = 
STRUCT_KEY(BgL_tablez00_71); 
if(
SYMBOLP(BgL_aux3155z00_6027))
{ /* Llib/hash.scm 475 */
BgL_res2565z00_3655 = BgL_aux3155z00_6027; }  else 
{ 
 obj_t BgL_auxz00_8898;
BgL_auxz00_8898 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18555L), BGl_string3945z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3155z00_6027); 
FAILURE(BgL_auxz00_8898,BFALSE,BFALSE);} } 
BgL_tmpz00_8894 = BgL_res2565z00_3655; } 
BgL_test4611z00_8893 = 
(BgL_tmpz00_8894==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4611z00_8893)
{ /* Llib/hash.scm 475 */
 int BgL_tmpz00_8903;
BgL_tmpz00_8903 = 
(int)(2L); 
BgL_bucketsz00_1846 = 
STRUCT_REF(BgL_tablez00_71, BgL_tmpz00_8903); }  else 
{ /* Llib/hash.scm 475 */
BgL_bucketsz00_1846 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_71); } } 
{ /* Llib/hash.scm 475 */

{ 
 long BgL_iz00_1849; obj_t BgL_resz00_1850;
BgL_iz00_1849 = 0L; 
BgL_resz00_1850 = BNIL; 
BgL_zc3z04anonymousza31529ze3z87_1851:
if(
(BgL_iz00_1849==BgL_siza7e3za7_1845))
{ /* Llib/hash.scm 478 */
return BgL_resz00_1850;}  else 
{ /* Llib/hash.scm 480 */
 bool_t BgL_test4614z00_8909;
{ /* Llib/hash.scm 480 */
 bool_t BgL_test4615z00_8910;
{ /* Llib/hash.scm 480 */
 obj_t BgL_vectorz00_3658;
if(
VECTORP(BgL_bucketsz00_1846))
{ /* Llib/hash.scm 480 */
BgL_vectorz00_3658 = BgL_bucketsz00_1846; }  else 
{ 
 obj_t BgL_auxz00_8913;
BgL_auxz00_8913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18678L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1846); 
FAILURE(BgL_auxz00_8913,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 480 */
 bool_t BgL_test4617z00_8917;
{ /* Llib/hash.scm 480 */
 long BgL_tmpz00_8918;
BgL_tmpz00_8918 = 
VECTOR_LENGTH(BgL_vectorz00_3658); 
BgL_test4617z00_8917 = 
BOUND_CHECK(BgL_iz00_1849, BgL_tmpz00_8918); } 
if(BgL_test4617z00_8917)
{ /* Llib/hash.scm 480 */
BgL_test4615z00_8910 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3658,BgL_iz00_1849))
; }  else 
{ 
 obj_t BgL_auxz00_8923;
BgL_auxz00_8923 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18666L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3658, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3658)), 
(int)(BgL_iz00_1849)); 
FAILURE(BgL_auxz00_8923,BFALSE,BFALSE);} } } 
if(BgL_test4615z00_8910)
{ /* Llib/hash.scm 480 */
 long BgL_arg1549z00_1864;
BgL_arg1549z00_1864 = 
(BgL_iz00_1849+2L); 
{ /* Llib/hash.scm 480 */
 obj_t BgL_vectorz00_3661;
if(
VECTORP(BgL_bucketsz00_1846))
{ /* Llib/hash.scm 480 */
BgL_vectorz00_3661 = BgL_bucketsz00_1846; }  else 
{ 
 obj_t BgL_auxz00_8933;
BgL_auxz00_8933 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18701L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1846); 
FAILURE(BgL_auxz00_8933,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 480 */
 bool_t BgL_test4619z00_8937;
{ /* Llib/hash.scm 480 */
 long BgL_tmpz00_8938;
BgL_tmpz00_8938 = 
VECTOR_LENGTH(BgL_vectorz00_3661); 
BgL_test4619z00_8937 = 
BOUND_CHECK(BgL_arg1549z00_1864, BgL_tmpz00_8938); } 
if(BgL_test4619z00_8937)
{ /* Llib/hash.scm 480 */
BgL_test4614z00_8909 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3661,BgL_arg1549z00_1864))
; }  else 
{ 
 obj_t BgL_auxz00_8943;
BgL_auxz00_8943 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18689L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3661, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3661)), 
(int)(BgL_arg1549z00_1864)); 
FAILURE(BgL_auxz00_8943,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 480 */
BgL_test4614z00_8909 = ((bool_t)0)
; } } 
if(BgL_test4614z00_8909)
{ /* Llib/hash.scm 481 */
 long BgL_arg1537z00_1856; obj_t BgL_arg1539z00_1857;
BgL_arg1537z00_1856 = 
(BgL_iz00_1849+3L); 
{ /* Llib/hash.scm 483 */
 obj_t BgL_arg1540z00_1858;
{ /* Llib/hash.scm 483 */
 obj_t BgL_arg1543z00_1859; obj_t BgL_arg1544z00_1860;
{ /* Llib/hash.scm 483 */
 obj_t BgL_vectorz00_3664;
if(
VECTORP(BgL_bucketsz00_1846))
{ /* Llib/hash.scm 483 */
BgL_vectorz00_3664 = BgL_bucketsz00_1846; }  else 
{ 
 obj_t BgL_auxz00_8953;
BgL_auxz00_8953 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18778L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1846); 
FAILURE(BgL_auxz00_8953,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 483 */
 bool_t BgL_test4621z00_8957;
{ /* Llib/hash.scm 483 */
 long BgL_tmpz00_8958;
BgL_tmpz00_8958 = 
VECTOR_LENGTH(BgL_vectorz00_3664); 
BgL_test4621z00_8957 = 
BOUND_CHECK(BgL_iz00_1849, BgL_tmpz00_8958); } 
if(BgL_test4621z00_8957)
{ /* Llib/hash.scm 483 */
BgL_arg1543z00_1859 = 
VECTOR_REF(BgL_vectorz00_3664,BgL_iz00_1849); }  else 
{ 
 obj_t BgL_auxz00_8962;
BgL_auxz00_8962 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18766L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3664, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3664)), 
(int)(BgL_iz00_1849)); 
FAILURE(BgL_auxz00_8962,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 483 */
 long BgL_arg1546z00_1861;
BgL_arg1546z00_1861 = 
(BgL_iz00_1849+1L); 
{ /* Llib/hash.scm 483 */
 obj_t BgL_vectorz00_3667;
if(
VECTORP(BgL_bucketsz00_1846))
{ /* Llib/hash.scm 483 */
BgL_vectorz00_3667 = BgL_bucketsz00_1846; }  else 
{ 
 obj_t BgL_auxz00_8972;
BgL_auxz00_8972 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18801L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1846); 
FAILURE(BgL_auxz00_8972,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 483 */
 bool_t BgL_test4623z00_8976;
{ /* Llib/hash.scm 483 */
 long BgL_tmpz00_8977;
BgL_tmpz00_8977 = 
VECTOR_LENGTH(BgL_vectorz00_3667); 
BgL_test4623z00_8976 = 
BOUND_CHECK(BgL_arg1546z00_1861, BgL_tmpz00_8977); } 
if(BgL_test4623z00_8976)
{ /* Llib/hash.scm 483 */
BgL_arg1544z00_1860 = 
VECTOR_REF(BgL_vectorz00_3667,BgL_arg1546z00_1861); }  else 
{ 
 obj_t BgL_auxz00_8981;
BgL_auxz00_8981 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18789L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3667, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3667)), 
(int)(BgL_arg1546z00_1861)); 
FAILURE(BgL_auxz00_8981,BFALSE,BFALSE);} } } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_72, 2))
{ /* Llib/hash.scm 483 */
BgL_arg1540z00_1858 = 
BGL_PROCEDURE_CALL2(BgL_funz00_72, BgL_arg1543z00_1859, BgL_arg1544z00_1860); }  else 
{ /* Llib/hash.scm 483 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list3947z00zz__hashz00,BgL_funz00_72);} } 
BgL_arg1539z00_1857 = 
MAKE_YOUNG_PAIR(BgL_arg1540z00_1858, BgL_resz00_1850); } 
{ 
 obj_t BgL_resz00_8998; long BgL_iz00_8997;
BgL_iz00_8997 = BgL_arg1537z00_1856; 
BgL_resz00_8998 = BgL_arg1539z00_1857; 
BgL_resz00_1850 = BgL_resz00_8998; 
BgL_iz00_1849 = BgL_iz00_8997; 
goto BgL_zc3z04anonymousza31529ze3z87_1851;} }  else 
{ 
 long BgL_iz00_8999;
BgL_iz00_8999 = 
(BgL_iz00_1849+3L); 
BgL_iz00_1849 = BgL_iz00_8999; 
goto BgL_zc3z04anonymousza31529ze3z87_1851;} } } } } } } } 

}



/* &open-string-hashtable-map */
obj_t BGl_z62openzd2stringzd2hashtablezd2mapzb0zz__hashz00(obj_t BgL_envz00_5441, obj_t BgL_tablez00_5442, obj_t BgL_funz00_5443)
{
{ /* Llib/hash.scm 472 */
{ /* Llib/hash.scm 473 */
 obj_t BgL_auxz00_9008; obj_t BgL_auxz00_9001;
if(
PROCEDUREP(BgL_funz00_5443))
{ /* Llib/hash.scm 473 */
BgL_auxz00_9008 = BgL_funz00_5443
; }  else 
{ 
 obj_t BgL_auxz00_9011;
BgL_auxz00_9011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18471L), BGl_string3954z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_funz00_5443); 
FAILURE(BgL_auxz00_9011,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5442))
{ /* Llib/hash.scm 473 */
BgL_auxz00_9001 = BgL_tablez00_5442
; }  else 
{ 
 obj_t BgL_auxz00_9004;
BgL_auxz00_9004 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(18471L), BGl_string3954z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5442); 
FAILURE(BgL_auxz00_9004,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2mapzd2zz__hashz00(BgL_auxz00_9001, BgL_auxz00_9008);} } 

}



/* plain-hashtable-map */
obj_t BGl_plainzd2hashtablezd2mapz00zz__hashz00(obj_t BgL_tablez00_73, obj_t BgL_funz00_74)
{
{ /* Llib/hash.scm 490 */
{ /* Llib/hash.scm 491 */
 obj_t BgL_bucketsz00_1866;
{ /* Llib/hash.scm 491 */
 bool_t BgL_test4627z00_9016;
{ /* Llib/hash.scm 491 */
 obj_t BgL_tmpz00_9017;
{ /* Llib/hash.scm 491 */
 obj_t BgL_res2566z00_3673;
{ /* Llib/hash.scm 491 */
 obj_t BgL_aux3170z00_6043;
BgL_aux3170z00_6043 = 
STRUCT_KEY(BgL_tablez00_73); 
if(
SYMBOLP(BgL_aux3170z00_6043))
{ /* Llib/hash.scm 491 */
BgL_res2566z00_3673 = BgL_aux3170z00_6043; }  else 
{ 
 obj_t BgL_auxz00_9021;
BgL_auxz00_9021 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19166L), BGl_string3955z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3170z00_6043); 
FAILURE(BgL_auxz00_9021,BFALSE,BFALSE);} } 
BgL_tmpz00_9017 = BgL_res2566z00_3673; } 
BgL_test4627z00_9016 = 
(BgL_tmpz00_9017==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4627z00_9016)
{ /* Llib/hash.scm 491 */
 int BgL_tmpz00_9026;
BgL_tmpz00_9026 = 
(int)(2L); 
BgL_bucketsz00_1866 = 
STRUCT_REF(BgL_tablez00_73, BgL_tmpz00_9026); }  else 
{ /* Llib/hash.scm 491 */
BgL_bucketsz00_1866 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_73); } } 
{ /* Llib/hash.scm 491 */
 long BgL_bucketszd2lenzd2_1867;
{ /* Llib/hash.scm 492 */
 obj_t BgL_vectorz00_3674;
if(
VECTORP(BgL_bucketsz00_1866))
{ /* Llib/hash.scm 492 */
BgL_vectorz00_3674 = BgL_bucketsz00_1866; }  else 
{ 
 obj_t BgL_auxz00_9032;
BgL_auxz00_9032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19225L), BGl_string3955z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1866); 
FAILURE(BgL_auxz00_9032,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1867 = 
VECTOR_LENGTH(BgL_vectorz00_3674); } 
{ /* Llib/hash.scm 492 */

{ 
 long BgL_iz00_1870; obj_t BgL_resz00_1871;
BgL_iz00_1870 = 0L; 
BgL_resz00_1871 = BNIL; 
BgL_zc3z04anonymousza31550ze3z87_1872:
if(
(BgL_iz00_1870<BgL_bucketszd2lenzd2_1867))
{ /* Llib/hash.scm 496 */
 obj_t BgL_g1067z00_1874;
{ /* Llib/hash.scm 496 */
 obj_t BgL_vectorz00_3677;
if(
VECTORP(BgL_bucketsz00_1866))
{ /* Llib/hash.scm 496 */
BgL_vectorz00_3677 = BgL_bucketsz00_1866; }  else 
{ 
 obj_t BgL_auxz00_9041;
BgL_auxz00_9041 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19336L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1866); 
FAILURE(BgL_auxz00_9041,BFALSE,BFALSE);} 
BgL_g1067z00_1874 = 
VECTOR_REF(BgL_vectorz00_3677,BgL_iz00_1870); } 
{ 
 obj_t BgL_lstz00_1876; obj_t BgL_resz00_1877;
BgL_lstz00_1876 = BgL_g1067z00_1874; 
BgL_resz00_1877 = BgL_resz00_1871; 
BgL_zc3z04anonymousza31552ze3z87_1878:
if(
NULLP(BgL_lstz00_1876))
{ 
 obj_t BgL_resz00_9050; long BgL_iz00_9048;
BgL_iz00_9048 = 
(BgL_iz00_1870+1L); 
BgL_resz00_9050 = BgL_resz00_1877; 
BgL_resz00_1871 = BgL_resz00_9050; 
BgL_iz00_1870 = BgL_iz00_9048; 
goto BgL_zc3z04anonymousza31550ze3z87_1872;}  else 
{ /* Llib/hash.scm 500 */
 obj_t BgL_cellz00_1881;
{ /* Llib/hash.scm 500 */
 obj_t BgL_pairz00_3680;
if(
PAIRP(BgL_lstz00_1876))
{ /* Llib/hash.scm 500 */
BgL_pairz00_3680 = BgL_lstz00_1876; }  else 
{ 
 obj_t BgL_auxz00_9053;
BgL_auxz00_9053 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19430L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_lstz00_1876); 
FAILURE(BgL_auxz00_9053,BFALSE,BFALSE);} 
BgL_cellz00_1881 = 
CAR(BgL_pairz00_3680); } 
{ /* Llib/hash.scm 501 */
 obj_t BgL_arg1555z00_1882; obj_t BgL_arg1556z00_1883;
{ /* Llib/hash.scm 501 */
 obj_t BgL_pairz00_3681;
if(
PAIRP(BgL_lstz00_1876))
{ /* Llib/hash.scm 501 */
BgL_pairz00_3681 = BgL_lstz00_1876; }  else 
{ 
 obj_t BgL_auxz00_9060;
BgL_auxz00_9060 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19457L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_lstz00_1876); 
FAILURE(BgL_auxz00_9060,BFALSE,BFALSE);} 
BgL_arg1555z00_1882 = 
CDR(BgL_pairz00_3681); } 
{ /* Llib/hash.scm 502 */
 obj_t BgL_arg1557z00_1884;
{ /* Llib/hash.scm 502 */
 obj_t BgL_arg1558z00_1885; obj_t BgL_arg1559z00_1886;
{ /* Llib/hash.scm 502 */
 obj_t BgL_pairz00_3682;
if(
PAIRP(BgL_cellz00_1881))
{ /* Llib/hash.scm 502 */
BgL_pairz00_3682 = BgL_cellz00_1881; }  else 
{ 
 obj_t BgL_auxz00_9067;
BgL_auxz00_9067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19486L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_1881); 
FAILURE(BgL_auxz00_9067,BFALSE,BFALSE);} 
BgL_arg1558z00_1885 = 
CAR(BgL_pairz00_3682); } 
{ /* Llib/hash.scm 502 */
 obj_t BgL_pairz00_3683;
if(
PAIRP(BgL_cellz00_1881))
{ /* Llib/hash.scm 502 */
BgL_pairz00_3683 = BgL_cellz00_1881; }  else 
{ 
 obj_t BgL_auxz00_9074;
BgL_auxz00_9074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19497L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_1881); 
FAILURE(BgL_auxz00_9074,BFALSE,BFALSE);} 
BgL_arg1559z00_1886 = 
CDR(BgL_pairz00_3683); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_74, 2))
{ /* Llib/hash.scm 502 */
BgL_arg1557z00_1884 = 
BGL_PROCEDURE_CALL2(BgL_funz00_74, BgL_arg1558z00_1885, BgL_arg1559z00_1886); }  else 
{ /* Llib/hash.scm 502 */
FAILURE(BGl_string3956z00zz__hashz00,BGl_list3957z00zz__hashz00,BgL_funz00_74);} } 
BgL_arg1556z00_1883 = 
MAKE_YOUNG_PAIR(BgL_arg1557z00_1884, BgL_resz00_1877); } 
{ 
 obj_t BgL_resz00_9089; obj_t BgL_lstz00_9088;
BgL_lstz00_9088 = BgL_arg1555z00_1882; 
BgL_resz00_9089 = BgL_arg1556z00_1883; 
BgL_resz00_1877 = BgL_resz00_9089; 
BgL_lstz00_1876 = BgL_lstz00_9088; 
goto BgL_zc3z04anonymousza31552ze3z87_1878;} } } } }  else 
{ /* Llib/hash.scm 495 */
return BgL_resz00_1871;} } } } } } 

}



/* hashtable-for-each */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t BgL_tablez00_75, obj_t BgL_funz00_76)
{
{ /* Llib/hash.scm 508 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_75))
{ /* Llib/hash.scm 510 */
BGL_TAIL return 
BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00(BgL_tablez00_75, BgL_funz00_76);}  else 
{ /* Llib/hash.scm 510 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_75))
{ /* Llib/hash.scm 512 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(BgL_tablez00_75, BgL_funz00_76);}  else 
{ /* Llib/hash.scm 512 */
return 
BBOOL(
BGl_plainzd2hashtablezd2forzd2eachzd2zz__hashz00(BgL_tablez00_75, BgL_funz00_76));} } } 

}



/* &hashtable-for-each */
obj_t BGl_z62hashtablezd2forzd2eachz62zz__hashz00(obj_t BgL_envz00_5444, obj_t BgL_tablez00_5445, obj_t BgL_funz00_5446)
{
{ /* Llib/hash.scm 508 */
{ /* Llib/hash.scm 510 */
 obj_t BgL_auxz00_9105; obj_t BgL_auxz00_9098;
if(
PROCEDUREP(BgL_funz00_5446))
{ /* Llib/hash.scm 510 */
BgL_auxz00_9105 = BgL_funz00_5446
; }  else 
{ 
 obj_t BgL_auxz00_9108;
BgL_auxz00_9108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19823L), BGl_string3962z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_funz00_5446); 
FAILURE(BgL_auxz00_9108,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5445))
{ /* Llib/hash.scm 510 */
BgL_auxz00_9098 = BgL_tablez00_5445
; }  else 
{ 
 obj_t BgL_auxz00_9101;
BgL_auxz00_9101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(19823L), BGl_string3962z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5445); 
FAILURE(BgL_auxz00_9101,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_auxz00_9098, BgL_auxz00_9105);} } 

}



/* open-string-hashtable-for-each */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00(obj_t BgL_tablez00_77, obj_t BgL_funz00_78)
{
{ /* Llib/hash.scm 520 */
{ /* Llib/hash.scm 521 */
 obj_t BgL_siza7eza7_1891;
{ /* Llib/hash.scm 521 */
 bool_t BgL_test4642z00_9113;
{ /* Llib/hash.scm 521 */
 obj_t BgL_tmpz00_9114;
{ /* Llib/hash.scm 521 */
 obj_t BgL_res2567z00_3689;
{ /* Llib/hash.scm 521 */
 obj_t BgL_aux3189z00_6063;
BgL_aux3189z00_6063 = 
STRUCT_KEY(BgL_tablez00_77); 
if(
SYMBOLP(BgL_aux3189z00_6063))
{ /* Llib/hash.scm 521 */
BgL_res2567z00_3689 = BgL_aux3189z00_6063; }  else 
{ 
 obj_t BgL_auxz00_9118;
BgL_auxz00_9118 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20338L), BGl_string3963z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3189z00_6063); 
FAILURE(BgL_auxz00_9118,BFALSE,BFALSE);} } 
BgL_tmpz00_9114 = BgL_res2567z00_3689; } 
BgL_test4642z00_9113 = 
(BgL_tmpz00_9114==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4642z00_9113)
{ /* Llib/hash.scm 521 */
 int BgL_tmpz00_9123;
BgL_tmpz00_9123 = 
(int)(1L); 
BgL_siza7eza7_1891 = 
STRUCT_REF(BgL_tablez00_77, BgL_tmpz00_9123); }  else 
{ /* Llib/hash.scm 521 */
BgL_siza7eza7_1891 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_77); } } 
{ /* Llib/hash.scm 521 */
 long BgL_siza7e3za7_1892;
{ /* Llib/hash.scm 522 */
 long BgL_za72za7_3690;
{ /* Llib/hash.scm 522 */
 obj_t BgL_tmpz00_9127;
if(
INTEGERP(BgL_siza7eza7_1891))
{ /* Llib/hash.scm 522 */
BgL_tmpz00_9127 = BgL_siza7eza7_1891
; }  else 
{ 
 obj_t BgL_auxz00_9130;
BgL_auxz00_9130 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20390L), BGl_string3963z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1891); 
FAILURE(BgL_auxz00_9130,BFALSE,BFALSE);} 
BgL_za72za7_3690 = 
(long)CINT(BgL_tmpz00_9127); } 
BgL_siza7e3za7_1892 = 
(3L*BgL_za72za7_3690); } 
{ /* Llib/hash.scm 522 */
 obj_t BgL_bucketsz00_1893;
{ /* Llib/hash.scm 523 */
 bool_t BgL_test4645z00_9136;
{ /* Llib/hash.scm 523 */
 obj_t BgL_tmpz00_9137;
{ /* Llib/hash.scm 523 */
 obj_t BgL_res2568z00_3694;
{ /* Llib/hash.scm 523 */
 obj_t BgL_aux3192z00_6066;
BgL_aux3192z00_6066 = 
STRUCT_KEY(BgL_tablez00_77); 
if(
SYMBOLP(BgL_aux3192z00_6066))
{ /* Llib/hash.scm 523 */
BgL_res2568z00_3694 = BgL_aux3192z00_6066; }  else 
{ 
 obj_t BgL_auxz00_9141;
BgL_auxz00_9141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20409L), BGl_string3963z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3192z00_6066); 
FAILURE(BgL_auxz00_9141,BFALSE,BFALSE);} } 
BgL_tmpz00_9137 = BgL_res2568z00_3694; } 
BgL_test4645z00_9136 = 
(BgL_tmpz00_9137==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4645z00_9136)
{ /* Llib/hash.scm 523 */
 int BgL_tmpz00_9146;
BgL_tmpz00_9146 = 
(int)(2L); 
BgL_bucketsz00_1893 = 
STRUCT_REF(BgL_tablez00_77, BgL_tmpz00_9146); }  else 
{ /* Llib/hash.scm 523 */
BgL_bucketsz00_1893 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_77); } } 
{ /* Llib/hash.scm 523 */

{ 
 long BgL_iz00_1895;
{ /* Llib/hash.scm 524 */
 bool_t BgL_tmpz00_9150;
BgL_iz00_1895 = 0L; 
BgL_zc3z04anonymousza31562ze3z87_1896:
if(
(BgL_iz00_1895==BgL_siza7e3za7_1892))
{ /* Llib/hash.scm 525 */
BgL_tmpz00_9150 = ((bool_t)0)
; }  else 
{ /* Llib/hash.scm 525 */
{ /* Llib/hash.scm 526 */
 bool_t BgL_test4648z00_9153;
{ /* Llib/hash.scm 526 */
 bool_t BgL_test4649z00_9154;
{ /* Llib/hash.scm 526 */
 obj_t BgL_vectorz00_3697;
if(
VECTORP(BgL_bucketsz00_1893))
{ /* Llib/hash.scm 526 */
BgL_vectorz00_3697 = BgL_bucketsz00_1893; }  else 
{ 
 obj_t BgL_auxz00_9157;
BgL_auxz00_9157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20514L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1893); 
FAILURE(BgL_auxz00_9157,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 526 */
 bool_t BgL_test4651z00_9161;
{ /* Llib/hash.scm 526 */
 long BgL_tmpz00_9162;
BgL_tmpz00_9162 = 
VECTOR_LENGTH(BgL_vectorz00_3697); 
BgL_test4651z00_9161 = 
BOUND_CHECK(BgL_iz00_1895, BgL_tmpz00_9162); } 
if(BgL_test4651z00_9161)
{ /* Llib/hash.scm 526 */
BgL_test4649z00_9154 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3697,BgL_iz00_1895))
; }  else 
{ 
 obj_t BgL_auxz00_9167;
BgL_auxz00_9167 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20502L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3697, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3697)), 
(int)(BgL_iz00_1895)); 
FAILURE(BgL_auxz00_9167,BFALSE,BFALSE);} } } 
if(BgL_test4649z00_9154)
{ /* Llib/hash.scm 526 */
 long BgL_arg1575z00_1905;
BgL_arg1575z00_1905 = 
(BgL_iz00_1895+2L); 
{ /* Llib/hash.scm 526 */
 obj_t BgL_vectorz00_3700;
if(
VECTORP(BgL_bucketsz00_1893))
{ /* Llib/hash.scm 526 */
BgL_vectorz00_3700 = BgL_bucketsz00_1893; }  else 
{ 
 obj_t BgL_auxz00_9177;
BgL_auxz00_9177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20537L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1893); 
FAILURE(BgL_auxz00_9177,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 526 */
 bool_t BgL_test4653z00_9181;
{ /* Llib/hash.scm 526 */
 long BgL_tmpz00_9182;
BgL_tmpz00_9182 = 
VECTOR_LENGTH(BgL_vectorz00_3700); 
BgL_test4653z00_9181 = 
BOUND_CHECK(BgL_arg1575z00_1905, BgL_tmpz00_9182); } 
if(BgL_test4653z00_9181)
{ /* Llib/hash.scm 526 */
BgL_test4648z00_9153 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3700,BgL_arg1575z00_1905))
; }  else 
{ 
 obj_t BgL_auxz00_9187;
BgL_auxz00_9187 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20525L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3700, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3700)), 
(int)(BgL_arg1575z00_1905)); 
FAILURE(BgL_auxz00_9187,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 526 */
BgL_test4648z00_9153 = ((bool_t)0)
; } } 
if(BgL_test4648z00_9153)
{ /* Llib/hash.scm 527 */
 obj_t BgL_arg1567z00_1901; obj_t BgL_arg1571z00_1902;
{ /* Llib/hash.scm 527 */
 obj_t BgL_vectorz00_3702;
if(
VECTORP(BgL_bucketsz00_1893))
{ /* Llib/hash.scm 527 */
BgL_vectorz00_3702 = BgL_bucketsz00_1893; }  else 
{ 
 obj_t BgL_auxz00_9196;
BgL_auxz00_9196 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20582L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1893); 
FAILURE(BgL_auxz00_9196,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 527 */
 bool_t BgL_test4655z00_9200;
{ /* Llib/hash.scm 527 */
 long BgL_tmpz00_9201;
BgL_tmpz00_9201 = 
VECTOR_LENGTH(BgL_vectorz00_3702); 
BgL_test4655z00_9200 = 
BOUND_CHECK(BgL_iz00_1895, BgL_tmpz00_9201); } 
if(BgL_test4655z00_9200)
{ /* Llib/hash.scm 527 */
BgL_arg1567z00_1901 = 
VECTOR_REF(BgL_vectorz00_3702,BgL_iz00_1895); }  else 
{ 
 obj_t BgL_auxz00_9205;
BgL_auxz00_9205 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20570L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3702, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3702)), 
(int)(BgL_iz00_1895)); 
FAILURE(BgL_auxz00_9205,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 527 */
 long BgL_arg1573z00_1903;
BgL_arg1573z00_1903 = 
(BgL_iz00_1895+1L); 
{ /* Llib/hash.scm 527 */
 obj_t BgL_vectorz00_3705;
if(
VECTORP(BgL_bucketsz00_1893))
{ /* Llib/hash.scm 527 */
BgL_vectorz00_3705 = BgL_bucketsz00_1893; }  else 
{ 
 obj_t BgL_auxz00_9215;
BgL_auxz00_9215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20605L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1893); 
FAILURE(BgL_auxz00_9215,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 527 */
 bool_t BgL_test4657z00_9219;
{ /* Llib/hash.scm 527 */
 long BgL_tmpz00_9220;
BgL_tmpz00_9220 = 
VECTOR_LENGTH(BgL_vectorz00_3705); 
BgL_test4657z00_9219 = 
BOUND_CHECK(BgL_arg1573z00_1903, BgL_tmpz00_9220); } 
if(BgL_test4657z00_9219)
{ /* Llib/hash.scm 527 */
BgL_arg1571z00_1902 = 
VECTOR_REF(BgL_vectorz00_3705,BgL_arg1573z00_1903); }  else 
{ 
 obj_t BgL_auxz00_9224;
BgL_auxz00_9224 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20593L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3705, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3705)), 
(int)(BgL_arg1573z00_1903)); 
FAILURE(BgL_auxz00_9224,BFALSE,BFALSE);} } } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_78, 2))
{ /* Llib/hash.scm 527 */
BGL_PROCEDURE_CALL2(BgL_funz00_78, BgL_arg1567z00_1901, BgL_arg1571z00_1902); }  else 
{ /* Llib/hash.scm 527 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list3964z00zz__hashz00,BgL_funz00_78);} }  else 
{ /* Llib/hash.scm 526 */BFALSE; } } 
{ 
 long BgL_iz00_9239;
BgL_iz00_9239 = 
(BgL_iz00_1895+3L); 
BgL_iz00_1895 = BgL_iz00_9239; 
goto BgL_zc3z04anonymousza31562ze3z87_1896;} } 
return 
BBOOL(BgL_tmpz00_9150);} } } } } } } 

}



/* &open-string-hashtable-for-each */
obj_t BGl_z62openzd2stringzd2hashtablezd2forzd2eachz62zz__hashz00(obj_t BgL_envz00_5447, obj_t BgL_tablez00_5448, obj_t BgL_funz00_5449)
{
{ /* Llib/hash.scm 520 */
{ /* Llib/hash.scm 521 */
 obj_t BgL_auxz00_9249; obj_t BgL_auxz00_9242;
if(
PROCEDUREP(BgL_funz00_5449))
{ /* Llib/hash.scm 521 */
BgL_auxz00_9249 = BgL_funz00_5449
; }  else 
{ 
 obj_t BgL_auxz00_9252;
BgL_auxz00_9252 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20325L), BGl_string3969z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_funz00_5449); 
FAILURE(BgL_auxz00_9252,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5448))
{ /* Llib/hash.scm 521 */
BgL_auxz00_9242 = BgL_tablez00_5448
; }  else 
{ 
 obj_t BgL_auxz00_9245;
BgL_auxz00_9245 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20325L), BGl_string3969z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5448); 
FAILURE(BgL_auxz00_9245,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2forzd2eachz00zz__hashz00(BgL_auxz00_9242, BgL_auxz00_9249);} } 

}



/* plain-hashtable-for-each */
bool_t BGl_plainzd2hashtablezd2forzd2eachzd2zz__hashz00(obj_t BgL_tablez00_79, obj_t BgL_funz00_80)
{
{ /* Llib/hash.scm 533 */
{ /* Llib/hash.scm 534 */
 obj_t BgL_bucketsz00_1908;
{ /* Llib/hash.scm 534 */
 bool_t BgL_test4661z00_9257;
{ /* Llib/hash.scm 534 */
 obj_t BgL_tmpz00_9258;
{ /* Llib/hash.scm 534 */
 obj_t BgL_res2569z00_3711;
{ /* Llib/hash.scm 534 */
 obj_t BgL_aux3207z00_6082;
BgL_aux3207z00_6082 = 
STRUCT_KEY(BgL_tablez00_79); 
if(
SYMBOLP(BgL_aux3207z00_6082))
{ /* Llib/hash.scm 534 */
BgL_res2569z00_3711 = BgL_aux3207z00_6082; }  else 
{ 
 obj_t BgL_auxz00_9262;
BgL_auxz00_9262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(20958L), BGl_string3970z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3207z00_6082); 
FAILURE(BgL_auxz00_9262,BFALSE,BFALSE);} } 
BgL_tmpz00_9258 = BgL_res2569z00_3711; } 
BgL_test4661z00_9257 = 
(BgL_tmpz00_9258==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4661z00_9257)
{ /* Llib/hash.scm 534 */
 int BgL_tmpz00_9267;
BgL_tmpz00_9267 = 
(int)(2L); 
BgL_bucketsz00_1908 = 
STRUCT_REF(BgL_tablez00_79, BgL_tmpz00_9267); }  else 
{ /* Llib/hash.scm 534 */
BgL_bucketsz00_1908 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_79); } } 
{ /* Llib/hash.scm 534 */
 long BgL_bucketszd2lenzd2_1909;
{ /* Llib/hash.scm 535 */
 obj_t BgL_vectorz00_3712;
if(
VECTORP(BgL_bucketsz00_1908))
{ /* Llib/hash.scm 535 */
BgL_vectorz00_3712 = BgL_bucketsz00_1908; }  else 
{ 
 obj_t BgL_auxz00_9273;
BgL_auxz00_9273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(21017L), BGl_string3970z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1908); 
FAILURE(BgL_auxz00_9273,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1909 = 
VECTOR_LENGTH(BgL_vectorz00_3712); } 
{ /* Llib/hash.scm 535 */

{ 
 long BgL_iz00_1911;
BgL_iz00_1911 = 0L; 
BgL_zc3z04anonymousza31577ze3z87_1912:
if(
(BgL_iz00_1911<BgL_bucketszd2lenzd2_1909))
{ /* Llib/hash.scm 537 */
{ /* Llib/hash.scm 539 */
 obj_t BgL_g1137z00_1914;
{ /* Llib/hash.scm 541 */
 obj_t BgL_vectorz00_3715;
if(
VECTORP(BgL_bucketsz00_1908))
{ /* Llib/hash.scm 541 */
BgL_vectorz00_3715 = BgL_bucketsz00_1908; }  else 
{ 
 obj_t BgL_auxz00_9282;
BgL_auxz00_9282 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(21175L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1908); 
FAILURE(BgL_auxz00_9282,BFALSE,BFALSE);} 
BgL_g1137z00_1914 = 
VECTOR_REF(BgL_vectorz00_3715,BgL_iz00_1911); } 
{ 
 obj_t BgL_l1135z00_1916;
BgL_l1135z00_1916 = BgL_g1137z00_1914; 
BgL_zc3z04anonymousza31579ze3z87_1917:
if(
PAIRP(BgL_l1135z00_1916))
{ /* Llib/hash.scm 541 */
{ /* Llib/hash.scm 540 */
 obj_t BgL_cellz00_1919;
BgL_cellz00_1919 = 
CAR(BgL_l1135z00_1916); 
{ /* Llib/hash.scm 540 */
 obj_t BgL_arg1582z00_1920; obj_t BgL_arg1583z00_1921;
{ /* Llib/hash.scm 540 */
 obj_t BgL_pairz00_3718;
if(
PAIRP(BgL_cellz00_1919))
{ /* Llib/hash.scm 540 */
BgL_pairz00_3718 = BgL_cellz00_1919; }  else 
{ 
 obj_t BgL_auxz00_9292;
BgL_auxz00_9292 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(21136L), BGl_string3971z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_1919); 
FAILURE(BgL_auxz00_9292,BFALSE,BFALSE);} 
BgL_arg1582z00_1920 = 
CAR(BgL_pairz00_3718); } 
{ /* Llib/hash.scm 540 */
 obj_t BgL_pairz00_3719;
if(
PAIRP(BgL_cellz00_1919))
{ /* Llib/hash.scm 540 */
BgL_pairz00_3719 = BgL_cellz00_1919; }  else 
{ 
 obj_t BgL_auxz00_9299;
BgL_auxz00_9299 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(21147L), BGl_string3971z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_1919); 
FAILURE(BgL_auxz00_9299,BFALSE,BFALSE);} 
BgL_arg1583z00_1921 = 
CDR(BgL_pairz00_3719); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_80, 2))
{ /* Llib/hash.scm 540 */
BGL_PROCEDURE_CALL2(BgL_funz00_80, BgL_arg1582z00_1920, BgL_arg1583z00_1921); }  else 
{ /* Llib/hash.scm 540 */
FAILURE(BGl_string3972z00zz__hashz00,BGl_list3973z00zz__hashz00,BgL_funz00_80);} } } 
{ 
 obj_t BgL_l1135z00_9312;
BgL_l1135z00_9312 = 
CDR(BgL_l1135z00_1916); 
BgL_l1135z00_1916 = BgL_l1135z00_9312; 
goto BgL_zc3z04anonymousza31579ze3z87_1917;} }  else 
{ /* Llib/hash.scm 541 */
if(
NULLP(BgL_l1135z00_1916))
{ /* Llib/hash.scm 541 */BTRUE; }  else 
{ /* Llib/hash.scm 541 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3978z00zz__hashz00, BGl_string3979z00zz__hashz00, BgL_l1135z00_1916, BGl_string3876z00zz__hashz00, 
BINT(21093L)); } } } } 
{ 
 long BgL_iz00_9318;
BgL_iz00_9318 = 
(BgL_iz00_1911+1L); 
BgL_iz00_1911 = BgL_iz00_9318; 
goto BgL_zc3z04anonymousza31577ze3z87_1912;} }  else 
{ /* Llib/hash.scm 537 */
return ((bool_t)0);} } } } } } 

}



/* hashtable-filter! */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2filterz12zc0zz__hashz00(obj_t BgL_tablez00_81, obj_t BgL_funz00_82)
{
{ /* Llib/hash.scm 547 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_81))
{ /* Llib/hash.scm 549 */
BGL_TAIL return 
BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00(BgL_tablez00_81, BgL_funz00_82);}  else 
{ /* Llib/hash.scm 549 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_81))
{ /* Llib/hash.scm 551 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(BgL_tablez00_81, BgL_funz00_82);}  else 
{ /* Llib/hash.scm 551 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2filterz12z12zz__hashz00(BgL_tablez00_81, BgL_funz00_82);} } } 

}



/* &hashtable-filter! */
obj_t BGl_z62hashtablezd2filterz12za2zz__hashz00(obj_t BgL_envz00_5450, obj_t BgL_tablez00_5451, obj_t BgL_funz00_5452)
{
{ /* Llib/hash.scm 547 */
{ /* Llib/hash.scm 549 */
 obj_t BgL_auxz00_9334; obj_t BgL_auxz00_9327;
if(
PROCEDUREP(BgL_funz00_5452))
{ /* Llib/hash.scm 549 */
BgL_auxz00_9334 = BgL_funz00_5452
; }  else 
{ 
 obj_t BgL_auxz00_9337;
BgL_auxz00_9337 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(21506L), BGl_string3980z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_funz00_5452); 
FAILURE(BgL_auxz00_9337,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5451))
{ /* Llib/hash.scm 549 */
BgL_auxz00_9327 = BgL_tablez00_5451
; }  else 
{ 
 obj_t BgL_auxz00_9330;
BgL_auxz00_9330 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(21506L), BGl_string3980z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5451); 
FAILURE(BgL_auxz00_9330,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2filterz12zc0zz__hashz00(BgL_auxz00_9327, BgL_auxz00_9334);} } 

}



/* open-string-hashtable-filter! */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00(obj_t BgL_tablez00_83, obj_t BgL_funz00_84)
{
{ /* Llib/hash.scm 559 */
{ /* Llib/hash.scm 560 */
 obj_t BgL_siza7eza7_1929;
{ /* Llib/hash.scm 560 */
 bool_t BgL_test4675z00_9342;
{ /* Llib/hash.scm 560 */
 obj_t BgL_tmpz00_9343;
{ /* Llib/hash.scm 560 */
 obj_t BgL_res2570z00_3727;
{ /* Llib/hash.scm 560 */
 obj_t BgL_aux3222z00_6098;
BgL_aux3222z00_6098 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3222z00_6098))
{ /* Llib/hash.scm 560 */
BgL_res2570z00_3727 = BgL_aux3222z00_6098; }  else 
{ 
 obj_t BgL_auxz00_9347;
BgL_auxz00_9347 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22017L), BGl_string3981z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3222z00_6098); 
FAILURE(BgL_auxz00_9347,BFALSE,BFALSE);} } 
BgL_tmpz00_9343 = BgL_res2570z00_3727; } 
BgL_test4675z00_9342 = 
(BgL_tmpz00_9343==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4675z00_9342)
{ /* Llib/hash.scm 560 */
 int BgL_tmpz00_9352;
BgL_tmpz00_9352 = 
(int)(1L); 
BgL_siza7eza7_1929 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_9352); }  else 
{ /* Llib/hash.scm 560 */
BgL_siza7eza7_1929 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_83); } } 
{ /* Llib/hash.scm 560 */
 long BgL_siza7e3za7_1930;
{ /* Llib/hash.scm 561 */
 long BgL_za72za7_3728;
{ /* Llib/hash.scm 561 */
 obj_t BgL_tmpz00_9356;
if(
INTEGERP(BgL_siza7eza7_1929))
{ /* Llib/hash.scm 561 */
BgL_tmpz00_9356 = BgL_siza7eza7_1929
; }  else 
{ 
 obj_t BgL_auxz00_9359;
BgL_auxz00_9359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22069L), BGl_string3981z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1929); 
FAILURE(BgL_auxz00_9359,BFALSE,BFALSE);} 
BgL_za72za7_3728 = 
(long)CINT(BgL_tmpz00_9356); } 
BgL_siza7e3za7_1930 = 
(3L*BgL_za72za7_3728); } 
{ /* Llib/hash.scm 561 */
 obj_t BgL_bucketsz00_1931;
{ /* Llib/hash.scm 562 */
 bool_t BgL_test4678z00_9365;
{ /* Llib/hash.scm 562 */
 obj_t BgL_tmpz00_9366;
{ /* Llib/hash.scm 562 */
 obj_t BgL_res2571z00_3732;
{ /* Llib/hash.scm 562 */
 obj_t BgL_aux3225z00_6101;
BgL_aux3225z00_6101 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3225z00_6101))
{ /* Llib/hash.scm 562 */
BgL_res2571z00_3732 = BgL_aux3225z00_6101; }  else 
{ 
 obj_t BgL_auxz00_9370;
BgL_auxz00_9370 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22088L), BGl_string3981z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3225z00_6101); 
FAILURE(BgL_auxz00_9370,BFALSE,BFALSE);} } 
BgL_tmpz00_9366 = BgL_res2571z00_3732; } 
BgL_test4678z00_9365 = 
(BgL_tmpz00_9366==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4678z00_9365)
{ /* Llib/hash.scm 562 */
 int BgL_tmpz00_9375;
BgL_tmpz00_9375 = 
(int)(2L); 
BgL_bucketsz00_1931 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_9375); }  else 
{ /* Llib/hash.scm 562 */
BgL_bucketsz00_1931 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_83); } } 
{ /* Llib/hash.scm 562 */

{ 
 long BgL_iz00_1933;
{ /* Llib/hash.scm 563 */
 bool_t BgL_tmpz00_9379;
BgL_iz00_1933 = 0L; 
BgL_zc3z04anonymousza31589ze3z87_1934:
if(
(BgL_iz00_1933==BgL_siza7e3za7_1930))
{ /* Llib/hash.scm 564 */
BgL_tmpz00_9379 = ((bool_t)0)
; }  else 
{ /* Llib/hash.scm 564 */
{ /* Llib/hash.scm 565 */
 bool_t BgL_test4681z00_9382;
{ /* Llib/hash.scm 565 */
 bool_t BgL_test4682z00_9383;
{ /* Llib/hash.scm 565 */
 obj_t BgL_vectorz00_3735;
if(
VECTORP(BgL_bucketsz00_1931))
{ /* Llib/hash.scm 565 */
BgL_vectorz00_3735 = BgL_bucketsz00_1931; }  else 
{ 
 obj_t BgL_auxz00_9386;
BgL_auxz00_9386 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22193L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1931); 
FAILURE(BgL_auxz00_9386,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 565 */
 bool_t BgL_test4684z00_9390;
{ /* Llib/hash.scm 565 */
 long BgL_tmpz00_9391;
BgL_tmpz00_9391 = 
VECTOR_LENGTH(BgL_vectorz00_3735); 
BgL_test4684z00_9390 = 
BOUND_CHECK(BgL_iz00_1933, BgL_tmpz00_9391); } 
if(BgL_test4684z00_9390)
{ /* Llib/hash.scm 565 */
BgL_test4682z00_9383 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3735,BgL_iz00_1933))
; }  else 
{ 
 obj_t BgL_auxz00_9396;
BgL_auxz00_9396 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22181L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3735, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3735)), 
(int)(BgL_iz00_1933)); 
FAILURE(BgL_auxz00_9396,BFALSE,BFALSE);} } } 
if(BgL_test4682z00_9383)
{ /* Llib/hash.scm 565 */
 long BgL_arg1609z00_1949;
BgL_arg1609z00_1949 = 
(BgL_iz00_1933+2L); 
{ /* Llib/hash.scm 565 */
 obj_t BgL_vectorz00_3738;
if(
VECTORP(BgL_bucketsz00_1931))
{ /* Llib/hash.scm 565 */
BgL_vectorz00_3738 = BgL_bucketsz00_1931; }  else 
{ 
 obj_t BgL_auxz00_9406;
BgL_auxz00_9406 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22216L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1931); 
FAILURE(BgL_auxz00_9406,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 565 */
 bool_t BgL_test4686z00_9410;
{ /* Llib/hash.scm 565 */
 long BgL_tmpz00_9411;
BgL_tmpz00_9411 = 
VECTOR_LENGTH(BgL_vectorz00_3738); 
BgL_test4686z00_9410 = 
BOUND_CHECK(BgL_arg1609z00_1949, BgL_tmpz00_9411); } 
if(BgL_test4686z00_9410)
{ /* Llib/hash.scm 565 */
BgL_test4681z00_9382 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3738,BgL_arg1609z00_1949))
; }  else 
{ 
 obj_t BgL_auxz00_9416;
BgL_auxz00_9416 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22204L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3738, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3738)), 
(int)(BgL_arg1609z00_1949)); 
FAILURE(BgL_auxz00_9416,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 565 */
BgL_test4681z00_9382 = ((bool_t)0)
; } } 
if(BgL_test4681z00_9382)
{ /* Llib/hash.scm 566 */
 bool_t BgL_test4687z00_9423;
{ /* Llib/hash.scm 566 */
 obj_t BgL_arg1606z00_1945; obj_t BgL_arg1607z00_1946;
{ /* Llib/hash.scm 566 */
 obj_t BgL_vectorz00_3740;
if(
VECTORP(BgL_bucketsz00_1931))
{ /* Llib/hash.scm 566 */
BgL_vectorz00_3740 = BgL_bucketsz00_1931; }  else 
{ 
 obj_t BgL_auxz00_9426;
BgL_auxz00_9426 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22269L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1931); 
FAILURE(BgL_auxz00_9426,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 566 */
 bool_t BgL_test4689z00_9430;
{ /* Llib/hash.scm 566 */
 long BgL_tmpz00_9431;
BgL_tmpz00_9431 = 
VECTOR_LENGTH(BgL_vectorz00_3740); 
BgL_test4689z00_9430 = 
BOUND_CHECK(BgL_iz00_1933, BgL_tmpz00_9431); } 
if(BgL_test4689z00_9430)
{ /* Llib/hash.scm 566 */
BgL_arg1606z00_1945 = 
VECTOR_REF(BgL_vectorz00_3740,BgL_iz00_1933); }  else 
{ 
 obj_t BgL_auxz00_9435;
BgL_auxz00_9435 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22257L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3740, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3740)), 
(int)(BgL_iz00_1933)); 
FAILURE(BgL_auxz00_9435,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 566 */
 long BgL_arg1608z00_1947;
BgL_arg1608z00_1947 = 
(BgL_iz00_1933+1L); 
{ /* Llib/hash.scm 566 */
 obj_t BgL_vectorz00_3743;
if(
VECTORP(BgL_bucketsz00_1931))
{ /* Llib/hash.scm 566 */
BgL_vectorz00_3743 = BgL_bucketsz00_1931; }  else 
{ 
 obj_t BgL_auxz00_9445;
BgL_auxz00_9445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22292L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1931); 
FAILURE(BgL_auxz00_9445,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 566 */
 bool_t BgL_test4691z00_9449;
{ /* Llib/hash.scm 566 */
 long BgL_tmpz00_9450;
BgL_tmpz00_9450 = 
VECTOR_LENGTH(BgL_vectorz00_3743); 
BgL_test4691z00_9449 = 
BOUND_CHECK(BgL_arg1608z00_1947, BgL_tmpz00_9450); } 
if(BgL_test4691z00_9449)
{ /* Llib/hash.scm 566 */
BgL_arg1607z00_1946 = 
VECTOR_REF(BgL_vectorz00_3743,BgL_arg1608z00_1947); }  else 
{ 
 obj_t BgL_auxz00_9454;
BgL_auxz00_9454 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22280L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3743, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3743)), 
(int)(BgL_arg1608z00_1947)); 
FAILURE(BgL_auxz00_9454,BFALSE,BFALSE);} } } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_84, 2))
{ /* Llib/hash.scm 566 */
BgL_test4687z00_9423 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_funz00_84, BgL_arg1606z00_1945, BgL_arg1607z00_1946))
; }  else 
{ /* Llib/hash.scm 566 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list3982z00zz__hashz00,BgL_funz00_84);} } 
if(BgL_test4687z00_9423)
{ /* Llib/hash.scm 566 */BFALSE; }  else 
{ /* Llib/hash.scm 566 */
{ /* Llib/hash.scm 567 */
 long BgL_arg1603z00_1943;
BgL_arg1603z00_1943 = 
(BgL_iz00_1933+1L); 
{ /* Llib/hash.scm 567 */
 obj_t BgL_vectorz00_3746;
if(
VECTORP(BgL_bucketsz00_1931))
{ /* Llib/hash.scm 567 */
BgL_vectorz00_3746 = BgL_bucketsz00_1931; }  else 
{ 
 obj_t BgL_auxz00_9473;
BgL_auxz00_9473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22329L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1931); 
FAILURE(BgL_auxz00_9473,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 567 */
 bool_t BgL_test4694z00_9477;
{ /* Llib/hash.scm 567 */
 long BgL_tmpz00_9478;
BgL_tmpz00_9478 = 
VECTOR_LENGTH(BgL_vectorz00_3746); 
BgL_test4694z00_9477 = 
BOUND_CHECK(BgL_arg1603z00_1943, BgL_tmpz00_9478); } 
if(BgL_test4694z00_9477)
{ /* Llib/hash.scm 567 */
VECTOR_SET(BgL_vectorz00_3746,BgL_arg1603z00_1943,BFALSE); }  else 
{ 
 obj_t BgL_auxz00_9482;
BgL_auxz00_9482 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22316L), BGl_string3933z00zz__hashz00, BgL_vectorz00_3746, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3746)), 
(int)(BgL_arg1603z00_1943)); 
FAILURE(BgL_auxz00_9482,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 568 */
 long BgL_arg1605z00_1944;
BgL_arg1605z00_1944 = 
(BgL_iz00_1933+2L); 
{ /* Llib/hash.scm 568 */
 obj_t BgL_vectorz00_3749;
if(
VECTORP(BgL_bucketsz00_1931))
{ /* Llib/hash.scm 568 */
BgL_vectorz00_3749 = BgL_bucketsz00_1931; }  else 
{ 
 obj_t BgL_auxz00_9492;
BgL_auxz00_9492 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22368L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1931); 
FAILURE(BgL_auxz00_9492,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 568 */
 bool_t BgL_test4696z00_9496;
{ /* Llib/hash.scm 568 */
 long BgL_tmpz00_9497;
BgL_tmpz00_9497 = 
VECTOR_LENGTH(BgL_vectorz00_3749); 
BgL_test4696z00_9496 = 
BOUND_CHECK(BgL_arg1605z00_1944, BgL_tmpz00_9497); } 
if(BgL_test4696z00_9496)
{ /* Llib/hash.scm 568 */
VECTOR_SET(BgL_vectorz00_3749,BgL_arg1605z00_1944,BFALSE); }  else 
{ 
 obj_t BgL_auxz00_9501;
BgL_auxz00_9501 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22355L), BGl_string3933z00zz__hashz00, BgL_vectorz00_3749, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3749)), 
(int)(BgL_arg1605z00_1944)); 
FAILURE(BgL_auxz00_9501,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 1303 */
 long BgL_arg2044z00_3751;
{ /* Llib/hash.scm 1303 */
 obj_t BgL_arg2045z00_3752;
{ /* Llib/hash.scm 1272 */
 bool_t BgL_test4697z00_9508;
{ /* Llib/hash.scm 1272 */
 obj_t BgL_tmpz00_9509;
{ /* Llib/hash.scm 1272 */
 obj_t BgL_res2572z00_3756;
{ /* Llib/hash.scm 1272 */
 obj_t BgL_aux3240z00_6117;
BgL_aux3240z00_6117 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3240z00_6117))
{ /* Llib/hash.scm 1272 */
BgL_res2572z00_3756 = BgL_aux3240z00_6117; }  else 
{ 
 obj_t BgL_auxz00_9513;
BgL_auxz00_9513 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50571L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3240z00_6117); 
FAILURE(BgL_auxz00_9513,BFALSE,BFALSE);} } 
BgL_tmpz00_9509 = BgL_res2572z00_3756; } 
BgL_test4697z00_9508 = 
(BgL_tmpz00_9509==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4697z00_9508)
{ /* Llib/hash.scm 1272 */
 int BgL_tmpz00_9518;
BgL_tmpz00_9518 = 
(int)(6L); 
BgL_arg2045z00_3752 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_9518); }  else 
{ /* Llib/hash.scm 1272 */
BgL_arg2045z00_3752 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_83); } } 
{ /* Llib/hash.scm 1303 */
 long BgL_za71za7_3757;
{ /* Llib/hash.scm 1303 */
 obj_t BgL_tmpz00_9522;
if(
INTEGERP(BgL_arg2045z00_3752))
{ /* Llib/hash.scm 1303 */
BgL_tmpz00_9522 = BgL_arg2045z00_3752
; }  else 
{ 
 obj_t BgL_auxz00_9525;
BgL_auxz00_9525 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51912L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2045z00_3752); 
FAILURE(BgL_auxz00_9525,BFALSE,BFALSE);} 
BgL_za71za7_3757 = 
(long)CINT(BgL_tmpz00_9522); } 
BgL_arg2044z00_3751 = 
(BgL_za71za7_3757+1L); } } 
{ /* Llib/hash.scm 1273 */
 bool_t BgL_test4700z00_9531;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_tmpz00_9532;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_res2573z00_3761;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_aux3243z00_6120;
BgL_aux3243z00_6120 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3243z00_6120))
{ /* Llib/hash.scm 1273 */
BgL_res2573z00_3761 = BgL_aux3243z00_6120; }  else 
{ 
 obj_t BgL_auxz00_9536;
BgL_auxz00_9536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50639L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3243z00_6120); 
FAILURE(BgL_auxz00_9536,BFALSE,BFALSE);} } 
BgL_tmpz00_9532 = BgL_res2573z00_3761; } 
BgL_test4700z00_9531 = 
(BgL_tmpz00_9532==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4700z00_9531)
{ /* Llib/hash.scm 1273 */
 obj_t BgL_auxz00_9543; int BgL_tmpz00_9541;
BgL_auxz00_9543 = 
BINT(BgL_arg2044z00_3751); 
BgL_tmpz00_9541 = 
(int)(6L); 
STRUCT_SET(BgL_tablez00_83, BgL_tmpz00_9541, BgL_auxz00_9543); }  else 
{ /* Llib/hash.scm 1273 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_83); } } } } }  else 
{ /* Llib/hash.scm 565 */BFALSE; } } 
{ 
 long BgL_iz00_9547;
BgL_iz00_9547 = 
(BgL_iz00_1933+3L); 
BgL_iz00_1933 = BgL_iz00_9547; 
goto BgL_zc3z04anonymousza31589ze3z87_1934;} } 
return 
BBOOL(BgL_tmpz00_9379);} } } } } } } 

}



/* &open-string-hashtable-filter! */
obj_t BGl_z62openzd2stringzd2hashtablezd2filterz12za2zz__hashz00(obj_t BgL_envz00_5453, obj_t BgL_tablez00_5454, obj_t BgL_funz00_5455)
{
{ /* Llib/hash.scm 559 */
{ /* Llib/hash.scm 560 */
 obj_t BgL_auxz00_9557; obj_t BgL_auxz00_9550;
if(
PROCEDUREP(BgL_funz00_5455))
{ /* Llib/hash.scm 560 */
BgL_auxz00_9557 = BgL_funz00_5455
; }  else 
{ 
 obj_t BgL_auxz00_9560;
BgL_auxz00_9560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22004L), BGl_string3987z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_funz00_5455); 
FAILURE(BgL_auxz00_9560,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5454))
{ /* Llib/hash.scm 560 */
BgL_auxz00_9550 = BgL_tablez00_5454
; }  else 
{ 
 obj_t BgL_auxz00_9553;
BgL_auxz00_9553 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22004L), BGl_string3987z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5454); 
FAILURE(BgL_auxz00_9553,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2filterz12zc0zz__hashz00(BgL_auxz00_9550, BgL_auxz00_9557);} } 

}



/* plain-hashtable-filter! */
obj_t BGl_plainzd2hashtablezd2filterz12z12zz__hashz00(obj_t BgL_tablez00_85, obj_t BgL_funz00_86)
{
{ /* Llib/hash.scm 575 */
{ /* Llib/hash.scm 576 */
 obj_t BgL_bucketsz00_1952;
{ /* Llib/hash.scm 576 */
 bool_t BgL_test4704z00_9565;
{ /* Llib/hash.scm 576 */
 obj_t BgL_tmpz00_9566;
{ /* Llib/hash.scm 576 */
 obj_t BgL_res2574z00_3766;
{ /* Llib/hash.scm 576 */
 obj_t BgL_aux3249z00_6126;
BgL_aux3249z00_6126 = 
STRUCT_KEY(BgL_tablez00_85); 
if(
SYMBOLP(BgL_aux3249z00_6126))
{ /* Llib/hash.scm 576 */
BgL_res2574z00_3766 = BgL_aux3249z00_6126; }  else 
{ 
 obj_t BgL_auxz00_9570;
BgL_auxz00_9570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22773L), BGl_string3988z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3249z00_6126); 
FAILURE(BgL_auxz00_9570,BFALSE,BFALSE);} } 
BgL_tmpz00_9566 = BgL_res2574z00_3766; } 
BgL_test4704z00_9565 = 
(BgL_tmpz00_9566==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4704z00_9565)
{ /* Llib/hash.scm 576 */
 int BgL_tmpz00_9575;
BgL_tmpz00_9575 = 
(int)(2L); 
BgL_bucketsz00_1952 = 
STRUCT_REF(BgL_tablez00_85, BgL_tmpz00_9575); }  else 
{ /* Llib/hash.scm 576 */
BgL_bucketsz00_1952 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_85); } } 
{ /* Llib/hash.scm 576 */
 long BgL_bucketszd2lenzd2_1953;
{ /* Llib/hash.scm 577 */
 obj_t BgL_vectorz00_3767;
if(
VECTORP(BgL_bucketsz00_1952))
{ /* Llib/hash.scm 577 */
BgL_vectorz00_3767 = BgL_bucketsz00_1952; }  else 
{ 
 obj_t BgL_auxz00_9581;
BgL_auxz00_9581 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22832L), BGl_string3988z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1952); 
FAILURE(BgL_auxz00_9581,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1953 = 
VECTOR_LENGTH(BgL_vectorz00_3767); } 
{ /* Llib/hash.scm 577 */

{ 
 long BgL_iz00_1955; long BgL_deltaz00_1956;
BgL_iz00_1955 = 0L; 
BgL_deltaz00_1956 = 0L; 
BgL_zc3z04anonymousza31611ze3z87_1957:
if(
(BgL_iz00_1955<BgL_bucketszd2lenzd2_1953))
{ /* Llib/hash.scm 580 */
 obj_t BgL_lz00_1959;
{ /* Llib/hash.scm 580 */
 obj_t BgL_vectorz00_3770;
if(
VECTORP(BgL_bucketsz00_1952))
{ /* Llib/hash.scm 580 */
BgL_vectorz00_3770 = BgL_bucketsz00_1952; }  else 
{ 
 obj_t BgL_auxz00_9590;
BgL_auxz00_9590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22934L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1952); 
FAILURE(BgL_auxz00_9590,BFALSE,BFALSE);} 
BgL_lz00_1959 = 
VECTOR_REF(BgL_vectorz00_3770,BgL_iz00_1955); } 
{ /* Llib/hash.scm 580 */
 long BgL_oldzd2lenzd2_1960;
{ /* Llib/hash.scm 581 */
 obj_t BgL_auxz00_9595;
{ /* Llib/hash.scm 581 */
 bool_t BgL_test4709z00_9596;
if(
PAIRP(BgL_lz00_1959))
{ /* Llib/hash.scm 581 */
BgL_test4709z00_9596 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 581 */
BgL_test4709z00_9596 = 
NULLP(BgL_lz00_1959)
; } 
if(BgL_test4709z00_9596)
{ /* Llib/hash.scm 581 */
BgL_auxz00_9595 = BgL_lz00_1959
; }  else 
{ 
 obj_t BgL_auxz00_9600;
BgL_auxz00_9600 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(22983L), BGl_string3931z00zz__hashz00, BGl_string3989z00zz__hashz00, BgL_lz00_1959); 
FAILURE(BgL_auxz00_9600,BFALSE,BFALSE);} } 
BgL_oldzd2lenzd2_1960 = 
bgl_list_length(BgL_auxz00_9595); } 
{ /* Llib/hash.scm 581 */
 obj_t BgL_newlz00_1961;
{ /* Llib/hash.scm 583 */
 obj_t BgL_zc3z04anonymousza31618ze3z87_5456;
BgL_zc3z04anonymousza31618ze3z87_5456 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31618ze3ze5zz__hashz00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31618ze3z87_5456, 
(int)(0L), BgL_funz00_86); 
{ /* Llib/hash.scm 582 */
 obj_t BgL_auxz00_9610;
{ /* Llib/hash.scm 584 */
 bool_t BgL_test4711z00_9611;
if(
PAIRP(BgL_lz00_1959))
{ /* Llib/hash.scm 584 */
BgL_test4711z00_9611 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 584 */
BgL_test4711z00_9611 = 
NULLP(BgL_lz00_1959)
; } 
if(BgL_test4711z00_9611)
{ /* Llib/hash.scm 584 */
BgL_auxz00_9610 = BgL_lz00_1959
; }  else 
{ 
 obj_t BgL_auxz00_9615;
BgL_auxz00_9615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23084L), BGl_string3931z00zz__hashz00, BGl_string3989z00zz__hashz00, BgL_lz00_1959); 
FAILURE(BgL_auxz00_9615,BFALSE,BFALSE);} } 
BgL_newlz00_1961 = 
BGl_filterz12z12zz__r4_control_features_6_9z00(BgL_zc3z04anonymousza31618ze3z87_5456, BgL_auxz00_9610); } } 
{ /* Llib/hash.scm 582 */
 long BgL_newzd2lenzd2_1962;
BgL_newzd2lenzd2_1962 = 
bgl_list_length(BgL_newlz00_1961); 
{ /* Llib/hash.scm 585 */

{ /* Llib/hash.scm 586 */
 obj_t BgL_vectorz00_3776;
if(
VECTORP(BgL_bucketsz00_1952))
{ /* Llib/hash.scm 586 */
BgL_vectorz00_3776 = BgL_bucketsz00_1952; }  else 
{ 
 obj_t BgL_auxz00_9623;
BgL_auxz00_9623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23151L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1952); 
FAILURE(BgL_auxz00_9623,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3776,BgL_iz00_1955,BgL_newlz00_1961); } 
{ 
 long BgL_deltaz00_9630; long BgL_iz00_9628;
BgL_iz00_9628 = 
(BgL_iz00_1955+1L); 
BgL_deltaz00_9630 = 
(BgL_deltaz00_1956+
(BgL_newzd2lenzd2_1962-BgL_oldzd2lenzd2_1960)); 
BgL_deltaz00_1956 = BgL_deltaz00_9630; 
BgL_iz00_1955 = BgL_iz00_9628; 
goto BgL_zc3z04anonymousza31611ze3z87_1957;} } } } } }  else 
{ /* Llib/hash.scm 589 */
 long BgL_arg1621z00_1972;
{ /* Llib/hash.scm 589 */
 obj_t BgL_arg1622z00_1973;
{ /* Llib/hash.scm 589 */
 bool_t BgL_test4714z00_9633;
{ /* Llib/hash.scm 589 */
 obj_t BgL_tmpz00_9634;
{ /* Llib/hash.scm 589 */
 obj_t BgL_res2575z00_3786;
{ /* Llib/hash.scm 589 */
 obj_t BgL_aux3261z00_6138;
BgL_aux3261z00_6138 = 
STRUCT_KEY(BgL_tablez00_85); 
if(
SYMBOLP(BgL_aux3261z00_6138))
{ /* Llib/hash.scm 589 */
BgL_res2575z00_3786 = BgL_aux3261z00_6138; }  else 
{ 
 obj_t BgL_auxz00_9638;
BgL_auxz00_9638 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23275L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3261z00_6138); 
FAILURE(BgL_auxz00_9638,BFALSE,BFALSE);} } 
BgL_tmpz00_9634 = BgL_res2575z00_3786; } 
BgL_test4714z00_9633 = 
(BgL_tmpz00_9634==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4714z00_9633)
{ /* Llib/hash.scm 589 */
 int BgL_tmpz00_9643;
BgL_tmpz00_9643 = 
(int)(0L); 
BgL_arg1622z00_1973 = 
STRUCT_REF(BgL_tablez00_85, BgL_tmpz00_9643); }  else 
{ /* Llib/hash.scm 589 */
BgL_arg1622z00_1973 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_85); } } 
{ /* Llib/hash.scm 589 */
 long BgL_za72za7_3788;
{ /* Llib/hash.scm 589 */
 obj_t BgL_tmpz00_9647;
if(
INTEGERP(BgL_arg1622z00_1973))
{ /* Llib/hash.scm 589 */
BgL_tmpz00_9647 = BgL_arg1622z00_1973
; }  else 
{ 
 obj_t BgL_auxz00_9650;
BgL_auxz00_9650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23297L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1622z00_1973); 
FAILURE(BgL_auxz00_9650,BFALSE,BFALSE);} 
BgL_za72za7_3788 = 
(long)CINT(BgL_tmpz00_9647); } 
BgL_arg1621z00_1972 = 
(BgL_deltaz00_1956+BgL_za72za7_3788); } } 
{ /* Llib/hash.scm 588 */
 bool_t BgL_test4717z00_9656;
{ /* Llib/hash.scm 588 */
 obj_t BgL_tmpz00_9657;
{ /* Llib/hash.scm 588 */
 obj_t BgL_res2576z00_3792;
{ /* Llib/hash.scm 588 */
 obj_t BgL_aux3264z00_6141;
BgL_aux3264z00_6141 = 
STRUCT_KEY(BgL_tablez00_85); 
if(
SYMBOLP(BgL_aux3264z00_6141))
{ /* Llib/hash.scm 588 */
BgL_res2576z00_3792 = BgL_aux3264z00_6141; }  else 
{ 
 obj_t BgL_auxz00_9661;
BgL_auxz00_9661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23234L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3264z00_6141); 
FAILURE(BgL_auxz00_9661,BFALSE,BFALSE);} } 
BgL_tmpz00_9657 = BgL_res2576z00_3792; } 
BgL_test4717z00_9656 = 
(BgL_tmpz00_9657==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4717z00_9656)
{ /* Llib/hash.scm 588 */
 obj_t BgL_auxz00_9668; int BgL_tmpz00_9666;
BgL_auxz00_9668 = 
BINT(BgL_arg1621z00_1972); 
BgL_tmpz00_9666 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_85, BgL_tmpz00_9666, BgL_auxz00_9668);}  else 
{ /* Llib/hash.scm 588 */
return 
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_85);} } } } } } } } 

}



/* &<@anonymous:1618> */
obj_t BGl_z62zc3z04anonymousza31618ze3ze5zz__hashz00(obj_t BgL_envz00_5457, obj_t BgL_cellz00_5459)
{
{ /* Llib/hash.scm 582 */
{ /* Llib/hash.scm 583 */
 obj_t BgL_funz00_5458;
BgL_funz00_5458 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_5457, 
(int)(0L))); 
{ /* Llib/hash.scm 583 */
 obj_t BgL_arg1619z00_6804; obj_t BgL_arg1620z00_6805;
{ /* Llib/hash.scm 583 */
 obj_t BgL_pairz00_6806;
if(
PAIRP(BgL_cellz00_5459))
{ /* Llib/hash.scm 583 */
BgL_pairz00_6806 = BgL_cellz00_5459; }  else 
{ 
 obj_t BgL_auxz00_9677;
BgL_auxz00_9677 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23057L), BGl_string3990z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_5459); 
FAILURE(BgL_auxz00_9677,BFALSE,BFALSE);} 
BgL_arg1619z00_6804 = 
CAR(BgL_pairz00_6806); } 
{ /* Llib/hash.scm 583 */
 obj_t BgL_pairz00_6807;
if(
PAIRP(BgL_cellz00_5459))
{ /* Llib/hash.scm 583 */
BgL_pairz00_6807 = BgL_cellz00_5459; }  else 
{ 
 obj_t BgL_auxz00_9684;
BgL_auxz00_9684 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23068L), BGl_string3990z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_5459); 
FAILURE(BgL_auxz00_9684,BFALSE,BFALSE);} 
BgL_arg1620z00_6805 = 
CDR(BgL_pairz00_6807); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5458, 2))
{ /* Llib/hash.scm 583 */
return 
BGL_PROCEDURE_CALL2(BgL_funz00_5458, BgL_arg1619z00_6804, BgL_arg1620z00_6805);}  else 
{ /* Llib/hash.scm 583 */
FAILURE(BGl_string3991z00zz__hashz00,BGl_list3992z00zz__hashz00,BgL_funz00_5458);} } } } 

}



/* hashtable-clear! */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2clearz12zc0zz__hashz00(obj_t BgL_tablez00_87)
{
{ /* Llib/hash.scm 594 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_87))
{ /* Llib/hash.scm 596 */
BGL_TAIL return 
BGl_openzd2stringzd2hashtablezd2clearz12zc0zz__hashz00(BgL_tablez00_87);}  else 
{ /* Llib/hash.scm 596 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_87))
{ /* Llib/hash.scm 598 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(BgL_tablez00_87);}  else 
{ /* Llib/hash.scm 598 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2clearz12z12zz__hashz00(BgL_tablez00_87);} } } 

}



/* &hashtable-clear! */
obj_t BGl_z62hashtablezd2clearz12za2zz__hashz00(obj_t BgL_envz00_5460, obj_t BgL_tablez00_5461)
{
{ /* Llib/hash.scm 594 */
{ /* Llib/hash.scm 596 */
 obj_t BgL_auxz00_9704;
if(
STRUCTP(BgL_tablez00_5461))
{ /* Llib/hash.scm 596 */
BgL_auxz00_9704 = BgL_tablez00_5461
; }  else 
{ 
 obj_t BgL_auxz00_9707;
BgL_auxz00_9707 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(23584L), BGl_string3997z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5461); 
FAILURE(BgL_auxz00_9707,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2clearz12zc0zz__hashz00(BgL_auxz00_9704);} } 

}



/* open-string-hashtable-clear! */
obj_t BGl_openzd2stringzd2hashtablezd2clearz12zc0zz__hashz00(obj_t BgL_tablez00_88)
{
{ /* Llib/hash.scm 606 */
{ /* Llib/hash.scm 607 */
 obj_t BgL_arg1625z00_1977;
{ /* Llib/hash.scm 607 */
 bool_t BgL_test4725z00_9712;
{ /* Llib/hash.scm 607 */
 obj_t BgL_tmpz00_9713;
{ /* Llib/hash.scm 607 */
 obj_t BgL_res2577z00_3796;
{ /* Llib/hash.scm 607 */
 obj_t BgL_aux3273z00_6151;
BgL_aux3273z00_6151 = 
STRUCT_KEY(BgL_tablez00_88); 
if(
SYMBOLP(BgL_aux3273z00_6151))
{ /* Llib/hash.scm 607 */
BgL_res2577z00_3796 = BgL_aux3273z00_6151; }  else 
{ 
 obj_t BgL_auxz00_9717;
BgL_auxz00_9717 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24068L), BGl_string3998z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3273z00_6151); 
FAILURE(BgL_auxz00_9717,BFALSE,BFALSE);} } 
BgL_tmpz00_9713 = BgL_res2577z00_3796; } 
BgL_test4725z00_9712 = 
(BgL_tmpz00_9713==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4725z00_9712)
{ /* Llib/hash.scm 607 */
 int BgL_tmpz00_9722;
BgL_tmpz00_9722 = 
(int)(2L); 
BgL_arg1625z00_1977 = 
STRUCT_REF(BgL_tablez00_88, BgL_tmpz00_9722); }  else 
{ /* Llib/hash.scm 607 */
BgL_arg1625z00_1977 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_88); } } 
{ /* Ieee/vector.scm 105 */
 long BgL_endz00_1981;
{ /* Ieee/vector.scm 105 */
 obj_t BgL_vectorz00_3797;
if(
VECTORP(BgL_arg1625z00_1977))
{ /* Ieee/vector.scm 105 */
BgL_vectorz00_3797 = BgL_arg1625z00_1977; }  else 
{ 
 obj_t BgL_auxz00_9728;
BgL_auxz00_9728 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3999z00zz__hashz00, 
BINT(4906L), BGl_string3998z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_arg1625z00_1977); 
FAILURE(BgL_auxz00_9728,BFALSE,BFALSE);} 
BgL_endz00_1981 = 
VECTOR_LENGTH(BgL_vectorz00_3797); } 
{ /* Ieee/vector.scm 105 */

{ /* Ieee/vector.scm 105 */
 obj_t BgL_auxz00_9733;
if(
VECTORP(BgL_arg1625z00_1977))
{ /* Llib/hash.scm 607 */
BgL_auxz00_9733 = BgL_arg1625z00_1977
; }  else 
{ 
 obj_t BgL_auxz00_9736;
BgL_auxz00_9736 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24093L), BGl_string3998z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_arg1625z00_1977); 
FAILURE(BgL_auxz00_9736,BFALSE,BFALSE);} 
BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_auxz00_9733, BFALSE, 0L, BgL_endz00_1981); } } } } 
{ /* Llib/hash.scm 1273 */
 bool_t BgL_test4729z00_9741;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_tmpz00_9742;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_res2578z00_3801;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_aux3279z00_6157;
BgL_aux3279z00_6157 = 
STRUCT_KEY(BgL_tablez00_88); 
if(
SYMBOLP(BgL_aux3279z00_6157))
{ /* Llib/hash.scm 1273 */
BgL_res2578z00_3801 = BgL_aux3279z00_6157; }  else 
{ 
 obj_t BgL_auxz00_9746;
BgL_auxz00_9746 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50639L), BGl_string3998z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3279z00_6157); 
FAILURE(BgL_auxz00_9746,BFALSE,BFALSE);} } 
BgL_tmpz00_9742 = BgL_res2578z00_3801; } 
BgL_test4729z00_9741 = 
(BgL_tmpz00_9742==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4729z00_9741)
{ /* Llib/hash.scm 1273 */
 obj_t BgL_auxz00_9753; int BgL_tmpz00_9751;
BgL_auxz00_9753 = 
BINT(0L); 
BgL_tmpz00_9751 = 
(int)(6L); 
STRUCT_SET(BgL_tablez00_88, BgL_tmpz00_9751, BgL_auxz00_9753); }  else 
{ /* Llib/hash.scm 1273 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_88); } } 
{ /* Llib/hash.scm 609 */
 bool_t BgL_test4731z00_9757;
{ /* Llib/hash.scm 609 */
 obj_t BgL_tmpz00_9758;
{ /* Llib/hash.scm 609 */
 obj_t BgL_res2579z00_3805;
{ /* Llib/hash.scm 609 */
 obj_t BgL_aux3281z00_6159;
BgL_aux3281z00_6159 = 
STRUCT_KEY(BgL_tablez00_88); 
if(
SYMBOLP(BgL_aux3281z00_6159))
{ /* Llib/hash.scm 609 */
BgL_res2579z00_3805 = BgL_aux3281z00_6159; }  else 
{ 
 obj_t BgL_auxz00_9762;
BgL_auxz00_9762 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24142L), BGl_string3998z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3281z00_6159); 
FAILURE(BgL_auxz00_9762,BFALSE,BFALSE);} } 
BgL_tmpz00_9758 = BgL_res2579z00_3805; } 
BgL_test4731z00_9757 = 
(BgL_tmpz00_9758==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4731z00_9757)
{ /* Llib/hash.scm 609 */
 obj_t BgL_auxz00_9769; int BgL_tmpz00_9767;
BgL_auxz00_9769 = 
BINT(0L); 
BgL_tmpz00_9767 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_88, BgL_tmpz00_9767, BgL_auxz00_9769);}  else 
{ /* Llib/hash.scm 609 */
return 
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_88);} } } 

}



/* plain-hashtable-clear! */
obj_t BGl_plainzd2hashtablezd2clearz12z12zz__hashz00(obj_t BgL_tablez00_89)
{
{ /* Llib/hash.scm 614 */
{ /* Llib/hash.scm 615 */
 obj_t BgL_bucketsz00_1982;
{ /* Llib/hash.scm 615 */
 bool_t BgL_test4733z00_9773;
{ /* Llib/hash.scm 615 */
 obj_t BgL_tmpz00_9774;
{ /* Llib/hash.scm 615 */
 obj_t BgL_res2580z00_3809;
{ /* Llib/hash.scm 615 */
 obj_t BgL_aux3283z00_6161;
BgL_aux3283z00_6161 = 
STRUCT_KEY(BgL_tablez00_89); 
if(
SYMBOLP(BgL_aux3283z00_6161))
{ /* Llib/hash.scm 615 */
BgL_res2580z00_3809 = BgL_aux3283z00_6161; }  else 
{ 
 obj_t BgL_auxz00_9778;
BgL_auxz00_9778 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24466L), BGl_string4000z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3283z00_6161); 
FAILURE(BgL_auxz00_9778,BFALSE,BFALSE);} } 
BgL_tmpz00_9774 = BgL_res2580z00_3809; } 
BgL_test4733z00_9773 = 
(BgL_tmpz00_9774==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4733z00_9773)
{ /* Llib/hash.scm 615 */
 int BgL_tmpz00_9783;
BgL_tmpz00_9783 = 
(int)(2L); 
BgL_bucketsz00_1982 = 
STRUCT_REF(BgL_tablez00_89, BgL_tmpz00_9783); }  else 
{ /* Llib/hash.scm 615 */
BgL_bucketsz00_1982 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_89); } } 
{ /* Llib/hash.scm 615 */
 long BgL_bucketszd2lenzd2_1983;
{ /* Llib/hash.scm 616 */
 obj_t BgL_vectorz00_3810;
if(
VECTORP(BgL_bucketsz00_1982))
{ /* Llib/hash.scm 616 */
BgL_vectorz00_3810 = BgL_bucketsz00_1982; }  else 
{ 
 obj_t BgL_auxz00_9789;
BgL_auxz00_9789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24525L), BGl_string4000z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1982); 
FAILURE(BgL_auxz00_9789,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1983 = 
VECTOR_LENGTH(BgL_vectorz00_3810); } 
{ /* Llib/hash.scm 616 */

{ 
 long BgL_iz00_3821;
BgL_iz00_3821 = 0L; 
BgL_loopz00_3820:
if(
(BgL_iz00_3821<BgL_bucketszd2lenzd2_1983))
{ /* Llib/hash.scm 618 */
{ /* Llib/hash.scm 620 */
 obj_t BgL_vectorz00_3825;
if(
VECTORP(BgL_bucketsz00_1982))
{ /* Llib/hash.scm 620 */
BgL_vectorz00_3825 = BgL_bucketsz00_1982; }  else 
{ 
 obj_t BgL_auxz00_9798;
BgL_auxz00_9798 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24617L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1982); 
FAILURE(BgL_auxz00_9798,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3825,BgL_iz00_3821,BNIL); } 
{ 
 long BgL_iz00_9803;
BgL_iz00_9803 = 
(BgL_iz00_3821+1L); 
BgL_iz00_3821 = BgL_iz00_9803; 
goto BgL_loopz00_3820;} }  else 
{ /* Llib/hash.scm 622 */
 bool_t BgL_test4738z00_9805;
{ /* Llib/hash.scm 622 */
 obj_t BgL_tmpz00_9806;
{ /* Llib/hash.scm 622 */
 obj_t BgL_res2581z00_3831;
{ /* Llib/hash.scm 622 */
 obj_t BgL_aux3289z00_6167;
BgL_aux3289z00_6167 = 
STRUCT_KEY(BgL_tablez00_89); 
if(
SYMBOLP(BgL_aux3289z00_6167))
{ /* Llib/hash.scm 622 */
BgL_res2581z00_3831 = BgL_aux3289z00_6167; }  else 
{ 
 obj_t BgL_auxz00_9810;
BgL_auxz00_9810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24665L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3289z00_6167); 
FAILURE(BgL_auxz00_9810,BFALSE,BFALSE);} } 
BgL_tmpz00_9806 = BgL_res2581z00_3831; } 
BgL_test4738z00_9805 = 
(BgL_tmpz00_9806==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4738z00_9805)
{ /* Llib/hash.scm 622 */
 obj_t BgL_auxz00_9817; int BgL_tmpz00_9815;
BgL_auxz00_9817 = 
BINT(0L); 
BgL_tmpz00_9815 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_89, BgL_tmpz00_9815, BgL_auxz00_9817);}  else 
{ /* Llib/hash.scm 622 */
return 
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_89);} } } } } } } 

}



/* hashtable-contains? */
BGL_EXPORTED_DEF bool_t BGl_hashtablezd2containszf3z21zz__hashz00(obj_t BgL_tablez00_90, obj_t BgL_keyz00_91)
{
{ /* Llib/hash.scm 627 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_90))
{ /* Llib/hash.scm 630 */
 obj_t BgL_tmpz00_9823;
{ /* Llib/hash.scm 630 */
 obj_t BgL_auxz00_9824;
if(
STRINGP(BgL_keyz00_91))
{ /* Llib/hash.scm 630 */
BgL_auxz00_9824 = BgL_keyz00_91
; }  else 
{ 
 obj_t BgL_auxz00_9827;
BgL_auxz00_9827 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25069L), BGl_string4001z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_91); 
FAILURE(BgL_auxz00_9827,BFALSE,BFALSE);} 
BgL_tmpz00_9823 = 
BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00(BgL_tablez00_90, BgL_auxz00_9824); } 
return 
CBOOL(BgL_tmpz00_9823);}  else 
{ /* Llib/hash.scm 629 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_90))
{ /* Llib/hash.scm 631 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(BgL_tablez00_90, BgL_keyz00_91);}  else 
{ /* Llib/hash.scm 631 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2containszf3zf3zz__hashz00(BgL_tablez00_90, BgL_keyz00_91);} } } 

}



/* &hashtable-contains? */
obj_t BGl_z62hashtablezd2containszf3z43zz__hashz00(obj_t BgL_envz00_5462, obj_t BgL_tablez00_5463, obj_t BgL_keyz00_5464)
{
{ /* Llib/hash.scm 627 */
{ /* Llib/hash.scm 629 */
 bool_t BgL_tmpz00_9837;
{ /* Llib/hash.scm 629 */
 obj_t BgL_auxz00_9838;
if(
STRUCTP(BgL_tablez00_5463))
{ /* Llib/hash.scm 629 */
BgL_auxz00_9838 = BgL_tablez00_5463
; }  else 
{ 
 obj_t BgL_auxz00_9841;
BgL_auxz00_9841 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(24991L), BGl_string4002z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5463); 
FAILURE(BgL_auxz00_9841,BFALSE,BFALSE);} 
BgL_tmpz00_9837 = 
BGl_hashtablezd2containszf3z21zz__hashz00(BgL_auxz00_9838, BgL_keyz00_5464); } 
return 
BBOOL(BgL_tmpz00_9837);} } 

}



/* open-string-hashtable-contains? */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00(obj_t BgL_tz00_92, obj_t BgL_keyz00_93)
{
{ /* Llib/hash.scm 639 */
{ /* Llib/hash.scm 640 */
 obj_t BgL_siza7eza7_1992;
{ /* Llib/hash.scm 640 */
 bool_t BgL_test4744z00_9847;
{ /* Llib/hash.scm 640 */
 obj_t BgL_tmpz00_9848;
{ /* Llib/hash.scm 640 */
 obj_t BgL_res2582z00_3838;
{ /* Llib/hash.scm 640 */
 obj_t BgL_aux3295z00_6173;
BgL_aux3295z00_6173 = 
STRUCT_KEY(BgL_tz00_92); 
if(
SYMBOLP(BgL_aux3295z00_6173))
{ /* Llib/hash.scm 640 */
BgL_res2582z00_3838 = BgL_aux3295z00_6173; }  else 
{ 
 obj_t BgL_auxz00_9852;
BgL_auxz00_9852 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25498L), BGl_string4003z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3295z00_6173); 
FAILURE(BgL_auxz00_9852,BFALSE,BFALSE);} } 
BgL_tmpz00_9848 = BgL_res2582z00_3838; } 
BgL_test4744z00_9847 = 
(BgL_tmpz00_9848==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4744z00_9847)
{ /* Llib/hash.scm 640 */
 int BgL_tmpz00_9857;
BgL_tmpz00_9857 = 
(int)(1L); 
BgL_siza7eza7_1992 = 
STRUCT_REF(BgL_tz00_92, BgL_tmpz00_9857); }  else 
{ /* Llib/hash.scm 640 */
BgL_siza7eza7_1992 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_92); } } 
{ /* Llib/hash.scm 640 */
 obj_t BgL_bucketsz00_1993;
{ /* Llib/hash.scm 641 */
 bool_t BgL_test4746z00_9861;
{ /* Llib/hash.scm 641 */
 obj_t BgL_tmpz00_9862;
{ /* Llib/hash.scm 641 */
 obj_t BgL_res2583z00_3842;
{ /* Llib/hash.scm 641 */
 obj_t BgL_aux3297z00_6175;
BgL_aux3297z00_6175 = 
STRUCT_KEY(BgL_tz00_92); 
if(
SYMBOLP(BgL_aux3297z00_6175))
{ /* Llib/hash.scm 641 */
BgL_res2583z00_3842 = BgL_aux3297z00_6175; }  else 
{ 
 obj_t BgL_auxz00_9866;
BgL_auxz00_9866 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25541L), BGl_string4003z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3297z00_6175); 
FAILURE(BgL_auxz00_9866,BFALSE,BFALSE);} } 
BgL_tmpz00_9862 = BgL_res2583z00_3842; } 
BgL_test4746z00_9861 = 
(BgL_tmpz00_9862==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4746z00_9861)
{ /* Llib/hash.scm 641 */
 int BgL_tmpz00_9871;
BgL_tmpz00_9871 = 
(int)(2L); 
BgL_bucketsz00_1993 = 
STRUCT_REF(BgL_tz00_92, BgL_tmpz00_9871); }  else 
{ /* Llib/hash.scm 641 */
BgL_bucketsz00_1993 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_92); } } 
{ /* Llib/hash.scm 641 */
 long BgL_hashz00_1994;
{ /* Llib/hash.scm 642 */
 long BgL_arg1646z00_2016;
BgL_arg1646z00_2016 = 
STRING_LENGTH(BgL_keyz00_93); 
BgL_hashz00_1994 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_93), 
(int)(0L), 
(int)(BgL_arg1646z00_2016)); } 
{ /* Llib/hash.scm 642 */

{ /* Llib/hash.scm 644 */
 long BgL_g1068z00_1995;
{ /* Llib/hash.scm 644 */
 long BgL_n1z00_3844; long BgL_n2z00_3845;
BgL_n1z00_3844 = BgL_hashz00_1994; 
{ /* Llib/hash.scm 644 */
 obj_t BgL_tmpz00_9880;
if(
INTEGERP(BgL_siza7eza7_1992))
{ /* Llib/hash.scm 644 */
BgL_tmpz00_9880 = BgL_siza7eza7_1992
; }  else 
{ 
 obj_t BgL_auxz00_9883;
BgL_auxz00_9883 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25679L), BGl_string4003z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1992); 
FAILURE(BgL_auxz00_9883,BFALSE,BFALSE);} 
BgL_n2z00_3845 = 
(long)CINT(BgL_tmpz00_9880); } 
{ /* Llib/hash.scm 644 */
 bool_t BgL_test4749z00_9888;
{ /* Llib/hash.scm 644 */
 long BgL_arg2221z00_3847;
BgL_arg2221z00_3847 = 
(((BgL_n1z00_3844) | (BgL_n2z00_3845)) & -2147483648); 
BgL_test4749z00_9888 = 
(BgL_arg2221z00_3847==0L); } 
if(BgL_test4749z00_9888)
{ /* Llib/hash.scm 644 */
 int32_t BgL_arg2218z00_3848;
{ /* Llib/hash.scm 644 */
 int32_t BgL_arg2219z00_3849; int32_t BgL_arg2220z00_3850;
BgL_arg2219z00_3849 = 
(int32_t)(BgL_n1z00_3844); 
BgL_arg2220z00_3850 = 
(int32_t)(BgL_n2z00_3845); 
BgL_arg2218z00_3848 = 
(BgL_arg2219z00_3849%BgL_arg2220z00_3850); } 
{ /* Llib/hash.scm 644 */
 long BgL_arg2326z00_3855;
BgL_arg2326z00_3855 = 
(long)(BgL_arg2218z00_3848); 
BgL_g1068z00_1995 = 
(long)(BgL_arg2326z00_3855); } }  else 
{ /* Llib/hash.scm 644 */
BgL_g1068z00_1995 = 
(BgL_n1z00_3844%BgL_n2z00_3845); } } } 
{ 
 long BgL_offz00_1997; long BgL_iz00_1998;
BgL_offz00_1997 = BgL_g1068z00_1995; 
BgL_iz00_1998 = 1L; 
BgL_zc3z04anonymousza31631ze3z87_1999:
{ /* Llib/hash.scm 646 */
 long BgL_off3z00_2000;
BgL_off3z00_2000 = 
(BgL_offz00_1997*3L); 
{ /* Llib/hash.scm 647 */
 bool_t BgL_test4750z00_9898;
{ /* Llib/hash.scm 647 */
 obj_t BgL_vectorz00_3858;
if(
VECTORP(BgL_bucketsz00_1993))
{ /* Llib/hash.scm 647 */
BgL_vectorz00_3858 = BgL_bucketsz00_1993; }  else 
{ 
 obj_t BgL_auxz00_9901;
BgL_auxz00_9901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25747L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1993); 
FAILURE(BgL_auxz00_9901,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 647 */
 bool_t BgL_test4752z00_9905;
{ /* Llib/hash.scm 647 */
 long BgL_tmpz00_9906;
BgL_tmpz00_9906 = 
VECTOR_LENGTH(BgL_vectorz00_3858); 
BgL_test4752z00_9905 = 
BOUND_CHECK(BgL_off3z00_2000, BgL_tmpz00_9906); } 
if(BgL_test4752z00_9905)
{ /* Llib/hash.scm 647 */
BgL_test4750z00_9898 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3858,BgL_off3z00_2000))
; }  else 
{ 
 obj_t BgL_auxz00_9911;
BgL_auxz00_9911 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25735L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3858, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3858)), 
(int)(BgL_off3z00_2000)); 
FAILURE(BgL_auxz00_9911,BFALSE,BFALSE);} } } 
if(BgL_test4750z00_9898)
{ /* Llib/hash.scm 648 */
 bool_t BgL_test4753z00_9918;
{ /* Llib/hash.scm 648 */
 obj_t BgL_arg1645z00_2014;
{ /* Llib/hash.scm 648 */
 obj_t BgL_vectorz00_3860;
if(
VECTORP(BgL_bucketsz00_1993))
{ /* Llib/hash.scm 648 */
BgL_vectorz00_3860 = BgL_bucketsz00_1993; }  else 
{ 
 obj_t BgL_auxz00_9921;
BgL_auxz00_9921 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25795L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1993); 
FAILURE(BgL_auxz00_9921,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 648 */
 bool_t BgL_test4755z00_9925;
{ /* Llib/hash.scm 648 */
 long BgL_tmpz00_9926;
BgL_tmpz00_9926 = 
VECTOR_LENGTH(BgL_vectorz00_3860); 
BgL_test4755z00_9925 = 
BOUND_CHECK(BgL_off3z00_2000, BgL_tmpz00_9926); } 
if(BgL_test4755z00_9925)
{ /* Llib/hash.scm 648 */
BgL_arg1645z00_2014 = 
VECTOR_REF(BgL_vectorz00_3860,BgL_off3z00_2000); }  else 
{ 
 obj_t BgL_auxz00_9930;
BgL_auxz00_9930 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25783L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3860, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3860)), 
(int)(BgL_off3z00_2000)); 
FAILURE(BgL_auxz00_9930,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 648 */
 obj_t BgL_string1z00_3862;
if(
STRINGP(BgL_arg1645z00_2014))
{ /* Llib/hash.scm 648 */
BgL_string1z00_3862 = BgL_arg1645z00_2014; }  else 
{ 
 obj_t BgL_auxz00_9939;
BgL_auxz00_9939 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25807L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1645z00_2014); 
FAILURE(BgL_auxz00_9939,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 648 */
 long BgL_l1z00_3864;
BgL_l1z00_3864 = 
STRING_LENGTH(BgL_string1z00_3862); 
if(
(BgL_l1z00_3864==
STRING_LENGTH(BgL_keyz00_93)))
{ /* Llib/hash.scm 648 */
 int BgL_arg2114z00_3867;
{ /* Llib/hash.scm 648 */
 char * BgL_auxz00_9949; char * BgL_tmpz00_9947;
BgL_auxz00_9949 = 
BSTRING_TO_STRING(BgL_keyz00_93); 
BgL_tmpz00_9947 = 
BSTRING_TO_STRING(BgL_string1z00_3862); 
BgL_arg2114z00_3867 = 
memcmp(BgL_tmpz00_9947, BgL_auxz00_9949, BgL_l1z00_3864); } 
BgL_test4753z00_9918 = 
(
(long)(BgL_arg2114z00_3867)==0L); }  else 
{ /* Llib/hash.scm 648 */
BgL_test4753z00_9918 = ((bool_t)0)
; } } } } 
if(BgL_test4753z00_9918)
{ /* Llib/hash.scm 649 */
 bool_t BgL_test4758z00_9954;
{ /* Llib/hash.scm 649 */
 long BgL_arg1639z00_2007;
BgL_arg1639z00_2007 = 
(BgL_off3z00_2000+1L); 
{ /* Llib/hash.scm 649 */
 obj_t BgL_vectorz00_3874;
if(
VECTORP(BgL_bucketsz00_1993))
{ /* Llib/hash.scm 649 */
BgL_vectorz00_3874 = BgL_bucketsz00_1993; }  else 
{ 
 obj_t BgL_auxz00_9958;
BgL_auxz00_9958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25837L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1993); 
FAILURE(BgL_auxz00_9958,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 649 */
 bool_t BgL_test4760z00_9962;
{ /* Llib/hash.scm 649 */
 long BgL_tmpz00_9963;
BgL_tmpz00_9963 = 
VECTOR_LENGTH(BgL_vectorz00_3874); 
BgL_test4760z00_9962 = 
BOUND_CHECK(BgL_arg1639z00_2007, BgL_tmpz00_9963); } 
if(BgL_test4760z00_9962)
{ /* Llib/hash.scm 649 */
BgL_test4758z00_9954 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_3874,BgL_arg1639z00_2007))
; }  else 
{ 
 obj_t BgL_auxz00_9968;
BgL_auxz00_9968 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25825L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3874, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3874)), 
(int)(BgL_arg1639z00_2007)); 
FAILURE(BgL_auxz00_9968,BFALSE,BFALSE);} } } } 
if(BgL_test4758z00_9954)
{ /* Llib/hash.scm 650 */
 long BgL_arg1638z00_2006;
BgL_arg1638z00_2006 = 
(BgL_off3z00_2000+1L); 
{ /* Llib/hash.scm 650 */
 obj_t BgL_vectorz00_3877;
if(
VECTORP(BgL_bucketsz00_1993))
{ /* Llib/hash.scm 650 */
BgL_vectorz00_3877 = BgL_bucketsz00_1993; }  else 
{ 
 obj_t BgL_auxz00_9978;
BgL_auxz00_9978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25879L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_1993); 
FAILURE(BgL_auxz00_9978,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 650 */
 bool_t BgL_test4762z00_9982;
{ /* Llib/hash.scm 650 */
 long BgL_tmpz00_9983;
BgL_tmpz00_9983 = 
VECTOR_LENGTH(BgL_vectorz00_3877); 
BgL_test4762z00_9982 = 
BOUND_CHECK(BgL_arg1638z00_2006, BgL_tmpz00_9983); } 
if(BgL_test4762z00_9982)
{ /* Llib/hash.scm 650 */
return 
VECTOR_REF(BgL_vectorz00_3877,BgL_arg1638z00_2006);}  else 
{ 
 obj_t BgL_auxz00_9987;
BgL_auxz00_9987 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25867L), BGl_string3881z00zz__hashz00, BgL_vectorz00_3877, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3877)), 
(int)(BgL_arg1638z00_2006)); 
FAILURE(BgL_auxz00_9987,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 649 */
return BFALSE;} }  else 
{ /* Llib/hash.scm 651 */
 long BgL_noffz00_2008;
BgL_noffz00_2008 = 
(BgL_offz00_1997+
(BgL_iz00_1998*BgL_iz00_1998)); 
{ /* Llib/hash.scm 652 */
 bool_t BgL_test4763z00_9996;
{ /* Llib/hash.scm 652 */
 long BgL_n2z00_3884;
{ /* Llib/hash.scm 652 */
 obj_t BgL_tmpz00_9997;
if(
INTEGERP(BgL_siza7eza7_1992))
{ /* Llib/hash.scm 652 */
BgL_tmpz00_9997 = BgL_siza7eza7_1992
; }  else 
{ 
 obj_t BgL_auxz00_10000;
BgL_auxz00_10000 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25964L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1992); 
FAILURE(BgL_auxz00_10000,BFALSE,BFALSE);} 
BgL_n2z00_3884 = 
(long)CINT(BgL_tmpz00_9997); } 
BgL_test4763z00_9996 = 
(BgL_noffz00_2008>=BgL_n2z00_3884); } 
if(BgL_test4763z00_9996)
{ /* Llib/hash.scm 653 */
 long BgL_arg1641z00_2010; long BgL_arg1642z00_2011;
{ /* Llib/hash.scm 653 */
 long BgL_n1z00_3885; long BgL_n2z00_3886;
BgL_n1z00_3885 = BgL_noffz00_2008; 
{ /* Llib/hash.scm 653 */
 obj_t BgL_tmpz00_10006;
if(
INTEGERP(BgL_siza7eza7_1992))
{ /* Llib/hash.scm 653 */
BgL_tmpz00_10006 = BgL_siza7eza7_1992
; }  else 
{ 
 obj_t BgL_auxz00_10009;
BgL_auxz00_10009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25999L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_1992); 
FAILURE(BgL_auxz00_10009,BFALSE,BFALSE);} 
BgL_n2z00_3886 = 
(long)CINT(BgL_tmpz00_10006); } 
{ /* Llib/hash.scm 653 */
 bool_t BgL_test4766z00_10014;
{ /* Llib/hash.scm 653 */
 long BgL_arg2221z00_3888;
BgL_arg2221z00_3888 = 
(((BgL_n1z00_3885) | (BgL_n2z00_3886)) & -2147483648); 
BgL_test4766z00_10014 = 
(BgL_arg2221z00_3888==0L); } 
if(BgL_test4766z00_10014)
{ /* Llib/hash.scm 653 */
 int32_t BgL_arg2218z00_3889;
{ /* Llib/hash.scm 653 */
 int32_t BgL_arg2219z00_3890; int32_t BgL_arg2220z00_3891;
BgL_arg2219z00_3890 = 
(int32_t)(BgL_n1z00_3885); 
BgL_arg2220z00_3891 = 
(int32_t)(BgL_n2z00_3886); 
BgL_arg2218z00_3889 = 
(BgL_arg2219z00_3890%BgL_arg2220z00_3891); } 
{ /* Llib/hash.scm 653 */
 long BgL_arg2326z00_3896;
BgL_arg2326z00_3896 = 
(long)(BgL_arg2218z00_3889); 
BgL_arg1641z00_2010 = 
(long)(BgL_arg2326z00_3896); } }  else 
{ /* Llib/hash.scm 653 */
BgL_arg1641z00_2010 = 
(BgL_n1z00_3885%BgL_n2z00_3886); } } } 
BgL_arg1642z00_2011 = 
(BgL_iz00_1998+1L); 
{ 
 long BgL_iz00_10025; long BgL_offz00_10024;
BgL_offz00_10024 = BgL_arg1641z00_2010; 
BgL_iz00_10025 = BgL_arg1642z00_2011; 
BgL_iz00_1998 = BgL_iz00_10025; 
BgL_offz00_1997 = BgL_offz00_10024; 
goto BgL_zc3z04anonymousza31631ze3z87_1999;} }  else 
{ 
 long BgL_iz00_10027; long BgL_offz00_10026;
BgL_offz00_10026 = BgL_noffz00_2008; 
BgL_iz00_10027 = 
(BgL_iz00_1998+1L); 
BgL_iz00_1998 = BgL_iz00_10027; 
BgL_offz00_1997 = BgL_offz00_10026; 
goto BgL_zc3z04anonymousza31631ze3z87_1999;} } } }  else 
{ /* Llib/hash.scm 647 */
return BFALSE;} } } } } } } } } } 

}



/* &open-string-hashtable-contains? */
obj_t BGl_z62openzd2stringzd2hashtablezd2containszf3z43zz__hashz00(obj_t BgL_envz00_5465, obj_t BgL_tz00_5466, obj_t BgL_keyz00_5467)
{
{ /* Llib/hash.scm 639 */
{ /* Llib/hash.scm 640 */
 obj_t BgL_auxz00_10036; obj_t BgL_auxz00_10029;
if(
STRINGP(BgL_keyz00_5467))
{ /* Llib/hash.scm 640 */
BgL_auxz00_10036 = BgL_keyz00_5467
; }  else 
{ 
 obj_t BgL_auxz00_10039;
BgL_auxz00_10039 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25485L), BGl_string4004z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5467); 
FAILURE(BgL_auxz00_10039,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tz00_5466))
{ /* Llib/hash.scm 640 */
BgL_auxz00_10029 = BgL_tz00_5466
; }  else 
{ 
 obj_t BgL_auxz00_10032;
BgL_auxz00_10032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(25485L), BGl_string4004z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tz00_5466); 
FAILURE(BgL_auxz00_10032,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2containszf3z21zz__hashz00(BgL_auxz00_10029, BgL_auxz00_10036);} } 

}



/* plain-hashtable-contains? */
bool_t BGl_plainzd2hashtablezd2containszf3zf3zz__hashz00(obj_t BgL_tablez00_94, obj_t BgL_keyz00_95)
{
{ /* Llib/hash.scm 659 */
{ /* Llib/hash.scm 660 */
 obj_t BgL_bucketsz00_2017;
{ /* Llib/hash.scm 660 */
 bool_t BgL_test4769z00_10044;
{ /* Llib/hash.scm 660 */
 obj_t BgL_tmpz00_10045;
{ /* Llib/hash.scm 660 */
 obj_t BgL_res2584z00_3903;
{ /* Llib/hash.scm 660 */
 obj_t BgL_aux3316z00_6194;
BgL_aux3316z00_6194 = 
STRUCT_KEY(BgL_tablez00_94); 
if(
SYMBOLP(BgL_aux3316z00_6194))
{ /* Llib/hash.scm 660 */
BgL_res2584z00_3903 = BgL_aux3316z00_6194; }  else 
{ 
 obj_t BgL_auxz00_10049;
BgL_auxz00_10049 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26352L), BGl_string4005z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3316z00_6194); 
FAILURE(BgL_auxz00_10049,BFALSE,BFALSE);} } 
BgL_tmpz00_10045 = BgL_res2584z00_3903; } 
BgL_test4769z00_10044 = 
(BgL_tmpz00_10045==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4769z00_10044)
{ /* Llib/hash.scm 660 */
 int BgL_tmpz00_10054;
BgL_tmpz00_10054 = 
(int)(2L); 
BgL_bucketsz00_2017 = 
STRUCT_REF(BgL_tablez00_94, BgL_tmpz00_10054); }  else 
{ /* Llib/hash.scm 660 */
BgL_bucketsz00_2017 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_94); } } 
{ /* Llib/hash.scm 660 */
 long BgL_bucketzd2lenzd2_2018;
{ /* Llib/hash.scm 661 */
 obj_t BgL_vectorz00_3904;
if(
VECTORP(BgL_bucketsz00_2017))
{ /* Llib/hash.scm 661 */
BgL_vectorz00_3904 = BgL_bucketsz00_2017; }  else 
{ 
 obj_t BgL_auxz00_10060;
BgL_auxz00_10060 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26410L), BGl_string4005z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2017); 
FAILURE(BgL_auxz00_10060,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2018 = 
VECTOR_LENGTH(BgL_vectorz00_3904); } 
{ /* Llib/hash.scm 661 */
 long BgL_bucketzd2numzd2_2019;
{ /* Llib/hash.scm 662 */
 long BgL_arg1653z00_2030;
BgL_arg1653z00_2030 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_94, BgL_keyz00_95); 
{ /* Llib/hash.scm 662 */
 long BgL_n1z00_3905; long BgL_n2z00_3906;
BgL_n1z00_3905 = BgL_arg1653z00_2030; 
BgL_n2z00_3906 = BgL_bucketzd2lenzd2_2018; 
{ /* Llib/hash.scm 662 */
 bool_t BgL_test4772z00_10066;
{ /* Llib/hash.scm 662 */
 long BgL_arg2221z00_3908;
BgL_arg2221z00_3908 = 
(((BgL_n1z00_3905) | (BgL_n2z00_3906)) & -2147483648); 
BgL_test4772z00_10066 = 
(BgL_arg2221z00_3908==0L); } 
if(BgL_test4772z00_10066)
{ /* Llib/hash.scm 662 */
 int32_t BgL_arg2218z00_3909;
{ /* Llib/hash.scm 662 */
 int32_t BgL_arg2219z00_3910; int32_t BgL_arg2220z00_3911;
BgL_arg2219z00_3910 = 
(int32_t)(BgL_n1z00_3905); 
BgL_arg2220z00_3911 = 
(int32_t)(BgL_n2z00_3906); 
BgL_arg2218z00_3909 = 
(BgL_arg2219z00_3910%BgL_arg2220z00_3911); } 
{ /* Llib/hash.scm 662 */
 long BgL_arg2326z00_3916;
BgL_arg2326z00_3916 = 
(long)(BgL_arg2218z00_3909); 
BgL_bucketzd2numzd2_2019 = 
(long)(BgL_arg2326z00_3916); } }  else 
{ /* Llib/hash.scm 662 */
BgL_bucketzd2numzd2_2019 = 
(BgL_n1z00_3905%BgL_n2z00_3906); } } } } 
{ /* Llib/hash.scm 662 */
 obj_t BgL_bucketz00_2020;
{ /* Llib/hash.scm 663 */
 obj_t BgL_vectorz00_3918;
if(
VECTORP(BgL_bucketsz00_2017))
{ /* Llib/hash.scm 663 */
BgL_vectorz00_3918 = BgL_bucketsz00_2017; }  else 
{ 
 obj_t BgL_auxz00_10077;
BgL_auxz00_10077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26520L), BGl_string4005z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2017); 
FAILURE(BgL_auxz00_10077,BFALSE,BFALSE);} 
BgL_bucketz00_2020 = 
VECTOR_REF(BgL_vectorz00_3918,BgL_bucketzd2numzd2_2019); } 
{ /* Llib/hash.scm 663 */

{ 
 obj_t BgL_bucketz00_2022;
BgL_bucketz00_2022 = BgL_bucketz00_2020; 
BgL_zc3z04anonymousza31647ze3z87_2023:
if(
NULLP(BgL_bucketz00_2022))
{ /* Llib/hash.scm 666 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 668 */
 bool_t BgL_test4775z00_10084;
{ /* Llib/hash.scm 668 */
 obj_t BgL_arg1652z00_2028;
{ /* Llib/hash.scm 668 */
 obj_t BgL_pairz00_3920;
if(
PAIRP(BgL_bucketz00_2022))
{ /* Llib/hash.scm 668 */
BgL_pairz00_3920 = BgL_bucketz00_2022; }  else 
{ 
 obj_t BgL_auxz00_10087;
BgL_auxz00_10087 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26651L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2022); 
FAILURE(BgL_auxz00_10087,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 668 */
 obj_t BgL_pairz00_3923;
{ /* Llib/hash.scm 668 */
 obj_t BgL_aux3324z00_6202;
BgL_aux3324z00_6202 = 
CAR(BgL_pairz00_3920); 
if(
PAIRP(BgL_aux3324z00_6202))
{ /* Llib/hash.scm 668 */
BgL_pairz00_3923 = BgL_aux3324z00_6202; }  else 
{ 
 obj_t BgL_auxz00_10094;
BgL_auxz00_10094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26645L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3324z00_6202); 
FAILURE(BgL_auxz00_10094,BFALSE,BFALSE);} } 
BgL_arg1652z00_2028 = 
CAR(BgL_pairz00_3923); } } 
{ /* Llib/hash.scm 668 */
 obj_t BgL_eqtz00_3924;
{ /* Llib/hash.scm 668 */
 bool_t BgL_test4778z00_10099;
{ /* Llib/hash.scm 668 */
 obj_t BgL_tmpz00_10100;
{ /* Llib/hash.scm 668 */
 obj_t BgL_res2585z00_3931;
{ /* Llib/hash.scm 668 */
 obj_t BgL_aux3326z00_6204;
BgL_aux3326z00_6204 = 
STRUCT_KEY(BgL_tablez00_94); 
if(
SYMBOLP(BgL_aux3326z00_6204))
{ /* Llib/hash.scm 668 */
BgL_res2585z00_3931 = BgL_aux3326z00_6204; }  else 
{ 
 obj_t BgL_auxz00_10104;
BgL_auxz00_10104 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26621L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3326z00_6204); 
FAILURE(BgL_auxz00_10104,BFALSE,BFALSE);} } 
BgL_tmpz00_10100 = BgL_res2585z00_3931; } 
BgL_test4778z00_10099 = 
(BgL_tmpz00_10100==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4778z00_10099)
{ /* Llib/hash.scm 668 */
 int BgL_tmpz00_10109;
BgL_tmpz00_10109 = 
(int)(3L); 
BgL_eqtz00_3924 = 
STRUCT_REF(BgL_tablez00_94, BgL_tmpz00_10109); }  else 
{ /* Llib/hash.scm 668 */
BgL_eqtz00_3924 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_94); } } 
if(
PROCEDUREP(BgL_eqtz00_3924))
{ /* Llib/hash.scm 668 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3924, 2))
{ /* Llib/hash.scm 668 */
BgL_test4775z00_10084 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3924, BgL_arg1652z00_2028, BgL_keyz00_95))
; }  else 
{ /* Llib/hash.scm 668 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4006z00zz__hashz00,BgL_eqtz00_3924);} }  else 
{ /* Llib/hash.scm 668 */
if(
(BgL_arg1652z00_2028==BgL_keyz00_95))
{ /* Llib/hash.scm 668 */
BgL_test4775z00_10084 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 668 */
if(
STRINGP(BgL_arg1652z00_2028))
{ /* Llib/hash.scm 668 */
if(
STRINGP(BgL_keyz00_95))
{ /* Llib/hash.scm 668 */
 long BgL_l1z00_3934;
BgL_l1z00_3934 = 
STRING_LENGTH(BgL_arg1652z00_2028); 
if(
(BgL_l1z00_3934==
STRING_LENGTH(BgL_keyz00_95)))
{ /* Llib/hash.scm 668 */
 int BgL_arg2114z00_3937;
{ /* Llib/hash.scm 668 */
 char * BgL_auxz00_10136; char * BgL_tmpz00_10134;
BgL_auxz00_10136 = 
BSTRING_TO_STRING(BgL_keyz00_95); 
BgL_tmpz00_10134 = 
BSTRING_TO_STRING(BgL_arg1652z00_2028); 
BgL_arg2114z00_3937 = 
memcmp(BgL_tmpz00_10134, BgL_auxz00_10136, BgL_l1z00_3934); } 
BgL_test4775z00_10084 = 
(
(long)(BgL_arg2114z00_3937)==0L); }  else 
{ /* Llib/hash.scm 668 */
BgL_test4775z00_10084 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 668 */
BgL_test4775z00_10084 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 668 */
BgL_test4775z00_10084 = ((bool_t)0)
; } } } } } 
if(BgL_test4775z00_10084)
{ /* Llib/hash.scm 668 */
return ((bool_t)1);}  else 
{ /* Llib/hash.scm 671 */
 obj_t BgL_arg1651z00_2027;
{ /* Llib/hash.scm 671 */
 obj_t BgL_pairz00_3943;
if(
PAIRP(BgL_bucketz00_2022))
{ /* Llib/hash.scm 671 */
BgL_pairz00_3943 = BgL_bucketz00_2022; }  else 
{ 
 obj_t BgL_auxz00_10143;
BgL_auxz00_10143 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(26702L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2022); 
FAILURE(BgL_auxz00_10143,BFALSE,BFALSE);} 
BgL_arg1651z00_2027 = 
CDR(BgL_pairz00_3943); } 
{ 
 obj_t BgL_bucketz00_10148;
BgL_bucketz00_10148 = BgL_arg1651z00_2027; 
BgL_bucketz00_2022 = BgL_bucketz00_10148; 
goto BgL_zc3z04anonymousza31647ze3z87_2023;} } } } } } } } } } 

}



/* hashtable-get */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t BgL_tablez00_96, obj_t BgL_keyz00_97)
{
{ /* Llib/hash.scm 676 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_96))
{ /* Llib/hash.scm 678 */
 obj_t BgL_auxz00_10151;
if(
STRINGP(BgL_keyz00_97))
{ /* Llib/hash.scm 678 */
BgL_auxz00_10151 = BgL_keyz00_97
; }  else 
{ 
 obj_t BgL_auxz00_10154;
BgL_auxz00_10154 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27066L), BGl_string4011z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_97); 
FAILURE(BgL_auxz00_10154,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(BgL_tablez00_96, BgL_auxz00_10151);}  else 
{ /* Llib/hash.scm 679 */
 bool_t BgL_test4789z00_10159;
{ /* Llib/hash.scm 300 */
 obj_t BgL_arg1441z00_3945;
{ /* Llib/hash.scm 300 */
 bool_t BgL_test4790z00_10160;
{ /* Llib/hash.scm 300 */
 obj_t BgL_tmpz00_10161;
{ /* Llib/hash.scm 300 */
 obj_t BgL_res2586z00_3949;
{ /* Llib/hash.scm 300 */
 obj_t BgL_aux3333z00_6212;
BgL_aux3333z00_6212 = 
STRUCT_KEY(BgL_tablez00_96); 
if(
SYMBOLP(BgL_aux3333z00_6212))
{ /* Llib/hash.scm 300 */
BgL_res2586z00_3949 = BgL_aux3333z00_6212; }  else 
{ 
 obj_t BgL_auxz00_10165;
BgL_auxz00_10165 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11344L), BGl_string4011z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3333z00_6212); 
FAILURE(BgL_auxz00_10165,BFALSE,BFALSE);} } 
BgL_tmpz00_10161 = BgL_res2586z00_3949; } 
BgL_test4790z00_10160 = 
(BgL_tmpz00_10161==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4790z00_10160)
{ /* Llib/hash.scm 300 */
 int BgL_tmpz00_10170;
BgL_tmpz00_10170 = 
(int)(5L); 
BgL_arg1441z00_3945 = 
STRUCT_REF(BgL_tablez00_96, BgL_tmpz00_10170); }  else 
{ /* Llib/hash.scm 300 */
BgL_arg1441z00_3945 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_96); } } 
{ /* Llib/hash.scm 300 */
 long BgL_n1z00_3950;
{ /* Llib/hash.scm 300 */
 obj_t BgL_tmpz00_10174;
if(
INTEGERP(BgL_arg1441z00_3945))
{ /* Llib/hash.scm 300 */
BgL_tmpz00_10174 = BgL_arg1441z00_3945
; }  else 
{ 
 obj_t BgL_auxz00_10177;
BgL_auxz00_10177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11366L), BGl_string4011z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1441z00_3945); 
FAILURE(BgL_auxz00_10177,BFALSE,BFALSE);} 
BgL_n1z00_3950 = 
(long)CINT(BgL_tmpz00_10174); } 
BgL_test4789z00_10159 = 
(BgL_n1z00_3950==4L); } } 
if(BgL_test4789z00_10159)
{ /* Llib/hash.scm 679 */
 obj_t BgL_auxz00_10183;
if(
STRINGP(BgL_keyz00_97))
{ /* Llib/hash.scm 679 */
BgL_auxz00_10183 = BgL_keyz00_97
; }  else 
{ 
 obj_t BgL_auxz00_10186;
BgL_auxz00_10186 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27133L), BGl_string4011z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_97); 
FAILURE(BgL_auxz00_10186,BFALSE,BFALSE);} 
return 
BGl_stringzd2hashtablezd2getz00zz__hashz00(BgL_tablez00_96, BgL_auxz00_10183);}  else 
{ /* Llib/hash.scm 679 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_96))
{ /* Llib/hash.scm 680 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2getz00zz__weakhashz00(BgL_tablez00_96, BgL_keyz00_97);}  else 
{ /* Llib/hash.scm 680 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2getz00zz__hashz00(BgL_tablez00_96, BgL_keyz00_97);} } } } 

}



/* &hashtable-get */
obj_t BGl_z62hashtablezd2getzb0zz__hashz00(obj_t BgL_envz00_5468, obj_t BgL_tablez00_5469, obj_t BgL_keyz00_5470)
{
{ /* Llib/hash.scm 676 */
{ /* Llib/hash.scm 678 */
 obj_t BgL_auxz00_10195;
if(
STRUCTP(BgL_tablez00_5469))
{ /* Llib/hash.scm 678 */
BgL_auxz00_10195 = BgL_tablez00_5469
; }  else 
{ 
 obj_t BgL_auxz00_10198;
BgL_auxz00_10198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27001L), BGl_string4012z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5469); 
FAILURE(BgL_auxz00_10198,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_10195, BgL_keyz00_5470);} } 

}



/* plain-hashtable-get */
obj_t BGl_plainzd2hashtablezd2getz00zz__hashz00(obj_t BgL_tablez00_98, obj_t BgL_keyz00_99)
{
{ /* Llib/hash.scm 686 */
{ /* Llib/hash.scm 687 */
 obj_t BgL_bucketsz00_2034;
{ /* Llib/hash.scm 687 */
 bool_t BgL_test4796z00_10203;
{ /* Llib/hash.scm 687 */
 obj_t BgL_tmpz00_10204;
{ /* Llib/hash.scm 687 */
 obj_t BgL_res2587z00_3954;
{ /* Llib/hash.scm 687 */
 obj_t BgL_aux3340z00_6219;
BgL_aux3340z00_6219 = 
STRUCT_KEY(BgL_tablez00_98); 
if(
SYMBOLP(BgL_aux3340z00_6219))
{ /* Llib/hash.scm 687 */
BgL_res2587z00_3954 = BgL_aux3340z00_6219; }  else 
{ 
 obj_t BgL_auxz00_10208;
BgL_auxz00_10208 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27544L), BGl_string4013z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3340z00_6219); 
FAILURE(BgL_auxz00_10208,BFALSE,BFALSE);} } 
BgL_tmpz00_10204 = BgL_res2587z00_3954; } 
BgL_test4796z00_10203 = 
(BgL_tmpz00_10204==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4796z00_10203)
{ /* Llib/hash.scm 687 */
 int BgL_tmpz00_10213;
BgL_tmpz00_10213 = 
(int)(2L); 
BgL_bucketsz00_2034 = 
STRUCT_REF(BgL_tablez00_98, BgL_tmpz00_10213); }  else 
{ /* Llib/hash.scm 687 */
BgL_bucketsz00_2034 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_98); } } 
{ /* Llib/hash.scm 687 */
 long BgL_bucketzd2lenzd2_2035;
{ /* Llib/hash.scm 688 */
 obj_t BgL_vectorz00_3955;
if(
VECTORP(BgL_bucketsz00_2034))
{ /* Llib/hash.scm 688 */
BgL_vectorz00_3955 = BgL_bucketsz00_2034; }  else 
{ 
 obj_t BgL_auxz00_10219;
BgL_auxz00_10219 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27602L), BGl_string4013z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2034); 
FAILURE(BgL_auxz00_10219,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2035 = 
VECTOR_LENGTH(BgL_vectorz00_3955); } 
{ /* Llib/hash.scm 688 */
 long BgL_bucketzd2numzd2_2036;
{ /* Llib/hash.scm 689 */
 long BgL_arg1667z00_2047;
BgL_arg1667z00_2047 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_98, BgL_keyz00_99); 
{ /* Llib/hash.scm 689 */
 long BgL_n1z00_3956; long BgL_n2z00_3957;
BgL_n1z00_3956 = BgL_arg1667z00_2047; 
BgL_n2z00_3957 = BgL_bucketzd2lenzd2_2035; 
{ /* Llib/hash.scm 689 */
 bool_t BgL_test4799z00_10225;
{ /* Llib/hash.scm 689 */
 long BgL_arg2221z00_3959;
BgL_arg2221z00_3959 = 
(((BgL_n1z00_3956) | (BgL_n2z00_3957)) & -2147483648); 
BgL_test4799z00_10225 = 
(BgL_arg2221z00_3959==0L); } 
if(BgL_test4799z00_10225)
{ /* Llib/hash.scm 689 */
 int32_t BgL_arg2218z00_3960;
{ /* Llib/hash.scm 689 */
 int32_t BgL_arg2219z00_3961; int32_t BgL_arg2220z00_3962;
BgL_arg2219z00_3961 = 
(int32_t)(BgL_n1z00_3956); 
BgL_arg2220z00_3962 = 
(int32_t)(BgL_n2z00_3957); 
BgL_arg2218z00_3960 = 
(BgL_arg2219z00_3961%BgL_arg2220z00_3962); } 
{ /* Llib/hash.scm 689 */
 long BgL_arg2326z00_3967;
BgL_arg2326z00_3967 = 
(long)(BgL_arg2218z00_3960); 
BgL_bucketzd2numzd2_2036 = 
(long)(BgL_arg2326z00_3967); } }  else 
{ /* Llib/hash.scm 689 */
BgL_bucketzd2numzd2_2036 = 
(BgL_n1z00_3956%BgL_n2z00_3957); } } } } 
{ /* Llib/hash.scm 689 */
 obj_t BgL_bucketz00_2037;
{ /* Llib/hash.scm 690 */
 obj_t BgL_vectorz00_3969;
if(
VECTORP(BgL_bucketsz00_2034))
{ /* Llib/hash.scm 690 */
BgL_vectorz00_3969 = BgL_bucketsz00_2034; }  else 
{ 
 obj_t BgL_auxz00_10236;
BgL_auxz00_10236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27712L), BGl_string4013z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2034); 
FAILURE(BgL_auxz00_10236,BFALSE,BFALSE);} 
BgL_bucketz00_2037 = 
VECTOR_REF(BgL_vectorz00_3969,BgL_bucketzd2numzd2_2036); } 
{ /* Llib/hash.scm 690 */

{ 
 obj_t BgL_bucketz00_2039;
BgL_bucketz00_2039 = BgL_bucketz00_2037; 
BgL_zc3z04anonymousza31657ze3z87_2040:
if(
NULLP(BgL_bucketz00_2039))
{ /* Llib/hash.scm 693 */
return BFALSE;}  else 
{ /* Llib/hash.scm 695 */
 bool_t BgL_test4802z00_10243;
{ /* Llib/hash.scm 695 */
 obj_t BgL_arg1664z00_2045;
{ /* Llib/hash.scm 695 */
 obj_t BgL_pairz00_3971;
if(
PAIRP(BgL_bucketz00_2039))
{ /* Llib/hash.scm 695 */
BgL_pairz00_3971 = BgL_bucketz00_2039; }  else 
{ 
 obj_t BgL_auxz00_10246;
BgL_auxz00_10246 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27843L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2039); 
FAILURE(BgL_auxz00_10246,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 695 */
 obj_t BgL_pairz00_3974;
{ /* Llib/hash.scm 695 */
 obj_t BgL_aux3348z00_6227;
BgL_aux3348z00_6227 = 
CAR(BgL_pairz00_3971); 
if(
PAIRP(BgL_aux3348z00_6227))
{ /* Llib/hash.scm 695 */
BgL_pairz00_3974 = BgL_aux3348z00_6227; }  else 
{ 
 obj_t BgL_auxz00_10253;
BgL_auxz00_10253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27837L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3348z00_6227); 
FAILURE(BgL_auxz00_10253,BFALSE,BFALSE);} } 
BgL_arg1664z00_2045 = 
CAR(BgL_pairz00_3974); } } 
{ /* Llib/hash.scm 695 */
 obj_t BgL_eqtz00_3975;
{ /* Llib/hash.scm 695 */
 bool_t BgL_test4805z00_10258;
{ /* Llib/hash.scm 695 */
 obj_t BgL_tmpz00_10259;
{ /* Llib/hash.scm 695 */
 obj_t BgL_res2588z00_3982;
{ /* Llib/hash.scm 695 */
 obj_t BgL_aux3350z00_6229;
BgL_aux3350z00_6229 = 
STRUCT_KEY(BgL_tablez00_98); 
if(
SYMBOLP(BgL_aux3350z00_6229))
{ /* Llib/hash.scm 695 */
BgL_res2588z00_3982 = BgL_aux3350z00_6229; }  else 
{ 
 obj_t BgL_auxz00_10263;
BgL_auxz00_10263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27813L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3350z00_6229); 
FAILURE(BgL_auxz00_10263,BFALSE,BFALSE);} } 
BgL_tmpz00_10259 = BgL_res2588z00_3982; } 
BgL_test4805z00_10258 = 
(BgL_tmpz00_10259==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4805z00_10258)
{ /* Llib/hash.scm 695 */
 int BgL_tmpz00_10268;
BgL_tmpz00_10268 = 
(int)(3L); 
BgL_eqtz00_3975 = 
STRUCT_REF(BgL_tablez00_98, BgL_tmpz00_10268); }  else 
{ /* Llib/hash.scm 695 */
BgL_eqtz00_3975 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_98); } } 
if(
PROCEDUREP(BgL_eqtz00_3975))
{ /* Llib/hash.scm 695 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3975, 2))
{ /* Llib/hash.scm 695 */
BgL_test4802z00_10243 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3975, BgL_arg1664z00_2045, BgL_keyz00_99))
; }  else 
{ /* Llib/hash.scm 695 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4014z00zz__hashz00,BgL_eqtz00_3975);} }  else 
{ /* Llib/hash.scm 695 */
if(
(BgL_arg1664z00_2045==BgL_keyz00_99))
{ /* Llib/hash.scm 695 */
BgL_test4802z00_10243 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 695 */
if(
STRINGP(BgL_arg1664z00_2045))
{ /* Llib/hash.scm 695 */
if(
STRINGP(BgL_keyz00_99))
{ /* Llib/hash.scm 695 */
 long BgL_l1z00_3985;
BgL_l1z00_3985 = 
STRING_LENGTH(BgL_arg1664z00_2045); 
if(
(BgL_l1z00_3985==
STRING_LENGTH(BgL_keyz00_99)))
{ /* Llib/hash.scm 695 */
 int BgL_arg2114z00_3988;
{ /* Llib/hash.scm 695 */
 char * BgL_auxz00_10295; char * BgL_tmpz00_10293;
BgL_auxz00_10295 = 
BSTRING_TO_STRING(BgL_keyz00_99); 
BgL_tmpz00_10293 = 
BSTRING_TO_STRING(BgL_arg1664z00_2045); 
BgL_arg2114z00_3988 = 
memcmp(BgL_tmpz00_10293, BgL_auxz00_10295, BgL_l1z00_3985); } 
BgL_test4802z00_10243 = 
(
(long)(BgL_arg2114z00_3988)==0L); }  else 
{ /* Llib/hash.scm 695 */
BgL_test4802z00_10243 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 695 */
BgL_test4802z00_10243 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 695 */
BgL_test4802z00_10243 = ((bool_t)0)
; } } } } } 
if(BgL_test4802z00_10243)
{ /* Llib/hash.scm 696 */
 obj_t BgL_pairz00_3994;
if(
PAIRP(BgL_bucketz00_2039))
{ /* Llib/hash.scm 696 */
BgL_pairz00_3994 = BgL_bucketz00_2039; }  else 
{ 
 obj_t BgL_auxz00_10302;
BgL_auxz00_10302 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27868L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2039); 
FAILURE(BgL_auxz00_10302,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 696 */
 obj_t BgL_pairz00_3997;
{ /* Llib/hash.scm 696 */
 obj_t BgL_aux3355z00_6235;
BgL_aux3355z00_6235 = 
CAR(BgL_pairz00_3994); 
if(
PAIRP(BgL_aux3355z00_6235))
{ /* Llib/hash.scm 696 */
BgL_pairz00_3997 = BgL_aux3355z00_6235; }  else 
{ 
 obj_t BgL_auxz00_10309;
BgL_auxz00_10309 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27862L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3355z00_6235); 
FAILURE(BgL_auxz00_10309,BFALSE,BFALSE);} } 
return 
CDR(BgL_pairz00_3997);} }  else 
{ /* Llib/hash.scm 698 */
 obj_t BgL_arg1663z00_2044;
{ /* Llib/hash.scm 698 */
 obj_t BgL_pairz00_3998;
if(
PAIRP(BgL_bucketz00_2039))
{ /* Llib/hash.scm 698 */
BgL_pairz00_3998 = BgL_bucketz00_2039; }  else 
{ 
 obj_t BgL_auxz00_10316;
BgL_auxz00_10316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(27905L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2039); 
FAILURE(BgL_auxz00_10316,BFALSE,BFALSE);} 
BgL_arg1663z00_2044 = 
CDR(BgL_pairz00_3998); } 
{ 
 obj_t BgL_bucketz00_10321;
BgL_bucketz00_10321 = BgL_arg1663z00_2044; 
BgL_bucketz00_2039 = BgL_bucketz00_10321; 
goto BgL_zc3z04anonymousza31657ze3z87_2040;} } } } } } } } } } 

}



/* string-hashtable-get */
BGL_EXPORTED_DEF obj_t BGl_stringzd2hashtablezd2getz00zz__hashz00(obj_t BgL_tablez00_100, obj_t BgL_keyz00_101)
{
{ /* Llib/hash.scm 703 */
{ /* Llib/hash.scm 704 */
 obj_t BgL_bucketsz00_2048;
{ /* Llib/hash.scm 704 */
 bool_t BgL_test4816z00_10322;
{ /* Llib/hash.scm 704 */
 obj_t BgL_tmpz00_10323;
{ /* Llib/hash.scm 704 */
 obj_t BgL_res2589z00_4002;
{ /* Llib/hash.scm 704 */
 obj_t BgL_aux3359z00_6239;
BgL_aux3359z00_6239 = 
STRUCT_KEY(BgL_tablez00_100); 
if(
SYMBOLP(BgL_aux3359z00_6239))
{ /* Llib/hash.scm 704 */
BgL_res2589z00_4002 = BgL_aux3359z00_6239; }  else 
{ 
 obj_t BgL_auxz00_10327;
BgL_auxz00_10327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28219L), BGl_string4017z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3359z00_6239); 
FAILURE(BgL_auxz00_10327,BFALSE,BFALSE);} } 
BgL_tmpz00_10323 = BgL_res2589z00_4002; } 
BgL_test4816z00_10322 = 
(BgL_tmpz00_10323==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4816z00_10322)
{ /* Llib/hash.scm 704 */
 int BgL_tmpz00_10332;
BgL_tmpz00_10332 = 
(int)(2L); 
BgL_bucketsz00_2048 = 
STRUCT_REF(BgL_tablez00_100, BgL_tmpz00_10332); }  else 
{ /* Llib/hash.scm 704 */
BgL_bucketsz00_2048 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_100); } } 
{ /* Llib/hash.scm 704 */
 long BgL_bucketzd2lenzd2_2049;
{ /* Llib/hash.scm 705 */
 obj_t BgL_vectorz00_4003;
if(
VECTORP(BgL_bucketsz00_2048))
{ /* Llib/hash.scm 705 */
BgL_vectorz00_4003 = BgL_bucketsz00_2048; }  else 
{ 
 obj_t BgL_auxz00_10338;
BgL_auxz00_10338 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28277L), BGl_string4017z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2048); 
FAILURE(BgL_auxz00_10338,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2049 = 
VECTOR_LENGTH(BgL_vectorz00_4003); } 
{ /* Llib/hash.scm 705 */
 long BgL_bucketzd2numzd2_2050;
{ /* Llib/hash.scm 706 */
 long BgL_arg1678z00_2061;
{ /* Llib/hash.scm 706 */
 long BgL_arg1681z00_2062;
BgL_arg1681z00_2062 = 
STRING_LENGTH(BgL_keyz00_101); 
BgL_arg1678z00_2061 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_101), 
(int)(0L), 
(int)(BgL_arg1681z00_2062)); } 
{ /* Llib/hash.scm 706 */
 long BgL_n1z00_4005; long BgL_n2z00_4006;
BgL_n1z00_4005 = BgL_arg1678z00_2061; 
BgL_n2z00_4006 = BgL_bucketzd2lenzd2_2049; 
{ /* Llib/hash.scm 706 */
 bool_t BgL_test4819z00_10348;
{ /* Llib/hash.scm 706 */
 long BgL_arg2221z00_4008;
BgL_arg2221z00_4008 = 
(((BgL_n1z00_4005) | (BgL_n2z00_4006)) & -2147483648); 
BgL_test4819z00_10348 = 
(BgL_arg2221z00_4008==0L); } 
if(BgL_test4819z00_10348)
{ /* Llib/hash.scm 706 */
 int32_t BgL_arg2218z00_4009;
{ /* Llib/hash.scm 706 */
 int32_t BgL_arg2219z00_4010; int32_t BgL_arg2220z00_4011;
BgL_arg2219z00_4010 = 
(int32_t)(BgL_n1z00_4005); 
BgL_arg2220z00_4011 = 
(int32_t)(BgL_n2z00_4006); 
BgL_arg2218z00_4009 = 
(BgL_arg2219z00_4010%BgL_arg2220z00_4011); } 
{ /* Llib/hash.scm 706 */
 long BgL_arg2326z00_4016;
BgL_arg2326z00_4016 = 
(long)(BgL_arg2218z00_4009); 
BgL_bucketzd2numzd2_2050 = 
(long)(BgL_arg2326z00_4016); } }  else 
{ /* Llib/hash.scm 706 */
BgL_bucketzd2numzd2_2050 = 
(BgL_n1z00_4005%BgL_n2z00_4006); } } } } 
{ /* Llib/hash.scm 706 */
 obj_t BgL_bucketz00_2051;
{ /* Llib/hash.scm 707 */
 obj_t BgL_vectorz00_4018;
if(
VECTORP(BgL_bucketsz00_2048))
{ /* Llib/hash.scm 707 */
BgL_vectorz00_4018 = BgL_bucketsz00_2048; }  else 
{ 
 obj_t BgL_auxz00_10359;
BgL_auxz00_10359 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28395L), BGl_string4017z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2048); 
FAILURE(BgL_auxz00_10359,BFALSE,BFALSE);} 
BgL_bucketz00_2051 = 
VECTOR_REF(BgL_vectorz00_4018,BgL_bucketzd2numzd2_2050); } 
{ /* Llib/hash.scm 707 */

{ 
 obj_t BgL_bucketz00_2053;
BgL_bucketz00_2053 = BgL_bucketz00_2051; 
BgL_zc3z04anonymousza31668ze3z87_2054:
if(
NULLP(BgL_bucketz00_2053))
{ /* Llib/hash.scm 710 */
return BFALSE;}  else 
{ /* Llib/hash.scm 712 */
 bool_t BgL_test4822z00_10366;
{ /* Llib/hash.scm 712 */
 obj_t BgL_arg1676z00_2059;
{ /* Llib/hash.scm 712 */
 obj_t BgL_pairz00_4020;
if(
PAIRP(BgL_bucketz00_2053))
{ /* Llib/hash.scm 712 */
BgL_pairz00_4020 = BgL_bucketz00_2053; }  else 
{ 
 obj_t BgL_auxz00_10369;
BgL_auxz00_10369 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28512L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2053); 
FAILURE(BgL_auxz00_10369,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 712 */
 obj_t BgL_pairz00_4023;
{ /* Llib/hash.scm 712 */
 obj_t BgL_aux3367z00_6247;
BgL_aux3367z00_6247 = 
CAR(BgL_pairz00_4020); 
if(
PAIRP(BgL_aux3367z00_6247))
{ /* Llib/hash.scm 712 */
BgL_pairz00_4023 = BgL_aux3367z00_6247; }  else 
{ 
 obj_t BgL_auxz00_10376;
BgL_auxz00_10376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28506L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3367z00_6247); 
FAILURE(BgL_auxz00_10376,BFALSE,BFALSE);} } 
BgL_arg1676z00_2059 = 
CAR(BgL_pairz00_4023); } } 
{ /* Llib/hash.scm 712 */
 obj_t BgL_string1z00_4024;
if(
STRINGP(BgL_arg1676z00_2059))
{ /* Llib/hash.scm 712 */
BgL_string1z00_4024 = BgL_arg1676z00_2059; }  else 
{ 
 obj_t BgL_auxz00_10383;
BgL_auxz00_10383 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28518L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1676z00_2059); 
FAILURE(BgL_auxz00_10383,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 712 */
 long BgL_l1z00_4026;
BgL_l1z00_4026 = 
STRING_LENGTH(BgL_string1z00_4024); 
if(
(BgL_l1z00_4026==
STRING_LENGTH(BgL_keyz00_101)))
{ /* Llib/hash.scm 712 */
 int BgL_arg2114z00_4029;
{ /* Llib/hash.scm 712 */
 char * BgL_auxz00_10393; char * BgL_tmpz00_10391;
BgL_auxz00_10393 = 
BSTRING_TO_STRING(BgL_keyz00_101); 
BgL_tmpz00_10391 = 
BSTRING_TO_STRING(BgL_string1z00_4024); 
BgL_arg2114z00_4029 = 
memcmp(BgL_tmpz00_10391, BgL_auxz00_10393, BgL_l1z00_4026); } 
BgL_test4822z00_10366 = 
(
(long)(BgL_arg2114z00_4029)==0L); }  else 
{ /* Llib/hash.scm 712 */
BgL_test4822z00_10366 = ((bool_t)0)
; } } } } 
if(BgL_test4822z00_10366)
{ /* Llib/hash.scm 713 */
 obj_t BgL_pairz00_4035;
if(
PAIRP(BgL_bucketz00_2053))
{ /* Llib/hash.scm 713 */
BgL_pairz00_4035 = BgL_bucketz00_2053; }  else 
{ 
 obj_t BgL_auxz00_10400;
BgL_auxz00_10400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28537L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2053); 
FAILURE(BgL_auxz00_10400,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 713 */
 obj_t BgL_pairz00_4038;
{ /* Llib/hash.scm 713 */
 obj_t BgL_aux3373z00_6253;
BgL_aux3373z00_6253 = 
CAR(BgL_pairz00_4035); 
if(
PAIRP(BgL_aux3373z00_6253))
{ /* Llib/hash.scm 713 */
BgL_pairz00_4038 = BgL_aux3373z00_6253; }  else 
{ 
 obj_t BgL_auxz00_10407;
BgL_auxz00_10407 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28531L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3373z00_6253); 
FAILURE(BgL_auxz00_10407,BFALSE,BFALSE);} } 
return 
CDR(BgL_pairz00_4038);} }  else 
{ /* Llib/hash.scm 715 */
 obj_t BgL_arg1675z00_2058;
{ /* Llib/hash.scm 715 */
 obj_t BgL_pairz00_4039;
if(
PAIRP(BgL_bucketz00_2053))
{ /* Llib/hash.scm 715 */
BgL_pairz00_4039 = BgL_bucketz00_2053; }  else 
{ 
 obj_t BgL_auxz00_10414;
BgL_auxz00_10414 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28574L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2053); 
FAILURE(BgL_auxz00_10414,BFALSE,BFALSE);} 
BgL_arg1675z00_2058 = 
CDR(BgL_pairz00_4039); } 
{ 
 obj_t BgL_bucketz00_10419;
BgL_bucketz00_10419 = BgL_arg1675z00_2058; 
BgL_bucketz00_2053 = BgL_bucketz00_10419; 
goto BgL_zc3z04anonymousza31668ze3z87_2054;} } } } } } } } } } 

}



/* &string-hashtable-get */
obj_t BGl_z62stringzd2hashtablezd2getz62zz__hashz00(obj_t BgL_envz00_5471, obj_t BgL_tablez00_5472, obj_t BgL_keyz00_5473)
{
{ /* Llib/hash.scm 703 */
{ /* Llib/hash.scm 704 */
 obj_t BgL_auxz00_10427; obj_t BgL_auxz00_10420;
if(
STRINGP(BgL_keyz00_5473))
{ /* Llib/hash.scm 704 */
BgL_auxz00_10427 = BgL_keyz00_5473
; }  else 
{ 
 obj_t BgL_auxz00_10430;
BgL_auxz00_10430 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28203L), BGl_string4018z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5473); 
FAILURE(BgL_auxz00_10430,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5472))
{ /* Llib/hash.scm 704 */
BgL_auxz00_10420 = BgL_tablez00_5472
; }  else 
{ 
 obj_t BgL_auxz00_10423;
BgL_auxz00_10423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28203L), BGl_string4018z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5472); 
FAILURE(BgL_auxz00_10423,BFALSE,BFALSE);} 
return 
BGl_stringzd2hashtablezd2getz00zz__hashz00(BgL_auxz00_10420, BgL_auxz00_10427);} } 

}



/* open-string-hashtable-get */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(obj_t BgL_tz00_102, obj_t BgL_keyz00_103)
{
{ /* Llib/hash.scm 720 */
{ /* Llib/hash.scm 721 */
 obj_t BgL_siza7eza7_2063;
{ /* Llib/hash.scm 721 */
 bool_t BgL_test4832z00_10435;
{ /* Llib/hash.scm 721 */
 obj_t BgL_tmpz00_10436;
{ /* Llib/hash.scm 721 */
 obj_t BgL_res2590z00_4043;
{ /* Llib/hash.scm 721 */
 obj_t BgL_aux3381z00_6261;
BgL_aux3381z00_6261 = 
STRUCT_KEY(BgL_tz00_102); 
if(
SYMBOLP(BgL_aux3381z00_6261))
{ /* Llib/hash.scm 721 */
BgL_res2590z00_4043 = BgL_aux3381z00_6261; }  else 
{ 
 obj_t BgL_auxz00_10440;
BgL_auxz00_10440 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28869L), BGl_string4019z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3381z00_6261); 
FAILURE(BgL_auxz00_10440,BFALSE,BFALSE);} } 
BgL_tmpz00_10436 = BgL_res2590z00_4043; } 
BgL_test4832z00_10435 = 
(BgL_tmpz00_10436==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4832z00_10435)
{ /* Llib/hash.scm 721 */
 int BgL_tmpz00_10445;
BgL_tmpz00_10445 = 
(int)(1L); 
BgL_siza7eza7_2063 = 
STRUCT_REF(BgL_tz00_102, BgL_tmpz00_10445); }  else 
{ /* Llib/hash.scm 721 */
BgL_siza7eza7_2063 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_102); } } 
{ /* Llib/hash.scm 721 */
 obj_t BgL_bucketsz00_2064;
{ /* Llib/hash.scm 722 */
 bool_t BgL_test4834z00_10449;
{ /* Llib/hash.scm 722 */
 obj_t BgL_tmpz00_10450;
{ /* Llib/hash.scm 722 */
 obj_t BgL_res2591z00_4047;
{ /* Llib/hash.scm 722 */
 obj_t BgL_aux3383z00_6263;
BgL_aux3383z00_6263 = 
STRUCT_KEY(BgL_tz00_102); 
if(
SYMBOLP(BgL_aux3383z00_6263))
{ /* Llib/hash.scm 722 */
BgL_res2591z00_4047 = BgL_aux3383z00_6263; }  else 
{ 
 obj_t BgL_auxz00_10454;
BgL_auxz00_10454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28912L), BGl_string4019z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3383z00_6263); 
FAILURE(BgL_auxz00_10454,BFALSE,BFALSE);} } 
BgL_tmpz00_10450 = BgL_res2591z00_4047; } 
BgL_test4834z00_10449 = 
(BgL_tmpz00_10450==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4834z00_10449)
{ /* Llib/hash.scm 722 */
 int BgL_tmpz00_10459;
BgL_tmpz00_10459 = 
(int)(2L); 
BgL_bucketsz00_2064 = 
STRUCT_REF(BgL_tz00_102, BgL_tmpz00_10459); }  else 
{ /* Llib/hash.scm 722 */
BgL_bucketsz00_2064 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_102); } } 
{ /* Llib/hash.scm 722 */
 long BgL_hashz00_2065;
{ /* Llib/hash.scm 723 */
 long BgL_arg1704z00_2087;
BgL_arg1704z00_2087 = 
STRING_LENGTH(BgL_keyz00_103); 
BgL_hashz00_2065 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_103), 
(int)(0L), 
(int)(BgL_arg1704z00_2087)); } 
{ /* Llib/hash.scm 723 */

{ /* Llib/hash.scm 724 */
 long BgL_g1069z00_2066;
{ /* Llib/hash.scm 724 */
 long BgL_n1z00_4049; long BgL_n2z00_4050;
BgL_n1z00_4049 = BgL_hashz00_2065; 
{ /* Llib/hash.scm 724 */
 obj_t BgL_tmpz00_10468;
if(
INTEGERP(BgL_siza7eza7_2063))
{ /* Llib/hash.scm 724 */
BgL_tmpz00_10468 = BgL_siza7eza7_2063
; }  else 
{ 
 obj_t BgL_auxz00_10471;
BgL_auxz00_10471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29028L), BGl_string4019z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2063); 
FAILURE(BgL_auxz00_10471,BFALSE,BFALSE);} 
BgL_n2z00_4050 = 
(long)CINT(BgL_tmpz00_10468); } 
{ /* Llib/hash.scm 724 */
 bool_t BgL_test4837z00_10476;
{ /* Llib/hash.scm 724 */
 long BgL_arg2221z00_4052;
BgL_arg2221z00_4052 = 
(((BgL_n1z00_4049) | (BgL_n2z00_4050)) & -2147483648); 
BgL_test4837z00_10476 = 
(BgL_arg2221z00_4052==0L); } 
if(BgL_test4837z00_10476)
{ /* Llib/hash.scm 724 */
 int32_t BgL_arg2218z00_4053;
{ /* Llib/hash.scm 724 */
 int32_t BgL_arg2219z00_4054; int32_t BgL_arg2220z00_4055;
BgL_arg2219z00_4054 = 
(int32_t)(BgL_n1z00_4049); 
BgL_arg2220z00_4055 = 
(int32_t)(BgL_n2z00_4050); 
BgL_arg2218z00_4053 = 
(BgL_arg2219z00_4054%BgL_arg2220z00_4055); } 
{ /* Llib/hash.scm 724 */
 long BgL_arg2326z00_4060;
BgL_arg2326z00_4060 = 
(long)(BgL_arg2218z00_4053); 
BgL_g1069z00_2066 = 
(long)(BgL_arg2326z00_4060); } }  else 
{ /* Llib/hash.scm 724 */
BgL_g1069z00_2066 = 
(BgL_n1z00_4049%BgL_n2z00_4050); } } } 
{ 
 long BgL_offz00_2068; long BgL_iz00_2069;
BgL_offz00_2068 = BgL_g1069z00_2066; 
BgL_iz00_2069 = 1L; 
BgL_zc3z04anonymousza31682ze3z87_2070:
{ /* Llib/hash.scm 726 */
 long BgL_off3z00_2071;
BgL_off3z00_2071 = 
(BgL_offz00_2068*3L); 
{ /* Llib/hash.scm 727 */
 bool_t BgL_test4838z00_10486;
{ /* Llib/hash.scm 727 */
 obj_t BgL_vectorz00_4063;
if(
VECTORP(BgL_bucketsz00_2064))
{ /* Llib/hash.scm 727 */
BgL_vectorz00_4063 = BgL_bucketsz00_2064; }  else 
{ 
 obj_t BgL_auxz00_10489;
BgL_auxz00_10489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29096L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2064); 
FAILURE(BgL_auxz00_10489,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 727 */
 bool_t BgL_test4840z00_10493;
{ /* Llib/hash.scm 727 */
 long BgL_tmpz00_10494;
BgL_tmpz00_10494 = 
VECTOR_LENGTH(BgL_vectorz00_4063); 
BgL_test4840z00_10493 = 
BOUND_CHECK(BgL_off3z00_2071, BgL_tmpz00_10494); } 
if(BgL_test4840z00_10493)
{ /* Llib/hash.scm 727 */
BgL_test4838z00_10486 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4063,BgL_off3z00_2071))
; }  else 
{ 
 obj_t BgL_auxz00_10499;
BgL_auxz00_10499 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29084L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4063, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4063)), 
(int)(BgL_off3z00_2071)); 
FAILURE(BgL_auxz00_10499,BFALSE,BFALSE);} } } 
if(BgL_test4838z00_10486)
{ /* Llib/hash.scm 728 */
 bool_t BgL_test4841z00_10506;
{ /* Llib/hash.scm 728 */
 obj_t BgL_arg1703z00_2085;
{ /* Llib/hash.scm 728 */
 obj_t BgL_vectorz00_4065;
if(
VECTORP(BgL_bucketsz00_2064))
{ /* Llib/hash.scm 728 */
BgL_vectorz00_4065 = BgL_bucketsz00_2064; }  else 
{ 
 obj_t BgL_auxz00_10509;
BgL_auxz00_10509 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29144L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2064); 
FAILURE(BgL_auxz00_10509,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 728 */
 bool_t BgL_test4843z00_10513;
{ /* Llib/hash.scm 728 */
 long BgL_tmpz00_10514;
BgL_tmpz00_10514 = 
VECTOR_LENGTH(BgL_vectorz00_4065); 
BgL_test4843z00_10513 = 
BOUND_CHECK(BgL_off3z00_2071, BgL_tmpz00_10514); } 
if(BgL_test4843z00_10513)
{ /* Llib/hash.scm 728 */
BgL_arg1703z00_2085 = 
VECTOR_REF(BgL_vectorz00_4065,BgL_off3z00_2071); }  else 
{ 
 obj_t BgL_auxz00_10518;
BgL_auxz00_10518 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29132L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4065, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4065)), 
(int)(BgL_off3z00_2071)); 
FAILURE(BgL_auxz00_10518,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 728 */
 obj_t BgL_string1z00_4067;
if(
STRINGP(BgL_arg1703z00_2085))
{ /* Llib/hash.scm 728 */
BgL_string1z00_4067 = BgL_arg1703z00_2085; }  else 
{ 
 obj_t BgL_auxz00_10527;
BgL_auxz00_10527 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29156L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1703z00_2085); 
FAILURE(BgL_auxz00_10527,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 728 */
 long BgL_l1z00_4069;
BgL_l1z00_4069 = 
STRING_LENGTH(BgL_string1z00_4067); 
if(
(BgL_l1z00_4069==
STRING_LENGTH(BgL_keyz00_103)))
{ /* Llib/hash.scm 728 */
 int BgL_arg2114z00_4072;
{ /* Llib/hash.scm 728 */
 char * BgL_auxz00_10537; char * BgL_tmpz00_10535;
BgL_auxz00_10537 = 
BSTRING_TO_STRING(BgL_keyz00_103); 
BgL_tmpz00_10535 = 
BSTRING_TO_STRING(BgL_string1z00_4067); 
BgL_arg2114z00_4072 = 
memcmp(BgL_tmpz00_10535, BgL_auxz00_10537, BgL_l1z00_4069); } 
BgL_test4841z00_10506 = 
(
(long)(BgL_arg2114z00_4072)==0L); }  else 
{ /* Llib/hash.scm 728 */
BgL_test4841z00_10506 = ((bool_t)0)
; } } } } 
if(BgL_test4841z00_10506)
{ /* Llib/hash.scm 729 */
 bool_t BgL_test4846z00_10542;
{ /* Llib/hash.scm 729 */
 long BgL_arg1692z00_2078;
BgL_arg1692z00_2078 = 
(BgL_off3z00_2071+2L); 
{ /* Llib/hash.scm 729 */
 obj_t BgL_vectorz00_4079;
if(
VECTORP(BgL_bucketsz00_2064))
{ /* Llib/hash.scm 729 */
BgL_vectorz00_4079 = BgL_bucketsz00_2064; }  else 
{ 
 obj_t BgL_auxz00_10546;
BgL_auxz00_10546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29186L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2064); 
FAILURE(BgL_auxz00_10546,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 729 */
 bool_t BgL_test4848z00_10550;
{ /* Llib/hash.scm 729 */
 long BgL_tmpz00_10551;
BgL_tmpz00_10551 = 
VECTOR_LENGTH(BgL_vectorz00_4079); 
BgL_test4848z00_10550 = 
BOUND_CHECK(BgL_arg1692z00_2078, BgL_tmpz00_10551); } 
if(BgL_test4848z00_10550)
{ /* Llib/hash.scm 729 */
BgL_test4846z00_10542 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4079,BgL_arg1692z00_2078))
; }  else 
{ 
 obj_t BgL_auxz00_10556;
BgL_auxz00_10556 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29174L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4079, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4079)), 
(int)(BgL_arg1692z00_2078)); 
FAILURE(BgL_auxz00_10556,BFALSE,BFALSE);} } } } 
if(BgL_test4846z00_10542)
{ /* Llib/hash.scm 730 */
 long BgL_arg1691z00_2077;
BgL_arg1691z00_2077 = 
(BgL_off3z00_2071+1L); 
{ /* Llib/hash.scm 730 */
 obj_t BgL_vectorz00_4082;
if(
VECTORP(BgL_bucketsz00_2064))
{ /* Llib/hash.scm 730 */
BgL_vectorz00_4082 = BgL_bucketsz00_2064; }  else 
{ 
 obj_t BgL_auxz00_10566;
BgL_auxz00_10566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29228L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2064); 
FAILURE(BgL_auxz00_10566,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 730 */
 bool_t BgL_test4850z00_10570;
{ /* Llib/hash.scm 730 */
 long BgL_tmpz00_10571;
BgL_tmpz00_10571 = 
VECTOR_LENGTH(BgL_vectorz00_4082); 
BgL_test4850z00_10570 = 
BOUND_CHECK(BgL_arg1691z00_2077, BgL_tmpz00_10571); } 
if(BgL_test4850z00_10570)
{ /* Llib/hash.scm 730 */
return 
VECTOR_REF(BgL_vectorz00_4082,BgL_arg1691z00_2077);}  else 
{ 
 obj_t BgL_auxz00_10575;
BgL_auxz00_10575 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29216L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4082, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4082)), 
(int)(BgL_arg1691z00_2077)); 
FAILURE(BgL_auxz00_10575,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 729 */
return BFALSE;} }  else 
{ /* Llib/hash.scm 731 */
 long BgL_noffz00_2079;
BgL_noffz00_2079 = 
(BgL_offz00_2068+
(BgL_iz00_2069*BgL_iz00_2069)); 
{ /* Llib/hash.scm 732 */
 bool_t BgL_test4851z00_10584;
{ /* Llib/hash.scm 732 */
 long BgL_n2z00_4089;
{ /* Llib/hash.scm 732 */
 obj_t BgL_tmpz00_10585;
if(
INTEGERP(BgL_siza7eza7_2063))
{ /* Llib/hash.scm 732 */
BgL_tmpz00_10585 = BgL_siza7eza7_2063
; }  else 
{ 
 obj_t BgL_auxz00_10588;
BgL_auxz00_10588 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29313L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2063); 
FAILURE(BgL_auxz00_10588,BFALSE,BFALSE);} 
BgL_n2z00_4089 = 
(long)CINT(BgL_tmpz00_10585); } 
BgL_test4851z00_10584 = 
(BgL_noffz00_2079>=BgL_n2z00_4089); } 
if(BgL_test4851z00_10584)
{ /* Llib/hash.scm 733 */
 long BgL_arg1699z00_2081; long BgL_arg1700z00_2082;
{ /* Llib/hash.scm 733 */
 long BgL_n1z00_4090; long BgL_n2z00_4091;
BgL_n1z00_4090 = BgL_noffz00_2079; 
{ /* Llib/hash.scm 733 */
 obj_t BgL_tmpz00_10594;
if(
INTEGERP(BgL_siza7eza7_2063))
{ /* Llib/hash.scm 733 */
BgL_tmpz00_10594 = BgL_siza7eza7_2063
; }  else 
{ 
 obj_t BgL_auxz00_10597;
BgL_auxz00_10597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29348L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2063); 
FAILURE(BgL_auxz00_10597,BFALSE,BFALSE);} 
BgL_n2z00_4091 = 
(long)CINT(BgL_tmpz00_10594); } 
{ /* Llib/hash.scm 733 */
 bool_t BgL_test4854z00_10602;
{ /* Llib/hash.scm 733 */
 long BgL_arg2221z00_4093;
BgL_arg2221z00_4093 = 
(((BgL_n1z00_4090) | (BgL_n2z00_4091)) & -2147483648); 
BgL_test4854z00_10602 = 
(BgL_arg2221z00_4093==0L); } 
if(BgL_test4854z00_10602)
{ /* Llib/hash.scm 733 */
 int32_t BgL_arg2218z00_4094;
{ /* Llib/hash.scm 733 */
 int32_t BgL_arg2219z00_4095; int32_t BgL_arg2220z00_4096;
BgL_arg2219z00_4095 = 
(int32_t)(BgL_n1z00_4090); 
BgL_arg2220z00_4096 = 
(int32_t)(BgL_n2z00_4091); 
BgL_arg2218z00_4094 = 
(BgL_arg2219z00_4095%BgL_arg2220z00_4096); } 
{ /* Llib/hash.scm 733 */
 long BgL_arg2326z00_4101;
BgL_arg2326z00_4101 = 
(long)(BgL_arg2218z00_4094); 
BgL_arg1699z00_2081 = 
(long)(BgL_arg2326z00_4101); } }  else 
{ /* Llib/hash.scm 733 */
BgL_arg1699z00_2081 = 
(BgL_n1z00_4090%BgL_n2z00_4091); } } } 
BgL_arg1700z00_2082 = 
(BgL_iz00_2069+1L); 
{ 
 long BgL_iz00_10613; long BgL_offz00_10612;
BgL_offz00_10612 = BgL_arg1699z00_2081; 
BgL_iz00_10613 = BgL_arg1700z00_2082; 
BgL_iz00_2069 = BgL_iz00_10613; 
BgL_offz00_2068 = BgL_offz00_10612; 
goto BgL_zc3z04anonymousza31682ze3z87_2070;} }  else 
{ 
 long BgL_iz00_10615; long BgL_offz00_10614;
BgL_offz00_10614 = BgL_noffz00_2079; 
BgL_iz00_10615 = 
(BgL_iz00_2069+1L); 
BgL_iz00_2069 = BgL_iz00_10615; 
BgL_offz00_2068 = BgL_offz00_10614; 
goto BgL_zc3z04anonymousza31682ze3z87_2070;} } } }  else 
{ /* Llib/hash.scm 727 */
return BFALSE;} } } } } } } } } } 

}



/* &open-string-hashtable-get */
obj_t BGl_z62openzd2stringzd2hashtablezd2getzb0zz__hashz00(obj_t BgL_envz00_5474, obj_t BgL_tz00_5475, obj_t BgL_keyz00_5476)
{
{ /* Llib/hash.scm 720 */
{ /* Llib/hash.scm 721 */
 obj_t BgL_auxz00_10624; obj_t BgL_auxz00_10617;
if(
STRINGP(BgL_keyz00_5476))
{ /* Llib/hash.scm 721 */
BgL_auxz00_10624 = BgL_keyz00_5476
; }  else 
{ 
 obj_t BgL_auxz00_10627;
BgL_auxz00_10627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28856L), BGl_string4020z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5476); 
FAILURE(BgL_auxz00_10627,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tz00_5475))
{ /* Llib/hash.scm 721 */
BgL_auxz00_10617 = BgL_tz00_5475
; }  else 
{ 
 obj_t BgL_auxz00_10620;
BgL_auxz00_10620 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(28856L), BGl_string4020z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tz00_5475); 
FAILURE(BgL_auxz00_10620,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2getzd2zz__hashz00(BgL_auxz00_10617, BgL_auxz00_10624);} } 

}



/* $open-string-hashtable-get */
BGL_EXPORTED_DEF obj_t BGl_z42openzd2stringzd2hashtablezd2getz90zz__hashz00(obj_t BgL_tz00_104, char * BgL_keyz00_105)
{
{ /* Llib/hash.scm 741 */
{ /* Llib/hash.scm 744 */
 obj_t BgL_siza7eza7_2088;
{ /* Llib/hash.scm 744 */
 bool_t BgL_test4857z00_10632;
{ /* Llib/hash.scm 744 */
 obj_t BgL_tmpz00_10633;
{ /* Llib/hash.scm 744 */
 obj_t BgL_res2592z00_4108;
{ /* Llib/hash.scm 744 */
 obj_t BgL_aux3402z00_6282;
BgL_aux3402z00_6282 = 
STRUCT_KEY(BgL_tz00_104); 
if(
SYMBOLP(BgL_aux3402z00_6282))
{ /* Llib/hash.scm 744 */
BgL_res2592z00_4108 = BgL_aux3402z00_6282; }  else 
{ 
 obj_t BgL_auxz00_10637;
BgL_auxz00_10637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29866L), BGl_string4021z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3402z00_6282); 
FAILURE(BgL_auxz00_10637,BFALSE,BFALSE);} } 
BgL_tmpz00_10633 = BgL_res2592z00_4108; } 
BgL_test4857z00_10632 = 
(BgL_tmpz00_10633==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4857z00_10632)
{ /* Llib/hash.scm 744 */
 int BgL_tmpz00_10642;
BgL_tmpz00_10642 = 
(int)(1L); 
BgL_siza7eza7_2088 = 
STRUCT_REF(BgL_tz00_104, BgL_tmpz00_10642); }  else 
{ /* Llib/hash.scm 744 */
BgL_siza7eza7_2088 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_104); } } 
{ /* Llib/hash.scm 744 */
 obj_t BgL_bucketsz00_2089;
{ /* Llib/hash.scm 745 */
 bool_t BgL_test4859z00_10646;
{ /* Llib/hash.scm 745 */
 obj_t BgL_tmpz00_10647;
{ /* Llib/hash.scm 745 */
 obj_t BgL_res2593z00_4112;
{ /* Llib/hash.scm 745 */
 obj_t BgL_aux3404z00_6284;
BgL_aux3404z00_6284 = 
STRUCT_KEY(BgL_tz00_104); 
if(
SYMBOLP(BgL_aux3404z00_6284))
{ /* Llib/hash.scm 745 */
BgL_res2593z00_4112 = BgL_aux3404z00_6284; }  else 
{ 
 obj_t BgL_auxz00_10651;
BgL_auxz00_10651 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29913L), BGl_string4021z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3404z00_6284); 
FAILURE(BgL_auxz00_10651,BFALSE,BFALSE);} } 
BgL_tmpz00_10647 = BgL_res2593z00_4112; } 
BgL_test4859z00_10646 = 
(BgL_tmpz00_10647==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4859z00_10646)
{ /* Llib/hash.scm 745 */
 int BgL_tmpz00_10656;
BgL_tmpz00_10656 = 
(int)(2L); 
BgL_bucketsz00_2089 = 
STRUCT_REF(BgL_tz00_104, BgL_tmpz00_10656); }  else 
{ /* Llib/hash.scm 745 */
BgL_bucketsz00_2089 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_104); } } 
{ /* Llib/hash.scm 745 */
 long BgL_lenz00_2090;
BgL_lenz00_2090 = 
strlen(BgL_keyz00_105); 
{ /* Llib/hash.scm 746 */
 long BgL_hashz00_2091;
BgL_hashz00_2091 = 
bgl_string_hash(BgL_keyz00_105, 
(int)(0L), 
(int)(BgL_lenz00_2090)); 
{ /* Llib/hash.scm 747 */

{ /* Llib/hash.scm 748 */
 long BgL_g1070z00_2092;
{ /* Llib/hash.scm 748 */
 long BgL_n1z00_4113; long BgL_n2z00_4114;
BgL_n1z00_4113 = BgL_hashz00_2091; 
{ /* Llib/hash.scm 748 */
 obj_t BgL_tmpz00_10664;
if(
INTEGERP(BgL_siza7eza7_2088))
{ /* Llib/hash.scm 748 */
BgL_tmpz00_10664 = BgL_siza7eza7_2088
; }  else 
{ 
 obj_t BgL_auxz00_10667;
BgL_auxz00_10667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30041L), BGl_string4021z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2088); 
FAILURE(BgL_auxz00_10667,BFALSE,BFALSE);} 
BgL_n2z00_4114 = 
(long)CINT(BgL_tmpz00_10664); } 
{ /* Llib/hash.scm 748 */
 bool_t BgL_test4862z00_10672;
{ /* Llib/hash.scm 748 */
 long BgL_arg2221z00_4116;
BgL_arg2221z00_4116 = 
(((BgL_n1z00_4113) | (BgL_n2z00_4114)) & -2147483648); 
BgL_test4862z00_10672 = 
(BgL_arg2221z00_4116==0L); } 
if(BgL_test4862z00_10672)
{ /* Llib/hash.scm 748 */
 int32_t BgL_arg2218z00_4117;
{ /* Llib/hash.scm 748 */
 int32_t BgL_arg2219z00_4118; int32_t BgL_arg2220z00_4119;
BgL_arg2219z00_4118 = 
(int32_t)(BgL_n1z00_4113); 
BgL_arg2220z00_4119 = 
(int32_t)(BgL_n2z00_4114); 
BgL_arg2218z00_4117 = 
(BgL_arg2219z00_4118%BgL_arg2220z00_4119); } 
{ /* Llib/hash.scm 748 */
 long BgL_arg2326z00_4124;
BgL_arg2326z00_4124 = 
(long)(BgL_arg2218z00_4117); 
BgL_g1070z00_2092 = 
(long)(BgL_arg2326z00_4124); } }  else 
{ /* Llib/hash.scm 748 */
BgL_g1070z00_2092 = 
(BgL_n1z00_4113%BgL_n2z00_4114); } } } 
{ 
 long BgL_offz00_2094; long BgL_iz00_2095;
BgL_offz00_2094 = BgL_g1070z00_2092; 
BgL_iz00_2095 = 1L; 
{ /* Llib/hash.scm 750 */
 long BgL_off3z00_2097;
BgL_off3z00_2097 = 
(BgL_offz00_2094*3L); 
{ /* Llib/hash.scm 751 */
 bool_t BgL_test4863z00_10682;
{ /* Llib/hash.scm 751 */
 obj_t BgL_vectorz00_4127;
if(
VECTORP(BgL_bucketsz00_2089))
{ /* Llib/hash.scm 751 */
BgL_vectorz00_4127 = BgL_bucketsz00_2089; }  else 
{ 
 obj_t BgL_auxz00_10685;
BgL_auxz00_10685 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30114L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2089); 
FAILURE(BgL_auxz00_10685,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 751 */
 bool_t BgL_test4865z00_10689;
{ /* Llib/hash.scm 751 */
 long BgL_tmpz00_10690;
BgL_tmpz00_10690 = 
VECTOR_LENGTH(BgL_vectorz00_4127); 
BgL_test4865z00_10689 = 
BOUND_CHECK(BgL_off3z00_2097, BgL_tmpz00_10690); } 
if(BgL_test4865z00_10689)
{ /* Llib/hash.scm 751 */
BgL_test4863z00_10682 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4127,BgL_off3z00_2097))
; }  else 
{ 
 obj_t BgL_auxz00_10695;
BgL_auxz00_10695 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30102L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4127, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4127)), 
(int)(BgL_off3z00_2097)); 
FAILURE(BgL_auxz00_10695,BFALSE,BFALSE);} } } 
if(BgL_test4863z00_10682)
{ /* Llib/hash.scm 751 */
{ /* Llib/hash.scm 752 */
 obj_t BgL_arg1723z00_2111;
{ /* Llib/hash.scm 752 */
 obj_t BgL_vectorz00_4129;
if(
VECTORP(BgL_bucketsz00_2089))
{ /* Llib/hash.scm 752 */
BgL_vectorz00_4129 = BgL_bucketsz00_2089; }  else 
{ 
 obj_t BgL_auxz00_10704;
BgL_auxz00_10704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30158L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2089); 
FAILURE(BgL_auxz00_10704,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 752 */
 bool_t BgL_test4867z00_10708;
{ /* Llib/hash.scm 752 */
 long BgL_tmpz00_10709;
BgL_tmpz00_10709 = 
VECTOR_LENGTH(BgL_vectorz00_4129); 
BgL_test4867z00_10708 = 
BOUND_CHECK(BgL_off3z00_2097, BgL_tmpz00_10709); } 
if(BgL_test4867z00_10708)
{ /* Llib/hash.scm 752 */
BgL_arg1723z00_2111 = 
VECTOR_REF(BgL_vectorz00_4129,BgL_off3z00_2097); }  else 
{ 
 obj_t BgL_auxz00_10713;
BgL_auxz00_10713 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30146L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4129, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4129)), 
(int)(BgL_off3z00_2097)); 
FAILURE(BgL_auxz00_10713,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 752 */
 int BgL_xz00_6293;
{ /* Llib/hash.scm 752 */
 char * BgL_tmpz00_10720;
{ /* Llib/hash.scm 752 */
 obj_t BgL_tmpz00_10721;
if(
STRINGP(BgL_arg1723z00_2111))
{ /* Llib/hash.scm 752 */
BgL_tmpz00_10721 = BgL_arg1723z00_2111
; }  else 
{ 
 obj_t BgL_auxz00_10724;
BgL_auxz00_10724 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30170L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1723z00_2111); 
FAILURE(BgL_auxz00_10724,BFALSE,BFALSE);} 
BgL_tmpz00_10720 = 
BSTRING_TO_STRING(BgL_tmpz00_10721); } 
BgL_xz00_6293 = 
memcmp(BgL_tmpz00_10720, BgL_keyz00_105, BgL_lenz00_2090); } ((bool_t)1); } } 
{ /* Llib/hash.scm 753 */
 bool_t BgL_test4869z00_10730;
{ /* Llib/hash.scm 753 */
 long BgL_arg1714z00_2104;
BgL_arg1714z00_2104 = 
(BgL_off3z00_2097+2L); 
{ /* Llib/hash.scm 753 */
 obj_t BgL_vectorz00_4132;
if(
VECTORP(BgL_bucketsz00_2089))
{ /* Llib/hash.scm 753 */
BgL_vectorz00_4132 = BgL_bucketsz00_2089; }  else 
{ 
 obj_t BgL_auxz00_10734;
BgL_auxz00_10734 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30208L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2089); 
FAILURE(BgL_auxz00_10734,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 753 */
 bool_t BgL_test4871z00_10738;
{ /* Llib/hash.scm 753 */
 long BgL_tmpz00_10739;
BgL_tmpz00_10739 = 
VECTOR_LENGTH(BgL_vectorz00_4132); 
BgL_test4871z00_10738 = 
BOUND_CHECK(BgL_arg1714z00_2104, BgL_tmpz00_10739); } 
if(BgL_test4871z00_10738)
{ /* Llib/hash.scm 753 */
BgL_test4869z00_10730 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4132,BgL_arg1714z00_2104))
; }  else 
{ 
 obj_t BgL_auxz00_10744;
BgL_auxz00_10744 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30196L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4132, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4132)), 
(int)(BgL_arg1714z00_2104)); 
FAILURE(BgL_auxz00_10744,BFALSE,BFALSE);} } } } 
if(BgL_test4869z00_10730)
{ /* Llib/hash.scm 754 */
 long BgL_arg1711z00_2103;
BgL_arg1711z00_2103 = 
(BgL_off3z00_2097+1L); 
{ /* Llib/hash.scm 754 */
 obj_t BgL_vectorz00_4135;
if(
VECTORP(BgL_bucketsz00_2089))
{ /* Llib/hash.scm 754 */
BgL_vectorz00_4135 = BgL_bucketsz00_2089; }  else 
{ 
 obj_t BgL_auxz00_10754;
BgL_auxz00_10754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30247L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2089); 
FAILURE(BgL_auxz00_10754,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 754 */
 bool_t BgL_test4873z00_10758;
{ /* Llib/hash.scm 754 */
 long BgL_tmpz00_10759;
BgL_tmpz00_10759 = 
VECTOR_LENGTH(BgL_vectorz00_4135); 
BgL_test4873z00_10758 = 
BOUND_CHECK(BgL_arg1711z00_2103, BgL_tmpz00_10759); } 
if(BgL_test4873z00_10758)
{ /* Llib/hash.scm 754 */
return 
VECTOR_REF(BgL_vectorz00_4135,BgL_arg1711z00_2103);}  else 
{ 
 obj_t BgL_auxz00_10763;
BgL_auxz00_10763 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30235L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4135, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4135)), 
(int)(BgL_arg1711z00_2103)); 
FAILURE(BgL_auxz00_10763,BFALSE,BFALSE);} } } }  else 
{ /* Llib/hash.scm 753 */
return BFALSE;} } }  else 
{ /* Llib/hash.scm 751 */
return BFALSE;} } } } } } } } } } } 

}



/* &$open-string-hashtable-get */
obj_t BGl_z62z42openzd2stringzd2hashtablezd2getzf2zz__hashz00(obj_t BgL_envz00_5477, obj_t BgL_tz00_5478, obj_t BgL_keyz00_5479)
{
{ /* Llib/hash.scm 741 */
{ /* Llib/hash.scm 744 */
 char * BgL_auxz00_10777; obj_t BgL_auxz00_10770;
{ /* Llib/hash.scm 744 */
 obj_t BgL_tmpz00_10778;
if(
STRINGP(BgL_keyz00_5479))
{ /* Llib/hash.scm 744 */
BgL_tmpz00_10778 = BgL_keyz00_5479
; }  else 
{ 
 obj_t BgL_auxz00_10781;
BgL_auxz00_10781 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29853L), BGl_string4022z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5479); 
FAILURE(BgL_auxz00_10781,BFALSE,BFALSE);} 
BgL_auxz00_10777 = 
BSTRING_TO_STRING(BgL_tmpz00_10778); } 
if(
STRUCTP(BgL_tz00_5478))
{ /* Llib/hash.scm 744 */
BgL_auxz00_10770 = BgL_tz00_5478
; }  else 
{ 
 obj_t BgL_auxz00_10773;
BgL_auxz00_10773 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(29853L), BGl_string4022z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tz00_5478); 
FAILURE(BgL_auxz00_10773,BFALSE,BFALSE);} 
return 
BGl_z42openzd2stringzd2hashtablezd2getz90zz__hashz00(BgL_auxz00_10770, BgL_auxz00_10777);} } 

}



/* hashtable-put! */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t BgL_tablez00_106, obj_t BgL_keyz00_107, obj_t BgL_objz00_108)
{
{ /* Llib/hash.scm 765 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_106))
{ /* Llib/hash.scm 768 */
 obj_t BgL_keyz00_4161;
if(
STRINGP(BgL_keyz00_107))
{ /* Llib/hash.scm 768 */
BgL_keyz00_4161 = BgL_keyz00_107; }  else 
{ 
 obj_t BgL_auxz00_10791;
BgL_auxz00_10791 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30852L), BGl_string4023z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_107); 
FAILURE(BgL_auxz00_10791,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 779 */
 long BgL_arg1726z00_4162;
{ /* Llib/hash.scm 779 */
 long BgL_arg1727z00_4163;
BgL_arg1727z00_4163 = 
STRING_LENGTH(BgL_keyz00_4161); 
BgL_arg1726z00_4162 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_4161), 
(int)(0L), 
(int)(BgL_arg1727z00_4163)); } 
return 
BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(BgL_tablez00_106, BgL_keyz00_4161, BgL_objz00_108, 
BINT(BgL_arg1726z00_4162));} }  else 
{ /* Llib/hash.scm 767 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_106))
{ /* Llib/hash.scm 769 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(BgL_tablez00_106, BgL_keyz00_107, BgL_objz00_108);}  else 
{ /* Llib/hash.scm 769 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2putz12z12zz__hashz00(BgL_tablez00_106, BgL_keyz00_107, BgL_objz00_108);} } } 

}



/* &hashtable-put! */
obj_t BGl_z62hashtablezd2putz12za2zz__hashz00(obj_t BgL_envz00_5480, obj_t BgL_tablez00_5481, obj_t BgL_keyz00_5482, obj_t BgL_objz00_5483)
{
{ /* Llib/hash.scm 765 */
{ /* Llib/hash.scm 767 */
 obj_t BgL_auxz00_10806;
if(
STRUCTP(BgL_tablez00_5481))
{ /* Llib/hash.scm 767 */
BgL_auxz00_10806 = BgL_tablez00_5481
; }  else 
{ 
 obj_t BgL_auxz00_10809;
BgL_auxz00_10809 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(30779L), BGl_string4024z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5481); 
FAILURE(BgL_auxz00_10809,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_10806, BgL_keyz00_5482, BgL_objz00_5483);} } 

}



/* open-string-hashtable-put! */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2putz12zc0zz__hashz00(obj_t BgL_tablez00_109, obj_t BgL_keyz00_110, obj_t BgL_objz00_111)
{
{ /* Llib/hash.scm 777 */
{ /* Llib/hash.scm 779 */
 long BgL_arg1726z00_4165;
{ /* Llib/hash.scm 779 */
 long BgL_arg1727z00_4166;
BgL_arg1727z00_4166 = 
STRING_LENGTH(BgL_keyz00_110); 
BgL_arg1726z00_4165 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_110), 
(int)(0L), 
(int)(BgL_arg1727z00_4166)); } 
return 
BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(BgL_tablez00_109, BgL_keyz00_110, BgL_objz00_111, 
BINT(BgL_arg1726z00_4165));} } 

}



/* &open-string-hashtable-put! */
obj_t BGl_z62openzd2stringzd2hashtablezd2putz12za2zz__hashz00(obj_t BgL_envz00_5484, obj_t BgL_tablez00_5485, obj_t BgL_keyz00_5486, obj_t BgL_objz00_5487)
{
{ /* Llib/hash.scm 777 */
{ /* Llib/hash.scm 779 */
 obj_t BgL_auxz00_10828; obj_t BgL_auxz00_10821;
if(
STRINGP(BgL_keyz00_5486))
{ /* Llib/hash.scm 779 */
BgL_auxz00_10828 = BgL_keyz00_5486
; }  else 
{ 
 obj_t BgL_auxz00_10831;
BgL_auxz00_10831 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31346L), BGl_string4025z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5486); 
FAILURE(BgL_auxz00_10831,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5485))
{ /* Llib/hash.scm 779 */
BgL_auxz00_10821 = BgL_tablez00_5485
; }  else 
{ 
 obj_t BgL_auxz00_10824;
BgL_auxz00_10824 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31346L), BGl_string4025z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5485); 
FAILURE(BgL_auxz00_10824,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_10821, BgL_auxz00_10828, BgL_objz00_5487);} } 

}



/* open-string-hashtable-put/hash! */
obj_t BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(obj_t BgL_tz00_112, obj_t BgL_keyz00_113, obj_t BgL_valz00_114, obj_t BgL_hashz00_115)
{
{ /* Llib/hash.scm 784 */
BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00:
{ /* Llib/hash.scm 785 */
 obj_t BgL_siza7eza7_2117; obj_t BgL_bucketsz00_2118;
{ /* Llib/hash.scm 785 */
 bool_t BgL_test4882z00_10836;
{ /* Llib/hash.scm 785 */
 obj_t BgL_tmpz00_10837;
{ /* Llib/hash.scm 785 */
 obj_t BgL_res2594z00_4171;
{ /* Llib/hash.scm 785 */
 obj_t BgL_aux3431z00_6312;
BgL_aux3431z00_6312 = 
STRUCT_KEY(BgL_tz00_112); 
if(
SYMBOLP(BgL_aux3431z00_6312))
{ /* Llib/hash.scm 785 */
BgL_res2594z00_4171 = BgL_aux3431z00_6312; }  else 
{ 
 obj_t BgL_auxz00_10841;
BgL_auxz00_10841 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31664L), BGl_string4026z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3431z00_6312); 
FAILURE(BgL_auxz00_10841,BFALSE,BFALSE);} } 
BgL_tmpz00_10837 = BgL_res2594z00_4171; } 
BgL_test4882z00_10836 = 
(BgL_tmpz00_10837==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4882z00_10836)
{ /* Llib/hash.scm 785 */
 int BgL_tmpz00_10846;
BgL_tmpz00_10846 = 
(int)(1L); 
BgL_siza7eza7_2117 = 
STRUCT_REF(BgL_tz00_112, BgL_tmpz00_10846); }  else 
{ /* Llib/hash.scm 785 */
BgL_siza7eza7_2117 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_112); } } 
{ /* Llib/hash.scm 786 */
 bool_t BgL_test4884z00_10850;
{ /* Llib/hash.scm 786 */
 obj_t BgL_tmpz00_10851;
{ /* Llib/hash.scm 786 */
 obj_t BgL_res2595z00_4175;
{ /* Llib/hash.scm 786 */
 obj_t BgL_aux3433z00_6314;
BgL_aux3433z00_6314 = 
STRUCT_KEY(BgL_tz00_112); 
if(
SYMBOLP(BgL_aux3433z00_6314))
{ /* Llib/hash.scm 786 */
BgL_res2595z00_4175 = BgL_aux3433z00_6314; }  else 
{ 
 obj_t BgL_auxz00_10855;
BgL_auxz00_10855 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31706L), BGl_string4026z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3433z00_6314); 
FAILURE(BgL_auxz00_10855,BFALSE,BFALSE);} } 
BgL_tmpz00_10851 = BgL_res2595z00_4175; } 
BgL_test4884z00_10850 = 
(BgL_tmpz00_10851==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4884z00_10850)
{ /* Llib/hash.scm 786 */
 int BgL_tmpz00_10860;
BgL_tmpz00_10860 = 
(int)(2L); 
BgL_bucketsz00_2118 = 
STRUCT_REF(BgL_tz00_112, BgL_tmpz00_10860); }  else 
{ /* Llib/hash.scm 786 */
BgL_bucketsz00_2118 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_112); } } 
{ /* Llib/hash.scm 787 */
 long BgL_g1071z00_2119;
{ /* Llib/hash.scm 787 */
 long BgL_n1z00_4176; long BgL_n2z00_4177;
{ /* Llib/hash.scm 787 */
 obj_t BgL_tmpz00_10864;
if(
INTEGERP(BgL_hashz00_115))
{ /* Llib/hash.scm 787 */
BgL_tmpz00_10864 = BgL_hashz00_115
; }  else 
{ 
 obj_t BgL_auxz00_10867;
BgL_auxz00_10867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31766L), BGl_string4026z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_hashz00_115); 
FAILURE(BgL_auxz00_10867,BFALSE,BFALSE);} 
BgL_n1z00_4176 = 
(long)CINT(BgL_tmpz00_10864); } 
{ /* Llib/hash.scm 787 */
 obj_t BgL_tmpz00_10872;
if(
INTEGERP(BgL_siza7eza7_2117))
{ /* Llib/hash.scm 787 */
BgL_tmpz00_10872 = BgL_siza7eza7_2117
; }  else 
{ 
 obj_t BgL_auxz00_10875;
BgL_auxz00_10875 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31771L), BGl_string4026z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2117); 
FAILURE(BgL_auxz00_10875,BFALSE,BFALSE);} 
BgL_n2z00_4177 = 
(long)CINT(BgL_tmpz00_10872); } 
{ /* Llib/hash.scm 787 */
 bool_t BgL_test4890z00_10880;
{ /* Llib/hash.scm 787 */
 long BgL_arg2221z00_4179;
BgL_arg2221z00_4179 = 
(((BgL_n1z00_4176) | (BgL_n2z00_4177)) & -2147483648); 
BgL_test4890z00_10880 = 
(BgL_arg2221z00_4179==0L); } 
if(BgL_test4890z00_10880)
{ /* Llib/hash.scm 787 */
 int32_t BgL_arg2218z00_4180;
{ /* Llib/hash.scm 787 */
 int32_t BgL_arg2219z00_4181; int32_t BgL_arg2220z00_4182;
BgL_arg2219z00_4181 = 
(int32_t)(BgL_n1z00_4176); 
BgL_arg2220z00_4182 = 
(int32_t)(BgL_n2z00_4177); 
BgL_arg2218z00_4180 = 
(BgL_arg2219z00_4181%BgL_arg2220z00_4182); } 
{ /* Llib/hash.scm 787 */
 long BgL_arg2326z00_4187;
BgL_arg2326z00_4187 = 
(long)(BgL_arg2218z00_4180); 
BgL_g1071z00_2119 = 
(long)(BgL_arg2326z00_4187); } }  else 
{ /* Llib/hash.scm 787 */
BgL_g1071z00_2119 = 
(BgL_n1z00_4176%BgL_n2z00_4177); } } } 
{ 
 long BgL_offz00_2121; long BgL_iz00_2122;
BgL_offz00_2121 = BgL_g1071z00_2119; 
BgL_iz00_2122 = 1L; 
BgL_zc3z04anonymousza31728ze3z87_2123:
{ /* Llib/hash.scm 789 */
 long BgL_off3z00_2124;
BgL_off3z00_2124 = 
(BgL_offz00_2121*3L); 
{ /* Llib/hash.scm 791 */
 bool_t BgL_test4892z00_10890;
{ /* Llib/hash.scm 791 */
 obj_t BgL_vectorz00_4190;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 791 */
BgL_vectorz00_4190 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_10893;
BgL_auxz00_10893 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31853L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_10893,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 791 */
 bool_t BgL_test4894z00_10897;
{ /* Llib/hash.scm 791 */
 long BgL_tmpz00_10898;
BgL_tmpz00_10898 = 
VECTOR_LENGTH(BgL_vectorz00_4190); 
BgL_test4894z00_10897 = 
BOUND_CHECK(BgL_off3z00_2124, BgL_tmpz00_10898); } 
if(BgL_test4894z00_10897)
{ /* Llib/hash.scm 791 */
BgL_test4892z00_10890 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4190,BgL_off3z00_2124))
; }  else 
{ 
 obj_t BgL_auxz00_10903;
BgL_auxz00_10903 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31841L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4190, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4190)), 
(int)(BgL_off3z00_2124)); 
FAILURE(BgL_auxz00_10903,BFALSE,BFALSE);} } } 
if(BgL_test4892z00_10890)
{ /* Llib/hash.scm 797 */
 bool_t BgL_test4895z00_10910;
{ /* Llib/hash.scm 797 */
 obj_t BgL_arg1747z00_2143;
{ /* Llib/hash.scm 797 */
 obj_t BgL_vectorz00_4192;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 797 */
BgL_vectorz00_4192 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_10913;
BgL_auxz00_10913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32072L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_10913,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 797 */
 bool_t BgL_test4897z00_10917;
{ /* Llib/hash.scm 797 */
 long BgL_tmpz00_10918;
BgL_tmpz00_10918 = 
VECTOR_LENGTH(BgL_vectorz00_4192); 
BgL_test4897z00_10917 = 
BOUND_CHECK(BgL_off3z00_2124, BgL_tmpz00_10918); } 
if(BgL_test4897z00_10917)
{ /* Llib/hash.scm 797 */
BgL_arg1747z00_2143 = 
VECTOR_REF(BgL_vectorz00_4192,BgL_off3z00_2124); }  else 
{ 
 obj_t BgL_auxz00_10922;
BgL_auxz00_10922 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32060L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4192, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4192)), 
(int)(BgL_off3z00_2124)); 
FAILURE(BgL_auxz00_10922,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 797 */
 obj_t BgL_string1z00_4194; obj_t BgL_string2z00_4195;
if(
STRINGP(BgL_arg1747z00_2143))
{ /* Llib/hash.scm 797 */
BgL_string1z00_4194 = BgL_arg1747z00_2143; }  else 
{ 
 obj_t BgL_auxz00_10931;
BgL_auxz00_10931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32084L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1747z00_2143); 
FAILURE(BgL_auxz00_10931,BFALSE,BFALSE);} 
if(
STRINGP(BgL_keyz00_113))
{ /* Llib/hash.scm 797 */
BgL_string2z00_4195 = BgL_keyz00_113; }  else 
{ 
 obj_t BgL_auxz00_10937;
BgL_auxz00_10937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32086L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_113); 
FAILURE(BgL_auxz00_10937,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 797 */
 long BgL_l1z00_4196;
BgL_l1z00_4196 = 
STRING_LENGTH(BgL_string1z00_4194); 
if(
(BgL_l1z00_4196==
STRING_LENGTH(BgL_string2z00_4195)))
{ /* Llib/hash.scm 797 */
 int BgL_arg2114z00_4199;
{ /* Llib/hash.scm 797 */
 char * BgL_auxz00_10947; char * BgL_tmpz00_10945;
BgL_auxz00_10947 = 
BSTRING_TO_STRING(BgL_string2z00_4195); 
BgL_tmpz00_10945 = 
BSTRING_TO_STRING(BgL_string1z00_4194); 
BgL_arg2114z00_4199 = 
memcmp(BgL_tmpz00_10945, BgL_auxz00_10947, BgL_l1z00_4196); } 
BgL_test4895z00_10910 = 
(
(long)(BgL_arg2114z00_4199)==0L); }  else 
{ /* Llib/hash.scm 797 */
BgL_test4895z00_10910 = ((bool_t)0)
; } } } } 
if(BgL_test4895z00_10910)
{ /* Llib/hash.scm 797 */
{ /* Llib/hash.scm 799 */
 long BgL_arg1733z00_2128;
BgL_arg1733z00_2128 = 
(BgL_off3z00_2124+1L); 
{ /* Llib/hash.scm 799 */
 obj_t BgL_vectorz00_4206;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 799 */
BgL_vectorz00_4206 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_10955;
BgL_auxz00_10955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32119L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_10955,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 799 */
 bool_t BgL_test4902z00_10959;
{ /* Llib/hash.scm 799 */
 long BgL_tmpz00_10960;
BgL_tmpz00_10960 = 
VECTOR_LENGTH(BgL_vectorz00_4206); 
BgL_test4902z00_10959 = 
BOUND_CHECK(BgL_arg1733z00_2128, BgL_tmpz00_10960); } 
if(BgL_test4902z00_10959)
{ /* Llib/hash.scm 799 */
VECTOR_SET(BgL_vectorz00_4206,BgL_arg1733z00_2128,BgL_valz00_114); }  else 
{ 
 obj_t BgL_auxz00_10964;
BgL_auxz00_10964 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32106L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4206, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4206)), 
(int)(BgL_arg1733z00_2128)); 
FAILURE(BgL_auxz00_10964,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 800 */
 long BgL_arg1734z00_2129;
BgL_arg1734z00_2129 = 
(BgL_off3z00_2124+2L); 
{ /* Llib/hash.scm 800 */
 obj_t BgL_vectorz00_4209;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 800 */
BgL_vectorz00_4209 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_10974;
BgL_auxz00_10974 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32160L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_10974,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 800 */
 bool_t BgL_test4904z00_10978;
{ /* Llib/hash.scm 800 */
 long BgL_tmpz00_10979;
BgL_tmpz00_10979 = 
VECTOR_LENGTH(BgL_vectorz00_4209); 
BgL_test4904z00_10978 = 
BOUND_CHECK(BgL_arg1734z00_2129, BgL_tmpz00_10979); } 
if(BgL_test4904z00_10978)
{ /* Llib/hash.scm 800 */
return 
VECTOR_SET(BgL_vectorz00_4209,BgL_arg1734z00_2129,BgL_hashz00_115);}  else 
{ 
 obj_t BgL_auxz00_10983;
BgL_auxz00_10983 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32147L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4209, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4209)), 
(int)(BgL_arg1734z00_2129)); 
FAILURE(BgL_auxz00_10983,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/hash.scm 801 */
 bool_t BgL_test4905z00_10990;
if(
(BgL_iz00_2122>=5L))
{ /* Llib/hash.scm 802 */
 obj_t BgL_arg1745z00_2141;
{ /* Llib/hash.scm 802 */
 bool_t BgL_test4907z00_10993;
{ /* Llib/hash.scm 802 */
 obj_t BgL_tmpz00_10994;
{ /* Llib/hash.scm 802 */
 obj_t BgL_res2596z00_4215;
{ /* Llib/hash.scm 802 */
 obj_t BgL_aux3449z00_6330;
BgL_aux3449z00_6330 = 
STRUCT_KEY(BgL_tz00_112); 
if(
SYMBOLP(BgL_aux3449z00_6330))
{ /* Llib/hash.scm 802 */
BgL_res2596z00_4215 = BgL_aux3449z00_6330; }  else 
{ 
 obj_t BgL_auxz00_10998;
BgL_auxz00_10998 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32225L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3449z00_6330); 
FAILURE(BgL_auxz00_10998,BFALSE,BFALSE);} } 
BgL_tmpz00_10994 = BgL_res2596z00_4215; } 
BgL_test4907z00_10993 = 
(BgL_tmpz00_10994==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4907z00_10993)
{ /* Llib/hash.scm 802 */
 int BgL_tmpz00_11003;
BgL_tmpz00_11003 = 
(int)(1L); 
BgL_arg1745z00_2141 = 
STRUCT_REF(BgL_tz00_112, BgL_tmpz00_11003); }  else 
{ /* Llib/hash.scm 802 */
BgL_arg1745z00_2141 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_112); } } 
{ /* Llib/hash.scm 802 */
 long BgL_n1z00_4216;
{ /* Llib/hash.scm 802 */
 obj_t BgL_tmpz00_11007;
if(
INTEGERP(BgL_arg1745z00_2141))
{ /* Llib/hash.scm 802 */
BgL_tmpz00_11007 = BgL_arg1745z00_2141
; }  else 
{ 
 obj_t BgL_auxz00_11010;
BgL_auxz00_11010 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32253L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1745z00_2141); 
FAILURE(BgL_auxz00_11010,BFALSE,BFALSE);} 
BgL_n1z00_4216 = 
(long)CINT(BgL_tmpz00_11007); } 
BgL_test4905z00_10990 = 
(BgL_n1z00_4216<8388608L); } }  else 
{ /* Llib/hash.scm 801 */
BgL_test4905z00_10990 = ((bool_t)0)
; } 
if(BgL_test4905z00_10990)
{ /* Llib/hash.scm 801 */
BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00(BgL_tz00_112); 
{ 

goto BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00;} }  else 
{ /* Llib/hash.scm 809 */
 long BgL_noffz00_2134;
BgL_noffz00_2134 = 
(BgL_offz00_2121+
(BgL_iz00_2122*BgL_iz00_2122)); 
{ /* Llib/hash.scm 810 */
 bool_t BgL_test4910z00_11019;
{ /* Llib/hash.scm 810 */
 long BgL_n2z00_4223;
{ /* Llib/hash.scm 810 */
 obj_t BgL_tmpz00_11020;
if(
INTEGERP(BgL_siza7eza7_2117))
{ /* Llib/hash.scm 810 */
BgL_tmpz00_11020 = BgL_siza7eza7_2117
; }  else 
{ 
 obj_t BgL_auxz00_11023;
BgL_auxz00_11023 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32485L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2117); 
FAILURE(BgL_auxz00_11023,BFALSE,BFALSE);} 
BgL_n2z00_4223 = 
(long)CINT(BgL_tmpz00_11020); } 
BgL_test4910z00_11019 = 
(BgL_noffz00_2134>=BgL_n2z00_4223); } 
if(BgL_test4910z00_11019)
{ /* Llib/hash.scm 811 */
 long BgL_arg1740z00_2136; long BgL_arg1741z00_2137;
{ /* Llib/hash.scm 811 */
 long BgL_n1z00_4224; long BgL_n2z00_4225;
BgL_n1z00_4224 = BgL_noffz00_2134; 
{ /* Llib/hash.scm 811 */
 obj_t BgL_tmpz00_11029;
if(
INTEGERP(BgL_siza7eza7_2117))
{ /* Llib/hash.scm 811 */
BgL_tmpz00_11029 = BgL_siza7eza7_2117
; }  else 
{ 
 obj_t BgL_auxz00_11032;
BgL_auxz00_11032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32524L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2117); 
FAILURE(BgL_auxz00_11032,BFALSE,BFALSE);} 
BgL_n2z00_4225 = 
(long)CINT(BgL_tmpz00_11029); } 
{ /* Llib/hash.scm 811 */
 bool_t BgL_test4913z00_11037;
{ /* Llib/hash.scm 811 */
 long BgL_arg2221z00_4227;
BgL_arg2221z00_4227 = 
(((BgL_n1z00_4224) | (BgL_n2z00_4225)) & -2147483648); 
BgL_test4913z00_11037 = 
(BgL_arg2221z00_4227==0L); } 
if(BgL_test4913z00_11037)
{ /* Llib/hash.scm 811 */
 int32_t BgL_arg2218z00_4228;
{ /* Llib/hash.scm 811 */
 int32_t BgL_arg2219z00_4229; int32_t BgL_arg2220z00_4230;
BgL_arg2219z00_4229 = 
(int32_t)(BgL_n1z00_4224); 
BgL_arg2220z00_4230 = 
(int32_t)(BgL_n2z00_4225); 
BgL_arg2218z00_4228 = 
(BgL_arg2219z00_4229%BgL_arg2220z00_4230); } 
{ /* Llib/hash.scm 811 */
 long BgL_arg2326z00_4235;
BgL_arg2326z00_4235 = 
(long)(BgL_arg2218z00_4228); 
BgL_arg1740z00_2136 = 
(long)(BgL_arg2326z00_4235); } }  else 
{ /* Llib/hash.scm 811 */
BgL_arg1740z00_2136 = 
(BgL_n1z00_4224%BgL_n2z00_4225); } } } 
BgL_arg1741z00_2137 = 
(BgL_iz00_2122+1L); 
{ 
 long BgL_iz00_11048; long BgL_offz00_11047;
BgL_offz00_11047 = BgL_arg1740z00_2136; 
BgL_iz00_11048 = BgL_arg1741z00_2137; 
BgL_iz00_2122 = BgL_iz00_11048; 
BgL_offz00_2121 = BgL_offz00_11047; 
goto BgL_zc3z04anonymousza31728ze3z87_2123;} }  else 
{ 
 long BgL_iz00_11050; long BgL_offz00_11049;
BgL_offz00_11049 = BgL_noffz00_2134; 
BgL_iz00_11050 = 
(BgL_iz00_2122+1L); 
BgL_iz00_2122 = BgL_iz00_11050; 
BgL_offz00_2121 = BgL_offz00_11049; 
goto BgL_zc3z04anonymousza31728ze3z87_2123;} } } } }  else 
{ /* Llib/hash.scm 791 */
{ /* Llib/hash.scm 793 */
 obj_t BgL_vectorz00_4239;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 793 */
BgL_vectorz00_4239 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_11054;
BgL_auxz00_11054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31901L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_11054,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 793 */
 bool_t BgL_test4915z00_11058;
{ /* Llib/hash.scm 793 */
 long BgL_tmpz00_11059;
BgL_tmpz00_11059 = 
VECTOR_LENGTH(BgL_vectorz00_4239); 
BgL_test4915z00_11058 = 
BOUND_CHECK(BgL_off3z00_2124, BgL_tmpz00_11059); } 
if(BgL_test4915z00_11058)
{ /* Llib/hash.scm 793 */
VECTOR_SET(BgL_vectorz00_4239,BgL_off3z00_2124,BgL_keyz00_113); }  else 
{ 
 obj_t BgL_auxz00_11063;
BgL_auxz00_11063 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31888L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4239, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4239)), 
(int)(BgL_off3z00_2124)); 
FAILURE(BgL_auxz00_11063,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 794 */
 long BgL_arg1748z00_2144;
BgL_arg1748z00_2144 = 
(BgL_off3z00_2124+1L); 
{ /* Llib/hash.scm 794 */
 obj_t BgL_vectorz00_4242;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 794 */
BgL_vectorz00_4242 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_11073;
BgL_auxz00_11073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31934L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_11073,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 794 */
 bool_t BgL_test4917z00_11077;
{ /* Llib/hash.scm 794 */
 long BgL_tmpz00_11078;
BgL_tmpz00_11078 = 
VECTOR_LENGTH(BgL_vectorz00_4242); 
BgL_test4917z00_11077 = 
BOUND_CHECK(BgL_arg1748z00_2144, BgL_tmpz00_11078); } 
if(BgL_test4917z00_11077)
{ /* Llib/hash.scm 794 */
VECTOR_SET(BgL_vectorz00_4242,BgL_arg1748z00_2144,BgL_valz00_114); }  else 
{ 
 obj_t BgL_auxz00_11082;
BgL_auxz00_11082 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31921L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4242, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4242)), 
(int)(BgL_arg1748z00_2144)); 
FAILURE(BgL_auxz00_11082,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 795 */
 long BgL_arg1749z00_2145;
BgL_arg1749z00_2145 = 
(BgL_off3z00_2124+2L); 
{ /* Llib/hash.scm 795 */
 obj_t BgL_vectorz00_4245;
if(
VECTORP(BgL_bucketsz00_2118))
{ /* Llib/hash.scm 795 */
BgL_vectorz00_4245 = BgL_bucketsz00_2118; }  else 
{ 
 obj_t BgL_auxz00_11092;
BgL_auxz00_11092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31975L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2118); 
FAILURE(BgL_auxz00_11092,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 795 */
 bool_t BgL_test4919z00_11096;
{ /* Llib/hash.scm 795 */
 long BgL_tmpz00_11097;
BgL_tmpz00_11097 = 
VECTOR_LENGTH(BgL_vectorz00_4245); 
BgL_test4919z00_11096 = 
BOUND_CHECK(BgL_arg1749z00_2145, BgL_tmpz00_11097); } 
if(BgL_test4919z00_11096)
{ /* Llib/hash.scm 795 */
VECTOR_SET(BgL_vectorz00_4245,BgL_arg1749z00_2145,BgL_hashz00_115); }  else 
{ 
 obj_t BgL_auxz00_11101;
BgL_auxz00_11101 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(31962L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4245, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4245)), 
(int)(BgL_arg1749z00_2145)); 
FAILURE(BgL_auxz00_11101,BFALSE,BFALSE);} } } } 
return 
BGl_openzd2stringzd2hashtablezd2siza7ezd2incz12zb5zz__hashz00(BgL_tz00_112);} } } } } } } 

}



/* string-hashtable-put! */
BGL_EXPORTED_DEF obj_t BGl_stringzd2hashtablezd2putz12z12zz__hashz00(obj_t BgL_tablez00_116, obj_t BgL_keyz00_117, obj_t BgL_objz00_118)
{
{ /* Llib/hash.scm 817 */
{ /* Llib/hash.scm 818 */
 obj_t BgL_bucketsz00_2147;
{ /* Llib/hash.scm 818 */
 bool_t BgL_test4920z00_11109;
{ /* Llib/hash.scm 818 */
 obj_t BgL_tmpz00_11110;
{ /* Llib/hash.scm 818 */
 obj_t BgL_res2597z00_4250;
{ /* Llib/hash.scm 818 */
 obj_t BgL_aux3460z00_6341;
BgL_aux3460z00_6341 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3460z00_6341))
{ /* Llib/hash.scm 818 */
BgL_res2597z00_4250 = BgL_aux3460z00_6341; }  else 
{ 
 obj_t BgL_auxz00_11114;
BgL_auxz00_11114 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32878L), BGl_string4027z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3460z00_6341); 
FAILURE(BgL_auxz00_11114,BFALSE,BFALSE);} } 
BgL_tmpz00_11110 = BgL_res2597z00_4250; } 
BgL_test4920z00_11109 = 
(BgL_tmpz00_11110==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4920z00_11109)
{ /* Llib/hash.scm 818 */
 int BgL_tmpz00_11119;
BgL_tmpz00_11119 = 
(int)(2L); 
BgL_bucketsz00_2147 = 
STRUCT_REF(BgL_tablez00_116, BgL_tmpz00_11119); }  else 
{ /* Llib/hash.scm 818 */
BgL_bucketsz00_2147 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } 
{ /* Llib/hash.scm 818 */
 long BgL_bucketzd2lenzd2_2148;
{ /* Llib/hash.scm 819 */
 obj_t BgL_vectorz00_4251;
if(
VECTORP(BgL_bucketsz00_2147))
{ /* Llib/hash.scm 819 */
BgL_vectorz00_4251 = BgL_bucketsz00_2147; }  else 
{ 
 obj_t BgL_auxz00_11125;
BgL_auxz00_11125 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32936L), BGl_string4027z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2147); 
FAILURE(BgL_auxz00_11125,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2148 = 
VECTOR_LENGTH(BgL_vectorz00_4251); } 
{ /* Llib/hash.scm 819 */
 long BgL_bucketzd2numzd2_2149;
{ /* Llib/hash.scm 820 */
 long BgL_arg1772z00_2181;
{ /* Llib/hash.scm 820 */
 long BgL_arg1773z00_2182;
BgL_arg1773z00_2182 = 
STRING_LENGTH(BgL_keyz00_117); 
BgL_arg1772z00_2181 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_117), 
(int)(0L), 
(int)(BgL_arg1773z00_2182)); } 
{ /* Llib/hash.scm 820 */
 long BgL_n1z00_4253; long BgL_n2z00_4254;
BgL_n1z00_4253 = BgL_arg1772z00_2181; 
BgL_n2z00_4254 = BgL_bucketzd2lenzd2_2148; 
{ /* Llib/hash.scm 820 */
 bool_t BgL_test4923z00_11135;
{ /* Llib/hash.scm 820 */
 long BgL_arg2221z00_4256;
BgL_arg2221z00_4256 = 
(((BgL_n1z00_4253) | (BgL_n2z00_4254)) & -2147483648); 
BgL_test4923z00_11135 = 
(BgL_arg2221z00_4256==0L); } 
if(BgL_test4923z00_11135)
{ /* Llib/hash.scm 820 */
 int32_t BgL_arg2218z00_4257;
{ /* Llib/hash.scm 820 */
 int32_t BgL_arg2219z00_4258; int32_t BgL_arg2220z00_4259;
BgL_arg2219z00_4258 = 
(int32_t)(BgL_n1z00_4253); 
BgL_arg2220z00_4259 = 
(int32_t)(BgL_n2z00_4254); 
BgL_arg2218z00_4257 = 
(BgL_arg2219z00_4258%BgL_arg2220z00_4259); } 
{ /* Llib/hash.scm 820 */
 long BgL_arg2326z00_4264;
BgL_arg2326z00_4264 = 
(long)(BgL_arg2218z00_4257); 
BgL_bucketzd2numzd2_2149 = 
(long)(BgL_arg2326z00_4264); } }  else 
{ /* Llib/hash.scm 820 */
BgL_bucketzd2numzd2_2149 = 
(BgL_n1z00_4253%BgL_n2z00_4254); } } } } 
{ /* Llib/hash.scm 820 */
 obj_t BgL_bucketz00_2150;
{ /* Llib/hash.scm 821 */
 obj_t BgL_vectorz00_4266;
if(
VECTORP(BgL_bucketsz00_2147))
{ /* Llib/hash.scm 821 */
BgL_vectorz00_4266 = BgL_bucketsz00_2147; }  else 
{ 
 obj_t BgL_auxz00_11146;
BgL_auxz00_11146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33054L), BGl_string4027z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2147); 
FAILURE(BgL_auxz00_11146,BFALSE,BFALSE);} 
BgL_bucketz00_2150 = 
VECTOR_REF(BgL_vectorz00_4266,BgL_bucketzd2numzd2_2149); } 
{ /* Llib/hash.scm 821 */
 obj_t BgL_maxzd2bucketzd2lenz00_2151;
{ /* Llib/hash.scm 822 */
 bool_t BgL_test4925z00_11151;
{ /* Llib/hash.scm 822 */
 obj_t BgL_tmpz00_11152;
{ /* Llib/hash.scm 822 */
 obj_t BgL_res2598z00_4271;
{ /* Llib/hash.scm 822 */
 obj_t BgL_aux3466z00_6347;
BgL_aux3466z00_6347 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3466z00_6347))
{ /* Llib/hash.scm 822 */
BgL_res2598z00_4271 = BgL_aux3466z00_6347; }  else 
{ 
 obj_t BgL_auxz00_11156;
BgL_auxz00_11156 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33094L), BGl_string4027z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3466z00_6347); 
FAILURE(BgL_auxz00_11156,BFALSE,BFALSE);} } 
BgL_tmpz00_11152 = BgL_res2598z00_4271; } 
BgL_test4925z00_11151 = 
(BgL_tmpz00_11152==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4925z00_11151)
{ /* Llib/hash.scm 822 */
 int BgL_tmpz00_11161;
BgL_tmpz00_11161 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_2151 = 
STRUCT_REF(BgL_tablez00_116, BgL_tmpz00_11161); }  else 
{ /* Llib/hash.scm 822 */
BgL_maxzd2bucketzd2lenz00_2151 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } 
{ /* Llib/hash.scm 822 */

{ /* Llib/hash.scm 827 */
 bool_t BgL_test4927z00_11165;
{ /* Llib/hash.scm 827 */
 bool_t BgL_test4928z00_11166;
{ /* Llib/hash.scm 300 */
 obj_t BgL_arg1441z00_4273;
{ /* Llib/hash.scm 300 */
 bool_t BgL_test4929z00_11167;
{ /* Llib/hash.scm 300 */
 obj_t BgL_tmpz00_11168;
{ /* Llib/hash.scm 300 */
 obj_t BgL_res2599z00_4277;
{ /* Llib/hash.scm 300 */
 obj_t BgL_aux3468z00_6349;
BgL_aux3468z00_6349 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3468z00_6349))
{ /* Llib/hash.scm 300 */
BgL_res2599z00_4277 = BgL_aux3468z00_6349; }  else 
{ 
 obj_t BgL_auxz00_11172;
BgL_auxz00_11172 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11344L), BGl_string4027z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3468z00_6349); 
FAILURE(BgL_auxz00_11172,BFALSE,BFALSE);} } 
BgL_tmpz00_11168 = BgL_res2599z00_4277; } 
BgL_test4929z00_11167 = 
(BgL_tmpz00_11168==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4929z00_11167)
{ /* Llib/hash.scm 300 */
 int BgL_tmpz00_11177;
BgL_tmpz00_11177 = 
(int)(5L); 
BgL_arg1441z00_4273 = 
STRUCT_REF(BgL_tablez00_116, BgL_tmpz00_11177); }  else 
{ /* Llib/hash.scm 300 */
BgL_arg1441z00_4273 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } 
{ /* Llib/hash.scm 300 */
 long BgL_n1z00_4278;
{ /* Llib/hash.scm 300 */
 obj_t BgL_tmpz00_11181;
if(
INTEGERP(BgL_arg1441z00_4273))
{ /* Llib/hash.scm 300 */
BgL_tmpz00_11181 = BgL_arg1441z00_4273
; }  else 
{ 
 obj_t BgL_auxz00_11184;
BgL_auxz00_11184 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(11366L), BGl_string4027z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1441z00_4273); 
FAILURE(BgL_auxz00_11184,BFALSE,BFALSE);} 
BgL_n1z00_4278 = 
(long)CINT(BgL_tmpz00_11181); } 
BgL_test4928z00_11166 = 
(BgL_n1z00_4278==4L); } } 
if(BgL_test4928z00_11166)
{ /* Llib/hash.scm 827 */
BgL_test4927z00_11165 = ((bool_t)0)
; }  else 
{ /* Llib/hash.scm 827 */
BgL_test4927z00_11165 = ((bool_t)0)
; } } 
if(BgL_test4927z00_11165)
{ /* Llib/hash.scm 827 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string4023z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_117); }  else 
{ /* Llib/hash.scm 827 */BFALSE; } } 
if(
NULLP(BgL_bucketz00_2150))
{ /* Llib/hash.scm 829 */
{ /* Llib/hash.scm 831 */
 long BgL_arg1754z00_2158;
{ /* Llib/hash.scm 831 */
 obj_t BgL_arg1755z00_2159;
{ /* Llib/hash.scm 831 */
 bool_t BgL_test4933z00_11193;
{ /* Llib/hash.scm 831 */
 obj_t BgL_tmpz00_11194;
{ /* Llib/hash.scm 831 */
 obj_t BgL_res2600z00_4282;
{ /* Llib/hash.scm 831 */
 obj_t BgL_aux3471z00_6352;
BgL_aux3471z00_6352 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3471z00_6352))
{ /* Llib/hash.scm 831 */
BgL_res2600z00_4282 = BgL_aux3471z00_6352; }  else 
{ 
 obj_t BgL_auxz00_11198;
BgL_auxz00_11198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33381L), BGl_string4027z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3471z00_6352); 
FAILURE(BgL_auxz00_11198,BFALSE,BFALSE);} } 
BgL_tmpz00_11194 = BgL_res2600z00_4282; } 
BgL_test4933z00_11193 = 
(BgL_tmpz00_11194==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4933z00_11193)
{ /* Llib/hash.scm 831 */
 int BgL_tmpz00_11203;
BgL_tmpz00_11203 = 
(int)(0L); 
BgL_arg1755z00_2159 = 
STRUCT_REF(BgL_tablez00_116, BgL_tmpz00_11203); }  else 
{ /* Llib/hash.scm 831 */
BgL_arg1755z00_2159 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } 
{ /* Llib/hash.scm 831 */
 long BgL_za71za7_4283;
{ /* Llib/hash.scm 831 */
 obj_t BgL_tmpz00_11207;
if(
INTEGERP(BgL_arg1755z00_2159))
{ /* Llib/hash.scm 831 */
BgL_tmpz00_11207 = BgL_arg1755z00_2159
; }  else 
{ 
 obj_t BgL_auxz00_11210;
BgL_auxz00_11210 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33403L), BGl_string4027z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1755z00_2159); 
FAILURE(BgL_auxz00_11210,BFALSE,BFALSE);} 
BgL_za71za7_4283 = 
(long)CINT(BgL_tmpz00_11207); } 
BgL_arg1754z00_2158 = 
(BgL_za71za7_4283+1L); } } 
{ /* Llib/hash.scm 831 */
 bool_t BgL_test4936z00_11216;
{ /* Llib/hash.scm 831 */
 obj_t BgL_tmpz00_11217;
{ /* Llib/hash.scm 831 */
 obj_t BgL_res2601z00_4287;
{ /* Llib/hash.scm 831 */
 obj_t BgL_aux3474z00_6355;
BgL_aux3474z00_6355 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3474z00_6355))
{ /* Llib/hash.scm 831 */
BgL_res2601z00_4287 = BgL_aux3474z00_6355; }  else 
{ 
 obj_t BgL_auxz00_11221;
BgL_auxz00_11221 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33348L), BGl_string4027z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3474z00_6355); 
FAILURE(BgL_auxz00_11221,BFALSE,BFALSE);} } 
BgL_tmpz00_11217 = BgL_res2601z00_4287; } 
BgL_test4936z00_11216 = 
(BgL_tmpz00_11217==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4936z00_11216)
{ /* Llib/hash.scm 831 */
 obj_t BgL_auxz00_11228; int BgL_tmpz00_11226;
BgL_auxz00_11228 = 
BINT(BgL_arg1754z00_2158); 
BgL_tmpz00_11226 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_116, BgL_tmpz00_11226, BgL_auxz00_11228); }  else 
{ /* Llib/hash.scm 831 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } } 
{ /* Llib/hash.scm 832 */
 obj_t BgL_arg1756z00_2160;
{ /* Llib/hash.scm 832 */
 obj_t BgL_arg1757z00_2161;
BgL_arg1757z00_2161 = 
MAKE_YOUNG_PAIR(BgL_keyz00_117, BgL_objz00_118); 
{ /* Llib/hash.scm 832 */
 obj_t BgL_list1758z00_2162;
BgL_list1758z00_2162 = 
MAKE_YOUNG_PAIR(BgL_arg1757z00_2161, BNIL); 
BgL_arg1756z00_2160 = BgL_list1758z00_2162; } } 
{ /* Llib/hash.scm 832 */
 obj_t BgL_vectorz00_4289;
if(
VECTORP(BgL_bucketsz00_2147))
{ /* Llib/hash.scm 832 */
BgL_vectorz00_4289 = BgL_bucketsz00_2147; }  else 
{ 
 obj_t BgL_auxz00_11236;
BgL_auxz00_11236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33431L), BGl_string4027z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2147); 
FAILURE(BgL_auxz00_11236,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4289,BgL_bucketzd2numzd2_2149,BgL_arg1756z00_2160); } } 
return BgL_objz00_118;}  else 
{ 
 obj_t BgL_buckz00_2164; long BgL_countz00_2165;
BgL_buckz00_2164 = BgL_bucketz00_2150; 
BgL_countz00_2165 = 0L; 
BgL_zc3z04anonymousza31759ze3z87_2166:
if(
NULLP(BgL_buckz00_2164))
{ /* Llib/hash.scm 837 */
{ /* Llib/hash.scm 838 */
 long BgL_arg1761z00_2168;
{ /* Llib/hash.scm 838 */
 obj_t BgL_arg1762z00_2169;
{ /* Llib/hash.scm 838 */
 bool_t BgL_test4940z00_11243;
{ /* Llib/hash.scm 838 */
 obj_t BgL_tmpz00_11244;
{ /* Llib/hash.scm 838 */
 obj_t BgL_res2603z00_4294;
{ /* Llib/hash.scm 838 */
 obj_t BgL_aux3478z00_6359;
BgL_aux3478z00_6359 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3478z00_6359))
{ /* Llib/hash.scm 838 */
BgL_res2603z00_4294 = BgL_aux3478z00_6359; }  else 
{ 
 obj_t BgL_auxz00_11248;
BgL_auxz00_11248 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33594L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3478z00_6359); 
FAILURE(BgL_auxz00_11248,BFALSE,BFALSE);} } 
BgL_tmpz00_11244 = BgL_res2603z00_4294; } 
BgL_test4940z00_11243 = 
(BgL_tmpz00_11244==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4940z00_11243)
{ /* Llib/hash.scm 838 */
 int BgL_tmpz00_11253;
BgL_tmpz00_11253 = 
(int)(0L); 
BgL_arg1762z00_2169 = 
STRUCT_REF(BgL_tablez00_116, BgL_tmpz00_11253); }  else 
{ /* Llib/hash.scm 838 */
BgL_arg1762z00_2169 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } 
{ /* Llib/hash.scm 838 */
 long BgL_za71za7_4295;
{ /* Llib/hash.scm 838 */
 obj_t BgL_tmpz00_11257;
if(
INTEGERP(BgL_arg1762z00_2169))
{ /* Llib/hash.scm 838 */
BgL_tmpz00_11257 = BgL_arg1762z00_2169
; }  else 
{ 
 obj_t BgL_auxz00_11260;
BgL_auxz00_11260 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33616L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1762z00_2169); 
FAILURE(BgL_auxz00_11260,BFALSE,BFALSE);} 
BgL_za71za7_4295 = 
(long)CINT(BgL_tmpz00_11257); } 
BgL_arg1761z00_2168 = 
(BgL_za71za7_4295+1L); } } 
{ /* Llib/hash.scm 838 */
 bool_t BgL_test4943z00_11266;
{ /* Llib/hash.scm 838 */
 obj_t BgL_tmpz00_11267;
{ /* Llib/hash.scm 838 */
 obj_t BgL_res2604z00_4299;
{ /* Llib/hash.scm 838 */
 obj_t BgL_aux3481z00_6362;
BgL_aux3481z00_6362 = 
STRUCT_KEY(BgL_tablez00_116); 
if(
SYMBOLP(BgL_aux3481z00_6362))
{ /* Llib/hash.scm 838 */
BgL_res2604z00_4299 = BgL_aux3481z00_6362; }  else 
{ 
 obj_t BgL_auxz00_11271;
BgL_auxz00_11271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33561L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3481z00_6362); 
FAILURE(BgL_auxz00_11271,BFALSE,BFALSE);} } 
BgL_tmpz00_11267 = BgL_res2604z00_4299; } 
BgL_test4943z00_11266 = 
(BgL_tmpz00_11267==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4943z00_11266)
{ /* Llib/hash.scm 838 */
 obj_t BgL_auxz00_11278; int BgL_tmpz00_11276;
BgL_auxz00_11278 = 
BINT(BgL_arg1761z00_2168); 
BgL_tmpz00_11276 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_116, BgL_tmpz00_11276, BgL_auxz00_11278); }  else 
{ /* Llib/hash.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_116); } } } 
{ /* Llib/hash.scm 839 */
 obj_t BgL_arg1763z00_2170;
{ /* Llib/hash.scm 839 */
 obj_t BgL_arg1764z00_2171;
BgL_arg1764z00_2171 = 
MAKE_YOUNG_PAIR(BgL_keyz00_117, BgL_objz00_118); 
BgL_arg1763z00_2170 = 
MAKE_YOUNG_PAIR(BgL_arg1764z00_2171, BgL_bucketz00_2150); } 
{ /* Llib/hash.scm 839 */
 obj_t BgL_vectorz00_4300;
if(
VECTORP(BgL_bucketsz00_2147))
{ /* Llib/hash.scm 839 */
BgL_vectorz00_4300 = BgL_bucketsz00_2147; }  else 
{ 
 obj_t BgL_auxz00_11286;
BgL_auxz00_11286 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33641L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2147); 
FAILURE(BgL_auxz00_11286,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4300,BgL_bucketzd2numzd2_2149,BgL_arg1763z00_2170); } } 
{ /* Llib/hash.scm 840 */
 bool_t BgL_test4946z00_11291;
{ /* Llib/hash.scm 840 */
 long BgL_n2z00_4303;
{ /* Llib/hash.scm 840 */
 obj_t BgL_tmpz00_11292;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_2151))
{ /* Llib/hash.scm 840 */
BgL_tmpz00_11292 = BgL_maxzd2bucketzd2lenz00_2151
; }  else 
{ 
 obj_t BgL_auxz00_11295;
BgL_auxz00_11295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33710L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_maxzd2bucketzd2lenz00_2151); 
FAILURE(BgL_auxz00_11295,BFALSE,BFALSE);} 
BgL_n2z00_4303 = 
(long)CINT(BgL_tmpz00_11292); } 
BgL_test4946z00_11291 = 
(BgL_countz00_2165>BgL_n2z00_4303); } 
if(BgL_test4946z00_11291)
{ /* Llib/hash.scm 840 */
BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(BgL_tablez00_116); }  else 
{ /* Llib/hash.scm 840 */BFALSE; } } 
return BgL_objz00_118;}  else 
{ /* Llib/hash.scm 843 */
 bool_t BgL_test4948z00_11302;
{ /* Llib/hash.scm 843 */
 obj_t BgL_arg1771z00_2179;
{ /* Llib/hash.scm 843 */
 obj_t BgL_pairz00_4304;
if(
PAIRP(BgL_buckz00_2164))
{ /* Llib/hash.scm 843 */
BgL_pairz00_4304 = BgL_buckz00_2164; }  else 
{ 
 obj_t BgL_auxz00_11305;
BgL_auxz00_11305 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33792L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2164); 
FAILURE(BgL_auxz00_11305,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 843 */
 obj_t BgL_pairz00_4307;
{ /* Llib/hash.scm 843 */
 obj_t BgL_aux3488z00_6369;
BgL_aux3488z00_6369 = 
CAR(BgL_pairz00_4304); 
if(
PAIRP(BgL_aux3488z00_6369))
{ /* Llib/hash.scm 843 */
BgL_pairz00_4307 = BgL_aux3488z00_6369; }  else 
{ 
 obj_t BgL_auxz00_11312;
BgL_auxz00_11312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33786L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3488z00_6369); 
FAILURE(BgL_auxz00_11312,BFALSE,BFALSE);} } 
BgL_arg1771z00_2179 = 
CAR(BgL_pairz00_4307); } } 
{ /* Llib/hash.scm 843 */
 obj_t BgL_string1z00_4308;
if(
STRINGP(BgL_arg1771z00_2179))
{ /* Llib/hash.scm 843 */
BgL_string1z00_4308 = BgL_arg1771z00_2179; }  else 
{ 
 obj_t BgL_auxz00_11319;
BgL_auxz00_11319 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33796L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1771z00_2179); 
FAILURE(BgL_auxz00_11319,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 843 */
 long BgL_l1z00_4310;
BgL_l1z00_4310 = 
STRING_LENGTH(BgL_string1z00_4308); 
if(
(BgL_l1z00_4310==
STRING_LENGTH(BgL_keyz00_117)))
{ /* Llib/hash.scm 843 */
 int BgL_arg2114z00_4313;
{ /* Llib/hash.scm 843 */
 char * BgL_auxz00_11329; char * BgL_tmpz00_11327;
BgL_auxz00_11329 = 
BSTRING_TO_STRING(BgL_keyz00_117); 
BgL_tmpz00_11327 = 
BSTRING_TO_STRING(BgL_string1z00_4308); 
BgL_arg2114z00_4313 = 
memcmp(BgL_tmpz00_11327, BgL_auxz00_11329, BgL_l1z00_4310); } 
BgL_test4948z00_11302 = 
(
(long)(BgL_arg2114z00_4313)==0L); }  else 
{ /* Llib/hash.scm 843 */
BgL_test4948z00_11302 = ((bool_t)0)
; } } } } 
if(BgL_test4948z00_11302)
{ /* Llib/hash.scm 844 */
 obj_t BgL_oldzd2objzd2_2175;
{ /* Llib/hash.scm 844 */
 obj_t BgL_pairz00_4319;
if(
PAIRP(BgL_buckz00_2164))
{ /* Llib/hash.scm 844 */
BgL_pairz00_4319 = BgL_buckz00_2164; }  else 
{ 
 obj_t BgL_auxz00_11336;
BgL_auxz00_11336 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33827L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2164); 
FAILURE(BgL_auxz00_11336,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 844 */
 obj_t BgL_pairz00_4322;
{ /* Llib/hash.scm 844 */
 obj_t BgL_aux3494z00_6375;
BgL_aux3494z00_6375 = 
CAR(BgL_pairz00_4319); 
if(
PAIRP(BgL_aux3494z00_6375))
{ /* Llib/hash.scm 844 */
BgL_pairz00_4322 = BgL_aux3494z00_6375; }  else 
{ 
 obj_t BgL_auxz00_11343;
BgL_auxz00_11343 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33821L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3494z00_6375); 
FAILURE(BgL_auxz00_11343,BFALSE,BFALSE);} } 
BgL_oldzd2objzd2_2175 = 
CDR(BgL_pairz00_4322); } } 
{ /* Llib/hash.scm 845 */
 obj_t BgL_arg1768z00_2176;
{ /* Llib/hash.scm 845 */
 obj_t BgL_pairz00_4323;
if(
PAIRP(BgL_buckz00_2164))
{ /* Llib/hash.scm 845 */
BgL_pairz00_4323 = BgL_buckz00_2164; }  else 
{ 
 obj_t BgL_auxz00_11350;
BgL_auxz00_11350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33856L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2164); 
FAILURE(BgL_auxz00_11350,BFALSE,BFALSE);} 
BgL_arg1768z00_2176 = 
CAR(BgL_pairz00_4323); } 
{ /* Llib/hash.scm 845 */
 obj_t BgL_pairz00_4324;
if(
PAIRP(BgL_arg1768z00_2176))
{ /* Llib/hash.scm 845 */
BgL_pairz00_4324 = BgL_arg1768z00_2176; }  else 
{ 
 obj_t BgL_auxz00_11357;
BgL_auxz00_11357 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33860L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_arg1768z00_2176); 
FAILURE(BgL_auxz00_11357,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_4324, BgL_objz00_118); } } 
return BgL_oldzd2objzd2_2175;}  else 
{ /* Llib/hash.scm 848 */
 obj_t BgL_arg1769z00_2177; long BgL_arg1770z00_2178;
{ /* Llib/hash.scm 848 */
 obj_t BgL_pairz00_4325;
if(
PAIRP(BgL_buckz00_2164))
{ /* Llib/hash.scm 848 */
BgL_pairz00_4325 = BgL_buckz00_2164; }  else 
{ 
 obj_t BgL_auxz00_11364;
BgL_auxz00_11364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(33905L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2164); 
FAILURE(BgL_auxz00_11364,BFALSE,BFALSE);} 
BgL_arg1769z00_2177 = 
CDR(BgL_pairz00_4325); } 
BgL_arg1770z00_2178 = 
(BgL_countz00_2165+1L); 
{ 
 long BgL_countz00_11371; obj_t BgL_buckz00_11370;
BgL_buckz00_11370 = BgL_arg1769z00_2177; 
BgL_countz00_11371 = BgL_arg1770z00_2178; 
BgL_countz00_2165 = BgL_countz00_11371; 
BgL_buckz00_2164 = BgL_buckz00_11370; 
goto BgL_zc3z04anonymousza31759ze3z87_2166;} } } } } } } } } } } 

}



/* &string-hashtable-put! */
obj_t BGl_z62stringzd2hashtablezd2putz12z70zz__hashz00(obj_t BgL_envz00_5488, obj_t BgL_tablez00_5489, obj_t BgL_keyz00_5490, obj_t BgL_objz00_5491)
{
{ /* Llib/hash.scm 817 */
{ /* Llib/hash.scm 818 */
 obj_t BgL_auxz00_11379; obj_t BgL_auxz00_11372;
if(
STRINGP(BgL_keyz00_5490))
{ /* Llib/hash.scm 818 */
BgL_auxz00_11379 = BgL_keyz00_5490
; }  else 
{ 
 obj_t BgL_auxz00_11382;
BgL_auxz00_11382 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32862L), BGl_string4028z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5490); 
FAILURE(BgL_auxz00_11382,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5489))
{ /* Llib/hash.scm 818 */
BgL_auxz00_11372 = BgL_tablez00_5489
; }  else 
{ 
 obj_t BgL_auxz00_11375;
BgL_auxz00_11375 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(32862L), BGl_string4028z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5489); 
FAILURE(BgL_auxz00_11375,BFALSE,BFALSE);} 
return 
BGl_stringzd2hashtablezd2putz12z12zz__hashz00(BgL_auxz00_11372, BgL_auxz00_11379, BgL_objz00_5491);} } 

}



/* plain-hashtable-put! */
obj_t BGl_plainzd2hashtablezd2putz12z12zz__hashz00(obj_t BgL_tablez00_119, obj_t BgL_keyz00_120, obj_t BgL_objz00_121)
{
{ /* Llib/hash.scm 853 */
{ /* Llib/hash.scm 854 */
 obj_t BgL_bucketsz00_2183;
{ /* Llib/hash.scm 854 */
 bool_t BgL_test4960z00_11387;
{ /* Llib/hash.scm 854 */
 obj_t BgL_tmpz00_11388;
{ /* Llib/hash.scm 854 */
 obj_t BgL_res2605z00_4330;
{ /* Llib/hash.scm 854 */
 obj_t BgL_aux3506z00_6387;
BgL_aux3506z00_6387 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3506z00_6387))
{ /* Llib/hash.scm 854 */
BgL_res2605z00_4330 = BgL_aux3506z00_6387; }  else 
{ 
 obj_t BgL_auxz00_11392;
BgL_auxz00_11392 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34237L), BGl_string4029z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3506z00_6387); 
FAILURE(BgL_auxz00_11392,BFALSE,BFALSE);} } 
BgL_tmpz00_11388 = BgL_res2605z00_4330; } 
BgL_test4960z00_11387 = 
(BgL_tmpz00_11388==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4960z00_11387)
{ /* Llib/hash.scm 854 */
 int BgL_tmpz00_11397;
BgL_tmpz00_11397 = 
(int)(2L); 
BgL_bucketsz00_2183 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_11397); }  else 
{ /* Llib/hash.scm 854 */
BgL_bucketsz00_2183 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } 
{ /* Llib/hash.scm 854 */
 long BgL_bucketzd2lenzd2_2184;
{ /* Llib/hash.scm 855 */
 obj_t BgL_vectorz00_4331;
if(
VECTORP(BgL_bucketsz00_2183))
{ /* Llib/hash.scm 855 */
BgL_vectorz00_4331 = BgL_bucketsz00_2183; }  else 
{ 
 obj_t BgL_auxz00_11403;
BgL_auxz00_11403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34295L), BGl_string4029z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2183); 
FAILURE(BgL_auxz00_11403,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2184 = 
VECTOR_LENGTH(BgL_vectorz00_4331); } 
{ /* Llib/hash.scm 855 */
 long BgL_bucketzd2numzd2_2185;
{ /* Llib/hash.scm 856 */
 long BgL_arg1796z00_2212;
BgL_arg1796z00_2212 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_119, BgL_keyz00_120); 
{ /* Llib/hash.scm 856 */
 long BgL_n1z00_4332; long BgL_n2z00_4333;
BgL_n1z00_4332 = BgL_arg1796z00_2212; 
BgL_n2z00_4333 = BgL_bucketzd2lenzd2_2184; 
{ /* Llib/hash.scm 856 */
 bool_t BgL_test4963z00_11409;
{ /* Llib/hash.scm 856 */
 long BgL_arg2221z00_4335;
BgL_arg2221z00_4335 = 
(((BgL_n1z00_4332) | (BgL_n2z00_4333)) & -2147483648); 
BgL_test4963z00_11409 = 
(BgL_arg2221z00_4335==0L); } 
if(BgL_test4963z00_11409)
{ /* Llib/hash.scm 856 */
 int32_t BgL_arg2218z00_4336;
{ /* Llib/hash.scm 856 */
 int32_t BgL_arg2219z00_4337; int32_t BgL_arg2220z00_4338;
BgL_arg2219z00_4337 = 
(int32_t)(BgL_n1z00_4332); 
BgL_arg2220z00_4338 = 
(int32_t)(BgL_n2z00_4333); 
BgL_arg2218z00_4336 = 
(BgL_arg2219z00_4337%BgL_arg2220z00_4338); } 
{ /* Llib/hash.scm 856 */
 long BgL_arg2326z00_4343;
BgL_arg2326z00_4343 = 
(long)(BgL_arg2218z00_4336); 
BgL_bucketzd2numzd2_2185 = 
(long)(BgL_arg2326z00_4343); } }  else 
{ /* Llib/hash.scm 856 */
BgL_bucketzd2numzd2_2185 = 
(BgL_n1z00_4332%BgL_n2z00_4333); } } } } 
{ /* Llib/hash.scm 856 */
 obj_t BgL_bucketz00_2186;
{ /* Llib/hash.scm 857 */
 obj_t BgL_vectorz00_4345;
if(
VECTORP(BgL_bucketsz00_2183))
{ /* Llib/hash.scm 857 */
BgL_vectorz00_4345 = BgL_bucketsz00_2183; }  else 
{ 
 obj_t BgL_auxz00_11420;
BgL_auxz00_11420 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34405L), BGl_string4029z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2183); 
FAILURE(BgL_auxz00_11420,BFALSE,BFALSE);} 
BgL_bucketz00_2186 = 
VECTOR_REF(BgL_vectorz00_4345,BgL_bucketzd2numzd2_2185); } 
{ /* Llib/hash.scm 857 */
 obj_t BgL_maxzd2bucketzd2lenz00_2187;
{ /* Llib/hash.scm 858 */
 bool_t BgL_test4965z00_11425;
{ /* Llib/hash.scm 858 */
 obj_t BgL_tmpz00_11426;
{ /* Llib/hash.scm 858 */
 obj_t BgL_res2606z00_4350;
{ /* Llib/hash.scm 858 */
 obj_t BgL_aux3512z00_6393;
BgL_aux3512z00_6393 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3512z00_6393))
{ /* Llib/hash.scm 858 */
BgL_res2606z00_4350 = BgL_aux3512z00_6393; }  else 
{ 
 obj_t BgL_auxz00_11430;
BgL_auxz00_11430 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34445L), BGl_string4029z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3512z00_6393); 
FAILURE(BgL_auxz00_11430,BFALSE,BFALSE);} } 
BgL_tmpz00_11426 = BgL_res2606z00_4350; } 
BgL_test4965z00_11425 = 
(BgL_tmpz00_11426==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4965z00_11425)
{ /* Llib/hash.scm 858 */
 int BgL_tmpz00_11435;
BgL_tmpz00_11435 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_2187 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_11435); }  else 
{ /* Llib/hash.scm 858 */
BgL_maxzd2bucketzd2lenz00_2187 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } 
{ /* Llib/hash.scm 858 */

if(
NULLP(BgL_bucketz00_2186))
{ /* Llib/hash.scm 859 */
{ /* Llib/hash.scm 861 */
 long BgL_arg1775z00_2189;
{ /* Llib/hash.scm 861 */
 obj_t BgL_arg1777z00_2190;
{ /* Llib/hash.scm 861 */
 bool_t BgL_test4968z00_11441;
{ /* Llib/hash.scm 861 */
 obj_t BgL_tmpz00_11442;
{ /* Llib/hash.scm 861 */
 obj_t BgL_res2607z00_4354;
{ /* Llib/hash.scm 861 */
 obj_t BgL_aux3514z00_6395;
BgL_aux3514z00_6395 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3514z00_6395))
{ /* Llib/hash.scm 861 */
BgL_res2607z00_4354 = BgL_aux3514z00_6395; }  else 
{ 
 obj_t BgL_auxz00_11446;
BgL_auxz00_11446 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34555L), BGl_string4029z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3514z00_6395); 
FAILURE(BgL_auxz00_11446,BFALSE,BFALSE);} } 
BgL_tmpz00_11442 = BgL_res2607z00_4354; } 
BgL_test4968z00_11441 = 
(BgL_tmpz00_11442==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4968z00_11441)
{ /* Llib/hash.scm 861 */
 int BgL_tmpz00_11451;
BgL_tmpz00_11451 = 
(int)(0L); 
BgL_arg1777z00_2190 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_11451); }  else 
{ /* Llib/hash.scm 861 */
BgL_arg1777z00_2190 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } 
{ /* Llib/hash.scm 861 */
 long BgL_za71za7_4355;
{ /* Llib/hash.scm 861 */
 obj_t BgL_tmpz00_11455;
if(
INTEGERP(BgL_arg1777z00_2190))
{ /* Llib/hash.scm 861 */
BgL_tmpz00_11455 = BgL_arg1777z00_2190
; }  else 
{ 
 obj_t BgL_auxz00_11458;
BgL_auxz00_11458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34577L), BGl_string4029z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1777z00_2190); 
FAILURE(BgL_auxz00_11458,BFALSE,BFALSE);} 
BgL_za71za7_4355 = 
(long)CINT(BgL_tmpz00_11455); } 
BgL_arg1775z00_2189 = 
(BgL_za71za7_4355+1L); } } 
{ /* Llib/hash.scm 861 */
 bool_t BgL_test4971z00_11464;
{ /* Llib/hash.scm 861 */
 obj_t BgL_tmpz00_11465;
{ /* Llib/hash.scm 861 */
 obj_t BgL_res2608z00_4359;
{ /* Llib/hash.scm 861 */
 obj_t BgL_aux3517z00_6398;
BgL_aux3517z00_6398 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3517z00_6398))
{ /* Llib/hash.scm 861 */
BgL_res2608z00_4359 = BgL_aux3517z00_6398; }  else 
{ 
 obj_t BgL_auxz00_11469;
BgL_auxz00_11469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34522L), BGl_string4029z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3517z00_6398); 
FAILURE(BgL_auxz00_11469,BFALSE,BFALSE);} } 
BgL_tmpz00_11465 = BgL_res2608z00_4359; } 
BgL_test4971z00_11464 = 
(BgL_tmpz00_11465==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4971z00_11464)
{ /* Llib/hash.scm 861 */
 obj_t BgL_auxz00_11476; int BgL_tmpz00_11474;
BgL_auxz00_11476 = 
BINT(BgL_arg1775z00_2189); 
BgL_tmpz00_11474 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_11474, BgL_auxz00_11476); }  else 
{ /* Llib/hash.scm 861 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } } 
{ /* Llib/hash.scm 862 */
 obj_t BgL_arg1779z00_2191;
{ /* Llib/hash.scm 862 */
 obj_t BgL_arg1781z00_2192;
BgL_arg1781z00_2192 = 
MAKE_YOUNG_PAIR(BgL_keyz00_120, BgL_objz00_121); 
{ /* Llib/hash.scm 862 */
 obj_t BgL_list1782z00_2193;
BgL_list1782z00_2193 = 
MAKE_YOUNG_PAIR(BgL_arg1781z00_2192, BNIL); 
BgL_arg1779z00_2191 = BgL_list1782z00_2193; } } 
{ /* Llib/hash.scm 862 */
 obj_t BgL_vectorz00_4361;
if(
VECTORP(BgL_bucketsz00_2183))
{ /* Llib/hash.scm 862 */
BgL_vectorz00_4361 = BgL_bucketsz00_2183; }  else 
{ 
 obj_t BgL_auxz00_11484;
BgL_auxz00_11484 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34605L), BGl_string4029z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2183); 
FAILURE(BgL_auxz00_11484,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4361,BgL_bucketzd2numzd2_2185,BgL_arg1779z00_2191); } } 
return BgL_objz00_121;}  else 
{ 
 obj_t BgL_buckz00_2195; long BgL_countz00_2196;
BgL_buckz00_2195 = BgL_bucketz00_2186; 
BgL_countz00_2196 = 0L; 
BgL_zc3z04anonymousza31783ze3z87_2197:
if(
NULLP(BgL_buckz00_2195))
{ /* Llib/hash.scm 867 */
{ /* Llib/hash.scm 868 */
 long BgL_arg1785z00_2199;
{ /* Llib/hash.scm 868 */
 obj_t BgL_arg1786z00_2200;
{ /* Llib/hash.scm 868 */
 bool_t BgL_test4975z00_11491;
{ /* Llib/hash.scm 868 */
 obj_t BgL_tmpz00_11492;
{ /* Llib/hash.scm 868 */
 obj_t BgL_res2610z00_4366;
{ /* Llib/hash.scm 868 */
 obj_t BgL_aux3521z00_6402;
BgL_aux3521z00_6402 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3521z00_6402))
{ /* Llib/hash.scm 868 */
BgL_res2610z00_4366 = BgL_aux3521z00_6402; }  else 
{ 
 obj_t BgL_auxz00_11496;
BgL_auxz00_11496 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34768L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3521z00_6402); 
FAILURE(BgL_auxz00_11496,BFALSE,BFALSE);} } 
BgL_tmpz00_11492 = BgL_res2610z00_4366; } 
BgL_test4975z00_11491 = 
(BgL_tmpz00_11492==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4975z00_11491)
{ /* Llib/hash.scm 868 */
 int BgL_tmpz00_11501;
BgL_tmpz00_11501 = 
(int)(0L); 
BgL_arg1786z00_2200 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_11501); }  else 
{ /* Llib/hash.scm 868 */
BgL_arg1786z00_2200 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } 
{ /* Llib/hash.scm 868 */
 long BgL_za71za7_4367;
{ /* Llib/hash.scm 868 */
 obj_t BgL_tmpz00_11505;
if(
INTEGERP(BgL_arg1786z00_2200))
{ /* Llib/hash.scm 868 */
BgL_tmpz00_11505 = BgL_arg1786z00_2200
; }  else 
{ 
 obj_t BgL_auxz00_11508;
BgL_auxz00_11508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34790L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1786z00_2200); 
FAILURE(BgL_auxz00_11508,BFALSE,BFALSE);} 
BgL_za71za7_4367 = 
(long)CINT(BgL_tmpz00_11505); } 
BgL_arg1785z00_2199 = 
(BgL_za71za7_4367+1L); } } 
{ /* Llib/hash.scm 868 */
 bool_t BgL_test4978z00_11514;
{ /* Llib/hash.scm 868 */
 obj_t BgL_tmpz00_11515;
{ /* Llib/hash.scm 868 */
 obj_t BgL_res2611z00_4371;
{ /* Llib/hash.scm 868 */
 obj_t BgL_aux3524z00_6405;
BgL_aux3524z00_6405 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3524z00_6405))
{ /* Llib/hash.scm 868 */
BgL_res2611z00_4371 = BgL_aux3524z00_6405; }  else 
{ 
 obj_t BgL_auxz00_11519;
BgL_auxz00_11519 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34735L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3524z00_6405); 
FAILURE(BgL_auxz00_11519,BFALSE,BFALSE);} } 
BgL_tmpz00_11515 = BgL_res2611z00_4371; } 
BgL_test4978z00_11514 = 
(BgL_tmpz00_11515==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4978z00_11514)
{ /* Llib/hash.scm 868 */
 obj_t BgL_auxz00_11526; int BgL_tmpz00_11524;
BgL_auxz00_11526 = 
BINT(BgL_arg1785z00_2199); 
BgL_tmpz00_11524 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_11524, BgL_auxz00_11526); }  else 
{ /* Llib/hash.scm 868 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } } 
{ /* Llib/hash.scm 869 */
 obj_t BgL_arg1787z00_2201;
{ /* Llib/hash.scm 869 */
 obj_t BgL_arg1788z00_2202;
BgL_arg1788z00_2202 = 
MAKE_YOUNG_PAIR(BgL_keyz00_120, BgL_objz00_121); 
BgL_arg1787z00_2201 = 
MAKE_YOUNG_PAIR(BgL_arg1788z00_2202, BgL_bucketz00_2186); } 
{ /* Llib/hash.scm 869 */
 obj_t BgL_vectorz00_4372;
if(
VECTORP(BgL_bucketsz00_2183))
{ /* Llib/hash.scm 869 */
BgL_vectorz00_4372 = BgL_bucketsz00_2183; }  else 
{ 
 obj_t BgL_auxz00_11534;
BgL_auxz00_11534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34815L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2183); 
FAILURE(BgL_auxz00_11534,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4372,BgL_bucketzd2numzd2_2185,BgL_arg1787z00_2201); } } 
{ /* Llib/hash.scm 870 */
 bool_t BgL_test4981z00_11539;
{ /* Llib/hash.scm 870 */
 long BgL_n2z00_4375;
{ /* Llib/hash.scm 870 */
 obj_t BgL_tmpz00_11540;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_2187))
{ /* Llib/hash.scm 870 */
BgL_tmpz00_11540 = BgL_maxzd2bucketzd2lenz00_2187
; }  else 
{ 
 obj_t BgL_auxz00_11543;
BgL_auxz00_11543 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34884L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_maxzd2bucketzd2lenz00_2187); 
FAILURE(BgL_auxz00_11543,BFALSE,BFALSE);} 
BgL_n2z00_4375 = 
(long)CINT(BgL_tmpz00_11540); } 
BgL_test4981z00_11539 = 
(BgL_countz00_2196>BgL_n2z00_4375); } 
if(BgL_test4981z00_11539)
{ /* Llib/hash.scm 870 */
BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(BgL_tablez00_119); }  else 
{ /* Llib/hash.scm 870 */BFALSE; } } 
return BgL_objz00_121;}  else 
{ /* Llib/hash.scm 873 */
 bool_t BgL_test4983z00_11550;
{ /* Llib/hash.scm 873 */
 obj_t BgL_arg1795z00_2210;
{ /* Llib/hash.scm 873 */
 obj_t BgL_pairz00_4376;
if(
PAIRP(BgL_buckz00_2195))
{ /* Llib/hash.scm 873 */
BgL_pairz00_4376 = BgL_buckz00_2195; }  else 
{ 
 obj_t BgL_auxz00_11553;
BgL_auxz00_11553 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34980L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2195); 
FAILURE(BgL_auxz00_11553,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 873 */
 obj_t BgL_pairz00_4379;
{ /* Llib/hash.scm 873 */
 obj_t BgL_aux3531z00_6412;
BgL_aux3531z00_6412 = 
CAR(BgL_pairz00_4376); 
if(
PAIRP(BgL_aux3531z00_6412))
{ /* Llib/hash.scm 873 */
BgL_pairz00_4379 = BgL_aux3531z00_6412; }  else 
{ 
 obj_t BgL_auxz00_11560;
BgL_auxz00_11560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34974L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3531z00_6412); 
FAILURE(BgL_auxz00_11560,BFALSE,BFALSE);} } 
BgL_arg1795z00_2210 = 
CAR(BgL_pairz00_4379); } } 
{ /* Llib/hash.scm 873 */
 obj_t BgL_eqtz00_4380;
{ /* Llib/hash.scm 873 */
 bool_t BgL_test4986z00_11565;
{ /* Llib/hash.scm 873 */
 obj_t BgL_tmpz00_11566;
{ /* Llib/hash.scm 873 */
 obj_t BgL_res2612z00_4387;
{ /* Llib/hash.scm 873 */
 obj_t BgL_aux3533z00_6414;
BgL_aux3533z00_6414 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3533z00_6414))
{ /* Llib/hash.scm 873 */
BgL_res2612z00_4387 = BgL_aux3533z00_6414; }  else 
{ 
 obj_t BgL_auxz00_11570;
BgL_auxz00_11570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(34950L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3533z00_6414); 
FAILURE(BgL_auxz00_11570,BFALSE,BFALSE);} } 
BgL_tmpz00_11566 = BgL_res2612z00_4387; } 
BgL_test4986z00_11565 = 
(BgL_tmpz00_11566==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test4986z00_11565)
{ /* Llib/hash.scm 873 */
 int BgL_tmpz00_11575;
BgL_tmpz00_11575 = 
(int)(3L); 
BgL_eqtz00_4380 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_11575); }  else 
{ /* Llib/hash.scm 873 */
BgL_eqtz00_4380 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_119); } } 
if(
PROCEDUREP(BgL_eqtz00_4380))
{ /* Llib/hash.scm 873 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_4380, 2))
{ /* Llib/hash.scm 873 */
BgL_test4983z00_11550 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_4380, BgL_arg1795z00_2210, BgL_keyz00_120))
; }  else 
{ /* Llib/hash.scm 873 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4030z00zz__hashz00,BgL_eqtz00_4380);} }  else 
{ /* Llib/hash.scm 873 */
if(
(BgL_arg1795z00_2210==BgL_keyz00_120))
{ /* Llib/hash.scm 873 */
BgL_test4983z00_11550 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 873 */
if(
STRINGP(BgL_arg1795z00_2210))
{ /* Llib/hash.scm 873 */
if(
STRINGP(BgL_keyz00_120))
{ /* Llib/hash.scm 873 */
 long BgL_l1z00_4390;
BgL_l1z00_4390 = 
STRING_LENGTH(BgL_arg1795z00_2210); 
if(
(BgL_l1z00_4390==
STRING_LENGTH(BgL_keyz00_120)))
{ /* Llib/hash.scm 873 */
 int BgL_arg2114z00_4393;
{ /* Llib/hash.scm 873 */
 char * BgL_auxz00_11602; char * BgL_tmpz00_11600;
BgL_auxz00_11602 = 
BSTRING_TO_STRING(BgL_keyz00_120); 
BgL_tmpz00_11600 = 
BSTRING_TO_STRING(BgL_arg1795z00_2210); 
BgL_arg2114z00_4393 = 
memcmp(BgL_tmpz00_11600, BgL_auxz00_11602, BgL_l1z00_4390); } 
BgL_test4983z00_11550 = 
(
(long)(BgL_arg2114z00_4393)==0L); }  else 
{ /* Llib/hash.scm 873 */
BgL_test4983z00_11550 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 873 */
BgL_test4983z00_11550 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 873 */
BgL_test4983z00_11550 = ((bool_t)0)
; } } } } } 
if(BgL_test4983z00_11550)
{ /* Llib/hash.scm 874 */
 obj_t BgL_oldzd2objzd2_2206;
{ /* Llib/hash.scm 874 */
 obj_t BgL_pairz00_4399;
if(
PAIRP(BgL_buckz00_2195))
{ /* Llib/hash.scm 874 */
BgL_pairz00_4399 = BgL_buckz00_2195; }  else 
{ 
 obj_t BgL_auxz00_11609;
BgL_auxz00_11609 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35015L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2195); 
FAILURE(BgL_auxz00_11609,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 874 */
 obj_t BgL_pairz00_4402;
{ /* Llib/hash.scm 874 */
 obj_t BgL_aux3538z00_6420;
BgL_aux3538z00_6420 = 
CAR(BgL_pairz00_4399); 
if(
PAIRP(BgL_aux3538z00_6420))
{ /* Llib/hash.scm 874 */
BgL_pairz00_4402 = BgL_aux3538z00_6420; }  else 
{ 
 obj_t BgL_auxz00_11616;
BgL_auxz00_11616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35009L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3538z00_6420); 
FAILURE(BgL_auxz00_11616,BFALSE,BFALSE);} } 
BgL_oldzd2objzd2_2206 = 
CDR(BgL_pairz00_4402); } } 
{ /* Llib/hash.scm 875 */
 obj_t BgL_arg1792z00_2207;
{ /* Llib/hash.scm 875 */
 obj_t BgL_pairz00_4403;
if(
PAIRP(BgL_buckz00_2195))
{ /* Llib/hash.scm 875 */
BgL_pairz00_4403 = BgL_buckz00_2195; }  else 
{ 
 obj_t BgL_auxz00_11623;
BgL_auxz00_11623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35044L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2195); 
FAILURE(BgL_auxz00_11623,BFALSE,BFALSE);} 
BgL_arg1792z00_2207 = 
CAR(BgL_pairz00_4403); } 
{ /* Llib/hash.scm 875 */
 obj_t BgL_pairz00_4404;
if(
PAIRP(BgL_arg1792z00_2207))
{ /* Llib/hash.scm 875 */
BgL_pairz00_4404 = BgL_arg1792z00_2207; }  else 
{ 
 obj_t BgL_auxz00_11630;
BgL_auxz00_11630 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35048L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_arg1792z00_2207); 
FAILURE(BgL_auxz00_11630,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_4404, BgL_objz00_121); } } 
return BgL_oldzd2objzd2_2206;}  else 
{ /* Llib/hash.scm 878 */
 obj_t BgL_arg1793z00_2208; long BgL_arg1794z00_2209;
{ /* Llib/hash.scm 878 */
 obj_t BgL_pairz00_4405;
if(
PAIRP(BgL_buckz00_2195))
{ /* Llib/hash.scm 878 */
BgL_pairz00_4405 = BgL_buckz00_2195; }  else 
{ 
 obj_t BgL_auxz00_11637;
BgL_auxz00_11637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35093L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2195); 
FAILURE(BgL_auxz00_11637,BFALSE,BFALSE);} 
BgL_arg1793z00_2208 = 
CDR(BgL_pairz00_4405); } 
BgL_arg1794z00_2209 = 
(BgL_countz00_2196+1L); 
{ 
 long BgL_countz00_11644; obj_t BgL_buckz00_11643;
BgL_buckz00_11643 = BgL_arg1793z00_2208; 
BgL_countz00_11644 = BgL_arg1794z00_2209; 
BgL_countz00_2196 = BgL_countz00_11644; 
BgL_buckz00_2195 = BgL_buckz00_11643; 
goto BgL_zc3z04anonymousza31783ze3z87_2197;} } } } } } } } } } } 

}



/* hashtable-update! */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2updatez12zc0zz__hashz00(obj_t BgL_tablez00_122, obj_t BgL_keyz00_123, obj_t BgL_procz00_124, obj_t BgL_objz00_125)
{
{ /* Llib/hash.scm 883 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_122))
{ /* Llib/hash.scm 886 */
 obj_t BgL_auxz00_11647;
if(
STRINGP(BgL_keyz00_123))
{ /* Llib/hash.scm 886 */
BgL_auxz00_11647 = BgL_keyz00_123
; }  else 
{ 
 obj_t BgL_auxz00_11650;
BgL_auxz00_11650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35505L), BGl_string4033z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_123); 
FAILURE(BgL_auxz00_11650,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00(BgL_tablez00_122, BgL_auxz00_11647, BgL_procz00_124, BgL_objz00_125);}  else 
{ /* Llib/hash.scm 885 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_122))
{ /* Llib/hash.scm 887 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(BgL_tablez00_122, BgL_keyz00_123, BgL_procz00_124, BgL_objz00_125);}  else 
{ /* Llib/hash.scm 887 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2updatez12z12zz__hashz00(BgL_tablez00_122, BgL_keyz00_123, BgL_procz00_124, BgL_objz00_125);} } } 

}



/* &hashtable-update! */
obj_t BGl_z62hashtablezd2updatez12za2zz__hashz00(obj_t BgL_envz00_5492, obj_t BgL_tablez00_5493, obj_t BgL_keyz00_5494, obj_t BgL_procz00_5495, obj_t BgL_objz00_5496)
{
{ /* Llib/hash.scm 883 */
{ /* Llib/hash.scm 885 */
 obj_t BgL_auxz00_11666; obj_t BgL_auxz00_11659;
if(
PROCEDUREP(BgL_procz00_5495))
{ /* Llib/hash.scm 885 */
BgL_auxz00_11666 = BgL_procz00_5495
; }  else 
{ 
 obj_t BgL_auxz00_11669;
BgL_auxz00_11669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35429L), BGl_string4034z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_procz00_5495); 
FAILURE(BgL_auxz00_11669,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5493))
{ /* Llib/hash.scm 885 */
BgL_auxz00_11659 = BgL_tablez00_5493
; }  else 
{ 
 obj_t BgL_auxz00_11662;
BgL_auxz00_11662 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35429L), BGl_string4034z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5493); 
FAILURE(BgL_auxz00_11662,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2updatez12zc0zz__hashz00(BgL_auxz00_11659, BgL_keyz00_5494, BgL_auxz00_11666, BgL_objz00_5496);} } 

}



/* open-string-hashtable-update! */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00(obj_t BgL_tablez00_126, obj_t BgL_keyz00_127, obj_t BgL_procz00_128, obj_t BgL_objz00_129)
{
{ /* Llib/hash.scm 895 */
{ /* Llib/hash.scm 896 */
 obj_t BgL_siza7eza7_2215;
{ /* Llib/hash.scm 896 */
 bool_t BgL_test5004z00_11674;
{ /* Llib/hash.scm 896 */
 obj_t BgL_tmpz00_11675;
{ /* Llib/hash.scm 896 */
 obj_t BgL_res2613z00_4412;
{ /* Llib/hash.scm 896 */
 obj_t BgL_aux3552z00_6434;
BgL_aux3552z00_6434 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3552z00_6434))
{ /* Llib/hash.scm 896 */
BgL_res2613z00_4412 = BgL_aux3552z00_6434; }  else 
{ 
 obj_t BgL_auxz00_11679;
BgL_auxz00_11679 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35996L), BGl_string4035z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3552z00_6434); 
FAILURE(BgL_auxz00_11679,BFALSE,BFALSE);} } 
BgL_tmpz00_11675 = BgL_res2613z00_4412; } 
BgL_test5004z00_11674 = 
(BgL_tmpz00_11675==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5004z00_11674)
{ /* Llib/hash.scm 896 */
 int BgL_tmpz00_11684;
BgL_tmpz00_11684 = 
(int)(1L); 
BgL_siza7eza7_2215 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11684); }  else 
{ /* Llib/hash.scm 896 */
BgL_siza7eza7_2215 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_126); } } 
{ /* Llib/hash.scm 896 */
 obj_t BgL_bucketsz00_2216;
{ /* Llib/hash.scm 897 */
 bool_t BgL_test5006z00_11688;
{ /* Llib/hash.scm 897 */
 obj_t BgL_tmpz00_11689;
{ /* Llib/hash.scm 897 */
 obj_t BgL_res2614z00_4416;
{ /* Llib/hash.scm 897 */
 obj_t BgL_aux3554z00_6436;
BgL_aux3554z00_6436 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3554z00_6436))
{ /* Llib/hash.scm 897 */
BgL_res2614z00_4416 = BgL_aux3554z00_6436; }  else 
{ 
 obj_t BgL_auxz00_11693;
BgL_auxz00_11693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36043L), BGl_string4035z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3554z00_6436); 
FAILURE(BgL_auxz00_11693,BFALSE,BFALSE);} } 
BgL_tmpz00_11689 = BgL_res2614z00_4416; } 
BgL_test5006z00_11688 = 
(BgL_tmpz00_11689==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5006z00_11688)
{ /* Llib/hash.scm 897 */
 int BgL_tmpz00_11698;
BgL_tmpz00_11698 = 
(int)(2L); 
BgL_bucketsz00_2216 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11698); }  else 
{ /* Llib/hash.scm 897 */
BgL_bucketsz00_2216 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_126); } } 
{ /* Llib/hash.scm 897 */
 long BgL_hashz00_2217;
{ /* Llib/hash.scm 898 */
 long BgL_arg1816z00_2243;
BgL_arg1816z00_2243 = 
STRING_LENGTH(BgL_keyz00_127); 
BgL_hashz00_2217 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_127), 
(int)(0L), 
(int)(BgL_arg1816z00_2243)); } 
{ /* Llib/hash.scm 898 */

{ /* Llib/hash.scm 900 */
 long BgL_g1072z00_2218;
{ /* Llib/hash.scm 900 */
 long BgL_n1z00_4418; long BgL_n2z00_4419;
BgL_n1z00_4418 = BgL_hashz00_2217; 
{ /* Llib/hash.scm 900 */
 obj_t BgL_tmpz00_11707;
if(
INTEGERP(BgL_siza7eza7_2215))
{ /* Llib/hash.scm 900 */
BgL_tmpz00_11707 = BgL_siza7eza7_2215
; }  else 
{ 
 obj_t BgL_auxz00_11710;
BgL_auxz00_11710 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36185L), BGl_string4035z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2215); 
FAILURE(BgL_auxz00_11710,BFALSE,BFALSE);} 
BgL_n2z00_4419 = 
(long)CINT(BgL_tmpz00_11707); } 
{ /* Llib/hash.scm 900 */
 bool_t BgL_test5009z00_11715;
{ /* Llib/hash.scm 900 */
 long BgL_arg2221z00_4421;
BgL_arg2221z00_4421 = 
(((BgL_n1z00_4418) | (BgL_n2z00_4419)) & -2147483648); 
BgL_test5009z00_11715 = 
(BgL_arg2221z00_4421==0L); } 
if(BgL_test5009z00_11715)
{ /* Llib/hash.scm 900 */
 int32_t BgL_arg2218z00_4422;
{ /* Llib/hash.scm 900 */
 int32_t BgL_arg2219z00_4423; int32_t BgL_arg2220z00_4424;
BgL_arg2219z00_4423 = 
(int32_t)(BgL_n1z00_4418); 
BgL_arg2220z00_4424 = 
(int32_t)(BgL_n2z00_4419); 
BgL_arg2218z00_4422 = 
(BgL_arg2219z00_4423%BgL_arg2220z00_4424); } 
{ /* Llib/hash.scm 900 */
 long BgL_arg2326z00_4429;
BgL_arg2326z00_4429 = 
(long)(BgL_arg2218z00_4422); 
BgL_g1072z00_2218 = 
(long)(BgL_arg2326z00_4429); } }  else 
{ /* Llib/hash.scm 900 */
BgL_g1072z00_2218 = 
(BgL_n1z00_4418%BgL_n2z00_4419); } } } 
{ 
 long BgL_offz00_2220; long BgL_iz00_2221;
BgL_offz00_2220 = BgL_g1072z00_2218; 
BgL_iz00_2221 = 1L; 
BgL_zc3z04anonymousza31799ze3z87_2222:
{ /* Llib/hash.scm 902 */
 long BgL_off3z00_2223;
BgL_off3z00_2223 = 
(BgL_offz00_2220*3L); 
{ /* Llib/hash.scm 903 */
 bool_t BgL_test5010z00_11725;
{ /* Llib/hash.scm 903 */
 obj_t BgL_vectorz00_4432;
if(
VECTORP(BgL_bucketsz00_2216))
{ /* Llib/hash.scm 903 */
BgL_vectorz00_4432 = BgL_bucketsz00_2216; }  else 
{ 
 obj_t BgL_auxz00_11728;
BgL_auxz00_11728 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36251L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2216); 
FAILURE(BgL_auxz00_11728,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 903 */
 bool_t BgL_test5012z00_11732;
{ /* Llib/hash.scm 903 */
 long BgL_tmpz00_11733;
BgL_tmpz00_11733 = 
VECTOR_LENGTH(BgL_vectorz00_4432); 
BgL_test5012z00_11732 = 
BOUND_CHECK(BgL_off3z00_2223, BgL_tmpz00_11733); } 
if(BgL_test5012z00_11732)
{ /* Llib/hash.scm 903 */
BgL_test5010z00_11725 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4432,BgL_off3z00_2223))
; }  else 
{ 
 obj_t BgL_auxz00_11738;
BgL_auxz00_11738 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36239L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4432, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4432)), 
(int)(BgL_off3z00_2223)); 
FAILURE(BgL_auxz00_11738,BFALSE,BFALSE);} } } 
if(BgL_test5010z00_11725)
{ /* Llib/hash.scm 904 */
 bool_t BgL_test5013z00_11745;
{ /* Llib/hash.scm 904 */
 obj_t BgL_arg1815z00_2241;
{ /* Llib/hash.scm 904 */
 obj_t BgL_vectorz00_4434;
if(
VECTORP(BgL_bucketsz00_2216))
{ /* Llib/hash.scm 904 */
BgL_vectorz00_4434 = BgL_bucketsz00_2216; }  else 
{ 
 obj_t BgL_auxz00_11748;
BgL_auxz00_11748 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36293L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2216); 
FAILURE(BgL_auxz00_11748,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 904 */
 bool_t BgL_test5015z00_11752;
{ /* Llib/hash.scm 904 */
 long BgL_tmpz00_11753;
BgL_tmpz00_11753 = 
VECTOR_LENGTH(BgL_vectorz00_4434); 
BgL_test5015z00_11752 = 
BOUND_CHECK(BgL_off3z00_2223, BgL_tmpz00_11753); } 
if(BgL_test5015z00_11752)
{ /* Llib/hash.scm 904 */
BgL_arg1815z00_2241 = 
VECTOR_REF(BgL_vectorz00_4434,BgL_off3z00_2223); }  else 
{ 
 obj_t BgL_auxz00_11757;
BgL_auxz00_11757 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36281L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4434, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4434)), 
(int)(BgL_off3z00_2223)); 
FAILURE(BgL_auxz00_11757,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 904 */
 obj_t BgL_string1z00_4436;
if(
STRINGP(BgL_arg1815z00_2241))
{ /* Llib/hash.scm 904 */
BgL_string1z00_4436 = BgL_arg1815z00_2241; }  else 
{ 
 obj_t BgL_auxz00_11766;
BgL_auxz00_11766 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36305L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1815z00_2241); 
FAILURE(BgL_auxz00_11766,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 904 */
 long BgL_l1z00_4438;
BgL_l1z00_4438 = 
STRING_LENGTH(BgL_string1z00_4436); 
if(
(BgL_l1z00_4438==
STRING_LENGTH(BgL_keyz00_127)))
{ /* Llib/hash.scm 904 */
 int BgL_arg2114z00_4441;
{ /* Llib/hash.scm 904 */
 char * BgL_auxz00_11776; char * BgL_tmpz00_11774;
BgL_auxz00_11776 = 
BSTRING_TO_STRING(BgL_keyz00_127); 
BgL_tmpz00_11774 = 
BSTRING_TO_STRING(BgL_string1z00_4436); 
BgL_arg2114z00_4441 = 
memcmp(BgL_tmpz00_11774, BgL_auxz00_11776, BgL_l1z00_4438); } 
BgL_test5013z00_11745 = 
(
(long)(BgL_arg2114z00_4441)==0L); }  else 
{ /* Llib/hash.scm 904 */
BgL_test5013z00_11745 = ((bool_t)0)
; } } } } 
if(BgL_test5013z00_11745)
{ /* Llib/hash.scm 905 */
 bool_t BgL_test5018z00_11781;
{ /* Llib/hash.scm 905 */
 long BgL_arg1809z00_2234;
BgL_arg1809z00_2234 = 
(BgL_off3z00_2223+2L); 
{ /* Llib/hash.scm 905 */
 obj_t BgL_vectorz00_4448;
if(
VECTORP(BgL_bucketsz00_2216))
{ /* Llib/hash.scm 905 */
BgL_vectorz00_4448 = BgL_bucketsz00_2216; }  else 
{ 
 obj_t BgL_auxz00_11785;
BgL_auxz00_11785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36334L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2216); 
FAILURE(BgL_auxz00_11785,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 905 */
 bool_t BgL_test5020z00_11789;
{ /* Llib/hash.scm 905 */
 long BgL_tmpz00_11790;
BgL_tmpz00_11790 = 
VECTOR_LENGTH(BgL_vectorz00_4448); 
BgL_test5020z00_11789 = 
BOUND_CHECK(BgL_arg1809z00_2234, BgL_tmpz00_11790); } 
if(BgL_test5020z00_11789)
{ /* Llib/hash.scm 905 */
BgL_test5018z00_11781 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4448,BgL_arg1809z00_2234))
; }  else 
{ 
 obj_t BgL_auxz00_11795;
BgL_auxz00_11795 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36322L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4448, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4448)), 
(int)(BgL_arg1809z00_2234)); 
FAILURE(BgL_auxz00_11795,BFALSE,BFALSE);} } } } 
if(BgL_test5018z00_11781)
{ /* Llib/hash.scm 906 */
 obj_t BgL_ovalz00_2229;
{ /* Llib/hash.scm 906 */
 long BgL_arg1807z00_2232;
BgL_arg1807z00_2232 = 
(BgL_off3z00_2223+1L); 
{ /* Llib/hash.scm 906 */
 obj_t BgL_vectorz00_4451;
if(
VECTORP(BgL_bucketsz00_2216))
{ /* Llib/hash.scm 906 */
BgL_vectorz00_4451 = BgL_bucketsz00_2216; }  else 
{ 
 obj_t BgL_auxz00_11805;
BgL_auxz00_11805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36383L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2216); 
FAILURE(BgL_auxz00_11805,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 906 */
 bool_t BgL_test5022z00_11809;
{ /* Llib/hash.scm 906 */
 long BgL_tmpz00_11810;
BgL_tmpz00_11810 = 
VECTOR_LENGTH(BgL_vectorz00_4451); 
BgL_test5022z00_11809 = 
BOUND_CHECK(BgL_arg1807z00_2232, BgL_tmpz00_11810); } 
if(BgL_test5022z00_11809)
{ /* Llib/hash.scm 906 */
BgL_ovalz00_2229 = 
VECTOR_REF(BgL_vectorz00_4451,BgL_arg1807z00_2232); }  else 
{ 
 obj_t BgL_auxz00_11814;
BgL_auxz00_11814 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36371L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4451, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4451)), 
(int)(BgL_arg1807z00_2232)); 
FAILURE(BgL_auxz00_11814,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 907 */
 long BgL_arg1805z00_2230; obj_t BgL_arg1806z00_2231;
BgL_arg1805z00_2230 = 
(BgL_off3z00_2223+1L); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_128, 1))
{ /* Llib/hash.scm 907 */
BgL_arg1806z00_2231 = 
BGL_PROCEDURE_CALL1(BgL_procz00_128, BgL_ovalz00_2229); }  else 
{ /* Llib/hash.scm 907 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4036z00zz__hashz00,BgL_procz00_128);} 
{ /* Llib/hash.scm 907 */
 obj_t BgL_vectorz00_4454;
if(
VECTORP(BgL_bucketsz00_2216))
{ /* Llib/hash.scm 907 */
BgL_vectorz00_4454 = BgL_bucketsz00_2216; }  else 
{ 
 obj_t BgL_auxz00_11831;
BgL_auxz00_11831 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36426L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2216); 
FAILURE(BgL_auxz00_11831,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 907 */
 bool_t BgL_test5025z00_11835;
{ /* Llib/hash.scm 907 */
 long BgL_tmpz00_11836;
BgL_tmpz00_11836 = 
VECTOR_LENGTH(BgL_vectorz00_4454); 
BgL_test5025z00_11835 = 
BOUND_CHECK(BgL_arg1805z00_2230, BgL_tmpz00_11836); } 
if(BgL_test5025z00_11835)
{ /* Llib/hash.scm 907 */
return 
VECTOR_SET(BgL_vectorz00_4454,BgL_arg1805z00_2230,BgL_arg1806z00_2231);}  else 
{ 
 obj_t BgL_auxz00_11840;
BgL_auxz00_11840 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36413L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4454, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4454)), 
(int)(BgL_arg1805z00_2230)); 
FAILURE(BgL_auxz00_11840,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/hash.scm 908 */
 long BgL_arg1808z00_2233;
BgL_arg1808z00_2233 = 
(BgL_off3z00_2223+1L); 
{ /* Llib/hash.scm 908 */
 obj_t BgL_vectorz00_4457;
if(
VECTORP(BgL_bucketsz00_2216))
{ /* Llib/hash.scm 908 */
BgL_vectorz00_4457 = BgL_bucketsz00_2216; }  else 
{ 
 obj_t BgL_auxz00_11850;
BgL_auxz00_11850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36477L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2216); 
FAILURE(BgL_auxz00_11850,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 908 */
 bool_t BgL_test5027z00_11854;
{ /* Llib/hash.scm 908 */
 long BgL_tmpz00_11855;
BgL_tmpz00_11855 = 
VECTOR_LENGTH(BgL_vectorz00_4457); 
BgL_test5027z00_11854 = 
BOUND_CHECK(BgL_arg1808z00_2233, BgL_tmpz00_11855); } 
if(BgL_test5027z00_11854)
{ /* Llib/hash.scm 908 */
return 
VECTOR_SET(BgL_vectorz00_4457,BgL_arg1808z00_2233,BgL_objz00_129);}  else 
{ 
 obj_t BgL_auxz00_11859;
BgL_auxz00_11859 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36464L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4457, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4457)), 
(int)(BgL_arg1808z00_2233)); 
FAILURE(BgL_auxz00_11859,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/hash.scm 909 */
 long BgL_noffz00_2235;
BgL_noffz00_2235 = 
(BgL_offz00_2220+
(BgL_iz00_2221*BgL_iz00_2221)); 
{ /* Llib/hash.scm 910 */
 bool_t BgL_test5028z00_11868;
{ /* Llib/hash.scm 910 */
 long BgL_n2z00_4464;
{ /* Llib/hash.scm 910 */
 obj_t BgL_tmpz00_11869;
if(
INTEGERP(BgL_siza7eza7_2215))
{ /* Llib/hash.scm 910 */
BgL_tmpz00_11869 = BgL_siza7eza7_2215
; }  else 
{ 
 obj_t BgL_auxz00_11872;
BgL_auxz00_11872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36568L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2215); 
FAILURE(BgL_auxz00_11872,BFALSE,BFALSE);} 
BgL_n2z00_4464 = 
(long)CINT(BgL_tmpz00_11869); } 
BgL_test5028z00_11868 = 
(BgL_noffz00_2235>=BgL_n2z00_4464); } 
if(BgL_test5028z00_11868)
{ /* Llib/hash.scm 911 */
 long BgL_arg1811z00_2237; long BgL_arg1812z00_2238;
{ /* Llib/hash.scm 911 */
 long BgL_n1z00_4465; long BgL_n2z00_4466;
BgL_n1z00_4465 = BgL_noffz00_2235; 
{ /* Llib/hash.scm 911 */
 obj_t BgL_tmpz00_11878;
if(
INTEGERP(BgL_siza7eza7_2215))
{ /* Llib/hash.scm 911 */
BgL_tmpz00_11878 = BgL_siza7eza7_2215
; }  else 
{ 
 obj_t BgL_auxz00_11881;
BgL_auxz00_11881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(36604L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2215); 
FAILURE(BgL_auxz00_11881,BFALSE,BFALSE);} 
BgL_n2z00_4466 = 
(long)CINT(BgL_tmpz00_11878); } 
{ /* Llib/hash.scm 911 */
 bool_t BgL_test5031z00_11886;
{ /* Llib/hash.scm 911 */
 long BgL_arg2221z00_4468;
BgL_arg2221z00_4468 = 
(((BgL_n1z00_4465) | (BgL_n2z00_4466)) & -2147483648); 
BgL_test5031z00_11886 = 
(BgL_arg2221z00_4468==0L); } 
if(BgL_test5031z00_11886)
{ /* Llib/hash.scm 911 */
 int32_t BgL_arg2218z00_4469;
{ /* Llib/hash.scm 911 */
 int32_t BgL_arg2219z00_4470; int32_t BgL_arg2220z00_4471;
BgL_arg2219z00_4470 = 
(int32_t)(BgL_n1z00_4465); 
BgL_arg2220z00_4471 = 
(int32_t)(BgL_n2z00_4466); 
BgL_arg2218z00_4469 = 
(BgL_arg2219z00_4470%BgL_arg2220z00_4471); } 
{ /* Llib/hash.scm 911 */
 long BgL_arg2326z00_4476;
BgL_arg2326z00_4476 = 
(long)(BgL_arg2218z00_4469); 
BgL_arg1811z00_2237 = 
(long)(BgL_arg2326z00_4476); } }  else 
{ /* Llib/hash.scm 911 */
BgL_arg1811z00_2237 = 
(BgL_n1z00_4465%BgL_n2z00_4466); } } } 
BgL_arg1812z00_2238 = 
(BgL_iz00_2221+1L); 
{ 
 long BgL_iz00_11897; long BgL_offz00_11896;
BgL_offz00_11896 = BgL_arg1811z00_2237; 
BgL_iz00_11897 = BgL_arg1812z00_2238; 
BgL_iz00_2221 = BgL_iz00_11897; 
BgL_offz00_2220 = BgL_offz00_11896; 
goto BgL_zc3z04anonymousza31799ze3z87_2222;} }  else 
{ 
 long BgL_iz00_11899; long BgL_offz00_11898;
BgL_offz00_11898 = BgL_noffz00_2235; 
BgL_iz00_11899 = 
(BgL_iz00_2221+1L); 
BgL_iz00_2221 = BgL_iz00_11899; 
BgL_offz00_2220 = BgL_offz00_11898; 
goto BgL_zc3z04anonymousza31799ze3z87_2222;} } } }  else 
{ /* Llib/hash.scm 903 */
return 
BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(BgL_tablez00_126, BgL_keyz00_127, BgL_objz00_129, 
BINT(BgL_hashz00_2217));} } } } } } } } } } 

}



/* &open-string-hashtable-update! */
obj_t BGl_z62openzd2stringzd2hashtablezd2updatez12za2zz__hashz00(obj_t BgL_envz00_5497, obj_t BgL_tablez00_5498, obj_t BgL_keyz00_5499, obj_t BgL_procz00_5500, obj_t BgL_objz00_5501)
{
{ /* Llib/hash.scm 895 */
{ /* Llib/hash.scm 896 */
 obj_t BgL_auxz00_11917; obj_t BgL_auxz00_11910; obj_t BgL_auxz00_11903;
if(
PROCEDUREP(BgL_procz00_5500))
{ /* Llib/hash.scm 896 */
BgL_auxz00_11917 = BgL_procz00_5500
; }  else 
{ 
 obj_t BgL_auxz00_11920;
BgL_auxz00_11920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35983L), BGl_string4041z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_procz00_5500); 
FAILURE(BgL_auxz00_11920,BFALSE,BFALSE);} 
if(
STRINGP(BgL_keyz00_5499))
{ /* Llib/hash.scm 896 */
BgL_auxz00_11910 = BgL_keyz00_5499
; }  else 
{ 
 obj_t BgL_auxz00_11913;
BgL_auxz00_11913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35983L), BGl_string4041z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5499); 
FAILURE(BgL_auxz00_11913,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5498))
{ /* Llib/hash.scm 896 */
BgL_auxz00_11903 = BgL_tablez00_5498
; }  else 
{ 
 obj_t BgL_auxz00_11906;
BgL_auxz00_11906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(35983L), BGl_string4041z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5498); 
FAILURE(BgL_auxz00_11906,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2updatez12zc0zz__hashz00(BgL_auxz00_11903, BgL_auxz00_11910, BgL_auxz00_11917, BgL_objz00_5501);} } 

}



/* plain-hashtable-update! */
obj_t BGl_plainzd2hashtablezd2updatez12z12zz__hashz00(obj_t BgL_tablez00_130, obj_t BgL_keyz00_131, obj_t BgL_procz00_132, obj_t BgL_objz00_133)
{
{ /* Llib/hash.scm 918 */
{ /* Llib/hash.scm 919 */
 obj_t BgL_bucketsz00_2244;
{ /* Llib/hash.scm 919 */
 bool_t BgL_test5035z00_11925;
{ /* Llib/hash.scm 919 */
 obj_t BgL_tmpz00_11926;
{ /* Llib/hash.scm 919 */
 obj_t BgL_res2615z00_4483;
{ /* Llib/hash.scm 919 */
 obj_t BgL_aux3580z00_6463;
BgL_aux3580z00_6463 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3580z00_6463))
{ /* Llib/hash.scm 919 */
BgL_res2615z00_4483 = BgL_aux3580z00_6463; }  else 
{ 
 obj_t BgL_auxz00_11930;
BgL_auxz00_11930 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37034L), BGl_string4042z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3580z00_6463); 
FAILURE(BgL_auxz00_11930,BFALSE,BFALSE);} } 
BgL_tmpz00_11926 = BgL_res2615z00_4483; } 
BgL_test5035z00_11925 = 
(BgL_tmpz00_11926==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5035z00_11925)
{ /* Llib/hash.scm 919 */
 int BgL_tmpz00_11935;
BgL_tmpz00_11935 = 
(int)(2L); 
BgL_bucketsz00_2244 = 
STRUCT_REF(BgL_tablez00_130, BgL_tmpz00_11935); }  else 
{ /* Llib/hash.scm 919 */
BgL_bucketsz00_2244 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } 
{ /* Llib/hash.scm 919 */
 long BgL_bucketzd2lenzd2_2245;
{ /* Llib/hash.scm 920 */
 obj_t BgL_vectorz00_4484;
if(
VECTORP(BgL_bucketsz00_2244))
{ /* Llib/hash.scm 920 */
BgL_vectorz00_4484 = BgL_bucketsz00_2244; }  else 
{ 
 obj_t BgL_auxz00_11941;
BgL_auxz00_11941 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37092L), BGl_string4042z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2244); 
FAILURE(BgL_auxz00_11941,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2245 = 
VECTOR_LENGTH(BgL_vectorz00_4484); } 
{ /* Llib/hash.scm 920 */
 long BgL_bucketzd2numzd2_2246;
{ /* Llib/hash.scm 921 */
 long BgL_arg1838z00_2274;
BgL_arg1838z00_2274 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_130, BgL_keyz00_131); 
{ /* Llib/hash.scm 921 */
 long BgL_n1z00_4485; long BgL_n2z00_4486;
BgL_n1z00_4485 = BgL_arg1838z00_2274; 
BgL_n2z00_4486 = BgL_bucketzd2lenzd2_2245; 
{ /* Llib/hash.scm 921 */
 bool_t BgL_test5038z00_11947;
{ /* Llib/hash.scm 921 */
 long BgL_arg2221z00_4488;
BgL_arg2221z00_4488 = 
(((BgL_n1z00_4485) | (BgL_n2z00_4486)) & -2147483648); 
BgL_test5038z00_11947 = 
(BgL_arg2221z00_4488==0L); } 
if(BgL_test5038z00_11947)
{ /* Llib/hash.scm 921 */
 int32_t BgL_arg2218z00_4489;
{ /* Llib/hash.scm 921 */
 int32_t BgL_arg2219z00_4490; int32_t BgL_arg2220z00_4491;
BgL_arg2219z00_4490 = 
(int32_t)(BgL_n1z00_4485); 
BgL_arg2220z00_4491 = 
(int32_t)(BgL_n2z00_4486); 
BgL_arg2218z00_4489 = 
(BgL_arg2219z00_4490%BgL_arg2220z00_4491); } 
{ /* Llib/hash.scm 921 */
 long BgL_arg2326z00_4496;
BgL_arg2326z00_4496 = 
(long)(BgL_arg2218z00_4489); 
BgL_bucketzd2numzd2_2246 = 
(long)(BgL_arg2326z00_4496); } }  else 
{ /* Llib/hash.scm 921 */
BgL_bucketzd2numzd2_2246 = 
(BgL_n1z00_4485%BgL_n2z00_4486); } } } } 
{ /* Llib/hash.scm 921 */
 obj_t BgL_bucketz00_2247;
{ /* Llib/hash.scm 922 */
 obj_t BgL_vectorz00_4498;
if(
VECTORP(BgL_bucketsz00_2244))
{ /* Llib/hash.scm 922 */
BgL_vectorz00_4498 = BgL_bucketsz00_2244; }  else 
{ 
 obj_t BgL_auxz00_11958;
BgL_auxz00_11958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37202L), BGl_string4042z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2244); 
FAILURE(BgL_auxz00_11958,BFALSE,BFALSE);} 
BgL_bucketz00_2247 = 
VECTOR_REF(BgL_vectorz00_4498,BgL_bucketzd2numzd2_2246); } 
{ /* Llib/hash.scm 922 */
 obj_t BgL_maxzd2bucketzd2lenz00_2248;
{ /* Llib/hash.scm 923 */
 bool_t BgL_test5040z00_11963;
{ /* Llib/hash.scm 923 */
 obj_t BgL_tmpz00_11964;
{ /* Llib/hash.scm 923 */
 obj_t BgL_res2616z00_4503;
{ /* Llib/hash.scm 923 */
 obj_t BgL_aux3586z00_6469;
BgL_aux3586z00_6469 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3586z00_6469))
{ /* Llib/hash.scm 923 */
BgL_res2616z00_4503 = BgL_aux3586z00_6469; }  else 
{ 
 obj_t BgL_auxz00_11968;
BgL_auxz00_11968 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37242L), BGl_string4042z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3586z00_6469); 
FAILURE(BgL_auxz00_11968,BFALSE,BFALSE);} } 
BgL_tmpz00_11964 = BgL_res2616z00_4503; } 
BgL_test5040z00_11963 = 
(BgL_tmpz00_11964==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5040z00_11963)
{ /* Llib/hash.scm 923 */
 int BgL_tmpz00_11973;
BgL_tmpz00_11973 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_2248 = 
STRUCT_REF(BgL_tablez00_130, BgL_tmpz00_11973); }  else 
{ /* Llib/hash.scm 923 */
BgL_maxzd2bucketzd2lenz00_2248 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } 
{ /* Llib/hash.scm 923 */

if(
NULLP(BgL_bucketz00_2247))
{ /* Llib/hash.scm 924 */
{ /* Llib/hash.scm 926 */
 long BgL_arg1818z00_2250;
{ /* Llib/hash.scm 926 */
 obj_t BgL_arg1819z00_2251;
{ /* Llib/hash.scm 926 */
 bool_t BgL_test5043z00_11979;
{ /* Llib/hash.scm 926 */
 obj_t BgL_tmpz00_11980;
{ /* Llib/hash.scm 926 */
 obj_t BgL_res2617z00_4507;
{ /* Llib/hash.scm 926 */
 obj_t BgL_aux3588z00_6471;
BgL_aux3588z00_6471 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3588z00_6471))
{ /* Llib/hash.scm 926 */
BgL_res2617z00_4507 = BgL_aux3588z00_6471; }  else 
{ 
 obj_t BgL_auxz00_11984;
BgL_auxz00_11984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37352L), BGl_string4042z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3588z00_6471); 
FAILURE(BgL_auxz00_11984,BFALSE,BFALSE);} } 
BgL_tmpz00_11980 = BgL_res2617z00_4507; } 
BgL_test5043z00_11979 = 
(BgL_tmpz00_11980==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5043z00_11979)
{ /* Llib/hash.scm 926 */
 int BgL_tmpz00_11989;
BgL_tmpz00_11989 = 
(int)(0L); 
BgL_arg1819z00_2251 = 
STRUCT_REF(BgL_tablez00_130, BgL_tmpz00_11989); }  else 
{ /* Llib/hash.scm 926 */
BgL_arg1819z00_2251 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } 
{ /* Llib/hash.scm 926 */
 long BgL_za71za7_4508;
{ /* Llib/hash.scm 926 */
 obj_t BgL_tmpz00_11993;
if(
INTEGERP(BgL_arg1819z00_2251))
{ /* Llib/hash.scm 926 */
BgL_tmpz00_11993 = BgL_arg1819z00_2251
; }  else 
{ 
 obj_t BgL_auxz00_11996;
BgL_auxz00_11996 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37374L), BGl_string4042z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1819z00_2251); 
FAILURE(BgL_auxz00_11996,BFALSE,BFALSE);} 
BgL_za71za7_4508 = 
(long)CINT(BgL_tmpz00_11993); } 
BgL_arg1818z00_2250 = 
(BgL_za71za7_4508+1L); } } 
{ /* Llib/hash.scm 926 */
 bool_t BgL_test5046z00_12002;
{ /* Llib/hash.scm 926 */
 obj_t BgL_tmpz00_12003;
{ /* Llib/hash.scm 926 */
 obj_t BgL_res2618z00_4512;
{ /* Llib/hash.scm 926 */
 obj_t BgL_aux3591z00_6474;
BgL_aux3591z00_6474 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3591z00_6474))
{ /* Llib/hash.scm 926 */
BgL_res2618z00_4512 = BgL_aux3591z00_6474; }  else 
{ 
 obj_t BgL_auxz00_12007;
BgL_auxz00_12007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37319L), BGl_string4042z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3591z00_6474); 
FAILURE(BgL_auxz00_12007,BFALSE,BFALSE);} } 
BgL_tmpz00_12003 = BgL_res2618z00_4512; } 
BgL_test5046z00_12002 = 
(BgL_tmpz00_12003==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5046z00_12002)
{ /* Llib/hash.scm 926 */
 obj_t BgL_auxz00_12014; int BgL_tmpz00_12012;
BgL_auxz00_12014 = 
BINT(BgL_arg1818z00_2250); 
BgL_tmpz00_12012 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_130, BgL_tmpz00_12012, BgL_auxz00_12014); }  else 
{ /* Llib/hash.scm 926 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } } 
{ /* Llib/hash.scm 927 */
 obj_t BgL_arg1820z00_2252;
{ /* Llib/hash.scm 927 */
 obj_t BgL_arg1822z00_2253;
BgL_arg1822z00_2253 = 
MAKE_YOUNG_PAIR(BgL_keyz00_131, BgL_objz00_133); 
{ /* Llib/hash.scm 927 */
 obj_t BgL_list1823z00_2254;
BgL_list1823z00_2254 = 
MAKE_YOUNG_PAIR(BgL_arg1822z00_2253, BNIL); 
BgL_arg1820z00_2252 = BgL_list1823z00_2254; } } 
{ /* Llib/hash.scm 927 */
 obj_t BgL_vectorz00_4514;
if(
VECTORP(BgL_bucketsz00_2244))
{ /* Llib/hash.scm 927 */
BgL_vectorz00_4514 = BgL_bucketsz00_2244; }  else 
{ 
 obj_t BgL_auxz00_12022;
BgL_auxz00_12022 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37402L), BGl_string4042z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2244); 
FAILURE(BgL_auxz00_12022,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4514,BgL_bucketzd2numzd2_2246,BgL_arg1820z00_2252); } } 
return BgL_objz00_133;}  else 
{ 
 obj_t BgL_buckz00_2256; long BgL_countz00_2257;
BgL_buckz00_2256 = BgL_bucketz00_2247; 
BgL_countz00_2257 = 0L; 
BgL_zc3z04anonymousza31824ze3z87_2258:
if(
NULLP(BgL_buckz00_2256))
{ /* Llib/hash.scm 932 */
{ /* Llib/hash.scm 933 */
 long BgL_arg1826z00_2260;
{ /* Llib/hash.scm 933 */
 obj_t BgL_arg1827z00_2261;
{ /* Llib/hash.scm 933 */
 bool_t BgL_test5050z00_12029;
{ /* Llib/hash.scm 933 */
 obj_t BgL_tmpz00_12030;
{ /* Llib/hash.scm 933 */
 obj_t BgL_res2620z00_4519;
{ /* Llib/hash.scm 933 */
 obj_t BgL_aux3595z00_6478;
BgL_aux3595z00_6478 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3595z00_6478))
{ /* Llib/hash.scm 933 */
BgL_res2620z00_4519 = BgL_aux3595z00_6478; }  else 
{ 
 obj_t BgL_auxz00_12034;
BgL_auxz00_12034 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37565L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3595z00_6478); 
FAILURE(BgL_auxz00_12034,BFALSE,BFALSE);} } 
BgL_tmpz00_12030 = BgL_res2620z00_4519; } 
BgL_test5050z00_12029 = 
(BgL_tmpz00_12030==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5050z00_12029)
{ /* Llib/hash.scm 933 */
 int BgL_tmpz00_12039;
BgL_tmpz00_12039 = 
(int)(0L); 
BgL_arg1827z00_2261 = 
STRUCT_REF(BgL_tablez00_130, BgL_tmpz00_12039); }  else 
{ /* Llib/hash.scm 933 */
BgL_arg1827z00_2261 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } 
{ /* Llib/hash.scm 933 */
 long BgL_za71za7_4520;
{ /* Llib/hash.scm 933 */
 obj_t BgL_tmpz00_12043;
if(
INTEGERP(BgL_arg1827z00_2261))
{ /* Llib/hash.scm 933 */
BgL_tmpz00_12043 = BgL_arg1827z00_2261
; }  else 
{ 
 obj_t BgL_auxz00_12046;
BgL_auxz00_12046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37587L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1827z00_2261); 
FAILURE(BgL_auxz00_12046,BFALSE,BFALSE);} 
BgL_za71za7_4520 = 
(long)CINT(BgL_tmpz00_12043); } 
BgL_arg1826z00_2260 = 
(BgL_za71za7_4520+1L); } } 
{ /* Llib/hash.scm 933 */
 bool_t BgL_test5053z00_12052;
{ /* Llib/hash.scm 933 */
 obj_t BgL_tmpz00_12053;
{ /* Llib/hash.scm 933 */
 obj_t BgL_res2621z00_4524;
{ /* Llib/hash.scm 933 */
 obj_t BgL_aux3598z00_6481;
BgL_aux3598z00_6481 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3598z00_6481))
{ /* Llib/hash.scm 933 */
BgL_res2621z00_4524 = BgL_aux3598z00_6481; }  else 
{ 
 obj_t BgL_auxz00_12057;
BgL_auxz00_12057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37532L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3598z00_6481); 
FAILURE(BgL_auxz00_12057,BFALSE,BFALSE);} } 
BgL_tmpz00_12053 = BgL_res2621z00_4524; } 
BgL_test5053z00_12052 = 
(BgL_tmpz00_12053==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5053z00_12052)
{ /* Llib/hash.scm 933 */
 obj_t BgL_auxz00_12064; int BgL_tmpz00_12062;
BgL_auxz00_12064 = 
BINT(BgL_arg1826z00_2260); 
BgL_tmpz00_12062 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_130, BgL_tmpz00_12062, BgL_auxz00_12064); }  else 
{ /* Llib/hash.scm 933 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } } 
{ /* Llib/hash.scm 934 */
 obj_t BgL_arg1828z00_2262;
{ /* Llib/hash.scm 934 */
 obj_t BgL_arg1829z00_2263;
BgL_arg1829z00_2263 = 
MAKE_YOUNG_PAIR(BgL_keyz00_131, BgL_objz00_133); 
BgL_arg1828z00_2262 = 
MAKE_YOUNG_PAIR(BgL_arg1829z00_2263, BgL_bucketz00_2247); } 
{ /* Llib/hash.scm 934 */
 obj_t BgL_vectorz00_4525;
if(
VECTORP(BgL_bucketsz00_2244))
{ /* Llib/hash.scm 934 */
BgL_vectorz00_4525 = BgL_bucketsz00_2244; }  else 
{ 
 obj_t BgL_auxz00_12072;
BgL_auxz00_12072 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37612L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2244); 
FAILURE(BgL_auxz00_12072,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4525,BgL_bucketzd2numzd2_2246,BgL_arg1828z00_2262); } } 
{ /* Llib/hash.scm 935 */
 bool_t BgL_test5056z00_12077;
{ /* Llib/hash.scm 935 */
 long BgL_n2z00_4528;
{ /* Llib/hash.scm 935 */
 obj_t BgL_tmpz00_12078;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_2248))
{ /* Llib/hash.scm 935 */
BgL_tmpz00_12078 = BgL_maxzd2bucketzd2lenz00_2248
; }  else 
{ 
 obj_t BgL_auxz00_12081;
BgL_auxz00_12081 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37681L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_maxzd2bucketzd2lenz00_2248); 
FAILURE(BgL_auxz00_12081,BFALSE,BFALSE);} 
BgL_n2z00_4528 = 
(long)CINT(BgL_tmpz00_12078); } 
BgL_test5056z00_12077 = 
(BgL_countz00_2257>BgL_n2z00_4528); } 
if(BgL_test5056z00_12077)
{ /* Llib/hash.scm 935 */
BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(BgL_tablez00_130); }  else 
{ /* Llib/hash.scm 935 */BFALSE; } } 
return BgL_objz00_133;}  else 
{ /* Llib/hash.scm 938 */
 bool_t BgL_test5058z00_12088;
{ /* Llib/hash.scm 938 */
 obj_t BgL_arg1837z00_2272;
{ /* Llib/hash.scm 938 */
 obj_t BgL_pairz00_4529;
if(
PAIRP(BgL_buckz00_2256))
{ /* Llib/hash.scm 938 */
BgL_pairz00_4529 = BgL_buckz00_2256; }  else 
{ 
 obj_t BgL_auxz00_12091;
BgL_auxz00_12091 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37777L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2256); 
FAILURE(BgL_auxz00_12091,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 938 */
 obj_t BgL_pairz00_4532;
{ /* Llib/hash.scm 938 */
 obj_t BgL_aux3605z00_6488;
BgL_aux3605z00_6488 = 
CAR(BgL_pairz00_4529); 
if(
PAIRP(BgL_aux3605z00_6488))
{ /* Llib/hash.scm 938 */
BgL_pairz00_4532 = BgL_aux3605z00_6488; }  else 
{ 
 obj_t BgL_auxz00_12098;
BgL_auxz00_12098 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37771L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3605z00_6488); 
FAILURE(BgL_auxz00_12098,BFALSE,BFALSE);} } 
BgL_arg1837z00_2272 = 
CAR(BgL_pairz00_4532); } } 
{ /* Llib/hash.scm 938 */
 obj_t BgL_eqtz00_4533;
{ /* Llib/hash.scm 938 */
 bool_t BgL_test5061z00_12103;
{ /* Llib/hash.scm 938 */
 obj_t BgL_tmpz00_12104;
{ /* Llib/hash.scm 938 */
 obj_t BgL_res2622z00_4540;
{ /* Llib/hash.scm 938 */
 obj_t BgL_aux3607z00_6490;
BgL_aux3607z00_6490 = 
STRUCT_KEY(BgL_tablez00_130); 
if(
SYMBOLP(BgL_aux3607z00_6490))
{ /* Llib/hash.scm 938 */
BgL_res2622z00_4540 = BgL_aux3607z00_6490; }  else 
{ 
 obj_t BgL_auxz00_12108;
BgL_auxz00_12108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37747L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3607z00_6490); 
FAILURE(BgL_auxz00_12108,BFALSE,BFALSE);} } 
BgL_tmpz00_12104 = BgL_res2622z00_4540; } 
BgL_test5061z00_12103 = 
(BgL_tmpz00_12104==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5061z00_12103)
{ /* Llib/hash.scm 938 */
 int BgL_tmpz00_12113;
BgL_tmpz00_12113 = 
(int)(3L); 
BgL_eqtz00_4533 = 
STRUCT_REF(BgL_tablez00_130, BgL_tmpz00_12113); }  else 
{ /* Llib/hash.scm 938 */
BgL_eqtz00_4533 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_130); } } 
if(
PROCEDUREP(BgL_eqtz00_4533))
{ /* Llib/hash.scm 938 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_4533, 2))
{ /* Llib/hash.scm 938 */
BgL_test5058z00_12088 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_4533, BgL_arg1837z00_2272, BgL_keyz00_131))
; }  else 
{ /* Llib/hash.scm 938 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4043z00zz__hashz00,BgL_eqtz00_4533);} }  else 
{ /* Llib/hash.scm 938 */
if(
(BgL_arg1837z00_2272==BgL_keyz00_131))
{ /* Llib/hash.scm 938 */
BgL_test5058z00_12088 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 938 */
if(
STRINGP(BgL_arg1837z00_2272))
{ /* Llib/hash.scm 938 */
if(
STRINGP(BgL_keyz00_131))
{ /* Llib/hash.scm 938 */
 long BgL_l1z00_4543;
BgL_l1z00_4543 = 
STRING_LENGTH(BgL_arg1837z00_2272); 
if(
(BgL_l1z00_4543==
STRING_LENGTH(BgL_keyz00_131)))
{ /* Llib/hash.scm 938 */
 int BgL_arg2114z00_4546;
{ /* Llib/hash.scm 938 */
 char * BgL_auxz00_12140; char * BgL_tmpz00_12138;
BgL_auxz00_12140 = 
BSTRING_TO_STRING(BgL_keyz00_131); 
BgL_tmpz00_12138 = 
BSTRING_TO_STRING(BgL_arg1837z00_2272); 
BgL_arg2114z00_4546 = 
memcmp(BgL_tmpz00_12138, BgL_auxz00_12140, BgL_l1z00_4543); } 
BgL_test5058z00_12088 = 
(
(long)(BgL_arg2114z00_4546)==0L); }  else 
{ /* Llib/hash.scm 938 */
BgL_test5058z00_12088 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 938 */
BgL_test5058z00_12088 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 938 */
BgL_test5058z00_12088 = ((bool_t)0)
; } } } } } 
if(BgL_test5058z00_12088)
{ /* Llib/hash.scm 939 */
 obj_t BgL_resz00_2267;
{ /* Llib/hash.scm 939 */
 obj_t BgL_arg1834z00_2269;
{ /* Llib/hash.scm 939 */
 obj_t BgL_pairz00_4552;
if(
PAIRP(BgL_buckz00_2256))
{ /* Llib/hash.scm 939 */
BgL_pairz00_4552 = BgL_buckz00_2256; }  else 
{ 
 obj_t BgL_auxz00_12147;
BgL_auxz00_12147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37814L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2256); 
FAILURE(BgL_auxz00_12147,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 939 */
 obj_t BgL_pairz00_4555;
{ /* Llib/hash.scm 939 */
 obj_t BgL_aux3612z00_6496;
BgL_aux3612z00_6496 = 
CAR(BgL_pairz00_4552); 
if(
PAIRP(BgL_aux3612z00_6496))
{ /* Llib/hash.scm 939 */
BgL_pairz00_4555 = BgL_aux3612z00_6496; }  else 
{ 
 obj_t BgL_auxz00_12154;
BgL_auxz00_12154 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37808L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3612z00_6496); 
FAILURE(BgL_auxz00_12154,BFALSE,BFALSE);} } 
BgL_arg1834z00_2269 = 
CDR(BgL_pairz00_4555); } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_132, 1))
{ /* Llib/hash.scm 939 */
BgL_resz00_2267 = 
BGL_PROCEDURE_CALL1(BgL_procz00_132, BgL_arg1834z00_2269); }  else 
{ /* Llib/hash.scm 939 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4046z00zz__hashz00,BgL_procz00_132);} } 
{ /* Llib/hash.scm 940 */
 obj_t BgL_arg1833z00_2268;
{ /* Llib/hash.scm 940 */
 obj_t BgL_pairz00_4556;
if(
PAIRP(BgL_buckz00_2256))
{ /* Llib/hash.scm 940 */
BgL_pairz00_4556 = BgL_buckz00_2256; }  else 
{ 
 obj_t BgL_auxz00_12168;
BgL_auxz00_12168 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37844L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2256); 
FAILURE(BgL_auxz00_12168,BFALSE,BFALSE);} 
BgL_arg1833z00_2268 = 
CAR(BgL_pairz00_4556); } 
{ /* Llib/hash.scm 940 */
 obj_t BgL_pairz00_4557;
if(
PAIRP(BgL_arg1833z00_2268))
{ /* Llib/hash.scm 940 */
BgL_pairz00_4557 = BgL_arg1833z00_2268; }  else 
{ 
 obj_t BgL_auxz00_12175;
BgL_auxz00_12175 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37848L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_arg1833z00_2268); 
FAILURE(BgL_auxz00_12175,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_4557, BgL_resz00_2267); } } 
return BgL_resz00_2267;}  else 
{ /* Llib/hash.scm 943 */
 obj_t BgL_arg1835z00_2270; long BgL_arg1836z00_2271;
{ /* Llib/hash.scm 943 */
 obj_t BgL_pairz00_4558;
if(
PAIRP(BgL_buckz00_2256))
{ /* Llib/hash.scm 943 */
BgL_pairz00_4558 = BgL_buckz00_2256; }  else 
{ 
 obj_t BgL_auxz00_12182;
BgL_auxz00_12182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(37889L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2256); 
FAILURE(BgL_auxz00_12182,BFALSE,BFALSE);} 
BgL_arg1835z00_2270 = 
CDR(BgL_pairz00_4558); } 
BgL_arg1836z00_2271 = 
(BgL_countz00_2257+1L); 
{ 
 long BgL_countz00_12189; obj_t BgL_buckz00_12188;
BgL_buckz00_12188 = BgL_arg1835z00_2270; 
BgL_countz00_12189 = BgL_arg1836z00_2271; 
BgL_countz00_2257 = BgL_countz00_12189; 
BgL_buckz00_2256 = BgL_buckz00_12188; 
goto BgL_zc3z04anonymousza31824ze3z87_2258;} } } } } } } } } } } 

}



/* hashtable-add! */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2addz12zc0zz__hashz00(obj_t BgL_tablez00_134, obj_t BgL_keyz00_135, obj_t BgL_p2z00_136, obj_t BgL_objz00_137, obj_t BgL_initz00_138)
{
{ /* Llib/hash.scm 948 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_134))
{ /* Llib/hash.scm 951 */
 obj_t BgL_auxz00_12192;
if(
STRINGP(BgL_keyz00_135))
{ /* Llib/hash.scm 951 */
BgL_auxz00_12192 = BgL_keyz00_135
; }  else 
{ 
 obj_t BgL_auxz00_12195;
BgL_auxz00_12195 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38301L), BGl_string4049z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_135); 
FAILURE(BgL_auxz00_12195,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(BgL_tablez00_134, BgL_auxz00_12192, BgL_p2z00_136, BgL_objz00_137, BgL_initz00_138);}  else 
{ /* Llib/hash.scm 950 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_134))
{ /* Llib/hash.scm 952 */
BGL_TAIL return 
BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(BgL_tablez00_134, BgL_keyz00_135, BgL_p2z00_136, BgL_objz00_137, BgL_initz00_138);}  else 
{ /* Llib/hash.scm 952 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2addz12z12zz__hashz00(BgL_tablez00_134, BgL_keyz00_135, BgL_p2z00_136, BgL_objz00_137, BgL_initz00_138);} } } 

}



/* &hashtable-add! */
obj_t BGl_z62hashtablezd2addz12za2zz__hashz00(obj_t BgL_envz00_5502, obj_t BgL_tablez00_5503, obj_t BgL_keyz00_5504, obj_t BgL_p2z00_5505, obj_t BgL_objz00_5506, obj_t BgL_initz00_5507)
{
{ /* Llib/hash.scm 948 */
{ /* Llib/hash.scm 950 */
 obj_t BgL_auxz00_12211; obj_t BgL_auxz00_12204;
if(
PROCEDUREP(BgL_p2z00_5505))
{ /* Llib/hash.scm 950 */
BgL_auxz00_12211 = BgL_p2z00_5505
; }  else 
{ 
 obj_t BgL_auxz00_12214;
BgL_auxz00_12214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38228L), BGl_string4050z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_p2z00_5505); 
FAILURE(BgL_auxz00_12214,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5503))
{ /* Llib/hash.scm 950 */
BgL_auxz00_12204 = BgL_tablez00_5503
; }  else 
{ 
 obj_t BgL_auxz00_12207;
BgL_auxz00_12207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38228L), BGl_string4050z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5503); 
FAILURE(BgL_auxz00_12207,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2addz12zc0zz__hashz00(BgL_auxz00_12204, BgL_keyz00_5504, BgL_auxz00_12211, BgL_objz00_5506, BgL_initz00_5507);} } 

}



/* open-string-hashtable-add! */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(obj_t BgL_tablez00_139, obj_t BgL_keyz00_140, obj_t BgL_procz00_141, obj_t BgL_objz00_142, obj_t BgL_initz00_143)
{
{ /* Llib/hash.scm 960 */
{ /* Llib/hash.scm 961 */
 obj_t BgL_siza7eza7_2277;
{ /* Llib/hash.scm 961 */
 bool_t BgL_test5080z00_12219;
{ /* Llib/hash.scm 961 */
 obj_t BgL_tmpz00_12220;
{ /* Llib/hash.scm 961 */
 obj_t BgL_res2623z00_4565;
{ /* Llib/hash.scm 961 */
 obj_t BgL_aux3627z00_6512;
BgL_aux3627z00_6512 = 
STRUCT_KEY(BgL_tablez00_139); 
if(
SYMBOLP(BgL_aux3627z00_6512))
{ /* Llib/hash.scm 961 */
BgL_res2623z00_4565 = BgL_aux3627z00_6512; }  else 
{ 
 obj_t BgL_auxz00_12224;
BgL_auxz00_12224 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38797L), BGl_string4051z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3627z00_6512); 
FAILURE(BgL_auxz00_12224,BFALSE,BFALSE);} } 
BgL_tmpz00_12220 = BgL_res2623z00_4565; } 
BgL_test5080z00_12219 = 
(BgL_tmpz00_12220==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5080z00_12219)
{ /* Llib/hash.scm 961 */
 int BgL_tmpz00_12229;
BgL_tmpz00_12229 = 
(int)(1L); 
BgL_siza7eza7_2277 = 
STRUCT_REF(BgL_tablez00_139, BgL_tmpz00_12229); }  else 
{ /* Llib/hash.scm 961 */
BgL_siza7eza7_2277 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_139); } } 
{ /* Llib/hash.scm 961 */
 obj_t BgL_bucketsz00_2278;
{ /* Llib/hash.scm 962 */
 bool_t BgL_test5082z00_12233;
{ /* Llib/hash.scm 962 */
 obj_t BgL_tmpz00_12234;
{ /* Llib/hash.scm 962 */
 obj_t BgL_res2624z00_4569;
{ /* Llib/hash.scm 962 */
 obj_t BgL_aux3629z00_6514;
BgL_aux3629z00_6514 = 
STRUCT_KEY(BgL_tablez00_139); 
if(
SYMBOLP(BgL_aux3629z00_6514))
{ /* Llib/hash.scm 962 */
BgL_res2624z00_4569 = BgL_aux3629z00_6514; }  else 
{ 
 obj_t BgL_auxz00_12238;
BgL_auxz00_12238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38844L), BGl_string4051z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3629z00_6514); 
FAILURE(BgL_auxz00_12238,BFALSE,BFALSE);} } 
BgL_tmpz00_12234 = BgL_res2624z00_4569; } 
BgL_test5082z00_12233 = 
(BgL_tmpz00_12234==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5082z00_12233)
{ /* Llib/hash.scm 962 */
 int BgL_tmpz00_12243;
BgL_tmpz00_12243 = 
(int)(2L); 
BgL_bucketsz00_2278 = 
STRUCT_REF(BgL_tablez00_139, BgL_tmpz00_12243); }  else 
{ /* Llib/hash.scm 962 */
BgL_bucketsz00_2278 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_139); } } 
{ /* Llib/hash.scm 962 */
 long BgL_hashz00_2279;
{ /* Llib/hash.scm 963 */
 long BgL_arg1862z00_2307;
BgL_arg1862z00_2307 = 
STRING_LENGTH(BgL_keyz00_140); 
BgL_hashz00_2279 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_140), 
(int)(0L), 
(int)(BgL_arg1862z00_2307)); } 
{ /* Llib/hash.scm 963 */

{ /* Llib/hash.scm 965 */
 long BgL_g1073z00_2280;
{ /* Llib/hash.scm 965 */
 long BgL_n1z00_4571; long BgL_n2z00_4572;
BgL_n1z00_4571 = BgL_hashz00_2279; 
{ /* Llib/hash.scm 965 */
 obj_t BgL_tmpz00_12252;
if(
INTEGERP(BgL_siza7eza7_2277))
{ /* Llib/hash.scm 965 */
BgL_tmpz00_12252 = BgL_siza7eza7_2277
; }  else 
{ 
 obj_t BgL_auxz00_12255;
BgL_auxz00_12255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38986L), BGl_string4051z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2277); 
FAILURE(BgL_auxz00_12255,BFALSE,BFALSE);} 
BgL_n2z00_4572 = 
(long)CINT(BgL_tmpz00_12252); } 
{ /* Llib/hash.scm 965 */
 bool_t BgL_test5085z00_12260;
{ /* Llib/hash.scm 965 */
 long BgL_arg2221z00_4574;
BgL_arg2221z00_4574 = 
(((BgL_n1z00_4571) | (BgL_n2z00_4572)) & -2147483648); 
BgL_test5085z00_12260 = 
(BgL_arg2221z00_4574==0L); } 
if(BgL_test5085z00_12260)
{ /* Llib/hash.scm 965 */
 int32_t BgL_arg2218z00_4575;
{ /* Llib/hash.scm 965 */
 int32_t BgL_arg2219z00_4576; int32_t BgL_arg2220z00_4577;
BgL_arg2219z00_4576 = 
(int32_t)(BgL_n1z00_4571); 
BgL_arg2220z00_4577 = 
(int32_t)(BgL_n2z00_4572); 
BgL_arg2218z00_4575 = 
(BgL_arg2219z00_4576%BgL_arg2220z00_4577); } 
{ /* Llib/hash.scm 965 */
 long BgL_arg2326z00_4582;
BgL_arg2326z00_4582 = 
(long)(BgL_arg2218z00_4575); 
BgL_g1073z00_2280 = 
(long)(BgL_arg2326z00_4582); } }  else 
{ /* Llib/hash.scm 965 */
BgL_g1073z00_2280 = 
(BgL_n1z00_4571%BgL_n2z00_4572); } } } 
{ 
 long BgL_offz00_2282; long BgL_iz00_2283;
BgL_offz00_2282 = BgL_g1073z00_2280; 
BgL_iz00_2283 = 1L; 
BgL_zc3z04anonymousza31841ze3z87_2284:
{ /* Llib/hash.scm 967 */
 long BgL_off3z00_2285;
BgL_off3z00_2285 = 
(BgL_offz00_2282*3L); 
{ /* Llib/hash.scm 968 */
 bool_t BgL_test5086z00_12270;
{ /* Llib/hash.scm 968 */
 obj_t BgL_vectorz00_4585;
if(
VECTORP(BgL_bucketsz00_2278))
{ /* Llib/hash.scm 968 */
BgL_vectorz00_4585 = BgL_bucketsz00_2278; }  else 
{ 
 obj_t BgL_auxz00_12273;
BgL_auxz00_12273 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39052L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2278); 
FAILURE(BgL_auxz00_12273,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 968 */
 bool_t BgL_test5088z00_12277;
{ /* Llib/hash.scm 968 */
 long BgL_tmpz00_12278;
BgL_tmpz00_12278 = 
VECTOR_LENGTH(BgL_vectorz00_4585); 
BgL_test5088z00_12277 = 
BOUND_CHECK(BgL_off3z00_2285, BgL_tmpz00_12278); } 
if(BgL_test5088z00_12277)
{ /* Llib/hash.scm 968 */
BgL_test5086z00_12270 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4585,BgL_off3z00_2285))
; }  else 
{ 
 obj_t BgL_auxz00_12283;
BgL_auxz00_12283 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39040L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4585, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4585)), 
(int)(BgL_off3z00_2285)); 
FAILURE(BgL_auxz00_12283,BFALSE,BFALSE);} } } 
if(BgL_test5086z00_12270)
{ /* Llib/hash.scm 969 */
 bool_t BgL_test5089z00_12290;
{ /* Llib/hash.scm 969 */
 obj_t BgL_arg1859z00_2304;
{ /* Llib/hash.scm 969 */
 obj_t BgL_vectorz00_4587;
if(
VECTORP(BgL_bucketsz00_2278))
{ /* Llib/hash.scm 969 */
BgL_vectorz00_4587 = BgL_bucketsz00_2278; }  else 
{ 
 obj_t BgL_auxz00_12293;
BgL_auxz00_12293 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39094L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2278); 
FAILURE(BgL_auxz00_12293,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 969 */
 bool_t BgL_test5091z00_12297;
{ /* Llib/hash.scm 969 */
 long BgL_tmpz00_12298;
BgL_tmpz00_12298 = 
VECTOR_LENGTH(BgL_vectorz00_4587); 
BgL_test5091z00_12297 = 
BOUND_CHECK(BgL_off3z00_2285, BgL_tmpz00_12298); } 
if(BgL_test5091z00_12297)
{ /* Llib/hash.scm 969 */
BgL_arg1859z00_2304 = 
VECTOR_REF(BgL_vectorz00_4587,BgL_off3z00_2285); }  else 
{ 
 obj_t BgL_auxz00_12302;
BgL_auxz00_12302 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39082L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4587, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4587)), 
(int)(BgL_off3z00_2285)); 
FAILURE(BgL_auxz00_12302,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 969 */
 obj_t BgL_string1z00_4589;
if(
STRINGP(BgL_arg1859z00_2304))
{ /* Llib/hash.scm 969 */
BgL_string1z00_4589 = BgL_arg1859z00_2304; }  else 
{ 
 obj_t BgL_auxz00_12311;
BgL_auxz00_12311 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39106L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1859z00_2304); 
FAILURE(BgL_auxz00_12311,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 969 */
 long BgL_l1z00_4591;
BgL_l1z00_4591 = 
STRING_LENGTH(BgL_string1z00_4589); 
if(
(BgL_l1z00_4591==
STRING_LENGTH(BgL_keyz00_140)))
{ /* Llib/hash.scm 969 */
 int BgL_arg2114z00_4594;
{ /* Llib/hash.scm 969 */
 char * BgL_auxz00_12321; char * BgL_tmpz00_12319;
BgL_auxz00_12321 = 
BSTRING_TO_STRING(BgL_keyz00_140); 
BgL_tmpz00_12319 = 
BSTRING_TO_STRING(BgL_string1z00_4589); 
BgL_arg2114z00_4594 = 
memcmp(BgL_tmpz00_12319, BgL_auxz00_12321, BgL_l1z00_4591); } 
BgL_test5089z00_12290 = 
(
(long)(BgL_arg2114z00_4594)==0L); }  else 
{ /* Llib/hash.scm 969 */
BgL_test5089z00_12290 = ((bool_t)0)
; } } } } 
if(BgL_test5089z00_12290)
{ /* Llib/hash.scm 970 */
 bool_t BgL_test5094z00_12326;
{ /* Llib/hash.scm 970 */
 long BgL_arg1852z00_2297;
BgL_arg1852z00_2297 = 
(BgL_off3z00_2285+2L); 
{ /* Llib/hash.scm 970 */
 obj_t BgL_vectorz00_4601;
if(
VECTORP(BgL_bucketsz00_2278))
{ /* Llib/hash.scm 970 */
BgL_vectorz00_4601 = BgL_bucketsz00_2278; }  else 
{ 
 obj_t BgL_auxz00_12330;
BgL_auxz00_12330 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39135L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2278); 
FAILURE(BgL_auxz00_12330,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 970 */
 bool_t BgL_test5096z00_12334;
{ /* Llib/hash.scm 970 */
 long BgL_tmpz00_12335;
BgL_tmpz00_12335 = 
VECTOR_LENGTH(BgL_vectorz00_4601); 
BgL_test5096z00_12334 = 
BOUND_CHECK(BgL_arg1852z00_2297, BgL_tmpz00_12335); } 
if(BgL_test5096z00_12334)
{ /* Llib/hash.scm 970 */
BgL_test5094z00_12326 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4601,BgL_arg1852z00_2297))
; }  else 
{ 
 obj_t BgL_auxz00_12340;
BgL_auxz00_12340 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39123L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4601, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4601)), 
(int)(BgL_arg1852z00_2297)); 
FAILURE(BgL_auxz00_12340,BFALSE,BFALSE);} } } } 
if(BgL_test5094z00_12326)
{ /* Llib/hash.scm 971 */
 obj_t BgL_ovalz00_2291;
{ /* Llib/hash.scm 971 */
 long BgL_arg1849z00_2294;
BgL_arg1849z00_2294 = 
(BgL_off3z00_2285+1L); 
{ /* Llib/hash.scm 971 */
 obj_t BgL_vectorz00_4604;
if(
VECTORP(BgL_bucketsz00_2278))
{ /* Llib/hash.scm 971 */
BgL_vectorz00_4604 = BgL_bucketsz00_2278; }  else 
{ 
 obj_t BgL_auxz00_12350;
BgL_auxz00_12350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39184L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2278); 
FAILURE(BgL_auxz00_12350,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 971 */
 bool_t BgL_test5098z00_12354;
{ /* Llib/hash.scm 971 */
 long BgL_tmpz00_12355;
BgL_tmpz00_12355 = 
VECTOR_LENGTH(BgL_vectorz00_4604); 
BgL_test5098z00_12354 = 
BOUND_CHECK(BgL_arg1849z00_2294, BgL_tmpz00_12355); } 
if(BgL_test5098z00_12354)
{ /* Llib/hash.scm 971 */
BgL_ovalz00_2291 = 
VECTOR_REF(BgL_vectorz00_4604,BgL_arg1849z00_2294); }  else 
{ 
 obj_t BgL_auxz00_12359;
BgL_auxz00_12359 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39172L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4604, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4604)), 
(int)(BgL_arg1849z00_2294)); 
FAILURE(BgL_auxz00_12359,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 972 */
 long BgL_arg1847z00_2292; obj_t BgL_arg1848z00_2293;
BgL_arg1847z00_2292 = 
(BgL_off3z00_2285+1L); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_141, 2))
{ /* Llib/hash.scm 973 */
BgL_arg1848z00_2293 = 
BGL_PROCEDURE_CALL2(BgL_procz00_141, BgL_ovalz00_2291, BgL_initz00_143); }  else 
{ /* Llib/hash.scm 973 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4052z00zz__hashz00,BgL_procz00_141);} 
{ /* Llib/hash.scm 972 */
 obj_t BgL_vectorz00_4607;
if(
VECTORP(BgL_bucketsz00_2278))
{ /* Llib/hash.scm 972 */
BgL_vectorz00_4607 = BgL_bucketsz00_2278; }  else 
{ 
 obj_t BgL_auxz00_12377;
BgL_auxz00_12377 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39227L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2278); 
FAILURE(BgL_auxz00_12377,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 972 */
 bool_t BgL_test5101z00_12381;
{ /* Llib/hash.scm 972 */
 long BgL_tmpz00_12382;
BgL_tmpz00_12382 = 
VECTOR_LENGTH(BgL_vectorz00_4607); 
BgL_test5101z00_12381 = 
BOUND_CHECK(BgL_arg1847z00_2292, BgL_tmpz00_12382); } 
if(BgL_test5101z00_12381)
{ /* Llib/hash.scm 972 */
return 
VECTOR_SET(BgL_vectorz00_4607,BgL_arg1847z00_2292,BgL_arg1848z00_2293);}  else 
{ 
 obj_t BgL_auxz00_12386;
BgL_auxz00_12386 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39214L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4607, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4607)), 
(int)(BgL_arg1847z00_2292)); 
FAILURE(BgL_auxz00_12386,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/hash.scm 974 */
 long BgL_arg1850z00_2295; obj_t BgL_arg1851z00_2296;
BgL_arg1850z00_2295 = 
(BgL_off3z00_2285+1L); 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_141, 2))
{ /* Llib/hash.scm 975 */
BgL_arg1851z00_2296 = 
BGL_PROCEDURE_CALL2(BgL_procz00_141, BgL_objz00_142, BgL_initz00_143); }  else 
{ /* Llib/hash.scm 975 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4055z00zz__hashz00,BgL_procz00_141);} 
{ /* Llib/hash.scm 974 */
 obj_t BgL_vectorz00_4610;
if(
VECTORP(BgL_bucketsz00_2278))
{ /* Llib/hash.scm 974 */
BgL_vectorz00_4610 = BgL_bucketsz00_2278; }  else 
{ 
 obj_t BgL_auxz00_12404;
BgL_auxz00_12404 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39292L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2278); 
FAILURE(BgL_auxz00_12404,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 974 */
 bool_t BgL_test5104z00_12408;
{ /* Llib/hash.scm 974 */
 long BgL_tmpz00_12409;
BgL_tmpz00_12409 = 
VECTOR_LENGTH(BgL_vectorz00_4610); 
BgL_test5104z00_12408 = 
BOUND_CHECK(BgL_arg1850z00_2295, BgL_tmpz00_12409); } 
if(BgL_test5104z00_12408)
{ /* Llib/hash.scm 974 */
return 
VECTOR_SET(BgL_vectorz00_4610,BgL_arg1850z00_2295,BgL_arg1851z00_2296);}  else 
{ 
 obj_t BgL_auxz00_12413;
BgL_auxz00_12413 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39279L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4610, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4610)), 
(int)(BgL_arg1850z00_2295)); 
FAILURE(BgL_auxz00_12413,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/hash.scm 976 */
 long BgL_noffz00_2298;
BgL_noffz00_2298 = 
(BgL_offz00_2282+
(BgL_iz00_2283*BgL_iz00_2283)); 
{ /* Llib/hash.scm 977 */
 bool_t BgL_test5105z00_12422;
{ /* Llib/hash.scm 977 */
 long BgL_n2z00_4617;
{ /* Llib/hash.scm 977 */
 obj_t BgL_tmpz00_12423;
if(
INTEGERP(BgL_siza7eza7_2277))
{ /* Llib/hash.scm 977 */
BgL_tmpz00_12423 = BgL_siza7eza7_2277
; }  else 
{ 
 obj_t BgL_auxz00_12426;
BgL_auxz00_12426 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39401L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2277); 
FAILURE(BgL_auxz00_12426,BFALSE,BFALSE);} 
BgL_n2z00_4617 = 
(long)CINT(BgL_tmpz00_12423); } 
BgL_test5105z00_12422 = 
(BgL_noffz00_2298>=BgL_n2z00_4617); } 
if(BgL_test5105z00_12422)
{ /* Llib/hash.scm 978 */
 long BgL_arg1854z00_2300; long BgL_arg1856z00_2301;
{ /* Llib/hash.scm 978 */
 long BgL_n1z00_4618; long BgL_n2z00_4619;
BgL_n1z00_4618 = BgL_noffz00_2298; 
{ /* Llib/hash.scm 978 */
 obj_t BgL_tmpz00_12432;
if(
INTEGERP(BgL_siza7eza7_2277))
{ /* Llib/hash.scm 978 */
BgL_tmpz00_12432 = BgL_siza7eza7_2277
; }  else 
{ 
 obj_t BgL_auxz00_12435;
BgL_auxz00_12435 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39437L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2277); 
FAILURE(BgL_auxz00_12435,BFALSE,BFALSE);} 
BgL_n2z00_4619 = 
(long)CINT(BgL_tmpz00_12432); } 
{ /* Llib/hash.scm 978 */
 bool_t BgL_test5108z00_12440;
{ /* Llib/hash.scm 978 */
 long BgL_arg2221z00_4621;
BgL_arg2221z00_4621 = 
(((BgL_n1z00_4618) | (BgL_n2z00_4619)) & -2147483648); 
BgL_test5108z00_12440 = 
(BgL_arg2221z00_4621==0L); } 
if(BgL_test5108z00_12440)
{ /* Llib/hash.scm 978 */
 int32_t BgL_arg2218z00_4622;
{ /* Llib/hash.scm 978 */
 int32_t BgL_arg2219z00_4623; int32_t BgL_arg2220z00_4624;
BgL_arg2219z00_4623 = 
(int32_t)(BgL_n1z00_4618); 
BgL_arg2220z00_4624 = 
(int32_t)(BgL_n2z00_4619); 
BgL_arg2218z00_4622 = 
(BgL_arg2219z00_4623%BgL_arg2220z00_4624); } 
{ /* Llib/hash.scm 978 */
 long BgL_arg2326z00_4629;
BgL_arg2326z00_4629 = 
(long)(BgL_arg2218z00_4622); 
BgL_arg1854z00_2300 = 
(long)(BgL_arg2326z00_4629); } }  else 
{ /* Llib/hash.scm 978 */
BgL_arg1854z00_2300 = 
(BgL_n1z00_4618%BgL_n2z00_4619); } } } 
BgL_arg1856z00_2301 = 
(BgL_iz00_2283+1L); 
{ 
 long BgL_iz00_12451; long BgL_offz00_12450;
BgL_offz00_12450 = BgL_arg1854z00_2300; 
BgL_iz00_12451 = BgL_arg1856z00_2301; 
BgL_iz00_2283 = BgL_iz00_12451; 
BgL_offz00_2282 = BgL_offz00_12450; 
goto BgL_zc3z04anonymousza31841ze3z87_2284;} }  else 
{ 
 long BgL_iz00_12453; long BgL_offz00_12452;
BgL_offz00_12452 = BgL_noffz00_2298; 
BgL_iz00_12453 = 
(BgL_iz00_2283+1L); 
BgL_iz00_2283 = BgL_iz00_12453; 
BgL_offz00_2282 = BgL_offz00_12452; 
goto BgL_zc3z04anonymousza31841ze3z87_2284;} } } }  else 
{ /* Llib/hash.scm 981 */
 obj_t BgL_arg1860z00_2305;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_141, 2))
{ /* Llib/hash.scm 981 */
BgL_arg1860z00_2305 = 
BGL_PROCEDURE_CALL2(BgL_procz00_141, BgL_objz00_142, BgL_initz00_143); }  else 
{ /* Llib/hash.scm 981 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4055z00zz__hashz00,BgL_procz00_141);} 
return 
BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(BgL_tablez00_139, BgL_keyz00_140, BgL_arg1860z00_2305, 
BINT(BgL_hashz00_2279));} } } } } } } } } } 

}



/* &open-string-hashtable-add! */
obj_t BGl_z62openzd2stringzd2hashtablezd2addz12za2zz__hashz00(obj_t BgL_envz00_5508, obj_t BgL_tablez00_5509, obj_t BgL_keyz00_5510, obj_t BgL_procz00_5511, obj_t BgL_objz00_5512, obj_t BgL_initz00_5513)
{
{ /* Llib/hash.scm 960 */
{ /* Llib/hash.scm 961 */
 obj_t BgL_auxz00_12479; obj_t BgL_auxz00_12472; obj_t BgL_auxz00_12465;
if(
PROCEDUREP(BgL_procz00_5511))
{ /* Llib/hash.scm 961 */
BgL_auxz00_12479 = BgL_procz00_5511
; }  else 
{ 
 obj_t BgL_auxz00_12482;
BgL_auxz00_12482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38784L), BGl_string4058z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_procz00_5511); 
FAILURE(BgL_auxz00_12482,BFALSE,BFALSE);} 
if(
STRINGP(BgL_keyz00_5510))
{ /* Llib/hash.scm 961 */
BgL_auxz00_12472 = BgL_keyz00_5510
; }  else 
{ 
 obj_t BgL_auxz00_12475;
BgL_auxz00_12475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38784L), BGl_string4058z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5510); 
FAILURE(BgL_auxz00_12475,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_5509))
{ /* Llib/hash.scm 961 */
BgL_auxz00_12465 = BgL_tablez00_5509
; }  else 
{ 
 obj_t BgL_auxz00_12468;
BgL_auxz00_12468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(38784L), BGl_string4058z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5509); 
FAILURE(BgL_auxz00_12468,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2addz12zc0zz__hashz00(BgL_auxz00_12465, BgL_auxz00_12472, BgL_auxz00_12479, BgL_objz00_5512, BgL_initz00_5513);} } 

}



/* plain-hashtable-add! */
obj_t BGl_plainzd2hashtablezd2addz12z12zz__hashz00(obj_t BgL_tablez00_144, obj_t BgL_keyz00_145, obj_t BgL_procz00_146, obj_t BgL_objz00_147, obj_t BgL_initz00_148)
{
{ /* Llib/hash.scm 986 */
{ /* Llib/hash.scm 987 */
 obj_t BgL_bucketsz00_2308;
{ /* Llib/hash.scm 987 */
 bool_t BgL_test5113z00_12487;
{ /* Llib/hash.scm 987 */
 obj_t BgL_tmpz00_12488;
{ /* Llib/hash.scm 987 */
 obj_t BgL_res2625z00_4636;
{ /* Llib/hash.scm 987 */
 obj_t BgL_aux3657z00_6545;
BgL_aux3657z00_6545 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3657z00_6545))
{ /* Llib/hash.scm 987 */
BgL_res2625z00_4636 = BgL_aux3657z00_6545; }  else 
{ 
 obj_t BgL_auxz00_12492;
BgL_auxz00_12492 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39883L), BGl_string4059z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3657z00_6545); 
FAILURE(BgL_auxz00_12492,BFALSE,BFALSE);} } 
BgL_tmpz00_12488 = BgL_res2625z00_4636; } 
BgL_test5113z00_12487 = 
(BgL_tmpz00_12488==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5113z00_12487)
{ /* Llib/hash.scm 987 */
 int BgL_tmpz00_12497;
BgL_tmpz00_12497 = 
(int)(2L); 
BgL_bucketsz00_2308 = 
STRUCT_REF(BgL_tablez00_144, BgL_tmpz00_12497); }  else 
{ /* Llib/hash.scm 987 */
BgL_bucketsz00_2308 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } 
{ /* Llib/hash.scm 987 */
 long BgL_bucketzd2lenzd2_2309;
{ /* Llib/hash.scm 988 */
 obj_t BgL_vectorz00_4637;
if(
VECTORP(BgL_bucketsz00_2308))
{ /* Llib/hash.scm 988 */
BgL_vectorz00_4637 = BgL_bucketsz00_2308; }  else 
{ 
 obj_t BgL_auxz00_12503;
BgL_auxz00_12503 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(39941L), BGl_string4059z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2308); 
FAILURE(BgL_auxz00_12503,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2309 = 
VECTOR_LENGTH(BgL_vectorz00_4637); } 
{ /* Llib/hash.scm 988 */
 long BgL_bucketzd2numzd2_2310;
{ /* Llib/hash.scm 989 */
 long BgL_arg1887z00_2340;
BgL_arg1887z00_2340 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_144, BgL_keyz00_145); 
{ /* Llib/hash.scm 989 */
 long BgL_n1z00_4638; long BgL_n2z00_4639;
BgL_n1z00_4638 = BgL_arg1887z00_2340; 
BgL_n2z00_4639 = BgL_bucketzd2lenzd2_2309; 
{ /* Llib/hash.scm 989 */
 bool_t BgL_test5116z00_12509;
{ /* Llib/hash.scm 989 */
 long BgL_arg2221z00_4641;
BgL_arg2221z00_4641 = 
(((BgL_n1z00_4638) | (BgL_n2z00_4639)) & -2147483648); 
BgL_test5116z00_12509 = 
(BgL_arg2221z00_4641==0L); } 
if(BgL_test5116z00_12509)
{ /* Llib/hash.scm 989 */
 int32_t BgL_arg2218z00_4642;
{ /* Llib/hash.scm 989 */
 int32_t BgL_arg2219z00_4643; int32_t BgL_arg2220z00_4644;
BgL_arg2219z00_4643 = 
(int32_t)(BgL_n1z00_4638); 
BgL_arg2220z00_4644 = 
(int32_t)(BgL_n2z00_4639); 
BgL_arg2218z00_4642 = 
(BgL_arg2219z00_4643%BgL_arg2220z00_4644); } 
{ /* Llib/hash.scm 989 */
 long BgL_arg2326z00_4649;
BgL_arg2326z00_4649 = 
(long)(BgL_arg2218z00_4642); 
BgL_bucketzd2numzd2_2310 = 
(long)(BgL_arg2326z00_4649); } }  else 
{ /* Llib/hash.scm 989 */
BgL_bucketzd2numzd2_2310 = 
(BgL_n1z00_4638%BgL_n2z00_4639); } } } } 
{ /* Llib/hash.scm 989 */
 obj_t BgL_bucketz00_2311;
{ /* Llib/hash.scm 990 */
 obj_t BgL_vectorz00_4651;
if(
VECTORP(BgL_bucketsz00_2308))
{ /* Llib/hash.scm 990 */
BgL_vectorz00_4651 = BgL_bucketsz00_2308; }  else 
{ 
 obj_t BgL_auxz00_12520;
BgL_auxz00_12520 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40051L), BGl_string4059z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2308); 
FAILURE(BgL_auxz00_12520,BFALSE,BFALSE);} 
BgL_bucketz00_2311 = 
VECTOR_REF(BgL_vectorz00_4651,BgL_bucketzd2numzd2_2310); } 
{ /* Llib/hash.scm 990 */
 obj_t BgL_maxzd2bucketzd2lenz00_2312;
{ /* Llib/hash.scm 991 */
 bool_t BgL_test5118z00_12525;
{ /* Llib/hash.scm 991 */
 obj_t BgL_tmpz00_12526;
{ /* Llib/hash.scm 991 */
 obj_t BgL_res2626z00_4656;
{ /* Llib/hash.scm 991 */
 obj_t BgL_aux3663z00_6551;
BgL_aux3663z00_6551 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3663z00_6551))
{ /* Llib/hash.scm 991 */
BgL_res2626z00_4656 = BgL_aux3663z00_6551; }  else 
{ 
 obj_t BgL_auxz00_12530;
BgL_auxz00_12530 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40091L), BGl_string4059z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3663z00_6551); 
FAILURE(BgL_auxz00_12530,BFALSE,BFALSE);} } 
BgL_tmpz00_12526 = BgL_res2626z00_4656; } 
BgL_test5118z00_12525 = 
(BgL_tmpz00_12526==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5118z00_12525)
{ /* Llib/hash.scm 991 */
 int BgL_tmpz00_12535;
BgL_tmpz00_12535 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_2312 = 
STRUCT_REF(BgL_tablez00_144, BgL_tmpz00_12535); }  else 
{ /* Llib/hash.scm 991 */
BgL_maxzd2bucketzd2lenz00_2312 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } 
{ /* Llib/hash.scm 991 */

if(
NULLP(BgL_bucketz00_2311))
{ /* Llib/hash.scm 993 */
 obj_t BgL_vz00_2314;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_146, 2))
{ /* Llib/hash.scm 993 */
BgL_vz00_2314 = 
BGL_PROCEDURE_CALL2(BgL_procz00_146, BgL_objz00_147, BgL_initz00_148); }  else 
{ /* Llib/hash.scm 993 */
FAILURE(BGl_string4060z00zz__hashz00,BGl_list4055z00zz__hashz00,BgL_procz00_146);} 
{ /* Llib/hash.scm 994 */
 long BgL_arg1864z00_2315;
{ /* Llib/hash.scm 994 */
 obj_t BgL_arg1866z00_2316;
{ /* Llib/hash.scm 994 */
 bool_t BgL_test5122z00_12549;
{ /* Llib/hash.scm 994 */
 obj_t BgL_tmpz00_12550;
{ /* Llib/hash.scm 994 */
 obj_t BgL_res2627z00_4660;
{ /* Llib/hash.scm 994 */
 obj_t BgL_aux3666z00_6555;
BgL_aux3666z00_6555 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3666z00_6555))
{ /* Llib/hash.scm 994 */
BgL_res2627z00_4660 = BgL_aux3666z00_6555; }  else 
{ 
 obj_t BgL_auxz00_12554;
BgL_auxz00_12554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40221L), BGl_string4059z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3666z00_6555); 
FAILURE(BgL_auxz00_12554,BFALSE,BFALSE);} } 
BgL_tmpz00_12550 = BgL_res2627z00_4660; } 
BgL_test5122z00_12549 = 
(BgL_tmpz00_12550==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5122z00_12549)
{ /* Llib/hash.scm 994 */
 int BgL_tmpz00_12559;
BgL_tmpz00_12559 = 
(int)(0L); 
BgL_arg1866z00_2316 = 
STRUCT_REF(BgL_tablez00_144, BgL_tmpz00_12559); }  else 
{ /* Llib/hash.scm 994 */
BgL_arg1866z00_2316 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } 
{ /* Llib/hash.scm 994 */
 long BgL_za71za7_4661;
{ /* Llib/hash.scm 994 */
 obj_t BgL_tmpz00_12563;
if(
INTEGERP(BgL_arg1866z00_2316))
{ /* Llib/hash.scm 994 */
BgL_tmpz00_12563 = BgL_arg1866z00_2316
; }  else 
{ 
 obj_t BgL_auxz00_12566;
BgL_auxz00_12566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40243L), BGl_string4059z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1866z00_2316); 
FAILURE(BgL_auxz00_12566,BFALSE,BFALSE);} 
BgL_za71za7_4661 = 
(long)CINT(BgL_tmpz00_12563); } 
BgL_arg1864z00_2315 = 
(BgL_za71za7_4661+1L); } } 
{ /* Llib/hash.scm 994 */
 bool_t BgL_test5125z00_12572;
{ /* Llib/hash.scm 994 */
 obj_t BgL_tmpz00_12573;
{ /* Llib/hash.scm 994 */
 obj_t BgL_res2628z00_4665;
{ /* Llib/hash.scm 994 */
 obj_t BgL_aux3669z00_6558;
BgL_aux3669z00_6558 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3669z00_6558))
{ /* Llib/hash.scm 994 */
BgL_res2628z00_4665 = BgL_aux3669z00_6558; }  else 
{ 
 obj_t BgL_auxz00_12577;
BgL_auxz00_12577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40188L), BGl_string4059z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3669z00_6558); 
FAILURE(BgL_auxz00_12577,BFALSE,BFALSE);} } 
BgL_tmpz00_12573 = BgL_res2628z00_4665; } 
BgL_test5125z00_12572 = 
(BgL_tmpz00_12573==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5125z00_12572)
{ /* Llib/hash.scm 994 */
 obj_t BgL_auxz00_12584; int BgL_tmpz00_12582;
BgL_auxz00_12584 = 
BINT(BgL_arg1864z00_2315); 
BgL_tmpz00_12582 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_144, BgL_tmpz00_12582, BgL_auxz00_12584); }  else 
{ /* Llib/hash.scm 994 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } } 
{ /* Llib/hash.scm 995 */
 obj_t BgL_arg1868z00_2317;
{ /* Llib/hash.scm 995 */
 obj_t BgL_arg1869z00_2318;
BgL_arg1869z00_2318 = 
MAKE_YOUNG_PAIR(BgL_keyz00_145, BgL_vz00_2314); 
{ /* Llib/hash.scm 995 */
 obj_t BgL_list1870z00_2319;
BgL_list1870z00_2319 = 
MAKE_YOUNG_PAIR(BgL_arg1869z00_2318, BNIL); 
BgL_arg1868z00_2317 = BgL_list1870z00_2319; } } 
{ /* Llib/hash.scm 995 */
 obj_t BgL_vectorz00_4667;
if(
VECTORP(BgL_bucketsz00_2308))
{ /* Llib/hash.scm 995 */
BgL_vectorz00_4667 = BgL_bucketsz00_2308; }  else 
{ 
 obj_t BgL_auxz00_12592;
BgL_auxz00_12592 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40271L), BGl_string4059z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2308); 
FAILURE(BgL_auxz00_12592,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4667,BgL_bucketzd2numzd2_2310,BgL_arg1868z00_2317); } } 
return BgL_vz00_2314;}  else 
{ 
 obj_t BgL_buckz00_2321; long BgL_countz00_2322;
BgL_buckz00_2321 = BgL_bucketz00_2311; 
BgL_countz00_2322 = 0L; 
BgL_zc3z04anonymousza31871ze3z87_2323:
if(
NULLP(BgL_buckz00_2321))
{ /* Llib/hash.scm 1001 */
 obj_t BgL_vz00_2325;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_146, 2))
{ /* Llib/hash.scm 1001 */
BgL_vz00_2325 = 
BGL_PROCEDURE_CALL2(BgL_procz00_146, BgL_objz00_147, BgL_initz00_148); }  else 
{ /* Llib/hash.scm 1001 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4055z00zz__hashz00,BgL_procz00_146);} 
{ /* Llib/hash.scm 1002 */
 long BgL_arg1873z00_2326;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_arg1874z00_2327;
{ /* Llib/hash.scm 1002 */
 bool_t BgL_test5130z00_12607;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_tmpz00_12608;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_res2630z00_4672;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_aux3674z00_6564;
BgL_aux3674z00_6564 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3674z00_6564))
{ /* Llib/hash.scm 1002 */
BgL_res2630z00_4672 = BgL_aux3674z00_6564; }  else 
{ 
 obj_t BgL_auxz00_12612;
BgL_auxz00_12612 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40463L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3674z00_6564); 
FAILURE(BgL_auxz00_12612,BFALSE,BFALSE);} } 
BgL_tmpz00_12608 = BgL_res2630z00_4672; } 
BgL_test5130z00_12607 = 
(BgL_tmpz00_12608==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5130z00_12607)
{ /* Llib/hash.scm 1002 */
 int BgL_tmpz00_12617;
BgL_tmpz00_12617 = 
(int)(0L); 
BgL_arg1874z00_2327 = 
STRUCT_REF(BgL_tablez00_144, BgL_tmpz00_12617); }  else 
{ /* Llib/hash.scm 1002 */
BgL_arg1874z00_2327 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } 
{ /* Llib/hash.scm 1002 */
 long BgL_za71za7_4673;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_tmpz00_12621;
if(
INTEGERP(BgL_arg1874z00_2327))
{ /* Llib/hash.scm 1002 */
BgL_tmpz00_12621 = BgL_arg1874z00_2327
; }  else 
{ 
 obj_t BgL_auxz00_12624;
BgL_auxz00_12624 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40485L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1874z00_2327); 
FAILURE(BgL_auxz00_12624,BFALSE,BFALSE);} 
BgL_za71za7_4673 = 
(long)CINT(BgL_tmpz00_12621); } 
BgL_arg1873z00_2326 = 
(BgL_za71za7_4673+1L); } } 
{ /* Llib/hash.scm 1002 */
 bool_t BgL_test5133z00_12630;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_tmpz00_12631;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_res2631z00_4677;
{ /* Llib/hash.scm 1002 */
 obj_t BgL_aux3677z00_6567;
BgL_aux3677z00_6567 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3677z00_6567))
{ /* Llib/hash.scm 1002 */
BgL_res2631z00_4677 = BgL_aux3677z00_6567; }  else 
{ 
 obj_t BgL_auxz00_12635;
BgL_auxz00_12635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40430L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3677z00_6567); 
FAILURE(BgL_auxz00_12635,BFALSE,BFALSE);} } 
BgL_tmpz00_12631 = BgL_res2631z00_4677; } 
BgL_test5133z00_12630 = 
(BgL_tmpz00_12631==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5133z00_12630)
{ /* Llib/hash.scm 1002 */
 obj_t BgL_auxz00_12642; int BgL_tmpz00_12640;
BgL_auxz00_12642 = 
BINT(BgL_arg1873z00_2326); 
BgL_tmpz00_12640 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_144, BgL_tmpz00_12640, BgL_auxz00_12642); }  else 
{ /* Llib/hash.scm 1002 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } } 
{ /* Llib/hash.scm 1003 */
 obj_t BgL_arg1875z00_2328;
{ /* Llib/hash.scm 1003 */
 obj_t BgL_arg1876z00_2329;
BgL_arg1876z00_2329 = 
MAKE_YOUNG_PAIR(BgL_keyz00_145, BgL_vz00_2325); 
BgL_arg1875z00_2328 = 
MAKE_YOUNG_PAIR(BgL_arg1876z00_2329, BgL_bucketz00_2311); } 
{ /* Llib/hash.scm 1003 */
 obj_t BgL_vectorz00_4678;
if(
VECTORP(BgL_bucketsz00_2308))
{ /* Llib/hash.scm 1003 */
BgL_vectorz00_4678 = BgL_bucketsz00_2308; }  else 
{ 
 obj_t BgL_auxz00_12650;
BgL_auxz00_12650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40513L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2308); 
FAILURE(BgL_auxz00_12650,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4678,BgL_bucketzd2numzd2_2310,BgL_arg1875z00_2328); } } 
{ /* Llib/hash.scm 1004 */
 bool_t BgL_test5136z00_12655;
{ /* Llib/hash.scm 1004 */
 long BgL_n2z00_4681;
{ /* Llib/hash.scm 1004 */
 obj_t BgL_tmpz00_12656;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_2312))
{ /* Llib/hash.scm 1004 */
BgL_tmpz00_12656 = BgL_maxzd2bucketzd2lenz00_2312
; }  else 
{ 
 obj_t BgL_auxz00_12659;
BgL_auxz00_12659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40583L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_maxzd2bucketzd2lenz00_2312); 
FAILURE(BgL_auxz00_12659,BFALSE,BFALSE);} 
BgL_n2z00_4681 = 
(long)CINT(BgL_tmpz00_12656); } 
BgL_test5136z00_12655 = 
(BgL_countz00_2322>BgL_n2z00_4681); } 
if(BgL_test5136z00_12655)
{ /* Llib/hash.scm 1004 */
BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(BgL_tablez00_144); }  else 
{ /* Llib/hash.scm 1004 */BFALSE; } } 
return BgL_vz00_2325;}  else 
{ /* Llib/hash.scm 1007 */
 bool_t BgL_test5138z00_12666;
{ /* Llib/hash.scm 1007 */
 obj_t BgL_arg1885z00_2338;
{ /* Llib/hash.scm 1007 */
 obj_t BgL_pairz00_4682;
if(
PAIRP(BgL_buckz00_2321))
{ /* Llib/hash.scm 1007 */
BgL_pairz00_4682 = BgL_buckz00_2321; }  else 
{ 
 obj_t BgL_auxz00_12669;
BgL_auxz00_12669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40684L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2321); 
FAILURE(BgL_auxz00_12669,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1007 */
 obj_t BgL_pairz00_4685;
{ /* Llib/hash.scm 1007 */
 obj_t BgL_aux3684z00_6574;
BgL_aux3684z00_6574 = 
CAR(BgL_pairz00_4682); 
if(
PAIRP(BgL_aux3684z00_6574))
{ /* Llib/hash.scm 1007 */
BgL_pairz00_4685 = BgL_aux3684z00_6574; }  else 
{ 
 obj_t BgL_auxz00_12676;
BgL_auxz00_12676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40678L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3684z00_6574); 
FAILURE(BgL_auxz00_12676,BFALSE,BFALSE);} } 
BgL_arg1885z00_2338 = 
CAR(BgL_pairz00_4685); } } 
{ /* Llib/hash.scm 1007 */
 obj_t BgL_eqtz00_4686;
{ /* Llib/hash.scm 1007 */
 bool_t BgL_test5141z00_12681;
{ /* Llib/hash.scm 1007 */
 obj_t BgL_tmpz00_12682;
{ /* Llib/hash.scm 1007 */
 obj_t BgL_res2632z00_4693;
{ /* Llib/hash.scm 1007 */
 obj_t BgL_aux3686z00_6576;
BgL_aux3686z00_6576 = 
STRUCT_KEY(BgL_tablez00_144); 
if(
SYMBOLP(BgL_aux3686z00_6576))
{ /* Llib/hash.scm 1007 */
BgL_res2632z00_4693 = BgL_aux3686z00_6576; }  else 
{ 
 obj_t BgL_auxz00_12686;
BgL_auxz00_12686 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40654L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3686z00_6576); 
FAILURE(BgL_auxz00_12686,BFALSE,BFALSE);} } 
BgL_tmpz00_12682 = BgL_res2632z00_4693; } 
BgL_test5141z00_12681 = 
(BgL_tmpz00_12682==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5141z00_12681)
{ /* Llib/hash.scm 1007 */
 int BgL_tmpz00_12691;
BgL_tmpz00_12691 = 
(int)(3L); 
BgL_eqtz00_4686 = 
STRUCT_REF(BgL_tablez00_144, BgL_tmpz00_12691); }  else 
{ /* Llib/hash.scm 1007 */
BgL_eqtz00_4686 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_144); } } 
if(
PROCEDUREP(BgL_eqtz00_4686))
{ /* Llib/hash.scm 1007 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_4686, 2))
{ /* Llib/hash.scm 1007 */
BgL_test5138z00_12666 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_4686, BgL_arg1885z00_2338, BgL_keyz00_145))
; }  else 
{ /* Llib/hash.scm 1007 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4061z00zz__hashz00,BgL_eqtz00_4686);} }  else 
{ /* Llib/hash.scm 1007 */
if(
(BgL_arg1885z00_2338==BgL_keyz00_145))
{ /* Llib/hash.scm 1007 */
BgL_test5138z00_12666 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 1007 */
if(
STRINGP(BgL_arg1885z00_2338))
{ /* Llib/hash.scm 1007 */
if(
STRINGP(BgL_keyz00_145))
{ /* Llib/hash.scm 1007 */
 long BgL_l1z00_4696;
BgL_l1z00_4696 = 
STRING_LENGTH(BgL_arg1885z00_2338); 
if(
(BgL_l1z00_4696==
STRING_LENGTH(BgL_keyz00_145)))
{ /* Llib/hash.scm 1007 */
 int BgL_arg2114z00_4699;
{ /* Llib/hash.scm 1007 */
 char * BgL_auxz00_12718; char * BgL_tmpz00_12716;
BgL_auxz00_12718 = 
BSTRING_TO_STRING(BgL_keyz00_145); 
BgL_tmpz00_12716 = 
BSTRING_TO_STRING(BgL_arg1885z00_2338); 
BgL_arg2114z00_4699 = 
memcmp(BgL_tmpz00_12716, BgL_auxz00_12718, BgL_l1z00_4696); } 
BgL_test5138z00_12666 = 
(
(long)(BgL_arg2114z00_4699)==0L); }  else 
{ /* Llib/hash.scm 1007 */
BgL_test5138z00_12666 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 1007 */
BgL_test5138z00_12666 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 1007 */
BgL_test5138z00_12666 = ((bool_t)0)
; } } } } } 
if(BgL_test5138z00_12666)
{ /* Llib/hash.scm 1008 */
 obj_t BgL_resz00_2333;
{ /* Llib/hash.scm 1008 */
 obj_t BgL_arg1882z00_2335;
{ /* Llib/hash.scm 1008 */
 obj_t BgL_pairz00_4705;
if(
PAIRP(BgL_buckz00_2321))
{ /* Llib/hash.scm 1008 */
BgL_pairz00_4705 = BgL_buckz00_2321; }  else 
{ 
 obj_t BgL_auxz00_12725;
BgL_auxz00_12725 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40725L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2321); 
FAILURE(BgL_auxz00_12725,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1008 */
 obj_t BgL_pairz00_4708;
{ /* Llib/hash.scm 1008 */
 obj_t BgL_aux3691z00_6582;
BgL_aux3691z00_6582 = 
CAR(BgL_pairz00_4705); 
if(
PAIRP(BgL_aux3691z00_6582))
{ /* Llib/hash.scm 1008 */
BgL_pairz00_4708 = BgL_aux3691z00_6582; }  else 
{ 
 obj_t BgL_auxz00_12732;
BgL_auxz00_12732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40719L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3691z00_6582); 
FAILURE(BgL_auxz00_12732,BFALSE,BFALSE);} } 
BgL_arg1882z00_2335 = 
CDR(BgL_pairz00_4708); } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_146, 2))
{ /* Llib/hash.scm 1008 */
BgL_resz00_2333 = 
BGL_PROCEDURE_CALL2(BgL_procz00_146, BgL_objz00_147, BgL_arg1882z00_2335); }  else 
{ /* Llib/hash.scm 1008 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4064z00zz__hashz00,BgL_procz00_146);} } 
{ /* Llib/hash.scm 1009 */
 obj_t BgL_arg1880z00_2334;
{ /* Llib/hash.scm 1009 */
 obj_t BgL_pairz00_4709;
if(
PAIRP(BgL_buckz00_2321))
{ /* Llib/hash.scm 1009 */
BgL_pairz00_4709 = BgL_buckz00_2321; }  else 
{ 
 obj_t BgL_auxz00_12747;
BgL_auxz00_12747 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40755L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2321); 
FAILURE(BgL_auxz00_12747,BFALSE,BFALSE);} 
BgL_arg1880z00_2334 = 
CAR(BgL_pairz00_4709); } 
{ /* Llib/hash.scm 1009 */
 obj_t BgL_pairz00_4710;
if(
PAIRP(BgL_arg1880z00_2334))
{ /* Llib/hash.scm 1009 */
BgL_pairz00_4710 = BgL_arg1880z00_2334; }  else 
{ 
 obj_t BgL_auxz00_12754;
BgL_auxz00_12754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40759L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_arg1880z00_2334); 
FAILURE(BgL_auxz00_12754,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_4710, BgL_resz00_2333); } } 
return BgL_resz00_2333;}  else 
{ /* Llib/hash.scm 1012 */
 obj_t BgL_arg1883z00_2336; long BgL_arg1884z00_2337;
{ /* Llib/hash.scm 1012 */
 obj_t BgL_pairz00_4711;
if(
PAIRP(BgL_buckz00_2321))
{ /* Llib/hash.scm 1012 */
BgL_pairz00_4711 = BgL_buckz00_2321; }  else 
{ 
 obj_t BgL_auxz00_12761;
BgL_auxz00_12761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(40800L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_buckz00_2321); 
FAILURE(BgL_auxz00_12761,BFALSE,BFALSE);} 
BgL_arg1883z00_2336 = 
CDR(BgL_pairz00_4711); } 
BgL_arg1884z00_2337 = 
(BgL_countz00_2322+1L); 
{ 
 long BgL_countz00_12768; obj_t BgL_buckz00_12767;
BgL_buckz00_12767 = BgL_arg1883z00_2336; 
BgL_countz00_12768 = BgL_arg1884z00_2337; 
BgL_countz00_2322 = BgL_countz00_12768; 
BgL_buckz00_2321 = BgL_buckz00_12767; 
goto BgL_zc3z04anonymousza31871ze3z87_2323;} } } } } } } } } } } 

}



/* hashtable-remove! */
BGL_EXPORTED_DEF bool_t BGl_hashtablezd2removez12zc0zz__hashz00(obj_t BgL_tablez00_149, obj_t BgL_keyz00_150)
{
{ /* Llib/hash.scm 1017 */
if(
BGl_hashtablezd2openzd2stringzf3zf3zz__hashz00(BgL_tablez00_149))
{ /* Llib/hash.scm 1020 */
 obj_t BgL_tmpz00_12771;
{ /* Llib/hash.scm 1020 */
 obj_t BgL_auxz00_12772;
if(
STRINGP(BgL_keyz00_150))
{ /* Llib/hash.scm 1020 */
BgL_auxz00_12772 = BgL_keyz00_150
; }  else 
{ 
 obj_t BgL_auxz00_12775;
BgL_auxz00_12775 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41195L), BGl_string4067z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_150); 
FAILURE(BgL_auxz00_12775,BFALSE,BFALSE);} 
BgL_tmpz00_12771 = 
BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00(BgL_tablez00_149, BgL_auxz00_12772); } 
return 
CBOOL(BgL_tmpz00_12771);}  else 
{ /* Llib/hash.scm 1019 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_149))
{ /* Llib/hash.scm 1021 */
return 
CBOOL(
BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(BgL_tablez00_149, BgL_keyz00_150));}  else 
{ /* Llib/hash.scm 1021 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2removez12z12zz__hashz00(BgL_tablez00_149, BgL_keyz00_150);} } } 

}



/* &hashtable-remove! */
obj_t BGl_z62hashtablezd2removez12za2zz__hashz00(obj_t BgL_envz00_5514, obj_t BgL_tablez00_5515, obj_t BgL_keyz00_5516)
{
{ /* Llib/hash.scm 1017 */
{ /* Llib/hash.scm 1019 */
 bool_t BgL_tmpz00_12786;
{ /* Llib/hash.scm 1019 */
 obj_t BgL_auxz00_12787;
if(
STRUCTP(BgL_tablez00_5515))
{ /* Llib/hash.scm 1019 */
BgL_auxz00_12787 = BgL_tablez00_5515
; }  else 
{ 
 obj_t BgL_auxz00_12790;
BgL_auxz00_12790 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41119L), BGl_string4068z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5515); 
FAILURE(BgL_auxz00_12790,BFALSE,BFALSE);} 
BgL_tmpz00_12786 = 
BGl_hashtablezd2removez12zc0zz__hashz00(BgL_auxz00_12787, BgL_keyz00_5516); } 
return 
BBOOL(BgL_tmpz00_12786);} } 

}



/* open-string-hashtable-remove! */
BGL_EXPORTED_DEF obj_t BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00(obj_t BgL_tz00_151, obj_t BgL_keyz00_152)
{
{ /* Llib/hash.scm 1029 */
{ /* Llib/hash.scm 1030 */
 obj_t BgL_siza7eza7_2343;
{ /* Llib/hash.scm 1030 */
 bool_t BgL_test5159z00_12796;
{ /* Llib/hash.scm 1030 */
 obj_t BgL_tmpz00_12797;
{ /* Llib/hash.scm 1030 */
 obj_t BgL_res2633z00_4718;
{ /* Llib/hash.scm 1030 */
 obj_t BgL_aux3704z00_6596;
BgL_aux3704z00_6596 = 
STRUCT_KEY(BgL_tz00_151); 
if(
SYMBOLP(BgL_aux3704z00_6596))
{ /* Llib/hash.scm 1030 */
BgL_res2633z00_4718 = BgL_aux3704z00_6596; }  else 
{ 
 obj_t BgL_auxz00_12801;
BgL_auxz00_12801 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41618L), BGl_string4069z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3704z00_6596); 
FAILURE(BgL_auxz00_12801,BFALSE,BFALSE);} } 
BgL_tmpz00_12797 = BgL_res2633z00_4718; } 
BgL_test5159z00_12796 = 
(BgL_tmpz00_12797==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5159z00_12796)
{ /* Llib/hash.scm 1030 */
 int BgL_tmpz00_12806;
BgL_tmpz00_12806 = 
(int)(1L); 
BgL_siza7eza7_2343 = 
STRUCT_REF(BgL_tz00_151, BgL_tmpz00_12806); }  else 
{ /* Llib/hash.scm 1030 */
BgL_siza7eza7_2343 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_151); } } 
{ /* Llib/hash.scm 1030 */
 obj_t BgL_bucketsz00_2344;
{ /* Llib/hash.scm 1031 */
 bool_t BgL_test5161z00_12810;
{ /* Llib/hash.scm 1031 */
 obj_t BgL_tmpz00_12811;
{ /* Llib/hash.scm 1031 */
 obj_t BgL_res2634z00_4722;
{ /* Llib/hash.scm 1031 */
 obj_t BgL_aux3706z00_6598;
BgL_aux3706z00_6598 = 
STRUCT_KEY(BgL_tz00_151); 
if(
SYMBOLP(BgL_aux3706z00_6598))
{ /* Llib/hash.scm 1031 */
BgL_res2634z00_4722 = BgL_aux3706z00_6598; }  else 
{ 
 obj_t BgL_auxz00_12815;
BgL_auxz00_12815 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41661L), BGl_string4069z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3706z00_6598); 
FAILURE(BgL_auxz00_12815,BFALSE,BFALSE);} } 
BgL_tmpz00_12811 = BgL_res2634z00_4722; } 
BgL_test5161z00_12810 = 
(BgL_tmpz00_12811==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5161z00_12810)
{ /* Llib/hash.scm 1031 */
 int BgL_tmpz00_12820;
BgL_tmpz00_12820 = 
(int)(2L); 
BgL_bucketsz00_2344 = 
STRUCT_REF(BgL_tz00_151, BgL_tmpz00_12820); }  else 
{ /* Llib/hash.scm 1031 */
BgL_bucketsz00_2344 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_151); } } 
{ /* Llib/hash.scm 1031 */
 long BgL_hashz00_2345;
{ /* Llib/hash.scm 1032 */
 long BgL_arg1904z00_2365;
BgL_arg1904z00_2365 = 
STRING_LENGTH(BgL_keyz00_152); 
BgL_hashz00_2345 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_152), 
(int)(0L), 
(int)(BgL_arg1904z00_2365)); } 
{ /* Llib/hash.scm 1032 */

{ /* Llib/hash.scm 1034 */
 long BgL_g1074z00_2346;
{ /* Llib/hash.scm 1034 */
 long BgL_n1z00_4724; long BgL_n2z00_4725;
BgL_n1z00_4724 = BgL_hashz00_2345; 
{ /* Llib/hash.scm 1034 */
 obj_t BgL_tmpz00_12829;
if(
INTEGERP(BgL_siza7eza7_2343))
{ /* Llib/hash.scm 1034 */
BgL_tmpz00_12829 = BgL_siza7eza7_2343
; }  else 
{ 
 obj_t BgL_auxz00_12832;
BgL_auxz00_12832 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41799L), BGl_string4069z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2343); 
FAILURE(BgL_auxz00_12832,BFALSE,BFALSE);} 
BgL_n2z00_4725 = 
(long)CINT(BgL_tmpz00_12829); } 
{ /* Llib/hash.scm 1034 */
 bool_t BgL_test5164z00_12837;
{ /* Llib/hash.scm 1034 */
 long BgL_arg2221z00_4727;
BgL_arg2221z00_4727 = 
(((BgL_n1z00_4724) | (BgL_n2z00_4725)) & -2147483648); 
BgL_test5164z00_12837 = 
(BgL_arg2221z00_4727==0L); } 
if(BgL_test5164z00_12837)
{ /* Llib/hash.scm 1034 */
 int32_t BgL_arg2218z00_4728;
{ /* Llib/hash.scm 1034 */
 int32_t BgL_arg2219z00_4729; int32_t BgL_arg2220z00_4730;
BgL_arg2219z00_4729 = 
(int32_t)(BgL_n1z00_4724); 
BgL_arg2220z00_4730 = 
(int32_t)(BgL_n2z00_4725); 
BgL_arg2218z00_4728 = 
(BgL_arg2219z00_4729%BgL_arg2220z00_4730); } 
{ /* Llib/hash.scm 1034 */
 long BgL_arg2326z00_4735;
BgL_arg2326z00_4735 = 
(long)(BgL_arg2218z00_4728); 
BgL_g1074z00_2346 = 
(long)(BgL_arg2326z00_4735); } }  else 
{ /* Llib/hash.scm 1034 */
BgL_g1074z00_2346 = 
(BgL_n1z00_4724%BgL_n2z00_4725); } } } 
{ 
 long BgL_offz00_2348; long BgL_iz00_2349;
BgL_offz00_2348 = BgL_g1074z00_2346; 
BgL_iz00_2349 = 1L; 
BgL_zc3z04anonymousza31890ze3z87_2350:
{ /* Llib/hash.scm 1036 */
 long BgL_off3z00_2351;
BgL_off3z00_2351 = 
(BgL_offz00_2348*3L); 
{ /* Llib/hash.scm 1037 */
 bool_t BgL_test5165z00_12847;
{ /* Llib/hash.scm 1037 */
 obj_t BgL_vectorz00_4738;
if(
VECTORP(BgL_bucketsz00_2344))
{ /* Llib/hash.scm 1037 */
BgL_vectorz00_4738 = BgL_bucketsz00_2344; }  else 
{ 
 obj_t BgL_auxz00_12850;
BgL_auxz00_12850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41867L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2344); 
FAILURE(BgL_auxz00_12850,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1037 */
 bool_t BgL_test5167z00_12854;
{ /* Llib/hash.scm 1037 */
 long BgL_tmpz00_12855;
BgL_tmpz00_12855 = 
VECTOR_LENGTH(BgL_vectorz00_4738); 
BgL_test5167z00_12854 = 
BOUND_CHECK(BgL_off3z00_2351, BgL_tmpz00_12855); } 
if(BgL_test5167z00_12854)
{ /* Llib/hash.scm 1037 */
BgL_test5165z00_12847 = 
CBOOL(
VECTOR_REF(BgL_vectorz00_4738,BgL_off3z00_2351))
; }  else 
{ 
 obj_t BgL_auxz00_12860;
BgL_auxz00_12860 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41855L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4738, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4738)), 
(int)(BgL_off3z00_2351)); 
FAILURE(BgL_auxz00_12860,BFALSE,BFALSE);} } } 
if(BgL_test5165z00_12847)
{ /* Llib/hash.scm 1038 */
 bool_t BgL_test5168z00_12867;
{ /* Llib/hash.scm 1038 */
 obj_t BgL_arg1903z00_2363;
{ /* Llib/hash.scm 1038 */
 obj_t BgL_vectorz00_4740;
if(
VECTORP(BgL_bucketsz00_2344))
{ /* Llib/hash.scm 1038 */
BgL_vectorz00_4740 = BgL_bucketsz00_2344; }  else 
{ 
 obj_t BgL_auxz00_12870;
BgL_auxz00_12870 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41915L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2344); 
FAILURE(BgL_auxz00_12870,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1038 */
 bool_t BgL_test5170z00_12874;
{ /* Llib/hash.scm 1038 */
 long BgL_tmpz00_12875;
BgL_tmpz00_12875 = 
VECTOR_LENGTH(BgL_vectorz00_4740); 
BgL_test5170z00_12874 = 
BOUND_CHECK(BgL_off3z00_2351, BgL_tmpz00_12875); } 
if(BgL_test5170z00_12874)
{ /* Llib/hash.scm 1038 */
BgL_arg1903z00_2363 = 
VECTOR_REF(BgL_vectorz00_4740,BgL_off3z00_2351); }  else 
{ 
 obj_t BgL_auxz00_12879;
BgL_auxz00_12879 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41903L), BGl_string3881z00zz__hashz00, BgL_vectorz00_4740, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4740)), 
(int)(BgL_off3z00_2351)); 
FAILURE(BgL_auxz00_12879,BFALSE,BFALSE);} } } 
{ /* Llib/hash.scm 1038 */
 obj_t BgL_string1z00_4742;
if(
STRINGP(BgL_arg1903z00_2363))
{ /* Llib/hash.scm 1038 */
BgL_string1z00_4742 = BgL_arg1903z00_2363; }  else 
{ 
 obj_t BgL_auxz00_12888;
BgL_auxz00_12888 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41927L), BGl_string3931z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_arg1903z00_2363); 
FAILURE(BgL_auxz00_12888,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1038 */
 long BgL_l1z00_4744;
BgL_l1z00_4744 = 
STRING_LENGTH(BgL_string1z00_4742); 
if(
(BgL_l1z00_4744==
STRING_LENGTH(BgL_keyz00_152)))
{ /* Llib/hash.scm 1038 */
 int BgL_arg2114z00_4747;
{ /* Llib/hash.scm 1038 */
 char * BgL_auxz00_12898; char * BgL_tmpz00_12896;
BgL_auxz00_12898 = 
BSTRING_TO_STRING(BgL_keyz00_152); 
BgL_tmpz00_12896 = 
BSTRING_TO_STRING(BgL_string1z00_4742); 
BgL_arg2114z00_4747 = 
memcmp(BgL_tmpz00_12896, BgL_auxz00_12898, BgL_l1z00_4744); } 
BgL_test5168z00_12867 = 
(
(long)(BgL_arg2114z00_4747)==0L); }  else 
{ /* Llib/hash.scm 1038 */
BgL_test5168z00_12867 = ((bool_t)0)
; } } } } 
if(BgL_test5168z00_12867)
{ /* Llib/hash.scm 1038 */
{ /* Llib/hash.scm 1040 */
 long BgL_arg1894z00_2355;
BgL_arg1894z00_2355 = 
(BgL_off3z00_2351+1L); 
{ /* Llib/hash.scm 1040 */
 obj_t BgL_vectorz00_4754;
if(
VECTORP(BgL_bucketsz00_2344))
{ /* Llib/hash.scm 1040 */
BgL_vectorz00_4754 = BgL_bucketsz00_2344; }  else 
{ 
 obj_t BgL_auxz00_12906;
BgL_auxz00_12906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41967L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2344); 
FAILURE(BgL_auxz00_12906,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1040 */
 bool_t BgL_test5174z00_12910;
{ /* Llib/hash.scm 1040 */
 long BgL_tmpz00_12911;
BgL_tmpz00_12911 = 
VECTOR_LENGTH(BgL_vectorz00_4754); 
BgL_test5174z00_12910 = 
BOUND_CHECK(BgL_arg1894z00_2355, BgL_tmpz00_12911); } 
if(BgL_test5174z00_12910)
{ /* Llib/hash.scm 1040 */
VECTOR_SET(BgL_vectorz00_4754,BgL_arg1894z00_2355,BFALSE); }  else 
{ 
 obj_t BgL_auxz00_12915;
BgL_auxz00_12915 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41954L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4754, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4754)), 
(int)(BgL_arg1894z00_2355)); 
FAILURE(BgL_auxz00_12915,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 1041 */
 long BgL_arg1896z00_2356;
BgL_arg1896z00_2356 = 
(BgL_off3z00_2351+2L); 
{ /* Llib/hash.scm 1041 */
 obj_t BgL_vectorz00_4757;
if(
VECTORP(BgL_bucketsz00_2344))
{ /* Llib/hash.scm 1041 */
BgL_vectorz00_4757 = BgL_bucketsz00_2344; }  else 
{ 
 obj_t BgL_auxz00_12925;
BgL_auxz00_12925 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42013L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2344); 
FAILURE(BgL_auxz00_12925,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1041 */
 bool_t BgL_test5176z00_12929;
{ /* Llib/hash.scm 1041 */
 long BgL_tmpz00_12930;
BgL_tmpz00_12930 = 
VECTOR_LENGTH(BgL_vectorz00_4757); 
BgL_test5176z00_12929 = 
BOUND_CHECK(BgL_arg1896z00_2356, BgL_tmpz00_12930); } 
if(BgL_test5176z00_12929)
{ /* Llib/hash.scm 1041 */
VECTOR_SET(BgL_vectorz00_4757,BgL_arg1896z00_2356,BFALSE); }  else 
{ 
 obj_t BgL_auxz00_12934;
BgL_auxz00_12934 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42000L), BGl_string3933z00zz__hashz00, BgL_vectorz00_4757, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4757)), 
(int)(BgL_arg1896z00_2356)); 
FAILURE(BgL_auxz00_12934,BFALSE,BFALSE);} } } } 
{ /* Llib/hash.scm 1303 */
 long BgL_arg2044z00_4759;
{ /* Llib/hash.scm 1303 */
 obj_t BgL_arg2045z00_4760;
{ /* Llib/hash.scm 1272 */
 bool_t BgL_test5177z00_12941;
{ /* Llib/hash.scm 1272 */
 obj_t BgL_tmpz00_12942;
{ /* Llib/hash.scm 1272 */
 obj_t BgL_res2635z00_4764;
{ /* Llib/hash.scm 1272 */
 obj_t BgL_aux3719z00_6611;
BgL_aux3719z00_6611 = 
STRUCT_KEY(BgL_tz00_151); 
if(
SYMBOLP(BgL_aux3719z00_6611))
{ /* Llib/hash.scm 1272 */
BgL_res2635z00_4764 = BgL_aux3719z00_6611; }  else 
{ 
 obj_t BgL_auxz00_12946;
BgL_auxz00_12946 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50571L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3719z00_6611); 
FAILURE(BgL_auxz00_12946,BFALSE,BFALSE);} } 
BgL_tmpz00_12942 = BgL_res2635z00_4764; } 
BgL_test5177z00_12941 = 
(BgL_tmpz00_12942==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5177z00_12941)
{ /* Llib/hash.scm 1272 */
 int BgL_tmpz00_12951;
BgL_tmpz00_12951 = 
(int)(6L); 
BgL_arg2045z00_4760 = 
STRUCT_REF(BgL_tz00_151, BgL_tmpz00_12951); }  else 
{ /* Llib/hash.scm 1272 */
BgL_arg2045z00_4760 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_151); } } 
{ /* Llib/hash.scm 1303 */
 long BgL_za71za7_4765;
{ /* Llib/hash.scm 1303 */
 obj_t BgL_tmpz00_12955;
if(
INTEGERP(BgL_arg2045z00_4760))
{ /* Llib/hash.scm 1303 */
BgL_tmpz00_12955 = BgL_arg2045z00_4760
; }  else 
{ 
 obj_t BgL_auxz00_12958;
BgL_auxz00_12958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51912L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2045z00_4760); 
FAILURE(BgL_auxz00_12958,BFALSE,BFALSE);} 
BgL_za71za7_4765 = 
(long)CINT(BgL_tmpz00_12955); } 
BgL_arg2044z00_4759 = 
(BgL_za71za7_4765+1L); } } 
{ /* Llib/hash.scm 1273 */
 bool_t BgL_test5180z00_12964;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_tmpz00_12965;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_res2636z00_4769;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_aux3722z00_6614;
BgL_aux3722z00_6614 = 
STRUCT_KEY(BgL_tz00_151); 
if(
SYMBOLP(BgL_aux3722z00_6614))
{ /* Llib/hash.scm 1273 */
BgL_res2636z00_4769 = BgL_aux3722z00_6614; }  else 
{ 
 obj_t BgL_auxz00_12969;
BgL_auxz00_12969 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50639L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3722z00_6614); 
FAILURE(BgL_auxz00_12969,BFALSE,BFALSE);} } 
BgL_tmpz00_12965 = BgL_res2636z00_4769; } 
BgL_test5180z00_12964 = 
(BgL_tmpz00_12965==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5180z00_12964)
{ /* Llib/hash.scm 1273 */
 obj_t BgL_auxz00_12976; int BgL_tmpz00_12974;
BgL_auxz00_12976 = 
BINT(BgL_arg2044z00_4759); 
BgL_tmpz00_12974 = 
(int)(6L); 
return 
STRUCT_SET(BgL_tz00_151, BgL_tmpz00_12974, BgL_auxz00_12976);}  else 
{ /* Llib/hash.scm 1273 */
return 
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_151);} } } }  else 
{ /* Llib/hash.scm 1043 */
 long BgL_noffz00_2357;
BgL_noffz00_2357 = 
(BgL_offz00_2348+
(BgL_iz00_2349*BgL_iz00_2349)); 
{ /* Llib/hash.scm 1044 */
 bool_t BgL_test5182z00_12982;
{ /* Llib/hash.scm 1044 */
 long BgL_n2z00_4775;
{ /* Llib/hash.scm 1044 */
 obj_t BgL_tmpz00_12983;
if(
INTEGERP(BgL_siza7eza7_2343))
{ /* Llib/hash.scm 1044 */
BgL_tmpz00_12983 = BgL_siza7eza7_2343
; }  else 
{ 
 obj_t BgL_auxz00_12986;
BgL_auxz00_12986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42151L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2343); 
FAILURE(BgL_auxz00_12986,BFALSE,BFALSE);} 
BgL_n2z00_4775 = 
(long)CINT(BgL_tmpz00_12983); } 
BgL_test5182z00_12982 = 
(BgL_noffz00_2357>=BgL_n2z00_4775); } 
if(BgL_test5182z00_12982)
{ /* Llib/hash.scm 1045 */
 long BgL_arg1898z00_2359; long BgL_arg1899z00_2360;
{ /* Llib/hash.scm 1045 */
 long BgL_n1z00_4776; long BgL_n2z00_4777;
BgL_n1z00_4776 = BgL_noffz00_2357; 
{ /* Llib/hash.scm 1045 */
 obj_t BgL_tmpz00_12992;
if(
INTEGERP(BgL_siza7eza7_2343))
{ /* Llib/hash.scm 1045 */
BgL_tmpz00_12992 = BgL_siza7eza7_2343
; }  else 
{ 
 obj_t BgL_auxz00_12995;
BgL_auxz00_12995 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42186L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_siza7eza7_2343); 
FAILURE(BgL_auxz00_12995,BFALSE,BFALSE);} 
BgL_n2z00_4777 = 
(long)CINT(BgL_tmpz00_12992); } 
{ /* Llib/hash.scm 1045 */
 bool_t BgL_test5185z00_13000;
{ /* Llib/hash.scm 1045 */
 long BgL_arg2221z00_4779;
BgL_arg2221z00_4779 = 
(((BgL_n1z00_4776) | (BgL_n2z00_4777)) & -2147483648); 
BgL_test5185z00_13000 = 
(BgL_arg2221z00_4779==0L); } 
if(BgL_test5185z00_13000)
{ /* Llib/hash.scm 1045 */
 int32_t BgL_arg2218z00_4780;
{ /* Llib/hash.scm 1045 */
 int32_t BgL_arg2219z00_4781; int32_t BgL_arg2220z00_4782;
BgL_arg2219z00_4781 = 
(int32_t)(BgL_n1z00_4776); 
BgL_arg2220z00_4782 = 
(int32_t)(BgL_n2z00_4777); 
BgL_arg2218z00_4780 = 
(BgL_arg2219z00_4781%BgL_arg2220z00_4782); } 
{ /* Llib/hash.scm 1045 */
 long BgL_arg2326z00_4787;
BgL_arg2326z00_4787 = 
(long)(BgL_arg2218z00_4780); 
BgL_arg1898z00_2359 = 
(long)(BgL_arg2326z00_4787); } }  else 
{ /* Llib/hash.scm 1045 */
BgL_arg1898z00_2359 = 
(BgL_n1z00_4776%BgL_n2z00_4777); } } } 
BgL_arg1899z00_2360 = 
(BgL_iz00_2349+1L); 
{ 
 long BgL_iz00_13011; long BgL_offz00_13010;
BgL_offz00_13010 = BgL_arg1898z00_2359; 
BgL_iz00_13011 = BgL_arg1899z00_2360; 
BgL_iz00_2349 = BgL_iz00_13011; 
BgL_offz00_2348 = BgL_offz00_13010; 
goto BgL_zc3z04anonymousza31890ze3z87_2350;} }  else 
{ 
 long BgL_iz00_13013; long BgL_offz00_13012;
BgL_offz00_13012 = BgL_noffz00_2357; 
BgL_iz00_13013 = 
(BgL_iz00_2349+1L); 
BgL_iz00_2349 = BgL_iz00_13013; 
BgL_offz00_2348 = BgL_offz00_13012; 
goto BgL_zc3z04anonymousza31890ze3z87_2350;} } } }  else 
{ /* Llib/hash.scm 1037 */
return BFALSE;} } } } } } } } } } 

}



/* &open-string-hashtable-remove! */
obj_t BGl_z62openzd2stringzd2hashtablezd2removez12za2zz__hashz00(obj_t BgL_envz00_5517, obj_t BgL_tz00_5518, obj_t BgL_keyz00_5519)
{
{ /* Llib/hash.scm 1029 */
{ /* Llib/hash.scm 1030 */
 obj_t BgL_auxz00_13022; obj_t BgL_auxz00_13015;
if(
STRINGP(BgL_keyz00_5519))
{ /* Llib/hash.scm 1030 */
BgL_auxz00_13022 = BgL_keyz00_5519
; }  else 
{ 
 obj_t BgL_auxz00_13025;
BgL_auxz00_13025 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41605L), BGl_string4070z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_keyz00_5519); 
FAILURE(BgL_auxz00_13025,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tz00_5518))
{ /* Llib/hash.scm 1030 */
BgL_auxz00_13015 = BgL_tz00_5518
; }  else 
{ 
 obj_t BgL_auxz00_13018;
BgL_auxz00_13018 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(41605L), BGl_string4070z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tz00_5518); 
FAILURE(BgL_auxz00_13018,BFALSE,BFALSE);} 
return 
BGl_openzd2stringzd2hashtablezd2removez12zc0zz__hashz00(BgL_auxz00_13015, BgL_auxz00_13022);} } 

}



/* plain-hashtable-remove! */
bool_t BGl_plainzd2hashtablezd2removez12z12zz__hashz00(obj_t BgL_tablez00_153, obj_t BgL_keyz00_154)
{
{ /* Llib/hash.scm 1051 */
{ /* Llib/hash.scm 1052 */
 obj_t BgL_bucketsz00_2366;
{ /* Llib/hash.scm 1052 */
 bool_t BgL_test5188z00_13030;
{ /* Llib/hash.scm 1052 */
 obj_t BgL_tmpz00_13031;
{ /* Llib/hash.scm 1052 */
 obj_t BgL_res2637z00_4794;
{ /* Llib/hash.scm 1052 */
 obj_t BgL_aux3730z00_6622;
BgL_aux3730z00_6622 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3730z00_6622))
{ /* Llib/hash.scm 1052 */
BgL_res2637z00_4794 = BgL_aux3730z00_6622; }  else 
{ 
 obj_t BgL_auxz00_13035;
BgL_auxz00_13035 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42537L), BGl_string4071z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3730z00_6622); 
FAILURE(BgL_auxz00_13035,BFALSE,BFALSE);} } 
BgL_tmpz00_13031 = BgL_res2637z00_4794; } 
BgL_test5188z00_13030 = 
(BgL_tmpz00_13031==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5188z00_13030)
{ /* Llib/hash.scm 1052 */
 int BgL_tmpz00_13040;
BgL_tmpz00_13040 = 
(int)(2L); 
BgL_bucketsz00_2366 = 
STRUCT_REF(BgL_tablez00_153, BgL_tmpz00_13040); }  else 
{ /* Llib/hash.scm 1052 */
BgL_bucketsz00_2366 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } 
{ /* Llib/hash.scm 1052 */
 long BgL_bucketzd2lenzd2_2367;
{ /* Llib/hash.scm 1053 */
 obj_t BgL_vectorz00_4795;
if(
VECTORP(BgL_bucketsz00_2366))
{ /* Llib/hash.scm 1053 */
BgL_vectorz00_4795 = BgL_bucketsz00_2366; }  else 
{ 
 obj_t BgL_auxz00_13046;
BgL_auxz00_13046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42595L), BGl_string4071z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2366); 
FAILURE(BgL_auxz00_13046,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_2367 = 
VECTOR_LENGTH(BgL_vectorz00_4795); } 
{ /* Llib/hash.scm 1053 */
 long BgL_bucketzd2numzd2_2368;
{ /* Llib/hash.scm 1054 */
 long BgL_arg1926z00_2391;
BgL_arg1926z00_2391 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_153, BgL_keyz00_154); 
{ /* Llib/hash.scm 1054 */
 long BgL_n1z00_4796; long BgL_n2z00_4797;
BgL_n1z00_4796 = BgL_arg1926z00_2391; 
BgL_n2z00_4797 = BgL_bucketzd2lenzd2_2367; 
{ /* Llib/hash.scm 1054 */
 bool_t BgL_test5191z00_13052;
{ /* Llib/hash.scm 1054 */
 long BgL_arg2221z00_4799;
BgL_arg2221z00_4799 = 
(((BgL_n1z00_4796) | (BgL_n2z00_4797)) & -2147483648); 
BgL_test5191z00_13052 = 
(BgL_arg2221z00_4799==0L); } 
if(BgL_test5191z00_13052)
{ /* Llib/hash.scm 1054 */
 int32_t BgL_arg2218z00_4800;
{ /* Llib/hash.scm 1054 */
 int32_t BgL_arg2219z00_4801; int32_t BgL_arg2220z00_4802;
BgL_arg2219z00_4801 = 
(int32_t)(BgL_n1z00_4796); 
BgL_arg2220z00_4802 = 
(int32_t)(BgL_n2z00_4797); 
BgL_arg2218z00_4800 = 
(BgL_arg2219z00_4801%BgL_arg2220z00_4802); } 
{ /* Llib/hash.scm 1054 */
 long BgL_arg2326z00_4807;
BgL_arg2326z00_4807 = 
(long)(BgL_arg2218z00_4800); 
BgL_bucketzd2numzd2_2368 = 
(long)(BgL_arg2326z00_4807); } }  else 
{ /* Llib/hash.scm 1054 */
BgL_bucketzd2numzd2_2368 = 
(BgL_n1z00_4796%BgL_n2z00_4797); } } } } 
{ /* Llib/hash.scm 1054 */
 obj_t BgL_bucketz00_2369;
{ /* Llib/hash.scm 1055 */
 obj_t BgL_vectorz00_4809;
if(
VECTORP(BgL_bucketsz00_2366))
{ /* Llib/hash.scm 1055 */
BgL_vectorz00_4809 = BgL_bucketsz00_2366; }  else 
{ 
 obj_t BgL_auxz00_13063;
BgL_auxz00_13063 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42705L), BGl_string4071z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2366); 
FAILURE(BgL_auxz00_13063,BFALSE,BFALSE);} 
BgL_bucketz00_2369 = 
VECTOR_REF(BgL_vectorz00_4809,BgL_bucketzd2numzd2_2368); } 
{ /* Llib/hash.scm 1055 */

if(
NULLP(BgL_bucketz00_2369))
{ /* Llib/hash.scm 1057 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 1059 */
 bool_t BgL_test5194z00_13070;
{ /* Llib/hash.scm 1059 */
 obj_t BgL_arg1925z00_2390;
{ /* Llib/hash.scm 1059 */
 obj_t BgL_pairz00_4811;
if(
PAIRP(BgL_bucketz00_2369))
{ /* Llib/hash.scm 1059 */
BgL_pairz00_4811 = BgL_bucketz00_2369; }  else 
{ 
 obj_t BgL_auxz00_13073;
BgL_auxz00_13073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42797L), BGl_string4071z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2369); 
FAILURE(BgL_auxz00_13073,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1059 */
 obj_t BgL_pairz00_4814;
{ /* Llib/hash.scm 1059 */
 obj_t BgL_aux3738z00_6630;
BgL_aux3738z00_6630 = 
CAR(BgL_pairz00_4811); 
if(
PAIRP(BgL_aux3738z00_6630))
{ /* Llib/hash.scm 1059 */
BgL_pairz00_4814 = BgL_aux3738z00_6630; }  else 
{ 
 obj_t BgL_auxz00_13080;
BgL_auxz00_13080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42791L), BGl_string4071z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3738z00_6630); 
FAILURE(BgL_auxz00_13080,BFALSE,BFALSE);} } 
BgL_arg1925z00_2390 = 
CAR(BgL_pairz00_4814); } } 
{ /* Llib/hash.scm 1059 */
 obj_t BgL_eqtz00_4815;
{ /* Llib/hash.scm 1059 */
 bool_t BgL_test5197z00_13085;
{ /* Llib/hash.scm 1059 */
 obj_t BgL_tmpz00_13086;
{ /* Llib/hash.scm 1059 */
 obj_t BgL_res2638z00_4822;
{ /* Llib/hash.scm 1059 */
 obj_t BgL_aux3740z00_6632;
BgL_aux3740z00_6632 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3740z00_6632))
{ /* Llib/hash.scm 1059 */
BgL_res2638z00_4822 = BgL_aux3740z00_6632; }  else 
{ 
 obj_t BgL_auxz00_13090;
BgL_auxz00_13090 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42767L), BGl_string4071z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3740z00_6632); 
FAILURE(BgL_auxz00_13090,BFALSE,BFALSE);} } 
BgL_tmpz00_13086 = BgL_res2638z00_4822; } 
BgL_test5197z00_13085 = 
(BgL_tmpz00_13086==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5197z00_13085)
{ /* Llib/hash.scm 1059 */
 int BgL_tmpz00_13095;
BgL_tmpz00_13095 = 
(int)(3L); 
BgL_eqtz00_4815 = 
STRUCT_REF(BgL_tablez00_153, BgL_tmpz00_13095); }  else 
{ /* Llib/hash.scm 1059 */
BgL_eqtz00_4815 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } 
if(
PROCEDUREP(BgL_eqtz00_4815))
{ /* Llib/hash.scm 1059 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_4815, 2))
{ /* Llib/hash.scm 1059 */
BgL_test5194z00_13070 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_4815, BgL_arg1925z00_2390, BgL_keyz00_154))
; }  else 
{ /* Llib/hash.scm 1059 */
FAILURE(BGl_string4072z00zz__hashz00,BGl_list4073z00zz__hashz00,BgL_eqtz00_4815);} }  else 
{ /* Llib/hash.scm 1059 */
if(
(BgL_arg1925z00_2390==BgL_keyz00_154))
{ /* Llib/hash.scm 1059 */
BgL_test5194z00_13070 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 1059 */
if(
STRINGP(BgL_arg1925z00_2390))
{ /* Llib/hash.scm 1059 */
if(
STRINGP(BgL_keyz00_154))
{ /* Llib/hash.scm 1059 */
 long BgL_l1z00_4825;
BgL_l1z00_4825 = 
STRING_LENGTH(BgL_arg1925z00_2390); 
if(
(BgL_l1z00_4825==
STRING_LENGTH(BgL_keyz00_154)))
{ /* Llib/hash.scm 1059 */
 int BgL_arg2114z00_4828;
{ /* Llib/hash.scm 1059 */
 char * BgL_auxz00_13122; char * BgL_tmpz00_13120;
BgL_auxz00_13122 = 
BSTRING_TO_STRING(BgL_keyz00_154); 
BgL_tmpz00_13120 = 
BSTRING_TO_STRING(BgL_arg1925z00_2390); 
BgL_arg2114z00_4828 = 
memcmp(BgL_tmpz00_13120, BgL_auxz00_13122, BgL_l1z00_4825); } 
BgL_test5194z00_13070 = 
(
(long)(BgL_arg2114z00_4828)==0L); }  else 
{ /* Llib/hash.scm 1059 */
BgL_test5194z00_13070 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 1059 */
BgL_test5194z00_13070 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 1059 */
BgL_test5194z00_13070 = ((bool_t)0)
; } } } } } 
if(BgL_test5194z00_13070)
{ /* Llib/hash.scm 1059 */
{ /* Llib/hash.scm 1060 */
 obj_t BgL_arg1910z00_2373;
{ /* Llib/hash.scm 1060 */
 obj_t BgL_pairz00_4834;
if(
PAIRP(BgL_bucketz00_2369))
{ /* Llib/hash.scm 1060 */
BgL_pairz00_4834 = BgL_bucketz00_2369; }  else 
{ 
 obj_t BgL_auxz00_13129;
BgL_auxz00_13129 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42853L), BGl_string4071z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2369); 
FAILURE(BgL_auxz00_13129,BFALSE,BFALSE);} 
BgL_arg1910z00_2373 = 
CDR(BgL_pairz00_4834); } 
{ /* Llib/hash.scm 1060 */
 obj_t BgL_vectorz00_4835;
if(
VECTORP(BgL_bucketsz00_2366))
{ /* Llib/hash.scm 1060 */
BgL_vectorz00_4835 = BgL_bucketsz00_2366; }  else 
{ 
 obj_t BgL_auxz00_13136;
BgL_auxz00_13136 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42829L), BGl_string4071z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2366); 
FAILURE(BgL_auxz00_13136,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_4835,BgL_bucketzd2numzd2_2368,BgL_arg1910z00_2373); } } 
{ /* Llib/hash.scm 1061 */
 long BgL_arg1911z00_2374;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_arg1912z00_2375;
{ /* Llib/hash.scm 1061 */
 bool_t BgL_test5207z00_13141;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_tmpz00_13142;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_res2639z00_4840;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_aux3747z00_6640;
BgL_aux3747z00_6640 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3747z00_6640))
{ /* Llib/hash.scm 1061 */
BgL_res2639z00_4840 = BgL_aux3747z00_6640; }  else 
{ 
 obj_t BgL_auxz00_13146;
BgL_auxz00_13146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42898L), BGl_string4071z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3747z00_6640); 
FAILURE(BgL_auxz00_13146,BFALSE,BFALSE);} } 
BgL_tmpz00_13142 = BgL_res2639z00_4840; } 
BgL_test5207z00_13141 = 
(BgL_tmpz00_13142==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5207z00_13141)
{ /* Llib/hash.scm 1061 */
 int BgL_tmpz00_13151;
BgL_tmpz00_13151 = 
(int)(0L); 
BgL_arg1912z00_2375 = 
STRUCT_REF(BgL_tablez00_153, BgL_tmpz00_13151); }  else 
{ /* Llib/hash.scm 1061 */
BgL_arg1912z00_2375 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } 
{ /* Llib/hash.scm 1061 */
 long BgL_za71za7_4841;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_tmpz00_13155;
if(
INTEGERP(BgL_arg1912z00_2375))
{ /* Llib/hash.scm 1061 */
BgL_tmpz00_13155 = BgL_arg1912z00_2375
; }  else 
{ 
 obj_t BgL_auxz00_13158;
BgL_auxz00_13158 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42920L), BGl_string4071z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1912z00_2375); 
FAILURE(BgL_auxz00_13158,BFALSE,BFALSE);} 
BgL_za71za7_4841 = 
(long)CINT(BgL_tmpz00_13155); } 
BgL_arg1911z00_2374 = 
(BgL_za71za7_4841-1L); } } 
{ /* Llib/hash.scm 1061 */
 bool_t BgL_test5210z00_13164;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_tmpz00_13165;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_res2640z00_4845;
{ /* Llib/hash.scm 1061 */
 obj_t BgL_aux3750z00_6643;
BgL_aux3750z00_6643 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3750z00_6643))
{ /* Llib/hash.scm 1061 */
BgL_res2640z00_4845 = BgL_aux3750z00_6643; }  else 
{ 
 obj_t BgL_auxz00_13169;
BgL_auxz00_13169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42865L), BGl_string4071z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3750z00_6643); 
FAILURE(BgL_auxz00_13169,BFALSE,BFALSE);} } 
BgL_tmpz00_13165 = BgL_res2640z00_4845; } 
BgL_test5210z00_13164 = 
(BgL_tmpz00_13165==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5210z00_13164)
{ /* Llib/hash.scm 1061 */
 obj_t BgL_auxz00_13176; int BgL_tmpz00_13174;
BgL_auxz00_13176 = 
BINT(BgL_arg1911z00_2374); 
BgL_tmpz00_13174 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_153, BgL_tmpz00_13174, BgL_auxz00_13176); }  else 
{ /* Llib/hash.scm 1061 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } } 
return ((bool_t)1);}  else 
{ /* Llib/hash.scm 1064 */
 obj_t BgL_g1075z00_2376;
{ /* Llib/hash.scm 1064 */
 obj_t BgL_pairz00_4846;
if(
PAIRP(BgL_bucketz00_2369))
{ /* Llib/hash.scm 1064 */
BgL_pairz00_4846 = BgL_bucketz00_2369; }  else 
{ 
 obj_t BgL_auxz00_13182;
BgL_auxz00_13182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(42968L), BGl_string4071z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2369); 
FAILURE(BgL_auxz00_13182,BFALSE,BFALSE);} 
BgL_g1075z00_2376 = 
CDR(BgL_pairz00_4846); } 
{ 
 obj_t BgL_bucketz00_2378; obj_t BgL_prevz00_2379;
BgL_bucketz00_2378 = BgL_g1075z00_2376; 
BgL_prevz00_2379 = BgL_bucketz00_2369; 
BgL_zc3z04anonymousza31913ze3z87_2380:
if(
PAIRP(BgL_bucketz00_2378))
{ /* Llib/hash.scm 1067 */
 bool_t BgL_test5214z00_13189;
{ /* Llib/hash.scm 1067 */
 obj_t BgL_arg1924z00_2388;
{ /* Llib/hash.scm 1067 */
 obj_t BgL_pairz00_4850;
{ /* Llib/hash.scm 1067 */
 obj_t BgL_aux3754z00_6647;
BgL_aux3754z00_6647 = 
CAR(BgL_bucketz00_2378); 
if(
PAIRP(BgL_aux3754z00_6647))
{ /* Llib/hash.scm 1067 */
BgL_pairz00_4850 = BgL_aux3754z00_6647; }  else 
{ 
 obj_t BgL_auxz00_13193;
BgL_auxz00_13193 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43055L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_aux3754z00_6647); 
FAILURE(BgL_auxz00_13193,BFALSE,BFALSE);} } 
BgL_arg1924z00_2388 = 
CAR(BgL_pairz00_4850); } 
{ /* Llib/hash.scm 1067 */
 obj_t BgL_eqtz00_4851;
{ /* Llib/hash.scm 1067 */
 bool_t BgL_test5216z00_13198;
{ /* Llib/hash.scm 1067 */
 obj_t BgL_tmpz00_13199;
{ /* Llib/hash.scm 1067 */
 obj_t BgL_res2641z00_4858;
{ /* Llib/hash.scm 1067 */
 obj_t BgL_aux3756z00_6649;
BgL_aux3756z00_6649 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3756z00_6649))
{ /* Llib/hash.scm 1067 */
BgL_res2641z00_4858 = BgL_aux3756z00_6649; }  else 
{ 
 obj_t BgL_auxz00_13203;
BgL_auxz00_13203 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43031L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3756z00_6649); 
FAILURE(BgL_auxz00_13203,BFALSE,BFALSE);} } 
BgL_tmpz00_13199 = BgL_res2641z00_4858; } 
BgL_test5216z00_13198 = 
(BgL_tmpz00_13199==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5216z00_13198)
{ /* Llib/hash.scm 1067 */
 int BgL_tmpz00_13208;
BgL_tmpz00_13208 = 
(int)(3L); 
BgL_eqtz00_4851 = 
STRUCT_REF(BgL_tablez00_153, BgL_tmpz00_13208); }  else 
{ /* Llib/hash.scm 1067 */
BgL_eqtz00_4851 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } 
if(
PROCEDUREP(BgL_eqtz00_4851))
{ /* Llib/hash.scm 1067 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_4851, 2))
{ /* Llib/hash.scm 1067 */
BgL_test5214z00_13189 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_4851, BgL_arg1924z00_2388, BgL_keyz00_154))
; }  else 
{ /* Llib/hash.scm 1067 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4076z00zz__hashz00,BgL_eqtz00_4851);} }  else 
{ /* Llib/hash.scm 1067 */
if(
(BgL_arg1924z00_2388==BgL_keyz00_154))
{ /* Llib/hash.scm 1067 */
BgL_test5214z00_13189 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 1067 */
if(
STRINGP(BgL_arg1924z00_2388))
{ /* Llib/hash.scm 1067 */
if(
STRINGP(BgL_keyz00_154))
{ /* Llib/hash.scm 1067 */
 long BgL_l1z00_4861;
BgL_l1z00_4861 = 
STRING_LENGTH(BgL_arg1924z00_2388); 
if(
(BgL_l1z00_4861==
STRING_LENGTH(BgL_keyz00_154)))
{ /* Llib/hash.scm 1067 */
 int BgL_arg2114z00_4864;
{ /* Llib/hash.scm 1067 */
 char * BgL_auxz00_13235; char * BgL_tmpz00_13233;
BgL_auxz00_13235 = 
BSTRING_TO_STRING(BgL_keyz00_154); 
BgL_tmpz00_13233 = 
BSTRING_TO_STRING(BgL_arg1924z00_2388); 
BgL_arg2114z00_4864 = 
memcmp(BgL_tmpz00_13233, BgL_auxz00_13235, BgL_l1z00_4861); } 
BgL_test5214z00_13189 = 
(
(long)(BgL_arg2114z00_4864)==0L); }  else 
{ /* Llib/hash.scm 1067 */
BgL_test5214z00_13189 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 1067 */
BgL_test5214z00_13189 = ((bool_t)0)
; } }  else 
{ /* Llib/hash.scm 1067 */
BgL_test5214z00_13189 = ((bool_t)0)
; } } } } } 
if(BgL_test5214z00_13189)
{ /* Llib/hash.scm 1067 */
{ /* Llib/hash.scm 1069 */
 obj_t BgL_arg1918z00_2384;
BgL_arg1918z00_2384 = 
CDR(BgL_bucketz00_2378); 
{ /* Llib/hash.scm 1069 */
 obj_t BgL_pairz00_4871;
if(
PAIRP(BgL_prevz00_2379))
{ /* Llib/hash.scm 1069 */
BgL_pairz00_4871 = BgL_prevz00_2379; }  else 
{ 
 obj_t BgL_auxz00_13243;
BgL_auxz00_13243 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43101L), BGl_string3931z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_prevz00_2379); 
FAILURE(BgL_auxz00_13243,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_4871, BgL_arg1918z00_2384); } } 
{ /* Llib/hash.scm 1071 */
 long BgL_arg1919z00_2385;
{ /* Llib/hash.scm 1071 */
 obj_t BgL_arg1920z00_2386;
{ /* Llib/hash.scm 1071 */
 bool_t BgL_test5225z00_13248;
{ /* Llib/hash.scm 1071 */
 obj_t BgL_tmpz00_13249;
{ /* Llib/hash.scm 1071 */
 obj_t BgL_res2642z00_4875;
{ /* Llib/hash.scm 1071 */
 obj_t BgL_aux3761z00_6655;
BgL_aux3761z00_6655 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3761z00_6655))
{ /* Llib/hash.scm 1071 */
BgL_res2642z00_4875 = BgL_aux3761z00_6655; }  else 
{ 
 obj_t BgL_auxz00_13253;
BgL_auxz00_13253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43167L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3761z00_6655); 
FAILURE(BgL_auxz00_13253,BFALSE,BFALSE);} } 
BgL_tmpz00_13249 = BgL_res2642z00_4875; } 
BgL_test5225z00_13248 = 
(BgL_tmpz00_13249==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5225z00_13248)
{ /* Llib/hash.scm 1071 */
 int BgL_tmpz00_13258;
BgL_tmpz00_13258 = 
(int)(0L); 
BgL_arg1920z00_2386 = 
STRUCT_REF(BgL_tablez00_153, BgL_tmpz00_13258); }  else 
{ /* Llib/hash.scm 1071 */
BgL_arg1920z00_2386 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } 
{ /* Llib/hash.scm 1071 */
 long BgL_za71za7_4876;
{ /* Llib/hash.scm 1071 */
 obj_t BgL_tmpz00_13262;
if(
INTEGERP(BgL_arg1920z00_2386))
{ /* Llib/hash.scm 1071 */
BgL_tmpz00_13262 = BgL_arg1920z00_2386
; }  else 
{ 
 obj_t BgL_auxz00_13265;
BgL_auxz00_13265 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43189L), BGl_string3931z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg1920z00_2386); 
FAILURE(BgL_auxz00_13265,BFALSE,BFALSE);} 
BgL_za71za7_4876 = 
(long)CINT(BgL_tmpz00_13262); } 
BgL_arg1919z00_2385 = 
(BgL_za71za7_4876-1L); } } 
{ /* Llib/hash.scm 1070 */
 bool_t BgL_test5228z00_13271;
{ /* Llib/hash.scm 1070 */
 obj_t BgL_tmpz00_13272;
{ /* Llib/hash.scm 1070 */
 obj_t BgL_res2643z00_4880;
{ /* Llib/hash.scm 1070 */
 obj_t BgL_aux3764z00_6658;
BgL_aux3764z00_6658 = 
STRUCT_KEY(BgL_tablez00_153); 
if(
SYMBOLP(BgL_aux3764z00_6658))
{ /* Llib/hash.scm 1070 */
BgL_res2643z00_4880 = BgL_aux3764z00_6658; }  else 
{ 
 obj_t BgL_auxz00_13276;
BgL_auxz00_13276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43123L), BGl_string3931z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3764z00_6658); 
FAILURE(BgL_auxz00_13276,BFALSE,BFALSE);} } 
BgL_tmpz00_13272 = BgL_res2643z00_4880; } 
BgL_test5228z00_13271 = 
(BgL_tmpz00_13272==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5228z00_13271)
{ /* Llib/hash.scm 1070 */
 obj_t BgL_auxz00_13283; int BgL_tmpz00_13281;
BgL_auxz00_13283 = 
BINT(BgL_arg1919z00_2385); 
BgL_tmpz00_13281 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_153, BgL_tmpz00_13281, BgL_auxz00_13283); }  else 
{ /* Llib/hash.scm 1070 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_153); } } } 
return ((bool_t)1);}  else 
{ 
 obj_t BgL_prevz00_13289; obj_t BgL_bucketz00_13287;
BgL_bucketz00_13287 = 
CDR(BgL_bucketz00_2378); 
BgL_prevz00_13289 = BgL_bucketz00_2378; 
BgL_prevz00_2379 = BgL_prevz00_13289; 
BgL_bucketz00_2378 = BgL_bucketz00_13287; 
goto BgL_zc3z04anonymousza31913ze3z87_2380;} }  else 
{ /* Llib/hash.scm 1066 */
return ((bool_t)0);} } } } } } } } } } 

}



/* plain-hashtable-expand! */
obj_t BGl_plainzd2hashtablezd2expandz12z12zz__hashz00(obj_t BgL_tablez00_156)
{
{ /* Llib/hash.scm 1087 */
{ /* Llib/hash.scm 1088 */
 obj_t BgL_oldzd2buckszd2_2393;
{ /* Llib/hash.scm 1088 */
 bool_t BgL_test5230z00_13290;
{ /* Llib/hash.scm 1088 */
 obj_t BgL_tmpz00_13291;
{ /* Llib/hash.scm 1088 */
 obj_t BgL_res2644z00_4885;
{ /* Llib/hash.scm 1088 */
 obj_t BgL_aux3766z00_6660;
BgL_aux3766z00_6660 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3766z00_6660))
{ /* Llib/hash.scm 1088 */
BgL_res2644z00_4885 = BgL_aux3766z00_6660; }  else 
{ 
 obj_t BgL_auxz00_13295;
BgL_auxz00_13295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43903L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3766z00_6660); 
FAILURE(BgL_auxz00_13295,BFALSE,BFALSE);} } 
BgL_tmpz00_13291 = BgL_res2644z00_4885; } 
BgL_test5230z00_13290 = 
(BgL_tmpz00_13291==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5230z00_13290)
{ /* Llib/hash.scm 1088 */
 int BgL_tmpz00_13300;
BgL_tmpz00_13300 = 
(int)(2L); 
BgL_oldzd2buckszd2_2393 = 
STRUCT_REF(BgL_tablez00_156, BgL_tmpz00_13300); }  else 
{ /* Llib/hash.scm 1088 */
BgL_oldzd2buckszd2_2393 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); } } 
{ /* Llib/hash.scm 1088 */
 long BgL_lenz00_2394;
{ /* Llib/hash.scm 1089 */
 obj_t BgL_vectorz00_4886;
if(
VECTORP(BgL_oldzd2buckszd2_2393))
{ /* Llib/hash.scm 1089 */
BgL_vectorz00_4886 = BgL_oldzd2buckszd2_2393; }  else 
{ 
 obj_t BgL_auxz00_13306;
BgL_auxz00_13306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(43954L), BGl_string4079z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_oldzd2buckszd2_2393); 
FAILURE(BgL_auxz00_13306,BFALSE,BFALSE);} 
BgL_lenz00_2394 = 
VECTOR_LENGTH(BgL_vectorz00_4886); } 
{ /* Llib/hash.scm 1089 */
 long BgL_newzd2lenzd2_2395;
BgL_newzd2lenzd2_2395 = 
(2L*BgL_lenz00_2394); 
{ /* Llib/hash.scm 1090 */
 obj_t BgL_maxzd2lenzd2_2396;
{ /* Llib/hash.scm 1091 */
 bool_t BgL_test5233z00_13312;
{ /* Llib/hash.scm 1091 */
 obj_t BgL_tmpz00_13313;
{ /* Llib/hash.scm 1091 */
 obj_t BgL_res2645z00_4891;
{ /* Llib/hash.scm 1091 */
 obj_t BgL_aux3770z00_6664;
BgL_aux3770z00_6664 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3770z00_6664))
{ /* Llib/hash.scm 1091 */
BgL_res2645z00_4891 = BgL_aux3770z00_6664; }  else 
{ 
 obj_t BgL_auxz00_13317;
BgL_auxz00_13317 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44003L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3770z00_6664); 
FAILURE(BgL_auxz00_13317,BFALSE,BFALSE);} } 
BgL_tmpz00_13313 = BgL_res2645z00_4891; } 
BgL_test5233z00_13312 = 
(BgL_tmpz00_13313==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5233z00_13312)
{ /* Llib/hash.scm 1091 */
 int BgL_tmpz00_13322;
BgL_tmpz00_13322 = 
(int)(6L); 
BgL_maxzd2lenzd2_2396 = 
STRUCT_REF(BgL_tablez00_156, BgL_tmpz00_13322); }  else 
{ /* Llib/hash.scm 1091 */
BgL_maxzd2lenzd2_2396 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); } } 
{ /* Llib/hash.scm 1091 */

{ /* Llib/hash.scm 1093 */
 obj_t BgL_nmaxz00_2397;
{ /* Llib/hash.scm 1093 */
 obj_t BgL_a1138z00_2400;
{ /* Llib/hash.scm 1093 */
 bool_t BgL_test5235z00_13326;
{ /* Llib/hash.scm 1093 */
 obj_t BgL_tmpz00_13327;
{ /* Llib/hash.scm 1093 */
 obj_t BgL_res2646z00_4895;
{ /* Llib/hash.scm 1093 */
 obj_t BgL_aux3772z00_6666;
BgL_aux3772z00_6666 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3772z00_6666))
{ /* Llib/hash.scm 1093 */
BgL_res2646z00_4895 = BgL_aux3772z00_6666; }  else 
{ 
 obj_t BgL_auxz00_13331;
BgL_auxz00_13331 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44092L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3772z00_6666); 
FAILURE(BgL_auxz00_13331,BFALSE,BFALSE);} } 
BgL_tmpz00_13327 = BgL_res2646z00_4895; } 
BgL_test5235z00_13326 = 
(BgL_tmpz00_13327==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5235z00_13326)
{ /* Llib/hash.scm 1093 */
 int BgL_tmpz00_13336;
BgL_tmpz00_13336 = 
(int)(1L); 
BgL_a1138z00_2400 = 
STRUCT_REF(BgL_tablez00_156, BgL_tmpz00_13336); }  else 
{ /* Llib/hash.scm 1093 */
BgL_a1138z00_2400 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); } } 
{ /* Llib/hash.scm 1094 */
 obj_t BgL_b1139z00_2401;
{ /* Llib/hash.scm 1094 */
 bool_t BgL_test5237z00_13340;
{ /* Llib/hash.scm 1094 */
 obj_t BgL_tmpz00_13341;
{ /* Llib/hash.scm 1094 */
 obj_t BgL_res2647z00_4899;
{ /* Llib/hash.scm 1094 */
 obj_t BgL_aux3774z00_6668;
BgL_aux3774z00_6668 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3774z00_6668))
{ /* Llib/hash.scm 1094 */
BgL_res2647z00_4899 = BgL_aux3774z00_6668; }  else 
{ 
 obj_t BgL_auxz00_13345;
BgL_auxz00_13345 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44133L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3774z00_6668); 
FAILURE(BgL_auxz00_13345,BFALSE,BFALSE);} } 
BgL_tmpz00_13341 = BgL_res2647z00_4899; } 
BgL_test5237z00_13340 = 
(BgL_tmpz00_13341==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5237z00_13340)
{ /* Llib/hash.scm 1094 */
 int BgL_tmpz00_13350;
BgL_tmpz00_13350 = 
(int)(7L); 
BgL_b1139z00_2401 = 
STRUCT_REF(BgL_tablez00_156, BgL_tmpz00_13350); }  else 
{ /* Llib/hash.scm 1094 */
BgL_b1139z00_2401 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); } } 
{ /* Llib/hash.scm 1093 */

{ /* Llib/hash.scm 1093 */
 bool_t BgL_test5239z00_13354;
if(
INTEGERP(BgL_a1138z00_2400))
{ /* Llib/hash.scm 1093 */
BgL_test5239z00_13354 = 
INTEGERP(BgL_b1139z00_2401)
; }  else 
{ /* Llib/hash.scm 1093 */
BgL_test5239z00_13354 = ((bool_t)0)
; } 
if(BgL_test5239z00_13354)
{ /* Llib/hash.scm 1093 */
BgL_nmaxz00_2397 = 
BINT(
(
(long)CINT(BgL_a1138z00_2400)*
(long)CINT(BgL_b1139z00_2401))); }  else 
{ /* Llib/hash.scm 1093 */
BgL_nmaxz00_2397 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1138z00_2400, BgL_b1139z00_2401); } } } } } 
{ /* Llib/hash.scm 1096 */
 obj_t BgL_arg1928z00_2398;
if(
REALP(BgL_nmaxz00_2397))
{ /* Llib/hash.scm 1096 */
BgL_arg1928z00_2398 = 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_nmaxz00_2397))); }  else 
{ /* Llib/hash.scm 1096 */
BgL_arg1928z00_2398 = BgL_nmaxz00_2397; } 
{ /* Llib/hash.scm 1095 */
 bool_t BgL_test5242z00_13368;
{ /* Llib/hash.scm 1095 */
 obj_t BgL_tmpz00_13369;
{ /* Llib/hash.scm 1095 */
 obj_t BgL_res2648z00_4906;
{ /* Llib/hash.scm 1095 */
 obj_t BgL_aux3776z00_6670;
BgL_aux3776z00_6670 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3776z00_6670))
{ /* Llib/hash.scm 1095 */
BgL_res2648z00_4906 = BgL_aux3776z00_6670; }  else 
{ 
 obj_t BgL_auxz00_13373;
BgL_auxz00_13373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44174L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3776z00_6670); 
FAILURE(BgL_auxz00_13373,BFALSE,BFALSE);} } 
BgL_tmpz00_13369 = BgL_res2648z00_4906; } 
BgL_test5242z00_13368 = 
(BgL_tmpz00_13369==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5242z00_13368)
{ /* Llib/hash.scm 1095 */
 int BgL_tmpz00_13378;
BgL_tmpz00_13378 = 
(int)(1L); 
STRUCT_SET(BgL_tablez00_156, BgL_tmpz00_13378, BgL_arg1928z00_2398); }  else 
{ /* Llib/hash.scm 1095 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); } } } } 
{ /* Llib/hash.scm 1098 */
 bool_t BgL_test5244z00_13382;
{ /* Llib/hash.scm 1098 */
 bool_t BgL_test5245z00_13383;
{ /* Llib/hash.scm 1098 */
 long BgL_n1z00_4907;
{ /* Llib/hash.scm 1098 */
 obj_t BgL_tmpz00_13384;
if(
INTEGERP(BgL_maxzd2lenzd2_2396))
{ /* Llib/hash.scm 1098 */
BgL_tmpz00_13384 = BgL_maxzd2lenzd2_2396
; }  else 
{ 
 obj_t BgL_auxz00_13387;
BgL_auxz00_13387 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44319L), BGl_string4079z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_maxzd2lenzd2_2396); 
FAILURE(BgL_auxz00_13387,BFALSE,BFALSE);} 
BgL_n1z00_4907 = 
(long)CINT(BgL_tmpz00_13384); } 
BgL_test5245z00_13383 = 
(BgL_n1z00_4907<0L); } 
if(BgL_test5245z00_13383)
{ /* Llib/hash.scm 1098 */
BgL_test5244z00_13382 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 1098 */
 long BgL_n2z00_4909;
{ /* Llib/hash.scm 1098 */
 obj_t BgL_tmpz00_13393;
if(
INTEGERP(BgL_maxzd2lenzd2_2396))
{ /* Llib/hash.scm 1098 */
BgL_tmpz00_13393 = BgL_maxzd2lenzd2_2396
; }  else 
{ 
 obj_t BgL_auxz00_13396;
BgL_auxz00_13396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44344L), BGl_string4079z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_maxzd2lenzd2_2396); 
FAILURE(BgL_auxz00_13396,BFALSE,BFALSE);} 
BgL_n2z00_4909 = 
(long)CINT(BgL_tmpz00_13393); } 
BgL_test5244z00_13382 = 
(BgL_newzd2lenzd2_2395<=BgL_n2z00_4909); } } 
if(BgL_test5244z00_13382)
{ /* Llib/hash.scm 1099 */
 obj_t BgL_newzd2buckszd2_2405;
BgL_newzd2buckszd2_2405 = 
make_vector(BgL_newzd2lenzd2_2395, BNIL); 
{ /* Llib/hash.scm 1100 */
 bool_t BgL_test5248z00_13403;
{ /* Llib/hash.scm 1100 */
 obj_t BgL_tmpz00_13404;
{ /* Llib/hash.scm 1100 */
 obj_t BgL_res2649z00_4913;
{ /* Llib/hash.scm 1100 */
 obj_t BgL_aux3780z00_6674;
BgL_aux3780z00_6674 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3780z00_6674))
{ /* Llib/hash.scm 1100 */
BgL_res2649z00_4913 = BgL_aux3780z00_6674; }  else 
{ 
 obj_t BgL_auxz00_13408;
BgL_auxz00_13408 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44408L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3780z00_6674); 
FAILURE(BgL_auxz00_13408,BFALSE,BFALSE);} } 
BgL_tmpz00_13404 = BgL_res2649z00_4913; } 
BgL_test5248z00_13403 = 
(BgL_tmpz00_13404==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5248z00_13403)
{ /* Llib/hash.scm 1100 */
 int BgL_tmpz00_13413;
BgL_tmpz00_13413 = 
(int)(2L); 
STRUCT_SET(BgL_tablez00_156, BgL_tmpz00_13413, BgL_newzd2buckszd2_2405); }  else 
{ /* Llib/hash.scm 1100 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); } } 
{ 
 long BgL_iz00_2407;
{ /* Llib/hash.scm 1101 */
 bool_t BgL_tmpz00_13417;
BgL_iz00_2407 = 0L; 
BgL_zc3z04anonymousza31933ze3z87_2408:
if(
(BgL_iz00_2407<BgL_lenz00_2394))
{ /* Llib/hash.scm 1102 */
{ /* Llib/hash.scm 1103 */
 obj_t BgL_g1142z00_2410;
{ /* Llib/hash.scm 1109 */
 obj_t BgL_vectorz00_4916;
if(
VECTORP(BgL_oldzd2buckszd2_2393))
{ /* Llib/hash.scm 1109 */
BgL_vectorz00_4916 = BgL_oldzd2buckszd2_2393; }  else 
{ 
 obj_t BgL_auxz00_13422;
BgL_auxz00_13422 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44748L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_oldzd2buckszd2_2393); 
FAILURE(BgL_auxz00_13422,BFALSE,BFALSE);} 
BgL_g1142z00_2410 = 
VECTOR_REF(BgL_vectorz00_4916,BgL_iz00_2407); } 
{ 
 obj_t BgL_l1140z00_2412;
BgL_l1140z00_2412 = BgL_g1142z00_2410; 
BgL_zc3z04anonymousza31935ze3z87_2413:
if(
PAIRP(BgL_l1140z00_2412))
{ /* Llib/hash.scm 1109 */
{ /* Llib/hash.scm 1104 */
 obj_t BgL_cellz00_2415;
BgL_cellz00_2415 = 
CAR(BgL_l1140z00_2412); 
{ /* Llib/hash.scm 1104 */
 obj_t BgL_keyz00_2416;
{ /* Llib/hash.scm 1104 */
 obj_t BgL_pairz00_4919;
if(
PAIRP(BgL_cellz00_2415))
{ /* Llib/hash.scm 1104 */
BgL_pairz00_4919 = BgL_cellz00_2415; }  else 
{ 
 obj_t BgL_auxz00_13432;
BgL_auxz00_13432 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44545L), BGl_string4080z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_cellz00_2415); 
FAILURE(BgL_auxz00_13432,BFALSE,BFALSE);} 
BgL_keyz00_2416 = 
CAR(BgL_pairz00_4919); } 
{ /* Llib/hash.scm 1104 */
 long BgL_nz00_2417;
BgL_nz00_2417 = 
BGl_tablezd2getzd2hashnumberz00zz__hashz00(BgL_tablez00_156, BgL_keyz00_2416); 
{ /* Llib/hash.scm 1105 */
 long BgL_hz00_2418;
{ /* Llib/hash.scm 1106 */
 long BgL_n1z00_4920; long BgL_n2z00_4921;
BgL_n1z00_4920 = BgL_nz00_2417; 
BgL_n2z00_4921 = BgL_newzd2lenzd2_2395; 
{ /* Llib/hash.scm 1106 */
 bool_t BgL_test5254z00_13438;
{ /* Llib/hash.scm 1106 */
 long BgL_arg2221z00_4923;
BgL_arg2221z00_4923 = 
(((BgL_n1z00_4920) | (BgL_n2z00_4921)) & -2147483648); 
BgL_test5254z00_13438 = 
(BgL_arg2221z00_4923==0L); } 
if(BgL_test5254z00_13438)
{ /* Llib/hash.scm 1106 */
 int32_t BgL_arg2218z00_4924;
{ /* Llib/hash.scm 1106 */
 int32_t BgL_arg2219z00_4925; int32_t BgL_arg2220z00_4926;
BgL_arg2219z00_4925 = 
(int32_t)(BgL_n1z00_4920); 
BgL_arg2220z00_4926 = 
(int32_t)(BgL_n2z00_4921); 
BgL_arg2218z00_4924 = 
(BgL_arg2219z00_4925%BgL_arg2220z00_4926); } 
{ /* Llib/hash.scm 1106 */
 long BgL_arg2326z00_4931;
BgL_arg2326z00_4931 = 
(long)(BgL_arg2218z00_4924); 
BgL_hz00_2418 = 
(long)(BgL_arg2326z00_4931); } }  else 
{ /* Llib/hash.scm 1106 */
BgL_hz00_2418 = 
(BgL_n1z00_4920%BgL_n2z00_4921); } } } 
{ /* Llib/hash.scm 1106 */

{ /* Llib/hash.scm 1108 */
 obj_t BgL_arg1937z00_2419;
{ /* Llib/hash.scm 1108 */
 obj_t BgL_arg1938z00_2420;
{ /* Llib/hash.scm 1108 */
 bool_t BgL_test5255z00_13447;
{ /* Llib/hash.scm 1108 */
 long BgL_tmpz00_13448;
BgL_tmpz00_13448 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2405); 
BgL_test5255z00_13447 = 
BOUND_CHECK(BgL_hz00_2418, BgL_tmpz00_13448); } 
if(BgL_test5255z00_13447)
{ /* Llib/hash.scm 1108 */
BgL_arg1938z00_2420 = 
VECTOR_REF(BgL_newzd2buckszd2_2405,BgL_hz00_2418); }  else 
{ 
 obj_t BgL_auxz00_13452;
BgL_auxz00_13452 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(44696L), BGl_string3881z00zz__hashz00, BgL_newzd2buckszd2_2405, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2405)), 
(int)(BgL_hz00_2418)); 
FAILURE(BgL_auxz00_13452,BFALSE,BFALSE);} } 
BgL_arg1937z00_2419 = 
MAKE_YOUNG_PAIR(BgL_cellz00_2415, BgL_arg1938z00_2420); } 
VECTOR_SET(BgL_newzd2buckszd2_2405,BgL_hz00_2418,BgL_arg1937z00_2419); } } } } } } 
{ 
 obj_t BgL_l1140z00_13461;
BgL_l1140z00_13461 = 
CDR(BgL_l1140z00_2412); 
BgL_l1140z00_2412 = BgL_l1140z00_13461; 
goto BgL_zc3z04anonymousza31935ze3z87_2413;} }  else 
{ /* Llib/hash.scm 1109 */
if(
NULLP(BgL_l1140z00_2412))
{ /* Llib/hash.scm 1109 */BTRUE; }  else 
{ /* Llib/hash.scm 1109 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3978z00zz__hashz00, BGl_string3979z00zz__hashz00, BgL_l1140z00_2412, BGl_string3876z00zz__hashz00, 
BINT(44499L)); } } } } 
{ 
 long BgL_iz00_13467;
BgL_iz00_13467 = 
(BgL_iz00_2407+1L); 
BgL_iz00_2407 = BgL_iz00_13467; 
goto BgL_zc3z04anonymousza31933ze3z87_2408;} }  else 
{ /* Llib/hash.scm 1102 */
BgL_tmpz00_13417 = ((bool_t)0)
; } 
return 
BBOOL(BgL_tmpz00_13417);} } }  else 
{ /* Llib/hash.scm 1114 */
 obj_t BgL_arg1942z00_2426;
{ /* Llib/hash.scm 1114 */
 long BgL_arg1943z00_2427;
{ /* Llib/hash.scm 1114 */
 long BgL_res2651z00_4944;
{ /* Llib/hash.scm 318 */
 bool_t BgL_test5257z00_13470;
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_13471;
{ /* Llib/hash.scm 318 */
 obj_t BgL_res2650z00_4943;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3786z00_6680;
BgL_aux3786z00_6680 = 
STRUCT_KEY(BgL_tablez00_156); 
if(
SYMBOLP(BgL_aux3786z00_6680))
{ /* Llib/hash.scm 318 */
BgL_res2650z00_4943 = BgL_aux3786z00_6680; }  else 
{ 
 obj_t BgL_auxz00_13475;
BgL_auxz00_13475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string4079z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3786z00_6680); 
FAILURE(BgL_auxz00_13475,BFALSE,BFALSE);} } 
BgL_tmpz00_13471 = BgL_res2650z00_4943; } 
BgL_test5257z00_13470 = 
(BgL_tmpz00_13471==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5257z00_13470)
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_13480;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3788z00_6682;
{ /* Llib/hash.scm 318 */
 int BgL_tmpz00_13481;
BgL_tmpz00_13481 = 
(int)(0L); 
BgL_aux3788z00_6682 = 
STRUCT_REF(BgL_tablez00_156, BgL_tmpz00_13481); } 
if(
INTEGERP(BgL_aux3788z00_6682))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_13480 = BgL_aux3788z00_6682
; }  else 
{ 
 obj_t BgL_auxz00_13486;
BgL_auxz00_13486 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string4079z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3788z00_6682); 
FAILURE(BgL_auxz00_13486,BFALSE,BFALSE);} } 
BgL_res2651z00_4944 = 
(long)CINT(BgL_tmpz00_13480); }  else 
{ /* Llib/hash.scm 318 */
 obj_t BgL_tmpz00_13491;
{ /* Llib/hash.scm 318 */
 obj_t BgL_aux3789z00_6683;
BgL_aux3789z00_6683 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_156); 
if(
INTEGERP(BgL_aux3789z00_6683))
{ /* Llib/hash.scm 318 */
BgL_tmpz00_13491 = BgL_aux3789z00_6683
; }  else 
{ 
 obj_t BgL_auxz00_13495;
BgL_auxz00_13495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(12331L), BGl_string4079z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_aux3789z00_6683); 
FAILURE(BgL_auxz00_13495,BFALSE,BFALSE);} } 
BgL_res2651z00_4944 = 
(long)CINT(BgL_tmpz00_13491); } } 
BgL_arg1943z00_2427 = BgL_res2651z00_4944; } 
{ /* Llib/hash.scm 1112 */
 obj_t BgL_list1944z00_2428;
{ /* Llib/hash.scm 1112 */
 obj_t BgL_arg1945z00_2429;
{ /* Llib/hash.scm 1112 */
 obj_t BgL_arg1946z00_2430;
BgL_arg1946z00_2430 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1943z00_2427), BNIL); 
BgL_arg1945z00_2429 = 
MAKE_YOUNG_PAIR(BgL_maxzd2lenzd2_2396, BgL_arg1946z00_2430); } 
BgL_list1944z00_2428 = 
MAKE_YOUNG_PAIR(
BINT(BgL_newzd2lenzd2_2395), BgL_arg1945z00_2429); } 
BgL_arg1942z00_2426 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string4081z00zz__hashz00, BgL_list1944z00_2428); } } 
return 
BGl_errorz00zz__errorz00(BGl_string4023z00zz__hashz00, BgL_arg1942z00_2426, BgL_tablez00_156);} } } } } } } } 

}



/* hashtable-collisions */
BGL_EXPORTED_DEF obj_t BGl_hashtablezd2collisionszd2zz__hashz00(obj_t BgL_tablez00_157)
{
{ /* Llib/hash.scm 1120 */
if(
BGl_hashtablezd2weakzf3z21zz__hashz00(BgL_tablez00_157))
{ /* Llib/hash.scm 1121 */
return BNIL;}  else 
{ /* Llib/hash.scm 1121 */
BGL_TAIL return 
BGl_plainzd2hashtablezd2collisionsz00zz__hashz00(BgL_tablez00_157);} } 

}



/* &hashtable-collisions */
obj_t BGl_z62hashtablezd2collisionszb0zz__hashz00(obj_t BgL_envz00_5520, obj_t BgL_tablez00_5521)
{
{ /* Llib/hash.scm 1120 */
{ /* Llib/hash.scm 1121 */
 obj_t BgL_auxz00_13510;
if(
STRUCTP(BgL_tablez00_5521))
{ /* Llib/hash.scm 1121 */
BgL_auxz00_13510 = BgL_tablez00_5521
; }  else 
{ 
 obj_t BgL_auxz00_13513;
BgL_auxz00_13513 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(45206L), BGl_string4082z00zz__hashz00, BGl_string3924z00zz__hashz00, BgL_tablez00_5521); 
FAILURE(BgL_auxz00_13513,BFALSE,BFALSE);} 
return 
BGl_hashtablezd2collisionszd2zz__hashz00(BgL_auxz00_13510);} } 

}



/* plain-hashtable-collisions */
obj_t BGl_plainzd2hashtablezd2collisionsz00zz__hashz00(obj_t BgL_tablez00_158)
{
{ /* Llib/hash.scm 1128 */
{ /* Llib/hash.scm 1129 */
 obj_t BgL_bucketsz00_2433;
{ /* Llib/hash.scm 1129 */
 bool_t BgL_test5263z00_13518;
{ /* Llib/hash.scm 1129 */
 obj_t BgL_tmpz00_13519;
{ /* Llib/hash.scm 1129 */
 obj_t BgL_res2652z00_4950;
{ /* Llib/hash.scm 1129 */
 obj_t BgL_aux3792z00_6686;
BgL_aux3792z00_6686 = 
STRUCT_KEY(BgL_tablez00_158); 
if(
SYMBOLP(BgL_aux3792z00_6686))
{ /* Llib/hash.scm 1129 */
BgL_res2652z00_4950 = BgL_aux3792z00_6686; }  else 
{ 
 obj_t BgL_auxz00_13523;
BgL_auxz00_13523 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(45612L), BGl_string4083z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3792z00_6686); 
FAILURE(BgL_auxz00_13523,BFALSE,BFALSE);} } 
BgL_tmpz00_13519 = BgL_res2652z00_4950; } 
BgL_test5263z00_13518 = 
(BgL_tmpz00_13519==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5263z00_13518)
{ /* Llib/hash.scm 1129 */
 int BgL_tmpz00_13528;
BgL_tmpz00_13528 = 
(int)(2L); 
BgL_bucketsz00_2433 = 
STRUCT_REF(BgL_tablez00_158, BgL_tmpz00_13528); }  else 
{ /* Llib/hash.scm 1129 */
BgL_bucketsz00_2433 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tablez00_158); } } 
{ /* Llib/hash.scm 1129 */
 long BgL_bucketszd2lenzd2_2434;
{ /* Llib/hash.scm 1130 */
 obj_t BgL_vectorz00_4951;
if(
VECTORP(BgL_bucketsz00_2433))
{ /* Llib/hash.scm 1130 */
BgL_vectorz00_4951 = BgL_bucketsz00_2433; }  else 
{ 
 obj_t BgL_auxz00_13534;
BgL_auxz00_13534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(45671L), BGl_string4083z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2433); 
FAILURE(BgL_auxz00_13534,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_2434 = 
VECTOR_LENGTH(BgL_vectorz00_4951); } 
{ /* Llib/hash.scm 1130 */

{ 
 long BgL_iz00_2437; obj_t BgL_resz00_2438;
BgL_iz00_2437 = 0L; 
BgL_resz00_2438 = BNIL; 
BgL_zc3z04anonymousza31948ze3z87_2439:
if(
(BgL_iz00_2437==BgL_bucketszd2lenzd2_2434))
{ /* Llib/hash.scm 1133 */
return BgL_resz00_2438;}  else 
{ /* Llib/hash.scm 1135 */
 obj_t BgL_g1078z00_2441;
{ /* Llib/hash.scm 1135 */
 obj_t BgL_vectorz00_4954;
if(
VECTORP(BgL_bucketsz00_2433))
{ /* Llib/hash.scm 1135 */
BgL_vectorz00_4954 = BgL_bucketsz00_2433; }  else 
{ 
 obj_t BgL_auxz00_13543;
BgL_auxz00_13543 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(45809L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_bucketsz00_2433); 
FAILURE(BgL_auxz00_13543,BFALSE,BFALSE);} 
BgL_g1078z00_2441 = 
VECTOR_REF(BgL_vectorz00_4954,BgL_iz00_2437); } 
{ 
 obj_t BgL_bucketz00_2443; obj_t BgL_resz00_2444; long BgL_collz00_2445;
BgL_bucketz00_2443 = BgL_g1078z00_2441; 
BgL_resz00_2444 = BgL_resz00_2438; 
BgL_collz00_2445 = 0L; 
BgL_zc3z04anonymousza31950ze3z87_2446:
if(
NULLP(BgL_bucketz00_2443))
{ 
 obj_t BgL_resz00_13552; long BgL_iz00_13550;
BgL_iz00_13550 = 
(BgL_iz00_2437+1L); 
BgL_resz00_13552 = BgL_resz00_2444; 
BgL_resz00_2438 = BgL_resz00_13552; 
BgL_iz00_2437 = BgL_iz00_13550; 
goto BgL_zc3z04anonymousza31948ze3z87_2439;}  else 
{ /* Llib/hash.scm 1140 */
 obj_t BgL_arg1953z00_2449; obj_t BgL_arg1954z00_2450; long BgL_arg1955z00_2451;
{ /* Llib/hash.scm 1140 */
 obj_t BgL_pairz00_4957;
if(
PAIRP(BgL_bucketz00_2443))
{ /* Llib/hash.scm 1140 */
BgL_pairz00_4957 = BgL_bucketz00_2443; }  else 
{ 
 obj_t BgL_auxz00_13555;
BgL_auxz00_13555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(45912L), BGl_string3935z00zz__hashz00, BGl_string3936z00zz__hashz00, BgL_bucketz00_2443); 
FAILURE(BgL_auxz00_13555,BFALSE,BFALSE);} 
BgL_arg1953z00_2449 = 
CDR(BgL_pairz00_4957); } 
if(
(BgL_collz00_2445>0L))
{ /* Llib/hash.scm 1141 */
BgL_arg1954z00_2450 = 
MAKE_YOUNG_PAIR(
BINT(BgL_collz00_2445), BgL_resz00_2444); }  else 
{ /* Llib/hash.scm 1141 */
BgL_arg1954z00_2450 = BgL_resz00_2444; } 
BgL_arg1955z00_2451 = 
(BgL_collz00_2445+1L); 
{ 
 long BgL_collz00_13567; obj_t BgL_resz00_13566; obj_t BgL_bucketz00_13565;
BgL_bucketz00_13565 = BgL_arg1953z00_2449; 
BgL_resz00_13566 = BgL_arg1954z00_2450; 
BgL_collz00_13567 = BgL_arg1955z00_2451; 
BgL_collz00_2445 = BgL_collz00_13567; 
BgL_resz00_2444 = BgL_resz00_13566; 
BgL_bucketz00_2443 = BgL_bucketz00_13565; 
goto BgL_zc3z04anonymousza31950ze3z87_2446;} } } } } } } } } 

}



/* get-hashnumber */
BGL_EXPORTED_DEF long BGl_getzd2hashnumberzd2zz__hashz00(obj_t BgL_keyz00_160)
{
{ /* Llib/hash.scm 1153 */
BGl_getzd2hashnumberzd2zz__hashz00:
if(
STRINGP(BgL_keyz00_160))
{ /* Llib/hash.scm 1156 */
 long BgL_arg1958z00_2456;
{ /* Llib/hash.scm 1267 */
 long BgL_arg2034z00_4961;
BgL_arg2034z00_4961 = 
STRING_LENGTH(BgL_keyz00_160); 
BgL_arg1958z00_2456 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_keyz00_160), 
(int)(0L), 
(int)(BgL_arg2034z00_4961)); } 
if(
(BgL_arg1958z00_2456<0L))
{ /* Llib/hash.scm 1156 */
return 
NEG(BgL_arg1958z00_2456);}  else 
{ /* Llib/hash.scm 1156 */
return BgL_arg1958z00_2456;} }  else 
{ /* Llib/hash.scm 1155 */
if(
SYMBOLP(BgL_keyz00_160))
{ /* Llib/hash.scm 1158 */
 long BgL_arg1960z00_2458;
BgL_arg1960z00_2458 = 
bgl_symbol_hash_number(BgL_keyz00_160); 
if(
(BgL_arg1960z00_2458<0L))
{ /* Llib/hash.scm 1158 */
return 
NEG(BgL_arg1960z00_2458);}  else 
{ /* Llib/hash.scm 1158 */
return BgL_arg1960z00_2458;} }  else 
{ /* Llib/hash.scm 1157 */
if(
KEYWORDP(BgL_keyz00_160))
{ /* Llib/hash.scm 1160 */
 long BgL_arg1962z00_2460;
BgL_arg1962z00_2460 = 
bgl_keyword_hash_number(BgL_keyz00_160); 
if(
(BgL_arg1962z00_2460<0L))
{ /* Llib/hash.scm 1160 */
return 
NEG(BgL_arg1962z00_2460);}  else 
{ /* Llib/hash.scm 1160 */
return BgL_arg1962z00_2460;} }  else 
{ /* Llib/hash.scm 1159 */
if(
INTEGERP(BgL_keyz00_160))
{ /* Llib/hash.scm 1162 */
 long BgL_nz00_4975;
BgL_nz00_4975 = 
(long)CINT(BgL_keyz00_160); 
if(
(BgL_nz00_4975<0L))
{ /* Llib/hash.scm 1162 */
return 
NEG(BgL_nz00_4975);}  else 
{ /* Llib/hash.scm 1162 */
return BgL_nz00_4975;} }  else 
{ /* Llib/hash.scm 1161 */
if(
ELONGP(BgL_keyz00_160))
{ /* Llib/hash.scm 1164 */
 long BgL_arg1965z00_2463;
{ /* Llib/hash.scm 1164 */
 long BgL_tmpz00_13598;
BgL_tmpz00_13598 = 
BELONG_TO_LONG(BgL_keyz00_160); 
BgL_arg1965z00_2463 = 
(long)(BgL_tmpz00_13598); } 
if(
(BgL_arg1965z00_2463<0L))
{ /* Llib/hash.scm 1164 */
return 
NEG(BgL_arg1965z00_2463);}  else 
{ /* Llib/hash.scm 1164 */
return BgL_arg1965z00_2463;} }  else 
{ /* Llib/hash.scm 1163 */
if(
LLONGP(BgL_keyz00_160))
{ /* Llib/hash.scm 1166 */
 long BgL_arg1967z00_2465;
{ /* Llib/hash.scm 1166 */
 BGL_LONGLONG_T BgL_tmpz00_13606;
BgL_tmpz00_13606 = 
BLLONG_TO_LLONG(BgL_keyz00_160); 
BgL_arg1967z00_2465 = 
(long)(BgL_tmpz00_13606); } 
if(
(BgL_arg1967z00_2465<0L))
{ /* Llib/hash.scm 1166 */
return 
NEG(BgL_arg1967z00_2465);}  else 
{ /* Llib/hash.scm 1166 */
return BgL_arg1967z00_2465;} }  else 
{ /* Llib/hash.scm 1165 */
if(
BGL_OBJECTP(BgL_keyz00_160))
{ /* Llib/hash.scm 1168 */
 long BgL_arg1969z00_2467;
BgL_arg1969z00_2467 = 
BGl_objectzd2hashnumberzd2zz__objectz00(
((BgL_objectz00_bglt)BgL_keyz00_160)); 
if(
(BgL_arg1969z00_2467<0L))
{ /* Llib/hash.scm 1168 */
return 
NEG(BgL_arg1969z00_2467);}  else 
{ /* Llib/hash.scm 1168 */
return BgL_arg1969z00_2467;} }  else 
{ /* Llib/hash.scm 1167 */
if(
FOREIGNP(BgL_keyz00_160))
{ /* Llib/hash.scm 1170 */
 long BgL_arg1971z00_2469;
BgL_arg1971z00_2469 = 
bgl_foreign_hash_number(BgL_keyz00_160); 
if(
(BgL_arg1971z00_2469<0L))
{ /* Llib/hash.scm 1170 */
return 
NEG(BgL_arg1971z00_2469);}  else 
{ /* Llib/hash.scm 1170 */
return BgL_arg1971z00_2469;} }  else 
{ /* Llib/hash.scm 1171 */
 bool_t BgL_test5287z00_13625;
if(
INTEGERP(BgL_keyz00_160))
{ /* Llib/hash.scm 1171 */
BgL_test5287z00_13625 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 1171 */
BgL_test5287z00_13625 = 
REALP(BgL_keyz00_160)
; } 
if(BgL_test5287z00_13625)
{ /* Llib/hash.scm 1172 */
 long BgL_arg1973z00_2471;
{ /* Llib/hash.scm 1172 */
 double BgL_xz00_4995;
{ /* Llib/hash.scm 1172 */
 obj_t BgL_tmpz00_13629;
if(
REALP(BgL_keyz00_160))
{ /* Llib/hash.scm 1172 */
BgL_tmpz00_13629 = BgL_keyz00_160
; }  else 
{ 
 obj_t BgL_auxz00_13632;
BgL_auxz00_13632 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(47058L), BGl_string4084z00zz__hashz00, BGl_string4085z00zz__hashz00, BgL_keyz00_160); 
FAILURE(BgL_auxz00_13632,BFALSE,BFALSE);} 
BgL_xz00_4995 = 
REAL_TO_DOUBLE(BgL_tmpz00_13629); } 
BgL_arg1973z00_2471 = 
(long)(BgL_xz00_4995); } 
{ 
 obj_t BgL_keyz00_13638;
BgL_keyz00_13638 = 
BINT(BgL_arg1973z00_2471); 
BgL_keyz00_160 = BgL_keyz00_13638; 
goto BGl_getzd2hashnumberzd2zz__hashz00;} }  else 
{ /* Llib/hash.scm 1174 */
 long BgL_arg1974z00_2472;
BgL_arg1974z00_2472 = 
bgl_obj_hash_number(BgL_keyz00_160); 
if(
(BgL_arg1974z00_2472<0L))
{ /* Llib/hash.scm 1174 */
return 
NEG(BgL_arg1974z00_2472);}  else 
{ /* Llib/hash.scm 1174 */
return BgL_arg1974z00_2472;} } } } } } } } } } } 

}



/* &get-hashnumber */
obj_t BGl_z62getzd2hashnumberzb0zz__hashz00(obj_t BgL_envz00_5522, obj_t BgL_keyz00_5523)
{
{ /* Llib/hash.scm 1153 */
return 
BINT(
BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_5523));} 

}



/* get-hashnumber-persistent */
BGL_EXPORTED_DEF long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t BgL_keyz00_161)
{
{ /* Llib/hash.scm 1181 */
BGL_TAIL return 
BGl_objzd2hashze70z35zz__hashz00(BgL_keyz00_161);} 

}



/* obj-hash~0 */
long BGl_objzd2hashze70z35zz__hashz00(obj_t BgL_keyz00_2521)
{
{ /* Llib/hash.scm 1247 */
BGl_objzd2hashze70z35zz__hashz00:
{ 
 obj_t BgL_keyz00_2479; obj_t BgL_keyz00_2504;
if(
CNSTP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1212 */
if(
(BgL_keyz00_2521==BTRUE))
{ /* Llib/hash.scm 1214 */
return 12L;}  else 
{ /* Llib/hash.scm 1214 */
if(
(BgL_keyz00_2521==BFALSE))
{ /* Llib/hash.scm 1215 */
return 445L;}  else 
{ /* Llib/hash.scm 1215 */
if(
(BgL_keyz00_2521==BUNSPEC))
{ /* Llib/hash.scm 1216 */
return 3199L;}  else 
{ /* Llib/hash.scm 1216 */
if(
(BgL_keyz00_2521==BNIL))
{ /* Llib/hash.scm 1217 */
return 453343L;}  else 
{ /* Llib/hash.scm 1217 */
return 21354L;} } } } }  else 
{ /* Llib/hash.scm 1212 */
if(
STRINGP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1220 */
 long BgL_arg1999z00_2525;
{ /* Llib/hash.scm 1220 */
 long BgL_arg2000z00_2526;
BgL_arg2000z00_2526 = 
STRING_LENGTH(BgL_keyz00_2521); 
BgL_arg1999z00_2525 = 
bgl_string_hash_persistent(
BSTRING_TO_STRING(BgL_keyz00_2521), 
(int)(0L), 
(int)(BgL_arg2000z00_2526)); } 
return 
(134217727L & BgL_arg1999z00_2525);}  else 
{ /* Llib/hash.scm 1219 */
if(
SYMBOLP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1221 */
return 
(134217727L & 
bgl_symbol_hash_number_persistent(BgL_keyz00_2521));}  else 
{ /* Llib/hash.scm 1221 */
if(
KEYWORDP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1223 */
return 
(134217727L & 
bgl_keyword_hash_number_persistent(BgL_keyz00_2521));}  else 
{ /* Llib/hash.scm 1223 */
if(
CHARP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1225 */
return 
(
CCHAR(BgL_keyz00_2521));}  else 
{ /* Llib/hash.scm 1225 */
if(
INTEGERP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1227 */
return 
(134217727L & 
(long)CINT(BgL_keyz00_2521));}  else 
{ /* Llib/hash.scm 1227 */
if(
ELONGP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1230 */
 long BgL_arg2008z00_2534;
{ /* Llib/hash.scm 1230 */
 long BgL_tmpz00_13683;
BgL_tmpz00_13683 = 
BELONG_TO_LONG(BgL_keyz00_2521); 
BgL_arg2008z00_2534 = 
(long)(BgL_tmpz00_13683); } 
return 
(134217727L & BgL_arg2008z00_2534);}  else 
{ /* Llib/hash.scm 1229 */
if(
LLONGP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1232 */
 long BgL_arg2010z00_2536;
{ /* Llib/hash.scm 1232 */
 BGL_LONGLONG_T BgL_tmpz00_13689;
BgL_tmpz00_13689 = 
BLLONG_TO_LLONG(BgL_keyz00_2521); 
BgL_arg2010z00_2536 = 
(long)(BgL_tmpz00_13689); } 
return 
(134217727L & BgL_arg2010z00_2536);}  else 
{ /* Llib/hash.scm 1231 */
if(
UCS2P(BgL_keyz00_2521))
{ /* Llib/hash.scm 1234 */
 long BgL_arg2012z00_2538;
{ /* Llib/hash.scm 1234 */
 int BgL_arg2013z00_2539;
{ /* Llib/hash.scm 1234 */
 ucs2_t BgL_ucs2z00_5047;
BgL_ucs2z00_5047 = 
CUCS2(BgL_keyz00_2521); 
{ /* Llib/hash.scm 1234 */
 obj_t BgL_tmpz00_13696;
BgL_tmpz00_13696 = 
BUCS2(BgL_ucs2z00_5047); 
BgL_arg2013z00_2539 = 
CUCS2(BgL_tmpz00_13696); } } 
BgL_arg2012z00_2538 = 
(39434L ^ 
(long)(BgL_arg2013z00_2539)); } 
return 
(134217727L & BgL_arg2012z00_2538);}  else 
{ /* Llib/hash.scm 1233 */
if(
BGL_DATEP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1236 */
 long BgL_arg2015z00_2541;
{ /* Llib/hash.scm 1236 */
 long BgL_arg2016z00_2542;
{ /* Llib/hash.scm 1236 */
 long BgL_arg2017z00_2543;
BgL_arg2017z00_2543 = 
bgl_date_to_seconds(BgL_keyz00_2521); 
BgL_arg2016z00_2542 = 
BGl_objzd2hashze70z35zz__hashz00(
make_belong(BgL_arg2017z00_2543)); } 
BgL_arg2015z00_2541 = 
(908L ^ BgL_arg2016z00_2542); } 
return 
(134217727L & BgL_arg2015z00_2541);}  else 
{ /* Llib/hash.scm 1237 */
 bool_t BgL_test5305z00_13709;
if(
INTEGERP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1237 */
BgL_test5305z00_13709 = ((bool_t)1)
; }  else 
{ /* Llib/hash.scm 1237 */
BgL_test5305z00_13709 = 
REALP(BgL_keyz00_2521)
; } 
if(BgL_test5305z00_13709)
{ /* Llib/hash.scm 1240 */
 long BgL_arg2019z00_2545;
{ /* Llib/hash.scm 1240 */
 int64_t BgL_arg2020z00_2546;
{ /* Llib/hash.scm 1240 */
 int64_t BgL_arg2021z00_2547; int64_t BgL_arg2022z00_2548;
{ /* Llib/hash.scm 1240 */
 double BgL_arg2024z00_2549;
{ /* Llib/hash.scm 1240 */
 double BgL_r1z00_5053;
{ /* Llib/hash.scm 1240 */
 obj_t BgL_tmpz00_13713;
if(
REALP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1240 */
BgL_tmpz00_13713 = BgL_keyz00_2521
; }  else 
{ 
 obj_t BgL_auxz00_13716;
BgL_auxz00_13716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49092L), BGl_string4096z00zz__hashz00, BGl_string4085z00zz__hashz00, BgL_keyz00_2521); 
FAILURE(BgL_auxz00_13716,BFALSE,BFALSE);} 
BgL_r1z00_5053 = 
REAL_TO_DOUBLE(BgL_tmpz00_13713); } 
BgL_arg2024z00_2549 = 
(BgL_r1z00_5053*((double)1000.0)); } 
BgL_arg2021z00_2547 = 
(int64_t)(BgL_arg2024z00_2549); } 
BgL_arg2022z00_2548 = 
((int64_t)(1) << 
(int)(29L)); 
BgL_arg2020z00_2546 = 
(BgL_arg2021z00_2547 & BgL_arg2022z00_2548); } 
{ /* Llib/hash.scm 1239 */
 long BgL_arg2325z00_5060;
BgL_arg2325z00_5060 = 
(long)(BgL_arg2020z00_2546); 
BgL_arg2019z00_2545 = 
(long)(BgL_arg2325z00_5060); } } 
{ 
 obj_t BgL_keyz00_13728;
BgL_keyz00_13728 = 
BINT(BgL_arg2019z00_2545); 
BgL_keyz00_2521 = BgL_keyz00_13728; 
goto BGl_objzd2hashze70z35zz__hashz00;} }  else 
{ /* Llib/hash.scm 1237 */
if(
UCS2_STRINGP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1184 */
 long BgL_tmpz00_13732;
BgL_keyz00_2504 = BgL_keyz00_2521; 
{ /* Llib/hash.scm 1200 */
 int BgL_lenz00_2506;
BgL_lenz00_2506 = 
UCS2_STRING_LENGTH(BgL_keyz00_2504); 
{ /* Llib/hash.scm 1201 */
 long BgL_g1081z00_2507; long BgL_g1082z00_2508;
BgL_g1081z00_2507 = 
(
(long)(BgL_lenz00_2506)-1L); 
BgL_g1082z00_2508 = 
(134217727L & 
(235643L ^ 
(long)(BgL_lenz00_2506))); 
{ 
 long BgL_iz00_2510; long BgL_accz00_2511;
BgL_iz00_2510 = BgL_g1081z00_2507; 
BgL_accz00_2511 = BgL_g1082z00_2508; 
BgL_zc3z04anonymousza31988ze3z87_2512:
if(
(BgL_iz00_2510==-1L))
{ /* Llib/hash.scm 1203 */
BgL_tmpz00_13732 = BgL_accz00_2511
; }  else 
{ /* Llib/hash.scm 1205 */
 long BgL_arg1990z00_2514; long BgL_arg1991z00_2515;
BgL_arg1990z00_2514 = 
(BgL_iz00_2510-1L); 
{ /* Llib/hash.scm 1208 */
 long BgL_arg1992z00_2516;
{ /* Llib/hash.scm 1208 */
 long BgL_arg1993z00_2517;
{ /* Llib/hash.scm 1208 */
 ucs2_t BgL_arg1994z00_2518;
{ /* Llib/hash.scm 1208 */
 ucs2_t BgL_res2653z00_5035;
{ /* Llib/hash.scm 1208 */
 int BgL_kz00_5023;
BgL_kz00_5023 = 
(int)(BgL_iz00_2510); 
{ /* Llib/hash.scm 1208 */
 bool_t BgL_test5310z00_13743;
{ /* Llib/hash.scm 1208 */
 long BgL_auxz00_13746; long BgL_tmpz00_13744;
BgL_auxz00_13746 = 
(long)(
UCS2_STRING_LENGTH(BgL_keyz00_2504)); 
BgL_tmpz00_13744 = 
(long)(BgL_kz00_5023); 
BgL_test5310z00_13743 = 
BOUND_CHECK(BgL_tmpz00_13744, BgL_auxz00_13746); } 
if(BgL_test5310z00_13743)
{ /* Llib/hash.scm 1208 */
BgL_res2653z00_5035 = 
UCS2_STRING_REF(BgL_keyz00_2504, BgL_kz00_5023); }  else 
{ /* Llib/hash.scm 1208 */
 obj_t BgL_arg2386z00_5026;
{ /* Llib/hash.scm 1208 */
 obj_t BgL_arg2387z00_5027;
{ /* Llib/hash.scm 1208 */

BgL_arg2387z00_5027 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(
UCS2_STRING_LENGTH(BgL_keyz00_2504))-1L), 10L); } 
BgL_arg2386z00_5026 = 
string_append_3(BGl_string4086z00zz__hashz00, BgL_arg2387z00_5027, BGl_string4087z00zz__hashz00); } 
{ /* Llib/hash.scm 1208 */
 obj_t BgL_tmpz00_13756;
{ /* Llib/hash.scm 1208 */
 obj_t BgL_aux3802z00_6696;
BgL_aux3802z00_6696 = 
BGl_errorz00zz__errorz00(BGl_symbol4088z00zz__hashz00, BgL_arg2386z00_5026, 
BINT(BgL_kz00_5023)); 
if(
UCS2P(BgL_aux3802z00_6696))
{ /* Llib/hash.scm 1208 */
BgL_tmpz00_13756 = BgL_aux3802z00_6696
; }  else 
{ 
 obj_t BgL_auxz00_13761;
BgL_auxz00_13761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(48264L), BGl_string3931z00zz__hashz00, BGl_string4090z00zz__hashz00, BgL_aux3802z00_6696); 
FAILURE(BgL_auxz00_13761,BFALSE,BFALSE);} } 
BgL_res2653z00_5035 = 
CUCS2(BgL_tmpz00_13756); } } } } 
BgL_arg1994z00_2518 = BgL_res2653z00_5035; } 
BgL_arg1993z00_2517 = 
BGl_objzd2hashze70z35zz__hashz00(
BUCS2(BgL_arg1994z00_2518)); } 
BgL_arg1992z00_2516 = 
(BgL_arg1993z00_2517 ^ BgL_accz00_2511); } 
BgL_arg1991z00_2515 = 
(134217727L & BgL_arg1992z00_2516); } 
{ 
 long BgL_accz00_13771; long BgL_iz00_13770;
BgL_iz00_13770 = BgL_arg1990z00_2514; 
BgL_accz00_13771 = BgL_arg1991z00_2515; 
BgL_accz00_2511 = BgL_accz00_13771; 
BgL_iz00_2510 = BgL_iz00_13770; 
goto BgL_zc3z04anonymousza31988ze3z87_2512;} } } } } 
return 
(134217727L & BgL_tmpz00_13732);}  else 
{ /* Llib/hash.scm 1242 */
if(
BGL_HVECTORP(BgL_keyz00_2521))
{ /* Llib/hash.scm 1244 */
BgL_keyz00_2479 = BgL_keyz00_2521; 
{ /* Llib/hash.scm 1187 */
 long BgL_lenz00_2481;
BgL_lenz00_2481 = 
BGL_HVECTOR_LENGTH(BgL_keyz00_2479); 
{ /* Llib/hash.scm 1188 */
 obj_t BgL_tagz00_2482;
BgL_tagz00_2482 = 
BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_keyz00_2479); 
{ /* Llib/hash.scm 1189 */
 obj_t BgL__z00_2483; obj_t BgL_getz00_2484; obj_t BgL__z00_2485; obj_t BgL__z00_2486;
{ /* Llib/hash.scm 1190 */
 obj_t BgL_tmpz00_5001;
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13777;
BgL_tmpz00_13777 = 
(int)(1L); 
BgL_tmpz00_5001 = 
BGL_MVALUES_VAL(BgL_tmpz00_13777); } 
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13780;
BgL_tmpz00_13780 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13780, BUNSPEC); } 
BgL__z00_2483 = BgL_tmpz00_5001; } 
{ /* Llib/hash.scm 1190 */
 obj_t BgL_tmpz00_5002;
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13783;
BgL_tmpz00_13783 = 
(int)(2L); 
BgL_tmpz00_5002 = 
BGL_MVALUES_VAL(BgL_tmpz00_13783); } 
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13786;
BgL_tmpz00_13786 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13786, BUNSPEC); } 
BgL_getz00_2484 = BgL_tmpz00_5002; } 
{ /* Llib/hash.scm 1190 */
 obj_t BgL_tmpz00_5003;
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13789;
BgL_tmpz00_13789 = 
(int)(3L); 
BgL_tmpz00_5003 = 
BGL_MVALUES_VAL(BgL_tmpz00_13789); } 
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13792;
BgL_tmpz00_13792 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13792, BUNSPEC); } 
BgL__z00_2485 = BgL_tmpz00_5003; } 
{ /* Llib/hash.scm 1190 */
 obj_t BgL_tmpz00_5004;
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13795;
BgL_tmpz00_13795 = 
(int)(4L); 
BgL_tmpz00_5004 = 
BGL_MVALUES_VAL(BgL_tmpz00_13795); } 
{ /* Llib/hash.scm 1190 */
 int BgL_tmpz00_13798;
BgL_tmpz00_13798 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_13798, BUNSPEC); } 
BgL__z00_2486 = BgL_tmpz00_5004; } 
{ /* Llib/hash.scm 1190 */
 long BgL_g1079z00_2487; long BgL_g1080z00_2488;
BgL_g1079z00_2487 = 
(BgL_lenz00_2481-1L); 
BgL_g1080z00_2488 = 
(134217727L & 
(98723L ^ 
(134217727L & 
(BgL_lenz00_2481 ^ 
BGl_objzd2hashze70z35zz__hashz00(BgL_tagz00_2482))))); 
{ 
 long BgL_iz00_2490; long BgL_accz00_2491;
BgL_iz00_2490 = BgL_g1079z00_2487; 
BgL_accz00_2491 = BgL_g1080z00_2488; 
BgL_zc3z04anonymousza31977ze3z87_2492:
if(
(BgL_iz00_2490==-1L))
{ /* Llib/hash.scm 1193 */
return BgL_accz00_2491;}  else 
{ /* Llib/hash.scm 1195 */
 obj_t BgL_oz00_2494;
{ /* Llib/hash.scm 1195 */
 obj_t BgL_funz00_6700;
if(
PROCEDUREP(BgL_getz00_2484))
{ /* Llib/hash.scm 1195 */
BgL_funz00_6700 = BgL_getz00_2484; }  else 
{ 
 obj_t BgL_auxz00_13811;
BgL_auxz00_13811 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(47937L), BGl_string3931z00zz__hashz00, BGl_string3944z00zz__hashz00, BgL_getz00_2484); 
FAILURE(BgL_auxz00_13811,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_6700, 2))
{ /* Llib/hash.scm 1195 */
BgL_oz00_2494 = 
(VA_PROCEDUREP( BgL_funz00_6700 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_6700))(BgL_getz00_2484, BgL_keyz00_2479, 
BINT(BgL_iz00_2490), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_6700))(BgL_getz00_2484, BgL_keyz00_2479, 
BINT(BgL_iz00_2490)) ); }  else 
{ /* Llib/hash.scm 1195 */
FAILURE(BGl_string3946z00zz__hashz00,BGl_list4091z00zz__hashz00,BgL_funz00_6700);} } 
{ 
 long BgL_accz00_13826; long BgL_iz00_13824;
BgL_iz00_13824 = 
(BgL_iz00_2490-1L); 
BgL_accz00_13826 = 
(134217727L & 
(BgL_accz00_2491 ^ 
BGl_objzd2hashze70z35zz__hashz00(BgL_oz00_2494))); 
BgL_accz00_2491 = BgL_accz00_13826; 
BgL_iz00_2490 = BgL_iz00_13824; 
goto BgL_zc3z04anonymousza31977ze3z87_2492;} } } } } } } }  else 
{ /* Llib/hash.scm 1247 */
 obj_t BgL_arg2029z00_2553;
{ /* Llib/hash.scm 1247 */

BgL_arg2029z00_2553 = 
obj_to_string(BgL_keyz00_2521, BFALSE); } 
{ 
 obj_t BgL_keyz00_13831;
BgL_keyz00_13831 = BgL_arg2029z00_2553; 
BgL_keyz00_2521 = BgL_keyz00_13831; 
goto BGl_objzd2hashze70z35zz__hashz00;} } } } } } } } } } } } } } } } 

}



/* &get-hashnumber-persistent */
obj_t BGl_z62getzd2hashnumberzd2persistentz62zz__hashz00(obj_t BgL_envz00_5524, obj_t BgL_keyz00_5525)
{
{ /* Llib/hash.scm 1181 */
return 
BINT(
BGl_getzd2hashnumberzd2persistentz00zz__hashz00(BgL_keyz00_5525));} 

}



/* get-pointer-hashnumber */
BGL_EXPORTED_DEF long BGl_getzd2pointerzd2hashnumberz00zz__hashz00(obj_t BgL_ptrz00_162, long BgL_powerz00_163)
{
{ /* Llib/hash.scm 1254 */
BGL_TAIL return 
bgl_pointer_hashnumber(BgL_ptrz00_162, BgL_powerz00_163);} 

}



/* &get-pointer-hashnumber */
obj_t BGl_z62getzd2pointerzd2hashnumberz62zz__hashz00(obj_t BgL_envz00_5526, obj_t BgL_ptrz00_5527, obj_t BgL_powerz00_5528)
{
{ /* Llib/hash.scm 1254 */
{ /* Llib/hash.scm 1255 */
 long BgL_tmpz00_13835;
{ /* Llib/hash.scm 1255 */
 long BgL_auxz00_13836;
{ /* Llib/hash.scm 1255 */
 obj_t BgL_tmpz00_13837;
if(
INTEGERP(BgL_powerz00_5528))
{ /* Llib/hash.scm 1255 */
BgL_tmpz00_13837 = BgL_powerz00_5528
; }  else 
{ 
 obj_t BgL_auxz00_13840;
BgL_auxz00_13840 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49622L), BGl_string4097z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_powerz00_5528); 
FAILURE(BgL_auxz00_13840,BFALSE,BFALSE);} 
BgL_auxz00_13836 = 
(long)CINT(BgL_tmpz00_13837); } 
BgL_tmpz00_13835 = 
BGl_getzd2pointerzd2hashnumberz00zz__hashz00(BgL_ptrz00_5527, BgL_auxz00_13836); } 
return 
BINT(BgL_tmpz00_13835);} } 

}



/* _string-hash */
obj_t BGl__stringzd2hashzd2zz__hashz00(obj_t BgL_env1174z00_168, obj_t BgL_opt1173z00_167)
{
{ /* Llib/hash.scm 1260 */
{ /* Llib/hash.scm 1260 */
 obj_t BgL_g1175z00_2560;
BgL_g1175z00_2560 = 
VECTOR_REF(BgL_opt1173z00_167,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1173z00_167)) { case 1L : 

{ /* Llib/hash.scm 1260 */

{ /* Llib/hash.scm 1260 */
 obj_t BgL_stringz00_5063;
if(
STRINGP(BgL_g1175z00_2560))
{ /* Llib/hash.scm 1260 */
BgL_stringz00_5063 = BgL_g1175z00_2560; }  else 
{ 
 obj_t BgL_auxz00_13850;
BgL_auxz00_13850 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49879L), BGl_string4101z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_g1175z00_2560); 
FAILURE(BgL_auxz00_13850,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1261 */
 obj_t BgL_arg2033z00_5064;
BgL_arg2033z00_5064 = 
BINT(
STRING_LENGTH(BgL_stringz00_5063)); 
{ /* Llib/hash.scm 1261 */
 long BgL_tmpz00_13856;
{ /* Llib/hash.scm 1261 */
 int BgL_tmpz00_13857;
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13860;
if(
INTEGERP(BgL_arg2033z00_5064))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13860 = BgL_arg2033z00_5064
; }  else 
{ 
 obj_t BgL_auxz00_13863;
BgL_auxz00_13863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49993L), BGl_string4101z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2033z00_5064); 
FAILURE(BgL_auxz00_13863,BFALSE,BFALSE);} 
BgL_tmpz00_13857 = 
CINT(BgL_tmpz00_13860); } 
BgL_tmpz00_13856 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_stringz00_5063), 
(int)(0L), BgL_tmpz00_13857); } 
return 
BINT(BgL_tmpz00_13856);} } } } break;case 2L : 

{ /* Llib/hash.scm 1260 */
 obj_t BgL_startz00_2565;
BgL_startz00_2565 = 
VECTOR_REF(BgL_opt1173z00_167,1L); 
{ /* Llib/hash.scm 1260 */

{ /* Llib/hash.scm 1260 */
 obj_t BgL_stringz00_5067;
if(
STRINGP(BgL_g1175z00_2560))
{ /* Llib/hash.scm 1260 */
BgL_stringz00_5067 = BgL_g1175z00_2560; }  else 
{ 
 obj_t BgL_auxz00_13873;
BgL_auxz00_13873 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49879L), BGl_string4101z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_g1175z00_2560); 
FAILURE(BgL_auxz00_13873,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1261 */
 obj_t BgL_arg2033z00_5068;
BgL_arg2033z00_5068 = 
BINT(
STRING_LENGTH(BgL_stringz00_5067)); 
{ /* Llib/hash.scm 1261 */
 long BgL_tmpz00_13879;
{ /* Llib/hash.scm 1261 */
 int BgL_auxz00_13890; int BgL_tmpz00_13880;
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13891;
if(
INTEGERP(BgL_arg2033z00_5068))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13891 = BgL_arg2033z00_5068
; }  else 
{ 
 obj_t BgL_auxz00_13894;
BgL_auxz00_13894 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49993L), BGl_string4101z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2033z00_5068); 
FAILURE(BgL_auxz00_13894,BFALSE,BFALSE);} 
BgL_auxz00_13890 = 
CINT(BgL_tmpz00_13891); } 
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13882;
if(
INTEGERP(BgL_startz00_2565))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13882 = BgL_startz00_2565
; }  else 
{ 
 obj_t BgL_auxz00_13885;
BgL_auxz00_13885 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49957L), BGl_string4101z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_startz00_2565); 
FAILURE(BgL_auxz00_13885,BFALSE,BFALSE);} 
BgL_tmpz00_13880 = 
CINT(BgL_tmpz00_13882); } 
BgL_tmpz00_13879 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_stringz00_5067), BgL_tmpz00_13880, BgL_auxz00_13890); } 
return 
BINT(BgL_tmpz00_13879);} } } } } break;case 3L : 

{ /* Llib/hash.scm 1260 */
 obj_t BgL_startz00_2567;
BgL_startz00_2567 = 
VECTOR_REF(BgL_opt1173z00_167,1L); 
{ /* Llib/hash.scm 1260 */
 obj_t BgL_lenz00_2568;
BgL_lenz00_2568 = 
VECTOR_REF(BgL_opt1173z00_167,2L); 
{ /* Llib/hash.scm 1260 */

{ /* Llib/hash.scm 1260 */
 obj_t BgL_stringz00_5071;
if(
STRINGP(BgL_g1175z00_2560))
{ /* Llib/hash.scm 1260 */
BgL_stringz00_5071 = BgL_g1175z00_2560; }  else 
{ 
 obj_t BgL_auxz00_13905;
BgL_auxz00_13905 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49879L), BGl_string4101z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_g1175z00_2560); 
FAILURE(BgL_auxz00_13905,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1261 */
 obj_t BgL_arg2033z00_5072;
if(
CBOOL(BgL_lenz00_2568))
{ /* Llib/hash.scm 1261 */
BgL_arg2033z00_5072 = BgL_lenz00_2568; }  else 
{ /* Llib/hash.scm 1261 */
BgL_arg2033z00_5072 = 
BINT(
STRING_LENGTH(BgL_stringz00_5071)); } 
{ /* Llib/hash.scm 1261 */
 long BgL_tmpz00_13913;
{ /* Llib/hash.scm 1261 */
 int BgL_auxz00_13924; int BgL_tmpz00_13914;
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13925;
if(
INTEGERP(BgL_arg2033z00_5072))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13925 = BgL_arg2033z00_5072
; }  else 
{ 
 obj_t BgL_auxz00_13928;
BgL_auxz00_13928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49993L), BGl_string4101z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2033z00_5072); 
FAILURE(BgL_auxz00_13928,BFALSE,BFALSE);} 
BgL_auxz00_13924 = 
CINT(BgL_tmpz00_13925); } 
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13916;
if(
INTEGERP(BgL_startz00_2567))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13916 = BgL_startz00_2567
; }  else 
{ 
 obj_t BgL_auxz00_13919;
BgL_auxz00_13919 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49957L), BGl_string4101z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_startz00_2567); 
FAILURE(BgL_auxz00_13919,BFALSE,BFALSE);} 
BgL_tmpz00_13914 = 
CINT(BgL_tmpz00_13916); } 
BgL_tmpz00_13913 = 
bgl_string_hash(
BSTRING_TO_STRING(BgL_stringz00_5071), BgL_tmpz00_13914, BgL_auxz00_13924); } 
return 
BINT(BgL_tmpz00_13913);} } } } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol4098z00zz__hashz00, BGl_string4100z00zz__hashz00, 
BINT(
VECTOR_LENGTH(BgL_opt1173z00_167)));} } } } 

}



/* string-hash */
BGL_EXPORTED_DEF long BGl_stringzd2hashzd2zz__hashz00(obj_t BgL_stringz00_164, obj_t BgL_startz00_165, obj_t BgL_lenz00_166)
{
{ /* Llib/hash.scm 1260 */
{ /* Llib/hash.scm 1261 */
 obj_t BgL_arg2033z00_5075;
if(
CBOOL(BgL_lenz00_166))
{ /* Llib/hash.scm 1261 */
BgL_arg2033z00_5075 = BgL_lenz00_166; }  else 
{ /* Llib/hash.scm 1261 */
BgL_arg2033z00_5075 = 
BINT(
STRING_LENGTH(BgL_stringz00_164)); } 
{ /* Llib/hash.scm 1261 */
 int BgL_auxz00_13954; int BgL_tmpz00_13944;
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13955;
if(
INTEGERP(BgL_arg2033z00_5075))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13955 = BgL_arg2033z00_5075
; }  else 
{ 
 obj_t BgL_auxz00_13958;
BgL_auxz00_13958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49993L), BGl_string4099z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2033z00_5075); 
FAILURE(BgL_auxz00_13958,BFALSE,BFALSE);} 
BgL_auxz00_13954 = 
CINT(BgL_tmpz00_13955); } 
{ /* Llib/hash.scm 1261 */
 obj_t BgL_tmpz00_13946;
if(
INTEGERP(BgL_startz00_165))
{ /* Llib/hash.scm 1261 */
BgL_tmpz00_13946 = BgL_startz00_165
; }  else 
{ 
 obj_t BgL_auxz00_13949;
BgL_auxz00_13949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(49957L), BGl_string4099z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_startz00_165); 
FAILURE(BgL_auxz00_13949,BFALSE,BFALSE);} 
BgL_tmpz00_13944 = 
CINT(BgL_tmpz00_13946); } 
return 
bgl_string_hash(
BSTRING_TO_STRING(BgL_stringz00_164), BgL_tmpz00_13944, BgL_auxz00_13954);} } } 

}



/* string-hash-number */
BGL_EXPORTED_DEF long BGl_stringzd2hashzd2numberz00zz__hashz00(obj_t BgL_stringz00_169)
{
{ /* Llib/hash.scm 1266 */
{ /* Llib/hash.scm 1267 */
 long BgL_arg2034z00_6808;
BgL_arg2034z00_6808 = 
STRING_LENGTH(BgL_stringz00_169); 
return 
bgl_string_hash(
BSTRING_TO_STRING(BgL_stringz00_169), 
(int)(0L), 
(int)(BgL_arg2034z00_6808));} } 

}



/* &string-hash-number */
obj_t BGl_z62stringzd2hashzd2numberz62zz__hashz00(obj_t BgL_envz00_5529, obj_t BgL_stringz00_5530)
{
{ /* Llib/hash.scm 1266 */
{ /* Llib/hash.scm 1267 */
 long BgL_tmpz00_13969;
{ /* Llib/hash.scm 1267 */
 obj_t BgL_auxz00_13970;
if(
STRINGP(BgL_stringz00_5530))
{ /* Llib/hash.scm 1267 */
BgL_auxz00_13970 = BgL_stringz00_5530
; }  else 
{ 
 obj_t BgL_auxz00_13973;
BgL_auxz00_13973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50289L), BGl_string4102z00zz__hashz00, BGl_string3918z00zz__hashz00, BgL_stringz00_5530); 
FAILURE(BgL_auxz00_13973,BFALSE,BFALSE);} 
BgL_tmpz00_13969 = 
BGl_stringzd2hashzd2numberz00zz__hashz00(BgL_auxz00_13970); } 
return 
BINT(BgL_tmpz00_13969);} } 

}



/* open-string-hashtable-rehash! */
bool_t BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00(obj_t BgL_tz00_173)
{
{ /* Llib/hash.scm 1278 */
{ /* Llib/hash.scm 1279 */
 obj_t BgL_osiza7eza7_2573;
{ /* Llib/hash.scm 1279 */
 bool_t BgL_test5330z00_13979;
{ /* Llib/hash.scm 1279 */
 obj_t BgL_tmpz00_13980;
{ /* Llib/hash.scm 1279 */
 obj_t BgL_res2656z00_5091;
{ /* Llib/hash.scm 1279 */
 obj_t BgL_aux3825z00_6720;
BgL_aux3825z00_6720 = 
STRUCT_KEY(BgL_tz00_173); 
if(
SYMBOLP(BgL_aux3825z00_6720))
{ /* Llib/hash.scm 1279 */
BgL_res2656z00_5091 = BgL_aux3825z00_6720; }  else 
{ 
 obj_t BgL_auxz00_13984;
BgL_auxz00_13984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50955L), BGl_string4103z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3825z00_6720); 
FAILURE(BgL_auxz00_13984,BFALSE,BFALSE);} } 
BgL_tmpz00_13980 = BgL_res2656z00_5091; } 
BgL_test5330z00_13979 = 
(BgL_tmpz00_13980==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5330z00_13979)
{ /* Llib/hash.scm 1279 */
 int BgL_tmpz00_13989;
BgL_tmpz00_13989 = 
(int)(1L); 
BgL_osiza7eza7_2573 = 
STRUCT_REF(BgL_tz00_173, BgL_tmpz00_13989); }  else 
{ /* Llib/hash.scm 1279 */
BgL_osiza7eza7_2573 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_173); } } 
{ /* Llib/hash.scm 1279 */
 long BgL_osiza7e3za7_2574;
{ /* Llib/hash.scm 1280 */
 long BgL_za72za7_5092;
{ /* Llib/hash.scm 1280 */
 obj_t BgL_tmpz00_13993;
if(
INTEGERP(BgL_osiza7eza7_2573))
{ /* Llib/hash.scm 1280 */
BgL_tmpz00_13993 = BgL_osiza7eza7_2573
; }  else 
{ 
 obj_t BgL_auxz00_13996;
BgL_auxz00_13996 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51004L), BGl_string4103z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_osiza7eza7_2573); 
FAILURE(BgL_auxz00_13996,BFALSE,BFALSE);} 
BgL_za72za7_5092 = 
(long)CINT(BgL_tmpz00_13993); } 
BgL_osiza7e3za7_2574 = 
(3L*BgL_za72za7_5092); } 
{ /* Llib/hash.scm 1280 */
 obj_t BgL_obucketsz00_2575;
{ /* Llib/hash.scm 1281 */
 bool_t BgL_test5333z00_14002;
{ /* Llib/hash.scm 1281 */
 obj_t BgL_tmpz00_14003;
{ /* Llib/hash.scm 1281 */
 obj_t BgL_res2657z00_5096;
{ /* Llib/hash.scm 1281 */
 obj_t BgL_aux3828z00_6723;
BgL_aux3828z00_6723 = 
STRUCT_KEY(BgL_tz00_173); 
if(
SYMBOLP(BgL_aux3828z00_6723))
{ /* Llib/hash.scm 1281 */
BgL_res2657z00_5096 = BgL_aux3828z00_6723; }  else 
{ 
 obj_t BgL_auxz00_14007;
BgL_auxz00_14007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51025L), BGl_string4103z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3828z00_6723); 
FAILURE(BgL_auxz00_14007,BFALSE,BFALSE);} } 
BgL_tmpz00_14003 = BgL_res2657z00_5096; } 
BgL_test5333z00_14002 = 
(BgL_tmpz00_14003==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5333z00_14002)
{ /* Llib/hash.scm 1281 */
 int BgL_tmpz00_14012;
BgL_tmpz00_14012 = 
(int)(2L); 
BgL_obucketsz00_2575 = 
STRUCT_REF(BgL_tz00_173, BgL_tmpz00_14012); }  else 
{ /* Llib/hash.scm 1281 */
BgL_obucketsz00_2575 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_173); } } 
{ /* Llib/hash.scm 1281 */
 long BgL_nsiza7eza7_2576;
{ /* Llib/hash.scm 1282 */
 long BgL_tmpz00_14016;
{ /* Llib/hash.scm 1282 */
 long BgL_za71za7_5097;
{ /* Llib/hash.scm 1282 */
 obj_t BgL_tmpz00_14017;
if(
INTEGERP(BgL_osiza7eza7_2573))
{ /* Llib/hash.scm 1282 */
BgL_tmpz00_14017 = BgL_osiza7eza7_2573
; }  else 
{ 
 obj_t BgL_auxz00_14020;
BgL_auxz00_14020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51071L), BGl_string4103z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_osiza7eza7_2573); 
FAILURE(BgL_auxz00_14020,BFALSE,BFALSE);} 
BgL_za71za7_5097 = 
(long)CINT(BgL_tmpz00_14017); } 
BgL_tmpz00_14016 = 
(BgL_za71za7_5097*2L); } 
BgL_nsiza7eza7_2576 = 
(1L+BgL_tmpz00_14016); } 
{ /* Llib/hash.scm 1282 */
 obj_t BgL_nbucketsz00_2577;
BgL_nbucketsz00_2577 = 
make_vector(
(BgL_nsiza7eza7_2576*3L), BFALSE); 
{ /* Llib/hash.scm 1283 */

{ /* Llib/hash.scm 1284 */
 bool_t BgL_test5336z00_14029;
{ /* Llib/hash.scm 1284 */
 obj_t BgL_tmpz00_14030;
{ /* Llib/hash.scm 1284 */
 obj_t BgL_res2658z00_5103;
{ /* Llib/hash.scm 1284 */
 obj_t BgL_aux3831z00_6726;
BgL_aux3831z00_6726 = 
STRUCT_KEY(BgL_tz00_173); 
if(
SYMBOLP(BgL_aux3831z00_6726))
{ /* Llib/hash.scm 1284 */
BgL_res2658z00_5103 = BgL_aux3831z00_6726; }  else 
{ 
 obj_t BgL_auxz00_14034;
BgL_auxz00_14034 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51134L), BGl_string4103z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3831z00_6726); 
FAILURE(BgL_auxz00_14034,BFALSE,BFALSE);} } 
BgL_tmpz00_14030 = BgL_res2658z00_5103; } 
BgL_test5336z00_14029 = 
(BgL_tmpz00_14030==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5336z00_14029)
{ /* Llib/hash.scm 1284 */
 obj_t BgL_auxz00_14041; int BgL_tmpz00_14039;
BgL_auxz00_14041 = 
BINT(BgL_nsiza7eza7_2576); 
BgL_tmpz00_14039 = 
(int)(1L); 
STRUCT_SET(BgL_tz00_173, BgL_tmpz00_14039, BgL_auxz00_14041); }  else 
{ /* Llib/hash.scm 1284 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_173); } } 
{ /* Llib/hash.scm 1285 */
 bool_t BgL_test5338z00_14045;
{ /* Llib/hash.scm 1285 */
 obj_t BgL_tmpz00_14046;
{ /* Llib/hash.scm 1285 */
 obj_t BgL_res2659z00_5107;
{ /* Llib/hash.scm 1285 */
 obj_t BgL_aux3833z00_6728;
BgL_aux3833z00_6728 = 
STRUCT_KEY(BgL_tz00_173); 
if(
SYMBOLP(BgL_aux3833z00_6728))
{ /* Llib/hash.scm 1285 */
BgL_res2659z00_5107 = BgL_aux3833z00_6728; }  else 
{ 
 obj_t BgL_auxz00_14050;
BgL_auxz00_14050 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51181L), BGl_string4103z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3833z00_6728); 
FAILURE(BgL_auxz00_14050,BFALSE,BFALSE);} } 
BgL_tmpz00_14046 = BgL_res2659z00_5107; } 
BgL_test5338z00_14045 = 
(BgL_tmpz00_14046==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5338z00_14045)
{ /* Llib/hash.scm 1285 */
 int BgL_tmpz00_14055;
BgL_tmpz00_14055 = 
(int)(2L); 
STRUCT_SET(BgL_tz00_173, BgL_tmpz00_14055, BgL_nbucketsz00_2577); }  else 
{ /* Llib/hash.scm 1285 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_173); } } 
{ /* Llib/hash.scm 1273 */
 bool_t BgL_test5340z00_14059;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_tmpz00_14060;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_res2660z00_5111;
{ /* Llib/hash.scm 1273 */
 obj_t BgL_aux3835z00_6730;
BgL_aux3835z00_6730 = 
STRUCT_KEY(BgL_tz00_173); 
if(
SYMBOLP(BgL_aux3835z00_6730))
{ /* Llib/hash.scm 1273 */
BgL_res2660z00_5111 = BgL_aux3835z00_6730; }  else 
{ 
 obj_t BgL_auxz00_14064;
BgL_auxz00_14064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(50639L), BGl_string4103z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3835z00_6730); 
FAILURE(BgL_auxz00_14064,BFALSE,BFALSE);} } 
BgL_tmpz00_14060 = BgL_res2660z00_5111; } 
BgL_test5340z00_14059 = 
(BgL_tmpz00_14060==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5340z00_14059)
{ /* Llib/hash.scm 1273 */
 obj_t BgL_auxz00_14071; int BgL_tmpz00_14069;
BgL_auxz00_14071 = 
BINT(0L); 
BgL_tmpz00_14069 = 
(int)(6L); 
STRUCT_SET(BgL_tz00_173, BgL_tmpz00_14069, BgL_auxz00_14071); }  else 
{ /* Llib/hash.scm 1273 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_173); } } 
{ /* Llib/hash.scm 1287 */
 bool_t BgL_test5342z00_14075;
{ /* Llib/hash.scm 1287 */
 obj_t BgL_tmpz00_14076;
{ /* Llib/hash.scm 1287 */
 obj_t BgL_res2661z00_5115;
{ /* Llib/hash.scm 1287 */
 obj_t BgL_aux3837z00_6732;
BgL_aux3837z00_6732 = 
STRUCT_KEY(BgL_tz00_173); 
if(
SYMBOLP(BgL_aux3837z00_6732))
{ /* Llib/hash.scm 1287 */
BgL_res2661z00_5115 = BgL_aux3837z00_6732; }  else 
{ 
 obj_t BgL_auxz00_14080;
BgL_auxz00_14080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51263L), BGl_string4103z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3837z00_6732); 
FAILURE(BgL_auxz00_14080,BFALSE,BFALSE);} } 
BgL_tmpz00_14076 = BgL_res2661z00_5115; } 
BgL_test5342z00_14075 = 
(BgL_tmpz00_14076==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5342z00_14075)
{ /* Llib/hash.scm 1287 */
 obj_t BgL_auxz00_14087; int BgL_tmpz00_14085;
BgL_auxz00_14087 = 
BINT(0L); 
BgL_tmpz00_14085 = 
(int)(0L); 
STRUCT_SET(BgL_tz00_173, BgL_tmpz00_14085, BgL_auxz00_14087); }  else 
{ /* Llib/hash.scm 1287 */
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_173); } } 
{ 
 long BgL_iz00_2579;
BgL_iz00_2579 = 0L; 
BgL_zc3z04anonymousza32035ze3z87_2580:
if(
(BgL_iz00_2579==BgL_osiza7e3za7_2574))
{ /* Llib/hash.scm 1289 */
return ((bool_t)0);}  else 
{ /* Llib/hash.scm 1290 */
 obj_t BgL_cz00_2582;
{ /* Llib/hash.scm 1290 */
 obj_t BgL_vectorz00_5118;
if(
VECTORP(BgL_obucketsz00_2575))
{ /* Llib/hash.scm 1290 */
BgL_vectorz00_5118 = BgL_obucketsz00_2575; }  else 
{ 
 obj_t BgL_auxz00_14095;
BgL_auxz00_14095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51365L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_obucketsz00_2575); 
FAILURE(BgL_auxz00_14095,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1290 */
 bool_t BgL_test5346z00_14099;
{ /* Llib/hash.scm 1290 */
 long BgL_tmpz00_14100;
BgL_tmpz00_14100 = 
VECTOR_LENGTH(BgL_vectorz00_5118); 
BgL_test5346z00_14099 = 
BOUND_CHECK(BgL_iz00_2579, BgL_tmpz00_14100); } 
if(BgL_test5346z00_14099)
{ /* Llib/hash.scm 1290 */
BgL_cz00_2582 = 
VECTOR_REF(BgL_vectorz00_5118,BgL_iz00_2579); }  else 
{ 
 obj_t BgL_auxz00_14104;
BgL_auxz00_14104 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51353L), BGl_string3881z00zz__hashz00, BgL_vectorz00_5118, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_5118)), 
(int)(BgL_iz00_2579)); 
FAILURE(BgL_auxz00_14104,BFALSE,BFALSE);} } } 
if(
CBOOL(BgL_cz00_2582))
{ /* Llib/hash.scm 1292 */
 obj_t BgL_hz00_2583;
{ /* Llib/hash.scm 1292 */
 long BgL_arg2039z00_2586;
BgL_arg2039z00_2586 = 
(BgL_iz00_2579+2L); 
{ /* Llib/hash.scm 1292 */
 obj_t BgL_vectorz00_5121;
if(
VECTORP(BgL_obucketsz00_2575))
{ /* Llib/hash.scm 1292 */
BgL_vectorz00_5121 = BgL_obucketsz00_2575; }  else 
{ 
 obj_t BgL_auxz00_14116;
BgL_auxz00_14116 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51420L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_obucketsz00_2575); 
FAILURE(BgL_auxz00_14116,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1292 */
 bool_t BgL_test5349z00_14120;
{ /* Llib/hash.scm 1292 */
 long BgL_tmpz00_14121;
BgL_tmpz00_14121 = 
VECTOR_LENGTH(BgL_vectorz00_5121); 
BgL_test5349z00_14120 = 
BOUND_CHECK(BgL_arg2039z00_2586, BgL_tmpz00_14121); } 
if(BgL_test5349z00_14120)
{ /* Llib/hash.scm 1292 */
BgL_hz00_2583 = 
VECTOR_REF(BgL_vectorz00_5121,BgL_arg2039z00_2586); }  else 
{ 
 obj_t BgL_auxz00_14125;
BgL_auxz00_14125 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51408L), BGl_string3881z00zz__hashz00, BgL_vectorz00_5121, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_5121)), 
(int)(BgL_arg2039z00_2586)); 
FAILURE(BgL_auxz00_14125,BFALSE,BFALSE);} } } } 
if(
CBOOL(BgL_hz00_2583))
{ /* Llib/hash.scm 1295 */
 obj_t BgL_arg2037z00_2584;
{ /* Llib/hash.scm 1295 */
 long BgL_arg2038z00_2585;
BgL_arg2038z00_2585 = 
(BgL_iz00_2579+1L); 
{ /* Llib/hash.scm 1295 */
 obj_t BgL_vectorz00_5124;
if(
VECTORP(BgL_obucketsz00_2575))
{ /* Llib/hash.scm 1295 */
BgL_vectorz00_5124 = BgL_obucketsz00_2575; }  else 
{ 
 obj_t BgL_auxz00_14137;
BgL_auxz00_14137 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51515L), BGl_string3931z00zz__hashz00, BGl_string3932z00zz__hashz00, BgL_obucketsz00_2575); 
FAILURE(BgL_auxz00_14137,BFALSE,BFALSE);} 
{ /* Llib/hash.scm 1295 */
 bool_t BgL_test5352z00_14141;
{ /* Llib/hash.scm 1295 */
 long BgL_tmpz00_14142;
BgL_tmpz00_14142 = 
VECTOR_LENGTH(BgL_vectorz00_5124); 
BgL_test5352z00_14141 = 
BOUND_CHECK(BgL_arg2038z00_2585, BgL_tmpz00_14142); } 
if(BgL_test5352z00_14141)
{ /* Llib/hash.scm 1295 */
BgL_arg2037z00_2584 = 
VECTOR_REF(BgL_vectorz00_5124,BgL_arg2038z00_2585); }  else 
{ 
 obj_t BgL_auxz00_14146;
BgL_auxz00_14146 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(51503L), BGl_string3881z00zz__hashz00, BgL_vectorz00_5124, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_5124)), 
(int)(BgL_arg2038z00_2585)); 
FAILURE(BgL_auxz00_14146,BFALSE,BFALSE);} } } } 
BGl_openzd2stringzd2hashtablezd2putzf2hashz12z32zz__hashz00(BgL_tz00_173, BgL_cz00_2582, BgL_arg2037z00_2584, BgL_hz00_2583); }  else 
{ /* Llib/hash.scm 1293 */BFALSE; } }  else 
{ /* Llib/hash.scm 1291 */BFALSE; } 
{ 
 long BgL_iz00_14154;
BgL_iz00_14154 = 
(BgL_iz00_2579+3L); 
BgL_iz00_2579 = BgL_iz00_14154; 
goto BgL_zc3z04anonymousza32035ze3z87_2580;} } } } } } } } } } 

}



/* open-string-hashtable-size-inc! */
obj_t BGl_openzd2stringzd2hashtablezd2siza7ezd2incz12zb5zz__hashz00(obj_t BgL_tz00_175)
{
{ /* Llib/hash.scm 1308 */
{ /* Llib/hash.scm 1309 */
 obj_t BgL_nz00_2593;
{ /* Llib/hash.scm 1309 */
 bool_t BgL_test5353z00_14156;
{ /* Llib/hash.scm 1309 */
 obj_t BgL_tmpz00_14157;
{ /* Llib/hash.scm 1309 */
 obj_t BgL_res2664z00_5141;
{ /* Llib/hash.scm 1309 */
 obj_t BgL_aux3845z00_6740;
BgL_aux3845z00_6740 = 
STRUCT_KEY(BgL_tz00_175); 
if(
SYMBOLP(BgL_aux3845z00_6740))
{ /* Llib/hash.scm 1309 */
BgL_res2664z00_5141 = BgL_aux3845z00_6740; }  else 
{ 
 obj_t BgL_auxz00_14161;
BgL_auxz00_14161 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(52198L), BGl_string4104z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3845z00_6740); 
FAILURE(BgL_auxz00_14161,BFALSE,BFALSE);} } 
BgL_tmpz00_14157 = BgL_res2664z00_5141; } 
BgL_test5353z00_14156 = 
(BgL_tmpz00_14157==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5353z00_14156)
{ /* Llib/hash.scm 1309 */
 int BgL_tmpz00_14166;
BgL_tmpz00_14166 = 
(int)(0L); 
BgL_nz00_2593 = 
STRUCT_REF(BgL_tz00_175, BgL_tmpz00_14166); }  else 
{ /* Llib/hash.scm 1309 */
BgL_nz00_2593 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_175); } } 
{ /* Llib/hash.scm 1310 */
 bool_t BgL_test5355z00_14170;
{ /* Llib/hash.scm 1310 */
 long BgL_arg2051z00_2599; long BgL_arg2052z00_2600;
{ /* Llib/hash.scm 1310 */
 long BgL_za71za7_5142;
{ /* Llib/hash.scm 1310 */
 obj_t BgL_tmpz00_14171;
if(
INTEGERP(BgL_nz00_2593))
{ /* Llib/hash.scm 1310 */
BgL_tmpz00_14171 = BgL_nz00_2593
; }  else 
{ 
 obj_t BgL_auxz00_14174;
BgL_auxz00_14174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(52240L), BGl_string4104z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_nz00_2593); 
FAILURE(BgL_auxz00_14174,BFALSE,BFALSE);} 
BgL_za71za7_5142 = 
(long)CINT(BgL_tmpz00_14171); } 
BgL_arg2051z00_2599 = 
(BgL_za71za7_5142*3L); } 
{ /* Llib/hash.scm 1310 */
 obj_t BgL_arg2055z00_2601;
{ /* Llib/hash.scm 1310 */
 bool_t BgL_test5357z00_14180;
{ /* Llib/hash.scm 1310 */
 obj_t BgL_tmpz00_14181;
{ /* Llib/hash.scm 1310 */
 obj_t BgL_res2665z00_5146;
{ /* Llib/hash.scm 1310 */
 obj_t BgL_aux3848z00_6743;
BgL_aux3848z00_6743 = 
STRUCT_KEY(BgL_tz00_175); 
if(
SYMBOLP(BgL_aux3848z00_6743))
{ /* Llib/hash.scm 1310 */
BgL_res2665z00_5146 = BgL_aux3848z00_6743; }  else 
{ 
 obj_t BgL_auxz00_14185;
BgL_auxz00_14185 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(52252L), BGl_string4104z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3848z00_6743); 
FAILURE(BgL_auxz00_14185,BFALSE,BFALSE);} } 
BgL_tmpz00_14181 = BgL_res2665z00_5146; } 
BgL_test5357z00_14180 = 
(BgL_tmpz00_14181==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5357z00_14180)
{ /* Llib/hash.scm 1310 */
 int BgL_tmpz00_14190;
BgL_tmpz00_14190 = 
(int)(1L); 
BgL_arg2055z00_2601 = 
STRUCT_REF(BgL_tz00_175, BgL_tmpz00_14190); }  else 
{ /* Llib/hash.scm 1310 */
BgL_arg2055z00_2601 = 
BGl_errorz00zz__errorz00(BGl_string3859z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_175); } } 
{ /* Llib/hash.scm 1310 */
 long BgL_za72za7_5147;
{ /* Llib/hash.scm 1310 */
 obj_t BgL_tmpz00_14194;
if(
INTEGERP(BgL_arg2055z00_2601))
{ /* Llib/hash.scm 1310 */
BgL_tmpz00_14194 = BgL_arg2055z00_2601
; }  else 
{ 
 obj_t BgL_auxz00_14197;
BgL_auxz00_14197 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(52280L), BGl_string4104z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_arg2055z00_2601); 
FAILURE(BgL_auxz00_14197,BFALSE,BFALSE);} 
BgL_za72za7_5147 = 
(long)CINT(BgL_tmpz00_14194); } 
BgL_arg2052z00_2600 = 
(2L*BgL_za72za7_5147); } } 
BgL_test5355z00_14170 = 
(BgL_arg2051z00_2599>BgL_arg2052z00_2600); } 
if(BgL_test5355z00_14170)
{ /* Llib/hash.scm 1310 */
return 
BBOOL(
BGl_openzd2stringzd2hashtablezd2rehashz12zc0zz__hashz00(BgL_tz00_175));}  else 
{ /* Llib/hash.scm 1312 */
 long BgL_arg2050z00_2598;
{ /* Llib/hash.scm 1312 */
 long BgL_za71za7_5150;
{ /* Llib/hash.scm 1312 */
 obj_t BgL_tmpz00_14206;
if(
INTEGERP(BgL_nz00_2593))
{ /* Llib/hash.scm 1312 */
BgL_tmpz00_14206 = BgL_nz00_2593
; }  else 
{ 
 obj_t BgL_auxz00_14209;
BgL_auxz00_14209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(52353L), BGl_string4104z00zz__hashz00, BGl_string3868z00zz__hashz00, BgL_nz00_2593); 
FAILURE(BgL_auxz00_14209,BFALSE,BFALSE);} 
BgL_za71za7_5150 = 
(long)CINT(BgL_tmpz00_14206); } 
BgL_arg2050z00_2598 = 
(BgL_za71za7_5150+1L); } 
{ /* Llib/hash.scm 1312 */
 bool_t BgL_test5361z00_14215;
{ /* Llib/hash.scm 1312 */
 obj_t BgL_tmpz00_14216;
{ /* Llib/hash.scm 1312 */
 obj_t BgL_res2666z00_5154;
{ /* Llib/hash.scm 1312 */
 obj_t BgL_aux3852z00_6747;
BgL_aux3852z00_6747 = 
STRUCT_KEY(BgL_tz00_175); 
if(
SYMBOLP(BgL_aux3852z00_6747))
{ /* Llib/hash.scm 1312 */
BgL_res2666z00_5154 = BgL_aux3852z00_6747; }  else 
{ 
 obj_t BgL_auxz00_14220;
BgL_auxz00_14220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3876z00zz__hashz00, 
BINT(52324L), BGl_string4104z00zz__hashz00, BGl_string3856z00zz__hashz00, BgL_aux3852z00_6747); 
FAILURE(BgL_auxz00_14220,BFALSE,BFALSE);} } 
BgL_tmpz00_14216 = BgL_res2666z00_5154; } 
BgL_test5361z00_14215 = 
(BgL_tmpz00_14216==BGl_symbol3857z00zz__hashz00); } 
if(BgL_test5361z00_14215)
{ /* Llib/hash.scm 1312 */
 obj_t BgL_auxz00_14227; int BgL_tmpz00_14225;
BgL_auxz00_14227 = 
BINT(BgL_arg2050z00_2598); 
BgL_tmpz00_14225 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tz00_175, BgL_tmpz00_14225, BgL_auxz00_14227);}  else 
{ /* Llib/hash.scm 1312 */
return 
BGl_errorz00zz__errorz00(BGl_string3877z00zz__hashz00, BGl_string3858z00zz__hashz00, BgL_tz00_175);} } } } } } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__hashz00(void)
{
{ /* Llib/hash.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string4105z00zz__hashz00)); 
BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L, 
BSTRING_TO_STRING(BGl_string4105z00zz__hashz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string4105z00zz__hashz00)); 
return 
BGl_modulezd2initializa7ationz75zz__weakhashz00(56552828L, 
BSTRING_TO_STRING(BGl_string4105z00zz__hashz00));} 

}

#ifdef __cplusplus
}
#endif
