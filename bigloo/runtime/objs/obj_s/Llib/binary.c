/*===========================================================================*/
/*   (Llib/binary.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/binary.scm -indent -o objs/obj_s/Llib/binary.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BINARY_TYPE_DEFINITIONS
#define BGL___BINARY_TYPE_DEFINITIONS
#endif // BGL___BINARY_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62inputzd2stringzb0zz__binaryz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62closezd2binaryzd2portz62zz__binaryz00(obj_t, obj_t);
static obj_t BGl_z62outputzd2bytezb0zz__binaryz00(obj_t, obj_t, obj_t);
extern obj_t append_output_binary_file(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__binaryz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_inputzd2stringzd2zz__binaryz00(obj_t, int);
static obj_t BGl_z62openzd2inputzd2binaryzd2filezb0zz__binaryz00(obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62outputzd2stringzb0zz__binaryz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62appendzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
extern int bgl_input_fill_string(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_closezd2binaryzd2portz00zz__binaryz00(obj_t);
extern obj_t output_obj(obj_t, obj_t);
extern unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
BGL_EXPORTED_DECL obj_t BGl_outputzd2objzd2zz__binaryz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2charzd2zz__binaryz00(obj_t, unsigned char);
extern obj_t input_obj(obj_t);
static obj_t BGl_genericzd2initzd2zz__binaryz00(void);
extern obj_t bgl_flush_binary_port(obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__binaryz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__binaryz00(void);
extern obj_t open_input_binary_file(obj_t);
BGL_EXPORTED_DECL obj_t BGl_appendzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
static obj_t BGl_objectzd2initzd2zz__binaryz00(void);
BGL_EXPORTED_DECL int BGl_inputzd2fillzd2stringz12z12zz__binaryz00(obj_t, obj_t);
extern obj_t bgl_input_string(obj_t, int);
static obj_t BGl_z62binaryzd2portzf3z43zz__binaryz00(obj_t, obj_t);
static obj_t BGl_z62inputzd2objzb0zz__binaryz00(obj_t, obj_t);
static obj_t BGl_z62outputzd2objzb0zz__binaryz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62flushzd2binaryzd2portz62zz__binaryz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2bytezd2zz__binaryz00(obj_t, char);
extern int bgl_output_string(obj_t, obj_t);
static obj_t BGl_z62inputzd2fillzd2stringz12z70zz__binaryz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t);
static obj_t BGl_z62openzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_binaryzd2portzf3z21zz__binaryz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2charzd2zz__binaryz00(obj_t);
static obj_t BGl_methodzd2initzd2zz__binaryz00(void);
extern obj_t open_output_binary_file(obj_t);
BGL_EXPORTED_DECL obj_t BGl_inputzd2objzd2zz__binaryz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_flushzd2binaryzd2portz00zz__binaryz00(obj_t);
static obj_t BGl_z62inputzd2charzb0zz__binaryz00(obj_t, obj_t);
extern obj_t close_binary_port(obj_t);
static obj_t BGl_z62outputzd2charzb0zz__binaryz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_outputzd2stringzd2zz__binaryz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2stringzd2envz00zz__binaryz00, BgL_bgl_za762inputza7d2strin1673z00, BGl_z62inputzd2stringzb0zz__binaryz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_binaryzd2portzf3zd2envzf3zz__binaryz00, BgL_bgl_za762binaryza7d2port1674z00, BGl_z62binaryzd2portzf3z43zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2stringzd2envz00zz__binaryz00, BgL_bgl_za762outputza7d2stri1675z00, BGl_z62outputzd2stringzb0zz__binaryz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_flushzd2binaryzd2portzd2envzd2zz__binaryz00, BgL_bgl_za762flushza7d2binar1676z00, BGl_z62flushzd2binaryzd2portz62zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2outputzd2binaryzd2filezd2envz00zz__binaryz00, BgL_bgl_za762openza7d2output1677z00, BGl_z62openzd2outputzd2binaryzd2filezb0zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2charzd2envz00zz__binaryz00, BgL_bgl_za762outputza7d2char1678z00, BGl_z62outputzd2charzb0zz__binaryz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2charzd2envz00zz__binaryz00, BgL_bgl_za762inputza7d2charza71679za7, BGl_z62inputzd2charzb0zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2objzd2envz00zz__binaryz00, BgL_bgl_za762outputza7d2objza71680za7, BGl_z62outputzd2objzb0zz__binaryz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_appendzd2outputzd2binaryzd2filezd2envz00zz__binaryz00, BgL_bgl_za762appendza7d2outp1681z00, BGl_z62appendzd2outputzd2binaryzd2filezb0zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2fillzd2stringz12zd2envzc0zz__binaryz00, BgL_bgl_za762inputza7d2fillza71682za7, BGl_z62inputzd2fillzd2stringz12z70zz__binaryz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1654z00zz__binaryz00, BgL_bgl_string1654za700za7za7_1683za7, "/tmp/bigloo/runtime/Llib/binary.scm", 35 );
DEFINE_STRING( BGl_string1655z00zz__binaryz00, BgL_bgl_string1655za700za7za7_1684za7, "&open-output-binary-file", 24 );
DEFINE_STRING( BGl_string1656z00zz__binaryz00, BgL_bgl_string1656za700za7za7_1685za7, "bstring", 7 );
DEFINE_STRING( BGl_string1657z00zz__binaryz00, BgL_bgl_string1657za700za7za7_1686za7, "&append-output-binary-file", 26 );
DEFINE_STRING( BGl_string1658z00zz__binaryz00, BgL_bgl_string1658za700za7za7_1687za7, "&open-input-binary-file", 23 );
DEFINE_STRING( BGl_string1659z00zz__binaryz00, BgL_bgl_string1659za700za7za7_1688za7, "&close-binary-port", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_inputzd2objzd2envz00zz__binaryz00, BgL_bgl_za762inputza7d2objza7b1689za7, BGl_z62inputzd2objzb0zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closezd2binaryzd2portzd2envzd2zz__binaryz00, BgL_bgl_za762closeza7d2binar1690z00, BGl_z62closezd2binaryzd2portz62zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1660z00zz__binaryz00, BgL_bgl_string1660za700za7za7_1691za7, "binary-port", 11 );
DEFINE_STRING( BGl_string1661z00zz__binaryz00, BgL_bgl_string1661za700za7za7_1692za7, "&flush-binary-port", 18 );
DEFINE_STRING( BGl_string1662z00zz__binaryz00, BgL_bgl_string1662za700za7za7_1693za7, "&input-obj", 10 );
DEFINE_STRING( BGl_string1663z00zz__binaryz00, BgL_bgl_string1663za700za7za7_1694za7, "&output-obj", 11 );
DEFINE_STRING( BGl_string1664z00zz__binaryz00, BgL_bgl_string1664za700za7za7_1695za7, "&output-char", 12 );
DEFINE_STRING( BGl_string1665z00zz__binaryz00, BgL_bgl_string1665za700za7za7_1696za7, "bchar", 5 );
DEFINE_STRING( BGl_string1666z00zz__binaryz00, BgL_bgl_string1666za700za7za7_1697za7, "&output-byte", 12 );
DEFINE_STRING( BGl_string1667z00zz__binaryz00, BgL_bgl_string1667za700za7za7_1698za7, "bint", 4 );
DEFINE_STRING( BGl_string1668z00zz__binaryz00, BgL_bgl_string1668za700za7za7_1699za7, "&input-char", 11 );
DEFINE_STRING( BGl_string1669z00zz__binaryz00, BgL_bgl_string1669za700za7za7_1700za7, "&output-string", 14 );
DEFINE_STRING( BGl_string1670z00zz__binaryz00, BgL_bgl_string1670za700za7za7_1701za7, "&input-string", 13 );
DEFINE_STRING( BGl_string1671z00zz__binaryz00, BgL_bgl_string1671za700za7za7_1702za7, "&input-fill-string!", 19 );
DEFINE_STRING( BGl_string1672z00zz__binaryz00, BgL_bgl_string1672za700za7za7_1703za7, "__binary", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2inputzd2binaryzd2filezd2envz00zz__binaryz00, BgL_bgl_za762openza7d2inputza71704za7, BGl_z62openzd2inputzd2binaryzd2filezb0zz__binaryz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_outputzd2bytezd2envz00zz__binaryz00, BgL_bgl_za762outputza7d2byte1705z00, BGl_z62outputzd2bytezb0zz__binaryz00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__binaryz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__binaryz00(long BgL_checksumz00_2044, char * BgL_fromz00_2045)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__binaryz00))
{ 
BGl_requirezd2initializa7ationz75zz__binaryz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__binaryz00(); 
BGl_importedzd2moduleszd2initz00zz__binaryz00(); 
return 
BGl_methodzd2initzd2zz__binaryz00();}  else 
{ 
return BUNSPEC;} } 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__binaryz00(void)
{
{ /* Llib/binary.scm 15 */
return 
bgl_gc_roots_register();} 

}



/* binary-port? */
BGL_EXPORTED_DEF bool_t BGl_binaryzd2portzf3z21zz__binaryz00(obj_t BgL_objz00_3)
{
{ /* Llib/binary.scm 138 */
return 
BINARY_PORTP(BgL_objz00_3);} 

}



/* &binary-port? */
obj_t BGl_z62binaryzd2portzf3z43zz__binaryz00(obj_t BgL_envz00_1975, obj_t BgL_objz00_1976)
{
{ /* Llib/binary.scm 138 */
return 
BBOOL(
BGl_binaryzd2portzf3z21zz__binaryz00(BgL_objz00_1976));} 

}



/* open-output-binary-file */
BGL_EXPORTED_DEF obj_t BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t BgL_strz00_4)
{
{ /* Llib/binary.scm 144 */
BGL_TAIL return 
open_output_binary_file(BgL_strz00_4);} 

}



/* &open-output-binary-file */
obj_t BGl_z62openzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t BgL_envz00_1977, obj_t BgL_strz00_1978)
{
{ /* Llib/binary.scm 144 */
{ /* Llib/binary.scm 145 */
 obj_t BgL_auxz00_2057;
if(
STRINGP(BgL_strz00_1978))
{ /* Llib/binary.scm 145 */
BgL_auxz00_2057 = BgL_strz00_1978
; }  else 
{ 
 obj_t BgL_auxz00_2060;
BgL_auxz00_2060 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(5623L), BGl_string1655z00zz__binaryz00, BGl_string1656z00zz__binaryz00, BgL_strz00_1978); 
FAILURE(BgL_auxz00_2060,BFALSE,BFALSE);} 
return 
BGl_openzd2outputzd2binaryzd2filezd2zz__binaryz00(BgL_auxz00_2057);} } 

}



/* append-output-binary-file */
BGL_EXPORTED_DEF obj_t BGl_appendzd2outputzd2binaryzd2filezd2zz__binaryz00(obj_t BgL_strz00_5)
{
{ /* Llib/binary.scm 150 */
BGL_TAIL return 
append_output_binary_file(BgL_strz00_5);} 

}



/* &append-output-binary-file */
obj_t BGl_z62appendzd2outputzd2binaryzd2filezb0zz__binaryz00(obj_t BgL_envz00_1979, obj_t BgL_strz00_1980)
{
{ /* Llib/binary.scm 150 */
{ /* Llib/binary.scm 151 */
 obj_t BgL_auxz00_2066;
if(
STRINGP(BgL_strz00_1980))
{ /* Llib/binary.scm 151 */
BgL_auxz00_2066 = BgL_strz00_1980
; }  else 
{ 
 obj_t BgL_auxz00_2069;
BgL_auxz00_2069 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(5921L), BGl_string1657z00zz__binaryz00, BGl_string1656z00zz__binaryz00, BgL_strz00_1980); 
FAILURE(BgL_auxz00_2069,BFALSE,BFALSE);} 
return 
BGl_appendzd2outputzd2binaryzd2filezd2zz__binaryz00(BgL_auxz00_2066);} } 

}



/* open-input-binary-file */
BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(obj_t BgL_strz00_6)
{
{ /* Llib/binary.scm 156 */
BGL_TAIL return 
open_input_binary_file(BgL_strz00_6);} 

}



/* &open-input-binary-file */
obj_t BGl_z62openzd2inputzd2binaryzd2filezb0zz__binaryz00(obj_t BgL_envz00_1981, obj_t BgL_strz00_1982)
{
{ /* Llib/binary.scm 156 */
{ /* Llib/binary.scm 157 */
 obj_t BgL_auxz00_2075;
if(
STRINGP(BgL_strz00_1982))
{ /* Llib/binary.scm 157 */
BgL_auxz00_2075 = BgL_strz00_1982
; }  else 
{ 
 obj_t BgL_auxz00_2078;
BgL_auxz00_2078 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(6218L), BGl_string1658z00zz__binaryz00, BGl_string1656z00zz__binaryz00, BgL_strz00_1982); 
FAILURE(BgL_auxz00_2078,BFALSE,BFALSE);} 
return 
BGl_openzd2inputzd2binaryzd2filezd2zz__binaryz00(BgL_auxz00_2075);} } 

}



/* close-binary-port */
BGL_EXPORTED_DEF obj_t BGl_closezd2binaryzd2portz00zz__binaryz00(obj_t BgL_portz00_7)
{
{ /* Llib/binary.scm 162 */
BGL_TAIL return 
close_binary_port(BgL_portz00_7);} 

}



/* &close-binary-port */
obj_t BGl_z62closezd2binaryzd2portz62zz__binaryz00(obj_t BgL_envz00_1983, obj_t BgL_portz00_1984)
{
{ /* Llib/binary.scm 162 */
{ /* Llib/binary.scm 163 */
 obj_t BgL_auxz00_2084;
if(
BINARY_PORTP(BgL_portz00_1984))
{ /* Llib/binary.scm 163 */
BgL_auxz00_2084 = BgL_portz00_1984
; }  else 
{ 
 obj_t BgL_auxz00_2087;
BgL_auxz00_2087 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(6515L), BGl_string1659z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1984); 
FAILURE(BgL_auxz00_2087,BFALSE,BFALSE);} 
return 
BGl_closezd2binaryzd2portz00zz__binaryz00(BgL_auxz00_2084);} } 

}



/* flush-binary-port */
BGL_EXPORTED_DEF obj_t BGl_flushzd2binaryzd2portz00zz__binaryz00(obj_t BgL_portz00_8)
{
{ /* Llib/binary.scm 168 */
BGL_TAIL return 
bgl_flush_binary_port(BgL_portz00_8);} 

}



/* &flush-binary-port */
obj_t BGl_z62flushzd2binaryzd2portz62zz__binaryz00(obj_t BgL_envz00_1985, obj_t BgL_portz00_1986)
{
{ /* Llib/binary.scm 168 */
{ /* Llib/binary.scm 169 */
 obj_t BgL_auxz00_2093;
if(
BINARY_PORTP(BgL_portz00_1986))
{ /* Llib/binary.scm 169 */
BgL_auxz00_2093 = BgL_portz00_1986
; }  else 
{ 
 obj_t BgL_auxz00_2096;
BgL_auxz00_2096 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(6808L), BGl_string1661z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1986); 
FAILURE(BgL_auxz00_2096,BFALSE,BFALSE);} 
return 
BGl_flushzd2binaryzd2portz00zz__binaryz00(BgL_auxz00_2093);} } 

}



/* input-obj */
BGL_EXPORTED_DEF obj_t BGl_inputzd2objzd2zz__binaryz00(obj_t BgL_portz00_9)
{
{ /* Llib/binary.scm 174 */
BGL_TAIL return 
input_obj(BgL_portz00_9);} 

}



/* &input-obj */
obj_t BGl_z62inputzd2objzb0zz__binaryz00(obj_t BgL_envz00_1987, obj_t BgL_portz00_1988)
{
{ /* Llib/binary.scm 174 */
{ /* Llib/binary.scm 175 */
 obj_t BgL_auxz00_2102;
if(
BINARY_PORTP(BgL_portz00_1988))
{ /* Llib/binary.scm 175 */
BgL_auxz00_2102 = BgL_portz00_1988
; }  else 
{ 
 obj_t BgL_auxz00_2105;
BgL_auxz00_2105 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(7093L), BGl_string1662z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1988); 
FAILURE(BgL_auxz00_2105,BFALSE,BFALSE);} 
return 
BGl_inputzd2objzd2zz__binaryz00(BgL_auxz00_2102);} } 

}



/* output-obj */
BGL_EXPORTED_DEF obj_t BGl_outputzd2objzd2zz__binaryz00(obj_t BgL_portz00_10, obj_t BgL_objz00_11)
{
{ /* Llib/binary.scm 180 */
BGL_TAIL return 
output_obj(BgL_portz00_10, BgL_objz00_11);} 

}



/* &output-obj */
obj_t BGl_z62outputzd2objzb0zz__binaryz00(obj_t BgL_envz00_1989, obj_t BgL_portz00_1990, obj_t BgL_objz00_1991)
{
{ /* Llib/binary.scm 180 */
{ /* Llib/binary.scm 181 */
 obj_t BgL_auxz00_2111;
if(
BINARY_PORTP(BgL_portz00_1990))
{ /* Llib/binary.scm 181 */
BgL_auxz00_2111 = BgL_portz00_1990
; }  else 
{ 
 obj_t BgL_auxz00_2114;
BgL_auxz00_2114 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(7375L), BGl_string1663z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1990); 
FAILURE(BgL_auxz00_2114,BFALSE,BFALSE);} 
return 
BGl_outputzd2objzd2zz__binaryz00(BgL_auxz00_2111, BgL_objz00_1991);} } 

}



/* output-char */
BGL_EXPORTED_DEF obj_t BGl_outputzd2charzd2zz__binaryz00(obj_t BgL_portz00_12, unsigned char BgL_charz00_13)
{
{ /* Llib/binary.scm 186 */
return 
BGL_OUTPUT_CHAR(BgL_portz00_12, BgL_charz00_13);} 

}



/* &output-char */
obj_t BGl_z62outputzd2charzb0zz__binaryz00(obj_t BgL_envz00_1992, obj_t BgL_portz00_1993, obj_t BgL_charz00_1994)
{
{ /* Llib/binary.scm 186 */
{ /* Llib/binary.scm 187 */
 unsigned char BgL_auxz00_2127; obj_t BgL_auxz00_2120;
{ /* Llib/binary.scm 187 */
 obj_t BgL_tmpz00_2128;
if(
CHARP(BgL_charz00_1994))
{ /* Llib/binary.scm 187 */
BgL_tmpz00_2128 = BgL_charz00_1994
; }  else 
{ 
 obj_t BgL_auxz00_2131;
BgL_auxz00_2131 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(7669L), BGl_string1664z00zz__binaryz00, BGl_string1665z00zz__binaryz00, BgL_charz00_1994); 
FAILURE(BgL_auxz00_2131,BFALSE,BFALSE);} 
BgL_auxz00_2127 = 
CCHAR(BgL_tmpz00_2128); } 
if(
BINARY_PORTP(BgL_portz00_1993))
{ /* Llib/binary.scm 187 */
BgL_auxz00_2120 = BgL_portz00_1993
; }  else 
{ 
 obj_t BgL_auxz00_2123;
BgL_auxz00_2123 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(7669L), BGl_string1664z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1993); 
FAILURE(BgL_auxz00_2123,BFALSE,BFALSE);} 
return 
BGl_outputzd2charzd2zz__binaryz00(BgL_auxz00_2120, BgL_auxz00_2127);} } 

}



/* output-byte */
BGL_EXPORTED_DEF obj_t BGl_outputzd2bytezd2zz__binaryz00(obj_t BgL_portz00_14, char BgL_charz00_15)
{
{ /* Llib/binary.scm 192 */
return 
BGL_OUTPUT_CHAR(BgL_portz00_14, BgL_charz00_15);} 

}



/* &output-byte */
obj_t BGl_z62outputzd2bytezb0zz__binaryz00(obj_t BgL_envz00_1995, obj_t BgL_portz00_1996, obj_t BgL_charz00_1997)
{
{ /* Llib/binary.scm 192 */
{ /* Llib/binary.scm 193 */
 char BgL_auxz00_2145; obj_t BgL_auxz00_2138;
{ /* Llib/binary.scm 193 */
 obj_t BgL_tmpz00_2146;
if(
INTEGERP(BgL_charz00_1997))
{ /* Llib/binary.scm 193 */
BgL_tmpz00_2146 = BgL_charz00_1997
; }  else 
{ 
 obj_t BgL_auxz00_2149;
BgL_auxz00_2149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(7960L), BGl_string1666z00zz__binaryz00, BGl_string1667z00zz__binaryz00, BgL_charz00_1997); 
FAILURE(BgL_auxz00_2149,BFALSE,BFALSE);} 
BgL_auxz00_2145 = 
(signed char)CINT(BgL_tmpz00_2146); } 
if(
BINARY_PORTP(BgL_portz00_1996))
{ /* Llib/binary.scm 193 */
BgL_auxz00_2138 = BgL_portz00_1996
; }  else 
{ 
 obj_t BgL_auxz00_2141;
BgL_auxz00_2141 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(7960L), BGl_string1666z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1996); 
FAILURE(BgL_auxz00_2141,BFALSE,BFALSE);} 
return 
BGl_outputzd2bytezd2zz__binaryz00(BgL_auxz00_2138, BgL_auxz00_2145);} } 

}



/* input-char */
BGL_EXPORTED_DEF obj_t BGl_inputzd2charzd2zz__binaryz00(obj_t BgL_portz00_16)
{
{ /* Llib/binary.scm 198 */
{ /* Llib/binary.scm 199 */
 int BgL_charz00_2043;
BgL_charz00_2043 = 
BGL_INPUT_CHAR(BgL_portz00_16); 
if(
BGL_INT_EOFP(BgL_charz00_2043))
{ /* Llib/binary.scm 200 */
return BEOF;}  else 
{ /* Llib/binary.scm 200 */
return 
BCHAR(
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(long)(BgL_charz00_2043)));} } } 

}



/* &input-char */
obj_t BGl_z62inputzd2charzb0zz__binaryz00(obj_t BgL_envz00_1998, obj_t BgL_portz00_1999)
{
{ /* Llib/binary.scm 198 */
{ /* Llib/binary.scm 199 */
 obj_t BgL_auxz00_2161;
if(
BINARY_PORTP(BgL_portz00_1999))
{ /* Llib/binary.scm 199 */
BgL_auxz00_2161 = BgL_portz00_1999
; }  else 
{ 
 obj_t BgL_auxz00_2164;
BgL_auxz00_2164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(8245L), BGl_string1668z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_1999); 
FAILURE(BgL_auxz00_2164,BFALSE,BFALSE);} 
return 
BGl_inputzd2charzd2zz__binaryz00(BgL_auxz00_2161);} } 

}



/* output-string */
BGL_EXPORTED_DEF obj_t BGl_outputzd2stringzd2zz__binaryz00(obj_t BgL_portz00_17, obj_t BgL_stringz00_18)
{
{ /* Llib/binary.scm 207 */
return 
BINT(
bgl_output_string(BgL_portz00_17, BgL_stringz00_18));} 

}



/* &output-string */
obj_t BGl_z62outputzd2stringzb0zz__binaryz00(obj_t BgL_envz00_2000, obj_t BgL_portz00_2001, obj_t BgL_stringz00_2002)
{
{ /* Llib/binary.scm 207 */
{ /* Llib/binary.scm 208 */
 obj_t BgL_auxz00_2178; obj_t BgL_auxz00_2171;
if(
STRINGP(BgL_stringz00_2002))
{ /* Llib/binary.scm 208 */
BgL_auxz00_2178 = BgL_stringz00_2002
; }  else 
{ 
 obj_t BgL_auxz00_2181;
BgL_auxz00_2181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(8609L), BGl_string1669z00zz__binaryz00, BGl_string1656z00zz__binaryz00, BgL_stringz00_2002); 
FAILURE(BgL_auxz00_2181,BFALSE,BFALSE);} 
if(
BINARY_PORTP(BgL_portz00_2001))
{ /* Llib/binary.scm 208 */
BgL_auxz00_2171 = BgL_portz00_2001
; }  else 
{ 
 obj_t BgL_auxz00_2174;
BgL_auxz00_2174 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(8609L), BGl_string1669z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_2001); 
FAILURE(BgL_auxz00_2174,BFALSE,BFALSE);} 
return 
BGl_outputzd2stringzd2zz__binaryz00(BgL_auxz00_2171, BgL_auxz00_2178);} } 

}



/* input-string */
BGL_EXPORTED_DEF obj_t BGl_inputzd2stringzd2zz__binaryz00(obj_t BgL_portz00_19, int BgL_lenz00_20)
{
{ /* Llib/binary.scm 213 */
BGL_TAIL return 
bgl_input_string(BgL_portz00_19, BgL_lenz00_20);} 

}



/* &input-string */
obj_t BGl_z62inputzd2stringzb0zz__binaryz00(obj_t BgL_envz00_2003, obj_t BgL_portz00_2004, obj_t BgL_lenz00_2005)
{
{ /* Llib/binary.scm 213 */
{ /* Llib/binary.scm 214 */
 int BgL_auxz00_2194; obj_t BgL_auxz00_2187;
{ /* Llib/binary.scm 214 */
 obj_t BgL_tmpz00_2195;
if(
INTEGERP(BgL_lenz00_2005))
{ /* Llib/binary.scm 214 */
BgL_tmpz00_2195 = BgL_lenz00_2005
; }  else 
{ 
 obj_t BgL_auxz00_2198;
BgL_auxz00_2198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(8904L), BGl_string1670z00zz__binaryz00, BGl_string1667z00zz__binaryz00, BgL_lenz00_2005); 
FAILURE(BgL_auxz00_2198,BFALSE,BFALSE);} 
BgL_auxz00_2194 = 
CINT(BgL_tmpz00_2195); } 
if(
BINARY_PORTP(BgL_portz00_2004))
{ /* Llib/binary.scm 214 */
BgL_auxz00_2187 = BgL_portz00_2004
; }  else 
{ 
 obj_t BgL_auxz00_2190;
BgL_auxz00_2190 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(8904L), BGl_string1670z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_2004); 
FAILURE(BgL_auxz00_2190,BFALSE,BFALSE);} 
return 
BGl_inputzd2stringzd2zz__binaryz00(BgL_auxz00_2187, BgL_auxz00_2194);} } 

}



/* input-fill-string! */
BGL_EXPORTED_DEF int BGl_inputzd2fillzd2stringz12z12zz__binaryz00(obj_t BgL_portz00_21, obj_t BgL_strz00_22)
{
{ /* Llib/binary.scm 219 */
BGL_TAIL return 
bgl_input_fill_string(BgL_portz00_21, BgL_strz00_22);} 

}



/* &input-fill-string! */
obj_t BGl_z62inputzd2fillzd2stringz12z70zz__binaryz00(obj_t BgL_envz00_2006, obj_t BgL_portz00_2007, obj_t BgL_strz00_2008)
{
{ /* Llib/binary.scm 219 */
{ /* Llib/binary.scm 220 */
 int BgL_tmpz00_2205;
{ /* Llib/binary.scm 220 */
 obj_t BgL_auxz00_2213; obj_t BgL_auxz00_2206;
if(
STRINGP(BgL_strz00_2008))
{ /* Llib/binary.scm 220 */
BgL_auxz00_2213 = BgL_strz00_2008
; }  else 
{ 
 obj_t BgL_auxz00_2216;
BgL_auxz00_2216 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(9201L), BGl_string1671z00zz__binaryz00, BGl_string1656z00zz__binaryz00, BgL_strz00_2008); 
FAILURE(BgL_auxz00_2216,BFALSE,BFALSE);} 
if(
BINARY_PORTP(BgL_portz00_2007))
{ /* Llib/binary.scm 220 */
BgL_auxz00_2206 = BgL_portz00_2007
; }  else 
{ 
 obj_t BgL_auxz00_2209;
BgL_auxz00_2209 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1654z00zz__binaryz00, 
BINT(9201L), BGl_string1671z00zz__binaryz00, BGl_string1660z00zz__binaryz00, BgL_portz00_2007); 
FAILURE(BgL_auxz00_2209,BFALSE,BFALSE);} 
BgL_tmpz00_2205 = 
BGl_inputzd2fillzd2stringz12z12zz__binaryz00(BgL_auxz00_2206, BgL_auxz00_2213); } 
return 
BINT(BgL_tmpz00_2205);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__binaryz00(void)
{
{ /* Llib/binary.scm 15 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__binaryz00(void)
{
{ /* Llib/binary.scm 15 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__binaryz00(void)
{
{ /* Llib/binary.scm 15 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__binaryz00(void)
{
{ /* Llib/binary.scm 15 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1672z00zz__binaryz00)); 
BGl_modulezd2initializa7ationz75zz__intextz00(6305717L, 
BSTRING_TO_STRING(BGl_string1672z00zz__binaryz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1672z00zz__binaryz00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string1672z00zz__binaryz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1672z00zz__binaryz00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1672z00zz__binaryz00));} 

}

#ifdef __cplusplus
}
#endif
