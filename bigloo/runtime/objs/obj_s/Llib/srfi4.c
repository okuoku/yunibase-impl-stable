/*===========================================================================*/
/*   (Llib/srfi4.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/srfi4.scm -indent -o objs/obj_s/Llib/srfi4.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SRFI4_TYPE_DEFINITIONS
#define BGL___SRFI4_TYPE_DEFINITIONS
#endif // BGL___SRFI4_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_u8vectorzd2setz12zc0zz__srfi4z00(obj_t, long, uint8_t);
static obj_t BGl_z62s64vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl__s8vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_u8vectorzd2ze3listz31zz__srfi4z00(obj_t);
static obj_t BGl_z62f64vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62f64vectorz62zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_u8vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3f64vectorz31zz__srfi4z00(obj_t);
static obj_t BGl_z62listzd2ze3s8vectorz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62s8vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__f64vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2f64vectorzd2zz__srfi4z00(long, double);
static obj_t BGl_z62listzd2ze3f64vectorz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_u16vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u16vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint16_t BGl_u16vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_z62u16vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_u16vectorzd2lengthzd2zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_s8vectorz00zz__srfi4z00(obj_t);
static obj_t BGl_z62s32vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__f32vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_homogeneouszd2vectorzf3z21zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u16vectorzd2setz12zc0zz__srfi4z00(obj_t, long, uint16_t);
static obj_t BGl_z62s16vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_u8vectorzd2lengthzd2zz__srfi4z00(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62u64vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62u32vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62s16vectorz62zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s16vectorz31zz__srfi4z00(obj_t);
static obj_t BGl_z62u16vectorz62zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62u8vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__makezd2s32vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u16vectorz31zz__srfi4z00(obj_t);
static obj_t BGl__makezd2u32vectorzd2zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62f64vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2s16vectorzd2zz__srfi4z00(long, int16_t);
static obj_t BGl_z62listzd2ze3s16vectorz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2u16vectorzd2zz__srfi4z00(long, uint16_t);
static obj_t BGl_z62listzd2ze3u16vectorz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62s8vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62u8vectorzf3z91zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2s8vectorzd2zz__srfi4z00(long, int8_t);
BGL_EXPORTED_DECL obj_t BGl_hvectorzd2rangezd2errorz00zz__srfi4z00(obj_t, obj_t, long);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__u64vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__makezd2s64vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl__makezd2u64vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_s16vectorzd2lengthzd2zz__srfi4z00(obj_t);
static obj_t BGl_z62u32vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_f32vectorz00zz__srfi4z00(obj_t);
static obj_t BGl_symbol2910z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_s32vectorzd2setz12zc0zz__srfi4z00(obj_t, long, int32_t);
static obj_t BGl_z62u64vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol2917z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_s64vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_z62f64vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
static obj_t BGl__u32vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL uint64_t BGl_u64vectorzd2refzd2zz__srfi4z00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_f64vectorzd2setz12zc0zz__srfi4z00(obj_t, long, double);
static obj_t BGl_z62f32vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62u8vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3017z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_f64vectorz00zz__srfi4z00(obj_t);
static obj_t BGl_symbol2922z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u8vectorz31zz__srfi4z00(obj_t);
static obj_t BGl_symbol2927z00zz__srfi4z00 = BUNSPEC;
extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
BGL_EXPORTED_DECL obj_t BGl_s16vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_s16vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
static obj_t BGl_z62s16vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62u16vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3025z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_symbol3028z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62s64vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol2932z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62u8vectorz62zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol2937z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL long BGl_s8vectorzd2lengthzd2zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_s16vectorz00zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u16vectorz00zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_u64vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_z62s8vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62s64vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__srfi4z00(void);
static obj_t BGl_symbol3031z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_s8vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_genericzd2initzd2zz__srfi4z00(void);
BGL_EXPORTED_DECL bool_t BGl_u8vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_symbol3034z00zz__srfi4z00 = BUNSPEC;
extern long bgl_list_length(obj_t);
static obj_t BGl_symbol3037z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_s8vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s8vectorz31zz__srfi4z00(obj_t);
static obj_t BGl_symbol2942z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_s8vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__srfi4z00(void);
static obj_t BGl_symbol2947z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_symbol2867z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62s8vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol2869z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__srfi4z00(void);
BGL_EXPORTED_DECL int32_t BGl_s32vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_objectzd2initzd2zz__srfi4z00(void);
static obj_t BGl__s64vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol3040z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_symbol3043z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
static obj_t BGl_symbol3046z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62s8vectorz62zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62u16vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol3049z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_s16vectorzd2setz12zc0zz__srfi4z00(obj_t, long, int16_t);
static obj_t BGl_symbol2952z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_symbol2871z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62homogeneouszd2vectorzd2infoz62zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol2873z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_f64vectorzd2ze3listz31zz__srfi4z00(obj_t);
static obj_t BGl_symbol2875z00zz__srfi4z00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_f64vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_u64vectorzd2setz12zc0zz__srfi4z00(obj_t, long, uint64_t);
static obj_t BGl_symbol2957z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62u32vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_f32vectorzd2lengthzd2zz__srfi4z00(obj_t);
static obj_t BGl_symbol2877z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_symbol2879z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62f64vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62s64vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62s32vectorz62zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s32vectorz31zz__srfi4z00(obj_t);
static obj_t BGl_z62u32vectorz62zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__s32vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u32vectorz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2s32vectorzd2zz__srfi4z00(long, int32_t);
static obj_t BGl_z62u64vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62listzd2ze3s32vectorz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_s8vectorzd2setz12zc0zz__srfi4z00(obj_t, long, int8_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long, uint32_t);
static obj_t BGl_z62listzd2ze3u32vectorz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_f32vectorzf3zf3zz__srfi4z00(obj_t);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
static obj_t BGl_z62u64vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_f32vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_f32vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL int8_t BGl_s8vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_symbol2881z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_symbol2883z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62f32vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_symbol2885z00zz__srfi4z00 = BUNSPEC;
static obj_t BGl_z62s64vectorz62zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL int16_t BGl_s16vectorzd2refzd2zz__srfi4z00(obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3s64vectorz31zz__srfi4z00(obj_t);
static obj_t BGl_z62u64vectorz62zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3u64vectorz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2s64vectorzd2zz__srfi4z00(long, int64_t);
BGL_EXPORTED_DECL long BGl_u32vectorzd2lengthzd2zz__srfi4z00(obj_t);
static obj_t BGl_z62listzd2ze3s64vectorz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2u64vectorzd2zz__srfi4z00(long, uint64_t);
static obj_t BGl_z62listzd2ze3u64vectorz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62s16vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__makezd2f32vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_s16vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_z62homogeneouszd2vectorzf3z43zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__u16vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_f32vectorzd2setz12zc0zz__srfi4z00(obj_t, long, float);
BGL_EXPORTED_DECL obj_t BGl_u64vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u64vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62u64vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62f32vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__u8vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL float BGl_f32vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_z62u8vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__makezd2f64vectorzd2zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62hvectorzd2rangezd2errorz62zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_u32vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u32vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
static obj_t BGl_z62u32vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_s32vectorzd2lengthzd2zz__srfi4z00(obj_t);
static obj_t BGl_z62s32vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__srfi4z00(void);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl__makezd2u8vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_s32vectorz00zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u32vectorz00zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_u16vectorzf3zf3zz__srfi4z00(obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_z62u16vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62s16vectorzf3z91zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_f64vectorzd2lengthzd2zz__srfi4z00(obj_t);
static obj_t BGl__makezd2s16vectorzd2zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__makezd2u16vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL int64_t BGl_s64vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_z62s8vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62u8vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_s64vectorz00zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u64vectorz00zz__srfi4z00(obj_t);
static obj_t BGl_z62s16vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_u64vectorzd2lengthzd2zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u32vectorzd2setz12zc0zz__srfi4z00(obj_t, long, uint32_t);
static obj_t BGl__makezd2s8vectorzd2zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_s64vectorzd2setz12zc0zz__srfi4z00(obj_t, long, int64_t);
BGL_EXPORTED_DECL bool_t BGl_s32vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_z62s32vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62f32vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62u16vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t BGl__s16vectorzd2copyz12zc0zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_s64vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_s64vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
static obj_t BGl_z62s64vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_f64vectorzf3zf3zz__srfi4z00(obj_t);
static obj_t BGl_z62f64vectorzd2setz12za2zz__srfi4z00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint8_t BGl_u8vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_z62listzd2ze3u8vectorz53zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62f32vectorzd2refzb0zz__srfi4z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_s32vectorzd2ze3listz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_s32vectorzd2copyz12zc0zz__srfi4z00(obj_t, long, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL uint32_t BGl_u32vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_z62s32vectorzd2ze3listz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_s64vectorzd2lengthzd2zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_u8vectorz00zz__srfi4z00(obj_t);
static obj_t BGl_z62u32vectorzd2lengthzb0zz__srfi4z00(obj_t, obj_t);
static obj_t BGl_z62f32vectorz62zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3f32vectorz31zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2f32vectorzd2zz__srfi4z00(long, float);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62listzd2ze3f32vectorz53zz__srfi4z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_u32vectorzf3zf3zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL double BGl_f64vectorzd2refzd2zz__srfi4z00(obj_t, long);
static obj_t BGl_z62s32vectorzf3z91zz__srfi4z00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string3010z00zz__srfi4z00, BgL_bgl_string3010za700za7za7_3053za7, "&list->u16vector", 16 );
DEFINE_STRING( BGl_string3011z00zz__srfi4z00, BgL_bgl_string3011za700za7za7_3054za7, "&list->s32vector", 16 );
DEFINE_STRING( BGl_string3012z00zz__srfi4z00, BgL_bgl_string3012za700za7za7_3055za7, "&list->u32vector", 16 );
DEFINE_STRING( BGl_string3013z00zz__srfi4z00, BgL_bgl_string3013za700za7za7_3056za7, "&list->s64vector", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_homogeneouszd2vectorzf3zd2envzf3zz__srfi4z00, BgL_bgl_za762homogeneousza7d3057z00, BGl_z62homogeneouszd2vectorzf3z43zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3014z00zz__srfi4z00, BgL_bgl_string3014za700za7za7_3058za7, "&list->u64vector", 16 );
DEFINE_STRING( BGl_string3015z00zz__srfi4z00, BgL_bgl_string3015za700za7za7_3059za7, "&list->f32vector", 16 );
DEFINE_STRING( BGl_string3016z00zz__srfi4z00, BgL_bgl_string3016za700za7za7_3060za7, "&list->f64vector", 16 );
DEFINE_STRING( BGl_string3018z00zz__srfi4z00, BgL_bgl_string3018za700za7za7_3061za7, "s8vector-copy!", 14 );
DEFINE_STRING( BGl_string3019z00zz__srfi4z00, BgL_bgl_string3019za700za7za7_3062za7, "wrong number of arguments: [3..5] expected, provided", 52 );
DEFINE_STRING( BGl_string2920z00zz__srfi4z00, BgL_bgl_string2920za700za7za7_3063za7, "buint8", 6 );
DEFINE_STRING( BGl_string2921z00zz__srfi4z00, BgL_bgl_string2921za700za7za7_3064za7, "u8vector-set!", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762u16vectorza762za73065za7, va_generic_entry, BGl_z62u16vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string2923z00zz__srfi4z00, BgL_bgl_string2923za700za7za7_3066za7, "make-s16vector", 14 );
DEFINE_STRING( BGl_string2924z00zz__srfi4z00, BgL_bgl_string2924za700za7za7_3067za7, "_make-s16vector", 15 );
DEFINE_STRING( BGl_string2925z00zz__srfi4z00, BgL_bgl_string2925za700za7za7_3068za7, "bint16", 6 );
DEFINE_STRING( BGl_string2926z00zz__srfi4z00, BgL_bgl_string2926za700za7za7_3069za7, "s16vector-set!", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3s16vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3s163070za7, BGl_z62listzd2ze3s16vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2928z00zz__srfi4z00, BgL_bgl_string2928za700za7za7_3071za7, "make-u16vector", 14 );
DEFINE_STRING( BGl_string2929z00zz__srfi4z00, BgL_bgl_string2929za700za7za7_3072za7, "_make-u16vector", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2s16vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2s16vecto3073za7, opt_generic_entry, BGl__makezd2s16vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__s64vectorza7d2cop3074za7, opt_generic_entry, BGl__s64vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762u8vectorza7f3za793075za7, BGl_z62u8vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3020z00zz__srfi4z00, BgL_bgl_string3020za700za7za7_3076za7, "_s8vector-copy!", 15 );
DEFINE_STRING( BGl_string3021z00zz__srfi4z00, BgL_bgl_string3021za700za7za7_3077za7, "Illegal target start index", 26 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__s8vectorza7d2copy3078za7, opt_generic_entry, BGl__s8vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3022z00zz__srfi4z00, BgL_bgl_string3022za700za7za7_3079za7, "Illegal source start index", 26 );
DEFINE_STRING( BGl_string3023z00zz__srfi4z00, BgL_bgl_string3023za700za7za7_3080za7, "Illegal source end index", 24 );
DEFINE_STRING( BGl_string3024z00zz__srfi4z00, BgL_bgl_string3024za700za7za7_3081za7, "Illegal source length", 21 );
DEFINE_STRING( BGl_string3026z00zz__srfi4z00, BgL_bgl_string3026za700za7za7_3082za7, "u8vector-copy!", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hvectorzd2rangezd2errorzd2envzd2zz__srfi4z00, BgL_bgl_za762hvectorza7d2ran3083z00, BGl_z62hvectorzd2rangezd2errorz62zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3027z00zz__srfi4z00, BgL_bgl_string3027za700za7za7_3084za7, "_u8vector-copy!", 15 );
DEFINE_STRING( BGl_string3029z00zz__srfi4z00, BgL_bgl_string3029za700za7za7_3085za7, "s16vector-copy!", 15 );
DEFINE_STRING( BGl_string2930z00zz__srfi4z00, BgL_bgl_string2930za700za7za7_3086za7, "buint16", 7 );
DEFINE_STRING( BGl_string2931z00zz__srfi4z00, BgL_bgl_string2931za700za7za7_3087za7, "u16vector-set!", 14 );
DEFINE_STRING( BGl_string2933z00zz__srfi4z00, BgL_bgl_string2933za700za7za7_3088za7, "make-s32vector", 14 );
DEFINE_STRING( BGl_string2934z00zz__srfi4z00, BgL_bgl_string2934za700za7za7_3089za7, "_make-s32vector", 15 );
DEFINE_STRING( BGl_string2935z00zz__srfi4z00, BgL_bgl_string2935za700za7za7_3090za7, "bint32", 6 );
DEFINE_STRING( BGl_string2936z00zz__srfi4z00, BgL_bgl_string2936za700za7za7_3091za7, "s32vector-set!", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__u32vectorza7d2cop3092za7, opt_generic_entry, BGl__u32vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2938z00zz__srfi4z00, BgL_bgl_string2938za700za7za7_3093za7, "make-u32vector", 14 );
DEFINE_STRING( BGl_string2939z00zz__srfi4z00, BgL_bgl_string2939za700za7za7_3094za7, "_make-u32vector", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762s32vectorza7d2l3095z00, BGl_z62s32vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762s16vectorza7d2l3096z00, BGl_z62s16vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3f32vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3f323097za7, BGl_z62listzd2ze3f32vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3030z00zz__srfi4z00, BgL_bgl_string3030za700za7za7_3098za7, "_s16vector-copy!", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762s16vectorza7d2s3099z00, BGl_z62s16vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3032z00zz__srfi4z00, BgL_bgl_string3032za700za7za7_3100za7, "u16vector-copy!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2f32vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2f32vecto3101za7, opt_generic_entry, BGl__makezd2f32vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3033z00zz__srfi4z00, BgL_bgl_string3033za700za7za7_3102za7, "_u16vector-copy!", 16 );
DEFINE_STRING( BGl_string3035z00zz__srfi4z00, BgL_bgl_string3035za700za7za7_3103za7, "s32vector-copy!", 15 );
DEFINE_STRING( BGl_string3036z00zz__srfi4z00, BgL_bgl_string3036za700za7za7_3104za7, "_s32vector-copy!", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762f64vectorza7d2r3105z00, BGl_z62f64vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762u16vectorza7f3za73106za7, BGl_z62u16vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3038z00zz__srfi4z00, BgL_bgl_string3038za700za7za7_3107za7, "u32vector-copy!", 15 );
DEFINE_STRING( BGl_string3039z00zz__srfi4z00, BgL_bgl_string3039za700za7za7_3108za7, "_u32vector-copy!", 16 );
DEFINE_STRING( BGl_string2940z00zz__srfi4z00, BgL_bgl_string2940za700za7za7_3109za7, "buint32", 7 );
DEFINE_STRING( BGl_string2941z00zz__srfi4z00, BgL_bgl_string2941za700za7za7_3110za7, "u32vector-set!", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762f64vectorza7d2l3111z00, BGl_z62f64vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2943z00zz__srfi4z00, BgL_bgl_string2943za700za7za7_3112za7, "make-s64vector", 14 );
DEFINE_STRING( BGl_string2944z00zz__srfi4z00, BgL_bgl_string2944za700za7za7_3113za7, "_make-s64vector", 15 );
DEFINE_STRING( BGl_string2945z00zz__srfi4z00, BgL_bgl_string2945za700za7za7_3114za7, "bint64", 6 );
DEFINE_STRING( BGl_string2946z00zz__srfi4z00, BgL_bgl_string2946za700za7za7_3115za7, "s64vector-set!", 14 );
DEFINE_STRING( BGl_string2865z00zz__srfi4z00, BgL_bgl_string2865za700za7za7_3116za7, "homogeneous-vector-info", 23 );
DEFINE_STRING( BGl_string2866z00zz__srfi4z00, BgL_bgl_string2866za700za7za7_3117za7, "Illegal hvector ident", 21 );
DEFINE_STRING( BGl_string2948z00zz__srfi4z00, BgL_bgl_string2948za700za7za7_3118za7, "make-u64vector", 14 );
DEFINE_STRING( BGl_string2949z00zz__srfi4z00, BgL_bgl_string2949za700za7za7_3119za7, "_make-u64vector", 15 );
DEFINE_STRING( BGl_string2868z00zz__srfi4z00, BgL_bgl_string2868za700za7za7_3120za7, "s8", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762s64vectorza7d2r3121z00, BGl_z62s64vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762u64vectorza7d2r3122z00, BGl_z62u64vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__f32vectorza7d2cop3123za7, opt_generic_entry, BGl__f32vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3041z00zz__srfi4z00, BgL_bgl_string3041za700za7za7_3124za7, "s64vector-copy!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__u8vectorza7d2copy3125za7, opt_generic_entry, BGl__u8vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3042z00zz__srfi4z00, BgL_bgl_string3042za700za7za7_3126za7, "_s64vector-copy!", 16 );
DEFINE_STRING( BGl_string3044z00zz__srfi4z00, BgL_bgl_string3044za700za7za7_3127za7, "u64vector-copy!", 15 );
DEFINE_STRING( BGl_string3045z00zz__srfi4z00, BgL_bgl_string3045za700za7za7_3128za7, "_u64vector-copy!", 16 );
DEFINE_STRING( BGl_string3047z00zz__srfi4z00, BgL_bgl_string3047za700za7za7_3129za7, "f32vector-copy!", 15 );
DEFINE_STRING( BGl_string3048z00zz__srfi4z00, BgL_bgl_string3048za700za7za7_3130za7, "_f32vector-copy!", 16 );
DEFINE_STRING( BGl_string2950z00zz__srfi4z00, BgL_bgl_string2950za700za7za7_3131za7, "buint64", 7 );
DEFINE_STRING( BGl_string2951z00zz__srfi4z00, BgL_bgl_string2951za700za7za7_3132za7, "u64vector-set!", 14 );
DEFINE_STRING( BGl_string2870z00zz__srfi4z00, BgL_bgl_string2870za700za7za7_3133za7, "u8", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762u64vectorza762za73134za7, va_generic_entry, BGl_z62u64vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string2953z00zz__srfi4z00, BgL_bgl_string2953za700za7za7_3135za7, "make-f32vector", 14 );
DEFINE_STRING( BGl_string2872z00zz__srfi4z00, BgL_bgl_string2872za700za7za7_3136za7, "s16", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762s16vectorza7d2za73137za7, BGl_z62s16vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2954z00zz__srfi4z00, BgL_bgl_string2954za700za7za7_3138za7, "_make-f32vector", 15 );
DEFINE_STRING( BGl_string2955z00zz__srfi4z00, BgL_bgl_string2955za700za7za7_3139za7, "real", 4 );
DEFINE_STRING( BGl_string2874z00zz__srfi4z00, BgL_bgl_string2874za700za7za7_3140za7, "u16", 3 );
DEFINE_STRING( BGl_string2956z00zz__srfi4z00, BgL_bgl_string2956za700za7za7_3141za7, "f32vector-set!", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3s64vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3s643142za7, BGl_z62listzd2ze3s64vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
extern obj_t BGl_zd3u32zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762s16vectorza7d2r3143z00, BGl_z62s16vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2876z00zz__srfi4z00, BgL_bgl_string2876za700za7za7_3144za7, "s32", 3 );
DEFINE_STRING( BGl_string2958z00zz__srfi4z00, BgL_bgl_string2958za700za7za7_3145za7, "make-f64vector", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762u16vectorza7d2r3146z00, BGl_z62u16vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2959z00zz__srfi4z00, BgL_bgl_string2959za700za7za7_3147za7, "_make-f64vector", 15 );
DEFINE_STRING( BGl_string2878z00zz__srfi4z00, BgL_bgl_string2878za700za7za7_3148za7, "u32", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2s64vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2s64vecto3149za7, opt_generic_entry, BGl__makezd2s64vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string3050z00zz__srfi4z00, BgL_bgl_string3050za700za7za7_3150za7, "f64vector-copy!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762u16vectorza7d2s3151z00, BGl_z62u16vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3051z00zz__srfi4z00, BgL_bgl_string3051za700za7za7_3152za7, "_f64vector-copy!", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762s64vectorza7d2s3153z00, BGl_z62s64vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
extern obj_t BGl_zd3u8zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762s32vectorza7f3za73154za7, BGl_z62s32vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3052z00zz__srfi4z00, BgL_bgl_string3052za700za7za7_3155za7, "__srfi4", 7 );
DEFINE_STRING( BGl_string2960z00zz__srfi4z00, BgL_bgl_string2960za700za7za7_3156za7, "f64vector-set!", 14 );
DEFINE_STRING( BGl_string2961z00zz__srfi4z00, BgL_bgl_string2961za700za7za7_3157za7, "index out of range [0..", 23 );
DEFINE_STRING( BGl_string2880z00zz__srfi4z00, BgL_bgl_string2880za700za7za7_3158za7, "s64", 3 );
DEFINE_STRING( BGl_string2962z00zz__srfi4z00, BgL_bgl_string2962za700za7za7_3159za7, "]", 1 );
DEFINE_STRING( BGl_string2963z00zz__srfi4z00, BgL_bgl_string2963za700za7za7_3160za7, "&hvector-range-error", 20 );
DEFINE_STRING( BGl_string2882z00zz__srfi4z00, BgL_bgl_string2882za700za7za7_3161za7, "u64", 3 );
DEFINE_STRING( BGl_string2964z00zz__srfi4z00, BgL_bgl_string2964za700za7za7_3162za7, "bstring", 7 );
DEFINE_STRING( BGl_string2965z00zz__srfi4z00, BgL_bgl_string2965za700za7za7_3163za7, "s8vector-ref", 12 );
DEFINE_STRING( BGl_string2884z00zz__srfi4z00, BgL_bgl_string2884za700za7za7_3164za7, "f32", 3 );
DEFINE_STRING( BGl_string2966z00zz__srfi4z00, BgL_bgl_string2966za700za7za7_3165za7, "&s8vector-ref", 13 );
DEFINE_STRING( BGl_string2967z00zz__srfi4z00, BgL_bgl_string2967za700za7za7_3166za7, "u8vector-ref", 12 );
DEFINE_STRING( BGl_string2886z00zz__srfi4z00, BgL_bgl_string2886za700za7za7_3167za7, "f64", 3 );
DEFINE_STRING( BGl_string2968z00zz__srfi4z00, BgL_bgl_string2968za700za7za7_3168za7, "&u8vector-ref", 13 );
DEFINE_STRING( BGl_string2887z00zz__srfi4z00, BgL_bgl_string2887za700za7za7_3169za7, "hvector", 7 );
DEFINE_STRING( BGl_string2969z00zz__srfi4z00, BgL_bgl_string2969za700za7za7_3170za7, "s16vector-ref", 13 );
DEFINE_STRING( BGl_string2888z00zz__srfi4z00, BgL_bgl_string2888za700za7za7_3171za7, "/tmp/bigloo/runtime/Llib/srfi4.scm", 34 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__u64vectorza7d2cop3172za7, opt_generic_entry, BGl__u64vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2889z00zz__srfi4z00, BgL_bgl_string2889za700za7za7_3173za7, "&s8vector-length", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762f64vectorza7f3za73174za7, BGl_z62f64vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2s8vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2s8vector3175za7, opt_generic_entry, BGl__makezd2s8vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762u8vectorza7d2re3176z00, BGl_z62u8vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762s32vectorza7d2za73177za7, BGl_z62s32vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2970z00zz__srfi4z00, BgL_bgl_string2970za700za7za7_3178za7, "&s16vector-ref", 14 );
DEFINE_STRING( BGl_string2971z00zz__srfi4z00, BgL_bgl_string2971za700za7za7_3179za7, "u16vector-ref", 13 );
DEFINE_STRING( BGl_string2890z00zz__srfi4z00, BgL_bgl_string2890za700za7za7_3180za7, "s8vector", 8 );
DEFINE_STRING( BGl_string2972z00zz__srfi4z00, BgL_bgl_string2972za700za7za7_3181za7, "&u16vector-ref", 14 );
DEFINE_STRING( BGl_string2891z00zz__srfi4z00, BgL_bgl_string2891za700za7za7_3182za7, "&u8vector-length", 16 );
DEFINE_STRING( BGl_string2973z00zz__srfi4z00, BgL_bgl_string2973za700za7za7_3183za7, "s32vector-ref", 13 );
DEFINE_STRING( BGl_string2892z00zz__srfi4z00, BgL_bgl_string2892za700za7za7_3184za7, "u8vector", 8 );
DEFINE_STRING( BGl_string2974z00zz__srfi4z00, BgL_bgl_string2974za700za7za7_3185za7, "&s32vector-ref", 14 );
DEFINE_STRING( BGl_string2893z00zz__srfi4z00, BgL_bgl_string2893za700za7za7_3186za7, "&s16vector-length", 17 );
DEFINE_STRING( BGl_string2975z00zz__srfi4z00, BgL_bgl_string2975za700za7za7_3187za7, "u32vector-ref", 13 );
DEFINE_STRING( BGl_string2894z00zz__srfi4z00, BgL_bgl_string2894za700za7za7_3188za7, "s16vector", 9 );
DEFINE_STRING( BGl_string2976z00zz__srfi4z00, BgL_bgl_string2976za700za7za7_3189za7, "&u32vector-ref", 14 );
DEFINE_STRING( BGl_string2895z00zz__srfi4z00, BgL_bgl_string2895za700za7za7_3190za7, "&u16vector-length", 17 );
DEFINE_STRING( BGl_string2977z00zz__srfi4z00, BgL_bgl_string2977za700za7za7_3191za7, "s64vector-ref", 13 );
DEFINE_STRING( BGl_string2896z00zz__srfi4z00, BgL_bgl_string2896za700za7za7_3192za7, "u16vector", 9 );
DEFINE_STRING( BGl_string2978z00zz__srfi4z00, BgL_bgl_string2978za700za7za7_3193za7, "&s64vector-ref", 14 );
DEFINE_STRING( BGl_string2897z00zz__srfi4z00, BgL_bgl_string2897za700za7za7_3194za7, "&s32vector-length", 17 );
DEFINE_STRING( BGl_string2979z00zz__srfi4z00, BgL_bgl_string2979za700za7za7_3195za7, "u64vector-ref", 13 );
DEFINE_STRING( BGl_string2898z00zz__srfi4z00, BgL_bgl_string2898za700za7za7_3196za7, "s32vector", 9 );
DEFINE_STRING( BGl_string2899z00zz__srfi4z00, BgL_bgl_string2899za700za7za7_3197za7, "&u32vector-length", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762s32vectorza7d2s3198z00, BGl_z62s32vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762s32vectorza762za73199za7, va_generic_entry, BGl_z62s32vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__f64vectorza7d2cop3200za7, opt_generic_entry, BGl__f64vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762u64vectorza7d2s3201z00, BGl_z62u64vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2980z00zz__srfi4z00, BgL_bgl_string2980za700za7za7_3202za7, "&u64vector-ref", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3u32vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3u323203za7, BGl_z62listzd2ze3u32vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2981z00zz__srfi4z00, BgL_bgl_string2981za700za7za7_3204za7, "f32vector-ref", 13 );
DEFINE_STRING( BGl_string2982z00zz__srfi4z00, BgL_bgl_string2982za700za7za7_3205za7, "&f32vector-ref", 14 );
DEFINE_STRING( BGl_string2983z00zz__srfi4z00, BgL_bgl_string2983za700za7za7_3206za7, "f64vector-ref", 13 );
DEFINE_STRING( BGl_string2984z00zz__srfi4z00, BgL_bgl_string2984za700za7za7_3207za7, "&f64vector-ref", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2u32vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2u32vecto3208za7, opt_generic_entry, BGl__makezd2u32vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762s8vectorza7d2se3209z00, BGl_z62s8vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2985z00zz__srfi4z00, BgL_bgl_string2985za700za7za7_3210za7, "&s8vector-set!", 14 );
DEFINE_STRING( BGl_string2986z00zz__srfi4z00, BgL_bgl_string2986za700za7za7_3211za7, "&u8vector-set!", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762u8vectorza7d2se3212z00, BGl_z62u8vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string2987z00zz__srfi4z00, BgL_bgl_string2987za700za7za7_3213za7, "&s16vector-set!", 15 );
DEFINE_STRING( BGl_string2988z00zz__srfi4z00, BgL_bgl_string2988za700za7za7_3214za7, "&u16vector-set!", 15 );
DEFINE_STRING( BGl_string2989z00zz__srfi4z00, BgL_bgl_string2989za700za7za7_3215za7, "&s32vector-set!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762f64vectorza7d2s3216z00, BGl_z62f64vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762s8vectorza7d2re3217z00, BGl_z62s8vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3f64vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3f643218za7, BGl_z62listzd2ze3f64vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2f64vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2f64vecto3219za7, opt_generic_entry, BGl__makezd2f64vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_STRING( BGl_string2990z00zz__srfi4z00, BgL_bgl_string2990za700za7za7_3220za7, "&u32vector-set!", 15 );
extern obj_t BGl_zd3s32zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_STRING( BGl_string2991z00zz__srfi4z00, BgL_bgl_string2991za700za7za7_3221za7, "&s64vector-set!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762u16vectorza7d2za73222za7, BGl_z62u16vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2992z00zz__srfi4z00, BgL_bgl_string2992za700za7za7_3223za7, "&u64vector-set!", 15 );
DEFINE_STRING( BGl_string2993z00zz__srfi4z00, BgL_bgl_string2993za700za7za7_3224za7, "&f32vector-set!", 15 );
DEFINE_STRING( BGl_string2994z00zz__srfi4z00, BgL_bgl_string2994za700za7za7_3225za7, "&f64vector-set!", 15 );
DEFINE_STRING( BGl_string2995z00zz__srfi4z00, BgL_bgl_string2995za700za7za7_3226za7, "&s8vector->list", 15 );
DEFINE_STRING( BGl_string2996z00zz__srfi4z00, BgL_bgl_string2996za700za7za7_3227za7, "&u8vector->list", 15 );
DEFINE_STRING( BGl_string2997z00zz__srfi4z00, BgL_bgl_string2997za700za7za7_3228za7, "&s16vector->list", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762u32vectorza7d2l3229z00, BGl_z62u32vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2998z00zz__srfi4z00, BgL_bgl_string2998za700za7za7_3230za7, "&u16vector->list", 16 );
DEFINE_STRING( BGl_string2999z00zz__srfi4z00, BgL_bgl_string2999za700za7za7_3231za7, "&s32vector->list", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762s8vectorza762za7za73232z00, va_generic_entry, BGl_z62s8vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762u16vectorza7d2l3233z00, BGl_z62u16vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762u32vectorza7d2s3234z00, BGl_z62u32vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762s16vectorza7f3za73235za7, BGl_z62s16vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762s16vectorza762za73236za7, va_generic_entry, BGl_z62s16vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762s8vectorza7d2le3237z00, BGl_z62s8vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762u64vectorza7f3za73238za7, BGl_z62u64vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3u16vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3u163239za7, BGl_z62listzd2ze3u16vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762s64vectorza7d2za73240za7, BGl_z62s64vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzd2setz12zd2envz12zz__srfi4z00, BgL_bgl_za762f32vectorza7d2s3241z00, BGl_z62f32vectorzd2setz12za2zz__srfi4z00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2u16vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2u16vecto3242za7, opt_generic_entry, BGl__makezd2u16vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762s8vectorza7f3za793243za7, BGl_z62s8vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3s8vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3s8v3244za7, BGl_z62listzd2ze3s8vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s8vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762s8vectorza7d2za7e3245za7, BGl_z62s8vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762f32vectorza7d2r3246z00, BGl_z62f32vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762u32vectorza7d2za73247za7, BGl_z62u32vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762s32vectorza7d2r3248z00, BGl_z62s32vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzd2refzd2envz00zz__srfi4z00, BgL_bgl_za762u32vectorza7d2r3249z00, BGl_z62u32vectorzd2refzb0zz__srfi4z00, 0L, BUNSPEC, 2 );
extern obj_t BGl_zd3u16zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762f32vectorza762za73250za7, va_generic_entry, BGl_z62f32vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762s64vectorza7d2l3251z00, BGl_z62s64vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
extern obj_t BGl_zd3s8zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762f32vectorza7d2l3252z00, BGl_z62f32vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
extern obj_t BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00;
extern obj_t BGl_zd3u64zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762f32vectorza7f3za73253za7, BGl_z62f32vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f32vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762f32vectorza7d2za73254za7, BGl_z62f32vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762u8vectorza7d2za7e3255za7, BGl_z62u8vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2u8vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2u8vector3256za7, opt_generic_entry, BGl__makezd2u8vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762s64vectorza762za73257za7, va_generic_entry, BGl_z62s64vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s16vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__s16vectorza7d2cop3258za7, opt_generic_entry, BGl__s16vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3u64vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3u643259za7, BGl_z62listzd2ze3u64vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2u64vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2u64vecto3260za7, opt_generic_entry, BGl__makezd2u64vectorzd2zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762u8vectorza7d2le3261z00, BGl_z62u8vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762u64vectorza7d2za73262za7, BGl_z62u64vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s32vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__s32vectorza7d2cop3263za7, opt_generic_entry, BGl__s32vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
extern obj_t BGl_zd3s16zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u8vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762u8vectorza762za7za73264z00, va_generic_entry, BGl_z62u8vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_homogeneouszd2vectorzd2infozd2envzd2zz__srfi4z00, BgL_bgl_za762homogeneousza7d3265z00, BGl_z62homogeneouszd2vectorzd2infoz62zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzd2ze3listzd2envze3zz__srfi4z00, BgL_bgl_za762f64vectorza7d2za73266za7, BGl_z62f64vectorzd2ze3listz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2900z00zz__srfi4z00, BgL_bgl_string2900za700za7za7_3267za7, "u32vector", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762u32vectorza7f3za73268za7, BGl_z62u32vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2901z00zz__srfi4z00, BgL_bgl_string2901za700za7za7_3269za7, "&s64vector-length", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u32vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762u32vectorza762za73270za7, va_generic_entry, BGl_z62u32vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string2902z00zz__srfi4z00, BgL_bgl_string2902za700za7za7_3271za7, "s64vector", 9 );
DEFINE_STRING( BGl_string2903z00zz__srfi4z00, BgL_bgl_string2903za700za7za7_3272za7, "&u64vector-length", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_s64vectorzf3zd2envz21zz__srfi4z00, BgL_bgl_za762s64vectorza7f3za73273za7, BGl_z62s64vectorzf3z91zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2904z00zz__srfi4z00, BgL_bgl_string2904za700za7za7_3274za7, "u64vector", 9 );
DEFINE_STRING( BGl_string2905z00zz__srfi4z00, BgL_bgl_string2905za700za7za7_3275za7, "&f32vector-length", 17 );
DEFINE_STRING( BGl_string2906z00zz__srfi4z00, BgL_bgl_string2906za700za7za7_3276za7, "f32vector", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3s32vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3s323277za7, BGl_z62listzd2ze3s32vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2907z00zz__srfi4z00, BgL_bgl_string2907za700za7za7_3278za7, "&f64vector-length", 17 );
DEFINE_STRING( BGl_string2908z00zz__srfi4z00, BgL_bgl_string2908za700za7za7_3279za7, "f64vector", 9 );
DEFINE_STRING( BGl_string2909z00zz__srfi4z00, BgL_bgl_string2909za700za7za7_3280za7, "pair-nil", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2s32vectorzd2envz00zz__srfi4z00, BgL_bgl__makeza7d2s32vecto3281za7, opt_generic_entry, BGl__makezd2s32vectorzd2zz__srfi4z00, BFALSE, -1 );
extern obj_t BGl_zd3s64zd2envz01zz__r4_numbers_6_5_fixnumz00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_f64vectorzd2envzd2zz__srfi4z00, BgL_bgl_za762f64vectorza762za73282za7, va_generic_entry, BGl_z62f64vectorz62zz__srfi4z00, BUNSPEC, -1 );
DEFINE_STRING( BGl_string3000z00zz__srfi4z00, BgL_bgl_string3000za700za7za7_3283za7, "&u32vector->list", 16 );
DEFINE_STRING( BGl_string3001z00zz__srfi4z00, BgL_bgl_string3001za700za7za7_3284za7, "&s64vector->list", 16 );
DEFINE_STRING( BGl_string3002z00zz__srfi4z00, BgL_bgl_string3002za700za7za7_3285za7, "&u64vector->list", 16 );
DEFINE_STRING( BGl_string3003z00zz__srfi4z00, BgL_bgl_string3003za700za7za7_3286za7, "&f32vector->list", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3u8vectorzd2envze3zz__srfi4z00, BgL_bgl_za762listza7d2za7e3u8v3287za7, BGl_z62listzd2ze3u8vectorz53zz__srfi4z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3004z00zz__srfi4z00, BgL_bgl_string3004za700za7za7_3288za7, "&f64vector->list", 16 );
DEFINE_STRING( BGl_string3005z00zz__srfi4z00, BgL_bgl_string3005za700za7za7_3289za7, "loop", 4 );
DEFINE_STRING( BGl_string3006z00zz__srfi4z00, BgL_bgl_string3006za700za7za7_3290za7, "pair", 4 );
DEFINE_STRING( BGl_string3007z00zz__srfi4z00, BgL_bgl_string3007za700za7za7_3291za7, "&list->s8vector", 15 );
DEFINE_STRING( BGl_string3008z00zz__srfi4z00, BgL_bgl_string3008za700za7za7_3292za7, "&list->u8vector", 15 );
DEFINE_STRING( BGl_string3009z00zz__srfi4z00, BgL_bgl_string3009za700za7za7_3293za7, "&list->s16vector", 16 );
DEFINE_STRING( BGl_string2911z00zz__srfi4z00, BgL_bgl_string2911za700za7za7_3294za7, "make-s8vector", 13 );
DEFINE_STRING( BGl_string2912z00zz__srfi4z00, BgL_bgl_string2912za700za7za7_3295za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string2913z00zz__srfi4z00, BgL_bgl_string2913za700za7za7_3296za7, "_make-s8vector", 14 );
DEFINE_STRING( BGl_string2914z00zz__srfi4z00, BgL_bgl_string2914za700za7za7_3297za7, "bint", 4 );
DEFINE_STRING( BGl_string2915z00zz__srfi4z00, BgL_bgl_string2915za700za7za7_3298za7, "bint8", 5 );
DEFINE_STRING( BGl_string2916z00zz__srfi4z00, BgL_bgl_string2916za700za7za7_3299za7, "s8vector-set!", 13 );
DEFINE_STRING( BGl_string2918z00zz__srfi4z00, BgL_bgl_string2918za700za7za7_3300za7, "make-u8vector", 13 );
DEFINE_STRING( BGl_string2919z00zz__srfi4z00, BgL_bgl_string2919za700za7za7_3301za7, "_make-u8vector", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u16vectorzd2copyz12zd2envz12zz__srfi4z00, BgL_bgl__u16vectorza7d2cop3302za7, opt_generic_entry, BGl__u16vectorzd2copyz12zc0zz__srfi4z00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_u64vectorzd2lengthzd2envz00zz__srfi4z00, BgL_bgl_za762u64vectorza7d2l3303z00, BGl_z62u64vectorzd2lengthzb0zz__srfi4z00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2910z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2917z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3017z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2922z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2927z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3025z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3028z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2932z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2937z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3031z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3034z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3037z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2942z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2947z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2867z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2869z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3040z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3043z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3046z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol3049z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2952z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2871z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2873z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2875z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2957z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2877z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2879z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2881z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2883z00zz__srfi4z00) );
ADD_ROOT( (void *)(&BGl_symbol2885z00zz__srfi4z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long BgL_checksumz00_4171, char * BgL_fromz00_4172)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__srfi4z00))
{ 
BGl_requirezd2initializa7ationz75zz__srfi4z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__srfi4z00(); 
BGl_cnstzd2initzd2zz__srfi4z00(); 
BGl_importedzd2moduleszd2initz00zz__srfi4z00(); 
return 
BGl_methodzd2initzd2zz__srfi4z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__srfi4z00(void)
{
{ /* Llib/srfi4.scm 15 */
BGl_symbol2867z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2868z00zz__srfi4z00); 
BGl_symbol2869z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2870z00zz__srfi4z00); 
BGl_symbol2871z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2872z00zz__srfi4z00); 
BGl_symbol2873z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2874z00zz__srfi4z00); 
BGl_symbol2875z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2876z00zz__srfi4z00); 
BGl_symbol2877z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2878z00zz__srfi4z00); 
BGl_symbol2879z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2880z00zz__srfi4z00); 
BGl_symbol2881z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2882z00zz__srfi4z00); 
BGl_symbol2883z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2884z00zz__srfi4z00); 
BGl_symbol2885z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2886z00zz__srfi4z00); 
BGl_symbol2910z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2911z00zz__srfi4z00); 
BGl_symbol2917z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2918z00zz__srfi4z00); 
BGl_symbol2922z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2923z00zz__srfi4z00); 
BGl_symbol2927z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2928z00zz__srfi4z00); 
BGl_symbol2932z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2933z00zz__srfi4z00); 
BGl_symbol2937z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2938z00zz__srfi4z00); 
BGl_symbol2942z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2943z00zz__srfi4z00); 
BGl_symbol2947z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2948z00zz__srfi4z00); 
BGl_symbol2952z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2953z00zz__srfi4z00); 
BGl_symbol2957z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string2958z00zz__srfi4z00); 
BGl_symbol3017z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3018z00zz__srfi4z00); 
BGl_symbol3025z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3026z00zz__srfi4z00); 
BGl_symbol3028z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3029z00zz__srfi4z00); 
BGl_symbol3031z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3032z00zz__srfi4z00); 
BGl_symbol3034z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3035z00zz__srfi4z00); 
BGl_symbol3037z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3038z00zz__srfi4z00); 
BGl_symbol3040z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3041z00zz__srfi4z00); 
BGl_symbol3043z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3044z00zz__srfi4z00); 
BGl_symbol3046z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3047z00zz__srfi4z00); 
return ( 
BGl_symbol3049z00zz__srfi4z00 = 
bstring_to_symbol(BGl_string3050z00zz__srfi4z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__srfi4z00(void)
{
{ /* Llib/srfi4.scm 15 */
return 
bgl_gc_roots_register();} 

}



/* homogeneous-vector? */
BGL_EXPORTED_DEF obj_t BGl_homogeneouszd2vectorzf3z21zz__srfi4z00(obj_t BgL_xz00_3)
{
{ /* Llib/srfi4.scm 569 */
return 
BBOOL(
BGL_HVECTORP(BgL_xz00_3));} 

}



/* &homogeneous-vector? */
obj_t BGl_z62homogeneouszd2vectorzf3z43zz__srfi4z00(obj_t BgL_envz00_3033, obj_t BgL_xz00_3034)
{
{ /* Llib/srfi4.scm 569 */
return 
BGl_homogeneouszd2vectorzf3z21zz__srfi4z00(BgL_xz00_3034);} 

}



/* s8vector? */
BGL_EXPORTED_DEF bool_t BGl_s8vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_4)
{
{ /* Llib/srfi4.scm 570 */
return 
BGL_S8VECTORP(BgL_xz00_4);} 

}



/* &s8vector? */
obj_t BGl_z62s8vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3035, obj_t BgL_xz00_3036)
{
{ /* Llib/srfi4.scm 570 */
return 
BBOOL(
BGl_s8vectorzf3zf3zz__srfi4z00(BgL_xz00_3036));} 

}



/* u8vector? */
BGL_EXPORTED_DEF bool_t BGl_u8vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_5)
{
{ /* Llib/srfi4.scm 571 */
return 
BGL_U8VECTORP(BgL_xz00_5);} 

}



/* &u8vector? */
obj_t BGl_z62u8vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3037, obj_t BgL_xz00_3038)
{
{ /* Llib/srfi4.scm 571 */
return 
BBOOL(
BGl_u8vectorzf3zf3zz__srfi4z00(BgL_xz00_3038));} 

}



/* s16vector? */
BGL_EXPORTED_DEF bool_t BGl_s16vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_6)
{
{ /* Llib/srfi4.scm 572 */
return 
BGL_S16VECTORP(BgL_xz00_6);} 

}



/* &s16vector? */
obj_t BGl_z62s16vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3039, obj_t BgL_xz00_3040)
{
{ /* Llib/srfi4.scm 572 */
return 
BBOOL(
BGl_s16vectorzf3zf3zz__srfi4z00(BgL_xz00_3040));} 

}



/* u16vector? */
BGL_EXPORTED_DEF bool_t BGl_u16vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_7)
{
{ /* Llib/srfi4.scm 573 */
return 
BGL_U16VECTORP(BgL_xz00_7);} 

}



/* &u16vector? */
obj_t BGl_z62u16vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3041, obj_t BgL_xz00_3042)
{
{ /* Llib/srfi4.scm 573 */
return 
BBOOL(
BGl_u16vectorzf3zf3zz__srfi4z00(BgL_xz00_3042));} 

}



/* s32vector? */
BGL_EXPORTED_DEF bool_t BGl_s32vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_8)
{
{ /* Llib/srfi4.scm 574 */
return 
BGL_S32VECTORP(BgL_xz00_8);} 

}



/* &s32vector? */
obj_t BGl_z62s32vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3043, obj_t BgL_xz00_3044)
{
{ /* Llib/srfi4.scm 574 */
return 
BBOOL(
BGl_s32vectorzf3zf3zz__srfi4z00(BgL_xz00_3044));} 

}



/* u32vector? */
BGL_EXPORTED_DEF bool_t BGl_u32vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_9)
{
{ /* Llib/srfi4.scm 575 */
return 
BGL_U32VECTORP(BgL_xz00_9);} 

}



/* &u32vector? */
obj_t BGl_z62u32vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3045, obj_t BgL_xz00_3046)
{
{ /* Llib/srfi4.scm 575 */
return 
BBOOL(
BGl_u32vectorzf3zf3zz__srfi4z00(BgL_xz00_3046));} 

}



/* s64vector? */
BGL_EXPORTED_DEF bool_t BGl_s64vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_10)
{
{ /* Llib/srfi4.scm 576 */
return 
BGL_S64VECTORP(BgL_xz00_10);} 

}



/* &s64vector? */
obj_t BGl_z62s64vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3047, obj_t BgL_xz00_3048)
{
{ /* Llib/srfi4.scm 576 */
return 
BBOOL(
BGl_s64vectorzf3zf3zz__srfi4z00(BgL_xz00_3048));} 

}



/* u64vector? */
BGL_EXPORTED_DEF bool_t BGl_u64vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_11)
{
{ /* Llib/srfi4.scm 577 */
return 
BGL_U64VECTORP(BgL_xz00_11);} 

}



/* &u64vector? */
obj_t BGl_z62u64vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3049, obj_t BgL_xz00_3050)
{
{ /* Llib/srfi4.scm 577 */
return 
BBOOL(
BGl_u64vectorzf3zf3zz__srfi4z00(BgL_xz00_3050));} 

}



/* f32vector? */
BGL_EXPORTED_DEF bool_t BGl_f32vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_12)
{
{ /* Llib/srfi4.scm 578 */
return 
BGL_F32VECTORP(BgL_xz00_12);} 

}



/* &f32vector? */
obj_t BGl_z62f32vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3051, obj_t BgL_xz00_3052)
{
{ /* Llib/srfi4.scm 578 */
return 
BBOOL(
BGl_f32vectorzf3zf3zz__srfi4z00(BgL_xz00_3052));} 

}



/* f64vector? */
BGL_EXPORTED_DEF bool_t BGl_f64vectorzf3zf3zz__srfi4z00(obj_t BgL_xz00_13)
{
{ /* Llib/srfi4.scm 579 */
return 
BGL_F64VECTORP(BgL_xz00_13);} 

}



/* &f64vector? */
obj_t BGl_z62f64vectorzf3z91zz__srfi4z00(obj_t BgL_envz00_3053, obj_t BgL_xz00_3054)
{
{ /* Llib/srfi4.scm 579 */
return 
BBOOL(
BGl_f64vectorzf3zf3zz__srfi4z00(BgL_xz00_3054));} 

}



/* homogeneous-vector-info */
BGL_EXPORTED_DEF obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t BgL_oz00_14)
{
{ /* Llib/srfi4.scm 584 */
if(
BGL_HVECTORP(BgL_oz00_14))
{ 

{ /* Llib/srfi4.scm 586 */
 int BgL_aux1043z00_1343;
BgL_aux1043z00_1343 = 
BGL_HVECTOR_IDENT(BgL_oz00_14); 
switch( 
(long)(BgL_aux1043z00_1343)) { case 0L : 

{ /* Llib/srfi4.scm 590 */
 obj_t BgL_val0_1145z00_1344;
BgL_val0_1145z00_1344 = BGl_symbol2867z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 590 */
 int BgL_tmpz00_4247;
BgL_tmpz00_4247 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4247); } 
{ /* Llib/srfi4.scm 590 */
 obj_t BgL_auxz00_4252; int BgL_tmpz00_4250;
BgL_auxz00_4252 = 
BINT(1L); 
BgL_tmpz00_4250 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4250, BgL_auxz00_4252); } 
{ /* Llib/srfi4.scm 590 */
 int BgL_tmpz00_4255;
BgL_tmpz00_4255 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4255, BGl_s8vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 590 */
 int BgL_tmpz00_4258;
BgL_tmpz00_4258 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4258, BGl_s8vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 590 */
 int BgL_tmpz00_4261;
BgL_tmpz00_4261 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4261, BGl_zd3s8zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1145z00_1344;} break;case 1L : 

{ /* Llib/srfi4.scm 592 */
 obj_t BgL_val0_1150z00_1349;
BgL_val0_1150z00_1349 = BGl_symbol2869z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 592 */
 int BgL_tmpz00_4264;
BgL_tmpz00_4264 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4264); } 
{ /* Llib/srfi4.scm 592 */
 obj_t BgL_auxz00_4269; int BgL_tmpz00_4267;
BgL_auxz00_4269 = 
BINT(1L); 
BgL_tmpz00_4267 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4267, BgL_auxz00_4269); } 
{ /* Llib/srfi4.scm 592 */
 int BgL_tmpz00_4272;
BgL_tmpz00_4272 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4272, BGl_u8vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 592 */
 int BgL_tmpz00_4275;
BgL_tmpz00_4275 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4275, BGl_u8vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 592 */
 int BgL_tmpz00_4278;
BgL_tmpz00_4278 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4278, BGl_zd3u8zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1150z00_1349;} break;case 2L : 

{ /* Llib/srfi4.scm 594 */
 obj_t BgL_val0_1155z00_1354;
BgL_val0_1155z00_1354 = BGl_symbol2871z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 594 */
 int BgL_tmpz00_4281;
BgL_tmpz00_4281 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4281); } 
{ /* Llib/srfi4.scm 594 */
 obj_t BgL_auxz00_4286; int BgL_tmpz00_4284;
BgL_auxz00_4286 = 
BINT(2L); 
BgL_tmpz00_4284 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4284, BgL_auxz00_4286); } 
{ /* Llib/srfi4.scm 594 */
 int BgL_tmpz00_4289;
BgL_tmpz00_4289 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4289, BGl_s16vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 594 */
 int BgL_tmpz00_4292;
BgL_tmpz00_4292 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4292, BGl_s16vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 594 */
 int BgL_tmpz00_4295;
BgL_tmpz00_4295 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4295, BGl_zd3s16zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1155z00_1354;} break;case 3L : 

{ /* Llib/srfi4.scm 596 */
 obj_t BgL_val0_1160z00_1359;
BgL_val0_1160z00_1359 = BGl_symbol2873z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 596 */
 int BgL_tmpz00_4298;
BgL_tmpz00_4298 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4298); } 
{ /* Llib/srfi4.scm 596 */
 obj_t BgL_auxz00_4303; int BgL_tmpz00_4301;
BgL_auxz00_4303 = 
BINT(2L); 
BgL_tmpz00_4301 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4301, BgL_auxz00_4303); } 
{ /* Llib/srfi4.scm 596 */
 int BgL_tmpz00_4306;
BgL_tmpz00_4306 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4306, BGl_u16vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 596 */
 int BgL_tmpz00_4309;
BgL_tmpz00_4309 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4309, BGl_u16vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 596 */
 int BgL_tmpz00_4312;
BgL_tmpz00_4312 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4312, BGl_zd3u16zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1160z00_1359;} break;case 4L : 

{ /* Llib/srfi4.scm 598 */
 obj_t BgL_val0_1165z00_1364;
BgL_val0_1165z00_1364 = BGl_symbol2875z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 598 */
 int BgL_tmpz00_4315;
BgL_tmpz00_4315 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4315); } 
{ /* Llib/srfi4.scm 598 */
 obj_t BgL_auxz00_4320; int BgL_tmpz00_4318;
BgL_auxz00_4320 = 
BINT(4L); 
BgL_tmpz00_4318 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4318, BgL_auxz00_4320); } 
{ /* Llib/srfi4.scm 598 */
 int BgL_tmpz00_4323;
BgL_tmpz00_4323 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4323, BGl_s32vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 598 */
 int BgL_tmpz00_4326;
BgL_tmpz00_4326 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4326, BGl_s32vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 598 */
 int BgL_tmpz00_4329;
BgL_tmpz00_4329 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4329, BGl_zd3s32zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1165z00_1364;} break;case 5L : 

{ /* Llib/srfi4.scm 600 */
 obj_t BgL_val0_1170z00_1369;
BgL_val0_1170z00_1369 = BGl_symbol2877z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 600 */
 int BgL_tmpz00_4332;
BgL_tmpz00_4332 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4332); } 
{ /* Llib/srfi4.scm 600 */
 obj_t BgL_auxz00_4337; int BgL_tmpz00_4335;
BgL_auxz00_4337 = 
BINT(4L); 
BgL_tmpz00_4335 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4335, BgL_auxz00_4337); } 
{ /* Llib/srfi4.scm 600 */
 int BgL_tmpz00_4340;
BgL_tmpz00_4340 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4340, BGl_u32vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 600 */
 int BgL_tmpz00_4343;
BgL_tmpz00_4343 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4343, BGl_u32vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 600 */
 int BgL_tmpz00_4346;
BgL_tmpz00_4346 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4346, BGl_zd3u32zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1170z00_1369;} break;case 6L : 

{ /* Llib/srfi4.scm 602 */
 obj_t BgL_val0_1175z00_1374;
BgL_val0_1175z00_1374 = BGl_symbol2879z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 602 */
 int BgL_tmpz00_4349;
BgL_tmpz00_4349 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4349); } 
{ /* Llib/srfi4.scm 602 */
 obj_t BgL_auxz00_4354; int BgL_tmpz00_4352;
BgL_auxz00_4354 = 
BINT(8L); 
BgL_tmpz00_4352 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4352, BgL_auxz00_4354); } 
{ /* Llib/srfi4.scm 602 */
 int BgL_tmpz00_4357;
BgL_tmpz00_4357 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4357, BGl_s64vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 602 */
 int BgL_tmpz00_4360;
BgL_tmpz00_4360 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4360, BGl_s64vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 602 */
 int BgL_tmpz00_4363;
BgL_tmpz00_4363 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4363, BGl_zd3s64zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1175z00_1374;} break;case 7L : 

{ /* Llib/srfi4.scm 604 */
 obj_t BgL_val0_1180z00_1379;
BgL_val0_1180z00_1379 = BGl_symbol2881z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 604 */
 int BgL_tmpz00_4366;
BgL_tmpz00_4366 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4366); } 
{ /* Llib/srfi4.scm 604 */
 obj_t BgL_auxz00_4371; int BgL_tmpz00_4369;
BgL_auxz00_4371 = 
BINT(8L); 
BgL_tmpz00_4369 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4369, BgL_auxz00_4371); } 
{ /* Llib/srfi4.scm 604 */
 int BgL_tmpz00_4374;
BgL_tmpz00_4374 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4374, BGl_u64vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 604 */
 int BgL_tmpz00_4377;
BgL_tmpz00_4377 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4377, BGl_u64vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 604 */
 int BgL_tmpz00_4380;
BgL_tmpz00_4380 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4380, BGl_zd3u64zd2envz01zz__r4_numbers_6_5_fixnumz00); } 
return BgL_val0_1180z00_1379;} break;case 8L : 

{ /* Llib/srfi4.scm 606 */
 obj_t BgL_val0_1185z00_1384;
BgL_val0_1185z00_1384 = BGl_symbol2883z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 606 */
 int BgL_tmpz00_4383;
BgL_tmpz00_4383 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4383); } 
{ /* Llib/srfi4.scm 606 */
 obj_t BgL_auxz00_4388; int BgL_tmpz00_4386;
BgL_auxz00_4388 = 
BINT(4L); 
BgL_tmpz00_4386 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4386, BgL_auxz00_4388); } 
{ /* Llib/srfi4.scm 606 */
 int BgL_tmpz00_4391;
BgL_tmpz00_4391 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4391, BGl_f32vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 606 */
 int BgL_tmpz00_4394;
BgL_tmpz00_4394 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4394, BGl_f32vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 606 */
 int BgL_tmpz00_4397;
BgL_tmpz00_4397 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4397, BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00); } 
return BgL_val0_1185z00_1384;} break;case 9L : 

{ /* Llib/srfi4.scm 608 */
 obj_t BgL_val0_1190z00_1389;
BgL_val0_1190z00_1389 = BGl_symbol2885z00zz__srfi4z00; 
{ /* Llib/srfi4.scm 608 */
 int BgL_tmpz00_4400;
BgL_tmpz00_4400 = 
(int)(5L); 
BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4400); } 
{ /* Llib/srfi4.scm 608 */
 obj_t BgL_auxz00_4405; int BgL_tmpz00_4403;
BgL_auxz00_4405 = 
BINT(8L); 
BgL_tmpz00_4403 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4403, BgL_auxz00_4405); } 
{ /* Llib/srfi4.scm 608 */
 int BgL_tmpz00_4408;
BgL_tmpz00_4408 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4408, BGl_f64vectorzd2refzd2envz00zz__srfi4z00); } 
{ /* Llib/srfi4.scm 608 */
 int BgL_tmpz00_4411;
BgL_tmpz00_4411 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4411, BGl_f64vectorzd2setz12zd2envz12zz__srfi4z00); } 
{ /* Llib/srfi4.scm 608 */
 int BgL_tmpz00_4414;
BgL_tmpz00_4414 = 
(int)(4L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_4414, BGl_zd3flzd2envz01zz__r4_numbers_6_5_flonumz00); } 
return BgL_val0_1190z00_1389;} break;
default: 
{ /* Llib/srfi4.scm 611 */
 int BgL_arg1408z00_1394;
BgL_arg1408z00_1394 = 
BGL_HVECTOR_IDENT(BgL_oz00_14); 
return 
BGl_errorz00zz__errorz00(BGl_string2865z00zz__srfi4z00, BGl_string2866z00zz__srfi4z00, 
BINT(BgL_arg1408z00_1394));} } } }  else 
{ /* Llib/srfi4.scm 585 */
return 
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string2865z00zz__srfi4z00, BGl_string2887z00zz__srfi4z00, BgL_oz00_14);} } 

}



/* &homogeneous-vector-info */
obj_t BGl_z62homogeneouszd2vectorzd2infoz62zz__srfi4z00(obj_t BgL_envz00_3055, obj_t BgL_oz00_3056)
{
{ /* Llib/srfi4.scm 584 */
return 
BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_oz00_3056);} 

}



/* s8vector-length */
BGL_EXPORTED_DEF long BGl_s8vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_15)
{
{ /* Llib/srfi4.scm 617 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_15);} 

}



/* &s8vector-length */
obj_t BGl_z62s8vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3154, obj_t BgL_xz00_3155)
{
{ /* Llib/srfi4.scm 617 */
{ /* Llib/srfi4.scm 617 */
 long BgL_tmpz00_4425;
{ /* Llib/srfi4.scm 617 */
 obj_t BgL_auxz00_4426;
if(
BGL_S8VECTORP(BgL_xz00_3155))
{ /* Llib/srfi4.scm 617 */
BgL_auxz00_4426 = BgL_xz00_3155
; }  else 
{ 
 obj_t BgL_auxz00_4429;
BgL_auxz00_4429 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25371L), BGl_string2889z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_xz00_3155); 
FAILURE(BgL_auxz00_4429,BFALSE,BFALSE);} 
BgL_tmpz00_4425 = 
BGl_s8vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4426); } 
return 
BINT(BgL_tmpz00_4425);} } 

}



/* u8vector-length */
BGL_EXPORTED_DEF long BGl_u8vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_16)
{
{ /* Llib/srfi4.scm 618 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_16);} 

}



/* &u8vector-length */
obj_t BGl_z62u8vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3156, obj_t BgL_xz00_3157)
{
{ /* Llib/srfi4.scm 618 */
{ /* Llib/srfi4.scm 618 */
 long BgL_tmpz00_4436;
{ /* Llib/srfi4.scm 618 */
 obj_t BgL_auxz00_4437;
if(
BGL_U8VECTORP(BgL_xz00_3157))
{ /* Llib/srfi4.scm 618 */
BgL_auxz00_4437 = BgL_xz00_3157
; }  else 
{ 
 obj_t BgL_auxz00_4440;
BgL_auxz00_4440 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25427L), BGl_string2891z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_xz00_3157); 
FAILURE(BgL_auxz00_4440,BFALSE,BFALSE);} 
BgL_tmpz00_4436 = 
BGl_u8vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4437); } 
return 
BINT(BgL_tmpz00_4436);} } 

}



/* s16vector-length */
BGL_EXPORTED_DEF long BGl_s16vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_17)
{
{ /* Llib/srfi4.scm 619 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_17);} 

}



/* &s16vector-length */
obj_t BGl_z62s16vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3158, obj_t BgL_xz00_3159)
{
{ /* Llib/srfi4.scm 619 */
{ /* Llib/srfi4.scm 619 */
 long BgL_tmpz00_4447;
{ /* Llib/srfi4.scm 619 */
 obj_t BgL_auxz00_4448;
if(
BGL_S16VECTORP(BgL_xz00_3159))
{ /* Llib/srfi4.scm 619 */
BgL_auxz00_4448 = BgL_xz00_3159
; }  else 
{ 
 obj_t BgL_auxz00_4451;
BgL_auxz00_4451 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25484L), BGl_string2893z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_xz00_3159); 
FAILURE(BgL_auxz00_4451,BFALSE,BFALSE);} 
BgL_tmpz00_4447 = 
BGl_s16vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4448); } 
return 
BINT(BgL_tmpz00_4447);} } 

}



/* u16vector-length */
BGL_EXPORTED_DEF long BGl_u16vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_18)
{
{ /* Llib/srfi4.scm 620 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_18);} 

}



/* &u16vector-length */
obj_t BGl_z62u16vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3160, obj_t BgL_xz00_3161)
{
{ /* Llib/srfi4.scm 620 */
{ /* Llib/srfi4.scm 620 */
 long BgL_tmpz00_4458;
{ /* Llib/srfi4.scm 620 */
 obj_t BgL_auxz00_4459;
if(
BGL_U16VECTORP(BgL_xz00_3161))
{ /* Llib/srfi4.scm 620 */
BgL_auxz00_4459 = BgL_xz00_3161
; }  else 
{ 
 obj_t BgL_auxz00_4462;
BgL_auxz00_4462 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25541L), BGl_string2895z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_xz00_3161); 
FAILURE(BgL_auxz00_4462,BFALSE,BFALSE);} 
BgL_tmpz00_4458 = 
BGl_u16vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4459); } 
return 
BINT(BgL_tmpz00_4458);} } 

}



/* s32vector-length */
BGL_EXPORTED_DEF long BGl_s32vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_19)
{
{ /* Llib/srfi4.scm 621 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_19);} 

}



/* &s32vector-length */
obj_t BGl_z62s32vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3162, obj_t BgL_xz00_3163)
{
{ /* Llib/srfi4.scm 621 */
{ /* Llib/srfi4.scm 621 */
 long BgL_tmpz00_4469;
{ /* Llib/srfi4.scm 621 */
 obj_t BgL_auxz00_4470;
if(
BGL_S32VECTORP(BgL_xz00_3163))
{ /* Llib/srfi4.scm 621 */
BgL_auxz00_4470 = BgL_xz00_3163
; }  else 
{ 
 obj_t BgL_auxz00_4473;
BgL_auxz00_4473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25598L), BGl_string2897z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_xz00_3163); 
FAILURE(BgL_auxz00_4473,BFALSE,BFALSE);} 
BgL_tmpz00_4469 = 
BGl_s32vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4470); } 
return 
BINT(BgL_tmpz00_4469);} } 

}



/* u32vector-length */
BGL_EXPORTED_DEF long BGl_u32vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_20)
{
{ /* Llib/srfi4.scm 622 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_20);} 

}



/* &u32vector-length */
obj_t BGl_z62u32vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3164, obj_t BgL_xz00_3165)
{
{ /* Llib/srfi4.scm 622 */
{ /* Llib/srfi4.scm 622 */
 long BgL_tmpz00_4480;
{ /* Llib/srfi4.scm 622 */
 obj_t BgL_auxz00_4481;
if(
BGL_U32VECTORP(BgL_xz00_3165))
{ /* Llib/srfi4.scm 622 */
BgL_auxz00_4481 = BgL_xz00_3165
; }  else 
{ 
 obj_t BgL_auxz00_4484;
BgL_auxz00_4484 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25655L), BGl_string2899z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_xz00_3165); 
FAILURE(BgL_auxz00_4484,BFALSE,BFALSE);} 
BgL_tmpz00_4480 = 
BGl_u32vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4481); } 
return 
BINT(BgL_tmpz00_4480);} } 

}



/* s64vector-length */
BGL_EXPORTED_DEF long BGl_s64vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_21)
{
{ /* Llib/srfi4.scm 623 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_21);} 

}



/* &s64vector-length */
obj_t BGl_z62s64vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3166, obj_t BgL_xz00_3167)
{
{ /* Llib/srfi4.scm 623 */
{ /* Llib/srfi4.scm 623 */
 long BgL_tmpz00_4491;
{ /* Llib/srfi4.scm 623 */
 obj_t BgL_auxz00_4492;
if(
BGL_S64VECTORP(BgL_xz00_3167))
{ /* Llib/srfi4.scm 623 */
BgL_auxz00_4492 = BgL_xz00_3167
; }  else 
{ 
 obj_t BgL_auxz00_4495;
BgL_auxz00_4495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25712L), BGl_string2901z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_xz00_3167); 
FAILURE(BgL_auxz00_4495,BFALSE,BFALSE);} 
BgL_tmpz00_4491 = 
BGl_s64vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4492); } 
return 
BINT(BgL_tmpz00_4491);} } 

}



/* u64vector-length */
BGL_EXPORTED_DEF long BGl_u64vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_22)
{
{ /* Llib/srfi4.scm 624 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_22);} 

}



/* &u64vector-length */
obj_t BGl_z62u64vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3168, obj_t BgL_xz00_3169)
{
{ /* Llib/srfi4.scm 624 */
{ /* Llib/srfi4.scm 624 */
 long BgL_tmpz00_4502;
{ /* Llib/srfi4.scm 624 */
 obj_t BgL_auxz00_4503;
if(
BGL_U64VECTORP(BgL_xz00_3169))
{ /* Llib/srfi4.scm 624 */
BgL_auxz00_4503 = BgL_xz00_3169
; }  else 
{ 
 obj_t BgL_auxz00_4506;
BgL_auxz00_4506 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25769L), BGl_string2903z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_xz00_3169); 
FAILURE(BgL_auxz00_4506,BFALSE,BFALSE);} 
BgL_tmpz00_4502 = 
BGl_u64vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4503); } 
return 
BINT(BgL_tmpz00_4502);} } 

}



/* f32vector-length */
BGL_EXPORTED_DEF long BGl_f32vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_23)
{
{ /* Llib/srfi4.scm 625 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_23);} 

}



/* &f32vector-length */
obj_t BGl_z62f32vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3170, obj_t BgL_xz00_3171)
{
{ /* Llib/srfi4.scm 625 */
{ /* Llib/srfi4.scm 625 */
 long BgL_tmpz00_4513;
{ /* Llib/srfi4.scm 625 */
 obj_t BgL_auxz00_4514;
if(
BGL_F32VECTORP(BgL_xz00_3171))
{ /* Llib/srfi4.scm 625 */
BgL_auxz00_4514 = BgL_xz00_3171
; }  else 
{ 
 obj_t BgL_auxz00_4517;
BgL_auxz00_4517 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25826L), BGl_string2905z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_xz00_3171); 
FAILURE(BgL_auxz00_4517,BFALSE,BFALSE);} 
BgL_tmpz00_4513 = 
BGl_f32vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4514); } 
return 
BINT(BgL_tmpz00_4513);} } 

}



/* f64vector-length */
BGL_EXPORTED_DEF long BGl_f64vectorzd2lengthzd2zz__srfi4z00(obj_t BgL_xz00_24)
{
{ /* Llib/srfi4.scm 626 */
return 
BGL_HVECTOR_LENGTH(BgL_xz00_24);} 

}



/* &f64vector-length */
obj_t BGl_z62f64vectorzd2lengthzb0zz__srfi4z00(obj_t BgL_envz00_3172, obj_t BgL_xz00_3173)
{
{ /* Llib/srfi4.scm 626 */
{ /* Llib/srfi4.scm 626 */
 long BgL_tmpz00_4524;
{ /* Llib/srfi4.scm 626 */
 obj_t BgL_auxz00_4525;
if(
BGL_F64VECTORP(BgL_xz00_3173))
{ /* Llib/srfi4.scm 626 */
BgL_auxz00_4525 = BgL_xz00_3173
; }  else 
{ 
 obj_t BgL_auxz00_4528;
BgL_auxz00_4528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(25883L), BGl_string2907z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_xz00_3173); 
FAILURE(BgL_auxz00_4528,BFALSE,BFALSE);} 
BgL_tmpz00_4524 = 
BGl_f64vectorzd2lengthzd2zz__srfi4z00(BgL_auxz00_4525); } 
return 
BINT(BgL_tmpz00_4524);} } 

}



/* s8vector */
BGL_EXPORTED_DEF obj_t BGl_s8vectorz00zz__srfi4z00(obj_t BgL_xz00_25)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4534;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3316z00_4535;
if(
PAIRP(BgL_xz00_25))
{ /* Llib/srfi4.scm 637 */
BgL_test3316z00_4535 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3316z00_4535 = 
NULLP(BgL_xz00_25)
; } 
if(BgL_test3316z00_4535)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4534 = BgL_xz00_25
; }  else 
{ 
 obj_t BgL_auxz00_4539;
BgL_auxz00_4539 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2890z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_25); 
FAILURE(BgL_auxz00_4539,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3s8vectorz31zz__srfi4z00(BgL_auxz00_4534);} } 

}



/* &s8vector */
obj_t BGl_z62s8vectorz62zz__srfi4z00(obj_t BgL_envz00_3174, obj_t BgL_xz00_3175)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_s8vectorz00zz__srfi4z00(BgL_xz00_3175);} 

}



/* u8vector */
BGL_EXPORTED_DEF obj_t BGl_u8vectorz00zz__srfi4z00(obj_t BgL_xz00_26)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4545;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3318z00_4546;
if(
PAIRP(BgL_xz00_26))
{ /* Llib/srfi4.scm 637 */
BgL_test3318z00_4546 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3318z00_4546 = 
NULLP(BgL_xz00_26)
; } 
if(BgL_test3318z00_4546)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4545 = BgL_xz00_26
; }  else 
{ 
 obj_t BgL_auxz00_4550;
BgL_auxz00_4550 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2892z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_26); 
FAILURE(BgL_auxz00_4550,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3u8vectorz31zz__srfi4z00(BgL_auxz00_4545);} } 

}



/* &u8vector */
obj_t BGl_z62u8vectorz62zz__srfi4z00(obj_t BgL_envz00_3176, obj_t BgL_xz00_3177)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_u8vectorz00zz__srfi4z00(BgL_xz00_3177);} 

}



/* s16vector */
BGL_EXPORTED_DEF obj_t BGl_s16vectorz00zz__srfi4z00(obj_t BgL_xz00_27)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4556;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3320z00_4557;
if(
PAIRP(BgL_xz00_27))
{ /* Llib/srfi4.scm 637 */
BgL_test3320z00_4557 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3320z00_4557 = 
NULLP(BgL_xz00_27)
; } 
if(BgL_test3320z00_4557)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4556 = BgL_xz00_27
; }  else 
{ 
 obj_t BgL_auxz00_4561;
BgL_auxz00_4561 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2894z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_27); 
FAILURE(BgL_auxz00_4561,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3s16vectorz31zz__srfi4z00(BgL_auxz00_4556);} } 

}



/* &s16vector */
obj_t BGl_z62s16vectorz62zz__srfi4z00(obj_t BgL_envz00_3178, obj_t BgL_xz00_3179)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_s16vectorz00zz__srfi4z00(BgL_xz00_3179);} 

}



/* u16vector */
BGL_EXPORTED_DEF obj_t BGl_u16vectorz00zz__srfi4z00(obj_t BgL_xz00_28)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4567;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3322z00_4568;
if(
PAIRP(BgL_xz00_28))
{ /* Llib/srfi4.scm 637 */
BgL_test3322z00_4568 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3322z00_4568 = 
NULLP(BgL_xz00_28)
; } 
if(BgL_test3322z00_4568)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4567 = BgL_xz00_28
; }  else 
{ 
 obj_t BgL_auxz00_4572;
BgL_auxz00_4572 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2896z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_28); 
FAILURE(BgL_auxz00_4572,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3u16vectorz31zz__srfi4z00(BgL_auxz00_4567);} } 

}



/* &u16vector */
obj_t BGl_z62u16vectorz62zz__srfi4z00(obj_t BgL_envz00_3180, obj_t BgL_xz00_3181)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_u16vectorz00zz__srfi4z00(BgL_xz00_3181);} 

}



/* s32vector */
BGL_EXPORTED_DEF obj_t BGl_s32vectorz00zz__srfi4z00(obj_t BgL_xz00_29)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4578;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3324z00_4579;
if(
PAIRP(BgL_xz00_29))
{ /* Llib/srfi4.scm 637 */
BgL_test3324z00_4579 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3324z00_4579 = 
NULLP(BgL_xz00_29)
; } 
if(BgL_test3324z00_4579)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4578 = BgL_xz00_29
; }  else 
{ 
 obj_t BgL_auxz00_4583;
BgL_auxz00_4583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2898z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_29); 
FAILURE(BgL_auxz00_4583,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3s32vectorz31zz__srfi4z00(BgL_auxz00_4578);} } 

}



/* &s32vector */
obj_t BGl_z62s32vectorz62zz__srfi4z00(obj_t BgL_envz00_3182, obj_t BgL_xz00_3183)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_s32vectorz00zz__srfi4z00(BgL_xz00_3183);} 

}



/* u32vector */
BGL_EXPORTED_DEF obj_t BGl_u32vectorz00zz__srfi4z00(obj_t BgL_xz00_30)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4589;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3326z00_4590;
if(
PAIRP(BgL_xz00_30))
{ /* Llib/srfi4.scm 637 */
BgL_test3326z00_4590 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3326z00_4590 = 
NULLP(BgL_xz00_30)
; } 
if(BgL_test3326z00_4590)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4589 = BgL_xz00_30
; }  else 
{ 
 obj_t BgL_auxz00_4594;
BgL_auxz00_4594 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2900z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_30); 
FAILURE(BgL_auxz00_4594,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3u32vectorz31zz__srfi4z00(BgL_auxz00_4589);} } 

}



/* &u32vector */
obj_t BGl_z62u32vectorz62zz__srfi4z00(obj_t BgL_envz00_3184, obj_t BgL_xz00_3185)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_u32vectorz00zz__srfi4z00(BgL_xz00_3185);} 

}



/* s64vector */
BGL_EXPORTED_DEF obj_t BGl_s64vectorz00zz__srfi4z00(obj_t BgL_xz00_31)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4600;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3328z00_4601;
if(
PAIRP(BgL_xz00_31))
{ /* Llib/srfi4.scm 637 */
BgL_test3328z00_4601 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3328z00_4601 = 
NULLP(BgL_xz00_31)
; } 
if(BgL_test3328z00_4601)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4600 = BgL_xz00_31
; }  else 
{ 
 obj_t BgL_auxz00_4605;
BgL_auxz00_4605 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2902z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_31); 
FAILURE(BgL_auxz00_4605,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3s64vectorz31zz__srfi4z00(BgL_auxz00_4600);} } 

}



/* &s64vector */
obj_t BGl_z62s64vectorz62zz__srfi4z00(obj_t BgL_envz00_3186, obj_t BgL_xz00_3187)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_s64vectorz00zz__srfi4z00(BgL_xz00_3187);} 

}



/* u64vector */
BGL_EXPORTED_DEF obj_t BGl_u64vectorz00zz__srfi4z00(obj_t BgL_xz00_32)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4611;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3330z00_4612;
if(
PAIRP(BgL_xz00_32))
{ /* Llib/srfi4.scm 637 */
BgL_test3330z00_4612 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3330z00_4612 = 
NULLP(BgL_xz00_32)
; } 
if(BgL_test3330z00_4612)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4611 = BgL_xz00_32
; }  else 
{ 
 obj_t BgL_auxz00_4616;
BgL_auxz00_4616 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2904z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_32); 
FAILURE(BgL_auxz00_4616,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3u64vectorz31zz__srfi4z00(BgL_auxz00_4611);} } 

}



/* &u64vector */
obj_t BGl_z62u64vectorz62zz__srfi4z00(obj_t BgL_envz00_3188, obj_t BgL_xz00_3189)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_u64vectorz00zz__srfi4z00(BgL_xz00_3189);} 

}



/* f32vector */
BGL_EXPORTED_DEF obj_t BGl_f32vectorz00zz__srfi4z00(obj_t BgL_xz00_33)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4622;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3332z00_4623;
if(
PAIRP(BgL_xz00_33))
{ /* Llib/srfi4.scm 637 */
BgL_test3332z00_4623 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3332z00_4623 = 
NULLP(BgL_xz00_33)
; } 
if(BgL_test3332z00_4623)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4622 = BgL_xz00_33
; }  else 
{ 
 obj_t BgL_auxz00_4627;
BgL_auxz00_4627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2906z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_33); 
FAILURE(BgL_auxz00_4627,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3f32vectorz31zz__srfi4z00(BgL_auxz00_4622);} } 

}



/* &f32vector */
obj_t BGl_z62f32vectorz62zz__srfi4z00(obj_t BgL_envz00_3190, obj_t BgL_xz00_3191)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_f32vectorz00zz__srfi4z00(BgL_xz00_3191);} 

}



/* f64vector */
BGL_EXPORTED_DEF obj_t BGl_f64vectorz00zz__srfi4z00(obj_t BgL_xz00_34)
{
{ /* Llib/srfi4.scm 637 */
{ /* Llib/srfi4.scm 637 */
 obj_t BgL_auxz00_4633;
{ /* Llib/srfi4.scm 637 */
 bool_t BgL_test3334z00_4634;
if(
PAIRP(BgL_xz00_34))
{ /* Llib/srfi4.scm 637 */
BgL_test3334z00_4634 = ((bool_t)1)
; }  else 
{ /* Llib/srfi4.scm 637 */
BgL_test3334z00_4634 = 
NULLP(BgL_xz00_34)
; } 
if(BgL_test3334z00_4634)
{ /* Llib/srfi4.scm 637 */
BgL_auxz00_4633 = BgL_xz00_34
; }  else 
{ 
 obj_t BgL_auxz00_4638;
BgL_auxz00_4638 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(26350L), BGl_string2908z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_34); 
FAILURE(BgL_auxz00_4638,BFALSE,BFALSE);} } 
return 
BGl_listzd2ze3f64vectorz31zz__srfi4z00(BgL_auxz00_4633);} } 

}



/* &f64vector */
obj_t BGl_z62f64vectorz62zz__srfi4z00(obj_t BgL_envz00_3192, obj_t BgL_xz00_3193)
{
{ /* Llib/srfi4.scm 637 */
return 
BGl_f64vectorz00zz__srfi4z00(BgL_xz00_3193);} 

}



/* _make-s8vector */
obj_t BGl__makezd2s8vectorzd2zz__srfi4z00(obj_t BgL_env1249z00_38, obj_t BgL_opt1248z00_37)
{
{ /* Llib/srfi4.scm 661 */
{ /* Llib/srfi4.scm 661 */
 obj_t BgL_g1250z00_1395;
BgL_g1250z00_1395 = 
VECTOR_REF(BgL_opt1248z00_37,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1248z00_37)) { case 1L : 

{ /* Llib/srfi4.scm 661 */

{ /* Llib/srfi4.scm 661 */
 long BgL_auxz00_4645;
{ /* Llib/srfi4.scm 661 */
 obj_t BgL_tmpz00_4646;
if(
INTEGERP(BgL_g1250z00_1395))
{ /* Llib/srfi4.scm 661 */
BgL_tmpz00_4646 = BgL_g1250z00_1395
; }  else 
{ 
 obj_t BgL_auxz00_4649;
BgL_auxz00_4649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27216L), BGl_string2913z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1250z00_1395); 
FAILURE(BgL_auxz00_4649,BFALSE,BFALSE);} 
BgL_auxz00_4645 = 
(long)CINT(BgL_tmpz00_4646); } 
return 
BGl_makezd2s8vectorzd2zz__srfi4z00(BgL_auxz00_4645, (int8_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 661 */
 obj_t BgL_initz00_1399;
BgL_initz00_1399 = 
VECTOR_REF(BgL_opt1248z00_37,1L); 
{ /* Llib/srfi4.scm 661 */

{ /* Llib/srfi4.scm 661 */
 int8_t BgL_auxz00_4665; long BgL_auxz00_4656;
{ /* Llib/srfi4.scm 661 */
 obj_t BgL_tmpz00_4666;
if(
BGL_INT8P(BgL_initz00_1399))
{ /* Llib/srfi4.scm 661 */
BgL_tmpz00_4666 = BgL_initz00_1399
; }  else 
{ 
 obj_t BgL_auxz00_4669;
BgL_auxz00_4669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27216L), BGl_string2913z00zz__srfi4z00, BGl_string2915z00zz__srfi4z00, BgL_initz00_1399); 
FAILURE(BgL_auxz00_4669,BFALSE,BFALSE);} 
BgL_auxz00_4665 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_4666); } 
{ /* Llib/srfi4.scm 661 */
 obj_t BgL_tmpz00_4657;
if(
INTEGERP(BgL_g1250z00_1395))
{ /* Llib/srfi4.scm 661 */
BgL_tmpz00_4657 = BgL_g1250z00_1395
; }  else 
{ 
 obj_t BgL_auxz00_4660;
BgL_auxz00_4660 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27216L), BGl_string2913z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1250z00_1395); 
FAILURE(BgL_auxz00_4660,BFALSE,BFALSE);} 
BgL_auxz00_4656 = 
(long)CINT(BgL_tmpz00_4657); } 
return 
BGl_makezd2s8vectorzd2zz__srfi4z00(BgL_auxz00_4656, BgL_auxz00_4665);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2910z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1248z00_37)));} } } } 

}



/* make-s8vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2s8vectorzd2zz__srfi4z00(long BgL_lenz00_35, int8_t BgL_initz00_36)
{
{ /* Llib/srfi4.scm 661 */
{ /* Llib/srfi4.scm 661 */
 obj_t BgL_vz00_1401;
{ /* Llib/srfi4.scm 661 */
 int32_t BgL_tmpz00_4680;
BgL_tmpz00_4680 = 
(int32_t)(BgL_lenz00_35); 
BgL_vz00_1401 = 
BGL_ALLOC_S8VECTOR(BgL_tmpz00_4680); } 
{ 
 long BgL_iz00_2483;
BgL_iz00_2483 = 0L; 
BgL_loopz00_2482:
if(
(BgL_iz00_2483<BgL_lenz00_35))
{ /* Llib/srfi4.scm 661 */
{ /* Llib/srfi4.scm 661 */
 long BgL_l2064z00_3240;
BgL_l2064z00_3240 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1401); 
if(
BOUND_CHECK(BgL_iz00_2483, BgL_l2064z00_3240))
{ /* Llib/srfi4.scm 661 */
BGL_S8VSET(BgL_vz00_1401, BgL_iz00_2483, BgL_initz00_36); }  else 
{ 
 obj_t BgL_auxz00_4689;
BgL_auxz00_4689 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27216L), BGl_string2916z00zz__srfi4z00, BgL_vz00_1401, 
(int)(BgL_l2064z00_3240), 
(int)(BgL_iz00_2483)); 
FAILURE(BgL_auxz00_4689,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_4695;
BgL_iz00_4695 = 
(BgL_iz00_2483+1L); 
BgL_iz00_2483 = BgL_iz00_4695; 
goto BgL_loopz00_2482;} }  else 
{ /* Llib/srfi4.scm 661 */((bool_t)0); } } 
return BgL_vz00_1401;} } 

}



/* _make-u8vector */
obj_t BGl__makezd2u8vectorzd2zz__srfi4z00(obj_t BgL_env1254z00_42, obj_t BgL_opt1253z00_41)
{
{ /* Llib/srfi4.scm 662 */
{ /* Llib/srfi4.scm 662 */
 obj_t BgL_g1255z00_1408;
BgL_g1255z00_1408 = 
VECTOR_REF(BgL_opt1253z00_41,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1253z00_41)) { case 1L : 

{ /* Llib/srfi4.scm 662 */

{ /* Llib/srfi4.scm 662 */
 long BgL_auxz00_4698;
{ /* Llib/srfi4.scm 662 */
 obj_t BgL_tmpz00_4699;
if(
INTEGERP(BgL_g1255z00_1408))
{ /* Llib/srfi4.scm 662 */
BgL_tmpz00_4699 = BgL_g1255z00_1408
; }  else 
{ 
 obj_t BgL_auxz00_4702;
BgL_auxz00_4702 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27248L), BGl_string2919z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1255z00_1408); 
FAILURE(BgL_auxz00_4702,BFALSE,BFALSE);} 
BgL_auxz00_4698 = 
(long)CINT(BgL_tmpz00_4699); } 
return 
BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_auxz00_4698, (uint8_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 662 */
 obj_t BgL_initz00_1412;
BgL_initz00_1412 = 
VECTOR_REF(BgL_opt1253z00_41,1L); 
{ /* Llib/srfi4.scm 662 */

{ /* Llib/srfi4.scm 662 */
 uint8_t BgL_auxz00_4718; long BgL_auxz00_4709;
{ /* Llib/srfi4.scm 662 */
 obj_t BgL_tmpz00_4719;
if(
BGL_UINT8P(BgL_initz00_1412))
{ /* Llib/srfi4.scm 662 */
BgL_tmpz00_4719 = BgL_initz00_1412
; }  else 
{ 
 obj_t BgL_auxz00_4722;
BgL_auxz00_4722 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27248L), BGl_string2919z00zz__srfi4z00, BGl_string2920z00zz__srfi4z00, BgL_initz00_1412); 
FAILURE(BgL_auxz00_4722,BFALSE,BFALSE);} 
BgL_auxz00_4718 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_4719); } 
{ /* Llib/srfi4.scm 662 */
 obj_t BgL_tmpz00_4710;
if(
INTEGERP(BgL_g1255z00_1408))
{ /* Llib/srfi4.scm 662 */
BgL_tmpz00_4710 = BgL_g1255z00_1408
; }  else 
{ 
 obj_t BgL_auxz00_4713;
BgL_auxz00_4713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27248L), BGl_string2919z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1255z00_1408); 
FAILURE(BgL_auxz00_4713,BFALSE,BFALSE);} 
BgL_auxz00_4709 = 
(long)CINT(BgL_tmpz00_4710); } 
return 
BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_auxz00_4709, BgL_auxz00_4718);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2917z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1253z00_41)));} } } } 

}



/* make-u8vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long BgL_lenz00_39, uint8_t BgL_initz00_40)
{
{ /* Llib/srfi4.scm 662 */
{ /* Llib/srfi4.scm 662 */
 obj_t BgL_vz00_1414;
{ /* Llib/srfi4.scm 662 */
 int32_t BgL_tmpz00_4733;
BgL_tmpz00_4733 = 
(int32_t)(BgL_lenz00_39); 
BgL_vz00_1414 = 
BGL_ALLOC_U8VECTOR(BgL_tmpz00_4733); } 
{ 
 long BgL_iz00_2493;
BgL_iz00_2493 = 0L; 
BgL_loopz00_2492:
if(
(BgL_iz00_2493<BgL_lenz00_39))
{ /* Llib/srfi4.scm 662 */
{ /* Llib/srfi4.scm 662 */
 long BgL_l2068z00_3244;
BgL_l2068z00_3244 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1414); 
if(
BOUND_CHECK(BgL_iz00_2493, BgL_l2068z00_3244))
{ /* Llib/srfi4.scm 662 */
BGL_U8VSET(BgL_vz00_1414, BgL_iz00_2493, BgL_initz00_40); }  else 
{ 
 obj_t BgL_auxz00_4742;
BgL_auxz00_4742 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27248L), BGl_string2921z00zz__srfi4z00, BgL_vz00_1414, 
(int)(BgL_l2068z00_3244), 
(int)(BgL_iz00_2493)); 
FAILURE(BgL_auxz00_4742,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_4748;
BgL_iz00_4748 = 
(BgL_iz00_2493+1L); 
BgL_iz00_2493 = BgL_iz00_4748; 
goto BgL_loopz00_2492;} }  else 
{ /* Llib/srfi4.scm 662 */((bool_t)0); } } 
return BgL_vz00_1414;} } 

}



/* _make-s16vector */
obj_t BGl__makezd2s16vectorzd2zz__srfi4z00(obj_t BgL_env1259z00_46, obj_t BgL_opt1258z00_45)
{
{ /* Llib/srfi4.scm 663 */
{ /* Llib/srfi4.scm 663 */
 obj_t BgL_g1260z00_1421;
BgL_g1260z00_1421 = 
VECTOR_REF(BgL_opt1258z00_45,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1258z00_45)) { case 1L : 

{ /* Llib/srfi4.scm 663 */

{ /* Llib/srfi4.scm 663 */
 long BgL_auxz00_4751;
{ /* Llib/srfi4.scm 663 */
 obj_t BgL_tmpz00_4752;
if(
INTEGERP(BgL_g1260z00_1421))
{ /* Llib/srfi4.scm 663 */
BgL_tmpz00_4752 = BgL_g1260z00_1421
; }  else 
{ 
 obj_t BgL_auxz00_4755;
BgL_auxz00_4755 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27280L), BGl_string2924z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1260z00_1421); 
FAILURE(BgL_auxz00_4755,BFALSE,BFALSE);} 
BgL_auxz00_4751 = 
(long)CINT(BgL_tmpz00_4752); } 
return 
BGl_makezd2s16vectorzd2zz__srfi4z00(BgL_auxz00_4751, (int16_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 663 */
 obj_t BgL_initz00_1425;
BgL_initz00_1425 = 
VECTOR_REF(BgL_opt1258z00_45,1L); 
{ /* Llib/srfi4.scm 663 */

{ /* Llib/srfi4.scm 663 */
 int16_t BgL_auxz00_4771; long BgL_auxz00_4762;
{ /* Llib/srfi4.scm 663 */
 obj_t BgL_tmpz00_4772;
if(
BGL_INT16P(BgL_initz00_1425))
{ /* Llib/srfi4.scm 663 */
BgL_tmpz00_4772 = BgL_initz00_1425
; }  else 
{ 
 obj_t BgL_auxz00_4775;
BgL_auxz00_4775 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27280L), BGl_string2924z00zz__srfi4z00, BGl_string2925z00zz__srfi4z00, BgL_initz00_1425); 
FAILURE(BgL_auxz00_4775,BFALSE,BFALSE);} 
BgL_auxz00_4771 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_4772); } 
{ /* Llib/srfi4.scm 663 */
 obj_t BgL_tmpz00_4763;
if(
INTEGERP(BgL_g1260z00_1421))
{ /* Llib/srfi4.scm 663 */
BgL_tmpz00_4763 = BgL_g1260z00_1421
; }  else 
{ 
 obj_t BgL_auxz00_4766;
BgL_auxz00_4766 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27280L), BGl_string2924z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1260z00_1421); 
FAILURE(BgL_auxz00_4766,BFALSE,BFALSE);} 
BgL_auxz00_4762 = 
(long)CINT(BgL_tmpz00_4763); } 
return 
BGl_makezd2s16vectorzd2zz__srfi4z00(BgL_auxz00_4762, BgL_auxz00_4771);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2922z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1258z00_45)));} } } } 

}



/* make-s16vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2s16vectorzd2zz__srfi4z00(long BgL_lenz00_43, int16_t BgL_initz00_44)
{
{ /* Llib/srfi4.scm 663 */
{ /* Llib/srfi4.scm 663 */
 obj_t BgL_vz00_1427;
{ /* Llib/srfi4.scm 663 */
 int32_t BgL_tmpz00_4786;
BgL_tmpz00_4786 = 
(int32_t)(BgL_lenz00_43); 
BgL_vz00_1427 = 
BGL_ALLOC_S16VECTOR(BgL_tmpz00_4786); } 
{ 
 long BgL_iz00_2503;
BgL_iz00_2503 = 0L; 
BgL_loopz00_2502:
if(
(BgL_iz00_2503<BgL_lenz00_43))
{ /* Llib/srfi4.scm 663 */
{ /* Llib/srfi4.scm 663 */
 long BgL_l2072z00_3248;
BgL_l2072z00_3248 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1427); 
if(
BOUND_CHECK(BgL_iz00_2503, BgL_l2072z00_3248))
{ /* Llib/srfi4.scm 663 */
BGL_S16VSET(BgL_vz00_1427, BgL_iz00_2503, BgL_initz00_44); }  else 
{ 
 obj_t BgL_auxz00_4795;
BgL_auxz00_4795 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27280L), BGl_string2926z00zz__srfi4z00, BgL_vz00_1427, 
(int)(BgL_l2072z00_3248), 
(int)(BgL_iz00_2503)); 
FAILURE(BgL_auxz00_4795,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_4801;
BgL_iz00_4801 = 
(BgL_iz00_2503+1L); 
BgL_iz00_2503 = BgL_iz00_4801; 
goto BgL_loopz00_2502;} }  else 
{ /* Llib/srfi4.scm 663 */((bool_t)0); } } 
return BgL_vz00_1427;} } 

}



/* _make-u16vector */
obj_t BGl__makezd2u16vectorzd2zz__srfi4z00(obj_t BgL_env1264z00_50, obj_t BgL_opt1263z00_49)
{
{ /* Llib/srfi4.scm 664 */
{ /* Llib/srfi4.scm 664 */
 obj_t BgL_g1265z00_1434;
BgL_g1265z00_1434 = 
VECTOR_REF(BgL_opt1263z00_49,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1263z00_49)) { case 1L : 

{ /* Llib/srfi4.scm 664 */

{ /* Llib/srfi4.scm 664 */
 long BgL_auxz00_4804;
{ /* Llib/srfi4.scm 664 */
 obj_t BgL_tmpz00_4805;
if(
INTEGERP(BgL_g1265z00_1434))
{ /* Llib/srfi4.scm 664 */
BgL_tmpz00_4805 = BgL_g1265z00_1434
; }  else 
{ 
 obj_t BgL_auxz00_4808;
BgL_auxz00_4808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27314L), BGl_string2929z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1265z00_1434); 
FAILURE(BgL_auxz00_4808,BFALSE,BFALSE);} 
BgL_auxz00_4804 = 
(long)CINT(BgL_tmpz00_4805); } 
return 
BGl_makezd2u16vectorzd2zz__srfi4z00(BgL_auxz00_4804, (uint16_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 664 */
 obj_t BgL_initz00_1438;
BgL_initz00_1438 = 
VECTOR_REF(BgL_opt1263z00_49,1L); 
{ /* Llib/srfi4.scm 664 */

{ /* Llib/srfi4.scm 664 */
 uint16_t BgL_auxz00_4824; long BgL_auxz00_4815;
{ /* Llib/srfi4.scm 664 */
 obj_t BgL_tmpz00_4825;
if(
BGL_UINT16P(BgL_initz00_1438))
{ /* Llib/srfi4.scm 664 */
BgL_tmpz00_4825 = BgL_initz00_1438
; }  else 
{ 
 obj_t BgL_auxz00_4828;
BgL_auxz00_4828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27314L), BGl_string2929z00zz__srfi4z00, BGl_string2930z00zz__srfi4z00, BgL_initz00_1438); 
FAILURE(BgL_auxz00_4828,BFALSE,BFALSE);} 
BgL_auxz00_4824 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_4825); } 
{ /* Llib/srfi4.scm 664 */
 obj_t BgL_tmpz00_4816;
if(
INTEGERP(BgL_g1265z00_1434))
{ /* Llib/srfi4.scm 664 */
BgL_tmpz00_4816 = BgL_g1265z00_1434
; }  else 
{ 
 obj_t BgL_auxz00_4819;
BgL_auxz00_4819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27314L), BGl_string2929z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1265z00_1434); 
FAILURE(BgL_auxz00_4819,BFALSE,BFALSE);} 
BgL_auxz00_4815 = 
(long)CINT(BgL_tmpz00_4816); } 
return 
BGl_makezd2u16vectorzd2zz__srfi4z00(BgL_auxz00_4815, BgL_auxz00_4824);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2927z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1263z00_49)));} } } } 

}



/* make-u16vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2u16vectorzd2zz__srfi4z00(long BgL_lenz00_47, uint16_t BgL_initz00_48)
{
{ /* Llib/srfi4.scm 664 */
{ /* Llib/srfi4.scm 664 */
 obj_t BgL_vz00_1440;
{ /* Llib/srfi4.scm 664 */
 int32_t BgL_tmpz00_4839;
BgL_tmpz00_4839 = 
(int32_t)(BgL_lenz00_47); 
BgL_vz00_1440 = 
BGL_ALLOC_U16VECTOR(BgL_tmpz00_4839); } 
{ 
 long BgL_iz00_2513;
BgL_iz00_2513 = 0L; 
BgL_loopz00_2512:
if(
(BgL_iz00_2513<BgL_lenz00_47))
{ /* Llib/srfi4.scm 664 */
{ /* Llib/srfi4.scm 664 */
 long BgL_l2076z00_3252;
BgL_l2076z00_3252 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1440); 
if(
BOUND_CHECK(BgL_iz00_2513, BgL_l2076z00_3252))
{ /* Llib/srfi4.scm 664 */
BGL_U16VSET(BgL_vz00_1440, BgL_iz00_2513, BgL_initz00_48); }  else 
{ 
 obj_t BgL_auxz00_4848;
BgL_auxz00_4848 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27314L), BGl_string2931z00zz__srfi4z00, BgL_vz00_1440, 
(int)(BgL_l2076z00_3252), 
(int)(BgL_iz00_2513)); 
FAILURE(BgL_auxz00_4848,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_4854;
BgL_iz00_4854 = 
(BgL_iz00_2513+1L); 
BgL_iz00_2513 = BgL_iz00_4854; 
goto BgL_loopz00_2512;} }  else 
{ /* Llib/srfi4.scm 664 */((bool_t)0); } } 
return BgL_vz00_1440;} } 

}



/* _make-s32vector */
obj_t BGl__makezd2s32vectorzd2zz__srfi4z00(obj_t BgL_env1269z00_54, obj_t BgL_opt1268z00_53)
{
{ /* Llib/srfi4.scm 665 */
{ /* Llib/srfi4.scm 665 */
 obj_t BgL_g1270z00_1447;
BgL_g1270z00_1447 = 
VECTOR_REF(BgL_opt1268z00_53,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1268z00_53)) { case 1L : 

{ /* Llib/srfi4.scm 665 */

{ /* Llib/srfi4.scm 665 */
 long BgL_auxz00_4857;
{ /* Llib/srfi4.scm 665 */
 obj_t BgL_tmpz00_4858;
if(
INTEGERP(BgL_g1270z00_1447))
{ /* Llib/srfi4.scm 665 */
BgL_tmpz00_4858 = BgL_g1270z00_1447
; }  else 
{ 
 obj_t BgL_auxz00_4861;
BgL_auxz00_4861 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27348L), BGl_string2934z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1270z00_1447); 
FAILURE(BgL_auxz00_4861,BFALSE,BFALSE);} 
BgL_auxz00_4857 = 
(long)CINT(BgL_tmpz00_4858); } 
return 
BGl_makezd2s32vectorzd2zz__srfi4z00(BgL_auxz00_4857, (int32_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 665 */
 obj_t BgL_initz00_1451;
BgL_initz00_1451 = 
VECTOR_REF(BgL_opt1268z00_53,1L); 
{ /* Llib/srfi4.scm 665 */

{ /* Llib/srfi4.scm 665 */
 int32_t BgL_auxz00_4877; long BgL_auxz00_4868;
{ /* Llib/srfi4.scm 665 */
 obj_t BgL_tmpz00_4878;
if(
BGL_INT32P(BgL_initz00_1451))
{ /* Llib/srfi4.scm 665 */
BgL_tmpz00_4878 = BgL_initz00_1451
; }  else 
{ 
 obj_t BgL_auxz00_4881;
BgL_auxz00_4881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27348L), BGl_string2934z00zz__srfi4z00, BGl_string2935z00zz__srfi4z00, BgL_initz00_1451); 
FAILURE(BgL_auxz00_4881,BFALSE,BFALSE);} 
BgL_auxz00_4877 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_4878); } 
{ /* Llib/srfi4.scm 665 */
 obj_t BgL_tmpz00_4869;
if(
INTEGERP(BgL_g1270z00_1447))
{ /* Llib/srfi4.scm 665 */
BgL_tmpz00_4869 = BgL_g1270z00_1447
; }  else 
{ 
 obj_t BgL_auxz00_4872;
BgL_auxz00_4872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27348L), BGl_string2934z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1270z00_1447); 
FAILURE(BgL_auxz00_4872,BFALSE,BFALSE);} 
BgL_auxz00_4868 = 
(long)CINT(BgL_tmpz00_4869); } 
return 
BGl_makezd2s32vectorzd2zz__srfi4z00(BgL_auxz00_4868, BgL_auxz00_4877);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2932z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1268z00_53)));} } } } 

}



/* make-s32vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2s32vectorzd2zz__srfi4z00(long BgL_lenz00_51, int32_t BgL_initz00_52)
{
{ /* Llib/srfi4.scm 665 */
{ /* Llib/srfi4.scm 665 */
 obj_t BgL_vz00_1453;
{ /* Llib/srfi4.scm 665 */
 int32_t BgL_tmpz00_4892;
BgL_tmpz00_4892 = 
(int32_t)(BgL_lenz00_51); 
BgL_vz00_1453 = 
BGL_ALLOC_S32VECTOR(BgL_tmpz00_4892); } 
{ 
 long BgL_iz00_2523;
BgL_iz00_2523 = 0L; 
BgL_loopz00_2522:
if(
(BgL_iz00_2523<BgL_lenz00_51))
{ /* Llib/srfi4.scm 665 */
{ /* Llib/srfi4.scm 665 */
 long BgL_l2080z00_3256;
BgL_l2080z00_3256 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1453); 
if(
BOUND_CHECK(BgL_iz00_2523, BgL_l2080z00_3256))
{ /* Llib/srfi4.scm 665 */
BGL_S32VSET(BgL_vz00_1453, BgL_iz00_2523, BgL_initz00_52); }  else 
{ 
 obj_t BgL_auxz00_4901;
BgL_auxz00_4901 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27348L), BGl_string2936z00zz__srfi4z00, BgL_vz00_1453, 
(int)(BgL_l2080z00_3256), 
(int)(BgL_iz00_2523)); 
FAILURE(BgL_auxz00_4901,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_4907;
BgL_iz00_4907 = 
(BgL_iz00_2523+1L); 
BgL_iz00_2523 = BgL_iz00_4907; 
goto BgL_loopz00_2522;} }  else 
{ /* Llib/srfi4.scm 665 */((bool_t)0); } } 
return BgL_vz00_1453;} } 

}



/* _make-u32vector */
obj_t BGl__makezd2u32vectorzd2zz__srfi4z00(obj_t BgL_env1274z00_58, obj_t BgL_opt1273z00_57)
{
{ /* Llib/srfi4.scm 666 */
{ /* Llib/srfi4.scm 666 */
 obj_t BgL_g1275z00_1460;
BgL_g1275z00_1460 = 
VECTOR_REF(BgL_opt1273z00_57,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1273z00_57)) { case 1L : 

{ /* Llib/srfi4.scm 666 */

{ /* Llib/srfi4.scm 666 */
 long BgL_auxz00_4910;
{ /* Llib/srfi4.scm 666 */
 obj_t BgL_tmpz00_4911;
if(
INTEGERP(BgL_g1275z00_1460))
{ /* Llib/srfi4.scm 666 */
BgL_tmpz00_4911 = BgL_g1275z00_1460
; }  else 
{ 
 obj_t BgL_auxz00_4914;
BgL_auxz00_4914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27382L), BGl_string2939z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1275z00_1460); 
FAILURE(BgL_auxz00_4914,BFALSE,BFALSE);} 
BgL_auxz00_4910 = 
(long)CINT(BgL_tmpz00_4911); } 
return 
BGl_makezd2u32vectorzd2zz__srfi4z00(BgL_auxz00_4910, (uint32_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 666 */
 obj_t BgL_initz00_1464;
BgL_initz00_1464 = 
VECTOR_REF(BgL_opt1273z00_57,1L); 
{ /* Llib/srfi4.scm 666 */

{ /* Llib/srfi4.scm 666 */
 uint32_t BgL_auxz00_4930; long BgL_auxz00_4921;
{ /* Llib/srfi4.scm 666 */
 obj_t BgL_tmpz00_4931;
if(
BGL_UINT32P(BgL_initz00_1464))
{ /* Llib/srfi4.scm 666 */
BgL_tmpz00_4931 = BgL_initz00_1464
; }  else 
{ 
 obj_t BgL_auxz00_4934;
BgL_auxz00_4934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27382L), BGl_string2939z00zz__srfi4z00, BGl_string2940z00zz__srfi4z00, BgL_initz00_1464); 
FAILURE(BgL_auxz00_4934,BFALSE,BFALSE);} 
BgL_auxz00_4930 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_4931); } 
{ /* Llib/srfi4.scm 666 */
 obj_t BgL_tmpz00_4922;
if(
INTEGERP(BgL_g1275z00_1460))
{ /* Llib/srfi4.scm 666 */
BgL_tmpz00_4922 = BgL_g1275z00_1460
; }  else 
{ 
 obj_t BgL_auxz00_4925;
BgL_auxz00_4925 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27382L), BGl_string2939z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1275z00_1460); 
FAILURE(BgL_auxz00_4925,BFALSE,BFALSE);} 
BgL_auxz00_4921 = 
(long)CINT(BgL_tmpz00_4922); } 
return 
BGl_makezd2u32vectorzd2zz__srfi4z00(BgL_auxz00_4921, BgL_auxz00_4930);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2937z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1273z00_57)));} } } } 

}



/* make-u32vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long BgL_lenz00_55, uint32_t BgL_initz00_56)
{
{ /* Llib/srfi4.scm 666 */
{ /* Llib/srfi4.scm 666 */
 obj_t BgL_vz00_1466;
{ /* Llib/srfi4.scm 666 */
 int32_t BgL_tmpz00_4945;
BgL_tmpz00_4945 = 
(int32_t)(BgL_lenz00_55); 
BgL_vz00_1466 = 
BGL_ALLOC_U32VECTOR(BgL_tmpz00_4945); } 
{ 
 long BgL_iz00_2533;
BgL_iz00_2533 = 0L; 
BgL_loopz00_2532:
if(
(BgL_iz00_2533<BgL_lenz00_55))
{ /* Llib/srfi4.scm 666 */
{ /* Llib/srfi4.scm 666 */
 long BgL_l2084z00_3260;
BgL_l2084z00_3260 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1466); 
if(
BOUND_CHECK(BgL_iz00_2533, BgL_l2084z00_3260))
{ /* Llib/srfi4.scm 666 */
BGL_U32VSET(BgL_vz00_1466, BgL_iz00_2533, BgL_initz00_56); }  else 
{ 
 obj_t BgL_auxz00_4954;
BgL_auxz00_4954 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27382L), BGl_string2941z00zz__srfi4z00, BgL_vz00_1466, 
(int)(BgL_l2084z00_3260), 
(int)(BgL_iz00_2533)); 
FAILURE(BgL_auxz00_4954,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_4960;
BgL_iz00_4960 = 
(BgL_iz00_2533+1L); 
BgL_iz00_2533 = BgL_iz00_4960; 
goto BgL_loopz00_2532;} }  else 
{ /* Llib/srfi4.scm 666 */((bool_t)0); } } 
return BgL_vz00_1466;} } 

}



/* _make-s64vector */
obj_t BGl__makezd2s64vectorzd2zz__srfi4z00(obj_t BgL_env1279z00_62, obj_t BgL_opt1278z00_61)
{
{ /* Llib/srfi4.scm 667 */
{ /* Llib/srfi4.scm 667 */
 obj_t BgL_g1280z00_1473;
BgL_g1280z00_1473 = 
VECTOR_REF(BgL_opt1278z00_61,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1278z00_61)) { case 1L : 

{ /* Llib/srfi4.scm 667 */

{ /* Llib/srfi4.scm 667 */
 long BgL_auxz00_4963;
{ /* Llib/srfi4.scm 667 */
 obj_t BgL_tmpz00_4964;
if(
INTEGERP(BgL_g1280z00_1473))
{ /* Llib/srfi4.scm 667 */
BgL_tmpz00_4964 = BgL_g1280z00_1473
; }  else 
{ 
 obj_t BgL_auxz00_4967;
BgL_auxz00_4967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27416L), BGl_string2944z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1280z00_1473); 
FAILURE(BgL_auxz00_4967,BFALSE,BFALSE);} 
BgL_auxz00_4963 = 
(long)CINT(BgL_tmpz00_4964); } 
return 
BGl_makezd2s64vectorzd2zz__srfi4z00(BgL_auxz00_4963, (int64_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 667 */
 obj_t BgL_initz00_1477;
BgL_initz00_1477 = 
VECTOR_REF(BgL_opt1278z00_61,1L); 
{ /* Llib/srfi4.scm 667 */

{ /* Llib/srfi4.scm 667 */
 int64_t BgL_auxz00_4983; long BgL_auxz00_4974;
{ /* Llib/srfi4.scm 667 */
 obj_t BgL_tmpz00_4984;
if(
BGL_INT64P(BgL_initz00_1477))
{ /* Llib/srfi4.scm 667 */
BgL_tmpz00_4984 = BgL_initz00_1477
; }  else 
{ 
 obj_t BgL_auxz00_4987;
BgL_auxz00_4987 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27416L), BGl_string2944z00zz__srfi4z00, BGl_string2945z00zz__srfi4z00, BgL_initz00_1477); 
FAILURE(BgL_auxz00_4987,BFALSE,BFALSE);} 
BgL_auxz00_4983 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_4984); } 
{ /* Llib/srfi4.scm 667 */
 obj_t BgL_tmpz00_4975;
if(
INTEGERP(BgL_g1280z00_1473))
{ /* Llib/srfi4.scm 667 */
BgL_tmpz00_4975 = BgL_g1280z00_1473
; }  else 
{ 
 obj_t BgL_auxz00_4978;
BgL_auxz00_4978 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27416L), BGl_string2944z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1280z00_1473); 
FAILURE(BgL_auxz00_4978,BFALSE,BFALSE);} 
BgL_auxz00_4974 = 
(long)CINT(BgL_tmpz00_4975); } 
return 
BGl_makezd2s64vectorzd2zz__srfi4z00(BgL_auxz00_4974, BgL_auxz00_4983);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2942z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1278z00_61)));} } } } 

}



/* make-s64vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2s64vectorzd2zz__srfi4z00(long BgL_lenz00_59, int64_t BgL_initz00_60)
{
{ /* Llib/srfi4.scm 667 */
{ /* Llib/srfi4.scm 667 */
 obj_t BgL_vz00_1479;
{ /* Llib/srfi4.scm 667 */
 int32_t BgL_tmpz00_4998;
BgL_tmpz00_4998 = 
(int32_t)(BgL_lenz00_59); 
BgL_vz00_1479 = 
BGL_ALLOC_S64VECTOR(BgL_tmpz00_4998); } 
{ 
 long BgL_iz00_2543;
BgL_iz00_2543 = 0L; 
BgL_loopz00_2542:
if(
(BgL_iz00_2543<BgL_lenz00_59))
{ /* Llib/srfi4.scm 667 */
{ /* Llib/srfi4.scm 667 */
 long BgL_l2088z00_3264;
BgL_l2088z00_3264 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1479); 
if(
BOUND_CHECK(BgL_iz00_2543, BgL_l2088z00_3264))
{ /* Llib/srfi4.scm 667 */
BGL_S64VSET(BgL_vz00_1479, BgL_iz00_2543, BgL_initz00_60); }  else 
{ 
 obj_t BgL_auxz00_5007;
BgL_auxz00_5007 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27416L), BGl_string2946z00zz__srfi4z00, BgL_vz00_1479, 
(int)(BgL_l2088z00_3264), 
(int)(BgL_iz00_2543)); 
FAILURE(BgL_auxz00_5007,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_5013;
BgL_iz00_5013 = 
(BgL_iz00_2543+1L); 
BgL_iz00_2543 = BgL_iz00_5013; 
goto BgL_loopz00_2542;} }  else 
{ /* Llib/srfi4.scm 667 */((bool_t)0); } } 
return BgL_vz00_1479;} } 

}



/* _make-u64vector */
obj_t BGl__makezd2u64vectorzd2zz__srfi4z00(obj_t BgL_env1284z00_66, obj_t BgL_opt1283z00_65)
{
{ /* Llib/srfi4.scm 668 */
{ /* Llib/srfi4.scm 668 */
 obj_t BgL_g1285z00_1486;
BgL_g1285z00_1486 = 
VECTOR_REF(BgL_opt1283z00_65,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1283z00_65)) { case 1L : 

{ /* Llib/srfi4.scm 668 */

{ /* Llib/srfi4.scm 668 */
 long BgL_auxz00_5016;
{ /* Llib/srfi4.scm 668 */
 obj_t BgL_tmpz00_5017;
if(
INTEGERP(BgL_g1285z00_1486))
{ /* Llib/srfi4.scm 668 */
BgL_tmpz00_5017 = BgL_g1285z00_1486
; }  else 
{ 
 obj_t BgL_auxz00_5020;
BgL_auxz00_5020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27450L), BGl_string2949z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1285z00_1486); 
FAILURE(BgL_auxz00_5020,BFALSE,BFALSE);} 
BgL_auxz00_5016 = 
(long)CINT(BgL_tmpz00_5017); } 
return 
BGl_makezd2u64vectorzd2zz__srfi4z00(BgL_auxz00_5016, (uint64_t)(0));} } break;case 2L : 

{ /* Llib/srfi4.scm 668 */
 obj_t BgL_initz00_1490;
BgL_initz00_1490 = 
VECTOR_REF(BgL_opt1283z00_65,1L); 
{ /* Llib/srfi4.scm 668 */

{ /* Llib/srfi4.scm 668 */
 uint64_t BgL_auxz00_5036; long BgL_auxz00_5027;
{ /* Llib/srfi4.scm 668 */
 obj_t BgL_tmpz00_5037;
if(
BGL_UINT64P(BgL_initz00_1490))
{ /* Llib/srfi4.scm 668 */
BgL_tmpz00_5037 = BgL_initz00_1490
; }  else 
{ 
 obj_t BgL_auxz00_5040;
BgL_auxz00_5040 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27450L), BGl_string2949z00zz__srfi4z00, BGl_string2950z00zz__srfi4z00, BgL_initz00_1490); 
FAILURE(BgL_auxz00_5040,BFALSE,BFALSE);} 
BgL_auxz00_5036 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5037); } 
{ /* Llib/srfi4.scm 668 */
 obj_t BgL_tmpz00_5028;
if(
INTEGERP(BgL_g1285z00_1486))
{ /* Llib/srfi4.scm 668 */
BgL_tmpz00_5028 = BgL_g1285z00_1486
; }  else 
{ 
 obj_t BgL_auxz00_5031;
BgL_auxz00_5031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27450L), BGl_string2949z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1285z00_1486); 
FAILURE(BgL_auxz00_5031,BFALSE,BFALSE);} 
BgL_auxz00_5027 = 
(long)CINT(BgL_tmpz00_5028); } 
return 
BGl_makezd2u64vectorzd2zz__srfi4z00(BgL_auxz00_5027, BgL_auxz00_5036);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2947z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1283z00_65)));} } } } 

}



/* make-u64vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2u64vectorzd2zz__srfi4z00(long BgL_lenz00_63, uint64_t BgL_initz00_64)
{
{ /* Llib/srfi4.scm 668 */
{ /* Llib/srfi4.scm 668 */
 obj_t BgL_vz00_1492;
{ /* Llib/srfi4.scm 668 */
 int32_t BgL_tmpz00_5051;
BgL_tmpz00_5051 = 
(int32_t)(BgL_lenz00_63); 
BgL_vz00_1492 = 
BGL_ALLOC_U64VECTOR(BgL_tmpz00_5051); } 
{ 
 long BgL_iz00_2553;
BgL_iz00_2553 = 0L; 
BgL_loopz00_2552:
if(
(BgL_iz00_2553<BgL_lenz00_63))
{ /* Llib/srfi4.scm 668 */
{ /* Llib/srfi4.scm 668 */
 long BgL_l2092z00_3268;
BgL_l2092z00_3268 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1492); 
if(
BOUND_CHECK(BgL_iz00_2553, BgL_l2092z00_3268))
{ /* Llib/srfi4.scm 668 */
BGL_U64VSET(BgL_vz00_1492, BgL_iz00_2553, BgL_initz00_64); }  else 
{ 
 obj_t BgL_auxz00_5060;
BgL_auxz00_5060 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27450L), BGl_string2951z00zz__srfi4z00, BgL_vz00_1492, 
(int)(BgL_l2092z00_3268), 
(int)(BgL_iz00_2553)); 
FAILURE(BgL_auxz00_5060,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_5066;
BgL_iz00_5066 = 
(BgL_iz00_2553+1L); 
BgL_iz00_2553 = BgL_iz00_5066; 
goto BgL_loopz00_2552;} }  else 
{ /* Llib/srfi4.scm 668 */((bool_t)0); } } 
return BgL_vz00_1492;} } 

}



/* _make-f32vector */
obj_t BGl__makezd2f32vectorzd2zz__srfi4z00(obj_t BgL_env1289z00_70, obj_t BgL_opt1288z00_69)
{
{ /* Llib/srfi4.scm 669 */
{ /* Llib/srfi4.scm 669 */
 obj_t BgL_g1290z00_1499;
BgL_g1290z00_1499 = 
VECTOR_REF(BgL_opt1288z00_69,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1288z00_69)) { case 1L : 

{ /* Llib/srfi4.scm 669 */

{ /* Llib/srfi4.scm 669 */
 long BgL_auxz00_5069;
{ /* Llib/srfi4.scm 669 */
 obj_t BgL_tmpz00_5070;
if(
INTEGERP(BgL_g1290z00_1499))
{ /* Llib/srfi4.scm 669 */
BgL_tmpz00_5070 = BgL_g1290z00_1499
; }  else 
{ 
 obj_t BgL_auxz00_5073;
BgL_auxz00_5073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27484L), BGl_string2954z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1290z00_1499); 
FAILURE(BgL_auxz00_5073,BFALSE,BFALSE);} 
BgL_auxz00_5069 = 
(long)CINT(BgL_tmpz00_5070); } 
return 
BGl_makezd2f32vectorzd2zz__srfi4z00(BgL_auxz00_5069, 
(float)(((double)0.0)));} } break;case 2L : 

{ /* Llib/srfi4.scm 669 */
 obj_t BgL_initz00_1503;
BgL_initz00_1503 = 
VECTOR_REF(BgL_opt1288z00_69,1L); 
{ /* Llib/srfi4.scm 669 */

{ /* Llib/srfi4.scm 669 */
 float BgL_auxz00_5090; long BgL_auxz00_5081;
{ /* Llib/srfi4.scm 669 */
 obj_t BgL_tmpz00_5091;
if(
REALP(BgL_initz00_1503))
{ /* Llib/srfi4.scm 669 */
BgL_tmpz00_5091 = BgL_initz00_1503
; }  else 
{ 
 obj_t BgL_auxz00_5094;
BgL_auxz00_5094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27484L), BGl_string2954z00zz__srfi4z00, BGl_string2955z00zz__srfi4z00, BgL_initz00_1503); 
FAILURE(BgL_auxz00_5094,BFALSE,BFALSE);} 
BgL_auxz00_5090 = 
REAL_TO_FLOAT(BgL_tmpz00_5091); } 
{ /* Llib/srfi4.scm 669 */
 obj_t BgL_tmpz00_5082;
if(
INTEGERP(BgL_g1290z00_1499))
{ /* Llib/srfi4.scm 669 */
BgL_tmpz00_5082 = BgL_g1290z00_1499
; }  else 
{ 
 obj_t BgL_auxz00_5085;
BgL_auxz00_5085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27484L), BGl_string2954z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1290z00_1499); 
FAILURE(BgL_auxz00_5085,BFALSE,BFALSE);} 
BgL_auxz00_5081 = 
(long)CINT(BgL_tmpz00_5082); } 
return 
BGl_makezd2f32vectorzd2zz__srfi4z00(BgL_auxz00_5081, BgL_auxz00_5090);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2952z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1288z00_69)));} } } } 

}



/* make-f32vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2f32vectorzd2zz__srfi4z00(long BgL_lenz00_67, float BgL_initz00_68)
{
{ /* Llib/srfi4.scm 669 */
{ /* Llib/srfi4.scm 669 */
 obj_t BgL_vz00_1505;
{ /* Llib/srfi4.scm 669 */
 int32_t BgL_tmpz00_5105;
BgL_tmpz00_5105 = 
(int32_t)(BgL_lenz00_67); 
BgL_vz00_1505 = 
BGL_ALLOC_F32VECTOR(BgL_tmpz00_5105); } 
{ 
 long BgL_iz00_2563;
BgL_iz00_2563 = 0L; 
BgL_loopz00_2562:
if(
(BgL_iz00_2563<BgL_lenz00_67))
{ /* Llib/srfi4.scm 669 */
{ /* Llib/srfi4.scm 669 */
 long BgL_l2096z00_3272;
BgL_l2096z00_3272 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1505); 
if(
BOUND_CHECK(BgL_iz00_2563, BgL_l2096z00_3272))
{ /* Llib/srfi4.scm 669 */
BGL_F32VSET(BgL_vz00_1505, BgL_iz00_2563, BgL_initz00_68); }  else 
{ 
 obj_t BgL_auxz00_5114;
BgL_auxz00_5114 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27484L), BGl_string2956z00zz__srfi4z00, BgL_vz00_1505, 
(int)(BgL_l2096z00_3272), 
(int)(BgL_iz00_2563)); 
FAILURE(BgL_auxz00_5114,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_5120;
BgL_iz00_5120 = 
(BgL_iz00_2563+1L); 
BgL_iz00_2563 = BgL_iz00_5120; 
goto BgL_loopz00_2562;} }  else 
{ /* Llib/srfi4.scm 669 */((bool_t)0); } } 
return BgL_vz00_1505;} } 

}



/* _make-f64vector */
obj_t BGl__makezd2f64vectorzd2zz__srfi4z00(obj_t BgL_env1294z00_74, obj_t BgL_opt1293z00_73)
{
{ /* Llib/srfi4.scm 670 */
{ /* Llib/srfi4.scm 670 */
 obj_t BgL_g1295z00_1512;
BgL_g1295z00_1512 = 
VECTOR_REF(BgL_opt1293z00_73,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1293z00_73)) { case 1L : 

{ /* Llib/srfi4.scm 670 */

{ /* Llib/srfi4.scm 670 */
 long BgL_auxz00_5123;
{ /* Llib/srfi4.scm 670 */
 obj_t BgL_tmpz00_5124;
if(
INTEGERP(BgL_g1295z00_1512))
{ /* Llib/srfi4.scm 670 */
BgL_tmpz00_5124 = BgL_g1295z00_1512
; }  else 
{ 
 obj_t BgL_auxz00_5127;
BgL_auxz00_5127 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27515L), BGl_string2959z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1295z00_1512); 
FAILURE(BgL_auxz00_5127,BFALSE,BFALSE);} 
BgL_auxz00_5123 = 
(long)CINT(BgL_tmpz00_5124); } 
return 
BGl_makezd2f64vectorzd2zz__srfi4z00(BgL_auxz00_5123, ((double)0.0));} } break;case 2L : 

{ /* Llib/srfi4.scm 670 */
 obj_t BgL_initz00_1516;
BgL_initz00_1516 = 
VECTOR_REF(BgL_opt1293z00_73,1L); 
{ /* Llib/srfi4.scm 670 */

{ /* Llib/srfi4.scm 670 */
 double BgL_auxz00_5143; long BgL_auxz00_5134;
{ /* Llib/srfi4.scm 670 */
 obj_t BgL_tmpz00_5144;
if(
REALP(BgL_initz00_1516))
{ /* Llib/srfi4.scm 670 */
BgL_tmpz00_5144 = BgL_initz00_1516
; }  else 
{ 
 obj_t BgL_auxz00_5147;
BgL_auxz00_5147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27515L), BGl_string2959z00zz__srfi4z00, BGl_string2955z00zz__srfi4z00, BgL_initz00_1516); 
FAILURE(BgL_auxz00_5147,BFALSE,BFALSE);} 
BgL_auxz00_5143 = 
REAL_TO_DOUBLE(BgL_tmpz00_5144); } 
{ /* Llib/srfi4.scm 670 */
 obj_t BgL_tmpz00_5135;
if(
INTEGERP(BgL_g1295z00_1512))
{ /* Llib/srfi4.scm 670 */
BgL_tmpz00_5135 = BgL_g1295z00_1512
; }  else 
{ 
 obj_t BgL_auxz00_5138;
BgL_auxz00_5138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27515L), BGl_string2959z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_g1295z00_1512); 
FAILURE(BgL_auxz00_5138,BFALSE,BFALSE);} 
BgL_auxz00_5134 = 
(long)CINT(BgL_tmpz00_5135); } 
return 
BGl_makezd2f64vectorzd2zz__srfi4z00(BgL_auxz00_5134, BgL_auxz00_5143);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2957z00zz__srfi4z00, BGl_string2912z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1293z00_73)));} } } } 

}



/* make-f64vector */
BGL_EXPORTED_DEF obj_t BGl_makezd2f64vectorzd2zz__srfi4z00(long BgL_lenz00_71, double BgL_initz00_72)
{
{ /* Llib/srfi4.scm 670 */
{ /* Llib/srfi4.scm 670 */
 obj_t BgL_vz00_1518;
{ /* Llib/srfi4.scm 670 */
 int32_t BgL_tmpz00_5158;
BgL_tmpz00_5158 = 
(int32_t)(BgL_lenz00_71); 
BgL_vz00_1518 = 
BGL_ALLOC_F64VECTOR(BgL_tmpz00_5158); } 
{ 
 long BgL_iz00_2573;
BgL_iz00_2573 = 0L; 
BgL_loopz00_2572:
if(
(BgL_iz00_2573<BgL_lenz00_71))
{ /* Llib/srfi4.scm 670 */
{ /* Llib/srfi4.scm 670 */
 long BgL_l2100z00_3276;
BgL_l2100z00_3276 = 
BGL_HVECTOR_LENGTH(BgL_vz00_1518); 
if(
BOUND_CHECK(BgL_iz00_2573, BgL_l2100z00_3276))
{ /* Llib/srfi4.scm 670 */
BGL_F64VSET(BgL_vz00_1518, BgL_iz00_2573, BgL_initz00_72); }  else 
{ 
 obj_t BgL_auxz00_5167;
BgL_auxz00_5167 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27515L), BGl_string2960z00zz__srfi4z00, BgL_vz00_1518, 
(int)(BgL_l2100z00_3276), 
(int)(BgL_iz00_2573)); 
FAILURE(BgL_auxz00_5167,BFALSE,BFALSE);} } BUNSPEC; 
{ 
 long BgL_iz00_5173;
BgL_iz00_5173 = 
(BgL_iz00_2573+1L); 
BgL_iz00_2573 = BgL_iz00_5173; 
goto BgL_loopz00_2572;} }  else 
{ /* Llib/srfi4.scm 670 */((bool_t)0); } } 
return BgL_vz00_1518;} } 

}



/* hvector-range-error */
BGL_EXPORTED_DEF obj_t BGl_hvectorzd2rangezd2errorz00zz__srfi4z00(obj_t BgL_funz00_75, obj_t BgL_vz00_76, long BgL_kz00_77)
{
{ /* Llib/srfi4.scm 675 */
{ /* Llib/srfi4.scm 678 */
 obj_t BgL_arg1451z00_2579;
{ /* Llib/srfi4.scm 678 */
 obj_t BgL_arg1452z00_2580;
{ /* Llib/srfi4.scm 678 */
 long BgL_arg1453z00_2581;
{ /* Llib/srfi4.scm 678 */
 long BgL_arg1455z00_2582;
BgL_arg1455z00_2582 = 
BGL_HVECTOR_LENGTH(BgL_vz00_76); 
BgL_arg1453z00_2581 = 
(BgL_arg1455z00_2582-1L); } 
{ /* Llib/srfi4.scm 678 */

BgL_arg1452z00_2580 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BgL_arg1453z00_2581, 10L); } } 
BgL_arg1451z00_2579 = 
string_append_3(BGl_string2961z00zz__srfi4z00, BgL_arg1452z00_2580, BGl_string2962z00zz__srfi4z00); } 
return 
BGl_errorz00zz__errorz00(BgL_funz00_75, BgL_arg1451z00_2579, 
BINT(BgL_kz00_77));} } 

}



/* &hvector-range-error */
obj_t BGl_z62hvectorzd2rangezd2errorz62zz__srfi4z00(obj_t BgL_envz00_3194, obj_t BgL_funz00_3195, obj_t BgL_vz00_3196, obj_t BgL_kz00_3197)
{
{ /* Llib/srfi4.scm 675 */
{ /* Llib/srfi4.scm 678 */
 long BgL_auxz00_5188; obj_t BgL_auxz00_5181;
{ /* Llib/srfi4.scm 678 */
 obj_t BgL_tmpz00_5189;
if(
INTEGERP(BgL_kz00_3197))
{ /* Llib/srfi4.scm 678 */
BgL_tmpz00_5189 = BgL_kz00_3197
; }  else 
{ 
 obj_t BgL_auxz00_5192;
BgL_auxz00_5192 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27892L), BGl_string2963z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3197); 
FAILURE(BgL_auxz00_5192,BFALSE,BFALSE);} 
BgL_auxz00_5188 = 
(long)CINT(BgL_tmpz00_5189); } 
if(
STRINGP(BgL_funz00_3195))
{ /* Llib/srfi4.scm 678 */
BgL_auxz00_5181 = BgL_funz00_3195
; }  else 
{ 
 obj_t BgL_auxz00_5184;
BgL_auxz00_5184 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(27892L), BGl_string2963z00zz__srfi4z00, BGl_string2964z00zz__srfi4z00, BgL_funz00_3195); 
FAILURE(BgL_auxz00_5184,BFALSE,BFALSE);} 
return 
BGl_hvectorzd2rangezd2errorz00zz__srfi4z00(BgL_auxz00_5181, BgL_vz00_3196, BgL_auxz00_5188);} } 

}



/* s8vector-ref */
BGL_EXPORTED_DEF int8_t BGl_s8vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_78, long BgL_kz00_79)
{
{ /* Llib/srfi4.scm 685 */
{ /* Llib/srfi4.scm 686 */
 long BgL_l2104z00_4081;
BgL_l2104z00_4081 = 
BGL_HVECTOR_LENGTH(BgL_vz00_78); 
if(
BOUND_CHECK(BgL_kz00_79, BgL_l2104z00_4081))
{ /* Llib/srfi4.scm 686 */
return 
BGL_S8VREF(BgL_vz00_78, BgL_kz00_79);}  else 
{ 
 obj_t BgL_auxz00_5202;
BgL_auxz00_5202 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28196L), BGl_string2965z00zz__srfi4z00, BgL_vz00_78, 
(int)(BgL_l2104z00_4081), 
(int)(BgL_kz00_79)); 
FAILURE(BgL_auxz00_5202,BFALSE,BFALSE);} } } 

}



/* &s8vector-ref */
obj_t BGl_z62s8vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3057, obj_t BgL_vz00_3058, obj_t BgL_kz00_3059)
{
{ /* Llib/srfi4.scm 685 */
{ /* Llib/srfi4.scm 686 */
 int8_t BgL_tmpz00_5208;
{ /* Llib/srfi4.scm 686 */
 obj_t BgL_vz00_4082; long BgL_kz00_4083;
if(
BGL_S8VECTORP(BgL_vz00_3058))
{ /* Llib/srfi4.scm 686 */
BgL_vz00_4082 = BgL_vz00_3058; }  else 
{ 
 obj_t BgL_auxz00_5211;
BgL_auxz00_5211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28196L), BGl_string2966z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_vz00_3058); 
FAILURE(BgL_auxz00_5211,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 686 */
 obj_t BgL_tmpz00_5215;
if(
INTEGERP(BgL_kz00_3059))
{ /* Llib/srfi4.scm 686 */
BgL_tmpz00_5215 = BgL_kz00_3059
; }  else 
{ 
 obj_t BgL_auxz00_5218;
BgL_auxz00_5218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28196L), BGl_string2966z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3059); 
FAILURE(BgL_auxz00_5218,BFALSE,BFALSE);} 
BgL_kz00_4083 = 
(long)CINT(BgL_tmpz00_5215); } 
{ /* Llib/srfi4.scm 686 */
 long BgL_l2104z00_4084;
BgL_l2104z00_4084 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4082); 
if(
BOUND_CHECK(BgL_kz00_4083, BgL_l2104z00_4084))
{ /* Llib/srfi4.scm 686 */
BgL_tmpz00_5208 = 
BGL_S8VREF(BgL_vz00_4082, BgL_kz00_4083)
; }  else 
{ 
 obj_t BgL_auxz00_5227;
BgL_auxz00_5227 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28196L), BGl_string2965z00zz__srfi4z00, BgL_vz00_4082, 
(int)(BgL_l2104z00_4084), 
(int)(BgL_kz00_4083)); 
FAILURE(BgL_auxz00_5227,BFALSE,BFALSE);} } } 
return 
BGL_INT8_TO_BINT8(BgL_tmpz00_5208);} } 

}



/* u8vector-ref */
BGL_EXPORTED_DEF uint8_t BGl_u8vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_80, long BgL_kz00_81)
{
{ /* Llib/srfi4.scm 688 */
{ /* Llib/srfi4.scm 689 */
 long BgL_l2108z00_4085;
BgL_l2108z00_4085 = 
BGL_HVECTOR_LENGTH(BgL_vz00_80); 
if(
BOUND_CHECK(BgL_kz00_81, BgL_l2108z00_4085))
{ /* Llib/srfi4.scm 689 */
return 
BGL_U8VREF(BgL_vz00_80, BgL_kz00_81);}  else 
{ 
 obj_t BgL_auxz00_5238;
BgL_auxz00_5238 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28255L), BGl_string2967z00zz__srfi4z00, BgL_vz00_80, 
(int)(BgL_l2108z00_4085), 
(int)(BgL_kz00_81)); 
FAILURE(BgL_auxz00_5238,BFALSE,BFALSE);} } } 

}



/* &u8vector-ref */
obj_t BGl_z62u8vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3067, obj_t BgL_vz00_3068, obj_t BgL_kz00_3069)
{
{ /* Llib/srfi4.scm 688 */
{ /* Llib/srfi4.scm 689 */
 uint8_t BgL_tmpz00_5244;
{ /* Llib/srfi4.scm 689 */
 obj_t BgL_vz00_4086; long BgL_kz00_4087;
if(
BGL_U8VECTORP(BgL_vz00_3068))
{ /* Llib/srfi4.scm 689 */
BgL_vz00_4086 = BgL_vz00_3068; }  else 
{ 
 obj_t BgL_auxz00_5247;
BgL_auxz00_5247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28255L), BGl_string2968z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_vz00_3068); 
FAILURE(BgL_auxz00_5247,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 689 */
 obj_t BgL_tmpz00_5251;
if(
INTEGERP(BgL_kz00_3069))
{ /* Llib/srfi4.scm 689 */
BgL_tmpz00_5251 = BgL_kz00_3069
; }  else 
{ 
 obj_t BgL_auxz00_5254;
BgL_auxz00_5254 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28255L), BGl_string2968z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3069); 
FAILURE(BgL_auxz00_5254,BFALSE,BFALSE);} 
BgL_kz00_4087 = 
(long)CINT(BgL_tmpz00_5251); } 
{ /* Llib/srfi4.scm 689 */
 long BgL_l2108z00_4088;
BgL_l2108z00_4088 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4086); 
if(
BOUND_CHECK(BgL_kz00_4087, BgL_l2108z00_4088))
{ /* Llib/srfi4.scm 689 */
BgL_tmpz00_5244 = 
BGL_U8VREF(BgL_vz00_4086, BgL_kz00_4087)
; }  else 
{ 
 obj_t BgL_auxz00_5263;
BgL_auxz00_5263 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28255L), BGl_string2967z00zz__srfi4z00, BgL_vz00_4086, 
(int)(BgL_l2108z00_4088), 
(int)(BgL_kz00_4087)); 
FAILURE(BgL_auxz00_5263,BFALSE,BFALSE);} } } 
return 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_5244);} } 

}



/* s16vector-ref */
BGL_EXPORTED_DEF int16_t BGl_s16vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_82, long BgL_kz00_83)
{
{ /* Llib/srfi4.scm 691 */
{ /* Llib/srfi4.scm 692 */
 long BgL_l2112z00_4089;
BgL_l2112z00_4089 = 
BGL_HVECTOR_LENGTH(BgL_vz00_82); 
if(
BOUND_CHECK(BgL_kz00_83, BgL_l2112z00_4089))
{ /* Llib/srfi4.scm 692 */
return 
BGL_S16VREF(BgL_vz00_82, BgL_kz00_83);}  else 
{ 
 obj_t BgL_auxz00_5274;
BgL_auxz00_5274 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28315L), BGl_string2969z00zz__srfi4z00, BgL_vz00_82, 
(int)(BgL_l2112z00_4089), 
(int)(BgL_kz00_83)); 
FAILURE(BgL_auxz00_5274,BFALSE,BFALSE);} } } 

}



/* &s16vector-ref */
obj_t BGl_z62s16vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3077, obj_t BgL_vz00_3078, obj_t BgL_kz00_3079)
{
{ /* Llib/srfi4.scm 691 */
{ /* Llib/srfi4.scm 692 */
 int16_t BgL_tmpz00_5280;
{ /* Llib/srfi4.scm 692 */
 obj_t BgL_vz00_4090; long BgL_kz00_4091;
if(
BGL_S16VECTORP(BgL_vz00_3078))
{ /* Llib/srfi4.scm 692 */
BgL_vz00_4090 = BgL_vz00_3078; }  else 
{ 
 obj_t BgL_auxz00_5283;
BgL_auxz00_5283 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28315L), BGl_string2970z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_vz00_3078); 
FAILURE(BgL_auxz00_5283,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 692 */
 obj_t BgL_tmpz00_5287;
if(
INTEGERP(BgL_kz00_3079))
{ /* Llib/srfi4.scm 692 */
BgL_tmpz00_5287 = BgL_kz00_3079
; }  else 
{ 
 obj_t BgL_auxz00_5290;
BgL_auxz00_5290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28315L), BGl_string2970z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3079); 
FAILURE(BgL_auxz00_5290,BFALSE,BFALSE);} 
BgL_kz00_4091 = 
(long)CINT(BgL_tmpz00_5287); } 
{ /* Llib/srfi4.scm 692 */
 long BgL_l2112z00_4092;
BgL_l2112z00_4092 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4090); 
if(
BOUND_CHECK(BgL_kz00_4091, BgL_l2112z00_4092))
{ /* Llib/srfi4.scm 692 */
BgL_tmpz00_5280 = 
BGL_S16VREF(BgL_vz00_4090, BgL_kz00_4091)
; }  else 
{ 
 obj_t BgL_auxz00_5299;
BgL_auxz00_5299 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28315L), BGl_string2969z00zz__srfi4z00, BgL_vz00_4090, 
(int)(BgL_l2112z00_4092), 
(int)(BgL_kz00_4091)); 
FAILURE(BgL_auxz00_5299,BFALSE,BFALSE);} } } 
return 
BGL_INT16_TO_BINT16(BgL_tmpz00_5280);} } 

}



/* u16vector-ref */
BGL_EXPORTED_DEF uint16_t BGl_u16vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_84, long BgL_kz00_85)
{
{ /* Llib/srfi4.scm 694 */
{ /* Llib/srfi4.scm 695 */
 long BgL_l2116z00_4093;
BgL_l2116z00_4093 = 
BGL_HVECTOR_LENGTH(BgL_vz00_84); 
if(
BOUND_CHECK(BgL_kz00_85, BgL_l2116z00_4093))
{ /* Llib/srfi4.scm 695 */
return 
BGL_U16VREF(BgL_vz00_84, BgL_kz00_85);}  else 
{ 
 obj_t BgL_auxz00_5310;
BgL_auxz00_5310 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28376L), BGl_string2971z00zz__srfi4z00, BgL_vz00_84, 
(int)(BgL_l2116z00_4093), 
(int)(BgL_kz00_85)); 
FAILURE(BgL_auxz00_5310,BFALSE,BFALSE);} } } 

}



/* &u16vector-ref */
obj_t BGl_z62u16vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3087, obj_t BgL_vz00_3088, obj_t BgL_kz00_3089)
{
{ /* Llib/srfi4.scm 694 */
{ /* Llib/srfi4.scm 695 */
 uint16_t BgL_tmpz00_5316;
{ /* Llib/srfi4.scm 695 */
 obj_t BgL_vz00_4094; long BgL_kz00_4095;
if(
BGL_U16VECTORP(BgL_vz00_3088))
{ /* Llib/srfi4.scm 695 */
BgL_vz00_4094 = BgL_vz00_3088; }  else 
{ 
 obj_t BgL_auxz00_5319;
BgL_auxz00_5319 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28376L), BGl_string2972z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_vz00_3088); 
FAILURE(BgL_auxz00_5319,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 695 */
 obj_t BgL_tmpz00_5323;
if(
INTEGERP(BgL_kz00_3089))
{ /* Llib/srfi4.scm 695 */
BgL_tmpz00_5323 = BgL_kz00_3089
; }  else 
{ 
 obj_t BgL_auxz00_5326;
BgL_auxz00_5326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28376L), BGl_string2972z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3089); 
FAILURE(BgL_auxz00_5326,BFALSE,BFALSE);} 
BgL_kz00_4095 = 
(long)CINT(BgL_tmpz00_5323); } 
{ /* Llib/srfi4.scm 695 */
 long BgL_l2116z00_4096;
BgL_l2116z00_4096 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4094); 
if(
BOUND_CHECK(BgL_kz00_4095, BgL_l2116z00_4096))
{ /* Llib/srfi4.scm 695 */
BgL_tmpz00_5316 = 
BGL_U16VREF(BgL_vz00_4094, BgL_kz00_4095)
; }  else 
{ 
 obj_t BgL_auxz00_5335;
BgL_auxz00_5335 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28376L), BGl_string2971z00zz__srfi4z00, BgL_vz00_4094, 
(int)(BgL_l2116z00_4096), 
(int)(BgL_kz00_4095)); 
FAILURE(BgL_auxz00_5335,BFALSE,BFALSE);} } } 
return 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_5316);} } 

}



/* s32vector-ref */
BGL_EXPORTED_DEF int32_t BGl_s32vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_86, long BgL_kz00_87)
{
{ /* Llib/srfi4.scm 697 */
{ /* Llib/srfi4.scm 698 */
 long BgL_l2120z00_4097;
BgL_l2120z00_4097 = 
BGL_HVECTOR_LENGTH(BgL_vz00_86); 
if(
BOUND_CHECK(BgL_kz00_87, BgL_l2120z00_4097))
{ /* Llib/srfi4.scm 698 */
return 
BGL_S32VREF(BgL_vz00_86, BgL_kz00_87);}  else 
{ 
 obj_t BgL_auxz00_5346;
BgL_auxz00_5346 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28437L), BGl_string2973z00zz__srfi4z00, BgL_vz00_86, 
(int)(BgL_l2120z00_4097), 
(int)(BgL_kz00_87)); 
FAILURE(BgL_auxz00_5346,BFALSE,BFALSE);} } } 

}



/* &s32vector-ref */
obj_t BGl_z62s32vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3097, obj_t BgL_vz00_3098, obj_t BgL_kz00_3099)
{
{ /* Llib/srfi4.scm 697 */
{ /* Llib/srfi4.scm 698 */
 int32_t BgL_tmpz00_5352;
{ /* Llib/srfi4.scm 698 */
 obj_t BgL_vz00_4098; long BgL_kz00_4099;
if(
BGL_S32VECTORP(BgL_vz00_3098))
{ /* Llib/srfi4.scm 698 */
BgL_vz00_4098 = BgL_vz00_3098; }  else 
{ 
 obj_t BgL_auxz00_5355;
BgL_auxz00_5355 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28437L), BGl_string2974z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_vz00_3098); 
FAILURE(BgL_auxz00_5355,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 698 */
 obj_t BgL_tmpz00_5359;
if(
INTEGERP(BgL_kz00_3099))
{ /* Llib/srfi4.scm 698 */
BgL_tmpz00_5359 = BgL_kz00_3099
; }  else 
{ 
 obj_t BgL_auxz00_5362;
BgL_auxz00_5362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28437L), BGl_string2974z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3099); 
FAILURE(BgL_auxz00_5362,BFALSE,BFALSE);} 
BgL_kz00_4099 = 
(long)CINT(BgL_tmpz00_5359); } 
{ /* Llib/srfi4.scm 698 */
 long BgL_l2120z00_4100;
BgL_l2120z00_4100 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4098); 
if(
BOUND_CHECK(BgL_kz00_4099, BgL_l2120z00_4100))
{ /* Llib/srfi4.scm 698 */
BgL_tmpz00_5352 = 
BGL_S32VREF(BgL_vz00_4098, BgL_kz00_4099)
; }  else 
{ 
 obj_t BgL_auxz00_5371;
BgL_auxz00_5371 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28437L), BGl_string2973z00zz__srfi4z00, BgL_vz00_4098, 
(int)(BgL_l2120z00_4100), 
(int)(BgL_kz00_4099)); 
FAILURE(BgL_auxz00_5371,BFALSE,BFALSE);} } } 
return 
BGL_INT32_TO_BINT32(BgL_tmpz00_5352);} } 

}



/* u32vector-ref */
BGL_EXPORTED_DEF uint32_t BGl_u32vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_88, long BgL_kz00_89)
{
{ /* Llib/srfi4.scm 700 */
{ /* Llib/srfi4.scm 701 */
 long BgL_l2124z00_4101;
BgL_l2124z00_4101 = 
BGL_HVECTOR_LENGTH(BgL_vz00_88); 
if(
BOUND_CHECK(BgL_kz00_89, BgL_l2124z00_4101))
{ /* Llib/srfi4.scm 701 */
return 
BGL_U32VREF(BgL_vz00_88, BgL_kz00_89);}  else 
{ 
 obj_t BgL_auxz00_5382;
BgL_auxz00_5382 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28498L), BGl_string2975z00zz__srfi4z00, BgL_vz00_88, 
(int)(BgL_l2124z00_4101), 
(int)(BgL_kz00_89)); 
FAILURE(BgL_auxz00_5382,BFALSE,BFALSE);} } } 

}



/* &u32vector-ref */
obj_t BGl_z62u32vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3107, obj_t BgL_vz00_3108, obj_t BgL_kz00_3109)
{
{ /* Llib/srfi4.scm 700 */
{ /* Llib/srfi4.scm 701 */
 uint32_t BgL_tmpz00_5388;
{ /* Llib/srfi4.scm 701 */
 obj_t BgL_vz00_4102; long BgL_kz00_4103;
if(
BGL_U32VECTORP(BgL_vz00_3108))
{ /* Llib/srfi4.scm 701 */
BgL_vz00_4102 = BgL_vz00_3108; }  else 
{ 
 obj_t BgL_auxz00_5391;
BgL_auxz00_5391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28498L), BGl_string2976z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_vz00_3108); 
FAILURE(BgL_auxz00_5391,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 701 */
 obj_t BgL_tmpz00_5395;
if(
INTEGERP(BgL_kz00_3109))
{ /* Llib/srfi4.scm 701 */
BgL_tmpz00_5395 = BgL_kz00_3109
; }  else 
{ 
 obj_t BgL_auxz00_5398;
BgL_auxz00_5398 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28498L), BGl_string2976z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3109); 
FAILURE(BgL_auxz00_5398,BFALSE,BFALSE);} 
BgL_kz00_4103 = 
(long)CINT(BgL_tmpz00_5395); } 
{ /* Llib/srfi4.scm 701 */
 long BgL_l2124z00_4104;
BgL_l2124z00_4104 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4102); 
if(
BOUND_CHECK(BgL_kz00_4103, BgL_l2124z00_4104))
{ /* Llib/srfi4.scm 701 */
BgL_tmpz00_5388 = 
BGL_U32VREF(BgL_vz00_4102, BgL_kz00_4103)
; }  else 
{ 
 obj_t BgL_auxz00_5407;
BgL_auxz00_5407 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28498L), BGl_string2975z00zz__srfi4z00, BgL_vz00_4102, 
(int)(BgL_l2124z00_4104), 
(int)(BgL_kz00_4103)); 
FAILURE(BgL_auxz00_5407,BFALSE,BFALSE);} } } 
return 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_5388);} } 

}



/* s64vector-ref */
BGL_EXPORTED_DEF int64_t BGl_s64vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_90, long BgL_kz00_91)
{
{ /* Llib/srfi4.scm 703 */
{ /* Llib/srfi4.scm 704 */
 long BgL_l2128z00_4105;
BgL_l2128z00_4105 = 
BGL_HVECTOR_LENGTH(BgL_vz00_90); 
if(
BOUND_CHECK(BgL_kz00_91, BgL_l2128z00_4105))
{ /* Llib/srfi4.scm 704 */
return 
BGL_S64VREF(BgL_vz00_90, BgL_kz00_91);}  else 
{ 
 obj_t BgL_auxz00_5418;
BgL_auxz00_5418 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28559L), BGl_string2977z00zz__srfi4z00, BgL_vz00_90, 
(int)(BgL_l2128z00_4105), 
(int)(BgL_kz00_91)); 
FAILURE(BgL_auxz00_5418,BFALSE,BFALSE);} } } 

}



/* &s64vector-ref */
obj_t BGl_z62s64vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3117, obj_t BgL_vz00_3118, obj_t BgL_kz00_3119)
{
{ /* Llib/srfi4.scm 703 */
{ /* Llib/srfi4.scm 704 */
 int64_t BgL_tmpz00_5424;
{ /* Llib/srfi4.scm 704 */
 obj_t BgL_vz00_4106; long BgL_kz00_4107;
if(
BGL_S64VECTORP(BgL_vz00_3118))
{ /* Llib/srfi4.scm 704 */
BgL_vz00_4106 = BgL_vz00_3118; }  else 
{ 
 obj_t BgL_auxz00_5427;
BgL_auxz00_5427 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28559L), BGl_string2978z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_vz00_3118); 
FAILURE(BgL_auxz00_5427,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 704 */
 obj_t BgL_tmpz00_5431;
if(
INTEGERP(BgL_kz00_3119))
{ /* Llib/srfi4.scm 704 */
BgL_tmpz00_5431 = BgL_kz00_3119
; }  else 
{ 
 obj_t BgL_auxz00_5434;
BgL_auxz00_5434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28559L), BGl_string2978z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3119); 
FAILURE(BgL_auxz00_5434,BFALSE,BFALSE);} 
BgL_kz00_4107 = 
(long)CINT(BgL_tmpz00_5431); } 
{ /* Llib/srfi4.scm 704 */
 long BgL_l2128z00_4108;
BgL_l2128z00_4108 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4106); 
if(
BOUND_CHECK(BgL_kz00_4107, BgL_l2128z00_4108))
{ /* Llib/srfi4.scm 704 */
BgL_tmpz00_5424 = 
BGL_S64VREF(BgL_vz00_4106, BgL_kz00_4107)
; }  else 
{ 
 obj_t BgL_auxz00_5443;
BgL_auxz00_5443 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28559L), BGl_string2977z00zz__srfi4z00, BgL_vz00_4106, 
(int)(BgL_l2128z00_4108), 
(int)(BgL_kz00_4107)); 
FAILURE(BgL_auxz00_5443,BFALSE,BFALSE);} } } 
return 
BGL_INT64_TO_BINT64(BgL_tmpz00_5424);} } 

}



/* u64vector-ref */
BGL_EXPORTED_DEF uint64_t BGl_u64vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_92, long BgL_kz00_93)
{
{ /* Llib/srfi4.scm 706 */
{ /* Llib/srfi4.scm 707 */
 long BgL_l2132z00_4109;
BgL_l2132z00_4109 = 
BGL_HVECTOR_LENGTH(BgL_vz00_92); 
if(
BOUND_CHECK(BgL_kz00_93, BgL_l2132z00_4109))
{ /* Llib/srfi4.scm 707 */
return 
BGL_U64VREF(BgL_vz00_92, BgL_kz00_93);}  else 
{ 
 obj_t BgL_auxz00_5454;
BgL_auxz00_5454 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28620L), BGl_string2979z00zz__srfi4z00, BgL_vz00_92, 
(int)(BgL_l2132z00_4109), 
(int)(BgL_kz00_93)); 
FAILURE(BgL_auxz00_5454,BFALSE,BFALSE);} } } 

}



/* &u64vector-ref */
obj_t BGl_z62u64vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3127, obj_t BgL_vz00_3128, obj_t BgL_kz00_3129)
{
{ /* Llib/srfi4.scm 706 */
{ /* Llib/srfi4.scm 707 */
 uint64_t BgL_tmpz00_5460;
{ /* Llib/srfi4.scm 707 */
 obj_t BgL_vz00_4110; long BgL_kz00_4111;
if(
BGL_U64VECTORP(BgL_vz00_3128))
{ /* Llib/srfi4.scm 707 */
BgL_vz00_4110 = BgL_vz00_3128; }  else 
{ 
 obj_t BgL_auxz00_5463;
BgL_auxz00_5463 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28620L), BGl_string2980z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_vz00_3128); 
FAILURE(BgL_auxz00_5463,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 707 */
 obj_t BgL_tmpz00_5467;
if(
INTEGERP(BgL_kz00_3129))
{ /* Llib/srfi4.scm 707 */
BgL_tmpz00_5467 = BgL_kz00_3129
; }  else 
{ 
 obj_t BgL_auxz00_5470;
BgL_auxz00_5470 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28620L), BGl_string2980z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3129); 
FAILURE(BgL_auxz00_5470,BFALSE,BFALSE);} 
BgL_kz00_4111 = 
(long)CINT(BgL_tmpz00_5467); } 
{ /* Llib/srfi4.scm 707 */
 long BgL_l2132z00_4112;
BgL_l2132z00_4112 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4110); 
if(
BOUND_CHECK(BgL_kz00_4111, BgL_l2132z00_4112))
{ /* Llib/srfi4.scm 707 */
BgL_tmpz00_5460 = 
BGL_U64VREF(BgL_vz00_4110, BgL_kz00_4111)
; }  else 
{ 
 obj_t BgL_auxz00_5479;
BgL_auxz00_5479 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28620L), BGl_string2979z00zz__srfi4z00, BgL_vz00_4110, 
(int)(BgL_l2132z00_4112), 
(int)(BgL_kz00_4111)); 
FAILURE(BgL_auxz00_5479,BFALSE,BFALSE);} } } 
return 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_5460);} } 

}



/* f32vector-ref */
BGL_EXPORTED_DEF float BGl_f32vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_94, long BgL_kz00_95)
{
{ /* Llib/srfi4.scm 709 */
{ /* Llib/srfi4.scm 710 */
 long BgL_l2136z00_4113;
BgL_l2136z00_4113 = 
BGL_HVECTOR_LENGTH(BgL_vz00_94); 
if(
BOUND_CHECK(BgL_kz00_95, BgL_l2136z00_4113))
{ /* Llib/srfi4.scm 710 */
return 
BGL_F32VREF(BgL_vz00_94, BgL_kz00_95);}  else 
{ 
 obj_t BgL_auxz00_5490;
BgL_auxz00_5490 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28681L), BGl_string2981z00zz__srfi4z00, BgL_vz00_94, 
(int)(BgL_l2136z00_4113), 
(int)(BgL_kz00_95)); 
FAILURE(BgL_auxz00_5490,BFALSE,BFALSE);} } } 

}



/* &f32vector-ref */
obj_t BGl_z62f32vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3137, obj_t BgL_vz00_3138, obj_t BgL_kz00_3139)
{
{ /* Llib/srfi4.scm 709 */
{ /* Llib/srfi4.scm 710 */
 float BgL_tmpz00_5496;
{ /* Llib/srfi4.scm 710 */
 obj_t BgL_vz00_4114; long BgL_kz00_4115;
if(
BGL_F32VECTORP(BgL_vz00_3138))
{ /* Llib/srfi4.scm 710 */
BgL_vz00_4114 = BgL_vz00_3138; }  else 
{ 
 obj_t BgL_auxz00_5499;
BgL_auxz00_5499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28681L), BGl_string2982z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_vz00_3138); 
FAILURE(BgL_auxz00_5499,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 710 */
 obj_t BgL_tmpz00_5503;
if(
INTEGERP(BgL_kz00_3139))
{ /* Llib/srfi4.scm 710 */
BgL_tmpz00_5503 = BgL_kz00_3139
; }  else 
{ 
 obj_t BgL_auxz00_5506;
BgL_auxz00_5506 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28681L), BGl_string2982z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3139); 
FAILURE(BgL_auxz00_5506,BFALSE,BFALSE);} 
BgL_kz00_4115 = 
(long)CINT(BgL_tmpz00_5503); } 
{ /* Llib/srfi4.scm 710 */
 long BgL_l2136z00_4116;
BgL_l2136z00_4116 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4114); 
if(
BOUND_CHECK(BgL_kz00_4115, BgL_l2136z00_4116))
{ /* Llib/srfi4.scm 710 */
BgL_tmpz00_5496 = 
BGL_F32VREF(BgL_vz00_4114, BgL_kz00_4115)
; }  else 
{ 
 obj_t BgL_auxz00_5515;
BgL_auxz00_5515 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28681L), BGl_string2981z00zz__srfi4z00, BgL_vz00_4114, 
(int)(BgL_l2136z00_4116), 
(int)(BgL_kz00_4115)); 
FAILURE(BgL_auxz00_5515,BFALSE,BFALSE);} } } 
return 
FLOAT_TO_REAL(BgL_tmpz00_5496);} } 

}



/* f64vector-ref */
BGL_EXPORTED_DEF double BGl_f64vectorzd2refzd2zz__srfi4z00(obj_t BgL_vz00_96, long BgL_kz00_97)
{
{ /* Llib/srfi4.scm 712 */
{ /* Llib/srfi4.scm 713 */
 long BgL_l2140z00_4117;
BgL_l2140z00_4117 = 
BGL_HVECTOR_LENGTH(BgL_vz00_96); 
if(
BOUND_CHECK(BgL_kz00_97, BgL_l2140z00_4117))
{ /* Llib/srfi4.scm 713 */
return 
BGL_F64VREF(BgL_vz00_96, BgL_kz00_97);}  else 
{ 
 obj_t BgL_auxz00_5526;
BgL_auxz00_5526 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28742L), BGl_string2983z00zz__srfi4z00, BgL_vz00_96, 
(int)(BgL_l2140z00_4117), 
(int)(BgL_kz00_97)); 
FAILURE(BgL_auxz00_5526,BFALSE,BFALSE);} } } 

}



/* &f64vector-ref */
obj_t BGl_z62f64vectorzd2refzb0zz__srfi4z00(obj_t BgL_envz00_3147, obj_t BgL_vz00_3148, obj_t BgL_kz00_3149)
{
{ /* Llib/srfi4.scm 712 */
{ /* Llib/srfi4.scm 713 */
 double BgL_tmpz00_5532;
{ /* Llib/srfi4.scm 713 */
 obj_t BgL_vz00_4118; long BgL_kz00_4119;
if(
BGL_F64VECTORP(BgL_vz00_3148))
{ /* Llib/srfi4.scm 713 */
BgL_vz00_4118 = BgL_vz00_3148; }  else 
{ 
 obj_t BgL_auxz00_5535;
BgL_auxz00_5535 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28742L), BGl_string2984z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_vz00_3148); 
FAILURE(BgL_auxz00_5535,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 713 */
 obj_t BgL_tmpz00_5539;
if(
INTEGERP(BgL_kz00_3149))
{ /* Llib/srfi4.scm 713 */
BgL_tmpz00_5539 = BgL_kz00_3149
; }  else 
{ 
 obj_t BgL_auxz00_5542;
BgL_auxz00_5542 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28742L), BGl_string2984z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3149); 
FAILURE(BgL_auxz00_5542,BFALSE,BFALSE);} 
BgL_kz00_4119 = 
(long)CINT(BgL_tmpz00_5539); } 
{ /* Llib/srfi4.scm 713 */
 long BgL_l2140z00_4120;
BgL_l2140z00_4120 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4118); 
if(
BOUND_CHECK(BgL_kz00_4119, BgL_l2140z00_4120))
{ /* Llib/srfi4.scm 713 */
BgL_tmpz00_5532 = 
BGL_F64VREF(BgL_vz00_4118, BgL_kz00_4119)
; }  else 
{ 
 obj_t BgL_auxz00_5551;
BgL_auxz00_5551 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(28742L), BGl_string2983z00zz__srfi4z00, BgL_vz00_4118, 
(int)(BgL_l2140z00_4120), 
(int)(BgL_kz00_4119)); 
FAILURE(BgL_auxz00_5551,BFALSE,BFALSE);} } } 
return 
DOUBLE_TO_REAL(BgL_tmpz00_5532);} } 

}



/* s8vector-set! */
BGL_EXPORTED_DEF obj_t BGl_s8vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_100, long BgL_kz00_101, int8_t BgL_valz00_102)
{
{ /* Llib/srfi4.scm 721 */
{ /* Llib/srfi4.scm 722 */
 long BgL_l2144z00_4121;
BgL_l2144z00_4121 = 
BGL_HVECTOR_LENGTH(BgL_vz00_100); 
if(
BOUND_CHECK(BgL_kz00_101, BgL_l2144z00_4121))
{ /* Llib/srfi4.scm 722 */
BGL_S8VSET(BgL_vz00_100, BgL_kz00_101, BgL_valz00_102); }  else 
{ 
 obj_t BgL_auxz00_5562;
BgL_auxz00_5562 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29099L), BGl_string2916z00zz__srfi4z00, BgL_vz00_100, 
(int)(BgL_l2144z00_4121), 
(int)(BgL_kz00_101)); 
FAILURE(BgL_auxz00_5562,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &s8vector-set! */
obj_t BGl_z62s8vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3060, obj_t BgL_vz00_3061, obj_t BgL_kz00_3062, obj_t BgL_valz00_3063)
{
{ /* Llib/srfi4.scm 721 */
{ /* Llib/srfi4.scm 721 */
 obj_t BgL_vz00_4122; long BgL_kz00_4123; int8_t BgL_valz00_4124;
if(
BGL_S8VECTORP(BgL_vz00_3061))
{ /* Llib/srfi4.scm 721 */
BgL_vz00_4122 = BgL_vz00_3061; }  else 
{ 
 obj_t BgL_auxz00_5570;
BgL_auxz00_5570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29057L), BGl_string2985z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_vz00_3061); 
FAILURE(BgL_auxz00_5570,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 721 */
 obj_t BgL_tmpz00_5574;
if(
INTEGERP(BgL_kz00_3062))
{ /* Llib/srfi4.scm 721 */
BgL_tmpz00_5574 = BgL_kz00_3062
; }  else 
{ 
 obj_t BgL_auxz00_5577;
BgL_auxz00_5577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29057L), BGl_string2985z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3062); 
FAILURE(BgL_auxz00_5577,BFALSE,BFALSE);} 
BgL_kz00_4123 = 
(long)CINT(BgL_tmpz00_5574); } 
{ /* Llib/srfi4.scm 721 */
 obj_t BgL_tmpz00_5582;
if(
BGL_INT8P(BgL_valz00_3063))
{ /* Llib/srfi4.scm 721 */
BgL_tmpz00_5582 = BgL_valz00_3063
; }  else 
{ 
 obj_t BgL_auxz00_5585;
BgL_auxz00_5585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29057L), BGl_string2985z00zz__srfi4z00, BGl_string2915z00zz__srfi4z00, BgL_valz00_3063); 
FAILURE(BgL_auxz00_5585,BFALSE,BFALSE);} 
BgL_valz00_4124 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_5582); } 
{ /* Llib/srfi4.scm 722 */
 long BgL_l2144z00_4125;
BgL_l2144z00_4125 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4122); 
if(
BOUND_CHECK(BgL_kz00_4123, BgL_l2144z00_4125))
{ /* Llib/srfi4.scm 722 */
BGL_S8VSET(BgL_vz00_4122, BgL_kz00_4123, BgL_valz00_4124); }  else 
{ 
 obj_t BgL_auxz00_5594;
BgL_auxz00_5594 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29099L), BGl_string2916z00zz__srfi4z00, BgL_vz00_4122, 
(int)(BgL_l2144z00_4125), 
(int)(BgL_kz00_4123)); 
FAILURE(BgL_auxz00_5594,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* u8vector-set! */
BGL_EXPORTED_DEF obj_t BGl_u8vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_103, long BgL_kz00_104, uint8_t BgL_valz00_105)
{
{ /* Llib/srfi4.scm 724 */
{ /* Llib/srfi4.scm 725 */
 long BgL_l2148z00_4126;
BgL_l2148z00_4126 = 
BGL_HVECTOR_LENGTH(BgL_vz00_103); 
if(
BOUND_CHECK(BgL_kz00_104, BgL_l2148z00_4126))
{ /* Llib/srfi4.scm 725 */
BGL_U8VSET(BgL_vz00_103, BgL_kz00_104, BgL_valz00_105); }  else 
{ 
 obj_t BgL_auxz00_5604;
BgL_auxz00_5604 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29168L), BGl_string2921z00zz__srfi4z00, BgL_vz00_103, 
(int)(BgL_l2148z00_4126), 
(int)(BgL_kz00_104)); 
FAILURE(BgL_auxz00_5604,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &u8vector-set! */
obj_t BGl_z62u8vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3070, obj_t BgL_vz00_3071, obj_t BgL_kz00_3072, obj_t BgL_valz00_3073)
{
{ /* Llib/srfi4.scm 724 */
{ /* Llib/srfi4.scm 724 */
 obj_t BgL_vz00_4127; long BgL_kz00_4128; uint8_t BgL_valz00_4129;
if(
BGL_U8VECTORP(BgL_vz00_3071))
{ /* Llib/srfi4.scm 724 */
BgL_vz00_4127 = BgL_vz00_3071; }  else 
{ 
 obj_t BgL_auxz00_5612;
BgL_auxz00_5612 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29126L), BGl_string2986z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_vz00_3071); 
FAILURE(BgL_auxz00_5612,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 724 */
 obj_t BgL_tmpz00_5616;
if(
INTEGERP(BgL_kz00_3072))
{ /* Llib/srfi4.scm 724 */
BgL_tmpz00_5616 = BgL_kz00_3072
; }  else 
{ 
 obj_t BgL_auxz00_5619;
BgL_auxz00_5619 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29126L), BGl_string2986z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3072); 
FAILURE(BgL_auxz00_5619,BFALSE,BFALSE);} 
BgL_kz00_4128 = 
(long)CINT(BgL_tmpz00_5616); } 
{ /* Llib/srfi4.scm 724 */
 obj_t BgL_tmpz00_5624;
if(
BGL_UINT8P(BgL_valz00_3073))
{ /* Llib/srfi4.scm 724 */
BgL_tmpz00_5624 = BgL_valz00_3073
; }  else 
{ 
 obj_t BgL_auxz00_5627;
BgL_auxz00_5627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29126L), BGl_string2986z00zz__srfi4z00, BGl_string2920z00zz__srfi4z00, BgL_valz00_3073); 
FAILURE(BgL_auxz00_5627,BFALSE,BFALSE);} 
BgL_valz00_4129 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_5624); } 
{ /* Llib/srfi4.scm 725 */
 long BgL_l2148z00_4130;
BgL_l2148z00_4130 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4127); 
if(
BOUND_CHECK(BgL_kz00_4128, BgL_l2148z00_4130))
{ /* Llib/srfi4.scm 725 */
BGL_U8VSET(BgL_vz00_4127, BgL_kz00_4128, BgL_valz00_4129); }  else 
{ 
 obj_t BgL_auxz00_5636;
BgL_auxz00_5636 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29168L), BGl_string2921z00zz__srfi4z00, BgL_vz00_4127, 
(int)(BgL_l2148z00_4130), 
(int)(BgL_kz00_4128)); 
FAILURE(BgL_auxz00_5636,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* s16vector-set! */
BGL_EXPORTED_DEF obj_t BGl_s16vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_106, long BgL_kz00_107, int16_t BgL_valz00_108)
{
{ /* Llib/srfi4.scm 727 */
{ /* Llib/srfi4.scm 728 */
 long BgL_l2152z00_4131;
BgL_l2152z00_4131 = 
BGL_HVECTOR_LENGTH(BgL_vz00_106); 
if(
BOUND_CHECK(BgL_kz00_107, BgL_l2152z00_4131))
{ /* Llib/srfi4.scm 728 */
BGL_S16VSET(BgL_vz00_106, BgL_kz00_107, BgL_valz00_108); }  else 
{ 
 obj_t BgL_auxz00_5646;
BgL_auxz00_5646 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29238L), BGl_string2926z00zz__srfi4z00, BgL_vz00_106, 
(int)(BgL_l2152z00_4131), 
(int)(BgL_kz00_107)); 
FAILURE(BgL_auxz00_5646,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &s16vector-set! */
obj_t BGl_z62s16vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3080, obj_t BgL_vz00_3081, obj_t BgL_kz00_3082, obj_t BgL_valz00_3083)
{
{ /* Llib/srfi4.scm 727 */
{ /* Llib/srfi4.scm 727 */
 obj_t BgL_vz00_4132; long BgL_kz00_4133; int16_t BgL_valz00_4134;
if(
BGL_S16VECTORP(BgL_vz00_3081))
{ /* Llib/srfi4.scm 727 */
BgL_vz00_4132 = BgL_vz00_3081; }  else 
{ 
 obj_t BgL_auxz00_5654;
BgL_auxz00_5654 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29195L), BGl_string2987z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_vz00_3081); 
FAILURE(BgL_auxz00_5654,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 727 */
 obj_t BgL_tmpz00_5658;
if(
INTEGERP(BgL_kz00_3082))
{ /* Llib/srfi4.scm 727 */
BgL_tmpz00_5658 = BgL_kz00_3082
; }  else 
{ 
 obj_t BgL_auxz00_5661;
BgL_auxz00_5661 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29195L), BGl_string2987z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3082); 
FAILURE(BgL_auxz00_5661,BFALSE,BFALSE);} 
BgL_kz00_4133 = 
(long)CINT(BgL_tmpz00_5658); } 
{ /* Llib/srfi4.scm 727 */
 obj_t BgL_tmpz00_5666;
if(
BGL_INT16P(BgL_valz00_3083))
{ /* Llib/srfi4.scm 727 */
BgL_tmpz00_5666 = BgL_valz00_3083
; }  else 
{ 
 obj_t BgL_auxz00_5669;
BgL_auxz00_5669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29195L), BGl_string2987z00zz__srfi4z00, BGl_string2925z00zz__srfi4z00, BgL_valz00_3083); 
FAILURE(BgL_auxz00_5669,BFALSE,BFALSE);} 
BgL_valz00_4134 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_5666); } 
{ /* Llib/srfi4.scm 728 */
 long BgL_l2152z00_4135;
BgL_l2152z00_4135 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4132); 
if(
BOUND_CHECK(BgL_kz00_4133, BgL_l2152z00_4135))
{ /* Llib/srfi4.scm 728 */
BGL_S16VSET(BgL_vz00_4132, BgL_kz00_4133, BgL_valz00_4134); }  else 
{ 
 obj_t BgL_auxz00_5678;
BgL_auxz00_5678 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29238L), BGl_string2926z00zz__srfi4z00, BgL_vz00_4132, 
(int)(BgL_l2152z00_4135), 
(int)(BgL_kz00_4133)); 
FAILURE(BgL_auxz00_5678,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* u16vector-set! */
BGL_EXPORTED_DEF obj_t BGl_u16vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_109, long BgL_kz00_110, uint16_t BgL_valz00_111)
{
{ /* Llib/srfi4.scm 730 */
{ /* Llib/srfi4.scm 731 */
 long BgL_l2156z00_4136;
BgL_l2156z00_4136 = 
BGL_HVECTOR_LENGTH(BgL_vz00_109); 
if(
BOUND_CHECK(BgL_kz00_110, BgL_l2156z00_4136))
{ /* Llib/srfi4.scm 731 */
BGL_U16VSET(BgL_vz00_109, BgL_kz00_110, BgL_valz00_111); }  else 
{ 
 obj_t BgL_auxz00_5688;
BgL_auxz00_5688 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29309L), BGl_string2931z00zz__srfi4z00, BgL_vz00_109, 
(int)(BgL_l2156z00_4136), 
(int)(BgL_kz00_110)); 
FAILURE(BgL_auxz00_5688,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &u16vector-set! */
obj_t BGl_z62u16vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3090, obj_t BgL_vz00_3091, obj_t BgL_kz00_3092, obj_t BgL_valz00_3093)
{
{ /* Llib/srfi4.scm 730 */
{ /* Llib/srfi4.scm 730 */
 obj_t BgL_vz00_4137; long BgL_kz00_4138; uint16_t BgL_valz00_4139;
if(
BGL_U16VECTORP(BgL_vz00_3091))
{ /* Llib/srfi4.scm 730 */
BgL_vz00_4137 = BgL_vz00_3091; }  else 
{ 
 obj_t BgL_auxz00_5696;
BgL_auxz00_5696 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29266L), BGl_string2988z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_vz00_3091); 
FAILURE(BgL_auxz00_5696,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 730 */
 obj_t BgL_tmpz00_5700;
if(
INTEGERP(BgL_kz00_3092))
{ /* Llib/srfi4.scm 730 */
BgL_tmpz00_5700 = BgL_kz00_3092
; }  else 
{ 
 obj_t BgL_auxz00_5703;
BgL_auxz00_5703 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29266L), BGl_string2988z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3092); 
FAILURE(BgL_auxz00_5703,BFALSE,BFALSE);} 
BgL_kz00_4138 = 
(long)CINT(BgL_tmpz00_5700); } 
{ /* Llib/srfi4.scm 730 */
 obj_t BgL_tmpz00_5708;
if(
BGL_UINT16P(BgL_valz00_3093))
{ /* Llib/srfi4.scm 730 */
BgL_tmpz00_5708 = BgL_valz00_3093
; }  else 
{ 
 obj_t BgL_auxz00_5711;
BgL_auxz00_5711 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29266L), BGl_string2988z00zz__srfi4z00, BGl_string2930z00zz__srfi4z00, BgL_valz00_3093); 
FAILURE(BgL_auxz00_5711,BFALSE,BFALSE);} 
BgL_valz00_4139 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_5708); } 
{ /* Llib/srfi4.scm 731 */
 long BgL_l2156z00_4140;
BgL_l2156z00_4140 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4137); 
if(
BOUND_CHECK(BgL_kz00_4138, BgL_l2156z00_4140))
{ /* Llib/srfi4.scm 731 */
BGL_U16VSET(BgL_vz00_4137, BgL_kz00_4138, BgL_valz00_4139); }  else 
{ 
 obj_t BgL_auxz00_5720;
BgL_auxz00_5720 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29309L), BGl_string2931z00zz__srfi4z00, BgL_vz00_4137, 
(int)(BgL_l2156z00_4140), 
(int)(BgL_kz00_4138)); 
FAILURE(BgL_auxz00_5720,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* s32vector-set! */
BGL_EXPORTED_DEF obj_t BGl_s32vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_112, long BgL_kz00_113, int32_t BgL_valz00_114)
{
{ /* Llib/srfi4.scm 733 */
{ /* Llib/srfi4.scm 734 */
 long BgL_l2160z00_4141;
BgL_l2160z00_4141 = 
BGL_HVECTOR_LENGTH(BgL_vz00_112); 
if(
BOUND_CHECK(BgL_kz00_113, BgL_l2160z00_4141))
{ /* Llib/srfi4.scm 734 */
BGL_S32VSET(BgL_vz00_112, BgL_kz00_113, BgL_valz00_114); }  else 
{ 
 obj_t BgL_auxz00_5730;
BgL_auxz00_5730 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29380L), BGl_string2936z00zz__srfi4z00, BgL_vz00_112, 
(int)(BgL_l2160z00_4141), 
(int)(BgL_kz00_113)); 
FAILURE(BgL_auxz00_5730,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &s32vector-set! */
obj_t BGl_z62s32vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3100, obj_t BgL_vz00_3101, obj_t BgL_kz00_3102, obj_t BgL_valz00_3103)
{
{ /* Llib/srfi4.scm 733 */
{ /* Llib/srfi4.scm 733 */
 obj_t BgL_vz00_4142; long BgL_kz00_4143; int32_t BgL_valz00_4144;
if(
BGL_S32VECTORP(BgL_vz00_3101))
{ /* Llib/srfi4.scm 733 */
BgL_vz00_4142 = BgL_vz00_3101; }  else 
{ 
 obj_t BgL_auxz00_5738;
BgL_auxz00_5738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29337L), BGl_string2989z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_vz00_3101); 
FAILURE(BgL_auxz00_5738,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 733 */
 obj_t BgL_tmpz00_5742;
if(
INTEGERP(BgL_kz00_3102))
{ /* Llib/srfi4.scm 733 */
BgL_tmpz00_5742 = BgL_kz00_3102
; }  else 
{ 
 obj_t BgL_auxz00_5745;
BgL_auxz00_5745 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29337L), BGl_string2989z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3102); 
FAILURE(BgL_auxz00_5745,BFALSE,BFALSE);} 
BgL_kz00_4143 = 
(long)CINT(BgL_tmpz00_5742); } 
{ /* Llib/srfi4.scm 733 */
 obj_t BgL_tmpz00_5750;
if(
BGL_INT32P(BgL_valz00_3103))
{ /* Llib/srfi4.scm 733 */
BgL_tmpz00_5750 = BgL_valz00_3103
; }  else 
{ 
 obj_t BgL_auxz00_5753;
BgL_auxz00_5753 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29337L), BGl_string2989z00zz__srfi4z00, BGl_string2935z00zz__srfi4z00, BgL_valz00_3103); 
FAILURE(BgL_auxz00_5753,BFALSE,BFALSE);} 
BgL_valz00_4144 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_5750); } 
{ /* Llib/srfi4.scm 734 */
 long BgL_l2160z00_4145;
BgL_l2160z00_4145 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4142); 
if(
BOUND_CHECK(BgL_kz00_4143, BgL_l2160z00_4145))
{ /* Llib/srfi4.scm 734 */
BGL_S32VSET(BgL_vz00_4142, BgL_kz00_4143, BgL_valz00_4144); }  else 
{ 
 obj_t BgL_auxz00_5762;
BgL_auxz00_5762 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29380L), BGl_string2936z00zz__srfi4z00, BgL_vz00_4142, 
(int)(BgL_l2160z00_4145), 
(int)(BgL_kz00_4143)); 
FAILURE(BgL_auxz00_5762,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* u32vector-set! */
BGL_EXPORTED_DEF obj_t BGl_u32vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_115, long BgL_kz00_116, uint32_t BgL_valz00_117)
{
{ /* Llib/srfi4.scm 736 */
{ /* Llib/srfi4.scm 737 */
 long BgL_l2164z00_4146;
BgL_l2164z00_4146 = 
BGL_HVECTOR_LENGTH(BgL_vz00_115); 
if(
BOUND_CHECK(BgL_kz00_116, BgL_l2164z00_4146))
{ /* Llib/srfi4.scm 737 */
BGL_U32VSET(BgL_vz00_115, BgL_kz00_116, BgL_valz00_117); }  else 
{ 
 obj_t BgL_auxz00_5772;
BgL_auxz00_5772 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29451L), BGl_string2941z00zz__srfi4z00, BgL_vz00_115, 
(int)(BgL_l2164z00_4146), 
(int)(BgL_kz00_116)); 
FAILURE(BgL_auxz00_5772,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &u32vector-set! */
obj_t BGl_z62u32vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3110, obj_t BgL_vz00_3111, obj_t BgL_kz00_3112, obj_t BgL_valz00_3113)
{
{ /* Llib/srfi4.scm 736 */
{ /* Llib/srfi4.scm 736 */
 obj_t BgL_vz00_4147; long BgL_kz00_4148; uint32_t BgL_valz00_4149;
if(
BGL_U32VECTORP(BgL_vz00_3111))
{ /* Llib/srfi4.scm 736 */
BgL_vz00_4147 = BgL_vz00_3111; }  else 
{ 
 obj_t BgL_auxz00_5780;
BgL_auxz00_5780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29408L), BGl_string2990z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_vz00_3111); 
FAILURE(BgL_auxz00_5780,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 736 */
 obj_t BgL_tmpz00_5784;
if(
INTEGERP(BgL_kz00_3112))
{ /* Llib/srfi4.scm 736 */
BgL_tmpz00_5784 = BgL_kz00_3112
; }  else 
{ 
 obj_t BgL_auxz00_5787;
BgL_auxz00_5787 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29408L), BGl_string2990z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3112); 
FAILURE(BgL_auxz00_5787,BFALSE,BFALSE);} 
BgL_kz00_4148 = 
(long)CINT(BgL_tmpz00_5784); } 
{ /* Llib/srfi4.scm 736 */
 obj_t BgL_tmpz00_5792;
if(
BGL_UINT32P(BgL_valz00_3113))
{ /* Llib/srfi4.scm 736 */
BgL_tmpz00_5792 = BgL_valz00_3113
; }  else 
{ 
 obj_t BgL_auxz00_5795;
BgL_auxz00_5795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29408L), BGl_string2990z00zz__srfi4z00, BGl_string2940z00zz__srfi4z00, BgL_valz00_3113); 
FAILURE(BgL_auxz00_5795,BFALSE,BFALSE);} 
BgL_valz00_4149 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_5792); } 
{ /* Llib/srfi4.scm 737 */
 long BgL_l2164z00_4150;
BgL_l2164z00_4150 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4147); 
if(
BOUND_CHECK(BgL_kz00_4148, BgL_l2164z00_4150))
{ /* Llib/srfi4.scm 737 */
BGL_U32VSET(BgL_vz00_4147, BgL_kz00_4148, BgL_valz00_4149); }  else 
{ 
 obj_t BgL_auxz00_5804;
BgL_auxz00_5804 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29451L), BGl_string2941z00zz__srfi4z00, BgL_vz00_4147, 
(int)(BgL_l2164z00_4150), 
(int)(BgL_kz00_4148)); 
FAILURE(BgL_auxz00_5804,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* s64vector-set! */
BGL_EXPORTED_DEF obj_t BGl_s64vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_118, long BgL_kz00_119, int64_t BgL_valz00_120)
{
{ /* Llib/srfi4.scm 739 */
{ /* Llib/srfi4.scm 740 */
 long BgL_l2168z00_4151;
BgL_l2168z00_4151 = 
BGL_HVECTOR_LENGTH(BgL_vz00_118); 
if(
BOUND_CHECK(BgL_kz00_119, BgL_l2168z00_4151))
{ /* Llib/srfi4.scm 740 */
BGL_S64VSET(BgL_vz00_118, BgL_kz00_119, BgL_valz00_120); }  else 
{ 
 obj_t BgL_auxz00_5814;
BgL_auxz00_5814 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29522L), BGl_string2946z00zz__srfi4z00, BgL_vz00_118, 
(int)(BgL_l2168z00_4151), 
(int)(BgL_kz00_119)); 
FAILURE(BgL_auxz00_5814,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &s64vector-set! */
obj_t BGl_z62s64vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3120, obj_t BgL_vz00_3121, obj_t BgL_kz00_3122, obj_t BgL_valz00_3123)
{
{ /* Llib/srfi4.scm 739 */
{ /* Llib/srfi4.scm 739 */
 obj_t BgL_vz00_4152; long BgL_kz00_4153; int64_t BgL_valz00_4154;
if(
BGL_S64VECTORP(BgL_vz00_3121))
{ /* Llib/srfi4.scm 739 */
BgL_vz00_4152 = BgL_vz00_3121; }  else 
{ 
 obj_t BgL_auxz00_5822;
BgL_auxz00_5822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29479L), BGl_string2991z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_vz00_3121); 
FAILURE(BgL_auxz00_5822,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 739 */
 obj_t BgL_tmpz00_5826;
if(
INTEGERP(BgL_kz00_3122))
{ /* Llib/srfi4.scm 739 */
BgL_tmpz00_5826 = BgL_kz00_3122
; }  else 
{ 
 obj_t BgL_auxz00_5829;
BgL_auxz00_5829 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29479L), BGl_string2991z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3122); 
FAILURE(BgL_auxz00_5829,BFALSE,BFALSE);} 
BgL_kz00_4153 = 
(long)CINT(BgL_tmpz00_5826); } 
{ /* Llib/srfi4.scm 739 */
 obj_t BgL_tmpz00_5834;
if(
BGL_INT64P(BgL_valz00_3123))
{ /* Llib/srfi4.scm 739 */
BgL_tmpz00_5834 = BgL_valz00_3123
; }  else 
{ 
 obj_t BgL_auxz00_5837;
BgL_auxz00_5837 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29479L), BGl_string2991z00zz__srfi4z00, BGl_string2945z00zz__srfi4z00, BgL_valz00_3123); 
FAILURE(BgL_auxz00_5837,BFALSE,BFALSE);} 
BgL_valz00_4154 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5834); } 
{ /* Llib/srfi4.scm 740 */
 long BgL_l2168z00_4155;
BgL_l2168z00_4155 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4152); 
if(
BOUND_CHECK(BgL_kz00_4153, BgL_l2168z00_4155))
{ /* Llib/srfi4.scm 740 */
BGL_S64VSET(BgL_vz00_4152, BgL_kz00_4153, BgL_valz00_4154); }  else 
{ 
 obj_t BgL_auxz00_5846;
BgL_auxz00_5846 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29522L), BGl_string2946z00zz__srfi4z00, BgL_vz00_4152, 
(int)(BgL_l2168z00_4155), 
(int)(BgL_kz00_4153)); 
FAILURE(BgL_auxz00_5846,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* u64vector-set! */
BGL_EXPORTED_DEF obj_t BGl_u64vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_121, long BgL_kz00_122, uint64_t BgL_valz00_123)
{
{ /* Llib/srfi4.scm 742 */
{ /* Llib/srfi4.scm 743 */
 long BgL_l2172z00_4156;
BgL_l2172z00_4156 = 
BGL_HVECTOR_LENGTH(BgL_vz00_121); 
if(
BOUND_CHECK(BgL_kz00_122, BgL_l2172z00_4156))
{ /* Llib/srfi4.scm 743 */
BGL_U64VSET(BgL_vz00_121, BgL_kz00_122, BgL_valz00_123); }  else 
{ 
 obj_t BgL_auxz00_5856;
BgL_auxz00_5856 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29593L), BGl_string2951z00zz__srfi4z00, BgL_vz00_121, 
(int)(BgL_l2172z00_4156), 
(int)(BgL_kz00_122)); 
FAILURE(BgL_auxz00_5856,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &u64vector-set! */
obj_t BGl_z62u64vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3130, obj_t BgL_vz00_3131, obj_t BgL_kz00_3132, obj_t BgL_valz00_3133)
{
{ /* Llib/srfi4.scm 742 */
{ /* Llib/srfi4.scm 742 */
 obj_t BgL_vz00_4157; long BgL_kz00_4158; uint64_t BgL_valz00_4159;
if(
BGL_U64VECTORP(BgL_vz00_3131))
{ /* Llib/srfi4.scm 742 */
BgL_vz00_4157 = BgL_vz00_3131; }  else 
{ 
 obj_t BgL_auxz00_5864;
BgL_auxz00_5864 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29550L), BGl_string2992z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_vz00_3131); 
FAILURE(BgL_auxz00_5864,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 742 */
 obj_t BgL_tmpz00_5868;
if(
INTEGERP(BgL_kz00_3132))
{ /* Llib/srfi4.scm 742 */
BgL_tmpz00_5868 = BgL_kz00_3132
; }  else 
{ 
 obj_t BgL_auxz00_5871;
BgL_auxz00_5871 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29550L), BGl_string2992z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3132); 
FAILURE(BgL_auxz00_5871,BFALSE,BFALSE);} 
BgL_kz00_4158 = 
(long)CINT(BgL_tmpz00_5868); } 
{ /* Llib/srfi4.scm 742 */
 obj_t BgL_tmpz00_5876;
if(
BGL_UINT64P(BgL_valz00_3133))
{ /* Llib/srfi4.scm 742 */
BgL_tmpz00_5876 = BgL_valz00_3133
; }  else 
{ 
 obj_t BgL_auxz00_5879;
BgL_auxz00_5879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29550L), BGl_string2992z00zz__srfi4z00, BGl_string2950z00zz__srfi4z00, BgL_valz00_3133); 
FAILURE(BgL_auxz00_5879,BFALSE,BFALSE);} 
BgL_valz00_4159 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_5876); } 
{ /* Llib/srfi4.scm 743 */
 long BgL_l2172z00_4160;
BgL_l2172z00_4160 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4157); 
if(
BOUND_CHECK(BgL_kz00_4158, BgL_l2172z00_4160))
{ /* Llib/srfi4.scm 743 */
BGL_U64VSET(BgL_vz00_4157, BgL_kz00_4158, BgL_valz00_4159); }  else 
{ 
 obj_t BgL_auxz00_5888;
BgL_auxz00_5888 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29593L), BGl_string2951z00zz__srfi4z00, BgL_vz00_4157, 
(int)(BgL_l2172z00_4160), 
(int)(BgL_kz00_4158)); 
FAILURE(BgL_auxz00_5888,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* f32vector-set! */
BGL_EXPORTED_DEF obj_t BGl_f32vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_124, long BgL_kz00_125, float BgL_valz00_126)
{
{ /* Llib/srfi4.scm 745 */
{ /* Llib/srfi4.scm 746 */
 long BgL_l2176z00_4161;
BgL_l2176z00_4161 = 
BGL_HVECTOR_LENGTH(BgL_vz00_124); 
if(
BOUND_CHECK(BgL_kz00_125, BgL_l2176z00_4161))
{ /* Llib/srfi4.scm 746 */
BGL_F32VSET(BgL_vz00_124, BgL_kz00_125, BgL_valz00_126); }  else 
{ 
 obj_t BgL_auxz00_5898;
BgL_auxz00_5898 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29664L), BGl_string2956z00zz__srfi4z00, BgL_vz00_124, 
(int)(BgL_l2176z00_4161), 
(int)(BgL_kz00_125)); 
FAILURE(BgL_auxz00_5898,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &f32vector-set! */
obj_t BGl_z62f32vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3140, obj_t BgL_vz00_3141, obj_t BgL_kz00_3142, obj_t BgL_valz00_3143)
{
{ /* Llib/srfi4.scm 745 */
{ /* Llib/srfi4.scm 745 */
 obj_t BgL_vz00_4162; long BgL_kz00_4163; float BgL_valz00_4164;
if(
BGL_F32VECTORP(BgL_vz00_3141))
{ /* Llib/srfi4.scm 745 */
BgL_vz00_4162 = BgL_vz00_3141; }  else 
{ 
 obj_t BgL_auxz00_5906;
BgL_auxz00_5906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29621L), BGl_string2993z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_vz00_3141); 
FAILURE(BgL_auxz00_5906,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 745 */
 obj_t BgL_tmpz00_5910;
if(
INTEGERP(BgL_kz00_3142))
{ /* Llib/srfi4.scm 745 */
BgL_tmpz00_5910 = BgL_kz00_3142
; }  else 
{ 
 obj_t BgL_auxz00_5913;
BgL_auxz00_5913 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29621L), BGl_string2993z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3142); 
FAILURE(BgL_auxz00_5913,BFALSE,BFALSE);} 
BgL_kz00_4163 = 
(long)CINT(BgL_tmpz00_5910); } 
{ /* Llib/srfi4.scm 745 */
 obj_t BgL_tmpz00_5918;
if(
REALP(BgL_valz00_3143))
{ /* Llib/srfi4.scm 745 */
BgL_tmpz00_5918 = BgL_valz00_3143
; }  else 
{ 
 obj_t BgL_auxz00_5921;
BgL_auxz00_5921 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29621L), BGl_string2993z00zz__srfi4z00, BGl_string2955z00zz__srfi4z00, BgL_valz00_3143); 
FAILURE(BgL_auxz00_5921,BFALSE,BFALSE);} 
BgL_valz00_4164 = 
REAL_TO_FLOAT(BgL_tmpz00_5918); } 
{ /* Llib/srfi4.scm 746 */
 long BgL_l2176z00_4165;
BgL_l2176z00_4165 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4162); 
if(
BOUND_CHECK(BgL_kz00_4163, BgL_l2176z00_4165))
{ /* Llib/srfi4.scm 746 */
BGL_F32VSET(BgL_vz00_4162, BgL_kz00_4163, BgL_valz00_4164); }  else 
{ 
 obj_t BgL_auxz00_5930;
BgL_auxz00_5930 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29664L), BGl_string2956z00zz__srfi4z00, BgL_vz00_4162, 
(int)(BgL_l2176z00_4165), 
(int)(BgL_kz00_4163)); 
FAILURE(BgL_auxz00_5930,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* f64vector-set! */
BGL_EXPORTED_DEF obj_t BGl_f64vectorzd2setz12zc0zz__srfi4z00(obj_t BgL_vz00_127, long BgL_kz00_128, double BgL_valz00_129)
{
{ /* Llib/srfi4.scm 748 */
{ /* Llib/srfi4.scm 749 */
 long BgL_l2180z00_4166;
BgL_l2180z00_4166 = 
BGL_HVECTOR_LENGTH(BgL_vz00_127); 
if(
BOUND_CHECK(BgL_kz00_128, BgL_l2180z00_4166))
{ /* Llib/srfi4.scm 749 */
BGL_F64VSET(BgL_vz00_127, BgL_kz00_128, BgL_valz00_129); }  else 
{ 
 obj_t BgL_auxz00_5940;
BgL_auxz00_5940 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29735L), BGl_string2960z00zz__srfi4z00, BgL_vz00_127, 
(int)(BgL_l2180z00_4166), 
(int)(BgL_kz00_128)); 
FAILURE(BgL_auxz00_5940,BFALSE,BFALSE);} } 
return BUNSPEC;} 

}



/* &f64vector-set! */
obj_t BGl_z62f64vectorzd2setz12za2zz__srfi4z00(obj_t BgL_envz00_3150, obj_t BgL_vz00_3151, obj_t BgL_kz00_3152, obj_t BgL_valz00_3153)
{
{ /* Llib/srfi4.scm 748 */
{ /* Llib/srfi4.scm 748 */
 obj_t BgL_vz00_4167; long BgL_kz00_4168; double BgL_valz00_4169;
if(
BGL_F64VECTORP(BgL_vz00_3151))
{ /* Llib/srfi4.scm 748 */
BgL_vz00_4167 = BgL_vz00_3151; }  else 
{ 
 obj_t BgL_auxz00_5948;
BgL_auxz00_5948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29692L), BGl_string2994z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_vz00_3151); 
FAILURE(BgL_auxz00_5948,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 748 */
 obj_t BgL_tmpz00_5952;
if(
INTEGERP(BgL_kz00_3152))
{ /* Llib/srfi4.scm 748 */
BgL_tmpz00_5952 = BgL_kz00_3152
; }  else 
{ 
 obj_t BgL_auxz00_5955;
BgL_auxz00_5955 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29692L), BGl_string2994z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_kz00_3152); 
FAILURE(BgL_auxz00_5955,BFALSE,BFALSE);} 
BgL_kz00_4168 = 
(long)CINT(BgL_tmpz00_5952); } 
{ /* Llib/srfi4.scm 748 */
 obj_t BgL_tmpz00_5960;
if(
REALP(BgL_valz00_3153))
{ /* Llib/srfi4.scm 748 */
BgL_tmpz00_5960 = BgL_valz00_3153
; }  else 
{ 
 obj_t BgL_auxz00_5963;
BgL_auxz00_5963 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29692L), BGl_string2994z00zz__srfi4z00, BGl_string2955z00zz__srfi4z00, BgL_valz00_3153); 
FAILURE(BgL_auxz00_5963,BFALSE,BFALSE);} 
BgL_valz00_4169 = 
REAL_TO_DOUBLE(BgL_tmpz00_5960); } 
{ /* Llib/srfi4.scm 749 */
 long BgL_l2180z00_4170;
BgL_l2180z00_4170 = 
BGL_HVECTOR_LENGTH(BgL_vz00_4167); 
if(
BOUND_CHECK(BgL_kz00_4168, BgL_l2180z00_4170))
{ /* Llib/srfi4.scm 749 */
BGL_F64VSET(BgL_vz00_4167, BgL_kz00_4168, BgL_valz00_4169); }  else 
{ 
 obj_t BgL_auxz00_5972;
BgL_auxz00_5972 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(29735L), BGl_string2960z00zz__srfi4z00, BgL_vz00_4167, 
(int)(BgL_l2180z00_4170), 
(int)(BgL_kz00_4168)); 
FAILURE(BgL_auxz00_5972,BFALSE,BFALSE);} } 
return BUNSPEC;} } 

}



/* s8vector->list */
BGL_EXPORTED_DEF obj_t BGl_s8vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_133)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1063z00_1531;
BgL_g1063z00_1531 = 
BGL_HVECTOR_LENGTH(BgL_xz00_133); 
{ 
 long BgL_iz00_1534; obj_t BgL_resz00_1535;
BgL_iz00_1534 = BgL_g1063z00_1531; 
BgL_resz00_1535 = BNIL; 
BgL_zc3z04anonymousza31456ze3z87_1536:
if(
(BgL_iz00_1534==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1535;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1538;
BgL_niz00_1538 = 
(BgL_iz00_1534-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1458z00_1539;
{ /* Llib/srfi4.scm 771 */
 int8_t BgL_arg1459z00_1540;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2184z00_3360;
BgL_l2184z00_3360 = 
BGL_HVECTOR_LENGTH(BgL_xz00_133); 
if(
BOUND_CHECK(BgL_niz00_1538, BgL_l2184z00_3360))
{ /* Llib/srfi4.scm 771 */
BgL_arg1459z00_1540 = 
BGL_S8VREF(BgL_xz00_133, BgL_niz00_1538); }  else 
{ 
 obj_t BgL_auxz00_5986;
BgL_auxz00_5986 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2965z00zz__srfi4z00, BgL_xz00_133, 
(int)(BgL_l2184z00_3360), 
(int)(BgL_niz00_1538)); 
FAILURE(BgL_auxz00_5986,BFALSE,BFALSE);} } 
BgL_arg1458z00_1539 = 
MAKE_YOUNG_PAIR(
BGL_INT8_TO_BINT8(BgL_arg1459z00_1540), BgL_resz00_1535); } 
{ 
 obj_t BgL_resz00_5995; long BgL_iz00_5994;
BgL_iz00_5994 = BgL_niz00_1538; 
BgL_resz00_5995 = BgL_arg1458z00_1539; 
BgL_resz00_1535 = BgL_resz00_5995; 
BgL_iz00_1534 = BgL_iz00_5994; 
goto BgL_zc3z04anonymousza31456ze3z87_1536;} } } } } } 

}



/* &s8vector->list */
obj_t BGl_z62s8vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3198, obj_t BgL_xz00_3199)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_5996;
if(
BGL_S8VECTORP(BgL_xz00_3199))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_5996 = BgL_xz00_3199
; }  else 
{ 
 obj_t BgL_auxz00_5999;
BgL_auxz00_5999 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2995z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_xz00_3199); 
FAILURE(BgL_auxz00_5999,BFALSE,BFALSE);} 
return 
BGl_s8vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_5996);} } 

}



/* u8vector->list */
BGL_EXPORTED_DEF obj_t BGl_u8vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_134)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1065z00_1542;
BgL_g1065z00_1542 = 
BGL_HVECTOR_LENGTH(BgL_xz00_134); 
{ 
 long BgL_iz00_1545; obj_t BgL_resz00_1546;
BgL_iz00_1545 = BgL_g1065z00_1542; 
BgL_resz00_1546 = BNIL; 
BgL_zc3z04anonymousza31460ze3z87_1547:
if(
(BgL_iz00_1545==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1546;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1549;
BgL_niz00_1549 = 
(BgL_iz00_1545-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1462z00_1550;
{ /* Llib/srfi4.scm 771 */
 uint8_t BgL_arg1463z00_1551;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2188z00_3364;
BgL_l2188z00_3364 = 
BGL_HVECTOR_LENGTH(BgL_xz00_134); 
if(
BOUND_CHECK(BgL_niz00_1549, BgL_l2188z00_3364))
{ /* Llib/srfi4.scm 771 */
BgL_arg1463z00_1551 = 
BGL_U8VREF(BgL_xz00_134, BgL_niz00_1549); }  else 
{ 
 obj_t BgL_auxz00_6012;
BgL_auxz00_6012 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2967z00zz__srfi4z00, BgL_xz00_134, 
(int)(BgL_l2188z00_3364), 
(int)(BgL_niz00_1549)); 
FAILURE(BgL_auxz00_6012,BFALSE,BFALSE);} } 
BgL_arg1462z00_1550 = 
MAKE_YOUNG_PAIR(
BGL_UINT8_TO_BUINT8(BgL_arg1463z00_1551), BgL_resz00_1546); } 
{ 
 obj_t BgL_resz00_6021; long BgL_iz00_6020;
BgL_iz00_6020 = BgL_niz00_1549; 
BgL_resz00_6021 = BgL_arg1462z00_1550; 
BgL_resz00_1546 = BgL_resz00_6021; 
BgL_iz00_1545 = BgL_iz00_6020; 
goto BgL_zc3z04anonymousza31460ze3z87_1547;} } } } } } 

}



/* &u8vector->list */
obj_t BGl_z62u8vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3200, obj_t BgL_xz00_3201)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6022;
if(
BGL_U8VECTORP(BgL_xz00_3201))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6022 = BgL_xz00_3201
; }  else 
{ 
 obj_t BgL_auxz00_6025;
BgL_auxz00_6025 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2996z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_xz00_3201); 
FAILURE(BgL_auxz00_6025,BFALSE,BFALSE);} 
return 
BGl_u8vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6022);} } 

}



/* s16vector->list */
BGL_EXPORTED_DEF obj_t BGl_s16vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_135)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1067z00_1553;
BgL_g1067z00_1553 = 
BGL_HVECTOR_LENGTH(BgL_xz00_135); 
{ 
 long BgL_iz00_1556; obj_t BgL_resz00_1557;
BgL_iz00_1556 = BgL_g1067z00_1553; 
BgL_resz00_1557 = BNIL; 
BgL_zc3z04anonymousza31464ze3z87_1558:
if(
(BgL_iz00_1556==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1557;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1560;
BgL_niz00_1560 = 
(BgL_iz00_1556-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1466z00_1561;
{ /* Llib/srfi4.scm 771 */
 int16_t BgL_arg1467z00_1562;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2192z00_3368;
BgL_l2192z00_3368 = 
BGL_HVECTOR_LENGTH(BgL_xz00_135); 
if(
BOUND_CHECK(BgL_niz00_1560, BgL_l2192z00_3368))
{ /* Llib/srfi4.scm 771 */
BgL_arg1467z00_1562 = 
BGL_S16VREF(BgL_xz00_135, BgL_niz00_1560); }  else 
{ 
 obj_t BgL_auxz00_6038;
BgL_auxz00_6038 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2969z00zz__srfi4z00, BgL_xz00_135, 
(int)(BgL_l2192z00_3368), 
(int)(BgL_niz00_1560)); 
FAILURE(BgL_auxz00_6038,BFALSE,BFALSE);} } 
BgL_arg1466z00_1561 = 
MAKE_YOUNG_PAIR(
BGL_INT16_TO_BINT16(BgL_arg1467z00_1562), BgL_resz00_1557); } 
{ 
 obj_t BgL_resz00_6047; long BgL_iz00_6046;
BgL_iz00_6046 = BgL_niz00_1560; 
BgL_resz00_6047 = BgL_arg1466z00_1561; 
BgL_resz00_1557 = BgL_resz00_6047; 
BgL_iz00_1556 = BgL_iz00_6046; 
goto BgL_zc3z04anonymousza31464ze3z87_1558;} } } } } } 

}



/* &s16vector->list */
obj_t BGl_z62s16vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3202, obj_t BgL_xz00_3203)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6048;
if(
BGL_S16VECTORP(BgL_xz00_3203))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6048 = BgL_xz00_3203
; }  else 
{ 
 obj_t BgL_auxz00_6051;
BgL_auxz00_6051 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2997z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_xz00_3203); 
FAILURE(BgL_auxz00_6051,BFALSE,BFALSE);} 
return 
BGl_s16vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6048);} } 

}



/* u16vector->list */
BGL_EXPORTED_DEF obj_t BGl_u16vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_136)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1069z00_1564;
BgL_g1069z00_1564 = 
BGL_HVECTOR_LENGTH(BgL_xz00_136); 
{ 
 long BgL_iz00_1567; obj_t BgL_resz00_1568;
BgL_iz00_1567 = BgL_g1069z00_1564; 
BgL_resz00_1568 = BNIL; 
BgL_zc3z04anonymousza31468ze3z87_1569:
if(
(BgL_iz00_1567==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1568;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1571;
BgL_niz00_1571 = 
(BgL_iz00_1567-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1472z00_1572;
{ /* Llib/srfi4.scm 771 */
 uint16_t BgL_arg1473z00_1573;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2196z00_3372;
BgL_l2196z00_3372 = 
BGL_HVECTOR_LENGTH(BgL_xz00_136); 
if(
BOUND_CHECK(BgL_niz00_1571, BgL_l2196z00_3372))
{ /* Llib/srfi4.scm 771 */
BgL_arg1473z00_1573 = 
BGL_U16VREF(BgL_xz00_136, BgL_niz00_1571); }  else 
{ 
 obj_t BgL_auxz00_6064;
BgL_auxz00_6064 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2971z00zz__srfi4z00, BgL_xz00_136, 
(int)(BgL_l2196z00_3372), 
(int)(BgL_niz00_1571)); 
FAILURE(BgL_auxz00_6064,BFALSE,BFALSE);} } 
BgL_arg1472z00_1572 = 
MAKE_YOUNG_PAIR(
BGL_UINT16_TO_BUINT16(BgL_arg1473z00_1573), BgL_resz00_1568); } 
{ 
 obj_t BgL_resz00_6073; long BgL_iz00_6072;
BgL_iz00_6072 = BgL_niz00_1571; 
BgL_resz00_6073 = BgL_arg1472z00_1572; 
BgL_resz00_1568 = BgL_resz00_6073; 
BgL_iz00_1567 = BgL_iz00_6072; 
goto BgL_zc3z04anonymousza31468ze3z87_1569;} } } } } } 

}



/* &u16vector->list */
obj_t BGl_z62u16vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3204, obj_t BgL_xz00_3205)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6074;
if(
BGL_U16VECTORP(BgL_xz00_3205))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6074 = BgL_xz00_3205
; }  else 
{ 
 obj_t BgL_auxz00_6077;
BgL_auxz00_6077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2998z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_xz00_3205); 
FAILURE(BgL_auxz00_6077,BFALSE,BFALSE);} 
return 
BGl_u16vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6074);} } 

}



/* s32vector->list */
BGL_EXPORTED_DEF obj_t BGl_s32vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_137)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1071z00_1575;
BgL_g1071z00_1575 = 
BGL_HVECTOR_LENGTH(BgL_xz00_137); 
{ 
 long BgL_iz00_1578; obj_t BgL_resz00_1579;
BgL_iz00_1578 = BgL_g1071z00_1575; 
BgL_resz00_1579 = BNIL; 
BgL_zc3z04anonymousza31474ze3z87_1580:
if(
(BgL_iz00_1578==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1579;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1582;
BgL_niz00_1582 = 
(BgL_iz00_1578-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1476z00_1583;
{ /* Llib/srfi4.scm 771 */
 int32_t BgL_arg1477z00_1584;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2200z00_3376;
BgL_l2200z00_3376 = 
BGL_HVECTOR_LENGTH(BgL_xz00_137); 
if(
BOUND_CHECK(BgL_niz00_1582, BgL_l2200z00_3376))
{ /* Llib/srfi4.scm 771 */
BgL_arg1477z00_1584 = 
BGL_S32VREF(BgL_xz00_137, BgL_niz00_1582); }  else 
{ 
 obj_t BgL_auxz00_6090;
BgL_auxz00_6090 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2973z00zz__srfi4z00, BgL_xz00_137, 
(int)(BgL_l2200z00_3376), 
(int)(BgL_niz00_1582)); 
FAILURE(BgL_auxz00_6090,BFALSE,BFALSE);} } 
BgL_arg1476z00_1583 = 
MAKE_YOUNG_PAIR(
BGL_INT32_TO_BINT32(BgL_arg1477z00_1584), BgL_resz00_1579); } 
{ 
 obj_t BgL_resz00_6099; long BgL_iz00_6098;
BgL_iz00_6098 = BgL_niz00_1582; 
BgL_resz00_6099 = BgL_arg1476z00_1583; 
BgL_resz00_1579 = BgL_resz00_6099; 
BgL_iz00_1578 = BgL_iz00_6098; 
goto BgL_zc3z04anonymousza31474ze3z87_1580;} } } } } } 

}



/* &s32vector->list */
obj_t BGl_z62s32vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3206, obj_t BgL_xz00_3207)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6100;
if(
BGL_S32VECTORP(BgL_xz00_3207))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6100 = BgL_xz00_3207
; }  else 
{ 
 obj_t BgL_auxz00_6103;
BgL_auxz00_6103 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2999z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_xz00_3207); 
FAILURE(BgL_auxz00_6103,BFALSE,BFALSE);} 
return 
BGl_s32vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6100);} } 

}



/* u32vector->list */
BGL_EXPORTED_DEF obj_t BGl_u32vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_138)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1073z00_1586;
BgL_g1073z00_1586 = 
BGL_HVECTOR_LENGTH(BgL_xz00_138); 
{ 
 long BgL_iz00_1589; obj_t BgL_resz00_1590;
BgL_iz00_1589 = BgL_g1073z00_1586; 
BgL_resz00_1590 = BNIL; 
BgL_zc3z04anonymousza31478ze3z87_1591:
if(
(BgL_iz00_1589==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1590;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1593;
BgL_niz00_1593 = 
(BgL_iz00_1589-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1480z00_1594;
{ /* Llib/srfi4.scm 771 */
 uint32_t BgL_arg1481z00_1595;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2204z00_3380;
BgL_l2204z00_3380 = 
BGL_HVECTOR_LENGTH(BgL_xz00_138); 
if(
BOUND_CHECK(BgL_niz00_1593, BgL_l2204z00_3380))
{ /* Llib/srfi4.scm 771 */
BgL_arg1481z00_1595 = 
BGL_U32VREF(BgL_xz00_138, BgL_niz00_1593); }  else 
{ 
 obj_t BgL_auxz00_6116;
BgL_auxz00_6116 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2975z00zz__srfi4z00, BgL_xz00_138, 
(int)(BgL_l2204z00_3380), 
(int)(BgL_niz00_1593)); 
FAILURE(BgL_auxz00_6116,BFALSE,BFALSE);} } 
BgL_arg1480z00_1594 = 
MAKE_YOUNG_PAIR(
BGL_UINT32_TO_BUINT32(BgL_arg1481z00_1595), BgL_resz00_1590); } 
{ 
 obj_t BgL_resz00_6125; long BgL_iz00_6124;
BgL_iz00_6124 = BgL_niz00_1593; 
BgL_resz00_6125 = BgL_arg1480z00_1594; 
BgL_resz00_1590 = BgL_resz00_6125; 
BgL_iz00_1589 = BgL_iz00_6124; 
goto BgL_zc3z04anonymousza31478ze3z87_1591;} } } } } } 

}



/* &u32vector->list */
obj_t BGl_z62u32vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3208, obj_t BgL_xz00_3209)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6126;
if(
BGL_U32VECTORP(BgL_xz00_3209))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6126 = BgL_xz00_3209
; }  else 
{ 
 obj_t BgL_auxz00_6129;
BgL_auxz00_6129 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string3000z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_xz00_3209); 
FAILURE(BgL_auxz00_6129,BFALSE,BFALSE);} 
return 
BGl_u32vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6126);} } 

}



/* s64vector->list */
BGL_EXPORTED_DEF obj_t BGl_s64vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_139)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1075z00_1597;
BgL_g1075z00_1597 = 
BGL_HVECTOR_LENGTH(BgL_xz00_139); 
{ 
 long BgL_iz00_1600; obj_t BgL_resz00_1601;
BgL_iz00_1600 = BgL_g1075z00_1597; 
BgL_resz00_1601 = BNIL; 
BgL_zc3z04anonymousza31482ze3z87_1602:
if(
(BgL_iz00_1600==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1601;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1604;
BgL_niz00_1604 = 
(BgL_iz00_1600-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1484z00_1605;
{ /* Llib/srfi4.scm 771 */
 int64_t BgL_arg1485z00_1606;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2208z00_3384;
BgL_l2208z00_3384 = 
BGL_HVECTOR_LENGTH(BgL_xz00_139); 
if(
BOUND_CHECK(BgL_niz00_1604, BgL_l2208z00_3384))
{ /* Llib/srfi4.scm 771 */
BgL_arg1485z00_1606 = 
BGL_S64VREF(BgL_xz00_139, BgL_niz00_1604); }  else 
{ 
 obj_t BgL_auxz00_6142;
BgL_auxz00_6142 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2977z00zz__srfi4z00, BgL_xz00_139, 
(int)(BgL_l2208z00_3384), 
(int)(BgL_niz00_1604)); 
FAILURE(BgL_auxz00_6142,BFALSE,BFALSE);} } 
BgL_arg1484z00_1605 = 
MAKE_YOUNG_PAIR(
BGL_INT64_TO_BINT64(BgL_arg1485z00_1606), BgL_resz00_1601); } 
{ 
 obj_t BgL_resz00_6151; long BgL_iz00_6150;
BgL_iz00_6150 = BgL_niz00_1604; 
BgL_resz00_6151 = BgL_arg1484z00_1605; 
BgL_resz00_1601 = BgL_resz00_6151; 
BgL_iz00_1600 = BgL_iz00_6150; 
goto BgL_zc3z04anonymousza31482ze3z87_1602;} } } } } } 

}



/* &s64vector->list */
obj_t BGl_z62s64vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3210, obj_t BgL_xz00_3211)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6152;
if(
BGL_S64VECTORP(BgL_xz00_3211))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6152 = BgL_xz00_3211
; }  else 
{ 
 obj_t BgL_auxz00_6155;
BgL_auxz00_6155 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string3001z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_xz00_3211); 
FAILURE(BgL_auxz00_6155,BFALSE,BFALSE);} 
return 
BGl_s64vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6152);} } 

}



/* u64vector->list */
BGL_EXPORTED_DEF obj_t BGl_u64vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_140)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1078z00_1608;
BgL_g1078z00_1608 = 
BGL_HVECTOR_LENGTH(BgL_xz00_140); 
{ 
 long BgL_iz00_1611; obj_t BgL_resz00_1612;
BgL_iz00_1611 = BgL_g1078z00_1608; 
BgL_resz00_1612 = BNIL; 
BgL_zc3z04anonymousza31486ze3z87_1613:
if(
(BgL_iz00_1611==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1612;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1615;
BgL_niz00_1615 = 
(BgL_iz00_1611-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1488z00_1616;
{ /* Llib/srfi4.scm 771 */
 uint64_t BgL_arg1489z00_1617;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2212z00_3388;
BgL_l2212z00_3388 = 
BGL_HVECTOR_LENGTH(BgL_xz00_140); 
if(
BOUND_CHECK(BgL_niz00_1615, BgL_l2212z00_3388))
{ /* Llib/srfi4.scm 771 */
BgL_arg1489z00_1617 = 
BGL_U64VREF(BgL_xz00_140, BgL_niz00_1615); }  else 
{ 
 obj_t BgL_auxz00_6168;
BgL_auxz00_6168 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2979z00zz__srfi4z00, BgL_xz00_140, 
(int)(BgL_l2212z00_3388), 
(int)(BgL_niz00_1615)); 
FAILURE(BgL_auxz00_6168,BFALSE,BFALSE);} } 
BgL_arg1488z00_1616 = 
MAKE_YOUNG_PAIR(
BGL_UINT64_TO_BUINT64(BgL_arg1489z00_1617), BgL_resz00_1612); } 
{ 
 obj_t BgL_resz00_6177; long BgL_iz00_6176;
BgL_iz00_6176 = BgL_niz00_1615; 
BgL_resz00_6177 = BgL_arg1488z00_1616; 
BgL_resz00_1612 = BgL_resz00_6177; 
BgL_iz00_1611 = BgL_iz00_6176; 
goto BgL_zc3z04anonymousza31486ze3z87_1613;} } } } } } 

}



/* &u64vector->list */
obj_t BGl_z62u64vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3212, obj_t BgL_xz00_3213)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6178;
if(
BGL_U64VECTORP(BgL_xz00_3213))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6178 = BgL_xz00_3213
; }  else 
{ 
 obj_t BgL_auxz00_6181;
BgL_auxz00_6181 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string3002z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_xz00_3213); 
FAILURE(BgL_auxz00_6181,BFALSE,BFALSE);} 
return 
BGl_u64vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6178);} } 

}



/* f32vector->list */
BGL_EXPORTED_DEF obj_t BGl_f32vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_141)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1080z00_1619;
BgL_g1080z00_1619 = 
BGL_HVECTOR_LENGTH(BgL_xz00_141); 
{ 
 long BgL_iz00_1622; obj_t BgL_resz00_1623;
BgL_iz00_1622 = BgL_g1080z00_1619; 
BgL_resz00_1623 = BNIL; 
BgL_zc3z04anonymousza31490ze3z87_1624:
if(
(BgL_iz00_1622==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1623;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1626;
BgL_niz00_1626 = 
(BgL_iz00_1622-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1492z00_1627;
{ /* Llib/srfi4.scm 771 */
 float BgL_arg1494z00_1628;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2216z00_3392;
BgL_l2216z00_3392 = 
BGL_HVECTOR_LENGTH(BgL_xz00_141); 
if(
BOUND_CHECK(BgL_niz00_1626, BgL_l2216z00_3392))
{ /* Llib/srfi4.scm 771 */
BgL_arg1494z00_1628 = 
BGL_F32VREF(BgL_xz00_141, BgL_niz00_1626); }  else 
{ 
 obj_t BgL_auxz00_6194;
BgL_auxz00_6194 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2981z00zz__srfi4z00, BgL_xz00_141, 
(int)(BgL_l2216z00_3392), 
(int)(BgL_niz00_1626)); 
FAILURE(BgL_auxz00_6194,BFALSE,BFALSE);} } 
BgL_arg1492z00_1627 = 
MAKE_YOUNG_PAIR(
FLOAT_TO_REAL(BgL_arg1494z00_1628), BgL_resz00_1623); } 
{ 
 obj_t BgL_resz00_6203; long BgL_iz00_6202;
BgL_iz00_6202 = BgL_niz00_1626; 
BgL_resz00_6203 = BgL_arg1492z00_1627; 
BgL_resz00_1623 = BgL_resz00_6203; 
BgL_iz00_1622 = BgL_iz00_6202; 
goto BgL_zc3z04anonymousza31490ze3z87_1624;} } } } } } 

}



/* &f32vector->list */
obj_t BGl_z62f32vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3214, obj_t BgL_xz00_3215)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6204;
if(
BGL_F32VECTORP(BgL_xz00_3215))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6204 = BgL_xz00_3215
; }  else 
{ 
 obj_t BgL_auxz00_6207;
BgL_auxz00_6207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string3003z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_xz00_3215); 
FAILURE(BgL_auxz00_6207,BFALSE,BFALSE);} 
return 
BGl_f32vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6204);} } 

}



/* f64vector->list */
BGL_EXPORTED_DEF obj_t BGl_f64vectorzd2ze3listz31zz__srfi4z00(obj_t BgL_xz00_142)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 long BgL_g1082z00_1630;
BgL_g1082z00_1630 = 
BGL_HVECTOR_LENGTH(BgL_xz00_142); 
{ 
 long BgL_iz00_1633; obj_t BgL_resz00_1634;
BgL_iz00_1633 = BgL_g1082z00_1630; 
BgL_resz00_1634 = BNIL; 
BgL_zc3z04anonymousza31495ze3z87_1635:
if(
(BgL_iz00_1633==0L))
{ /* Llib/srfi4.scm 771 */
return BgL_resz00_1634;}  else 
{ /* Llib/srfi4.scm 771 */
 long BgL_niz00_1637;
BgL_niz00_1637 = 
(BgL_iz00_1633-1L); 
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_arg1497z00_1638;
{ /* Llib/srfi4.scm 771 */
 double BgL_arg1498z00_1639;
{ /* Llib/srfi4.scm 771 */
 long BgL_l2220z00_3396;
BgL_l2220z00_3396 = 
BGL_HVECTOR_LENGTH(BgL_xz00_142); 
if(
BOUND_CHECK(BgL_niz00_1637, BgL_l2220z00_3396))
{ /* Llib/srfi4.scm 771 */
BgL_arg1498z00_1639 = 
BGL_F64VREF(BgL_xz00_142, BgL_niz00_1637); }  else 
{ 
 obj_t BgL_auxz00_6220;
BgL_auxz00_6220 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string2983z00zz__srfi4z00, BgL_xz00_142, 
(int)(BgL_l2220z00_3396), 
(int)(BgL_niz00_1637)); 
FAILURE(BgL_auxz00_6220,BFALSE,BFALSE);} } 
BgL_arg1497z00_1638 = 
MAKE_YOUNG_PAIR(
DOUBLE_TO_REAL(BgL_arg1498z00_1639), BgL_resz00_1634); } 
{ 
 obj_t BgL_resz00_6229; long BgL_iz00_6228;
BgL_iz00_6228 = BgL_niz00_1637; 
BgL_resz00_6229 = BgL_arg1497z00_1638; 
BgL_resz00_1634 = BgL_resz00_6229; 
BgL_iz00_1633 = BgL_iz00_6228; 
goto BgL_zc3z04anonymousza31495ze3z87_1635;} } } } } } 

}



/* &f64vector->list */
obj_t BGl_z62f64vectorzd2ze3listz53zz__srfi4z00(obj_t BgL_envz00_3216, obj_t BgL_xz00_3217)
{
{ /* Llib/srfi4.scm 771 */
{ /* Llib/srfi4.scm 771 */
 obj_t BgL_auxz00_6230;
if(
BGL_F64VECTORP(BgL_xz00_3217))
{ /* Llib/srfi4.scm 771 */
BgL_auxz00_6230 = BgL_xz00_3217
; }  else 
{ 
 obj_t BgL_auxz00_6233;
BgL_auxz00_6233 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(30505L), BGl_string3004z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_xz00_3217); 
FAILURE(BgL_auxz00_6233,BFALSE,BFALSE);} 
return 
BGl_f64vectorzd2ze3listz31zz__srfi4z00(BgL_auxz00_6230);} } 

}



/* list->s8vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s8vectorz31zz__srfi4z00(obj_t BgL_xz00_143)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1641;
BgL_lenz00_1641 = 
bgl_list_length(BgL_xz00_143); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1642;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6239;
BgL_tmpz00_6239 = 
(int32_t)(BgL_lenz00_1641); 
BgL_vecz00_1642 = 
BGL_ALLOC_S8VECTOR(BgL_tmpz00_6239); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1644; obj_t BgL_lz00_1645;
BgL_iz00_1644 = 0L; 
BgL_lz00_1645 = BgL_xz00_143; 
BgL_zc3z04anonymousza31499ze3z87_1646:
if(
(BgL_iz00_1644==BgL_lenz00_1641))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1642;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1501z00_1648;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3509z00_6244;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6245;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2608;
if(
PAIRP(BgL_lz00_1645))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2608 = BgL_lz00_1645; }  else 
{ 
 obj_t BgL_auxz00_6248;
BgL_auxz00_6248 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1645); 
FAILURE(BgL_auxz00_6248,BFALSE,BFALSE);} 
BgL_tmpz00_6245 = 
CAR(BgL_pairz00_2608); } 
BgL_test3509z00_6244 = 
INTEGERP(BgL_tmpz00_6245); } 
if(BgL_test3509z00_6244)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1504z00_1651;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2609;
if(
PAIRP(BgL_lz00_1645))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2609 = BgL_lz00_1645; }  else 
{ 
 obj_t BgL_auxz00_6256;
BgL_auxz00_6256 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1645); 
FAILURE(BgL_auxz00_6256,BFALSE,BFALSE);} 
BgL_arg1504z00_1651 = 
CAR(BgL_pairz00_2609); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2610;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6261;
if(
INTEGERP(BgL_arg1504z00_1651))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6261 = BgL_arg1504z00_1651
; }  else 
{ 
 obj_t BgL_auxz00_6264;
BgL_auxz00_6264 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1504z00_1651); 
FAILURE(BgL_auxz00_6264,BFALSE,BFALSE);} 
BgL_xz00_2610 = 
(long)CINT(BgL_tmpz00_6261); } 
{ /* Llib/srfi4.scm 796 */
 int8_t BgL_tmpz00_6269;
BgL_tmpz00_6269 = 
(int8_t)(BgL_xz00_2610); 
BgL_arg1501z00_1648 = 
BGL_INT8_TO_BINT8(BgL_tmpz00_6269); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2611;
if(
PAIRP(BgL_lz00_1645))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2611 = BgL_lz00_1645; }  else 
{ 
 obj_t BgL_auxz00_6274;
BgL_auxz00_6274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1645); 
FAILURE(BgL_auxz00_6274,BFALSE,BFALSE);} 
BgL_arg1501z00_1648 = 
CAR(BgL_pairz00_2611); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2224z00_3400;
BgL_l2224z00_3400 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1642); 
if(
BOUND_CHECK(BgL_iz00_1644, BgL_l2224z00_3400))
{ /* Llib/srfi4.scm 796 */
 int8_t BgL_tmpz00_6282;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6283;
if(
BGL_INT8P(BgL_arg1501z00_1648))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6283 = BgL_arg1501z00_1648
; }  else 
{ 
 obj_t BgL_auxz00_6286;
BgL_auxz00_6286 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2915z00zz__srfi4z00, BgL_arg1501z00_1648); 
FAILURE(BgL_auxz00_6286,BFALSE,BFALSE);} 
BgL_tmpz00_6282 = 
BGL_BINT8_TO_INT8(BgL_tmpz00_6283); } 
BGL_S8VSET(BgL_vecz00_1642, BgL_iz00_1644, BgL_tmpz00_6282); }  else 
{ 
 obj_t BgL_auxz00_6292;
BgL_auxz00_6292 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2916z00zz__srfi4z00, BgL_vecz00_1642, 
(int)(BgL_l2224z00_3400), 
(int)(BgL_iz00_1644)); 
FAILURE(BgL_auxz00_6292,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1506z00_1653; obj_t BgL_arg1507z00_1654;
BgL_arg1506z00_1653 = 
(BgL_iz00_1644+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2613;
if(
PAIRP(BgL_lz00_1645))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2613 = BgL_lz00_1645; }  else 
{ 
 obj_t BgL_auxz00_6301;
BgL_auxz00_6301 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1645); 
FAILURE(BgL_auxz00_6301,BFALSE,BFALSE);} 
BgL_arg1507z00_1654 = 
CDR(BgL_pairz00_2613); } 
{ 
 obj_t BgL_lz00_6307; long BgL_iz00_6306;
BgL_iz00_6306 = BgL_arg1506z00_1653; 
BgL_lz00_6307 = BgL_arg1507z00_1654; 
BgL_lz00_1645 = BgL_lz00_6307; 
BgL_iz00_1644 = BgL_iz00_6306; 
goto BgL_zc3z04anonymousza31499ze3z87_1646;} } } } } } } } 

}



/* &list->s8vector */
obj_t BGl_z62listzd2ze3s8vectorz53zz__srfi4z00(obj_t BgL_envz00_3218, obj_t BgL_xz00_3219)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6308;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3219))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6308 = BgL_xz00_3219
; }  else 
{ 
 obj_t BgL_auxz00_6311;
BgL_auxz00_6311 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3007z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3219); 
FAILURE(BgL_auxz00_6311,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3s8vectorz31zz__srfi4z00(BgL_auxz00_6308);} } 

}



/* list->u8vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u8vectorz31zz__srfi4z00(obj_t BgL_xz00_144)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1656;
BgL_lenz00_1656 = 
bgl_list_length(BgL_xz00_144); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1657;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6317;
BgL_tmpz00_6317 = 
(int32_t)(BgL_lenz00_1656); 
BgL_vecz00_1657 = 
BGL_ALLOC_U8VECTOR(BgL_tmpz00_6317); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1659; obj_t BgL_lz00_1660;
BgL_iz00_1659 = 0L; 
BgL_lz00_1660 = BgL_xz00_144; 
BgL_zc3z04anonymousza31508ze3z87_1661:
if(
(BgL_iz00_1659==BgL_lenz00_1656))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1657;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1510z00_1663;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3519z00_6322;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6323;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2616;
if(
PAIRP(BgL_lz00_1660))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2616 = BgL_lz00_1660; }  else 
{ 
 obj_t BgL_auxz00_6326;
BgL_auxz00_6326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1660); 
FAILURE(BgL_auxz00_6326,BFALSE,BFALSE);} 
BgL_tmpz00_6323 = 
CAR(BgL_pairz00_2616); } 
BgL_test3519z00_6322 = 
INTEGERP(BgL_tmpz00_6323); } 
if(BgL_test3519z00_6322)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1513z00_1666;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2617;
if(
PAIRP(BgL_lz00_1660))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2617 = BgL_lz00_1660; }  else 
{ 
 obj_t BgL_auxz00_6334;
BgL_auxz00_6334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1660); 
FAILURE(BgL_auxz00_6334,BFALSE,BFALSE);} 
BgL_arg1513z00_1666 = 
CAR(BgL_pairz00_2617); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2618;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6339;
if(
INTEGERP(BgL_arg1513z00_1666))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6339 = BgL_arg1513z00_1666
; }  else 
{ 
 obj_t BgL_auxz00_6342;
BgL_auxz00_6342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1513z00_1666); 
FAILURE(BgL_auxz00_6342,BFALSE,BFALSE);} 
BgL_xz00_2618 = 
(long)CINT(BgL_tmpz00_6339); } 
{ /* Llib/srfi4.scm 796 */
 uint8_t BgL_tmpz00_6347;
BgL_tmpz00_6347 = 
(uint8_t)(BgL_xz00_2618); 
BgL_arg1510z00_1663 = 
BGL_UINT8_TO_BUINT8(BgL_tmpz00_6347); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2619;
if(
PAIRP(BgL_lz00_1660))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2619 = BgL_lz00_1660; }  else 
{ 
 obj_t BgL_auxz00_6352;
BgL_auxz00_6352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1660); 
FAILURE(BgL_auxz00_6352,BFALSE,BFALSE);} 
BgL_arg1510z00_1663 = 
CAR(BgL_pairz00_2619); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2228z00_3404;
BgL_l2228z00_3404 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1657); 
if(
BOUND_CHECK(BgL_iz00_1659, BgL_l2228z00_3404))
{ /* Llib/srfi4.scm 796 */
 uint8_t BgL_tmpz00_6360;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6361;
if(
BGL_UINT8P(BgL_arg1510z00_1663))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6361 = BgL_arg1510z00_1663
; }  else 
{ 
 obj_t BgL_auxz00_6364;
BgL_auxz00_6364 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2920z00zz__srfi4z00, BgL_arg1510z00_1663); 
FAILURE(BgL_auxz00_6364,BFALSE,BFALSE);} 
BgL_tmpz00_6360 = 
BGL_BUINT8_TO_UINT8(BgL_tmpz00_6361); } 
BGL_U8VSET(BgL_vecz00_1657, BgL_iz00_1659, BgL_tmpz00_6360); }  else 
{ 
 obj_t BgL_auxz00_6370;
BgL_auxz00_6370 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2921z00zz__srfi4z00, BgL_vecz00_1657, 
(int)(BgL_l2228z00_3404), 
(int)(BgL_iz00_1659)); 
FAILURE(BgL_auxz00_6370,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1516z00_1668; obj_t BgL_arg1517z00_1669;
BgL_arg1516z00_1668 = 
(BgL_iz00_1659+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2621;
if(
PAIRP(BgL_lz00_1660))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2621 = BgL_lz00_1660; }  else 
{ 
 obj_t BgL_auxz00_6379;
BgL_auxz00_6379 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1660); 
FAILURE(BgL_auxz00_6379,BFALSE,BFALSE);} 
BgL_arg1517z00_1669 = 
CDR(BgL_pairz00_2621); } 
{ 
 obj_t BgL_lz00_6385; long BgL_iz00_6384;
BgL_iz00_6384 = BgL_arg1516z00_1668; 
BgL_lz00_6385 = BgL_arg1517z00_1669; 
BgL_lz00_1660 = BgL_lz00_6385; 
BgL_iz00_1659 = BgL_iz00_6384; 
goto BgL_zc3z04anonymousza31508ze3z87_1661;} } } } } } } } 

}



/* &list->u8vector */
obj_t BGl_z62listzd2ze3u8vectorz53zz__srfi4z00(obj_t BgL_envz00_3220, obj_t BgL_xz00_3221)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6386;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3221))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6386 = BgL_xz00_3221
; }  else 
{ 
 obj_t BgL_auxz00_6389;
BgL_auxz00_6389 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3008z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3221); 
FAILURE(BgL_auxz00_6389,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3u8vectorz31zz__srfi4z00(BgL_auxz00_6386);} } 

}



/* list->s16vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s16vectorz31zz__srfi4z00(obj_t BgL_xz00_145)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1671;
BgL_lenz00_1671 = 
bgl_list_length(BgL_xz00_145); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1672;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6395;
BgL_tmpz00_6395 = 
(int32_t)(BgL_lenz00_1671); 
BgL_vecz00_1672 = 
BGL_ALLOC_S16VECTOR(BgL_tmpz00_6395); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1674; obj_t BgL_lz00_1675;
BgL_iz00_1674 = 0L; 
BgL_lz00_1675 = BgL_xz00_145; 
BgL_zc3z04anonymousza31518ze3z87_1676:
if(
(BgL_iz00_1674==BgL_lenz00_1671))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1672;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1521z00_1678;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3529z00_6400;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6401;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2624;
if(
PAIRP(BgL_lz00_1675))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2624 = BgL_lz00_1675; }  else 
{ 
 obj_t BgL_auxz00_6404;
BgL_auxz00_6404 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1675); 
FAILURE(BgL_auxz00_6404,BFALSE,BFALSE);} 
BgL_tmpz00_6401 = 
CAR(BgL_pairz00_2624); } 
BgL_test3529z00_6400 = 
INTEGERP(BgL_tmpz00_6401); } 
if(BgL_test3529z00_6400)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1524z00_1681;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2625;
if(
PAIRP(BgL_lz00_1675))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2625 = BgL_lz00_1675; }  else 
{ 
 obj_t BgL_auxz00_6412;
BgL_auxz00_6412 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1675); 
FAILURE(BgL_auxz00_6412,BFALSE,BFALSE);} 
BgL_arg1524z00_1681 = 
CAR(BgL_pairz00_2625); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2626;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6417;
if(
INTEGERP(BgL_arg1524z00_1681))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6417 = BgL_arg1524z00_1681
; }  else 
{ 
 obj_t BgL_auxz00_6420;
BgL_auxz00_6420 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1524z00_1681); 
FAILURE(BgL_auxz00_6420,BFALSE,BFALSE);} 
BgL_xz00_2626 = 
(long)CINT(BgL_tmpz00_6417); } 
{ /* Llib/srfi4.scm 796 */
 int16_t BgL_tmpz00_6425;
BgL_tmpz00_6425 = 
(int16_t)(BgL_xz00_2626); 
BgL_arg1521z00_1678 = 
BGL_INT16_TO_BINT16(BgL_tmpz00_6425); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2627;
if(
PAIRP(BgL_lz00_1675))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2627 = BgL_lz00_1675; }  else 
{ 
 obj_t BgL_auxz00_6430;
BgL_auxz00_6430 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1675); 
FAILURE(BgL_auxz00_6430,BFALSE,BFALSE);} 
BgL_arg1521z00_1678 = 
CAR(BgL_pairz00_2627); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2232z00_3408;
BgL_l2232z00_3408 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1672); 
if(
BOUND_CHECK(BgL_iz00_1674, BgL_l2232z00_3408))
{ /* Llib/srfi4.scm 796 */
 int16_t BgL_tmpz00_6438;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6439;
if(
BGL_INT16P(BgL_arg1521z00_1678))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6439 = BgL_arg1521z00_1678
; }  else 
{ 
 obj_t BgL_auxz00_6442;
BgL_auxz00_6442 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2925z00zz__srfi4z00, BgL_arg1521z00_1678); 
FAILURE(BgL_auxz00_6442,BFALSE,BFALSE);} 
BgL_tmpz00_6438 = 
BGL_BINT16_TO_INT16(BgL_tmpz00_6439); } 
BGL_S16VSET(BgL_vecz00_1672, BgL_iz00_1674, BgL_tmpz00_6438); }  else 
{ 
 obj_t BgL_auxz00_6448;
BgL_auxz00_6448 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2926z00zz__srfi4z00, BgL_vecz00_1672, 
(int)(BgL_l2232z00_3408), 
(int)(BgL_iz00_1674)); 
FAILURE(BgL_auxz00_6448,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1526z00_1683; obj_t BgL_arg1527z00_1684;
BgL_arg1526z00_1683 = 
(BgL_iz00_1674+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2629;
if(
PAIRP(BgL_lz00_1675))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2629 = BgL_lz00_1675; }  else 
{ 
 obj_t BgL_auxz00_6457;
BgL_auxz00_6457 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1675); 
FAILURE(BgL_auxz00_6457,BFALSE,BFALSE);} 
BgL_arg1527z00_1684 = 
CDR(BgL_pairz00_2629); } 
{ 
 obj_t BgL_lz00_6463; long BgL_iz00_6462;
BgL_iz00_6462 = BgL_arg1526z00_1683; 
BgL_lz00_6463 = BgL_arg1527z00_1684; 
BgL_lz00_1675 = BgL_lz00_6463; 
BgL_iz00_1674 = BgL_iz00_6462; 
goto BgL_zc3z04anonymousza31518ze3z87_1676;} } } } } } } } 

}



/* &list->s16vector */
obj_t BGl_z62listzd2ze3s16vectorz53zz__srfi4z00(obj_t BgL_envz00_3222, obj_t BgL_xz00_3223)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6464;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3223))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6464 = BgL_xz00_3223
; }  else 
{ 
 obj_t BgL_auxz00_6467;
BgL_auxz00_6467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3009z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3223); 
FAILURE(BgL_auxz00_6467,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3s16vectorz31zz__srfi4z00(BgL_auxz00_6464);} } 

}



/* list->u16vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u16vectorz31zz__srfi4z00(obj_t BgL_xz00_146)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1686;
BgL_lenz00_1686 = 
bgl_list_length(BgL_xz00_146); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1687;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6473;
BgL_tmpz00_6473 = 
(int32_t)(BgL_lenz00_1686); 
BgL_vecz00_1687 = 
BGL_ALLOC_U16VECTOR(BgL_tmpz00_6473); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1689; obj_t BgL_lz00_1690;
BgL_iz00_1689 = 0L; 
BgL_lz00_1690 = BgL_xz00_146; 
BgL_zc3z04anonymousza31528ze3z87_1691:
if(
(BgL_iz00_1689==BgL_lenz00_1686))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1687;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1530z00_1693;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3539z00_6478;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6479;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2632;
if(
PAIRP(BgL_lz00_1690))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2632 = BgL_lz00_1690; }  else 
{ 
 obj_t BgL_auxz00_6482;
BgL_auxz00_6482 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1690); 
FAILURE(BgL_auxz00_6482,BFALSE,BFALSE);} 
BgL_tmpz00_6479 = 
CAR(BgL_pairz00_2632); } 
BgL_test3539z00_6478 = 
INTEGERP(BgL_tmpz00_6479); } 
if(BgL_test3539z00_6478)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1535z00_1696;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2633;
if(
PAIRP(BgL_lz00_1690))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2633 = BgL_lz00_1690; }  else 
{ 
 obj_t BgL_auxz00_6490;
BgL_auxz00_6490 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1690); 
FAILURE(BgL_auxz00_6490,BFALSE,BFALSE);} 
BgL_arg1535z00_1696 = 
CAR(BgL_pairz00_2633); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2634;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6495;
if(
INTEGERP(BgL_arg1535z00_1696))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6495 = BgL_arg1535z00_1696
; }  else 
{ 
 obj_t BgL_auxz00_6498;
BgL_auxz00_6498 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1535z00_1696); 
FAILURE(BgL_auxz00_6498,BFALSE,BFALSE);} 
BgL_xz00_2634 = 
(long)CINT(BgL_tmpz00_6495); } 
{ /* Llib/srfi4.scm 796 */
 uint16_t BgL_tmpz00_6503;
BgL_tmpz00_6503 = 
(uint16_t)(BgL_xz00_2634); 
BgL_arg1530z00_1693 = 
BGL_UINT16_TO_BUINT16(BgL_tmpz00_6503); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2635;
if(
PAIRP(BgL_lz00_1690))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2635 = BgL_lz00_1690; }  else 
{ 
 obj_t BgL_auxz00_6508;
BgL_auxz00_6508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1690); 
FAILURE(BgL_auxz00_6508,BFALSE,BFALSE);} 
BgL_arg1530z00_1693 = 
CAR(BgL_pairz00_2635); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2236z00_3412;
BgL_l2236z00_3412 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1687); 
if(
BOUND_CHECK(BgL_iz00_1689, BgL_l2236z00_3412))
{ /* Llib/srfi4.scm 796 */
 uint16_t BgL_tmpz00_6516;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6517;
if(
BGL_UINT16P(BgL_arg1530z00_1693))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6517 = BgL_arg1530z00_1693
; }  else 
{ 
 obj_t BgL_auxz00_6520;
BgL_auxz00_6520 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2930z00zz__srfi4z00, BgL_arg1530z00_1693); 
FAILURE(BgL_auxz00_6520,BFALSE,BFALSE);} 
BgL_tmpz00_6516 = 
BGL_BUINT16_TO_UINT16(BgL_tmpz00_6517); } 
BGL_U16VSET(BgL_vecz00_1687, BgL_iz00_1689, BgL_tmpz00_6516); }  else 
{ 
 obj_t BgL_auxz00_6526;
BgL_auxz00_6526 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2931z00zz__srfi4z00, BgL_vecz00_1687, 
(int)(BgL_l2236z00_3412), 
(int)(BgL_iz00_1689)); 
FAILURE(BgL_auxz00_6526,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1537z00_1698; obj_t BgL_arg1539z00_1699;
BgL_arg1537z00_1698 = 
(BgL_iz00_1689+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2637;
if(
PAIRP(BgL_lz00_1690))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2637 = BgL_lz00_1690; }  else 
{ 
 obj_t BgL_auxz00_6535;
BgL_auxz00_6535 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1690); 
FAILURE(BgL_auxz00_6535,BFALSE,BFALSE);} 
BgL_arg1539z00_1699 = 
CDR(BgL_pairz00_2637); } 
{ 
 obj_t BgL_lz00_6541; long BgL_iz00_6540;
BgL_iz00_6540 = BgL_arg1537z00_1698; 
BgL_lz00_6541 = BgL_arg1539z00_1699; 
BgL_lz00_1690 = BgL_lz00_6541; 
BgL_iz00_1689 = BgL_iz00_6540; 
goto BgL_zc3z04anonymousza31528ze3z87_1691;} } } } } } } } 

}



/* &list->u16vector */
obj_t BGl_z62listzd2ze3u16vectorz53zz__srfi4z00(obj_t BgL_envz00_3224, obj_t BgL_xz00_3225)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6542;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3225))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6542 = BgL_xz00_3225
; }  else 
{ 
 obj_t BgL_auxz00_6545;
BgL_auxz00_6545 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3010z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3225); 
FAILURE(BgL_auxz00_6545,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3u16vectorz31zz__srfi4z00(BgL_auxz00_6542);} } 

}



/* list->s32vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s32vectorz31zz__srfi4z00(obj_t BgL_xz00_147)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1701;
BgL_lenz00_1701 = 
bgl_list_length(BgL_xz00_147); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1702;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6551;
BgL_tmpz00_6551 = 
(int32_t)(BgL_lenz00_1701); 
BgL_vecz00_1702 = 
BGL_ALLOC_S32VECTOR(BgL_tmpz00_6551); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1704; obj_t BgL_lz00_1705;
BgL_iz00_1704 = 0L; 
BgL_lz00_1705 = BgL_xz00_147; 
BgL_zc3z04anonymousza31540ze3z87_1706:
if(
(BgL_iz00_1704==BgL_lenz00_1701))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1702;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1543z00_1708;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3549z00_6556;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6557;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2640;
if(
PAIRP(BgL_lz00_1705))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2640 = BgL_lz00_1705; }  else 
{ 
 obj_t BgL_auxz00_6560;
BgL_auxz00_6560 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1705); 
FAILURE(BgL_auxz00_6560,BFALSE,BFALSE);} 
BgL_tmpz00_6557 = 
CAR(BgL_pairz00_2640); } 
BgL_test3549z00_6556 = 
INTEGERP(BgL_tmpz00_6557); } 
if(BgL_test3549z00_6556)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1546z00_1711;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2641;
if(
PAIRP(BgL_lz00_1705))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2641 = BgL_lz00_1705; }  else 
{ 
 obj_t BgL_auxz00_6568;
BgL_auxz00_6568 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1705); 
FAILURE(BgL_auxz00_6568,BFALSE,BFALSE);} 
BgL_arg1546z00_1711 = 
CAR(BgL_pairz00_2641); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2642;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6573;
if(
INTEGERP(BgL_arg1546z00_1711))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6573 = BgL_arg1546z00_1711
; }  else 
{ 
 obj_t BgL_auxz00_6576;
BgL_auxz00_6576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1546z00_1711); 
FAILURE(BgL_auxz00_6576,BFALSE,BFALSE);} 
BgL_xz00_2642 = 
(long)CINT(BgL_tmpz00_6573); } 
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6581;
BgL_tmpz00_6581 = 
(int32_t)(BgL_xz00_2642); 
BgL_arg1543z00_1708 = 
BGL_INT32_TO_BINT32(BgL_tmpz00_6581); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2643;
if(
PAIRP(BgL_lz00_1705))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2643 = BgL_lz00_1705; }  else 
{ 
 obj_t BgL_auxz00_6586;
BgL_auxz00_6586 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1705); 
FAILURE(BgL_auxz00_6586,BFALSE,BFALSE);} 
BgL_arg1543z00_1708 = 
CAR(BgL_pairz00_2643); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2240z00_3416;
BgL_l2240z00_3416 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1702); 
if(
BOUND_CHECK(BgL_iz00_1704, BgL_l2240z00_3416))
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6594;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6595;
if(
BGL_INT32P(BgL_arg1543z00_1708))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6595 = BgL_arg1543z00_1708
; }  else 
{ 
 obj_t BgL_auxz00_6598;
BgL_auxz00_6598 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2935z00zz__srfi4z00, BgL_arg1543z00_1708); 
FAILURE(BgL_auxz00_6598,BFALSE,BFALSE);} 
BgL_tmpz00_6594 = 
BGL_BINT32_TO_INT32(BgL_tmpz00_6595); } 
BGL_S32VSET(BgL_vecz00_1702, BgL_iz00_1704, BgL_tmpz00_6594); }  else 
{ 
 obj_t BgL_auxz00_6604;
BgL_auxz00_6604 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2936z00zz__srfi4z00, BgL_vecz00_1702, 
(int)(BgL_l2240z00_3416), 
(int)(BgL_iz00_1704)); 
FAILURE(BgL_auxz00_6604,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1549z00_1713; obj_t BgL_arg1552z00_1714;
BgL_arg1549z00_1713 = 
(BgL_iz00_1704+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2645;
if(
PAIRP(BgL_lz00_1705))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2645 = BgL_lz00_1705; }  else 
{ 
 obj_t BgL_auxz00_6613;
BgL_auxz00_6613 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1705); 
FAILURE(BgL_auxz00_6613,BFALSE,BFALSE);} 
BgL_arg1552z00_1714 = 
CDR(BgL_pairz00_2645); } 
{ 
 obj_t BgL_lz00_6619; long BgL_iz00_6618;
BgL_iz00_6618 = BgL_arg1549z00_1713; 
BgL_lz00_6619 = BgL_arg1552z00_1714; 
BgL_lz00_1705 = BgL_lz00_6619; 
BgL_iz00_1704 = BgL_iz00_6618; 
goto BgL_zc3z04anonymousza31540ze3z87_1706;} } } } } } } } 

}



/* &list->s32vector */
obj_t BGl_z62listzd2ze3s32vectorz53zz__srfi4z00(obj_t BgL_envz00_3226, obj_t BgL_xz00_3227)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6620;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3227))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6620 = BgL_xz00_3227
; }  else 
{ 
 obj_t BgL_auxz00_6623;
BgL_auxz00_6623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3011z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3227); 
FAILURE(BgL_auxz00_6623,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3s32vectorz31zz__srfi4z00(BgL_auxz00_6620);} } 

}



/* list->u32vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u32vectorz31zz__srfi4z00(obj_t BgL_xz00_148)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1716;
BgL_lenz00_1716 = 
bgl_list_length(BgL_xz00_148); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1717;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6629;
BgL_tmpz00_6629 = 
(int32_t)(BgL_lenz00_1716); 
BgL_vecz00_1717 = 
BGL_ALLOC_U32VECTOR(BgL_tmpz00_6629); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1719; obj_t BgL_lz00_1720;
BgL_iz00_1719 = 0L; 
BgL_lz00_1720 = BgL_xz00_148; 
BgL_zc3z04anonymousza31553ze3z87_1721:
if(
(BgL_iz00_1719==BgL_lenz00_1716))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1717;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1555z00_1723;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3559z00_6634;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6635;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2648;
if(
PAIRP(BgL_lz00_1720))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2648 = BgL_lz00_1720; }  else 
{ 
 obj_t BgL_auxz00_6638;
BgL_auxz00_6638 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1720); 
FAILURE(BgL_auxz00_6638,BFALSE,BFALSE);} 
BgL_tmpz00_6635 = 
CAR(BgL_pairz00_2648); } 
BgL_test3559z00_6634 = 
INTEGERP(BgL_tmpz00_6635); } 
if(BgL_test3559z00_6634)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1558z00_1726;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2649;
if(
PAIRP(BgL_lz00_1720))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2649 = BgL_lz00_1720; }  else 
{ 
 obj_t BgL_auxz00_6646;
BgL_auxz00_6646 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1720); 
FAILURE(BgL_auxz00_6646,BFALSE,BFALSE);} 
BgL_arg1558z00_1726 = 
CAR(BgL_pairz00_2649); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2650;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6651;
if(
INTEGERP(BgL_arg1558z00_1726))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6651 = BgL_arg1558z00_1726
; }  else 
{ 
 obj_t BgL_auxz00_6654;
BgL_auxz00_6654 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1558z00_1726); 
FAILURE(BgL_auxz00_6654,BFALSE,BFALSE);} 
BgL_xz00_2650 = 
(long)CINT(BgL_tmpz00_6651); } 
{ /* Llib/srfi4.scm 796 */
 uint32_t BgL_tmpz00_6659;
BgL_tmpz00_6659 = 
(uint32_t)(BgL_xz00_2650); 
BgL_arg1555z00_1723 = 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_6659); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2651;
if(
PAIRP(BgL_lz00_1720))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2651 = BgL_lz00_1720; }  else 
{ 
 obj_t BgL_auxz00_6664;
BgL_auxz00_6664 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1720); 
FAILURE(BgL_auxz00_6664,BFALSE,BFALSE);} 
BgL_arg1555z00_1723 = 
CAR(BgL_pairz00_2651); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2244z00_3420;
BgL_l2244z00_3420 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1717); 
if(
BOUND_CHECK(BgL_iz00_1719, BgL_l2244z00_3420))
{ /* Llib/srfi4.scm 796 */
 uint32_t BgL_tmpz00_6672;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6673;
if(
BGL_UINT32P(BgL_arg1555z00_1723))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6673 = BgL_arg1555z00_1723
; }  else 
{ 
 obj_t BgL_auxz00_6676;
BgL_auxz00_6676 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2940z00zz__srfi4z00, BgL_arg1555z00_1723); 
FAILURE(BgL_auxz00_6676,BFALSE,BFALSE);} 
BgL_tmpz00_6672 = 
BGL_BUINT32_TO_UINT32(BgL_tmpz00_6673); } 
BGL_U32VSET(BgL_vecz00_1717, BgL_iz00_1719, BgL_tmpz00_6672); }  else 
{ 
 obj_t BgL_auxz00_6682;
BgL_auxz00_6682 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2941z00zz__srfi4z00, BgL_vecz00_1717, 
(int)(BgL_l2244z00_3420), 
(int)(BgL_iz00_1719)); 
FAILURE(BgL_auxz00_6682,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1561z00_1728; obj_t BgL_arg1562z00_1729;
BgL_arg1561z00_1728 = 
(BgL_iz00_1719+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2653;
if(
PAIRP(BgL_lz00_1720))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2653 = BgL_lz00_1720; }  else 
{ 
 obj_t BgL_auxz00_6691;
BgL_auxz00_6691 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1720); 
FAILURE(BgL_auxz00_6691,BFALSE,BFALSE);} 
BgL_arg1562z00_1729 = 
CDR(BgL_pairz00_2653); } 
{ 
 obj_t BgL_lz00_6697; long BgL_iz00_6696;
BgL_iz00_6696 = BgL_arg1561z00_1728; 
BgL_lz00_6697 = BgL_arg1562z00_1729; 
BgL_lz00_1720 = BgL_lz00_6697; 
BgL_iz00_1719 = BgL_iz00_6696; 
goto BgL_zc3z04anonymousza31553ze3z87_1721;} } } } } } } } 

}



/* &list->u32vector */
obj_t BGl_z62listzd2ze3u32vectorz53zz__srfi4z00(obj_t BgL_envz00_3228, obj_t BgL_xz00_3229)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6698;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3229))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6698 = BgL_xz00_3229
; }  else 
{ 
 obj_t BgL_auxz00_6701;
BgL_auxz00_6701 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3012z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3229); 
FAILURE(BgL_auxz00_6701,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3u32vectorz31zz__srfi4z00(BgL_auxz00_6698);} } 

}



/* list->s64vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3s64vectorz31zz__srfi4z00(obj_t BgL_xz00_149)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1731;
BgL_lenz00_1731 = 
bgl_list_length(BgL_xz00_149); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1732;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6707;
BgL_tmpz00_6707 = 
(int32_t)(BgL_lenz00_1731); 
BgL_vecz00_1732 = 
BGL_ALLOC_S64VECTOR(BgL_tmpz00_6707); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1734; obj_t BgL_lz00_1735;
BgL_iz00_1734 = 0L; 
BgL_lz00_1735 = BgL_xz00_149; 
BgL_zc3z04anonymousza31563ze3z87_1736:
if(
(BgL_iz00_1734==BgL_lenz00_1731))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1732;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1565z00_1738;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3569z00_6712;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6713;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2656;
if(
PAIRP(BgL_lz00_1735))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2656 = BgL_lz00_1735; }  else 
{ 
 obj_t BgL_auxz00_6716;
BgL_auxz00_6716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1735); 
FAILURE(BgL_auxz00_6716,BFALSE,BFALSE);} 
BgL_tmpz00_6713 = 
CAR(BgL_pairz00_2656); } 
BgL_test3569z00_6712 = 
INTEGERP(BgL_tmpz00_6713); } 
if(BgL_test3569z00_6712)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1571z00_1741;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2657;
if(
PAIRP(BgL_lz00_1735))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2657 = BgL_lz00_1735; }  else 
{ 
 obj_t BgL_auxz00_6724;
BgL_auxz00_6724 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1735); 
FAILURE(BgL_auxz00_6724,BFALSE,BFALSE);} 
BgL_arg1571z00_1741 = 
CAR(BgL_pairz00_2657); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2658;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6729;
if(
INTEGERP(BgL_arg1571z00_1741))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6729 = BgL_arg1571z00_1741
; }  else 
{ 
 obj_t BgL_auxz00_6732;
BgL_auxz00_6732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1571z00_1741); 
FAILURE(BgL_auxz00_6732,BFALSE,BFALSE);} 
BgL_xz00_2658 = 
(long)CINT(BgL_tmpz00_6729); } 
{ /* Llib/srfi4.scm 796 */
 int64_t BgL_tmpz00_6737;
BgL_tmpz00_6737 = 
(int64_t)(BgL_xz00_2658); 
BgL_arg1565z00_1738 = 
BGL_INT64_TO_BINT64(BgL_tmpz00_6737); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2659;
if(
PAIRP(BgL_lz00_1735))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2659 = BgL_lz00_1735; }  else 
{ 
 obj_t BgL_auxz00_6742;
BgL_auxz00_6742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1735); 
FAILURE(BgL_auxz00_6742,BFALSE,BFALSE);} 
BgL_arg1565z00_1738 = 
CAR(BgL_pairz00_2659); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2248z00_3424;
BgL_l2248z00_3424 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1732); 
if(
BOUND_CHECK(BgL_iz00_1734, BgL_l2248z00_3424))
{ /* Llib/srfi4.scm 796 */
 int64_t BgL_tmpz00_6750;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6751;
if(
BGL_INT64P(BgL_arg1565z00_1738))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6751 = BgL_arg1565z00_1738
; }  else 
{ 
 obj_t BgL_auxz00_6754;
BgL_auxz00_6754 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2945z00zz__srfi4z00, BgL_arg1565z00_1738); 
FAILURE(BgL_auxz00_6754,BFALSE,BFALSE);} 
BgL_tmpz00_6750 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_6751); } 
BGL_S64VSET(BgL_vecz00_1732, BgL_iz00_1734, BgL_tmpz00_6750); }  else 
{ 
 obj_t BgL_auxz00_6760;
BgL_auxz00_6760 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2946z00zz__srfi4z00, BgL_vecz00_1732, 
(int)(BgL_l2248z00_3424), 
(int)(BgL_iz00_1734)); 
FAILURE(BgL_auxz00_6760,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1575z00_1743; obj_t BgL_arg1576z00_1744;
BgL_arg1575z00_1743 = 
(BgL_iz00_1734+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2661;
if(
PAIRP(BgL_lz00_1735))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2661 = BgL_lz00_1735; }  else 
{ 
 obj_t BgL_auxz00_6769;
BgL_auxz00_6769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1735); 
FAILURE(BgL_auxz00_6769,BFALSE,BFALSE);} 
BgL_arg1576z00_1744 = 
CDR(BgL_pairz00_2661); } 
{ 
 obj_t BgL_lz00_6775; long BgL_iz00_6774;
BgL_iz00_6774 = BgL_arg1575z00_1743; 
BgL_lz00_6775 = BgL_arg1576z00_1744; 
BgL_lz00_1735 = BgL_lz00_6775; 
BgL_iz00_1734 = BgL_iz00_6774; 
goto BgL_zc3z04anonymousza31563ze3z87_1736;} } } } } } } } 

}



/* &list->s64vector */
obj_t BGl_z62listzd2ze3s64vectorz53zz__srfi4z00(obj_t BgL_envz00_3230, obj_t BgL_xz00_3231)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6776;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3231))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6776 = BgL_xz00_3231
; }  else 
{ 
 obj_t BgL_auxz00_6779;
BgL_auxz00_6779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3013z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3231); 
FAILURE(BgL_auxz00_6779,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3s64vectorz31zz__srfi4z00(BgL_auxz00_6776);} } 

}



/* list->u64vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3u64vectorz31zz__srfi4z00(obj_t BgL_xz00_150)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1746;
BgL_lenz00_1746 = 
bgl_list_length(BgL_xz00_150); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1747;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6785;
BgL_tmpz00_6785 = 
(int32_t)(BgL_lenz00_1746); 
BgL_vecz00_1747 = 
BGL_ALLOC_U64VECTOR(BgL_tmpz00_6785); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1749; obj_t BgL_lz00_1750;
BgL_iz00_1749 = 0L; 
BgL_lz00_1750 = BgL_xz00_150; 
BgL_zc3z04anonymousza31577ze3z87_1751:
if(
(BgL_iz00_1749==BgL_lenz00_1746))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1747;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1579z00_1753;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3579z00_6790;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6791;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2664;
if(
PAIRP(BgL_lz00_1750))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2664 = BgL_lz00_1750; }  else 
{ 
 obj_t BgL_auxz00_6794;
BgL_auxz00_6794 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1750); 
FAILURE(BgL_auxz00_6794,BFALSE,BFALSE);} 
BgL_tmpz00_6791 = 
CAR(BgL_pairz00_2664); } 
BgL_test3579z00_6790 = 
INTEGERP(BgL_tmpz00_6791); } 
if(BgL_test3579z00_6790)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1582z00_1756;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2665;
if(
PAIRP(BgL_lz00_1750))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2665 = BgL_lz00_1750; }  else 
{ 
 obj_t BgL_auxz00_6802;
BgL_auxz00_6802 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1750); 
FAILURE(BgL_auxz00_6802,BFALSE,BFALSE);} 
BgL_arg1582z00_1756 = 
CAR(BgL_pairz00_2665); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2666;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6807;
if(
INTEGERP(BgL_arg1582z00_1756))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6807 = BgL_arg1582z00_1756
; }  else 
{ 
 obj_t BgL_auxz00_6810;
BgL_auxz00_6810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1582z00_1756); 
FAILURE(BgL_auxz00_6810,BFALSE,BFALSE);} 
BgL_xz00_2666 = 
(long)CINT(BgL_tmpz00_6807); } 
{ /* Llib/srfi4.scm 796 */
 uint64_t BgL_tmpz00_6815;
BgL_tmpz00_6815 = 
(uint64_t)(BgL_xz00_2666); 
BgL_arg1579z00_1753 = 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_6815); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2667;
if(
PAIRP(BgL_lz00_1750))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2667 = BgL_lz00_1750; }  else 
{ 
 obj_t BgL_auxz00_6820;
BgL_auxz00_6820 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1750); 
FAILURE(BgL_auxz00_6820,BFALSE,BFALSE);} 
BgL_arg1579z00_1753 = 
CAR(BgL_pairz00_2667); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2252z00_3428;
BgL_l2252z00_3428 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1747); 
if(
BOUND_CHECK(BgL_iz00_1749, BgL_l2252z00_3428))
{ /* Llib/srfi4.scm 796 */
 uint64_t BgL_tmpz00_6828;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6829;
if(
BGL_UINT64P(BgL_arg1579z00_1753))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6829 = BgL_arg1579z00_1753
; }  else 
{ 
 obj_t BgL_auxz00_6832;
BgL_auxz00_6832 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2950z00zz__srfi4z00, BgL_arg1579z00_1753); 
FAILURE(BgL_auxz00_6832,BFALSE,BFALSE);} 
BgL_tmpz00_6828 = 
BGL_BINT64_TO_INT64(BgL_tmpz00_6829); } 
BGL_U64VSET(BgL_vecz00_1747, BgL_iz00_1749, BgL_tmpz00_6828); }  else 
{ 
 obj_t BgL_auxz00_6838;
BgL_auxz00_6838 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2951z00zz__srfi4z00, BgL_vecz00_1747, 
(int)(BgL_l2252z00_3428), 
(int)(BgL_iz00_1749)); 
FAILURE(BgL_auxz00_6838,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1584z00_1758; obj_t BgL_arg1585z00_1759;
BgL_arg1584z00_1758 = 
(BgL_iz00_1749+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2669;
if(
PAIRP(BgL_lz00_1750))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2669 = BgL_lz00_1750; }  else 
{ 
 obj_t BgL_auxz00_6847;
BgL_auxz00_6847 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1750); 
FAILURE(BgL_auxz00_6847,BFALSE,BFALSE);} 
BgL_arg1585z00_1759 = 
CDR(BgL_pairz00_2669); } 
{ 
 obj_t BgL_lz00_6853; long BgL_iz00_6852;
BgL_iz00_6852 = BgL_arg1584z00_1758; 
BgL_lz00_6853 = BgL_arg1585z00_1759; 
BgL_lz00_1750 = BgL_lz00_6853; 
BgL_iz00_1749 = BgL_iz00_6852; 
goto BgL_zc3z04anonymousza31577ze3z87_1751;} } } } } } } } 

}



/* &list->u64vector */
obj_t BGl_z62listzd2ze3u64vectorz53zz__srfi4z00(obj_t BgL_envz00_3232, obj_t BgL_xz00_3233)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6854;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3233))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6854 = BgL_xz00_3233
; }  else 
{ 
 obj_t BgL_auxz00_6857;
BgL_auxz00_6857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3014z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3233); 
FAILURE(BgL_auxz00_6857,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3u64vectorz31zz__srfi4z00(BgL_auxz00_6854);} } 

}



/* list->f32vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3f32vectorz31zz__srfi4z00(obj_t BgL_xz00_151)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1761;
BgL_lenz00_1761 = 
bgl_list_length(BgL_xz00_151); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1762;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6863;
BgL_tmpz00_6863 = 
(int32_t)(BgL_lenz00_1761); 
BgL_vecz00_1762 = 
BGL_ALLOC_F32VECTOR(BgL_tmpz00_6863); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1764; obj_t BgL_lz00_1765;
BgL_iz00_1764 = 0L; 
BgL_lz00_1765 = BgL_xz00_151; 
BgL_zc3z04anonymousza31586ze3z87_1766:
if(
(BgL_iz00_1764==BgL_lenz00_1761))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1762;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1589z00_1768;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3589z00_6868;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6869;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2672;
if(
PAIRP(BgL_lz00_1765))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2672 = BgL_lz00_1765; }  else 
{ 
 obj_t BgL_auxz00_6872;
BgL_auxz00_6872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1765); 
FAILURE(BgL_auxz00_6872,BFALSE,BFALSE);} 
BgL_tmpz00_6869 = 
CAR(BgL_pairz00_2672); } 
BgL_test3589z00_6868 = 
INTEGERP(BgL_tmpz00_6869); } 
if(BgL_test3589z00_6868)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1593z00_1771;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2673;
if(
PAIRP(BgL_lz00_1765))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2673 = BgL_lz00_1765; }  else 
{ 
 obj_t BgL_auxz00_6880;
BgL_auxz00_6880 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1765); 
FAILURE(BgL_auxz00_6880,BFALSE,BFALSE);} 
BgL_arg1593z00_1771 = 
CAR(BgL_pairz00_2673); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2674;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6885;
if(
INTEGERP(BgL_arg1593z00_1771))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6885 = BgL_arg1593z00_1771
; }  else 
{ 
 obj_t BgL_auxz00_6888;
BgL_auxz00_6888 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1593z00_1771); 
FAILURE(BgL_auxz00_6888,BFALSE,BFALSE);} 
BgL_xz00_2674 = 
(long)CINT(BgL_tmpz00_6885); } 
{ /* Llib/srfi4.scm 796 */
 uint32_t BgL_tmpz00_6893;
BgL_tmpz00_6893 = 
(uint32_t)(BgL_xz00_2674); 
BgL_arg1589z00_1768 = 
BGL_UINT32_TO_BUINT32(BgL_tmpz00_6893); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2675;
if(
PAIRP(BgL_lz00_1765))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2675 = BgL_lz00_1765; }  else 
{ 
 obj_t BgL_auxz00_6898;
BgL_auxz00_6898 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1765); 
FAILURE(BgL_auxz00_6898,BFALSE,BFALSE);} 
BgL_arg1589z00_1768 = 
CAR(BgL_pairz00_2675); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2256z00_3432;
BgL_l2256z00_3432 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1762); 
if(
BOUND_CHECK(BgL_iz00_1764, BgL_l2256z00_3432))
{ /* Llib/srfi4.scm 796 */
 float BgL_tmpz00_6906;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6907;
if(
REALP(BgL_arg1589z00_1768))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6907 = BgL_arg1589z00_1768
; }  else 
{ 
 obj_t BgL_auxz00_6910;
BgL_auxz00_6910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2955z00zz__srfi4z00, BgL_arg1589z00_1768); 
FAILURE(BgL_auxz00_6910,BFALSE,BFALSE);} 
BgL_tmpz00_6906 = 
REAL_TO_FLOAT(BgL_tmpz00_6907); } 
BGL_F32VSET(BgL_vecz00_1762, BgL_iz00_1764, BgL_tmpz00_6906); }  else 
{ 
 obj_t BgL_auxz00_6916;
BgL_auxz00_6916 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2956z00zz__srfi4z00, BgL_vecz00_1762, 
(int)(BgL_l2256z00_3432), 
(int)(BgL_iz00_1764)); 
FAILURE(BgL_auxz00_6916,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1595z00_1773; obj_t BgL_arg1598z00_1774;
BgL_arg1595z00_1773 = 
(BgL_iz00_1764+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2677;
if(
PAIRP(BgL_lz00_1765))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2677 = BgL_lz00_1765; }  else 
{ 
 obj_t BgL_auxz00_6925;
BgL_auxz00_6925 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1765); 
FAILURE(BgL_auxz00_6925,BFALSE,BFALSE);} 
BgL_arg1598z00_1774 = 
CDR(BgL_pairz00_2677); } 
{ 
 obj_t BgL_lz00_6931; long BgL_iz00_6930;
BgL_iz00_6930 = BgL_arg1595z00_1773; 
BgL_lz00_6931 = BgL_arg1598z00_1774; 
BgL_lz00_1765 = BgL_lz00_6931; 
BgL_iz00_1764 = BgL_iz00_6930; 
goto BgL_zc3z04anonymousza31586ze3z87_1766;} } } } } } } } 

}



/* &list->f32vector */
obj_t BGl_z62listzd2ze3f32vectorz53zz__srfi4z00(obj_t BgL_envz00_3234, obj_t BgL_xz00_3235)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_6932;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3235))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_6932 = BgL_xz00_3235
; }  else 
{ 
 obj_t BgL_auxz00_6935;
BgL_auxz00_6935 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3015z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3235); 
FAILURE(BgL_auxz00_6935,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3f32vectorz31zz__srfi4z00(BgL_auxz00_6932);} } 

}



/* list->f64vector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3f64vectorz31zz__srfi4z00(obj_t BgL_xz00_152)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 long BgL_lenz00_1776;
BgL_lenz00_1776 = 
bgl_list_length(BgL_xz00_152); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_vecz00_1777;
{ /* Llib/srfi4.scm 796 */
 int32_t BgL_tmpz00_6941;
BgL_tmpz00_6941 = 
(int32_t)(BgL_lenz00_1776); 
BgL_vecz00_1777 = 
BGL_ALLOC_F64VECTOR(BgL_tmpz00_6941); } 
{ /* Llib/srfi4.scm 796 */

{ 
 long BgL_iz00_1779; obj_t BgL_lz00_1780;
BgL_iz00_1779 = 0L; 
BgL_lz00_1780 = BgL_xz00_152; 
BgL_zc3z04anonymousza31599ze3z87_1781:
if(
(BgL_iz00_1779==BgL_lenz00_1776))
{ /* Llib/srfi4.scm 796 */
return BgL_vecz00_1777;}  else 
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1601z00_1783;
{ /* Llib/srfi4.scm 796 */
 bool_t BgL_test3599z00_6946;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6947;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2680;
if(
PAIRP(BgL_lz00_1780))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2680 = BgL_lz00_1780; }  else 
{ 
 obj_t BgL_auxz00_6950;
BgL_auxz00_6950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1780); 
FAILURE(BgL_auxz00_6950,BFALSE,BFALSE);} 
BgL_tmpz00_6947 = 
CAR(BgL_pairz00_2680); } 
BgL_test3599z00_6946 = 
INTEGERP(BgL_tmpz00_6947); } 
if(BgL_test3599z00_6946)
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_arg1605z00_1786;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2681;
if(
PAIRP(BgL_lz00_1780))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2681 = BgL_lz00_1780; }  else 
{ 
 obj_t BgL_auxz00_6958;
BgL_auxz00_6958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1780); 
FAILURE(BgL_auxz00_6958,BFALSE,BFALSE);} 
BgL_arg1605z00_1786 = 
CAR(BgL_pairz00_2681); } 
{ /* Llib/srfi4.scm 796 */
 long BgL_xz00_2682;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6963;
if(
INTEGERP(BgL_arg1605z00_1786))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6963 = BgL_arg1605z00_1786
; }  else 
{ 
 obj_t BgL_auxz00_6966;
BgL_auxz00_6966 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_arg1605z00_1786); 
FAILURE(BgL_auxz00_6966,BFALSE,BFALSE);} 
BgL_xz00_2682 = 
(long)CINT(BgL_tmpz00_6963); } 
{ /* Llib/srfi4.scm 796 */
 uint64_t BgL_tmpz00_6971;
BgL_tmpz00_6971 = 
(uint64_t)(BgL_xz00_2682); 
BgL_arg1601z00_1783 = 
BGL_UINT64_TO_BUINT64(BgL_tmpz00_6971); } } }  else 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2683;
if(
PAIRP(BgL_lz00_1780))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2683 = BgL_lz00_1780; }  else 
{ 
 obj_t BgL_auxz00_6976;
BgL_auxz00_6976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1780); 
FAILURE(BgL_auxz00_6976,BFALSE,BFALSE);} 
BgL_arg1601z00_1783 = 
CAR(BgL_pairz00_2683); } } 
{ /* Llib/srfi4.scm 796 */
 long BgL_l2260z00_3436;
BgL_l2260z00_3436 = 
BGL_HVECTOR_LENGTH(BgL_vecz00_1777); 
if(
BOUND_CHECK(BgL_iz00_1779, BgL_l2260z00_3436))
{ /* Llib/srfi4.scm 796 */
 double BgL_tmpz00_6984;
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_tmpz00_6985;
if(
REALP(BgL_arg1601z00_1783))
{ /* Llib/srfi4.scm 796 */
BgL_tmpz00_6985 = BgL_arg1601z00_1783
; }  else 
{ 
 obj_t BgL_auxz00_6988;
BgL_auxz00_6988 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string2955z00zz__srfi4z00, BgL_arg1601z00_1783); 
FAILURE(BgL_auxz00_6988,BFALSE,BFALSE);} 
BgL_tmpz00_6984 = 
REAL_TO_DOUBLE(BgL_tmpz00_6985); } 
BGL_F64VSET(BgL_vecz00_1777, BgL_iz00_1779, BgL_tmpz00_6984); }  else 
{ 
 obj_t BgL_auxz00_6994;
BgL_auxz00_6994 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string2960z00zz__srfi4z00, BgL_vecz00_1777, 
(int)(BgL_l2260z00_3436), 
(int)(BgL_iz00_1779)); 
FAILURE(BgL_auxz00_6994,BFALSE,BFALSE);} } BUNSPEC; } 
{ /* Llib/srfi4.scm 796 */
 long BgL_arg1607z00_1788; obj_t BgL_arg1608z00_1789;
BgL_arg1607z00_1788 = 
(BgL_iz00_1779+1L); 
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_pairz00_2685;
if(
PAIRP(BgL_lz00_1780))
{ /* Llib/srfi4.scm 796 */
BgL_pairz00_2685 = BgL_lz00_1780; }  else 
{ 
 obj_t BgL_auxz00_7003;
BgL_auxz00_7003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3005z00zz__srfi4z00, BGl_string3006z00zz__srfi4z00, BgL_lz00_1780); 
FAILURE(BgL_auxz00_7003,BFALSE,BFALSE);} 
BgL_arg1608z00_1789 = 
CDR(BgL_pairz00_2685); } 
{ 
 obj_t BgL_lz00_7009; long BgL_iz00_7008;
BgL_iz00_7008 = BgL_arg1607z00_1788; 
BgL_lz00_7009 = BgL_arg1608z00_1789; 
BgL_lz00_1780 = BgL_lz00_7009; 
BgL_iz00_1779 = BgL_iz00_7008; 
goto BgL_zc3z04anonymousza31599ze3z87_1781;} } } } } } } } 

}



/* &list->f64vector */
obj_t BGl_z62listzd2ze3f64vectorz53zz__srfi4z00(obj_t BgL_envz00_3236, obj_t BgL_xz00_3237)
{
{ /* Llib/srfi4.scm 796 */
{ /* Llib/srfi4.scm 796 */
 obj_t BgL_auxz00_7010;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_xz00_3237))
{ /* Llib/srfi4.scm 796 */
BgL_auxz00_7010 = BgL_xz00_3237
; }  else 
{ 
 obj_t BgL_auxz00_7013;
BgL_auxz00_7013 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(31419L), BGl_string3016z00zz__srfi4z00, BGl_string2909z00zz__srfi4z00, BgL_xz00_3237); 
FAILURE(BgL_auxz00_7013,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3f64vectorz31zz__srfi4z00(BgL_auxz00_7010);} } 

}



/* _s8vector-copy! */
obj_t BGl__s8vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1299z00_159, obj_t BgL_opt1298z00_158)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1791; obj_t BgL_tstartz00_1792; obj_t BgL_sourcez00_1793;
BgL_targetz00_1791 = 
VECTOR_REF(BgL_opt1298z00_158,0L); 
BgL_tstartz00_1792 = 
VECTOR_REF(BgL_opt1298z00_158,1L); 
BgL_sourcez00_1793 = 
VECTOR_REF(BgL_opt1298z00_158,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1298z00_158)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1797;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2686;
if(
BGL_S8VECTORP(BgL_sourcez00_1793))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2686 = BgL_sourcez00_1793; }  else 
{ 
 obj_t BgL_auxz00_7023;
BgL_auxz00_7023 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_sourcez00_1793); 
FAILURE(BgL_auxz00_7023,BFALSE,BFALSE);} 
BgL_sendz00_1797 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2686); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7044; long BgL_auxz00_7035; obj_t BgL_auxz00_7028;
if(
BGL_S8VECTORP(BgL_sourcez00_1793))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7044 = BgL_sourcez00_1793
; }  else 
{ 
 obj_t BgL_auxz00_7047;
BgL_auxz00_7047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_sourcez00_1793); 
FAILURE(BgL_auxz00_7047,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7036;
if(
INTEGERP(BgL_tstartz00_1792))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7036 = BgL_tstartz00_1792
; }  else 
{ 
 obj_t BgL_auxz00_7039;
BgL_auxz00_7039 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1792); 
FAILURE(BgL_auxz00_7039,BFALSE,BFALSE);} 
BgL_auxz00_7035 = 
(long)CINT(BgL_tmpz00_7036); } 
if(
BGL_S8VECTORP(BgL_targetz00_1791))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7028 = BgL_targetz00_1791
; }  else 
{ 
 obj_t BgL_auxz00_7031;
BgL_auxz00_7031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_targetz00_1791); 
FAILURE(BgL_auxz00_7031,BFALSE,BFALSE);} 
return 
BGl_s8vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7028, BgL_auxz00_7035, BgL_auxz00_7044, 
BINT(0L), 
BINT(BgL_sendz00_1797));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1798;
BgL_sstartz00_1798 = 
VECTOR_REF(BgL_opt1298z00_158,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1799;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2687;
if(
BGL_S8VECTORP(BgL_sourcez00_1793))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2687 = BgL_sourcez00_1793; }  else 
{ 
 obj_t BgL_auxz00_7057;
BgL_auxz00_7057 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_sourcez00_1793); 
FAILURE(BgL_auxz00_7057,BFALSE,BFALSE);} 
BgL_sendz00_1799 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2687); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7078; long BgL_auxz00_7069; obj_t BgL_auxz00_7062;
if(
BGL_S8VECTORP(BgL_sourcez00_1793))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7078 = BgL_sourcez00_1793
; }  else 
{ 
 obj_t BgL_auxz00_7081;
BgL_auxz00_7081 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_sourcez00_1793); 
FAILURE(BgL_auxz00_7081,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7070;
if(
INTEGERP(BgL_tstartz00_1792))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7070 = BgL_tstartz00_1792
; }  else 
{ 
 obj_t BgL_auxz00_7073;
BgL_auxz00_7073 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1792); 
FAILURE(BgL_auxz00_7073,BFALSE,BFALSE);} 
BgL_auxz00_7069 = 
(long)CINT(BgL_tmpz00_7070); } 
if(
BGL_S8VECTORP(BgL_targetz00_1791))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7062 = BgL_targetz00_1791
; }  else 
{ 
 obj_t BgL_auxz00_7065;
BgL_auxz00_7065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_targetz00_1791); 
FAILURE(BgL_auxz00_7065,BFALSE,BFALSE);} 
return 
BGl_s8vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7062, BgL_auxz00_7069, BgL_auxz00_7078, BgL_sstartz00_1798, 
BINT(BgL_sendz00_1799));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1800;
BgL_sstartz00_1800 = 
VECTOR_REF(BgL_opt1298z00_158,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1801;
BgL_sendz00_1801 = 
VECTOR_REF(BgL_opt1298z00_158,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7105; long BgL_auxz00_7096; obj_t BgL_auxz00_7089;
if(
BGL_S8VECTORP(BgL_sourcez00_1793))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7105 = BgL_sourcez00_1793
; }  else 
{ 
 obj_t BgL_auxz00_7108;
BgL_auxz00_7108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_sourcez00_1793); 
FAILURE(BgL_auxz00_7108,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7097;
if(
INTEGERP(BgL_tstartz00_1792))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7097 = BgL_tstartz00_1792
; }  else 
{ 
 obj_t BgL_auxz00_7100;
BgL_auxz00_7100 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1792); 
FAILURE(BgL_auxz00_7100,BFALSE,BFALSE);} 
BgL_auxz00_7096 = 
(long)CINT(BgL_tmpz00_7097); } 
if(
BGL_S8VECTORP(BgL_targetz00_1791))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7089 = BgL_targetz00_1791
; }  else 
{ 
 obj_t BgL_auxz00_7092;
BgL_auxz00_7092 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3020z00zz__srfi4z00, BGl_string2890z00zz__srfi4z00, BgL_targetz00_1791); 
FAILURE(BgL_auxz00_7092,BFALSE,BFALSE);} 
return 
BGl_s8vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7089, BgL_auxz00_7096, BgL_auxz00_7105, BgL_sstartz00_1800, BgL_sendz00_1801);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3017z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1298z00_158)));} } } } 

}



/* s8vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_s8vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_153, long BgL_tstartz00_154, obj_t BgL_sourcez00_155, obj_t BgL_sstartz00_156, obj_t BgL_sendz00_157)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_154<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3018z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_154)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3620z00_7122;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2689;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7123;
if(
INTEGERP(BgL_sstartz00_156))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7123 = BgL_sstartz00_156
; }  else 
{ 
 obj_t BgL_auxz00_7126;
BgL_auxz00_7126 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_156); 
FAILURE(BgL_auxz00_7126,BFALSE,BFALSE);} 
BgL_n1z00_2689 = 
(long)CINT(BgL_tmpz00_7123); } 
BgL_test3620z00_7122 = 
(BgL_n1z00_2689<0L); } 
if(BgL_test3620z00_7122)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3018z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_156); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3622z00_7133;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1618z00_1816;
BgL_arg1618z00_1816 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_155); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2691;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7135;
if(
INTEGERP(BgL_sendz00_157))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7135 = BgL_sendz00_157
; }  else 
{ 
 obj_t BgL_auxz00_7138;
BgL_auxz00_7138 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_157); 
FAILURE(BgL_auxz00_7138,BFALSE,BFALSE);} 
BgL_n1z00_2691 = 
(long)CINT(BgL_tmpz00_7135); } 
BgL_test3622z00_7133 = 
(BgL_n1z00_2691>BgL_arg1618z00_1816); } } 
if(BgL_test3622z00_7133)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3018z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_157); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3624z00_7145;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2693; long BgL_n2z00_2694;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7146;
if(
INTEGERP(BgL_sendz00_157))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7146 = BgL_sendz00_157
; }  else 
{ 
 obj_t BgL_auxz00_7149;
BgL_auxz00_7149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_157); 
FAILURE(BgL_auxz00_7149,BFALSE,BFALSE);} 
BgL_n1z00_2693 = 
(long)CINT(BgL_tmpz00_7146); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7154;
if(
INTEGERP(BgL_sstartz00_156))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7154 = BgL_sstartz00_156
; }  else 
{ 
 obj_t BgL_auxz00_7157;
BgL_auxz00_7157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_156); 
FAILURE(BgL_auxz00_7157,BFALSE,BFALSE);} 
BgL_n2z00_2694 = 
(long)CINT(BgL_tmpz00_7154); } 
BgL_test3624z00_7145 = 
(BgL_n1z00_2693<BgL_n2z00_2694); } 
if(BgL_test3624z00_7145)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3018z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_157); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3627z00_7164;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1206z00_1813;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2695; long BgL_za72za7_2696;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7165;
if(
INTEGERP(BgL_sendz00_157))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7165 = BgL_sendz00_157
; }  else 
{ 
 obj_t BgL_auxz00_7168;
BgL_auxz00_7168 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_157); 
FAILURE(BgL_auxz00_7168,BFALSE,BFALSE);} 
BgL_za71za7_2695 = 
(long)CINT(BgL_tmpz00_7165); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7173;
if(
INTEGERP(BgL_sstartz00_156))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7173 = BgL_sstartz00_156
; }  else 
{ 
 obj_t BgL_auxz00_7176;
BgL_auxz00_7176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_156); 
FAILURE(BgL_auxz00_7176,BFALSE,BFALSE);} 
BgL_za72za7_2696 = 
(long)CINT(BgL_tmpz00_7173); } 
BgL_a1206z00_1813 = 
(BgL_za71za7_2695-BgL_za72za7_2696); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1207z00_1814;
BgL_b1207z00_1814 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_153); 
{ /* Llib/srfi4.scm 838 */

BgL_test3627z00_7164 = 
(BgL_a1206z00_1813>BgL_b1207z00_1814); } } } 
if(BgL_test3627z00_7164)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1617z00_1812;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2700; long BgL_za72za7_2701;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7184;
if(
INTEGERP(BgL_sendz00_157))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7184 = BgL_sendz00_157
; }  else 
{ 
 obj_t BgL_auxz00_7187;
BgL_auxz00_7187 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_157); 
FAILURE(BgL_auxz00_7187,BFALSE,BFALSE);} 
BgL_za71za7_2700 = 
(long)CINT(BgL_tmpz00_7184); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7192;
if(
INTEGERP(BgL_sstartz00_156))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7192 = BgL_sstartz00_156
; }  else 
{ 
 obj_t BgL_auxz00_7195;
BgL_auxz00_7195 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_156); 
FAILURE(BgL_auxz00_7195,BFALSE,BFALSE);} 
BgL_za72za7_2701 = 
(long)CINT(BgL_tmpz00_7192); } 
BgL_arg1617z00_1812 = 
(BgL_za71za7_2700-BgL_za72za7_2701); } 
BGl_errorz00zz__errorz00(BGl_string3018z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1617z00_1812)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_7212; long BgL_tmpz00_7203;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7213;
if(
INTEGERP(BgL_sendz00_157))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7213 = BgL_sendz00_157
; }  else 
{ 
 obj_t BgL_auxz00_7216;
BgL_auxz00_7216 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_157); 
FAILURE(BgL_auxz00_7216,BFALSE,BFALSE);} 
BgL_auxz00_7212 = 
(long)CINT(BgL_tmpz00_7213); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7204;
if(
INTEGERP(BgL_sstartz00_156))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7204 = BgL_sstartz00_156
; }  else 
{ 
 obj_t BgL_auxz00_7207;
BgL_auxz00_7207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3018z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_156); 
FAILURE(BgL_auxz00_7207,BFALSE,BFALSE);} 
BgL_tmpz00_7203 = 
(long)CINT(BgL_tmpz00_7204); } 
BGL_SU8VECTOR_COPY(BgL_targetz00_153, BgL_tstartz00_154, BgL_sourcez00_155, BgL_tmpz00_7203, BgL_auxz00_7212); } 
return BUNSPEC;} 

}



/* _u8vector-copy! */
obj_t BGl__u8vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1303z00_166, obj_t BgL_opt1302z00_165)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1817; obj_t BgL_tstartz00_1818; obj_t BgL_sourcez00_1819;
BgL_targetz00_1817 = 
VECTOR_REF(BgL_opt1302z00_165,0L); 
BgL_tstartz00_1818 = 
VECTOR_REF(BgL_opt1302z00_165,1L); 
BgL_sourcez00_1819 = 
VECTOR_REF(BgL_opt1302z00_165,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1302z00_165)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1823;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2702;
if(
BGL_U8VECTORP(BgL_sourcez00_1819))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2702 = BgL_sourcez00_1819; }  else 
{ 
 obj_t BgL_auxz00_7227;
BgL_auxz00_7227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_sourcez00_1819); 
FAILURE(BgL_auxz00_7227,BFALSE,BFALSE);} 
BgL_sendz00_1823 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2702); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7248; long BgL_auxz00_7239; obj_t BgL_auxz00_7232;
if(
BGL_U8VECTORP(BgL_sourcez00_1819))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7248 = BgL_sourcez00_1819
; }  else 
{ 
 obj_t BgL_auxz00_7251;
BgL_auxz00_7251 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_sourcez00_1819); 
FAILURE(BgL_auxz00_7251,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7240;
if(
INTEGERP(BgL_tstartz00_1818))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7240 = BgL_tstartz00_1818
; }  else 
{ 
 obj_t BgL_auxz00_7243;
BgL_auxz00_7243 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1818); 
FAILURE(BgL_auxz00_7243,BFALSE,BFALSE);} 
BgL_auxz00_7239 = 
(long)CINT(BgL_tmpz00_7240); } 
if(
BGL_U8VECTORP(BgL_targetz00_1817))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7232 = BgL_targetz00_1817
; }  else 
{ 
 obj_t BgL_auxz00_7235;
BgL_auxz00_7235 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_targetz00_1817); 
FAILURE(BgL_auxz00_7235,BFALSE,BFALSE);} 
return 
BGl_u8vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7232, BgL_auxz00_7239, BgL_auxz00_7248, 
BINT(0L), 
BINT(BgL_sendz00_1823));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1824;
BgL_sstartz00_1824 = 
VECTOR_REF(BgL_opt1302z00_165,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1825;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2703;
if(
BGL_U8VECTORP(BgL_sourcez00_1819))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2703 = BgL_sourcez00_1819; }  else 
{ 
 obj_t BgL_auxz00_7261;
BgL_auxz00_7261 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_sourcez00_1819); 
FAILURE(BgL_auxz00_7261,BFALSE,BFALSE);} 
BgL_sendz00_1825 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2703); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7282; long BgL_auxz00_7273; obj_t BgL_auxz00_7266;
if(
BGL_U8VECTORP(BgL_sourcez00_1819))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7282 = BgL_sourcez00_1819
; }  else 
{ 
 obj_t BgL_auxz00_7285;
BgL_auxz00_7285 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_sourcez00_1819); 
FAILURE(BgL_auxz00_7285,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7274;
if(
INTEGERP(BgL_tstartz00_1818))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7274 = BgL_tstartz00_1818
; }  else 
{ 
 obj_t BgL_auxz00_7277;
BgL_auxz00_7277 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1818); 
FAILURE(BgL_auxz00_7277,BFALSE,BFALSE);} 
BgL_auxz00_7273 = 
(long)CINT(BgL_tmpz00_7274); } 
if(
BGL_U8VECTORP(BgL_targetz00_1817))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7266 = BgL_targetz00_1817
; }  else 
{ 
 obj_t BgL_auxz00_7269;
BgL_auxz00_7269 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_targetz00_1817); 
FAILURE(BgL_auxz00_7269,BFALSE,BFALSE);} 
return 
BGl_u8vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7266, BgL_auxz00_7273, BgL_auxz00_7282, BgL_sstartz00_1824, 
BINT(BgL_sendz00_1825));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1826;
BgL_sstartz00_1826 = 
VECTOR_REF(BgL_opt1302z00_165,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1827;
BgL_sendz00_1827 = 
VECTOR_REF(BgL_opt1302z00_165,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7309; long BgL_auxz00_7300; obj_t BgL_auxz00_7293;
if(
BGL_U8VECTORP(BgL_sourcez00_1819))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7309 = BgL_sourcez00_1819
; }  else 
{ 
 obj_t BgL_auxz00_7312;
BgL_auxz00_7312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_sourcez00_1819); 
FAILURE(BgL_auxz00_7312,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7301;
if(
INTEGERP(BgL_tstartz00_1818))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7301 = BgL_tstartz00_1818
; }  else 
{ 
 obj_t BgL_auxz00_7304;
BgL_auxz00_7304 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1818); 
FAILURE(BgL_auxz00_7304,BFALSE,BFALSE);} 
BgL_auxz00_7300 = 
(long)CINT(BgL_tmpz00_7301); } 
if(
BGL_U8VECTORP(BgL_targetz00_1817))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7293 = BgL_targetz00_1817
; }  else 
{ 
 obj_t BgL_auxz00_7296;
BgL_auxz00_7296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3027z00zz__srfi4z00, BGl_string2892z00zz__srfi4z00, BgL_targetz00_1817); 
FAILURE(BgL_auxz00_7296,BFALSE,BFALSE);} 
return 
BGl_u8vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7293, BgL_auxz00_7300, BgL_auxz00_7309, BgL_sstartz00_1826, BgL_sendz00_1827);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3025z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1302z00_165)));} } } } 

}



/* u8vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_u8vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_160, long BgL_tstartz00_161, obj_t BgL_sourcez00_162, obj_t BgL_sstartz00_163, obj_t BgL_sendz00_164)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_161<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3026z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_161)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3646z00_7326;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2705;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7327;
if(
INTEGERP(BgL_sstartz00_163))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7327 = BgL_sstartz00_163
; }  else 
{ 
 obj_t BgL_auxz00_7330;
BgL_auxz00_7330 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_163); 
FAILURE(BgL_auxz00_7330,BFALSE,BFALSE);} 
BgL_n1z00_2705 = 
(long)CINT(BgL_tmpz00_7327); } 
BgL_test3646z00_7326 = 
(BgL_n1z00_2705<0L); } 
if(BgL_test3646z00_7326)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3026z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_163); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3648z00_7337;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1628z00_1842;
BgL_arg1628z00_1842 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_162); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2707;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7339;
if(
INTEGERP(BgL_sendz00_164))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7339 = BgL_sendz00_164
; }  else 
{ 
 obj_t BgL_auxz00_7342;
BgL_auxz00_7342 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_164); 
FAILURE(BgL_auxz00_7342,BFALSE,BFALSE);} 
BgL_n1z00_2707 = 
(long)CINT(BgL_tmpz00_7339); } 
BgL_test3648z00_7337 = 
(BgL_n1z00_2707>BgL_arg1628z00_1842); } } 
if(BgL_test3648z00_7337)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3026z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_164); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3650z00_7349;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2709; long BgL_n2z00_2710;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7350;
if(
INTEGERP(BgL_sendz00_164))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7350 = BgL_sendz00_164
; }  else 
{ 
 obj_t BgL_auxz00_7353;
BgL_auxz00_7353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_164); 
FAILURE(BgL_auxz00_7353,BFALSE,BFALSE);} 
BgL_n1z00_2709 = 
(long)CINT(BgL_tmpz00_7350); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7358;
if(
INTEGERP(BgL_sstartz00_163))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7358 = BgL_sstartz00_163
; }  else 
{ 
 obj_t BgL_auxz00_7361;
BgL_auxz00_7361 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_163); 
FAILURE(BgL_auxz00_7361,BFALSE,BFALSE);} 
BgL_n2z00_2710 = 
(long)CINT(BgL_tmpz00_7358); } 
BgL_test3650z00_7349 = 
(BgL_n1z00_2709<BgL_n2z00_2710); } 
if(BgL_test3650z00_7349)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3026z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_164); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3653z00_7368;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1209z00_1839;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2711; long BgL_za72za7_2712;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7369;
if(
INTEGERP(BgL_sendz00_164))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7369 = BgL_sendz00_164
; }  else 
{ 
 obj_t BgL_auxz00_7372;
BgL_auxz00_7372 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_164); 
FAILURE(BgL_auxz00_7372,BFALSE,BFALSE);} 
BgL_za71za7_2711 = 
(long)CINT(BgL_tmpz00_7369); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7377;
if(
INTEGERP(BgL_sstartz00_163))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7377 = BgL_sstartz00_163
; }  else 
{ 
 obj_t BgL_auxz00_7380;
BgL_auxz00_7380 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_163); 
FAILURE(BgL_auxz00_7380,BFALSE,BFALSE);} 
BgL_za72za7_2712 = 
(long)CINT(BgL_tmpz00_7377); } 
BgL_a1209z00_1839 = 
(BgL_za71za7_2711-BgL_za72za7_2712); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1210z00_1840;
BgL_b1210z00_1840 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_160); 
{ /* Llib/srfi4.scm 838 */

BgL_test3653z00_7368 = 
(BgL_a1209z00_1839>BgL_b1210z00_1840); } } } 
if(BgL_test3653z00_7368)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1627z00_1838;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2716; long BgL_za72za7_2717;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7388;
if(
INTEGERP(BgL_sendz00_164))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7388 = BgL_sendz00_164
; }  else 
{ 
 obj_t BgL_auxz00_7391;
BgL_auxz00_7391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_164); 
FAILURE(BgL_auxz00_7391,BFALSE,BFALSE);} 
BgL_za71za7_2716 = 
(long)CINT(BgL_tmpz00_7388); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7396;
if(
INTEGERP(BgL_sstartz00_163))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7396 = BgL_sstartz00_163
; }  else 
{ 
 obj_t BgL_auxz00_7399;
BgL_auxz00_7399 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_163); 
FAILURE(BgL_auxz00_7399,BFALSE,BFALSE);} 
BgL_za72za7_2717 = 
(long)CINT(BgL_tmpz00_7396); } 
BgL_arg1627z00_1838 = 
(BgL_za71za7_2716-BgL_za72za7_2717); } 
BGl_errorz00zz__errorz00(BGl_string3026z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1627z00_1838)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_7416; long BgL_tmpz00_7407;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7417;
if(
INTEGERP(BgL_sendz00_164))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7417 = BgL_sendz00_164
; }  else 
{ 
 obj_t BgL_auxz00_7420;
BgL_auxz00_7420 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_164); 
FAILURE(BgL_auxz00_7420,BFALSE,BFALSE);} 
BgL_auxz00_7416 = 
(long)CINT(BgL_tmpz00_7417); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7408;
if(
INTEGERP(BgL_sstartz00_163))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7408 = BgL_sstartz00_163
; }  else 
{ 
 obj_t BgL_auxz00_7411;
BgL_auxz00_7411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3026z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_163); 
FAILURE(BgL_auxz00_7411,BFALSE,BFALSE);} 
BgL_tmpz00_7407 = 
(long)CINT(BgL_tmpz00_7408); } 
BGL_SU8VECTOR_COPY(BgL_targetz00_160, BgL_tstartz00_161, BgL_sourcez00_162, BgL_tmpz00_7407, BgL_auxz00_7416); } 
return BUNSPEC;} 

}



/* _s16vector-copy! */
obj_t BGl__s16vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1307z00_173, obj_t BgL_opt1306z00_172)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1843; obj_t BgL_tstartz00_1844; obj_t BgL_sourcez00_1845;
BgL_targetz00_1843 = 
VECTOR_REF(BgL_opt1306z00_172,0L); 
BgL_tstartz00_1844 = 
VECTOR_REF(BgL_opt1306z00_172,1L); 
BgL_sourcez00_1845 = 
VECTOR_REF(BgL_opt1306z00_172,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1306z00_172)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1849;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2718;
if(
BGL_S16VECTORP(BgL_sourcez00_1845))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2718 = BgL_sourcez00_1845; }  else 
{ 
 obj_t BgL_auxz00_7431;
BgL_auxz00_7431 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_sourcez00_1845); 
FAILURE(BgL_auxz00_7431,BFALSE,BFALSE);} 
BgL_sendz00_1849 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2718); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7452; long BgL_auxz00_7443; obj_t BgL_auxz00_7436;
if(
BGL_S16VECTORP(BgL_sourcez00_1845))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7452 = BgL_sourcez00_1845
; }  else 
{ 
 obj_t BgL_auxz00_7455;
BgL_auxz00_7455 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_sourcez00_1845); 
FAILURE(BgL_auxz00_7455,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7444;
if(
INTEGERP(BgL_tstartz00_1844))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7444 = BgL_tstartz00_1844
; }  else 
{ 
 obj_t BgL_auxz00_7447;
BgL_auxz00_7447 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1844); 
FAILURE(BgL_auxz00_7447,BFALSE,BFALSE);} 
BgL_auxz00_7443 = 
(long)CINT(BgL_tmpz00_7444); } 
if(
BGL_S16VECTORP(BgL_targetz00_1843))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7436 = BgL_targetz00_1843
; }  else 
{ 
 obj_t BgL_auxz00_7439;
BgL_auxz00_7439 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_targetz00_1843); 
FAILURE(BgL_auxz00_7439,BFALSE,BFALSE);} 
return 
BGl_s16vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7436, BgL_auxz00_7443, BgL_auxz00_7452, 
BINT(0L), 
BINT(BgL_sendz00_1849));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1850;
BgL_sstartz00_1850 = 
VECTOR_REF(BgL_opt1306z00_172,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1851;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2719;
if(
BGL_S16VECTORP(BgL_sourcez00_1845))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2719 = BgL_sourcez00_1845; }  else 
{ 
 obj_t BgL_auxz00_7465;
BgL_auxz00_7465 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_sourcez00_1845); 
FAILURE(BgL_auxz00_7465,BFALSE,BFALSE);} 
BgL_sendz00_1851 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2719); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7486; long BgL_auxz00_7477; obj_t BgL_auxz00_7470;
if(
BGL_S16VECTORP(BgL_sourcez00_1845))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7486 = BgL_sourcez00_1845
; }  else 
{ 
 obj_t BgL_auxz00_7489;
BgL_auxz00_7489 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_sourcez00_1845); 
FAILURE(BgL_auxz00_7489,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7478;
if(
INTEGERP(BgL_tstartz00_1844))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7478 = BgL_tstartz00_1844
; }  else 
{ 
 obj_t BgL_auxz00_7481;
BgL_auxz00_7481 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1844); 
FAILURE(BgL_auxz00_7481,BFALSE,BFALSE);} 
BgL_auxz00_7477 = 
(long)CINT(BgL_tmpz00_7478); } 
if(
BGL_S16VECTORP(BgL_targetz00_1843))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7470 = BgL_targetz00_1843
; }  else 
{ 
 obj_t BgL_auxz00_7473;
BgL_auxz00_7473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_targetz00_1843); 
FAILURE(BgL_auxz00_7473,BFALSE,BFALSE);} 
return 
BGl_s16vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7470, BgL_auxz00_7477, BgL_auxz00_7486, BgL_sstartz00_1850, 
BINT(BgL_sendz00_1851));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1852;
BgL_sstartz00_1852 = 
VECTOR_REF(BgL_opt1306z00_172,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1853;
BgL_sendz00_1853 = 
VECTOR_REF(BgL_opt1306z00_172,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7513; long BgL_auxz00_7504; obj_t BgL_auxz00_7497;
if(
BGL_S16VECTORP(BgL_sourcez00_1845))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7513 = BgL_sourcez00_1845
; }  else 
{ 
 obj_t BgL_auxz00_7516;
BgL_auxz00_7516 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_sourcez00_1845); 
FAILURE(BgL_auxz00_7516,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7505;
if(
INTEGERP(BgL_tstartz00_1844))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7505 = BgL_tstartz00_1844
; }  else 
{ 
 obj_t BgL_auxz00_7508;
BgL_auxz00_7508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1844); 
FAILURE(BgL_auxz00_7508,BFALSE,BFALSE);} 
BgL_auxz00_7504 = 
(long)CINT(BgL_tmpz00_7505); } 
if(
BGL_S16VECTORP(BgL_targetz00_1843))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7497 = BgL_targetz00_1843
; }  else 
{ 
 obj_t BgL_auxz00_7500;
BgL_auxz00_7500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3030z00zz__srfi4z00, BGl_string2894z00zz__srfi4z00, BgL_targetz00_1843); 
FAILURE(BgL_auxz00_7500,BFALSE,BFALSE);} 
return 
BGl_s16vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7497, BgL_auxz00_7504, BgL_auxz00_7513, BgL_sstartz00_1852, BgL_sendz00_1853);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3028z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1306z00_172)));} } } } 

}



/* s16vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_s16vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_167, long BgL_tstartz00_168, obj_t BgL_sourcez00_169, obj_t BgL_sstartz00_170, obj_t BgL_sendz00_171)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_168<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3029z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_168)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3672z00_7530;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2721;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7531;
if(
INTEGERP(BgL_sstartz00_170))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7531 = BgL_sstartz00_170
; }  else 
{ 
 obj_t BgL_auxz00_7534;
BgL_auxz00_7534 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_170); 
FAILURE(BgL_auxz00_7534,BFALSE,BFALSE);} 
BgL_n1z00_2721 = 
(long)CINT(BgL_tmpz00_7531); } 
BgL_test3672z00_7530 = 
(BgL_n1z00_2721<0L); } 
if(BgL_test3672z00_7530)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3029z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_170); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3674z00_7541;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1640z00_1868;
BgL_arg1640z00_1868 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_169); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2723;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7543;
if(
INTEGERP(BgL_sendz00_171))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7543 = BgL_sendz00_171
; }  else 
{ 
 obj_t BgL_auxz00_7546;
BgL_auxz00_7546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_171); 
FAILURE(BgL_auxz00_7546,BFALSE,BFALSE);} 
BgL_n1z00_2723 = 
(long)CINT(BgL_tmpz00_7543); } 
BgL_test3674z00_7541 = 
(BgL_n1z00_2723>BgL_arg1640z00_1868); } } 
if(BgL_test3674z00_7541)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3029z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_171); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3676z00_7553;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2725; long BgL_n2z00_2726;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7554;
if(
INTEGERP(BgL_sendz00_171))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7554 = BgL_sendz00_171
; }  else 
{ 
 obj_t BgL_auxz00_7557;
BgL_auxz00_7557 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_171); 
FAILURE(BgL_auxz00_7557,BFALSE,BFALSE);} 
BgL_n1z00_2725 = 
(long)CINT(BgL_tmpz00_7554); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7562;
if(
INTEGERP(BgL_sstartz00_170))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7562 = BgL_sstartz00_170
; }  else 
{ 
 obj_t BgL_auxz00_7565;
BgL_auxz00_7565 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_170); 
FAILURE(BgL_auxz00_7565,BFALSE,BFALSE);} 
BgL_n2z00_2726 = 
(long)CINT(BgL_tmpz00_7562); } 
BgL_test3676z00_7553 = 
(BgL_n1z00_2725<BgL_n2z00_2726); } 
if(BgL_test3676z00_7553)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3029z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_171); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3679z00_7572;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1212z00_1865;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2727; long BgL_za72za7_2728;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7573;
if(
INTEGERP(BgL_sendz00_171))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7573 = BgL_sendz00_171
; }  else 
{ 
 obj_t BgL_auxz00_7576;
BgL_auxz00_7576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_171); 
FAILURE(BgL_auxz00_7576,BFALSE,BFALSE);} 
BgL_za71za7_2727 = 
(long)CINT(BgL_tmpz00_7573); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7581;
if(
INTEGERP(BgL_sstartz00_170))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7581 = BgL_sstartz00_170
; }  else 
{ 
 obj_t BgL_auxz00_7584;
BgL_auxz00_7584 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_170); 
FAILURE(BgL_auxz00_7584,BFALSE,BFALSE);} 
BgL_za72za7_2728 = 
(long)CINT(BgL_tmpz00_7581); } 
BgL_a1212z00_1865 = 
(BgL_za71za7_2727-BgL_za72za7_2728); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1213z00_1866;
BgL_b1213z00_1866 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_167); 
{ /* Llib/srfi4.scm 838 */

BgL_test3679z00_7572 = 
(BgL_a1212z00_1865>BgL_b1213z00_1866); } } } 
if(BgL_test3679z00_7572)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1639z00_1864;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2732; long BgL_za72za7_2733;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7592;
if(
INTEGERP(BgL_sendz00_171))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7592 = BgL_sendz00_171
; }  else 
{ 
 obj_t BgL_auxz00_7595;
BgL_auxz00_7595 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_171); 
FAILURE(BgL_auxz00_7595,BFALSE,BFALSE);} 
BgL_za71za7_2732 = 
(long)CINT(BgL_tmpz00_7592); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7600;
if(
INTEGERP(BgL_sstartz00_170))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7600 = BgL_sstartz00_170
; }  else 
{ 
 obj_t BgL_auxz00_7603;
BgL_auxz00_7603 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_170); 
FAILURE(BgL_auxz00_7603,BFALSE,BFALSE);} 
BgL_za72za7_2733 = 
(long)CINT(BgL_tmpz00_7600); } 
BgL_arg1639z00_1864 = 
(BgL_za71za7_2732-BgL_za72za7_2733); } 
BGl_errorz00zz__errorz00(BGl_string3029z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1639z00_1864)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_7620; long BgL_tmpz00_7611;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7621;
if(
INTEGERP(BgL_sendz00_171))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7621 = BgL_sendz00_171
; }  else 
{ 
 obj_t BgL_auxz00_7624;
BgL_auxz00_7624 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_171); 
FAILURE(BgL_auxz00_7624,BFALSE,BFALSE);} 
BgL_auxz00_7620 = 
(long)CINT(BgL_tmpz00_7621); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7612;
if(
INTEGERP(BgL_sstartz00_170))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7612 = BgL_sstartz00_170
; }  else 
{ 
 obj_t BgL_auxz00_7615;
BgL_auxz00_7615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3029z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_170); 
FAILURE(BgL_auxz00_7615,BFALSE,BFALSE);} 
BgL_tmpz00_7611 = 
(long)CINT(BgL_tmpz00_7612); } 
BGL_SU16VECTOR_COPY(BgL_targetz00_167, BgL_tstartz00_168, BgL_sourcez00_169, BgL_tmpz00_7611, BgL_auxz00_7620); } 
return BUNSPEC;} 

}



/* _u16vector-copy! */
obj_t BGl__u16vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1311z00_180, obj_t BgL_opt1310z00_179)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1869; obj_t BgL_tstartz00_1870; obj_t BgL_sourcez00_1871;
BgL_targetz00_1869 = 
VECTOR_REF(BgL_opt1310z00_179,0L); 
BgL_tstartz00_1870 = 
VECTOR_REF(BgL_opt1310z00_179,1L); 
BgL_sourcez00_1871 = 
VECTOR_REF(BgL_opt1310z00_179,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1310z00_179)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1875;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2734;
if(
BGL_U16VECTORP(BgL_sourcez00_1871))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2734 = BgL_sourcez00_1871; }  else 
{ 
 obj_t BgL_auxz00_7635;
BgL_auxz00_7635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_sourcez00_1871); 
FAILURE(BgL_auxz00_7635,BFALSE,BFALSE);} 
BgL_sendz00_1875 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2734); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7656; long BgL_auxz00_7647; obj_t BgL_auxz00_7640;
if(
BGL_U16VECTORP(BgL_sourcez00_1871))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7656 = BgL_sourcez00_1871
; }  else 
{ 
 obj_t BgL_auxz00_7659;
BgL_auxz00_7659 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_sourcez00_1871); 
FAILURE(BgL_auxz00_7659,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7648;
if(
INTEGERP(BgL_tstartz00_1870))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7648 = BgL_tstartz00_1870
; }  else 
{ 
 obj_t BgL_auxz00_7651;
BgL_auxz00_7651 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1870); 
FAILURE(BgL_auxz00_7651,BFALSE,BFALSE);} 
BgL_auxz00_7647 = 
(long)CINT(BgL_tmpz00_7648); } 
if(
BGL_U16VECTORP(BgL_targetz00_1869))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7640 = BgL_targetz00_1869
; }  else 
{ 
 obj_t BgL_auxz00_7643;
BgL_auxz00_7643 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_targetz00_1869); 
FAILURE(BgL_auxz00_7643,BFALSE,BFALSE);} 
return 
BGl_u16vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7640, BgL_auxz00_7647, BgL_auxz00_7656, 
BINT(0L), 
BINT(BgL_sendz00_1875));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1876;
BgL_sstartz00_1876 = 
VECTOR_REF(BgL_opt1310z00_179,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1877;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2735;
if(
BGL_U16VECTORP(BgL_sourcez00_1871))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2735 = BgL_sourcez00_1871; }  else 
{ 
 obj_t BgL_auxz00_7669;
BgL_auxz00_7669 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_sourcez00_1871); 
FAILURE(BgL_auxz00_7669,BFALSE,BFALSE);} 
BgL_sendz00_1877 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2735); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7690; long BgL_auxz00_7681; obj_t BgL_auxz00_7674;
if(
BGL_U16VECTORP(BgL_sourcez00_1871))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7690 = BgL_sourcez00_1871
; }  else 
{ 
 obj_t BgL_auxz00_7693;
BgL_auxz00_7693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_sourcez00_1871); 
FAILURE(BgL_auxz00_7693,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7682;
if(
INTEGERP(BgL_tstartz00_1870))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7682 = BgL_tstartz00_1870
; }  else 
{ 
 obj_t BgL_auxz00_7685;
BgL_auxz00_7685 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1870); 
FAILURE(BgL_auxz00_7685,BFALSE,BFALSE);} 
BgL_auxz00_7681 = 
(long)CINT(BgL_tmpz00_7682); } 
if(
BGL_U16VECTORP(BgL_targetz00_1869))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7674 = BgL_targetz00_1869
; }  else 
{ 
 obj_t BgL_auxz00_7677;
BgL_auxz00_7677 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_targetz00_1869); 
FAILURE(BgL_auxz00_7677,BFALSE,BFALSE);} 
return 
BGl_u16vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7674, BgL_auxz00_7681, BgL_auxz00_7690, BgL_sstartz00_1876, 
BINT(BgL_sendz00_1877));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1878;
BgL_sstartz00_1878 = 
VECTOR_REF(BgL_opt1310z00_179,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1879;
BgL_sendz00_1879 = 
VECTOR_REF(BgL_opt1310z00_179,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7717; long BgL_auxz00_7708; obj_t BgL_auxz00_7701;
if(
BGL_U16VECTORP(BgL_sourcez00_1871))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7717 = BgL_sourcez00_1871
; }  else 
{ 
 obj_t BgL_auxz00_7720;
BgL_auxz00_7720 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_sourcez00_1871); 
FAILURE(BgL_auxz00_7720,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7709;
if(
INTEGERP(BgL_tstartz00_1870))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7709 = BgL_tstartz00_1870
; }  else 
{ 
 obj_t BgL_auxz00_7712;
BgL_auxz00_7712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1870); 
FAILURE(BgL_auxz00_7712,BFALSE,BFALSE);} 
BgL_auxz00_7708 = 
(long)CINT(BgL_tmpz00_7709); } 
if(
BGL_U16VECTORP(BgL_targetz00_1869))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7701 = BgL_targetz00_1869
; }  else 
{ 
 obj_t BgL_auxz00_7704;
BgL_auxz00_7704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3033z00zz__srfi4z00, BGl_string2896z00zz__srfi4z00, BgL_targetz00_1869); 
FAILURE(BgL_auxz00_7704,BFALSE,BFALSE);} 
return 
BGl_u16vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7701, BgL_auxz00_7708, BgL_auxz00_7717, BgL_sstartz00_1878, BgL_sendz00_1879);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3031z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1310z00_179)));} } } } 

}



/* u16vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_u16vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_174, long BgL_tstartz00_175, obj_t BgL_sourcez00_176, obj_t BgL_sstartz00_177, obj_t BgL_sendz00_178)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_175<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3032z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_175)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3698z00_7734;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2737;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7735;
if(
INTEGERP(BgL_sstartz00_177))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7735 = BgL_sstartz00_177
; }  else 
{ 
 obj_t BgL_auxz00_7738;
BgL_auxz00_7738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_177); 
FAILURE(BgL_auxz00_7738,BFALSE,BFALSE);} 
BgL_n1z00_2737 = 
(long)CINT(BgL_tmpz00_7735); } 
BgL_test3698z00_7734 = 
(BgL_n1z00_2737<0L); } 
if(BgL_test3698z00_7734)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3032z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_177); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3700z00_7745;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1650z00_1894;
BgL_arg1650z00_1894 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_176); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2739;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7747;
if(
INTEGERP(BgL_sendz00_178))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7747 = BgL_sendz00_178
; }  else 
{ 
 obj_t BgL_auxz00_7750;
BgL_auxz00_7750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_178); 
FAILURE(BgL_auxz00_7750,BFALSE,BFALSE);} 
BgL_n1z00_2739 = 
(long)CINT(BgL_tmpz00_7747); } 
BgL_test3700z00_7745 = 
(BgL_n1z00_2739>BgL_arg1650z00_1894); } } 
if(BgL_test3700z00_7745)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3032z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_178); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3702z00_7757;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2741; long BgL_n2z00_2742;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7758;
if(
INTEGERP(BgL_sendz00_178))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7758 = BgL_sendz00_178
; }  else 
{ 
 obj_t BgL_auxz00_7761;
BgL_auxz00_7761 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_178); 
FAILURE(BgL_auxz00_7761,BFALSE,BFALSE);} 
BgL_n1z00_2741 = 
(long)CINT(BgL_tmpz00_7758); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7766;
if(
INTEGERP(BgL_sstartz00_177))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7766 = BgL_sstartz00_177
; }  else 
{ 
 obj_t BgL_auxz00_7769;
BgL_auxz00_7769 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_177); 
FAILURE(BgL_auxz00_7769,BFALSE,BFALSE);} 
BgL_n2z00_2742 = 
(long)CINT(BgL_tmpz00_7766); } 
BgL_test3702z00_7757 = 
(BgL_n1z00_2741<BgL_n2z00_2742); } 
if(BgL_test3702z00_7757)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3032z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_178); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3705z00_7776;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1215z00_1891;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2743; long BgL_za72za7_2744;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7777;
if(
INTEGERP(BgL_sendz00_178))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7777 = BgL_sendz00_178
; }  else 
{ 
 obj_t BgL_auxz00_7780;
BgL_auxz00_7780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_178); 
FAILURE(BgL_auxz00_7780,BFALSE,BFALSE);} 
BgL_za71za7_2743 = 
(long)CINT(BgL_tmpz00_7777); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7785;
if(
INTEGERP(BgL_sstartz00_177))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7785 = BgL_sstartz00_177
; }  else 
{ 
 obj_t BgL_auxz00_7788;
BgL_auxz00_7788 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_177); 
FAILURE(BgL_auxz00_7788,BFALSE,BFALSE);} 
BgL_za72za7_2744 = 
(long)CINT(BgL_tmpz00_7785); } 
BgL_a1215z00_1891 = 
(BgL_za71za7_2743-BgL_za72za7_2744); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1216z00_1892;
BgL_b1216z00_1892 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_174); 
{ /* Llib/srfi4.scm 838 */

BgL_test3705z00_7776 = 
(BgL_a1215z00_1891>BgL_b1216z00_1892); } } } 
if(BgL_test3705z00_7776)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1649z00_1890;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2748; long BgL_za72za7_2749;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7796;
if(
INTEGERP(BgL_sendz00_178))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7796 = BgL_sendz00_178
; }  else 
{ 
 obj_t BgL_auxz00_7799;
BgL_auxz00_7799 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_178); 
FAILURE(BgL_auxz00_7799,BFALSE,BFALSE);} 
BgL_za71za7_2748 = 
(long)CINT(BgL_tmpz00_7796); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7804;
if(
INTEGERP(BgL_sstartz00_177))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7804 = BgL_sstartz00_177
; }  else 
{ 
 obj_t BgL_auxz00_7807;
BgL_auxz00_7807 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_177); 
FAILURE(BgL_auxz00_7807,BFALSE,BFALSE);} 
BgL_za72za7_2749 = 
(long)CINT(BgL_tmpz00_7804); } 
BgL_arg1649z00_1890 = 
(BgL_za71za7_2748-BgL_za72za7_2749); } 
BGl_errorz00zz__errorz00(BGl_string3032z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1649z00_1890)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_7824; long BgL_tmpz00_7815;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7825;
if(
INTEGERP(BgL_sendz00_178))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7825 = BgL_sendz00_178
; }  else 
{ 
 obj_t BgL_auxz00_7828;
BgL_auxz00_7828 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_178); 
FAILURE(BgL_auxz00_7828,BFALSE,BFALSE);} 
BgL_auxz00_7824 = 
(long)CINT(BgL_tmpz00_7825); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7816;
if(
INTEGERP(BgL_sstartz00_177))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7816 = BgL_sstartz00_177
; }  else 
{ 
 obj_t BgL_auxz00_7819;
BgL_auxz00_7819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3032z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_177); 
FAILURE(BgL_auxz00_7819,BFALSE,BFALSE);} 
BgL_tmpz00_7815 = 
(long)CINT(BgL_tmpz00_7816); } 
BGL_SU16VECTOR_COPY(BgL_targetz00_174, BgL_tstartz00_175, BgL_sourcez00_176, BgL_tmpz00_7815, BgL_auxz00_7824); } 
return BUNSPEC;} 

}



/* _s32vector-copy! */
obj_t BGl__s32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1315z00_187, obj_t BgL_opt1314z00_186)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1895; obj_t BgL_tstartz00_1896; obj_t BgL_sourcez00_1897;
BgL_targetz00_1895 = 
VECTOR_REF(BgL_opt1314z00_186,0L); 
BgL_tstartz00_1896 = 
VECTOR_REF(BgL_opt1314z00_186,1L); 
BgL_sourcez00_1897 = 
VECTOR_REF(BgL_opt1314z00_186,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1314z00_186)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1901;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2750;
if(
BGL_S32VECTORP(BgL_sourcez00_1897))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2750 = BgL_sourcez00_1897; }  else 
{ 
 obj_t BgL_auxz00_7839;
BgL_auxz00_7839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_sourcez00_1897); 
FAILURE(BgL_auxz00_7839,BFALSE,BFALSE);} 
BgL_sendz00_1901 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2750); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7860; long BgL_auxz00_7851; obj_t BgL_auxz00_7844;
if(
BGL_S32VECTORP(BgL_sourcez00_1897))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7860 = BgL_sourcez00_1897
; }  else 
{ 
 obj_t BgL_auxz00_7863;
BgL_auxz00_7863 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_sourcez00_1897); 
FAILURE(BgL_auxz00_7863,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7852;
if(
INTEGERP(BgL_tstartz00_1896))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7852 = BgL_tstartz00_1896
; }  else 
{ 
 obj_t BgL_auxz00_7855;
BgL_auxz00_7855 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1896); 
FAILURE(BgL_auxz00_7855,BFALSE,BFALSE);} 
BgL_auxz00_7851 = 
(long)CINT(BgL_tmpz00_7852); } 
if(
BGL_S32VECTORP(BgL_targetz00_1895))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7844 = BgL_targetz00_1895
; }  else 
{ 
 obj_t BgL_auxz00_7847;
BgL_auxz00_7847 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_targetz00_1895); 
FAILURE(BgL_auxz00_7847,BFALSE,BFALSE);} 
return 
BGl_s32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7844, BgL_auxz00_7851, BgL_auxz00_7860, 
BINT(0L), 
BINT(BgL_sendz00_1901));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1902;
BgL_sstartz00_1902 = 
VECTOR_REF(BgL_opt1314z00_186,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1903;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2751;
if(
BGL_S32VECTORP(BgL_sourcez00_1897))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2751 = BgL_sourcez00_1897; }  else 
{ 
 obj_t BgL_auxz00_7873;
BgL_auxz00_7873 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_sourcez00_1897); 
FAILURE(BgL_auxz00_7873,BFALSE,BFALSE);} 
BgL_sendz00_1903 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2751); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7894; long BgL_auxz00_7885; obj_t BgL_auxz00_7878;
if(
BGL_S32VECTORP(BgL_sourcez00_1897))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7894 = BgL_sourcez00_1897
; }  else 
{ 
 obj_t BgL_auxz00_7897;
BgL_auxz00_7897 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_sourcez00_1897); 
FAILURE(BgL_auxz00_7897,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7886;
if(
INTEGERP(BgL_tstartz00_1896))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7886 = BgL_tstartz00_1896
; }  else 
{ 
 obj_t BgL_auxz00_7889;
BgL_auxz00_7889 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1896); 
FAILURE(BgL_auxz00_7889,BFALSE,BFALSE);} 
BgL_auxz00_7885 = 
(long)CINT(BgL_tmpz00_7886); } 
if(
BGL_S32VECTORP(BgL_targetz00_1895))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7878 = BgL_targetz00_1895
; }  else 
{ 
 obj_t BgL_auxz00_7881;
BgL_auxz00_7881 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_targetz00_1895); 
FAILURE(BgL_auxz00_7881,BFALSE,BFALSE);} 
return 
BGl_s32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7878, BgL_auxz00_7885, BgL_auxz00_7894, BgL_sstartz00_1902, 
BINT(BgL_sendz00_1903));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1904;
BgL_sstartz00_1904 = 
VECTOR_REF(BgL_opt1314z00_186,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1905;
BgL_sendz00_1905 = 
VECTOR_REF(BgL_opt1314z00_186,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_7921; long BgL_auxz00_7912; obj_t BgL_auxz00_7905;
if(
BGL_S32VECTORP(BgL_sourcez00_1897))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7921 = BgL_sourcez00_1897
; }  else 
{ 
 obj_t BgL_auxz00_7924;
BgL_auxz00_7924 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_sourcez00_1897); 
FAILURE(BgL_auxz00_7924,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7913;
if(
INTEGERP(BgL_tstartz00_1896))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7913 = BgL_tstartz00_1896
; }  else 
{ 
 obj_t BgL_auxz00_7916;
BgL_auxz00_7916 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1896); 
FAILURE(BgL_auxz00_7916,BFALSE,BFALSE);} 
BgL_auxz00_7912 = 
(long)CINT(BgL_tmpz00_7913); } 
if(
BGL_S32VECTORP(BgL_targetz00_1895))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_7905 = BgL_targetz00_1895
; }  else 
{ 
 obj_t BgL_auxz00_7908;
BgL_auxz00_7908 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3036z00zz__srfi4z00, BGl_string2898z00zz__srfi4z00, BgL_targetz00_1895); 
FAILURE(BgL_auxz00_7908,BFALSE,BFALSE);} 
return 
BGl_s32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_7905, BgL_auxz00_7912, BgL_auxz00_7921, BgL_sstartz00_1904, BgL_sendz00_1905);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3034z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1314z00_186)));} } } } 

}



/* s32vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_s32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_181, long BgL_tstartz00_182, obj_t BgL_sourcez00_183, obj_t BgL_sstartz00_184, obj_t BgL_sendz00_185)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_182<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3035z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_182)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3724z00_7938;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2753;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7939;
if(
INTEGERP(BgL_sstartz00_184))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7939 = BgL_sstartz00_184
; }  else 
{ 
 obj_t BgL_auxz00_7942;
BgL_auxz00_7942 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_184); 
FAILURE(BgL_auxz00_7942,BFALSE,BFALSE);} 
BgL_n1z00_2753 = 
(long)CINT(BgL_tmpz00_7939); } 
BgL_test3724z00_7938 = 
(BgL_n1z00_2753<0L); } 
if(BgL_test3724z00_7938)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3035z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_184); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3726z00_7949;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1663z00_1920;
BgL_arg1663z00_1920 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_183); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2755;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7951;
if(
INTEGERP(BgL_sendz00_185))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7951 = BgL_sendz00_185
; }  else 
{ 
 obj_t BgL_auxz00_7954;
BgL_auxz00_7954 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_185); 
FAILURE(BgL_auxz00_7954,BFALSE,BFALSE);} 
BgL_n1z00_2755 = 
(long)CINT(BgL_tmpz00_7951); } 
BgL_test3726z00_7949 = 
(BgL_n1z00_2755>BgL_arg1663z00_1920); } } 
if(BgL_test3726z00_7949)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3035z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_185); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3728z00_7961;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2757; long BgL_n2z00_2758;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7962;
if(
INTEGERP(BgL_sendz00_185))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7962 = BgL_sendz00_185
; }  else 
{ 
 obj_t BgL_auxz00_7965;
BgL_auxz00_7965 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_185); 
FAILURE(BgL_auxz00_7965,BFALSE,BFALSE);} 
BgL_n1z00_2757 = 
(long)CINT(BgL_tmpz00_7962); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7970;
if(
INTEGERP(BgL_sstartz00_184))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7970 = BgL_sstartz00_184
; }  else 
{ 
 obj_t BgL_auxz00_7973;
BgL_auxz00_7973 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_184); 
FAILURE(BgL_auxz00_7973,BFALSE,BFALSE);} 
BgL_n2z00_2758 = 
(long)CINT(BgL_tmpz00_7970); } 
BgL_test3728z00_7961 = 
(BgL_n1z00_2757<BgL_n2z00_2758); } 
if(BgL_test3728z00_7961)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3035z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_185); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3731z00_7980;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1218z00_1917;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2759; long BgL_za72za7_2760;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7981;
if(
INTEGERP(BgL_sendz00_185))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7981 = BgL_sendz00_185
; }  else 
{ 
 obj_t BgL_auxz00_7984;
BgL_auxz00_7984 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_185); 
FAILURE(BgL_auxz00_7984,BFALSE,BFALSE);} 
BgL_za71za7_2759 = 
(long)CINT(BgL_tmpz00_7981); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_7989;
if(
INTEGERP(BgL_sstartz00_184))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_7989 = BgL_sstartz00_184
; }  else 
{ 
 obj_t BgL_auxz00_7992;
BgL_auxz00_7992 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_184); 
FAILURE(BgL_auxz00_7992,BFALSE,BFALSE);} 
BgL_za72za7_2760 = 
(long)CINT(BgL_tmpz00_7989); } 
BgL_a1218z00_1917 = 
(BgL_za71za7_2759-BgL_za72za7_2760); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1219z00_1918;
BgL_b1219z00_1918 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_181); 
{ /* Llib/srfi4.scm 838 */

BgL_test3731z00_7980 = 
(BgL_a1218z00_1917>BgL_b1219z00_1918); } } } 
if(BgL_test3731z00_7980)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1661z00_1916;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2764; long BgL_za72za7_2765;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8000;
if(
INTEGERP(BgL_sendz00_185))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8000 = BgL_sendz00_185
; }  else 
{ 
 obj_t BgL_auxz00_8003;
BgL_auxz00_8003 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_185); 
FAILURE(BgL_auxz00_8003,BFALSE,BFALSE);} 
BgL_za71za7_2764 = 
(long)CINT(BgL_tmpz00_8000); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8008;
if(
INTEGERP(BgL_sstartz00_184))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8008 = BgL_sstartz00_184
; }  else 
{ 
 obj_t BgL_auxz00_8011;
BgL_auxz00_8011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_184); 
FAILURE(BgL_auxz00_8011,BFALSE,BFALSE);} 
BgL_za72za7_2765 = 
(long)CINT(BgL_tmpz00_8008); } 
BgL_arg1661z00_1916 = 
(BgL_za71za7_2764-BgL_za72za7_2765); } 
BGl_errorz00zz__errorz00(BGl_string3035z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1661z00_1916)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_8028; long BgL_tmpz00_8019;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8029;
if(
INTEGERP(BgL_sendz00_185))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8029 = BgL_sendz00_185
; }  else 
{ 
 obj_t BgL_auxz00_8032;
BgL_auxz00_8032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_185); 
FAILURE(BgL_auxz00_8032,BFALSE,BFALSE);} 
BgL_auxz00_8028 = 
(long)CINT(BgL_tmpz00_8029); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8020;
if(
INTEGERP(BgL_sstartz00_184))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8020 = BgL_sstartz00_184
; }  else 
{ 
 obj_t BgL_auxz00_8023;
BgL_auxz00_8023 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3035z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_184); 
FAILURE(BgL_auxz00_8023,BFALSE,BFALSE);} 
BgL_tmpz00_8019 = 
(long)CINT(BgL_tmpz00_8020); } 
BGL_SU32VECTOR_COPY(BgL_targetz00_181, BgL_tstartz00_182, BgL_sourcez00_183, BgL_tmpz00_8019, BgL_auxz00_8028); } 
return BUNSPEC;} 

}



/* _u32vector-copy! */
obj_t BGl__u32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1319z00_194, obj_t BgL_opt1318z00_193)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1921; obj_t BgL_tstartz00_1922; obj_t BgL_sourcez00_1923;
BgL_targetz00_1921 = 
VECTOR_REF(BgL_opt1318z00_193,0L); 
BgL_tstartz00_1922 = 
VECTOR_REF(BgL_opt1318z00_193,1L); 
BgL_sourcez00_1923 = 
VECTOR_REF(BgL_opt1318z00_193,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1318z00_193)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1927;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2766;
if(
BGL_U32VECTORP(BgL_sourcez00_1923))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2766 = BgL_sourcez00_1923; }  else 
{ 
 obj_t BgL_auxz00_8043;
BgL_auxz00_8043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_sourcez00_1923); 
FAILURE(BgL_auxz00_8043,BFALSE,BFALSE);} 
BgL_sendz00_1927 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2766); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8064; long BgL_auxz00_8055; obj_t BgL_auxz00_8048;
if(
BGL_U32VECTORP(BgL_sourcez00_1923))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8064 = BgL_sourcez00_1923
; }  else 
{ 
 obj_t BgL_auxz00_8067;
BgL_auxz00_8067 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_sourcez00_1923); 
FAILURE(BgL_auxz00_8067,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8056;
if(
INTEGERP(BgL_tstartz00_1922))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8056 = BgL_tstartz00_1922
; }  else 
{ 
 obj_t BgL_auxz00_8059;
BgL_auxz00_8059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1922); 
FAILURE(BgL_auxz00_8059,BFALSE,BFALSE);} 
BgL_auxz00_8055 = 
(long)CINT(BgL_tmpz00_8056); } 
if(
BGL_U32VECTORP(BgL_targetz00_1921))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8048 = BgL_targetz00_1921
; }  else 
{ 
 obj_t BgL_auxz00_8051;
BgL_auxz00_8051 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_targetz00_1921); 
FAILURE(BgL_auxz00_8051,BFALSE,BFALSE);} 
return 
BGl_u32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8048, BgL_auxz00_8055, BgL_auxz00_8064, 
BINT(0L), 
BINT(BgL_sendz00_1927));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1928;
BgL_sstartz00_1928 = 
VECTOR_REF(BgL_opt1318z00_193,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1929;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2767;
if(
BGL_U32VECTORP(BgL_sourcez00_1923))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2767 = BgL_sourcez00_1923; }  else 
{ 
 obj_t BgL_auxz00_8077;
BgL_auxz00_8077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_sourcez00_1923); 
FAILURE(BgL_auxz00_8077,BFALSE,BFALSE);} 
BgL_sendz00_1929 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2767); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8098; long BgL_auxz00_8089; obj_t BgL_auxz00_8082;
if(
BGL_U32VECTORP(BgL_sourcez00_1923))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8098 = BgL_sourcez00_1923
; }  else 
{ 
 obj_t BgL_auxz00_8101;
BgL_auxz00_8101 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_sourcez00_1923); 
FAILURE(BgL_auxz00_8101,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8090;
if(
INTEGERP(BgL_tstartz00_1922))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8090 = BgL_tstartz00_1922
; }  else 
{ 
 obj_t BgL_auxz00_8093;
BgL_auxz00_8093 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1922); 
FAILURE(BgL_auxz00_8093,BFALSE,BFALSE);} 
BgL_auxz00_8089 = 
(long)CINT(BgL_tmpz00_8090); } 
if(
BGL_U32VECTORP(BgL_targetz00_1921))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8082 = BgL_targetz00_1921
; }  else 
{ 
 obj_t BgL_auxz00_8085;
BgL_auxz00_8085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_targetz00_1921); 
FAILURE(BgL_auxz00_8085,BFALSE,BFALSE);} 
return 
BGl_u32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8082, BgL_auxz00_8089, BgL_auxz00_8098, BgL_sstartz00_1928, 
BINT(BgL_sendz00_1929));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1930;
BgL_sstartz00_1930 = 
VECTOR_REF(BgL_opt1318z00_193,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1931;
BgL_sendz00_1931 = 
VECTOR_REF(BgL_opt1318z00_193,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8125; long BgL_auxz00_8116; obj_t BgL_auxz00_8109;
if(
BGL_U32VECTORP(BgL_sourcez00_1923))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8125 = BgL_sourcez00_1923
; }  else 
{ 
 obj_t BgL_auxz00_8128;
BgL_auxz00_8128 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_sourcez00_1923); 
FAILURE(BgL_auxz00_8128,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8117;
if(
INTEGERP(BgL_tstartz00_1922))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8117 = BgL_tstartz00_1922
; }  else 
{ 
 obj_t BgL_auxz00_8120;
BgL_auxz00_8120 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1922); 
FAILURE(BgL_auxz00_8120,BFALSE,BFALSE);} 
BgL_auxz00_8116 = 
(long)CINT(BgL_tmpz00_8117); } 
if(
BGL_U32VECTORP(BgL_targetz00_1921))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8109 = BgL_targetz00_1921
; }  else 
{ 
 obj_t BgL_auxz00_8112;
BgL_auxz00_8112 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3039z00zz__srfi4z00, BGl_string2900z00zz__srfi4z00, BgL_targetz00_1921); 
FAILURE(BgL_auxz00_8112,BFALSE,BFALSE);} 
return 
BGl_u32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8109, BgL_auxz00_8116, BgL_auxz00_8125, BgL_sstartz00_1930, BgL_sendz00_1931);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3037z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1318z00_193)));} } } } 

}



/* u32vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_u32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_188, long BgL_tstartz00_189, obj_t BgL_sourcez00_190, obj_t BgL_sstartz00_191, obj_t BgL_sendz00_192)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_189<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3038z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_189)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3750z00_8142;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2769;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8143;
if(
INTEGERP(BgL_sstartz00_191))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8143 = BgL_sstartz00_191
; }  else 
{ 
 obj_t BgL_auxz00_8146;
BgL_auxz00_8146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_191); 
FAILURE(BgL_auxz00_8146,BFALSE,BFALSE);} 
BgL_n1z00_2769 = 
(long)CINT(BgL_tmpz00_8143); } 
BgL_test3750z00_8142 = 
(BgL_n1z00_2769<0L); } 
if(BgL_test3750z00_8142)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3038z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_191); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3752z00_8153;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1676z00_1946;
BgL_arg1676z00_1946 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_190); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2771;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8155;
if(
INTEGERP(BgL_sendz00_192))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8155 = BgL_sendz00_192
; }  else 
{ 
 obj_t BgL_auxz00_8158;
BgL_auxz00_8158 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_192); 
FAILURE(BgL_auxz00_8158,BFALSE,BFALSE);} 
BgL_n1z00_2771 = 
(long)CINT(BgL_tmpz00_8155); } 
BgL_test3752z00_8153 = 
(BgL_n1z00_2771>BgL_arg1676z00_1946); } } 
if(BgL_test3752z00_8153)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3038z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_192); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3754z00_8165;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2773; long BgL_n2z00_2774;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8166;
if(
INTEGERP(BgL_sendz00_192))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8166 = BgL_sendz00_192
; }  else 
{ 
 obj_t BgL_auxz00_8169;
BgL_auxz00_8169 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_192); 
FAILURE(BgL_auxz00_8169,BFALSE,BFALSE);} 
BgL_n1z00_2773 = 
(long)CINT(BgL_tmpz00_8166); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8174;
if(
INTEGERP(BgL_sstartz00_191))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8174 = BgL_sstartz00_191
; }  else 
{ 
 obj_t BgL_auxz00_8177;
BgL_auxz00_8177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_191); 
FAILURE(BgL_auxz00_8177,BFALSE,BFALSE);} 
BgL_n2z00_2774 = 
(long)CINT(BgL_tmpz00_8174); } 
BgL_test3754z00_8165 = 
(BgL_n1z00_2773<BgL_n2z00_2774); } 
if(BgL_test3754z00_8165)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3038z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_192); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3757z00_8184;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1221z00_1943;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2775; long BgL_za72za7_2776;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8185;
if(
INTEGERP(BgL_sendz00_192))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8185 = BgL_sendz00_192
; }  else 
{ 
 obj_t BgL_auxz00_8188;
BgL_auxz00_8188 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_192); 
FAILURE(BgL_auxz00_8188,BFALSE,BFALSE);} 
BgL_za71za7_2775 = 
(long)CINT(BgL_tmpz00_8185); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8193;
if(
INTEGERP(BgL_sstartz00_191))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8193 = BgL_sstartz00_191
; }  else 
{ 
 obj_t BgL_auxz00_8196;
BgL_auxz00_8196 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_191); 
FAILURE(BgL_auxz00_8196,BFALSE,BFALSE);} 
BgL_za72za7_2776 = 
(long)CINT(BgL_tmpz00_8193); } 
BgL_a1221z00_1943 = 
(BgL_za71za7_2775-BgL_za72za7_2776); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1222z00_1944;
BgL_b1222z00_1944 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_188); 
{ /* Llib/srfi4.scm 838 */

BgL_test3757z00_8184 = 
(BgL_a1221z00_1943>BgL_b1222z00_1944); } } } 
if(BgL_test3757z00_8184)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1675z00_1942;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2780; long BgL_za72za7_2781;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8204;
if(
INTEGERP(BgL_sendz00_192))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8204 = BgL_sendz00_192
; }  else 
{ 
 obj_t BgL_auxz00_8207;
BgL_auxz00_8207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_192); 
FAILURE(BgL_auxz00_8207,BFALSE,BFALSE);} 
BgL_za71za7_2780 = 
(long)CINT(BgL_tmpz00_8204); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8212;
if(
INTEGERP(BgL_sstartz00_191))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8212 = BgL_sstartz00_191
; }  else 
{ 
 obj_t BgL_auxz00_8215;
BgL_auxz00_8215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_191); 
FAILURE(BgL_auxz00_8215,BFALSE,BFALSE);} 
BgL_za72za7_2781 = 
(long)CINT(BgL_tmpz00_8212); } 
BgL_arg1675z00_1942 = 
(BgL_za71za7_2780-BgL_za72za7_2781); } 
BGl_errorz00zz__errorz00(BGl_string3038z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1675z00_1942)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_8232; long BgL_tmpz00_8223;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8233;
if(
INTEGERP(BgL_sendz00_192))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8233 = BgL_sendz00_192
; }  else 
{ 
 obj_t BgL_auxz00_8236;
BgL_auxz00_8236 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_192); 
FAILURE(BgL_auxz00_8236,BFALSE,BFALSE);} 
BgL_auxz00_8232 = 
(long)CINT(BgL_tmpz00_8233); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8224;
if(
INTEGERP(BgL_sstartz00_191))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8224 = BgL_sstartz00_191
; }  else 
{ 
 obj_t BgL_auxz00_8227;
BgL_auxz00_8227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3038z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_191); 
FAILURE(BgL_auxz00_8227,BFALSE,BFALSE);} 
BgL_tmpz00_8223 = 
(long)CINT(BgL_tmpz00_8224); } 
BGL_SU32VECTOR_COPY(BgL_targetz00_188, BgL_tstartz00_189, BgL_sourcez00_190, BgL_tmpz00_8223, BgL_auxz00_8232); } 
return BUNSPEC;} 

}



/* _s64vector-copy! */
obj_t BGl__s64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1323z00_201, obj_t BgL_opt1322z00_200)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1947; obj_t BgL_tstartz00_1948; obj_t BgL_sourcez00_1949;
BgL_targetz00_1947 = 
VECTOR_REF(BgL_opt1322z00_200,0L); 
BgL_tstartz00_1948 = 
VECTOR_REF(BgL_opt1322z00_200,1L); 
BgL_sourcez00_1949 = 
VECTOR_REF(BgL_opt1322z00_200,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1322z00_200)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1953;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2782;
if(
BGL_S64VECTORP(BgL_sourcez00_1949))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2782 = BgL_sourcez00_1949; }  else 
{ 
 obj_t BgL_auxz00_8247;
BgL_auxz00_8247 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_sourcez00_1949); 
FAILURE(BgL_auxz00_8247,BFALSE,BFALSE);} 
BgL_sendz00_1953 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2782); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8268; long BgL_auxz00_8259; obj_t BgL_auxz00_8252;
if(
BGL_S64VECTORP(BgL_sourcez00_1949))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8268 = BgL_sourcez00_1949
; }  else 
{ 
 obj_t BgL_auxz00_8271;
BgL_auxz00_8271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_sourcez00_1949); 
FAILURE(BgL_auxz00_8271,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8260;
if(
INTEGERP(BgL_tstartz00_1948))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8260 = BgL_tstartz00_1948
; }  else 
{ 
 obj_t BgL_auxz00_8263;
BgL_auxz00_8263 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1948); 
FAILURE(BgL_auxz00_8263,BFALSE,BFALSE);} 
BgL_auxz00_8259 = 
(long)CINT(BgL_tmpz00_8260); } 
if(
BGL_S64VECTORP(BgL_targetz00_1947))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8252 = BgL_targetz00_1947
; }  else 
{ 
 obj_t BgL_auxz00_8255;
BgL_auxz00_8255 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_targetz00_1947); 
FAILURE(BgL_auxz00_8255,BFALSE,BFALSE);} 
return 
BGl_s64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8252, BgL_auxz00_8259, BgL_auxz00_8268, 
BINT(0L), 
BINT(BgL_sendz00_1953));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1954;
BgL_sstartz00_1954 = 
VECTOR_REF(BgL_opt1322z00_200,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1955;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2783;
if(
BGL_S64VECTORP(BgL_sourcez00_1949))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2783 = BgL_sourcez00_1949; }  else 
{ 
 obj_t BgL_auxz00_8281;
BgL_auxz00_8281 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_sourcez00_1949); 
FAILURE(BgL_auxz00_8281,BFALSE,BFALSE);} 
BgL_sendz00_1955 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2783); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8302; long BgL_auxz00_8293; obj_t BgL_auxz00_8286;
if(
BGL_S64VECTORP(BgL_sourcez00_1949))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8302 = BgL_sourcez00_1949
; }  else 
{ 
 obj_t BgL_auxz00_8305;
BgL_auxz00_8305 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_sourcez00_1949); 
FAILURE(BgL_auxz00_8305,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8294;
if(
INTEGERP(BgL_tstartz00_1948))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8294 = BgL_tstartz00_1948
; }  else 
{ 
 obj_t BgL_auxz00_8297;
BgL_auxz00_8297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1948); 
FAILURE(BgL_auxz00_8297,BFALSE,BFALSE);} 
BgL_auxz00_8293 = 
(long)CINT(BgL_tmpz00_8294); } 
if(
BGL_S64VECTORP(BgL_targetz00_1947))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8286 = BgL_targetz00_1947
; }  else 
{ 
 obj_t BgL_auxz00_8289;
BgL_auxz00_8289 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_targetz00_1947); 
FAILURE(BgL_auxz00_8289,BFALSE,BFALSE);} 
return 
BGl_s64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8286, BgL_auxz00_8293, BgL_auxz00_8302, BgL_sstartz00_1954, 
BINT(BgL_sendz00_1955));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1956;
BgL_sstartz00_1956 = 
VECTOR_REF(BgL_opt1322z00_200,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1957;
BgL_sendz00_1957 = 
VECTOR_REF(BgL_opt1322z00_200,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8329; long BgL_auxz00_8320; obj_t BgL_auxz00_8313;
if(
BGL_S64VECTORP(BgL_sourcez00_1949))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8329 = BgL_sourcez00_1949
; }  else 
{ 
 obj_t BgL_auxz00_8332;
BgL_auxz00_8332 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_sourcez00_1949); 
FAILURE(BgL_auxz00_8332,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8321;
if(
INTEGERP(BgL_tstartz00_1948))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8321 = BgL_tstartz00_1948
; }  else 
{ 
 obj_t BgL_auxz00_8324;
BgL_auxz00_8324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1948); 
FAILURE(BgL_auxz00_8324,BFALSE,BFALSE);} 
BgL_auxz00_8320 = 
(long)CINT(BgL_tmpz00_8321); } 
if(
BGL_S64VECTORP(BgL_targetz00_1947))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8313 = BgL_targetz00_1947
; }  else 
{ 
 obj_t BgL_auxz00_8316;
BgL_auxz00_8316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3042z00zz__srfi4z00, BGl_string2902z00zz__srfi4z00, BgL_targetz00_1947); 
FAILURE(BgL_auxz00_8316,BFALSE,BFALSE);} 
return 
BGl_s64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8313, BgL_auxz00_8320, BgL_auxz00_8329, BgL_sstartz00_1956, BgL_sendz00_1957);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3040z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1322z00_200)));} } } } 

}



/* s64vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_s64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_195, long BgL_tstartz00_196, obj_t BgL_sourcez00_197, obj_t BgL_sstartz00_198, obj_t BgL_sendz00_199)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_196<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3041z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_196)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3776z00_8346;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2785;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8347;
if(
INTEGERP(BgL_sstartz00_198))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8347 = BgL_sstartz00_198
; }  else 
{ 
 obj_t BgL_auxz00_8350;
BgL_auxz00_8350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_198); 
FAILURE(BgL_auxz00_8350,BFALSE,BFALSE);} 
BgL_n1z00_2785 = 
(long)CINT(BgL_tmpz00_8347); } 
BgL_test3776z00_8346 = 
(BgL_n1z00_2785<0L); } 
if(BgL_test3776z00_8346)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3041z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_198); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3778z00_8357;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1689z00_1972;
BgL_arg1689z00_1972 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_197); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2787;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8359;
if(
INTEGERP(BgL_sendz00_199))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8359 = BgL_sendz00_199
; }  else 
{ 
 obj_t BgL_auxz00_8362;
BgL_auxz00_8362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_199); 
FAILURE(BgL_auxz00_8362,BFALSE,BFALSE);} 
BgL_n1z00_2787 = 
(long)CINT(BgL_tmpz00_8359); } 
BgL_test3778z00_8357 = 
(BgL_n1z00_2787>BgL_arg1689z00_1972); } } 
if(BgL_test3778z00_8357)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3041z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_199); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3780z00_8369;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2789; long BgL_n2z00_2790;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8370;
if(
INTEGERP(BgL_sendz00_199))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8370 = BgL_sendz00_199
; }  else 
{ 
 obj_t BgL_auxz00_8373;
BgL_auxz00_8373 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_199); 
FAILURE(BgL_auxz00_8373,BFALSE,BFALSE);} 
BgL_n1z00_2789 = 
(long)CINT(BgL_tmpz00_8370); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8378;
if(
INTEGERP(BgL_sstartz00_198))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8378 = BgL_sstartz00_198
; }  else 
{ 
 obj_t BgL_auxz00_8381;
BgL_auxz00_8381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_198); 
FAILURE(BgL_auxz00_8381,BFALSE,BFALSE);} 
BgL_n2z00_2790 = 
(long)CINT(BgL_tmpz00_8378); } 
BgL_test3780z00_8369 = 
(BgL_n1z00_2789<BgL_n2z00_2790); } 
if(BgL_test3780z00_8369)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3041z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_199); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3783z00_8388;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1224z00_1969;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2791; long BgL_za72za7_2792;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8389;
if(
INTEGERP(BgL_sendz00_199))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8389 = BgL_sendz00_199
; }  else 
{ 
 obj_t BgL_auxz00_8392;
BgL_auxz00_8392 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_199); 
FAILURE(BgL_auxz00_8392,BFALSE,BFALSE);} 
BgL_za71za7_2791 = 
(long)CINT(BgL_tmpz00_8389); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8397;
if(
INTEGERP(BgL_sstartz00_198))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8397 = BgL_sstartz00_198
; }  else 
{ 
 obj_t BgL_auxz00_8400;
BgL_auxz00_8400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_198); 
FAILURE(BgL_auxz00_8400,BFALSE,BFALSE);} 
BgL_za72za7_2792 = 
(long)CINT(BgL_tmpz00_8397); } 
BgL_a1224z00_1969 = 
(BgL_za71za7_2791-BgL_za72za7_2792); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1225z00_1970;
BgL_b1225z00_1970 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_195); 
{ /* Llib/srfi4.scm 838 */

BgL_test3783z00_8388 = 
(BgL_a1224z00_1969>BgL_b1225z00_1970); } } } 
if(BgL_test3783z00_8388)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1688z00_1968;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2796; long BgL_za72za7_2797;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8408;
if(
INTEGERP(BgL_sendz00_199))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8408 = BgL_sendz00_199
; }  else 
{ 
 obj_t BgL_auxz00_8411;
BgL_auxz00_8411 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_199); 
FAILURE(BgL_auxz00_8411,BFALSE,BFALSE);} 
BgL_za71za7_2796 = 
(long)CINT(BgL_tmpz00_8408); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8416;
if(
INTEGERP(BgL_sstartz00_198))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8416 = BgL_sstartz00_198
; }  else 
{ 
 obj_t BgL_auxz00_8419;
BgL_auxz00_8419 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_198); 
FAILURE(BgL_auxz00_8419,BFALSE,BFALSE);} 
BgL_za72za7_2797 = 
(long)CINT(BgL_tmpz00_8416); } 
BgL_arg1688z00_1968 = 
(BgL_za71za7_2796-BgL_za72za7_2797); } 
BGl_errorz00zz__errorz00(BGl_string3041z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1688z00_1968)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_8436; long BgL_tmpz00_8427;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8437;
if(
INTEGERP(BgL_sendz00_199))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8437 = BgL_sendz00_199
; }  else 
{ 
 obj_t BgL_auxz00_8440;
BgL_auxz00_8440 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_199); 
FAILURE(BgL_auxz00_8440,BFALSE,BFALSE);} 
BgL_auxz00_8436 = 
(long)CINT(BgL_tmpz00_8437); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8428;
if(
INTEGERP(BgL_sstartz00_198))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8428 = BgL_sstartz00_198
; }  else 
{ 
 obj_t BgL_auxz00_8431;
BgL_auxz00_8431 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3041z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_198); 
FAILURE(BgL_auxz00_8431,BFALSE,BFALSE);} 
BgL_tmpz00_8427 = 
(long)CINT(BgL_tmpz00_8428); } 
BGL_SU64VECTOR_COPY(BgL_targetz00_195, BgL_tstartz00_196, BgL_sourcez00_197, BgL_tmpz00_8427, BgL_auxz00_8436); } 
return BUNSPEC;} 

}



/* _u64vector-copy! */
obj_t BGl__u64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1327z00_208, obj_t BgL_opt1326z00_207)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1973; obj_t BgL_tstartz00_1974; obj_t BgL_sourcez00_1975;
BgL_targetz00_1973 = 
VECTOR_REF(BgL_opt1326z00_207,0L); 
BgL_tstartz00_1974 = 
VECTOR_REF(BgL_opt1326z00_207,1L); 
BgL_sourcez00_1975 = 
VECTOR_REF(BgL_opt1326z00_207,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1326z00_207)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1979;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2798;
if(
BGL_U64VECTORP(BgL_sourcez00_1975))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2798 = BgL_sourcez00_1975; }  else 
{ 
 obj_t BgL_auxz00_8451;
BgL_auxz00_8451 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_sourcez00_1975); 
FAILURE(BgL_auxz00_8451,BFALSE,BFALSE);} 
BgL_sendz00_1979 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2798); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8472; long BgL_auxz00_8463; obj_t BgL_auxz00_8456;
if(
BGL_U64VECTORP(BgL_sourcez00_1975))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8472 = BgL_sourcez00_1975
; }  else 
{ 
 obj_t BgL_auxz00_8475;
BgL_auxz00_8475 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_sourcez00_1975); 
FAILURE(BgL_auxz00_8475,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8464;
if(
INTEGERP(BgL_tstartz00_1974))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8464 = BgL_tstartz00_1974
; }  else 
{ 
 obj_t BgL_auxz00_8467;
BgL_auxz00_8467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1974); 
FAILURE(BgL_auxz00_8467,BFALSE,BFALSE);} 
BgL_auxz00_8463 = 
(long)CINT(BgL_tmpz00_8464); } 
if(
BGL_U64VECTORP(BgL_targetz00_1973))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8456 = BgL_targetz00_1973
; }  else 
{ 
 obj_t BgL_auxz00_8459;
BgL_auxz00_8459 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_targetz00_1973); 
FAILURE(BgL_auxz00_8459,BFALSE,BFALSE);} 
return 
BGl_u64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8456, BgL_auxz00_8463, BgL_auxz00_8472, 
BINT(0L), 
BINT(BgL_sendz00_1979));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1980;
BgL_sstartz00_1980 = 
VECTOR_REF(BgL_opt1326z00_207,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_1981;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2799;
if(
BGL_U64VECTORP(BgL_sourcez00_1975))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2799 = BgL_sourcez00_1975; }  else 
{ 
 obj_t BgL_auxz00_8485;
BgL_auxz00_8485 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_sourcez00_1975); 
FAILURE(BgL_auxz00_8485,BFALSE,BFALSE);} 
BgL_sendz00_1981 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2799); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8506; long BgL_auxz00_8497; obj_t BgL_auxz00_8490;
if(
BGL_U64VECTORP(BgL_sourcez00_1975))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8506 = BgL_sourcez00_1975
; }  else 
{ 
 obj_t BgL_auxz00_8509;
BgL_auxz00_8509 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_sourcez00_1975); 
FAILURE(BgL_auxz00_8509,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8498;
if(
INTEGERP(BgL_tstartz00_1974))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8498 = BgL_tstartz00_1974
; }  else 
{ 
 obj_t BgL_auxz00_8501;
BgL_auxz00_8501 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1974); 
FAILURE(BgL_auxz00_8501,BFALSE,BFALSE);} 
BgL_auxz00_8497 = 
(long)CINT(BgL_tmpz00_8498); } 
if(
BGL_U64VECTORP(BgL_targetz00_1973))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8490 = BgL_targetz00_1973
; }  else 
{ 
 obj_t BgL_auxz00_8493;
BgL_auxz00_8493 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_targetz00_1973); 
FAILURE(BgL_auxz00_8493,BFALSE,BFALSE);} 
return 
BGl_u64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8490, BgL_auxz00_8497, BgL_auxz00_8506, BgL_sstartz00_1980, 
BINT(BgL_sendz00_1981));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_1982;
BgL_sstartz00_1982 = 
VECTOR_REF(BgL_opt1326z00_207,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_1983;
BgL_sendz00_1983 = 
VECTOR_REF(BgL_opt1326z00_207,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8533; long BgL_auxz00_8524; obj_t BgL_auxz00_8517;
if(
BGL_U64VECTORP(BgL_sourcez00_1975))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8533 = BgL_sourcez00_1975
; }  else 
{ 
 obj_t BgL_auxz00_8536;
BgL_auxz00_8536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_sourcez00_1975); 
FAILURE(BgL_auxz00_8536,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8525;
if(
INTEGERP(BgL_tstartz00_1974))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8525 = BgL_tstartz00_1974
; }  else 
{ 
 obj_t BgL_auxz00_8528;
BgL_auxz00_8528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_1974); 
FAILURE(BgL_auxz00_8528,BFALSE,BFALSE);} 
BgL_auxz00_8524 = 
(long)CINT(BgL_tmpz00_8525); } 
if(
BGL_U64VECTORP(BgL_targetz00_1973))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8517 = BgL_targetz00_1973
; }  else 
{ 
 obj_t BgL_auxz00_8520;
BgL_auxz00_8520 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3045z00zz__srfi4z00, BGl_string2904z00zz__srfi4z00, BgL_targetz00_1973); 
FAILURE(BgL_auxz00_8520,BFALSE,BFALSE);} 
return 
BGl_u64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8517, BgL_auxz00_8524, BgL_auxz00_8533, BgL_sstartz00_1982, BgL_sendz00_1983);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3043z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1326z00_207)));} } } } 

}



/* u64vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_u64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_202, long BgL_tstartz00_203, obj_t BgL_sourcez00_204, obj_t BgL_sstartz00_205, obj_t BgL_sendz00_206)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_203<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3044z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_203)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3802z00_8550;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2801;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8551;
if(
INTEGERP(BgL_sstartz00_205))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8551 = BgL_sstartz00_205
; }  else 
{ 
 obj_t BgL_auxz00_8554;
BgL_auxz00_8554 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_205); 
FAILURE(BgL_auxz00_8554,BFALSE,BFALSE);} 
BgL_n1z00_2801 = 
(long)CINT(BgL_tmpz00_8551); } 
BgL_test3802z00_8550 = 
(BgL_n1z00_2801<0L); } 
if(BgL_test3802z00_8550)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3044z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_205); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3804z00_8561;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1705z00_1998;
BgL_arg1705z00_1998 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_204); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2803;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8563;
if(
INTEGERP(BgL_sendz00_206))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8563 = BgL_sendz00_206
; }  else 
{ 
 obj_t BgL_auxz00_8566;
BgL_auxz00_8566 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_206); 
FAILURE(BgL_auxz00_8566,BFALSE,BFALSE);} 
BgL_n1z00_2803 = 
(long)CINT(BgL_tmpz00_8563); } 
BgL_test3804z00_8561 = 
(BgL_n1z00_2803>BgL_arg1705z00_1998); } } 
if(BgL_test3804z00_8561)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3044z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_206); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3806z00_8573;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2805; long BgL_n2z00_2806;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8574;
if(
INTEGERP(BgL_sendz00_206))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8574 = BgL_sendz00_206
; }  else 
{ 
 obj_t BgL_auxz00_8577;
BgL_auxz00_8577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_206); 
FAILURE(BgL_auxz00_8577,BFALSE,BFALSE);} 
BgL_n1z00_2805 = 
(long)CINT(BgL_tmpz00_8574); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8582;
if(
INTEGERP(BgL_sstartz00_205))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8582 = BgL_sstartz00_205
; }  else 
{ 
 obj_t BgL_auxz00_8585;
BgL_auxz00_8585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_205); 
FAILURE(BgL_auxz00_8585,BFALSE,BFALSE);} 
BgL_n2z00_2806 = 
(long)CINT(BgL_tmpz00_8582); } 
BgL_test3806z00_8573 = 
(BgL_n1z00_2805<BgL_n2z00_2806); } 
if(BgL_test3806z00_8573)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3044z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_206); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3809z00_8592;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1227z00_1995;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2807; long BgL_za72za7_2808;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8593;
if(
INTEGERP(BgL_sendz00_206))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8593 = BgL_sendz00_206
; }  else 
{ 
 obj_t BgL_auxz00_8596;
BgL_auxz00_8596 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_206); 
FAILURE(BgL_auxz00_8596,BFALSE,BFALSE);} 
BgL_za71za7_2807 = 
(long)CINT(BgL_tmpz00_8593); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8601;
if(
INTEGERP(BgL_sstartz00_205))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8601 = BgL_sstartz00_205
; }  else 
{ 
 obj_t BgL_auxz00_8604;
BgL_auxz00_8604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_205); 
FAILURE(BgL_auxz00_8604,BFALSE,BFALSE);} 
BgL_za72za7_2808 = 
(long)CINT(BgL_tmpz00_8601); } 
BgL_a1227z00_1995 = 
(BgL_za71za7_2807-BgL_za72za7_2808); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1228z00_1996;
BgL_b1228z00_1996 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_202); 
{ /* Llib/srfi4.scm 838 */

BgL_test3809z00_8592 = 
(BgL_a1227z00_1995>BgL_b1228z00_1996); } } } 
if(BgL_test3809z00_8592)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1704z00_1994;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2812; long BgL_za72za7_2813;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8612;
if(
INTEGERP(BgL_sendz00_206))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8612 = BgL_sendz00_206
; }  else 
{ 
 obj_t BgL_auxz00_8615;
BgL_auxz00_8615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_206); 
FAILURE(BgL_auxz00_8615,BFALSE,BFALSE);} 
BgL_za71za7_2812 = 
(long)CINT(BgL_tmpz00_8612); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8620;
if(
INTEGERP(BgL_sstartz00_205))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8620 = BgL_sstartz00_205
; }  else 
{ 
 obj_t BgL_auxz00_8623;
BgL_auxz00_8623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_205); 
FAILURE(BgL_auxz00_8623,BFALSE,BFALSE);} 
BgL_za72za7_2813 = 
(long)CINT(BgL_tmpz00_8620); } 
BgL_arg1704z00_1994 = 
(BgL_za71za7_2812-BgL_za72za7_2813); } 
BGl_errorz00zz__errorz00(BGl_string3044z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1704z00_1994)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_8640; long BgL_tmpz00_8631;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8641;
if(
INTEGERP(BgL_sendz00_206))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8641 = BgL_sendz00_206
; }  else 
{ 
 obj_t BgL_auxz00_8644;
BgL_auxz00_8644 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_206); 
FAILURE(BgL_auxz00_8644,BFALSE,BFALSE);} 
BgL_auxz00_8640 = 
(long)CINT(BgL_tmpz00_8641); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8632;
if(
INTEGERP(BgL_sstartz00_205))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8632 = BgL_sstartz00_205
; }  else 
{ 
 obj_t BgL_auxz00_8635;
BgL_auxz00_8635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3044z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_205); 
FAILURE(BgL_auxz00_8635,BFALSE,BFALSE);} 
BgL_tmpz00_8631 = 
(long)CINT(BgL_tmpz00_8632); } 
BGL_SU64VECTOR_COPY(BgL_targetz00_202, BgL_tstartz00_203, BgL_sourcez00_204, BgL_tmpz00_8631, BgL_auxz00_8640); } 
return BUNSPEC;} 

}



/* _f32vector-copy! */
obj_t BGl__f32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1331z00_215, obj_t BgL_opt1330z00_214)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_1999; obj_t BgL_tstartz00_2000; obj_t BgL_sourcez00_2001;
BgL_targetz00_1999 = 
VECTOR_REF(BgL_opt1330z00_214,0L); 
BgL_tstartz00_2000 = 
VECTOR_REF(BgL_opt1330z00_214,1L); 
BgL_sourcez00_2001 = 
VECTOR_REF(BgL_opt1330z00_214,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1330z00_214)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_2005;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2814;
if(
BGL_F32VECTORP(BgL_sourcez00_2001))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2814 = BgL_sourcez00_2001; }  else 
{ 
 obj_t BgL_auxz00_8655;
BgL_auxz00_8655 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_sourcez00_2001); 
FAILURE(BgL_auxz00_8655,BFALSE,BFALSE);} 
BgL_sendz00_2005 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2814); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8676; long BgL_auxz00_8667; obj_t BgL_auxz00_8660;
if(
BGL_F32VECTORP(BgL_sourcez00_2001))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8676 = BgL_sourcez00_2001
; }  else 
{ 
 obj_t BgL_auxz00_8679;
BgL_auxz00_8679 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_sourcez00_2001); 
FAILURE(BgL_auxz00_8679,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8668;
if(
INTEGERP(BgL_tstartz00_2000))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8668 = BgL_tstartz00_2000
; }  else 
{ 
 obj_t BgL_auxz00_8671;
BgL_auxz00_8671 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_2000); 
FAILURE(BgL_auxz00_8671,BFALSE,BFALSE);} 
BgL_auxz00_8667 = 
(long)CINT(BgL_tmpz00_8668); } 
if(
BGL_F32VECTORP(BgL_targetz00_1999))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8660 = BgL_targetz00_1999
; }  else 
{ 
 obj_t BgL_auxz00_8663;
BgL_auxz00_8663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_targetz00_1999); 
FAILURE(BgL_auxz00_8663,BFALSE,BFALSE);} 
return 
BGl_f32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8660, BgL_auxz00_8667, BgL_auxz00_8676, 
BINT(0L), 
BINT(BgL_sendz00_2005));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_2006;
BgL_sstartz00_2006 = 
VECTOR_REF(BgL_opt1330z00_214,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_2007;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2815;
if(
BGL_F32VECTORP(BgL_sourcez00_2001))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2815 = BgL_sourcez00_2001; }  else 
{ 
 obj_t BgL_auxz00_8689;
BgL_auxz00_8689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_sourcez00_2001); 
FAILURE(BgL_auxz00_8689,BFALSE,BFALSE);} 
BgL_sendz00_2007 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2815); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8710; long BgL_auxz00_8701; obj_t BgL_auxz00_8694;
if(
BGL_F32VECTORP(BgL_sourcez00_2001))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8710 = BgL_sourcez00_2001
; }  else 
{ 
 obj_t BgL_auxz00_8713;
BgL_auxz00_8713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_sourcez00_2001); 
FAILURE(BgL_auxz00_8713,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8702;
if(
INTEGERP(BgL_tstartz00_2000))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8702 = BgL_tstartz00_2000
; }  else 
{ 
 obj_t BgL_auxz00_8705;
BgL_auxz00_8705 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_2000); 
FAILURE(BgL_auxz00_8705,BFALSE,BFALSE);} 
BgL_auxz00_8701 = 
(long)CINT(BgL_tmpz00_8702); } 
if(
BGL_F32VECTORP(BgL_targetz00_1999))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8694 = BgL_targetz00_1999
; }  else 
{ 
 obj_t BgL_auxz00_8697;
BgL_auxz00_8697 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_targetz00_1999); 
FAILURE(BgL_auxz00_8697,BFALSE,BFALSE);} 
return 
BGl_f32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8694, BgL_auxz00_8701, BgL_auxz00_8710, BgL_sstartz00_2006, 
BINT(BgL_sendz00_2007));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_2008;
BgL_sstartz00_2008 = 
VECTOR_REF(BgL_opt1330z00_214,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_2009;
BgL_sendz00_2009 = 
VECTOR_REF(BgL_opt1330z00_214,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8737; long BgL_auxz00_8728; obj_t BgL_auxz00_8721;
if(
BGL_F32VECTORP(BgL_sourcez00_2001))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8737 = BgL_sourcez00_2001
; }  else 
{ 
 obj_t BgL_auxz00_8740;
BgL_auxz00_8740 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_sourcez00_2001); 
FAILURE(BgL_auxz00_8740,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8729;
if(
INTEGERP(BgL_tstartz00_2000))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8729 = BgL_tstartz00_2000
; }  else 
{ 
 obj_t BgL_auxz00_8732;
BgL_auxz00_8732 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_2000); 
FAILURE(BgL_auxz00_8732,BFALSE,BFALSE);} 
BgL_auxz00_8728 = 
(long)CINT(BgL_tmpz00_8729); } 
if(
BGL_F32VECTORP(BgL_targetz00_1999))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8721 = BgL_targetz00_1999
; }  else 
{ 
 obj_t BgL_auxz00_8724;
BgL_auxz00_8724 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3048z00zz__srfi4z00, BGl_string2906z00zz__srfi4z00, BgL_targetz00_1999); 
FAILURE(BgL_auxz00_8724,BFALSE,BFALSE);} 
return 
BGl_f32vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8721, BgL_auxz00_8728, BgL_auxz00_8737, BgL_sstartz00_2008, BgL_sendz00_2009);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3046z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1330z00_214)));} } } } 

}



/* f32vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_f32vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_209, long BgL_tstartz00_210, obj_t BgL_sourcez00_211, obj_t BgL_sstartz00_212, obj_t BgL_sendz00_213)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_210<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3047z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_210)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3828z00_8754;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2817;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8755;
if(
INTEGERP(BgL_sstartz00_212))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8755 = BgL_sstartz00_212
; }  else 
{ 
 obj_t BgL_auxz00_8758;
BgL_auxz00_8758 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_212); 
FAILURE(BgL_auxz00_8758,BFALSE,BFALSE);} 
BgL_n1z00_2817 = 
(long)CINT(BgL_tmpz00_8755); } 
BgL_test3828z00_8754 = 
(BgL_n1z00_2817<0L); } 
if(BgL_test3828z00_8754)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3047z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_212); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3830z00_8765;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1715z00_2024;
BgL_arg1715z00_2024 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_211); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2819;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8767;
if(
INTEGERP(BgL_sendz00_213))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8767 = BgL_sendz00_213
; }  else 
{ 
 obj_t BgL_auxz00_8770;
BgL_auxz00_8770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_213); 
FAILURE(BgL_auxz00_8770,BFALSE,BFALSE);} 
BgL_n1z00_2819 = 
(long)CINT(BgL_tmpz00_8767); } 
BgL_test3830z00_8765 = 
(BgL_n1z00_2819>BgL_arg1715z00_2024); } } 
if(BgL_test3830z00_8765)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3047z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_213); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3832z00_8777;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2821; long BgL_n2z00_2822;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8778;
if(
INTEGERP(BgL_sendz00_213))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8778 = BgL_sendz00_213
; }  else 
{ 
 obj_t BgL_auxz00_8781;
BgL_auxz00_8781 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_213); 
FAILURE(BgL_auxz00_8781,BFALSE,BFALSE);} 
BgL_n1z00_2821 = 
(long)CINT(BgL_tmpz00_8778); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8786;
if(
INTEGERP(BgL_sstartz00_212))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8786 = BgL_sstartz00_212
; }  else 
{ 
 obj_t BgL_auxz00_8789;
BgL_auxz00_8789 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_212); 
FAILURE(BgL_auxz00_8789,BFALSE,BFALSE);} 
BgL_n2z00_2822 = 
(long)CINT(BgL_tmpz00_8786); } 
BgL_test3832z00_8777 = 
(BgL_n1z00_2821<BgL_n2z00_2822); } 
if(BgL_test3832z00_8777)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3047z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_213); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3835z00_8796;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1230z00_2021;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2823; long BgL_za72za7_2824;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8797;
if(
INTEGERP(BgL_sendz00_213))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8797 = BgL_sendz00_213
; }  else 
{ 
 obj_t BgL_auxz00_8800;
BgL_auxz00_8800 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_213); 
FAILURE(BgL_auxz00_8800,BFALSE,BFALSE);} 
BgL_za71za7_2823 = 
(long)CINT(BgL_tmpz00_8797); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8805;
if(
INTEGERP(BgL_sstartz00_212))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8805 = BgL_sstartz00_212
; }  else 
{ 
 obj_t BgL_auxz00_8808;
BgL_auxz00_8808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_212); 
FAILURE(BgL_auxz00_8808,BFALSE,BFALSE);} 
BgL_za72za7_2824 = 
(long)CINT(BgL_tmpz00_8805); } 
BgL_a1230z00_2021 = 
(BgL_za71za7_2823-BgL_za72za7_2824); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1231z00_2022;
BgL_b1231z00_2022 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_209); 
{ /* Llib/srfi4.scm 838 */

BgL_test3835z00_8796 = 
(BgL_a1230z00_2021>BgL_b1231z00_2022); } } } 
if(BgL_test3835z00_8796)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1714z00_2020;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2828; long BgL_za72za7_2829;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8816;
if(
INTEGERP(BgL_sendz00_213))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8816 = BgL_sendz00_213
; }  else 
{ 
 obj_t BgL_auxz00_8819;
BgL_auxz00_8819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_213); 
FAILURE(BgL_auxz00_8819,BFALSE,BFALSE);} 
BgL_za71za7_2828 = 
(long)CINT(BgL_tmpz00_8816); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8824;
if(
INTEGERP(BgL_sstartz00_212))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8824 = BgL_sstartz00_212
; }  else 
{ 
 obj_t BgL_auxz00_8827;
BgL_auxz00_8827 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_212); 
FAILURE(BgL_auxz00_8827,BFALSE,BFALSE);} 
BgL_za72za7_2829 = 
(long)CINT(BgL_tmpz00_8824); } 
BgL_arg1714z00_2020 = 
(BgL_za71za7_2828-BgL_za72za7_2829); } 
BGl_errorz00zz__errorz00(BGl_string3047z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1714z00_2020)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_8844; long BgL_tmpz00_8835;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8845;
if(
INTEGERP(BgL_sendz00_213))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8845 = BgL_sendz00_213
; }  else 
{ 
 obj_t BgL_auxz00_8848;
BgL_auxz00_8848 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_213); 
FAILURE(BgL_auxz00_8848,BFALSE,BFALSE);} 
BgL_auxz00_8844 = 
(long)CINT(BgL_tmpz00_8845); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8836;
if(
INTEGERP(BgL_sstartz00_212))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8836 = BgL_sstartz00_212
; }  else 
{ 
 obj_t BgL_auxz00_8839;
BgL_auxz00_8839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3047z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_212); 
FAILURE(BgL_auxz00_8839,BFALSE,BFALSE);} 
BgL_tmpz00_8835 = 
(long)CINT(BgL_tmpz00_8836); } 
BGL_F32VECTOR_COPY(BgL_targetz00_209, BgL_tstartz00_210, BgL_sourcez00_211, BgL_tmpz00_8835, BgL_auxz00_8844); } 
return BUNSPEC;} 

}



/* _f64vector-copy! */
obj_t BGl__f64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_env1335z00_222, obj_t BgL_opt1334z00_221)
{
{ /* Llib/srfi4.scm 838 */
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_targetz00_2025; obj_t BgL_tstartz00_2026; obj_t BgL_sourcez00_2027;
BgL_targetz00_2025 = 
VECTOR_REF(BgL_opt1334z00_221,0L); 
BgL_tstartz00_2026 = 
VECTOR_REF(BgL_opt1334z00_221,1L); 
BgL_sourcez00_2027 = 
VECTOR_REF(BgL_opt1334z00_221,2L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1334z00_221)) { case 3L : 

{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_2031;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2830;
if(
BGL_F64VECTORP(BgL_sourcez00_2027))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2830 = BgL_sourcez00_2027; }  else 
{ 
 obj_t BgL_auxz00_8859;
BgL_auxz00_8859 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_sourcez00_2027); 
FAILURE(BgL_auxz00_8859,BFALSE,BFALSE);} 
BgL_sendz00_2031 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2830); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8880; long BgL_auxz00_8871; obj_t BgL_auxz00_8864;
if(
BGL_F64VECTORP(BgL_sourcez00_2027))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8880 = BgL_sourcez00_2027
; }  else 
{ 
 obj_t BgL_auxz00_8883;
BgL_auxz00_8883 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_sourcez00_2027); 
FAILURE(BgL_auxz00_8883,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8872;
if(
INTEGERP(BgL_tstartz00_2026))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8872 = BgL_tstartz00_2026
; }  else 
{ 
 obj_t BgL_auxz00_8875;
BgL_auxz00_8875 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_2026); 
FAILURE(BgL_auxz00_8875,BFALSE,BFALSE);} 
BgL_auxz00_8871 = 
(long)CINT(BgL_tmpz00_8872); } 
if(
BGL_F64VECTORP(BgL_targetz00_2025))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8864 = BgL_targetz00_2025
; }  else 
{ 
 obj_t BgL_auxz00_8867;
BgL_auxz00_8867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_targetz00_2025); 
FAILURE(BgL_auxz00_8867,BFALSE,BFALSE);} 
return 
BGl_f64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8864, BgL_auxz00_8871, BgL_auxz00_8880, 
BINT(0L), 
BINT(BgL_sendz00_2031));} } } break;case 4L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_2032;
BgL_sstartz00_2032 = 
VECTOR_REF(BgL_opt1334z00_221,3L); 
{ /* Llib/srfi4.scm 838 */
 long BgL_sendz00_2033;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_xz00_2831;
if(
BGL_F64VECTORP(BgL_sourcez00_2027))
{ /* Llib/srfi4.scm 838 */
BgL_xz00_2831 = BgL_sourcez00_2027; }  else 
{ 
 obj_t BgL_auxz00_8893;
BgL_auxz00_8893 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_sourcez00_2027); 
FAILURE(BgL_auxz00_8893,BFALSE,BFALSE);} 
BgL_sendz00_2033 = 
BGL_HVECTOR_LENGTH(BgL_xz00_2831); } 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8914; long BgL_auxz00_8905; obj_t BgL_auxz00_8898;
if(
BGL_F64VECTORP(BgL_sourcez00_2027))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8914 = BgL_sourcez00_2027
; }  else 
{ 
 obj_t BgL_auxz00_8917;
BgL_auxz00_8917 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_sourcez00_2027); 
FAILURE(BgL_auxz00_8917,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8906;
if(
INTEGERP(BgL_tstartz00_2026))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8906 = BgL_tstartz00_2026
; }  else 
{ 
 obj_t BgL_auxz00_8909;
BgL_auxz00_8909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_2026); 
FAILURE(BgL_auxz00_8909,BFALSE,BFALSE);} 
BgL_auxz00_8905 = 
(long)CINT(BgL_tmpz00_8906); } 
if(
BGL_F64VECTORP(BgL_targetz00_2025))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8898 = BgL_targetz00_2025
; }  else 
{ 
 obj_t BgL_auxz00_8901;
BgL_auxz00_8901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_targetz00_2025); 
FAILURE(BgL_auxz00_8901,BFALSE,BFALSE);} 
return 
BGl_f64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8898, BgL_auxz00_8905, BgL_auxz00_8914, BgL_sstartz00_2032, 
BINT(BgL_sendz00_2033));} } } } break;case 5L : 

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sstartz00_2034;
BgL_sstartz00_2034 = 
VECTOR_REF(BgL_opt1334z00_221,3L); 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_sendz00_2035;
BgL_sendz00_2035 = 
VECTOR_REF(BgL_opt1334z00_221,4L); 
{ /* Llib/srfi4.scm 838 */

{ /* Llib/srfi4.scm 838 */
 obj_t BgL_auxz00_8941; long BgL_auxz00_8932; obj_t BgL_auxz00_8925;
if(
BGL_F64VECTORP(BgL_sourcez00_2027))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8941 = BgL_sourcez00_2027
; }  else 
{ 
 obj_t BgL_auxz00_8944;
BgL_auxz00_8944 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_sourcez00_2027); 
FAILURE(BgL_auxz00_8944,BFALSE,BFALSE);} 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8933;
if(
INTEGERP(BgL_tstartz00_2026))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8933 = BgL_tstartz00_2026
; }  else 
{ 
 obj_t BgL_auxz00_8936;
BgL_auxz00_8936 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_tstartz00_2026); 
FAILURE(BgL_auxz00_8936,BFALSE,BFALSE);} 
BgL_auxz00_8932 = 
(long)CINT(BgL_tmpz00_8933); } 
if(
BGL_F64VECTORP(BgL_targetz00_2025))
{ /* Llib/srfi4.scm 838 */
BgL_auxz00_8925 = BgL_targetz00_2025
; }  else 
{ 
 obj_t BgL_auxz00_8928;
BgL_auxz00_8928 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3051z00zz__srfi4z00, BGl_string2908z00zz__srfi4z00, BgL_targetz00_2025); 
FAILURE(BgL_auxz00_8928,BFALSE,BFALSE);} 
return 
BGl_f64vectorzd2copyz12zc0zz__srfi4z00(BgL_auxz00_8925, BgL_auxz00_8932, BgL_auxz00_8941, BgL_sstartz00_2034, BgL_sendz00_2035);} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol3049z00zz__srfi4z00, BGl_string3019z00zz__srfi4z00, 
BINT(
VECTOR_LENGTH(BgL_opt1334z00_221)));} } } } 

}



/* f64vector-copy! */
BGL_EXPORTED_DEF obj_t BGl_f64vectorzd2copyz12zc0zz__srfi4z00(obj_t BgL_targetz00_216, long BgL_tstartz00_217, obj_t BgL_sourcez00_218, obj_t BgL_sstartz00_219, obj_t BgL_sendz00_220)
{
{ /* Llib/srfi4.scm 838 */
if(
(BgL_tstartz00_217<0L))
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3050z00zz__srfi4z00, BGl_string3021z00zz__srfi4z00, 
BINT(BgL_tstartz00_217)); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3854z00_8958;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2833;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8959;
if(
INTEGERP(BgL_sstartz00_219))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8959 = BgL_sstartz00_219
; }  else 
{ 
 obj_t BgL_auxz00_8962;
BgL_auxz00_8962 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_219); 
FAILURE(BgL_auxz00_8962,BFALSE,BFALSE);} 
BgL_n1z00_2833 = 
(long)CINT(BgL_tmpz00_8959); } 
BgL_test3854z00_8958 = 
(BgL_n1z00_2833<0L); } 
if(BgL_test3854z00_8958)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3050z00zz__srfi4z00, BGl_string3022z00zz__srfi4z00, BgL_sstartz00_219); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3856z00_8969;
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1726z00_2050;
BgL_arg1726z00_2050 = 
BGL_HVECTOR_LENGTH(BgL_sourcez00_218); 
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2835;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8971;
if(
INTEGERP(BgL_sendz00_220))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8971 = BgL_sendz00_220
; }  else 
{ 
 obj_t BgL_auxz00_8974;
BgL_auxz00_8974 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_220); 
FAILURE(BgL_auxz00_8974,BFALSE,BFALSE);} 
BgL_n1z00_2835 = 
(long)CINT(BgL_tmpz00_8971); } 
BgL_test3856z00_8969 = 
(BgL_n1z00_2835>BgL_arg1726z00_2050); } } 
if(BgL_test3856z00_8969)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3050z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_220); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3858z00_8981;
{ /* Llib/srfi4.scm 838 */
 long BgL_n1z00_2837; long BgL_n2z00_2838;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8982;
if(
INTEGERP(BgL_sendz00_220))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8982 = BgL_sendz00_220
; }  else 
{ 
 obj_t BgL_auxz00_8985;
BgL_auxz00_8985 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_220); 
FAILURE(BgL_auxz00_8985,BFALSE,BFALSE);} 
BgL_n1z00_2837 = 
(long)CINT(BgL_tmpz00_8982); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_8990;
if(
INTEGERP(BgL_sstartz00_219))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_8990 = BgL_sstartz00_219
; }  else 
{ 
 obj_t BgL_auxz00_8993;
BgL_auxz00_8993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_219); 
FAILURE(BgL_auxz00_8993,BFALSE,BFALSE);} 
BgL_n2z00_2838 = 
(long)CINT(BgL_tmpz00_8990); } 
BgL_test3858z00_8981 = 
(BgL_n1z00_2837<BgL_n2z00_2838); } 
if(BgL_test3858z00_8981)
{ /* Llib/srfi4.scm 838 */
BGl_errorz00zz__errorz00(BGl_string3050z00zz__srfi4z00, BGl_string3023z00zz__srfi4z00, BgL_sendz00_220); }  else 
{ /* Llib/srfi4.scm 838 */
 bool_t BgL_test3861z00_9000;
{ /* Llib/srfi4.scm 838 */
 long BgL_a1233z00_2047;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2839; long BgL_za72za7_2840;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_9001;
if(
INTEGERP(BgL_sendz00_220))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_9001 = BgL_sendz00_220
; }  else 
{ 
 obj_t BgL_auxz00_9004;
BgL_auxz00_9004 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_220); 
FAILURE(BgL_auxz00_9004,BFALSE,BFALSE);} 
BgL_za71za7_2839 = 
(long)CINT(BgL_tmpz00_9001); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_9009;
if(
INTEGERP(BgL_sstartz00_219))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_9009 = BgL_sstartz00_219
; }  else 
{ 
 obj_t BgL_auxz00_9012;
BgL_auxz00_9012 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_219); 
FAILURE(BgL_auxz00_9012,BFALSE,BFALSE);} 
BgL_za72za7_2840 = 
(long)CINT(BgL_tmpz00_9009); } 
BgL_a1233z00_2047 = 
(BgL_za71za7_2839-BgL_za72za7_2840); } 
{ /* Llib/srfi4.scm 838 */
 long BgL_b1234z00_2048;
BgL_b1234z00_2048 = 
BGL_HVECTOR_LENGTH(BgL_targetz00_216); 
{ /* Llib/srfi4.scm 838 */

BgL_test3861z00_9000 = 
(BgL_a1233z00_2047>BgL_b1234z00_2048); } } } 
if(BgL_test3861z00_9000)
{ /* Llib/srfi4.scm 838 */
 long BgL_arg1725z00_2046;
{ /* Llib/srfi4.scm 838 */
 long BgL_za71za7_2844; long BgL_za72za7_2845;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_9020;
if(
INTEGERP(BgL_sendz00_220))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_9020 = BgL_sendz00_220
; }  else 
{ 
 obj_t BgL_auxz00_9023;
BgL_auxz00_9023 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_220); 
FAILURE(BgL_auxz00_9023,BFALSE,BFALSE);} 
BgL_za71za7_2844 = 
(long)CINT(BgL_tmpz00_9020); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_9028;
if(
INTEGERP(BgL_sstartz00_219))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_9028 = BgL_sstartz00_219
; }  else 
{ 
 obj_t BgL_auxz00_9031;
BgL_auxz00_9031 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_219); 
FAILURE(BgL_auxz00_9031,BFALSE,BFALSE);} 
BgL_za72za7_2845 = 
(long)CINT(BgL_tmpz00_9028); } 
BgL_arg1725z00_2046 = 
(BgL_za71za7_2844-BgL_za72za7_2845); } 
BGl_errorz00zz__errorz00(BGl_string3050z00zz__srfi4z00, BGl_string3024z00zz__srfi4z00, 
BINT(BgL_arg1725z00_2046)); }  else 
{ /* Llib/srfi4.scm 838 */BFALSE; } } } } } 
{ /* Llib/srfi4.scm 838 */
 long BgL_auxz00_9048; long BgL_tmpz00_9039;
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_9049;
if(
INTEGERP(BgL_sendz00_220))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_9049 = BgL_sendz00_220
; }  else 
{ 
 obj_t BgL_auxz00_9052;
BgL_auxz00_9052 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sendz00_220); 
FAILURE(BgL_auxz00_9052,BFALSE,BFALSE);} 
BgL_auxz00_9048 = 
(long)CINT(BgL_tmpz00_9049); } 
{ /* Llib/srfi4.scm 838 */
 obj_t BgL_tmpz00_9040;
if(
INTEGERP(BgL_sstartz00_219))
{ /* Llib/srfi4.scm 838 */
BgL_tmpz00_9040 = BgL_sstartz00_219
; }  else 
{ 
 obj_t BgL_auxz00_9043;
BgL_auxz00_9043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2888z00zz__srfi4z00, 
BINT(33208L), BGl_string3050z00zz__srfi4z00, BGl_string2914z00zz__srfi4z00, BgL_sstartz00_219); 
FAILURE(BgL_auxz00_9043,BFALSE,BFALSE);} 
BgL_tmpz00_9039 = 
(long)CINT(BgL_tmpz00_9040); } 
BGL_F64VECTOR_COPY(BgL_targetz00_216, BgL_tstartz00_217, BgL_sourcez00_218, BgL_tmpz00_9039, BgL_auxz00_9048); } 
return BUNSPEC;} 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__srfi4z00(void)
{
{ /* Llib/srfi4.scm 15 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__srfi4z00(void)
{
{ /* Llib/srfi4.scm 15 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__srfi4z00(void)
{
{ /* Llib/srfi4.scm 15 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__srfi4z00(void)
{
{ /* Llib/srfi4.scm 15 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string3052z00zz__srfi4z00)); 
BGl_modulezd2initializa7ationz75zz__hashz00(482391669L, 
BSTRING_TO_STRING(BGl_string3052z00zz__srfi4z00)); 
BGl_modulezd2initializa7ationz75zz__readerz00(220648206L, 
BSTRING_TO_STRING(BGl_string3052z00zz__srfi4z00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string3052z00zz__srfi4z00)); 
return 
BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L, 
BSTRING_TO_STRING(BGl_string3052z00zz__srfi4z00));} 

}

#ifdef __cplusplus
}
#endif
