/*===========================================================================*/
/*   (Llib/custom.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/custom.scm -indent -o objs/obj_s/Llib/custom.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___CUSTOM_TYPE_DEFINITIONS
#define BGL___CUSTOM_TYPE_DEFINITIONS
#endif // BGL___CUSTOM_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL bool_t BGl_customzf3zf3zz__customz00(obj_t);
static obj_t BGl_z62customzf3z91zz__customz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__customz00 = BUNSPEC;
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__customz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_customzd2identifierzd2setz12z12zz__customz00(obj_t, char *);
BGL_EXPORTED_DECL obj_t BGl_customzd2nilzd2zz__customz00(void);
static obj_t BGl_z62customzd2hashzb0zz__customz00(obj_t, obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__customz00(void);
static obj_t BGl_importedzd2moduleszd2initz00zz__customz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__customz00(void);
static obj_t BGl_objectzd2initzd2zz__customz00(void);
extern obj_t bgl_custom_nil(void);
static obj_t BGl_z62customzd2identifierzb0zz__customz00(obj_t, obj_t);
static obj_t BGl_z62customzd2nilzb0zz__customz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_customzd2equalzf3z21zz__customz00(obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__customz00(void);
BGL_EXPORTED_DECL char * BGl_customzd2identifierzd2zz__customz00(obj_t);
static obj_t BGl_z62customzd2identifierzd2setz12z70zz__customz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_customzd2hashzd2zz__customz00(obj_t, int);
static obj_t BGl_z62customzd2equalzf3z43zz__customz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_customzd2nilzd2envz00zz__customz00, BgL_bgl_za762customza7d2nilza71604za7, BGl_z62customzd2nilzb0zz__customz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_customzd2equalzf3zd2envzf3zz__customz00, BgL_bgl_za762customza7d2equa1605z00, BGl_z62customzd2equalzf3z43zz__customz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1600z00zz__customz00, BgL_bgl_string1600za700za7za7_1606za7, "bstring", 7 );
DEFINE_STRING( BGl_string1601z00zz__customz00, BgL_bgl_string1601za700za7za7_1607za7, "&custom-hash", 12 );
DEFINE_STRING( BGl_string1602z00zz__customz00, BgL_bgl_string1602za700za7za7_1608za7, "bint", 4 );
DEFINE_STRING( BGl_string1603z00zz__customz00, BgL_bgl_string1603za700za7za7_1609za7, "__custom", 8 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_customzd2identifierzd2setz12zd2envzc0zz__customz00, BgL_bgl_za762customza7d2iden1610z00, BGl_z62customzd2identifierzd2setz12z70zz__customz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_customzd2identifierzd2envz00zz__customz00, BgL_bgl_za762customza7d2iden1611z00, BGl_z62customzd2identifierzb0zz__customz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_customzf3zd2envz21zz__customz00, BgL_bgl_za762customza7f3za791za71612z00, BGl_z62customzf3z91zz__customz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_customzd2hashzd2envz00zz__customz00, BgL_bgl_za762customza7d2hash1613z00, BGl_z62customzd2hashzb0zz__customz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1595z00zz__customz00, BgL_bgl_string1595za700za7za7_1614za7, "/tmp/bigloo/runtime/Llib/custom.scm", 35 );
DEFINE_STRING( BGl_string1596z00zz__customz00, BgL_bgl_string1596za700za7za7_1615za7, "&custom-equal?", 14 );
DEFINE_STRING( BGl_string1597z00zz__customz00, BgL_bgl_string1597za700za7za7_1616za7, "custom", 6 );
DEFINE_STRING( BGl_string1598z00zz__customz00, BgL_bgl_string1598za700za7za7_1617za7, "&custom-identifier", 18 );
DEFINE_STRING( BGl_string1599z00zz__customz00, BgL_bgl_string1599za700za7za7_1618za7, "&custom-identifier-set!", 23 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__customz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__customz00(long BgL_checksumz00_1758, char * BgL_fromz00_1759)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__customz00))
{ 
BGl_requirezd2initializa7ationz75zz__customz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__customz00(); 
BGl_importedzd2moduleszd2initz00zz__customz00(); 
return 
BGl_methodzd2initzd2zz__customz00();}  else 
{ 
return BUNSPEC;} } 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__customz00(void)
{
{ /* Llib/custom.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* custom? */
BGL_EXPORTED_DEF bool_t BGl_customzf3zf3zz__customz00(obj_t BgL_objz00_3)
{
{ /* Llib/custom.scm 84 */
return 
CUSTOMP(BgL_objz00_3);} 

}



/* &custom? */
obj_t BGl_z62customzf3z91zz__customz00(obj_t BgL_envz00_1731, obj_t BgL_objz00_1732)
{
{ /* Llib/custom.scm 84 */
return 
BBOOL(
BGl_customzf3zf3zz__customz00(BgL_objz00_1732));} 

}



/* custom-nil */
BGL_EXPORTED_DEF obj_t BGl_customzd2nilzd2zz__customz00(void)
{
{ /* Llib/custom.scm 90 */
BGL_TAIL return 
bgl_custom_nil();} 

}



/* &custom-nil */
obj_t BGl_z62customzd2nilzb0zz__customz00(obj_t BgL_envz00_1733)
{
{ /* Llib/custom.scm 90 */
return 
BGl_customzd2nilzd2zz__customz00();} 

}



/* custom-equal? */
BGL_EXPORTED_DEF bool_t BGl_customzd2equalzf3z21zz__customz00(obj_t BgL_obj1z00_4, obj_t BgL_obj2z00_5)
{
{ /* Llib/custom.scm 96 */
return 
CUSTOM_CMP(BgL_obj1z00_4, BgL_obj2z00_5);} 

}



/* &custom-equal? */
obj_t BGl_z62customzd2equalzf3z43zz__customz00(obj_t BgL_envz00_1734, obj_t BgL_obj1z00_1735, obj_t BgL_obj2z00_1736)
{
{ /* Llib/custom.scm 96 */
{ /* Llib/custom.scm 97 */
 bool_t BgL_tmpz00_1773;
{ /* Llib/custom.scm 97 */
 obj_t BgL_auxz00_1781; obj_t BgL_auxz00_1774;
if(
CUSTOMP(BgL_obj2z00_1736))
{ /* Llib/custom.scm 97 */
BgL_auxz00_1781 = BgL_obj2z00_1736
; }  else 
{ 
 obj_t BgL_auxz00_1784;
BgL_auxz00_1784 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(3593L), BGl_string1596z00zz__customz00, BGl_string1597z00zz__customz00, BgL_obj2z00_1736); 
FAILURE(BgL_auxz00_1784,BFALSE,BFALSE);} 
if(
CUSTOMP(BgL_obj1z00_1735))
{ /* Llib/custom.scm 97 */
BgL_auxz00_1774 = BgL_obj1z00_1735
; }  else 
{ 
 obj_t BgL_auxz00_1777;
BgL_auxz00_1777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(3593L), BGl_string1596z00zz__customz00, BGl_string1597z00zz__customz00, BgL_obj1z00_1735); 
FAILURE(BgL_auxz00_1777,BFALSE,BFALSE);} 
BgL_tmpz00_1773 = 
BGl_customzd2equalzf3z21zz__customz00(BgL_auxz00_1774, BgL_auxz00_1781); } 
return 
BBOOL(BgL_tmpz00_1773);} } 

}



/* custom-identifier */
BGL_EXPORTED_DEF char * BGl_customzd2identifierzd2zz__customz00(obj_t BgL_customz00_6)
{
{ /* Llib/custom.scm 102 */
return 
CUSTOM_IDENTIFIER(BgL_customz00_6);} 

}



/* &custom-identifier */
obj_t BGl_z62customzd2identifierzb0zz__customz00(obj_t BgL_envz00_1737, obj_t BgL_customz00_1738)
{
{ /* Llib/custom.scm 102 */
{ /* Llib/custom.scm 103 */
 char * BgL_tmpz00_1791;
{ /* Llib/custom.scm 103 */
 obj_t BgL_auxz00_1792;
if(
CUSTOMP(BgL_customz00_1738))
{ /* Llib/custom.scm 103 */
BgL_auxz00_1792 = BgL_customz00_1738
; }  else 
{ 
 obj_t BgL_auxz00_1795;
BgL_auxz00_1795 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(3906L), BGl_string1598z00zz__customz00, BGl_string1597z00zz__customz00, BgL_customz00_1738); 
FAILURE(BgL_auxz00_1795,BFALSE,BFALSE);} 
BgL_tmpz00_1791 = 
BGl_customzd2identifierzd2zz__customz00(BgL_auxz00_1792); } 
return 
string_to_bstring(BgL_tmpz00_1791);} } 

}



/* custom-identifier-set! */
BGL_EXPORTED_DEF obj_t BGl_customzd2identifierzd2setz12z12zz__customz00(obj_t BgL_customz00_7, char * BgL_strz00_8)
{
{ /* Llib/custom.scm 108 */
return 
CUSTOM_IDENTIFIER_SET(BgL_customz00_7, BgL_strz00_8);} 

}



/* &custom-identifier-set! */
obj_t BGl_z62customzd2identifierzd2setz12z70zz__customz00(obj_t BgL_envz00_1739, obj_t BgL_customz00_1740, obj_t BgL_strz00_1741)
{
{ /* Llib/custom.scm 108 */
{ /* Llib/custom.scm 109 */
 char * BgL_auxz00_1809; obj_t BgL_auxz00_1802;
{ /* Llib/custom.scm 109 */
 obj_t BgL_tmpz00_1810;
if(
STRINGP(BgL_strz00_1741))
{ /* Llib/custom.scm 109 */
BgL_tmpz00_1810 = BgL_strz00_1741
; }  else 
{ 
 obj_t BgL_auxz00_1813;
BgL_auxz00_1813 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(4229L), BGl_string1599z00zz__customz00, BGl_string1600z00zz__customz00, BgL_strz00_1741); 
FAILURE(BgL_auxz00_1813,BFALSE,BFALSE);} 
BgL_auxz00_1809 = 
BSTRING_TO_STRING(BgL_tmpz00_1810); } 
if(
CUSTOMP(BgL_customz00_1740))
{ /* Llib/custom.scm 109 */
BgL_auxz00_1802 = BgL_customz00_1740
; }  else 
{ 
 obj_t BgL_auxz00_1805;
BgL_auxz00_1805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(4229L), BGl_string1599z00zz__customz00, BGl_string1597z00zz__customz00, BgL_customz00_1740); 
FAILURE(BgL_auxz00_1805,BFALSE,BFALSE);} 
return 
BGl_customzd2identifierzd2setz12z12zz__customz00(BgL_auxz00_1802, BgL_auxz00_1809);} } 

}



/* custom-hash */
BGL_EXPORTED_DEF obj_t BGl_customzd2hashzd2zz__customz00(obj_t BgL_customz00_9, int BgL_modz00_10)
{
{ /* Llib/custom.scm 114 */
{ /* Llib/custom.scm 115 */
 int BgL_numz00_1517;
BgL_numz00_1517 = 
CUSTOM_HASH_NUMBER(BgL_customz00_9); 
{ /* Llib/custom.scm 116 */
 long BgL_n1z00_1518; long BgL_n2z00_1519;
BgL_n1z00_1518 = 
(long)(BgL_numz00_1517); 
BgL_n2z00_1519 = 
(long)(BgL_modz00_10); 
{ /* Llib/custom.scm 116 */
 bool_t BgL_test1625z00_1822;
{ /* Llib/custom.scm 116 */
 long BgL_arg1392z00_1521;
BgL_arg1392z00_1521 = 
(((BgL_n1z00_1518) | (BgL_n2z00_1519)) & -2147483648); 
BgL_test1625z00_1822 = 
(BgL_arg1392z00_1521==0L); } 
if(BgL_test1625z00_1822)
{ /* Llib/custom.scm 116 */
 int32_t BgL_arg1389z00_1522;
{ /* Llib/custom.scm 116 */
 int32_t BgL_arg1390z00_1523; int32_t BgL_arg1391z00_1524;
BgL_arg1390z00_1523 = 
(int32_t)(BgL_n1z00_1518); 
BgL_arg1391z00_1524 = 
(int32_t)(BgL_n2z00_1519); 
BgL_arg1389z00_1522 = 
(BgL_arg1390z00_1523%BgL_arg1391z00_1524); } 
{ /* Llib/custom.scm 116 */
 long BgL_arg1500z00_1529;
BgL_arg1500z00_1529 = 
(long)(BgL_arg1389z00_1522); 
return 
BINT(
(long)(BgL_arg1500z00_1529));} }  else 
{ /* Llib/custom.scm 116 */
return 
BINT(
(BgL_n1z00_1518%BgL_n2z00_1519));} } } } } 

}



/* &custom-hash */
obj_t BGl_z62customzd2hashzb0zz__customz00(obj_t BgL_envz00_1742, obj_t BgL_customz00_1743, obj_t BgL_modz00_1744)
{
{ /* Llib/custom.scm 114 */
{ /* Llib/custom.scm 115 */
 int BgL_auxz00_1840; obj_t BgL_auxz00_1833;
{ /* Llib/custom.scm 115 */
 obj_t BgL_tmpz00_1841;
if(
INTEGERP(BgL_modz00_1744))
{ /* Llib/custom.scm 115 */
BgL_tmpz00_1841 = BgL_modz00_1744
; }  else 
{ 
 obj_t BgL_auxz00_1844;
BgL_auxz00_1844 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(4540L), BGl_string1601z00zz__customz00, BGl_string1602z00zz__customz00, BgL_modz00_1744); 
FAILURE(BgL_auxz00_1844,BFALSE,BFALSE);} 
BgL_auxz00_1840 = 
CINT(BgL_tmpz00_1841); } 
if(
CUSTOMP(BgL_customz00_1743))
{ /* Llib/custom.scm 115 */
BgL_auxz00_1833 = BgL_customz00_1743
; }  else 
{ 
 obj_t BgL_auxz00_1836;
BgL_auxz00_1836 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1595z00zz__customz00, 
BINT(4540L), BGl_string1601z00zz__customz00, BGl_string1597z00zz__customz00, BgL_customz00_1743); 
FAILURE(BgL_auxz00_1836,BFALSE,BFALSE);} 
return 
BGl_customzd2hashzd2zz__customz00(BgL_auxz00_1833, BgL_auxz00_1840);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__customz00(void)
{
{ /* Llib/custom.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__customz00(void)
{
{ /* Llib/custom.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__customz00(void)
{
{ /* Llib/custom.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__customz00(void)
{
{ /* Llib/custom.scm 14 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1603z00zz__customz00));} 

}

#ifdef __cplusplus
}
#endif
