/*===========================================================================*/
/*   (Llib/ucs2.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/ucs2.scm -indent -o objs/obj_s/Llib/ucs2.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___UCS2_TYPE_DEFINITIONS
#define BGL___UCS2_TYPE_DEFINITIONS
#endif // BGL___UCS2_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62ucs2ze3zf3z72zz__ucs2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2upcasezb0zz__ucs2z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cizc3zf3ze2zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zc3zd3zf3ze3zz__ucs2z00(ucs2_t, ucs2_t);
static obj_t BGl_z62ucs2zc3zd3zf3z81zz__ucs2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2lowerzd2casezf3z91zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2ze3charz53zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62integerzd2ze3ucs2zd2urz81zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__ucs2z00 = BUNSPEC;
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cizd3zf3zf2zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cize3zf3zc2zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL ucs2_t BGl_integerzd2ze3ucs2z31zz__ucs2z00(int);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL bool_t BGl_ucs2zc3zf3z30zz__ucs2z00(ucs2_t, ucs2_t);
static obj_t BGl_z62ucs2zd2alphabeticzf3z43zz__ucs2z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zf3zf3zz__ucs2z00(obj_t);
extern bool_t ucs2_digitp(ucs2_t);
BGL_EXPORTED_DECL int BGl_ucs2zd2ze3integerz31zz__ucs2z00(ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2ze3zd3zf3zc3zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd3zf3z20zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2numericzf3z21zz__ucs2z00(ucs2_t);
static obj_t BGl_z62ucs2ze3zd3zf3za1zz__ucs2z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2downcasezd2zz__ucs2z00(ucs2_t);
BGL_EXPORTED_DECL ucs2_t BGl_charzd2ze3ucs2z31zz__ucs2z00(unsigned char);
extern unsigned char BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(long);
static obj_t BGl_cnstzd2initzd2zz__ucs2z00(void);
BGL_EXPORTED_DECL bool_t BGl_ucs2ze3zf3z10zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2upperzd2casezf3zf3zz__ucs2z00(ucs2_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__ucs2z00(void);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cizc3zd3zf3z31zz__ucs2z00(ucs2_t, ucs2_t);
extern ucs2_t ucs2_tolower(ucs2_t);
static obj_t BGl_gczd2rootszd2initz00zz__ucs2z00(void);
static obj_t BGl_z62integerzd2ze3ucs2z53zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62charzd2ze3ucs2z53zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62ucs2zf3z91zz__ucs2z00(obj_t, obj_t);
BGL_EXPORTED_DECL ucs2_t BGl_ucs2zd2upcasezd2zz__ucs2z00(ucs2_t);
static obj_t BGl_z62ucs2zd2cizc3zd3zf3z53zz__ucs2z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2whitespacezf3z21zz__ucs2z00(ucs2_t);
extern bool_t ucs2_upperp(ucs2_t);
static obj_t BGl_z62ucs2zd2ze3integerz53zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2numericzf3z43zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd2downcasezb0zz__ucs2z00(obj_t, obj_t);
extern bool_t ucs2_definedp(int);
extern ucs2_t ucs2_toupper(ucs2_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_symbol1530z00zz__ucs2z00 = BUNSPEC;
extern bool_t ucs2_whitespacep(ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2cize3zd3zf3z11zz__ucs2z00(ucs2_t, ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2lowerzd2casezf3zf3zz__ucs2z00(ucs2_t);
static obj_t BGl_symbol1540z00zz__ucs2z00 = BUNSPEC;
BGL_EXPORTED_DECL ucs2_t BGl_integerzd2ze3ucs2zd2urze3zz__ucs2z00(int);
static obj_t BGl_z62ucs2zd2upperzd2casezf3z91zz__ucs2z00(obj_t, obj_t);
extern bool_t ucs2_lowerp(ucs2_t);
static obj_t BGl_z62ucs2zd2cize3zd3zf3z73zz__ucs2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2cizc3zf3z80zz__ucs2z00(obj_t, obj_t, obj_t);
extern bool_t ucs2_letterp(ucs2_t);
BGL_EXPORTED_DECL bool_t BGl_ucs2zd2alphabeticzf3z21zz__ucs2z00(ucs2_t);
static obj_t BGl_z62ucs2zd2cizd3zf3z90zz__ucs2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zc3zf3z52zz__ucs2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2cize3zf3za0zz__ucs2z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62ucs2zd2whitespacezf3z43zz__ucs2z00(obj_t, obj_t);
static obj_t BGl_z62ucs2zd3zf3z42zz__ucs2z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL unsigned char BGl_ucs2zd2ze3charz31zz__ucs2z00(ucs2_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_integerzd2ze3ucs2zd2urzd2envz31zz__ucs2z00, BgL_bgl_za762integerza7d2za7e31545za7, BGl_z62integerzd2ze3ucs2zd2urz81zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zc3zf3zd2envze2zz__ucs2z00, BgL_bgl_za762ucs2za7c3za7f3za7521546z00, BGl_z62ucs2zc3zf3z52zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zf3zd2envz21zz__ucs2z00, BgL_bgl_za762ucs2za7f3za791za7za7_1547za7, BGl_z62ucs2zf3z91zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2upcasezd2envz00zz__ucs2z00, BgL_bgl_za762ucs2za7d2upcase1548z00, BGl_z62ucs2zd2upcasezb0zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2cizc3zf3zd2envz30zz__ucs2z00, BgL_bgl_za762ucs2za7d2ciza7c3za71549z00, BGl_z62ucs2zd2cizc3zf3z80zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zc3zd3zf3zd2envz31zz__ucs2z00, BgL_bgl_za762ucs2za7c3za7d3za7f31550z00, BGl_z62ucs2zc3zd3zf3z81zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2numericzf3zd2envzf3zz__ucs2z00, BgL_bgl_za762ucs2za7d2numeri1551z00, BGl_z62ucs2zd2numericzf3z43zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1511z00zz__ucs2z00, BgL_bgl_string1511za700za7za7_1552za7, "/tmp/bigloo/runtime/Llib/ucs2.scm", 33 );
DEFINE_STRING( BGl_string1512z00zz__ucs2z00, BgL_bgl_string1512za700za7za7_1553za7, "&ucs2=?", 7 );
DEFINE_STRING( BGl_string1513z00zz__ucs2z00, BgL_bgl_string1513za700za7za7_1554za7, "bucs2", 5 );
DEFINE_STRING( BGl_string1514z00zz__ucs2z00, BgL_bgl_string1514za700za7za7_1555za7, "&ucs2<?", 7 );
DEFINE_STRING( BGl_string1515z00zz__ucs2z00, BgL_bgl_string1515za700za7za7_1556za7, "&ucs2>?", 7 );
DEFINE_STRING( BGl_string1516z00zz__ucs2z00, BgL_bgl_string1516za700za7za7_1557za7, "&ucs2<=?", 8 );
DEFINE_STRING( BGl_string1517z00zz__ucs2z00, BgL_bgl_string1517za700za7za7_1558za7, "&ucs2>=?", 8 );
DEFINE_STRING( BGl_string1518z00zz__ucs2z00, BgL_bgl_string1518za700za7za7_1559za7, "&ucs2-ci=?", 10 );
DEFINE_STRING( BGl_string1519z00zz__ucs2z00, BgL_bgl_string1519za700za7za7_1560za7, "&ucs2-ci<?", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2alphabeticzf3zd2envzf3zz__ucs2z00, BgL_bgl_za762ucs2za7d2alphab1561z00, BGl_z62ucs2zd2alphabeticzf3z43zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1520z00zz__ucs2z00, BgL_bgl_string1520za700za7za7_1562za7, "&ucs2-ci>?", 10 );
DEFINE_STRING( BGl_string1521z00zz__ucs2z00, BgL_bgl_string1521za700za7za7_1563za7, "&ucs2-ci<=?", 11 );
DEFINE_STRING( BGl_string1522z00zz__ucs2z00, BgL_bgl_string1522za700za7za7_1564za7, "&ucs2-ci>=?", 11 );
DEFINE_STRING( BGl_string1523z00zz__ucs2z00, BgL_bgl_string1523za700za7za7_1565za7, "&ucs2-alphabetic?", 17 );
DEFINE_STRING( BGl_string1524z00zz__ucs2z00, BgL_bgl_string1524za700za7za7_1566za7, "&ucs2-numeric?", 14 );
DEFINE_STRING( BGl_string1525z00zz__ucs2z00, BgL_bgl_string1525za700za7za7_1567za7, "&ucs2-whitespace?", 17 );
DEFINE_STRING( BGl_string1526z00zz__ucs2z00, BgL_bgl_string1526za700za7za7_1568za7, "&ucs2-upper-case?", 17 );
DEFINE_STRING( BGl_string1527z00zz__ucs2z00, BgL_bgl_string1527za700za7za7_1569za7, "&ucs2-lower-case?", 17 );
DEFINE_STRING( BGl_string1528z00zz__ucs2z00, BgL_bgl_string1528za700za7za7_1570za7, "&ucs2-upcase", 12 );
DEFINE_STRING( BGl_string1529z00zz__ucs2z00, BgL_bgl_string1529za700za7za7_1571za7, "&ucs2-downcase", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2lowerzd2casezf3zd2envz21zz__ucs2z00, BgL_bgl_za762ucs2za7d2lowerza71572za7, BGl_z62ucs2zd2lowerzd2casezf3z91zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2ze3charzd2envze3zz__ucs2z00, BgL_bgl_za762ucs2za7d2za7e3cha1573za7, BGl_z62ucs2zd2ze3charz53zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1531z00zz__ucs2z00, BgL_bgl_string1531za700za7za7_1574za7, "integer->ucs2", 13 );
DEFINE_STRING( BGl_string1532z00zz__ucs2z00, BgL_bgl_string1532za700za7za7_1575za7, "Undefined UCS-2 character", 25 );
DEFINE_STRING( BGl_string1533z00zz__ucs2z00, BgL_bgl_string1533za700za7za7_1576za7, "integer out of range or ", 24 );
DEFINE_STRING( BGl_string1534z00zz__ucs2z00, BgL_bgl_string1534za700za7za7_1577za7, "&integer->ucs2", 14 );
DEFINE_STRING( BGl_string1535z00zz__ucs2z00, BgL_bgl_string1535za700za7za7_1578za7, "bint", 4 );
DEFINE_STRING( BGl_string1536z00zz__ucs2z00, BgL_bgl_string1536za700za7za7_1579za7, "&integer->ucs2-ur", 17 );
DEFINE_STRING( BGl_string1537z00zz__ucs2z00, BgL_bgl_string1537za700za7za7_1580za7, "&ucs2->integer", 14 );
DEFINE_STRING( BGl_string1538z00zz__ucs2z00, BgL_bgl_string1538za700za7za7_1581za7, "&char->ucs2", 11 );
DEFINE_STRING( BGl_string1539z00zz__ucs2z00, BgL_bgl_string1539za700za7za7_1582za7, "bchar", 5 );
DEFINE_STRING( BGl_string1541z00zz__ucs2z00, BgL_bgl_string1541za700za7za7_1583za7, "ucs2->char", 10 );
DEFINE_STRING( BGl_string1542z00zz__ucs2z00, BgL_bgl_string1542za700za7za7_1584za7, "UCS-2 character out of ISO-LATIN-1 range", 40 );
DEFINE_STRING( BGl_string1543z00zz__ucs2z00, BgL_bgl_string1543za700za7za7_1585za7, "&ucs2->char", 11 );
DEFINE_STRING( BGl_string1544z00zz__ucs2z00, BgL_bgl_string1544za700za7za7_1586za7, "__ucs2", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd3zf3zd2envzf2zz__ucs2z00, BgL_bgl_za762ucs2za7d3za7f3za7421587z00, BGl_z62ucs2zd3zf3z42zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2cizd3zf3zd2envz20zz__ucs2z00, BgL_bgl_za762ucs2za7d2ciza7d3za71588z00, BGl_z62ucs2zd2cizd3zf3z90zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2cize3zd3zf3zd2envzc3zz__ucs2z00, BgL_bgl_za762ucs2za7d2ciza7e3za71589z00, BGl_z62ucs2zd2cize3zd3zf3z73zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_integerzd2ze3ucs2zd2envze3zz__ucs2z00, BgL_bgl_za762integerza7d2za7e31590za7, BGl_z62integerzd2ze3ucs2z53zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2ze3integerzd2envze3zz__ucs2z00, BgL_bgl_za762ucs2za7d2za7e3int1591za7, BGl_z62ucs2zd2ze3integerz53zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2downcasezd2envz00zz__ucs2z00, BgL_bgl_za762ucs2za7d2downca1592z00, BGl_z62ucs2zd2downcasezb0zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_charzd2ze3ucs2zd2envze3zz__ucs2z00, BgL_bgl_za762charza7d2za7e3ucs1593za7, BGl_z62charzd2ze3ucs2z53zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2ze3zf3zd2envzc2zz__ucs2z00, BgL_bgl_za762ucs2za7e3za7f3za7721594z00, BGl_z62ucs2ze3zf3z72zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2upperzd2casezf3zd2envz21zz__ucs2z00, BgL_bgl_za762ucs2za7d2upperza71595za7, BGl_z62ucs2zd2upperzd2casezf3z91zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2cizc3zd3zf3zd2envze3zz__ucs2z00, BgL_bgl_za762ucs2za7d2ciza7c3za71596z00, BGl_z62ucs2zd2cizc3zd3zf3z53zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2cize3zf3zd2envz10zz__ucs2z00, BgL_bgl_za762ucs2za7d2ciza7e3za71597z00, BGl_z62ucs2zd2cize3zf3za0zz__ucs2z00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2zd2whitespacezf3zd2envzf3zz__ucs2z00, BgL_bgl_za762ucs2za7d2whites1598z00, BGl_z62ucs2zd2whitespacezf3z43zz__ucs2z00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_ucs2ze3zd3zf3zd2envz11zz__ucs2z00, BgL_bgl_za762ucs2za7e3za7d3za7f31599z00, BGl_z62ucs2ze3zd3zf3za1zz__ucs2z00, 0L, BUNSPEC, 2 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__ucs2z00) );
ADD_ROOT( (void *)(&BGl_symbol1530z00zz__ucs2z00) );
ADD_ROOT( (void *)(&BGl_symbol1540z00zz__ucs2z00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long BgL_checksumz00_1268, char * BgL_fromz00_1269)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__ucs2z00))
{ 
BGl_requirezd2initializa7ationz75zz__ucs2z00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__ucs2z00(); 
BGl_cnstzd2initzd2zz__ucs2z00(); 
return 
BGl_importedzd2moduleszd2initz00zz__ucs2z00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__ucs2z00(void)
{
{ /* Llib/ucs2.scm 14 */
BGl_symbol1530z00zz__ucs2z00 = 
bstring_to_symbol(BGl_string1531z00zz__ucs2z00); 
return ( 
BGl_symbol1540z00zz__ucs2z00 = 
bstring_to_symbol(BGl_string1541z00zz__ucs2z00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__ucs2z00(void)
{
{ /* Llib/ucs2.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* ucs2? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zf3zf3zz__ucs2z00(obj_t BgL_objz00_3)
{
{ /* Llib/ucs2.scm 146 */
return 
UCS2P(BgL_objz00_3);} 

}



/* &ucs2? */
obj_t BGl_z62ucs2zf3z91zz__ucs2z00(obj_t BgL_envz00_1142, obj_t BgL_objz00_1143)
{
{ /* Llib/ucs2.scm 146 */
return 
BBOOL(
BGl_ucs2zf3zf3zz__ucs2z00(BgL_objz00_1143));} 

}



/* ucs2=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd3zf3z20zz__ucs2z00(ucs2_t BgL_ucs2az00_4, ucs2_t BgL_ucs2bz00_5)
{
{ /* Llib/ucs2.scm 152 */
return 
(BgL_ucs2az00_4==BgL_ucs2bz00_5);} 

}



/* &ucs2=? */
obj_t BGl_z62ucs2zd3zf3z42zz__ucs2z00(obj_t BgL_envz00_1144, obj_t BgL_ucs2az00_1145, obj_t BgL_ucs2bz00_1146)
{
{ /* Llib/ucs2.scm 152 */
{ /* Llib/ucs2.scm 153 */
 bool_t BgL_tmpz00_1283;
{ /* Llib/ucs2.scm 153 */
 ucs2_t BgL_auxz00_1293; ucs2_t BgL_auxz00_1284;
{ /* Llib/ucs2.scm 153 */
 obj_t BgL_tmpz00_1294;
if(
UCS2P(BgL_ucs2bz00_1146))
{ /* Llib/ucs2.scm 153 */
BgL_tmpz00_1294 = BgL_ucs2bz00_1146
; }  else 
{ 
 obj_t BgL_auxz00_1297;
BgL_auxz00_1297 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(6361L), BGl_string1512z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1146); 
FAILURE(BgL_auxz00_1297,BFALSE,BFALSE);} 
BgL_auxz00_1293 = 
CUCS2(BgL_tmpz00_1294); } 
{ /* Llib/ucs2.scm 153 */
 obj_t BgL_tmpz00_1285;
if(
UCS2P(BgL_ucs2az00_1145))
{ /* Llib/ucs2.scm 153 */
BgL_tmpz00_1285 = BgL_ucs2az00_1145
; }  else 
{ 
 obj_t BgL_auxz00_1288;
BgL_auxz00_1288 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(6361L), BGl_string1512z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1145); 
FAILURE(BgL_auxz00_1288,BFALSE,BFALSE);} 
BgL_auxz00_1284 = 
CUCS2(BgL_tmpz00_1285); } 
BgL_tmpz00_1283 = 
BGl_ucs2zd3zf3z20zz__ucs2z00(BgL_auxz00_1284, BgL_auxz00_1293); } 
return 
BBOOL(BgL_tmpz00_1283);} } 

}



/* ucs2<? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zc3zf3z30zz__ucs2z00(ucs2_t BgL_ucs2az00_6, ucs2_t BgL_ucs2bz00_7)
{
{ /* Llib/ucs2.scm 158 */
return 
(BgL_ucs2az00_6<BgL_ucs2bz00_7);} 

}



/* &ucs2<? */
obj_t BGl_z62ucs2zc3zf3z52zz__ucs2z00(obj_t BgL_envz00_1147, obj_t BgL_ucs2az00_1148, obj_t BgL_ucs2bz00_1149)
{
{ /* Llib/ucs2.scm 158 */
{ /* Llib/ucs2.scm 159 */
 bool_t BgL_tmpz00_1305;
{ /* Llib/ucs2.scm 159 */
 ucs2_t BgL_auxz00_1315; ucs2_t BgL_auxz00_1306;
{ /* Llib/ucs2.scm 159 */
 obj_t BgL_tmpz00_1316;
if(
UCS2P(BgL_ucs2bz00_1149))
{ /* Llib/ucs2.scm 159 */
BgL_tmpz00_1316 = BgL_ucs2bz00_1149
; }  else 
{ 
 obj_t BgL_auxz00_1319;
BgL_auxz00_1319 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(6647L), BGl_string1514z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1149); 
FAILURE(BgL_auxz00_1319,BFALSE,BFALSE);} 
BgL_auxz00_1315 = 
CUCS2(BgL_tmpz00_1316); } 
{ /* Llib/ucs2.scm 159 */
 obj_t BgL_tmpz00_1307;
if(
UCS2P(BgL_ucs2az00_1148))
{ /* Llib/ucs2.scm 159 */
BgL_tmpz00_1307 = BgL_ucs2az00_1148
; }  else 
{ 
 obj_t BgL_auxz00_1310;
BgL_auxz00_1310 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(6647L), BGl_string1514z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1148); 
FAILURE(BgL_auxz00_1310,BFALSE,BFALSE);} 
BgL_auxz00_1306 = 
CUCS2(BgL_tmpz00_1307); } 
BgL_tmpz00_1305 = 
BGl_ucs2zc3zf3z30zz__ucs2z00(BgL_auxz00_1306, BgL_auxz00_1315); } 
return 
BBOOL(BgL_tmpz00_1305);} } 

}



/* ucs2>? */
BGL_EXPORTED_DEF bool_t BGl_ucs2ze3zf3z10zz__ucs2z00(ucs2_t BgL_ucs2az00_8, ucs2_t BgL_ucs2bz00_9)
{
{ /* Llib/ucs2.scm 164 */
return 
(BgL_ucs2az00_8>BgL_ucs2bz00_9);} 

}



/* &ucs2>? */
obj_t BGl_z62ucs2ze3zf3z72zz__ucs2z00(obj_t BgL_envz00_1150, obj_t BgL_ucs2az00_1151, obj_t BgL_ucs2bz00_1152)
{
{ /* Llib/ucs2.scm 164 */
{ /* Llib/ucs2.scm 165 */
 bool_t BgL_tmpz00_1327;
{ /* Llib/ucs2.scm 165 */
 ucs2_t BgL_auxz00_1337; ucs2_t BgL_auxz00_1328;
{ /* Llib/ucs2.scm 165 */
 obj_t BgL_tmpz00_1338;
if(
UCS2P(BgL_ucs2bz00_1152))
{ /* Llib/ucs2.scm 165 */
BgL_tmpz00_1338 = BgL_ucs2bz00_1152
; }  else 
{ 
 obj_t BgL_auxz00_1341;
BgL_auxz00_1341 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(6934L), BGl_string1515z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1152); 
FAILURE(BgL_auxz00_1341,BFALSE,BFALSE);} 
BgL_auxz00_1337 = 
CUCS2(BgL_tmpz00_1338); } 
{ /* Llib/ucs2.scm 165 */
 obj_t BgL_tmpz00_1329;
if(
UCS2P(BgL_ucs2az00_1151))
{ /* Llib/ucs2.scm 165 */
BgL_tmpz00_1329 = BgL_ucs2az00_1151
; }  else 
{ 
 obj_t BgL_auxz00_1332;
BgL_auxz00_1332 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(6934L), BGl_string1515z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1151); 
FAILURE(BgL_auxz00_1332,BFALSE,BFALSE);} 
BgL_auxz00_1328 = 
CUCS2(BgL_tmpz00_1329); } 
BgL_tmpz00_1327 = 
BGl_ucs2ze3zf3z10zz__ucs2z00(BgL_auxz00_1328, BgL_auxz00_1337); } 
return 
BBOOL(BgL_tmpz00_1327);} } 

}



/* ucs2<=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zc3zd3zf3ze3zz__ucs2z00(ucs2_t BgL_ucs2az00_10, ucs2_t BgL_ucs2bz00_11)
{
{ /* Llib/ucs2.scm 170 */
return 
(BgL_ucs2az00_10<=BgL_ucs2bz00_11);} 

}



/* &ucs2<=? */
obj_t BGl_z62ucs2zc3zd3zf3z81zz__ucs2z00(obj_t BgL_envz00_1153, obj_t BgL_ucs2az00_1154, obj_t BgL_ucs2bz00_1155)
{
{ /* Llib/ucs2.scm 170 */
{ /* Llib/ucs2.scm 171 */
 bool_t BgL_tmpz00_1349;
{ /* Llib/ucs2.scm 171 */
 ucs2_t BgL_auxz00_1359; ucs2_t BgL_auxz00_1350;
{ /* Llib/ucs2.scm 171 */
 obj_t BgL_tmpz00_1360;
if(
UCS2P(BgL_ucs2bz00_1155))
{ /* Llib/ucs2.scm 171 */
BgL_tmpz00_1360 = BgL_ucs2bz00_1155
; }  else 
{ 
 obj_t BgL_auxz00_1363;
BgL_auxz00_1363 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(7221L), BGl_string1516z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1155); 
FAILURE(BgL_auxz00_1363,BFALSE,BFALSE);} 
BgL_auxz00_1359 = 
CUCS2(BgL_tmpz00_1360); } 
{ /* Llib/ucs2.scm 171 */
 obj_t BgL_tmpz00_1351;
if(
UCS2P(BgL_ucs2az00_1154))
{ /* Llib/ucs2.scm 171 */
BgL_tmpz00_1351 = BgL_ucs2az00_1154
; }  else 
{ 
 obj_t BgL_auxz00_1354;
BgL_auxz00_1354 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(7221L), BGl_string1516z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1154); 
FAILURE(BgL_auxz00_1354,BFALSE,BFALSE);} 
BgL_auxz00_1350 = 
CUCS2(BgL_tmpz00_1351); } 
BgL_tmpz00_1349 = 
BGl_ucs2zc3zd3zf3ze3zz__ucs2z00(BgL_auxz00_1350, BgL_auxz00_1359); } 
return 
BBOOL(BgL_tmpz00_1349);} } 

}



/* ucs2>=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2ze3zd3zf3zc3zz__ucs2z00(ucs2_t BgL_ucs2az00_12, ucs2_t BgL_ucs2bz00_13)
{
{ /* Llib/ucs2.scm 176 */
return 
(BgL_ucs2az00_12>=BgL_ucs2bz00_13);} 

}



/* &ucs2>=? */
obj_t BGl_z62ucs2ze3zd3zf3za1zz__ucs2z00(obj_t BgL_envz00_1156, obj_t BgL_ucs2az00_1157, obj_t BgL_ucs2bz00_1158)
{
{ /* Llib/ucs2.scm 176 */
{ /* Llib/ucs2.scm 177 */
 bool_t BgL_tmpz00_1371;
{ /* Llib/ucs2.scm 177 */
 ucs2_t BgL_auxz00_1381; ucs2_t BgL_auxz00_1372;
{ /* Llib/ucs2.scm 177 */
 obj_t BgL_tmpz00_1382;
if(
UCS2P(BgL_ucs2bz00_1158))
{ /* Llib/ucs2.scm 177 */
BgL_tmpz00_1382 = BgL_ucs2bz00_1158
; }  else 
{ 
 obj_t BgL_auxz00_1385;
BgL_auxz00_1385 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(7510L), BGl_string1517z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1158); 
FAILURE(BgL_auxz00_1385,BFALSE,BFALSE);} 
BgL_auxz00_1381 = 
CUCS2(BgL_tmpz00_1382); } 
{ /* Llib/ucs2.scm 177 */
 obj_t BgL_tmpz00_1373;
if(
UCS2P(BgL_ucs2az00_1157))
{ /* Llib/ucs2.scm 177 */
BgL_tmpz00_1373 = BgL_ucs2az00_1157
; }  else 
{ 
 obj_t BgL_auxz00_1376;
BgL_auxz00_1376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(7510L), BGl_string1517z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1157); 
FAILURE(BgL_auxz00_1376,BFALSE,BFALSE);} 
BgL_auxz00_1372 = 
CUCS2(BgL_tmpz00_1373); } 
BgL_tmpz00_1371 = 
BGl_ucs2ze3zd3zf3zc3zz__ucs2z00(BgL_auxz00_1372, BgL_auxz00_1381); } 
return 
BBOOL(BgL_tmpz00_1371);} } 

}



/* ucs2-ci=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cizd3zf3zf2zz__ucs2z00(ucs2_t BgL_ucs2az00_14, ucs2_t BgL_ucs2bz00_15)
{
{ /* Llib/ucs2.scm 182 */
return 
(
ucs2_toupper(BgL_ucs2az00_14)==
ucs2_toupper(BgL_ucs2bz00_15));} 

}



/* &ucs2-ci=? */
obj_t BGl_z62ucs2zd2cizd3zf3z90zz__ucs2z00(obj_t BgL_envz00_1159, obj_t BgL_ucs2az00_1160, obj_t BgL_ucs2bz00_1161)
{
{ /* Llib/ucs2.scm 182 */
{ /* Llib/ucs2.scm 183 */
 bool_t BgL_tmpz00_1395;
{ /* Llib/ucs2.scm 183 */
 ucs2_t BgL_auxz00_1405; ucs2_t BgL_auxz00_1396;
{ /* Llib/ucs2.scm 183 */
 obj_t BgL_tmpz00_1406;
if(
UCS2P(BgL_ucs2bz00_1161))
{ /* Llib/ucs2.scm 183 */
BgL_tmpz00_1406 = BgL_ucs2bz00_1161
; }  else 
{ 
 obj_t BgL_auxz00_1409;
BgL_auxz00_1409 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(7808L), BGl_string1518z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1161); 
FAILURE(BgL_auxz00_1409,BFALSE,BFALSE);} 
BgL_auxz00_1405 = 
CUCS2(BgL_tmpz00_1406); } 
{ /* Llib/ucs2.scm 183 */
 obj_t BgL_tmpz00_1397;
if(
UCS2P(BgL_ucs2az00_1160))
{ /* Llib/ucs2.scm 183 */
BgL_tmpz00_1397 = BgL_ucs2az00_1160
; }  else 
{ 
 obj_t BgL_auxz00_1400;
BgL_auxz00_1400 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(7808L), BGl_string1518z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1160); 
FAILURE(BgL_auxz00_1400,BFALSE,BFALSE);} 
BgL_auxz00_1396 = 
CUCS2(BgL_tmpz00_1397); } 
BgL_tmpz00_1395 = 
BGl_ucs2zd2cizd3zf3zf2zz__ucs2z00(BgL_auxz00_1396, BgL_auxz00_1405); } 
return 
BBOOL(BgL_tmpz00_1395);} } 

}



/* ucs2-ci<? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cizc3zf3ze2zz__ucs2z00(ucs2_t BgL_ucs2az00_16, ucs2_t BgL_ucs2bz00_17)
{
{ /* Llib/ucs2.scm 188 */
return 
(
ucs2_toupper(BgL_ucs2az00_16)<
ucs2_toupper(BgL_ucs2bz00_17));} 

}



/* &ucs2-ci<? */
obj_t BGl_z62ucs2zd2cizc3zf3z80zz__ucs2z00(obj_t BgL_envz00_1162, obj_t BgL_ucs2az00_1163, obj_t BgL_ucs2bz00_1164)
{
{ /* Llib/ucs2.scm 188 */
{ /* Llib/ucs2.scm 189 */
 bool_t BgL_tmpz00_1419;
{ /* Llib/ucs2.scm 189 */
 ucs2_t BgL_auxz00_1429; ucs2_t BgL_auxz00_1420;
{ /* Llib/ucs2.scm 189 */
 obj_t BgL_tmpz00_1430;
if(
UCS2P(BgL_ucs2bz00_1164))
{ /* Llib/ucs2.scm 189 */
BgL_tmpz00_1430 = BgL_ucs2bz00_1164
; }  else 
{ 
 obj_t BgL_auxz00_1433;
BgL_auxz00_1433 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(8123L), BGl_string1519z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1164); 
FAILURE(BgL_auxz00_1433,BFALSE,BFALSE);} 
BgL_auxz00_1429 = 
CUCS2(BgL_tmpz00_1430); } 
{ /* Llib/ucs2.scm 189 */
 obj_t BgL_tmpz00_1421;
if(
UCS2P(BgL_ucs2az00_1163))
{ /* Llib/ucs2.scm 189 */
BgL_tmpz00_1421 = BgL_ucs2az00_1163
; }  else 
{ 
 obj_t BgL_auxz00_1424;
BgL_auxz00_1424 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(8123L), BGl_string1519z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1163); 
FAILURE(BgL_auxz00_1424,BFALSE,BFALSE);} 
BgL_auxz00_1420 = 
CUCS2(BgL_tmpz00_1421); } 
BgL_tmpz00_1419 = 
BGl_ucs2zd2cizc3zf3ze2zz__ucs2z00(BgL_auxz00_1420, BgL_auxz00_1429); } 
return 
BBOOL(BgL_tmpz00_1419);} } 

}



/* ucs2-ci>? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cize3zf3zc2zz__ucs2z00(ucs2_t BgL_ucs2az00_18, ucs2_t BgL_ucs2bz00_19)
{
{ /* Llib/ucs2.scm 194 */
return 
(
ucs2_toupper(BgL_ucs2az00_18)>
ucs2_toupper(BgL_ucs2bz00_19));} 

}



/* &ucs2-ci>? */
obj_t BGl_z62ucs2zd2cize3zf3za0zz__ucs2z00(obj_t BgL_envz00_1165, obj_t BgL_ucs2az00_1166, obj_t BgL_ucs2bz00_1167)
{
{ /* Llib/ucs2.scm 194 */
{ /* Llib/ucs2.scm 195 */
 bool_t BgL_tmpz00_1443;
{ /* Llib/ucs2.scm 195 */
 ucs2_t BgL_auxz00_1453; ucs2_t BgL_auxz00_1444;
{ /* Llib/ucs2.scm 195 */
 obj_t BgL_tmpz00_1454;
if(
UCS2P(BgL_ucs2bz00_1167))
{ /* Llib/ucs2.scm 195 */
BgL_tmpz00_1454 = BgL_ucs2bz00_1167
; }  else 
{ 
 obj_t BgL_auxz00_1457;
BgL_auxz00_1457 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(8440L), BGl_string1520z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1167); 
FAILURE(BgL_auxz00_1457,BFALSE,BFALSE);} 
BgL_auxz00_1453 = 
CUCS2(BgL_tmpz00_1454); } 
{ /* Llib/ucs2.scm 195 */
 obj_t BgL_tmpz00_1445;
if(
UCS2P(BgL_ucs2az00_1166))
{ /* Llib/ucs2.scm 195 */
BgL_tmpz00_1445 = BgL_ucs2az00_1166
; }  else 
{ 
 obj_t BgL_auxz00_1448;
BgL_auxz00_1448 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(8440L), BGl_string1520z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1166); 
FAILURE(BgL_auxz00_1448,BFALSE,BFALSE);} 
BgL_auxz00_1444 = 
CUCS2(BgL_tmpz00_1445); } 
BgL_tmpz00_1443 = 
BGl_ucs2zd2cize3zf3zc2zz__ucs2z00(BgL_auxz00_1444, BgL_auxz00_1453); } 
return 
BBOOL(BgL_tmpz00_1443);} } 

}



/* ucs2-ci<=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cizc3zd3zf3z31zz__ucs2z00(ucs2_t BgL_ucs2az00_20, ucs2_t BgL_ucs2bz00_21)
{
{ /* Llib/ucs2.scm 200 */
return 
(
ucs2_toupper(BgL_ucs2az00_20)<=
ucs2_toupper(BgL_ucs2bz00_21));} 

}



/* &ucs2-ci<=? */
obj_t BGl_z62ucs2zd2cizc3zd3zf3z53zz__ucs2z00(obj_t BgL_envz00_1168, obj_t BgL_ucs2az00_1169, obj_t BgL_ucs2bz00_1170)
{
{ /* Llib/ucs2.scm 200 */
{ /* Llib/ucs2.scm 201 */
 bool_t BgL_tmpz00_1467;
{ /* Llib/ucs2.scm 201 */
 ucs2_t BgL_auxz00_1477; ucs2_t BgL_auxz00_1468;
{ /* Llib/ucs2.scm 201 */
 obj_t BgL_tmpz00_1478;
if(
UCS2P(BgL_ucs2bz00_1170))
{ /* Llib/ucs2.scm 201 */
BgL_tmpz00_1478 = BgL_ucs2bz00_1170
; }  else 
{ 
 obj_t BgL_auxz00_1481;
BgL_auxz00_1481 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(8757L), BGl_string1521z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1170); 
FAILURE(BgL_auxz00_1481,BFALSE,BFALSE);} 
BgL_auxz00_1477 = 
CUCS2(BgL_tmpz00_1478); } 
{ /* Llib/ucs2.scm 201 */
 obj_t BgL_tmpz00_1469;
if(
UCS2P(BgL_ucs2az00_1169))
{ /* Llib/ucs2.scm 201 */
BgL_tmpz00_1469 = BgL_ucs2az00_1169
; }  else 
{ 
 obj_t BgL_auxz00_1472;
BgL_auxz00_1472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(8757L), BGl_string1521z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1169); 
FAILURE(BgL_auxz00_1472,BFALSE,BFALSE);} 
BgL_auxz00_1468 = 
CUCS2(BgL_tmpz00_1469); } 
BgL_tmpz00_1467 = 
BGl_ucs2zd2cizc3zd3zf3z31zz__ucs2z00(BgL_auxz00_1468, BgL_auxz00_1477); } 
return 
BBOOL(BgL_tmpz00_1467);} } 

}



/* ucs2-ci>=? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2cize3zd3zf3z11zz__ucs2z00(ucs2_t BgL_ucs2az00_22, ucs2_t BgL_ucs2bz00_23)
{
{ /* Llib/ucs2.scm 206 */
return 
(
ucs2_toupper(BgL_ucs2az00_22)>=
ucs2_toupper(BgL_ucs2bz00_23));} 

}



/* &ucs2-ci>=? */
obj_t BGl_z62ucs2zd2cize3zd3zf3z73zz__ucs2z00(obj_t BgL_envz00_1171, obj_t BgL_ucs2az00_1172, obj_t BgL_ucs2bz00_1173)
{
{ /* Llib/ucs2.scm 206 */
{ /* Llib/ucs2.scm 207 */
 bool_t BgL_tmpz00_1491;
{ /* Llib/ucs2.scm 207 */
 ucs2_t BgL_auxz00_1501; ucs2_t BgL_auxz00_1492;
{ /* Llib/ucs2.scm 207 */
 obj_t BgL_tmpz00_1502;
if(
UCS2P(BgL_ucs2bz00_1173))
{ /* Llib/ucs2.scm 207 */
BgL_tmpz00_1502 = BgL_ucs2bz00_1173
; }  else 
{ 
 obj_t BgL_auxz00_1505;
BgL_auxz00_1505 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(9074L), BGl_string1522z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2bz00_1173); 
FAILURE(BgL_auxz00_1505,BFALSE,BFALSE);} 
BgL_auxz00_1501 = 
CUCS2(BgL_tmpz00_1502); } 
{ /* Llib/ucs2.scm 207 */
 obj_t BgL_tmpz00_1493;
if(
UCS2P(BgL_ucs2az00_1172))
{ /* Llib/ucs2.scm 207 */
BgL_tmpz00_1493 = BgL_ucs2az00_1172
; }  else 
{ 
 obj_t BgL_auxz00_1496;
BgL_auxz00_1496 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(9074L), BGl_string1522z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2az00_1172); 
FAILURE(BgL_auxz00_1496,BFALSE,BFALSE);} 
BgL_auxz00_1492 = 
CUCS2(BgL_tmpz00_1493); } 
BgL_tmpz00_1491 = 
BGl_ucs2zd2cize3zd3zf3z11zz__ucs2z00(BgL_auxz00_1492, BgL_auxz00_1501); } 
return 
BBOOL(BgL_tmpz00_1491);} } 

}



/* ucs2-alphabetic? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2alphabeticzf3z21zz__ucs2z00(ucs2_t BgL_ucs2z00_24)
{
{ /* Llib/ucs2.scm 212 */
BGL_TAIL return 
ucs2_letterp(BgL_ucs2z00_24);} 

}



/* &ucs2-alphabetic? */
obj_t BGl_z62ucs2zd2alphabeticzf3z43zz__ucs2z00(obj_t BgL_envz00_1174, obj_t BgL_ucs2z00_1175)
{
{ /* Llib/ucs2.scm 212 */
{ /* Llib/ucs2.scm 213 */
 bool_t BgL_tmpz00_1513;
{ /* Llib/ucs2.scm 213 */
 ucs2_t BgL_auxz00_1514;
{ /* Llib/ucs2.scm 213 */
 obj_t BgL_tmpz00_1515;
if(
UCS2P(BgL_ucs2z00_1175))
{ /* Llib/ucs2.scm 213 */
BgL_tmpz00_1515 = BgL_ucs2z00_1175
; }  else 
{ 
 obj_t BgL_auxz00_1518;
BgL_auxz00_1518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(9381L), BGl_string1523z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1175); 
FAILURE(BgL_auxz00_1518,BFALSE,BFALSE);} 
BgL_auxz00_1514 = 
CUCS2(BgL_tmpz00_1515); } 
BgL_tmpz00_1513 = 
BGl_ucs2zd2alphabeticzf3z21zz__ucs2z00(BgL_auxz00_1514); } 
return 
BBOOL(BgL_tmpz00_1513);} } 

}



/* ucs2-numeric? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2numericzf3z21zz__ucs2z00(ucs2_t BgL_ucs2z00_25)
{
{ /* Llib/ucs2.scm 218 */
BGL_TAIL return 
ucs2_digitp(BgL_ucs2z00_25);} 

}



/* &ucs2-numeric? */
obj_t BGl_z62ucs2zd2numericzf3z43zz__ucs2z00(obj_t BgL_envz00_1176, obj_t BgL_ucs2z00_1177)
{
{ /* Llib/ucs2.scm 218 */
{ /* Llib/ucs2.scm 219 */
 bool_t BgL_tmpz00_1526;
{ /* Llib/ucs2.scm 219 */
 ucs2_t BgL_auxz00_1527;
{ /* Llib/ucs2.scm 219 */
 obj_t BgL_tmpz00_1528;
if(
UCS2P(BgL_ucs2z00_1177))
{ /* Llib/ucs2.scm 219 */
BgL_tmpz00_1528 = BgL_ucs2z00_1177
; }  else 
{ 
 obj_t BgL_auxz00_1531;
BgL_auxz00_1531 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(9666L), BGl_string1524z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1177); 
FAILURE(BgL_auxz00_1531,BFALSE,BFALSE);} 
BgL_auxz00_1527 = 
CUCS2(BgL_tmpz00_1528); } 
BgL_tmpz00_1526 = 
BGl_ucs2zd2numericzf3z21zz__ucs2z00(BgL_auxz00_1527); } 
return 
BBOOL(BgL_tmpz00_1526);} } 

}



/* ucs2-whitespace? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2whitespacezf3z21zz__ucs2z00(ucs2_t BgL_ucs2z00_26)
{
{ /* Llib/ucs2.scm 224 */
BGL_TAIL return 
ucs2_whitespacep(BgL_ucs2z00_26);} 

}



/* &ucs2-whitespace? */
obj_t BGl_z62ucs2zd2whitespacezf3z43zz__ucs2z00(obj_t BgL_envz00_1178, obj_t BgL_ucs2z00_1179)
{
{ /* Llib/ucs2.scm 224 */
{ /* Llib/ucs2.scm 225 */
 bool_t BgL_tmpz00_1539;
{ /* Llib/ucs2.scm 225 */
 ucs2_t BgL_auxz00_1540;
{ /* Llib/ucs2.scm 225 */
 obj_t BgL_tmpz00_1541;
if(
UCS2P(BgL_ucs2z00_1179))
{ /* Llib/ucs2.scm 225 */
BgL_tmpz00_1541 = BgL_ucs2z00_1179
; }  else 
{ 
 obj_t BgL_auxz00_1544;
BgL_auxz00_1544 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(9953L), BGl_string1525z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1179); 
FAILURE(BgL_auxz00_1544,BFALSE,BFALSE);} 
BgL_auxz00_1540 = 
CUCS2(BgL_tmpz00_1541); } 
BgL_tmpz00_1539 = 
BGl_ucs2zd2whitespacezf3z21zz__ucs2z00(BgL_auxz00_1540); } 
return 
BBOOL(BgL_tmpz00_1539);} } 

}



/* ucs2-upper-case? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2upperzd2casezf3zf3zz__ucs2z00(ucs2_t BgL_ucs2z00_27)
{
{ /* Llib/ucs2.scm 230 */
BGL_TAIL return 
ucs2_upperp(BgL_ucs2z00_27);} 

}



/* &ucs2-upper-case? */
obj_t BGl_z62ucs2zd2upperzd2casezf3z91zz__ucs2z00(obj_t BgL_envz00_1180, obj_t BgL_ucs2z00_1181)
{
{ /* Llib/ucs2.scm 230 */
{ /* Llib/ucs2.scm 231 */
 bool_t BgL_tmpz00_1552;
{ /* Llib/ucs2.scm 231 */
 ucs2_t BgL_auxz00_1553;
{ /* Llib/ucs2.scm 231 */
 obj_t BgL_tmpz00_1554;
if(
UCS2P(BgL_ucs2z00_1181))
{ /* Llib/ucs2.scm 231 */
BgL_tmpz00_1554 = BgL_ucs2z00_1181
; }  else 
{ 
 obj_t BgL_auxz00_1557;
BgL_auxz00_1557 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(10245L), BGl_string1526z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1181); 
FAILURE(BgL_auxz00_1557,BFALSE,BFALSE);} 
BgL_auxz00_1553 = 
CUCS2(BgL_tmpz00_1554); } 
BgL_tmpz00_1552 = 
BGl_ucs2zd2upperzd2casezf3zf3zz__ucs2z00(BgL_auxz00_1553); } 
return 
BBOOL(BgL_tmpz00_1552);} } 

}



/* ucs2-lower-case? */
BGL_EXPORTED_DEF bool_t BGl_ucs2zd2lowerzd2casezf3zf3zz__ucs2z00(ucs2_t BgL_ucs2z00_28)
{
{ /* Llib/ucs2.scm 236 */
BGL_TAIL return 
ucs2_lowerp(BgL_ucs2z00_28);} 

}



/* &ucs2-lower-case? */
obj_t BGl_z62ucs2zd2lowerzd2casezf3z91zz__ucs2z00(obj_t BgL_envz00_1182, obj_t BgL_ucs2z00_1183)
{
{ /* Llib/ucs2.scm 236 */
{ /* Llib/ucs2.scm 237 */
 bool_t BgL_tmpz00_1565;
{ /* Llib/ucs2.scm 237 */
 ucs2_t BgL_auxz00_1566;
{ /* Llib/ucs2.scm 237 */
 obj_t BgL_tmpz00_1567;
if(
UCS2P(BgL_ucs2z00_1183))
{ /* Llib/ucs2.scm 237 */
BgL_tmpz00_1567 = BgL_ucs2z00_1183
; }  else 
{ 
 obj_t BgL_auxz00_1570;
BgL_auxz00_1570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(10532L), BGl_string1527z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1183); 
FAILURE(BgL_auxz00_1570,BFALSE,BFALSE);} 
BgL_auxz00_1566 = 
CUCS2(BgL_tmpz00_1567); } 
BgL_tmpz00_1565 = 
BGl_ucs2zd2lowerzd2casezf3zf3zz__ucs2z00(BgL_auxz00_1566); } 
return 
BBOOL(BgL_tmpz00_1565);} } 

}



/* ucs2-upcase */
BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2upcasezd2zz__ucs2z00(ucs2_t BgL_ucs2z00_29)
{
{ /* Llib/ucs2.scm 242 */
BGL_TAIL return 
ucs2_toupper(BgL_ucs2z00_29);} 

}



/* &ucs2-upcase */
obj_t BGl_z62ucs2zd2upcasezb0zz__ucs2z00(obj_t BgL_envz00_1184, obj_t BgL_ucs2z00_1185)
{
{ /* Llib/ucs2.scm 242 */
{ /* Llib/ucs2.scm 243 */
 ucs2_t BgL_tmpz00_1578;
{ /* Llib/ucs2.scm 243 */
 ucs2_t BgL_auxz00_1579;
{ /* Llib/ucs2.scm 243 */
 obj_t BgL_tmpz00_1580;
if(
UCS2P(BgL_ucs2z00_1185))
{ /* Llib/ucs2.scm 243 */
BgL_tmpz00_1580 = BgL_ucs2z00_1185
; }  else 
{ 
 obj_t BgL_auxz00_1583;
BgL_auxz00_1583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(10814L), BGl_string1528z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1185); 
FAILURE(BgL_auxz00_1583,BFALSE,BFALSE);} 
BgL_auxz00_1579 = 
CUCS2(BgL_tmpz00_1580); } 
BgL_tmpz00_1578 = 
BGl_ucs2zd2upcasezd2zz__ucs2z00(BgL_auxz00_1579); } 
return 
BUCS2(BgL_tmpz00_1578);} } 

}



/* ucs2-downcase */
BGL_EXPORTED_DEF ucs2_t BGl_ucs2zd2downcasezd2zz__ucs2z00(ucs2_t BgL_ucs2z00_30)
{
{ /* Llib/ucs2.scm 248 */
BGL_TAIL return 
ucs2_tolower(BgL_ucs2z00_30);} 

}



/* &ucs2-downcase */
obj_t BGl_z62ucs2zd2downcasezb0zz__ucs2z00(obj_t BgL_envz00_1186, obj_t BgL_ucs2z00_1187)
{
{ /* Llib/ucs2.scm 248 */
{ /* Llib/ucs2.scm 249 */
 ucs2_t BgL_tmpz00_1591;
{ /* Llib/ucs2.scm 249 */
 ucs2_t BgL_auxz00_1592;
{ /* Llib/ucs2.scm 249 */
 obj_t BgL_tmpz00_1593;
if(
UCS2P(BgL_ucs2z00_1187))
{ /* Llib/ucs2.scm 249 */
BgL_tmpz00_1593 = BgL_ucs2z00_1187
; }  else 
{ 
 obj_t BgL_auxz00_1596;
BgL_auxz00_1596 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(11098L), BGl_string1529z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1187); 
FAILURE(BgL_auxz00_1596,BFALSE,BFALSE);} 
BgL_auxz00_1592 = 
CUCS2(BgL_tmpz00_1593); } 
BgL_tmpz00_1591 = 
BGl_ucs2zd2downcasezd2zz__ucs2z00(BgL_auxz00_1592); } 
return 
BUCS2(BgL_tmpz00_1591);} } 

}



/* integer->ucs2 */
BGL_EXPORTED_DEF ucs2_t BGl_integerzd2ze3ucs2z31zz__ucs2z00(int BgL_intz00_31)
{
{ /* Llib/ucs2.scm 254 */
{ /* Llib/ucs2.scm 255 */
 bool_t BgL_test1628z00_1603;
if(
(
(long)(BgL_intz00_31)>=0L))
{ /* Llib/ucs2.scm 255 */
BgL_test1628z00_1603 = 
(
(long)(BgL_intz00_31)<65536L)
; }  else 
{ /* Llib/ucs2.scm 255 */
BgL_test1628z00_1603 = ((bool_t)0)
; } 
if(BgL_test1628z00_1603)
{ /* Llib/ucs2.scm 255 */
if(
ucs2_definedp(BgL_intz00_31))
{ /* Llib/ucs2.scm 256 */
return 
BGL_INT_TO_UCS2(BgL_intz00_31);}  else 
{ /* Llib/ucs2.scm 258 */
 obj_t BgL_tmpz00_1612;
{ /* Llib/ucs2.scm 258 */
 obj_t BgL_aux1497z00_1252;
BgL_aux1497z00_1252 = 
BGl_errorz00zz__errorz00(BGl_symbol1530z00zz__ucs2z00, BGl_string1532z00zz__ucs2z00, 
BINT(BgL_intz00_31)); 
if(
UCS2P(BgL_aux1497z00_1252))
{ /* Llib/ucs2.scm 258 */
BgL_tmpz00_1612 = BgL_aux1497z00_1252
; }  else 
{ 
 obj_t BgL_auxz00_1617;
BgL_auxz00_1617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(11496L), BGl_string1531z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_aux1497z00_1252); 
FAILURE(BgL_auxz00_1617,BFALSE,BFALSE);} } 
return 
CUCS2(BgL_tmpz00_1612);} }  else 
{ /* Llib/ucs2.scm 259 */
 obj_t BgL_tmpz00_1622;
{ /* Llib/ucs2.scm 259 */
 obj_t BgL_aux1499z00_1254;
BgL_aux1499z00_1254 = 
BGl_errorz00zz__errorz00(BGl_symbol1530z00zz__ucs2z00, BGl_string1533z00zz__ucs2z00, 
BINT(BgL_intz00_31)); 
if(
UCS2P(BgL_aux1499z00_1254))
{ /* Llib/ucs2.scm 259 */
BgL_tmpz00_1622 = BgL_aux1499z00_1254
; }  else 
{ 
 obj_t BgL_auxz00_1627;
BgL_auxz00_1627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(11559L), BGl_string1531z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_aux1499z00_1254); 
FAILURE(BgL_auxz00_1627,BFALSE,BFALSE);} } 
return 
CUCS2(BgL_tmpz00_1622);} } } 

}



/* &integer->ucs2 */
obj_t BGl_z62integerzd2ze3ucs2z53zz__ucs2z00(obj_t BgL_envz00_1188, obj_t BgL_intz00_1189)
{
{ /* Llib/ucs2.scm 254 */
{ /* Llib/ucs2.scm 255 */
 ucs2_t BgL_tmpz00_1632;
{ /* Llib/ucs2.scm 255 */
 int BgL_auxz00_1633;
{ /* Llib/ucs2.scm 255 */
 obj_t BgL_tmpz00_1634;
if(
INTEGERP(BgL_intz00_1189))
{ /* Llib/ucs2.scm 255 */
BgL_tmpz00_1634 = BgL_intz00_1189
; }  else 
{ 
 obj_t BgL_auxz00_1637;
BgL_auxz00_1637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(11394L), BGl_string1534z00zz__ucs2z00, BGl_string1535z00zz__ucs2z00, BgL_intz00_1189); 
FAILURE(BgL_auxz00_1637,BFALSE,BFALSE);} 
BgL_auxz00_1633 = 
CINT(BgL_tmpz00_1634); } 
BgL_tmpz00_1632 = 
BGl_integerzd2ze3ucs2z31zz__ucs2z00(BgL_auxz00_1633); } 
return 
BUCS2(BgL_tmpz00_1632);} } 

}



/* integer->ucs2-ur */
BGL_EXPORTED_DEF ucs2_t BGl_integerzd2ze3ucs2zd2urze3zz__ucs2z00(int BgL_intz00_32)
{
{ /* Llib/ucs2.scm 264 */
return 
BGL_INT_TO_UCS2(BgL_intz00_32);} 

}



/* &integer->ucs2-ur */
obj_t BGl_z62integerzd2ze3ucs2zd2urz81zz__ucs2z00(obj_t BgL_envz00_1190, obj_t BgL_intz00_1191)
{
{ /* Llib/ucs2.scm 264 */
{ /* Llib/ucs2.scm 265 */
 ucs2_t BgL_tmpz00_1645;
{ /* Llib/ucs2.scm 265 */
 int BgL_auxz00_1646;
{ /* Llib/ucs2.scm 265 */
 obj_t BgL_tmpz00_1647;
if(
INTEGERP(BgL_intz00_1191))
{ /* Llib/ucs2.scm 265 */
BgL_tmpz00_1647 = BgL_intz00_1191
; }  else 
{ 
 obj_t BgL_auxz00_1650;
BgL_auxz00_1650 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(11890L), BGl_string1536z00zz__ucs2z00, BGl_string1535z00zz__ucs2z00, BgL_intz00_1191); 
FAILURE(BgL_auxz00_1650,BFALSE,BFALSE);} 
BgL_auxz00_1646 = 
CINT(BgL_tmpz00_1647); } 
BgL_tmpz00_1645 = 
BGl_integerzd2ze3ucs2zd2urze3zz__ucs2z00(BgL_auxz00_1646); } 
return 
BUCS2(BgL_tmpz00_1645);} } 

}



/* ucs2->integer */
BGL_EXPORTED_DEF int BGl_ucs2zd2ze3integerz31zz__ucs2z00(ucs2_t BgL_ucs2z00_33)
{
{ /* Llib/ucs2.scm 270 */
{ /* Llib/ucs2.scm 271 */
 obj_t BgL_tmpz00_1657;
BgL_tmpz00_1657 = 
BUCS2(BgL_ucs2z00_33); 
return 
CUCS2(BgL_tmpz00_1657);} } 

}



/* &ucs2->integer */
obj_t BGl_z62ucs2zd2ze3integerz53zz__ucs2z00(obj_t BgL_envz00_1192, obj_t BgL_ucs2z00_1193)
{
{ /* Llib/ucs2.scm 270 */
{ /* Llib/ucs2.scm 271 */
 int BgL_tmpz00_1660;
{ /* Llib/ucs2.scm 271 */
 ucs2_t BgL_auxz00_1661;
{ /* Llib/ucs2.scm 271 */
 obj_t BgL_tmpz00_1662;
if(
UCS2P(BgL_ucs2z00_1193))
{ /* Llib/ucs2.scm 271 */
BgL_tmpz00_1662 = BgL_ucs2z00_1193
; }  else 
{ 
 obj_t BgL_auxz00_1665;
BgL_auxz00_1665 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(12186L), BGl_string1537z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1193); 
FAILURE(BgL_auxz00_1665,BFALSE,BFALSE);} 
BgL_auxz00_1661 = 
CUCS2(BgL_tmpz00_1662); } 
BgL_tmpz00_1660 = 
BGl_ucs2zd2ze3integerz31zz__ucs2z00(BgL_auxz00_1661); } 
return 
BINT(BgL_tmpz00_1660);} } 

}



/* char->ucs2 */
BGL_EXPORTED_DEF ucs2_t BGl_charzd2ze3ucs2z31zz__ucs2z00(unsigned char BgL_charz00_34)
{
{ /* Llib/ucs2.scm 276 */
{ /* Llib/ucs2.scm 265 */
 int BgL_tmpz00_1672;
BgL_tmpz00_1672 = 
(int)(
(
(unsigned char)(BgL_charz00_34))); 
return 
BGL_INT_TO_UCS2(BgL_tmpz00_1672);} } 

}



/* &char->ucs2 */
obj_t BGl_z62charzd2ze3ucs2z53zz__ucs2z00(obj_t BgL_envz00_1194, obj_t BgL_charz00_1195)
{
{ /* Llib/ucs2.scm 276 */
{ /* Llib/ucs2.scm 277 */
 ucs2_t BgL_tmpz00_1677;
{ /* Llib/ucs2.scm 277 */
 unsigned char BgL_auxz00_1678;
{ /* Llib/ucs2.scm 277 */
 obj_t BgL_tmpz00_1679;
if(
CHARP(BgL_charz00_1195))
{ /* Llib/ucs2.scm 277 */
BgL_tmpz00_1679 = BgL_charz00_1195
; }  else 
{ 
 obj_t BgL_auxz00_1682;
BgL_auxz00_1682 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(12499L), BGl_string1538z00zz__ucs2z00, BGl_string1539z00zz__ucs2z00, BgL_charz00_1195); 
FAILURE(BgL_auxz00_1682,BFALSE,BFALSE);} 
BgL_auxz00_1678 = 
CCHAR(BgL_tmpz00_1679); } 
BgL_tmpz00_1677 = 
BGl_charzd2ze3ucs2z31zz__ucs2z00(BgL_auxz00_1678); } 
return 
BUCS2(BgL_tmpz00_1677);} } 

}



/* ucs2->char */
BGL_EXPORTED_DEF unsigned char BGl_ucs2zd2ze3charz31zz__ucs2z00(ucs2_t BgL_ucs2z00_35)
{
{ /* Llib/ucs2.scm 282 */
{ /* Llib/ucs2.scm 283 */
 int BgL_intz00_781;
{ /* Llib/ucs2.scm 271 */
 obj_t BgL_tmpz00_1689;
BgL_tmpz00_1689 = 
BUCS2(BgL_ucs2z00_35); 
BgL_intz00_781 = 
CUCS2(BgL_tmpz00_1689); } 
if(
(
(long)(BgL_intz00_781)<256L))
{ /* Llib/ucs2.scm 284 */
return 
(char)(
BGl_integerzd2ze3charz31zz__r4_characters_6_6z00(
(long)(BgL_intz00_781)));}  else 
{ /* Llib/ucs2.scm 286 */
 obj_t BgL_tmpz00_1698;
{ /* Llib/ucs2.scm 286 */
 obj_t BgL_aux1507z00_1262;
BgL_aux1507z00_1262 = 
BGl_errorz00zz__errorz00(BGl_symbol1540z00zz__ucs2z00, BGl_string1542z00zz__ucs2z00, 
BUCS2(BgL_ucs2z00_35)); 
if(
CHARP(BgL_aux1507z00_1262))
{ /* Llib/ucs2.scm 286 */
BgL_tmpz00_1698 = BgL_aux1507z00_1262
; }  else 
{ 
 obj_t BgL_auxz00_1703;
BgL_auxz00_1703 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(12870L), BGl_string1541z00zz__ucs2z00, BGl_string1539z00zz__ucs2z00, BgL_aux1507z00_1262); 
FAILURE(BgL_auxz00_1703,BFALSE,BFALSE);} } 
return 
CCHAR(BgL_tmpz00_1698);} } } 

}



/* &ucs2->char */
obj_t BGl_z62ucs2zd2ze3charz53zz__ucs2z00(obj_t BgL_envz00_1196, obj_t BgL_ucs2z00_1197)
{
{ /* Llib/ucs2.scm 282 */
{ /* Llib/ucs2.scm 283 */
 unsigned char BgL_tmpz00_1708;
{ /* Llib/ucs2.scm 283 */
 ucs2_t BgL_auxz00_1709;
{ /* Llib/ucs2.scm 283 */
 obj_t BgL_tmpz00_1710;
if(
UCS2P(BgL_ucs2z00_1197))
{ /* Llib/ucs2.scm 283 */
BgL_tmpz00_1710 = BgL_ucs2z00_1197
; }  else 
{ 
 obj_t BgL_auxz00_1713;
BgL_auxz00_1713 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1511z00zz__ucs2z00, 
BINT(12786L), BGl_string1543z00zz__ucs2z00, BGl_string1513z00zz__ucs2z00, BgL_ucs2z00_1197); 
FAILURE(BgL_auxz00_1713,BFALSE,BFALSE);} 
BgL_auxz00_1709 = 
CUCS2(BgL_tmpz00_1710); } 
BgL_tmpz00_1708 = 
BGl_ucs2zd2ze3charz31zz__ucs2z00(BgL_auxz00_1709); } 
return 
BCHAR(BgL_tmpz00_1708);} } 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__ucs2z00(void)
{
{ /* Llib/ucs2.scm 14 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1544z00zz__ucs2z00));} 

}

#ifdef __cplusplus
}
#endif
