/*===========================================================================*/
/*   (Llib/tvector.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/tvector.scm -indent -o objs/obj_s/Llib/tvector.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___TVECTOR_TYPE_DEFINITIONS
#define BGL___TVECTOR_TYPE_DEFINITIONS
#endif // BGL___TVECTOR_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
static obj_t BGl_symbol1839z00zz__tvectorz00 = BUNSPEC;
BGL_EXPORTED_DECL int BGl_tvectorzd2lengthzd2zz__tvectorz00(obj_t);
static obj_t BGl_z62tvectorzd2refzb0zz__tvectorz00(obj_t, obj_t);
static obj_t BGl_symbol1845z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_list1812z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_symbol1847z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_z62tvectorzf3z91zz__tvectorz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__tvectorz00 = BUNSPEC;
static obj_t BGl_list1822z00zz__tvectorz00 = BUNSPEC;
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_list1838z00zz__tvectorz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_vectorzd2ze3tvectorz31zz__tvectorz00(obj_t, obj_t);
static obj_t BGl_symbol1794z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_z62vectorzd2ze3tvectorz53zz__tvectorz00(obj_t, obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__tvectorz00(void);
static obj_t BGl_list1844z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_symbol1799z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_z62getzd2tvectorzd2descriptorz62zz__tvectorz00(obj_t, obj_t);
extern obj_t create_struct(obj_t, int);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__tvectorz00(void);
static obj_t BGl_genericzd2initzd2zz__tvectorz00(void);
extern long bgl_list_length(obj_t);
BGL_EXPORTED_DECL obj_t BGl_tvectorzd2ze3vectorz31zz__tvectorz00(obj_t);
static obj_t BGl_z62tvectorzd2ze3vectorz53zz__tvectorz00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__tvectorz00(void);
static obj_t BGl_gczd2rootszd2initz00zz__tvectorz00(void);
static obj_t BGl_objectzd2initzd2zz__tvectorz00(void);
BGL_EXPORTED_DECL obj_t BGl_declarezd2tvectorz12zc0zz__tvectorz00(char *, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_tvectorzd2refzd2zz__tvectorz00(obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62tvectorzd2lengthzb0zz__tvectorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t get_tvector_descriptor(obj_t);
static obj_t BGl_methodzd2initzd2zz__tvectorz00(void);
BGL_EXPORTED_DECL bool_t BGl_tvectorzf3zf3zz__tvectorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_listzd2ze3tvectorz31zz__tvectorz00(obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t);
static obj_t BGl_symbol1801z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_z62declarezd2tvectorz12za2zz__tvectorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_bigloozd2casezd2sensitivityz00zz__readerz00(void);
static obj_t BGl_symbol1813z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_symbol1815z00zz__tvectorz00 = BUNSPEC;
extern obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_symbol1817z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_z62tvectorzd2idzb0zz__tvectorz00(obj_t, obj_t);
extern obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
static obj_t BGl_symbol1823z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_symbol1825z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_symbol1827z00zz__tvectorz00 = BUNSPEC;
static obj_t BGl_symbol1829z00zz__tvectorz00 = BUNSPEC;
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_za2tvectorzd2tableza2zd2zz__tvectorz00 = BUNSPEC;
static obj_t BGl_z62listzd2ze3tvectorz53zz__tvectorz00(obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string1790z00zz__tvectorz00, BgL_bgl_string1790za700za7za7_1852za7, "tvector", 7 );
DEFINE_STRING( BGl_string1791z00zz__tvectorz00, BgL_bgl_string1791za700za7za7_1853za7, "tvector-id", 10 );
DEFINE_STRING( BGl_string1792z00zz__tvectorz00, BgL_bgl_string1792za700za7za7_1854za7, "struct", 6 );
DEFINE_STRING( BGl_string1793z00zz__tvectorz00, BgL_bgl_string1793za700za7za7_1855za7, "symbol", 6 );
DEFINE_STRING( BGl_string1795z00zz__tvectorz00, BgL_bgl_string1795za700za7za7_1856za7, "tvect-descr", 11 );
DEFINE_STRING( BGl_string1796z00zz__tvectorz00, BgL_bgl_string1796za700za7za7_1857za7, "struct-ref:not an instance of", 29 );
DEFINE_STRING( BGl_string1797z00zz__tvectorz00, BgL_bgl_string1797za700za7za7_1858za7, "&tvector-id", 11 );
DEFINE_STRING( BGl_string1798z00zz__tvectorz00, BgL_bgl_string1798za700za7za7_1859za7, "&get-tvector-descriptor", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tvectorzf3zd2envz21zz__tvectorz00, BgL_bgl_za762tvectorza7f3za7911860za7, BGl_z62tvectorzf3z91zz__tvectorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_listzd2ze3tvectorzd2envze3zz__tvectorz00, BgL_bgl_za762listza7d2za7e3tve1861za7, BGl_z62listzd2ze3tvectorz53zz__tvectorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_vectorzd2ze3tvectorzd2envze3zz__tvectorz00, BgL_bgl_za762vectorza7d2za7e3t1862za7, BGl_z62vectorzd2ze3tvectorz53zz__tvectorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tvectorzd2lengthzd2envz00zz__tvectorz00, BgL_bgl_za762tvectorza7d2len1863z00, BGl_z62tvectorzd2lengthzb0zz__tvectorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tvectorzd2ze3vectorzd2envze3zz__tvectorz00, BgL_bgl_za762tvectorza7d2za7e31864za7, BGl_z62tvectorzd2ze3vectorz53zz__tvectorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1800z00zz__tvectorz00, BgL_bgl_string1800za700za7za7_1865za7, "upcase", 6 );
DEFINE_STRING( BGl_string1802z00zz__tvectorz00, BgL_bgl_string1802za700za7za7_1866za7, "downcase", 8 );
DEFINE_STRING( BGl_string1803z00zz__tvectorz00, BgL_bgl_string1803za700za7za7_1867za7, "declare-tvector!", 16 );
DEFINE_STRING( BGl_string1804z00zz__tvectorz00, BgL_bgl_string1804za700za7za7_1868za7, "struct-set!:not an instance of", 30 );
DEFINE_STRING( BGl_string1805z00zz__tvectorz00, BgL_bgl_string1805za700za7za7_1869za7, "&declare-tvector!", 17 );
DEFINE_STRING( BGl_string1806z00zz__tvectorz00, BgL_bgl_string1806za700za7za7_1870za7, "bstring", 7 );
DEFINE_STRING( BGl_string1807z00zz__tvectorz00, BgL_bgl_string1807za700za7za7_1871za7, "procedure", 9 );
DEFINE_STRING( BGl_string1808z00zz__tvectorz00, BgL_bgl_string1808za700za7za7_1872za7, "tvector-ref", 11 );
DEFINE_STRING( BGl_string1809z00zz__tvectorz00, BgL_bgl_string1809za700za7za7_1873za7, "&tvector-ref", 12 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2tvectorzd2descriptorzd2envzd2zz__tvectorz00, BgL_bgl_za762getza7d2tvector1874z00, BGl_z62getzd2tvectorzd2descriptorz62zz__tvectorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_declarezd2tvectorz12zd2envz12zz__tvectorz00, BgL_bgl_za762declareza7d2tve1875z00, BGl_z62declarezd2tvectorz12za2zz__tvectorz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string1810z00zz__tvectorz00, BgL_bgl_string1810za700za7za7_1876za7, "list->tvector", 13 );
DEFINE_STRING( BGl_string1811z00zz__tvectorz00, BgL_bgl_string1811za700za7za7_1877za7, "list->tvector:Wrong number of arguments", 39 );
DEFINE_STRING( BGl_string1814z00zz__tvectorz00, BgL_bgl_string1814za700za7za7_1878za7, "funcall", 7 );
DEFINE_STRING( BGl_string1816z00zz__tvectorz00, BgL_bgl_string1816za700za7za7_1879za7, "allocate", 8 );
DEFINE_STRING( BGl_string1818z00zz__tvectorz00, BgL_bgl_string1818za700za7za7_1880za7, "len", 3 );
DEFINE_STRING( BGl_string1819z00zz__tvectorz00, BgL_bgl_string1819za700za7za7_1881za7, "loop", 4 );
DEFINE_STRING( BGl_string1820z00zz__tvectorz00, BgL_bgl_string1820za700za7za7_1882za7, "pair", 4 );
DEFINE_STRING( BGl_string1821z00zz__tvectorz00, BgL_bgl_string1821za700za7za7_1883za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string1824z00zz__tvectorz00, BgL_bgl_string1824za700za7za7_1884za7, "set", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tvectorzd2refzd2envz00zz__tvectorz00, BgL_bgl_za762tvectorza7d2ref1885z00, BGl_z62tvectorzd2refzb0zz__tvectorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1826z00zz__tvectorz00, BgL_bgl_string1826za700za7za7_1886za7, "tvec", 4 );
DEFINE_STRING( BGl_string1828z00zz__tvectorz00, BgL_bgl_string1828za700za7za7_1887za7, "i", 1 );
DEFINE_STRING( BGl_string1830z00zz__tvectorz00, BgL_bgl_string1830za700za7za7_1888za7, "arg1229", 7 );
DEFINE_STRING( BGl_string1831z00zz__tvectorz00, BgL_bgl_string1831za700za7za7_1889za7, "Unable to convert to such tvector", 33 );
DEFINE_STRING( BGl_string1832z00zz__tvectorz00, BgL_bgl_string1832za700za7za7_1890za7, "Undeclared tvector", 18 );
DEFINE_STRING( BGl_string1833z00zz__tvectorz00, BgL_bgl_string1833za700za7za7_1891za7, "&list->tvector", 14 );
DEFINE_STRING( BGl_string1834z00zz__tvectorz00, BgL_bgl_string1834za700za7za7_1892za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string1835z00zz__tvectorz00, BgL_bgl_string1835za700za7za7_1893za7, "vector->tvector", 15 );
DEFINE_STRING( BGl_string1836z00zz__tvectorz00, BgL_bgl_string1836za700za7za7_1894za7, "vector->tvector:Wrong number of arguments", 41 );
DEFINE_STRING( BGl_string1837z00zz__tvectorz00, BgL_bgl_string1837za700za7za7_1895za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string1840z00zz__tvectorz00, BgL_bgl_string1840za700za7za7_1896za7, "arg1236", 7 );
DEFINE_STRING( BGl_string1841z00zz__tvectorz00, BgL_bgl_string1841za700za7za7_1897za7, "&vector->tvector", 16 );
DEFINE_STRING( BGl_string1842z00zz__tvectorz00, BgL_bgl_string1842za700za7za7_1898za7, "vector", 6 );
DEFINE_STRING( BGl_string1843z00zz__tvectorz00, BgL_bgl_string1843za700za7za7_1899za7, "tvector->vector", 15 );
DEFINE_STRING( BGl_string1846z00zz__tvectorz00, BgL_bgl_string1846za700za7za7_1900za7, "ref", 3 );
DEFINE_STRING( BGl_string1848z00zz__tvectorz00, BgL_bgl_string1848za700za7za7_1901za7, "tv", 2 );
DEFINE_STRING( BGl_string1849z00zz__tvectorz00, BgL_bgl_string1849za700za7za7_1902za7, "vector-set!", 11 );
DEFINE_STRING( BGl_string1850z00zz__tvectorz00, BgL_bgl_string1850za700za7za7_1903za7, "&tvector->vector", 16 );
DEFINE_STRING( BGl_string1851z00zz__tvectorz00, BgL_bgl_string1851za700za7za7_1904za7, "__tvector", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_tvectorzd2idzd2envz00zz__tvectorz00, BgL_bgl_za762tvectorza7d2idza71905za7, BGl_z62tvectorzd2idzb0zz__tvectorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1788z00zz__tvectorz00, BgL_bgl_string1788za700za7za7_1906za7, "/tmp/bigloo/runtime/Llib/tvector.scm", 36 );
DEFINE_STRING( BGl_string1789z00zz__tvectorz00, BgL_bgl_string1789za700za7za7_1907za7, "&tvector-length", 15 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_symbol1839z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1845z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_list1812z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1847z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_list1822z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_list1838z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1794z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_list1844z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1799z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1801z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1813z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1815z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1817z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1823z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1825z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1827z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_symbol1829z00zz__tvectorz00) );
ADD_ROOT( (void *)(&BGl_za2tvectorzd2tableza2zd2zz__tvectorz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long BgL_checksumz00_2112, char * BgL_fromz00_2113)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__tvectorz00))
{ 
BGl_requirezd2initializa7ationz75zz__tvectorz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__tvectorz00(); 
BGl_cnstzd2initzd2zz__tvectorz00(); 
BGl_importedzd2moduleszd2initz00zz__tvectorz00(); 
return 
BGl_toplevelzd2initzd2zz__tvectorz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
BGl_symbol1794z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1795z00zz__tvectorz00); 
BGl_symbol1799z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1800z00zz__tvectorz00); 
BGl_symbol1801z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1802z00zz__tvectorz00); 
BGl_symbol1813z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1814z00zz__tvectorz00); 
BGl_symbol1815z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1816z00zz__tvectorz00); 
BGl_symbol1817z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1818z00zz__tvectorz00); 
BGl_list1812z00zz__tvectorz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1813z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1815z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1815z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1817z00zz__tvectorz00, BNIL)))); 
BGl_symbol1823z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1824z00zz__tvectorz00); 
BGl_symbol1825z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1826z00zz__tvectorz00); 
BGl_symbol1827z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1828z00zz__tvectorz00); 
BGl_symbol1829z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1830z00zz__tvectorz00); 
BGl_list1822z00zz__tvectorz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1813z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1823z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1823z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1825z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1827z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1829z00zz__tvectorz00, BNIL)))))); 
BGl_symbol1839z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1840z00zz__tvectorz00); 
BGl_list1838z00zz__tvectorz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1813z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1823z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1823z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1825z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1827z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1839z00zz__tvectorz00, BNIL)))))); 
BGl_symbol1845z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1846z00zz__tvectorz00); 
BGl_symbol1847z00zz__tvectorz00 = 
bstring_to_symbol(BGl_string1848z00zz__tvectorz00); 
return ( 
BGl_list1844z00zz__tvectorz00 = 
MAKE_YOUNG_PAIR(BGl_symbol1813z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1847z00zz__tvectorz00, 
MAKE_YOUNG_PAIR(BGl_symbol1827z00zz__tvectorz00, BNIL))))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
return ( 
BGl_za2tvectorzd2tableza2zd2zz__tvectorz00 = BNIL, BUNSPEC) ;} 

}



/* tvector? */
BGL_EXPORTED_DEF bool_t BGl_tvectorzf3zf3zz__tvectorz00(obj_t BgL_objz00_3)
{
{ /* Llib/tvector.scm 92 */
return 
TVECTORP(BgL_objz00_3);} 

}



/* &tvector? */
obj_t BGl_z62tvectorzf3z91zz__tvectorz00(obj_t BgL_envz00_1975, obj_t BgL_objz00_1976)
{
{ /* Llib/tvector.scm 92 */
return 
BBOOL(
BGl_tvectorzf3zf3zz__tvectorz00(BgL_objz00_1976));} 

}



/* tvector-length */
BGL_EXPORTED_DEF int BGl_tvectorzd2lengthzd2zz__tvectorz00(obj_t BgL_objz00_4)
{
{ /* Llib/tvector.scm 98 */
return 
TVECTOR_LENGTH(BgL_objz00_4);} 

}



/* &tvector-length */
obj_t BGl_z62tvectorzd2lengthzb0zz__tvectorz00(obj_t BgL_envz00_1977, obj_t BgL_objz00_1978)
{
{ /* Llib/tvector.scm 98 */
{ /* Llib/tvector.scm 99 */
 int BgL_tmpz00_2160;
{ /* Llib/tvector.scm 99 */
 obj_t BgL_auxz00_2161;
if(
TVECTORP(BgL_objz00_1978))
{ /* Llib/tvector.scm 99 */
BgL_auxz00_2161 = BgL_objz00_1978
; }  else 
{ 
 obj_t BgL_auxz00_2164;
BgL_auxz00_2164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(3558L), BGl_string1789z00zz__tvectorz00, BGl_string1790z00zz__tvectorz00, BgL_objz00_1978); 
FAILURE(BgL_auxz00_2164,BFALSE,BFALSE);} 
BgL_tmpz00_2160 = 
BGl_tvectorzd2lengthzd2zz__tvectorz00(BgL_auxz00_2161); } 
return 
BINT(BgL_tmpz00_2160);} } 

}



/* tvector-id */
BGL_EXPORTED_DEF obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t BgL_tvectz00_5)
{
{ /* Llib/tvector.scm 104 */
{ /* Llib/tvector.scm 105 */
 obj_t BgL_arg1154z00_1644;
BgL_arg1154z00_1644 = 
TVECTOR_DESCR(BgL_tvectz00_5); 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1910z00_2171;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2172;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1666z00_1648;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1647;
if(
STRUCTP(BgL_arg1154z00_1644))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1647 = BgL_arg1154z00_1644; }  else 
{ 
 obj_t BgL_auxz00_2175;
BgL_auxz00_2175 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1791z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_arg1154z00_1644); 
FAILURE(BgL_auxz00_2175,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1705z00_2011;
BgL_aux1705z00_2011 = 
STRUCT_KEY(BgL_sz00_1647); 
if(
SYMBOLP(BgL_aux1705z00_2011))
{ /* Llib/tvector.scm 113 */
BgL_res1666z00_1648 = BgL_aux1705z00_2011; }  else 
{ 
 obj_t BgL_auxz00_2182;
BgL_auxz00_2182 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1791z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1705z00_2011); 
FAILURE(BgL_auxz00_2182,BFALSE,BFALSE);} } } 
BgL_tmpz00_2172 = BgL_res1666z00_1648; } 
BgL_test1910z00_2171 = 
(BgL_tmpz00_2172==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1910z00_2171)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2187;
BgL_tmpz00_2187 = 
(int)(0L); 
return 
STRUCT_REF(BgL_arg1154z00_1644, BgL_tmpz00_2187);}  else 
{ /* Llib/tvector.scm 113 */
return 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_arg1154z00_1644);} } } } 

}



/* &tvector-id */
obj_t BGl_z62tvectorzd2idzb0zz__tvectorz00(obj_t BgL_envz00_1979, obj_t BgL_tvectz00_1980)
{
{ /* Llib/tvector.scm 104 */
{ /* Llib/tvector.scm 105 */
 obj_t BgL_auxz00_2191;
if(
TVECTORP(BgL_tvectz00_1980))
{ /* Llib/tvector.scm 105 */
BgL_auxz00_2191 = BgL_tvectz00_1980
; }  else 
{ 
 obj_t BgL_auxz00_2194;
BgL_auxz00_2194 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(3850L), BGl_string1797z00zz__tvectorz00, BGl_string1790z00zz__tvectorz00, BgL_tvectz00_1980); 
FAILURE(BgL_auxz00_2194,BFALSE,BFALSE);} 
return 
BGl_tvectorzd2idzd2zz__tvectorz00(BgL_auxz00_2191);} } 

}



/* get-tvector-descriptor */
BGL_EXPORTED_DEF obj_t get_tvector_descriptor(obj_t BgL_idz00_24)
{
{ /* Llib/tvector.scm 127 */
if(
NULLP(BGl_za2tvectorzd2tableza2zd2zz__tvectorz00))
{ /* Llib/tvector.scm 128 */
return BFALSE;}  else 
{ /* Llib/tvector.scm 129 */
 obj_t BgL_cellz00_1169;
BgL_cellz00_1169 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_24, BGl_za2tvectorzd2tableza2zd2zz__tvectorz00); 
if(
PAIRP(BgL_cellz00_1169))
{ /* Llib/tvector.scm 130 */
return 
CDR(BgL_cellz00_1169);}  else 
{ /* Llib/tvector.scm 130 */
return BFALSE;} } } 

}



/* &get-tvector-descriptor */
obj_t BGl_z62getzd2tvectorzd2descriptorz62zz__tvectorz00(obj_t BgL_envz00_1981, obj_t BgL_idz00_1982)
{
{ /* Llib/tvector.scm 127 */
{ /* Llib/tvector.scm 128 */
 obj_t BgL_auxz00_2205;
if(
SYMBOLP(BgL_idz00_1982))
{ /* Llib/tvector.scm 128 */
BgL_auxz00_2205 = BgL_idz00_1982
; }  else 
{ 
 obj_t BgL_auxz00_2208;
BgL_auxz00_2208 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(5101L), BGl_string1798z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_idz00_1982); 
FAILURE(BgL_auxz00_2208,BFALSE,BFALSE);} 
return 
get_tvector_descriptor(BgL_auxz00_2205);} } 

}



/* declare-tvector! */
BGL_EXPORTED_DEF obj_t BGl_declarezd2tvectorz12zc0zz__tvectorz00(char * BgL_idz00_25, obj_t BgL_allocatez00_26, obj_t BgL_refz00_27, obj_t BgL_setz00_28)
{
{ /* Llib/tvector.scm 141 */
{ /* Llib/tvector.scm 142 */
 obj_t BgL_idz00_1171;
{ /* Llib/tvector.scm 142 */
 obj_t BgL_arg1219z00_1176;
{ /* Llib/tvector.scm 142 */
 obj_t BgL_casezd2valuezd2_1177;
BgL_casezd2valuezd2_1177 = 
BGl_bigloozd2casezd2sensitivityz00zz__readerz00(); 
if(
(BgL_casezd2valuezd2_1177==BGl_symbol1799z00zz__tvectorz00))
{ /* Llib/tvector.scm 142 */
BgL_arg1219z00_1176 = 
BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(
string_to_bstring(BgL_idz00_25)); }  else 
{ /* Llib/tvector.scm 142 */
if(
(BgL_casezd2valuezd2_1177==BGl_symbol1801z00zz__tvectorz00))
{ /* Llib/tvector.scm 142 */
BgL_arg1219z00_1176 = 
BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(
string_to_bstring(BgL_idz00_25)); }  else 
{ /* Llib/tvector.scm 142 */
BgL_arg1219z00_1176 = 
string_to_bstring(BgL_idz00_25); } } } 
BgL_idz00_1171 = 
bstring_to_symbol(BgL_arg1219z00_1176); } 
{ /* Llib/tvector.scm 142 */
 obj_t BgL_oldz00_1172;
BgL_oldz00_1172 = 
get_tvector_descriptor(BgL_idz00_1171); 
{ /* Llib/tvector.scm 149 */

{ /* Llib/tvector.scm 150 */
 bool_t BgL_test1919z00_2225;
if(
STRUCTP(BgL_oldz00_1172))
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2228;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1680z00_1710;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1711z00_2017;
BgL_aux1711z00_2017 = 
STRUCT_KEY(BgL_oldz00_1172); 
if(
SYMBOLP(BgL_aux1711z00_2017))
{ /* Llib/tvector.scm 113 */
BgL_res1680z00_1710 = BgL_aux1711z00_2017; }  else 
{ 
 obj_t BgL_auxz00_2232;
BgL_auxz00_2232 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1803z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1711z00_2017); 
FAILURE(BgL_auxz00_2232,BFALSE,BFALSE);} } 
BgL_tmpz00_2228 = BgL_res1680z00_1710; } 
BgL_test1919z00_2225 = 
(BgL_tmpz00_2228==BGl_symbol1794z00zz__tvectorz00); }  else 
{ /* Llib/tvector.scm 113 */
BgL_test1919z00_2225 = ((bool_t)0)
; } 
if(BgL_test1919z00_2225)
{ /* Llib/tvector.scm 150 */
return BgL_oldz00_1172;}  else 
{ /* Llib/tvector.scm 151 */
 obj_t BgL_newz00_1174;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_newz00_1711;
BgL_newz00_1711 = 
create_struct(BGl_symbol1794z00zz__tvectorz00, 
(int)(4L)); 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1922z00_2239;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2240;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1681z00_1715;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1713z00_2019;
BgL_aux1713z00_2019 = 
STRUCT_KEY(BgL_newz00_1711); 
if(
SYMBOLP(BgL_aux1713z00_2019))
{ /* Llib/tvector.scm 113 */
BgL_res1681z00_1715 = BgL_aux1713z00_2019; }  else 
{ 
 obj_t BgL_auxz00_2244;
BgL_auxz00_2244 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1803z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1713z00_2019); 
FAILURE(BgL_auxz00_2244,BFALSE,BFALSE);} } 
BgL_tmpz00_2240 = BgL_res1681z00_1715; } 
BgL_test1922z00_2239 = 
(BgL_tmpz00_2240==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1922z00_2239)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2249;
BgL_tmpz00_2249 = 
(int)(3L); 
STRUCT_SET(BgL_newz00_1711, BgL_tmpz00_2249, BgL_setz00_28); }  else 
{ /* Llib/tvector.scm 113 */
BGl_errorz00zz__errorz00(BGl_string1804z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_newz00_1711); } } 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1924z00_2253;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2254;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1682z00_1719;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1715z00_2021;
BgL_aux1715z00_2021 = 
STRUCT_KEY(BgL_newz00_1711); 
if(
SYMBOLP(BgL_aux1715z00_2021))
{ /* Llib/tvector.scm 113 */
BgL_res1682z00_1719 = BgL_aux1715z00_2021; }  else 
{ 
 obj_t BgL_auxz00_2258;
BgL_auxz00_2258 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1803z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1715z00_2021); 
FAILURE(BgL_auxz00_2258,BFALSE,BFALSE);} } 
BgL_tmpz00_2254 = BgL_res1682z00_1719; } 
BgL_test1924z00_2253 = 
(BgL_tmpz00_2254==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1924z00_2253)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2263;
BgL_tmpz00_2263 = 
(int)(2L); 
STRUCT_SET(BgL_newz00_1711, BgL_tmpz00_2263, BgL_refz00_27); }  else 
{ /* Llib/tvector.scm 113 */
BGl_errorz00zz__errorz00(BGl_string1804z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_newz00_1711); } } 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1926z00_2267;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2268;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1683z00_1723;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1717z00_2023;
BgL_aux1717z00_2023 = 
STRUCT_KEY(BgL_newz00_1711); 
if(
SYMBOLP(BgL_aux1717z00_2023))
{ /* Llib/tvector.scm 113 */
BgL_res1683z00_1723 = BgL_aux1717z00_2023; }  else 
{ 
 obj_t BgL_auxz00_2272;
BgL_auxz00_2272 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1803z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1717z00_2023); 
FAILURE(BgL_auxz00_2272,BFALSE,BFALSE);} } 
BgL_tmpz00_2268 = BgL_res1683z00_1723; } 
BgL_test1926z00_2267 = 
(BgL_tmpz00_2268==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1926z00_2267)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2277;
BgL_tmpz00_2277 = 
(int)(1L); 
STRUCT_SET(BgL_newz00_1711, BgL_tmpz00_2277, BgL_allocatez00_26); }  else 
{ /* Llib/tvector.scm 113 */
BGl_errorz00zz__errorz00(BGl_string1804z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_newz00_1711); } } 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1928z00_2281;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2282;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1684z00_1727;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1719z00_2025;
BgL_aux1719z00_2025 = 
STRUCT_KEY(BgL_newz00_1711); 
if(
SYMBOLP(BgL_aux1719z00_2025))
{ /* Llib/tvector.scm 113 */
BgL_res1684z00_1727 = BgL_aux1719z00_2025; }  else 
{ 
 obj_t BgL_auxz00_2286;
BgL_auxz00_2286 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1803z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1719z00_2025); 
FAILURE(BgL_auxz00_2286,BFALSE,BFALSE);} } 
BgL_tmpz00_2282 = BgL_res1684z00_1727; } 
BgL_test1928z00_2281 = 
(BgL_tmpz00_2282==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1928z00_2281)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2291;
BgL_tmpz00_2291 = 
(int)(0L); 
STRUCT_SET(BgL_newz00_1711, BgL_tmpz00_2291, BgL_idz00_1171); }  else 
{ /* Llib/tvector.scm 113 */
BGl_errorz00zz__errorz00(BGl_string1804z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_newz00_1711); } } 
BgL_newz00_1174 = BgL_newz00_1711; } 
{ /* Llib/tvector.scm 152 */
 obj_t BgL_arg1218z00_1175;
BgL_arg1218z00_1175 = 
MAKE_YOUNG_PAIR(BgL_idz00_1171, BgL_newz00_1174); 
BGl_za2tvectorzd2tableza2zd2zz__tvectorz00 = 
MAKE_YOUNG_PAIR(BgL_arg1218z00_1175, BGl_za2tvectorzd2tableza2zd2zz__tvectorz00); } 
return BgL_newz00_1174;} } } } } } 

}



/* &declare-tvector! */
obj_t BGl_z62declarezd2tvectorz12za2zz__tvectorz00(obj_t BgL_envz00_1983, obj_t BgL_idz00_1984, obj_t BgL_allocatez00_1985, obj_t BgL_refz00_1986, obj_t BgL_setz00_1987)
{
{ /* Llib/tvector.scm 141 */
{ /* Llib/tvector.scm 142 */
 obj_t BgL_auxz00_2306; char * BgL_auxz00_2297;
if(
PROCEDUREP(BgL_allocatez00_1985))
{ /* Llib/tvector.scm 142 */
BgL_auxz00_2306 = BgL_allocatez00_1985
; }  else 
{ 
 obj_t BgL_auxz00_2309;
BgL_auxz00_2309 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(5790L), BGl_string1805z00zz__tvectorz00, BGl_string1807z00zz__tvectorz00, BgL_allocatez00_1985); 
FAILURE(BgL_auxz00_2309,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 142 */
 obj_t BgL_tmpz00_2298;
if(
STRINGP(BgL_idz00_1984))
{ /* Llib/tvector.scm 142 */
BgL_tmpz00_2298 = BgL_idz00_1984
; }  else 
{ 
 obj_t BgL_auxz00_2301;
BgL_auxz00_2301 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(5790L), BGl_string1805z00zz__tvectorz00, BGl_string1806z00zz__tvectorz00, BgL_idz00_1984); 
FAILURE(BgL_auxz00_2301,BFALSE,BFALSE);} 
BgL_auxz00_2297 = 
BSTRING_TO_STRING(BgL_tmpz00_2298); } 
return 
BGl_declarezd2tvectorz12zc0zz__tvectorz00(BgL_auxz00_2297, BgL_auxz00_2306, BgL_refz00_1986, BgL_setz00_1987);} } 

}



/* tvector-ref */
BGL_EXPORTED_DEF obj_t BGl_tvectorzd2refzd2zz__tvectorz00(obj_t BgL_tvectorz00_29)
{
{ /* Llib/tvector.scm 159 */
{ /* Llib/tvector.scm 160 */
 obj_t BgL_arg1225z00_1728;
BgL_arg1225z00_1728 = 
TVECTOR_DESCR(BgL_tvectorz00_29); 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1932z00_2315;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2316;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1685z00_1732;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1731;
if(
STRUCTP(BgL_arg1225z00_1728))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1731 = BgL_arg1225z00_1728; }  else 
{ 
 obj_t BgL_auxz00_2319;
BgL_auxz00_2319 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1808z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_arg1225z00_1728); 
FAILURE(BgL_auxz00_2319,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1727z00_2033;
BgL_aux1727z00_2033 = 
STRUCT_KEY(BgL_sz00_1731); 
if(
SYMBOLP(BgL_aux1727z00_2033))
{ /* Llib/tvector.scm 113 */
BgL_res1685z00_1732 = BgL_aux1727z00_2033; }  else 
{ 
 obj_t BgL_auxz00_2326;
BgL_auxz00_2326 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1808z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1727z00_2033); 
FAILURE(BgL_auxz00_2326,BFALSE,BFALSE);} } } 
BgL_tmpz00_2316 = BgL_res1685z00_1732; } 
BgL_test1932z00_2315 = 
(BgL_tmpz00_2316==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1932z00_2315)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2331;
BgL_tmpz00_2331 = 
(int)(2L); 
return 
STRUCT_REF(BgL_arg1225z00_1728, BgL_tmpz00_2331);}  else 
{ /* Llib/tvector.scm 113 */
return 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_arg1225z00_1728);} } } } 

}



/* &tvector-ref */
obj_t BGl_z62tvectorzd2refzb0zz__tvectorz00(obj_t BgL_envz00_1988, obj_t BgL_tvectorz00_1989)
{
{ /* Llib/tvector.scm 159 */
{ /* Llib/tvector.scm 160 */
 obj_t BgL_auxz00_2335;
if(
TVECTORP(BgL_tvectorz00_1989))
{ /* Llib/tvector.scm 160 */
BgL_auxz00_2335 = BgL_tvectorz00_1989
; }  else 
{ 
 obj_t BgL_auxz00_2338;
BgL_auxz00_2338 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(6511L), BGl_string1809z00zz__tvectorz00, BGl_string1790z00zz__tvectorz00, BgL_tvectorz00_1989); 
FAILURE(BgL_auxz00_2338,BFALSE,BFALSE);} 
return 
BGl_tvectorzd2refzd2zz__tvectorz00(BgL_auxz00_2335);} } 

}



/* list->tvector */
BGL_EXPORTED_DEF obj_t BGl_listzd2ze3tvectorz31zz__tvectorz00(obj_t BgL_idz00_30, obj_t BgL_lz00_31)
{
{ /* Llib/tvector.scm 165 */
{ /* Llib/tvector.scm 166 */
 obj_t BgL_descrz00_1183;
BgL_descrz00_1183 = 
get_tvector_descriptor(BgL_idz00_30); 
if(
CBOOL(BgL_descrz00_1183))
{ /* Llib/tvector.scm 169 */
 obj_t BgL_allocatez00_1184; obj_t BgL_setz00_1185;
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1937z00_2346;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2347;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1686z00_1736;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1735;
if(
STRUCTP(BgL_descrz00_1183))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1735 = BgL_descrz00_1183; }  else 
{ 
 obj_t BgL_auxz00_2350;
BgL_auxz00_2350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1810z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_descrz00_1183); 
FAILURE(BgL_auxz00_2350,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1733z00_2039;
BgL_aux1733z00_2039 = 
STRUCT_KEY(BgL_sz00_1735); 
if(
SYMBOLP(BgL_aux1733z00_2039))
{ /* Llib/tvector.scm 113 */
BgL_res1686z00_1736 = BgL_aux1733z00_2039; }  else 
{ 
 obj_t BgL_auxz00_2357;
BgL_auxz00_2357 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1810z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1733z00_2039); 
FAILURE(BgL_auxz00_2357,BFALSE,BFALSE);} } } 
BgL_tmpz00_2347 = BgL_res1686z00_1736; } 
BgL_test1937z00_2346 = 
(BgL_tmpz00_2347==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1937z00_2346)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2362;
BgL_tmpz00_2362 = 
(int)(1L); 
BgL_allocatez00_1184 = 
STRUCT_REF(BgL_descrz00_1183, BgL_tmpz00_2362); }  else 
{ /* Llib/tvector.scm 113 */
BgL_allocatez00_1184 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_descrz00_1183); } } 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1940z00_2366;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2367;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1687z00_1740;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1739;
if(
STRUCTP(BgL_descrz00_1183))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1739 = BgL_descrz00_1183; }  else 
{ 
 obj_t BgL_auxz00_2370;
BgL_auxz00_2370 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1810z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_descrz00_1183); 
FAILURE(BgL_auxz00_2370,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1737z00_2043;
BgL_aux1737z00_2043 = 
STRUCT_KEY(BgL_sz00_1739); 
if(
SYMBOLP(BgL_aux1737z00_2043))
{ /* Llib/tvector.scm 113 */
BgL_res1687z00_1740 = BgL_aux1737z00_2043; }  else 
{ 
 obj_t BgL_auxz00_2377;
BgL_auxz00_2377 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1810z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1737z00_2043); 
FAILURE(BgL_auxz00_2377,BFALSE,BFALSE);} } } 
BgL_tmpz00_2367 = BgL_res1687z00_1740; } 
BgL_test1940z00_2366 = 
(BgL_tmpz00_2367==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1940z00_2366)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2382;
BgL_tmpz00_2382 = 
(int)(3L); 
BgL_setz00_1185 = 
STRUCT_REF(BgL_descrz00_1183, BgL_tmpz00_2382); }  else 
{ /* Llib/tvector.scm 113 */
BgL_setz00_1185 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_descrz00_1183); } } 
if(
PROCEDUREP(BgL_setz00_1185))
{ /* Llib/tvector.scm 175 */
 long BgL_lenz00_1187;
BgL_lenz00_1187 = 
bgl_list_length(BgL_lz00_31); 
{ /* Llib/tvector.scm 175 */
 obj_t BgL_tvecz00_1188;
{ /* Llib/tvector.scm 176 */
 obj_t BgL_funz00_2047;
if(
PROCEDUREP(BgL_allocatez00_1184))
{ /* Llib/tvector.scm 176 */
BgL_funz00_2047 = BgL_allocatez00_1184; }  else 
{ 
 obj_t BgL_auxz00_2391;
BgL_auxz00_2391 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(7157L), BGl_string1810z00zz__tvectorz00, BGl_string1807z00zz__tvectorz00, BgL_allocatez00_1184); 
FAILURE(BgL_auxz00_2391,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_2047, 1))
{ /* Llib/tvector.scm 176 */
BgL_tvecz00_1188 = 
(VA_PROCEDUREP( BgL_funz00_2047 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_2047))(BgL_allocatez00_1184, 
BINT(BgL_lenz00_1187), BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_2047))(BgL_allocatez00_1184, 
BINT(BgL_lenz00_1187)) ); }  else 
{ /* Llib/tvector.scm 176 */
FAILURE(BGl_string1811z00zz__tvectorz00,BGl_list1812z00zz__tvectorz00,BgL_funz00_2047);} } 
{ /* Llib/tvector.scm 176 */

{ 
 obj_t BgL_lz00_1745; long BgL_iz00_1746;
BgL_lz00_1745 = BgL_lz00_31; 
BgL_iz00_1746 = 0L; 
BgL_loopz00_1744:
if(
NULLP(BgL_lz00_1745))
{ /* Llib/tvector.scm 179 */
return BgL_tvecz00_1188;}  else 
{ /* Llib/tvector.scm 179 */
{ /* Llib/tvector.scm 182 */
 obj_t BgL_arg1229z00_1748;
{ /* Llib/tvector.scm 182 */
 obj_t BgL_pairz00_1749;
if(
PAIRP(BgL_lz00_1745))
{ /* Llib/tvector.scm 182 */
BgL_pairz00_1749 = BgL_lz00_1745; }  else 
{ 
 obj_t BgL_auxz00_2407;
BgL_auxz00_2407 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(7287L), BGl_string1819z00zz__tvectorz00, BGl_string1820z00zz__tvectorz00, BgL_lz00_1745); 
FAILURE(BgL_auxz00_2407,BFALSE,BFALSE);} 
BgL_arg1229z00_1748 = 
CAR(BgL_pairz00_1749); } 
if(
PROCEDURE_CORRECT_ARITYP(
((obj_t)BgL_setz00_1185), 3))
{ /* Llib/tvector.scm 182 */
 obj_t BgL_tmpfunz00_2421;
BgL_tmpfunz00_2421 = 
((obj_t)BgL_setz00_1185); 
(VA_PROCEDUREP( BgL_tmpfunz00_2421 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_tmpfunz00_2421))(BgL_setz00_1185, BgL_tvecz00_1188, 
BINT(BgL_iz00_1746), BgL_arg1229z00_1748, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_tmpfunz00_2421))(BgL_setz00_1185, BgL_tvecz00_1188, 
BINT(BgL_iz00_1746), BgL_arg1229z00_1748) ); }  else 
{ 
 obj_t BgL_auxz00_2423;
BgL_auxz00_2423 = 
((obj_t)BgL_setz00_1185); 
FAILURE(BGl_string1821z00zz__tvectorz00,BGl_list1822z00zz__tvectorz00,BgL_auxz00_2423);} } 
{ /* Llib/tvector.scm 183 */
 obj_t BgL_arg1230z00_1750; long BgL_arg1231z00_1751;
{ /* Llib/tvector.scm 183 */
 obj_t BgL_pairz00_1752;
if(
PAIRP(BgL_lz00_1745))
{ /* Llib/tvector.scm 183 */
BgL_pairz00_1752 = BgL_lz00_1745; }  else 
{ 
 obj_t BgL_auxz00_2428;
BgL_auxz00_2428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(7311L), BGl_string1819z00zz__tvectorz00, BGl_string1820z00zz__tvectorz00, BgL_lz00_1745); 
FAILURE(BgL_auxz00_2428,BFALSE,BFALSE);} 
BgL_arg1230z00_1750 = 
CDR(BgL_pairz00_1752); } 
BgL_arg1231z00_1751 = 
(BgL_iz00_1746+1L); 
{ 
 long BgL_iz00_2435; obj_t BgL_lz00_2434;
BgL_lz00_2434 = BgL_arg1230z00_1750; 
BgL_iz00_2435 = BgL_arg1231z00_1751; 
BgL_iz00_1746 = BgL_iz00_2435; 
BgL_lz00_1745 = BgL_lz00_2434; 
goto BgL_loopz00_1744;} } } } } } }  else 
{ /* Llib/tvector.scm 171 */
return 
BGl_errorz00zz__errorz00(BGl_string1810z00zz__tvectorz00, BGl_string1831z00zz__tvectorz00, BgL_idz00_30);} }  else 
{ /* Llib/tvector.scm 167 */
return 
BGl_errorz00zz__errorz00(BGl_string1810z00zz__tvectorz00, BGl_string1832z00zz__tvectorz00, BgL_idz00_30);} } } 

}



/* &list->tvector */
obj_t BGl_z62listzd2ze3tvectorz53zz__tvectorz00(obj_t BgL_envz00_1990, obj_t BgL_idz00_1991, obj_t BgL_lz00_1992)
{
{ /* Llib/tvector.scm 165 */
{ /* Llib/tvector.scm 166 */
 obj_t BgL_auxz00_2445; obj_t BgL_auxz00_2438;
if(
BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_lz00_1992))
{ /* Llib/tvector.scm 166 */
BgL_auxz00_2445 = BgL_lz00_1992
; }  else 
{ 
 obj_t BgL_auxz00_2448;
BgL_auxz00_2448 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(6812L), BGl_string1833z00zz__tvectorz00, BGl_string1834z00zz__tvectorz00, BgL_lz00_1992); 
FAILURE(BgL_auxz00_2448,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_idz00_1991))
{ /* Llib/tvector.scm 166 */
BgL_auxz00_2438 = BgL_idz00_1991
; }  else 
{ 
 obj_t BgL_auxz00_2441;
BgL_auxz00_2441 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(6812L), BGl_string1833z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_idz00_1991); 
FAILURE(BgL_auxz00_2441,BFALSE,BFALSE);} 
return 
BGl_listzd2ze3tvectorz31zz__tvectorz00(BgL_auxz00_2438, BgL_auxz00_2445);} } 

}



/* vector->tvector */
BGL_EXPORTED_DEF obj_t BGl_vectorzd2ze3tvectorz31zz__tvectorz00(obj_t BgL_idz00_32, obj_t BgL_vz00_33)
{
{ /* Llib/tvector.scm 188 */
{ /* Llib/tvector.scm 189 */
 obj_t BgL_descrz00_1198;
BgL_descrz00_1198 = 
get_tvector_descriptor(BgL_idz00_32); 
if(
CBOOL(BgL_descrz00_1198))
{ /* Llib/tvector.scm 192 */
 obj_t BgL_allocatez00_1199; obj_t BgL_setz00_1200;
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1953z00_2456;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2457;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1688z00_1757;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1756;
if(
STRUCTP(BgL_descrz00_1198))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1756 = BgL_descrz00_1198; }  else 
{ 
 obj_t BgL_auxz00_2460;
BgL_auxz00_2460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1835z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_descrz00_1198); 
FAILURE(BgL_auxz00_2460,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1755z00_2063;
BgL_aux1755z00_2063 = 
STRUCT_KEY(BgL_sz00_1756); 
if(
SYMBOLP(BgL_aux1755z00_2063))
{ /* Llib/tvector.scm 113 */
BgL_res1688z00_1757 = BgL_aux1755z00_2063; }  else 
{ 
 obj_t BgL_auxz00_2467;
BgL_auxz00_2467 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1835z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1755z00_2063); 
FAILURE(BgL_auxz00_2467,BFALSE,BFALSE);} } } 
BgL_tmpz00_2457 = BgL_res1688z00_1757; } 
BgL_test1953z00_2456 = 
(BgL_tmpz00_2457==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1953z00_2456)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2472;
BgL_tmpz00_2472 = 
(int)(1L); 
BgL_allocatez00_1199 = 
STRUCT_REF(BgL_descrz00_1198, BgL_tmpz00_2472); }  else 
{ /* Llib/tvector.scm 113 */
BgL_allocatez00_1199 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_descrz00_1198); } } 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1956z00_2476;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2477;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1689z00_1761;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1760;
if(
STRUCTP(BgL_descrz00_1198))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1760 = BgL_descrz00_1198; }  else 
{ 
 obj_t BgL_auxz00_2480;
BgL_auxz00_2480 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1835z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_descrz00_1198); 
FAILURE(BgL_auxz00_2480,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1759z00_2067;
BgL_aux1759z00_2067 = 
STRUCT_KEY(BgL_sz00_1760); 
if(
SYMBOLP(BgL_aux1759z00_2067))
{ /* Llib/tvector.scm 113 */
BgL_res1689z00_1761 = BgL_aux1759z00_2067; }  else 
{ 
 obj_t BgL_auxz00_2487;
BgL_auxz00_2487 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1835z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1759z00_2067); 
FAILURE(BgL_auxz00_2487,BFALSE,BFALSE);} } } 
BgL_tmpz00_2477 = BgL_res1689z00_1761; } 
BgL_test1956z00_2476 = 
(BgL_tmpz00_2477==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1956z00_2476)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2492;
BgL_tmpz00_2492 = 
(int)(3L); 
BgL_setz00_1200 = 
STRUCT_REF(BgL_descrz00_1198, BgL_tmpz00_2492); }  else 
{ /* Llib/tvector.scm 113 */
BgL_setz00_1200 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_descrz00_1198); } } 
if(
PROCEDUREP(BgL_setz00_1200))
{ /* Llib/tvector.scm 198 */
 obj_t BgL_tvecz00_1203;
{ /* Llib/tvector.scm 199 */
 obj_t BgL_funz00_2071;
if(
PROCEDUREP(BgL_allocatez00_1199))
{ /* Llib/tvector.scm 199 */
BgL_funz00_2071 = BgL_allocatez00_1199; }  else 
{ 
 obj_t BgL_auxz00_2500;
BgL_auxz00_2500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(7963L), BGl_string1835z00zz__tvectorz00, BGl_string1807z00zz__tvectorz00, BgL_allocatez00_1199); 
FAILURE(BgL_auxz00_2500,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_2071, 1))
{ /* Llib/tvector.scm 199 */
BgL_tvecz00_1203 = 
(VA_PROCEDUREP( BgL_funz00_2071 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_2071))(BgL_allocatez00_1199, 
BINT(
VECTOR_LENGTH(BgL_vz00_33)), BEOA) : ((obj_t (*)(obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_2071))(BgL_allocatez00_1199, 
BINT(
VECTOR_LENGTH(BgL_vz00_33))) ); }  else 
{ /* Llib/tvector.scm 199 */
FAILURE(BGl_string1836z00zz__tvectorz00,BGl_list1812z00zz__tvectorz00,BgL_funz00_2071);} } 
{ /* Llib/tvector.scm 199 */

{ /* Llib/tvector.scm 200 */
 long BgL_g1040z00_1204;
BgL_g1040z00_1204 = 
(
VECTOR_LENGTH(BgL_vz00_33)-1L); 
{ 
 long BgL_iz00_1206;
BgL_iz00_1206 = BgL_g1040z00_1204; 
BgL_zc3z04anonymousza31233ze3z87_1207:
if(
(BgL_iz00_1206==-1L))
{ /* Llib/tvector.scm 201 */
return BgL_tvecz00_1203;}  else 
{ /* Llib/tvector.scm 201 */
{ /* Llib/tvector.scm 204 */
 obj_t BgL_arg1236z00_1209;
{ /* Llib/tvector.scm 204 */
 bool_t BgL_test1963z00_2517;
{ /* Llib/tvector.scm 204 */
 long BgL_tmpz00_2518;
BgL_tmpz00_2518 = 
VECTOR_LENGTH(BgL_vz00_33); 
BgL_test1963z00_2517 = 
BOUND_CHECK(BgL_iz00_1206, BgL_tmpz00_2518); } 
if(BgL_test1963z00_2517)
{ /* Llib/tvector.scm 204 */
BgL_arg1236z00_1209 = 
VECTOR_REF(BgL_vz00_33,BgL_iz00_1206); }  else 
{ 
 obj_t BgL_auxz00_2522;
BgL_auxz00_2522 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(8083L), BGl_string1837z00zz__tvectorz00, BgL_vz00_33, 
(int)(
VECTOR_LENGTH(BgL_vz00_33)), 
(int)(BgL_iz00_1206)); 
FAILURE(BgL_auxz00_2522,BFALSE,BFALSE);} } 
if(
PROCEDURE_CORRECT_ARITYP(
((obj_t)BgL_setz00_1200), 3))
{ /* Llib/tvector.scm 204 */
 obj_t BgL_tmpfunz00_2538;
BgL_tmpfunz00_2538 = 
((obj_t)BgL_setz00_1200); 
(VA_PROCEDUREP( BgL_tmpfunz00_2538 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_tmpfunz00_2538))(BgL_setz00_1200, BgL_tvecz00_1203, 
BINT(BgL_iz00_1206), BgL_arg1236z00_1209, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_tmpfunz00_2538))(BgL_setz00_1200, BgL_tvecz00_1203, 
BINT(BgL_iz00_1206), BgL_arg1236z00_1209) ); }  else 
{ 
 obj_t BgL_auxz00_2540;
BgL_auxz00_2540 = 
((obj_t)BgL_setz00_1200); 
FAILURE(BGl_string1821z00zz__tvectorz00,BGl_list1838z00zz__tvectorz00,BgL_auxz00_2540);} } 
{ 
 long BgL_iz00_2543;
BgL_iz00_2543 = 
(BgL_iz00_1206-1L); 
BgL_iz00_1206 = BgL_iz00_2543; 
goto BgL_zc3z04anonymousza31233ze3z87_1207;} } } } } }  else 
{ /* Llib/tvector.scm 194 */
return 
BGl_errorz00zz__errorz00(BGl_string1835z00zz__tvectorz00, BGl_string1831z00zz__tvectorz00, BgL_idz00_32);} }  else 
{ /* Llib/tvector.scm 190 */
return 
BGl_errorz00zz__errorz00(BGl_string1835z00zz__tvectorz00, BGl_string1832z00zz__tvectorz00, BgL_idz00_32);} } } 

}



/* &vector->tvector */
obj_t BGl_z62vectorzd2ze3tvectorz53zz__tvectorz00(obj_t BgL_envz00_1993, obj_t BgL_idz00_1994, obj_t BgL_vz00_1995)
{
{ /* Llib/tvector.scm 188 */
{ /* Llib/tvector.scm 189 */
 obj_t BgL_auxz00_2554; obj_t BgL_auxz00_2547;
if(
VECTORP(BgL_vz00_1995))
{ /* Llib/tvector.scm 189 */
BgL_auxz00_2554 = BgL_vz00_1995
; }  else 
{ 
 obj_t BgL_auxz00_2557;
BgL_auxz00_2557 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(7607L), BGl_string1841z00zz__tvectorz00, BGl_string1842z00zz__tvectorz00, BgL_vz00_1995); 
FAILURE(BgL_auxz00_2557,BFALSE,BFALSE);} 
if(
SYMBOLP(BgL_idz00_1994))
{ /* Llib/tvector.scm 189 */
BgL_auxz00_2547 = BgL_idz00_1994
; }  else 
{ 
 obj_t BgL_auxz00_2550;
BgL_auxz00_2550 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(7607L), BGl_string1841z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_idz00_1994); 
FAILURE(BgL_auxz00_2550,BFALSE,BFALSE);} 
return 
BGl_vectorzd2ze3tvectorz31zz__tvectorz00(BgL_auxz00_2547, BgL_auxz00_2554);} } 

}



/* tvector->vector */
BGL_EXPORTED_DEF obj_t BGl_tvectorzd2ze3vectorz31zz__tvectorz00(obj_t BgL_tvz00_34)
{
{ /* Llib/tvector.scm 210 */
{ /* Llib/tvector.scm 211 */
 obj_t BgL_descrz00_1212;
BgL_descrz00_1212 = 
TVECTOR_DESCR(BgL_tvz00_34); 
{ /* Llib/tvector.scm 212 */
 obj_t BgL_allocatez00_1213; obj_t BgL_refz00_1214;
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1967z00_2563;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2564;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1690z00_1771;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1770;
if(
STRUCTP(BgL_descrz00_1212))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1770 = BgL_descrz00_1212; }  else 
{ 
 obj_t BgL_auxz00_2567;
BgL_auxz00_2567 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1843z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_descrz00_1212); 
FAILURE(BgL_auxz00_2567,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1773z00_2083;
BgL_aux1773z00_2083 = 
STRUCT_KEY(BgL_sz00_1770); 
if(
SYMBOLP(BgL_aux1773z00_2083))
{ /* Llib/tvector.scm 113 */
BgL_res1690z00_1771 = BgL_aux1773z00_2083; }  else 
{ 
 obj_t BgL_auxz00_2574;
BgL_auxz00_2574 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1843z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1773z00_2083); 
FAILURE(BgL_auxz00_2574,BFALSE,BFALSE);} } } 
BgL_tmpz00_2564 = BgL_res1690z00_1771; } 
BgL_test1967z00_2563 = 
(BgL_tmpz00_2564==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1967z00_2563)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2579;
BgL_tmpz00_2579 = 
(int)(1L); 
BgL_allocatez00_1213 = 
STRUCT_REF(BgL_descrz00_1212, BgL_tmpz00_2579); }  else 
{ /* Llib/tvector.scm 113 */
BgL_allocatez00_1213 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_descrz00_1212); } } 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1970z00_2583;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2584;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1691z00_1775;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1774;
if(
STRUCTP(BgL_descrz00_1212))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1774 = BgL_descrz00_1212; }  else 
{ 
 obj_t BgL_auxz00_2587;
BgL_auxz00_2587 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1843z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_descrz00_1212); 
FAILURE(BgL_auxz00_2587,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1777z00_2087;
BgL_aux1777z00_2087 = 
STRUCT_KEY(BgL_sz00_1774); 
if(
SYMBOLP(BgL_aux1777z00_2087))
{ /* Llib/tvector.scm 113 */
BgL_res1691z00_1775 = BgL_aux1777z00_2087; }  else 
{ 
 obj_t BgL_auxz00_2594;
BgL_auxz00_2594 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1843z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1777z00_2087); 
FAILURE(BgL_auxz00_2594,BFALSE,BFALSE);} } } 
BgL_tmpz00_2584 = BgL_res1691z00_1775; } 
BgL_test1970z00_2583 = 
(BgL_tmpz00_2584==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1970z00_2583)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2599;
BgL_tmpz00_2599 = 
(int)(2L); 
BgL_refz00_1214 = 
STRUCT_REF(BgL_descrz00_1212, BgL_tmpz00_2599); }  else 
{ /* Llib/tvector.scm 113 */
BgL_refz00_1214 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_descrz00_1212); } } 
if(
PROCEDUREP(BgL_refz00_1214))
{ /* Llib/tvector.scm 218 */
 int BgL_lenz00_1216;
BgL_lenz00_1216 = 
TVECTOR_LENGTH(BgL_tvz00_34); 
{ /* Llib/tvector.scm 218 */
 obj_t BgL_vecz00_1217;
BgL_vecz00_1217 = 
create_vector(
(long)(BgL_lenz00_1216)); 
{ /* Llib/tvector.scm 219 */

{ /* Llib/tvector.scm 220 */
 long BgL_g1041z00_1218;
BgL_g1041z00_1218 = 
(
(long)(BgL_lenz00_1216)-1L); 
{ 
 long BgL_iz00_1220;
BgL_iz00_1220 = BgL_g1041z00_1218; 
BgL_zc3z04anonymousza31240ze3z87_1221:
if(
(BgL_iz00_1220==-1L))
{ /* Llib/tvector.scm 221 */
return BgL_vecz00_1217;}  else 
{ /* Llib/tvector.scm 221 */
{ /* Llib/tvector.scm 224 */
 obj_t BgL_arg1242z00_1223;
if(
PROCEDURE_CORRECT_ARITYP(
((obj_t)BgL_refz00_1214), 2))
{ /* Llib/tvector.scm 224 */
 obj_t BgL_tmpfunz00_2620;
BgL_tmpfunz00_2620 = 
((obj_t)BgL_refz00_1214); 
BgL_arg1242z00_1223 = 
(VA_PROCEDUREP( BgL_tmpfunz00_2620 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_tmpfunz00_2620))(BgL_refz00_1214, BgL_tvz00_34, 
BINT(BgL_iz00_1220), BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_tmpfunz00_2620))(BgL_refz00_1214, BgL_tvz00_34, 
BINT(BgL_iz00_1220)) ); }  else 
{ 
 obj_t BgL_auxz00_2622;
BgL_auxz00_2622 = 
((obj_t)BgL_refz00_1214); 
FAILURE(BGl_string1821z00zz__tvectorz00,BGl_list1844z00zz__tvectorz00,BgL_auxz00_2622);} 
{ /* Llib/tvector.scm 224 */
 bool_t BgL_test1976z00_2625;
{ /* Llib/tvector.scm 224 */
 long BgL_tmpz00_2626;
BgL_tmpz00_2626 = 
VECTOR_LENGTH(BgL_vecz00_1217); 
BgL_test1976z00_2625 = 
BOUND_CHECK(BgL_iz00_1220, BgL_tmpz00_2626); } 
if(BgL_test1976z00_2625)
{ /* Llib/tvector.scm 224 */
VECTOR_SET(BgL_vecz00_1217,BgL_iz00_1220,BgL_arg1242z00_1223); }  else 
{ 
 obj_t BgL_auxz00_2630;
BgL_auxz00_2630 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(8812L), BGl_string1849z00zz__tvectorz00, BgL_vecz00_1217, 
(int)(
VECTOR_LENGTH(BgL_vecz00_1217)), 
(int)(BgL_iz00_1220)); 
FAILURE(BgL_auxz00_2630,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_2637;
BgL_iz00_2637 = 
(BgL_iz00_1220-1L); 
BgL_iz00_1220 = BgL_iz00_2637; 
goto BgL_zc3z04anonymousza31240ze3z87_1221;} } } } } } }  else 
{ /* Llib/tvector.scm 217 */
 obj_t BgL_arg1248z00_1226;
{ /* Llib/tvector.scm 105 */
 obj_t BgL_arg1154z00_1783;
BgL_arg1154z00_1783 = 
TVECTOR_DESCR(BgL_tvz00_34); 
{ /* Llib/tvector.scm 113 */
 bool_t BgL_test1977z00_2640;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_tmpz00_2641;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_res1692z00_1787;
{ /* Llib/tvector.scm 113 */
 obj_t BgL_sz00_1786;
if(
STRUCTP(BgL_arg1154z00_1783))
{ /* Llib/tvector.scm 113 */
BgL_sz00_1786 = BgL_arg1154z00_1783; }  else 
{ 
 obj_t BgL_auxz00_2644;
BgL_auxz00_2644 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1843z00zz__tvectorz00, BGl_string1792z00zz__tvectorz00, BgL_arg1154z00_1783); 
FAILURE(BgL_auxz00_2644,BFALSE,BFALSE);} 
{ /* Llib/tvector.scm 113 */
 obj_t BgL_aux1784z00_2095;
BgL_aux1784z00_2095 = 
STRUCT_KEY(BgL_sz00_1786); 
if(
SYMBOLP(BgL_aux1784z00_2095))
{ /* Llib/tvector.scm 113 */
BgL_res1692z00_1787 = BgL_aux1784z00_2095; }  else 
{ 
 obj_t BgL_auxz00_2651;
BgL_auxz00_2651 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(4320L), BGl_string1843z00zz__tvectorz00, BGl_string1793z00zz__tvectorz00, BgL_aux1784z00_2095); 
FAILURE(BgL_auxz00_2651,BFALSE,BFALSE);} } } 
BgL_tmpz00_2641 = BgL_res1692z00_1787; } 
BgL_test1977z00_2640 = 
(BgL_tmpz00_2641==BGl_symbol1794z00zz__tvectorz00); } 
if(BgL_test1977z00_2640)
{ /* Llib/tvector.scm 113 */
 int BgL_tmpz00_2656;
BgL_tmpz00_2656 = 
(int)(0L); 
BgL_arg1248z00_1226 = 
STRUCT_REF(BgL_arg1154z00_1783, BgL_tmpz00_2656); }  else 
{ /* Llib/tvector.scm 113 */
BgL_arg1248z00_1226 = 
BGl_errorz00zz__errorz00(BGl_string1796z00zz__tvectorz00, BGl_string1795z00zz__tvectorz00, BgL_arg1154z00_1783); } } } 
return 
BGl_errorz00zz__errorz00(BGl_string1843z00zz__tvectorz00, BGl_string1831z00zz__tvectorz00, BgL_arg1248z00_1226);} } } } 

}



/* &tvector->vector */
obj_t BGl_z62tvectorzd2ze3vectorz53zz__tvectorz00(obj_t BgL_envz00_1996, obj_t BgL_tvz00_1997)
{
{ /* Llib/tvector.scm 210 */
{ /* Llib/tvector.scm 211 */
 obj_t BgL_auxz00_2661;
if(
TVECTORP(BgL_tvz00_1997))
{ /* Llib/tvector.scm 211 */
BgL_auxz00_2661 = BgL_tvz00_1997
; }  else 
{ 
 obj_t BgL_auxz00_2664;
BgL_auxz00_2664 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1788z00zz__tvectorz00, 
BINT(8400L), BGl_string1850z00zz__tvectorz00, BGl_string1790z00zz__tvectorz00, BgL_tvz00_1997); 
FAILURE(BgL_auxz00_2664,BFALSE,BFALSE);} 
return 
BGl_tvectorzd2ze3vectorz31zz__tvectorz00(BgL_auxz00_2661);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__tvectorz00(void)
{
{ /* Llib/tvector.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__hashz00(482391669L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__readerz00(220648206L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00)); 
return 
BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L, 
BSTRING_TO_STRING(BGl_string1851z00zz__tvectorz00));} 

}

#ifdef __cplusplus
}
#endif
