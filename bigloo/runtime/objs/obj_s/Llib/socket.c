/*===========================================================================*/
/*   (Llib/socket.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/socket.scm -indent -o objs/obj_s/Llib/socket.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SOCKET_TYPE_DEFINITIONS
#define BGL___SOCKET_TYPE_DEFINITIONS
#endif // BGL___SOCKET_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

extern obj_t bgl_gethostname_by_address(obj_t);
extern obj_t bgl_socket_host_addr(obj_t);
static obj_t BGl_z62socketzd2localzf3z43zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2outputzd2zz__socketz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2portzd2numberz00zz__socketz00(obj_t);
extern obj_t bgl_datagram_socket_send(obj_t, obj_t, obj_t, int);
extern obj_t bgl_datagram_socket_close(obj_t);
static obj_t BGl_z62hostz62zz__socketz00(obj_t, obj_t);
extern obj_t bgl_make_client_socket(obj_t, int, int, obj_t, obj_t, obj_t);
static obj_t BGl_z62socketzd2hostzd2addresszd3zf3z42zz__socketz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2closezd2zz__socketz00(obj_t);
extern long bgl_socket_accept_many(obj_t, bool_t, obj_t, obj_t, obj_t);
extern obj_t bstring_to_keyword(obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__socketz00 = BUNSPEC;
static obj_t BGl_keyword2068z00zz__socketz00 = BUNSPEC;
static obj_t BGl_symbol2100z00zz__socketz00 = BUNSPEC;
static obj_t BGl_symbol2102z00zz__socketz00 = BUNSPEC;
extern obj_t bgl_make_server_socket(obj_t, int, int, obj_t);
static obj_t BGl_symbol2104z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62datagramzd2socketzd2portzd2numberzb0zz__socketz00(obj_t, obj_t);
extern obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2acceptzd2zz__socketz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62socketzd2inputzb0zz__socketz00(obj_t, obj_t);
static obj_t BGl_keyword2070z00zz__socketz00 = BUNSPEC;
static obj_t BGl_keyword2078z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62socketzd2portzd2numberz62zz__socketz00(obj_t, obj_t);
extern obj_t bgl_setsockopt(obj_t, obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2110z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62socketzd2localzd2addressz62zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__socketz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
BGL_EXPORTED_DECL obj_t BGl_hostz00zz__socketz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2clientzd2socketz00zz__socketz00(obj_t, int, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2036z00zz__socketz00 = BUNSPEC;
extern obj_t bgl_gethostname(void);
extern obj_t bgl_gethostinterfaces(void);
BGL_EXPORTED_DECL obj_t BGl_socketzd2acceptzd2manyz00zz__socketz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2optionzd2setz12z12zz__socketz00(obj_t, obj_t, obj_t);
static obj_t BGl_keyword2085z00zz__socketz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2outputz00zz__socketz00(obj_t);
static obj_t BGl_keyword2087z00zz__socketz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_makezd2serverzd2socketz00zz__socketz00(obj_t);
static obj_t BGl_symbol2048z00zz__socketz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_socketzd2inputzd2zz__socketz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2hostzd2addressz00zz__socketz00(obj_t);
static obj_t BGl_toplevelzd2initzd2zz__socketz00(void);
static obj_t BGl_z62socketzd2outputzb0zz__socketz00(obj_t, obj_t);
extern obj_t bgl_make_datagram_unbound_socket(obj_t);
extern void socket_cleanup(void);
BGL_EXPORTED_DECL obj_t BGl_z52socketzd2initz12z92zz__socketz00(void);
static obj_t BGl__socketzd2shutdownzd2zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_socketzd2localzd2addressz00zz__socketz00(obj_t);
static obj_t BGl_z62getzd2interfaceszb0zz__socketz00(obj_t);
static obj_t BGl_symbol2131z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62datagramzd2socketzd2optionzd2setz12za2zz__socketz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__hostnamez00zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_hostnamez00zz__socketz00(obj_t);
static obj_t BGl_symbol2135z00zz__socketz00 = BUNSPEC;
static obj_t BGl_symbol2055z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62datagramzd2socketzd2optionz62zz__socketz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2hostnamez00zz__socketz00(obj_t);
static obj_t BGl_symbol2138z00zz__socketz00 = BUNSPEC;
static obj_t BGl_symbol2057z00zz__socketz00 = BUNSPEC;
extern obj_t bgl_getsockopt(obj_t, obj_t);
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
static obj_t BGl_z62socketzd2optionzd2setz12z70zz__socketz00(obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(obj_t, obj_t, obj_t);
extern obj_t bgl_getprotobyname(char *);
BGL_EXPORTED_DECL obj_t BGl_socketzd2hostnamezd2zz__socketz00(obj_t);
static obj_t BGl_cnstzd2initzd2zz__socketz00(void);
extern obj_t BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t, obj_t, int);
static obj_t BGl_z62socketzd2hostnamezb0zz__socketz00(obj_t, obj_t);
static obj_t BGl_symbol2060z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62makezd2serverzd2socketz62zz__socketz00(obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__socketz00(void);
static obj_t BGl_symbol2062z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62resolvz62zz__socketz00(obj_t, obj_t, obj_t);
extern bool_t bgl_socket_localp(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2receivez00zz__socketz00(obj_t, int);
static obj_t BGl_list2039z00zz__socketz00 = BUNSPEC;
static obj_t BGl_search1131ze70ze7zz__socketz00(long, obj_t, obj_t, long);
static obj_t BGl_importedzd2moduleszd2initz00zz__socketz00(void);
static obj_t BGl_z62datagramzd2socketzd2receivez62zz__socketz00(obj_t, obj_t, obj_t);
static obj_t BGl__makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t, obj_t);
static obj_t BGl_gczd2rootszd2initz00zz__socketz00(void);
BGL_EXPORTED_DECL obj_t BGl_getzd2protocolszd2zz__socketz00(void);
static obj_t BGl_z62socketzd2downzf3z43zz__socketz00(obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__socketz00(void);
static obj_t BGl_z62socketzd2clientzf3z43zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2closez00zz__socketz00(obj_t);
static obj_t BGl__makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t, obj_t);
extern obj_t bgl_make_server_unix_socket(obj_t, int);
BGL_EXPORTED_DECL obj_t BGl_socketzd2optionzd2zz__socketz00(obj_t, obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t socket_close(obj_t);
static obj_t BGl_z62socketzd2serverzf3z43zz__socketz00(obj_t, obj_t);
static obj_t BGl_symbol2080z00zz__socketz00 = BUNSPEC;
static bool_t BGl_za2socketzd2initializa7edza2z75zz__socketz00;
static obj_t BGl_symbol2089z00zz__socketz00 = BUNSPEC;
extern obj_t bgl_datagram_socket_receive(obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_socketzd2downzf3z21zz__socketz00(obj_t);
static obj_t BGl_z62datagramzd2socketzd2sendz62zz__socketz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62z52socketzd2initz12zf0zz__socketz00(obj_t);
BGL_EXPORTED_DECL bool_t BGl_datagramzd2socketzf3z21zz__socketz00(obj_t);
static obj_t BGl_za2socketzd2mutexza2zd2zz__socketz00 = BUNSPEC;
static obj_t BGl__makezd2clientzd2socketz00zz__socketz00(obj_t, obj_t);
extern obj_t bgl_host(obj_t);
static obj_t BGl_symbol2096z00zz__socketz00 = BUNSPEC;
static obj_t BGl__socketzd2acceptzd2manyz00zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_socketzf3zf3zz__socketz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_hostinfoz00zz__socketz00(obj_t);
static obj_t BGl_list2067z00zz__socketz00 = BUNSPEC;
static obj_t BGl_search1141ze70ze7zz__socketz00(long, obj_t, obj_t, long);
extern obj_t bgl_getsockopt(obj_t, obj_t);
static obj_t BGl_z62socketzf3z91zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t, int, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2inputz00zz__socketz00(obj_t);
static obj_t BGl_z62datagramzd2socketzd2hostzd2addresszb0zz__socketz00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2portzd2numberzd2zz__socketz00(obj_t);
extern obj_t bgl_res_query(obj_t, obj_t);
extern obj_t make_vector(long, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t, obj_t);
static obj_t BGl__makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t, obj_t);
static obj_t BGl_list2077z00zz__socketz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2sendz00zz__socketz00(obj_t, obj_t, obj_t, int);
BGL_EXPORTED_DECL obj_t BGl_getzd2interfaceszd2zz__socketz00(void);
extern obj_t bgl_setsockopt(obj_t, obj_t, obj_t);
static obj_t BGl_z62getzd2protocolszb0zz__socketz00(obj_t);
extern bool_t bgl_socket_host_addr_cmp(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2optionz00zz__socketz00(obj_t, obj_t);
static obj_t BGl_z62datagramzd2socketzd2closez62zz__socketz00(obj_t, obj_t);
extern obj_t BGl_registerzd2exitzd2functionz12z12zz__biglooz00(obj_t);
static obj_t BGl_methodzd2initzd2zz__socketz00(void);
static obj_t BGl_z62datagramzd2socketzd2hostnamez62zz__socketz00(obj_t, obj_t);
static obj_t BGl_list2084z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31324ze3ze5zz__socketz00(obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL int BGl_socketzd2shutdownzd2zz__socketz00(obj_t, obj_t);
extern obj_t bgl_make_datagram_client_socket(obj_t, int, bool_t, obj_t);
static obj_t BGl_z62socketzd2optionzb0zz__socketz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2hostzd2addresszd2zz__socketz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_resolvz00zz__socketz00(obj_t, obj_t);
extern obj_t BGl_mapz00zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_socketzd2localzf3z21zz__socketz00(obj_t);
extern obj_t bgl_make_datagram_server_socket(int, obj_t);
extern obj_t bgl_socket_accept(obj_t, bool_t, obj_t, obj_t);
static obj_t BGl_z62socketzd2hostzd2addressz62zz__socketz00(obj_t, obj_t);
static obj_t BGl_search1117ze70ze7zz__socketz00(long, obj_t, obj_t, long);
BGL_EXPORTED_DECL bool_t BGl_socketzd2clientzf3z21zz__socketz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_datagramzd2socketzd2optionzd2setz12zc0zz__socketz00(obj_t, obj_t, obj_t);
extern void socket_startup(void);
extern obj_t BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_socketzd2hostzd2addresszd3zf3z20zz__socketz00(obj_t, obj_t);
static obj_t BGl_z62getzd2protocolzb0zz__socketz00(obj_t, obj_t);
static obj_t BGl__socketzd2acceptzd2zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_socketzd2serverzf3z21zz__socketz00(obj_t);
extern obj_t bgl_socket_local_addr(obj_t);
static obj_t BGl_z62zc3z04anonymousza31245ze3ze5zz__socketz00(obj_t, obj_t);
extern obj_t bgl_getprotoents(void);
extern int socket_shutdown(obj_t, int);
static obj_t BGl_z62datagramzd2socketzd2inputz62zz__socketz00(obj_t, obj_t);
static obj_t BGl_z62datagramzd2socketzd2outputz62zz__socketz00(obj_t, obj_t);
extern obj_t bgl_getprotobynumber(int);
BGL_EXPORTED_DECL bool_t BGl_datagramzd2socketzd2clientzf3zf3zz__socketz00(obj_t);
static obj_t BGl_z62datagramzd2socketzd2clientzf3z91zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t);
extern obj_t bgl_make_client_unix_socket(obj_t, int, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_datagramzd2socketzd2serverzf3zf3zz__socketz00(obj_t);
static obj_t BGl_z62datagramzd2socketzf3z43zz__socketz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_getzd2protocolzd2zz__socketz00(obj_t);
static obj_t BGl_z62datagramzd2socketzd2serverzf3z91zz__socketz00(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_keyword2040z00zz__socketz00 = BUNSPEC;
static obj_t BGl_keyword2042z00zz__socketz00 = BUNSPEC;
extern obj_t bgl_hostinfo(obj_t);
static obj_t BGl_keyword2044z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62hostinfoz62zz__socketz00(obj_t, obj_t);
static obj_t BGl_keyword2046z00zz__socketz00 = BUNSPEC;
static obj_t BGl_z62socketzd2closezb0zz__socketz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string2120z00zz__socketz00, BgL_bgl_string2120za700za7za7_2148za7, "datagram-socket", 15 );
DEFINE_STRING( BGl_string2121z00zz__socketz00, BgL_bgl_string2121za700za7za7_2149za7, "&datagram-socket-host-address", 29 );
DEFINE_STRING( BGl_string2122z00zz__socketz00, BgL_bgl_string2122za700za7za7_2150za7, "&datagram-socket-port-number", 28 );
DEFINE_STRING( BGl_string2041z00zz__socketz00, BgL_bgl_string2041za700za7za7_2151za7, "domain", 6 );
DEFINE_STRING( BGl_string2123z00zz__socketz00, BgL_bgl_string2123za700za7za7_2152za7, "datagram-socket-output", 22 );
DEFINE_STRING( BGl_string2124z00zz__socketz00, BgL_bgl_string2124za700za7za7_2153za7, "output-port", 11 );
DEFINE_STRING( BGl_string2043z00zz__socketz00, BgL_bgl_string2043za700za7za7_2154za7, "inbuf", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hostinfozd2envzd2zz__socketz00, BgL_bgl_za762hostinfoza762za7za72155z00, BGl_z62hostinfoz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2125z00zz__socketz00, BgL_bgl_string2125za700za7za7_2156za7, "Datagram-socket has no output port", 34 );
DEFINE_STRING( BGl_string2126z00zz__socketz00, BgL_bgl_string2126za700za7za7_2157za7, "&datagram-socket-output", 23 );
DEFINE_STRING( BGl_string2045z00zz__socketz00, BgL_bgl_string2045za700za7za7_2158za7, "outbuf", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2closezd2envz00zz__socketz00, BgL_bgl_za762socketza7d2clos2159z00, BGl_z62socketzd2closezb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2127z00zz__socketz00, BgL_bgl_string2127za700za7za7_2160za7, "datagram-socket-input", 21 );
DEFINE_STRING( BGl_string2128z00zz__socketz00, BgL_bgl_string2128za700za7za7_2161za7, "input-port", 10 );
DEFINE_STRING( BGl_string2047z00zz__socketz00, BgL_bgl_string2047za700za7za7_2162za7, "timeout", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2datagramzd2clientzd2socketzd2envz00zz__socketz00, BgL_bgl__makeza7d2datagram2163za7, opt_generic_entry, BGl__makezd2datagramzd2clientzd2socketzd2zz__socketz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2129z00zz__socketz00, BgL_bgl_string2129za700za7za7_2164za7, "Datagram-socket has no input port", 33 );
DEFINE_STRING( BGl_string2049z00zz__socketz00, BgL_bgl_string2049za700za7za7_2165za7, "make-client-socket::socket", 26 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2datagramzd2serverzd2socketzd2envz00zz__socketz00, BgL_bgl__makeza7d2datagram2166za7, opt_generic_entry, BGl__makezd2datagramzd2serverzd2socketzd2zz__socketz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2130z00zz__socketz00, BgL_bgl_string2130za700za7za7_2167za7, "&datagram-socket-input", 22 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2receivezd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2168z00, BGl_z62datagramzd2socketzd2receivez62zz__socketz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2050z00zz__socketz00, BgL_bgl_string2050za700za7za7_2169za7, "Illegal keyword argument", 24 );
DEFINE_STRING( BGl_string2132z00zz__socketz00, BgL_bgl_string2132za700za7za7_2170za7, "make-datagram-server-socket", 27 );
DEFINE_STRING( BGl_string2051z00zz__socketz00, BgL_bgl_string2051za700za7za7_2171za7, "_make-client-socket", 19 );
DEFINE_STRING( BGl_string2133z00zz__socketz00, BgL_bgl_string2133za700za7za7_2172za7, "wrong number of arguments: [0..2] expected, provided", 52 );
DEFINE_STRING( BGl_string2052z00zz__socketz00, BgL_bgl_string2052za700za7za7_2173za7, "bint", 4 );
DEFINE_STRING( BGl_string2134z00zz__socketz00, BgL_bgl_string2134za700za7za7_2174za7, "_make-datagram-server-socket", 28 );
DEFINE_STRING( BGl_string2053z00zz__socketz00, BgL_bgl_string2053za700za7za7_2175za7, "wrong number of arguments: [2..6] expected, provided", 52 );
DEFINE_STRING( BGl_string2054z00zz__socketz00, BgL_bgl_string2054za700za7za7_2176za7, "make-client-socket", 18 );
DEFINE_STRING( BGl_string2136z00zz__socketz00, BgL_bgl_string2136za700za7za7_2177za7, "make-datagram-unbound-socket", 28 );
DEFINE_STRING( BGl_string2137z00zz__socketz00, BgL_bgl_string2137za700za7za7_2178za7, "_make-datagram-unbound-socket", 29 );
DEFINE_STRING( BGl_string2056z00zz__socketz00, BgL_bgl_string2056za700za7za7_2179za7, "inet6", 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2sendzd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2180z00, BGl_z62datagramzd2socketzd2sendz62zz__socketz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string2139z00zz__socketz00, BgL_bgl_string2139za700za7za7_2181za7, "make-datagram-client-socket", 27 );
DEFINE_STRING( BGl_string2058z00zz__socketz00, BgL_bgl_string2058za700za7za7_2182za7, "unspec", 6 );
DEFINE_STRING( BGl_string2059z00zz__socketz00, BgL_bgl_string2059za700za7za7_2183za7, "symbol", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2clientzd2socketzd2envzd2zz__socketz00, BgL_bgl__makeza7d2clientza7d2184z00, opt_generic_entry, BGl__makezd2clientzd2socketz00zz__socketz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2140z00zz__socketz00, BgL_bgl_string2140za700za7za7_2185za7, "wrong number of arguments: [2..4] expected, provided", 52 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2acceptzd2manyzd2envzd2zz__socketz00, BgL_bgl__socketza7d2accept2186za7, opt_generic_entry, BGl__socketzd2acceptzd2manyz00zz__socketz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2141z00zz__socketz00, BgL_bgl_string2141za700za7za7_2187za7, "_make-datagram-client-socket", 28 );
DEFINE_STRING( BGl_string2142z00zz__socketz00, BgL_bgl_string2142za700za7za7_2188za7, "&datagram-socket-close", 22 );
DEFINE_STRING( BGl_string2061z00zz__socketz00, BgL_bgl_string2061za700za7za7_2189za7, "unix", 4 );
DEFINE_STRING( BGl_string2143z00zz__socketz00, BgL_bgl_string2143za700za7za7_2190za7, "&datagram-socket-receive", 24 );
DEFINE_STRING( BGl_string2144z00zz__socketz00, BgL_bgl_string2144za700za7za7_2191za7, "&datagram-socket-send", 21 );
DEFINE_STRING( BGl_string2063z00zz__socketz00, BgL_bgl_string2063za700za7za7_2192za7, "local", 5 );
DEFINE_STRING( BGl_string2145z00zz__socketz00, BgL_bgl_string2145za700za7za7_2193za7, "&datagram-socket-option", 23 );
DEFINE_STRING( BGl_string2064z00zz__socketz00, BgL_bgl_string2064za700za7za7_2194za7, "Unknown socket domain", 21 );
DEFINE_STRING( BGl_string2146z00zz__socketz00, BgL_bgl_string2146za700za7za7_2195za7, "&datagram-socket-option-set!", 28 );
DEFINE_STRING( BGl_string2065z00zz__socketz00, BgL_bgl_string2065za700za7za7_2196za7, "make-server-socket", 18 );
DEFINE_STRING( BGl_string2147z00zz__socketz00, BgL_bgl_string2147za700za7za7_2197za7, "__socket", 8 );
DEFINE_STRING( BGl_string2066z00zz__socketz00, BgL_bgl_string2066za700za7za7_2198za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2optionzd2envz00zz__socketz00, BgL_bgl_za762socketza7d2opti2199z00, BGl_z62socketzd2optionzb0zz__socketz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2069z00zz__socketz00, BgL_bgl_string2069za700za7za7_2200za7, "backlog", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2shutdownzd2envz00zz__socketz00, BgL_bgl__socketza7d2shutdo2201za7, opt_generic_entry, BGl__socketzd2shutdownzd2zz__socketz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2serverzd2socketzd2envzd2zz__socketz00, BgL_bgl_za762makeza7d2server2202z00, va_generic_entry, BGl_z62makezd2serverzd2socketz62zz__socketz00, BUNSPEC, -1 );
extern obj_t BGl_stringzd2appendzd2envz00zz__r4_strings_6_7z00;
DEFINE_STRING( BGl_string2071z00zz__socketz00, BgL_bgl_string2071za700za7za7_2203za7, "name", 4 );
DEFINE_STRING( BGl_string2072z00zz__socketz00, BgL_bgl_string2072za700za7za7_2204za7, "Unsupported domain", 18 );
DEFINE_STRING( BGl_string2074z00zz__socketz00, BgL_bgl_string2074za700za7za7_2205za7, "Illegal extra key arguments: ", 29 );
DEFINE_STRING( BGl_string2075z00zz__socketz00, BgL_bgl_string2075za700za7za7_2206za7, "dsssl-get-key-arg", 17 );
DEFINE_STRING( BGl_string2076z00zz__socketz00, BgL_bgl_string2076za700za7za7_2207za7, "~a ", 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z52socketzd2initz12zd2envz40zz__socketz00, BgL_bgl_za762za752socketza7d2i2208za7, BGl_z62z52socketzd2initz12zf0zz__socketz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2portzd2numberzd2envzd2zz__socketz00, BgL_bgl_za762socketza7d2port2209z00, BGl_z62socketzd2portzd2numberz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2079z00zz__socketz00, BgL_bgl_string2079za700za7za7_2210za7, "errp", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2localzd2addresszd2envzd2zz__socketz00, BgL_bgl_za762socketza7d2loca2211z00, BGl_z62socketzd2localzd2addressz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2073z00zz__socketz00, BgL_bgl_za762za7c3za704anonymo2212za7, BGl_z62zc3z04anonymousza31324ze3ze5zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2081z00zz__socketz00, BgL_bgl_string2081za700za7za7_2213za7, "socket-accept", 13 );
DEFINE_STRING( BGl_string2082z00zz__socketz00, BgL_bgl_string2082za700za7za7_2214za7, "_socket-accept", 14 );
DEFINE_STRING( BGl_string2083z00zz__socketz00, BgL_bgl_string2083za700za7za7_2215za7, "wrong number of arguments: [1..4] expected, provided", 52 );
DEFINE_STRING( BGl_string2086z00zz__socketz00, BgL_bgl_string2086za700za7za7_2216za7, "inbufs", 6 );
DEFINE_STRING( BGl_string2088z00zz__socketz00, BgL_bgl_string2088za700za7za7_2217za7, "outbufs", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2portzd2numberzd2envz00zz__socketz00, BgL_bgl_za762datagramza7d2so2218z00, BGl_z62datagramzd2socketzd2portzd2numberzb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2clientzf3zd2envz21zz__socketz00, BgL_bgl_za762datagramza7d2so2219z00, BGl_z62datagramzd2socketzd2clientzf3z91zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_makezd2datagramzd2unboundzd2socketzd2envz00zz__socketz00, BgL_bgl__makeza7d2datagram2220za7, opt_generic_entry, BGl__makezd2datagramzd2unboundzd2socketzd2zz__socketz00, BFALSE, -1 );
DEFINE_STRING( BGl_string2090z00zz__socketz00, BgL_bgl_string2090za700za7za7_2221za7, "socket-accept-many", 18 );
DEFINE_STRING( BGl_string2091z00zz__socketz00, BgL_bgl_string2091za700za7za7_2222za7, "_socket-accept-many", 19 );
DEFINE_STRING( BGl_string2092z00zz__socketz00, BgL_bgl_string2092za700za7za7_2223za7, "vector", 6 );
DEFINE_STRING( BGl_string2093z00zz__socketz00, BgL_bgl_string2093za700za7za7_2224za7, "wrong number of arguments: [2..5] expected, provided", 52 );
DEFINE_STRING( BGl_string2094z00zz__socketz00, BgL_bgl_string2094za700za7za7_2225za7, "loop", 4 );
DEFINE_STRING( BGl_string2095z00zz__socketz00, BgL_bgl_string2095za700za7za7_2226za7, "vector-set!", 11 );
DEFINE_STRING( BGl_string2097z00zz__socketz00, BgL_bgl_string2097za700za7za7_2227za7, "socket-shutdown", 15 );
DEFINE_STRING( BGl_string2098z00zz__socketz00, BgL_bgl_string2098za700za7za7_2228za7, "wrong number of arguments: [1..2] expected, provided", 52 );
DEFINE_STRING( BGl_string2099z00zz__socketz00, BgL_bgl_string2099za700za7za7_2229za7, "_socket-shutdown", 16 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2serverzf3zd2envz21zz__socketz00, BgL_bgl_za762datagramza7d2so2230z00, BGl_z62datagramzd2socketzd2serverzf3z91zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2optionzd2setz12zd2envzc0zz__socketz00, BgL_bgl_za762socketza7d2opti2231z00, BGl_z62socketzd2optionzd2setz12z70zz__socketz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2outputzd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2232z00, BGl_z62datagramzd2socketzd2outputz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_resolvzd2envzd2zz__socketz00, BgL_bgl_za762resolvza762za7za7__2233z00, BGl_z62resolvz62zz__socketz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2optionzd2setz12zd2envz12zz__socketz00, BgL_bgl_za762datagramza7d2so2234z00, BGl_z62datagramzd2socketzd2optionzd2setz12za2zz__socketz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2hostzd2addresszd3zf3zd2envzf2zz__socketz00, BgL_bgl_za762socketza7d2host2235z00, BGl_z62socketzd2hostzd2addresszd3zf3z42zz__socketz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzf3zd2envz21zz__socketz00, BgL_bgl_za762socketza7f3za791za72236z00, BGl_z62socketzf3z91zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2inputzd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2237z00, BGl_z62datagramzd2socketzd2inputz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2hostnamezd2envz00zz__socketz00, BgL_bgl_za762socketza7d2host2238z00, BGl_z62socketzd2hostnamezb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2acceptzd2envz00zz__socketz00, BgL_bgl__socketza7d2accept2239za7, opt_generic_entry, BGl__socketzd2acceptzd2zz__socketz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hostzd2envzd2zz__socketz00, BgL_bgl_za762hostza762za7za7__so2240z00, BGl_z62hostz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2protocolzd2envz00zz__socketz00, BgL_bgl_za762getza7d2protoco2241z00, BGl_z62getzd2protocolzb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2localzf3zd2envzf3zz__socketz00, BgL_bgl_za762socketza7d2loca2242z00, BGl_z62socketzd2localzf3z43zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2inputzd2envz00zz__socketz00, BgL_bgl_za762socketza7d2inpu2243z00, BGl_z62socketzd2inputzb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2outputzd2envz00zz__socketz00, BgL_bgl_za762socketza7d2outp2244z00, BGl_z62socketzd2outputzb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2downzf3zd2envzf3zz__socketz00, BgL_bgl_za762socketza7d2down2245z00, BGl_z62socketzd2downzf3z43zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2clientzf3zd2envzf3zz__socketz00, BgL_bgl_za762socketza7d2clie2246z00, BGl_z62socketzd2clientzf3z43zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2serverzf3zd2envzf3zz__socketz00, BgL_bgl_za762socketza7d2serv2247z00, BGl_z62socketzd2serverzf3z43zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2interfaceszd2envz00zz__socketz00, BgL_bgl_za762getza7d2interfa2248z00, BGl_z62getzd2interfaceszb0zz__socketz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2protocolszd2envz00zz__socketz00, BgL_bgl_za762getza7d2protoco2249z00, BGl_z62getzd2protocolszb0zz__socketz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2closezd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2250z00, BGl_z62datagramzd2socketzd2closez62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2optionzd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2251z00, BGl_z62datagramzd2socketzd2optionz62zz__socketz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string2101z00zz__socketz00, BgL_bgl_string2101za700za7za7_2252za7, "RDWR", 4 );
DEFINE_STRING( BGl_string2103z00zz__socketz00, BgL_bgl_string2103za700za7za7_2253za7, "WR", 2 );
DEFINE_STRING( BGl_string2023z00zz__socketz00, BgL_bgl_string2023za700za7za7_2254za7, "socket", 6 );
DEFINE_STRING( BGl_string2105z00zz__socketz00, BgL_bgl_string2105za700za7za7_2255za7, "RD", 2 );
DEFINE_STRING( BGl_string2106z00zz__socketz00, BgL_bgl_string2106za700za7za7_2256za7, "wrong optional argument", 23 );
DEFINE_STRING( BGl_string2025z00zz__socketz00, BgL_bgl_string2025za700za7za7_2257za7, "/tmp/bigloo/runtime/Llib/socket.scm", 35 );
DEFINE_STRING( BGl_string2107z00zz__socketz00, BgL_bgl_string2107za700za7za7_2258za7, "&socket-close", 13 );
DEFINE_STRING( BGl_string2026z00zz__socketz00, BgL_bgl_string2026za700za7za7_2259za7, "&socket-hostname", 16 );
DEFINE_STRING( BGl_string2108z00zz__socketz00, BgL_bgl_string2108za700za7za7_2260za7, "&host", 5 );
DEFINE_STRING( BGl_string2027z00zz__socketz00, BgL_bgl_string2027za700za7za7_2261za7, "&socket-host-address", 20 );
DEFINE_STRING( BGl_string2109z00zz__socketz00, BgL_bgl_string2109za700za7za7_2262za7, "&hostinfo", 9 );
DEFINE_STRING( BGl_string2028z00zz__socketz00, BgL_bgl_string2028za700za7za7_2263za7, "&socket-local-address", 21 );
DEFINE_STRING( BGl_string2029z00zz__socketz00, BgL_bgl_string2029za700za7za7_2264za7, "&socket-local?", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2hostzd2addresszd2envz00zz__socketz00, BgL_bgl_za762datagramza7d2so2265z00, BGl_z62datagramzd2socketzd2hostzd2addresszb0zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2111z00zz__socketz00, BgL_bgl_string2111za700za7za7_2266za7, "hostname", 8 );
DEFINE_STRING( BGl_string2030z00zz__socketz00, BgL_bgl_string2030za700za7za7_2267za7, "&socket-host-address=?", 22 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2024z00zz__socketz00, BgL_bgl_za762za7c3za704anonymo2268za7, BGl_z62zc3z04anonymousza31245ze3ze5zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2112z00zz__socketz00, BgL_bgl_string2112za700za7za7_2269za7, "wrong number of arguments: [0..1] expected, provided", 52 );
DEFINE_STRING( BGl_string2031z00zz__socketz00, BgL_bgl_string2031za700za7za7_2270za7, "bstring", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_socketzd2hostzd2addresszd2envzd2zz__socketz00, BgL_bgl_za762socketza7d2host2271z00, BGl_z62socketzd2hostzd2addressz62zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2113z00zz__socketz00, BgL_bgl_string2113za700za7za7_2272za7, "_hostname", 9 );
DEFINE_STRING( BGl_string2032z00zz__socketz00, BgL_bgl_string2032za700za7za7_2273za7, "&socket-down?", 13 );
DEFINE_STRING( BGl_string2114z00zz__socketz00, BgL_bgl_string2114za700za7za7_2274za7, "&resolv", 7 );
DEFINE_STRING( BGl_string2033z00zz__socketz00, BgL_bgl_string2033za700za7za7_2275za7, "&socket-port-number", 19 );
DEFINE_STRING( BGl_string2115z00zz__socketz00, BgL_bgl_string2115za700za7za7_2276za7, "get-protocol", 12 );
DEFINE_STRING( BGl_string2034z00zz__socketz00, BgL_bgl_string2034za700za7za7_2277za7, "&socket-input", 13 );
DEFINE_STRING( BGl_string2116z00zz__socketz00, BgL_bgl_string2116za700za7za7_2278za7, "&socket-option", 14 );
DEFINE_STRING( BGl_string2035z00zz__socketz00, BgL_bgl_string2035za700za7za7_2279za7, "&socket-output", 14 );
DEFINE_STRING( BGl_string2117z00zz__socketz00, BgL_bgl_string2117za700za7za7_2280za7, "keyword", 7 );
DEFINE_STRING( BGl_string2118z00zz__socketz00, BgL_bgl_string2118za700za7za7_2281za7, "&socket-option-set!", 19 );
DEFINE_STRING( BGl_string2037z00zz__socketz00, BgL_bgl_string2037za700za7za7_2282za7, "inet", 4 );
DEFINE_STRING( BGl_string2119z00zz__socketz00, BgL_bgl_string2119za700za7za7_2283za7, "&datagram-socket-hostname", 25 );
DEFINE_STRING( BGl_string2038z00zz__socketz00, BgL_bgl_string2038za700za7za7_2284za7, "vector-ref", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzf3zd2envzf3zz__socketz00, BgL_bgl_za762datagramza7d2so2285z00, BGl_z62datagramzd2socketzf3z43zz__socketz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_hostnamezd2envzd2zz__socketz00, BgL_bgl__hostnameza700za7za7__2286za7, opt_generic_entry, BGl__hostnamez00zz__socketz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_datagramzd2socketzd2hostnamezd2envzd2zz__socketz00, BgL_bgl_za762datagramza7d2so2287z00, BGl_z62datagramzd2socketzd2hostnamez62zz__socketz00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2068z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2100z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2102z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2104z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2070z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2078z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2110z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2036z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2085z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2087z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2048z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2131z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2135z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2055z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2138z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2057z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2060z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2062z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_list2039z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2080z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2089z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_za2socketzd2mutexza2zd2zz__socketz00) );
ADD_ROOT( (void *)(&BGl_symbol2096z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_list2067z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_list2077z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_list2084z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2040z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2042z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2044z00zz__socketz00) );
ADD_ROOT( (void *)(&BGl_keyword2046z00zz__socketz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__socketz00(long BgL_checksumz00_2628, char * BgL_fromz00_2629)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__socketz00))
{ 
BGl_requirezd2initializa7ationz75zz__socketz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__socketz00(); 
BGl_cnstzd2initzd2zz__socketz00(); 
BGl_importedzd2moduleszd2initz00zz__socketz00(); 
return 
BGl_toplevelzd2initzd2zz__socketz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
BGl_symbol2036z00zz__socketz00 = 
bstring_to_symbol(BGl_string2037z00zz__socketz00); 
BGl_keyword2040z00zz__socketz00 = 
bstring_to_keyword(BGl_string2041z00zz__socketz00); 
BGl_keyword2042z00zz__socketz00 = 
bstring_to_keyword(BGl_string2043z00zz__socketz00); 
BGl_keyword2044z00zz__socketz00 = 
bstring_to_keyword(BGl_string2045z00zz__socketz00); 
BGl_keyword2046z00zz__socketz00 = 
bstring_to_keyword(BGl_string2047z00zz__socketz00); 
BGl_list2039z00zz__socketz00 = 
MAKE_YOUNG_PAIR(BGl_keyword2040z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2042z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2044z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2046z00zz__socketz00, BNIL)))); 
BGl_symbol2048z00zz__socketz00 = 
bstring_to_symbol(BGl_string2049z00zz__socketz00); 
BGl_symbol2055z00zz__socketz00 = 
bstring_to_symbol(BGl_string2056z00zz__socketz00); 
BGl_symbol2057z00zz__socketz00 = 
bstring_to_symbol(BGl_string2058z00zz__socketz00); 
BGl_symbol2060z00zz__socketz00 = 
bstring_to_symbol(BGl_string2061z00zz__socketz00); 
BGl_symbol2062z00zz__socketz00 = 
bstring_to_symbol(BGl_string2063z00zz__socketz00); 
BGl_keyword2068z00zz__socketz00 = 
bstring_to_keyword(BGl_string2069z00zz__socketz00); 
BGl_keyword2070z00zz__socketz00 = 
bstring_to_keyword(BGl_string2071z00zz__socketz00); 
BGl_list2067z00zz__socketz00 = 
MAKE_YOUNG_PAIR(BGl_keyword2040z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2068z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2070z00zz__socketz00, BNIL))); 
BGl_keyword2078z00zz__socketz00 = 
bstring_to_keyword(BGl_string2079z00zz__socketz00); 
BGl_list2077z00zz__socketz00 = 
MAKE_YOUNG_PAIR(BGl_keyword2078z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2042z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2044z00zz__socketz00, BNIL))); 
BGl_symbol2080z00zz__socketz00 = 
bstring_to_symbol(BGl_string2081z00zz__socketz00); 
BGl_keyword2085z00zz__socketz00 = 
bstring_to_keyword(BGl_string2086z00zz__socketz00); 
BGl_keyword2087z00zz__socketz00 = 
bstring_to_keyword(BGl_string2088z00zz__socketz00); 
BGl_list2084z00zz__socketz00 = 
MAKE_YOUNG_PAIR(BGl_keyword2078z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2085z00zz__socketz00, 
MAKE_YOUNG_PAIR(BGl_keyword2087z00zz__socketz00, BNIL))); 
BGl_symbol2089z00zz__socketz00 = 
bstring_to_symbol(BGl_string2090z00zz__socketz00); 
BGl_symbol2096z00zz__socketz00 = 
bstring_to_symbol(BGl_string2097z00zz__socketz00); 
BGl_symbol2100z00zz__socketz00 = 
bstring_to_symbol(BGl_string2101z00zz__socketz00); 
BGl_symbol2102z00zz__socketz00 = 
bstring_to_symbol(BGl_string2103z00zz__socketz00); 
BGl_symbol2104z00zz__socketz00 = 
bstring_to_symbol(BGl_string2105z00zz__socketz00); 
BGl_symbol2110z00zz__socketz00 = 
bstring_to_symbol(BGl_string2111z00zz__socketz00); 
BGl_symbol2131z00zz__socketz00 = 
bstring_to_symbol(BGl_string2132z00zz__socketz00); 
BGl_symbol2135z00zz__socketz00 = 
bstring_to_symbol(BGl_string2136z00zz__socketz00); 
return ( 
BGl_symbol2138z00zz__socketz00 = 
bstring_to_symbol(BGl_string2139z00zz__socketz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
BGl_za2socketzd2initializa7edza2z75zz__socketz00 = ((bool_t)0); 
return ( 
BGl_za2socketzd2mutexza2zd2zz__socketz00 = 
bgl_make_mutex(BGl_string2023z00zz__socketz00), BUNSPEC) ;} 

}



/* %socket-init! */
BGL_EXPORTED_DEF obj_t BGl_z52socketzd2initz12z92zz__socketz00(void)
{
{ /* Llib/socket.scm 286 */
{ /* Llib/socket.scm 287 */
 obj_t BgL_top2290z00_2678;
BgL_top2290z00_2678 = 
BGL_EXITD_TOP_AS_OBJ(); 
BGL_MUTEX_LOCK(BGl_za2socketzd2mutexza2zd2zz__socketz00); 
BGL_EXITD_PUSH_PROTECT(BgL_top2290z00_2678, BGl_za2socketzd2mutexza2zd2zz__socketz00); BUNSPEC; 
{ /* Llib/socket.scm 287 */
 obj_t BgL_tmp2289z00_2677;
if(BGl_za2socketzd2initializa7edza2z75zz__socketz00)
{ /* Llib/socket.scm 288 */
BgL_tmp2289z00_2677 = BFALSE; }  else 
{ /* Llib/socket.scm 288 */
BGl_za2socketzd2initializa7edza2z75zz__socketz00 = ((bool_t)1); 
socket_startup(); BUNSPEC; 
BGl_registerzd2exitzd2functionz12z12zz__biglooz00(BGl_proc2024z00zz__socketz00); 
BgL_tmp2289z00_2677 = BUNSPEC; } 
BGL_EXITD_POP_PROTECT(BgL_top2290z00_2678); BUNSPEC; 
BGL_MUTEX_UNLOCK(BGl_za2socketzd2mutexza2zd2zz__socketz00); 
return BgL_tmp2289z00_2677;} } } 

}



/* &%socket-init! */
obj_t BGl_z62z52socketzd2initz12zf0zz__socketz00(obj_t BgL_envz00_2222)
{
{ /* Llib/socket.scm 286 */
return 
BGl_z52socketzd2initz12z92zz__socketz00();} 

}



/* &<@anonymous:1245> */
obj_t BGl_z62zc3z04anonymousza31245ze3ze5zz__socketz00(obj_t BgL_envz00_2223, obj_t BgL_xz00_2224)
{
{ /* Llib/socket.scm 292 */
socket_cleanup(); BUNSPEC; 
return BgL_xz00_2224;} 

}



/* socket? */
BGL_EXPORTED_DEF bool_t BGl_socketzf3zf3zz__socketz00(obj_t BgL_objz00_3)
{
{ /* Llib/socket.scm 302 */
return 
SOCKETP(BgL_objz00_3);} 

}



/* &socket? */
obj_t BGl_z62socketzf3z91zz__socketz00(obj_t BgL_envz00_2225, obj_t BgL_objz00_2226)
{
{ /* Llib/socket.scm 302 */
return 
BBOOL(
BGl_socketzf3zf3zz__socketz00(BgL_objz00_2226));} 

}



/* socket-server? */
BGL_EXPORTED_DEF bool_t BGl_socketzd2serverzf3z21zz__socketz00(obj_t BgL_objz00_4)
{
{ /* Llib/socket.scm 308 */
return 
BGL_SOCKET_SERVERP(BgL_objz00_4);} 

}



/* &socket-server? */
obj_t BGl_z62socketzd2serverzf3z43zz__socketz00(obj_t BgL_envz00_2227, obj_t BgL_objz00_2228)
{
{ /* Llib/socket.scm 308 */
return 
BBOOL(
BGl_socketzd2serverzf3z21zz__socketz00(BgL_objz00_2228));} 

}



/* socket-client? */
BGL_EXPORTED_DEF bool_t BGl_socketzd2clientzf3z21zz__socketz00(obj_t BgL_objz00_5)
{
{ /* Llib/socket.scm 314 */
return 
BGL_SOCKET_CLIENTP(BgL_objz00_5);} 

}



/* &socket-client? */
obj_t BGl_z62socketzd2clientzf3z43zz__socketz00(obj_t BgL_envz00_2229, obj_t BgL_objz00_2230)
{
{ /* Llib/socket.scm 314 */
return 
BBOOL(
BGl_socketzd2clientzf3z21zz__socketz00(BgL_objz00_2230));} 

}



/* socket-hostname */
BGL_EXPORTED_DEF obj_t BGl_socketzd2hostnamezd2zz__socketz00(obj_t BgL_socketz00_6)
{
{ /* Llib/socket.scm 320 */
return 
SOCKET_HOSTNAME(BgL_socketz00_6);} 

}



/* &socket-hostname */
obj_t BGl_z62socketzd2hostnamezb0zz__socketz00(obj_t BgL_envz00_2231, obj_t BgL_socketz00_2232)
{
{ /* Llib/socket.scm 320 */
{ /* Llib/socket.scm 321 */
 obj_t BgL_auxz00_2699;
if(
SOCKETP(BgL_socketz00_2232))
{ /* Llib/socket.scm 321 */
BgL_auxz00_2699 = BgL_socketz00_2232
; }  else 
{ 
 obj_t BgL_auxz00_2702;
BgL_auxz00_2702 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(14228L), BGl_string2026z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2232); 
FAILURE(BgL_auxz00_2702,BFALSE,BFALSE);} 
return 
BGl_socketzd2hostnamezd2zz__socketz00(BgL_auxz00_2699);} } 

}



/* socket-host-address */
BGL_EXPORTED_DEF obj_t BGl_socketzd2hostzd2addressz00zz__socketz00(obj_t BgL_socketz00_7)
{
{ /* Llib/socket.scm 326 */
BGL_TAIL return 
bgl_socket_host_addr(BgL_socketz00_7);} 

}



/* &socket-host-address */
obj_t BGl_z62socketzd2hostzd2addressz62zz__socketz00(obj_t BgL_envz00_2233, obj_t BgL_socketz00_2234)
{
{ /* Llib/socket.scm 326 */
{ /* Llib/socket.scm 327 */
 obj_t BgL_auxz00_2708;
if(
SOCKETP(BgL_socketz00_2234))
{ /* Llib/socket.scm 327 */
BgL_auxz00_2708 = BgL_socketz00_2234
; }  else 
{ 
 obj_t BgL_auxz00_2711;
BgL_auxz00_2711 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(14526L), BGl_string2027z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2234); 
FAILURE(BgL_auxz00_2711,BFALSE,BFALSE);} 
return 
BGl_socketzd2hostzd2addressz00zz__socketz00(BgL_auxz00_2708);} } 

}



/* socket-local-address */
BGL_EXPORTED_DEF obj_t BGl_socketzd2localzd2addressz00zz__socketz00(obj_t BgL_socketz00_8)
{
{ /* Llib/socket.scm 332 */
BGL_TAIL return 
bgl_socket_local_addr(BgL_socketz00_8);} 

}



/* &socket-local-address */
obj_t BGl_z62socketzd2localzd2addressz62zz__socketz00(obj_t BgL_envz00_2235, obj_t BgL_socketz00_2236)
{
{ /* Llib/socket.scm 332 */
{ /* Llib/socket.scm 333 */
 obj_t BgL_auxz00_2717;
if(
SOCKETP(BgL_socketz00_2236))
{ /* Llib/socket.scm 333 */
BgL_auxz00_2717 = BgL_socketz00_2236
; }  else 
{ 
 obj_t BgL_auxz00_2720;
BgL_auxz00_2720 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(14833L), BGl_string2028z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2236); 
FAILURE(BgL_auxz00_2720,BFALSE,BFALSE);} 
return 
BGl_socketzd2localzd2addressz00zz__socketz00(BgL_auxz00_2717);} } 

}



/* socket-local? */
BGL_EXPORTED_DEF bool_t BGl_socketzd2localzf3z21zz__socketz00(obj_t BgL_socketz00_9)
{
{ /* Llib/socket.scm 338 */
BGL_TAIL return 
bgl_socket_localp(BgL_socketz00_9);} 

}



/* &socket-local? */
obj_t BGl_z62socketzd2localzf3z43zz__socketz00(obj_t BgL_envz00_2237, obj_t BgL_socketz00_2238)
{
{ /* Llib/socket.scm 338 */
{ /* Llib/socket.scm 341 */
 bool_t BgL_tmpz00_2726;
{ /* Llib/socket.scm 341 */
 obj_t BgL_auxz00_2727;
if(
SOCKETP(BgL_socketz00_2238))
{ /* Llib/socket.scm 341 */
BgL_auxz00_2727 = BgL_socketz00_2238
; }  else 
{ 
 obj_t BgL_auxz00_2730;
BgL_auxz00_2730 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(15170L), BGl_string2029z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2238); 
FAILURE(BgL_auxz00_2730,BFALSE,BFALSE);} 
BgL_tmpz00_2726 = 
BGl_socketzd2localzf3z21zz__socketz00(BgL_auxz00_2727); } 
return 
BBOOL(BgL_tmpz00_2726);} } 

}



/* socket-host-address=? */
BGL_EXPORTED_DEF bool_t BGl_socketzd2hostzd2addresszd3zf3z20zz__socketz00(obj_t BgL_socketz00_10, obj_t BgL_addrz00_11)
{
{ /* Llib/socket.scm 348 */
BGL_TAIL return 
bgl_socket_host_addr_cmp(BgL_socketz00_10, BgL_addrz00_11);} 

}



/* &socket-host-address=? */
obj_t BGl_z62socketzd2hostzd2addresszd3zf3z42zz__socketz00(obj_t BgL_envz00_2239, obj_t BgL_socketz00_2240, obj_t BgL_addrz00_2241)
{
{ /* Llib/socket.scm 348 */
{ /* Llib/socket.scm 351 */
 bool_t BgL_tmpz00_2737;
{ /* Llib/socket.scm 351 */
 obj_t BgL_auxz00_2745; obj_t BgL_auxz00_2738;
if(
STRINGP(BgL_addrz00_2241))
{ /* Llib/socket.scm 351 */
BgL_auxz00_2745 = BgL_addrz00_2241
; }  else 
{ 
 obj_t BgL_auxz00_2748;
BgL_auxz00_2748 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(15624L), BGl_string2030z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_addrz00_2241); 
FAILURE(BgL_auxz00_2748,BFALSE,BFALSE);} 
if(
SOCKETP(BgL_socketz00_2240))
{ /* Llib/socket.scm 351 */
BgL_auxz00_2738 = BgL_socketz00_2240
; }  else 
{ 
 obj_t BgL_auxz00_2741;
BgL_auxz00_2741 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(15624L), BGl_string2030z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2240); 
FAILURE(BgL_auxz00_2741,BFALSE,BFALSE);} 
BgL_tmpz00_2737 = 
BGl_socketzd2hostzd2addresszd3zf3z20zz__socketz00(BgL_auxz00_2738, BgL_auxz00_2745); } 
return 
BBOOL(BgL_tmpz00_2737);} } 

}



/* socket-down? */
BGL_EXPORTED_DEF bool_t BGl_socketzd2downzf3z21zz__socketz00(obj_t BgL_socketz00_12)
{
{ /* Llib/socket.scm 358 */
return 
SOCKET_DOWNP(BgL_socketz00_12);} 

}



/* &socket-down? */
obj_t BGl_z62socketzd2downzf3z43zz__socketz00(obj_t BgL_envz00_2242, obj_t BgL_socketz00_2243)
{
{ /* Llib/socket.scm 358 */
{ /* Llib/socket.scm 359 */
 bool_t BgL_tmpz00_2755;
{ /* Llib/socket.scm 359 */
 obj_t BgL_auxz00_2756;
if(
SOCKETP(BgL_socketz00_2243))
{ /* Llib/socket.scm 359 */
BgL_auxz00_2756 = BgL_socketz00_2243
; }  else 
{ 
 obj_t BgL_auxz00_2759;
BgL_auxz00_2759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(16009L), BGl_string2032z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2243); 
FAILURE(BgL_auxz00_2759,BFALSE,BFALSE);} 
BgL_tmpz00_2755 = 
BGl_socketzd2downzf3z21zz__socketz00(BgL_auxz00_2756); } 
return 
BBOOL(BgL_tmpz00_2755);} } 

}



/* socket-port-number */
BGL_EXPORTED_DEF obj_t BGl_socketzd2portzd2numberz00zz__socketz00(obj_t BgL_socketz00_13)
{
{ /* Llib/socket.scm 364 */
return 
BINT(
SOCKET_PORT(BgL_socketz00_13));} 

}



/* &socket-port-number */
obj_t BGl_z62socketzd2portzd2numberz62zz__socketz00(obj_t BgL_envz00_2244, obj_t BgL_socketz00_2245)
{
{ /* Llib/socket.scm 364 */
{ /* Llib/socket.scm 365 */
 obj_t BgL_auxz00_2767;
if(
SOCKETP(BgL_socketz00_2245))
{ /* Llib/socket.scm 365 */
BgL_auxz00_2767 = BgL_socketz00_2245
; }  else 
{ 
 obj_t BgL_auxz00_2770;
BgL_auxz00_2770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(16317L), BGl_string2033z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2245); 
FAILURE(BgL_auxz00_2770,BFALSE,BFALSE);} 
return 
BGl_socketzd2portzd2numberz00zz__socketz00(BgL_auxz00_2767);} } 

}



/* socket-input */
BGL_EXPORTED_DEF obj_t BGl_socketzd2inputzd2zz__socketz00(obj_t BgL_socketz00_14)
{
{ /* Llib/socket.scm 370 */
return 
SOCKET_INPUT(BgL_socketz00_14);} 

}



/* &socket-input */
obj_t BGl_z62socketzd2inputzb0zz__socketz00(obj_t BgL_envz00_2246, obj_t BgL_socketz00_2247)
{
{ /* Llib/socket.scm 370 */
{ /* Llib/socket.scm 371 */
 obj_t BgL_auxz00_2776;
if(
SOCKETP(BgL_socketz00_2247))
{ /* Llib/socket.scm 371 */
BgL_auxz00_2776 = BgL_socketz00_2247
; }  else 
{ 
 obj_t BgL_auxz00_2779;
BgL_auxz00_2779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(16619L), BGl_string2034z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2247); 
FAILURE(BgL_auxz00_2779,BFALSE,BFALSE);} 
return 
BGl_socketzd2inputzd2zz__socketz00(BgL_auxz00_2776);} } 

}



/* socket-output */
BGL_EXPORTED_DEF obj_t BGl_socketzd2outputzd2zz__socketz00(obj_t BgL_socketz00_15)
{
{ /* Llib/socket.scm 376 */
return 
SOCKET_OUTPUT(BgL_socketz00_15);} 

}



/* &socket-output */
obj_t BGl_z62socketzd2outputzb0zz__socketz00(obj_t BgL_envz00_2248, obj_t BgL_socketz00_2249)
{
{ /* Llib/socket.scm 376 */
{ /* Llib/socket.scm 377 */
 obj_t BgL_auxz00_2785;
if(
SOCKETP(BgL_socketz00_2249))
{ /* Llib/socket.scm 377 */
BgL_auxz00_2785 = BgL_socketz00_2249
; }  else 
{ 
 obj_t BgL_auxz00_2788;
BgL_auxz00_2788 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(16916L), BGl_string2035z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2249); 
FAILURE(BgL_auxz00_2788,BFALSE,BFALSE);} 
return 
BGl_socketzd2outputzd2zz__socketz00(BgL_auxz00_2785);} } 

}



/* _make-client-socket */
obj_t BGl__makezd2clientzd2socketz00zz__socketz00(obj_t BgL_env1115z00_23, obj_t BgL_opt1114z00_22)
{
{ /* Llib/socket.scm 382 */
{ /* Llib/socket.scm 382 */
 obj_t BgL_g1124z00_1205; obj_t BgL_g1125z00_1206;
BgL_g1124z00_1205 = 
VECTOR_REF(BgL_opt1114z00_22,0L); 
BgL_g1125z00_1206 = 
VECTOR_REF(BgL_opt1114z00_22,1L); 
{ /* Llib/socket.scm 382 */
 obj_t BgL_domainz00_1207;
BgL_domainz00_1207 = BGl_symbol2036z00zz__socketz00; 
{ /* Llib/socket.scm 382 */
 obj_t BgL_inbufz00_1208;
BgL_inbufz00_1208 = BTRUE; 
{ /* Llib/socket.scm 382 */
 obj_t BgL_outbufz00_1209;
BgL_outbufz00_1209 = BTRUE; 
{ /* Llib/socket.scm 382 */
 obj_t BgL_timeoutz00_1210;
BgL_timeoutz00_1210 = 
BINT(0L); 
{ /* Llib/socket.scm 382 */

{ 
 long BgL_iz00_1211;
BgL_iz00_1211 = 2L; 
BgL_check1118z00_1212:
if(
(BgL_iz00_1211==
VECTOR_LENGTH(BgL_opt1114z00_22)))
{ /* Llib/socket.scm 382 */BNIL; }  else 
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2303z00_2799;
{ /* Llib/socket.scm 382 */
 obj_t BgL_arg1272z00_1218;
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2304z00_2800;
{ /* Llib/socket.scm 382 */
 long BgL_tmpz00_2801;
BgL_tmpz00_2801 = 
VECTOR_LENGTH(BgL_opt1114z00_22); 
BgL_test2304z00_2800 = 
BOUND_CHECK(BgL_iz00_1211, BgL_tmpz00_2801); } 
if(BgL_test2304z00_2800)
{ /* Llib/socket.scm 382 */
BgL_arg1272z00_1218 = 
VECTOR_REF(BgL_opt1114z00_22,BgL_iz00_1211); }  else 
{ 
 obj_t BgL_auxz00_2805;
BgL_auxz00_2805 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2038z00zz__socketz00, BgL_opt1114z00_22, 
(int)(
VECTOR_LENGTH(BgL_opt1114z00_22)), 
(int)(BgL_iz00_1211)); 
FAILURE(BgL_auxz00_2805,BFALSE,BFALSE);} } 
BgL_test2303z00_2799 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1272z00_1218, BGl_list2039z00zz__socketz00)); } 
if(BgL_test2303z00_2799)
{ 
 long BgL_iz00_2814;
BgL_iz00_2814 = 
(BgL_iz00_1211+2L); 
BgL_iz00_1211 = BgL_iz00_2814; 
goto BgL_check1118z00_1212;}  else 
{ /* Llib/socket.scm 382 */
 obj_t BgL_arg1268z00_1217;
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2305z00_2816;
{ /* Llib/socket.scm 382 */
 long BgL_tmpz00_2817;
BgL_tmpz00_2817 = 
VECTOR_LENGTH(BgL_opt1114z00_22); 
BgL_test2305z00_2816 = 
BOUND_CHECK(BgL_iz00_1211, BgL_tmpz00_2817); } 
if(BgL_test2305z00_2816)
{ /* Llib/socket.scm 382 */
BgL_arg1268z00_1217 = 
VECTOR_REF(BgL_opt1114z00_22,BgL_iz00_1211); }  else 
{ 
 obj_t BgL_auxz00_2821;
BgL_auxz00_2821 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2038z00zz__socketz00, BgL_opt1114z00_22, 
(int)(
VECTOR_LENGTH(BgL_opt1114z00_22)), 
(int)(BgL_iz00_1211)); 
FAILURE(BgL_auxz00_2821,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol2048z00zz__socketz00, BGl_string2050z00zz__socketz00, BgL_arg1268z00_1217); } } } 
{ /* Llib/socket.scm 382 */
 obj_t BgL_index1120z00_1219;
BgL_index1120z00_1219 = 
BGl_search1117ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1114z00_22), BgL_opt1114z00_22, BGl_keyword2040z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2306z00_2831;
{ /* Llib/socket.scm 382 */
 long BgL_n1z00_1878;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2832;
if(
INTEGERP(BgL_index1120z00_1219))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2832 = BgL_index1120z00_1219
; }  else 
{ 
 obj_t BgL_auxz00_2835;
BgL_auxz00_2835 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1120z00_1219); 
FAILURE(BgL_auxz00_2835,BFALSE,BFALSE);} 
BgL_n1z00_1878 = 
(long)CINT(BgL_tmpz00_2832); } 
BgL_test2306z00_2831 = 
(BgL_n1z00_1878>=0L); } 
if(BgL_test2306z00_2831)
{ 
 long BgL_auxz00_2841;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2842;
if(
INTEGERP(BgL_index1120z00_1219))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2842 = BgL_index1120z00_1219
; }  else 
{ 
 obj_t BgL_auxz00_2845;
BgL_auxz00_2845 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1120z00_1219); 
FAILURE(BgL_auxz00_2845,BFALSE,BFALSE);} 
BgL_auxz00_2841 = 
(long)CINT(BgL_tmpz00_2842); } 
BgL_domainz00_1207 = 
VECTOR_REF(BgL_opt1114z00_22,BgL_auxz00_2841); }  else 
{ /* Llib/socket.scm 382 */BFALSE; } } } 
{ /* Llib/socket.scm 382 */
 obj_t BgL_index1121z00_1221;
BgL_index1121z00_1221 = 
BGl_search1117ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1114z00_22), BgL_opt1114z00_22, BGl_keyword2042z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2309z00_2853;
{ /* Llib/socket.scm 382 */
 long BgL_n1z00_1879;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2854;
if(
INTEGERP(BgL_index1121z00_1221))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2854 = BgL_index1121z00_1221
; }  else 
{ 
 obj_t BgL_auxz00_2857;
BgL_auxz00_2857 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1121z00_1221); 
FAILURE(BgL_auxz00_2857,BFALSE,BFALSE);} 
BgL_n1z00_1879 = 
(long)CINT(BgL_tmpz00_2854); } 
BgL_test2309z00_2853 = 
(BgL_n1z00_1879>=0L); } 
if(BgL_test2309z00_2853)
{ 
 long BgL_auxz00_2863;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2864;
if(
INTEGERP(BgL_index1121z00_1221))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2864 = BgL_index1121z00_1221
; }  else 
{ 
 obj_t BgL_auxz00_2867;
BgL_auxz00_2867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1121z00_1221); 
FAILURE(BgL_auxz00_2867,BFALSE,BFALSE);} 
BgL_auxz00_2863 = 
(long)CINT(BgL_tmpz00_2864); } 
BgL_inbufz00_1208 = 
VECTOR_REF(BgL_opt1114z00_22,BgL_auxz00_2863); }  else 
{ /* Llib/socket.scm 382 */BFALSE; } } } 
{ /* Llib/socket.scm 382 */
 obj_t BgL_index1122z00_1223;
BgL_index1122z00_1223 = 
BGl_search1117ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1114z00_22), BgL_opt1114z00_22, BGl_keyword2044z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2312z00_2875;
{ /* Llib/socket.scm 382 */
 long BgL_n1z00_1880;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2876;
if(
INTEGERP(BgL_index1122z00_1223))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2876 = BgL_index1122z00_1223
; }  else 
{ 
 obj_t BgL_auxz00_2879;
BgL_auxz00_2879 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1122z00_1223); 
FAILURE(BgL_auxz00_2879,BFALSE,BFALSE);} 
BgL_n1z00_1880 = 
(long)CINT(BgL_tmpz00_2876); } 
BgL_test2312z00_2875 = 
(BgL_n1z00_1880>=0L); } 
if(BgL_test2312z00_2875)
{ 
 long BgL_auxz00_2885;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2886;
if(
INTEGERP(BgL_index1122z00_1223))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2886 = BgL_index1122z00_1223
; }  else 
{ 
 obj_t BgL_auxz00_2889;
BgL_auxz00_2889 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1122z00_1223); 
FAILURE(BgL_auxz00_2889,BFALSE,BFALSE);} 
BgL_auxz00_2885 = 
(long)CINT(BgL_tmpz00_2886); } 
BgL_outbufz00_1209 = 
VECTOR_REF(BgL_opt1114z00_22,BgL_auxz00_2885); }  else 
{ /* Llib/socket.scm 382 */BFALSE; } } } 
{ /* Llib/socket.scm 382 */
 obj_t BgL_index1123z00_1225;
BgL_index1123z00_1225 = 
BGl_search1117ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1114z00_22), BgL_opt1114z00_22, BGl_keyword2046z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 382 */
 bool_t BgL_test2315z00_2897;
{ /* Llib/socket.scm 382 */
 long BgL_n1z00_1881;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2898;
if(
INTEGERP(BgL_index1123z00_1225))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2898 = BgL_index1123z00_1225
; }  else 
{ 
 obj_t BgL_auxz00_2901;
BgL_auxz00_2901 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1123z00_1225); 
FAILURE(BgL_auxz00_2901,BFALSE,BFALSE);} 
BgL_n1z00_1881 = 
(long)CINT(BgL_tmpz00_2898); } 
BgL_test2315z00_2897 = 
(BgL_n1z00_1881>=0L); } 
if(BgL_test2315z00_2897)
{ 
 long BgL_auxz00_2907;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2908;
if(
INTEGERP(BgL_index1123z00_1225))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2908 = BgL_index1123z00_1225
; }  else 
{ 
 obj_t BgL_auxz00_2911;
BgL_auxz00_2911 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1123z00_1225); 
FAILURE(BgL_auxz00_2911,BFALSE,BFALSE);} 
BgL_auxz00_2907 = 
(long)CINT(BgL_tmpz00_2908); } 
BgL_timeoutz00_1210 = 
VECTOR_REF(BgL_opt1114z00_22,BgL_auxz00_2907); }  else 
{ /* Llib/socket.scm 382 */BFALSE; } } } 
{ /* Llib/socket.scm 382 */
 obj_t BgL_arg1284z00_1227; obj_t BgL_arg1304z00_1228;
BgL_arg1284z00_1227 = 
VECTOR_REF(BgL_opt1114z00_22,0L); 
BgL_arg1304z00_1228 = 
VECTOR_REF(BgL_opt1114z00_22,1L); 
{ /* Llib/socket.scm 382 */
 obj_t BgL_domainz00_1229;
BgL_domainz00_1229 = BgL_domainz00_1207; 
{ /* Llib/socket.scm 382 */
 obj_t BgL_inbufz00_1230;
BgL_inbufz00_1230 = BgL_inbufz00_1208; 
{ /* Llib/socket.scm 382 */
 obj_t BgL_outbufz00_1231;
BgL_outbufz00_1231 = BgL_outbufz00_1209; 
{ /* Llib/socket.scm 382 */
 obj_t BgL_timeoutz00_1232;
BgL_timeoutz00_1232 = BgL_timeoutz00_1210; 
{ /* Llib/socket.scm 382 */
 int BgL_auxz00_2926; obj_t BgL_auxz00_2919;
{ /* Llib/socket.scm 382 */
 obj_t BgL_tmpz00_2927;
if(
INTEGERP(BgL_arg1304z00_1228))
{ /* Llib/socket.scm 382 */
BgL_tmpz00_2927 = BgL_arg1304z00_1228
; }  else 
{ 
 obj_t BgL_auxz00_2930;
BgL_auxz00_2930 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_arg1304z00_1228); 
FAILURE(BgL_auxz00_2930,BFALSE,BFALSE);} 
BgL_auxz00_2926 = 
CINT(BgL_tmpz00_2927); } 
if(
STRINGP(BgL_arg1284z00_1227))
{ /* Llib/socket.scm 382 */
BgL_auxz00_2919 = BgL_arg1284z00_1227
; }  else 
{ 
 obj_t BgL_auxz00_2922;
BgL_auxz00_2922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17165L), BGl_string2051z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_arg1284z00_1227); 
FAILURE(BgL_auxz00_2922,BFALSE,BFALSE);} 
return 
BGl_makezd2clientzd2socketz00zz__socketz00(BgL_auxz00_2919, BgL_auxz00_2926, BgL_domainz00_1229, BgL_inbufz00_1230, BgL_outbufz00_1231, BgL_timeoutz00_1232);} } } } } } } } } } } } } 

}



/* search1117~0 */
obj_t BGl_search1117ze70ze7zz__socketz00(long BgL_l1116z00_2315, obj_t BgL_opt1114z00_2314, obj_t BgL_k1z00_1202, long BgL_iz00_1203)
{
{ /* Llib/socket.scm 382 */
BGl_search1117ze70ze7zz__socketz00:
if(
(BgL_iz00_1203==BgL_l1116z00_2315))
{ /* Llib/socket.scm 382 */
return 
BINT(-1L);}  else 
{ /* Llib/socket.scm 382 */
if(
(BgL_iz00_1203==
(BgL_l1116z00_2315-1L)))
{ /* Llib/socket.scm 382 */
return 
BGl_errorz00zz__errorz00(BGl_symbol2048z00zz__socketz00, BGl_string2053z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1114z00_2314)));}  else 
{ /* Llib/socket.scm 382 */
 obj_t BgL_vz00_1237;
BgL_vz00_1237 = 
VECTOR_REF(BgL_opt1114z00_2314,BgL_iz00_1203); 
if(
(BgL_vz00_1237==BgL_k1z00_1202))
{ /* Llib/socket.scm 382 */
return 
BINT(
(BgL_iz00_1203+1L));}  else 
{ 
 long BgL_iz00_2950;
BgL_iz00_2950 = 
(BgL_iz00_1203+2L); 
BgL_iz00_1203 = BgL_iz00_2950; 
goto BGl_search1117ze70ze7zz__socketz00;} } } } 

}



/* make-client-socket */
BGL_EXPORTED_DEF obj_t BGl_makezd2clientzd2socketz00zz__socketz00(obj_t BgL_hostz00_16, int BgL_portz00_17, obj_t BgL_domainz00_18, obj_t BgL_inbufz00_19, obj_t BgL_outbufz00_20, obj_t BgL_timeoutz00_21)
{
{ /* Llib/socket.scm 382 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 384 */
 obj_t BgL_inbufz00_1240; obj_t BgL_outbufz00_1241;
BgL_inbufz00_1240 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2054z00zz__socketz00, BgL_inbufz00_19, 
(int)(512L)); 
BgL_outbufz00_1241 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2054z00zz__socketz00, BgL_outbufz00_20, 
(int)(1024L)); 
{ /* Llib/socket.scm 386 */
 bool_t BgL_test2323z00_2957;
{ /* Llib/socket.scm 386 */
 bool_t BgL__ortest_1040z00_1249;
BgL__ortest_1040z00_1249 = 
(BgL_domainz00_18==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1040z00_1249)
{ /* Llib/socket.scm 386 */
BgL_test2323z00_2957 = BgL__ortest_1040z00_1249
; }  else 
{ /* Llib/socket.scm 386 */
 bool_t BgL__ortest_1041z00_1250;
BgL__ortest_1041z00_1250 = 
(BgL_domainz00_18==BGl_symbol2055z00zz__socketz00); 
if(BgL__ortest_1041z00_1250)
{ /* Llib/socket.scm 386 */
BgL_test2323z00_2957 = BgL__ortest_1041z00_1250
; }  else 
{ /* Llib/socket.scm 386 */
BgL_test2323z00_2957 = 
(BgL_domainz00_18==BGl_symbol2057z00zz__socketz00)
; } } } 
if(BgL_test2323z00_2957)
{ /* Llib/socket.scm 388 */
 obj_t BgL_auxz00_2972; int BgL_tmpz00_2963;
if(
SYMBOLP(BgL_domainz00_18))
{ /* Llib/socket.scm 388 */
BgL_auxz00_2972 = BgL_domainz00_18
; }  else 
{ 
 obj_t BgL_auxz00_2975;
BgL_auxz00_2975 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17513L), BGl_string2054z00zz__socketz00, BGl_string2059z00zz__socketz00, BgL_domainz00_18); 
FAILURE(BgL_auxz00_2975,BFALSE,BFALSE);} 
{ /* Llib/socket.scm 388 */
 obj_t BgL_tmpz00_2964;
if(
INTEGERP(BgL_timeoutz00_21))
{ /* Llib/socket.scm 388 */
BgL_tmpz00_2964 = BgL_timeoutz00_21
; }  else 
{ 
 obj_t BgL_auxz00_2967;
BgL_auxz00_2967 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17492L), BGl_string2054z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_timeoutz00_21); 
FAILURE(BgL_auxz00_2967,BFALSE,BFALSE);} 
BgL_tmpz00_2963 = 
CINT(BgL_tmpz00_2964); } 
return 
bgl_make_client_socket(BgL_hostz00_16, BgL_portz00_17, BgL_tmpz00_2963, BgL_inbufz00_1240, BgL_outbufz00_1241, BgL_auxz00_2972);}  else 
{ /* Llib/socket.scm 386 */
 bool_t BgL_test2328z00_2980;
{ /* Llib/socket.scm 386 */
 bool_t BgL__ortest_1042z00_1248;
BgL__ortest_1042z00_1248 = 
(BgL_domainz00_18==BGl_symbol2060z00zz__socketz00); 
if(BgL__ortest_1042z00_1248)
{ /* Llib/socket.scm 386 */
BgL_test2328z00_2980 = BgL__ortest_1042z00_1248
; }  else 
{ /* Llib/socket.scm 386 */
BgL_test2328z00_2980 = 
(BgL_domainz00_18==BGl_symbol2062z00zz__socketz00)
; } } 
if(BgL_test2328z00_2980)
{ /* Llib/socket.scm 391 */
 int BgL_tmpz00_2984;
{ /* Llib/socket.scm 391 */
 obj_t BgL_tmpz00_2985;
if(
INTEGERP(BgL_timeoutz00_21))
{ /* Llib/socket.scm 391 */
BgL_tmpz00_2985 = BgL_timeoutz00_21
; }  else 
{ 
 obj_t BgL_auxz00_2988;
BgL_auxz00_2988 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17601L), BGl_string2054z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_timeoutz00_21); 
FAILURE(BgL_auxz00_2988,BFALSE,BFALSE);} 
BgL_tmpz00_2984 = 
CINT(BgL_tmpz00_2985); } 
return 
bgl_make_client_unix_socket(BgL_hostz00_16, BgL_tmpz00_2984, BgL_inbufz00_1240, BgL_outbufz00_1241);}  else 
{ /* Llib/socket.scm 394 */
 obj_t BgL_aux1871z00_2383;
BgL_aux1871z00_2383 = 
BGl_errorz00zz__errorz00(BGl_string2054z00zz__socketz00, BGl_string2064z00zz__socketz00, BgL_domainz00_18); 
if(
SOCKETP(BgL_aux1871z00_2383))
{ /* Llib/socket.scm 394 */
return BgL_aux1871z00_2383;}  else 
{ 
 obj_t BgL_auxz00_2997;
BgL_auxz00_2997 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(17707L), BGl_string2054z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_aux1871z00_2383); 
FAILURE(BgL_auxz00_2997,BFALSE,BFALSE);} } } } } } 

}



/* make-server-socket */
BGL_EXPORTED_DEF obj_t BGl_makezd2serverzd2socketz00zz__socketz00(obj_t BgL_portz00_24)
{
{ /* Llib/socket.scm 399 */
{ /* Llib/socket.scm 228 */
 obj_t BgL_dsssl1126z00_1251;
BgL_dsssl1126z00_1251 = BgL_portz00_24; 
{ /* Llib/socket.scm 228 */
 obj_t BgL_portz00_1252;
{ /* Llib/socket.scm 228 */
 bool_t BgL_test2332z00_3001;
if(
NULLP(BgL_dsssl1126z00_1251))
{ /* Llib/socket.scm 228 */
BgL_test2332z00_3001 = ((bool_t)1)
; }  else 
{ /* Llib/socket.scm 228 */
 obj_t BgL_tmpz00_3004;
{ /* Llib/socket.scm 228 */
 obj_t BgL_auxz00_3005;
{ /* Llib/socket.scm 228 */
 obj_t BgL_pairz00_1883;
{ /* Llib/socket.scm 228 */
 obj_t BgL_aux1873z00_2385;
BgL_aux1873z00_2385 = BgL_dsssl1126z00_1251; 
if(
PAIRP(BgL_aux1873z00_2385))
{ /* Llib/socket.scm 228 */
BgL_pairz00_1883 = BgL_aux1873z00_2385; }  else 
{ 
 obj_t BgL_auxz00_3008;
BgL_auxz00_3008 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(9891L), BGl_string2065z00zz__socketz00, BGl_string2066z00zz__socketz00, BgL_aux1873z00_2385); 
FAILURE(BgL_auxz00_3008,BFALSE,BFALSE);} } 
BgL_auxz00_3005 = 
CAR(BgL_pairz00_1883); } 
BgL_tmpz00_3004 = 
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_3005, BGl_list2067z00zz__socketz00); } 
BgL_test2332z00_3001 = 
CBOOL(BgL_tmpz00_3004); } 
if(BgL_test2332z00_3001)
{ /* Llib/socket.scm 228 */
BgL_portz00_1252 = 
BINT(0L); }  else 
{ /* Llib/socket.scm 228 */
 obj_t BgL_tmp1127z00_1279;
{ /* Llib/socket.scm 228 */
 obj_t BgL_pairz00_1884;
{ /* Llib/socket.scm 228 */
 obj_t BgL_aux1875z00_2387;
BgL_aux1875z00_2387 = BgL_dsssl1126z00_1251; 
if(
PAIRP(BgL_aux1875z00_2387))
{ /* Llib/socket.scm 228 */
BgL_pairz00_1884 = BgL_aux1875z00_2387; }  else 
{ 
 obj_t BgL_auxz00_3018;
BgL_auxz00_3018 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(9891L), BGl_string2065z00zz__socketz00, BGl_string2066z00zz__socketz00, BgL_aux1875z00_2387); 
FAILURE(BgL_auxz00_3018,BFALSE,BFALSE);} } 
BgL_tmp1127z00_1279 = 
CAR(BgL_pairz00_1884); } 
{ /* Llib/socket.scm 228 */
 obj_t BgL_pairz00_1885;
{ /* Llib/socket.scm 228 */
 obj_t BgL_aux1877z00_2389;
BgL_aux1877z00_2389 = BgL_dsssl1126z00_1251; 
if(
PAIRP(BgL_aux1877z00_2389))
{ /* Llib/socket.scm 228 */
BgL_pairz00_1885 = BgL_aux1877z00_2389; }  else 
{ 
 obj_t BgL_auxz00_3025;
BgL_auxz00_3025 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(9891L), BGl_string2065z00zz__socketz00, BGl_string2066z00zz__socketz00, BgL_aux1877z00_2389); 
FAILURE(BgL_auxz00_3025,BFALSE,BFALSE);} } 
BgL_dsssl1126z00_1251 = 
CDR(BgL_pairz00_1885); } 
BgL_portz00_1252 = BgL_tmp1127z00_1279; } } 
{ /* Llib/socket.scm 228 */
 obj_t BgL_namez00_1253;
BgL_namez00_1253 = 
BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(BgL_dsssl1126z00_1251, BGl_keyword2070z00zz__socketz00, BFALSE); 
{ /* Llib/socket.scm 228 */
 obj_t BgL_backlogz00_1254;
BgL_backlogz00_1254 = 
BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(BgL_dsssl1126z00_1251, BGl_keyword2068z00zz__socketz00, 
BINT(5L)); 
{ /* Llib/socket.scm 228 */
 obj_t BgL_domainz00_1255;
BgL_domainz00_1255 = 
BGl_dssslzd2getzd2keyzd2argzd2zz__dssslz00(BgL_dsssl1126z00_1251, BGl_keyword2040z00zz__socketz00, BGl_symbol2036z00zz__socketz00); 
if(
NULLP(
BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(BgL_dsssl1126z00_1251, BGl_list2067z00zz__socketz00)))
{ /* Llib/socket.scm 228 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 401 */
 bool_t BgL_test2338z00_3038;
{ /* Llib/socket.scm 401 */
 bool_t BgL__ortest_1045z00_1264;
BgL__ortest_1045z00_1264 = 
(BgL_domainz00_1255==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1045z00_1264)
{ /* Llib/socket.scm 401 */
BgL_test2338z00_3038 = BgL__ortest_1045z00_1264
; }  else 
{ /* Llib/socket.scm 401 */
BgL_test2338z00_3038 = 
(BgL_domainz00_1255==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2338z00_3038)
{ /* Llib/socket.scm 403 */
 int BgL_auxz00_3051; int BgL_tmpz00_3042;
{ /* Llib/socket.scm 403 */
 obj_t BgL_tmpz00_3052;
if(
INTEGERP(BgL_backlogz00_1254))
{ /* Llib/socket.scm 403 */
BgL_tmpz00_3052 = BgL_backlogz00_1254
; }  else 
{ 
 obj_t BgL_auxz00_3055;
BgL_auxz00_3055 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18187L), BGl_string2065z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_backlogz00_1254); 
FAILURE(BgL_auxz00_3055,BFALSE,BFALSE);} 
BgL_auxz00_3051 = 
CINT(BgL_tmpz00_3052); } 
{ /* Llib/socket.scm 403 */
 obj_t BgL_tmpz00_3043;
if(
INTEGERP(BgL_portz00_1252))
{ /* Llib/socket.scm 403 */
BgL_tmpz00_3043 = BgL_portz00_1252
; }  else 
{ 
 obj_t BgL_auxz00_3046;
BgL_auxz00_3046 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18182L), BGl_string2065z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_portz00_1252); 
FAILURE(BgL_auxz00_3046,BFALSE,BFALSE);} 
BgL_tmpz00_3042 = 
CINT(BgL_tmpz00_3043); } 
return 
bgl_make_server_socket(BgL_namez00_1253, BgL_tmpz00_3042, BgL_auxz00_3051, BgL_domainz00_1255);}  else 
{ /* Llib/socket.scm 401 */
 bool_t BgL_test2342z00_3061;
{ /* Llib/socket.scm 401 */
 bool_t BgL__ortest_1046z00_1263;
BgL__ortest_1046z00_1263 = 
(BgL_domainz00_1255==BGl_symbol2060z00zz__socketz00); 
if(BgL__ortest_1046z00_1263)
{ /* Llib/socket.scm 401 */
BgL_test2342z00_3061 = BgL__ortest_1046z00_1263
; }  else 
{ /* Llib/socket.scm 401 */
BgL_test2342z00_3061 = 
(BgL_domainz00_1255==BGl_symbol2062z00zz__socketz00)
; } } 
if(BgL_test2342z00_3061)
{ /* Llib/socket.scm 406 */
 int BgL_auxz00_3072; obj_t BgL_tmpz00_3065;
{ /* Llib/socket.scm 406 */
 obj_t BgL_tmpz00_3073;
if(
INTEGERP(BgL_backlogz00_1254))
{ /* Llib/socket.scm 406 */
BgL_tmpz00_3073 = BgL_backlogz00_1254
; }  else 
{ 
 obj_t BgL_auxz00_3076;
BgL_auxz00_3076 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18295L), BGl_string2065z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_backlogz00_1254); 
FAILURE(BgL_auxz00_3076,BFALSE,BFALSE);} 
BgL_auxz00_3072 = 
CINT(BgL_tmpz00_3073); } 
if(
STRINGP(BgL_namez00_1253))
{ /* Llib/socket.scm 406 */
BgL_tmpz00_3065 = BgL_namez00_1253
; }  else 
{ 
 obj_t BgL_auxz00_3068;
BgL_auxz00_3068 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18290L), BGl_string2065z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_namez00_1253); 
FAILURE(BgL_auxz00_3068,BFALSE,BFALSE);} 
return 
bgl_make_server_unix_socket(BgL_tmpz00_3065, BgL_auxz00_3072);}  else 
{ /* Llib/socket.scm 410 */
 obj_t BgL_aux1884z00_2396;
BgL_aux1884z00_2396 = 
BGl_errorz00zz__errorz00(BGl_string2065z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_1255); 
if(
SOCKETP(BgL_aux1884z00_2396))
{ /* Llib/socket.scm 410 */
return BgL_aux1884z00_2396;}  else 
{ 
 obj_t BgL_auxz00_3085;
BgL_auxz00_3085 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18411L), BGl_string2065z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_aux1884z00_2396); 
FAILURE(BgL_auxz00_3085,BFALSE,BFALSE);} } } } }  else 
{ /* Llib/socket.scm 228 */
 obj_t BgL_arg1318z00_1265;
{ /* Llib/socket.scm 228 */
 obj_t BgL_arg1319z00_1266;
{ /* Llib/socket.scm 228 */
 obj_t BgL_arg1322z00_1269;
BgL_arg1322z00_1269 = 
BGl_dssslzd2getzd2keyzd2restzd2argz00zz__dssslz00(BgL_dsssl1126z00_1251, BGl_list2067z00zz__socketz00); 
{ /* Llib/socket.scm 228 */
 obj_t BgL_list1323z00_1270;
BgL_list1323z00_1270 = 
MAKE_YOUNG_PAIR(BgL_arg1322z00_1269, BNIL); 
BgL_arg1319z00_1266 = 
BGl_mapz00zz__r4_control_features_6_9z00(BGl_proc2073z00zz__socketz00, BgL_list1323z00_1270); } } 
{ /* Llib/socket.scm 228 */
 obj_t BgL_list1320z00_1267;
BgL_list1320z00_1267 = 
MAKE_YOUNG_PAIR(BgL_arg1319z00_1266, BNIL); 
BgL_arg1318z00_1265 = 
BGl_applyz00zz__r4_control_features_6_9z00(BGl_stringzd2appendzd2envz00zz__r4_strings_6_7z00, BGl_string2074z00zz__socketz00, BgL_list1320z00_1267); } } 
{ /* Llib/socket.scm 228 */
 obj_t BgL_aux1886z00_2398;
BgL_aux1886z00_2398 = 
BGl_errorz00zz__errorz00(BGl_string2075z00zz__socketz00, BgL_arg1318z00_1265, BgL_dsssl1126z00_1251); 
if(
SOCKETP(BgL_aux1886z00_2398))
{ /* Llib/socket.scm 228 */
return BgL_aux1886z00_2398;}  else 
{ 
 obj_t BgL_auxz00_3097;
BgL_auxz00_3097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(9891L), BGl_string2065z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_aux1886z00_2398); 
FAILURE(BgL_auxz00_3097,BFALSE,BFALSE);} } } } } } } } } 

}



/* &make-server-socket */
obj_t BGl_z62makezd2serverzd2socketz62zz__socketz00(obj_t BgL_envz00_2251, obj_t BgL_portz00_2252)
{
{ /* Llib/socket.scm 399 */
return 
BGl_makezd2serverzd2socketz00zz__socketz00(BgL_portz00_2252);} 

}



/* &<@anonymous:1324> */
obj_t BGl_z62zc3z04anonymousza31324ze3ze5zz__socketz00(obj_t BgL_envz00_2253, obj_t BgL_vz00_2254)
{
{ /* Llib/socket.scm 228 */
{ /* Llib/socket.scm 228 */
 obj_t BgL_list1325z00_2562;
BgL_list1325z00_2562 = 
MAKE_YOUNG_PAIR(BgL_vz00_2254, BNIL); 
return 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string2076z00zz__socketz00, BgL_list1325z00_2562);} } 

}



/* _socket-accept */
obj_t BGl__socketzd2acceptzd2zz__socketz00(obj_t BgL_env1129z00_30, obj_t BgL_opt1128z00_29)
{
{ /* Llib/socket.scm 415 */
{ /* Llib/socket.scm 415 */
 obj_t BgL_g1137z00_1286;
BgL_g1137z00_1286 = 
VECTOR_REF(BgL_opt1128z00_29,0L); 
{ /* Llib/socket.scm 415 */
 obj_t BgL_errpz00_1287;
BgL_errpz00_1287 = BTRUE; 
{ /* Llib/socket.scm 415 */
 obj_t BgL_inbufz00_1288;
BgL_inbufz00_1288 = BTRUE; 
{ /* Llib/socket.scm 415 */
 obj_t BgL_outbufz00_1289;
BgL_outbufz00_1289 = BTRUE; 
{ /* Llib/socket.scm 415 */

{ 
 long BgL_iz00_1290;
BgL_iz00_1290 = 1L; 
BgL_check1132z00_1291:
if(
(BgL_iz00_1290==
VECTOR_LENGTH(BgL_opt1128z00_29)))
{ /* Llib/socket.scm 415 */BNIL; }  else 
{ /* Llib/socket.scm 415 */
 bool_t BgL_test2349z00_3108;
{ /* Llib/socket.scm 415 */
 obj_t BgL_arg1337z00_1297;
{ /* Llib/socket.scm 415 */
 bool_t BgL_test2350z00_3109;
{ /* Llib/socket.scm 415 */
 long BgL_tmpz00_3110;
BgL_tmpz00_3110 = 
VECTOR_LENGTH(BgL_opt1128z00_29); 
BgL_test2350z00_3109 = 
BOUND_CHECK(BgL_iz00_1290, BgL_tmpz00_3110); } 
if(BgL_test2350z00_3109)
{ /* Llib/socket.scm 415 */
BgL_arg1337z00_1297 = 
VECTOR_REF(BgL_opt1128z00_29,BgL_iz00_1290); }  else 
{ 
 obj_t BgL_auxz00_3114;
BgL_auxz00_3114 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2038z00zz__socketz00, BgL_opt1128z00_29, 
(int)(
VECTOR_LENGTH(BgL_opt1128z00_29)), 
(int)(BgL_iz00_1290)); 
FAILURE(BgL_auxz00_3114,BFALSE,BFALSE);} } 
BgL_test2349z00_3108 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1337z00_1297, BGl_list2077z00zz__socketz00)); } 
if(BgL_test2349z00_3108)
{ 
 long BgL_iz00_3123;
BgL_iz00_3123 = 
(BgL_iz00_1290+2L); 
BgL_iz00_1290 = BgL_iz00_3123; 
goto BgL_check1132z00_1291;}  else 
{ /* Llib/socket.scm 415 */
 obj_t BgL_arg1336z00_1296;
{ /* Llib/socket.scm 415 */
 bool_t BgL_test2351z00_3125;
{ /* Llib/socket.scm 415 */
 long BgL_tmpz00_3126;
BgL_tmpz00_3126 = 
VECTOR_LENGTH(BgL_opt1128z00_29); 
BgL_test2351z00_3125 = 
BOUND_CHECK(BgL_iz00_1290, BgL_tmpz00_3126); } 
if(BgL_test2351z00_3125)
{ /* Llib/socket.scm 415 */
BgL_arg1336z00_1296 = 
VECTOR_REF(BgL_opt1128z00_29,BgL_iz00_1290); }  else 
{ 
 obj_t BgL_auxz00_3130;
BgL_auxz00_3130 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2038z00zz__socketz00, BgL_opt1128z00_29, 
(int)(
VECTOR_LENGTH(BgL_opt1128z00_29)), 
(int)(BgL_iz00_1290)); 
FAILURE(BgL_auxz00_3130,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol2080z00zz__socketz00, BGl_string2050z00zz__socketz00, BgL_arg1336z00_1296); } } } 
{ /* Llib/socket.scm 415 */
 obj_t BgL_index1134z00_1298;
BgL_index1134z00_1298 = 
BGl_search1131ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1128z00_29), BgL_opt1128z00_29, BGl_keyword2078z00zz__socketz00, 1L); 
{ /* Llib/socket.scm 415 */
 bool_t BgL_test2352z00_3140;
{ /* Llib/socket.scm 415 */
 long BgL_n1z00_1902;
{ /* Llib/socket.scm 415 */
 obj_t BgL_tmpz00_3141;
if(
INTEGERP(BgL_index1134z00_1298))
{ /* Llib/socket.scm 415 */
BgL_tmpz00_3141 = BgL_index1134z00_1298
; }  else 
{ 
 obj_t BgL_auxz00_3144;
BgL_auxz00_3144 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1134z00_1298); 
FAILURE(BgL_auxz00_3144,BFALSE,BFALSE);} 
BgL_n1z00_1902 = 
(long)CINT(BgL_tmpz00_3141); } 
BgL_test2352z00_3140 = 
(BgL_n1z00_1902>=0L); } 
if(BgL_test2352z00_3140)
{ 
 long BgL_auxz00_3150;
{ /* Llib/socket.scm 415 */
 obj_t BgL_tmpz00_3151;
if(
INTEGERP(BgL_index1134z00_1298))
{ /* Llib/socket.scm 415 */
BgL_tmpz00_3151 = BgL_index1134z00_1298
; }  else 
{ 
 obj_t BgL_auxz00_3154;
BgL_auxz00_3154 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1134z00_1298); 
FAILURE(BgL_auxz00_3154,BFALSE,BFALSE);} 
BgL_auxz00_3150 = 
(long)CINT(BgL_tmpz00_3151); } 
BgL_errpz00_1287 = 
VECTOR_REF(BgL_opt1128z00_29,BgL_auxz00_3150); }  else 
{ /* Llib/socket.scm 415 */BFALSE; } } } 
{ /* Llib/socket.scm 415 */
 obj_t BgL_index1135z00_1300;
BgL_index1135z00_1300 = 
BGl_search1131ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1128z00_29), BgL_opt1128z00_29, BGl_keyword2042z00zz__socketz00, 1L); 
{ /* Llib/socket.scm 415 */
 bool_t BgL_test2355z00_3162;
{ /* Llib/socket.scm 415 */
 long BgL_n1z00_1903;
{ /* Llib/socket.scm 415 */
 obj_t BgL_tmpz00_3163;
if(
INTEGERP(BgL_index1135z00_1300))
{ /* Llib/socket.scm 415 */
BgL_tmpz00_3163 = BgL_index1135z00_1300
; }  else 
{ 
 obj_t BgL_auxz00_3166;
BgL_auxz00_3166 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1135z00_1300); 
FAILURE(BgL_auxz00_3166,BFALSE,BFALSE);} 
BgL_n1z00_1903 = 
(long)CINT(BgL_tmpz00_3163); } 
BgL_test2355z00_3162 = 
(BgL_n1z00_1903>=0L); } 
if(BgL_test2355z00_3162)
{ 
 long BgL_auxz00_3172;
{ /* Llib/socket.scm 415 */
 obj_t BgL_tmpz00_3173;
if(
INTEGERP(BgL_index1135z00_1300))
{ /* Llib/socket.scm 415 */
BgL_tmpz00_3173 = BgL_index1135z00_1300
; }  else 
{ 
 obj_t BgL_auxz00_3176;
BgL_auxz00_3176 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1135z00_1300); 
FAILURE(BgL_auxz00_3176,BFALSE,BFALSE);} 
BgL_auxz00_3172 = 
(long)CINT(BgL_tmpz00_3173); } 
BgL_inbufz00_1288 = 
VECTOR_REF(BgL_opt1128z00_29,BgL_auxz00_3172); }  else 
{ /* Llib/socket.scm 415 */BFALSE; } } } 
{ /* Llib/socket.scm 415 */
 obj_t BgL_index1136z00_1302;
BgL_index1136z00_1302 = 
BGl_search1131ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1128z00_29), BgL_opt1128z00_29, BGl_keyword2044z00zz__socketz00, 1L); 
{ /* Llib/socket.scm 415 */
 bool_t BgL_test2358z00_3184;
{ /* Llib/socket.scm 415 */
 long BgL_n1z00_1904;
{ /* Llib/socket.scm 415 */
 obj_t BgL_tmpz00_3185;
if(
INTEGERP(BgL_index1136z00_1302))
{ /* Llib/socket.scm 415 */
BgL_tmpz00_3185 = BgL_index1136z00_1302
; }  else 
{ 
 obj_t BgL_auxz00_3188;
BgL_auxz00_3188 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1136z00_1302); 
FAILURE(BgL_auxz00_3188,BFALSE,BFALSE);} 
BgL_n1z00_1904 = 
(long)CINT(BgL_tmpz00_3185); } 
BgL_test2358z00_3184 = 
(BgL_n1z00_1904>=0L); } 
if(BgL_test2358z00_3184)
{ 
 long BgL_auxz00_3194;
{ /* Llib/socket.scm 415 */
 obj_t BgL_tmpz00_3195;
if(
INTEGERP(BgL_index1136z00_1302))
{ /* Llib/socket.scm 415 */
BgL_tmpz00_3195 = BgL_index1136z00_1302
; }  else 
{ 
 obj_t BgL_auxz00_3198;
BgL_auxz00_3198 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1136z00_1302); 
FAILURE(BgL_auxz00_3198,BFALSE,BFALSE);} 
BgL_auxz00_3194 = 
(long)CINT(BgL_tmpz00_3195); } 
BgL_outbufz00_1289 = 
VECTOR_REF(BgL_opt1128z00_29,BgL_auxz00_3194); }  else 
{ /* Llib/socket.scm 415 */BFALSE; } } } 
{ /* Llib/socket.scm 415 */
 obj_t BgL_arg1341z00_1304;
BgL_arg1341z00_1304 = 
VECTOR_REF(BgL_opt1128z00_29,0L); 
{ /* Llib/socket.scm 415 */
 obj_t BgL_errpz00_1305;
BgL_errpz00_1305 = BgL_errpz00_1287; 
{ /* Llib/socket.scm 415 */
 obj_t BgL_inbufz00_1306;
BgL_inbufz00_1306 = BgL_inbufz00_1288; 
{ /* Llib/socket.scm 415 */
 obj_t BgL_outbufz00_1307;
BgL_outbufz00_1307 = BgL_outbufz00_1289; 
{ /* Llib/socket.scm 415 */
 obj_t BgL_socketz00_1905;
if(
SOCKETP(BgL_arg1341z00_1304))
{ /* Llib/socket.scm 415 */
BgL_socketz00_1905 = BgL_arg1341z00_1304; }  else 
{ 
 obj_t BgL_auxz00_3207;
BgL_auxz00_3207 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(18694L), BGl_string2082z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_arg1341z00_1304); 
FAILURE(BgL_auxz00_3207,BFALSE,BFALSE);} 
return 
bgl_socket_accept(BgL_socketz00_1905, 
CBOOL(BgL_errpz00_1305), 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2081z00zz__socketz00, BgL_inbufz00_1306, 
(int)(512L)), 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2081z00zz__socketz00, BgL_outbufz00_1307, 
(int)(1024L)));} } } } } } } } } } } 

}



/* search1131~0 */
obj_t BGl_search1131ze70ze7zz__socketz00(long BgL_l1130z00_2313, obj_t BgL_opt1128z00_2312, obj_t BgL_k1z00_1283, long BgL_iz00_1284)
{
{ /* Llib/socket.scm 415 */
BGl_search1131ze70ze7zz__socketz00:
if(
(BgL_iz00_1284==BgL_l1130z00_2313))
{ /* Llib/socket.scm 415 */
return 
BINT(-1L);}  else 
{ /* Llib/socket.scm 415 */
if(
(BgL_iz00_1284==
(BgL_l1130z00_2313-1L)))
{ /* Llib/socket.scm 415 */
return 
BGl_errorz00zz__errorz00(BGl_symbol2080z00zz__socketz00, BGl_string2083z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1128z00_2312)));}  else 
{ /* Llib/socket.scm 415 */
 obj_t BgL_vz00_1312;
BgL_vz00_1312 = 
VECTOR_REF(BgL_opt1128z00_2312,BgL_iz00_1284); 
if(
(BgL_vz00_1312==BgL_k1z00_1283))
{ /* Llib/socket.scm 415 */
return 
BINT(
(BgL_iz00_1284+1L));}  else 
{ 
 long BgL_iz00_3231;
BgL_iz00_3231 = 
(BgL_iz00_1284+2L); 
BgL_iz00_1284 = BgL_iz00_3231; 
goto BGl_search1131ze70ze7zz__socketz00;} } } } 

}



/* socket-accept */
BGL_EXPORTED_DEF obj_t BGl_socketzd2acceptzd2zz__socketz00(obj_t BgL_socketz00_25, obj_t BgL_errpz00_26, obj_t BgL_inbufz00_27, obj_t BgL_outbufz00_28)
{
{ /* Llib/socket.scm 415 */
return 
bgl_socket_accept(BgL_socketz00_25, 
CBOOL(BgL_errpz00_26), 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2081z00zz__socketz00, BgL_inbufz00_27, 
(int)(512L)), 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2081z00zz__socketz00, BgL_outbufz00_28, 
(int)(1024L)));} 

}



/* _socket-accept-many */
obj_t BGl__socketzd2acceptzd2manyz00zz__socketz00(obj_t BgL_env1139z00_37, obj_t BgL_opt1138z00_36)
{
{ /* Llib/socket.scm 426 */
{ /* Llib/socket.scm 426 */
 obj_t BgL_g1147z00_1321; obj_t BgL_g1148z00_1322;
BgL_g1147z00_1321 = 
VECTOR_REF(BgL_opt1138z00_36,0L); 
BgL_g1148z00_1322 = 
VECTOR_REF(BgL_opt1138z00_36,1L); 
{ /* Llib/socket.scm 426 */
 obj_t BgL_errpz00_1323;
BgL_errpz00_1323 = BTRUE; 
{ /* Llib/socket.scm 426 */
 obj_t BgL_inbufsz00_1324;
BgL_inbufsz00_1324 = BTRUE; 
{ /* Llib/socket.scm 426 */
 obj_t BgL_outbufsz00_1325;
BgL_outbufsz00_1325 = BTRUE; 
{ /* Llib/socket.scm 426 */

{ 
 long BgL_iz00_1326;
BgL_iz00_1326 = 2L; 
BgL_check1142z00_1327:
if(
(BgL_iz00_1326==
VECTOR_LENGTH(BgL_opt1138z00_36)))
{ /* Llib/socket.scm 426 */BNIL; }  else 
{ /* Llib/socket.scm 426 */
 bool_t BgL_test2366z00_3244;
{ /* Llib/socket.scm 426 */
 obj_t BgL_arg1356z00_1333;
{ /* Llib/socket.scm 426 */
 bool_t BgL_test2367z00_3245;
{ /* Llib/socket.scm 426 */
 long BgL_tmpz00_3246;
BgL_tmpz00_3246 = 
VECTOR_LENGTH(BgL_opt1138z00_36); 
BgL_test2367z00_3245 = 
BOUND_CHECK(BgL_iz00_1326, BgL_tmpz00_3246); } 
if(BgL_test2367z00_3245)
{ /* Llib/socket.scm 426 */
BgL_arg1356z00_1333 = 
VECTOR_REF(BgL_opt1138z00_36,BgL_iz00_1326); }  else 
{ 
 obj_t BgL_auxz00_3250;
BgL_auxz00_3250 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2038z00zz__socketz00, BgL_opt1138z00_36, 
(int)(
VECTOR_LENGTH(BgL_opt1138z00_36)), 
(int)(BgL_iz00_1326)); 
FAILURE(BgL_auxz00_3250,BFALSE,BFALSE);} } 
BgL_test2366z00_3244 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1356z00_1333, BGl_list2084z00zz__socketz00)); } 
if(BgL_test2366z00_3244)
{ 
 long BgL_iz00_3259;
BgL_iz00_3259 = 
(BgL_iz00_1326+2L); 
BgL_iz00_1326 = BgL_iz00_3259; 
goto BgL_check1142z00_1327;}  else 
{ /* Llib/socket.scm 426 */
 obj_t BgL_arg1354z00_1332;
{ /* Llib/socket.scm 426 */
 bool_t BgL_test2368z00_3261;
{ /* Llib/socket.scm 426 */
 long BgL_tmpz00_3262;
BgL_tmpz00_3262 = 
VECTOR_LENGTH(BgL_opt1138z00_36); 
BgL_test2368z00_3261 = 
BOUND_CHECK(BgL_iz00_1326, BgL_tmpz00_3262); } 
if(BgL_test2368z00_3261)
{ /* Llib/socket.scm 426 */
BgL_arg1354z00_1332 = 
VECTOR_REF(BgL_opt1138z00_36,BgL_iz00_1326); }  else 
{ 
 obj_t BgL_auxz00_3266;
BgL_auxz00_3266 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2038z00zz__socketz00, BgL_opt1138z00_36, 
(int)(
VECTOR_LENGTH(BgL_opt1138z00_36)), 
(int)(BgL_iz00_1326)); 
FAILURE(BgL_auxz00_3266,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol2089z00zz__socketz00, BGl_string2050z00zz__socketz00, BgL_arg1354z00_1332); } } } 
{ /* Llib/socket.scm 426 */
 obj_t BgL_index1144z00_1334;
BgL_index1144z00_1334 = 
BGl_search1141ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1138z00_36), BgL_opt1138z00_36, BGl_keyword2078z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 426 */
 bool_t BgL_test2369z00_3276;
{ /* Llib/socket.scm 426 */
 long BgL_n1z00_1925;
{ /* Llib/socket.scm 426 */
 obj_t BgL_tmpz00_3277;
if(
INTEGERP(BgL_index1144z00_1334))
{ /* Llib/socket.scm 426 */
BgL_tmpz00_3277 = BgL_index1144z00_1334
; }  else 
{ 
 obj_t BgL_auxz00_3280;
BgL_auxz00_3280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1144z00_1334); 
FAILURE(BgL_auxz00_3280,BFALSE,BFALSE);} 
BgL_n1z00_1925 = 
(long)CINT(BgL_tmpz00_3277); } 
BgL_test2369z00_3276 = 
(BgL_n1z00_1925>=0L); } 
if(BgL_test2369z00_3276)
{ 
 long BgL_auxz00_3286;
{ /* Llib/socket.scm 426 */
 obj_t BgL_tmpz00_3287;
if(
INTEGERP(BgL_index1144z00_1334))
{ /* Llib/socket.scm 426 */
BgL_tmpz00_3287 = BgL_index1144z00_1334
; }  else 
{ 
 obj_t BgL_auxz00_3290;
BgL_auxz00_3290 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1144z00_1334); 
FAILURE(BgL_auxz00_3290,BFALSE,BFALSE);} 
BgL_auxz00_3286 = 
(long)CINT(BgL_tmpz00_3287); } 
BgL_errpz00_1323 = 
VECTOR_REF(BgL_opt1138z00_36,BgL_auxz00_3286); }  else 
{ /* Llib/socket.scm 426 */BFALSE; } } } 
{ /* Llib/socket.scm 426 */
 obj_t BgL_index1145z00_1336;
BgL_index1145z00_1336 = 
BGl_search1141ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1138z00_36), BgL_opt1138z00_36, BGl_keyword2085z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 426 */
 bool_t BgL_test2372z00_3298;
{ /* Llib/socket.scm 426 */
 long BgL_n1z00_1926;
{ /* Llib/socket.scm 426 */
 obj_t BgL_tmpz00_3299;
if(
INTEGERP(BgL_index1145z00_1336))
{ /* Llib/socket.scm 426 */
BgL_tmpz00_3299 = BgL_index1145z00_1336
; }  else 
{ 
 obj_t BgL_auxz00_3302;
BgL_auxz00_3302 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1145z00_1336); 
FAILURE(BgL_auxz00_3302,BFALSE,BFALSE);} 
BgL_n1z00_1926 = 
(long)CINT(BgL_tmpz00_3299); } 
BgL_test2372z00_3298 = 
(BgL_n1z00_1926>=0L); } 
if(BgL_test2372z00_3298)
{ 
 long BgL_auxz00_3308;
{ /* Llib/socket.scm 426 */
 obj_t BgL_tmpz00_3309;
if(
INTEGERP(BgL_index1145z00_1336))
{ /* Llib/socket.scm 426 */
BgL_tmpz00_3309 = BgL_index1145z00_1336
; }  else 
{ 
 obj_t BgL_auxz00_3312;
BgL_auxz00_3312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1145z00_1336); 
FAILURE(BgL_auxz00_3312,BFALSE,BFALSE);} 
BgL_auxz00_3308 = 
(long)CINT(BgL_tmpz00_3309); } 
BgL_inbufsz00_1324 = 
VECTOR_REF(BgL_opt1138z00_36,BgL_auxz00_3308); }  else 
{ /* Llib/socket.scm 426 */BFALSE; } } } 
{ /* Llib/socket.scm 426 */
 obj_t BgL_index1146z00_1338;
BgL_index1146z00_1338 = 
BGl_search1141ze70ze7zz__socketz00(
VECTOR_LENGTH(BgL_opt1138z00_36), BgL_opt1138z00_36, BGl_keyword2087z00zz__socketz00, 2L); 
{ /* Llib/socket.scm 426 */
 bool_t BgL_test2375z00_3320;
{ /* Llib/socket.scm 426 */
 long BgL_n1z00_1927;
{ /* Llib/socket.scm 426 */
 obj_t BgL_tmpz00_3321;
if(
INTEGERP(BgL_index1146z00_1338))
{ /* Llib/socket.scm 426 */
BgL_tmpz00_3321 = BgL_index1146z00_1338
; }  else 
{ 
 obj_t BgL_auxz00_3324;
BgL_auxz00_3324 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1146z00_1338); 
FAILURE(BgL_auxz00_3324,BFALSE,BFALSE);} 
BgL_n1z00_1927 = 
(long)CINT(BgL_tmpz00_3321); } 
BgL_test2375z00_3320 = 
(BgL_n1z00_1927>=0L); } 
if(BgL_test2375z00_3320)
{ 
 long BgL_auxz00_3330;
{ /* Llib/socket.scm 426 */
 obj_t BgL_tmpz00_3331;
if(
INTEGERP(BgL_index1146z00_1338))
{ /* Llib/socket.scm 426 */
BgL_tmpz00_3331 = BgL_index1146z00_1338
; }  else 
{ 
 obj_t BgL_auxz00_3334;
BgL_auxz00_3334 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_index1146z00_1338); 
FAILURE(BgL_auxz00_3334,BFALSE,BFALSE);} 
BgL_auxz00_3330 = 
(long)CINT(BgL_tmpz00_3331); } 
BgL_outbufsz00_1325 = 
VECTOR_REF(BgL_opt1138z00_36,BgL_auxz00_3330); }  else 
{ /* Llib/socket.scm 426 */BFALSE; } } } 
{ /* Llib/socket.scm 426 */
 obj_t BgL_arg1360z00_1340; obj_t BgL_arg1361z00_1341;
BgL_arg1360z00_1340 = 
VECTOR_REF(BgL_opt1138z00_36,0L); 
BgL_arg1361z00_1341 = 
VECTOR_REF(BgL_opt1138z00_36,1L); 
{ /* Llib/socket.scm 426 */
 obj_t BgL_errpz00_1342;
BgL_errpz00_1342 = BgL_errpz00_1323; 
{ /* Llib/socket.scm 426 */
 obj_t BgL_inbufsz00_1343;
BgL_inbufsz00_1343 = BgL_inbufsz00_1324; 
{ /* Llib/socket.scm 426 */
 obj_t BgL_outbufsz00_1344;
BgL_outbufsz00_1344 = BgL_outbufsz00_1325; 
{ /* Llib/socket.scm 426 */
 obj_t BgL_auxz00_3349; obj_t BgL_auxz00_3342;
if(
VECTORP(BgL_arg1361z00_1341))
{ /* Llib/socket.scm 426 */
BgL_auxz00_3349 = BgL_arg1361z00_1341
; }  else 
{ 
 obj_t BgL_auxz00_3352;
BgL_auxz00_3352 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2092z00zz__socketz00, BgL_arg1361z00_1341); 
FAILURE(BgL_auxz00_3352,BFALSE,BFALSE);} 
if(
SOCKETP(BgL_arg1360z00_1340))
{ /* Llib/socket.scm 426 */
BgL_auxz00_3342 = BgL_arg1360z00_1340
; }  else 
{ 
 obj_t BgL_auxz00_3345;
BgL_auxz00_3345 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19386L), BGl_string2091z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_arg1360z00_1340); 
FAILURE(BgL_auxz00_3345,BFALSE,BFALSE);} 
return 
BGl_socketzd2acceptzd2manyz00zz__socketz00(BgL_auxz00_3342, BgL_auxz00_3349, BgL_errpz00_1342, BgL_inbufsz00_1343, BgL_outbufsz00_1344);} } } } } } } } } } } 

}



/* search1141~0 */
obj_t BGl_search1141ze70ze7zz__socketz00(long BgL_l1140z00_2311, obj_t BgL_opt1138z00_2310, obj_t BgL_k1z00_1318, long BgL_iz00_1319)
{
{ /* Llib/socket.scm 426 */
BGl_search1141ze70ze7zz__socketz00:
if(
(BgL_iz00_1319==BgL_l1140z00_2311))
{ /* Llib/socket.scm 426 */
return 
BINT(-1L);}  else 
{ /* Llib/socket.scm 426 */
if(
(BgL_iz00_1319==
(BgL_l1140z00_2311-1L)))
{ /* Llib/socket.scm 426 */
return 
BGl_errorz00zz__errorz00(BGl_symbol2089z00zz__socketz00, BGl_string2093z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1138z00_2310)));}  else 
{ /* Llib/socket.scm 426 */
 obj_t BgL_vz00_1349;
BgL_vz00_1349 = 
VECTOR_REF(BgL_opt1138z00_2310,BgL_iz00_1319); 
if(
(BgL_vz00_1349==BgL_k1z00_1318))
{ /* Llib/socket.scm 426 */
return 
BINT(
(BgL_iz00_1319+1L));}  else 
{ 
 long BgL_iz00_3371;
BgL_iz00_3371 = 
(BgL_iz00_1319+2L); 
BgL_iz00_1319 = BgL_iz00_3371; 
goto BGl_search1141ze70ze7zz__socketz00;} } } } 

}



/* socket-accept-many */
BGL_EXPORTED_DEF obj_t BGl_socketzd2acceptzd2manyz00zz__socketz00(obj_t BgL_socketz00_31, obj_t BgL_resultz00_32, obj_t BgL_errpz00_33, obj_t BgL_inbufsz00_34, obj_t BgL_outbufsz00_35)
{
{ /* Llib/socket.scm 426 */
if(
VECTORP(BgL_inbufsz00_34))
{ /* Llib/socket.scm 427 */((bool_t)0); }  else 
{ /* Llib/socket.scm 427 */
BgL_inbufsz00_34 = 
make_vector(
VECTOR_LENGTH(BgL_resultz00_32), BUNSPEC); 
{ 
 long BgL_iz00_1356;
BgL_iz00_1356 = 0L; 
BgL_zc3z04anonymousza31371ze3z87_1357:
if(
(BgL_iz00_1356<
VECTOR_LENGTH(BgL_resultz00_32)))
{ /* Llib/socket.scm 431 */
 obj_t BgL_bufz00_1360;
BgL_bufz00_1360 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2090z00zz__socketz00, BTRUE, 
(int)(512L)); 
{ /* Llib/socket.scm 432 */
 obj_t BgL_vectorz00_1934;
{ /* Llib/socket.scm 432 */
 obj_t BgL_aux1906z00_2419;
BgL_aux1906z00_2419 = BgL_inbufsz00_34; 
if(
VECTORP(BgL_aux1906z00_2419))
{ /* Llib/socket.scm 432 */
BgL_vectorz00_1934 = BgL_aux1906z00_2419; }  else 
{ 
 obj_t BgL_auxz00_3384;
BgL_auxz00_3384 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19711L), BGl_string2094z00zz__socketz00, BGl_string2092z00zz__socketz00, BgL_aux1906z00_2419); 
FAILURE(BgL_auxz00_3384,BFALSE,BFALSE);} } 
{ /* Llib/socket.scm 432 */
 bool_t BgL_test2386z00_3388;
{ /* Llib/socket.scm 432 */
 long BgL_tmpz00_3389;
BgL_tmpz00_3389 = 
VECTOR_LENGTH(BgL_vectorz00_1934); 
BgL_test2386z00_3388 = 
BOUND_CHECK(BgL_iz00_1356, BgL_tmpz00_3389); } 
if(BgL_test2386z00_3388)
{ /* Llib/socket.scm 432 */
VECTOR_SET(BgL_vectorz00_1934,BgL_iz00_1356,BgL_bufz00_1360); }  else 
{ 
 obj_t BgL_auxz00_3393;
BgL_auxz00_3393 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19698L), BGl_string2095z00zz__socketz00, BgL_vectorz00_1934, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_1934)), 
(int)(BgL_iz00_1356)); 
FAILURE(BgL_auxz00_3393,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_3400;
BgL_iz00_3400 = 
(BgL_iz00_1356+1L); 
BgL_iz00_1356 = BgL_iz00_3400; 
goto BgL_zc3z04anonymousza31371ze3z87_1357;} }  else 
{ /* Llib/socket.scm 430 */((bool_t)0); } } } 
if(
VECTORP(BgL_outbufsz00_35))
{ /* Llib/socket.scm 434 */((bool_t)0); }  else 
{ /* Llib/socket.scm 434 */
BgL_outbufsz00_35 = 
make_vector(
VECTOR_LENGTH(BgL_resultz00_32), BUNSPEC); 
{ 
 long BgL_iz00_1368;
BgL_iz00_1368 = 0L; 
BgL_zc3z04anonymousza31380ze3z87_1369:
if(
(BgL_iz00_1368<
VECTOR_LENGTH(BgL_resultz00_32)))
{ /* Llib/socket.scm 438 */
 obj_t BgL_bufz00_1372;
BgL_bufz00_1372 = 
BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(BGl_string2090z00zz__socketz00, BTRUE, 
(int)(512L)); 
{ /* Llib/socket.scm 439 */
 obj_t BgL_vectorz00_1943;
{ /* Llib/socket.scm 439 */
 obj_t BgL_aux1908z00_2422;
BgL_aux1908z00_2422 = BgL_outbufsz00_35; 
if(
VECTORP(BgL_aux1908z00_2422))
{ /* Llib/socket.scm 439 */
BgL_vectorz00_1943 = BgL_aux1908z00_2422; }  else 
{ 
 obj_t BgL_auxz00_3413;
BgL_auxz00_3413 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19989L), BGl_string2094z00zz__socketz00, BGl_string2092z00zz__socketz00, BgL_aux1908z00_2422); 
FAILURE(BgL_auxz00_3413,BFALSE,BFALSE);} } 
{ /* Llib/socket.scm 439 */
 bool_t BgL_test2390z00_3417;
{ /* Llib/socket.scm 439 */
 long BgL_tmpz00_3418;
BgL_tmpz00_3418 = 
VECTOR_LENGTH(BgL_vectorz00_1943); 
BgL_test2390z00_3417 = 
BOUND_CHECK(BgL_iz00_1368, BgL_tmpz00_3418); } 
if(BgL_test2390z00_3417)
{ /* Llib/socket.scm 439 */
VECTOR_SET(BgL_vectorz00_1943,BgL_iz00_1368,BgL_bufz00_1372); }  else 
{ 
 obj_t BgL_auxz00_3422;
BgL_auxz00_3422 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(19976L), BGl_string2095z00zz__socketz00, BgL_vectorz00_1943, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_1943)), 
(int)(BgL_iz00_1368)); 
FAILURE(BgL_auxz00_3422,BFALSE,BFALSE);} } } 
{ 
 long BgL_iz00_3429;
BgL_iz00_3429 = 
(BgL_iz00_1368+1L); 
BgL_iz00_1368 = BgL_iz00_3429; 
goto BgL_zc3z04anonymousza31380ze3z87_1369;} }  else 
{ /* Llib/socket.scm 437 */((bool_t)0); } } } 
{ /* Llib/socket.scm 443 */
 long BgL_tmpz00_3431;
{ /* Llib/socket.scm 443 */
 obj_t BgL_auxz00_3440; obj_t BgL_tmpz00_3432;
{ /* Llib/socket.scm 443 */
 obj_t BgL_aux1912z00_2426;
BgL_aux1912z00_2426 = BgL_outbufsz00_35; 
if(
VECTORP(BgL_aux1912z00_2426))
{ /* Llib/socket.scm 443 */
BgL_auxz00_3440 = BgL_aux1912z00_2426
; }  else 
{ 
 obj_t BgL_auxz00_3443;
BgL_auxz00_3443 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(20112L), BGl_string2090z00zz__socketz00, BGl_string2092z00zz__socketz00, BgL_aux1912z00_2426); 
FAILURE(BgL_auxz00_3443,BFALSE,BFALSE);} } 
{ /* Llib/socket.scm 443 */
 obj_t BgL_aux1910z00_2424;
BgL_aux1910z00_2424 = BgL_inbufsz00_34; 
if(
VECTORP(BgL_aux1910z00_2424))
{ /* Llib/socket.scm 443 */
BgL_tmpz00_3432 = BgL_aux1910z00_2424
; }  else 
{ 
 obj_t BgL_auxz00_3436;
BgL_auxz00_3436 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(20105L), BGl_string2090z00zz__socketz00, BGl_string2092z00zz__socketz00, BgL_aux1910z00_2424); 
FAILURE(BgL_auxz00_3436,BFALSE,BFALSE);} } 
BgL_tmpz00_3431 = 
bgl_socket_accept_many(BgL_socketz00_31, 
CBOOL(BgL_errpz00_33), BgL_tmpz00_3432, BgL_auxz00_3440, BgL_resultz00_32); } 
return 
BINT(BgL_tmpz00_3431);} } 

}



/* _socket-shutdown */
obj_t BGl__socketzd2shutdownzd2zz__socketz00(obj_t BgL_env1150z00_41, obj_t BgL_opt1149z00_40)
{
{ /* Llib/socket.scm 454 */
{ /* Llib/socket.scm 454 */
 obj_t BgL_g1151z00_1376;
BgL_g1151z00_1376 = 
VECTOR_REF(BgL_opt1149z00_40,0L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1149z00_40)) { case 1L : 

{ /* Llib/socket.scm 454 */

{ /* Llib/socket.scm 454 */
 int BgL_tmpz00_3450;
{ /* Llib/socket.scm 454 */
 obj_t BgL_auxz00_3451;
if(
SOCKETP(BgL_g1151z00_1376))
{ /* Llib/socket.scm 454 */
BgL_auxz00_3451 = BgL_g1151z00_1376
; }  else 
{ 
 obj_t BgL_auxz00_3454;
BgL_auxz00_3454 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(20524L), BGl_string2099z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_g1151z00_1376); 
FAILURE(BgL_auxz00_3454,BFALSE,BFALSE);} 
BgL_tmpz00_3450 = 
BGl_socketzd2shutdownzd2zz__socketz00(BgL_auxz00_3451, BTRUE); } 
return 
BINT(BgL_tmpz00_3450);} } break;case 2L : 

{ /* Llib/socket.scm 454 */
 obj_t BgL_howz00_1380;
BgL_howz00_1380 = 
VECTOR_REF(BgL_opt1149z00_40,1L); 
{ /* Llib/socket.scm 454 */

{ /* Llib/socket.scm 454 */
 int BgL_tmpz00_3461;
{ /* Llib/socket.scm 454 */
 obj_t BgL_auxz00_3462;
if(
SOCKETP(BgL_g1151z00_1376))
{ /* Llib/socket.scm 454 */
BgL_auxz00_3462 = BgL_g1151z00_1376
; }  else 
{ 
 obj_t BgL_auxz00_3465;
BgL_auxz00_3465 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(20524L), BGl_string2099z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_g1151z00_1376); 
FAILURE(BgL_auxz00_3465,BFALSE,BFALSE);} 
BgL_tmpz00_3461 = 
BGl_socketzd2shutdownzd2zz__socketz00(BgL_auxz00_3462, BgL_howz00_1380); } 
return 
BINT(BgL_tmpz00_3461);} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2096z00zz__socketz00, BGl_string2098z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1149z00_40)));} } } } 

}



/* socket-shutdown */
BGL_EXPORTED_DEF int BGl_socketzd2shutdownzd2zz__socketz00(obj_t BgL_socketz00_38, obj_t BgL_howz00_39)
{
{ /* Llib/socket.scm 454 */
if(
(BgL_howz00_39==BTRUE))
{ /* Llib/socket.scm 457 */
 int BgL_rz00_1382;
BgL_rz00_1382 = 
socket_shutdown(BgL_socketz00_38, 
(int)(2L)); 
socket_close(BgL_socketz00_38); 
return BgL_rz00_1382;}  else 
{ /* Llib/socket.scm 460 */
 bool_t BgL_test2396z00_3481;
if(
(BgL_howz00_39==BFALSE))
{ /* Llib/socket.scm 460 */
BgL_test2396z00_3481 = ((bool_t)1)
; }  else 
{ /* Llib/socket.scm 460 */
BgL_test2396z00_3481 = 
(BgL_howz00_39==BGl_symbol2100z00zz__socketz00)
; } 
if(BgL_test2396z00_3481)
{ /* Llib/socket.scm 460 */
return 
socket_shutdown(BgL_socketz00_38, 
(int)(2L));}  else 
{ /* Llib/socket.scm 460 */
if(
(BgL_howz00_39==BGl_symbol2102z00zz__socketz00))
{ /* Llib/socket.scm 462 */
return 
socket_shutdown(BgL_socketz00_38, 
(int)(1L));}  else 
{ /* Llib/socket.scm 462 */
if(
(BgL_howz00_39==BGl_symbol2104z00zz__socketz00))
{ /* Llib/socket.scm 464 */
return 
socket_shutdown(BgL_socketz00_38, 
(int)(0L));}  else 
{ /* Llib/socket.scm 467 */
 obj_t BgL_tmpz00_3495;
{ /* Llib/socket.scm 467 */
 obj_t BgL_aux1918z00_2432;
BgL_aux1918z00_2432 = 
BGl_errorz00zz__errorz00(BGl_string2097z00zz__socketz00, BGl_string2106z00zz__socketz00, BgL_howz00_39); 
if(
INTEGERP(BgL_aux1918z00_2432))
{ /* Llib/socket.scm 467 */
BgL_tmpz00_3495 = BgL_aux1918z00_2432
; }  else 
{ 
 obj_t BgL_auxz00_3499;
BgL_auxz00_3499 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(20902L), BGl_string2097z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_aux1918z00_2432); 
FAILURE(BgL_auxz00_3499,BFALSE,BFALSE);} } 
return 
CINT(BgL_tmpz00_3495);} } } } } 

}



/* socket-close */
BGL_EXPORTED_DEF obj_t BGl_socketzd2closezd2zz__socketz00(obj_t BgL_socketz00_42)
{
{ /* Llib/socket.scm 472 */
BGL_TAIL return 
socket_close(BgL_socketz00_42);} 

}



/* &socket-close */
obj_t BGl_z62socketzd2closezb0zz__socketz00(obj_t BgL_envz00_2257, obj_t BgL_socketz00_2258)
{
{ /* Llib/socket.scm 472 */
{ /* Llib/socket.scm 473 */
 obj_t BgL_auxz00_3505;
if(
SOCKETP(BgL_socketz00_2258))
{ /* Llib/socket.scm 473 */
BgL_auxz00_3505 = BgL_socketz00_2258
; }  else 
{ 
 obj_t BgL_auxz00_3508;
BgL_auxz00_3508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(21232L), BGl_string2107z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2258); 
FAILURE(BgL_auxz00_3508,BFALSE,BFALSE);} 
return 
BGl_socketzd2closezd2zz__socketz00(BgL_auxz00_3505);} } 

}



/* host */
BGL_EXPORTED_DEF obj_t BGl_hostz00zz__socketz00(obj_t BgL_hostnamez00_43)
{
{ /* Llib/socket.scm 478 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
BGL_TAIL return 
bgl_host(BgL_hostnamez00_43);} 

}



/* &host */
obj_t BGl_z62hostz62zz__socketz00(obj_t BgL_envz00_2259, obj_t BgL_hostnamez00_2260)
{
{ /* Llib/socket.scm 478 */
{ /* Llib/socket.scm 480 */
 obj_t BgL_auxz00_3515;
if(
STRINGP(BgL_hostnamez00_2260))
{ /* Llib/socket.scm 480 */
BgL_auxz00_3515 = BgL_hostnamez00_2260
; }  else 
{ 
 obj_t BgL_auxz00_3518;
BgL_auxz00_3518 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(21540L), BGl_string2108z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_hostnamez00_2260); 
FAILURE(BgL_auxz00_3518,BFALSE,BFALSE);} 
return 
BGl_hostz00zz__socketz00(BgL_auxz00_3515);} } 

}



/* hostinfo */
BGL_EXPORTED_DEF obj_t BGl_hostinfoz00zz__socketz00(obj_t BgL_hostnamez00_44)
{
{ /* Llib/socket.scm 486 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
BGL_TAIL return 
bgl_hostinfo(BgL_hostnamez00_44);} 

}



/* &hostinfo */
obj_t BGl_z62hostinfoz62zz__socketz00(obj_t BgL_envz00_2261, obj_t BgL_hostnamez00_2262)
{
{ /* Llib/socket.scm 486 */
{ /* Llib/socket.scm 488 */
 obj_t BgL_auxz00_3525;
if(
STRINGP(BgL_hostnamez00_2262))
{ /* Llib/socket.scm 488 */
BgL_auxz00_3525 = BgL_hostnamez00_2262
; }  else 
{ 
 obj_t BgL_auxz00_3528;
BgL_auxz00_3528 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(21855L), BGl_string2109z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_hostnamez00_2262); 
FAILURE(BgL_auxz00_3528,BFALSE,BFALSE);} 
return 
BGl_hostinfoz00zz__socketz00(BgL_auxz00_3525);} } 

}



/* _hostname */
obj_t BGl__hostnamez00zz__socketz00(obj_t BgL_env1155z00_47, obj_t BgL_opt1154z00_46)
{
{ /* Llib/socket.scm 494 */
{ /* Llib/socket.scm 494 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1154z00_46)) { case 0L : 

{ /* Llib/socket.scm 494 */

BGl_z52socketzd2initz12z92zz__socketz00(); 
return 
bgl_gethostname();} break;case 1L : 

{ /* Llib/socket.scm 494 */
 obj_t BgL_hostipz00_1387;
BgL_hostipz00_1387 = 
VECTOR_REF(BgL_opt1154z00_46,0L); 
{ /* Llib/socket.scm 494 */

BGl_z52socketzd2initz12z92zz__socketz00(); 
if(
CBOOL(BgL_hostipz00_1387))
{ /* Llib/socket.scm 497 */
 obj_t BgL_tmpz00_3539;
if(
STRINGP(BgL_hostipz00_1387))
{ /* Llib/socket.scm 497 */
BgL_tmpz00_3539 = BgL_hostipz00_1387
; }  else 
{ 
 obj_t BgL_auxz00_3542;
BgL_auxz00_3542 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(22211L), BGl_string2113z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_hostipz00_1387); 
FAILURE(BgL_auxz00_3542,BFALSE,BFALSE);} 
return 
bgl_gethostname_by_address(BgL_tmpz00_3539);}  else 
{ /* Llib/socket.scm 496 */
return 
bgl_gethostname();} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2110z00zz__socketz00, BGl_string2112z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1154z00_46)));} } } } 

}



/* hostname */
BGL_EXPORTED_DEF obj_t BGl_hostnamez00zz__socketz00(obj_t BgL_hostipz00_45)
{
{ /* Llib/socket.scm 494 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
if(
CBOOL(BgL_hostipz00_45))
{ /* Llib/socket.scm 497 */
 obj_t BgL_tmpz00_3556;
if(
STRINGP(BgL_hostipz00_45))
{ /* Llib/socket.scm 497 */
BgL_tmpz00_3556 = BgL_hostipz00_45
; }  else 
{ 
 obj_t BgL_auxz00_3559;
BgL_auxz00_3559 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(22211L), BGl_string2111z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_hostipz00_45); 
FAILURE(BgL_auxz00_3559,BFALSE,BFALSE);} 
return 
bgl_gethostname_by_address(BgL_tmpz00_3556);}  else 
{ /* Llib/socket.scm 496 */
return 
bgl_gethostname();} } 

}



/* resolv */
BGL_EXPORTED_DEF obj_t BGl_resolvz00zz__socketz00(obj_t BgL_hostnamez00_48, obj_t BgL_typez00_49)
{
{ /* Llib/socket.scm 503 */
{ /* Llib/socket.scm 506 */
 obj_t BgL_arg1391z00_1947;
BgL_arg1391z00_1947 = 
SYMBOL_TO_STRING(BgL_typez00_49); 
return 
bgl_res_query(BgL_hostnamez00_48, BgL_arg1391z00_1947);} } 

}



/* &resolv */
obj_t BGl_z62resolvz62zz__socketz00(obj_t BgL_envz00_2263, obj_t BgL_hostnamez00_2264, obj_t BgL_typez00_2265)
{
{ /* Llib/socket.scm 503 */
{ /* Llib/socket.scm 506 */
 obj_t BgL_auxz00_3574; obj_t BgL_auxz00_3567;
if(
SYMBOLP(BgL_typez00_2265))
{ /* Llib/socket.scm 506 */
BgL_auxz00_3574 = BgL_typez00_2265
; }  else 
{ 
 obj_t BgL_auxz00_3577;
BgL_auxz00_3577 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(22554L), BGl_string2114z00zz__socketz00, BGl_string2059z00zz__socketz00, BgL_typez00_2265); 
FAILURE(BgL_auxz00_3577,BFALSE,BFALSE);} 
if(
STRINGP(BgL_hostnamez00_2264))
{ /* Llib/socket.scm 506 */
BgL_auxz00_3567 = BgL_hostnamez00_2264
; }  else 
{ 
 obj_t BgL_auxz00_3570;
BgL_auxz00_3570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(22554L), BGl_string2114z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_hostnamez00_2264); 
FAILURE(BgL_auxz00_3570,BFALSE,BFALSE);} 
return 
BGl_resolvz00zz__socketz00(BgL_auxz00_3567, BgL_auxz00_3574);} } 

}



/* get-interfaces */
BGL_EXPORTED_DEF obj_t BGl_getzd2interfaceszd2zz__socketz00(void)
{
{ /* Llib/socket.scm 513 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
BGL_TAIL return 
bgl_gethostinterfaces();} 

}



/* &get-interfaces */
obj_t BGl_z62getzd2interfaceszb0zz__socketz00(obj_t BgL_envz00_2266)
{
{ /* Llib/socket.scm 513 */
return 
BGl_getzd2interfaceszd2zz__socketz00();} 

}



/* get-protocols */
BGL_EXPORTED_DEF obj_t BGl_getzd2protocolszd2zz__socketz00(void)
{
{ /* Llib/socket.scm 521 */
BGL_TAIL return 
bgl_getprotoents();} 

}



/* &get-protocols */
obj_t BGl_z62getzd2protocolszb0zz__socketz00(obj_t BgL_envz00_2267)
{
{ /* Llib/socket.scm 521 */
return 
BGl_getzd2protocolszd2zz__socketz00();} 

}



/* get-protocol */
BGL_EXPORTED_DEF obj_t BGl_getzd2protocolzd2zz__socketz00(obj_t BgL_protocolz00_50)
{
{ /* Llib/socket.scm 527 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_protocolz00_50))
{ /* Llib/socket.scm 530 */
 int BgL_tmpz00_3589;
{ /* Llib/socket.scm 530 */
 obj_t BgL_tmpz00_3590;
if(
INTEGERP(BgL_protocolz00_50))
{ /* Llib/socket.scm 530 */
BgL_tmpz00_3590 = BgL_protocolz00_50
; }  else 
{ 
 obj_t BgL_auxz00_3593;
BgL_auxz00_3593 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(23513L), BGl_string2115z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_protocolz00_50); 
FAILURE(BgL_auxz00_3593,BFALSE,BFALSE);} 
BgL_tmpz00_3589 = 
CINT(BgL_tmpz00_3590); } 
return 
bgl_getprotobynumber(BgL_tmpz00_3589);}  else 
{ /* Llib/socket.scm 529 */
if(
STRINGP(BgL_protocolz00_50))
{ /* Llib/socket.scm 531 */
return 
bgl_getprotobyname(
BSTRING_TO_STRING(BgL_protocolz00_50));}  else 
{ /* Llib/socket.scm 531 */
return BFALSE;} } } 

}



/* &get-protocol */
obj_t BGl_z62getzd2protocolzb0zz__socketz00(obj_t BgL_envz00_2268, obj_t BgL_protocolz00_2269)
{
{ /* Llib/socket.scm 527 */
return 
BGl_getzd2protocolzd2zz__socketz00(BgL_protocolz00_2269);} 

}



/* socket-option */
BGL_EXPORTED_DEF obj_t BGl_socketzd2optionzd2zz__socketz00(obj_t BgL_socketz00_51, obj_t BgL_optionz00_52)
{
{ /* Llib/socket.scm 539 */
BGL_TAIL return 
bgl_getsockopt(BgL_socketz00_51, BgL_optionz00_52);} 

}



/* &socket-option */
obj_t BGl_z62socketzd2optionzb0zz__socketz00(obj_t BgL_envz00_2270, obj_t BgL_socketz00_2271, obj_t BgL_optionz00_2272)
{
{ /* Llib/socket.scm 539 */
{ /* Llib/socket.scm 540 */
 obj_t BgL_auxz00_3612; obj_t BgL_auxz00_3605;
if(
KEYWORDP(BgL_optionz00_2272))
{ /* Llib/socket.scm 540 */
BgL_auxz00_3612 = BgL_optionz00_2272
; }  else 
{ 
 obj_t BgL_auxz00_3615;
BgL_auxz00_3615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(23881L), BGl_string2116z00zz__socketz00, BGl_string2117z00zz__socketz00, BgL_optionz00_2272); 
FAILURE(BgL_auxz00_3615,BFALSE,BFALSE);} 
if(
SOCKETP(BgL_socketz00_2271))
{ /* Llib/socket.scm 540 */
BgL_auxz00_3605 = BgL_socketz00_2271
; }  else 
{ 
 obj_t BgL_auxz00_3608;
BgL_auxz00_3608 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(23881L), BGl_string2116z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2271); 
FAILURE(BgL_auxz00_3608,BFALSE,BFALSE);} 
return 
BGl_socketzd2optionzd2zz__socketz00(BgL_auxz00_3605, BgL_auxz00_3612);} } 

}



/* socket-option-set! */
BGL_EXPORTED_DEF obj_t BGl_socketzd2optionzd2setz12z12zz__socketz00(obj_t BgL_socketz00_53, obj_t BgL_optionz00_54, obj_t BgL_valz00_55)
{
{ /* Llib/socket.scm 545 */
BGL_TAIL return 
bgl_setsockopt(BgL_socketz00_53, BgL_optionz00_54, BgL_valz00_55);} 

}



/* &socket-option-set! */
obj_t BGl_z62socketzd2optionzd2setz12z70zz__socketz00(obj_t BgL_envz00_2273, obj_t BgL_socketz00_2274, obj_t BgL_optionz00_2275, obj_t BgL_valz00_2276)
{
{ /* Llib/socket.scm 545 */
{ /* Llib/socket.scm 546 */
 obj_t BgL_auxz00_3628; obj_t BgL_auxz00_3621;
if(
KEYWORDP(BgL_optionz00_2275))
{ /* Llib/socket.scm 546 */
BgL_auxz00_3628 = BgL_optionz00_2275
; }  else 
{ 
 obj_t BgL_auxz00_3631;
BgL_auxz00_3631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(24190L), BGl_string2118z00zz__socketz00, BGl_string2117z00zz__socketz00, BgL_optionz00_2275); 
FAILURE(BgL_auxz00_3631,BFALSE,BFALSE);} 
if(
SOCKETP(BgL_socketz00_2274))
{ /* Llib/socket.scm 546 */
BgL_auxz00_3621 = BgL_socketz00_2274
; }  else 
{ 
 obj_t BgL_auxz00_3624;
BgL_auxz00_3624 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(24190L), BGl_string2118z00zz__socketz00, BGl_string2023z00zz__socketz00, BgL_socketz00_2274); 
FAILURE(BgL_auxz00_3624,BFALSE,BFALSE);} 
return 
BGl_socketzd2optionzd2setz12z12zz__socketz00(BgL_auxz00_3621, BgL_auxz00_3628, BgL_valz00_2276);} } 

}



/* datagram-socket? */
BGL_EXPORTED_DEF bool_t BGl_datagramzd2socketzf3z21zz__socketz00(obj_t BgL_objz00_56)
{
{ /* Llib/socket.scm 551 */
return 
BGL_DATAGRAM_SOCKETP(BgL_objz00_56);} 

}



/* &datagram-socket? */
obj_t BGl_z62datagramzd2socketzf3z43zz__socketz00(obj_t BgL_envz00_2277, obj_t BgL_objz00_2278)
{
{ /* Llib/socket.scm 551 */
return 
BBOOL(
BGl_datagramzd2socketzf3z21zz__socketz00(BgL_objz00_2278));} 

}



/* datagram-socket-server? */
BGL_EXPORTED_DEF bool_t BGl_datagramzd2socketzd2serverzf3zf3zz__socketz00(obj_t BgL_objz00_57)
{
{ /* Llib/socket.scm 557 */
return 
BGL_DATAGRAM_SOCKET_SERVERP(BgL_objz00_57);} 

}



/* &datagram-socket-server? */
obj_t BGl_z62datagramzd2socketzd2serverzf3z91zz__socketz00(obj_t BgL_envz00_2279, obj_t BgL_objz00_2280)
{
{ /* Llib/socket.scm 557 */
return 
BBOOL(
BGl_datagramzd2socketzd2serverzf3zf3zz__socketz00(BgL_objz00_2280));} 

}



/* datagram-socket-client? */
BGL_EXPORTED_DEF bool_t BGl_datagramzd2socketzd2clientzf3zf3zz__socketz00(obj_t BgL_objz00_58)
{
{ /* Llib/socket.scm 563 */
return 
BGL_DATAGRAM_SOCKET_CLIENTP(BgL_objz00_58);} 

}



/* &datagram-socket-client? */
obj_t BGl_z62datagramzd2socketzd2clientzf3z91zz__socketz00(obj_t BgL_envz00_2281, obj_t BgL_objz00_2282)
{
{ /* Llib/socket.scm 563 */
return 
BBOOL(
BGl_datagramzd2socketzd2clientzf3zf3zz__socketz00(BgL_objz00_2282));} 

}



/* datagram-socket-hostname */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2hostnamez00zz__socketz00(obj_t BgL_socketz00_59)
{
{ /* Llib/socket.scm 569 */
return 
BGL_DATAGRAM_SOCKET_HOSTNAME(BgL_socketz00_59);} 

}



/* &datagram-socket-hostname */
obj_t BGl_z62datagramzd2socketzd2hostnamez62zz__socketz00(obj_t BgL_envz00_2283, obj_t BgL_socketz00_2284)
{
{ /* Llib/socket.scm 569 */
{ /* Llib/socket.scm 570 */
 obj_t BgL_auxz00_3646;
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2284))
{ /* Llib/socket.scm 570 */
BgL_auxz00_3646 = BgL_socketz00_2284
; }  else 
{ 
 obj_t BgL_auxz00_3649;
BgL_auxz00_3649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(25394L), BGl_string2119z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2284); 
FAILURE(BgL_auxz00_3649,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2hostnamez00zz__socketz00(BgL_auxz00_3646);} } 

}



/* datagram-socket-host-address */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2hostzd2addresszd2zz__socketz00(obj_t BgL_socketz00_60)
{
{ /* Llib/socket.scm 575 */
return 
BGL_DATAGRAM_SOCKET_HOSTIP(BgL_socketz00_60);} 

}



/* &datagram-socket-host-address */
obj_t BGl_z62datagramzd2socketzd2hostzd2addresszb0zz__socketz00(obj_t BgL_envz00_2285, obj_t BgL_socketz00_2286)
{
{ /* Llib/socket.scm 575 */
{ /* Llib/socket.scm 576 */
 obj_t BgL_auxz00_3655;
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2286))
{ /* Llib/socket.scm 576 */
BgL_auxz00_3655 = BgL_socketz00_2286
; }  else 
{ 
 obj_t BgL_auxz00_3658;
BgL_auxz00_3658 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(25709L), BGl_string2121z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2286); 
FAILURE(BgL_auxz00_3658,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2hostzd2addresszd2zz__socketz00(BgL_auxz00_3655);} } 

}



/* datagram-socket-port-number */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2portzd2numberzd2zz__socketz00(obj_t BgL_socketz00_61)
{
{ /* Llib/socket.scm 581 */
return 
BINT(
BGL_DATAGRAM_SOCKET_PORTNUM(BgL_socketz00_61));} 

}



/* &datagram-socket-port-number */
obj_t BGl_z62datagramzd2socketzd2portzd2numberzb0zz__socketz00(obj_t BgL_envz00_2287, obj_t BgL_socketz00_2288)
{
{ /* Llib/socket.scm 581 */
{ /* Llib/socket.scm 582 */
 obj_t BgL_auxz00_3665;
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2288))
{ /* Llib/socket.scm 582 */
BgL_auxz00_3665 = BgL_socketz00_2288
; }  else 
{ 
 obj_t BgL_auxz00_3668;
BgL_auxz00_3668 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26021L), BGl_string2122z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2288); 
FAILURE(BgL_auxz00_3668,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2portzd2numberzd2zz__socketz00(BgL_auxz00_3665);} } 

}



/* datagram-socket-output */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2outputz00zz__socketz00(obj_t BgL_socketz00_62)
{
{ /* Llib/socket.scm 587 */
{ /* Llib/socket.scm 588 */
 bool_t BgL_test2420z00_3673;
{ /* Llib/socket.scm 588 */
 obj_t BgL_arg1396z00_2563;
BgL_arg1396z00_2563 = 
BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_62); 
BgL_test2420z00_3673 = 
OUTPUT_PORTP(BgL_arg1396z00_2563); } 
if(BgL_test2420z00_3673)
{ /* Llib/socket.scm 589 */
 obj_t BgL_aux1949z00_2564;
BgL_aux1949z00_2564 = 
BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_62); 
if(
OUTPUT_PORTP(BgL_aux1949z00_2564))
{ /* Llib/socket.scm 589 */
return BgL_aux1949z00_2564;}  else 
{ 
 obj_t BgL_auxz00_3679;
BgL_auxz00_3679 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26390L), BGl_string2123z00zz__socketz00, BGl_string2124z00zz__socketz00, BgL_aux1949z00_2564); 
FAILURE(BgL_auxz00_3679,BFALSE,BFALSE);} }  else 
{ /* Llib/socket.scm 590 */
 obj_t BgL_aux1951z00_2565;
BgL_aux1951z00_2565 = 
BGl_errorz00zz__errorz00(BGl_string2123z00zz__socketz00, BGl_string2125z00zz__socketz00, BgL_socketz00_62); 
if(
OUTPUT_PORTP(BgL_aux1951z00_2565))
{ /* Llib/socket.scm 590 */
return BgL_aux1951z00_2565;}  else 
{ 
 obj_t BgL_auxz00_3686;
BgL_auxz00_3686 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26428L), BGl_string2123z00zz__socketz00, BGl_string2124z00zz__socketz00, BgL_aux1951z00_2565); 
FAILURE(BgL_auxz00_3686,BFALSE,BFALSE);} } } } 

}



/* &datagram-socket-output */
obj_t BGl_z62datagramzd2socketzd2outputz62zz__socketz00(obj_t BgL_envz00_2289, obj_t BgL_socketz00_2290)
{
{ /* Llib/socket.scm 587 */
{ /* Llib/socket.scm 588 */
 obj_t BgL_auxz00_3690;
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2290))
{ /* Llib/socket.scm 588 */
BgL_auxz00_3690 = BgL_socketz00_2290
; }  else 
{ 
 obj_t BgL_auxz00_3693;
BgL_auxz00_3693 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26333L), BGl_string2126z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2290); 
FAILURE(BgL_auxz00_3693,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2outputz00zz__socketz00(BgL_auxz00_3690);} } 

}



/* datagram-socket-input */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2inputz00zz__socketz00(obj_t BgL_socketz00_63)
{
{ /* Llib/socket.scm 596 */
{ /* Llib/socket.scm 597 */
 bool_t BgL_test2424z00_3698;
{ /* Llib/socket.scm 597 */
 obj_t BgL_arg1399z00_2566;
BgL_arg1399z00_2566 = 
BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_63); 
BgL_test2424z00_3698 = 
INPUT_PORTP(BgL_arg1399z00_2566); } 
if(BgL_test2424z00_3698)
{ /* Llib/socket.scm 598 */
 obj_t BgL_aux1955z00_2567;
BgL_aux1955z00_2567 = 
BGL_DATAGRAM_SOCKET_PORT(BgL_socketz00_63); 
if(
INPUT_PORTP(BgL_aux1955z00_2567))
{ /* Llib/socket.scm 598 */
return BgL_aux1955z00_2567;}  else 
{ 
 obj_t BgL_auxz00_3704;
BgL_auxz00_3704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26838L), BGl_string2127z00zz__socketz00, BGl_string2128z00zz__socketz00, BgL_aux1955z00_2567); 
FAILURE(BgL_auxz00_3704,BFALSE,BFALSE);} }  else 
{ /* Llib/socket.scm 599 */
 obj_t BgL_aux1957z00_2568;
BgL_aux1957z00_2568 = 
BGl_errorz00zz__errorz00(BGl_string2127z00zz__socketz00, BGl_string2129z00zz__socketz00, BgL_socketz00_63); 
if(
INPUT_PORTP(BgL_aux1957z00_2568))
{ /* Llib/socket.scm 599 */
return BgL_aux1957z00_2568;}  else 
{ 
 obj_t BgL_auxz00_3711;
BgL_auxz00_3711 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26876L), BGl_string2127z00zz__socketz00, BGl_string2128z00zz__socketz00, BgL_aux1957z00_2568); 
FAILURE(BgL_auxz00_3711,BFALSE,BFALSE);} } } } 

}



/* &datagram-socket-input */
obj_t BGl_z62datagramzd2socketzd2inputz62zz__socketz00(obj_t BgL_envz00_2291, obj_t BgL_socketz00_2292)
{
{ /* Llib/socket.scm 596 */
{ /* Llib/socket.scm 597 */
 obj_t BgL_auxz00_3715;
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2292))
{ /* Llib/socket.scm 597 */
BgL_auxz00_3715 = BgL_socketz00_2292
; }  else 
{ 
 obj_t BgL_auxz00_3718;
BgL_auxz00_3718 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(26782L), BGl_string2130z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2292); 
FAILURE(BgL_auxz00_3718,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2inputz00zz__socketz00(BgL_auxz00_3715);} } 

}



/* _make-datagram-server-socket */
obj_t BGl__makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t BgL_env1159z00_67, obj_t BgL_opt1158z00_66)
{
{ /* Llib/socket.scm 605 */
{ /* Llib/socket.scm 605 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1158z00_66)) { case 0L : 

{ /* Llib/socket.scm 605 */
 obj_t BgL_domainz00_2570;
BgL_domainz00_2570 = BGl_symbol2036z00zz__socketz00; 
{ /* Llib/socket.scm 605 */

{ /* Llib/socket.scm 605 */
 obj_t BgL_res1795z00_2571;
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 607 */
 bool_t BgL_test2428z00_3724;
{ /* Llib/socket.scm 607 */
 bool_t BgL__ortest_1053z00_2572;
BgL__ortest_1053z00_2572 = 
(BgL_domainz00_2570==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1053z00_2572)
{ /* Llib/socket.scm 607 */
BgL_test2428z00_3724 = BgL__ortest_1053z00_2572
; }  else 
{ /* Llib/socket.scm 607 */
BgL_test2428z00_3724 = 
(BgL_domainz00_2570==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2428z00_3724)
{ /* Llib/socket.scm 607 */
BgL_res1795z00_2571 = 
bgl_make_datagram_server_socket(
(int)(0L), BgL_domainz00_2570); }  else 
{ /* Llib/socket.scm 611 */
 obj_t BgL_aux1961z00_2573;
BgL_aux1961z00_2573 = 
BGl_errorz00zz__errorz00(BGl_string2132z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2570); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1961z00_2573))
{ /* Llib/socket.scm 611 */
BgL_res1795z00_2571 = BgL_aux1961z00_2573; }  else 
{ 
 obj_t BgL_auxz00_3733;
BgL_auxz00_3733 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27384L), BGl_string2134z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1961z00_2573); 
FAILURE(BgL_auxz00_3733,BFALSE,BFALSE);} } } 
return BgL_res1795z00_2571;} } } break;case 1L : 

{ /* Llib/socket.scm 605 */
 obj_t BgL_portz00_2574;
BgL_portz00_2574 = 
VECTOR_REF(BgL_opt1158z00_66,0L); 
{ /* Llib/socket.scm 605 */
 obj_t BgL_domainz00_2575;
BgL_domainz00_2575 = BGl_symbol2036z00zz__socketz00; 
{ /* Llib/socket.scm 605 */

{ /* Llib/socket.scm 605 */
 obj_t BgL_res1796z00_2576;
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 607 */
 bool_t BgL_test2431z00_3739;
{ /* Llib/socket.scm 607 */
 bool_t BgL__ortest_1053z00_2577;
BgL__ortest_1053z00_2577 = 
(BgL_domainz00_2575==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1053z00_2577)
{ /* Llib/socket.scm 607 */
BgL_test2431z00_3739 = BgL__ortest_1053z00_2577
; }  else 
{ /* Llib/socket.scm 607 */
BgL_test2431z00_3739 = 
(BgL_domainz00_2575==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2431z00_3739)
{ /* Llib/socket.scm 609 */
 int BgL_tmpz00_3743;
{ /* Llib/socket.scm 609 */
 obj_t BgL_tmpz00_3744;
if(
INTEGERP(BgL_portz00_2574))
{ /* Llib/socket.scm 609 */
BgL_tmpz00_3744 = BgL_portz00_2574
; }  else 
{ 
 obj_t BgL_auxz00_3747;
BgL_auxz00_3747 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27351L), BGl_string2134z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_portz00_2574); 
FAILURE(BgL_auxz00_3747,BFALSE,BFALSE);} 
BgL_tmpz00_3743 = 
CINT(BgL_tmpz00_3744); } 
BgL_res1796z00_2576 = 
bgl_make_datagram_server_socket(BgL_tmpz00_3743, BgL_domainz00_2575); }  else 
{ /* Llib/socket.scm 611 */
 obj_t BgL_aux1964z00_2578;
BgL_aux1964z00_2578 = 
BGl_errorz00zz__errorz00(BGl_string2132z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2575); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1964z00_2578))
{ /* Llib/socket.scm 611 */
BgL_res1796z00_2576 = BgL_aux1964z00_2578; }  else 
{ 
 obj_t BgL_auxz00_3756;
BgL_auxz00_3756 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27384L), BGl_string2134z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1964z00_2578); 
FAILURE(BgL_auxz00_3756,BFALSE,BFALSE);} } } 
return BgL_res1796z00_2576;} } } } break;case 2L : 

{ /* Llib/socket.scm 605 */
 obj_t BgL_portz00_2579;
BgL_portz00_2579 = 
VECTOR_REF(BgL_opt1158z00_66,0L); 
{ /* Llib/socket.scm 605 */
 obj_t BgL_domainz00_2580;
BgL_domainz00_2580 = 
VECTOR_REF(BgL_opt1158z00_66,1L); 
{ /* Llib/socket.scm 605 */

{ /* Llib/socket.scm 605 */
 obj_t BgL_res1797z00_2581;
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 607 */
 bool_t BgL_test2435z00_3763;
{ /* Llib/socket.scm 607 */
 bool_t BgL__ortest_1053z00_2582;
BgL__ortest_1053z00_2582 = 
(BgL_domainz00_2580==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1053z00_2582)
{ /* Llib/socket.scm 607 */
BgL_test2435z00_3763 = BgL__ortest_1053z00_2582
; }  else 
{ /* Llib/socket.scm 607 */
BgL_test2435z00_3763 = 
(BgL_domainz00_2580==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2435z00_3763)
{ /* Llib/socket.scm 609 */
 obj_t BgL_auxz00_3776; int BgL_tmpz00_3767;
if(
SYMBOLP(BgL_domainz00_2580))
{ /* Llib/socket.scm 609 */
BgL_auxz00_3776 = BgL_domainz00_2580
; }  else 
{ 
 obj_t BgL_auxz00_3779;
BgL_auxz00_3779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27356L), BGl_string2134z00zz__socketz00, BGl_string2059z00zz__socketz00, BgL_domainz00_2580); 
FAILURE(BgL_auxz00_3779,BFALSE,BFALSE);} 
{ /* Llib/socket.scm 609 */
 obj_t BgL_tmpz00_3768;
if(
INTEGERP(BgL_portz00_2579))
{ /* Llib/socket.scm 609 */
BgL_tmpz00_3768 = BgL_portz00_2579
; }  else 
{ 
 obj_t BgL_auxz00_3771;
BgL_auxz00_3771 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27351L), BGl_string2134z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_portz00_2579); 
FAILURE(BgL_auxz00_3771,BFALSE,BFALSE);} 
BgL_tmpz00_3767 = 
CINT(BgL_tmpz00_3768); } 
BgL_res1797z00_2581 = 
bgl_make_datagram_server_socket(BgL_tmpz00_3767, BgL_auxz00_3776); }  else 
{ /* Llib/socket.scm 611 */
 obj_t BgL_aux1969z00_2583;
BgL_aux1969z00_2583 = 
BGl_errorz00zz__errorz00(BGl_string2132z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2580); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1969z00_2583))
{ /* Llib/socket.scm 611 */
BgL_res1797z00_2581 = BgL_aux1969z00_2583; }  else 
{ 
 obj_t BgL_auxz00_3787;
BgL_auxz00_3787 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27384L), BGl_string2134z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1969z00_2583); 
FAILURE(BgL_auxz00_3787,BFALSE,BFALSE);} } } 
return BgL_res1797z00_2581;} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2131z00zz__socketz00, BGl_string2133z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1158z00_66)));} } } } 

}



/* make-datagram-server-socket */
BGL_EXPORTED_DEF obj_t BGl_makezd2datagramzd2serverzd2socketzd2zz__socketz00(obj_t BgL_portz00_64, obj_t BgL_domainz00_65)
{
{ /* Llib/socket.scm 605 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 607 */
 bool_t BgL_test2440z00_3797;
{ /* Llib/socket.scm 607 */
 bool_t BgL__ortest_1053z00_2584;
BgL__ortest_1053z00_2584 = 
(BgL_domainz00_65==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1053z00_2584)
{ /* Llib/socket.scm 607 */
BgL_test2440z00_3797 = BgL__ortest_1053z00_2584
; }  else 
{ /* Llib/socket.scm 607 */
BgL_test2440z00_3797 = 
(BgL_domainz00_65==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2440z00_3797)
{ /* Llib/socket.scm 609 */
 obj_t BgL_auxz00_3810; int BgL_tmpz00_3801;
if(
SYMBOLP(BgL_domainz00_65))
{ /* Llib/socket.scm 609 */
BgL_auxz00_3810 = BgL_domainz00_65
; }  else 
{ 
 obj_t BgL_auxz00_3813;
BgL_auxz00_3813 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27356L), BGl_string2132z00zz__socketz00, BGl_string2059z00zz__socketz00, BgL_domainz00_65); 
FAILURE(BgL_auxz00_3813,BFALSE,BFALSE);} 
{ /* Llib/socket.scm 609 */
 obj_t BgL_tmpz00_3802;
if(
INTEGERP(BgL_portz00_64))
{ /* Llib/socket.scm 609 */
BgL_tmpz00_3802 = BgL_portz00_64
; }  else 
{ 
 obj_t BgL_auxz00_3805;
BgL_auxz00_3805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27351L), BGl_string2132z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_portz00_64); 
FAILURE(BgL_auxz00_3805,BFALSE,BFALSE);} 
BgL_tmpz00_3801 = 
CINT(BgL_tmpz00_3802); } 
return 
bgl_make_datagram_server_socket(BgL_tmpz00_3801, BgL_auxz00_3810);}  else 
{ /* Llib/socket.scm 611 */
 obj_t BgL_aux1974z00_2585;
BgL_aux1974z00_2585 = 
BGl_errorz00zz__errorz00(BGl_string2132z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_65); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1974z00_2585))
{ /* Llib/socket.scm 611 */
return BgL_aux1974z00_2585;}  else 
{ 
 obj_t BgL_auxz00_3821;
BgL_auxz00_3821 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27384L), BGl_string2132z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1974z00_2585); 
FAILURE(BgL_auxz00_3821,BFALSE,BFALSE);} } } } 

}



/* _make-datagram-unbound-socket */
obj_t BGl__makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t BgL_env1163z00_70, obj_t BgL_opt1162z00_69)
{
{ /* Llib/socket.scm 616 */
{ /* Llib/socket.scm 616 */

{ 

switch( 
VECTOR_LENGTH(BgL_opt1162z00_69)) { case 0L : 

{ /* Llib/socket.scm 616 */
 obj_t BgL_domainz00_2587;
BgL_domainz00_2587 = BGl_symbol2036z00zz__socketz00; 
{ /* Llib/socket.scm 616 */

{ /* Llib/socket.scm 616 */
 obj_t BgL_res1798z00_2588;
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 618 */
 bool_t BgL_test2445z00_3826;
{ /* Llib/socket.scm 618 */
 bool_t BgL__ortest_1056z00_2589;
BgL__ortest_1056z00_2589 = 
(BgL_domainz00_2587==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1056z00_2589)
{ /* Llib/socket.scm 618 */
BgL_test2445z00_3826 = BgL__ortest_1056z00_2589
; }  else 
{ /* Llib/socket.scm 618 */
BgL_test2445z00_3826 = 
(BgL_domainz00_2587==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2445z00_3826)
{ /* Llib/socket.scm 618 */
BgL_res1798z00_2588 = 
bgl_make_datagram_unbound_socket(BgL_domainz00_2587); }  else 
{ /* Llib/socket.scm 622 */
 obj_t BgL_aux1976z00_2590;
BgL_aux1976z00_2590 = 
BGl_errorz00zz__errorz00(BGl_string2136z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2587); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1976z00_2590))
{ /* Llib/socket.scm 622 */
BgL_res1798z00_2588 = BgL_aux1976z00_2590; }  else 
{ 
 obj_t BgL_auxz00_3834;
BgL_auxz00_3834 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27877L), BGl_string2137z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1976z00_2590); 
FAILURE(BgL_auxz00_3834,BFALSE,BFALSE);} } } 
return BgL_res1798z00_2588;} } } break;case 1L : 

{ /* Llib/socket.scm 616 */
 obj_t BgL_domainz00_2591;
BgL_domainz00_2591 = 
VECTOR_REF(BgL_opt1162z00_69,0L); 
{ /* Llib/socket.scm 616 */

{ /* Llib/socket.scm 616 */
 obj_t BgL_res1799z00_2592;
{ /* Llib/socket.scm 616 */
 obj_t BgL_domainz00_2593;
if(
SYMBOLP(BgL_domainz00_2591))
{ /* Llib/socket.scm 616 */
BgL_domainz00_2593 = BgL_domainz00_2591; }  else 
{ 
 obj_t BgL_auxz00_3841;
BgL_auxz00_3841 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27676L), BGl_string2137z00zz__socketz00, BGl_string2059z00zz__socketz00, BgL_domainz00_2591); 
FAILURE(BgL_auxz00_3841,BFALSE,BFALSE);} 
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 618 */
 bool_t BgL_test2449z00_3846;
{ /* Llib/socket.scm 618 */
 bool_t BgL__ortest_1056z00_2594;
BgL__ortest_1056z00_2594 = 
(BgL_domainz00_2593==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1056z00_2594)
{ /* Llib/socket.scm 618 */
BgL_test2449z00_3846 = BgL__ortest_1056z00_2594
; }  else 
{ /* Llib/socket.scm 618 */
BgL_test2449z00_3846 = 
(BgL_domainz00_2593==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2449z00_3846)
{ /* Llib/socket.scm 618 */
BgL_res1799z00_2592 = 
bgl_make_datagram_unbound_socket(BgL_domainz00_2593); }  else 
{ /* Llib/socket.scm 622 */
 obj_t BgL_aux1980z00_2595;
BgL_aux1980z00_2595 = 
BGl_errorz00zz__errorz00(BGl_string2136z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2593); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1980z00_2595))
{ /* Llib/socket.scm 622 */
BgL_res1799z00_2592 = BgL_aux1980z00_2595; }  else 
{ 
 obj_t BgL_auxz00_3854;
BgL_auxz00_3854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27877L), BGl_string2137z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1980z00_2595); 
FAILURE(BgL_auxz00_3854,BFALSE,BFALSE);} } } } 
return BgL_res1799z00_2592;} } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2135z00zz__socketz00, BGl_string2112z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1162z00_69)));} } } } 

}



/* make-datagram-unbound-socket */
BGL_EXPORTED_DEF obj_t BGl_makezd2datagramzd2unboundzd2socketzd2zz__socketz00(obj_t BgL_domainz00_68)
{
{ /* Llib/socket.scm 616 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 618 */
 bool_t BgL_test2452z00_3864;
{ /* Llib/socket.scm 618 */
 bool_t BgL__ortest_1056z00_2596;
BgL__ortest_1056z00_2596 = 
(BgL_domainz00_68==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1056z00_2596)
{ /* Llib/socket.scm 618 */
BgL_test2452z00_3864 = BgL__ortest_1056z00_2596
; }  else 
{ /* Llib/socket.scm 618 */
BgL_test2452z00_3864 = 
(BgL_domainz00_68==BGl_symbol2055z00zz__socketz00)
; } } 
if(BgL_test2452z00_3864)
{ /* Llib/socket.scm 618 */
BGL_TAIL return 
bgl_make_datagram_unbound_socket(BgL_domainz00_68);}  else 
{ /* Llib/socket.scm 622 */
 obj_t BgL_aux1982z00_2597;
BgL_aux1982z00_2597 = 
BGl_errorz00zz__errorz00(BGl_string2136z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_68); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1982z00_2597))
{ /* Llib/socket.scm 622 */
return BgL_aux1982z00_2597;}  else 
{ 
 obj_t BgL_auxz00_3872;
BgL_auxz00_3872 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(27877L), BGl_string2136z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1982z00_2597); 
FAILURE(BgL_auxz00_3872,BFALSE,BFALSE);} } } } 

}



/* _make-datagram-client-socket */
obj_t BGl__makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t BgL_env1167z00_76, obj_t BgL_opt1166z00_75)
{
{ /* Llib/socket.scm 627 */
{ /* Llib/socket.scm 627 */
 obj_t BgL_g1168z00_2598; obj_t BgL_g1169z00_2599;
BgL_g1168z00_2598 = 
VECTOR_REF(BgL_opt1166z00_75,0L); 
BgL_g1169z00_2599 = 
VECTOR_REF(BgL_opt1166z00_75,1L); 
{ 

switch( 
VECTOR_LENGTH(BgL_opt1166z00_75)) { case 2L : 

{ /* Llib/socket.scm 627 */
 obj_t BgL_domainz00_2601;
BgL_domainz00_2601 = BGl_symbol2036z00zz__socketz00; 
{ /* Llib/socket.scm 627 */

{ /* Llib/socket.scm 627 */
 obj_t BgL_res1800z00_2602;
{ /* Llib/socket.scm 627 */
 obj_t BgL_hostnamez00_2603; int BgL_portz00_2604;
if(
STRINGP(BgL_g1168z00_2598))
{ /* Llib/socket.scm 627 */
BgL_hostnamez00_2603 = BgL_g1168z00_2598; }  else 
{ 
 obj_t BgL_auxz00_3880;
BgL_auxz00_3880 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_g1168z00_2598); 
FAILURE(BgL_auxz00_3880,BFALSE,BFALSE);} 
{ /* Llib/socket.scm 627 */
 obj_t BgL_tmpz00_3884;
if(
INTEGERP(BgL_g1169z00_2599))
{ /* Llib/socket.scm 627 */
BgL_tmpz00_3884 = BgL_g1169z00_2599
; }  else 
{ 
 obj_t BgL_auxz00_3887;
BgL_auxz00_3887 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_g1169z00_2599); 
FAILURE(BgL_auxz00_3887,BFALSE,BFALSE);} 
BgL_portz00_2604 = 
CINT(BgL_tmpz00_3884); } 
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 629 */
 bool_t BgL_test2457z00_3893;
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1059z00_2605;
BgL__ortest_1059z00_2605 = 
(BgL_domainz00_2601==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1059z00_2605)
{ /* Llib/socket.scm 629 */
BgL_test2457z00_3893 = BgL__ortest_1059z00_2605
; }  else 
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1060z00_2606;
BgL__ortest_1060z00_2606 = 
(BgL_domainz00_2601==BGl_symbol2055z00zz__socketz00); 
if(BgL__ortest_1060z00_2606)
{ /* Llib/socket.scm 629 */
BgL_test2457z00_3893 = BgL__ortest_1060z00_2606
; }  else 
{ /* Llib/socket.scm 629 */
BgL_test2457z00_3893 = 
(BgL_domainz00_2601==BGl_symbol2057z00zz__socketz00)
; } } } 
if(BgL_test2457z00_3893)
{ /* Llib/socket.scm 629 */
BgL_res1800z00_2602 = 
bgl_make_datagram_client_socket(BgL_hostnamez00_2603, BgL_portz00_2604, ((bool_t)0), BgL_domainz00_2601); }  else 
{ /* Llib/socket.scm 633 */
 obj_t BgL_aux1987z00_2607;
BgL_aux1987z00_2607 = 
BGl_errorz00zz__errorz00(BGl_string2139z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2601); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1987z00_2607))
{ /* Llib/socket.scm 633 */
BgL_res1800z00_2602 = BgL_aux1987z00_2607; }  else 
{ 
 obj_t BgL_auxz00_3903;
BgL_auxz00_3903 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28424L), BGl_string2141z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1987z00_2607); 
FAILURE(BgL_auxz00_3903,BFALSE,BFALSE);} } } } 
return BgL_res1800z00_2602;} } } break;case 3L : 

{ /* Llib/socket.scm 627 */
 obj_t BgL_broadcastz00_2608;
BgL_broadcastz00_2608 = 
VECTOR_REF(BgL_opt1166z00_75,2L); 
{ /* Llib/socket.scm 627 */
 obj_t BgL_domainz00_2609;
BgL_domainz00_2609 = BGl_symbol2036z00zz__socketz00; 
{ /* Llib/socket.scm 627 */

{ /* Llib/socket.scm 627 */
 obj_t BgL_res1801z00_2610;
{ /* Llib/socket.scm 627 */
 obj_t BgL_hostnamez00_2611; int BgL_portz00_2612;
if(
STRINGP(BgL_g1168z00_2598))
{ /* Llib/socket.scm 627 */
BgL_hostnamez00_2611 = BgL_g1168z00_2598; }  else 
{ 
 obj_t BgL_auxz00_3910;
BgL_auxz00_3910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_g1168z00_2598); 
FAILURE(BgL_auxz00_3910,BFALSE,BFALSE);} 
{ /* Llib/socket.scm 627 */
 obj_t BgL_tmpz00_3914;
if(
INTEGERP(BgL_g1169z00_2599))
{ /* Llib/socket.scm 627 */
BgL_tmpz00_3914 = BgL_g1169z00_2599
; }  else 
{ 
 obj_t BgL_auxz00_3917;
BgL_auxz00_3917 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_g1169z00_2599); 
FAILURE(BgL_auxz00_3917,BFALSE,BFALSE);} 
BgL_portz00_2612 = 
CINT(BgL_tmpz00_3914); } 
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 629 */
 bool_t BgL_test2463z00_3923;
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1059z00_2613;
BgL__ortest_1059z00_2613 = 
(BgL_domainz00_2609==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1059z00_2613)
{ /* Llib/socket.scm 629 */
BgL_test2463z00_3923 = BgL__ortest_1059z00_2613
; }  else 
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1060z00_2614;
BgL__ortest_1060z00_2614 = 
(BgL_domainz00_2609==BGl_symbol2055z00zz__socketz00); 
if(BgL__ortest_1060z00_2614)
{ /* Llib/socket.scm 629 */
BgL_test2463z00_3923 = BgL__ortest_1060z00_2614
; }  else 
{ /* Llib/socket.scm 629 */
BgL_test2463z00_3923 = 
(BgL_domainz00_2609==BGl_symbol2057z00zz__socketz00)
; } } } 
if(BgL_test2463z00_3923)
{ /* Llib/socket.scm 629 */
BgL_res1801z00_2610 = 
bgl_make_datagram_client_socket(BgL_hostnamez00_2611, BgL_portz00_2612, 
CBOOL(BgL_broadcastz00_2608), BgL_domainz00_2609); }  else 
{ /* Llib/socket.scm 633 */
 obj_t BgL_aux1992z00_2615;
BgL_aux1992z00_2615 = 
BGl_errorz00zz__errorz00(BGl_string2139z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2609); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1992z00_2615))
{ /* Llib/socket.scm 633 */
BgL_res1801z00_2610 = BgL_aux1992z00_2615; }  else 
{ 
 obj_t BgL_auxz00_3934;
BgL_auxz00_3934 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28424L), BGl_string2141z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1992z00_2615); 
FAILURE(BgL_auxz00_3934,BFALSE,BFALSE);} } } } 
return BgL_res1801z00_2610;} } } } break;case 4L : 

{ /* Llib/socket.scm 627 */
 obj_t BgL_broadcastz00_2616;
BgL_broadcastz00_2616 = 
VECTOR_REF(BgL_opt1166z00_75,2L); 
{ /* Llib/socket.scm 627 */
 obj_t BgL_domainz00_2617;
BgL_domainz00_2617 = 
VECTOR_REF(BgL_opt1166z00_75,3L); 
{ /* Llib/socket.scm 627 */

{ /* Llib/socket.scm 627 */
 obj_t BgL_res1802z00_2618;
{ /* Llib/socket.scm 627 */
 obj_t BgL_hostnamez00_2619; int BgL_portz00_2620; obj_t BgL_domainz00_2621;
if(
STRINGP(BgL_g1168z00_2598))
{ /* Llib/socket.scm 627 */
BgL_hostnamez00_2619 = BgL_g1168z00_2598; }  else 
{ 
 obj_t BgL_auxz00_3942;
BgL_auxz00_3942 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_g1168z00_2598); 
FAILURE(BgL_auxz00_3942,BFALSE,BFALSE);} 
{ /* Llib/socket.scm 627 */
 obj_t BgL_tmpz00_3946;
if(
INTEGERP(BgL_g1169z00_2599))
{ /* Llib/socket.scm 627 */
BgL_tmpz00_3946 = BgL_g1169z00_2599
; }  else 
{ 
 obj_t BgL_auxz00_3949;
BgL_auxz00_3949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_g1169z00_2599); 
FAILURE(BgL_auxz00_3949,BFALSE,BFALSE);} 
BgL_portz00_2620 = 
CINT(BgL_tmpz00_3946); } 
if(
SYMBOLP(BgL_domainz00_2617))
{ /* Llib/socket.scm 627 */
BgL_domainz00_2621 = BgL_domainz00_2617; }  else 
{ 
 obj_t BgL_auxz00_3956;
BgL_auxz00_3956 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28170L), BGl_string2141z00zz__socketz00, BGl_string2059z00zz__socketz00, BgL_domainz00_2617); 
FAILURE(BgL_auxz00_3956,BFALSE,BFALSE);} 
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 629 */
 bool_t BgL_test2470z00_3961;
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1059z00_2622;
BgL__ortest_1059z00_2622 = 
(BgL_domainz00_2621==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1059z00_2622)
{ /* Llib/socket.scm 629 */
BgL_test2470z00_3961 = BgL__ortest_1059z00_2622
; }  else 
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1060z00_2623;
BgL__ortest_1060z00_2623 = 
(BgL_domainz00_2621==BGl_symbol2055z00zz__socketz00); 
if(BgL__ortest_1060z00_2623)
{ /* Llib/socket.scm 629 */
BgL_test2470z00_3961 = BgL__ortest_1060z00_2623
; }  else 
{ /* Llib/socket.scm 629 */
BgL_test2470z00_3961 = 
(BgL_domainz00_2621==BGl_symbol2057z00zz__socketz00)
; } } } 
if(BgL_test2470z00_3961)
{ /* Llib/socket.scm 629 */
BgL_res1802z00_2618 = 
bgl_make_datagram_client_socket(BgL_hostnamez00_2619, BgL_portz00_2620, 
CBOOL(BgL_broadcastz00_2616), BgL_domainz00_2621); }  else 
{ /* Llib/socket.scm 633 */
 obj_t BgL_aux1999z00_2624;
BgL_aux1999z00_2624 = 
BGl_errorz00zz__errorz00(BGl_string2139z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_2621); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux1999z00_2624))
{ /* Llib/socket.scm 633 */
BgL_res1802z00_2618 = BgL_aux1999z00_2624; }  else 
{ 
 obj_t BgL_auxz00_3972;
BgL_auxz00_3972 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28424L), BGl_string2141z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux1999z00_2624); 
FAILURE(BgL_auxz00_3972,BFALSE,BFALSE);} } } } 
return BgL_res1802z00_2618;} } } } break;
default: 
return 
BGl_errorz00zz__errorz00(BGl_symbol2138z00zz__socketz00, BGl_string2140z00zz__socketz00, 
BINT(
VECTOR_LENGTH(BgL_opt1166z00_75)));} } } } 

}



/* make-datagram-client-socket */
BGL_EXPORTED_DEF obj_t BGl_makezd2datagramzd2clientzd2socketzd2zz__socketz00(obj_t BgL_hostnamez00_71, int BgL_portz00_72, obj_t BgL_broadcastz00_73, obj_t BgL_domainz00_74)
{
{ /* Llib/socket.scm 627 */
BGl_z52socketzd2initz12z92zz__socketz00(); 
{ /* Llib/socket.scm 629 */
 bool_t BgL_test2474z00_3982;
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1059z00_2625;
BgL__ortest_1059z00_2625 = 
(BgL_domainz00_74==BGl_symbol2036z00zz__socketz00); 
if(BgL__ortest_1059z00_2625)
{ /* Llib/socket.scm 629 */
BgL_test2474z00_3982 = BgL__ortest_1059z00_2625
; }  else 
{ /* Llib/socket.scm 629 */
 bool_t BgL__ortest_1060z00_2626;
BgL__ortest_1060z00_2626 = 
(BgL_domainz00_74==BGl_symbol2055z00zz__socketz00); 
if(BgL__ortest_1060z00_2626)
{ /* Llib/socket.scm 629 */
BgL_test2474z00_3982 = BgL__ortest_1060z00_2626
; }  else 
{ /* Llib/socket.scm 629 */
BgL_test2474z00_3982 = 
(BgL_domainz00_74==BGl_symbol2057z00zz__socketz00)
; } } } 
if(BgL_test2474z00_3982)
{ /* Llib/socket.scm 629 */
return 
bgl_make_datagram_client_socket(BgL_hostnamez00_71, BgL_portz00_72, 
CBOOL(BgL_broadcastz00_73), BgL_domainz00_74);}  else 
{ /* Llib/socket.scm 633 */
 obj_t BgL_aux2001z00_2627;
BgL_aux2001z00_2627 = 
BGl_errorz00zz__errorz00(BGl_string2139z00zz__socketz00, BGl_string2072z00zz__socketz00, BgL_domainz00_74); 
if(
BGL_DATAGRAM_SOCKETP(BgL_aux2001z00_2627))
{ /* Llib/socket.scm 633 */
return BgL_aux2001z00_2627;}  else 
{ 
 obj_t BgL_auxz00_3993;
BgL_auxz00_3993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28424L), BGl_string2139z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_aux2001z00_2627); 
FAILURE(BgL_auxz00_3993,BFALSE,BFALSE);} } } } 

}



/* datagram-socket-close */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2closez00zz__socketz00(obj_t BgL_socketz00_77)
{
{ /* Llib/socket.scm 638 */
BGL_TAIL return 
bgl_datagram_socket_close(BgL_socketz00_77);} 

}



/* &datagram-socket-close */
obj_t BGl_z62datagramzd2socketzd2closez62zz__socketz00(obj_t BgL_envz00_2293, obj_t BgL_socketz00_2294)
{
{ /* Llib/socket.scm 638 */
{ /* Llib/socket.scm 639 */
 obj_t BgL_auxz00_3998;
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2294))
{ /* Llib/socket.scm 639 */
BgL_auxz00_3998 = BgL_socketz00_2294
; }  else 
{ 
 obj_t BgL_auxz00_4001;
BgL_auxz00_4001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(28765L), BGl_string2142z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2294); 
FAILURE(BgL_auxz00_4001,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2closez00zz__socketz00(BgL_auxz00_3998);} } 

}



/* datagram-socket-receive */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2receivez00zz__socketz00(obj_t BgL_socketz00_78, int BgL_lengthz00_79)
{
{ /* Llib/socket.scm 644 */
return 
bgl_datagram_socket_receive(BgL_socketz00_78, 
(long)(BgL_lengthz00_79));} 

}



/* &datagram-socket-receive */
obj_t BGl_z62datagramzd2socketzd2receivez62zz__socketz00(obj_t BgL_envz00_2295, obj_t BgL_socketz00_2296, obj_t BgL_lengthz00_2297)
{
{ /* Llib/socket.scm 644 */
{ /* Llib/socket.scm 645 */
 int BgL_auxz00_4015; obj_t BgL_auxz00_4008;
{ /* Llib/socket.scm 645 */
 obj_t BgL_tmpz00_4016;
if(
INTEGERP(BgL_lengthz00_2297))
{ /* Llib/socket.scm 645 */
BgL_tmpz00_4016 = BgL_lengthz00_2297
; }  else 
{ 
 obj_t BgL_auxz00_4019;
BgL_auxz00_4019 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29082L), BGl_string2143z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_lengthz00_2297); 
FAILURE(BgL_auxz00_4019,BFALSE,BFALSE);} 
BgL_auxz00_4015 = 
CINT(BgL_tmpz00_4016); } 
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2296))
{ /* Llib/socket.scm 645 */
BgL_auxz00_4008 = BgL_socketz00_2296
; }  else 
{ 
 obj_t BgL_auxz00_4011;
BgL_auxz00_4011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29082L), BGl_string2143z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2296); 
FAILURE(BgL_auxz00_4011,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2receivez00zz__socketz00(BgL_auxz00_4008, BgL_auxz00_4015);} } 

}



/* datagram-socket-send */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2sendz00zz__socketz00(obj_t BgL_socketz00_80, obj_t BgL_stringz00_81, obj_t BgL_hostz00_82, int BgL_portz00_83)
{
{ /* Llib/socket.scm 650 */
BGL_TAIL return 
bgl_datagram_socket_send(BgL_socketz00_80, BgL_stringz00_81, BgL_hostz00_82, BgL_portz00_83);} 

}



/* &datagram-socket-send */
obj_t BGl_z62datagramzd2socketzd2sendz62zz__socketz00(obj_t BgL_envz00_2298, obj_t BgL_socketz00_2299, obj_t BgL_stringz00_2300, obj_t BgL_hostz00_2301, obj_t BgL_portz00_2302)
{
{ /* Llib/socket.scm 650 */
{ /* Llib/socket.scm 651 */
 int BgL_auxz00_4047; obj_t BgL_auxz00_4040; obj_t BgL_auxz00_4033; obj_t BgL_auxz00_4026;
{ /* Llib/socket.scm 651 */
 obj_t BgL_tmpz00_4048;
if(
INTEGERP(BgL_portz00_2302))
{ /* Llib/socket.scm 651 */
BgL_tmpz00_4048 = BgL_portz00_2302
; }  else 
{ 
 obj_t BgL_auxz00_4051;
BgL_auxz00_4051 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29412L), BGl_string2144z00zz__socketz00, BGl_string2052z00zz__socketz00, BgL_portz00_2302); 
FAILURE(BgL_auxz00_4051,BFALSE,BFALSE);} 
BgL_auxz00_4047 = 
CINT(BgL_tmpz00_4048); } 
if(
STRINGP(BgL_hostz00_2301))
{ /* Llib/socket.scm 651 */
BgL_auxz00_4040 = BgL_hostz00_2301
; }  else 
{ 
 obj_t BgL_auxz00_4043;
BgL_auxz00_4043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29412L), BGl_string2144z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_hostz00_2301); 
FAILURE(BgL_auxz00_4043,BFALSE,BFALSE);} 
if(
STRINGP(BgL_stringz00_2300))
{ /* Llib/socket.scm 651 */
BgL_auxz00_4033 = BgL_stringz00_2300
; }  else 
{ 
 obj_t BgL_auxz00_4036;
BgL_auxz00_4036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29412L), BGl_string2144z00zz__socketz00, BGl_string2031z00zz__socketz00, BgL_stringz00_2300); 
FAILURE(BgL_auxz00_4036,BFALSE,BFALSE);} 
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2299))
{ /* Llib/socket.scm 651 */
BgL_auxz00_4026 = BgL_socketz00_2299
; }  else 
{ 
 obj_t BgL_auxz00_4029;
BgL_auxz00_4029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29412L), BGl_string2144z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2299); 
FAILURE(BgL_auxz00_4029,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2sendz00zz__socketz00(BgL_auxz00_4026, BgL_auxz00_4033, BgL_auxz00_4040, BgL_auxz00_4047);} } 

}



/* datagram-socket-option */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2optionz00zz__socketz00(obj_t BgL_socketz00_84, obj_t BgL_optionz00_85)
{
{ /* Llib/socket.scm 656 */
BGL_TAIL return 
bgl_getsockopt(BgL_socketz00_84, BgL_optionz00_85);} 

}



/* &datagram-socket-option */
obj_t BGl_z62datagramzd2socketzd2optionz62zz__socketz00(obj_t BgL_envz00_2303, obj_t BgL_socketz00_2304, obj_t BgL_optionz00_2305)
{
{ /* Llib/socket.scm 656 */
{ /* Llib/socket.scm 657 */
 obj_t BgL_auxz00_4065; obj_t BgL_auxz00_4058;
if(
KEYWORDP(BgL_optionz00_2305))
{ /* Llib/socket.scm 657 */
BgL_auxz00_4065 = BgL_optionz00_2305
; }  else 
{ 
 obj_t BgL_auxz00_4068;
BgL_auxz00_4068 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29741L), BGl_string2145z00zz__socketz00, BGl_string2117z00zz__socketz00, BgL_optionz00_2305); 
FAILURE(BgL_auxz00_4068,BFALSE,BFALSE);} 
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2304))
{ /* Llib/socket.scm 657 */
BgL_auxz00_4058 = BgL_socketz00_2304
; }  else 
{ 
 obj_t BgL_auxz00_4061;
BgL_auxz00_4061 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(29741L), BGl_string2145z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2304); 
FAILURE(BgL_auxz00_4061,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2optionz00zz__socketz00(BgL_auxz00_4058, BgL_auxz00_4065);} } 

}



/* datagram-socket-option-set! */
BGL_EXPORTED_DEF obj_t BGl_datagramzd2socketzd2optionzd2setz12zc0zz__socketz00(obj_t BgL_socketz00_86, obj_t BgL_optionz00_87, obj_t BgL_valz00_88)
{
{ /* Llib/socket.scm 662 */
BGL_TAIL return 
bgl_setsockopt(BgL_socketz00_86, BgL_optionz00_87, BgL_valz00_88);} 

}



/* &datagram-socket-option-set! */
obj_t BGl_z62datagramzd2socketzd2optionzd2setz12za2zz__socketz00(obj_t BgL_envz00_2306, obj_t BgL_socketz00_2307, obj_t BgL_optionz00_2308, obj_t BgL_valz00_2309)
{
{ /* Llib/socket.scm 662 */
{ /* Llib/socket.scm 663 */
 obj_t BgL_auxz00_4081; obj_t BgL_auxz00_4074;
if(
KEYWORDP(BgL_optionz00_2308))
{ /* Llib/socket.scm 663 */
BgL_auxz00_4081 = BgL_optionz00_2308
; }  else 
{ 
 obj_t BgL_auxz00_4084;
BgL_auxz00_4084 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(30060L), BGl_string2146z00zz__socketz00, BGl_string2117z00zz__socketz00, BgL_optionz00_2308); 
FAILURE(BgL_auxz00_4084,BFALSE,BFALSE);} 
if(
BGL_DATAGRAM_SOCKETP(BgL_socketz00_2307))
{ /* Llib/socket.scm 663 */
BgL_auxz00_4074 = BgL_socketz00_2307
; }  else 
{ 
 obj_t BgL_auxz00_4077;
BgL_auxz00_4077 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2025z00zz__socketz00, 
BINT(30060L), BGl_string2146z00zz__socketz00, BGl_string2120z00zz__socketz00, BgL_socketz00_2307); 
FAILURE(BgL_auxz00_4077,BFALSE,BFALSE);} 
return 
BGl_datagramzd2socketzd2optionzd2setz12zc0zz__socketz00(BgL_auxz00_4074, BgL_auxz00_4081, BgL_valz00_2309);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__socketz00(void)
{
{ /* Llib/socket.scm 14 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string2147z00zz__socketz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string2147z00zz__socketz00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string2147z00zz__socketz00));} 

}

#ifdef __cplusplus
}
#endif
