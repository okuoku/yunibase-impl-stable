/*===========================================================================*/
/*   (Llib/error.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/error.scm -indent -o objs/obj_s/Llib/error.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___ERROR_TYPE_DEFINITIONS
#define BGL___ERROR_TYPE_DEFINITIONS

/* object type definitions */
typedef struct BgL_z62exceptionz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
} *BgL_z62exceptionz62_bglt;

typedef struct BgL_z62errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62errorz62_bglt;

typedef struct BgL_z62typezd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
   obj_t BgL_typez00;
} *BgL_z62typezd2errorzb0_bglt;

typedef struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
   obj_t BgL_indexz00;
} *BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt;

typedef struct BgL_z62iozd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2errorzb0_bglt;

typedef struct BgL_z62iozd2portzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2portzd2errorz62_bglt;

typedef struct BgL_z62iozd2readzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2readzd2errorz62_bglt;

typedef struct BgL_z62iozd2writezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2writezd2errorz62_bglt;

typedef struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt;

typedef struct BgL_z62iozd2parsezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2parsezd2errorz62_bglt;

typedef struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt;

typedef struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt;

typedef struct BgL_z62iozd2sigpipezd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2sigpipezd2errorz62_bglt;

typedef struct BgL_z62iozd2timeoutzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2timeoutzd2errorz62_bglt;

typedef struct BgL_z62iozd2connectionzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62iozd2connectionzd2errorz62_bglt;

typedef struct BgL_z62processzd2exceptionzb0_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62processzd2exceptionzb0_bglt;

typedef struct BgL_z62stackzd2overflowzd2errorz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_procz00;
   obj_t BgL_msgz00;
   obj_t BgL_objz00;
} *BgL_z62stackzd2overflowzd2errorz62_bglt;

typedef struct BgL_z62warningz62_bgl {
   header_t header;
   obj_t widening;
   obj_t BgL_fnamez00;
   obj_t BgL_locationz00;
   obj_t BgL_stackz00;
   obj_t BgL_argsz00;
} *BgL_z62warningz62_bglt;


#endif // BGL___ERROR_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_z62exitz62zz__errorz00(obj_t, obj_t);
extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
extern obj_t BGl_z62processzd2exceptionzb0zz__objectz00;
static obj_t BGl_zc3z04exitza32251ze3ze70z60zz__errorz00(obj_t, obj_t);
static obj_t BGl_relativeze70ze7zz__errorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_setzd2errorzd2handlerz12z12zz__errorz00(obj_t);
static obj_t BGl_z62errorzd2notifyzb0zz__errorz00(obj_t, obj_t);
extern int BGl_bigloozd2warningzd2zz__paramz00(void);
extern obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t, unsigned char, unsigned char);
static obj_t BGl_fixzd2tabulationz12zc0zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t the_failure(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza32253ze3ze5zz__errorz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__errorz00 = BUNSPEC;
extern obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2tracezd2stackzd2sourcezd2zz__errorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_raisez00zz__errorz00(obj_t);
extern obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__errorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_errorzd2notifyzf2locationz20zz__errorz00(obj_t, obj_t, int);
extern obj_t BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00;
BGL_EXPORTED_DECL obj_t BGl_exitz00zz__errorz00(obj_t);
static obj_t BGl_symbol2904z00zz__errorz00 = BUNSPEC;
static obj_t BGl_symbol2823z00zz__errorz00 = BUNSPEC;
extern obj_t BGl_applyz00zz__r4_control_features_6_9z00(obj_t, obj_t, obj_t);
static obj_t BGl_z62errorzf2czd2locationz42zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00;
static obj_t BGl_z62z62tryz00zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_errorzf2sourcezd2locationz20zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62setzd2errorzd2handlerz12z70zz__errorz00(obj_t, obj_t);
static obj_t BGl_z62errorzf2locationz90zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_envzd2getzd2errorzd2handlerzd2zz__errorz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long, char *);
extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_system_failure(int, obj_t, obj_t, obj_t);
extern obj_t BGl_za2classesza2z00zz__objectz00;
extern obj_t BGl_unwindzd2untilz12zc0zz__bexitz00(obj_t, obj_t);
extern obj_t BGl_z62errorz62zz__objectz00;
extern obj_t BGl_z62iozd2sigpipezd2errorz62zz__objectz00;
extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
extern bool_t fexists(char *);
BGL_EXPORTED_DECL obj_t BGl_warningz00zz__errorz00(obj_t);
static obj_t BGl_relativezd2filezd2namez00zz__errorz00(obj_t);
static obj_t BGl_z62displayzd2tracezd2stackzd2sourcezb0zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
extern obj_t BGl_z62warningz62zz__objectz00;
static obj_t BGl_z62sigsegvzd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_z62tryz62zz__errorz00(obj_t, obj_t);
extern obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_toplevelzd2initzd2zz__errorz00(void);
static obj_t BGl_z62indexzd2outzd2ofzd2bounds2770zb0zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_z62iozd2writezd2errorz62zz__objectz00;
BGL_EXPORTED_DECL obj_t BGl_warningzf2czd2locationz20zz__errorz00(char *, long, obj_t);
extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long, long);
extern obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_z62stackzd2overflowzd2errorz62zz__objectz00;
extern obj_t BGl_z62typezd2errorzb0zz__objectz00;
static obj_t BGl_uncygdrivez00zz__errorz00(obj_t);
extern obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62warningzd2notifyzb0zz__errorz00(obj_t, obj_t);
extern obj_t BGl_z62iozd2portzd2errorz62zz__objectz00;
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t BGl_getenvz00zz__osz00(obj_t);
static obj_t BGl_locationzd2linezd2numz00zz__errorz00(obj_t);
static obj_t BGl_filenamezd2forzd2errorz00zz__errorz00(obj_t, long);
extern bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
extern obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
extern obj_t BGl_z62iozd2connectionzd2errorz62zz__objectz00;
BGL_EXPORTED_DECL obj_t BGl_notifyzd2interruptzd2zz__errorz00(int);
static obj_t BGl_z62typezd2error2769zb0zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__errorz00(void);
static obj_t BGl_z62raisez62zz__errorz00(obj_t, obj_t);
extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
extern obj_t bgl_reverse(obj_t);
static obj_t BGl_z62typeofz62zz__errorz00(obj_t, obj_t);
extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
static obj_t BGl_z62errorzd2notifyzf2locationz42zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_genericzd2initzd2zz__errorz00(void);
extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t);
static obj_t BGl_z62escz62zz__errorz00(obj_t, obj_t);
extern long bgl_list_length(obj_t);
BGL_EXPORTED_DECL obj_t BGl_warningzd2notifyzf2locationz20zz__errorz00(obj_t, obj_t, int);
static obj_t BGl_symbol2860z00zz__errorz00 = BUNSPEC;
extern obj_t BGl_z62iozd2timeoutzd2errorz62zz__objectz00;
static obj_t BGl_z62warningzd2notifyzf2locationz42zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__errorz00(void);
static obj_t BGl_defaultzd2interruptzd2notifierz00zz__errorz00(int);
BGL_EXPORTED_DECL obj_t bgl_stack_overflow_error(void);
static obj_t BGl_symbol2869z00zz__errorz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__errorz00(void);
static obj_t BGl_z62currentzd2exceptionzd2handlerz62zz__errorz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__errorz00(obj_t, obj_t);
static obj_t BGl_z62warningzf2czd2locationz42zz__errorz00(obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_z62iozd2errorzb0zz__objectz00;
BGL_EXPORTED_DECL obj_t BGl_dumpzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_displayzd2tracezd2stackz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62errorzf2sourcezd2locationz42zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62stackzd2overflowzd2erro2767z62zz__errorz00(obj_t);
static obj_t BGl_objectzd2initzd2zz__errorz00(void);
static obj_t BGl_z62envzd2getzd2errorzd2handlerzb0zz__errorz00(obj_t, obj_t);
extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_pushzd2errorzd2handlerz12z12zz__errorz00(obj_t, obj_t);
static obj_t BGl_symbol2871z00zz__errorz00 = BUNSPEC;
static obj_t BGl_z62bigloozd2typezd2errorz62zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62warningzf2locz90zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62withzd2exceptionzd2handlerz62zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62envzd2setzd2errorzd2handlerz12za2zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62errorzf2sourcez90zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
extern obj_t BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00;
static obj_t BGl_z62sigbuszd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
extern obj_t BGl_z62exceptionz62zz__objectz00;
static obj_t BGl_openzd2forzd2errorz00zz__errorz00(obj_t);
extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
extern obj_t bgl_reverse_bang(obj_t);
static obj_t BGl_dozd2warnzf2locationz20zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_dirnamez00zz__osz00(obj_t);
static obj_t BGl_z62modulezd2initzd2errorz62zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62sigfpezd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
static obj_t BGl_locationzd2linezd2colze70ze7zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62findzd2runtimezd2typez62zz__errorz00(obj_t, obj_t);
static obj_t BGl_z62error2768z62zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62getzd2errorzd2handlerz62zz__errorz00(obj_t);
extern obj_t BGl_pwdz00zz__osz00(void);
BGL_EXPORTED_DECL obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__errorz00(obj_t, obj_t);
static obj_t BGl_printzd2cursorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_warningzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
static obj_t BGl_dirnamezd2ze3listz31zz__errorz00(obj_t);
static obj_t BGl_locationzd2atze70z35zz__errorz00(obj_t, obj_t);
static obj_t BGl_symbol2891z00zz__errorz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_errorzf2sourcezf2zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol2893z00zz__errorz00 = BUNSPEC;
static obj_t BGl_z62warning2771z62zz__errorz00(obj_t, obj_t);
extern obj_t BGl_z62conditionz62zz__objectz00;
static obj_t BGl_z62the_failurez62zz__errorz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_zc3z04exitza31686ze3ze70z60zz__errorz00(obj_t, obj_t, obj_t);
extern bool_t bgl_directoryp(char *);
extern obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL char * bgl_show_type(obj_t);
static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__errorz00(obj_t, obj_t);
static obj_t BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00(obj_t, obj_t, obj_t, long);
extern obj_t BGl_basenamez00zz__osz00(obj_t);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_currentzd2exceptionzd2handlerz00zz__errorz00(void);
static obj_t BGl_warningzf2locationzd2filez20zz__errorz00(obj_t, obj_t, obj_t);
static BgL_z62typezd2errorzb0_bglt BGl_typenamezd2errorzd2zz__errorz00(bool_t, bool_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62bigloozd2typezd2errorzd2msgzb0zz__errorz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31799ze3ze5zz__errorz00(obj_t);
static obj_t BGl_methodzd2initzd2zz__errorz00(void);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_notifyzd2z62errorzf2locz42zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62errorzf2errnoz90zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
static obj_t BGl_notifyzd2z62errorzf2locationzd2nozd2locz42zz__errorz00(obj_t);
static obj_t BGl__displayzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t bgl_typeof(obj_t);
static obj_t BGl_z62notifyzd2interruptzb0zz__errorz00(obj_t, obj_t);
extern int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initzd2errorz00zz__errorz00(char *, char *);
BGL_EXPORTED_DECL obj_t bgl_find_runtime_type(obj_t);
BGL_EXPORTED_DECL obj_t BGl_getzd2errorzd2handlerz00zz__errorz00(void);
BGL_EXPORTED_DECL obj_t BGl_errorzf2czd2locationz20zz__errorz00(obj_t, obj_t, obj_t, char *, long);
BGL_EXPORTED_DECL obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
extern bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_displayz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_z62dumpzd2tracezd2stackz62zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_envzd2setzd2errorzd2handlerz12zc0zz__errorz00(obj_t, obj_t);
extern obj_t make_string(long, unsigned char);
extern obj_t bgl_close_input_port(obj_t);
static obj_t BGl_z62zc3z04anonymousza31835ze3ze5zz__errorz00(obj_t);
static bool_t BGl_exceptionzd2locationzf3z21zz__errorz00(obj_t);
static obj_t BGl_z62zc3z04anonymousza31495ze3ze5zz__errorz00(obj_t, obj_t);
static obj_t BGl_z62czd2debuggingzd2showzd2typezb0zz__errorz00(obj_t, obj_t);
extern obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
static obj_t BGl_z62sigillzd2errorzd2handlerz62zz__errorz00(obj_t, obj_t);
static obj_t BGl_defaultzd2exceptionzd2handlerz00zz__errorz00(obj_t);
static obj_t BGl_z62bigloozd2typezd2errorzf2locationz90zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
extern long BGl_stringzd2ze3integerz31zz__r4_numbers_6_5_fixnumz00(obj_t, long, long);
extern obj_t BGl_fprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
extern obj_t BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00;
static obj_t BGl__getzd2tracezd2stackz00zz__errorz00(obj_t, obj_t);
extern obj_t BGl_signalz00zz__osz00(int, obj_t);
extern obj_t BGl_z62iozd2readzd2errorz62zz__objectz00;
extern bool_t bigloo_strncmp(obj_t, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_warningzf2loczf2zz__errorz00(obj_t, obj_t);
static obj_t BGl_z62warningzf2locationz90zz__errorz00(obj_t, obj_t, obj_t, obj_t);
extern long BGl_maxfxz00zz__r4_numbers_6_5_fixnumz00(long, obj_t);
static obj_t BGl_z62pushzd2errorzd2handlerz12z70zz__errorz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t BGl_valuesz00zz__r5_control_features_6_4z00(obj_t);
extern obj_t BGl_newlinez00zz__r4_output_6_10_3z00(obj_t);
static obj_t BGl_notifyzd2z62errorzb0zz__errorz00(obj_t);
static obj_t *__cnst;


DEFINE_EXPORT_BGL_PROCEDURE( BGl_envzd2setzd2errorzd2handlerz12zd2envz12zz__errorz00, BgL_bgl_za762envza7d2setza7d2e2970za7, BGl_z62envzd2setzd2errorzd2handlerz12za2zz__errorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_the_failurezd2envzd2zz__errorz00, BgL_bgl_za762the_failureza762971z00, BGl_z62the_failurez62zz__errorz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzf2sourcezd2envz20zz__errorz00, BgL_bgl_za762errorza7f2sourc2972z00, BGl_z62errorzf2sourcez90zz__errorz00, 0L, BUNSPEC, 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2830z00zz__errorz00, BgL_bgl_za762za7c3za704anonymo2973za7, BGl_z62zc3z04anonymousza31458ze3ze5zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2920z00zz__errorz00, BgL_bgl_string2920za700za7za7_2974za7, "epair", 5 );
DEFINE_STRING( BGl_string2921z00zz__errorz00, BgL_bgl_string2921za700za7za7_2975za7, "pair", 4 );
DEFINE_STRING( BGl_string2840z00zz__errorz00, BgL_bgl_string2840za700za7za7_2976za7, "'.\n", 3 );
DEFINE_STRING( BGl_string2922z00zz__errorz00, BgL_bgl_string2922za700za7za7_2977za7, "class", 5 );
DEFINE_STRING( BGl_string2841z00zz__errorz00, BgL_bgl_string2841za700za7za7_2978za7, "' is inconsistently initialized by module `", 43 );
DEFINE_STRING( BGl_string2923z00zz__errorz00, BgL_bgl_string2923za700za7za7_2979za7, "vector", 6 );
DEFINE_STRING( BGl_string2842z00zz__errorz00, BgL_bgl_string2842za700za7za7_2980za7, "Module `", 8 );
DEFINE_STRING( BGl_string2924z00zz__errorz00, BgL_bgl_string2924za700za7za7_2981za7, "tvector", 7 );
DEFINE_STRING( BGl_string2843z00zz__errorz00, BgL_bgl_string2843za700za7za7_2982za7, ":Inconsistent module initialization\n", 36 );
DEFINE_STRING( BGl_string2925z00zz__errorz00, BgL_bgl_string2925za700za7za7_2983za7, "struct:", 7 );
DEFINE_STRING( BGl_string2844z00zz__errorz00, BgL_bgl_string2844za700za7za7_2984za7, "*** ERROR:", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_notifyzd2interruptzd2envz00zz__errorz00, BgL_bgl_za762notifyza7d2inte2985z00, BGl_z62notifyzd2interruptzb0zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2926z00zz__errorz00, BgL_bgl_string2926za700za7za7_2986za7, "input-port", 10 );
DEFINE_STRING( BGl_string2845z00zz__errorz00, BgL_bgl_string2845za700za7za7_2987za7, "&module-init-error", 18 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_raisezd2envzd2zz__errorz00, BgL_bgl_za762raiseza762za7za7__e2988z00, BGl_z62raisez62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2927z00zz__errorz00, BgL_bgl_string2927za700za7za7_2989za7, "binary-port", 11 );
DEFINE_STRING( BGl_string2846z00zz__errorz00, BgL_bgl_string2846za700za7za7_2990za7, "bstring", 7 );
DEFINE_STRING( BGl_string2928z00zz__errorz00, BgL_bgl_string2928za700za7za7_2991za7, "cell", 4 );
DEFINE_STRING( BGl_string2847z00zz__errorz00, BgL_bgl_string2847za700za7za7_2992za7, "&error/c-location", 17 );
DEFINE_STRING( BGl_string2929z00zz__errorz00, BgL_bgl_string2929za700za7za7_2993za7, "foreign:", 8 );
DEFINE_STRING( BGl_string2848z00zz__errorz00, BgL_bgl_string2848za700za7za7_2994za7, "\" provided", 10 );
DEFINE_STRING( BGl_string2849z00zz__errorz00, BgL_bgl_string2849za700za7za7_2995za7, "\" expected, \"", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzd2notifyzf2locationzd2envzf2zz__errorz00, BgL_bgl_za762errorza7d2notif2996z00, BGl_z62errorzd2notifyzf2locationz42zz__errorz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2tracezd2stackzd2envzd2zz__errorz00, BgL_bgl__getza7d2traceza7d2s2997z00, opt_generic_entry, BGl__getzd2tracezd2stackz00zz__errorz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_currentzd2exceptionzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762currentza7d2exc2998z00, BGl_z62currentzd2exceptionzd2handlerz62zz__errorz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2930z00zz__errorz00, BgL_bgl_string2930za700za7za7_2999za7, "socket", 6 );
DEFINE_STRING( BGl_string2931z00zz__errorz00, BgL_bgl_string2931za700za7za7_3000za7, "datagram-socket", 15 );
DEFINE_STRING( BGl_string2850z00zz__errorz00, BgL_bgl_string2850za700za7za7_3001za7, " \"", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzd2notifyzd2envz00zz__errorz00, BgL_bgl_za762errorza7d2notif3002z00, BGl_z62errorzd2notifyzb0zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzf2sourcezd2locationzd2envzf2zz__errorz00, BgL_bgl_za762errorza7f2sourc3003z00, BGl_z62errorzf2sourcezd2locationz42zz__errorz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string2932z00zz__errorz00, BgL_bgl_string2932za700za7za7_3004za7, "process", 7 );
DEFINE_STRING( BGl_string2851z00zz__errorz00, BgL_bgl_string2851za700za7za7_3005za7, "&bigloo-type-error-msg", 22 );
DEFINE_STRING( BGl_string2933z00zz__errorz00, BgL_bgl_string2933za700za7za7_3006za7, "custom", 6 );
DEFINE_STRING( BGl_string2852z00zz__errorz00, BgL_bgl_string2852za700za7za7_3007za7, "\077\077?", 3 );
DEFINE_STRING( BGl_string2934z00zz__errorz00, BgL_bgl_string2934za700za7za7_3008za7, "opaque", 6 );
DEFINE_STRING( BGl_string2853z00zz__errorz00, BgL_bgl_string2853za700za7za7_3009za7, "Type", 4 );
DEFINE_STRING( BGl_string2935z00zz__errorz00, BgL_bgl_string2935za700za7za7_3010za7, "_", 1 );
DEFINE_STRING( BGl_string2854z00zz__errorz00, BgL_bgl_string2854za700za7za7_3011za7, "]", 1 );
DEFINE_STRING( BGl_string2936z00zz__errorz00, BgL_bgl_string2936za700za7za7_3012za7, "ucs2string", 10 );
DEFINE_STRING( BGl_string2855z00zz__errorz00, BgL_bgl_string2855za700za7za7_3013za7, " out of range [0..", 18 );
DEFINE_STRING( BGl_string2937z00zz__errorz00, BgL_bgl_string2937za700za7za7_3014za7, "ucs2", 4 );
DEFINE_STRING( BGl_string2856z00zz__errorz00, BgL_bgl_string2856za700za7za7_3015za7, "index ", 6 );
DEFINE_STRING( BGl_string2938z00zz__errorz00, BgL_bgl_string2938za700za7za7_3016za7, "elong", 5 );
DEFINE_STRING( BGl_string2857z00zz__errorz00, BgL_bgl_string2857za700za7za7_3017za7, "&index-out-of-bounds2770", 24 );
DEFINE_STRING( BGl_string2939z00zz__errorz00, BgL_bgl_string2939za700za7za7_3018za7, "llong", 5 );
DEFINE_STRING( BGl_string2858z00zz__errorz00, BgL_bgl_string2858za700za7za7_3019za7, "&warning/c-location", 19 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_envzd2getzd2errorzd2handlerzd2envz00zz__errorz00, BgL_bgl_za762envza7d2getza7d2e3020za7, BGl_z62envzd2getzd2errorzd2handlerzb0zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2859z00zz__errorz00, BgL_bgl_string2859za700za7za7_3021za7, ":\n", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2typezd2errorzd2envzd2zz__errorz00, BgL_bgl_za762biglooza7d2type3022z00, BGl_z62bigloozd2typezd2errorz62zz__errorz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_pushzd2errorzd2handlerz12zd2envzc0zz__errorz00, BgL_bgl_za762pushza7d2errorza73023za7, BGl_z62pushzd2errorzd2handlerz12z70zz__errorz00, 0L, BUNSPEC, 2 );
extern obj_t BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00;
DEFINE_EXPORT_BGL_PROCEDURE( BGl_indexzd2outzd2ofzd2boundszd2errorzd2envzd2zz__errorz00, BgL_bgl_za762indexza7d2outza7d3024za7, BGl_z62indexzd2outzd2ofzd2bounds2770zb0zz__errorz00, 0L, BUNSPEC, 6 );
DEFINE_STRING( BGl_string2940z00zz__errorz00, BgL_bgl_string2940za700za7za7_3025za7, "mutex", 5 );
DEFINE_STRING( BGl_string2941z00zz__errorz00, BgL_bgl_string2941za700za7za7_3026za7, "condvar", 7 );
DEFINE_STRING( BGl_string2942z00zz__errorz00, BgL_bgl_string2942za700za7za7_3027za7, "date", 4 );
DEFINE_STRING( BGl_string2861z00zz__errorz00, BgL_bgl_string2861za700za7za7_3028za7, "%no-error-obj", 13 );
DEFINE_STRING( BGl_string2943z00zz__errorz00, BgL_bgl_string2943za700za7za7_3029za7, "bignum", 6 );
DEFINE_STRING( BGl_string2862z00zz__errorz00, BgL_bgl_string2862za700za7za7_3030za7, " -- ", 4 );
DEFINE_STRING( BGl_string2944z00zz__errorz00, BgL_bgl_string2944za700za7za7_3031za7, "mmap", 4 );
DEFINE_STRING( BGl_string2863z00zz__errorz00, BgL_bgl_string2863za700za7za7_3032za7, "\", character ", 13 );
DEFINE_STRING( BGl_string2945z00zz__errorz00, BgL_bgl_string2945za700za7za7_3033za7, "regexp", 6 );
DEFINE_STRING( BGl_string2864z00zz__errorz00, BgL_bgl_string2864za700za7za7_3034za7, "File \"", 6 );
DEFINE_STRING( BGl_string2946z00zz__errorz00, BgL_bgl_string2946za700za7za7_3035za7, "int8", 4 );
DEFINE_STRING( BGl_string2865z00zz__errorz00, BgL_bgl_string2865za700za7za7_3036za7, "...", 3 );
DEFINE_STRING( BGl_string2947z00zz__errorz00, BgL_bgl_string2947za700za7za7_3037za7, "uint8", 5 );
DEFINE_STRING( BGl_string2866z00zz__errorz00, BgL_bgl_string2866za700za7za7_3038za7, "", 0 );
DEFINE_STRING( BGl_string2948z00zz__errorz00, BgL_bgl_string2948za700za7za7_3039za7, "int16", 5 );
DEFINE_STRING( BGl_string2867z00zz__errorz00, BgL_bgl_string2867za700za7za7_3040za7, "stdin", 5 );
DEFINE_STRING( BGl_string2949z00zz__errorz00, BgL_bgl_string2949za700za7za7_3041za7, "uint16", 6 );
DEFINE_STRING( BGl_string2868z00zz__errorz00, BgL_bgl_string2868za700za7za7_3042za7, "string://", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_modulezd2initzd2errorzd2envzd2zz__errorz00, BgL_bgl_za762moduleza7d2init3043z00, BGl_z62modulezd2initzd2errorz62zz__errorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_getzd2errorzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762getza7d2errorza7d3044za7, BGl_z62getzd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 0 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzf2czd2locationzd2envzf2zz__errorz00, BgL_bgl_za762errorza7f2cza7d2l3045za7, BGl_z62errorzf2czd2locationz42zz__errorz00, 0L, BUNSPEC, 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2typezd2errorzf2locationzd2envz20zz__errorz00, BgL_bgl_za762biglooza7d2type3046z00, BGl_z62bigloozd2typezd2errorzf2locationz90zz__errorz00, 0L, BUNSPEC, 5 );
DEFINE_STRING( BGl_string2950z00zz__errorz00, BgL_bgl_string2950za700za7za7_3047za7, "int32", 5 );
DEFINE_STRING( BGl_string2951z00zz__errorz00, BgL_bgl_string2951za700za7za7_3048za7, "uint32", 6 );
DEFINE_STRING( BGl_string2870z00zz__errorz00, BgL_bgl_string2870za700za7za7_3049za7, "line-col", 8 );
DEFINE_STRING( BGl_string2952z00zz__errorz00, BgL_bgl_string2952za700za7za7_3050za7, "int64", 5 );
DEFINE_STRING( BGl_string2953z00zz__errorz00, BgL_bgl_string2953za700za7za7_3051za7, "uint64", 6 );
DEFINE_STRING( BGl_string2872z00zz__errorz00, BgL_bgl_string2872za700za7za7_3052za7, "line", 4 );
DEFINE_STRING( BGl_string2954z00zz__errorz00, BgL_bgl_string2954za700za7za7_3053za7, "bcnst", 5 );
DEFINE_STRING( BGl_string2873z00zz__errorz00, BgL_bgl_string2873za700za7za7_3054za7, "win32", 5 );
DEFINE_STRING( BGl_string2955z00zz__errorz00, BgL_bgl_string2955za700za7za7_3055za7, "&&try", 5 );
DEFINE_STRING( BGl_string2874z00zz__errorz00, BgL_bgl_string2874za700za7za7_3056za7, "<eof>", 5 );
DEFINE_STRING( BGl_string2956z00zz__errorz00, BgL_bgl_string2956za700za7za7_3057za7, "&push-error-handler!", 20 );
DEFINE_STRING( BGl_string2875z00zz__errorz00, BgL_bgl_string2875za700za7za7_3058za7, "*** CONDITION: ", 15 );
DEFINE_STRING( BGl_string2957z00zz__errorz00, BgL_bgl_string2957za700za7za7_3059za7, "&env-set-error-handler!", 23 );
DEFINE_STRING( BGl_string2876z00zz__errorz00, BgL_bgl_string2876za700za7za7_3060za7, "&error-notify/location", 22 );
DEFINE_STRING( BGl_string2958z00zz__errorz00, BgL_bgl_string2958za700za7za7_3061za7, "dynamic-env", 11 );
DEFINE_STRING( BGl_string2877z00zz__errorz00, BgL_bgl_string2877za700za7za7_3062za7, "*** WARNING: ", 13 );
DEFINE_STRING( BGl_string2959z00zz__errorz00, BgL_bgl_string2959za700za7za7_3063za7, "&env-get-error-handler", 22 );
DEFINE_STRING( BGl_string2879z00zz__errorz00, BgL_bgl_string2879za700za7za7_3064za7, "[string]", 8 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_sigbuszd2errorzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762sigbusza7d2erro3065z00, BGl_z62sigbuszd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2960z00zz__errorz00, BgL_bgl_string2960za700za7za7_3066za7, "&notify-interrupt", 17 );
DEFINE_STRING( BGl_string2961z00zz__errorz00, BgL_bgl_string2961za700za7za7_3067za7, "*** INTERRUPT:bigloo:", 21 );
DEFINE_STRING( BGl_string2880z00zz__errorz00, BgL_bgl_string2880za700za7za7_3068za7, "[stdin]", 7 );
DEFINE_STRING( BGl_string2962z00zz__errorz00, BgL_bgl_string2962za700za7za7_3069za7, "arithmetic procedure", 20 );
DEFINE_STRING( BGl_string2881z00zz__errorz00, BgL_bgl_string2881za700za7za7_3070za7, "&warning-notify/location", 24 );
DEFINE_STRING( BGl_string2963z00zz__errorz00, BgL_bgl_string2963za700za7za7_3071za7, "`floating point' exception", 26 );
DEFINE_STRING( BGl_string2964z00zz__errorz00, BgL_bgl_string2964za700za7za7_3072za7, "raised", 6 );
DEFINE_STRING( BGl_string2883z00zz__errorz00, BgL_bgl_string2883za700za7za7_3073za7, "_display-trace-stack", 20 );
DEFINE_STRING( BGl_string2965z00zz__errorz00, BgL_bgl_string2965za700za7za7_3074za7, "bigloo", 6 );
DEFINE_STRING( BGl_string2884z00zz__errorz00, BgL_bgl_string2884za700za7za7_3075za7, "output-port", 11 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2878z00zz__errorz00, BgL_bgl_za762za7c3za704anonymo3076za7, BGl_z62zc3z04anonymousza31895ze3ze5zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2966z00zz__errorz00, BgL_bgl_string2966za700za7za7_3077za7, "`illegal instruction' exception", 31 );
DEFINE_STRING( BGl_string2885z00zz__errorz00, BgL_bgl_string2885za700za7za7_3078za7, "\n*** INTERNAL ERROR: corrupted stack -- ~s\n", 43 );
DEFINE_STRING( BGl_string2967z00zz__errorz00, BgL_bgl_string2967za700za7za7_3079za7, "`bus error' exception", 21 );
DEFINE_STRING( BGl_string2886z00zz__errorz00, BgL_bgl_string2886za700za7za7_3080za7, "! ", 2 );
DEFINE_STRING( BGl_string2968z00zz__errorz00, BgL_bgl_string2968za700za7za7_3081za7, "`segmentation violation' exception", 34 );
DEFINE_STRING( BGl_string2887z00zz__errorz00, BgL_bgl_string2887za700za7za7_3082za7, "    ", 4 );
DEFINE_STRING( BGl_string2969z00zz__errorz00, BgL_bgl_string2969za700za7za7_3083za7, "__error", 7 );
DEFINE_STRING( BGl_string2888z00zz__errorz00, BgL_bgl_string2888za700za7za7_3084za7, "   ", 3 );
DEFINE_STRING( BGl_string2889z00zz__errorz00, BgL_bgl_string2889za700za7za7_3085za7, "  ", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_warningzd2notifyzf2locationzd2envzf2zz__errorz00, BgL_bgl_za762warningza7d2not3086z00, BGl_z62warningzd2notifyzf2locationz42zz__errorz00, 0L, BUNSPEC, 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc2882z00zz__errorz00, BgL_bgl_za762za7c3za704anonymo3087za7, BGl_z62zc3z04anonymousza31928ze3ze5zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2890z00zz__errorz00, BgL_bgl_string2890za700za7za7_3088za7, ". ", 2 );
DEFINE_STRING( BGl_string2892z00zz__errorz00, BgL_bgl_string2892za700za7za7_3089za7, "margin", 6 );
DEFINE_STRING( BGl_string2894z00zz__errorz00, BgL_bgl_string2894za700za7za7_3090za7, "format", 6 );
DEFINE_STRING( BGl_string2895z00zz__errorz00, BgL_bgl_string2895za700za7za7_3091za7, " ", 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_typezd2errorzd2envz00zz__errorz00, BgL_bgl_za762typeza7d2error23092z00, BGl_z62typezd2error2769zb0zz__errorz00, 0L, BUNSPEC, 5 );
DEFINE_STRING( BGl_string2896z00zz__errorz00, BgL_bgl_string2896za700za7za7_3093za7, " (* ", 4 );
DEFINE_STRING( BGl_string2897z00zz__errorz00, BgL_bgl_string2897za700za7za7_3094za7, ")", 1 );
DEFINE_STRING( BGl_string2898z00zz__errorz00, BgL_bgl_string2898za700za7za7_3095za7, ".", 1 );
DEFINE_STRING( BGl_string2899z00zz__errorz00, BgL_bgl_string2899za700za7za7_3096za7, ", ", 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzd2envzd2zz__errorz00, BgL_bgl_za762error2768za762za73097za7, BGl_z62error2768z62zz__errorz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_typeofzd2envzd2zz__errorz00, BgL_bgl_za762typeofza762za7za7__3098z00, BGl_z62typeofz62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_sigsegvzd2errorzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762sigsegvza7d2err3099z00, BGl_z62sigsegvzd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_z62tryzd2envzb0zz__errorz00, BgL_bgl_za762za762tryza700za7za7__3100za7, BGl_z62z62tryz00zz__errorz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_sigillzd2errorzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762sigillza7d2erro3101z00, BGl_z62sigillzd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_withzd2exceptionzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762withza7d2except3102z00, BGl_z62withzd2exceptionzd2handlerz62zz__errorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_exitzd2envzd2zz__errorz00, BgL_bgl_za762exitza762za7za7__er3103z00, va_generic_entry, BGl_z62exitz62zz__errorz00, BUNSPEC, -1 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_sigfpezd2errorzd2handlerzd2envzd2zz__errorz00, BgL_bgl_za762sigfpeza7d2erro3104z00, BGl_z62sigfpezd2errorzd2handlerz62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_dumpzd2tracezd2stackzd2envzd2zz__errorz00, BgL_bgl_za762dumpza7d2traceza73105za7, BGl_z62dumpzd2tracezd2stackz62zz__errorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2tracezd2stackzd2envzd2zz__errorz00, BgL_bgl__displayza7d2trace3106za7, opt_generic_entry, BGl__displayzd2tracezd2stackz00zz__errorz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_warningzd2notifyzd2envz00zz__errorz00, BgL_bgl_za762warningza7d2not3107z00, BGl_z62warningzd2notifyzb0zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_warningzf2loczd2envz20zz__errorz00, BgL_bgl_za762warningza7f2loc3108z00, va_generic_entry, BGl_z62warningzf2locz90zz__errorz00, BUNSPEC, -2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_warningzd2envzd2zz__errorz00, BgL_bgl_za762warning2771za763109z00, va_generic_entry, BGl_z62warning2771z62zz__errorz00, BUNSPEC, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_warningzf2czd2locationzd2envzf2zz__errorz00, BgL_bgl_za762warningza7f2cza7d3110za7, va_generic_entry, BGl_z62warningzf2czd2locationz42zz__errorz00, BUNSPEC, -3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_setzd2errorzd2handlerz12zd2envzc0zz__errorz00, BgL_bgl_za762setza7d2errorza7d3111za7, BGl_z62setzd2errorzd2handlerz12z70zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_displayzd2tracezd2stackzd2sourcezd2envz00zz__errorz00, BgL_bgl_za762displayza7d2tra3112z00, BGl_z62displayzd2tracezd2stackzd2sourcezb0zz__errorz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_czd2debuggingzd2showzd2typezd2envz00zz__errorz00, BgL_bgl_za762cza7d2debugging3113z00, BGl_z62czd2debuggingzd2showzd2typezb0zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stackzd2overflowzd2errorzd2envzd2zz__errorz00, BgL_bgl_za762stackza7d2overf3114z00, BGl_z62stackzd2overflowzd2erro2767z62zz__errorz00, 0L, BUNSPEC, 0 );
DEFINE_STRING( BGl_string2818z00zz__errorz00, BgL_bgl_string2818za700za7za7_3115za7, "BIGLOOSTACKDEPTH", 16 );
DEFINE_STRING( BGl_string2819z00zz__errorz00, BgL_bgl_string2819za700za7za7_3116za7, "/tmp/bigloo/runtime/Llib/error.scm", 34 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_bigloozd2typezd2errorzd2msgzd2envz00zz__errorz00, BgL_bgl_za762biglooza7d2type3117z00, BGl_z62bigloozd2typezd2errorzd2msgzb0zz__errorz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzf2errnozd2envz20zz__errorz00, BgL_bgl_za762errorza7f2errno3118z00, BGl_z62errorzf2errnoz90zz__errorz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string2900z00zz__errorz00, BgL_bgl_string2900za700za7za7_3119za7, "@", 1 );
DEFINE_STRING( BGl_string2901z00zz__errorz00, BgL_bgl_string2901za700za7za7_3120za7, "File ~s, line ~d, character ~d\n", 31 );
DEFINE_STRING( BGl_string2820z00zz__errorz00, BgL_bgl_string2820za700za7za7_3121za7, "&error/errno", 12 );
DEFINE_STRING( BGl_string2902z00zz__errorz00, BgL_bgl_string2902za700za7za7_3122za7, "File ~s, character ~d\n", 22 );
DEFINE_STRING( BGl_string2821z00zz__errorz00, BgL_bgl_string2821za700za7za7_3123za7, "bint", 4 );
DEFINE_STRING( BGl_string2903z00zz__errorz00, BgL_bgl_string2903za700za7za7_3124za7, "&display-trace-stack-source", 27 );
DEFINE_STRING( BGl_string2822z00zz__errorz00, BgL_bgl_string2822za700za7za7_3125za7, "stack overflow", 14 );
DEFINE_STRING( BGl_string2905z00zz__errorz00, BgL_bgl_string2905za700za7za7_3126za7, "done", 4 );
DEFINE_STRING( BGl_string2824z00zz__errorz00, BgL_bgl_string2824za700za7za7_3127za7, "at", 2 );
DEFINE_STRING( BGl_string2906z00zz__errorz00, BgL_bgl_string2906za700za7za7_3128za7, "^", 1 );
DEFINE_STRING( BGl_string2825z00zz__errorz00, BgL_bgl_string2825za700za7za7_3129za7, "with-exception-handler", 22 );
DEFINE_STRING( BGl_string2907z00zz__errorz00, BgL_bgl_string2907za700za7za7_3130za7, "#", 1 );
DEFINE_STRING( BGl_string2826z00zz__errorz00, BgL_bgl_string2826za700za7za7_3131za7, "Incorrect thunk arity", 21 );
DEFINE_STRING( BGl_string2908z00zz__errorz00, BgL_bgl_string2908za700za7za7_3132za7, ", character ", 12 );
DEFINE_STRING( BGl_string2827z00zz__errorz00, BgL_bgl_string2827za700za7za7_3133za7, "Incorrect handler arity", 23 );
DEFINE_STRING( BGl_string2909z00zz__errorz00, BgL_bgl_string2909za700za7za7_3134za7, "\", line ", 8 );
DEFINE_STRING( BGl_string2828z00zz__errorz00, BgL_bgl_string2828za700za7za7_3135za7, "&with-exception-handler", 23 );
DEFINE_STRING( BGl_string2829z00zz__errorz00, BgL_bgl_string2829za700za7za7_3136za7, "procedure", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_findzd2runtimezd2typezd2envzd2zz__errorz00, BgL_bgl_za762findza7d2runtim3137z00, BGl_z62findzd2runtimezd2typez62zz__errorz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string2910z00zz__errorz00, BgL_bgl_string2910za700za7za7_3138za7, "../", 3 );
DEFINE_STRING( BGl_string2911z00zz__errorz00, BgL_bgl_string2911za700za7za7_3139za7, "/", 1 );
DEFINE_STRING( BGl_string2912z00zz__errorz00, BgL_bgl_string2912za700za7za7_3140za7, "/cygdrive/", 10 );
DEFINE_STRING( BGl_string2831z00zz__errorz00, BgL_bgl_string2831za700za7za7_3141za7, "*** INTERNAL ERROR: wrong error handler ", 40 );
DEFINE_STRING( BGl_string2913z00zz__errorz00, BgL_bgl_string2913za700za7za7_3142za7, "real", 4 );
DEFINE_STRING( BGl_string2832z00zz__errorz00, BgL_bgl_string2832za700za7za7_3143za7, ":", 1 );
DEFINE_STRING( BGl_string2914z00zz__errorz00, BgL_bgl_string2914za700za7za7_3144za7, "symbol", 6 );
DEFINE_STRING( BGl_string2833z00zz__errorz00, BgL_bgl_string2833za700za7za7_3145za7, ",", 1 );
DEFINE_STRING( BGl_string2915z00zz__errorz00, BgL_bgl_string2915za700za7za7_3146za7, "keyword", 7 );
DEFINE_STRING( BGl_string2834z00zz__errorz00, BgL_bgl_string2834za700za7za7_3147za7, "Llib/error.scm", 14 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_errorzf2locationzd2envz20zz__errorz00, BgL_bgl_za762errorza7f2locat3148z00, BGl_z62errorzf2locationz90zz__errorz00, 0L, BUNSPEC, 5 );
DEFINE_STRING( BGl_string2916z00zz__errorz00, BgL_bgl_string2916za700za7za7_3149za7, "bchar", 5 );
DEFINE_STRING( BGl_string2835z00zz__errorz00, BgL_bgl_string2835za700za7za7_3150za7, "when raising error: ", 20 );
DEFINE_STRING( BGl_string2917z00zz__errorz00, BgL_bgl_string2917za700za7za7_3151za7, "bbool", 5 );
DEFINE_STRING( BGl_string2836z00zz__errorz00, BgL_bgl_string2836za700za7za7_3152za7, "raise", 5 );
DEFINE_STRING( BGl_string2918z00zz__errorz00, BgL_bgl_string2918za700za7za7_3153za7, "bnil", 4 );
DEFINE_STRING( BGl_string2837z00zz__errorz00, BgL_bgl_string2837za700za7za7_3154za7, "uncaught exception", 18 );
DEFINE_STRING( BGl_string2919z00zz__errorz00, BgL_bgl_string2919za700za7za7_3155za7, "unspecified", 11 );
DEFINE_STRING( BGl_string2838z00zz__errorz00, BgL_bgl_string2838za700za7za7_3156za7, "' must be recompiled (see also -unsafev option).", 48 );
DEFINE_STRING( BGl_string2839z00zz__errorz00, BgL_bgl_string2839za700za7za7_3157za7, "At least `", 10 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_warningzf2locationzd2envz20zz__errorz00, BgL_bgl_za762warningza7f2loc3158z00, va_generic_entry, BGl_z62warningzf2locationz90zz__errorz00, BUNSPEC, -3 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2904z00zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2823z00zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2860z00zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2869z00zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2871z00zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2891z00zz__errorz00) );
ADD_ROOT( (void *)(&BGl_symbol2893z00zz__errorz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long BgL_checksumz00_5368, char * BgL_fromz00_5369)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__errorz00))
{ 
BGl_requirezd2initializa7ationz75zz__errorz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__errorz00(); 
BGl_cnstzd2initzd2zz__errorz00(); 
BGl_importedzd2moduleszd2initz00zz__errorz00(); 
return 
BGl_toplevelzd2initzd2zz__errorz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
BGl_symbol2823z00zz__errorz00 = 
bstring_to_symbol(BGl_string2824z00zz__errorz00); 
BGl_symbol2860z00zz__errorz00 = 
bstring_to_symbol(BGl_string2861z00zz__errorz00); 
BGl_symbol2869z00zz__errorz00 = 
bstring_to_symbol(BGl_string2870z00zz__errorz00); 
BGl_symbol2871z00zz__errorz00 = 
bstring_to_symbol(BGl_string2872z00zz__errorz00); 
BGl_symbol2891z00zz__errorz00 = 
bstring_to_symbol(BGl_string2892z00zz__errorz00); 
BGl_symbol2893z00zz__errorz00 = 
bstring_to_symbol(BGl_string2894z00zz__errorz00); 
return ( 
BGl_symbol2904z00zz__errorz00 = 
bstring_to_symbol(BGl_string2905z00zz__errorz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
BGl_signalz00zz__osz00(SIGFPE, BGl_sigfpezd2errorzd2handlerzd2envzd2zz__errorz00); 
BGl_signalz00zz__osz00(SIGTRAP, BGl_sigfpezd2errorzd2handlerzd2envzd2zz__errorz00); 
BGl_signalz00zz__osz00(SIGILL, BGl_sigillzd2errorzd2handlerzd2envzd2zz__errorz00); 
BGl_signalz00zz__osz00(SIGBUS, BGl_sigbuszd2errorzd2handlerzd2envzd2zz__errorz00); 
return 
BGl_signalz00zz__osz00(SIGSEGV, BGl_sigsegvzd2errorzd2handlerzd2envzd2zz__errorz00);} 

}



/* _get-trace-stack */
obj_t BGl__getzd2tracezd2stackz00zz__errorz00(obj_t BgL_env1210z00_5, obj_t BgL_opt1209z00_4)
{
{ /* Llib/error.scm 308 */
{ /* Llib/error.scm 308 */

switch( 
VECTOR_LENGTH(BgL_opt1209z00_4)) { case 0L : 

{ /* Llib/error.scm 308 */

return 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE);} break;case 1L : 

{ /* Llib/error.scm 308 */
 obj_t BgL_depthz00_1622;
BgL_depthz00_1622 = 
VECTOR_REF(BgL_opt1209z00_4,0L); 
{ /* Llib/error.scm 308 */

return 
BGl_getzd2tracezd2stackz00zz__errorz00(BgL_depthz00_1622);} } break;
default: 
return BUNSPEC;} } } 

}



/* get-trace-stack */
BGL_EXPORTED_DEF obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t BgL_depthz00_3)
{
{ /* Llib/error.scm 308 */
{ /* Llib/error.scm 309 */
 obj_t BgL_dz00_1623;
if(
INTEGERP(BgL_depthz00_3))
{ /* Llib/error.scm 310 */
BgL_dz00_1623 = BgL_depthz00_3; }  else 
{ /* Llib/error.scm 311 */
 obj_t BgL_g1040z00_1625;
BgL_g1040z00_1625 = 
BGl_getenvz00zz__osz00(BGl_string2818z00zz__errorz00); 
if(
CBOOL(BgL_g1040z00_1625))
{ /* Ieee/fixnum.scm 1005 */

BgL_dz00_1623 = 
BINT(
BGl_stringzd2ze3integerz31zz__r4_numbers_6_5_fixnumz00(BgL_g1040z00_1625, 10L, 0L)); }  else 
{ /* Llib/error.scm 311 */
{ /* Llib/error.scm 312 */
 int BgL_arg1340z00_1633;
BgL_arg1340z00_1633 = 
BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(); ((bool_t)1); } 
BgL_dz00_1623 = 
BINT(
BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00()); } } 
{ /* Llib/error.scm 314 */
 int BgL_tmpz00_5405;
BgL_tmpz00_5405 = 
CINT(BgL_dz00_1623); 
return 
bgl_get_trace_stack(BgL_tmpz00_5405);} } } 

}



/* the_failure */
BGL_EXPORTED_DEF obj_t the_failure(obj_t BgL_procz00_6, obj_t BgL_msgz00_7, obj_t BgL_objz00_8)
{
{ /* Llib/error.scm 319 */
{ /* Llib/error.scm 320 */
 bool_t BgL_test3162z00_5408;
{ /* Llib/error.scm 320 */
 obj_t BgL_classz00_3507;
BgL_classz00_3507 = BGl_z62exceptionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_procz00_6))
{ /* Llib/error.scm 320 */
 BgL_objectz00_bglt BgL_arg2721z00_3509;
BgL_arg2721z00_3509 = 
(BgL_objectz00_bglt)(BgL_procz00_6); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 320 */
 long BgL_idxz00_3515;
BgL_idxz00_3515 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_3509); 
BgL_test3162z00_5408 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_3515+2L))==BgL_classz00_3507); }  else 
{ /* Llib/error.scm 320 */
 bool_t BgL_res2747z00_3540;
{ /* Llib/error.scm 320 */
 obj_t BgL_oclassz00_3523;
{ /* Llib/error.scm 320 */
 obj_t BgL_arg2728z00_3531; long BgL_arg2729z00_3532;
BgL_arg2728z00_3531 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 320 */
 long BgL_arg2731z00_3533;
BgL_arg2731z00_3533 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_3509); 
BgL_arg2729z00_3532 = 
(BgL_arg2731z00_3533-OBJECT_TYPE); } 
BgL_oclassz00_3523 = 
VECTOR_REF(BgL_arg2728z00_3531,BgL_arg2729z00_3532); } 
{ /* Llib/error.scm 320 */
 bool_t BgL__ortest_1198z00_3524;
BgL__ortest_1198z00_3524 = 
(BgL_classz00_3507==BgL_oclassz00_3523); 
if(BgL__ortest_1198z00_3524)
{ /* Llib/error.scm 320 */
BgL_res2747z00_3540 = BgL__ortest_1198z00_3524; }  else 
{ /* Llib/error.scm 320 */
 long BgL_odepthz00_3525;
{ /* Llib/error.scm 320 */
 obj_t BgL_arg2717z00_3526;
BgL_arg2717z00_3526 = 
(BgL_oclassz00_3523); 
BgL_odepthz00_3525 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_3526); } 
if(
(2L<BgL_odepthz00_3525))
{ /* Llib/error.scm 320 */
 obj_t BgL_arg2715z00_3528;
{ /* Llib/error.scm 320 */
 obj_t BgL_arg2716z00_3529;
BgL_arg2716z00_3529 = 
(BgL_oclassz00_3523); 
BgL_arg2715z00_3528 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_3529, 2L); } 
BgL_res2747z00_3540 = 
(BgL_arg2715z00_3528==BgL_classz00_3507); }  else 
{ /* Llib/error.scm 320 */
BgL_res2747z00_3540 = ((bool_t)0); } } } } 
BgL_test3162z00_5408 = BgL_res2747z00_3540; } }  else 
{ /* Llib/error.scm 320 */
BgL_test3162z00_5408 = ((bool_t)0)
; } } 
if(BgL_test3162z00_5408)
{ /* Llib/error.scm 320 */
return 
BGl_raisez00zz__errorz00(BgL_procz00_6);}  else 
{ /* Llib/error.scm 320 */
BGL_TAIL return 
BGl_errorz00zz__errorz00(BgL_procz00_6, BgL_msgz00_7, BgL_objz00_8);} } } 

}



/* &the_failure */
obj_t BGl_z62the_failurez62zz__errorz00(obj_t BgL_envz00_5097, obj_t BgL_procz00_5098, obj_t BgL_msgz00_5099, obj_t BgL_objz00_5100)
{
{ /* Llib/error.scm 319 */
return 
the_failure(BgL_procz00_5098, BgL_msgz00_5099, BgL_objz00_5100);} 

}



/* error/errno */
BGL_EXPORTED_DEF obj_t bgl_system_failure(int BgL_sysnoz00_9, obj_t BgL_procz00_10, obj_t BgL_msgz00_11, obj_t BgL_objz00_12)
{
{ /* Llib/error.scm 327 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_ERROR)))
{ /* Llib/error.scm 331 */
 BgL_z62iozd2errorzb0_bglt BgL_arg1343z00_1636;
{ /* Llib/error.scm 331 */
 BgL_z62iozd2errorzb0_bglt BgL_new1043z00_1637;
{ /* Llib/error.scm 331 */
 BgL_z62iozd2errorzb0_bglt BgL_new1042z00_1640;
BgL_new1042z00_1640 = 
((BgL_z62iozd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2errorzb0_bgl) ))); 
{ /* Llib/error.scm 331 */
 long BgL_arg1347z00_1641;
BgL_arg1347z00_1641 = 
BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1042z00_1640), BgL_arg1347z00_1641); } 
BgL_new1043z00_1637 = BgL_new1042z00_1640; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1043z00_1637)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1043z00_1637)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5446;
{ /* Llib/error.scm 331 */
 obj_t BgL_arg1344z00_1638;
{ /* Llib/error.scm 331 */
 obj_t BgL_arg1346z00_1639;
{ /* Llib/error.scm 331 */
 obj_t BgL_classz00_3546;
BgL_classz00_3546 = BGl_z62iozd2errorzb0zz__objectz00; 
BgL_arg1346z00_1639 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3546); } 
BgL_arg1344z00_1638 = 
VECTOR_REF(BgL_arg1346z00_1639,2L); } 
BgL_auxz00_5446 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1344z00_1638); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1043z00_1637)))->BgL_stackz00)=((obj_t)BgL_auxz00_5446),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1043z00_1637)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1043z00_1637)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1043z00_1637)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1343z00_1636 = BgL_new1043z00_1637; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1343z00_1636));}  else 
{ /* Llib/error.scm 329 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_PORT_ERROR)))
{ /* Llib/error.scm 334 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_arg1349z00_1643;
{ /* Llib/error.scm 334 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_new1045z00_1644;
{ /* Llib/error.scm 334 */
 BgL_z62iozd2portzd2errorz62_bglt BgL_new1044z00_1647;
BgL_new1044z00_1647 = 
((BgL_z62iozd2portzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2portzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 334 */
 long BgL_arg1352z00_1648;
BgL_arg1352z00_1648 = 
BGL_CLASS_NUM(BGl_z62iozd2portzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1044z00_1647), BgL_arg1352z00_1648); } 
BgL_new1045z00_1644 = BgL_new1044z00_1647; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1045z00_1644)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1045z00_1644)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5472;
{ /* Llib/error.scm 334 */
 obj_t BgL_arg1350z00_1645;
{ /* Llib/error.scm 334 */
 obj_t BgL_arg1351z00_1646;
{ /* Llib/error.scm 334 */
 obj_t BgL_classz00_3553;
BgL_classz00_3553 = BGl_z62iozd2portzd2errorz62zz__objectz00; 
BgL_arg1351z00_1646 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3553); } 
BgL_arg1350z00_1645 = 
VECTOR_REF(BgL_arg1351z00_1646,2L); } 
BgL_auxz00_5472 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1350z00_1645); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1045z00_1644)))->BgL_stackz00)=((obj_t)BgL_auxz00_5472),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1045z00_1644)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1045z00_1644)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1045z00_1644)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1349z00_1643 = BgL_new1045z00_1644; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1349z00_1643));}  else 
{ /* Llib/error.scm 332 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_READ_ERROR)))
{ /* Llib/error.scm 337 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_arg1354z00_1650;
{ /* Llib/error.scm 337 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_new1047z00_1651;
{ /* Llib/error.scm 337 */
 BgL_z62iozd2readzd2errorz62_bglt BgL_new1046z00_1654;
BgL_new1046z00_1654 = 
((BgL_z62iozd2readzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2readzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 337 */
 long BgL_arg1358z00_1655;
BgL_arg1358z00_1655 = 
BGL_CLASS_NUM(BGl_z62iozd2readzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1046z00_1654), BgL_arg1358z00_1655); } 
BgL_new1047z00_1651 = BgL_new1046z00_1654; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1047z00_1651)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1047z00_1651)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5498;
{ /* Llib/error.scm 337 */
 obj_t BgL_arg1356z00_1652;
{ /* Llib/error.scm 337 */
 obj_t BgL_arg1357z00_1653;
{ /* Llib/error.scm 337 */
 obj_t BgL_classz00_3560;
BgL_classz00_3560 = BGl_z62iozd2readzd2errorz62zz__objectz00; 
BgL_arg1357z00_1653 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3560); } 
BgL_arg1356z00_1652 = 
VECTOR_REF(BgL_arg1357z00_1653,2L); } 
BgL_auxz00_5498 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1356z00_1652); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1047z00_1651)))->BgL_stackz00)=((obj_t)BgL_auxz00_5498),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1047z00_1651)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1047z00_1651)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1047z00_1651)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1354z00_1650 = BgL_new1047z00_1651; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1354z00_1650));}  else 
{ /* Llib/error.scm 335 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_WRITE_ERROR)))
{ /* Llib/error.scm 340 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_arg1360z00_1657;
{ /* Llib/error.scm 340 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_new1049z00_1658;
{ /* Llib/error.scm 340 */
 BgL_z62iozd2writezd2errorz62_bglt BgL_new1048z00_1661;
BgL_new1048z00_1661 = 
((BgL_z62iozd2writezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2writezd2errorz62_bgl) ))); 
{ /* Llib/error.scm 340 */
 long BgL_arg1363z00_1662;
BgL_arg1363z00_1662 = 
BGL_CLASS_NUM(BGl_z62iozd2writezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1048z00_1661), BgL_arg1363z00_1662); } 
BgL_new1049z00_1658 = BgL_new1048z00_1661; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1049z00_1658)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1049z00_1658)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5524;
{ /* Llib/error.scm 340 */
 obj_t BgL_arg1361z00_1659;
{ /* Llib/error.scm 340 */
 obj_t BgL_arg1362z00_1660;
{ /* Llib/error.scm 340 */
 obj_t BgL_classz00_3567;
BgL_classz00_3567 = BGl_z62iozd2writezd2errorz62zz__objectz00; 
BgL_arg1362z00_1660 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3567); } 
BgL_arg1361z00_1659 = 
VECTOR_REF(BgL_arg1362z00_1660,2L); } 
BgL_auxz00_5524 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1361z00_1659); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1049z00_1658)))->BgL_stackz00)=((obj_t)BgL_auxz00_5524),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1049z00_1658)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1049z00_1658)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1049z00_1658)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1360z00_1657 = BgL_new1049z00_1658; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1360z00_1657));}  else 
{ /* Llib/error.scm 338 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_UNKNOWN_HOST_ERROR)))
{ /* Llib/error.scm 343 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_arg1365z00_1664;
{ /* Llib/error.scm 343 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1051z00_1665;
{ /* Llib/error.scm 343 */
 BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt BgL_new1050z00_1668;
BgL_new1050z00_1668 = 
((BgL_z62iozd2unknownzd2hostzd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2unknownzd2hostzd2errorzb0_bgl) ))); 
{ /* Llib/error.scm 343 */
 long BgL_arg1368z00_1669;
BgL_arg1368z00_1669 = 
BGL_CLASS_NUM(BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1050z00_1668), BgL_arg1368z00_1669); } 
BgL_new1051z00_1665 = BgL_new1050z00_1668; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1051z00_1665)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1051z00_1665)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5550;
{ /* Llib/error.scm 343 */
 obj_t BgL_arg1366z00_1666;
{ /* Llib/error.scm 343 */
 obj_t BgL_arg1367z00_1667;
{ /* Llib/error.scm 343 */
 obj_t BgL_classz00_3574;
BgL_classz00_3574 = BGl_z62iozd2unknownzd2hostzd2errorzb0zz__objectz00; 
BgL_arg1367z00_1667 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3574); } 
BgL_arg1366z00_1666 = 
VECTOR_REF(BgL_arg1367z00_1667,2L); } 
BgL_auxz00_5550 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1366z00_1666); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1051z00_1665)))->BgL_stackz00)=((obj_t)BgL_auxz00_5550),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1051z00_1665)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1051z00_1665)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1051z00_1665)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1365z00_1664 = BgL_new1051z00_1665; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1365z00_1664));}  else 
{ /* Llib/error.scm 341 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_FILE_NOT_FOUND_ERROR)))
{ /* Llib/error.scm 346 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_arg1370z00_1671;
{ /* Llib/error.scm 346 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1053z00_1672;
{ /* Llib/error.scm 346 */
 BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt BgL_new1052z00_1675;
BgL_new1052z00_1675 = 
((BgL_z62iozd2filezd2notzd2foundzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2filezd2notzd2foundzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 346 */
 long BgL_arg1373z00_1676;
BgL_arg1373z00_1676 = 
BGL_CLASS_NUM(BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1052z00_1675), BgL_arg1373z00_1676); } 
BgL_new1053z00_1672 = BgL_new1052z00_1675; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1053z00_1672)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1053z00_1672)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5576;
{ /* Llib/error.scm 346 */
 obj_t BgL_arg1371z00_1673;
{ /* Llib/error.scm 346 */
 obj_t BgL_arg1372z00_1674;
{ /* Llib/error.scm 346 */
 obj_t BgL_classz00_3581;
BgL_classz00_3581 = BGl_z62iozd2filezd2notzd2foundzd2errorz62zz__objectz00; 
BgL_arg1372z00_1674 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3581); } 
BgL_arg1371z00_1673 = 
VECTOR_REF(BgL_arg1372z00_1674,2L); } 
BgL_auxz00_5576 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1371z00_1673); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1053z00_1672)))->BgL_stackz00)=((obj_t)BgL_auxz00_5576),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1053z00_1672)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1053z00_1672)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1053z00_1672)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1370z00_1671 = BgL_new1053z00_1672; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1370z00_1671));}  else 
{ /* Llib/error.scm 344 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_PARSE_ERROR)))
{ /* Llib/error.scm 349 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1375z00_1678;
{ /* Llib/error.scm 349 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1055z00_1679;
{ /* Llib/error.scm 349 */
 BgL_z62iozd2parsezd2errorz62_bglt BgL_new1054z00_1682;
BgL_new1054z00_1682 = 
((BgL_z62iozd2parsezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2parsezd2errorz62_bgl) ))); 
{ /* Llib/error.scm 349 */
 long BgL_arg1378z00_1683;
BgL_arg1378z00_1683 = 
BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1054z00_1682), BgL_arg1378z00_1683); } 
BgL_new1055z00_1679 = BgL_new1054z00_1682; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1055z00_1679)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1055z00_1679)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5602;
{ /* Llib/error.scm 349 */
 obj_t BgL_arg1376z00_1680;
{ /* Llib/error.scm 349 */
 obj_t BgL_arg1377z00_1681;
{ /* Llib/error.scm 349 */
 obj_t BgL_classz00_3588;
BgL_classz00_3588 = BGl_z62iozd2parsezd2errorz62zz__objectz00; 
BgL_arg1377z00_1681 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3588); } 
BgL_arg1376z00_1680 = 
VECTOR_REF(BgL_arg1377z00_1681,2L); } 
BgL_auxz00_5602 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1376z00_1680); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1055z00_1679)))->BgL_stackz00)=((obj_t)BgL_auxz00_5602),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1055z00_1679)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1055z00_1679)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1055z00_1679)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1375z00_1678 = BgL_new1055z00_1679; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1375z00_1678));}  else 
{ /* Llib/error.scm 347 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_MALFORMED_URL_ERROR)))
{ /* Llib/error.scm 352 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_arg1380z00_1685;
{ /* Llib/error.scm 352 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1057z00_1686;
{ /* Llib/error.scm 352 */
 BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt BgL_new1056z00_1689;
BgL_new1056z00_1689 = 
((BgL_z62iozd2malformedzd2urlzd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2malformedzd2urlzd2errorzb0_bgl) ))); 
{ /* Llib/error.scm 352 */
 long BgL_arg1384z00_1690;
BgL_arg1384z00_1690 = 
BGL_CLASS_NUM(BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1056z00_1689), BgL_arg1384z00_1690); } 
BgL_new1057z00_1686 = BgL_new1056z00_1689; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1057z00_1686)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1057z00_1686)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5628;
{ /* Llib/error.scm 352 */
 obj_t BgL_arg1382z00_1687;
{ /* Llib/error.scm 352 */
 obj_t BgL_arg1383z00_1688;
{ /* Llib/error.scm 352 */
 obj_t BgL_classz00_3595;
BgL_classz00_3595 = BGl_z62iozd2malformedzd2urlzd2errorzb0zz__objectz00; 
BgL_arg1383z00_1688 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3595); } 
BgL_arg1382z00_1687 = 
VECTOR_REF(BgL_arg1383z00_1688,2L); } 
BgL_auxz00_5628 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1382z00_1687); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1057z00_1686)))->BgL_stackz00)=((obj_t)BgL_auxz00_5628),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1057z00_1686)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1057z00_1686)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1057z00_1686)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1380z00_1685 = BgL_new1057z00_1686; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1380z00_1685));}  else 
{ /* Llib/error.scm 350 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_SIGPIPE_ERROR)))
{ /* Llib/error.scm 355 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_arg1387z00_1692;
{ /* Llib/error.scm 355 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1059z00_1693;
{ /* Llib/error.scm 355 */
 BgL_z62iozd2sigpipezd2errorz62_bglt BgL_new1058z00_1696;
BgL_new1058z00_1696 = 
((BgL_z62iozd2sigpipezd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2sigpipezd2errorz62_bgl) ))); 
{ /* Llib/error.scm 355 */
 long BgL_arg1390z00_1697;
BgL_arg1390z00_1697 = 
BGL_CLASS_NUM(BGl_z62iozd2sigpipezd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1058z00_1696), BgL_arg1390z00_1697); } 
BgL_new1059z00_1693 = BgL_new1058z00_1696; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1059z00_1693)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1059z00_1693)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5654;
{ /* Llib/error.scm 355 */
 obj_t BgL_arg1388z00_1694;
{ /* Llib/error.scm 355 */
 obj_t BgL_arg1389z00_1695;
{ /* Llib/error.scm 355 */
 obj_t BgL_classz00_3602;
BgL_classz00_3602 = BGl_z62iozd2sigpipezd2errorz62zz__objectz00; 
BgL_arg1389z00_1695 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3602); } 
BgL_arg1388z00_1694 = 
VECTOR_REF(BgL_arg1389z00_1695,2L); } 
BgL_auxz00_5654 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1388z00_1694); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1059z00_1693)))->BgL_stackz00)=((obj_t)BgL_auxz00_5654),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1059z00_1693)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1059z00_1693)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1059z00_1693)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1387z00_1692 = BgL_new1059z00_1693; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1387z00_1692));}  else 
{ /* Llib/error.scm 353 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_TIMEOUT_ERROR)))
{ /* Llib/error.scm 358 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_arg1392z00_1699;
{ /* Llib/error.scm 358 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1062z00_1700;
{ /* Llib/error.scm 358 */
 BgL_z62iozd2timeoutzd2errorz62_bglt BgL_new1061z00_1703;
BgL_new1061z00_1703 = 
((BgL_z62iozd2timeoutzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2timeoutzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 358 */
 long BgL_arg1395z00_1704;
BgL_arg1395z00_1704 = 
BGL_CLASS_NUM(BGl_z62iozd2timeoutzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1061z00_1703), BgL_arg1395z00_1704); } 
BgL_new1062z00_1700 = BgL_new1061z00_1703; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1062z00_1700)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1062z00_1700)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5680;
{ /* Llib/error.scm 358 */
 obj_t BgL_arg1393z00_1701;
{ /* Llib/error.scm 358 */
 obj_t BgL_arg1394z00_1702;
{ /* Llib/error.scm 358 */
 obj_t BgL_classz00_3609;
BgL_classz00_3609 = BGl_z62iozd2timeoutzd2errorz62zz__objectz00; 
BgL_arg1394z00_1702 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3609); } 
BgL_arg1393z00_1701 = 
VECTOR_REF(BgL_arg1394z00_1702,2L); } 
BgL_auxz00_5680 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1393z00_1701); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1062z00_1700)))->BgL_stackz00)=((obj_t)BgL_auxz00_5680),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1062z00_1700)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1062z00_1700)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1062z00_1700)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1392z00_1699 = BgL_new1062z00_1700; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1392z00_1699));}  else 
{ /* Llib/error.scm 356 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_IO_CONNECTION_ERROR)))
{ /* Llib/error.scm 361 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_arg1397z00_1706;
{ /* Llib/error.scm 361 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1064z00_1707;
{ /* Llib/error.scm 361 */
 BgL_z62iozd2connectionzd2errorz62_bglt BgL_new1063z00_1710;
BgL_new1063z00_1710 = 
((BgL_z62iozd2connectionzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62iozd2connectionzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 361 */
 long BgL_arg1401z00_1711;
BgL_arg1401z00_1711 = 
BGL_CLASS_NUM(BGl_z62iozd2connectionzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1063z00_1710), BgL_arg1401z00_1711); } 
BgL_new1064z00_1707 = BgL_new1063z00_1710; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1064z00_1707)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1064z00_1707)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5706;
{ /* Llib/error.scm 361 */
 obj_t BgL_arg1399z00_1708;
{ /* Llib/error.scm 361 */
 obj_t BgL_arg1400z00_1709;
{ /* Llib/error.scm 361 */
 obj_t BgL_classz00_3616;
BgL_classz00_3616 = BGl_z62iozd2connectionzd2errorz62zz__objectz00; 
BgL_arg1400z00_1709 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3616); } 
BgL_arg1399z00_1708 = 
VECTOR_REF(BgL_arg1400z00_1709,2L); } 
BgL_auxz00_5706 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1399z00_1708); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1064z00_1707)))->BgL_stackz00)=((obj_t)BgL_auxz00_5706),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1064z00_1707)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1064z00_1707)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1064z00_1707)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1397z00_1706 = BgL_new1064z00_1707; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1397z00_1706));}  else 
{ /* Llib/error.scm 359 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_PROCESS_EXCEPTION)))
{ /* Llib/error.scm 364 */
 BgL_z62processzd2exceptionzb0_bglt BgL_arg1403z00_1713;
{ /* Llib/error.scm 364 */
 BgL_z62processzd2exceptionzb0_bglt BgL_new1066z00_1714;
{ /* Llib/error.scm 364 */
 BgL_z62processzd2exceptionzb0_bglt BgL_new1065z00_1717;
BgL_new1065z00_1717 = 
((BgL_z62processzd2exceptionzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62processzd2exceptionzb0_bgl) ))); 
{ /* Llib/error.scm 364 */
 long BgL_arg1406z00_1718;
BgL_arg1406z00_1718 = 
BGL_CLASS_NUM(BGl_z62processzd2exceptionzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1065z00_1717), BgL_arg1406z00_1718); } 
BgL_new1066z00_1714 = BgL_new1065z00_1717; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1066z00_1714)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1066z00_1714)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5732;
{ /* Llib/error.scm 364 */
 obj_t BgL_arg1404z00_1715;
{ /* Llib/error.scm 364 */
 obj_t BgL_arg1405z00_1716;
{ /* Llib/error.scm 364 */
 obj_t BgL_classz00_3623;
BgL_classz00_3623 = BGl_z62processzd2exceptionzb0zz__objectz00; 
BgL_arg1405z00_1716 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3623); } 
BgL_arg1404z00_1715 = 
VECTOR_REF(BgL_arg1405z00_1716,2L); } 
BgL_auxz00_5732 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1404z00_1715); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1066z00_1714)))->BgL_stackz00)=((obj_t)BgL_auxz00_5732),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1066z00_1714)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1066z00_1714)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1066z00_1714)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
BgL_arg1403z00_1713 = BgL_new1066z00_1714; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1403z00_1713));}  else 
{ /* Llib/error.scm 362 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_TYPE_ERROR)))
{ /* Llib/error.scm 365 */
return 
BGl_raisez00zz__errorz00(
BGl_typezd2errorzd2zz__errorz00(BFALSE, BFALSE, BgL_procz00_10, BgL_msgz00_11, BgL_objz00_12));}  else 
{ /* Llib/error.scm 365 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_TYPENAME_ERROR)))
{ /* Llib/error.scm 368 */
 BgL_z62typezd2errorzb0_bglt BgL_arg1410z00_1722;
BgL_arg1410z00_1722 = 
BGl_typenamezd2errorzd2zz__errorz00(((bool_t)0), ((bool_t)0), BgL_procz00_10, BgL_msgz00_11, BgL_objz00_12); 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1410z00_1722));}  else 
{ /* Llib/error.scm 367 */
if(
(
(long)(BgL_sysnoz00_9)==
(long)(BGL_INDEX_OUT_OF_BOUND_ERROR)))
{ /* Llib/error.scm 370 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_arg1412z00_1724;
{ /* Llib/error.scm 370 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1068z00_1725;
{ /* Llib/error.scm 370 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1067z00_1728;
BgL_new1067z00_1728 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl) ))); 
{ /* Llib/error.scm 370 */
 long BgL_arg1415z00_1729;
BgL_arg1415z00_1729 = 
BGL_CLASS_NUM(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1067z00_1728), BgL_arg1415z00_1729); } 
BgL_new1068z00_1725 = BgL_new1067z00_1728; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1068z00_1725)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1068z00_1725)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5771;
{ /* Llib/error.scm 370 */
 obj_t BgL_arg1413z00_1726;
{ /* Llib/error.scm 370 */
 obj_t BgL_arg1414z00_1727;
{ /* Llib/error.scm 370 */
 obj_t BgL_classz00_3634;
BgL_classz00_3634 = BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00; 
BgL_arg1414z00_1727 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3634); } 
BgL_arg1413z00_1726 = 
VECTOR_REF(BgL_arg1414z00_1727,2L); } 
BgL_auxz00_5771 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1413z00_1726); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1068z00_1725)))->BgL_stackz00)=((obj_t)BgL_auxz00_5771),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1068z00_1725)))->BgL_procz00)=((obj_t)BgL_procz00_10),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1068z00_1725)))->BgL_msgz00)=((obj_t)BgL_msgz00_11),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1068z00_1725)))->BgL_objz00)=((obj_t)BgL_objz00_12),BUNSPEC); 
((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)COBJECT(BgL_new1068z00_1725))->BgL_indexz00)=((obj_t)
BINT(-1L)),BUNSPEC); 
BgL_arg1412z00_1724 = BgL_new1068z00_1725; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1412z00_1724));}  else 
{ /* Llib/error.scm 369 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_10, BgL_msgz00_11, BgL_objz00_12);} } } } } } } } } } } } } } } } 

}



/* &error/errno */
obj_t BGl_z62errorzf2errnoz90zz__errorz00(obj_t BgL_envz00_5101, obj_t BgL_sysnoz00_5102, obj_t BgL_procz00_5103, obj_t BgL_msgz00_5104, obj_t BgL_objz00_5105)
{
{ /* Llib/error.scm 327 */
{ /* Llib/error.scm 329 */
 int BgL_auxz00_5788;
{ /* Llib/error.scm 329 */
 obj_t BgL_tmpz00_5789;
if(
INTEGERP(BgL_sysnoz00_5102))
{ /* Llib/error.scm 329 */
BgL_tmpz00_5789 = BgL_sysnoz00_5102
; }  else 
{ 
 obj_t BgL_auxz00_5792;
BgL_auxz00_5792 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(13566L), BGl_string2820z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_sysnoz00_5102); 
FAILURE(BgL_auxz00_5792,BFALSE,BFALSE);} 
BgL_auxz00_5788 = 
CINT(BgL_tmpz00_5789); } 
return 
bgl_system_failure(BgL_auxz00_5788, BgL_procz00_5103, BgL_msgz00_5104, BgL_objz00_5105);} } 

}



/* stack-overflow-error */
BGL_EXPORTED_DEF obj_t bgl_stack_overflow_error(void)
{
{ /* Llib/error.scm 378 */
{ /* Llib/error.scm 379 */
 obj_t BgL_stkz00_1730;
{ /* Llib/error.scm 379 */

{ /* Llib/error.scm 379 */

BgL_stkz00_1730 = 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE); } } 
{ 
 obj_t BgL_procz00_1735; obj_t BgL_procz00_1731; obj_t BgL_fnamez00_1732; obj_t BgL_locz00_1733;
if(
PAIRP(BgL_stkz00_1730))
{ /* Llib/error.scm 380 */
 obj_t BgL_carzd2111zd2_1740;
BgL_carzd2111zd2_1740 = 
CAR(
((obj_t)BgL_stkz00_1730)); 
if(
PAIRP(BgL_carzd2111zd2_1740))
{ /* Llib/error.scm 380 */
 obj_t BgL_cdrzd2117zd2_1742;
BgL_cdrzd2117zd2_1742 = 
CDR(BgL_carzd2111zd2_1740); 
if(
PAIRP(BgL_cdrzd2117zd2_1742))
{ /* Llib/error.scm 380 */
 obj_t BgL_carzd2121zd2_1744;
BgL_carzd2121zd2_1744 = 
CAR(BgL_cdrzd2117zd2_1742); 
if(
PAIRP(BgL_carzd2121zd2_1744))
{ /* Llib/error.scm 380 */
 obj_t BgL_cdrzd2126zd2_1746;
BgL_cdrzd2126zd2_1746 = 
CDR(BgL_carzd2121zd2_1744); 
if(
(
CAR(BgL_carzd2121zd2_1744)==BGl_symbol2823z00zz__errorz00))
{ /* Llib/error.scm 380 */
if(
PAIRP(BgL_cdrzd2126zd2_1746))
{ /* Llib/error.scm 380 */
 obj_t BgL_cdrzd2130zd2_1750;
BgL_cdrzd2130zd2_1750 = 
CDR(BgL_cdrzd2126zd2_1746); 
if(
PAIRP(BgL_cdrzd2130zd2_1750))
{ /* Llib/error.scm 380 */
if(
NULLP(
CDR(BgL_cdrzd2130zd2_1750)))
{ /* Llib/error.scm 380 */
if(
NULLP(
CDR(BgL_cdrzd2117zd2_1742)))
{ /* Llib/error.scm 380 */
BgL_procz00_1731 = 
CAR(BgL_carzd2111zd2_1740); 
BgL_fnamez00_1732 = 
CAR(BgL_cdrzd2126zd2_1746); 
BgL_locz00_1733 = 
CAR(BgL_cdrzd2130zd2_1750); 
{ /* Llib/error.scm 383 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_arg1444z00_1770;
{ /* Llib/error.scm 383 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1070z00_1771;
{ /* Llib/error.scm 384 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1069z00_1772;
BgL_new1069z00_1772 = 
((BgL_z62stackzd2overflowzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62stackzd2overflowzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 384 */
 long BgL_arg1445z00_1773;
BgL_arg1445z00_1773 = 
BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1069z00_1772), BgL_arg1445z00_1773); } 
BgL_new1070z00_1771 = BgL_new1069z00_1772; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1070z00_1771)))->BgL_fnamez00)=((obj_t)BgL_fnamez00_1732),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1070z00_1771)))->BgL_locationz00)=((obj_t)BgL_locz00_1733),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1070z00_1771)))->BgL_stackz00)=((obj_t)BgL_stkz00_1730),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1070z00_1771)))->BgL_procz00)=((obj_t)BgL_procz00_1731),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1070z00_1771)))->BgL_msgz00)=((obj_t)BGl_string2822z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1070z00_1771)))->BgL_objz00)=((obj_t)
BGL_CURRENT_DYNAMIC_ENV()),BUNSPEC); 
BgL_arg1444z00_1770 = BgL_new1070z00_1771; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1444z00_1770));} }  else 
{ /* Llib/error.scm 380 */
BgL_procz00_1735 = BgL_carzd2111zd2_1740; 
BgL_tagzd2102zd2_1736:
{ /* Llib/error.scm 392 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_arg1446z00_1774;
{ /* Llib/error.scm 392 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1072z00_1775;
{ /* Llib/error.scm 392 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1071z00_1776;
BgL_new1071z00_1776 = 
((BgL_z62stackzd2overflowzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62stackzd2overflowzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 392 */
 long BgL_arg1447z00_1777;
BgL_arg1447z00_1777 = 
BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1071z00_1776), BgL_arg1447z00_1777); } 
BgL_new1072z00_1775 = BgL_new1071z00_1776; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1072z00_1775)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1072z00_1775)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1072z00_1775)))->BgL_stackz00)=((obj_t)BgL_stkz00_1730),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1072z00_1775)))->BgL_procz00)=((obj_t)BgL_procz00_1735),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1072z00_1775)))->BgL_msgz00)=((obj_t)BGl_string2822z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1072z00_1775)))->BgL_objz00)=((obj_t)
BGL_CURRENT_DYNAMIC_ENV()),BUNSPEC); 
BgL_arg1446z00_1774 = BgL_new1072z00_1775; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1446z00_1774));} } }  else 
{ 
 obj_t BgL_procz00_5867;
BgL_procz00_5867 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5867; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ 
 obj_t BgL_procz00_5868;
BgL_procz00_5868 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5868; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ 
 obj_t BgL_procz00_5869;
BgL_procz00_5869 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5869; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ 
 obj_t BgL_procz00_5870;
BgL_procz00_5870 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5870; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ 
 obj_t BgL_procz00_5871;
BgL_procz00_5871 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5871; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ 
 obj_t BgL_procz00_5872;
BgL_procz00_5872 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5872; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ 
 obj_t BgL_procz00_5873;
BgL_procz00_5873 = BgL_carzd2111zd2_1740; 
BgL_procz00_1735 = BgL_procz00_5873; 
goto BgL_tagzd2102zd2_1736;} }  else 
{ /* Llib/error.scm 380 */
{ /* Llib/error.scm 399 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_arg1448z00_1778;
{ /* Llib/error.scm 399 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1074z00_1779;
{ /* Llib/error.scm 399 */
 BgL_z62stackzd2overflowzd2errorz62_bglt BgL_new1073z00_1780;
BgL_new1073z00_1780 = 
((BgL_z62stackzd2overflowzd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62stackzd2overflowzd2errorz62_bgl) ))); 
{ /* Llib/error.scm 399 */
 long BgL_arg1449z00_1781;
BgL_arg1449z00_1781 = 
BGL_CLASS_NUM(BGl_z62stackzd2overflowzd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1073z00_1780), BgL_arg1449z00_1781); } 
BgL_new1074z00_1779 = BgL_new1073z00_1780; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1074z00_1779)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1074z00_1779)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1074z00_1779)))->BgL_stackz00)=((obj_t)BgL_stkz00_1730),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1074z00_1779)))->BgL_procz00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1074z00_1779)))->BgL_msgz00)=((obj_t)BGl_string2822z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1074z00_1779)))->BgL_objz00)=((obj_t)
BGL_CURRENT_DYNAMIC_ENV()),BUNSPEC); 
BgL_arg1448z00_1778 = BgL_new1074z00_1779; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1448z00_1778));} } } } } 

}



/* &stack-overflow-erro2767 */
obj_t BGl_z62stackzd2overflowzd2erro2767z62zz__errorz00(obj_t BgL_envz00_5106)
{
{ /* Llib/error.scm 378 */
return 
bgl_stack_overflow_error();} 

}



/* exit */
BGL_EXPORTED_DEF obj_t BGl_exitz00zz__errorz00(obj_t BgL_nz00_13)
{
{ /* Llib/error.scm 408 */
{ /* Llib/error.scm 409 */
 obj_t BgL_valz00_1783;
if(
NULLP(BgL_nz00_13))
{ /* Llib/error.scm 410 */
BgL_valz00_1783 = 
BINT(0L); }  else 
{ /* Llib/error.scm 410 */
if(
INTEGERP(
CAR(
((obj_t)BgL_nz00_13))))
{ /* Llib/error.scm 411 */
BgL_valz00_1783 = 
CAR(
((obj_t)BgL_nz00_13)); }  else 
{ /* Llib/error.scm 411 */
BgL_valz00_1783 = 
BINT(0L); } } 
BIGLOO_EXIT(BgL_valz00_1783); 
return BgL_valz00_1783;} } 

}



/* &exit */
obj_t BGl_z62exitz62zz__errorz00(obj_t BgL_envz00_5107, obj_t BgL_nz00_5108)
{
{ /* Llib/error.scm 408 */
return 
BGl_exitz00zz__errorz00(BgL_nz00_5108);} 

}



/* with-exception-handler */
BGL_EXPORTED_DEF obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t BgL_handlerz00_14, obj_t BgL_thunkz00_15)
{
{ /* Llib/error.scm 419 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_handlerz00_14, 
(int)(1L)))
{ /* Llib/error.scm 421 */
 obj_t BgL_oldzd2handlerszd2_1788;
BgL_oldzd2handlerszd2_1788 = 
BGL_ERROR_HANDLER_GET(); 
{ /* Llib/error.scm 424 */
 obj_t BgL_arg1454z00_1789;
{ /* Llib/error.scm 424 */
 obj_t BgL_zc3z04anonymousza31456ze3z87_5109;
BgL_zc3z04anonymousza31456ze3z87_5109 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31456ze3ze5zz__errorz00, 
(int)(1L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_5109, 
(int)(0L), BgL_oldzd2handlerszd2_1788); 
PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_5109, 
(int)(1L), 
((obj_t)BgL_handlerz00_14)); 
BgL_arg1454z00_1789 = 
MAKE_STACK_PAIR(BgL_zc3z04anonymousza31456ze3z87_5109, BUNSPEC); } 
BGL_ERROR_HANDLER_SET(BgL_arg1454z00_1789); BUNSPEC; } 
{ /* Llib/error.scm 427 */
 obj_t BgL_exitd1075z00_1794;
BgL_exitd1075z00_1794 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/error.scm 427 */
 obj_t BgL_tmp1077z00_1795;
{ /* Llib/error.scm 427 */
 obj_t BgL_arg1457z00_1797;
BgL_arg1457z00_1797 = 
BGL_EXITD_PROTECT(BgL_exitd1075z00_1794); 
BgL_tmp1077z00_1795 = 
MAKE_YOUNG_PAIR(BgL_oldzd2handlerszd2_1788, BgL_arg1457z00_1797); } 
{ /* Llib/error.scm 427 */

BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1794, BgL_tmp1077z00_1795); BUNSPEC; 
{ /* Llib/error.scm 428 */
 obj_t BgL_tmp1076z00_1796;
if(
PROCEDURE_CORRECT_ARITYP(BgL_thunkz00_15, 
(int)(0L)))
{ /* Llib/error.scm 428 */
BgL_tmp1076z00_1796 = 
BGL_PROCEDURE_CALL0(BgL_thunkz00_15); }  else 
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_3667;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_3668;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_3669;
BgL_new1078z00_3669 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_3670;
BgL_arg1513z00_3670 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_3669), BgL_arg1513z00_3670); } 
BgL_new1079z00_3668 = BgL_new1078z00_3669; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_3668)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_3668)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5938;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_3671;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_3672;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_3676;
BgL_classz00_3676 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_3672 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3676); } 
BgL_arg1510z00_3671 = 
VECTOR_REF(BgL_arg1511z00_3672,2L); } 
BgL_auxz00_5938 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_3671); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_3668)))->BgL_stackz00)=((obj_t)BgL_auxz00_5938),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_3668))->BgL_procz00)=((obj_t)BGl_string2825z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_3668))->BgL_msgz00)=((obj_t)BGl_string2826z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_3668))->BgL_objz00)=((obj_t)BgL_thunkz00_15),BUNSPEC); 
BgL_arg1509z00_3667 = BgL_new1079z00_3668; } 
BgL_tmp1076z00_1796 = 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_3667)); } 
{ /* Llib/error.scm 427 */
 bool_t BgL_test3196z00_5949;
{ /* Llib/error.scm 427 */
 obj_t BgL_arg2609z00_3679;
BgL_arg2609z00_3679 = 
BGL_EXITD_PROTECT(BgL_exitd1075z00_1794); 
BgL_test3196z00_5949 = 
PAIRP(BgL_arg2609z00_3679); } 
if(BgL_test3196z00_5949)
{ /* Llib/error.scm 427 */
 obj_t BgL_arg2607z00_3680;
{ /* Llib/error.scm 427 */
 obj_t BgL_arg2608z00_3681;
BgL_arg2608z00_3681 = 
BGL_EXITD_PROTECT(BgL_exitd1075z00_1794); 
BgL_arg2607z00_3680 = 
CDR(
((obj_t)BgL_arg2608z00_3681)); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1075z00_1794, BgL_arg2607z00_3680); BUNSPEC; }  else 
{ /* Llib/error.scm 427 */BFALSE; } } 
BGL_ERROR_HANDLER_SET(BgL_oldzd2handlerszd2_1788); BUNSPEC; 
return BgL_tmp1076z00_1796;} } } } }  else 
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_3683;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_3684;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_3685;
BgL_new1078z00_3685 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_3686;
BgL_arg1513z00_3686 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_3685), BgL_arg1513z00_3686); } 
BgL_new1079z00_3684 = BgL_new1078z00_3685; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_3684)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_3684)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_5965;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_3687;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_3688;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_3692;
BgL_classz00_3692 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_3688 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3692); } 
BgL_arg1510z00_3687 = 
VECTOR_REF(BgL_arg1511z00_3688,2L); } 
BgL_auxz00_5965 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_3687); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_3684)))->BgL_stackz00)=((obj_t)BgL_auxz00_5965),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_3684))->BgL_procz00)=((obj_t)BGl_string2825z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_3684))->BgL_msgz00)=((obj_t)BGl_string2827z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_3684))->BgL_objz00)=((obj_t)BgL_handlerz00_14),BUNSPEC); 
BgL_arg1509z00_3683 = BgL_new1079z00_3684; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_3683));} } 

}



/* &with-exception-handler */
obj_t BGl_z62withzd2exceptionzd2handlerz62zz__errorz00(obj_t BgL_envz00_5110, obj_t BgL_handlerz00_5111, obj_t BgL_thunkz00_5112)
{
{ /* Llib/error.scm 419 */
{ /* Llib/error.scm 420 */
 obj_t BgL_auxz00_5983; obj_t BgL_auxz00_5976;
if(
PROCEDUREP(BgL_thunkz00_5112))
{ /* Llib/error.scm 420 */
BgL_auxz00_5983 = BgL_thunkz00_5112
; }  else 
{ 
 obj_t BgL_auxz00_5986;
BgL_auxz00_5986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(16958L), BGl_string2828z00zz__errorz00, BGl_string2829z00zz__errorz00, BgL_thunkz00_5112); 
FAILURE(BgL_auxz00_5986,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_handlerz00_5111))
{ /* Llib/error.scm 420 */
BgL_auxz00_5976 = BgL_handlerz00_5111
; }  else 
{ 
 obj_t BgL_auxz00_5979;
BgL_auxz00_5979 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(16958L), BGl_string2828z00zz__errorz00, BGl_string2829z00zz__errorz00, BgL_handlerz00_5111); 
FAILURE(BgL_auxz00_5979,BFALSE,BFALSE);} 
return 
BGl_withzd2exceptionzd2handlerz00zz__errorz00(BgL_auxz00_5976, BgL_auxz00_5983);} } 

}



/* &<@anonymous:1456> */
obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__errorz00(obj_t BgL_envz00_5113, obj_t BgL_cz00_5116)
{
{ /* Llib/error.scm 423 */
{ /* Llib/error.scm 424 */
 obj_t BgL_oldzd2handlerszd2_5114; obj_t BgL_handlerz00_5115;
BgL_oldzd2handlerszd2_5114 = 
PROCEDURE_REF(BgL_envz00_5113, 
(int)(0L)); 
BgL_handlerz00_5115 = 
PROCEDURE_REF(BgL_envz00_5113, 
(int)(1L)); 
BGL_ERROR_HANDLER_SET(BgL_oldzd2handlerszd2_5114); BUNSPEC; 
return 
BGL_PROCEDURE_CALL1(BgL_handlerz00_5115, BgL_cz00_5116);} } 

}



/* current-exception-handler */
BGL_EXPORTED_DEF obj_t BGl_currentzd2exceptionzd2handlerz00zz__errorz00(void)
{
{ /* Llib/error.scm 437 */
return BGl_proc2830z00zz__errorz00;} 

}



/* &current-exception-handler */
obj_t BGl_z62currentzd2exceptionzd2handlerz62zz__errorz00(obj_t BgL_envz00_5118)
{
{ /* Llib/error.scm 437 */
return 
BGl_currentzd2exceptionzd2handlerz00zz__errorz00();} 

}



/* &<@anonymous:1458> */
obj_t BGl_z62zc3z04anonymousza31458ze3ze5zz__errorz00(obj_t BgL_envz00_5119, obj_t BgL_valz00_5120)
{
{ /* Llib/error.scm 438 */
return 
BGl_raisez00zz__errorz00(BgL_valz00_5120);} 

}



/* raise */
BGL_EXPORTED_DEF obj_t BGl_raisez00zz__errorz00(obj_t BgL_valz00_16)
{
{ /* Llib/error.scm 465 */
{ 
 obj_t BgL_handlerz00_1829; obj_t BgL_valz00_1830;
{ /* Llib/error.scm 502 */
 obj_t BgL_handlerz00_1804;
BgL_handlerz00_1804 = 
BGL_ERROR_HANDLER_GET(); 
if(
PAIRP(BgL_handlerz00_1804))
{ /* Llib/error.scm 506 */
 bool_t BgL_test3200z00_6005;
{ /* Llib/error.scm 506 */
 obj_t BgL_tmpz00_6006;
BgL_tmpz00_6006 = 
CDR(BgL_handlerz00_1804); 
BgL_test3200z00_6005 = 
BGL_DYNAMIC_ENVP(BgL_tmpz00_6006); } 
if(BgL_test3200z00_6005)
{ /* Llib/error.scm 472 */
 obj_t BgL_denvz00_3707; obj_t BgL_hz00_3708;
BgL_denvz00_3707 = 
CDR(BgL_handlerz00_1804); 
BgL_hz00_3708 = 
CAR(BgL_handlerz00_1804); 
SET_CDR(BgL_handlerz00_1804, BgL_valz00_16); 
return 
BGl_unwindzd2untilz12zc0zz__bexitz00(BgL_hz00_3708, BgL_denvz00_3707);}  else 
{ /* Llib/error.scm 508 */
 bool_t BgL_test3201z00_6013;
{ /* Llib/error.scm 508 */
 obj_t BgL_tmpz00_6014;
BgL_tmpz00_6014 = 
CDR(BgL_handlerz00_1804); 
BgL_test3201z00_6013 = 
CELLP(BgL_tmpz00_6014); } 
if(BgL_test3201z00_6013)
{ /* Llib/error.scm 482 */
 obj_t BgL_cellz00_3713; obj_t BgL_hz00_3714;
BgL_cellz00_3713 = 
CDR(BgL_handlerz00_1804); 
BgL_hz00_3714 = 
CAR(BgL_handlerz00_1804); 
CELL_SET(
((obj_t)BgL_cellz00_3713), BgL_valz00_16); 
return 
BGl_unwindzd2untilz12zc0zz__bexitz00(BgL_hz00_3714, BgL_cellz00_3713);}  else 
{ /* Llib/error.scm 508 */
if(
CBOOL(
CDR(BgL_handlerz00_1804)))
{ /* Llib/error.scm 510 */
if(
(
CDR(BgL_handlerz00_1804)==BUNSPEC))
{ /* Llib/error.scm 514 */
 obj_t BgL_fun1467z00_1813;
BgL_fun1467z00_1813 = 
CAR(BgL_handlerz00_1804); 
return 
BGL_PROCEDURE_CALL1(BgL_fun1467z00_1813, BgL_valz00_16);}  else 
{ /* Llib/error.scm 513 */
BgL_handlerz00_1829 = BgL_handlerz00_1804; 
BgL_valz00_1830 = BgL_valz00_16; 
BgL_zc3z04anonymousza31476ze3z87_1831:
{ /* Llib/error.scm 498 */
 obj_t BgL_arg1477z00_1832;
{ /* Llib/error.scm 498 */
 obj_t BgL_tmpz00_6033;
BgL_tmpz00_6033 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1477z00_1832 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6033); } 
{ /* Llib/error.scm 498 */
 obj_t BgL_list1478z00_1833;
{ /* Llib/error.scm 498 */
 obj_t BgL_arg1479z00_1834;
{ /* Llib/error.scm 498 */
 obj_t BgL_arg1480z00_1835;
{ /* Llib/error.scm 498 */
 obj_t BgL_arg1481z00_1836;
{ /* Llib/error.scm 498 */
 obj_t BgL_arg1482z00_1837;
{ /* Llib/error.scm 498 */
 obj_t BgL_arg1483z00_1838;
BgL_arg1483z00_1838 = 
MAKE_YOUNG_PAIR(BgL_handlerz00_1829, BNIL); 
BgL_arg1482z00_1837 = 
MAKE_YOUNG_PAIR(BGl_string2831z00zz__errorz00, BgL_arg1483z00_1838); } 
BgL_arg1481z00_1836 = 
MAKE_YOUNG_PAIR(BGl_string2832z00zz__errorz00, BgL_arg1482z00_1837); } 
BgL_arg1480z00_1835 = 
MAKE_YOUNG_PAIR(
BINT(498L), BgL_arg1481z00_1836); } 
BgL_arg1479z00_1834 = 
MAKE_YOUNG_PAIR(BGl_string2833z00zz__errorz00, BgL_arg1480z00_1835); } 
BgL_list1478z00_1833 = 
MAKE_YOUNG_PAIR(BGl_string2834z00zz__errorz00, BgL_arg1479z00_1834); } 
BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1477z00_1832, BgL_list1478z00_1833); } } 
{ /* Llib/error.scm 499 */
 obj_t BgL_arg1484z00_1839;
{ /* Llib/error.scm 499 */
 obj_t BgL_tmpz00_6044;
BgL_tmpz00_6044 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1484z00_1839 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6044); } 
{ /* Llib/error.scm 499 */
 obj_t BgL_list1485z00_1840;
{ /* Llib/error.scm 499 */
 obj_t BgL_arg1486z00_1841;
{ /* Llib/error.scm 499 */
 obj_t BgL_arg1487z00_1842;
{ /* Llib/error.scm 499 */
 obj_t BgL_arg1488z00_1843;
{ /* Llib/error.scm 499 */
 obj_t BgL_arg1489z00_1844;
{ /* Llib/error.scm 499 */
 obj_t BgL_arg1490z00_1845;
BgL_arg1490z00_1845 = 
MAKE_YOUNG_PAIR(BgL_valz00_1830, BNIL); 
BgL_arg1489z00_1844 = 
MAKE_YOUNG_PAIR(BGl_string2835z00zz__errorz00, BgL_arg1490z00_1845); } 
BgL_arg1488z00_1843 = 
MAKE_YOUNG_PAIR(BGl_string2832z00zz__errorz00, BgL_arg1489z00_1844); } 
BgL_arg1487z00_1842 = 
MAKE_YOUNG_PAIR(
BINT(499L), BgL_arg1488z00_1843); } 
BgL_arg1486z00_1841 = 
MAKE_YOUNG_PAIR(BGl_string2833z00zz__errorz00, BgL_arg1487z00_1842); } 
BgL_list1485z00_1840 = 
MAKE_YOUNG_PAIR(BGl_string2834z00zz__errorz00, BgL_arg1486z00_1841); } 
BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1484z00_1839, BgL_list1485z00_1840); } } 
{ /* Llib/error.scm 500 */
 obj_t BgL_list1491z00_1846;
BgL_list1491z00_1846 = 
MAKE_YOUNG_PAIR(
BINT(0L), BNIL); 
BGL_TAIL return 
BGl_exitz00zz__errorz00(BgL_list1491z00_1846);} } }  else 
{ /* Llib/error.scm 510 */
BGl_defaultzd2exceptionzd2handlerz00zz__errorz00(BgL_valz00_16); 
{ /* Llib/error.scm 512 */
 obj_t BgL_res2749z00_3756;
{ /* Llib/error.scm 320 */
 obj_t BgL_classz00_3722;
BgL_classz00_3722 = BGl_z62exceptionz62zz__objectz00; ((bool_t)0); } 
BgL_res2749z00_3756 = 
BGl_errorz00zz__errorz00(BGl_string2836z00zz__errorz00, BGl_string2837z00zz__errorz00, BgL_valz00_16); 
return BgL_res2749z00_3756;} } } } }  else 
{ 
 obj_t BgL_valz00_6061; obj_t BgL_handlerz00_6060;
BgL_handlerz00_6060 = BgL_handlerz00_1804; 
BgL_valz00_6061 = BgL_valz00_16; 
BgL_valz00_1830 = BgL_valz00_6061; 
BgL_handlerz00_1829 = BgL_handlerz00_6060; 
goto BgL_zc3z04anonymousza31476ze3z87_1831;} } } } 

}



/* &raise */
obj_t BGl_z62raisez62zz__errorz00(obj_t BgL_envz00_5121, obj_t BgL_valz00_5122)
{
{ /* Llib/error.scm 465 */
return 
BGl_raisez00zz__errorz00(BgL_valz00_5122);} 

}



/* default-exception-handler */
obj_t BGl_defaultzd2exceptionzd2handlerz00zz__errorz00(obj_t BgL_valz00_17)
{
{ /* Llib/error.scm 523 */
BGl_exceptionzd2notifyzd2zz__objectz00(BgL_valz00_17); 
{ /* Llib/error.scm 525 */
 bool_t BgL_test3204z00_6064;
{ /* Llib/error.scm 525 */
 obj_t BgL_classz00_3757;
BgL_classz00_3757 = BGl_z62warningz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_valz00_17))
{ /* Llib/error.scm 525 */
 BgL_objectz00_bglt BgL_arg2721z00_3759;
BgL_arg2721z00_3759 = 
(BgL_objectz00_bglt)(BgL_valz00_17); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 525 */
 long BgL_idxz00_3765;
BgL_idxz00_3765 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_3759); 
BgL_test3204z00_6064 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_3765+3L))==BgL_classz00_3757); }  else 
{ /* Llib/error.scm 525 */
 bool_t BgL_res2750z00_3790;
{ /* Llib/error.scm 525 */
 obj_t BgL_oclassz00_3773;
{ /* Llib/error.scm 525 */
 obj_t BgL_arg2728z00_3781; long BgL_arg2729z00_3782;
BgL_arg2728z00_3781 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 525 */
 long BgL_arg2731z00_3783;
BgL_arg2731z00_3783 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_3759); 
BgL_arg2729z00_3782 = 
(BgL_arg2731z00_3783-OBJECT_TYPE); } 
BgL_oclassz00_3773 = 
VECTOR_REF(BgL_arg2728z00_3781,BgL_arg2729z00_3782); } 
{ /* Llib/error.scm 525 */
 bool_t BgL__ortest_1198z00_3774;
BgL__ortest_1198z00_3774 = 
(BgL_classz00_3757==BgL_oclassz00_3773); 
if(BgL__ortest_1198z00_3774)
{ /* Llib/error.scm 525 */
BgL_res2750z00_3790 = BgL__ortest_1198z00_3774; }  else 
{ /* Llib/error.scm 525 */
 long BgL_odepthz00_3775;
{ /* Llib/error.scm 525 */
 obj_t BgL_arg2717z00_3776;
BgL_arg2717z00_3776 = 
(BgL_oclassz00_3773); 
BgL_odepthz00_3775 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_3776); } 
if(
(3L<BgL_odepthz00_3775))
{ /* Llib/error.scm 525 */
 obj_t BgL_arg2715z00_3778;
{ /* Llib/error.scm 525 */
 obj_t BgL_arg2716z00_3779;
BgL_arg2716z00_3779 = 
(BgL_oclassz00_3773); 
BgL_arg2715z00_3778 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_3779, 3L); } 
BgL_res2750z00_3790 = 
(BgL_arg2715z00_3778==BgL_classz00_3757); }  else 
{ /* Llib/error.scm 525 */
BgL_res2750z00_3790 = ((bool_t)0); } } } } 
BgL_test3204z00_6064 = BgL_res2750z00_3790; } }  else 
{ /* Llib/error.scm 525 */
BgL_test3204z00_6064 = ((bool_t)0)
; } } 
if(BgL_test3204z00_6064)
{ /* Llib/error.scm 525 */BFALSE; }  else 
{ /* Llib/error.scm 526 */
 long BgL_retvalz00_1852;
{ /* Llib/error.scm 526 */
 bool_t BgL_test3209z00_6087;
{ /* Llib/error.scm 526 */
 obj_t BgL_classz00_3791;
BgL_classz00_3791 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_valz00_17))
{ /* Llib/error.scm 526 */
 BgL_objectz00_bglt BgL_arg2721z00_3793;
BgL_arg2721z00_3793 = 
(BgL_objectz00_bglt)(BgL_valz00_17); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 526 */
 long BgL_idxz00_3799;
BgL_idxz00_3799 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_3793); 
BgL_test3209z00_6087 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_3799+3L))==BgL_classz00_3791); }  else 
{ /* Llib/error.scm 526 */
 bool_t BgL_res2751z00_3824;
{ /* Llib/error.scm 526 */
 obj_t BgL_oclassz00_3807;
{ /* Llib/error.scm 526 */
 obj_t BgL_arg2728z00_3815; long BgL_arg2729z00_3816;
BgL_arg2728z00_3815 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 526 */
 long BgL_arg2731z00_3817;
BgL_arg2731z00_3817 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_3793); 
BgL_arg2729z00_3816 = 
(BgL_arg2731z00_3817-OBJECT_TYPE); } 
BgL_oclassz00_3807 = 
VECTOR_REF(BgL_arg2728z00_3815,BgL_arg2729z00_3816); } 
{ /* Llib/error.scm 526 */
 bool_t BgL__ortest_1198z00_3808;
BgL__ortest_1198z00_3808 = 
(BgL_classz00_3791==BgL_oclassz00_3807); 
if(BgL__ortest_1198z00_3808)
{ /* Llib/error.scm 526 */
BgL_res2751z00_3824 = BgL__ortest_1198z00_3808; }  else 
{ /* Llib/error.scm 526 */
 long BgL_odepthz00_3809;
{ /* Llib/error.scm 526 */
 obj_t BgL_arg2717z00_3810;
BgL_arg2717z00_3810 = 
(BgL_oclassz00_3807); 
BgL_odepthz00_3809 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_3810); } 
if(
(3L<BgL_odepthz00_3809))
{ /* Llib/error.scm 526 */
 obj_t BgL_arg2715z00_3812;
{ /* Llib/error.scm 526 */
 obj_t BgL_arg2716z00_3813;
BgL_arg2716z00_3813 = 
(BgL_oclassz00_3807); 
BgL_arg2715z00_3812 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_3813, 3L); } 
BgL_res2751z00_3824 = 
(BgL_arg2715z00_3812==BgL_classz00_3791); }  else 
{ /* Llib/error.scm 526 */
BgL_res2751z00_3824 = ((bool_t)0); } } } } 
BgL_test3209z00_6087 = BgL_res2751z00_3824; } }  else 
{ /* Llib/error.scm 526 */
BgL_test3209z00_6087 = ((bool_t)0)
; } } 
if(BgL_test3209z00_6087)
{ /* Llib/error.scm 526 */
BgL_retvalz00_1852 = 1L; }  else 
{ /* Llib/error.scm 526 */
BgL_retvalz00_1852 = 2L; } } 
{ /* Llib/error.scm 527 */
 obj_t BgL_zc3z04anonymousza31495ze3z87_5123;
BgL_zc3z04anonymousza31495ze3z87_5123 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31495ze3ze5zz__errorz00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31495ze3z87_5123, 
(int)(0L), 
BINT(BgL_retvalz00_1852)); 
unwind_stack_until(BFALSE, BFALSE, 
BINT(BgL_retvalz00_1852), BgL_zc3z04anonymousza31495ze3z87_5123, BFALSE); } } } 
return BUNSPEC;} 

}



/* &<@anonymous:1495> */
obj_t BGl_z62zc3z04anonymousza31495ze3ze5zz__errorz00(obj_t BgL_envz00_5124, obj_t BgL_xz00_5126)
{
{ /* Llib/error.scm 527 */
{ /* Llib/error.scm 527 */
 long BgL_retvalz00_5125;
BgL_retvalz00_5125 = 
(long)CINT(
PROCEDURE_REF(BgL_envz00_5124, 
(int)(0L))); 
{ /* Llib/error.scm 527 */
 obj_t BgL_tmpz00_6121;
BgL_tmpz00_6121 = 
BINT(BgL_retvalz00_5125); 
return 
BIGLOO_EXIT(BgL_tmpz00_6121);} } } 

}



/* module-init-error */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initzd2errorz00zz__errorz00(char * BgL_currentz00_18, char * BgL_fromz00_19)
{
{ /* Llib/error.scm 533 */
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1497z00_1858;
{ /* Llib/error.scm 534 */
 obj_t BgL_tmpz00_6124;
BgL_tmpz00_6124 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1497z00_1858 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6124); } 
{ /* Llib/error.scm 534 */
 obj_t BgL_list1498z00_1859;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1499z00_1860;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1500z00_1861;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1501z00_1862;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1502z00_1863;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1503z00_1864;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1504z00_1865;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1505z00_1866;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1506z00_1867;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1507z00_1868;
{ /* Llib/error.scm 534 */
 obj_t BgL_arg1508z00_1869;
BgL_arg1508z00_1869 = 
MAKE_YOUNG_PAIR(BGl_string2838z00zz__errorz00, BNIL); 
BgL_arg1507z00_1868 = 
MAKE_YOUNG_PAIR(
string_to_bstring(BgL_fromz00_19), BgL_arg1508z00_1869); } 
BgL_arg1506z00_1867 = 
MAKE_YOUNG_PAIR(BGl_string2839z00zz__errorz00, BgL_arg1507z00_1868); } 
BgL_arg1505z00_1866 = 
MAKE_YOUNG_PAIR(BGl_string2840z00zz__errorz00, BgL_arg1506z00_1867); } 
BgL_arg1504z00_1865 = 
MAKE_YOUNG_PAIR(
string_to_bstring(BgL_fromz00_19), BgL_arg1505z00_1866); } 
BgL_arg1503z00_1864 = 
MAKE_YOUNG_PAIR(BGl_string2841z00zz__errorz00, BgL_arg1504z00_1865); } 
BgL_arg1502z00_1863 = 
MAKE_YOUNG_PAIR(
string_to_bstring(BgL_currentz00_18), BgL_arg1503z00_1864); } 
BgL_arg1501z00_1862 = 
MAKE_YOUNG_PAIR(BGl_string2842z00zz__errorz00, BgL_arg1502z00_1863); } 
BgL_arg1500z00_1861 = 
MAKE_YOUNG_PAIR(BGl_string2843z00zz__errorz00, BgL_arg1501z00_1862); } 
BgL_arg1499z00_1860 = 
MAKE_YOUNG_PAIR(
string_to_bstring(BgL_currentz00_18), BgL_arg1500z00_1861); } 
BgL_list1498z00_1859 = 
MAKE_YOUNG_PAIR(BGl_string2844z00zz__errorz00, BgL_arg1499z00_1860); } 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg1497z00_1858, BgL_list1498z00_1859); } } 
{ /* Llib/error.scm 539 */
 obj_t BgL_tmpz00_6143;
BgL_tmpz00_6143 = 
BINT(1L); 
return 
BIGLOO_EXIT(BgL_tmpz00_6143);} } 

}



/* &module-init-error */
obj_t BGl_z62modulezd2initzd2errorz62zz__errorz00(obj_t BgL_envz00_5127, obj_t BgL_currentz00_5128, obj_t BgL_fromz00_5129)
{
{ /* Llib/error.scm 533 */
{ /* Llib/error.scm 538 */
 char * BgL_auxz00_6155; char * BgL_auxz00_6146;
{ /* Llib/error.scm 538 */
 obj_t BgL_tmpz00_6156;
if(
STRINGP(BgL_fromz00_5129))
{ /* Llib/error.scm 538 */
BgL_tmpz00_6156 = BgL_fromz00_5129
; }  else 
{ 
 obj_t BgL_auxz00_6159;
BgL_auxz00_6159 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(22087L), BGl_string2845z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_fromz00_5129); 
FAILURE(BgL_auxz00_6159,BFALSE,BFALSE);} 
BgL_auxz00_6155 = 
BSTRING_TO_STRING(BgL_tmpz00_6156); } 
{ /* Llib/error.scm 538 */
 obj_t BgL_tmpz00_6147;
if(
STRINGP(BgL_currentz00_5128))
{ /* Llib/error.scm 538 */
BgL_tmpz00_6147 = BgL_currentz00_5128
; }  else 
{ 
 obj_t BgL_auxz00_6150;
BgL_auxz00_6150 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(22087L), BGl_string2845z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_currentz00_5128); 
FAILURE(BgL_auxz00_6150,BFALSE,BFALSE);} 
BgL_auxz00_6146 = 
BSTRING_TO_STRING(BgL_tmpz00_6147); } 
return 
BGl_modulezd2initzd2errorz00zz__errorz00(BgL_auxz00_6146, BgL_auxz00_6155);} } 

}



/* error */
BGL_EXPORTED_DEF obj_t BGl_errorz00zz__errorz00(obj_t BgL_procz00_20, obj_t BgL_msgz00_21, obj_t BgL_objz00_22)
{
{ /* Llib/error.scm 544 */
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_1870;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_1871;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_1874;
BgL_new1078z00_1874 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_1875;
BgL_arg1513z00_1875 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_1874), BgL_arg1513z00_1875); } 
BgL_new1079z00_1871 = BgL_new1078z00_1874; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_1871)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_1871)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6173;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_1872;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_1873;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_3829;
BgL_classz00_3829 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_1873 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3829); } 
BgL_arg1510z00_1872 = 
VECTOR_REF(BgL_arg1511z00_1873,2L); } 
BgL_auxz00_6173 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_1872); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_1871)))->BgL_stackz00)=((obj_t)BgL_auxz00_6173),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_1871))->BgL_procz00)=((obj_t)BgL_procz00_20),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_1871))->BgL_msgz00)=((obj_t)BgL_msgz00_21),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_1871))->BgL_objz00)=((obj_t)BgL_objz00_22),BUNSPEC); 
BgL_arg1509z00_1870 = BgL_new1079z00_1871; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_1870));} } 

}



/* &error2768 */
obj_t BGl_z62error2768z62zz__errorz00(obj_t BgL_envz00_5130, obj_t BgL_procz00_5131, obj_t BgL_msgz00_5132, obj_t BgL_objz00_5133)
{
{ /* Llib/error.scm 544 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_5131, BgL_msgz00_5132, BgL_objz00_5133);} 

}



/* error/location */
BGL_EXPORTED_DEF obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t BgL_procz00_23, obj_t BgL_msgz00_24, obj_t BgL_objz00_25, obj_t BgL_fnamez00_26, obj_t BgL_locz00_27)
{
{ /* Llib/error.scm 554 */
{ /* Llib/error.scm 556 */
 BgL_z62errorz62_bglt BgL_arg1514z00_3831;
{ /* Llib/error.scm 556 */
 BgL_z62errorz62_bglt BgL_new1081z00_3832;
{ /* Llib/error.scm 557 */
 BgL_z62errorz62_bglt BgL_new1080z00_3833;
BgL_new1080z00_3833 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 557 */
 long BgL_arg1521z00_3834;
BgL_arg1521z00_3834 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1080z00_3833), BgL_arg1521z00_3834); } 
BgL_new1081z00_3832 = BgL_new1080z00_3833; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1081z00_3832)))->BgL_fnamez00)=((obj_t)BgL_fnamez00_26),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1081z00_3832)))->BgL_locationz00)=((obj_t)BgL_locz00_27),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6193;
{ /* Llib/error.scm 558 */
 obj_t BgL_arg1516z00_3835;
{ /* Llib/error.scm 558 */
 obj_t BgL_arg1517z00_3836;
{ /* Llib/error.scm 558 */
 obj_t BgL_classz00_3840;
BgL_classz00_3840 = BGl_z62errorz62zz__objectz00; 
BgL_arg1517z00_3836 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3840); } 
BgL_arg1516z00_3835 = 
VECTOR_REF(BgL_arg1517z00_3836,2L); } 
BgL_auxz00_6193 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1516z00_3835); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1081z00_3832)))->BgL_stackz00)=((obj_t)BgL_auxz00_6193),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1081z00_3832))->BgL_procz00)=((obj_t)BgL_procz00_23),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1081z00_3832))->BgL_msgz00)=((obj_t)BgL_msgz00_24),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1081z00_3832))->BgL_objz00)=((obj_t)BgL_objz00_25),BUNSPEC); 
BgL_arg1514z00_3831 = BgL_new1081z00_3832; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1514z00_3831));} } 

}



/* &error/location */
obj_t BGl_z62errorzf2locationz90zz__errorz00(obj_t BgL_envz00_5134, obj_t BgL_procz00_5135, obj_t BgL_msgz00_5136, obj_t BgL_objz00_5137, obj_t BgL_fnamez00_5138, obj_t BgL_locz00_5139)
{
{ /* Llib/error.scm 554 */
return 
BGl_errorzf2locationzf2zz__errorz00(BgL_procz00_5135, BgL_msgz00_5136, BgL_objz00_5137, BgL_fnamez00_5138, BgL_locz00_5139);} 

}



/* error/source-location */
BGL_EXPORTED_DEF obj_t BGl_errorzf2sourcezd2locationz20zz__errorz00(obj_t BgL_procz00_28, obj_t BgL_msgz00_29, obj_t BgL_objz00_30, obj_t BgL_locz00_31)
{
{ /* Llib/error.scm 566 */
if(
PAIRP(BgL_locz00_31))
{ /* Llib/error.scm 567 */
 obj_t BgL_cdrzd2186zd2_1888;
BgL_cdrzd2186zd2_1888 = 
CDR(
((obj_t)BgL_locz00_31)); 
if(
(
CAR(
((obj_t)BgL_locz00_31))==BGl_symbol2823z00zz__errorz00))
{ /* Llib/error.scm 567 */
if(
PAIRP(BgL_cdrzd2186zd2_1888))
{ /* Llib/error.scm 567 */
 obj_t BgL_cdrzd2190zd2_1892;
BgL_cdrzd2190zd2_1892 = 
CDR(BgL_cdrzd2186zd2_1888); 
if(
PAIRP(BgL_cdrzd2190zd2_1892))
{ /* Llib/error.scm 567 */
if(
NULLP(
CDR(BgL_cdrzd2190zd2_1892)))
{ /* Llib/error.scm 567 */
return 
BGl_errorzf2locationzf2zz__errorz00(BgL_procz00_28, BgL_msgz00_29, BgL_objz00_30, 
CAR(BgL_cdrzd2186zd2_1888), 
CAR(BgL_cdrzd2190zd2_1892));}  else 
{ /* Llib/error.scm 567 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29, BgL_objz00_30);} }  else 
{ /* Llib/error.scm 567 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29, BgL_objz00_30);} }  else 
{ /* Llib/error.scm 567 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29, BgL_objz00_30);} }  else 
{ /* Llib/error.scm 567 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29, BgL_objz00_30);} }  else 
{ /* Llib/error.scm 567 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_28, BgL_msgz00_29, BgL_objz00_30);} } 

}



/* &error/source-location */
obj_t BGl_z62errorzf2sourcezd2locationz42zz__errorz00(obj_t BgL_envz00_5140, obj_t BgL_procz00_5141, obj_t BgL_msgz00_5142, obj_t BgL_objz00_5143, obj_t BgL_locz00_5144)
{
{ /* Llib/error.scm 566 */
return 
BGl_errorzf2sourcezd2locationz20zz__errorz00(BgL_procz00_5141, BgL_msgz00_5142, BgL_objz00_5143, BgL_locz00_5144);} 

}



/* error/source */
BGL_EXPORTED_DEF obj_t BGl_errorzf2sourcezf2zz__errorz00(obj_t BgL_procz00_32, obj_t BgL_msgz00_33, obj_t BgL_objz00_34, obj_t BgL_sourcez00_35)
{
{ /* Llib/error.scm 576 */
if(
EPAIRP(BgL_sourcez00_35))
{ /* Llib/error.scm 579 */
 obj_t BgL_arg1537z00_3861;
BgL_arg1537z00_3861 = 
CER(
((obj_t)BgL_sourcez00_35)); 
BGL_TAIL return 
BGl_errorzf2sourcezd2locationz20zz__errorz00(BgL_procz00_32, BgL_msgz00_33, BgL_objz00_34, BgL_arg1537z00_3861);}  else 
{ /* Llib/error.scm 577 */
return 
BGl_errorz00zz__errorz00(BgL_procz00_32, BgL_msgz00_33, BgL_objz00_34);} } 

}



/* &error/source */
obj_t BGl_z62errorzf2sourcez90zz__errorz00(obj_t BgL_envz00_5145, obj_t BgL_procz00_5146, obj_t BgL_msgz00_5147, obj_t BgL_objz00_5148, obj_t BgL_sourcez00_5149)
{
{ /* Llib/error.scm 576 */
return 
BGl_errorzf2sourcezf2zz__errorz00(BgL_procz00_5146, BgL_msgz00_5147, BgL_objz00_5148, BgL_sourcez00_5149);} 

}



/* error/c-location */
BGL_EXPORTED_DEF obj_t BGl_errorzf2czd2locationz20zz__errorz00(obj_t BgL_procz00_36, obj_t BgL_messagez00_37, obj_t BgL_objectz00_38, char * BgL_fnamez00_39, long BgL_locz00_40)
{
{ /* Llib/error.scm 588 */
{ /* Llib/error.scm 556 */
 BgL_z62errorz62_bglt BgL_arg1514z00_3863;
{ /* Llib/error.scm 556 */
 BgL_z62errorz62_bglt BgL_new1081z00_3864;
{ /* Llib/error.scm 557 */
 BgL_z62errorz62_bglt BgL_new1080z00_3865;
BgL_new1080z00_3865 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 557 */
 long BgL_arg1521z00_3866;
BgL_arg1521z00_3866 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1080z00_3865), BgL_arg1521z00_3866); } 
BgL_new1081z00_3864 = BgL_new1080z00_3865; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1081z00_3864)))->BgL_fnamez00)=((obj_t)
string_to_bstring(BgL_fnamez00_39)),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1081z00_3864)))->BgL_locationz00)=((obj_t)
BINT(BgL_locz00_40)),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6247;
{ /* Llib/error.scm 558 */
 obj_t BgL_arg1516z00_3867;
{ /* Llib/error.scm 558 */
 obj_t BgL_arg1517z00_3868;
{ /* Llib/error.scm 558 */
 obj_t BgL_classz00_3872;
BgL_classz00_3872 = BGl_z62errorz62zz__objectz00; 
BgL_arg1517z00_3868 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3872); } 
BgL_arg1516z00_3867 = 
VECTOR_REF(BgL_arg1517z00_3868,2L); } 
BgL_auxz00_6247 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1516z00_3867); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1081z00_3864)))->BgL_stackz00)=((obj_t)BgL_auxz00_6247),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1081z00_3864))->BgL_procz00)=((obj_t)BgL_procz00_36),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1081z00_3864))->BgL_msgz00)=((obj_t)BgL_messagez00_37),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1081z00_3864))->BgL_objz00)=((obj_t)BgL_objectz00_38),BUNSPEC); 
BgL_arg1514z00_3863 = BgL_new1081z00_3864; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1514z00_3863));} } 

}



/* &error/c-location */
obj_t BGl_z62errorzf2czd2locationz42zz__errorz00(obj_t BgL_envz00_5150, obj_t BgL_procz00_5151, obj_t BgL_messagez00_5152, obj_t BgL_objectz00_5153, obj_t BgL_fnamez00_5154, obj_t BgL_locz00_5155)
{
{ /* Llib/error.scm 588 */
{ /* Llib/error.scm 556 */
 long BgL_auxz00_6267; char * BgL_auxz00_6258;
{ /* Llib/error.scm 556 */
 obj_t BgL_tmpz00_6268;
if(
INTEGERP(BgL_locz00_5155))
{ /* Llib/error.scm 556 */
BgL_tmpz00_6268 = BgL_locz00_5155
; }  else 
{ 
 obj_t BgL_auxz00_6271;
BgL_auxz00_6271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(22720L), BGl_string2847z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_locz00_5155); 
FAILURE(BgL_auxz00_6271,BFALSE,BFALSE);} 
BgL_auxz00_6267 = 
(long)CINT(BgL_tmpz00_6268); } 
{ /* Llib/error.scm 556 */
 obj_t BgL_tmpz00_6259;
if(
STRINGP(BgL_fnamez00_5154))
{ /* Llib/error.scm 556 */
BgL_tmpz00_6259 = BgL_fnamez00_5154
; }  else 
{ 
 obj_t BgL_auxz00_6262;
BgL_auxz00_6262 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(22720L), BGl_string2847z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_fnamez00_5154); 
FAILURE(BgL_auxz00_6262,BFALSE,BFALSE);} 
BgL_auxz00_6258 = 
BSTRING_TO_STRING(BgL_tmpz00_6259); } 
return 
BGl_errorzf2czd2locationz20zz__errorz00(BgL_procz00_5151, BgL_messagez00_5152, BgL_objectz00_5153, BgL_auxz00_6258, BgL_auxz00_6267);} } 

}



/* bigloo-type-error-msg */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(obj_t BgL_prefixz00_41, obj_t BgL_fromz00_42, obj_t BgL_toz00_43)
{
{ /* Llib/error.scm 594 */
{ /* Llib/error.scm 595 */
 obj_t BgL_list1538z00_3874;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1539z00_3875;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1540z00_3876;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1543z00_3877;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1544z00_3878;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1546z00_3879;
BgL_arg1546z00_3879 = 
MAKE_YOUNG_PAIR(BGl_string2848z00zz__errorz00, BNIL); 
BgL_arg1544z00_3878 = 
MAKE_YOUNG_PAIR(BgL_toz00_43, BgL_arg1546z00_3879); } 
BgL_arg1543z00_3877 = 
MAKE_YOUNG_PAIR(BGl_string2849z00zz__errorz00, BgL_arg1544z00_3878); } 
BgL_arg1540z00_3876 = 
MAKE_YOUNG_PAIR(BgL_fromz00_42, BgL_arg1543z00_3877); } 
BgL_arg1539z00_3875 = 
MAKE_YOUNG_PAIR(BGl_string2850z00zz__errorz00, BgL_arg1540z00_3876); } 
BgL_list1538z00_3874 = 
MAKE_YOUNG_PAIR(BgL_prefixz00_41, BgL_arg1539z00_3875); } 
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1538z00_3874);} } 

}



/* &bigloo-type-error-msg */
obj_t BGl_z62bigloozd2typezd2errorzd2msgzb0zz__errorz00(obj_t BgL_envz00_5156, obj_t BgL_prefixz00_5157, obj_t BgL_fromz00_5158, obj_t BgL_toz00_5159)
{
{ /* Llib/error.scm 594 */
{ /* Llib/error.scm 595 */
 obj_t BgL_auxz00_6298; obj_t BgL_auxz00_6291; obj_t BgL_auxz00_6284;
if(
STRINGP(BgL_toz00_5159))
{ /* Llib/error.scm 595 */
BgL_auxz00_6298 = BgL_toz00_5159
; }  else 
{ 
 obj_t BgL_auxz00_6301;
BgL_auxz00_6301 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(24503L), BGl_string2851z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_toz00_5159); 
FAILURE(BgL_auxz00_6301,BFALSE,BFALSE);} 
if(
STRINGP(BgL_fromz00_5158))
{ /* Llib/error.scm 595 */
BgL_auxz00_6291 = BgL_fromz00_5158
; }  else 
{ 
 obj_t BgL_auxz00_6294;
BgL_auxz00_6294 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(24503L), BGl_string2851z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_fromz00_5158); 
FAILURE(BgL_auxz00_6294,BFALSE,BFALSE);} 
if(
STRINGP(BgL_prefixz00_5157))
{ /* Llib/error.scm 595 */
BgL_auxz00_6284 = BgL_prefixz00_5157
; }  else 
{ 
 obj_t BgL_auxz00_6287;
BgL_auxz00_6287 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(24503L), BGl_string2851z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_prefixz00_5157); 
FAILURE(BgL_auxz00_6287,BFALSE,BFALSE);} 
return 
BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(BgL_auxz00_6284, BgL_auxz00_6291, BgL_auxz00_6298);} } 

}



/* bigloo-type-error */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t BgL_procz00_44, obj_t BgL_typez00_45, obj_t BgL_objz00_46)
{
{ /* Llib/error.scm 600 */
{ /* Llib/error.scm 601 */
 obj_t BgL_tyz00_1908;
if(
STRINGP(BgL_typez00_45))
{ /* Llib/error.scm 602 */
BgL_tyz00_1908 = BgL_typez00_45; }  else 
{ /* Llib/error.scm 602 */
if(
SYMBOLP(BgL_typez00_45))
{ /* Llib/error.scm 605 */
 obj_t BgL_arg2328z00_3881;
BgL_arg2328z00_3881 = 
SYMBOL_TO_STRING(BgL_typez00_45); 
BgL_tyz00_1908 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_3881); }  else 
{ /* Llib/error.scm 604 */
BgL_tyz00_1908 = BGl_string2852z00zz__errorz00; } } 
{ /* Llib/error.scm 601 */
 obj_t BgL_msgz00_1909;
{ /* Llib/error.scm 608 */
 obj_t BgL_arg1554z00_1916;
BgL_arg1554z00_1916 = 
bgl_typeof(BgL_objz00_46); 
{ /* Llib/error.scm 595 */
 obj_t BgL_list1538z00_3885;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1539z00_3886;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1540z00_3887;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1543z00_3888;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1544z00_3889;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1546z00_3890;
BgL_arg1546z00_3890 = 
MAKE_YOUNG_PAIR(BGl_string2848z00zz__errorz00, BNIL); 
BgL_arg1544z00_3889 = 
MAKE_YOUNG_PAIR(BgL_arg1554z00_1916, BgL_arg1546z00_3890); } 
BgL_arg1543z00_3888 = 
MAKE_YOUNG_PAIR(BGl_string2849z00zz__errorz00, BgL_arg1544z00_3889); } 
BgL_arg1540z00_3887 = 
MAKE_YOUNG_PAIR(BgL_tyz00_1908, BgL_arg1543z00_3888); } 
BgL_arg1539z00_3886 = 
MAKE_YOUNG_PAIR(BGl_string2850z00zz__errorz00, BgL_arg1540z00_3887); } 
BgL_list1538z00_3885 = 
MAKE_YOUNG_PAIR(BGl_string2853z00zz__errorz00, BgL_arg1539z00_3886); } 
BgL_msgz00_1909 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1538z00_3885); } } 
{ /* Llib/error.scm 608 */

{ /* Llib/error.scm 610 */
 BgL_z62typezd2errorzb0_bglt BgL_arg1547z00_1910;
{ /* Llib/error.scm 610 */
 BgL_z62typezd2errorzb0_bglt BgL_new1083z00_1911;
{ /* Llib/error.scm 610 */
 BgL_z62typezd2errorzb0_bglt BgL_new1082z00_1914;
BgL_new1082z00_1914 = 
((BgL_z62typezd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62typezd2errorzb0_bgl) ))); 
{ /* Llib/error.scm 610 */
 long BgL_arg1553z00_1915;
BgL_arg1553z00_1915 = 
BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1082z00_1914), BgL_arg1553z00_1915); } 
BgL_new1083z00_1911 = BgL_new1082z00_1914; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1083z00_1911)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1083z00_1911)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6328;
{ /* Llib/error.scm 610 */
 obj_t BgL_arg1549z00_1912;
{ /* Llib/error.scm 610 */
 obj_t BgL_arg1552z00_1913;
{ /* Llib/error.scm 610 */
 obj_t BgL_classz00_3894;
BgL_classz00_3894 = BGl_z62typezd2errorzb0zz__objectz00; 
BgL_arg1552z00_1913 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3894); } 
BgL_arg1549z00_1912 = 
VECTOR_REF(BgL_arg1552z00_1913,2L); } 
BgL_auxz00_6328 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1549z00_1912); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1083z00_1911)))->BgL_stackz00)=((obj_t)BgL_auxz00_6328),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1083z00_1911)))->BgL_procz00)=((obj_t)BgL_procz00_44),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1083z00_1911)))->BgL_msgz00)=((obj_t)BgL_msgz00_1909),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1083z00_1911)))->BgL_objz00)=((obj_t)BgL_objz00_46),BUNSPEC); 
((((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_new1083z00_1911))->BgL_typez00)=((obj_t)BgL_typez00_45),BUNSPEC); 
BgL_arg1547z00_1910 = BgL_new1083z00_1911; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1547z00_1910));} } } } } 

}



/* &bigloo-type-error */
obj_t BGl_z62bigloozd2typezd2errorz62zz__errorz00(obj_t BgL_envz00_5160, obj_t BgL_procz00_5161, obj_t BgL_typez00_5162, obj_t BgL_objz00_5163)
{
{ /* Llib/error.scm 600 */
return 
BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procz00_5161, BgL_typez00_5162, BgL_objz00_5163);} 

}



/* bigloo-type-error/location */
BGL_EXPORTED_DEF obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t BgL_procz00_47, obj_t BgL_typez00_48, obj_t BgL_objz00_49, obj_t BgL_fnamez00_50, obj_t BgL_locz00_51)
{
{ /* Llib/error.scm 619 */
return 
BGl_raisez00zz__errorz00(
BGl_typezd2errorzd2zz__errorz00(BgL_fnamez00_50, BgL_locz00_51, BgL_procz00_47, BgL_typez00_48, BgL_objz00_49));} 

}



/* &bigloo-type-error/location */
obj_t BGl_z62bigloozd2typezd2errorzf2locationz90zz__errorz00(obj_t BgL_envz00_5164, obj_t BgL_procz00_5165, obj_t BgL_typez00_5166, obj_t BgL_objz00_5167, obj_t BgL_fnamez00_5168, obj_t BgL_locz00_5169)
{
{ /* Llib/error.scm 619 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BgL_procz00_5165, BgL_typez00_5166, BgL_objz00_5167, BgL_fnamez00_5168, BgL_locz00_5169);} 

}



/* type-error */
BGL_EXPORTED_DEF obj_t BGl_typezd2errorzd2zz__errorz00(obj_t BgL_fnamez00_52, obj_t BgL_locz00_53, obj_t BgL_procz00_54, obj_t BgL_typez00_55, obj_t BgL_objz00_56)
{
{ /* Llib/error.scm 625 */
{ /* Llib/error.scm 626 */
 obj_t BgL_tyz00_1920;
if(
STRINGP(BgL_typez00_55))
{ /* Llib/error.scm 627 */
BgL_tyz00_1920 = BgL_typez00_55; }  else 
{ /* Llib/error.scm 627 */
if(
SYMBOLP(BgL_typez00_55))
{ /* Llib/error.scm 628 */
 obj_t BgL_arg2328z00_3898;
BgL_arg2328z00_3898 = 
SYMBOL_TO_STRING(BgL_typez00_55); 
BgL_tyz00_1920 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_3898); }  else 
{ /* Llib/error.scm 628 */
BgL_tyz00_1920 = BGl_string2852z00zz__errorz00; } } 
{ /* Llib/error.scm 626 */
 obj_t BgL_msgz00_1921;
{ /* Llib/error.scm 630 */
 obj_t BgL_arg1562z00_1927;
BgL_arg1562z00_1927 = 
bgl_typeof(BgL_objz00_56); 
{ /* Llib/error.scm 595 */
 obj_t BgL_list1538z00_3902;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1539z00_3903;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1540z00_3904;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1543z00_3905;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1544z00_3906;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1546z00_3907;
BgL_arg1546z00_3907 = 
MAKE_YOUNG_PAIR(BGl_string2848z00zz__errorz00, BNIL); 
BgL_arg1544z00_3906 = 
MAKE_YOUNG_PAIR(BgL_arg1562z00_1927, BgL_arg1546z00_3907); } 
BgL_arg1543z00_3905 = 
MAKE_YOUNG_PAIR(BGl_string2849z00zz__errorz00, BgL_arg1544z00_3906); } 
BgL_arg1540z00_3904 = 
MAKE_YOUNG_PAIR(BgL_tyz00_1920, BgL_arg1543z00_3905); } 
BgL_arg1539z00_3903 = 
MAKE_YOUNG_PAIR(BGl_string2850z00zz__errorz00, BgL_arg1540z00_3904); } 
BgL_list1538z00_3902 = 
MAKE_YOUNG_PAIR(BGl_string2853z00zz__errorz00, BgL_arg1539z00_3903); } 
BgL_msgz00_1921 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1538z00_3902); } } 
{ /* Llib/error.scm 630 */

{ /* Llib/error.scm 631 */
 BgL_z62typezd2errorzb0_bglt BgL_new1085z00_1922;
{ /* Llib/error.scm 632 */
 BgL_z62typezd2errorzb0_bglt BgL_new1084z00_1925;
BgL_new1084z00_1925 = 
((BgL_z62typezd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62typezd2errorzb0_bgl) ))); 
{ /* Llib/error.scm 632 */
 long BgL_arg1561z00_1926;
BgL_arg1561z00_1926 = 
BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1084z00_1925), BgL_arg1561z00_1926); } 
BgL_new1085z00_1922 = BgL_new1084z00_1925; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1085z00_1922)))->BgL_fnamez00)=((obj_t)BgL_fnamez00_52),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1085z00_1922)))->BgL_locationz00)=((obj_t)BgL_locz00_53),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6369;
{ /* Llib/error.scm 633 */
 obj_t BgL_arg1558z00_1923;
{ /* Llib/error.scm 633 */
 obj_t BgL_arg1559z00_1924;
{ /* Llib/error.scm 633 */
 obj_t BgL_classz00_3911;
BgL_classz00_3911 = BGl_z62typezd2errorzb0zz__objectz00; 
BgL_arg1559z00_1924 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3911); } 
BgL_arg1558z00_1923 = 
VECTOR_REF(BgL_arg1559z00_1924,2L); } 
BgL_auxz00_6369 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1558z00_1923); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1085z00_1922)))->BgL_stackz00)=((obj_t)BgL_auxz00_6369),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1085z00_1922)))->BgL_procz00)=((obj_t)BgL_procz00_54),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1085z00_1922)))->BgL_msgz00)=((obj_t)BgL_msgz00_1921),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1085z00_1922)))->BgL_objz00)=((obj_t)BgL_objz00_56),BUNSPEC); 
((((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_new1085z00_1922))->BgL_typez00)=((obj_t)BgL_typez00_55),BUNSPEC); 
return 
((obj_t)BgL_new1085z00_1922);} } } } } 

}



/* &type-error2769 */
obj_t BGl_z62typezd2error2769zb0zz__errorz00(obj_t BgL_envz00_5170, obj_t BgL_fnamez00_5171, obj_t BgL_locz00_5172, obj_t BgL_procz00_5173, obj_t BgL_typez00_5174, obj_t BgL_objz00_5175)
{
{ /* Llib/error.scm 625 */
return 
BGl_typezd2errorzd2zz__errorz00(BgL_fnamez00_5171, BgL_locz00_5172, BgL_procz00_5173, BgL_typez00_5174, BgL_objz00_5175);} 

}



/* typename-error */
BgL_z62typezd2errorzb0_bglt BGl_typenamezd2errorzd2zz__errorz00(bool_t BgL_fnamez00_57, bool_t BgL_locz00_58, obj_t BgL_procz00_59, obj_t BgL_typez00_60, obj_t BgL_objz00_61)
{
{ /* Llib/error.scm 642 */
{ /* Llib/error.scm 643 */
 obj_t BgL_tyz00_1930;
if(
STRINGP(BgL_typez00_60))
{ /* Llib/error.scm 644 */
BgL_tyz00_1930 = BgL_typez00_60; }  else 
{ /* Llib/error.scm 644 */
if(
SYMBOLP(BgL_typez00_60))
{ /* Llib/error.scm 645 */
 obj_t BgL_arg2328z00_3914;
BgL_arg2328z00_3914 = 
SYMBOL_TO_STRING(BgL_typez00_60); 
BgL_tyz00_1930 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_3914); }  else 
{ /* Llib/error.scm 645 */
BgL_tyz00_1930 = BGl_string2852z00zz__errorz00; } } 
{ /* Llib/error.scm 643 */
 obj_t BgL_msgz00_1931;
{ /* Llib/error.scm 595 */
 obj_t BgL_list1538z00_3918;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1539z00_3919;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1540z00_3920;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1543z00_3921;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1544z00_3922;
{ /* Llib/error.scm 595 */
 obj_t BgL_arg1546z00_3923;
BgL_arg1546z00_3923 = 
MAKE_YOUNG_PAIR(BGl_string2848z00zz__errorz00, BNIL); 
BgL_arg1544z00_3922 = 
MAKE_YOUNG_PAIR(
((obj_t)BgL_objz00_61), BgL_arg1546z00_3923); } 
BgL_arg1543z00_3921 = 
MAKE_YOUNG_PAIR(BGl_string2849z00zz__errorz00, BgL_arg1544z00_3922); } 
BgL_arg1540z00_3920 = 
MAKE_YOUNG_PAIR(BgL_tyz00_1930, BgL_arg1543z00_3921); } 
BgL_arg1539z00_3919 = 
MAKE_YOUNG_PAIR(BGl_string2850z00zz__errorz00, BgL_arg1540z00_3920); } 
BgL_list1538z00_3918 = 
MAKE_YOUNG_PAIR(BGl_string2853z00zz__errorz00, BgL_arg1539z00_3919); } 
BgL_msgz00_1931 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1538z00_3918); } 
{ /* Llib/error.scm 647 */

{ /* Llib/error.scm 648 */
 BgL_z62typezd2errorzb0_bglt BgL_new1087z00_1932;
{ /* Llib/error.scm 649 */
 BgL_z62typezd2errorzb0_bglt BgL_new1086z00_1935;
BgL_new1086z00_1935 = 
((BgL_z62typezd2errorzb0_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62typezd2errorzb0_bgl) ))); 
{ /* Llib/error.scm 649 */
 long BgL_arg1571z00_1936;
BgL_arg1571z00_1936 = 
BGL_CLASS_NUM(BGl_z62typezd2errorzb0zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1086z00_1935), BgL_arg1571z00_1936); } 
BgL_new1087z00_1932 = BgL_new1086z00_1935; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1087z00_1932)))->BgL_fnamez00)=((obj_t)
BBOOL(BgL_fnamez00_57)),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1087z00_1932)))->BgL_locationz00)=((obj_t)
BBOOL(BgL_locz00_58)),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6408;
{ /* Llib/error.scm 650 */
 obj_t BgL_arg1565z00_1933;
{ /* Llib/error.scm 650 */
 obj_t BgL_arg1567z00_1934;
{ /* Llib/error.scm 650 */
 obj_t BgL_classz00_3927;
BgL_classz00_3927 = BGl_z62typezd2errorzb0zz__objectz00; 
BgL_arg1567z00_1934 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3927); } 
BgL_arg1565z00_1933 = 
VECTOR_REF(BgL_arg1567z00_1934,2L); } 
BgL_auxz00_6408 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1565z00_1933); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1087z00_1932)))->BgL_stackz00)=((obj_t)BgL_auxz00_6408),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1087z00_1932)))->BgL_procz00)=((obj_t)BgL_procz00_59),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1087z00_1932)))->BgL_msgz00)=((obj_t)BgL_msgz00_1931),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1087z00_1932)))->BgL_objz00)=((obj_t)BUNSPEC),BUNSPEC); 
((((BgL_z62typezd2errorzb0_bglt)COBJECT(BgL_new1087z00_1932))->BgL_typez00)=((obj_t)BgL_typez00_60),BUNSPEC); 
return BgL_new1087z00_1932;} } } } } 

}



/* index-out-of-bounds-error */
BGL_EXPORTED_DEF obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t BgL_fnamez00_62, obj_t BgL_locz00_63, obj_t BgL_procz00_64, obj_t BgL_objz00_65, int BgL_lenz00_66, int BgL_iz00_67)
{
{ /* Llib/error.scm 659 */
{ /* Llib/error.scm 660 */
 obj_t BgL_msgz00_1940;
{ /* Llib/error.scm 665 */
 obj_t BgL_arg1579z00_1946; obj_t BgL_arg1580z00_1947;
{ /* Ieee/fixnum.scm 1001 */

BgL_arg1579z00_1946 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(long)(BgL_iz00_67), 10L); } 
{ /* Ieee/fixnum.scm 1001 */

BgL_arg1580z00_1947 = 
BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
(
(long)(BgL_lenz00_66)-1L), 10L); } 
{ /* Llib/error.scm 664 */
 obj_t BgL_list1581z00_1948;
{ /* Llib/error.scm 664 */
 obj_t BgL_arg1582z00_1949;
{ /* Llib/error.scm 664 */
 obj_t BgL_arg1583z00_1950;
{ /* Llib/error.scm 664 */
 obj_t BgL_arg1584z00_1951;
{ /* Llib/error.scm 664 */
 obj_t BgL_arg1585z00_1952;
BgL_arg1585z00_1952 = 
MAKE_YOUNG_PAIR(BGl_string2854z00zz__errorz00, BNIL); 
BgL_arg1584z00_1951 = 
MAKE_YOUNG_PAIR(BgL_arg1580z00_1947, BgL_arg1585z00_1952); } 
BgL_arg1583z00_1950 = 
MAKE_YOUNG_PAIR(BGl_string2855z00zz__errorz00, BgL_arg1584z00_1951); } 
BgL_arg1582z00_1949 = 
MAKE_YOUNG_PAIR(BgL_arg1579z00_1946, BgL_arg1583z00_1950); } 
BgL_list1581z00_1948 = 
MAKE_YOUNG_PAIR(BGl_string2856z00zz__errorz00, BgL_arg1582z00_1949); } 
BgL_msgz00_1940 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1581z00_1948); } } 
{ /* Llib/error.scm 664 */

{ /* Llib/error.scm 668 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1089z00_1941;
{ /* Llib/error.scm 669 */
 BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt BgL_new1088z00_1944;
BgL_new1088z00_1944 = 
((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bgl) ))); 
{ /* Llib/error.scm 669 */
 long BgL_arg1578z00_1945;
BgL_arg1578z00_1945 = 
BGL_CLASS_NUM(BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1088z00_1944), BgL_arg1578z00_1945); } 
BgL_new1089z00_1941 = BgL_new1088z00_1944; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1089z00_1941)))->BgL_fnamez00)=((obj_t)BgL_fnamez00_62),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1089z00_1941)))->BgL_locationz00)=((obj_t)BgL_locz00_63),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6440;
{ /* Llib/error.scm 670 */
 obj_t BgL_arg1575z00_1942;
{ /* Llib/error.scm 670 */
 obj_t BgL_arg1576z00_1943;
{ /* Llib/error.scm 670 */
 obj_t BgL_classz00_3933;
BgL_classz00_3933 = BGl_z62indexzd2outzd2ofzd2boundszd2errorz62zz__objectz00; 
BgL_arg1576z00_1943 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3933); } 
BgL_arg1575z00_1942 = 
VECTOR_REF(BgL_arg1576z00_1943,2L); } 
BgL_auxz00_6440 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1575z00_1942); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1089z00_1941)))->BgL_stackz00)=((obj_t)BgL_auxz00_6440),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1089z00_1941)))->BgL_procz00)=((obj_t)BgL_procz00_64),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1089z00_1941)))->BgL_msgz00)=((obj_t)BgL_msgz00_1940),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_new1089z00_1941)))->BgL_objz00)=((obj_t)BgL_objz00_65),BUNSPEC); 
((((BgL_z62indexzd2outzd2ofzd2boundszd2errorz62_bglt)COBJECT(BgL_new1089z00_1941))->BgL_indexz00)=((obj_t)
BINT(BgL_lenz00_66)),BUNSPEC); 
return 
((obj_t)BgL_new1089z00_1941);} } } } 

}



/* &index-out-of-bounds2770 */
obj_t BGl_z62indexzd2outzd2ofzd2bounds2770zb0zz__errorz00(obj_t BgL_envz00_5176, obj_t BgL_fnamez00_5177, obj_t BgL_locz00_5178, obj_t BgL_procz00_5179, obj_t BgL_objz00_5180, obj_t BgL_lenz00_5181, obj_t BgL_iz00_5182)
{
{ /* Llib/error.scm 659 */
{ /* Llib/error.scm 660 */
 int BgL_auxz00_6464; int BgL_auxz00_6455;
{ /* Llib/error.scm 660 */
 obj_t BgL_tmpz00_6465;
if(
INTEGERP(BgL_iz00_5182))
{ /* Llib/error.scm 660 */
BgL_tmpz00_6465 = BgL_iz00_5182
; }  else 
{ 
 obj_t BgL_auxz00_6468;
BgL_auxz00_6468 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(26845L), BGl_string2857z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_iz00_5182); 
FAILURE(BgL_auxz00_6468,BFALSE,BFALSE);} 
BgL_auxz00_6464 = 
CINT(BgL_tmpz00_6465); } 
{ /* Llib/error.scm 660 */
 obj_t BgL_tmpz00_6456;
if(
INTEGERP(BgL_lenz00_5181))
{ /* Llib/error.scm 660 */
BgL_tmpz00_6456 = BgL_lenz00_5181
; }  else 
{ 
 obj_t BgL_auxz00_6459;
BgL_auxz00_6459 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(26845L), BGl_string2857z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_lenz00_5181); 
FAILURE(BgL_auxz00_6459,BFALSE,BFALSE);} 
BgL_auxz00_6455 = 
CINT(BgL_tmpz00_6456); } 
return 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BgL_fnamez00_5177, BgL_locz00_5178, BgL_procz00_5179, BgL_objz00_5180, BgL_auxz00_6455, BgL_auxz00_6464);} } 

}



/* warning */
BGL_EXPORTED_DEF obj_t BGl_warningz00zz__errorz00(obj_t BgL_argsz00_68)
{
{ /* Llib/error.scm 679 */
{ /* Llib/error.scm 681 */
 BgL_z62warningz62_bglt BgL_arg1591z00_1963;
{ /* Llib/error.scm 681 */
 BgL_z62warningz62_bglt BgL_new1091z00_1964;
{ /* Llib/error.scm 681 */
 BgL_z62warningz62_bglt BgL_new1090z00_1967;
BgL_new1090z00_1967 = 
((BgL_z62warningz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62warningz62_bgl) ))); 
{ /* Llib/error.scm 681 */
 long BgL_arg1595z00_1968;
BgL_arg1595z00_1968 = 
BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1090z00_1967), BgL_arg1595z00_1968); } 
BgL_new1091z00_1964 = BgL_new1090z00_1967; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1091z00_1964)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1091z00_1964)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6482;
{ /* Llib/error.scm 681 */
 obj_t BgL_arg1593z00_1965;
{ /* Llib/error.scm 681 */
 obj_t BgL_arg1594z00_1966;
{ /* Llib/error.scm 681 */
 obj_t BgL_classz00_3938;
BgL_classz00_3938 = BGl_z62warningz62zz__objectz00; 
BgL_arg1594z00_1966 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3938); } 
BgL_arg1593z00_1965 = 
VECTOR_REF(BgL_arg1594z00_1966,2L); } 
BgL_auxz00_6482 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1593z00_1965); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1091z00_1964)))->BgL_stackz00)=((obj_t)BgL_auxz00_6482),BUNSPEC); } 
((((BgL_z62warningz62_bglt)COBJECT(BgL_new1091z00_1964))->BgL_argsz00)=((obj_t)BgL_argsz00_68),BUNSPEC); 
BgL_arg1591z00_1963 = BgL_new1091z00_1964; } 
BGL_TAIL return 
BGl_warningzd2notifyzd2zz__errorz00(
((obj_t)BgL_arg1591z00_1963));} } 

}



/* &warning2771 */
obj_t BGl_z62warning2771z62zz__errorz00(obj_t BgL_envz00_5183, obj_t BgL_argsz00_5184)
{
{ /* Llib/error.scm 679 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_5184);} 

}



/* warning/location */
BGL_EXPORTED_DEF obj_t BGl_warningzf2locationzf2zz__errorz00(obj_t BgL_fnamez00_69, obj_t BgL_locz00_70, obj_t BgL_argsz00_71)
{
{ /* Llib/error.scm 686 */
{ /* Llib/error.scm 688 */
 BgL_z62warningz62_bglt BgL_arg1598z00_1969;
{ /* Llib/error.scm 688 */
 BgL_z62warningz62_bglt BgL_new1093z00_1970;
{ /* Llib/error.scm 688 */
 BgL_z62warningz62_bglt BgL_new1092z00_1973;
BgL_new1092z00_1973 = 
((BgL_z62warningz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62warningz62_bgl) ))); 
{ /* Llib/error.scm 688 */
 long BgL_arg1603z00_1974;
BgL_arg1603z00_1974 = 
BGL_CLASS_NUM(BGl_z62warningz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1092z00_1973), BgL_arg1603z00_1974); } 
BgL_new1093z00_1970 = BgL_new1092z00_1973; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1093z00_1970)))->BgL_fnamez00)=((obj_t)BgL_fnamez00_69),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1093z00_1970)))->BgL_locationz00)=((obj_t)BgL_locz00_70),BUNSPEC); 
{ 
 obj_t BgL_auxz00_6500;
{ /* Llib/error.scm 688 */
 obj_t BgL_arg1601z00_1971;
{ /* Llib/error.scm 688 */
 obj_t BgL_arg1602z00_1972;
{ /* Llib/error.scm 688 */
 obj_t BgL_classz00_3943;
BgL_classz00_3943 = BGl_z62warningz62zz__objectz00; 
BgL_arg1602z00_1972 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_3943); } 
BgL_arg1601z00_1971 = 
VECTOR_REF(BgL_arg1602z00_1972,2L); } 
BgL_auxz00_6500 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1601z00_1971); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1093z00_1970)))->BgL_stackz00)=((obj_t)BgL_auxz00_6500),BUNSPEC); } 
((((BgL_z62warningz62_bglt)COBJECT(BgL_new1093z00_1970))->BgL_argsz00)=((obj_t)BgL_argsz00_71),BUNSPEC); 
BgL_arg1598z00_1969 = BgL_new1093z00_1970; } 
return 
BGl_warningzd2notifyzd2zz__errorz00(
((obj_t)BgL_arg1598z00_1969));} } 

}



/* &warning/location */
obj_t BGl_z62warningzf2locationz90zz__errorz00(obj_t BgL_envz00_5185, obj_t BgL_fnamez00_5186, obj_t BgL_locz00_5187, obj_t BgL_argsz00_5188)
{
{ /* Llib/error.scm 686 */
return 
BGl_warningzf2locationzf2zz__errorz00(BgL_fnamez00_5186, BgL_locz00_5187, BgL_argsz00_5188);} 

}



/* warning/loc */
BGL_EXPORTED_DEF obj_t BGl_warningzf2loczf2zz__errorz00(obj_t BgL_locz00_72, obj_t BgL_argsz00_73)
{
{ /* Llib/error.scm 693 */
if(
PAIRP(BgL_locz00_72))
{ /* Llib/error.scm 694 */
 obj_t BgL_cdrzd2204zd2_1981;
BgL_cdrzd2204zd2_1981 = 
CDR(
((obj_t)BgL_locz00_72)); 
if(
(
CAR(
((obj_t)BgL_locz00_72))==BGl_symbol2823z00zz__errorz00))
{ /* Llib/error.scm 694 */
if(
PAIRP(BgL_cdrzd2204zd2_1981))
{ /* Llib/error.scm 694 */
 obj_t BgL_cdrzd2208zd2_1985;
BgL_cdrzd2208zd2_1985 = 
CDR(BgL_cdrzd2204zd2_1981); 
if(
PAIRP(BgL_cdrzd2208zd2_1985))
{ /* Llib/error.scm 694 */
if(
NULLP(
CDR(BgL_cdrzd2208zd2_1985)))
{ /* Llib/error.scm 694 */
 obj_t BgL_arg1611z00_1989; obj_t BgL_arg1612z00_1990;
BgL_arg1611z00_1989 = 
CAR(BgL_cdrzd2204zd2_1981); 
BgL_arg1612z00_1990 = 
CAR(BgL_cdrzd2208zd2_1985); 
{ /* Llib/error.scm 696 */
 obj_t BgL_list1616z00_3955;
{ /* Llib/error.scm 696 */
 obj_t BgL_arg1617z00_3956;
BgL_arg1617z00_3956 = 
MAKE_YOUNG_PAIR(BgL_argsz00_73, BNIL); 
BgL_list1616z00_3955 = 
MAKE_YOUNG_PAIR(BgL_arg1612z00_1990, BgL_arg1617z00_3956); } 
return 
BGl_applyz00zz__r4_control_features_6_9z00(BGl_warningzf2locationzd2envz20zz__errorz00, BgL_arg1611z00_1989, BgL_list1616z00_3955);} }  else 
{ /* Llib/error.scm 694 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_73);} }  else 
{ /* Llib/error.scm 694 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_73);} }  else 
{ /* Llib/error.scm 694 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_73);} }  else 
{ /* Llib/error.scm 694 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_73);} }  else 
{ /* Llib/error.scm 694 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_73);} } 

}



/* &warning/loc */
obj_t BGl_z62warningzf2locz90zz__errorz00(obj_t BgL_envz00_5189, obj_t BgL_locz00_5190, obj_t BgL_argsz00_5191)
{
{ /* Llib/error.scm 693 */
return 
BGl_warningzf2loczf2zz__errorz00(BgL_locz00_5190, BgL_argsz00_5191);} 

}



/* warning/c-location */
BGL_EXPORTED_DEF obj_t BGl_warningzf2czd2locationz20zz__errorz00(char * BgL_fnamez00_74, long BgL_locz00_75, obj_t BgL_argsz00_76)
{
{ /* Llib/error.scm 707 */
{ /* Llib/error.scm 708 */
 obj_t BgL_list1619z00_3962;
{ /* Llib/error.scm 708 */
 obj_t BgL_arg1620z00_3963;
BgL_arg1620z00_3963 = 
MAKE_YOUNG_PAIR(BgL_argsz00_76, BNIL); 
BgL_list1619z00_3962 = 
MAKE_YOUNG_PAIR(
BINT(BgL_locz00_75), BgL_arg1620z00_3963); } 
return 
BGl_applyz00zz__r4_control_features_6_9z00(BGl_warningzf2locationzd2envz20zz__errorz00, 
string_to_bstring(BgL_fnamez00_74), BgL_list1619z00_3962);} } 

}



/* &warning/c-location */
obj_t BGl_z62warningzf2czd2locationz42zz__errorz00(obj_t BgL_envz00_5192, obj_t BgL_fnamez00_5193, obj_t BgL_locz00_5194, obj_t BgL_argsz00_5195)
{
{ /* Llib/error.scm 707 */
{ /* Llib/error.scm 708 */
 long BgL_auxz00_6551; char * BgL_auxz00_6542;
{ /* Llib/error.scm 708 */
 obj_t BgL_tmpz00_6552;
if(
INTEGERP(BgL_locz00_5194))
{ /* Llib/error.scm 708 */
BgL_tmpz00_6552 = BgL_locz00_5194
; }  else 
{ 
 obj_t BgL_auxz00_6555;
BgL_auxz00_6555 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(28832L), BGl_string2858z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_locz00_5194); 
FAILURE(BgL_auxz00_6555,BFALSE,BFALSE);} 
BgL_auxz00_6551 = 
(long)CINT(BgL_tmpz00_6552); } 
{ /* Llib/error.scm 708 */
 obj_t BgL_tmpz00_6543;
if(
STRINGP(BgL_fnamez00_5193))
{ /* Llib/error.scm 708 */
BgL_tmpz00_6543 = BgL_fnamez00_5193
; }  else 
{ 
 obj_t BgL_auxz00_6546;
BgL_auxz00_6546 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(28832L), BGl_string2858z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_fnamez00_5193); 
FAILURE(BgL_auxz00_6546,BFALSE,BFALSE);} 
BgL_auxz00_6542 = 
BSTRING_TO_STRING(BgL_tmpz00_6543); } 
return 
BGl_warningzf2czd2locationz20zz__errorz00(BgL_auxz00_6542, BgL_auxz00_6551, BgL_argsz00_5195);} } 

}



/* notify-&error */
obj_t BGl_notifyzd2z62errorzb0zz__errorz00(obj_t BgL_errz00_77)
{
{ /* Llib/error.scm 713 */
{ /* Llib/error.scm 714 */
 obj_t BgL_portz00_1998;
{ /* Llib/error.scm 714 */
 obj_t BgL_tmpz00_6561;
BgL_tmpz00_6561 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_1998 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6561); } 
bgl_flush_output_port(BgL_portz00_1998); 
{ /* Llib/error.scm 717 */
 obj_t BgL_list1621z00_2000;
BgL_list1621z00_2000 = 
MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2844z00zz__errorz00, BgL_list1621z00_2000); } 
{ /* Llib/error.scm 718 */
 obj_t BgL_arg1622z00_2001;
BgL_arg1622z00_2001 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_77)))->BgL_procz00); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1622z00_2001, BgL_portz00_1998); } 
{ /* Llib/error.scm 719 */
 obj_t BgL_list1623z00_2002;
BgL_list1623z00_2002 = 
MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2859z00zz__errorz00, BgL_list1623z00_2002); } 
{ /* Llib/error.scm 720 */
 obj_t BgL_arg1624z00_2003;
BgL_arg1624z00_2003 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_77)))->BgL_msgz00); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1624z00_2003, BgL_portz00_1998); } 
if(
(
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_77)))->BgL_objz00)==BGl_symbol2860z00zz__errorz00))
{ /* Llib/error.scm 721 */BFALSE; }  else 
{ /* Llib/error.scm 721 */
{ /* Llib/error.scm 722 */
 obj_t BgL_list1627z00_2006;
BgL_list1627z00_2006 = 
MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2862z00zz__errorz00, BgL_list1627z00_2006); } 
{ /* Llib/error.scm 723 */
 obj_t BgL_arg1628z00_2007;
BgL_arg1628z00_2007 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_77)))->BgL_objz00); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1628z00_2007, BgL_portz00_1998); } } 
{ /* Llib/error.scm 724 */
 obj_t BgL_list1630z00_2009;
BgL_list1630z00_2009 = 
MAKE_YOUNG_PAIR(BgL_portz00_1998, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1630z00_2009); } 
{ /* Llib/error.scm 725 */
 obj_t BgL_arg1631z00_2010;
{ /* Llib/error.scm 725 */
 obj_t BgL__ortest_1095z00_2014;
BgL__ortest_1095z00_2014 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62errorz62_bglt)BgL_errz00_77))))->BgL_stackz00); 
if(
CBOOL(BgL__ortest_1095z00_2014))
{ /* Llib/error.scm 725 */
BgL_arg1631z00_2010 = BgL__ortest_1095z00_2014; }  else 
{ /* Llib/error.scm 725 */

{ /* Llib/error.scm 725 */

BgL_arg1631z00_2010 = 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE); } } } 
{ /* Llib/error.scm 273 */

BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_arg1631z00_2010, BgL_portz00_1998, 
BINT(1L)); } } 
return 
bgl_flush_output_port(BgL_portz00_1998);} } 

}



/* notify-&error/location-no-loc */
obj_t BGl_notifyzd2z62errorzf2locationzd2nozd2locz42zz__errorz00(obj_t BgL_errz00_78)
{
{ /* Llib/error.scm 731 */
{ /* Llib/error.scm 732 */
 obj_t BgL_portz00_2016;
{ /* Llib/error.scm 732 */
 obj_t BgL_tmpz00_6595;
BgL_tmpz00_6595 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2016 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6595); } 
bgl_flush_output_port(BgL_portz00_2016); 
{ /* Llib/error.scm 735 */
 obj_t BgL_list1632z00_2018;
BgL_list1632z00_2018 = 
MAKE_YOUNG_PAIR(BgL_portz00_2016, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1632z00_2018); } 
{ /* Llib/error.scm 736 */
 obj_t BgL_arg1634z00_2019; obj_t BgL_arg1636z00_2020;
BgL_arg1634z00_2019 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62errorz62_bglt)BgL_errz00_78))))->BgL_fnamez00); 
BgL_arg1636z00_2020 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62errorz62_bglt)BgL_errz00_78))))->BgL_locationz00); 
{ /* Llib/error.scm 736 */
 obj_t BgL_list1637z00_2021;
{ /* Llib/error.scm 736 */
 obj_t BgL_arg1638z00_2022;
{ /* Llib/error.scm 736 */
 obj_t BgL_arg1639z00_2023;
{ /* Llib/error.scm 736 */
 obj_t BgL_arg1640z00_2024;
{ /* Llib/error.scm 736 */
 obj_t BgL_arg1641z00_2025;
BgL_arg1641z00_2025 = 
MAKE_YOUNG_PAIR(
BCHAR(((unsigned char)':')), BNIL); 
BgL_arg1640z00_2024 = 
MAKE_YOUNG_PAIR(BgL_arg1636z00_2020, BgL_arg1641z00_2025); } 
BgL_arg1639z00_2023 = 
MAKE_YOUNG_PAIR(BGl_string2863z00zz__errorz00, BgL_arg1640z00_2024); } 
BgL_arg1638z00_2022 = 
MAKE_YOUNG_PAIR(BgL_arg1634z00_2019, BgL_arg1639z00_2023); } 
BgL_list1637z00_2021 = 
MAKE_YOUNG_PAIR(BGl_string2864z00zz__errorz00, BgL_arg1638z00_2022); } 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_portz00_2016, BgL_list1637z00_2021); } } 
BGL_TAIL return 
BGl_notifyzd2z62errorzb0zz__errorz00(BgL_errz00_78);} } 

}



/* notify-&error/location-loc */
obj_t BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00(obj_t BgL_errz00_79, obj_t BgL_fnamez00_80, obj_t BgL_linez00_81, obj_t BgL_locz00_82, obj_t BgL_stringz00_83, obj_t BgL_colz00_84)
{
{ /* Llib/error.scm 742 */
BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00:
{ /* Llib/error.scm 743 */
 long BgL_lenz00_2026;
BgL_lenz00_2026 = 
STRING_LENGTH(
((obj_t)BgL_stringz00_83)); 
if(
(BgL_lenz00_2026>256L))
{ /* Llib/error.scm 745 */
 obj_t BgL_nstringz00_2029;
{ /* Llib/error.scm 746 */
 long BgL_arg1649z00_2035; long BgL_arg1650z00_2036;
{ /* Llib/error.scm 746 */
 long BgL_arg1651z00_2037;
BgL_arg1651z00_2037 = 
(
(long)CINT(BgL_colz00_84)-60L); 
{ /* Llib/error.scm 746 */
 obj_t BgL_list1652z00_2038;
BgL_list1652z00_2038 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1651z00_2037), BNIL); 
BgL_arg1649z00_2035 = 
BGl_maxfxz00zz__r4_numbers_6_5_fixnumz00(0L, BgL_list1652z00_2038); } } 
BgL_arg1650z00_2036 = 
(
(long)CINT(BgL_colz00_84)+10L); 
BgL_nstringz00_2029 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_83, BgL_arg1649z00_2035, BgL_arg1650z00_2036); } 
{ /* Llib/error.scm 746 */

{ /* Llib/error.scm 748 */
 obj_t BgL_arg1643z00_2030; long BgL_arg1644z00_2031;
{ /* Llib/error.scm 748 */
 obj_t BgL_list1645z00_2032;
{ /* Llib/error.scm 748 */
 obj_t BgL_arg1646z00_2033;
{ /* Llib/error.scm 748 */
 obj_t BgL_arg1648z00_2034;
BgL_arg1648z00_2034 = 
MAKE_YOUNG_PAIR(BGl_string2865z00zz__errorz00, BNIL); 
BgL_arg1646z00_2033 = 
MAKE_YOUNG_PAIR(BgL_nstringz00_2029, BgL_arg1648z00_2034); } 
BgL_list1645z00_2032 = 
MAKE_YOUNG_PAIR(BGl_string2865z00zz__errorz00, BgL_arg1646z00_2033); } 
BgL_arg1643z00_2030 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1645z00_2032); } 
BgL_arg1644z00_2031 = 
(60L+3L); 
{ 
 obj_t BgL_colz00_6633; obj_t BgL_stringz00_6632;
BgL_stringz00_6632 = BgL_arg1643z00_2030; 
BgL_colz00_6633 = 
BINT(BgL_arg1644z00_2031); 
BgL_colz00_84 = BgL_colz00_6633; 
BgL_stringz00_83 = BgL_stringz00_6632; 
goto BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00;} } } }  else 
{ /* Llib/error.scm 751 */
 obj_t BgL_portz00_2040;
{ /* Llib/error.scm 751 */
 obj_t BgL_tmpz00_6635;
BgL_tmpz00_6635 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2040 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6635); } 
bgl_flush_output_port(BgL_portz00_2040); 
{ /* Llib/error.scm 754 */
 obj_t BgL_list1653z00_2041;
BgL_list1653z00_2041 = 
MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1653z00_2041); } 
{ /* Llib/error.scm 755 */
 obj_t BgL_spacezd2stringzd2_2042;
if(
(
(long)CINT(BgL_colz00_84)>0L))
{ /* Llib/error.scm 755 */
BgL_spacezd2stringzd2_2042 = 
make_string(
(long)CINT(BgL_colz00_84), ((unsigned char)' ')); }  else 
{ /* Llib/error.scm 755 */
BgL_spacezd2stringzd2_2042 = BGl_string2866z00zz__errorz00; } 
{ /* Llib/error.scm 755 */
 long BgL_lz00_2043;
BgL_lz00_2043 = 
STRING_LENGTH(
((obj_t)BgL_stringz00_83)); 
{ /* Llib/error.scm 756 */
 obj_t BgL_nzd2colzd2_2044;
if(
(
(long)CINT(BgL_colz00_84)>=BgL_lz00_2043))
{ /* Llib/error.scm 757 */
BgL_nzd2colzd2_2044 = 
BINT(BgL_lz00_2043); }  else 
{ /* Llib/error.scm 757 */
BgL_nzd2colzd2_2044 = BgL_colz00_84; } 
{ /* Llib/error.scm 757 */

BGl_fixzd2tabulationz12zc0zz__errorz00(BgL_nzd2colzd2_2044, BgL_stringz00_83, BgL_spacezd2stringzd2_2042); 
BGl_printzd2cursorzd2zz__errorz00(BgL_fnamez00_80, BgL_linez00_81, BgL_locz00_82, BgL_stringz00_83, BgL_spacezd2stringzd2_2042); 
{ /* Llib/error.scm 763 */
 obj_t BgL_list1654z00_2045;
BgL_list1654z00_2045 = 
MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2844z00zz__errorz00, BgL_list1654z00_2045); } 
{ /* Llib/error.scm 764 */
 obj_t BgL_arg1656z00_2046;
BgL_arg1656z00_2046 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_79)))->BgL_procz00); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1656z00_2046, BgL_portz00_2040); } 
{ /* Llib/error.scm 765 */
 obj_t BgL_list1657z00_2047;
BgL_list1657z00_2047 = 
MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1657z00_2047); } 
{ /* Llib/error.scm 766 */
 obj_t BgL_arg1658z00_2048;
BgL_arg1658z00_2048 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_79)))->BgL_msgz00); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1658z00_2048, BgL_portz00_2040); } 
if(
(
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_79)))->BgL_objz00)==BGl_symbol2860z00zz__errorz00))
{ /* Llib/error.scm 767 */BFALSE; }  else 
{ /* Llib/error.scm 767 */
{ /* Llib/error.scm 768 */
 obj_t BgL_list1663z00_2051;
BgL_list1663z00_2051 = 
MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2862z00zz__errorz00, BgL_list1663z00_2051); } 
{ /* Llib/error.scm 769 */
 obj_t BgL_arg1664z00_2052;
BgL_arg1664z00_2052 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_errz00_79)))->BgL_objz00); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1664z00_2052, BgL_portz00_2040); } } 
{ /* Llib/error.scm 770 */
 obj_t BgL_list1668z00_2054;
BgL_list1668z00_2054 = 
MAKE_YOUNG_PAIR(BgL_portz00_2040, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1668z00_2054); } 
{ /* Llib/error.scm 771 */
 obj_t BgL_arg1669z00_2055;
{ /* Llib/error.scm 771 */
 obj_t BgL__ortest_1098z00_2059;
BgL__ortest_1098z00_2059 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62errorz62_bglt)BgL_errz00_79))))->BgL_stackz00); 
if(
CBOOL(BgL__ortest_1098z00_2059))
{ /* Llib/error.scm 771 */
BgL_arg1669z00_2055 = BgL__ortest_1098z00_2059; }  else 
{ /* Llib/error.scm 771 */

{ /* Llib/error.scm 771 */

BgL_arg1669z00_2055 = 
BGl_getzd2tracezd2stackz00zz__errorz00(BFALSE); } } } 
{ /* Llib/error.scm 273 */

BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_arg1669z00_2055, BgL_portz00_2040, 
BINT(1L)); } } 
return 
bgl_flush_output_port(BgL_portz00_2040);} } } } } } } 

}



/* notify-&error/loc */
obj_t BGl_notifyzd2z62errorzf2locz42zz__errorz00(obj_t BgL_errz00_85, obj_t BgL_fnamez00_86, obj_t BgL_locz00_87)
{
{ /* Llib/error.scm 778 */
{ /* Llib/error.scm 779 */
 bool_t BgL_test3249z00_6684;
if(
STRINGP(BgL_fnamez00_86))
{ /* Llib/error.scm 779 */
if(
INTEGERP(BgL_locz00_87))
{ /* Llib/error.scm 779 */
BgL_test3249z00_6684 = ((bool_t)0)
; }  else 
{ /* Llib/error.scm 779 */
BgL_test3249z00_6684 = ((bool_t)1)
; } }  else 
{ /* Llib/error.scm 779 */
BgL_test3249z00_6684 = ((bool_t)1)
; } 
if(BgL_test3249z00_6684)
{ /* Llib/error.scm 779 */
return 
BGl_notifyzd2z62errorzb0zz__errorz00(BgL_errz00_85);}  else 
{ /* Llib/error.scm 781 */
 obj_t BgL_filez00_2066;
{ /* Llib/error.scm 782 */
 obj_t BgL_arg1675z00_2070;
{ /* Llib/error.scm 782 */
 obj_t BgL_list1676z00_2071;
{ /* Llib/error.scm 782 */
 obj_t BgL_arg1678z00_2072;
{ /* Llib/error.scm 782 */
 obj_t BgL_arg1681z00_2073;
BgL_arg1681z00_2073 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
BgL_arg1678z00_2072 = 
MAKE_YOUNG_PAIR(BgL_locz00_87, BgL_arg1681z00_2073); } 
BgL_list1676z00_2071 = 
MAKE_YOUNG_PAIR(BgL_fnamez00_86, BgL_arg1678z00_2072); } 
BgL_arg1675z00_2070 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BGl_symbol2823z00zz__errorz00, BgL_list1676z00_2071); } 
BgL_filez00_2066 = 
BGl_locationzd2linezd2numz00zz__errorz00(BgL_arg1675z00_2070); } 
{ /* Llib/error.scm 782 */
 obj_t BgL_lnumz00_2067; obj_t BgL_lpointz00_2068; obj_t BgL_lstringz00_2069;
{ /* Llib/error.scm 783 */
 obj_t BgL_tmpz00_3985;
{ /* Llib/error.scm 783 */
 int BgL_tmpz00_6695;
BgL_tmpz00_6695 = 
(int)(1L); 
BgL_tmpz00_3985 = 
BGL_MVALUES_VAL(BgL_tmpz00_6695); } 
{ /* Llib/error.scm 783 */
 int BgL_tmpz00_6698;
BgL_tmpz00_6698 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6698, BUNSPEC); } 
BgL_lnumz00_2067 = BgL_tmpz00_3985; } 
{ /* Llib/error.scm 783 */
 obj_t BgL_tmpz00_3986;
{ /* Llib/error.scm 783 */
 int BgL_tmpz00_6701;
BgL_tmpz00_6701 = 
(int)(2L); 
BgL_tmpz00_3986 = 
BGL_MVALUES_VAL(BgL_tmpz00_6701); } 
{ /* Llib/error.scm 783 */
 int BgL_tmpz00_6704;
BgL_tmpz00_6704 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6704, BUNSPEC); } 
BgL_lpointz00_2068 = BgL_tmpz00_3986; } 
{ /* Llib/error.scm 783 */
 obj_t BgL_tmpz00_3987;
{ /* Llib/error.scm 783 */
 int BgL_tmpz00_6707;
BgL_tmpz00_6707 = 
(int)(3L); 
BgL_tmpz00_3987 = 
BGL_MVALUES_VAL(BgL_tmpz00_6707); } 
{ /* Llib/error.scm 783 */
 int BgL_tmpz00_6710;
BgL_tmpz00_6710 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_6710, BUNSPEC); } 
BgL_lstringz00_2069 = BgL_tmpz00_3987; } 
if(
CBOOL(BgL_lnumz00_2067))
{ /* Llib/error.scm 783 */
return 
BGl_notifyzd2z62errorzf2locationzd2locz90zz__errorz00(BgL_errz00_85, BgL_fnamez00_86, BgL_lnumz00_2067, BgL_locz00_87, BgL_lstringz00_2069, BgL_lpointz00_2068);}  else 
{ /* Llib/error.scm 783 */
return 
BGl_notifyzd2z62errorzf2locationzd2nozd2locz42zz__errorz00(BgL_errz00_85);} } } } } 

}



/* open-for-error */
obj_t BGl_openzd2forzd2errorz00zz__errorz00(obj_t BgL_fnamez00_88)
{
{ /* Llib/error.scm 790 */
if(
fexists(
BSTRING_TO_STRING(BgL_fnamez00_88)))
{ /* Llib/error.scm 792 */
if(
bgl_directoryp(
BSTRING_TO_STRING(BgL_fnamez00_88)))
{ /* Llib/error.scm 793 */
return BFALSE;}  else 
{ /* Llib/error.scm 794 */
 obj_t BgL_env1107z00_2079;
BgL_env1107z00_2079 = 
BGL_CURRENT_DYNAMIC_ENV(); 
{ /* Llib/error.scm 794 */
 obj_t BgL_cell1102z00_2080;
BgL_cell1102z00_2080 = 
MAKE_STACK_CELL(BUNSPEC); 
{ /* Llib/error.scm 794 */
 obj_t BgL_val1106z00_2081;
BgL_val1106z00_2081 = 
BGl_zc3z04exitza31686ze3ze70z60zz__errorz00(BgL_fnamez00_88, BgL_cell1102z00_2080, BgL_env1107z00_2079); 
if(
(BgL_val1106z00_2081==BgL_cell1102z00_2080))
{ /* Llib/error.scm 794 */
{ /* Llib/error.scm 794 */
 int BgL_tmpz00_6728;
BgL_tmpz00_6728 = 
(int)(0L); 
BGL_SIGSETMASK(BgL_tmpz00_6728); } 
{ /* Llib/error.scm 794 */
 obj_t BgL_arg1685z00_2083;
BgL_arg1685z00_2083 = 
CELL_REF(
((obj_t)BgL_val1106z00_2081)); 
return BFALSE;} }  else 
{ /* Llib/error.scm 794 */
return BgL_val1106z00_2081;} } } } }  else 
{ /* Llib/error.scm 797 */
 bool_t BgL_test3256z00_6733;
{ /* Llib/error.scm 797 */
 long BgL_l1z00_3998;
BgL_l1z00_3998 = 
STRING_LENGTH(
((obj_t)BgL_fnamez00_88)); 
if(
(BgL_l1z00_3998==5L))
{ /* Llib/error.scm 797 */
 int BgL_arg2321z00_4001;
{ /* Llib/error.scm 797 */
 char * BgL_auxz00_6741; char * BgL_tmpz00_6738;
BgL_auxz00_6741 = 
BSTRING_TO_STRING(BGl_string2867z00zz__errorz00); 
BgL_tmpz00_6738 = 
BSTRING_TO_STRING(
((obj_t)BgL_fnamez00_88)); 
BgL_arg2321z00_4001 = 
memcmp(BgL_tmpz00_6738, BgL_auxz00_6741, BgL_l1z00_3998); } 
BgL_test3256z00_6733 = 
(
(long)(BgL_arg2321z00_4001)==0L); }  else 
{ /* Llib/error.scm 797 */
BgL_test3256z00_6733 = ((bool_t)0)
; } } 
if(BgL_test3256z00_6733)
{ /* Llib/error.scm 798 */
 obj_t BgL_arg1691z00_2101;
{ /* Llib/error.scm 798 */
 obj_t BgL_arg1692z00_2105;
{ /* Llib/error.scm 798 */
 obj_t BgL_tmpz00_6746;
BgL_tmpz00_6746 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1692z00_2105 = 
BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_6746); } 
BgL_arg1691z00_2101 = 
BGL_INPUT_PORT_BUFFER(BgL_arg1692z00_2105); } 
{ /* Ieee/port.scm 469 */
 long BgL_endz00_2104;
BgL_endz00_2104 = 
STRING_LENGTH(BgL_arg1691z00_2101); 
{ /* Ieee/port.scm 469 */

return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_arg1691z00_2101, 
BINT(0L), 
BINT(BgL_endz00_2104));} } }  else 
{ /* Llib/error.scm 799 */
 bool_t BgL_test3258z00_6754;
{ /* Llib/error.scm 799 */

BgL_test3258z00_6754 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BGl_string2868z00zz__errorz00, BgL_fnamez00_88, BFALSE, BFALSE, BFALSE, BFALSE); } 
if(BgL_test3258z00_6754)
{ /* Llib/error.scm 800 */
 obj_t BgL_arg1699z00_2113;
{ /* Ieee/string.scm 194 */
 long BgL_endz00_2119;
BgL_endz00_2119 = 
STRING_LENGTH(
((obj_t)BgL_fnamez00_88)); 
{ /* Ieee/string.scm 194 */

BgL_arg1699z00_2113 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_fnamez00_88, 9L, BgL_endz00_2119); } } 
{ /* Ieee/port.scm 469 */
 long BgL_endz00_2116;
BgL_endz00_2116 = 
STRING_LENGTH(BgL_arg1699z00_2113); 
{ /* Ieee/port.scm 469 */

return 
BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_arg1699z00_2113, 
BINT(0L), 
BINT(BgL_endz00_2116));} } }  else 
{ /* Llib/error.scm 799 */
return BFALSE;} } } } 

}



/* <@exit:1686>~0 */
obj_t BGl_zc3z04exitza31686ze3ze70z60zz__errorz00(obj_t BgL_fnamez00_5265, obj_t BgL_cell1102z00_5264, obj_t BgL_env1107z00_5263)
{
{ /* Llib/error.scm 794 */
jmp_buf_t jmpbuf; 
 void * BgL_an_exit1111z00_2085;
if( SET_EXIT(BgL_an_exit1111z00_2085 ) ) { 
return 
BGL_EXIT_VALUE();
} else {
#if( SIGSETJMP_SAVESIGS == 0 )
  // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
  // bgl_restore_signal_handlers();
#endif

BgL_an_exit1111z00_2085 = 
(void *)jmpbuf; 
PUSH_ENV_EXIT(BgL_env1107z00_5263, BgL_an_exit1111z00_2085, 1L); 
{ /* Llib/error.scm 794 */
 obj_t BgL_escape1103z00_2086;
BgL_escape1103z00_2086 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1107z00_5263); 
{ /* Llib/error.scm 794 */
 obj_t BgL_res1114z00_2087;
{ /* Llib/error.scm 794 */
 obj_t BgL_ohs1099z00_2088;
BgL_ohs1099z00_2088 = 
BGL_ENV_ERROR_HANDLER_GET(BgL_env1107z00_5263); 
{ /* Llib/error.scm 794 */
 obj_t BgL_hds1100z00_2089;
BgL_hds1100z00_2089 = 
MAKE_STACK_PAIR(BgL_escape1103z00_2086, BgL_cell1102z00_5264); 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1107z00_5263, BgL_hds1100z00_2089); BUNSPEC; 
{ /* Llib/error.scm 794 */
 obj_t BgL_exitd1108z00_2090;
BgL_exitd1108z00_2090 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1107z00_5263); 
{ /* Llib/error.scm 794 */
 obj_t BgL_tmp1110z00_2091;
{ /* Llib/error.scm 794 */
 obj_t BgL_arg1688z00_2096;
BgL_arg1688z00_2096 = 
BGL_EXITD_PROTECT(BgL_exitd1108z00_2090); 
BgL_tmp1110z00_2091 = 
MAKE_YOUNG_PAIR(BgL_ohs1099z00_2088, BgL_arg1688z00_2096); } 
{ /* Llib/error.scm 794 */

BGL_EXITD_PROTECT_SET(BgL_exitd1108z00_2090, BgL_tmp1110z00_2091); BUNSPEC; 
{ /* Llib/error.scm 796 */
 obj_t BgL_tmp1109z00_2092;
{ /* Ieee/port.scm 466 */

BgL_tmp1109z00_2092 = 
BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_fnamez00_5265, BTRUE, 
BINT(5000000L)); } 
{ /* Llib/error.scm 794 */
 bool_t BgL_test3259z00_6776;
{ /* Llib/error.scm 794 */
 obj_t BgL_arg2609z00_3991;
BgL_arg2609z00_3991 = 
BGL_EXITD_PROTECT(BgL_exitd1108z00_2090); 
BgL_test3259z00_6776 = 
PAIRP(BgL_arg2609z00_3991); } 
if(BgL_test3259z00_6776)
{ /* Llib/error.scm 794 */
 obj_t BgL_arg2607z00_3992;
{ /* Llib/error.scm 794 */
 obj_t BgL_arg2608z00_3993;
BgL_arg2608z00_3993 = 
BGL_EXITD_PROTECT(BgL_exitd1108z00_2090); 
BgL_arg2607z00_3992 = 
CDR(
((obj_t)BgL_arg2608z00_3993)); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1108z00_2090, BgL_arg2607z00_3992); BUNSPEC; }  else 
{ /* Llib/error.scm 794 */BFALSE; } } 
BGL_ENV_ERROR_HANDLER_SET(BgL_env1107z00_5263, BgL_ohs1099z00_2088); BUNSPEC; 
BgL_res1114z00_2087 = BgL_tmp1109z00_2092; } } } } } } 
POP_ENV_EXIT(BgL_env1107z00_5263); 
return BgL_res1114z00_2087;} } 
}} 

}



/* filename-for-error */
obj_t BGl_filenamezd2forzd2errorz00zz__errorz00(obj_t BgL_filez00_89, long BgL_sza7za7_90)
{
{ /* Llib/error.scm 807 */
if(
(BgL_sza7za7_90<0L))
{ /* Llib/error.scm 809 */
return BGl_string2865z00zz__errorz00;}  else 
{ /* Llib/error.scm 809 */
if(
fexists(
BSTRING_TO_STRING(BgL_filez00_89)))
{ /* Llib/error.scm 811 */
return 
BGl_relativezd2filezd2namez00zz__errorz00(BgL_filez00_89);}  else 
{ /* Llib/error.scm 813 */
 bool_t BgL_test3262z00_6791;
{ /* Llib/error.scm 813 */

BgL_test3262z00_6791 = 
BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(BGl_string2868z00zz__errorz00, BgL_filez00_89, BFALSE, BFALSE, BFALSE, BFALSE); } 
if(BgL_test3262z00_6791)
{ /* Llib/error.scm 813 */
if(
(
STRING_LENGTH(
((obj_t)BgL_filez00_89))<=
(9L+BgL_sza7za7_90)))
{ /* Ieee/string.scm 194 */
 long BgL_endz00_2145;
BgL_endz00_2145 = 
STRING_LENGTH(
((obj_t)BgL_filez00_89)); 
{ /* Ieee/string.scm 194 */

return 
BGl_substringz00zz__r4_strings_6_7z00(BgL_filez00_89, 9L, BgL_endz00_2145);} }  else 
{ /* Llib/error.scm 816 */
 obj_t BgL_arg1707z00_2146;
BgL_arg1707z00_2146 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_filez00_89, 9L, 
(BgL_sza7za7_90+6L)); 
{ /* Llib/error.scm 816 */
 obj_t BgL_list1708z00_2147;
{ /* Llib/error.scm 816 */
 obj_t BgL_arg1709z00_2148;
BgL_arg1709z00_2148 = 
MAKE_YOUNG_PAIR(BGl_string2865z00zz__errorz00, BNIL); 
BgL_list1708z00_2147 = 
MAKE_YOUNG_PAIR(BgL_arg1707z00_2146, BgL_arg1709z00_2148); } 
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1708z00_2147);} } }  else 
{ /* Llib/error.scm 813 */
if(
(
STRING_LENGTH(
((obj_t)BgL_filez00_89))<=BgL_sza7za7_90))
{ /* Llib/error.scm 817 */
return BgL_filez00_89;}  else 
{ /* Llib/error.scm 817 */
if(
(BgL_sza7za7_90<4L))
{ /* Llib/error.scm 819 */
return BGl_string2865z00zz__errorz00;}  else 
{ /* Llib/error.scm 822 */
 obj_t BgL_arg1718z00_2155;
BgL_arg1718z00_2155 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_filez00_89, 0L, 
(BgL_sza7za7_90-3L)); 
{ /* Llib/error.scm 822 */
 obj_t BgL_list1719z00_2156;
{ /* Llib/error.scm 822 */
 obj_t BgL_arg1720z00_2157;
BgL_arg1720z00_2157 = 
MAKE_YOUNG_PAIR(BGl_string2865z00zz__errorz00, BNIL); 
BgL_list1719z00_2156 = 
MAKE_YOUNG_PAIR(BgL_arg1718z00_2155, BgL_arg1720z00_2157); } 
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1719z00_2156);} } } } } } } 

}



/* location-line-num */
obj_t BGl_locationzd2linezd2numz00zz__errorz00(obj_t BgL_locz00_93)
{
{ /* Llib/error.scm 827 */
{ 

if(
PAIRP(BgL_locz00_93))
{ /* Llib/error.scm 881 */
 obj_t BgL_cdrzd2226zd2_2182;
BgL_cdrzd2226zd2_2182 = 
CDR(
((obj_t)BgL_locz00_93)); 
if(
(
CAR(
((obj_t)BgL_locz00_93))==BGl_symbol2823z00zz__errorz00))
{ /* Llib/error.scm 881 */
if(
PAIRP(BgL_cdrzd2226zd2_2182))
{ /* Llib/error.scm 881 */
 obj_t BgL_cdrzd2230zd2_2186;
BgL_cdrzd2230zd2_2186 = 
CDR(BgL_cdrzd2226zd2_2182); 
if(
PAIRP(BgL_cdrzd2230zd2_2186))
{ /* Llib/error.scm 881 */
if(
NULLP(
CDR(BgL_cdrzd2230zd2_2186)))
{ /* Llib/error.scm 881 */
return 
BGl_locationzd2atze70z35zz__errorz00(
CAR(BgL_cdrzd2226zd2_2182), 
CAR(BgL_cdrzd2230zd2_2186));}  else 
{ /* Llib/error.scm 881 */
BgL_tagzd2217zd2_2179:
{ /* Llib/error.scm 892 */
 obj_t BgL_list1759z00_2221;
{ /* Llib/error.scm 892 */
 obj_t BgL_arg1760z00_2222;
{ /* Llib/error.scm 892 */
 obj_t BgL_arg1761z00_2223;
{ /* Llib/error.scm 892 */
 obj_t BgL_arg1762z00_2224;
BgL_arg1762z00_2224 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
BgL_arg1761z00_2223 = 
MAKE_YOUNG_PAIR(BFALSE, BgL_arg1762z00_2224); } 
BgL_arg1760z00_2222 = 
MAKE_YOUNG_PAIR(BFALSE, BgL_arg1761z00_2223); } 
BgL_list1759z00_2221 = 
MAKE_YOUNG_PAIR(BFALSE, BgL_arg1760z00_2222); } 
BGL_TAIL return 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1759z00_2221);} } }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
if(
(
CAR(
((obj_t)BgL_locz00_93))==BGl_symbol2869z00zz__errorz00))
{ /* Llib/error.scm 881 */
if(
PAIRP(BgL_cdrzd2226zd2_2182))
{ /* Llib/error.scm 881 */
 obj_t BgL_cdrzd2282zd2_2197;
BgL_cdrzd2282zd2_2197 = 
CDR(BgL_cdrzd2226zd2_2182); 
if(
PAIRP(BgL_cdrzd2282zd2_2197))
{ /* Llib/error.scm 881 */
 obj_t BgL_cdrzd2287zd2_2199;
BgL_cdrzd2287zd2_2199 = 
CDR(BgL_cdrzd2282zd2_2197); 
if(
PAIRP(BgL_cdrzd2287zd2_2199))
{ /* Llib/error.scm 881 */
if(
NULLP(
CDR(BgL_cdrzd2287zd2_2199)))
{ /* Llib/error.scm 881 */
return 
BGl_locationzd2linezd2colze70ze7zz__errorz00(
CAR(BgL_cdrzd2226zd2_2182), 
CAR(BgL_cdrzd2282zd2_2197), 
CAR(BgL_cdrzd2287zd2_2199));}  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
 obj_t BgL_cdrzd2314zd2_2207;
BgL_cdrzd2314zd2_2207 = 
CDR(
((obj_t)BgL_locz00_93)); 
if(
(
CAR(
((obj_t)BgL_locz00_93))==BGl_symbol2871z00zz__errorz00))
{ /* Llib/error.scm 881 */
if(
PAIRP(BgL_cdrzd2314zd2_2207))
{ /* Llib/error.scm 881 */
 obj_t BgL_cdrzd2318zd2_2211;
BgL_cdrzd2318zd2_2211 = 
CDR(BgL_cdrzd2314zd2_2207); 
if(
PAIRP(BgL_cdrzd2318zd2_2211))
{ /* Llib/error.scm 881 */
if(
NULLP(
CDR(BgL_cdrzd2318zd2_2211)))
{ /* Llib/error.scm 881 */
return 
BGl_locationzd2linezd2colze70ze7zz__errorz00(
CAR(BgL_cdrzd2314zd2_2207), 
CAR(BgL_cdrzd2318zd2_2211), 
BINT(0L));}  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} } } }  else 
{ /* Llib/error.scm 881 */
goto BgL_tagzd2217zd2_2179;} } } 

}



/* location-at~0 */
obj_t BGl_locationzd2atze70z35zz__errorz00(obj_t BgL_filez00_2233, obj_t BgL_pointz00_2234)
{
{ /* Llib/error.scm 857 */
{ /* Llib/error.scm 836 */
 bool_t BgL_test3280z00_6878;
if(
STRINGP(BgL_filez00_2233))
{ /* Llib/error.scm 836 */
BgL_test3280z00_6878 = 
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_pointz00_2234)
; }  else 
{ /* Llib/error.scm 836 */
BgL_test3280z00_6878 = ((bool_t)0)
; } 
if(BgL_test3280z00_6878)
{ /* Llib/error.scm 837 */
 obj_t BgL_fnamez00_2238;
{ /* Llib/error.scm 837 */
 bool_t BgL_test3282z00_6882;
{ /* Llib/error.scm 837 */
 obj_t BgL_string1z00_4029;
BgL_string1z00_4029 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/error.scm 837 */
 long BgL_l1z00_4031;
BgL_l1z00_4031 = 
STRING_LENGTH(BgL_string1z00_4029); 
if(
(BgL_l1z00_4031==5L))
{ /* Llib/error.scm 837 */
 int BgL_arg2321z00_4034;
{ /* Llib/error.scm 837 */
 char * BgL_auxz00_6889; char * BgL_tmpz00_6887;
BgL_auxz00_6889 = 
BSTRING_TO_STRING(BGl_string2873z00zz__errorz00); 
BgL_tmpz00_6887 = 
BSTRING_TO_STRING(BgL_string1z00_4029); 
BgL_arg2321z00_4034 = 
memcmp(BgL_tmpz00_6887, BgL_auxz00_6889, BgL_l1z00_4031); } 
BgL_test3282z00_6882 = 
(
(long)(BgL_arg2321z00_4034)==0L); }  else 
{ /* Llib/error.scm 837 */
BgL_test3282z00_6882 = ((bool_t)0)
; } } } 
if(BgL_test3282z00_6882)
{ /* Llib/error.scm 837 */
BgL_fnamez00_2238 = 
BGl_stringzd2replacezd2zz__r4_strings_6_7z00(
BGl_uncygdrivez00zz__errorz00(BgL_filez00_2233), 
(char)(((unsigned char)'/')), 
(char)(((unsigned char)'\\'))); }  else 
{ /* Llib/error.scm 837 */
BgL_fnamez00_2238 = BgL_filez00_2233; } } 
{ /* Llib/error.scm 837 */
 obj_t BgL_portz00_2239;
BgL_portz00_2239 = 
BGl_openzd2forzd2errorz00zz__errorz00(BgL_fnamez00_2238); 
{ /* Llib/error.scm 840 */

if(
INPUT_PORTP(BgL_portz00_2239))
{ /* Llib/error.scm 842 */
 obj_t BgL_exitd1117z00_2241;
BgL_exitd1117z00_2241 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/error.scm 856 */
 obj_t BgL_zc3z04anonymousza31799ze3z87_5197;
BgL_zc3z04anonymousza31799ze3z87_5197 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31799ze3ze5zz__errorz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31799ze3z87_5197, 
(int)(0L), BgL_portz00_2239); 
{ /* Llib/error.scm 842 */
 obj_t BgL_arg2610z00_4041;
{ /* Llib/error.scm 842 */
 obj_t BgL_arg2611z00_4042;
BgL_arg2611z00_4042 = 
BGL_EXITD_PROTECT(BgL_exitd1117z00_2241); 
BgL_arg2610z00_4041 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31799ze3z87_5197, BgL_arg2611z00_4042); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_2241, BgL_arg2610z00_4041); BUNSPEC; } 
{ /* Llib/error.scm 846 */
 obj_t BgL_tmp1119z00_2243;
{ 
 obj_t BgL_ostringz00_2245; long BgL_lnumz00_2246; long BgL_oposz00_2247;
BgL_ostringz00_2245 = BFALSE; 
BgL_lnumz00_2246 = 1L; 
BgL_oposz00_2247 = 0L; 
BgL_zc3z04anonymousza31773ze3z87_2248:
{ /* Llib/error.scm 846 */
 obj_t BgL_lstringz00_2249;
BgL_lstringz00_2249 = 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_portz00_2239); 
if(
EOF_OBJECTP(BgL_lstringz00_2249))
{ /* Llib/error.scm 848 */
 obj_t BgL_arg1775z00_2251; long BgL_arg1777z00_2252; obj_t BgL_arg1779z00_2253;
BgL_arg1775z00_2251 = 
BGl_relativeze70ze7zz__errorz00(BgL_fnamez00_2238); 
BgL_arg1777z00_2252 = 
(1L+
(
(long)CINT(BgL_pointz00_2234)-BgL_oposz00_2247)); 
if(
STRINGP(BgL_ostringz00_2245))
{ /* Llib/error.scm 850 */
 obj_t BgL_list1787z00_2260;
{ /* Llib/error.scm 850 */
 obj_t BgL_arg1788z00_2261;
BgL_arg1788z00_2261 = 
MAKE_YOUNG_PAIR(BGl_string2874z00zz__errorz00, BNIL); 
BgL_list1787z00_2260 = 
MAKE_YOUNG_PAIR(BgL_ostringz00_2245, BgL_arg1788z00_2261); } 
BgL_arg1779z00_2253 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1787z00_2260); }  else 
{ /* Llib/error.scm 849 */
BgL_arg1779z00_2253 = BGl_string2874z00zz__errorz00; } 
{ /* Llib/error.scm 848 */
 obj_t BgL_list1780z00_2254;
{ /* Llib/error.scm 848 */
 obj_t BgL_arg1781z00_2255;
{ /* Llib/error.scm 848 */
 obj_t BgL_arg1782z00_2256;
{ /* Llib/error.scm 848 */
 obj_t BgL_arg1783z00_2257;
BgL_arg1783z00_2257 = 
MAKE_YOUNG_PAIR(BgL_arg1779z00_2253, BNIL); 
BgL_arg1782z00_2256 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1777z00_2252), BgL_arg1783z00_2257); } 
BgL_arg1781z00_2255 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lnumz00_2246), BgL_arg1782z00_2256); } 
BgL_list1780z00_2254 = 
MAKE_YOUNG_PAIR(BgL_arg1775z00_2251, BgL_arg1781z00_2255); } 
BgL_tmp1119z00_2243 = 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1780z00_2254); } }  else 
{ /* Llib/error.scm 852 */
 bool_t BgL_test3287z00_6929;
{ /* Llib/error.scm 852 */
 long BgL_arg1798z00_2272;
{ /* Llib/error.scm 852 */
 obj_t BgL_tmpz00_6930;
BgL_tmpz00_6930 = 
((obj_t)BgL_portz00_2239); 
BgL_arg1798z00_2272 = 
INPUT_PORT_FILEPOS(BgL_tmpz00_6930); } 
BgL_test3287z00_6929 = 
(BgL_arg1798z00_2272>
(long)CINT(BgL_pointz00_2234)); } 
if(BgL_test3287z00_6929)
{ /* Llib/error.scm 853 */
 obj_t BgL_arg1791z00_2264; long BgL_arg1792z00_2265;
BgL_arg1791z00_2264 = 
BGl_relativeze70ze7zz__errorz00(BgL_filez00_2233); 
BgL_arg1792z00_2265 = 
(
(long)CINT(BgL_pointz00_2234)-BgL_oposz00_2247); 
{ /* Llib/error.scm 853 */
 obj_t BgL_list1793z00_2266;
{ /* Llib/error.scm 853 */
 obj_t BgL_arg1794z00_2267;
{ /* Llib/error.scm 853 */
 obj_t BgL_arg1795z00_2268;
{ /* Llib/error.scm 853 */
 obj_t BgL_arg1796z00_2269;
BgL_arg1796z00_2269 = 
MAKE_YOUNG_PAIR(BgL_lstringz00_2249, BNIL); 
BgL_arg1795z00_2268 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1792z00_2265), BgL_arg1796z00_2269); } 
BgL_arg1794z00_2267 = 
MAKE_YOUNG_PAIR(
BINT(BgL_lnumz00_2246), BgL_arg1795z00_2268); } 
BgL_list1793z00_2266 = 
MAKE_YOUNG_PAIR(BgL_arg1791z00_2264, BgL_arg1794z00_2267); } 
BgL_tmp1119z00_2243 = 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1793z00_2266); } }  else 
{ /* Llib/error.scm 854 */
 long BgL_oposz00_2270;
{ /* Llib/error.scm 854 */
 obj_t BgL_tmpz00_6945;
BgL_tmpz00_6945 = 
((obj_t)BgL_portz00_2239); 
BgL_oposz00_2270 = 
INPUT_PORT_FILEPOS(BgL_tmpz00_6945); } 
{ 
 long BgL_oposz00_6951; long BgL_lnumz00_6949; obj_t BgL_ostringz00_6948;
BgL_ostringz00_6948 = BgL_lstringz00_2249; 
BgL_lnumz00_6949 = 
(BgL_lnumz00_2246+1L); 
BgL_oposz00_6951 = BgL_oposz00_2270; 
BgL_oposz00_2247 = BgL_oposz00_6951; 
BgL_lnumz00_2246 = BgL_lnumz00_6949; 
BgL_ostringz00_2245 = BgL_ostringz00_6948; 
goto BgL_zc3z04anonymousza31773ze3z87_2248;} } } } } 
{ /* Llib/error.scm 842 */
 bool_t BgL_test3288z00_6952;
{ /* Llib/error.scm 842 */
 obj_t BgL_arg2609z00_4054;
BgL_arg2609z00_4054 = 
BGL_EXITD_PROTECT(BgL_exitd1117z00_2241); 
BgL_test3288z00_6952 = 
PAIRP(BgL_arg2609z00_4054); } 
if(BgL_test3288z00_6952)
{ /* Llib/error.scm 842 */
 obj_t BgL_arg2607z00_4055;
{ /* Llib/error.scm 842 */
 obj_t BgL_arg2608z00_4056;
BgL_arg2608z00_4056 = 
BGL_EXITD_PROTECT(BgL_exitd1117z00_2241); 
BgL_arg2607z00_4055 = 
CDR(
((obj_t)BgL_arg2608z00_4056)); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_2241, BgL_arg2607z00_4055); BUNSPEC; }  else 
{ /* Llib/error.scm 842 */BFALSE; } } 
bgl_close_input_port(BgL_portz00_2239); 
return BgL_tmp1119z00_2243;} } }  else 
{ /* Llib/error.scm 857 */
 obj_t BgL_arg1800z00_2276;
BgL_arg1800z00_2276 = 
BGl_relativeze70ze7zz__errorz00(BgL_filez00_2233); 
{ /* Llib/error.scm 857 */
 obj_t BgL_list1801z00_2277;
{ /* Llib/error.scm 857 */
 obj_t BgL_arg1802z00_2278;
{ /* Llib/error.scm 857 */
 obj_t BgL_arg1803z00_2279;
{ /* Llib/error.scm 857 */
 obj_t BgL_arg1804z00_2280;
BgL_arg1804z00_2280 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
BgL_arg1803z00_2279 = 
MAKE_YOUNG_PAIR(BgL_pointz00_2234, BgL_arg1804z00_2280); } 
BgL_arg1802z00_2278 = 
MAKE_YOUNG_PAIR(BFALSE, BgL_arg1803z00_2279); } 
BgL_list1801z00_2277 = 
MAKE_YOUNG_PAIR(BgL_arg1800z00_2276, BgL_arg1802z00_2278); } 
return 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1801z00_2277);} } } } }  else 
{ /* Llib/error.scm 836 */
return BFALSE;} } } 

}



/* relative~0 */
obj_t BGl_relativeze70ze7zz__errorz00(obj_t BgL_filez00_2225)
{
{ /* Llib/error.scm 833 */
{ /* Llib/error.scm 830 */
 obj_t BgL_relz00_2227;
BgL_relz00_2227 = 
BGl_relativezd2filezd2namez00zz__errorz00(BgL_filez00_2225); 
if(
(
STRING_LENGTH(
((obj_t)BgL_relz00_2227))<
STRING_LENGTH(
((obj_t)BgL_filez00_2225))))
{ /* Llib/error.scm 831 */
return BgL_relz00_2227;}  else 
{ /* Llib/error.scm 831 */
return BgL_filez00_2225;} } } 

}



/* location-line-col~0 */
obj_t BGl_locationzd2linezd2colze70ze7zz__errorz00(obj_t BgL_filez00_2286, obj_t BgL_linez00_2287, obj_t BgL_colz00_2288)
{
{ /* Llib/error.scm 879 */
{ /* Llib/error.scm 860 */
 bool_t BgL_test3290z00_6973;
if(
(
(long)CINT(BgL_linez00_2287)>=0L))
{ /* Llib/error.scm 860 */
BgL_test3290z00_6973 = 
(
(long)CINT(BgL_colz00_2288)>=0L)
; }  else 
{ /* Llib/error.scm 860 */
BgL_test3290z00_6973 = ((bool_t)0)
; } 
if(BgL_test3290z00_6973)
{ /* Llib/error.scm 861 */
 obj_t BgL_fnamez00_2292;
{ /* Llib/error.scm 861 */
 bool_t BgL_test3292z00_6979;
{ /* Llib/error.scm 861 */
 obj_t BgL_string1z00_4061;
BgL_string1z00_4061 = 
string_to_bstring(OS_CLASS); 
{ /* Llib/error.scm 861 */
 long BgL_l1z00_4063;
BgL_l1z00_4063 = 
STRING_LENGTH(BgL_string1z00_4061); 
if(
(BgL_l1z00_4063==5L))
{ /* Llib/error.scm 861 */
 int BgL_arg2321z00_4066;
{ /* Llib/error.scm 861 */
 char * BgL_auxz00_6986; char * BgL_tmpz00_6984;
BgL_auxz00_6986 = 
BSTRING_TO_STRING(BGl_string2873z00zz__errorz00); 
BgL_tmpz00_6984 = 
BSTRING_TO_STRING(BgL_string1z00_4061); 
BgL_arg2321z00_4066 = 
memcmp(BgL_tmpz00_6984, BgL_auxz00_6986, BgL_l1z00_4063); } 
BgL_test3292z00_6979 = 
(
(long)(BgL_arg2321z00_4066)==0L); }  else 
{ /* Llib/error.scm 861 */
BgL_test3292z00_6979 = ((bool_t)0)
; } } } 
if(BgL_test3292z00_6979)
{ /* Llib/error.scm 861 */
BgL_fnamez00_2292 = 
BGl_stringzd2replacezd2zz__r4_strings_6_7z00(
BGl_uncygdrivez00zz__errorz00(BgL_filez00_2286), 
(char)(((unsigned char)'/')), 
(char)(((unsigned char)'\\'))); }  else 
{ /* Llib/error.scm 861 */
BgL_fnamez00_2292 = BgL_filez00_2286; } } 
{ /* Llib/error.scm 861 */
 obj_t BgL_portz00_2293;
BgL_portz00_2293 = 
BGl_openzd2forzd2errorz00zz__errorz00(BgL_fnamez00_2292); 
{ /* Llib/error.scm 864 */

if(
INPUT_PORTP(BgL_portz00_2293))
{ /* Llib/error.scm 866 */
 obj_t BgL_exitd1121z00_2295;
BgL_exitd1121z00_2295 = 
BGL_EXITD_TOP_AS_OBJ(); 
{ /* Llib/error.scm 877 */
 obj_t BgL_zc3z04anonymousza31835ze3z87_5196;
BgL_zc3z04anonymousza31835ze3z87_5196 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31835ze3ze5zz__errorz00, 
(int)(0L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31835ze3z87_5196, 
(int)(0L), BgL_portz00_2293); 
{ /* Llib/error.scm 866 */
 obj_t BgL_arg2610z00_4073;
{ /* Llib/error.scm 866 */
 obj_t BgL_arg2611z00_4074;
BgL_arg2611z00_4074 = 
BGL_EXITD_PROTECT(BgL_exitd1121z00_2295); 
BgL_arg2610z00_4073 = 
MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31835ze3z87_5196, BgL_arg2611z00_4074); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1121z00_2295, BgL_arg2610z00_4073); BUNSPEC; } 
{ /* Llib/error.scm 869 */
 obj_t BgL_tmp1123z00_2297;
{ 
 obj_t BgL_ostringz00_2299; obj_t BgL_lnumz00_2300;
BgL_ostringz00_2299 = BFALSE; 
BgL_lnumz00_2300 = BgL_linez00_2287; 
BgL_zc3z04anonymousza31813ze3z87_2301:
{ /* Llib/error.scm 869 */
 obj_t BgL_lstringz00_2302;
BgL_lstringz00_2302 = 
BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_portz00_2293); 
if(
EOF_OBJECTP(BgL_lstringz00_2302))
{ /* Llib/error.scm 871 */
 obj_t BgL_arg1815z00_2304; long BgL_arg1816z00_2305; obj_t BgL_arg1817z00_2306;
BgL_arg1815z00_2304 = 
BGl_relativeze70ze7zz__errorz00(BgL_filez00_2286); 
BgL_arg1816z00_2305 = 
(
(long)CINT(BgL_colz00_2288)+1L); 
{ /* Llib/error.scm 872 */
 obj_t BgL_list1823z00_2311;
{ /* Llib/error.scm 872 */
 obj_t BgL_arg1826z00_2312;
BgL_arg1826z00_2312 = 
MAKE_YOUNG_PAIR(BGl_string2874z00zz__errorz00, BNIL); 
BgL_list1823z00_2311 = 
MAKE_YOUNG_PAIR(BgL_ostringz00_2299, BgL_arg1826z00_2312); } 
BgL_arg1817z00_2306 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1823z00_2311); } 
{ /* Llib/error.scm 871 */
 obj_t BgL_list1818z00_2307;
{ /* Llib/error.scm 871 */
 obj_t BgL_arg1819z00_2308;
{ /* Llib/error.scm 871 */
 obj_t BgL_arg1820z00_2309;
{ /* Llib/error.scm 871 */
 obj_t BgL_arg1822z00_2310;
BgL_arg1822z00_2310 = 
MAKE_YOUNG_PAIR(BgL_arg1817z00_2306, BNIL); 
BgL_arg1820z00_2309 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1816z00_2305), BgL_arg1822z00_2310); } 
BgL_arg1819z00_2308 = 
MAKE_YOUNG_PAIR(BgL_linez00_2287, BgL_arg1820z00_2309); } 
BgL_list1818z00_2307 = 
MAKE_YOUNG_PAIR(BgL_arg1815z00_2304, BgL_arg1819z00_2308); } 
BgL_tmp1123z00_2297 = 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1818z00_2307); } }  else 
{ /* Llib/error.scm 870 */
if(
(
(long)CINT(BgL_lnumz00_2300)==0L))
{ /* Llib/error.scm 874 */
 obj_t BgL_arg1828z00_2314;
BgL_arg1828z00_2314 = 
BGl_relativeze70ze7zz__errorz00(BgL_filez00_2286); 
{ /* Llib/error.scm 874 */
 obj_t BgL_list1829z00_2315;
{ /* Llib/error.scm 874 */
 obj_t BgL_arg1831z00_2316;
{ /* Llib/error.scm 874 */
 obj_t BgL_arg1832z00_2317;
{ /* Llib/error.scm 874 */
 obj_t BgL_arg1833z00_2318;
BgL_arg1833z00_2318 = 
MAKE_YOUNG_PAIR(BgL_lstringz00_2302, BNIL); 
BgL_arg1832z00_2317 = 
MAKE_YOUNG_PAIR(BgL_colz00_2288, BgL_arg1833z00_2318); } 
BgL_arg1831z00_2316 = 
MAKE_YOUNG_PAIR(BgL_linez00_2287, BgL_arg1832z00_2317); } 
BgL_list1829z00_2315 = 
MAKE_YOUNG_PAIR(BgL_arg1828z00_2314, BgL_arg1831z00_2316); } 
BgL_tmp1123z00_2297 = 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1829z00_2315); } }  else 
{ /* Llib/error.scm 875 */
 long BgL_oposz00_2319;
{ /* Llib/error.scm 875 */
 obj_t BgL_tmpz00_7031;
BgL_tmpz00_7031 = 
((obj_t)BgL_portz00_2293); 
BgL_oposz00_2319 = 
INPUT_PORT_FILEPOS(BgL_tmpz00_7031); } 
{ /* Llib/error.scm 876 */
 long BgL_arg1834z00_2320;
BgL_arg1834z00_2320 = 
(
(long)CINT(BgL_lnumz00_2300)-1L); 
{ 
 obj_t BgL_lnumz00_7037; obj_t BgL_ostringz00_7036;
BgL_ostringz00_7036 = BgL_lstringz00_2302; 
BgL_lnumz00_7037 = 
BINT(BgL_arg1834z00_2320); 
BgL_lnumz00_2300 = BgL_lnumz00_7037; 
BgL_ostringz00_2299 = BgL_ostringz00_7036; 
goto BgL_zc3z04anonymousza31813ze3z87_2301;} } } } } } 
{ /* Llib/error.scm 866 */
 bool_t BgL_test3297z00_7039;
{ /* Llib/error.scm 866 */
 obj_t BgL_arg2609z00_4080;
BgL_arg2609z00_4080 = 
BGL_EXITD_PROTECT(BgL_exitd1121z00_2295); 
BgL_test3297z00_7039 = 
PAIRP(BgL_arg2609z00_4080); } 
if(BgL_test3297z00_7039)
{ /* Llib/error.scm 866 */
 obj_t BgL_arg2607z00_4081;
{ /* Llib/error.scm 866 */
 obj_t BgL_arg2608z00_4082;
BgL_arg2608z00_4082 = 
BGL_EXITD_PROTECT(BgL_exitd1121z00_2295); 
BgL_arg2607z00_4081 = 
CDR(
((obj_t)BgL_arg2608z00_4082)); } 
BGL_EXITD_PROTECT_SET(BgL_exitd1121z00_2295, BgL_arg2607z00_4081); BUNSPEC; }  else 
{ /* Llib/error.scm 866 */BFALSE; } } 
bgl_close_input_port(BgL_portz00_2293); 
return BgL_tmp1123z00_2297;} } }  else 
{ /* Llib/error.scm 878 */
 obj_t BgL_arg1836z00_2324;
BgL_arg1836z00_2324 = 
BGl_relativeze70ze7zz__errorz00(BgL_filez00_2286); 
{ /* Llib/error.scm 878 */
 obj_t BgL_list1837z00_2325;
{ /* Llib/error.scm 878 */
 obj_t BgL_arg1838z00_2326;
{ /* Llib/error.scm 878 */
 obj_t BgL_arg1839z00_2327;
{ /* Llib/error.scm 878 */
 obj_t BgL_arg1840z00_2328;
BgL_arg1840z00_2328 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
BgL_arg1839z00_2327 = 
MAKE_YOUNG_PAIR(BgL_colz00_2288, BgL_arg1840z00_2328); } 
BgL_arg1838z00_2326 = 
MAKE_YOUNG_PAIR(BgL_linez00_2287, BgL_arg1839z00_2327); } 
BgL_list1837z00_2325 = 
MAKE_YOUNG_PAIR(BgL_arg1836z00_2324, BgL_arg1838z00_2326); } 
return 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1837z00_2325);} } } } }  else 
{ /* Llib/error.scm 879 */
 obj_t BgL_arg1846z00_2333;
BgL_arg1846z00_2333 = 
BGl_relativeze70ze7zz__errorz00(BgL_filez00_2286); 
{ /* Llib/error.scm 879 */
 obj_t BgL_list1847z00_2334;
{ /* Llib/error.scm 879 */
 obj_t BgL_arg1848z00_2335;
{ /* Llib/error.scm 879 */
 obj_t BgL_arg1849z00_2336;
{ /* Llib/error.scm 879 */
 obj_t BgL_arg1850z00_2337;
BgL_arg1850z00_2337 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
BgL_arg1849z00_2336 = 
MAKE_YOUNG_PAIR(BgL_colz00_2288, BgL_arg1850z00_2337); } 
BgL_arg1848z00_2335 = 
MAKE_YOUNG_PAIR(BgL_linez00_2287, BgL_arg1849z00_2336); } 
BgL_list1847z00_2334 = 
MAKE_YOUNG_PAIR(BgL_arg1846z00_2333, BgL_arg1848z00_2335); } 
return 
BGl_valuesz00zz__r5_control_features_6_4z00(BgL_list1847z00_2334);} } } } 

}



/* &<@anonymous:1835> */
obj_t BGl_z62zc3z04anonymousza31835ze3ze5zz__errorz00(obj_t BgL_envz00_5198)
{
{ /* Llib/error.scm 866 */
{ /* Llib/error.scm 877 */
 obj_t BgL_portz00_5199;
BgL_portz00_5199 = 
PROCEDURE_REF(BgL_envz00_5198, 
(int)(0L)); 
return 
bgl_close_input_port(
((obj_t)BgL_portz00_5199));} } 

}



/* &<@anonymous:1799> */
obj_t BGl_z62zc3z04anonymousza31799ze3ze5zz__errorz00(obj_t BgL_envz00_5200)
{
{ /* Llib/error.scm 842 */
{ /* Llib/error.scm 856 */
 obj_t BgL_portz00_5201;
BgL_portz00_5201 = 
PROCEDURE_REF(BgL_envz00_5200, 
(int)(0L)); 
return 
bgl_close_input_port(
((obj_t)BgL_portz00_5201));} } 

}



/* exception-location? */
bool_t BGl_exceptionzd2locationzf3z21zz__errorz00(obj_t BgL_ez00_94)
{
{ /* Llib/error.scm 897 */
{ /* Llib/error.scm 899 */
 bool_t BgL_test3298z00_7067;
{ /* Llib/error.scm 899 */
 obj_t BgL_tmpz00_7068;
BgL_tmpz00_7068 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_ez00_94)))->BgL_fnamez00); 
BgL_test3298z00_7067 = 
STRINGP(BgL_tmpz00_7068); } 
if(BgL_test3298z00_7067)
{ /* Llib/error.scm 899 */
BGL_TAIL return 
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_ez00_94)))->BgL_locationz00));}  else 
{ /* Llib/error.scm 899 */
return ((bool_t)0);} } } 

}



/* error-notify */
BGL_EXPORTED_DEF obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t BgL_ez00_95)
{
{ /* Llib/error.scm 904 */
{ /* Llib/error.scm 906 */
 bool_t BgL_test3299z00_7075;
{ /* Llib/error.scm 906 */
 obj_t BgL_classz00_4108;
BgL_classz00_4108 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_ez00_95))
{ /* Llib/error.scm 906 */
 BgL_objectz00_bglt BgL_arg2721z00_4110;
BgL_arg2721z00_4110 = 
(BgL_objectz00_bglt)(BgL_ez00_95); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 906 */
 long BgL_idxz00_4116;
BgL_idxz00_4116 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_4110); 
BgL_test3299z00_7075 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_4116+3L))==BgL_classz00_4108); }  else 
{ /* Llib/error.scm 906 */
 bool_t BgL_res2752z00_4141;
{ /* Llib/error.scm 906 */
 obj_t BgL_oclassz00_4124;
{ /* Llib/error.scm 906 */
 obj_t BgL_arg2728z00_4132; long BgL_arg2729z00_4133;
BgL_arg2728z00_4132 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 906 */
 long BgL_arg2731z00_4134;
BgL_arg2731z00_4134 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_4110); 
BgL_arg2729z00_4133 = 
(BgL_arg2731z00_4134-OBJECT_TYPE); } 
BgL_oclassz00_4124 = 
VECTOR_REF(BgL_arg2728z00_4132,BgL_arg2729z00_4133); } 
{ /* Llib/error.scm 906 */
 bool_t BgL__ortest_1198z00_4125;
BgL__ortest_1198z00_4125 = 
(BgL_classz00_4108==BgL_oclassz00_4124); 
if(BgL__ortest_1198z00_4125)
{ /* Llib/error.scm 906 */
BgL_res2752z00_4141 = BgL__ortest_1198z00_4125; }  else 
{ /* Llib/error.scm 906 */
 long BgL_odepthz00_4126;
{ /* Llib/error.scm 906 */
 obj_t BgL_arg2717z00_4127;
BgL_arg2717z00_4127 = 
(BgL_oclassz00_4124); 
BgL_odepthz00_4126 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_4127); } 
if(
(3L<BgL_odepthz00_4126))
{ /* Llib/error.scm 906 */
 obj_t BgL_arg2715z00_4129;
{ /* Llib/error.scm 906 */
 obj_t BgL_arg2716z00_4130;
BgL_arg2716z00_4130 = 
(BgL_oclassz00_4124); 
BgL_arg2715z00_4129 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_4130, 3L); } 
BgL_res2752z00_4141 = 
(BgL_arg2715z00_4129==BgL_classz00_4108); }  else 
{ /* Llib/error.scm 906 */
BgL_res2752z00_4141 = ((bool_t)0); } } } } 
BgL_test3299z00_7075 = BgL_res2752z00_4141; } }  else 
{ /* Llib/error.scm 906 */
BgL_test3299z00_7075 = ((bool_t)0)
; } } 
if(BgL_test3299z00_7075)
{ /* Llib/error.scm 906 */
if(
BGl_exceptionzd2locationzf3z21zz__errorz00(BgL_ez00_95))
{ /* Llib/error.scm 909 */
 obj_t BgL_arg1856z00_2349; obj_t BgL_arg1857z00_2350;
BgL_arg1856z00_2349 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_ez00_95)))->BgL_fnamez00); 
BgL_arg1857z00_2350 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_ez00_95)))->BgL_locationz00); 
return 
BGl_notifyzd2z62errorzf2locz42zz__errorz00(BgL_ez00_95, BgL_arg1856z00_2349, BgL_arg1857z00_2350);}  else 
{ /* Llib/error.scm 907 */
BGL_TAIL return 
BGl_notifyzd2z62errorzb0zz__errorz00(BgL_ez00_95);} }  else 
{ /* Llib/error.scm 911 */
 bool_t BgL_test3305z00_7106;
{ /* Llib/error.scm 911 */
 obj_t BgL_classz00_4142;
BgL_classz00_4142 = BGl_z62conditionz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_ez00_95))
{ /* Llib/error.scm 911 */
 BgL_objectz00_bglt BgL_arg2721z00_4144;
BgL_arg2721z00_4144 = 
(BgL_objectz00_bglt)(BgL_ez00_95); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 911 */
 long BgL_idxz00_4150;
BgL_idxz00_4150 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_4144); 
BgL_test3305z00_7106 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_4150+1L))==BgL_classz00_4142); }  else 
{ /* Llib/error.scm 911 */
 bool_t BgL_res2753z00_4175;
{ /* Llib/error.scm 911 */
 obj_t BgL_oclassz00_4158;
{ /* Llib/error.scm 911 */
 obj_t BgL_arg2728z00_4166; long BgL_arg2729z00_4167;
BgL_arg2728z00_4166 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 911 */
 long BgL_arg2731z00_4168;
BgL_arg2731z00_4168 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_4144); 
BgL_arg2729z00_4167 = 
(BgL_arg2731z00_4168-OBJECT_TYPE); } 
BgL_oclassz00_4158 = 
VECTOR_REF(BgL_arg2728z00_4166,BgL_arg2729z00_4167); } 
{ /* Llib/error.scm 911 */
 bool_t BgL__ortest_1198z00_4159;
BgL__ortest_1198z00_4159 = 
(BgL_classz00_4142==BgL_oclassz00_4158); 
if(BgL__ortest_1198z00_4159)
{ /* Llib/error.scm 911 */
BgL_res2753z00_4175 = BgL__ortest_1198z00_4159; }  else 
{ /* Llib/error.scm 911 */
 long BgL_odepthz00_4160;
{ /* Llib/error.scm 911 */
 obj_t BgL_arg2717z00_4161;
BgL_arg2717z00_4161 = 
(BgL_oclassz00_4158); 
BgL_odepthz00_4160 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_4161); } 
if(
(1L<BgL_odepthz00_4160))
{ /* Llib/error.scm 911 */
 obj_t BgL_arg2715z00_4163;
{ /* Llib/error.scm 911 */
 obj_t BgL_arg2716z00_4164;
BgL_arg2716z00_4164 = 
(BgL_oclassz00_4158); 
BgL_arg2715z00_4163 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_4164, 1L); } 
BgL_res2753z00_4175 = 
(BgL_arg2715z00_4163==BgL_classz00_4142); }  else 
{ /* Llib/error.scm 911 */
BgL_res2753z00_4175 = ((bool_t)0); } } } } 
BgL_test3305z00_7106 = BgL_res2753z00_4175; } }  else 
{ /* Llib/error.scm 911 */
BgL_test3305z00_7106 = ((bool_t)0)
; } } 
if(BgL_test3305z00_7106)
{ /* Llib/error.scm 912 */
 obj_t BgL_arg1859z00_2352;
{ /* Llib/error.scm 912 */
 obj_t BgL_tmpz00_7129;
BgL_tmpz00_7129 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1859z00_2352 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7129); } 
{ /* Llib/error.scm 912 */
 obj_t BgL_list1860z00_2353;
{ /* Llib/error.scm 912 */
 obj_t BgL_arg1862z00_2354;
BgL_arg1862z00_2354 = 
MAKE_YOUNG_PAIR(BgL_ez00_95, BNIL); 
BgL_list1860z00_2353 = 
MAKE_YOUNG_PAIR(BGl_string2875z00zz__errorz00, BgL_arg1862z00_2354); } 
return 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg1859z00_2352, BgL_list1860z00_2353);} }  else 
{ /* Llib/error.scm 911 */
return BFALSE;} } } } 

}



/* &error-notify */
obj_t BGl_z62errorzd2notifyzb0zz__errorz00(obj_t BgL_envz00_5202, obj_t BgL_ez00_5203)
{
{ /* Llib/error.scm 904 */
return 
BGl_errorzd2notifyzd2zz__errorz00(BgL_ez00_5203);} 

}



/* error-notify/location */
BGL_EXPORTED_DEF obj_t BGl_errorzd2notifyzf2locationz20zz__errorz00(obj_t BgL_ez00_96, obj_t BgL_fnamez00_97, int BgL_locationz00_98)
{
{ /* Llib/error.scm 917 */
{ /* Llib/error.scm 918 */
 bool_t BgL_test3310z00_7136;
{ /* Llib/error.scm 918 */
 obj_t BgL_classz00_4178;
BgL_classz00_4178 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_ez00_96))
{ /* Llib/error.scm 918 */
 BgL_objectz00_bglt BgL_arg2721z00_4180;
BgL_arg2721z00_4180 = 
(BgL_objectz00_bglt)(BgL_ez00_96); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 918 */
 long BgL_idxz00_4186;
BgL_idxz00_4186 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_4180); 
BgL_test3310z00_7136 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_4186+3L))==BgL_classz00_4178); }  else 
{ /* Llib/error.scm 918 */
 bool_t BgL_res2754z00_4211;
{ /* Llib/error.scm 918 */
 obj_t BgL_oclassz00_4194;
{ /* Llib/error.scm 918 */
 obj_t BgL_arg2728z00_4202; long BgL_arg2729z00_4203;
BgL_arg2728z00_4202 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 918 */
 long BgL_arg2731z00_4204;
BgL_arg2731z00_4204 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_4180); 
BgL_arg2729z00_4203 = 
(BgL_arg2731z00_4204-OBJECT_TYPE); } 
BgL_oclassz00_4194 = 
VECTOR_REF(BgL_arg2728z00_4202,BgL_arg2729z00_4203); } 
{ /* Llib/error.scm 918 */
 bool_t BgL__ortest_1198z00_4195;
BgL__ortest_1198z00_4195 = 
(BgL_classz00_4178==BgL_oclassz00_4194); 
if(BgL__ortest_1198z00_4195)
{ /* Llib/error.scm 918 */
BgL_res2754z00_4211 = BgL__ortest_1198z00_4195; }  else 
{ /* Llib/error.scm 918 */
 long BgL_odepthz00_4196;
{ /* Llib/error.scm 918 */
 obj_t BgL_arg2717z00_4197;
BgL_arg2717z00_4197 = 
(BgL_oclassz00_4194); 
BgL_odepthz00_4196 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_4197); } 
if(
(3L<BgL_odepthz00_4196))
{ /* Llib/error.scm 918 */
 obj_t BgL_arg2715z00_4199;
{ /* Llib/error.scm 918 */
 obj_t BgL_arg2716z00_4200;
BgL_arg2716z00_4200 = 
(BgL_oclassz00_4194); 
BgL_arg2715z00_4199 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_4200, 3L); } 
BgL_res2754z00_4211 = 
(BgL_arg2715z00_4199==BgL_classz00_4178); }  else 
{ /* Llib/error.scm 918 */
BgL_res2754z00_4211 = ((bool_t)0); } } } } 
BgL_test3310z00_7136 = BgL_res2754z00_4211; } }  else 
{ /* Llib/error.scm 918 */
BgL_test3310z00_7136 = ((bool_t)0)
; } } 
if(BgL_test3310z00_7136)
{ /* Llib/error.scm 918 */
return 
BGl_notifyzd2z62errorzf2locz42zz__errorz00(BgL_ez00_96, BgL_fnamez00_97, 
BINT(BgL_locationz00_98));}  else 
{ /* Llib/error.scm 918 */
return BFALSE;} } } 

}



/* &error-notify/location */
obj_t BGl_z62errorzd2notifyzf2locationz42zz__errorz00(obj_t BgL_envz00_5204, obj_t BgL_ez00_5205, obj_t BgL_fnamez00_5206, obj_t BgL_locationz00_5207)
{
{ /* Llib/error.scm 917 */
{ /* Llib/error.scm 918 */
 int BgL_auxz00_7168; obj_t BgL_auxz00_7161;
{ /* Llib/error.scm 918 */
 obj_t BgL_tmpz00_7169;
if(
INTEGERP(BgL_locationz00_5207))
{ /* Llib/error.scm 918 */
BgL_tmpz00_7169 = BgL_locationz00_5207
; }  else 
{ 
 obj_t BgL_auxz00_7172;
BgL_auxz00_7172 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(36967L), BGl_string2876z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_locationz00_5207); 
FAILURE(BgL_auxz00_7172,BFALSE,BFALSE);} 
BgL_auxz00_7168 = 
CINT(BgL_tmpz00_7169); } 
if(
STRINGP(BgL_fnamez00_5206))
{ /* Llib/error.scm 918 */
BgL_auxz00_7161 = BgL_fnamez00_5206
; }  else 
{ 
 obj_t BgL_auxz00_7164;
BgL_auxz00_7164 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(36967L), BGl_string2876z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_fnamez00_5206); 
FAILURE(BgL_auxz00_7164,BFALSE,BFALSE);} 
return 
BGl_errorzd2notifyzf2locationz20zz__errorz00(BgL_ez00_5205, BgL_auxz00_7161, BgL_auxz00_7168);} } 

}



/* warning-notify */
BGL_EXPORTED_DEF obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t BgL_ez00_99)
{
{ /* Llib/error.scm 924 */
{ 
 obj_t BgL_ez00_2378;
{ /* Llib/error.scm 938 */
 bool_t BgL_test3317z00_7178;
{ /* Llib/error.scm 938 */
 int BgL_arg1880z00_2377;
BgL_arg1880z00_2377 = 
BGl_bigloozd2warningzd2zz__paramz00(); 
BgL_test3317z00_7178 = 
(
(long)(BgL_arg1880z00_2377)>0L); } 
if(BgL_test3317z00_7178)
{ /* Llib/error.scm 938 */
if(
BGl_exceptionzd2locationzf3z21zz__errorz00(BgL_ez00_99))
{ /* Llib/error.scm 942 */
 bool_t BgL_test3319z00_7184;
{ /* Llib/error.scm 942 */
 obj_t BgL_arg1876z00_2369;
BgL_arg1876z00_2369 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62warningz62_bglt)BgL_ez00_99))))->BgL_fnamez00); 
{ /* Llib/error.scm 942 */
 long BgL_l1z00_4227;
BgL_l1z00_4227 = 
STRING_LENGTH(
((obj_t)BgL_arg1876z00_2369)); 
if(
(BgL_l1z00_4227==8L))
{ /* Llib/error.scm 942 */
 int BgL_arg2321z00_4230;
{ /* Llib/error.scm 942 */
 char * BgL_auxz00_7195; char * BgL_tmpz00_7192;
BgL_auxz00_7195 = 
BSTRING_TO_STRING(BGl_string2879z00zz__errorz00); 
BgL_tmpz00_7192 = 
BSTRING_TO_STRING(
((obj_t)BgL_arg1876z00_2369)); 
BgL_arg2321z00_4230 = 
memcmp(BgL_tmpz00_7192, BgL_auxz00_7195, BgL_l1z00_4227); } 
BgL_test3319z00_7184 = 
(
(long)(BgL_arg2321z00_4230)==0L); }  else 
{ /* Llib/error.scm 942 */
BgL_test3319z00_7184 = ((bool_t)0)
; } } } 
if(BgL_test3319z00_7184)
{ /* Llib/error.scm 942 */
BgL_ez00_2378 = BgL_ez00_99; 
BgL_zc3z04anonymousza31881ze3z87_2379:
{ /* Llib/error.scm 926 */
 obj_t BgL_arg1882z00_2380;
{ /* Llib/error.scm 926 */
 obj_t BgL_tmpz00_7200;
BgL_tmpz00_7200 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1882z00_2380 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7200); } 
bgl_flush_output_port(BgL_arg1882z00_2380); } 
{ /* Llib/error.scm 927 */
 obj_t BgL_arg1883z00_2381;
{ /* Llib/error.scm 927 */
 obj_t BgL_tmpz00_7204;
BgL_tmpz00_7204 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1883z00_2381 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7204); } 
{ /* Llib/error.scm 927 */
 obj_t BgL_list1884z00_2382;
BgL_list1884z00_2382 = 
MAKE_YOUNG_PAIR(BgL_arg1883z00_2381, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2877z00zz__errorz00, BgL_list1884z00_2382); } } 
if(
NULLP(
(((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_ez00_2378)))->BgL_argsz00)))
{ /* Llib/error.scm 929 */BFALSE; }  else 
{ /* Llib/error.scm 929 */
{ /* Llib/error.scm 931 */
 obj_t BgL_arg1887z00_2386; obj_t BgL_arg1888z00_2387;
{ /* Llib/error.scm 931 */
 obj_t BgL_pairz00_4215;
BgL_pairz00_4215 = 
(((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_ez00_2378)))->BgL_argsz00); 
BgL_arg1887z00_2386 = 
CAR(BgL_pairz00_4215); } 
{ /* Llib/error.scm 931 */
 obj_t BgL_tmpz00_7216;
BgL_tmpz00_7216 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1888z00_2387 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7216); } 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1887z00_2386, BgL_arg1888z00_2387); } 
{ /* Llib/error.scm 932 */
 obj_t BgL_arg1890z00_2389;
{ /* Llib/error.scm 932 */
 obj_t BgL_tmpz00_7220;
BgL_tmpz00_7220 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1890z00_2389 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7220); } 
{ /* Llib/error.scm 932 */
 obj_t BgL_list1891z00_2390;
BgL_list1891z00_2390 = 
MAKE_YOUNG_PAIR(BgL_arg1890z00_2389, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1891z00_2390); } } 
{ /* Llib/error.scm 933 */
 obj_t BgL_arg1893z00_2392;
{ /* Llib/error.scm 935 */
 obj_t BgL_pairz00_4218;
BgL_pairz00_4218 = 
(((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_ez00_2378)))->BgL_argsz00); 
BgL_arg1893z00_2392 = 
CDR(BgL_pairz00_4218); } 
{ /* Llib/error.scm 933 */
 obj_t BgL_list1894z00_2393;
BgL_list1894z00_2393 = 
MAKE_YOUNG_PAIR(BgL_arg1893z00_2392, BNIL); 
BGl_forzd2eachzd2zz__r4_control_features_6_9z00(BGl_proc2878z00zz__errorz00, BgL_list1894z00_2393); } } } 
{ /* Llib/error.scm 936 */
 obj_t BgL_arg1899z00_2400;
{ /* Llib/error.scm 936 */
 obj_t BgL_tmpz00_7230;
BgL_tmpz00_7230 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1899z00_2400 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7230); } 
{ /* Llib/error.scm 936 */
 obj_t BgL_list1900z00_2401;
BgL_list1900z00_2401 = 
MAKE_YOUNG_PAIR(BgL_arg1899z00_2400, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1900z00_2401); } } 
{ /* Llib/error.scm 937 */
 obj_t BgL_arg1901z00_2402;
{ /* Llib/error.scm 937 */
 obj_t BgL_tmpz00_7235;
BgL_tmpz00_7235 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1901z00_2402 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7235); } 
bgl_flush_output_port(BgL_arg1901z00_2402); } }  else 
{ /* Llib/error.scm 944 */
 bool_t BgL_test3322z00_7239;
{ /* Llib/error.scm 944 */
 obj_t BgL_arg1875z00_2368;
BgL_arg1875z00_2368 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62warningz62_bglt)BgL_ez00_99))))->BgL_fnamez00); 
{ /* Llib/error.scm 944 */
 long BgL_l1z00_4238;
BgL_l1z00_4238 = 
STRING_LENGTH(
((obj_t)BgL_arg1875z00_2368)); 
if(
(BgL_l1z00_4238==7L))
{ /* Llib/error.scm 944 */
 int BgL_arg2321z00_4241;
{ /* Llib/error.scm 944 */
 char * BgL_auxz00_7250; char * BgL_tmpz00_7247;
BgL_auxz00_7250 = 
BSTRING_TO_STRING(BGl_string2880z00zz__errorz00); 
BgL_tmpz00_7247 = 
BSTRING_TO_STRING(
((obj_t)BgL_arg1875z00_2368)); 
BgL_arg2321z00_4241 = 
memcmp(BgL_tmpz00_7247, BgL_auxz00_7250, BgL_l1z00_4238); } 
BgL_test3322z00_7239 = 
(
(long)(BgL_arg2321z00_4241)==0L); }  else 
{ /* Llib/error.scm 944 */
BgL_test3322z00_7239 = ((bool_t)0)
; } } } 
if(BgL_test3322z00_7239)
{ 
 obj_t BgL_ez00_7255;
BgL_ez00_7255 = BgL_ez00_99; 
BgL_ez00_2378 = BgL_ez00_7255; 
goto BgL_zc3z04anonymousza31881ze3z87_2379;}  else 
{ /* Llib/error.scm 947 */
 obj_t BgL_arg1872z00_2365; obj_t BgL_arg1873z00_2366; obj_t BgL_arg1874z00_2367;
BgL_arg1872z00_2365 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62warningz62_bglt)BgL_ez00_99))))->BgL_fnamez00); 
BgL_arg1873z00_2366 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62warningz62_bglt)BgL_ez00_99))))->BgL_locationz00); 
BgL_arg1874z00_2367 = 
(((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_ez00_99)))->BgL_argsz00); 
BGl_warningzf2locationzd2filez20zz__errorz00(BgL_arg1872z00_2365, BgL_arg1873z00_2366, BgL_arg1874z00_2367); } } }  else 
{ 
 obj_t BgL_ez00_7265;
BgL_ez00_7265 = BgL_ez00_99; 
BgL_ez00_2378 = BgL_ez00_7265; 
goto BgL_zc3z04anonymousza31881ze3z87_2379;} 
if(
CBOOL(
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62warningz62_bglt)BgL_ez00_99))))->BgL_stackz00)))
{ /* Llib/error.scm 952 */
 obj_t BgL_arg1878z00_2372; obj_t BgL_arg1879z00_2373;
BgL_arg1878z00_2372 = 
(((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)
((BgL_z62warningz62_bglt)BgL_ez00_99))))->BgL_stackz00); 
{ /* Llib/error.scm 952 */
 obj_t BgL_tmpz00_7274;
BgL_tmpz00_7274 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1879z00_2373 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7274); } 
{ /* Llib/error.scm 273 */

BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_arg1878z00_2372, BgL_arg1879z00_2373, 
BINT(1L)); } }  else 
{ /* Llib/error.scm 951 */BFALSE; } }  else 
{ /* Llib/error.scm 938 */BFALSE; } } 
return BFALSE;} } 

}



/* &warning-notify */
obj_t BGl_z62warningzd2notifyzb0zz__errorz00(obj_t BgL_envz00_5209, obj_t BgL_ez00_5210)
{
{ /* Llib/error.scm 924 */
return 
BGl_warningzd2notifyzd2zz__errorz00(BgL_ez00_5210);} 

}



/* &<@anonymous:1895> */
obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__errorz00(obj_t BgL_envz00_5211, obj_t BgL_az00_5212)
{
{ /* Llib/error.scm 933 */
{ /* Llib/error.scm 934 */
 obj_t BgL_arg1896z00_5319;
{ /* Llib/error.scm 934 */
 obj_t BgL_tmpz00_7280;
BgL_tmpz00_7280 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1896z00_5319 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7280); } 
return 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_az00_5212, BgL_arg1896z00_5319);} } 

}



/* warning-notify/location */
BGL_EXPORTED_DEF obj_t BGl_warningzd2notifyzf2locationz20zz__errorz00(obj_t BgL_ez00_100, obj_t BgL_fnamez00_101, int BgL_locationz00_102)
{
{ /* Llib/error.scm 958 */
{ /* Llib/error.scm 959 */
 bool_t BgL_test3325z00_7284;
{ /* Llib/error.scm 959 */
 int BgL_arg1906z00_4249;
BgL_arg1906z00_4249 = 
BGl_bigloozd2warningzd2zz__paramz00(); 
BgL_test3325z00_7284 = 
(
(long)(BgL_arg1906z00_4249)>0L); } 
if(BgL_test3325z00_7284)
{ /* Llib/error.scm 961 */
 obj_t BgL_arg1904z00_4251;
BgL_arg1904z00_4251 = 
(((BgL_z62warningz62_bglt)COBJECT(
((BgL_z62warningz62_bglt)BgL_ez00_100)))->BgL_argsz00); 
return 
BGl_warningzf2locationzd2filez20zz__errorz00(BgL_fnamez00_101, 
BINT(BgL_locationz00_102), BgL_arg1904z00_4251);}  else 
{ /* Llib/error.scm 959 */
return BFALSE;} } } 

}



/* &warning-notify/location */
obj_t BGl_z62warningzd2notifyzf2locationz42zz__errorz00(obj_t BgL_envz00_5213, obj_t BgL_ez00_5214, obj_t BgL_fnamez00_5215, obj_t BgL_locationz00_5216)
{
{ /* Llib/error.scm 958 */
{ /* Llib/error.scm 959 */
 int BgL_auxz00_7299; obj_t BgL_auxz00_7292;
{ /* Llib/error.scm 959 */
 obj_t BgL_tmpz00_7300;
if(
INTEGERP(BgL_locationz00_5216))
{ /* Llib/error.scm 959 */
BgL_tmpz00_7300 = BgL_locationz00_5216
; }  else 
{ 
 obj_t BgL_auxz00_7303;
BgL_auxz00_7303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(38480L), BGl_string2881z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_locationz00_5216); 
FAILURE(BgL_auxz00_7303,BFALSE,BFALSE);} 
BgL_auxz00_7299 = 
CINT(BgL_tmpz00_7300); } 
if(
STRINGP(BgL_fnamez00_5215))
{ /* Llib/error.scm 959 */
BgL_auxz00_7292 = BgL_fnamez00_5215
; }  else 
{ 
 obj_t BgL_auxz00_7295;
BgL_auxz00_7295 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(38480L), BGl_string2881z00zz__errorz00, BGl_string2846z00zz__errorz00, BgL_fnamez00_5215); 
FAILURE(BgL_auxz00_7295,BFALSE,BFALSE);} 
return 
BGl_warningzd2notifyzf2locationz20zz__errorz00(BgL_ez00_5214, BgL_auxz00_7292, BgL_auxz00_7299);} } 

}



/* warning/location-file */
obj_t BGl_warningzf2locationzd2filez20zz__errorz00(obj_t BgL_fnamez00_103, obj_t BgL_locz00_104, obj_t BgL_argsz00_105)
{
{ /* Llib/error.scm 966 */
{ /* Llib/error.scm 968 */
 obj_t BgL_portz00_2409;
BgL_portz00_2409 = 
BGl_openzd2forzd2errorz00zz__errorz00(BgL_fnamez00_103); 
if(
INPUT_PORTP(BgL_portz00_2409))
{ /* Llib/error.scm 974 */
 obj_t BgL_filez00_2411;
{ /* Llib/error.scm 975 */
 obj_t BgL_arg1910z00_2416;
{ /* Llib/error.scm 975 */
 obj_t BgL_list1911z00_2417;
{ /* Llib/error.scm 975 */
 obj_t BgL_arg1912z00_2418;
{ /* Llib/error.scm 975 */
 obj_t BgL_arg1913z00_2419;
BgL_arg1913z00_2419 = 
MAKE_YOUNG_PAIR(BNIL, BNIL); 
BgL_arg1912z00_2418 = 
MAKE_YOUNG_PAIR(BgL_locz00_104, BgL_arg1913z00_2419); } 
BgL_list1911z00_2417 = 
MAKE_YOUNG_PAIR(BgL_fnamez00_103, BgL_arg1912z00_2418); } 
BgL_arg1910z00_2416 = 
BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BGl_symbol2823z00zz__errorz00, BgL_list1911z00_2417); } 
BgL_filez00_2411 = 
BGl_locationzd2linezd2numz00zz__errorz00(BgL_arg1910z00_2416); } 
{ /* Llib/error.scm 975 */
 obj_t BgL_lnumz00_2412; obj_t BgL_lpointz00_2413; obj_t BgL_lstringz00_2414;
{ /* Llib/error.scm 976 */
 obj_t BgL_tmpz00_4253;
{ /* Llib/error.scm 976 */
 int BgL_tmpz00_7317;
BgL_tmpz00_7317 = 
(int)(1L); 
BgL_tmpz00_4253 = 
BGL_MVALUES_VAL(BgL_tmpz00_7317); } 
{ /* Llib/error.scm 976 */
 int BgL_tmpz00_7320;
BgL_tmpz00_7320 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7320, BUNSPEC); } 
BgL_lnumz00_2412 = BgL_tmpz00_4253; } 
{ /* Llib/error.scm 976 */
 obj_t BgL_tmpz00_4254;
{ /* Llib/error.scm 976 */
 int BgL_tmpz00_7323;
BgL_tmpz00_7323 = 
(int)(2L); 
BgL_tmpz00_4254 = 
BGL_MVALUES_VAL(BgL_tmpz00_7323); } 
{ /* Llib/error.scm 976 */
 int BgL_tmpz00_7326;
BgL_tmpz00_7326 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7326, BUNSPEC); } 
BgL_lpointz00_2413 = BgL_tmpz00_4254; } 
{ /* Llib/error.scm 976 */
 obj_t BgL_tmpz00_4255;
{ /* Llib/error.scm 976 */
 int BgL_tmpz00_7329;
BgL_tmpz00_7329 = 
(int)(3L); 
BgL_tmpz00_4255 = 
BGL_MVALUES_VAL(BgL_tmpz00_7329); } 
{ /* Llib/error.scm 976 */
 int BgL_tmpz00_7332;
BgL_tmpz00_7332 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7332, BUNSPEC); } 
BgL_lstringz00_2414 = BgL_tmpz00_4255; } 
if(
CBOOL(BgL_lnumz00_2412))
{ /* Llib/error.scm 976 */
return 
BGl_dozd2warnzf2locationz20zz__errorz00(BgL_fnamez00_103, BgL_lnumz00_2412, BgL_locz00_104, BgL_lstringz00_2414, BgL_lpointz00_2413, BgL_argsz00_105);}  else 
{ /* Llib/error.scm 976 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_105);} } }  else 
{ /* Llib/error.scm 969 */
return 
BGl_warningz00zz__errorz00(BgL_argsz00_105);} } } 

}



/* do-warn/location */
obj_t BGl_dozd2warnzf2locationz20zz__errorz00(obj_t BgL_fnamez00_106, obj_t BgL_linez00_107, obj_t BgL_charz00_108, obj_t BgL_stringz00_109, obj_t BgL_markerz00_110, obj_t BgL_argsz00_111)
{
{ /* Llib/error.scm 983 */
{ /* Llib/error.scm 984 */
 obj_t BgL_arg1916z00_2421;
{ /* Llib/error.scm 984 */
 obj_t BgL_tmpz00_7340;
BgL_tmpz00_7340 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1916z00_2421 = 
BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7340); } 
bgl_flush_output_port(BgL_arg1916z00_2421); } 
{ /* Llib/error.scm 985 */
 obj_t BgL_arg1917z00_2422;
{ /* Llib/error.scm 985 */
 obj_t BgL_tmpz00_7344;
BgL_tmpz00_7344 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1917z00_2422 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7344); } 
{ /* Llib/error.scm 985 */
 obj_t BgL_list1918z00_2423;
BgL_list1918z00_2423 = 
MAKE_YOUNG_PAIR(BgL_arg1917z00_2422, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1918z00_2423); } } 
{ /* Llib/error.scm 986 */
 obj_t BgL_spacezd2stringzd2_2424;
if(
(
(long)CINT(BgL_markerz00_110)>0L))
{ /* Llib/error.scm 986 */
BgL_spacezd2stringzd2_2424 = 
make_string(
(long)CINT(BgL_markerz00_110), ((unsigned char)' ')); }  else 
{ /* Llib/error.scm 986 */
BgL_spacezd2stringzd2_2424 = BGl_string2866z00zz__errorz00; } 
{ /* Llib/error.scm 986 */
 long BgL_lz00_2425;
BgL_lz00_2425 = 
STRING_LENGTH(
((obj_t)BgL_stringz00_109)); 
{ /* Llib/error.scm 989 */
 obj_t BgL_nzd2markerzd2_2426;
if(
(
(long)CINT(BgL_markerz00_110)>=BgL_lz00_2425))
{ /* Llib/error.scm 990 */
BgL_nzd2markerzd2_2426 = 
BINT(BgL_lz00_2425); }  else 
{ /* Llib/error.scm 990 */
BgL_nzd2markerzd2_2426 = BgL_markerz00_110; } 
{ /* Llib/error.scm 990 */

BGl_fixzd2tabulationz12zc0zz__errorz00(BgL_nzd2markerzd2_2426, BgL_stringz00_109, BgL_spacezd2stringzd2_2424); 
BGl_printzd2cursorzd2zz__errorz00(BgL_fnamez00_106, BgL_linez00_107, BgL_charz00_108, BgL_stringz00_109, BgL_spacezd2stringzd2_2424); 
{ /* Llib/error.scm 996 */
 obj_t BgL_arg1919z00_2427;
{ /* Llib/error.scm 996 */
 obj_t BgL_tmpz00_7362;
BgL_tmpz00_7362 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1919z00_2427 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7362); } 
{ /* Llib/error.scm 996 */
 obj_t BgL_list1920z00_2428;
BgL_list1920z00_2428 = 
MAKE_YOUNG_PAIR(BgL_arg1919z00_2427, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2877z00zz__errorz00, BgL_list1920z00_2428); } } 
if(
NULLP(BgL_argsz00_111))
{ /* Llib/error.scm 997 */BFALSE; }  else 
{ /* Llib/error.scm 998 */
 obj_t BgL_portz00_2430;
{ /* Llib/error.scm 998 */
 obj_t BgL_tmpz00_7369;
BgL_tmpz00_7369 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2430 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7369); } 
{ /* Llib/error.scm 999 */
 obj_t BgL_arg1923z00_2431;
BgL_arg1923z00_2431 = 
CAR(
((obj_t)BgL_argsz00_111)); 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_arg1923z00_2431, BgL_portz00_2430); } 
{ /* Llib/error.scm 1000 */
 obj_t BgL_list1924z00_2432;
BgL_list1924z00_2432 = 
MAKE_YOUNG_PAIR(BgL_portz00_2430, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1924z00_2432); } 
{ /* Llib/error.scm 1001 */
 obj_t BgL_arg1926z00_2434;
BgL_arg1926z00_2434 = 
CDR(
((obj_t)BgL_argsz00_111)); 
{ /* Llib/error.scm 1001 */
 obj_t BgL_list1927z00_2435;
BgL_list1927z00_2435 = 
MAKE_YOUNG_PAIR(BgL_arg1926z00_2434, BNIL); 
BGl_forzd2eachzd2zz__r4_control_features_6_9z00(BGl_proc2882z00zz__errorz00, BgL_list1927z00_2435); } } } 
{ /* Llib/error.scm 1004 */
 obj_t BgL_arg1930z00_2440;
{ /* Llib/error.scm 1004 */
 obj_t BgL_tmpz00_7381;
BgL_tmpz00_7381 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1930z00_2440 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7381); } 
{ /* Llib/error.scm 1004 */
 obj_t BgL_list1931z00_2441;
BgL_list1931z00_2441 = 
MAKE_YOUNG_PAIR(BgL_arg1930z00_2440, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list1931z00_2441); } } 
{ /* Llib/error.scm 1005 */
 obj_t BgL_arg1932z00_2442;
{ /* Llib/error.scm 1005 */
 obj_t BgL_tmpz00_7386;
BgL_tmpz00_7386 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1932z00_2442 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7386); } 
return 
bgl_flush_output_port(BgL_arg1932z00_2442);} } } } } } 

}



/* &<@anonymous:1928> */
obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__errorz00(obj_t BgL_envz00_5218, obj_t BgL_az00_5219)
{
{ /* Llib/error.scm 1001 */
{ /* Llib/error.scm 1002 */
 obj_t BgL_arg1929z00_5320;
{ /* Llib/error.scm 1002 */
 obj_t BgL_tmpz00_7390;
BgL_tmpz00_7390 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1929z00_5320 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7390); } 
return 
BGl_displayzd2circlezd2zz__pp_circlez00(BgL_az00_5219, BgL_arg1929z00_5320);} } 

}



/* _display-trace-stack */
obj_t BGl__displayzd2tracezd2stackz00zz__errorz00(obj_t BgL_env1218z00_117, obj_t BgL_opt1217z00_116)
{
{ /* Llib/error.scm 1018 */
{ /* Llib/error.scm 1018 */
 obj_t BgL_g1219z00_2447; obj_t BgL_g1220z00_2448;
BgL_g1219z00_2447 = 
VECTOR_REF(BgL_opt1217z00_116,0L); 
BgL_g1220z00_2448 = 
VECTOR_REF(BgL_opt1217z00_116,1L); 
switch( 
VECTOR_LENGTH(BgL_opt1217z00_116)) { case 2L : 

{ /* Llib/error.scm 1018 */

{ /* Llib/error.scm 1018 */
 obj_t BgL_auxz00_7396;
if(
OUTPUT_PORTP(BgL_g1220z00_2448))
{ /* Llib/error.scm 1018 */
BgL_auxz00_7396 = BgL_g1220z00_2448
; }  else 
{ 
 obj_t BgL_auxz00_7399;
BgL_auxz00_7399 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(41116L), BGl_string2883z00zz__errorz00, BGl_string2884z00zz__errorz00, BgL_g1220z00_2448); 
FAILURE(BgL_auxz00_7399,BFALSE,BFALSE);} 
return 
BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_g1219z00_2447, BgL_auxz00_7396, 
BINT(1L));} } break;case 3L : 

{ /* Llib/error.scm 1018 */
 obj_t BgL_offsetz00_2452;
BgL_offsetz00_2452 = 
VECTOR_REF(BgL_opt1217z00_116,2L); 
{ /* Llib/error.scm 1018 */

{ /* Llib/error.scm 1018 */
 obj_t BgL_auxz00_7406;
if(
OUTPUT_PORTP(BgL_g1220z00_2448))
{ /* Llib/error.scm 1018 */
BgL_auxz00_7406 = BgL_g1220z00_2448
; }  else 
{ 
 obj_t BgL_auxz00_7409;
BgL_auxz00_7409 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(41116L), BGl_string2883z00zz__errorz00, BGl_string2884z00zz__errorz00, BgL_g1220z00_2448); 
FAILURE(BgL_auxz00_7409,BFALSE,BFALSE);} 
return 
BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_g1219z00_2447, BgL_auxz00_7406, BgL_offsetz00_2452);} } } break;
default: 
return BUNSPEC;} } } 

}



/* display-trace-stack */
BGL_EXPORTED_DEF obj_t BGl_displayzd2tracezd2stackz00zz__errorz00(obj_t BgL_stackz00_113, obj_t BgL_portz00_114, obj_t BgL_offsetz00_115)
{
{ /* Llib/error.scm 1018 */
if(
PAIRP(BgL_stackz00_113))
{ /* Llib/error.scm 1102 */
 obj_t BgL_g1134z00_2456; obj_t BgL_g1135z00_2457;
BgL_g1134z00_2456 = 
CDR(BgL_stackz00_113); 
BgL_g1135z00_2457 = 
CAR(BgL_stackz00_113); 
{ 
 obj_t BgL_iz00_2459; obj_t BgL_stkz00_2460; obj_t BgL_hdsz00_2461; long BgL_hdnz00_2462;
BgL_iz00_2459 = BgL_offsetz00_115; 
BgL_stkz00_2460 = BgL_g1134z00_2456; 
BgL_hdsz00_2461 = BgL_g1135z00_2457; 
BgL_hdnz00_2462 = 1L; 
BgL_zc3z04anonymousza31937ze3z87_2463:
if(
NULLP(BgL_stkz00_2460))
{ /* Llib/error.scm 1107 */
BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00(BgL_portz00_114, BgL_hdsz00_2461, BgL_iz00_2459, BgL_hdnz00_2462); 
return 
bgl_flush_output_port(BgL_portz00_114);}  else 
{ /* Llib/error.scm 1107 */
if(
PAIRP(BgL_stkz00_2460))
{ /* Llib/error.scm 1110 */
if(
(
CAR(BgL_stkz00_2460)==BgL_hdsz00_2461))
{ /* Llib/error.scm 1115 */
 long BgL_arg1942z00_2468; obj_t BgL_arg1943z00_2469; long BgL_arg1944z00_2470;
BgL_arg1942z00_2468 = 
(
(long)CINT(BgL_iz00_2459)+1L); 
BgL_arg1943z00_2469 = 
CDR(BgL_stkz00_2460); 
BgL_arg1944z00_2470 = 
(BgL_hdnz00_2462+1L); 
{ 
 long BgL_hdnz00_7436; obj_t BgL_stkz00_7435; obj_t BgL_iz00_7433;
BgL_iz00_7433 = 
BINT(BgL_arg1942z00_2468); 
BgL_stkz00_7435 = BgL_arg1943z00_2469; 
BgL_hdnz00_7436 = BgL_arg1944z00_2470; 
BgL_hdnz00_2462 = BgL_hdnz00_7436; 
BgL_stkz00_2460 = BgL_stkz00_7435; 
BgL_iz00_2459 = BgL_iz00_7433; 
goto BgL_zc3z04anonymousza31937ze3z87_2463;} }  else 
{ 
 long BgL_hdnz00_7443; obj_t BgL_hdsz00_7441; obj_t BgL_stkz00_7439; obj_t BgL_iz00_7437;
BgL_iz00_7437 = 
BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00(BgL_portz00_114, BgL_hdsz00_2461, BgL_iz00_2459, BgL_hdnz00_2462); 
BgL_stkz00_7439 = 
CDR(BgL_stkz00_2460); 
BgL_hdsz00_7441 = 
CAR(BgL_stkz00_2460); 
BgL_hdnz00_7443 = 1L; 
BgL_hdnz00_2462 = BgL_hdnz00_7443; 
BgL_hdsz00_2461 = BgL_hdsz00_7441; 
BgL_stkz00_2460 = BgL_stkz00_7439; 
BgL_iz00_2459 = BgL_iz00_7437; 
goto BgL_zc3z04anonymousza31937ze3z87_2463;} }  else 
{ /* Llib/error.scm 1110 */
{ /* Llib/error.scm 1111 */
 obj_t BgL_arg1948z00_2475;
{ /* Llib/error.scm 1111 */
 obj_t BgL_tmpz00_7444;
BgL_tmpz00_7444 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg1948z00_2475 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7444); } 
{ /* Llib/error.scm 1111 */
 obj_t BgL_list1949z00_2476;
BgL_list1949z00_2476 = 
MAKE_YOUNG_PAIR(BgL_stackz00_113, BNIL); 
BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_arg1948z00_2475, BGl_string2885z00zz__errorz00, BgL_list1949z00_2476); } } 
return 
bgl_flush_output_port(BgL_portz00_114);} } } }  else 
{ /* Llib/error.scm 1101 */
return BFALSE;} } 

}



/* display-trace-stack-frame~0 */
obj_t BGl_displayzd2tracezd2stackzd2frameze70z35zz__errorz00(obj_t BgL_portz00_5262, obj_t BgL_framez00_2484, obj_t BgL_levelz00_2485, long BgL_numz00_2486)
{
{ /* Llib/error.scm 1099 */
{ 
 obj_t BgL_namez00_2488; obj_t BgL_locz00_2489; obj_t BgL_restz00_2490; obj_t BgL_namez00_2492;
if(
PAIRP(BgL_framez00_2484))
{ /* Llib/error.scm 1099 */
 obj_t BgL_cdrzd2347zd2_2498;
BgL_cdrzd2347zd2_2498 = 
CDR(
((obj_t)BgL_framez00_2484)); 
if(
PAIRP(BgL_cdrzd2347zd2_2498))
{ /* Llib/error.scm 1099 */
 obj_t BgL_cdrzd2352zd2_2500;
BgL_cdrzd2352zd2_2500 = 
CDR(BgL_cdrzd2347zd2_2498); 
{ /* Llib/error.scm 1099 */
 bool_t BgL_test3341z00_7457;
if(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_cdrzd2352zd2_2500))
{ /* Llib/error.scm 1013 */
 obj_t BgL_list1935z00_4310;
BgL_list1935z00_4310 = 
MAKE_YOUNG_PAIR(BgL_cdrzd2352zd2_2500, BNIL); 
BgL_test3341z00_7457 = 
CBOOL(
BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_list1935z00_4310)); }  else 
{ /* Llib/error.scm 1013 */
BgL_test3341z00_7457 = ((bool_t)0)
; } 
if(BgL_test3341z00_7457)
{ /* Llib/error.scm 1099 */
 obj_t BgL_arg1957z00_2502; obj_t BgL_arg1958z00_2503;
BgL_arg1957z00_2502 = 
CAR(
((obj_t)BgL_framez00_2484)); 
BgL_arg1958z00_2503 = 
CAR(BgL_cdrzd2347zd2_2498); 
{ /* Llib/error.scm 1099 */
 long BgL_tmpz00_7466;
BgL_namez00_2488 = BgL_arg1957z00_2502; 
BgL_locz00_2489 = BgL_arg1958z00_2503; 
BgL_restz00_2490 = BgL_cdrzd2352zd2_2500; 
{ /* Llib/error.scm 1032 */
 obj_t BgL_namez00_2511;
if(
SYMBOLP(BgL_namez00_2488))
{ /* Llib/error.scm 1032 */
 obj_t BgL_arg2328z00_4276;
BgL_arg2328z00_4276 = 
SYMBOL_TO_STRING(BgL_namez00_2488); 
BgL_namez00_2511 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_4276); }  else 
{ /* Llib/error.scm 1032 */
BgL_namez00_2511 = BgL_namez00_2488; } 
{ /* Llib/error.scm 1032 */
 obj_t BgL_marginz00_2512;
BgL_marginz00_2512 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BGl_symbol2891z00zz__errorz00, BgL_restz00_2490); 
{ /* Llib/error.scm 1033 */
 obj_t BgL_fmtz00_2513;
BgL_fmtz00_2513 = 
BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BGl_symbol2893z00zz__errorz00, BgL_restz00_2490); 
{ /* Llib/error.scm 1034 */
 obj_t BgL_nmz00_2514;
{ /* Llib/error.scm 1035 */
 bool_t BgL_test3344z00_7473;
if(
PAIRP(BgL_fmtz00_2513))
{ /* Llib/error.scm 1035 */
 obj_t BgL_tmpz00_7476;
BgL_tmpz00_7476 = 
CDR(BgL_fmtz00_2513); 
BgL_test3344z00_7473 = 
STRINGP(BgL_tmpz00_7476); }  else 
{ /* Llib/error.scm 1035 */
BgL_test3344z00_7473 = ((bool_t)0)
; } 
if(BgL_test3344z00_7473)
{ /* Llib/error.scm 1036 */
 obj_t BgL_arg2008z00_2567;
BgL_arg2008z00_2567 = 
CDR(BgL_fmtz00_2513); 
{ /* Llib/error.scm 1036 */
 obj_t BgL_list2009z00_2568;
BgL_list2009z00_2568 = 
MAKE_YOUNG_PAIR(BgL_namez00_2511, BNIL); 
BgL_nmz00_2514 = 
BGl_formatz00zz__r4_output_6_10_3z00(BgL_arg2008z00_2567, BgL_list2009z00_2568); } }  else 
{ /* Llib/error.scm 1035 */
BgL_nmz00_2514 = BgL_namez00_2511; } } 
{ /* Llib/error.scm 1035 */

{ /* Llib/error.scm 1039 */
 bool_t BgL_test3346z00_7482;
if(
PAIRP(BgL_marginz00_2512))
{ /* Llib/error.scm 1039 */
 obj_t BgL_tmpz00_7485;
BgL_tmpz00_7485 = 
CDR(BgL_marginz00_2512); 
BgL_test3346z00_7482 = 
CHARP(BgL_tmpz00_7485); }  else 
{ /* Llib/error.scm 1039 */
BgL_test3346z00_7482 = ((bool_t)0)
; } 
if(BgL_test3346z00_7482)
{ /* Llib/error.scm 1040 */
 obj_t BgL_arg1969z00_2518;
BgL_arg1969z00_2518 = 
CDR(BgL_marginz00_2512); 
{ /* Llib/error.scm 1040 */
 obj_t BgL_list1970z00_2519;
BgL_list1970z00_2519 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_arg1969z00_2518, BgL_list1970z00_2519); } }  else 
{ /* Llib/error.scm 1041 */
 obj_t BgL_list1971z00_2520;
BgL_list1971z00_2520 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2895z00zz__errorz00, BgL_list1971z00_2520); } } 
if(
(
(long)CINT(BgL_levelz00_2485)<10L))
{ /* Llib/error.scm 1043 */
 obj_t BgL_list1974z00_2524;
BgL_list1974z00_2524 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2888z00zz__errorz00, BgL_list1974z00_2524); }  else 
{ /* Llib/error.scm 1043 */
if(
(
(long)CINT(BgL_levelz00_2485)<100L))
{ /* Llib/error.scm 1044 */
 obj_t BgL_list1976z00_2526;
BgL_list1976z00_2526 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2889z00zz__errorz00, BgL_list1976z00_2526); }  else 
{ /* Llib/error.scm 1044 */
if(
(
(long)CINT(BgL_levelz00_2485)<1000L))
{ /* Llib/error.scm 1045 */
 obj_t BgL_list1978z00_2528;
BgL_list1978z00_2528 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2895z00zz__errorz00, BgL_list1978z00_2528); }  else 
{ /* Llib/error.scm 1045 */BFALSE; } } } 
{ /* Llib/error.scm 1047 */
 obj_t BgL_list1979z00_2529;
BgL_list1979z00_2529 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_levelz00_2485, BgL_list1979z00_2529); } 
{ /* Llib/error.scm 1048 */
 obj_t BgL_list1980z00_2530;
BgL_list1980z00_2530 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2890z00zz__errorz00, BgL_list1980z00_2530); } 
{ /* Llib/error.scm 1050 */
 obj_t BgL_iz00_2531;
{ /* Ieee/string.scm 223 */

BgL_iz00_2531 = 
BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_namez00_2511, 
BCHAR(((unsigned char)'@')), 
BINT(0L)); } 
{ /* Llib/error.scm 1051 */
 bool_t BgL_test3351z00_7515;
if(
CBOOL(BgL_iz00_2531))
{ /* Llib/error.scm 1051 */
if(
(
(long)CINT(BgL_iz00_2531)>0L))
{ /* Llib/error.scm 1051 */
BgL_test3351z00_7515 = 
CBOOL(BgL_locz00_2489)
; }  else 
{ /* Llib/error.scm 1051 */
BgL_test3351z00_7515 = ((bool_t)0)
; } }  else 
{ /* Llib/error.scm 1051 */
BgL_test3351z00_7515 = ((bool_t)0)
; } 
if(BgL_test3351z00_7515)
{ /* Llib/error.scm 1052 */
 obj_t BgL_arg1983z00_2534;
BgL_arg1983z00_2534 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_namez00_2511, 0L, 
(long)CINT(BgL_iz00_2531)); 
{ /* Llib/error.scm 1052 */
 obj_t BgL_list1984z00_2535;
BgL_list1984z00_2535 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_arg1983z00_2534, BgL_list1984z00_2535); } }  else 
{ /* Llib/error.scm 1053 */
 obj_t BgL_list1985z00_2536;
BgL_list1985z00_2536 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_namez00_2511, BgL_list1985z00_2536); } } } 
if(
(BgL_numz00_2486>1L))
{ /* Llib/error.scm 1055 */
{ /* Llib/error.scm 1056 */
 obj_t BgL_list1989z00_2542;
BgL_list1989z00_2542 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2896z00zz__errorz00, BgL_list1989z00_2542); } 
{ /* Llib/error.scm 1057 */
 obj_t BgL_list1990z00_2543;
BgL_list1990z00_2543 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(
BINT(BgL_numz00_2486), BgL_list1990z00_2543); } 
{ /* Llib/error.scm 1058 */
 obj_t BgL_list1991z00_2544;
BgL_list1991z00_2544 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2897z00zz__errorz00, BgL_list1991z00_2544); } }  else 
{ /* Llib/error.scm 1055 */
if(
CBOOL(BgL_locz00_2489))
{ /* Llib/error.scm 1060 */
 obj_t BgL_filez00_2545;
BgL_filez00_2545 = 
BGl_locationzd2linezd2numz00zz__errorz00(BgL_locz00_2489); 
{ /* Llib/error.scm 1061 */
 obj_t BgL_lnumz00_2546; obj_t BgL_lpointz00_2547; obj_t BgL_lstringz00_2548;
{ /* Llib/error.scm 1065 */
 obj_t BgL_tmpz00_4286;
{ /* Llib/error.scm 1065 */
 int BgL_tmpz00_7540;
BgL_tmpz00_7540 = 
(int)(1L); 
BgL_tmpz00_4286 = 
BGL_MVALUES_VAL(BgL_tmpz00_7540); } 
{ /* Llib/error.scm 1065 */
 int BgL_tmpz00_7543;
BgL_tmpz00_7543 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7543, BUNSPEC); } 
BgL_lnumz00_2546 = BgL_tmpz00_4286; } 
{ /* Llib/error.scm 1065 */
 obj_t BgL_tmpz00_4287;
{ /* Llib/error.scm 1065 */
 int BgL_tmpz00_7546;
BgL_tmpz00_7546 = 
(int)(2L); 
BgL_tmpz00_4287 = 
BGL_MVALUES_VAL(BgL_tmpz00_7546); } 
{ /* Llib/error.scm 1065 */
 int BgL_tmpz00_7549;
BgL_tmpz00_7549 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7549, BUNSPEC); } 
BgL_lpointz00_2547 = BgL_tmpz00_4287; } 
{ /* Llib/error.scm 1065 */
 obj_t BgL_tmpz00_4288;
{ /* Llib/error.scm 1065 */
 int BgL_tmpz00_7552;
BgL_tmpz00_7552 = 
(int)(3L); 
BgL_tmpz00_4288 = 
BGL_MVALUES_VAL(BgL_tmpz00_7552); } 
{ /* Llib/error.scm 1065 */
 int BgL_tmpz00_7555;
BgL_tmpz00_7555 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7555, BUNSPEC); } 
BgL_lstringz00_2548 = BgL_tmpz00_4288; } 
{ /* Llib/error.scm 1063 */
 bool_t BgL_test3356z00_7558;
if(
STRINGP(BgL_filez00_2545))
{ /* Llib/error.scm 1063 */
 bool_t BgL_test3358z00_7561;
{ /* Llib/error.scm 1063 */
 long BgL_l1z00_4291;
BgL_l1z00_4291 = 
STRING_LENGTH(BgL_filez00_2545); 
if(
(BgL_l1z00_4291==1L))
{ /* Llib/error.scm 1063 */
 int BgL_arg2321z00_4294;
{ /* Llib/error.scm 1063 */
 char * BgL_auxz00_7567; char * BgL_tmpz00_7565;
BgL_auxz00_7567 = 
BSTRING_TO_STRING(BGl_string2898z00zz__errorz00); 
BgL_tmpz00_7565 = 
BSTRING_TO_STRING(BgL_filez00_2545); 
BgL_arg2321z00_4294 = 
memcmp(BgL_tmpz00_7565, BgL_auxz00_7567, BgL_l1z00_4291); } 
BgL_test3358z00_7561 = 
(
(long)(BgL_arg2321z00_4294)==0L); }  else 
{ /* Llib/error.scm 1063 */
BgL_test3358z00_7561 = ((bool_t)0)
; } } 
if(BgL_test3358z00_7561)
{ /* Llib/error.scm 1063 */
BgL_test3356z00_7558 = ((bool_t)0)
; }  else 
{ /* Llib/error.scm 1063 */
BgL_test3356z00_7558 = ((bool_t)1)
; } }  else 
{ /* Llib/error.scm 1063 */
BgL_test3356z00_7558 = ((bool_t)0)
; } 
if(BgL_test3356z00_7558)
{ /* Llib/error.scm 1063 */
{ /* Llib/error.scm 1064 */
 obj_t BgL_list1995z00_2552;
BgL_list1995z00_2552 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2899z00zz__errorz00, BgL_list1995z00_2552); } 
{ /* Llib/error.scm 1065 */
 obj_t BgL_list1996z00_2553;
BgL_list1996z00_2553 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_filez00_2545, BgL_list1996z00_2553); } }  else 
{ /* Llib/error.scm 1063 */BFALSE; } } 
{ /* Llib/error.scm 1068 */
 bool_t BgL_test3360z00_7576;
if(
INTEGERP(BgL_lpointz00_2547))
{ /* Llib/error.scm 1068 */
BgL_test3360z00_7576 = 
(
(long)CINT(BgL_lpointz00_2547)==0L)
; }  else 
{ /* Llib/error.scm 1068 */
BgL_test3360z00_7576 = ((bool_t)0)
; } 
if(BgL_test3360z00_7576)
{ /* Llib/error.scm 1068 */BUNSPEC; }  else 
{ /* Llib/error.scm 1068 */
if(
CBOOL(BgL_lnumz00_2546))
{ /* Llib/error.scm 1070 */
{ /* Llib/error.scm 1071 */
 obj_t BgL_list1999z00_2558;
BgL_list1999z00_2558 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2832z00zz__errorz00, BgL_list1999z00_2558); } 
{ /* Llib/error.scm 1072 */
 obj_t BgL_list2000z00_2559;
BgL_list2000z00_2559 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_lnumz00_2546, BgL_list2000z00_2559); } }  else 
{ /* Llib/error.scm 1070 */
if(
CBOOL(BgL_lpointz00_2547))
{ /* Llib/error.scm 1073 */
{ /* Llib/error.scm 1074 */
 obj_t BgL_list2001z00_2560;
BgL_list2001z00_2560 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2900z00zz__errorz00, BgL_list2001z00_2560); } 
{ /* Llib/error.scm 1075 */
 obj_t BgL_list2002z00_2561;
BgL_list2002z00_2561 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_lpointz00_2547, BgL_list2002z00_2561); } }  else 
{ /* Llib/error.scm 1073 */BFALSE; } } } } } }  else 
{ /* Llib/error.scm 1059 */BFALSE; } } 
{ /* Llib/error.scm 1076 */
 obj_t BgL_list2003z00_2563;
BgL_list2003z00_2563 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2003z00_2563); } } } } } } 
BgL_tmpz00_7466 = 
(
(long)CINT(BgL_levelz00_2485)+1L); 
return 
BINT(BgL_tmpz00_7466);} }  else 
{ /* Llib/error.scm 1099 */
 long BgL_tmpz00_7598;
BgL_tagzd2338zd2_2495:
{ /* Llib/error.scm 1096 */
 obj_t BgL_list2027z00_2588;
BgL_list2027z00_2588 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2886z00zz__errorz00, BgL_list2027z00_2588); } 
{ /* Llib/error.scm 1097 */
 obj_t BgL_list2028z00_2589;
BgL_list2028z00_2589 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_framez00_2484, BgL_list2028z00_2589); } 
{ /* Llib/error.scm 1098 */
 obj_t BgL_list2029z00_2590;
BgL_list2029z00_2590 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2029z00_2590); } 
BgL_tmpz00_7598 = 
(
(long)CINT(BgL_levelz00_2485)+1L); 
return 
BINT(BgL_tmpz00_7598);} } }  else 
{ /* Llib/error.scm 1099 */
if(
NULLP(BgL_cdrzd2347zd2_2498))
{ /* Llib/error.scm 1099 */
 obj_t BgL_arg1962z00_2507;
BgL_arg1962z00_2507 = 
CAR(
((obj_t)BgL_framez00_2484)); 
{ /* Llib/error.scm 1099 */
 long BgL_tmpz00_7612;
BgL_namez00_2492 = BgL_arg1962z00_2507; 
if(
(
(long)CINT(BgL_levelz00_2485)<10L))
{ /* Llib/error.scm 1080 */
 obj_t BgL_list2013z00_2573;
BgL_list2013z00_2573 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2887z00zz__errorz00, BgL_list2013z00_2573); }  else 
{ /* Llib/error.scm 1080 */
if(
(
(long)CINT(BgL_levelz00_2485)<100L))
{ /* Llib/error.scm 1081 */
 obj_t BgL_list2015z00_2575;
BgL_list2015z00_2575 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2888z00zz__errorz00, BgL_list2015z00_2575); }  else 
{ /* Llib/error.scm 1081 */
if(
(
(long)CINT(BgL_levelz00_2485)<1000L))
{ /* Llib/error.scm 1082 */
 obj_t BgL_list2017z00_2577;
BgL_list2017z00_2577 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BGl_string2889z00zz__errorz00, BgL_list2017z00_2577); }  else 
{ /* Llib/error.scm 1082 */BFALSE; } } } 
{ /* Llib/error.scm 1084 */
 obj_t BgL_list2018z00_2578;
BgL_list2018z00_2578 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_levelz00_2485, BgL_list2018z00_2578); } 
{ /* Llib/error.scm 1085 */
 obj_t BgL_arg2019z00_2579;
{ /* Llib/error.scm 1085 */
 bool_t BgL_test3368z00_7630;
if(
SYMBOLP(BgL_namez00_2492))
{ /* Llib/error.scm 1085 */
BgL_test3368z00_7630 = ((bool_t)1)
; }  else 
{ /* Llib/error.scm 1085 */
BgL_test3368z00_7630 = 
STRINGP(BgL_namez00_2492)
; } 
if(BgL_test3368z00_7630)
{ /* Llib/error.scm 1085 */
BgL_arg2019z00_2579 = BGl_string2890z00zz__errorz00; }  else 
{ /* Llib/error.scm 1085 */
BgL_arg2019z00_2579 = BGl_string2886z00zz__errorz00; } } 
{ /* Llib/error.scm 1085 */
 obj_t BgL_list2020z00_2580;
BgL_list2020z00_2580 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_arg2019z00_2579, BgL_list2020z00_2580); } } 
{ /* Llib/error.scm 1086 */
 obj_t BgL_list2023z00_2584;
BgL_list2023z00_2584 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_namez00_2492, BgL_list2023z00_2584); } 
{ /* Llib/error.scm 1087 */
 obj_t BgL_list2024z00_2585;
BgL_list2024z00_2585 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2024z00_2585); } 
BgL_tmpz00_7612 = 
(
(long)CINT(BgL_levelz00_2485)+1L); 
return 
BINT(BgL_tmpz00_7612);} }  else 
{ /* Llib/error.scm 1099 */
 long BgL_tmpz00_7643;
goto BgL_tagzd2338zd2_2495;
return 
BINT(BgL_tmpz00_7643);} } }  else 
{ /* Llib/error.scm 1099 */
if(
STRINGP(BgL_framez00_2484))
{ /* Llib/error.scm 1099 */
{ /* Llib/error.scm 1091 */
 obj_t BgL_list2025z00_2586;
BgL_list2025z00_2586 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_displayz00zz__r4_output_6_10_3z00(BgL_framez00_2484, BgL_list2025z00_2586); } 
{ /* Llib/error.scm 1092 */
 obj_t BgL_list2026z00_2587;
BgL_list2026z00_2587 = 
MAKE_YOUNG_PAIR(BgL_portz00_5262, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2026z00_2587); } 
return BgL_levelz00_2485;}  else 
{ /* Llib/error.scm 1099 */
 long BgL_tmpz00_7651;
goto BgL_tagzd2338zd2_2495;
return 
BINT(BgL_tmpz00_7651);} } } } 

}



/* display-trace-stack-source */
BGL_EXPORTED_DEF obj_t BGl_displayzd2tracezd2stackzd2sourcezd2zz__errorz00(obj_t BgL_stackz00_118, obj_t BgL_portz00_119)
{
{ /* Llib/error.scm 1126 */
{ 
 obj_t BgL_stackz00_2595;
BgL_stackz00_2595 = BgL_stackz00_118; 
BgL_zc3z04anonymousza32030ze3z87_2596:
if(
PAIRP(BgL_stackz00_2595))
{ 
 obj_t BgL_namez00_2598; obj_t BgL_locz00_2599; obj_t BgL_restz00_2600;
{ /* Llib/error.scm 1139 */
 obj_t BgL_ezd2371zd2_2603;
BgL_ezd2371zd2_2603 = 
CAR(BgL_stackz00_2595); 
if(
PAIRP(BgL_ezd2371zd2_2603))
{ /* Llib/error.scm 1139 */
 obj_t BgL_cdrzd2379zd2_2605;
BgL_cdrzd2379zd2_2605 = 
CDR(BgL_ezd2371zd2_2603); 
if(
PAIRP(BgL_cdrzd2379zd2_2605))
{ /* Llib/error.scm 1139 */
 obj_t BgL_cdrzd2384zd2_2607;
BgL_cdrzd2384zd2_2607 = 
CDR(BgL_cdrzd2379zd2_2605); 
{ /* Llib/error.scm 1139 */
 bool_t BgL_test3374z00_7662;
if(
BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_cdrzd2384zd2_2607))
{ /* Llib/error.scm 1013 */
 obj_t BgL_list1935z00_4358;
BgL_list1935z00_4358 = 
MAKE_YOUNG_PAIR(BgL_cdrzd2384zd2_2607, BNIL); 
BgL_test3374z00_7662 = 
CBOOL(
BGl_everyz00zz__r4_pairs_and_lists_6_3z00(BGl_pairzf3zd2envz21zz__r4_pairs_and_lists_6_3z00, BgL_list1935z00_4358)); }  else 
{ /* Llib/error.scm 1013 */
BgL_test3374z00_7662 = ((bool_t)0)
; } 
if(BgL_test3374z00_7662)
{ /* Llib/error.scm 1139 */
BgL_namez00_2598 = 
CAR(BgL_ezd2371zd2_2603); 
BgL_locz00_2599 = 
CAR(BgL_cdrzd2379zd2_2605); 
BgL_restz00_2600 = BgL_cdrzd2384zd2_2607; 
{ /* Llib/error.scm 1142 */
 obj_t BgL_filez00_2611;
BgL_filez00_2611 = 
BGl_locationzd2linezd2numz00zz__errorz00(BgL_locz00_2599); 
{ /* Llib/error.scm 1143 */
 obj_t BgL_lnumz00_2612; obj_t BgL_lpointz00_2613; obj_t BgL_lstringz00_2614;
{ /* Llib/error.scm 1145 */
 obj_t BgL_tmpz00_4336;
{ /* Llib/error.scm 1145 */
 int BgL_tmpz00_7669;
BgL_tmpz00_7669 = 
(int)(1L); 
BgL_tmpz00_4336 = 
BGL_MVALUES_VAL(BgL_tmpz00_7669); } 
{ /* Llib/error.scm 1145 */
 int BgL_tmpz00_7672;
BgL_tmpz00_7672 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7672, BUNSPEC); } 
BgL_lnumz00_2612 = BgL_tmpz00_4336; } 
{ /* Llib/error.scm 1145 */
 obj_t BgL_tmpz00_4337;
{ /* Llib/error.scm 1145 */
 int BgL_tmpz00_7675;
BgL_tmpz00_7675 = 
(int)(2L); 
BgL_tmpz00_4337 = 
BGL_MVALUES_VAL(BgL_tmpz00_7675); } 
{ /* Llib/error.scm 1145 */
 int BgL_tmpz00_7678;
BgL_tmpz00_7678 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7678, BUNSPEC); } 
BgL_lpointz00_2613 = BgL_tmpz00_4337; } 
{ /* Llib/error.scm 1145 */
 obj_t BgL_tmpz00_4338;
{ /* Llib/error.scm 1145 */
 int BgL_tmpz00_7681;
BgL_tmpz00_7681 = 
(int)(3L); 
BgL_tmpz00_4338 = 
BGL_MVALUES_VAL(BgL_tmpz00_7681); } 
{ /* Llib/error.scm 1145 */
 int BgL_tmpz00_7684;
BgL_tmpz00_7684 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_7684, BUNSPEC); } 
BgL_lstringz00_2614 = BgL_tmpz00_4338; } 
{ /* Llib/error.scm 1145 */
 bool_t BgL_test3376z00_7687;
if(
STRINGP(BgL_filez00_2611))
{ /* Llib/error.scm 1145 */
BgL_test3376z00_7687 = 
STRINGP(BgL_lstringz00_2614)
; }  else 
{ /* Llib/error.scm 1145 */
BgL_test3376z00_7687 = ((bool_t)0)
; } 
if(BgL_test3376z00_7687)
{ /* Llib/error.scm 1129 */
 obj_t BgL_tabsz00_4339;
if(
(
(long)CINT(BgL_lpointz00_2613)>0L))
{ /* Llib/error.scm 1129 */
BgL_tabsz00_4339 = 
make_string(
(long)CINT(BgL_lpointz00_2613), ((unsigned char)' ')); }  else 
{ /* Llib/error.scm 1129 */
BgL_tabsz00_4339 = BGl_string2866z00zz__errorz00; } 
{ /* Llib/error.scm 1129 */
 long BgL_lz00_4341;
BgL_lz00_4341 = 
STRING_LENGTH(BgL_lstringz00_2614); 
{ /* Llib/error.scm 1130 */
 obj_t BgL_ncolz00_4342;
if(
(
(long)CINT(BgL_lpointz00_2613)>=BgL_lz00_4341))
{ /* Llib/error.scm 1131 */
BgL_ncolz00_4342 = 
BINT(BgL_lz00_4341); }  else 
{ /* Llib/error.scm 1131 */
BgL_ncolz00_4342 = BgL_lpointz00_2613; } 
{ /* Llib/error.scm 1131 */

BGl_fixzd2tabulationz12zc0zz__errorz00(BgL_ncolz00_4342, BgL_lstringz00_2614, BgL_tabsz00_4339); 
return 
BGl_printzd2cursorzd2zz__errorz00(BgL_filez00_2611, BgL_lnumz00_2612, BgL_lpointz00_2613, BgL_lstringz00_2614, BgL_tabsz00_4339);} } } }  else 
{ /* Llib/error.scm 1147 */
 bool_t BgL_test3380z00_7703;
if(
STRINGP(BgL_filez00_2611))
{ /* Llib/error.scm 1147 */
BgL_test3380z00_7703 = 
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_lpointz00_2613)
; }  else 
{ /* Llib/error.scm 1147 */
BgL_test3380z00_7703 = ((bool_t)0)
; } 
if(BgL_test3380z00_7703)
{ /* Llib/error.scm 1147 */
if(
BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_lnumz00_2612))
{ /* Llib/error.scm 1149 */
 obj_t BgL_arg2044z00_2620; obj_t BgL_arg2045z00_2621;
{ /* Llib/error.scm 1149 */
 obj_t BgL_tmpz00_7709;
BgL_tmpz00_7709 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2044z00_2620 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7709); } 
{ /* Llib/error.scm 1151 */

BgL_arg2045z00_2621 = 
BGl_filenamezd2forzd2errorz00zz__errorz00(BgL_filez00_2611, 255L); } 
{ /* Llib/error.scm 1149 */
 obj_t BgL_list2046z00_2622;
{ /* Llib/error.scm 1149 */
 obj_t BgL_arg2047z00_2623;
{ /* Llib/error.scm 1149 */
 obj_t BgL_arg2048z00_2624;
BgL_arg2048z00_2624 = 
MAKE_YOUNG_PAIR(BgL_lpointz00_2613, BNIL); 
BgL_arg2047z00_2623 = 
MAKE_YOUNG_PAIR(BgL_lnumz00_2612, BgL_arg2048z00_2624); } 
BgL_list2046z00_2622 = 
MAKE_YOUNG_PAIR(BgL_arg2045z00_2621, BgL_arg2047z00_2623); } 
return 
BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_arg2044z00_2620, BGl_string2901z00zz__errorz00, BgL_list2046z00_2622);} }  else 
{ /* Llib/error.scm 1154 */
 obj_t BgL_arg2049z00_2627; obj_t BgL_arg2050z00_2628;
{ /* Llib/error.scm 1154 */
 obj_t BgL_tmpz00_7717;
BgL_tmpz00_7717 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2049z00_2627 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7717); } 
{ /* Llib/error.scm 1156 */

BgL_arg2050z00_2628 = 
BGl_filenamezd2forzd2errorz00zz__errorz00(BgL_filez00_2611, 255L); } 
{ /* Llib/error.scm 1154 */
 obj_t BgL_list2051z00_2629;
{ /* Llib/error.scm 1154 */
 obj_t BgL_arg2052z00_2630;
BgL_arg2052z00_2630 = 
MAKE_YOUNG_PAIR(BgL_lpointz00_2613, BNIL); 
BgL_list2051z00_2629 = 
MAKE_YOUNG_PAIR(BgL_arg2050z00_2628, BgL_arg2052z00_2630); } 
return 
BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_arg2049z00_2627, BGl_string2902z00zz__errorz00, BgL_list2051z00_2629);} } }  else 
{ /* Llib/error.scm 1159 */
 obj_t BgL_arg2055z00_2633;
BgL_arg2055z00_2633 = 
CDR(
((obj_t)BgL_stackz00_2595)); 
{ 
 obj_t BgL_stackz00_7726;
BgL_stackz00_7726 = BgL_arg2055z00_2633; 
BgL_stackz00_2595 = BgL_stackz00_7726; 
goto BgL_zc3z04anonymousza32030ze3z87_2596;} } } } } } }  else 
{ 
 obj_t BgL_stackz00_7729;
BgL_stackz00_7729 = 
CDR(BgL_stackz00_2595); 
BgL_stackz00_2595 = BgL_stackz00_7729; 
goto BgL_zc3z04anonymousza32030ze3z87_2596;} } }  else 
{ 
 obj_t BgL_stackz00_7731;
BgL_stackz00_7731 = 
CDR(BgL_stackz00_2595); 
BgL_stackz00_2595 = BgL_stackz00_7731; 
goto BgL_zc3z04anonymousza32030ze3z87_2596;} }  else 
{ 
 obj_t BgL_stackz00_7733;
BgL_stackz00_7733 = 
CDR(BgL_stackz00_2595); 
BgL_stackz00_2595 = BgL_stackz00_7733; 
goto BgL_zc3z04anonymousza32030ze3z87_2596;} } }  else 
{ /* Llib/error.scm 1138 */
return BFALSE;} } } 

}



/* &display-trace-stack-source */
obj_t BGl_z62displayzd2tracezd2stackzd2sourcezb0zz__errorz00(obj_t BgL_envz00_5222, obj_t BgL_stackz00_5223, obj_t BgL_portz00_5224)
{
{ /* Llib/error.scm 1126 */
{ /* Llib/error.scm 1138 */
 obj_t BgL_auxz00_7735;
if(
OUTPUT_PORTP(BgL_portz00_5224))
{ /* Llib/error.scm 1138 */
BgL_auxz00_7735 = BgL_portz00_5224
; }  else 
{ 
 obj_t BgL_auxz00_7738;
BgL_auxz00_7738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(44711L), BGl_string2903z00zz__errorz00, BGl_string2884z00zz__errorz00, BgL_portz00_5224); 
FAILURE(BgL_auxz00_7738,BFALSE,BFALSE);} 
return 
BGl_displayzd2tracezd2stackzd2sourcezd2zz__errorz00(BgL_stackz00_5223, BgL_auxz00_7735);} } 

}



/* dump-trace-stack */
BGL_EXPORTED_DEF obj_t BGl_dumpzd2tracezd2stackz00zz__errorz00(obj_t BgL_portz00_120, obj_t BgL_depthz00_121)
{
{ /* Llib/error.scm 1166 */
{ /* Llib/error.scm 1167 */
 obj_t BgL_arg2060z00_4367;
BgL_arg2060z00_4367 = 
BGl_getzd2tracezd2stackz00zz__errorz00(BgL_depthz00_121); 
{ /* Llib/error.scm 273 */

return 
BGl_displayzd2tracezd2stackz00zz__errorz00(BgL_arg2060z00_4367, BgL_portz00_120, 
BINT(1L));} } } 

}



/* &dump-trace-stack */
obj_t BGl_z62dumpzd2tracezd2stackz62zz__errorz00(obj_t BgL_envz00_5225, obj_t BgL_portz00_5226, obj_t BgL_depthz00_5227)
{
{ /* Llib/error.scm 1166 */
return 
BGl_dumpzd2tracezd2stackz00zz__errorz00(BgL_portz00_5226, BgL_depthz00_5227);} 

}



/* fix-tabulation! */
obj_t BGl_fixzd2tabulationz12zc0zz__errorz00(obj_t BgL_markerz00_122, obj_t BgL_srcz00_123, obj_t BgL_dstz00_124)
{
{ /* Llib/error.scm 1172 */
{ /* Llib/error.scm 1173 */
 long BgL_g1136z00_2653;
BgL_g1136z00_2653 = 
(
(long)CINT(BgL_markerz00_122)-1L); 
{ 
 long BgL_readz00_2655;
BgL_readz00_2655 = BgL_g1136z00_2653; 
BgL_zc3z04anonymousza32061ze3z87_2656:
if(
(BgL_readz00_2655==-1L))
{ /* Llib/error.scm 1175 */
return BGl_symbol2904z00zz__errorz00;}  else 
{ /* Llib/error.scm 1175 */
if(
(
STRING_REF(
((obj_t)BgL_srcz00_123), BgL_readz00_2655)==((unsigned char)9)))
{ /* Llib/error.scm 1177 */
STRING_SET(BgL_dstz00_124, BgL_readz00_2655, ((unsigned char)9)); 
{ 
 long BgL_readz00_7756;
BgL_readz00_7756 = 
(BgL_readz00_2655-1L); 
BgL_readz00_2655 = BgL_readz00_7756; 
goto BgL_zc3z04anonymousza32061ze3z87_2656;} }  else 
{ 
 long BgL_readz00_7758;
BgL_readz00_7758 = 
(BgL_readz00_2655-1L); 
BgL_readz00_2655 = BgL_readz00_7758; 
goto BgL_zc3z04anonymousza32061ze3z87_2656;} } } } } 

}



/* print-cursor */
obj_t BGl_printzd2cursorzd2zz__errorz00(obj_t BgL_fnamez00_125, obj_t BgL_linez00_126, obj_t BgL_charz00_127, obj_t BgL_stringz00_128, obj_t BgL_spacezd2stringzd2_129)
{
{ /* Llib/error.scm 1186 */
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2069z00_2664; obj_t BgL_arg2070z00_2665;
{ /* Llib/error.scm 1187 */
 obj_t BgL_tmpz00_7760;
BgL_tmpz00_7760 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2069z00_2664 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7760); } 
{ /* Llib/error.scm 1188 */

BgL_arg2070z00_2665 = 
BGl_filenamezd2forzd2errorz00zz__errorz00(BgL_fnamez00_125, 255L); } 
{ /* Llib/error.scm 1187 */
 obj_t BgL_list2071z00_2666;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2072z00_2667;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2074z00_2668;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2075z00_2669;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2076z00_2670;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2077z00_2671;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2078z00_2672;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2079z00_2673;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2080z00_2674;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2081z00_2675;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2082z00_2676;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2083z00_2677;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2084z00_2678;
{ /* Llib/error.scm 1187 */
 obj_t BgL_arg2086z00_2679;
BgL_arg2086z00_2679 = 
MAKE_YOUNG_PAIR(BGl_string2906z00zz__errorz00, BNIL); 
BgL_arg2084z00_2678 = 
MAKE_YOUNG_PAIR(BgL_spacezd2stringzd2_129, BgL_arg2086z00_2679); } 
BgL_arg2083z00_2677 = 
MAKE_YOUNG_PAIR(BGl_string2907z00zz__errorz00, BgL_arg2084z00_2678); } 
BgL_arg2082z00_2676 = 
MAKE_YOUNG_PAIR(
BCHAR(((unsigned char)10)), BgL_arg2083z00_2677); } 
BgL_arg2081z00_2675 = 
MAKE_YOUNG_PAIR(BgL_stringz00_128, BgL_arg2082z00_2676); } 
BgL_arg2080z00_2674 = 
MAKE_YOUNG_PAIR(BGl_string2907z00zz__errorz00, BgL_arg2081z00_2675); } 
BgL_arg2079z00_2673 = 
MAKE_YOUNG_PAIR(
BCHAR(((unsigned char)10)), BgL_arg2080z00_2674); } 
BgL_arg2078z00_2672 = 
MAKE_YOUNG_PAIR(BGl_string2832z00zz__errorz00, BgL_arg2079z00_2673); } 
BgL_arg2077z00_2671 = 
MAKE_YOUNG_PAIR(BgL_charz00_127, BgL_arg2078z00_2672); } 
BgL_arg2076z00_2670 = 
MAKE_YOUNG_PAIR(BGl_string2908z00zz__errorz00, BgL_arg2077z00_2671); } 
BgL_arg2075z00_2669 = 
MAKE_YOUNG_PAIR(BgL_linez00_126, BgL_arg2076z00_2670); } 
BgL_arg2074z00_2668 = 
MAKE_YOUNG_PAIR(BGl_string2909z00zz__errorz00, BgL_arg2075z00_2669); } 
BgL_arg2072z00_2667 = 
MAKE_YOUNG_PAIR(BgL_arg2070z00_2665, BgL_arg2074z00_2668); } 
BgL_list2071z00_2666 = 
MAKE_YOUNG_PAIR(BGl_string2864z00zz__errorz00, BgL_arg2072z00_2667); } 
return 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg2069z00_2664, BgL_list2071z00_2666);} } } 

}



/* relative-file-name */
obj_t BGl_relativezd2filezd2namez00zz__errorz00(obj_t BgL_fnamez00_130)
{
{ /* Llib/error.scm 1201 */
{ /* Llib/error.scm 1202 */
 obj_t BgL_pwdz00_2682; obj_t BgL_dnamez00_2683;
BgL_pwdz00_2682 = 
BGl_pwdz00zz__osz00(); 
BgL_dnamez00_2683 = 
BGl_dirnamez00zz__osz00(BgL_fnamez00_130); 
{ /* Llib/error.scm 1204 */
 bool_t BgL_test3386z00_7783;
if(
STRINGP(BgL_pwdz00_2682))
{ /* Llib/error.scm 1205 */
 bool_t BgL_test3388z00_7786;
{ /* Llib/error.scm 1205 */
 long BgL_l1z00_4385;
BgL_l1z00_4385 = 
STRING_LENGTH(BgL_dnamez00_2683); 
if(
(BgL_l1z00_4385==1L))
{ /* Llib/error.scm 1205 */
 int BgL_arg2321z00_4388;
{ /* Llib/error.scm 1205 */
 char * BgL_auxz00_7792; char * BgL_tmpz00_7790;
BgL_auxz00_7792 = 
BSTRING_TO_STRING(BGl_string2898z00zz__errorz00); 
BgL_tmpz00_7790 = 
BSTRING_TO_STRING(BgL_dnamez00_2683); 
BgL_arg2321z00_4388 = 
memcmp(BgL_tmpz00_7790, BgL_auxz00_7792, BgL_l1z00_4385); } 
BgL_test3388z00_7786 = 
(
(long)(BgL_arg2321z00_4388)==0L); }  else 
{ /* Llib/error.scm 1205 */
BgL_test3388z00_7786 = ((bool_t)0)
; } } 
if(BgL_test3388z00_7786)
{ /* Llib/error.scm 1205 */
BgL_test3386z00_7783 = ((bool_t)1)
; }  else 
{ /* Llib/error.scm 1205 */
if(
(
STRING_REF(
((obj_t)BgL_fnamez00_130), 0L)==((unsigned char)'/')))
{ /* Llib/error.scm 1206 */
BgL_test3386z00_7783 = ((bool_t)0)
; }  else 
{ /* Llib/error.scm 1206 */
BgL_test3386z00_7783 = ((bool_t)1)
; } } }  else 
{ /* Llib/error.scm 1204 */
BgL_test3386z00_7783 = ((bool_t)1)
; } 
if(BgL_test3386z00_7783)
{ /* Llib/error.scm 1204 */
return BgL_fnamez00_130;}  else 
{ /* Llib/error.scm 1209 */
 obj_t BgL_originalzd2cmpzd2pathz00_2690;
BgL_originalzd2cmpzd2pathz00_2690 = 
BGl_dirnamezd2ze3listz31zz__errorz00(BgL_dnamez00_2683); 
{ /* Llib/error.scm 1210 */
 obj_t BgL_g1137z00_2691;
BgL_g1137z00_2691 = 
BGl_dirnamezd2ze3listz31zz__errorz00(BgL_pwdz00_2682); 
{ 
 obj_t BgL_cmpzd2pathzd2_2693; obj_t BgL_curzd2pathzd2_2694;
BgL_cmpzd2pathzd2_2693 = BgL_originalzd2cmpzd2pathz00_2690; 
BgL_curzd2pathzd2_2694 = BgL_g1137z00_2691; 
BgL_zc3z04anonymousza32093ze3z87_2695:
if(
NULLP(BgL_cmpzd2pathzd2_2693))
{ /* Llib/error.scm 1213 */
if(
NULLP(BgL_curzd2pathzd2_2694))
{ /* Llib/error.scm 1214 */
return 
BGl_basenamez00zz__osz00(BgL_fnamez00_130);}  else 
{ /* Llib/error.scm 1217 */
 long BgL_g1138z00_2698; obj_t BgL_g1139z00_2699;
BgL_g1138z00_2698 = 
bgl_list_length(BgL_curzd2pathzd2_2694); 
BgL_g1139z00_2699 = 
BGl_basenamez00zz__osz00(BgL_fnamez00_130); 
{ 
 long BgL_lenz00_2701; obj_t BgL_resz00_2702;
BgL_lenz00_2701 = BgL_g1138z00_2698; 
BgL_resz00_2702 = BgL_g1139z00_2699; 
BgL_zc3z04anonymousza32096ze3z87_2703:
if(
(BgL_lenz00_2701==0L))
{ /* Llib/error.scm 1219 */
return BgL_resz00_2702;}  else 
{ /* Llib/error.scm 1221 */
 long BgL_arg2098z00_2705; obj_t BgL_arg2099z00_2706;
BgL_arg2098z00_2705 = 
(BgL_lenz00_2701-1L); 
{ /* Llib/error.scm 1221 */
 obj_t BgL_list2100z00_2707;
{ /* Llib/error.scm 1221 */
 obj_t BgL_arg2101z00_2708;
BgL_arg2101z00_2708 = 
MAKE_YOUNG_PAIR(BgL_resz00_2702, BNIL); 
BgL_list2100z00_2707 = 
MAKE_YOUNG_PAIR(BGl_string2910z00zz__errorz00, BgL_arg2101z00_2708); } 
BgL_arg2099z00_2706 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2100z00_2707); } 
{ 
 obj_t BgL_resz00_7817; long BgL_lenz00_7816;
BgL_lenz00_7816 = BgL_arg2098z00_2705; 
BgL_resz00_7817 = BgL_arg2099z00_2706; 
BgL_resz00_2702 = BgL_resz00_7817; 
BgL_lenz00_2701 = BgL_lenz00_7816; 
goto BgL_zc3z04anonymousza32096ze3z87_2703;} } } } }  else 
{ /* Llib/error.scm 1213 */
if(
NULLP(BgL_curzd2pathzd2_2694))
{ /* Llib/error.scm 1223 */
 obj_t BgL_g1140z00_2711; obj_t BgL_g1141z00_2712;
BgL_g1140z00_2711 = 
bgl_reverse_bang(BgL_cmpzd2pathzd2_2693); 
BgL_g1141z00_2712 = 
BGl_basenamez00zz__osz00(BgL_fnamez00_130); 
{ 
 obj_t BgL_pathz00_2714; obj_t BgL_resz00_2715;
BgL_pathz00_2714 = BgL_g1140z00_2711; 
BgL_resz00_2715 = BgL_g1141z00_2712; 
BgL_zc3z04anonymousza32103ze3z87_2716:
if(
NULLP(BgL_pathz00_2714))
{ /* Llib/error.scm 1225 */
return BgL_resz00_2715;}  else 
{ /* Llib/error.scm 1227 */
 obj_t BgL_arg2105z00_2718; obj_t BgL_arg2106z00_2719;
BgL_arg2105z00_2718 = 
CDR(
((obj_t)BgL_pathz00_2714)); 
{ /* Llib/error.scm 1228 */
 obj_t BgL_arg2107z00_2720;
BgL_arg2107z00_2720 = 
CAR(
((obj_t)BgL_pathz00_2714)); 
{ /* Llib/error.scm 1228 */
 obj_t BgL_list2108z00_2721;
{ /* Llib/error.scm 1228 */
 obj_t BgL_arg2109z00_2722;
{ /* Llib/error.scm 1228 */
 obj_t BgL_arg2110z00_2723;
BgL_arg2110z00_2723 = 
MAKE_YOUNG_PAIR(BgL_resz00_2715, BNIL); 
BgL_arg2109z00_2722 = 
MAKE_YOUNG_PAIR(BGl_string2911z00zz__errorz00, BgL_arg2110z00_2723); } 
BgL_list2108z00_2721 = 
MAKE_YOUNG_PAIR(BgL_arg2107z00_2720, BgL_arg2109z00_2722); } 
BgL_arg2106z00_2719 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2108z00_2721); } } 
{ 
 obj_t BgL_resz00_7833; obj_t BgL_pathz00_7832;
BgL_pathz00_7832 = BgL_arg2105z00_2718; 
BgL_resz00_7833 = BgL_arg2106z00_2719; 
BgL_resz00_2715 = BgL_resz00_7833; 
BgL_pathz00_2714 = BgL_pathz00_7832; 
goto BgL_zc3z04anonymousza32103ze3z87_2716;} } } }  else 
{ /* Llib/error.scm 1229 */
 bool_t BgL_test3396z00_7834;
{ /* Llib/error.scm 1229 */
 obj_t BgL_arg2134z00_2758; obj_t BgL_arg2135z00_2759;
BgL_arg2134z00_2758 = 
CAR(
((obj_t)BgL_curzd2pathzd2_2694)); 
BgL_arg2135z00_2759 = 
CAR(
((obj_t)BgL_cmpzd2pathzd2_2693)); 
{ /* Llib/error.scm 1229 */
 long BgL_l1z00_4405;
BgL_l1z00_4405 = 
STRING_LENGTH(
((obj_t)BgL_arg2134z00_2758)); 
if(
(BgL_l1z00_4405==
STRING_LENGTH(
((obj_t)BgL_arg2135z00_2759))))
{ /* Llib/error.scm 1229 */
 int BgL_arg2321z00_4408;
{ /* Llib/error.scm 1229 */
 char * BgL_auxz00_7848; char * BgL_tmpz00_7845;
BgL_auxz00_7848 = 
BSTRING_TO_STRING(
((obj_t)BgL_arg2135z00_2759)); 
BgL_tmpz00_7845 = 
BSTRING_TO_STRING(
((obj_t)BgL_arg2134z00_2758)); 
BgL_arg2321z00_4408 = 
memcmp(BgL_tmpz00_7845, BgL_auxz00_7848, BgL_l1z00_4405); } 
BgL_test3396z00_7834 = 
(
(long)(BgL_arg2321z00_4408)==0L); }  else 
{ /* Llib/error.scm 1229 */
BgL_test3396z00_7834 = ((bool_t)0)
; } } } 
if(BgL_test3396z00_7834)
{ /* Llib/error.scm 1230 */
 obj_t BgL_arg2114z00_2728; obj_t BgL_arg2115z00_2729;
BgL_arg2114z00_2728 = 
CDR(
((obj_t)BgL_cmpzd2pathzd2_2693)); 
BgL_arg2115z00_2729 = 
CDR(
((obj_t)BgL_curzd2pathzd2_2694)); 
{ 
 obj_t BgL_curzd2pathzd2_7859; obj_t BgL_cmpzd2pathzd2_7858;
BgL_cmpzd2pathzd2_7858 = BgL_arg2114z00_2728; 
BgL_curzd2pathzd2_7859 = BgL_arg2115z00_2729; 
BgL_curzd2pathzd2_2694 = BgL_curzd2pathzd2_7859; 
BgL_cmpzd2pathzd2_2693 = BgL_cmpzd2pathzd2_7858; 
goto BgL_zc3z04anonymousza32093ze3z87_2695;} }  else 
{ /* Llib/error.scm 1232 */
 obj_t BgL_g1142z00_2730; obj_t BgL_g1143z00_2731;
BgL_g1142z00_2730 = 
bgl_reverse(BgL_cmpzd2pathzd2_2693); 
BgL_g1143z00_2731 = 
BGl_basenamez00zz__osz00(BgL_fnamez00_130); 
{ 
 obj_t BgL_pathz00_2733; obj_t BgL_resz00_2734;
BgL_pathz00_2733 = BgL_g1142z00_2730; 
BgL_resz00_2734 = BgL_g1143z00_2731; 
BgL_zc3z04anonymousza32116ze3z87_2735:
if(
NULLP(BgL_pathz00_2733))
{ /* Llib/error.scm 1234 */
if(
(BgL_cmpzd2pathzd2_2693==BgL_originalzd2cmpzd2pathz00_2690))
{ /* Llib/error.scm 1236 */
 obj_t BgL_list2119z00_2738;
{ /* Llib/error.scm 1236 */
 obj_t BgL_arg2120z00_2739;
BgL_arg2120z00_2739 = 
MAKE_YOUNG_PAIR(BgL_resz00_2734, BNIL); 
BgL_list2119z00_2738 = 
MAKE_YOUNG_PAIR(BGl_string2911z00zz__errorz00, BgL_arg2120z00_2739); } 
BGL_TAIL return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2119z00_2738);}  else 
{ /* Llib/error.scm 1237 */
 long BgL_g1144z00_2740;
BgL_g1144z00_2740 = 
bgl_list_length(BgL_curzd2pathzd2_2694); 
{ 
 long BgL_lenz00_2742; obj_t BgL_resz00_2743;
BgL_lenz00_2742 = BgL_g1144z00_2740; 
BgL_resz00_2743 = BgL_resz00_2734; 
BgL_zc3z04anonymousza32121ze3z87_2744:
if(
(BgL_lenz00_2742==0L))
{ /* Llib/error.scm 1239 */
return BgL_resz00_2743;}  else 
{ /* Llib/error.scm 1241 */
 long BgL_arg2123z00_2746; obj_t BgL_arg2124z00_2747;
BgL_arg2123z00_2746 = 
(BgL_lenz00_2742-1L); 
{ /* Llib/error.scm 1242 */
 obj_t BgL_list2125z00_2748;
{ /* Llib/error.scm 1242 */
 obj_t BgL_arg2126z00_2749;
BgL_arg2126z00_2749 = 
MAKE_YOUNG_PAIR(BgL_resz00_2743, BNIL); 
BgL_list2125z00_2748 = 
MAKE_YOUNG_PAIR(BGl_string2910z00zz__errorz00, BgL_arg2126z00_2749); } 
BgL_arg2124z00_2747 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2125z00_2748); } 
{ 
 obj_t BgL_resz00_7877; long BgL_lenz00_7876;
BgL_lenz00_7876 = BgL_arg2123z00_2746; 
BgL_resz00_7877 = BgL_arg2124z00_2747; 
BgL_resz00_2743 = BgL_resz00_7877; 
BgL_lenz00_2742 = BgL_lenz00_7876; 
goto BgL_zc3z04anonymousza32121ze3z87_2744;} } } } }  else 
{ /* Llib/error.scm 1243 */
 obj_t BgL_arg2127z00_2751; obj_t BgL_arg2129z00_2752;
BgL_arg2127z00_2751 = 
CDR(
((obj_t)BgL_pathz00_2733)); 
{ /* Llib/error.scm 1244 */
 obj_t BgL_arg2130z00_2753;
BgL_arg2130z00_2753 = 
CAR(
((obj_t)BgL_pathz00_2733)); 
{ /* Llib/error.scm 1244 */
 obj_t BgL_list2131z00_2754;
{ /* Llib/error.scm 1244 */
 obj_t BgL_arg2132z00_2755;
{ /* Llib/error.scm 1244 */
 obj_t BgL_arg2133z00_2756;
BgL_arg2133z00_2756 = 
MAKE_YOUNG_PAIR(BgL_resz00_2734, BNIL); 
BgL_arg2132z00_2755 = 
MAKE_YOUNG_PAIR(BGl_string2911z00zz__errorz00, BgL_arg2133z00_2756); } 
BgL_list2131z00_2754 = 
MAKE_YOUNG_PAIR(BgL_arg2130z00_2753, BgL_arg2132z00_2755); } 
BgL_arg2129z00_2752 = 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2131z00_2754); } } 
{ 
 obj_t BgL_resz00_7887; obj_t BgL_pathz00_7886;
BgL_pathz00_7886 = BgL_arg2127z00_2751; 
BgL_resz00_7887 = BgL_arg2129z00_2752; 
BgL_resz00_2734 = BgL_resz00_7887; 
BgL_pathz00_2733 = BgL_pathz00_7886; 
goto BgL_zc3z04anonymousza32116ze3z87_2735;} } } } } } } } } } } } 

}



/* uncygdrive */
obj_t BGl_uncygdrivez00zz__errorz00(obj_t BgL_strz00_131)
{
{ /* Llib/error.scm 1252 */
if(
bigloo_strncmp(BGl_string2912z00zz__errorz00, BgL_strz00_131, 10L))
{ /* Llib/error.scm 1254 */
 bool_t BgL_test3402z00_7890;
if(
(
STRING_LENGTH(BgL_strz00_131)>12L))
{ /* Llib/error.scm 1255 */
 bool_t BgL_test3404z00_7894;
{ /* Llib/error.scm 1255 */
 unsigned char BgL_tmpz00_7895;
BgL_tmpz00_7895 = 
STRING_REF(BgL_strz00_131, 10L); 
BgL_test3404z00_7894 = 
isalpha(BgL_tmpz00_7895); } 
if(BgL_test3404z00_7894)
{ /* Llib/error.scm 1255 */
BgL_test3402z00_7890 = 
(
STRING_REF(BgL_strz00_131, 11L)==((unsigned char)'/'))
; }  else 
{ /* Llib/error.scm 1255 */
BgL_test3402z00_7890 = ((bool_t)0)
; } }  else 
{ /* Llib/error.scm 1254 */
BgL_test3402z00_7890 = ((bool_t)0)
; } 
if(BgL_test3402z00_7890)
{ /* Llib/error.scm 1257 */
 obj_t BgL_arg2147z00_2774; obj_t BgL_arg2148z00_2775;
{ /* Llib/error.scm 1257 */
 unsigned char BgL_arg2151z00_2778;
BgL_arg2151z00_2778 = 
STRING_REF(BgL_strz00_131, 10L); 
{ /* Llib/error.scm 1257 */
 obj_t BgL_list2152z00_2779;
{ /* Llib/error.scm 1257 */
 obj_t BgL_arg2154z00_2780;
{ /* Llib/error.scm 1257 */
 obj_t BgL_arg2155z00_2781;
BgL_arg2155z00_2781 = 
MAKE_YOUNG_PAIR(
BCHAR(((unsigned char)'/')), BNIL); 
BgL_arg2154z00_2780 = 
MAKE_YOUNG_PAIR(
BCHAR(((unsigned char)':')), BgL_arg2155z00_2781); } 
BgL_list2152z00_2779 = 
MAKE_YOUNG_PAIR(
BCHAR(BgL_arg2151z00_2778), BgL_arg2154z00_2780); } 
BgL_arg2147z00_2774 = 
BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_list2152z00_2779); } } 
BgL_arg2148z00_2775 = 
BGl_substringz00zz__r4_strings_6_7z00(BgL_strz00_131, 12L, 
STRING_LENGTH(BgL_strz00_131)); 
{ /* Llib/error.scm 1257 */
 obj_t BgL_list2149z00_2776;
{ /* Llib/error.scm 1257 */
 obj_t BgL_arg2150z00_2777;
BgL_arg2150z00_2777 = 
MAKE_YOUNG_PAIR(BgL_arg2148z00_2775, BNIL); 
BgL_list2149z00_2776 = 
MAKE_YOUNG_PAIR(BgL_arg2147z00_2774, BgL_arg2150z00_2777); } 
return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2149z00_2776);} }  else 
{ /* Llib/error.scm 1254 */
return BgL_strz00_131;} }  else 
{ /* Llib/error.scm 1253 */
return BgL_strz00_131;} } 

}



/* dirname->list */
obj_t BGl_dirnamezd2ze3listz31zz__errorz00(obj_t BgL_pathz00_132)
{
{ /* Llib/error.scm 1265 */
{ /* Llib/error.scm 1266 */
 long BgL_lenz00_2788; long BgL_initz00_2789;
{ /* Llib/error.scm 1266 */
 long BgL_lenz00_2811;
BgL_lenz00_2811 = 
STRING_LENGTH(
((obj_t)BgL_pathz00_132)); 
if(
(
STRING_REF(
((obj_t)BgL_pathz00_132), 
(BgL_lenz00_2811-1L))==
(unsigned char)(FILE_SEPARATOR)))
{ /* Llib/error.scm 1267 */
BgL_lenz00_2788 = 
(BgL_lenz00_2811-1L); }  else 
{ /* Llib/error.scm 1267 */
BgL_lenz00_2788 = BgL_lenz00_2811; } } 
if(
(
STRING_REF(
((obj_t)BgL_pathz00_132), 0L)==
(unsigned char)(FILE_SEPARATOR)))
{ /* Llib/error.scm 1270 */
BgL_initz00_2789 = 1L; }  else 
{ /* Llib/error.scm 1270 */
BgL_initz00_2789 = 0L; } 
{ /* Llib/error.scm 1271 */
 bool_t BgL_test3407z00_7927;
{ /* Llib/error.scm 1271 */
 long BgL_l1z00_4443;
BgL_l1z00_4443 = 
STRING_LENGTH(
((obj_t)BgL_pathz00_132)); 
if(
(BgL_l1z00_4443==1L))
{ /* Llib/error.scm 1271 */
 int BgL_arg2321z00_4446;
{ /* Llib/error.scm 1271 */
 char * BgL_auxz00_7935; char * BgL_tmpz00_7932;
BgL_auxz00_7935 = 
BSTRING_TO_STRING(BGl_string2911z00zz__errorz00); 
BgL_tmpz00_7932 = 
BSTRING_TO_STRING(
((obj_t)BgL_pathz00_132)); 
BgL_arg2321z00_4446 = 
memcmp(BgL_tmpz00_7932, BgL_auxz00_7935, BgL_l1z00_4443); } 
BgL_test3407z00_7927 = 
(
(long)(BgL_arg2321z00_4446)==0L); }  else 
{ /* Llib/error.scm 1271 */
BgL_test3407z00_7927 = ((bool_t)0)
; } } 
if(BgL_test3407z00_7927)
{ /* Llib/error.scm 1271 */
return BNIL;}  else 
{ 
 long BgL_readz00_2793; long BgL_prevz00_2794; obj_t BgL_listz00_2795;
BgL_readz00_2793 = BgL_initz00_2789; 
BgL_prevz00_2794 = BgL_initz00_2789; 
BgL_listz00_2795 = BNIL; 
BgL_zc3z04anonymousza32161ze3z87_2796:
if(
(BgL_readz00_2793==BgL_lenz00_2788))
{ /* Llib/error.scm 1278 */
 obj_t BgL_arg2163z00_2798;
BgL_arg2163z00_2798 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_pathz00_132, BgL_prevz00_2794, BgL_readz00_2793), BgL_listz00_2795); 
return 
bgl_reverse_bang(BgL_arg2163z00_2798);}  else 
{ /* Llib/error.scm 1277 */
if(
(
STRING_REF(
((obj_t)BgL_pathz00_132), BgL_readz00_2793)==
(unsigned char)(FILE_SEPARATOR)))
{ /* Llib/error.scm 1280 */
 long BgL_arg2168z00_2803; long BgL_arg2169z00_2804; obj_t BgL_arg2170z00_2805;
BgL_arg2168z00_2803 = 
(BgL_readz00_2793+1L); 
BgL_arg2169z00_2804 = 
(BgL_readz00_2793+1L); 
BgL_arg2170z00_2805 = 
MAKE_YOUNG_PAIR(
BGl_substringz00zz__r4_strings_6_7z00(BgL_pathz00_132, BgL_prevz00_2794, BgL_readz00_2793), BgL_listz00_2795); 
{ 
 obj_t BgL_listz00_7956; long BgL_prevz00_7955; long BgL_readz00_7954;
BgL_readz00_7954 = BgL_arg2168z00_2803; 
BgL_prevz00_7955 = BgL_arg2169z00_2804; 
BgL_listz00_7956 = BgL_arg2170z00_2805; 
BgL_listz00_2795 = BgL_listz00_7956; 
BgL_prevz00_2794 = BgL_prevz00_7955; 
BgL_readz00_2793 = BgL_readz00_7954; 
goto BgL_zc3z04anonymousza32161ze3z87_2796;} }  else 
{ 
 long BgL_readz00_7957;
BgL_readz00_7957 = 
(BgL_readz00_2793+1L); 
BgL_readz00_2793 = BgL_readz00_7957; 
goto BgL_zc3z04anonymousza32161ze3z87_2796;} } } } } } 

}



/* typeof */
BGL_EXPORTED_DEF obj_t bgl_typeof(obj_t BgL_objz00_133)
{
{ /* Llib/error.scm 1292 */
if(
INTEGERP(BgL_objz00_133))
{ /* Llib/error.scm 1294 */
return BGl_string2821z00zz__errorz00;}  else 
{ /* Llib/error.scm 1294 */
if(
REALP(BgL_objz00_133))
{ /* Llib/error.scm 1296 */
return BGl_string2913z00zz__errorz00;}  else 
{ /* Llib/error.scm 1296 */
if(
STRINGP(BgL_objz00_133))
{ /* Llib/error.scm 1298 */
return BGl_string2846z00zz__errorz00;}  else 
{ /* Llib/error.scm 1298 */
if(
SYMBOLP(BgL_objz00_133))
{ /* Llib/error.scm 1300 */
return BGl_string2914z00zz__errorz00;}  else 
{ /* Llib/error.scm 1300 */
if(
KEYWORDP(BgL_objz00_133))
{ /* Llib/error.scm 1302 */
return BGl_string2915z00zz__errorz00;}  else 
{ /* Llib/error.scm 1302 */
if(
CHARP(BgL_objz00_133))
{ /* Llib/error.scm 1304 */
return BGl_string2916z00zz__errorz00;}  else 
{ /* Llib/error.scm 1304 */
if(
BOOLEANP(BgL_objz00_133))
{ /* Llib/error.scm 1306 */
return BGl_string2917z00zz__errorz00;}  else 
{ /* Llib/error.scm 1306 */
if(
NULLP(BgL_objz00_133))
{ /* Llib/error.scm 1308 */
return BGl_string2918z00zz__errorz00;}  else 
{ /* Llib/error.scm 1308 */
if(
(BgL_objz00_133==BUNSPEC))
{ /* Llib/error.scm 1310 */
return BGl_string2919z00zz__errorz00;}  else 
{ /* Llib/error.scm 1310 */
if(
EPAIRP(BgL_objz00_133))
{ /* Llib/error.scm 1312 */
return BGl_string2920z00zz__errorz00;}  else 
{ /* Llib/error.scm 1312 */
if(
PAIRP(BgL_objz00_133))
{ /* Llib/error.scm 1314 */
return BGl_string2921z00zz__errorz00;}  else 
{ /* Llib/error.scm 1314 */
if(
BGl_classzf3zf3zz__objectz00(BgL_objz00_133))
{ /* Llib/error.scm 1316 */
return BGl_string2922z00zz__errorz00;}  else 
{ /* Llib/error.scm 1316 */
if(
VECTORP(BgL_objz00_133))
{ /* Llib/error.scm 1318 */
return BGl_string2923z00zz__errorz00;}  else 
{ /* Llib/error.scm 1318 */
if(
TVECTORP(BgL_objz00_133))
{ /* Llib/error.scm 1320 */
return BGl_string2924z00zz__errorz00;}  else 
{ /* Llib/error.scm 1320 */
if(
STRUCTP(BgL_objz00_133))
{ /* Llib/error.scm 1323 */
 obj_t BgL_arg2202z00_2839;
{ /* Llib/error.scm 1323 */
 obj_t BgL_arg2205z00_2842;
BgL_arg2205z00_2842 = 
STRUCT_KEY(BgL_objz00_133); 
{ /* Llib/error.scm 1323 */
 obj_t BgL_arg2328z00_4464;
BgL_arg2328z00_4464 = 
SYMBOL_TO_STRING(BgL_arg2205z00_2842); 
BgL_arg2202z00_2839 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_4464); } } 
{ /* Llib/error.scm 1323 */
 obj_t BgL_list2203z00_2840;
{ /* Llib/error.scm 1323 */
 obj_t BgL_arg2204z00_2841;
BgL_arg2204z00_2841 = 
MAKE_YOUNG_PAIR(BgL_arg2202z00_2839, BNIL); 
BgL_list2203z00_2840 = 
MAKE_YOUNG_PAIR(BGl_string2925z00zz__errorz00, BgL_arg2204z00_2841); } 
BGL_TAIL return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2203z00_2840);} }  else 
{ /* Llib/error.scm 1322 */
if(
PROCEDUREP(BgL_objz00_133))
{ /* Llib/error.scm 1324 */
return BGl_string2829z00zz__errorz00;}  else 
{ /* Llib/error.scm 1324 */
if(
INPUT_PORTP(BgL_objz00_133))
{ /* Llib/error.scm 1326 */
return BGl_string2926z00zz__errorz00;}  else 
{ /* Llib/error.scm 1326 */
if(
OUTPUT_PORTP(BgL_objz00_133))
{ /* Llib/error.scm 1328 */
return BGl_string2884z00zz__errorz00;}  else 
{ /* Llib/error.scm 1328 */
if(
BINARY_PORTP(BgL_objz00_133))
{ /* Llib/error.scm 1330 */
return BGl_string2927z00zz__errorz00;}  else 
{ /* Llib/error.scm 1330 */
if(
CELLP(BgL_objz00_133))
{ /* Llib/error.scm 1332 */
return BGl_string2928z00zz__errorz00;}  else 
{ /* Llib/error.scm 1332 */
if(
FOREIGNP(BgL_objz00_133))
{ /* Llib/error.scm 1335 */
 obj_t BgL_arg2212z00_2849;
{ /* Llib/error.scm 1335 */
 obj_t BgL_arg2217z00_2852;
BgL_arg2217z00_2852 = 
FOREIGN_ID(BgL_objz00_133); 
{ /* Llib/error.scm 1335 */
 obj_t BgL_arg2328z00_4466;
BgL_arg2328z00_4466 = 
SYMBOL_TO_STRING(BgL_arg2217z00_2852); 
BgL_arg2212z00_2849 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_4466); } } 
{ /* Llib/error.scm 1335 */
 obj_t BgL_list2215z00_2850;
{ /* Llib/error.scm 1335 */
 obj_t BgL_arg2216z00_2851;
BgL_arg2216z00_2851 = 
MAKE_YOUNG_PAIR(BgL_arg2212z00_2849, BNIL); 
BgL_list2215z00_2850 = 
MAKE_YOUNG_PAIR(BGl_string2929z00zz__errorz00, BgL_arg2216z00_2851); } 
BGL_TAIL return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2215z00_2850);} }  else 
{ /* Llib/error.scm 1334 */
if(
SOCKETP(BgL_objz00_133))
{ /* Llib/error.scm 1336 */
return BGl_string2930z00zz__errorz00;}  else 
{ /* Llib/error.scm 1336 */
if(
BGL_DATAGRAM_SOCKETP(BgL_objz00_133))
{ /* Llib/error.scm 1338 */
return BGl_string2931z00zz__errorz00;}  else 
{ /* Llib/error.scm 1338 */
if(
PROCESSP(BgL_objz00_133))
{ /* Llib/error.scm 1340 */
return BGl_string2932z00zz__errorz00;}  else 
{ /* Llib/error.scm 1340 */
if(
CUSTOMP(BgL_objz00_133))
{ /* Llib/error.scm 1342 */
return BGl_string2933z00zz__errorz00;}  else 
{ /* Llib/error.scm 1342 */
if(
OPAQUEP(BgL_objz00_133))
{ /* Llib/error.scm 1344 */
return BGl_string2934z00zz__errorz00;}  else 
{ /* Llib/error.scm 1344 */
if(
BGL_OBJECTP(BgL_objz00_133))
{ /* Llib/error.scm 1347 */
 obj_t BgL_classz00_2859;
{ /* Llib/error.scm 1347 */
 obj_t BgL_arg2728z00_4468; long BgL_arg2729z00_4469;
BgL_arg2728z00_4468 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 1347 */
 long BgL_arg2731z00_4470;
BgL_arg2731z00_4470 = 
BGL_OBJECT_CLASS_NUM(
((BgL_objectz00_bglt)BgL_objz00_133)); 
BgL_arg2729z00_4469 = 
(BgL_arg2731z00_4470-OBJECT_TYPE); } 
BgL_classz00_2859 = 
VECTOR_REF(BgL_arg2728z00_4468,BgL_arg2729z00_4469); } 
if(
BGl_classzf3zf3zz__objectz00(BgL_classz00_2859))
{ /* Llib/error.scm 1352 */
 obj_t BgL_symz00_2861;
BgL_symz00_2861 = 
BGl_classzd2namezd2zz__objectz00(BgL_classz00_2859); 
{ /* Llib/error.scm 1354 */
 obj_t BgL_arg2328z00_4477;
BgL_arg2328z00_4477 = 
SYMBOL_TO_STRING(BgL_symz00_2861); 
return 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_4477);} }  else 
{ /* Llib/error.scm 1351 */
return BGl_string2935z00zz__errorz00;} }  else 
{ /* Llib/error.scm 1346 */
if(
UCS2_STRINGP(BgL_objz00_133))
{ /* Llib/error.scm 1357 */
return BGl_string2936z00zz__errorz00;}  else 
{ /* Llib/error.scm 1357 */
if(
UCS2P(BgL_objz00_133))
{ /* Llib/error.scm 1359 */
return BGl_string2937z00zz__errorz00;}  else 
{ /* Llib/error.scm 1359 */
if(
ELONGP(BgL_objz00_133))
{ /* Llib/error.scm 1361 */
return BGl_string2938z00zz__errorz00;}  else 
{ /* Llib/error.scm 1361 */
if(
LLONGP(BgL_objz00_133))
{ /* Llib/error.scm 1363 */
return BGl_string2939z00zz__errorz00;}  else 
{ /* Llib/error.scm 1363 */
if(
BGL_MUTEXP(BgL_objz00_133))
{ /* Llib/error.scm 1365 */
return BGl_string2940z00zz__errorz00;}  else 
{ /* Llib/error.scm 1365 */
if(
BGL_CONDVARP(BgL_objz00_133))
{ /* Llib/error.scm 1367 */
return BGl_string2941z00zz__errorz00;}  else 
{ /* Llib/error.scm 1367 */
if(
BGL_DATEP(BgL_objz00_133))
{ /* Llib/error.scm 1369 */
return BGl_string2942z00zz__errorz00;}  else 
{ /* Llib/error.scm 1369 */
if(
BGL_HVECTORP(BgL_objz00_133))
{ /* Llib/error.scm 1372 */
 obj_t BgL_tagz00_2871;
BgL_tagz00_2871 = 
BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_objz00_133); 
{ /* Llib/error.scm 1373 */
 obj_t BgL__z00_2872; obj_t BgL__z00_2873; obj_t BgL__z00_2874;
{ /* Llib/error.scm 1374 */
 obj_t BgL_tmpz00_4478;
{ /* Llib/error.scm 1374 */
 int BgL_tmpz00_8052;
BgL_tmpz00_8052 = 
(int)(1L); 
BgL_tmpz00_4478 = 
BGL_MVALUES_VAL(BgL_tmpz00_8052); } 
{ /* Llib/error.scm 1374 */
 int BgL_tmpz00_8055;
BgL_tmpz00_8055 = 
(int)(1L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_8055, BUNSPEC); } 
BgL__z00_2872 = BgL_tmpz00_4478; } 
{ /* Llib/error.scm 1374 */
 obj_t BgL_tmpz00_4479;
{ /* Llib/error.scm 1374 */
 int BgL_tmpz00_8058;
BgL_tmpz00_8058 = 
(int)(2L); 
BgL_tmpz00_4479 = 
BGL_MVALUES_VAL(BgL_tmpz00_8058); } 
{ /* Llib/error.scm 1374 */
 int BgL_tmpz00_8061;
BgL_tmpz00_8061 = 
(int)(2L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_8061, BUNSPEC); } 
BgL__z00_2873 = BgL_tmpz00_4479; } 
{ /* Llib/error.scm 1374 */
 obj_t BgL_tmpz00_4480;
{ /* Llib/error.scm 1374 */
 int BgL_tmpz00_8064;
BgL_tmpz00_8064 = 
(int)(3L); 
BgL_tmpz00_4480 = 
BGL_MVALUES_VAL(BgL_tmpz00_8064); } 
{ /* Llib/error.scm 1374 */
 int BgL_tmpz00_8067;
BgL_tmpz00_8067 = 
(int)(3L); 
BGL_MVALUES_VAL_SET(BgL_tmpz00_8067, BUNSPEC); } 
BgL__z00_2874 = BgL_tmpz00_4480; } 
{ /* Llib/error.scm 1374 */
 obj_t BgL_arg2234z00_2875;
{ /* Llib/error.scm 1374 */
 obj_t BgL_arg2328z00_4482;
BgL_arg2328z00_4482 = 
SYMBOL_TO_STRING(
((obj_t)BgL_tagz00_2871)); 
BgL_arg2234z00_2875 = 
BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2328z00_4482); } 
{ /* Llib/error.scm 1374 */
 obj_t BgL_list2235z00_2876;
{ /* Llib/error.scm 1374 */
 obj_t BgL_arg2236z00_2877;
BgL_arg2236z00_2877 = 
MAKE_YOUNG_PAIR(BGl_string2923z00zz__errorz00, BNIL); 
BgL_list2235z00_2876 = 
MAKE_YOUNG_PAIR(BgL_arg2234z00_2875, BgL_arg2236z00_2877); } 
BGL_TAIL return 
BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list2235z00_2876);} } } }  else 
{ /* Llib/error.scm 1371 */
if(
BIGNUMP(BgL_objz00_133))
{ /* Llib/error.scm 1375 */
return BGl_string2943z00zz__errorz00;}  else 
{ /* Llib/error.scm 1375 */
if(
BGL_MMAPP(BgL_objz00_133))
{ /* Llib/error.scm 1377 */
return BGl_string2944z00zz__errorz00;}  else 
{ /* Llib/error.scm 1377 */
if(
BGL_REGEXPP(BgL_objz00_133))
{ /* Llib/error.scm 1379 */
return BGl_string2945z00zz__errorz00;}  else 
{ /* Llib/error.scm 1379 */
if(
BGL_INT8P(BgL_objz00_133))
{ /* Llib/error.scm 1381 */
return BGl_string2946z00zz__errorz00;}  else 
{ /* Llib/error.scm 1381 */
if(
BGL_UINT8P(BgL_objz00_133))
{ /* Llib/error.scm 1383 */
return BGl_string2947z00zz__errorz00;}  else 
{ /* Llib/error.scm 1383 */
if(
BGL_INT16P(BgL_objz00_133))
{ /* Llib/error.scm 1385 */
return BGl_string2948z00zz__errorz00;}  else 
{ /* Llib/error.scm 1385 */
if(
BGL_UINT16P(BgL_objz00_133))
{ /* Llib/error.scm 1387 */
return BGl_string2949z00zz__errorz00;}  else 
{ /* Llib/error.scm 1387 */
if(
BGL_INT32P(BgL_objz00_133))
{ /* Llib/error.scm 1389 */
return BGl_string2950z00zz__errorz00;}  else 
{ /* Llib/error.scm 1389 */
if(
BGL_UINT32P(BgL_objz00_133))
{ /* Llib/error.scm 1391 */
return BGl_string2951z00zz__errorz00;}  else 
{ /* Llib/error.scm 1391 */
if(
BGL_INT64P(BgL_objz00_133))
{ /* Llib/error.scm 1393 */
return BGl_string2952z00zz__errorz00;}  else 
{ /* Llib/error.scm 1393 */
if(
BGL_UINT64P(BgL_objz00_133))
{ /* Llib/error.scm 1395 */
return BGl_string2953z00zz__errorz00;}  else 
{ /* Llib/error.scm 1395 */
if(
CNSTP(BgL_objz00_133))
{ /* Llib/error.scm 1397 */
return BGl_string2954z00zz__errorz00;}  else 
{ /* Llib/error.scm 1397 */
return 
string_to_bstring(
FOREIGN_TYPE_NAME(BgL_objz00_133));} } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } 

}



/* &typeof */
obj_t BGl_z62typeofz62zz__errorz00(obj_t BgL_envz00_5228, obj_t BgL_objz00_5229)
{
{ /* Llib/error.scm 1292 */
return 
bgl_typeof(BgL_objz00_5229);} 

}



/* find-runtime-type */
BGL_EXPORTED_DEF obj_t bgl_find_runtime_type(obj_t BgL_oz00_134)
{
{ /* Llib/error.scm 1405 */
BGL_TAIL return 
bgl_typeof(BgL_oz00_134);} 

}



/* &find-runtime-type */
obj_t BGl_z62findzd2runtimezd2typez62zz__errorz00(obj_t BgL_envz00_5230, obj_t BgL_oz00_5231)
{
{ /* Llib/error.scm 1405 */
return 
bgl_find_runtime_type(BgL_oz00_5231);} 

}



/* c-debugging-show-type */
BGL_EXPORTED_DEF char * bgl_show_type(obj_t BgL_objz00_135)
{
{ /* Llib/error.scm 1413 */
{ /* Llib/error.scm 1414 */
 obj_t BgL_tz00_4483;
BgL_tz00_4483 = 
bgl_typeof(BgL_objz00_135); 
{ /* Llib/error.scm 1415 */
 obj_t BgL_arg2249z00_4484;
{ /* Llib/error.scm 1415 */
 obj_t BgL_tmpz00_8106;
BgL_tmpz00_8106 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_arg2249z00_4484 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8106); } 
{ /* Llib/error.scm 1415 */
 obj_t BgL_list2250z00_4485;
BgL_list2250z00_4485 = 
MAKE_YOUNG_PAIR(BgL_tz00_4483, BNIL); 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_arg2249z00_4484, BgL_list2250z00_4485); } } 
return 
BSTRING_TO_STRING(BgL_tz00_4483);} } 

}



/* &c-debugging-show-type */
obj_t BGl_z62czd2debuggingzd2showzd2typezb0zz__errorz00(obj_t BgL_envz00_5232, obj_t BgL_objz00_5233)
{
{ /* Llib/error.scm 1413 */
return 
string_to_bstring(
bgl_show_type(BgL_objz00_5233));} 

}



/* &try */
BGL_EXPORTED_DEF obj_t BGl_z62tryz62zz__errorz00(obj_t BgL_thunkz00_136, obj_t BgL_handlerz00_137)
{
{ /* Llib/error.scm 1421 */
BGL_TAIL return 
BGl_zc3z04exitza32251ze3ze70z60zz__errorz00(BgL_thunkz00_136, BgL_handlerz00_137);} 

}



/* <@exit:2251>~0 */
obj_t BGl_zc3z04exitza32251ze3ze70z60zz__errorz00(obj_t BgL_thunkz00_5261, obj_t BgL_handlerz00_5260)
{
{ /* Llib/error.scm 1422 */
jmp_buf_t jmpbuf; 
 void * BgL_an_exit1147z00_2894;
if( SET_EXIT(BgL_an_exit1147z00_2894 ) ) { 
return 
BGL_EXIT_VALUE();
} else {
#if( SIGSETJMP_SAVESIGS == 0 )
  // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
  // bgl_restore_signal_handlers();
#endif

BgL_an_exit1147z00_2894 = 
(void *)jmpbuf; 
{ /* Llib/error.scm 1422 */
 obj_t BgL_env1151z00_2895;
BgL_env1151z00_2895 = 
BGL_CURRENT_DYNAMIC_ENV(); 
PUSH_ENV_EXIT(BgL_env1151z00_2895, BgL_an_exit1147z00_2894, 1L); 
{ /* Llib/error.scm 1422 */
 obj_t BgL_an_exitd1148z00_2896;
BgL_an_exitd1148z00_2896 = 
BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1151z00_2895); 
{ /* Llib/error.scm 1422 */
 obj_t BgL_escz00_5234;
BgL_escz00_5234 = 
MAKE_FX_PROCEDURE(BGl_z62escz62zz__errorz00, 
(int)(1L), 
(int)(1L)); 
PROCEDURE_SET(BgL_escz00_5234, 
(int)(0L), BgL_an_exitd1148z00_2896); 
{ /* Llib/error.scm 1422 */
 obj_t BgL_res1150z00_2899;
{ /* Llib/error.scm 1425 */
 obj_t BgL_zc3z04anonymousza32253ze3z87_5235;
BgL_zc3z04anonymousza32253ze3z87_5235 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32253ze3ze5zz__errorz00, 
(int)(1L), 
(int)(3L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza32253ze3z87_5235, 
(int)(0L), BgL_an_exitd1148z00_2896); 
PROCEDURE_SET(BgL_zc3z04anonymousza32253ze3z87_5235, 
(int)(1L), BgL_handlerz00_5260); 
PROCEDURE_SET(BgL_zc3z04anonymousza32253ze3z87_5235, 
(int)(2L), BgL_escz00_5234); 
BgL_res1150z00_2899 = 
BGl_withzd2exceptionzd2handlerz00zz__errorz00(BgL_zc3z04anonymousza32253ze3z87_5235, BgL_thunkz00_5261); } 
POP_ENV_EXIT(BgL_env1151z00_2895); 
return BgL_res1150z00_2899;} } } } 
}} 

}



/* &&try */
obj_t BGl_z62z62tryz00zz__errorz00(obj_t BgL_envz00_5236, obj_t BgL_thunkz00_5237, obj_t BgL_handlerz00_5238)
{
{ /* Llib/error.scm 1421 */
{ /* Llib/error.scm 1422 */
 obj_t BgL_auxz00_8143; obj_t BgL_auxz00_8136;
if(
PROCEDUREP(BgL_handlerz00_5238))
{ /* Llib/error.scm 1422 */
BgL_auxz00_8143 = BgL_handlerz00_5238
; }  else 
{ 
 obj_t BgL_auxz00_8146;
BgL_auxz00_8146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(54010L), BGl_string2955z00zz__errorz00, BGl_string2829z00zz__errorz00, BgL_handlerz00_5238); 
FAILURE(BgL_auxz00_8146,BFALSE,BFALSE);} 
if(
PROCEDUREP(BgL_thunkz00_5237))
{ /* Llib/error.scm 1422 */
BgL_auxz00_8136 = BgL_thunkz00_5237
; }  else 
{ 
 obj_t BgL_auxz00_8139;
BgL_auxz00_8139 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(54010L), BGl_string2955z00zz__errorz00, BGl_string2829z00zz__errorz00, BgL_thunkz00_5237); 
FAILURE(BgL_auxz00_8139,BFALSE,BFALSE);} 
return 
BGl_z62tryz62zz__errorz00(BgL_auxz00_8136, BgL_auxz00_8143);} } 

}



/* &<@anonymous:2253> */
obj_t BGl_z62zc3z04anonymousza32253ze3ze5zz__errorz00(obj_t BgL_envz00_5239, obj_t BgL_ez00_5243)
{
{ /* Llib/error.scm 1424 */
{ /* Llib/error.scm 1425 */
 obj_t BgL_handlerz00_5241; obj_t BgL_escz00_5242;
BgL_handlerz00_5241 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_5239, 
(int)(1L))); 
BgL_escz00_5242 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_5239, 
(int)(2L))); 
{ /* Llib/error.scm 1425 */
 bool_t BgL_test3461z00_8157;
{ /* Llib/error.scm 1425 */
 obj_t BgL_classz00_5321;
BgL_classz00_5321 = BGl_z62errorz62zz__objectz00; 
if(
BGL_OBJECTP(BgL_ez00_5243))
{ /* Llib/error.scm 1425 */
 BgL_objectz00_bglt BgL_arg2721z00_5322;
BgL_arg2721z00_5322 = 
(BgL_objectz00_bglt)(BgL_ez00_5243); 
if(
BGL_CONDEXPAND_ISA_ARCH64())
{ /* Llib/error.scm 1425 */
 long BgL_idxz00_5323;
BgL_idxz00_5323 = 
BGL_OBJECT_INHERITANCE_NUM(BgL_arg2721z00_5322); 
BgL_test3461z00_8157 = 
(
VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
(BgL_idxz00_5323+3L))==BgL_classz00_5321); }  else 
{ /* Llib/error.scm 1425 */
 bool_t BgL_res2756z00_5326;
{ /* Llib/error.scm 1425 */
 obj_t BgL_oclassz00_5327;
{ /* Llib/error.scm 1425 */
 obj_t BgL_arg2728z00_5328; long BgL_arg2729z00_5329;
BgL_arg2728z00_5328 = 
(BGl_za2classesza2z00zz__objectz00); 
{ /* Llib/error.scm 1425 */
 long BgL_arg2731z00_5330;
BgL_arg2731z00_5330 = 
BGL_OBJECT_CLASS_NUM(BgL_arg2721z00_5322); 
BgL_arg2729z00_5329 = 
(BgL_arg2731z00_5330-OBJECT_TYPE); } 
BgL_oclassz00_5327 = 
VECTOR_REF(BgL_arg2728z00_5328,BgL_arg2729z00_5329); } 
{ /* Llib/error.scm 1425 */
 bool_t BgL__ortest_1198z00_5331;
BgL__ortest_1198z00_5331 = 
(BgL_classz00_5321==BgL_oclassz00_5327); 
if(BgL__ortest_1198z00_5331)
{ /* Llib/error.scm 1425 */
BgL_res2756z00_5326 = BgL__ortest_1198z00_5331; }  else 
{ /* Llib/error.scm 1425 */
 long BgL_odepthz00_5332;
{ /* Llib/error.scm 1425 */
 obj_t BgL_arg2717z00_5333;
BgL_arg2717z00_5333 = 
(BgL_oclassz00_5327); 
BgL_odepthz00_5332 = 
BGL_CLASS_DEPTH(BgL_arg2717z00_5333); } 
if(
(3L<BgL_odepthz00_5332))
{ /* Llib/error.scm 1425 */
 obj_t BgL_arg2715z00_5334;
{ /* Llib/error.scm 1425 */
 obj_t BgL_arg2716z00_5335;
BgL_arg2716z00_5335 = 
(BgL_oclassz00_5327); 
BgL_arg2715z00_5334 = 
BGL_CLASS_ANCESTORS_REF(BgL_arg2716z00_5335, 3L); } 
BgL_res2756z00_5326 = 
(BgL_arg2715z00_5334==BgL_classz00_5321); }  else 
{ /* Llib/error.scm 1425 */
BgL_res2756z00_5326 = ((bool_t)0); } } } } 
BgL_test3461z00_8157 = BgL_res2756z00_5326; } }  else 
{ /* Llib/error.scm 1425 */
BgL_test3461z00_8157 = ((bool_t)0)
; } } 
if(BgL_test3461z00_8157)
{ /* Llib/error.scm 1425 */
{ /* Llib/error.scm 1427 */
 obj_t BgL_arg2255z00_5336; obj_t BgL_arg2256z00_5337; obj_t BgL_arg2257z00_5338;
BgL_arg2255z00_5336 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_ez00_5243)))->BgL_procz00); 
BgL_arg2256z00_5337 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_ez00_5243)))->BgL_msgz00); 
BgL_arg2257z00_5338 = 
(((BgL_z62errorz62_bglt)COBJECT(
((BgL_z62errorz62_bglt)BgL_ez00_5243)))->BgL_objz00); 
BGL_PROCEDURE_CALL4(BgL_handlerz00_5241, BgL_escz00_5242, BgL_arg2255z00_5336, BgL_arg2256z00_5337, BgL_arg2257z00_5338); } 
{ /* Llib/error.scm 1428 */
 obj_t BgL_list2258z00_5339;
BgL_list2258z00_5339 = 
MAKE_YOUNG_PAIR(
BINT(4L), BNIL); 
return 
BGl_exitz00zz__errorz00(BgL_list2258z00_5339);} }  else 
{ /* Llib/error.scm 1425 */
return 
BGl_raisez00zz__errorz00(BgL_ez00_5243);} } } } 

}



/* &esc */
obj_t BGl_z62escz62zz__errorz00(obj_t BgL_envz00_5244, obj_t BgL_val1149z00_5246)
{
{ /* Llib/error.scm 1422 */
return 
unwind_stack_until(
PROCEDURE_REF(BgL_envz00_5244, 
(int)(0L)), BFALSE, BgL_val1149z00_5246, BFALSE, BFALSE);} 

}



/* push-error-handler! */
BGL_EXPORTED_DEF obj_t BGl_pushzd2errorzd2handlerz12z12zz__errorz00(obj_t BgL_hdlz00_138, obj_t BgL_exitz00_139)
{
{ /* Llib/error.scm 1435 */
BGL_ERROR_HANDLER_PUSH(BgL_hdlz00_138, BgL_exitz00_139); 
return BUNSPEC;} 

}



/* &push-error-handler! */
obj_t BGl_z62pushzd2errorzd2handlerz12z70zz__errorz00(obj_t BgL_envz00_5247, obj_t BgL_hdlz00_5248, obj_t BgL_exitz00_5249)
{
{ /* Llib/error.scm 1435 */
{ /* Llib/error.scm 1435 */
 obj_t BgL_auxz00_8201;
if(
PROCEDUREP(BgL_hdlz00_5248))
{ /* Llib/error.scm 1435 */
BgL_auxz00_8201 = BgL_hdlz00_5248
; }  else 
{ 
 obj_t BgL_auxz00_8204;
BgL_auxz00_8204 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(54431L), BGl_string2956z00zz__errorz00, BGl_string2829z00zz__errorz00, BgL_hdlz00_5248); 
FAILURE(BgL_auxz00_8204,BFALSE,BFALSE);} 
return 
BGl_pushzd2errorzd2handlerz12z12zz__errorz00(BgL_auxz00_8201, BgL_exitz00_5249);} } 

}



/* set-error-handler! */
BGL_EXPORTED_DEF obj_t BGl_setzd2errorzd2handlerz12z12zz__errorz00(obj_t BgL_ehdlz00_140)
{
{ /* Llib/error.scm 1441 */
BGL_ERROR_HANDLER_SET(BgL_ehdlz00_140); 
return BUNSPEC;} 

}



/* &set-error-handler! */
obj_t BGl_z62setzd2errorzd2handlerz12z70zz__errorz00(obj_t BgL_envz00_5250, obj_t BgL_ehdlz00_5251)
{
{ /* Llib/error.scm 1441 */
return 
BGl_setzd2errorzd2handlerz12z12zz__errorz00(BgL_ehdlz00_5251);} 

}



/* env-set-error-handler! */
BGL_EXPORTED_DEF obj_t BGl_envzd2setzd2errorzd2handlerz12zc0zz__errorz00(obj_t BgL_envz00_141, obj_t BgL_ehdlz00_142)
{
{ /* Llib/error.scm 1447 */
BGL_ENV_ERROR_HANDLER_SET(BgL_envz00_141, BgL_ehdlz00_142); 
return BUNSPEC;} 

}



/* &env-set-error-handler! */
obj_t BGl_z62envzd2setzd2errorzd2handlerz12za2zz__errorz00(obj_t BgL_envz00_5252, obj_t BgL_envz00_5253, obj_t BgL_ehdlz00_5254)
{
{ /* Llib/error.scm 1447 */
{ /* Llib/error.scm 1447 */
 obj_t BgL_auxz00_8212;
if(
BGL_DYNAMIC_ENVP(BgL_envz00_5253))
{ /* Llib/error.scm 1447 */
BgL_auxz00_8212 = BgL_envz00_5253
; }  else 
{ 
 obj_t BgL_auxz00_8215;
BgL_auxz00_8215 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(55031L), BGl_string2957z00zz__errorz00, BGl_string2958z00zz__errorz00, BgL_envz00_5253); 
FAILURE(BgL_auxz00_8215,BFALSE,BFALSE);} 
return 
BGl_envzd2setzd2errorzd2handlerz12zc0zz__errorz00(BgL_auxz00_8212, BgL_ehdlz00_5254);} } 

}



/* get-error-handler */
BGL_EXPORTED_DEF obj_t BGl_getzd2errorzd2handlerz00zz__errorz00(void)
{
{ /* Llib/error.scm 1453 */
return 
BGL_ERROR_HANDLER_GET();} 

}



/* &get-error-handler */
obj_t BGl_z62getzd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5255)
{
{ /* Llib/error.scm 1453 */
return 
BGl_getzd2errorzd2handlerz00zz__errorz00();} 

}



/* env-get-error-handler */
BGL_EXPORTED_DEF obj_t BGl_envzd2getzd2errorzd2handlerzd2zz__errorz00(obj_t BgL_envz00_143)
{
{ /* Llib/error.scm 1459 */
return 
BGL_ENV_ERROR_HANDLER_GET(BgL_envz00_143);} 

}



/* &env-get-error-handler */
obj_t BGl_z62envzd2getzd2errorzd2handlerzb0zz__errorz00(obj_t BgL_envz00_5256, obj_t BgL_envz00_5257)
{
{ /* Llib/error.scm 1459 */
{ /* Llib/error.scm 1460 */
 obj_t BgL_auxz00_8223;
if(
BGL_DYNAMIC_ENVP(BgL_envz00_5257))
{ /* Llib/error.scm 1460 */
BgL_auxz00_8223 = BgL_envz00_5257
; }  else 
{ 
 obj_t BgL_auxz00_8226;
BgL_auxz00_8226 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(55671L), BGl_string2959z00zz__errorz00, BGl_string2958z00zz__errorz00, BgL_envz00_5257); 
FAILURE(BgL_auxz00_8226,BFALSE,BFALSE);} 
return 
BGl_envzd2getzd2errorzd2handlerzd2zz__errorz00(BgL_auxz00_8223);} } 

}



/* notify-interrupt */
BGL_EXPORTED_DEF obj_t BGl_notifyzd2interruptzd2zz__errorz00(int BgL_sigz00_144)
{
{ /* Llib/error.scm 1465 */
{ /* Llib/error.scm 1466 */
 obj_t BgL_notifyz00_4521;
BgL_notifyz00_4521 = 
BGL_INTERRUPT_NOTIFIER_GET(); 
if(
PROCEDUREP(BgL_notifyz00_4521))
{ /* Llib/error.scm 1467 */
return 
BGL_PROCEDURE_CALL1(BgL_notifyz00_4521, 
BINT(BgL_sigz00_144));}  else 
{ /* Llib/error.scm 1467 */
BGL_TAIL return 
BGl_defaultzd2interruptzd2notifierz00zz__errorz00(BgL_sigz00_144);} } } 

}



/* &notify-interrupt */
obj_t BGl_z62notifyzd2interruptzb0zz__errorz00(obj_t BgL_envz00_5258, obj_t BgL_sigz00_5259)
{
{ /* Llib/error.scm 1465 */
{ /* Llib/error.scm 1466 */
 int BgL_auxz00_8240;
{ /* Llib/error.scm 1466 */
 obj_t BgL_tmpz00_8241;
if(
INTEGERP(BgL_sigz00_5259))
{ /* Llib/error.scm 1466 */
BgL_tmpz00_8241 = BgL_sigz00_5259
; }  else 
{ 
 obj_t BgL_auxz00_8244;
BgL_auxz00_8244 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string2819z00zz__errorz00, 
BINT(55958L), BGl_string2960z00zz__errorz00, BGl_string2821z00zz__errorz00, BgL_sigz00_5259); 
FAILURE(BgL_auxz00_8244,BFALSE,BFALSE);} 
BgL_auxz00_8240 = 
CINT(BgL_tmpz00_8241); } 
return 
BGl_notifyzd2interruptzd2zz__errorz00(BgL_auxz00_8240);} } 

}



/* default-interrupt-notifier */
obj_t BGl_defaultzd2interruptzd2notifierz00zz__errorz00(int BgL_sigz00_145)
{
{ /* Llib/error.scm 1475 */
{ /* Llib/error.scm 1476 */
 obj_t BgL_portz00_2912;
{ /* Llib/error.scm 1476 */
 obj_t BgL_tmpz00_8250;
BgL_tmpz00_8250 = 
BGL_CURRENT_DYNAMIC_ENV(); 
BgL_portz00_2912 = 
BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8250); } 
{ /* Llib/error.scm 1477 */
 obj_t BgL_list2260z00_2913;
BgL_list2260z00_2913 = 
MAKE_YOUNG_PAIR(BgL_portz00_2912, BNIL); 
BGl_newlinez00zz__r4_output_6_10_3z00(BgL_list2260z00_2913); } 
{ /* Llib/error.scm 1478 */
 obj_t BgL_list2261z00_2914;
BgL_list2261z00_2914 = 
MAKE_YOUNG_PAIR(BGl_string2961z00zz__errorz00, BNIL); 
BGl_fprintz00zz__r4_output_6_10_3z00(BgL_portz00_2912, BgL_list2261z00_2914); } 
return 
bgl_flush_output_port(BgL_portz00_2912);} } 

}



/* &sigfpe-error-handler */
obj_t BGl_z62sigfpezd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5089, obj_t BgL_nz00_5090)
{
{ /* Llib/error.scm 1484 */
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_5340;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_5341;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_5342;
BgL_new1078z00_5342 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_5343;
BgL_arg1513z00_5343 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_5342), BgL_arg1513z00_5343); } 
BgL_new1079z00_5341 = BgL_new1078z00_5342; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5341)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5341)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8266;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_5344;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_5345;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_5346;
BgL_classz00_5346 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_5345 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5346); } 
BgL_arg1510z00_5344 = 
VECTOR_REF(BgL_arg1511z00_5345,2L); } 
BgL_auxz00_8266 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_5344); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5341)))->BgL_stackz00)=((obj_t)BgL_auxz00_8266),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5341))->BgL_procz00)=((obj_t)BGl_string2962z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5341))->BgL_msgz00)=((obj_t)BGl_string2963z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5341))->BgL_objz00)=((obj_t)BGl_string2964z00zz__errorz00),BUNSPEC); 
BgL_arg1509z00_5340 = BgL_new1079z00_5341; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_5340));} } 

}



/* &sigill-error-handler */
obj_t BGl_z62sigillzd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5091, obj_t BgL_nz00_5092)
{
{ /* Llib/error.scm 1490 */
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_5347;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_5348;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_5349;
BgL_new1078z00_5349 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_5350;
BgL_arg1513z00_5350 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_5349), BgL_arg1513z00_5350); } 
BgL_new1079z00_5348 = BgL_new1078z00_5349; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5348)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5348)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8285;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_5351;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_5352;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_5353;
BgL_classz00_5353 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_5352 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5353); } 
BgL_arg1510z00_5351 = 
VECTOR_REF(BgL_arg1511z00_5352,2L); } 
BgL_auxz00_8285 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_5351); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5348)))->BgL_stackz00)=((obj_t)BgL_auxz00_8285),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5348))->BgL_procz00)=((obj_t)BGl_string2965z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5348))->BgL_msgz00)=((obj_t)BGl_string2966z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5348))->BgL_objz00)=((obj_t)BGl_string2964z00zz__errorz00),BUNSPEC); 
BgL_arg1509z00_5347 = BgL_new1079z00_5348; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_5347));} } 

}



/* &sigbus-error-handler */
obj_t BGl_z62sigbuszd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5093, obj_t BgL_nz00_5094)
{
{ /* Llib/error.scm 1496 */
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_5354;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_5355;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_5356;
BgL_new1078z00_5356 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_5357;
BgL_arg1513z00_5357 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_5356), BgL_arg1513z00_5357); } 
BgL_new1079z00_5355 = BgL_new1078z00_5356; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5355)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5355)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8304;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_5358;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_5359;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_5360;
BgL_classz00_5360 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_5359 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5360); } 
BgL_arg1510z00_5358 = 
VECTOR_REF(BgL_arg1511z00_5359,2L); } 
BgL_auxz00_8304 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_5358); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5355)))->BgL_stackz00)=((obj_t)BgL_auxz00_8304),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5355))->BgL_procz00)=((obj_t)BGl_string2965z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5355))->BgL_msgz00)=((obj_t)BGl_string2967z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5355))->BgL_objz00)=((obj_t)BGl_string2964z00zz__errorz00),BUNSPEC); 
BgL_arg1509z00_5354 = BgL_new1079z00_5355; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_5354));} } 

}



/* &sigsegv-error-handler */
obj_t BGl_z62sigsegvzd2errorzd2handlerz62zz__errorz00(obj_t BgL_envz00_5095, obj_t BgL_nz00_5096)
{
{ /* Llib/error.scm 1502 */
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_arg1509z00_5361;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1079z00_5362;
{ /* Llib/error.scm 546 */
 BgL_z62errorz62_bglt BgL_new1078z00_5363;
BgL_new1078z00_5363 = 
((BgL_z62errorz62_bglt)BOBJECT( GC_MALLOC( sizeof(struct BgL_z62errorz62_bgl) ))); 
{ /* Llib/error.scm 546 */
 long BgL_arg1513z00_5364;
BgL_arg1513z00_5364 = 
BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00); 
BGL_OBJECT_CLASS_NUM_SET(
((BgL_objectz00_bglt)BgL_new1078z00_5363), BgL_arg1513z00_5364); } 
BgL_new1079z00_5362 = BgL_new1078z00_5363; } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5362)))->BgL_fnamez00)=((obj_t)BFALSE),BUNSPEC); 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5362)))->BgL_locationz00)=((obj_t)BFALSE),BUNSPEC); 
{ 
 obj_t BgL_auxz00_8323;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1510z00_5365;
{ /* Llib/error.scm 546 */
 obj_t BgL_arg1511z00_5366;
{ /* Llib/error.scm 546 */
 obj_t BgL_classz00_5367;
BgL_classz00_5367 = BGl_z62errorz62zz__objectz00; 
BgL_arg1511z00_5366 = 
BGL_CLASS_ALL_FIELDS(BgL_classz00_5367); } 
BgL_arg1510z00_5365 = 
VECTOR_REF(BgL_arg1511z00_5366,2L); } 
BgL_auxz00_8323 = 
BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(BgL_arg1510z00_5365); } 
((((BgL_z62exceptionz62_bglt)COBJECT(
((BgL_z62exceptionz62_bglt)BgL_new1079z00_5362)))->BgL_stackz00)=((obj_t)BgL_auxz00_8323),BUNSPEC); } 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5362))->BgL_procz00)=((obj_t)BGl_string2965z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5362))->BgL_msgz00)=((obj_t)BGl_string2968z00zz__errorz00),BUNSPEC); 
((((BgL_z62errorz62_bglt)COBJECT(BgL_new1079z00_5362))->BgL_objz00)=((obj_t)BGl_string2964z00zz__errorz00),BUNSPEC); 
BgL_arg1509z00_5361 = BgL_new1079z00_5362; } 
return 
BGl_raisez00zz__errorz00(
((obj_t)BgL_arg1509z00_5361));} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__errorz00(void)
{
{ /* Llib/error.scm 18 */
BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L, 
BSTRING_TO_STRING(BGl_string2969z00zz__errorz00)); 
return 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string2969z00zz__errorz00));} 

}

#ifdef __cplusplus
}
#endif
