/*===========================================================================*/
/*   (Llib/mmap.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/mmap.scm -indent -o objs/obj_s/Llib/mmap.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MMAP_TYPE_DEFINITIONS
#define BGL___MMAP_TYPE_DEFINITIONS
#endif // BGL___MMAP_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

BGL_EXPORTED_DECL obj_t BGl_mmapzd2putzd2charz12z12zz__mmapz00(obj_t, unsigned char);
extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62closezd2mmapzb0zz__mmapz00(obj_t, obj_t);
static obj_t BGl_z62mmapzd2readzd2positionz62zz__mmapz00(obj_t, obj_t);
extern obj_t bstring_to_keyword(obj_t);
BGL_EXPORTED_DECL obj_t BGl_mmapzd2ze3bstringz31zz__mmapz00(obj_t);
static obj_t BGl_z62mmapzf3z91zz__mmapz00(obj_t, obj_t);
static obj_t BGl_requirezd2initializa7ationz75zz__mmapz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_mmapzd2putzd2stringz12z12zz__mmapz00(obj_t, obj_t);
static obj_t BGl_symbol1850z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_z62mmapzd2getzd2charz62zz__mmapz00(obj_t, obj_t);
static obj_t BGl__openzd2mmapzd2zz__mmapz00(obj_t, obj_t);
static obj_t BGl_symbol1857z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_z62mmapzd2substringzd2setz12z70zz__mmapz00(obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__mmapz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_z62mmapzd2namezb0zz__mmapz00(obj_t, obj_t);
static obj_t BGl_z62mmapzd2lengthzb0zz__mmapz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mmapzd2setz12zc0zz__mmapz00(obj_t, long, unsigned char);
static obj_t BGl_search1104ze70ze7zz__mmapz00(long, obj_t, obj_t, long);
BGL_EXPORTED_DECL obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t, obj_t, obj_t);
extern obj_t string_to_bstring_len(char *, int);
BGL_EXPORTED_DECL obj_t BGl_stringzd2ze3mmapz31zz__mmapz00(obj_t, obj_t, obj_t);
static obj_t BGl_search1113ze70ze7zz__mmapz00(long, obj_t, obj_t, long);
static obj_t BGl_symbol1877z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_list1845z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_z62mmapzd2refzd2urz62zz__mmapz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mmapzd2getzd2stringz00zz__mmapz00(obj_t, long);
static obj_t BGl_z62mmapzd2getzd2stringz62zz__mmapz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol1880z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_symbol1883z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_z62mmapzd2ze3stringz53zz__mmapz00(obj_t, obj_t);
static obj_t BGl_z62mmapzd2ze3bstringz53zz__mmapz00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__mmapz00(void);
static obj_t BGl_genericzd2initzd2zz__mmapz00(void);
static obj_t BGl_symbol1892z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_z62mmapzd2writezd2positionz62zz__mmapz00(obj_t, obj_t);
static obj_t BGl_importedzd2moduleszd2initz00zz__mmapz00(void);
static obj_t BGl_symbol1895z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__mmapz00(void);
BGL_EXPORTED_DECL unsigned char BGl_mmapzd2refzd2zz__mmapz00(obj_t, long);
static obj_t BGl_objectzd2initzd2zz__mmapz00(void);
BGL_EXPORTED_DECL obj_t BGl_mmapzd2namezd2zz__mmapz00(obj_t);
extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_z62mmapzd2substringzb0zz__mmapz00(obj_t, obj_t, obj_t, obj_t);
extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
BGL_EXPORTED_DECL char * BGl_mmapzd2ze3stringz31zz__mmapz00(obj_t);
extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
static obj_t BGl_z62mmapzd2putzd2stringz12z70zz__mmapz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62mmapzd2setzd2urz12z70zz__mmapz00(obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_mmapzd2writezd2positionzd2setz12zc0zz__mmapz00(obj_t, long);
extern obj_t string_append_3(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mmapzd2setzd2urz12z12zz__mmapz00(obj_t, long, unsigned char);
extern obj_t bstring_to_symbol(obj_t);
static obj_t BGl_z62mmapzd2putzd2charz12z70zz__mmapz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_mmapzd2substringzd2setz12z12zz__mmapz00(obj_t, long, obj_t);
BGL_EXPORTED_DECL long BGl_mmapzd2lengthzd2zz__mmapz00(obj_t);
static obj_t BGl_methodzd2initzd2zz__mmapz00(void);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL unsigned char BGl_mmapzd2refzd2urz00zz__mmapz00(obj_t, long);
static obj_t BGl_keyword1846z00zz__mmapz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_mmapzd2substringzd2zz__mmapz00(obj_t, long, long);
static obj_t BGl_keyword1848z00zz__mmapz00 = BUNSPEC;
static obj_t BGl_z62mmapzd2refzb0zz__mmapz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_mmapzf3zf3zz__mmapz00(obj_t);
BGL_EXPORTED_DECL long BGl_mmapzd2readzd2positionzd2setz12zc0zz__mmapz00(obj_t, long);
static obj_t BGl_z62mmapzd2readzd2positionzd2setz12za2zz__mmapz00(obj_t, obj_t, obj_t);
extern obj_t string_append(obj_t, obj_t);
BGL_EXPORTED_DECL unsigned char BGl_mmapzd2getzd2charz00zz__mmapz00(obj_t);
static obj_t BGl_z62mmapzd2writezd2positionzd2setz12za2zz__mmapz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL long BGl_mmapzd2writezd2positionz00zz__mmapz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_closezd2mmapzd2zz__mmapz00(obj_t);
extern obj_t make_string_sans_fill(long);
BGL_EXPORTED_DECL long BGl_mmapzd2readzd2positionz00zz__mmapz00(obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62mmapzd2setz12za2zz__mmapz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl__stringzd2ze3mmapz31zz__mmapz00(obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string1870z00zz__mmapz00, BgL_bgl_string1870za700za7za7_1903za7, "&mmap-write-position", 20 );
DEFINE_STRING( BGl_string1871z00zz__mmapz00, BgL_bgl_string1871za700za7za7_1904za7, "&mmap-write-position-set!", 25 );
DEFINE_STRING( BGl_string1872z00zz__mmapz00, BgL_bgl_string1872za700za7za7_1905za7, "&mmap-ref-ur", 12 );
DEFINE_STRING( BGl_string1873z00zz__mmapz00, BgL_bgl_string1873za700za7za7_1906za7, "&mmap-set-ur!", 13 );
DEFINE_STRING( BGl_string1874z00zz__mmapz00, BgL_bgl_string1874za700za7za7_1907za7, "bchar", 5 );
DEFINE_STRING( BGl_string1875z00zz__mmapz00, BgL_bgl_string1875za700za7za7_1908za7, "index out of range [0..", 23 );
DEFINE_STRING( BGl_string1876z00zz__mmapz00, BgL_bgl_string1876za700za7za7_1909za7, "]", 1 );
DEFINE_STRING( BGl_string1878z00zz__mmapz00, BgL_bgl_string1878za700za7za7_1910za7, "mmap-ref", 8 );
DEFINE_STRING( BGl_string1879z00zz__mmapz00, BgL_bgl_string1879za700za7za7_1911za7, "&mmap-ref", 9 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2writezd2positionzd2envzd2zz__mmapz00, BgL_bgl_za762mmapza7d2writeza71912za7, BGl_z62mmapzd2writezd2positionz62zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1881z00zz__mmapz00, BgL_bgl_string1881za700za7za7_1913za7, "mmap-set!", 9 );
DEFINE_STRING( BGl_string1882z00zz__mmapz00, BgL_bgl_string1882za700za7za7_1914za7, "&mmap-set!", 10 );
DEFINE_STRING( BGl_string1884z00zz__mmapz00, BgL_bgl_string1884za700za7za7_1915za7, "mmap-substring", 14 );
DEFINE_STRING( BGl_string1885z00zz__mmapz00, BgL_bgl_string1885za700za7za7_1916za7, "length too small", 16 );
DEFINE_STRING( BGl_string1886z00zz__mmapz00, BgL_bgl_string1886za700za7za7_1917za7, "string-set!", 11 );
DEFINE_STRING( BGl_string1887z00zz__mmapz00, BgL_bgl_string1887za700za7za7_1918za7, "Illegal index", 13 );
DEFINE_STRING( BGl_string1888z00zz__mmapz00, BgL_bgl_string1888za700za7za7_1919za7, "start+length bigger than ", 25 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2lengthzd2envz00zz__mmapz00, BgL_bgl_za762mmapza7d2length1920z00, BGl_z62mmapzd2lengthzb0zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1889z00zz__mmapz00, BgL_bgl_string1889za700za7za7_1921za7, "&mmap-substring", 15 );
DEFINE_STRING( BGl_string1890z00zz__mmapz00, BgL_bgl_string1890za700za7za7_1922za7, "mmap-substring-set!", 19 );
DEFINE_STRING( BGl_string1891z00zz__mmapz00, BgL_bgl_string1891za700za7za7_1923za7, "string-ref", 10 );
DEFINE_STRING( BGl_string1893z00zz__mmapz00, BgL_bgl_string1893za700za7za7_1924za7, "mmap-sbustring-set!", 19 );
DEFINE_STRING( BGl_string1894z00zz__mmapz00, BgL_bgl_string1894za700za7za7_1925za7, "[", 1 );
DEFINE_STRING( BGl_string1896z00zz__mmapz00, BgL_bgl_string1896za700za7za7_1926za7, "&mmap-substring-set!", 20 );
DEFINE_STRING( BGl_string1897z00zz__mmapz00, BgL_bgl_string1897za700za7za7_1927za7, "mmap-get-char", 13 );
DEFINE_STRING( BGl_string1898z00zz__mmapz00, BgL_bgl_string1898za700za7za7_1928za7, "&mmap-get-char", 14 );
DEFINE_STRING( BGl_string1899z00zz__mmapz00, BgL_bgl_string1899za700za7za7_1929za7, "&mmap-put-char!", 15 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2putzd2charz12zd2envzc0zz__mmapz00, BgL_bgl_za762mmapza7d2putza7d21930za7, BGl_z62mmapzd2putzd2charz12z70zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2writezd2positionzd2setz12zd2envz12zz__mmapz00, BgL_bgl_za762mmapza7d2writeza71931za7, BGl_z62mmapzd2writezd2positionzd2setz12za2zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2getzd2stringzd2envzd2zz__mmapz00, BgL_bgl_za762mmapza7d2getza7d21932za7, BGl_z62mmapzd2getzd2stringz62zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzf3zd2envz21zz__mmapz00, BgL_bgl_za762mmapza7f3za791za7za7_1933za7, BGl_z62mmapzf3z91zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_closezd2mmapzd2envz00zz__mmapz00, BgL_bgl_za762closeza7d2mmapza71934za7, BGl_z62closezd2mmapzb0zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2substringzd2setz12zd2envzc0zz__mmapz00, BgL_bgl_za762mmapza7d2substr1935z00, BGl_z62mmapzd2substringzd2setz12z70zz__mmapz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2readzd2positionzd2envzd2zz__mmapz00, BgL_bgl_za762mmapza7d2readza7d1936za7, BGl_z62mmapzd2readzd2positionz62zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2setz12zd2envz12zz__mmapz00, BgL_bgl_za762mmapza7d2setza7121937za7, BGl_z62mmapzd2setz12za2zz__mmapz00, 0L, BUNSPEC, 3 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_stringzd2ze3mmapzd2envze3zz__mmapz00, BgL_bgl__stringza7d2za7e3mma1938z00, opt_generic_entry, BGl__stringzd2ze3mmapz31zz__mmapz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2putzd2stringz12zd2envzc0zz__mmapz00, BgL_bgl_za762mmapza7d2putza7d21939za7, BGl_z62mmapzd2putzd2stringz12z70zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2getzd2charzd2envzd2zz__mmapz00, BgL_bgl_za762mmapza7d2getza7d21940za7, BGl_z62mmapzd2getzd2charz62zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2refzd2envz00zz__mmapz00, BgL_bgl_za762mmapza7d2refza7b01941za7, BGl_z62mmapzd2refzb0zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2namezd2envz00zz__mmapz00, BgL_bgl_za762mmapza7d2nameza7b1942za7, BGl_z62mmapzd2namezb0zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_openzd2mmapzd2envz00zz__mmapz00, BgL_bgl__openza7d2mmapza7d2za71943za7, opt_generic_entry, BGl__openzd2mmapzd2zz__mmapz00, BFALSE, -1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2ze3stringzd2envze3zz__mmapz00, BgL_bgl_za762mmapza7d2za7e3str1944za7, BGl_z62mmapzd2ze3stringz53zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string1900z00zz__mmapz00, BgL_bgl_string1900za700za7za7_1945za7, "&mmap-get-string", 16 );
DEFINE_STRING( BGl_string1901z00zz__mmapz00, BgL_bgl_string1901za700za7za7_1946za7, "&mmap-put-string!", 17 );
DEFINE_STRING( BGl_string1902z00zz__mmapz00, BgL_bgl_string1902za700za7za7_1947za7, "__mmap", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2ze3bstringzd2envze3zz__mmapz00, BgL_bgl_za762mmapza7d2za7e3bst1948za7, BGl_z62mmapzd2ze3bstringz53zz__mmapz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2readzd2positionzd2setz12zd2envz12zz__mmapz00, BgL_bgl_za762mmapza7d2readza7d1949za7, BGl_z62mmapzd2readzd2positionzd2setz12za2zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2setzd2urz12zd2envzc0zz__mmapz00, BgL_bgl_za762mmapza7d2setza7d21950za7, BGl_z62mmapzd2setzd2urz12z70zz__mmapz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string1843z00zz__mmapz00, BgL_bgl_string1843za700za7za7_1951za7, "/tmp/bigloo/runtime/Llib/mmap.scm", 33 );
DEFINE_STRING( BGl_string1844z00zz__mmapz00, BgL_bgl_string1844za700za7za7_1952za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string1847z00zz__mmapz00, BgL_bgl_string1847za700za7za7_1953za7, "read", 4 );
DEFINE_STRING( BGl_string1849z00zz__mmapz00, BgL_bgl_string1849za700za7za7_1954za7, "write", 5 );
DEFINE_STRING( BGl_string1851z00zz__mmapz00, BgL_bgl_string1851za700za7za7_1955za7, "open-mmap", 9 );
DEFINE_STRING( BGl_string1852z00zz__mmapz00, BgL_bgl_string1852za700za7za7_1956za7, "Illegal keyword argument", 24 );
DEFINE_STRING( BGl_string1853z00zz__mmapz00, BgL_bgl_string1853za700za7za7_1957za7, "_open-mmap", 10 );
DEFINE_STRING( BGl_string1854z00zz__mmapz00, BgL_bgl_string1854za700za7za7_1958za7, "bint", 4 );
DEFINE_STRING( BGl_string1855z00zz__mmapz00, BgL_bgl_string1855za700za7za7_1959za7, "bstring", 7 );
DEFINE_STRING( BGl_string1856z00zz__mmapz00, BgL_bgl_string1856za700za7za7_1960za7, "wrong number of arguments: [1..3] expected, provided", 52 );
DEFINE_STRING( BGl_string1858z00zz__mmapz00, BgL_bgl_string1858za700za7za7_1961za7, "string->mmap", 12 );
DEFINE_STRING( BGl_string1859z00zz__mmapz00, BgL_bgl_string1859za700za7za7_1962za7, "_string->mmap", 13 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2refzd2urzd2envzd2zz__mmapz00, BgL_bgl_za762mmapza7d2refza7d21963za7, BGl_z62mmapzd2refzd2urz62zz__mmapz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string1860z00zz__mmapz00, BgL_bgl_string1860za700za7za7_1964za7, "mmap-name", 9 );
DEFINE_STRING( BGl_string1861z00zz__mmapz00, BgL_bgl_string1861za700za7za7_1965za7, "&mmap-name", 10 );
DEFINE_STRING( BGl_string1862z00zz__mmapz00, BgL_bgl_string1862za700za7za7_1966za7, "mmap", 4 );
DEFINE_STRING( BGl_string1863z00zz__mmapz00, BgL_bgl_string1863za700za7za7_1967za7, "&mmap->string", 13 );
DEFINE_STRING( BGl_string1864z00zz__mmapz00, BgL_bgl_string1864za700za7za7_1968za7, "&mmap->bstring", 14 );
DEFINE_STRING( BGl_string1865z00zz__mmapz00, BgL_bgl_string1865za700za7za7_1969za7, "&close-mmap", 11 );
DEFINE_STRING( BGl_string1866z00zz__mmapz00, BgL_bgl_string1866za700za7za7_1970za7, "&mmap-length", 12 );
DEFINE_STRING( BGl_string1867z00zz__mmapz00, BgL_bgl_string1867za700za7za7_1971za7, "&mmap-read-position", 19 );
DEFINE_STRING( BGl_string1868z00zz__mmapz00, BgL_bgl_string1868za700za7za7_1972za7, "&mmap-read-position-set!", 24 );
DEFINE_STRING( BGl_string1869z00zz__mmapz00, BgL_bgl_string1869za700za7za7_1973za7, "belong", 6 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_mmapzd2substringzd2envz00zz__mmapz00, BgL_bgl_za762mmapza7d2substr1974z00, BGl_z62mmapzd2substringzb0zz__mmapz00, 0L, BUNSPEC, 3 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1850z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1857z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1877z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_list1845z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1880z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1883z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1892z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_symbol1895z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_keyword1846z00zz__mmapz00) );
ADD_ROOT( (void *)(&BGl_keyword1848z00zz__mmapz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__mmapz00(long BgL_checksumz00_2342, char * BgL_fromz00_2343)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__mmapz00))
{ 
BGl_requirezd2initializa7ationz75zz__mmapz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__mmapz00(); 
BGl_cnstzd2initzd2zz__mmapz00(); 
BGl_importedzd2moduleszd2initz00zz__mmapz00(); 
return 
BGl_methodzd2initzd2zz__mmapz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__mmapz00(void)
{
{ /* Llib/mmap.scm 19 */
BGl_keyword1846z00zz__mmapz00 = 
bstring_to_keyword(BGl_string1847z00zz__mmapz00); 
BGl_keyword1848z00zz__mmapz00 = 
bstring_to_keyword(BGl_string1849z00zz__mmapz00); 
BGl_list1845z00zz__mmapz00 = 
MAKE_YOUNG_PAIR(BGl_keyword1846z00zz__mmapz00, 
MAKE_YOUNG_PAIR(BGl_keyword1848z00zz__mmapz00, BNIL)); 
BGl_symbol1850z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1851z00zz__mmapz00); 
BGl_symbol1857z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1858z00zz__mmapz00); 
BGl_symbol1877z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1878z00zz__mmapz00); 
BGl_symbol1880z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1881z00zz__mmapz00); 
BGl_symbol1883z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1884z00zz__mmapz00); 
BGl_symbol1892z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1893z00zz__mmapz00); 
return ( 
BGl_symbol1895z00zz__mmapz00 = 
bstring_to_symbol(BGl_string1890z00zz__mmapz00), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__mmapz00(void)
{
{ /* Llib/mmap.scm 19 */
return 
bgl_gc_roots_register();} 

}



/* mmap? */
BGL_EXPORTED_DEF bool_t BGl_mmapzf3zf3zz__mmapz00(obj_t BgL_objz00_3)
{
{ /* Llib/mmap.scm 117 */
return 
BGL_MMAPP(BgL_objz00_3);} 

}



/* &mmap? */
obj_t BGl_z62mmapzf3z91zz__mmapz00(obj_t BgL_envz00_2114, obj_t BgL_objz00_2115)
{
{ /* Llib/mmap.scm 117 */
return 
BBOOL(
BGl_mmapzf3zf3zz__mmapz00(BgL_objz00_2115));} 

}



/* _open-mmap */
obj_t BGl__openzd2mmapzd2zz__mmapz00(obj_t BgL_env1102z00_8, obj_t BgL_opt1101z00_7)
{
{ /* Llib/mmap.scm 123 */
{ /* Llib/mmap.scm 123 */
 obj_t BgL_g1109z00_1143;
BgL_g1109z00_1143 = 
VECTOR_REF(BgL_opt1101z00_7,0L); 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_readz00_1144;
BgL_readz00_1144 = BTRUE; 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_writez00_1145;
BgL_writez00_1145 = BTRUE; 
{ /* Llib/mmap.scm 123 */

{ 
 long BgL_iz00_1146;
BgL_iz00_1146 = 1L; 
BgL_check1105z00_1147:
if(
(BgL_iz00_1146==
VECTOR_LENGTH(BgL_opt1101z00_7)))
{ /* Llib/mmap.scm 123 */BNIL; }  else 
{ /* Llib/mmap.scm 123 */
 bool_t BgL_test1977z00_2370;
{ /* Llib/mmap.scm 123 */
 obj_t BgL_arg1196z00_1153;
{ /* Llib/mmap.scm 123 */
 bool_t BgL_test1978z00_2371;
{ /* Llib/mmap.scm 123 */
 long BgL_tmpz00_2372;
BgL_tmpz00_2372 = 
VECTOR_LENGTH(BgL_opt1101z00_7); 
BgL_test1978z00_2371 = 
BOUND_CHECK(BgL_iz00_1146, BgL_tmpz00_2372); } 
if(BgL_test1978z00_2371)
{ /* Llib/mmap.scm 123 */
BgL_arg1196z00_1153 = 
VECTOR_REF(BgL_opt1101z00_7,BgL_iz00_1146); }  else 
{ 
 obj_t BgL_auxz00_2376;
BgL_auxz00_2376 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1844z00zz__mmapz00, BgL_opt1101z00_7, 
(int)(
VECTOR_LENGTH(BgL_opt1101z00_7)), 
(int)(BgL_iz00_1146)); 
FAILURE(BgL_auxz00_2376,BFALSE,BFALSE);} } 
BgL_test1977z00_2370 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1196z00_1153, BGl_list1845z00zz__mmapz00)); } 
if(BgL_test1977z00_2370)
{ 
 long BgL_iz00_2385;
BgL_iz00_2385 = 
(BgL_iz00_1146+2L); 
BgL_iz00_1146 = BgL_iz00_2385; 
goto BgL_check1105z00_1147;}  else 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_arg1194z00_1152;
{ /* Llib/mmap.scm 123 */
 bool_t BgL_test1979z00_2387;
{ /* Llib/mmap.scm 123 */
 long BgL_tmpz00_2388;
BgL_tmpz00_2388 = 
VECTOR_LENGTH(BgL_opt1101z00_7); 
BgL_test1979z00_2387 = 
BOUND_CHECK(BgL_iz00_1146, BgL_tmpz00_2388); } 
if(BgL_test1979z00_2387)
{ /* Llib/mmap.scm 123 */
BgL_arg1194z00_1152 = 
VECTOR_REF(BgL_opt1101z00_7,BgL_iz00_1146); }  else 
{ 
 obj_t BgL_auxz00_2392;
BgL_auxz00_2392 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1844z00zz__mmapz00, BgL_opt1101z00_7, 
(int)(
VECTOR_LENGTH(BgL_opt1101z00_7)), 
(int)(BgL_iz00_1146)); 
FAILURE(BgL_auxz00_2392,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol1850z00zz__mmapz00, BGl_string1852z00zz__mmapz00, BgL_arg1194z00_1152); } } } 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_index1107z00_1154;
BgL_index1107z00_1154 = 
BGl_search1104ze70ze7zz__mmapz00(
VECTOR_LENGTH(BgL_opt1101z00_7), BgL_opt1101z00_7, BGl_keyword1846z00zz__mmapz00, 1L); 
{ /* Llib/mmap.scm 123 */
 bool_t BgL_test1980z00_2402;
{ /* Llib/mmap.scm 123 */
 long BgL_n1z00_1721;
{ /* Llib/mmap.scm 123 */
 obj_t BgL_tmpz00_2403;
if(
INTEGERP(BgL_index1107z00_1154))
{ /* Llib/mmap.scm 123 */
BgL_tmpz00_2403 = BgL_index1107z00_1154
; }  else 
{ 
 obj_t BgL_auxz00_2406;
BgL_auxz00_2406 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1853z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1107z00_1154); 
FAILURE(BgL_auxz00_2406,BFALSE,BFALSE);} 
BgL_n1z00_1721 = 
(long)CINT(BgL_tmpz00_2403); } 
BgL_test1980z00_2402 = 
(BgL_n1z00_1721>=0L); } 
if(BgL_test1980z00_2402)
{ 
 long BgL_auxz00_2412;
{ /* Llib/mmap.scm 123 */
 obj_t BgL_tmpz00_2413;
if(
INTEGERP(BgL_index1107z00_1154))
{ /* Llib/mmap.scm 123 */
BgL_tmpz00_2413 = BgL_index1107z00_1154
; }  else 
{ 
 obj_t BgL_auxz00_2416;
BgL_auxz00_2416 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1853z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1107z00_1154); 
FAILURE(BgL_auxz00_2416,BFALSE,BFALSE);} 
BgL_auxz00_2412 = 
(long)CINT(BgL_tmpz00_2413); } 
BgL_readz00_1144 = 
VECTOR_REF(BgL_opt1101z00_7,BgL_auxz00_2412); }  else 
{ /* Llib/mmap.scm 123 */BFALSE; } } } 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_index1108z00_1156;
BgL_index1108z00_1156 = 
BGl_search1104ze70ze7zz__mmapz00(
VECTOR_LENGTH(BgL_opt1101z00_7), BgL_opt1101z00_7, BGl_keyword1848z00zz__mmapz00, 1L); 
{ /* Llib/mmap.scm 123 */
 bool_t BgL_test1983z00_2424;
{ /* Llib/mmap.scm 123 */
 long BgL_n1z00_1722;
{ /* Llib/mmap.scm 123 */
 obj_t BgL_tmpz00_2425;
if(
INTEGERP(BgL_index1108z00_1156))
{ /* Llib/mmap.scm 123 */
BgL_tmpz00_2425 = BgL_index1108z00_1156
; }  else 
{ 
 obj_t BgL_auxz00_2428;
BgL_auxz00_2428 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1853z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1108z00_1156); 
FAILURE(BgL_auxz00_2428,BFALSE,BFALSE);} 
BgL_n1z00_1722 = 
(long)CINT(BgL_tmpz00_2425); } 
BgL_test1983z00_2424 = 
(BgL_n1z00_1722>=0L); } 
if(BgL_test1983z00_2424)
{ 
 long BgL_auxz00_2434;
{ /* Llib/mmap.scm 123 */
 obj_t BgL_tmpz00_2435;
if(
INTEGERP(BgL_index1108z00_1156))
{ /* Llib/mmap.scm 123 */
BgL_tmpz00_2435 = BgL_index1108z00_1156
; }  else 
{ 
 obj_t BgL_auxz00_2438;
BgL_auxz00_2438 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1853z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1108z00_1156); 
FAILURE(BgL_auxz00_2438,BFALSE,BFALSE);} 
BgL_auxz00_2434 = 
(long)CINT(BgL_tmpz00_2435); } 
BgL_writez00_1145 = 
VECTOR_REF(BgL_opt1101z00_7,BgL_auxz00_2434); }  else 
{ /* Llib/mmap.scm 123 */BFALSE; } } } 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_arg1199z00_1158;
BgL_arg1199z00_1158 = 
VECTOR_REF(BgL_opt1101z00_7,0L); 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_readz00_1159;
BgL_readz00_1159 = BgL_readz00_1144; 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_writez00_1160;
BgL_writez00_1160 = BgL_writez00_1145; 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_namez00_1723;
if(
STRINGP(BgL_arg1199z00_1158))
{ /* Llib/mmap.scm 123 */
BgL_namez00_1723 = BgL_arg1199z00_1158; }  else 
{ 
 obj_t BgL_auxz00_2447;
BgL_auxz00_2447 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5352L), BGl_string1853z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_arg1199z00_1158); 
FAILURE(BgL_auxz00_2447,BFALSE,BFALSE);} 
{ /* Llib/mmap.scm 124 */
 bool_t BgL_auxz00_2453; bool_t BgL_tmpz00_2451;
BgL_auxz00_2453 = 
CBOOL(BgL_writez00_1160); 
BgL_tmpz00_2451 = 
CBOOL(BgL_readz00_1159); 
return 
bgl_open_mmap(BgL_namez00_1723, BgL_tmpz00_2451, BgL_auxz00_2453);} } } } } } } } } } 

}



/* search1104~0 */
obj_t BGl_search1104ze70ze7zz__mmapz00(long BgL_l1103z00_2172, obj_t BgL_opt1101z00_2171, obj_t BgL_k1z00_1140, long BgL_iz00_1141)
{
{ /* Llib/mmap.scm 123 */
BGl_search1104ze70ze7zz__mmapz00:
if(
(BgL_iz00_1141==BgL_l1103z00_2172))
{ /* Llib/mmap.scm 123 */
return 
BINT(-1L);}  else 
{ /* Llib/mmap.scm 123 */
if(
(BgL_iz00_1141==
(BgL_l1103z00_2172-1L)))
{ /* Llib/mmap.scm 123 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1850z00zz__mmapz00, BGl_string1856z00zz__mmapz00, 
BINT(
VECTOR_LENGTH(BgL_opt1101z00_2171)));}  else 
{ /* Llib/mmap.scm 123 */
 obj_t BgL_vz00_1165;
BgL_vz00_1165 = 
VECTOR_REF(BgL_opt1101z00_2171,BgL_iz00_1141); 
if(
(BgL_vz00_1165==BgL_k1z00_1140))
{ /* Llib/mmap.scm 123 */
return 
BINT(
(BgL_iz00_1141+1L));}  else 
{ 
 long BgL_iz00_2470;
BgL_iz00_2470 = 
(BgL_iz00_1141+2L); 
BgL_iz00_1141 = BgL_iz00_2470; 
goto BGl_search1104ze70ze7zz__mmapz00;} } } } 

}



/* open-mmap */
BGL_EXPORTED_DEF obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t BgL_namez00_4, obj_t BgL_readz00_5, obj_t BgL_writez00_6)
{
{ /* Llib/mmap.scm 123 */
{ /* Llib/mmap.scm 124 */
 bool_t BgL_auxz00_2474; bool_t BgL_tmpz00_2472;
BgL_auxz00_2474 = 
CBOOL(BgL_writez00_6); 
BgL_tmpz00_2472 = 
CBOOL(BgL_readz00_5); 
return 
bgl_open_mmap(BgL_namez00_4, BgL_tmpz00_2472, BgL_auxz00_2474);} } 

}



/* _string->mmap */
obj_t BGl__stringzd2ze3mmapz31zz__mmapz00(obj_t BgL_env1111z00_13, obj_t BgL_opt1110z00_12)
{
{ /* Llib/mmap.scm 129 */
{ /* Llib/mmap.scm 129 */
 obj_t BgL_g1118z00_1172;
BgL_g1118z00_1172 = 
VECTOR_REF(BgL_opt1110z00_12,0L); 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_readz00_1173;
BgL_readz00_1173 = BTRUE; 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_writez00_1174;
BgL_writez00_1174 = BTRUE; 
{ /* Llib/mmap.scm 129 */

{ 
 long BgL_iz00_1175;
BgL_iz00_1175 = 1L; 
BgL_check1114z00_1176:
if(
(BgL_iz00_1175==
VECTOR_LENGTH(BgL_opt1110z00_12)))
{ /* Llib/mmap.scm 129 */BNIL; }  else 
{ /* Llib/mmap.scm 129 */
 bool_t BgL_test1991z00_2481;
{ /* Llib/mmap.scm 129 */
 obj_t BgL_arg1216z00_1182;
{ /* Llib/mmap.scm 129 */
 bool_t BgL_test1992z00_2482;
{ /* Llib/mmap.scm 129 */
 long BgL_tmpz00_2483;
BgL_tmpz00_2483 = 
VECTOR_LENGTH(BgL_opt1110z00_12); 
BgL_test1992z00_2482 = 
BOUND_CHECK(BgL_iz00_1175, BgL_tmpz00_2483); } 
if(BgL_test1992z00_2482)
{ /* Llib/mmap.scm 129 */
BgL_arg1216z00_1182 = 
VECTOR_REF(BgL_opt1110z00_12,BgL_iz00_1175); }  else 
{ 
 obj_t BgL_auxz00_2487;
BgL_auxz00_2487 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1844z00zz__mmapz00, BgL_opt1110z00_12, 
(int)(
VECTOR_LENGTH(BgL_opt1110z00_12)), 
(int)(BgL_iz00_1175)); 
FAILURE(BgL_auxz00_2487,BFALSE,BFALSE);} } 
BgL_test1991z00_2481 = 
CBOOL(
BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1216z00_1182, BGl_list1845z00zz__mmapz00)); } 
if(BgL_test1991z00_2481)
{ 
 long BgL_iz00_2496;
BgL_iz00_2496 = 
(BgL_iz00_1175+2L); 
BgL_iz00_1175 = BgL_iz00_2496; 
goto BgL_check1114z00_1176;}  else 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_arg1215z00_1181;
{ /* Llib/mmap.scm 129 */
 bool_t BgL_test1993z00_2498;
{ /* Llib/mmap.scm 129 */
 long BgL_tmpz00_2499;
BgL_tmpz00_2499 = 
VECTOR_LENGTH(BgL_opt1110z00_12); 
BgL_test1993z00_2498 = 
BOUND_CHECK(BgL_iz00_1175, BgL_tmpz00_2499); } 
if(BgL_test1993z00_2498)
{ /* Llib/mmap.scm 129 */
BgL_arg1215z00_1181 = 
VECTOR_REF(BgL_opt1110z00_12,BgL_iz00_1175); }  else 
{ 
 obj_t BgL_auxz00_2503;
BgL_auxz00_2503 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1844z00zz__mmapz00, BgL_opt1110z00_12, 
(int)(
VECTOR_LENGTH(BgL_opt1110z00_12)), 
(int)(BgL_iz00_1175)); 
FAILURE(BgL_auxz00_2503,BFALSE,BFALSE);} } 
BGl_errorz00zz__errorz00(BGl_symbol1857z00zz__mmapz00, BGl_string1852z00zz__mmapz00, BgL_arg1215z00_1181); } } } 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_index1116z00_1183;
BgL_index1116z00_1183 = 
BGl_search1113ze70ze7zz__mmapz00(
VECTOR_LENGTH(BgL_opt1110z00_12), BgL_opt1110z00_12, BGl_keyword1846z00zz__mmapz00, 1L); 
{ /* Llib/mmap.scm 129 */
 bool_t BgL_test1994z00_2513;
{ /* Llib/mmap.scm 129 */
 long BgL_n1z00_1739;
{ /* Llib/mmap.scm 129 */
 obj_t BgL_tmpz00_2514;
if(
INTEGERP(BgL_index1116z00_1183))
{ /* Llib/mmap.scm 129 */
BgL_tmpz00_2514 = BgL_index1116z00_1183
; }  else 
{ 
 obj_t BgL_auxz00_2517;
BgL_auxz00_2517 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1859z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1116z00_1183); 
FAILURE(BgL_auxz00_2517,BFALSE,BFALSE);} 
BgL_n1z00_1739 = 
(long)CINT(BgL_tmpz00_2514); } 
BgL_test1994z00_2513 = 
(BgL_n1z00_1739>=0L); } 
if(BgL_test1994z00_2513)
{ 
 long BgL_auxz00_2523;
{ /* Llib/mmap.scm 129 */
 obj_t BgL_tmpz00_2524;
if(
INTEGERP(BgL_index1116z00_1183))
{ /* Llib/mmap.scm 129 */
BgL_tmpz00_2524 = BgL_index1116z00_1183
; }  else 
{ 
 obj_t BgL_auxz00_2527;
BgL_auxz00_2527 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1859z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1116z00_1183); 
FAILURE(BgL_auxz00_2527,BFALSE,BFALSE);} 
BgL_auxz00_2523 = 
(long)CINT(BgL_tmpz00_2524); } 
BgL_readz00_1173 = 
VECTOR_REF(BgL_opt1110z00_12,BgL_auxz00_2523); }  else 
{ /* Llib/mmap.scm 129 */BFALSE; } } } 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_index1117z00_1185;
BgL_index1117z00_1185 = 
BGl_search1113ze70ze7zz__mmapz00(
VECTOR_LENGTH(BgL_opt1110z00_12), BgL_opt1110z00_12, BGl_keyword1848z00zz__mmapz00, 1L); 
{ /* Llib/mmap.scm 129 */
 bool_t BgL_test1997z00_2535;
{ /* Llib/mmap.scm 129 */
 long BgL_n1z00_1740;
{ /* Llib/mmap.scm 129 */
 obj_t BgL_tmpz00_2536;
if(
INTEGERP(BgL_index1117z00_1185))
{ /* Llib/mmap.scm 129 */
BgL_tmpz00_2536 = BgL_index1117z00_1185
; }  else 
{ 
 obj_t BgL_auxz00_2539;
BgL_auxz00_2539 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1859z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1117z00_1185); 
FAILURE(BgL_auxz00_2539,BFALSE,BFALSE);} 
BgL_n1z00_1740 = 
(long)CINT(BgL_tmpz00_2536); } 
BgL_test1997z00_2535 = 
(BgL_n1z00_1740>=0L); } 
if(BgL_test1997z00_2535)
{ 
 long BgL_auxz00_2545;
{ /* Llib/mmap.scm 129 */
 obj_t BgL_tmpz00_2546;
if(
INTEGERP(BgL_index1117z00_1185))
{ /* Llib/mmap.scm 129 */
BgL_tmpz00_2546 = BgL_index1117z00_1185
; }  else 
{ 
 obj_t BgL_auxz00_2549;
BgL_auxz00_2549 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1859z00zz__mmapz00, BGl_string1854z00zz__mmapz00, BgL_index1117z00_1185); 
FAILURE(BgL_auxz00_2549,BFALSE,BFALSE);} 
BgL_auxz00_2545 = 
(long)CINT(BgL_tmpz00_2546); } 
BgL_writez00_1174 = 
VECTOR_REF(BgL_opt1110z00_12,BgL_auxz00_2545); }  else 
{ /* Llib/mmap.scm 129 */BFALSE; } } } 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_arg1219z00_1187;
BgL_arg1219z00_1187 = 
VECTOR_REF(BgL_opt1110z00_12,0L); 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_readz00_1188;
BgL_readz00_1188 = BgL_readz00_1173; 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_writez00_1189;
BgL_writez00_1189 = BgL_writez00_1174; 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_sz00_1741;
if(
STRINGP(BgL_arg1219z00_1187))
{ /* Llib/mmap.scm 129 */
BgL_sz00_1741 = BgL_arg1219z00_1187; }  else 
{ 
 obj_t BgL_auxz00_2558;
BgL_auxz00_2558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(5669L), BGl_string1859z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_arg1219z00_1187); 
FAILURE(BgL_auxz00_2558,BFALSE,BFALSE);} 
{ /* Llib/mmap.scm 130 */
 bool_t BgL_auxz00_2564; bool_t BgL_tmpz00_2562;
BgL_auxz00_2564 = 
CBOOL(BgL_writez00_1189); 
BgL_tmpz00_2562 = 
CBOOL(BgL_readz00_1188); 
return 
bgl_string_to_mmap(BgL_sz00_1741, BgL_tmpz00_2562, BgL_auxz00_2564);} } } } } } } } } } 

}



/* search1113~0 */
obj_t BGl_search1113ze70ze7zz__mmapz00(long BgL_l1112z00_2170, obj_t BgL_opt1110z00_2169, obj_t BgL_k1z00_1169, long BgL_iz00_1170)
{
{ /* Llib/mmap.scm 129 */
BGl_search1113ze70ze7zz__mmapz00:
if(
(BgL_iz00_1170==BgL_l1112z00_2170))
{ /* Llib/mmap.scm 129 */
return 
BINT(-1L);}  else 
{ /* Llib/mmap.scm 129 */
if(
(BgL_iz00_1170==
(BgL_l1112z00_2170-1L)))
{ /* Llib/mmap.scm 129 */
return 
BGl_errorz00zz__errorz00(BGl_symbol1857z00zz__mmapz00, BGl_string1856z00zz__mmapz00, 
BINT(
VECTOR_LENGTH(BgL_opt1110z00_2169)));}  else 
{ /* Llib/mmap.scm 129 */
 obj_t BgL_vz00_1194;
BgL_vz00_1194 = 
VECTOR_REF(BgL_opt1110z00_2169,BgL_iz00_1170); 
if(
(BgL_vz00_1194==BgL_k1z00_1169))
{ /* Llib/mmap.scm 129 */
return 
BINT(
(BgL_iz00_1170+1L));}  else 
{ 
 long BgL_iz00_2581;
BgL_iz00_2581 = 
(BgL_iz00_1170+2L); 
BgL_iz00_1170 = BgL_iz00_2581; 
goto BGl_search1113ze70ze7zz__mmapz00;} } } } 

}



/* string->mmap */
BGL_EXPORTED_DEF obj_t BGl_stringzd2ze3mmapz31zz__mmapz00(obj_t BgL_sz00_9, obj_t BgL_readz00_10, obj_t BgL_writez00_11)
{
{ /* Llib/mmap.scm 129 */
{ /* Llib/mmap.scm 130 */
 bool_t BgL_auxz00_2585; bool_t BgL_tmpz00_2583;
BgL_auxz00_2585 = 
CBOOL(BgL_writez00_11); 
BgL_tmpz00_2583 = 
CBOOL(BgL_readz00_10); 
return 
bgl_string_to_mmap(BgL_sz00_9, BgL_tmpz00_2583, BgL_auxz00_2585);} } 

}



/* mmap-name */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2namezd2zz__mmapz00(obj_t BgL_mmapz00_14)
{
{ /* Llib/mmap.scm 135 */
{ /* Llib/mmap.scm 136 */
 obj_t BgL_aux1763z00_2298;
BgL_aux1763z00_2298 = 
BGL_MMAP_NAME(BgL_mmapz00_14); 
if(
STRINGP(BgL_aux1763z00_2298))
{ /* Llib/mmap.scm 136 */
return BgL_aux1763z00_2298;}  else 
{ 
 obj_t BgL_auxz00_2591;
BgL_auxz00_2591 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(6027L), BGl_string1860z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_aux1763z00_2298); 
FAILURE(BgL_auxz00_2591,BFALSE,BFALSE);} } } 

}



/* &mmap-name */
obj_t BGl_z62mmapzd2namezb0zz__mmapz00(obj_t BgL_envz00_2116, obj_t BgL_mmapz00_2117)
{
{ /* Llib/mmap.scm 135 */
{ /* Llib/mmap.scm 136 */
 obj_t BgL_auxz00_2595;
if(
BGL_MMAPP(BgL_mmapz00_2117))
{ /* Llib/mmap.scm 136 */
BgL_auxz00_2595 = BgL_mmapz00_2117
; }  else 
{ 
 obj_t BgL_auxz00_2598;
BgL_auxz00_2598 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(6027L), BGl_string1861z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmapz00_2117); 
FAILURE(BgL_auxz00_2598,BFALSE,BFALSE);} 
return 
BGl_mmapzd2namezd2zz__mmapz00(BgL_auxz00_2595);} } 

}



/* mmap->string */
BGL_EXPORTED_DEF char * BGl_mmapzd2ze3stringz31zz__mmapz00(obj_t BgL_mmapz00_15)
{
{ /* Llib/mmap.scm 141 */
return 
BGL_MMAP_TO_STRING(BgL_mmapz00_15);} 

}



/* &mmap->string */
obj_t BGl_z62mmapzd2ze3stringz53zz__mmapz00(obj_t BgL_envz00_2118, obj_t BgL_mmapz00_2119)
{
{ /* Llib/mmap.scm 141 */
{ /* Llib/mmap.scm 142 */
 char * BgL_tmpz00_2604;
{ /* Llib/mmap.scm 142 */
 obj_t BgL_auxz00_2605;
if(
BGL_MMAPP(BgL_mmapz00_2119))
{ /* Llib/mmap.scm 142 */
BgL_auxz00_2605 = BgL_mmapz00_2119
; }  else 
{ 
 obj_t BgL_auxz00_2608;
BgL_auxz00_2608 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(6321L), BGl_string1863z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmapz00_2119); 
FAILURE(BgL_auxz00_2608,BFALSE,BFALSE);} 
BgL_tmpz00_2604 = 
BGl_mmapzd2ze3stringz31zz__mmapz00(BgL_auxz00_2605); } 
return 
string_to_bstring(BgL_tmpz00_2604);} } 

}



/* mmap->bstring */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2ze3bstringz31zz__mmapz00(obj_t BgL_mmapz00_16)
{
{ /* Llib/mmap.scm 147 */
{ /* Llib/mmap.scm 148 */
 int BgL_lenz00_2299;
{ /* Llib/mmap.scm 148 */
 long BgL_arg1228z00_2300;
BgL_arg1228z00_2300 = 
BGL_MMAP_LENGTH(BgL_mmapz00_16); 
BgL_lenz00_2299 = 
(int)(
(long)(BgL_arg1228z00_2300)); } 
{ /* Llib/mmap.scm 149 */
 char * BgL_arg1227z00_2301;
BgL_arg1227z00_2301 = 
BGL_MMAP_TO_STRING(BgL_mmapz00_16); 
return 
string_to_bstring_len(BgL_arg1227z00_2301, BgL_lenz00_2299);} } } 

}



/* &mmap->bstring */
obj_t BGl_z62mmapzd2ze3bstringz53zz__mmapz00(obj_t BgL_envz00_2120, obj_t BgL_mmapz00_2121)
{
{ /* Llib/mmap.scm 147 */
{ /* Llib/mmap.scm 148 */
 obj_t BgL_auxz00_2619;
if(
BGL_MMAPP(BgL_mmapz00_2121))
{ /* Llib/mmap.scm 148 */
BgL_auxz00_2619 = BgL_mmapz00_2121
; }  else 
{ 
 obj_t BgL_auxz00_2622;
BgL_auxz00_2622 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(6620L), BGl_string1864z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmapz00_2121); 
FAILURE(BgL_auxz00_2622,BFALSE,BFALSE);} 
return 
BGl_mmapzd2ze3bstringz31zz__mmapz00(BgL_auxz00_2619);} } 

}



/* close-mmap */
BGL_EXPORTED_DEF obj_t BGl_closezd2mmapzd2zz__mmapz00(obj_t BgL_mmapz00_17)
{
{ /* Llib/mmap.scm 154 */
return 
bgl_close_mmap(BgL_mmapz00_17);} 

}



/* &close-mmap */
obj_t BGl_z62closezd2mmapzb0zz__mmapz00(obj_t BgL_envz00_2122, obj_t BgL_mmapz00_2123)
{
{ /* Llib/mmap.scm 154 */
{ /* Llib/mmap.scm 155 */
 obj_t BgL_auxz00_2628;
if(
BGL_MMAPP(BgL_mmapz00_2123))
{ /* Llib/mmap.scm 155 */
BgL_auxz00_2628 = BgL_mmapz00_2123
; }  else 
{ 
 obj_t BgL_auxz00_2631;
BgL_auxz00_2631 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(6996L), BGl_string1865z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmapz00_2123); 
FAILURE(BgL_auxz00_2631,BFALSE,BFALSE);} 
return 
BGl_closezd2mmapzd2zz__mmapz00(BgL_auxz00_2628);} } 

}



/* mmap-length */
BGL_EXPORTED_DEF long BGl_mmapzd2lengthzd2zz__mmapz00(obj_t BgL_objz00_18)
{
{ /* Llib/mmap.scm 160 */
return 
BGL_MMAP_LENGTH(BgL_objz00_18);} 

}



/* &mmap-length */
obj_t BGl_z62mmapzd2lengthzb0zz__mmapz00(obj_t BgL_envz00_2124, obj_t BgL_objz00_2125)
{
{ /* Llib/mmap.scm 160 */
{ /* Llib/mmap.scm 161 */
 long BgL_tmpz00_2637;
{ /* Llib/mmap.scm 161 */
 obj_t BgL_auxz00_2638;
if(
BGL_MMAPP(BgL_objz00_2125))
{ /* Llib/mmap.scm 161 */
BgL_auxz00_2638 = BgL_objz00_2125
; }  else 
{ 
 obj_t BgL_auxz00_2641;
BgL_auxz00_2641 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(7281L), BGl_string1866z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_objz00_2125); 
FAILURE(BgL_auxz00_2641,BFALSE,BFALSE);} 
BgL_tmpz00_2637 = 
BGl_mmapzd2lengthzd2zz__mmapz00(BgL_auxz00_2638); } 
return 
make_belong(BgL_tmpz00_2637);} } 

}



/* mmap-read-position */
BGL_EXPORTED_DEF long BGl_mmapzd2readzd2positionz00zz__mmapz00(obj_t BgL_mmz00_19)
{
{ /* Llib/mmap.scm 166 */
return 
BGL_MMAP_RP_GET(BgL_mmz00_19);} 

}



/* &mmap-read-position */
obj_t BGl_z62mmapzd2readzd2positionz62zz__mmapz00(obj_t BgL_envz00_2126, obj_t BgL_mmz00_2127)
{
{ /* Llib/mmap.scm 166 */
{ /* Llib/mmap.scm 167 */
 long BgL_tmpz00_2648;
{ /* Llib/mmap.scm 167 */
 obj_t BgL_auxz00_2649;
if(
BGL_MMAPP(BgL_mmz00_2127))
{ /* Llib/mmap.scm 167 */
BgL_auxz00_2649 = BgL_mmz00_2127
; }  else 
{ 
 obj_t BgL_auxz00_2652;
BgL_auxz00_2652 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(7572L), BGl_string1867z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2127); 
FAILURE(BgL_auxz00_2652,BFALSE,BFALSE);} 
BgL_tmpz00_2648 = 
BGl_mmapzd2readzd2positionz00zz__mmapz00(BgL_auxz00_2649); } 
return 
make_belong(BgL_tmpz00_2648);} } 

}



/* mmap-read-position-set! */
BGL_EXPORTED_DEF long BGl_mmapzd2readzd2positionzd2setz12zc0zz__mmapz00(obj_t BgL_mmz00_20, long BgL_pz00_21)
{
{ /* Llib/mmap.scm 172 */
BGL_MMAP_RP_SET(BgL_mmz00_20, BgL_pz00_21); BUNSPEC; 
return BgL_pz00_21;} 

}



/* &mmap-read-position-set! */
obj_t BGl_z62mmapzd2readzd2positionzd2setz12za2zz__mmapz00(obj_t BgL_envz00_2128, obj_t BgL_mmz00_2129, obj_t BgL_pz00_2130)
{
{ /* Llib/mmap.scm 172 */
{ /* Llib/mmap.scm 173 */
 long BgL_tmpz00_2659;
{ /* Llib/mmap.scm 173 */
 long BgL_auxz00_2667; obj_t BgL_auxz00_2660;
{ /* Llib/mmap.scm 173 */
 obj_t BgL_tmpz00_2668;
if(
ELONGP(BgL_pz00_2130))
{ /* Llib/mmap.scm 173 */
BgL_tmpz00_2668 = BgL_pz00_2130
; }  else 
{ 
 obj_t BgL_auxz00_2671;
BgL_auxz00_2671 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(7884L), BGl_string1868z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_pz00_2130); 
FAILURE(BgL_auxz00_2671,BFALSE,BFALSE);} 
BgL_auxz00_2667 = 
BELONG_TO_LONG(BgL_tmpz00_2668); } 
if(
BGL_MMAPP(BgL_mmz00_2129))
{ /* Llib/mmap.scm 173 */
BgL_auxz00_2660 = BgL_mmz00_2129
; }  else 
{ 
 obj_t BgL_auxz00_2663;
BgL_auxz00_2663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(7884L), BGl_string1868z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2129); 
FAILURE(BgL_auxz00_2663,BFALSE,BFALSE);} 
BgL_tmpz00_2659 = 
BGl_mmapzd2readzd2positionzd2setz12zc0zz__mmapz00(BgL_auxz00_2660, BgL_auxz00_2667); } 
return 
make_belong(BgL_tmpz00_2659);} } 

}



/* mmap-write-position */
BGL_EXPORTED_DEF long BGl_mmapzd2writezd2positionz00zz__mmapz00(obj_t BgL_mmz00_22)
{
{ /* Llib/mmap.scm 179 */
return 
BGL_MMAP_WP_GET(BgL_mmz00_22);} 

}



/* &mmap-write-position */
obj_t BGl_z62mmapzd2writezd2positionz62zz__mmapz00(obj_t BgL_envz00_2131, obj_t BgL_mmz00_2132)
{
{ /* Llib/mmap.scm 179 */
{ /* Llib/mmap.scm 180 */
 long BgL_tmpz00_2679;
{ /* Llib/mmap.scm 180 */
 obj_t BgL_auxz00_2680;
if(
BGL_MMAPP(BgL_mmz00_2132))
{ /* Llib/mmap.scm 180 */
BgL_auxz00_2680 = BgL_mmz00_2132
; }  else 
{ 
 obj_t BgL_auxz00_2683;
BgL_auxz00_2683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(8164L), BGl_string1870z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2132); 
FAILURE(BgL_auxz00_2683,BFALSE,BFALSE);} 
BgL_tmpz00_2679 = 
BGl_mmapzd2writezd2positionz00zz__mmapz00(BgL_auxz00_2680); } 
return 
make_belong(BgL_tmpz00_2679);} } 

}



/* mmap-write-position-set! */
BGL_EXPORTED_DEF long BGl_mmapzd2writezd2positionzd2setz12zc0zz__mmapz00(obj_t BgL_mmz00_23, long BgL_pz00_24)
{
{ /* Llib/mmap.scm 185 */
BGL_MMAP_WP_SET(BgL_mmz00_23, BgL_pz00_24); BUNSPEC; 
return BgL_pz00_24;} 

}



/* &mmap-write-position-set! */
obj_t BGl_z62mmapzd2writezd2positionzd2setz12za2zz__mmapz00(obj_t BgL_envz00_2133, obj_t BgL_mmz00_2134, obj_t BgL_pz00_2135)
{
{ /* Llib/mmap.scm 185 */
{ /* Llib/mmap.scm 186 */
 long BgL_tmpz00_2690;
{ /* Llib/mmap.scm 186 */
 long BgL_auxz00_2698; obj_t BgL_auxz00_2691;
{ /* Llib/mmap.scm 186 */
 obj_t BgL_tmpz00_2699;
if(
ELONGP(BgL_pz00_2135))
{ /* Llib/mmap.scm 186 */
BgL_tmpz00_2699 = BgL_pz00_2135
; }  else 
{ 
 obj_t BgL_auxz00_2702;
BgL_auxz00_2702 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(8477L), BGl_string1871z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_pz00_2135); 
FAILURE(BgL_auxz00_2702,BFALSE,BFALSE);} 
BgL_auxz00_2698 = 
BELONG_TO_LONG(BgL_tmpz00_2699); } 
if(
BGL_MMAPP(BgL_mmz00_2134))
{ /* Llib/mmap.scm 186 */
BgL_auxz00_2691 = BgL_mmz00_2134
; }  else 
{ 
 obj_t BgL_auxz00_2694;
BgL_auxz00_2694 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(8477L), BGl_string1871z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2134); 
FAILURE(BgL_auxz00_2694,BFALSE,BFALSE);} 
BgL_tmpz00_2690 = 
BGl_mmapzd2writezd2positionzd2setz12zc0zz__mmapz00(BgL_auxz00_2691, BgL_auxz00_2698); } 
return 
make_belong(BgL_tmpz00_2690);} } 

}



/* mmap-ref-ur */
BGL_EXPORTED_DEF unsigned char BGl_mmapzd2refzd2urz00zz__mmapz00(obj_t BgL_mmz00_25, long BgL_iz00_26)
{
{ /* Llib/mmap.scm 192 */
{ /* Llib/mmap.scm 193 */
 unsigned char BgL_cz00_2302;
BgL_cz00_2302 = 
BGL_MMAP_REF(BgL_mmz00_25, BgL_iz00_26); 
{ /* Llib/mmap.scm 194 */
 long BgL_arg1229z00_2303;
BgL_arg1229z00_2303 = 
(BgL_iz00_26+((long)1)); 
BGL_MMAP_RP_SET(BgL_mmz00_25, BgL_arg1229z00_2303); BUNSPEC; BgL_arg1229z00_2303; } 
return BgL_cz00_2302;} } 

}



/* &mmap-ref-ur */
obj_t BGl_z62mmapzd2refzd2urz62zz__mmapz00(obj_t BgL_envz00_2136, obj_t BgL_mmz00_2137, obj_t BgL_iz00_2138)
{
{ /* Llib/mmap.scm 192 */
{ /* Llib/mmap.scm 193 */
 unsigned char BgL_tmpz00_2712;
{ /* Llib/mmap.scm 193 */
 long BgL_auxz00_2720; obj_t BgL_auxz00_2713;
{ /* Llib/mmap.scm 193 */
 obj_t BgL_tmpz00_2721;
if(
ELONGP(BgL_iz00_2138))
{ /* Llib/mmap.scm 193 */
BgL_tmpz00_2721 = BgL_iz00_2138
; }  else 
{ 
 obj_t BgL_auxz00_2724;
BgL_auxz00_2724 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(8765L), BGl_string1872z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_iz00_2138); 
FAILURE(BgL_auxz00_2724,BFALSE,BFALSE);} 
BgL_auxz00_2720 = 
BELONG_TO_LONG(BgL_tmpz00_2721); } 
if(
BGL_MMAPP(BgL_mmz00_2137))
{ /* Llib/mmap.scm 193 */
BgL_auxz00_2713 = BgL_mmz00_2137
; }  else 
{ 
 obj_t BgL_auxz00_2716;
BgL_auxz00_2716 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(8765L), BGl_string1872z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2137); 
FAILURE(BgL_auxz00_2716,BFALSE,BFALSE);} 
BgL_tmpz00_2712 = 
BGl_mmapzd2refzd2urz00zz__mmapz00(BgL_auxz00_2713, BgL_auxz00_2720); } 
return 
BCHAR(BgL_tmpz00_2712);} } 

}



/* mmap-set-ur! */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2setzd2urz12z12zz__mmapz00(obj_t BgL_mmz00_27, long BgL_iz00_28, unsigned char BgL_cz00_29)
{
{ /* Llib/mmap.scm 200 */
BGL_MMAP_SET(BgL_mmz00_27, BgL_iz00_28, BgL_cz00_29); 
{ /* Llib/mmap.scm 202 */
 long BgL_arg1230z00_2304;
BgL_arg1230z00_2304 = 
(BgL_iz00_28+((long)1)); 
BGL_MMAP_WP_SET(BgL_mmz00_27, BgL_arg1230z00_2304); BUNSPEC; 
return 
make_belong(BgL_arg1230z00_2304);} } 

}



/* &mmap-set-ur! */
obj_t BGl_z62mmapzd2setzd2urz12z70zz__mmapz00(obj_t BgL_envz00_2139, obj_t BgL_mmz00_2140, obj_t BgL_iz00_2141, obj_t BgL_cz00_2142)
{
{ /* Llib/mmap.scm 200 */
{ /* Llib/mmap.scm 201 */
 unsigned char BgL_auxz00_2751; long BgL_auxz00_2742; obj_t BgL_auxz00_2735;
{ /* Llib/mmap.scm 201 */
 obj_t BgL_tmpz00_2752;
if(
CHARP(BgL_cz00_2142))
{ /* Llib/mmap.scm 201 */
BgL_tmpz00_2752 = BgL_cz00_2142
; }  else 
{ 
 obj_t BgL_auxz00_2755;
BgL_auxz00_2755 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9159L), BGl_string1873z00zz__mmapz00, BGl_string1874z00zz__mmapz00, BgL_cz00_2142); 
FAILURE(BgL_auxz00_2755,BFALSE,BFALSE);} 
BgL_auxz00_2751 = 
CCHAR(BgL_tmpz00_2752); } 
{ /* Llib/mmap.scm 201 */
 obj_t BgL_tmpz00_2743;
if(
ELONGP(BgL_iz00_2141))
{ /* Llib/mmap.scm 201 */
BgL_tmpz00_2743 = BgL_iz00_2141
; }  else 
{ 
 obj_t BgL_auxz00_2746;
BgL_auxz00_2746 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9159L), BGl_string1873z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_iz00_2141); 
FAILURE(BgL_auxz00_2746,BFALSE,BFALSE);} 
BgL_auxz00_2742 = 
BELONG_TO_LONG(BgL_tmpz00_2743); } 
if(
BGL_MMAPP(BgL_mmz00_2140))
{ /* Llib/mmap.scm 201 */
BgL_auxz00_2735 = BgL_mmz00_2140
; }  else 
{ 
 obj_t BgL_auxz00_2738;
BgL_auxz00_2738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9159L), BGl_string1873z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2140); 
FAILURE(BgL_auxz00_2738,BFALSE,BFALSE);} 
return 
BGl_mmapzd2setzd2urz12z12zz__mmapz00(BgL_auxz00_2735, BgL_auxz00_2742, BgL_auxz00_2751);} } 

}



/* mmap-ref */
BGL_EXPORTED_DEF unsigned char BGl_mmapzd2refzd2zz__mmapz00(obj_t BgL_mmz00_30, long BgL_iz00_31)
{
{ /* Llib/mmap.scm 207 */
{ /* Llib/mmap.scm 208 */
 bool_t BgL_test2021z00_2761;
{ /* Llib/mmap.scm 208 */
 long BgL_arg1238z00_2305;
BgL_arg1238z00_2305 = 
BGL_MMAP_LENGTH(BgL_mmz00_30); 
BgL_test2021z00_2761 = 
BOUND_CHECK(BgL_iz00_31, BgL_arg1238z00_2305); } 
if(BgL_test2021z00_2761)
{ /* Llib/mmap.scm 209 */
 unsigned char BgL_res1721z00_2306;
{ /* Llib/mmap.scm 193 */
 unsigned char BgL_cz00_2307;
BgL_cz00_2307 = 
BGL_MMAP_REF(BgL_mmz00_30, BgL_iz00_31); 
{ /* Llib/mmap.scm 194 */
 long BgL_arg1229z00_2308;
BgL_arg1229z00_2308 = 
(BgL_iz00_31+((long)1)); 
BGL_MMAP_RP_SET(BgL_mmz00_30, BgL_arg1229z00_2308); BUNSPEC; BgL_arg1229z00_2308; } 
BgL_res1721z00_2306 = BgL_cz00_2307; } 
return BgL_res1721z00_2306;}  else 
{ /* Llib/mmap.scm 212 */
 obj_t BgL_arg1233z00_2309;
{ /* Llib/mmap.scm 212 */
 obj_t BgL_arg1234z00_2310;
{ /* Llib/mmap.scm 212 */
 obj_t BgL_arg1236z00_2311;
{ /* Llib/mmap.scm 212 */
 long BgL_a1084z00_2312;
BgL_a1084z00_2312 = 
BGL_MMAP_LENGTH(BgL_mmz00_30); 
{ /* Llib/mmap.scm 212 */

BgL_arg1236z00_2311 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(
make_belong(BgL_a1084z00_2312), 
BINT(1L)); } } 
{ /* Llib/mmap.scm 212 */

BgL_arg1234z00_2310 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1236z00_2311, 
BINT(10L)); } } 
BgL_arg1233z00_2309 = 
string_append_3(BGl_string1875z00zz__mmapz00, BgL_arg1234z00_2310, BGl_string1876z00zz__mmapz00); } 
{ /* Llib/mmap.scm 210 */
 obj_t BgL_tmpz00_2774;
{ /* Llib/mmap.scm 210 */
 obj_t BgL_aux1797z00_2313;
BgL_aux1797z00_2313 = 
BGl_errorz00zz__errorz00(BGl_symbol1877z00zz__mmapz00, BgL_arg1233z00_2309, 
make_belong(BgL_iz00_31)); 
if(
CHARP(BgL_aux1797z00_2313))
{ /* Llib/mmap.scm 210 */
BgL_tmpz00_2774 = BgL_aux1797z00_2313
; }  else 
{ 
 obj_t BgL_auxz00_2779;
BgL_auxz00_2779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9564L), BGl_string1878z00zz__mmapz00, BGl_string1874z00zz__mmapz00, BgL_aux1797z00_2313); 
FAILURE(BgL_auxz00_2779,BFALSE,BFALSE);} } 
return 
CCHAR(BgL_tmpz00_2774);} } } } 

}



/* &mmap-ref */
obj_t BGl_z62mmapzd2refzb0zz__mmapz00(obj_t BgL_envz00_2143, obj_t BgL_mmz00_2144, obj_t BgL_iz00_2145)
{
{ /* Llib/mmap.scm 207 */
{ /* Llib/mmap.scm 208 */
 unsigned char BgL_tmpz00_2784;
{ /* Llib/mmap.scm 208 */
 long BgL_auxz00_2792; obj_t BgL_auxz00_2785;
{ /* Llib/mmap.scm 208 */
 obj_t BgL_tmpz00_2793;
if(
ELONGP(BgL_iz00_2145))
{ /* Llib/mmap.scm 208 */
BgL_tmpz00_2793 = BgL_iz00_2145
; }  else 
{ 
 obj_t BgL_auxz00_2796;
BgL_auxz00_2796 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9487L), BGl_string1879z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_iz00_2145); 
FAILURE(BgL_auxz00_2796,BFALSE,BFALSE);} 
BgL_auxz00_2792 = 
BELONG_TO_LONG(BgL_tmpz00_2793); } 
if(
BGL_MMAPP(BgL_mmz00_2144))
{ /* Llib/mmap.scm 208 */
BgL_auxz00_2785 = BgL_mmz00_2144
; }  else 
{ 
 obj_t BgL_auxz00_2788;
BgL_auxz00_2788 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9487L), BGl_string1879z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2144); 
FAILURE(BgL_auxz00_2788,BFALSE,BFALSE);} 
BgL_tmpz00_2784 = 
BGl_mmapzd2refzd2zz__mmapz00(BgL_auxz00_2785, BgL_auxz00_2792); } 
return 
BCHAR(BgL_tmpz00_2784);} } 

}



/* mmap-set! */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2setz12zc0zz__mmapz00(obj_t BgL_mmz00_32, long BgL_iz00_33, unsigned char BgL_cz00_34)
{
{ /* Llib/mmap.scm 219 */
{ /* Llib/mmap.scm 220 */
 bool_t BgL_test2025z00_2803;
{ /* Llib/mmap.scm 220 */
 long BgL_arg1252z00_2314;
BgL_arg1252z00_2314 = 
BGL_MMAP_LENGTH(BgL_mmz00_32); 
BgL_test2025z00_2803 = 
BOUND_CHECK(BgL_iz00_33, BgL_arg1252z00_2314); } 
if(BgL_test2025z00_2803)
{ /* Llib/mmap.scm 220 */
BGL_MMAP_SET(BgL_mmz00_32, BgL_iz00_33, BgL_cz00_34); 
{ /* Llib/mmap.scm 202 */
 long BgL_arg1230z00_2315;
BgL_arg1230z00_2315 = 
(BgL_iz00_33+((long)1)); 
BGL_MMAP_WP_SET(BgL_mmz00_32, BgL_arg1230z00_2315); BUNSPEC; 
return 
make_belong(BgL_arg1230z00_2315);} }  else 
{ /* Llib/mmap.scm 224 */
 obj_t BgL_arg1242z00_2316;
{ /* Llib/mmap.scm 224 */
 obj_t BgL_arg1244z00_2317;
{ /* Llib/mmap.scm 224 */
 obj_t BgL_arg1248z00_2318;
{ /* Llib/mmap.scm 224 */
 long BgL_a1086z00_2319;
BgL_a1086z00_2319 = 
BGL_MMAP_LENGTH(BgL_mmz00_32); 
{ /* Llib/mmap.scm 224 */

BgL_arg1248z00_2318 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(
make_belong(BgL_a1086z00_2319), 
BINT(1L)); } } 
{ /* Llib/mmap.scm 224 */

BgL_arg1244z00_2317 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1248z00_2318, 
BINT(10L)); } } 
BgL_arg1242z00_2316 = 
string_append_3(BGl_string1875z00zz__mmapz00, BgL_arg1244z00_2317, BGl_string1876z00zz__mmapz00); } 
return 
BGl_errorz00zz__errorz00(BGl_symbol1880z00zz__mmapz00, BgL_arg1242z00_2316, 
make_belong(BgL_iz00_33));} } } 

}



/* &mmap-set! */
obj_t BGl_z62mmapzd2setz12za2zz__mmapz00(obj_t BgL_envz00_2146, obj_t BgL_mmz00_2147, obj_t BgL_iz00_2148, obj_t BgL_cz00_2149)
{
{ /* Llib/mmap.scm 219 */
{ /* Llib/mmap.scm 220 */
 unsigned char BgL_auxz00_2835; long BgL_auxz00_2826; obj_t BgL_auxz00_2819;
{ /* Llib/mmap.scm 220 */
 obj_t BgL_tmpz00_2836;
if(
CHARP(BgL_cz00_2149))
{ /* Llib/mmap.scm 220 */
BgL_tmpz00_2836 = BgL_cz00_2149
; }  else 
{ 
 obj_t BgL_auxz00_2839;
BgL_auxz00_2839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9987L), BGl_string1882z00zz__mmapz00, BGl_string1874z00zz__mmapz00, BgL_cz00_2149); 
FAILURE(BgL_auxz00_2839,BFALSE,BFALSE);} 
BgL_auxz00_2835 = 
CCHAR(BgL_tmpz00_2836); } 
{ /* Llib/mmap.scm 220 */
 obj_t BgL_tmpz00_2827;
if(
ELONGP(BgL_iz00_2148))
{ /* Llib/mmap.scm 220 */
BgL_tmpz00_2827 = BgL_iz00_2148
; }  else 
{ 
 obj_t BgL_auxz00_2830;
BgL_auxz00_2830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9987L), BGl_string1882z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_iz00_2148); 
FAILURE(BgL_auxz00_2830,BFALSE,BFALSE);} 
BgL_auxz00_2826 = 
BELONG_TO_LONG(BgL_tmpz00_2827); } 
if(
BGL_MMAPP(BgL_mmz00_2147))
{ /* Llib/mmap.scm 220 */
BgL_auxz00_2819 = BgL_mmz00_2147
; }  else 
{ 
 obj_t BgL_auxz00_2822;
BgL_auxz00_2822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9987L), BGl_string1882z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2147); 
FAILURE(BgL_auxz00_2822,BFALSE,BFALSE);} 
return 
BGl_mmapzd2setz12zc0zz__mmapz00(BgL_auxz00_2819, BgL_auxz00_2826, BgL_auxz00_2835);} } 

}



/* mmap-substring */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2substringzd2zz__mmapz00(obj_t BgL_mmz00_35, long BgL_startz00_36, long BgL_endz00_37)
{
{ /* Llib/mmap.scm 231 */
if(
(BgL_endz00_37<BgL_startz00_36))
{ /* Llib/mmap.scm 234 */
 long BgL_arg1268z00_1226;
{ /* Llib/mmap.scm 234 */
 long BgL_res1722z00_1806;
{ /* Llib/mmap.scm 234 */
 long BgL_tmpz00_2847;
BgL_tmpz00_2847 = 
(BgL_endz00_37-BgL_startz00_36); 
BgL_res1722z00_1806 = 
(long)(BgL_tmpz00_2847); } 
BgL_arg1268z00_1226 = BgL_res1722z00_1806; } 
{ /* Llib/mmap.scm 234 */
 obj_t BgL_aux1809z00_2255;
BgL_aux1809z00_2255 = 
BGl_errorz00zz__errorz00(BGl_symbol1883z00zz__mmapz00, BGl_string1885z00zz__mmapz00, 
make_belong(BgL_arg1268z00_1226)); 
if(
STRINGP(BgL_aux1809z00_2255))
{ /* Llib/mmap.scm 234 */
return BgL_aux1809z00_2255;}  else 
{ 
 obj_t BgL_auxz00_2854;
BgL_auxz00_2854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(10524L), BGl_string1884z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_aux1809z00_2255); 
FAILURE(BgL_auxz00_2854,BFALSE,BFALSE);} } }  else 
{ /* Llib/mmap.scm 235 */
 bool_t BgL_test2031z00_2858;
{ /* Llib/mmap.scm 235 */
 long BgL_arg1318z00_1250;
{ /* Llib/mmap.scm 235 */
 long BgL_arg1319z00_1251;
BgL_arg1319z00_1251 = 
BGL_MMAP_LENGTH(BgL_mmz00_35); 
BgL_arg1318z00_1250 = 
(((long)1)+BgL_arg1319z00_1251); } 
BgL_test2031z00_2858 = 
BOUND_CHECK(BgL_endz00_37, BgL_arg1318z00_1250); } 
if(BgL_test2031z00_2858)
{ /* Llib/mmap.scm 240 */
 bool_t BgL_test2032z00_2862;
{ /* Llib/mmap.scm 240 */
 long BgL_arg1314z00_1244;
BgL_arg1314z00_1244 = 
BGL_MMAP_LENGTH(BgL_mmz00_35); 
BgL_test2032z00_2862 = 
BOUND_CHECK(BgL_startz00_36, BgL_arg1314z00_1244); } 
if(BgL_test2032z00_2862)
{ /* Llib/mmap.scm 243 */
 obj_t BgL_rz00_1232;
{ /* Llib/mmap.scm 243 */
 long BgL_arg1311z00_1242;
{ /* Llib/mmap.scm 243 */
 long BgL_arg1312z00_1243;
{ /* Llib/mmap.scm 243 */
 long BgL_res1723z00_1813;
{ /* Llib/mmap.scm 243 */
 long BgL_tmpz00_2865;
BgL_tmpz00_2865 = 
(BgL_endz00_37-BgL_startz00_36); 
BgL_res1723z00_1813 = 
(long)(BgL_tmpz00_2865); } 
BgL_arg1312z00_1243 = BgL_res1723z00_1813; } 
BgL_arg1311z00_1242 = 
(long)(BgL_arg1312z00_1243); } 
BgL_rz00_1232 = 
make_string_sans_fill(BgL_arg1311z00_1242); } 
{ 
 long BgL_iz00_1234; long BgL_jz00_1235;
BgL_iz00_1234 = BgL_startz00_36; 
BgL_jz00_1235 = 0L; 
BgL_zc3z04anonymousza31306ze3z87_1236:
if(
(BgL_iz00_1234==BgL_endz00_37))
{ /* Llib/mmap.scm 246 */
BGL_MMAP_RP_SET(BgL_mmz00_35, BgL_iz00_1234); BUNSPEC; BgL_iz00_1234; 
return BgL_rz00_1232;}  else 
{ /* Llib/mmap.scm 246 */
{ /* Llib/mmap.scm 251 */
 unsigned char BgL_arg1308z00_1238;
{ /* Llib/mmap.scm 251 */
 unsigned char BgL_res1724z00_1827;
{ /* Llib/mmap.scm 193 */
 unsigned char BgL_cz00_1821;
BgL_cz00_1821 = 
BGL_MMAP_REF(BgL_mmz00_35, BgL_iz00_1234); 
{ /* Llib/mmap.scm 194 */
 long BgL_arg1229z00_1822;
BgL_arg1229z00_1822 = 
(BgL_iz00_1234+((long)1)); 
BGL_MMAP_RP_SET(BgL_mmz00_35, BgL_arg1229z00_1822); BUNSPEC; BgL_arg1229z00_1822; } 
BgL_res1724z00_1827 = BgL_cz00_1821; } 
BgL_arg1308z00_1238 = BgL_res1724z00_1827; } 
{ /* Llib/mmap.scm 251 */
 long BgL_l1745z00_2191;
BgL_l1745z00_2191 = 
STRING_LENGTH(BgL_rz00_1232); 
if(
BOUND_CHECK(BgL_jz00_1235, BgL_l1745z00_2191))
{ /* Llib/mmap.scm 251 */
STRING_SET(BgL_rz00_1232, BgL_jz00_1235, BgL_arg1308z00_1238); }  else 
{ 
 obj_t BgL_auxz00_2880;
BgL_auxz00_2880 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(11125L), BGl_string1886z00zz__mmapz00, BgL_rz00_1232, 
(int)(BgL_l1745z00_2191), 
(int)(BgL_jz00_1235)); 
FAILURE(BgL_auxz00_2880,BFALSE,BFALSE);} } } 
{ /* Llib/mmap.scm 252 */
 long BgL_arg1309z00_1239; long BgL_arg1310z00_1240;
BgL_arg1309z00_1239 = 
(BgL_iz00_1234+
(long)(1L)); 
BgL_arg1310z00_1240 = 
(BgL_jz00_1235+1L); 
{ 
 long BgL_jz00_2890; long BgL_iz00_2889;
BgL_iz00_2889 = BgL_arg1309z00_1239; 
BgL_jz00_2890 = BgL_arg1310z00_1240; 
BgL_jz00_1235 = BgL_jz00_2890; 
BgL_iz00_1234 = BgL_iz00_2889; 
goto BgL_zc3z04anonymousza31306ze3z87_1236;} } } } }  else 
{ /* Llib/mmap.scm 241 */
 obj_t BgL_aux1811z00_2257;
BgL_aux1811z00_2257 = 
BGl_errorz00zz__errorz00(BGl_symbol1883z00zz__mmapz00, BGl_string1887z00zz__mmapz00, 
make_belong(BgL_startz00_36)); 
if(
STRINGP(BgL_aux1811z00_2257))
{ /* Llib/mmap.scm 241 */
return BgL_aux1811z00_2257;}  else 
{ 
 obj_t BgL_auxz00_2895;
BgL_auxz00_2895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(10855L), BGl_string1884z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_aux1811z00_2257); 
FAILURE(BgL_auxz00_2895,BFALSE,BFALSE);} } }  else 
{ /* Llib/mmap.scm 238 */
 obj_t BgL_arg1315z00_1245;
{ /* Llib/mmap.scm 238 */
 obj_t BgL_arg1316z00_1246;
{ /* Llib/mmap.scm 238 */
 long BgL_arg1317z00_1247;
BgL_arg1317z00_1247 = 
BGL_MMAP_LENGTH(BgL_mmz00_35); 
{ /* Ieee/number.scm 174 */

BgL_arg1316z00_1246 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(
make_belong(BgL_arg1317z00_1247), 
BINT(10L)); } } 
BgL_arg1315z00_1245 = 
string_append(BGl_string1888z00zz__mmapz00, BgL_arg1316z00_1246); } 
{ /* Llib/mmap.scm 236 */
 obj_t BgL_aux1813z00_2259;
BgL_aux1813z00_2259 = 
BGl_errorz00zz__errorz00(BGl_symbol1883z00zz__mmapz00, BgL_arg1315z00_1245, 
make_belong(BgL_endz00_37)); 
if(
STRINGP(BgL_aux1813z00_2259))
{ /* Llib/mmap.scm 236 */
return BgL_aux1813z00_2259;}  else 
{ 
 obj_t BgL_auxz00_2908;
BgL_auxz00_2908 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(10662L), BGl_string1884z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_aux1813z00_2259); 
FAILURE(BgL_auxz00_2908,BFALSE,BFALSE);} } } } } 

}



/* &mmap-substring */
obj_t BGl_z62mmapzd2substringzb0zz__mmapz00(obj_t BgL_envz00_2150, obj_t BgL_mmz00_2151, obj_t BgL_startz00_2152, obj_t BgL_endz00_2153)
{
{ /* Llib/mmap.scm 231 */
{ /* Llib/mmap.scm 233 */
 long BgL_auxz00_2928; long BgL_auxz00_2919; obj_t BgL_auxz00_2912;
{ /* Llib/mmap.scm 233 */
 obj_t BgL_tmpz00_2929;
if(
ELONGP(BgL_endz00_2153))
{ /* Llib/mmap.scm 233 */
BgL_tmpz00_2929 = BgL_endz00_2153
; }  else 
{ 
 obj_t BgL_auxz00_2932;
BgL_auxz00_2932 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(10497L), BGl_string1889z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_endz00_2153); 
FAILURE(BgL_auxz00_2932,BFALSE,BFALSE);} 
BgL_auxz00_2928 = 
BELONG_TO_LONG(BgL_tmpz00_2929); } 
{ /* Llib/mmap.scm 233 */
 obj_t BgL_tmpz00_2920;
if(
ELONGP(BgL_startz00_2152))
{ /* Llib/mmap.scm 233 */
BgL_tmpz00_2920 = BgL_startz00_2152
; }  else 
{ 
 obj_t BgL_auxz00_2923;
BgL_auxz00_2923 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(10497L), BGl_string1889z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_startz00_2152); 
FAILURE(BgL_auxz00_2923,BFALSE,BFALSE);} 
BgL_auxz00_2919 = 
BELONG_TO_LONG(BgL_tmpz00_2920); } 
if(
BGL_MMAPP(BgL_mmz00_2151))
{ /* Llib/mmap.scm 233 */
BgL_auxz00_2912 = BgL_mmz00_2151
; }  else 
{ 
 obj_t BgL_auxz00_2915;
BgL_auxz00_2915 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(10497L), BGl_string1889z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2151); 
FAILURE(BgL_auxz00_2915,BFALSE,BFALSE);} 
return 
BGl_mmapzd2substringzd2zz__mmapz00(BgL_auxz00_2912, BgL_auxz00_2919, BgL_auxz00_2928);} } 

}



/* mmap-substring-set! */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2substringzd2setz12z12zz__mmapz00(obj_t BgL_mmz00_38, long BgL_oz00_39, obj_t BgL_sz00_40)
{
{ /* Llib/mmap.scm 257 */
{ /* Llib/mmap.scm 258 */
 long BgL_lenz00_1252;
BgL_lenz00_1252 = 
STRING_LENGTH(BgL_sz00_40); 
if(
(BgL_oz00_39<((long)0)))
{ /* Llib/mmap.scm 260 */
return 
BGl_errorz00zz__errorz00(BGl_string1890z00zz__mmapz00, BGl_string1887z00zz__mmapz00, 
make_belong(BgL_oz00_39));}  else 
{ /* Llib/mmap.scm 262 */
 bool_t BgL_test2041z00_2943;
{ /* Llib/mmap.scm 262 */
 long BgL_arg1348z00_1285;
{ /* Llib/mmap.scm 262 */
 long BgL_arg1349z00_1286;
BgL_arg1349z00_1286 = 
BGL_MMAP_LENGTH(BgL_mmz00_38); 
BgL_arg1348z00_1285 = 
(BgL_arg1349z00_1286+((long)1)); } 
BgL_test2041z00_2943 = 
BOUND_CHECK(BgL_oz00_39, BgL_arg1348z00_1285); } 
if(BgL_test2041z00_2943)
{ /* Llib/mmap.scm 268 */
 bool_t BgL_test2042z00_2947;
{ /* Llib/mmap.scm 268 */
 long BgL_arg1340z00_1276; long BgL_arg1341z00_1277;
{ /* Llib/mmap.scm 268 */
 long BgL_arg1342z00_1278;
BgL_arg1342z00_1278 = 
(long)(BgL_lenz00_1252); 
BgL_arg1340z00_1276 = 
(BgL_oz00_39+BgL_arg1342z00_1278); } 
{ /* Llib/mmap.scm 269 */
 long BgL_arg1343z00_1279;
BgL_arg1343z00_1279 = 
BGL_MMAP_LENGTH(BgL_mmz00_38); 
BgL_arg1341z00_1277 = 
(BgL_arg1343z00_1279+((long)1)); } 
BgL_test2042z00_2947 = 
BOUND_CHECK(BgL_arg1340z00_1276, BgL_arg1341z00_1277); } 
if(BgL_test2042z00_2947)
{ 
 long BgL_iz00_1263; long BgL_jz00_1264;
BgL_iz00_1263 = 0L; 
BgL_jz00_1264 = BgL_oz00_39; 
BgL_zc3z04anonymousza31330ze3z87_1265:
if(
(BgL_iz00_1263==BgL_lenz00_1252))
{ /* Llib/mmap.scm 278 */
BGL_MMAP_WP_SET(BgL_mmz00_38, BgL_jz00_1264); BUNSPEC; BgL_jz00_1264; 
return BgL_mmz00_38;}  else 
{ /* Llib/mmap.scm 278 */
{ /* Llib/mmap.scm 283 */
 unsigned char BgL_arg1332z00_1267;
{ /* Llib/mmap.scm 283 */
 long BgL_l1749z00_2195;
BgL_l1749z00_2195 = 
STRING_LENGTH(BgL_sz00_40); 
if(
BOUND_CHECK(BgL_iz00_1263, BgL_l1749z00_2195))
{ /* Llib/mmap.scm 283 */
BgL_arg1332z00_1267 = 
STRING_REF(BgL_sz00_40, BgL_iz00_1263); }  else 
{ 
 obj_t BgL_auxz00_2960;
BgL_auxz00_2960 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(12191L), BGl_string1891z00zz__mmapz00, BgL_sz00_40, 
(int)(BgL_l1749z00_2195), 
(int)(BgL_iz00_1263)); 
FAILURE(BgL_auxz00_2960,BFALSE,BFALSE);} } 
BGL_MMAP_SET(BgL_mmz00_38, BgL_jz00_1264, BgL_arg1332z00_1267); 
{ /* Llib/mmap.scm 202 */
 long BgL_arg1230z00_1856;
BgL_arg1230z00_1856 = 
(BgL_jz00_1264+((long)1)); 
BGL_MMAP_WP_SET(BgL_mmz00_38, BgL_arg1230z00_1856); BUNSPEC; BgL_arg1230z00_1856; } } 
{ /* Llib/mmap.scm 284 */
 long BgL_arg1333z00_1268; long BgL_arg1334z00_1269;
BgL_arg1333z00_1268 = 
(BgL_iz00_1263+1L); 
BgL_arg1334z00_1269 = 
(BgL_jz00_1264+
(long)(1L)); 
{ 
 long BgL_jz00_2973; long BgL_iz00_2972;
BgL_iz00_2972 = BgL_arg1333z00_1268; 
BgL_jz00_2973 = BgL_arg1334z00_1269; 
BgL_jz00_1264 = BgL_jz00_2973; 
BgL_iz00_1263 = BgL_iz00_2972; 
goto BgL_zc3z04anonymousza31330ze3z87_1265;} } } }  else 
{ /* Llib/mmap.scm 272 */
 obj_t BgL_arg1335z00_1271; obj_t BgL_arg1336z00_1272;
{ /* Llib/mmap.scm 272 */
 obj_t BgL_arg1337z00_1273;
{ /* Llib/mmap.scm 272 */
 long BgL_arg1338z00_1274;
BgL_arg1338z00_1274 = 
BGL_MMAP_LENGTH(BgL_mmz00_38); 
BgL_arg1337z00_1273 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(
make_belong(BgL_arg1338z00_1274), 
BINT(1L)); } 
BgL_arg1335z00_1271 = 
string_append_3(BGl_string1875z00zz__mmapz00, BgL_arg1337z00_1273, BGl_string1876z00zz__mmapz00); } 
BgL_arg1336z00_1272 = 
BGl_2zb2zb2zz__r4_numbers_6_5z00(
make_belong(BgL_oz00_39), 
BINT(BgL_lenz00_1252)); 
return 
BGl_errorz00zz__errorz00(BGl_symbol1892z00zz__mmapz00, BgL_arg1335z00_1271, BgL_arg1336z00_1272);} }  else 
{ /* Llib/mmap.scm 265 */
 obj_t BgL_arg1344z00_1280;
{ /* Llib/mmap.scm 265 */
 obj_t BgL_arg1346z00_1281;
{ /* Llib/mmap.scm 265 */
 long BgL_arg1347z00_1282;
BgL_arg1347z00_1282 = 
BGL_MMAP_LENGTH(BgL_mmz00_38); 
{ /* Ieee/number.scm 174 */

BgL_arg1346z00_1281 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(
make_belong(BgL_arg1347z00_1282), 
BINT(10L)); } } 
BgL_arg1344z00_1280 = 
string_append_3(BGl_string1875z00zz__mmapz00, BgL_arg1346z00_1281, BGl_string1894z00zz__mmapz00); } 
return 
BGl_errorz00zz__errorz00(BGl_symbol1895z00zz__mmapz00, BgL_arg1344z00_1280, 
make_belong(BgL_oz00_39));} } } } 

}



/* &mmap-substring-set! */
obj_t BGl_z62mmapzd2substringzd2setz12z70zz__mmapz00(obj_t BgL_envz00_2154, obj_t BgL_mmz00_2155, obj_t BgL_oz00_2156, obj_t BgL_sz00_2157)
{
{ /* Llib/mmap.scm 257 */
{ /* Llib/mmap.scm 258 */
 obj_t BgL_auxz00_3006; long BgL_auxz00_2997; obj_t BgL_auxz00_2990;
if(
STRINGP(BgL_sz00_2157))
{ /* Llib/mmap.scm 258 */
BgL_auxz00_3006 = BgL_sz00_2157
; }  else 
{ 
 obj_t BgL_auxz00_3009;
BgL_auxz00_3009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(11493L), BGl_string1896z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_sz00_2157); 
FAILURE(BgL_auxz00_3009,BFALSE,BFALSE);} 
{ /* Llib/mmap.scm 258 */
 obj_t BgL_tmpz00_2998;
if(
ELONGP(BgL_oz00_2156))
{ /* Llib/mmap.scm 258 */
BgL_tmpz00_2998 = BgL_oz00_2156
; }  else 
{ 
 obj_t BgL_auxz00_3001;
BgL_auxz00_3001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(11493L), BGl_string1896z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_oz00_2156); 
FAILURE(BgL_auxz00_3001,BFALSE,BFALSE);} 
BgL_auxz00_2997 = 
BELONG_TO_LONG(BgL_tmpz00_2998); } 
if(
BGL_MMAPP(BgL_mmz00_2155))
{ /* Llib/mmap.scm 258 */
BgL_auxz00_2990 = BgL_mmz00_2155
; }  else 
{ 
 obj_t BgL_auxz00_2993;
BgL_auxz00_2993 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(11493L), BGl_string1896z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2155); 
FAILURE(BgL_auxz00_2993,BFALSE,BFALSE);} 
return 
BGl_mmapzd2substringzd2setz12z12zz__mmapz00(BgL_auxz00_2990, BgL_auxz00_2997, BgL_auxz00_3006);} } 

}



/* mmap-get-char */
BGL_EXPORTED_DEF unsigned char BGl_mmapzd2getzd2charz00zz__mmapz00(obj_t BgL_mmz00_41)
{
{ /* Llib/mmap.scm 289 */
{ /* Llib/mmap.scm 290 */
 long BgL_arg1350z00_2320;
BgL_arg1350z00_2320 = 
BGL_MMAP_RP_GET(BgL_mmz00_41); 
{ /* Llib/mmap.scm 290 */
 unsigned char BgL_res1726z00_2321;
{ /* Llib/mmap.scm 208 */
 bool_t BgL_test2048z00_3015;
{ /* Llib/mmap.scm 208 */
 long BgL_arg1238z00_2322;
BgL_arg1238z00_2322 = 
BGL_MMAP_LENGTH(BgL_mmz00_41); 
BgL_test2048z00_3015 = 
BOUND_CHECK(BgL_arg1350z00_2320, BgL_arg1238z00_2322); } 
if(BgL_test2048z00_3015)
{ /* Llib/mmap.scm 209 */
 unsigned char BgL_res1725z00_2323;
{ /* Llib/mmap.scm 193 */
 unsigned char BgL_cz00_2324;
BgL_cz00_2324 = 
BGL_MMAP_REF(BgL_mmz00_41, BgL_arg1350z00_2320); 
{ /* Llib/mmap.scm 194 */
 long BgL_arg1229z00_2325;
BgL_arg1229z00_2325 = 
(BgL_arg1350z00_2320+((long)1)); 
BGL_MMAP_RP_SET(BgL_mmz00_41, BgL_arg1229z00_2325); BUNSPEC; BgL_arg1229z00_2325; } 
BgL_res1725z00_2323 = BgL_cz00_2324; } 
BgL_res1726z00_2321 = BgL_res1725z00_2323; }  else 
{ /* Llib/mmap.scm 212 */
 obj_t BgL_arg1233z00_2326;
{ /* Llib/mmap.scm 212 */
 obj_t BgL_arg1234z00_2327;
{ /* Llib/mmap.scm 212 */
 obj_t BgL_arg1236z00_2328;
{ /* Llib/mmap.scm 212 */
 long BgL_a1084z00_2329;
BgL_a1084z00_2329 = 
BGL_MMAP_LENGTH(BgL_mmz00_41); 
{ /* Llib/mmap.scm 212 */

BgL_arg1236z00_2328 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(
make_belong(BgL_a1084z00_2329), 
BINT(1L)); } } 
{ /* Llib/mmap.scm 212 */

BgL_arg1234z00_2327 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1236z00_2328, 
BINT(10L)); } } 
BgL_arg1233z00_2326 = 
string_append_3(BGl_string1875z00zz__mmapz00, BgL_arg1234z00_2327, BGl_string1876z00zz__mmapz00); } 
{ /* Llib/mmap.scm 210 */
 obj_t BgL_tmpz00_3028;
{ /* Llib/mmap.scm 210 */
 obj_t BgL_aux1827z00_2330;
BgL_aux1827z00_2330 = 
BGl_errorz00zz__errorz00(BGl_symbol1877z00zz__mmapz00, BgL_arg1233z00_2326, 
make_belong(BgL_arg1350z00_2320)); 
if(
CHARP(BgL_aux1827z00_2330))
{ /* Llib/mmap.scm 210 */
BgL_tmpz00_3028 = BgL_aux1827z00_2330
; }  else 
{ 
 obj_t BgL_auxz00_3033;
BgL_auxz00_3033 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(9564L), BGl_string1897z00zz__mmapz00, BGl_string1874z00zz__mmapz00, BgL_aux1827z00_2330); 
FAILURE(BgL_auxz00_3033,BFALSE,BFALSE);} } 
BgL_res1726z00_2321 = 
CCHAR(BgL_tmpz00_3028); } } } 
return BgL_res1726z00_2321;} } } 

}



/* &mmap-get-char */
obj_t BGl_z62mmapzd2getzd2charz62zz__mmapz00(obj_t BgL_envz00_2158, obj_t BgL_mmz00_2159)
{
{ /* Llib/mmap.scm 289 */
{ /* Llib/mmap.scm 290 */
 unsigned char BgL_tmpz00_3038;
{ /* Llib/mmap.scm 290 */
 obj_t BgL_auxz00_3039;
if(
BGL_MMAPP(BgL_mmz00_2159))
{ /* Llib/mmap.scm 290 */
BgL_auxz00_3039 = BgL_mmz00_2159
; }  else 
{ 
 obj_t BgL_auxz00_3042;
BgL_auxz00_3042 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(12534L), BGl_string1898z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2159); 
FAILURE(BgL_auxz00_3042,BFALSE,BFALSE);} 
BgL_tmpz00_3038 = 
BGl_mmapzd2getzd2charz00zz__mmapz00(BgL_auxz00_3039); } 
return 
BCHAR(BgL_tmpz00_3038);} } 

}



/* mmap-put-char! */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2putzd2charz12z12zz__mmapz00(obj_t BgL_mmz00_42, unsigned char BgL_cz00_43)
{
{ /* Llib/mmap.scm 295 */
{ /* Llib/mmap.scm 296 */
 long BgL_arg1351z00_2331;
BgL_arg1351z00_2331 = 
BGL_MMAP_WP_GET(BgL_mmz00_42); 
{ /* Llib/mmap.scm 220 */
 bool_t BgL_test2051z00_3049;
{ /* Llib/mmap.scm 220 */
 long BgL_arg1252z00_2332;
BgL_arg1252z00_2332 = 
BGL_MMAP_LENGTH(BgL_mmz00_42); 
BgL_test2051z00_3049 = 
BOUND_CHECK(BgL_arg1351z00_2331, BgL_arg1252z00_2332); } 
if(BgL_test2051z00_3049)
{ /* Llib/mmap.scm 220 */
BGL_MMAP_SET(BgL_mmz00_42, BgL_arg1351z00_2331, BgL_cz00_43); 
{ /* Llib/mmap.scm 202 */
 long BgL_arg1230z00_2333;
BgL_arg1230z00_2333 = 
(BgL_arg1351z00_2331+((long)1)); 
BGL_MMAP_WP_SET(BgL_mmz00_42, BgL_arg1230z00_2333); BUNSPEC; 
return 
make_belong(BgL_arg1230z00_2333);} }  else 
{ /* Llib/mmap.scm 224 */
 obj_t BgL_arg1242z00_2334;
{ /* Llib/mmap.scm 224 */
 obj_t BgL_arg1244z00_2335;
{ /* Llib/mmap.scm 224 */
 obj_t BgL_arg1248z00_2336;
{ /* Llib/mmap.scm 224 */
 long BgL_a1086z00_2337;
BgL_a1086z00_2337 = 
BGL_MMAP_LENGTH(BgL_mmz00_42); 
{ /* Llib/mmap.scm 224 */

BgL_arg1248z00_2336 = 
BGl_2zd2zd2zz__r4_numbers_6_5z00(
make_belong(BgL_a1086z00_2337), 
BINT(1L)); } } 
{ /* Llib/mmap.scm 224 */

BgL_arg1244z00_2335 = 
BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(BgL_arg1248z00_2336, 
BINT(10L)); } } 
BgL_arg1242z00_2334 = 
string_append_3(BGl_string1875z00zz__mmapz00, BgL_arg1244z00_2335, BGl_string1876z00zz__mmapz00); } 
return 
BGl_errorz00zz__errorz00(BGl_symbol1880z00zz__mmapz00, BgL_arg1242z00_2334, 
make_belong(BgL_arg1351z00_2331));} } } } 

}



/* &mmap-put-char! */
obj_t BGl_z62mmapzd2putzd2charz12z70zz__mmapz00(obj_t BgL_envz00_2160, obj_t BgL_mmz00_2161, obj_t BgL_cz00_2162)
{
{ /* Llib/mmap.scm 295 */
{ /* Llib/mmap.scm 296 */
 unsigned char BgL_auxz00_3072; obj_t BgL_auxz00_3065;
{ /* Llib/mmap.scm 296 */
 obj_t BgL_tmpz00_3073;
if(
CHARP(BgL_cz00_2162))
{ /* Llib/mmap.scm 296 */
BgL_tmpz00_3073 = BgL_cz00_2162
; }  else 
{ 
 obj_t BgL_auxz00_3076;
BgL_auxz00_3076 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(12833L), BGl_string1899z00zz__mmapz00, BGl_string1874z00zz__mmapz00, BgL_cz00_2162); 
FAILURE(BgL_auxz00_3076,BFALSE,BFALSE);} 
BgL_auxz00_3072 = 
CCHAR(BgL_tmpz00_3073); } 
if(
BGL_MMAPP(BgL_mmz00_2161))
{ /* Llib/mmap.scm 296 */
BgL_auxz00_3065 = BgL_mmz00_2161
; }  else 
{ 
 obj_t BgL_auxz00_3068;
BgL_auxz00_3068 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(12833L), BGl_string1899z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2161); 
FAILURE(BgL_auxz00_3068,BFALSE,BFALSE);} 
return 
BGl_mmapzd2putzd2charz12z12zz__mmapz00(BgL_auxz00_3065, BgL_auxz00_3072);} } 

}



/* mmap-get-string */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2getzd2stringz00zz__mmapz00(obj_t BgL_mmz00_44, long BgL_lenz00_45)
{
{ /* Llib/mmap.scm 301 */
{ /* Llib/mmap.scm 302 */
 long BgL_arg1352z00_2338; long BgL_arg1354z00_2339;
BgL_arg1352z00_2338 = 
BGL_MMAP_RP_GET(BgL_mmz00_44); 
{ /* Llib/mmap.scm 302 */
 long BgL_arg1356z00_2340;
BgL_arg1356z00_2340 = 
BGL_MMAP_RP_GET(BgL_mmz00_44); 
BgL_arg1354z00_2339 = 
(BgL_arg1356z00_2340+BgL_lenz00_45); } 
return 
BGl_mmapzd2substringzd2zz__mmapz00(BgL_mmz00_44, BgL_arg1352z00_2338, BgL_arg1354z00_2339);} } 

}



/* &mmap-get-string */
obj_t BGl_z62mmapzd2getzd2stringz62zz__mmapz00(obj_t BgL_envz00_2163, obj_t BgL_mmz00_2164, obj_t BgL_lenz00_2165)
{
{ /* Llib/mmap.scm 301 */
{ /* Llib/mmap.scm 302 */
 long BgL_auxz00_3093; obj_t BgL_auxz00_3086;
{ /* Llib/mmap.scm 302 */
 obj_t BgL_tmpz00_3094;
if(
ELONGP(BgL_lenz00_2165))
{ /* Llib/mmap.scm 302 */
BgL_tmpz00_3094 = BgL_lenz00_2165
; }  else 
{ 
 obj_t BgL_auxz00_3097;
BgL_auxz00_3097 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(13142L), BGl_string1900z00zz__mmapz00, BGl_string1869z00zz__mmapz00, BgL_lenz00_2165); 
FAILURE(BgL_auxz00_3097,BFALSE,BFALSE);} 
BgL_auxz00_3093 = 
BELONG_TO_LONG(BgL_tmpz00_3094); } 
if(
BGL_MMAPP(BgL_mmz00_2164))
{ /* Llib/mmap.scm 302 */
BgL_auxz00_3086 = BgL_mmz00_2164
; }  else 
{ 
 obj_t BgL_auxz00_3089;
BgL_auxz00_3089 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(13142L), BGl_string1900z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2164); 
FAILURE(BgL_auxz00_3089,BFALSE,BFALSE);} 
return 
BGl_mmapzd2getzd2stringz00zz__mmapz00(BgL_auxz00_3086, BgL_auxz00_3093);} } 

}



/* mmap-put-string! */
BGL_EXPORTED_DEF obj_t BGl_mmapzd2putzd2stringz12z12zz__mmapz00(obj_t BgL_mmz00_46, obj_t BgL_sz00_47)
{
{ /* Llib/mmap.scm 307 */
{ /* Llib/mmap.scm 308 */
 long BgL_arg1357z00_2341;
BgL_arg1357z00_2341 = 
BGL_MMAP_WP_GET(BgL_mmz00_46); 
return 
BGl_mmapzd2substringzd2setz12z12zz__mmapz00(BgL_mmz00_46, BgL_arg1357z00_2341, BgL_sz00_47);} } 

}



/* &mmap-put-string! */
obj_t BGl_z62mmapzd2putzd2stringz12z70zz__mmapz00(obj_t BgL_envz00_2166, obj_t BgL_mmz00_2167, obj_t BgL_sz00_2168)
{
{ /* Llib/mmap.scm 307 */
{ /* Llib/mmap.scm 308 */
 obj_t BgL_auxz00_3112; obj_t BgL_auxz00_3105;
if(
STRINGP(BgL_sz00_2168))
{ /* Llib/mmap.scm 308 */
BgL_auxz00_3112 = BgL_sz00_2168
; }  else 
{ 
 obj_t BgL_auxz00_3115;
BgL_auxz00_3115 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(13480L), BGl_string1901z00zz__mmapz00, BGl_string1855z00zz__mmapz00, BgL_sz00_2168); 
FAILURE(BgL_auxz00_3115,BFALSE,BFALSE);} 
if(
BGL_MMAPP(BgL_mmz00_2167))
{ /* Llib/mmap.scm 308 */
BgL_auxz00_3105 = BgL_mmz00_2167
; }  else 
{ 
 obj_t BgL_auxz00_3108;
BgL_auxz00_3108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string1843z00zz__mmapz00, 
BINT(13480L), BGl_string1901z00zz__mmapz00, BGl_string1862z00zz__mmapz00, BgL_mmz00_2167); 
FAILURE(BgL_auxz00_3108,BFALSE,BFALSE);} 
return 
BGl_mmapzd2putzd2stringz12z12zz__mmapz00(BgL_auxz00_3105, BgL_auxz00_3112);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__mmapz00(void)
{
{ /* Llib/mmap.scm 19 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__mmapz00(void)
{
{ /* Llib/mmap.scm 19 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__mmapz00(void)
{
{ /* Llib/mmap.scm 19 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__mmapz00(void)
{
{ /* Llib/mmap.scm 19 */
return 
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string1902z00zz__mmapz00));} 

}

#ifdef __cplusplus
}
#endif
