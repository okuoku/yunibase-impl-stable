/*===========================================================================*/
/*   (Llib/weakhash.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-safe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -O3 -mklib -cc gcc -fsharing -q -unsafev -eval (set! *indent* 4) -stdc -c Llib/weakhash.scm -indent -o objs/obj_s/Llib/weakhash.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___WEAKHASH_TYPE_DEFINITIONS
#define BGL___WEAKHASH_TYPE_DEFINITIONS
#endif // BGL___WEAKHASH_TYPE_DEFINITIONS
#endif // BGL_MODULE_TYPE_DEFINITIONS

static obj_t BGl_list3501z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3537z00zz__weakhashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_symbol3458z00zz__weakhashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2getz00zz__weakhashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2forzd2eachzb0zz__weakhashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(obj_t);
static obj_t BGl_symbol3460z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3462z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62weakzd2hashtablezd2keyzd2listzb0zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_symbol3546z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3432z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3515z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3549z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3469z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_requirezd2initializa7ationz75zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3551z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3472z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3474z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3523z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_traversezd2bucketszd2zz__weakhashz00(obj_t, obj_t, long, obj_t);
static obj_t BGl_symbol3478z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3528z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31509ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2getz62zz__weakhashz00(obj_t, obj_t, obj_t);
extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__weakhashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long, char *);
extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
static obj_t BGl_symbol3561z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3480z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3563z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3566z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3569z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3536z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_removez00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3457z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31526ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
extern long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t);
static obj_t BGl_z62weakzd2hashtablezd2clearz12z70zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_weakzd2oldzd2hashtablezd2addz12zc0zz__weakhashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3574z00zz__weakhashz00 = BUNSPEC;
static bool_t BGl_weakzd2oldzd2hashtablezd2removez12zc0zz__weakhashz00(obj_t, obj_t);
extern bool_t BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(obj_t);
static obj_t BGl_list3542z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3576z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3495z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_toplevelzd2initzd2zz__weakhashz00(void);
static obj_t BGl_list3545z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31470ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_list3548z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3468z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_weakzd2oldzd2hashtablezd2putz12zc0zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_keepgoingz00zz__weakhashz00 = BUNSPEC;
extern void bgl_weakptr_ref_set(obj_t, obj_t);
extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
static obj_t BGl_symbol3584z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3471z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3587z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3556z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3476z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3477z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31463ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
static bool_t BGl_weakzd2keyszd2hashtablezd2containszf3z21zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_cnstzd2initzd2zz__weakhashz00(void);
static obj_t BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t);
static obj_t BGl_genericzd2initzd2zz__weakhashz00(void);
static bool_t BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2removez12z70zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2ze3vectorz81zz__weakhashz00(obj_t, obj_t);
static bool_t BGl_weakzd2keyszd2hashtablezd2removez12zc0zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31400ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_list3560z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_importedzd2moduleszd2initz00zz__weakhashz00(void);
static obj_t BGl_list3565z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_gczd2rootszd2initz00zz__weakhashz00(void);
static obj_t BGl_list3568z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_weakzd2keyszd2hashtablezd2getzd2zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_objectzd2initzd2zz__weakhashz00(void);
extern obj_t BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(obj_t, long);
extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
static obj_t BGl_list3573z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3494z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31651ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31481ze3ze5zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31473ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2expandz12z70zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t);
static obj_t BGl_z62weakzd2hashtablezd2filterz12z70zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2addz12z70zz__weakhashz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
static bool_t BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_keyszd2traversezd2hashz00zz__weakhashz00(obj_t, obj_t);
extern obj_t bgl_weakptr_data(obj_t);
extern obj_t bgl_weakptr_ref(obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2putz12z70zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2ze3listz81zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_list3583z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_list3586z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_weakzd2oldzd2hashtablezd2getzd2zz__weakhashz00(obj_t, obj_t);
extern obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
BGL_EXPORTED_DECL bool_t BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31750ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31718ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31475ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31548ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static bool_t BGl_weakzd2oldzd2hashtablezd2containszf3z21zz__weakhashz00(obj_t, obj_t);
extern obj_t bstring_to_symbol(obj_t);
extern obj_t make_vector(long, obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(obj_t);
static obj_t BGl_z62weakzd2hashtablezd2mapz62zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_weakzd2oldzd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_methodzd2initzd2zz__weakhashz00(void);
extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t bgl_make_weakptr(obj_t, obj_t);
extern obj_t BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(obj_t, obj_t, obj_t, obj_t, int, int);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(obj_t);
extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31511ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62zc3z04anonymousza31503ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2updatez12z70zz__weakhashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_weakzd2keyszd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3502z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3504z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3428z00zz__weakhashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2expandz12z12zz__weakhashz00(obj_t);
static obj_t BGl_removestopz00zz__weakhashz00 = BUNSPEC;
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_symbol3433z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3516z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3435z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3518z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_symbol3437z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_z62zc3z04anonymousza31602ze3ze5zz__weakhashz00(obj_t, obj_t, obj_t, obj_t);
extern long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t);
extern long BGl_getzd2hashnumberzd2zz__hashz00(obj_t);
static obj_t BGl_symbol3440z00zz__weakhashz00 = BUNSPEC;
static obj_t BGl_weakzd2keyszd2hashtablezd2addz12zc0zz__weakhashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t BGl_symbol3524z00zz__weakhashz00 = BUNSPEC;
extern bool_t BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(obj_t);
static obj_t BGl_symbol3529z00zz__weakhashz00 = BUNSPEC;
static bool_t BGl_oldzd2traversezd2hashz00zz__weakhashz00(obj_t, obj_t);
extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
extern obj_t BGl_gcz00zz__biglooz00(obj_t);
static long BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(obj_t, obj_t);
static obj_t BGl_weakzd2keyszd2hashtablezd2putz12zc0zz__weakhashz00(obj_t, obj_t, obj_t);
static obj_t BGl_z62weakzd2hashtablezd2containszf3z91zz__weakhashz00(obj_t, obj_t, obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(obj_t);
BGL_EXPORTED_DECL obj_t BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(obj_t, obj_t, obj_t, obj_t, obj_t);
static obj_t *__cnst;


DEFINE_STRING( BGl_string3570z00zz__weakhashz00, BgL_bgl_string3570za700za7za7_3605za7, "arg1689", 7 );
DEFINE_STRING( BGl_string3571z00zz__weakhashz00, BgL_bgl_string3571za700za7za7_3606za7, "weak-old-hashtable-add!", 23 );
DEFINE_STRING( BGl_string3490z00zz__weakhashz00, BgL_bgl_string3490za700za7za7_3607za7, "&weak-hashtable-key-list", 24 );
DEFINE_STRING( BGl_string3572z00zz__weakhashz00, BgL_bgl_string3572za700za7za7_3608za7, "weak-old-hashtable-add!:Wrong number of arguments", 49 );
DEFINE_STRING( BGl_string3491z00zz__weakhashz00, BgL_bgl_string3491za700za7za7_3609za7, "weak-hashtable-map", 18 );
DEFINE_STRING( BGl_string3492z00zz__weakhashz00, BgL_bgl_string3492za700za7za7_3610za7, "&weak-hashtable-map", 19 );
DEFINE_STRING( BGl_string3493z00zz__weakhashz00, BgL_bgl_string3493za700za7za7_3611za7, "<@anonymous:1475>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3575z00zz__weakhashz00, BgL_bgl_string3575za700za7za7_3612za7, "arg1714", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2filterz12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3613z00, BGl_z62weakzd2hashtablezd2filterz12z70zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3577z00zz__weakhashz00, BgL_bgl_string3577za700za7za7_3614za7, "arg1715", 7 );
DEFINE_STRING( BGl_string3496z00zz__weakhashz00, BgL_bgl_string3496za700za7za7_3615za7, "val", 3 );
DEFINE_STRING( BGl_string3578z00zz__weakhashz00, BgL_bgl_string3578za700za7za7_3616za7, "<@anonymous:1718>", 17 );
DEFINE_STRING( BGl_string3497z00zz__weakhashz00, BgL_bgl_string3497za700za7za7_3617za7, "&weak-hashtable-for-each", 24 );
DEFINE_STRING( BGl_string3579z00zz__weakhashz00, BgL_bgl_string3579za700za7za7_3618za7, "<@anonymous:1718>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3498z00zz__weakhashz00, BgL_bgl_string3498za700za7za7_3619za7, "weak-keys-hashtable-filter!", 27 );
DEFINE_STRING( BGl_string3499z00zz__weakhashz00, BgL_bgl_string3499za700za7za7_3620za7, "<@anonymous:1481>", 17 );
DEFINE_STRING( BGl_string3580z00zz__weakhashz00, BgL_bgl_string3580za700za7za7_3621za7, "&weak-hashtable-add!", 20 );
DEFINE_STRING( BGl_string3581z00zz__weakhashz00, BgL_bgl_string3581za700za7za7_3622za7, "weak-keys-hashtable-remove!", 27 );
DEFINE_STRING( BGl_string3582z00zz__weakhashz00, BgL_bgl_string3582za700za7za7_3623za7, "weak-keys-hashtable-remove!:Wrong number of arguments", 53 );
DEFINE_STRING( BGl_string3585z00zz__weakhashz00, BgL_bgl_string3585za700za7za7_3624za7, "arg1746", 7 );
DEFINE_STRING( BGl_string3588z00zz__weakhashz00, BgL_bgl_string3588za700za7za7_3625za7, "arg1744", 7 );
DEFINE_STRING( BGl_string3589z00zz__weakhashz00, BgL_bgl_string3589za700za7za7_3626za7, "weak-old-hashtable-remove!", 26 );
DEFINE_STRING( BGl_string3590z00zz__weakhashz00, BgL_bgl_string3590za700za7za7_3627za7, "<@anonymous:1750>", 17 );
DEFINE_STRING( BGl_string3591z00zz__weakhashz00, BgL_bgl_string3591za700za7za7_3628za7, "<@anonymous:1750>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3592z00zz__weakhashz00, BgL_bgl_string3592za700za7za7_3629za7, "&weak-hashtable-remove!", 23 );
DEFINE_STRING( BGl_string3593z00zz__weakhashz00, BgL_bgl_string3593za700za7za7_3630za7, "weak-keys-hashtable-expand!", 27 );
DEFINE_STRING( BGl_string3594z00zz__weakhashz00, BgL_bgl_string3594za700za7za7_3631za7, "<@anonymous:1757>", 17 );
DEFINE_STRING( BGl_string3595z00zz__weakhashz00, BgL_bgl_string3595za700za7za7_3632za7, "<@anonymous:1773>", 17 );
DEFINE_STRING( BGl_string3596z00zz__weakhashz00, BgL_bgl_string3596za700za7za7_3633za7, "Hashtable too large (new-len=~a/~a, size=~a)", 44 );
DEFINE_STRING( BGl_string3597z00zz__weakhashz00, BgL_bgl_string3597za700za7za7_3634za7, "hashtable-put!", 14 );
DEFINE_STRING( BGl_string3598z00zz__weakhashz00, BgL_bgl_string3598za700za7za7_3635za7, "weak-old-hashtable-expand!", 26 );
DEFINE_STRING( BGl_string3599z00zz__weakhashz00, BgL_bgl_string3599za700za7za7_3636za7, "<@anonymous:1794>", 17 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2clearz12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3637z00, BGl_z62weakzd2hashtablezd2clearz12z70zz__weakhashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2ze3vectorzd2envz31zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3638z00, BGl_z62weakzd2hashtablezd2ze3vectorz81zz__weakhashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2mapzd2envzd2zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3639z00, BGl_z62weakzd2hashtablezd2mapz62zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2getzd2envzd2zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3640z00, BGl_z62weakzd2hashtablezd2getz62zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2containszf3zd2envz21zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3641z00, BGl_z62weakzd2hashtablezd2containszf3z91zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2expandz12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3642z00, BGl_z62weakzd2hashtablezd2expandz12z70zz__weakhashz00, 0L, BUNSPEC, 1 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2ze3listzd2envz31zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3643z00, BGl_z62weakzd2hashtablezd2ze3listz81zz__weakhashz00, 0L, BUNSPEC, 1 );
DEFINE_STRING( BGl_string3500z00zz__weakhashz00, BgL_bgl_string3500za700za7za7_3644za7, "<@anonymous:1481>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3503z00zz__weakhashz00, BgL_bgl_string3503za700za7za7_3645za7, "arg1492", 7 );
DEFINE_STRING( BGl_string3505z00zz__weakhashz00, BgL_bgl_string3505za700za7za7_3646za7, "arg1494", 7 );
DEFINE_STRING( BGl_string3506z00zz__weakhashz00, BgL_bgl_string3506za700za7za7_3647za7, "weak-old-hashtable-filter!", 26 );
DEFINE_STRING( BGl_string3425z00zz__weakhashz00, BgL_bgl_string3425za700za7za7_3648za7, "/tmp/bigloo/runtime/Llib/hash.sch", 33 );
DEFINE_STRING( BGl_string3507z00zz__weakhashz00, BgL_bgl_string3507za700za7za7_3649za7, "<@anonymous:1503>", 17 );
DEFINE_STRING( BGl_string3426z00zz__weakhashz00, BgL_bgl_string3426za700za7za7_3650za7, "table-get-hashnumber", 20 );
DEFINE_STRING( BGl_string3508z00zz__weakhashz00, BgL_bgl_string3508za700za7za7_3651za7, "<@anonymous:1503>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3427z00zz__weakhashz00, BgL_bgl_string3427za700za7za7_3652za7, "symbol", 6 );
DEFINE_STRING( BGl_string3509z00zz__weakhashz00, BgL_bgl_string3509za700za7za7_3653za7, "&weak-hashtable-filter!", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2updatez12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3654z00, BGl_z62weakzd2hashtablezd2updatez12z70zz__weakhashz00, 0L, BUNSPEC, 4 );
DEFINE_STRING( BGl_string3429z00zz__weakhashz00, BgL_bgl_string3429za700za7za7_3655za7, "%hashtable", 10 );
DEFINE_STRING( BGl_string3430z00zz__weakhashz00, BgL_bgl_string3430za700za7za7_3656za7, "struct-ref:not an instance of", 29 );
DEFINE_STRING( BGl_string3512z00zz__weakhashz00, BgL_bgl_string3512za700za7za7_3657za7, "&weak-hashtable-clear!", 22 );
DEFINE_STRING( BGl_string3431z00zz__weakhashz00, BgL_bgl_string3431za700za7za7_3658za7, "table-get-hashnumber:Wrong number of arguments", 46 );
DEFINE_STRING( BGl_string3513z00zz__weakhashz00, BgL_bgl_string3513za700za7za7_3659za7, "weak-keys-hashtable-contains?", 29 );
DEFINE_STRING( BGl_string3514z00zz__weakhashz00, BgL_bgl_string3514za700za7za7_3660za7, "loop:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string3434z00zz__weakhashz00, BgL_bgl_string3434za700za7za7_3661za7, "funcall", 7 );
DEFINE_STRING( BGl_string3517z00zz__weakhashz00, BgL_bgl_string3517za700za7za7_3662za7, "eqt", 3 );
DEFINE_STRING( BGl_string3436z00zz__weakhashz00, BgL_bgl_string3436za700za7za7_3663za7, "hashn", 5 );
DEFINE_STRING( BGl_string3519z00zz__weakhashz00, BgL_bgl_string3519za700za7za7_3664za7, "arg1522", 7 );
DEFINE_STRING( BGl_string3438z00zz__weakhashz00, BgL_bgl_string3438za700za7za7_3665za7, "key", 3 );
DEFINE_STRING( BGl_string3439z00zz__weakhashz00, BgL_bgl_string3439za700za7za7_3666za7, "bint", 4 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3510z00zz__weakhashz00, BgL_bgl_za762za7c3za704anonymo3667za7, BGl_z62zc3z04anonymousza31509ze3ze5zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3511z00zz__weakhashz00, BgL_bgl_za762za7c3za704anonymo3668za7, BGl_z62zc3z04anonymousza31511ze3ze5zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3600z00zz__weakhashz00, BgL_bgl_string3600za700za7za7_3669za7, "<@anonymous:1810>", 17 );
DEFINE_STRING( BGl_string3601z00zz__weakhashz00, BgL_bgl_string3601za700za7za7_3670za7, "<@anonymous:1827>", 17 );
DEFINE_STRING( BGl_string3520z00zz__weakhashz00, BgL_bgl_string3520za700za7za7_3671za7, "weak-old-hashtable-contains?", 28 );
DEFINE_STRING( BGl_string3602z00zz__weakhashz00, BgL_bgl_string3602za700za7za7_3672za7, "<@anonymous:1844>", 17 );
DEFINE_STRING( BGl_string3521z00zz__weakhashz00, BgL_bgl_string3521za700za7za7_3673za7, "<@anonymous:1526>", 17 );
DEFINE_STRING( BGl_string3603z00zz__weakhashz00, BgL_bgl_string3603za700za7za7_3674za7, "&weak-hashtable-expand!", 23 );
DEFINE_STRING( BGl_string3522z00zz__weakhashz00, BgL_bgl_string3522za700za7za7_3675za7, "<@anonymous:1526>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3441z00zz__weakhashz00, BgL_bgl_string3441za700za7za7_3676za7, "persistent", 10 );
DEFINE_STRING( BGl_string3604z00zz__weakhashz00, BgL_bgl_string3604za700za7za7_3677za7, "__weakhash", 10 );
DEFINE_STRING( BGl_string3442z00zz__weakhashz00, BgL_bgl_string3442za700za7za7_3678za7, "/tmp/bigloo/runtime/Llib/weakhash.scm", 37 );
DEFINE_STRING( BGl_string3443z00zz__weakhashz00, BgL_bgl_string3443za700za7za7_3679za7, "traverse-buckets", 16 );
DEFINE_STRING( BGl_string3525z00zz__weakhashz00, BgL_bgl_string3525za700za7za7_3680za7, "bkey", 4 );
DEFINE_STRING( BGl_string3444z00zz__weakhashz00, BgL_bgl_string3444za700za7za7_3681za7, "vector", 6 );
DEFINE_STRING( BGl_string3526z00zz__weakhashz00, BgL_bgl_string3526za700za7za7_3682za7, "&weak-hashtable-contains?", 25 );
DEFINE_STRING( BGl_string3445z00zz__weakhashz00, BgL_bgl_string3445za700za7za7_3683za7, "vector-ref", 10 );
DEFINE_STRING( BGl_string3527z00zz__weakhashz00, BgL_bgl_string3527za700za7za7_3684za7, "weak-keys-hashtable-get", 23 );
DEFINE_STRING( BGl_string3446z00zz__weakhashz00, BgL_bgl_string3446za700za7za7_3685za7, "liip", 4 );
DEFINE_STRING( BGl_string3447z00zz__weakhashz00, BgL_bgl_string3447za700za7za7_3686za7, "pair", 4 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2forzd2eachzd2envz00zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3687z00, BGl_z62weakzd2hashtablezd2forzd2eachzb0zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3448z00zz__weakhashz00, BgL_bgl_string3448za700za7za7_3688za7, "weakptr", 7 );
DEFINE_STRING( BGl_string3449z00zz__weakhashz00, BgL_bgl_string3449za700za7za7_3689za7, "struct-set!:not an instance of", 30 );
DEFINE_STRING( BGl_string3530z00zz__weakhashz00, BgL_bgl_string3530za700za7za7_3690za7, "arg1543", 7 );
DEFINE_STRING( BGl_string3531z00zz__weakhashz00, BgL_bgl_string3531za700za7za7_3691za7, "weak-old-hashtable-get", 22 );
DEFINE_STRING( BGl_string3450z00zz__weakhashz00, BgL_bgl_string3450za700za7za7_3692za7, "vector-set!", 11 );
DEFINE_STRING( BGl_string3532z00zz__weakhashz00, BgL_bgl_string3532za700za7za7_3693za7, "<@anonymous:1548>", 17 );
DEFINE_STRING( BGl_string3533z00zz__weakhashz00, BgL_bgl_string3533za700za7za7_3694za7, "<@anonymous:1548>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3452z00zz__weakhashz00, BgL_bgl_string3452za700za7za7_3695za7, "keys-traverse-hash", 18 );
DEFINE_STRING( BGl_string3534z00zz__weakhashz00, BgL_bgl_string3534za700za7za7_3696za7, "&weak-hashtable-get", 19 );
DEFINE_STRING( BGl_string3453z00zz__weakhashz00, BgL_bgl_string3453za700za7za7_3697za7, "loop", 4 );
DEFINE_STRING( BGl_string3535z00zz__weakhashz00, BgL_bgl_string3535za700za7za7_3698za7, "weak-keys-hashtable-put!", 24 );
DEFINE_STRING( BGl_string3454z00zz__weakhashz00, BgL_bgl_string3454za700za7za7_3699za7, "<@anonymous:1403>", 17 );
DEFINE_STRING( BGl_string3455z00zz__weakhashz00, BgL_bgl_string3455za700za7za7_3700za7, "procedure", 9 );
DEFINE_STRING( BGl_string3456z00zz__weakhashz00, BgL_bgl_string3456za700za7za7_3701za7, "<@anonymous:1403>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3538z00zz__weakhashz00, BgL_bgl_string3538za700za7za7_3702za7, "arg1580", 7 );
DEFINE_STRING( BGl_string3539z00zz__weakhashz00, BgL_bgl_string3539za700za7za7_3703za7, "weak-old-hashtable-put!", 23 );
DEFINE_STRING( BGl_string3459z00zz__weakhashz00, BgL_bgl_string3459za700za7za7_3704za7, "fun", 3 );
DEFINE_STATIC_BGL_PROCEDURE( BGl_proc3451z00zz__weakhashz00, BgL_bgl_za762za7c3za704anonymo3705za7, BGl_z62zc3z04anonymousza31400ze3ze5zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_STRING( BGl_string3540z00zz__weakhashz00, BgL_bgl_string3540za700za7za7_3706za7, "<@anonymous:1602>", 17 );
DEFINE_STRING( BGl_string3541z00zz__weakhashz00, BgL_bgl_string3541za700za7za7_3707za7, "<@anonymous:1602>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3461z00zz__weakhashz00, BgL_bgl_string3461za700za7za7_3708za7, "arg1407", 7 );
DEFINE_STRING( BGl_string3543z00zz__weakhashz00, BgL_bgl_string3543za700za7za7_3709za7, "&weak-hashtable-put!", 20 );
DEFINE_STRING( BGl_string3544z00zz__weakhashz00, BgL_bgl_string3544za700za7za7_3710za7, "weak-keys-hashtable-update!", 27 );
DEFINE_STRING( BGl_string3463z00zz__weakhashz00, BgL_bgl_string3463za700za7za7_3711za7, "arg1408", 7 );
DEFINE_STRING( BGl_string3464z00zz__weakhashz00, BgL_bgl_string3464za700za7za7_3712za7, "for-each", 8 );
DEFINE_STRING( BGl_string3465z00zz__weakhashz00, BgL_bgl_string3465za700za7za7_3713za7, "list", 4 );
DEFINE_STRING( BGl_string3547z00zz__weakhashz00, BgL_bgl_string3547za700za7za7_3714za7, "arg1634", 7 );
DEFINE_STRING( BGl_string3466z00zz__weakhashz00, BgL_bgl_string3466za700za7za7_3715za7, "old-traverse-hash", 17 );
DEFINE_STRING( BGl_string3467z00zz__weakhashz00, BgL_bgl_string3467za700za7za7_3716za7, "liip:Wrong number of arguments", 30 );
DEFINE_STRING( BGl_string3550z00zz__weakhashz00, BgL_bgl_string3550za700za7za7_3717za7, "proc", 4 );
DEFINE_STRING( BGl_string3470z00zz__weakhashz00, BgL_bgl_string3470za700za7za7_3718za7, "arg1421", 7 );
DEFINE_STRING( BGl_string3552z00zz__weakhashz00, BgL_bgl_string3552za700za7za7_3719za7, "arg1628", 7 );
DEFINE_STRING( BGl_string3553z00zz__weakhashz00, BgL_bgl_string3553za700za7za7_3720za7, "weak-old-hashtable-update!", 26 );
DEFINE_STRING( BGl_string3554z00zz__weakhashz00, BgL_bgl_string3554za700za7za7_3721za7, "<@anonymous:1651>", 17 );
DEFINE_STRING( BGl_string3473z00zz__weakhashz00, BgL_bgl_string3473za700za7za7_3722za7, "arg1434", 7 );
DEFINE_STRING( BGl_string3555z00zz__weakhashz00, BgL_bgl_string3555za700za7za7_3723za7, "<@anonymous:1651>:Wrong number of arguments", 43 );
DEFINE_STRING( BGl_string3475z00zz__weakhashz00, BgL_bgl_string3475za700za7za7_3724za7, "data", 4 );
DEFINE_STRING( BGl_string3557z00zz__weakhashz00, BgL_bgl_string3557za700za7za7_3725za7, "&weak-hashtable-update!", 23 );
DEFINE_STRING( BGl_string3558z00zz__weakhashz00, BgL_bgl_string3558za700za7za7_3726za7, "weak-keys-hashtable-add!", 24 );
DEFINE_STRING( BGl_string3559z00zz__weakhashz00, BgL_bgl_string3559za700za7za7_3727za7, "weak-keys-hashtable-add!:Wrong number of arguments", 50 );
DEFINE_STRING( BGl_string3479z00zz__weakhashz00, BgL_bgl_string3479za700za7za7_3728za7, "arg1455", 7 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2addz12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3729z00, BGl_z62weakzd2hashtablezd2addz12z70zz__weakhashz00, 0L, BUNSPEC, 5 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2removez12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3730z00, BGl_z62weakzd2hashtablezd2removez12z70zz__weakhashz00, 0L, BUNSPEC, 2 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2putz12zd2envzc0zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3731z00, BGl_z62weakzd2hashtablezd2putz12z70zz__weakhashz00, 0L, BUNSPEC, 3 );
DEFINE_STRING( BGl_string3562z00zz__weakhashz00, BgL_bgl_string3562za700za7za7_3732za7, "obj", 3 );
DEFINE_STRING( BGl_string3481z00zz__weakhashz00, BgL_bgl_string3481za700za7za7_3733za7, "arg1456", 7 );
DEFINE_STRING( BGl_string3482z00zz__weakhashz00, BgL_bgl_string3482za700za7za7_3734za7, "weak-hashtable->vector", 22 );
DEFINE_STRING( BGl_string3564z00zz__weakhashz00, BgL_bgl_string3564za700za7za7_3735za7, "init", 4 );
DEFINE_STRING( BGl_string3483z00zz__weakhashz00, BgL_bgl_string3483za700za7za7_3736za7, "&weak-hashtable->vector", 23 );
DEFINE_STRING( BGl_string3484z00zz__weakhashz00, BgL_bgl_string3484za700za7za7_3737za7, "struct", 6 );
DEFINE_STRING( BGl_string3485z00zz__weakhashz00, BgL_bgl_string3485za700za7za7_3738za7, "<@anonymous:1463>", 17 );
DEFINE_STRING( BGl_string3567z00zz__weakhashz00, BgL_bgl_string3567za700za7za7_3739za7, "arg1700", 7 );
DEFINE_STRING( BGl_string3486z00zz__weakhashz00, BgL_bgl_string3486za700za7za7_3740za7, "weak-hashtable->list", 20 );
DEFINE_STRING( BGl_string3487z00zz__weakhashz00, BgL_bgl_string3487za700za7za7_3741za7, "pair-nil", 8 );
DEFINE_STRING( BGl_string3488z00zz__weakhashz00, BgL_bgl_string3488za700za7za7_3742za7, "&weak-hashtable->list", 21 );
DEFINE_STRING( BGl_string3489z00zz__weakhashz00, BgL_bgl_string3489za700za7za7_3743za7, "weak-hashtable-key-list", 23 );
DEFINE_EXPORT_BGL_PROCEDURE( BGl_weakzd2hashtablezd2keyzd2listzd2envz00zz__weakhashz00, BgL_bgl_za762weakza7d2hashta3744z00, BGl_z62weakzd2hashtablezd2keyzd2listzb0zz__weakhashz00, 0L, BUNSPEC, 1 );

/* GC roots registration */
static obj_t bgl_gc_roots_register() {
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
void *roots_min = (void*)ULONG_MAX, *roots_max = 0;
ADD_ROOT( (void *)(&BGl_list3501z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3537z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3458z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3460z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3462z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3546z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3432z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3515z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3549z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3469z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_requirezd2initializa7ationz75zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3551z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3472z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3474z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3523z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3478z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3528z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3561z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3480z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3563z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3566z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3569z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3536z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_removez00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3457z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3574z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3542z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3576z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3495z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3545z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3548z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3468z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_keepgoingz00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3584z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3471z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3587z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3556z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3476z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3477z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3560z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3565z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3568z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3573z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3494z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3583z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_list3586z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3502z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3504z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3428z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_removestopz00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3433z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3516z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3435z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3518z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3437z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3440z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3524z00zz__weakhashz00) );
ADD_ROOT( (void *)(&BGl_symbol3529z00zz__weakhashz00) );
#undef ADD_ROOT
if( roots_max > 0 ) GC_add_roots( roots_min, ((void **)roots_max) + 1 );
#endif
return BUNSPEC;
}



/* module-initialization */
BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__weakhashz00(long BgL_checksumz00_5793, char * BgL_fromz00_5794)
{
{ 
if(
CBOOL(BGl_requirezd2initializa7ationz75zz__weakhashz00))
{ 
BGl_requirezd2initializa7ationz75zz__weakhashz00 = 
BBOOL(((bool_t)0)); 
BGl_gczd2rootszd2initz00zz__weakhashz00(); 
BGl_cnstzd2initzd2zz__weakhashz00(); 
BGl_importedzd2moduleszd2initz00zz__weakhashz00(); 
return 
BGl_toplevelzd2initzd2zz__weakhashz00();}  else 
{ 
return BUNSPEC;} } 

}



/* cnst-init */
obj_t BGl_cnstzd2initzd2zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
BGl_symbol3428z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3429z00zz__weakhashz00); 
BGl_symbol3433z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3434z00zz__weakhashz00); 
BGl_symbol3435z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3436z00zz__weakhashz00); 
BGl_symbol3437z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3438z00zz__weakhashz00); 
BGl_list3432z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3435z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3435z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL)))); 
BGl_symbol3440z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3441z00zz__weakhashz00); 
BGl_symbol3458z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3459z00zz__weakhashz00); 
BGl_symbol3460z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3461z00zz__weakhashz00); 
BGl_symbol3462z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3463z00zz__weakhashz00); 
BGl_list3457z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3460z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3462z00zz__weakhashz00, BNIL))))); 
BGl_symbol3469z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3470z00zz__weakhashz00); 
BGl_list3468z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3469z00zz__weakhashz00, BNIL))))); 
BGl_symbol3472z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3473z00zz__weakhashz00); 
BGl_symbol3474z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3475z00zz__weakhashz00); 
BGl_list3471z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3472z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3474z00zz__weakhashz00, BNIL))))); 
BGl_list3476z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3474z00zz__weakhashz00, BNIL))))); 
BGl_symbol3478z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3479z00zz__weakhashz00); 
BGl_symbol3480z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3481z00zz__weakhashz00); 
BGl_list3477z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3478z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3480z00zz__weakhashz00, BNIL))))); 
BGl_symbol3495z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3496z00zz__weakhashz00); 
BGl_list3494z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3495z00zz__weakhashz00, BNIL))))); 
BGl_symbol3502z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3503z00zz__weakhashz00); 
BGl_symbol3504z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3505z00zz__weakhashz00); 
BGl_list3501z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3458z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3502z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3504z00zz__weakhashz00, BNIL))))); 
BGl_symbol3516z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3517z00zz__weakhashz00); 
BGl_symbol3518z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3519z00zz__weakhashz00); 
BGl_list3515z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3518z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_symbol3524z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3525z00zz__weakhashz00); 
BGl_list3523z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3524z00zz__weakhashz00, BNIL))))); 
BGl_symbol3529z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3530z00zz__weakhashz00); 
BGl_list3528z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3529z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_symbol3537z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3538z00zz__weakhashz00); 
BGl_list3536z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3537z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_list3542z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3524z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_symbol3546z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3547z00zz__weakhashz00); 
BGl_list3545z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3546z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_symbol3549z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3550z00zz__weakhashz00); 
BGl_symbol3551z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3552z00zz__weakhashz00); 
BGl_list3548z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3551z00zz__weakhashz00, BNIL)))); 
BGl_list3556z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3495z00zz__weakhashz00, BNIL)))); 
BGl_symbol3561z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3562z00zz__weakhashz00); 
BGl_symbol3563z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3564z00zz__weakhashz00); 
BGl_list3560z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3561z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3563z00zz__weakhashz00, BNIL))))); 
BGl_symbol3566z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3567z00zz__weakhashz00); 
BGl_list3565z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3566z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_symbol3569z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3570z00zz__weakhashz00); 
BGl_list3568z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3561z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3569z00zz__weakhashz00, BNIL))))); 
BGl_symbol3574z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3575z00zz__weakhashz00); 
BGl_symbol3576z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3577z00zz__weakhashz00); 
BGl_list3573z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3549z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3574z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3576z00zz__weakhashz00, BNIL))))); 
BGl_symbol3584z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3585z00zz__weakhashz00); 
BGl_list3583z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3584z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))); 
BGl_symbol3587z00zz__weakhashz00 = 
bstring_to_symbol(BGl_string3588z00zz__weakhashz00); 
return ( 
BGl_list3586z00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BGl_symbol3433z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3516z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3587z00zz__weakhashz00, 
MAKE_YOUNG_PAIR(BGl_symbol3437z00zz__weakhashz00, BNIL))))), BUNSPEC) ;} 

}



/* gc-roots-init */
obj_t BGl_gczd2rootszd2initz00zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
return 
bgl_gc_roots_register();} 

}



/* toplevel-init */
obj_t BGl_toplevelzd2initzd2zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
BGl_keepgoingz00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BUNSPEC, BUNSPEC); 
BGl_removez00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BUNSPEC, BUNSPEC); 
return ( 
BGl_removestopz00zz__weakhashz00 = 
MAKE_YOUNG_PAIR(BUNSPEC, BUNSPEC), BUNSPEC) ;} 

}



/* table-get-hashnumber */
long BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(obj_t BgL_tablez00_37, obj_t BgL_keyz00_38)
{
{ /* Llib/hash.sch 21 */
{ /* Llib/hash.sch 22 */
 obj_t BgL_hashnz00_1330;
{ /* Llib/hash.sch 15 */
 bool_t BgL_test3746z00_5945;
{ /* Llib/hash.sch 15 */
 obj_t BgL_tmpz00_5946;
{ /* Llib/hash.sch 15 */
 obj_t BgL_res2203z00_2627;
{ /* Llib/hash.sch 15 */
 obj_t BgL_aux2541z00_4773;
BgL_aux2541z00_4773 = 
STRUCT_KEY(BgL_tablez00_37); 
if(
SYMBOLP(BgL_aux2541z00_4773))
{ /* Llib/hash.sch 15 */
BgL_res2203z00_2627 = BgL_aux2541z00_4773; }  else 
{ 
 obj_t BgL_auxz00_5950;
BgL_auxz00_5950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3425z00zz__weakhashz00, 
BINT(963L), BGl_string3426z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2541z00_4773); 
FAILURE(BgL_auxz00_5950,BFALSE,BFALSE);} } 
BgL_tmpz00_5946 = BgL_res2203z00_2627; } 
BgL_test3746z00_5945 = 
(BgL_tmpz00_5946==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3746z00_5945)
{ /* Llib/hash.sch 15 */
 int BgL_tmpz00_5955;
BgL_tmpz00_5955 = 
(int)(4L); 
BgL_hashnz00_1330 = 
STRUCT_REF(BgL_tablez00_37, BgL_tmpz00_5955); }  else 
{ /* Llib/hash.sch 15 */
BgL_hashnz00_1330 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_37); } } 
if(
PROCEDUREP(BgL_hashnz00_1330))
{ /* Llib/hash.sch 25 */
 obj_t BgL_arg1354z00_1332;
if(
PROCEDURE_CORRECT_ARITYP(BgL_hashnz00_1330, 1))
{ /* Llib/hash.sch 25 */
BgL_arg1354z00_1332 = 
BGL_PROCEDURE_CALL1(BgL_hashnz00_1330, BgL_keyz00_38); }  else 
{ /* Llib/hash.sch 25 */
FAILURE(BGl_string3431z00zz__weakhashz00,BGl_list3432z00zz__weakhashz00,BgL_hashnz00_1330);} 
{ /* Llib/hash.sch 25 */
 long BgL_nz00_2628;
{ /* Llib/hash.sch 25 */
 obj_t BgL_tmpz00_5968;
if(
INTEGERP(BgL_arg1354z00_1332))
{ /* Llib/hash.sch 25 */
BgL_tmpz00_5968 = BgL_arg1354z00_1332
; }  else 
{ 
 obj_t BgL_auxz00_5971;
BgL_auxz00_5971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3425z00zz__weakhashz00, 
BINT(1434L), BGl_string3426z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1354z00_1332); 
FAILURE(BgL_auxz00_5971,BFALSE,BFALSE);} 
BgL_nz00_2628 = 
(long)CINT(BgL_tmpz00_5968); } 
if(
(BgL_nz00_2628<0L))
{ /* Llib/hash.sch 25 */
return 
NEG(BgL_nz00_2628);}  else 
{ /* Llib/hash.sch 25 */
return BgL_nz00_2628;} } }  else 
{ /* Llib/hash.sch 24 */
if(
(BgL_hashnz00_1330==BGl_symbol3440z00zz__weakhashz00))
{ /* Llib/hash.sch 26 */
return 
BGl_getzd2hashnumberzd2persistentz00zz__hashz00(BgL_keyz00_38);}  else 
{ /* Llib/hash.sch 26 */
return 
BGl_getzd2hashnumberzd2zz__hashz00(BgL_keyz00_38);} } } } 

}



/* traverse-buckets */
obj_t BGl_traversezd2bucketszd2zz__weakhashz00(obj_t BgL_tablez00_47, obj_t BgL_bucketsz00_48, long BgL_iz00_49, obj_t BgL_funz00_50)
{
{ /* Llib/weakhash.scm 144 */
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3753z00_5983;
{ /* Llib/weakhash.scm 151 */
 obj_t BgL_arg1397z00_1402;
{ /* Llib/weakhash.scm 151 */
 bool_t BgL_test3754z00_5984;
{ /* Llib/weakhash.scm 151 */
 obj_t BgL_tmpz00_5985;
{ /* Llib/weakhash.scm 151 */
 obj_t BgL_res2207z00_2672;
{ /* Llib/weakhash.scm 151 */
 obj_t BgL_aux2545z00_4778;
BgL_aux2545z00_4778 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2545z00_4778))
{ /* Llib/weakhash.scm 151 */
BgL_res2207z00_2672 = BgL_aux2545z00_4778; }  else 
{ 
 obj_t BgL_auxz00_5989;
BgL_auxz00_5989 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5859L), BGl_string3443z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2545z00_4778); 
FAILURE(BgL_auxz00_5989,BFALSE,BFALSE);} } 
BgL_tmpz00_5985 = BgL_res2207z00_2672; } 
BgL_test3754z00_5984 = 
(BgL_tmpz00_5985==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3754z00_5984)
{ /* Llib/weakhash.scm 151 */
 int BgL_tmpz00_5994;
BgL_tmpz00_5994 = 
(int)(5L); 
BgL_arg1397z00_1402 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_5994); }  else 
{ /* Llib/weakhash.scm 151 */
BgL_arg1397z00_1402 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 151 */
 long BgL_n2z00_2673;
{ /* Llib/weakhash.scm 151 */
 obj_t BgL_tmpz00_5998;
if(
INTEGERP(BgL_arg1397z00_1402))
{ /* Llib/weakhash.scm 151 */
BgL_tmpz00_5998 = BgL_arg1397z00_1402
; }  else 
{ 
 obj_t BgL_auxz00_6001;
BgL_auxz00_6001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5881L), BGl_string3443z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1397z00_1402); 
FAILURE(BgL_auxz00_6001,BFALSE,BFALSE);} 
BgL_n2z00_2673 = 
(long)CINT(BgL_tmpz00_5998); } 
BgL_test3753z00_5983 = 
(1L==BgL_n2z00_2673); } } 
if(BgL_test3753z00_5983)
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_g1060z00_1343;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_vectorz00_2674;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 145 */
BgL_vectorz00_2674 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6009;
BgL_auxz00_6009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3443z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6009,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3758z00_6013;
{ /* Llib/weakhash.scm 145 */
 long BgL_tmpz00_6014;
BgL_tmpz00_6014 = 
VECTOR_LENGTH(BgL_vectorz00_2674); 
BgL_test3758z00_6013 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6014); } 
if(BgL_test3758z00_6013)
{ /* Llib/weakhash.scm 145 */
BgL_g1060z00_1343 = 
VECTOR_REF(BgL_vectorz00_2674,BgL_iz00_49); }  else 
{ 
 obj_t BgL_auxz00_6018;
BgL_auxz00_6018 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2674, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2674)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6018,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1345; obj_t BgL_lastzd2bucketzd2_1346;
BgL_bucketz00_1345 = BgL_g1060z00_1343; 
BgL_lastzd2bucketzd2_1346 = BFALSE; 
BgL_zc3z04anonymousza31364ze3z87_1347:
if(
NULLP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 145 */
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_retz00_1349;
{ /* Llib/weakhash.scm 153 */
 obj_t BgL_keyz00_1352;
{ /* Llib/weakhash.scm 153 */
 obj_t BgL_arg1369z00_1354;
{ /* Llib/weakhash.scm 153 */
 obj_t BgL_pairz00_2676;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 153 */
BgL_pairz00_2676 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6029;
BgL_auxz00_6029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5939L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6029,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 153 */
 obj_t BgL_pairz00_2679;
{ /* Llib/weakhash.scm 153 */
 obj_t BgL_aux2552z00_4785;
BgL_aux2552z00_4785 = 
CAR(BgL_pairz00_2676); 
if(
PAIRP(BgL_aux2552z00_4785))
{ /* Llib/weakhash.scm 153 */
BgL_pairz00_2679 = BgL_aux2552z00_4785; }  else 
{ 
 obj_t BgL_auxz00_6036;
BgL_auxz00_6036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5933L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2552z00_4785); 
FAILURE(BgL_auxz00_6036,BFALSE,BFALSE);} } 
BgL_arg1369z00_1354 = 
CAR(BgL_pairz00_2679); } } 
{ /* Llib/weakhash.scm 153 */
 obj_t BgL_objz00_2680;
if(
BGL_WEAKPTRP(BgL_arg1369z00_1354))
{ /* Llib/weakhash.scm 153 */
BgL_objz00_2680 = BgL_arg1369z00_1354; }  else 
{ 
 obj_t BgL_auxz00_6043;
BgL_auxz00_6043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5945L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1369z00_1354); 
FAILURE(BgL_auxz00_6043,BFALSE,BFALSE);} 
BgL_keyz00_1352 = 
bgl_weakptr_data(BgL_objz00_2680); } } 
if(
(BgL_keyz00_1352==BUNSPEC))
{ /* Llib/weakhash.scm 154 */
BgL_retz00_1349 = BGl_removez00zz__weakhashz00; }  else 
{ /* Llib/weakhash.scm 156 */
 obj_t BgL_arg1368z00_1353;
{ /* Llib/weakhash.scm 156 */
 obj_t BgL_pairz00_2681;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 156 */
BgL_pairz00_2681 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6052;
BgL_auxz00_6052 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6025L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6052,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 156 */
 obj_t BgL_pairz00_2684;
{ /* Llib/weakhash.scm 156 */
 obj_t BgL_aux2558z00_4791;
BgL_aux2558z00_4791 = 
CAR(BgL_pairz00_2681); 
if(
PAIRP(BgL_aux2558z00_4791))
{ /* Llib/weakhash.scm 156 */
BgL_pairz00_2684 = BgL_aux2558z00_4791; }  else 
{ 
 obj_t BgL_auxz00_6059;
BgL_auxz00_6059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6019L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2558z00_4791); 
FAILURE(BgL_auxz00_6059,BFALSE,BFALSE);} } 
BgL_arg1368z00_1353 = 
CDR(BgL_pairz00_2684); } } 
BgL_retz00_1349 = 
((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_L_ENTRY(BgL_funz00_50))(BgL_funz00_50, BgL_keyz00_1352, BgL_arg1368z00_1353, BgL_bucketz00_1345); } } 
if(
(BgL_retz00_1349==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1366z00_1350;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2685;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2685 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6074;
BgL_auxz00_6074 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6074,BFALSE,BFALSE);} 
BgL_arg1366z00_1350 = 
CDR(BgL_pairz00_2685); } 
{ 
 obj_t BgL_lastzd2bucketzd2_6080; obj_t BgL_bucketz00_6079;
BgL_bucketz00_6079 = BgL_arg1366z00_1350; 
BgL_lastzd2bucketzd2_6080 = BgL_bucketz00_1345; 
BgL_lastzd2bucketzd2_1346 = BgL_lastzd2bucketzd2_6080; 
BgL_bucketz00_1345 = BgL_bucketz00_6079; 
goto BgL_zc3z04anonymousza31364ze3z87_1347;} }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1349==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2686;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2687;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3769z00_6083;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6084;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2208z00_2693;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2562z00_4795;
BgL_aux2562z00_4795 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2562z00_4795))
{ /* Llib/weakhash.scm 134 */
BgL_res2208z00_2693 = BgL_aux2562z00_4795; }  else 
{ 
 obj_t BgL_auxz00_6088;
BgL_auxz00_6088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2562z00_4795); 
FAILURE(BgL_auxz00_6088,BFALSE,BFALSE);} } 
BgL_tmpz00_6084 = BgL_res2208z00_2693; } 
BgL_test3769z00_6083 = 
(BgL_tmpz00_6084==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3769z00_6083)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6093;
BgL_tmpz00_6093 = 
(int)(0L); 
BgL_arg1359z00_2687 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6093); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2687 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2694;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6097;
if(
INTEGERP(BgL_arg1359z00_2687))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6097 = BgL_arg1359z00_2687
; }  else 
{ 
 obj_t BgL_auxz00_6100;
BgL_auxz00_6100 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2687); 
FAILURE(BgL_auxz00_6100,BFALSE,BFALSE);} 
BgL_za71za7_2694 = 
(long)CINT(BgL_tmpz00_6097); } 
BgL_arg1358z00_2686 = 
(BgL_za71za7_2694-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3772z00_6106;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6107;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2209z00_2698;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2565z00_4798;
BgL_aux2565z00_4798 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2565z00_4798))
{ /* Llib/weakhash.scm 134 */
BgL_res2209z00_2698 = BgL_aux2565z00_4798; }  else 
{ 
 obj_t BgL_auxz00_6111;
BgL_auxz00_6111 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2565z00_4798); 
FAILURE(BgL_auxz00_6111,BFALSE,BFALSE);} } 
BgL_tmpz00_6107 = BgL_res2209z00_2698; } 
BgL_test3772z00_6106 = 
(BgL_tmpz00_6107==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3772z00_6106)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6118; int BgL_tmpz00_6116;
BgL_auxz00_6118 = 
BINT(BgL_arg1358z00_2686); 
BgL_tmpz00_6116 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6116, BgL_auxz00_6118); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1346))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2688;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2699;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2699 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6126;
BgL_auxz00_6126 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6126,BFALSE,BFALSE);} 
BgL_arg1360z00_2688 = 
CDR(BgL_pairz00_2699); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2700;
if(
PAIRP(BgL_lastzd2bucketzd2_1346))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2700 = BgL_lastzd2bucketzd2_1346; }  else 
{ 
 obj_t BgL_auxz00_6133;
BgL_auxz00_6133 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1346); 
FAILURE(BgL_auxz00_6133,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2700, BgL_arg1360z00_2688); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2689;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2701;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2701 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6140;
BgL_auxz00_6140 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6140,BFALSE,BFALSE);} 
BgL_arg1361z00_2689 = 
CDR(BgL_pairz00_2701); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2702;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2702 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6147;
BgL_auxz00_6147 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6147,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3779z00_6151;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6152;
BgL_tmpz00_6152 = 
VECTOR_LENGTH(BgL_vectorz00_2702); 
BgL_test3779z00_6151 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6152); } 
if(BgL_test3779z00_6151)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2702,BgL_iz00_49,BgL_arg1361z00_2689); }  else 
{ 
 obj_t BgL_auxz00_6156;
BgL_auxz00_6156 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2702, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2702)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6156,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1367z00_1351;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2704;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2704 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6165;
BgL_auxz00_6165 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6165,BFALSE,BFALSE);} 
BgL_arg1367z00_1351 = 
CDR(BgL_pairz00_2704); } 
{ 
 obj_t BgL_bucketz00_6170;
BgL_bucketz00_6170 = BgL_arg1367z00_1351; 
BgL_bucketz00_1345 = BgL_bucketz00_6170; 
goto BgL_zc3z04anonymousza31364ze3z87_1347;} } }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1349==BGl_removestopz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2705;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2706;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3782z00_6173;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6174;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2210z00_2712;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2577z00_4810;
BgL_aux2577z00_4810 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2577z00_4810))
{ /* Llib/weakhash.scm 134 */
BgL_res2210z00_2712 = BgL_aux2577z00_4810; }  else 
{ 
 obj_t BgL_auxz00_6178;
BgL_auxz00_6178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2577z00_4810); 
FAILURE(BgL_auxz00_6178,BFALSE,BFALSE);} } 
BgL_tmpz00_6174 = BgL_res2210z00_2712; } 
BgL_test3782z00_6173 = 
(BgL_tmpz00_6174==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3782z00_6173)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6183;
BgL_tmpz00_6183 = 
(int)(0L); 
BgL_arg1359z00_2706 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6183); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2706 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2713;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6187;
if(
INTEGERP(BgL_arg1359z00_2706))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6187 = BgL_arg1359z00_2706
; }  else 
{ 
 obj_t BgL_auxz00_6190;
BgL_auxz00_6190 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2706); 
FAILURE(BgL_auxz00_6190,BFALSE,BFALSE);} 
BgL_za71za7_2713 = 
(long)CINT(BgL_tmpz00_6187); } 
BgL_arg1358z00_2705 = 
(BgL_za71za7_2713-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3785z00_6196;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6197;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2211z00_2717;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2580z00_4813;
BgL_aux2580z00_4813 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2580z00_4813))
{ /* Llib/weakhash.scm 134 */
BgL_res2211z00_2717 = BgL_aux2580z00_4813; }  else 
{ 
 obj_t BgL_auxz00_6201;
BgL_auxz00_6201 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2580z00_4813); 
FAILURE(BgL_auxz00_6201,BFALSE,BFALSE);} } 
BgL_tmpz00_6197 = BgL_res2211z00_2717; } 
BgL_test3785z00_6196 = 
(BgL_tmpz00_6197==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3785z00_6196)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6208; int BgL_tmpz00_6206;
BgL_auxz00_6208 = 
BINT(BgL_arg1358z00_2705); 
BgL_tmpz00_6206 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6206, BgL_auxz00_6208); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1346))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2707;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2718;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2718 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6216;
BgL_auxz00_6216 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6216,BFALSE,BFALSE);} 
BgL_arg1360z00_2707 = 
CDR(BgL_pairz00_2718); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2719;
if(
PAIRP(BgL_lastzd2bucketzd2_1346))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2719 = BgL_lastzd2bucketzd2_1346; }  else 
{ 
 obj_t BgL_auxz00_6223;
BgL_auxz00_6223 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1346); 
FAILURE(BgL_auxz00_6223,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2719, BgL_arg1360z00_2707); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2708;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2720;
if(
PAIRP(BgL_bucketz00_1345))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2720 = BgL_bucketz00_1345; }  else 
{ 
 obj_t BgL_auxz00_6230;
BgL_auxz00_6230 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1345); 
FAILURE(BgL_auxz00_6230,BFALSE,BFALSE);} 
BgL_arg1361z00_2708 = 
CDR(BgL_pairz00_2720); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2721;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2721 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6237;
BgL_auxz00_6237 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6237,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3792z00_6241;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6242;
BgL_tmpz00_6242 = 
VECTOR_LENGTH(BgL_vectorz00_2721); 
BgL_test3792z00_6241 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6242); } 
if(BgL_test3792z00_6241)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2721,BgL_iz00_49,BgL_arg1361z00_2708); }  else 
{ 
 obj_t BgL_auxz00_6246;
BgL_auxz00_6246 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2721, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2721)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6246,BFALSE,BFALSE);} } } } 
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
return BgL_retz00_1349;} } } } } }  else 
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3793z00_6253;
{ /* Llib/weakhash.scm 157 */
 obj_t BgL_arg1396z00_1401;
{ /* Llib/weakhash.scm 157 */
 bool_t BgL_test3794z00_6254;
{ /* Llib/weakhash.scm 157 */
 obj_t BgL_tmpz00_6255;
{ /* Llib/weakhash.scm 157 */
 obj_t BgL_res2212z00_2726;
{ /* Llib/weakhash.scm 157 */
 obj_t BgL_aux2590z00_4823;
BgL_aux2590z00_4823 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2590z00_4823))
{ /* Llib/weakhash.scm 157 */
BgL_res2212z00_2726 = BgL_aux2590z00_4823; }  else 
{ 
 obj_t BgL_auxz00_6259;
BgL_auxz00_6259 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6071L), BGl_string3443z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2590z00_4823); 
FAILURE(BgL_auxz00_6259,BFALSE,BFALSE);} } 
BgL_tmpz00_6255 = BgL_res2212z00_2726; } 
BgL_test3794z00_6254 = 
(BgL_tmpz00_6255==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3794z00_6254)
{ /* Llib/weakhash.scm 157 */
 int BgL_tmpz00_6264;
BgL_tmpz00_6264 = 
(int)(5L); 
BgL_arg1396z00_1401 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6264); }  else 
{ /* Llib/weakhash.scm 157 */
BgL_arg1396z00_1401 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 157 */
 long BgL_n2z00_2727;
{ /* Llib/weakhash.scm 157 */
 obj_t BgL_tmpz00_6268;
if(
INTEGERP(BgL_arg1396z00_1401))
{ /* Llib/weakhash.scm 157 */
BgL_tmpz00_6268 = BgL_arg1396z00_1401
; }  else 
{ 
 obj_t BgL_auxz00_6271;
BgL_auxz00_6271 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6093L), BGl_string3443z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1396z00_1401); 
FAILURE(BgL_auxz00_6271,BFALSE,BFALSE);} 
BgL_n2z00_2727 = 
(long)CINT(BgL_tmpz00_6268); } 
BgL_test3793z00_6253 = 
(2L==BgL_n2z00_2727); } } 
if(BgL_test3793z00_6253)
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_g1061z00_1358;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_vectorz00_2728;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 145 */
BgL_vectorz00_2728 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6279;
BgL_auxz00_6279 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3443z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6279,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3798z00_6283;
{ /* Llib/weakhash.scm 145 */
 long BgL_tmpz00_6284;
BgL_tmpz00_6284 = 
VECTOR_LENGTH(BgL_vectorz00_2728); 
BgL_test3798z00_6283 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6284); } 
if(BgL_test3798z00_6283)
{ /* Llib/weakhash.scm 145 */
BgL_g1061z00_1358 = 
VECTOR_REF(BgL_vectorz00_2728,BgL_iz00_49); }  else 
{ 
 obj_t BgL_auxz00_6288;
BgL_auxz00_6288 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2728, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2728)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6288,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1360; obj_t BgL_lastzd2bucketzd2_1361;
BgL_bucketz00_1360 = BgL_g1061z00_1358; 
BgL_lastzd2bucketzd2_1361 = BFALSE; 
BgL_zc3z04anonymousza31372ze3z87_1362:
if(
NULLP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 145 */
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_retz00_1364;
{ /* Llib/weakhash.scm 159 */
 obj_t BgL_dataz00_1367;
{ /* Llib/weakhash.scm 159 */
 obj_t BgL_arg1378z00_1369;
{ /* Llib/weakhash.scm 159 */
 obj_t BgL_pairz00_2730;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 159 */
BgL_pairz00_2730 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6299;
BgL_auxz00_6299 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6152L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6299,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 159 */
 obj_t BgL_pairz00_2733;
{ /* Llib/weakhash.scm 159 */
 obj_t BgL_aux2597z00_4830;
BgL_aux2597z00_4830 = 
CAR(BgL_pairz00_2730); 
if(
PAIRP(BgL_aux2597z00_4830))
{ /* Llib/weakhash.scm 159 */
BgL_pairz00_2733 = BgL_aux2597z00_4830; }  else 
{ 
 obj_t BgL_auxz00_6306;
BgL_auxz00_6306 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6146L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2597z00_4830); 
FAILURE(BgL_auxz00_6306,BFALSE,BFALSE);} } 
BgL_arg1378z00_1369 = 
CDR(BgL_pairz00_2733); } } 
{ /* Llib/weakhash.scm 159 */
 obj_t BgL_objz00_2734;
if(
BGL_WEAKPTRP(BgL_arg1378z00_1369))
{ /* Llib/weakhash.scm 159 */
BgL_objz00_2734 = BgL_arg1378z00_1369; }  else 
{ 
 obj_t BgL_auxz00_6313;
BgL_auxz00_6313 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6158L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1378z00_1369); 
FAILURE(BgL_auxz00_6313,BFALSE,BFALSE);} 
BgL_dataz00_1367 = 
bgl_weakptr_data(BgL_objz00_2734); } } 
if(
(BgL_dataz00_1367==BUNSPEC))
{ /* Llib/weakhash.scm 160 */
BgL_retz00_1364 = BGl_removez00zz__weakhashz00; }  else 
{ /* Llib/weakhash.scm 162 */
 obj_t BgL_arg1377z00_1368;
{ /* Llib/weakhash.scm 162 */
 obj_t BgL_pairz00_2735;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 162 */
BgL_pairz00_2735 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6322;
BgL_auxz00_6322 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6235L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6322,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 162 */
 obj_t BgL_pairz00_2738;
{ /* Llib/weakhash.scm 162 */
 obj_t BgL_aux2603z00_4836;
BgL_aux2603z00_4836 = 
CAR(BgL_pairz00_2735); 
if(
PAIRP(BgL_aux2603z00_4836))
{ /* Llib/weakhash.scm 162 */
BgL_pairz00_2738 = BgL_aux2603z00_4836; }  else 
{ 
 obj_t BgL_auxz00_6329;
BgL_auxz00_6329 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6229L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2603z00_4836); 
FAILURE(BgL_auxz00_6329,BFALSE,BFALSE);} } 
BgL_arg1377z00_1368 = 
CAR(BgL_pairz00_2738); } } 
BgL_retz00_1364 = 
((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_L_ENTRY(BgL_funz00_50))(BgL_funz00_50, BgL_arg1377z00_1368, BgL_dataz00_1367, BgL_bucketz00_1360); } } 
if(
(BgL_retz00_1364==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1375z00_1365;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2739;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2739 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6344;
BgL_auxz00_6344 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6344,BFALSE,BFALSE);} 
BgL_arg1375z00_1365 = 
CDR(BgL_pairz00_2739); } 
{ 
 obj_t BgL_lastzd2bucketzd2_6350; obj_t BgL_bucketz00_6349;
BgL_bucketz00_6349 = BgL_arg1375z00_1365; 
BgL_lastzd2bucketzd2_6350 = BgL_bucketz00_1360; 
BgL_lastzd2bucketzd2_1361 = BgL_lastzd2bucketzd2_6350; 
BgL_bucketz00_1360 = BgL_bucketz00_6349; 
goto BgL_zc3z04anonymousza31372ze3z87_1362;} }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1364==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2740;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2741;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3809z00_6353;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6354;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2213z00_2747;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2607z00_4840;
BgL_aux2607z00_4840 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2607z00_4840))
{ /* Llib/weakhash.scm 134 */
BgL_res2213z00_2747 = BgL_aux2607z00_4840; }  else 
{ 
 obj_t BgL_auxz00_6358;
BgL_auxz00_6358 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2607z00_4840); 
FAILURE(BgL_auxz00_6358,BFALSE,BFALSE);} } 
BgL_tmpz00_6354 = BgL_res2213z00_2747; } 
BgL_test3809z00_6353 = 
(BgL_tmpz00_6354==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3809z00_6353)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6363;
BgL_tmpz00_6363 = 
(int)(0L); 
BgL_arg1359z00_2741 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6363); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2741 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2748;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6367;
if(
INTEGERP(BgL_arg1359z00_2741))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6367 = BgL_arg1359z00_2741
; }  else 
{ 
 obj_t BgL_auxz00_6370;
BgL_auxz00_6370 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2741); 
FAILURE(BgL_auxz00_6370,BFALSE,BFALSE);} 
BgL_za71za7_2748 = 
(long)CINT(BgL_tmpz00_6367); } 
BgL_arg1358z00_2740 = 
(BgL_za71za7_2748-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3812z00_6376;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6377;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2214z00_2752;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2610z00_4843;
BgL_aux2610z00_4843 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2610z00_4843))
{ /* Llib/weakhash.scm 134 */
BgL_res2214z00_2752 = BgL_aux2610z00_4843; }  else 
{ 
 obj_t BgL_auxz00_6381;
BgL_auxz00_6381 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2610z00_4843); 
FAILURE(BgL_auxz00_6381,BFALSE,BFALSE);} } 
BgL_tmpz00_6377 = BgL_res2214z00_2752; } 
BgL_test3812z00_6376 = 
(BgL_tmpz00_6377==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3812z00_6376)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6388; int BgL_tmpz00_6386;
BgL_auxz00_6388 = 
BINT(BgL_arg1358z00_2740); 
BgL_tmpz00_6386 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6386, BgL_auxz00_6388); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1361))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2742;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2753;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2753 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6396;
BgL_auxz00_6396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6396,BFALSE,BFALSE);} 
BgL_arg1360z00_2742 = 
CDR(BgL_pairz00_2753); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2754;
if(
PAIRP(BgL_lastzd2bucketzd2_1361))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2754 = BgL_lastzd2bucketzd2_1361; }  else 
{ 
 obj_t BgL_auxz00_6403;
BgL_auxz00_6403 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1361); 
FAILURE(BgL_auxz00_6403,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2754, BgL_arg1360z00_2742); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2743;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2755;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2755 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6410;
BgL_auxz00_6410 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6410,BFALSE,BFALSE);} 
BgL_arg1361z00_2743 = 
CDR(BgL_pairz00_2755); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2756;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2756 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6417;
BgL_auxz00_6417 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6417,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3819z00_6421;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6422;
BgL_tmpz00_6422 = 
VECTOR_LENGTH(BgL_vectorz00_2756); 
BgL_test3819z00_6421 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6422); } 
if(BgL_test3819z00_6421)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2756,BgL_iz00_49,BgL_arg1361z00_2743); }  else 
{ 
 obj_t BgL_auxz00_6426;
BgL_auxz00_6426 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2756, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2756)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6426,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1376z00_1366;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2758;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2758 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6435;
BgL_auxz00_6435 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6435,BFALSE,BFALSE);} 
BgL_arg1376z00_1366 = 
CDR(BgL_pairz00_2758); } 
{ 
 obj_t BgL_bucketz00_6440;
BgL_bucketz00_6440 = BgL_arg1376z00_1366; 
BgL_bucketz00_1360 = BgL_bucketz00_6440; 
goto BgL_zc3z04anonymousza31372ze3z87_1362;} } }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1364==BGl_removestopz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2759;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2760;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3822z00_6443;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6444;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2215z00_2766;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2622z00_4855;
BgL_aux2622z00_4855 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2622z00_4855))
{ /* Llib/weakhash.scm 134 */
BgL_res2215z00_2766 = BgL_aux2622z00_4855; }  else 
{ 
 obj_t BgL_auxz00_6448;
BgL_auxz00_6448 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2622z00_4855); 
FAILURE(BgL_auxz00_6448,BFALSE,BFALSE);} } 
BgL_tmpz00_6444 = BgL_res2215z00_2766; } 
BgL_test3822z00_6443 = 
(BgL_tmpz00_6444==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3822z00_6443)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6453;
BgL_tmpz00_6453 = 
(int)(0L); 
BgL_arg1359z00_2760 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6453); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2760 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2767;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6457;
if(
INTEGERP(BgL_arg1359z00_2760))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6457 = BgL_arg1359z00_2760
; }  else 
{ 
 obj_t BgL_auxz00_6460;
BgL_auxz00_6460 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2760); 
FAILURE(BgL_auxz00_6460,BFALSE,BFALSE);} 
BgL_za71za7_2767 = 
(long)CINT(BgL_tmpz00_6457); } 
BgL_arg1358z00_2759 = 
(BgL_za71za7_2767-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3825z00_6466;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6467;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2216z00_2771;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2625z00_4858;
BgL_aux2625z00_4858 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2625z00_4858))
{ /* Llib/weakhash.scm 134 */
BgL_res2216z00_2771 = BgL_aux2625z00_4858; }  else 
{ 
 obj_t BgL_auxz00_6471;
BgL_auxz00_6471 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2625z00_4858); 
FAILURE(BgL_auxz00_6471,BFALSE,BFALSE);} } 
BgL_tmpz00_6467 = BgL_res2216z00_2771; } 
BgL_test3825z00_6466 = 
(BgL_tmpz00_6467==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3825z00_6466)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6478; int BgL_tmpz00_6476;
BgL_auxz00_6478 = 
BINT(BgL_arg1358z00_2759); 
BgL_tmpz00_6476 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6476, BgL_auxz00_6478); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1361))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2761;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2772;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2772 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6486;
BgL_auxz00_6486 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6486,BFALSE,BFALSE);} 
BgL_arg1360z00_2761 = 
CDR(BgL_pairz00_2772); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2773;
if(
PAIRP(BgL_lastzd2bucketzd2_1361))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2773 = BgL_lastzd2bucketzd2_1361; }  else 
{ 
 obj_t BgL_auxz00_6493;
BgL_auxz00_6493 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1361); 
FAILURE(BgL_auxz00_6493,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2773, BgL_arg1360z00_2761); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2762;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2774;
if(
PAIRP(BgL_bucketz00_1360))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2774 = BgL_bucketz00_1360; }  else 
{ 
 obj_t BgL_auxz00_6500;
BgL_auxz00_6500 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1360); 
FAILURE(BgL_auxz00_6500,BFALSE,BFALSE);} 
BgL_arg1361z00_2762 = 
CDR(BgL_pairz00_2774); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2775;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2775 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6507;
BgL_auxz00_6507 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6507,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3832z00_6511;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6512;
BgL_tmpz00_6512 = 
VECTOR_LENGTH(BgL_vectorz00_2775); 
BgL_test3832z00_6511 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6512); } 
if(BgL_test3832z00_6511)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2775,BgL_iz00_49,BgL_arg1361z00_2762); }  else 
{ 
 obj_t BgL_auxz00_6516;
BgL_auxz00_6516 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2775, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2775)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6516,BFALSE,BFALSE);} } } } 
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
return BgL_retz00_1364;} } } } } }  else 
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3833z00_6523;
{ /* Llib/weakhash.scm 163 */
 obj_t BgL_arg1395z00_1400;
{ /* Llib/weakhash.scm 163 */
 bool_t BgL_test3834z00_6524;
{ /* Llib/weakhash.scm 163 */
 obj_t BgL_tmpz00_6525;
{ /* Llib/weakhash.scm 163 */
 obj_t BgL_res2217z00_2780;
{ /* Llib/weakhash.scm 163 */
 obj_t BgL_aux2635z00_4868;
BgL_aux2635z00_4868 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2635z00_4868))
{ /* Llib/weakhash.scm 163 */
BgL_res2217z00_2780 = BgL_aux2635z00_4868; }  else 
{ 
 obj_t BgL_auxz00_6529;
BgL_auxz00_6529 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6286L), BGl_string3443z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2635z00_4868); 
FAILURE(BgL_auxz00_6529,BFALSE,BFALSE);} } 
BgL_tmpz00_6525 = BgL_res2217z00_2780; } 
BgL_test3834z00_6524 = 
(BgL_tmpz00_6525==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3834z00_6524)
{ /* Llib/weakhash.scm 163 */
 int BgL_tmpz00_6534;
BgL_tmpz00_6534 = 
(int)(5L); 
BgL_arg1395z00_1400 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6534); }  else 
{ /* Llib/weakhash.scm 163 */
BgL_arg1395z00_1400 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 163 */
 long BgL_n2z00_2781;
{ /* Llib/weakhash.scm 163 */
 obj_t BgL_tmpz00_6538;
if(
INTEGERP(BgL_arg1395z00_1400))
{ /* Llib/weakhash.scm 163 */
BgL_tmpz00_6538 = BgL_arg1395z00_1400
; }  else 
{ 
 obj_t BgL_auxz00_6541;
BgL_auxz00_6541 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6308L), BGl_string3443z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1395z00_1400); 
FAILURE(BgL_auxz00_6541,BFALSE,BFALSE);} 
BgL_n2z00_2781 = 
(long)CINT(BgL_tmpz00_6538); } 
BgL_test3833z00_6523 = 
(3L==BgL_n2z00_2781); } } 
if(BgL_test3833z00_6523)
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_g1062z00_1373;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_vectorz00_2782;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 145 */
BgL_vectorz00_2782 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6549;
BgL_auxz00_6549 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3443z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6549,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3838z00_6553;
{ /* Llib/weakhash.scm 145 */
 long BgL_tmpz00_6554;
BgL_tmpz00_6554 = 
VECTOR_LENGTH(BgL_vectorz00_2782); 
BgL_test3838z00_6553 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6554); } 
if(BgL_test3838z00_6553)
{ /* Llib/weakhash.scm 145 */
BgL_g1062z00_1373 = 
VECTOR_REF(BgL_vectorz00_2782,BgL_iz00_49); }  else 
{ 
 obj_t BgL_auxz00_6558;
BgL_auxz00_6558 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2782, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2782)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6558,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1375; obj_t BgL_lastzd2bucketzd2_1376;
BgL_bucketz00_1375 = BgL_g1062z00_1373; 
BgL_lastzd2bucketzd2_1376 = BFALSE; 
BgL_zc3z04anonymousza31381ze3z87_1377:
if(
NULLP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 145 */
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_retz00_1379;
{ /* Llib/weakhash.scm 165 */
 obj_t BgL_keyz00_1382; obj_t BgL_dataz00_1383;
{ /* Llib/weakhash.scm 165 */
 obj_t BgL_arg1387z00_1385;
{ /* Llib/weakhash.scm 165 */
 obj_t BgL_pairz00_2784;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 165 */
BgL_pairz00_2784 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6569;
BgL_auxz00_6569 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6370L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6569,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 165 */
 obj_t BgL_pairz00_2787;
{ /* Llib/weakhash.scm 165 */
 obj_t BgL_aux2642z00_4875;
BgL_aux2642z00_4875 = 
CAR(BgL_pairz00_2784); 
if(
PAIRP(BgL_aux2642z00_4875))
{ /* Llib/weakhash.scm 165 */
BgL_pairz00_2787 = BgL_aux2642z00_4875; }  else 
{ 
 obj_t BgL_auxz00_6576;
BgL_auxz00_6576 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6364L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2642z00_4875); 
FAILURE(BgL_auxz00_6576,BFALSE,BFALSE);} } 
BgL_arg1387z00_1385 = 
CAR(BgL_pairz00_2787); } } 
{ /* Llib/weakhash.scm 165 */
 obj_t BgL_objz00_2788;
if(
BGL_WEAKPTRP(BgL_arg1387z00_1385))
{ /* Llib/weakhash.scm 165 */
BgL_objz00_2788 = BgL_arg1387z00_1385; }  else 
{ 
 obj_t BgL_auxz00_6583;
BgL_auxz00_6583 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6376L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1387z00_1385); 
FAILURE(BgL_auxz00_6583,BFALSE,BFALSE);} 
BgL_keyz00_1382 = 
bgl_weakptr_data(BgL_objz00_2788); } } 
{ /* Llib/weakhash.scm 166 */
 obj_t BgL_arg1388z00_1386;
{ /* Llib/weakhash.scm 166 */
 obj_t BgL_pairz00_2789;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 166 */
BgL_pairz00_2789 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6590;
BgL_auxz00_6590 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6415L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6590,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 166 */
 obj_t BgL_pairz00_2792;
{ /* Llib/weakhash.scm 166 */
 obj_t BgL_aux2648z00_4881;
BgL_aux2648z00_4881 = 
CAR(BgL_pairz00_2789); 
if(
PAIRP(BgL_aux2648z00_4881))
{ /* Llib/weakhash.scm 166 */
BgL_pairz00_2792 = BgL_aux2648z00_4881; }  else 
{ 
 obj_t BgL_auxz00_6597;
BgL_auxz00_6597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6409L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2648z00_4881); 
FAILURE(BgL_auxz00_6597,BFALSE,BFALSE);} } 
BgL_arg1388z00_1386 = 
CDR(BgL_pairz00_2792); } } 
{ /* Llib/weakhash.scm 166 */
 obj_t BgL_objz00_2793;
if(
BGL_WEAKPTRP(BgL_arg1388z00_1386))
{ /* Llib/weakhash.scm 166 */
BgL_objz00_2793 = BgL_arg1388z00_1386; }  else 
{ 
 obj_t BgL_auxz00_6604;
BgL_auxz00_6604 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6421L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1388z00_1386); 
FAILURE(BgL_auxz00_6604,BFALSE,BFALSE);} 
BgL_dataz00_1383 = 
bgl_weakptr_data(BgL_objz00_2793); } } 
{ /* Llib/weakhash.scm 167 */
 bool_t BgL_test3846z00_6609;
if(
(BgL_keyz00_1382==BUNSPEC))
{ /* Llib/weakhash.scm 167 */
BgL_test3846z00_6609 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 167 */
BgL_test3846z00_6609 = 
(BgL_dataz00_1383==BUNSPEC)
; } 
if(BgL_test3846z00_6609)
{ /* Llib/weakhash.scm 167 */
BgL_retz00_1379 = BGl_removez00zz__weakhashz00; }  else 
{ /* Llib/weakhash.scm 167 */
BgL_retz00_1379 = 
((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_L_ENTRY(BgL_funz00_50))(BgL_funz00_50, BgL_keyz00_1382, BgL_dataz00_1383, BgL_bucketz00_1375); } } } 
if(
(BgL_retz00_1379==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1383z00_1380;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2794;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2794 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6623;
BgL_auxz00_6623 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6623,BFALSE,BFALSE);} 
BgL_arg1383z00_1380 = 
CDR(BgL_pairz00_2794); } 
{ 
 obj_t BgL_lastzd2bucketzd2_6629; obj_t BgL_bucketz00_6628;
BgL_bucketz00_6628 = BgL_arg1383z00_1380; 
BgL_lastzd2bucketzd2_6629 = BgL_bucketz00_1375; 
BgL_lastzd2bucketzd2_1376 = BgL_lastzd2bucketzd2_6629; 
BgL_bucketz00_1375 = BgL_bucketz00_6628; 
goto BgL_zc3z04anonymousza31381ze3z87_1377;} }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1379==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2795;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2796;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3851z00_6632;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6633;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2218z00_2802;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2654z00_4887;
BgL_aux2654z00_4887 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2654z00_4887))
{ /* Llib/weakhash.scm 134 */
BgL_res2218z00_2802 = BgL_aux2654z00_4887; }  else 
{ 
 obj_t BgL_auxz00_6637;
BgL_auxz00_6637 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2654z00_4887); 
FAILURE(BgL_auxz00_6637,BFALSE,BFALSE);} } 
BgL_tmpz00_6633 = BgL_res2218z00_2802; } 
BgL_test3851z00_6632 = 
(BgL_tmpz00_6633==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3851z00_6632)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6642;
BgL_tmpz00_6642 = 
(int)(0L); 
BgL_arg1359z00_2796 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6642); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2796 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2803;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6646;
if(
INTEGERP(BgL_arg1359z00_2796))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6646 = BgL_arg1359z00_2796
; }  else 
{ 
 obj_t BgL_auxz00_6649;
BgL_auxz00_6649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2796); 
FAILURE(BgL_auxz00_6649,BFALSE,BFALSE);} 
BgL_za71za7_2803 = 
(long)CINT(BgL_tmpz00_6646); } 
BgL_arg1358z00_2795 = 
(BgL_za71za7_2803-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3854z00_6655;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6656;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2219z00_2807;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2657z00_4890;
BgL_aux2657z00_4890 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2657z00_4890))
{ /* Llib/weakhash.scm 134 */
BgL_res2219z00_2807 = BgL_aux2657z00_4890; }  else 
{ 
 obj_t BgL_auxz00_6660;
BgL_auxz00_6660 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2657z00_4890); 
FAILURE(BgL_auxz00_6660,BFALSE,BFALSE);} } 
BgL_tmpz00_6656 = BgL_res2219z00_2807; } 
BgL_test3854z00_6655 = 
(BgL_tmpz00_6656==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3854z00_6655)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6667; int BgL_tmpz00_6665;
BgL_auxz00_6667 = 
BINT(BgL_arg1358z00_2795); 
BgL_tmpz00_6665 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6665, BgL_auxz00_6667); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1376))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2797;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2808;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2808 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6675;
BgL_auxz00_6675 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6675,BFALSE,BFALSE);} 
BgL_arg1360z00_2797 = 
CDR(BgL_pairz00_2808); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2809;
if(
PAIRP(BgL_lastzd2bucketzd2_1376))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2809 = BgL_lastzd2bucketzd2_1376; }  else 
{ 
 obj_t BgL_auxz00_6682;
BgL_auxz00_6682 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1376); 
FAILURE(BgL_auxz00_6682,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2809, BgL_arg1360z00_2797); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2798;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2810;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2810 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6689;
BgL_auxz00_6689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6689,BFALSE,BFALSE);} 
BgL_arg1361z00_2798 = 
CDR(BgL_pairz00_2810); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2811;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2811 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6696;
BgL_auxz00_6696 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6696,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3861z00_6700;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6701;
BgL_tmpz00_6701 = 
VECTOR_LENGTH(BgL_vectorz00_2811); 
BgL_test3861z00_6700 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6701); } 
if(BgL_test3861z00_6700)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2811,BgL_iz00_49,BgL_arg1361z00_2798); }  else 
{ 
 obj_t BgL_auxz00_6705;
BgL_auxz00_6705 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2811, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2811)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6705,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1384z00_1381;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2813;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2813 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6714;
BgL_auxz00_6714 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6714,BFALSE,BFALSE);} 
BgL_arg1384z00_1381 = 
CDR(BgL_pairz00_2813); } 
{ 
 obj_t BgL_bucketz00_6719;
BgL_bucketz00_6719 = BgL_arg1384z00_1381; 
BgL_bucketz00_1375 = BgL_bucketz00_6719; 
goto BgL_zc3z04anonymousza31381ze3z87_1377;} } }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1379==BGl_removestopz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2814;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2815;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3864z00_6722;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6723;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2220z00_2821;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2669z00_4902;
BgL_aux2669z00_4902 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2669z00_4902))
{ /* Llib/weakhash.scm 134 */
BgL_res2220z00_2821 = BgL_aux2669z00_4902; }  else 
{ 
 obj_t BgL_auxz00_6727;
BgL_auxz00_6727 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2669z00_4902); 
FAILURE(BgL_auxz00_6727,BFALSE,BFALSE);} } 
BgL_tmpz00_6723 = BgL_res2220z00_2821; } 
BgL_test3864z00_6722 = 
(BgL_tmpz00_6723==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3864z00_6722)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6732;
BgL_tmpz00_6732 = 
(int)(0L); 
BgL_arg1359z00_2815 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6732); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2815 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2822;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6736;
if(
INTEGERP(BgL_arg1359z00_2815))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6736 = BgL_arg1359z00_2815
; }  else 
{ 
 obj_t BgL_auxz00_6739;
BgL_auxz00_6739 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2815); 
FAILURE(BgL_auxz00_6739,BFALSE,BFALSE);} 
BgL_za71za7_2822 = 
(long)CINT(BgL_tmpz00_6736); } 
BgL_arg1358z00_2814 = 
(BgL_za71za7_2822-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3867z00_6745;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6746;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2221z00_2826;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2672z00_4905;
BgL_aux2672z00_4905 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2672z00_4905))
{ /* Llib/weakhash.scm 134 */
BgL_res2221z00_2826 = BgL_aux2672z00_4905; }  else 
{ 
 obj_t BgL_auxz00_6750;
BgL_auxz00_6750 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2672z00_4905); 
FAILURE(BgL_auxz00_6750,BFALSE,BFALSE);} } 
BgL_tmpz00_6746 = BgL_res2221z00_2826; } 
BgL_test3867z00_6745 = 
(BgL_tmpz00_6746==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3867z00_6745)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6757; int BgL_tmpz00_6755;
BgL_auxz00_6757 = 
BINT(BgL_arg1358z00_2814); 
BgL_tmpz00_6755 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6755, BgL_auxz00_6757); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1376))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2816;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2827;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2827 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6765;
BgL_auxz00_6765 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6765,BFALSE,BFALSE);} 
BgL_arg1360z00_2816 = 
CDR(BgL_pairz00_2827); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2828;
if(
PAIRP(BgL_lastzd2bucketzd2_1376))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2828 = BgL_lastzd2bucketzd2_1376; }  else 
{ 
 obj_t BgL_auxz00_6772;
BgL_auxz00_6772 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1376); 
FAILURE(BgL_auxz00_6772,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2828, BgL_arg1360z00_2816); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2817;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2829;
if(
PAIRP(BgL_bucketz00_1375))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2829 = BgL_bucketz00_1375; }  else 
{ 
 obj_t BgL_auxz00_6779;
BgL_auxz00_6779 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1375); 
FAILURE(BgL_auxz00_6779,BFALSE,BFALSE);} 
BgL_arg1361z00_2817 = 
CDR(BgL_pairz00_2829); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2830;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2830 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6786;
BgL_auxz00_6786 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6786,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3874z00_6790;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6791;
BgL_tmpz00_6791 = 
VECTOR_LENGTH(BgL_vectorz00_2830); 
BgL_test3874z00_6790 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6791); } 
if(BgL_test3874z00_6790)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2830,BgL_iz00_49,BgL_arg1361z00_2817); }  else 
{ 
 obj_t BgL_auxz00_6795;
BgL_auxz00_6795 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2830, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2830)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6795,BFALSE,BFALSE);} } } } 
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
return BgL_retz00_1379;} } } } } }  else 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_g1063z00_1388;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_vectorz00_2832;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 145 */
BgL_vectorz00_2832 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6804;
BgL_auxz00_6804 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3443z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6804,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 145 */
 bool_t BgL_test3876z00_6808;
{ /* Llib/weakhash.scm 145 */
 long BgL_tmpz00_6809;
BgL_tmpz00_6809 = 
VECTOR_LENGTH(BgL_vectorz00_2832); 
BgL_test3876z00_6808 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6809); } 
if(BgL_test3876z00_6808)
{ /* Llib/weakhash.scm 145 */
BgL_g1063z00_1388 = 
VECTOR_REF(BgL_vectorz00_2832,BgL_iz00_49); }  else 
{ 
 obj_t BgL_auxz00_6813;
BgL_auxz00_6813 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2832, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2832)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6813,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1390; obj_t BgL_lastzd2bucketzd2_1391;
BgL_bucketz00_1390 = BgL_g1063z00_1388; 
BgL_lastzd2bucketzd2_1391 = BFALSE; 
BgL_zc3z04anonymousza31389ze3z87_1392:
if(
NULLP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 145 */
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_retz00_1394;
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_arg1393z00_1397; obj_t BgL_arg1394z00_1398;
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_pairz00_2834;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 173 */
BgL_pairz00_2834 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_6824;
BgL_auxz00_6824 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6593L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_6824,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_pairz00_2837;
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_aux2686z00_4919;
BgL_aux2686z00_4919 = 
CAR(BgL_pairz00_2834); 
if(
PAIRP(BgL_aux2686z00_4919))
{ /* Llib/weakhash.scm 173 */
BgL_pairz00_2837 = BgL_aux2686z00_4919; }  else 
{ 
 obj_t BgL_auxz00_6831;
BgL_auxz00_6831 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6587L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2686z00_4919); 
FAILURE(BgL_auxz00_6831,BFALSE,BFALSE);} } 
BgL_arg1393z00_1397 = 
CAR(BgL_pairz00_2837); } } 
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_pairz00_2838;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 173 */
BgL_pairz00_2838 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_6838;
BgL_auxz00_6838 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6607L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_6838,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_pairz00_2841;
{ /* Llib/weakhash.scm 173 */
 obj_t BgL_aux2690z00_4923;
BgL_aux2690z00_4923 = 
CAR(BgL_pairz00_2838); 
if(
PAIRP(BgL_aux2690z00_4923))
{ /* Llib/weakhash.scm 173 */
BgL_pairz00_2841 = BgL_aux2690z00_4923; }  else 
{ 
 obj_t BgL_auxz00_6845;
BgL_auxz00_6845 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(6601L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2690z00_4923); 
FAILURE(BgL_auxz00_6845,BFALSE,BFALSE);} } 
BgL_arg1394z00_1398 = 
CDR(BgL_pairz00_2841); } } 
BgL_retz00_1394 = 
((obj_t (*)(obj_t, obj_t, obj_t, obj_t))PROCEDURE_L_ENTRY(BgL_funz00_50))(BgL_funz00_50, BgL_arg1393z00_1397, BgL_arg1394z00_1398, BgL_bucketz00_1390); } 
if(
(BgL_retz00_1394==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1391z00_1395;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2842;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2842 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_6860;
BgL_auxz00_6860 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_6860,BFALSE,BFALSE);} 
BgL_arg1391z00_1395 = 
CDR(BgL_pairz00_2842); } 
{ 
 obj_t BgL_lastzd2bucketzd2_6866; obj_t BgL_bucketz00_6865;
BgL_bucketz00_6865 = BgL_arg1391z00_1395; 
BgL_lastzd2bucketzd2_6866 = BgL_bucketz00_1390; 
BgL_lastzd2bucketzd2_1391 = BgL_lastzd2bucketzd2_6866; 
BgL_bucketz00_1390 = BgL_bucketz00_6865; 
goto BgL_zc3z04anonymousza31389ze3z87_1392;} }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1394==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2843;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2844;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3885z00_6869;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6870;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2222z00_2850;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2694z00_4927;
BgL_aux2694z00_4927 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2694z00_4927))
{ /* Llib/weakhash.scm 134 */
BgL_res2222z00_2850 = BgL_aux2694z00_4927; }  else 
{ 
 obj_t BgL_auxz00_6874;
BgL_auxz00_6874 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2694z00_4927); 
FAILURE(BgL_auxz00_6874,BFALSE,BFALSE);} } 
BgL_tmpz00_6870 = BgL_res2222z00_2850; } 
BgL_test3885z00_6869 = 
(BgL_tmpz00_6870==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3885z00_6869)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6879;
BgL_tmpz00_6879 = 
(int)(0L); 
BgL_arg1359z00_2844 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6879); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2844 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2851;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6883;
if(
INTEGERP(BgL_arg1359z00_2844))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6883 = BgL_arg1359z00_2844
; }  else 
{ 
 obj_t BgL_auxz00_6886;
BgL_auxz00_6886 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2844); 
FAILURE(BgL_auxz00_6886,BFALSE,BFALSE);} 
BgL_za71za7_2851 = 
(long)CINT(BgL_tmpz00_6883); } 
BgL_arg1358z00_2843 = 
(BgL_za71za7_2851-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3888z00_6892;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6893;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2223z00_2855;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2697z00_4930;
BgL_aux2697z00_4930 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2697z00_4930))
{ /* Llib/weakhash.scm 134 */
BgL_res2223z00_2855 = BgL_aux2697z00_4930; }  else 
{ 
 obj_t BgL_auxz00_6897;
BgL_auxz00_6897 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2697z00_4930); 
FAILURE(BgL_auxz00_6897,BFALSE,BFALSE);} } 
BgL_tmpz00_6893 = BgL_res2223z00_2855; } 
BgL_test3888z00_6892 = 
(BgL_tmpz00_6893==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3888z00_6892)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6904; int BgL_tmpz00_6902;
BgL_auxz00_6904 = 
BINT(BgL_arg1358z00_2843); 
BgL_tmpz00_6902 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6902, BgL_auxz00_6904); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1391))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2845;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2856;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2856 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_6912;
BgL_auxz00_6912 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_6912,BFALSE,BFALSE);} 
BgL_arg1360z00_2845 = 
CDR(BgL_pairz00_2856); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2857;
if(
PAIRP(BgL_lastzd2bucketzd2_1391))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2857 = BgL_lastzd2bucketzd2_1391; }  else 
{ 
 obj_t BgL_auxz00_6919;
BgL_auxz00_6919 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1391); 
FAILURE(BgL_auxz00_6919,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2857, BgL_arg1360z00_2845); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2846;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2858;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2858 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_6926;
BgL_auxz00_6926 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_6926,BFALSE,BFALSE);} 
BgL_arg1361z00_2846 = 
CDR(BgL_pairz00_2858); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2859;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2859 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_6933;
BgL_auxz00_6933 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_6933,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3895z00_6937;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_6938;
BgL_tmpz00_6938 = 
VECTOR_LENGTH(BgL_vectorz00_2859); 
BgL_test3895z00_6937 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_6938); } 
if(BgL_test3895z00_6937)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2859,BgL_iz00_49,BgL_arg1361z00_2846); }  else 
{ 
 obj_t BgL_auxz00_6942;
BgL_auxz00_6942 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2859, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2859)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_6942,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_arg1392z00_1396;
{ /* Llib/weakhash.scm 145 */
 obj_t BgL_pairz00_2861;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 145 */
BgL_pairz00_2861 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_6951;
BgL_auxz00_6951 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5685L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_6951,BFALSE,BFALSE);} 
BgL_arg1392z00_1396 = 
CDR(BgL_pairz00_2861); } 
{ 
 obj_t BgL_bucketz00_6956;
BgL_bucketz00_6956 = BgL_arg1392z00_1396; 
BgL_bucketz00_1390 = BgL_bucketz00_6956; 
goto BgL_zc3z04anonymousza31389ze3z87_1392;} } }  else 
{ /* Llib/weakhash.scm 145 */
if(
(BgL_retz00_1394==BGl_removestopz00zz__weakhashz00))
{ /* Llib/weakhash.scm 145 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2862;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2863;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3898z00_6959;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6960;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2224z00_2869;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2709z00_4942;
BgL_aux2709z00_4942 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2709z00_4942))
{ /* Llib/weakhash.scm 134 */
BgL_res2224z00_2869 = BgL_aux2709z00_4942; }  else 
{ 
 obj_t BgL_auxz00_6964;
BgL_auxz00_6964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2709z00_4942); 
FAILURE(BgL_auxz00_6964,BFALSE,BFALSE);} } 
BgL_tmpz00_6960 = BgL_res2224z00_2869; } 
BgL_test3898z00_6959 = 
(BgL_tmpz00_6960==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3898z00_6959)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_6969;
BgL_tmpz00_6969 = 
(int)(0L); 
BgL_arg1359z00_2863 = 
STRUCT_REF(BgL_tablez00_47, BgL_tmpz00_6969); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2863 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2870;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6973;
if(
INTEGERP(BgL_arg1359z00_2863))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_6973 = BgL_arg1359z00_2863
; }  else 
{ 
 obj_t BgL_auxz00_6976;
BgL_auxz00_6976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2863); 
FAILURE(BgL_auxz00_6976,BFALSE,BFALSE);} 
BgL_za71za7_2870 = 
(long)CINT(BgL_tmpz00_6973); } 
BgL_arg1358z00_2862 = 
(BgL_za71za7_2870-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3901z00_6982;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_6983;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2225z00_2874;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2712z00_4945;
BgL_aux2712z00_4945 = 
STRUCT_KEY(BgL_tablez00_47); 
if(
SYMBOLP(BgL_aux2712z00_4945))
{ /* Llib/weakhash.scm 134 */
BgL_res2225z00_2874 = BgL_aux2712z00_4945; }  else 
{ 
 obj_t BgL_auxz00_6987;
BgL_auxz00_6987 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2712z00_4945); 
FAILURE(BgL_auxz00_6987,BFALSE,BFALSE);} } 
BgL_tmpz00_6983 = BgL_res2225z00_2874; } 
BgL_test3901z00_6982 = 
(BgL_tmpz00_6983==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3901z00_6982)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_6994; int BgL_tmpz00_6992;
BgL_auxz00_6994 = 
BINT(BgL_arg1358z00_2862); 
BgL_tmpz00_6992 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_47, BgL_tmpz00_6992, BgL_auxz00_6994); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_47); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1391))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2864;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2875;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2875 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_7002;
BgL_auxz00_7002 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_7002,BFALSE,BFALSE);} 
BgL_arg1360z00_2864 = 
CDR(BgL_pairz00_2875); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2876;
if(
PAIRP(BgL_lastzd2bucketzd2_1391))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2876 = BgL_lastzd2bucketzd2_1391; }  else 
{ 
 obj_t BgL_auxz00_7009;
BgL_auxz00_7009 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1391); 
FAILURE(BgL_auxz00_7009,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2876, BgL_arg1360z00_2864); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2865;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2877;
if(
PAIRP(BgL_bucketz00_1390))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2877 = BgL_bucketz00_1390; }  else 
{ 
 obj_t BgL_auxz00_7016;
BgL_auxz00_7016 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1390); 
FAILURE(BgL_auxz00_7016,BFALSE,BFALSE);} 
BgL_arg1361z00_2865 = 
CDR(BgL_pairz00_2877); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2878;
if(
VECTORP(BgL_bucketsz00_48))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2878 = BgL_bucketsz00_48; }  else 
{ 
 obj_t BgL_auxz00_7023;
BgL_auxz00_7023 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_48); 
FAILURE(BgL_auxz00_7023,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3908z00_7027;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_7028;
BgL_tmpz00_7028 = 
VECTOR_LENGTH(BgL_vectorz00_2878); 
BgL_test3908z00_7027 = 
BOUND_CHECK(BgL_iz00_49, BgL_tmpz00_7028); } 
if(BgL_test3908z00_7027)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2878,BgL_iz00_49,BgL_arg1361z00_2865); }  else 
{ 
 obj_t BgL_auxz00_7032;
BgL_auxz00_7032 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2878, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2878)), 
(int)(BgL_iz00_49)); 
FAILURE(BgL_auxz00_7032,BFALSE,BFALSE);} } } } 
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 145 */
return BgL_retz00_1394;} } } } } } } } } } 

}



/* keys-traverse-hash */
obj_t BGl_keyszd2traversezd2hashz00zz__weakhashz00(obj_t BgL_tablez00_51, obj_t BgL_funz00_52)
{
{ /* Llib/weakhash.scm 187 */
BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(BgL_tablez00_51, BGl_proc3451z00zz__weakhashz00); 
{ /* Llib/weakhash.scm 191 */
 obj_t BgL_bucketsz00_1408;
{ /* Llib/weakhash.scm 191 */
 bool_t BgL_test3909z00_7040;
{ /* Llib/weakhash.scm 191 */
 obj_t BgL_tmpz00_7041;
{ /* Llib/weakhash.scm 191 */
 obj_t BgL_res2226z00_2883;
{ /* Llib/weakhash.scm 191 */
 obj_t BgL_aux2722z00_4955;
BgL_aux2722z00_4955 = 
STRUCT_KEY(BgL_tablez00_51); 
if(
SYMBOLP(BgL_aux2722z00_4955))
{ /* Llib/weakhash.scm 191 */
BgL_res2226z00_2883 = BgL_aux2722z00_4955; }  else 
{ 
 obj_t BgL_auxz00_7045;
BgL_auxz00_7045 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7326L), BGl_string3452z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2722z00_4955); 
FAILURE(BgL_auxz00_7045,BFALSE,BFALSE);} } 
BgL_tmpz00_7041 = BgL_res2226z00_2883; } 
BgL_test3909z00_7040 = 
(BgL_tmpz00_7041==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3909z00_7040)
{ /* Llib/weakhash.scm 191 */
 int BgL_tmpz00_7050;
BgL_tmpz00_7050 = 
(int)(2L); 
BgL_bucketsz00_1408 = 
STRUCT_REF(BgL_tablez00_51, BgL_tmpz00_7050); }  else 
{ /* Llib/weakhash.scm 191 */
BgL_bucketsz00_1408 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_51); } } 
{ /* Llib/weakhash.scm 191 */
 long BgL_bucketszd2lenzd2_1409;
{ /* Llib/weakhash.scm 192 */
 obj_t BgL_vectorz00_2884;
if(
VECTORP(BgL_bucketsz00_1408))
{ /* Llib/weakhash.scm 192 */
BgL_vectorz00_2884 = BgL_bucketsz00_1408; }  else 
{ 
 obj_t BgL_auxz00_7056;
BgL_auxz00_7056 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7385L), BGl_string3452z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1408); 
FAILURE(BgL_auxz00_7056,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1409 = 
VECTOR_LENGTH(BgL_vectorz00_2884); } 
{ /* Llib/weakhash.scm 192 */

{ 
 long BgL_iz00_1411;
BgL_iz00_1411 = 0L; 
if(
(BgL_iz00_1411==BgL_bucketszd2lenzd2_1409))
{ /* Llib/weakhash.scm 194 */
return BFALSE;}  else 
{ /* Llib/weakhash.scm 195 */
 obj_t BgL_bucketz00_1414;
{ /* Llib/weakhash.scm 195 */
 obj_t BgL_vectorz00_2887;
if(
VECTORP(BgL_bucketsz00_1408))
{ /* Llib/weakhash.scm 195 */
BgL_vectorz00_2887 = BgL_bucketsz00_1408; }  else 
{ 
 obj_t BgL_auxz00_7065;
BgL_auxz00_7065 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7481L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1408); 
FAILURE(BgL_auxz00_7065,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 195 */
 bool_t BgL_test3914z00_7069;
{ /* Llib/weakhash.scm 195 */
 long BgL_tmpz00_7070;
BgL_tmpz00_7070 = 
VECTOR_LENGTH(BgL_vectorz00_2887); 
BgL_test3914z00_7069 = 
BOUND_CHECK(BgL_iz00_1411, BgL_tmpz00_7070); } 
if(BgL_test3914z00_7069)
{ /* Llib/weakhash.scm 195 */
BgL_bucketz00_1414 = 
VECTOR_REF(BgL_vectorz00_2887,BgL_iz00_1411); }  else 
{ 
 obj_t BgL_auxz00_7074;
BgL_auxz00_7074 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7469L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2887, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2887)), 
(int)(BgL_iz00_1411)); 
FAILURE(BgL_auxz00_7074,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_l1113z00_1416;
BgL_l1113z00_1416 = BgL_bucketz00_1414; 
BgL_zc3z04anonymousza31403ze3z87_1417:
if(
PAIRP(BgL_l1113z00_1416))
{ /* Llib/weakhash.scm 196 */
{ /* Llib/weakhash.scm 197 */
 obj_t BgL_pz00_1419;
BgL_pz00_1419 = 
CAR(BgL_l1113z00_1416); 
{ /* Llib/weakhash.scm 197 */
 bool_t BgL_test3916z00_7084;
{ /* Llib/weakhash.scm 197 */
 obj_t BgL_arg1410z00_1424;
{ /* Llib/weakhash.scm 197 */
 obj_t BgL_objz00_2890;
if(
BGL_WEAKPTRP(BgL_pz00_1419))
{ /* Llib/weakhash.scm 197 */
BgL_objz00_2890 = BgL_pz00_1419; }  else 
{ 
 obj_t BgL_auxz00_7087;
BgL_auxz00_7087 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7558L), BGl_string3454z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_pz00_1419); 
FAILURE(BgL_auxz00_7087,BFALSE,BFALSE);} 
BgL_arg1410z00_1424 = 
bgl_weakptr_data(BgL_objz00_2890); } 
BgL_test3916z00_7084 = 
(BgL_arg1410z00_1424==BUNSPEC); } 
if(BgL_test3916z00_7084)
{ /* Llib/weakhash.scm 197 */BFALSE; }  else 
{ /* Llib/weakhash.scm 198 */
 obj_t BgL_arg1407z00_1422; obj_t BgL_arg1408z00_1423;
{ /* Llib/weakhash.scm 198 */
 obj_t BgL_objz00_2891;
if(
BGL_WEAKPTRP(BgL_pz00_1419))
{ /* Llib/weakhash.scm 198 */
BgL_objz00_2891 = BgL_pz00_1419; }  else 
{ 
 obj_t BgL_auxz00_7095;
BgL_auxz00_7095 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7604L), BGl_string3454z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_pz00_1419); 
FAILURE(BgL_auxz00_7095,BFALSE,BFALSE);} 
BgL_arg1407z00_1422 = 
bgl_weakptr_data(BgL_objz00_2891); } 
{ /* Llib/weakhash.scm 198 */
 obj_t BgL_objz00_2892;
if(
BGL_WEAKPTRP(BgL_pz00_1419))
{ /* Llib/weakhash.scm 198 */
BgL_objz00_2892 = BgL_pz00_1419; }  else 
{ 
 obj_t BgL_auxz00_7102;
BgL_auxz00_7102 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7620L), BGl_string3454z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_pz00_1419); 
FAILURE(BgL_auxz00_7102,BFALSE,BFALSE);} 
BgL_arg1408z00_1423 = 
bgl_weakptr_ref(BgL_objz00_2892); } 
{ /* Llib/weakhash.scm 198 */
 obj_t BgL_funz00_4969;
if(
PROCEDUREP(BgL_funz00_52))
{ /* Llib/weakhash.scm 198 */
BgL_funz00_4969 = BgL_funz00_52; }  else 
{ 
 obj_t BgL_auxz00_7109;
BgL_auxz00_7109 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7585L), BGl_string3454z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_52); 
FAILURE(BgL_auxz00_7109,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4969, 2))
{ /* Llib/weakhash.scm 198 */
(VA_PROCEDUREP( BgL_funz00_4969 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4969))(BgL_funz00_52, BgL_arg1407z00_1422, BgL_arg1408z00_1423, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4969))(BgL_funz00_52, BgL_arg1407z00_1422, BgL_arg1408z00_1423) ); }  else 
{ /* Llib/weakhash.scm 198 */
FAILURE(BGl_string3456z00zz__weakhashz00,BGl_list3457z00zz__weakhashz00,BgL_funz00_4969);} } } } } 
{ 
 obj_t BgL_l1113z00_7121;
BgL_l1113z00_7121 = 
CDR(BgL_l1113z00_1416); 
BgL_l1113z00_1416 = BgL_l1113z00_7121; 
goto BgL_zc3z04anonymousza31403ze3z87_1417;} }  else 
{ /* Llib/weakhash.scm 196 */
if(
NULLP(BgL_l1113z00_1416))
{ /* Llib/weakhash.scm 196 */
return BTRUE;}  else 
{ /* Llib/weakhash.scm 196 */
return 
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3464z00zz__weakhashz00, BGl_string3465z00zz__weakhashz00, BgL_l1113z00_1416, BGl_string3442z00zz__weakhashz00, 
BINT(7502L));} } } } } } } } } 

}



/* &<@anonymous:1400> */
obj_t BGl_z62zc3z04anonymousza31400ze3ze5zz__weakhashz00(obj_t BgL_envz00_4417, obj_t BgL_kz00_4418, obj_t BgL_vz00_4419)
{
{ /* Llib/weakhash.scm 189 */
return 
BBOOL(((bool_t)1));} 

}



/* old-traverse-hash */
bool_t BGl_oldzd2traversezd2hashz00zz__weakhashz00(obj_t BgL_tablez00_53, obj_t BgL_funz00_54)
{
{ /* Llib/weakhash.scm 204 */
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3923z00_7128;
{ /* Llib/weakhash.scm 214 */
 obj_t BgL_arg1460z00_1522;
{ /* Llib/weakhash.scm 214 */
 bool_t BgL_test3924z00_7129;
{ /* Llib/weakhash.scm 214 */
 obj_t BgL_tmpz00_7130;
{ /* Llib/weakhash.scm 214 */
 obj_t BgL_res2227z00_2897;
{ /* Llib/weakhash.scm 214 */
 obj_t BgL_aux2737z00_4971;
BgL_aux2737z00_4971 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2737z00_4971))
{ /* Llib/weakhash.scm 214 */
BgL_res2227z00_2897 = BgL_aux2737z00_4971; }  else 
{ 
 obj_t BgL_auxz00_7134;
BgL_auxz00_7134 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8215L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2737z00_4971); 
FAILURE(BgL_auxz00_7134,BFALSE,BFALSE);} } 
BgL_tmpz00_7130 = BgL_res2227z00_2897; } 
BgL_test3924z00_7129 = 
(BgL_tmpz00_7130==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3924z00_7129)
{ /* Llib/weakhash.scm 214 */
 int BgL_tmpz00_7139;
BgL_tmpz00_7139 = 
(int)(5L); 
BgL_arg1460z00_1522 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7139); }  else 
{ /* Llib/weakhash.scm 214 */
BgL_arg1460z00_1522 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 214 */
 long BgL_n2z00_2898;
{ /* Llib/weakhash.scm 214 */
 obj_t BgL_tmpz00_7143;
if(
INTEGERP(BgL_arg1460z00_1522))
{ /* Llib/weakhash.scm 214 */
BgL_tmpz00_7143 = BgL_arg1460z00_1522
; }  else 
{ 
 obj_t BgL_auxz00_7146;
BgL_auxz00_7146 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8237L), BGl_string3466z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1460z00_1522); 
FAILURE(BgL_auxz00_7146,BFALSE,BFALSE);} 
BgL_n2z00_2898 = 
(long)CINT(BgL_tmpz00_7143); } 
BgL_test3923z00_7128 = 
(1L==BgL_n2z00_2898); } } 
if(BgL_test3923z00_7128)
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_bucketsz00_1431;
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3927z00_7152;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_tmpz00_7153;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_res2228z00_2902;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_aux2740z00_4974;
BgL_aux2740z00_4974 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2740z00_4974))
{ /* Llib/weakhash.scm 205 */
BgL_res2228z00_2902 = BgL_aux2740z00_4974; }  else 
{ 
 obj_t BgL_auxz00_7157;
BgL_auxz00_7157 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2740z00_4974); 
FAILURE(BgL_auxz00_7157,BFALSE,BFALSE);} } 
BgL_tmpz00_7153 = BgL_res2228z00_2902; } 
BgL_test3927z00_7152 = 
(BgL_tmpz00_7153==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3927z00_7152)
{ /* Llib/weakhash.scm 205 */
 int BgL_tmpz00_7162;
BgL_tmpz00_7162 = 
(int)(2L); 
BgL_bucketsz00_1431 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7162); }  else 
{ /* Llib/weakhash.scm 205 */
BgL_bucketsz00_1431 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 205 */
 long BgL_bucketszd2lenzd2_1432;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_2903;
if(
VECTORP(BgL_bucketsz00_1431))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_2903 = BgL_bucketsz00_1431; }  else 
{ 
 obj_t BgL_auxz00_7168;
BgL_auxz00_7168 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1431); 
FAILURE(BgL_auxz00_7168,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1432 = 
VECTOR_LENGTH(BgL_vectorz00_2903); } 
{ /* Llib/weakhash.scm 205 */

{ 
 long BgL_iz00_1434;
BgL_iz00_1434 = 0L; 
BgL_zc3z04anonymousza31415ze3z87_1435:
if(
(BgL_iz00_1434==BgL_bucketszd2lenzd2_1432))
{ /* Llib/weakhash.scm 205 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_g1064z00_1437;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_2906;
if(
VECTORP(BgL_bucketsz00_1431))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_2906 = BgL_bucketsz00_1431; }  else 
{ 
 obj_t BgL_auxz00_7177;
BgL_auxz00_7177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1431); 
FAILURE(BgL_auxz00_7177,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3932z00_7181;
{ /* Llib/weakhash.scm 205 */
 long BgL_tmpz00_7182;
BgL_tmpz00_7182 = 
VECTOR_LENGTH(BgL_vectorz00_2906); 
BgL_test3932z00_7181 = 
BOUND_CHECK(BgL_iz00_1434, BgL_tmpz00_7182); } 
if(BgL_test3932z00_7181)
{ /* Llib/weakhash.scm 205 */
BgL_g1064z00_1437 = 
VECTOR_REF(BgL_vectorz00_2906,BgL_iz00_1434); }  else 
{ 
 obj_t BgL_auxz00_7186;
BgL_auxz00_7186 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2906, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2906)), 
(int)(BgL_iz00_1434)); 
FAILURE(BgL_auxz00_7186,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1439; obj_t BgL_lastzd2bucketzd2_1440;
BgL_bucketz00_1439 = BgL_g1064z00_1437; 
BgL_lastzd2bucketzd2_1440 = BFALSE; 
BgL_zc3z04anonymousza31417ze3z87_1441:
if(
NULLP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 205 */((bool_t)0); }  else 
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_retz00_1443;
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_keyz00_1446;
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_arg1422z00_1448;
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_pairz00_2908;
if(
PAIRP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 216 */
BgL_pairz00_2908 = BgL_bucketz00_1439; }  else 
{ 
 obj_t BgL_auxz00_7197;
BgL_auxz00_7197 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8301L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1439); 
FAILURE(BgL_auxz00_7197,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_pairz00_2911;
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_aux2748z00_4982;
BgL_aux2748z00_4982 = 
CAR(BgL_pairz00_2908); 
if(
PAIRP(BgL_aux2748z00_4982))
{ /* Llib/weakhash.scm 216 */
BgL_pairz00_2911 = BgL_aux2748z00_4982; }  else 
{ 
 obj_t BgL_auxz00_7204;
BgL_auxz00_7204 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8295L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2748z00_4982); 
FAILURE(BgL_auxz00_7204,BFALSE,BFALSE);} } 
BgL_arg1422z00_1448 = 
CAR(BgL_pairz00_2911); } } 
{ /* Llib/weakhash.scm 216 */
 obj_t BgL_objz00_2912;
if(
BGL_WEAKPTRP(BgL_arg1422z00_1448))
{ /* Llib/weakhash.scm 216 */
BgL_objz00_2912 = BgL_arg1422z00_1448; }  else 
{ 
 obj_t BgL_auxz00_7211;
BgL_auxz00_7211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8307L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1422z00_1448); 
FAILURE(BgL_auxz00_7211,BFALSE,BFALSE);} 
BgL_keyz00_1446 = 
bgl_weakptr_data(BgL_objz00_2912); } } 
if(
(BgL_keyz00_1446==BUNSPEC))
{ /* Llib/weakhash.scm 217 */
BgL_retz00_1443 = BGl_removez00zz__weakhashz00; }  else 
{ /* Llib/weakhash.scm 219 */
 obj_t BgL_arg1421z00_1447;
{ /* Llib/weakhash.scm 219 */
 obj_t BgL_pairz00_2913;
if(
PAIRP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 219 */
BgL_pairz00_2913 = BgL_bucketz00_1439; }  else 
{ 
 obj_t BgL_auxz00_7220;
BgL_auxz00_7220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8382L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1439); 
FAILURE(BgL_auxz00_7220,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 219 */
 obj_t BgL_pairz00_2916;
{ /* Llib/weakhash.scm 219 */
 obj_t BgL_aux2754z00_4988;
BgL_aux2754z00_4988 = 
CAR(BgL_pairz00_2913); 
if(
PAIRP(BgL_aux2754z00_4988))
{ /* Llib/weakhash.scm 219 */
BgL_pairz00_2916 = BgL_aux2754z00_4988; }  else 
{ 
 obj_t BgL_auxz00_7227;
BgL_auxz00_7227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8376L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2754z00_4988); 
FAILURE(BgL_auxz00_7227,BFALSE,BFALSE);} } 
BgL_arg1421z00_1447 = 
CDR(BgL_pairz00_2916); } } 
{ /* Llib/weakhash.scm 219 */
 obj_t BgL_funz00_4992;
if(
PROCEDUREP(BgL_funz00_54))
{ /* Llib/weakhash.scm 219 */
BgL_funz00_4992 = BgL_funz00_54; }  else 
{ 
 obj_t BgL_auxz00_7234;
BgL_auxz00_7234 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8367L), BGl_string3446z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_54); 
FAILURE(BgL_auxz00_7234,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4992, 2))
{ /* Llib/weakhash.scm 219 */
BgL_retz00_1443 = 
(VA_PROCEDUREP( BgL_funz00_4992 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_4992))(BgL_funz00_54, BgL_keyz00_1446, BgL_arg1421z00_1447, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_4992))(BgL_funz00_54, BgL_keyz00_1446, BgL_arg1421z00_1447) ); }  else 
{ /* Llib/weakhash.scm 219 */
FAILURE(BGl_string3467z00zz__weakhashz00,BGl_list3468z00zz__weakhashz00,BgL_funz00_4992);} } } } 
if(
(BgL_retz00_1443==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2917;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2918;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3943z00_7248;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7249;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2229z00_2924;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2759z00_4994;
BgL_aux2759z00_4994 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2759z00_4994))
{ /* Llib/weakhash.scm 134 */
BgL_res2229z00_2924 = BgL_aux2759z00_4994; }  else 
{ 
 obj_t BgL_auxz00_7253;
BgL_auxz00_7253 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2759z00_4994); 
FAILURE(BgL_auxz00_7253,BFALSE,BFALSE);} } 
BgL_tmpz00_7249 = BgL_res2229z00_2924; } 
BgL_test3943z00_7248 = 
(BgL_tmpz00_7249==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3943z00_7248)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_7258;
BgL_tmpz00_7258 = 
(int)(0L); 
BgL_arg1359z00_2918 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7258); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2918 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2925;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7262;
if(
INTEGERP(BgL_arg1359z00_2918))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_7262 = BgL_arg1359z00_2918
; }  else 
{ 
 obj_t BgL_auxz00_7265;
BgL_auxz00_7265 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2918); 
FAILURE(BgL_auxz00_7265,BFALSE,BFALSE);} 
BgL_za71za7_2925 = 
(long)CINT(BgL_tmpz00_7262); } 
BgL_arg1358z00_2917 = 
(BgL_za71za7_2925-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3946z00_7271;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7272;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2230z00_2929;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2762z00_4997;
BgL_aux2762z00_4997 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2762z00_4997))
{ /* Llib/weakhash.scm 134 */
BgL_res2230z00_2929 = BgL_aux2762z00_4997; }  else 
{ 
 obj_t BgL_auxz00_7276;
BgL_auxz00_7276 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2762z00_4997); 
FAILURE(BgL_auxz00_7276,BFALSE,BFALSE);} } 
BgL_tmpz00_7272 = BgL_res2230z00_2929; } 
BgL_test3946z00_7271 = 
(BgL_tmpz00_7272==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3946z00_7271)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_7283; int BgL_tmpz00_7281;
BgL_auxz00_7283 = 
BINT(BgL_arg1358z00_2917); 
BgL_tmpz00_7281 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_53, BgL_tmpz00_7281, BgL_auxz00_7283); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1440))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2919;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2930;
if(
PAIRP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2930 = BgL_bucketz00_1439; }  else 
{ 
 obj_t BgL_auxz00_7291;
BgL_auxz00_7291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1439); 
FAILURE(BgL_auxz00_7291,BFALSE,BFALSE);} 
BgL_arg1360z00_2919 = 
CDR(BgL_pairz00_2930); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2931;
if(
PAIRP(BgL_lastzd2bucketzd2_1440))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2931 = BgL_lastzd2bucketzd2_1440; }  else 
{ 
 obj_t BgL_auxz00_7298;
BgL_auxz00_7298 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1440); 
FAILURE(BgL_auxz00_7298,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2931, BgL_arg1360z00_2919); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2920;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2932;
if(
PAIRP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2932 = BgL_bucketz00_1439; }  else 
{ 
 obj_t BgL_auxz00_7305;
BgL_auxz00_7305 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1439); 
FAILURE(BgL_auxz00_7305,BFALSE,BFALSE);} 
BgL_arg1361z00_2920 = 
CDR(BgL_pairz00_2932); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2933;
if(
VECTORP(BgL_bucketsz00_1431))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2933 = BgL_bucketsz00_1431; }  else 
{ 
 obj_t BgL_auxz00_7312;
BgL_auxz00_7312 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1431); 
FAILURE(BgL_auxz00_7312,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3953z00_7316;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_7317;
BgL_tmpz00_7317 = 
VECTOR_LENGTH(BgL_vectorz00_2933); 
BgL_test3953z00_7316 = 
BOUND_CHECK(BgL_iz00_1434, BgL_tmpz00_7317); } 
if(BgL_test3953z00_7316)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2933,BgL_iz00_1434,BgL_arg1361z00_2920); }  else 
{ 
 obj_t BgL_auxz00_7321;
BgL_auxz00_7321 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2933, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2933)), 
(int)(BgL_iz00_1434)); 
FAILURE(BgL_auxz00_7321,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1419z00_1444;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_2935;
if(
PAIRP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_2935 = BgL_bucketz00_1439; }  else 
{ 
 obj_t BgL_auxz00_7330;
BgL_auxz00_7330 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1439); 
FAILURE(BgL_auxz00_7330,BFALSE,BFALSE);} 
BgL_arg1419z00_1444 = 
CDR(BgL_pairz00_2935); } 
{ 
 obj_t BgL_bucketz00_7335;
BgL_bucketz00_7335 = BgL_arg1419z00_1444; 
BgL_bucketz00_1439 = BgL_bucketz00_7335; 
goto BgL_zc3z04anonymousza31417ze3z87_1441;} } }  else 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1420z00_1445;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_2936;
if(
PAIRP(BgL_bucketz00_1439))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_2936 = BgL_bucketz00_1439; }  else 
{ 
 obj_t BgL_auxz00_7338;
BgL_auxz00_7338 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1439); 
FAILURE(BgL_auxz00_7338,BFALSE,BFALSE);} 
BgL_arg1420z00_1445 = 
CDR(BgL_pairz00_2936); } 
{ 
 obj_t BgL_lastzd2bucketzd2_7344; obj_t BgL_bucketz00_7343;
BgL_bucketz00_7343 = BgL_arg1420z00_1445; 
BgL_lastzd2bucketzd2_7344 = BgL_bucketz00_1439; 
BgL_lastzd2bucketzd2_1440 = BgL_lastzd2bucketzd2_7344; 
BgL_bucketz00_1439 = BgL_bucketz00_7343; 
goto BgL_zc3z04anonymousza31417ze3z87_1441;} } } } } 
{ 
 long BgL_iz00_7345;
BgL_iz00_7345 = 
(BgL_iz00_1434+1L); 
BgL_iz00_1434 = BgL_iz00_7345; 
goto BgL_zc3z04anonymousza31415ze3z87_1435;} } } } } }  else 
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3956z00_7347;
{ /* Llib/weakhash.scm 220 */
 obj_t BgL_arg1459z00_1521;
{ /* Llib/weakhash.scm 220 */
 bool_t BgL_test3957z00_7348;
{ /* Llib/weakhash.scm 220 */
 obj_t BgL_tmpz00_7349;
{ /* Llib/weakhash.scm 220 */
 obj_t BgL_res2231z00_2941;
{ /* Llib/weakhash.scm 220 */
 obj_t BgL_aux2776z00_5011;
BgL_aux2776z00_5011 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2776z00_5011))
{ /* Llib/weakhash.scm 220 */
BgL_res2231z00_2941 = BgL_aux2776z00_5011; }  else 
{ 
 obj_t BgL_auxz00_7353;
BgL_auxz00_7353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8417L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2776z00_5011); 
FAILURE(BgL_auxz00_7353,BFALSE,BFALSE);} } 
BgL_tmpz00_7349 = BgL_res2231z00_2941; } 
BgL_test3957z00_7348 = 
(BgL_tmpz00_7349==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3957z00_7348)
{ /* Llib/weakhash.scm 220 */
 int BgL_tmpz00_7358;
BgL_tmpz00_7358 = 
(int)(5L); 
BgL_arg1459z00_1521 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7358); }  else 
{ /* Llib/weakhash.scm 220 */
BgL_arg1459z00_1521 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 220 */
 long BgL_n2z00_2942;
{ /* Llib/weakhash.scm 220 */
 obj_t BgL_tmpz00_7362;
if(
INTEGERP(BgL_arg1459z00_1521))
{ /* Llib/weakhash.scm 220 */
BgL_tmpz00_7362 = BgL_arg1459z00_1521
; }  else 
{ 
 obj_t BgL_auxz00_7365;
BgL_auxz00_7365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8439L), BGl_string3466z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1459z00_1521); 
FAILURE(BgL_auxz00_7365,BFALSE,BFALSE);} 
BgL_n2z00_2942 = 
(long)CINT(BgL_tmpz00_7362); } 
BgL_test3956z00_7347 = 
(2L==BgL_n2z00_2942); } } 
if(BgL_test3956z00_7347)
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_bucketsz00_1454;
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3960z00_7371;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_tmpz00_7372;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_res2232z00_2946;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_aux2779z00_5014;
BgL_aux2779z00_5014 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2779z00_5014))
{ /* Llib/weakhash.scm 205 */
BgL_res2232z00_2946 = BgL_aux2779z00_5014; }  else 
{ 
 obj_t BgL_auxz00_7376;
BgL_auxz00_7376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2779z00_5014); 
FAILURE(BgL_auxz00_7376,BFALSE,BFALSE);} } 
BgL_tmpz00_7372 = BgL_res2232z00_2946; } 
BgL_test3960z00_7371 = 
(BgL_tmpz00_7372==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3960z00_7371)
{ /* Llib/weakhash.scm 205 */
 int BgL_tmpz00_7381;
BgL_tmpz00_7381 = 
(int)(2L); 
BgL_bucketsz00_1454 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7381); }  else 
{ /* Llib/weakhash.scm 205 */
BgL_bucketsz00_1454 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 205 */
 long BgL_bucketszd2lenzd2_1455;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_2947;
if(
VECTORP(BgL_bucketsz00_1454))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_2947 = BgL_bucketsz00_1454; }  else 
{ 
 obj_t BgL_auxz00_7387;
BgL_auxz00_7387 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1454); 
FAILURE(BgL_auxz00_7387,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1455 = 
VECTOR_LENGTH(BgL_vectorz00_2947); } 
{ /* Llib/weakhash.scm 205 */

{ 
 long BgL_iz00_1457;
BgL_iz00_1457 = 0L; 
BgL_zc3z04anonymousza31426ze3z87_1458:
if(
(BgL_iz00_1457==BgL_bucketszd2lenzd2_1455))
{ /* Llib/weakhash.scm 205 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_g1065z00_1460;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_2950;
if(
VECTORP(BgL_bucketsz00_1454))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_2950 = BgL_bucketsz00_1454; }  else 
{ 
 obj_t BgL_auxz00_7396;
BgL_auxz00_7396 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1454); 
FAILURE(BgL_auxz00_7396,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3965z00_7400;
{ /* Llib/weakhash.scm 205 */
 long BgL_tmpz00_7401;
BgL_tmpz00_7401 = 
VECTOR_LENGTH(BgL_vectorz00_2950); 
BgL_test3965z00_7400 = 
BOUND_CHECK(BgL_iz00_1457, BgL_tmpz00_7401); } 
if(BgL_test3965z00_7400)
{ /* Llib/weakhash.scm 205 */
BgL_g1065z00_1460 = 
VECTOR_REF(BgL_vectorz00_2950,BgL_iz00_1457); }  else 
{ 
 obj_t BgL_auxz00_7405;
BgL_auxz00_7405 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2950, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2950)), 
(int)(BgL_iz00_1457)); 
FAILURE(BgL_auxz00_7405,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1462; obj_t BgL_lastzd2bucketzd2_1463;
BgL_bucketz00_1462 = BgL_g1065z00_1460; 
BgL_lastzd2bucketzd2_1463 = BFALSE; 
BgL_zc3z04anonymousza31428ze3z87_1464:
if(
NULLP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 205 */((bool_t)0); }  else 
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_retz00_1466;
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_dataz00_1469;
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_arg1435z00_1471;
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_pairz00_2952;
if(
PAIRP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 222 */
BgL_pairz00_2952 = BgL_bucketz00_1462; }  else 
{ 
 obj_t BgL_auxz00_7416;
BgL_auxz00_7416 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8504L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1462); 
FAILURE(BgL_auxz00_7416,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_pairz00_2955;
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_aux2787z00_5022;
BgL_aux2787z00_5022 = 
CAR(BgL_pairz00_2952); 
if(
PAIRP(BgL_aux2787z00_5022))
{ /* Llib/weakhash.scm 222 */
BgL_pairz00_2955 = BgL_aux2787z00_5022; }  else 
{ 
 obj_t BgL_auxz00_7423;
BgL_auxz00_7423 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8498L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2787z00_5022); 
FAILURE(BgL_auxz00_7423,BFALSE,BFALSE);} } 
BgL_arg1435z00_1471 = 
CDR(BgL_pairz00_2955); } } 
{ /* Llib/weakhash.scm 222 */
 obj_t BgL_objz00_2956;
if(
BGL_WEAKPTRP(BgL_arg1435z00_1471))
{ /* Llib/weakhash.scm 222 */
BgL_objz00_2956 = BgL_arg1435z00_1471; }  else 
{ 
 obj_t BgL_auxz00_7430;
BgL_auxz00_7430 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8510L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1435z00_1471); 
FAILURE(BgL_auxz00_7430,BFALSE,BFALSE);} 
BgL_dataz00_1469 = 
bgl_weakptr_data(BgL_objz00_2956); } } 
if(
(BgL_dataz00_1469==BUNSPEC))
{ /* Llib/weakhash.scm 223 */
BgL_retz00_1466 = BGl_removez00zz__weakhashz00; }  else 
{ /* Llib/weakhash.scm 225 */
 obj_t BgL_arg1434z00_1470;
{ /* Llib/weakhash.scm 225 */
 obj_t BgL_pairz00_2957;
if(
PAIRP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 225 */
BgL_pairz00_2957 = BgL_bucketz00_1462; }  else 
{ 
 obj_t BgL_auxz00_7439;
BgL_auxz00_7439 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8582L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1462); 
FAILURE(BgL_auxz00_7439,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 225 */
 obj_t BgL_pairz00_2960;
{ /* Llib/weakhash.scm 225 */
 obj_t BgL_aux2793z00_5028;
BgL_aux2793z00_5028 = 
CAR(BgL_pairz00_2957); 
if(
PAIRP(BgL_aux2793z00_5028))
{ /* Llib/weakhash.scm 225 */
BgL_pairz00_2960 = BgL_aux2793z00_5028; }  else 
{ 
 obj_t BgL_auxz00_7446;
BgL_auxz00_7446 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8576L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2793z00_5028); 
FAILURE(BgL_auxz00_7446,BFALSE,BFALSE);} } 
BgL_arg1434z00_1470 = 
CAR(BgL_pairz00_2960); } } 
{ /* Llib/weakhash.scm 225 */
 obj_t BgL_funz00_5032;
if(
PROCEDUREP(BgL_funz00_54))
{ /* Llib/weakhash.scm 225 */
BgL_funz00_5032 = BgL_funz00_54; }  else 
{ 
 obj_t BgL_auxz00_7453;
BgL_auxz00_7453 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8571L), BGl_string3446z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_54); 
FAILURE(BgL_auxz00_7453,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5032, 2))
{ /* Llib/weakhash.scm 225 */
BgL_retz00_1466 = 
(VA_PROCEDUREP( BgL_funz00_5032 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5032))(BgL_funz00_54, BgL_arg1434z00_1470, BgL_dataz00_1469, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5032))(BgL_funz00_54, BgL_arg1434z00_1470, BgL_dataz00_1469) ); }  else 
{ /* Llib/weakhash.scm 225 */
FAILURE(BGl_string3467z00zz__weakhashz00,BGl_list3471z00zz__weakhashz00,BgL_funz00_5032);} } } } 
if(
(BgL_retz00_1466==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_2961;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_2962;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3976z00_7467;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7468;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2233z00_2968;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2798z00_5034;
BgL_aux2798z00_5034 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2798z00_5034))
{ /* Llib/weakhash.scm 134 */
BgL_res2233z00_2968 = BgL_aux2798z00_5034; }  else 
{ 
 obj_t BgL_auxz00_7472;
BgL_auxz00_7472 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2798z00_5034); 
FAILURE(BgL_auxz00_7472,BFALSE,BFALSE);} } 
BgL_tmpz00_7468 = BgL_res2233z00_2968; } 
BgL_test3976z00_7467 = 
(BgL_tmpz00_7468==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3976z00_7467)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_7477;
BgL_tmpz00_7477 = 
(int)(0L); 
BgL_arg1359z00_2962 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7477); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_2962 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_2969;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7481;
if(
INTEGERP(BgL_arg1359z00_2962))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_7481 = BgL_arg1359z00_2962
; }  else 
{ 
 obj_t BgL_auxz00_7484;
BgL_auxz00_7484 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_2962); 
FAILURE(BgL_auxz00_7484,BFALSE,BFALSE);} 
BgL_za71za7_2969 = 
(long)CINT(BgL_tmpz00_7481); } 
BgL_arg1358z00_2961 = 
(BgL_za71za7_2969-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test3979z00_7490;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7491;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2234z00_2973;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2801z00_5037;
BgL_aux2801z00_5037 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2801z00_5037))
{ /* Llib/weakhash.scm 134 */
BgL_res2234z00_2973 = BgL_aux2801z00_5037; }  else 
{ 
 obj_t BgL_auxz00_7495;
BgL_auxz00_7495 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2801z00_5037); 
FAILURE(BgL_auxz00_7495,BFALSE,BFALSE);} } 
BgL_tmpz00_7491 = BgL_res2234z00_2973; } 
BgL_test3979z00_7490 = 
(BgL_tmpz00_7491==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3979z00_7490)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_7502; int BgL_tmpz00_7500;
BgL_auxz00_7502 = 
BINT(BgL_arg1358z00_2961); 
BgL_tmpz00_7500 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_53, BgL_tmpz00_7500, BgL_auxz00_7502); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1463))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_2963;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2974;
if(
PAIRP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2974 = BgL_bucketz00_1462; }  else 
{ 
 obj_t BgL_auxz00_7510;
BgL_auxz00_7510 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1462); 
FAILURE(BgL_auxz00_7510,BFALSE,BFALSE);} 
BgL_arg1360z00_2963 = 
CDR(BgL_pairz00_2974); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_2975;
if(
PAIRP(BgL_lastzd2bucketzd2_1463))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_2975 = BgL_lastzd2bucketzd2_1463; }  else 
{ 
 obj_t BgL_auxz00_7517;
BgL_auxz00_7517 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1463); 
FAILURE(BgL_auxz00_7517,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_2975, BgL_arg1360z00_2963); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_2964;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_2976;
if(
PAIRP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_2976 = BgL_bucketz00_1462; }  else 
{ 
 obj_t BgL_auxz00_7524;
BgL_auxz00_7524 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1462); 
FAILURE(BgL_auxz00_7524,BFALSE,BFALSE);} 
BgL_arg1361z00_2964 = 
CDR(BgL_pairz00_2976); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_2977;
if(
VECTORP(BgL_bucketsz00_1454))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_2977 = BgL_bucketsz00_1454; }  else 
{ 
 obj_t BgL_auxz00_7531;
BgL_auxz00_7531 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1454); 
FAILURE(BgL_auxz00_7531,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test3986z00_7535;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_7536;
BgL_tmpz00_7536 = 
VECTOR_LENGTH(BgL_vectorz00_2977); 
BgL_test3986z00_7535 = 
BOUND_CHECK(BgL_iz00_1457, BgL_tmpz00_7536); } 
if(BgL_test3986z00_7535)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_2977,BgL_iz00_1457,BgL_arg1361z00_2964); }  else 
{ 
 obj_t BgL_auxz00_7540;
BgL_auxz00_7540 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_2977, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2977)), 
(int)(BgL_iz00_1457)); 
FAILURE(BgL_auxz00_7540,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1430z00_1467;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_2979;
if(
PAIRP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_2979 = BgL_bucketz00_1462; }  else 
{ 
 obj_t BgL_auxz00_7549;
BgL_auxz00_7549 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1462); 
FAILURE(BgL_auxz00_7549,BFALSE,BFALSE);} 
BgL_arg1430z00_1467 = 
CDR(BgL_pairz00_2979); } 
{ 
 obj_t BgL_bucketz00_7554;
BgL_bucketz00_7554 = BgL_arg1430z00_1467; 
BgL_bucketz00_1462 = BgL_bucketz00_7554; 
goto BgL_zc3z04anonymousza31428ze3z87_1464;} } }  else 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1431z00_1468;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_2980;
if(
PAIRP(BgL_bucketz00_1462))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_2980 = BgL_bucketz00_1462; }  else 
{ 
 obj_t BgL_auxz00_7557;
BgL_auxz00_7557 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1462); 
FAILURE(BgL_auxz00_7557,BFALSE,BFALSE);} 
BgL_arg1431z00_1468 = 
CDR(BgL_pairz00_2980); } 
{ 
 obj_t BgL_lastzd2bucketzd2_7563; obj_t BgL_bucketz00_7562;
BgL_bucketz00_7562 = BgL_arg1431z00_1468; 
BgL_lastzd2bucketzd2_7563 = BgL_bucketz00_1462; 
BgL_lastzd2bucketzd2_1463 = BgL_lastzd2bucketzd2_7563; 
BgL_bucketz00_1462 = BgL_bucketz00_7562; 
goto BgL_zc3z04anonymousza31428ze3z87_1464;} } } } } 
{ 
 long BgL_iz00_7564;
BgL_iz00_7564 = 
(BgL_iz00_1457+1L); 
BgL_iz00_1457 = BgL_iz00_7564; 
goto BgL_zc3z04anonymousza31426ze3z87_1458;} } } } } }  else 
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3989z00_7566;
{ /* Llib/weakhash.scm 226 */
 obj_t BgL_arg1458z00_1520;
{ /* Llib/weakhash.scm 226 */
 bool_t BgL_test3990z00_7567;
{ /* Llib/weakhash.scm 226 */
 obj_t BgL_tmpz00_7568;
{ /* Llib/weakhash.scm 226 */
 obj_t BgL_res2235z00_2985;
{ /* Llib/weakhash.scm 226 */
 obj_t BgL_aux2815z00_5051;
BgL_aux2815z00_5051 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2815z00_5051))
{ /* Llib/weakhash.scm 226 */
BgL_res2235z00_2985 = BgL_aux2815z00_5051; }  else 
{ 
 obj_t BgL_auxz00_7572;
BgL_auxz00_7572 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8622L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2815z00_5051); 
FAILURE(BgL_auxz00_7572,BFALSE,BFALSE);} } 
BgL_tmpz00_7568 = BgL_res2235z00_2985; } 
BgL_test3990z00_7567 = 
(BgL_tmpz00_7568==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3990z00_7567)
{ /* Llib/weakhash.scm 226 */
 int BgL_tmpz00_7577;
BgL_tmpz00_7577 = 
(int)(5L); 
BgL_arg1458z00_1520 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7577); }  else 
{ /* Llib/weakhash.scm 226 */
BgL_arg1458z00_1520 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 226 */
 long BgL_n2z00_2986;
{ /* Llib/weakhash.scm 226 */
 obj_t BgL_tmpz00_7581;
if(
INTEGERP(BgL_arg1458z00_1520))
{ /* Llib/weakhash.scm 226 */
BgL_tmpz00_7581 = BgL_arg1458z00_1520
; }  else 
{ 
 obj_t BgL_auxz00_7584;
BgL_auxz00_7584 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8644L), BGl_string3466z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1458z00_1520); 
FAILURE(BgL_auxz00_7584,BFALSE,BFALSE);} 
BgL_n2z00_2986 = 
(long)CINT(BgL_tmpz00_7581); } 
BgL_test3989z00_7566 = 
(3L==BgL_n2z00_2986); } } 
if(BgL_test3989z00_7566)
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_bucketsz00_1477;
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3993z00_7590;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_tmpz00_7591;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_res2236z00_2990;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_aux2818z00_5054;
BgL_aux2818z00_5054 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2818z00_5054))
{ /* Llib/weakhash.scm 205 */
BgL_res2236z00_2990 = BgL_aux2818z00_5054; }  else 
{ 
 obj_t BgL_auxz00_7595;
BgL_auxz00_7595 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2818z00_5054); 
FAILURE(BgL_auxz00_7595,BFALSE,BFALSE);} } 
BgL_tmpz00_7591 = BgL_res2236z00_2990; } 
BgL_test3993z00_7590 = 
(BgL_tmpz00_7591==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test3993z00_7590)
{ /* Llib/weakhash.scm 205 */
 int BgL_tmpz00_7600;
BgL_tmpz00_7600 = 
(int)(2L); 
BgL_bucketsz00_1477 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7600); }  else 
{ /* Llib/weakhash.scm 205 */
BgL_bucketsz00_1477 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 205 */
 long BgL_bucketszd2lenzd2_1478;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_2991;
if(
VECTORP(BgL_bucketsz00_1477))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_2991 = BgL_bucketsz00_1477; }  else 
{ 
 obj_t BgL_auxz00_7606;
BgL_auxz00_7606 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1477); 
FAILURE(BgL_auxz00_7606,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1478 = 
VECTOR_LENGTH(BgL_vectorz00_2991); } 
{ /* Llib/weakhash.scm 205 */

{ 
 long BgL_iz00_1480;
BgL_iz00_1480 = 0L; 
BgL_zc3z04anonymousza31439ze3z87_1481:
if(
(BgL_iz00_1480==BgL_bucketszd2lenzd2_1478))
{ /* Llib/weakhash.scm 205 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_g1066z00_1483;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_2994;
if(
VECTORP(BgL_bucketsz00_1477))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_2994 = BgL_bucketsz00_1477; }  else 
{ 
 obj_t BgL_auxz00_7615;
BgL_auxz00_7615 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1477); 
FAILURE(BgL_auxz00_7615,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test3998z00_7619;
{ /* Llib/weakhash.scm 205 */
 long BgL_tmpz00_7620;
BgL_tmpz00_7620 = 
VECTOR_LENGTH(BgL_vectorz00_2994); 
BgL_test3998z00_7619 = 
BOUND_CHECK(BgL_iz00_1480, BgL_tmpz00_7620); } 
if(BgL_test3998z00_7619)
{ /* Llib/weakhash.scm 205 */
BgL_g1066z00_1483 = 
VECTOR_REF(BgL_vectorz00_2994,BgL_iz00_1480); }  else 
{ 
 obj_t BgL_auxz00_7624;
BgL_auxz00_7624 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_2994, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_2994)), 
(int)(BgL_iz00_1480)); 
FAILURE(BgL_auxz00_7624,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1485; obj_t BgL_lastzd2bucketzd2_1486;
BgL_bucketz00_1485 = BgL_g1066z00_1483; 
BgL_lastzd2bucketzd2_1486 = BFALSE; 
BgL_zc3z04anonymousza31441ze3z87_1487:
if(
NULLP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 205 */((bool_t)0); }  else 
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_retz00_1489;
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_keyz00_1492; obj_t BgL_dataz00_1493;
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_arg1446z00_1495;
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_pairz00_2996;
if(
PAIRP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 228 */
BgL_pairz00_2996 = BgL_bucketz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7635;
BgL_auxz00_7635 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8712L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1485); 
FAILURE(BgL_auxz00_7635,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_pairz00_2999;
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_aux2826z00_5062;
BgL_aux2826z00_5062 = 
CAR(BgL_pairz00_2996); 
if(
PAIRP(BgL_aux2826z00_5062))
{ /* Llib/weakhash.scm 228 */
BgL_pairz00_2999 = BgL_aux2826z00_5062; }  else 
{ 
 obj_t BgL_auxz00_7642;
BgL_auxz00_7642 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8706L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2826z00_5062); 
FAILURE(BgL_auxz00_7642,BFALSE,BFALSE);} } 
BgL_arg1446z00_1495 = 
CAR(BgL_pairz00_2999); } } 
{ /* Llib/weakhash.scm 228 */
 obj_t BgL_objz00_3000;
if(
BGL_WEAKPTRP(BgL_arg1446z00_1495))
{ /* Llib/weakhash.scm 228 */
BgL_objz00_3000 = BgL_arg1446z00_1495; }  else 
{ 
 obj_t BgL_auxz00_7649;
BgL_auxz00_7649 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8718L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1446z00_1495); 
FAILURE(BgL_auxz00_7649,BFALSE,BFALSE);} 
BgL_keyz00_1492 = 
bgl_weakptr_data(BgL_objz00_3000); } } 
{ /* Llib/weakhash.scm 229 */
 obj_t BgL_arg1447z00_1496;
{ /* Llib/weakhash.scm 229 */
 obj_t BgL_pairz00_3001;
if(
PAIRP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 229 */
BgL_pairz00_3001 = BgL_bucketz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7656;
BgL_auxz00_7656 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8753L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1485); 
FAILURE(BgL_auxz00_7656,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 229 */
 obj_t BgL_pairz00_3004;
{ /* Llib/weakhash.scm 229 */
 obj_t BgL_aux2832z00_5068;
BgL_aux2832z00_5068 = 
CAR(BgL_pairz00_3001); 
if(
PAIRP(BgL_aux2832z00_5068))
{ /* Llib/weakhash.scm 229 */
BgL_pairz00_3004 = BgL_aux2832z00_5068; }  else 
{ 
 obj_t BgL_auxz00_7663;
BgL_auxz00_7663 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8747L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2832z00_5068); 
FAILURE(BgL_auxz00_7663,BFALSE,BFALSE);} } 
BgL_arg1447z00_1496 = 
CDR(BgL_pairz00_3004); } } 
{ /* Llib/weakhash.scm 229 */
 obj_t BgL_objz00_3005;
if(
BGL_WEAKPTRP(BgL_arg1447z00_1496))
{ /* Llib/weakhash.scm 229 */
BgL_objz00_3005 = BgL_arg1447z00_1496; }  else 
{ 
 obj_t BgL_auxz00_7670;
BgL_auxz00_7670 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8759L), BGl_string3446z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1447z00_1496); 
FAILURE(BgL_auxz00_7670,BFALSE,BFALSE);} 
BgL_dataz00_1493 = 
bgl_weakptr_data(BgL_objz00_3005); } } 
{ /* Llib/weakhash.scm 230 */
 bool_t BgL_test4006z00_7675;
if(
(BgL_keyz00_1492==BUNSPEC))
{ /* Llib/weakhash.scm 230 */
BgL_test4006z00_7675 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 230 */
BgL_test4006z00_7675 = 
(BgL_dataz00_1493==BUNSPEC)
; } 
if(BgL_test4006z00_7675)
{ /* Llib/weakhash.scm 230 */
BgL_retz00_1489 = BGl_removez00zz__weakhashz00; }  else 
{ /* Llib/weakhash.scm 233 */
 obj_t BgL_funz00_5074;
if(
PROCEDUREP(BgL_funz00_54))
{ /* Llib/weakhash.scm 233 */
BgL_funz00_5074 = BgL_funz00_54; }  else 
{ 
 obj_t BgL_auxz00_7681;
BgL_auxz00_7681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8858L), BGl_string3446z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_54); 
FAILURE(BgL_auxz00_7681,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5074, 2))
{ /* Llib/weakhash.scm 233 */
BgL_retz00_1489 = 
(VA_PROCEDUREP( BgL_funz00_5074 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5074))(BgL_funz00_54, BgL_keyz00_1492, BgL_dataz00_1493, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5074))(BgL_funz00_54, BgL_keyz00_1492, BgL_dataz00_1493) ); }  else 
{ /* Llib/weakhash.scm 233 */
FAILURE(BGl_string3467z00zz__weakhashz00,BGl_list3476z00zz__weakhashz00,BgL_funz00_5074);} } } } 
if(
(BgL_retz00_1489==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_3006;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_3007;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test4011z00_7695;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7696;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2237z00_3013;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2839z00_5076;
BgL_aux2839z00_5076 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2839z00_5076))
{ /* Llib/weakhash.scm 134 */
BgL_res2237z00_3013 = BgL_aux2839z00_5076; }  else 
{ 
 obj_t BgL_auxz00_7700;
BgL_auxz00_7700 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2839z00_5076); 
FAILURE(BgL_auxz00_7700,BFALSE,BFALSE);} } 
BgL_tmpz00_7696 = BgL_res2237z00_3013; } 
BgL_test4011z00_7695 = 
(BgL_tmpz00_7696==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4011z00_7695)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_7705;
BgL_tmpz00_7705 = 
(int)(0L); 
BgL_arg1359z00_3007 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7705); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_3007 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_3014;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7709;
if(
INTEGERP(BgL_arg1359z00_3007))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_7709 = BgL_arg1359z00_3007
; }  else 
{ 
 obj_t BgL_auxz00_7712;
BgL_auxz00_7712 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_3007); 
FAILURE(BgL_auxz00_7712,BFALSE,BFALSE);} 
BgL_za71za7_3014 = 
(long)CINT(BgL_tmpz00_7709); } 
BgL_arg1358z00_3006 = 
(BgL_za71za7_3014-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test4014z00_7718;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7719;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2238z00_3018;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2842z00_5079;
BgL_aux2842z00_5079 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2842z00_5079))
{ /* Llib/weakhash.scm 134 */
BgL_res2238z00_3018 = BgL_aux2842z00_5079; }  else 
{ 
 obj_t BgL_auxz00_7723;
BgL_auxz00_7723 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2842z00_5079); 
FAILURE(BgL_auxz00_7723,BFALSE,BFALSE);} } 
BgL_tmpz00_7719 = BgL_res2238z00_3018; } 
BgL_test4014z00_7718 = 
(BgL_tmpz00_7719==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4014z00_7718)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_7730; int BgL_tmpz00_7728;
BgL_auxz00_7730 = 
BINT(BgL_arg1358z00_3006); 
BgL_tmpz00_7728 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_53, BgL_tmpz00_7728, BgL_auxz00_7730); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1486))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_3008;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_3019;
if(
PAIRP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_3019 = BgL_bucketz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7738;
BgL_auxz00_7738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1485); 
FAILURE(BgL_auxz00_7738,BFALSE,BFALSE);} 
BgL_arg1360z00_3008 = 
CDR(BgL_pairz00_3019); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_3020;
if(
PAIRP(BgL_lastzd2bucketzd2_1486))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_3020 = BgL_lastzd2bucketzd2_1486; }  else 
{ 
 obj_t BgL_auxz00_7745;
BgL_auxz00_7745 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1486); 
FAILURE(BgL_auxz00_7745,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_3020, BgL_arg1360z00_3008); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_3009;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_3021;
if(
PAIRP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_3021 = BgL_bucketz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7752;
BgL_auxz00_7752 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1485); 
FAILURE(BgL_auxz00_7752,BFALSE,BFALSE);} 
BgL_arg1361z00_3009 = 
CDR(BgL_pairz00_3021); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_3022;
if(
VECTORP(BgL_bucketsz00_1477))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_3022 = BgL_bucketsz00_1477; }  else 
{ 
 obj_t BgL_auxz00_7759;
BgL_auxz00_7759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1477); 
FAILURE(BgL_auxz00_7759,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test4021z00_7763;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_7764;
BgL_tmpz00_7764 = 
VECTOR_LENGTH(BgL_vectorz00_3022); 
BgL_test4021z00_7763 = 
BOUND_CHECK(BgL_iz00_1480, BgL_tmpz00_7764); } 
if(BgL_test4021z00_7763)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_3022,BgL_iz00_1480,BgL_arg1361z00_3009); }  else 
{ 
 obj_t BgL_auxz00_7768;
BgL_auxz00_7768 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_3022, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3022)), 
(int)(BgL_iz00_1480)); 
FAILURE(BgL_auxz00_7768,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1443z00_1490;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_3024;
if(
PAIRP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_3024 = BgL_bucketz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7777;
BgL_auxz00_7777 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1485); 
FAILURE(BgL_auxz00_7777,BFALSE,BFALSE);} 
BgL_arg1443z00_1490 = 
CDR(BgL_pairz00_3024); } 
{ 
 obj_t BgL_bucketz00_7782;
BgL_bucketz00_7782 = BgL_arg1443z00_1490; 
BgL_bucketz00_1485 = BgL_bucketz00_7782; 
goto BgL_zc3z04anonymousza31441ze3z87_1487;} } }  else 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1444z00_1491;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_3025;
if(
PAIRP(BgL_bucketz00_1485))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_3025 = BgL_bucketz00_1485; }  else 
{ 
 obj_t BgL_auxz00_7785;
BgL_auxz00_7785 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1485); 
FAILURE(BgL_auxz00_7785,BFALSE,BFALSE);} 
BgL_arg1444z00_1491 = 
CDR(BgL_pairz00_3025); } 
{ 
 obj_t BgL_lastzd2bucketzd2_7791; obj_t BgL_bucketz00_7790;
BgL_bucketz00_7790 = BgL_arg1444z00_1491; 
BgL_lastzd2bucketzd2_7791 = BgL_bucketz00_1485; 
BgL_lastzd2bucketzd2_1486 = BgL_lastzd2bucketzd2_7791; 
BgL_bucketz00_1485 = BgL_bucketz00_7790; 
goto BgL_zc3z04anonymousza31441ze3z87_1487;} } } } } 
{ 
 long BgL_iz00_7792;
BgL_iz00_7792 = 
(BgL_iz00_1480+1L); 
BgL_iz00_1480 = BgL_iz00_7792; 
goto BgL_zc3z04anonymousza31439ze3z87_1481;} } } } } }  else 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_bucketsz00_1500;
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test4024z00_7794;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_tmpz00_7795;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_res2239z00_3030;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_aux2856z00_5093;
BgL_aux2856z00_5093 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2856z00_5093))
{ /* Llib/weakhash.scm 205 */
BgL_res2239z00_3030 = BgL_aux2856z00_5093; }  else 
{ 
 obj_t BgL_auxz00_7799;
BgL_auxz00_7799 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2856z00_5093); 
FAILURE(BgL_auxz00_7799,BFALSE,BFALSE);} } 
BgL_tmpz00_7795 = BgL_res2239z00_3030; } 
BgL_test4024z00_7794 = 
(BgL_tmpz00_7795==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4024z00_7794)
{ /* Llib/weakhash.scm 205 */
 int BgL_tmpz00_7804;
BgL_tmpz00_7804 = 
(int)(2L); 
BgL_bucketsz00_1500 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7804); }  else 
{ /* Llib/weakhash.scm 205 */
BgL_bucketsz00_1500 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 205 */
 long BgL_bucketszd2lenzd2_1501;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_3031;
if(
VECTORP(BgL_bucketsz00_1500))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_3031 = BgL_bucketsz00_1500; }  else 
{ 
 obj_t BgL_auxz00_7810;
BgL_auxz00_7810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3466z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1500); 
FAILURE(BgL_auxz00_7810,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1501 = 
VECTOR_LENGTH(BgL_vectorz00_3031); } 
{ /* Llib/weakhash.scm 205 */

{ 
 long BgL_iz00_1503;
BgL_iz00_1503 = 0L; 
BgL_zc3z04anonymousza31449ze3z87_1504:
if(
(BgL_iz00_1503==BgL_bucketszd2lenzd2_1501))
{ /* Llib/weakhash.scm 205 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_g1067z00_1506;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_vectorz00_3034;
if(
VECTORP(BgL_bucketsz00_1500))
{ /* Llib/weakhash.scm 205 */
BgL_vectorz00_3034 = BgL_bucketsz00_1500; }  else 
{ 
 obj_t BgL_auxz00_7819;
BgL_auxz00_7819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1500); 
FAILURE(BgL_auxz00_7819,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 205 */
 bool_t BgL_test4029z00_7823;
{ /* Llib/weakhash.scm 205 */
 long BgL_tmpz00_7824;
BgL_tmpz00_7824 = 
VECTOR_LENGTH(BgL_vectorz00_3034); 
BgL_test4029z00_7823 = 
BOUND_CHECK(BgL_iz00_1503, BgL_tmpz00_7824); } 
if(BgL_test4029z00_7823)
{ /* Llib/weakhash.scm 205 */
BgL_g1067z00_1506 = 
VECTOR_REF(BgL_vectorz00_3034,BgL_iz00_1503); }  else 
{ 
 obj_t BgL_auxz00_7828;
BgL_auxz00_7828 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3034, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3034)), 
(int)(BgL_iz00_1503)); 
FAILURE(BgL_auxz00_7828,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_bucketz00_1508; obj_t BgL_lastzd2bucketzd2_1509;
BgL_bucketz00_1508 = BgL_g1067z00_1506; 
BgL_lastzd2bucketzd2_1509 = BFALSE; 
BgL_zc3z04anonymousza31451ze3z87_1510:
if(
NULLP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 205 */((bool_t)0); }  else 
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_retz00_1512;
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_arg1455z00_1515; obj_t BgL_arg1456z00_1516;
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_pairz00_3036;
if(
PAIRP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 236 */
BgL_pairz00_3036 = BgL_bucketz00_1508; }  else 
{ 
 obj_t BgL_auxz00_7839;
BgL_auxz00_7839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8924L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1508); 
FAILURE(BgL_auxz00_7839,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_pairz00_3039;
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_aux2864z00_5101;
BgL_aux2864z00_5101 = 
CAR(BgL_pairz00_3036); 
if(
PAIRP(BgL_aux2864z00_5101))
{ /* Llib/weakhash.scm 236 */
BgL_pairz00_3039 = BgL_aux2864z00_5101; }  else 
{ 
 obj_t BgL_auxz00_7846;
BgL_auxz00_7846 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8918L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2864z00_5101); 
FAILURE(BgL_auxz00_7846,BFALSE,BFALSE);} } 
BgL_arg1455z00_1515 = 
CAR(BgL_pairz00_3039); } } 
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_pairz00_3040;
if(
PAIRP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 236 */
BgL_pairz00_3040 = BgL_bucketz00_1508; }  else 
{ 
 obj_t BgL_auxz00_7853;
BgL_auxz00_7853 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8938L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1508); 
FAILURE(BgL_auxz00_7853,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_pairz00_3043;
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_aux2868z00_5105;
BgL_aux2868z00_5105 = 
CAR(BgL_pairz00_3040); 
if(
PAIRP(BgL_aux2868z00_5105))
{ /* Llib/weakhash.scm 236 */
BgL_pairz00_3043 = BgL_aux2868z00_5105; }  else 
{ 
 obj_t BgL_auxz00_7860;
BgL_auxz00_7860 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8932L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_aux2868z00_5105); 
FAILURE(BgL_auxz00_7860,BFALSE,BFALSE);} } 
BgL_arg1456z00_1516 = 
CDR(BgL_pairz00_3043); } } 
{ /* Llib/weakhash.scm 236 */
 obj_t BgL_funz00_5109;
if(
PROCEDUREP(BgL_funz00_54))
{ /* Llib/weakhash.scm 236 */
BgL_funz00_5109 = BgL_funz00_54; }  else 
{ 
 obj_t BgL_auxz00_7867;
BgL_auxz00_7867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(8913L), BGl_string3446z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_54); 
FAILURE(BgL_auxz00_7867,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5109, 2))
{ /* Llib/weakhash.scm 236 */
BgL_retz00_1512 = 
(VA_PROCEDUREP( BgL_funz00_5109 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5109))(BgL_funz00_54, BgL_arg1455z00_1515, BgL_arg1456z00_1516, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5109))(BgL_funz00_54, BgL_arg1455z00_1515, BgL_arg1456z00_1516) ); }  else 
{ /* Llib/weakhash.scm 236 */
FAILURE(BGl_string3467z00zz__weakhashz00,BGl_list3477z00zz__weakhashz00,BgL_funz00_5109);} } } 
if(
(BgL_retz00_1512==BGl_removez00zz__weakhashz00))
{ /* Llib/weakhash.scm 205 */
{ /* Llib/weakhash.scm 134 */
 long BgL_arg1358z00_3044;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_arg1359z00_3045;
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test4038z00_7881;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7882;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2240z00_3051;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2873z00_5111;
BgL_aux2873z00_5111 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2873z00_5111))
{ /* Llib/weakhash.scm 134 */
BgL_res2240z00_3051 = BgL_aux2873z00_5111; }  else 
{ 
 obj_t BgL_auxz00_7886;
BgL_auxz00_7886 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5201L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2873z00_5111); 
FAILURE(BgL_auxz00_7886,BFALSE,BFALSE);} } 
BgL_tmpz00_7882 = BgL_res2240z00_3051; } 
BgL_test4038z00_7881 = 
(BgL_tmpz00_7882==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4038z00_7881)
{ /* Llib/weakhash.scm 134 */
 int BgL_tmpz00_7891;
BgL_tmpz00_7891 = 
(int)(0L); 
BgL_arg1359z00_3045 = 
STRUCT_REF(BgL_tablez00_53, BgL_tmpz00_7891); }  else 
{ /* Llib/weakhash.scm 134 */
BgL_arg1359z00_3045 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } 
{ /* Llib/weakhash.scm 134 */
 long BgL_za71za7_3052;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7895;
if(
INTEGERP(BgL_arg1359z00_3045))
{ /* Llib/weakhash.scm 134 */
BgL_tmpz00_7895 = BgL_arg1359z00_3045
; }  else 
{ 
 obj_t BgL_auxz00_7898;
BgL_auxz00_7898 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5223L), BGl_string3446z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1359z00_3045); 
FAILURE(BgL_auxz00_7898,BFALSE,BFALSE);} 
BgL_za71za7_3052 = 
(long)CINT(BgL_tmpz00_7895); } 
BgL_arg1358z00_3044 = 
(BgL_za71za7_3052-1L); } } 
{ /* Llib/weakhash.scm 134 */
 bool_t BgL_test4041z00_7904;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_tmpz00_7905;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_res2241z00_3056;
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_aux2876z00_5114;
BgL_aux2876z00_5114 = 
STRUCT_KEY(BgL_tablez00_53); 
if(
SYMBOLP(BgL_aux2876z00_5114))
{ /* Llib/weakhash.scm 134 */
BgL_res2241z00_3056 = BgL_aux2876z00_5114; }  else 
{ 
 obj_t BgL_auxz00_7909;
BgL_auxz00_7909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5168L), BGl_string3446z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2876z00_5114); 
FAILURE(BgL_auxz00_7909,BFALSE,BFALSE);} } 
BgL_tmpz00_7905 = BgL_res2241z00_3056; } 
BgL_test4041z00_7904 = 
(BgL_tmpz00_7905==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4041z00_7904)
{ /* Llib/weakhash.scm 134 */
 obj_t BgL_auxz00_7916; int BgL_tmpz00_7914;
BgL_auxz00_7916 = 
BINT(BgL_arg1358z00_3044); 
BgL_tmpz00_7914 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_53, BgL_tmpz00_7914, BgL_auxz00_7916); }  else 
{ /* Llib/weakhash.scm 134 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_53); } } } 
if(
CBOOL(BgL_lastzd2bucketzd2_1509))
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_arg1360z00_3046;
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_3057;
if(
PAIRP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_3057 = BgL_bucketz00_1508; }  else 
{ 
 obj_t BgL_auxz00_7924;
BgL_auxz00_7924 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5282L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1508); 
FAILURE(BgL_auxz00_7924,BFALSE,BFALSE);} 
BgL_arg1360z00_3046 = 
CDR(BgL_pairz00_3057); } 
{ /* Llib/weakhash.scm 136 */
 obj_t BgL_pairz00_3058;
if(
PAIRP(BgL_lastzd2bucketzd2_1509))
{ /* Llib/weakhash.scm 136 */
BgL_pairz00_3058 = BgL_lastzd2bucketzd2_1509; }  else 
{ 
 obj_t BgL_auxz00_7931;
BgL_auxz00_7931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5265L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_lastzd2bucketzd2_1509); 
FAILURE(BgL_auxz00_7931,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_3058, BgL_arg1360z00_3046); } }  else 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_arg1361z00_3047;
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_pairz00_3059;
if(
PAIRP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 137 */
BgL_pairz00_3059 = BgL_bucketz00_1508; }  else 
{ 
 obj_t BgL_auxz00_7938;
BgL_auxz00_7938 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5326L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1508); 
FAILURE(BgL_auxz00_7938,BFALSE,BFALSE);} 
BgL_arg1361z00_3047 = 
CDR(BgL_pairz00_3059); } 
{ /* Llib/weakhash.scm 137 */
 obj_t BgL_vectorz00_3060;
if(
VECTORP(BgL_bucketsz00_1500))
{ /* Llib/weakhash.scm 137 */
BgL_vectorz00_3060 = BgL_bucketsz00_1500; }  else 
{ 
 obj_t BgL_auxz00_7945;
BgL_auxz00_7945 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5311L), BGl_string3446z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1500); 
FAILURE(BgL_auxz00_7945,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 137 */
 bool_t BgL_test4048z00_7949;
{ /* Llib/weakhash.scm 137 */
 long BgL_tmpz00_7950;
BgL_tmpz00_7950 = 
VECTOR_LENGTH(BgL_vectorz00_3060); 
BgL_test4048z00_7949 = 
BOUND_CHECK(BgL_iz00_1503, BgL_tmpz00_7950); } 
if(BgL_test4048z00_7949)
{ /* Llib/weakhash.scm 137 */
VECTOR_SET(BgL_vectorz00_3060,BgL_iz00_1503,BgL_arg1361z00_3047); }  else 
{ 
 obj_t BgL_auxz00_7954;
BgL_auxz00_7954 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(5298L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_3060, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3060)), 
(int)(BgL_iz00_1503)); 
FAILURE(BgL_auxz00_7954,BFALSE,BFALSE);} } } } BGl_keepgoingz00zz__weakhashz00; 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1453z00_1513;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_3062;
if(
PAIRP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_3062 = BgL_bucketz00_1508; }  else 
{ 
 obj_t BgL_auxz00_7963;
BgL_auxz00_7963 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1508); 
FAILURE(BgL_auxz00_7963,BFALSE,BFALSE);} 
BgL_arg1453z00_1513 = 
CDR(BgL_pairz00_3062); } 
{ 
 obj_t BgL_bucketz00_7968;
BgL_bucketz00_7968 = BgL_arg1453z00_1513; 
BgL_bucketz00_1508 = BgL_bucketz00_7968; 
goto BgL_zc3z04anonymousza31451ze3z87_1510;} } }  else 
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_arg1454z00_1514;
{ /* Llib/weakhash.scm 205 */
 obj_t BgL_pairz00_3063;
if(
PAIRP(BgL_bucketz00_1508))
{ /* Llib/weakhash.scm 205 */
BgL_pairz00_3063 = BgL_bucketz00_1508; }  else 
{ 
 obj_t BgL_auxz00_7971;
BgL_auxz00_7971 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(7915L), BGl_string3446z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1508); 
FAILURE(BgL_auxz00_7971,BFALSE,BFALSE);} 
BgL_arg1454z00_1514 = 
CDR(BgL_pairz00_3063); } 
{ 
 obj_t BgL_lastzd2bucketzd2_7977; obj_t BgL_bucketz00_7976;
BgL_bucketz00_7976 = BgL_arg1454z00_1514; 
BgL_lastzd2bucketzd2_7977 = BgL_bucketz00_1508; 
BgL_lastzd2bucketzd2_1509 = BgL_lastzd2bucketzd2_7977; 
BgL_bucketz00_1508 = BgL_bucketz00_7976; 
goto BgL_zc3z04anonymousza31451ze3z87_1510;} } } } } 
{ 
 long BgL_iz00_7978;
BgL_iz00_7978 = 
(BgL_iz00_1503+1L); 
BgL_iz00_1503 = BgL_iz00_7978; 
goto BgL_zc3z04anonymousza31449ze3z87_1504;} } } } } } } } } } 

}



/* weak-hashtable->vector */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(obj_t BgL_tablez00_57)
{
{ /* Llib/weakhash.scm 256 */
{ /* Llib/weakhash.scm 257 */
 obj_t BgL_vecz00_1524; obj_t BgL_wz00_4429;
BgL_vecz00_1524 = 
make_vector(
BGl_hashtablezd2siza7ez75zz__hashz00(BgL_tablez00_57), BUNSPEC); 
BgL_wz00_4429 = 
MAKE_CELL(
BINT(0L)); 
{ /* Llib/weakhash.scm 261 */
 obj_t BgL_zc3z04anonymousza31463ze3z87_4420;
BgL_zc3z04anonymousza31463ze3z87_4420 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31463ze3ze5zz__weakhashz00, 
(int)(2L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31463ze3z87_4420, 
(int)(0L), BgL_vecz00_1524); 
PROCEDURE_SET(BgL_zc3z04anonymousza31463ze3z87_4420, 
(int)(1L), 
((obj_t)BgL_wz00_4429)); 
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_57))
{ /* Llib/weakhash.scm 249 */
BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_57, BgL_zc3z04anonymousza31463ze3z87_4420); }  else 
{ /* Llib/weakhash.scm 249 */
BBOOL(
BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_57, BgL_zc3z04anonymousza31463ze3z87_4420)); } } 
{ /* Llib/weakhash.scm 263 */
 bool_t BgL_test4052z00_7996;
{ /* Llib/weakhash.scm 263 */
 long BgL_arg1466z00_1533;
BgL_arg1466z00_1533 = 
BGl_hashtablezd2siza7ez75zz__hashz00(BgL_tablez00_57); 
{ /* Llib/weakhash.scm 263 */
 long BgL_n1z00_3072;
{ /* Llib/weakhash.scm 263 */
 obj_t BgL_tmpz00_7998;
{ /* Llib/weakhash.scm 263 */
 obj_t BgL_aux2890z00_5129;
BgL_aux2890z00_5129 = 
CELL_REF(BgL_wz00_4429); 
if(
INTEGERP(BgL_aux2890z00_5129))
{ /* Llib/weakhash.scm 263 */
BgL_tmpz00_7998 = BgL_aux2890z00_5129
; }  else 
{ 
 obj_t BgL_auxz00_8001;
BgL_auxz00_8001 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10032L), BGl_string3482z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux2890z00_5129); 
FAILURE(BgL_auxz00_8001,BFALSE,BFALSE);} } 
BgL_n1z00_3072 = 
(long)CINT(BgL_tmpz00_7998); } 
BgL_test4052z00_7996 = 
(BgL_n1z00_3072<BgL_arg1466z00_1533); } } 
if(BgL_test4052z00_7996)
{ /* Llib/weakhash.scm 264 */
 long BgL_auxz00_8007;
{ /* Llib/weakhash.scm 264 */
 obj_t BgL_tmpz00_8008;
{ /* Llib/weakhash.scm 264 */
 obj_t BgL_aux2891z00_5130;
BgL_aux2891z00_5130 = 
CELL_REF(BgL_wz00_4429); 
if(
INTEGERP(BgL_aux2891z00_5130))
{ /* Llib/weakhash.scm 264 */
BgL_tmpz00_8008 = BgL_aux2891z00_5130
; }  else 
{ 
 obj_t BgL_auxz00_8011;
BgL_auxz00_8011 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10078L), BGl_string3482z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux2891z00_5130); 
FAILURE(BgL_auxz00_8011,BFALSE,BFALSE);} } 
BgL_auxz00_8007 = 
(long)CINT(BgL_tmpz00_8008); } 
return 
BGl_copyzd2vectorzd2zz__r4_vectors_6_8z00(BgL_vecz00_1524, BgL_auxz00_8007);}  else 
{ /* Llib/weakhash.scm 263 */
return BgL_vecz00_1524;} } } } 

}



/* &weak-hashtable->vector */
obj_t BGl_z62weakzd2hashtablezd2ze3vectorz81zz__weakhashz00(obj_t BgL_envz00_4421, obj_t BgL_tablez00_4422)
{
{ /* Llib/weakhash.scm 256 */
{ /* Llib/weakhash.scm 257 */
 obj_t BgL_auxz00_8017;
if(
STRUCTP(BgL_tablez00_4422))
{ /* Llib/weakhash.scm 257 */
BgL_auxz00_8017 = BgL_tablez00_4422
; }  else 
{ 
 obj_t BgL_auxz00_8020;
BgL_auxz00_8020 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(9857L), BGl_string3483z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4422); 
FAILURE(BgL_auxz00_8020,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2ze3vectorze3zz__weakhashz00(BgL_auxz00_8017);} } 

}



/* &<@anonymous:1463> */
obj_t BGl_z62zc3z04anonymousza31463ze3ze5zz__weakhashz00(obj_t BgL_envz00_4423, obj_t BgL_keyz00_4426, obj_t BgL_valz00_4427)
{
{ /* Llib/weakhash.scm 260 */
{ /* Llib/weakhash.scm 261 */
 obj_t BgL_vecz00_4424; obj_t BgL_wz00_4425;
BgL_vecz00_4424 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_4423, 
(int)(0L))); 
BgL_wz00_4425 = 
PROCEDURE_REF(BgL_envz00_4423, 
(int)(1L)); 
{ /* Llib/weakhash.scm 261 */
 obj_t BgL_vectorz00_5720; long BgL_kz00_5721;
if(
VECTORP(BgL_vecz00_4424))
{ /* Llib/weakhash.scm 261 */
BgL_vectorz00_5720 = BgL_vecz00_4424; }  else 
{ 
 obj_t BgL_auxz00_8032;
BgL_auxz00_8032 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(9980L), BGl_string3485z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_vecz00_4424); 
FAILURE(BgL_auxz00_8032,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 261 */
 obj_t BgL_tmpz00_8036;
{ /* Llib/weakhash.scm 261 */
 obj_t BgL_aux2896z00_5722;
BgL_aux2896z00_5722 = 
CELL_REF(BgL_wz00_4425); 
if(
INTEGERP(BgL_aux2896z00_5722))
{ /* Llib/weakhash.scm 261 */
BgL_tmpz00_8036 = BgL_aux2896z00_5722
; }  else 
{ 
 obj_t BgL_auxz00_8039;
BgL_auxz00_8039 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(9984L), BGl_string3485z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux2896z00_5722); 
FAILURE(BgL_auxz00_8039,BFALSE,BFALSE);} } 
BgL_kz00_5721 = 
(long)CINT(BgL_tmpz00_8036); } 
{ /* Llib/weakhash.scm 261 */
 bool_t BgL_test4058z00_8044;
{ /* Llib/weakhash.scm 261 */
 long BgL_tmpz00_8045;
BgL_tmpz00_8045 = 
VECTOR_LENGTH(BgL_vectorz00_5720); 
BgL_test4058z00_8044 = 
BOUND_CHECK(BgL_kz00_5721, BgL_tmpz00_8045); } 
if(BgL_test4058z00_8044)
{ /* Llib/weakhash.scm 261 */
VECTOR_SET(BgL_vectorz00_5720,BgL_kz00_5721,BgL_valz00_4427); }  else 
{ 
 obj_t BgL_auxz00_8049;
BgL_auxz00_8049 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(9967L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_5720, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_5720)), 
(int)(BgL_kz00_5721)); 
FAILURE(BgL_auxz00_8049,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 262 */
 obj_t BgL_auxz00_5723;
{ /* Llib/weakhash.scm 261 */
 obj_t BgL_tmpz00_8056;
{ /* Llib/weakhash.scm 262 */
 obj_t BgL_aux2897z00_5724;
BgL_aux2897z00_5724 = 
CELL_REF(BgL_wz00_4425); 
if(
INTEGERP(BgL_aux2897z00_5724))
{ /* Llib/weakhash.scm 262 */
BgL_tmpz00_8056 = BgL_aux2897z00_5724
; }  else 
{ 
 obj_t BgL_auxz00_8059;
BgL_auxz00_8059 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10009L), BGl_string3485z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux2897z00_5724); 
FAILURE(BgL_auxz00_8059,BFALSE,BFALSE);} } 
BgL_auxz00_5723 = 
ADDFX(BgL_tmpz00_8056, 
BINT(1L)); } 
return 
CELL_SET(BgL_wz00_4425, BgL_auxz00_5723);} } } 

}



/* weak-hashtable->list */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(obj_t BgL_tablez00_58)
{
{ /* Llib/weakhash.scm 270 */
{ /* Llib/weakhash.scm 271 */
 obj_t BgL_resz00_4439;
BgL_resz00_4439 = 
MAKE_CELL(BNIL); 
{ /* Llib/weakhash.scm 274 */
 obj_t BgL_zc3z04anonymousza31470ze3z87_4431;
BgL_zc3z04anonymousza31470ze3z87_4431 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31470ze3ze5zz__weakhashz00, 
(int)(2L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31470ze3z87_4431, 
(int)(0L), 
((obj_t)BgL_resz00_4439)); 
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_58))
{ /* Llib/weakhash.scm 249 */
BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_58, BgL_zc3z04anonymousza31470ze3z87_4431); }  else 
{ /* Llib/weakhash.scm 249 */
BBOOL(
BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_58, BgL_zc3z04anonymousza31470ze3z87_4431)); } } 
{ /* Llib/weakhash.scm 275 */
 obj_t BgL_aux2898z00_5137;
BgL_aux2898z00_5137 = 
CELL_REF(BgL_resz00_4439); 
{ /* Llib/weakhash.scm 275 */
 bool_t BgL_test4061z00_8076;
if(
PAIRP(BgL_aux2898z00_5137))
{ /* Llib/weakhash.scm 275 */
BgL_test4061z00_8076 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 275 */
BgL_test4061z00_8076 = 
NULLP(BgL_aux2898z00_5137)
; } 
if(BgL_test4061z00_8076)
{ /* Llib/weakhash.scm 275 */
return BgL_aux2898z00_5137;}  else 
{ 
 obj_t BgL_auxz00_8080;
BgL_auxz00_8080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10465L), BGl_string3486z00zz__weakhashz00, BGl_string3487z00zz__weakhashz00, BgL_aux2898z00_5137); 
FAILURE(BgL_auxz00_8080,BFALSE,BFALSE);} } } } } 

}



/* &weak-hashtable->list */
obj_t BGl_z62weakzd2hashtablezd2ze3listz81zz__weakhashz00(obj_t BgL_envz00_4432, obj_t BgL_tablez00_4433)
{
{ /* Llib/weakhash.scm 270 */
{ /* Llib/weakhash.scm 271 */
 obj_t BgL_auxz00_8084;
if(
STRUCTP(BgL_tablez00_4433))
{ /* Llib/weakhash.scm 271 */
BgL_auxz00_8084 = BgL_tablez00_4433
; }  else 
{ 
 obj_t BgL_auxz00_8087;
BgL_auxz00_8087 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10362L), BGl_string3488z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4433); 
FAILURE(BgL_auxz00_8087,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2ze3listze3zz__weakhashz00(BgL_auxz00_8084);} } 

}



/* &<@anonymous:1470> */
obj_t BGl_z62zc3z04anonymousza31470ze3ze5zz__weakhashz00(obj_t BgL_envz00_4434, obj_t BgL_keyz00_4436, obj_t BgL_valz00_4437)
{
{ /* Llib/weakhash.scm 273 */
{ /* Llib/weakhash.scm 274 */
 obj_t BgL_resz00_4435;
BgL_resz00_4435 = 
PROCEDURE_REF(BgL_envz00_4434, 
(int)(0L)); 
{ /* Llib/weakhash.scm 274 */
 obj_t BgL_auxz00_5726;
BgL_auxz00_5726 = 
MAKE_YOUNG_PAIR(BgL_valz00_4437, 
CELL_REF(BgL_resz00_4435)); 
return 
CELL_SET(BgL_resz00_4435, BgL_auxz00_5726);} } } 

}



/* weak-hashtable-key-list */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(obj_t BgL_tablez00_59)
{
{ /* Llib/weakhash.scm 280 */
{ /* Llib/weakhash.scm 281 */
 obj_t BgL_resz00_4449;
BgL_resz00_4449 = 
MAKE_CELL(BNIL); 
{ /* Llib/weakhash.scm 284 */
 obj_t BgL_zc3z04anonymousza31473ze3z87_4441;
BgL_zc3z04anonymousza31473ze3z87_4441 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31473ze3ze5zz__weakhashz00, 
(int)(2L), 
(int)(1L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31473ze3z87_4441, 
(int)(0L), 
((obj_t)BgL_resz00_4449)); 
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_59))
{ /* Llib/weakhash.scm 249 */
BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_59, BgL_zc3z04anonymousza31473ze3z87_4441); }  else 
{ /* Llib/weakhash.scm 249 */
BBOOL(
BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_59, BgL_zc3z04anonymousza31473ze3z87_4441)); } } 
{ /* Llib/weakhash.scm 285 */
 obj_t BgL_aux2902z00_5141;
BgL_aux2902z00_5141 = 
CELL_REF(BgL_resz00_4449); 
{ /* Llib/weakhash.scm 285 */
 bool_t BgL_test4065z00_8106;
if(
PAIRP(BgL_aux2902z00_5141))
{ /* Llib/weakhash.scm 285 */
BgL_test4065z00_8106 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 285 */
BgL_test4065z00_8106 = 
NULLP(BgL_aux2902z00_5141)
; } 
if(BgL_test4065z00_8106)
{ /* Llib/weakhash.scm 285 */
return BgL_aux2902z00_5141;}  else 
{ 
 obj_t BgL_auxz00_8110;
BgL_auxz00_8110 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10848L), BGl_string3489z00zz__weakhashz00, BGl_string3487z00zz__weakhashz00, BgL_aux2902z00_5141); 
FAILURE(BgL_auxz00_8110,BFALSE,BFALSE);} } } } } 

}



/* &weak-hashtable-key-list */
obj_t BGl_z62weakzd2hashtablezd2keyzd2listzb0zz__weakhashz00(obj_t BgL_envz00_4442, obj_t BgL_tablez00_4443)
{
{ /* Llib/weakhash.scm 280 */
{ /* Llib/weakhash.scm 281 */
 obj_t BgL_auxz00_8114;
if(
STRUCTP(BgL_tablez00_4443))
{ /* Llib/weakhash.scm 281 */
BgL_auxz00_8114 = BgL_tablez00_4443
; }  else 
{ 
 obj_t BgL_auxz00_8117;
BgL_auxz00_8117 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(10745L), BGl_string3490z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4443); 
FAILURE(BgL_auxz00_8117,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2keyzd2listzd2zz__weakhashz00(BgL_auxz00_8114);} } 

}



/* &<@anonymous:1473> */
obj_t BGl_z62zc3z04anonymousza31473ze3ze5zz__weakhashz00(obj_t BgL_envz00_4444, obj_t BgL_keyz00_4446, obj_t BgL_valz00_4447)
{
{ /* Llib/weakhash.scm 283 */
{ /* Llib/weakhash.scm 284 */
 obj_t BgL_resz00_4445;
BgL_resz00_4445 = 
PROCEDURE_REF(BgL_envz00_4444, 
(int)(0L)); 
{ /* Llib/weakhash.scm 284 */
 obj_t BgL_auxz00_5728;
BgL_auxz00_5728 = 
MAKE_YOUNG_PAIR(BgL_keyz00_4446, 
CELL_REF(BgL_resz00_4445)); 
return 
CELL_SET(BgL_resz00_4445, BgL_auxz00_5728);} } } 

}



/* weak-hashtable-map */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(obj_t BgL_tablez00_60, obj_t BgL_funz00_61)
{
{ /* Llib/weakhash.scm 290 */
{ /* Llib/weakhash.scm 291 */
 obj_t BgL_resz00_4461;
BgL_resz00_4461 = 
MAKE_CELL(BNIL); 
{ /* Llib/weakhash.scm 294 */
 obj_t BgL_zc3z04anonymousza31475ze3z87_4451;
BgL_zc3z04anonymousza31475ze3z87_4451 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31475ze3ze5zz__weakhashz00, 
(int)(2L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31475ze3z87_4451, 
(int)(0L), BgL_funz00_61); 
PROCEDURE_SET(BgL_zc3z04anonymousza31475ze3z87_4451, 
(int)(1L), 
((obj_t)BgL_resz00_4461)); 
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_60))
{ /* Llib/weakhash.scm 249 */
BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_60, BgL_zc3z04anonymousza31475ze3z87_4451); }  else 
{ /* Llib/weakhash.scm 249 */
BBOOL(
BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_60, BgL_zc3z04anonymousza31475ze3z87_4451)); } } 
{ /* Llib/weakhash.scm 295 */
 obj_t BgL_aux2906z00_5145;
BgL_aux2906z00_5145 = 
CELL_REF(BgL_resz00_4461); 
{ /* Llib/weakhash.scm 295 */
 bool_t BgL_test4069z00_8138;
if(
PAIRP(BgL_aux2906z00_5145))
{ /* Llib/weakhash.scm 295 */
BgL_test4069z00_8138 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 295 */
BgL_test4069z00_8138 = 
NULLP(BgL_aux2906z00_5145)
; } 
if(BgL_test4069z00_8138)
{ /* Llib/weakhash.scm 295 */
return BgL_aux2906z00_5145;}  else 
{ 
 obj_t BgL_auxz00_8142;
BgL_auxz00_8142 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(11251L), BGl_string3491z00zz__weakhashz00, BGl_string3487z00zz__weakhashz00, BgL_aux2906z00_5145); 
FAILURE(BgL_auxz00_8142,BFALSE,BFALSE);} } } } } 

}



/* &weak-hashtable-map */
obj_t BGl_z62weakzd2hashtablezd2mapz62zz__weakhashz00(obj_t BgL_envz00_4452, obj_t BgL_tablez00_4453, obj_t BgL_funz00_4454)
{
{ /* Llib/weakhash.scm 290 */
{ /* Llib/weakhash.scm 291 */
 obj_t BgL_auxz00_8153; obj_t BgL_auxz00_8146;
if(
PROCEDUREP(BgL_funz00_4454))
{ /* Llib/weakhash.scm 291 */
BgL_auxz00_8153 = BgL_funz00_4454
; }  else 
{ 
 obj_t BgL_auxz00_8156;
BgL_auxz00_8156 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(11138L), BGl_string3492z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_4454); 
FAILURE(BgL_auxz00_8156,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_4453))
{ /* Llib/weakhash.scm 291 */
BgL_auxz00_8146 = BgL_tablez00_4453
; }  else 
{ 
 obj_t BgL_auxz00_8149;
BgL_auxz00_8149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(11138L), BGl_string3492z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4453); 
FAILURE(BgL_auxz00_8149,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2mapz00zz__weakhashz00(BgL_auxz00_8146, BgL_auxz00_8153);} } 

}



/* &<@anonymous:1475> */
obj_t BGl_z62zc3z04anonymousza31475ze3ze5zz__weakhashz00(obj_t BgL_envz00_4455, obj_t BgL_keyz00_4458, obj_t BgL_valz00_4459)
{
{ /* Llib/weakhash.scm 293 */
{ /* Llib/weakhash.scm 294 */
 obj_t BgL_funz00_4456; obj_t BgL_resz00_4457;
BgL_funz00_4456 = 
((obj_t)
PROCEDURE_REF(BgL_envz00_4455, 
(int)(0L))); 
BgL_resz00_4457 = 
PROCEDURE_REF(BgL_envz00_4455, 
(int)(1L)); 
{ /* Llib/weakhash.scm 294 */
 obj_t BgL_auxz00_5730;
{ /* Llib/weakhash.scm 294 */
 obj_t BgL_arg1476z00_5731;
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_4456, 2))
{ /* Llib/weakhash.scm 294 */
BgL_arg1476z00_5731 = 
BGL_PROCEDURE_CALL2(BgL_funz00_4456, BgL_keyz00_4458, BgL_valz00_4459); }  else 
{ /* Llib/weakhash.scm 294 */
FAILURE(BGl_string3493z00zz__weakhashz00,BGl_list3494z00zz__weakhashz00,BgL_funz00_4456);} 
BgL_auxz00_5730 = 
MAKE_YOUNG_PAIR(BgL_arg1476z00_5731, 
CELL_REF(BgL_resz00_4457)); } 
return 
CELL_SET(BgL_resz00_4457, BgL_auxz00_5730);} } } 

}



/* weak-hashtable-for-each */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(obj_t BgL_tablez00_62, obj_t BgL_funz00_63)
{
{ /* Llib/weakhash.scm 300 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_62))
{ /* Llib/weakhash.scm 249 */
return 
BGl_keyszd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_62, BgL_funz00_63);}  else 
{ /* Llib/weakhash.scm 249 */
return 
BBOOL(
BGl_oldzd2traversezd2hashz00zz__weakhashz00(BgL_tablez00_62, BgL_funz00_63));} } 

}



/* &weak-hashtable-for-each */
obj_t BGl_z62weakzd2hashtablezd2forzd2eachzb0zz__weakhashz00(obj_t BgL_envz00_4463, obj_t BgL_tablez00_4464, obj_t BgL_funz00_4465)
{
{ /* Llib/weakhash.scm 300 */
{ /* Llib/weakhash.scm 249 */
 obj_t BgL_auxz00_8187; obj_t BgL_auxz00_8180;
if(
PROCEDUREP(BgL_funz00_4465))
{ /* Llib/weakhash.scm 249 */
BgL_auxz00_8187 = BgL_funz00_4465
; }  else 
{ 
 obj_t BgL_auxz00_8190;
BgL_auxz00_8190 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(9474L), BGl_string3497z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_4465); 
FAILURE(BgL_auxz00_8190,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_4464))
{ /* Llib/weakhash.scm 249 */
BgL_auxz00_8180 = BgL_tablez00_4464
; }  else 
{ 
 obj_t BgL_auxz00_8183;
BgL_auxz00_8183 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(9474L), BGl_string3497z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4464); 
FAILURE(BgL_auxz00_8183,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2forzd2eachzd2zz__weakhashz00(BgL_auxz00_8180, BgL_auxz00_8187);} } 

}



/* weak-keys-hashtable-filter! */
bool_t BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t BgL_tablez00_64, obj_t BgL_funz00_65)
{
{ /* Llib/weakhash.scm 306 */
{ /* Llib/weakhash.scm 307 */
 obj_t BgL_bucketsz00_1555;
{ /* Llib/weakhash.scm 307 */
 bool_t BgL_test4077z00_8195;
{ /* Llib/weakhash.scm 307 */
 obj_t BgL_tmpz00_8196;
{ /* Llib/weakhash.scm 307 */
 obj_t BgL_res2243z00_3091;
{ /* Llib/weakhash.scm 307 */
 obj_t BgL_aux2917z00_5157;
BgL_aux2917z00_5157 = 
STRUCT_KEY(BgL_tablez00_64); 
if(
SYMBOLP(BgL_aux2917z00_5157))
{ /* Llib/weakhash.scm 307 */
BgL_res2243z00_3091 = BgL_aux2917z00_5157; }  else 
{ 
 obj_t BgL_auxz00_8200;
BgL_auxz00_8200 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(11882L), BGl_string3498z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2917z00_5157); 
FAILURE(BgL_auxz00_8200,BFALSE,BFALSE);} } 
BgL_tmpz00_8196 = BgL_res2243z00_3091; } 
BgL_test4077z00_8195 = 
(BgL_tmpz00_8196==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4077z00_8195)
{ /* Llib/weakhash.scm 307 */
 int BgL_tmpz00_8205;
BgL_tmpz00_8205 = 
(int)(2L); 
BgL_bucketsz00_1555 = 
STRUCT_REF(BgL_tablez00_64, BgL_tmpz00_8205); }  else 
{ /* Llib/weakhash.scm 307 */
BgL_bucketsz00_1555 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_64); } } 
{ /* Llib/weakhash.scm 307 */
 long BgL_bucketszd2lenzd2_1556;
{ /* Llib/weakhash.scm 308 */
 obj_t BgL_vectorz00_3092;
if(
VECTORP(BgL_bucketsz00_1555))
{ /* Llib/weakhash.scm 308 */
BgL_vectorz00_3092 = BgL_bucketsz00_1555; }  else 
{ 
 obj_t BgL_auxz00_8211;
BgL_auxz00_8211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(11941L), BGl_string3498z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1555); 
FAILURE(BgL_auxz00_8211,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1556 = 
VECTOR_LENGTH(BgL_vectorz00_3092); } 
{ /* Llib/weakhash.scm 308 */

{ 
 long BgL_iz00_1558;
BgL_iz00_1558 = 0L; 
BgL_zc3z04anonymousza31477ze3z87_1559:
if(
(BgL_iz00_1558<BgL_bucketszd2lenzd2_1556))
{ /* Llib/weakhash.scm 311 */
 obj_t BgL_bucketz00_1561; obj_t BgL_countz00_4472;
{ /* Llib/weakhash.scm 311 */
 obj_t BgL_vectorz00_3095;
if(
VECTORP(BgL_bucketsz00_1555))
{ /* Llib/weakhash.scm 311 */
BgL_vectorz00_3095 = BgL_bucketsz00_1555; }  else 
{ 
 obj_t BgL_auxz00_8220;
BgL_auxz00_8220 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12035L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1555); 
FAILURE(BgL_auxz00_8220,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 311 */
 bool_t BgL_test4082z00_8224;
{ /* Llib/weakhash.scm 311 */
 long BgL_tmpz00_8225;
BgL_tmpz00_8225 = 
VECTOR_LENGTH(BgL_vectorz00_3095); 
BgL_test4082z00_8224 = 
BOUND_CHECK(BgL_iz00_1558, BgL_tmpz00_8225); } 
if(BgL_test4082z00_8224)
{ /* Llib/weakhash.scm 311 */
BgL_bucketz00_1561 = 
VECTOR_REF(BgL_vectorz00_3095,BgL_iz00_1558); }  else 
{ 
 obj_t BgL_auxz00_8229;
BgL_auxz00_8229 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12023L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3095, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3095)), 
(int)(BgL_iz00_1558)); 
FAILURE(BgL_auxz00_8229,BFALSE,BFALSE);} } } 
BgL_countz00_4472 = 
MAKE_CELL(
BINT(0L)); 
{ /* Llib/weakhash.scm 315 */
 obj_t BgL_arg1479z00_1563;
{ /* Llib/weakhash.scm 315 */
 obj_t BgL_zc3z04anonymousza31481ze3z87_4466;
BgL_zc3z04anonymousza31481ze3z87_4466 = 
MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31481ze3ze5zz__weakhashz00, 
(int)(1L), 
(int)(2L)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_4466, 
(int)(0L), 
((obj_t)BgL_countz00_4472)); 
PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_4466, 
(int)(1L), 
((obj_t)BgL_funz00_65)); 
{ /* Llib/weakhash.scm 314 */
 obj_t BgL_auxz00_8246;
{ /* Llib/weakhash.scm 321 */
 bool_t BgL_test4083z00_8247;
if(
PAIRP(BgL_bucketz00_1561))
{ /* Llib/weakhash.scm 321 */
BgL_test4083z00_8247 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 321 */
BgL_test4083z00_8247 = 
NULLP(BgL_bucketz00_1561)
; } 
if(BgL_test4083z00_8247)
{ /* Llib/weakhash.scm 321 */
BgL_auxz00_8246 = BgL_bucketz00_1561
; }  else 
{ 
 obj_t BgL_auxz00_8251;
BgL_auxz00_8251 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12307L), BGl_string3453z00zz__weakhashz00, BGl_string3487z00zz__weakhashz00, BgL_bucketz00_1561); 
FAILURE(BgL_auxz00_8251,BFALSE,BFALSE);} } 
BgL_arg1479z00_1563 = 
BGl_filterz12z12zz__r4_control_features_6_9z00(BgL_zc3z04anonymousza31481ze3z87_4466, BgL_auxz00_8246); } } 
{ /* Llib/weakhash.scm 313 */
 obj_t BgL_vectorz00_3101;
if(
VECTORP(BgL_bucketsz00_1555))
{ /* Llib/weakhash.scm 313 */
BgL_vectorz00_3101 = BgL_bucketsz00_1555; }  else 
{ 
 obj_t BgL_auxz00_8258;
BgL_auxz00_8258 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12083L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1555); 
FAILURE(BgL_auxz00_8258,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 313 */
 bool_t BgL_test4086z00_8262;
{ /* Llib/weakhash.scm 313 */
 long BgL_tmpz00_8263;
BgL_tmpz00_8263 = 
VECTOR_LENGTH(BgL_vectorz00_3101); 
BgL_test4086z00_8262 = 
BOUND_CHECK(BgL_iz00_1558, BgL_tmpz00_8263); } 
if(BgL_test4086z00_8262)
{ /* Llib/weakhash.scm 313 */
VECTOR_SET(BgL_vectorz00_3101,BgL_iz00_1558,BgL_arg1479z00_1563); }  else 
{ 
 obj_t BgL_auxz00_8267;
BgL_auxz00_8267 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12070L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_3101, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3101)), 
(int)(BgL_iz00_1558)); 
FAILURE(BgL_auxz00_8267,BFALSE,BFALSE);} } } } 
{ /* Llib/weakhash.scm 322 */
 long BgL_arg1497z00_1582;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_arg1498z00_1583;
{ /* Llib/weakhash.scm 322 */
 bool_t BgL_test4087z00_8274;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_tmpz00_8275;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_res2244z00_3106;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_aux2927z00_5167;
BgL_aux2927z00_5167 = 
STRUCT_KEY(BgL_tablez00_64); 
if(
SYMBOLP(BgL_aux2927z00_5167))
{ /* Llib/weakhash.scm 322 */
BgL_res2244z00_3106 = BgL_aux2927z00_5167; }  else 
{ 
 obj_t BgL_auxz00_8279;
BgL_auxz00_8279 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12357L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2927z00_5167); 
FAILURE(BgL_auxz00_8279,BFALSE,BFALSE);} } 
BgL_tmpz00_8275 = BgL_res2244z00_3106; } 
BgL_test4087z00_8274 = 
(BgL_tmpz00_8275==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4087z00_8274)
{ /* Llib/weakhash.scm 322 */
 int BgL_tmpz00_8284;
BgL_tmpz00_8284 = 
(int)(0L); 
BgL_arg1498z00_1583 = 
STRUCT_REF(BgL_tablez00_64, BgL_tmpz00_8284); }  else 
{ /* Llib/weakhash.scm 322 */
BgL_arg1498z00_1583 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_64); } } 
{ /* Llib/weakhash.scm 322 */
 long BgL_za71za7_3107; long BgL_za72za7_3108;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_tmpz00_8288;
if(
INTEGERP(BgL_arg1498z00_1583))
{ /* Llib/weakhash.scm 322 */
BgL_tmpz00_8288 = BgL_arg1498z00_1583
; }  else 
{ 
 obj_t BgL_auxz00_8291;
BgL_auxz00_8291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12379L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1498z00_1583); 
FAILURE(BgL_auxz00_8291,BFALSE,BFALSE);} 
BgL_za71za7_3107 = 
(long)CINT(BgL_tmpz00_8288); } 
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_tmpz00_8296;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_aux2930z00_5170;
BgL_aux2930z00_5170 = 
CELL_REF(BgL_countz00_4472); 
if(
INTEGERP(BgL_aux2930z00_5170))
{ /* Llib/weakhash.scm 322 */
BgL_tmpz00_8296 = BgL_aux2930z00_5170
; }  else 
{ 
 obj_t BgL_auxz00_8299;
BgL_auxz00_8299 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12381L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux2930z00_5170); 
FAILURE(BgL_auxz00_8299,BFALSE,BFALSE);} } 
BgL_za72za7_3108 = 
(long)CINT(BgL_tmpz00_8296); } 
BgL_arg1497z00_1582 = 
(BgL_za71za7_3107-BgL_za72za7_3108); } } 
{ /* Llib/weakhash.scm 322 */
 bool_t BgL_test4091z00_8305;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_tmpz00_8306;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_res2245z00_3112;
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_aux2931z00_5171;
BgL_aux2931z00_5171 = 
STRUCT_KEY(BgL_tablez00_64); 
if(
SYMBOLP(BgL_aux2931z00_5171))
{ /* Llib/weakhash.scm 322 */
BgL_res2245z00_3112 = BgL_aux2931z00_5171; }  else 
{ 
 obj_t BgL_auxz00_8310;
BgL_auxz00_8310 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12324L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2931z00_5171); 
FAILURE(BgL_auxz00_8310,BFALSE,BFALSE);} } 
BgL_tmpz00_8306 = BgL_res2245z00_3112; } 
BgL_test4091z00_8305 = 
(BgL_tmpz00_8306==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4091z00_8305)
{ /* Llib/weakhash.scm 322 */
 obj_t BgL_auxz00_8317; int BgL_tmpz00_8315;
BgL_auxz00_8317 = 
BINT(BgL_arg1497z00_1582); 
BgL_tmpz00_8315 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_64, BgL_tmpz00_8315, BgL_auxz00_8317); }  else 
{ /* Llib/weakhash.scm 322 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_64); } } } 
{ 
 long BgL_iz00_8321;
BgL_iz00_8321 = 
(BgL_iz00_1558+1L); 
BgL_iz00_1558 = BgL_iz00_8321; 
goto BgL_zc3z04anonymousza31477ze3z87_1559;} }  else 
{ /* Llib/weakhash.scm 310 */
return ((bool_t)0);} } } } } } 

}



/* &<@anonymous:1481> */
obj_t BGl_z62zc3z04anonymousza31481ze3ze5zz__weakhashz00(obj_t BgL_envz00_4467, obj_t BgL_vz00_4470)
{
{ /* Llib/weakhash.scm 314 */
{ /* Llib/weakhash.scm 315 */
 obj_t BgL_countz00_4468; obj_t BgL_funz00_4469;
BgL_countz00_4468 = 
PROCEDURE_REF(BgL_envz00_4467, 
(int)(0L)); 
BgL_funz00_4469 = 
PROCEDURE_REF(BgL_envz00_4467, 
(int)(1L)); 
{ /* Llib/weakhash.scm 315 */
 bool_t BgL_tmpz00_8327;
{ /* Llib/weakhash.scm 315 */
 bool_t BgL_test4093z00_8328;
{ /* Llib/weakhash.scm 315 */
 bool_t BgL_test4094z00_8329;
{ /* Llib/weakhash.scm 315 */
 obj_t BgL_arg1495z00_5733;
{ /* Llib/weakhash.scm 315 */
 obj_t BgL_objz00_5734;
if(
BGL_WEAKPTRP(BgL_vz00_4470))
{ /* Llib/weakhash.scm 315 */
BgL_objz00_5734 = BgL_vz00_4470; }  else 
{ 
 obj_t BgL_auxz00_8332;
BgL_auxz00_8332 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12154L), BGl_string3499z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_vz00_4470); 
FAILURE(BgL_auxz00_8332,BFALSE,BFALSE);} 
BgL_arg1495z00_5733 = 
bgl_weakptr_data(BgL_objz00_5734); } 
BgL_test4094z00_8329 = 
(BgL_arg1495z00_5733==BUNSPEC); } 
if(BgL_test4094z00_8329)
{ /* Llib/weakhash.scm 315 */
BgL_test4093z00_8328 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 316 */
 bool_t BgL_test4096z00_8338;
{ /* Llib/weakhash.scm 316 */
 obj_t BgL_arg1492z00_5735; obj_t BgL_arg1494z00_5736;
{ /* Llib/weakhash.scm 316 */
 obj_t BgL_objz00_5737;
if(
BGL_WEAKPTRP(BgL_vz00_4470))
{ /* Llib/weakhash.scm 316 */
BgL_objz00_5737 = BgL_vz00_4470; }  else 
{ 
 obj_t BgL_auxz00_8341;
BgL_auxz00_8341 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12205L), BGl_string3499z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_vz00_4470); 
FAILURE(BgL_auxz00_8341,BFALSE,BFALSE);} 
BgL_arg1492z00_5735 = 
bgl_weakptr_data(BgL_objz00_5737); } 
{ /* Llib/weakhash.scm 316 */
 obj_t BgL_objz00_5738;
if(
BGL_WEAKPTRP(BgL_vz00_4470))
{ /* Llib/weakhash.scm 316 */
BgL_objz00_5738 = BgL_vz00_4470; }  else 
{ 
 obj_t BgL_auxz00_8348;
BgL_auxz00_8348 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12221L), BGl_string3499z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_vz00_4470); 
FAILURE(BgL_auxz00_8348,BFALSE,BFALSE);} 
BgL_arg1494z00_5736 = 
bgl_weakptr_ref(BgL_objz00_5738); } 
{ /* Llib/weakhash.scm 316 */
 obj_t BgL_funz00_5739;
if(
PROCEDUREP(BgL_funz00_4469))
{ /* Llib/weakhash.scm 316 */
BgL_funz00_5739 = BgL_funz00_4469; }  else 
{ 
 obj_t BgL_auxz00_8355;
BgL_auxz00_8355 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12186L), BGl_string3499z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_4469); 
FAILURE(BgL_auxz00_8355,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5739, 2))
{ /* Llib/weakhash.scm 316 */
BgL_test4096z00_8338 = 
CBOOL(
(VA_PROCEDUREP( BgL_funz00_5739 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5739))(BgL_funz00_4469, BgL_arg1492z00_5735, BgL_arg1494z00_5736, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5739))(BgL_funz00_4469, BgL_arg1492z00_5735, BgL_arg1494z00_5736) ))
; }  else 
{ /* Llib/weakhash.scm 316 */
FAILURE(BGl_string3500z00zz__weakhashz00,BGl_list3501z00zz__weakhashz00,BgL_funz00_5739);} } } 
if(BgL_test4096z00_8338)
{ /* Llib/weakhash.scm 316 */
BgL_test4093z00_8328 = ((bool_t)0)
; }  else 
{ /* Llib/weakhash.scm 316 */
BgL_test4093z00_8328 = ((bool_t)1)
; } } } 
if(BgL_test4093z00_8328)
{ /* Llib/weakhash.scm 315 */
{ /* Llib/weakhash.scm 318 */
 obj_t BgL_auxz00_5740;
{ /* Llib/weakhash.scm 315 */
 obj_t BgL_tmpz00_8368;
{ /* Llib/weakhash.scm 318 */
 obj_t BgL_aux2942z00_5741;
BgL_aux2942z00_5741 = 
CELL_REF(BgL_countz00_4468); 
if(
INTEGERP(BgL_aux2942z00_5741))
{ /* Llib/weakhash.scm 318 */
BgL_tmpz00_8368 = BgL_aux2942z00_5741
; }  else 
{ 
 obj_t BgL_auxz00_8371;
BgL_auxz00_8371 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12266L), BGl_string3499z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux2942z00_5741); 
FAILURE(BgL_auxz00_8371,BFALSE,BFALSE);} } 
BgL_auxz00_5740 = 
ADDFX(BgL_tmpz00_8368, 
BINT(1L)); } 
CELL_SET(BgL_countz00_4468, BgL_auxz00_5740); } 
BgL_tmpz00_8327 = ((bool_t)0); }  else 
{ /* Llib/weakhash.scm 315 */
BgL_tmpz00_8327 = ((bool_t)1)
; } } 
return 
BBOOL(BgL_tmpz00_8327);} } } 

}



/* weak-old-hashtable-filter! */
bool_t BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00(obj_t BgL_tablez00_66, obj_t BgL_funz00_67)
{
{ /* Llib/weakhash.scm 328 */
{ /* Llib/weakhash.scm 329 */
 obj_t BgL_bucketsz00_1586;
{ /* Llib/weakhash.scm 329 */
 bool_t BgL_test4102z00_8378;
{ /* Llib/weakhash.scm 329 */
 obj_t BgL_tmpz00_8379;
{ /* Llib/weakhash.scm 329 */
 obj_t BgL_res2246z00_3117;
{ /* Llib/weakhash.scm 329 */
 obj_t BgL_aux2943z00_5184;
BgL_aux2943z00_5184 = 
STRUCT_KEY(BgL_tablez00_66); 
if(
SYMBOLP(BgL_aux2943z00_5184))
{ /* Llib/weakhash.scm 329 */
BgL_res2246z00_3117 = BgL_aux2943z00_5184; }  else 
{ 
 obj_t BgL_auxz00_8383;
BgL_auxz00_8383 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12727L), BGl_string3506z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2943z00_5184); 
FAILURE(BgL_auxz00_8383,BFALSE,BFALSE);} } 
BgL_tmpz00_8379 = BgL_res2246z00_3117; } 
BgL_test4102z00_8378 = 
(BgL_tmpz00_8379==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4102z00_8378)
{ /* Llib/weakhash.scm 329 */
 int BgL_tmpz00_8388;
BgL_tmpz00_8388 = 
(int)(2L); 
BgL_bucketsz00_1586 = 
STRUCT_REF(BgL_tablez00_66, BgL_tmpz00_8388); }  else 
{ /* Llib/weakhash.scm 329 */
BgL_bucketsz00_1586 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_66); } } 
{ /* Llib/weakhash.scm 329 */
 long BgL_bucketszd2lenzd2_1587;
{ /* Llib/weakhash.scm 330 */
 obj_t BgL_vectorz00_3118;
if(
VECTORP(BgL_bucketsz00_1586))
{ /* Llib/weakhash.scm 330 */
BgL_vectorz00_3118 = BgL_bucketsz00_1586; }  else 
{ 
 obj_t BgL_auxz00_8394;
BgL_auxz00_8394 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12786L), BGl_string3506z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1586); 
FAILURE(BgL_auxz00_8394,BFALSE,BFALSE);} 
BgL_bucketszd2lenzd2_1587 = 
VECTOR_LENGTH(BgL_vectorz00_3118); } 
{ /* Llib/weakhash.scm 330 */

{ 
 long BgL_iz00_1589;
BgL_iz00_1589 = 0L; 
BgL_zc3z04anonymousza31500ze3z87_1590:
if(
(BgL_iz00_1589<BgL_bucketszd2lenzd2_1587))
{ /* Llib/weakhash.scm 332 */
{ /* Llib/weakhash.scm 336 */
 obj_t BgL_zc3z04anonymousza31503ze3z87_4474;
{ 
 int BgL_tmpz00_8401;
BgL_tmpz00_8401 = 
(int)(1L); 
BgL_zc3z04anonymousza31503ze3z87_4474 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31503ze3ze5zz__weakhashz00, BgL_tmpz00_8401); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31503ze3z87_4474, 
(int)(0L), 
((obj_t)BgL_funz00_67)); 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_66, BgL_bucketsz00_1586, BgL_iz00_1589, BgL_zc3z04anonymousza31503ze3z87_4474); } 
{ 
 long BgL_iz00_8408;
BgL_iz00_8408 = 
(BgL_iz00_1589+1L); 
BgL_iz00_1589 = BgL_iz00_8408; 
goto BgL_zc3z04anonymousza31500ze3z87_1590;} }  else 
{ /* Llib/weakhash.scm 332 */
return ((bool_t)0);} } } } } } 

}



/* &<@anonymous:1503> */
obj_t BGl_z62zc3z04anonymousza31503ze3ze5zz__weakhashz00(obj_t BgL_envz00_4475, obj_t BgL_keyz00_4477, obj_t BgL_valz00_4478, obj_t BgL_bucketz00_4479)
{
{ /* Llib/weakhash.scm 335 */
{ /* Llib/weakhash.scm 336 */
 obj_t BgL_funz00_4476;
BgL_funz00_4476 = 
PROCEDURE_L_REF(BgL_envz00_4475, 
(int)(0L)); 
{ /* Llib/weakhash.scm 336 */
 bool_t BgL_test4106z00_8412;
{ /* Llib/weakhash.scm 336 */
 obj_t BgL_funz00_5742;
if(
PROCEDUREP(BgL_funz00_4476))
{ /* Llib/weakhash.scm 336 */
BgL_funz00_5742 = BgL_funz00_4476; }  else 
{ 
 obj_t BgL_auxz00_8415;
BgL_auxz00_8415 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(12938L), BGl_string3507z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_4476); 
FAILURE(BgL_auxz00_8415,BFALSE,BFALSE);} 
if(
PROCEDURE_CORRECT_ARITYP(BgL_funz00_5742, 2))
{ /* Llib/weakhash.scm 336 */
BgL_test4106z00_8412 = 
CBOOL(
(VA_PROCEDUREP( BgL_funz00_5742 ) ? ((obj_t (*)(obj_t, ...))PROCEDURE_ENTRY(BgL_funz00_5742))(BgL_funz00_4476, BgL_keyz00_4477, BgL_valz00_4478, BEOA) : ((obj_t (*)(obj_t, obj_t, obj_t))PROCEDURE_ENTRY(BgL_funz00_5742))(BgL_funz00_4476, BgL_keyz00_4477, BgL_valz00_4478) ))
; }  else 
{ /* Llib/weakhash.scm 336 */
FAILURE(BGl_string3508z00zz__weakhashz00,BGl_list3494z00zz__weakhashz00,BgL_funz00_5742);} } 
if(BgL_test4106z00_8412)
{ /* Llib/weakhash.scm 336 */
return BGl_keepgoingz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 336 */
return BGl_removez00zz__weakhashz00;} } } } 

}



/* weak-hashtable-filter! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(obj_t BgL_tablez00_68, obj_t BgL_funz00_69)
{
{ /* Llib/weakhash.scm 344 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_68))
{ /* Llib/weakhash.scm 345 */
return 
BBOOL(
BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(BgL_tablez00_68, BgL_funz00_69));}  else 
{ /* Llib/weakhash.scm 345 */
return 
BBOOL(
BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00(BgL_tablez00_68, BgL_funz00_69));} } 

}



/* &weak-hashtable-filter! */
obj_t BGl_z62weakzd2hashtablezd2filterz12z70zz__weakhashz00(obj_t BgL_envz00_4480, obj_t BgL_tablez00_4481, obj_t BgL_funz00_4482)
{
{ /* Llib/weakhash.scm 344 */
{ /* Llib/weakhash.scm 345 */
 obj_t BgL_auxz00_8441; obj_t BgL_auxz00_8434;
if(
PROCEDUREP(BgL_funz00_4482))
{ /* Llib/weakhash.scm 345 */
BgL_auxz00_8441 = BgL_funz00_4482
; }  else 
{ 
 obj_t BgL_auxz00_8444;
BgL_auxz00_8444 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(13294L), BGl_string3509z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_funz00_4482); 
FAILURE(BgL_auxz00_8444,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_4481))
{ /* Llib/weakhash.scm 345 */
BgL_auxz00_8434 = BgL_tablez00_4481
; }  else 
{ 
 obj_t BgL_auxz00_8437;
BgL_auxz00_8437 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(13294L), BGl_string3509z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4481); 
FAILURE(BgL_auxz00_8437,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2filterz12z12zz__weakhashz00(BgL_auxz00_8434, BgL_auxz00_8441);} } 

}



/* weak-hashtable-clear! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(obj_t BgL_tablez00_70)
{
{ /* Llib/weakhash.scm 352 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_70))
{ /* Llib/weakhash.scm 353 */
return 
BBOOL(
BGl_weakzd2keyszd2hashtablezd2filterz12zc0zz__weakhashz00(BgL_tablez00_70, BGl_proc3510z00zz__weakhashz00));}  else 
{ /* Llib/weakhash.scm 353 */
return 
BBOOL(
BGl_weakzd2oldzd2hashtablezd2filterz12zc0zz__weakhashz00(BgL_tablez00_70, BGl_proc3511z00zz__weakhashz00));} } 

}



/* &weak-hashtable-clear! */
obj_t BGl_z62weakzd2hashtablezd2clearz12z70zz__weakhashz00(obj_t BgL_envz00_4485, obj_t BgL_tablez00_4486)
{
{ /* Llib/weakhash.scm 352 */
{ /* Llib/weakhash.scm 353 */
 obj_t BgL_auxz00_8455;
if(
STRUCTP(BgL_tablez00_4486))
{ /* Llib/weakhash.scm 353 */
BgL_auxz00_8455 = BgL_tablez00_4486
; }  else 
{ 
 obj_t BgL_auxz00_8458;
BgL_auxz00_8458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(13701L), BGl_string3512z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4486); 
FAILURE(BgL_auxz00_8458,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2clearz12z12zz__weakhashz00(BgL_auxz00_8455);} } 

}



/* &<@anonymous:1509> */
obj_t BGl_z62zc3z04anonymousza31509ze3ze5zz__weakhashz00(obj_t BgL_envz00_4487, obj_t BgL_kz00_4488, obj_t BgL_vz00_4489)
{
{ /* Llib/weakhash.scm 354 */
return 
BBOOL(((bool_t)0));} 

}



/* &<@anonymous:1511> */
obj_t BGl_z62zc3z04anonymousza31511ze3ze5zz__weakhashz00(obj_t BgL_envz00_4490, obj_t BgL_kz00_4491, obj_t BgL_vz00_4492)
{
{ /* Llib/weakhash.scm 355 */
return 
BBOOL(((bool_t)0));} 

}



/* weak-keys-hashtable-contains? */
bool_t BGl_weakzd2keyszd2hashtablezd2containszf3z21zz__weakhashz00(obj_t BgL_tablez00_71, obj_t BgL_keyz00_72)
{
{ /* Llib/weakhash.scm 360 */
{ /* Llib/weakhash.scm 361 */
 obj_t BgL_bucketsz00_1613;
{ /* Llib/weakhash.scm 361 */
 bool_t BgL_test4114z00_8465;
{ /* Llib/weakhash.scm 361 */
 obj_t BgL_tmpz00_8466;
{ /* Llib/weakhash.scm 361 */
 obj_t BgL_res2247z00_3127;
{ /* Llib/weakhash.scm 361 */
 obj_t BgL_aux2956z00_5198;
BgL_aux2956z00_5198 = 
STRUCT_KEY(BgL_tablez00_71); 
if(
SYMBOLP(BgL_aux2956z00_5198))
{ /* Llib/weakhash.scm 361 */
BgL_res2247z00_3127 = BgL_aux2956z00_5198; }  else 
{ 
 obj_t BgL_auxz00_8470;
BgL_auxz00_8470 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14162L), BGl_string3513z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2956z00_5198); 
FAILURE(BgL_auxz00_8470,BFALSE,BFALSE);} } 
BgL_tmpz00_8466 = BgL_res2247z00_3127; } 
BgL_test4114z00_8465 = 
(BgL_tmpz00_8466==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4114z00_8465)
{ /* Llib/weakhash.scm 361 */
 int BgL_tmpz00_8475;
BgL_tmpz00_8475 = 
(int)(2L); 
BgL_bucketsz00_1613 = 
STRUCT_REF(BgL_tablez00_71, BgL_tmpz00_8475); }  else 
{ /* Llib/weakhash.scm 361 */
BgL_bucketsz00_1613 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_71); } } 
{ /* Llib/weakhash.scm 361 */
 long BgL_bucketzd2lenzd2_1614;
{ /* Llib/weakhash.scm 362 */
 obj_t BgL_vectorz00_3128;
if(
VECTORP(BgL_bucketsz00_1613))
{ /* Llib/weakhash.scm 362 */
BgL_vectorz00_3128 = BgL_bucketsz00_1613; }  else 
{ 
 obj_t BgL_auxz00_8481;
BgL_auxz00_8481 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14220L), BGl_string3513z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1613); 
FAILURE(BgL_auxz00_8481,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1614 = 
VECTOR_LENGTH(BgL_vectorz00_3128); } 
{ /* Llib/weakhash.scm 362 */
 long BgL_bucketzd2numzd2_1615;
{ /* Llib/weakhash.scm 363 */
 long BgL_arg1524z00_1628;
BgL_arg1524z00_1628 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_71, BgL_keyz00_72); 
{ /* Llib/weakhash.scm 363 */
 long BgL_n1z00_3129; long BgL_n2z00_3130;
BgL_n1z00_3129 = BgL_arg1524z00_1628; 
BgL_n2z00_3130 = BgL_bucketzd2lenzd2_1614; 
{ /* Llib/weakhash.scm 363 */
 bool_t BgL_test4117z00_8487;
{ /* Llib/weakhash.scm 363 */
 long BgL_arg2024z00_3132;
BgL_arg2024z00_3132 = 
(((BgL_n1z00_3129) | (BgL_n2z00_3130)) & -2147483648); 
BgL_test4117z00_8487 = 
(BgL_arg2024z00_3132==0L); } 
if(BgL_test4117z00_8487)
{ /* Llib/weakhash.scm 363 */
 int32_t BgL_arg2020z00_3133;
{ /* Llib/weakhash.scm 363 */
 int32_t BgL_arg2021z00_3134; int32_t BgL_arg2022z00_3135;
BgL_arg2021z00_3134 = 
(int32_t)(BgL_n1z00_3129); 
BgL_arg2022z00_3135 = 
(int32_t)(BgL_n2z00_3130); 
BgL_arg2020z00_3133 = 
(BgL_arg2021z00_3134%BgL_arg2022z00_3135); } 
{ /* Llib/weakhash.scm 363 */
 long BgL_arg2135z00_3140;
BgL_arg2135z00_3140 = 
(long)(BgL_arg2020z00_3133); 
BgL_bucketzd2numzd2_1615 = 
(long)(BgL_arg2135z00_3140); } }  else 
{ /* Llib/weakhash.scm 363 */
BgL_bucketzd2numzd2_1615 = 
(BgL_n1z00_3129%BgL_n2z00_3130); } } } } 
{ /* Llib/weakhash.scm 363 */
 obj_t BgL_bucketz00_1616;
{ /* Llib/weakhash.scm 364 */
 obj_t BgL_vectorz00_3142;
if(
VECTORP(BgL_bucketsz00_1613))
{ /* Llib/weakhash.scm 364 */
BgL_vectorz00_3142 = BgL_bucketsz00_1613; }  else 
{ 
 obj_t BgL_auxz00_8498;
BgL_auxz00_8498 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14327L), BGl_string3513z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1613); 
FAILURE(BgL_auxz00_8498,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 364 */
 bool_t BgL_test4119z00_8502;
{ /* Llib/weakhash.scm 364 */
 long BgL_tmpz00_8503;
BgL_tmpz00_8503 = 
VECTOR_LENGTH(BgL_vectorz00_3142); 
BgL_test4119z00_8502 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1615, BgL_tmpz00_8503); } 
if(BgL_test4119z00_8502)
{ /* Llib/weakhash.scm 364 */
BgL_bucketz00_1616 = 
VECTOR_REF(BgL_vectorz00_3142,BgL_bucketzd2numzd2_1615); }  else 
{ 
 obj_t BgL_auxz00_8507;
BgL_auxz00_8507 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14315L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3142, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3142)), 
(int)(BgL_bucketzd2numzd2_1615)); 
FAILURE(BgL_auxz00_8507,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 364 */

{ 
 obj_t BgL_bucketz00_1618;
BgL_bucketz00_1618 = BgL_bucketz00_1616; 
BgL_zc3z04anonymousza31512ze3z87_1619:
if(
NULLP(BgL_bucketz00_1618))
{ /* Llib/weakhash.scm 367 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 369 */
 bool_t BgL_test4121z00_8516;
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_arg1522z00_1625;
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_arg1523z00_1626;
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_pairz00_3144;
if(
PAIRP(BgL_bucketz00_1618))
{ /* Llib/weakhash.scm 369 */
BgL_pairz00_3144 = BgL_bucketz00_1618; }  else 
{ 
 obj_t BgL_auxz00_8519;
BgL_auxz00_8519 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14471L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1618); 
FAILURE(BgL_auxz00_8519,BFALSE,BFALSE);} 
BgL_arg1523z00_1626 = 
CAR(BgL_pairz00_3144); } 
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_objz00_3145;
if(
BGL_WEAKPTRP(BgL_arg1523z00_1626))
{ /* Llib/weakhash.scm 369 */
BgL_objz00_3145 = BgL_arg1523z00_1626; }  else 
{ 
 obj_t BgL_auxz00_8526;
BgL_auxz00_8526 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14477L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1523z00_1626); 
FAILURE(BgL_auxz00_8526,BFALSE,BFALSE);} 
BgL_arg1522z00_1625 = 
bgl_weakptr_data(BgL_objz00_3145); } } 
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_eqtz00_3146;
{ /* Llib/weakhash.scm 369 */
 bool_t BgL_test4124z00_8531;
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_tmpz00_8532;
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_res2248z00_3153;
{ /* Llib/weakhash.scm 369 */
 obj_t BgL_aux2966z00_5208;
BgL_aux2966z00_5208 = 
STRUCT_KEY(BgL_tablez00_71); 
if(
SYMBOLP(BgL_aux2966z00_5208))
{ /* Llib/weakhash.scm 369 */
BgL_res2248z00_3153 = BgL_aux2966z00_5208; }  else 
{ 
 obj_t BgL_auxz00_8536;
BgL_auxz00_8536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14428L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2966z00_5208); 
FAILURE(BgL_auxz00_8536,BFALSE,BFALSE);} } 
BgL_tmpz00_8532 = BgL_res2248z00_3153; } 
BgL_test4124z00_8531 = 
(BgL_tmpz00_8532==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4124z00_8531)
{ /* Llib/weakhash.scm 369 */
 int BgL_tmpz00_8541;
BgL_tmpz00_8541 = 
(int)(3L); 
BgL_eqtz00_3146 = 
STRUCT_REF(BgL_tablez00_71, BgL_tmpz00_8541); }  else 
{ /* Llib/weakhash.scm 369 */
BgL_eqtz00_3146 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_71); } } 
if(
PROCEDUREP(BgL_eqtz00_3146))
{ /* Llib/weakhash.scm 369 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3146, 2))
{ /* Llib/weakhash.scm 369 */
BgL_test4121z00_8516 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3146, BgL_arg1522z00_1625, BgL_keyz00_72))
; }  else 
{ /* Llib/weakhash.scm 369 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3515z00zz__weakhashz00,BgL_eqtz00_3146);} }  else 
{ /* Llib/weakhash.scm 369 */
if(
(BgL_arg1522z00_1625==BgL_keyz00_72))
{ /* Llib/weakhash.scm 369 */
BgL_test4121z00_8516 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 369 */
if(
STRINGP(BgL_arg1522z00_1625))
{ /* Llib/weakhash.scm 369 */
if(
STRINGP(BgL_keyz00_72))
{ /* Llib/weakhash.scm 369 */
 long BgL_l1z00_3156;
BgL_l1z00_3156 = 
STRING_LENGTH(BgL_arg1522z00_1625); 
if(
(BgL_l1z00_3156==
STRING_LENGTH(BgL_keyz00_72)))
{ /* Llib/weakhash.scm 369 */
 int BgL_arg1918z00_3159;
{ /* Llib/weakhash.scm 369 */
 char * BgL_auxz00_8568; char * BgL_tmpz00_8566;
BgL_auxz00_8568 = 
BSTRING_TO_STRING(BgL_keyz00_72); 
BgL_tmpz00_8566 = 
BSTRING_TO_STRING(BgL_arg1522z00_1625); 
BgL_arg1918z00_3159 = 
memcmp(BgL_tmpz00_8566, BgL_auxz00_8568, BgL_l1z00_3156); } 
BgL_test4121z00_8516 = 
(
(long)(BgL_arg1918z00_3159)==0L); }  else 
{ /* Llib/weakhash.scm 369 */
BgL_test4121z00_8516 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 369 */
BgL_test4121z00_8516 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 369 */
BgL_test4121z00_8516 = ((bool_t)0)
; } } } } } 
if(BgL_test4121z00_8516)
{ /* Llib/weakhash.scm 369 */
return ((bool_t)1);}  else 
{ /* Llib/weakhash.scm 372 */
 obj_t BgL_arg1521z00_1624;
{ /* Llib/weakhash.scm 372 */
 obj_t BgL_pairz00_3165;
if(
PAIRP(BgL_bucketz00_1618))
{ /* Llib/weakhash.scm 372 */
BgL_pairz00_3165 = BgL_bucketz00_1618; }  else 
{ 
 obj_t BgL_auxz00_8575;
BgL_auxz00_8575 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14523L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1618); 
FAILURE(BgL_auxz00_8575,BFALSE,BFALSE);} 
BgL_arg1521z00_1624 = 
CDR(BgL_pairz00_3165); } 
{ 
 obj_t BgL_bucketz00_8580;
BgL_bucketz00_8580 = BgL_arg1521z00_1624; 
BgL_bucketz00_1618 = BgL_bucketz00_8580; 
goto BgL_zc3z04anonymousza31512ze3z87_1619;} } } } } } } } } } 

}



/* weak-old-hashtable-contains? */
bool_t BGl_weakzd2oldzd2hashtablezd2containszf3z21zz__weakhashz00(obj_t BgL_tablez00_73, obj_t BgL_keyz00_74)
{
{ /* Llib/weakhash.scm 377 */
{ /* Llib/weakhash.scm 378 */
 obj_t BgL_bucketsz00_1629;
{ /* Llib/weakhash.scm 378 */
 bool_t BgL_test4133z00_8581;
{ /* Llib/weakhash.scm 378 */
 obj_t BgL_tmpz00_8582;
{ /* Llib/weakhash.scm 378 */
 obj_t BgL_res2249z00_3169;
{ /* Llib/weakhash.scm 378 */
 obj_t BgL_aux2971z00_5214;
BgL_aux2971z00_5214 = 
STRUCT_KEY(BgL_tablez00_73); 
if(
SYMBOLP(BgL_aux2971z00_5214))
{ /* Llib/weakhash.scm 378 */
BgL_res2249z00_3169 = BgL_aux2971z00_5214; }  else 
{ 
 obj_t BgL_auxz00_8586;
BgL_auxz00_8586 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14841L), BGl_string3520z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2971z00_5214); 
FAILURE(BgL_auxz00_8586,BFALSE,BFALSE);} } 
BgL_tmpz00_8582 = BgL_res2249z00_3169; } 
BgL_test4133z00_8581 = 
(BgL_tmpz00_8582==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4133z00_8581)
{ /* Llib/weakhash.scm 378 */
 int BgL_tmpz00_8591;
BgL_tmpz00_8591 = 
(int)(2L); 
BgL_bucketsz00_1629 = 
STRUCT_REF(BgL_tablez00_73, BgL_tmpz00_8591); }  else 
{ /* Llib/weakhash.scm 378 */
BgL_bucketsz00_1629 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_73); } } 
{ /* Llib/weakhash.scm 378 */
 long BgL_bucketzd2lenzd2_1630;
{ /* Llib/weakhash.scm 379 */
 obj_t BgL_vectorz00_3170;
if(
VECTORP(BgL_bucketsz00_1629))
{ /* Llib/weakhash.scm 379 */
BgL_vectorz00_3170 = BgL_bucketsz00_1629; }  else 
{ 
 obj_t BgL_auxz00_8597;
BgL_auxz00_8597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(14899L), BGl_string3520z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1629); 
FAILURE(BgL_auxz00_8597,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1630 = 
VECTOR_LENGTH(BgL_vectorz00_3170); } 
{ /* Llib/weakhash.scm 379 */
 long BgL_bucketzd2numzd2_1631;
{ /* Llib/weakhash.scm 380 */
 long BgL_arg1528z00_1640;
BgL_arg1528z00_1640 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_73, BgL_keyz00_74); 
{ /* Llib/weakhash.scm 380 */
 long BgL_n1z00_3171; long BgL_n2z00_3172;
BgL_n1z00_3171 = BgL_arg1528z00_1640; 
BgL_n2z00_3172 = BgL_bucketzd2lenzd2_1630; 
{ /* Llib/weakhash.scm 380 */
 bool_t BgL_test4136z00_8603;
{ /* Llib/weakhash.scm 380 */
 long BgL_arg2024z00_3174;
BgL_arg2024z00_3174 = 
(((BgL_n1z00_3171) | (BgL_n2z00_3172)) & -2147483648); 
BgL_test4136z00_8603 = 
(BgL_arg2024z00_3174==0L); } 
if(BgL_test4136z00_8603)
{ /* Llib/weakhash.scm 380 */
 int32_t BgL_arg2020z00_3175;
{ /* Llib/weakhash.scm 380 */
 int32_t BgL_arg2021z00_3176; int32_t BgL_arg2022z00_3177;
BgL_arg2021z00_3176 = 
(int32_t)(BgL_n1z00_3171); 
BgL_arg2022z00_3177 = 
(int32_t)(BgL_n2z00_3172); 
BgL_arg2020z00_3175 = 
(BgL_arg2021z00_3176%BgL_arg2022z00_3177); } 
{ /* Llib/weakhash.scm 380 */
 long BgL_arg2135z00_3182;
BgL_arg2135z00_3182 = 
(long)(BgL_arg2020z00_3175); 
BgL_bucketzd2numzd2_1631 = 
(long)(BgL_arg2135z00_3182); } }  else 
{ /* Llib/weakhash.scm 380 */
BgL_bucketzd2numzd2_1631 = 
(BgL_n1z00_3171%BgL_n2z00_3172); } } } } 
{ /* Llib/weakhash.scm 380 */
 obj_t BgL_retz00_1632;
{ /* Llib/weakhash.scm 383 */
 obj_t BgL_zc3z04anonymousza31526ze3z87_4493;
{ 
 int BgL_tmpz00_8612;
BgL_tmpz00_8612 = 
(int)(2L); 
BgL_zc3z04anonymousza31526ze3z87_4493 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31526ze3ze5zz__weakhashz00, BgL_tmpz00_8612); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31526ze3z87_4493, 
(int)(0L), BgL_tablez00_73); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31526ze3z87_4493, 
(int)(1L), BgL_keyz00_74); 
BgL_retz00_1632 = 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_73, BgL_bucketsz00_1629, BgL_bucketzd2numzd2_1631, BgL_zc3z04anonymousza31526ze3z87_4493); } 
{ /* Llib/weakhash.scm 381 */

if(
(BgL_retz00_1632==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 386 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 386 */
return ((bool_t)1);} } } } } } } 

}



/* &<@anonymous:1526> */
obj_t BGl_z62zc3z04anonymousza31526ze3ze5zz__weakhashz00(obj_t BgL_envz00_4494, obj_t BgL_bkeyz00_4497, obj_t BgL_valz00_4498, obj_t BgL_bucketz00_4499)
{
{ /* Llib/weakhash.scm 382 */
{ /* Llib/weakhash.scm 383 */
 obj_t BgL_tablez00_4495; obj_t BgL_keyz00_4496;
BgL_tablez00_4495 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4494, 
(int)(0L))); 
BgL_keyz00_4496 = 
PROCEDURE_L_REF(BgL_envz00_4494, 
(int)(1L)); 
{ /* Llib/weakhash.scm 383 */
 bool_t BgL_test4138z00_8627;
{ /* Llib/weakhash.scm 383 */
 obj_t BgL_eqtz00_5743;
{ /* Llib/weakhash.scm 383 */
 bool_t BgL_test4139z00_8628;
{ /* Llib/weakhash.scm 383 */
 obj_t BgL_tmpz00_8629;
{ /* Llib/weakhash.scm 383 */
 obj_t BgL_res2250z00_5744;
{ /* Llib/weakhash.scm 383 */
 obj_t BgL_aux2975z00_5745;
BgL_aux2975z00_5745 = 
STRUCT_KEY(BgL_tablez00_4495); 
if(
SYMBOLP(BgL_aux2975z00_5745))
{ /* Llib/weakhash.scm 383 */
BgL_res2250z00_5744 = BgL_aux2975z00_5745; }  else 
{ 
 obj_t BgL_auxz00_8633;
BgL_auxz00_8633 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(15075L), BGl_string3521z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2975z00_5745); 
FAILURE(BgL_auxz00_8633,BFALSE,BFALSE);} } 
BgL_tmpz00_8629 = BgL_res2250z00_5744; } 
BgL_test4139z00_8628 = 
(BgL_tmpz00_8629==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4139z00_8628)
{ /* Llib/weakhash.scm 383 */
 int BgL_tmpz00_8638;
BgL_tmpz00_8638 = 
(int)(3L); 
BgL_eqtz00_5743 = 
STRUCT_REF(BgL_tablez00_4495, BgL_tmpz00_8638); }  else 
{ /* Llib/weakhash.scm 383 */
BgL_eqtz00_5743 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_4495); } } 
if(
PROCEDUREP(BgL_eqtz00_5743))
{ /* Llib/weakhash.scm 383 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_5743, 2))
{ /* Llib/weakhash.scm 383 */
BgL_test4138z00_8627 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_5743, BgL_keyz00_4496, BgL_bkeyz00_4497))
; }  else 
{ /* Llib/weakhash.scm 383 */
FAILURE(BGl_string3522z00zz__weakhashz00,BGl_list3523z00zz__weakhashz00,BgL_eqtz00_5743);} }  else 
{ /* Llib/weakhash.scm 383 */
if(
(BgL_keyz00_4496==BgL_bkeyz00_4497))
{ /* Llib/weakhash.scm 383 */
BgL_test4138z00_8627 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 383 */
if(
STRINGP(BgL_keyz00_4496))
{ /* Llib/weakhash.scm 383 */
if(
STRINGP(BgL_bkeyz00_4497))
{ /* Llib/weakhash.scm 383 */
 long BgL_l1z00_5746;
BgL_l1z00_5746 = 
STRING_LENGTH(BgL_keyz00_4496); 
if(
(BgL_l1z00_5746==
STRING_LENGTH(BgL_bkeyz00_4497)))
{ /* Llib/weakhash.scm 383 */
 int BgL_arg1918z00_5747;
{ /* Llib/weakhash.scm 383 */
 char * BgL_auxz00_8665; char * BgL_tmpz00_8663;
BgL_auxz00_8665 = 
BSTRING_TO_STRING(BgL_bkeyz00_4497); 
BgL_tmpz00_8663 = 
BSTRING_TO_STRING(BgL_keyz00_4496); 
BgL_arg1918z00_5747 = 
memcmp(BgL_tmpz00_8663, BgL_auxz00_8665, BgL_l1z00_5746); } 
BgL_test4138z00_8627 = 
(
(long)(BgL_arg1918z00_5747)==0L); }  else 
{ /* Llib/weakhash.scm 383 */
BgL_test4138z00_8627 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 383 */
BgL_test4138z00_8627 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 383 */
BgL_test4138z00_8627 = ((bool_t)0)
; } } } } 
if(BgL_test4138z00_8627)
{ /* Llib/weakhash.scm 383 */
return BTRUE;}  else 
{ /* Llib/weakhash.scm 383 */
return BGl_keepgoingz00zz__weakhashz00;} } } } 

}



/* weak-hashtable-contains? */
BGL_EXPORTED_DEF bool_t BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(obj_t BgL_tablez00_75, obj_t BgL_keyz00_76)
{
{ /* Llib/weakhash.scm 391 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_75))
{ /* Llib/weakhash.scm 392 */
BGL_TAIL return 
BGl_weakzd2keyszd2hashtablezd2containszf3z21zz__weakhashz00(BgL_tablez00_75, BgL_keyz00_76);}  else 
{ /* Llib/weakhash.scm 392 */
BGL_TAIL return 
BGl_weakzd2oldzd2hashtablezd2containszf3z21zz__weakhashz00(BgL_tablez00_75, BgL_keyz00_76);} } 

}



/* &weak-hashtable-contains? */
obj_t BGl_z62weakzd2hashtablezd2containszf3z91zz__weakhashz00(obj_t BgL_envz00_4500, obj_t BgL_tablez00_4501, obj_t BgL_keyz00_4502)
{
{ /* Llib/weakhash.scm 391 */
{ /* Llib/weakhash.scm 392 */
 bool_t BgL_tmpz00_8674;
{ /* Llib/weakhash.scm 392 */
 obj_t BgL_auxz00_8675;
if(
STRUCTP(BgL_tablez00_4501))
{ /* Llib/weakhash.scm 392 */
BgL_auxz00_8675 = BgL_tablez00_4501
; }  else 
{ 
 obj_t BgL_auxz00_8678;
BgL_auxz00_8678 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(15453L), BGl_string3526z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4501); 
FAILURE(BgL_auxz00_8678,BFALSE,BFALSE);} 
BgL_tmpz00_8674 = 
BGl_weakzd2hashtablezd2containszf3zf3zz__weakhashz00(BgL_auxz00_8675, BgL_keyz00_4502); } 
return 
BBOOL(BgL_tmpz00_8674);} } 

}



/* weak-keys-hashtable-get */
obj_t BGl_weakzd2keyszd2hashtablezd2getzd2zz__weakhashz00(obj_t BgL_tablez00_77, obj_t BgL_keyz00_78)
{
{ /* Llib/weakhash.scm 399 */
{ /* Llib/weakhash.scm 400 */
 obj_t BgL_bucketsz00_1642;
{ /* Llib/weakhash.scm 400 */
 bool_t BgL_test4149z00_8684;
{ /* Llib/weakhash.scm 400 */
 obj_t BgL_tmpz00_8685;
{ /* Llib/weakhash.scm 400 */
 obj_t BgL_res2251z00_3208;
{ /* Llib/weakhash.scm 400 */
 obj_t BgL_aux2980z00_5224;
BgL_aux2980z00_5224 = 
STRUCT_KEY(BgL_tablez00_77); 
if(
SYMBOLP(BgL_aux2980z00_5224))
{ /* Llib/weakhash.scm 400 */
BgL_res2251z00_3208 = BgL_aux2980z00_5224; }  else 
{ 
 obj_t BgL_auxz00_8689;
BgL_auxz00_8689 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(15884L), BGl_string3527z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2980z00_5224); 
FAILURE(BgL_auxz00_8689,BFALSE,BFALSE);} } 
BgL_tmpz00_8685 = BgL_res2251z00_3208; } 
BgL_test4149z00_8684 = 
(BgL_tmpz00_8685==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4149z00_8684)
{ /* Llib/weakhash.scm 400 */
 int BgL_tmpz00_8694;
BgL_tmpz00_8694 = 
(int)(2L); 
BgL_bucketsz00_1642 = 
STRUCT_REF(BgL_tablez00_77, BgL_tmpz00_8694); }  else 
{ /* Llib/weakhash.scm 400 */
BgL_bucketsz00_1642 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_77); } } 
{ /* Llib/weakhash.scm 400 */
 long BgL_bucketzd2lenzd2_1643;
{ /* Llib/weakhash.scm 401 */
 obj_t BgL_vectorz00_3209;
if(
VECTORP(BgL_bucketsz00_1642))
{ /* Llib/weakhash.scm 401 */
BgL_vectorz00_3209 = BgL_bucketsz00_1642; }  else 
{ 
 obj_t BgL_auxz00_8700;
BgL_auxz00_8700 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(15942L), BGl_string3527z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1642); 
FAILURE(BgL_auxz00_8700,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1643 = 
VECTOR_LENGTH(BgL_vectorz00_3209); } 
{ /* Llib/weakhash.scm 401 */
 long BgL_bucketzd2numzd2_1644;
{ /* Llib/weakhash.scm 402 */
 long BgL_arg1546z00_1658;
BgL_arg1546z00_1658 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_77, BgL_keyz00_78); 
{ /* Llib/weakhash.scm 402 */
 long BgL_n1z00_3210; long BgL_n2z00_3211;
BgL_n1z00_3210 = BgL_arg1546z00_1658; 
BgL_n2z00_3211 = BgL_bucketzd2lenzd2_1643; 
{ /* Llib/weakhash.scm 402 */
 bool_t BgL_test4152z00_8706;
{ /* Llib/weakhash.scm 402 */
 long BgL_arg2024z00_3213;
BgL_arg2024z00_3213 = 
(((BgL_n1z00_3210) | (BgL_n2z00_3211)) & -2147483648); 
BgL_test4152z00_8706 = 
(BgL_arg2024z00_3213==0L); } 
if(BgL_test4152z00_8706)
{ /* Llib/weakhash.scm 402 */
 int32_t BgL_arg2020z00_3214;
{ /* Llib/weakhash.scm 402 */
 int32_t BgL_arg2021z00_3215; int32_t BgL_arg2022z00_3216;
BgL_arg2021z00_3215 = 
(int32_t)(BgL_n1z00_3210); 
BgL_arg2022z00_3216 = 
(int32_t)(BgL_n2z00_3211); 
BgL_arg2020z00_3214 = 
(BgL_arg2021z00_3215%BgL_arg2022z00_3216); } 
{ /* Llib/weakhash.scm 402 */
 long BgL_arg2135z00_3221;
BgL_arg2135z00_3221 = 
(long)(BgL_arg2020z00_3214); 
BgL_bucketzd2numzd2_1644 = 
(long)(BgL_arg2135z00_3221); } }  else 
{ /* Llib/weakhash.scm 402 */
BgL_bucketzd2numzd2_1644 = 
(BgL_n1z00_3210%BgL_n2z00_3211); } } } } 
{ /* Llib/weakhash.scm 402 */
 obj_t BgL_bucketz00_1645;
{ /* Llib/weakhash.scm 403 */
 obj_t BgL_vectorz00_3223;
if(
VECTORP(BgL_bucketsz00_1642))
{ /* Llib/weakhash.scm 403 */
BgL_vectorz00_3223 = BgL_bucketsz00_1642; }  else 
{ 
 obj_t BgL_auxz00_8717;
BgL_auxz00_8717 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16049L), BGl_string3527z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1642); 
FAILURE(BgL_auxz00_8717,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 403 */
 bool_t BgL_test4154z00_8721;
{ /* Llib/weakhash.scm 403 */
 long BgL_tmpz00_8722;
BgL_tmpz00_8722 = 
VECTOR_LENGTH(BgL_vectorz00_3223); 
BgL_test4154z00_8721 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1644, BgL_tmpz00_8722); } 
if(BgL_test4154z00_8721)
{ /* Llib/weakhash.scm 403 */
BgL_bucketz00_1645 = 
VECTOR_REF(BgL_vectorz00_3223,BgL_bucketzd2numzd2_1644); }  else 
{ 
 obj_t BgL_auxz00_8726;
BgL_auxz00_8726 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16037L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3223, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3223)), 
(int)(BgL_bucketzd2numzd2_1644)); 
FAILURE(BgL_auxz00_8726,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 403 */

{ 
 obj_t BgL_bucketz00_1647;
BgL_bucketz00_1647 = BgL_bucketz00_1645; 
BgL_zc3z04anonymousza31530ze3z87_1648:
if(
NULLP(BgL_bucketz00_1647))
{ /* Llib/weakhash.scm 406 */
return BFALSE;}  else 
{ /* Llib/weakhash.scm 408 */
 bool_t BgL_test4156z00_8735;
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_arg1543z00_1655;
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_arg1544z00_1656;
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_pairz00_3225;
if(
PAIRP(BgL_bucketz00_1647))
{ /* Llib/weakhash.scm 408 */
BgL_pairz00_3225 = BgL_bucketz00_1647; }  else 
{ 
 obj_t BgL_auxz00_8738;
BgL_auxz00_8738 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16193L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1647); 
FAILURE(BgL_auxz00_8738,BFALSE,BFALSE);} 
BgL_arg1544z00_1656 = 
CAR(BgL_pairz00_3225); } 
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_objz00_3226;
if(
BGL_WEAKPTRP(BgL_arg1544z00_1656))
{ /* Llib/weakhash.scm 408 */
BgL_objz00_3226 = BgL_arg1544z00_1656; }  else 
{ 
 obj_t BgL_auxz00_8745;
BgL_auxz00_8745 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16199L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1544z00_1656); 
FAILURE(BgL_auxz00_8745,BFALSE,BFALSE);} 
BgL_arg1543z00_1655 = 
bgl_weakptr_data(BgL_objz00_3226); } } 
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_eqtz00_3227;
{ /* Llib/weakhash.scm 408 */
 bool_t BgL_test4159z00_8750;
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_tmpz00_8751;
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_res2252z00_3234;
{ /* Llib/weakhash.scm 408 */
 obj_t BgL_aux2990z00_5234;
BgL_aux2990z00_5234 = 
STRUCT_KEY(BgL_tablez00_77); 
if(
SYMBOLP(BgL_aux2990z00_5234))
{ /* Llib/weakhash.scm 408 */
BgL_res2252z00_3234 = BgL_aux2990z00_5234; }  else 
{ 
 obj_t BgL_auxz00_8755;
BgL_auxz00_8755 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16150L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2990z00_5234); 
FAILURE(BgL_auxz00_8755,BFALSE,BFALSE);} } 
BgL_tmpz00_8751 = BgL_res2252z00_3234; } 
BgL_test4159z00_8750 = 
(BgL_tmpz00_8751==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4159z00_8750)
{ /* Llib/weakhash.scm 408 */
 int BgL_tmpz00_8760;
BgL_tmpz00_8760 = 
(int)(3L); 
BgL_eqtz00_3227 = 
STRUCT_REF(BgL_tablez00_77, BgL_tmpz00_8760); }  else 
{ /* Llib/weakhash.scm 408 */
BgL_eqtz00_3227 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_77); } } 
if(
PROCEDUREP(BgL_eqtz00_3227))
{ /* Llib/weakhash.scm 408 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3227, 2))
{ /* Llib/weakhash.scm 408 */
BgL_test4156z00_8735 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3227, BgL_arg1543z00_1655, BgL_keyz00_78))
; }  else 
{ /* Llib/weakhash.scm 408 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3528z00zz__weakhashz00,BgL_eqtz00_3227);} }  else 
{ /* Llib/weakhash.scm 408 */
if(
(BgL_arg1543z00_1655==BgL_keyz00_78))
{ /* Llib/weakhash.scm 408 */
BgL_test4156z00_8735 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 408 */
if(
STRINGP(BgL_arg1543z00_1655))
{ /* Llib/weakhash.scm 408 */
if(
STRINGP(BgL_keyz00_78))
{ /* Llib/weakhash.scm 408 */
 long BgL_l1z00_3237;
BgL_l1z00_3237 = 
STRING_LENGTH(BgL_arg1543z00_1655); 
if(
(BgL_l1z00_3237==
STRING_LENGTH(BgL_keyz00_78)))
{ /* Llib/weakhash.scm 408 */
 int BgL_arg1918z00_3240;
{ /* Llib/weakhash.scm 408 */
 char * BgL_auxz00_8787; char * BgL_tmpz00_8785;
BgL_auxz00_8787 = 
BSTRING_TO_STRING(BgL_keyz00_78); 
BgL_tmpz00_8785 = 
BSTRING_TO_STRING(BgL_arg1543z00_1655); 
BgL_arg1918z00_3240 = 
memcmp(BgL_tmpz00_8785, BgL_auxz00_8787, BgL_l1z00_3237); } 
BgL_test4156z00_8735 = 
(
(long)(BgL_arg1918z00_3240)==0L); }  else 
{ /* Llib/weakhash.scm 408 */
BgL_test4156z00_8735 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 408 */
BgL_test4156z00_8735 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 408 */
BgL_test4156z00_8735 = ((bool_t)0)
; } } } } } 
if(BgL_test4156z00_8735)
{ /* Llib/weakhash.scm 409 */
 obj_t BgL_arg1539z00_1653;
{ /* Llib/weakhash.scm 409 */
 obj_t BgL_pairz00_3246;
if(
PAIRP(BgL_bucketz00_1647))
{ /* Llib/weakhash.scm 409 */
BgL_pairz00_3246 = BgL_bucketz00_1647; }  else 
{ 
 obj_t BgL_auxz00_8794;
BgL_auxz00_8794 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16231L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1647); 
FAILURE(BgL_auxz00_8794,BFALSE,BFALSE);} 
BgL_arg1539z00_1653 = 
CAR(BgL_pairz00_3246); } 
{ /* Llib/weakhash.scm 409 */
 obj_t BgL_objz00_3247;
if(
BGL_WEAKPTRP(BgL_arg1539z00_1653))
{ /* Llib/weakhash.scm 409 */
BgL_objz00_3247 = BgL_arg1539z00_1653; }  else 
{ 
 obj_t BgL_auxz00_8801;
BgL_auxz00_8801 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16237L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1539z00_1653); 
FAILURE(BgL_auxz00_8801,BFALSE,BFALSE);} 
return 
bgl_weakptr_ref(BgL_objz00_3247);} }  else 
{ /* Llib/weakhash.scm 411 */
 obj_t BgL_arg1540z00_1654;
{ /* Llib/weakhash.scm 411 */
 obj_t BgL_pairz00_3248;
if(
PAIRP(BgL_bucketz00_1647))
{ /* Llib/weakhash.scm 411 */
BgL_pairz00_3248 = BgL_bucketz00_1647; }  else 
{ 
 obj_t BgL_auxz00_8808;
BgL_auxz00_8808 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16269L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1647); 
FAILURE(BgL_auxz00_8808,BFALSE,BFALSE);} 
BgL_arg1540z00_1654 = 
CDR(BgL_pairz00_3248); } 
{ 
 obj_t BgL_bucketz00_8813;
BgL_bucketz00_8813 = BgL_arg1540z00_1654; 
BgL_bucketz00_1647 = BgL_bucketz00_8813; 
goto BgL_zc3z04anonymousza31530ze3z87_1648;} } } } } } } } } } 

}



/* weak-old-hashtable-get */
obj_t BGl_weakzd2oldzd2hashtablezd2getzd2zz__weakhashz00(obj_t BgL_tablez00_79, obj_t BgL_keyz00_80)
{
{ /* Llib/weakhash.scm 416 */
{ /* Llib/weakhash.scm 417 */
 obj_t BgL_bucketsz00_1659;
{ /* Llib/weakhash.scm 417 */
 bool_t BgL_test4170z00_8814;
{ /* Llib/weakhash.scm 417 */
 obj_t BgL_tmpz00_8815;
{ /* Llib/weakhash.scm 417 */
 obj_t BgL_res2253z00_3252;
{ /* Llib/weakhash.scm 417 */
 obj_t BgL_aux2999z00_5244;
BgL_aux2999z00_5244 = 
STRUCT_KEY(BgL_tablez00_79); 
if(
SYMBOLP(BgL_aux2999z00_5244))
{ /* Llib/weakhash.scm 417 */
BgL_res2253z00_3252 = BgL_aux2999z00_5244; }  else 
{ 
 obj_t BgL_auxz00_8819;
BgL_auxz00_8819 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16581L), BGl_string3531z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux2999z00_5244); 
FAILURE(BgL_auxz00_8819,BFALSE,BFALSE);} } 
BgL_tmpz00_8815 = BgL_res2253z00_3252; } 
BgL_test4170z00_8814 = 
(BgL_tmpz00_8815==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4170z00_8814)
{ /* Llib/weakhash.scm 417 */
 int BgL_tmpz00_8824;
BgL_tmpz00_8824 = 
(int)(2L); 
BgL_bucketsz00_1659 = 
STRUCT_REF(BgL_tablez00_79, BgL_tmpz00_8824); }  else 
{ /* Llib/weakhash.scm 417 */
BgL_bucketsz00_1659 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_79); } } 
{ /* Llib/weakhash.scm 417 */
 long BgL_bucketzd2lenzd2_1660;
{ /* Llib/weakhash.scm 418 */
 obj_t BgL_vectorz00_3253;
if(
VECTORP(BgL_bucketsz00_1659))
{ /* Llib/weakhash.scm 418 */
BgL_vectorz00_3253 = BgL_bucketsz00_1659; }  else 
{ 
 obj_t BgL_auxz00_8830;
BgL_auxz00_8830 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16639L), BGl_string3531z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1659); 
FAILURE(BgL_auxz00_8830,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1660 = 
VECTOR_LENGTH(BgL_vectorz00_3253); } 
{ /* Llib/weakhash.scm 418 */
 long BgL_bucketzd2numzd2_1661;
{ /* Llib/weakhash.scm 419 */
 long BgL_arg1552z00_1670;
BgL_arg1552z00_1670 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_79, BgL_keyz00_80); 
{ /* Llib/weakhash.scm 419 */
 long BgL_n1z00_3254; long BgL_n2z00_3255;
BgL_n1z00_3254 = BgL_arg1552z00_1670; 
BgL_n2z00_3255 = BgL_bucketzd2lenzd2_1660; 
{ /* Llib/weakhash.scm 419 */
 bool_t BgL_test4173z00_8836;
{ /* Llib/weakhash.scm 419 */
 long BgL_arg2024z00_3257;
BgL_arg2024z00_3257 = 
(((BgL_n1z00_3254) | (BgL_n2z00_3255)) & -2147483648); 
BgL_test4173z00_8836 = 
(BgL_arg2024z00_3257==0L); } 
if(BgL_test4173z00_8836)
{ /* Llib/weakhash.scm 419 */
 int32_t BgL_arg2020z00_3258;
{ /* Llib/weakhash.scm 419 */
 int32_t BgL_arg2021z00_3259; int32_t BgL_arg2022z00_3260;
BgL_arg2021z00_3259 = 
(int32_t)(BgL_n1z00_3254); 
BgL_arg2022z00_3260 = 
(int32_t)(BgL_n2z00_3255); 
BgL_arg2020z00_3258 = 
(BgL_arg2021z00_3259%BgL_arg2022z00_3260); } 
{ /* Llib/weakhash.scm 419 */
 long BgL_arg2135z00_3265;
BgL_arg2135z00_3265 = 
(long)(BgL_arg2020z00_3258); 
BgL_bucketzd2numzd2_1661 = 
(long)(BgL_arg2135z00_3265); } }  else 
{ /* Llib/weakhash.scm 419 */
BgL_bucketzd2numzd2_1661 = 
(BgL_n1z00_3254%BgL_n2z00_3255); } } } } 
{ /* Llib/weakhash.scm 419 */
 obj_t BgL_retz00_1662;
{ /* Llib/weakhash.scm 422 */
 obj_t BgL_zc3z04anonymousza31548ze3z87_4503;
{ 
 int BgL_tmpz00_8845;
BgL_tmpz00_8845 = 
(int)(2L); 
BgL_zc3z04anonymousza31548ze3z87_4503 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31548ze3ze5zz__weakhashz00, BgL_tmpz00_8845); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31548ze3z87_4503, 
(int)(0L), BgL_tablez00_79); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31548ze3z87_4503, 
(int)(1L), BgL_keyz00_80); 
BgL_retz00_1662 = 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_79, BgL_bucketsz00_1659, BgL_bucketzd2numzd2_1661, BgL_zc3z04anonymousza31548ze3z87_4503); } 
{ /* Llib/weakhash.scm 420 */

if(
(BgL_retz00_1662==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 425 */
return BFALSE;}  else 
{ /* Llib/weakhash.scm 425 */
return BgL_retz00_1662;} } } } } } } 

}



/* &<@anonymous:1548> */
obj_t BGl_z62zc3z04anonymousza31548ze3ze5zz__weakhashz00(obj_t BgL_envz00_4504, obj_t BgL_bkeyz00_4507, obj_t BgL_valz00_4508, obj_t BgL_bucketz00_4509)
{
{ /* Llib/weakhash.scm 421 */
{ /* Llib/weakhash.scm 422 */
 obj_t BgL_tablez00_4505; obj_t BgL_keyz00_4506;
BgL_tablez00_4505 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4504, 
(int)(0L))); 
BgL_keyz00_4506 = 
PROCEDURE_L_REF(BgL_envz00_4504, 
(int)(1L)); 
{ /* Llib/weakhash.scm 422 */
 bool_t BgL_test4175z00_8860;
{ /* Llib/weakhash.scm 422 */
 obj_t BgL_eqtz00_5748;
{ /* Llib/weakhash.scm 422 */
 bool_t BgL_test4176z00_8861;
{ /* Llib/weakhash.scm 422 */
 obj_t BgL_tmpz00_8862;
{ /* Llib/weakhash.scm 422 */
 obj_t BgL_res2254z00_5749;
{ /* Llib/weakhash.scm 422 */
 obj_t BgL_aux3003z00_5750;
BgL_aux3003z00_5750 = 
STRUCT_KEY(BgL_tablez00_4505); 
if(
SYMBOLP(BgL_aux3003z00_5750))
{ /* Llib/weakhash.scm 422 */
BgL_res2254z00_5749 = BgL_aux3003z00_5750; }  else 
{ 
 obj_t BgL_auxz00_8866;
BgL_auxz00_8866 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(16815L), BGl_string3532z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3003z00_5750); 
FAILURE(BgL_auxz00_8866,BFALSE,BFALSE);} } 
BgL_tmpz00_8862 = BgL_res2254z00_5749; } 
BgL_test4176z00_8861 = 
(BgL_tmpz00_8862==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4176z00_8861)
{ /* Llib/weakhash.scm 422 */
 int BgL_tmpz00_8871;
BgL_tmpz00_8871 = 
(int)(3L); 
BgL_eqtz00_5748 = 
STRUCT_REF(BgL_tablez00_4505, BgL_tmpz00_8871); }  else 
{ /* Llib/weakhash.scm 422 */
BgL_eqtz00_5748 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_4505); } } 
if(
PROCEDUREP(BgL_eqtz00_5748))
{ /* Llib/weakhash.scm 422 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_5748, 2))
{ /* Llib/weakhash.scm 422 */
BgL_test4175z00_8860 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_5748, BgL_keyz00_4506, BgL_bkeyz00_4507))
; }  else 
{ /* Llib/weakhash.scm 422 */
FAILURE(BGl_string3533z00zz__weakhashz00,BGl_list3523z00zz__weakhashz00,BgL_eqtz00_5748);} }  else 
{ /* Llib/weakhash.scm 422 */
if(
(BgL_keyz00_4506==BgL_bkeyz00_4507))
{ /* Llib/weakhash.scm 422 */
BgL_test4175z00_8860 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 422 */
if(
STRINGP(BgL_keyz00_4506))
{ /* Llib/weakhash.scm 422 */
if(
STRINGP(BgL_bkeyz00_4507))
{ /* Llib/weakhash.scm 422 */
 long BgL_l1z00_5751;
BgL_l1z00_5751 = 
STRING_LENGTH(BgL_keyz00_4506); 
if(
(BgL_l1z00_5751==
STRING_LENGTH(BgL_bkeyz00_4507)))
{ /* Llib/weakhash.scm 422 */
 int BgL_arg1918z00_5752;
{ /* Llib/weakhash.scm 422 */
 char * BgL_auxz00_8898; char * BgL_tmpz00_8896;
BgL_auxz00_8898 = 
BSTRING_TO_STRING(BgL_bkeyz00_4507); 
BgL_tmpz00_8896 = 
BSTRING_TO_STRING(BgL_keyz00_4506); 
BgL_arg1918z00_5752 = 
memcmp(BgL_tmpz00_8896, BgL_auxz00_8898, BgL_l1z00_5751); } 
BgL_test4175z00_8860 = 
(
(long)(BgL_arg1918z00_5752)==0L); }  else 
{ /* Llib/weakhash.scm 422 */
BgL_test4175z00_8860 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 422 */
BgL_test4175z00_8860 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 422 */
BgL_test4175z00_8860 = ((bool_t)0)
; } } } } 
if(BgL_test4175z00_8860)
{ /* Llib/weakhash.scm 422 */
return BgL_valz00_4508;}  else 
{ /* Llib/weakhash.scm 422 */
return BGl_keepgoingz00zz__weakhashz00;} } } } 

}



/* weak-hashtable-get */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2getz00zz__weakhashz00(obj_t BgL_tablez00_81, obj_t BgL_keyz00_82)
{
{ /* Llib/weakhash.scm 432 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_81))
{ /* Llib/weakhash.scm 433 */
BGL_TAIL return 
BGl_weakzd2keyszd2hashtablezd2getzd2zz__weakhashz00(BgL_tablez00_81, BgL_keyz00_82);}  else 
{ /* Llib/weakhash.scm 433 */
BGL_TAIL return 
BGl_weakzd2oldzd2hashtablezd2getzd2zz__weakhashz00(BgL_tablez00_81, BgL_keyz00_82);} } 

}



/* &weak-hashtable-get */
obj_t BGl_z62weakzd2hashtablezd2getz62zz__weakhashz00(obj_t BgL_envz00_4510, obj_t BgL_tablez00_4511, obj_t BgL_keyz00_4512)
{
{ /* Llib/weakhash.scm 432 */
{ /* Llib/weakhash.scm 433 */
 obj_t BgL_auxz00_8907;
if(
STRUCTP(BgL_tablez00_4511))
{ /* Llib/weakhash.scm 433 */
BgL_auxz00_8907 = BgL_tablez00_4511
; }  else 
{ 
 obj_t BgL_auxz00_8910;
BgL_auxz00_8910 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17200L), BGl_string3534z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4511); 
FAILURE(BgL_auxz00_8910,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2getz00zz__weakhashz00(BgL_auxz00_8907, BgL_keyz00_4512);} } 

}



/* weak-keys-hashtable-put! */
obj_t BGl_weakzd2keyszd2hashtablezd2putz12zc0zz__weakhashz00(obj_t BgL_tablez00_83, obj_t BgL_keyz00_84, obj_t BgL_objz00_85)
{
{ /* Llib/weakhash.scm 440 */
{ /* Llib/weakhash.scm 441 */
 obj_t BgL_bucketsz00_1672;
{ /* Llib/weakhash.scm 441 */
 bool_t BgL_test4186z00_8915;
{ /* Llib/weakhash.scm 441 */
 obj_t BgL_tmpz00_8916;
{ /* Llib/weakhash.scm 441 */
 obj_t BgL_res2255z00_3291;
{ /* Llib/weakhash.scm 441 */
 obj_t BgL_aux3008z00_5254;
BgL_aux3008z00_5254 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3008z00_5254))
{ /* Llib/weakhash.scm 441 */
BgL_res2255z00_3291 = BgL_aux3008z00_5254; }  else 
{ 
 obj_t BgL_auxz00_8920;
BgL_auxz00_8920 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17632L), BGl_string3535z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3008z00_5254); 
FAILURE(BgL_auxz00_8920,BFALSE,BFALSE);} } 
BgL_tmpz00_8916 = BgL_res2255z00_3291; } 
BgL_test4186z00_8915 = 
(BgL_tmpz00_8916==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4186z00_8915)
{ /* Llib/weakhash.scm 441 */
 int BgL_tmpz00_8925;
BgL_tmpz00_8925 = 
(int)(2L); 
BgL_bucketsz00_1672 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_8925); }  else 
{ /* Llib/weakhash.scm 441 */
BgL_bucketsz00_1672 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } 
{ /* Llib/weakhash.scm 441 */
 long BgL_bucketzd2lenzd2_1673;
{ /* Llib/weakhash.scm 442 */
 obj_t BgL_vectorz00_3292;
if(
VECTORP(BgL_bucketsz00_1672))
{ /* Llib/weakhash.scm 442 */
BgL_vectorz00_3292 = BgL_bucketsz00_1672; }  else 
{ 
 obj_t BgL_auxz00_8931;
BgL_auxz00_8931 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17690L), BGl_string3535z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1672); 
FAILURE(BgL_auxz00_8931,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1673 = 
VECTOR_LENGTH(BgL_vectorz00_3292); } 
{ /* Llib/weakhash.scm 442 */
 long BgL_bucketzd2numzd2_1674;
{ /* Llib/weakhash.scm 443 */
 long BgL_arg1583z00_1704;
BgL_arg1583z00_1704 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_83, BgL_keyz00_84); 
{ /* Llib/weakhash.scm 443 */
 long BgL_n1z00_3293; long BgL_n2z00_3294;
BgL_n1z00_3293 = BgL_arg1583z00_1704; 
BgL_n2z00_3294 = BgL_bucketzd2lenzd2_1673; 
{ /* Llib/weakhash.scm 443 */
 bool_t BgL_test4189z00_8937;
{ /* Llib/weakhash.scm 443 */
 long BgL_arg2024z00_3296;
BgL_arg2024z00_3296 = 
(((BgL_n1z00_3293) | (BgL_n2z00_3294)) & -2147483648); 
BgL_test4189z00_8937 = 
(BgL_arg2024z00_3296==0L); } 
if(BgL_test4189z00_8937)
{ /* Llib/weakhash.scm 443 */
 int32_t BgL_arg2020z00_3297;
{ /* Llib/weakhash.scm 443 */
 int32_t BgL_arg2021z00_3298; int32_t BgL_arg2022z00_3299;
BgL_arg2021z00_3298 = 
(int32_t)(BgL_n1z00_3293); 
BgL_arg2022z00_3299 = 
(int32_t)(BgL_n2z00_3294); 
BgL_arg2020z00_3297 = 
(BgL_arg2021z00_3298%BgL_arg2022z00_3299); } 
{ /* Llib/weakhash.scm 443 */
 long BgL_arg2135z00_3304;
BgL_arg2135z00_3304 = 
(long)(BgL_arg2020z00_3297); 
BgL_bucketzd2numzd2_1674 = 
(long)(BgL_arg2135z00_3304); } }  else 
{ /* Llib/weakhash.scm 443 */
BgL_bucketzd2numzd2_1674 = 
(BgL_n1z00_3293%BgL_n2z00_3294); } } } } 
{ /* Llib/weakhash.scm 443 */
 obj_t BgL_bucketz00_1675;
{ /* Llib/weakhash.scm 444 */
 obj_t BgL_vectorz00_3306;
if(
VECTORP(BgL_bucketsz00_1672))
{ /* Llib/weakhash.scm 444 */
BgL_vectorz00_3306 = BgL_bucketsz00_1672; }  else 
{ 
 obj_t BgL_auxz00_8948;
BgL_auxz00_8948 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17800L), BGl_string3535z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1672); 
FAILURE(BgL_auxz00_8948,BFALSE,BFALSE);} 
BgL_bucketz00_1675 = 
VECTOR_REF(BgL_vectorz00_3306,BgL_bucketzd2numzd2_1674); } 
{ /* Llib/weakhash.scm 444 */
 obj_t BgL_maxzd2bucketzd2lenz00_1676;
{ /* Llib/weakhash.scm 445 */
 bool_t BgL_test4191z00_8953;
{ /* Llib/weakhash.scm 445 */
 obj_t BgL_tmpz00_8954;
{ /* Llib/weakhash.scm 445 */
 obj_t BgL_res2256z00_3311;
{ /* Llib/weakhash.scm 445 */
 obj_t BgL_aux3014z00_5260;
BgL_aux3014z00_5260 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3014z00_5260))
{ /* Llib/weakhash.scm 445 */
BgL_res2256z00_3311 = BgL_aux3014z00_5260; }  else 
{ 
 obj_t BgL_auxz00_8958;
BgL_auxz00_8958 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17840L), BGl_string3535z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3014z00_5260); 
FAILURE(BgL_auxz00_8958,BFALSE,BFALSE);} } 
BgL_tmpz00_8954 = BgL_res2256z00_3311; } 
BgL_test4191z00_8953 = 
(BgL_tmpz00_8954==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4191z00_8953)
{ /* Llib/weakhash.scm 445 */
 int BgL_tmpz00_8963;
BgL_tmpz00_8963 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1676 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_8963); }  else 
{ /* Llib/weakhash.scm 445 */
BgL_maxzd2bucketzd2lenz00_1676 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } 
{ /* Llib/weakhash.scm 445 */

if(
NULLP(BgL_bucketz00_1675))
{ /* Llib/weakhash.scm 446 */
{ /* Llib/weakhash.scm 448 */
 long BgL_arg1555z00_1678;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_arg1556z00_1679;
{ /* Llib/weakhash.scm 448 */
 bool_t BgL_test4194z00_8969;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_tmpz00_8970;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_res2257z00_3315;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_aux3016z00_5262;
BgL_aux3016z00_5262 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3016z00_5262))
{ /* Llib/weakhash.scm 448 */
BgL_res2257z00_3315 = BgL_aux3016z00_5262; }  else 
{ 
 obj_t BgL_auxz00_8974;
BgL_auxz00_8974 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17950L), BGl_string3535z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3016z00_5262); 
FAILURE(BgL_auxz00_8974,BFALSE,BFALSE);} } 
BgL_tmpz00_8970 = BgL_res2257z00_3315; } 
BgL_test4194z00_8969 = 
(BgL_tmpz00_8970==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4194z00_8969)
{ /* Llib/weakhash.scm 448 */
 int BgL_tmpz00_8979;
BgL_tmpz00_8979 = 
(int)(0L); 
BgL_arg1556z00_1679 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_8979); }  else 
{ /* Llib/weakhash.scm 448 */
BgL_arg1556z00_1679 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } 
{ /* Llib/weakhash.scm 448 */
 long BgL_za71za7_3316;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_tmpz00_8983;
if(
INTEGERP(BgL_arg1556z00_1679))
{ /* Llib/weakhash.scm 448 */
BgL_tmpz00_8983 = BgL_arg1556z00_1679
; }  else 
{ 
 obj_t BgL_auxz00_8986;
BgL_auxz00_8986 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17972L), BGl_string3535z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1556z00_1679); 
FAILURE(BgL_auxz00_8986,BFALSE,BFALSE);} 
BgL_za71za7_3316 = 
(long)CINT(BgL_tmpz00_8983); } 
BgL_arg1555z00_1678 = 
(BgL_za71za7_3316+1L); } } 
{ /* Llib/weakhash.scm 448 */
 bool_t BgL_test4197z00_8992;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_tmpz00_8993;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_res2258z00_3320;
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_aux3019z00_5265;
BgL_aux3019z00_5265 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3019z00_5265))
{ /* Llib/weakhash.scm 448 */
BgL_res2258z00_3320 = BgL_aux3019z00_5265; }  else 
{ 
 obj_t BgL_auxz00_8997;
BgL_auxz00_8997 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(17917L), BGl_string3535z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3019z00_5265); 
FAILURE(BgL_auxz00_8997,BFALSE,BFALSE);} } 
BgL_tmpz00_8993 = BgL_res2258z00_3320; } 
BgL_test4197z00_8992 = 
(BgL_tmpz00_8993==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4197z00_8992)
{ /* Llib/weakhash.scm 448 */
 obj_t BgL_auxz00_9004; int BgL_tmpz00_9002;
BgL_auxz00_9004 = 
BINT(BgL_arg1555z00_1678); 
BgL_tmpz00_9002 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_83, BgL_tmpz00_9002, BgL_auxz00_9004); }  else 
{ /* Llib/weakhash.scm 448 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } } 
{ /* Llib/weakhash.scm 449 */
 obj_t BgL_arg1557z00_1680;
{ /* Llib/weakhash.scm 449 */
 obj_t BgL_arg1558z00_1681;
BgL_arg1558z00_1681 = 
bgl_make_weakptr(BgL_keyz00_84, BgL_objz00_85); 
{ /* Llib/weakhash.scm 449 */
 obj_t BgL_list1559z00_1682;
BgL_list1559z00_1682 = 
MAKE_YOUNG_PAIR(BgL_arg1558z00_1681, BNIL); 
BgL_arg1557z00_1680 = BgL_list1559z00_1682; } } 
{ /* Llib/weakhash.scm 449 */
 obj_t BgL_vectorz00_3322;
if(
VECTORP(BgL_bucketsz00_1672))
{ /* Llib/weakhash.scm 449 */
BgL_vectorz00_3322 = BgL_bucketsz00_1672; }  else 
{ 
 obj_t BgL_auxz00_9012;
BgL_auxz00_9012 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18000L), BGl_string3535z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1672); 
FAILURE(BgL_auxz00_9012,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3322,BgL_bucketzd2numzd2_1674,BgL_arg1557z00_1680); } } 
return BgL_objz00_85;}  else 
{ 
 obj_t BgL_buckz00_1684; long BgL_countz00_1685;
BgL_buckz00_1684 = BgL_bucketz00_1675; 
BgL_countz00_1685 = 0L; 
BgL_zc3z04anonymousza31560ze3z87_1686:
if(
NULLP(BgL_buckz00_1684))
{ /* Llib/weakhash.scm 454 */
{ /* Llib/weakhash.scm 455 */
 long BgL_arg1562z00_1688;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_arg1564z00_1689;
{ /* Llib/weakhash.scm 455 */
 bool_t BgL_test4201z00_9019;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_tmpz00_9020;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_res2260z00_3327;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_aux3023z00_5269;
BgL_aux3023z00_5269 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3023z00_5269))
{ /* Llib/weakhash.scm 455 */
BgL_res2260z00_3327 = BgL_aux3023z00_5269; }  else 
{ 
 obj_t BgL_auxz00_9024;
BgL_auxz00_9024 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18171L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3023z00_5269); 
FAILURE(BgL_auxz00_9024,BFALSE,BFALSE);} } 
BgL_tmpz00_9020 = BgL_res2260z00_3327; } 
BgL_test4201z00_9019 = 
(BgL_tmpz00_9020==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4201z00_9019)
{ /* Llib/weakhash.scm 455 */
 int BgL_tmpz00_9029;
BgL_tmpz00_9029 = 
(int)(0L); 
BgL_arg1564z00_1689 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_9029); }  else 
{ /* Llib/weakhash.scm 455 */
BgL_arg1564z00_1689 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } 
{ /* Llib/weakhash.scm 455 */
 long BgL_za71za7_3328;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_tmpz00_9033;
if(
INTEGERP(BgL_arg1564z00_1689))
{ /* Llib/weakhash.scm 455 */
BgL_tmpz00_9033 = BgL_arg1564z00_1689
; }  else 
{ 
 obj_t BgL_auxz00_9036;
BgL_auxz00_9036 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18193L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1564z00_1689); 
FAILURE(BgL_auxz00_9036,BFALSE,BFALSE);} 
BgL_za71za7_3328 = 
(long)CINT(BgL_tmpz00_9033); } 
BgL_arg1562z00_1688 = 
(BgL_za71za7_3328+1L); } } 
{ /* Llib/weakhash.scm 455 */
 bool_t BgL_test4204z00_9042;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_tmpz00_9043;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_res2261z00_3332;
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_aux3026z00_5272;
BgL_aux3026z00_5272 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3026z00_5272))
{ /* Llib/weakhash.scm 455 */
BgL_res2261z00_3332 = BgL_aux3026z00_5272; }  else 
{ 
 obj_t BgL_auxz00_9047;
BgL_auxz00_9047 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18138L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3026z00_5272); 
FAILURE(BgL_auxz00_9047,BFALSE,BFALSE);} } 
BgL_tmpz00_9043 = BgL_res2261z00_3332; } 
BgL_test4204z00_9042 = 
(BgL_tmpz00_9043==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4204z00_9042)
{ /* Llib/weakhash.scm 455 */
 obj_t BgL_auxz00_9054; int BgL_tmpz00_9052;
BgL_auxz00_9054 = 
BINT(BgL_arg1562z00_1688); 
BgL_tmpz00_9052 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_83, BgL_tmpz00_9052, BgL_auxz00_9054); }  else 
{ /* Llib/weakhash.scm 455 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } } 
{ /* Llib/weakhash.scm 457 */
 obj_t BgL_arg1565z00_1690;
BgL_arg1565z00_1690 = 
MAKE_YOUNG_PAIR(
bgl_make_weakptr(BgL_keyz00_84, BgL_objz00_85), BgL_bucketz00_1675); 
{ /* Llib/weakhash.scm 456 */
 obj_t BgL_vectorz00_3333;
if(
VECTORP(BgL_bucketsz00_1672))
{ /* Llib/weakhash.scm 456 */
BgL_vectorz00_3333 = BgL_bucketsz00_1672; }  else 
{ 
 obj_t BgL_auxz00_9062;
BgL_auxz00_9062 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18218L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1672); 
FAILURE(BgL_auxz00_9062,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3333,BgL_bucketzd2numzd2_1674,BgL_arg1565z00_1690); } } 
{ /* Llib/weakhash.scm 458 */
 bool_t BgL_test4207z00_9067;
{ /* Llib/weakhash.scm 458 */
 long BgL_n2z00_3336;
{ /* Llib/weakhash.scm 458 */
 obj_t BgL_tmpz00_9068;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1676))
{ /* Llib/weakhash.scm 458 */
BgL_tmpz00_9068 = BgL_maxzd2bucketzd2lenz00_1676
; }  else 
{ 
 obj_t BgL_auxz00_9071;
BgL_auxz00_9071 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18301L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1676); 
FAILURE(BgL_auxz00_9071,BFALSE,BFALSE);} 
BgL_n2z00_3336 = 
(long)CINT(BgL_tmpz00_9068); } 
BgL_test4207z00_9067 = 
(BgL_countz00_1685>BgL_n2z00_3336); } 
if(BgL_test4207z00_9067)
{ /* Llib/weakhash.scm 458 */
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_83); }  else 
{ /* Llib/weakhash.scm 458 */BFALSE; } } 
return BgL_objz00_85;}  else 
{ /* Llib/weakhash.scm 461 */
 bool_t BgL_test4209z00_9078;
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_arg1580z00_1701;
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_arg1582z00_1702;
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_pairz00_3337;
if(
PAIRP(BgL_buckz00_1684))
{ /* Llib/weakhash.scm 461 */
BgL_pairz00_3337 = BgL_buckz00_1684; }  else 
{ 
 obj_t BgL_auxz00_9081;
BgL_auxz00_9081 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18414L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1684); 
FAILURE(BgL_auxz00_9081,BFALSE,BFALSE);} 
BgL_arg1582z00_1702 = 
CAR(BgL_pairz00_3337); } 
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_objz00_3338;
if(
BGL_WEAKPTRP(BgL_arg1582z00_1702))
{ /* Llib/weakhash.scm 461 */
BgL_objz00_3338 = BgL_arg1582z00_1702; }  else 
{ 
 obj_t BgL_auxz00_9088;
BgL_auxz00_9088 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18418L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1582z00_1702); 
FAILURE(BgL_auxz00_9088,BFALSE,BFALSE);} 
BgL_arg1580z00_1701 = 
bgl_weakptr_data(BgL_objz00_3338); } } 
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_eqtz00_3339;
{ /* Llib/weakhash.scm 461 */
 bool_t BgL_test4212z00_9093;
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_tmpz00_9094;
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_res2262z00_3346;
{ /* Llib/weakhash.scm 461 */
 obj_t BgL_aux3035z00_5281;
BgL_aux3035z00_5281 = 
STRUCT_KEY(BgL_tablez00_83); 
if(
SYMBOLP(BgL_aux3035z00_5281))
{ /* Llib/weakhash.scm 461 */
BgL_res2262z00_3346 = BgL_aux3035z00_5281; }  else 
{ 
 obj_t BgL_auxz00_9098;
BgL_auxz00_9098 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18371L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3035z00_5281); 
FAILURE(BgL_auxz00_9098,BFALSE,BFALSE);} } 
BgL_tmpz00_9094 = BgL_res2262z00_3346; } 
BgL_test4212z00_9093 = 
(BgL_tmpz00_9094==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4212z00_9093)
{ /* Llib/weakhash.scm 461 */
 int BgL_tmpz00_9103;
BgL_tmpz00_9103 = 
(int)(3L); 
BgL_eqtz00_3339 = 
STRUCT_REF(BgL_tablez00_83, BgL_tmpz00_9103); }  else 
{ /* Llib/weakhash.scm 461 */
BgL_eqtz00_3339 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_83); } } 
if(
PROCEDUREP(BgL_eqtz00_3339))
{ /* Llib/weakhash.scm 461 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3339, 2))
{ /* Llib/weakhash.scm 461 */
BgL_test4209z00_9078 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3339, BgL_arg1580z00_1701, BgL_keyz00_84))
; }  else 
{ /* Llib/weakhash.scm 461 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3536z00zz__weakhashz00,BgL_eqtz00_3339);} }  else 
{ /* Llib/weakhash.scm 461 */
if(
(BgL_arg1580z00_1701==BgL_keyz00_84))
{ /* Llib/weakhash.scm 461 */
BgL_test4209z00_9078 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 461 */
if(
STRINGP(BgL_arg1580z00_1701))
{ /* Llib/weakhash.scm 461 */
if(
STRINGP(BgL_keyz00_84))
{ /* Llib/weakhash.scm 461 */
 long BgL_l1z00_3349;
BgL_l1z00_3349 = 
STRING_LENGTH(BgL_arg1580z00_1701); 
if(
(BgL_l1z00_3349==
STRING_LENGTH(BgL_keyz00_84)))
{ /* Llib/weakhash.scm 461 */
 int BgL_arg1918z00_3352;
{ /* Llib/weakhash.scm 461 */
 char * BgL_auxz00_9130; char * BgL_tmpz00_9128;
BgL_auxz00_9130 = 
BSTRING_TO_STRING(BgL_keyz00_84); 
BgL_tmpz00_9128 = 
BSTRING_TO_STRING(BgL_arg1580z00_1701); 
BgL_arg1918z00_3352 = 
memcmp(BgL_tmpz00_9128, BgL_auxz00_9130, BgL_l1z00_3349); } 
BgL_test4209z00_9078 = 
(
(long)(BgL_arg1918z00_3352)==0L); }  else 
{ /* Llib/weakhash.scm 461 */
BgL_test4209z00_9078 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 461 */
BgL_test4209z00_9078 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 461 */
BgL_test4209z00_9078 = ((bool_t)0)
; } } } } } 
if(BgL_test4209z00_9078)
{ /* Llib/weakhash.scm 462 */
 obj_t BgL_oldzd2objzd2_1696;
{ /* Llib/weakhash.scm 462 */
 obj_t BgL_arg1576z00_1698;
{ /* Llib/weakhash.scm 462 */
 obj_t BgL_pairz00_3358;
if(
PAIRP(BgL_buckz00_1684))
{ /* Llib/weakhash.scm 462 */
BgL_pairz00_3358 = BgL_buckz00_1684; }  else 
{ 
 obj_t BgL_auxz00_9137;
BgL_auxz00_9137 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18462L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1684); 
FAILURE(BgL_auxz00_9137,BFALSE,BFALSE);} 
BgL_arg1576z00_1698 = 
CAR(BgL_pairz00_3358); } 
{ /* Llib/weakhash.scm 462 */
 obj_t BgL_objz00_3359;
if(
BGL_WEAKPTRP(BgL_arg1576z00_1698))
{ /* Llib/weakhash.scm 462 */
BgL_objz00_3359 = BgL_arg1576z00_1698; }  else 
{ 
 obj_t BgL_auxz00_9144;
BgL_auxz00_9144 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18466L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1576z00_1698); 
FAILURE(BgL_auxz00_9144,BFALSE,BFALSE);} 
BgL_oldzd2objzd2_1696 = 
bgl_weakptr_ref(BgL_objz00_3359); } } 
{ /* Llib/weakhash.scm 463 */
 obj_t BgL_arg1575z00_1697;
{ /* Llib/weakhash.scm 463 */
 obj_t BgL_pairz00_3360;
if(
PAIRP(BgL_buckz00_1684))
{ /* Llib/weakhash.scm 463 */
BgL_pairz00_3360 = BgL_buckz00_1684; }  else 
{ 
 obj_t BgL_auxz00_9151;
BgL_auxz00_9151 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18500L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1684); 
FAILURE(BgL_auxz00_9151,BFALSE,BFALSE);} 
BgL_arg1575z00_1697 = 
CAR(BgL_pairz00_3360); } 
{ /* Llib/weakhash.scm 463 */
 obj_t BgL_ptrz00_3361;
if(
BGL_WEAKPTRP(BgL_arg1575z00_1697))
{ /* Llib/weakhash.scm 463 */
BgL_ptrz00_3361 = BgL_arg1575z00_1697; }  else 
{ 
 obj_t BgL_auxz00_9158;
BgL_auxz00_9158 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18504L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1575z00_1697); 
FAILURE(BgL_auxz00_9158,BFALSE,BFALSE);} 
bgl_weakptr_ref_set(BgL_ptrz00_3361, BgL_objz00_85); BUNSPEC; BUNSPEC; } } 
return BgL_oldzd2objzd2_1696;}  else 
{ /* Llib/weakhash.scm 466 */
 obj_t BgL_arg1578z00_1699; long BgL_arg1579z00_1700;
{ /* Llib/weakhash.scm 466 */
 obj_t BgL_pairz00_3362;
if(
PAIRP(BgL_buckz00_1684))
{ /* Llib/weakhash.scm 466 */
BgL_pairz00_3362 = BgL_buckz00_1684; }  else 
{ 
 obj_t BgL_auxz00_9165;
BgL_auxz00_9165 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18549L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1684); 
FAILURE(BgL_auxz00_9165,BFALSE,BFALSE);} 
BgL_arg1578z00_1699 = 
CDR(BgL_pairz00_3362); } 
BgL_arg1579z00_1700 = 
(BgL_countz00_1685+1L); 
{ 
 long BgL_countz00_9172; obj_t BgL_buckz00_9171;
BgL_buckz00_9171 = BgL_arg1578z00_1699; 
BgL_countz00_9172 = BgL_arg1579z00_1700; 
BgL_countz00_1685 = BgL_countz00_9172; 
BgL_buckz00_1684 = BgL_buckz00_9171; 
goto BgL_zc3z04anonymousza31560ze3z87_1686;} } } } } } } } } } } 

}



/* weak-old-hashtable-put! */
obj_t BGl_weakzd2oldzd2hashtablezd2putz12zc0zz__weakhashz00(obj_t BgL_tablez00_86, obj_t BgL_keyz00_87, obj_t BgL_objz00_88)
{
{ /* Llib/weakhash.scm 471 */
{ /* Llib/weakhash.scm 472 */
 obj_t BgL_bucketsz00_1705;
{ /* Llib/weakhash.scm 472 */
 bool_t BgL_test4225z00_9173;
{ /* Llib/weakhash.scm 472 */
 obj_t BgL_tmpz00_9174;
{ /* Llib/weakhash.scm 472 */
 obj_t BgL_res2263z00_3367;
{ /* Llib/weakhash.scm 472 */
 obj_t BgL_aux3048z00_5295;
BgL_aux3048z00_5295 = 
STRUCT_KEY(BgL_tablez00_86); 
if(
SYMBOLP(BgL_aux3048z00_5295))
{ /* Llib/weakhash.scm 472 */
BgL_res2263z00_3367 = BgL_aux3048z00_5295; }  else 
{ 
 obj_t BgL_auxz00_9178;
BgL_auxz00_9178 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18884L), BGl_string3539z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3048z00_5295); 
FAILURE(BgL_auxz00_9178,BFALSE,BFALSE);} } 
BgL_tmpz00_9174 = BgL_res2263z00_3367; } 
BgL_test4225z00_9173 = 
(BgL_tmpz00_9174==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4225z00_9173)
{ /* Llib/weakhash.scm 472 */
 int BgL_tmpz00_9183;
BgL_tmpz00_9183 = 
(int)(2L); 
BgL_bucketsz00_1705 = 
STRUCT_REF(BgL_tablez00_86, BgL_tmpz00_9183); }  else 
{ /* Llib/weakhash.scm 472 */
BgL_bucketsz00_1705 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_86); } } 
{ /* Llib/weakhash.scm 472 */
 long BgL_bucketzd2lenzd2_1706;
{ /* Llib/weakhash.scm 473 */
 obj_t BgL_vectorz00_3368;
if(
VECTORP(BgL_bucketsz00_1705))
{ /* Llib/weakhash.scm 473 */
BgL_vectorz00_3368 = BgL_bucketsz00_1705; }  else 
{ 
 obj_t BgL_auxz00_9189;
BgL_auxz00_9189 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(18942L), BGl_string3539z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1705); 
FAILURE(BgL_auxz00_9189,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1706 = 
VECTOR_LENGTH(BgL_vectorz00_3368); } 
{ /* Llib/weakhash.scm 473 */
 long BgL_bucketzd2numzd2_1707;
{ /* Llib/weakhash.scm 474 */
 long BgL_arg1608z00_1739;
BgL_arg1608z00_1739 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_86, BgL_keyz00_87); 
{ /* Llib/weakhash.scm 474 */
 long BgL_n1z00_3369; long BgL_n2z00_3370;
BgL_n1z00_3369 = BgL_arg1608z00_1739; 
BgL_n2z00_3370 = BgL_bucketzd2lenzd2_1706; 
{ /* Llib/weakhash.scm 474 */
 bool_t BgL_test4228z00_9195;
{ /* Llib/weakhash.scm 474 */
 long BgL_arg2024z00_3372;
BgL_arg2024z00_3372 = 
(((BgL_n1z00_3369) | (BgL_n2z00_3370)) & -2147483648); 
BgL_test4228z00_9195 = 
(BgL_arg2024z00_3372==0L); } 
if(BgL_test4228z00_9195)
{ /* Llib/weakhash.scm 474 */
 int32_t BgL_arg2020z00_3373;
{ /* Llib/weakhash.scm 474 */
 int32_t BgL_arg2021z00_3374; int32_t BgL_arg2022z00_3375;
BgL_arg2021z00_3374 = 
(int32_t)(BgL_n1z00_3369); 
BgL_arg2022z00_3375 = 
(int32_t)(BgL_n2z00_3370); 
BgL_arg2020z00_3373 = 
(BgL_arg2021z00_3374%BgL_arg2022z00_3375); } 
{ /* Llib/weakhash.scm 474 */
 long BgL_arg2135z00_3380;
BgL_arg2135z00_3380 = 
(long)(BgL_arg2020z00_3373); 
BgL_bucketzd2numzd2_1707 = 
(long)(BgL_arg2135z00_3380); } }  else 
{ /* Llib/weakhash.scm 474 */
BgL_bucketzd2numzd2_1707 = 
(BgL_n1z00_3369%BgL_n2z00_3370); } } } } 
{ /* Llib/weakhash.scm 474 */
 obj_t BgL_bucketz00_1708;
{ /* Llib/weakhash.scm 475 */
 obj_t BgL_vectorz00_3382;
if(
VECTORP(BgL_bucketsz00_1705))
{ /* Llib/weakhash.scm 475 */
BgL_vectorz00_3382 = BgL_bucketsz00_1705; }  else 
{ 
 obj_t BgL_auxz00_9206;
BgL_auxz00_9206 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19049L), BGl_string3539z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1705); 
FAILURE(BgL_auxz00_9206,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 475 */
 bool_t BgL_test4230z00_9210;
{ /* Llib/weakhash.scm 475 */
 long BgL_tmpz00_9211;
BgL_tmpz00_9211 = 
VECTOR_LENGTH(BgL_vectorz00_3382); 
BgL_test4230z00_9210 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1707, BgL_tmpz00_9211); } 
if(BgL_test4230z00_9210)
{ /* Llib/weakhash.scm 475 */
BgL_bucketz00_1708 = 
VECTOR_REF(BgL_vectorz00_3382,BgL_bucketzd2numzd2_1707); }  else 
{ 
 obj_t BgL_auxz00_9215;
BgL_auxz00_9215 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19037L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3382, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3382)), 
(int)(BgL_bucketzd2numzd2_1707)); 
FAILURE(BgL_auxz00_9215,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 475 */
 obj_t BgL_maxzd2bucketzd2lenz00_1709;
{ /* Llib/weakhash.scm 476 */
 bool_t BgL_test4231z00_9222;
{ /* Llib/weakhash.scm 476 */
 obj_t BgL_tmpz00_9223;
{ /* Llib/weakhash.scm 476 */
 obj_t BgL_res2264z00_3387;
{ /* Llib/weakhash.scm 476 */
 obj_t BgL_aux3054z00_5301;
BgL_aux3054z00_5301 = 
STRUCT_KEY(BgL_tablez00_86); 
if(
SYMBOLP(BgL_aux3054z00_5301))
{ /* Llib/weakhash.scm 476 */
BgL_res2264z00_3387 = BgL_aux3054z00_5301; }  else 
{ 
 obj_t BgL_auxz00_9227;
BgL_auxz00_9227 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19089L), BGl_string3539z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3054z00_5301); 
FAILURE(BgL_auxz00_9227,BFALSE,BFALSE);} } 
BgL_tmpz00_9223 = BgL_res2264z00_3387; } 
BgL_test4231z00_9222 = 
(BgL_tmpz00_9223==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4231z00_9222)
{ /* Llib/weakhash.scm 476 */
 int BgL_tmpz00_9232;
BgL_tmpz00_9232 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1709 = 
STRUCT_REF(BgL_tablez00_86, BgL_tmpz00_9232); }  else 
{ /* Llib/weakhash.scm 476 */
BgL_maxzd2bucketzd2lenz00_1709 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_86); } } 
{ /* Llib/weakhash.scm 476 */
 obj_t BgL_countz00_4523;
BgL_countz00_4523 = 
MAKE_CELL(
BINT(0L)); 
{ /* Llib/weakhash.scm 477 */
 obj_t BgL_foundz00_1711;
{ /* Llib/weakhash.scm 482 */
 obj_t BgL_zc3z04anonymousza31602ze3z87_4513;
{ 
 int BgL_tmpz00_9237;
BgL_tmpz00_9237 = 
(int)(4L); 
BgL_zc3z04anonymousza31602ze3z87_4513 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31602ze3ze5zz__weakhashz00, BgL_tmpz00_9237); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31602ze3z87_4513, 
(int)(0L), 
((obj_t)BgL_countz00_4523)); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31602ze3z87_4513, 
(int)(1L), BgL_objz00_88); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31602ze3z87_4513, 
(int)(2L), BgL_tablez00_86); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31602ze3z87_4513, 
(int)(3L), BgL_keyz00_87); 
BgL_foundz00_1711 = 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_86, BgL_bucketsz00_1705, BgL_bucketzd2numzd2_1707, BgL_zc3z04anonymousza31602ze3z87_4513); } 
{ /* Llib/weakhash.scm 479 */

if(
(BgL_foundz00_1711==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 493 */
{ /* Llib/weakhash.scm 496 */
 long BgL_arg1584z00_1712;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_arg1585z00_1713;
{ /* Llib/weakhash.scm 496 */
 bool_t BgL_test4234z00_9252;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_tmpz00_9253;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_res2266z00_3413;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_aux3056z00_5303;
BgL_aux3056z00_5303 = 
STRUCT_KEY(BgL_tablez00_86); 
if(
SYMBOLP(BgL_aux3056z00_5303))
{ /* Llib/weakhash.scm 496 */
BgL_res2266z00_3413 = BgL_aux3056z00_5303; }  else 
{ 
 obj_t BgL_auxz00_9257;
BgL_auxz00_9257 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19678L), BGl_string3539z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3056z00_5303); 
FAILURE(BgL_auxz00_9257,BFALSE,BFALSE);} } 
BgL_tmpz00_9253 = BgL_res2266z00_3413; } 
BgL_test4234z00_9252 = 
(BgL_tmpz00_9253==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4234z00_9252)
{ /* Llib/weakhash.scm 496 */
 int BgL_tmpz00_9262;
BgL_tmpz00_9262 = 
(int)(0L); 
BgL_arg1585z00_1713 = 
STRUCT_REF(BgL_tablez00_86, BgL_tmpz00_9262); }  else 
{ /* Llib/weakhash.scm 496 */
BgL_arg1585z00_1713 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_86); } } 
{ /* Llib/weakhash.scm 496 */
 long BgL_za71za7_3414;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_tmpz00_9266;
if(
INTEGERP(BgL_arg1585z00_1713))
{ /* Llib/weakhash.scm 496 */
BgL_tmpz00_9266 = BgL_arg1585z00_1713
; }  else 
{ 
 obj_t BgL_auxz00_9269;
BgL_auxz00_9269 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19700L), BGl_string3539z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1585z00_1713); 
FAILURE(BgL_auxz00_9269,BFALSE,BFALSE);} 
BgL_za71za7_3414 = 
(long)CINT(BgL_tmpz00_9266); } 
BgL_arg1584z00_1712 = 
(BgL_za71za7_3414+1L); } } 
{ /* Llib/weakhash.scm 496 */
 bool_t BgL_test4237z00_9275;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_tmpz00_9276;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_res2267z00_3418;
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_aux3059z00_5306;
BgL_aux3059z00_5306 = 
STRUCT_KEY(BgL_tablez00_86); 
if(
SYMBOLP(BgL_aux3059z00_5306))
{ /* Llib/weakhash.scm 496 */
BgL_res2267z00_3418 = BgL_aux3059z00_5306; }  else 
{ 
 obj_t BgL_auxz00_9280;
BgL_auxz00_9280 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19645L), BGl_string3539z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3059z00_5306); 
FAILURE(BgL_auxz00_9280,BFALSE,BFALSE);} } 
BgL_tmpz00_9276 = BgL_res2267z00_3418; } 
BgL_test4237z00_9275 = 
(BgL_tmpz00_9276==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4237z00_9275)
{ /* Llib/weakhash.scm 496 */
 obj_t BgL_auxz00_9287; int BgL_tmpz00_9285;
BgL_auxz00_9287 = 
BINT(BgL_arg1584z00_1712); 
BgL_tmpz00_9285 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_86, BgL_tmpz00_9285, BgL_auxz00_9287); }  else 
{ /* Llib/weakhash.scm 496 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_86); } } } 
{ /* Llib/weakhash.scm 498 */
 obj_t BgL_arg1586z00_1714;
{ /* Llib/weakhash.scm 498 */
 obj_t BgL_arg1587z00_1715; obj_t BgL_arg1589z00_1716;
{ /* Llib/weakhash.scm 498 */
 obj_t BgL_arg1591z00_1717; obj_t BgL_arg1593z00_1718;
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_86))
{ /* Llib/weakhash.scm 499 */

BgL_arg1591z00_1717 = 
bgl_make_weakptr(BgL_keyz00_87, BFALSE); }  else 
{ /* Llib/weakhash.scm 498 */
BgL_arg1591z00_1717 = BgL_keyz00_87; } 
if(
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_tablez00_86))
{ /* Llib/weakhash.scm 502 */

BgL_arg1593z00_1718 = 
bgl_make_weakptr(BgL_objz00_88, BFALSE); }  else 
{ /* Llib/weakhash.scm 501 */
BgL_arg1593z00_1718 = BgL_objz00_88; } 
BgL_arg1587z00_1715 = 
MAKE_YOUNG_PAIR(BgL_arg1591z00_1717, BgL_arg1593z00_1718); } 
{ /* Llib/weakhash.scm 506 */
 obj_t BgL_arg1598z00_1725;
{ /* Llib/weakhash.scm 506 */
 bool_t BgL_test4241z00_9298;
{ /* Llib/weakhash.scm 506 */
 obj_t BgL_tmpz00_9299;
{ /* Llib/weakhash.scm 506 */
 obj_t BgL_res2268z00_3422;
{ /* Llib/weakhash.scm 506 */
 obj_t BgL_aux3061z00_5308;
BgL_aux3061z00_5308 = 
STRUCT_KEY(BgL_tablez00_86); 
if(
SYMBOLP(BgL_aux3061z00_5308))
{ /* Llib/weakhash.scm 506 */
BgL_res2268z00_3422 = BgL_aux3061z00_5308; }  else 
{ 
 obj_t BgL_auxz00_9303;
BgL_auxz00_9303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19999L), BGl_string3539z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3061z00_5308); 
FAILURE(BgL_auxz00_9303,BFALSE,BFALSE);} } 
BgL_tmpz00_9299 = BgL_res2268z00_3422; } 
BgL_test4241z00_9298 = 
(BgL_tmpz00_9299==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4241z00_9298)
{ /* Llib/weakhash.scm 506 */
 int BgL_tmpz00_9308;
BgL_tmpz00_9308 = 
(int)(2L); 
BgL_arg1598z00_1725 = 
STRUCT_REF(BgL_tablez00_86, BgL_tmpz00_9308); }  else 
{ /* Llib/weakhash.scm 506 */
BgL_arg1598z00_1725 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_86); } } 
{ /* Llib/weakhash.scm 506 */
 obj_t BgL_vectorz00_3423;
if(
VECTORP(BgL_arg1598z00_1725))
{ /* Llib/weakhash.scm 506 */
BgL_vectorz00_3423 = BgL_arg1598z00_1725; }  else 
{ 
 obj_t BgL_auxz00_9314;
BgL_auxz00_9314 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(20024L), BGl_string3539z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_arg1598z00_1725); 
FAILURE(BgL_auxz00_9314,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 506 */
 bool_t BgL_test4244z00_9318;
{ /* Llib/weakhash.scm 506 */
 long BgL_tmpz00_9319;
BgL_tmpz00_9319 = 
VECTOR_LENGTH(BgL_vectorz00_3423); 
BgL_test4244z00_9318 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1707, BgL_tmpz00_9319); } 
if(BgL_test4244z00_9318)
{ /* Llib/weakhash.scm 506 */
BgL_arg1589z00_1716 = 
VECTOR_REF(BgL_vectorz00_3423,BgL_bucketzd2numzd2_1707); }  else 
{ 
 obj_t BgL_auxz00_9323;
BgL_auxz00_9323 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19987L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3423, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3423)), 
(int)(BgL_bucketzd2numzd2_1707)); 
FAILURE(BgL_auxz00_9323,BFALSE,BFALSE);} } } } 
BgL_arg1586z00_1714 = 
MAKE_YOUNG_PAIR(BgL_arg1587z00_1715, BgL_arg1589z00_1716); } 
{ /* Llib/weakhash.scm 497 */
 obj_t BgL_vectorz00_3425;
if(
VECTORP(BgL_bucketsz00_1705))
{ /* Llib/weakhash.scm 497 */
BgL_vectorz00_3425 = BgL_bucketsz00_1705; }  else 
{ 
 obj_t BgL_auxz00_9333;
BgL_auxz00_9333 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19725L), BGl_string3539z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1705); 
FAILURE(BgL_auxz00_9333,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 497 */
 bool_t BgL_test4246z00_9337;
{ /* Llib/weakhash.scm 497 */
 long BgL_tmpz00_9338;
BgL_tmpz00_9338 = 
VECTOR_LENGTH(BgL_vectorz00_3425); 
BgL_test4246z00_9337 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1707, BgL_tmpz00_9338); } 
if(BgL_test4246z00_9337)
{ /* Llib/weakhash.scm 497 */
VECTOR_SET(BgL_vectorz00_3425,BgL_bucketzd2numzd2_1707,BgL_arg1586z00_1714); }  else 
{ 
 obj_t BgL_auxz00_9342;
BgL_auxz00_9342 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19712L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_3425, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3425)), 
(int)(BgL_bucketzd2numzd2_1707)); 
FAILURE(BgL_auxz00_9342,BFALSE,BFALSE);} } } } 
{ /* Llib/weakhash.scm 508 */
 bool_t BgL_test4247z00_9349;
{ /* Llib/weakhash.scm 508 */
 long BgL_n1z00_3427; long BgL_n2z00_3428;
{ /* Llib/weakhash.scm 508 */
 obj_t BgL_tmpz00_9350;
{ /* Llib/weakhash.scm 508 */
 obj_t BgL_aux3067z00_5314;
BgL_aux3067z00_5314 = 
CELL_REF(BgL_countz00_4523); 
if(
INTEGERP(BgL_aux3067z00_5314))
{ /* Llib/weakhash.scm 508 */
BgL_tmpz00_9350 = BgL_aux3067z00_5314
; }  else 
{ 
 obj_t BgL_auxz00_9353;
BgL_auxz00_9353 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(20065L), BGl_string3539z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3067z00_5314); 
FAILURE(BgL_auxz00_9353,BFALSE,BFALSE);} } 
BgL_n1z00_3427 = 
(long)CINT(BgL_tmpz00_9350); } 
{ /* Llib/weakhash.scm 508 */
 obj_t BgL_tmpz00_9358;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1709))
{ /* Llib/weakhash.scm 508 */
BgL_tmpz00_9358 = BgL_maxzd2bucketzd2lenz00_1709
; }  else 
{ 
 obj_t BgL_auxz00_9361;
BgL_auxz00_9361 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(20071L), BGl_string3539z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1709); 
FAILURE(BgL_auxz00_9361,BFALSE,BFALSE);} 
BgL_n2z00_3428 = 
(long)CINT(BgL_tmpz00_9358); } 
BgL_test4247z00_9349 = 
(BgL_n1z00_3427>BgL_n2z00_3428); } 
if(BgL_test4247z00_9349)
{ /* Llib/weakhash.scm 508 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_86))
{ /* Llib/weakhash.scm 877 */
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_86); }  else 
{ /* Llib/weakhash.scm 877 */
BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_86); } }  else 
{ /* Llib/weakhash.scm 508 */BFALSE; } } 
return BgL_objz00_88;}  else 
{ /* Llib/weakhash.scm 493 */
return BgL_foundz00_1711;} } } } } } } } } } 

}



/* &<@anonymous:1602> */
obj_t BGl_z62zc3z04anonymousza31602ze3ze5zz__weakhashz00(obj_t BgL_envz00_4514, obj_t BgL_bkeyz00_4519, obj_t BgL_valz00_4520, obj_t BgL_bucketz00_4521)
{
{ /* Llib/weakhash.scm 481 */
{ /* Llib/weakhash.scm 482 */
 obj_t BgL_countz00_4515; obj_t BgL_objz00_4516; obj_t BgL_tablez00_4517; obj_t BgL_keyz00_4518;
BgL_countz00_4515 = 
PROCEDURE_L_REF(BgL_envz00_4514, 
(int)(0L)); 
BgL_objz00_4516 = 
PROCEDURE_L_REF(BgL_envz00_4514, 
(int)(1L)); 
BgL_tablez00_4517 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4514, 
(int)(2L))); 
BgL_keyz00_4518 = 
PROCEDURE_L_REF(BgL_envz00_4514, 
(int)(3L)); 
{ /* Llib/weakhash.scm 482 */
 obj_t BgL_auxz00_5753;
{ /* Llib/weakhash.scm 482 */
 obj_t BgL_tmpz00_9380;
{ /* Llib/weakhash.scm 482 */
 obj_t BgL_aux3069z00_5754;
BgL_aux3069z00_5754 = 
CELL_REF(BgL_countz00_4515); 
if(
INTEGERP(BgL_aux3069z00_5754))
{ /* Llib/weakhash.scm 482 */
BgL_tmpz00_9380 = BgL_aux3069z00_5754
; }  else 
{ 
 obj_t BgL_auxz00_9383;
BgL_auxz00_9383 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19302L), BGl_string3540z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3069z00_5754); 
FAILURE(BgL_auxz00_9383,BFALSE,BFALSE);} } 
BgL_auxz00_5753 = 
ADDFX(BgL_tmpz00_9380, 
BINT(1L)); } 
CELL_SET(BgL_countz00_4515, BgL_auxz00_5753); } 
{ /* Llib/weakhash.scm 483 */
 bool_t BgL_test4252z00_9389;
{ /* Llib/weakhash.scm 483 */
 obj_t BgL_eqtz00_5755;
{ /* Llib/weakhash.scm 483 */
 bool_t BgL_test4253z00_9390;
{ /* Llib/weakhash.scm 483 */
 obj_t BgL_tmpz00_9391;
{ /* Llib/weakhash.scm 483 */
 obj_t BgL_res2265z00_5756;
{ /* Llib/weakhash.scm 483 */
 obj_t BgL_aux3070z00_5757;
BgL_aux3070z00_5757 = 
STRUCT_KEY(BgL_tablez00_4517); 
if(
SYMBOLP(BgL_aux3070z00_5757))
{ /* Llib/weakhash.scm 483 */
BgL_res2265z00_5756 = BgL_aux3070z00_5757; }  else 
{ 
 obj_t BgL_auxz00_9395;
BgL_auxz00_9395 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19325L), BGl_string3540z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3070z00_5757); 
FAILURE(BgL_auxz00_9395,BFALSE,BFALSE);} } 
BgL_tmpz00_9391 = BgL_res2265z00_5756; } 
BgL_test4253z00_9390 = 
(BgL_tmpz00_9391==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4253z00_9390)
{ /* Llib/weakhash.scm 483 */
 int BgL_tmpz00_9400;
BgL_tmpz00_9400 = 
(int)(3L); 
BgL_eqtz00_5755 = 
STRUCT_REF(BgL_tablez00_4517, BgL_tmpz00_9400); }  else 
{ /* Llib/weakhash.scm 483 */
BgL_eqtz00_5755 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_4517); } } 
if(
PROCEDUREP(BgL_eqtz00_5755))
{ /* Llib/weakhash.scm 483 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_5755, 2))
{ /* Llib/weakhash.scm 483 */
BgL_test4252z00_9389 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_5755, BgL_bkeyz00_4519, BgL_keyz00_4518))
; }  else 
{ /* Llib/weakhash.scm 483 */
FAILURE(BGl_string3541z00zz__weakhashz00,BGl_list3542z00zz__weakhashz00,BgL_eqtz00_5755);} }  else 
{ /* Llib/weakhash.scm 483 */
if(
(BgL_bkeyz00_4519==BgL_keyz00_4518))
{ /* Llib/weakhash.scm 483 */
BgL_test4252z00_9389 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 483 */
if(
STRINGP(BgL_bkeyz00_4519))
{ /* Llib/weakhash.scm 483 */
if(
STRINGP(BgL_keyz00_4518))
{ /* Llib/weakhash.scm 483 */
 long BgL_l1z00_5758;
BgL_l1z00_5758 = 
STRING_LENGTH(BgL_bkeyz00_4519); 
if(
(BgL_l1z00_5758==
STRING_LENGTH(BgL_keyz00_4518)))
{ /* Llib/weakhash.scm 483 */
 int BgL_arg1918z00_5759;
{ /* Llib/weakhash.scm 483 */
 char * BgL_auxz00_9427; char * BgL_tmpz00_9425;
BgL_auxz00_9427 = 
BSTRING_TO_STRING(BgL_keyz00_4518); 
BgL_tmpz00_9425 = 
BSTRING_TO_STRING(BgL_bkeyz00_4519); 
BgL_arg1918z00_5759 = 
memcmp(BgL_tmpz00_9425, BgL_auxz00_9427, BgL_l1z00_5758); } 
BgL_test4252z00_9389 = 
(
(long)(BgL_arg1918z00_5759)==0L); }  else 
{ /* Llib/weakhash.scm 483 */
BgL_test4252z00_9389 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 483 */
BgL_test4252z00_9389 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 483 */
BgL_test4252z00_9389 = ((bool_t)0)
; } } } } 
if(BgL_test4252z00_9389)
{ /* Llib/weakhash.scm 483 */
{ /* Llib/weakhash.scm 485 */
 obj_t BgL_arg1605z00_5760; obj_t BgL_arg1606z00_5761;
{ /* Llib/weakhash.scm 485 */
 obj_t BgL_pairz00_5762;
if(
PAIRP(BgL_bucketz00_4521))
{ /* Llib/weakhash.scm 485 */
BgL_pairz00_5762 = BgL_bucketz00_4521; }  else 
{ 
 obj_t BgL_auxz00_9434;
BgL_auxz00_9434 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19396L), BGl_string3540z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_4521); 
FAILURE(BgL_auxz00_9434,BFALSE,BFALSE);} 
BgL_arg1605z00_5760 = 
CAR(BgL_pairz00_5762); } 
if(
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_tablez00_4517))
{ /* Llib/weakhash.scm 487 */

BgL_arg1606z00_5761 = 
bgl_make_weakptr(BgL_objz00_4516, BFALSE); }  else 
{ /* Llib/weakhash.scm 486 */
BgL_arg1606z00_5761 = BgL_objz00_4516; } 
{ /* Llib/weakhash.scm 485 */
 obj_t BgL_pairz00_5763;
if(
PAIRP(BgL_arg1605z00_5760))
{ /* Llib/weakhash.scm 485 */
BgL_pairz00_5763 = BgL_arg1605z00_5760; }  else 
{ 
 obj_t BgL_auxz00_9444;
BgL_auxz00_9444 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(19402L), BGl_string3540z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_arg1605z00_5760); 
FAILURE(BgL_auxz00_9444,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_5763, BgL_arg1606z00_5761); } } 
return BgL_valz00_4520;}  else 
{ /* Llib/weakhash.scm 483 */
return BGl_keepgoingz00zz__weakhashz00;} } } } 

}



/* weak-hashtable-put! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(obj_t BgL_tablez00_89, obj_t BgL_keyz00_90, obj_t BgL_objz00_91)
{
{ /* Llib/weakhash.scm 515 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_89))
{ /* Llib/weakhash.scm 516 */
BGL_TAIL return 
BGl_weakzd2keyszd2hashtablezd2putz12zc0zz__weakhashz00(BgL_tablez00_89, BgL_keyz00_90, BgL_objz00_91);}  else 
{ /* Llib/weakhash.scm 516 */
BGL_TAIL return 
BGl_weakzd2oldzd2hashtablezd2putz12zc0zz__weakhashz00(BgL_tablez00_89, BgL_keyz00_90, BgL_objz00_91);} } 

}



/* &weak-hashtable-put! */
obj_t BGl_z62weakzd2hashtablezd2putz12z70zz__weakhashz00(obj_t BgL_envz00_4525, obj_t BgL_tablez00_4526, obj_t BgL_keyz00_4527, obj_t BgL_objz00_4528)
{
{ /* Llib/weakhash.scm 515 */
{ /* Llib/weakhash.scm 516 */
 obj_t BgL_auxz00_9453;
if(
STRUCTP(BgL_tablez00_4526))
{ /* Llib/weakhash.scm 516 */
BgL_auxz00_9453 = BgL_tablez00_4526
; }  else 
{ 
 obj_t BgL_auxz00_9456;
BgL_auxz00_9456 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(20423L), BGl_string3543z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4526); 
FAILURE(BgL_auxz00_9456,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2putz12z12zz__weakhashz00(BgL_auxz00_9453, BgL_keyz00_4527, BgL_objz00_4528);} } 

}



/* weak-keys-hashtable-update! */
obj_t BGl_weakzd2keyszd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t BgL_tablez00_92, obj_t BgL_keyz00_93, obj_t BgL_procz00_94, obj_t BgL_objz00_95)
{
{ /* Llib/weakhash.scm 523 */
{ /* Llib/weakhash.scm 524 */
 obj_t BgL_bucketsz00_1741;
{ /* Llib/weakhash.scm 524 */
 bool_t BgL_test4266z00_9461;
{ /* Llib/weakhash.scm 524 */
 obj_t BgL_tmpz00_9462;
{ /* Llib/weakhash.scm 524 */
 obj_t BgL_res2269z00_3435;
{ /* Llib/weakhash.scm 524 */
 obj_t BgL_aux3079z00_5327;
BgL_aux3079z00_5327 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3079z00_5327))
{ /* Llib/weakhash.scm 524 */
BgL_res2269z00_3435 = BgL_aux3079z00_5327; }  else 
{ 
 obj_t BgL_auxz00_9466;
BgL_auxz00_9466 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(20876L), BGl_string3544z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3079z00_5327); 
FAILURE(BgL_auxz00_9466,BFALSE,BFALSE);} } 
BgL_tmpz00_9462 = BgL_res2269z00_3435; } 
BgL_test4266z00_9461 = 
(BgL_tmpz00_9462==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4266z00_9461)
{ /* Llib/weakhash.scm 524 */
 int BgL_tmpz00_9471;
BgL_tmpz00_9471 = 
(int)(2L); 
BgL_bucketsz00_1741 = 
STRUCT_REF(BgL_tablez00_92, BgL_tmpz00_9471); }  else 
{ /* Llib/weakhash.scm 524 */
BgL_bucketsz00_1741 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } 
{ /* Llib/weakhash.scm 524 */
 long BgL_bucketzd2lenzd2_1742;
{ /* Llib/weakhash.scm 525 */
 obj_t BgL_vectorz00_3436;
if(
VECTORP(BgL_bucketsz00_1741))
{ /* Llib/weakhash.scm 525 */
BgL_vectorz00_3436 = BgL_bucketsz00_1741; }  else 
{ 
 obj_t BgL_auxz00_9477;
BgL_auxz00_9477 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(20934L), BGl_string3544z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1741); 
FAILURE(BgL_auxz00_9477,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1742 = 
VECTOR_LENGTH(BgL_vectorz00_3436); } 
{ /* Llib/weakhash.scm 525 */
 long BgL_bucketzd2numzd2_1743;
{ /* Llib/weakhash.scm 526 */
 long BgL_arg1637z00_1774;
BgL_arg1637z00_1774 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_92, BgL_keyz00_93); 
{ /* Llib/weakhash.scm 526 */
 long BgL_n1z00_3437; long BgL_n2z00_3438;
BgL_n1z00_3437 = BgL_arg1637z00_1774; 
BgL_n2z00_3438 = BgL_bucketzd2lenzd2_1742; 
{ /* Llib/weakhash.scm 526 */
 bool_t BgL_test4269z00_9483;
{ /* Llib/weakhash.scm 526 */
 long BgL_arg2024z00_3440;
BgL_arg2024z00_3440 = 
(((BgL_n1z00_3437) | (BgL_n2z00_3438)) & -2147483648); 
BgL_test4269z00_9483 = 
(BgL_arg2024z00_3440==0L); } 
if(BgL_test4269z00_9483)
{ /* Llib/weakhash.scm 526 */
 int32_t BgL_arg2020z00_3441;
{ /* Llib/weakhash.scm 526 */
 int32_t BgL_arg2021z00_3442; int32_t BgL_arg2022z00_3443;
BgL_arg2021z00_3442 = 
(int32_t)(BgL_n1z00_3437); 
BgL_arg2022z00_3443 = 
(int32_t)(BgL_n2z00_3438); 
BgL_arg2020z00_3441 = 
(BgL_arg2021z00_3442%BgL_arg2022z00_3443); } 
{ /* Llib/weakhash.scm 526 */
 long BgL_arg2135z00_3448;
BgL_arg2135z00_3448 = 
(long)(BgL_arg2020z00_3441); 
BgL_bucketzd2numzd2_1743 = 
(long)(BgL_arg2135z00_3448); } }  else 
{ /* Llib/weakhash.scm 526 */
BgL_bucketzd2numzd2_1743 = 
(BgL_n1z00_3437%BgL_n2z00_3438); } } } } 
{ /* Llib/weakhash.scm 526 */
 obj_t BgL_bucketz00_1744;
{ /* Llib/weakhash.scm 527 */
 obj_t BgL_vectorz00_3450;
if(
VECTORP(BgL_bucketsz00_1741))
{ /* Llib/weakhash.scm 527 */
BgL_vectorz00_3450 = BgL_bucketsz00_1741; }  else 
{ 
 obj_t BgL_auxz00_9494;
BgL_auxz00_9494 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21044L), BGl_string3544z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1741); 
FAILURE(BgL_auxz00_9494,BFALSE,BFALSE);} 
BgL_bucketz00_1744 = 
VECTOR_REF(BgL_vectorz00_3450,BgL_bucketzd2numzd2_1743); } 
{ /* Llib/weakhash.scm 527 */
 obj_t BgL_maxzd2bucketzd2lenz00_1745;
{ /* Llib/weakhash.scm 528 */
 bool_t BgL_test4271z00_9499;
{ /* Llib/weakhash.scm 528 */
 obj_t BgL_tmpz00_9500;
{ /* Llib/weakhash.scm 528 */
 obj_t BgL_res2270z00_3455;
{ /* Llib/weakhash.scm 528 */
 obj_t BgL_aux3085z00_5333;
BgL_aux3085z00_5333 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3085z00_5333))
{ /* Llib/weakhash.scm 528 */
BgL_res2270z00_3455 = BgL_aux3085z00_5333; }  else 
{ 
 obj_t BgL_auxz00_9504;
BgL_auxz00_9504 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21084L), BGl_string3544z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3085z00_5333); 
FAILURE(BgL_auxz00_9504,BFALSE,BFALSE);} } 
BgL_tmpz00_9500 = BgL_res2270z00_3455; } 
BgL_test4271z00_9499 = 
(BgL_tmpz00_9500==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4271z00_9499)
{ /* Llib/weakhash.scm 528 */
 int BgL_tmpz00_9509;
BgL_tmpz00_9509 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1745 = 
STRUCT_REF(BgL_tablez00_92, BgL_tmpz00_9509); }  else 
{ /* Llib/weakhash.scm 528 */
BgL_maxzd2bucketzd2lenz00_1745 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } 
{ /* Llib/weakhash.scm 528 */

if(
NULLP(BgL_bucketz00_1744))
{ /* Llib/weakhash.scm 529 */
{ /* Llib/weakhash.scm 531 */
 long BgL_arg1611z00_1747;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_arg1612z00_1748;
{ /* Llib/weakhash.scm 531 */
 bool_t BgL_test4274z00_9515;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_tmpz00_9516;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_res2271z00_3459;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_aux3087z00_5335;
BgL_aux3087z00_5335 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3087z00_5335))
{ /* Llib/weakhash.scm 531 */
BgL_res2271z00_3459 = BgL_aux3087z00_5335; }  else 
{ 
 obj_t BgL_auxz00_9520;
BgL_auxz00_9520 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21194L), BGl_string3544z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3087z00_5335); 
FAILURE(BgL_auxz00_9520,BFALSE,BFALSE);} } 
BgL_tmpz00_9516 = BgL_res2271z00_3459; } 
BgL_test4274z00_9515 = 
(BgL_tmpz00_9516==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4274z00_9515)
{ /* Llib/weakhash.scm 531 */
 int BgL_tmpz00_9525;
BgL_tmpz00_9525 = 
(int)(0L); 
BgL_arg1612z00_1748 = 
STRUCT_REF(BgL_tablez00_92, BgL_tmpz00_9525); }  else 
{ /* Llib/weakhash.scm 531 */
BgL_arg1612z00_1748 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } 
{ /* Llib/weakhash.scm 531 */
 long BgL_za71za7_3460;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_tmpz00_9529;
if(
INTEGERP(BgL_arg1612z00_1748))
{ /* Llib/weakhash.scm 531 */
BgL_tmpz00_9529 = BgL_arg1612z00_1748
; }  else 
{ 
 obj_t BgL_auxz00_9532;
BgL_auxz00_9532 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21216L), BGl_string3544z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1612z00_1748); 
FAILURE(BgL_auxz00_9532,BFALSE,BFALSE);} 
BgL_za71za7_3460 = 
(long)CINT(BgL_tmpz00_9529); } 
BgL_arg1611z00_1747 = 
(BgL_za71za7_3460+1L); } } 
{ /* Llib/weakhash.scm 531 */
 bool_t BgL_test4277z00_9538;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_tmpz00_9539;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_res2272z00_3464;
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_aux3090z00_5338;
BgL_aux3090z00_5338 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3090z00_5338))
{ /* Llib/weakhash.scm 531 */
BgL_res2272z00_3464 = BgL_aux3090z00_5338; }  else 
{ 
 obj_t BgL_auxz00_9543;
BgL_auxz00_9543 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21161L), BGl_string3544z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3090z00_5338); 
FAILURE(BgL_auxz00_9543,BFALSE,BFALSE);} } 
BgL_tmpz00_9539 = BgL_res2272z00_3464; } 
BgL_test4277z00_9538 = 
(BgL_tmpz00_9539==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4277z00_9538)
{ /* Llib/weakhash.scm 531 */
 obj_t BgL_auxz00_9550; int BgL_tmpz00_9548;
BgL_auxz00_9550 = 
BINT(BgL_arg1611z00_1747); 
BgL_tmpz00_9548 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_92, BgL_tmpz00_9548, BgL_auxz00_9550); }  else 
{ /* Llib/weakhash.scm 531 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } } 
{ /* Llib/weakhash.scm 532 */
 obj_t BgL_arg1613z00_1749;
{ /* Llib/weakhash.scm 532 */
 obj_t BgL_arg1615z00_1750;
BgL_arg1615z00_1750 = 
MAKE_YOUNG_PAIR(BgL_keyz00_93, BgL_objz00_95); 
{ /* Llib/weakhash.scm 532 */
 obj_t BgL_list1616z00_1751;
BgL_list1616z00_1751 = 
MAKE_YOUNG_PAIR(BgL_arg1615z00_1750, BNIL); 
BgL_arg1613z00_1749 = BgL_list1616z00_1751; } } 
{ /* Llib/weakhash.scm 532 */
 obj_t BgL_vectorz00_3466;
if(
VECTORP(BgL_bucketsz00_1741))
{ /* Llib/weakhash.scm 532 */
BgL_vectorz00_3466 = BgL_bucketsz00_1741; }  else 
{ 
 obj_t BgL_auxz00_9558;
BgL_auxz00_9558 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21244L), BGl_string3544z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1741); 
FAILURE(BgL_auxz00_9558,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3466,BgL_bucketzd2numzd2_1743,BgL_arg1613z00_1749); } } 
return BgL_objz00_95;}  else 
{ 
 obj_t BgL_buckz00_1753; long BgL_countz00_1754;
BgL_buckz00_1753 = BgL_bucketz00_1744; 
BgL_countz00_1754 = 0L; 
BgL_zc3z04anonymousza31617ze3z87_1755:
if(
NULLP(BgL_buckz00_1753))
{ /* Llib/weakhash.scm 537 */
{ /* Llib/weakhash.scm 538 */
 long BgL_arg1619z00_1757;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_arg1620z00_1758;
{ /* Llib/weakhash.scm 538 */
 bool_t BgL_test4281z00_9565;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_tmpz00_9566;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_res2274z00_3471;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_aux3094z00_5342;
BgL_aux3094z00_5342 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3094z00_5342))
{ /* Llib/weakhash.scm 538 */
BgL_res2274z00_3471 = BgL_aux3094z00_5342; }  else 
{ 
 obj_t BgL_auxz00_9570;
BgL_auxz00_9570 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21407L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3094z00_5342); 
FAILURE(BgL_auxz00_9570,BFALSE,BFALSE);} } 
BgL_tmpz00_9566 = BgL_res2274z00_3471; } 
BgL_test4281z00_9565 = 
(BgL_tmpz00_9566==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4281z00_9565)
{ /* Llib/weakhash.scm 538 */
 int BgL_tmpz00_9575;
BgL_tmpz00_9575 = 
(int)(0L); 
BgL_arg1620z00_1758 = 
STRUCT_REF(BgL_tablez00_92, BgL_tmpz00_9575); }  else 
{ /* Llib/weakhash.scm 538 */
BgL_arg1620z00_1758 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } 
{ /* Llib/weakhash.scm 538 */
 long BgL_za71za7_3472;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_tmpz00_9579;
if(
INTEGERP(BgL_arg1620z00_1758))
{ /* Llib/weakhash.scm 538 */
BgL_tmpz00_9579 = BgL_arg1620z00_1758
; }  else 
{ 
 obj_t BgL_auxz00_9582;
BgL_auxz00_9582 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21429L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1620z00_1758); 
FAILURE(BgL_auxz00_9582,BFALSE,BFALSE);} 
BgL_za71za7_3472 = 
(long)CINT(BgL_tmpz00_9579); } 
BgL_arg1619z00_1757 = 
(BgL_za71za7_3472+1L); } } 
{ /* Llib/weakhash.scm 538 */
 bool_t BgL_test4284z00_9588;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_tmpz00_9589;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_res2275z00_3476;
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_aux3097z00_5345;
BgL_aux3097z00_5345 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3097z00_5345))
{ /* Llib/weakhash.scm 538 */
BgL_res2275z00_3476 = BgL_aux3097z00_5345; }  else 
{ 
 obj_t BgL_auxz00_9593;
BgL_auxz00_9593 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21374L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3097z00_5345); 
FAILURE(BgL_auxz00_9593,BFALSE,BFALSE);} } 
BgL_tmpz00_9589 = BgL_res2275z00_3476; } 
BgL_test4284z00_9588 = 
(BgL_tmpz00_9589==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4284z00_9588)
{ /* Llib/weakhash.scm 538 */
 obj_t BgL_auxz00_9600; int BgL_tmpz00_9598;
BgL_auxz00_9600 = 
BINT(BgL_arg1619z00_1757); 
BgL_tmpz00_9598 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_92, BgL_tmpz00_9598, BgL_auxz00_9600); }  else 
{ /* Llib/weakhash.scm 538 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } } 
{ /* Llib/weakhash.scm 540 */
 obj_t BgL_arg1621z00_1759;
BgL_arg1621z00_1759 = 
MAKE_YOUNG_PAIR(
bgl_make_weakptr(BgL_keyz00_93, BgL_objz00_95), BgL_bucketz00_1744); 
{ /* Llib/weakhash.scm 539 */
 obj_t BgL_vectorz00_3477;
if(
VECTORP(BgL_bucketsz00_1741))
{ /* Llib/weakhash.scm 539 */
BgL_vectorz00_3477 = BgL_bucketsz00_1741; }  else 
{ 
 obj_t BgL_auxz00_9608;
BgL_auxz00_9608 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21454L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1741); 
FAILURE(BgL_auxz00_9608,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3477,BgL_bucketzd2numzd2_1743,BgL_arg1621z00_1759); } } 
{ /* Llib/weakhash.scm 541 */
 bool_t BgL_test4287z00_9613;
{ /* Llib/weakhash.scm 541 */
 long BgL_n2z00_3480;
{ /* Llib/weakhash.scm 541 */
 obj_t BgL_tmpz00_9614;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1745))
{ /* Llib/weakhash.scm 541 */
BgL_tmpz00_9614 = BgL_maxzd2bucketzd2lenz00_1745
; }  else 
{ 
 obj_t BgL_auxz00_9617;
BgL_auxz00_9617 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21537L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1745); 
FAILURE(BgL_auxz00_9617,BFALSE,BFALSE);} 
BgL_n2z00_3480 = 
(long)CINT(BgL_tmpz00_9614); } 
BgL_test4287z00_9613 = 
(BgL_countz00_1754>BgL_n2z00_3480); } 
if(BgL_test4287z00_9613)
{ /* Llib/weakhash.scm 541 */
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_92); }  else 
{ /* Llib/weakhash.scm 541 */BFALSE; } } 
return BgL_objz00_95;}  else 
{ /* Llib/weakhash.scm 544 */
 bool_t BgL_test4289z00_9624;
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_arg1634z00_1771;
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_arg1636z00_1772;
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_pairz00_3481;
if(
PAIRP(BgL_buckz00_1753))
{ /* Llib/weakhash.scm 544 */
BgL_pairz00_3481 = BgL_buckz00_1753; }  else 
{ 
 obj_t BgL_auxz00_9627;
BgL_auxz00_9627 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21650L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1753); 
FAILURE(BgL_auxz00_9627,BFALSE,BFALSE);} 
BgL_arg1636z00_1772 = 
CAR(BgL_pairz00_3481); } 
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_objz00_3482;
if(
BGL_WEAKPTRP(BgL_arg1636z00_1772))
{ /* Llib/weakhash.scm 544 */
BgL_objz00_3482 = BgL_arg1636z00_1772; }  else 
{ 
 obj_t BgL_auxz00_9634;
BgL_auxz00_9634 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21654L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1636z00_1772); 
FAILURE(BgL_auxz00_9634,BFALSE,BFALSE);} 
BgL_arg1634z00_1771 = 
bgl_weakptr_data(BgL_objz00_3482); } } 
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_eqtz00_3483;
{ /* Llib/weakhash.scm 544 */
 bool_t BgL_test4292z00_9639;
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_tmpz00_9640;
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_res2276z00_3490;
{ /* Llib/weakhash.scm 544 */
 obj_t BgL_aux3106z00_5354;
BgL_aux3106z00_5354 = 
STRUCT_KEY(BgL_tablez00_92); 
if(
SYMBOLP(BgL_aux3106z00_5354))
{ /* Llib/weakhash.scm 544 */
BgL_res2276z00_3490 = BgL_aux3106z00_5354; }  else 
{ 
 obj_t BgL_auxz00_9644;
BgL_auxz00_9644 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21607L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3106z00_5354); 
FAILURE(BgL_auxz00_9644,BFALSE,BFALSE);} } 
BgL_tmpz00_9640 = BgL_res2276z00_3490; } 
BgL_test4292z00_9639 = 
(BgL_tmpz00_9640==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4292z00_9639)
{ /* Llib/weakhash.scm 544 */
 int BgL_tmpz00_9649;
BgL_tmpz00_9649 = 
(int)(3L); 
BgL_eqtz00_3483 = 
STRUCT_REF(BgL_tablez00_92, BgL_tmpz00_9649); }  else 
{ /* Llib/weakhash.scm 544 */
BgL_eqtz00_3483 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_92); } } 
if(
PROCEDUREP(BgL_eqtz00_3483))
{ /* Llib/weakhash.scm 544 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3483, 2))
{ /* Llib/weakhash.scm 544 */
BgL_test4289z00_9624 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3483, BgL_arg1634z00_1771, BgL_keyz00_93))
; }  else 
{ /* Llib/weakhash.scm 544 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3545z00zz__weakhashz00,BgL_eqtz00_3483);} }  else 
{ /* Llib/weakhash.scm 544 */
if(
(BgL_arg1634z00_1771==BgL_keyz00_93))
{ /* Llib/weakhash.scm 544 */
BgL_test4289z00_9624 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 544 */
if(
STRINGP(BgL_arg1634z00_1771))
{ /* Llib/weakhash.scm 544 */
if(
STRINGP(BgL_keyz00_93))
{ /* Llib/weakhash.scm 544 */
 long BgL_l1z00_3493;
BgL_l1z00_3493 = 
STRING_LENGTH(BgL_arg1634z00_1771); 
if(
(BgL_l1z00_3493==
STRING_LENGTH(BgL_keyz00_93)))
{ /* Llib/weakhash.scm 544 */
 int BgL_arg1918z00_3496;
{ /* Llib/weakhash.scm 544 */
 char * BgL_auxz00_9676; char * BgL_tmpz00_9674;
BgL_auxz00_9676 = 
BSTRING_TO_STRING(BgL_keyz00_93); 
BgL_tmpz00_9674 = 
BSTRING_TO_STRING(BgL_arg1634z00_1771); 
BgL_arg1918z00_3496 = 
memcmp(BgL_tmpz00_9674, BgL_auxz00_9676, BgL_l1z00_3493); } 
BgL_test4289z00_9624 = 
(
(long)(BgL_arg1918z00_3496)==0L); }  else 
{ /* Llib/weakhash.scm 544 */
BgL_test4289z00_9624 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 544 */
BgL_test4289z00_9624 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 544 */
BgL_test4289z00_9624 = ((bool_t)0)
; } } } } } 
if(BgL_test4289z00_9624)
{ /* Llib/weakhash.scm 545 */
 obj_t BgL_resz00_1765;
{ /* Llib/weakhash.scm 545 */
 obj_t BgL_arg1628z00_1767;
{ /* Llib/weakhash.scm 545 */
 obj_t BgL_arg1629z00_1768;
{ /* Llib/weakhash.scm 545 */
 obj_t BgL_pairz00_3502;
if(
PAIRP(BgL_buckz00_1753))
{ /* Llib/weakhash.scm 545 */
BgL_pairz00_3502 = BgL_buckz00_1753; }  else 
{ 
 obj_t BgL_auxz00_9683;
BgL_auxz00_9683 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21700L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1753); 
FAILURE(BgL_auxz00_9683,BFALSE,BFALSE);} 
BgL_arg1629z00_1768 = 
CAR(BgL_pairz00_3502); } 
{ /* Llib/weakhash.scm 545 */
 obj_t BgL_objz00_3503;
if(
BGL_WEAKPTRP(BgL_arg1629z00_1768))
{ /* Llib/weakhash.scm 545 */
BgL_objz00_3503 = BgL_arg1629z00_1768; }  else 
{ 
 obj_t BgL_auxz00_9690;
BgL_auxz00_9690 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21704L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1629z00_1768); 
FAILURE(BgL_auxz00_9690,BFALSE,BFALSE);} 
BgL_arg1628z00_1767 = 
bgl_weakptr_ref(BgL_objz00_3503); } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_94, 1))
{ /* Llib/weakhash.scm 545 */
BgL_resz00_1765 = 
BGL_PROCEDURE_CALL1(BgL_procz00_94, BgL_arg1628z00_1767); }  else 
{ /* Llib/weakhash.scm 545 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3548z00zz__weakhashz00,BgL_procz00_94);} } 
{ /* Llib/weakhash.scm 546 */
 obj_t BgL_arg1627z00_1766;
{ /* Llib/weakhash.scm 546 */
 obj_t BgL_pairz00_3504;
if(
PAIRP(BgL_buckz00_1753))
{ /* Llib/weakhash.scm 546 */
BgL_pairz00_3504 = BgL_buckz00_1753; }  else 
{ 
 obj_t BgL_auxz00_9704;
BgL_auxz00_9704 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21739L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1753); 
FAILURE(BgL_auxz00_9704,BFALSE,BFALSE);} 
BgL_arg1627z00_1766 = 
CAR(BgL_pairz00_3504); } 
{ /* Llib/weakhash.scm 546 */
 obj_t BgL_ptrz00_3505;
if(
BGL_WEAKPTRP(BgL_arg1627z00_1766))
{ /* Llib/weakhash.scm 546 */
BgL_ptrz00_3505 = BgL_arg1627z00_1766; }  else 
{ 
 obj_t BgL_auxz00_9711;
BgL_auxz00_9711 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21743L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1627z00_1766); 
FAILURE(BgL_auxz00_9711,BFALSE,BFALSE);} 
bgl_weakptr_ref_set(BgL_ptrz00_3505, BgL_resz00_1765); BUNSPEC; BUNSPEC; } } 
return BgL_resz00_1765;}  else 
{ /* Llib/weakhash.scm 549 */
 obj_t BgL_arg1630z00_1769; long BgL_arg1631z00_1770;
{ /* Llib/weakhash.scm 549 */
 obj_t BgL_pairz00_3506;
if(
PAIRP(BgL_buckz00_1753))
{ /* Llib/weakhash.scm 549 */
BgL_pairz00_3506 = BgL_buckz00_1753; }  else 
{ 
 obj_t BgL_auxz00_9718;
BgL_auxz00_9718 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(21784L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1753); 
FAILURE(BgL_auxz00_9718,BFALSE,BFALSE);} 
BgL_arg1630z00_1769 = 
CDR(BgL_pairz00_3506); } 
BgL_arg1631z00_1770 = 
(BgL_countz00_1754+1L); 
{ 
 long BgL_countz00_9725; obj_t BgL_buckz00_9724;
BgL_buckz00_9724 = BgL_arg1630z00_1769; 
BgL_countz00_9725 = BgL_arg1631z00_1770; 
BgL_countz00_1754 = BgL_countz00_9725; 
BgL_buckz00_1753 = BgL_buckz00_9724; 
goto BgL_zc3z04anonymousza31617ze3z87_1755;} } } } } } } } } } } 

}



/* weak-old-hashtable-update! */
obj_t BGl_weakzd2oldzd2hashtablezd2updatez12zc0zz__weakhashz00(obj_t BgL_tablez00_96, obj_t BgL_keyz00_97, obj_t BgL_procz00_98, obj_t BgL_objz00_99)
{
{ /* Llib/weakhash.scm 554 */
{ /* Llib/weakhash.scm 555 */
 obj_t BgL_bucketsz00_1775;
{ /* Llib/weakhash.scm 555 */
 bool_t BgL_test4306z00_9726;
{ /* Llib/weakhash.scm 555 */
 obj_t BgL_tmpz00_9727;
{ /* Llib/weakhash.scm 555 */
 obj_t BgL_res2277z00_3511;
{ /* Llib/weakhash.scm 555 */
 obj_t BgL_aux3120z00_5370;
BgL_aux3120z00_5370 = 
STRUCT_KEY(BgL_tablez00_96); 
if(
SYMBOLP(BgL_aux3120z00_5370))
{ /* Llib/weakhash.scm 555 */
BgL_res2277z00_3511 = BgL_aux3120z00_5370; }  else 
{ 
 obj_t BgL_auxz00_9731;
BgL_auxz00_9731 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22136L), BGl_string3553z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3120z00_5370); 
FAILURE(BgL_auxz00_9731,BFALSE,BFALSE);} } 
BgL_tmpz00_9727 = BgL_res2277z00_3511; } 
BgL_test4306z00_9726 = 
(BgL_tmpz00_9727==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4306z00_9726)
{ /* Llib/weakhash.scm 555 */
 int BgL_tmpz00_9736;
BgL_tmpz00_9736 = 
(int)(2L); 
BgL_bucketsz00_1775 = 
STRUCT_REF(BgL_tablez00_96, BgL_tmpz00_9736); }  else 
{ /* Llib/weakhash.scm 555 */
BgL_bucketsz00_1775 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_96); } } 
{ /* Llib/weakhash.scm 555 */
 long BgL_bucketzd2lenzd2_1776;
{ /* Llib/weakhash.scm 556 */
 obj_t BgL_vectorz00_3512;
if(
VECTORP(BgL_bucketsz00_1775))
{ /* Llib/weakhash.scm 556 */
BgL_vectorz00_3512 = BgL_bucketsz00_1775; }  else 
{ 
 obj_t BgL_auxz00_9742;
BgL_auxz00_9742 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22194L), BGl_string3553z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1775); 
FAILURE(BgL_auxz00_9742,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1776 = 
VECTOR_LENGTH(BgL_vectorz00_3512); } 
{ /* Llib/weakhash.scm 556 */
 long BgL_bucketzd2numzd2_1777;
{ /* Llib/weakhash.scm 557 */
 long BgL_arg1656z00_1810;
BgL_arg1656z00_1810 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_96, BgL_keyz00_97); 
{ /* Llib/weakhash.scm 557 */
 long BgL_n1z00_3513; long BgL_n2z00_3514;
BgL_n1z00_3513 = BgL_arg1656z00_1810; 
BgL_n2z00_3514 = BgL_bucketzd2lenzd2_1776; 
{ /* Llib/weakhash.scm 557 */
 bool_t BgL_test4309z00_9748;
{ /* Llib/weakhash.scm 557 */
 long BgL_arg2024z00_3516;
BgL_arg2024z00_3516 = 
(((BgL_n1z00_3513) | (BgL_n2z00_3514)) & -2147483648); 
BgL_test4309z00_9748 = 
(BgL_arg2024z00_3516==0L); } 
if(BgL_test4309z00_9748)
{ /* Llib/weakhash.scm 557 */
 int32_t BgL_arg2020z00_3517;
{ /* Llib/weakhash.scm 557 */
 int32_t BgL_arg2021z00_3518; int32_t BgL_arg2022z00_3519;
BgL_arg2021z00_3518 = 
(int32_t)(BgL_n1z00_3513); 
BgL_arg2022z00_3519 = 
(int32_t)(BgL_n2z00_3514); 
BgL_arg2020z00_3517 = 
(BgL_arg2021z00_3518%BgL_arg2022z00_3519); } 
{ /* Llib/weakhash.scm 557 */
 long BgL_arg2135z00_3524;
BgL_arg2135z00_3524 = 
(long)(BgL_arg2020z00_3517); 
BgL_bucketzd2numzd2_1777 = 
(long)(BgL_arg2135z00_3524); } }  else 
{ /* Llib/weakhash.scm 557 */
BgL_bucketzd2numzd2_1777 = 
(BgL_n1z00_3513%BgL_n2z00_3514); } } } } 
{ /* Llib/weakhash.scm 557 */
 obj_t BgL_bucketz00_1778;
{ /* Llib/weakhash.scm 558 */
 obj_t BgL_vectorz00_3526;
if(
VECTORP(BgL_bucketsz00_1775))
{ /* Llib/weakhash.scm 558 */
BgL_vectorz00_3526 = BgL_bucketsz00_1775; }  else 
{ 
 obj_t BgL_auxz00_9759;
BgL_auxz00_9759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22301L), BGl_string3553z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1775); 
FAILURE(BgL_auxz00_9759,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 558 */
 bool_t BgL_test4311z00_9763;
{ /* Llib/weakhash.scm 558 */
 long BgL_tmpz00_9764;
BgL_tmpz00_9764 = 
VECTOR_LENGTH(BgL_vectorz00_3526); 
BgL_test4311z00_9763 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1777, BgL_tmpz00_9764); } 
if(BgL_test4311z00_9763)
{ /* Llib/weakhash.scm 558 */
BgL_bucketz00_1778 = 
VECTOR_REF(BgL_vectorz00_3526,BgL_bucketzd2numzd2_1777); }  else 
{ 
 obj_t BgL_auxz00_9768;
BgL_auxz00_9768 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22289L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3526, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3526)), 
(int)(BgL_bucketzd2numzd2_1777)); 
FAILURE(BgL_auxz00_9768,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 558 */
 obj_t BgL_maxzd2bucketzd2lenz00_1779;
{ /* Llib/weakhash.scm 559 */
 bool_t BgL_test4312z00_9775;
{ /* Llib/weakhash.scm 559 */
 obj_t BgL_tmpz00_9776;
{ /* Llib/weakhash.scm 559 */
 obj_t BgL_res2278z00_3531;
{ /* Llib/weakhash.scm 559 */
 obj_t BgL_aux3126z00_5376;
BgL_aux3126z00_5376 = 
STRUCT_KEY(BgL_tablez00_96); 
if(
SYMBOLP(BgL_aux3126z00_5376))
{ /* Llib/weakhash.scm 559 */
BgL_res2278z00_3531 = BgL_aux3126z00_5376; }  else 
{ 
 obj_t BgL_auxz00_9780;
BgL_auxz00_9780 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22341L), BGl_string3553z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3126z00_5376); 
FAILURE(BgL_auxz00_9780,BFALSE,BFALSE);} } 
BgL_tmpz00_9776 = BgL_res2278z00_3531; } 
BgL_test4312z00_9775 = 
(BgL_tmpz00_9776==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4312z00_9775)
{ /* Llib/weakhash.scm 559 */
 int BgL_tmpz00_9785;
BgL_tmpz00_9785 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1779 = 
STRUCT_REF(BgL_tablez00_96, BgL_tmpz00_9785); }  else 
{ /* Llib/weakhash.scm 559 */
BgL_maxzd2bucketzd2lenz00_1779 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_96); } } 
{ /* Llib/weakhash.scm 559 */
 obj_t BgL_countz00_4539;
BgL_countz00_4539 = 
MAKE_CELL(
BINT(0L)); 
{ /* Llib/weakhash.scm 560 */
 obj_t BgL_foundz00_1781;
{ /* Llib/weakhash.scm 565 */
 obj_t BgL_zc3z04anonymousza31651ze3z87_4529;
{ 
 int BgL_tmpz00_9790;
BgL_tmpz00_9790 = 
(int)(4L); 
BgL_zc3z04anonymousza31651ze3z87_4529 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31651ze3ze5zz__weakhashz00, BgL_tmpz00_9790); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31651ze3z87_4529, 
(int)(0L), 
((obj_t)BgL_countz00_4539)); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31651ze3z87_4529, 
(int)(1L), BgL_procz00_98); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31651ze3z87_4529, 
(int)(2L), BgL_tablez00_96); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31651ze3z87_4529, 
(int)(3L), BgL_keyz00_97); 
BgL_foundz00_1781 = 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_96, BgL_bucketsz00_1775, BgL_bucketzd2numzd2_1777, BgL_zc3z04anonymousza31651ze3z87_4529); } 
{ /* Llib/weakhash.scm 562 */

if(
(BgL_foundz00_1781==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 576 */
{ /* Llib/weakhash.scm 579 */
 long BgL_arg1638z00_1782;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_arg1639z00_1783;
{ /* Llib/weakhash.scm 579 */
 bool_t BgL_test4315z00_9805;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_tmpz00_9806;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_res2280z00_3557;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_aux3128z00_5378;
BgL_aux3128z00_5378 = 
STRUCT_KEY(BgL_tablez00_96); 
if(
SYMBOLP(BgL_aux3128z00_5378))
{ /* Llib/weakhash.scm 579 */
BgL_res2280z00_3557 = BgL_aux3128z00_5378; }  else 
{ 
 obj_t BgL_auxz00_9810;
BgL_auxz00_9810 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22959L), BGl_string3553z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3128z00_5378); 
FAILURE(BgL_auxz00_9810,BFALSE,BFALSE);} } 
BgL_tmpz00_9806 = BgL_res2280z00_3557; } 
BgL_test4315z00_9805 = 
(BgL_tmpz00_9806==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4315z00_9805)
{ /* Llib/weakhash.scm 579 */
 int BgL_tmpz00_9815;
BgL_tmpz00_9815 = 
(int)(0L); 
BgL_arg1639z00_1783 = 
STRUCT_REF(BgL_tablez00_96, BgL_tmpz00_9815); }  else 
{ /* Llib/weakhash.scm 579 */
BgL_arg1639z00_1783 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_96); } } 
{ /* Llib/weakhash.scm 579 */
 long BgL_za71za7_3558;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_tmpz00_9819;
if(
INTEGERP(BgL_arg1639z00_1783))
{ /* Llib/weakhash.scm 579 */
BgL_tmpz00_9819 = BgL_arg1639z00_1783
; }  else 
{ 
 obj_t BgL_auxz00_9822;
BgL_auxz00_9822 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22981L), BGl_string3553z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1639z00_1783); 
FAILURE(BgL_auxz00_9822,BFALSE,BFALSE);} 
BgL_za71za7_3558 = 
(long)CINT(BgL_tmpz00_9819); } 
BgL_arg1638z00_1782 = 
(BgL_za71za7_3558+1L); } } 
{ /* Llib/weakhash.scm 579 */
 bool_t BgL_test4318z00_9828;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_tmpz00_9829;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_res2281z00_3562;
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_aux3131z00_5381;
BgL_aux3131z00_5381 = 
STRUCT_KEY(BgL_tablez00_96); 
if(
SYMBOLP(BgL_aux3131z00_5381))
{ /* Llib/weakhash.scm 579 */
BgL_res2281z00_3562 = BgL_aux3131z00_5381; }  else 
{ 
 obj_t BgL_auxz00_9833;
BgL_auxz00_9833 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22926L), BGl_string3553z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3131z00_5381); 
FAILURE(BgL_auxz00_9833,BFALSE,BFALSE);} } 
BgL_tmpz00_9829 = BgL_res2281z00_3562; } 
BgL_test4318z00_9828 = 
(BgL_tmpz00_9829==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4318z00_9828)
{ /* Llib/weakhash.scm 579 */
 obj_t BgL_auxz00_9840; int BgL_tmpz00_9838;
BgL_auxz00_9840 = 
BINT(BgL_arg1638z00_1782); 
BgL_tmpz00_9838 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_96, BgL_tmpz00_9838, BgL_auxz00_9840); }  else 
{ /* Llib/weakhash.scm 579 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_96); } } } 
{ /* Llib/weakhash.scm 581 */
 obj_t BgL_arg1640z00_1784;
{ /* Llib/weakhash.scm 581 */
 obj_t BgL_arg1641z00_1785; obj_t BgL_arg1642z00_1786;
{ /* Llib/weakhash.scm 581 */
 obj_t BgL_arg1643z00_1787; obj_t BgL_arg1644z00_1788;
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_96))
{ /* Llib/weakhash.scm 582 */

BgL_arg1643z00_1787 = 
bgl_make_weakptr(BgL_keyz00_97, BFALSE); }  else 
{ /* Llib/weakhash.scm 581 */
BgL_arg1643z00_1787 = BgL_keyz00_97; } 
if(
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_tablez00_96))
{ /* Llib/weakhash.scm 585 */

BgL_arg1644z00_1788 = 
bgl_make_weakptr(BgL_objz00_99, BFALSE); }  else 
{ /* Llib/weakhash.scm 584 */
BgL_arg1644z00_1788 = BgL_objz00_99; } 
BgL_arg1641z00_1785 = 
MAKE_YOUNG_PAIR(BgL_arg1643z00_1787, BgL_arg1644z00_1788); } 
{ /* Llib/weakhash.scm 589 */
 obj_t BgL_arg1648z00_1795;
{ /* Llib/weakhash.scm 589 */
 bool_t BgL_test4322z00_9851;
{ /* Llib/weakhash.scm 589 */
 obj_t BgL_tmpz00_9852;
{ /* Llib/weakhash.scm 589 */
 obj_t BgL_res2282z00_3566;
{ /* Llib/weakhash.scm 589 */
 obj_t BgL_aux3133z00_5383;
BgL_aux3133z00_5383 = 
STRUCT_KEY(BgL_tablez00_96); 
if(
SYMBOLP(BgL_aux3133z00_5383))
{ /* Llib/weakhash.scm 589 */
BgL_res2282z00_3566 = BgL_aux3133z00_5383; }  else 
{ 
 obj_t BgL_auxz00_9856;
BgL_auxz00_9856 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23280L), BGl_string3553z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3133z00_5383); 
FAILURE(BgL_auxz00_9856,BFALSE,BFALSE);} } 
BgL_tmpz00_9852 = BgL_res2282z00_3566; } 
BgL_test4322z00_9851 = 
(BgL_tmpz00_9852==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4322z00_9851)
{ /* Llib/weakhash.scm 589 */
 int BgL_tmpz00_9861;
BgL_tmpz00_9861 = 
(int)(2L); 
BgL_arg1648z00_1795 = 
STRUCT_REF(BgL_tablez00_96, BgL_tmpz00_9861); }  else 
{ /* Llib/weakhash.scm 589 */
BgL_arg1648z00_1795 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_96); } } 
{ /* Llib/weakhash.scm 589 */
 obj_t BgL_vectorz00_3567;
if(
VECTORP(BgL_arg1648z00_1795))
{ /* Llib/weakhash.scm 589 */
BgL_vectorz00_3567 = BgL_arg1648z00_1795; }  else 
{ 
 obj_t BgL_auxz00_9867;
BgL_auxz00_9867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23305L), BGl_string3553z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_arg1648z00_1795); 
FAILURE(BgL_auxz00_9867,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 589 */
 bool_t BgL_test4325z00_9871;
{ /* Llib/weakhash.scm 589 */
 long BgL_tmpz00_9872;
BgL_tmpz00_9872 = 
VECTOR_LENGTH(BgL_vectorz00_3567); 
BgL_test4325z00_9871 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1777, BgL_tmpz00_9872); } 
if(BgL_test4325z00_9871)
{ /* Llib/weakhash.scm 589 */
BgL_arg1642z00_1786 = 
VECTOR_REF(BgL_vectorz00_3567,BgL_bucketzd2numzd2_1777); }  else 
{ 
 obj_t BgL_auxz00_9876;
BgL_auxz00_9876 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23268L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3567, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3567)), 
(int)(BgL_bucketzd2numzd2_1777)); 
FAILURE(BgL_auxz00_9876,BFALSE,BFALSE);} } } } 
BgL_arg1640z00_1784 = 
MAKE_YOUNG_PAIR(BgL_arg1641z00_1785, BgL_arg1642z00_1786); } 
{ /* Llib/weakhash.scm 580 */
 obj_t BgL_vectorz00_3569;
if(
VECTORP(BgL_bucketsz00_1775))
{ /* Llib/weakhash.scm 580 */
BgL_vectorz00_3569 = BgL_bucketsz00_1775; }  else 
{ 
 obj_t BgL_auxz00_9886;
BgL_auxz00_9886 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23006L), BGl_string3553z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1775); 
FAILURE(BgL_auxz00_9886,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 580 */
 bool_t BgL_test4327z00_9890;
{ /* Llib/weakhash.scm 580 */
 long BgL_tmpz00_9891;
BgL_tmpz00_9891 = 
VECTOR_LENGTH(BgL_vectorz00_3569); 
BgL_test4327z00_9890 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1777, BgL_tmpz00_9891); } 
if(BgL_test4327z00_9890)
{ /* Llib/weakhash.scm 580 */
VECTOR_SET(BgL_vectorz00_3569,BgL_bucketzd2numzd2_1777,BgL_arg1640z00_1784); }  else 
{ 
 obj_t BgL_auxz00_9895;
BgL_auxz00_9895 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22993L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_3569, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3569)), 
(int)(BgL_bucketzd2numzd2_1777)); 
FAILURE(BgL_auxz00_9895,BFALSE,BFALSE);} } } } 
{ /* Llib/weakhash.scm 591 */
 bool_t BgL_test4328z00_9902;
{ /* Llib/weakhash.scm 591 */
 long BgL_n1z00_3571; long BgL_n2z00_3572;
{ /* Llib/weakhash.scm 591 */
 obj_t BgL_tmpz00_9903;
{ /* Llib/weakhash.scm 591 */
 obj_t BgL_aux3139z00_5389;
BgL_aux3139z00_5389 = 
CELL_REF(BgL_countz00_4539); 
if(
INTEGERP(BgL_aux3139z00_5389))
{ /* Llib/weakhash.scm 591 */
BgL_tmpz00_9903 = BgL_aux3139z00_5389
; }  else 
{ 
 obj_t BgL_auxz00_9906;
BgL_auxz00_9906 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23344L), BGl_string3553z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3139z00_5389); 
FAILURE(BgL_auxz00_9906,BFALSE,BFALSE);} } 
BgL_n1z00_3571 = 
(long)CINT(BgL_tmpz00_9903); } 
{ /* Llib/weakhash.scm 591 */
 obj_t BgL_tmpz00_9911;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1779))
{ /* Llib/weakhash.scm 591 */
BgL_tmpz00_9911 = BgL_maxzd2bucketzd2lenz00_1779
; }  else 
{ 
 obj_t BgL_auxz00_9914;
BgL_auxz00_9914 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23350L), BGl_string3553z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1779); 
FAILURE(BgL_auxz00_9914,BFALSE,BFALSE);} 
BgL_n2z00_3572 = 
(long)CINT(BgL_tmpz00_9911); } 
BgL_test4328z00_9902 = 
(BgL_n1z00_3571>BgL_n2z00_3572); } 
if(BgL_test4328z00_9902)
{ /* Llib/weakhash.scm 591 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_96))
{ /* Llib/weakhash.scm 877 */
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_96); }  else 
{ /* Llib/weakhash.scm 877 */
BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_96); } }  else 
{ /* Llib/weakhash.scm 591 */BFALSE; } } 
return BgL_objz00_99;}  else 
{ /* Llib/weakhash.scm 576 */
return BgL_foundz00_1781;} } } } } } } } } } 

}



/* &<@anonymous:1651> */
obj_t BGl_z62zc3z04anonymousza31651ze3ze5zz__weakhashz00(obj_t BgL_envz00_4530, obj_t BgL_bkeyz00_4535, obj_t BgL_valz00_4536, obj_t BgL_bucketz00_4537)
{
{ /* Llib/weakhash.scm 564 */
{ /* Llib/weakhash.scm 565 */
 obj_t BgL_countz00_4531; obj_t BgL_procz00_4532; obj_t BgL_tablez00_4533; obj_t BgL_keyz00_4534;
BgL_countz00_4531 = 
PROCEDURE_L_REF(BgL_envz00_4530, 
(int)(0L)); 
BgL_procz00_4532 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4530, 
(int)(1L))); 
BgL_tablez00_4533 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4530, 
(int)(2L))); 
BgL_keyz00_4534 = 
PROCEDURE_L_REF(BgL_envz00_4530, 
(int)(3L)); 
{ /* Llib/weakhash.scm 565 */
 obj_t BgL_auxz00_5764;
{ /* Llib/weakhash.scm 565 */
 obj_t BgL_tmpz00_9934;
{ /* Llib/weakhash.scm 565 */
 obj_t BgL_aux3141z00_5765;
BgL_aux3141z00_5765 = 
CELL_REF(BgL_countz00_4531); 
if(
INTEGERP(BgL_aux3141z00_5765))
{ /* Llib/weakhash.scm 565 */
BgL_tmpz00_9934 = BgL_aux3141z00_5765
; }  else 
{ 
 obj_t BgL_auxz00_9937;
BgL_auxz00_9937 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22554L), BGl_string3554z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3141z00_5765); 
FAILURE(BgL_auxz00_9937,BFALSE,BFALSE);} } 
BgL_auxz00_5764 = 
ADDFX(BgL_tmpz00_9934, 
BINT(1L)); } 
CELL_SET(BgL_countz00_4531, BgL_auxz00_5764); } 
{ /* Llib/weakhash.scm 566 */
 bool_t BgL_test4333z00_9943;
{ /* Llib/weakhash.scm 566 */
 obj_t BgL_eqtz00_5766;
{ /* Llib/weakhash.scm 566 */
 bool_t BgL_test4334z00_9944;
{ /* Llib/weakhash.scm 566 */
 obj_t BgL_tmpz00_9945;
{ /* Llib/weakhash.scm 566 */
 obj_t BgL_res2279z00_5767;
{ /* Llib/weakhash.scm 566 */
 obj_t BgL_aux3142z00_5768;
BgL_aux3142z00_5768 = 
STRUCT_KEY(BgL_tablez00_4533); 
if(
SYMBOLP(BgL_aux3142z00_5768))
{ /* Llib/weakhash.scm 566 */
BgL_res2279z00_5767 = BgL_aux3142z00_5768; }  else 
{ 
 obj_t BgL_auxz00_9949;
BgL_auxz00_9949 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22577L), BGl_string3554z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3142z00_5768); 
FAILURE(BgL_auxz00_9949,BFALSE,BFALSE);} } 
BgL_tmpz00_9945 = BgL_res2279z00_5767; } 
BgL_test4334z00_9944 = 
(BgL_tmpz00_9945==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4334z00_9944)
{ /* Llib/weakhash.scm 566 */
 int BgL_tmpz00_9954;
BgL_tmpz00_9954 = 
(int)(3L); 
BgL_eqtz00_5766 = 
STRUCT_REF(BgL_tablez00_4533, BgL_tmpz00_9954); }  else 
{ /* Llib/weakhash.scm 566 */
BgL_eqtz00_5766 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_4533); } } 
if(
PROCEDUREP(BgL_eqtz00_5766))
{ /* Llib/weakhash.scm 566 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_5766, 2))
{ /* Llib/weakhash.scm 566 */
BgL_test4333z00_9943 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_5766, BgL_bkeyz00_4535, BgL_keyz00_4534))
; }  else 
{ /* Llib/weakhash.scm 566 */
FAILURE(BGl_string3555z00zz__weakhashz00,BGl_list3542z00zz__weakhashz00,BgL_eqtz00_5766);} }  else 
{ /* Llib/weakhash.scm 566 */
if(
(BgL_bkeyz00_4535==BgL_keyz00_4534))
{ /* Llib/weakhash.scm 566 */
BgL_test4333z00_9943 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 566 */
if(
STRINGP(BgL_bkeyz00_4535))
{ /* Llib/weakhash.scm 566 */
if(
STRINGP(BgL_keyz00_4534))
{ /* Llib/weakhash.scm 566 */
 long BgL_l1z00_5769;
BgL_l1z00_5769 = 
STRING_LENGTH(BgL_bkeyz00_4535); 
if(
(BgL_l1z00_5769==
STRING_LENGTH(BgL_keyz00_4534)))
{ /* Llib/weakhash.scm 566 */
 int BgL_arg1918z00_5770;
{ /* Llib/weakhash.scm 566 */
 char * BgL_auxz00_9981; char * BgL_tmpz00_9979;
BgL_auxz00_9981 = 
BSTRING_TO_STRING(BgL_keyz00_4534); 
BgL_tmpz00_9979 = 
BSTRING_TO_STRING(BgL_bkeyz00_4535); 
BgL_arg1918z00_5770 = 
memcmp(BgL_tmpz00_9979, BgL_auxz00_9981, BgL_l1z00_5769); } 
BgL_test4333z00_9943 = 
(
(long)(BgL_arg1918z00_5770)==0L); }  else 
{ /* Llib/weakhash.scm 566 */
BgL_test4333z00_9943 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 566 */
BgL_test4333z00_9943 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 566 */
BgL_test4333z00_9943 = ((bool_t)0)
; } } } } 
if(BgL_test4333z00_9943)
{ /* Llib/weakhash.scm 567 */
 obj_t BgL_newvalz00_5771;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_4532, 1))
{ /* Llib/weakhash.scm 567 */
BgL_newvalz00_5771 = 
BGL_PROCEDURE_CALL1(BgL_procz00_4532, BgL_valz00_4536); }  else 
{ /* Llib/weakhash.scm 567 */
FAILURE(BGl_string3555z00zz__weakhashz00,BGl_list3556z00zz__weakhashz00,BgL_procz00_4532);} 
{ /* Llib/weakhash.scm 568 */
 obj_t BgL_arg1653z00_5772; obj_t BgL_arg1654z00_5773;
{ /* Llib/weakhash.scm 568 */
 obj_t BgL_pairz00_5774;
if(
PAIRP(BgL_bucketz00_4537))
{ /* Llib/weakhash.scm 568 */
BgL_pairz00_5774 = BgL_bucketz00_4537; }  else 
{ 
 obj_t BgL_auxz00_9995;
BgL_auxz00_9995 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22668L), BGl_string3554z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_4537); 
FAILURE(BgL_auxz00_9995,BFALSE,BFALSE);} 
BgL_arg1653z00_5772 = 
CAR(BgL_pairz00_5774); } 
if(
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_tablez00_4533))
{ /* Llib/weakhash.scm 570 */

BgL_arg1654z00_5773 = 
bgl_make_weakptr(BgL_newvalz00_5771, BFALSE); }  else 
{ /* Llib/weakhash.scm 569 */
BgL_arg1654z00_5773 = BgL_newvalz00_5771; } 
{ /* Llib/weakhash.scm 568 */
 obj_t BgL_pairz00_5775;
if(
PAIRP(BgL_arg1653z00_5772))
{ /* Llib/weakhash.scm 568 */
BgL_pairz00_5775 = BgL_arg1653z00_5772; }  else 
{ 
 obj_t BgL_auxz00_10005;
BgL_auxz00_10005 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(22674L), BGl_string3554z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_arg1653z00_5772); 
FAILURE(BgL_auxz00_10005,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_5775, BgL_arg1654z00_5773); } } 
return BgL_newvalz00_5771;}  else 
{ /* Llib/weakhash.scm 566 */
return BGl_keepgoingz00zz__weakhashz00;} } } } 

}



/* weak-hashtable-update! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(obj_t BgL_tablez00_100, obj_t BgL_keyz00_101, obj_t BgL_procz00_102, obj_t BgL_objz00_103)
{
{ /* Llib/weakhash.scm 598 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_100))
{ /* Llib/weakhash.scm 599 */
BGL_TAIL return 
BGl_weakzd2keyszd2hashtablezd2updatez12zc0zz__weakhashz00(BgL_tablez00_100, BgL_keyz00_101, BgL_procz00_102, BgL_objz00_103);}  else 
{ /* Llib/weakhash.scm 599 */
BGL_TAIL return 
BGl_weakzd2oldzd2hashtablezd2updatez12zc0zz__weakhashz00(BgL_tablez00_100, BgL_keyz00_101, BgL_procz00_102, BgL_objz00_103);} } 

}



/* &weak-hashtable-update! */
obj_t BGl_z62weakzd2hashtablezd2updatez12z70zz__weakhashz00(obj_t BgL_envz00_4541, obj_t BgL_tablez00_4542, obj_t BgL_keyz00_4543, obj_t BgL_procz00_4544, obj_t BgL_objz00_4545)
{
{ /* Llib/weakhash.scm 598 */
{ /* Llib/weakhash.scm 599 */
 obj_t BgL_auxz00_10021; obj_t BgL_auxz00_10014;
if(
PROCEDUREP(BgL_procz00_4544))
{ /* Llib/weakhash.scm 599 */
BgL_auxz00_10021 = BgL_procz00_4544
; }  else 
{ 
 obj_t BgL_auxz00_10024;
BgL_auxz00_10024 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23717L), BGl_string3557z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_procz00_4544); 
FAILURE(BgL_auxz00_10024,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_4542))
{ /* Llib/weakhash.scm 599 */
BgL_auxz00_10014 = BgL_tablez00_4542
; }  else 
{ 
 obj_t BgL_auxz00_10017;
BgL_auxz00_10017 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(23717L), BGl_string3557z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4542); 
FAILURE(BgL_auxz00_10017,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2updatez12z12zz__weakhashz00(BgL_auxz00_10014, BgL_keyz00_4543, BgL_auxz00_10021, BgL_objz00_4545);} } 

}



/* weak-keys-hashtable-add! */
obj_t BGl_weakzd2keyszd2hashtablezd2addz12zc0zz__weakhashz00(obj_t BgL_tablez00_104, obj_t BgL_keyz00_105, obj_t BgL_procz00_106, obj_t BgL_objz00_107, obj_t BgL_initz00_108)
{
{ /* Llib/weakhash.scm 606 */
{ /* Llib/weakhash.scm 607 */
 obj_t BgL_bucketsz00_1812;
{ /* Llib/weakhash.scm 607 */
 bool_t BgL_test4349z00_10029;
{ /* Llib/weakhash.scm 607 */
 obj_t BgL_tmpz00_10030;
{ /* Llib/weakhash.scm 607 */
 obj_t BgL_res2283z00_3579;
{ /* Llib/weakhash.scm 607 */
 obj_t BgL_aux3154z00_5406;
BgL_aux3154z00_5406 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3154z00_5406))
{ /* Llib/weakhash.scm 607 */
BgL_res2283z00_3579 = BgL_aux3154z00_5406; }  else 
{ 
 obj_t BgL_auxz00_10034;
BgL_auxz00_10034 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24188L), BGl_string3558z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3154z00_5406); 
FAILURE(BgL_auxz00_10034,BFALSE,BFALSE);} } 
BgL_tmpz00_10030 = BgL_res2283z00_3579; } 
BgL_test4349z00_10029 = 
(BgL_tmpz00_10030==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4349z00_10029)
{ /* Llib/weakhash.scm 607 */
 int BgL_tmpz00_10039;
BgL_tmpz00_10039 = 
(int)(2L); 
BgL_bucketsz00_1812 = 
STRUCT_REF(BgL_tablez00_104, BgL_tmpz00_10039); }  else 
{ /* Llib/weakhash.scm 607 */
BgL_bucketsz00_1812 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } 
{ /* Llib/weakhash.scm 607 */
 long BgL_bucketzd2lenzd2_1813;
{ /* Llib/weakhash.scm 608 */
 obj_t BgL_vectorz00_3580;
if(
VECTORP(BgL_bucketsz00_1812))
{ /* Llib/weakhash.scm 608 */
BgL_vectorz00_3580 = BgL_bucketsz00_1812; }  else 
{ 
 obj_t BgL_auxz00_10045;
BgL_auxz00_10045 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24246L), BGl_string3558z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1812); 
FAILURE(BgL_auxz00_10045,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1813 = 
VECTOR_LENGTH(BgL_vectorz00_3580); } 
{ /* Llib/weakhash.scm 608 */
 long BgL_bucketzd2numzd2_1814;
{ /* Llib/weakhash.scm 609 */
 long BgL_arg1702z00_1847;
BgL_arg1702z00_1847 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_104, BgL_keyz00_105); 
{ /* Llib/weakhash.scm 609 */
 long BgL_n1z00_3581; long BgL_n2z00_3582;
BgL_n1z00_3581 = BgL_arg1702z00_1847; 
BgL_n2z00_3582 = BgL_bucketzd2lenzd2_1813; 
{ /* Llib/weakhash.scm 609 */
 bool_t BgL_test4352z00_10051;
{ /* Llib/weakhash.scm 609 */
 long BgL_arg2024z00_3584;
BgL_arg2024z00_3584 = 
(((BgL_n1z00_3581) | (BgL_n2z00_3582)) & -2147483648); 
BgL_test4352z00_10051 = 
(BgL_arg2024z00_3584==0L); } 
if(BgL_test4352z00_10051)
{ /* Llib/weakhash.scm 609 */
 int32_t BgL_arg2020z00_3585;
{ /* Llib/weakhash.scm 609 */
 int32_t BgL_arg2021z00_3586; int32_t BgL_arg2022z00_3587;
BgL_arg2021z00_3586 = 
(int32_t)(BgL_n1z00_3581); 
BgL_arg2022z00_3587 = 
(int32_t)(BgL_n2z00_3582); 
BgL_arg2020z00_3585 = 
(BgL_arg2021z00_3586%BgL_arg2022z00_3587); } 
{ /* Llib/weakhash.scm 609 */
 long BgL_arg2135z00_3592;
BgL_arg2135z00_3592 = 
(long)(BgL_arg2020z00_3585); 
BgL_bucketzd2numzd2_1814 = 
(long)(BgL_arg2135z00_3592); } }  else 
{ /* Llib/weakhash.scm 609 */
BgL_bucketzd2numzd2_1814 = 
(BgL_n1z00_3581%BgL_n2z00_3582); } } } } 
{ /* Llib/weakhash.scm 609 */
 obj_t BgL_bucketz00_1815;
{ /* Llib/weakhash.scm 610 */
 obj_t BgL_vectorz00_3594;
if(
VECTORP(BgL_bucketsz00_1812))
{ /* Llib/weakhash.scm 610 */
BgL_vectorz00_3594 = BgL_bucketsz00_1812; }  else 
{ 
 obj_t BgL_auxz00_10062;
BgL_auxz00_10062 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24356L), BGl_string3558z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1812); 
FAILURE(BgL_auxz00_10062,BFALSE,BFALSE);} 
BgL_bucketz00_1815 = 
VECTOR_REF(BgL_vectorz00_3594,BgL_bucketzd2numzd2_1814); } 
{ /* Llib/weakhash.scm 610 */
 obj_t BgL_maxzd2bucketzd2lenz00_1816;
{ /* Llib/weakhash.scm 611 */
 bool_t BgL_test4354z00_10067;
{ /* Llib/weakhash.scm 611 */
 obj_t BgL_tmpz00_10068;
{ /* Llib/weakhash.scm 611 */
 obj_t BgL_res2284z00_3599;
{ /* Llib/weakhash.scm 611 */
 obj_t BgL_aux3160z00_5412;
BgL_aux3160z00_5412 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3160z00_5412))
{ /* Llib/weakhash.scm 611 */
BgL_res2284z00_3599 = BgL_aux3160z00_5412; }  else 
{ 
 obj_t BgL_auxz00_10072;
BgL_auxz00_10072 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24396L), BGl_string3558z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3160z00_5412); 
FAILURE(BgL_auxz00_10072,BFALSE,BFALSE);} } 
BgL_tmpz00_10068 = BgL_res2284z00_3599; } 
BgL_test4354z00_10067 = 
(BgL_tmpz00_10068==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4354z00_10067)
{ /* Llib/weakhash.scm 611 */
 int BgL_tmpz00_10077;
BgL_tmpz00_10077 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1816 = 
STRUCT_REF(BgL_tablez00_104, BgL_tmpz00_10077); }  else 
{ /* Llib/weakhash.scm 611 */
BgL_maxzd2bucketzd2lenz00_1816 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } 
{ /* Llib/weakhash.scm 611 */

if(
NULLP(BgL_bucketz00_1815))
{ /* Llib/weakhash.scm 613 */
 obj_t BgL_vz00_1818;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_106, 2))
{ /* Llib/weakhash.scm 613 */
BgL_vz00_1818 = 
BGL_PROCEDURE_CALL2(BgL_procz00_106, BgL_objz00_107, BgL_initz00_108); }  else 
{ /* Llib/weakhash.scm 613 */
FAILURE(BGl_string3559z00zz__weakhashz00,BGl_list3560z00zz__weakhashz00,BgL_procz00_106);} 
{ /* Llib/weakhash.scm 614 */
 long BgL_arg1661z00_1819;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_arg1663z00_1820;
{ /* Llib/weakhash.scm 614 */
 bool_t BgL_test4358z00_10091;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_tmpz00_10092;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_res2285z00_3603;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_aux3163z00_5416;
BgL_aux3163z00_5416 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3163z00_5416))
{ /* Llib/weakhash.scm 614 */
BgL_res2285z00_3603 = BgL_aux3163z00_5416; }  else 
{ 
 obj_t BgL_auxz00_10096;
BgL_auxz00_10096 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24526L), BGl_string3558z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3163z00_5416); 
FAILURE(BgL_auxz00_10096,BFALSE,BFALSE);} } 
BgL_tmpz00_10092 = BgL_res2285z00_3603; } 
BgL_test4358z00_10091 = 
(BgL_tmpz00_10092==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4358z00_10091)
{ /* Llib/weakhash.scm 614 */
 int BgL_tmpz00_10101;
BgL_tmpz00_10101 = 
(int)(0L); 
BgL_arg1663z00_1820 = 
STRUCT_REF(BgL_tablez00_104, BgL_tmpz00_10101); }  else 
{ /* Llib/weakhash.scm 614 */
BgL_arg1663z00_1820 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } 
{ /* Llib/weakhash.scm 614 */
 long BgL_za71za7_3604;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_tmpz00_10105;
if(
INTEGERP(BgL_arg1663z00_1820))
{ /* Llib/weakhash.scm 614 */
BgL_tmpz00_10105 = BgL_arg1663z00_1820
; }  else 
{ 
 obj_t BgL_auxz00_10108;
BgL_auxz00_10108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24548L), BGl_string3558z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1663z00_1820); 
FAILURE(BgL_auxz00_10108,BFALSE,BFALSE);} 
BgL_za71za7_3604 = 
(long)CINT(BgL_tmpz00_10105); } 
BgL_arg1661z00_1819 = 
(BgL_za71za7_3604+1L); } } 
{ /* Llib/weakhash.scm 614 */
 bool_t BgL_test4361z00_10114;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_tmpz00_10115;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_res2286z00_3608;
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_aux3166z00_5419;
BgL_aux3166z00_5419 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3166z00_5419))
{ /* Llib/weakhash.scm 614 */
BgL_res2286z00_3608 = BgL_aux3166z00_5419; }  else 
{ 
 obj_t BgL_auxz00_10119;
BgL_auxz00_10119 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24493L), BGl_string3558z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3166z00_5419); 
FAILURE(BgL_auxz00_10119,BFALSE,BFALSE);} } 
BgL_tmpz00_10115 = BgL_res2286z00_3608; } 
BgL_test4361z00_10114 = 
(BgL_tmpz00_10115==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4361z00_10114)
{ /* Llib/weakhash.scm 614 */
 obj_t BgL_auxz00_10126; int BgL_tmpz00_10124;
BgL_auxz00_10126 = 
BINT(BgL_arg1661z00_1819); 
BgL_tmpz00_10124 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_104, BgL_tmpz00_10124, BgL_auxz00_10126); }  else 
{ /* Llib/weakhash.scm 614 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } } 
{ /* Llib/weakhash.scm 615 */
 obj_t BgL_arg1664z00_1821;
{ /* Llib/weakhash.scm 615 */
 obj_t BgL_arg1667z00_1822;
BgL_arg1667z00_1822 = 
MAKE_YOUNG_PAIR(BgL_keyz00_105, BgL_vz00_1818); 
{ /* Llib/weakhash.scm 615 */
 obj_t BgL_list1668z00_1823;
BgL_list1668z00_1823 = 
MAKE_YOUNG_PAIR(BgL_arg1667z00_1822, BNIL); 
BgL_arg1664z00_1821 = BgL_list1668z00_1823; } } 
{ /* Llib/weakhash.scm 615 */
 obj_t BgL_vectorz00_3610;
if(
VECTORP(BgL_bucketsz00_1812))
{ /* Llib/weakhash.scm 615 */
BgL_vectorz00_3610 = BgL_bucketsz00_1812; }  else 
{ 
 obj_t BgL_auxz00_10134;
BgL_auxz00_10134 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24576L), BGl_string3558z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1812); 
FAILURE(BgL_auxz00_10134,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3610,BgL_bucketzd2numzd2_1814,BgL_arg1664z00_1821); } } 
return BgL_vz00_1818;}  else 
{ 
 obj_t BgL_buckz00_1825; long BgL_countz00_1826;
BgL_buckz00_1825 = BgL_bucketz00_1815; 
BgL_countz00_1826 = 0L; 
BgL_zc3z04anonymousza31669ze3z87_1827:
if(
NULLP(BgL_buckz00_1825))
{ /* Llib/weakhash.scm 621 */
 obj_t BgL_vz00_1829;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_106, 2))
{ /* Llib/weakhash.scm 621 */
BgL_vz00_1829 = 
BGL_PROCEDURE_CALL2(BgL_procz00_106, BgL_objz00_107, BgL_initz00_108); }  else 
{ /* Llib/weakhash.scm 621 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3560z00zz__weakhashz00,BgL_procz00_106);} 
{ /* Llib/weakhash.scm 622 */
 long BgL_arg1675z00_1830;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_arg1676z00_1831;
{ /* Llib/weakhash.scm 622 */
 bool_t BgL_test4366z00_10149;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_tmpz00_10150;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_res2288z00_3615;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_aux3171z00_5425;
BgL_aux3171z00_5425 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3171z00_5425))
{ /* Llib/weakhash.scm 622 */
BgL_res2288z00_3615 = BgL_aux3171z00_5425; }  else 
{ 
 obj_t BgL_auxz00_10154;
BgL_auxz00_10154 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24768L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3171z00_5425); 
FAILURE(BgL_auxz00_10154,BFALSE,BFALSE);} } 
BgL_tmpz00_10150 = BgL_res2288z00_3615; } 
BgL_test4366z00_10149 = 
(BgL_tmpz00_10150==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4366z00_10149)
{ /* Llib/weakhash.scm 622 */
 int BgL_tmpz00_10159;
BgL_tmpz00_10159 = 
(int)(0L); 
BgL_arg1676z00_1831 = 
STRUCT_REF(BgL_tablez00_104, BgL_tmpz00_10159); }  else 
{ /* Llib/weakhash.scm 622 */
BgL_arg1676z00_1831 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } 
{ /* Llib/weakhash.scm 622 */
 long BgL_za71za7_3616;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_tmpz00_10163;
if(
INTEGERP(BgL_arg1676z00_1831))
{ /* Llib/weakhash.scm 622 */
BgL_tmpz00_10163 = BgL_arg1676z00_1831
; }  else 
{ 
 obj_t BgL_auxz00_10166;
BgL_auxz00_10166 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24790L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1676z00_1831); 
FAILURE(BgL_auxz00_10166,BFALSE,BFALSE);} 
BgL_za71za7_3616 = 
(long)CINT(BgL_tmpz00_10163); } 
BgL_arg1675z00_1830 = 
(BgL_za71za7_3616+1L); } } 
{ /* Llib/weakhash.scm 622 */
 bool_t BgL_test4369z00_10172;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_tmpz00_10173;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_res2289z00_3620;
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_aux3174z00_5428;
BgL_aux3174z00_5428 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3174z00_5428))
{ /* Llib/weakhash.scm 622 */
BgL_res2289z00_3620 = BgL_aux3174z00_5428; }  else 
{ 
 obj_t BgL_auxz00_10177;
BgL_auxz00_10177 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24735L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3174z00_5428); 
FAILURE(BgL_auxz00_10177,BFALSE,BFALSE);} } 
BgL_tmpz00_10173 = BgL_res2289z00_3620; } 
BgL_test4369z00_10172 = 
(BgL_tmpz00_10173==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4369z00_10172)
{ /* Llib/weakhash.scm 622 */
 obj_t BgL_auxz00_10184; int BgL_tmpz00_10182;
BgL_auxz00_10184 = 
BINT(BgL_arg1675z00_1830); 
BgL_tmpz00_10182 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_104, BgL_tmpz00_10182, BgL_auxz00_10184); }  else 
{ /* Llib/weakhash.scm 622 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } } 
{ /* Llib/weakhash.scm 624 */
 obj_t BgL_arg1678z00_1832;
BgL_arg1678z00_1832 = 
MAKE_YOUNG_PAIR(
bgl_make_weakptr(BgL_keyz00_105, BgL_vz00_1829), BgL_bucketz00_1815); 
{ /* Llib/weakhash.scm 623 */
 obj_t BgL_vectorz00_3621;
if(
VECTORP(BgL_bucketsz00_1812))
{ /* Llib/weakhash.scm 623 */
BgL_vectorz00_3621 = BgL_bucketsz00_1812; }  else 
{ 
 obj_t BgL_auxz00_10192;
BgL_auxz00_10192 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24818L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1812); 
FAILURE(BgL_auxz00_10192,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3621,BgL_bucketzd2numzd2_1814,BgL_arg1678z00_1832); } } 
{ /* Llib/weakhash.scm 625 */
 bool_t BgL_test4372z00_10197;
{ /* Llib/weakhash.scm 625 */
 long BgL_n2z00_3624;
{ /* Llib/weakhash.scm 625 */
 obj_t BgL_tmpz00_10198;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1816))
{ /* Llib/weakhash.scm 625 */
BgL_tmpz00_10198 = BgL_maxzd2bucketzd2lenz00_1816
; }  else 
{ 
 obj_t BgL_auxz00_10201;
BgL_auxz00_10201 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24905L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1816); 
FAILURE(BgL_auxz00_10201,BFALSE,BFALSE);} 
BgL_n2z00_3624 = 
(long)CINT(BgL_tmpz00_10198); } 
BgL_test4372z00_10197 = 
(BgL_countz00_1826>BgL_n2z00_3624); } 
if(BgL_test4372z00_10197)
{ /* Llib/weakhash.scm 625 */
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_104); }  else 
{ /* Llib/weakhash.scm 625 */BFALSE; } } 
return BgL_vz00_1829;}  else 
{ /* Llib/weakhash.scm 628 */
 bool_t BgL_test4374z00_10208;
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_arg1700z00_1844;
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_arg1701z00_1845;
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_pairz00_3625;
if(
PAIRP(BgL_buckz00_1825))
{ /* Llib/weakhash.scm 628 */
BgL_pairz00_3625 = BgL_buckz00_1825; }  else 
{ 
 obj_t BgL_auxz00_10211;
BgL_auxz00_10211 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25023L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1825); 
FAILURE(BgL_auxz00_10211,BFALSE,BFALSE);} 
BgL_arg1701z00_1845 = 
CAR(BgL_pairz00_3625); } 
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_objz00_3626;
if(
BGL_WEAKPTRP(BgL_arg1701z00_1845))
{ /* Llib/weakhash.scm 628 */
BgL_objz00_3626 = BgL_arg1701z00_1845; }  else 
{ 
 obj_t BgL_auxz00_10218;
BgL_auxz00_10218 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25027L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1701z00_1845); 
FAILURE(BgL_auxz00_10218,BFALSE,BFALSE);} 
BgL_arg1700z00_1844 = 
bgl_weakptr_data(BgL_objz00_3626); } } 
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_eqtz00_3627;
{ /* Llib/weakhash.scm 628 */
 bool_t BgL_test4377z00_10223;
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_tmpz00_10224;
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_res2290z00_3634;
{ /* Llib/weakhash.scm 628 */
 obj_t BgL_aux3183z00_5437;
BgL_aux3183z00_5437 = 
STRUCT_KEY(BgL_tablez00_104); 
if(
SYMBOLP(BgL_aux3183z00_5437))
{ /* Llib/weakhash.scm 628 */
BgL_res2290z00_3634 = BgL_aux3183z00_5437; }  else 
{ 
 obj_t BgL_auxz00_10228;
BgL_auxz00_10228 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(24980L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3183z00_5437); 
FAILURE(BgL_auxz00_10228,BFALSE,BFALSE);} } 
BgL_tmpz00_10224 = BgL_res2290z00_3634; } 
BgL_test4377z00_10223 = 
(BgL_tmpz00_10224==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4377z00_10223)
{ /* Llib/weakhash.scm 628 */
 int BgL_tmpz00_10233;
BgL_tmpz00_10233 = 
(int)(3L); 
BgL_eqtz00_3627 = 
STRUCT_REF(BgL_tablez00_104, BgL_tmpz00_10233); }  else 
{ /* Llib/weakhash.scm 628 */
BgL_eqtz00_3627 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_104); } } 
if(
PROCEDUREP(BgL_eqtz00_3627))
{ /* Llib/weakhash.scm 628 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3627, 2))
{ /* Llib/weakhash.scm 628 */
BgL_test4374z00_10208 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3627, BgL_arg1700z00_1844, BgL_keyz00_105))
; }  else 
{ /* Llib/weakhash.scm 628 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3565z00zz__weakhashz00,BgL_eqtz00_3627);} }  else 
{ /* Llib/weakhash.scm 628 */
if(
(BgL_arg1700z00_1844==BgL_keyz00_105))
{ /* Llib/weakhash.scm 628 */
BgL_test4374z00_10208 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 628 */
if(
STRINGP(BgL_arg1700z00_1844))
{ /* Llib/weakhash.scm 628 */
if(
STRINGP(BgL_keyz00_105))
{ /* Llib/weakhash.scm 628 */
 long BgL_l1z00_3637;
BgL_l1z00_3637 = 
STRING_LENGTH(BgL_arg1700z00_1844); 
if(
(BgL_l1z00_3637==
STRING_LENGTH(BgL_keyz00_105)))
{ /* Llib/weakhash.scm 628 */
 int BgL_arg1918z00_3640;
{ /* Llib/weakhash.scm 628 */
 char * BgL_auxz00_10260; char * BgL_tmpz00_10258;
BgL_auxz00_10260 = 
BSTRING_TO_STRING(BgL_keyz00_105); 
BgL_tmpz00_10258 = 
BSTRING_TO_STRING(BgL_arg1700z00_1844); 
BgL_arg1918z00_3640 = 
memcmp(BgL_tmpz00_10258, BgL_auxz00_10260, BgL_l1z00_3637); } 
BgL_test4374z00_10208 = 
(
(long)(BgL_arg1918z00_3640)==0L); }  else 
{ /* Llib/weakhash.scm 628 */
BgL_test4374z00_10208 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 628 */
BgL_test4374z00_10208 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 628 */
BgL_test4374z00_10208 = ((bool_t)0)
; } } } } } 
if(BgL_test4374z00_10208)
{ /* Llib/weakhash.scm 629 */
 obj_t BgL_resz00_1838;
{ /* Llib/weakhash.scm 629 */
 obj_t BgL_arg1689z00_1840;
{ /* Llib/weakhash.scm 629 */
 obj_t BgL_arg1691z00_1841;
{ /* Llib/weakhash.scm 629 */
 obj_t BgL_pairz00_3646;
if(
PAIRP(BgL_buckz00_1825))
{ /* Llib/weakhash.scm 629 */
BgL_pairz00_3646 = BgL_buckz00_1825; }  else 
{ 
 obj_t BgL_auxz00_10267;
BgL_auxz00_10267 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25077L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1825); 
FAILURE(BgL_auxz00_10267,BFALSE,BFALSE);} 
BgL_arg1691z00_1841 = 
CAR(BgL_pairz00_3646); } 
{ /* Llib/weakhash.scm 629 */
 obj_t BgL_objz00_3647;
if(
BGL_WEAKPTRP(BgL_arg1691z00_1841))
{ /* Llib/weakhash.scm 629 */
BgL_objz00_3647 = BgL_arg1691z00_1841; }  else 
{ 
 obj_t BgL_auxz00_10274;
BgL_auxz00_10274 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25081L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1691z00_1841); 
FAILURE(BgL_auxz00_10274,BFALSE,BFALSE);} 
BgL_arg1689z00_1840 = 
bgl_weakptr_ref(BgL_objz00_3647); } } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_106, 2))
{ /* Llib/weakhash.scm 629 */
BgL_resz00_1838 = 
BGL_PROCEDURE_CALL2(BgL_procz00_106, BgL_objz00_107, BgL_arg1689z00_1840); }  else 
{ /* Llib/weakhash.scm 629 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3568z00zz__weakhashz00,BgL_procz00_106);} } 
{ /* Llib/weakhash.scm 630 */
 obj_t BgL_arg1688z00_1839;
{ /* Llib/weakhash.scm 630 */
 obj_t BgL_pairz00_3648;
if(
PAIRP(BgL_buckz00_1825))
{ /* Llib/weakhash.scm 630 */
BgL_pairz00_3648 = BgL_buckz00_1825; }  else 
{ 
 obj_t BgL_auxz00_10289;
BgL_auxz00_10289 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25116L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1825); 
FAILURE(BgL_auxz00_10289,BFALSE,BFALSE);} 
BgL_arg1688z00_1839 = 
CAR(BgL_pairz00_3648); } 
{ /* Llib/weakhash.scm 630 */
 obj_t BgL_ptrz00_3649;
if(
BGL_WEAKPTRP(BgL_arg1688z00_1839))
{ /* Llib/weakhash.scm 630 */
BgL_ptrz00_3649 = BgL_arg1688z00_1839; }  else 
{ 
 obj_t BgL_auxz00_10296;
BgL_auxz00_10296 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25120L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1688z00_1839); 
FAILURE(BgL_auxz00_10296,BFALSE,BFALSE);} 
bgl_weakptr_ref_set(BgL_ptrz00_3649, BgL_resz00_1838); BUNSPEC; BUNSPEC; } } 
return BgL_resz00_1838;}  else 
{ /* Llib/weakhash.scm 633 */
 obj_t BgL_arg1692z00_1842; long BgL_arg1699z00_1843;
{ /* Llib/weakhash.scm 633 */
 obj_t BgL_pairz00_3650;
if(
PAIRP(BgL_buckz00_1825))
{ /* Llib/weakhash.scm 633 */
BgL_pairz00_3650 = BgL_buckz00_1825; }  else 
{ 
 obj_t BgL_auxz00_10303;
BgL_auxz00_10303 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25161L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_buckz00_1825); 
FAILURE(BgL_auxz00_10303,BFALSE,BFALSE);} 
BgL_arg1692z00_1842 = 
CDR(BgL_pairz00_3650); } 
BgL_arg1699z00_1843 = 
(BgL_countz00_1826+1L); 
{ 
 long BgL_countz00_10310; obj_t BgL_buckz00_10309;
BgL_buckz00_10309 = BgL_arg1692z00_1842; 
BgL_countz00_10310 = BgL_arg1699z00_1843; 
BgL_countz00_1826 = BgL_countz00_10310; 
BgL_buckz00_1825 = BgL_buckz00_10309; 
goto BgL_zc3z04anonymousza31669ze3z87_1827;} } } } } } } } } } } 

}



/* weak-old-hashtable-add! */
obj_t BGl_weakzd2oldzd2hashtablezd2addz12zc0zz__weakhashz00(obj_t BgL_tablez00_109, obj_t BgL_keyz00_110, obj_t BgL_procz00_111, obj_t BgL_objz00_112, obj_t BgL_initz00_113)
{
{ /* Llib/weakhash.scm 638 */
{ /* Llib/weakhash.scm 639 */
 obj_t BgL_bucketsz00_1848;
{ /* Llib/weakhash.scm 639 */
 bool_t BgL_test4391z00_10311;
{ /* Llib/weakhash.scm 639 */
 obj_t BgL_tmpz00_10312;
{ /* Llib/weakhash.scm 639 */
 obj_t BgL_res2291z00_3655;
{ /* Llib/weakhash.scm 639 */
 obj_t BgL_aux3197z00_5453;
BgL_aux3197z00_5453 = 
STRUCT_KEY(BgL_tablez00_109); 
if(
SYMBOLP(BgL_aux3197z00_5453))
{ /* Llib/weakhash.scm 639 */
BgL_res2291z00_3655 = BgL_aux3197z00_5453; }  else 
{ 
 obj_t BgL_auxz00_10316;
BgL_auxz00_10316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25515L), BGl_string3571z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3197z00_5453); 
FAILURE(BgL_auxz00_10316,BFALSE,BFALSE);} } 
BgL_tmpz00_10312 = BgL_res2291z00_3655; } 
BgL_test4391z00_10311 = 
(BgL_tmpz00_10312==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4391z00_10311)
{ /* Llib/weakhash.scm 639 */
 int BgL_tmpz00_10321;
BgL_tmpz00_10321 = 
(int)(2L); 
BgL_bucketsz00_1848 = 
STRUCT_REF(BgL_tablez00_109, BgL_tmpz00_10321); }  else 
{ /* Llib/weakhash.scm 639 */
BgL_bucketsz00_1848 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_109); } } 
{ /* Llib/weakhash.scm 639 */
 long BgL_bucketzd2lenzd2_1849;
{ /* Llib/weakhash.scm 640 */
 obj_t BgL_vectorz00_3656;
if(
VECTORP(BgL_bucketsz00_1848))
{ /* Llib/weakhash.scm 640 */
BgL_vectorz00_3656 = BgL_bucketsz00_1848; }  else 
{ 
 obj_t BgL_auxz00_10327;
BgL_auxz00_10327 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25573L), BGl_string3571z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1848); 
FAILURE(BgL_auxz00_10327,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1849 = 
VECTOR_LENGTH(BgL_vectorz00_3656); } 
{ /* Llib/weakhash.scm 640 */
 long BgL_bucketzd2numzd2_1850;
{ /* Llib/weakhash.scm 641 */
 long BgL_arg1724z00_1887;
BgL_arg1724z00_1887 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_109, BgL_keyz00_110); 
{ /* Llib/weakhash.scm 641 */
 long BgL_n1z00_3657; long BgL_n2z00_3658;
BgL_n1z00_3657 = BgL_arg1724z00_1887; 
BgL_n2z00_3658 = BgL_bucketzd2lenzd2_1849; 
{ /* Llib/weakhash.scm 641 */
 bool_t BgL_test4394z00_10333;
{ /* Llib/weakhash.scm 641 */
 long BgL_arg2024z00_3660;
BgL_arg2024z00_3660 = 
(((BgL_n1z00_3657) | (BgL_n2z00_3658)) & -2147483648); 
BgL_test4394z00_10333 = 
(BgL_arg2024z00_3660==0L); } 
if(BgL_test4394z00_10333)
{ /* Llib/weakhash.scm 641 */
 int32_t BgL_arg2020z00_3661;
{ /* Llib/weakhash.scm 641 */
 int32_t BgL_arg2021z00_3662; int32_t BgL_arg2022z00_3663;
BgL_arg2021z00_3662 = 
(int32_t)(BgL_n1z00_3657); 
BgL_arg2022z00_3663 = 
(int32_t)(BgL_n2z00_3658); 
BgL_arg2020z00_3661 = 
(BgL_arg2021z00_3662%BgL_arg2022z00_3663); } 
{ /* Llib/weakhash.scm 641 */
 long BgL_arg2135z00_3668;
BgL_arg2135z00_3668 = 
(long)(BgL_arg2020z00_3661); 
BgL_bucketzd2numzd2_1850 = 
(long)(BgL_arg2135z00_3668); } }  else 
{ /* Llib/weakhash.scm 641 */
BgL_bucketzd2numzd2_1850 = 
(BgL_n1z00_3657%BgL_n2z00_3658); } } } } 
{ /* Llib/weakhash.scm 641 */
 obj_t BgL_bucketz00_1851;
{ /* Llib/weakhash.scm 642 */
 obj_t BgL_vectorz00_3670;
if(
VECTORP(BgL_bucketsz00_1848))
{ /* Llib/weakhash.scm 642 */
BgL_vectorz00_3670 = BgL_bucketsz00_1848; }  else 
{ 
 obj_t BgL_auxz00_10344;
BgL_auxz00_10344 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25680L), BGl_string3571z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1848); 
FAILURE(BgL_auxz00_10344,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 642 */
 bool_t BgL_test4396z00_10348;
{ /* Llib/weakhash.scm 642 */
 long BgL_tmpz00_10349;
BgL_tmpz00_10349 = 
VECTOR_LENGTH(BgL_vectorz00_3670); 
BgL_test4396z00_10348 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1850, BgL_tmpz00_10349); } 
if(BgL_test4396z00_10348)
{ /* Llib/weakhash.scm 642 */
BgL_bucketz00_1851 = 
VECTOR_REF(BgL_vectorz00_3670,BgL_bucketzd2numzd2_1850); }  else 
{ 
 obj_t BgL_auxz00_10353;
BgL_auxz00_10353 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25668L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3670, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3670)), 
(int)(BgL_bucketzd2numzd2_1850)); 
FAILURE(BgL_auxz00_10353,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 642 */
 obj_t BgL_maxzd2bucketzd2lenz00_1852;
{ /* Llib/weakhash.scm 643 */
 bool_t BgL_test4397z00_10360;
{ /* Llib/weakhash.scm 643 */
 obj_t BgL_tmpz00_10361;
{ /* Llib/weakhash.scm 643 */
 obj_t BgL_res2292z00_3675;
{ /* Llib/weakhash.scm 643 */
 obj_t BgL_aux3203z00_5459;
BgL_aux3203z00_5459 = 
STRUCT_KEY(BgL_tablez00_109); 
if(
SYMBOLP(BgL_aux3203z00_5459))
{ /* Llib/weakhash.scm 643 */
BgL_res2292z00_3675 = BgL_aux3203z00_5459; }  else 
{ 
 obj_t BgL_auxz00_10365;
BgL_auxz00_10365 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25720L), BGl_string3571z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3203z00_5459); 
FAILURE(BgL_auxz00_10365,BFALSE,BFALSE);} } 
BgL_tmpz00_10361 = BgL_res2292z00_3675; } 
BgL_test4397z00_10360 = 
(BgL_tmpz00_10361==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4397z00_10360)
{ /* Llib/weakhash.scm 643 */
 int BgL_tmpz00_10370;
BgL_tmpz00_10370 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1852 = 
STRUCT_REF(BgL_tablez00_109, BgL_tmpz00_10370); }  else 
{ /* Llib/weakhash.scm 643 */
BgL_maxzd2bucketzd2lenz00_1852 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_109); } } 
{ /* Llib/weakhash.scm 643 */
 obj_t BgL_countz00_4556;
BgL_countz00_4556 = 
MAKE_CELL(
BINT(0L)); 
{ /* Llib/weakhash.scm 644 */
 obj_t BgL_foundz00_1854;
{ /* Llib/weakhash.scm 649 */
 obj_t BgL_zc3z04anonymousza31718ze3z87_4546;
{ 
 int BgL_tmpz00_10375;
BgL_tmpz00_10375 = 
(int)(4L); 
BgL_zc3z04anonymousza31718ze3z87_4546 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31718ze3ze5zz__weakhashz00, BgL_tmpz00_10375); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31718ze3z87_4546, 
(int)(0L), 
((obj_t)BgL_countz00_4556)); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31718ze3z87_4546, 
(int)(1L), BgL_procz00_111); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31718ze3z87_4546, 
(int)(2L), BgL_tablez00_109); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31718ze3z87_4546, 
(int)(3L), BgL_keyz00_110); 
BgL_foundz00_1854 = 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_109, BgL_bucketsz00_1848, BgL_bucketzd2numzd2_1850, BgL_zc3z04anonymousza31718ze3z87_4546); } 
{ /* Llib/weakhash.scm 646 */

if(
(BgL_foundz00_1854==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 662 */
 obj_t BgL_vz00_1855;
if(
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_tablez00_109))
{ /* Llib/weakhash.scm 663 */
 obj_t BgL_arg1714z00_1868; obj_t BgL_arg1715z00_1869;
{ /* Llib/weakhash.scm 663 */

BgL_arg1714z00_1868 = 
bgl_make_weakptr(BgL_objz00_112, BFALSE); } 
{ /* Llib/weakhash.scm 663 */

BgL_arg1715z00_1869 = 
bgl_make_weakptr(BgL_initz00_113, BFALSE); } 
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_111, 2))
{ /* Llib/weakhash.scm 663 */
BgL_vz00_1855 = 
BGL_PROCEDURE_CALL2(BgL_procz00_111, BgL_arg1714z00_1868, BgL_arg1715z00_1869); }  else 
{ /* Llib/weakhash.scm 663 */
FAILURE(BGl_string3572z00zz__weakhashz00,BGl_list3573z00zz__weakhashz00,BgL_procz00_111);} }  else 
{ /* Llib/weakhash.scm 662 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_111, 2))
{ /* Llib/weakhash.scm 664 */
BgL_vz00_1855 = 
BGL_PROCEDURE_CALL2(BgL_procz00_111, BgL_objz00_112, BgL_initz00_113); }  else 
{ /* Llib/weakhash.scm 664 */
FAILURE(BGl_string3572z00zz__weakhashz00,BGl_list3560z00zz__weakhashz00,BgL_procz00_111);} } 
{ /* Llib/weakhash.scm 665 */
 long BgL_arg1703z00_1856;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_arg1704z00_1857;
{ /* Llib/weakhash.scm 665 */
 bool_t BgL_test4403z00_10410;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_tmpz00_10411;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_res2294z00_3701;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_aux3207z00_5465;
BgL_aux3207z00_5465 = 
STRUCT_KEY(BgL_tablez00_109); 
if(
SYMBOLP(BgL_aux3207z00_5465))
{ /* Llib/weakhash.scm 665 */
BgL_res2294z00_3701 = BgL_aux3207z00_5465; }  else 
{ 
 obj_t BgL_auxz00_10415;
BgL_auxz00_10415 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26456L), BGl_string3571z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3207z00_5465); 
FAILURE(BgL_auxz00_10415,BFALSE,BFALSE);} } 
BgL_tmpz00_10411 = BgL_res2294z00_3701; } 
BgL_test4403z00_10410 = 
(BgL_tmpz00_10411==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4403z00_10410)
{ /* Llib/weakhash.scm 665 */
 int BgL_tmpz00_10420;
BgL_tmpz00_10420 = 
(int)(0L); 
BgL_arg1704z00_1857 = 
STRUCT_REF(BgL_tablez00_109, BgL_tmpz00_10420); }  else 
{ /* Llib/weakhash.scm 665 */
BgL_arg1704z00_1857 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_109); } } 
{ /* Llib/weakhash.scm 665 */
 long BgL_za71za7_3702;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_tmpz00_10424;
if(
INTEGERP(BgL_arg1704z00_1857))
{ /* Llib/weakhash.scm 665 */
BgL_tmpz00_10424 = BgL_arg1704z00_1857
; }  else 
{ 
 obj_t BgL_auxz00_10427;
BgL_auxz00_10427 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26478L), BGl_string3571z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1704z00_1857); 
FAILURE(BgL_auxz00_10427,BFALSE,BFALSE);} 
BgL_za71za7_3702 = 
(long)CINT(BgL_tmpz00_10424); } 
BgL_arg1703z00_1856 = 
(BgL_za71za7_3702+1L); } } 
{ /* Llib/weakhash.scm 665 */
 bool_t BgL_test4406z00_10433;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_tmpz00_10434;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_res2295z00_3706;
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_aux3210z00_5468;
BgL_aux3210z00_5468 = 
STRUCT_KEY(BgL_tablez00_109); 
if(
SYMBOLP(BgL_aux3210z00_5468))
{ /* Llib/weakhash.scm 665 */
BgL_res2295z00_3706 = BgL_aux3210z00_5468; }  else 
{ 
 obj_t BgL_auxz00_10438;
BgL_auxz00_10438 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26423L), BGl_string3571z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3210z00_5468); 
FAILURE(BgL_auxz00_10438,BFALSE,BFALSE);} } 
BgL_tmpz00_10434 = BgL_res2295z00_3706; } 
BgL_test4406z00_10433 = 
(BgL_tmpz00_10434==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4406z00_10433)
{ /* Llib/weakhash.scm 665 */
 obj_t BgL_auxz00_10445; int BgL_tmpz00_10443;
BgL_auxz00_10445 = 
BINT(BgL_arg1703z00_1856); 
BgL_tmpz00_10443 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_109, BgL_tmpz00_10443, BgL_auxz00_10445); }  else 
{ /* Llib/weakhash.scm 665 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_109); } } } 
{ /* Llib/weakhash.scm 667 */
 obj_t BgL_arg1705z00_1858;
{ /* Llib/weakhash.scm 667 */
 obj_t BgL_arg1706z00_1859; obj_t BgL_arg1707z00_1860;
{ /* Llib/weakhash.scm 667 */
 obj_t BgL_arg1708z00_1861;
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_109))
{ /* Llib/weakhash.scm 668 */

BgL_arg1708z00_1861 = 
bgl_make_weakptr(BgL_keyz00_110, BFALSE); }  else 
{ /* Llib/weakhash.scm 667 */
BgL_arg1708z00_1861 = BgL_keyz00_110; } 
BgL_arg1706z00_1859 = 
MAKE_YOUNG_PAIR(BgL_arg1708z00_1861, BgL_vz00_1855); } 
{ /* Llib/weakhash.scm 673 */
 obj_t BgL_arg1710z00_1865;
{ /* Llib/weakhash.scm 673 */
 bool_t BgL_test4409z00_10453;
{ /* Llib/weakhash.scm 673 */
 obj_t BgL_tmpz00_10454;
{ /* Llib/weakhash.scm 673 */
 obj_t BgL_res2296z00_3710;
{ /* Llib/weakhash.scm 673 */
 obj_t BgL_aux3212z00_5470;
BgL_aux3212z00_5470 = 
STRUCT_KEY(BgL_tablez00_109); 
if(
SYMBOLP(BgL_aux3212z00_5470))
{ /* Llib/weakhash.scm 673 */
BgL_res2296z00_3710 = BgL_aux3212z00_5470; }  else 
{ 
 obj_t BgL_auxz00_10458;
BgL_auxz00_10458 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26706L), BGl_string3571z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3212z00_5470); 
FAILURE(BgL_auxz00_10458,BFALSE,BFALSE);} } 
BgL_tmpz00_10454 = BgL_res2296z00_3710; } 
BgL_test4409z00_10453 = 
(BgL_tmpz00_10454==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4409z00_10453)
{ /* Llib/weakhash.scm 673 */
 int BgL_tmpz00_10463;
BgL_tmpz00_10463 = 
(int)(2L); 
BgL_arg1710z00_1865 = 
STRUCT_REF(BgL_tablez00_109, BgL_tmpz00_10463); }  else 
{ /* Llib/weakhash.scm 673 */
BgL_arg1710z00_1865 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_109); } } 
{ /* Llib/weakhash.scm 673 */
 obj_t BgL_vectorz00_3711;
if(
VECTORP(BgL_arg1710z00_1865))
{ /* Llib/weakhash.scm 673 */
BgL_vectorz00_3711 = BgL_arg1710z00_1865; }  else 
{ 
 obj_t BgL_auxz00_10469;
BgL_auxz00_10469 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26731L), BGl_string3571z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_arg1710z00_1865); 
FAILURE(BgL_auxz00_10469,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 673 */
 bool_t BgL_test4412z00_10473;
{ /* Llib/weakhash.scm 673 */
 long BgL_tmpz00_10474;
BgL_tmpz00_10474 = 
VECTOR_LENGTH(BgL_vectorz00_3711); 
BgL_test4412z00_10473 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1850, BgL_tmpz00_10474); } 
if(BgL_test4412z00_10473)
{ /* Llib/weakhash.scm 673 */
BgL_arg1707z00_1860 = 
VECTOR_REF(BgL_vectorz00_3711,BgL_bucketzd2numzd2_1850); }  else 
{ 
 obj_t BgL_auxz00_10478;
BgL_auxz00_10478 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26694L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3711, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3711)), 
(int)(BgL_bucketzd2numzd2_1850)); 
FAILURE(BgL_auxz00_10478,BFALSE,BFALSE);} } } } 
BgL_arg1705z00_1858 = 
MAKE_YOUNG_PAIR(BgL_arg1706z00_1859, BgL_arg1707z00_1860); } 
{ /* Llib/weakhash.scm 666 */
 obj_t BgL_vectorz00_3713;
if(
VECTORP(BgL_bucketsz00_1848))
{ /* Llib/weakhash.scm 666 */
BgL_vectorz00_3713 = BgL_bucketsz00_1848; }  else 
{ 
 obj_t BgL_auxz00_10488;
BgL_auxz00_10488 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26503L), BGl_string3571z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1848); 
FAILURE(BgL_auxz00_10488,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 666 */
 bool_t BgL_test4414z00_10492;
{ /* Llib/weakhash.scm 666 */
 long BgL_tmpz00_10493;
BgL_tmpz00_10493 = 
VECTOR_LENGTH(BgL_vectorz00_3713); 
BgL_test4414z00_10492 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1850, BgL_tmpz00_10493); } 
if(BgL_test4414z00_10492)
{ /* Llib/weakhash.scm 666 */
VECTOR_SET(BgL_vectorz00_3713,BgL_bucketzd2numzd2_1850,BgL_arg1705z00_1858); }  else 
{ 
 obj_t BgL_auxz00_10497;
BgL_auxz00_10497 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26490L), BGl_string3450z00zz__weakhashz00, BgL_vectorz00_3713, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3713)), 
(int)(BgL_bucketzd2numzd2_1850)); 
FAILURE(BgL_auxz00_10497,BFALSE,BFALSE);} } } } 
{ /* Llib/weakhash.scm 675 */
 bool_t BgL_test4415z00_10504;
{ /* Llib/weakhash.scm 675 */
 long BgL_n1z00_3715; long BgL_n2z00_3716;
{ /* Llib/weakhash.scm 675 */
 obj_t BgL_tmpz00_10505;
{ /* Llib/weakhash.scm 675 */
 obj_t BgL_aux3218z00_5476;
BgL_aux3218z00_5476 = 
CELL_REF(BgL_countz00_4556); 
if(
INTEGERP(BgL_aux3218z00_5476))
{ /* Llib/weakhash.scm 675 */
BgL_tmpz00_10505 = BgL_aux3218z00_5476
; }  else 
{ 
 obj_t BgL_auxz00_10508;
BgL_auxz00_10508 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26770L), BGl_string3571z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3218z00_5476); 
FAILURE(BgL_auxz00_10508,BFALSE,BFALSE);} } 
BgL_n1z00_3715 = 
(long)CINT(BgL_tmpz00_10505); } 
{ /* Llib/weakhash.scm 675 */
 obj_t BgL_tmpz00_10513;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1852))
{ /* Llib/weakhash.scm 675 */
BgL_tmpz00_10513 = BgL_maxzd2bucketzd2lenz00_1852
; }  else 
{ 
 obj_t BgL_auxz00_10516;
BgL_auxz00_10516 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26776L), BGl_string3571z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1852); 
FAILURE(BgL_auxz00_10516,BFALSE,BFALSE);} 
BgL_n2z00_3716 = 
(long)CINT(BgL_tmpz00_10513); } 
BgL_test4415z00_10504 = 
(BgL_n1z00_3715>BgL_n2z00_3716); } 
if(BgL_test4415z00_10504)
{ /* Llib/weakhash.scm 675 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_109))
{ /* Llib/weakhash.scm 877 */
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_109); }  else 
{ /* Llib/weakhash.scm 877 */
BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_109); } }  else 
{ /* Llib/weakhash.scm 675 */BFALSE; } } 
return BgL_vz00_1855;}  else 
{ /* Llib/weakhash.scm 660 */
return BgL_foundz00_1854;} } } } } } } } } } 

}



/* &<@anonymous:1718> */
obj_t BGl_z62zc3z04anonymousza31718ze3ze5zz__weakhashz00(obj_t BgL_envz00_4547, obj_t BgL_bkeyz00_4552, obj_t BgL_valz00_4553, obj_t BgL_bucketz00_4554)
{
{ /* Llib/weakhash.scm 648 */
{ /* Llib/weakhash.scm 649 */
 obj_t BgL_countz00_4548; obj_t BgL_procz00_4549; obj_t BgL_tablez00_4550; obj_t BgL_keyz00_4551;
BgL_countz00_4548 = 
PROCEDURE_L_REF(BgL_envz00_4547, 
(int)(0L)); 
BgL_procz00_4549 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4547, 
(int)(1L))); 
BgL_tablez00_4550 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4547, 
(int)(2L))); 
BgL_keyz00_4551 = 
PROCEDURE_L_REF(BgL_envz00_4547, 
(int)(3L)); 
{ /* Llib/weakhash.scm 649 */
 obj_t BgL_auxz00_5776;
{ /* Llib/weakhash.scm 649 */
 obj_t BgL_tmpz00_10536;
{ /* Llib/weakhash.scm 649 */
 obj_t BgL_aux3220z00_5777;
BgL_aux3220z00_5777 = 
CELL_REF(BgL_countz00_4548); 
if(
INTEGERP(BgL_aux3220z00_5777))
{ /* Llib/weakhash.scm 649 */
BgL_tmpz00_10536 = BgL_aux3220z00_5777
; }  else 
{ 
 obj_t BgL_auxz00_10539;
BgL_auxz00_10539 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25933L), BGl_string3578z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3220z00_5777); 
FAILURE(BgL_auxz00_10539,BFALSE,BFALSE);} } 
BgL_auxz00_5776 = 
ADDFX(BgL_tmpz00_10536, 
BINT(1L)); } 
CELL_SET(BgL_countz00_4548, BgL_auxz00_5776); } 
{ /* Llib/weakhash.scm 650 */
 bool_t BgL_test4420z00_10545;
{ /* Llib/weakhash.scm 650 */
 obj_t BgL_eqtz00_5778;
{ /* Llib/weakhash.scm 650 */
 bool_t BgL_test4421z00_10546;
{ /* Llib/weakhash.scm 650 */
 obj_t BgL_tmpz00_10547;
{ /* Llib/weakhash.scm 650 */
 obj_t BgL_res2293z00_5779;
{ /* Llib/weakhash.scm 650 */
 obj_t BgL_aux3221z00_5780;
BgL_aux3221z00_5780 = 
STRUCT_KEY(BgL_tablez00_4550); 
if(
SYMBOLP(BgL_aux3221z00_5780))
{ /* Llib/weakhash.scm 650 */
BgL_res2293z00_5779 = BgL_aux3221z00_5780; }  else 
{ 
 obj_t BgL_auxz00_10551;
BgL_auxz00_10551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(25956L), BGl_string3578z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3221z00_5780); 
FAILURE(BgL_auxz00_10551,BFALSE,BFALSE);} } 
BgL_tmpz00_10547 = BgL_res2293z00_5779; } 
BgL_test4421z00_10546 = 
(BgL_tmpz00_10547==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4421z00_10546)
{ /* Llib/weakhash.scm 650 */
 int BgL_tmpz00_10556;
BgL_tmpz00_10556 = 
(int)(3L); 
BgL_eqtz00_5778 = 
STRUCT_REF(BgL_tablez00_4550, BgL_tmpz00_10556); }  else 
{ /* Llib/weakhash.scm 650 */
BgL_eqtz00_5778 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_4550); } } 
if(
PROCEDUREP(BgL_eqtz00_5778))
{ /* Llib/weakhash.scm 650 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_5778, 2))
{ /* Llib/weakhash.scm 650 */
BgL_test4420z00_10545 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_5778, BgL_bkeyz00_4552, BgL_keyz00_4551))
; }  else 
{ /* Llib/weakhash.scm 650 */
FAILURE(BGl_string3579z00zz__weakhashz00,BGl_list3542z00zz__weakhashz00,BgL_eqtz00_5778);} }  else 
{ /* Llib/weakhash.scm 650 */
if(
(BgL_bkeyz00_4552==BgL_keyz00_4551))
{ /* Llib/weakhash.scm 650 */
BgL_test4420z00_10545 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 650 */
if(
STRINGP(BgL_bkeyz00_4552))
{ /* Llib/weakhash.scm 650 */
if(
STRINGP(BgL_keyz00_4551))
{ /* Llib/weakhash.scm 650 */
 long BgL_l1z00_5781;
BgL_l1z00_5781 = 
STRING_LENGTH(BgL_bkeyz00_4552); 
if(
(BgL_l1z00_5781==
STRING_LENGTH(BgL_keyz00_4551)))
{ /* Llib/weakhash.scm 650 */
 int BgL_arg1918z00_5782;
{ /* Llib/weakhash.scm 650 */
 char * BgL_auxz00_10583; char * BgL_tmpz00_10581;
BgL_auxz00_10583 = 
BSTRING_TO_STRING(BgL_keyz00_4551); 
BgL_tmpz00_10581 = 
BSTRING_TO_STRING(BgL_bkeyz00_4552); 
BgL_arg1918z00_5782 = 
memcmp(BgL_tmpz00_10581, BgL_auxz00_10583, BgL_l1z00_5781); } 
BgL_test4420z00_10545 = 
(
(long)(BgL_arg1918z00_5782)==0L); }  else 
{ /* Llib/weakhash.scm 650 */
BgL_test4420z00_10545 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 650 */
BgL_test4420z00_10545 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 650 */
BgL_test4420z00_10545 = ((bool_t)0)
; } } } } 
if(BgL_test4420z00_10545)
{ /* Llib/weakhash.scm 651 */
 obj_t BgL_newvalz00_5783;
if(
PROCEDURE_CORRECT_ARITYP(BgL_procz00_4549, 1))
{ /* Llib/weakhash.scm 651 */
BgL_newvalz00_5783 = 
BGL_PROCEDURE_CALL1(BgL_procz00_4549, BgL_valz00_4553); }  else 
{ /* Llib/weakhash.scm 651 */
FAILURE(BGl_string3579z00zz__weakhashz00,BGl_list3556z00zz__weakhashz00,BgL_procz00_4549);} 
{ /* Llib/weakhash.scm 652 */
 obj_t BgL_arg1720z00_5784; obj_t BgL_arg1722z00_5785;
{ /* Llib/weakhash.scm 652 */
 obj_t BgL_pairz00_5786;
if(
PAIRP(BgL_bucketz00_4554))
{ /* Llib/weakhash.scm 652 */
BgL_pairz00_5786 = BgL_bucketz00_4554; }  else 
{ 
 obj_t BgL_auxz00_10597;
BgL_auxz00_10597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26047L), BGl_string3578z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_4554); 
FAILURE(BgL_auxz00_10597,BFALSE,BFALSE);} 
BgL_arg1720z00_5784 = 
CAR(BgL_pairz00_5786); } 
if(
BGl_hashtablezd2weakzd2datazf3zf3zz__hashz00(BgL_tablez00_4550))
{ /* Llib/weakhash.scm 654 */

BgL_arg1722z00_5785 = 
bgl_make_weakptr(BgL_newvalz00_5783, BFALSE); }  else 
{ /* Llib/weakhash.scm 653 */
BgL_arg1722z00_5785 = BgL_newvalz00_5783; } 
{ /* Llib/weakhash.scm 652 */
 obj_t BgL_pairz00_5787;
if(
PAIRP(BgL_arg1720z00_5784))
{ /* Llib/weakhash.scm 652 */
BgL_pairz00_5787 = BgL_arg1720z00_5784; }  else 
{ 
 obj_t BgL_auxz00_10607;
BgL_auxz00_10607 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(26053L), BGl_string3578z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_arg1720z00_5784); 
FAILURE(BgL_auxz00_10607,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_5787, BgL_arg1722z00_5785); } } 
return BgL_newvalz00_5783;}  else 
{ /* Llib/weakhash.scm 650 */
return BGl_keepgoingz00zz__weakhashz00;} } } } 

}



/* weak-hashtable-add! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(obj_t BgL_tablez00_114, obj_t BgL_keyz00_115, obj_t BgL_procz00_116, obj_t BgL_objz00_117, obj_t BgL_initz00_118)
{
{ /* Llib/weakhash.scm 682 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_114))
{ /* Llib/weakhash.scm 683 */
BGL_TAIL return 
BGl_weakzd2keyszd2hashtablezd2addz12zc0zz__weakhashz00(BgL_tablez00_114, BgL_keyz00_115, BgL_procz00_116, BgL_objz00_117, BgL_initz00_118);}  else 
{ /* Llib/weakhash.scm 683 */
BGL_TAIL return 
BGl_weakzd2oldzd2hashtablezd2addz12zc0zz__weakhashz00(BgL_tablez00_114, BgL_keyz00_115, BgL_procz00_116, BgL_objz00_117, BgL_initz00_118);} } 

}



/* &weak-hashtable-add! */
obj_t BGl_z62weakzd2hashtablezd2addz12z70zz__weakhashz00(obj_t BgL_envz00_4558, obj_t BgL_tablez00_4559, obj_t BgL_keyz00_4560, obj_t BgL_procz00_4561, obj_t BgL_objz00_4562, obj_t BgL_initz00_4563)
{
{ /* Llib/weakhash.scm 682 */
{ /* Llib/weakhash.scm 683 */
 obj_t BgL_auxz00_10623; obj_t BgL_auxz00_10616;
if(
PROCEDUREP(BgL_procz00_4561))
{ /* Llib/weakhash.scm 683 */
BgL_auxz00_10623 = BgL_procz00_4561
; }  else 
{ 
 obj_t BgL_auxz00_10626;
BgL_auxz00_10626 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27143L), BGl_string3580z00zz__weakhashz00, BGl_string3455z00zz__weakhashz00, BgL_procz00_4561); 
FAILURE(BgL_auxz00_10626,BFALSE,BFALSE);} 
if(
STRUCTP(BgL_tablez00_4559))
{ /* Llib/weakhash.scm 683 */
BgL_auxz00_10616 = BgL_tablez00_4559
; }  else 
{ 
 obj_t BgL_auxz00_10619;
BgL_auxz00_10619 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27143L), BGl_string3580z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4559); 
FAILURE(BgL_auxz00_10619,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2addz12z12zz__weakhashz00(BgL_auxz00_10616, BgL_keyz00_4560, BgL_auxz00_10623, BgL_objz00_4562, BgL_initz00_4563);} } 

}



/* weak-keys-hashtable-remove! */
bool_t BGl_weakzd2keyszd2hashtablezd2removez12zc0zz__weakhashz00(obj_t BgL_tablez00_119, obj_t BgL_keyz00_120)
{
{ /* Llib/weakhash.scm 690 */
{ /* Llib/weakhash.scm 691 */
 obj_t BgL_bucketsz00_1889;
{ /* Llib/weakhash.scm 691 */
 bool_t BgL_test4436z00_10631;
{ /* Llib/weakhash.scm 691 */
 obj_t BgL_tmpz00_10632;
{ /* Llib/weakhash.scm 691 */
 obj_t BgL_res2297z00_3723;
{ /* Llib/weakhash.scm 691 */
 obj_t BgL_aux3233z00_5493;
BgL_aux3233z00_5493 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3233z00_5493))
{ /* Llib/weakhash.scm 691 */
BgL_res2297z00_3723 = BgL_aux3233z00_5493; }  else 
{ 
 obj_t BgL_auxz00_10636;
BgL_auxz00_10636 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27596L), BGl_string3581z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3233z00_5493); 
FAILURE(BgL_auxz00_10636,BFALSE,BFALSE);} } 
BgL_tmpz00_10632 = BgL_res2297z00_3723; } 
BgL_test4436z00_10631 = 
(BgL_tmpz00_10632==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4436z00_10631)
{ /* Llib/weakhash.scm 691 */
 int BgL_tmpz00_10641;
BgL_tmpz00_10641 = 
(int)(2L); 
BgL_bucketsz00_1889 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_10641); }  else 
{ /* Llib/weakhash.scm 691 */
BgL_bucketsz00_1889 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } 
{ /* Llib/weakhash.scm 691 */
 long BgL_bucketzd2lenzd2_1890;
{ /* Llib/weakhash.scm 692 */
 obj_t BgL_vectorz00_3724;
if(
VECTORP(BgL_bucketsz00_1889))
{ /* Llib/weakhash.scm 692 */
BgL_vectorz00_3724 = BgL_bucketsz00_1889; }  else 
{ 
 obj_t BgL_auxz00_10647;
BgL_auxz00_10647 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27654L), BGl_string3581z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1889); 
FAILURE(BgL_auxz00_10647,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1890 = 
VECTOR_LENGTH(BgL_vectorz00_3724); } 
{ /* Llib/weakhash.scm 692 */
 long BgL_bucketzd2numzd2_1891;
{ /* Llib/weakhash.scm 693 */
 long BgL_arg1748z00_1918;
BgL_arg1748z00_1918 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_119, BgL_keyz00_120); 
{ /* Llib/weakhash.scm 693 */
 long BgL_n1z00_3725; long BgL_n2z00_3726;
BgL_n1z00_3725 = BgL_arg1748z00_1918; 
BgL_n2z00_3726 = BgL_bucketzd2lenzd2_1890; 
{ /* Llib/weakhash.scm 693 */
 bool_t BgL_test4439z00_10653;
{ /* Llib/weakhash.scm 693 */
 long BgL_arg2024z00_3728;
BgL_arg2024z00_3728 = 
(((BgL_n1z00_3725) | (BgL_n2z00_3726)) & -2147483648); 
BgL_test4439z00_10653 = 
(BgL_arg2024z00_3728==0L); } 
if(BgL_test4439z00_10653)
{ /* Llib/weakhash.scm 693 */
 int32_t BgL_arg2020z00_3729;
{ /* Llib/weakhash.scm 693 */
 int32_t BgL_arg2021z00_3730; int32_t BgL_arg2022z00_3731;
BgL_arg2021z00_3730 = 
(int32_t)(BgL_n1z00_3725); 
BgL_arg2022z00_3731 = 
(int32_t)(BgL_n2z00_3726); 
BgL_arg2020z00_3729 = 
(BgL_arg2021z00_3730%BgL_arg2022z00_3731); } 
{ /* Llib/weakhash.scm 693 */
 long BgL_arg2135z00_3736;
BgL_arg2135z00_3736 = 
(long)(BgL_arg2020z00_3729); 
BgL_bucketzd2numzd2_1891 = 
(long)(BgL_arg2135z00_3736); } }  else 
{ /* Llib/weakhash.scm 693 */
BgL_bucketzd2numzd2_1891 = 
(BgL_n1z00_3725%BgL_n2z00_3726); } } } } 
{ /* Llib/weakhash.scm 693 */
 obj_t BgL_bucketz00_1892;
{ /* Llib/weakhash.scm 694 */
 obj_t BgL_vectorz00_3738;
if(
VECTORP(BgL_bucketsz00_1889))
{ /* Llib/weakhash.scm 694 */
BgL_vectorz00_3738 = BgL_bucketsz00_1889; }  else 
{ 
 obj_t BgL_auxz00_10664;
BgL_auxz00_10664 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27764L), BGl_string3581z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1889); 
FAILURE(BgL_auxz00_10664,BFALSE,BFALSE);} 
BgL_bucketz00_1892 = 
VECTOR_REF(BgL_vectorz00_3738,BgL_bucketzd2numzd2_1891); } 
{ /* Llib/weakhash.scm 694 */

if(
NULLP(BgL_bucketz00_1892))
{ /* Llib/weakhash.scm 696 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 698 */
 bool_t BgL_test4442z00_10671;
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_arg1746z00_1916;
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_arg1747z00_1917;
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_pairz00_3740;
if(
PAIRP(BgL_bucketz00_1892))
{ /* Llib/weakhash.scm 698 */
BgL_pairz00_3740 = BgL_bucketz00_1892; }  else 
{ 
 obj_t BgL_auxz00_10674;
BgL_auxz00_10674 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27869L), BGl_string3581z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1892); 
FAILURE(BgL_auxz00_10674,BFALSE,BFALSE);} 
BgL_arg1747z00_1917 = 
CAR(BgL_pairz00_3740); } 
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_objz00_3741;
if(
BGL_WEAKPTRP(BgL_arg1747z00_1917))
{ /* Llib/weakhash.scm 698 */
BgL_objz00_3741 = BgL_arg1747z00_1917; }  else 
{ 
 obj_t BgL_auxz00_10681;
BgL_auxz00_10681 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27875L), BGl_string3581z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1747z00_1917); 
FAILURE(BgL_auxz00_10681,BFALSE,BFALSE);} 
BgL_arg1746z00_1916 = 
bgl_weakptr_data(BgL_objz00_3741); } } 
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_eqtz00_3742;
{ /* Llib/weakhash.scm 698 */
 bool_t BgL_test4445z00_10686;
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_tmpz00_10687;
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_res2298z00_3749;
{ /* Llib/weakhash.scm 698 */
 obj_t BgL_aux3243z00_5503;
BgL_aux3243z00_5503 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3243z00_5503))
{ /* Llib/weakhash.scm 698 */
BgL_res2298z00_3749 = BgL_aux3243z00_5503; }  else 
{ 
 obj_t BgL_auxz00_10691;
BgL_auxz00_10691 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27826L), BGl_string3581z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3243z00_5503); 
FAILURE(BgL_auxz00_10691,BFALSE,BFALSE);} } 
BgL_tmpz00_10687 = BgL_res2298z00_3749; } 
BgL_test4445z00_10686 = 
(BgL_tmpz00_10687==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4445z00_10686)
{ /* Llib/weakhash.scm 698 */
 int BgL_tmpz00_10696;
BgL_tmpz00_10696 = 
(int)(3L); 
BgL_eqtz00_3742 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_10696); }  else 
{ /* Llib/weakhash.scm 698 */
BgL_eqtz00_3742 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } 
if(
PROCEDUREP(BgL_eqtz00_3742))
{ /* Llib/weakhash.scm 698 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3742, 2))
{ /* Llib/weakhash.scm 698 */
BgL_test4442z00_10671 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3742, BgL_arg1746z00_1916, BgL_keyz00_120))
; }  else 
{ /* Llib/weakhash.scm 698 */
FAILURE(BGl_string3582z00zz__weakhashz00,BGl_list3583z00zz__weakhashz00,BgL_eqtz00_3742);} }  else 
{ /* Llib/weakhash.scm 698 */
if(
(BgL_arg1746z00_1916==BgL_keyz00_120))
{ /* Llib/weakhash.scm 698 */
BgL_test4442z00_10671 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 698 */
if(
STRINGP(BgL_arg1746z00_1916))
{ /* Llib/weakhash.scm 698 */
if(
STRINGP(BgL_keyz00_120))
{ /* Llib/weakhash.scm 698 */
 long BgL_l1z00_3752;
BgL_l1z00_3752 = 
STRING_LENGTH(BgL_arg1746z00_1916); 
if(
(BgL_l1z00_3752==
STRING_LENGTH(BgL_keyz00_120)))
{ /* Llib/weakhash.scm 698 */
 int BgL_arg1918z00_3755;
{ /* Llib/weakhash.scm 698 */
 char * BgL_auxz00_10723; char * BgL_tmpz00_10721;
BgL_auxz00_10723 = 
BSTRING_TO_STRING(BgL_keyz00_120); 
BgL_tmpz00_10721 = 
BSTRING_TO_STRING(BgL_arg1746z00_1916); 
BgL_arg1918z00_3755 = 
memcmp(BgL_tmpz00_10721, BgL_auxz00_10723, BgL_l1z00_3752); } 
BgL_test4442z00_10671 = 
(
(long)(BgL_arg1918z00_3755)==0L); }  else 
{ /* Llib/weakhash.scm 698 */
BgL_test4442z00_10671 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 698 */
BgL_test4442z00_10671 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 698 */
BgL_test4442z00_10671 = ((bool_t)0)
; } } } } } 
if(BgL_test4442z00_10671)
{ /* Llib/weakhash.scm 698 */
{ /* Llib/weakhash.scm 699 */
 obj_t BgL_arg1730z00_1897;
{ /* Llib/weakhash.scm 699 */
 obj_t BgL_pairz00_3761;
if(
PAIRP(BgL_bucketz00_1892))
{ /* Llib/weakhash.scm 699 */
BgL_pairz00_3761 = BgL_bucketz00_1892; }  else 
{ 
 obj_t BgL_auxz00_10730;
BgL_auxz00_10730 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27926L), BGl_string3581z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1892); 
FAILURE(BgL_auxz00_10730,BFALSE,BFALSE);} 
BgL_arg1730z00_1897 = 
CDR(BgL_pairz00_3761); } 
{ /* Llib/weakhash.scm 699 */
 obj_t BgL_vectorz00_3762;
if(
VECTORP(BgL_bucketsz00_1889))
{ /* Llib/weakhash.scm 699 */
BgL_vectorz00_3762 = BgL_bucketsz00_1889; }  else 
{ 
 obj_t BgL_auxz00_10737;
BgL_auxz00_10737 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27902L), BGl_string3581z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1889); 
FAILURE(BgL_auxz00_10737,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3762,BgL_bucketzd2numzd2_1891,BgL_arg1730z00_1897); } } 
{ /* Llib/weakhash.scm 700 */
 long BgL_arg1731z00_1898;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_arg1733z00_1899;
{ /* Llib/weakhash.scm 700 */
 bool_t BgL_test4455z00_10742;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_tmpz00_10743;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_res2299z00_3767;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_aux3250z00_5511;
BgL_aux3250z00_5511 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3250z00_5511))
{ /* Llib/weakhash.scm 700 */
BgL_res2299z00_3767 = BgL_aux3250z00_5511; }  else 
{ 
 obj_t BgL_auxz00_10747;
BgL_auxz00_10747 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27971L), BGl_string3581z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3250z00_5511); 
FAILURE(BgL_auxz00_10747,BFALSE,BFALSE);} } 
BgL_tmpz00_10743 = BgL_res2299z00_3767; } 
BgL_test4455z00_10742 = 
(BgL_tmpz00_10743==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4455z00_10742)
{ /* Llib/weakhash.scm 700 */
 int BgL_tmpz00_10752;
BgL_tmpz00_10752 = 
(int)(0L); 
BgL_arg1733z00_1899 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_10752); }  else 
{ /* Llib/weakhash.scm 700 */
BgL_arg1733z00_1899 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } 
{ /* Llib/weakhash.scm 700 */
 long BgL_za71za7_3768;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_tmpz00_10756;
if(
INTEGERP(BgL_arg1733z00_1899))
{ /* Llib/weakhash.scm 700 */
BgL_tmpz00_10756 = BgL_arg1733z00_1899
; }  else 
{ 
 obj_t BgL_auxz00_10759;
BgL_auxz00_10759 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27993L), BGl_string3581z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1733z00_1899); 
FAILURE(BgL_auxz00_10759,BFALSE,BFALSE);} 
BgL_za71za7_3768 = 
(long)CINT(BgL_tmpz00_10756); } 
BgL_arg1731z00_1898 = 
(BgL_za71za7_3768-1L); } } 
{ /* Llib/weakhash.scm 700 */
 bool_t BgL_test4458z00_10765;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_tmpz00_10766;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_res2300z00_3772;
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_aux3253z00_5514;
BgL_aux3253z00_5514 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3253z00_5514))
{ /* Llib/weakhash.scm 700 */
BgL_res2300z00_3772 = BgL_aux3253z00_5514; }  else 
{ 
 obj_t BgL_auxz00_10770;
BgL_auxz00_10770 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(27938L), BGl_string3581z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3253z00_5514); 
FAILURE(BgL_auxz00_10770,BFALSE,BFALSE);} } 
BgL_tmpz00_10766 = BgL_res2300z00_3772; } 
BgL_test4458z00_10765 = 
(BgL_tmpz00_10766==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4458z00_10765)
{ /* Llib/weakhash.scm 700 */
 obj_t BgL_auxz00_10777; int BgL_tmpz00_10775;
BgL_auxz00_10777 = 
BINT(BgL_arg1731z00_1898); 
BgL_tmpz00_10775 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_10775, BgL_auxz00_10777); }  else 
{ /* Llib/weakhash.scm 700 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } } 
return ((bool_t)1);}  else 
{ /* Llib/weakhash.scm 703 */
 obj_t BgL_g1068z00_1900;
{ /* Llib/weakhash.scm 703 */
 obj_t BgL_pairz00_3773;
if(
PAIRP(BgL_bucketz00_1892))
{ /* Llib/weakhash.scm 703 */
BgL_pairz00_3773 = BgL_bucketz00_1892; }  else 
{ 
 obj_t BgL_auxz00_10783;
BgL_auxz00_10783 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28041L), BGl_string3581z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_bucketz00_1892); 
FAILURE(BgL_auxz00_10783,BFALSE,BFALSE);} 
BgL_g1068z00_1900 = 
CDR(BgL_pairz00_3773); } 
{ 
 obj_t BgL_bucketz00_1902; obj_t BgL_prevz00_1903;
BgL_bucketz00_1902 = BgL_g1068z00_1900; 
BgL_prevz00_1903 = BgL_bucketz00_1892; 
BgL_zc3z04anonymousza31734ze3z87_1904:
if(
PAIRP(BgL_bucketz00_1902))
{ /* Llib/weakhash.scm 706 */
 bool_t BgL_test4462z00_10790;
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_arg1744z00_1913;
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_arg1745z00_1914;
BgL_arg1745z00_1914 = 
CAR(BgL_bucketz00_1902); 
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_objz00_3775;
if(
BGL_WEAKPTRP(BgL_arg1745z00_1914))
{ /* Llib/weakhash.scm 706 */
BgL_objz00_3775 = BgL_arg1745z00_1914; }  else 
{ 
 obj_t BgL_auxz00_10794;
BgL_auxz00_10794 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28153L), BGl_string3453z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1745z00_1914); 
FAILURE(BgL_auxz00_10794,BFALSE,BFALSE);} 
BgL_arg1744z00_1913 = 
bgl_weakptr_data(BgL_objz00_3775); } } 
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_eqtz00_3776;
{ /* Llib/weakhash.scm 706 */
 bool_t BgL_test4464z00_10799;
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_tmpz00_10800;
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_res2301z00_3783;
{ /* Llib/weakhash.scm 706 */
 obj_t BgL_aux3259z00_5520;
BgL_aux3259z00_5520 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3259z00_5520))
{ /* Llib/weakhash.scm 706 */
BgL_res2301z00_3783 = BgL_aux3259z00_5520; }  else 
{ 
 obj_t BgL_auxz00_10804;
BgL_auxz00_10804 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28104L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3259z00_5520); 
FAILURE(BgL_auxz00_10804,BFALSE,BFALSE);} } 
BgL_tmpz00_10800 = BgL_res2301z00_3783; } 
BgL_test4464z00_10799 = 
(BgL_tmpz00_10800==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4464z00_10799)
{ /* Llib/weakhash.scm 706 */
 int BgL_tmpz00_10809;
BgL_tmpz00_10809 = 
(int)(3L); 
BgL_eqtz00_3776 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_10809); }  else 
{ /* Llib/weakhash.scm 706 */
BgL_eqtz00_3776 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } 
if(
PROCEDUREP(BgL_eqtz00_3776))
{ /* Llib/weakhash.scm 706 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_3776, 2))
{ /* Llib/weakhash.scm 706 */
BgL_test4462z00_10790 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_3776, BgL_arg1744z00_1913, BgL_keyz00_120))
; }  else 
{ /* Llib/weakhash.scm 706 */
FAILURE(BGl_string3514z00zz__weakhashz00,BGl_list3586z00zz__weakhashz00,BgL_eqtz00_3776);} }  else 
{ /* Llib/weakhash.scm 706 */
if(
(BgL_arg1744z00_1913==BgL_keyz00_120))
{ /* Llib/weakhash.scm 706 */
BgL_test4462z00_10790 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 706 */
if(
STRINGP(BgL_arg1744z00_1913))
{ /* Llib/weakhash.scm 706 */
if(
STRINGP(BgL_keyz00_120))
{ /* Llib/weakhash.scm 706 */
 long BgL_l1z00_3786;
BgL_l1z00_3786 = 
STRING_LENGTH(BgL_arg1744z00_1913); 
if(
(BgL_l1z00_3786==
STRING_LENGTH(BgL_keyz00_120)))
{ /* Llib/weakhash.scm 706 */
 int BgL_arg1918z00_3789;
{ /* Llib/weakhash.scm 706 */
 char * BgL_auxz00_10836; char * BgL_tmpz00_10834;
BgL_auxz00_10836 = 
BSTRING_TO_STRING(BgL_keyz00_120); 
BgL_tmpz00_10834 = 
BSTRING_TO_STRING(BgL_arg1744z00_1913); 
BgL_arg1918z00_3789 = 
memcmp(BgL_tmpz00_10834, BgL_auxz00_10836, BgL_l1z00_3786); } 
BgL_test4462z00_10790 = 
(
(long)(BgL_arg1918z00_3789)==0L); }  else 
{ /* Llib/weakhash.scm 706 */
BgL_test4462z00_10790 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 706 */
BgL_test4462z00_10790 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 706 */
BgL_test4462z00_10790 = ((bool_t)0)
; } } } } } 
if(BgL_test4462z00_10790)
{ /* Llib/weakhash.scm 706 */
{ /* Llib/weakhash.scm 708 */
 obj_t BgL_arg1739z00_1909;
BgL_arg1739z00_1909 = 
CDR(BgL_bucketz00_1902); 
{ /* Llib/weakhash.scm 708 */
 obj_t BgL_pairz00_3796;
if(
PAIRP(BgL_prevz00_1903))
{ /* Llib/weakhash.scm 708 */
BgL_pairz00_3796 = BgL_prevz00_1903; }  else 
{ 
 obj_t BgL_auxz00_10844;
BgL_auxz00_10844 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28188L), BGl_string3453z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_prevz00_1903); 
FAILURE(BgL_auxz00_10844,BFALSE,BFALSE);} 
SET_CDR(BgL_pairz00_3796, BgL_arg1739z00_1909); } } 
{ /* Llib/weakhash.scm 710 */
 long BgL_arg1740z00_1910;
{ /* Llib/weakhash.scm 710 */
 obj_t BgL_arg1741z00_1911;
{ /* Llib/weakhash.scm 710 */
 bool_t BgL_test4473z00_10849;
{ /* Llib/weakhash.scm 710 */
 obj_t BgL_tmpz00_10850;
{ /* Llib/weakhash.scm 710 */
 obj_t BgL_res2302z00_3800;
{ /* Llib/weakhash.scm 710 */
 obj_t BgL_aux3264z00_5526;
BgL_aux3264z00_5526 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3264z00_5526))
{ /* Llib/weakhash.scm 710 */
BgL_res2302z00_3800 = BgL_aux3264z00_5526; }  else 
{ 
 obj_t BgL_auxz00_10854;
BgL_auxz00_10854 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28249L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3264z00_5526); 
FAILURE(BgL_auxz00_10854,BFALSE,BFALSE);} } 
BgL_tmpz00_10850 = BgL_res2302z00_3800; } 
BgL_test4473z00_10849 = 
(BgL_tmpz00_10850==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4473z00_10849)
{ /* Llib/weakhash.scm 710 */
 int BgL_tmpz00_10859;
BgL_tmpz00_10859 = 
(int)(0L); 
BgL_arg1741z00_1911 = 
STRUCT_REF(BgL_tablez00_119, BgL_tmpz00_10859); }  else 
{ /* Llib/weakhash.scm 710 */
BgL_arg1741z00_1911 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } 
{ /* Llib/weakhash.scm 710 */
 long BgL_za71za7_3801;
{ /* Llib/weakhash.scm 710 */
 obj_t BgL_tmpz00_10863;
if(
INTEGERP(BgL_arg1741z00_1911))
{ /* Llib/weakhash.scm 710 */
BgL_tmpz00_10863 = BgL_arg1741z00_1911
; }  else 
{ 
 obj_t BgL_auxz00_10866;
BgL_auxz00_10866 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28271L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1741z00_1911); 
FAILURE(BgL_auxz00_10866,BFALSE,BFALSE);} 
BgL_za71za7_3801 = 
(long)CINT(BgL_tmpz00_10863); } 
BgL_arg1740z00_1910 = 
(BgL_za71za7_3801-1L); } } 
{ /* Llib/weakhash.scm 709 */
 bool_t BgL_test4476z00_10872;
{ /* Llib/weakhash.scm 709 */
 obj_t BgL_tmpz00_10873;
{ /* Llib/weakhash.scm 709 */
 obj_t BgL_res2303z00_3805;
{ /* Llib/weakhash.scm 709 */
 obj_t BgL_aux3267z00_5529;
BgL_aux3267z00_5529 = 
STRUCT_KEY(BgL_tablez00_119); 
if(
SYMBOLP(BgL_aux3267z00_5529))
{ /* Llib/weakhash.scm 709 */
BgL_res2303z00_3805 = BgL_aux3267z00_5529; }  else 
{ 
 obj_t BgL_auxz00_10877;
BgL_auxz00_10877 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28210L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3267z00_5529); 
FAILURE(BgL_auxz00_10877,BFALSE,BFALSE);} } 
BgL_tmpz00_10873 = BgL_res2303z00_3805; } 
BgL_test4476z00_10872 = 
(BgL_tmpz00_10873==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4476z00_10872)
{ /* Llib/weakhash.scm 709 */
 obj_t BgL_auxz00_10884; int BgL_tmpz00_10882;
BgL_auxz00_10884 = 
BINT(BgL_arg1740z00_1910); 
BgL_tmpz00_10882 = 
(int)(0L); 
STRUCT_SET(BgL_tablez00_119, BgL_tmpz00_10882, BgL_auxz00_10884); }  else 
{ /* Llib/weakhash.scm 709 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_119); } } } 
return ((bool_t)1);}  else 
{ 
 obj_t BgL_prevz00_10890; obj_t BgL_bucketz00_10888;
BgL_bucketz00_10888 = 
CDR(BgL_bucketz00_1902); 
BgL_prevz00_10890 = BgL_bucketz00_1902; 
BgL_prevz00_1903 = BgL_prevz00_10890; 
BgL_bucketz00_1902 = BgL_bucketz00_10888; 
goto BgL_zc3z04anonymousza31734ze3z87_1904;} }  else 
{ /* Llib/weakhash.scm 705 */
return ((bool_t)0);} } } } } } } } } } 

}



/* weak-old-hashtable-remove! */
bool_t BGl_weakzd2oldzd2hashtablezd2removez12zc0zz__weakhashz00(obj_t BgL_tablez00_121, obj_t BgL_keyz00_122)
{
{ /* Llib/weakhash.scm 718 */
{ /* Llib/weakhash.scm 719 */
 obj_t BgL_bucketsz00_1919;
{ /* Llib/weakhash.scm 719 */
 bool_t BgL_test4478z00_10891;
{ /* Llib/weakhash.scm 719 */
 obj_t BgL_tmpz00_10892;
{ /* Llib/weakhash.scm 719 */
 obj_t BgL_res2304z00_3810;
{ /* Llib/weakhash.scm 719 */
 obj_t BgL_aux3269z00_5531;
BgL_aux3269z00_5531 = 
STRUCT_KEY(BgL_tablez00_121); 
if(
SYMBOLP(BgL_aux3269z00_5531))
{ /* Llib/weakhash.scm 719 */
BgL_res2304z00_3810 = BgL_aux3269z00_5531; }  else 
{ 
 obj_t BgL_auxz00_10896;
BgL_auxz00_10896 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28633L), BGl_string3589z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3269z00_5531); 
FAILURE(BgL_auxz00_10896,BFALSE,BFALSE);} } 
BgL_tmpz00_10892 = BgL_res2304z00_3810; } 
BgL_test4478z00_10891 = 
(BgL_tmpz00_10892==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4478z00_10891)
{ /* Llib/weakhash.scm 719 */
 int BgL_tmpz00_10901;
BgL_tmpz00_10901 = 
(int)(2L); 
BgL_bucketsz00_1919 = 
STRUCT_REF(BgL_tablez00_121, BgL_tmpz00_10901); }  else 
{ /* Llib/weakhash.scm 719 */
BgL_bucketsz00_1919 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_121); } } 
{ /* Llib/weakhash.scm 719 */
 long BgL_bucketzd2lenzd2_1920;
{ /* Llib/weakhash.scm 720 */
 obj_t BgL_vectorz00_3811;
if(
VECTORP(BgL_bucketsz00_1919))
{ /* Llib/weakhash.scm 720 */
BgL_vectorz00_3811 = BgL_bucketsz00_1919; }  else 
{ 
 obj_t BgL_auxz00_10907;
BgL_auxz00_10907 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28691L), BGl_string3589z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1919); 
FAILURE(BgL_auxz00_10907,BFALSE,BFALSE);} 
BgL_bucketzd2lenzd2_1920 = 
VECTOR_LENGTH(BgL_vectorz00_3811); } 
{ /* Llib/weakhash.scm 720 */
 long BgL_bucketzd2numzd2_1921;
{ /* Llib/weakhash.scm 721 */
 long BgL_arg1752z00_1931;
BgL_arg1752z00_1931 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_121, BgL_keyz00_122); 
{ /* Llib/weakhash.scm 721 */
 long BgL_n1z00_3812; long BgL_n2z00_3813;
BgL_n1z00_3812 = BgL_arg1752z00_1931; 
BgL_n2z00_3813 = BgL_bucketzd2lenzd2_1920; 
{ /* Llib/weakhash.scm 721 */
 bool_t BgL_test4481z00_10913;
{ /* Llib/weakhash.scm 721 */
 long BgL_arg2024z00_3815;
BgL_arg2024z00_3815 = 
(((BgL_n1z00_3812) | (BgL_n2z00_3813)) & -2147483648); 
BgL_test4481z00_10913 = 
(BgL_arg2024z00_3815==0L); } 
if(BgL_test4481z00_10913)
{ /* Llib/weakhash.scm 721 */
 int32_t BgL_arg2020z00_3816;
{ /* Llib/weakhash.scm 721 */
 int32_t BgL_arg2021z00_3817; int32_t BgL_arg2022z00_3818;
BgL_arg2021z00_3817 = 
(int32_t)(BgL_n1z00_3812); 
BgL_arg2022z00_3818 = 
(int32_t)(BgL_n2z00_3813); 
BgL_arg2020z00_3816 = 
(BgL_arg2021z00_3817%BgL_arg2022z00_3818); } 
{ /* Llib/weakhash.scm 721 */
 long BgL_arg2135z00_3823;
BgL_arg2135z00_3823 = 
(long)(BgL_arg2020z00_3816); 
BgL_bucketzd2numzd2_1921 = 
(long)(BgL_arg2135z00_3823); } }  else 
{ /* Llib/weakhash.scm 721 */
BgL_bucketzd2numzd2_1921 = 
(BgL_n1z00_3812%BgL_n2z00_3813); } } } } 
{ /* Llib/weakhash.scm 721 */
 obj_t BgL_bucketz00_1922;
{ /* Llib/weakhash.scm 722 */
 obj_t BgL_vectorz00_3825;
if(
VECTORP(BgL_bucketsz00_1919))
{ /* Llib/weakhash.scm 722 */
BgL_vectorz00_3825 = BgL_bucketsz00_1919; }  else 
{ 
 obj_t BgL_auxz00_10924;
BgL_auxz00_10924 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28798L), BGl_string3589z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_bucketsz00_1919); 
FAILURE(BgL_auxz00_10924,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 722 */
 bool_t BgL_test4483z00_10928;
{ /* Llib/weakhash.scm 722 */
 long BgL_tmpz00_10929;
BgL_tmpz00_10929 = 
VECTOR_LENGTH(BgL_vectorz00_3825); 
BgL_test4483z00_10928 = 
BOUND_CHECK(BgL_bucketzd2numzd2_1921, BgL_tmpz00_10929); } 
if(BgL_test4483z00_10928)
{ /* Llib/weakhash.scm 722 */
BgL_bucketz00_1922 = 
VECTOR_REF(BgL_vectorz00_3825,BgL_bucketzd2numzd2_1921); }  else 
{ 
 obj_t BgL_auxz00_10933;
BgL_auxz00_10933 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28786L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3825, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3825)), 
(int)(BgL_bucketzd2numzd2_1921)); 
FAILURE(BgL_auxz00_10933,BFALSE,BFALSE);} } } 
{ /* Llib/weakhash.scm 722 */
 obj_t BgL_foundz00_1923;
{ /* Llib/weakhash.scm 726 */
 obj_t BgL_zc3z04anonymousza31750ze3z87_4564;
{ 
 int BgL_tmpz00_10940;
BgL_tmpz00_10940 = 
(int)(2L); 
BgL_zc3z04anonymousza31750ze3z87_4564 = 
MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31750ze3ze5zz__weakhashz00, BgL_tmpz00_10940); } 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31750ze3z87_4564, 
(int)(0L), BgL_tablez00_121); 
PROCEDURE_L_SET(BgL_zc3z04anonymousza31750ze3z87_4564, 
(int)(1L), BgL_keyz00_122); 
BgL_foundz00_1923 = 
BGl_traversezd2bucketszd2zz__weakhashz00(BgL_tablez00_121, BgL_bucketsz00_1919, BgL_bucketzd2numzd2_1921, BgL_zc3z04anonymousza31750ze3z87_4564); } 
{ /* Llib/weakhash.scm 723 */

if(
(BgL_foundz00_1923==BGl_keepgoingz00zz__weakhashz00))
{ /* Llib/weakhash.scm 729 */
return ((bool_t)0);}  else 
{ /* Llib/weakhash.scm 729 */
return ((bool_t)1);} } } } } } } } 

}



/* &<@anonymous:1750> */
obj_t BGl_z62zc3z04anonymousza31750ze3ze5zz__weakhashz00(obj_t BgL_envz00_4565, obj_t BgL_bkeyz00_4568, obj_t BgL_valz00_4569, obj_t BgL_bucketz00_4570)
{
{ /* Llib/weakhash.scm 725 */
{ /* Llib/weakhash.scm 726 */
 obj_t BgL_tablez00_4566; obj_t BgL_keyz00_4567;
BgL_tablez00_4566 = 
((obj_t)
PROCEDURE_L_REF(BgL_envz00_4565, 
(int)(0L))); 
BgL_keyz00_4567 = 
PROCEDURE_L_REF(BgL_envz00_4565, 
(int)(1L)); 
{ /* Llib/weakhash.scm 726 */
 bool_t BgL_test4485z00_10955;
{ /* Llib/weakhash.scm 726 */
 obj_t BgL_eqtz00_5788;
{ /* Llib/weakhash.scm 726 */
 bool_t BgL_test4486z00_10956;
{ /* Llib/weakhash.scm 726 */
 obj_t BgL_tmpz00_10957;
{ /* Llib/weakhash.scm 726 */
 obj_t BgL_res2305z00_5789;
{ /* Llib/weakhash.scm 726 */
 obj_t BgL_aux3275z00_5790;
BgL_aux3275z00_5790 = 
STRUCT_KEY(BgL_tablez00_4566); 
if(
SYMBOLP(BgL_aux3275z00_5790))
{ /* Llib/weakhash.scm 726 */
BgL_res2305z00_5789 = BgL_aux3275z00_5790; }  else 
{ 
 obj_t BgL_auxz00_10961;
BgL_auxz00_10961 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(28927L), BGl_string3590z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3275z00_5790); 
FAILURE(BgL_auxz00_10961,BFALSE,BFALSE);} } 
BgL_tmpz00_10957 = BgL_res2305z00_5789; } 
BgL_test4486z00_10956 = 
(BgL_tmpz00_10957==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4486z00_10956)
{ /* Llib/weakhash.scm 726 */
 int BgL_tmpz00_10966;
BgL_tmpz00_10966 = 
(int)(3L); 
BgL_eqtz00_5788 = 
STRUCT_REF(BgL_tablez00_4566, BgL_tmpz00_10966); }  else 
{ /* Llib/weakhash.scm 726 */
BgL_eqtz00_5788 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_4566); } } 
if(
PROCEDUREP(BgL_eqtz00_5788))
{ /* Llib/weakhash.scm 726 */
if(
PROCEDURE_CORRECT_ARITYP(BgL_eqtz00_5788, 2))
{ /* Llib/weakhash.scm 726 */
BgL_test4485z00_10955 = 
CBOOL(
BGL_PROCEDURE_CALL2(BgL_eqtz00_5788, BgL_keyz00_4567, BgL_bkeyz00_4568))
; }  else 
{ /* Llib/weakhash.scm 726 */
FAILURE(BGl_string3591z00zz__weakhashz00,BGl_list3523z00zz__weakhashz00,BgL_eqtz00_5788);} }  else 
{ /* Llib/weakhash.scm 726 */
if(
(BgL_keyz00_4567==BgL_bkeyz00_4568))
{ /* Llib/weakhash.scm 726 */
BgL_test4485z00_10955 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 726 */
if(
STRINGP(BgL_keyz00_4567))
{ /* Llib/weakhash.scm 726 */
if(
STRINGP(BgL_bkeyz00_4568))
{ /* Llib/weakhash.scm 726 */
 long BgL_l1z00_5791;
BgL_l1z00_5791 = 
STRING_LENGTH(BgL_keyz00_4567); 
if(
(BgL_l1z00_5791==
STRING_LENGTH(BgL_bkeyz00_4568)))
{ /* Llib/weakhash.scm 726 */
 int BgL_arg1918z00_5792;
{ /* Llib/weakhash.scm 726 */
 char * BgL_auxz00_10993; char * BgL_tmpz00_10991;
BgL_auxz00_10993 = 
BSTRING_TO_STRING(BgL_bkeyz00_4568); 
BgL_tmpz00_10991 = 
BSTRING_TO_STRING(BgL_keyz00_4567); 
BgL_arg1918z00_5792 = 
memcmp(BgL_tmpz00_10991, BgL_auxz00_10993, BgL_l1z00_5791); } 
BgL_test4485z00_10955 = 
(
(long)(BgL_arg1918z00_5792)==0L); }  else 
{ /* Llib/weakhash.scm 726 */
BgL_test4485z00_10955 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 726 */
BgL_test4485z00_10955 = ((bool_t)0)
; } }  else 
{ /* Llib/weakhash.scm 726 */
BgL_test4485z00_10955 = ((bool_t)0)
; } } } } 
if(BgL_test4485z00_10955)
{ /* Llib/weakhash.scm 726 */
return BGl_removestopz00zz__weakhashz00;}  else 
{ /* Llib/weakhash.scm 726 */
return BGl_keepgoingz00zz__weakhashz00;} } } } 

}



/* weak-hashtable-remove! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(obj_t BgL_tablez00_123, obj_t BgL_keyz00_124)
{
{ /* Llib/weakhash.scm 734 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_123))
{ /* Llib/weakhash.scm 735 */
return 
BBOOL(
BGl_weakzd2keyszd2hashtablezd2removez12zc0zz__weakhashz00(BgL_tablez00_123, BgL_keyz00_124));}  else 
{ /* Llib/weakhash.scm 735 */
return 
BBOOL(
BGl_weakzd2oldzd2hashtablezd2removez12zc0zz__weakhashz00(BgL_tablez00_123, BgL_keyz00_124));} } 

}



/* &weak-hashtable-remove! */
obj_t BGl_z62weakzd2hashtablezd2removez12z70zz__weakhashz00(obj_t BgL_envz00_4571, obj_t BgL_tablez00_4572, obj_t BgL_keyz00_4573)
{
{ /* Llib/weakhash.scm 734 */
{ /* Llib/weakhash.scm 735 */
 obj_t BgL_auxz00_11004;
if(
STRUCTP(BgL_tablez00_4572))
{ /* Llib/weakhash.scm 735 */
BgL_auxz00_11004 = BgL_tablez00_4572
; }  else 
{ 
 obj_t BgL_auxz00_11007;
BgL_auxz00_11007 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(29319L), BGl_string3592z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4572); 
FAILURE(BgL_auxz00_11007,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2removez12z12zz__weakhashz00(BgL_auxz00_11004, BgL_keyz00_4573);} } 

}



/* weak-keys-hashtable-expand! */
obj_t BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t BgL_tablez00_125)
{
{ /* Llib/weakhash.scm 742 */
BGl_gcz00zz__biglooz00(BTRUE); 
{ /* Llib/weakhash.scm 746 */
 obj_t BgL_oldzd2buckszd2_1934;
{ /* Llib/weakhash.scm 746 */
 bool_t BgL_test4496z00_11013;
{ /* Llib/weakhash.scm 746 */
 obj_t BgL_tmpz00_11014;
{ /* Llib/weakhash.scm 746 */
 obj_t BgL_res2306z00_3851;
{ /* Llib/weakhash.scm 746 */
 obj_t BgL_aux3280z00_5543;
BgL_aux3280z00_5543 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3280z00_5543))
{ /* Llib/weakhash.scm 746 */
BgL_res2306z00_3851 = BgL_aux3280z00_5543; }  else 
{ 
 obj_t BgL_auxz00_11018;
BgL_auxz00_11018 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(29786L), BGl_string3593z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3280z00_5543); 
FAILURE(BgL_auxz00_11018,BFALSE,BFALSE);} } 
BgL_tmpz00_11014 = BgL_res2306z00_3851; } 
BgL_test4496z00_11013 = 
(BgL_tmpz00_11014==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4496z00_11013)
{ /* Llib/weakhash.scm 746 */
 int BgL_tmpz00_11023;
BgL_tmpz00_11023 = 
(int)(2L); 
BgL_oldzd2buckszd2_1934 = 
STRUCT_REF(BgL_tablez00_125, BgL_tmpz00_11023); }  else 
{ /* Llib/weakhash.scm 746 */
BgL_oldzd2buckszd2_1934 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } 
{ /* Llib/weakhash.scm 746 */
 long BgL_lenz00_1935;
{ /* Llib/weakhash.scm 747 */
 obj_t BgL_vectorz00_3852;
if(
VECTORP(BgL_oldzd2buckszd2_1934))
{ /* Llib/weakhash.scm 747 */
BgL_vectorz00_3852 = BgL_oldzd2buckszd2_1934; }  else 
{ 
 obj_t BgL_auxz00_11029;
BgL_auxz00_11029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(29837L), BGl_string3593z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_1934); 
FAILURE(BgL_auxz00_11029,BFALSE,BFALSE);} 
BgL_lenz00_1935 = 
VECTOR_LENGTH(BgL_vectorz00_3852); } 
{ /* Llib/weakhash.scm 747 */
 obj_t BgL_maxzd2bucketzd2lenz00_1936;
{ /* Llib/weakhash.scm 748 */
 bool_t BgL_test4499z00_11034;
{ /* Llib/weakhash.scm 748 */
 obj_t BgL_tmpz00_11035;
{ /* Llib/weakhash.scm 748 */
 obj_t BgL_res2307z00_3856;
{ /* Llib/weakhash.scm 748 */
 obj_t BgL_aux3284z00_5547;
BgL_aux3284z00_5547 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3284z00_5547))
{ /* Llib/weakhash.scm 748 */
BgL_res2307z00_3856 = BgL_aux3284z00_5547; }  else 
{ 
 obj_t BgL_auxz00_11039;
BgL_auxz00_11039 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(29868L), BGl_string3593z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3284z00_5547); 
FAILURE(BgL_auxz00_11039,BFALSE,BFALSE);} } 
BgL_tmpz00_11035 = BgL_res2307z00_3856; } 
BgL_test4499z00_11034 = 
(BgL_tmpz00_11035==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4499z00_11034)
{ /* Llib/weakhash.scm 748 */
 int BgL_tmpz00_11044;
BgL_tmpz00_11044 = 
(int)(1L); 
BgL_maxzd2bucketzd2lenz00_1936 = 
STRUCT_REF(BgL_tablez00_125, BgL_tmpz00_11044); }  else 
{ /* Llib/weakhash.scm 748 */
BgL_maxzd2bucketzd2lenz00_1936 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } 
{ /* Llib/weakhash.scm 748 */

{ 
 long BgL_iz00_1938; long BgL_siza7eza7_1939; bool_t BgL_expandz00_1940;
BgL_iz00_1938 = 0L; 
BgL_siza7eza7_1939 = 0L; 
BgL_expandz00_1940 = ((bool_t)0); 
BgL_zc3z04anonymousza31754ze3z87_1941:
if(
(BgL_iz00_1938<BgL_lenz00_1935))
{ /* Llib/weakhash.scm 754 */
 long BgL_countz00_1943;
BgL_countz00_1943 = 0L; 
{ /* Llib/weakhash.scm 756 */
 obj_t BgL_arg1756z00_1944;
{ /* Llib/weakhash.scm 756 */
 obj_t BgL_hook1119z00_1945;
BgL_hook1119z00_1945 = 
MAKE_YOUNG_PAIR(BFALSE, BNIL); 
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_g1120z00_1946;
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_vectorz00_3859;
if(
VECTORP(BgL_oldzd2buckszd2_1934))
{ /* Llib/weakhash.scm 761 */
BgL_vectorz00_3859 = BgL_oldzd2buckszd2_1934; }  else 
{ 
 obj_t BgL_auxz00_11053;
BgL_auxz00_11053 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30213L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_1934); 
FAILURE(BgL_auxz00_11053,BFALSE,BFALSE);} 
BgL_g1120z00_1946 = 
VECTOR_REF(BgL_vectorz00_3859,BgL_iz00_1938); } 
{ 
 obj_t BgL_l1116z00_1948; obj_t BgL_h1117z00_1949;
BgL_l1116z00_1948 = BgL_g1120z00_1946; 
BgL_h1117z00_1949 = BgL_hook1119z00_1945; 
BgL_zc3z04anonymousza31757ze3z87_1950:
if(
NULLP(BgL_l1116z00_1948))
{ /* Llib/weakhash.scm 761 */
BgL_arg1756z00_1944 = 
CDR(BgL_hook1119z00_1945); }  else 
{ /* Llib/weakhash.scm 761 */
 bool_t BgL_test4504z00_11061;
{ /* Llib/weakhash.scm 757 */
 obj_t BgL_cellz00_1959;
{ /* Llib/weakhash.scm 757 */
 obj_t BgL_pairz00_3862;
if(
PAIRP(BgL_l1116z00_1948))
{ /* Llib/weakhash.scm 757 */
BgL_pairz00_3862 = BgL_l1116z00_1948; }  else 
{ 
 obj_t BgL_auxz00_11064;
BgL_auxz00_11064 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30071L), BGl_string3594z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_l1116z00_1948); 
FAILURE(BgL_auxz00_11064,BFALSE,BFALSE);} 
BgL_cellz00_1959 = 
CAR(BgL_pairz00_3862); } 
{ /* Llib/weakhash.scm 757 */
 obj_t BgL_keyz00_1960;
{ /* Llib/weakhash.scm 757 */
 obj_t BgL_objz00_3863;
if(
BGL_WEAKPTRP(BgL_cellz00_1959))
{ /* Llib/weakhash.scm 757 */
BgL_objz00_3863 = BgL_cellz00_1959; }  else 
{ 
 obj_t BgL_auxz00_11071;
BgL_auxz00_11071 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30096L), BGl_string3594z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_cellz00_1959); 
FAILURE(BgL_auxz00_11071,BFALSE,BFALSE);} 
BgL_keyz00_1960 = 
bgl_weakptr_data(BgL_objz00_3863); } 
if(
(BgL_keyz00_1960==BUNSPEC))
{ /* Llib/weakhash.scm 758 */
BgL_test4504z00_11061 = ((bool_t)0)
; }  else 
{ /* Llib/weakhash.scm 758 */
BgL_countz00_1943 = 
(BgL_countz00_1943+1L); 
BgL_test4504z00_11061 = 
CBOOL(BgL_keyz00_1960); } } } 
if(BgL_test4504z00_11061)
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_nh1118z00_1955;
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_arg1761z00_1957;
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_pairz00_3865;
if(
PAIRP(BgL_l1116z00_1948))
{ /* Llib/weakhash.scm 761 */
BgL_pairz00_3865 = BgL_l1116z00_1948; }  else 
{ 
 obj_t BgL_auxz00_11082;
BgL_auxz00_11082 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30198L), BGl_string3594z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_l1116z00_1948); 
FAILURE(BgL_auxz00_11082,BFALSE,BFALSE);} 
BgL_arg1761z00_1957 = 
CAR(BgL_pairz00_3865); } 
BgL_nh1118z00_1955 = 
MAKE_YOUNG_PAIR(BgL_arg1761z00_1957, BNIL); } 
SET_CDR(BgL_h1117z00_1949, BgL_nh1118z00_1955); 
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_arg1760z00_1956;
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_pairz00_3867;
if(
PAIRP(BgL_l1116z00_1948))
{ /* Llib/weakhash.scm 761 */
BgL_pairz00_3867 = BgL_l1116z00_1948; }  else 
{ 
 obj_t BgL_auxz00_11091;
BgL_auxz00_11091 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30198L), BGl_string3594z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_l1116z00_1948); 
FAILURE(BgL_auxz00_11091,BFALSE,BFALSE);} 
BgL_arg1760z00_1956 = 
CDR(BgL_pairz00_3867); } 
{ 
 obj_t BgL_h1117z00_11097; obj_t BgL_l1116z00_11096;
BgL_l1116z00_11096 = BgL_arg1760z00_1956; 
BgL_h1117z00_11097 = BgL_nh1118z00_1955; 
BgL_h1117z00_1949 = BgL_h1117z00_11097; 
BgL_l1116z00_1948 = BgL_l1116z00_11096; 
goto BgL_zc3z04anonymousza31757ze3z87_1950;} } }  else 
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_arg1762z00_1958;
{ /* Llib/weakhash.scm 761 */
 obj_t BgL_pairz00_3868;
if(
PAIRP(BgL_l1116z00_1948))
{ /* Llib/weakhash.scm 761 */
BgL_pairz00_3868 = BgL_l1116z00_1948; }  else 
{ 
 obj_t BgL_auxz00_11100;
BgL_auxz00_11100 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30198L), BGl_string3594z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_l1116z00_1948); 
FAILURE(BgL_auxz00_11100,BFALSE,BFALSE);} 
BgL_arg1762z00_1958 = 
CDR(BgL_pairz00_3868); } 
{ 
 obj_t BgL_l1116z00_11105;
BgL_l1116z00_11105 = BgL_arg1762z00_1958; 
BgL_l1116z00_1948 = BgL_l1116z00_11105; 
goto BgL_zc3z04anonymousza31757ze3z87_1950;} } } } } } 
{ /* Llib/weakhash.scm 755 */
 obj_t BgL_vectorz00_3869;
if(
VECTORP(BgL_oldzd2buckszd2_1934))
{ /* Llib/weakhash.scm 755 */
BgL_vectorz00_3869 = BgL_oldzd2buckszd2_1934; }  else 
{ 
 obj_t BgL_auxz00_11108;
BgL_auxz00_11108 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30022L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_1934); 
FAILURE(BgL_auxz00_11108,BFALSE,BFALSE);} 
VECTOR_SET(BgL_vectorz00_3869,BgL_iz00_1938,BgL_arg1756z00_1944); } } 
{ /* Llib/weakhash.scm 762 */
 long BgL_arg1763z00_1962; long BgL_arg1764z00_1963; bool_t BgL_arg1765z00_1964;
BgL_arg1763z00_1962 = 
(BgL_iz00_1938+1L); 
BgL_arg1764z00_1963 = 
(BgL_siza7eza7_1939+BgL_countz00_1943); 
if(BgL_expandz00_1940)
{ /* Llib/weakhash.scm 763 */
BgL_arg1765z00_1964 = BgL_expandz00_1940; }  else 
{ /* Llib/weakhash.scm 763 */
 long BgL_n1z00_3874; long BgL_n2z00_3875;
BgL_n1z00_3874 = BgL_countz00_1943; 
{ /* Llib/weakhash.scm 763 */
 obj_t BgL_tmpz00_11116;
if(
INTEGERP(BgL_maxzd2bucketzd2lenz00_1936))
{ /* Llib/weakhash.scm 763 */
BgL_tmpz00_11116 = BgL_maxzd2bucketzd2lenz00_1936
; }  else 
{ 
 obj_t BgL_auxz00_11119;
BgL_auxz00_11119 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30290L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2bucketzd2lenz00_1936); 
FAILURE(BgL_auxz00_11119,BFALSE,BFALSE);} 
BgL_n2z00_3875 = 
(long)CINT(BgL_tmpz00_11116); } 
BgL_arg1765z00_1964 = 
(BgL_n1z00_3874>BgL_n2z00_3875); } 
{ 
 bool_t BgL_expandz00_11127; long BgL_siza7eza7_11126; long BgL_iz00_11125;
BgL_iz00_11125 = BgL_arg1763z00_1962; 
BgL_siza7eza7_11126 = BgL_arg1764z00_1963; 
BgL_expandz00_11127 = BgL_arg1765z00_1964; 
BgL_expandz00_1940 = BgL_expandz00_11127; 
BgL_siza7eza7_1939 = BgL_siza7eza7_11126; 
BgL_iz00_1938 = BgL_iz00_11125; 
goto BgL_zc3z04anonymousza31754ze3z87_1941;} } }  else 
{ /* Llib/weakhash.scm 753 */
if(BgL_expandz00_1940)
{ /* Llib/weakhash.scm 765 */
 long BgL_newzd2lenzd2_1966;
BgL_newzd2lenzd2_1966 = 
(2L*BgL_lenz00_1935); 
{ /* Llib/weakhash.scm 765 */
 obj_t BgL_maxzd2lenzd2_1967;
{ /* Llib/weakhash.scm 766 */
 bool_t BgL_test4515z00_11130;
{ /* Llib/weakhash.scm 766 */
 obj_t BgL_tmpz00_11131;
{ /* Llib/weakhash.scm 766 */
 obj_t BgL_res2308z00_3880;
{ /* Llib/weakhash.scm 766 */
 obj_t BgL_aux3301z00_5564;
BgL_aux3301z00_5564 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3301z00_5564))
{ /* Llib/weakhash.scm 766 */
BgL_res2308z00_3880 = BgL_aux3301z00_5564; }  else 
{ 
 obj_t BgL_auxz00_11135;
BgL_auxz00_11135 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30373L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3301z00_5564); 
FAILURE(BgL_auxz00_11135,BFALSE,BFALSE);} } 
BgL_tmpz00_11131 = BgL_res2308z00_3880; } 
BgL_test4515z00_11130 = 
(BgL_tmpz00_11131==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4515z00_11130)
{ /* Llib/weakhash.scm 766 */
 int BgL_tmpz00_11140;
BgL_tmpz00_11140 = 
(int)(6L); 
BgL_maxzd2lenzd2_1967 = 
STRUCT_REF(BgL_tablez00_125, BgL_tmpz00_11140); }  else 
{ /* Llib/weakhash.scm 766 */
BgL_maxzd2lenzd2_1967 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } 
{ /* Llib/weakhash.scm 766 */

{ /* Llib/weakhash.scm 768 */
 obj_t BgL_nmaxz00_1968;
{ /* Llib/weakhash.scm 768 */
 obj_t BgL_a1121z00_1971;
{ /* Llib/weakhash.scm 768 */
 bool_t BgL_test4517z00_11144;
{ /* Llib/weakhash.scm 768 */
 obj_t BgL_tmpz00_11145;
{ /* Llib/weakhash.scm 768 */
 obj_t BgL_res2309z00_3884;
{ /* Llib/weakhash.scm 768 */
 obj_t BgL_aux3303z00_5566;
BgL_aux3303z00_5566 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3303z00_5566))
{ /* Llib/weakhash.scm 768 */
BgL_res2309z00_3884 = BgL_aux3303z00_5566; }  else 
{ 
 obj_t BgL_auxz00_11149;
BgL_auxz00_11149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30454L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3303z00_5566); 
FAILURE(BgL_auxz00_11149,BFALSE,BFALSE);} } 
BgL_tmpz00_11145 = BgL_res2309z00_3884; } 
BgL_test4517z00_11144 = 
(BgL_tmpz00_11145==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4517z00_11144)
{ /* Llib/weakhash.scm 768 */
 int BgL_tmpz00_11154;
BgL_tmpz00_11154 = 
(int)(1L); 
BgL_a1121z00_1971 = 
STRUCT_REF(BgL_tablez00_125, BgL_tmpz00_11154); }  else 
{ /* Llib/weakhash.scm 768 */
BgL_a1121z00_1971 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } 
{ /* Llib/weakhash.scm 769 */
 obj_t BgL_b1122z00_1972;
{ /* Llib/weakhash.scm 769 */
 bool_t BgL_test4519z00_11158;
{ /* Llib/weakhash.scm 769 */
 obj_t BgL_tmpz00_11159;
{ /* Llib/weakhash.scm 769 */
 obj_t BgL_res2310z00_3888;
{ /* Llib/weakhash.scm 769 */
 obj_t BgL_aux3305z00_5568;
BgL_aux3305z00_5568 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3305z00_5568))
{ /* Llib/weakhash.scm 769 */
BgL_res2310z00_3888 = BgL_aux3305z00_5568; }  else 
{ 
 obj_t BgL_auxz00_11163;
BgL_auxz00_11163 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30498L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3305z00_5568); 
FAILURE(BgL_auxz00_11163,BFALSE,BFALSE);} } 
BgL_tmpz00_11159 = BgL_res2310z00_3888; } 
BgL_test4519z00_11158 = 
(BgL_tmpz00_11159==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4519z00_11158)
{ /* Llib/weakhash.scm 769 */
 int BgL_tmpz00_11168;
BgL_tmpz00_11168 = 
(int)(7L); 
BgL_b1122z00_1972 = 
STRUCT_REF(BgL_tablez00_125, BgL_tmpz00_11168); }  else 
{ /* Llib/weakhash.scm 769 */
BgL_b1122z00_1972 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } 
{ /* Llib/weakhash.scm 768 */

{ /* Llib/weakhash.scm 768 */
 bool_t BgL_test4521z00_11172;
if(
INTEGERP(BgL_a1121z00_1971))
{ /* Llib/weakhash.scm 768 */
BgL_test4521z00_11172 = 
INTEGERP(BgL_b1122z00_1972)
; }  else 
{ /* Llib/weakhash.scm 768 */
BgL_test4521z00_11172 = ((bool_t)0)
; } 
if(BgL_test4521z00_11172)
{ /* Llib/weakhash.scm 768 */
BgL_nmaxz00_1968 = 
BINT(
(
(long)CINT(BgL_a1121z00_1971)*
(long)CINT(BgL_b1122z00_1972))); }  else 
{ /* Llib/weakhash.scm 768 */
BgL_nmaxz00_1968 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1121z00_1971, BgL_b1122z00_1972); } } } } } 
{ /* Llib/weakhash.scm 771 */
 obj_t BgL_arg1766z00_1969;
if(
REALP(BgL_nmaxz00_1968))
{ /* Llib/weakhash.scm 771 */
BgL_arg1766z00_1969 = 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_nmaxz00_1968))); }  else 
{ /* Llib/weakhash.scm 771 */
BgL_arg1766z00_1969 = BgL_nmaxz00_1968; } 
{ /* Llib/weakhash.scm 770 */
 bool_t BgL_test4524z00_11186;
{ /* Llib/weakhash.scm 770 */
 obj_t BgL_tmpz00_11187;
{ /* Llib/weakhash.scm 770 */
 obj_t BgL_res2311z00_3895;
{ /* Llib/weakhash.scm 770 */
 obj_t BgL_aux3307z00_5570;
BgL_aux3307z00_5570 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3307z00_5570))
{ /* Llib/weakhash.scm 770 */
BgL_res2311z00_3895 = BgL_aux3307z00_5570; }  else 
{ 
 obj_t BgL_auxz00_11191;
BgL_auxz00_11191 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30542L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3307z00_5570); 
FAILURE(BgL_auxz00_11191,BFALSE,BFALSE);} } 
BgL_tmpz00_11187 = BgL_res2311z00_3895; } 
BgL_test4524z00_11186 = 
(BgL_tmpz00_11187==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4524z00_11186)
{ /* Llib/weakhash.scm 770 */
 int BgL_tmpz00_11196;
BgL_tmpz00_11196 = 
(int)(1L); 
STRUCT_SET(BgL_tablez00_125, BgL_tmpz00_11196, BgL_arg1766z00_1969); }  else 
{ /* Llib/weakhash.scm 770 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } } } 
{ /* Llib/weakhash.scm 773 */
 bool_t BgL_test4526z00_11200;
{ /* Llib/weakhash.scm 773 */
 bool_t BgL_test4527z00_11201;
{ /* Llib/weakhash.scm 773 */
 long BgL_n1z00_3896;
{ /* Llib/weakhash.scm 773 */
 obj_t BgL_tmpz00_11202;
if(
INTEGERP(BgL_maxzd2lenzd2_1967))
{ /* Llib/weakhash.scm 773 */
BgL_tmpz00_11202 = BgL_maxzd2lenzd2_1967
; }  else 
{ 
 obj_t BgL_auxz00_11205;
BgL_auxz00_11205 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30682L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2lenzd2_1967); 
FAILURE(BgL_auxz00_11205,BFALSE,BFALSE);} 
BgL_n1z00_3896 = 
(long)CINT(BgL_tmpz00_11202); } 
BgL_test4527z00_11201 = 
(BgL_n1z00_3896<0L); } 
if(BgL_test4527z00_11201)
{ /* Llib/weakhash.scm 773 */
BgL_test4526z00_11200 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 773 */
 long BgL_n2z00_3898;
{ /* Llib/weakhash.scm 773 */
 obj_t BgL_tmpz00_11211;
if(
INTEGERP(BgL_maxzd2lenzd2_1967))
{ /* Llib/weakhash.scm 773 */
BgL_tmpz00_11211 = BgL_maxzd2lenzd2_1967
; }  else 
{ 
 obj_t BgL_auxz00_11214;
BgL_auxz00_11214 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30707L), BGl_string3453z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_maxzd2lenzd2_1967); 
FAILURE(BgL_auxz00_11214,BFALSE,BFALSE);} 
BgL_n2z00_3898 = 
(long)CINT(BgL_tmpz00_11211); } 
BgL_test4526z00_11200 = 
(BgL_newzd2lenzd2_1966<=BgL_n2z00_3898); } } 
if(BgL_test4526z00_11200)
{ /* Llib/weakhash.scm 774 */
 obj_t BgL_newzd2buckszd2_1976;
BgL_newzd2buckszd2_1976 = 
make_vector(BgL_newzd2lenzd2_1966, BNIL); 
{ /* Llib/weakhash.scm 775 */
 bool_t BgL_test4530z00_11221;
{ /* Llib/weakhash.scm 775 */
 obj_t BgL_tmpz00_11222;
{ /* Llib/weakhash.scm 775 */
 obj_t BgL_res2312z00_3902;
{ /* Llib/weakhash.scm 775 */
 obj_t BgL_aux3311z00_5574;
BgL_aux3311z00_5574 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3311z00_5574))
{ /* Llib/weakhash.scm 775 */
BgL_res2312z00_3902 = BgL_aux3311z00_5574; }  else 
{ 
 obj_t BgL_auxz00_11226;
BgL_auxz00_11226 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30777L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3311z00_5574); 
FAILURE(BgL_auxz00_11226,BFALSE,BFALSE);} } 
BgL_tmpz00_11222 = BgL_res2312z00_3902; } 
BgL_test4530z00_11221 = 
(BgL_tmpz00_11222==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4530z00_11221)
{ /* Llib/weakhash.scm 775 */
 int BgL_tmpz00_11231;
BgL_tmpz00_11231 = 
(int)(2L); 
STRUCT_SET(BgL_tablez00_125, BgL_tmpz00_11231, BgL_newzd2buckszd2_1976); }  else 
{ /* Llib/weakhash.scm 775 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125); } } 
{ 
 long BgL_iz00_1978;
BgL_iz00_1978 = 0L; 
BgL_zc3z04anonymousza31771ze3z87_1979:
if(
(BgL_iz00_1978<BgL_lenz00_1935))
{ /* Llib/weakhash.scm 778 */
{ /* Llib/weakhash.scm 779 */
 obj_t BgL_g1125z00_1981;
{ /* Llib/weakhash.scm 786 */
 obj_t BgL_vectorz00_3905;
if(
VECTORP(BgL_oldzd2buckszd2_1934))
{ /* Llib/weakhash.scm 786 */
BgL_vectorz00_3905 = BgL_oldzd2buckszd2_1934; }  else 
{ 
 obj_t BgL_auxz00_11239;
BgL_auxz00_11239 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31164L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_1934); 
FAILURE(BgL_auxz00_11239,BFALSE,BFALSE);} 
BgL_g1125z00_1981 = 
VECTOR_REF(BgL_vectorz00_3905,BgL_iz00_1978); } 
{ 
 obj_t BgL_l1123z00_1983;
BgL_l1123z00_1983 = BgL_g1125z00_1981; 
BgL_zc3z04anonymousza31773ze3z87_1984:
if(
PAIRP(BgL_l1123z00_1983))
{ /* Llib/weakhash.scm 786 */
{ /* Llib/weakhash.scm 780 */
 obj_t BgL_cellz00_1986;
BgL_cellz00_1986 = 
CAR(BgL_l1123z00_1983); 
{ /* Llib/weakhash.scm 780 */
 obj_t BgL_keyz00_1987;
{ /* Llib/weakhash.scm 780 */
 obj_t BgL_objz00_3908;
if(
BGL_WEAKPTRP(BgL_cellz00_1986))
{ /* Llib/weakhash.scm 780 */
BgL_objz00_3908 = BgL_cellz00_1986; }  else 
{ 
 obj_t BgL_auxz00_11249;
BgL_auxz00_11249 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(30945L), BGl_string3595z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_cellz00_1986); 
FAILURE(BgL_auxz00_11249,BFALSE,BFALSE);} 
BgL_keyz00_1987 = 
bgl_weakptr_data(BgL_objz00_3908); } 
{ /* Llib/weakhash.scm 781 */
 long BgL_nz00_1988;
BgL_nz00_1988 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_125, BgL_keyz00_1987); 
{ /* Llib/weakhash.scm 781 */
 long BgL_hz00_1989;
{ /* Llib/weakhash.scm 783 */
 long BgL_n1z00_3909; long BgL_n2z00_3910;
BgL_n1z00_3909 = BgL_nz00_1988; 
BgL_n2z00_3910 = BgL_newzd2lenzd2_1966; 
{ /* Llib/weakhash.scm 783 */
 bool_t BgL_test4536z00_11255;
{ /* Llib/weakhash.scm 783 */
 long BgL_arg2024z00_3912;
BgL_arg2024z00_3912 = 
(((BgL_n1z00_3909) | (BgL_n2z00_3910)) & -2147483648); 
BgL_test4536z00_11255 = 
(BgL_arg2024z00_3912==0L); } 
if(BgL_test4536z00_11255)
{ /* Llib/weakhash.scm 783 */
 int32_t BgL_arg2020z00_3913;
{ /* Llib/weakhash.scm 783 */
 int32_t BgL_arg2021z00_3914; int32_t BgL_arg2022z00_3915;
BgL_arg2021z00_3914 = 
(int32_t)(BgL_n1z00_3909); 
BgL_arg2022z00_3915 = 
(int32_t)(BgL_n2z00_3910); 
BgL_arg2020z00_3913 = 
(BgL_arg2021z00_3914%BgL_arg2022z00_3915); } 
{ /* Llib/weakhash.scm 783 */
 long BgL_arg2135z00_3920;
BgL_arg2135z00_3920 = 
(long)(BgL_arg2020z00_3913); 
BgL_hz00_1989 = 
(long)(BgL_arg2135z00_3920); } }  else 
{ /* Llib/weakhash.scm 783 */
BgL_hz00_1989 = 
(BgL_n1z00_3909%BgL_n2z00_3910); } } } 
{ /* Llib/weakhash.scm 783 */

{ /* Llib/weakhash.scm 785 */
 obj_t BgL_arg1775z00_1990;
{ /* Llib/weakhash.scm 785 */
 obj_t BgL_arg1777z00_1991;
{ /* Llib/weakhash.scm 785 */
 bool_t BgL_test4537z00_11264;
{ /* Llib/weakhash.scm 785 */
 long BgL_tmpz00_11265;
BgL_tmpz00_11265 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_1976); 
BgL_test4537z00_11264 = 
BOUND_CHECK(BgL_hz00_1989, BgL_tmpz00_11265); } 
if(BgL_test4537z00_11264)
{ /* Llib/weakhash.scm 785 */
BgL_arg1777z00_1991 = 
VECTOR_REF(BgL_newzd2buckszd2_1976,BgL_hz00_1989); }  else 
{ 
 obj_t BgL_auxz00_11269;
BgL_auxz00_11269 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31114L), BGl_string3445z00zz__weakhashz00, BgL_newzd2buckszd2_1976, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_1976)), 
(int)(BgL_hz00_1989)); 
FAILURE(BgL_auxz00_11269,BFALSE,BFALSE);} } 
BgL_arg1775z00_1990 = 
MAKE_YOUNG_PAIR(BgL_cellz00_1986, BgL_arg1777z00_1991); } 
VECTOR_SET(BgL_newzd2buckszd2_1976,BgL_hz00_1989,BgL_arg1775z00_1990); } } } } } } 
{ 
 obj_t BgL_l1123z00_11278;
BgL_l1123z00_11278 = 
CDR(BgL_l1123z00_1983); 
BgL_l1123z00_1983 = BgL_l1123z00_11278; 
goto BgL_zc3z04anonymousza31773ze3z87_1984;} }  else 
{ /* Llib/weakhash.scm 786 */
if(
NULLP(BgL_l1123z00_1983))
{ /* Llib/weakhash.scm 786 */BTRUE; }  else 
{ /* Llib/weakhash.scm 786 */
BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(BGl_string3464z00zz__weakhashz00, BGl_string3465z00zz__weakhashz00, BgL_l1123z00_1983, BGl_string3442z00zz__weakhashz00, 
BINT(30887L)); } } } } 
{ 
 long BgL_iz00_11284;
BgL_iz00_11284 = 
(BgL_iz00_1978+1L); 
BgL_iz00_1978 = BgL_iz00_11284; 
goto BgL_zc3z04anonymousza31771ze3z87_1979;} }  else 
{ /* Llib/weakhash.scm 789 */
 bool_t BgL_test4539z00_11286;
{ /* Llib/weakhash.scm 789 */
 obj_t BgL_tmpz00_11287;
{ /* Llib/weakhash.scm 789 */
 obj_t BgL_res2313z00_3931;
{ /* Llib/weakhash.scm 789 */
 obj_t BgL_aux3317z00_5580;
BgL_aux3317z00_5580 = 
STRUCT_KEY(BgL_tablez00_125); 
if(
SYMBOLP(BgL_aux3317z00_5580))
{ /* Llib/weakhash.scm 789 */
BgL_res2313z00_3931 = BgL_aux3317z00_5580; }  else 
{ 
 obj_t BgL_auxz00_11291;
BgL_auxz00_11291 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31228L), BGl_string3453z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3317z00_5580); 
FAILURE(BgL_auxz00_11291,BFALSE,BFALSE);} } 
BgL_tmpz00_11287 = BgL_res2313z00_3931; } 
BgL_test4539z00_11286 = 
(BgL_tmpz00_11287==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4539z00_11286)
{ /* Llib/weakhash.scm 789 */
 obj_t BgL_auxz00_11298; int BgL_tmpz00_11296;
BgL_auxz00_11298 = 
BINT(BgL_siza7eza7_1939); 
BgL_tmpz00_11296 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_125, BgL_tmpz00_11296, BgL_auxz00_11298);}  else 
{ /* Llib/weakhash.scm 789 */
return 
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_125);} } } }  else 
{ /* Llib/weakhash.scm 793 */
 obj_t BgL_arg1782z00_1997;
{ /* Llib/weakhash.scm 793 */
 long BgL_arg1783z00_1998;
BgL_arg1783z00_1998 = 
BGl_hashtablezd2siza7ez75zz__hashz00(BgL_tablez00_125); 
{ /* Llib/weakhash.scm 791 */
 obj_t BgL_list1784z00_1999;
{ /* Llib/weakhash.scm 791 */
 obj_t BgL_arg1785z00_2000;
{ /* Llib/weakhash.scm 791 */
 obj_t BgL_arg1786z00_2001;
BgL_arg1786z00_2001 = 
MAKE_YOUNG_PAIR(
BINT(BgL_arg1783z00_1998), BNIL); 
BgL_arg1785z00_2000 = 
MAKE_YOUNG_PAIR(BgL_maxzd2lenzd2_1967, BgL_arg1786z00_2001); } 
BgL_list1784z00_1999 = 
MAKE_YOUNG_PAIR(
BINT(BgL_newzd2lenzd2_1966), BgL_arg1785z00_2000); } 
BgL_arg1782z00_1997 = 
BGl_formatz00zz__r4_output_6_10_3z00(BGl_string3596z00zz__weakhashz00, BgL_list1784z00_1999); } } 
return 
BGl_errorz00zz__errorz00(BGl_string3597z00zz__weakhashz00, BgL_arg1782z00_1997, BgL_tablez00_125);} } } } }  else 
{ /* Llib/weakhash.scm 764 */
return BFALSE;} } } } } } } } 

}



/* weak-old-hashtable-expand! */
obj_t BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(obj_t BgL_tablez00_126)
{
{ /* Llib/weakhash.scm 799 */
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4541z00_11310;
{ /* Llib/weakhash.scm 816 */
 obj_t BgL_arg1856z00_2143;
{ /* Llib/weakhash.scm 816 */
 bool_t BgL_test4542z00_11311;
{ /* Llib/weakhash.scm 816 */
 obj_t BgL_tmpz00_11312;
{ /* Llib/weakhash.scm 816 */
 obj_t BgL_res2314z00_3935;
{ /* Llib/weakhash.scm 816 */
 obj_t BgL_aux3319z00_5582;
BgL_aux3319z00_5582 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3319z00_5582))
{ /* Llib/weakhash.scm 816 */
BgL_res2314z00_3935 = BgL_aux3319z00_5582; }  else 
{ 
 obj_t BgL_auxz00_11316;
BgL_auxz00_11316 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32344L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3319z00_5582); 
FAILURE(BgL_auxz00_11316,BFALSE,BFALSE);} } 
BgL_tmpz00_11312 = BgL_res2314z00_3935; } 
BgL_test4542z00_11311 = 
(BgL_tmpz00_11312==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4542z00_11311)
{ /* Llib/weakhash.scm 816 */
 int BgL_tmpz00_11321;
BgL_tmpz00_11321 = 
(int)(5L); 
BgL_arg1856z00_2143 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11321); }  else 
{ /* Llib/weakhash.scm 816 */
BgL_arg1856z00_2143 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 816 */
 long BgL_n2z00_3936;
{ /* Llib/weakhash.scm 816 */
 obj_t BgL_tmpz00_11325;
if(
INTEGERP(BgL_arg1856z00_2143))
{ /* Llib/weakhash.scm 816 */
BgL_tmpz00_11325 = BgL_arg1856z00_2143
; }  else 
{ 
 obj_t BgL_auxz00_11328;
BgL_auxz00_11328 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32366L), BGl_string3598z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1856z00_2143); 
FAILURE(BgL_auxz00_11328,BFALSE,BFALSE);} 
BgL_n2z00_3936 = 
(long)CINT(BgL_tmpz00_11325); } 
BgL_test4541z00_11310 = 
(1L==BgL_n2z00_3936); } } 
if(BgL_test4541z00_11310)
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_oldzd2buckszd2_2006;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4545z00_11334;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11335;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2315z00_3940;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3322z00_5585;
BgL_aux3322z00_5585 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3322z00_5585))
{ /* Llib/weakhash.scm 800 */
BgL_res2315z00_3940 = BgL_aux3322z00_5585; }  else 
{ 
 obj_t BgL_auxz00_11339;
BgL_auxz00_11339 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3322z00_5585); 
FAILURE(BgL_auxz00_11339,BFALSE,BFALSE);} } 
BgL_tmpz00_11335 = BgL_res2315z00_3940; } 
BgL_test4545z00_11334 = 
(BgL_tmpz00_11335==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4545z00_11334)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11344;
BgL_tmpz00_11344 = 
(int)(2L); 
BgL_oldzd2buckszd2_2006 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11344); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_oldzd2buckszd2_2006 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 long BgL_oldzd2buckszd2lenz00_2007;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_3941;
if(
VECTORP(BgL_oldzd2buckszd2_2006))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_3941 = BgL_oldzd2buckszd2_2006; }  else 
{ 
 obj_t BgL_auxz00_11350;
BgL_auxz00_11350 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2006); 
FAILURE(BgL_auxz00_11350,BFALSE,BFALSE);} 
BgL_oldzd2buckszd2lenz00_2007 = 
VECTOR_LENGTH(BgL_vectorz00_3941); } 
{ /* Llib/weakhash.scm 800 */
 long BgL_newzd2buckszd2lenz00_2008;
BgL_newzd2buckszd2lenz00_2008 = 
(2L*BgL_oldzd2buckszd2lenz00_2007); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_newzd2buckszd2_2009;
BgL_newzd2buckszd2_2009 = 
make_vector(BgL_newzd2buckszd2lenz00_2008, BNIL); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_countz00_2010;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4548z00_11357;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11358;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2316z00_3946;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3326z00_5589;
BgL_aux3326z00_5589 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3326z00_5589))
{ /* Llib/weakhash.scm 800 */
BgL_res2316z00_3946 = BgL_aux3326z00_5589; }  else 
{ 
 obj_t BgL_auxz00_11362;
BgL_auxz00_11362 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3326z00_5589); 
FAILURE(BgL_auxz00_11362,BFALSE,BFALSE);} } 
BgL_tmpz00_11358 = BgL_res2316z00_3946; } 
BgL_test4548z00_11357 = 
(BgL_tmpz00_11358==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4548z00_11357)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11367;
BgL_tmpz00_11367 = 
(int)(0L); 
BgL_countz00_2010 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11367); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_countz00_2010 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 obj_t BgL_nmaxz00_2011;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_a1126z00_2014;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4550z00_11371;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11372;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2317z00_3950;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3328z00_5591;
BgL_aux3328z00_5591 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3328z00_5591))
{ /* Llib/weakhash.scm 800 */
BgL_res2317z00_3950 = BgL_aux3328z00_5591; }  else 
{ 
 obj_t BgL_auxz00_11376;
BgL_auxz00_11376 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3328z00_5591); 
FAILURE(BgL_auxz00_11376,BFALSE,BFALSE);} } 
BgL_tmpz00_11372 = BgL_res2317z00_3950; } 
BgL_test4550z00_11371 = 
(BgL_tmpz00_11372==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4550z00_11371)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11381;
BgL_tmpz00_11381 = 
(int)(1L); 
BgL_a1126z00_2014 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11381); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_a1126z00_2014 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_b1127z00_2015;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4552z00_11385;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11386;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2318z00_3954;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3330z00_5593;
BgL_aux3330z00_5593 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3330z00_5593))
{ /* Llib/weakhash.scm 800 */
BgL_res2318z00_3954 = BgL_aux3330z00_5593; }  else 
{ 
 obj_t BgL_auxz00_11390;
BgL_auxz00_11390 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3330z00_5593); 
FAILURE(BgL_auxz00_11390,BFALSE,BFALSE);} } 
BgL_tmpz00_11386 = BgL_res2318z00_3954; } 
BgL_test4552z00_11385 = 
(BgL_tmpz00_11386==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4552z00_11385)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11395;
BgL_tmpz00_11395 = 
(int)(7L); 
BgL_b1127z00_2015 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11395); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_b1127z00_2015 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4554z00_11399;
if(
INTEGERP(BgL_a1126z00_2014))
{ /* Llib/weakhash.scm 800 */
BgL_test4554z00_11399 = 
INTEGERP(BgL_b1127z00_2015)
; }  else 
{ /* Llib/weakhash.scm 800 */
BgL_test4554z00_11399 = ((bool_t)0)
; } 
if(BgL_test4554z00_11399)
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2011 = 
BINT(
(
(long)CINT(BgL_a1126z00_2014)*
(long)CINT(BgL_b1127z00_2015))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2011 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1126z00_2014, BgL_b1127z00_2015); } } } } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_arg1789z00_2012;
if(
REALP(BgL_nmaxz00_2011))
{ /* Llib/weakhash.scm 800 */
BgL_arg1789z00_2012 = 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_nmaxz00_2011))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_arg1789z00_2012 = BgL_nmaxz00_2011; } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4557z00_11413;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11414;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2319z00_3961;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3332z00_5595;
BgL_aux3332z00_5595 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3332z00_5595))
{ /* Llib/weakhash.scm 800 */
BgL_res2319z00_3961 = BgL_aux3332z00_5595; }  else 
{ 
 obj_t BgL_auxz00_11418;
BgL_auxz00_11418 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3332z00_5595); 
FAILURE(BgL_auxz00_11418,BFALSE,BFALSE);} } 
BgL_tmpz00_11414 = BgL_res2319z00_3961; } 
BgL_test4557z00_11413 = 
(BgL_tmpz00_11414==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4557z00_11413)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11423;
BgL_tmpz00_11423 = 
(int)(1L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11423, BgL_arg1789z00_2012); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } } } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4559z00_11427;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11428;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2320z00_3965;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3334z00_5597;
BgL_aux3334z00_5597 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3334z00_5597))
{ /* Llib/weakhash.scm 800 */
BgL_res2320z00_3965 = BgL_aux3334z00_5597; }  else 
{ 
 obj_t BgL_auxz00_11432;
BgL_auxz00_11432 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3334z00_5597); 
FAILURE(BgL_auxz00_11432,BFALSE,BFALSE);} } 
BgL_tmpz00_11428 = BgL_res2320z00_3965; } 
BgL_test4559z00_11427 = 
(BgL_tmpz00_11428==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4559z00_11427)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11437;
BgL_tmpz00_11437 = 
(int)(2L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11437, BgL_newzd2buckszd2_2009); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ 
 long BgL_iz00_2018;
BgL_iz00_2018 = 0L; 
BgL_zc3z04anonymousza31792ze3z87_2019:
if(
(BgL_iz00_2018<BgL_oldzd2buckszd2lenz00_2007))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_g1130z00_2021;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_3968;
if(
VECTORP(BgL_oldzd2buckszd2_2006))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_3968 = BgL_oldzd2buckszd2_2006; }  else 
{ 
 obj_t BgL_auxz00_11445;
BgL_auxz00_11445 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2006); 
FAILURE(BgL_auxz00_11445,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4564z00_11449;
{ /* Llib/weakhash.scm 800 */
 long BgL_tmpz00_11450;
BgL_tmpz00_11450 = 
VECTOR_LENGTH(BgL_vectorz00_3968); 
BgL_test4564z00_11449 = 
BOUND_CHECK(BgL_iz00_2018, BgL_tmpz00_11450); } 
if(BgL_test4564z00_11449)
{ /* Llib/weakhash.scm 800 */
BgL_g1130z00_2021 = 
VECTOR_REF(BgL_vectorz00_3968,BgL_iz00_2018); }  else 
{ 
 obj_t BgL_auxz00_11454;
BgL_auxz00_11454 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_3968, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_3968)), 
(int)(BgL_iz00_2018)); 
FAILURE(BgL_auxz00_11454,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_l1128z00_2023;
BgL_l1128z00_2023 = BgL_g1130z00_2021; 
BgL_zc3z04anonymousza31794ze3z87_2024:
if(
PAIRP(BgL_l1128z00_2023))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 818 */
 obj_t BgL_cellz00_2026;
BgL_cellz00_2026 = 
CAR(BgL_l1128z00_2023); 
{ /* Llib/weakhash.scm 818 */
 obj_t BgL_keyz00_2027;
{ /* Llib/weakhash.scm 818 */
 obj_t BgL_arg1799z00_2032;
{ /* Llib/weakhash.scm 818 */
 obj_t BgL_pairz00_3971;
if(
PAIRP(BgL_cellz00_2026))
{ /* Llib/weakhash.scm 818 */
BgL_pairz00_3971 = BgL_cellz00_2026; }  else 
{ 
 obj_t BgL_auxz00_11466;
BgL_auxz00_11466 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32431L), BGl_string3599z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_cellz00_2026); 
FAILURE(BgL_auxz00_11466,BFALSE,BFALSE);} 
BgL_arg1799z00_2032 = 
CAR(BgL_pairz00_3971); } 
{ /* Llib/weakhash.scm 818 */
 obj_t BgL_objz00_3972;
if(
BGL_WEAKPTRP(BgL_arg1799z00_2032))
{ /* Llib/weakhash.scm 818 */
BgL_objz00_3972 = BgL_arg1799z00_2032; }  else 
{ 
 obj_t BgL_auxz00_11473;
BgL_auxz00_11473 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32435L), BGl_string3599z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1799z00_2032); 
FAILURE(BgL_auxz00_11473,BFALSE,BFALSE);} 
BgL_keyz00_2027 = 
bgl_weakptr_data(BgL_objz00_3972); } } 
if(
(BgL_keyz00_2027==BUNSPEC))
{ 
 obj_t BgL_tmpz00_11480;
{ /* Llib/weakhash.scm 820 */
 obj_t BgL_aux3342z00_5605;
BgL_aux3342z00_5605 = BgL_countz00_2010; 
if(
INTEGERP(BgL_aux3342z00_5605))
{ /* Llib/weakhash.scm 820 */
BgL_tmpz00_11480 = BgL_aux3342z00_5605
; }  else 
{ 
 obj_t BgL_auxz00_11483;
BgL_auxz00_11483 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32501L), BGl_string3599z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3342z00_5605); 
FAILURE(BgL_auxz00_11483,BFALSE,BFALSE);} } 
BgL_countz00_2010 = 
SUBFX(BgL_tmpz00_11480, 
BINT(1L)); }  else 
{ /* Llib/weakhash.scm 821 */
 long BgL_hz00_2028;
{ /* Llib/weakhash.scm 822 */
 long BgL_arg1798z00_2031;
BgL_arg1798z00_2031 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_126, BgL_keyz00_2027); 
{ /* Llib/weakhash.scm 821 */
 long BgL_n1z00_3974; long BgL_n2z00_3975;
BgL_n1z00_3974 = BgL_arg1798z00_2031; 
BgL_n2z00_3975 = BgL_newzd2buckszd2lenz00_2008; 
{ /* Llib/weakhash.scm 821 */
 bool_t BgL_test4570z00_11490;
{ /* Llib/weakhash.scm 821 */
 long BgL_arg2024z00_3977;
BgL_arg2024z00_3977 = 
(((BgL_n1z00_3974) | (BgL_n2z00_3975)) & -2147483648); 
BgL_test4570z00_11490 = 
(BgL_arg2024z00_3977==0L); } 
if(BgL_test4570z00_11490)
{ /* Llib/weakhash.scm 821 */
 int32_t BgL_arg2020z00_3978;
{ /* Llib/weakhash.scm 821 */
 int32_t BgL_arg2021z00_3979; int32_t BgL_arg2022z00_3980;
BgL_arg2021z00_3979 = 
(int32_t)(BgL_n1z00_3974); 
BgL_arg2022z00_3980 = 
(int32_t)(BgL_n2z00_3975); 
BgL_arg2020z00_3978 = 
(BgL_arg2021z00_3979%BgL_arg2022z00_3980); } 
{ /* Llib/weakhash.scm 821 */
 long BgL_arg2135z00_3985;
BgL_arg2135z00_3985 = 
(long)(BgL_arg2020z00_3978); 
BgL_hz00_2028 = 
(long)(BgL_arg2135z00_3985); } }  else 
{ /* Llib/weakhash.scm 821 */
BgL_hz00_2028 = 
(BgL_n1z00_3974%BgL_n2z00_3975); } } } } 
{ /* Llib/weakhash.scm 827 */
 obj_t BgL_arg1796z00_2029;
{ /* Llib/weakhash.scm 827 */
 obj_t BgL_arg1797z00_2030;
{ /* Llib/weakhash.scm 827 */
 bool_t BgL_test4571z00_11499;
{ /* Llib/weakhash.scm 827 */
 long BgL_tmpz00_11500;
BgL_tmpz00_11500 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2009); 
BgL_test4571z00_11499 = 
BOUND_CHECK(BgL_hz00_2028, BgL_tmpz00_11500); } 
if(BgL_test4571z00_11499)
{ /* Llib/weakhash.scm 827 */
BgL_arg1797z00_2030 = 
VECTOR_REF(BgL_newzd2buckszd2_2009,BgL_hz00_2028); }  else 
{ 
 obj_t BgL_auxz00_11504;
BgL_auxz00_11504 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32682L), BGl_string3445z00zz__weakhashz00, BgL_newzd2buckszd2_2009, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2009)), 
(int)(BgL_hz00_2028)); 
FAILURE(BgL_auxz00_11504,BFALSE,BFALSE);} } 
BgL_arg1796z00_2029 = 
MAKE_YOUNG_PAIR(BgL_cellz00_2026, BgL_arg1797z00_2030); } 
{ /* Llib/weakhash.scm 824 */
 bool_t BgL_test4572z00_11512;
{ /* Llib/weakhash.scm 824 */
 long BgL_tmpz00_11513;
BgL_tmpz00_11513 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2009); 
BgL_test4572z00_11512 = 
BOUND_CHECK(BgL_hz00_2028, BgL_tmpz00_11513); } 
if(BgL_test4572z00_11512)
{ /* Llib/weakhash.scm 824 */
VECTOR_SET(BgL_newzd2buckszd2_2009,BgL_hz00_2028,BgL_arg1796z00_2029); }  else 
{ 
 obj_t BgL_auxz00_11517;
BgL_auxz00_11517 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32620L), BGl_string3450z00zz__weakhashz00, BgL_newzd2buckszd2_2009, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2009)), 
(int)(BgL_hz00_2028)); 
FAILURE(BgL_auxz00_11517,BFALSE,BFALSE);} } } } } } 
{ 
 obj_t BgL_l1128z00_11524;
BgL_l1128z00_11524 = 
CDR(BgL_l1128z00_2023); 
BgL_l1128z00_2023 = BgL_l1128z00_11524; 
goto BgL_zc3z04anonymousza31794ze3z87_2024;} }  else 
{ /* Llib/weakhash.scm 800 */
if(
NULLP(BgL_l1128z00_2023))
{ /* Llib/weakhash.scm 800 */BTRUE; }  else 
{ /* Llib/weakhash.scm 800 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3464z00zz__weakhashz00, BGl_string3465z00zz__weakhashz00, BgL_l1128z00_2023); } } } } 
{ 
 long BgL_iz00_11529;
BgL_iz00_11529 = 
(BgL_iz00_2018+1L); 
BgL_iz00_2018 = BgL_iz00_11529; 
goto BgL_zc3z04anonymousza31792ze3z87_2019;} }  else 
{ /* Llib/weakhash.scm 800 */((bool_t)0); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vz00_3993;
BgL_vz00_3993 = BgL_countz00_2010; 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4574z00_11531;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11532;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2321z00_3997;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3343z00_5606;
BgL_aux3343z00_5606 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3343z00_5606))
{ /* Llib/weakhash.scm 800 */
BgL_res2321z00_3997 = BgL_aux3343z00_5606; }  else 
{ 
 obj_t BgL_auxz00_11536;
BgL_auxz00_11536 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3343z00_5606); 
FAILURE(BgL_auxz00_11536,BFALSE,BFALSE);} } 
BgL_tmpz00_11532 = BgL_res2321z00_3997; } 
BgL_test4574z00_11531 = 
(BgL_tmpz00_11532==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4574z00_11531)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11541;
BgL_tmpz00_11541 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11541, BgL_vz00_3993);}  else 
{ /* Llib/weakhash.scm 800 */
return 
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126);} } } } } } } } }  else 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4576z00_11545;
{ /* Llib/weakhash.scm 829 */
 obj_t BgL_arg1854z00_2142;
{ /* Llib/weakhash.scm 829 */
 bool_t BgL_test4577z00_11546;
{ /* Llib/weakhash.scm 829 */
 obj_t BgL_tmpz00_11547;
{ /* Llib/weakhash.scm 829 */
 obj_t BgL_res2322z00_4001;
{ /* Llib/weakhash.scm 829 */
 obj_t BgL_aux3345z00_5608;
BgL_aux3345z00_5608 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3345z00_5608))
{ /* Llib/weakhash.scm 829 */
BgL_res2322z00_4001 = BgL_aux3345z00_5608; }  else 
{ 
 obj_t BgL_auxz00_11551;
BgL_auxz00_11551 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32750L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3345z00_5608); 
FAILURE(BgL_auxz00_11551,BFALSE,BFALSE);} } 
BgL_tmpz00_11547 = BgL_res2322z00_4001; } 
BgL_test4577z00_11546 = 
(BgL_tmpz00_11547==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4577z00_11546)
{ /* Llib/weakhash.scm 829 */
 int BgL_tmpz00_11556;
BgL_tmpz00_11556 = 
(int)(5L); 
BgL_arg1854z00_2142 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11556); }  else 
{ /* Llib/weakhash.scm 829 */
BgL_arg1854z00_2142 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 829 */
 long BgL_n2z00_4002;
{ /* Llib/weakhash.scm 829 */
 obj_t BgL_tmpz00_11560;
if(
INTEGERP(BgL_arg1854z00_2142))
{ /* Llib/weakhash.scm 829 */
BgL_tmpz00_11560 = BgL_arg1854z00_2142
; }  else 
{ 
 obj_t BgL_auxz00_11563;
BgL_auxz00_11563 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32772L), BGl_string3598z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1854z00_2142); 
FAILURE(BgL_auxz00_11563,BFALSE,BFALSE);} 
BgL_n2z00_4002 = 
(long)CINT(BgL_tmpz00_11560); } 
BgL_test4576z00_11545 = 
(2L==BgL_n2z00_4002); } } 
if(BgL_test4576z00_11545)
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_oldzd2buckszd2_2040;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4580z00_11569;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11570;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2323z00_4006;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3348z00_5611;
BgL_aux3348z00_5611 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3348z00_5611))
{ /* Llib/weakhash.scm 800 */
BgL_res2323z00_4006 = BgL_aux3348z00_5611; }  else 
{ 
 obj_t BgL_auxz00_11574;
BgL_auxz00_11574 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3348z00_5611); 
FAILURE(BgL_auxz00_11574,BFALSE,BFALSE);} } 
BgL_tmpz00_11570 = BgL_res2323z00_4006; } 
BgL_test4580z00_11569 = 
(BgL_tmpz00_11570==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4580z00_11569)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11579;
BgL_tmpz00_11579 = 
(int)(2L); 
BgL_oldzd2buckszd2_2040 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11579); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_oldzd2buckszd2_2040 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 long BgL_oldzd2buckszd2lenz00_2041;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_4007;
if(
VECTORP(BgL_oldzd2buckszd2_2040))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_4007 = BgL_oldzd2buckszd2_2040; }  else 
{ 
 obj_t BgL_auxz00_11585;
BgL_auxz00_11585 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2040); 
FAILURE(BgL_auxz00_11585,BFALSE,BFALSE);} 
BgL_oldzd2buckszd2lenz00_2041 = 
VECTOR_LENGTH(BgL_vectorz00_4007); } 
{ /* Llib/weakhash.scm 800 */
 long BgL_newzd2buckszd2lenz00_2042;
BgL_newzd2buckszd2lenz00_2042 = 
(2L*BgL_oldzd2buckszd2lenz00_2041); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_newzd2buckszd2_2043;
BgL_newzd2buckszd2_2043 = 
make_vector(BgL_newzd2buckszd2lenz00_2042, BNIL); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_countz00_2044;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4583z00_11592;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11593;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2324z00_4012;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3352z00_5615;
BgL_aux3352z00_5615 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3352z00_5615))
{ /* Llib/weakhash.scm 800 */
BgL_res2324z00_4012 = BgL_aux3352z00_5615; }  else 
{ 
 obj_t BgL_auxz00_11597;
BgL_auxz00_11597 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3352z00_5615); 
FAILURE(BgL_auxz00_11597,BFALSE,BFALSE);} } 
BgL_tmpz00_11593 = BgL_res2324z00_4012; } 
BgL_test4583z00_11592 = 
(BgL_tmpz00_11593==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4583z00_11592)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11602;
BgL_tmpz00_11602 = 
(int)(0L); 
BgL_countz00_2044 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11602); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_countz00_2044 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 obj_t BgL_nmaxz00_2045;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_a1131z00_2048;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4585z00_11606;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11607;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2325z00_4016;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3354z00_5617;
BgL_aux3354z00_5617 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3354z00_5617))
{ /* Llib/weakhash.scm 800 */
BgL_res2325z00_4016 = BgL_aux3354z00_5617; }  else 
{ 
 obj_t BgL_auxz00_11611;
BgL_auxz00_11611 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3354z00_5617); 
FAILURE(BgL_auxz00_11611,BFALSE,BFALSE);} } 
BgL_tmpz00_11607 = BgL_res2325z00_4016; } 
BgL_test4585z00_11606 = 
(BgL_tmpz00_11607==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4585z00_11606)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11616;
BgL_tmpz00_11616 = 
(int)(1L); 
BgL_a1131z00_2048 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11616); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_a1131z00_2048 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_b1132z00_2049;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4587z00_11620;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11621;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2326z00_4020;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3356z00_5619;
BgL_aux3356z00_5619 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3356z00_5619))
{ /* Llib/weakhash.scm 800 */
BgL_res2326z00_4020 = BgL_aux3356z00_5619; }  else 
{ 
 obj_t BgL_auxz00_11625;
BgL_auxz00_11625 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3356z00_5619); 
FAILURE(BgL_auxz00_11625,BFALSE,BFALSE);} } 
BgL_tmpz00_11621 = BgL_res2326z00_4020; } 
BgL_test4587z00_11620 = 
(BgL_tmpz00_11621==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4587z00_11620)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11630;
BgL_tmpz00_11630 = 
(int)(7L); 
BgL_b1132z00_2049 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11630); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_b1132z00_2049 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4589z00_11634;
if(
INTEGERP(BgL_a1131z00_2048))
{ /* Llib/weakhash.scm 800 */
BgL_test4589z00_11634 = 
INTEGERP(BgL_b1132z00_2049)
; }  else 
{ /* Llib/weakhash.scm 800 */
BgL_test4589z00_11634 = ((bool_t)0)
; } 
if(BgL_test4589z00_11634)
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2045 = 
BINT(
(
(long)CINT(BgL_a1131z00_2048)*
(long)CINT(BgL_b1132z00_2049))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2045 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1131z00_2048, BgL_b1132z00_2049); } } } } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_arg1805z00_2046;
if(
REALP(BgL_nmaxz00_2045))
{ /* Llib/weakhash.scm 800 */
BgL_arg1805z00_2046 = 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_nmaxz00_2045))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_arg1805z00_2046 = BgL_nmaxz00_2045; } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4592z00_11648;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11649;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2327z00_4027;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3358z00_5621;
BgL_aux3358z00_5621 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3358z00_5621))
{ /* Llib/weakhash.scm 800 */
BgL_res2327z00_4027 = BgL_aux3358z00_5621; }  else 
{ 
 obj_t BgL_auxz00_11653;
BgL_auxz00_11653 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3358z00_5621); 
FAILURE(BgL_auxz00_11653,BFALSE,BFALSE);} } 
BgL_tmpz00_11649 = BgL_res2327z00_4027; } 
BgL_test4592z00_11648 = 
(BgL_tmpz00_11649==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4592z00_11648)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11658;
BgL_tmpz00_11658 = 
(int)(1L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11658, BgL_arg1805z00_2046); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } } } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4594z00_11662;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11663;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2328z00_4031;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3360z00_5623;
BgL_aux3360z00_5623 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3360z00_5623))
{ /* Llib/weakhash.scm 800 */
BgL_res2328z00_4031 = BgL_aux3360z00_5623; }  else 
{ 
 obj_t BgL_auxz00_11667;
BgL_auxz00_11667 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3360z00_5623); 
FAILURE(BgL_auxz00_11667,BFALSE,BFALSE);} } 
BgL_tmpz00_11663 = BgL_res2328z00_4031; } 
BgL_test4594z00_11662 = 
(BgL_tmpz00_11663==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4594z00_11662)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11672;
BgL_tmpz00_11672 = 
(int)(2L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11672, BgL_newzd2buckszd2_2043); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ 
 long BgL_iz00_2052;
BgL_iz00_2052 = 0L; 
BgL_zc3z04anonymousza31808ze3z87_2053:
if(
(BgL_iz00_2052<BgL_oldzd2buckszd2lenz00_2041))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_g1135z00_2055;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_4034;
if(
VECTORP(BgL_oldzd2buckszd2_2040))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_4034 = BgL_oldzd2buckszd2_2040; }  else 
{ 
 obj_t BgL_auxz00_11680;
BgL_auxz00_11680 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2040); 
FAILURE(BgL_auxz00_11680,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4598z00_11684;
{ /* Llib/weakhash.scm 800 */
 long BgL_tmpz00_11685;
BgL_tmpz00_11685 = 
VECTOR_LENGTH(BgL_vectorz00_4034); 
BgL_test4598z00_11684 = 
BOUND_CHECK(BgL_iz00_2052, BgL_tmpz00_11685); } 
if(BgL_test4598z00_11684)
{ /* Llib/weakhash.scm 800 */
BgL_g1135z00_2055 = 
VECTOR_REF(BgL_vectorz00_4034,BgL_iz00_2052); }  else 
{ 
 obj_t BgL_auxz00_11689;
BgL_auxz00_11689 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_4034, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4034)), 
(int)(BgL_iz00_2052)); 
FAILURE(BgL_auxz00_11689,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_l1133z00_2057;
BgL_l1133z00_2057 = BgL_g1135z00_2055; 
BgL_zc3z04anonymousza31810ze3z87_2058:
if(
PAIRP(BgL_l1133z00_2057))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 831 */
 obj_t BgL_cellz00_2060;
BgL_cellz00_2060 = 
CAR(BgL_l1133z00_2057); 
{ /* Llib/weakhash.scm 831 */
 obj_t BgL_dataz00_2061;
{ /* Llib/weakhash.scm 831 */
 obj_t BgL_arg1816z00_2067;
{ /* Llib/weakhash.scm 831 */
 obj_t BgL_pairz00_4037;
if(
PAIRP(BgL_cellz00_2060))
{ /* Llib/weakhash.scm 831 */
BgL_pairz00_4037 = BgL_cellz00_2060; }  else 
{ 
 obj_t BgL_auxz00_11701;
BgL_auxz00_11701 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32837L), BGl_string3600z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_cellz00_2060); 
FAILURE(BgL_auxz00_11701,BFALSE,BFALSE);} 
BgL_arg1816z00_2067 = 
CDR(BgL_pairz00_4037); } 
{ /* Llib/weakhash.scm 831 */
 obj_t BgL_objz00_4038;
if(
BGL_WEAKPTRP(BgL_arg1816z00_2067))
{ /* Llib/weakhash.scm 831 */
BgL_objz00_4038 = BgL_arg1816z00_2067; }  else 
{ 
 obj_t BgL_auxz00_11708;
BgL_auxz00_11708 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32841L), BGl_string3600z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1816z00_2067); 
FAILURE(BgL_auxz00_11708,BFALSE,BFALSE);} 
BgL_dataz00_2061 = 
bgl_weakptr_data(BgL_objz00_4038); } } 
if(
(BgL_dataz00_2061==BUNSPEC))
{ 
 obj_t BgL_tmpz00_11715;
{ /* Llib/weakhash.scm 833 */
 obj_t BgL_aux3368z00_5631;
BgL_aux3368z00_5631 = BgL_countz00_2044; 
if(
INTEGERP(BgL_aux3368z00_5631))
{ /* Llib/weakhash.scm 833 */
BgL_tmpz00_11715 = BgL_aux3368z00_5631
; }  else 
{ 
 obj_t BgL_auxz00_11718;
BgL_auxz00_11718 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(32908L), BGl_string3600z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3368z00_5631); 
FAILURE(BgL_auxz00_11718,BFALSE,BFALSE);} } 
BgL_countz00_2044 = 
SUBFX(BgL_tmpz00_11715, 
BINT(1L)); }  else 
{ /* Llib/weakhash.scm 834 */
 long BgL_hz00_2062;
{ /* Llib/weakhash.scm 836 */
 long BgL_arg1814z00_2065;
{ /* Llib/weakhash.scm 836 */
 obj_t BgL_arg1815z00_2066;
{ /* Llib/weakhash.scm 836 */
 obj_t BgL_pairz00_4040;
if(
PAIRP(BgL_cellz00_2060))
{ /* Llib/weakhash.scm 836 */
BgL_pairz00_4040 = BgL_cellz00_2060; }  else 
{ 
 obj_t BgL_auxz00_11726;
BgL_auxz00_11726 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33001L), BGl_string3600z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_cellz00_2060); 
FAILURE(BgL_auxz00_11726,BFALSE,BFALSE);} 
BgL_arg1815z00_2066 = 
CAR(BgL_pairz00_4040); } 
BgL_arg1814z00_2065 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_126, BgL_arg1815z00_2066); } 
{ /* Llib/weakhash.scm 834 */
 long BgL_n1z00_4041; long BgL_n2z00_4042;
BgL_n1z00_4041 = BgL_arg1814z00_2065; 
BgL_n2z00_4042 = BgL_newzd2buckszd2lenz00_2042; 
{ /* Llib/weakhash.scm 834 */
 bool_t BgL_test4605z00_11732;
{ /* Llib/weakhash.scm 834 */
 long BgL_arg2024z00_4044;
BgL_arg2024z00_4044 = 
(((BgL_n1z00_4041) | (BgL_n2z00_4042)) & -2147483648); 
BgL_test4605z00_11732 = 
(BgL_arg2024z00_4044==0L); } 
if(BgL_test4605z00_11732)
{ /* Llib/weakhash.scm 834 */
 int32_t BgL_arg2020z00_4045;
{ /* Llib/weakhash.scm 834 */
 int32_t BgL_arg2021z00_4046; int32_t BgL_arg2022z00_4047;
BgL_arg2021z00_4046 = 
(int32_t)(BgL_n1z00_4041); 
BgL_arg2022z00_4047 = 
(int32_t)(BgL_n2z00_4042); 
BgL_arg2020z00_4045 = 
(BgL_arg2021z00_4046%BgL_arg2022z00_4047); } 
{ /* Llib/weakhash.scm 834 */
 long BgL_arg2135z00_4052;
BgL_arg2135z00_4052 = 
(long)(BgL_arg2020z00_4045); 
BgL_hz00_2062 = 
(long)(BgL_arg2135z00_4052); } }  else 
{ /* Llib/weakhash.scm 834 */
BgL_hz00_2062 = 
(BgL_n1z00_4041%BgL_n2z00_4042); } } } } 
{ /* Llib/weakhash.scm 841 */
 obj_t BgL_arg1812z00_2063;
{ /* Llib/weakhash.scm 841 */
 obj_t BgL_arg1813z00_2064;
{ /* Llib/weakhash.scm 841 */
 bool_t BgL_test4606z00_11741;
{ /* Llib/weakhash.scm 841 */
 long BgL_tmpz00_11742;
BgL_tmpz00_11742 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2043); 
BgL_test4606z00_11741 = 
BOUND_CHECK(BgL_hz00_2062, BgL_tmpz00_11742); } 
if(BgL_test4606z00_11741)
{ /* Llib/weakhash.scm 841 */
BgL_arg1813z00_2064 = 
VECTOR_REF(BgL_newzd2buckszd2_2043,BgL_hz00_2062); }  else 
{ 
 obj_t BgL_auxz00_11746;
BgL_auxz00_11746 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33107L), BGl_string3445z00zz__weakhashz00, BgL_newzd2buckszd2_2043, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2043)), 
(int)(BgL_hz00_2062)); 
FAILURE(BgL_auxz00_11746,BFALSE,BFALSE);} } 
BgL_arg1812z00_2063 = 
MAKE_YOUNG_PAIR(BgL_cellz00_2060, BgL_arg1813z00_2064); } 
{ /* Llib/weakhash.scm 838 */
 bool_t BgL_test4607z00_11754;
{ /* Llib/weakhash.scm 838 */
 long BgL_tmpz00_11755;
BgL_tmpz00_11755 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2043); 
BgL_test4607z00_11754 = 
BOUND_CHECK(BgL_hz00_2062, BgL_tmpz00_11755); } 
if(BgL_test4607z00_11754)
{ /* Llib/weakhash.scm 838 */
VECTOR_SET(BgL_newzd2buckszd2_2043,BgL_hz00_2062,BgL_arg1812z00_2063); }  else 
{ 
 obj_t BgL_auxz00_11759;
BgL_auxz00_11759 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33045L), BGl_string3450z00zz__weakhashz00, BgL_newzd2buckszd2_2043, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2043)), 
(int)(BgL_hz00_2062)); 
FAILURE(BgL_auxz00_11759,BFALSE,BFALSE);} } } } } } 
{ 
 obj_t BgL_l1133z00_11766;
BgL_l1133z00_11766 = 
CDR(BgL_l1133z00_2057); 
BgL_l1133z00_2057 = BgL_l1133z00_11766; 
goto BgL_zc3z04anonymousza31810ze3z87_2058;} }  else 
{ /* Llib/weakhash.scm 800 */
if(
NULLP(BgL_l1133z00_2057))
{ /* Llib/weakhash.scm 800 */BTRUE; }  else 
{ /* Llib/weakhash.scm 800 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3464z00zz__weakhashz00, BGl_string3465z00zz__weakhashz00, BgL_l1133z00_2057); } } } } 
{ 
 long BgL_iz00_11771;
BgL_iz00_11771 = 
(BgL_iz00_2052+1L); 
BgL_iz00_2052 = BgL_iz00_11771; 
goto BgL_zc3z04anonymousza31808ze3z87_2053;} }  else 
{ /* Llib/weakhash.scm 800 */((bool_t)0); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vz00_4060;
BgL_vz00_4060 = BgL_countz00_2044; 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4609z00_11773;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11774;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2329z00_4064;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3371z00_5634;
BgL_aux3371z00_5634 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3371z00_5634))
{ /* Llib/weakhash.scm 800 */
BgL_res2329z00_4064 = BgL_aux3371z00_5634; }  else 
{ 
 obj_t BgL_auxz00_11778;
BgL_auxz00_11778 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3371z00_5634); 
FAILURE(BgL_auxz00_11778,BFALSE,BFALSE);} } 
BgL_tmpz00_11774 = BgL_res2329z00_4064; } 
BgL_test4609z00_11773 = 
(BgL_tmpz00_11774==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4609z00_11773)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11783;
BgL_tmpz00_11783 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11783, BgL_vz00_4060);}  else 
{ /* Llib/weakhash.scm 800 */
return 
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126);} } } } } } } } }  else 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4611z00_11787;
{ /* Llib/weakhash.scm 843 */
 obj_t BgL_arg1853z00_2141;
{ /* Llib/weakhash.scm 843 */
 bool_t BgL_test4612z00_11788;
{ /* Llib/weakhash.scm 843 */
 obj_t BgL_tmpz00_11789;
{ /* Llib/weakhash.scm 843 */
 obj_t BgL_res2330z00_4068;
{ /* Llib/weakhash.scm 843 */
 obj_t BgL_aux3373z00_5636;
BgL_aux3373z00_5636 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3373z00_5636))
{ /* Llib/weakhash.scm 843 */
BgL_res2330z00_4068 = BgL_aux3373z00_5636; }  else 
{ 
 obj_t BgL_auxz00_11793;
BgL_auxz00_11793 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33175L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3373z00_5636); 
FAILURE(BgL_auxz00_11793,BFALSE,BFALSE);} } 
BgL_tmpz00_11789 = BgL_res2330z00_4068; } 
BgL_test4612z00_11788 = 
(BgL_tmpz00_11789==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4612z00_11788)
{ /* Llib/weakhash.scm 843 */
 int BgL_tmpz00_11798;
BgL_tmpz00_11798 = 
(int)(5L); 
BgL_arg1853z00_2141 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11798); }  else 
{ /* Llib/weakhash.scm 843 */
BgL_arg1853z00_2141 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 843 */
 long BgL_n2z00_4069;
{ /* Llib/weakhash.scm 843 */
 obj_t BgL_tmpz00_11802;
if(
INTEGERP(BgL_arg1853z00_2141))
{ /* Llib/weakhash.scm 843 */
BgL_tmpz00_11802 = BgL_arg1853z00_2141
; }  else 
{ 
 obj_t BgL_auxz00_11805;
BgL_auxz00_11805 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33197L), BGl_string3598z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_arg1853z00_2141); 
FAILURE(BgL_auxz00_11805,BFALSE,BFALSE);} 
BgL_n2z00_4069 = 
(long)CINT(BgL_tmpz00_11802); } 
BgL_test4611z00_11787 = 
(3L==BgL_n2z00_4069); } } 
if(BgL_test4611z00_11787)
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_oldzd2buckszd2_2075;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4615z00_11811;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11812;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2331z00_4073;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3376z00_5639;
BgL_aux3376z00_5639 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3376z00_5639))
{ /* Llib/weakhash.scm 800 */
BgL_res2331z00_4073 = BgL_aux3376z00_5639; }  else 
{ 
 obj_t BgL_auxz00_11816;
BgL_auxz00_11816 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3376z00_5639); 
FAILURE(BgL_auxz00_11816,BFALSE,BFALSE);} } 
BgL_tmpz00_11812 = BgL_res2331z00_4073; } 
BgL_test4615z00_11811 = 
(BgL_tmpz00_11812==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4615z00_11811)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11821;
BgL_tmpz00_11821 = 
(int)(2L); 
BgL_oldzd2buckszd2_2075 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11821); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_oldzd2buckszd2_2075 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 long BgL_oldzd2buckszd2lenz00_2076;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_4074;
if(
VECTORP(BgL_oldzd2buckszd2_2075))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_4074 = BgL_oldzd2buckszd2_2075; }  else 
{ 
 obj_t BgL_auxz00_11827;
BgL_auxz00_11827 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2075); 
FAILURE(BgL_auxz00_11827,BFALSE,BFALSE);} 
BgL_oldzd2buckszd2lenz00_2076 = 
VECTOR_LENGTH(BgL_vectorz00_4074); } 
{ /* Llib/weakhash.scm 800 */
 long BgL_newzd2buckszd2lenz00_2077;
BgL_newzd2buckszd2lenz00_2077 = 
(2L*BgL_oldzd2buckszd2lenz00_2076); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_newzd2buckszd2_2078;
BgL_newzd2buckszd2_2078 = 
make_vector(BgL_newzd2buckszd2lenz00_2077, BNIL); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_countz00_2079;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4618z00_11834;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11835;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2332z00_4079;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3380z00_5643;
BgL_aux3380z00_5643 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3380z00_5643))
{ /* Llib/weakhash.scm 800 */
BgL_res2332z00_4079 = BgL_aux3380z00_5643; }  else 
{ 
 obj_t BgL_auxz00_11839;
BgL_auxz00_11839 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3380z00_5643); 
FAILURE(BgL_auxz00_11839,BFALSE,BFALSE);} } 
BgL_tmpz00_11835 = BgL_res2332z00_4079; } 
BgL_test4618z00_11834 = 
(BgL_tmpz00_11835==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4618z00_11834)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11844;
BgL_tmpz00_11844 = 
(int)(0L); 
BgL_countz00_2079 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11844); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_countz00_2079 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 obj_t BgL_nmaxz00_2080;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_a1136z00_2083;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4620z00_11848;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11849;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2333z00_4083;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3382z00_5645;
BgL_aux3382z00_5645 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3382z00_5645))
{ /* Llib/weakhash.scm 800 */
BgL_res2333z00_4083 = BgL_aux3382z00_5645; }  else 
{ 
 obj_t BgL_auxz00_11853;
BgL_auxz00_11853 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3382z00_5645); 
FAILURE(BgL_auxz00_11853,BFALSE,BFALSE);} } 
BgL_tmpz00_11849 = BgL_res2333z00_4083; } 
BgL_test4620z00_11848 = 
(BgL_tmpz00_11849==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4620z00_11848)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11858;
BgL_tmpz00_11858 = 
(int)(1L); 
BgL_a1136z00_2083 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11858); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_a1136z00_2083 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_b1137z00_2084;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4622z00_11862;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11863;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2334z00_4087;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3384z00_5647;
BgL_aux3384z00_5647 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3384z00_5647))
{ /* Llib/weakhash.scm 800 */
BgL_res2334z00_4087 = BgL_aux3384z00_5647; }  else 
{ 
 obj_t BgL_auxz00_11867;
BgL_auxz00_11867 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3384z00_5647); 
FAILURE(BgL_auxz00_11867,BFALSE,BFALSE);} } 
BgL_tmpz00_11863 = BgL_res2334z00_4087; } 
BgL_test4622z00_11862 = 
(BgL_tmpz00_11863==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4622z00_11862)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11872;
BgL_tmpz00_11872 = 
(int)(7L); 
BgL_b1137z00_2084 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_11872); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_b1137z00_2084 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4624z00_11876;
if(
INTEGERP(BgL_a1136z00_2083))
{ /* Llib/weakhash.scm 800 */
BgL_test4624z00_11876 = 
INTEGERP(BgL_b1137z00_2084)
; }  else 
{ /* Llib/weakhash.scm 800 */
BgL_test4624z00_11876 = ((bool_t)0)
; } 
if(BgL_test4624z00_11876)
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2080 = 
BINT(
(
(long)CINT(BgL_a1136z00_2083)*
(long)CINT(BgL_b1137z00_2084))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2080 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1136z00_2083, BgL_b1137z00_2084); } } } } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_arg1822z00_2081;
if(
REALP(BgL_nmaxz00_2080))
{ /* Llib/weakhash.scm 800 */
BgL_arg1822z00_2081 = 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_nmaxz00_2080))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_arg1822z00_2081 = BgL_nmaxz00_2080; } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4627z00_11890;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11891;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2335z00_4094;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3386z00_5649;
BgL_aux3386z00_5649 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3386z00_5649))
{ /* Llib/weakhash.scm 800 */
BgL_res2335z00_4094 = BgL_aux3386z00_5649; }  else 
{ 
 obj_t BgL_auxz00_11895;
BgL_auxz00_11895 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3386z00_5649); 
FAILURE(BgL_auxz00_11895,BFALSE,BFALSE);} } 
BgL_tmpz00_11891 = BgL_res2335z00_4094; } 
BgL_test4627z00_11890 = 
(BgL_tmpz00_11891==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4627z00_11890)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11900;
BgL_tmpz00_11900 = 
(int)(1L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11900, BgL_arg1822z00_2081); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } } } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4629z00_11904;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_11905;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2336z00_4098;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3388z00_5651;
BgL_aux3388z00_5651 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3388z00_5651))
{ /* Llib/weakhash.scm 800 */
BgL_res2336z00_4098 = BgL_aux3388z00_5651; }  else 
{ 
 obj_t BgL_auxz00_11909;
BgL_auxz00_11909 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3388z00_5651); 
FAILURE(BgL_auxz00_11909,BFALSE,BFALSE);} } 
BgL_tmpz00_11905 = BgL_res2336z00_4098; } 
BgL_test4629z00_11904 = 
(BgL_tmpz00_11905==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4629z00_11904)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_11914;
BgL_tmpz00_11914 = 
(int)(2L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_11914, BgL_newzd2buckszd2_2078); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ 
 long BgL_iz00_2087;
BgL_iz00_2087 = 0L; 
BgL_zc3z04anonymousza31825ze3z87_2088:
if(
(BgL_iz00_2087<BgL_oldzd2buckszd2lenz00_2076))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_g1140z00_2090;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_4101;
if(
VECTORP(BgL_oldzd2buckszd2_2075))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_4101 = BgL_oldzd2buckszd2_2075; }  else 
{ 
 obj_t BgL_auxz00_11922;
BgL_auxz00_11922 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2075); 
FAILURE(BgL_auxz00_11922,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4633z00_11926;
{ /* Llib/weakhash.scm 800 */
 long BgL_tmpz00_11927;
BgL_tmpz00_11927 = 
VECTOR_LENGTH(BgL_vectorz00_4101); 
BgL_test4633z00_11926 = 
BOUND_CHECK(BgL_iz00_2087, BgL_tmpz00_11927); } 
if(BgL_test4633z00_11926)
{ /* Llib/weakhash.scm 800 */
BgL_g1140z00_2090 = 
VECTOR_REF(BgL_vectorz00_4101,BgL_iz00_2087); }  else 
{ 
 obj_t BgL_auxz00_11931;
BgL_auxz00_11931 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_4101, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4101)), 
(int)(BgL_iz00_2087)); 
FAILURE(BgL_auxz00_11931,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_l1138z00_2092;
BgL_l1138z00_2092 = BgL_g1140z00_2090; 
BgL_zc3z04anonymousza31827ze3z87_2093:
if(
PAIRP(BgL_l1138z00_2092))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 845 */
 obj_t BgL_cellz00_2095;
BgL_cellz00_2095 = 
CAR(BgL_l1138z00_2092); 
{ /* Llib/weakhash.scm 845 */
 obj_t BgL_keyz00_2096; obj_t BgL_dataz00_2097;
{ /* Llib/weakhash.scm 845 */
 obj_t BgL_arg1834z00_2103;
{ /* Llib/weakhash.scm 845 */
 obj_t BgL_pairz00_4104;
if(
PAIRP(BgL_cellz00_2095))
{ /* Llib/weakhash.scm 845 */
BgL_pairz00_4104 = BgL_cellz00_2095; }  else 
{ 
 obj_t BgL_auxz00_11943;
BgL_auxz00_11943 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33265L), BGl_string3601z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_cellz00_2095); 
FAILURE(BgL_auxz00_11943,BFALSE,BFALSE);} 
BgL_arg1834z00_2103 = 
CAR(BgL_pairz00_4104); } 
{ /* Llib/weakhash.scm 845 */
 obj_t BgL_objz00_4105;
if(
BGL_WEAKPTRP(BgL_arg1834z00_2103))
{ /* Llib/weakhash.scm 845 */
BgL_objz00_4105 = BgL_arg1834z00_2103; }  else 
{ 
 obj_t BgL_auxz00_11950;
BgL_auxz00_11950 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33269L), BGl_string3601z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1834z00_2103); 
FAILURE(BgL_auxz00_11950,BFALSE,BFALSE);} 
BgL_keyz00_2096 = 
bgl_weakptr_data(BgL_objz00_4105); } } 
{ /* Llib/weakhash.scm 846 */
 obj_t BgL_arg1835z00_2104;
{ /* Llib/weakhash.scm 846 */
 obj_t BgL_pairz00_4106;
if(
PAIRP(BgL_cellz00_2095))
{ /* Llib/weakhash.scm 846 */
BgL_pairz00_4106 = BgL_cellz00_2095; }  else 
{ 
 obj_t BgL_auxz00_11957;
BgL_auxz00_11957 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33304L), BGl_string3601z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_cellz00_2095); 
FAILURE(BgL_auxz00_11957,BFALSE,BFALSE);} 
BgL_arg1835z00_2104 = 
CDR(BgL_pairz00_4106); } 
{ /* Llib/weakhash.scm 846 */
 obj_t BgL_objz00_4107;
if(
BGL_WEAKPTRP(BgL_arg1835z00_2104))
{ /* Llib/weakhash.scm 846 */
BgL_objz00_4107 = BgL_arg1835z00_2104; }  else 
{ 
 obj_t BgL_auxz00_11964;
BgL_auxz00_11964 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33308L), BGl_string3601z00zz__weakhashz00, BGl_string3448z00zz__weakhashz00, BgL_arg1835z00_2104); 
FAILURE(BgL_auxz00_11964,BFALSE,BFALSE);} 
BgL_dataz00_2097 = 
bgl_weakptr_data(BgL_objz00_4107); } } 
{ /* Llib/weakhash.scm 847 */
 bool_t BgL_test4639z00_11969;
if(
(BgL_keyz00_2096==BUNSPEC))
{ /* Llib/weakhash.scm 847 */
BgL_test4639z00_11969 = ((bool_t)1)
; }  else 
{ /* Llib/weakhash.scm 847 */
BgL_test4639z00_11969 = 
(BgL_dataz00_2097==BUNSPEC)
; } 
if(BgL_test4639z00_11969)
{ 
 obj_t BgL_tmpz00_11973;
{ /* Llib/weakhash.scm 849 */
 obj_t BgL_aux3400z00_5663;
BgL_aux3400z00_5663 = BgL_countz00_2079; 
if(
INTEGERP(BgL_aux3400z00_5663))
{ /* Llib/weakhash.scm 849 */
BgL_tmpz00_11973 = BgL_aux3400z00_5663
; }  else 
{ 
 obj_t BgL_auxz00_11976;
BgL_auxz00_11976 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33414L), BGl_string3601z00zz__weakhashz00, BGl_string3439z00zz__weakhashz00, BgL_aux3400z00_5663); 
FAILURE(BgL_auxz00_11976,BFALSE,BFALSE);} } 
BgL_countz00_2079 = 
SUBFX(BgL_tmpz00_11973, 
BINT(1L)); }  else 
{ /* Llib/weakhash.scm 850 */
 long BgL_hz00_2099;
{ /* Llib/weakhash.scm 851 */
 long BgL_arg1833z00_2102;
BgL_arg1833z00_2102 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_126, BgL_keyz00_2096); 
{ /* Llib/weakhash.scm 850 */
 long BgL_n1z00_4109; long BgL_n2z00_4110;
BgL_n1z00_4109 = BgL_arg1833z00_2102; 
BgL_n2z00_4110 = BgL_newzd2buckszd2lenz00_2077; 
{ /* Llib/weakhash.scm 850 */
 bool_t BgL_test4642z00_11983;
{ /* Llib/weakhash.scm 850 */
 long BgL_arg2024z00_4112;
BgL_arg2024z00_4112 = 
(((BgL_n1z00_4109) | (BgL_n2z00_4110)) & -2147483648); 
BgL_test4642z00_11983 = 
(BgL_arg2024z00_4112==0L); } 
if(BgL_test4642z00_11983)
{ /* Llib/weakhash.scm 850 */
 int32_t BgL_arg2020z00_4113;
{ /* Llib/weakhash.scm 850 */
 int32_t BgL_arg2021z00_4114; int32_t BgL_arg2022z00_4115;
BgL_arg2021z00_4114 = 
(int32_t)(BgL_n1z00_4109); 
BgL_arg2022z00_4115 = 
(int32_t)(BgL_n2z00_4110); 
BgL_arg2020z00_4113 = 
(BgL_arg2021z00_4114%BgL_arg2022z00_4115); } 
{ /* Llib/weakhash.scm 850 */
 long BgL_arg2135z00_4120;
BgL_arg2135z00_4120 = 
(long)(BgL_arg2020z00_4113); 
BgL_hz00_2099 = 
(long)(BgL_arg2135z00_4120); } }  else 
{ /* Llib/weakhash.scm 850 */
BgL_hz00_2099 = 
(BgL_n1z00_4109%BgL_n2z00_4110); } } } } 
{ /* Llib/weakhash.scm 856 */
 obj_t BgL_arg1831z00_2100;
{ /* Llib/weakhash.scm 856 */
 obj_t BgL_arg1832z00_2101;
{ /* Llib/weakhash.scm 856 */
 bool_t BgL_test4643z00_11992;
{ /* Llib/weakhash.scm 856 */
 long BgL_tmpz00_11993;
BgL_tmpz00_11993 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2078); 
BgL_test4643z00_11992 = 
BOUND_CHECK(BgL_hz00_2099, BgL_tmpz00_11993); } 
if(BgL_test4643z00_11992)
{ /* Llib/weakhash.scm 856 */
BgL_arg1832z00_2101 = 
VECTOR_REF(BgL_newzd2buckszd2_2078,BgL_hz00_2099); }  else 
{ 
 obj_t BgL_auxz00_11997;
BgL_auxz00_11997 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33595L), BGl_string3445z00zz__weakhashz00, BgL_newzd2buckszd2_2078, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2078)), 
(int)(BgL_hz00_2099)); 
FAILURE(BgL_auxz00_11997,BFALSE,BFALSE);} } 
BgL_arg1831z00_2100 = 
MAKE_YOUNG_PAIR(BgL_cellz00_2095, BgL_arg1832z00_2101); } 
{ /* Llib/weakhash.scm 853 */
 bool_t BgL_test4644z00_12005;
{ /* Llib/weakhash.scm 853 */
 long BgL_tmpz00_12006;
BgL_tmpz00_12006 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2078); 
BgL_test4644z00_12005 = 
BOUND_CHECK(BgL_hz00_2099, BgL_tmpz00_12006); } 
if(BgL_test4644z00_12005)
{ /* Llib/weakhash.scm 853 */
VECTOR_SET(BgL_newzd2buckszd2_2078,BgL_hz00_2099,BgL_arg1831z00_2100); }  else 
{ 
 obj_t BgL_auxz00_12010;
BgL_auxz00_12010 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33533L), BGl_string3450z00zz__weakhashz00, BgL_newzd2buckszd2_2078, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2078)), 
(int)(BgL_hz00_2099)); 
FAILURE(BgL_auxz00_12010,BFALSE,BFALSE);} } } } } } } 
{ 
 obj_t BgL_l1138z00_12017;
BgL_l1138z00_12017 = 
CDR(BgL_l1138z00_2092); 
BgL_l1138z00_2092 = BgL_l1138z00_12017; 
goto BgL_zc3z04anonymousza31827ze3z87_2093;} }  else 
{ /* Llib/weakhash.scm 800 */
if(
NULLP(BgL_l1138z00_2092))
{ /* Llib/weakhash.scm 800 */BTRUE; }  else 
{ /* Llib/weakhash.scm 800 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3464z00zz__weakhashz00, BGl_string3465z00zz__weakhashz00, BgL_l1138z00_2092); } } } } 
{ 
 long BgL_iz00_12022;
BgL_iz00_12022 = 
(BgL_iz00_2087+1L); 
BgL_iz00_2087 = BgL_iz00_12022; 
goto BgL_zc3z04anonymousza31825ze3z87_2088;} }  else 
{ /* Llib/weakhash.scm 800 */((bool_t)0); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vz00_4128;
BgL_vz00_4128 = BgL_countz00_2079; 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4646z00_12024;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12025;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2337z00_4132;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3401z00_5664;
BgL_aux3401z00_5664 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3401z00_5664))
{ /* Llib/weakhash.scm 800 */
BgL_res2337z00_4132 = BgL_aux3401z00_5664; }  else 
{ 
 obj_t BgL_auxz00_12029;
BgL_auxz00_12029 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3401z00_5664); 
FAILURE(BgL_auxz00_12029,BFALSE,BFALSE);} } 
BgL_tmpz00_12025 = BgL_res2337z00_4132; } 
BgL_test4646z00_12024 = 
(BgL_tmpz00_12025==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4646z00_12024)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12034;
BgL_tmpz00_12034 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_12034, BgL_vz00_4128);}  else 
{ /* Llib/weakhash.scm 800 */
return 
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126);} } } } } } } } }  else 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_oldzd2buckszd2_2110;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4648z00_12038;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12039;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2338z00_4136;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3403z00_5666;
BgL_aux3403z00_5666 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3403z00_5666))
{ /* Llib/weakhash.scm 800 */
BgL_res2338z00_4136 = BgL_aux3403z00_5666; }  else 
{ 
 obj_t BgL_auxz00_12043;
BgL_auxz00_12043 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3403z00_5666); 
FAILURE(BgL_auxz00_12043,BFALSE,BFALSE);} } 
BgL_tmpz00_12039 = BgL_res2338z00_4136; } 
BgL_test4648z00_12038 = 
(BgL_tmpz00_12039==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4648z00_12038)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12048;
BgL_tmpz00_12048 = 
(int)(2L); 
BgL_oldzd2buckszd2_2110 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_12048); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_oldzd2buckszd2_2110 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 long BgL_oldzd2buckszd2lenz00_2111;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_4137;
if(
VECTORP(BgL_oldzd2buckszd2_2110))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_4137 = BgL_oldzd2buckszd2_2110; }  else 
{ 
 obj_t BgL_auxz00_12054;
BgL_auxz00_12054 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2110); 
FAILURE(BgL_auxz00_12054,BFALSE,BFALSE);} 
BgL_oldzd2buckszd2lenz00_2111 = 
VECTOR_LENGTH(BgL_vectorz00_4137); } 
{ /* Llib/weakhash.scm 800 */
 long BgL_newzd2buckszd2lenz00_2112;
BgL_newzd2buckszd2lenz00_2112 = 
(2L*BgL_oldzd2buckszd2lenz00_2111); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_newzd2buckszd2_2113;
BgL_newzd2buckszd2_2113 = 
make_vector(BgL_newzd2buckszd2lenz00_2112, BNIL); 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_countz00_2114;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4651z00_12061;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12062;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2339z00_4142;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3407z00_5670;
BgL_aux3407z00_5670 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3407z00_5670))
{ /* Llib/weakhash.scm 800 */
BgL_res2339z00_4142 = BgL_aux3407z00_5670; }  else 
{ 
 obj_t BgL_auxz00_12066;
BgL_auxz00_12066 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3407z00_5670); 
FAILURE(BgL_auxz00_12066,BFALSE,BFALSE);} } 
BgL_tmpz00_12062 = BgL_res2339z00_4142; } 
BgL_test4651z00_12061 = 
(BgL_tmpz00_12062==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4651z00_12061)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12071;
BgL_tmpz00_12071 = 
(int)(0L); 
BgL_countz00_2114 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_12071); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_countz00_2114 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 obj_t BgL_nmaxz00_2115;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_a1141z00_2118;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4653z00_12075;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12076;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2340z00_4146;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3409z00_5672;
BgL_aux3409z00_5672 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3409z00_5672))
{ /* Llib/weakhash.scm 800 */
BgL_res2340z00_4146 = BgL_aux3409z00_5672; }  else 
{ 
 obj_t BgL_auxz00_12080;
BgL_auxz00_12080 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3409z00_5672); 
FAILURE(BgL_auxz00_12080,BFALSE,BFALSE);} } 
BgL_tmpz00_12076 = BgL_res2340z00_4146; } 
BgL_test4653z00_12075 = 
(BgL_tmpz00_12076==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4653z00_12075)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12085;
BgL_tmpz00_12085 = 
(int)(1L); 
BgL_a1141z00_2118 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_12085); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_a1141z00_2118 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_b1142z00_2119;
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4655z00_12089;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12090;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2341z00_4150;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3411z00_5674;
BgL_aux3411z00_5674 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3411z00_5674))
{ /* Llib/weakhash.scm 800 */
BgL_res2341z00_4150 = BgL_aux3411z00_5674; }  else 
{ 
 obj_t BgL_auxz00_12094;
BgL_auxz00_12094 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3411z00_5674); 
FAILURE(BgL_auxz00_12094,BFALSE,BFALSE);} } 
BgL_tmpz00_12090 = BgL_res2341z00_4150; } 
BgL_test4655z00_12089 = 
(BgL_tmpz00_12090==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4655z00_12089)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12099;
BgL_tmpz00_12099 = 
(int)(7L); 
BgL_b1142z00_2119 = 
STRUCT_REF(BgL_tablez00_126, BgL_tmpz00_12099); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_b1142z00_2119 = 
BGl_errorz00zz__errorz00(BGl_string3430z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ /* Llib/weakhash.scm 800 */

{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4657z00_12103;
if(
INTEGERP(BgL_a1141z00_2118))
{ /* Llib/weakhash.scm 800 */
BgL_test4657z00_12103 = 
INTEGERP(BgL_b1142z00_2119)
; }  else 
{ /* Llib/weakhash.scm 800 */
BgL_test4657z00_12103 = ((bool_t)0)
; } 
if(BgL_test4657z00_12103)
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2115 = 
BINT(
(
(long)CINT(BgL_a1141z00_2118)*
(long)CINT(BgL_b1142z00_2119))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_nmaxz00_2115 = 
BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1141z00_2118, BgL_b1142z00_2119); } } } } } 
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_arg1839z00_2116;
if(
REALP(BgL_nmaxz00_2115))
{ /* Llib/weakhash.scm 800 */
BgL_arg1839z00_2116 = 
BINT(
(long)(
REAL_TO_DOUBLE(BgL_nmaxz00_2115))); }  else 
{ /* Llib/weakhash.scm 800 */
BgL_arg1839z00_2116 = BgL_nmaxz00_2115; } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4660z00_12117;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12118;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2342z00_4157;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3413z00_5676;
BgL_aux3413z00_5676 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3413z00_5676))
{ /* Llib/weakhash.scm 800 */
BgL_res2342z00_4157 = BgL_aux3413z00_5676; }  else 
{ 
 obj_t BgL_auxz00_12122;
BgL_auxz00_12122 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3413z00_5676); 
FAILURE(BgL_auxz00_12122,BFALSE,BFALSE);} } 
BgL_tmpz00_12118 = BgL_res2342z00_4157; } 
BgL_test4660z00_12117 = 
(BgL_tmpz00_12118==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4660z00_12117)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12127;
BgL_tmpz00_12127 = 
(int)(1L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_12127, BgL_arg1839z00_2116); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } } } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4662z00_12131;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12132;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2343z00_4161;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3415z00_5678;
BgL_aux3415z00_5678 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3415z00_5678))
{ /* Llib/weakhash.scm 800 */
BgL_res2343z00_4161 = BgL_aux3415z00_5678; }  else 
{ 
 obj_t BgL_auxz00_12136;
BgL_auxz00_12136 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3415z00_5678); 
FAILURE(BgL_auxz00_12136,BFALSE,BFALSE);} } 
BgL_tmpz00_12132 = BgL_res2343z00_4161; } 
BgL_test4662z00_12131 = 
(BgL_tmpz00_12132==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4662z00_12131)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12141;
BgL_tmpz00_12141 = 
(int)(2L); 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_12141, BgL_newzd2buckszd2_2113); }  else 
{ /* Llib/weakhash.scm 800 */
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126); } } 
{ 
 long BgL_iz00_2122;
BgL_iz00_2122 = 0L; 
BgL_zc3z04anonymousza31842ze3z87_2123:
if(
(BgL_iz00_2122<BgL_oldzd2buckszd2lenz00_2111))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_g1145z00_2125;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_vectorz00_4164;
if(
VECTORP(BgL_oldzd2buckszd2_2110))
{ /* Llib/weakhash.scm 800 */
BgL_vectorz00_4164 = BgL_oldzd2buckszd2_2110; }  else 
{ 
 obj_t BgL_auxz00_12149;
BgL_auxz00_12149 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3453z00zz__weakhashz00, BGl_string3444z00zz__weakhashz00, BgL_oldzd2buckszd2_2110); 
FAILURE(BgL_auxz00_12149,BFALSE,BFALSE);} 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4666z00_12153;
{ /* Llib/weakhash.scm 800 */
 long BgL_tmpz00_12154;
BgL_tmpz00_12154 = 
VECTOR_LENGTH(BgL_vectorz00_4164); 
BgL_test4666z00_12153 = 
BOUND_CHECK(BgL_iz00_2122, BgL_tmpz00_12154); } 
if(BgL_test4666z00_12153)
{ /* Llib/weakhash.scm 800 */
BgL_g1145z00_2125 = 
VECTOR_REF(BgL_vectorz00_4164,BgL_iz00_2122); }  else 
{ 
 obj_t BgL_auxz00_12158;
BgL_auxz00_12158 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3445z00zz__weakhashz00, BgL_vectorz00_4164, 
(int)(
VECTOR_LENGTH(BgL_vectorz00_4164)), 
(int)(BgL_iz00_2122)); 
FAILURE(BgL_auxz00_12158,BFALSE,BFALSE);} } } 
{ 
 obj_t BgL_l1143z00_2127;
BgL_l1143z00_2127 = BgL_g1145z00_2125; 
BgL_zc3z04anonymousza31844ze3z87_2128:
if(
PAIRP(BgL_l1143z00_2127))
{ /* Llib/weakhash.scm 800 */
{ /* Llib/weakhash.scm 860 */
 obj_t BgL_cellz00_2130;
BgL_cellz00_2130 = 
CAR(BgL_l1143z00_2127); 
{ /* Llib/weakhash.scm 860 */
 long BgL_hz00_2131;
{ /* Llib/weakhash.scm 862 */
 long BgL_arg1848z00_2134;
{ /* Llib/weakhash.scm 862 */
 obj_t BgL_arg1849z00_2135;
{ /* Llib/weakhash.scm 862 */
 obj_t BgL_pairz00_4167;
if(
PAIRP(BgL_cellz00_2130))
{ /* Llib/weakhash.scm 862 */
BgL_pairz00_4167 = BgL_cellz00_2130; }  else 
{ 
 obj_t BgL_auxz00_12170;
BgL_auxz00_12170 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33754L), BGl_string3602z00zz__weakhashz00, BGl_string3447z00zz__weakhashz00, BgL_cellz00_2130); 
FAILURE(BgL_auxz00_12170,BFALSE,BFALSE);} 
BgL_arg1849z00_2135 = 
CAR(BgL_pairz00_4167); } 
BgL_arg1848z00_2134 = 
BGl_tablezd2getzd2hashnumberz00zz__weakhashz00(BgL_tablez00_126, BgL_arg1849z00_2135); } 
{ /* Llib/weakhash.scm 860 */
 long BgL_n1z00_4168; long BgL_n2z00_4169;
BgL_n1z00_4168 = BgL_arg1848z00_2134; 
BgL_n2z00_4169 = BgL_newzd2buckszd2lenz00_2112; 
{ /* Llib/weakhash.scm 860 */
 bool_t BgL_test4669z00_12176;
{ /* Llib/weakhash.scm 860 */
 long BgL_arg2024z00_4171;
BgL_arg2024z00_4171 = 
(((BgL_n1z00_4168) | (BgL_n2z00_4169)) & -2147483648); 
BgL_test4669z00_12176 = 
(BgL_arg2024z00_4171==0L); } 
if(BgL_test4669z00_12176)
{ /* Llib/weakhash.scm 860 */
 int32_t BgL_arg2020z00_4172;
{ /* Llib/weakhash.scm 860 */
 int32_t BgL_arg2021z00_4173; int32_t BgL_arg2022z00_4174;
BgL_arg2021z00_4173 = 
(int32_t)(BgL_n1z00_4168); 
BgL_arg2022z00_4174 = 
(int32_t)(BgL_n2z00_4169); 
BgL_arg2020z00_4172 = 
(BgL_arg2021z00_4173%BgL_arg2022z00_4174); } 
{ /* Llib/weakhash.scm 860 */
 long BgL_arg2135z00_4179;
BgL_arg2135z00_4179 = 
(long)(BgL_arg2020z00_4172); 
BgL_hz00_2131 = 
(long)(BgL_arg2135z00_4179); } }  else 
{ /* Llib/weakhash.scm 860 */
BgL_hz00_2131 = 
(BgL_n1z00_4168%BgL_n2z00_4169); } } } } 
{ /* Llib/weakhash.scm 868 */
 obj_t BgL_arg1846z00_2132;
{ /* Llib/weakhash.scm 868 */
 obj_t BgL_arg1847z00_2133;
{ /* Llib/weakhash.scm 868 */
 bool_t BgL_test4670z00_12185;
{ /* Llib/weakhash.scm 868 */
 long BgL_tmpz00_12186;
BgL_tmpz00_12186 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2113); 
BgL_test4670z00_12185 = 
BOUND_CHECK(BgL_hz00_2131, BgL_tmpz00_12186); } 
if(BgL_test4670z00_12185)
{ /* Llib/weakhash.scm 868 */
BgL_arg1847z00_2133 = 
VECTOR_REF(BgL_newzd2buckszd2_2113,BgL_hz00_2131); }  else 
{ 
 obj_t BgL_auxz00_12190;
BgL_auxz00_12190 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33856L), BGl_string3445z00zz__weakhashz00, BgL_newzd2buckszd2_2113, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2113)), 
(int)(BgL_hz00_2131)); 
FAILURE(BgL_auxz00_12190,BFALSE,BFALSE);} } 
BgL_arg1846z00_2132 = 
MAKE_YOUNG_PAIR(BgL_cellz00_2130, BgL_arg1847z00_2133); } 
{ /* Llib/weakhash.scm 864 */
 bool_t BgL_test4671z00_12198;
{ /* Llib/weakhash.scm 864 */
 long BgL_tmpz00_12199;
BgL_tmpz00_12199 = 
VECTOR_LENGTH(BgL_newzd2buckszd2_2113); 
BgL_test4671z00_12198 = 
BOUND_CHECK(BgL_hz00_2131, BgL_tmpz00_12199); } 
if(BgL_test4671z00_12198)
{ /* Llib/weakhash.scm 864 */
VECTOR_SET(BgL_newzd2buckszd2_2113,BgL_hz00_2131,BgL_arg1846z00_2132); }  else 
{ 
 obj_t BgL_auxz00_12203;
BgL_auxz00_12203 = 
BGl_indexzd2outzd2ofzd2boundszd2errorz00zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(33798L), BGl_string3450z00zz__weakhashz00, BgL_newzd2buckszd2_2113, 
(int)(
VECTOR_LENGTH(BgL_newzd2buckszd2_2113)), 
(int)(BgL_hz00_2131)); 
FAILURE(BgL_auxz00_12203,BFALSE,BFALSE);} } } } } 
{ 
 obj_t BgL_l1143z00_12210;
BgL_l1143z00_12210 = 
CDR(BgL_l1143z00_2127); 
BgL_l1143z00_2127 = BgL_l1143z00_12210; 
goto BgL_zc3z04anonymousza31844ze3z87_2128;} }  else 
{ /* Llib/weakhash.scm 800 */
if(
NULLP(BgL_l1143z00_2127))
{ /* Llib/weakhash.scm 800 */BTRUE; }  else 
{ /* Llib/weakhash.scm 800 */
BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string3464z00zz__weakhashz00, BGl_string3465z00zz__weakhashz00, BgL_l1143z00_2127); } } } } 
{ 
 long BgL_iz00_12215;
BgL_iz00_12215 = 
(BgL_iz00_2122+1L); 
BgL_iz00_2122 = BgL_iz00_12215; 
goto BgL_zc3z04anonymousza31842ze3z87_2123;} }  else 
{ /* Llib/weakhash.scm 800 */((bool_t)0); } } 
{ /* Llib/weakhash.scm 800 */
 bool_t BgL_test4673z00_12217;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_tmpz00_12218;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_res2344z00_4190;
{ /* Llib/weakhash.scm 800 */
 obj_t BgL_aux3421z00_5684;
BgL_aux3421z00_5684 = 
STRUCT_KEY(BgL_tablez00_126); 
if(
SYMBOLP(BgL_aux3421z00_5684))
{ /* Llib/weakhash.scm 800 */
BgL_res2344z00_4190 = BgL_aux3421z00_5684; }  else 
{ 
 obj_t BgL_auxz00_12222;
BgL_auxz00_12222 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(31705L), BGl_string3598z00zz__weakhashz00, BGl_string3427z00zz__weakhashz00, BgL_aux3421z00_5684); 
FAILURE(BgL_auxz00_12222,BFALSE,BFALSE);} } 
BgL_tmpz00_12218 = BgL_res2344z00_4190; } 
BgL_test4673z00_12217 = 
(BgL_tmpz00_12218==BGl_symbol3428z00zz__weakhashz00); } 
if(BgL_test4673z00_12217)
{ /* Llib/weakhash.scm 800 */
 int BgL_tmpz00_12227;
BgL_tmpz00_12227 = 
(int)(0L); 
return 
STRUCT_SET(BgL_tablez00_126, BgL_tmpz00_12227, BgL_countz00_2114);}  else 
{ /* Llib/weakhash.scm 800 */
return 
BGl_errorz00zz__errorz00(BGl_string3449z00zz__weakhashz00, BGl_string3429z00zz__weakhashz00, BgL_tablez00_126);} } } } } } } } } } } } 

}



/* weak-hashtable-expand! */
BGL_EXPORTED_DEF obj_t BGl_weakzd2hashtablezd2expandz12z12zz__weakhashz00(obj_t BgL_tablez00_127)
{
{ /* Llib/weakhash.scm 876 */
if(
BGl_hashtablezd2weakzd2keyszf3zf3zz__hashz00(BgL_tablez00_127))
{ /* Llib/weakhash.scm 877 */
BGL_TAIL return 
BGl_weakzd2keyszd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_127);}  else 
{ /* Llib/weakhash.scm 877 */
BGL_TAIL return 
BGl_weakzd2oldzd2hashtablezd2expandz12zc0zz__weakhashz00(BgL_tablez00_127);} } 

}



/* &weak-hashtable-expand! */
obj_t BGl_z62weakzd2hashtablezd2expandz12z70zz__weakhashz00(obj_t BgL_envz00_4574, obj_t BgL_tablez00_4575)
{
{ /* Llib/weakhash.scm 876 */
{ /* Llib/weakhash.scm 877 */
 obj_t BgL_auxz00_12235;
if(
STRUCTP(BgL_tablez00_4575))
{ /* Llib/weakhash.scm 877 */
BgL_auxz00_12235 = BgL_tablez00_4575
; }  else 
{ 
 obj_t BgL_auxz00_12238;
BgL_auxz00_12238 = 
BGl_typezd2errorzd2zz__errorz00(BGl_string3442z00zz__weakhashz00, 
BINT(34257L), BGl_string3603z00zz__weakhashz00, BGl_string3484z00zz__weakhashz00, BgL_tablez00_4575); 
FAILURE(BgL_auxz00_12238,BFALSE,BFALSE);} 
return 
BGl_weakzd2hashtablezd2expandz12z12zz__weakhashz00(BgL_auxz00_12235);} } 

}



/* object-init */
obj_t BGl_objectzd2initzd2zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
return BUNSPEC;} 

}



/* generic-init */
obj_t BGl_genericzd2initzd2zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
return BUNSPEC;} 

}



/* method-init */
obj_t BGl_methodzd2initzd2zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
return BUNSPEC;} 

}



/* imported-modules-init */
obj_t BGl_importedzd2moduleszd2initz00zz__weakhashz00(void)
{
{ /* Llib/weakhash.scm 18 */
BGl_modulezd2initializa7ationz75zz__errorz00(88804785L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00)); 
BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00)); 
BGl_modulezd2initializa7ationz75zz__paramz00(453939141L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00)); 
BGl_modulezd2initializa7ationz75zz__hashz00(482391669L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00)); 
BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00)); 
BGl_modulezd2initializa7ationz75zz__objectz00(475449627L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00)); 
return 
BGl_modulezd2initializa7ationz75zz__threadz00(149516032L, 
BSTRING_TO_STRING(BGl_string3604z00zz__weakhashz00));} 

}

#ifdef __cplusplus
}
#endif
