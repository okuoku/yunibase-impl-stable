/*===========================================================================*/
/*   (User/user.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent User/user.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_USER_USER_TYPE_DEFINITIONS
#define BGL_USER_USER_TYPE_DEFINITIONS
#endif													// BGL_USER_USER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzuser_userz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2userzd2passza2zd2zz__evalz00;
	extern obj_t BGl_getzd2toplevelzd2unitz00zzmodule_includez00(void);
	static obj_t BGl_toplevelzd2initzd2zzuser_userz00(void);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzuser_userz00(void);
	static obj_t BGl_objectzd2initzd2zzuser_userz00(void);
	BGL_IMPORT obj_t BGl_za2userzd2passzd2nameza2z00zz__evalz00;
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzuser_userz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzuser_userz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzuser_userz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzuser_userz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzuser_userz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzuser_userz00(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62userzd2walkzb0zzuser_userz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_userzd2walkzd2zzuser_userz00(obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_STRING(BGl_string1074z00zzuser_userz00,
		BgL_bgl_string1074za700za7za7u1084za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1075z00zzuser_userz00,
		BgL_bgl_string1075za700za7za7u1085za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1076z00zzuser_userz00,
		BgL_bgl_string1076za700za7za7u1086za7, " error", 6);
	      DEFINE_STRING(BGl_string1077z00zzuser_userz00,
		BgL_bgl_string1077za700za7za7u1087za7, "s", 1);
	      DEFINE_STRING(BGl_string1078z00zzuser_userz00,
		BgL_bgl_string1078za700za7za7u1088za7, "", 0);
	      DEFINE_STRING(BGl_string1079z00zzuser_userz00,
		BgL_bgl_string1079za700za7za7u1089za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1080z00zzuser_userz00,
		BgL_bgl_string1080za700za7za7u1090za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1081z00zzuser_userz00,
		BgL_bgl_string1081za700za7za7u1091za7, "user_user", 9);
	      DEFINE_STRING(BGl_string1082z00zzuser_userz00,
		BgL_bgl_string1082za700za7za7u1092za7, "done dummy nothing pass-started ",
		32);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_userzd2walkzd2envz00zzuser_userz00,
		BgL_bgl_za762userza7d2walkza7b1093za7, BGl_z62userzd2walkzb0zzuser_userz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzuser_userz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzuser_userz00(long
		BgL_checksumz00_135, char *BgL_fromz00_136)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzuser_userz00))
				{
					BGl_requirezd2initializa7ationz75zzuser_userz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzuser_userz00();
					BGl_libraryzd2moduleszd2initz00zzuser_userz00();
					BGl_cnstzd2initzd2zzuser_userz00();
					BGl_importedzd2moduleszd2initz00zzuser_userz00();
					return BGl_toplevelzd2initzd2zzuser_userz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"user_user");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"user_user");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"user_user");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "user_user");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "user_user");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			{	/* User/user.scm 15 */
				obj_t BgL_cportz00_124;

				{	/* User/user.scm 15 */
					obj_t BgL_stringz00_131;

					BgL_stringz00_131 = BGl_string1082z00zzuser_userz00;
					{	/* User/user.scm 15 */
						obj_t BgL_startz00_132;

						BgL_startz00_132 = BINT(0L);
						{	/* User/user.scm 15 */
							obj_t BgL_endz00_133;

							BgL_endz00_133 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_131)));
							{	/* User/user.scm 15 */

								BgL_cportz00_124 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_131, BgL_startz00_132, BgL_endz00_133);
				}}}}
				{
					long BgL_iz00_125;

					BgL_iz00_125 = 3L;
				BgL_loopz00_126:
					if ((BgL_iz00_125 == -1L))
						{	/* User/user.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* User/user.scm 15 */
							{	/* User/user.scm 15 */
								obj_t BgL_arg1083z00_127;

								{	/* User/user.scm 15 */

									{	/* User/user.scm 15 */
										obj_t BgL_locationz00_129;

										BgL_locationz00_129 = BBOOL(((bool_t) 0));
										{	/* User/user.scm 15 */

											BgL_arg1083z00_127 =
												BGl_readz00zz__readerz00(BgL_cportz00_124,
												BgL_locationz00_129);
										}
									}
								}
								{	/* User/user.scm 15 */
									int BgL_tmpz00_166;

									BgL_tmpz00_166 = (int) (BgL_iz00_125);
									CNST_TABLE_SET(BgL_tmpz00_166, BgL_arg1083z00_127);
							}}
							{	/* User/user.scm 15 */
								int BgL_auxz00_130;

								BgL_auxz00_130 = (int) ((BgL_iz00_125 - 1L));
								{
									long BgL_iz00_171;

									BgL_iz00_171 = (long) (BgL_auxz00_130);
									BgL_iz00_125 = BgL_iz00_171;
									goto BgL_loopz00_126;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			return BUNSPEC;
		}

	}



/* user-walk */
	BGL_EXPORTED_DEF obj_t BGl_userzd2walkzd2zzuser_userz00(obj_t BgL_unitsz00_25)
	{
		{	/* User/user.scm 27 */
			if (PROCEDUREP(BGl_za2userzd2passza2zd2zz__evalz00))
				{	/* User/user.scm 29 */
					obj_t BgL_unitz00_49;

					BgL_unitz00_49 = BGl_getzd2toplevelzd2unitz00zzmodule_includez00();
					{	/* User/user.scm 30 */
						obj_t BgL_list1039z00_50;

						{	/* User/user.scm 30 */
							obj_t BgL_arg1040z00_51;

							{	/* User/user.scm 30 */
								obj_t BgL_arg1041z00_52;

								BgL_arg1041z00_52 =
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
								BgL_arg1040z00_51 =
									MAKE_YOUNG_PAIR(BGl_za2userzd2passzd2nameza2z00zz__evalz00,
									BgL_arg1041z00_52);
							}
							BgL_list1039z00_50 =
								MAKE_YOUNG_PAIR(BGl_string1074z00zzuser_userz00,
								BgL_arg1040z00_51);
						}
						BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1039z00_50);
					}
					BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
					BGl_za2currentzd2passza2zd2zzengine_passz00 =
						BGl_za2userzd2passzd2nameza2z00zz__evalz00;
					{	/* User/user.scm 30 */
						obj_t BgL_g1016z00_53;

						BgL_g1016z00_53 = BNIL;
						{
							obj_t BgL_hooksz00_56;
							obj_t BgL_hnamesz00_57;

							BgL_hooksz00_56 = BgL_g1016z00_53;
							BgL_hnamesz00_57 = BNIL;
						BgL_zc3z04anonymousza31042ze3z87_58:
							if (NULLP(BgL_hooksz00_56))
								{	/* User/user.scm 30 */
									CNST_TABLE_REF(0);
								}
							else
								{	/* User/user.scm 30 */
									bool_t BgL_test1098z00_187;

									{	/* User/user.scm 30 */
										obj_t BgL_fun1049z00_65;

										BgL_fun1049z00_65 = CAR(((obj_t) BgL_hooksz00_56));
										BgL_test1098z00_187 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1049z00_65));
									}
									if (BgL_test1098z00_187)
										{	/* User/user.scm 30 */
											obj_t BgL_arg1046z00_62;
											obj_t BgL_arg1047z00_63;

											BgL_arg1046z00_62 = CDR(((obj_t) BgL_hooksz00_56));
											BgL_arg1047z00_63 = CDR(((obj_t) BgL_hnamesz00_57));
											{
												obj_t BgL_hnamesz00_199;
												obj_t BgL_hooksz00_198;

												BgL_hooksz00_198 = BgL_arg1046z00_62;
												BgL_hnamesz00_199 = BgL_arg1047z00_63;
												BgL_hnamesz00_57 = BgL_hnamesz00_199;
												BgL_hooksz00_56 = BgL_hooksz00_198;
												goto BgL_zc3z04anonymousza31042ze3z87_58;
											}
										}
									else
										{	/* User/user.scm 30 */
											obj_t BgL_arg1048z00_64;

											BgL_arg1048z00_64 = CAR(((obj_t) BgL_hnamesz00_57));
											BGl_internalzd2errorzd2zztools_errorz00
												(BGl_za2userzd2passzd2nameza2z00zz__evalz00,
												BGl_string1075z00zzuser_userz00, BgL_arg1048z00_64);
										}
								}
						}
					}
					{	/* User/user.scm 31 */
						bool_t BgL_test1099z00_203;

						{	/* User/user.scm 31 */
							obj_t BgL_tmpz00_204;

							BgL_tmpz00_204 = STRUCT_REF(((obj_t) BgL_unitz00_49), (int) (2L));
							BgL_test1099z00_203 = PROCEDUREP(BgL_tmpz00_204);
						}
						if (BgL_test1099z00_203)
							{	/* User/user.scm 31 */
								CNST_TABLE_REF(1);
							}
						else
							{	/* User/user.scm 35 */
								obj_t BgL_arg1053z00_70;

								{	/* User/user.scm 35 */
									obj_t BgL_arg1054z00_71;

									BgL_arg1054z00_71 =
										STRUCT_REF(((obj_t) BgL_unitz00_49), (int) (2L));
									BgL_arg1053z00_70 =
										BGL_PROCEDURE_CALL1(BGl_za2userzd2passza2zd2zz__evalz00,
										BgL_arg1054z00_71);
								}
								{	/* User/user.scm 35 */
									int BgL_auxz00_219;
									obj_t BgL_tmpz00_217;

									BgL_auxz00_219 = (int) (2L);
									BgL_tmpz00_217 = ((obj_t) BgL_unitz00_49);
									STRUCT_SET(BgL_tmpz00_217, BgL_auxz00_219, BgL_arg1053z00_70);
					}}}
					{	/* User/user.scm 36 */
						obj_t BgL_valuez00_73;

						BgL_valuez00_73 = CNST_TABLE_REF(2);
						if (
							((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
								> 0L))
							{	/* User/user.scm 36 */
								{	/* User/user.scm 36 */
									obj_t BgL_port1020z00_75;

									{	/* User/user.scm 36 */
										obj_t BgL_tmpz00_226;

										BgL_tmpz00_226 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_port1020z00_75 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_226);
									}
									bgl_display_obj
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
										BgL_port1020z00_75);
									bgl_display_string(BGl_string1076z00zzuser_userz00,
										BgL_port1020z00_75);
									{	/* User/user.scm 36 */
										obj_t BgL_arg1057z00_76;

										{	/* User/user.scm 36 */
											bool_t BgL_test1101z00_231;

											if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
												{	/* User/user.scm 36 */
													if (INTEGERP
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
														{	/* User/user.scm 36 */
															BgL_test1101z00_231 =
																(
																(long)
																CINT
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																> 1L);
														}
													else
														{	/* User/user.scm 36 */
															BgL_test1101z00_231 =
																BGl_2ze3ze3zz__r4_numbers_6_5z00
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																BINT(1L));
														}
												}
											else
												{	/* User/user.scm 36 */
													BgL_test1101z00_231 = ((bool_t) 0);
												}
											if (BgL_test1101z00_231)
												{	/* User/user.scm 36 */
													BgL_arg1057z00_76 = BGl_string1077z00zzuser_userz00;
												}
											else
												{	/* User/user.scm 36 */
													BgL_arg1057z00_76 = BGl_string1078z00zzuser_userz00;
												}
										}
										bgl_display_obj(BgL_arg1057z00_76, BgL_port1020z00_75);
									}
									bgl_display_string(BGl_string1079z00zzuser_userz00,
										BgL_port1020z00_75);
									bgl_display_char(((unsigned char) 10), BgL_port1020z00_75);
								}
								{	/* User/user.scm 36 */
									obj_t BgL_list1060z00_80;

									BgL_list1060z00_80 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
									BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1060z00_80);
								}
							}
						else
							{	/* User/user.scm 36 */
								obj_t BgL_g1018z00_81;

								BgL_g1018z00_81 = BNIL;
								{
									obj_t BgL_hooksz00_84;
									obj_t BgL_hnamesz00_85;

									BgL_hooksz00_84 = BgL_g1018z00_81;
									BgL_hnamesz00_85 = BNIL;
								BgL_zc3z04anonymousza31061ze3z87_86:
									if (NULLP(BgL_hooksz00_84))
										{	/* User/user.scm 36 */
											return BgL_valuez00_73;
										}
									else
										{	/* User/user.scm 36 */
											bool_t BgL_test1105z00_248;

											{	/* User/user.scm 36 */
												obj_t BgL_fun1069z00_93;

												BgL_fun1069z00_93 = CAR(((obj_t) BgL_hooksz00_84));
												BgL_test1105z00_248 =
													CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1069z00_93));
											}
											if (BgL_test1105z00_248)
												{	/* User/user.scm 36 */
													obj_t BgL_arg1065z00_90;
													obj_t BgL_arg1066z00_91;

													BgL_arg1065z00_90 = CDR(((obj_t) BgL_hooksz00_84));
													BgL_arg1066z00_91 = CDR(((obj_t) BgL_hnamesz00_85));
													{
														obj_t BgL_hnamesz00_260;
														obj_t BgL_hooksz00_259;

														BgL_hooksz00_259 = BgL_arg1065z00_90;
														BgL_hnamesz00_260 = BgL_arg1066z00_91;
														BgL_hnamesz00_85 = BgL_hnamesz00_260;
														BgL_hooksz00_84 = BgL_hooksz00_259;
														goto BgL_zc3z04anonymousza31061ze3z87_86;
													}
												}
											else
												{	/* User/user.scm 36 */
													obj_t BgL_arg1068z00_92;

													BgL_arg1068z00_92 = CAR(((obj_t) BgL_hnamesz00_85));
													return
														BGl_internalzd2errorzd2zztools_errorz00
														(BGl_za2currentzd2passza2zd2zzengine_passz00,
														BGl_string1080z00zzuser_userz00, BgL_arg1068z00_92);
												}
										}
								}
							}
					}
				}
			else
				{	/* User/user.scm 28 */
					return CNST_TABLE_REF(3);
				}
		}

	}



/* &user-walk */
	obj_t BGl_z62userzd2walkzb0zzuser_userz00(obj_t BgL_envz00_121,
		obj_t BgL_unitsz00_122)
	{
		{	/* User/user.scm 27 */
			return BGl_userzd2walkzd2zzuser_userz00(BgL_unitsz00_122);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzuser_userz00(void)
	{
		{	/* User/user.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1081z00zzuser_userz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1081z00zzuser_userz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1081z00zzuser_userz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1081z00zzuser_userz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1081z00zzuser_userz00));
		}

	}

#ifdef __cplusplus
}
#endif
