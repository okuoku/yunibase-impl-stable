/*===========================================================================*/
/*   (Fxop/walk.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Fxop/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FXOP_WALK_TYPE_DEFINITIONS
#define BGL_FXOP_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_FXOP_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_initzd2fxopzd2cachez12z12zzfxop_walkz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzfxop_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62fxopz12z70zzfxop_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzfxop_walkz00(void);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzfxop_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzfxop_walkz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static obj_t BGl_z62initzd2fxopzd2cachez12z70zzfxop_walkz00(obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static obj_t BGl_za2longzd2ze3bintza2z31zzfxop_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzfxop_walkz00(void);
	static obj_t BGl_fxopzd2doublezd2intzd2letvarzf3ze70zc6zzfxop_walkz00(obj_t,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_fxopzd2funz12zc0zzfxop_walkz00(obj_t);
	static obj_t BGl_findzd2fxopze70z35zzfxop_walkz00(obj_t);
	static bool_t BGl_longzd2ze3bintzf3ze70z25zzfxop_walkz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzfxop_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_z62clearzd2fxopzd2cachez12z70zzfxop_walkz00(obj_t);
	static obj_t BGl_fxopzd2simplezd2intzd2letvarzf3ze70zc6zzfxop_walkz00(obj_t,
		BgL_nodez00_bglt);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzfxop_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzfxop_walkz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzfxop_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzfxop_walkz00(void);
	static BgL_nodez00_bglt BGl_bintze70ze7zzfxop_walkz00(BgL_nodez00_bglt);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_za2bintzd2ze3longza2z31zzfxop_walkz00 = BUNSPEC;
	static obj_t BGl_za2fxopsza2z00zzfxop_walkz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_fxopzd2walkz12zc0zzfxop_walkz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_fxopzd2boolzd2letvarzf3ze70z14zzfxop_walkz00(obj_t,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fxopzd2walkz12za2zzfxop_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62fxopz12zd2letzd2var1299z70zzfxop_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62fxopz121296z70zzfxop_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_fxopz12z12zzfxop_walkz00(BgL_nodez00_bglt);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static bool_t BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00(obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fxopzd2walkz12zd2envz12zzfxop_walkz00,
		BgL_bgl_za762fxopza7d2walkza711924za7,
		BGl_z62fxopzd2walkz12za2zzfxop_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1908z00zzfxop_walkz00,
		BgL_bgl_string1908za700za7za7f1925za7, "Fxop", 4);
	      DEFINE_STRING(BGl_string1909z00zzfxop_walkz00,
		BgL_bgl_string1909za700za7za7f1926za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1910z00zzfxop_walkz00,
		BgL_bgl_string1910za700za7za7f1927za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1911z00zzfxop_walkz00,
		BgL_bgl_string1911za700za7za7f1928za7, " error", 6);
	      DEFINE_STRING(BGl_string1912z00zzfxop_walkz00,
		BgL_bgl_string1912za700za7za7f1929za7, "s", 1);
	      DEFINE_STRING(BGl_string1913z00zzfxop_walkz00,
		BgL_bgl_string1913za700za7za7f1930za7, "", 0);
	      DEFINE_STRING(BGl_string1914z00zzfxop_walkz00,
		BgL_bgl_string1914za700za7za7f1931za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1915z00zzfxop_walkz00,
		BgL_bgl_string1915za700za7za7f1932za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1917z00zzfxop_walkz00,
		BgL_bgl_string1917za700za7za7f1933za7, "fxop!1296", 9);
	      DEFINE_STRING(BGl_string1919z00zzfxop_walkz00,
		BgL_bgl_string1919za700za7za7f1934za7, "fxop!::node", 11);
	      DEFINE_STRING(BGl_string1920z00zzfxop_walkz00,
		BgL_bgl_string1920za700za7za7f1935za7, "fxop_walk", 9);
	      DEFINE_STRING(BGl_string1921z00zzfxop_walkz00,
		BgL_bgl_string1921za700za7za7f1936za7,
		"(($+fx $addfx) ($-fx $subfx) ($<fx $ltfx) ($<=fx $lefx) ($>fx $c-gtfx) ($>=fx $c-gefx) ($=fx $c-egfx)) $long->bint $bint->long foreign (clear-fxop-cache!) pass-started (init-fxop-cache!) ",
		187);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzfxop_walkz00,
		BgL_bgl_za762fxopza7121296za771937za7, BGl_z62fxopz121296z70zzfxop_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1918z00zzfxop_walkz00,
		BgL_bgl_za762fxopza712za7d2let1938za7,
		BGl_z62fxopz12zd2letzd2var1299z70zzfxop_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_clearzd2fxopzd2cachez12zd2envzc0zzfxop_walkz00,
		BgL_bgl_za762clearza7d2fxopza71939za7,
		BGl_z62clearzd2fxopzd2cachez12z70zzfxop_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_GENERIC(BGl_fxopz12zd2envzc0zzfxop_walkz00,
		BgL_bgl_za762fxopza712za770za7za7f1940za7, BGl_z62fxopz12z70zzfxop_walkz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initzd2fxopzd2cachez12zd2envzc0zzfxop_walkz00,
		BgL_bgl_za762initza7d2fxopza7d1941za7,
		BGl_z62initzd2fxopzd2cachez12z70zzfxop_walkz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzfxop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2longzd2ze3bintza2z31zzfxop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2bintzd2ze3longza2z31zzfxop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2fxopsza2z00zzfxop_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzfxop_walkz00(long
		BgL_checksumz00_2675, char *BgL_fromz00_2676)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzfxop_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzfxop_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzfxop_walkz00();
					BGl_libraryzd2moduleszd2initz00zzfxop_walkz00();
					BGl_cnstzd2initzd2zzfxop_walkz00();
					BGl_importedzd2moduleszd2initz00zzfxop_walkz00();
					BGl_genericzd2initzd2zzfxop_walkz00();
					BGl_methodzd2initzd2zzfxop_walkz00();
					return BGl_toplevelzd2initzd2zzfxop_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"fxop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "fxop_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "fxop_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			{	/* Fxop/walk.scm 21 */
				obj_t BgL_cportz00_2543;

				{	/* Fxop/walk.scm 21 */
					obj_t BgL_stringz00_2550;

					BgL_stringz00_2550 = BGl_string1921z00zzfxop_walkz00;
					{	/* Fxop/walk.scm 21 */
						obj_t BgL_startz00_2551;

						BgL_startz00_2551 = BINT(0L);
						{	/* Fxop/walk.scm 21 */
							obj_t BgL_endz00_2552;

							BgL_endz00_2552 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2550)));
							{	/* Fxop/walk.scm 21 */

								BgL_cportz00_2543 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2550, BgL_startz00_2551, BgL_endz00_2552);
				}}}}
				{
					long BgL_iz00_2544;

					BgL_iz00_2544 = 6L;
				BgL_loopz00_2545:
					if ((BgL_iz00_2544 == -1L))
						{	/* Fxop/walk.scm 21 */
							return BUNSPEC;
						}
					else
						{	/* Fxop/walk.scm 21 */
							{	/* Fxop/walk.scm 21 */
								obj_t BgL_arg1923z00_2546;

								{	/* Fxop/walk.scm 21 */

									{	/* Fxop/walk.scm 21 */
										obj_t BgL_locationz00_2548;

										BgL_locationz00_2548 = BBOOL(((bool_t) 0));
										{	/* Fxop/walk.scm 21 */

											BgL_arg1923z00_2546 =
												BGl_readz00zz__readerz00(BgL_cportz00_2543,
												BgL_locationz00_2548);
										}
									}
								}
								{	/* Fxop/walk.scm 21 */
									int BgL_tmpz00_2709;

									BgL_tmpz00_2709 = (int) (BgL_iz00_2544);
									CNST_TABLE_SET(BgL_tmpz00_2709, BgL_arg1923z00_2546);
							}}
							{	/* Fxop/walk.scm 21 */
								int BgL_auxz00_2549;

								BgL_auxz00_2549 = (int) ((BgL_iz00_2544 - 1L));
								{
									long BgL_iz00_2714;

									BgL_iz00_2714 = (long) (BgL_auxz00_2549);
									BgL_iz00_2544 = BgL_iz00_2714;
									goto BgL_loopz00_2545;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			BGl_za2longzd2ze3bintza2z31zzfxop_walkz00 = BUNSPEC;
			BGl_za2bintzd2ze3longza2z31zzfxop_walkz00 = BUNSPEC;
			BGl_za2fxopsza2z00zzfxop_walkz00 = BNIL;
			return BUNSPEC;
		}

	}



/* fxop-walk! */
	BGL_EXPORTED_DEF obj_t BGl_fxopzd2walkz12zc0zzfxop_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Fxop/walk.scm 45 */
			{	/* Fxop/walk.scm 46 */
				obj_t BgL_list1318z00_1479;

				{	/* Fxop/walk.scm 46 */
					obj_t BgL_arg1319z00_1480;

					{	/* Fxop/walk.scm 46 */
						obj_t BgL_arg1320z00_1481;

						BgL_arg1320z00_1481 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1319z00_1480 =
							MAKE_YOUNG_PAIR(BGl_string1908z00zzfxop_walkz00,
							BgL_arg1320z00_1481);
					}
					BgL_list1318z00_1479 =
						MAKE_YOUNG_PAIR(BGl_string1909z00zzfxop_walkz00,
						BgL_arg1319z00_1480);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1318z00_1479);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1908z00zzfxop_walkz00;
			{	/* Fxop/walk.scm 46 */
				obj_t BgL_g1117z00_1482;
				obj_t BgL_g1118z00_1483;

				{	/* Fxop/walk.scm 46 */
					obj_t BgL_list1329z00_1496;

					BgL_list1329z00_1496 =
						MAKE_YOUNG_PAIR(BGl_initzd2fxopzd2cachez12zd2envzc0zzfxop_walkz00,
						BNIL);
					BgL_g1117z00_1482 = BgL_list1329z00_1496;
				}
				BgL_g1118z00_1483 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1485;
					obj_t BgL_hnamesz00_1486;

					BgL_hooksz00_1485 = BgL_g1117z00_1482;
					BgL_hnamesz00_1486 = BgL_g1118z00_1483;
				BgL_zc3z04anonymousza31321ze3z87_1487:
					if (NULLP(BgL_hooksz00_1485))
						{	/* Fxop/walk.scm 46 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Fxop/walk.scm 46 */
							bool_t BgL_test1945z00_2729;

							{	/* Fxop/walk.scm 46 */
								obj_t BgL_fun1328z00_1494;

								BgL_fun1328z00_1494 = CAR(((obj_t) BgL_hooksz00_1485));
								BgL_test1945z00_2729 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1328z00_1494));
							}
							if (BgL_test1945z00_2729)
								{	/* Fxop/walk.scm 46 */
									obj_t BgL_arg1325z00_1491;
									obj_t BgL_arg1326z00_1492;

									BgL_arg1325z00_1491 = CDR(((obj_t) BgL_hooksz00_1485));
									BgL_arg1326z00_1492 = CDR(((obj_t) BgL_hnamesz00_1486));
									{
										obj_t BgL_hnamesz00_2741;
										obj_t BgL_hooksz00_2740;

										BgL_hooksz00_2740 = BgL_arg1325z00_1491;
										BgL_hnamesz00_2741 = BgL_arg1326z00_1492;
										BgL_hnamesz00_1486 = BgL_hnamesz00_2741;
										BgL_hooksz00_1485 = BgL_hooksz00_2740;
										goto BgL_zc3z04anonymousza31321ze3z87_1487;
									}
								}
							else
								{	/* Fxop/walk.scm 46 */
									obj_t BgL_arg1327z00_1493;

									BgL_arg1327z00_1493 = CAR(((obj_t) BgL_hnamesz00_1486));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1908z00zzfxop_walkz00,
										BGl_string1910z00zzfxop_walkz00, BgL_arg1327z00_1493);
								}
						}
				}
			}
			{
				obj_t BgL_l1288z00_1498;

				BgL_l1288z00_1498 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31330ze3z87_1499:
				if (PAIRP(BgL_l1288z00_1498))
					{	/* Fxop/walk.scm 47 */
						BGl_fxopzd2funz12zc0zzfxop_walkz00(CAR(BgL_l1288z00_1498));
						{
							obj_t BgL_l1288z00_2749;

							BgL_l1288z00_2749 = CDR(BgL_l1288z00_1498);
							BgL_l1288z00_1498 = BgL_l1288z00_2749;
							goto BgL_zc3z04anonymousza31330ze3z87_1499;
						}
					}
				else
					{	/* Fxop/walk.scm 47 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Fxop/walk.scm 48 */
					{	/* Fxop/walk.scm 48 */
						obj_t BgL_port1290z00_1506;

						{	/* Fxop/walk.scm 48 */
							obj_t BgL_tmpz00_2754;

							BgL_tmpz00_2754 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1290z00_1506 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2754);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1290z00_1506);
						bgl_display_string(BGl_string1911z00zzfxop_walkz00,
							BgL_port1290z00_1506);
						{	/* Fxop/walk.scm 48 */
							obj_t BgL_arg1335z00_1507;

							{	/* Fxop/walk.scm 48 */
								bool_t BgL_test1948z00_2759;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Fxop/walk.scm 48 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Fxop/walk.scm 48 */
												BgL_test1948z00_2759 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Fxop/walk.scm 48 */
												BgL_test1948z00_2759 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Fxop/walk.scm 48 */
										BgL_test1948z00_2759 = ((bool_t) 0);
									}
								if (BgL_test1948z00_2759)
									{	/* Fxop/walk.scm 48 */
										BgL_arg1335z00_1507 = BGl_string1912z00zzfxop_walkz00;
									}
								else
									{	/* Fxop/walk.scm 48 */
										BgL_arg1335z00_1507 = BGl_string1913z00zzfxop_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1335z00_1507, BgL_port1290z00_1506);
						}
						bgl_display_string(BGl_string1914z00zzfxop_walkz00,
							BgL_port1290z00_1506);
						bgl_display_char(((unsigned char) 10), BgL_port1290z00_1506);
					}
					{	/* Fxop/walk.scm 48 */
						obj_t BgL_list1338z00_1511;

						BgL_list1338z00_1511 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1338z00_1511);
					}
				}
			else
				{	/* Fxop/walk.scm 48 */
					obj_t BgL_g1119z00_1512;
					obj_t BgL_g1120z00_1513;

					{	/* Fxop/walk.scm 48 */
						obj_t BgL_list1352z00_1526;

						BgL_list1352z00_1526 =
							MAKE_YOUNG_PAIR
							(BGl_clearzd2fxopzd2cachez12zd2envzc0zzfxop_walkz00, BNIL);
						BgL_g1119z00_1512 = BgL_list1352z00_1526;
					}
					BgL_g1120z00_1513 = CNST_TABLE_REF(2);
					{
						obj_t BgL_hooksz00_1515;
						obj_t BgL_hnamesz00_1516;

						BgL_hooksz00_1515 = BgL_g1119z00_1512;
						BgL_hnamesz00_1516 = BgL_g1120z00_1513;
					BgL_zc3z04anonymousza31339ze3z87_1517:
						if (NULLP(BgL_hooksz00_1515))
							{	/* Fxop/walk.scm 48 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Fxop/walk.scm 48 */
								bool_t BgL_test1952z00_2778;

								{	/* Fxop/walk.scm 48 */
									obj_t BgL_fun1351z00_1524;

									BgL_fun1351z00_1524 = CAR(((obj_t) BgL_hooksz00_1515));
									BgL_test1952z00_2778 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1351z00_1524));
								}
								if (BgL_test1952z00_2778)
									{	/* Fxop/walk.scm 48 */
										obj_t BgL_arg1346z00_1521;
										obj_t BgL_arg1348z00_1522;

										BgL_arg1346z00_1521 = CDR(((obj_t) BgL_hooksz00_1515));
										BgL_arg1348z00_1522 = CDR(((obj_t) BgL_hnamesz00_1516));
										{
											obj_t BgL_hnamesz00_2790;
											obj_t BgL_hooksz00_2789;

											BgL_hooksz00_2789 = BgL_arg1346z00_1521;
											BgL_hnamesz00_2790 = BgL_arg1348z00_1522;
											BgL_hnamesz00_1516 = BgL_hnamesz00_2790;
											BgL_hooksz00_1515 = BgL_hooksz00_2789;
											goto BgL_zc3z04anonymousza31339ze3z87_1517;
										}
									}
								else
									{	/* Fxop/walk.scm 48 */
										obj_t BgL_arg1349z00_1523;

										BgL_arg1349z00_1523 = CAR(((obj_t) BgL_hnamesz00_1516));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1915z00zzfxop_walkz00, BgL_arg1349z00_1523);
									}
							}
					}
				}
		}

	}



/* &fxop-walk! */
	obj_t BGl_z62fxopzd2walkz12za2zzfxop_walkz00(obj_t BgL_envz00_2530,
		obj_t BgL_globalsz00_2531)
	{
		{	/* Fxop/walk.scm 45 */
			return BGl_fxopzd2walkz12zc0zzfxop_walkz00(BgL_globalsz00_2531);
		}

	}



/* init-fxop-cache! */
	BGL_EXPORTED_DEF obj_t BGl_initzd2fxopzd2cachez12z12zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 60 */
			if (PAIRP(BGl_za2fxopsza2z00zzfxop_walkz00))
				{	/* Fxop/walk.scm 61 */
					BFALSE;
				}
			else
				{	/* Fxop/walk.scm 61 */
					{	/* Fxop/walk.scm 62 */
						obj_t BgL_list1355z00_1528;

						BgL_list1355z00_1528 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
						BGl_za2bintzd2ze3longza2z31zzfxop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(4),
							BgL_list1355z00_1528);
					}
					{	/* Fxop/walk.scm 63 */
						obj_t BgL_list1356z00_1529;

						BgL_list1356z00_1529 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
						BGl_za2longzd2ze3bintza2z31zzfxop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(5),
							BgL_list1356z00_1529);
					}
					{	/* Fxop/walk.scm 65 */
						obj_t BgL_l1291z00_1530;

						BgL_l1291z00_1530 = CNST_TABLE_REF(6);
						{	/* Fxop/walk.scm 65 */
							obj_t BgL_head1293z00_1532;

							BgL_head1293z00_1532 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1291z00_1534;
								obj_t BgL_tail1294z00_1535;

								BgL_l1291z00_1534 = BgL_l1291z00_1530;
								BgL_tail1294z00_1535 = BgL_head1293z00_1532;
							BgL_zc3z04anonymousza31358ze3z87_1536:
								if (NULLP(BgL_l1291z00_1534))
									{	/* Fxop/walk.scm 65 */
										BGl_za2fxopsza2z00zzfxop_walkz00 =
											CDR(BgL_head1293z00_1532);
									}
								else
									{	/* Fxop/walk.scm 65 */
										obj_t BgL_newtail1295z00_1538;

										{	/* Fxop/walk.scm 65 */
											obj_t BgL_arg1364z00_1540;

											{	/* Fxop/walk.scm 65 */
												obj_t BgL_utz00_1541;

												BgL_utz00_1541 = CAR(((obj_t) BgL_l1291z00_1534));
												{	/* Fxop/walk.scm 66 */
													obj_t BgL_arg1367z00_1542;
													obj_t BgL_arg1370z00_1543;

													{	/* Fxop/walk.scm 66 */
														obj_t BgL_arg1371z00_1544;

														BgL_arg1371z00_1544 = CAR(((obj_t) BgL_utz00_1541));
														{	/* Fxop/walk.scm 66 */
															obj_t BgL_list1372z00_1545;

															BgL_list1372z00_1545 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
															BgL_arg1367z00_1542 =
																BGl_findzd2globalzd2zzast_envz00
																(BgL_arg1371z00_1544, BgL_list1372z00_1545);
														}
													}
													{	/* Fxop/walk.scm 67 */
														obj_t BgL_arg1375z00_1546;

														{	/* Fxop/walk.scm 67 */
															obj_t BgL_pairz00_1992;

															BgL_pairz00_1992 = CDR(((obj_t) BgL_utz00_1541));
															BgL_arg1375z00_1546 = CAR(BgL_pairz00_1992);
														}
														{	/* Fxop/walk.scm 67 */
															obj_t BgL_list1376z00_1547;

															BgL_list1376z00_1547 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
															BgL_arg1370z00_1543 =
																BGl_findzd2globalzd2zzast_envz00
																(BgL_arg1375z00_1546, BgL_list1376z00_1547);
														}
													}
													BgL_arg1364z00_1540 =
														MAKE_YOUNG_PAIR(BgL_arg1367z00_1542,
														BgL_arg1370z00_1543);
												}
											}
											BgL_newtail1295z00_1538 =
												MAKE_YOUNG_PAIR(BgL_arg1364z00_1540, BNIL);
										}
										SET_CDR(BgL_tail1294z00_1535, BgL_newtail1295z00_1538);
										{	/* Fxop/walk.scm 65 */
											obj_t BgL_arg1361z00_1539;

											BgL_arg1361z00_1539 = CDR(((obj_t) BgL_l1291z00_1534));
											{
												obj_t BgL_tail1294z00_2829;
												obj_t BgL_l1291z00_2828;

												BgL_l1291z00_2828 = BgL_arg1361z00_1539;
												BgL_tail1294z00_2829 = BgL_newtail1295z00_1538;
												BgL_tail1294z00_1535 = BgL_tail1294z00_2829;
												BgL_l1291z00_1534 = BgL_l1291z00_2828;
												goto BgL_zc3z04anonymousza31358ze3z87_1536;
											}
										}
									}
							}
						}
					}
				}
			return BUNSPEC;
		}

	}



/* &init-fxop-cache! */
	obj_t BGl_z62initzd2fxopzd2cachez12z70zzfxop_walkz00(obj_t BgL_envz00_2532)
	{
		{	/* Fxop/walk.scm 60 */
			return BGl_initzd2fxopzd2cachez12z12zzfxop_walkz00();
		}

	}



/* &clear-fxop-cache! */
	obj_t BGl_z62clearzd2fxopzd2cachez12z70zzfxop_walkz00(obj_t BgL_envz00_2533)
	{
		{	/* Fxop/walk.scm 80 */
			BGl_za2longzd2ze3bintza2z31zzfxop_walkz00 = BFALSE;
			BGl_za2bintzd2ze3longza2z31zzfxop_walkz00 = BFALSE;
			return (BGl_za2fxopsza2z00zzfxop_walkz00 = BNIL, BUNSPEC);
		}

	}



/* fxop-fun! */
	obj_t BGl_fxopzd2funz12zc0zzfxop_walkz00(obj_t BgL_varz00_18)
	{
		{	/* Fxop/walk.scm 88 */
			{	/* Fxop/walk.scm 89 */
				obj_t BgL_arg1377z00_1549;

				BgL_arg1377z00_1549 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1377z00_1549);
			}
			{	/* Fxop/walk.scm 90 */
				BgL_valuez00_bglt BgL_funz00_1550;

				BgL_funz00_1550 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_valuez00);
				{	/* Fxop/walk.scm 91 */
					BgL_nodez00_bglt BgL_arg1378z00_1551;

					{	/* Fxop/walk.scm 91 */
						obj_t BgL_arg1379z00_1552;

						BgL_arg1379z00_1552 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1550)))->BgL_bodyz00);
						BgL_arg1378z00_1551 =
							BGl_fxopz12z12zzfxop_walkz00(
							((BgL_nodez00_bglt) BgL_arg1379z00_1552));
					}
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1550)))->BgL_bodyz00) =
						((obj_t) ((obj_t) BgL_arg1378z00_1551)), BUNSPEC);
				}
				BGl_leavezd2functionzd2zztools_errorz00();
				return BgL_varz00_18;
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_fxopz12zd2envzc0zzfxop_walkz00, BGl_proc1916z00zzfxop_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string1917z00zzfxop_walkz00);
		}

	}



/* &fxop!1296 */
	obj_t BGl_z62fxopz121296z70zzfxop_walkz00(obj_t BgL_envz00_2537,
		obj_t BgL_nodez00_2538)
	{
		{	/* Fxop/walk.scm 98 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_2538),
					BGl_fxopz12zd2envzc0zzfxop_walkz00));
		}

	}



/* fxop! */
	BgL_nodez00_bglt BGl_fxopz12z12zzfxop_walkz00(BgL_nodez00_bglt BgL_nodez00_19)
	{
		{	/* Fxop/walk.scm 98 */
			{	/* Fxop/walk.scm 98 */
				obj_t BgL_method1297z00_1568;

				{	/* Fxop/walk.scm 98 */
					obj_t BgL_res1893z00_2030;

					{	/* Fxop/walk.scm 98 */
						long BgL_objzd2classzd2numz00_2001;

						BgL_objzd2classzd2numz00_2001 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_19));
						{	/* Fxop/walk.scm 98 */
							obj_t BgL_arg1811z00_2002;

							BgL_arg1811z00_2002 =
								PROCEDURE_REF(BGl_fxopz12zd2envzc0zzfxop_walkz00, (int) (1L));
							{	/* Fxop/walk.scm 98 */
								int BgL_offsetz00_2005;

								BgL_offsetz00_2005 = (int) (BgL_objzd2classzd2numz00_2001);
								{	/* Fxop/walk.scm 98 */
									long BgL_offsetz00_2006;

									BgL_offsetz00_2006 =
										((long) (BgL_offsetz00_2005) - OBJECT_TYPE);
									{	/* Fxop/walk.scm 98 */
										long BgL_modz00_2007;

										BgL_modz00_2007 =
											(BgL_offsetz00_2006 >> (int) ((long) ((int) (4L))));
										{	/* Fxop/walk.scm 98 */
											long BgL_restz00_2009;

											BgL_restz00_2009 =
												(BgL_offsetz00_2006 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Fxop/walk.scm 98 */

												{	/* Fxop/walk.scm 98 */
													obj_t BgL_bucketz00_2011;

													BgL_bucketz00_2011 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2002), BgL_modz00_2007);
													BgL_res1893z00_2030 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2011), BgL_restz00_2009);
					}}}}}}}}
					BgL_method1297z00_1568 = BgL_res1893z00_2030;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1297z00_1568,
						((obj_t) BgL_nodez00_19)));
			}
		}

	}



/* &fxop! */
	BgL_nodez00_bglt BGl_z62fxopz12z70zzfxop_walkz00(obj_t BgL_envz00_2535,
		obj_t BgL_nodez00_2536)
	{
		{	/* Fxop/walk.scm 98 */
			return
				BGl_fxopz12z12zzfxop_walkz00(((BgL_nodez00_bglt) BgL_nodez00_2536));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fxopz12zd2envzc0zzfxop_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1918z00zzfxop_walkz00, BGl_string1919z00zzfxop_walkz00);
		}

	}



/* &fxop!-let-var1299 */
	BgL_nodez00_bglt BGl_z62fxopz12zd2letzd2var1299z70zzfxop_walkz00(obj_t
		BgL_envz00_2540, obj_t BgL_nodez00_2541)
	{
		{	/* Fxop/walk.scm 104 */
			{
				obj_t BgL_opz00_2649;
				BgL_letzd2varzd2_bglt BgL_nodez00_2650;
				obj_t BgL_opz00_2579;
				BgL_letzd2varzd2_bglt BgL_nodez00_2580;
				obj_t BgL_opz00_2559;
				BgL_letzd2varzd2_bglt BgL_nodez00_2560;

				{	/* Fxop/walk.scm 220 */
					obj_t BgL_g1162z00_2669;

					{	/* Fxop/walk.scm 220 */
						bool_t BgL_test1955z00_2882;

						{	/* Fxop/walk.scm 220 */
							BgL_typez00_bglt BgL_arg1454z00_2670;

							BgL_arg1454z00_2670 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2541))))->
								BgL_typez00);
							BgL_test1955z00_2882 =
								(((obj_t) BgL_arg1454z00_2670) ==
								BGl_za2bintza2z00zztype_cachez00);
						}
						if (BgL_test1955z00_2882)
							{	/* Fxop/walk.scm 220 */
								BgL_g1162z00_2669 =
									BGl_fxopzd2doublezd2intzd2letvarzf3ze70zc6zzfxop_walkz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2541)))->
										BgL_bindingsz00),
									(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
													BgL_nodez00_2541)))->BgL_bodyz00));
							}
						else
							{	/* Fxop/walk.scm 220 */
								BgL_g1162z00_2669 = BFALSE;
							}
					}
					if (CBOOL(BgL_g1162z00_2669))
						{
							BgL_appz00_bglt BgL_auxz00_2895;

							BgL_opz00_2559 = BgL_g1162z00_2669;
							BgL_nodez00_2560 = ((BgL_letzd2varzd2_bglt) BgL_nodez00_2541);
							{	/* Fxop/walk.scm 177 */
								BgL_appz00_bglt BgL_new1145z00_2561;

								{	/* Fxop/walk.scm 177 */
									BgL_appz00_bglt BgL_new1144z00_2562;

									BgL_new1144z00_2562 =
										((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_appz00_bgl))));
									{	/* Fxop/walk.scm 177 */
										long BgL_arg1629z00_2563;

										BgL_arg1629z00_2563 =
											BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1144z00_2562),
											BgL_arg1629z00_2563);
									}
									{	/* Fxop/walk.scm 177 */
										BgL_objectz00_bglt BgL_tmpz00_2900;

										BgL_tmpz00_2900 =
											((BgL_objectz00_bglt) BgL_new1144z00_2562);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2900, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1144z00_2562);
									BgL_new1145z00_2561 = BgL_new1144z00_2562;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1145z00_2561)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1145z00_2561)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2bintza2z00zztype_cachez00)), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1145z00_2561)))->BgL_sidezd2effectzd2) =
									((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1145z00_2561)))->BgL_keyz00) =
									((obj_t) BINT(-1L)), BUNSPEC);
								{
									BgL_varz00_bglt BgL_auxz00_2914;

									{	/* Fxop/walk.scm 179 */
										BgL_refz00_bglt BgL_new1147z00_2564;

										{	/* Fxop/walk.scm 179 */
											BgL_refz00_bglt BgL_new1146z00_2565;

											BgL_new1146z00_2565 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Fxop/walk.scm 179 */
												long BgL_arg1609z00_2566;

												{	/* Fxop/walk.scm 179 */
													obj_t BgL_classz00_2567;

													BgL_classz00_2567 = BGl_refz00zzast_nodez00;
													BgL_arg1609z00_2566 =
														BGL_CLASS_NUM(BgL_classz00_2567);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1146z00_2565),
													BgL_arg1609z00_2566);
											}
											{	/* Fxop/walk.scm 179 */
												BgL_objectz00_bglt BgL_tmpz00_2919;

												BgL_tmpz00_2919 =
													((BgL_objectz00_bglt) BgL_new1146z00_2565);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2919, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1146z00_2565);
											BgL_new1147z00_2564 = BgL_new1146z00_2565;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1147z00_2564)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1147z00_2564)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2bintza2z00zztype_cachez00)), BUNSPEC);
										((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_new1147z00_2564)))->BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BgL_opz00_2559)), BUNSPEC);
										BgL_auxz00_2914 = ((BgL_varz00_bglt) BgL_new1147z00_2564);
									}
									((((BgL_appz00_bglt) COBJECT(BgL_new1145z00_2561))->
											BgL_funz00) =
										((BgL_varz00_bglt) BgL_auxz00_2914), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_2933;

									{	/* Fxop/walk.scm 183 */
										BgL_nodez00_bglt BgL_arg1611z00_2568;
										BgL_nodez00_bglt BgL_arg1613z00_2569;

										{	/* Fxop/walk.scm 183 */
											obj_t BgL_arg1616z00_2570;

											{	/* Fxop/walk.scm 183 */
												obj_t BgL_pairz00_2571;

												BgL_pairz00_2571 =
													(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2560))->
													BgL_bindingsz00);
												{	/* Fxop/walk.scm 183 */
													obj_t BgL_pairz00_2572;

													BgL_pairz00_2572 = CAR(BgL_pairz00_2571);
													BgL_arg1616z00_2570 = CDR(BgL_pairz00_2572);
											}}
											BgL_arg1611z00_2568 =
												BGl_bintze70ze7zzfxop_walkz00(
												((BgL_nodez00_bglt) BgL_arg1616z00_2570));
										}
										{	/* Fxop/walk.scm 184 */
											obj_t BgL_arg1626z00_2573;

											{	/* Fxop/walk.scm 184 */
												obj_t BgL_pairz00_2574;

												BgL_pairz00_2574 =
													(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2560))->
													BgL_bindingsz00);
												{	/* Fxop/walk.scm 184 */
													obj_t BgL_pairz00_2575;

													{	/* Fxop/walk.scm 184 */
														obj_t BgL_pairz00_2576;

														BgL_pairz00_2576 = CDR(BgL_pairz00_2574);
														BgL_pairz00_2575 = CAR(BgL_pairz00_2576);
													}
													BgL_arg1626z00_2573 = CDR(BgL_pairz00_2575);
											}}
											BgL_arg1613z00_2569 =
												BGl_bintze70ze7zzfxop_walkz00(
												((BgL_nodez00_bglt) BgL_arg1626z00_2573));
										}
										{	/* Fxop/walk.scm 182 */
											obj_t BgL_list1614z00_2577;

											{	/* Fxop/walk.scm 182 */
												obj_t BgL_arg1615z00_2578;

												BgL_arg1615z00_2578 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1613z00_2569), BNIL);
												BgL_list1614z00_2577 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg1611z00_2568), BgL_arg1615z00_2578);
											}
											BgL_auxz00_2933 = BgL_list1614z00_2577;
									}}
									((((BgL_appz00_bglt) COBJECT(BgL_new1145z00_2561))->
											BgL_argsz00) = ((obj_t) BgL_auxz00_2933), BUNSPEC);
								}
								((((BgL_appz00_bglt) COBJECT(BgL_new1145z00_2561))->
										BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
								BgL_auxz00_2895 = BgL_new1145z00_2561;
							}
							return ((BgL_nodez00_bglt) BgL_auxz00_2895);
						}
					else
						{	/* Fxop/walk.scm 224 */
							obj_t BgL_g1168z00_2671;

							{	/* Fxop/walk.scm 224 */
								bool_t BgL_test1957z00_2953;

								{	/* Fxop/walk.scm 224 */
									BgL_typez00_bglt BgL_arg1437z00_2672;

									BgL_arg1437z00_2672 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2541))))->
										BgL_typez00);
									BgL_test1957z00_2953 =
										(((obj_t) BgL_arg1437z00_2672) ==
										BGl_za2bintza2z00zztype_cachez00);
								}
								if (BgL_test1957z00_2953)
									{	/* Fxop/walk.scm 224 */
										BgL_g1168z00_2671 =
											BGl_fxopzd2simplezd2intzd2letvarzf3ze70zc6zzfxop_walkz00(
											(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2541)))->
												BgL_bindingsz00),
											(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
															BgL_nodez00_2541)))->BgL_bodyz00));
									}
								else
									{	/* Fxop/walk.scm 224 */
										BgL_g1168z00_2671 = BFALSE;
									}
							}
							if (CBOOL(BgL_g1168z00_2671))
								{
									BgL_appz00_bglt BgL_auxz00_2966;

									BgL_opz00_2579 = BgL_g1168z00_2671;
									BgL_nodez00_2580 = ((BgL_letzd2varzd2_bglt) BgL_nodez00_2541);
									{	/* Fxop/walk.scm 188 */
										obj_t BgL_callz00_2581;

										{	/* Fxop/walk.scm 188 */
											obj_t BgL_pairz00_2582;

											BgL_pairz00_2582 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt)
															(((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_nodez00_2580))->BgL_bodyz00))))->
												BgL_argsz00);
											BgL_callz00_2581 = CAR(BgL_pairz00_2582);
										}
										{	/* Fxop/walk.scm 190 */
											BgL_appz00_bglt BgL_new1151z00_2583;

											{	/* Fxop/walk.scm 190 */
												BgL_appz00_bglt BgL_new1150z00_2584;

												BgL_new1150z00_2584 =
													((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_appz00_bgl))));
												{	/* Fxop/walk.scm 190 */
													long BgL_arg1754z00_2585;

													BgL_arg1754z00_2585 =
														BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1150z00_2584),
														BgL_arg1754z00_2585);
												}
												{	/* Fxop/walk.scm 190 */
													BgL_objectz00_bglt BgL_tmpz00_2975;

													BgL_tmpz00_2975 =
														((BgL_objectz00_bglt) BgL_new1150z00_2584);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2975, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1150z00_2584);
												BgL_new1151z00_2583 = BgL_new1150z00_2584;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1151z00_2583)))->
													BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1151z00_2583)))->BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BGl_za2bintza2z00zztype_cachez00)), BUNSPEC);
											((((BgL_nodezf2effectzf2_bglt)
														COBJECT(((BgL_nodezf2effectzf2_bglt)
																BgL_new1151z00_2583)))->BgL_sidezd2effectzd2) =
												((obj_t) BUNSPEC), BUNSPEC);
											((((BgL_nodezf2effectzf2_bglt)
														COBJECT(((BgL_nodezf2effectzf2_bglt)
																BgL_new1151z00_2583)))->BgL_keyz00) =
												((obj_t) BINT(-1L)), BUNSPEC);
											{
												BgL_varz00_bglt BgL_auxz00_2989;

												{	/* Fxop/walk.scm 192 */
													BgL_refz00_bglt BgL_new1153z00_2586;

													{	/* Fxop/walk.scm 192 */
														BgL_refz00_bglt BgL_new1152z00_2587;

														BgL_new1152z00_2587 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Fxop/walk.scm 192 */
															long BgL_arg1642z00_2588;

															{	/* Fxop/walk.scm 192 */
																obj_t BgL_classz00_2589;

																BgL_classz00_2589 = BGl_refz00zzast_nodez00;
																BgL_arg1642z00_2588 =
																	BGL_CLASS_NUM(BgL_classz00_2589);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1152z00_2587),
																BgL_arg1642z00_2588);
														}
														{	/* Fxop/walk.scm 192 */
															BgL_objectz00_bglt BgL_tmpz00_2994;

															BgL_tmpz00_2994 =
																((BgL_objectz00_bglt) BgL_new1152z00_2587);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2994, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1152z00_2587);
														BgL_new1153z00_2586 = BgL_new1152z00_2587;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1153z00_2586)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1153z00_2586)))->BgL_typez00) =
														((BgL_typez00_bglt) ((BgL_typez00_bglt)
																BGl_za2bintza2z00zztype_cachez00)), BUNSPEC);
													((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																		BgL_new1153z00_2586)))->BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_opz00_2579)), BUNSPEC);
													BgL_auxz00_2989 =
														((BgL_varz00_bglt) BgL_new1153z00_2586);
												}
												((((BgL_appz00_bglt) COBJECT(BgL_new1151z00_2583))->
														BgL_funz00) =
													((BgL_varz00_bglt) BgL_auxz00_2989), BUNSPEC);
											}
											{
												obj_t BgL_auxz00_3008;

												{	/* Fxop/walk.scm 196 */
													BgL_nodez00_bglt BgL_arg1646z00_2590;
													BgL_nodez00_bglt BgL_arg1650z00_2591;

													{	/* Fxop/walk.scm 196 */
														bool_t BgL_test1959z00_3009;

														{	/* Fxop/walk.scm 196 */
															bool_t BgL_test1960z00_3010;

															{	/* Fxop/walk.scm 196 */
																obj_t BgL_arg1711z00_2592;

																{	/* Fxop/walk.scm 196 */
																	obj_t BgL_pairz00_2593;

																	BgL_pairz00_2593 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_callz00_2581)))->
																		BgL_argsz00);
																	BgL_arg1711z00_2592 = CAR(BgL_pairz00_2593);
																}
																{	/* Fxop/walk.scm 196 */
																	obj_t BgL_classz00_2594;

																	BgL_classz00_2594 = BGl_varz00zzast_nodez00;
																	if (BGL_OBJECTP(BgL_arg1711z00_2592))
																		{	/* Fxop/walk.scm 196 */
																			BgL_objectz00_bglt BgL_arg1807z00_2595;

																			BgL_arg1807z00_2595 =
																				(BgL_objectz00_bglt)
																				(BgL_arg1711z00_2592);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Fxop/walk.scm 196 */
																					long BgL_idxz00_2596;

																					BgL_idxz00_2596 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2595);
																					{	/* Fxop/walk.scm 196 */
																						obj_t BgL_arg1800z00_2597;

																						{	/* Fxop/walk.scm 196 */
																							long BgL_arg1801z00_2598;

																							BgL_arg1801z00_2598 =
																								(BgL_idxz00_2596 + 2L);
																							{	/* Fxop/walk.scm 196 */
																								obj_t BgL_vectorz00_2599;

																								BgL_vectorz00_2599 =
																									BGl_za2inheritancesza2z00zz__objectz00;
																								BgL_arg1800z00_2597 =
																									VECTOR_REF(BgL_vectorz00_2599,
																									BgL_arg1801z00_2598);
																						}}
																						BgL_test1960z00_3010 =
																							(BgL_arg1800z00_2597 ==
																							BgL_classz00_2594);
																				}}
																			else
																				{	/* Fxop/walk.scm 196 */
																					bool_t BgL_res1902z00_2600;

																					{	/* Fxop/walk.scm 196 */
																						obj_t BgL_oclassz00_2601;

																						{	/* Fxop/walk.scm 196 */
																							obj_t BgL_arg1815z00_2602;
																							long BgL_arg1816z00_2603;

																							BgL_arg1815z00_2602 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Fxop/walk.scm 196 */
																								long BgL_arg1817z00_2604;

																								BgL_arg1817z00_2604 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2595);
																								BgL_arg1816z00_2603 =
																									(BgL_arg1817z00_2604 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2601 =
																								VECTOR_REF(BgL_arg1815z00_2602,
																								BgL_arg1816z00_2603);
																						}
																						{	/* Fxop/walk.scm 196 */
																							bool_t BgL__ortest_1115z00_2605;

																							BgL__ortest_1115z00_2605 =
																								(BgL_classz00_2594 ==
																								BgL_oclassz00_2601);
																							if (BgL__ortest_1115z00_2605)
																								{	/* Fxop/walk.scm 196 */
																									BgL_res1902z00_2600 =
																										BgL__ortest_1115z00_2605;
																								}
																							else
																								{	/* Fxop/walk.scm 196 */
																									long BgL_odepthz00_2606;

																									{	/* Fxop/walk.scm 196 */
																										obj_t BgL_arg1804z00_2607;

																										BgL_arg1804z00_2607 =
																											(BgL_oclassz00_2601);
																										BgL_odepthz00_2606 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2607);
																									}
																									if ((2L < BgL_odepthz00_2606))
																										{	/* Fxop/walk.scm 196 */
																											obj_t BgL_arg1802z00_2608;

																											{	/* Fxop/walk.scm 196 */
																												obj_t
																													BgL_arg1803z00_2609;
																												BgL_arg1803z00_2609 =
																													(BgL_oclassz00_2601);
																												BgL_arg1802z00_2608 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2609,
																													2L);
																											}
																											BgL_res1902z00_2600 =
																												(BgL_arg1802z00_2608 ==
																												BgL_classz00_2594);
																										}
																									else
																										{	/* Fxop/walk.scm 196 */
																											BgL_res1902z00_2600 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1960z00_3010 =
																						BgL_res1902z00_2600;
																				}
																		}
																	else
																		{	/* Fxop/walk.scm 196 */
																			BgL_test1960z00_3010 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test1960z00_3010)
																{	/* Fxop/walk.scm 197 */
																	obj_t BgL_auxz00_3045;
																	obj_t BgL_tmpz00_3036;

																	{	/* Fxop/walk.scm 198 */
																		obj_t BgL_pairz00_2611;

																		BgL_pairz00_2611 =
																			(((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_nodez00_2580))->
																			BgL_bindingsz00);
																		{	/* Fxop/walk.scm 198 */
																			obj_t BgL_pairz00_2612;

																			BgL_pairz00_2612 = CAR(BgL_pairz00_2611);
																			BgL_auxz00_3045 = CAR(BgL_pairz00_2612);
																		}
																	}
																	{
																		BgL_variablez00_bglt BgL_auxz00_3037;

																		{
																			BgL_varz00_bglt BgL_auxz00_3038;

																			{	/* Fxop/walk.scm 197 */
																				obj_t BgL_pairz00_2610;

																				BgL_pairz00_2610 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								BgL_callz00_2581)))->
																					BgL_argsz00);
																				BgL_auxz00_3038 =
																					((BgL_varz00_bglt)
																					CAR(BgL_pairz00_2610));
																			}
																			BgL_auxz00_3037 =
																				(((BgL_varz00_bglt)
																					COBJECT(BgL_auxz00_3038))->
																				BgL_variablez00);
																		}
																		BgL_tmpz00_3036 = ((obj_t) BgL_auxz00_3037);
																	}
																	BgL_test1959z00_3009 =
																		(BgL_tmpz00_3036 == BgL_auxz00_3045);
																}
															else
																{	/* Fxop/walk.scm 196 */
																	BgL_test1959z00_3009 = ((bool_t) 0);
																}
														}
														if (BgL_test1959z00_3009)
															{	/* Fxop/walk.scm 199 */
																obj_t BgL_arg1699z00_2613;

																{	/* Fxop/walk.scm 199 */
																	obj_t BgL_pairz00_2614;

																	BgL_pairz00_2614 =
																		(((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_nodez00_2580))->
																		BgL_bindingsz00);
																	{	/* Fxop/walk.scm 199 */
																		obj_t BgL_pairz00_2615;

																		BgL_pairz00_2615 = CAR(BgL_pairz00_2614);
																		BgL_arg1699z00_2613 = CDR(BgL_pairz00_2615);
																	}
																}
																BgL_arg1646z00_2590 =
																	BGl_bintze70ze7zzfxop_walkz00(
																	((BgL_nodez00_bglt) BgL_arg1699z00_2613));
															}
														else
															{	/* Fxop/walk.scm 200 */
																obj_t BgL_arg1701z00_2616;

																{	/* Fxop/walk.scm 200 */
																	obj_t BgL_pairz00_2617;

																	BgL_pairz00_2617 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_callz00_2581)))->
																		BgL_argsz00);
																	BgL_arg1701z00_2616 = CAR(BgL_pairz00_2617);
																}
																BgL_arg1646z00_2590 =
																	BGl_bintze70ze7zzfxop_walkz00(
																	((BgL_nodez00_bglt) BgL_arg1701z00_2616));
															}
													}
													{	/* Fxop/walk.scm 201 */
														bool_t BgL_test1965z00_3060;

														{	/* Fxop/walk.scm 201 */
															bool_t BgL_test1966z00_3061;

															{	/* Fxop/walk.scm 201 */
																obj_t BgL_arg1752z00_2618;

																{	/* Fxop/walk.scm 201 */
																	obj_t BgL_pairz00_2619;

																	BgL_pairz00_2619 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_callz00_2581)))->
																		BgL_argsz00);
																	{	/* Fxop/walk.scm 201 */
																		obj_t BgL_pairz00_2620;

																		BgL_pairz00_2620 = CDR(BgL_pairz00_2619);
																		BgL_arg1752z00_2618 = CAR(BgL_pairz00_2620);
																	}
																}
																{	/* Fxop/walk.scm 201 */
																	obj_t BgL_classz00_2621;

																	BgL_classz00_2621 = BGl_varz00zzast_nodez00;
																	if (BGL_OBJECTP(BgL_arg1752z00_2618))
																		{	/* Fxop/walk.scm 201 */
																			BgL_objectz00_bglt BgL_arg1807z00_2622;

																			BgL_arg1807z00_2622 =
																				(BgL_objectz00_bglt)
																				(BgL_arg1752z00_2618);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Fxop/walk.scm 201 */
																					long BgL_idxz00_2623;

																					BgL_idxz00_2623 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2622);
																					{	/* Fxop/walk.scm 201 */
																						obj_t BgL_arg1800z00_2624;

																						{	/* Fxop/walk.scm 201 */
																							long BgL_arg1801z00_2625;

																							BgL_arg1801z00_2625 =
																								(BgL_idxz00_2623 + 2L);
																							{	/* Fxop/walk.scm 201 */
																								obj_t BgL_vectorz00_2626;

																								BgL_vectorz00_2626 =
																									BGl_za2inheritancesza2z00zz__objectz00;
																								BgL_arg1800z00_2624 =
																									VECTOR_REF(BgL_vectorz00_2626,
																									BgL_arg1801z00_2625);
																						}}
																						BgL_test1966z00_3061 =
																							(BgL_arg1800z00_2624 ==
																							BgL_classz00_2621);
																				}}
																			else
																				{	/* Fxop/walk.scm 201 */
																					bool_t BgL_res1903z00_2627;

																					{	/* Fxop/walk.scm 201 */
																						obj_t BgL_oclassz00_2628;

																						{	/* Fxop/walk.scm 201 */
																							obj_t BgL_arg1815z00_2629;
																							long BgL_arg1816z00_2630;

																							BgL_arg1815z00_2629 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Fxop/walk.scm 201 */
																								long BgL_arg1817z00_2631;

																								BgL_arg1817z00_2631 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2622);
																								BgL_arg1816z00_2630 =
																									(BgL_arg1817z00_2631 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2628 =
																								VECTOR_REF(BgL_arg1815z00_2629,
																								BgL_arg1816z00_2630);
																						}
																						{	/* Fxop/walk.scm 201 */
																							bool_t BgL__ortest_1115z00_2632;

																							BgL__ortest_1115z00_2632 =
																								(BgL_classz00_2621 ==
																								BgL_oclassz00_2628);
																							if (BgL__ortest_1115z00_2632)
																								{	/* Fxop/walk.scm 201 */
																									BgL_res1903z00_2627 =
																										BgL__ortest_1115z00_2632;
																								}
																							else
																								{	/* Fxop/walk.scm 201 */
																									long BgL_odepthz00_2633;

																									{	/* Fxop/walk.scm 201 */
																										obj_t BgL_arg1804z00_2634;

																										BgL_arg1804z00_2634 =
																											(BgL_oclassz00_2628);
																										BgL_odepthz00_2633 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2634);
																									}
																									if ((2L < BgL_odepthz00_2633))
																										{	/* Fxop/walk.scm 201 */
																											obj_t BgL_arg1802z00_2635;

																											{	/* Fxop/walk.scm 201 */
																												obj_t
																													BgL_arg1803z00_2636;
																												BgL_arg1803z00_2636 =
																													(BgL_oclassz00_2628);
																												BgL_arg1802z00_2635 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2636,
																													2L);
																											}
																											BgL_res1903z00_2627 =
																												(BgL_arg1802z00_2635 ==
																												BgL_classz00_2621);
																										}
																									else
																										{	/* Fxop/walk.scm 201 */
																											BgL_res1903z00_2627 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1966z00_3061 =
																						BgL_res1903z00_2627;
																				}
																		}
																	else
																		{	/* Fxop/walk.scm 201 */
																			BgL_test1966z00_3061 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test1966z00_3061)
																{	/* Fxop/walk.scm 202 */
																	obj_t BgL_auxz00_3099;
																	obj_t BgL_tmpz00_3088;

																	{	/* Fxop/walk.scm 203 */
																		obj_t BgL_pairz00_2639;

																		BgL_pairz00_2639 =
																			(((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_nodez00_2580))->
																			BgL_bindingsz00);
																		{	/* Fxop/walk.scm 203 */
																			obj_t BgL_pairz00_2640;

																			BgL_pairz00_2640 = CAR(BgL_pairz00_2639);
																			BgL_auxz00_3099 = CAR(BgL_pairz00_2640);
																		}
																	}
																	{
																		BgL_variablez00_bglt BgL_auxz00_3089;

																		{
																			BgL_varz00_bglt BgL_auxz00_3090;

																			{
																				obj_t BgL_auxz00_3091;

																				{	/* Fxop/walk.scm 202 */
																					obj_t BgL_pairz00_2637;

																					BgL_pairz00_2637 =
																						(((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_callz00_2581)))->
																						BgL_argsz00);
																					{	/* Fxop/walk.scm 202 */
																						obj_t BgL_pairz00_2638;

																						BgL_pairz00_2638 =
																							CDR(BgL_pairz00_2637);
																						BgL_auxz00_3091 =
																							CAR(BgL_pairz00_2638);
																					}
																				}
																				BgL_auxz00_3090 =
																					((BgL_varz00_bglt) BgL_auxz00_3091);
																			}
																			BgL_auxz00_3089 =
																				(((BgL_varz00_bglt)
																					COBJECT(BgL_auxz00_3090))->
																				BgL_variablez00);
																		}
																		BgL_tmpz00_3088 = ((obj_t) BgL_auxz00_3089);
																	}
																	BgL_test1965z00_3060 =
																		(BgL_tmpz00_3088 == BgL_auxz00_3099);
																}
															else
																{	/* Fxop/walk.scm 201 */
																	BgL_test1965z00_3060 = ((bool_t) 0);
																}
														}
														if (BgL_test1965z00_3060)
															{	/* Fxop/walk.scm 204 */
																obj_t BgL_arg1738z00_2641;

																{	/* Fxop/walk.scm 204 */
																	obj_t BgL_pairz00_2642;

																	BgL_pairz00_2642 =
																		(((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_nodez00_2580))->
																		BgL_bindingsz00);
																	{	/* Fxop/walk.scm 204 */
																		obj_t BgL_pairz00_2643;

																		BgL_pairz00_2643 = CAR(BgL_pairz00_2642);
																		BgL_arg1738z00_2641 = CDR(BgL_pairz00_2643);
																	}
																}
																BgL_arg1650z00_2591 =
																	BGl_bintze70ze7zzfxop_walkz00(
																	((BgL_nodez00_bglt) BgL_arg1738z00_2641));
															}
														else
															{	/* Fxop/walk.scm 205 */
																obj_t BgL_arg1740z00_2644;

																{	/* Fxop/walk.scm 205 */
																	obj_t BgL_pairz00_2645;

																	BgL_pairz00_2645 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_callz00_2581)))->
																		BgL_argsz00);
																	{	/* Fxop/walk.scm 205 */
																		obj_t BgL_pairz00_2646;

																		BgL_pairz00_2646 = CDR(BgL_pairz00_2645);
																		BgL_arg1740z00_2644 = CAR(BgL_pairz00_2646);
																	}
																}
																BgL_arg1650z00_2591 =
																	BGl_bintze70ze7zzfxop_walkz00(
																	((BgL_nodez00_bglt) BgL_arg1740z00_2644));
															}
													}
													{	/* Fxop/walk.scm 195 */
														obj_t BgL_list1651z00_2647;

														{	/* Fxop/walk.scm 195 */
															obj_t BgL_arg1654z00_2648;

															BgL_arg1654z00_2648 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1650z00_2591), BNIL);
															BgL_list1651z00_2647 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1646z00_2590),
																BgL_arg1654z00_2648);
														}
														BgL_auxz00_3008 = BgL_list1651z00_2647;
													}
												}
												((((BgL_appz00_bglt) COBJECT(BgL_new1151z00_2583))->
														BgL_argsz00) = ((obj_t) BgL_auxz00_3008), BUNSPEC);
											}
											((((BgL_appz00_bglt) COBJECT(BgL_new1151z00_2583))->
													BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
											BgL_auxz00_2966 = BgL_new1151z00_2583;
										}
									}
									return ((BgL_nodez00_bglt) BgL_auxz00_2966);
								}
							else
								{	/* Fxop/walk.scm 228 */
									obj_t BgL_g1171z00_2673;

									{	/* Fxop/walk.scm 228 */
										bool_t BgL_test1971z00_3123;

										{	/* Fxop/walk.scm 228 */
											BgL_typez00_bglt BgL_arg1421z00_2674;

											BgL_arg1421z00_2674 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2541))))->
												BgL_typez00);
											BgL_test1971z00_3123 =
												(((obj_t) BgL_arg1421z00_2674) ==
												BGl_za2boolza2z00zztype_cachez00);
										}
										if (BgL_test1971z00_3123)
											{	/* Fxop/walk.scm 228 */
												BgL_g1171z00_2673 =
													BGl_fxopzd2boolzd2letvarzf3ze70z14zzfxop_walkz00(
													(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_nodez00_2541)))->
														BgL_bindingsz00),
													(((BgL_letzd2varzd2_bglt)
															COBJECT(((BgL_letzd2varzd2_bglt)
																	BgL_nodez00_2541)))->BgL_bodyz00));
											}
										else
											{	/* Fxop/walk.scm 228 */
												BgL_g1171z00_2673 = BFALSE;
											}
									}
									if (CBOOL(BgL_g1171z00_2673))
										{
											BgL_appz00_bglt BgL_auxz00_3136;

											BgL_opz00_2649 = BgL_g1171z00_2673;
											BgL_nodez00_2650 =
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2541);
											{	/* Fxop/walk.scm 209 */
												BgL_appz00_bglt BgL_new1157z00_2651;

												{	/* Fxop/walk.scm 209 */
													BgL_appz00_bglt BgL_new1156z00_2652;

													BgL_new1156z00_2652 =
														((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_appz00_bgl))));
													{	/* Fxop/walk.scm 209 */
														long BgL_arg1806z00_2653;

														BgL_arg1806z00_2653 =
															BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1156z00_2652),
															BgL_arg1806z00_2653);
													}
													{	/* Fxop/walk.scm 209 */
														BgL_objectz00_bglt BgL_tmpz00_3141;

														BgL_tmpz00_3141 =
															((BgL_objectz00_bglt) BgL_new1156z00_2652);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3141, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1156z00_2652);
													BgL_new1157z00_2651 = BgL_new1156z00_2652;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1157z00_2651)))->
														BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1157z00_2651)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2boolza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_nodezf2effectzf2_bglt)
															COBJECT(((BgL_nodezf2effectzf2_bglt)
																	BgL_new1157z00_2651)))->
														BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
												((((BgL_nodezf2effectzf2_bglt)
															COBJECT(((BgL_nodezf2effectzf2_bglt)
																	BgL_new1157z00_2651)))->BgL_keyz00) =
													((obj_t) BINT(-1L)), BUNSPEC);
												{
													BgL_varz00_bglt BgL_auxz00_3155;

													{	/* Fxop/walk.scm 211 */
														BgL_refz00_bglt BgL_new1159z00_2654;

														{	/* Fxop/walk.scm 211 */
															BgL_refz00_bglt BgL_new1158z00_2655;

															BgL_new1158z00_2655 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Fxop/walk.scm 211 */
																long BgL_arg1765z00_2656;

																{	/* Fxop/walk.scm 211 */
																	obj_t BgL_classz00_2657;

																	BgL_classz00_2657 = BGl_refz00zzast_nodez00;
																	BgL_arg1765z00_2656 =
																		BGL_CLASS_NUM(BgL_classz00_2657);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1158z00_2655),
																	BgL_arg1765z00_2656);
															}
															{	/* Fxop/walk.scm 211 */
																BgL_objectz00_bglt BgL_tmpz00_3160;

																BgL_tmpz00_3160 =
																	((BgL_objectz00_bglt) BgL_new1158z00_2655);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3160,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1158z00_2655);
															BgL_new1159z00_2654 = BgL_new1158z00_2655;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1159z00_2654)))->
																BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1159z00_2654)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_opz00_2649)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																			BgL_new1159z00_2654)))->BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_opz00_2649)), BUNSPEC);
														BgL_auxz00_3155 =
															((BgL_varz00_bglt) BgL_new1159z00_2654);
													}
													((((BgL_appz00_bglt) COBJECT(BgL_new1157z00_2651))->
															BgL_funz00) =
														((BgL_varz00_bglt) BgL_auxz00_3155), BUNSPEC);
												}
												{
													obj_t BgL_auxz00_3175;

													{	/* Fxop/walk.scm 215 */
														BgL_nodez00_bglt BgL_arg1767z00_2658;
														BgL_nodez00_bglt BgL_arg1770z00_2659;

														{	/* Fxop/walk.scm 215 */
															obj_t BgL_arg1775z00_2660;

															{	/* Fxop/walk.scm 215 */
																obj_t BgL_pairz00_2661;

																BgL_pairz00_2661 =
																	(((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_nodez00_2650))->
																	BgL_bindingsz00);
																{	/* Fxop/walk.scm 215 */
																	obj_t BgL_pairz00_2662;

																	BgL_pairz00_2662 = CAR(BgL_pairz00_2661);
																	BgL_arg1775z00_2660 = CDR(BgL_pairz00_2662);
															}}
															BgL_arg1767z00_2658 =
																BGl_bintze70ze7zzfxop_walkz00(
																((BgL_nodez00_bglt) BgL_arg1775z00_2660));
														}
														{	/* Fxop/walk.scm 216 */
															obj_t BgL_arg1799z00_2663;

															{	/* Fxop/walk.scm 216 */
																obj_t BgL_pairz00_2664;

																BgL_pairz00_2664 =
																	(((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_nodez00_2650))->
																	BgL_bindingsz00);
																{	/* Fxop/walk.scm 216 */
																	obj_t BgL_pairz00_2665;

																	{	/* Fxop/walk.scm 216 */
																		obj_t BgL_pairz00_2666;

																		BgL_pairz00_2666 = CDR(BgL_pairz00_2664);
																		BgL_pairz00_2665 = CAR(BgL_pairz00_2666);
																	}
																	BgL_arg1799z00_2663 = CDR(BgL_pairz00_2665);
															}}
															BgL_arg1770z00_2659 =
																BGl_bintze70ze7zzfxop_walkz00(
																((BgL_nodez00_bglt) BgL_arg1799z00_2663));
														}
														{	/* Fxop/walk.scm 214 */
															obj_t BgL_list1771z00_2667;

															{	/* Fxop/walk.scm 214 */
																obj_t BgL_arg1773z00_2668;

																BgL_arg1773z00_2668 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1770z00_2659), BNIL);
																BgL_list1771z00_2667 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1767z00_2658),
																	BgL_arg1773z00_2668);
															}
															BgL_auxz00_3175 = BgL_list1771z00_2667;
													}}
													((((BgL_appz00_bglt) COBJECT(BgL_new1157z00_2651))->
															BgL_argsz00) =
														((obj_t) BgL_auxz00_3175), BUNSPEC);
												}
												((((BgL_appz00_bglt) COBJECT(BgL_new1157z00_2651))->
														BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
												BgL_auxz00_3136 = BgL_new1157z00_2651;
											}
											return ((BgL_nodez00_bglt) BgL_auxz00_3136);
										}
									else
										{	/* Fxop/walk.scm 228 */
											return
												BGl_walk0z12z12zzast_walkz00(
												((BgL_nodez00_bglt)
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2541)),
												BGl_fxopz12zd2envzc0zzfxop_walkz00);
										}
								}
						}
				}
			}
		}

	}



/* find-fxop~0 */
	obj_t BGl_findzd2fxopze70z35zzfxop_walkz00(obj_t BgL_exprz00_1622)
	{
		{	/* Fxop/walk.scm 120 */
			{	/* Fxop/walk.scm 117 */
				bool_t BgL_test1973z00_3198;

				{	/* Fxop/walk.scm 117 */
					obj_t BgL_classz00_2103;

					BgL_classz00_2103 = BGl_appz00zzast_nodez00;
					if (BGL_OBJECTP(BgL_exprz00_1622))
						{	/* Fxop/walk.scm 117 */
							BgL_objectz00_bglt BgL_arg1807z00_2105;

							BgL_arg1807z00_2105 = (BgL_objectz00_bglt) (BgL_exprz00_1622);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Fxop/walk.scm 117 */
									long BgL_idxz00_2111;

									BgL_idxz00_2111 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2105);
									BgL_test1973z00_3198 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2111 + 3L)) == BgL_classz00_2103);
								}
							else
								{	/* Fxop/walk.scm 117 */
									bool_t BgL_res1896z00_2136;

									{	/* Fxop/walk.scm 117 */
										obj_t BgL_oclassz00_2119;

										{	/* Fxop/walk.scm 117 */
											obj_t BgL_arg1815z00_2127;
											long BgL_arg1816z00_2128;

											BgL_arg1815z00_2127 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Fxop/walk.scm 117 */
												long BgL_arg1817z00_2129;

												BgL_arg1817z00_2129 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2105);
												BgL_arg1816z00_2128 =
													(BgL_arg1817z00_2129 - OBJECT_TYPE);
											}
											BgL_oclassz00_2119 =
												VECTOR_REF(BgL_arg1815z00_2127, BgL_arg1816z00_2128);
										}
										{	/* Fxop/walk.scm 117 */
											bool_t BgL__ortest_1115z00_2120;

											BgL__ortest_1115z00_2120 =
												(BgL_classz00_2103 == BgL_oclassz00_2119);
											if (BgL__ortest_1115z00_2120)
												{	/* Fxop/walk.scm 117 */
													BgL_res1896z00_2136 = BgL__ortest_1115z00_2120;
												}
											else
												{	/* Fxop/walk.scm 117 */
													long BgL_odepthz00_2121;

													{	/* Fxop/walk.scm 117 */
														obj_t BgL_arg1804z00_2122;

														BgL_arg1804z00_2122 = (BgL_oclassz00_2119);
														BgL_odepthz00_2121 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2122);
													}
													if ((3L < BgL_odepthz00_2121))
														{	/* Fxop/walk.scm 117 */
															obj_t BgL_arg1802z00_2124;

															{	/* Fxop/walk.scm 117 */
																obj_t BgL_arg1803z00_2125;

																BgL_arg1803z00_2125 = (BgL_oclassz00_2119);
																BgL_arg1802z00_2124 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2125,
																	3L);
															}
															BgL_res1896z00_2136 =
																(BgL_arg1802z00_2124 == BgL_classz00_2103);
														}
													else
														{	/* Fxop/walk.scm 117 */
															BgL_res1896z00_2136 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1973z00_3198 = BgL_res1896z00_2136;
								}
						}
					else
						{	/* Fxop/walk.scm 117 */
							BgL_test1973z00_3198 = ((bool_t) 0);
						}
				}
				if (BgL_test1973z00_3198)
					{	/* Fxop/walk.scm 118 */
						BgL_variablez00_bglt BgL_vz00_1625;

						BgL_vz00_1625 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_exprz00_1622)))->BgL_funz00)))->
							BgL_variablez00);
						{	/* Fxop/walk.scm 119 */
							obj_t BgL_cz00_1626;

							BgL_cz00_1626 =
								BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
								((obj_t) BgL_vz00_1625), BGl_za2fxopsza2z00zzfxop_walkz00);
							if (CBOOL(BgL_cz00_1626))
								{	/* Fxop/walk.scm 120 */
									return CDR(((obj_t) BgL_cz00_1626));
								}
							else
								{	/* Fxop/walk.scm 120 */
									return BFALSE;
								}
						}
					}
				else
					{	/* Fxop/walk.scm 117 */
						return BFALSE;
					}
			}
		}

	}



/* bint~0 */
	BgL_nodez00_bglt BGl_bintze70ze7zzfxop_walkz00(BgL_nodez00_bglt
		BgL_exprz00_1676)
	{
		{	/* Fxop/walk.scm 173 */
			{	/* Fxop/walk.scm 163 */
				bool_t BgL_test1979z00_3230;

				{	/* Fxop/walk.scm 163 */
					BgL_typez00_bglt BgL_arg1606z00_1690;

					BgL_arg1606z00_1690 =
						(((BgL_nodez00_bglt) COBJECT(BgL_exprz00_1676))->BgL_typez00);
					BgL_test1979z00_3230 =
						(((obj_t) BgL_arg1606z00_1690) == BGl_za2bintza2z00zztype_cachez00);
				}
				if (BgL_test1979z00_3230)
					{	/* Fxop/walk.scm 163 */
						return BgL_exprz00_1676;
					}
				else
					{	/* Fxop/walk.scm 163 */
						if (BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00(
								((obj_t) BgL_exprz00_1676)))
							{	/* Fxop/walk.scm 166 */
								obj_t BgL_pairz00_2300;

								BgL_pairz00_2300 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_exprz00_1676)))->BgL_argsz00);
								return ((BgL_nodez00_bglt) CAR(BgL_pairz00_2300));
							}
						else
							{	/* Fxop/walk.scm 168 */
								BgL_appz00_bglt BgL_new1140z00_1683;

								{	/* Fxop/walk.scm 168 */
									BgL_appz00_bglt BgL_new1139z00_1688;

									BgL_new1139z00_1688 =
										((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_appz00_bgl))));
									{	/* Fxop/walk.scm 168 */
										long BgL_arg1605z00_1689;

										BgL_arg1605z00_1689 =
											BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1139z00_1688),
											BgL_arg1605z00_1689);
									}
									{	/* Fxop/walk.scm 168 */
										BgL_objectz00_bglt BgL_tmpz00_3245;

										BgL_tmpz00_3245 =
											((BgL_objectz00_bglt) BgL_new1139z00_1688);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3245, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1139z00_1688);
									BgL_new1140z00_1683 = BgL_new1139z00_1688;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1140z00_1683)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1140z00_1683)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2bintza2z00zztype_cachez00)), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1140z00_1683)))->BgL_sidezd2effectzd2) =
									((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1140z00_1683)))->BgL_keyz00) =
									((obj_t) BINT(-1L)), BUNSPEC);
								{
									BgL_varz00_bglt BgL_auxz00_3259;

									{	/* Fxop/walk.scm 170 */
										BgL_refz00_bglt BgL_new1142z00_1684;

										{	/* Fxop/walk.scm 170 */
											BgL_refz00_bglt BgL_new1141z00_1685;

											BgL_new1141z00_1685 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Fxop/walk.scm 170 */
												long BgL_arg1602z00_1686;

												{	/* Fxop/walk.scm 170 */
													obj_t BgL_classz00_2305;

													BgL_classz00_2305 = BGl_refz00zzast_nodez00;
													BgL_arg1602z00_1686 =
														BGL_CLASS_NUM(BgL_classz00_2305);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1141z00_1685),
													BgL_arg1602z00_1686);
											}
											{	/* Fxop/walk.scm 170 */
												BgL_objectz00_bglt BgL_tmpz00_3264;

												BgL_tmpz00_3264 =
													((BgL_objectz00_bglt) BgL_new1141z00_1685);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3264, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1141z00_1685);
											BgL_new1142z00_1684 = BgL_new1141z00_1685;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1142z00_1684)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1142z00_1684)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2bintza2z00zztype_cachez00)), BUNSPEC);
										((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_new1142z00_1684)))->BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BGl_za2longzd2ze3bintza2z31zzfxop_walkz00)), BUNSPEC);
										BgL_auxz00_3259 = ((BgL_varz00_bglt) BgL_new1142z00_1684);
									}
									((((BgL_appz00_bglt) COBJECT(BgL_new1140z00_1683))->
											BgL_funz00) =
										((BgL_varz00_bglt) BgL_auxz00_3259), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_3278;

									{	/* Fxop/walk.scm 173 */
										obj_t BgL_list1603z00_1687;

										BgL_list1603z00_1687 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_exprz00_1676), BNIL);
										BgL_auxz00_3278 = BgL_list1603z00_1687;
									}
									((((BgL_appz00_bglt) COBJECT(BgL_new1140z00_1683))->
											BgL_argsz00) = ((obj_t) BgL_auxz00_3278), BUNSPEC);
								}
								((((BgL_appz00_bglt) COBJECT(BgL_new1140z00_1683))->
										BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
								return ((BgL_nodez00_bglt) BgL_new1140z00_1683);
							}
					}
			}
		}

	}



/* bint->long?~0 */
	bool_t BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00(obj_t BgL_exprz00_1612)
	{
		{	/* Fxop/walk.scm 109 */
			{	/* Fxop/walk.scm 107 */
				bool_t BgL_test1981z00_3284;

				{	/* Fxop/walk.scm 107 */
					obj_t BgL_classz00_2031;

					BgL_classz00_2031 = BGl_appz00zzast_nodez00;
					if (BGL_OBJECTP(BgL_exprz00_1612))
						{	/* Fxop/walk.scm 107 */
							BgL_objectz00_bglt BgL_arg1807z00_2033;

							BgL_arg1807z00_2033 = (BgL_objectz00_bglt) (BgL_exprz00_1612);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Fxop/walk.scm 107 */
									long BgL_idxz00_2039;

									BgL_idxz00_2039 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2033);
									BgL_test1981z00_3284 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2039 + 3L)) == BgL_classz00_2031);
								}
							else
								{	/* Fxop/walk.scm 107 */
									bool_t BgL_res1894z00_2064;

									{	/* Fxop/walk.scm 107 */
										obj_t BgL_oclassz00_2047;

										{	/* Fxop/walk.scm 107 */
											obj_t BgL_arg1815z00_2055;
											long BgL_arg1816z00_2056;

											BgL_arg1815z00_2055 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Fxop/walk.scm 107 */
												long BgL_arg1817z00_2057;

												BgL_arg1817z00_2057 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2033);
												BgL_arg1816z00_2056 =
													(BgL_arg1817z00_2057 - OBJECT_TYPE);
											}
											BgL_oclassz00_2047 =
												VECTOR_REF(BgL_arg1815z00_2055, BgL_arg1816z00_2056);
										}
										{	/* Fxop/walk.scm 107 */
											bool_t BgL__ortest_1115z00_2048;

											BgL__ortest_1115z00_2048 =
												(BgL_classz00_2031 == BgL_oclassz00_2047);
											if (BgL__ortest_1115z00_2048)
												{	/* Fxop/walk.scm 107 */
													BgL_res1894z00_2064 = BgL__ortest_1115z00_2048;
												}
											else
												{	/* Fxop/walk.scm 107 */
													long BgL_odepthz00_2049;

													{	/* Fxop/walk.scm 107 */
														obj_t BgL_arg1804z00_2050;

														BgL_arg1804z00_2050 = (BgL_oclassz00_2047);
														BgL_odepthz00_2049 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2050);
													}
													if ((3L < BgL_odepthz00_2049))
														{	/* Fxop/walk.scm 107 */
															obj_t BgL_arg1802z00_2052;

															{	/* Fxop/walk.scm 107 */
																obj_t BgL_arg1803z00_2053;

																BgL_arg1803z00_2053 = (BgL_oclassz00_2047);
																BgL_arg1802z00_2052 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2053,
																	3L);
															}
															BgL_res1894z00_2064 =
																(BgL_arg1802z00_2052 == BgL_classz00_2031);
														}
													else
														{	/* Fxop/walk.scm 107 */
															BgL_res1894z00_2064 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1981z00_3284 = BgL_res1894z00_2064;
								}
						}
					else
						{	/* Fxop/walk.scm 107 */
							BgL_test1981z00_3284 = ((bool_t) 0);
						}
				}
				if (BgL_test1981z00_3284)
					{	/* Fxop/walk.scm 108 */
						BgL_variablez00_bglt BgL_vz00_1615;

						BgL_vz00_1615 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_exprz00_1612)))->BgL_funz00)))->
							BgL_variablez00);
						return (((obj_t) BgL_vz00_1615) ==
							BGl_za2bintzd2ze3longza2z31zzfxop_walkz00);
					}
				else
					{	/* Fxop/walk.scm 107 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* long->bint?~0 */
	bool_t BGl_longzd2ze3bintzf3ze70z25zzfxop_walkz00(BgL_nodez00_bglt
		BgL_exprz00_1617)
	{
		{	/* Fxop/walk.scm 114 */
			{	/* Fxop/walk.scm 112 */
				bool_t BgL_test1986z00_3312;

				{	/* Fxop/walk.scm 112 */
					obj_t BgL_classz00_2067;

					BgL_classz00_2067 = BGl_appz00zzast_nodez00;
					{	/* Fxop/walk.scm 112 */
						BgL_objectz00_bglt BgL_arg1807z00_2069;

						{	/* Fxop/walk.scm 112 */
							obj_t BgL_tmpz00_3313;

							BgL_tmpz00_3313 =
								((obj_t) ((BgL_objectz00_bglt) BgL_exprz00_1617));
							BgL_arg1807z00_2069 = (BgL_objectz00_bglt) (BgL_tmpz00_3313);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Fxop/walk.scm 112 */
								long BgL_idxz00_2075;

								BgL_idxz00_2075 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2069);
								BgL_test1986z00_3312 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2075 + 3L)) == BgL_classz00_2067);
							}
						else
							{	/* Fxop/walk.scm 112 */
								bool_t BgL_res1895z00_2100;

								{	/* Fxop/walk.scm 112 */
									obj_t BgL_oclassz00_2083;

									{	/* Fxop/walk.scm 112 */
										obj_t BgL_arg1815z00_2091;
										long BgL_arg1816z00_2092;

										BgL_arg1815z00_2091 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Fxop/walk.scm 112 */
											long BgL_arg1817z00_2093;

											BgL_arg1817z00_2093 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2069);
											BgL_arg1816z00_2092 = (BgL_arg1817z00_2093 - OBJECT_TYPE);
										}
										BgL_oclassz00_2083 =
											VECTOR_REF(BgL_arg1815z00_2091, BgL_arg1816z00_2092);
									}
									{	/* Fxop/walk.scm 112 */
										bool_t BgL__ortest_1115z00_2084;

										BgL__ortest_1115z00_2084 =
											(BgL_classz00_2067 == BgL_oclassz00_2083);
										if (BgL__ortest_1115z00_2084)
											{	/* Fxop/walk.scm 112 */
												BgL_res1895z00_2100 = BgL__ortest_1115z00_2084;
											}
										else
											{	/* Fxop/walk.scm 112 */
												long BgL_odepthz00_2085;

												{	/* Fxop/walk.scm 112 */
													obj_t BgL_arg1804z00_2086;

													BgL_arg1804z00_2086 = (BgL_oclassz00_2083);
													BgL_odepthz00_2085 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2086);
												}
												if ((3L < BgL_odepthz00_2085))
													{	/* Fxop/walk.scm 112 */
														obj_t BgL_arg1802z00_2088;

														{	/* Fxop/walk.scm 112 */
															obj_t BgL_arg1803z00_2089;

															BgL_arg1803z00_2089 = (BgL_oclassz00_2083);
															BgL_arg1802z00_2088 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2089,
																3L);
														}
														BgL_res1895z00_2100 =
															(BgL_arg1802z00_2088 == BgL_classz00_2067);
													}
												else
													{	/* Fxop/walk.scm 112 */
														BgL_res1895z00_2100 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1986z00_3312 = BgL_res1895z00_2100;
							}
					}
				}
				if (BgL_test1986z00_3312)
					{	/* Fxop/walk.scm 113 */
						BgL_variablez00_bglt BgL_vz00_1620;

						BgL_vz00_1620 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_exprz00_1617)))->BgL_funz00)))->
							BgL_variablez00);
						return (((obj_t) BgL_vz00_1620) ==
							BGl_za2longzd2ze3bintza2z31zzfxop_walkz00);
					}
				else
					{	/* Fxop/walk.scm 112 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* fxop-double-int-letvar?~0 */
	obj_t BGl_fxopzd2doublezd2intzd2letvarzf3ze70zc6zzfxop_walkz00(obj_t
		BgL_bindingsz00_1628, BgL_nodez00_bglt BgL_bodyz00_1629)
	{
		{	/* Fxop/walk.scm 136 */
			if ((bgl_list_length(BgL_bindingsz00_1628) == 2L))
				{	/* Fxop/walk.scm 128 */
					bool_t BgL_test1991z00_3344;

					{	/* Fxop/walk.scm 128 */
						bool_t BgL__ortest_1125z00_1637;

						{	/* Fxop/walk.scm 128 */
							bool_t BgL__ortest_1126z00_1645;

							{	/* Fxop/walk.scm 128 */
								obj_t BgL_arg1559z00_1651;

								{	/* Fxop/walk.scm 128 */
									obj_t BgL_pairz00_2144;

									BgL_pairz00_2144 = CAR(((obj_t) BgL_bindingsz00_1628));
									BgL_arg1559z00_1651 = CDR(BgL_pairz00_2144);
								}
								BgL__ortest_1126z00_1645 =
									BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00
									(BgL_arg1559z00_1651);
							}
							if (BgL__ortest_1126z00_1645)
								{	/* Fxop/walk.scm 128 */
									BgL__ortest_1125z00_1637 = BgL__ortest_1126z00_1645;
								}
							else
								{	/* Fxop/walk.scm 129 */
									bool_t BgL__ortest_1127z00_1646;

									{	/* Fxop/walk.scm 129 */
										BgL_typez00_bglt BgL_arg1552z00_1649;

										{
											BgL_variablez00_bglt BgL_auxz00_3350;

											{	/* Fxop/walk.scm 129 */
												obj_t BgL_pairz00_2148;

												BgL_pairz00_2148 = CAR(((obj_t) BgL_bindingsz00_1628));
												BgL_auxz00_3350 =
													((BgL_variablez00_bglt) CAR(BgL_pairz00_2148));
											}
											BgL_arg1552z00_1649 =
												(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_3350))->
												BgL_typez00);
										}
										BgL__ortest_1127z00_1646 =
											(
											((obj_t) BgL_arg1552z00_1649) ==
											BGl_za2bintza2z00zztype_cachez00);
									}
									if (BgL__ortest_1127z00_1646)
										{	/* Fxop/walk.scm 129 */
											BgL__ortest_1125z00_1637 = BgL__ortest_1127z00_1646;
										}
									else
										{	/* Fxop/walk.scm 130 */
											BgL_typez00_bglt BgL_arg1544z00_1647;

											{
												BgL_variablez00_bglt BgL_auxz00_3359;

												{	/* Fxop/walk.scm 130 */
													obj_t BgL_pairz00_2153;

													BgL_pairz00_2153 =
														CAR(((obj_t) BgL_bindingsz00_1628));
													BgL_auxz00_3359 =
														((BgL_variablez00_bglt) CAR(BgL_pairz00_2153));
												}
												BgL_arg1544z00_1647 =
													(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_3359))->
													BgL_typez00);
											}
											BgL__ortest_1125z00_1637 =
												(
												((obj_t) BgL_arg1544z00_1647) ==
												BGl_za2longza2z00zztype_cachez00);
										}
								}
						}
						if (BgL__ortest_1125z00_1637)
							{	/* Fxop/walk.scm 128 */
								BgL_test1991z00_3344 = BgL__ortest_1125z00_1637;
							}
						else
							{	/* Fxop/walk.scm 131 */
								bool_t BgL__ortest_1128z00_1638;

								{	/* Fxop/walk.scm 131 */
									obj_t BgL_arg1540z00_1644;

									{	/* Fxop/walk.scm 131 */
										obj_t BgL_pairz00_2160;

										{	/* Fxop/walk.scm 131 */
											obj_t BgL_pairz00_2159;

											BgL_pairz00_2159 = CDR(((obj_t) BgL_bindingsz00_1628));
											BgL_pairz00_2160 = CAR(BgL_pairz00_2159);
										}
										BgL_arg1540z00_1644 = CDR(BgL_pairz00_2160);
									}
									BgL__ortest_1128z00_1638 =
										BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00
										(BgL_arg1540z00_1644);
								}
								if (BgL__ortest_1128z00_1638)
									{	/* Fxop/walk.scm 131 */
										BgL_test1991z00_3344 = BgL__ortest_1128z00_1638;
									}
								else
									{	/* Fxop/walk.scm 132 */
										bool_t BgL__ortest_1129z00_1639;

										{	/* Fxop/walk.scm 132 */
											BgL_typez00_bglt BgL_arg1516z00_1642;

											{
												BgL_variablez00_bglt BgL_auxz00_3374;

												{	/* Fxop/walk.scm 132 */
													obj_t BgL_pairz00_2166;

													{	/* Fxop/walk.scm 132 */
														obj_t BgL_pairz00_2165;

														BgL_pairz00_2165 =
															CDR(((obj_t) BgL_bindingsz00_1628));
														BgL_pairz00_2166 = CAR(BgL_pairz00_2165);
													}
													BgL_auxz00_3374 =
														((BgL_variablez00_bglt) CAR(BgL_pairz00_2166));
												}
												BgL_arg1516z00_1642 =
													(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_3374))->
													BgL_typez00);
											}
											BgL__ortest_1129z00_1639 =
												(
												((obj_t) BgL_arg1516z00_1642) ==
												BGl_za2bintza2z00zztype_cachez00);
										}
										if (BgL__ortest_1129z00_1639)
											{	/* Fxop/walk.scm 132 */
												BgL_test1991z00_3344 = BgL__ortest_1129z00_1639;
											}
										else
											{	/* Fxop/walk.scm 133 */
												BgL_typez00_bglt BgL_arg1513z00_1640;

												{
													BgL_variablez00_bglt BgL_auxz00_3384;

													{	/* Fxop/walk.scm 133 */
														obj_t BgL_pairz00_2173;

														{	/* Fxop/walk.scm 133 */
															obj_t BgL_pairz00_2172;

															BgL_pairz00_2172 =
																CDR(((obj_t) BgL_bindingsz00_1628));
															BgL_pairz00_2173 = CAR(BgL_pairz00_2172);
														}
														BgL_auxz00_3384 =
															((BgL_variablez00_bglt) CAR(BgL_pairz00_2173));
													}
													BgL_arg1513z00_1640 =
														(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_3384))->
														BgL_typez00);
												}
												BgL_test1991z00_3344 =
													(
													((obj_t) BgL_arg1513z00_1640) ==
													BGl_za2longza2z00zztype_cachez00);
											}
									}
							}
					}
					if (BgL_test1991z00_3344)
						{	/* Fxop/walk.scm 134 */
							bool_t BgL_test1997z00_3393;

							{	/* Fxop/walk.scm 134 */
								obj_t BgL_classz00_2175;

								BgL_classz00_2175 = BGl_appz00zzast_nodez00;
								{	/* Fxop/walk.scm 134 */
									BgL_objectz00_bglt BgL_arg1807z00_2177;

									{	/* Fxop/walk.scm 134 */
										obj_t BgL_tmpz00_3394;

										BgL_tmpz00_3394 =
											((obj_t) ((BgL_objectz00_bglt) BgL_bodyz00_1629));
										BgL_arg1807z00_2177 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3394);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Fxop/walk.scm 134 */
											long BgL_idxz00_2183;

											BgL_idxz00_2183 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2177);
											BgL_test1997z00_3393 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2183 + 3L)) == BgL_classz00_2175);
										}
									else
										{	/* Fxop/walk.scm 134 */
											bool_t BgL_res1897z00_2208;

											{	/* Fxop/walk.scm 134 */
												obj_t BgL_oclassz00_2191;

												{	/* Fxop/walk.scm 134 */
													obj_t BgL_arg1815z00_2199;
													long BgL_arg1816z00_2200;

													BgL_arg1815z00_2199 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Fxop/walk.scm 134 */
														long BgL_arg1817z00_2201;

														BgL_arg1817z00_2201 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2177);
														BgL_arg1816z00_2200 =
															(BgL_arg1817z00_2201 - OBJECT_TYPE);
													}
													BgL_oclassz00_2191 =
														VECTOR_REF(BgL_arg1815z00_2199,
														BgL_arg1816z00_2200);
												}
												{	/* Fxop/walk.scm 134 */
													bool_t BgL__ortest_1115z00_2192;

													BgL__ortest_1115z00_2192 =
														(BgL_classz00_2175 == BgL_oclassz00_2191);
													if (BgL__ortest_1115z00_2192)
														{	/* Fxop/walk.scm 134 */
															BgL_res1897z00_2208 = BgL__ortest_1115z00_2192;
														}
													else
														{	/* Fxop/walk.scm 134 */
															long BgL_odepthz00_2193;

															{	/* Fxop/walk.scm 134 */
																obj_t BgL_arg1804z00_2194;

																BgL_arg1804z00_2194 = (BgL_oclassz00_2191);
																BgL_odepthz00_2193 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2194);
															}
															if ((3L < BgL_odepthz00_2193))
																{	/* Fxop/walk.scm 134 */
																	obj_t BgL_arg1802z00_2196;

																	{	/* Fxop/walk.scm 134 */
																		obj_t BgL_arg1803z00_2197;

																		BgL_arg1803z00_2197 = (BgL_oclassz00_2191);
																		BgL_arg1802z00_2196 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2197, 3L);
																	}
																	BgL_res1897z00_2208 =
																		(BgL_arg1802z00_2196 == BgL_classz00_2175);
																}
															else
																{	/* Fxop/walk.scm 134 */
																	BgL_res1897z00_2208 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1997z00_3393 = BgL_res1897z00_2208;
										}
								}
							}
							if (BgL_test1997z00_3393)
								{	/* Fxop/walk.scm 134 */
									if (BGl_longzd2ze3bintzf3ze70z25zzfxop_walkz00
										(BgL_bodyz00_1629))
										{	/* Fxop/walk.scm 136 */
											obj_t BgL_arg1502z00_1635;

											{	/* Fxop/walk.scm 136 */
												obj_t BgL_pairz00_2210;

												BgL_pairz00_2210 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_bodyz00_1629)))->
													BgL_argsz00);
												BgL_arg1502z00_1635 = CAR(BgL_pairz00_2210);
											}
											return
												BGl_findzd2fxopze70z35zzfxop_walkz00
												(BgL_arg1502z00_1635);
										}
									else
										{	/* Fxop/walk.scm 135 */
											return BFALSE;
										}
								}
							else
								{	/* Fxop/walk.scm 134 */
									return BFALSE;
								}
						}
					else
						{	/* Fxop/walk.scm 128 */
							return BFALSE;
						}
				}
			else
				{	/* Fxop/walk.scm 127 */
					return BFALSE;
				}
		}

	}



/* fxop-simple-int-letvar?~0 */
	obj_t BGl_fxopzd2simplezd2intzd2letvarzf3ze70zc6zzfxop_walkz00(obj_t
		BgL_bindingsz00_1653, BgL_nodez00_bglt BgL_bodyz00_1654)
	{
		{	/* Fxop/walk.scm 147 */
			if ((bgl_list_length(BgL_bindingsz00_1653) == 1L))
				{	/* Fxop/walk.scm 144 */
					bool_t BgL_test2003z00_3426;

					{	/* Fxop/walk.scm 144 */
						obj_t BgL_arg1571z00_1662;

						{	/* Fxop/walk.scm 144 */
							obj_t BgL_pairz00_2215;

							BgL_pairz00_2215 = CAR(((obj_t) BgL_bindingsz00_1653));
							BgL_arg1571z00_1662 = CDR(BgL_pairz00_2215);
						}
						BgL_test2003z00_3426 =
							BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00(BgL_arg1571z00_1662);
					}
					if (BgL_test2003z00_3426)
						{	/* Fxop/walk.scm 145 */
							bool_t BgL_test2004z00_3431;

							{	/* Fxop/walk.scm 145 */
								obj_t BgL_classz00_2216;

								BgL_classz00_2216 = BGl_appz00zzast_nodez00;
								{	/* Fxop/walk.scm 145 */
									BgL_objectz00_bglt BgL_arg1807z00_2218;

									{	/* Fxop/walk.scm 145 */
										obj_t BgL_tmpz00_3432;

										BgL_tmpz00_3432 =
											((obj_t) ((BgL_objectz00_bglt) BgL_bodyz00_1654));
										BgL_arg1807z00_2218 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3432);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Fxop/walk.scm 145 */
											long BgL_idxz00_2224;

											BgL_idxz00_2224 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2218);
											BgL_test2004z00_3431 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2224 + 3L)) == BgL_classz00_2216);
										}
									else
										{	/* Fxop/walk.scm 145 */
											bool_t BgL_res1898z00_2249;

											{	/* Fxop/walk.scm 145 */
												obj_t BgL_oclassz00_2232;

												{	/* Fxop/walk.scm 145 */
													obj_t BgL_arg1815z00_2240;
													long BgL_arg1816z00_2241;

													BgL_arg1815z00_2240 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Fxop/walk.scm 145 */
														long BgL_arg1817z00_2242;

														BgL_arg1817z00_2242 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2218);
														BgL_arg1816z00_2241 =
															(BgL_arg1817z00_2242 - OBJECT_TYPE);
													}
													BgL_oclassz00_2232 =
														VECTOR_REF(BgL_arg1815z00_2240,
														BgL_arg1816z00_2241);
												}
												{	/* Fxop/walk.scm 145 */
													bool_t BgL__ortest_1115z00_2233;

													BgL__ortest_1115z00_2233 =
														(BgL_classz00_2216 == BgL_oclassz00_2232);
													if (BgL__ortest_1115z00_2233)
														{	/* Fxop/walk.scm 145 */
															BgL_res1898z00_2249 = BgL__ortest_1115z00_2233;
														}
													else
														{	/* Fxop/walk.scm 145 */
															long BgL_odepthz00_2234;

															{	/* Fxop/walk.scm 145 */
																obj_t BgL_arg1804z00_2235;

																BgL_arg1804z00_2235 = (BgL_oclassz00_2232);
																BgL_odepthz00_2234 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2235);
															}
															if ((3L < BgL_odepthz00_2234))
																{	/* Fxop/walk.scm 145 */
																	obj_t BgL_arg1802z00_2237;

																	{	/* Fxop/walk.scm 145 */
																		obj_t BgL_arg1803z00_2238;

																		BgL_arg1803z00_2238 = (BgL_oclassz00_2232);
																		BgL_arg1802z00_2237 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2238, 3L);
																	}
																	BgL_res1898z00_2249 =
																		(BgL_arg1802z00_2237 == BgL_classz00_2216);
																}
															else
																{	/* Fxop/walk.scm 145 */
																	BgL_res1898z00_2249 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2004z00_3431 = BgL_res1898z00_2249;
										}
								}
							}
							if (BgL_test2004z00_3431)
								{	/* Fxop/walk.scm 145 */
									if (BGl_longzd2ze3bintzf3ze70z25zzfxop_walkz00
										(BgL_bodyz00_1654))
										{	/* Fxop/walk.scm 147 */
											obj_t BgL_arg1564z00_1660;

											{	/* Fxop/walk.scm 147 */
												obj_t BgL_pairz00_2251;

												BgL_pairz00_2251 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_bodyz00_1654)))->
													BgL_argsz00);
												BgL_arg1564z00_1660 = CAR(BgL_pairz00_2251);
											}
											return
												BGl_findzd2fxopze70z35zzfxop_walkz00
												(BgL_arg1564z00_1660);
										}
									else
										{	/* Fxop/walk.scm 146 */
											return BFALSE;
										}
								}
							else
								{	/* Fxop/walk.scm 145 */
									return BFALSE;
								}
						}
					else
						{	/* Fxop/walk.scm 144 */
							return BFALSE;
						}
				}
			else
				{	/* Fxop/walk.scm 143 */
					return BFALSE;
				}
		}

	}



/* fxop-bool-letvar?~0 */
	obj_t BGl_fxopzd2boolzd2letvarzf3ze70z14zzfxop_walkz00(obj_t
		BgL_bindingsz00_1664, BgL_nodez00_bglt BgL_bodyz00_1665)
	{
		{	/* Fxop/walk.scm 158 */
			{	/* Fxop/walk.scm 154 */
				bool_t BgL_test2009z00_3461;

				{	/* Fxop/walk.scm 154 */
					obj_t BgL_classz00_2252;

					BgL_classz00_2252 = BGl_appz00zzast_nodez00;
					{	/* Fxop/walk.scm 154 */
						BgL_objectz00_bglt BgL_arg1807z00_2254;

						{	/* Fxop/walk.scm 154 */
							obj_t BgL_tmpz00_3462;

							BgL_tmpz00_3462 =
								((obj_t) ((BgL_objectz00_bglt) BgL_bodyz00_1665));
							BgL_arg1807z00_2254 = (BgL_objectz00_bglt) (BgL_tmpz00_3462);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Fxop/walk.scm 154 */
								long BgL_idxz00_2260;

								BgL_idxz00_2260 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2254);
								BgL_test2009z00_3461 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2260 + 3L)) == BgL_classz00_2252);
							}
						else
							{	/* Fxop/walk.scm 154 */
								bool_t BgL_res1899z00_2285;

								{	/* Fxop/walk.scm 154 */
									obj_t BgL_oclassz00_2268;

									{	/* Fxop/walk.scm 154 */
										obj_t BgL_arg1815z00_2276;
										long BgL_arg1816z00_2277;

										BgL_arg1815z00_2276 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Fxop/walk.scm 154 */
											long BgL_arg1817z00_2278;

											BgL_arg1817z00_2278 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2254);
											BgL_arg1816z00_2277 = (BgL_arg1817z00_2278 - OBJECT_TYPE);
										}
										BgL_oclassz00_2268 =
											VECTOR_REF(BgL_arg1815z00_2276, BgL_arg1816z00_2277);
									}
									{	/* Fxop/walk.scm 154 */
										bool_t BgL__ortest_1115z00_2269;

										BgL__ortest_1115z00_2269 =
											(BgL_classz00_2252 == BgL_oclassz00_2268);
										if (BgL__ortest_1115z00_2269)
											{	/* Fxop/walk.scm 154 */
												BgL_res1899z00_2285 = BgL__ortest_1115z00_2269;
											}
										else
											{	/* Fxop/walk.scm 154 */
												long BgL_odepthz00_2270;

												{	/* Fxop/walk.scm 154 */
													obj_t BgL_arg1804z00_2271;

													BgL_arg1804z00_2271 = (BgL_oclassz00_2268);
													BgL_odepthz00_2270 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2271);
												}
												if ((3L < BgL_odepthz00_2270))
													{	/* Fxop/walk.scm 154 */
														obj_t BgL_arg1802z00_2273;

														{	/* Fxop/walk.scm 154 */
															obj_t BgL_arg1803z00_2274;

															BgL_arg1803z00_2274 = (BgL_oclassz00_2268);
															BgL_arg1802z00_2273 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2274,
																3L);
														}
														BgL_res1899z00_2285 =
															(BgL_arg1802z00_2273 == BgL_classz00_2252);
													}
												else
													{	/* Fxop/walk.scm 154 */
														BgL_res1899z00_2285 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2009z00_3461 = BgL_res1899z00_2285;
							}
					}
				}
				if (BgL_test2009z00_3461)
					{	/* Fxop/walk.scm 154 */
						if ((bgl_list_length(BgL_bindingsz00_1664) == 2L))
							{	/* Fxop/walk.scm 156 */
								bool_t BgL_test2014z00_3488;

								{	/* Fxop/walk.scm 156 */
									bool_t BgL_test2015z00_3489;

									{	/* Fxop/walk.scm 156 */
										obj_t BgL_arg1585z00_1674;

										{	/* Fxop/walk.scm 156 */
											obj_t BgL_pairz00_2290;

											BgL_pairz00_2290 = CAR(((obj_t) BgL_bindingsz00_1664));
											BgL_arg1585z00_1674 = CDR(BgL_pairz00_2290);
										}
										BgL_test2015z00_3489 =
											BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00
											(BgL_arg1585z00_1674);
									}
									if (BgL_test2015z00_3489)
										{	/* Fxop/walk.scm 157 */
											obj_t BgL_arg1584z00_1673;

											{	/* Fxop/walk.scm 157 */
												obj_t BgL_pairz00_2296;

												{	/* Fxop/walk.scm 157 */
													obj_t BgL_pairz00_2295;

													BgL_pairz00_2295 =
														CDR(((obj_t) BgL_bindingsz00_1664));
													BgL_pairz00_2296 = CAR(BgL_pairz00_2295);
												}
												BgL_arg1584z00_1673 = CDR(BgL_pairz00_2296);
											}
											BgL_test2014z00_3488 =
												BGl_bintzd2ze3longzf3ze70z25zzfxop_walkz00
												(BgL_arg1584z00_1673);
										}
									else
										{	/* Fxop/walk.scm 156 */
											BgL_test2014z00_3488 = ((bool_t) 0);
										}
								}
								if (BgL_test2014z00_3488)
									{	/* Fxop/walk.scm 158 */
										obj_t BgL_arg1575z00_1670;

										{	/* Fxop/walk.scm 158 */
											obj_t BgL_pairz00_2298;

											BgL_pairz00_2298 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_bodyz00_1665)))->
												BgL_argsz00);
											BgL_arg1575z00_1670 = CAR(BgL_pairz00_2298);
										}
										return
											BGl_findzd2fxopze70z35zzfxop_walkz00(BgL_arg1575z00_1670);
									}
								else
									{	/* Fxop/walk.scm 156 */
										return BFALSE;
									}
							}
						else
							{	/* Fxop/walk.scm 155 */
								return BFALSE;
							}
					}
				else
					{	/* Fxop/walk.scm 154 */
						return BFALSE;
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzfxop_walkz00(void)
	{
		{	/* Fxop/walk.scm 21 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1920z00zzfxop_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
