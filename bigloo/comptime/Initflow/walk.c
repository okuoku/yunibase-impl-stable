/*===========================================================================*/
/*   (Initflow/walk.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Initflow/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INITFLOW_WALK_TYPE_DEFINITIONS
#define BGL_INITFLOW_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2iflowzf2_bgl
	{
	}                      *BgL_sfunzf2iflowzf2_bglt;


#endif													// BGL_INITFLOW_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzinitflow_walkz00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_sfunzf2iflowzf2zzinitflow_walkz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzinitflow_walkz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_z62initflowzd2nodezd2letzd2fu1368zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzinitflow_walkz00(void);
	static obj_t BGl_z62initflowzd2nodezd2letzd2va1371zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2node1337zb0zzinitflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzinitflow_walkz00(void);
	static obj_t BGl_z62initflowzd2nodezd2var1340z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2walkz12za2zzinitflow_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2appzd2ly1348zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzinitflow_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinitflow_walkz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62initflowzd2nodezd2app1346z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_initflowzd2funzd2zzinitflow_walkz00(BgL_variablez00_bglt,
		bool_t);
	static obj_t BGl_z62initflowzd2nodezb0zzinitflow_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62initflowzd2nodezd2funcal1350z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2setzd2ex1374zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_initflowzd2walkz12zc0zzinitflow_walkz00(obj_t);
	static obj_t BGl_lubz00zzinitflow_walkz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62initflowzd2nodezd2extern1352z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t BGl_z62initflowzd2nodezd2condit1356z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2switch1360z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzinitflow_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern bool_t BGl_globalzd2readzd2onlyzf3zf3zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62initflowzd2nodezd2boxzd2re1364zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getzd2classzd2typez00zztype_cachez00(void);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62initflowzd2nodezd2makezd2b1362zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzinitflow_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinitflow_walkz00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzinitflow_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinitflow_walkz00(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_initflowzd2nodezd2zzinitflow_walkz00(BgL_nodez00_bglt,
		bool_t);
	static obj_t BGl_z62initflowzd2nodezd2boxzd2se1366zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2jumpzd2e1377zb0zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_sfunz00_bglt BGl_z62lambda1723z62zzinitflow_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzinitflow_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_sfunz00_bglt BGl_z62lambda1734z62zzinitflow_walkz00(obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1737z62zzinitflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2setq1354z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_initflowzd2nodeza2z70zzinitflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2sync1344z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62initflowzd2nodezd2fail1358z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62initflowzd2nodezd2sequen1342z62zzinitflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2032z00zzinitflow_walkz00,
		BgL_bgl_za762lambda1737za7622070z00, BGl_z62lambda1737z62zzinitflow_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2033z00zzinitflow_walkz00,
		BgL_bgl_za762za7c3za704anonymo2071za7,
		BGl_z62zc3z04anonymousza31736ze3ze5zzinitflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2034z00zzinitflow_walkz00,
		BgL_bgl_za762lambda1734za7622072z00, BGl_z62lambda1734z62zzinitflow_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2035z00zzinitflow_walkz00,
		BgL_bgl_za762lambda1723za7622073z00, BGl_z62lambda1723z62zzinitflow_walkz00,
		0L, BUNSPEC, 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2036z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2074z00,
		BGl_z62initflowzd2node1337zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2038z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2075z00,
		BGl_z62initflowzd2nodezd2var1340z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2040z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2076z00,
		BGl_z62initflowzd2nodezd2sequen1342z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2041z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2077z00,
		BGl_z62initflowzd2nodezd2sync1344z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2042z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2078z00,
		BGl_z62initflowzd2nodezd2app1346z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2043z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2079z00,
		BGl_z62initflowzd2nodezd2appzd2ly1348zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2044z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2080z00,
		BGl_z62initflowzd2nodezd2funcal1350z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2045z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2081z00,
		BGl_z62initflowzd2nodezd2extern1352z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2046z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2082z00,
		BGl_z62initflowzd2nodezd2setq1354z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2047z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2083z00,
		BGl_z62initflowzd2nodezd2condit1356z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2048z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2084z00,
		BGl_z62initflowzd2nodezd2fail1358z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2085z00,
		BGl_z62initflowzd2nodezd2switch1360z62zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2057z00zzinitflow_walkz00,
		BgL_bgl_string2057za700za7za7i2086za7,
		"Typed global variable used before initialized", 45);
	      DEFINE_STRING(BGl_string2058z00zzinitflow_walkz00,
		BgL_bgl_string2058za700za7za7i2087za7, "initflow_walk", 13);
	      DEFINE_STRING(BGl_string2059z00zzinitflow_walkz00,
		BgL_bgl_string2059za700za7za7i2088za7,
		"no-init-flow _ initflow_walk sfun/iflow (object-init toplevel-init generic-init method-init) read pass-started ",
		111);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2050z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2089z00,
		BGl_z62initflowzd2nodezd2makezd2b1362zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2051z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2090z00,
		BGl_z62initflowzd2nodezd2boxzd2re1364zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2091z00,
		BGl_z62initflowzd2nodezd2boxzd2se1366zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2053z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2092z00,
		BGl_z62initflowzd2nodezd2letzd2fu1368zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2054z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2093z00,
		BGl_z62initflowzd2nodezd2letzd2va1371zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2094z00,
		BGl_z62initflowzd2nodezd2setzd2ex1374zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2095z00,
		BGl_z62initflowzd2nodezd2jumpzd2e1377zb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initflowzd2walkz12zd2envz12zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2wa2096z00,
		BGl_z62initflowzd2walkz12za2zzinitflow_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
		BgL_bgl_za762initflowza7d2no2097z00,
		BGl_z62initflowzd2nodezb0zzinitflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2023z00zzinitflow_walkz00,
		BgL_bgl_string2023za700za7za7i2098za7, "Initflow", 8);
	      DEFINE_STRING(BGl_string2024z00zzinitflow_walkz00,
		BgL_bgl_string2024za700za7za7i2099za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2025z00zzinitflow_walkz00,
		BgL_bgl_string2025za700za7za7i2100za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2026z00zzinitflow_walkz00,
		BgL_bgl_string2026za700za7za7i2101za7, "      uninitialized globals : ",
		30);
	      DEFINE_STRING(BGl_string2027z00zzinitflow_walkz00,
		BgL_bgl_string2027za700za7za7i2102za7, " error", 6);
	      DEFINE_STRING(BGl_string2028z00zzinitflow_walkz00,
		BgL_bgl_string2028za700za7za7i2103za7, "s", 1);
	      DEFINE_STRING(BGl_string2029z00zzinitflow_walkz00,
		BgL_bgl_string2029za700za7za7i2104za7, "", 0);
	      DEFINE_STRING(BGl_string2030z00zzinitflow_walkz00,
		BgL_bgl_string2030za700za7za7i2105za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2031z00zzinitflow_walkz00,
		BgL_bgl_string2031za700za7za7i2106za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string2037z00zzinitflow_walkz00,
		BgL_bgl_string2037za700za7za7i2107za7, "initflow-node1337", 17);
	      DEFINE_STRING(BGl_string2039z00zzinitflow_walkz00,
		BgL_bgl_string2039za700za7za7i2108za7, "initflow-node", 13);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzinitflow_walkz00));
		     ADD_ROOT((void *) (&BGl_sfunzf2iflowzf2zzinitflow_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinitflow_walkz00(long
		BgL_checksumz00_3116, char *BgL_fromz00_3117)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinitflow_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzinitflow_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinitflow_walkz00();
					BGl_libraryzd2moduleszd2initz00zzinitflow_walkz00();
					BGl_cnstzd2initzd2zzinitflow_walkz00();
					BGl_importedzd2moduleszd2initz00zzinitflow_walkz00();
					BGl_objectzd2initzd2zzinitflow_walkz00();
					BGl_genericzd2initzd2zzinitflow_walkz00();
					BGl_methodzd2initzd2zzinitflow_walkz00();
					return BGl_toplevelzd2initzd2zzinitflow_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"initflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"initflow_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			{	/* Initflow/walk.scm 27 */
				obj_t BgL_cportz00_2903;

				{	/* Initflow/walk.scm 27 */
					obj_t BgL_stringz00_2910;

					BgL_stringz00_2910 = BGl_string2059z00zzinitflow_walkz00;
					{	/* Initflow/walk.scm 27 */
						obj_t BgL_startz00_2911;

						BgL_startz00_2911 = BINT(0L);
						{	/* Initflow/walk.scm 27 */
							obj_t BgL_endz00_2912;

							BgL_endz00_2912 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2910)));
							{	/* Initflow/walk.scm 27 */

								BgL_cportz00_2903 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2910, BgL_startz00_2911, BgL_endz00_2912);
				}}}}
				{
					long BgL_iz00_2904;

					BgL_iz00_2904 = 6L;
				BgL_loopz00_2905:
					if ((BgL_iz00_2904 == -1L))
						{	/* Initflow/walk.scm 27 */
							return BUNSPEC;
						}
					else
						{	/* Initflow/walk.scm 27 */
							{	/* Initflow/walk.scm 27 */
								obj_t BgL_arg2060z00_2906;

								{	/* Initflow/walk.scm 27 */

									{	/* Initflow/walk.scm 27 */
										obj_t BgL_locationz00_2908;

										BgL_locationz00_2908 = BBOOL(((bool_t) 0));
										{	/* Initflow/walk.scm 27 */

											BgL_arg2060z00_2906 =
												BGl_readz00zz__readerz00(BgL_cportz00_2903,
												BgL_locationz00_2908);
										}
									}
								}
								{	/* Initflow/walk.scm 27 */
									int BgL_tmpz00_3150;

									BgL_tmpz00_3150 = (int) (BgL_iz00_2904);
									CNST_TABLE_SET(BgL_tmpz00_3150, BgL_arg2060z00_2906);
							}}
							{	/* Initflow/walk.scm 27 */
								int BgL_auxz00_2909;

								BgL_auxz00_2909 = (int) ((BgL_iz00_2904 - 1L));
								{
									long BgL_iz00_3155;

									BgL_iz00_3155 = (long) (BgL_auxz00_2909);
									BgL_iz00_2904 = BgL_iz00_3155;
									goto BgL_loopz00_2905;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzinitflow_walkz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1475;

				BgL_headz00_1475 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1476;
					obj_t BgL_tailz00_1477;

					BgL_prevz00_1476 = BgL_headz00_1475;
					BgL_tailz00_1477 = BgL_l1z00_1;
				BgL_loopz00_1478:
					if (PAIRP(BgL_tailz00_1477))
						{
							obj_t BgL_newzd2prevzd2_1480;

							BgL_newzd2prevzd2_1480 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1477), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1476, BgL_newzd2prevzd2_1480);
							{
								obj_t BgL_tailz00_3165;
								obj_t BgL_prevz00_3164;

								BgL_prevz00_3164 = BgL_newzd2prevzd2_1480;
								BgL_tailz00_3165 = CDR(BgL_tailz00_1477);
								BgL_tailz00_1477 = BgL_tailz00_3165;
								BgL_prevz00_1476 = BgL_prevz00_3164;
								goto BgL_loopz00_1478;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1475);
				}
			}
		}

	}



/* initflow-walk! */
	BGL_EXPORTED_DEF obj_t BGl_initflowzd2walkz12zc0zzinitflow_walkz00(obj_t
		BgL_globalsz00_80)
	{
		{	/* Initflow/walk.scm 48 */
			{	/* Initflow/walk.scm 49 */
				obj_t BgL_list1423z00_1489;

				{	/* Initflow/walk.scm 49 */
					obj_t BgL_arg1434z00_1490;

					{	/* Initflow/walk.scm 49 */
						obj_t BgL_arg1437z00_1491;

						BgL_arg1437z00_1491 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1434z00_1490 =
							MAKE_YOUNG_PAIR(BGl_string2023z00zzinitflow_walkz00,
							BgL_arg1437z00_1491);
					}
					BgL_list1423z00_1489 =
						MAKE_YOUNG_PAIR(BGl_string2024z00zzinitflow_walkz00,
						BgL_arg1434z00_1490);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1423z00_1489);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2023z00zzinitflow_walkz00;
			{	/* Initflow/walk.scm 49 */
				obj_t BgL_g1143z00_1492;

				BgL_g1143z00_1492 = BNIL;
				{
					obj_t BgL_hooksz00_1495;
					obj_t BgL_hnamesz00_1496;

					BgL_hooksz00_1495 = BgL_g1143z00_1492;
					BgL_hnamesz00_1496 = BNIL;
				BgL_zc3z04anonymousza31438ze3z87_1497:
					if (NULLP(BgL_hooksz00_1495))
						{	/* Initflow/walk.scm 49 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Initflow/walk.scm 49 */
							bool_t BgL_test2113z00_3178;

							{	/* Initflow/walk.scm 49 */
								obj_t BgL_fun1456z00_1504;

								BgL_fun1456z00_1504 = CAR(((obj_t) BgL_hooksz00_1495));
								BgL_test2113z00_3178 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1456z00_1504));
							}
							if (BgL_test2113z00_3178)
								{	/* Initflow/walk.scm 49 */
									obj_t BgL_arg1448z00_1501;
									obj_t BgL_arg1453z00_1502;

									BgL_arg1448z00_1501 = CDR(((obj_t) BgL_hooksz00_1495));
									BgL_arg1453z00_1502 = CDR(((obj_t) BgL_hnamesz00_1496));
									{
										obj_t BgL_hnamesz00_3190;
										obj_t BgL_hooksz00_3189;

										BgL_hooksz00_3189 = BgL_arg1448z00_1501;
										BgL_hnamesz00_3190 = BgL_arg1453z00_1502;
										BgL_hnamesz00_1496 = BgL_hnamesz00_3190;
										BgL_hooksz00_1495 = BgL_hooksz00_3189;
										goto BgL_zc3z04anonymousza31438ze3z87_1497;
									}
								}
							else
								{	/* Initflow/walk.scm 49 */
									obj_t BgL_arg1454z00_1503;

									BgL_arg1454z00_1503 = CAR(((obj_t) BgL_hnamesz00_1496));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2023z00zzinitflow_walkz00,
										BGl_string2025z00zzinitflow_walkz00, BgL_arg1454z00_1503);
								}
						}
				}
			}
			{
				obj_t BgL_l1294z00_1508;

				BgL_l1294z00_1508 = BgL_globalsz00_80;
			BgL_zc3z04anonymousza31458ze3z87_1509:
				if (PAIRP(BgL_l1294z00_1508))
					{	/* Initflow/walk.scm 53 */
						{	/* Initflow/walk.scm 54 */
							obj_t BgL_gz00_1511;

							BgL_gz00_1511 = CAR(BgL_l1294z00_1508);
							if (
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_1511))))->
										BgL_accessz00) == CNST_TABLE_REF(1)))
								{	/* Initflow/walk.scm 55 */
									BgL_valuez00_bglt BgL_valuez00_1514;

									BgL_valuez00_1514 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_1511))))->
										BgL_valuez00);
									{	/* Initflow/walk.scm 56 */
										bool_t BgL_test2116z00_3206;

										{	/* Initflow/walk.scm 56 */
											obj_t BgL_classz00_2199;

											BgL_classz00_2199 = BGl_sfunz00zzast_varz00;
											{	/* Initflow/walk.scm 56 */
												BgL_objectz00_bglt BgL_arg1807z00_2201;

												{	/* Initflow/walk.scm 56 */
													obj_t BgL_tmpz00_3207;

													BgL_tmpz00_3207 =
														((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_1514));
													BgL_arg1807z00_2201 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3207);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Initflow/walk.scm 56 */
														long BgL_idxz00_2207;

														BgL_idxz00_2207 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2201);
														BgL_test2116z00_3206 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2207 + 3L)) == BgL_classz00_2199);
													}
												else
													{	/* Initflow/walk.scm 56 */
														bool_t BgL_res2001z00_2232;

														{	/* Initflow/walk.scm 56 */
															obj_t BgL_oclassz00_2215;

															{	/* Initflow/walk.scm 56 */
																obj_t BgL_arg1815z00_2223;
																long BgL_arg1816z00_2224;

																BgL_arg1815z00_2223 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Initflow/walk.scm 56 */
																	long BgL_arg1817z00_2225;

																	BgL_arg1817z00_2225 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2201);
																	BgL_arg1816z00_2224 =
																		(BgL_arg1817z00_2225 - OBJECT_TYPE);
																}
																BgL_oclassz00_2215 =
																	VECTOR_REF(BgL_arg1815z00_2223,
																	BgL_arg1816z00_2224);
															}
															{	/* Initflow/walk.scm 56 */
																bool_t BgL__ortest_1115z00_2216;

																BgL__ortest_1115z00_2216 =
																	(BgL_classz00_2199 == BgL_oclassz00_2215);
																if (BgL__ortest_1115z00_2216)
																	{	/* Initflow/walk.scm 56 */
																		BgL_res2001z00_2232 =
																			BgL__ortest_1115z00_2216;
																	}
																else
																	{	/* Initflow/walk.scm 56 */
																		long BgL_odepthz00_2217;

																		{	/* Initflow/walk.scm 56 */
																			obj_t BgL_arg1804z00_2218;

																			BgL_arg1804z00_2218 =
																				(BgL_oclassz00_2215);
																			BgL_odepthz00_2217 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2218);
																		}
																		if ((3L < BgL_odepthz00_2217))
																			{	/* Initflow/walk.scm 56 */
																				obj_t BgL_arg1802z00_2220;

																				{	/* Initflow/walk.scm 56 */
																					obj_t BgL_arg1803z00_2221;

																					BgL_arg1803z00_2221 =
																						(BgL_oclassz00_2215);
																					BgL_arg1802z00_2220 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2221, 3L);
																				}
																				BgL_res2001z00_2232 =
																					(BgL_arg1802z00_2220 ==
																					BgL_classz00_2199);
																			}
																		else
																			{	/* Initflow/walk.scm 56 */
																				BgL_res2001z00_2232 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2116z00_3206 = BgL_res2001z00_2232;
													}
											}
										}
										if (BgL_test2116z00_3206)
											{	/* Initflow/walk.scm 56 */
												((((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_1511)))->
														BgL_initz00) = ((obj_t) BTRUE), BUNSPEC);
											}
										else
											{	/* Initflow/walk.scm 56 */
												BFALSE;
											}
									}
								}
							else
								{	/* Initflow/walk.scm 54 */
									BFALSE;
								}
						}
						{
							obj_t BgL_l1294z00_3232;

							BgL_l1294z00_3232 = CDR(BgL_l1294z00_1508);
							BgL_l1294z00_1508 = BgL_l1294z00_3232;
							goto BgL_zc3z04anonymousza31458ze3z87_1509;
						}
					}
				else
					{	/* Initflow/walk.scm 53 */
						((bool_t) 1);
					}
			}
			{	/* Initflow/walk.scm 60 */
				obj_t BgL_g1309z00_1519;

				BgL_g1309z00_1519 = CNST_TABLE_REF(2);
				{
					obj_t BgL_l1307z00_1521;

					BgL_l1307z00_1521 = BgL_g1309z00_1519;
				BgL_zc3z04anonymousza31490ze3z87_1522:
					if (PAIRP(BgL_l1307z00_1521))
						{	/* Initflow/walk.scm 72 */
							{	/* Initflow/walk.scm 61 */
								obj_t BgL_idz00_1524;

								BgL_idz00_1524 = CAR(BgL_l1307z00_1521);
								{	/* Initflow/walk.scm 61 */
									obj_t BgL_rootz00_1525;

									BgL_rootz00_1525 =
										BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_1524,
										BGl_za2moduleza2z00zzmodule_modulez00);
									{	/* Initflow/walk.scm 62 */
										bool_t BgL_test2121z00_3239;

										{	/* Initflow/walk.scm 62 */
											obj_t BgL_classz00_2236;

											BgL_classz00_2236 = BGl_globalz00zzast_varz00;
											if (BGL_OBJECTP(BgL_rootz00_1525))
												{	/* Initflow/walk.scm 62 */
													BgL_objectz00_bglt BgL_arg1807z00_2238;

													BgL_arg1807z00_2238 =
														(BgL_objectz00_bglt) (BgL_rootz00_1525);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Initflow/walk.scm 62 */
															long BgL_idxz00_2244;

															BgL_idxz00_2244 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2238);
															BgL_test2121z00_3239 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2244 + 2L)) == BgL_classz00_2236);
														}
													else
														{	/* Initflow/walk.scm 62 */
															bool_t BgL_res2002z00_2269;

															{	/* Initflow/walk.scm 62 */
																obj_t BgL_oclassz00_2252;

																{	/* Initflow/walk.scm 62 */
																	obj_t BgL_arg1815z00_2260;
																	long BgL_arg1816z00_2261;

																	BgL_arg1815z00_2260 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Initflow/walk.scm 62 */
																		long BgL_arg1817z00_2262;

																		BgL_arg1817z00_2262 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2238);
																		BgL_arg1816z00_2261 =
																			(BgL_arg1817z00_2262 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2252 =
																		VECTOR_REF(BgL_arg1815z00_2260,
																		BgL_arg1816z00_2261);
																}
																{	/* Initflow/walk.scm 62 */
																	bool_t BgL__ortest_1115z00_2253;

																	BgL__ortest_1115z00_2253 =
																		(BgL_classz00_2236 == BgL_oclassz00_2252);
																	if (BgL__ortest_1115z00_2253)
																		{	/* Initflow/walk.scm 62 */
																			BgL_res2002z00_2269 =
																				BgL__ortest_1115z00_2253;
																		}
																	else
																		{	/* Initflow/walk.scm 62 */
																			long BgL_odepthz00_2254;

																			{	/* Initflow/walk.scm 62 */
																				obj_t BgL_arg1804z00_2255;

																				BgL_arg1804z00_2255 =
																					(BgL_oclassz00_2252);
																				BgL_odepthz00_2254 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2255);
																			}
																			if ((2L < BgL_odepthz00_2254))
																				{	/* Initflow/walk.scm 62 */
																					obj_t BgL_arg1802z00_2257;

																					{	/* Initflow/walk.scm 62 */
																						obj_t BgL_arg1803z00_2258;

																						BgL_arg1803z00_2258 =
																							(BgL_oclassz00_2252);
																						BgL_arg1802z00_2257 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2258, 2L);
																					}
																					BgL_res2002z00_2269 =
																						(BgL_arg1802z00_2257 ==
																						BgL_classz00_2236);
																				}
																			else
																				{	/* Initflow/walk.scm 62 */
																					BgL_res2002z00_2269 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2121z00_3239 = BgL_res2002z00_2269;
														}
												}
											else
												{	/* Initflow/walk.scm 62 */
													BgL_test2121z00_3239 = ((bool_t) 0);
												}
										}
										if (BgL_test2121z00_3239)
											{	/* Initflow/walk.scm 62 */
												BGl_initflowzd2funzd2zzinitflow_walkz00(
													((BgL_variablez00_bglt) BgL_rootz00_1525),
													((bool_t) 1));
												{	/* Initflow/walk.scm 64 */
													obj_t BgL_lz00_1527;

													{	/* Initflow/walk.scm 64 */
														obj_t BgL_hook1300z00_1549;

														BgL_hook1300z00_1549 =
															MAKE_YOUNG_PAIR(BFALSE, BNIL);
														{
															obj_t BgL_l1297z00_1551;
															obj_t BgL_h1298z00_1552;

															BgL_l1297z00_1551 = BgL_globalsz00_80;
															BgL_h1298z00_1552 = BgL_hook1300z00_1549;
														BgL_zc3z04anonymousza31553ze3z87_1553:
															if (NULLP(BgL_l1297z00_1551))
																{	/* Initflow/walk.scm 64 */
																	BgL_lz00_1527 = CDR(BgL_hook1300z00_1549);
																}
															else
																{	/* Initflow/walk.scm 64 */
																	bool_t BgL_test2127z00_3268;

																	{	/* Initflow/walk.scm 65 */
																		obj_t BgL_gz00_1566;

																		BgL_gz00_1566 =
																			CAR(((obj_t) BgL_l1297z00_1551));
																		{	/* Initflow/walk.scm 65 */
																			bool_t BgL_test2128z00_3271;

																			{	/* Initflow/walk.scm 65 */
																				BgL_valuez00_bglt BgL_arg1584z00_1570;

																				BgL_arg1584z00_1570 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_gz00_1566))))->
																					BgL_valuez00);
																				{	/* Initflow/walk.scm 65 */
																					obj_t BgL_classz00_2273;

																					BgL_classz00_2273 =
																						BGl_sfunz00zzast_varz00;
																					{	/* Initflow/walk.scm 65 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2275;
																						{	/* Initflow/walk.scm 65 */
																							obj_t BgL_tmpz00_3275;

																							BgL_tmpz00_3275 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_arg1584z00_1570));
																							BgL_arg1807z00_2275 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_3275);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Initflow/walk.scm 65 */
																								long BgL_idxz00_2281;

																								BgL_idxz00_2281 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2275);
																								BgL_test2128z00_3271 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2281 + 3L)) ==
																									BgL_classz00_2273);
																							}
																						else
																							{	/* Initflow/walk.scm 65 */
																								bool_t BgL_res2003z00_2306;

																								{	/* Initflow/walk.scm 65 */
																									obj_t BgL_oclassz00_2289;

																									{	/* Initflow/walk.scm 65 */
																										obj_t BgL_arg1815z00_2297;
																										long BgL_arg1816z00_2298;

																										BgL_arg1815z00_2297 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Initflow/walk.scm 65 */
																											long BgL_arg1817z00_2299;

																											BgL_arg1817z00_2299 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2275);
																											BgL_arg1816z00_2298 =
																												(BgL_arg1817z00_2299 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2289 =
																											VECTOR_REF
																											(BgL_arg1815z00_2297,
																											BgL_arg1816z00_2298);
																									}
																									{	/* Initflow/walk.scm 65 */
																										bool_t
																											BgL__ortest_1115z00_2290;
																										BgL__ortest_1115z00_2290 =
																											(BgL_classz00_2273 ==
																											BgL_oclassz00_2289);
																										if (BgL__ortest_1115z00_2290)
																											{	/* Initflow/walk.scm 65 */
																												BgL_res2003z00_2306 =
																													BgL__ortest_1115z00_2290;
																											}
																										else
																											{	/* Initflow/walk.scm 65 */
																												long BgL_odepthz00_2291;

																												{	/* Initflow/walk.scm 65 */
																													obj_t
																														BgL_arg1804z00_2292;
																													BgL_arg1804z00_2292 =
																														(BgL_oclassz00_2289);
																													BgL_odepthz00_2291 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2292);
																												}
																												if (
																													(3L <
																														BgL_odepthz00_2291))
																													{	/* Initflow/walk.scm 65 */
																														obj_t
																															BgL_arg1802z00_2294;
																														{	/* Initflow/walk.scm 65 */
																															obj_t
																																BgL_arg1803z00_2295;
																															BgL_arg1803z00_2295
																																=
																																(BgL_oclassz00_2289);
																															BgL_arg1802z00_2294
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2295,
																																3L);
																														}
																														BgL_res2003z00_2306
																															=
																															(BgL_arg1802z00_2294
																															==
																															BgL_classz00_2273);
																													}
																												else
																													{	/* Initflow/walk.scm 65 */
																														BgL_res2003z00_2306
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2128z00_3271 =
																									BgL_res2003z00_2306;
																							}
																					}
																				}
																			}
																			if (BgL_test2128z00_3271)
																				{	/* Initflow/walk.scm 65 */
																					BgL_test2127z00_3268 = ((bool_t) 0);
																				}
																			else
																				{	/* Initflow/walk.scm 65 */
																					if (
																						((((BgL_globalz00_bglt) COBJECT(
																										((BgL_globalz00_bglt)
																											BgL_gz00_1566)))->
																								BgL_initz00) == BTRUE))
																						{	/* Initflow/walk.scm 66 */
																							BgL_test2127z00_3268 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Initflow/walk.scm 66 */
																							BgL_test2127z00_3268 =
																								((bool_t) 1);
																						}
																				}
																		}
																	}
																	if (BgL_test2127z00_3268)
																		{	/* Initflow/walk.scm 64 */
																			obj_t BgL_nh1299z00_1562;

																			{	/* Initflow/walk.scm 64 */
																				obj_t BgL_arg1573z00_1564;

																				BgL_arg1573z00_1564 =
																					CAR(((obj_t) BgL_l1297z00_1551));
																				BgL_nh1299z00_1562 =
																					MAKE_YOUNG_PAIR(BgL_arg1573z00_1564,
																					BNIL);
																			}
																			SET_CDR(BgL_h1298z00_1552,
																				BgL_nh1299z00_1562);
																			{	/* Initflow/walk.scm 64 */
																				obj_t BgL_arg1571z00_1563;

																				BgL_arg1571z00_1563 =
																					CDR(((obj_t) BgL_l1297z00_1551));
																				{
																					obj_t BgL_h1298z00_3309;
																					obj_t BgL_l1297z00_3308;

																					BgL_l1297z00_3308 =
																						BgL_arg1571z00_1563;
																					BgL_h1298z00_3309 =
																						BgL_nh1299z00_1562;
																					BgL_h1298z00_1552 = BgL_h1298z00_3309;
																					BgL_l1297z00_1551 = BgL_l1297z00_3308;
																					goto
																						BgL_zc3z04anonymousza31553ze3z87_1553;
																				}
																			}
																		}
																	else
																		{	/* Initflow/walk.scm 64 */
																			obj_t BgL_arg1575z00_1565;

																			BgL_arg1575z00_1565 =
																				CDR(((obj_t) BgL_l1297z00_1551));
																			{
																				obj_t BgL_l1297z00_3312;

																				BgL_l1297z00_3312 = BgL_arg1575z00_1565;
																				BgL_l1297z00_1551 = BgL_l1297z00_3312;
																				goto
																					BgL_zc3z04anonymousza31553ze3z87_1553;
																			}
																		}
																}
														}
													}
													if (PAIRP(BgL_lz00_1527))
														{	/* Initflow/walk.scm 70 */
															obj_t BgL_arg1502z00_1529;

															{	/* Initflow/walk.scm 70 */
																obj_t BgL_head1303z00_1535;

																BgL_head1303z00_1535 =
																	MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00
																	(CAR(BgL_lz00_1527)), BNIL);
																{	/* Initflow/walk.scm 70 */
																	obj_t BgL_g1306z00_1536;

																	BgL_g1306z00_1536 = CDR(BgL_lz00_1527);
																	{
																		obj_t BgL_l1301z00_1538;
																		obj_t BgL_tail1304z00_1539;

																		BgL_l1301z00_1538 = BgL_g1306z00_1536;
																		BgL_tail1304z00_1539 = BgL_head1303z00_1535;
																	BgL_zc3z04anonymousza31515ze3z87_1540:
																		if (NULLP(BgL_l1301z00_1538))
																			{	/* Initflow/walk.scm 70 */
																				BgL_arg1502z00_1529 =
																					BgL_head1303z00_1535;
																			}
																		else
																			{	/* Initflow/walk.scm 70 */
																				obj_t BgL_newtail1305z00_1542;

																				{	/* Initflow/walk.scm 70 */
																					obj_t BgL_arg1540z00_1544;

																					{	/* Initflow/walk.scm 70 */
																						obj_t BgL_arg1544z00_1545;

																						BgL_arg1544z00_1545 =
																							CAR(((obj_t) BgL_l1301z00_1538));
																						BgL_arg1540z00_1544 =
																							BGl_shapez00zztools_shapez00
																							(BgL_arg1544z00_1545);
																					}
																					BgL_newtail1305z00_1542 =
																						MAKE_YOUNG_PAIR(BgL_arg1540z00_1544,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1304z00_1539,
																					BgL_newtail1305z00_1542);
																				{	/* Initflow/walk.scm 70 */
																					obj_t BgL_arg1535z00_1543;

																					BgL_arg1535z00_1543 =
																						CDR(((obj_t) BgL_l1301z00_1538));
																					{
																						obj_t BgL_tail1304z00_3329;
																						obj_t BgL_l1301z00_3328;

																						BgL_l1301z00_3328 =
																							BgL_arg1535z00_1543;
																						BgL_tail1304z00_3329 =
																							BgL_newtail1305z00_1542;
																						BgL_tail1304z00_1539 =
																							BgL_tail1304z00_3329;
																						BgL_l1301z00_1538 =
																							BgL_l1301z00_3328;
																						goto
																							BgL_zc3z04anonymousza31515ze3z87_1540;
																					}
																				}
																			}
																	}
																}
															}
															{	/* Initflow/walk.scm 69 */
																obj_t BgL_list1503z00_1530;

																{	/* Initflow/walk.scm 69 */
																	obj_t BgL_arg1509z00_1531;

																	{	/* Initflow/walk.scm 69 */
																		obj_t BgL_arg1513z00_1532;

																		BgL_arg1513z00_1532 =
																			MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																					10)), BNIL);
																		BgL_arg1509z00_1531 =
																			MAKE_YOUNG_PAIR(BgL_arg1502z00_1529,
																			BgL_arg1513z00_1532);
																	}
																	BgL_list1503z00_1530 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2026z00zzinitflow_walkz00,
																		BgL_arg1509z00_1531);
																}
																BGl_verbosez00zztools_speekz00(BINT(2L),
																	BgL_list1503z00_1530);
														}}
													else
														{	/* Initflow/walk.scm 68 */
															BFALSE;
														}
												}
											}
										else
											{	/* Initflow/walk.scm 62 */
												BFALSE;
											}
									}
								}
							}
							{
								obj_t BgL_l1307z00_3336;

								BgL_l1307z00_3336 = CDR(BgL_l1307z00_1521);
								BgL_l1307z00_1521 = BgL_l1307z00_3336;
								goto BgL_zc3z04anonymousza31490ze3z87_1522;
							}
						}
					else
						{	/* Initflow/walk.scm 72 */
							((bool_t) 1);
						}
				}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Initflow/walk.scm 74 */
					{	/* Initflow/walk.scm 74 */
						obj_t BgL_port1310z00_1576;

						{	/* Initflow/walk.scm 74 */
							obj_t BgL_tmpz00_3341;

							BgL_tmpz00_3341 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1310z00_1576 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3341);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1310z00_1576);
						bgl_display_string(BGl_string2027z00zzinitflow_walkz00,
							BgL_port1310z00_1576);
						{	/* Initflow/walk.scm 74 */
							obj_t BgL_arg1589z00_1577;

							{	/* Initflow/walk.scm 74 */
								bool_t BgL_test2136z00_3346;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Initflow/walk.scm 74 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Initflow/walk.scm 74 */
												BgL_test2136z00_3346 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Initflow/walk.scm 74 */
												BgL_test2136z00_3346 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Initflow/walk.scm 74 */
										BgL_test2136z00_3346 = ((bool_t) 0);
									}
								if (BgL_test2136z00_3346)
									{	/* Initflow/walk.scm 74 */
										BgL_arg1589z00_1577 = BGl_string2028z00zzinitflow_walkz00;
									}
								else
									{	/* Initflow/walk.scm 74 */
										BgL_arg1589z00_1577 = BGl_string2029z00zzinitflow_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1589z00_1577, BgL_port1310z00_1576);
						}
						bgl_display_string(BGl_string2030z00zzinitflow_walkz00,
							BgL_port1310z00_1576);
						bgl_display_char(((unsigned char) 10), BgL_port1310z00_1576);
					}
					{	/* Initflow/walk.scm 74 */
						obj_t BgL_list1595z00_1581;

						BgL_list1595z00_1581 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						return BGl_exitz00zz__errorz00(BgL_list1595z00_1581);
					}
				}
			else
				{	/* Initflow/walk.scm 74 */
					obj_t BgL_g1146z00_1582;

					BgL_g1146z00_1582 = BNIL;
					{
						obj_t BgL_hooksz00_1585;
						obj_t BgL_hnamesz00_1586;

						BgL_hooksz00_1585 = BgL_g1146z00_1582;
						BgL_hnamesz00_1586 = BNIL;
					BgL_zc3z04anonymousza31596ze3z87_1587:
						if (NULLP(BgL_hooksz00_1585))
							{	/* Initflow/walk.scm 74 */
								return BgL_globalsz00_80;
							}
						else
							{	/* Initflow/walk.scm 74 */
								bool_t BgL_test2140z00_3363;

								{	/* Initflow/walk.scm 74 */
									obj_t BgL_fun1608z00_1594;

									BgL_fun1608z00_1594 = CAR(((obj_t) BgL_hooksz00_1585));
									BgL_test2140z00_3363 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1608z00_1594));
								}
								if (BgL_test2140z00_3363)
									{	/* Initflow/walk.scm 74 */
										obj_t BgL_arg1602z00_1591;
										obj_t BgL_arg1605z00_1592;

										BgL_arg1602z00_1591 = CDR(((obj_t) BgL_hooksz00_1585));
										BgL_arg1605z00_1592 = CDR(((obj_t) BgL_hnamesz00_1586));
										{
											obj_t BgL_hnamesz00_3375;
											obj_t BgL_hooksz00_3374;

											BgL_hooksz00_3374 = BgL_arg1602z00_1591;
											BgL_hnamesz00_3375 = BgL_arg1605z00_1592;
											BgL_hnamesz00_1586 = BgL_hnamesz00_3375;
											BgL_hooksz00_1585 = BgL_hooksz00_3374;
											goto BgL_zc3z04anonymousza31596ze3z87_1587;
										}
									}
								else
									{	/* Initflow/walk.scm 74 */
										obj_t BgL_arg1606z00_1593;

										BgL_arg1606z00_1593 = CAR(((obj_t) BgL_hnamesz00_1586));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2031z00zzinitflow_walkz00, BgL_arg1606z00_1593);
									}
							}
					}
				}
		}

	}



/* &initflow-walk! */
	obj_t BGl_z62initflowzd2walkz12za2zzinitflow_walkz00(obj_t BgL_envz00_2789,
		obj_t BgL_globalsz00_2790)
	{
		{	/* Initflow/walk.scm 48 */
			return BGl_initflowzd2walkz12zc0zzinitflow_walkz00(BgL_globalsz00_2790);
		}

	}



/* initflow-fun */
	obj_t BGl_initflowzd2funzd2zzinitflow_walkz00(BgL_variablez00_bglt
		BgL_varz00_81, bool_t BgL_ez00_82)
	{
		{	/* Initflow/walk.scm 79 */
			{	/* Initflow/walk.scm 80 */
				BgL_valuez00_bglt BgL_fz00_1597;

				BgL_fz00_1597 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_81))->BgL_valuez00);
				{	/* Initflow/walk.scm 82 */
					bool_t BgL_test2141z00_3381;

					{	/* Initflow/walk.scm 82 */
						obj_t BgL_arg1625z00_1607;

						BgL_arg1625z00_1607 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_fz00_1597)))->BgL_bodyz00);
						{	/* Initflow/walk.scm 82 */
							obj_t BgL_classz00_2333;

							BgL_classz00_2333 = BGl_nodez00zzast_nodez00;
							if (BGL_OBJECTP(BgL_arg1625z00_1607))
								{	/* Initflow/walk.scm 82 */
									BgL_objectz00_bglt BgL_arg1807z00_2335;

									BgL_arg1807z00_2335 =
										(BgL_objectz00_bglt) (BgL_arg1625z00_1607);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Initflow/walk.scm 82 */
											long BgL_idxz00_2341;

											BgL_idxz00_2341 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2335);
											BgL_test2141z00_3381 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2341 + 1L)) == BgL_classz00_2333);
										}
									else
										{	/* Initflow/walk.scm 82 */
											bool_t BgL_res2005z00_2366;

											{	/* Initflow/walk.scm 82 */
												obj_t BgL_oclassz00_2349;

												{	/* Initflow/walk.scm 82 */
													obj_t BgL_arg1815z00_2357;
													long BgL_arg1816z00_2358;

													BgL_arg1815z00_2357 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Initflow/walk.scm 82 */
														long BgL_arg1817z00_2359;

														BgL_arg1817z00_2359 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2335);
														BgL_arg1816z00_2358 =
															(BgL_arg1817z00_2359 - OBJECT_TYPE);
													}
													BgL_oclassz00_2349 =
														VECTOR_REF(BgL_arg1815z00_2357,
														BgL_arg1816z00_2358);
												}
												{	/* Initflow/walk.scm 82 */
													bool_t BgL__ortest_1115z00_2350;

													BgL__ortest_1115z00_2350 =
														(BgL_classz00_2333 == BgL_oclassz00_2349);
													if (BgL__ortest_1115z00_2350)
														{	/* Initflow/walk.scm 82 */
															BgL_res2005z00_2366 = BgL__ortest_1115z00_2350;
														}
													else
														{	/* Initflow/walk.scm 82 */
															long BgL_odepthz00_2351;

															{	/* Initflow/walk.scm 82 */
																obj_t BgL_arg1804z00_2352;

																BgL_arg1804z00_2352 = (BgL_oclassz00_2349);
																BgL_odepthz00_2351 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2352);
															}
															if ((1L < BgL_odepthz00_2351))
																{	/* Initflow/walk.scm 82 */
																	obj_t BgL_arg1802z00_2354;

																	{	/* Initflow/walk.scm 82 */
																		obj_t BgL_arg1803z00_2355;

																		BgL_arg1803z00_2355 = (BgL_oclassz00_2349);
																		BgL_arg1802z00_2354 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2355, 1L);
																	}
																	BgL_res2005z00_2366 =
																		(BgL_arg1802z00_2354 == BgL_classz00_2333);
																}
															else
																{	/* Initflow/walk.scm 82 */
																	BgL_res2005z00_2366 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2141z00_3381 = BgL_res2005z00_2366;
										}
								}
							else
								{	/* Initflow/walk.scm 82 */
									BgL_test2141z00_3381 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2141z00_3381)
						{	/* Initflow/walk.scm 84 */
							bool_t BgL_test2146z00_3406;

							{	/* Initflow/walk.scm 84 */
								obj_t BgL_classz00_2367;

								BgL_classz00_2367 = BGl_sfunzf2iflowzf2zzinitflow_walkz00;
								{	/* Initflow/walk.scm 84 */
									obj_t BgL_oclassz00_2369;

									{	/* Initflow/walk.scm 84 */
										obj_t BgL_arg1815z00_2371;
										long BgL_arg1816z00_2372;

										BgL_arg1815z00_2371 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Initflow/walk.scm 84 */
											long BgL_arg1817z00_2373;

											BgL_arg1817z00_2373 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_fz00_1597));
											BgL_arg1816z00_2372 = (BgL_arg1817z00_2373 - OBJECT_TYPE);
										}
										BgL_oclassz00_2369 =
											VECTOR_REF(BgL_arg1815z00_2371, BgL_arg1816z00_2372);
									}
									BgL_test2146z00_3406 =
										(BgL_oclassz00_2369 == BgL_classz00_2367);
							}}
							if (BgL_test2146z00_3406)
								{	/* Initflow/walk.scm 84 */
									return BNIL;
								}
							else
								{	/* Initflow/walk.scm 84 */
									{	/* Initflow/walk.scm 87 */
										BgL_sfunzf2iflowzf2_bglt BgL_wide1150z00_1603;

										BgL_wide1150z00_1603 =
											((BgL_sfunzf2iflowzf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_sfunzf2iflowzf2_bgl))));
										{	/* Initflow/walk.scm 87 */
											obj_t BgL_auxz00_3418;
											BgL_objectz00_bglt BgL_tmpz00_3414;

											BgL_auxz00_3418 = ((obj_t) BgL_wide1150z00_1603);
											BgL_tmpz00_3414 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt)
													((BgL_sfunz00_bglt) BgL_fz00_1597)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3414, BgL_auxz00_3418);
										}
										((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_fz00_1597)));
										{	/* Initflow/walk.scm 87 */
											long BgL_arg1615z00_1604;

											BgL_arg1615z00_1604 =
												BGL_CLASS_NUM(BGl_sfunzf2iflowzf2zzinitflow_walkz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_fz00_1597))),
												BgL_arg1615z00_1604);
										}
										((BgL_sfunz00_bglt)
											((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_fz00_1597)));
									}
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_fz00_1597));
									{	/* Initflow/walk.scm 88 */
										obj_t BgL_arg1616z00_1606;

										BgL_arg1616z00_1606 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_fz00_1597)))->BgL_bodyz00);
										return
											BGl_initflowzd2nodezd2zzinitflow_walkz00(
											((BgL_nodez00_bglt) BgL_arg1616z00_1606), BgL_ez00_82);
									}
								}
						}
					else
						{	/* Initflow/walk.scm 82 */
							return BNIL;
						}
				}
			}
		}

	}



/* initflow-node* */
	obj_t BGl_initflowzd2nodeza2z70zzinitflow_walkz00(obj_t BgL_nodesz00_87,
		obj_t BgL_ez00_88)
	{
		{	/* Initflow/walk.scm 124 */
			{
				obj_t BgL_nodesz00_1610;
				obj_t BgL_isz00_1611;

				BgL_nodesz00_1610 = BgL_nodesz00_87;
				BgL_isz00_1611 = BNIL;
			BgL_zc3z04anonymousza31626ze3z87_1612:
				if (NULLP(BgL_nodesz00_1610))
					{	/* Initflow/walk.scm 127 */
						if (CBOOL(BgL_ez00_88))
							{	/* Initflow/walk.scm 128 */
								{
									obj_t BgL_l1311z00_1615;

									BgL_l1311z00_1615 = BgL_isz00_1611;
								BgL_zc3z04anonymousza31629ze3z87_1616:
									if (PAIRP(BgL_l1311z00_1615))
										{	/* Initflow/walk.scm 130 */
											{	/* Initflow/walk.scm 131 */
												obj_t BgL_gz00_1618;

												BgL_gz00_1618 = CAR(BgL_l1311z00_1615);
												if (
													((((BgL_globalz00_bglt) COBJECT(
																	((BgL_globalz00_bglt) BgL_gz00_1618)))->
															BgL_initz00) == BUNSPEC))
													{	/* Initflow/walk.scm 131 */
														((((BgL_globalz00_bglt) COBJECT(
																		((BgL_globalz00_bglt) BgL_gz00_1618)))->
																BgL_initz00) = ((obj_t) BTRUE), BUNSPEC);
													}
												else
													{	/* Initflow/walk.scm 131 */
														BFALSE;
													}
											}
											{
												obj_t BgL_l1311z00_3451;

												BgL_l1311z00_3451 = CDR(BgL_l1311z00_1615);
												BgL_l1311z00_1615 = BgL_l1311z00_3451;
												goto BgL_zc3z04anonymousza31629ze3z87_1616;
											}
										}
									else
										{	/* Initflow/walk.scm 130 */
											((bool_t) 1);
										}
								}
								return BNIL;
							}
						else
							{	/* Initflow/walk.scm 135 */
								obj_t BgL_hook1317z00_1624;

								BgL_hook1317z00_1624 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{
									obj_t BgL_l1314z00_1626;
									obj_t BgL_h1315z00_1627;

									BgL_l1314z00_1626 = BgL_isz00_1611;
									BgL_h1315z00_1627 = BgL_hook1317z00_1624;
								BgL_zc3z04anonymousza31651ze3z87_1628:
									if (NULLP(BgL_l1314z00_1626))
										{	/* Initflow/walk.scm 135 */
											return CDR(BgL_hook1317z00_1624);
										}
									else
										{	/* Initflow/walk.scm 135 */
											if (CBOOL(
													(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt)
																	CAR(
																		((obj_t) BgL_l1314z00_1626)))))->
														BgL_initz00)))
												{	/* Initflow/walk.scm 135 */
													obj_t BgL_nh1316z00_1632;

													{	/* Initflow/walk.scm 135 */
														obj_t BgL_arg1663z00_1634;

														BgL_arg1663z00_1634 =
															CAR(((obj_t) BgL_l1314z00_1626));
														BgL_nh1316z00_1632 =
															MAKE_YOUNG_PAIR(BgL_arg1663z00_1634, BNIL);
													}
													SET_CDR(BgL_h1315z00_1627, BgL_nh1316z00_1632);
													{	/* Initflow/walk.scm 135 */
														obj_t BgL_arg1661z00_1633;

														BgL_arg1661z00_1633 =
															CDR(((obj_t) BgL_l1314z00_1626));
														{
															obj_t BgL_h1315z00_3470;
															obj_t BgL_l1314z00_3469;

															BgL_l1314z00_3469 = BgL_arg1661z00_1633;
															BgL_h1315z00_3470 = BgL_nh1316z00_1632;
															BgL_h1315z00_1627 = BgL_h1315z00_3470;
															BgL_l1314z00_1626 = BgL_l1314z00_3469;
															goto BgL_zc3z04anonymousza31651ze3z87_1628;
														}
													}
												}
											else
												{	/* Initflow/walk.scm 135 */
													obj_t BgL_arg1675z00_1635;

													BgL_arg1675z00_1635 =
														CDR(((obj_t) BgL_l1314z00_1626));
													{
														obj_t BgL_l1314z00_3473;

														BgL_l1314z00_3473 = BgL_arg1675z00_1635;
														BgL_l1314z00_1626 = BgL_l1314z00_3473;
														goto BgL_zc3z04anonymousza31651ze3z87_1628;
													}
												}
										}
								}
							}
					}
				else
					{	/* Initflow/walk.scm 136 */
						obj_t BgL_iz00_1638;

						{	/* Initflow/walk.scm 136 */
							obj_t BgL_arg1689z00_1641;

							BgL_arg1689z00_1641 = CAR(((obj_t) BgL_nodesz00_1610));
							BgL_iz00_1638 =
								BGl_initflowzd2nodezd2zzinitflow_walkz00(
								((BgL_nodez00_bglt) BgL_arg1689z00_1641), ((bool_t) 0));
						}
						{	/* Initflow/walk.scm 137 */
							obj_t BgL_arg1681z00_1639;
							obj_t BgL_arg1688z00_1640;

							BgL_arg1681z00_1639 = CDR(((obj_t) BgL_nodesz00_1610));
							BgL_arg1688z00_1640 =
								BGl_appendzd221011zd2zzinitflow_walkz00(BgL_iz00_1638,
								BgL_isz00_1611);
							{
								obj_t BgL_isz00_3482;
								obj_t BgL_nodesz00_3481;

								BgL_nodesz00_3481 = BgL_arg1681z00_1639;
								BgL_isz00_3482 = BgL_arg1688z00_1640;
								BgL_isz00_1611 = BgL_isz00_3482;
								BgL_nodesz00_1610 = BgL_nodesz00_3481;
								goto BgL_zc3z04anonymousza31626ze3z87_1612;
							}
						}
					}
			}
		}

	}



/* lub */
	obj_t BGl_lubz00zzinitflow_walkz00(obj_t BgL_isz00_103, obj_t BgL_ez00_104)
	{
		{	/* Initflow/walk.scm 216 */
			if (NULLP(BgL_isz00_103))
				{	/* Initflow/walk.scm 217 */
					return BNIL;
				}
			else
				{	/* Initflow/walk.scm 219 */
					obj_t BgL_lubz00_1644;

					BgL_lubz00_1644 = BNIL;
					{	/* Initflow/walk.scm 220 */
						obj_t BgL_g1324z00_1645;

						BgL_g1324z00_1645 = CAR(((obj_t) BgL_isz00_103));
						{
							obj_t BgL_l1322z00_1647;

							BgL_l1322z00_1647 = BgL_g1324z00_1645;
						BgL_zc3z04anonymousza31691ze3z87_1648:
							if (PAIRP(BgL_l1322z00_1647))
								{	/* Initflow/walk.scm 226 */
									{	/* Initflow/walk.scm 221 */
										obj_t BgL_gz00_1650;

										BgL_gz00_1650 = CAR(BgL_l1322z00_1647);
										{	/* Initflow/walk.scm 221 */
											bool_t BgL_test2155z00_3490;

											if (
												((((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_1650)))->
														BgL_initz00) == BUNSPEC))
												{
													obj_t BgL_l1318z00_1668;

													BgL_l1318z00_1668 = CDR(((obj_t) BgL_isz00_103));
												BgL_zc3z04anonymousza31707ze3z87_1669:
													if (NULLP(BgL_l1318z00_1668))
														{	/* Initflow/walk.scm 222 */
															BgL_test2155z00_3490 = ((bool_t) 1);
														}
													else
														{	/* Initflow/walk.scm 222 */
															if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_gz00_1650,
																		CAR(((obj_t) BgL_l1318z00_1668)))))
																{
																	obj_t BgL_l1318z00_3502;

																	BgL_l1318z00_3502 =
																		CDR(((obj_t) BgL_l1318z00_1668));
																	BgL_l1318z00_1668 = BgL_l1318z00_3502;
																	goto BgL_zc3z04anonymousza31707ze3z87_1669;
																}
															else
																{	/* Initflow/walk.scm 222 */
																	BgL_test2155z00_3490 = ((bool_t) 0);
																}
														}
												}
											else
												{	/* Initflow/walk.scm 221 */
													BgL_test2155z00_3490 = ((bool_t) 0);
												}
											if (BgL_test2155z00_3490)
												{	/* Initflow/walk.scm 221 */
													if (CBOOL(BgL_ez00_104))
														{	/* Initflow/walk.scm 223 */
															((((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt) BgL_gz00_1650)))->
																	BgL_initz00) = ((obj_t) BTRUE), BUNSPEC);
														}
													else
														{	/* Initflow/walk.scm 223 */
															BgL_lubz00_1644 =
																MAKE_YOUNG_PAIR(BgL_gz00_1650, BgL_lubz00_1644);
														}
												}
											else
												{	/* Initflow/walk.scm 221 */
													BFALSE;
												}
										}
									}
									{
										obj_t BgL_l1322z00_3512;

										BgL_l1322z00_3512 = CDR(BgL_l1322z00_1647);
										BgL_l1322z00_1647 = BgL_l1322z00_3512;
										goto BgL_zc3z04anonymousza31691ze3z87_1648;
									}
								}
							else
								{	/* Initflow/walk.scm 226 */
									((bool_t) 1);
								}
						}
					}
					return BgL_lubz00_1644;
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			{	/* Initflow/walk.scm 42 */
				obj_t BgL_arg1720z00_1683;
				obj_t BgL_arg1722z00_1684;

				{	/* Initflow/walk.scm 42 */
					obj_t BgL_v1335z00_1727;

					BgL_v1335z00_1727 = create_vector(0L);
					BgL_arg1720z00_1683 = BgL_v1335z00_1727;
				}
				{	/* Initflow/walk.scm 42 */
					obj_t BgL_v1336z00_1728;

					BgL_v1336z00_1728 = create_vector(0L);
					BgL_arg1722z00_1684 = BgL_v1336z00_1728;
				}
				return (BGl_sfunzf2iflowzf2zzinitflow_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
						CNST_TABLE_REF(4), BGl_sfunz00zzast_varz00, 37049L,
						BGl_proc2035z00zzinitflow_walkz00,
						BGl_proc2034z00zzinitflow_walkz00, BFALSE,
						BGl_proc2033z00zzinitflow_walkz00,
						BGl_proc2032z00zzinitflow_walkz00, BgL_arg1720z00_1683,
						BgL_arg1722z00_1684), BUNSPEC);
			}
		}

	}



/* &lambda1737 */
	BgL_sfunz00_bglt BGl_z62lambda1737z62zzinitflow_walkz00(obj_t BgL_envz00_2795,
		obj_t BgL_o1130z00_2796)
	{
		{	/* Initflow/walk.scm 42 */
			{	/* Initflow/walk.scm 42 */
				long BgL_arg1738z00_2915;

				{	/* Initflow/walk.scm 42 */
					obj_t BgL_arg1739z00_2916;

					{	/* Initflow/walk.scm 42 */
						obj_t BgL_arg1740z00_2917;

						{	/* Initflow/walk.scm 42 */
							obj_t BgL_arg1815z00_2918;
							long BgL_arg1816z00_2919;

							BgL_arg1815z00_2918 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Initflow/walk.scm 42 */
								long BgL_arg1817z00_2920;

								BgL_arg1817z00_2920 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_o1130z00_2796)));
								BgL_arg1816z00_2919 = (BgL_arg1817z00_2920 - OBJECT_TYPE);
							}
							BgL_arg1740z00_2917 =
								VECTOR_REF(BgL_arg1815z00_2918, BgL_arg1816z00_2919);
						}
						BgL_arg1739z00_2916 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1740z00_2917);
					}
					{	/* Initflow/walk.scm 42 */
						obj_t BgL_tmpz00_3526;

						BgL_tmpz00_3526 = ((obj_t) BgL_arg1739z00_2916);
						BgL_arg1738z00_2915 = BGL_CLASS_NUM(BgL_tmpz00_3526);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) BgL_o1130z00_2796)), BgL_arg1738z00_2915);
			}
			{	/* Initflow/walk.scm 42 */
				BgL_objectz00_bglt BgL_tmpz00_3532;

				BgL_tmpz00_3532 =
					((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1130z00_2796));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3532, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1130z00_2796));
			return ((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1130z00_2796));
		}

	}



/* &<@anonymous:1736> */
	obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzinitflow_walkz00(obj_t
		BgL_envz00_2797, obj_t BgL_new1129z00_2798)
	{
		{	/* Initflow/walk.scm 42 */
			{
				BgL_sfunz00_bglt BgL_auxz00_3540;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_new1129z00_2798))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_propertyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_argszd2namezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_dssslzd2keywordszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_optionalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_keysz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1129z00_2798))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_3540 = ((BgL_sfunz00_bglt) BgL_new1129z00_2798);
				return ((obj_t) BgL_auxz00_3540);
			}
		}

	}



/* &lambda1734 */
	BgL_sfunz00_bglt BGl_z62lambda1734z62zzinitflow_walkz00(obj_t BgL_envz00_2799,
		obj_t BgL_o1126z00_2800)
	{
		{	/* Initflow/walk.scm 42 */
			{	/* Initflow/walk.scm 42 */
				BgL_sfunzf2iflowzf2_bglt BgL_wide1128z00_2923;

				BgL_wide1128z00_2923 =
					((BgL_sfunzf2iflowzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sfunzf2iflowzf2_bgl))));
				{	/* Initflow/walk.scm 42 */
					obj_t BgL_auxz00_3615;
					BgL_objectz00_bglt BgL_tmpz00_3611;

					BgL_auxz00_3615 = ((obj_t) BgL_wide1128z00_2923);
					BgL_tmpz00_3611 =
						((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1126z00_2800)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3611, BgL_auxz00_3615);
				}
				((BgL_objectz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1126z00_2800)));
				{	/* Initflow/walk.scm 42 */
					long BgL_arg1735z00_2924;

					BgL_arg1735z00_2924 =
						BGL_CLASS_NUM(BGl_sfunzf2iflowzf2zzinitflow_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) BgL_o1126z00_2800))), BgL_arg1735z00_2924);
				}
				return
					((BgL_sfunz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1126z00_2800)));
			}
		}

	}



/* &lambda1723 */
	BgL_sfunz00_bglt BGl_z62lambda1723z62zzinitflow_walkz00(obj_t BgL_envz00_2801,
		obj_t BgL_arity1104z00_2802, obj_t BgL_sidezd2effect1105zd2_2803,
		obj_t BgL_predicatezd2of1106zd2_2804,
		obj_t BgL_stackzd2allocator1107zd2_2805, obj_t BgL_topzf31108zf3_2806,
		obj_t BgL_thezd2closure1109zd2_2807, obj_t BgL_effect1110z00_2808,
		obj_t BgL_failsafe1111z00_2809, obj_t BgL_argszd2noescape1112zd2_2810,
		obj_t BgL_argszd2retescape1113zd2_2811, obj_t BgL_property1114z00_2812,
		obj_t BgL_args1115z00_2813, obj_t BgL_argszd2name1116zd2_2814,
		obj_t BgL_body1117z00_2815, obj_t BgL_class1118z00_2816,
		obj_t BgL_dssslzd2keywords1119zd2_2817, obj_t BgL_loc1120z00_2818,
		obj_t BgL_optionals1121z00_2819, obj_t BgL_keys1122z00_2820,
		obj_t BgL_thezd2closurezd2global1123z00_2821,
		obj_t BgL_strength1124z00_2822, obj_t BgL_stackable1125z00_2823)
	{
		{	/* Initflow/walk.scm 42 */
			{	/* Initflow/walk.scm 42 */
				long BgL_arity1104z00_2925;
				bool_t BgL_topzf31108zf3_2926;

				BgL_arity1104z00_2925 = (long) CINT(BgL_arity1104z00_2802);
				BgL_topzf31108zf3_2926 = CBOOL(BgL_topzf31108zf3_2806);
				{	/* Initflow/walk.scm 42 */
					BgL_sfunz00_bglt BgL_new1175z00_2928;

					{	/* Initflow/walk.scm 42 */
						BgL_sfunz00_bglt BgL_tmp1173z00_2929;
						BgL_sfunzf2iflowzf2_bglt BgL_wide1174z00_2930;

						{
							BgL_sfunz00_bglt BgL_auxz00_3631;

							{	/* Initflow/walk.scm 42 */
								BgL_sfunz00_bglt BgL_new1172z00_2931;

								BgL_new1172z00_2931 =
									((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sfunz00_bgl))));
								{	/* Initflow/walk.scm 42 */
									long BgL_arg1733z00_2932;

									BgL_arg1733z00_2932 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1172z00_2931),
										BgL_arg1733z00_2932);
								}
								{	/* Initflow/walk.scm 42 */
									BgL_objectz00_bglt BgL_tmpz00_3636;

									BgL_tmpz00_3636 = ((BgL_objectz00_bglt) BgL_new1172z00_2931);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3636, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1172z00_2931);
								BgL_auxz00_3631 = BgL_new1172z00_2931;
							}
							BgL_tmp1173z00_2929 = ((BgL_sfunz00_bglt) BgL_auxz00_3631);
						}
						BgL_wide1174z00_2930 =
							((BgL_sfunzf2iflowzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sfunzf2iflowzf2_bgl))));
						{	/* Initflow/walk.scm 42 */
							obj_t BgL_auxz00_3644;
							BgL_objectz00_bglt BgL_tmpz00_3642;

							BgL_auxz00_3644 = ((obj_t) BgL_wide1174z00_2930);
							BgL_tmpz00_3642 = ((BgL_objectz00_bglt) BgL_tmp1173z00_2929);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3642, BgL_auxz00_3644);
						}
						((BgL_objectz00_bglt) BgL_tmp1173z00_2929);
						{	/* Initflow/walk.scm 42 */
							long BgL_arg1724z00_2933;

							BgL_arg1724z00_2933 =
								BGL_CLASS_NUM(BGl_sfunzf2iflowzf2zzinitflow_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1173z00_2929),
								BgL_arg1724z00_2933);
						}
						BgL_new1175z00_2928 = ((BgL_sfunz00_bglt) BgL_tmp1173z00_2929);
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1175z00_2928)))->BgL_arityz00) =
						((long) BgL_arity1104z00_2925), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1105zd2_2803), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1106zd2_2804), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1107zd2_2805), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31108zf3_2926), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1109zd2_2807), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_effectz00) =
						((obj_t) BgL_effect1110z00_2808), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1111z00_2809), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1112zd2_2810), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1175z00_2928)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1113zd2_2811), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_propertyz00) =
						((obj_t) BgL_property1114z00_2812), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_argsz00) =
						((obj_t) BgL_args1115z00_2813), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_argszd2namezd2) =
						((obj_t) BgL_argszd2name1116zd2_2814), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_bodyz00) =
						((obj_t) BgL_body1117z00_2815), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_classz00) =
						((obj_t) BgL_class1118z00_2816), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_dssslzd2keywordszd2) =
						((obj_t) BgL_dssslzd2keywords1119zd2_2817), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_locz00) =
						((obj_t) BgL_loc1120z00_2818), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_optionalsz00) =
						((obj_t) BgL_optionals1121z00_2819), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_keysz00) =
						((obj_t) BgL_keys1122z00_2820), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_thezd2closurezd2globalz00) =
						((obj_t) BgL_thezd2closurezd2global1123z00_2821), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_strengthz00) =
						((obj_t) ((obj_t) BgL_strength1124z00_2822)), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1175z00_2928)))->BgL_stackablez00) =
						((obj_t) BgL_stackable1125z00_2823), BUNSPEC);
					return BgL_new1175z00_2928;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_proc2036z00zzinitflow_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2037z00zzinitflow_walkz00);
		}

	}



/* &initflow-node1337 */
	obj_t BGl_z62initflowzd2node1337zb0zzinitflow_walkz00(obj_t BgL_envz00_2825,
		obj_t BgL_nodez00_2826, obj_t BgL_ez00_2827)
	{
		{	/* Initflow/walk.scm 93 */
			return BNIL;
		}

	}



/* initflow-node */
	obj_t BGl_initflowzd2nodezd2zzinitflow_walkz00(BgL_nodez00_bglt
		BgL_nodez00_83, bool_t BgL_ez00_84)
	{
		{	/* Initflow/walk.scm 93 */
			{	/* Initflow/walk.scm 93 */
				obj_t BgL_method1338z00_1738;

				{	/* Initflow/walk.scm 93 */
					obj_t BgL_res2010z00_2466;

					{	/* Initflow/walk.scm 93 */
						long BgL_objzd2classzd2numz00_2437;

						BgL_objzd2classzd2numz00_2437 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_83));
						{	/* Initflow/walk.scm 93 */
							obj_t BgL_arg1811z00_2438;

							BgL_arg1811z00_2438 =
								PROCEDURE_REF(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
								(int) (1L));
							{	/* Initflow/walk.scm 93 */
								int BgL_offsetz00_2441;

								BgL_offsetz00_2441 = (int) (BgL_objzd2classzd2numz00_2437);
								{	/* Initflow/walk.scm 93 */
									long BgL_offsetz00_2442;

									BgL_offsetz00_2442 =
										((long) (BgL_offsetz00_2441) - OBJECT_TYPE);
									{	/* Initflow/walk.scm 93 */
										long BgL_modz00_2443;

										BgL_modz00_2443 =
											(BgL_offsetz00_2442 >> (int) ((long) ((int) (4L))));
										{	/* Initflow/walk.scm 93 */
											long BgL_restz00_2445;

											BgL_restz00_2445 =
												(BgL_offsetz00_2442 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Initflow/walk.scm 93 */

												{	/* Initflow/walk.scm 93 */
													obj_t BgL_bucketz00_2447;

													BgL_bucketz00_2447 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2438), BgL_modz00_2443);
													BgL_res2010z00_2466 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2447), BgL_restz00_2445);
					}}}}}}}}
					BgL_method1338z00_1738 = BgL_res2010z00_2466;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1338z00_1738,
					((obj_t) BgL_nodez00_83), BBOOL(BgL_ez00_84));
			}
		}

	}



/* &initflow-node */
	obj_t BGl_z62initflowzd2nodezb0zzinitflow_walkz00(obj_t BgL_envz00_2828,
		obj_t BgL_nodez00_2829, obj_t BgL_ez00_2830)
	{
		{	/* Initflow/walk.scm 93 */
			return
				BGl_initflowzd2nodezd2zzinitflow_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_2829), CBOOL(BgL_ez00_2830));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_varz00zzast_nodez00, BGl_proc2038z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2040z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_syncz00zzast_nodez00, BGl_proc2041z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc2042z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2043z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2044z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_externz00zzast_nodez00, BGl_proc2045z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_setqz00zzast_nodez00, BGl_proc2046z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2047z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_failz00zzast_nodez00, BGl_proc2048z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_switchz00zzast_nodez00, BGl_proc2049z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2050z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2051z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2052z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2053z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2054z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2055z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_initflowzd2nodezd2envz00zzinitflow_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2056z00zzinitflow_walkz00,
				BGl_string2039z00zzinitflow_walkz00);
		}

	}



/* &initflow-node-jump-e1377 */
	obj_t BGl_z62initflowzd2nodezd2jumpzd2e1377zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2849, obj_t BgL_nodez00_2850, obj_t BgL_ez00_2851)
	{
		{	/* Initflow/walk.scm 306 */
			{	/* Initflow/walk.scm 307 */
				obj_t BgL_res2061z00_2942;

				{	/* Initflow/walk.scm 308 */
					obj_t BgL_arg1941z00_2937;

					{	/* Initflow/walk.scm 308 */
						BgL_nodez00_bglt BgL_arg1942z00_2938;
						BgL_nodez00_bglt BgL_arg1943z00_2939;

						BgL_arg1942z00_2938 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2850)))->
							BgL_exitz00);
						BgL_arg1943z00_2939 =
							(((BgL_jumpzd2exzd2itz00_bglt)
								COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2850)))->
							BgL_valuez00);
						{	/* Initflow/walk.scm 308 */
							obj_t BgL_list1944z00_2940;

							{	/* Initflow/walk.scm 308 */
								obj_t BgL_arg1945z00_2941;

								BgL_arg1945z00_2941 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1943z00_2939), BNIL);
								BgL_list1944z00_2940 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1942z00_2938), BgL_arg1945z00_2941);
							}
							BgL_arg1941z00_2937 = BgL_list1944z00_2940;
						}
					}
					BgL_res2061z00_2942 =
						BGl_initflowzd2nodeza2z70zzinitflow_walkz00(BgL_arg1941z00_2937,
						BgL_ez00_2851);
				}
				return BgL_res2061z00_2942;
			}
		}

	}



/* &initflow-node-set-ex1374 */
	obj_t BGl_z62initflowzd2nodezd2setzd2ex1374zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2852, obj_t BgL_nodez00_2853, obj_t BgL_ez00_2854)
	{
		{	/* Initflow/walk.scm 298 */
			BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2853)))->BgL_onexitz00),
				CBOOL(BgL_ez00_2854));
			return
				BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2853)))->BgL_bodyz00),
				CBOOL(BgL_ez00_2854));
		}

	}



/* &initflow-node-let-va1371 */
	obj_t BGl_z62initflowzd2nodezd2letzd2va1371zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2855, obj_t BgL_nodez00_2856, obj_t BgL_ez00_2857)
	{
		{	/* Initflow/walk.scm 289 */
			{	/* Initflow/walk.scm 290 */
				obj_t BgL_res2062z00_2957;

				{	/* Initflow/walk.scm 291 */
					obj_t BgL_isz00_2945;

					{	/* Initflow/walk.scm 291 */
						obj_t BgL_l1330z00_2946;

						BgL_l1330z00_2946 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2856)))->
							BgL_bindingsz00);
						if (NULLP(BgL_l1330z00_2946))
							{	/* Initflow/walk.scm 291 */
								BgL_isz00_2945 = BNIL;
							}
						else
							{	/* Initflow/walk.scm 291 */
								obj_t BgL_head1332z00_2947;

								BgL_head1332z00_2947 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1330z00_2949;
									obj_t BgL_tail1333z00_2950;

									BgL_l1330z00_2949 = BgL_l1330z00_2946;
									BgL_tail1333z00_2950 = BgL_head1332z00_2947;
								BgL_zc3z04anonymousza31934ze3z87_2948:
									if (NULLP(BgL_l1330z00_2949))
										{	/* Initflow/walk.scm 291 */
											BgL_isz00_2945 = CDR(BgL_head1332z00_2947);
										}
									else
										{	/* Initflow/walk.scm 291 */
											obj_t BgL_newtail1334z00_2951;

											{	/* Initflow/walk.scm 291 */
												obj_t BgL_arg1937z00_2952;

												{	/* Initflow/walk.scm 291 */
													obj_t BgL_bz00_2953;

													BgL_bz00_2953 = CAR(((obj_t) BgL_l1330z00_2949));
													{	/* Initflow/walk.scm 291 */
														obj_t BgL_arg1938z00_2954;

														BgL_arg1938z00_2954 = CDR(((obj_t) BgL_bz00_2953));
														BgL_arg1937z00_2952 =
															BGl_initflowzd2nodezd2zzinitflow_walkz00(
															((BgL_nodez00_bglt) BgL_arg1938z00_2954),
															CBOOL(BgL_ez00_2857));
													}
												}
												BgL_newtail1334z00_2951 =
													MAKE_YOUNG_PAIR(BgL_arg1937z00_2952, BNIL);
											}
											SET_CDR(BgL_tail1333z00_2950, BgL_newtail1334z00_2951);
											{	/* Initflow/walk.scm 291 */
												obj_t BgL_arg1936z00_2955;

												BgL_arg1936z00_2955 = CDR(((obj_t) BgL_l1330z00_2949));
												{
													obj_t BgL_tail1333z00_3788;
													obj_t BgL_l1330z00_3787;

													BgL_l1330z00_3787 = BgL_arg1936z00_2955;
													BgL_tail1333z00_3788 = BgL_newtail1334z00_2951;
													BgL_tail1333z00_2950 = BgL_tail1333z00_3788;
													BgL_l1330z00_2949 = BgL_l1330z00_3787;
													goto BgL_zc3z04anonymousza31934ze3z87_2948;
												}
											}
										}
								}
							}
					}
					{	/* Initflow/walk.scm 291 */
						obj_t BgL_lubz00_2956;

						BgL_lubz00_2956 =
							BGl_lubz00zzinitflow_walkz00(BgL_isz00_2945, BgL_ez00_2857);
						{	/* Initflow/walk.scm 292 */

							BgL_res2062z00_2957 =
								BGl_appendzd221011zd2zzinitflow_walkz00(BgL_lubz00_2956,
								BGl_initflowzd2nodezd2zzinitflow_walkz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2856)))->
										BgL_bodyz00), CBOOL(BgL_ez00_2857)));
						}
					}
				}
				return BgL_res2062z00_2957;
			}
		}

	}



/* &initflow-node-let-fu1368 */
	obj_t BGl_z62initflowzd2nodezd2letzd2fu1368zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2858, obj_t BgL_nodez00_2859, obj_t BgL_ez00_2860)
	{
		{	/* Initflow/walk.scm 282 */
			return
				BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2859)))->BgL_bodyz00),
				CBOOL(BgL_ez00_2860));
		}

	}



/* &initflow-node-box-se1366 */
	obj_t BGl_z62initflowzd2nodezd2boxzd2se1366zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2861, obj_t BgL_nodez00_2862, obj_t BgL_ez00_2863)
	{
		{	/* Initflow/walk.scm 275 */
			return
				BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2862)))->BgL_valuez00),
				CBOOL(BgL_ez00_2863));
		}

	}



/* &initflow-node-box-re1364 */
	obj_t BGl_z62initflowzd2nodezd2boxzd2re1364zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2864, obj_t BgL_nodez00_2865, obj_t BgL_ez00_2866)
	{
		{	/* Initflow/walk.scm 268 */
			{	/* Initflow/walk.scm 270 */
				BgL_varz00_bglt BgL_arg1928z00_2961;

				BgL_arg1928z00_2961 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2865)))->BgL_varz00);
				return
					BGl_initflowzd2nodezd2zzinitflow_walkz00(
					((BgL_nodez00_bglt) BgL_arg1928z00_2961), CBOOL(BgL_ez00_2866));
			}
		}

	}



/* &initflow-node-make-b1362 */
	obj_t BGl_z62initflowzd2nodezd2makezd2b1362zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2867, obj_t BgL_nodez00_2868, obj_t BgL_ez00_2869)
	{
		{	/* Initflow/walk.scm 261 */
			return
				BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2868)))->BgL_valuez00),
				CBOOL(BgL_ez00_2869));
		}

	}



/* &initflow-node-switch1360 */
	obj_t BGl_z62initflowzd2nodezd2switch1360z62zzinitflow_walkz00(obj_t
		BgL_envz00_2870, obj_t BgL_nodez00_2871, obj_t BgL_ez00_2872)
	{
		{	/* Initflow/walk.scm 251 */
			{	/* Initflow/walk.scm 252 */
				obj_t BgL_res2063z00_2976;

				{	/* Initflow/walk.scm 253 */
					obj_t BgL_i0z00_2964;

					BgL_i0z00_2964 =
						BGl_initflowzd2nodezd2zzinitflow_walkz00(
						(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2871)))->BgL_testz00),
						CBOOL(BgL_ez00_2872));
					{	/* Initflow/walk.scm 254 */
						obj_t BgL_isz00_2965;

						{	/* Initflow/walk.scm 254 */
							obj_t BgL_l1325z00_2966;

							BgL_l1325z00_2966 =
								(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_2871)))->BgL_clausesz00);
							if (NULLP(BgL_l1325z00_2966))
								{	/* Initflow/walk.scm 254 */
									BgL_isz00_2965 = BNIL;
								}
							else
								{	/* Initflow/walk.scm 254 */
									obj_t BgL_head1327z00_2967;

									BgL_head1327z00_2967 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1325z00_2969;
										obj_t BgL_tail1328z00_2970;

										BgL_l1325z00_2969 = BgL_l1325z00_2966;
										BgL_tail1328z00_2970 = BgL_head1327z00_2967;
									BgL_zc3z04anonymousza31919ze3z87_2968:
										if (NULLP(BgL_l1325z00_2969))
											{	/* Initflow/walk.scm 254 */
												BgL_isz00_2965 = CDR(BgL_head1327z00_2967);
											}
										else
											{	/* Initflow/walk.scm 254 */
												obj_t BgL_newtail1329z00_2971;

												{	/* Initflow/walk.scm 254 */
													obj_t BgL_arg1924z00_2972;

													{	/* Initflow/walk.scm 254 */
														obj_t BgL_cz00_2973;

														BgL_cz00_2973 = CAR(((obj_t) BgL_l1325z00_2969));
														{	/* Initflow/walk.scm 254 */
															obj_t BgL_arg1925z00_2974;

															BgL_arg1925z00_2974 =
																CDR(((obj_t) BgL_cz00_2973));
															BgL_arg1924z00_2972 =
																BGl_initflowzd2nodezd2zzinitflow_walkz00(
																((BgL_nodez00_bglt) BgL_arg1925z00_2974),
																((bool_t) 0));
														}
													}
													BgL_newtail1329z00_2971 =
														MAKE_YOUNG_PAIR(BgL_arg1924z00_2972, BNIL);
												}
												SET_CDR(BgL_tail1328z00_2970, BgL_newtail1329z00_2971);
												{	/* Initflow/walk.scm 254 */
													obj_t BgL_arg1923z00_2975;

													BgL_arg1923z00_2975 =
														CDR(((obj_t) BgL_l1325z00_2969));
													{
														obj_t BgL_tail1328z00_3835;
														obj_t BgL_l1325z00_3834;

														BgL_l1325z00_3834 = BgL_arg1923z00_2975;
														BgL_tail1328z00_3835 = BgL_newtail1329z00_2971;
														BgL_tail1328z00_2970 = BgL_tail1328z00_3835;
														BgL_l1325z00_2969 = BgL_l1325z00_3834;
														goto BgL_zc3z04anonymousza31919ze3z87_2968;
													}
												}
											}
									}
								}
						}
						BgL_res2063z00_2976 =
							BGl_appendzd221011zd2zzinitflow_walkz00
							(BGl_lubz00zzinitflow_walkz00(BgL_isz00_2965, BgL_ez00_2872),
							BgL_i0z00_2964);
					}
				}
				return BgL_res2063z00_2976;
			}
		}

	}



/* &initflow-node-fail1358 */
	obj_t BGl_z62initflowzd2nodezd2fail1358z62zzinitflow_walkz00(obj_t
		BgL_envz00_2873, obj_t BgL_nodez00_2874, obj_t BgL_ez00_2875)
	{
		{	/* Initflow/walk.scm 244 */
			{	/* Initflow/walk.scm 245 */
				obj_t BgL_res2064z00_2985;

				{	/* Initflow/walk.scm 246 */
					obj_t BgL_arg1906z00_2978;

					{	/* Initflow/walk.scm 246 */
						BgL_nodez00_bglt BgL_arg1910z00_2979;
						BgL_nodez00_bglt BgL_arg1911z00_2980;
						BgL_nodez00_bglt BgL_arg1912z00_2981;

						BgL_arg1910z00_2979 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2874)))->BgL_procz00);
						BgL_arg1911z00_2980 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2874)))->BgL_msgz00);
						BgL_arg1912z00_2981 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2874)))->BgL_objz00);
						{	/* Initflow/walk.scm 246 */
							obj_t BgL_list1913z00_2982;

							{	/* Initflow/walk.scm 246 */
								obj_t BgL_arg1914z00_2983;

								{	/* Initflow/walk.scm 246 */
									obj_t BgL_arg1916z00_2984;

									BgL_arg1916z00_2984 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg1912z00_2981), BNIL);
									BgL_arg1914z00_2983 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg1911z00_2980), BgL_arg1916z00_2984);
								}
								BgL_list1913z00_2982 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1910z00_2979), BgL_arg1914z00_2983);
							}
							BgL_arg1906z00_2978 = BgL_list1913z00_2982;
						}
					}
					BgL_res2064z00_2985 =
						BGl_initflowzd2nodeza2z70zzinitflow_walkz00(BgL_arg1906z00_2978,
						BgL_ez00_2875);
				}
				return BgL_res2064z00_2985;
			}
		}

	}



/* &initflow-node-condit1356 */
	obj_t BGl_z62initflowzd2nodezd2condit1356z62zzinitflow_walkz00(obj_t
		BgL_envz00_2876, obj_t BgL_nodez00_2877, obj_t BgL_ez00_2878)
	{
		{	/* Initflow/walk.scm 232 */
			{	/* Initflow/walk.scm 233 */
				obj_t BgL_res2065z00_2994;

				{	/* Initflow/walk.scm 234 */
					obj_t BgL_i0z00_2987;

					BgL_i0z00_2987 =
						BGl_initflowzd2nodezd2zzinitflow_walkz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2877)))->BgL_testz00),
						CBOOL(BgL_ez00_2878));
					{	/* Initflow/walk.scm 236 */
						obj_t BgL_i1z00_2988;
						obj_t BgL_i2z00_2989;

						BgL_i1z00_2988 =
							BGl_initflowzd2nodezd2zzinitflow_walkz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2877)))->
								BgL_truez00), ((bool_t) 0));
						BgL_i2z00_2989 =
							BGl_initflowzd2nodezd2zzinitflow_walkz00(((
									(BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
											BgL_nodez00_2877)))->BgL_falsez00), ((bool_t) 0));
						{	/* Initflow/walk.scm 239 */
							obj_t BgL_arg1897z00_2990;

							{	/* Initflow/walk.scm 239 */
								obj_t BgL_arg1898z00_2991;

								{	/* Initflow/walk.scm 239 */
									obj_t BgL_list1899z00_2992;

									{	/* Initflow/walk.scm 239 */
										obj_t BgL_arg1901z00_2993;

										BgL_arg1901z00_2993 = MAKE_YOUNG_PAIR(BgL_i2z00_2989, BNIL);
										BgL_list1899z00_2992 =
											MAKE_YOUNG_PAIR(BgL_i1z00_2988, BgL_arg1901z00_2993);
									}
									BgL_arg1898z00_2991 = BgL_list1899z00_2992;
								}
								BgL_arg1897z00_2990 =
									BGl_lubz00zzinitflow_walkz00(BgL_arg1898z00_2991,
									BgL_ez00_2878);
							}
							BgL_res2065z00_2994 =
								BGl_appendzd221011zd2zzinitflow_walkz00(BgL_arg1897z00_2990,
								BgL_i0z00_2987);
						}
					}
				}
				return BgL_res2065z00_2994;
			}
		}

	}



/* &initflow-node-setq1354 */
	obj_t BGl_z62initflowzd2nodezd2setq1354z62zzinitflow_walkz00(obj_t
		BgL_envz00_2879, obj_t BgL_nodez00_2880, obj_t BgL_ez00_2881)
	{
		{	/* Initflow/walk.scm 199 */
			{	/* Initflow/walk.scm 201 */
				obj_t BgL_isz00_2996;
				BgL_variablez00_bglt BgL_vz00_2997;

				BgL_isz00_2996 =
					BGl_initflowzd2nodezd2zzinitflow_walkz00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2880)))->BgL_valuez00),
					CBOOL(BgL_ez00_2881));
				BgL_vz00_2997 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_2880)))->BgL_varz00)))->
					BgL_variablez00);
				{	/* Initflow/walk.scm 203 */
					bool_t BgL_test2164z00_3872;

					{	/* Initflow/walk.scm 203 */
						bool_t BgL_test2165z00_3873;

						{	/* Initflow/walk.scm 203 */
							obj_t BgL_classz00_2998;

							BgL_classz00_2998 = BGl_globalz00zzast_varz00;
							{	/* Initflow/walk.scm 203 */
								BgL_objectz00_bglt BgL_arg1807z00_2999;

								{	/* Initflow/walk.scm 203 */
									obj_t BgL_tmpz00_3874;

									BgL_tmpz00_3874 =
										((obj_t) ((BgL_objectz00_bglt) BgL_vz00_2997));
									BgL_arg1807z00_2999 = (BgL_objectz00_bglt) (BgL_tmpz00_3874);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Initflow/walk.scm 203 */
										long BgL_idxz00_3000;

										BgL_idxz00_3000 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2999);
										BgL_test2165z00_3873 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3000 + 2L)) == BgL_classz00_2998);
									}
								else
									{	/* Initflow/walk.scm 203 */
										bool_t BgL_res2016z00_3003;

										{	/* Initflow/walk.scm 203 */
											obj_t BgL_oclassz00_3004;

											{	/* Initflow/walk.scm 203 */
												obj_t BgL_arg1815z00_3005;
												long BgL_arg1816z00_3006;

												BgL_arg1815z00_3005 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Initflow/walk.scm 203 */
													long BgL_arg1817z00_3007;

													BgL_arg1817z00_3007 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2999);
													BgL_arg1816z00_3006 =
														(BgL_arg1817z00_3007 - OBJECT_TYPE);
												}
												BgL_oclassz00_3004 =
													VECTOR_REF(BgL_arg1815z00_3005, BgL_arg1816z00_3006);
											}
											{	/* Initflow/walk.scm 203 */
												bool_t BgL__ortest_1115z00_3008;

												BgL__ortest_1115z00_3008 =
													(BgL_classz00_2998 == BgL_oclassz00_3004);
												if (BgL__ortest_1115z00_3008)
													{	/* Initflow/walk.scm 203 */
														BgL_res2016z00_3003 = BgL__ortest_1115z00_3008;
													}
												else
													{	/* Initflow/walk.scm 203 */
														long BgL_odepthz00_3009;

														{	/* Initflow/walk.scm 203 */
															obj_t BgL_arg1804z00_3010;

															BgL_arg1804z00_3010 = (BgL_oclassz00_3004);
															BgL_odepthz00_3009 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3010);
														}
														if ((2L < BgL_odepthz00_3009))
															{	/* Initflow/walk.scm 203 */
																obj_t BgL_arg1802z00_3011;

																{	/* Initflow/walk.scm 203 */
																	obj_t BgL_arg1803z00_3012;

																	BgL_arg1803z00_3012 = (BgL_oclassz00_3004);
																	BgL_arg1802z00_3011 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3012,
																		2L);
																}
																BgL_res2016z00_3003 =
																	(BgL_arg1802z00_3011 == BgL_classz00_2998);
															}
														else
															{	/* Initflow/walk.scm 203 */
																BgL_res2016z00_3003 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2165z00_3873 = BgL_res2016z00_3003;
									}
							}
						}
						if (BgL_test2165z00_3873)
							{	/* Initflow/walk.scm 203 */
								if (
									((((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_vz00_2997)))->
											BgL_initz00) == BUNSPEC))
									{	/* Initflow/walk.scm 205 */
										obj_t BgL_arg1892z00_3013;

										BgL_arg1892z00_3013 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_vz00_2997)))->
											BgL_modulez00);
										BgL_test2164z00_3872 =
											(BgL_arg1892z00_3013 ==
											BGl_za2moduleza2z00zzmodule_modulez00);
									}
								else
									{	/* Initflow/walk.scm 204 */
										BgL_test2164z00_3872 = ((bool_t) 0);
									}
							}
						else
							{	/* Initflow/walk.scm 203 */
								BgL_test2164z00_3872 = ((bool_t) 0);
							}
					}
					if (BgL_test2164z00_3872)
						{	/* Initflow/walk.scm 203 */
							if (CBOOL(BgL_ez00_2881))
								{	/* Initflow/walk.scm 206 */
									((((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_vz00_2997)))->BgL_initz00) =
										((obj_t) BTRUE), BUNSPEC);
									return BNIL;
								}
							else
								{	/* Initflow/walk.scm 206 */
									return
										MAKE_YOUNG_PAIR(((obj_t) BgL_vz00_2997), BgL_isz00_2996);
								}
						}
					else
						{	/* Initflow/walk.scm 203 */
							return BgL_isz00_2996;
						}
				}
			}
		}

	}



/* &initflow-node-extern1352 */
	obj_t BGl_z62initflowzd2nodezd2extern1352z62zzinitflow_walkz00(obj_t
		BgL_envz00_2882, obj_t BgL_nodez00_2883, obj_t BgL_ez00_2884)
	{
		{	/* Initflow/walk.scm 192 */
			{	/* Initflow/walk.scm 193 */
				obj_t BgL_res2066z00_3015;

				BgL_res2066z00_3015 =
					BGl_initflowzd2nodeza2z70zzinitflow_walkz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2883)))->BgL_exprza2za2),
					BgL_ez00_2884);
				return BgL_res2066z00_3015;
			}
		}

	}



/* &initflow-node-funcal1350 */
	obj_t BGl_z62initflowzd2nodezd2funcal1350z62zzinitflow_walkz00(obj_t
		BgL_envz00_2885, obj_t BgL_nodez00_2886, obj_t BgL_ez00_2887)
	{
		{	/* Initflow/walk.scm 185 */
			{	/* Initflow/walk.scm 186 */
				obj_t BgL_res2067z00_3020;

				{	/* Initflow/walk.scm 187 */
					obj_t BgL_arg1882z00_3017;

					{	/* Initflow/walk.scm 187 */
						BgL_nodez00_bglt BgL_arg1883z00_3018;
						obj_t BgL_arg1884z00_3019;

						BgL_arg1883z00_3018 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2886)))->BgL_funz00);
						BgL_arg1884z00_3019 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2886)))->BgL_argsz00);
						BgL_arg1882z00_3017 =
							MAKE_YOUNG_PAIR(
							((obj_t) BgL_arg1883z00_3018), BgL_arg1884z00_3019);
					}
					BgL_res2067z00_3020 =
						BGl_initflowzd2nodeza2z70zzinitflow_walkz00(BgL_arg1882z00_3017,
						BgL_ez00_2887);
				}
				return BgL_res2067z00_3020;
			}
		}

	}



/* &initflow-node-app-ly1348 */
	obj_t BGl_z62initflowzd2nodezd2appzd2ly1348zb0zzinitflow_walkz00(obj_t
		BgL_envz00_2888, obj_t BgL_nodez00_2889, obj_t BgL_ez00_2890)
	{
		{	/* Initflow/walk.scm 178 */
			{	/* Initflow/walk.scm 179 */
				obj_t BgL_res2068z00_3027;

				{	/* Initflow/walk.scm 180 */
					obj_t BgL_arg1876z00_3022;

					{	/* Initflow/walk.scm 180 */
						BgL_nodez00_bglt BgL_arg1877z00_3023;
						BgL_nodez00_bglt BgL_arg1878z00_3024;

						BgL_arg1877z00_3023 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_2889)))->BgL_funz00);
						BgL_arg1878z00_3024 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_2889)))->BgL_argz00);
						{	/* Initflow/walk.scm 180 */
							obj_t BgL_list1879z00_3025;

							{	/* Initflow/walk.scm 180 */
								obj_t BgL_arg1880z00_3026;

								BgL_arg1880z00_3026 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1878z00_3024), BNIL);
								BgL_list1879z00_3025 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1877z00_3023), BgL_arg1880z00_3026);
							}
							BgL_arg1876z00_3022 = BgL_list1879z00_3025;
						}
					}
					BgL_res2068z00_3027 =
						BGl_initflowzd2nodeza2z70zzinitflow_walkz00(BgL_arg1876z00_3022,
						BgL_ez00_2890);
				}
				return BgL_res2068z00_3027;
			}
		}

	}



/* &initflow-node-app1346 */
	obj_t BGl_z62initflowzd2nodezd2app1346z62zzinitflow_walkz00(obj_t
		BgL_envz00_2891, obj_t BgL_nodez00_2892, obj_t BgL_ez00_2893)
	{
		{	/* Initflow/walk.scm 164 */
			{	/* Initflow/walk.scm 166 */
				BgL_variablez00_bglt BgL_vz00_3029;

				BgL_vz00_3029 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2892)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Initflow/walk.scm 166 */
					BgL_valuez00_bglt BgL_fz00_3030;

					BgL_fz00_3030 =
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_3029))->BgL_valuez00);
					{	/* Initflow/walk.scm 167 */

						{	/* Initflow/walk.scm 168 */
							bool_t BgL_test2171z00_3933;

							{	/* Initflow/walk.scm 168 */
								bool_t BgL_test2172z00_3934;

								{	/* Initflow/walk.scm 168 */
									obj_t BgL_classz00_3031;

									BgL_classz00_3031 = BGl_sfunz00zzast_varz00;
									{	/* Initflow/walk.scm 168 */
										BgL_objectz00_bglt BgL_arg1807z00_3032;

										{	/* Initflow/walk.scm 168 */
											obj_t BgL_tmpz00_3935;

											BgL_tmpz00_3935 =
												((obj_t) ((BgL_objectz00_bglt) BgL_fz00_3030));
											BgL_arg1807z00_3032 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3935);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Initflow/walk.scm 168 */
												long BgL_idxz00_3033;

												BgL_idxz00_3033 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3032);
												BgL_test2172z00_3934 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3033 + 3L)) == BgL_classz00_3031);
											}
										else
											{	/* Initflow/walk.scm 168 */
												bool_t BgL_res2013z00_3036;

												{	/* Initflow/walk.scm 168 */
													obj_t BgL_oclassz00_3037;

													{	/* Initflow/walk.scm 168 */
														obj_t BgL_arg1815z00_3038;
														long BgL_arg1816z00_3039;

														BgL_arg1815z00_3038 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Initflow/walk.scm 168 */
															long BgL_arg1817z00_3040;

															BgL_arg1817z00_3040 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3032);
															BgL_arg1816z00_3039 =
																(BgL_arg1817z00_3040 - OBJECT_TYPE);
														}
														BgL_oclassz00_3037 =
															VECTOR_REF(BgL_arg1815z00_3038,
															BgL_arg1816z00_3039);
													}
													{	/* Initflow/walk.scm 168 */
														bool_t BgL__ortest_1115z00_3041;

														BgL__ortest_1115z00_3041 =
															(BgL_classz00_3031 == BgL_oclassz00_3037);
														if (BgL__ortest_1115z00_3041)
															{	/* Initflow/walk.scm 168 */
																BgL_res2013z00_3036 = BgL__ortest_1115z00_3041;
															}
														else
															{	/* Initflow/walk.scm 168 */
																long BgL_odepthz00_3042;

																{	/* Initflow/walk.scm 168 */
																	obj_t BgL_arg1804z00_3043;

																	BgL_arg1804z00_3043 = (BgL_oclassz00_3037);
																	BgL_odepthz00_3042 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3043);
																}
																if ((3L < BgL_odepthz00_3042))
																	{	/* Initflow/walk.scm 168 */
																		obj_t BgL_arg1802z00_3044;

																		{	/* Initflow/walk.scm 168 */
																			obj_t BgL_arg1803z00_3045;

																			BgL_arg1803z00_3045 =
																				(BgL_oclassz00_3037);
																			BgL_arg1802z00_3044 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3045, 3L);
																		}
																		BgL_res2013z00_3036 =
																			(BgL_arg1802z00_3044 ==
																			BgL_classz00_3031);
																	}
																else
																	{	/* Initflow/walk.scm 168 */
																		BgL_res2013z00_3036 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2172z00_3934 = BgL_res2013z00_3036;
											}
									}
								}
								if (BgL_test2172z00_3934)
									{	/* Initflow/walk.scm 168 */
										BgL_test2171z00_3933 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(CNST_TABLE_REF(6),
												(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																BgL_fz00_3030)))->BgL_propertyz00)));
									}
								else
									{	/* Initflow/walk.scm 168 */
										BgL_test2171z00_3933 = ((bool_t) 0);
									}
							}
							if (BgL_test2171z00_3933)
								{	/* Initflow/walk.scm 168 */
									return BNIL;
								}
							else
								{	/* Initflow/walk.scm 170 */
									obj_t BgL_isz00_3046;

									BgL_isz00_3046 =
										BGl_initflowzd2nodeza2z70zzinitflow_walkz00(
										(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_2892)))->BgL_argsz00),
										BgL_ez00_2893);
									{	/* Initflow/walk.scm 171 */
										bool_t BgL_test2176z00_3966;

										{	/* Initflow/walk.scm 171 */
											obj_t BgL_classz00_3047;

											BgL_classz00_3047 = BGl_sfunz00zzast_varz00;
											{	/* Initflow/walk.scm 171 */
												BgL_objectz00_bglt BgL_arg1807z00_3048;

												{	/* Initflow/walk.scm 171 */
													obj_t BgL_tmpz00_3967;

													BgL_tmpz00_3967 =
														((obj_t) ((BgL_objectz00_bglt) BgL_fz00_3030));
													BgL_arg1807z00_3048 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3967);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Initflow/walk.scm 171 */
														long BgL_idxz00_3049;

														BgL_idxz00_3049 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3048);
														BgL_test2176z00_3966 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3049 + 3L)) == BgL_classz00_3047);
													}
												else
													{	/* Initflow/walk.scm 171 */
														bool_t BgL_res2014z00_3052;

														{	/* Initflow/walk.scm 171 */
															obj_t BgL_oclassz00_3053;

															{	/* Initflow/walk.scm 171 */
																obj_t BgL_arg1815z00_3054;
																long BgL_arg1816z00_3055;

																BgL_arg1815z00_3054 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Initflow/walk.scm 171 */
																	long BgL_arg1817z00_3056;

																	BgL_arg1817z00_3056 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3048);
																	BgL_arg1816z00_3055 =
																		(BgL_arg1817z00_3056 - OBJECT_TYPE);
																}
																BgL_oclassz00_3053 =
																	VECTOR_REF(BgL_arg1815z00_3054,
																	BgL_arg1816z00_3055);
															}
															{	/* Initflow/walk.scm 171 */
																bool_t BgL__ortest_1115z00_3057;

																BgL__ortest_1115z00_3057 =
																	(BgL_classz00_3047 == BgL_oclassz00_3053);
																if (BgL__ortest_1115z00_3057)
																	{	/* Initflow/walk.scm 171 */
																		BgL_res2014z00_3052 =
																			BgL__ortest_1115z00_3057;
																	}
																else
																	{	/* Initflow/walk.scm 171 */
																		long BgL_odepthz00_3058;

																		{	/* Initflow/walk.scm 171 */
																			obj_t BgL_arg1804z00_3059;

																			BgL_arg1804z00_3059 =
																				(BgL_oclassz00_3053);
																			BgL_odepthz00_3058 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3059);
																		}
																		if ((3L < BgL_odepthz00_3058))
																			{	/* Initflow/walk.scm 171 */
																				obj_t BgL_arg1802z00_3060;

																				{	/* Initflow/walk.scm 171 */
																					obj_t BgL_arg1803z00_3061;

																					BgL_arg1803z00_3061 =
																						(BgL_oclassz00_3053);
																					BgL_arg1802z00_3060 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3061, 3L);
																				}
																				BgL_res2014z00_3052 =
																					(BgL_arg1802z00_3060 ==
																					BgL_classz00_3047);
																			}
																		else
																			{	/* Initflow/walk.scm 171 */
																				BgL_res2014z00_3052 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2176z00_3966 = BgL_res2014z00_3052;
													}
											}
										}
										if (BgL_test2176z00_3966)
											{	/* Initflow/walk.scm 171 */
												return
													BGl_appendzd221011zd2zzinitflow_walkz00
													(BGl_initflowzd2funzd2zzinitflow_walkz00
													(BgL_vz00_3029, CBOOL(BgL_ez00_2893)),
													BgL_isz00_3046);
											}
										else
											{	/* Initflow/walk.scm 171 */
												return BgL_isz00_3046;
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* &initflow-node-sync1344 */
	obj_t BGl_z62initflowzd2nodezd2sync1344z62zzinitflow_walkz00(obj_t
		BgL_envz00_2894, obj_t BgL_nodez00_2895, obj_t BgL_ez00_2896)
	{
		{	/* Initflow/walk.scm 155 */
			BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2895)))->BgL_mutexz00),
				CBOOL(BgL_ez00_2896));
			BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2895)))->BgL_prelockz00),
				CBOOL(BgL_ez00_2896));
			return
				BGl_initflowzd2nodezd2zzinitflow_walkz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2895)))->BgL_bodyz00),
				CBOOL(BgL_ez00_2896));
		}

	}



/* &initflow-node-sequen1342 */
	obj_t BGl_z62initflowzd2nodezd2sequen1342z62zzinitflow_walkz00(obj_t
		BgL_envz00_2897, obj_t BgL_nodez00_2898, obj_t BgL_ez00_2899)
	{
		{	/* Initflow/walk.scm 142 */
			{	/* Initflow/walk.scm 143 */
				obj_t BgL_res2069z00_3071;

				{
					obj_t BgL_nodesz00_3065;
					obj_t BgL_isz00_3066;

					BgL_nodesz00_3065 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2898)))->BgL_nodesz00);
					BgL_isz00_3066 = BNIL;
				BgL_loopz00_3064:
					if (NULLP(BgL_nodesz00_3065))
						{	/* Initflow/walk.scm 146 */
							BgL_res2069z00_3071 = BgL_isz00_3066;
						}
					else
						{	/* Initflow/walk.scm 148 */
							obj_t BgL_iz00_3067;

							{	/* Initflow/walk.scm 148 */
								obj_t BgL_arg1862z00_3068;

								BgL_arg1862z00_3068 = CAR(((obj_t) BgL_nodesz00_3065));
								BgL_iz00_3067 =
									BGl_initflowzd2nodezd2zzinitflow_walkz00(
									((BgL_nodez00_bglt) BgL_arg1862z00_3068),
									CBOOL(BgL_ez00_2899));
							}
							{	/* Initflow/walk.scm 149 */
								obj_t BgL_arg1859z00_3069;
								obj_t BgL_arg1860z00_3070;

								BgL_arg1859z00_3069 = CDR(((obj_t) BgL_nodesz00_3065));
								BgL_arg1860z00_3070 =
									BGl_appendzd221011zd2zzinitflow_walkz00(BgL_iz00_3067,
									BgL_isz00_3066);
								{
									obj_t BgL_isz00_4016;
									obj_t BgL_nodesz00_4015;

									BgL_nodesz00_4015 = BgL_arg1859z00_3069;
									BgL_isz00_4016 = BgL_arg1860z00_3070;
									BgL_isz00_3066 = BgL_isz00_4016;
									BgL_nodesz00_3065 = BgL_nodesz00_4015;
									goto BgL_loopz00_3064;
								}
							}
						}
				}
				return BgL_res2069z00_3071;
			}
		}

	}



/* &initflow-node-var1340 */
	obj_t BGl_z62initflowzd2nodezd2var1340z62zzinitflow_walkz00(obj_t
		BgL_envz00_2900, obj_t BgL_nodez00_2901, obj_t BgL_ez00_2902)
	{
		{	/* Initflow/walk.scm 99 */
			{	/* Initflow/walk.scm 101 */
				bool_t BgL_test2181z00_4019;

				{	/* Initflow/walk.scm 101 */
					bool_t BgL_test2182z00_4020;

					{	/* Initflow/walk.scm 101 */
						BgL_variablez00_bglt BgL_arg1848z00_3073;

						BgL_arg1848z00_3073 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_nodez00_2901)))->BgL_variablez00);
						{	/* Initflow/walk.scm 101 */
							obj_t BgL_classz00_3074;

							BgL_classz00_3074 = BGl_globalz00zzast_varz00;
							{	/* Initflow/walk.scm 101 */
								BgL_objectz00_bglt BgL_arg1807z00_3075;

								{	/* Initflow/walk.scm 101 */
									obj_t BgL_tmpz00_4023;

									BgL_tmpz00_4023 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1848z00_3073));
									BgL_arg1807z00_3075 = (BgL_objectz00_bglt) (BgL_tmpz00_4023);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Initflow/walk.scm 101 */
										long BgL_idxz00_3076;

										BgL_idxz00_3076 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3075);
										BgL_test2182z00_4020 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3076 + 2L)) == BgL_classz00_3074);
									}
								else
									{	/* Initflow/walk.scm 101 */
										bool_t BgL_res2011z00_3079;

										{	/* Initflow/walk.scm 101 */
											obj_t BgL_oclassz00_3080;

											{	/* Initflow/walk.scm 101 */
												obj_t BgL_arg1815z00_3081;
												long BgL_arg1816z00_3082;

												BgL_arg1815z00_3081 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Initflow/walk.scm 101 */
													long BgL_arg1817z00_3083;

													BgL_arg1817z00_3083 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3075);
													BgL_arg1816z00_3082 =
														(BgL_arg1817z00_3083 - OBJECT_TYPE);
												}
												BgL_oclassz00_3080 =
													VECTOR_REF(BgL_arg1815z00_3081, BgL_arg1816z00_3082);
											}
											{	/* Initflow/walk.scm 101 */
												bool_t BgL__ortest_1115z00_3084;

												BgL__ortest_1115z00_3084 =
													(BgL_classz00_3074 == BgL_oclassz00_3080);
												if (BgL__ortest_1115z00_3084)
													{	/* Initflow/walk.scm 101 */
														BgL_res2011z00_3079 = BgL__ortest_1115z00_3084;
													}
												else
													{	/* Initflow/walk.scm 101 */
														long BgL_odepthz00_3085;

														{	/* Initflow/walk.scm 101 */
															obj_t BgL_arg1804z00_3086;

															BgL_arg1804z00_3086 = (BgL_oclassz00_3080);
															BgL_odepthz00_3085 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3086);
														}
														if ((2L < BgL_odepthz00_3085))
															{	/* Initflow/walk.scm 101 */
																obj_t BgL_arg1802z00_3087;

																{	/* Initflow/walk.scm 101 */
																	obj_t BgL_arg1803z00_3088;

																	BgL_arg1803z00_3088 = (BgL_oclassz00_3080);
																	BgL_arg1802z00_3087 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3088,
																		2L);
																}
																BgL_res2011z00_3079 =
																	(BgL_arg1802z00_3087 == BgL_classz00_3074);
															}
														else
															{	/* Initflow/walk.scm 101 */
																BgL_res2011z00_3079 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2182z00_4020 = BgL_res2011z00_3079;
									}
							}
						}
					}
					if (BgL_test2182z00_4020)
						{	/* Initflow/walk.scm 101 */
							if (
								((((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2901)))->
														BgL_variablez00))))->BgL_initz00) == BUNSPEC))
								{	/* Initflow/walk.scm 103 */
									obj_t BgL_arg1844z00_3089;

									BgL_arg1844z00_3089 =
										(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2901)))->
														BgL_variablez00))))->BgL_modulez00);
									BgL_test2181z00_4019 =
										(BgL_arg1844z00_3089 ==
										BGl_za2moduleza2z00zzmodule_modulez00);
								}
							else
								{	/* Initflow/walk.scm 102 */
									BgL_test2181z00_4019 = ((bool_t) 0);
								}
						}
					else
						{	/* Initflow/walk.scm 101 */
							BgL_test2181z00_4019 = ((bool_t) 0);
						}
				}
				if (BgL_test2181z00_4019)
					{	/* Initflow/walk.scm 101 */
						{	/* Initflow/walk.scm 104 */
							BgL_variablez00_bglt BgL_arg1765z00_3090;

							BgL_arg1765z00_3090 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_2901)))->BgL_variablez00);
							((((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_arg1765z00_3090)))->
									BgL_initz00) = ((obj_t) BFALSE), BUNSPEC);
						}
						{	/* Initflow/walk.scm 106 */
							bool_t BgL_test2187z00_4061;

							{	/* Initflow/walk.scm 106 */
								BgL_typez00_bglt BgL_arg1842z00_3091;

								BgL_arg1842z00_3091 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2901)))->
														BgL_variablez00)))))->BgL_typez00);
								BgL_test2187z00_4061 =
									(((obj_t) BgL_arg1842z00_3091) ==
									BGl_za2_za2z00zztype_cachez00);
							}
							if (BgL_test2187z00_4061)
								{	/* Initflow/walk.scm 107 */
									BgL_variablez00_bglt BgL_arg1773z00_3092;

									BgL_arg1773z00_3092 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_2901)))->
										BgL_variablez00);
									{	/* Initflow/walk.scm 107 */
										BgL_typez00_bglt BgL_vz00_3093;

										BgL_vz00_3093 =
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_arg1773z00_3092))))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_vz00_3093), BUNSPEC);
									}
								}
							else
								{	/* Initflow/walk.scm 108 */
									bool_t BgL_test2188z00_4075;

									{	/* Initflow/walk.scm 108 */
										BgL_typez00_bglt BgL_arg1839z00_3094;

										BgL_arg1839z00_3094 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt)
															(((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_nodez00_2901)))->
																BgL_variablez00)))))->BgL_typez00);
										BgL_test2188z00_4075 =
											(((obj_t) BgL_arg1839z00_3094) ==
											BGl_za2objza2z00zztype_cachez00);
									}
									if (BgL_test2188z00_4075)
										{	/* Initflow/walk.scm 108 */
											BUNSPEC;
										}
									else
										{	/* Initflow/walk.scm 110 */
											bool_t BgL_test2189z00_4083;

											{	/* Initflow/walk.scm 110 */
												bool_t BgL_test2190z00_4084;

												{	/* Initflow/walk.scm 110 */
													BgL_variablez00_bglt BgL_arg1838z00_3095;

													BgL_arg1838z00_3095 =
														(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2901)))->
														BgL_variablez00);
													BgL_test2190z00_4084 =
														BGl_globalzd2readzd2onlyzf3zf3zzast_varz00((
															(BgL_globalz00_bglt) BgL_arg1838z00_3095));
												}
												if (BgL_test2190z00_4084)
													{	/* Initflow/walk.scm 111 */
														BgL_typez00_bglt BgL_arg1835z00_3096;
														obj_t BgL_arg1836z00_3097;

														BgL_arg1835z00_3096 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt)
																			(((BgL_varz00_bglt) COBJECT(
																						((BgL_varz00_bglt)
																							BgL_nodez00_2901)))->
																				BgL_variablez00)))))->BgL_typez00);
														BgL_arg1836z00_3097 =
															BGl_getzd2classzd2typez00zztype_cachez00();
														BgL_test2189z00_4083 =
															(((obj_t) BgL_arg1835z00_3096) ==
															BgL_arg1836z00_3097);
													}
												else
													{	/* Initflow/walk.scm 110 */
														BgL_test2189z00_4083 = ((bool_t) 0);
													}
											}
											if (BgL_test2189z00_4083)
												{	/* Initflow/walk.scm 110 */
													BUNSPEC;
												}
											else
												{	/* Initflow/walk.scm 114 */
													obj_t BgL_arg1831z00_3098;
													obj_t BgL_arg1832z00_3099;

													BgL_arg1831z00_3098 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_varz00_bglt) BgL_nodez00_2901))))->
														BgL_locz00);
													BgL_arg1832z00_3099 =
														(((BgL_variablez00_bglt) COBJECT((((BgL_varz00_bglt)
																		COBJECT(((BgL_varz00_bglt)
																				BgL_nodez00_2901)))->
																	BgL_variablez00)))->BgL_idz00);
													BGl_userzd2errorzf2locationz20zztools_errorz00
														(BgL_arg1831z00_3098,
														BGl_za2moduleza2z00zzmodule_modulez00,
														BGl_string2057z00zzinitflow_walkz00,
														BgL_arg1832z00_3099, BNIL);
												}
										}
								}
						}
					}
				else
					{	/* Initflow/walk.scm 101 */
						BFALSE;
					}
			}
			{	/* Initflow/walk.scm 117 */
				bool_t BgL_test2191z00_4104;

				{	/* Initflow/walk.scm 117 */
					BgL_valuez00_bglt BgL_arg1853z00_3100;

					BgL_arg1853z00_3100 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_2901)))->
									BgL_variablez00)))->BgL_valuez00);
					{	/* Initflow/walk.scm 117 */
						obj_t BgL_classz00_3101;

						BgL_classz00_3101 = BGl_sfunz00zzast_varz00;
						{	/* Initflow/walk.scm 117 */
							BgL_objectz00_bglt BgL_arg1807z00_3102;

							{	/* Initflow/walk.scm 117 */
								obj_t BgL_tmpz00_4108;

								BgL_tmpz00_4108 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1853z00_3100));
								BgL_arg1807z00_3102 = (BgL_objectz00_bglt) (BgL_tmpz00_4108);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Initflow/walk.scm 117 */
									long BgL_idxz00_3103;

									BgL_idxz00_3103 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3102);
									BgL_test2191z00_4104 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3103 + 3L)) == BgL_classz00_3101);
								}
							else
								{	/* Initflow/walk.scm 117 */
									bool_t BgL_res2012z00_3106;

									{	/* Initflow/walk.scm 117 */
										obj_t BgL_oclassz00_3107;

										{	/* Initflow/walk.scm 117 */
											obj_t BgL_arg1815z00_3108;
											long BgL_arg1816z00_3109;

											BgL_arg1815z00_3108 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Initflow/walk.scm 117 */
												long BgL_arg1817z00_3110;

												BgL_arg1817z00_3110 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3102);
												BgL_arg1816z00_3109 =
													(BgL_arg1817z00_3110 - OBJECT_TYPE);
											}
											BgL_oclassz00_3107 =
												VECTOR_REF(BgL_arg1815z00_3108, BgL_arg1816z00_3109);
										}
										{	/* Initflow/walk.scm 117 */
											bool_t BgL__ortest_1115z00_3111;

											BgL__ortest_1115z00_3111 =
												(BgL_classz00_3101 == BgL_oclassz00_3107);
											if (BgL__ortest_1115z00_3111)
												{	/* Initflow/walk.scm 117 */
													BgL_res2012z00_3106 = BgL__ortest_1115z00_3111;
												}
											else
												{	/* Initflow/walk.scm 117 */
													long BgL_odepthz00_3112;

													{	/* Initflow/walk.scm 117 */
														obj_t BgL_arg1804z00_3113;

														BgL_arg1804z00_3113 = (BgL_oclassz00_3107);
														BgL_odepthz00_3112 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3113);
													}
													if ((3L < BgL_odepthz00_3112))
														{	/* Initflow/walk.scm 117 */
															obj_t BgL_arg1802z00_3114;

															{	/* Initflow/walk.scm 117 */
																obj_t BgL_arg1803z00_3115;

																BgL_arg1803z00_3115 = (BgL_oclassz00_3107);
																BgL_arg1802z00_3114 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3115,
																	3L);
															}
															BgL_res2012z00_3106 =
																(BgL_arg1802z00_3114 == BgL_classz00_3101);
														}
													else
														{	/* Initflow/walk.scm 117 */
															BgL_res2012z00_3106 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2191z00_4104 = BgL_res2012z00_3106;
								}
						}
					}
				}
				if (BgL_test2191z00_4104)
					{	/* Initflow/walk.scm 117 */
						return
							BGl_initflowzd2funzd2zzinitflow_walkz00(
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_2901)))->BgL_variablez00),
							CBOOL(BgL_ez00_2902));
					}
				else
					{	/* Initflow/walk.scm 117 */
						return BNIL;
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinitflow_walkz00(void)
	{
		{	/* Initflow/walk.scm 27 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2058z00zzinitflow_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
