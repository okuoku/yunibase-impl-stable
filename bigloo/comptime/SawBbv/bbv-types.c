/*===========================================================================*/
/*   (SawBbv/bbv-types.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-types.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2TYPESzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2TYPESzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                    *BgL_rtl_returnz00_bglt;

	typedef struct BgL_rtl_selectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
	}                    *BgL_rtl_selectz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_loadfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                     *BgL_rtl_loadfunz00_bglt;

	typedef struct BgL_rtl_globalrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                       *BgL_rtl_globalrefz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                    *BgL_rtl_vallocz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                     *BgL_rtl_vlengthz00_bglt;

	typedef struct BgL_rtl_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                        *BgL_rtl_instanceofz00_bglt;

	typedef struct BgL_rtl_storegz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                    *BgL_rtl_storegz00_bglt;

	typedef struct BgL_rtl_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_setfieldz00_bglt;

	typedef struct BgL_rtl_vsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vsetz00_bglt;

	typedef struct BgL_rtl_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_constrz00;
	}                 *BgL_rtl_newz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_lightfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_funsz00;
		obj_t BgL_rettypez00;
	}                          *BgL_rtl_lightfuncallz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_totypez00;
		struct BgL_typez00_bgl *BgL_fromtypez00;
	}                  *BgL_rtl_castz00_bglt;

	typedef struct BgL_rtl_cast_nullz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                       *BgL_rtl_cast_nullz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_bbvzd2rangezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_loz00;
		obj_t BgL_upz00;
	}                     *BgL_bbvzd2rangezd2_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2queuezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_blocksz00;
		obj_t BgL_lastz00;
	}                     *BgL_bbvzd2queuezd2_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2TYPESzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda2656z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2pragmazf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_inszd2pragmazf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2 = BUNSPEC;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_vset1923z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32672ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2663z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32389ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2664z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	extern obj_t BGl_rtl_newz00zzsaw_defsz00;
	static BgL_bbvzd2queuezd2_bglt BGl_z62lambda2586z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62lambda2588z62zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_vref1913z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_store1919z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static obj_t BGl_z62blockVzd2livezd2versionsz62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62shapezd2blockS1822zb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2typeszd2 = BUNSPEC;
	static obj_t BGl_z62lambda2670z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32657ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2671z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62lambda2596z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2677z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62lambda2597z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2678z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2errorza2z00zzsaw_bbvzd2cachezd2;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_selec1897z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62bbvzd2hashzd2rtl_new1873z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2;
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2685z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2686z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_listzd2eqzf3z21zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62bbvzd2hashzd2rtl_return1847z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza32772ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2typeszd2(int, int, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2errorzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62lambda2692z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2693z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2typeszd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static obj_t BGl_z62dumpzd2blockV1828zb0zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t BGl_z62bbvzd2hashzd2rtl_loadg1851z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static bool_t
		BGl_rtl_inszd2booleanzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt,
		bool_t);
	static obj_t BGl_z62shapezd2bbvzd2ctx1824z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2typeszd2(void);
	static BgL_blockz00_bglt
		BGl_z62bbvzd2queuezd2popz12z70zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_ifeqz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_inszd2boolzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_vlengthz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62blockzd2predszd2updatez12z70zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_dumpzd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2loadgzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32790ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32774ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2vlenzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62rtl_inszd2switchzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62bbvzd2hashzd2rtl_vset1871z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl__extendzd2ctxzd2zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2typeszd2(obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_vref1861z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_vsetz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62bbvzd2equalzf3zd2rtl_reg1893z91zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_globa1907z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32694ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62zc3z04anonymousza32767ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2brzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_cast_1935z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2truezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t
		BGl_bbvzd2ctxentryzd2equalzf3zf3zzsaw_bbvzd2typeszd2
		(BgL_bbvzd2ctxentryzd2_bglt, BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_inszf2b1891z63zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2hashzd2type1835z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62rtl_inszd2loadizf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static long BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2(long, obj_t, obj_t,
		long);
	static obj_t BGl_z62zc3z04anonymousza32687ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62rtl_inszd2nopzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32598ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32679ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2initz62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_getfi1909z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_extendzd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt, obj_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_vallocz00zzsaw_defsz00;
	extern obj_t BGl_rtl_cast_nullz00zzsaw_defsz00;
	static obj_t BGl_z62zc3z04anonymousza31991ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_loadfunz00zzsaw_defsz00;
	extern obj_t BGl_dumpza2za2zzsaw_defsz00(obj_t, obj_t, int);
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_newzd2ctxentryze70z35zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		BgL_rtl_regz00_bglt, obj_t, bool_t, obj_t);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_newzd2ctxentryze71z35zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t,
		BgL_rtl_regz00_bglt, obj_t, bool_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2getz62zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2errorzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62bbvzd2hashzd2rtl_instanc1865z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_setfiel1869z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31984ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2equalzf3zd2rtl_new1925z91zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62dumpzd2rtl_inszf2bbv1826z42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2failzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_bbvzd2queuezd2popz12z12zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_setfi1921z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_inszd2movzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2equalzf3zd2rtl_fun1895z91zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2assocz62zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2typeszd2(void);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_vallo1911z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_loadi1849z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2loadgzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62extendzd2ctxzf2entryza2ze0zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t bgl_typeof(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_rtl_insz00_bglt);
	static obj_t
		BGl_z62blockzd2predszd2updatez121936z70zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2typeszd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_blockzd2livezf3z21zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt);
	extern obj_t BGl_dumpz00zzsaw_defsz00(obj_t, obj_t, int);
	static obj_t BGl_z62rtl_inszd2falsezf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2queuezd2lengthz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2strlenzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_rtl_switchz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_inszd2strlenzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_goz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2typecheckzd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2loadizf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_rtl_ifnez00zzsaw_defsz00;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_loadf1905z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_instanceofz00zzsaw_defsz00;
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_rtl_setfieldz00zzsaw_defsz00;
	static obj_t BGl_z62bbvzd2equalzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2emptyzf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt);
	static obj_t BGl_z62zc3z04anonymousza32404ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2brzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl__extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl__extendzd2ctxza2z70zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	extern obj_t BGl_rtl_selectz00zzsaw_defsz00;
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2 = BUNSPEC;
	static obj_t BGl_extendzd2entriesze70z35zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t, BgL_bbvzd2ctxzd2_bglt, BgL_rtl_regz00_bglt, obj_t, bool_t, obj_t);
	BGL_IMPORT long bgl_symbol_hash_number(obj_t);
	static obj_t
		BGl_extendzd2entriesze71z35zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62bbvzd2hashzd2rtl_getfiel1857z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2boolzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt);
	extern obj_t BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2;
	static long BGl_search1799ze70ze7zzsaw_bbvzd2typeszd2(long, obj_t, obj_t,
		long);
	BGL_EXPORTED_DEF obj_t BGl_blockSz00zzsaw_bbvzd2typeszd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_blockVz00zzsaw_bbvzd2typeszd2 = BUNSPEC;
	extern obj_t BGl_rtl_storegz00zzsaw_defsz00;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_pragm1931z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(obj_t);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62aliaszd2ctxzb0zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32512ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32504ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32415ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_call1927z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62objectzd2printzd2blockS1780z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_loadg1903z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rtl_lastz00zzsaw_defsz00;
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt, obj_t, bool_t, obj_t, obj_t);
	extern obj_t BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_extendzd2ctxza2z70zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt, obj_t,
		obj_t, bool_t, obj_t);
	extern obj_t BGl_rtl_returnz00zzsaw_defsz00;
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2falsezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2gczd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2equalzf3z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2lastzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2queuezd2lengthz62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32530ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t
		BGl_z62blockzd2predszd2updatez12zd21939za2zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_storeg1867z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2equalzf31884z43zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2queuezd2haszf3z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_extendzd2ctxzf2entryz20zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockzd2livezf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2queuezd2emptyzf3z91zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32442ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2typeszd2(obj_t);
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	static obj_t BGl_z62shapezd2bbvzd2ctxentry1782z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_pragma1879z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_aliaszd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt, BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_failz00zzsaw_defsz00;
	extern obj_t BGl_regsetzd2ze3listz31zzsaw_regsetz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32257ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62bbvzd2hashzd2rtl_vlength1863z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2equalzf3zd2atom1887z91zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2typeszd2(void);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_cast1933z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_paramszd2ze3ctxz31zzsaw_bbvzd2typeszd2(obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_z62rtl_inszd2gozf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static long BGl_za2bbvzd2ctxzd2idza2z00zzsaw_bbvzd2typeszd2 = 0L;
	static obj_t BGl_z62bbvzd2hashzd2rtl_globalr1855z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_unaliaszd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_callzd2valueszd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static bool_t BGl_regzf3ze70z14zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2typeszd2(void);
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2 = BUNSPEC;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_light1929z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2402z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32541ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62bbvzd2hashzd2rtl_cast_nu1883z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2403z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32371ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_callzd2valueszb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2;
	static obj_t BGl_z62rtl_inszd2ifeqzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2;
	BGL_IMPORT obj_t bgl_remq(obj_t, obj_t);
	extern obj_t BGl_rtl_castz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2assocz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt, obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_reg1843z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_call1875z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2413z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2414z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32607ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32429ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt, int);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	extern obj_t BGl_rtl_globalrefz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_inszd2callzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_vleng1915z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static obj_t
		BGl_z62bbvzd2hashzd2rtl_inszf2bbv1841z90zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2502z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32632ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2503z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62dumpzd2blockS1830zb0zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_insta1917z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2424z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2427z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	extern obj_t BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2returnzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62bbvzd2equalzf3zd2blockS1889z91zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62rtl_inszd2returnzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2typecheckzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2510z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2430z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2511z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32382ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32463ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32625ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2nopzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2queuezd2haszf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt,
		BgL_blockz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2516z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_loadi1901z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2517z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_lightfu1877z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62shapezd2rtl_inszf2bbv1818z42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2vlenzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62bbvzd2equalzf3zd2rtl_switc1899z91zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62extendzd2ctxzf2entryz42zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62getzd2bbzd2markz62zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2440z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32480ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2441z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2522z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2523z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32456ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2605z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32618ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2606z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda2366z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2447z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2528z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2448z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2529z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2pushz12z12zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt,
		BgL_blockz00_bglt);
	static BgL_rtl_insz00_bglt BGl_z62lambda2369z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2queuezd2pushz12z70zzsaw_bbvzd2typeszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2fxovopzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62bbvzd2hashzd2rtl_fun1845z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_cast1881z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2truezf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62rtl_inszd2fxovopzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_bitzd2xorza2z70zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32643ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2613z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda2372z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32449ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2454z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2616z62zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2455z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2539z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62bbvzd2hash1831zb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzsaw_bbvzd2typeszd2(int, BgL_rtl_regz00_bglt,
		obj_t, obj_t, bool_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_lblze70ze7zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_loopze71ze7zzsaw_bbvzd2typeszd2(int, BgL_rtl_regz00_bglt,
		BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	static obj_t BGl_lblze71ze7zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62bbvzd2hashzd2rtl_loadfun1853z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2ifnezf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2hashzd2blockS1839z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_callzd2predicatezb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62lambda2540z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32571ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62lambda2380z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2461z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2623z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2381z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2462z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2624z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2547z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62lambda2548z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62lambda2387z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62objectzd2printzd2blockV1778z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2388z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	BGL_IMPORT obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static long BGl_za2bbzd2markza2zd2zzsaw_bbvzd2typeszd2 = 0L;
	static obj_t BGl_z62lambda2630z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2631z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32564ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2554z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2555z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2474z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2394z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2395z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2477z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62lambda2639z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2typeszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62paramszd2ze3ctxz53zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2 = BUNSPEC;
	static obj_t BGl_z62bbvzd2hashzd2rtl_valloc1859z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2failzf3z43zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	extern obj_t
		BGl_bbvzd2gczd2blockzd2reachablezf3z21zzsaw_bbvzd2gczd2(BgL_blockz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62unaliaszd2ctxzb0zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	extern BgL_bbvzd2rangezd2_bglt
		BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt,
		int);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62lambda2641z62zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2562z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2481z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32549ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2563z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62shapezd2block1820zb0zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	BGL_IMPORT long BGl_getzd2hashnumberzd2zz__hashz00(obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda2648z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszd2lastzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62lambda2649z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2569z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	extern obj_t BGl_dumpzd2marginzd2zzsaw_defsz00(obj_t, int);
	static obj_t BGl_z62bbvzd2hashzd2atom1837z62zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_extendzd2ctxzf2entryza2z82zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32590ze3ze5zzsaw_bbvzd2typeszd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32493ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2570z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32396ze3ze5zzsaw_bbvzd2typeszd2(obj_t);
	static obj_t BGl_z62lambda2491z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2492z62zzsaw_bbvzd2typeszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2655z62zzsaw_bbvzd2typeszd2(obj_t, obj_t);
	static obj_t __cnst[48];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2gozf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2goza73893za7,
		BGl_z62rtl_inszd2gozf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73894za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2pushz12zd2envzc0zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2queueza7d3895za7,
		BGl_z62bbvzd2queuezd2pushz12z70zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2truezf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2tru3896z00,
		BGl_z62rtl_inszd2truezf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2loadgzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2loa3897z00,
		BGl_z62rtl_inszd2loadgzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2nilza73898za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f3899za7,
		BGl_z62bbvzd2equalzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	extern obj_t BGl_dumpzd2envzd2zzsaw_defsz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73900za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73901za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extendzd2ctxz12zd2envz12zzsaw_bbvzd2typeszd2,
		BgL_bgl__extendza7d2ctxza7123902z00, opt_generic_entry,
		BGl__extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2assoczd2envzd2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2a3903za7,
		BGl_z62bbvzd2ctxzd2assocz62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73904za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2typeszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73905za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2typeszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73906za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extendzd2ctxzf2entryzd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762extendza7d2ctxza73907za7,
		BGl_z62extendzd2ctxzf2entryz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2livezf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762blockza7d2liveza73908za7,
		BGl_z62blockzd2livezf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73909za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2typeszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2strlenzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2str3910z00,
		BGl_z62rtl_inszd2strlenzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_blockzd2predszd2updatez12zd2envzc0zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762blockza7d2preds3911z00,
		BGl_z62blockzd2predszd2updatez12z70zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2regv3912z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2nopzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2nop3913z00,
		BGl_z62rtl_inszd2nopzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2haszf3zd2envz21zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2queueza7d3914za7,
		BGl_z62bbvzd2queuezd2haszf3z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2switchzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2swi3915z00,
		BGl_z62rtl_inszd2switchzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2emptyzf3zd2envz21zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2queueza7d3916za7,
		BGl_z62bbvzd2queuezd2emptyzf3z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extendzd2ctxza2zd2envza2zzsaw_bbvzd2typeszd2,
		BgL_bgl__extendza7d2ctxza7a23917z00, opt_generic_entry,
		BGl__extendzd2ctxza2z70zzsaw_bbvzd2typeszd2, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2leng3918z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2lastzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2las3919z00,
		BGl_z62rtl_inszd2lastzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2msiza73920za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73921za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73922za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2ifnezf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2ifn3923z00,
		BGl_z62rtl_inszd2ifnezf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73924za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2getzd2envzd2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2g3925za7,
		BGl_z62bbvzd2ctxzd2getz62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_aliaszd2ctxzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762aliasza7d2ctxza7b3926za7,
		BGl_z62aliaszd2ctxzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2typecheckzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2typ3927z00,
		BGl_z62rtl_inszd2typecheckzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2loadizf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2loa3928z00,
		BGl_z62rtl_inszd2loadizf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_objectzd2printzd2envz00zz__objectz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2errorzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2err3929z00,
		BGl_z62rtl_inszd2errorzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_paramszd2ze3ctxzd2envze3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762paramsza7d2za7e3c3930za7,
		BGl_z62paramszd2ze3ctxz53zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2popz12zd2envzc0zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2queueza7d3931za7,
		BGl_z62bbvzd2queuezd2popz12z70zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3700z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2369za7623932z00,
		BGl_z62lambda2369z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3701z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2366za7623933z00,
		BGl_z62lambda2366z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3702z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3934za7,
		BGl_z62zc3z04anonymousza32442ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3703z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2441za7623935z00,
		BGl_z62lambda2441z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3704z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2440za7623936z00,
		BGl_z62lambda2440z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3705z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3937za7,
		BGl_z62zc3z04anonymousza32449ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3706z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2448za7623938z00,
		BGl_z62lambda2448z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3707z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2447za7623939z00,
		BGl_z62lambda2447z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3708z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3940za7,
		BGl_z62zc3z04anonymousza32456ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3709z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2455za7623941z00,
		BGl_z62lambda2455z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73942za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2callzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2cal3943z00,
		BGl_z62rtl_inszd2callzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73944za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3710z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2454za7623945z00,
		BGl_z62lambda2454z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3711z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3946za7,
		BGl_z62zc3z04anonymousza32463ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3712z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2462za7623947z00,
		BGl_z62lambda2462z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3713z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2461za7623948z00,
		BGl_z62lambda2461z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3714z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2430za7623949z00,
		BGl_z62lambda2430z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3802z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3802za700za7za7s3950za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3715z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3951za7,
		BGl_z62zc3z04anonymousza32429ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73952za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3716z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2427za7623953z00,
		BGl_z62lambda2427z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3717z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2424za7623954z00,
		BGl_z62lambda2424z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3718z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3955za7,
		BGl_z62zc3z04anonymousza32493ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3719z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2492za7623956z00,
		BGl_z62lambda2492z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3808z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3808za700za7za7s3957za7, "dump", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2stri3958z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3800z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762objectza7d2prin3959z00,
		BGl_z62objectzd2printzd2blockS1780z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3801z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762shapeza7d2bbvza7d3960za7,
		BGl_z62shapezd2bbvzd2ctxentry1782z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3720z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2491za7623961z00,
		BGl_z62lambda2491z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3721z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3962za7,
		BGl_z62zc3z04anonymousza32504ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3803z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762shapeza7d2rtl_i3963z00,
		BGl_z62shapezd2rtl_inszf2bbv1818z42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3722z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2503za7623964z00,
		BGl_z62lambda2503z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3804z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762shapeza7d2block3965z00,
		BGl_z62shapezd2block1820zb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3723z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2502za7623966z00,
		BGl_z62lambda2502z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3805z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762shapeza7d2block3967z00,
		BGl_z62shapezd2blockS1822zb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3724z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3968za7,
		BGl_z62zc3z04anonymousza32512ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3812z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3812za700za7za7s3969za7, "bbv-hash", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3806z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762shapeza7d2bbvza7d3970za7,
		BGl_z62shapezd2bbvzd2ctx1824z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3725z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2511za7623971z00,
		BGl_z62lambda2511z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3807z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762dumpza7d2rtl_in3972z00,
		BGl_z62dumpzd2rtl_inszf2bbv1826z42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3726z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2510za7623973z00,
		BGl_z62lambda2510z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3727z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2517za7623974z00,
		BGl_z62lambda2517z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3809z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762dumpza7d2blockv3975z00,
		BGl_z62dumpzd2blockV1828zb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3728z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2516za7623976z00,
		BGl_z62lambda2516z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3729z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2523za7623977z00,
		BGl_z62lambda2523z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73978za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza73979za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3810z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762dumpza7d2blocks3980z00,
		BGl_z62dumpzd2blockS1830zb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3811z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23981za7,
		BGl_z62bbvzd2hashzd2type1835z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3730z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2522za7623982z00,
		BGl_z62lambda2522z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3731z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3983za7,
		BGl_z62zc3z04anonymousza32530ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2ifeqzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2ife3984z00,
		BGl_z62rtl_inszd2ifeqzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3813z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23985za7,
		BGl_z62bbvzd2hashzd2atom1837z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3732z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2529za7623986z00,
		BGl_z62lambda2529z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3814z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23987za7,
		BGl_z62bbvzd2hashzd2blockS1839z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3733z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2528za7623988z00,
		BGl_z62lambda2528z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2stri3989z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3815z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23990za7,
		BGl_z62bbvzd2hashzd2rtl_inszf2bbv1841z90zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3734z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3991za7,
		BGl_z62zc3z04anonymousza32541ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3816z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23992za7,
		BGl_z62bbvzd2hashzd2rtl_reg1843z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3735z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2540za7623993z00,
		BGl_z62lambda2540z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3817z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23994za7,
		BGl_z62bbvzd2hashzd2rtl_fun1845z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3736z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2539za7623995z00,
		BGl_z62lambda2539z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3818z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23996za7,
		BGl_z62bbvzd2hashzd2rtl_return1847z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3737z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo3997za7,
		BGl_z62zc3z04anonymousza32549ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3819z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d23998za7,
		BGl_z62bbvzd2hashzd2rtl_loadi1849z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3738z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2548za7623999z00,
		BGl_z62lambda2548z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3739z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2547za7624000z00,
		BGl_z62lambda2547z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2brzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2brza74001za7,
		BGl_z62rtl_inszd2brzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2livezd2versionszd2envzd2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762blockvza7d2live4002z00,
		BGl_z62blockVzd2livezd2versionsz62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3820z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24003za7,
		BGl_z62bbvzd2hashzd2rtl_loadg1851z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3821z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24004za7,
		BGl_z62bbvzd2hashzd2rtl_loadfun1853z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3740z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2555za7624005z00,
		BGl_z62lambda2555z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3822z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24006za7,
		BGl_z62bbvzd2hashzd2rtl_globalr1855z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3741z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2554za7624007z00,
		BGl_z62lambda2554z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3823z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24008za7,
		BGl_z62bbvzd2hashzd2rtl_getfiel1857z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3742z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4009za7,
		BGl_z62zc3z04anonymousza32564ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74010za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2typeszd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3824z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24011za7,
		BGl_z62bbvzd2hashzd2rtl_valloc1859z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3743z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2563za7624012z00,
		BGl_z62lambda2563z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3825z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24013za7,
		BGl_z62bbvzd2hashzd2rtl_vref1861z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3744z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2562za7624014z00,
		BGl_z62lambda2562z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3826z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24015za7,
		BGl_z62bbvzd2hashzd2rtl_vlength1863z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3745z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4016za7,
		BGl_z62zc3z04anonymousza32571ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3827z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24017za7,
		BGl_z62bbvzd2hashzd2rtl_instanc1865z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3746z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2570za7624018z00,
		BGl_z62lambda2570z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3828z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24019za7,
		BGl_z62bbvzd2hashzd2rtl_storeg1867z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3747z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2569za7624020z00,
		BGl_z62lambda2569z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3829z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24021za7,
		BGl_z62bbvzd2hashzd2rtl_setfiel1869z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3748z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2481za7624022z00,
		BGl_z62lambda2481z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3749z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4023za7,
		BGl_z62zc3z04anonymousza32480ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3838z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3838za700za7za7s4024za7, "bbv-equal?", 10);
	      DEFINE_STRING(BGl_string3679z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3679za700za7za7s4025za7, "bbv-queue-pop!", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762makeza7d2rtl_re4026z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3830z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24027za7,
		BGl_z62bbvzd2hashzd2rtl_vset1871z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3831z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24028za7,
		BGl_z62bbvzd2hashzd2rtl_new1873z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3750z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2477za7624029z00,
		BGl_z62lambda2477z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3832z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24030za7,
		BGl_z62bbvzd2hashzd2rtl_call1875z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3751z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2474za7624031z00,
		BGl_z62lambda2474z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3833z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24032za7,
		BGl_z62bbvzd2hashzd2rtl_lightfu1877z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3752z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4033za7,
		BGl_z62zc3z04anonymousza32598ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3834z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24034za7,
		BGl_z62bbvzd2hashzd2rtl_pragma1879z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3753z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2597za7624035z00,
		BGl_z62lambda2597z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74036za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3835z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24037za7,
		BGl_z62bbvzd2hashzd2rtl_cast1881z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3754z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2596za7624038z00,
		BGl_z62lambda2596z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3836z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7d24039za7,
		BGl_z62bbvzd2hashzd2rtl_cast_nu1883z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3755z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4040za7,
		BGl_z62zc3z04anonymousza32607ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3680z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3680za700za7za7s4041za7, "Illegal empty queue", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3837z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4042za7,
		BGl_z62bbvzd2equalzf3zd2atom1887z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3756z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2606za7624043z00,
		BGl_z62lambda2606z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3757z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2605za7624044z00,
		BGl_z62lambda2605z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3839z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4045za7,
		BGl_z62bbvzd2equalzf3zd2blockS1889z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3758z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4046za7,
		BGl_z62zc3z04anonymousza32590ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3759z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2588za7624047z00,
		BGl_z62lambda2588z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2leng4048z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2returnzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2ret4049z00,
		BGl_z62rtl_inszd2returnzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3840z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4050za7,
		BGl_z62bbvzd2equalzf3zd2rtl_inszf2b1891z63zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3841z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4051za7,
		BGl_z62bbvzd2equalzf3zd2rtl_reg1893z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3760z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2586za7624052z00,
		BGl_z62lambda2586z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3842z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4053za7,
		BGl_z62bbvzd2equalzf3zd2rtl_fun1895z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3761z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4054za7,
		BGl_z62zc3z04anonymousza32625ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3843z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4055za7,
		BGl_z62bbvzd2equalzf3zd2rtl_selec1897z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3762z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2624za7624056z00,
		BGl_z62lambda2624z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3681z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4057za7,
		BGl_z62zc3z04anonymousza31991ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3844z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4058za7,
		BGl_z62bbvzd2equalzf3zd2rtl_switc1899z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3763z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2623za7624059z00,
		BGl_z62lambda2623z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3682z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4060za7,
		BGl_z62zc3z04anonymousza31984ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3845z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4061za7,
		BGl_z62bbvzd2equalzf3zd2rtl_loadi1901z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3764z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4062za7,
		BGl_z62zc3z04anonymousza32632ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3683z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4063za7,
		BGl_z62zc3z04anonymousza32382ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3846z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4064za7,
		BGl_z62bbvzd2equalzf3zd2rtl_loadg1903z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3765z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2631za7624065z00,
		BGl_z62lambda2631z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3684z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2381za7624066z00,
		BGl_z62lambda2381z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3847z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4067za7,
		BGl_z62bbvzd2equalzf3zd2rtl_loadf1905z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3766z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2630za7624068z00,
		BGl_z62lambda2630z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3685z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2380za7624069z00,
		BGl_z62lambda2380z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3848z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4070za7,
		BGl_z62bbvzd2equalzf3zd2rtl_globa1907z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3767z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4071za7,
		BGl_z62zc3z04anonymousza32618ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3686z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4072za7,
		BGl_z62zc3z04anonymousza32389ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3849z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4073za7,
		BGl_z62bbvzd2equalzf3zd2rtl_getfi1909z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3768z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2616za7624074z00,
		BGl_z62lambda2616z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3687z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2388za7624075z00,
		BGl_z62lambda2388z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3769z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2613za7624076z00,
		BGl_z62lambda2613z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3688z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2387za7624077z00,
		BGl_z62lambda2387z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2initzd2envzd2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i4078za7,
		BGl_z62bbvzd2ctxzd2initz62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3689z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4079za7,
		BGl_z62zc3z04anonymousza32396ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2vlenzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2vle4080z00,
		BGl_z62rtl_inszd2vlenzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74081za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2failzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2fai4082z00,
		BGl_z62rtl_inszd2failzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2bbzd2markzd2envzd2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762getza7d2bbza7d2ma4083za7,
		BGl_z62getzd2bbzd2markz62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_callzd2predicatezd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_callza7d2pr4084z00,
		BGl_z62rtl_callzd2predicatezb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3850z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4085za7,
		BGl_z62bbvzd2equalzf3zd2rtl_vallo1911z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3851z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4086za7,
		BGl_z62bbvzd2equalzf3zd2rtl_vref1913z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3770z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2649za7624087z00,
		BGl_z62lambda2649z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3852z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4088za7,
		BGl_z62bbvzd2equalzf3zd2rtl_vleng1915z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3771z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2648za7624089z00,
		BGl_z62lambda2648z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3690z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2395za7624090z00,
		BGl_z62lambda2395z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3853z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4091za7,
		BGl_z62bbvzd2equalzf3zd2rtl_insta1917z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3772z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4092za7,
		BGl_z62zc3z04anonymousza32657ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3691z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2394za7624093z00,
		BGl_z62lambda2394z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3854z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4094za7,
		BGl_z62bbvzd2equalzf3zd2rtl_store1919z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3773z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2656za7624095z00,
		BGl_z62lambda2656z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3692z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4096za7,
		BGl_z62zc3z04anonymousza32404ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3855z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4097za7,
		BGl_z62bbvzd2equalzf3zd2rtl_setfi1921z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3774z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2655za7624098z00,
		BGl_z62lambda2655z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3693z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2403za7624099z00,
		BGl_z62lambda2403z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3856z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4100za7,
		BGl_z62bbvzd2equalzf3zd2rtl_vset1923z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3775z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2664za7624101z00,
		BGl_z62lambda2664z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3694z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2402za7624102z00,
		BGl_z62lambda2402z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74103za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3857z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4104za7,
		BGl_z62bbvzd2equalzf3zd2rtl_new1925z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3776z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2663za7624105z00,
		BGl_z62lambda2663z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3695z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4106za7,
		BGl_z62zc3z04anonymousza32415ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3864z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3864za700za7za7s4107za7, "block-preds-update!", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3858z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4108za7,
		BGl_z62bbvzd2equalzf3zd2rtl_call1927z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3777z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4109za7,
		BGl_z62zc3z04anonymousza32672ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3696z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2414za7624110z00,
		BGl_z62lambda2414z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3865z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3865za700za7za7s4111za7, "(blockS ", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3859z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4112za7,
		BGl_z62bbvzd2equalzf3zd2rtl_light1929z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3778z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2671za7624113z00,
		BGl_z62lambda2671z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3697z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2413za7624114z00,
		BGl_z62lambda2413z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3866z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3866za700za7za7s4115za7, ":parent ", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3779z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2670za7624116z00,
		BGl_z62lambda2670z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3698z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2372za7624117z00,
		BGl_z62lambda2372z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3867z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3867za700za7za7s4118za7, ":merge ", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3699z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4119za7,
		BGl_z62zc3z04anonymousza32371ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3868z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3868za700za7za7s4120za7, ":preds ", 7);
	      DEFINE_STRING(BGl_string3869z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3869za700za7za7s4121za7, ":succs ", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74122za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3860z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4123za7,
		BGl_z62bbvzd2equalzf3zd2rtl_pragm1931z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3861z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4124za7,
		BGl_z62bbvzd2equalzf3zd2rtl_cast1933z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3780z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4125za7,
		BGl_z62zc3z04anonymousza32679ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3862z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4126za7,
		BGl_z62bbvzd2equalzf3zd2rtl_cast_1935z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3781z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2678za7624127z00,
		BGl_z62lambda2678z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3863z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762blockza7d2preds4128z00,
		BGl_z62blockzd2predszd2updatez12zd21939za2zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3782z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2677za7624129z00,
		BGl_z62lambda2677z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3870z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3870za700za7za7s4130za7, ":ctx ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3783z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4131za7,
		BGl_z62zc3z04anonymousza32687ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3871z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3871za700za7za7s4132za7, ":cnt ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3784z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2686za7624133z00,
		BGl_z62lambda2686z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3872z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3872za700za7za7s4134za7, ":asleep ", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3785z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2685za7624135z00,
		BGl_z62lambda2685z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762makeza7d2regset4136z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string3873z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3873za700za7za7s4137za7, "\n )\n", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3786z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4138za7,
		BGl_z62zc3z04anonymousza32694ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3874z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3874za700za7za7s4139za7, "(blockV ", 8);
	      DEFINE_STRING(BGl_string3793z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3793za700za7za7s4140za7, "bbv-hash1831", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3787z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2693za7624141z00,
		BGl_z62lambda2693z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3875z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3875za700za7za7s4142za7, "[", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3788z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2692za7624143z00,
		BGl_z62lambda2692z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3876z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3876za700za7za7s4144za7, "(", 1);
	      DEFINE_STRING(BGl_string3795z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3795za700za7za7s4145za7, "bbv-equal?1884", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3789z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762za7c3za704anonymo4146za7,
		BGl_z62zc3z04anonymousza32643ze3ze5zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3877z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3877za700za7za7s4147za7, " <- ", 4);
	      DEFINE_STRING(BGl_string3878z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3878za700za7za7s4148za7, ")", 1);
	      DEFINE_STRING(BGl_string3797z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3797za700za7za7s4149za7, "block-preds-update!1936", 23);
	      DEFINE_STRING(BGl_string3879z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3879za700za7za7s4150za7, " ", 1);
	      DEFINE_STRING(BGl_string3799z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3799za700za7za7s4151za7, "object-print", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_extendzd2ctxzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl__extendza7d2ctxza7d24152z00, opt_generic_entry,
		BGl__extendzd2ctxzd2zzsaw_bbvzd2typeszd2, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3790z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2641za7624153z00,
		BGl_z62lambda2641z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3791z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762lambda2639za7624154z00,
		BGl_z62lambda2639z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3792z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hash1834155z00,
		BGl_z62bbvzd2hash1831zb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3880z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3880za700za7za7s4156za7, "]", 1);
	      DEFINE_STRING(BGl_string3881z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3881za700za7za7s4157za7, " ;;", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3794z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2equalza7f4158za7,
		BGl_z62bbvzd2equalzf31884z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3882z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3882za700za7za7s4159za7, " def=", 5);
	      DEFINE_STRING(BGl_string3883z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3883za700za7za7s4160za7, " in=", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3796z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762blockza7d2preds4161z00,
		BGl_z62blockzd2predszd2updatez121936z70zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string3884z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3884za700za7za7s4162za7, " out=", 5);
	      DEFINE_STRING(BGl_string3885z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3885za700za7za7s4163za7, "~a:~a", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3798z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762objectza7d2prin4164z00,
		BGl_z62objectzd2printzd2blockV1778z62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3886z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3886za700za7za7s4165za7, "!~a", 3);
	      DEFINE_STRING(BGl_string3887z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3887za700za7za7s4166za7, "[~( )]", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lengthzd2envzd2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2queueza7d4167za7,
		BGl_z62bbvzd2queuezd2lengthz62zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3888z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3888za700za7za7s4168za7, "~s", 2);
	      DEFINE_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3889za700za7za7s4169za7, "saw_bbv-types", 13);
	      DEFINE_STRING(BGl_string3890z00zzsaw_bbvzd2typeszd2,
		BgL_bgl_string3890za700za7za7s4170za7,
		"root bbv-ctxentry initval aliases value count polarity pair types reg bbv-ctx entries id bbv-queue last blocks blockS bool asleep merges creator mblock gcmark gccnt parent %blacklist blockV merge long %mark generic pair-nil versions saw_bbv-types rtl_ins/bbv %hash ctx in out obj def number :value :count :aliases _ cnt ssr ",
		324);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unaliaszd2ctxzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762unaliasza7d2ctx4171z00,
		BGl_z62unaliaszd2ctxzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2falsezf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2fal4172z00,
		BGl_z62rtl_inszd2falsezf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extendzd2ctxzf2entryza2zd2envz50zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762extendza7d2ctxza74173za7, va_generic_entry,
		BGl_z62extendzd2ctxzf2entryza2ze0zzsaw_bbvzd2typeszd2, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2pragmazf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2pra4174z00,
		BGl_z62rtl_inszd2pragmazf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2boolzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2boo4175z00,
		BGl_z62rtl_inszd2boolzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7f3za791za74176z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2hashza7b04177za7,
		BGl_z62bbvzd2hashzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74178za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_callzd2valueszd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_callza7d2va4179z00,
		BGl_z62rtl_callzd2valueszb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2fxovopzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2fxo4180z00,
		BGl_z62rtl_inszd2fxovopzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_regza7f2raza74181za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2typeszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2movzf3zd2envzf3zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762rtl_insza7d2mov4182z00,
		BGl_z62rtl_inszd2movzf3z43zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2equalzf3zd2envz21zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e4183za7,
		BGl_z62bbvzd2ctxzd2equalzf3z91zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2typeszd2,
		BgL_bgl_za762regsetza7d2regl4184z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2typeszd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2typeszd2));
		     ADD_ROOT((void *) (&BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2));
		     ADD_ROOT((void *) (&BGl_blockSz00zzsaw_bbvzd2typeszd2));
		     ADD_ROOT((void *) (&BGl_blockVz00zzsaw_bbvzd2typeszd2));
		     ADD_ROOT((void *) (&BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2));
		     ADD_ROOT((void *) (&BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long
		BgL_checksumz00_10096, char *BgL_fromz00_10097)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2typeszd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2typeszd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2typeszd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2typeszd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2typeszd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2typeszd2();
					BGl_objectzd2initzd2zzsaw_bbvzd2typeszd2();
					BGl_genericzd2initzd2zzsaw_bbvzd2typeszd2();
					BGl_methodzd2initzd2zzsaw_bbvzd2typeszd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2typeszd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-types");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-types");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			{	/* SawBbv/bbv-types.scm 15 */
				obj_t BgL_cportz00_8908;

				{	/* SawBbv/bbv-types.scm 15 */
					obj_t BgL_stringz00_8915;

					BgL_stringz00_8915 = BGl_string3890z00zzsaw_bbvzd2typeszd2;
					{	/* SawBbv/bbv-types.scm 15 */
						obj_t BgL_startz00_8916;

						BgL_startz00_8916 = BINT(0L);
						{	/* SawBbv/bbv-types.scm 15 */
							obj_t BgL_endz00_8917;

							BgL_endz00_8917 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_8915)));
							{	/* SawBbv/bbv-types.scm 15 */

								BgL_cportz00_8908 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_8915, BgL_startz00_8916, BgL_endz00_8917);
				}}}}
				{
					long BgL_iz00_8909;

					BgL_iz00_8909 = 47L;
				BgL_loopz00_8910:
					if ((BgL_iz00_8909 == -1L))
						{	/* SawBbv/bbv-types.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-types.scm 15 */
							{	/* SawBbv/bbv-types.scm 15 */
								obj_t BgL_arg3891z00_8911;

								{	/* SawBbv/bbv-types.scm 15 */

									{	/* SawBbv/bbv-types.scm 15 */
										obj_t BgL_locationz00_8913;

										BgL_locationz00_8913 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-types.scm 15 */

											BgL_arg3891z00_8911 =
												BGl_readz00zz__readerz00(BgL_cportz00_8908,
												BgL_locationz00_8913);
										}
									}
								}
								{	/* SawBbv/bbv-types.scm 15 */
									int BgL_tmpz00_10134;

									BgL_tmpz00_10134 = (int) (BgL_iz00_8909);
									CNST_TABLE_SET(BgL_tmpz00_10134, BgL_arg3891z00_8911);
							}}
							{	/* SawBbv/bbv-types.scm 15 */
								int BgL_auxz00_8914;

								BgL_auxz00_8914 = (int) ((BgL_iz00_8909 - 1L));
								{
									long BgL_iz00_10139;

									BgL_iz00_10139 = (long) (BgL_auxz00_8914);
									BgL_iz00_8909 = BgL_iz00_10139;
									goto BgL_loopz00_8910;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			BGl_za2bbvzd2ctxzd2idza2z00zzsaw_bbvzd2typeszd2 = 0L;
			BGl_za2bbzd2markza2zd2zzsaw_bbvzd2typeszd2 = -1L;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2typeszd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2156;

				BgL_headz00_2156 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2157;
					obj_t BgL_tailz00_2158;

					BgL_prevz00_2157 = BgL_headz00_2156;
					BgL_tailz00_2158 = BgL_l1z00_1;
				BgL_loopz00_2159:
					if (PAIRP(BgL_tailz00_2158))
						{
							obj_t BgL_newzd2prevzd2_2161;

							BgL_newzd2prevzd2_2161 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2158), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2157, BgL_newzd2prevzd2_2161);
							{
								obj_t BgL_tailz00_10149;
								obj_t BgL_prevz00_10148;

								BgL_prevz00_10148 = BgL_newzd2prevzd2_2161;
								BgL_tailz00_10149 = CDR(BgL_tailz00_2158);
								BgL_tailz00_2158 = BgL_tailz00_10149;
								BgL_prevz00_2157 = BgL_prevz00_10148;
								goto BgL_loopz00_2159;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2156);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2typeszd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1234z00_8919;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1232z00_8920;
					BgL_rtl_regzf2razf2_bglt BgL_wide1233z00_8921;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_10152;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1231z00_8922;

							BgL_new1231z00_8922 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1944z00_8923;

								BgL_arg1944z00_8923 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1231z00_8922),
									BgL_arg1944z00_8923);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_10157;

								BgL_tmpz00_10157 = ((BgL_objectz00_bglt) BgL_new1231z00_8922);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10157, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1231z00_8922);
							BgL_auxz00_10152 = BgL_new1231z00_8922;
						}
						BgL_tmp1232z00_8920 = ((BgL_rtl_regz00_bglt) BgL_auxz00_10152);
					}
					BgL_wide1233z00_8921 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_10165;
						BgL_objectz00_bglt BgL_tmpz00_10163;

						BgL_auxz00_10165 = ((obj_t) BgL_wide1233z00_8921);
						BgL_tmpz00_10163 = ((BgL_objectz00_bglt) BgL_tmp1232z00_8920);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10163, BgL_auxz00_10165);
					}
					((BgL_objectz00_bglt) BgL_tmp1232z00_8920);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1943z00_8924;

						BgL_arg1943z00_8924 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1232z00_8920), BgL_arg1943z00_8924);
					}
					BgL_new1234z00_8919 = ((BgL_rtl_regz00_bglt) BgL_tmp1232z00_8920);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1234z00_8919)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1234z00_8919)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1234z00_8919)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1234z00_8919)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1234z00_8919)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1234z00_8919)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1234z00_8919)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_10187;

					{
						obj_t BgL_auxz00_10188;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_10189;

							BgL_tmpz00_10189 = ((BgL_objectz00_bglt) BgL_new1234z00_8919);
							BgL_auxz00_10188 = BGL_OBJECT_WIDENING(BgL_tmpz00_10189);
						}
						BgL_auxz00_10187 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10188);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10187))->
							BgL_numz00) = ((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_10194;

					{
						obj_t BgL_auxz00_10195;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_10196;

							BgL_tmpz00_10196 = ((BgL_objectz00_bglt) BgL_new1234z00_8919);
							BgL_auxz00_10195 = BGL_OBJECT_WIDENING(BgL_tmpz00_10196);
						}
						BgL_auxz00_10194 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10195);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10194))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_10201;

					{
						obj_t BgL_auxz00_10202;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_10203;

							BgL_tmpz00_10203 = ((BgL_objectz00_bglt) BgL_new1234z00_8919);
							BgL_auxz00_10202 = BGL_OBJECT_WIDENING(BgL_tmpz00_10203);
						}
						BgL_auxz00_10201 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10202);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10201))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_10208;

					{
						obj_t BgL_auxz00_10209;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_10210;

							BgL_tmpz00_10210 = ((BgL_objectz00_bglt) BgL_new1234z00_8919);
							BgL_auxz00_10209 = BGL_OBJECT_WIDENING(BgL_tmpz00_10210);
						}
						BgL_auxz00_10208 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10209);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10208))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_10215;

					{
						obj_t BgL_auxz00_10216;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_10217;

							BgL_tmpz00_10217 = ((BgL_objectz00_bglt) BgL_new1234z00_8919);
							BgL_auxz00_10216 = BGL_OBJECT_WIDENING(BgL_tmpz00_10217);
						}
						BgL_auxz00_10215 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10216);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10215))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_10222;

					{
						obj_t BgL_auxz00_10223;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_10224;

							BgL_tmpz00_10224 = ((BgL_objectz00_bglt) BgL_new1234z00_8919);
							BgL_auxz00_10223 = BGL_OBJECT_WIDENING(BgL_tmpz00_10224);
						}
						BgL_auxz00_10222 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10223);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10222))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1234z00_8919;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8057, obj_t BgL_type1179z00_8058, obj_t BgL_var1180z00_8059,
		obj_t BgL_onexprzf31181zf3_8060, obj_t BgL_name1182z00_8061,
		obj_t BgL_key1183z00_8062, obj_t BgL_hardware1184z00_8063,
		obj_t BgL_num1185z00_8064, obj_t BgL_color1186z00_8065,
		obj_t BgL_coalesce1187z00_8066, obj_t BgL_occurrences1188z00_8067,
		obj_t BgL_interfere1189z00_8068, obj_t BgL_interfere21190z00_8069)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2typeszd2(
				((BgL_typez00_bglt) BgL_type1179z00_8058), BgL_var1180z00_8059,
				BgL_onexprzf31181zf3_8060, BgL_name1182z00_8061, BgL_key1183z00_8062,
				BgL_hardware1184z00_8063, CINT(BgL_num1185z00_8064),
				BgL_color1186z00_8065, BgL_coalesce1187z00_8066,
				CINT(BgL_occurrences1188z00_8067), BgL_interfere1189z00_8068,
				BgL_interfere21190z00_8069);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2typeszd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_8925;

				BgL_classz00_8925 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_8926;

						BgL_arg1807z00_8926 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_8927;

								BgL_idxz00_8927 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8926);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8927 + 2L)) == BgL_classz00_8925);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res3586z00_8930;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_8931;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_8932;
										long BgL_arg1816z00_8933;

										BgL_arg1815z00_8932 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_8934;

											BgL_arg1817z00_8934 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8926);
											BgL_arg1816z00_8933 = (BgL_arg1817z00_8934 - OBJECT_TYPE);
										}
										BgL_oclassz00_8931 =
											VECTOR_REF(BgL_arg1815z00_8932, BgL_arg1816z00_8933);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_8935;

										BgL__ortest_1115z00_8935 =
											(BgL_classz00_8925 == BgL_oclassz00_8931);
										if (BgL__ortest_1115z00_8935)
											{	/* SawMill/regset.sch 56 */
												BgL_res3586z00_8930 = BgL__ortest_1115z00_8935;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_8936;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_8937;

													BgL_arg1804z00_8937 = (BgL_oclassz00_8931);
													BgL_odepthz00_8936 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8937);
												}
												if ((2L < BgL_odepthz00_8936))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_8938;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_8939;

															BgL_arg1803z00_8939 = (BgL_oclassz00_8931);
															BgL_arg1802z00_8938 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8939,
																2L);
														}
														BgL_res3586z00_8930 =
															(BgL_arg1802z00_8938 == BgL_classz00_8925);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res3586z00_8930 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3586z00_8930;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8070,
		obj_t BgL_objz00_8071)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2typeszd2(BgL_objz00_8071));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_4825;

				BgL_classz00_4825 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_4826;

					BgL__ortest_1117z00_4826 = BGL_CLASS_NIL(BgL_classz00_4825);
					if (CBOOL(BgL__ortest_1117z00_4826))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_4826);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4825));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8072)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2typeszd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10264;

				{
					obj_t BgL_auxz00_10265;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_10266;

						BgL_tmpz00_10266 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_10265 = BGL_OBJECT_WIDENING(BgL_tmpz00_10266);
					}
					BgL_auxz00_10264 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10265);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10264))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8073, obj_t BgL_oz00_8074)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8074));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10273;

				{
					obj_t BgL_auxz00_10274;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_10275;

						BgL_tmpz00_10275 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_10274 = BGL_OBJECT_WIDENING(BgL_tmpz00_10275);
					}
					BgL_auxz00_10273 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10274);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10273))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8075, obj_t BgL_oz00_8076, obj_t BgL_vz00_8077)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8076), BgL_vz00_8077);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10282;

				{
					obj_t BgL_auxz00_10283;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_10284;

						BgL_tmpz00_10284 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_10283 = BGL_OBJECT_WIDENING(BgL_tmpz00_10284);
					}
					BgL_auxz00_10282 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10283);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10282))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8078, obj_t BgL_oz00_8079)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8079));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10291;

				{
					obj_t BgL_auxz00_10292;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_10293;

						BgL_tmpz00_10293 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_10292 = BGL_OBJECT_WIDENING(BgL_tmpz00_10293);
					}
					BgL_auxz00_10291 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10292);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10291))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8080, obj_t BgL_oz00_8081, obj_t BgL_vz00_8082)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8081), BgL_vz00_8082);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10300;

				{
					obj_t BgL_auxz00_10301;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_10302;

						BgL_tmpz00_10302 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_10301 = BGL_OBJECT_WIDENING(BgL_tmpz00_10302);
					}
					BgL_auxz00_10300 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10301);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10300))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8083, obj_t BgL_oz00_8084)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2typeszd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_8084)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10310;

				{
					obj_t BgL_auxz00_10311;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_10312;

						BgL_tmpz00_10312 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_10311 = BGL_OBJECT_WIDENING(BgL_tmpz00_10312);
					}
					BgL_auxz00_10310 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10311);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10310))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8085, obj_t BgL_oz00_8086, obj_t BgL_vz00_8087)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8086), CINT(BgL_vz00_8087));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10320;

				{
					obj_t BgL_auxz00_10321;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_10322;

						BgL_tmpz00_10322 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_10321 = BGL_OBJECT_WIDENING(BgL_tmpz00_10322);
					}
					BgL_auxz00_10320 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10321);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10320))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8088, obj_t BgL_oz00_8089)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8089));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10329;

				{
					obj_t BgL_auxz00_10330;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_10331;

						BgL_tmpz00_10331 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_10330 = BGL_OBJECT_WIDENING(BgL_tmpz00_10331);
					}
					BgL_auxz00_10329 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10330);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10329))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8090, obj_t BgL_oz00_8091, obj_t BgL_vz00_8092)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8091), BgL_vz00_8092);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10338;

				{
					obj_t BgL_auxz00_10339;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_10340;

						BgL_tmpz00_10340 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_10339 = BGL_OBJECT_WIDENING(BgL_tmpz00_10340);
					}
					BgL_auxz00_10338 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10339);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10338))->
					BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8093, obj_t BgL_oz00_8094)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8094));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10347;

				{
					obj_t BgL_auxz00_10348;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_10349;

						BgL_tmpz00_10349 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_10348 = BGL_OBJECT_WIDENING(BgL_tmpz00_10349);
					}
					BgL_auxz00_10347 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10348);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10347))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8095, obj_t BgL_oz00_8096, obj_t BgL_vz00_8097)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8096), BgL_vz00_8097);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_10356;

				{
					obj_t BgL_auxz00_10357;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_10358;

						BgL_tmpz00_10358 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_10357 = BGL_OBJECT_WIDENING(BgL_tmpz00_10358);
					}
					BgL_auxz00_10356 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10357);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10356))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8098,
		obj_t BgL_oz00_8099)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2typeszd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_8099)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8100, obj_t BgL_oz00_8101)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8101));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8102,
		obj_t BgL_oz00_8103)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8103));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8104,
		obj_t BgL_oz00_8105)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8105));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8106, obj_t BgL_oz00_8107)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8107));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2typeszd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8108, obj_t BgL_oz00_8109, obj_t BgL_vz00_8110)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8109), BgL_vz00_8110);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8111,
		obj_t BgL_oz00_8112)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8112));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8113, obj_t BgL_oz00_8114, obj_t BgL_vz00_8115)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8114), BgL_vz00_8115);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8116, obj_t BgL_oz00_8117)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8117));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2typeszd2(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8118, obj_t BgL_oz00_8119, obj_t BgL_vz00_8120)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2typeszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_8119),
				((BgL_typez00_bglt) BgL_vz00_8120));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2typeszd2(int BgL_length1172z00_52,
		int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1236z00_8940;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1235z00_8941;

					BgL_new1235z00_8941 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1945z00_8942;

						BgL_arg1945z00_8942 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1235z00_8941), BgL_arg1945z00_8942);
					}
					BgL_new1236z00_8940 = BgL_new1235z00_8941;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1236z00_8940))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1236z00_8940))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1236z00_8940))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1236z00_8940))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1236z00_8940))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1236z00_8940;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8121, obj_t BgL_length1172z00_8122,
		obj_t BgL_msiza7e1173za7_8123, obj_t BgL_regv1174z00_8124,
		obj_t BgL_regl1175z00_8125, obj_t BgL_string1176z00_8126)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2typeszd2(CINT(BgL_length1172z00_8122),
				CINT(BgL_msiza7e1173za7_8123), BgL_regv1174z00_8124,
				BgL_regl1175z00_8125, BgL_string1176z00_8126);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2typeszd2(obj_t
		BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_8943;

				BgL_classz00_8943 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_8944;

						BgL_arg1807z00_8944 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_8945;

								BgL_idxz00_8945 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8944);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8945 + 1L)) == BgL_classz00_8943);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res3587z00_8948;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_8949;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_8950;
										long BgL_arg1816z00_8951;

										BgL_arg1815z00_8950 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_8952;

											BgL_arg1817z00_8952 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8944);
											BgL_arg1816z00_8951 = (BgL_arg1817z00_8952 - OBJECT_TYPE);
										}
										BgL_oclassz00_8949 =
											VECTOR_REF(BgL_arg1815z00_8950, BgL_arg1816z00_8951);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_8953;

										BgL__ortest_1115z00_8953 =
											(BgL_classz00_8943 == BgL_oclassz00_8949);
										if (BgL__ortest_1115z00_8953)
											{	/* SawMill/regset.sch 85 */
												BgL_res3587z00_8948 = BgL__ortest_1115z00_8953;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_8954;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_8955;

													BgL_arg1804z00_8955 = (BgL_oclassz00_8949);
													BgL_odepthz00_8954 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8955);
												}
												if ((1L < BgL_odepthz00_8954))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_8956;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_8957;

															BgL_arg1803z00_8957 = (BgL_oclassz00_8949);
															BgL_arg1802z00_8956 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8957,
																1L);
														}
														BgL_res3587z00_8948 =
															(BgL_arg1802z00_8956 == BgL_classz00_8943);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res3587z00_8948 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3587z00_8948;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8127,
		obj_t BgL_objz00_8128)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2typeszd2(BgL_objz00_8128));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_4878;

				BgL_classz00_4878 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_4879;

					BgL__ortest_1117z00_4879 = BGL_CLASS_NIL(BgL_classz00_4878);
					if (CBOOL(BgL__ortest_1117z00_4879))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_4879);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4878));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8129)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2typeszd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8130,
		obj_t BgL_oz00_8131)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2typeszd2(
				((BgL_regsetz00_bglt) BgL_oz00_8131));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8132, obj_t BgL_oz00_8133, obj_t BgL_vz00_8134)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2typeszd2(
				((BgL_regsetz00_bglt) BgL_oz00_8133), BgL_vz00_8134);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8135,
		obj_t BgL_oz00_8136)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2typeszd2(
				((BgL_regsetz00_bglt) BgL_oz00_8136));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8137,
		obj_t BgL_oz00_8138)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2typeszd2(
				((BgL_regsetz00_bglt) BgL_oz00_8138));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8139,
		obj_t BgL_oz00_8140)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2typeszd2(
					((BgL_regsetz00_bglt) BgL_oz00_8140)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8141,
		obj_t BgL_oz00_8142)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2typeszd2(
					((BgL_regsetz00_bglt) BgL_oz00_8142)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2typeszd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8143, obj_t BgL_oz00_8144, obj_t BgL_vz00_8145)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2typeszd2(
				((BgL_regsetz00_bglt) BgL_oz00_8144), CINT(BgL_vz00_8145));
		}

	}



/* &bbv-ctx-init */
	obj_t BGl_z62bbvzd2ctxzd2initz62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8146,
		obj_t BgL_thisz00_8147)
	{
		{	/* SawBbv/bbv-types.scm 166 */
			BGl_za2bbvzd2ctxzd2idza2z00zzsaw_bbvzd2typeszd2 =
				(1L + BGl_za2bbvzd2ctxzd2idza2z00zzsaw_bbvzd2typeszd2);
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxzd2_bglt) BgL_thisz00_8147)))->BgL_idz00) =
				((long) BGl_za2bbvzd2ctxzd2idza2z00zzsaw_bbvzd2typeszd2), BUNSPEC);
		}

	}



/* get-bb-mark */
	BGL_EXPORTED_DEF obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 204 */
			BGl_za2bbzd2markza2zd2zzsaw_bbvzd2typeszd2 =
				(1L + BGl_za2bbzd2markza2zd2zzsaw_bbvzd2typeszd2);
			return BINT(BGl_za2bbzd2markza2zd2zzsaw_bbvzd2typeszd2);
		}

	}



/* &get-bb-mark */
	obj_t BGl_z62getzd2bbzd2markz62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8148)
	{
		{	/* SawBbv/bbv-types.scm 204 */
			return BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
		}

	}



/* bbv-queue-length */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2queuezd2lengthz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt
		BgL_queuez00_92)
	{
		{	/* SawBbv/bbv-types.scm 211 */
			return
				bgl_list_length(
				(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_92))->BgL_blocksz00));
		}

	}



/* &bbv-queue-length */
	obj_t BGl_z62bbvzd2queuezd2lengthz62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8149, obj_t BgL_queuez00_8150)
	{
		{	/* SawBbv/bbv-types.scm 211 */
			return
				BINT(BGl_bbvzd2queuezd2lengthz00zzsaw_bbvzd2typeszd2(
					((BgL_bbvzd2queuezd2_bglt) BgL_queuez00_8150)));
		}

	}



/* bbv-queue-empty? */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2emptyzf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt
		BgL_queuez00_93)
	{
		{	/* SawBbv/bbv-types.scm 218 */
			return
				BBOOL(NULLP(
					(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_93))->
						BgL_blocksz00)));
		}

	}



/* &bbv-queue-empty? */
	obj_t BGl_z62bbvzd2queuezd2emptyzf3z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8151, obj_t BgL_queuez00_8152)
	{
		{	/* SawBbv/bbv-types.scm 218 */
			return
				BGl_bbvzd2queuezd2emptyzf3zf3zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_queuez00_8152));
		}

	}



/* bbv-queue-push! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2pushz12z12zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt
		BgL_queuez00_94, BgL_blockz00_bglt BgL_versionz00_95)
	{
		{	/* SawBbv/bbv-types.scm 227 */
			{	/* SawBbv/bbv-types.scm 229 */
				obj_t BgL_nlastz00_2191;

				BgL_nlastz00_2191 = MAKE_YOUNG_PAIR(((obj_t) BgL_versionz00_95), BNIL);
				if (NULLP(
						(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_94))->
							BgL_lastz00)))
					{	/* SawBbv/bbv-types.scm 230 */
						((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_94))->
								BgL_blocksz00) = ((obj_t) BgL_nlastz00_2191), BUNSPEC);
					}
				else
					{	/* SawBbv/bbv-types.scm 231 */
						obj_t BgL_arg1960z00_2194;

						BgL_arg1960z00_2194 =
							(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_94))->
							BgL_lastz00);
						{	/* SawBbv/bbv-types.scm 231 */
							obj_t BgL_tmpz00_10493;

							BgL_tmpz00_10493 = ((obj_t) BgL_arg1960z00_2194);
							SET_CDR(BgL_tmpz00_10493, BgL_nlastz00_2191);
						}
					}
				return
					((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_94))->BgL_lastz00) =
					((obj_t) BgL_nlastz00_2191), BUNSPEC);
			}
		}

	}



/* &bbv-queue-push! */
	obj_t BGl_z62bbvzd2queuezd2pushz12z70zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8153, obj_t BgL_queuez00_8154, obj_t BgL_versionz00_8155)
	{
		{	/* SawBbv/bbv-types.scm 227 */
			return
				BGl_bbvzd2queuezd2pushz12z12zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_queuez00_8154),
				((BgL_blockz00_bglt) BgL_versionz00_8155));
		}

	}



/* bbv-queue-pop! */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_bbvzd2queuezd2popz12z12zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt
		BgL_queuez00_96)
	{
		{	/* SawBbv/bbv-types.scm 240 */
			if (NULLP(
					(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_96))->
						BgL_blocksz00)))
				{	/* SawBbv/bbv-types.scm 242 */
					return
						((BgL_blockz00_bglt)
						BGl_errorz00zz__errorz00(BGl_string3679z00zzsaw_bbvzd2typeszd2,
							BGl_string3680z00zzsaw_bbvzd2typeszd2,
							((obj_t) BgL_queuez00_96)));
				}
			else
				{	/* SawBbv/bbv-types.scm 243 */
					obj_t BgL_bz00_2199;

					BgL_bz00_2199 =
						CAR(
						(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_96))->
							BgL_blocksz00));
					{
						obj_t BgL_auxz00_10508;

						{	/* SawBbv/bbv-types.scm 244 */
							obj_t BgL_pairz00_4889;

							BgL_pairz00_4889 =
								(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_96))->
								BgL_blocksz00);
							BgL_auxz00_10508 = CDR(BgL_pairz00_4889);
						}
						((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_96))->
								BgL_blocksz00) = ((obj_t) BgL_auxz00_10508), BUNSPEC);
					}
					if (NULLP(
							(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_96))->
								BgL_blocksz00)))
						{	/* SawBbv/bbv-types.scm 245 */
							((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_96))->
									BgL_lastz00) = ((obj_t) BNIL), BUNSPEC);
						}
					else
						{	/* SawBbv/bbv-types.scm 245 */
							BFALSE;
						}
					return ((BgL_blockz00_bglt) BgL_bz00_2199);
				}
		}

	}



/* &bbv-queue-pop! */
	BgL_blockz00_bglt BGl_z62bbvzd2queuezd2popz12z70zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8156, obj_t BgL_queuez00_8157)
	{
		{	/* SawBbv/bbv-types.scm 240 */
			return
				BGl_bbvzd2queuezd2popz12z12zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_queuez00_8157));
		}

	}



/* bbv-queue-has? */
	BGL_EXPORTED_DEF bool_t
		BGl_bbvzd2queuezd2haszf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2queuezd2_bglt
		BgL_queuez00_97, BgL_blockz00_bglt BgL_bz00_98)
	{
		{	/* SawBbv/bbv-types.scm 253 */
			return
				CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
					((obj_t) BgL_bz00_98),
					(((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_queuez00_97))->
						BgL_blocksz00)));
		}

	}



/* &bbv-queue-has? */
	obj_t BGl_z62bbvzd2queuezd2haszf3z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8158, obj_t BgL_queuez00_8159, obj_t BgL_bz00_8160)
	{
		{	/* SawBbv/bbv-types.scm 253 */
			return
				BBOOL(BGl_bbvzd2queuezd2haszf3zf3zzsaw_bbvzd2typeszd2(
					((BgL_bbvzd2queuezd2_bglt) BgL_queuez00_8159),
					((BgL_blockz00_bglt) BgL_bz00_8160)));
		}

	}



/* blockV-live-versions */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt
		BgL_bvz00_99)
	{
		{	/* SawBbv/bbv-types.scm 260 */
			{	/* SawBbv/bbv-types.scm 262 */
				obj_t BgL_hook1662z00_2209;

				BgL_hook1662z00_2209 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{	/* SawBbv/bbv-types.scm 262 */
					obj_t BgL_g1663z00_2210;

					{
						BgL_blockvz00_bglt BgL_auxz00_10528;

						{
							obj_t BgL_auxz00_10529;

							{	/* SawBbv/bbv-types.scm 262 */
								BgL_objectz00_bglt BgL_tmpz00_10530;

								BgL_tmpz00_10530 = ((BgL_objectz00_bglt) BgL_bvz00_99);
								BgL_auxz00_10529 = BGL_OBJECT_WIDENING(BgL_tmpz00_10530);
							}
							BgL_auxz00_10528 = ((BgL_blockvz00_bglt) BgL_auxz00_10529);
						}
						BgL_g1663z00_2210 =
							(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_10528))->
							BgL_versionsz00);
					}
					{
						obj_t BgL_l1659z00_2212;
						obj_t BgL_h1660z00_2213;

						BgL_l1659z00_2212 = BgL_g1663z00_2210;
						BgL_h1660z00_2213 = BgL_hook1662z00_2209;
					BgL_zc3z04anonymousza31971ze3z87_2214:
						if (NULLP(BgL_l1659z00_2212))
							{	/* SawBbv/bbv-types.scm 262 */
								return CDR(BgL_hook1662z00_2209);
							}
						else
							{	/* SawBbv/bbv-types.scm 262 */
								bool_t BgL_test4202z00_10538;

								{	/* SawBbv/bbv-types.scm 262 */
									obj_t BgL_arg1978z00_2222;

									BgL_arg1978z00_2222 = CAR(((obj_t) BgL_l1659z00_2212));
									BgL_test4202z00_10538 =
										BGl_blockzd2livezf3z21zzsaw_bbvzd2typeszd2(
										((BgL_blockz00_bglt) BgL_arg1978z00_2222));
								}
								if (BgL_test4202z00_10538)
									{	/* SawBbv/bbv-types.scm 262 */
										obj_t BgL_nh1661z00_2218;

										{	/* SawBbv/bbv-types.scm 262 */
											obj_t BgL_arg1976z00_2220;

											BgL_arg1976z00_2220 = CAR(((obj_t) BgL_l1659z00_2212));
											BgL_nh1661z00_2218 =
												MAKE_YOUNG_PAIR(BgL_arg1976z00_2220, BNIL);
										}
										SET_CDR(BgL_h1660z00_2213, BgL_nh1661z00_2218);
										{	/* SawBbv/bbv-types.scm 262 */
											obj_t BgL_arg1975z00_2219;

											BgL_arg1975z00_2219 = CDR(((obj_t) BgL_l1659z00_2212));
											{
												obj_t BgL_h1660z00_10550;
												obj_t BgL_l1659z00_10549;

												BgL_l1659z00_10549 = BgL_arg1975z00_2219;
												BgL_h1660z00_10550 = BgL_nh1661z00_2218;
												BgL_h1660z00_2213 = BgL_h1660z00_10550;
												BgL_l1659z00_2212 = BgL_l1659z00_10549;
												goto BgL_zc3z04anonymousza31971ze3z87_2214;
											}
										}
									}
								else
									{	/* SawBbv/bbv-types.scm 262 */
										obj_t BgL_arg1977z00_2221;

										BgL_arg1977z00_2221 = CDR(((obj_t) BgL_l1659z00_2212));
										{
											obj_t BgL_l1659z00_10553;

											BgL_l1659z00_10553 = BgL_arg1977z00_2221;
											BgL_l1659z00_2212 = BgL_l1659z00_10553;
											goto BgL_zc3z04anonymousza31971ze3z87_2214;
										}
									}
							}
					}
				}
			}
		}

	}



/* &blockV-live-versions */
	obj_t BGl_z62blockVzd2livezd2versionsz62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8161, obj_t BgL_bvz00_8162)
	{
		{	/* SawBbv/bbv-types.scm 260 */
			return
				BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2(
				((BgL_blockz00_bglt) BgL_bvz00_8162));
		}

	}



/* block-live? */
	BGL_EXPORTED_DEF bool_t
		BGl_blockzd2livezf3z21zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt BgL_bsz00_100)
	{
		{	/* SawBbv/bbv-types.scm 267 */
			{	/* SawBbv/bbv-types.scm 269 */
				obj_t BgL__andtest_1247z00_2225;

				{
					BgL_blocksz00_bglt BgL_auxz00_10556;

					{
						obj_t BgL_auxz00_10557;

						{	/* SawBbv/bbv-types.scm 269 */
							BgL_objectz00_bglt BgL_tmpz00_10558;

							BgL_tmpz00_10558 = ((BgL_objectz00_bglt) BgL_bsz00_100);
							BgL_auxz00_10557 = BGL_OBJECT_WIDENING(BgL_tmpz00_10558);
						}
						BgL_auxz00_10556 = ((BgL_blocksz00_bglt) BgL_auxz00_10557);
					}
					BgL__andtest_1247z00_2225 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_10556))->BgL_mblockz00);
				}
				if (CBOOL(BgL__andtest_1247z00_2225))
					{	/* SawBbv/bbv-types.scm 269 */
						return ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-types.scm 270 */
						obj_t BgL_casezd2valuezd2_2226;

						BgL_casezd2valuezd2_2226 =
							BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2;
						if ((BgL_casezd2valuezd2_2226 == CNST_TABLE_REF(0)))
							{	/* SawBbv/bbv-types.scm 270 */
								return
									CBOOL(BGl_bbvzd2gczd2blockzd2reachablezf3z21zzsaw_bbvzd2gczd2
									(BgL_bsz00_100));
							}
						else
							{	/* SawBbv/bbv-types.scm 270 */
								if ((BgL_casezd2valuezd2_2226 == CNST_TABLE_REF(1)))
									{	/* SawBbv/bbv-types.scm 272 */
										long BgL_arg1981z00_2229;

										{
											BgL_blocksz00_bglt BgL_auxz00_10573;

											{
												obj_t BgL_auxz00_10574;

												{	/* SawBbv/bbv-types.scm 272 */
													BgL_objectz00_bglt BgL_tmpz00_10575;

													BgL_tmpz00_10575 =
														((BgL_objectz00_bglt) BgL_bsz00_100);
													BgL_auxz00_10574 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_10575);
												}
												BgL_auxz00_10573 =
													((BgL_blocksz00_bglt) BgL_auxz00_10574);
											}
											BgL_arg1981z00_2229 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_10573))->
												BgL_gccntz00);
										}
										return (BgL_arg1981z00_2229 > 0L);
									}
								else
									{	/* SawBbv/bbv-types.scm 270 */
										return ((bool_t) 1);
									}
							}
					}
			}
		}

	}



/* &block-live? */
	obj_t BGl_z62blockzd2livezf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8163,
		obj_t BgL_bsz00_8164)
	{
		{	/* SawBbv/bbv-types.scm 267 */
			return
				BBOOL(BGl_blockzd2livezf3z21zzsaw_bbvzd2typeszd2(
					((BgL_blockz00_bglt) BgL_bsz00_8164)));
		}

	}



/* params->ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_paramszd2ze3ctxz31zzsaw_bbvzd2typeszd2(obj_t BgL_paramsz00_101)
	{
		{	/* SawBbv/bbv-types.scm 278 */
			{	/* SawBbv/bbv-types.scm 279 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1249z00_2230;

				{	/* SawBbv/bbv-types.scm 279 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1248z00_2262;

					BgL_new1248z00_2262 =
						((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxzd2_bgl))));
					{	/* SawBbv/bbv-types.scm 279 */
						long BgL_arg2004z00_2263;

						BgL_arg2004z00_2263 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1248z00_2262), BgL_arg2004z00_2263);
					}
					BgL_new1249z00_2230 = BgL_new1248z00_2262;
				}
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1249z00_2230))->BgL_idz00) =
					((long) -1L), BUNSPEC);
				{
					obj_t BgL_auxz00_10589;

					{	/* SawBbv/bbv-types.scm 280 */
						obj_t BgL_arg1983z00_2232;

						{	/* SawBbv/bbv-types.scm 283 */
							obj_t BgL_list1990z00_2241;

							BgL_list1990z00_2241 = MAKE_YOUNG_PAIR(BgL_paramsz00_101, BNIL);
							BgL_arg1983z00_2232 =
								BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
								(BGl_proc3681z00zzsaw_bbvzd2typeszd2, BgL_list1990z00_2241);
						}
						BgL_auxz00_10589 =
							BGl_sortz00zz__r4_vectors_6_8z00
							(BGl_proc3682z00zzsaw_bbvzd2typeszd2, BgL_arg1983z00_2232);
					}
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1249z00_2230))->
							BgL_entriesz00) = ((obj_t) BgL_auxz00_10589), BUNSPEC);
				}
				{	/* SawBbv/bbv-types.scm 279 */
					obj_t BgL_fun2003z00_2261;

					BgL_fun2003z00_2261 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_PROCEDURE_CALL1(BgL_fun2003z00_2261,
						((obj_t) BgL_new1249z00_2230));
				}
				return BgL_new1249z00_2230;
			}
		}

	}



/* &params->ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62paramszd2ze3ctxz53zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8167, obj_t BgL_paramsz00_8168)
	{
		{	/* SawBbv/bbv-types.scm 278 */
			return BGl_paramszd2ze3ctxz31zzsaw_bbvzd2typeszd2(BgL_paramsz00_8168);
		}

	}



/* &<@anonymous:1984> */
	obj_t BGl_z62zc3z04anonymousza31984ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8169, obj_t BgL_xz00_8170, obj_t BgL_yz00_8171)
	{
		{	/* SawBbv/bbv-types.scm 280 */
			{	/* SawBbv/bbv-types.scm 281 */
				bool_t BgL_tmpz00_10601;

				{	/* SawBbv/bbv-types.scm 281 */
					int BgL_arg1985z00_8959;
					int BgL_arg1986z00_8960;

					{	/* SawBbv/bbv-types.scm 281 */
						BgL_rtl_regz00_bglt BgL_oz00_8961;

						BgL_oz00_8961 =
							((BgL_rtl_regz00_bglt)
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_xz00_8170)))->
								BgL_regz00));
						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_10605;

							{
								obj_t BgL_auxz00_10606;

								{	/* SawBbv/bbv-types.scm 281 */
									BgL_objectz00_bglt BgL_tmpz00_10607;

									BgL_tmpz00_10607 = ((BgL_objectz00_bglt) BgL_oz00_8961);
									BgL_auxz00_10606 = BGL_OBJECT_WIDENING(BgL_tmpz00_10607);
								}
								BgL_auxz00_10605 =
									((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10606);
							}
							BgL_arg1985z00_8959 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10605))->
								BgL_numz00);
					}}
					{	/* SawBbv/bbv-types.scm 282 */
						BgL_rtl_regz00_bglt BgL_oz00_8962;

						BgL_oz00_8962 =
							((BgL_rtl_regz00_bglt)
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_yz00_8171)))->
								BgL_regz00));
						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_10615;

							{
								obj_t BgL_auxz00_10616;

								{	/* SawBbv/bbv-types.scm 282 */
									BgL_objectz00_bglt BgL_tmpz00_10617;

									BgL_tmpz00_10617 = ((BgL_objectz00_bglt) BgL_oz00_8962);
									BgL_auxz00_10616 = BGL_OBJECT_WIDENING(BgL_tmpz00_10617);
								}
								BgL_auxz00_10615 =
									((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10616);
							}
							BgL_arg1986z00_8960 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10615))->
								BgL_numz00);
					}}
					BgL_tmpz00_10601 =
						((long) (BgL_arg1985z00_8959) <= (long) (BgL_arg1986z00_8960));
				}
				return BBOOL(BgL_tmpz00_10601);
			}
		}

	}



/* &<@anonymous:1991> */
	obj_t BGl_z62zc3z04anonymousza31991ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8172, obj_t BgL_pz00_8173)
	{
		{	/* SawBbv/bbv-types.scm 283 */
			{	/* SawBbv/bbv-types.scm 284 */
				bool_t BgL_test4206z00_10626;

				{	/* SawBbv/bbv-types.scm 284 */
					obj_t BgL_classz00_8963;

					BgL_classz00_8963 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
					if (BGL_OBJECTP(BgL_pz00_8173))
						{	/* SawBbv/bbv-types.scm 284 */
							BgL_objectz00_bglt BgL_arg1807z00_8964;

							BgL_arg1807z00_8964 = (BgL_objectz00_bglt) (BgL_pz00_8173);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 284 */
									long BgL_idxz00_8965;

									BgL_idxz00_8965 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8964);
									BgL_test4206z00_10626 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_8965 + 2L)) == BgL_classz00_8963);
								}
							else
								{	/* SawBbv/bbv-types.scm 284 */
									bool_t BgL_res3588z00_8968;

									{	/* SawBbv/bbv-types.scm 284 */
										obj_t BgL_oclassz00_8969;

										{	/* SawBbv/bbv-types.scm 284 */
											obj_t BgL_arg1815z00_8970;
											long BgL_arg1816z00_8971;

											BgL_arg1815z00_8970 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 284 */
												long BgL_arg1817z00_8972;

												BgL_arg1817z00_8972 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8964);
												BgL_arg1816z00_8971 =
													(BgL_arg1817z00_8972 - OBJECT_TYPE);
											}
											BgL_oclassz00_8969 =
												VECTOR_REF(BgL_arg1815z00_8970, BgL_arg1816z00_8971);
										}
										{	/* SawBbv/bbv-types.scm 284 */
											bool_t BgL__ortest_1115z00_8973;

											BgL__ortest_1115z00_8973 =
												(BgL_classz00_8963 == BgL_oclassz00_8969);
											if (BgL__ortest_1115z00_8973)
												{	/* SawBbv/bbv-types.scm 284 */
													BgL_res3588z00_8968 = BgL__ortest_1115z00_8973;
												}
											else
												{	/* SawBbv/bbv-types.scm 284 */
													long BgL_odepthz00_8974;

													{	/* SawBbv/bbv-types.scm 284 */
														obj_t BgL_arg1804z00_8975;

														BgL_arg1804z00_8975 = (BgL_oclassz00_8969);
														BgL_odepthz00_8974 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_8975);
													}
													if ((2L < BgL_odepthz00_8974))
														{	/* SawBbv/bbv-types.scm 284 */
															obj_t BgL_arg1802z00_8976;

															{	/* SawBbv/bbv-types.scm 284 */
																obj_t BgL_arg1803z00_8977;

																BgL_arg1803z00_8977 = (BgL_oclassz00_8969);
																BgL_arg1802z00_8976 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8977,
																	2L);
															}
															BgL_res3588z00_8968 =
																(BgL_arg1802z00_8976 == BgL_classz00_8963);
														}
													else
														{	/* SawBbv/bbv-types.scm 284 */
															BgL_res3588z00_8968 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4206z00_10626 = BgL_res3588z00_8968;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 284 */
							BgL_test4206z00_10626 = ((bool_t) 0);
						}
				}
				if (BgL_test4206z00_10626)
					{	/* SawBbv/bbv-types.scm 286 */
						BgL_bbvzd2ctxentryzd2_bglt BgL_new1252z00_8978;

						{	/* SawBbv/bbv-types.scm 287 */
							BgL_bbvzd2ctxentryzd2_bglt BgL_new1251z00_8979;

							BgL_new1251z00_8979 =
								((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bbvzd2ctxentryzd2_bgl))));
							{	/* SawBbv/bbv-types.scm 287 */
								long BgL_arg2002z00_8980;

								BgL_arg2002z00_8980 =
									BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1251z00_8979),
									BgL_arg2002z00_8980);
							}
							BgL_new1252z00_8978 = BgL_new1251z00_8979;
						}
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
								BgL_regz00) =
							((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_pz00_8173)),
							BUNSPEC);
						{
							obj_t BgL_auxz00_10655;

							{	/* SawBbv/bbv-types.scm 288 */
								BgL_typez00_bglt BgL_arg1993z00_8981;

								BgL_arg1993z00_8981 =
									(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt) BgL_pz00_8173)))->BgL_typez00);
								{	/* SawBbv/bbv-types.scm 288 */
									obj_t BgL_list1994z00_8982;

									BgL_list1994z00_8982 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg1993z00_8981), BNIL);
									BgL_auxz00_10655 = BgL_list1994z00_8982;
							}}
							((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
									BgL_typesz00) = ((obj_t) BgL_auxz00_10655), BUNSPEC);
						}
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
								BgL_polarityz00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
								BgL_countz00) = ((long) 10L), BUNSPEC);
						{
							obj_t BgL_auxz00_10663;

							{	/* SawBbv/bbv-types.scm 290 */
								bool_t BgL_test4211z00_10664;

								{	/* SawBbv/bbv-types.scm 290 */
									bool_t BgL_test4212z00_10665;

									{	/* SawBbv/bbv-types.scm 290 */
										BgL_typez00_bglt BgL_arg2001z00_8983;

										BgL_arg2001z00_8983 =
											(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_pz00_8173)))->BgL_typez00);
										BgL_test4212z00_10665 =
											(
											((obj_t) BgL_arg2001z00_8983) ==
											BGl_za2bintza2z00zztype_cachez00);
									}
									if (BgL_test4212z00_10665)
										{	/* SawBbv/bbv-types.scm 290 */
											BgL_test4211z00_10664 = ((bool_t) 1);
										}
									else
										{	/* SawBbv/bbv-types.scm 291 */
											BgL_typez00_bglt BgL_arg2000z00_8984;

											BgL_arg2000z00_8984 =
												(((BgL_rtl_regz00_bglt) COBJECT(
														((BgL_rtl_regz00_bglt) BgL_pz00_8173)))->
												BgL_typez00);
											BgL_test4211z00_10664 =
												(((obj_t) BgL_arg2000z00_8984) ==
												BGl_za2longza2z00zztype_cachez00);
										}
								}
								if (BgL_test4211z00_10664)
									{	/* SawBbv/bbv-types.scm 290 */
										BgL_auxz00_10663 =
											((obj_t) BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2());
									}
								else
									{	/* SawBbv/bbv-types.scm 290 */
										BgL_auxz00_10663 = CNST_TABLE_REF(2);
									}
							}
							((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
									BgL_valuez00) = ((obj_t) BgL_auxz00_10663), BUNSPEC);
						}
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
								BgL_aliasesz00) = ((obj_t) BNIL), BUNSPEC);
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1252z00_8978))->
								BgL_initvalz00) = ((obj_t) BUNSPEC), BUNSPEC);
						return ((obj_t) BgL_new1252z00_8978);
					}
				else
					{	/* SawBbv/bbv-types.scm 284 */
						return BFALSE;
					}
			}
		}

	}



/* list-eq? */
	obj_t BGl_listzd2eqzf3z21zzsaw_bbvzd2typeszd2(obj_t BgL_l1z00_102,
		obj_t BgL_l2z00_103)
	{
		{	/* SawBbv/bbv-types.scm 300 */
			if ((bgl_list_length(BgL_l1z00_102) == bgl_list_length(BgL_l2z00_103)))
				{	/* SawBbv/bbv-types.scm 302 */
					obj_t BgL_list2009z00_2267;

					{	/* SawBbv/bbv-types.scm 302 */
						obj_t BgL_arg2010z00_2268;

						BgL_arg2010z00_2268 = MAKE_YOUNG_PAIR(BgL_l2z00_103, BNIL);
						BgL_list2009z00_2267 =
							MAKE_YOUNG_PAIR(BgL_l1z00_102, BgL_arg2010z00_2268);
					}
					return
						BGl_everyz00zz__r4_pairs_and_lists_6_3z00
						(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BgL_list2009z00_2267);
				}
			else
				{	/* SawBbv/bbv-types.scm 301 */
					return BFALSE;
				}
		}

	}



/* bbv-ctxentry-equal? */
	obj_t
		BGl_bbvzd2ctxentryzd2equalzf3zf3zzsaw_bbvzd2typeszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_xz00_104,
		BgL_bbvzd2ctxentryzd2_bglt BgL_yz00_105)
	{
		{	/* SawBbv/bbv-types.scm 307 */
			if (
				(((obj_t)
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_104))->
							BgL_regz00)) ==
					((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_105))->
							BgL_regz00))))
				{	/* SawBbv/bbv-types.scm 318 */
					if (
						(BBOOL(
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_104))->
									BgL_polarityz00)) ==
							BBOOL((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_105))->
									BgL_polarityz00))))
						{	/* SawBbv/bbv-types.scm 319 */
							if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_104))->
										BgL_valuez00),
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_105))->
										BgL_valuez00)))
								{	/* SawBbv/bbv-types.scm 321 */
									obj_t BgL__andtest_1258z00_2276;

									BgL__andtest_1258z00_2276 =
										BGl_listzd2eqzf3z21zzsaw_bbvzd2typeszd2(
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_104))->
											BgL_typesz00),
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_105))->
											BgL_typesz00));
									if (CBOOL(BgL__andtest_1258z00_2276))
										{	/* SawBbv/bbv-types.scm 321 */
											return
												BGl_listzd2eqzf3z21zzsaw_bbvzd2typeszd2(
												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_104))->
													BgL_aliasesz00),
												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_105))->
													BgL_aliasesz00));
										}
									else
										{	/* SawBbv/bbv-types.scm 321 */
											return BFALSE;
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 320 */
									return BFALSE;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 319 */
							return BFALSE;
						}
				}
			else
				{	/* SawBbv/bbv-types.scm 318 */
					return BFALSE;
				}
		}

	}



/* bbv-ctx-equal? */
	BGL_EXPORTED_DEF bool_t
		BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_xz00_106, BgL_bbvzd2ctxzd2_bglt BgL_yz00_107)
	{
		{	/* SawBbv/bbv-types.scm 327 */
			if (
				(bgl_list_length(
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_xz00_106))->
							BgL_entriesz00)) ==
					bgl_list_length((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_yz00_107))->
							BgL_entriesz00))))
				{	/* SawBbv/bbv-types.scm 331 */
					obj_t BgL_g1666z00_2294;

					BgL_g1666z00_2294 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_xz00_106))->BgL_entriesz00);
					{
						obj_t BgL_l1664z00_2296;

						BgL_l1664z00_2296 = BgL_g1666z00_2294;
					BgL_zc3z04anonymousza32029ze3z87_2297:
						if (NULLP(BgL_l1664z00_2296))
							{	/* SawBbv/bbv-types.scm 331 */
								return ((bool_t) 1);
							}
						else
							{	/* SawBbv/bbv-types.scm 332 */
								obj_t BgL_nvz00_2299;

								{	/* SawBbv/bbv-types.scm 332 */
									obj_t BgL_xez00_2302;

									BgL_xez00_2302 = CAR(((obj_t) BgL_l1664z00_2296));
									{	/* SawBbv/bbv-types.scm 333 */
										obj_t BgL_yez00_2304;

										BgL_yez00_2304 =
											BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_yz00_107,
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_xez00_2302)))->
												BgL_regz00));
										BgL_nvz00_2299 =
											BGl_bbvzd2ctxentryzd2equalzf3zf3zzsaw_bbvzd2typeszd2((
												(BgL_bbvzd2ctxentryzd2_bglt) BgL_xez00_2302),
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_yez00_2304));
									}
								}
								if (CBOOL(BgL_nvz00_2299))
									{	/* SawBbv/bbv-types.scm 331 */
										obj_t BgL_arg2031z00_2301;

										BgL_arg2031z00_2301 = CDR(((obj_t) BgL_l1664z00_2296));
										{
											obj_t BgL_l1664z00_10733;

											BgL_l1664z00_10733 = BgL_arg2031z00_2301;
											BgL_l1664z00_2296 = BgL_l1664z00_10733;
											goto BgL_zc3z04anonymousza32029ze3z87_2297;
										}
									}
								else
									{	/* SawBbv/bbv-types.scm 331 */
										return ((bool_t) 0);
									}
							}
					}
				}
			else
				{	/* SawBbv/bbv-types.scm 330 */
					return ((bool_t) 0);
				}
		}

	}



/* &bbv-ctx-equal? */
	obj_t BGl_z62bbvzd2ctxzd2equalzf3z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8177, obj_t BgL_xz00_8178, obj_t BgL_yz00_8179)
	{
		{	/* SawBbv/bbv-types.scm 327 */
			return
				BBOOL(BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2(
					((BgL_bbvzd2ctxzd2_bglt) BgL_xz00_8178),
					((BgL_bbvzd2ctxzd2_bglt) BgL_yz00_8179)));
		}

	}



/* bbv-ctx-assoc */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2assocz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_cz00_108, obj_t BgL_versionsz00_109)
	{
		{	/* SawBbv/bbv-types.scm 340 */
			{
				obj_t BgL_versionsz00_2312;

				BgL_versionsz00_2312 = BgL_versionsz00_109;
			BgL_zc3z04anonymousza32039ze3z87_2313:
				if (PAIRP(BgL_versionsz00_2312))
					{	/* SawBbv/bbv-types.scm 343 */
						BgL_blockz00_bglt BgL_i1262z00_2315;

						BgL_i1262z00_2315 = ((BgL_blockz00_bglt) CAR(BgL_versionsz00_2312));
						{	/* SawBbv/bbv-types.scm 344 */
							bool_t BgL_test4222z00_10742;

							{	/* SawBbv/bbv-types.scm 344 */
								BgL_bbvzd2ctxzd2_bglt BgL_arg2045z00_2319;

								{
									BgL_blocksz00_bglt BgL_auxz00_10743;

									{
										obj_t BgL_auxz00_10744;

										{	/* SawBbv/bbv-types.scm 344 */
											BgL_objectz00_bglt BgL_tmpz00_10745;

											BgL_tmpz00_10745 =
												((BgL_objectz00_bglt) BgL_i1262z00_2315);
											BgL_auxz00_10744 = BGL_OBJECT_WIDENING(BgL_tmpz00_10745);
										}
										BgL_auxz00_10743 = ((BgL_blocksz00_bglt) BgL_auxz00_10744);
									}
									BgL_arg2045z00_2319 =
										(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_10743))->
										BgL_ctxz00);
								}
								BgL_test4222z00_10742 =
									BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2(BgL_cz00_108,
									BgL_arg2045z00_2319);
							}
							if (BgL_test4222z00_10742)
								{	/* SawBbv/bbv-types.scm 344 */
									return CAR(BgL_versionsz00_2312);
								}
							else
								{
									obj_t BgL_versionsz00_10752;

									BgL_versionsz00_10752 = CDR(BgL_versionsz00_2312);
									BgL_versionsz00_2312 = BgL_versionsz00_10752;
									goto BgL_zc3z04anonymousza32039ze3z87_2313;
								}
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 342 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-ctx-assoc */
	obj_t BGl_z62bbvzd2ctxzd2assocz62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8180,
		obj_t BgL_cz00_8181, obj_t BgL_versionsz00_8182)
	{
		{	/* SawBbv/bbv-types.scm 340 */
			return
				BGl_bbvzd2ctxzd2assocz00zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_cz00_8181), BgL_versionsz00_8182);
		}

	}



/* bbv-ctx-get */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_110, BgL_rtl_regz00_bglt BgL_regz00_111)
	{
		{	/* SawBbv/bbv-types.scm 351 */
			{
				obj_t BgL_esz00_2324;

				BgL_esz00_2324 =
					(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_110))->BgL_entriesz00);
			BgL_zc3z04anonymousza32047ze3z87_2325:
				if (PAIRP(BgL_esz00_2324))
					{	/* SawBbv/bbv-types.scm 354 */
						if (
							(((obj_t)
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt)
													CAR(BgL_esz00_2324))))->BgL_regz00)) ==
								((obj_t) BgL_regz00_111)))
							{	/* SawBbv/bbv-types.scm 355 */
								return CAR(BgL_esz00_2324);
							}
						else
							{
								obj_t BgL_esz00_10766;

								BgL_esz00_10766 = CDR(BgL_esz00_2324);
								BgL_esz00_2324 = BgL_esz00_10766;
								goto BgL_zc3z04anonymousza32047ze3z87_2325;
							}
					}
				else
					{	/* SawBbv/bbv-types.scm 354 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-ctx-get */
	obj_t BGl_z62bbvzd2ctxzd2getz62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8183,
		obj_t BgL_ctxz00_8184, obj_t BgL_regz00_8185)
	{
		{	/* SawBbv/bbv-types.scm 351 */
			return
				BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_8184),
				((BgL_rtl_regz00_bglt) BgL_regz00_8185));
		}

	}



/* extend-ctx/entry */
	BGL_EXPORTED_DEF obj_t
		BGl_extendzd2ctxzf2entryz20zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_112, BgL_bbvzd2ctxentryzd2_bglt BgL_entryz00_113)
	{
		{	/* SawBbv/bbv-types.scm 365 */
			{	/* SawBbv/bbv-types.scm 382 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1265z00_2336;

				{	/* SawBbv/bbv-types.scm 382 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1268z00_2338;

					BgL_new1268z00_2338 =
						((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxzd2_bgl))));
					{	/* SawBbv/bbv-types.scm 382 */
						long BgL_arg2058z00_2339;

						BgL_arg2058z00_2339 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1268z00_2338), BgL_arg2058z00_2339);
					}
					BgL_new1265z00_2336 = BgL_new1268z00_2338;
				}
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1265z00_2336))->BgL_idz00) =
					((long) (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_112))->
							BgL_idz00)), BUNSPEC);
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1265z00_2336))->
						BgL_entriesz00) =
					((obj_t)
						BGl_extendzd2entriesze71z35zzsaw_bbvzd2typeszd2(BgL_ctxz00_112,
							BgL_entryz00_113)), BUNSPEC);
				{	/* SawBbv/bbv-types.scm 383 */
					obj_t BgL_fun2057z00_2337;

					BgL_fun2057z00_2337 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_PROCEDURE_CALL1(BgL_fun2057z00_2337,
						((obj_t) BgL_new1265z00_2336));
				}
				return ((obj_t) BgL_new1265z00_2336);
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzsaw_bbvzd2typeszd2(int BgL_rnumz00_8906,
		BgL_rtl_regz00_bglt BgL_regz00_8905,
		BgL_bbvzd2ctxentryzd2_bglt BgL_entryz00_8904, obj_t BgL_esz00_2348)
	{
		{	/* SawBbv/bbv-types.scm 371 */
			if (NULLP(BgL_esz00_2348))
				{	/* SawBbv/bbv-types.scm 374 */
					obj_t BgL_list2063z00_2351;

					BgL_list2063z00_2351 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_entryz00_8904), BNIL);
					return BgL_list2063z00_2351;
				}
			else
				{	/* SawBbv/bbv-types.scm 375 */
					bool_t BgL_test4226z00_10791;

					{	/* SawBbv/bbv-types.scm 375 */
						int BgL_arg2079z00_2365;

						{	/* SawBbv/bbv-types.scm 375 */
							BgL_rtl_regz00_bglt BgL_oz00_4977;

							BgL_oz00_4977 =
								((BgL_rtl_regz00_bglt)
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt)
												CAR(((obj_t) BgL_esz00_2348)))))->BgL_regz00));
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_10797;

								{
									obj_t BgL_auxz00_10798;

									{	/* SawBbv/bbv-types.scm 375 */
										BgL_objectz00_bglt BgL_tmpz00_10799;

										BgL_tmpz00_10799 = ((BgL_objectz00_bglt) BgL_oz00_4977);
										BgL_auxz00_10798 = BGL_OBJECT_WIDENING(BgL_tmpz00_10799);
									}
									BgL_auxz00_10797 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10798);
								}
								BgL_arg2079z00_2365 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10797))->
									BgL_numz00);
						}}
						BgL_test4226z00_10791 =
							((long) (BgL_arg2079z00_2365) > (long) (BgL_rnumz00_8906));
					}
					if (BgL_test4226z00_10791)
						{	/* SawBbv/bbv-types.scm 375 */
							return
								MAKE_YOUNG_PAIR(((obj_t) BgL_entryz00_8904), BgL_esz00_2348);
						}
					else
						{	/* SawBbv/bbv-types.scm 375 */
							if (
								(((obj_t)
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt)
														CAR(
															((obj_t) BgL_esz00_2348)))))->BgL_regz00)) ==
									((obj_t) BgL_regz00_8905)))
								{	/* SawBbv/bbv-types.scm 378 */
									obj_t BgL_arg2072z00_2359;

									BgL_arg2072z00_2359 = CDR(((obj_t) BgL_esz00_2348));
									return
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_entryz00_8904), BgL_arg2072z00_2359);
								}
							else
								{	/* SawBbv/bbv-types.scm 380 */
									obj_t BgL_arg2074z00_2360;
									obj_t BgL_arg2075z00_2361;

									BgL_arg2074z00_2360 = CAR(((obj_t) BgL_esz00_2348));
									{	/* SawBbv/bbv-types.scm 380 */
										obj_t BgL_arg2076z00_2362;

										BgL_arg2076z00_2362 = CDR(((obj_t) BgL_esz00_2348));
										BgL_arg2075z00_2361 =
											BGl_loopze71ze7zzsaw_bbvzd2typeszd2(BgL_rnumz00_8906,
											BgL_regz00_8905, BgL_entryz00_8904, BgL_arg2076z00_2362);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg2074z00_2360, BgL_arg2075z00_2361);
								}
						}
				}
		}

	}



/* extend-entries~1 */
	obj_t BGl_extendzd2entriesze71z35zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_2340, BgL_bbvzd2ctxentryzd2_bglt BgL_entryz00_2341)
	{
		{	/* SawBbv/bbv-types.scm 380 */
			{	/* SawBbv/bbv-types.scm 368 */
				BgL_rtl_regz00_bglt BgL_regz00_2343;

				BgL_regz00_2343 =
					(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_entryz00_2341))->
					BgL_regz00);
				{	/* SawBbv/bbv-types.scm 368 */
					int BgL_rnumz00_2344;

					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_10828;

						{
							obj_t BgL_auxz00_10829;

							{	/* SawBbv/bbv-types.scm 369 */
								BgL_objectz00_bglt BgL_tmpz00_10830;

								BgL_tmpz00_10830 =
									((BgL_objectz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_regz00_2343));
								BgL_auxz00_10829 = BGL_OBJECT_WIDENING(BgL_tmpz00_10830);
							}
							BgL_auxz00_10828 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10829);
						}
						BgL_rnumz00_2344 =
							(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10828))->
							BgL_numz00);
					}
					{	/* SawBbv/bbv-types.scm 369 */

						return
							BGl_loopze71ze7zzsaw_bbvzd2typeszd2(BgL_rnumz00_2344,
							BgL_regz00_2343, BgL_entryz00_2341,
							(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2340))->
								BgL_entriesz00));
					}
				}
			}
		}

	}



/* &extend-ctx/entry */
	obj_t BGl_z62extendzd2ctxzf2entryz42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8186, obj_t BgL_ctxz00_8187, obj_t BgL_entryz00_8188)
	{
		{	/* SawBbv/bbv-types.scm 365 */
			return
				BGl_extendzd2ctxzf2entryz20zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_8187),
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_entryz00_8188));
		}

	}



/* extend-ctx/entry* */
	BGL_EXPORTED_DEF obj_t
		BGl_extendzd2ctxzf2entryza2z82zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_114, obj_t BgL_entriesz00_115)
	{
		{	/* SawBbv/bbv-types.scm 391 */
			{
				obj_t BgL_entriesz00_2371;
				obj_t BgL_ctxz00_2372;

				BgL_entriesz00_2371 = BgL_entriesz00_115;
				BgL_ctxz00_2372 = ((obj_t) BgL_ctxz00_114);
			BgL_zc3z04anonymousza32082ze3z87_2373:
				if (NULLP(BgL_entriesz00_2371))
					{	/* SawBbv/bbv-types.scm 394 */
						return BgL_ctxz00_2372;
					}
				else
					{	/* SawBbv/bbv-types.scm 396 */
						obj_t BgL_arg2084z00_2375;
						obj_t BgL_arg2086z00_2376;

						BgL_arg2084z00_2375 = CDR(((obj_t) BgL_entriesz00_2371));
						{	/* SawBbv/bbv-types.scm 396 */
							obj_t BgL_arg2087z00_2377;

							BgL_arg2087z00_2377 = CAR(((obj_t) BgL_entriesz00_2371));
							BgL_arg2086z00_2376 =
								BGl_extendzd2ctxzf2entryz20zzsaw_bbvzd2typeszd2(
								((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_2372),
								((BgL_bbvzd2ctxentryzd2_bglt) BgL_arg2087z00_2377));
						}
						{
							obj_t BgL_ctxz00_10851;
							obj_t BgL_entriesz00_10850;

							BgL_entriesz00_10850 = BgL_arg2084z00_2375;
							BgL_ctxz00_10851 = BgL_arg2086z00_2376;
							BgL_ctxz00_2372 = BgL_ctxz00_10851;
							BgL_entriesz00_2371 = BgL_entriesz00_10850;
							goto BgL_zc3z04anonymousza32082ze3z87_2373;
						}
					}
			}
		}

	}



/* &extend-ctx/entry* */
	obj_t BGl_z62extendzd2ctxzf2entryza2ze0zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8189, obj_t BgL_ctxz00_8190, obj_t BgL_entriesz00_8191)
	{
		{	/* SawBbv/bbv-types.scm 391 */
			return
				BGl_extendzd2ctxzf2entryza2z82zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_8190), BgL_entriesz00_8191);
		}

	}



/* _extend-ctx */
	obj_t BGl__extendzd2ctxzd2zzsaw_bbvzd2typeszd2(obj_t BgL_env1784z00_124,
		obj_t BgL_opt1783z00_123)
	{
		{	/* SawBbv/bbv-types.scm 403 */
			{	/* SawBbv/bbv-types.scm 403 */
				obj_t BgL_g1792z00_2383;
				obj_t BgL_g1793z00_2384;
				obj_t BgL_g1794z00_2385;
				obj_t BgL_g1795z00_2386;

				BgL_g1792z00_2383 = VECTOR_REF(BgL_opt1783z00_123, 0L);
				BgL_g1793z00_2384 = VECTOR_REF(BgL_opt1783z00_123, 1L);
				BgL_g1794z00_2385 = VECTOR_REF(BgL_opt1783z00_123, 2L);
				BgL_g1795z00_2386 = VECTOR_REF(BgL_opt1783z00_123, 3L);
				{	/* SawBbv/bbv-types.scm 403 */
					obj_t BgL_aliasesz00_2387;

					BgL_aliasesz00_2387 = BFALSE;
					{	/* SawBbv/bbv-types.scm 403 */
						obj_t BgL_countz00_2388;

						BgL_countz00_2388 = BINT(1L);
						{	/* SawBbv/bbv-types.scm 404 */
							obj_t BgL_valuez00_2389;

							BgL_valuez00_2389 = CNST_TABLE_REF(2);
							{	/* SawBbv/bbv-types.scm 403 */

								{	/* SawBbv/bbv-types.scm 403 */
									long BgL_index1789z00_2390;

									BgL_index1789z00_2390 =
										BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2(VECTOR_LENGTH
										(BgL_opt1783z00_123), BgL_opt1783z00_123, CNST_TABLE_REF(3),
										4L);
									if ((BgL_index1789z00_2390 >= 0L))
										{	/* SawBbv/bbv-types.scm 403 */
											BgL_aliasesz00_2387 =
												VECTOR_REF(BgL_opt1783z00_123, BgL_index1789z00_2390);
										}
									else
										{	/* SawBbv/bbv-types.scm 403 */
											BFALSE;
										}
								}
								{	/* SawBbv/bbv-types.scm 403 */
									long BgL_index1790z00_2392;

									BgL_index1790z00_2392 =
										BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2(VECTOR_LENGTH
										(BgL_opt1783z00_123), BgL_opt1783z00_123, CNST_TABLE_REF(4),
										4L);
									if ((BgL_index1790z00_2392 >= 0L))
										{	/* SawBbv/bbv-types.scm 403 */
											BgL_countz00_2388 =
												VECTOR_REF(BgL_opt1783z00_123, BgL_index1790z00_2392);
										}
									else
										{	/* SawBbv/bbv-types.scm 403 */
											BFALSE;
										}
								}
								{	/* SawBbv/bbv-types.scm 403 */
									long BgL_index1791z00_2394;

									BgL_index1791z00_2394 =
										BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2(VECTOR_LENGTH
										(BgL_opt1783z00_123), BgL_opt1783z00_123, CNST_TABLE_REF(5),
										4L);
									if ((BgL_index1791z00_2394 >= 0L))
										{	/* SawBbv/bbv-types.scm 403 */
											BgL_valuez00_2389 =
												VECTOR_REF(BgL_opt1783z00_123, BgL_index1791z00_2394);
										}
									else
										{	/* SawBbv/bbv-types.scm 403 */
											BFALSE;
										}
								}
								{	/* SawBbv/bbv-types.scm 403 */
									obj_t BgL_arg2091z00_2396;
									obj_t BgL_arg2093z00_2397;
									obj_t BgL_arg2094z00_2398;
									obj_t BgL_arg2095z00_2399;

									BgL_arg2091z00_2396 = VECTOR_REF(BgL_opt1783z00_123, 0L);
									BgL_arg2093z00_2397 = VECTOR_REF(BgL_opt1783z00_123, 1L);
									BgL_arg2094z00_2398 = VECTOR_REF(BgL_opt1783z00_123, 2L);
									BgL_arg2095z00_2399 = VECTOR_REF(BgL_opt1783z00_123, 3L);
									{	/* SawBbv/bbv-types.scm 403 */
										obj_t BgL_aliasesz00_2400;

										BgL_aliasesz00_2400 = BgL_aliasesz00_2387;
										{	/* SawBbv/bbv-types.scm 403 */
											obj_t BgL_countz00_2401;

											BgL_countz00_2401 = BgL_countz00_2388;
											{	/* SawBbv/bbv-types.scm 403 */
												obj_t BgL_valuez00_2402;

												BgL_valuez00_2402 = BgL_valuez00_2389;
												return
													((obj_t)
													BGl_extendzd2ctxzd2zzsaw_bbvzd2typeszd2(
														((BgL_bbvzd2ctxzd2_bglt) BgL_arg2091z00_2396),
														((BgL_rtl_regz00_bglt) BgL_arg2093z00_2397),
														BgL_arg2094z00_2398, CBOOL(BgL_arg2095z00_2399),
														BgL_aliasesz00_2400, BgL_countz00_2401,
														BgL_valuez00_2402));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* search1786~0 */
	long BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2(long BgL_l1785z00_8903,
		obj_t BgL_opt1783z00_8902, obj_t BgL_k1z00_2380, long BgL_iz00_2381)
	{
		{	/* SawBbv/bbv-types.scm 403 */
		BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2:
			if ((BgL_iz00_2381 == BgL_l1785z00_8903))
				{	/* SawBbv/bbv-types.scm 403 */
					return -1L;
				}
			else
				{	/* SawBbv/bbv-types.scm 403 */
					obj_t BgL_vz00_2404;

					BgL_vz00_2404 = VECTOR_REF(BgL_opt1783z00_8902, BgL_iz00_2381);
					if ((BgL_vz00_2404 == BgL_k1z00_2380))
						{	/* SawBbv/bbv-types.scm 403 */
							return (BgL_iz00_2381 + 1L);
						}
					else
						{
							long BgL_iz00_10894;

							BgL_iz00_10894 = (BgL_iz00_2381 + 2L);
							BgL_iz00_2381 = BgL_iz00_10894;
							goto BGl_search1786ze70ze7zzsaw_bbvzd2typeszd2;
						}
				}
		}

	}



/* extend-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_extendzd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_116, BgL_rtl_regz00_bglt BgL_regz00_117, obj_t BgL_typesz00_118,
		bool_t BgL_polarityz00_119, obj_t BgL_aliasesz00_120,
		obj_t BgL_countz00_121, obj_t BgL_valuez00_122)
	{
		{	/* SawBbv/bbv-types.scm 403 */
			{	/* SawBbv/bbv-types.scm 447 */
				bool_t BgL_test4234z00_10896;

				{	/* SawBbv/bbv-types.scm 447 */
					obj_t BgL_classz00_5027;

					BgL_classz00_5027 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
					{	/* SawBbv/bbv-types.scm 447 */
						BgL_objectz00_bglt BgL_arg1807z00_5029;

						{	/* SawBbv/bbv-types.scm 447 */
							obj_t BgL_tmpz00_10897;

							BgL_tmpz00_10897 = ((obj_t) BgL_regz00_117);
							BgL_arg1807z00_5029 = (BgL_objectz00_bglt) (BgL_tmpz00_10897);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 447 */
								long BgL_idxz00_5035;

								BgL_idxz00_5035 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5029);
								BgL_test4234z00_10896 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5035 + 2L)) == BgL_classz00_5027);
							}
						else
							{	/* SawBbv/bbv-types.scm 447 */
								bool_t BgL_res3592z00_5060;

								{	/* SawBbv/bbv-types.scm 447 */
									obj_t BgL_oclassz00_5043;

									{	/* SawBbv/bbv-types.scm 447 */
										obj_t BgL_arg1815z00_5051;
										long BgL_arg1816z00_5052;

										BgL_arg1815z00_5051 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 447 */
											long BgL_arg1817z00_5053;

											BgL_arg1817z00_5053 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5029);
											BgL_arg1816z00_5052 = (BgL_arg1817z00_5053 - OBJECT_TYPE);
										}
										BgL_oclassz00_5043 =
											VECTOR_REF(BgL_arg1815z00_5051, BgL_arg1816z00_5052);
									}
									{	/* SawBbv/bbv-types.scm 447 */
										bool_t BgL__ortest_1115z00_5044;

										BgL__ortest_1115z00_5044 =
											(BgL_classz00_5027 == BgL_oclassz00_5043);
										if (BgL__ortest_1115z00_5044)
											{	/* SawBbv/bbv-types.scm 447 */
												BgL_res3592z00_5060 = BgL__ortest_1115z00_5044;
											}
										else
											{	/* SawBbv/bbv-types.scm 447 */
												long BgL_odepthz00_5045;

												{	/* SawBbv/bbv-types.scm 447 */
													obj_t BgL_arg1804z00_5046;

													BgL_arg1804z00_5046 = (BgL_oclassz00_5043);
													BgL_odepthz00_5045 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5046);
												}
												if ((2L < BgL_odepthz00_5045))
													{	/* SawBbv/bbv-types.scm 447 */
														obj_t BgL_arg1802z00_5048;

														{	/* SawBbv/bbv-types.scm 447 */
															obj_t BgL_arg1803z00_5049;

															BgL_arg1803z00_5049 = (BgL_oclassz00_5043);
															BgL_arg1802z00_5048 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5049,
																2L);
														}
														BgL_res3592z00_5060 =
															(BgL_arg1802z00_5048 == BgL_classz00_5027);
													}
												else
													{	/* SawBbv/bbv-types.scm 447 */
														BgL_res3592z00_5060 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test4234z00_10896 = BgL_res3592z00_5060;
							}
					}
				}
				if (BgL_test4234z00_10896)
					{	/* SawBbv/bbv-types.scm 449 */
						obj_t BgL_vz00_2409;

						{	/* SawBbv/bbv-types.scm 449 */
							bool_t BgL_test4238z00_10919;

							if ((BgL_valuez00_122 == CNST_TABLE_REF(2)))
								{	/* SawBbv/bbv-types.scm 449 */
									if (BgL_polarityz00_119)
										{	/* SawBbv/bbv-types.scm 452 */
											bool_t BgL_test4241z00_10924;

											{	/* SawBbv/bbv-types.scm 452 */
												bool_t BgL_test4242z00_10925;

												{	/* SawBbv/bbv-types.scm 452 */
													obj_t BgL_arg2113z00_2431;

													BgL_arg2113z00_2431 = CAR(BgL_typesz00_118);
													BgL_test4242z00_10925 =
														(BgL_arg2113z00_2431 ==
														BGl_za2bintza2z00zztype_cachez00);
												}
												if (BgL_test4242z00_10925)
													{	/* SawBbv/bbv-types.scm 452 */
														BgL_test4241z00_10924 = ((bool_t) 1);
													}
												else
													{	/* SawBbv/bbv-types.scm 452 */
														obj_t BgL_arg2112z00_2430;

														BgL_arg2112z00_2430 = CAR(BgL_typesz00_118);
														BgL_test4241z00_10924 =
															(BgL_arg2112z00_2430 ==
															BGl_za2longza2z00zztype_cachez00);
													}
											}
											if (BgL_test4241z00_10924)
												{	/* SawBbv/bbv-types.scm 452 */
													BgL_test4238z00_10919 = NULLP(CDR(BgL_typesz00_118));
												}
											else
												{	/* SawBbv/bbv-types.scm 452 */
													BgL_test4238z00_10919 = ((bool_t) 0);
												}
										}
									else
										{	/* SawBbv/bbv-types.scm 449 */
											BgL_test4238z00_10919 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 449 */
									BgL_test4238z00_10919 = ((bool_t) 0);
								}
							if (BgL_test4238z00_10919)
								{	/* SawBbv/bbv-types.scm 449 */
									BgL_vz00_2409 =
										((obj_t) BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2());
								}
							else
								{	/* SawBbv/bbv-types.scm 449 */
									BgL_vz00_2409 = BgL_valuez00_122;
								}
						}
						{	/* SawBbv/bbv-types.scm 456 */
							BgL_bbvzd2ctxzd2_bglt BgL_new1286z00_2411;

							{	/* SawBbv/bbv-types.scm 456 */
								BgL_bbvzd2ctxzd2_bglt BgL_new1289z00_2413;

								BgL_new1289z00_2413 =
									((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_bbvzd2ctxzd2_bgl))));
								{	/* SawBbv/bbv-types.scm 456 */
									long BgL_arg2100z00_2414;

									BgL_arg2100z00_2414 =
										BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1289z00_2413),
										BgL_arg2100z00_2414);
								}
								BgL_new1286z00_2411 = BgL_new1289z00_2413;
							}
							((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1286z00_2411))->
									BgL_idz00) =
								((long) (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_116))->
										BgL_idz00)), BUNSPEC);
							((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1286z00_2411))->
									BgL_entriesz00) =
								((obj_t)
									BGl_extendzd2entriesze70z35zzsaw_bbvzd2typeszd2
									(BgL_aliasesz00_120, BgL_countz00_121, BgL_typesz00_118,
										BgL_ctxz00_116, BgL_regz00_117, BgL_typesz00_118,
										BgL_polarityz00_119, BgL_vz00_2409)), BUNSPEC);
							{	/* SawBbv/bbv-types.scm 457 */
								obj_t BgL_fun2099z00_2412;

								BgL_fun2099z00_2412 =
									BGl_classzd2constructorzd2zz__objectz00
									(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
								BGL_PROCEDURE_CALL1(BgL_fun2099z00_2412,
									((obj_t) BgL_new1286z00_2411));
							}
							return BgL_new1286z00_2411;
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 447 */
						return BgL_ctxz00_116;
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzsaw_bbvzd2typeszd2(int BgL_rnumz00_8895,
		BgL_rtl_regz00_bglt BgL_regz00_8894, obj_t BgL_aliasesz00_8893,
		obj_t BgL_valuez00_8892, bool_t BgL_polarityz00_8891,
		obj_t BgL_typesz00_8890, obj_t BgL_countz00_8889, obj_t BgL_typesz00_8888,
		obj_t BgL_entriesz00_2452)
	{
		{	/* SawBbv/bbv-types.scm 418 */
			if (NULLP(BgL_entriesz00_2452))
				{	/* SawBbv/bbv-types.scm 421 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2455;

					BgL_nz00_2455 =
						BGl_newzd2ctxentryze71z35zzsaw_bbvzd2typeszd2(BgL_aliasesz00_8893,
						BgL_countz00_8889, BgL_typesz00_8888, BgL_regz00_8894,
						BgL_typesz00_8890, BgL_polarityz00_8891, BgL_valuez00_8892);
					{	/* SawBbv/bbv-types.scm 422 */
						obj_t BgL_list2121z00_2456;

						BgL_list2121z00_2456 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_nz00_2455), BNIL);
						return BgL_list2121z00_2456;
					}
				}
			else
				{	/* SawBbv/bbv-types.scm 423 */
					bool_t BgL_test4244z00_10953;

					{	/* SawBbv/bbv-types.scm 423 */
						int BgL_arg2147z00_2494;

						{	/* SawBbv/bbv-types.scm 423 */
							BgL_rtl_regz00_bglt BgL_oz00_5007;

							BgL_oz00_5007 =
								((BgL_rtl_regz00_bglt)
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt)
												CAR(((obj_t) BgL_entriesz00_2452)))))->BgL_regz00));
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_10959;

								{
									obj_t BgL_auxz00_10960;

									{	/* SawBbv/bbv-types.scm 423 */
										BgL_objectz00_bglt BgL_tmpz00_10961;

										BgL_tmpz00_10961 = ((BgL_objectz00_bglt) BgL_oz00_5007);
										BgL_auxz00_10960 = BGL_OBJECT_WIDENING(BgL_tmpz00_10961);
									}
									BgL_auxz00_10959 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_10960);
								}
								BgL_arg2147z00_2494 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_10959))->
									BgL_numz00);
						}}
						BgL_test4244z00_10953 =
							((long) (BgL_arg2147z00_2494) > (long) (BgL_rnumz00_8895));
					}
					if (BgL_test4244z00_10953)
						{	/* SawBbv/bbv-types.scm 424 */
							BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2461;

							BgL_nz00_2461 =
								BGl_newzd2ctxentryze71z35zzsaw_bbvzd2typeszd2
								(BgL_aliasesz00_8893, BgL_countz00_8889, BgL_typesz00_8888,
								BgL_regz00_8894, BgL_typesz00_8890, BgL_polarityz00_8891,
								BgL_valuez00_8892);
							return MAKE_YOUNG_PAIR(((obj_t) BgL_nz00_2461),
								BgL_entriesz00_2452);
						}
					else
						{	/* SawBbv/bbv-types.scm 423 */
							if (
								(((obj_t)
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt)
														CAR(
															((obj_t) BgL_entriesz00_2452)))))->BgL_regz00)) ==
									((obj_t) BgL_regz00_8894)))
								{	/* SawBbv/bbv-types.scm 427 */
									BgL_bbvzd2ctxentryzd2_bglt BgL_i1274z00_2465;

									BgL_i1274z00_2465 =
										((BgL_bbvzd2ctxentryzd2_bglt)
										CAR(((obj_t) BgL_entriesz00_2452)));
									{	/* SawBbv/bbv-types.scm 428 */
										bool_t BgL_test4246z00_10983;

										if (
											(BBOOL(BgL_polarityz00_8891) ==
												BBOOL(
													(((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_i1274z00_2465))->BgL_polarityz00))))
											{	/* SawBbv/bbv-types.scm 428 */
												if (BgL_polarityz00_8891)
													{	/* SawBbv/bbv-types.scm 428 */
														BgL_test4246z00_10983 = ((bool_t) 0);
													}
												else
													{	/* SawBbv/bbv-types.scm 428 */
														BgL_test4246z00_10983 = ((bool_t) 1);
													}
											}
										else
											{	/* SawBbv/bbv-types.scm 428 */
												BgL_test4246z00_10983 = ((bool_t) 0);
											}
										if (BgL_test4246z00_10983)
											{	/* SawBbv/bbv-types.scm 430 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2470;

												{	/* SawBbv/bbv-types.scm 430 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_duplicated1277z00_2472;
													BgL_bbvzd2ctxentryzd2_bglt BgL_new1275z00_2473;

													BgL_duplicated1277z00_2472 =
														((BgL_bbvzd2ctxentryzd2_bglt)
														CAR(((obj_t) BgL_entriesz00_2452)));
													{	/* SawBbv/bbv-types.scm 430 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_new1279z00_2477;

														BgL_new1279z00_2477 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_bbvzd2ctxentryzd2_bgl))));
														{	/* SawBbv/bbv-types.scm 430 */
															long BgL_arg2136z00_2478;

															BgL_arg2136z00_2478 =
																BGL_CLASS_NUM
																(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1279z00_2477), BgL_arg2136z00_2478);
														}
														BgL_new1275z00_2473 = BgL_new1279z00_2477;
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1275z00_2473))->BgL_regz00) =
														((BgL_rtl_regz00_bglt) ((
																	(BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_duplicated1277z00_2472))->
																BgL_regz00)), BUNSPEC);
													{
														obj_t BgL_auxz00_10999;

														{	/* SawBbv/bbv-types.scm 431 */
															obj_t BgL_arg2134z00_2474;

															{	/* SawBbv/bbv-types.scm 431 */
																obj_t BgL_arg2135z00_2475;

																BgL_arg2135z00_2475 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_i1274z00_2465))->BgL_typesz00);
																BgL_arg2134z00_2474 =
																	BGl_appendzd221011zd2zzsaw_bbvzd2typeszd2
																	(BgL_arg2135z00_2475, BgL_typesz00_8890);
															}
															BgL_auxz00_10999 =
																BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
																(BgL_arg2134z00_2474,
																BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00);
														}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1275z00_2473))->BgL_typesz00) =
															((obj_t) BgL_auxz00_10999), BUNSPEC);
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1275z00_2473))->
															BgL_polarityz00) =
														((bool_t) BgL_polarityz00_8891), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1275z00_2473))->BgL_countz00) =
														((long) 0L), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1275z00_2473))->BgL_valuez00) =
														((obj_t) BgL_valuez00_8892), BUNSPEC);
													{
														obj_t BgL_auxz00_11007;

														if (CBOOL(BgL_aliasesz00_8893))
															{	/* SawBbv/bbv-types.scm 435 */
																BgL_auxz00_11007 = BgL_aliasesz00_8893;
															}
														else
															{	/* SawBbv/bbv-types.scm 435 */
																BgL_auxz00_11007 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_i1274z00_2465))->
																	BgL_aliasesz00);
															}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1275z00_2473))->
																BgL_aliasesz00) =
															((obj_t) BgL_auxz00_11007), BUNSPEC);
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1275z00_2473))->BgL_initvalz00) =
														((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_duplicated1277z00_2472))->
																BgL_initvalz00)), BUNSPEC);
													BgL_nz00_2470 = BgL_new1275z00_2473;
												}
												{	/* SawBbv/bbv-types.scm 436 */
													obj_t BgL_arg2133z00_2471;

													BgL_arg2133z00_2471 =
														CDR(((obj_t) BgL_entriesz00_2452));
													return
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_nz00_2470), BgL_arg2133z00_2471);
												}
											}
										else
											{	/* SawBbv/bbv-types.scm 437 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2479;

												{	/* SawBbv/bbv-types.scm 437 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_duplicated1283z00_2481;
													BgL_bbvzd2ctxentryzd2_bglt BgL_new1281z00_2482;

													BgL_duplicated1283z00_2481 =
														((BgL_bbvzd2ctxentryzd2_bglt)
														CAR(((obj_t) BgL_entriesz00_2452)));
													{	/* SawBbv/bbv-types.scm 437 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_new1284z00_2485;

														BgL_new1284z00_2485 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_bbvzd2ctxentryzd2_bgl))));
														{	/* SawBbv/bbv-types.scm 437 */
															long BgL_arg2139z00_2486;

															BgL_arg2139z00_2486 =
																BGL_CLASS_NUM
																(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1284z00_2485), BgL_arg2139z00_2486);
														}
														BgL_new1281z00_2482 = BgL_new1284z00_2485;
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1281z00_2482))->BgL_regz00) =
														((BgL_rtl_regz00_bglt) ((
																	(BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_duplicated1283z00_2481))->
																BgL_regz00)), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1281z00_2482))->BgL_typesz00) =
														((obj_t) BgL_typesz00_8890), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1281z00_2482))->
															BgL_polarityz00) =
														((bool_t) BgL_polarityz00_8891), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1281z00_2482))->BgL_countz00) =
														((long) ((((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_i1274z00_2465))->BgL_countz00) +
																1L)), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1281z00_2482))->BgL_valuez00) =
														((obj_t) BgL_valuez00_8892), BUNSPEC);
													{
														obj_t BgL_auxz00_11033;

														if (CBOOL(BgL_aliasesz00_8893))
															{	/* SawBbv/bbv-types.scm 442 */
																BgL_auxz00_11033 = BgL_aliasesz00_8893;
															}
														else
															{	/* SawBbv/bbv-types.scm 442 */
																BgL_auxz00_11033 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_i1274z00_2465))->
																	BgL_aliasesz00);
															}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1281z00_2482))->
																BgL_aliasesz00) =
															((obj_t) BgL_auxz00_11033), BUNSPEC);
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1281z00_2482))->BgL_initvalz00) =
														((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_duplicated1283z00_2481))->
																BgL_initvalz00)), BUNSPEC);
													BgL_nz00_2479 = BgL_new1281z00_2482;
												}
												{	/* SawBbv/bbv-types.scm 443 */
													obj_t BgL_arg2137z00_2480;

													BgL_arg2137z00_2480 =
														CDR(((obj_t) BgL_entriesz00_2452));
													return
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_nz00_2479), BgL_arg2137z00_2480);
												}
											}
									}
								}
							else
								{	/* SawBbv/bbv-types.scm 445 */
									obj_t BgL_arg2142z00_2489;
									obj_t BgL_arg2143z00_2490;

									BgL_arg2142z00_2489 = CAR(((obj_t) BgL_entriesz00_2452));
									{	/* SawBbv/bbv-types.scm 445 */
										obj_t BgL_arg2144z00_2491;

										BgL_arg2144z00_2491 = CDR(((obj_t) BgL_entriesz00_2452));
										BgL_arg2143z00_2490 =
											BGl_loopze70ze7zzsaw_bbvzd2typeszd2(BgL_rnumz00_8895,
											BgL_regz00_8894, BgL_aliasesz00_8893, BgL_valuez00_8892,
											BgL_polarityz00_8891, BgL_typesz00_8890,
											BgL_countz00_8889, BgL_typesz00_8888,
											BgL_arg2144z00_2491);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg2142z00_2489, BgL_arg2143z00_2490);
								}
						}
				}
		}

	}



/* new-ctxentry~1 */
	BgL_bbvzd2ctxentryzd2_bglt BGl_newzd2ctxentryze71z35zzsaw_bbvzd2typeszd2(obj_t
		BgL_aliasesz00_8898, obj_t BgL_countz00_8897, obj_t BgL_typesz00_8896,
		BgL_rtl_regz00_bglt BgL_regz00_2432, obj_t BgL_typez00_2433,
		bool_t BgL_polarityz00_2434, obj_t BgL_valuez00_2435)
	{
		{	/* SawBbv/bbv-types.scm 413 */
			{	/* SawBbv/bbv-types.scm 407 */
				BgL_bbvzd2ctxentryzd2_bglt BgL_new1271z00_2437;

				{	/* SawBbv/bbv-types.scm 408 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_new1270z00_2440;

					BgL_new1270z00_2440 =
						((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxentryzd2_bgl))));
					{	/* SawBbv/bbv-types.scm 408 */
						long BgL_arg2116z00_2441;

						BgL_arg2116z00_2441 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1270z00_2440), BgL_arg2116z00_2441);
					}
					BgL_new1271z00_2437 = BgL_new1270z00_2440;
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
						BgL_regz00) = ((BgL_rtl_regz00_bglt) BgL_regz00_2432), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
						BgL_typesz00) = ((obj_t) BgL_typesz00_8896), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
						BgL_polarityz00) = ((bool_t) BgL_polarityz00_2434), BUNSPEC);
				{
					long BgL_auxz00_11057;

					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
							(BGl_za2objza2z00zztype_cachez00, BgL_typesz00_8896)))
						{	/* SawBbv/bbv-types.scm 410 */
							BgL_auxz00_11057 = 0L;
						}
					else
						{	/* SawBbv/bbv-types.scm 410 */
							BgL_auxz00_11057 = (long) CINT(BgL_countz00_8897);
						}
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
							BgL_countz00) = ((long) BgL_auxz00_11057), BUNSPEC);
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
						BgL_valuez00) = ((obj_t) BgL_valuez00_2435), BUNSPEC);
				{
					obj_t BgL_auxz00_11064;

					if (CBOOL(BgL_aliasesz00_8898))
						{	/* SawBbv/bbv-types.scm 413 */
							BgL_auxz00_11064 = BgL_aliasesz00_8898;
						}
					else
						{	/* SawBbv/bbv-types.scm 413 */
							BgL_auxz00_11064 = BNIL;
						}
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
							BgL_aliasesz00) = ((obj_t) BgL_auxz00_11064), BUNSPEC);
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1271z00_2437))->
						BgL_initvalz00) = ((obj_t) BUNSPEC), BUNSPEC);
				return BgL_new1271z00_2437;
			}
		}

	}



/* extend-entries~0 */
	obj_t BGl_extendzd2entriesze70z35zzsaw_bbvzd2typeszd2(obj_t
		BgL_aliasesz00_8901, obj_t BgL_countz00_8900, obj_t BgL_typesz00_8899,
		BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2442, BgL_rtl_regz00_bglt BgL_regz00_2443,
		obj_t BgL_typesz00_2444, bool_t BgL_polarityz00_2445,
		obj_t BgL_valuez00_2446)
	{
		{	/* SawBbv/bbv-types.scm 445 */
			{	/* SawBbv/bbv-types.scm 416 */
				int BgL_rnumz00_2448;

				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_11069;

					{
						obj_t BgL_auxz00_11070;

						{	/* SawBbv/bbv-types.scm 416 */
							BgL_objectz00_bglt BgL_tmpz00_11071;

							BgL_tmpz00_11071 =
								((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_regz00_2443));
							BgL_auxz00_11070 = BGL_OBJECT_WIDENING(BgL_tmpz00_11071);
						}
						BgL_auxz00_11069 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_11070);
					}
					BgL_rnumz00_2448 =
						(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_11069))->
						BgL_numz00);
				}
				return
					BGl_loopze70ze7zzsaw_bbvzd2typeszd2(BgL_rnumz00_2448, BgL_regz00_2443,
					BgL_aliasesz00_8901, BgL_valuez00_2446, BgL_polarityz00_2445,
					BgL_typesz00_2444, BgL_countz00_8900, BgL_typesz00_8899,
					(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2442))->BgL_entriesz00));
			}
		}

	}



/* _extend-ctx! */
	obj_t BGl__extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(obj_t BgL_env1797z00_132,
		obj_t BgL_opt1796z00_131)
	{
		{	/* SawBbv/bbv-types.scm 468 */
			{	/* SawBbv/bbv-types.scm 468 */
				obj_t BgL_g1804z00_2504;
				obj_t BgL_g1805z00_2505;
				obj_t BgL_g1806z00_2506;
				obj_t BgL_g1807z00_2507;

				BgL_g1804z00_2504 = VECTOR_REF(BgL_opt1796z00_131, 0L);
				BgL_g1805z00_2505 = VECTOR_REF(BgL_opt1796z00_131, 1L);
				BgL_g1806z00_2506 = VECTOR_REF(BgL_opt1796z00_131, 2L);
				BgL_g1807z00_2507 = VECTOR_REF(BgL_opt1796z00_131, 3L);
				{	/* SawBbv/bbv-types.scm 468 */
					obj_t BgL_aliasesz00_2508;

					BgL_aliasesz00_2508 = BFALSE;
					{	/* SawBbv/bbv-types.scm 469 */
						obj_t BgL_valuez00_2509;

						BgL_valuez00_2509 = CNST_TABLE_REF(2);
						{	/* SawBbv/bbv-types.scm 468 */

							{	/* SawBbv/bbv-types.scm 468 */
								long BgL_index1802z00_2510;

								BgL_index1802z00_2510 =
									BGl_search1799ze70ze7zzsaw_bbvzd2typeszd2(VECTOR_LENGTH
									(BgL_opt1796z00_131), BgL_opt1796z00_131, CNST_TABLE_REF(3),
									4L);
								if ((BgL_index1802z00_2510 >= 0L))
									{	/* SawBbv/bbv-types.scm 468 */
										BgL_aliasesz00_2508 =
											VECTOR_REF(BgL_opt1796z00_131, BgL_index1802z00_2510);
									}
								else
									{	/* SawBbv/bbv-types.scm 468 */
										BFALSE;
									}
							}
							{	/* SawBbv/bbv-types.scm 468 */
								long BgL_index1803z00_2512;

								BgL_index1803z00_2512 =
									BGl_search1799ze70ze7zzsaw_bbvzd2typeszd2(VECTOR_LENGTH
									(BgL_opt1796z00_131), BgL_opt1796z00_131, CNST_TABLE_REF(5),
									4L);
								if ((BgL_index1803z00_2512 >= 0L))
									{	/* SawBbv/bbv-types.scm 468 */
										BgL_valuez00_2509 =
											VECTOR_REF(BgL_opt1796z00_131, BgL_index1803z00_2512);
									}
								else
									{	/* SawBbv/bbv-types.scm 468 */
										BFALSE;
									}
							}
							{	/* SawBbv/bbv-types.scm 468 */
								obj_t BgL_arg2152z00_2514;
								obj_t BgL_arg2154z00_2515;
								obj_t BgL_arg2155z00_2516;
								obj_t BgL_arg2156z00_2517;

								BgL_arg2152z00_2514 = VECTOR_REF(BgL_opt1796z00_131, 0L);
								BgL_arg2154z00_2515 = VECTOR_REF(BgL_opt1796z00_131, 1L);
								BgL_arg2155z00_2516 = VECTOR_REF(BgL_opt1796z00_131, 2L);
								BgL_arg2156z00_2517 = VECTOR_REF(BgL_opt1796z00_131, 3L);
								{	/* SawBbv/bbv-types.scm 468 */
									obj_t BgL_aliasesz00_2518;

									BgL_aliasesz00_2518 = BgL_aliasesz00_2508;
									{	/* SawBbv/bbv-types.scm 468 */
										obj_t BgL_valuez00_2519;

										BgL_valuez00_2519 = BgL_valuez00_2509;
										return
											((obj_t)
											BGl_extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(
												((BgL_bbvzd2ctxzd2_bglt) BgL_arg2152z00_2514),
												((BgL_rtl_regz00_bglt) BgL_arg2154z00_2515),
												BgL_arg2155z00_2516, CBOOL(BgL_arg2156z00_2517),
												BgL_aliasesz00_2518, BgL_valuez00_2519));
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* search1799~0 */
	long BGl_search1799ze70ze7zzsaw_bbvzd2typeszd2(long BgL_l1798z00_8887,
		obj_t BgL_opt1796z00_8886, obj_t BgL_k1z00_2501, long BgL_iz00_2502)
	{
		{	/* SawBbv/bbv-types.scm 468 */
		BGl_search1799ze70ze7zzsaw_bbvzd2typeszd2:
			if ((BgL_iz00_2502 == BgL_l1798z00_8887))
				{	/* SawBbv/bbv-types.scm 468 */
					return -1L;
				}
			else
				{	/* SawBbv/bbv-types.scm 468 */
					obj_t BgL_vz00_2521;

					BgL_vz00_2521 = VECTOR_REF(BgL_opt1796z00_8886, BgL_iz00_2502);
					if ((BgL_vz00_2521 == BgL_k1z00_2501))
						{	/* SawBbv/bbv-types.scm 468 */
							return (BgL_iz00_2502 + 1L);
						}
					else
						{
							long BgL_iz00_11111;

							BgL_iz00_11111 = (BgL_iz00_2502 + 2L);
							BgL_iz00_2502 = BgL_iz00_11111;
							goto BGl_search1799ze70ze7zzsaw_bbvzd2typeszd2;
						}
				}
		}

	}



/* extend-ctx! */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_125, BgL_rtl_regz00_bglt BgL_regz00_126, obj_t BgL_typesz00_127,
		bool_t BgL_polarityz00_128, obj_t BgL_aliasesz00_129,
		obj_t BgL_valuez00_130)
	{
		{	/* SawBbv/bbv-types.scm 468 */
			{
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2553;
				BgL_rtl_regz00_bglt BgL_regz00_2554;
				obj_t BgL_typesz00_2555;
				bool_t BgL_polarityz00_2556;
				obj_t BgL_valuez00_2557;

				{	/* SawBbv/bbv-types.scm 504 */
					obj_t BgL_vz00_2525;

					{	/* SawBbv/bbv-types.scm 504 */
						bool_t BgL_test4257z00_11113;

						if ((BgL_valuez00_130 == CNST_TABLE_REF(2)))
							{	/* SawBbv/bbv-types.scm 504 */
								if (BgL_polarityz00_128)
									{	/* SawBbv/bbv-types.scm 507 */
										bool_t BgL_test4260z00_11118;

										{	/* SawBbv/bbv-types.scm 507 */
											bool_t BgL_test4261z00_11119;

											{	/* SawBbv/bbv-types.scm 507 */
												obj_t BgL_arg2171z00_2543;

												BgL_arg2171z00_2543 = CAR(BgL_typesz00_127);
												BgL_test4261z00_11119 =
													(BgL_arg2171z00_2543 ==
													BGl_za2bintza2z00zztype_cachez00);
											}
											if (BgL_test4261z00_11119)
												{	/* SawBbv/bbv-types.scm 507 */
													BgL_test4260z00_11118 = ((bool_t) 1);
												}
											else
												{	/* SawBbv/bbv-types.scm 507 */
													obj_t BgL_arg2170z00_2542;

													BgL_arg2170z00_2542 = CAR(BgL_typesz00_127);
													BgL_test4260z00_11118 =
														(BgL_arg2170z00_2542 ==
														BGl_za2longza2z00zztype_cachez00);
												}
										}
										if (BgL_test4260z00_11118)
											{	/* SawBbv/bbv-types.scm 507 */
												BgL_test4257z00_11113 = NULLP(CDR(BgL_typesz00_127));
											}
										else
											{	/* SawBbv/bbv-types.scm 507 */
												BgL_test4257z00_11113 = ((bool_t) 0);
											}
									}
								else
									{	/* SawBbv/bbv-types.scm 504 */
										BgL_test4257z00_11113 = ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 504 */
								BgL_test4257z00_11113 = ((bool_t) 0);
							}
						if (BgL_test4257z00_11113)
							{	/* SawBbv/bbv-types.scm 504 */
								BgL_vz00_2525 =
									((obj_t) BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2());
							}
						else
							{	/* SawBbv/bbv-types.scm 504 */
								BgL_vz00_2525 = BgL_valuez00_130;
							}
					}
					{
						obj_t BgL_auxz00_11128;

						BgL_ctxz00_2553 = BgL_ctxz00_125;
						BgL_regz00_2554 = BgL_regz00_126;
						BgL_typesz00_2555 = BgL_typesz00_127;
						BgL_polarityz00_2556 = BgL_polarityz00_128;
						BgL_valuez00_2557 = BgL_vz00_2525;
						{	/* SawBbv/bbv-types.scm 480 */
							int BgL_rnumz00_2559;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_11129;

								{
									obj_t BgL_auxz00_11130;

									{	/* SawBbv/bbv-types.scm 480 */
										BgL_objectz00_bglt BgL_tmpz00_11131;

										BgL_tmpz00_11131 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_regz00_2554));
										BgL_auxz00_11130 = BGL_OBJECT_WIDENING(BgL_tmpz00_11131);
									}
									BgL_auxz00_11129 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_11130);
								}
								BgL_rnumz00_2559 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_11129))->
									BgL_numz00);
							}
							if (NULLP(
									(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2553))->
										BgL_entriesz00)))
								{	/* SawBbv/bbv-types.scm 484 */
									BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2563;

									BgL_nz00_2563 =
										BGl_newzd2ctxentryze70z35zzsaw_bbvzd2typeszd2
										(BgL_aliasesz00_129, BgL_typesz00_127, BgL_regz00_2554,
										BGl_typez00zztype_typez00, BgL_polarityz00_2556,
										BgL_valuez00_2557);
									{	/* SawBbv/bbv-types.scm 485 */
										obj_t BgL_list2177z00_2564;

										BgL_list2177z00_2564 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_nz00_2563), BNIL);
										BgL_auxz00_11128 = BgL_list2177z00_2564;
									}
								}
							else
								{	/* SawBbv/bbv-types.scm 486 */
									bool_t BgL_test4263z00_11143;

									{	/* SawBbv/bbv-types.scm 486 */
										int BgL_arg2200z00_2594;

										{	/* SawBbv/bbv-types.scm 486 */
											BgL_rtl_regz00_bglt BgL_oz00_5082;

											{
												BgL_rtl_regz00_bglt BgL_auxz00_11144;

												{
													BgL_bbvzd2ctxentryzd2_bglt BgL_auxz00_11145;

													{	/* SawBbv/bbv-types.scm 486 */
														obj_t BgL_pairz00_5080;

														BgL_pairz00_5080 =
															(((BgL_bbvzd2ctxzd2_bglt)
																COBJECT(BgL_ctxz00_2553))->BgL_entriesz00);
														BgL_auxz00_11145 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															CAR(BgL_pairz00_5080));
													}
													BgL_auxz00_11144 =
														(((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_auxz00_11145))->BgL_regz00);
												}
												BgL_oz00_5082 =
													((BgL_rtl_regz00_bglt) BgL_auxz00_11144);
											}
											{
												BgL_rtl_regzf2razf2_bglt BgL_auxz00_11151;

												{
													obj_t BgL_auxz00_11152;

													{	/* SawBbv/bbv-types.scm 486 */
														BgL_objectz00_bglt BgL_tmpz00_11153;

														BgL_tmpz00_11153 =
															((BgL_objectz00_bglt) BgL_oz00_5082);
														BgL_auxz00_11152 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_11153);
													}
													BgL_auxz00_11151 =
														((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_11152);
												}
												BgL_arg2200z00_2594 =
													(((BgL_rtl_regzf2razf2_bglt)
														COBJECT(BgL_auxz00_11151))->BgL_numz00);
										}}
										BgL_test4263z00_11143 =
											(
											(long) (BgL_arg2200z00_2594) > (long) (BgL_rnumz00_2559));
									}
									if (BgL_test4263z00_11143)
										{	/* SawBbv/bbv-types.scm 487 */
											BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2570;

											BgL_nz00_2570 =
												BGl_newzd2ctxentryze70z35zzsaw_bbvzd2typeszd2
												(BgL_aliasesz00_129, BgL_typesz00_127, BgL_regz00_2554,
												BGl_typez00zztype_typez00, BgL_polarityz00_2556,
												BgL_valuez00_2557);
											{	/* SawBbv/bbv-types.scm 488 */
												obj_t BgL_arg2183z00_2571;

												BgL_arg2183z00_2571 =
													(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2553))->
													BgL_entriesz00);
												BgL_auxz00_11128 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_nz00_2570),
													BgL_arg2183z00_2571);
											}
										}
									else
										{	/* SawBbv/bbv-types.scm 490 */
											obj_t BgL_g1295z00_2572;

											BgL_g1295z00_2572 =
												CDR(
												(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2553))->
													BgL_entriesz00));
											{
												obj_t BgL_curz00_2575;
												obj_t BgL_prevz00_2576;

												BgL_curz00_2575 = BgL_g1295z00_2572;
												BgL_prevz00_2576 =
													(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2553))->
													BgL_entriesz00);
											BgL_zc3z04anonymousza32185ze3z87_2577:
												if (NULLP(BgL_curz00_2575))
													{	/* SawBbv/bbv-types.scm 494 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2579;

														BgL_nz00_2579 =
															BGl_newzd2ctxentryze70z35zzsaw_bbvzd2typeszd2
															(BgL_aliasesz00_129, BgL_typesz00_127,
															BgL_regz00_2554, BGl_typez00zztype_typez00,
															BgL_polarityz00_2556, BgL_valuez00_2557);
														{	/* SawBbv/bbv-types.scm 495 */
															obj_t BgL_arg2187z00_2580;

															{	/* SawBbv/bbv-types.scm 495 */
																obj_t BgL_list2188z00_2581;

																BgL_list2188z00_2581 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_nz00_2579), BNIL);
																BgL_arg2187z00_2580 = BgL_list2188z00_2581;
															}
															{	/* SawBbv/bbv-types.scm 495 */
																obj_t BgL_tmpz00_11172;

																BgL_tmpz00_11172 = ((obj_t) BgL_prevz00_2576);
																SET_CDR(BgL_tmpz00_11172, BgL_arg2187z00_2580);
															}
														}
														BgL_auxz00_11128 =
															(((BgL_bbvzd2ctxzd2_bglt)
																COBJECT(BgL_ctxz00_2553))->BgL_entriesz00);
													}
												else
													{	/* SawBbv/bbv-types.scm 497 */
														bool_t BgL_test4265z00_11176;

														{	/* SawBbv/bbv-types.scm 497 */
															int BgL_arg2196z00_2589;

															{	/* SawBbv/bbv-types.scm 497 */
																BgL_rtl_regz00_bglt BgL_oz00_5091;

																BgL_oz00_5091 =
																	((BgL_rtl_regz00_bglt)
																	(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																				((BgL_bbvzd2ctxentryzd2_bglt)
																					CAR(
																						((obj_t) BgL_curz00_2575)))))->
																		BgL_regz00));
																{
																	BgL_rtl_regzf2razf2_bglt BgL_auxz00_11182;

																	{
																		obj_t BgL_auxz00_11183;

																		{	/* SawBbv/bbv-types.scm 497 */
																			BgL_objectz00_bglt BgL_tmpz00_11184;

																			BgL_tmpz00_11184 =
																				((BgL_objectz00_bglt) BgL_oz00_5091);
																			BgL_auxz00_11183 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_11184);
																		}
																		BgL_auxz00_11182 =
																			((BgL_rtl_regzf2razf2_bglt)
																			BgL_auxz00_11183);
																	}
																	BgL_arg2196z00_2589 =
																		(((BgL_rtl_regzf2razf2_bglt)
																			COBJECT(BgL_auxz00_11182))->BgL_numz00);
															}}
															BgL_test4265z00_11176 =
																(
																(long) (BgL_arg2196z00_2589) >
																(long) (BgL_rnumz00_2559));
														}
														if (BgL_test4265z00_11176)
															{	/* SawBbv/bbv-types.scm 498 */
																BgL_bbvzd2ctxentryzd2_bglt BgL_nz00_2586;

																BgL_nz00_2586 =
																	BGl_newzd2ctxentryze70z35zzsaw_bbvzd2typeszd2
																	(BgL_aliasesz00_129, BgL_typesz00_127,
																	BgL_regz00_2554, BGl_typez00zztype_typez00,
																	BgL_polarityz00_2556, BgL_valuez00_2557);
																{	/* SawBbv/bbv-types.scm 499 */
																	obj_t BgL_arg2193z00_2587;

																	BgL_arg2193z00_2587 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_nz00_2586), BgL_curz00_2575);
																	{	/* SawBbv/bbv-types.scm 499 */
																		obj_t BgL_tmpz00_11195;

																		BgL_tmpz00_11195 =
																			((obj_t) BgL_prevz00_2576);
																		SET_CDR(BgL_tmpz00_11195,
																			BgL_arg2193z00_2587);
																	}
																}
																BgL_auxz00_11128 =
																	(((BgL_bbvzd2ctxzd2_bglt)
																		COBJECT(BgL_ctxz00_2553))->BgL_entriesz00);
															}
														else
															{	/* SawBbv/bbv-types.scm 502 */
																obj_t BgL_arg2194z00_2588;

																BgL_arg2194z00_2588 =
																	CDR(((obj_t) BgL_curz00_2575));
																{
																	obj_t BgL_prevz00_11202;
																	obj_t BgL_curz00_11201;

																	BgL_curz00_11201 = BgL_arg2194z00_2588;
																	BgL_prevz00_11202 = BgL_curz00_2575;
																	BgL_prevz00_2576 = BgL_prevz00_11202;
																	BgL_curz00_2575 = BgL_curz00_11201;
																	goto BgL_zc3z04anonymousza32185ze3z87_2577;
																}
															}
													}
											}
										}
								}
						}
						((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_125))->
								BgL_entriesz00) = ((obj_t) BgL_auxz00_11128), BUNSPEC);
					}
					return BgL_ctxz00_125;
				}
			}
		}

	}



/* new-ctxentry~0 */
	BgL_bbvzd2ctxentryzd2_bglt BGl_newzd2ctxentryze70z35zzsaw_bbvzd2typeszd2(obj_t
		BgL_aliasesz00_8885, obj_t BgL_typesz00_8884,
		BgL_rtl_regz00_bglt BgL_regz00_2544, obj_t BgL_typez00_2545,
		bool_t BgL_polarityz00_2546, obj_t BgL_valuez00_2547)
	{
		{	/* SawBbv/bbv-types.scm 477 */
			{	/* SawBbv/bbv-types.scm 472 */
				BgL_bbvzd2ctxentryzd2_bglt BgL_new1292z00_2549;

				{	/* SawBbv/bbv-types.scm 473 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_new1291z00_2551;

					BgL_new1291z00_2551 =
						((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxentryzd2_bgl))));
					{	/* SawBbv/bbv-types.scm 473 */
						long BgL_arg2173z00_2552;

						BgL_arg2173z00_2552 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1291z00_2551), BgL_arg2173z00_2552);
					}
					BgL_new1292z00_2549 = BgL_new1291z00_2551;
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
						BgL_regz00) = ((BgL_rtl_regz00_bglt) BgL_regz00_2544), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
						BgL_typesz00) = ((obj_t) BgL_typesz00_8884), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
						BgL_polarityz00) = ((bool_t) BgL_polarityz00_2546), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
						BgL_countz00) = ((long) 0L), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
						BgL_valuez00) = ((obj_t) BgL_valuez00_2547), BUNSPEC);
				{
					obj_t BgL_auxz00_11214;

					if (CBOOL(BgL_aliasesz00_8885))
						{	/* SawBbv/bbv-types.scm 477 */
							BgL_auxz00_11214 = BgL_aliasesz00_8885;
						}
					else
						{	/* SawBbv/bbv-types.scm 477 */
							BgL_auxz00_11214 = BNIL;
						}
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
							BgL_aliasesz00) = ((obj_t) BgL_auxz00_11214), BUNSPEC);
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1292z00_2549))->
						BgL_initvalz00) = ((obj_t) BUNSPEC), BUNSPEC);
				return BgL_new1292z00_2549;
			}
		}

	}



/* _extend-ctx* */
	obj_t BGl__extendzd2ctxza2z70zzsaw_bbvzd2typeszd2(obj_t BgL_env1809z00_139,
		obj_t BgL_opt1808z00_138)
	{
		{	/* SawBbv/bbv-types.scm 520 */
			{
				obj_t BgL_k1z00_2602;
				long BgL_iz00_2603;

				{	/* SawBbv/bbv-types.scm 520 */
					obj_t BgL_ctxz00_2605;
					obj_t BgL_regsz00_2606;
					obj_t BgL_g1815z00_2607;
					obj_t BgL_g1816z00_2608;

					BgL_ctxz00_2605 = VECTOR_REF(BgL_opt1808z00_138, 0L);
					BgL_regsz00_2606 = VECTOR_REF(BgL_opt1808z00_138, 1L);
					BgL_g1815z00_2607 = VECTOR_REF(BgL_opt1808z00_138, 2L);
					BgL_g1816z00_2608 = VECTOR_REF(BgL_opt1808z00_138, 3L);
					{	/* SawBbv/bbv-types.scm 521 */
						obj_t BgL_valuez00_2609;

						BgL_valuez00_2609 = CNST_TABLE_REF(2);
						{	/* SawBbv/bbv-types.scm 520 */

							{	/* SawBbv/bbv-types.scm 520 */
								long BgL_index1814z00_2610;

								BgL_k1z00_2602 = CNST_TABLE_REF(5);
								BgL_iz00_2603 = 4L;
							BgL_search1811z00_2604:
								if ((BgL_iz00_2603 == VECTOR_LENGTH(BgL_opt1808z00_138)))
									{	/* SawBbv/bbv-types.scm 520 */
										BgL_index1814z00_2610 = -1L;
									}
								else
									{	/* SawBbv/bbv-types.scm 520 */
										obj_t BgL_vz00_2618;

										BgL_vz00_2618 =
											VECTOR_REF(BgL_opt1808z00_138, BgL_iz00_2603);
										if ((BgL_vz00_2618 == BgL_k1z00_2602))
											{	/* SawBbv/bbv-types.scm 520 */
												BgL_index1814z00_2610 = (BgL_iz00_2603 + 1L);
											}
										else
											{
												long BgL_iz00_11231;

												BgL_iz00_11231 = (BgL_iz00_2603 + 2L);
												BgL_iz00_2603 = BgL_iz00_11231;
												goto BgL_search1811z00_2604;
											}
									}
								if ((BgL_index1814z00_2610 >= 0L))
									{	/* SawBbv/bbv-types.scm 520 */
										BgL_valuez00_2609 =
											VECTOR_REF(BgL_opt1808z00_138, BgL_index1814z00_2610);
									}
								else
									{	/* SawBbv/bbv-types.scm 520 */
										BFALSE;
									}
							}
							{	/* SawBbv/bbv-types.scm 520 */
								obj_t BgL_arg2206z00_2612;
								obj_t BgL_arg2207z00_2613;
								obj_t BgL_arg2208z00_2614;
								obj_t BgL_arg2209z00_2615;

								BgL_arg2206z00_2612 = VECTOR_REF(BgL_opt1808z00_138, 0L);
								BgL_arg2207z00_2613 = VECTOR_REF(BgL_opt1808z00_138, 1L);
								BgL_arg2208z00_2614 = VECTOR_REF(BgL_opt1808z00_138, 2L);
								BgL_arg2209z00_2615 = VECTOR_REF(BgL_opt1808z00_138, 3L);
								{	/* SawBbv/bbv-types.scm 520 */
									obj_t BgL_valuez00_2616;

									BgL_valuez00_2616 = BgL_valuez00_2609;
									return
										BGl_extendzd2ctxza2z70zzsaw_bbvzd2typeszd2(
										((BgL_bbvzd2ctxzd2_bglt) BgL_arg2206z00_2612),
										BgL_arg2207z00_2613, BgL_arg2208z00_2614,
										CBOOL(BgL_arg2209z00_2615), BgL_valuez00_2616);
								}
							}
						}
					}
				}
			}
		}

	}



/* extend-ctx* */
	BGL_EXPORTED_DEF obj_t
		BGl_extendzd2ctxza2z70zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_133, obj_t BgL_regsz00_134, obj_t BgL_typesz00_135,
		bool_t BgL_polarityz00_136, obj_t BgL_valuez00_137)
	{
		{	/* SawBbv/bbv-types.scm 520 */
			{
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2621;
				obj_t BgL_regsz00_2622;

				{
					BgL_bbvzd2ctxzd2_bglt BgL_auxz00_11244;

					BgL_ctxz00_2621 = BgL_ctxz00_133;
					BgL_regsz00_2622 = BgL_regsz00_134;
				BgL_zc3z04anonymousza32212ze3z87_2623:
					if (NULLP(BgL_regsz00_2622))
						{	/* SawBbv/bbv-types.scm 524 */
							BgL_auxz00_11244 = BgL_ctxz00_2621;
						}
					else
						{	/* SawBbv/bbv-types.scm 526 */
							BgL_bbvzd2ctxzd2_bglt BgL_arg2214z00_2625;
							obj_t BgL_arg2215z00_2626;

							{	/* SawBbv/bbv-types.scm 526 */
								obj_t BgL_arg2216z00_2627;

								BgL_arg2216z00_2627 = CAR(((obj_t) BgL_regsz00_2622));
								BgL_arg2214z00_2625 =
									BGl_extendzd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_ctxz00_2621,
									((BgL_rtl_regz00_bglt) BgL_arg2216z00_2627), BgL_typesz00_135,
									BgL_polarityz00_136, BFALSE, BINT(1L), BgL_valuez00_137);
							}
							BgL_arg2215z00_2626 = CDR(((obj_t) BgL_regsz00_2622));
							{
								obj_t BgL_regsz00_11255;
								BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_11254;

								BgL_ctxz00_11254 = BgL_arg2214z00_2625;
								BgL_regsz00_11255 = BgL_arg2215z00_2626;
								BgL_regsz00_2622 = BgL_regsz00_11255;
								BgL_ctxz00_2621 = BgL_ctxz00_11254;
								goto BgL_zc3z04anonymousza32212ze3z87_2623;
							}
						}
					return ((obj_t) BgL_auxz00_11244);
				}
			}
		}

	}



/* alias-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_aliaszd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_140,
		BgL_rtl_regz00_bglt BgL_regz00_141, BgL_rtl_regz00_bglt BgL_aliasz00_142)
	{
		{	/* SawBbv/bbv-types.scm 534 */
			if (CBOOL(BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2))
				{	/* SawBbv/bbv-types.scm 535 */
					if ((((obj_t) BgL_regz00_141) == ((obj_t) BgL_aliasz00_142)))
						{	/* SawBbv/bbv-types.scm 536 */
							return BgL_ctxz00_140;
						}
					else
						{	/* SawBbv/bbv-types.scm 538 */
							obj_t BgL_rez00_2632;
							obj_t BgL_aez00_2633;

							BgL_rez00_2632 =
								BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_ctxz00_140,
								BgL_regz00_141);
							BgL_aez00_2633 =
								BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_ctxz00_140,
								BgL_aliasz00_142);
							{	/* SawBbv/bbv-types.scm 542 */
								obj_t BgL_allz00_2636;

								{	/* SawBbv/bbv-types.scm 543 */
									obj_t BgL_arg2236z00_2672;

									{	/* SawBbv/bbv-types.scm 543 */
										obj_t BgL_arg2237z00_2673;

										{	/* SawBbv/bbv-types.scm 543 */
											obj_t BgL_arg2238z00_2674;
											obj_t BgL_arg2239z00_2675;

											BgL_arg2238z00_2674 =
												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_rez00_2632)))->
												BgL_aliasesz00);
											BgL_arg2239z00_2675 =
												(((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_aez00_2633)))->BgL_aliasesz00);
											BgL_arg2237z00_2673 =
												BGl_appendzd221011zd2zzsaw_bbvzd2typeszd2
												(BgL_arg2238z00_2674, BgL_arg2239z00_2675);
										}
										BgL_arg2236z00_2672 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_aliasz00_142), BgL_arg2237z00_2673);
									}
									BgL_allz00_2636 =
										BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
										(BgL_arg2236z00_2672,
										BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00);
								}
								{	/* SawBbv/bbv-types.scm 545 */
									BgL_bbvzd2ctxentryzd2_bglt BgL_nrez00_2637;

									{	/* SawBbv/bbv-types.scm 545 */
										BgL_bbvzd2ctxentryzd2_bglt BgL_new1300z00_2669;

										{	/* SawBbv/bbv-types.scm 545 */
											BgL_bbvzd2ctxentryzd2_bglt BgL_new1303z00_2670;

											BgL_new1303z00_2670 =
												((BgL_bbvzd2ctxentryzd2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_bbvzd2ctxentryzd2_bgl))));
											{	/* SawBbv/bbv-types.scm 545 */
												long BgL_arg2235z00_2671;

												BgL_arg2235z00_2671 =
													BGL_CLASS_NUM
													(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1303z00_2670), BgL_arg2235z00_2671);
											}
											BgL_new1300z00_2669 = BgL_new1303z00_2670;
										}
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_regz00) =
											((BgL_rtl_regz00_bglt) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_rez00_2632)))->BgL_regz00)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_typesz00) =
											((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_rez00_2632)))->BgL_typesz00)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_polarityz00) =
											((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_rez00_2632)))->BgL_polarityz00)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_countz00) =
											((long) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_rez00_2632)))->BgL_countz00)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_valuez00) =
											((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_rez00_2632)))->BgL_valuez00)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_aliasesz00) =
											((obj_t) bgl_remq(((obj_t) BgL_regz00_141),
													BgL_allz00_2636)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1300z00_2669))->BgL_initvalz00) =
											((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_rez00_2632)))->BgL_initvalz00)), BUNSPEC);
										BgL_nrez00_2637 = BgL_new1300z00_2669;
									}
									{	/* SawBbv/bbv-types.scm 547 */
										obj_t BgL_g1304z00_2638;

										BgL_g1304z00_2638 =
											BGl_extendzd2ctxzf2entryz20zzsaw_bbvzd2typeszd2
											(BgL_ctxz00_140, BgL_nrez00_2637);
										{
											obj_t BgL_allz00_2640;
											obj_t BgL_ctxz00_2641;

											{
												obj_t BgL_auxz00_11299;

												BgL_allz00_2640 = BgL_allz00_2636;
												BgL_ctxz00_2641 = BgL_g1304z00_2638;
											BgL_zc3z04anonymousza32217ze3z87_2642:
												if (NULLP(BgL_allz00_2640))
													{	/* SawBbv/bbv-types.scm 549 */
														BgL_auxz00_11299 = BgL_ctxz00_2641;
													}
												else
													{	/* SawBbv/bbv-types.scm 551 */
														obj_t BgL_aez00_2644;

														BgL_aez00_2644 =
															BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(
															((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_2641),
															((BgL_rtl_regz00_bglt)
																CAR(((obj_t) BgL_allz00_2640))));
														if (CBOOL(BgL_aez00_2644))
															{	/* SawBbv/bbv-types.scm 554 */
																bool_t BgL_test4275z00_11309;

																if (
																	(((obj_t) BgL_regz00_141) ==
																		((obj_t)
																			(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																						((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_aez00_2644)))->BgL_regz00))))
																	{	/* SawBbv/bbv-types.scm 554 */
																		BgL_test4275z00_11309 = ((bool_t) 1);
																	}
																else
																	{	/* SawBbv/bbv-types.scm 554 */
																		BgL_test4275z00_11309 =
																			CBOOL
																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
																				((obj_t) BgL_regz00_141),
																				(((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT((
																								(BgL_bbvzd2ctxentryzd2_bglt)
																								BgL_aez00_2644)))->
																					BgL_aliasesz00)));
																	}
																if (BgL_test4275z00_11309)
																	{	/* SawBbv/bbv-types.scm 555 */
																		obj_t BgL_arg2224z00_2651;

																		BgL_arg2224z00_2651 =
																			CDR(((obj_t) BgL_allz00_2640));
																		{
																			obj_t BgL_allz00_11323;

																			BgL_allz00_11323 = BgL_arg2224z00_2651;
																			BgL_allz00_2640 = BgL_allz00_11323;
																			goto
																				BgL_zc3z04anonymousza32217ze3z87_2642;
																		}
																	}
																else
																	{	/* SawBbv/bbv-types.scm 556 */
																		BgL_bbvzd2ctxentryzd2_bglt BgL_naez00_2652;

																		{	/* SawBbv/bbv-types.scm 556 */
																			BgL_bbvzd2ctxentryzd2_bglt
																				BgL_new1307z00_2656;
																			{	/* SawBbv/bbv-types.scm 556 */
																				BgL_bbvzd2ctxentryzd2_bglt
																					BgL_new1310z00_2660;
																				BgL_new1310z00_2660 =
																					((BgL_bbvzd2ctxentryzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_bbvzd2ctxentryzd2_bgl))));
																				{	/* SawBbv/bbv-types.scm 556 */
																					long BgL_arg2230z00_2661;

																					BgL_arg2230z00_2661 =
																						BGL_CLASS_NUM
																						(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1310z00_2660),
																						BgL_arg2230z00_2661);
																				}
																				BgL_new1307z00_2656 =
																					BgL_new1310z00_2660;
																			}
																			((((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT(BgL_new1307z00_2656))->
																					BgL_regz00) =
																				((BgL_rtl_regz00_bglt) ((
																							(BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT((
																									(BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_aez00_2644)))->
																						BgL_regz00)), BUNSPEC);
																			((((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT(BgL_new1307z00_2656))->
																					BgL_typesz00) =
																				((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT((
																									(BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_aez00_2644)))->
																						BgL_typesz00)), BUNSPEC);
																			((((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT(BgL_new1307z00_2656))->
																					BgL_polarityz00) =
																				((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT((
																									(BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_aez00_2644)))->
																						BgL_polarityz00)), BUNSPEC);
																			((((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT(BgL_new1307z00_2656))->
																					BgL_countz00) =
																				((long) (((BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT((
																									(BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_aez00_2644)))->
																						BgL_countz00)), BUNSPEC);
																			((((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT(BgL_new1307z00_2656))->
																					BgL_valuez00) =
																				((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT((
																									(BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_aez00_2644)))->
																						BgL_valuez00)), BUNSPEC);
																			{
																				obj_t BgL_auxz00_11343;

																				{	/* SawBbv/bbv-types.scm 559 */
																					obj_t BgL_arg2227z00_2657;

																					{	/* SawBbv/bbv-types.scm 559 */
																						obj_t BgL_arg2228z00_2658;

																						{	/* SawBbv/bbv-types.scm 559 */
																							obj_t BgL_arg2229z00_2659;

																							BgL_arg2229z00_2659 =
																								(((BgL_bbvzd2ctxentryzd2_bglt)
																									COBJECT((
																											(BgL_bbvzd2ctxentryzd2_bglt)
																											BgL_aez00_2644)))->
																								BgL_aliasesz00);
																							BgL_arg2228z00_2658 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_regz00_141),
																								BgL_arg2229z00_2659);
																						}
																						BgL_arg2227z00_2657 =
																							BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_arg2228z00_2658,
																							BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00);
																					}
																					BgL_auxz00_11343 =
																						bgl_remq(
																						((obj_t) BgL_aliasz00_142),
																						BgL_arg2227z00_2657);
																				}
																				((((BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT(BgL_new1307z00_2656))->
																						BgL_aliasesz00) =
																					((obj_t) BgL_auxz00_11343), BUNSPEC);
																			}
																			((((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT(BgL_new1307z00_2656))->
																					BgL_initvalz00) =
																				((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																							COBJECT((
																									(BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_aez00_2644)))->
																						BgL_initvalz00)), BUNSPEC);
																			BgL_naez00_2652 = BgL_new1307z00_2656;
																		}
																		{	/* SawBbv/bbv-types.scm 561 */
																			obj_t BgL_arg2225z00_2653;
																			obj_t BgL_arg2226z00_2654;

																			BgL_arg2225z00_2653 =
																				CDR(((obj_t) BgL_allz00_2640));
																			BgL_arg2226z00_2654 =
																				BGl_extendzd2ctxzf2entryz20zzsaw_bbvzd2typeszd2
																				(((BgL_bbvzd2ctxzd2_bglt)
																					BgL_ctxz00_2641), BgL_naez00_2652);
																			{
																				obj_t BgL_ctxz00_11360;
																				obj_t BgL_allz00_11359;

																				BgL_allz00_11359 = BgL_arg2225z00_2653;
																				BgL_ctxz00_11360 = BgL_arg2226z00_2654;
																				BgL_ctxz00_2641 = BgL_ctxz00_11360;
																				BgL_allz00_2640 = BgL_allz00_11359;
																				goto
																					BgL_zc3z04anonymousza32217ze3z87_2642;
																			}
																		}
																	}
															}
														else
															{	/* SawBbv/bbv-types.scm 563 */
																obj_t BgL_arg2233z00_2665;

																BgL_arg2233z00_2665 =
																	CDR(((obj_t) BgL_allz00_2640));
																{
																	obj_t BgL_allz00_11363;

																	BgL_allz00_11363 = BgL_arg2233z00_2665;
																	BgL_allz00_2640 = BgL_allz00_11363;
																	goto BgL_zc3z04anonymousza32217ze3z87_2642;
																}
															}
													}
												return ((BgL_bbvzd2ctxzd2_bglt) BgL_auxz00_11299);
											}
										}
									}
								}
							}
						}
				}
			else
				{	/* SawBbv/bbv-types.scm 535 */
					return BgL_ctxz00_140;
				}
		}

	}



/* &alias-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62aliaszd2ctxzb0zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8192, obj_t BgL_ctxz00_8193, obj_t BgL_regz00_8194,
		obj_t BgL_aliasz00_8195)
	{
		{	/* SawBbv/bbv-types.scm 534 */
			return
				BGl_aliaszd2ctxzd2zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_8193),
				((BgL_rtl_regz00_bglt) BgL_regz00_8194),
				((BgL_rtl_regz00_bglt) BgL_aliasz00_8195));
		}

	}



/* unalias-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_unaliaszd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_143, BgL_rtl_regz00_bglt BgL_regz00_144)
	{
		{	/* SawBbv/bbv-types.scm 571 */
			if (CBOOL(BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2))
				{	/* SawBbv/bbv-types.scm 574 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1314z00_2677;

					{	/* SawBbv/bbv-types.scm 574 */
						BgL_bbvzd2ctxzd2_bglt BgL_new1313z00_2705;

						BgL_new1313z00_2705 =
							((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2ctxzd2_bgl))));
						{	/* SawBbv/bbv-types.scm 574 */
							long BgL_arg2252z00_2706;

							BgL_arg2252z00_2706 =
								BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1313z00_2705),
								BgL_arg2252z00_2706);
						}
						BgL_new1314z00_2677 = BgL_new1313z00_2705;
					}
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1314z00_2677))->BgL_idz00) =
						((long) -1L), BUNSPEC);
					{
						obj_t BgL_auxz00_11376;

						{	/* SawBbv/bbv-types.scm 575 */
							obj_t BgL_l1671z00_2678;

							BgL_l1671z00_2678 =
								(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_143))->
								BgL_entriesz00);
							if (NULLP(BgL_l1671z00_2678))
								{	/* SawBbv/bbv-types.scm 575 */
									BgL_auxz00_11376 = BNIL;
								}
							else
								{	/* SawBbv/bbv-types.scm 575 */
									obj_t BgL_head1673z00_2680;

									BgL_head1673z00_2680 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1671z00_2682;
										obj_t BgL_tail1674z00_2683;

										BgL_l1671z00_2682 = BgL_l1671z00_2678;
										BgL_tail1674z00_2683 = BgL_head1673z00_2680;
									BgL_zc3z04anonymousza32241ze3z87_2684:
										if (NULLP(BgL_l1671z00_2682))
											{	/* SawBbv/bbv-types.scm 575 */
												BgL_auxz00_11376 = CDR(BgL_head1673z00_2680);
											}
										else
											{	/* SawBbv/bbv-types.scm 575 */
												obj_t BgL_newtail1675z00_2686;

												{	/* SawBbv/bbv-types.scm 575 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_arg2244z00_2688;

													{	/* SawBbv/bbv-types.scm 575 */
														obj_t BgL_ez00_2689;

														BgL_ez00_2689 = CAR(((obj_t) BgL_l1671z00_2682));
														if (
															(((obj_t)
																	(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																				((BgL_bbvzd2ctxentryzd2_bglt)
																					BgL_ez00_2689)))->BgL_regz00)) ==
																((obj_t) BgL_regz00_144)))
															{	/* SawBbv/bbv-types.scm 578 */
																BgL_bbvzd2ctxentryzd2_bglt BgL_new1316z00_2694;

																{	/* SawBbv/bbv-types.scm 579 */
																	BgL_bbvzd2ctxentryzd2_bglt
																		BgL_new1319z00_2695;
																	BgL_new1319z00_2695 =
																		((BgL_bbvzd2ctxentryzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_bbvzd2ctxentryzd2_bgl))));
																	{	/* SawBbv/bbv-types.scm 579 */
																		long BgL_arg2247z00_2696;

																		{	/* SawBbv/bbv-types.scm 579 */
																			obj_t BgL_classz00_5123;

																			BgL_classz00_5123 =
																				BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
																			BgL_arg2247z00_2696 =
																				BGL_CLASS_NUM(BgL_classz00_5123);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1319z00_2695),
																			BgL_arg2247z00_2696);
																	}
																	BgL_new1316z00_2694 = BgL_new1319z00_2695;
																}
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_regz00) =
																	((BgL_rtl_regz00_bglt) ((
																				(BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_regz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_typesz00) =
																	((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_typesz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_polarityz00) =
																	((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_polarityz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_countz00) =
																	((long) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_countz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_valuez00) =
																	((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_valuez00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_aliasesz00) = ((obj_t) BNIL), BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1316z00_2694))->
																		BgL_initvalz00) =
																	((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_initvalz00)),
																	BUNSPEC);
																BgL_arg2244z00_2688 = BgL_new1316z00_2694;
															}
														else
															{	/* SawBbv/bbv-types.scm 580 */
																BgL_bbvzd2ctxentryzd2_bglt BgL_new1320z00_2698;

																{	/* SawBbv/bbv-types.scm 581 */
																	BgL_bbvzd2ctxentryzd2_bglt
																		BgL_new1323z00_2700;
																	BgL_new1323z00_2700 =
																		((BgL_bbvzd2ctxentryzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_bbvzd2ctxentryzd2_bgl))));
																	{	/* SawBbv/bbv-types.scm 581 */
																		long BgL_arg2249z00_2701;

																		{	/* SawBbv/bbv-types.scm 581 */
																			obj_t BgL_classz00_5126;

																			BgL_classz00_5126 =
																				BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
																			BgL_arg2249z00_2701 =
																				BGL_CLASS_NUM(BgL_classz00_5126);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1323z00_2700),
																			BgL_arg2249z00_2701);
																	}
																	BgL_new1320z00_2698 = BgL_new1323z00_2700;
																}
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_regz00) =
																	((BgL_rtl_regz00_bglt) ((
																				(BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_regz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_typesz00) =
																	((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_typesz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_polarityz00) =
																	((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_polarityz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_countz00) =
																	((long) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_countz00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_valuez00) =
																	((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_valuez00)),
																	BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_aliasesz00) =
																	((obj_t) bgl_remq(((obj_t) BgL_regz00_144),
																			(((BgL_bbvzd2ctxentryzd2_bglt)
																					COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_ez00_2689)))->
																				BgL_aliasesz00))), BUNSPEC);
																((((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_new1320z00_2698))->
																		BgL_initvalz00) =
																	((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_ez00_2689)))->BgL_initvalz00)),
																	BUNSPEC);
																BgL_arg2244z00_2688 = BgL_new1320z00_2698;
													}}
													BgL_newtail1675z00_2686 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg2244z00_2688), BNIL);
												}
												SET_CDR(BgL_tail1674z00_2683, BgL_newtail1675z00_2686);
												{	/* SawBbv/bbv-types.scm 575 */
													obj_t BgL_arg2243z00_2687;

													BgL_arg2243z00_2687 =
														CDR(((obj_t) BgL_l1671z00_2682));
													{
														obj_t BgL_tail1674z00_11448;
														obj_t BgL_l1671z00_11447;

														BgL_l1671z00_11447 = BgL_arg2243z00_2687;
														BgL_tail1674z00_11448 = BgL_newtail1675z00_2686;
														BgL_tail1674z00_2683 = BgL_tail1674z00_11448;
														BgL_l1671z00_2682 = BgL_l1671z00_11447;
														goto BgL_zc3z04anonymousza32241ze3z87_2684;
													}
												}
											}
									}
								}
						}
						((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1314z00_2677))->
								BgL_entriesz00) = ((obj_t) BgL_auxz00_11376), BUNSPEC);
					}
					{	/* SawBbv/bbv-types.scm 574 */
						obj_t BgL_fun2251z00_2704;

						BgL_fun2251z00_2704 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_PROCEDURE_CALL1(BgL_fun2251z00_2704,
							((obj_t) BgL_new1314z00_2677));
					}
					return BgL_new1314z00_2677;
				}
			else
				{	/* SawBbv/bbv-types.scm 572 */
					return BgL_ctxz00_143;
				}
		}

	}



/* &unalias-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62unaliaszd2ctxzb0zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8196, obj_t BgL_ctxz00_8197, obj_t BgL_regz00_8198)
	{
		{	/* SawBbv/bbv-types.scm 571 */
			return
				BGl_unaliaszd2ctxzd2zzsaw_bbvzd2typeszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_8197),
				((BgL_rtl_regz00_bglt) BgL_regz00_8198));
		}

	}



/* dump-ctx */
	obj_t BGl_dumpzd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_149, obj_t BgL_inz00_150, obj_t BgL_pz00_151)
	{
		{	/* SawBbv/bbv-types.scm 619 */
			{	/* SawBbv/bbv-types.scm 621 */
				obj_t BgL_arg2253z00_2708;

				{	/* SawBbv/bbv-types.scm 621 */
					obj_t BgL_arg2255z00_2710;

					BgL_arg2255z00_2710 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_149))->BgL_entriesz00);
					{	/* SawBbv/bbv-types.scm 622 */
						obj_t BgL_zc3z04anonymousza32257ze3z87_8199;

						BgL_zc3z04anonymousza32257ze3z87_8199 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32257ze3ze5zzsaw_bbvzd2typeszd2,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32257ze3z87_8199, (int) (0L),
							BgL_inz00_150);
						{	/* SawBbv/bbv-types.scm 621 */
							obj_t BgL_list2256z00_2711;

							BgL_list2256z00_2711 = MAKE_YOUNG_PAIR(BgL_arg2255z00_2710, BNIL);
							BgL_arg2253z00_2708 =
								BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
								(BgL_zc3z04anonymousza32257ze3z87_8199, BgL_list2256z00_2711);
				}}}
				return bgl_display_obj(BgL_arg2253z00_2708, BgL_pz00_151);
			}
		}

	}



/* &<@anonymous:2257> */
	obj_t BGl_z62zc3z04anonymousza32257ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8200, obj_t BgL_ez00_8202)
	{
		{	/* SawBbv/bbv-types.scm 621 */
			{	/* SawBbv/bbv-types.scm 622 */
				obj_t BgL_inz00_8201;

				BgL_inz00_8201 = PROCEDURE_REF(BgL_envz00_8200, (int) (0L));
				{	/* SawBbv/bbv-types.scm 623 */
					bool_t BgL_test4281z00_11470;

					{	/* SawBbv/bbv-types.scm 623 */
						bool_t BgL_test4282z00_11471;

						{	/* SawBbv/bbv-types.scm 623 */
							BgL_rtl_regz00_bglt BgL_arg2264z00_8985;

							BgL_arg2264z00_8985 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8202)))->BgL_regz00);
							{	/* SawBbv/bbv-types.scm 623 */
								obj_t BgL_classz00_8986;

								BgL_classz00_8986 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
								{	/* SawBbv/bbv-types.scm 623 */
									BgL_objectz00_bglt BgL_arg1807z00_8987;

									{	/* SawBbv/bbv-types.scm 623 */
										obj_t BgL_tmpz00_11474;

										BgL_tmpz00_11474 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2264z00_8985));
										BgL_arg1807z00_8987 =
											(BgL_objectz00_bglt) (BgL_tmpz00_11474);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-types.scm 623 */
											long BgL_idxz00_8988;

											BgL_idxz00_8988 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8987);
											BgL_test4282z00_11471 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_8988 + 2L)) == BgL_classz00_8986);
										}
									else
										{	/* SawBbv/bbv-types.scm 623 */
											bool_t BgL_res3595z00_8991;

											{	/* SawBbv/bbv-types.scm 623 */
												obj_t BgL_oclassz00_8992;

												{	/* SawBbv/bbv-types.scm 623 */
													obj_t BgL_arg1815z00_8993;
													long BgL_arg1816z00_8994;

													BgL_arg1815z00_8993 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-types.scm 623 */
														long BgL_arg1817z00_8995;

														BgL_arg1817z00_8995 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8987);
														BgL_arg1816z00_8994 =
															(BgL_arg1817z00_8995 - OBJECT_TYPE);
													}
													BgL_oclassz00_8992 =
														VECTOR_REF(BgL_arg1815z00_8993,
														BgL_arg1816z00_8994);
												}
												{	/* SawBbv/bbv-types.scm 623 */
													bool_t BgL__ortest_1115z00_8996;

													BgL__ortest_1115z00_8996 =
														(BgL_classz00_8986 == BgL_oclassz00_8992);
													if (BgL__ortest_1115z00_8996)
														{	/* SawBbv/bbv-types.scm 623 */
															BgL_res3595z00_8991 = BgL__ortest_1115z00_8996;
														}
													else
														{	/* SawBbv/bbv-types.scm 623 */
															long BgL_odepthz00_8997;

															{	/* SawBbv/bbv-types.scm 623 */
																obj_t BgL_arg1804z00_8998;

																BgL_arg1804z00_8998 = (BgL_oclassz00_8992);
																BgL_odepthz00_8997 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_8998);
															}
															if ((2L < BgL_odepthz00_8997))
																{	/* SawBbv/bbv-types.scm 623 */
																	obj_t BgL_arg1802z00_8999;

																	{	/* SawBbv/bbv-types.scm 623 */
																		obj_t BgL_arg1803z00_9000;

																		BgL_arg1803z00_9000 = (BgL_oclassz00_8992);
																		BgL_arg1802z00_8999 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_9000, 2L);
																	}
																	BgL_res3595z00_8991 =
																		(BgL_arg1802z00_8999 == BgL_classz00_8986);
																}
															else
																{	/* SawBbv/bbv-types.scm 623 */
																	BgL_res3595z00_8991 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test4282z00_11471 = BgL_res3595z00_8991;
										}
								}
							}
						}
						if (BgL_test4282z00_11471)
							{	/* SawBbv/bbv-types.scm 624 */
								BgL_rtl_regz00_bglt BgL_regz00_9001;

								BgL_regz00_9001 =
									((BgL_rtl_regz00_bglt)
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8202)))->
										BgL_regz00));
								{	/* SawBbv/bbv-types.scm 624 */
									long BgL_basez00_9002;
									long BgL_bitz00_9003;

									{	/* SawBbv/bbv-types.scm 624 */
										int BgL_arg3449z00_9004;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_11500;

											{
												obj_t BgL_auxz00_11501;

												{	/* SawBbv/bbv-types.scm 624 */
													BgL_objectz00_bglt BgL_tmpz00_11502;

													BgL_tmpz00_11502 =
														((BgL_objectz00_bglt) BgL_regz00_9001);
													BgL_auxz00_11501 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_11502);
												}
												BgL_auxz00_11500 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_11501);
											}
											BgL_arg3449z00_9004 =
												(((BgL_rtl_regzf2razf2_bglt)
													COBJECT(BgL_auxz00_11500))->BgL_numz00);
										}
										BgL_basez00_9002 = ((long) (BgL_arg3449z00_9004) / 8L);
									}
									{	/* SawBbv/bbv-types.scm 624 */
										int BgL_arg3451z00_9005;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_11509;

											{
												obj_t BgL_auxz00_11510;

												{	/* SawBbv/bbv-types.scm 624 */
													BgL_objectz00_bglt BgL_tmpz00_11511;

													BgL_tmpz00_11511 =
														((BgL_objectz00_bglt) BgL_regz00_9001);
													BgL_auxz00_11510 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_11511);
												}
												BgL_auxz00_11509 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_11510);
											}
											BgL_arg3451z00_9005 =
												(((BgL_rtl_regzf2razf2_bglt)
													COBJECT(BgL_auxz00_11509))->BgL_numz00);
										}
										{	/* SawBbv/bbv-types.scm 624 */
											long BgL_n1z00_9006;
											long BgL_n2z00_9007;

											BgL_n1z00_9006 = (long) (BgL_arg3451z00_9005);
											BgL_n2z00_9007 = 8L;
											{	/* SawBbv/bbv-types.scm 624 */
												bool_t BgL_test4286z00_11517;

												{	/* SawBbv/bbv-types.scm 624 */
													long BgL_arg1338z00_9008;

													BgL_arg1338z00_9008 =
														(((BgL_n1z00_9006) | (BgL_n2z00_9007)) &
														-2147483648);
													BgL_test4286z00_11517 = (BgL_arg1338z00_9008 == 0L);
												}
												if (BgL_test4286z00_11517)
													{	/* SawBbv/bbv-types.scm 624 */
														int32_t BgL_arg1334z00_9009;

														{	/* SawBbv/bbv-types.scm 624 */
															int32_t BgL_arg1336z00_9010;
															int32_t BgL_arg1337z00_9011;

															BgL_arg1336z00_9010 = (int32_t) (BgL_n1z00_9006);
															BgL_arg1337z00_9011 = (int32_t) (BgL_n2z00_9007);
															BgL_arg1334z00_9009 =
																(BgL_arg1336z00_9010 % BgL_arg1337z00_9011);
														}
														{	/* SawBbv/bbv-types.scm 624 */
															long BgL_arg1446z00_9012;

															BgL_arg1446z00_9012 =
																(long) (BgL_arg1334z00_9009);
															BgL_bitz00_9003 = (long) (BgL_arg1446z00_9012);
													}}
												else
													{	/* SawBbv/bbv-types.scm 624 */
														BgL_bitz00_9003 = (BgL_n1z00_9006 % BgL_n2z00_9007);
													}
											}
										}
									}
									if (
										(BgL_basez00_9002 <
											STRING_LENGTH(
												(((BgL_regsetz00_bglt) COBJECT(
															((BgL_regsetz00_bglt) BgL_inz00_8201)))->
													BgL_stringz00))))
										{	/* SawBbv/bbv-types.scm 624 */
											BgL_test4281z00_11470 =
												(
												((STRING_REF(
															(((BgL_regsetz00_bglt) COBJECT(
																		((BgL_regsetz00_bglt) BgL_inz00_8201)))->
																BgL_stringz00),
															BgL_basez00_9002)) & (1L <<
														(int) (BgL_bitz00_9003))) > 0L);
										}
									else
										{	/* SawBbv/bbv-types.scm 624 */
											BgL_test4281z00_11470 = ((bool_t) 0);
										}
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 623 */
								BgL_test4281z00_11470 = ((bool_t) 1);
							}
					}
					if (BgL_test4281z00_11470)
						{	/* SawBbv/bbv-types.scm 623 */
							return BGl_shapez00zztools_shapez00(BgL_ez00_8202);
						}
					else
						{	/* SawBbv/bbv-types.scm 623 */
							return BFALSE;
						}
				}
			}
		}

	}



/* rtl_ins-last? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2lastzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_161)
	{
		{	/* SawBbv/bbv-types.scm 712 */
			{	/* SawBbv/bbv-types.scm 714 */
				BgL_rtl_funz00_bglt BgL_arg2265z00_5209;

				BgL_arg2265z00_5209 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_161))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 714 */
					obj_t BgL_classz00_5210;

					BgL_classz00_5210 = BGl_rtl_lastz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 714 */
						BgL_objectz00_bglt BgL_arg1807z00_5212;

						{	/* SawBbv/bbv-types.scm 714 */
							obj_t BgL_tmpz00_11541;

							BgL_tmpz00_11541 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2265z00_5209));
							BgL_arg1807z00_5212 = (BgL_objectz00_bglt) (BgL_tmpz00_11541);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 714 */
								long BgL_idxz00_5218;

								BgL_idxz00_5218 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5212);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5218 + 2L)) == BgL_classz00_5210);
							}
						else
							{	/* SawBbv/bbv-types.scm 714 */
								bool_t BgL_res3596z00_5243;

								{	/* SawBbv/bbv-types.scm 714 */
									obj_t BgL_oclassz00_5226;

									{	/* SawBbv/bbv-types.scm 714 */
										obj_t BgL_arg1815z00_5234;
										long BgL_arg1816z00_5235;

										BgL_arg1815z00_5234 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 714 */
											long BgL_arg1817z00_5236;

											BgL_arg1817z00_5236 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5212);
											BgL_arg1816z00_5235 = (BgL_arg1817z00_5236 - OBJECT_TYPE);
										}
										BgL_oclassz00_5226 =
											VECTOR_REF(BgL_arg1815z00_5234, BgL_arg1816z00_5235);
									}
									{	/* SawBbv/bbv-types.scm 714 */
										bool_t BgL__ortest_1115z00_5227;

										BgL__ortest_1115z00_5227 =
											(BgL_classz00_5210 == BgL_oclassz00_5226);
										if (BgL__ortest_1115z00_5227)
											{	/* SawBbv/bbv-types.scm 714 */
												BgL_res3596z00_5243 = BgL__ortest_1115z00_5227;
											}
										else
											{	/* SawBbv/bbv-types.scm 714 */
												long BgL_odepthz00_5228;

												{	/* SawBbv/bbv-types.scm 714 */
													obj_t BgL_arg1804z00_5229;

													BgL_arg1804z00_5229 = (BgL_oclassz00_5226);
													BgL_odepthz00_5228 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5229);
												}
												if ((2L < BgL_odepthz00_5228))
													{	/* SawBbv/bbv-types.scm 714 */
														obj_t BgL_arg1802z00_5231;

														{	/* SawBbv/bbv-types.scm 714 */
															obj_t BgL_arg1803z00_5232;

															BgL_arg1803z00_5232 = (BgL_oclassz00_5226);
															BgL_arg1802z00_5231 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5232,
																2L);
														}
														BgL_res3596z00_5243 =
															(BgL_arg1802z00_5231 == BgL_classz00_5210);
													}
												else
													{	/* SawBbv/bbv-types.scm 714 */
														BgL_res3596z00_5243 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3596z00_5243;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-last? */
	obj_t BGl_z62rtl_inszd2lastzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8203,
		obj_t BgL_iz00_8204)
	{
		{	/* SawBbv/bbv-types.scm 712 */
			return
				BBOOL(BGl_rtl_inszd2lastzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8204)));
		}

	}



/* rtl_ins-nop? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2nopzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_162)
	{
		{	/* SawBbv/bbv-types.scm 719 */
			{	/* SawBbv/bbv-types.scm 721 */
				BgL_rtl_funz00_bglt BgL_arg2266z00_5245;

				BgL_arg2266z00_5245 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_162))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 721 */
					obj_t BgL_classz00_5246;

					BgL_classz00_5246 = BGl_rtl_nopz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 721 */
						BgL_objectz00_bglt BgL_arg1807z00_5248;

						{	/* SawBbv/bbv-types.scm 721 */
							obj_t BgL_tmpz00_11568;

							BgL_tmpz00_11568 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2266z00_5245));
							BgL_arg1807z00_5248 = (BgL_objectz00_bglt) (BgL_tmpz00_11568);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 721 */
								long BgL_idxz00_5254;

								BgL_idxz00_5254 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5248);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5254 + 3L)) == BgL_classz00_5246);
							}
						else
							{	/* SawBbv/bbv-types.scm 721 */
								bool_t BgL_res3597z00_5279;

								{	/* SawBbv/bbv-types.scm 721 */
									obj_t BgL_oclassz00_5262;

									{	/* SawBbv/bbv-types.scm 721 */
										obj_t BgL_arg1815z00_5270;
										long BgL_arg1816z00_5271;

										BgL_arg1815z00_5270 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 721 */
											long BgL_arg1817z00_5272;

											BgL_arg1817z00_5272 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5248);
											BgL_arg1816z00_5271 = (BgL_arg1817z00_5272 - OBJECT_TYPE);
										}
										BgL_oclassz00_5262 =
											VECTOR_REF(BgL_arg1815z00_5270, BgL_arg1816z00_5271);
									}
									{	/* SawBbv/bbv-types.scm 721 */
										bool_t BgL__ortest_1115z00_5263;

										BgL__ortest_1115z00_5263 =
											(BgL_classz00_5246 == BgL_oclassz00_5262);
										if (BgL__ortest_1115z00_5263)
											{	/* SawBbv/bbv-types.scm 721 */
												BgL_res3597z00_5279 = BgL__ortest_1115z00_5263;
											}
										else
											{	/* SawBbv/bbv-types.scm 721 */
												long BgL_odepthz00_5264;

												{	/* SawBbv/bbv-types.scm 721 */
													obj_t BgL_arg1804z00_5265;

													BgL_arg1804z00_5265 = (BgL_oclassz00_5262);
													BgL_odepthz00_5264 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5265);
												}
												if ((3L < BgL_odepthz00_5264))
													{	/* SawBbv/bbv-types.scm 721 */
														obj_t BgL_arg1802z00_5267;

														{	/* SawBbv/bbv-types.scm 721 */
															obj_t BgL_arg1803z00_5268;

															BgL_arg1803z00_5268 = (BgL_oclassz00_5262);
															BgL_arg1802z00_5267 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5268,
																3L);
														}
														BgL_res3597z00_5279 =
															(BgL_arg1802z00_5267 == BgL_classz00_5246);
													}
												else
													{	/* SawBbv/bbv-types.scm 721 */
														BgL_res3597z00_5279 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3597z00_5279;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-nop? */
	obj_t BGl_z62rtl_inszd2nopzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8205,
		obj_t BgL_iz00_8206)
	{
		{	/* SawBbv/bbv-types.scm 719 */
			return
				BBOOL(BGl_rtl_inszd2nopzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8206)));
		}

	}



/* rtl_ins-mov? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_163)
	{
		{	/* SawBbv/bbv-types.scm 726 */
			{	/* SawBbv/bbv-types.scm 728 */
				BgL_rtl_funz00_bglt BgL_arg2267z00_5281;

				BgL_arg2267z00_5281 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_163))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 728 */
					obj_t BgL_classz00_5282;

					BgL_classz00_5282 = BGl_rtl_movz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 728 */
						BgL_objectz00_bglt BgL_arg1807z00_5284;

						{	/* SawBbv/bbv-types.scm 728 */
							obj_t BgL_tmpz00_11595;

							BgL_tmpz00_11595 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2267z00_5281));
							BgL_arg1807z00_5284 = (BgL_objectz00_bglt) (BgL_tmpz00_11595);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 728 */
								long BgL_idxz00_5290;

								BgL_idxz00_5290 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5284);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5290 + 3L)) == BgL_classz00_5282);
							}
						else
							{	/* SawBbv/bbv-types.scm 728 */
								bool_t BgL_res3598z00_5315;

								{	/* SawBbv/bbv-types.scm 728 */
									obj_t BgL_oclassz00_5298;

									{	/* SawBbv/bbv-types.scm 728 */
										obj_t BgL_arg1815z00_5306;
										long BgL_arg1816z00_5307;

										BgL_arg1815z00_5306 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 728 */
											long BgL_arg1817z00_5308;

											BgL_arg1817z00_5308 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5284);
											BgL_arg1816z00_5307 = (BgL_arg1817z00_5308 - OBJECT_TYPE);
										}
										BgL_oclassz00_5298 =
											VECTOR_REF(BgL_arg1815z00_5306, BgL_arg1816z00_5307);
									}
									{	/* SawBbv/bbv-types.scm 728 */
										bool_t BgL__ortest_1115z00_5299;

										BgL__ortest_1115z00_5299 =
											(BgL_classz00_5282 == BgL_oclassz00_5298);
										if (BgL__ortest_1115z00_5299)
											{	/* SawBbv/bbv-types.scm 728 */
												BgL_res3598z00_5315 = BgL__ortest_1115z00_5299;
											}
										else
											{	/* SawBbv/bbv-types.scm 728 */
												long BgL_odepthz00_5300;

												{	/* SawBbv/bbv-types.scm 728 */
													obj_t BgL_arg1804z00_5301;

													BgL_arg1804z00_5301 = (BgL_oclassz00_5298);
													BgL_odepthz00_5300 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5301);
												}
												if ((3L < BgL_odepthz00_5300))
													{	/* SawBbv/bbv-types.scm 728 */
														obj_t BgL_arg1802z00_5303;

														{	/* SawBbv/bbv-types.scm 728 */
															obj_t BgL_arg1803z00_5304;

															BgL_arg1803z00_5304 = (BgL_oclassz00_5298);
															BgL_arg1802z00_5303 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5304,
																3L);
														}
														BgL_res3598z00_5315 =
															(BgL_arg1802z00_5303 == BgL_classz00_5282);
													}
												else
													{	/* SawBbv/bbv-types.scm 728 */
														BgL_res3598z00_5315 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3598z00_5315;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-mov? */
	obj_t BGl_z62rtl_inszd2movzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8207,
		obj_t BgL_iz00_8208)
	{
		{	/* SawBbv/bbv-types.scm 726 */
			return
				BBOOL(BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8208)));
		}

	}



/* rtl_ins-go? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt BgL_iz00_164)
	{
		{	/* SawBbv/bbv-types.scm 733 */
			{	/* SawBbv/bbv-types.scm 735 */
				BgL_rtl_funz00_bglt BgL_arg2268z00_5317;

				BgL_arg2268z00_5317 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_164))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 735 */
					obj_t BgL_classz00_5318;

					BgL_classz00_5318 = BGl_rtl_goz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 735 */
						BgL_objectz00_bglt BgL_arg1807z00_5320;

						{	/* SawBbv/bbv-types.scm 735 */
							obj_t BgL_tmpz00_11622;

							BgL_tmpz00_11622 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2268z00_5317));
							BgL_arg1807z00_5320 = (BgL_objectz00_bglt) (BgL_tmpz00_11622);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 735 */
								long BgL_idxz00_5326;

								BgL_idxz00_5326 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5320);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5326 + 3L)) == BgL_classz00_5318);
							}
						else
							{	/* SawBbv/bbv-types.scm 735 */
								bool_t BgL_res3599z00_5351;

								{	/* SawBbv/bbv-types.scm 735 */
									obj_t BgL_oclassz00_5334;

									{	/* SawBbv/bbv-types.scm 735 */
										obj_t BgL_arg1815z00_5342;
										long BgL_arg1816z00_5343;

										BgL_arg1815z00_5342 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 735 */
											long BgL_arg1817z00_5344;

											BgL_arg1817z00_5344 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5320);
											BgL_arg1816z00_5343 = (BgL_arg1817z00_5344 - OBJECT_TYPE);
										}
										BgL_oclassz00_5334 =
											VECTOR_REF(BgL_arg1815z00_5342, BgL_arg1816z00_5343);
									}
									{	/* SawBbv/bbv-types.scm 735 */
										bool_t BgL__ortest_1115z00_5335;

										BgL__ortest_1115z00_5335 =
											(BgL_classz00_5318 == BgL_oclassz00_5334);
										if (BgL__ortest_1115z00_5335)
											{	/* SawBbv/bbv-types.scm 735 */
												BgL_res3599z00_5351 = BgL__ortest_1115z00_5335;
											}
										else
											{	/* SawBbv/bbv-types.scm 735 */
												long BgL_odepthz00_5336;

												{	/* SawBbv/bbv-types.scm 735 */
													obj_t BgL_arg1804z00_5337;

													BgL_arg1804z00_5337 = (BgL_oclassz00_5334);
													BgL_odepthz00_5336 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5337);
												}
												if ((3L < BgL_odepthz00_5336))
													{	/* SawBbv/bbv-types.scm 735 */
														obj_t BgL_arg1802z00_5339;

														{	/* SawBbv/bbv-types.scm 735 */
															obj_t BgL_arg1803z00_5340;

															BgL_arg1803z00_5340 = (BgL_oclassz00_5334);
															BgL_arg1802z00_5339 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5340,
																3L);
														}
														BgL_res3599z00_5351 =
															(BgL_arg1802z00_5339 == BgL_classz00_5318);
													}
												else
													{	/* SawBbv/bbv-types.scm 735 */
														BgL_res3599z00_5351 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3599z00_5351;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-go? */
	obj_t BGl_z62rtl_inszd2gozf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8209,
		obj_t BgL_iz00_8210)
	{
		{	/* SawBbv/bbv-types.scm 733 */
			return
				BBOOL(BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8210)));
		}

	}



/* rtl_ins-pragma? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2pragmazf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_165)
	{
		{	/* SawBbv/bbv-types.scm 740 */
			{	/* SawBbv/bbv-types.scm 742 */
				BgL_rtl_funz00_bglt BgL_arg2269z00_5353;

				BgL_arg2269z00_5353 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_165))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 742 */
					obj_t BgL_classz00_5354;

					BgL_classz00_5354 = BGl_rtl_pragmaz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 742 */
						BgL_objectz00_bglt BgL_arg1807z00_5356;

						{	/* SawBbv/bbv-types.scm 742 */
							obj_t BgL_tmpz00_11649;

							BgL_tmpz00_11649 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2269z00_5353));
							BgL_arg1807z00_5356 = (BgL_objectz00_bglt) (BgL_tmpz00_11649);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 742 */
								long BgL_idxz00_5362;

								BgL_idxz00_5362 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5356);
								{	/* SawBbv/bbv-types.scm 742 */
									obj_t BgL_arg1800z00_5363;

									BgL_arg1800z00_5363 =
										VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5362 + 2L));
									return BBOOL((BgL_arg1800z00_5363 == BgL_classz00_5354));
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 742 */
								bool_t BgL_res3600z00_5387;

								{	/* SawBbv/bbv-types.scm 742 */
									obj_t BgL_oclassz00_5370;

									{	/* SawBbv/bbv-types.scm 742 */
										obj_t BgL_arg1815z00_5378;
										long BgL_arg1816z00_5379;

										BgL_arg1815z00_5378 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 742 */
											long BgL_arg1817z00_5380;

											BgL_arg1817z00_5380 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5356);
											BgL_arg1816z00_5379 = (BgL_arg1817z00_5380 - OBJECT_TYPE);
										}
										BgL_oclassz00_5370 =
											VECTOR_REF(BgL_arg1815z00_5378, BgL_arg1816z00_5379);
									}
									{	/* SawBbv/bbv-types.scm 742 */
										bool_t BgL__ortest_1115z00_5371;

										BgL__ortest_1115z00_5371 =
											(BgL_classz00_5354 == BgL_oclassz00_5370);
										if (BgL__ortest_1115z00_5371)
											{	/* SawBbv/bbv-types.scm 742 */
												BgL_res3600z00_5387 = BgL__ortest_1115z00_5371;
											}
										else
											{	/* SawBbv/bbv-types.scm 742 */
												long BgL_odepthz00_5372;

												{	/* SawBbv/bbv-types.scm 742 */
													obj_t BgL_arg1804z00_5373;

													BgL_arg1804z00_5373 = (BgL_oclassz00_5370);
													BgL_odepthz00_5372 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5373);
												}
												if ((2L < BgL_odepthz00_5372))
													{	/* SawBbv/bbv-types.scm 742 */
														obj_t BgL_arg1802z00_5375;

														{	/* SawBbv/bbv-types.scm 742 */
															obj_t BgL_arg1803z00_5376;

															BgL_arg1803z00_5376 = (BgL_oclassz00_5370);
															BgL_arg1802z00_5375 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5376,
																2L);
														}
														BgL_res3600z00_5387 =
															(BgL_arg1802z00_5375 == BgL_classz00_5354);
													}
												else
													{	/* SawBbv/bbv-types.scm 742 */
														BgL_res3600z00_5387 = ((bool_t) 0);
													}
											}
									}
								}
								return BBOOL(BgL_res3600z00_5387);
							}
					}
				}
			}
		}

	}



/* &rtl_ins-pragma? */
	obj_t BGl_z62rtl_inszd2pragmazf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8211,
		obj_t BgL_iz00_8212)
	{
		{	/* SawBbv/bbv-types.scm 740 */
			return
				BGl_rtl_inszd2pragmazf3z21zzsaw_bbvzd2typeszd2(
				((BgL_rtl_insz00_bglt) BgL_iz00_8212));
		}

	}



/* rtl_ins-fail? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2failzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_166)
	{
		{	/* SawBbv/bbv-types.scm 747 */
			{	/* SawBbv/bbv-types.scm 749 */
				BgL_rtl_funz00_bglt BgL_arg2270z00_5389;

				BgL_arg2270z00_5389 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_166))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 749 */
					obj_t BgL_classz00_5390;

					BgL_classz00_5390 = BGl_rtl_failz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 749 */
						BgL_objectz00_bglt BgL_arg1807z00_5392;

						{	/* SawBbv/bbv-types.scm 749 */
							obj_t BgL_tmpz00_11677;

							BgL_tmpz00_11677 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2270z00_5389));
							BgL_arg1807z00_5392 = (BgL_objectz00_bglt) (BgL_tmpz00_11677);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 749 */
								long BgL_idxz00_5398;

								BgL_idxz00_5398 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5392);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5398 + 3L)) == BgL_classz00_5390);
							}
						else
							{	/* SawBbv/bbv-types.scm 749 */
								bool_t BgL_res3601z00_5423;

								{	/* SawBbv/bbv-types.scm 749 */
									obj_t BgL_oclassz00_5406;

									{	/* SawBbv/bbv-types.scm 749 */
										obj_t BgL_arg1815z00_5414;
										long BgL_arg1816z00_5415;

										BgL_arg1815z00_5414 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 749 */
											long BgL_arg1817z00_5416;

											BgL_arg1817z00_5416 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5392);
											BgL_arg1816z00_5415 = (BgL_arg1817z00_5416 - OBJECT_TYPE);
										}
										BgL_oclassz00_5406 =
											VECTOR_REF(BgL_arg1815z00_5414, BgL_arg1816z00_5415);
									}
									{	/* SawBbv/bbv-types.scm 749 */
										bool_t BgL__ortest_1115z00_5407;

										BgL__ortest_1115z00_5407 =
											(BgL_classz00_5390 == BgL_oclassz00_5406);
										if (BgL__ortest_1115z00_5407)
											{	/* SawBbv/bbv-types.scm 749 */
												BgL_res3601z00_5423 = BgL__ortest_1115z00_5407;
											}
										else
											{	/* SawBbv/bbv-types.scm 749 */
												long BgL_odepthz00_5408;

												{	/* SawBbv/bbv-types.scm 749 */
													obj_t BgL_arg1804z00_5409;

													BgL_arg1804z00_5409 = (BgL_oclassz00_5406);
													BgL_odepthz00_5408 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5409);
												}
												if ((3L < BgL_odepthz00_5408))
													{	/* SawBbv/bbv-types.scm 749 */
														obj_t BgL_arg1802z00_5411;

														{	/* SawBbv/bbv-types.scm 749 */
															obj_t BgL_arg1803z00_5412;

															BgL_arg1803z00_5412 = (BgL_oclassz00_5406);
															BgL_arg1802z00_5411 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5412,
																3L);
														}
														BgL_res3601z00_5423 =
															(BgL_arg1802z00_5411 == BgL_classz00_5390);
													}
												else
													{	/* SawBbv/bbv-types.scm 749 */
														BgL_res3601z00_5423 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3601z00_5423;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-fail? */
	obj_t BGl_z62rtl_inszd2failzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8213,
		obj_t BgL_iz00_8214)
	{
		{	/* SawBbv/bbv-types.scm 747 */
			return
				BBOOL(BGl_rtl_inszd2failzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8214)));
		}

	}



/* rtl_ins-switch? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_167)
	{
		{	/* SawBbv/bbv-types.scm 754 */
			{	/* SawBbv/bbv-types.scm 756 */
				BgL_rtl_funz00_bglt BgL_arg2271z00_5425;

				BgL_arg2271z00_5425 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_167))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 756 */
					obj_t BgL_classz00_5426;

					BgL_classz00_5426 = BGl_rtl_switchz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 756 */
						BgL_objectz00_bglt BgL_arg1807z00_5428;

						{	/* SawBbv/bbv-types.scm 756 */
							obj_t BgL_tmpz00_11704;

							BgL_tmpz00_11704 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2271z00_5425));
							BgL_arg1807z00_5428 = (BgL_objectz00_bglt) (BgL_tmpz00_11704);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 756 */
								long BgL_idxz00_5434;

								BgL_idxz00_5434 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5428);
								{	/* SawBbv/bbv-types.scm 756 */
									obj_t BgL_arg1800z00_5435;

									BgL_arg1800z00_5435 =
										VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5434 + 4L));
									return BBOOL((BgL_arg1800z00_5435 == BgL_classz00_5426));
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 756 */
								bool_t BgL_res3602z00_5459;

								{	/* SawBbv/bbv-types.scm 756 */
									obj_t BgL_oclassz00_5442;

									{	/* SawBbv/bbv-types.scm 756 */
										obj_t BgL_arg1815z00_5450;
										long BgL_arg1816z00_5451;

										BgL_arg1815z00_5450 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 756 */
											long BgL_arg1817z00_5452;

											BgL_arg1817z00_5452 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5428);
											BgL_arg1816z00_5451 = (BgL_arg1817z00_5452 - OBJECT_TYPE);
										}
										BgL_oclassz00_5442 =
											VECTOR_REF(BgL_arg1815z00_5450, BgL_arg1816z00_5451);
									}
									{	/* SawBbv/bbv-types.scm 756 */
										bool_t BgL__ortest_1115z00_5443;

										BgL__ortest_1115z00_5443 =
											(BgL_classz00_5426 == BgL_oclassz00_5442);
										if (BgL__ortest_1115z00_5443)
											{	/* SawBbv/bbv-types.scm 756 */
												BgL_res3602z00_5459 = BgL__ortest_1115z00_5443;
											}
										else
											{	/* SawBbv/bbv-types.scm 756 */
												long BgL_odepthz00_5444;

												{	/* SawBbv/bbv-types.scm 756 */
													obj_t BgL_arg1804z00_5445;

													BgL_arg1804z00_5445 = (BgL_oclassz00_5442);
													BgL_odepthz00_5444 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5445);
												}
												if ((4L < BgL_odepthz00_5444))
													{	/* SawBbv/bbv-types.scm 756 */
														obj_t BgL_arg1802z00_5447;

														{	/* SawBbv/bbv-types.scm 756 */
															obj_t BgL_arg1803z00_5448;

															BgL_arg1803z00_5448 = (BgL_oclassz00_5442);
															BgL_arg1802z00_5447 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5448,
																4L);
														}
														BgL_res3602z00_5459 =
															(BgL_arg1802z00_5447 == BgL_classz00_5426);
													}
												else
													{	/* SawBbv/bbv-types.scm 756 */
														BgL_res3602z00_5459 = ((bool_t) 0);
													}
											}
									}
								}
								return BBOOL(BgL_res3602z00_5459);
							}
					}
				}
			}
		}

	}



/* &rtl_ins-switch? */
	obj_t BGl_z62rtl_inszd2switchzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8215,
		obj_t BgL_iz00_8216)
	{
		{	/* SawBbv/bbv-types.scm 754 */
			return
				BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2(
				((BgL_rtl_insz00_bglt) BgL_iz00_8216));
		}

	}



/* rtl_ins-br? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2brzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt BgL_iz00_168)
	{
		{	/* SawBbv/bbv-types.scm 761 */
			{	/* SawBbv/bbv-types.scm 762 */
				bool_t BgL__ortest_1338z00_5460;

				{	/* SawBbv/bbv-types.scm 769 */
					BgL_rtl_funz00_bglt BgL_arg2272z00_5463;

					BgL_arg2272z00_5463 =
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_168))->BgL_funz00);
					{	/* SawBbv/bbv-types.scm 769 */
						obj_t BgL_classz00_5464;

						BgL_classz00_5464 = BGl_rtl_ifeqz00zzsaw_defsz00;
						{	/* SawBbv/bbv-types.scm 769 */
							BgL_objectz00_bglt BgL_arg1807z00_5466;

							{	/* SawBbv/bbv-types.scm 769 */
								obj_t BgL_tmpz00_11732;

								BgL_tmpz00_11732 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2272z00_5463));
								BgL_arg1807z00_5466 = (BgL_objectz00_bglt) (BgL_tmpz00_11732);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 769 */
									long BgL_idxz00_5472;

									BgL_idxz00_5472 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5466);
									BgL__ortest_1338z00_5460 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5472 + 3L)) == BgL_classz00_5464);
								}
							else
								{	/* SawBbv/bbv-types.scm 769 */
									bool_t BgL_res3603z00_5497;

									{	/* SawBbv/bbv-types.scm 769 */
										obj_t BgL_oclassz00_5480;

										{	/* SawBbv/bbv-types.scm 769 */
											obj_t BgL_arg1815z00_5488;
											long BgL_arg1816z00_5489;

											BgL_arg1815z00_5488 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 769 */
												long BgL_arg1817z00_5490;

												BgL_arg1817z00_5490 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5466);
												BgL_arg1816z00_5489 =
													(BgL_arg1817z00_5490 - OBJECT_TYPE);
											}
											BgL_oclassz00_5480 =
												VECTOR_REF(BgL_arg1815z00_5488, BgL_arg1816z00_5489);
										}
										{	/* SawBbv/bbv-types.scm 769 */
											bool_t BgL__ortest_1115z00_5481;

											BgL__ortest_1115z00_5481 =
												(BgL_classz00_5464 == BgL_oclassz00_5480);
											if (BgL__ortest_1115z00_5481)
												{	/* SawBbv/bbv-types.scm 769 */
													BgL_res3603z00_5497 = BgL__ortest_1115z00_5481;
												}
											else
												{	/* SawBbv/bbv-types.scm 769 */
													long BgL_odepthz00_5482;

													{	/* SawBbv/bbv-types.scm 769 */
														obj_t BgL_arg1804z00_5483;

														BgL_arg1804z00_5483 = (BgL_oclassz00_5480);
														BgL_odepthz00_5482 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5483);
													}
													if ((3L < BgL_odepthz00_5482))
														{	/* SawBbv/bbv-types.scm 769 */
															obj_t BgL_arg1802z00_5485;

															{	/* SawBbv/bbv-types.scm 769 */
																obj_t BgL_arg1803z00_5486;

																BgL_arg1803z00_5486 = (BgL_oclassz00_5480);
																BgL_arg1802z00_5485 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5486,
																	3L);
															}
															BgL_res3603z00_5497 =
																(BgL_arg1802z00_5485 == BgL_classz00_5464);
														}
													else
														{	/* SawBbv/bbv-types.scm 769 */
															BgL_res3603z00_5497 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1338z00_5460 = BgL_res3603z00_5497;
								}
						}
					}
				}
				if (BgL__ortest_1338z00_5460)
					{	/* SawBbv/bbv-types.scm 762 */
						return BgL__ortest_1338z00_5460;
					}
				else
					{	/* SawBbv/bbv-types.scm 776 */
						BgL_rtl_funz00_bglt BgL_arg2273z00_5500;

						BgL_arg2273z00_5500 =
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_168))->BgL_funz00);
						{	/* SawBbv/bbv-types.scm 776 */
							obj_t BgL_classz00_5501;

							BgL_classz00_5501 = BGl_rtl_ifnez00zzsaw_defsz00;
							{	/* SawBbv/bbv-types.scm 776 */
								BgL_objectz00_bglt BgL_arg1807z00_5503;

								{	/* SawBbv/bbv-types.scm 776 */
									obj_t BgL_tmpz00_11757;

									BgL_tmpz00_11757 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg2273z00_5500));
									BgL_arg1807z00_5503 = (BgL_objectz00_bglt) (BgL_tmpz00_11757);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 776 */
										long BgL_idxz00_5509;

										BgL_idxz00_5509 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5503);
										return
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5509 + 3L)) == BgL_classz00_5501);
									}
								else
									{	/* SawBbv/bbv-types.scm 776 */
										bool_t BgL_res3604z00_5534;

										{	/* SawBbv/bbv-types.scm 776 */
											obj_t BgL_oclassz00_5517;

											{	/* SawBbv/bbv-types.scm 776 */
												obj_t BgL_arg1815z00_5525;
												long BgL_arg1816z00_5526;

												BgL_arg1815z00_5525 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 776 */
													long BgL_arg1817z00_5527;

													BgL_arg1817z00_5527 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5503);
													BgL_arg1816z00_5526 =
														(BgL_arg1817z00_5527 - OBJECT_TYPE);
												}
												BgL_oclassz00_5517 =
													VECTOR_REF(BgL_arg1815z00_5525, BgL_arg1816z00_5526);
											}
											{	/* SawBbv/bbv-types.scm 776 */
												bool_t BgL__ortest_1115z00_5518;

												BgL__ortest_1115z00_5518 =
													(BgL_classz00_5501 == BgL_oclassz00_5517);
												if (BgL__ortest_1115z00_5518)
													{	/* SawBbv/bbv-types.scm 776 */
														BgL_res3604z00_5534 = BgL__ortest_1115z00_5518;
													}
												else
													{	/* SawBbv/bbv-types.scm 776 */
														long BgL_odepthz00_5519;

														{	/* SawBbv/bbv-types.scm 776 */
															obj_t BgL_arg1804z00_5520;

															BgL_arg1804z00_5520 = (BgL_oclassz00_5517);
															BgL_odepthz00_5519 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5520);
														}
														if ((3L < BgL_odepthz00_5519))
															{	/* SawBbv/bbv-types.scm 776 */
																obj_t BgL_arg1802z00_5522;

																{	/* SawBbv/bbv-types.scm 776 */
																	obj_t BgL_arg1803z00_5523;

																	BgL_arg1803z00_5523 = (BgL_oclassz00_5517);
																	BgL_arg1802z00_5522 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5523,
																		3L);
																}
																BgL_res3604z00_5534 =
																	(BgL_arg1802z00_5522 == BgL_classz00_5501);
															}
														else
															{	/* SawBbv/bbv-types.scm 776 */
																BgL_res3604z00_5534 = ((bool_t) 0);
															}
													}
											}
										}
										return BgL_res3604z00_5534;
									}
							}
						}
					}
			}
		}

	}



/* &rtl_ins-br? */
	obj_t BGl_z62rtl_inszd2brzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8217,
		obj_t BgL_iz00_8218)
	{
		{	/* SawBbv/bbv-types.scm 761 */
			return
				BBOOL(BGl_rtl_inszd2brzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8218)));
		}

	}



/* rtl_ins-ifeq? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_169)
	{
		{	/* SawBbv/bbv-types.scm 767 */
			{	/* SawBbv/bbv-types.scm 769 */
				BgL_rtl_funz00_bglt BgL_arg2272z00_5536;

				BgL_arg2272z00_5536 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_169))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 769 */
					obj_t BgL_classz00_5537;

					BgL_classz00_5537 = BGl_rtl_ifeqz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 769 */
						BgL_objectz00_bglt BgL_arg1807z00_5539;

						{	/* SawBbv/bbv-types.scm 769 */
							obj_t BgL_tmpz00_11784;

							BgL_tmpz00_11784 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2272z00_5536));
							BgL_arg1807z00_5539 = (BgL_objectz00_bglt) (BgL_tmpz00_11784);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 769 */
								long BgL_idxz00_5545;

								BgL_idxz00_5545 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5539);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5545 + 3L)) == BgL_classz00_5537);
							}
						else
							{	/* SawBbv/bbv-types.scm 769 */
								bool_t BgL_res3605z00_5570;

								{	/* SawBbv/bbv-types.scm 769 */
									obj_t BgL_oclassz00_5553;

									{	/* SawBbv/bbv-types.scm 769 */
										obj_t BgL_arg1815z00_5561;
										long BgL_arg1816z00_5562;

										BgL_arg1815z00_5561 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 769 */
											long BgL_arg1817z00_5563;

											BgL_arg1817z00_5563 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5539);
											BgL_arg1816z00_5562 = (BgL_arg1817z00_5563 - OBJECT_TYPE);
										}
										BgL_oclassz00_5553 =
											VECTOR_REF(BgL_arg1815z00_5561, BgL_arg1816z00_5562);
									}
									{	/* SawBbv/bbv-types.scm 769 */
										bool_t BgL__ortest_1115z00_5554;

										BgL__ortest_1115z00_5554 =
											(BgL_classz00_5537 == BgL_oclassz00_5553);
										if (BgL__ortest_1115z00_5554)
											{	/* SawBbv/bbv-types.scm 769 */
												BgL_res3605z00_5570 = BgL__ortest_1115z00_5554;
											}
										else
											{	/* SawBbv/bbv-types.scm 769 */
												long BgL_odepthz00_5555;

												{	/* SawBbv/bbv-types.scm 769 */
													obj_t BgL_arg1804z00_5556;

													BgL_arg1804z00_5556 = (BgL_oclassz00_5553);
													BgL_odepthz00_5555 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5556);
												}
												if ((3L < BgL_odepthz00_5555))
													{	/* SawBbv/bbv-types.scm 769 */
														obj_t BgL_arg1802z00_5558;

														{	/* SawBbv/bbv-types.scm 769 */
															obj_t BgL_arg1803z00_5559;

															BgL_arg1803z00_5559 = (BgL_oclassz00_5553);
															BgL_arg1802z00_5558 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5559,
																3L);
														}
														BgL_res3605z00_5570 =
															(BgL_arg1802z00_5558 == BgL_classz00_5537);
													}
												else
													{	/* SawBbv/bbv-types.scm 769 */
														BgL_res3605z00_5570 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3605z00_5570;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-ifeq? */
	obj_t BGl_z62rtl_inszd2ifeqzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8219,
		obj_t BgL_iz00_8220)
	{
		{	/* SawBbv/bbv-types.scm 767 */
			return
				BBOOL(BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8220)));
		}

	}



/* rtl_ins-ifne? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_170)
	{
		{	/* SawBbv/bbv-types.scm 774 */
			{	/* SawBbv/bbv-types.scm 776 */
				BgL_rtl_funz00_bglt BgL_arg2273z00_5572;

				BgL_arg2273z00_5572 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_170))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 776 */
					obj_t BgL_classz00_5573;

					BgL_classz00_5573 = BGl_rtl_ifnez00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 776 */
						BgL_objectz00_bglt BgL_arg1807z00_5575;

						{	/* SawBbv/bbv-types.scm 776 */
							obj_t BgL_tmpz00_11811;

							BgL_tmpz00_11811 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2273z00_5572));
							BgL_arg1807z00_5575 = (BgL_objectz00_bglt) (BgL_tmpz00_11811);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 776 */
								long BgL_idxz00_5581;

								BgL_idxz00_5581 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5575);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5581 + 3L)) == BgL_classz00_5573);
							}
						else
							{	/* SawBbv/bbv-types.scm 776 */
								bool_t BgL_res3606z00_5606;

								{	/* SawBbv/bbv-types.scm 776 */
									obj_t BgL_oclassz00_5589;

									{	/* SawBbv/bbv-types.scm 776 */
										obj_t BgL_arg1815z00_5597;
										long BgL_arg1816z00_5598;

										BgL_arg1815z00_5597 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 776 */
											long BgL_arg1817z00_5599;

											BgL_arg1817z00_5599 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5575);
											BgL_arg1816z00_5598 = (BgL_arg1817z00_5599 - OBJECT_TYPE);
										}
										BgL_oclassz00_5589 =
											VECTOR_REF(BgL_arg1815z00_5597, BgL_arg1816z00_5598);
									}
									{	/* SawBbv/bbv-types.scm 776 */
										bool_t BgL__ortest_1115z00_5590;

										BgL__ortest_1115z00_5590 =
											(BgL_classz00_5573 == BgL_oclassz00_5589);
										if (BgL__ortest_1115z00_5590)
											{	/* SawBbv/bbv-types.scm 776 */
												BgL_res3606z00_5606 = BgL__ortest_1115z00_5590;
											}
										else
											{	/* SawBbv/bbv-types.scm 776 */
												long BgL_odepthz00_5591;

												{	/* SawBbv/bbv-types.scm 776 */
													obj_t BgL_arg1804z00_5592;

													BgL_arg1804z00_5592 = (BgL_oclassz00_5589);
													BgL_odepthz00_5591 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5592);
												}
												if ((3L < BgL_odepthz00_5591))
													{	/* SawBbv/bbv-types.scm 776 */
														obj_t BgL_arg1802z00_5594;

														{	/* SawBbv/bbv-types.scm 776 */
															obj_t BgL_arg1803z00_5595;

															BgL_arg1803z00_5595 = (BgL_oclassz00_5589);
															BgL_arg1802z00_5594 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5595,
																3L);
														}
														BgL_res3606z00_5606 =
															(BgL_arg1802z00_5594 == BgL_classz00_5573);
													}
												else
													{	/* SawBbv/bbv-types.scm 776 */
														BgL_res3606z00_5606 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3606z00_5606;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-ifne? */
	obj_t BGl_z62rtl_inszd2ifnezf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8221,
		obj_t BgL_iz00_8222)
	{
		{	/* SawBbv/bbv-types.scm 774 */
			return
				BBOOL(BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8222)));
		}

	}



/* rtl_ins-call? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_171)
	{
		{	/* SawBbv/bbv-types.scm 781 */
			{	/* SawBbv/bbv-types.scm 783 */
				BgL_rtl_funz00_bglt BgL_arg2274z00_5608;

				BgL_arg2274z00_5608 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_171))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 783 */
					obj_t BgL_classz00_5609;

					BgL_classz00_5609 = BGl_rtl_callz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 783 */
						BgL_objectz00_bglt BgL_arg1807z00_5611;

						{	/* SawBbv/bbv-types.scm 783 */
							obj_t BgL_tmpz00_11838;

							BgL_tmpz00_11838 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2274z00_5608));
							BgL_arg1807z00_5611 = (BgL_objectz00_bglt) (BgL_tmpz00_11838);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 783 */
								long BgL_idxz00_5617;

								BgL_idxz00_5617 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5611);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5617 + 2L)) == BgL_classz00_5609);
							}
						else
							{	/* SawBbv/bbv-types.scm 783 */
								bool_t BgL_res3607z00_5642;

								{	/* SawBbv/bbv-types.scm 783 */
									obj_t BgL_oclassz00_5625;

									{	/* SawBbv/bbv-types.scm 783 */
										obj_t BgL_arg1815z00_5633;
										long BgL_arg1816z00_5634;

										BgL_arg1815z00_5633 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 783 */
											long BgL_arg1817z00_5635;

											BgL_arg1817z00_5635 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5611);
											BgL_arg1816z00_5634 = (BgL_arg1817z00_5635 - OBJECT_TYPE);
										}
										BgL_oclassz00_5625 =
											VECTOR_REF(BgL_arg1815z00_5633, BgL_arg1816z00_5634);
									}
									{	/* SawBbv/bbv-types.scm 783 */
										bool_t BgL__ortest_1115z00_5626;

										BgL__ortest_1115z00_5626 =
											(BgL_classz00_5609 == BgL_oclassz00_5625);
										if (BgL__ortest_1115z00_5626)
											{	/* SawBbv/bbv-types.scm 783 */
												BgL_res3607z00_5642 = BgL__ortest_1115z00_5626;
											}
										else
											{	/* SawBbv/bbv-types.scm 783 */
												long BgL_odepthz00_5627;

												{	/* SawBbv/bbv-types.scm 783 */
													obj_t BgL_arg1804z00_5628;

													BgL_arg1804z00_5628 = (BgL_oclassz00_5625);
													BgL_odepthz00_5627 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5628);
												}
												if ((2L < BgL_odepthz00_5627))
													{	/* SawBbv/bbv-types.scm 783 */
														obj_t BgL_arg1802z00_5630;

														{	/* SawBbv/bbv-types.scm 783 */
															obj_t BgL_arg1803z00_5631;

															BgL_arg1803z00_5631 = (BgL_oclassz00_5625);
															BgL_arg1802z00_5630 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5631,
																2L);
														}
														BgL_res3607z00_5642 =
															(BgL_arg1802z00_5630 == BgL_classz00_5609);
													}
												else
													{	/* SawBbv/bbv-types.scm 783 */
														BgL_res3607z00_5642 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3607z00_5642;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-call? */
	obj_t BGl_z62rtl_inszd2callzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8223,
		obj_t BgL_iz00_8224)
	{
		{	/* SawBbv/bbv-types.scm 781 */
			return
				BBOOL(BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8224)));
		}

	}



/* rtl_ins-vlen? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2vlenzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_172)
	{
		{	/* SawBbv/bbv-types.scm 788 */
			{	/* SawBbv/bbv-types.scm 790 */
				BgL_rtl_funz00_bglt BgL_arg2275z00_5644;

				BgL_arg2275z00_5644 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_172))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 790 */
					obj_t BgL_classz00_5645;

					BgL_classz00_5645 = BGl_rtl_vlengthz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 790 */
						BgL_objectz00_bglt BgL_arg1807z00_5647;

						{	/* SawBbv/bbv-types.scm 790 */
							obj_t BgL_tmpz00_11865;

							BgL_tmpz00_11865 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2275z00_5644));
							BgL_arg1807z00_5647 = (BgL_objectz00_bglt) (BgL_tmpz00_11865);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 790 */
								long BgL_idxz00_5653;

								BgL_idxz00_5653 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5647);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5653 + 3L)) == BgL_classz00_5645);
							}
						else
							{	/* SawBbv/bbv-types.scm 790 */
								bool_t BgL_res3608z00_5678;

								{	/* SawBbv/bbv-types.scm 790 */
									obj_t BgL_oclassz00_5661;

									{	/* SawBbv/bbv-types.scm 790 */
										obj_t BgL_arg1815z00_5669;
										long BgL_arg1816z00_5670;

										BgL_arg1815z00_5669 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 790 */
											long BgL_arg1817z00_5671;

											BgL_arg1817z00_5671 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5647);
											BgL_arg1816z00_5670 = (BgL_arg1817z00_5671 - OBJECT_TYPE);
										}
										BgL_oclassz00_5661 =
											VECTOR_REF(BgL_arg1815z00_5669, BgL_arg1816z00_5670);
									}
									{	/* SawBbv/bbv-types.scm 790 */
										bool_t BgL__ortest_1115z00_5662;

										BgL__ortest_1115z00_5662 =
											(BgL_classz00_5645 == BgL_oclassz00_5661);
										if (BgL__ortest_1115z00_5662)
											{	/* SawBbv/bbv-types.scm 790 */
												BgL_res3608z00_5678 = BgL__ortest_1115z00_5662;
											}
										else
											{	/* SawBbv/bbv-types.scm 790 */
												long BgL_odepthz00_5663;

												{	/* SawBbv/bbv-types.scm 790 */
													obj_t BgL_arg1804z00_5664;

													BgL_arg1804z00_5664 = (BgL_oclassz00_5661);
													BgL_odepthz00_5663 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5664);
												}
												if ((3L < BgL_odepthz00_5663))
													{	/* SawBbv/bbv-types.scm 790 */
														obj_t BgL_arg1802z00_5666;

														{	/* SawBbv/bbv-types.scm 790 */
															obj_t BgL_arg1803z00_5667;

															BgL_arg1803z00_5667 = (BgL_oclassz00_5661);
															BgL_arg1802z00_5666 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5667,
																3L);
														}
														BgL_res3608z00_5678 =
															(BgL_arg1802z00_5666 == BgL_classz00_5645);
													}
												else
													{	/* SawBbv/bbv-types.scm 790 */
														BgL_res3608z00_5678 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3608z00_5678;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-vlen? */
	obj_t BGl_z62rtl_inszd2vlenzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8225,
		obj_t BgL_iz00_8226)
	{
		{	/* SawBbv/bbv-types.scm 788 */
			return
				BBOOL(BGl_rtl_inszd2vlenzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8226)));
		}

	}



/* rtl_ins-strlen? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2strlenzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_173)
	{
		{	/* SawBbv/bbv-types.scm 795 */
			{	/* SawBbv/bbv-types.scm 797 */
				bool_t BgL_test4328z00_11891;

				{	/* SawBbv/bbv-types.scm 797 */
					BgL_rtl_funz00_bglt BgL_arg2280z00_2752;

					BgL_arg2280z00_2752 =
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_173))->BgL_funz00);
					{	/* SawBbv/bbv-types.scm 797 */
						obj_t BgL_classz00_5679;

						BgL_classz00_5679 = BGl_rtl_callz00zzsaw_defsz00;
						{	/* SawBbv/bbv-types.scm 797 */
							BgL_objectz00_bglt BgL_arg1807z00_5681;

							{	/* SawBbv/bbv-types.scm 797 */
								obj_t BgL_tmpz00_11893;

								BgL_tmpz00_11893 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2280z00_2752));
								BgL_arg1807z00_5681 = (BgL_objectz00_bglt) (BgL_tmpz00_11893);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 797 */
									long BgL_idxz00_5687;

									BgL_idxz00_5687 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5681);
									BgL_test4328z00_11891 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5687 + 2L)) == BgL_classz00_5679);
								}
							else
								{	/* SawBbv/bbv-types.scm 797 */
									bool_t BgL_res3609z00_5712;

									{	/* SawBbv/bbv-types.scm 797 */
										obj_t BgL_oclassz00_5695;

										{	/* SawBbv/bbv-types.scm 797 */
											obj_t BgL_arg1815z00_5703;
											long BgL_arg1816z00_5704;

											BgL_arg1815z00_5703 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 797 */
												long BgL_arg1817z00_5705;

												BgL_arg1817z00_5705 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5681);
												BgL_arg1816z00_5704 =
													(BgL_arg1817z00_5705 - OBJECT_TYPE);
											}
											BgL_oclassz00_5695 =
												VECTOR_REF(BgL_arg1815z00_5703, BgL_arg1816z00_5704);
										}
										{	/* SawBbv/bbv-types.scm 797 */
											bool_t BgL__ortest_1115z00_5696;

											BgL__ortest_1115z00_5696 =
												(BgL_classz00_5679 == BgL_oclassz00_5695);
											if (BgL__ortest_1115z00_5696)
												{	/* SawBbv/bbv-types.scm 797 */
													BgL_res3609z00_5712 = BgL__ortest_1115z00_5696;
												}
											else
												{	/* SawBbv/bbv-types.scm 797 */
													long BgL_odepthz00_5697;

													{	/* SawBbv/bbv-types.scm 797 */
														obj_t BgL_arg1804z00_5698;

														BgL_arg1804z00_5698 = (BgL_oclassz00_5695);
														BgL_odepthz00_5697 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5698);
													}
													if ((2L < BgL_odepthz00_5697))
														{	/* SawBbv/bbv-types.scm 797 */
															obj_t BgL_arg1802z00_5700;

															{	/* SawBbv/bbv-types.scm 797 */
																obj_t BgL_arg1803z00_5701;

																BgL_arg1803z00_5701 = (BgL_oclassz00_5695);
																BgL_arg1802z00_5700 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5701,
																	2L);
															}
															BgL_res3609z00_5712 =
																(BgL_arg1802z00_5700 == BgL_classz00_5679);
														}
													else
														{	/* SawBbv/bbv-types.scm 797 */
															BgL_res3609z00_5712 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4328z00_11891 = BgL_res3609z00_5712;
								}
						}
					}
				}
				if (BgL_test4328z00_11891)
					{	/* SawBbv/bbv-types.scm 798 */
						BgL_rtl_callz00_bglt BgL_i1344z00_2750;

						BgL_i1344z00_2750 =
							((BgL_rtl_callz00_bglt)
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_173))->BgL_funz00));
						{	/* SawBbv/bbv-types.scm 799 */
							BgL_globalz00_bglt BgL_arg2279z00_2751;

							BgL_arg2279z00_2751 =
								(((BgL_rtl_callz00_bglt) COBJECT(BgL_i1344z00_2750))->
								BgL_varz00);
							return (((obj_t) BgL_arg2279z00_2751) ==
								BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2);
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 797 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins-strlen? */
	obj_t BGl_z62rtl_inszd2strlenzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8227,
		obj_t BgL_iz00_8228)
	{
		{	/* SawBbv/bbv-types.scm 795 */
			return
				BBOOL(BGl_rtl_inszd2strlenzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8228)));
		}

	}



/* rtl_ins-loadi? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2loadizf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_174)
	{
		{	/* SawBbv/bbv-types.scm 804 */
			{	/* SawBbv/bbv-types.scm 806 */
				BgL_rtl_funz00_bglt BgL_arg2281z00_5714;

				BgL_arg2281z00_5714 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_174))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 806 */
					obj_t BgL_classz00_5715;

					BgL_classz00_5715 = BGl_rtl_loadiz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 806 */
						BgL_objectz00_bglt BgL_arg1807z00_5717;

						{	/* SawBbv/bbv-types.scm 806 */
							obj_t BgL_tmpz00_11925;

							BgL_tmpz00_11925 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2281z00_5714));
							BgL_arg1807z00_5717 = (BgL_objectz00_bglt) (BgL_tmpz00_11925);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 806 */
								long BgL_idxz00_5723;

								BgL_idxz00_5723 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5717);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5723 + 3L)) == BgL_classz00_5715);
							}
						else
							{	/* SawBbv/bbv-types.scm 806 */
								bool_t BgL_res3610z00_5748;

								{	/* SawBbv/bbv-types.scm 806 */
									obj_t BgL_oclassz00_5731;

									{	/* SawBbv/bbv-types.scm 806 */
										obj_t BgL_arg1815z00_5739;
										long BgL_arg1816z00_5740;

										BgL_arg1815z00_5739 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 806 */
											long BgL_arg1817z00_5741;

											BgL_arg1817z00_5741 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5717);
											BgL_arg1816z00_5740 = (BgL_arg1817z00_5741 - OBJECT_TYPE);
										}
										BgL_oclassz00_5731 =
											VECTOR_REF(BgL_arg1815z00_5739, BgL_arg1816z00_5740);
									}
									{	/* SawBbv/bbv-types.scm 806 */
										bool_t BgL__ortest_1115z00_5732;

										BgL__ortest_1115z00_5732 =
											(BgL_classz00_5715 == BgL_oclassz00_5731);
										if (BgL__ortest_1115z00_5732)
											{	/* SawBbv/bbv-types.scm 806 */
												BgL_res3610z00_5748 = BgL__ortest_1115z00_5732;
											}
										else
											{	/* SawBbv/bbv-types.scm 806 */
												long BgL_odepthz00_5733;

												{	/* SawBbv/bbv-types.scm 806 */
													obj_t BgL_arg1804z00_5734;

													BgL_arg1804z00_5734 = (BgL_oclassz00_5731);
													BgL_odepthz00_5733 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5734);
												}
												if ((3L < BgL_odepthz00_5733))
													{	/* SawBbv/bbv-types.scm 806 */
														obj_t BgL_arg1802z00_5736;

														{	/* SawBbv/bbv-types.scm 806 */
															obj_t BgL_arg1803z00_5737;

															BgL_arg1803z00_5737 = (BgL_oclassz00_5731);
															BgL_arg1802z00_5736 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5737,
																3L);
														}
														BgL_res3610z00_5748 =
															(BgL_arg1802z00_5736 == BgL_classz00_5715);
													}
												else
													{	/* SawBbv/bbv-types.scm 806 */
														BgL_res3610z00_5748 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3610z00_5748;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-loadi? */
	obj_t BGl_z62rtl_inszd2loadizf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8229,
		obj_t BgL_iz00_8230)
	{
		{	/* SawBbv/bbv-types.scm 804 */
			return
				BBOOL(BGl_rtl_inszd2loadizf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8230)));
		}

	}



/* rtl_ins-loadg? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2loadgzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_175)
	{
		{	/* SawBbv/bbv-types.scm 811 */
			{	/* SawBbv/bbv-types.scm 813 */
				BgL_rtl_funz00_bglt BgL_arg2282z00_5750;

				BgL_arg2282z00_5750 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_175))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 813 */
					obj_t BgL_classz00_5751;

					BgL_classz00_5751 = BGl_rtl_loadgz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 813 */
						BgL_objectz00_bglt BgL_arg1807z00_5753;

						{	/* SawBbv/bbv-types.scm 813 */
							obj_t BgL_tmpz00_11952;

							BgL_tmpz00_11952 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2282z00_5750));
							BgL_arg1807z00_5753 = (BgL_objectz00_bglt) (BgL_tmpz00_11952);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 813 */
								long BgL_idxz00_5759;

								BgL_idxz00_5759 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5753);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5759 + 3L)) == BgL_classz00_5751);
							}
						else
							{	/* SawBbv/bbv-types.scm 813 */
								bool_t BgL_res3611z00_5784;

								{	/* SawBbv/bbv-types.scm 813 */
									obj_t BgL_oclassz00_5767;

									{	/* SawBbv/bbv-types.scm 813 */
										obj_t BgL_arg1815z00_5775;
										long BgL_arg1816z00_5776;

										BgL_arg1815z00_5775 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 813 */
											long BgL_arg1817z00_5777;

											BgL_arg1817z00_5777 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5753);
											BgL_arg1816z00_5776 = (BgL_arg1817z00_5777 - OBJECT_TYPE);
										}
										BgL_oclassz00_5767 =
											VECTOR_REF(BgL_arg1815z00_5775, BgL_arg1816z00_5776);
									}
									{	/* SawBbv/bbv-types.scm 813 */
										bool_t BgL__ortest_1115z00_5768;

										BgL__ortest_1115z00_5768 =
											(BgL_classz00_5751 == BgL_oclassz00_5767);
										if (BgL__ortest_1115z00_5768)
											{	/* SawBbv/bbv-types.scm 813 */
												BgL_res3611z00_5784 = BgL__ortest_1115z00_5768;
											}
										else
											{	/* SawBbv/bbv-types.scm 813 */
												long BgL_odepthz00_5769;

												{	/* SawBbv/bbv-types.scm 813 */
													obj_t BgL_arg1804z00_5770;

													BgL_arg1804z00_5770 = (BgL_oclassz00_5767);
													BgL_odepthz00_5769 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5770);
												}
												if ((3L < BgL_odepthz00_5769))
													{	/* SawBbv/bbv-types.scm 813 */
														obj_t BgL_arg1802z00_5772;

														{	/* SawBbv/bbv-types.scm 813 */
															obj_t BgL_arg1803z00_5773;

															BgL_arg1803z00_5773 = (BgL_oclassz00_5767);
															BgL_arg1802z00_5772 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5773,
																3L);
														}
														BgL_res3611z00_5784 =
															(BgL_arg1802z00_5772 == BgL_classz00_5751);
													}
												else
													{	/* SawBbv/bbv-types.scm 813 */
														BgL_res3611z00_5784 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3611z00_5784;
							}
					}
				}
			}
		}

	}



/* &rtl_ins-loadg? */
	obj_t BGl_z62rtl_inszd2loadgzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8231,
		obj_t BgL_iz00_8232)
	{
		{	/* SawBbv/bbv-types.scm 811 */
			return
				BBOOL(BGl_rtl_inszd2loadgzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8232)));
		}

	}



/* rtl_ins-boolean? */
	bool_t BGl_rtl_inszd2booleanzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_insz00_176, bool_t BgL_boolz00_177)
	{
		{	/* SawBbv/bbv-types.scm 818 */
			{	/* SawBbv/bbv-types.scm 820 */
				bool_t BgL_test4338z00_11978;

				{	/* SawBbv/bbv-types.scm 820 */
					BgL_rtl_funz00_bglt BgL_arg2287z00_2763;

					BgL_arg2287z00_2763 =
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_176))->BgL_funz00);
					{	/* SawBbv/bbv-types.scm 820 */
						obj_t BgL_classz00_5785;

						BgL_classz00_5785 = BGl_rtl_loadiz00zzsaw_defsz00;
						{	/* SawBbv/bbv-types.scm 820 */
							BgL_objectz00_bglt BgL_arg1807z00_5787;

							{	/* SawBbv/bbv-types.scm 820 */
								obj_t BgL_tmpz00_11980;

								BgL_tmpz00_11980 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2287z00_2763));
								BgL_arg1807z00_5787 = (BgL_objectz00_bglt) (BgL_tmpz00_11980);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 820 */
									long BgL_idxz00_5793;

									BgL_idxz00_5793 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5787);
									BgL_test4338z00_11978 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5793 + 3L)) == BgL_classz00_5785);
								}
							else
								{	/* SawBbv/bbv-types.scm 820 */
									bool_t BgL_res3612z00_5818;

									{	/* SawBbv/bbv-types.scm 820 */
										obj_t BgL_oclassz00_5801;

										{	/* SawBbv/bbv-types.scm 820 */
											obj_t BgL_arg1815z00_5809;
											long BgL_arg1816z00_5810;

											BgL_arg1815z00_5809 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 820 */
												long BgL_arg1817z00_5811;

												BgL_arg1817z00_5811 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5787);
												BgL_arg1816z00_5810 =
													(BgL_arg1817z00_5811 - OBJECT_TYPE);
											}
											BgL_oclassz00_5801 =
												VECTOR_REF(BgL_arg1815z00_5809, BgL_arg1816z00_5810);
										}
										{	/* SawBbv/bbv-types.scm 820 */
											bool_t BgL__ortest_1115z00_5802;

											BgL__ortest_1115z00_5802 =
												(BgL_classz00_5785 == BgL_oclassz00_5801);
											if (BgL__ortest_1115z00_5802)
												{	/* SawBbv/bbv-types.scm 820 */
													BgL_res3612z00_5818 = BgL__ortest_1115z00_5802;
												}
											else
												{	/* SawBbv/bbv-types.scm 820 */
													long BgL_odepthz00_5803;

													{	/* SawBbv/bbv-types.scm 820 */
														obj_t BgL_arg1804z00_5804;

														BgL_arg1804z00_5804 = (BgL_oclassz00_5801);
														BgL_odepthz00_5803 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5804);
													}
													if ((3L < BgL_odepthz00_5803))
														{	/* SawBbv/bbv-types.scm 820 */
															obj_t BgL_arg1802z00_5806;

															{	/* SawBbv/bbv-types.scm 820 */
																obj_t BgL_arg1803z00_5807;

																BgL_arg1803z00_5807 = (BgL_oclassz00_5801);
																BgL_arg1802z00_5806 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5807,
																	3L);
															}
															BgL_res3612z00_5818 =
																(BgL_arg1802z00_5806 == BgL_classz00_5785);
														}
													else
														{	/* SawBbv/bbv-types.scm 820 */
															BgL_res3612z00_5818 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4338z00_11978 = BgL_res3612z00_5818;
								}
						}
					}
				}
				if (BgL_test4338z00_11978)
					{	/* SawBbv/bbv-types.scm 820 */
						return
							(
							(((BgL_atomz00_bglt) COBJECT(
										((BgL_atomz00_bglt)
											((BgL_literalz00_bglt)
												(((BgL_rtl_loadiz00_bglt) COBJECT(
															((BgL_rtl_loadiz00_bglt)
																(((BgL_rtl_insz00_bglt)
																		COBJECT(BgL_insz00_176))->BgL_funz00))))->
													BgL_constantz00)))))->BgL_valuez00) ==
							BBOOL(BgL_boolz00_177));
					}
				else
					{	/* SawBbv/bbv-types.scm 820 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* rtl_ins-true? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2truezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_insz00_178)
	{
		{	/* SawBbv/bbv-types.scm 825 */
			return
				BGl_rtl_inszd2booleanzf3z21zzsaw_bbvzd2typeszd2(BgL_insz00_178,
				((bool_t) 1));
		}

	}



/* &rtl_ins-true? */
	obj_t BGl_z62rtl_inszd2truezf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8233,
		obj_t BgL_insz00_8234)
	{
		{	/* SawBbv/bbv-types.scm 825 */
			return
				BBOOL(BGl_rtl_inszd2truezf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_insz00_8234)));
		}

	}



/* rtl_ins-false? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2falsezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_insz00_179)
	{
		{	/* SawBbv/bbv-types.scm 828 */
			return
				BGl_rtl_inszd2booleanzf3z21zzsaw_bbvzd2typeszd2(BgL_insz00_179,
				((bool_t) 0));
		}

	}



/* &rtl_ins-false? */
	obj_t BGl_z62rtl_inszd2falsezf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8235,
		obj_t BgL_insz00_8236)
	{
		{	/* SawBbv/bbv-types.scm 828 */
			return
				BBOOL(BGl_rtl_inszd2falsezf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_insz00_8236)));
		}

	}



/* rtl_ins-return? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2returnzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_180)
	{
		{	/* SawBbv/bbv-types.scm 834 */
			{	/* SawBbv/bbv-types.scm 836 */
				BgL_rtl_funz00_bglt BgL_arg2288z00_5820;

				BgL_arg2288z00_5820 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_180))->BgL_funz00);
				{	/* SawBbv/bbv-types.scm 836 */
					obj_t BgL_classz00_5821;

					BgL_classz00_5821 = BGl_rtl_returnz00zzsaw_defsz00;
					{	/* SawBbv/bbv-types.scm 836 */
						BgL_objectz00_bglt BgL_arg1807z00_5823;

						{	/* SawBbv/bbv-types.scm 836 */
							obj_t BgL_tmpz00_12020;

							BgL_tmpz00_12020 =
								((obj_t) ((BgL_objectz00_bglt) BgL_arg2288z00_5820));
							BgL_arg1807z00_5823 = (BgL_objectz00_bglt) (BgL_tmpz00_12020);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.scm 836 */
								long BgL_idxz00_5829;

								BgL_idxz00_5829 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5823);
								{	/* SawBbv/bbv-types.scm 836 */
									obj_t BgL_arg1800z00_5830;

									BgL_arg1800z00_5830 =
										VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5829 + 3L));
									return BBOOL((BgL_arg1800z00_5830 == BgL_classz00_5821));
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 836 */
								bool_t BgL_res3613z00_5854;

								{	/* SawBbv/bbv-types.scm 836 */
									obj_t BgL_oclassz00_5837;

									{	/* SawBbv/bbv-types.scm 836 */
										obj_t BgL_arg1815z00_5845;
										long BgL_arg1816z00_5846;

										BgL_arg1815z00_5845 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.scm 836 */
											long BgL_arg1817z00_5847;

											BgL_arg1817z00_5847 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5823);
											BgL_arg1816z00_5846 = (BgL_arg1817z00_5847 - OBJECT_TYPE);
										}
										BgL_oclassz00_5837 =
											VECTOR_REF(BgL_arg1815z00_5845, BgL_arg1816z00_5846);
									}
									{	/* SawBbv/bbv-types.scm 836 */
										bool_t BgL__ortest_1115z00_5838;

										BgL__ortest_1115z00_5838 =
											(BgL_classz00_5821 == BgL_oclassz00_5837);
										if (BgL__ortest_1115z00_5838)
											{	/* SawBbv/bbv-types.scm 836 */
												BgL_res3613z00_5854 = BgL__ortest_1115z00_5838;
											}
										else
											{	/* SawBbv/bbv-types.scm 836 */
												long BgL_odepthz00_5839;

												{	/* SawBbv/bbv-types.scm 836 */
													obj_t BgL_arg1804z00_5840;

													BgL_arg1804z00_5840 = (BgL_oclassz00_5837);
													BgL_odepthz00_5839 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5840);
												}
												if ((3L < BgL_odepthz00_5839))
													{	/* SawBbv/bbv-types.scm 836 */
														obj_t BgL_arg1802z00_5842;

														{	/* SawBbv/bbv-types.scm 836 */
															obj_t BgL_arg1803z00_5843;

															BgL_arg1803z00_5843 = (BgL_oclassz00_5837);
															BgL_arg1802z00_5842 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5843,
																3L);
														}
														BgL_res3613z00_5854 =
															(BgL_arg1802z00_5842 == BgL_classz00_5821);
													}
												else
													{	/* SawBbv/bbv-types.scm 836 */
														BgL_res3613z00_5854 = ((bool_t) 0);
													}
											}
									}
								}
								return BBOOL(BgL_res3613z00_5854);
							}
					}
				}
			}
		}

	}



/* &rtl_ins-return? */
	obj_t BGl_z62rtl_inszd2returnzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8237,
		obj_t BgL_iz00_8238)
	{
		{	/* SawBbv/bbv-types.scm 834 */
			return
				BGl_rtl_inszd2returnzf3z21zzsaw_bbvzd2typeszd2(
				((BgL_rtl_insz00_bglt) BgL_iz00_8238));
		}

	}



/* rtl_ins-fxovop? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2fxovopzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_181)
	{
		{	/* SawBbv/bbv-types.scm 841 */
			{
				obj_t BgL_iz00_2791;

				{	/* SawBbv/bbv-types.scm 862 */
					bool_t BgL_test4345z00_12047;

					{	/* SawBbv/bbv-types.scm 776 */
						BgL_rtl_funz00_bglt BgL_arg2273z00_6155;

						BgL_arg2273z00_6155 =
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_181))->BgL_funz00);
						{	/* SawBbv/bbv-types.scm 776 */
							obj_t BgL_classz00_6156;

							BgL_classz00_6156 = BGl_rtl_ifnez00zzsaw_defsz00;
							{	/* SawBbv/bbv-types.scm 776 */
								BgL_objectz00_bglt BgL_arg1807z00_6158;

								{	/* SawBbv/bbv-types.scm 776 */
									obj_t BgL_tmpz00_12049;

									BgL_tmpz00_12049 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg2273z00_6155));
									BgL_arg1807z00_6158 = (BgL_objectz00_bglt) (BgL_tmpz00_12049);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 776 */
										long BgL_idxz00_6164;

										BgL_idxz00_6164 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6158);
										BgL_test4345z00_12047 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6164 + 3L)) == BgL_classz00_6156);
									}
								else
									{	/* SawBbv/bbv-types.scm 776 */
										bool_t BgL_res3622z00_6189;

										{	/* SawBbv/bbv-types.scm 776 */
											obj_t BgL_oclassz00_6172;

											{	/* SawBbv/bbv-types.scm 776 */
												obj_t BgL_arg1815z00_6180;
												long BgL_arg1816z00_6181;

												BgL_arg1815z00_6180 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 776 */
													long BgL_arg1817z00_6182;

													BgL_arg1817z00_6182 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6158);
													BgL_arg1816z00_6181 =
														(BgL_arg1817z00_6182 - OBJECT_TYPE);
												}
												BgL_oclassz00_6172 =
													VECTOR_REF(BgL_arg1815z00_6180, BgL_arg1816z00_6181);
											}
											{	/* SawBbv/bbv-types.scm 776 */
												bool_t BgL__ortest_1115z00_6173;

												BgL__ortest_1115z00_6173 =
													(BgL_classz00_6156 == BgL_oclassz00_6172);
												if (BgL__ortest_1115z00_6173)
													{	/* SawBbv/bbv-types.scm 776 */
														BgL_res3622z00_6189 = BgL__ortest_1115z00_6173;
													}
												else
													{	/* SawBbv/bbv-types.scm 776 */
														long BgL_odepthz00_6174;

														{	/* SawBbv/bbv-types.scm 776 */
															obj_t BgL_arg1804z00_6175;

															BgL_arg1804z00_6175 = (BgL_oclassz00_6172);
															BgL_odepthz00_6174 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6175);
														}
														if ((3L < BgL_odepthz00_6174))
															{	/* SawBbv/bbv-types.scm 776 */
																obj_t BgL_arg1802z00_6177;

																{	/* SawBbv/bbv-types.scm 776 */
																	obj_t BgL_arg1803z00_6178;

																	BgL_arg1803z00_6178 = (BgL_oclassz00_6172);
																	BgL_arg1802z00_6177 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6178,
																		3L);
																}
																BgL_res3622z00_6189 =
																	(BgL_arg1802z00_6177 == BgL_classz00_6156);
															}
														else
															{	/* SawBbv/bbv-types.scm 776 */
																BgL_res3622z00_6189 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4345z00_12047 = BgL_res3622z00_6189;
									}
							}
						}
					}
					if (BgL_test4345z00_12047)
						{	/* SawBbv/bbv-types.scm 864 */
							bool_t BgL_test4349z00_12072;

							if (NULLP(
									(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_181))->BgL_argsz00)))
								{	/* SawBbv/bbv-types.scm 864 */
									BgL_test4349z00_12072 = ((bool_t) 0);
								}
							else
								{	/* SawBbv/bbv-types.scm 864 */
									BgL_test4349z00_12072 =
										NULLP(CDR(
											(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_181))->
												BgL_argsz00)));
								}
							if (BgL_test4349z00_12072)
								{	/* SawBbv/bbv-types.scm 864 */
									BgL_iz00_2791 =
										CAR(
										(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_181))->
											BgL_argsz00));
									{	/* SawBbv/bbv-types.scm 851 */
										bool_t BgL_test4351z00_12079;

										{	/* SawBbv/bbv-types.scm 851 */
											bool_t BgL_test4352z00_12080;

											{	/* SawBbv/bbv-types.scm 851 */
												obj_t BgL_classz00_5991;

												BgL_classz00_5991 = BGl_rtl_insz00zzsaw_defsz00;
												if (BGL_OBJECTP(BgL_iz00_2791))
													{	/* SawBbv/bbv-types.scm 851 */
														BgL_objectz00_bglt BgL_arg1807z00_5993;

														BgL_arg1807z00_5993 =
															(BgL_objectz00_bglt) (BgL_iz00_2791);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-types.scm 851 */
																long BgL_idxz00_5999;

																BgL_idxz00_5999 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5993);
																BgL_test4352z00_12080 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5999 + 1L)) ==
																	BgL_classz00_5991);
															}
														else
															{	/* SawBbv/bbv-types.scm 851 */
																bool_t BgL_res3618z00_6024;

																{	/* SawBbv/bbv-types.scm 851 */
																	obj_t BgL_oclassz00_6007;

																	{	/* SawBbv/bbv-types.scm 851 */
																		obj_t BgL_arg1815z00_6015;
																		long BgL_arg1816z00_6016;

																		BgL_arg1815z00_6015 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-types.scm 851 */
																			long BgL_arg1817z00_6017;

																			BgL_arg1817z00_6017 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5993);
																			BgL_arg1816z00_6016 =
																				(BgL_arg1817z00_6017 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_6007 =
																			VECTOR_REF(BgL_arg1815z00_6015,
																			BgL_arg1816z00_6016);
																	}
																	{	/* SawBbv/bbv-types.scm 851 */
																		bool_t BgL__ortest_1115z00_6008;

																		BgL__ortest_1115z00_6008 =
																			(BgL_classz00_5991 == BgL_oclassz00_6007);
																		if (BgL__ortest_1115z00_6008)
																			{	/* SawBbv/bbv-types.scm 851 */
																				BgL_res3618z00_6024 =
																					BgL__ortest_1115z00_6008;
																			}
																		else
																			{	/* SawBbv/bbv-types.scm 851 */
																				long BgL_odepthz00_6009;

																				{	/* SawBbv/bbv-types.scm 851 */
																					obj_t BgL_arg1804z00_6010;

																					BgL_arg1804z00_6010 =
																						(BgL_oclassz00_6007);
																					BgL_odepthz00_6009 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_6010);
																				}
																				if ((1L < BgL_odepthz00_6009))
																					{	/* SawBbv/bbv-types.scm 851 */
																						obj_t BgL_arg1802z00_6012;

																						{	/* SawBbv/bbv-types.scm 851 */
																							obj_t BgL_arg1803z00_6013;

																							BgL_arg1803z00_6013 =
																								(BgL_oclassz00_6007);
																							BgL_arg1802z00_6012 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_6013, 1L);
																						}
																						BgL_res3618z00_6024 =
																							(BgL_arg1802z00_6012 ==
																							BgL_classz00_5991);
																					}
																				else
																					{	/* SawBbv/bbv-types.scm 851 */
																						BgL_res3618z00_6024 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test4352z00_12080 = BgL_res3618z00_6024;
															}
													}
												else
													{	/* SawBbv/bbv-types.scm 851 */
														BgL_test4352z00_12080 = ((bool_t) 0);
													}
											}
											if (BgL_test4352z00_12080)
												{	/* SawBbv/bbv-types.scm 783 */
													BgL_rtl_funz00_bglt BgL_arg2274z00_6027;

													BgL_arg2274z00_6027 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_iz00_2791)))->
														BgL_funz00);
													{	/* SawBbv/bbv-types.scm 783 */
														obj_t BgL_classz00_6028;

														BgL_classz00_6028 = BGl_rtl_callz00zzsaw_defsz00;
														{	/* SawBbv/bbv-types.scm 783 */
															BgL_objectz00_bglt BgL_arg1807z00_6030;

															{	/* SawBbv/bbv-types.scm 783 */
																obj_t BgL_tmpz00_12105;

																BgL_tmpz00_12105 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_arg2274z00_6027));
																BgL_arg1807z00_6030 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_12105);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawBbv/bbv-types.scm 783 */
																	long BgL_idxz00_6036;

																	BgL_idxz00_6036 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_6030);
																	BgL_test4351z00_12079 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_6036 + 2L)) ==
																		BgL_classz00_6028);
																}
															else
																{	/* SawBbv/bbv-types.scm 783 */
																	bool_t BgL_res3619z00_6061;

																	{	/* SawBbv/bbv-types.scm 783 */
																		obj_t BgL_oclassz00_6044;

																		{	/* SawBbv/bbv-types.scm 783 */
																			obj_t BgL_arg1815z00_6052;
																			long BgL_arg1816z00_6053;

																			BgL_arg1815z00_6052 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawBbv/bbv-types.scm 783 */
																				long BgL_arg1817z00_6054;

																				BgL_arg1817z00_6054 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_6030);
																				BgL_arg1816z00_6053 =
																					(BgL_arg1817z00_6054 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_6044 =
																				VECTOR_REF(BgL_arg1815z00_6052,
																				BgL_arg1816z00_6053);
																		}
																		{	/* SawBbv/bbv-types.scm 783 */
																			bool_t BgL__ortest_1115z00_6045;

																			BgL__ortest_1115z00_6045 =
																				(BgL_classz00_6028 ==
																				BgL_oclassz00_6044);
																			if (BgL__ortest_1115z00_6045)
																				{	/* SawBbv/bbv-types.scm 783 */
																					BgL_res3619z00_6061 =
																						BgL__ortest_1115z00_6045;
																				}
																			else
																				{	/* SawBbv/bbv-types.scm 783 */
																					long BgL_odepthz00_6046;

																					{	/* SawBbv/bbv-types.scm 783 */
																						obj_t BgL_arg1804z00_6047;

																						BgL_arg1804z00_6047 =
																							(BgL_oclassz00_6044);
																						BgL_odepthz00_6046 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_6047);
																					}
																					if ((2L < BgL_odepthz00_6046))
																						{	/* SawBbv/bbv-types.scm 783 */
																							obj_t BgL_arg1802z00_6049;

																							{	/* SawBbv/bbv-types.scm 783 */
																								obj_t BgL_arg1803z00_6050;

																								BgL_arg1803z00_6050 =
																									(BgL_oclassz00_6044);
																								BgL_arg1802z00_6049 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_6050, 2L);
																							}
																							BgL_res3619z00_6061 =
																								(BgL_arg1802z00_6049 ==
																								BgL_classz00_6028);
																						}
																					else
																						{	/* SawBbv/bbv-types.scm 783 */
																							BgL_res3619z00_6061 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test4351z00_12079 = BgL_res3619z00_6061;
																}
														}
													}
												}
											else
												{	/* SawBbv/bbv-types.scm 851 */
													BgL_test4351z00_12079 = ((bool_t) 0);
												}
										}
										if (BgL_test4351z00_12079)
											{	/* SawBbv/bbv-types.scm 853 */
												BgL_rtl_callz00_bglt BgL_i1355z00_2796;

												BgL_i1355z00_2796 =
													((BgL_rtl_callz00_bglt)
													(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_iz00_2791)))->
														BgL_funz00));
												if ((bgl_list_length((((BgL_rtl_insz00_bglt)
																	COBJECT(((BgL_rtl_insz00_bglt)
																			BgL_iz00_2791)))->BgL_argsz00)) == 3L))
													{	/* SawBbv/bbv-types.scm 855 */
														bool_t BgL_test4361z00_12136;

														{	/* SawBbv/bbv-types.scm 855 */
															bool_t BgL__ortest_1360z00_2813;

															{	/* SawBbv/bbv-types.scm 855 */
																BgL_globalz00_bglt BgL_arg2324z00_2817;

																BgL_arg2324z00_2817 =
																	(((BgL_rtl_callz00_bglt)
																		COBJECT(BgL_i1355z00_2796))->BgL_varz00);
																BgL__ortest_1360z00_2813 =
																	(((obj_t) BgL_arg2324z00_2817) ==
																	BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2);
															}
															if (BgL__ortest_1360z00_2813)
																{	/* SawBbv/bbv-types.scm 855 */
																	BgL_test4361z00_12136 =
																		BgL__ortest_1360z00_2813;
																}
															else
																{	/* SawBbv/bbv-types.scm 856 */
																	bool_t BgL__ortest_1361z00_2814;

																	{	/* SawBbv/bbv-types.scm 856 */
																		BgL_globalz00_bglt BgL_arg2323z00_2816;

																		BgL_arg2323z00_2816 =
																			(((BgL_rtl_callz00_bglt)
																				COBJECT(BgL_i1355z00_2796))->
																			BgL_varz00);
																		BgL__ortest_1361z00_2814 =
																			(((obj_t) BgL_arg2323z00_2816) ==
																			BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2);
																	}
																	if (BgL__ortest_1361z00_2814)
																		{	/* SawBbv/bbv-types.scm 856 */
																			BgL_test4361z00_12136 =
																				BgL__ortest_1361z00_2814;
																		}
																	else
																		{	/* SawBbv/bbv-types.scm 857 */
																			BgL_globalz00_bglt BgL_arg2321z00_2815;

																			BgL_arg2321z00_2815 =
																				(((BgL_rtl_callz00_bglt)
																					COBJECT(BgL_i1355z00_2796))->
																				BgL_varz00);
																			BgL_test4361z00_12136 =
																				(((obj_t) BgL_arg2321z00_2815) ==
																				BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2);
																		}
																}
														}
														if (BgL_test4361z00_12136)
															{	/* SawBbv/bbv-types.scm 858 */
																bool_t BgL_test4364z00_12148;

																{	/* SawBbv/bbv-types.scm 858 */
																	bool_t BgL__ortest_1362z00_2808;

																	{	/* SawBbv/bbv-types.scm 858 */
																		obj_t BgL_arg2319z00_2811;

																		{	/* SawBbv/bbv-types.scm 858 */
																			obj_t BgL_pairz00_6063;

																			BgL_pairz00_6063 =
																				(((BgL_rtl_insz00_bglt) COBJECT(
																						((BgL_rtl_insz00_bglt)
																							BgL_iz00_2791)))->BgL_argsz00);
																			BgL_arg2319z00_2811 =
																				CAR(BgL_pairz00_6063);
																		}
																		BgL__ortest_1362z00_2808 =
																			BGl_regzf3ze70z14zzsaw_bbvzd2typeszd2
																			(BgL_arg2319z00_2811);
																	}
																	if (BgL__ortest_1362z00_2808)
																		{	/* SawBbv/bbv-types.scm 858 */
																			BgL_test4364z00_12148 =
																				BgL__ortest_1362z00_2808;
																		}
																	else
																		{	/* SawBbv/bbv-types.scm 858 */
																			obj_t BgL_arg2317z00_2809;

																			{	/* SawBbv/bbv-types.scm 858 */
																				obj_t BgL_pairz00_6064;

																				BgL_pairz00_6064 =
																					(((BgL_rtl_insz00_bglt) COBJECT(
																							((BgL_rtl_insz00_bglt)
																								BgL_iz00_2791)))->BgL_argsz00);
																				BgL_arg2317z00_2809 =
																					CAR(BgL_pairz00_6064);
																			}
																			{	/* SawBbv/bbv-types.scm 806 */
																				BgL_rtl_funz00_bglt BgL_arg2281z00_6067;

																				BgL_arg2281z00_6067 =
																					(((BgL_rtl_insz00_bglt) COBJECT(
																							((BgL_rtl_insz00_bglt)
																								BgL_arg2317z00_2809)))->
																					BgL_funz00);
																				{	/* SawBbv/bbv-types.scm 806 */
																					obj_t BgL_classz00_6068;

																					BgL_classz00_6068 =
																						BGl_rtl_loadiz00zzsaw_defsz00;
																					{	/* SawBbv/bbv-types.scm 806 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_6070;
																						{	/* SawBbv/bbv-types.scm 806 */
																							obj_t BgL_tmpz00_12159;

																							BgL_tmpz00_12159 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_arg2281z00_6067));
																							BgL_arg1807z00_6070 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_12159);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* SawBbv/bbv-types.scm 806 */
																								long BgL_idxz00_6076;

																								BgL_idxz00_6076 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_6070);
																								BgL_test4364z00_12148 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_6076 + 3L)) ==
																									BgL_classz00_6068);
																							}
																						else
																							{	/* SawBbv/bbv-types.scm 806 */
																								bool_t BgL_res3620z00_6101;

																								{	/* SawBbv/bbv-types.scm 806 */
																									obj_t BgL_oclassz00_6084;

																									{	/* SawBbv/bbv-types.scm 806 */
																										obj_t BgL_arg1815z00_6092;
																										long BgL_arg1816z00_6093;

																										BgL_arg1815z00_6092 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* SawBbv/bbv-types.scm 806 */
																											long BgL_arg1817z00_6094;

																											BgL_arg1817z00_6094 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_6070);
																											BgL_arg1816z00_6093 =
																												(BgL_arg1817z00_6094 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_6084 =
																											VECTOR_REF
																											(BgL_arg1815z00_6092,
																											BgL_arg1816z00_6093);
																									}
																									{	/* SawBbv/bbv-types.scm 806 */
																										bool_t
																											BgL__ortest_1115z00_6085;
																										BgL__ortest_1115z00_6085 =
																											(BgL_classz00_6068 ==
																											BgL_oclassz00_6084);
																										if (BgL__ortest_1115z00_6085)
																											{	/* SawBbv/bbv-types.scm 806 */
																												BgL_res3620z00_6101 =
																													BgL__ortest_1115z00_6085;
																											}
																										else
																											{	/* SawBbv/bbv-types.scm 806 */
																												long BgL_odepthz00_6086;

																												{	/* SawBbv/bbv-types.scm 806 */
																													obj_t
																														BgL_arg1804z00_6087;
																													BgL_arg1804z00_6087 =
																														(BgL_oclassz00_6084);
																													BgL_odepthz00_6086 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_6087);
																												}
																												if (
																													(3L <
																														BgL_odepthz00_6086))
																													{	/* SawBbv/bbv-types.scm 806 */
																														obj_t
																															BgL_arg1802z00_6089;
																														{	/* SawBbv/bbv-types.scm 806 */
																															obj_t
																																BgL_arg1803z00_6090;
																															BgL_arg1803z00_6090
																																=
																																(BgL_oclassz00_6084);
																															BgL_arg1802z00_6089
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_6090,
																																3L);
																														}
																														BgL_res3620z00_6101
																															=
																															(BgL_arg1802z00_6089
																															==
																															BgL_classz00_6068);
																													}
																												else
																													{	/* SawBbv/bbv-types.scm 806 */
																														BgL_res3620z00_6101
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test4364z00_12148 =
																									BgL_res3620z00_6101;
																							}
																					}
																				}
																			}
																		}
																}
																if (BgL_test4364z00_12148)
																	{	/* SawBbv/bbv-types.scm 859 */
																		bool_t BgL_test4369z00_12182;

																		{	/* SawBbv/bbv-types.scm 859 */
																			bool_t BgL__ortest_1363z00_2803;

																			{	/* SawBbv/bbv-types.scm 859 */
																				obj_t BgL_arg2315z00_2806;

																				{	/* SawBbv/bbv-types.scm 859 */
																					obj_t BgL_pairz00_6102;

																					BgL_pairz00_6102 =
																						(((BgL_rtl_insz00_bglt) COBJECT(
																								((BgL_rtl_insz00_bglt)
																									BgL_iz00_2791)))->
																						BgL_argsz00);
																					BgL_arg2315z00_2806 =
																						CAR(CDR(BgL_pairz00_6102));
																				}
																				BgL__ortest_1363z00_2803 =
																					BGl_regzf3ze70z14zzsaw_bbvzd2typeszd2
																					(BgL_arg2315z00_2806);
																			}
																			if (BgL__ortest_1363z00_2803)
																				{	/* SawBbv/bbv-types.scm 859 */
																					BgL_test4369z00_12182 =
																						BgL__ortest_1363z00_2803;
																				}
																			else
																				{	/* SawBbv/bbv-types.scm 859 */
																					obj_t BgL_arg2313z00_2804;

																					{	/* SawBbv/bbv-types.scm 859 */
																						obj_t BgL_pairz00_6106;

																						BgL_pairz00_6106 =
																							(((BgL_rtl_insz00_bglt) COBJECT(
																									((BgL_rtl_insz00_bglt)
																										BgL_iz00_2791)))->
																							BgL_argsz00);
																						BgL_arg2313z00_2804 =
																							CAR(CDR(BgL_pairz00_6106));
																					}
																					{	/* SawBbv/bbv-types.scm 806 */
																						BgL_rtl_funz00_bglt
																							BgL_arg2281z00_6112;
																						BgL_arg2281z00_6112 =
																							(((BgL_rtl_insz00_bglt)
																								COBJECT(((BgL_rtl_insz00_bglt)
																										BgL_arg2313z00_2804)))->
																							BgL_funz00);
																						{	/* SawBbv/bbv-types.scm 806 */
																							obj_t BgL_classz00_6113;

																							BgL_classz00_6113 =
																								BGl_rtl_loadiz00zzsaw_defsz00;
																							{	/* SawBbv/bbv-types.scm 806 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_6115;
																								{	/* SawBbv/bbv-types.scm 806 */
																									obj_t BgL_tmpz00_12195;

																									BgL_tmpz00_12195 =
																										((obj_t)
																										((BgL_objectz00_bglt)
																											BgL_arg2281z00_6112));
																									BgL_arg1807z00_6115 =
																										(BgL_objectz00_bglt)
																										(BgL_tmpz00_12195);
																								}
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* SawBbv/bbv-types.scm 806 */
																										long BgL_idxz00_6121;

																										BgL_idxz00_6121 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_6115);
																										BgL_test4369z00_12182 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_6121 +
																													3L)) ==
																											BgL_classz00_6113);
																									}
																								else
																									{	/* SawBbv/bbv-types.scm 806 */
																										bool_t BgL_res3621z00_6146;

																										{	/* SawBbv/bbv-types.scm 806 */
																											obj_t BgL_oclassz00_6129;

																											{	/* SawBbv/bbv-types.scm 806 */
																												obj_t
																													BgL_arg1815z00_6137;
																												long
																													BgL_arg1816z00_6138;
																												BgL_arg1815z00_6137 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* SawBbv/bbv-types.scm 806 */
																													long
																														BgL_arg1817z00_6139;
																													BgL_arg1817z00_6139 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_6115);
																													BgL_arg1816z00_6138 =
																														(BgL_arg1817z00_6139
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_6129 =
																													VECTOR_REF
																													(BgL_arg1815z00_6137,
																													BgL_arg1816z00_6138);
																											}
																											{	/* SawBbv/bbv-types.scm 806 */
																												bool_t
																													BgL__ortest_1115z00_6130;
																												BgL__ortest_1115z00_6130
																													=
																													(BgL_classz00_6113 ==
																													BgL_oclassz00_6129);
																												if (BgL__ortest_1115z00_6130)
																													{	/* SawBbv/bbv-types.scm 806 */
																														BgL_res3621z00_6146
																															=
																															BgL__ortest_1115z00_6130;
																													}
																												else
																													{	/* SawBbv/bbv-types.scm 806 */
																														long
																															BgL_odepthz00_6131;
																														{	/* SawBbv/bbv-types.scm 806 */
																															obj_t
																																BgL_arg1804z00_6132;
																															BgL_arg1804z00_6132
																																=
																																(BgL_oclassz00_6129);
																															BgL_odepthz00_6131
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_6132);
																														}
																														if (
																															(3L <
																																BgL_odepthz00_6131))
																															{	/* SawBbv/bbv-types.scm 806 */
																																obj_t
																																	BgL_arg1802z00_6134;
																																{	/* SawBbv/bbv-types.scm 806 */
																																	obj_t
																																		BgL_arg1803z00_6135;
																																	BgL_arg1803z00_6135
																																		=
																																		(BgL_oclassz00_6129);
																																	BgL_arg1802z00_6134
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_6135,
																																		3L);
																																}
																																BgL_res3621z00_6146
																																	=
																																	(BgL_arg1802z00_6134
																																	==
																																	BgL_classz00_6113);
																															}
																														else
																															{	/* SawBbv/bbv-types.scm 806 */
																																BgL_res3621z00_6146
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test4369z00_12182 =
																											BgL_res3621z00_6146;
																									}
																							}
																						}
																					}
																				}
																		}
																		if (BgL_test4369z00_12182)
																			{	/* SawBbv/bbv-types.scm 860 */
																				obj_t BgL_arg2311z00_2801;

																				{	/* SawBbv/bbv-types.scm 860 */
																					obj_t BgL_pairz00_6147;

																					BgL_pairz00_6147 =
																						(((BgL_rtl_insz00_bglt) COBJECT(
																								((BgL_rtl_insz00_bglt)
																									BgL_iz00_2791)))->
																						BgL_argsz00);
																					BgL_arg2311z00_2801 =
																						CAR(CDR(CDR(BgL_pairz00_6147)));
																				}
																				return
																					BGl_regzf3ze70z14zzsaw_bbvzd2typeszd2
																					(BgL_arg2311z00_2801);
																			}
																		else
																			{	/* SawBbv/bbv-types.scm 859 */
																				return ((bool_t) 0);
																			}
																	}
																else
																	{	/* SawBbv/bbv-types.scm 858 */
																		return ((bool_t) 0);
																	}
															}
														else
															{	/* SawBbv/bbv-types.scm 855 */
																return ((bool_t) 0);
															}
													}
												else
													{	/* SawBbv/bbv-types.scm 854 */
														return ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-types.scm 851 */
												return ((bool_t) 0);
											}
									}
								}
							else
								{	/* SawBbv/bbv-types.scm 864 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 862 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* reg?~0 */
	bool_t BGl_regzf3ze70z14zzsaw_bbvzd2typeszd2(obj_t BgL_az00_2782)
	{
		{	/* SawBbv/bbv-types.scm 848 */
			{	/* SawBbv/bbv-types.scm 844 */
				bool_t BgL__ortest_1351z00_2784;

				{	/* SawBbv/bbv-types.scm 844 */
					obj_t BgL_classz00_5855;

					BgL_classz00_5855 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_az00_2782))
						{	/* SawBbv/bbv-types.scm 844 */
							BgL_objectz00_bglt BgL_arg1807z00_5857;

							BgL_arg1807z00_5857 = (BgL_objectz00_bglt) (BgL_az00_2782);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 844 */
									long BgL_idxz00_5863;

									BgL_idxz00_5863 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5857);
									BgL__ortest_1351z00_2784 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5863 + 1L)) == BgL_classz00_5855);
								}
							else
								{	/* SawBbv/bbv-types.scm 844 */
									bool_t BgL_res3614z00_5888;

									{	/* SawBbv/bbv-types.scm 844 */
										obj_t BgL_oclassz00_5871;

										{	/* SawBbv/bbv-types.scm 844 */
											obj_t BgL_arg1815z00_5879;
											long BgL_arg1816z00_5880;

											BgL_arg1815z00_5879 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 844 */
												long BgL_arg1817z00_5881;

												BgL_arg1817z00_5881 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5857);
												BgL_arg1816z00_5880 =
													(BgL_arg1817z00_5881 - OBJECT_TYPE);
											}
											BgL_oclassz00_5871 =
												VECTOR_REF(BgL_arg1815z00_5879, BgL_arg1816z00_5880);
										}
										{	/* SawBbv/bbv-types.scm 844 */
											bool_t BgL__ortest_1115z00_5872;

											BgL__ortest_1115z00_5872 =
												(BgL_classz00_5855 == BgL_oclassz00_5871);
											if (BgL__ortest_1115z00_5872)
												{	/* SawBbv/bbv-types.scm 844 */
													BgL_res3614z00_5888 = BgL__ortest_1115z00_5872;
												}
											else
												{	/* SawBbv/bbv-types.scm 844 */
													long BgL_odepthz00_5873;

													{	/* SawBbv/bbv-types.scm 844 */
														obj_t BgL_arg1804z00_5874;

														BgL_arg1804z00_5874 = (BgL_oclassz00_5871);
														BgL_odepthz00_5873 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5874);
													}
													if ((1L < BgL_odepthz00_5873))
														{	/* SawBbv/bbv-types.scm 844 */
															obj_t BgL_arg1802z00_5876;

															{	/* SawBbv/bbv-types.scm 844 */
																obj_t BgL_arg1803z00_5877;

																BgL_arg1803z00_5877 = (BgL_oclassz00_5871);
																BgL_arg1802z00_5876 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5877,
																	1L);
															}
															BgL_res3614z00_5888 =
																(BgL_arg1802z00_5876 == BgL_classz00_5855);
														}
													else
														{	/* SawBbv/bbv-types.scm 844 */
															BgL_res3614z00_5888 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1351z00_2784 = BgL_res3614z00_5888;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 844 */
							BgL__ortest_1351z00_2784 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1351z00_2784)
					{	/* SawBbv/bbv-types.scm 844 */
						return BgL__ortest_1351z00_2784;
					}
				else
					{	/* SawBbv/bbv-types.scm 845 */
						bool_t BgL_test4379z00_12249;

						{	/* SawBbv/bbv-types.scm 845 */
							obj_t BgL_classz00_5889;

							BgL_classz00_5889 = BGl_rtl_insz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_az00_2782))
								{	/* SawBbv/bbv-types.scm 845 */
									BgL_objectz00_bglt BgL_arg1807z00_5891;

									BgL_arg1807z00_5891 = (BgL_objectz00_bglt) (BgL_az00_2782);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-types.scm 845 */
											long BgL_idxz00_5897;

											BgL_idxz00_5897 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5891);
											BgL_test4379z00_12249 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5897 + 1L)) == BgL_classz00_5889);
										}
									else
										{	/* SawBbv/bbv-types.scm 845 */
											bool_t BgL_res3615z00_5922;

											{	/* SawBbv/bbv-types.scm 845 */
												obj_t BgL_oclassz00_5905;

												{	/* SawBbv/bbv-types.scm 845 */
													obj_t BgL_arg1815z00_5913;
													long BgL_arg1816z00_5914;

													BgL_arg1815z00_5913 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-types.scm 845 */
														long BgL_arg1817z00_5915;

														BgL_arg1817z00_5915 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5891);
														BgL_arg1816z00_5914 =
															(BgL_arg1817z00_5915 - OBJECT_TYPE);
													}
													BgL_oclassz00_5905 =
														VECTOR_REF(BgL_arg1815z00_5913,
														BgL_arg1816z00_5914);
												}
												{	/* SawBbv/bbv-types.scm 845 */
													bool_t BgL__ortest_1115z00_5906;

													BgL__ortest_1115z00_5906 =
														(BgL_classz00_5889 == BgL_oclassz00_5905);
													if (BgL__ortest_1115z00_5906)
														{	/* SawBbv/bbv-types.scm 845 */
															BgL_res3615z00_5922 = BgL__ortest_1115z00_5906;
														}
													else
														{	/* SawBbv/bbv-types.scm 845 */
															long BgL_odepthz00_5907;

															{	/* SawBbv/bbv-types.scm 845 */
																obj_t BgL_arg1804z00_5908;

																BgL_arg1804z00_5908 = (BgL_oclassz00_5905);
																BgL_odepthz00_5907 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5908);
															}
															if ((1L < BgL_odepthz00_5907))
																{	/* SawBbv/bbv-types.scm 845 */
																	obj_t BgL_arg1802z00_5910;

																	{	/* SawBbv/bbv-types.scm 845 */
																		obj_t BgL_arg1803z00_5911;

																		BgL_arg1803z00_5911 = (BgL_oclassz00_5905);
																		BgL_arg1802z00_5910 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5911, 1L);
																	}
																	BgL_res3615z00_5922 =
																		(BgL_arg1802z00_5910 == BgL_classz00_5889);
																}
															else
																{	/* SawBbv/bbv-types.scm 845 */
																	BgL_res3615z00_5922 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test4379z00_12249 = BgL_res3615z00_5922;
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 845 */
									BgL_test4379z00_12249 = ((bool_t) 0);
								}
						}
						if (BgL_test4379z00_12249)
							{	/* SawBbv/bbv-types.scm 847 */
								bool_t BgL_test4384z00_12272;

								{	/* SawBbv/bbv-types.scm 847 */
									BgL_rtl_funz00_bglt BgL_arg2307z00_2790;

									BgL_arg2307z00_2790 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_az00_2782)))->BgL_funz00);
									{	/* SawBbv/bbv-types.scm 847 */
										obj_t BgL_classz00_5923;

										BgL_classz00_5923 = BGl_rtl_callz00zzsaw_defsz00;
										{	/* SawBbv/bbv-types.scm 847 */
											BgL_objectz00_bglt BgL_arg1807z00_5925;

											{	/* SawBbv/bbv-types.scm 847 */
												obj_t BgL_tmpz00_12275;

												BgL_tmpz00_12275 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg2307z00_2790));
												BgL_arg1807z00_5925 =
													(BgL_objectz00_bglt) (BgL_tmpz00_12275);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-types.scm 847 */
													long BgL_idxz00_5931;

													BgL_idxz00_5931 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5925);
													BgL_test4384z00_12272 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5931 + 2L)) == BgL_classz00_5923);
												}
											else
												{	/* SawBbv/bbv-types.scm 847 */
													bool_t BgL_res3616z00_5956;

													{	/* SawBbv/bbv-types.scm 847 */
														obj_t BgL_oclassz00_5939;

														{	/* SawBbv/bbv-types.scm 847 */
															obj_t BgL_arg1815z00_5947;
															long BgL_arg1816z00_5948;

															BgL_arg1815z00_5947 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-types.scm 847 */
																long BgL_arg1817z00_5949;

																BgL_arg1817z00_5949 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5925);
																BgL_arg1816z00_5948 =
																	(BgL_arg1817z00_5949 - OBJECT_TYPE);
															}
															BgL_oclassz00_5939 =
																VECTOR_REF(BgL_arg1815z00_5947,
																BgL_arg1816z00_5948);
														}
														{	/* SawBbv/bbv-types.scm 847 */
															bool_t BgL__ortest_1115z00_5940;

															BgL__ortest_1115z00_5940 =
																(BgL_classz00_5923 == BgL_oclassz00_5939);
															if (BgL__ortest_1115z00_5940)
																{	/* SawBbv/bbv-types.scm 847 */
																	BgL_res3616z00_5956 =
																		BgL__ortest_1115z00_5940;
																}
															else
																{	/* SawBbv/bbv-types.scm 847 */
																	long BgL_odepthz00_5941;

																	{	/* SawBbv/bbv-types.scm 847 */
																		obj_t BgL_arg1804z00_5942;

																		BgL_arg1804z00_5942 = (BgL_oclassz00_5939);
																		BgL_odepthz00_5941 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5942);
																	}
																	if ((2L < BgL_odepthz00_5941))
																		{	/* SawBbv/bbv-types.scm 847 */
																			obj_t BgL_arg1802z00_5944;

																			{	/* SawBbv/bbv-types.scm 847 */
																				obj_t BgL_arg1803z00_5945;

																				BgL_arg1803z00_5945 =
																					(BgL_oclassz00_5939);
																				BgL_arg1802z00_5944 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5945, 2L);
																			}
																			BgL_res3616z00_5956 =
																				(BgL_arg1802z00_5944 ==
																				BgL_classz00_5923);
																		}
																	else
																		{	/* SawBbv/bbv-types.scm 847 */
																			BgL_res3616z00_5956 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test4384z00_12272 = BgL_res3616z00_5956;
												}
										}
									}
								}
								if (BgL_test4384z00_12272)
									{	/* SawBbv/bbv-types.scm 848 */
										obj_t BgL_arg2306z00_2789;

										BgL_arg2306z00_2789 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_az00_2782)))->BgL_destz00);
										{	/* SawBbv/bbv-types.scm 848 */
											obj_t BgL_classz00_5957;

											BgL_classz00_5957 = BGl_rtl_regz00zzsaw_defsz00;
											if (BGL_OBJECTP(BgL_arg2306z00_2789))
												{	/* SawBbv/bbv-types.scm 848 */
													BgL_objectz00_bglt BgL_arg1807z00_5959;

													BgL_arg1807z00_5959 =
														(BgL_objectz00_bglt) (BgL_arg2306z00_2789);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-types.scm 848 */
															long BgL_idxz00_5965;

															BgL_idxz00_5965 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5959);
															return
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5965 + 1L)) == BgL_classz00_5957);
														}
													else
														{	/* SawBbv/bbv-types.scm 848 */
															bool_t BgL_res3617z00_5990;

															{	/* SawBbv/bbv-types.scm 848 */
																obj_t BgL_oclassz00_5973;

																{	/* SawBbv/bbv-types.scm 848 */
																	obj_t BgL_arg1815z00_5981;
																	long BgL_arg1816z00_5982;

																	BgL_arg1815z00_5981 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-types.scm 848 */
																		long BgL_arg1817z00_5983;

																		BgL_arg1817z00_5983 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5959);
																		BgL_arg1816z00_5982 =
																			(BgL_arg1817z00_5983 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5973 =
																		VECTOR_REF(BgL_arg1815z00_5981,
																		BgL_arg1816z00_5982);
																}
																{	/* SawBbv/bbv-types.scm 848 */
																	bool_t BgL__ortest_1115z00_5974;

																	BgL__ortest_1115z00_5974 =
																		(BgL_classz00_5957 == BgL_oclassz00_5973);
																	if (BgL__ortest_1115z00_5974)
																		{	/* SawBbv/bbv-types.scm 848 */
																			BgL_res3617z00_5990 =
																				BgL__ortest_1115z00_5974;
																		}
																	else
																		{	/* SawBbv/bbv-types.scm 848 */
																			long BgL_odepthz00_5975;

																			{	/* SawBbv/bbv-types.scm 848 */
																				obj_t BgL_arg1804z00_5976;

																				BgL_arg1804z00_5976 =
																					(BgL_oclassz00_5973);
																				BgL_odepthz00_5975 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5976);
																			}
																			if ((1L < BgL_odepthz00_5975))
																				{	/* SawBbv/bbv-types.scm 848 */
																					obj_t BgL_arg1802z00_5978;

																					{	/* SawBbv/bbv-types.scm 848 */
																						obj_t BgL_arg1803z00_5979;

																						BgL_arg1803z00_5979 =
																							(BgL_oclassz00_5973);
																						BgL_arg1802z00_5978 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5979, 1L);
																					}
																					BgL_res3617z00_5990 =
																						(BgL_arg1802z00_5978 ==
																						BgL_classz00_5957);
																				}
																			else
																				{	/* SawBbv/bbv-types.scm 848 */
																					BgL_res3617z00_5990 = ((bool_t) 0);
																				}
																		}
																}
															}
															return BgL_res3617z00_5990;
														}
												}
											else
												{	/* SawBbv/bbv-types.scm 848 */
													return ((bool_t) 0);
												}
										}
									}
								else
									{	/* SawBbv/bbv-types.scm 847 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 845 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* &rtl_ins-fxovop? */
	obj_t BGl_z62rtl_inszd2fxovopzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8239,
		obj_t BgL_iz00_8240)
	{
		{	/* SawBbv/bbv-types.scm 841 */
			return
				BBOOL(BGl_rtl_inszd2fxovopzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8240)));
		}

	}



/* rtl_ins-error? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2errorzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_182)
	{
		{	/* SawBbv/bbv-types.scm 870 */
			{	/* SawBbv/bbv-types.scm 871 */
				bool_t BgL_test4392z00_12325;

				{	/* SawBbv/bbv-types.scm 783 */
					BgL_rtl_funz00_bglt BgL_arg2274z00_6194;

					BgL_arg2274z00_6194 =
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_182))->BgL_funz00);
					{	/* SawBbv/bbv-types.scm 783 */
						obj_t BgL_classz00_6195;

						BgL_classz00_6195 = BGl_rtl_callz00zzsaw_defsz00;
						{	/* SawBbv/bbv-types.scm 783 */
							BgL_objectz00_bglt BgL_arg1807z00_6197;

							{	/* SawBbv/bbv-types.scm 783 */
								obj_t BgL_tmpz00_12327;

								BgL_tmpz00_12327 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2274z00_6194));
								BgL_arg1807z00_6197 = (BgL_objectz00_bglt) (BgL_tmpz00_12327);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 783 */
									long BgL_idxz00_6203;

									BgL_idxz00_6203 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6197);
									BgL_test4392z00_12325 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6203 + 2L)) == BgL_classz00_6195);
								}
							else
								{	/* SawBbv/bbv-types.scm 783 */
									bool_t BgL_res3623z00_6228;

									{	/* SawBbv/bbv-types.scm 783 */
										obj_t BgL_oclassz00_6211;

										{	/* SawBbv/bbv-types.scm 783 */
											obj_t BgL_arg1815z00_6219;
											long BgL_arg1816z00_6220;

											BgL_arg1815z00_6219 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 783 */
												long BgL_arg1817z00_6221;

												BgL_arg1817z00_6221 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6197);
												BgL_arg1816z00_6220 =
													(BgL_arg1817z00_6221 - OBJECT_TYPE);
											}
											BgL_oclassz00_6211 =
												VECTOR_REF(BgL_arg1815z00_6219, BgL_arg1816z00_6220);
										}
										{	/* SawBbv/bbv-types.scm 783 */
											bool_t BgL__ortest_1115z00_6212;

											BgL__ortest_1115z00_6212 =
												(BgL_classz00_6195 == BgL_oclassz00_6211);
											if (BgL__ortest_1115z00_6212)
												{	/* SawBbv/bbv-types.scm 783 */
													BgL_res3623z00_6228 = BgL__ortest_1115z00_6212;
												}
											else
												{	/* SawBbv/bbv-types.scm 783 */
													long BgL_odepthz00_6213;

													{	/* SawBbv/bbv-types.scm 783 */
														obj_t BgL_arg1804z00_6214;

														BgL_arg1804z00_6214 = (BgL_oclassz00_6211);
														BgL_odepthz00_6213 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6214);
													}
													if ((2L < BgL_odepthz00_6213))
														{	/* SawBbv/bbv-types.scm 783 */
															obj_t BgL_arg1802z00_6216;

															{	/* SawBbv/bbv-types.scm 783 */
																obj_t BgL_arg1803z00_6217;

																BgL_arg1803z00_6217 = (BgL_oclassz00_6211);
																BgL_arg1802z00_6216 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6217,
																	2L);
															}
															BgL_res3623z00_6228 =
																(BgL_arg1802z00_6216 == BgL_classz00_6195);
														}
													else
														{	/* SawBbv/bbv-types.scm 783 */
															BgL_res3623z00_6228 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4392z00_12325 = BgL_res3623z00_6228;
								}
						}
					}
				}
				if (BgL_test4392z00_12325)
					{	/* SawBbv/bbv-types.scm 873 */
						BgL_rtl_callz00_bglt BgL_i1366z00_2825;

						BgL_i1366z00_2825 =
							((BgL_rtl_callz00_bglt)
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_182))->BgL_funz00));
						{	/* SawBbv/bbv-types.scm 874 */
							BgL_globalz00_bglt BgL_arg2328z00_2826;

							BgL_arg2328z00_2826 =
								(((BgL_rtl_callz00_bglt) COBJECT(BgL_i1366z00_2825))->
								BgL_varz00);
							return (((obj_t) BgL_arg2328z00_2826) ==
								BGl_za2errorza2z00zzsaw_bbvzd2cachezd2);
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 871 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins-error? */
	obj_t BGl_z62rtl_inszd2errorzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8241,
		obj_t BgL_iz00_8242)
	{
		{	/* SawBbv/bbv-types.scm 870 */
			return
				BBOOL(BGl_rtl_inszd2errorzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8242)));
		}

	}



/* rtl_ins-bool? */
	BGL_EXPORTED_DEF bool_t
		BGl_rtl_inszd2boolzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_183)
	{
		{	/* SawBbv/bbv-types.scm 879 */
			{	/* SawBbv/bbv-types.scm 881 */
				bool_t BgL_test4396z00_12358;

				{	/* SawBbv/bbv-types.scm 881 */
					bool_t BgL_test4397z00_12359;

					{	/* SawBbv/bbv-types.scm 881 */
						obj_t BgL_arg2339z00_2838;

						BgL_arg2339z00_2838 =
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_183))->BgL_destz00);
						{	/* SawBbv/bbv-types.scm 881 */
							obj_t BgL_classz00_6229;

							BgL_classz00_6229 = BGl_rtl_regz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_arg2339z00_2838))
								{	/* SawBbv/bbv-types.scm 881 */
									BgL_objectz00_bglt BgL_arg1807z00_6231;

									BgL_arg1807z00_6231 =
										(BgL_objectz00_bglt) (BgL_arg2339z00_2838);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-types.scm 881 */
											long BgL_idxz00_6237;

											BgL_idxz00_6237 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6231);
											BgL_test4397z00_12359 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6237 + 1L)) == BgL_classz00_6229);
										}
									else
										{	/* SawBbv/bbv-types.scm 881 */
											bool_t BgL_res3624z00_6262;

											{	/* SawBbv/bbv-types.scm 881 */
												obj_t BgL_oclassz00_6245;

												{	/* SawBbv/bbv-types.scm 881 */
													obj_t BgL_arg1815z00_6253;
													long BgL_arg1816z00_6254;

													BgL_arg1815z00_6253 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-types.scm 881 */
														long BgL_arg1817z00_6255;

														BgL_arg1817z00_6255 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6231);
														BgL_arg1816z00_6254 =
															(BgL_arg1817z00_6255 - OBJECT_TYPE);
													}
													BgL_oclassz00_6245 =
														VECTOR_REF(BgL_arg1815z00_6253,
														BgL_arg1816z00_6254);
												}
												{	/* SawBbv/bbv-types.scm 881 */
													bool_t BgL__ortest_1115z00_6246;

													BgL__ortest_1115z00_6246 =
														(BgL_classz00_6229 == BgL_oclassz00_6245);
													if (BgL__ortest_1115z00_6246)
														{	/* SawBbv/bbv-types.scm 881 */
															BgL_res3624z00_6262 = BgL__ortest_1115z00_6246;
														}
													else
														{	/* SawBbv/bbv-types.scm 881 */
															long BgL_odepthz00_6247;

															{	/* SawBbv/bbv-types.scm 881 */
																obj_t BgL_arg1804z00_6248;

																BgL_arg1804z00_6248 = (BgL_oclassz00_6245);
																BgL_odepthz00_6247 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6248);
															}
															if ((1L < BgL_odepthz00_6247))
																{	/* SawBbv/bbv-types.scm 881 */
																	obj_t BgL_arg1802z00_6250;

																	{	/* SawBbv/bbv-types.scm 881 */
																		obj_t BgL_arg1803z00_6251;

																		BgL_arg1803z00_6251 = (BgL_oclassz00_6245);
																		BgL_arg1802z00_6250 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6251, 1L);
																	}
																	BgL_res3624z00_6262 =
																		(BgL_arg1802z00_6250 == BgL_classz00_6229);
																}
															else
																{	/* SawBbv/bbv-types.scm 881 */
																	BgL_res3624z00_6262 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test4397z00_12359 = BgL_res3624z00_6262;
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 881 */
									BgL_test4397z00_12359 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test4397z00_12359)
						{	/* SawBbv/bbv-types.scm 881 */
							BgL_rtl_funz00_bglt BgL_arg2338z00_2837;

							BgL_arg2338z00_2837 =
								(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_183))->BgL_funz00);
							{	/* SawBbv/bbv-types.scm 881 */
								obj_t BgL_classz00_6263;

								BgL_classz00_6263 = BGl_rtl_callz00zzsaw_defsz00;
								{	/* SawBbv/bbv-types.scm 881 */
									BgL_objectz00_bglt BgL_arg1807z00_6265;

									{	/* SawBbv/bbv-types.scm 881 */
										obj_t BgL_tmpz00_12384;

										BgL_tmpz00_12384 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2338z00_2837));
										BgL_arg1807z00_6265 =
											(BgL_objectz00_bglt) (BgL_tmpz00_12384);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-types.scm 881 */
											long BgL_idxz00_6271;

											BgL_idxz00_6271 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6265);
											BgL_test4396z00_12358 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6271 + 2L)) == BgL_classz00_6263);
										}
									else
										{	/* SawBbv/bbv-types.scm 881 */
											bool_t BgL_res3625z00_6296;

											{	/* SawBbv/bbv-types.scm 881 */
												obj_t BgL_oclassz00_6279;

												{	/* SawBbv/bbv-types.scm 881 */
													obj_t BgL_arg1815z00_6287;
													long BgL_arg1816z00_6288;

													BgL_arg1815z00_6287 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-types.scm 881 */
														long BgL_arg1817z00_6289;

														BgL_arg1817z00_6289 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6265);
														BgL_arg1816z00_6288 =
															(BgL_arg1817z00_6289 - OBJECT_TYPE);
													}
													BgL_oclassz00_6279 =
														VECTOR_REF(BgL_arg1815z00_6287,
														BgL_arg1816z00_6288);
												}
												{	/* SawBbv/bbv-types.scm 881 */
													bool_t BgL__ortest_1115z00_6280;

													BgL__ortest_1115z00_6280 =
														(BgL_classz00_6263 == BgL_oclassz00_6279);
													if (BgL__ortest_1115z00_6280)
														{	/* SawBbv/bbv-types.scm 881 */
															BgL_res3625z00_6296 = BgL__ortest_1115z00_6280;
														}
													else
														{	/* SawBbv/bbv-types.scm 881 */
															long BgL_odepthz00_6281;

															{	/* SawBbv/bbv-types.scm 881 */
																obj_t BgL_arg1804z00_6282;

																BgL_arg1804z00_6282 = (BgL_oclassz00_6279);
																BgL_odepthz00_6281 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6282);
															}
															if ((2L < BgL_odepthz00_6281))
																{	/* SawBbv/bbv-types.scm 881 */
																	obj_t BgL_arg1802z00_6284;

																	{	/* SawBbv/bbv-types.scm 881 */
																		obj_t BgL_arg1803z00_6285;

																		BgL_arg1803z00_6285 = (BgL_oclassz00_6279);
																		BgL_arg1802z00_6284 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6285, 2L);
																	}
																	BgL_res3625z00_6296 =
																		(BgL_arg1802z00_6284 == BgL_classz00_6263);
																}
															else
																{	/* SawBbv/bbv-types.scm 881 */
																	BgL_res3625z00_6296 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test4396z00_12358 = BgL_res3625z00_6296;
										}
								}
							}
						}
					else
						{	/* SawBbv/bbv-types.scm 881 */
							BgL_test4396z00_12358 = ((bool_t) 0);
						}
				}
				if (BgL_test4396z00_12358)
					{	/* SawBbv/bbv-types.scm 882 */
						BgL_rtl_callz00_bglt BgL_i1368z00_2833;

						BgL_i1368z00_2833 =
							((BgL_rtl_callz00_bglt)
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_183))->BgL_funz00));
						{	/* SawBbv/bbv-types.scm 883 */
							BgL_globalz00_bglt BgL_i1369z00_2834;

							BgL_i1369z00_2834 =
								(((BgL_rtl_callz00_bglt) COBJECT(BgL_i1368z00_2833))->
								BgL_varz00);
							{	/* SawBbv/bbv-types.scm 884 */
								BgL_typez00_bglt BgL_arg2337z00_2835;

								BgL_arg2337z00_2835 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_i1369z00_2834)))->
									BgL_typez00);
								return (((obj_t) BgL_arg2337z00_2835) ==
									BGl_za2boolza2z00zztype_cachez00);
							}
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 881 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins-bool? */
	obj_t BGl_z62rtl_inszd2boolzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8243,
		obj_t BgL_iz00_8244)
	{
		{	/* SawBbv/bbv-types.scm 879 */
			return
				BBOOL(BGl_rtl_inszd2boolzf3z21zzsaw_bbvzd2typeszd2(
					((BgL_rtl_insz00_bglt) BgL_iz00_8244)));
		}

	}



/* rtl_ins-typecheck */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2typecheckzd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_184)
	{
		{	/* SawBbv/bbv-types.scm 889 */
			{	/* SawBbv/bbv-types.scm 891 */
				obj_t BgL_typz00_2840;
				obj_t BgL_valz00_2841;

				{	/* SawBbv/bbv-types.scm 891 */
					obj_t BgL_arg2340z00_2847;

					BgL_arg2340z00_2847 =
						CAR((((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_184))->BgL_argsz00));
					BgL_typz00_2840 =
						BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(
						((BgL_rtl_insz00_bglt) BgL_arg2340z00_2847));
				}
				{	/* SawBbv/bbv-types.scm 892 */
					obj_t BgL_arg2342z00_2849;

					BgL_arg2342z00_2849 =
						CAR((((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_184))->BgL_argsz00));
					BgL_valz00_2841 =
						BGl_rtl_callzd2valueszd2zzsaw_bbvzd2typeszd2(
						((BgL_rtl_insz00_bglt) BgL_arg2342z00_2849));
				}
				{	/* SawBbv/bbv-types.scm 893 */
					obj_t BgL_argsz00_2842;

					BgL_argsz00_2842 =
						BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_iz00_184);
					{	/* SawBbv/bbv-types.scm 894 */
						obj_t BgL_val0_1737z00_2843;
						bool_t BgL_val2_1739z00_2845;

						BgL_val0_1737z00_2843 = CAR(((obj_t) BgL_argsz00_2842));
						{	/* SawBbv/bbv-types.scm 776 */
							BgL_rtl_funz00_bglt BgL_arg2273z00_6302;

							BgL_arg2273z00_6302 =
								(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_184))->BgL_funz00);
							{	/* SawBbv/bbv-types.scm 776 */
								obj_t BgL_classz00_6303;

								BgL_classz00_6303 = BGl_rtl_ifnez00zzsaw_defsz00;
								{	/* SawBbv/bbv-types.scm 776 */
									BgL_objectz00_bglt BgL_arg1807z00_6305;

									{	/* SawBbv/bbv-types.scm 776 */
										obj_t BgL_tmpz00_12429;

										BgL_tmpz00_12429 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2273z00_6302));
										BgL_arg1807z00_6305 =
											(BgL_objectz00_bglt) (BgL_tmpz00_12429);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-types.scm 776 */
											long BgL_idxz00_6311;

											BgL_idxz00_6311 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6305);
											BgL_val2_1739z00_2845 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6311 + 3L)) == BgL_classz00_6303);
										}
									else
										{	/* SawBbv/bbv-types.scm 776 */
											bool_t BgL_res3626z00_6336;

											{	/* SawBbv/bbv-types.scm 776 */
												obj_t BgL_oclassz00_6319;

												{	/* SawBbv/bbv-types.scm 776 */
													obj_t BgL_arg1815z00_6327;
													long BgL_arg1816z00_6328;

													BgL_arg1815z00_6327 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-types.scm 776 */
														long BgL_arg1817z00_6329;

														BgL_arg1817z00_6329 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6305);
														BgL_arg1816z00_6328 =
															(BgL_arg1817z00_6329 - OBJECT_TYPE);
													}
													BgL_oclassz00_6319 =
														VECTOR_REF(BgL_arg1815z00_6327,
														BgL_arg1816z00_6328);
												}
												{	/* SawBbv/bbv-types.scm 776 */
													bool_t BgL__ortest_1115z00_6320;

													BgL__ortest_1115z00_6320 =
														(BgL_classz00_6303 == BgL_oclassz00_6319);
													if (BgL__ortest_1115z00_6320)
														{	/* SawBbv/bbv-types.scm 776 */
															BgL_res3626z00_6336 = BgL__ortest_1115z00_6320;
														}
													else
														{	/* SawBbv/bbv-types.scm 776 */
															long BgL_odepthz00_6321;

															{	/* SawBbv/bbv-types.scm 776 */
																obj_t BgL_arg1804z00_6322;

																BgL_arg1804z00_6322 = (BgL_oclassz00_6319);
																BgL_odepthz00_6321 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6322);
															}
															if ((3L < BgL_odepthz00_6321))
																{	/* SawBbv/bbv-types.scm 776 */
																	obj_t BgL_arg1802z00_6324;

																	{	/* SawBbv/bbv-types.scm 776 */
																		obj_t BgL_arg1803z00_6325;

																		BgL_arg1803z00_6325 = (BgL_oclassz00_6319);
																		BgL_arg1802z00_6324 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6325, 3L);
																	}
																	BgL_res3626z00_6336 =
																		(BgL_arg1802z00_6324 == BgL_classz00_6303);
																}
															else
																{	/* SawBbv/bbv-types.scm 776 */
																	BgL_res3626z00_6336 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_val2_1739z00_2845 = BgL_res3626z00_6336;
										}
								}
							}
						}
						{	/* SawBbv/bbv-types.scm 894 */
							int BgL_tmpz00_12452;

							BgL_tmpz00_12452 = (int) (4L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12452);
						}
						{	/* SawBbv/bbv-types.scm 894 */
							int BgL_tmpz00_12455;

							BgL_tmpz00_12455 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_12455, BgL_typz00_2840);
						}
						{	/* SawBbv/bbv-types.scm 894 */
							obj_t BgL_auxz00_12460;
							int BgL_tmpz00_12458;

							BgL_auxz00_12460 = BBOOL(BgL_val2_1739z00_2845);
							BgL_tmpz00_12458 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_12458, BgL_auxz00_12460);
						}
						{	/* SawBbv/bbv-types.scm 894 */
							int BgL_tmpz00_12463;

							BgL_tmpz00_12463 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_12463, BgL_valz00_2841);
						}
						return BgL_val0_1737z00_2843;
					}
				}
			}
		}

	}



/* &rtl_ins-typecheck */
	obj_t BGl_z62rtl_inszd2typecheckzb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8245,
		obj_t BgL_iz00_8246)
	{
		{	/* SawBbv/bbv-types.scm 889 */
			return
				BGl_rtl_inszd2typecheckzd2zzsaw_bbvzd2typeszd2(
				((BgL_rtl_insz00_bglt) BgL_iz00_8246));
		}

	}



/* rtl_call-predicate */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_185)
	{
		{	/* SawBbv/bbv-types.scm 899 */
			{	/* SawBbv/bbv-types.scm 901 */
				BgL_rtl_callz00_bglt BgL_i1372z00_2852;

				BgL_i1372z00_2852 =
					((BgL_rtl_callz00_bglt)
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_185))->BgL_funz00));
				{	/* SawBbv/bbv-types.scm 902 */
					BgL_valuez00_bglt BgL_valz00_2853;

					BgL_valz00_2853 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									(((BgL_rtl_callz00_bglt) COBJECT(BgL_i1372z00_2852))->
										BgL_varz00))))->BgL_valuez00);
					{	/* SawBbv/bbv-types.scm 904 */
						obj_t BgL_g1373z00_2854;

						BgL_g1373z00_2854 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_valz00_2853)))->BgL_predicatezd2ofzd2);
						if (CBOOL(BgL_g1373z00_2854))
							{	/* SawBbv/bbv-types.scm 904 */
								return BgL_g1373z00_2854;
							}
						else
							{	/* SawBbv/bbv-types.scm 905 */
								bool_t BgL_test4409z00_12477;

								{	/* SawBbv/bbv-types.scm 905 */
									BgL_globalz00_bglt BgL_arg2348z00_2859;

									BgL_arg2348z00_2859 =
										(((BgL_rtl_callz00_bglt) COBJECT(BgL_i1372z00_2852))->
										BgL_varz00);
									BgL_test4409z00_12477 =
										(((obj_t) BgL_arg2348z00_2859) ==
										BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2);
								}
								if (BgL_test4409z00_12477)
									{	/* SawBbv/bbv-types.scm 905 */
										return CNST_TABLE_REF(6);
									}
								else
									{	/* SawBbv/bbv-types.scm 905 */
										return BFALSE;
									}
							}
					}
				}
			}
		}

	}



/* &rtl_call-predicate */
	obj_t BGl_z62rtl_callzd2predicatezb0zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8247, obj_t BgL_iz00_8248)
	{
		{	/* SawBbv/bbv-types.scm 899 */
			return
				BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(
				((BgL_rtl_insz00_bglt) BgL_iz00_8248));
		}

	}



/* rtl_call-values */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_callzd2valueszd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt
		BgL_iz00_186)
	{
		{	/* SawBbv/bbv-types.scm 911 */
			{	/* SawBbv/bbv-types.scm 913 */
				BgL_rtl_callz00_bglt BgL_i1376z00_2862;

				BgL_i1376z00_2862 =
					((BgL_rtl_callz00_bglt)
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_iz00_186))->BgL_funz00));
				{	/* SawBbv/bbv-types.scm 914 */
					BgL_valuez00_bglt BgL_valz00_2863;

					BgL_valz00_2863 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									(((BgL_rtl_callz00_bglt) COBJECT(BgL_i1376z00_2862))->
										BgL_varz00))))->BgL_valuez00);
					{	/* SawBbv/bbv-types.scm 914 */
						obj_t BgL_typz00_2864;

						BgL_typz00_2864 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_valz00_2863)))->BgL_predicatezd2ofzd2);
						{	/* SawBbv/bbv-types.scm 915 */

							if ((BgL_typz00_2864 == BGl_za2bintza2z00zztype_cachez00))
								{	/* SawBbv/bbv-types.scm 917 */
									return ((obj_t) BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2());
								}
							else
								{	/* SawBbv/bbv-types.scm 917 */
									return CNST_TABLE_REF(2);
								}
						}
					}
				}
			}
		}

	}



/* &rtl_call-values */
	obj_t BGl_z62rtl_callzd2valueszb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8249,
		obj_t BgL_iz00_8250)
	{
		{	/* SawBbv/bbv-types.scm 911 */
			return
				BGl_rtl_callzd2valueszd2zzsaw_bbvzd2typeszd2(
				((BgL_rtl_insz00_bglt) BgL_iz00_8250));
		}

	}



/* bit-xor* */
	obj_t BGl_bitzd2xorza2z70zzsaw_bbvzd2typeszd2(obj_t BgL_lstz00_187)
	{
		{	/* SawBbv/bbv-types.scm 923 */
			if (NULLP(BgL_lstz00_187))
				{	/* SawBbv/bbv-types.scm 924 */
					return BINT(0L);
				}
			else
				{
					obj_t BgL_lstz00_2870;
					obj_t BgL_hashz00_2871;

					BgL_lstz00_2870 = CDR(BgL_lstz00_187);
					BgL_hashz00_2871 = CAR(BgL_lstz00_187);
				BgL_zc3z04anonymousza32352ze3z87_2872:
					if (NULLP(BgL_lstz00_2870))
						{	/* SawBbv/bbv-types.scm 928 */
							return BgL_hashz00_2871;
						}
					else
						{
							obj_t BgL_hashz00_12506;
							obj_t BgL_lstz00_12503;

							BgL_lstz00_12503 = CDR(((obj_t) BgL_lstz00_2870));
							BgL_hashz00_12506 =
								BINT(
								((long) CINT(CAR(
											((obj_t) BgL_lstz00_2870))) ^
									(long) CINT(BgL_hashz00_2871)));
							BgL_hashz00_2871 = BgL_hashz00_12506;
							BgL_lstz00_2870 = BgL_lstz00_12503;
							goto BgL_zc3z04anonymousza32352ze3z87_2872;
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			{	/* SawBbv/bbv-types.scm 38 */
				obj_t BgL_arg2364z00_2882;
				obj_t BgL_arg2365z00_2883;

				{	/* SawBbv/bbv-types.scm 38 */
					obj_t BgL_v1765z00_2914;

					BgL_v1765z00_2914 = create_vector(5L);
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_arg2376z00_2915;

						BgL_arg2376z00_2915 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc3685z00zzsaw_bbvzd2typeszd2,
							BGl_proc3684z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3683z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1765z00_2914, 0L, BgL_arg2376z00_2915);
					}
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_arg2383z00_2928;

						BgL_arg2383z00_2928 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc3688z00zzsaw_bbvzd2typeszd2,
							BGl_proc3687z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3686z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1765z00_2914, 1L, BgL_arg2383z00_2928);
					}
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_arg2390z00_2941;

						BgL_arg2390z00_2941 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc3691z00zzsaw_bbvzd2typeszd2,
							BGl_proc3690z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3689z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1765z00_2914, 2L, BgL_arg2390z00_2941);
					}
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_arg2397z00_2954;

						BgL_arg2397z00_2954 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc3694z00zzsaw_bbvzd2typeszd2,
							BGl_proc3693z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3692z00zzsaw_bbvzd2typeszd2,
							BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						VECTOR_SET(BgL_v1765z00_2914, 3L, BgL_arg2397z00_2954);
					}
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_arg2408z00_2971;

						BgL_arg2408z00_2971 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc3697z00zzsaw_bbvzd2typeszd2,
							BGl_proc3696z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3695z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1765z00_2914, 4L, BgL_arg2408z00_2971);
					}
					BgL_arg2364z00_2882 = BgL_v1765z00_2914;
				}
				{	/* SawBbv/bbv-types.scm 38 */
					obj_t BgL_v1766z00_2984;

					BgL_v1766z00_2984 = create_vector(0L);
					BgL_arg2365z00_2883 = BgL_v1766z00_2984;
				}
				BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(13),
					CNST_TABLE_REF(14), BGl_rtl_insz00zzsaw_defsz00, 65096L,
					BGl_proc3701z00zzsaw_bbvzd2typeszd2,
					BGl_proc3700z00zzsaw_bbvzd2typeszd2, BFALSE,
					BGl_proc3699z00zzsaw_bbvzd2typeszd2,
					BGl_proc3698z00zzsaw_bbvzd2typeszd2, BgL_arg2364z00_2882,
					BgL_arg2365z00_2883);
			}
			{	/* SawBbv/bbv-types.scm 46 */
				obj_t BgL_arg2421z00_2993;
				obj_t BgL_arg2423z00_2994;

				{	/* SawBbv/bbv-types.scm 46 */
					obj_t BgL_v1767z00_3023;

					BgL_v1767z00_3023 = create_vector(4L);
					{	/* SawBbv/bbv-types.scm 46 */
						obj_t BgL_arg2435z00_3024;

						BgL_arg2435z00_3024 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc3704z00zzsaw_bbvzd2typeszd2,
							BGl_proc3703z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3702z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1767z00_3023, 0L, BgL_arg2435z00_3024);
					}
					{	/* SawBbv/bbv-types.scm 46 */
						obj_t BgL_arg2443z00_3037;

						BgL_arg2443z00_3037 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc3707z00zzsaw_bbvzd2typeszd2,
							BGl_proc3706z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3705z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1767z00_3023, 1L, BgL_arg2443z00_3037);
					}
					{	/* SawBbv/bbv-types.scm 46 */
						obj_t BgL_arg2450z00_3050;

						BgL_arg2450z00_3050 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc3710z00zzsaw_bbvzd2typeszd2,
							BGl_proc3709z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3708z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1767z00_3023, 2L, BgL_arg2450z00_3050);
					}
					{	/* SawBbv/bbv-types.scm 46 */
						obj_t BgL_arg2457z00_3063;

						BgL_arg2457z00_3063 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc3713z00zzsaw_bbvzd2typeszd2,
							BGl_proc3712z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3711z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1767z00_3023, 3L, BgL_arg2457z00_3063);
					}
					BgL_arg2421z00_2993 = BgL_v1767z00_3023;
				}
				{	/* SawBbv/bbv-types.scm 46 */
					obj_t BgL_v1768z00_3076;

					BgL_v1768z00_3076 = create_vector(0L);
					BgL_arg2423z00_2994 = BgL_v1768z00_3076;
				}
				BGl_blockVz00zzsaw_bbvzd2typeszd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(21),
					CNST_TABLE_REF(14), BGl_blockz00zzsaw_defsz00, 27668L,
					BGl_proc3717z00zzsaw_bbvzd2typeszd2,
					BGl_proc3716z00zzsaw_bbvzd2typeszd2, BFALSE,
					BGl_proc3715z00zzsaw_bbvzd2typeszd2,
					BGl_proc3714z00zzsaw_bbvzd2typeszd2, BgL_arg2421z00_2993,
					BgL_arg2423z00_2994);
			}
			{	/* SawBbv/bbv-types.scm 53 */
				obj_t BgL_arg2471z00_3085;
				obj_t BgL_arg2473z00_3086;

				{	/* SawBbv/bbv-types.scm 53 */
					obj_t BgL_v1769z00_3122;

					BgL_v1769z00_3122 = create_vector(11L);
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2486z00_3123;

						BgL_arg2486z00_3123 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc3720z00zzsaw_bbvzd2typeszd2,
							BGl_proc3719z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3718z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1769z00_3122, 0L, BgL_arg2486z00_3123);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2495z00_3136;

						BgL_arg2495z00_3136 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc3723z00zzsaw_bbvzd2typeszd2,
							BGl_proc3722z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3721z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1769z00_3122, 1L, BgL_arg2495z00_3136);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2505z00_3149;

						BgL_arg2505z00_3149 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc3726z00zzsaw_bbvzd2typeszd2,
							BGl_proc3725z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3724z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1769z00_3122, 2L, BgL_arg2505z00_3149);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2513z00_3162;

						BgL_arg2513z00_3162 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc3728z00zzsaw_bbvzd2typeszd2,
							BGl_proc3727z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						VECTOR_SET(BgL_v1769z00_3122, 3L, BgL_arg2513z00_3162);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2518z00_3172;

						BgL_arg2518z00_3172 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc3730z00zzsaw_bbvzd2typeszd2,
							BGl_proc3729z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_blockVz00zzsaw_bbvzd2typeszd2);
						VECTOR_SET(BgL_v1769z00_3122, 4L, BgL_arg2518z00_3172);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2524z00_3182;

						BgL_arg2524z00_3182 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc3733z00zzsaw_bbvzd2typeszd2,
							BGl_proc3732z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3731z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1769z00_3122, 5L, BgL_arg2524z00_3182);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2534z00_3195;

						BgL_arg2534z00_3195 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc3736z00zzsaw_bbvzd2typeszd2,
							BGl_proc3735z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3734z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1769z00_3122, 6L, BgL_arg2534z00_3195);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2542z00_3208;

						BgL_arg2542z00_3208 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc3739z00zzsaw_bbvzd2typeszd2,
							BGl_proc3738z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3737z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1769z00_3122, 7L, BgL_arg2542z00_3208);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2551z00_3221;

						BgL_arg2551z00_3221 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc3741z00zzsaw_bbvzd2typeszd2,
							BGl_proc3740z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1769z00_3122, 8L, BgL_arg2551z00_3221);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2556z00_3231;

						BgL_arg2556z00_3231 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc3744z00zzsaw_bbvzd2typeszd2,
							BGl_proc3743z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3742z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1769z00_3122, 9L, BgL_arg2556z00_3231);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2565z00_3244;

						BgL_arg2565z00_3244 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(29),
							BGl_proc3747z00zzsaw_bbvzd2typeszd2,
							BGl_proc3746z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3745z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1769z00_3122, 10L, BgL_arg2565z00_3244);
					}
					BgL_arg2471z00_3085 = BgL_v1769z00_3122;
				}
				{	/* SawBbv/bbv-types.scm 53 */
					obj_t BgL_v1770z00_3257;

					BgL_v1770z00_3257 = create_vector(0L);
					BgL_arg2473z00_3086 = BgL_v1770z00_3257;
				}
				BGl_blockSz00zzsaw_bbvzd2typeszd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(31),
					CNST_TABLE_REF(14), BGl_blockz00zzsaw_defsz00, 48320L,
					BGl_proc3751z00zzsaw_bbvzd2typeszd2,
					BGl_proc3750z00zzsaw_bbvzd2typeszd2, BFALSE,
					BGl_proc3749z00zzsaw_bbvzd2typeszd2,
					BGl_proc3748z00zzsaw_bbvzd2typeszd2, BgL_arg2471z00_3085,
					BgL_arg2473z00_3086);
			}
			{	/* SawBbv/bbv-types.scm 67 */
				obj_t BgL_arg2584z00_3265;
				obj_t BgL_arg2585z00_3266;

				{	/* SawBbv/bbv-types.scm 67 */
					obj_t BgL_v1771z00_3278;

					BgL_v1771z00_3278 = create_vector(2L);
					{	/* SawBbv/bbv-types.scm 67 */
						obj_t BgL_arg2591z00_3279;

						BgL_arg2591z00_3279 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(32),
							BGl_proc3754z00zzsaw_bbvzd2typeszd2,
							BGl_proc3753z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3752z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1771z00_3278, 0L, BgL_arg2591z00_3279);
					}
					{	/* SawBbv/bbv-types.scm 67 */
						obj_t BgL_arg2599z00_3292;

						BgL_arg2599z00_3292 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc3757z00zzsaw_bbvzd2typeszd2,
							BGl_proc3756z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3755z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1771z00_3278, 1L, BgL_arg2599z00_3292);
					}
					BgL_arg2584z00_3265 = BgL_v1771z00_3278;
				}
				{	/* SawBbv/bbv-types.scm 67 */
					obj_t BgL_v1772z00_3305;

					BgL_v1772z00_3305 = create_vector(0L);
					BgL_arg2585z00_3266 = BgL_v1772z00_3305;
				}
				BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(34),
					CNST_TABLE_REF(14), BGl_objectz00zz__objectz00, 60505L,
					BGl_proc3760z00zzsaw_bbvzd2typeszd2,
					BGl_proc3759z00zzsaw_bbvzd2typeszd2, BFALSE,
					BGl_proc3758z00zzsaw_bbvzd2typeszd2, BFALSE, BgL_arg2584z00_3265,
					BgL_arg2585z00_3266);
			}
			{	/* SawBbv/bbv-types.scm 72 */
				obj_t BgL_arg2611z00_3312;
				obj_t BgL_arg2612z00_3313;

				{	/* SawBbv/bbv-types.scm 72 */
					obj_t BgL_v1773z00_3326;

					BgL_v1773z00_3326 = create_vector(2L);
					{	/* SawBbv/bbv-types.scm 72 */
						obj_t BgL_arg2619z00_3327;

						BgL_arg2619z00_3327 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(35),
							BGl_proc3763z00zzsaw_bbvzd2typeszd2,
							BGl_proc3762z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3761z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1773z00_3326, 0L, BgL_arg2619z00_3327);
					}
					{	/* SawBbv/bbv-types.scm 72 */
						obj_t BgL_arg2626z00_3340;

						BgL_arg2626z00_3340 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(36),
							BGl_proc3766z00zzsaw_bbvzd2typeszd2,
							BGl_proc3765z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3764z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1773z00_3326, 1L, BgL_arg2626z00_3340);
					}
					BgL_arg2611z00_3312 = BgL_v1773z00_3326;
				}
				{	/* SawBbv/bbv-types.scm 72 */
					obj_t BgL_v1774z00_3353;

					BgL_v1774z00_3353 = create_vector(0L);
					BgL_arg2612z00_3313 = BgL_v1774z00_3353;
				}
				BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(37),
					CNST_TABLE_REF(14), BGl_objectz00zz__objectz00, 7768L,
					BGl_proc3769z00zzsaw_bbvzd2typeszd2,
					BGl_proc3768z00zzsaw_bbvzd2typeszd2,
					BGl_bbvzd2ctxzd2initzd2envzd2zzsaw_bbvzd2typeszd2,
					BGl_proc3767z00zzsaw_bbvzd2typeszd2, BFALSE, BgL_arg2611z00_3312,
					BgL_arg2612z00_3313);
			}
			{	/* SawBbv/bbv-types.scm 79 */
				obj_t BgL_arg2637z00_3360;
				obj_t BgL_arg2638z00_3361;

				{	/* SawBbv/bbv-types.scm 79 */
					obj_t BgL_v1775z00_3378;

					BgL_v1775z00_3378 = create_vector(7L);
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2644z00_3379;

						BgL_arg2644z00_3379 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(38),
							BGl_proc3771z00zzsaw_bbvzd2typeszd2,
							BGl_proc3770z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_rtl_regz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1775z00_3378, 0L, BgL_arg2644z00_3379);
					}
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2650z00_3389;

						BgL_arg2650z00_3389 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc3774z00zzsaw_bbvzd2typeszd2,
							BGl_proc3773z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc3772z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(40));
						VECTOR_SET(BgL_v1775z00_3378, 1L, BgL_arg2650z00_3389);
					}
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2659z00_3403;

						BgL_arg2659z00_3403 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc3776z00zzsaw_bbvzd2typeszd2,
							BGl_proc3775z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1775z00_3378, 2L, BgL_arg2659z00_3403);
					}
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2665z00_3413;

						BgL_arg2665z00_3413 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(42),
							BGl_proc3779z00zzsaw_bbvzd2typeszd2,
							BGl_proc3778z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3777z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1775z00_3378, 3L, BgL_arg2665z00_3413);
					}
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2673z00_3426;

						BgL_arg2673z00_3426 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(43),
							BGl_proc3782z00zzsaw_bbvzd2typeszd2,
							BGl_proc3781z00zzsaw_bbvzd2typeszd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc3780z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1775z00_3378, 4L, BgL_arg2673z00_3426);
					}
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2680z00_3439;

						BgL_arg2680z00_3439 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(44),
							BGl_proc3785z00zzsaw_bbvzd2typeszd2,
							BGl_proc3784z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3783z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1775z00_3378, 5L, BgL_arg2680z00_3439);
					}
					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_arg2688z00_3452;

						BgL_arg2688z00_3452 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(45),
							BGl_proc3788z00zzsaw_bbvzd2typeszd2,
							BGl_proc3787z00zzsaw_bbvzd2typeszd2, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc3786z00zzsaw_bbvzd2typeszd2, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1775z00_3378, 6L, BgL_arg2688z00_3452);
					}
					BgL_arg2637z00_3360 = BgL_v1775z00_3378;
				}
				{	/* SawBbv/bbv-types.scm 79 */
					obj_t BgL_v1776z00_3465;

					BgL_v1776z00_3465 = create_vector(0L);
					BgL_arg2638z00_3361 = BgL_v1776z00_3465;
				}
				return (BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(46),
						CNST_TABLE_REF(14), BGl_objectz00zz__objectz00, 49073L,
						BGl_proc3791z00zzsaw_bbvzd2typeszd2,
						BGl_proc3790z00zzsaw_bbvzd2typeszd2, BFALSE,
						BGl_proc3789z00zzsaw_bbvzd2typeszd2, BFALSE, BgL_arg2637z00_3360,
						BgL_arg2638z00_3361), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2643> */
	obj_t BGl_z62zc3z04anonymousza32643ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8360, obj_t BgL_new1225z00_8361)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			{
				BgL_bbvzd2ctxentryzd2_bglt BgL_auxz00_12665;

				{
					BgL_rtl_regz00_bglt BgL_auxz00_12666;

					{	/* SawBbv/bbv-types.scm 79 */
						obj_t BgL_classz00_9014;

						BgL_classz00_9014 = BGl_rtl_regz00zzsaw_defsz00;
						{	/* SawBbv/bbv-types.scm 79 */
							obj_t BgL__ortest_1117z00_9015;

							BgL__ortest_1117z00_9015 = BGL_CLASS_NIL(BgL_classz00_9014);
							if (CBOOL(BgL__ortest_1117z00_9015))
								{	/* SawBbv/bbv-types.scm 79 */
									BgL_auxz00_12666 =
										((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_9015);
								}
							else
								{	/* SawBbv/bbv-types.scm 79 */
									BgL_auxz00_12666 =
										((BgL_rtl_regz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9014));
								}
						}
					}
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_new1225z00_8361)))->
							BgL_regz00) = ((BgL_rtl_regz00_bglt) BgL_auxz00_12666), BUNSPEC);
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxentryzd2_bglt) BgL_new1225z00_8361)))->
						BgL_typesz00) = ((obj_t) MAKE_YOUNG_PAIR(BFALSE, BFALSE)), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
									BgL_new1225z00_8361)))->BgL_polarityz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
									BgL_new1225z00_8361)))->BgL_countz00) = ((long) 0L), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
									BgL_new1225z00_8361)))->BgL_valuez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
									BgL_new1225z00_8361)))->BgL_aliasesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
									BgL_new1225z00_8361)))->BgL_initvalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_12665 = ((BgL_bbvzd2ctxentryzd2_bglt) BgL_new1225z00_8361);
				return ((obj_t) BgL_auxz00_12665);
			}
		}

	}



/* &lambda2641 */
	BgL_bbvzd2ctxentryzd2_bglt BGl_z62lambda2641z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8362)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			{	/* SawBbv/bbv-types.scm 79 */
				BgL_bbvzd2ctxentryzd2_bglt BgL_new1224z00_9016;

				BgL_new1224z00_9016 =
					((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bbvzd2ctxentryzd2_bgl))));
				{	/* SawBbv/bbv-types.scm 79 */
					long BgL_arg2642z00_9017;

					BgL_arg2642z00_9017 =
						BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1224z00_9016), BgL_arg2642z00_9017);
				}
				return BgL_new1224z00_9016;
			}
		}

	}



/* &lambda2639 */
	BgL_bbvzd2ctxentryzd2_bglt BGl_z62lambda2639z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8363, obj_t BgL_reg1217z00_8364, obj_t BgL_types1218z00_8365,
		obj_t BgL_polarity1219z00_8366, obj_t BgL_count1220z00_8367,
		obj_t BgL_value1221z00_8368, obj_t BgL_aliases1222z00_8369,
		obj_t BgL_initval1223z00_8370)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			{	/* SawBbv/bbv-types.scm 79 */
				bool_t BgL_polarity1219z00_9020;
				long BgL_count1220z00_9021;

				BgL_polarity1219z00_9020 = CBOOL(BgL_polarity1219z00_8366);
				BgL_count1220z00_9021 = (long) CINT(BgL_count1220z00_8367);
				{	/* SawBbv/bbv-types.scm 79 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_new1436z00_9023;

					{	/* SawBbv/bbv-types.scm 79 */
						BgL_bbvzd2ctxentryzd2_bglt BgL_new1435z00_9024;

						BgL_new1435z00_9024 =
							((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2ctxentryzd2_bgl))));
						{	/* SawBbv/bbv-types.scm 79 */
							long BgL_arg2640z00_9025;

							BgL_arg2640z00_9025 =
								BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1435z00_9024),
								BgL_arg2640z00_9025);
						}
						BgL_new1436z00_9023 = BgL_new1435z00_9024;
					}
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_regz00) =
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_reg1217z00_8364)),
						BUNSPEC);
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_typesz00) =
						((obj_t) ((obj_t) BgL_types1218z00_8365)), BUNSPEC);
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_polarityz00) = ((bool_t) BgL_polarity1219z00_9020), BUNSPEC);
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_countz00) = ((long) BgL_count1220z00_9021), BUNSPEC);
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_valuez00) = ((obj_t) BgL_value1221z00_8368), BUNSPEC);
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_aliasesz00) =
						((obj_t) ((obj_t) BgL_aliases1222z00_8369)), BUNSPEC);
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1436z00_9023))->
							BgL_initvalz00) = ((obj_t) BgL_initval1223z00_8370), BUNSPEC);
					return BgL_new1436z00_9023;
				}
			}
		}

	}



/* &<@anonymous:2694> */
	obj_t BGl_z62zc3z04anonymousza32694ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8371)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return BUNSPEC;
		}

	}



/* &lambda2693 */
	obj_t BGl_z62lambda2693z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8372,
		obj_t BgL_oz00_8373, obj_t BgL_vz00_8374)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8373)))->BgL_initvalz00) =
				((obj_t) BgL_vz00_8374), BUNSPEC);
		}

	}



/* &lambda2692 */
	obj_t BGl_z62lambda2692z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8375,
		obj_t BgL_oz00_8376)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
						((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8376)))->BgL_initvalz00);
		}

	}



/* &<@anonymous:2687> */
	obj_t BGl_z62zc3z04anonymousza32687ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8377)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return BNIL;
		}

	}



/* &lambda2686 */
	obj_t BGl_z62lambda2686z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8378,
		obj_t BgL_oz00_8379, obj_t BgL_vz00_8380)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8379)))->BgL_aliasesz00) =
				((obj_t) ((obj_t) BgL_vz00_8380)), BUNSPEC);
		}

	}



/* &lambda2685 */
	obj_t BGl_z62lambda2685z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8381,
		obj_t BgL_oz00_8382)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
						((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8382)))->BgL_aliasesz00);
		}

	}



/* &<@anonymous:2679> */
	obj_t BGl_z62zc3z04anonymousza32679ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8383)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return CNST_TABLE_REF(2);
		}

	}



/* &lambda2678 */
	obj_t BGl_z62lambda2678z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8384,
		obj_t BgL_oz00_8385, obj_t BgL_vz00_8386)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8385)))->BgL_valuez00) =
				((obj_t) BgL_vz00_8386), BUNSPEC);
		}

	}



/* &lambda2677 */
	obj_t BGl_z62lambda2677z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8387,
		obj_t BgL_oz00_8388)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
						((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8388)))->BgL_valuez00);
		}

	}



/* &<@anonymous:2672> */
	obj_t BGl_z62zc3z04anonymousza32672ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8389)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return BINT(0L);
		}

	}



/* &lambda2671 */
	obj_t BGl_z62lambda2671z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8390,
		obj_t BgL_oz00_8391, obj_t BgL_vz00_8392)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			{	/* SawBbv/bbv-types.scm 79 */
				long BgL_vz00_9034;

				BgL_vz00_9034 = (long) CINT(BgL_vz00_8392);
				return
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8391)))->BgL_countz00) =
					((long) BgL_vz00_9034), BUNSPEC);
		}}

	}



/* &lambda2670 */
	obj_t BGl_z62lambda2670z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8393,
		obj_t BgL_oz00_8394)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				BINT(
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8394)))->BgL_countz00));
		}

	}



/* &lambda2664 */
	obj_t BGl_z62lambda2664z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8395,
		obj_t BgL_oz00_8396, obj_t BgL_vz00_8397)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			{	/* SawBbv/bbv-types.scm 79 */
				bool_t BgL_vz00_9037;

				BgL_vz00_9037 = CBOOL(BgL_vz00_8397);
				return
					((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8396)))->
						BgL_polarityz00) = ((bool_t) BgL_vz00_9037), BUNSPEC);
			}
		}

	}



/* &lambda2663 */
	obj_t BGl_z62lambda2663z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8398,
		obj_t BgL_oz00_8399)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				BBOOL(
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8399)))->BgL_polarityz00));
		}

	}



/* &<@anonymous:2657> */
	obj_t BGl_z62zc3z04anonymousza32657ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8400)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			{	/* SawBbv/bbv-types.scm 81 */
				obj_t BgL_res3892z00_9040;

				{	/* SawBbv/bbv-types.scm 81 */
					obj_t BgL_list2658z00_9039;

					BgL_list2658z00_9039 =
						MAKE_YOUNG_PAIR(BGl_za2objza2z00zztype_cachez00, BNIL);
					BgL_res3892z00_9040 = BgL_list2658z00_9039;
				}
				return BgL_res3892z00_9040;
			}
		}

	}



/* &lambda2656 */
	obj_t BGl_z62lambda2656z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8401,
		obj_t BgL_oz00_8402, obj_t BgL_vz00_8403)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8402)))->BgL_typesz00) =
				((obj_t) ((obj_t) BgL_vz00_8403)), BUNSPEC);
		}

	}



/* &lambda2655 */
	obj_t BGl_z62lambda2655z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8404,
		obj_t BgL_oz00_8405)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
						((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8405)))->BgL_typesz00);
		}

	}



/* &lambda2649 */
	obj_t BGl_z62lambda2649z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8406,
		obj_t BgL_oz00_8407, obj_t BgL_vz00_8408)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8407)))->BgL_regz00) =
				((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_vz00_8408)), BUNSPEC);
		}

	}



/* &lambda2648 */
	BgL_rtl_regz00_bglt BGl_z62lambda2648z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8409, obj_t BgL_oz00_8410)
	{
		{	/* SawBbv/bbv-types.scm 79 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
						((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_8410)))->BgL_regz00);
		}

	}



/* &<@anonymous:2618> */
	obj_t BGl_z62zc3z04anonymousza32618ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8411, obj_t BgL_new1215z00_8412)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			{
				BgL_bbvzd2ctxzd2_bglt BgL_auxz00_12748;

				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxzd2_bglt) BgL_new1215z00_8412)))->BgL_idz00) =
					((long) 0L), BUNSPEC);
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(((BgL_bbvzd2ctxzd2_bglt)
									BgL_new1215z00_8412)))->BgL_entriesz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_12748 = ((BgL_bbvzd2ctxzd2_bglt) BgL_new1215z00_8412);
				return ((obj_t) BgL_auxz00_12748);
			}
		}

	}



/* &lambda2616 */
	BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2616z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8413)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			{	/* SawBbv/bbv-types.scm 72 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1214z00_9048;

				BgL_new1214z00_9048 =
					((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bbvzd2ctxzd2_bgl))));
				{	/* SawBbv/bbv-types.scm 72 */
					long BgL_arg2617z00_9049;

					BgL_arg2617z00_9049 =
						BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1214z00_9048), BgL_arg2617z00_9049);
				}
				return BgL_new1214z00_9048;
			}
		}

	}



/* &lambda2613 */
	BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2613z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8414, obj_t BgL_id1212z00_8415, obj_t BgL_entries1213z00_8416)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			{	/* SawBbv/bbv-types.scm 72 */
				long BgL_id1212z00_9050;

				BgL_id1212z00_9050 = (long) CINT(BgL_id1212z00_8415);
				{	/* SawBbv/bbv-types.scm 72 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1434z00_9052;

					{	/* SawBbv/bbv-types.scm 72 */
						BgL_bbvzd2ctxzd2_bglt BgL_new1433z00_9053;

						BgL_new1433z00_9053 =
							((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2ctxzd2_bgl))));
						{	/* SawBbv/bbv-types.scm 72 */
							long BgL_arg2615z00_9054;

							BgL_arg2615z00_9054 =
								BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1433z00_9053),
								BgL_arg2615z00_9054);
						}
						BgL_new1434z00_9052 = BgL_new1433z00_9053;
					}
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1434z00_9052))->BgL_idz00) =
						((long) BgL_id1212z00_9050), BUNSPEC);
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1434z00_9052))->
							BgL_entriesz00) =
						((obj_t) ((obj_t) BgL_entries1213z00_8416)), BUNSPEC);
					{	/* SawBbv/bbv-types.scm 72 */
						obj_t BgL_fun2614z00_9055;

						BgL_fun2614z00_9055 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_PROCEDURE_CALL1(BgL_fun2614z00_9055,
							((obj_t) BgL_new1434z00_9052));
					}
					return BgL_new1434z00_9052;
				}
			}
		}

	}



/* &<@anonymous:2632> */
	obj_t BGl_z62zc3z04anonymousza32632ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8417)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			return BNIL;
		}

	}



/* &lambda2631 */
	obj_t BGl_z62lambda2631z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8418,
		obj_t BgL_oz00_8419, obj_t BgL_vz00_8420)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_8419)))->BgL_entriesz00) =
				((obj_t) ((obj_t) BgL_vz00_8420)), BUNSPEC);
		}

	}



/* &lambda2630 */
	obj_t BGl_z62lambda2630z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8421,
		obj_t BgL_oz00_8422)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			return
				(((BgL_bbvzd2ctxzd2_bglt) COBJECT(
						((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_8422)))->BgL_entriesz00);
		}

	}



/* &<@anonymous:2625> */
	obj_t BGl_z62zc3z04anonymousza32625ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8423)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			return BINT(-1L);
		}

	}



/* &lambda2624 */
	obj_t BGl_z62lambda2624z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8424,
		obj_t BgL_oz00_8425, obj_t BgL_vz00_8426)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			{	/* SawBbv/bbv-types.scm 72 */
				long BgL_vz00_9060;

				BgL_vz00_9060 = (long) CINT(BgL_vz00_8426);
				return
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_8425)))->BgL_idz00) =
					((long) BgL_vz00_9060), BUNSPEC);
		}}

	}



/* &lambda2623 */
	obj_t BGl_z62lambda2623z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8427,
		obj_t BgL_oz00_8428)
	{
		{	/* SawBbv/bbv-types.scm 72 */
			return
				BINT(
				(((BgL_bbvzd2ctxzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_8428)))->BgL_idz00));
		}

	}



/* &<@anonymous:2590> */
	obj_t BGl_z62zc3z04anonymousza32590ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8429, obj_t BgL_new1210z00_8430)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			{
				BgL_bbvzd2queuezd2_bglt BgL_auxz00_12785;

				((((BgL_bbvzd2queuezd2_bglt) COBJECT(
								((BgL_bbvzd2queuezd2_bglt) BgL_new1210z00_8430)))->
						BgL_blocksz00) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(((BgL_bbvzd2queuezd2_bglt)
									BgL_new1210z00_8430)))->BgL_lastz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_12785 = ((BgL_bbvzd2queuezd2_bglt) BgL_new1210z00_8430);
				return ((obj_t) BgL_auxz00_12785);
			}
		}

	}



/* &lambda2588 */
	BgL_bbvzd2queuezd2_bglt BGl_z62lambda2588z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8431)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			{	/* SawBbv/bbv-types.scm 67 */
				BgL_bbvzd2queuezd2_bglt BgL_new1209z00_9063;

				BgL_new1209z00_9063 =
					((BgL_bbvzd2queuezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bbvzd2queuezd2_bgl))));
				{	/* SawBbv/bbv-types.scm 67 */
					long BgL_arg2589z00_9064;

					BgL_arg2589z00_9064 =
						BGL_CLASS_NUM(BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1209z00_9063), BgL_arg2589z00_9064);
				}
				return BgL_new1209z00_9063;
			}
		}

	}



/* &lambda2586 */
	BgL_bbvzd2queuezd2_bglt BGl_z62lambda2586z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8432, obj_t BgL_blocks1207z00_8433, obj_t BgL_last1208z00_8434)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			{	/* SawBbv/bbv-types.scm 67 */
				BgL_bbvzd2queuezd2_bglt BgL_new1432z00_9067;

				{	/* SawBbv/bbv-types.scm 67 */
					BgL_bbvzd2queuezd2_bglt BgL_new1431z00_9068;

					BgL_new1431z00_9068 =
						((BgL_bbvzd2queuezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2queuezd2_bgl))));
					{	/* SawBbv/bbv-types.scm 67 */
						long BgL_arg2587z00_9069;

						BgL_arg2587z00_9069 =
							BGL_CLASS_NUM(BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1431z00_9068), BgL_arg2587z00_9069);
					}
					BgL_new1432z00_9067 = BgL_new1431z00_9068;
				}
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1432z00_9067))->
						BgL_blocksz00) =
					((obj_t) ((obj_t) BgL_blocks1207z00_8433)), BUNSPEC);
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1432z00_9067))->
						BgL_lastz00) = ((obj_t) ((obj_t) BgL_last1208z00_8434)), BUNSPEC);
				return BgL_new1432z00_9067;
			}
		}

	}



/* &<@anonymous:2607> */
	obj_t BGl_z62zc3z04anonymousza32607ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8435)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			return BNIL;
		}

	}



/* &lambda2606 */
	obj_t BGl_z62lambda2606z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8436,
		obj_t BgL_oz00_8437, obj_t BgL_vz00_8438)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(
							((BgL_bbvzd2queuezd2_bglt) BgL_oz00_8437)))->BgL_lastz00) =
				((obj_t) ((obj_t) BgL_vz00_8438)), BUNSPEC);
		}

	}



/* &lambda2605 */
	obj_t BGl_z62lambda2605z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8439,
		obj_t BgL_oz00_8440)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			return
				(((BgL_bbvzd2queuezd2_bglt) COBJECT(
						((BgL_bbvzd2queuezd2_bglt) BgL_oz00_8440)))->BgL_lastz00);
		}

	}



/* &<@anonymous:2598> */
	obj_t BGl_z62zc3z04anonymousza32598ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8441)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			return BNIL;
		}

	}



/* &lambda2597 */
	obj_t BGl_z62lambda2597z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8442,
		obj_t BgL_oz00_8443, obj_t BgL_vz00_8444)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(
							((BgL_bbvzd2queuezd2_bglt) BgL_oz00_8443)))->BgL_blocksz00) =
				((obj_t) ((obj_t) BgL_vz00_8444)), BUNSPEC);
		}

	}



/* &lambda2596 */
	obj_t BGl_z62lambda2596z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8445,
		obj_t BgL_oz00_8446)
	{
		{	/* SawBbv/bbv-types.scm 67 */
			return
				(((BgL_bbvzd2queuezd2_bglt) COBJECT(
						((BgL_bbvzd2queuezd2_bglt) BgL_oz00_8446)))->BgL_blocksz00);
		}

	}



/* &lambda2481 */
	BgL_blockz00_bglt BGl_z62lambda2481z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8447, obj_t BgL_o1205z00_8448)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_arg2482z00_9077;

				{	/* SawBbv/bbv-types.scm 53 */
					obj_t BgL_arg2483z00_9078;

					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_arg2484z00_9079;

						{	/* SawBbv/bbv-types.scm 53 */
							obj_t BgL_arg1815z00_9080;
							long BgL_arg1816z00_9081;

							BgL_arg1815z00_9080 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawBbv/bbv-types.scm 53 */
								long BgL_arg1817z00_9082;

								BgL_arg1817z00_9082 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1205z00_8448)));
								BgL_arg1816z00_9081 = (BgL_arg1817z00_9082 - OBJECT_TYPE);
							}
							BgL_arg2484z00_9079 =
								VECTOR_REF(BgL_arg1815z00_9080, BgL_arg1816z00_9081);
						}
						BgL_arg2483z00_9078 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2484z00_9079);
					}
					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_tmpz00_12821;

						BgL_tmpz00_12821 = ((obj_t) BgL_arg2483z00_9078);
						BgL_arg2482z00_9077 = BGL_CLASS_NUM(BgL_tmpz00_12821);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1205z00_8448)), BgL_arg2482z00_9077);
			}
			{	/* SawBbv/bbv-types.scm 53 */
				BgL_objectz00_bglt BgL_tmpz00_12827;

				BgL_tmpz00_12827 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1205z00_8448));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12827, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1205z00_8448));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1205z00_8448));
		}

	}



/* &<@anonymous:2480> */
	obj_t BGl_z62zc3z04anonymousza32480ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8449, obj_t BgL_new1204z00_8450)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blockz00_bglt BgL_auxz00_12835;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1204z00_8450))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1204z00_8450))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1204z00_8450))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1204z00_8450))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_blocksz00_bglt BgL_auxz00_12849;

					{
						obj_t BgL_auxz00_12850;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12851;

							BgL_tmpz00_12851 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12850 = BGL_OBJECT_WIDENING(BgL_tmpz00_12851);
						}
						BgL_auxz00_12849 = ((BgL_blocksz00_bglt) BgL_auxz00_12850);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12849))->BgL_z52markz52) =
						((long) 0L), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12857;

					{
						obj_t BgL_auxz00_12858;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12859;

							BgL_tmpz00_12859 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12858 = BGL_OBJECT_WIDENING(BgL_tmpz00_12859);
						}
						BgL_auxz00_12857 = ((BgL_blocksz00_bglt) BgL_auxz00_12858);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12857))->BgL_z52hashz52) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12865;

					{
						obj_t BgL_auxz00_12866;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12867;

							BgL_tmpz00_12867 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12866 = BGL_OBJECT_WIDENING(BgL_tmpz00_12867);
						}
						BgL_auxz00_12865 = ((BgL_blocksz00_bglt) BgL_auxz00_12866);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12865))->
							BgL_z52blacklistz52) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_bbvzd2ctxzd2_bglt BgL_auxz00_12880;
					BgL_blocksz00_bglt BgL_auxz00_12873;

					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_classz00_9084;

						BgL_classz00_9084 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
						{	/* SawBbv/bbv-types.scm 53 */
							obj_t BgL__ortest_1117z00_9085;

							BgL__ortest_1117z00_9085 = BGL_CLASS_NIL(BgL_classz00_9084);
							if (CBOOL(BgL__ortest_1117z00_9085))
								{	/* SawBbv/bbv-types.scm 53 */
									BgL_auxz00_12880 =
										((BgL_bbvzd2ctxzd2_bglt) BgL__ortest_1117z00_9085);
								}
							else
								{	/* SawBbv/bbv-types.scm 53 */
									BgL_auxz00_12880 =
										((BgL_bbvzd2ctxzd2_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9084));
								}
						}
					}
					{
						obj_t BgL_auxz00_12874;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12875;

							BgL_tmpz00_12875 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12874 = BGL_OBJECT_WIDENING(BgL_tmpz00_12875);
						}
						BgL_auxz00_12873 = ((BgL_blocksz00_bglt) BgL_auxz00_12874);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12873))->BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_auxz00_12880), BUNSPEC);
				}
				{
					BgL_blockz00_bglt BgL_auxz00_12895;
					BgL_blocksz00_bglt BgL_auxz00_12888;

					{	/* SawBbv/bbv-types.scm 53 */
						obj_t BgL_classz00_9086;

						BgL_classz00_9086 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
						{	/* SawBbv/bbv-types.scm 53 */
							obj_t BgL__ortest_1117z00_9087;

							BgL__ortest_1117z00_9087 = BGL_CLASS_NIL(BgL_classz00_9086);
							if (CBOOL(BgL__ortest_1117z00_9087))
								{	/* SawBbv/bbv-types.scm 53 */
									BgL_auxz00_12895 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_9087);
								}
							else
								{	/* SawBbv/bbv-types.scm 53 */
									BgL_auxz00_12895 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9086));
								}
						}
					}
					{
						obj_t BgL_auxz00_12889;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12890;

							BgL_tmpz00_12890 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12889 = BGL_OBJECT_WIDENING(BgL_tmpz00_12890);
						}
						BgL_auxz00_12888 = ((BgL_blocksz00_bglt) BgL_auxz00_12889);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12888))->BgL_parentz00) =
						((BgL_blockz00_bglt) BgL_auxz00_12895), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12903;

					{
						obj_t BgL_auxz00_12904;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12905;

							BgL_tmpz00_12905 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12904 = BGL_OBJECT_WIDENING(BgL_tmpz00_12905);
						}
						BgL_auxz00_12903 = ((BgL_blocksz00_bglt) BgL_auxz00_12904);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12903))->BgL_gccntz00) =
						((long) 0L), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12911;

					{
						obj_t BgL_auxz00_12912;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12913;

							BgL_tmpz00_12913 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12912 = BGL_OBJECT_WIDENING(BgL_tmpz00_12913);
						}
						BgL_auxz00_12911 = ((BgL_blocksz00_bglt) BgL_auxz00_12912);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12911))->BgL_gcmarkz00) =
						((long) 0L), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12919;

					{
						obj_t BgL_auxz00_12920;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12921;

							BgL_tmpz00_12921 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12920 = BGL_OBJECT_WIDENING(BgL_tmpz00_12921);
						}
						BgL_auxz00_12919 = ((BgL_blocksz00_bglt) BgL_auxz00_12920);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12919))->BgL_mblockz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12927;

					{
						obj_t BgL_auxz00_12928;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12929;

							BgL_tmpz00_12929 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12928 = BGL_OBJECT_WIDENING(BgL_tmpz00_12929);
						}
						BgL_auxz00_12927 = ((BgL_blocksz00_bglt) BgL_auxz00_12928);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12927))->BgL_creatorz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12935;

					{
						obj_t BgL_auxz00_12936;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12937;

							BgL_tmpz00_12937 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12936 = BGL_OBJECT_WIDENING(BgL_tmpz00_12937);
						}
						BgL_auxz00_12935 = ((BgL_blocksz00_bglt) BgL_auxz00_12936);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12935))->BgL_mergesz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_12943;

					{
						obj_t BgL_auxz00_12944;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_12945;

							BgL_tmpz00_12945 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1204z00_8450));
							BgL_auxz00_12944 = BGL_OBJECT_WIDENING(BgL_tmpz00_12945);
						}
						BgL_auxz00_12943 = ((BgL_blocksz00_bglt) BgL_auxz00_12944);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_12943))->BgL_asleepz00) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_12835 = ((BgL_blockz00_bglt) BgL_new1204z00_8450);
				return ((obj_t) BgL_auxz00_12835);
			}
		}

	}



/* &lambda2477 */
	BgL_blockz00_bglt BGl_z62lambda2477z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8451, obj_t BgL_o1200z00_8452)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				BgL_blocksz00_bglt BgL_wide1202z00_9089;

				BgL_wide1202z00_9089 =
					((BgL_blocksz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_blocksz00_bgl))));
				{	/* SawBbv/bbv-types.scm 53 */
					obj_t BgL_auxz00_12958;
					BgL_objectz00_bglt BgL_tmpz00_12954;

					BgL_auxz00_12958 = ((obj_t) BgL_wide1202z00_9089);
					BgL_tmpz00_12954 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1200z00_8452)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12954, BgL_auxz00_12958);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1200z00_8452)));
				{	/* SawBbv/bbv-types.scm 53 */
					long BgL_arg2479z00_9090;

					BgL_arg2479z00_9090 =
						BGL_CLASS_NUM(BGl_blockSz00zzsaw_bbvzd2typeszd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1200z00_8452))), BgL_arg2479z00_9090);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1200z00_8452)));
			}
		}

	}



/* &lambda2474 */
	BgL_blockz00_bglt BGl_z62lambda2474z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8453, obj_t BgL_label1185z00_8454, obj_t BgL_preds1186z00_8455,
		obj_t BgL_succs1187z00_8456, obj_t BgL_first1188z00_8457,
		obj_t BgL_z52mark1189z52_8458, obj_t BgL_z52hash1190z52_8459,
		obj_t BgL_z52blacklist1191z52_8460, obj_t BgL_ctx1192z00_8461,
		obj_t BgL_parent1193z00_8462, obj_t BgL_gccnt1194z00_8463,
		obj_t BgL_gcmark1195z00_8464, obj_t BgL_mblock1196z00_8465,
		obj_t BgL_creator1197z00_8466, obj_t BgL_merges1198z00_8467,
		obj_t BgL_asleep1199z00_8468)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				int BgL_label1185z00_9091;
				long BgL_z52mark1189z52_9095;
				long BgL_gccnt1194z00_9098;
				long BgL_gcmark1195z00_9099;
				bool_t BgL_asleep1199z00_9101;

				BgL_label1185z00_9091 = CINT(BgL_label1185z00_8454);
				BgL_z52mark1189z52_9095 = (long) CINT(BgL_z52mark1189z52_8458);
				BgL_gccnt1194z00_9098 = (long) CINT(BgL_gccnt1194z00_8463);
				BgL_gcmark1195z00_9099 = (long) CINT(BgL_gcmark1195z00_8464);
				BgL_asleep1199z00_9101 = CBOOL(BgL_asleep1199z00_8468);
				{	/* SawBbv/bbv-types.scm 53 */
					BgL_blockz00_bglt BgL_new1429z00_9102;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_blockz00_bglt BgL_tmp1427z00_9103;
						BgL_blocksz00_bglt BgL_wide1428z00_9104;

						{
							BgL_blockz00_bglt BgL_auxz00_12977;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_blockz00_bglt BgL_new1426z00_9105;

								BgL_new1426z00_9105 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawBbv/bbv-types.scm 53 */
									long BgL_arg2476z00_9106;

									BgL_arg2476z00_9106 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1426z00_9105),
										BgL_arg2476z00_9106);
								}
								{	/* SawBbv/bbv-types.scm 53 */
									BgL_objectz00_bglt BgL_tmpz00_12982;

									BgL_tmpz00_12982 = ((BgL_objectz00_bglt) BgL_new1426z00_9105);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12982, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1426z00_9105);
								BgL_auxz00_12977 = BgL_new1426z00_9105;
							}
							BgL_tmp1427z00_9103 = ((BgL_blockz00_bglt) BgL_auxz00_12977);
						}
						BgL_wide1428z00_9104 =
							((BgL_blocksz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_blocksz00_bgl))));
						{	/* SawBbv/bbv-types.scm 53 */
							obj_t BgL_auxz00_12990;
							BgL_objectz00_bglt BgL_tmpz00_12988;

							BgL_auxz00_12990 = ((obj_t) BgL_wide1428z00_9104);
							BgL_tmpz00_12988 = ((BgL_objectz00_bglt) BgL_tmp1427z00_9103);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12988, BgL_auxz00_12990);
						}
						((BgL_objectz00_bglt) BgL_tmp1427z00_9103);
						{	/* SawBbv/bbv-types.scm 53 */
							long BgL_arg2475z00_9107;

							BgL_arg2475z00_9107 =
								BGL_CLASS_NUM(BGl_blockSz00zzsaw_bbvzd2typeszd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1427z00_9103),
								BgL_arg2475z00_9107);
						}
						BgL_new1429z00_9102 = ((BgL_blockz00_bglt) BgL_tmp1427z00_9103);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1429z00_9102)))->BgL_labelz00) =
						((int) BgL_label1185z00_9091), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1429z00_9102)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1186z00_8455)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1429z00_9102)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1187z00_8456)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1429z00_9102)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1188z00_8457)), BUNSPEC);
					{
						BgL_blocksz00_bglt BgL_auxz00_13009;

						{
							obj_t BgL_auxz00_13010;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13011;

								BgL_tmpz00_13011 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13010 = BGL_OBJECT_WIDENING(BgL_tmpz00_13011);
							}
							BgL_auxz00_13009 = ((BgL_blocksz00_bglt) BgL_auxz00_13010);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13009))->
								BgL_z52markz52) = ((long) BgL_z52mark1189z52_9095), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13016;

						{
							obj_t BgL_auxz00_13017;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13018;

								BgL_tmpz00_13018 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13017 = BGL_OBJECT_WIDENING(BgL_tmpz00_13018);
							}
							BgL_auxz00_13016 = ((BgL_blocksz00_bglt) BgL_auxz00_13017);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13016))->
								BgL_z52hashz52) = ((obj_t) BgL_z52hash1190z52_8459), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13023;

						{
							obj_t BgL_auxz00_13024;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13025;

								BgL_tmpz00_13025 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13024 = BGL_OBJECT_WIDENING(BgL_tmpz00_13025);
							}
							BgL_auxz00_13023 = ((BgL_blocksz00_bglt) BgL_auxz00_13024);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13023))->
								BgL_z52blacklistz52) =
							((obj_t) BgL_z52blacklist1191z52_8460), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13030;

						{
							obj_t BgL_auxz00_13031;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13032;

								BgL_tmpz00_13032 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13031 = BGL_OBJECT_WIDENING(BgL_tmpz00_13032);
							}
							BgL_auxz00_13030 = ((BgL_blocksz00_bglt) BgL_auxz00_13031);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13030))->BgL_ctxz00) =
							((BgL_bbvzd2ctxzd2_bglt) ((BgL_bbvzd2ctxzd2_bglt)
									BgL_ctx1192z00_8461)), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13038;

						{
							obj_t BgL_auxz00_13039;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13040;

								BgL_tmpz00_13040 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13039 = BGL_OBJECT_WIDENING(BgL_tmpz00_13040);
							}
							BgL_auxz00_13038 = ((BgL_blocksz00_bglt) BgL_auxz00_13039);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13038))->BgL_parentz00) =
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
									BgL_parent1193z00_8462)), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13046;

						{
							obj_t BgL_auxz00_13047;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13048;

								BgL_tmpz00_13048 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13047 = BGL_OBJECT_WIDENING(BgL_tmpz00_13048);
							}
							BgL_auxz00_13046 = ((BgL_blocksz00_bglt) BgL_auxz00_13047);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13046))->BgL_gccntz00) =
							((long) BgL_gccnt1194z00_9098), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13053;

						{
							obj_t BgL_auxz00_13054;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13055;

								BgL_tmpz00_13055 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13054 = BGL_OBJECT_WIDENING(BgL_tmpz00_13055);
							}
							BgL_auxz00_13053 = ((BgL_blocksz00_bglt) BgL_auxz00_13054);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13053))->BgL_gcmarkz00) =
							((long) BgL_gcmark1195z00_9099), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13060;

						{
							obj_t BgL_auxz00_13061;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13062;

								BgL_tmpz00_13062 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13061 = BGL_OBJECT_WIDENING(BgL_tmpz00_13062);
							}
							BgL_auxz00_13060 = ((BgL_blocksz00_bglt) BgL_auxz00_13061);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13060))->BgL_mblockz00) =
							((obj_t) BgL_mblock1196z00_8465), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13067;

						{
							obj_t BgL_auxz00_13068;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13069;

								BgL_tmpz00_13069 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13068 = BGL_OBJECT_WIDENING(BgL_tmpz00_13069);
							}
							BgL_auxz00_13067 = ((BgL_blocksz00_bglt) BgL_auxz00_13068);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13067))->
								BgL_creatorz00) = ((obj_t) BgL_creator1197z00_8466), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13074;

						{
							obj_t BgL_auxz00_13075;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13076;

								BgL_tmpz00_13076 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13075 = BGL_OBJECT_WIDENING(BgL_tmpz00_13076);
							}
							BgL_auxz00_13074 = ((BgL_blocksz00_bglt) BgL_auxz00_13075);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13074))->BgL_mergesz00) =
							((obj_t) ((obj_t) BgL_merges1198z00_8467)), BUNSPEC);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_13082;

						{
							obj_t BgL_auxz00_13083;

							{	/* SawBbv/bbv-types.scm 53 */
								BgL_objectz00_bglt BgL_tmpz00_13084;

								BgL_tmpz00_13084 = ((BgL_objectz00_bglt) BgL_new1429z00_9102);
								BgL_auxz00_13083 = BGL_OBJECT_WIDENING(BgL_tmpz00_13084);
							}
							BgL_auxz00_13082 = ((BgL_blocksz00_bglt) BgL_auxz00_13083);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13082))->BgL_asleepz00) =
							((bool_t) BgL_asleep1199z00_9101), BUNSPEC);
					}
					return BgL_new1429z00_9102;
				}
			}
		}

	}



/* &<@anonymous:2571> */
	obj_t BGl_z62zc3z04anonymousza32571ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8469)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2570 */
	obj_t BGl_z62lambda2570z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8470,
		obj_t BgL_oz00_8471, obj_t BgL_vz00_8472)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				bool_t BgL_vz00_9109;

				BgL_vz00_9109 = CBOOL(BgL_vz00_8472);
				{
					BgL_blocksz00_bglt BgL_auxz00_13091;

					{
						obj_t BgL_auxz00_13092;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13093;

							BgL_tmpz00_13093 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8471));
							BgL_auxz00_13092 = BGL_OBJECT_WIDENING(BgL_tmpz00_13093);
						}
						BgL_auxz00_13091 = ((BgL_blocksz00_bglt) BgL_auxz00_13092);
					}
					return
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13091))->BgL_asleepz00) =
						((bool_t) BgL_vz00_9109), BUNSPEC);
				}
			}
		}

	}



/* &lambda2569 */
	obj_t BGl_z62lambda2569z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8473,
		obj_t BgL_oz00_8474)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				bool_t BgL_tmpz00_13099;

				{
					BgL_blocksz00_bglt BgL_auxz00_13100;

					{
						obj_t BgL_auxz00_13101;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13102;

							BgL_tmpz00_13102 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8474));
							BgL_auxz00_13101 = BGL_OBJECT_WIDENING(BgL_tmpz00_13102);
						}
						BgL_auxz00_13100 = ((BgL_blocksz00_bglt) BgL_auxz00_13101);
					}
					BgL_tmpz00_13099 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13100))->BgL_asleepz00);
				}
				return BBOOL(BgL_tmpz00_13099);
			}
		}

	}



/* &<@anonymous:2564> */
	obj_t BGl_z62zc3z04anonymousza32564ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8475)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BNIL;
		}

	}



/* &lambda2563 */
	obj_t BGl_z62lambda2563z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8476,
		obj_t BgL_oz00_8477, obj_t BgL_vz00_8478)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13109;

				{
					obj_t BgL_auxz00_13110;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13111;

						BgL_tmpz00_13111 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8477));
						BgL_auxz00_13110 = BGL_OBJECT_WIDENING(BgL_tmpz00_13111);
					}
					BgL_auxz00_13109 = ((BgL_blocksz00_bglt) BgL_auxz00_13110);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13109))->BgL_mergesz00) =
					((obj_t) ((obj_t) BgL_vz00_8478)), BUNSPEC);
			}
		}

	}



/* &lambda2562 */
	obj_t BGl_z62lambda2562z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8479,
		obj_t BgL_oz00_8480)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13118;

				{
					obj_t BgL_auxz00_13119;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13120;

						BgL_tmpz00_13120 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8480));
						BgL_auxz00_13119 = BGL_OBJECT_WIDENING(BgL_tmpz00_13120);
					}
					BgL_auxz00_13118 = ((BgL_blocksz00_bglt) BgL_auxz00_13119);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13118))->BgL_mergesz00);
			}
		}

	}



/* &lambda2555 */
	obj_t BGl_z62lambda2555z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8481,
		obj_t BgL_oz00_8482, obj_t BgL_vz00_8483)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13126;

				{
					obj_t BgL_auxz00_13127;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13128;

						BgL_tmpz00_13128 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8482));
						BgL_auxz00_13127 = BGL_OBJECT_WIDENING(BgL_tmpz00_13128);
					}
					BgL_auxz00_13126 = ((BgL_blocksz00_bglt) BgL_auxz00_13127);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13126))->BgL_creatorz00) =
					((obj_t) BgL_vz00_8483), BUNSPEC);
			}
		}

	}



/* &lambda2554 */
	obj_t BGl_z62lambda2554z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8484,
		obj_t BgL_oz00_8485)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13134;

				{
					obj_t BgL_auxz00_13135;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13136;

						BgL_tmpz00_13136 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8485));
						BgL_auxz00_13135 = BGL_OBJECT_WIDENING(BgL_tmpz00_13136);
					}
					BgL_auxz00_13134 = ((BgL_blocksz00_bglt) BgL_auxz00_13135);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13134))->BgL_creatorz00);
			}
		}

	}



/* &<@anonymous:2549> */
	obj_t BGl_z62zc3z04anonymousza32549ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8486)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2548 */
	obj_t BGl_z62lambda2548z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8487,
		obj_t BgL_oz00_8488, obj_t BgL_vz00_8489)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13143;

				{
					obj_t BgL_auxz00_13144;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13145;

						BgL_tmpz00_13145 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8488));
						BgL_auxz00_13144 = BGL_OBJECT_WIDENING(BgL_tmpz00_13145);
					}
					BgL_auxz00_13143 = ((BgL_blocksz00_bglt) BgL_auxz00_13144);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13143))->BgL_mblockz00) =
					((obj_t) BgL_vz00_8489), BUNSPEC);
			}
		}

	}



/* &lambda2547 */
	obj_t BGl_z62lambda2547z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8490,
		obj_t BgL_oz00_8491)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13151;

				{
					obj_t BgL_auxz00_13152;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13153;

						BgL_tmpz00_13153 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8491));
						BgL_auxz00_13152 = BGL_OBJECT_WIDENING(BgL_tmpz00_13153);
					}
					BgL_auxz00_13151 = ((BgL_blocksz00_bglt) BgL_auxz00_13152);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13151))->BgL_mblockz00);
			}
		}

	}



/* &<@anonymous:2541> */
	obj_t BGl_z62zc3z04anonymousza32541ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8492)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BINT(-1L);
		}

	}



/* &lambda2540 */
	obj_t BGl_z62lambda2540z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8493,
		obj_t BgL_oz00_8494, obj_t BgL_vz00_8495)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_vz00_9119;

				BgL_vz00_9119 = (long) CINT(BgL_vz00_8495);
				{
					BgL_blocksz00_bglt BgL_auxz00_13161;

					{
						obj_t BgL_auxz00_13162;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13163;

							BgL_tmpz00_13163 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8494));
							BgL_auxz00_13162 = BGL_OBJECT_WIDENING(BgL_tmpz00_13163);
						}
						BgL_auxz00_13161 = ((BgL_blocksz00_bglt) BgL_auxz00_13162);
					}
					return
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13161))->BgL_gcmarkz00) =
						((long) BgL_vz00_9119), BUNSPEC);
		}}}

	}



/* &lambda2539 */
	obj_t BGl_z62lambda2539z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8496,
		obj_t BgL_oz00_8497)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_tmpz00_13169;

				{
					BgL_blocksz00_bglt BgL_auxz00_13170;

					{
						obj_t BgL_auxz00_13171;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13172;

							BgL_tmpz00_13172 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8497));
							BgL_auxz00_13171 = BGL_OBJECT_WIDENING(BgL_tmpz00_13172);
						}
						BgL_auxz00_13170 = ((BgL_blocksz00_bglt) BgL_auxz00_13171);
					}
					BgL_tmpz00_13169 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13170))->BgL_gcmarkz00);
				}
				return BINT(BgL_tmpz00_13169);
			}
		}

	}



/* &<@anonymous:2530> */
	obj_t BGl_z62zc3z04anonymousza32530ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8498)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BINT(0L);
		}

	}



/* &lambda2529 */
	obj_t BGl_z62lambda2529z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8499,
		obj_t BgL_oz00_8500, obj_t BgL_vz00_8501)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_vz00_9122;

				BgL_vz00_9122 = (long) CINT(BgL_vz00_8501);
				{
					BgL_blocksz00_bglt BgL_auxz00_13181;

					{
						obj_t BgL_auxz00_13182;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13183;

							BgL_tmpz00_13183 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8500));
							BgL_auxz00_13182 = BGL_OBJECT_WIDENING(BgL_tmpz00_13183);
						}
						BgL_auxz00_13181 = ((BgL_blocksz00_bglt) BgL_auxz00_13182);
					}
					return
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13181))->BgL_gccntz00) =
						((long) BgL_vz00_9122), BUNSPEC);
		}}}

	}



/* &lambda2528 */
	obj_t BGl_z62lambda2528z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8502,
		obj_t BgL_oz00_8503)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_tmpz00_13189;

				{
					BgL_blocksz00_bglt BgL_auxz00_13190;

					{
						obj_t BgL_auxz00_13191;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13192;

							BgL_tmpz00_13192 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8503));
							BgL_auxz00_13191 = BGL_OBJECT_WIDENING(BgL_tmpz00_13192);
						}
						BgL_auxz00_13190 = ((BgL_blocksz00_bglt) BgL_auxz00_13191);
					}
					BgL_tmpz00_13189 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13190))->BgL_gccntz00);
				}
				return BINT(BgL_tmpz00_13189);
			}
		}

	}



/* &lambda2523 */
	obj_t BGl_z62lambda2523z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8504,
		obj_t BgL_oz00_8505, obj_t BgL_vz00_8506)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13199;

				{
					obj_t BgL_auxz00_13200;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13201;

						BgL_tmpz00_13201 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8505));
						BgL_auxz00_13200 = BGL_OBJECT_WIDENING(BgL_tmpz00_13201);
					}
					BgL_auxz00_13199 = ((BgL_blocksz00_bglt) BgL_auxz00_13200);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13199))->BgL_parentz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_8506)), BUNSPEC);
			}
		}

	}



/* &lambda2522 */
	BgL_blockz00_bglt BGl_z62lambda2522z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8507, obj_t BgL_oz00_8508)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13208;

				{
					obj_t BgL_auxz00_13209;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13210;

						BgL_tmpz00_13210 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8508));
						BgL_auxz00_13209 = BGL_OBJECT_WIDENING(BgL_tmpz00_13210);
					}
					BgL_auxz00_13208 = ((BgL_blocksz00_bglt) BgL_auxz00_13209);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13208))->BgL_parentz00);
			}
		}

	}



/* &lambda2517 */
	obj_t BGl_z62lambda2517z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8509,
		obj_t BgL_oz00_8510, obj_t BgL_vz00_8511)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13216;

				{
					obj_t BgL_auxz00_13217;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13218;

						BgL_tmpz00_13218 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8510));
						BgL_auxz00_13217 = BGL_OBJECT_WIDENING(BgL_tmpz00_13218);
					}
					BgL_auxz00_13216 = ((BgL_blocksz00_bglt) BgL_auxz00_13217);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13216))->BgL_ctxz00) =
					((BgL_bbvzd2ctxzd2_bglt) ((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_8511)),
					BUNSPEC);
			}
		}

	}



/* &lambda2516 */
	BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2516z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8512, obj_t BgL_oz00_8513)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13225;

				{
					obj_t BgL_auxz00_13226;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13227;

						BgL_tmpz00_13227 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8513));
						BgL_auxz00_13226 = BGL_OBJECT_WIDENING(BgL_tmpz00_13227);
					}
					BgL_auxz00_13225 = ((BgL_blocksz00_bglt) BgL_auxz00_13226);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13225))->BgL_ctxz00);
			}
		}

	}



/* &<@anonymous:2512> */
	obj_t BGl_z62zc3z04anonymousza32512ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8514)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BNIL;
		}

	}



/* &lambda2511 */
	obj_t BGl_z62lambda2511z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8515,
		obj_t BgL_oz00_8516, obj_t BgL_vz00_8517)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13233;

				{
					obj_t BgL_auxz00_13234;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13235;

						BgL_tmpz00_13235 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8516));
						BgL_auxz00_13234 = BGL_OBJECT_WIDENING(BgL_tmpz00_13235);
					}
					BgL_auxz00_13233 = ((BgL_blocksz00_bglt) BgL_auxz00_13234);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13233))->
						BgL_z52blacklistz52) = ((obj_t) BgL_vz00_8517), BUNSPEC);
			}
		}

	}



/* &lambda2510 */
	obj_t BGl_z62lambda2510z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8518,
		obj_t BgL_oz00_8519)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13241;

				{
					obj_t BgL_auxz00_13242;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13243;

						BgL_tmpz00_13243 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8519));
						BgL_auxz00_13242 = BGL_OBJECT_WIDENING(BgL_tmpz00_13243);
					}
					BgL_auxz00_13241 = ((BgL_blocksz00_bglt) BgL_auxz00_13242);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13241))->
					BgL_z52blacklistz52);
			}
		}

	}



/* &<@anonymous:2504> */
	obj_t BGl_z62zc3z04anonymousza32504ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8520)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2503 */
	obj_t BGl_z62lambda2503z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8521,
		obj_t BgL_oz00_8522, obj_t BgL_vz00_8523)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13250;

				{
					obj_t BgL_auxz00_13251;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13252;

						BgL_tmpz00_13252 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8522));
						BgL_auxz00_13251 = BGL_OBJECT_WIDENING(BgL_tmpz00_13252);
					}
					BgL_auxz00_13250 = ((BgL_blocksz00_bglt) BgL_auxz00_13251);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13250))->BgL_z52hashz52) =
					((obj_t) BgL_vz00_8523), BUNSPEC);
			}
		}

	}



/* &lambda2502 */
	obj_t BGl_z62lambda2502z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8524,
		obj_t BgL_oz00_8525)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{
				BgL_blocksz00_bglt BgL_auxz00_13258;

				{
					obj_t BgL_auxz00_13259;

					{	/* SawBbv/bbv-types.scm 53 */
						BgL_objectz00_bglt BgL_tmpz00_13260;

						BgL_tmpz00_13260 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8525));
						BgL_auxz00_13259 = BGL_OBJECT_WIDENING(BgL_tmpz00_13260);
					}
					BgL_auxz00_13258 = ((BgL_blocksz00_bglt) BgL_auxz00_13259);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13258))->BgL_z52hashz52);
			}
		}

	}



/* &<@anonymous:2493> */
	obj_t BGl_z62zc3z04anonymousza32493ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8526)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			return BINT(-1L);
		}

	}



/* &lambda2492 */
	obj_t BGl_z62lambda2492z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8527,
		obj_t BgL_oz00_8528, obj_t BgL_vz00_8529)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_vz00_9135;

				BgL_vz00_9135 = (long) CINT(BgL_vz00_8529);
				{
					BgL_blocksz00_bglt BgL_auxz00_13268;

					{
						obj_t BgL_auxz00_13269;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13270;

							BgL_tmpz00_13270 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8528));
							BgL_auxz00_13269 = BGL_OBJECT_WIDENING(BgL_tmpz00_13270);
						}
						BgL_auxz00_13268 = ((BgL_blocksz00_bglt) BgL_auxz00_13269);
					}
					return
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13268))->
							BgL_z52markz52) = ((long) BgL_vz00_9135), BUNSPEC);
		}}}

	}



/* &lambda2491 */
	obj_t BGl_z62lambda2491z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8530,
		obj_t BgL_oz00_8531)
	{
		{	/* SawBbv/bbv-types.scm 53 */
			{	/* SawBbv/bbv-types.scm 53 */
				long BgL_tmpz00_13276;

				{
					BgL_blocksz00_bglt BgL_auxz00_13277;

					{
						obj_t BgL_auxz00_13278;

						{	/* SawBbv/bbv-types.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_13279;

							BgL_tmpz00_13279 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8531));
							BgL_auxz00_13278 = BGL_OBJECT_WIDENING(BgL_tmpz00_13279);
						}
						BgL_auxz00_13277 = ((BgL_blocksz00_bglt) BgL_auxz00_13278);
					}
					BgL_tmpz00_13276 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13277))->BgL_z52markz52);
				}
				return BINT(BgL_tmpz00_13276);
			}
		}

	}



/* &lambda2430 */
	BgL_blockz00_bglt BGl_z62lambda2430z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8532, obj_t BgL_o1183z00_8533)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{	/* SawBbv/bbv-types.scm 46 */
				long BgL_arg2431z00_9138;

				{	/* SawBbv/bbv-types.scm 46 */
					obj_t BgL_arg2432z00_9139;

					{	/* SawBbv/bbv-types.scm 46 */
						obj_t BgL_arg2434z00_9140;

						{	/* SawBbv/bbv-types.scm 46 */
							obj_t BgL_arg1815z00_9141;
							long BgL_arg1816z00_9142;

							BgL_arg1815z00_9141 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawBbv/bbv-types.scm 46 */
								long BgL_arg1817z00_9143;

								BgL_arg1817z00_9143 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1183z00_8533)));
								BgL_arg1816z00_9142 = (BgL_arg1817z00_9143 - OBJECT_TYPE);
							}
							BgL_arg2434z00_9140 =
								VECTOR_REF(BgL_arg1815z00_9141, BgL_arg1816z00_9142);
						}
						BgL_arg2432z00_9139 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2434z00_9140);
					}
					{	/* SawBbv/bbv-types.scm 46 */
						obj_t BgL_tmpz00_13293;

						BgL_tmpz00_13293 = ((obj_t) BgL_arg2432z00_9139);
						BgL_arg2431z00_9138 = BGL_CLASS_NUM(BgL_tmpz00_13293);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1183z00_8533)), BgL_arg2431z00_9138);
			}
			{	/* SawBbv/bbv-types.scm 46 */
				BgL_objectz00_bglt BgL_tmpz00_13299;

				BgL_tmpz00_13299 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1183z00_8533));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13299, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1183z00_8533));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1183z00_8533));
		}

	}



/* &<@anonymous:2429> */
	obj_t BGl_z62zc3z04anonymousza32429ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8534, obj_t BgL_new1182z00_8535)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockz00_bglt BgL_auxz00_13307;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1182z00_8535))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1182z00_8535))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1182z00_8535))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1182z00_8535))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_blockvz00_bglt BgL_auxz00_13321;

					{
						obj_t BgL_auxz00_13322;

						{	/* SawBbv/bbv-types.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_13323;

							BgL_tmpz00_13323 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1182z00_8535));
							BgL_auxz00_13322 = BGL_OBJECT_WIDENING(BgL_tmpz00_13323);
						}
						BgL_auxz00_13321 = ((BgL_blockvz00_bglt) BgL_auxz00_13322);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13321))->BgL_versionsz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_13329;

					{
						obj_t BgL_auxz00_13330;

						{	/* SawBbv/bbv-types.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_13331;

							BgL_tmpz00_13331 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1182z00_8535));
							BgL_auxz00_13330 = BGL_OBJECT_WIDENING(BgL_tmpz00_13331);
						}
						BgL_auxz00_13329 = ((BgL_blockvz00_bglt) BgL_auxz00_13330);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13329))->BgL_genericz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_13337;

					{
						obj_t BgL_auxz00_13338;

						{	/* SawBbv/bbv-types.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_13339;

							BgL_tmpz00_13339 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1182z00_8535));
							BgL_auxz00_13338 = BGL_OBJECT_WIDENING(BgL_tmpz00_13339);
						}
						BgL_auxz00_13337 = ((BgL_blockvz00_bglt) BgL_auxz00_13338);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13337))->BgL_z52markz52) =
						((long) 0L), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_13345;

					{
						obj_t BgL_auxz00_13346;

						{	/* SawBbv/bbv-types.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_13347;

							BgL_tmpz00_13347 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1182z00_8535));
							BgL_auxz00_13346 = BGL_OBJECT_WIDENING(BgL_tmpz00_13347);
						}
						BgL_auxz00_13345 = ((BgL_blockvz00_bglt) BgL_auxz00_13346);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13345))->BgL_mergez00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_13307 = ((BgL_blockz00_bglt) BgL_new1182z00_8535);
				return ((obj_t) BgL_auxz00_13307);
			}
		}

	}



/* &lambda2427 */
	BgL_blockz00_bglt BGl_z62lambda2427z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8536, obj_t BgL_o1178z00_8537)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{	/* SawBbv/bbv-types.scm 46 */
				BgL_blockvz00_bglt BgL_wide1180z00_9146;

				BgL_wide1180z00_9146 =
					((BgL_blockvz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_blockvz00_bgl))));
				{	/* SawBbv/bbv-types.scm 46 */
					obj_t BgL_auxz00_13360;
					BgL_objectz00_bglt BgL_tmpz00_13356;

					BgL_auxz00_13360 = ((obj_t) BgL_wide1180z00_9146);
					BgL_tmpz00_13356 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1178z00_8537)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13356, BgL_auxz00_13360);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1178z00_8537)));
				{	/* SawBbv/bbv-types.scm 46 */
					long BgL_arg2428z00_9147;

					BgL_arg2428z00_9147 =
						BGL_CLASS_NUM(BGl_blockVz00zzsaw_bbvzd2typeszd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1178z00_8537))), BgL_arg2428z00_9147);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1178z00_8537)));
			}
		}

	}



/* &lambda2424 */
	BgL_blockz00_bglt BGl_z62lambda2424z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8538, obj_t BgL_label1170z00_8539, obj_t BgL_preds1171z00_8540,
		obj_t BgL_succs1172z00_8541, obj_t BgL_first1173z00_8542,
		obj_t BgL_versions1174z00_8543, obj_t BgL_generic1175z00_8544,
		obj_t BgL_z52mark1176z52_8545, obj_t BgL_merge1177z00_8546)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{	/* SawBbv/bbv-types.scm 46 */
				int BgL_label1170z00_9148;
				long BgL_z52mark1176z52_9153;

				BgL_label1170z00_9148 = CINT(BgL_label1170z00_8539);
				BgL_z52mark1176z52_9153 = (long) CINT(BgL_z52mark1176z52_8545);
				{	/* SawBbv/bbv-types.scm 46 */
					BgL_blockz00_bglt BgL_new1424z00_9154;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_blockz00_bglt BgL_tmp1422z00_9155;
						BgL_blockvz00_bglt BgL_wide1423z00_9156;

						{
							BgL_blockz00_bglt BgL_auxz00_13376;

							{	/* SawBbv/bbv-types.scm 46 */
								BgL_blockz00_bglt BgL_new1421z00_9157;

								BgL_new1421z00_9157 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawBbv/bbv-types.scm 46 */
									long BgL_arg2426z00_9158;

									BgL_arg2426z00_9158 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1421z00_9157),
										BgL_arg2426z00_9158);
								}
								{	/* SawBbv/bbv-types.scm 46 */
									BgL_objectz00_bglt BgL_tmpz00_13381;

									BgL_tmpz00_13381 = ((BgL_objectz00_bglt) BgL_new1421z00_9157);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13381, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1421z00_9157);
								BgL_auxz00_13376 = BgL_new1421z00_9157;
							}
							BgL_tmp1422z00_9155 = ((BgL_blockz00_bglt) BgL_auxz00_13376);
						}
						BgL_wide1423z00_9156 =
							((BgL_blockvz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_blockvz00_bgl))));
						{	/* SawBbv/bbv-types.scm 46 */
							obj_t BgL_auxz00_13389;
							BgL_objectz00_bglt BgL_tmpz00_13387;

							BgL_auxz00_13389 = ((obj_t) BgL_wide1423z00_9156);
							BgL_tmpz00_13387 = ((BgL_objectz00_bglt) BgL_tmp1422z00_9155);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13387, BgL_auxz00_13389);
						}
						((BgL_objectz00_bglt) BgL_tmp1422z00_9155);
						{	/* SawBbv/bbv-types.scm 46 */
							long BgL_arg2425z00_9159;

							BgL_arg2425z00_9159 =
								BGL_CLASS_NUM(BGl_blockVz00zzsaw_bbvzd2typeszd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1422z00_9155),
								BgL_arg2425z00_9159);
						}
						BgL_new1424z00_9154 = ((BgL_blockz00_bglt) BgL_tmp1422z00_9155);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1424z00_9154)))->BgL_labelz00) =
						((int) BgL_label1170z00_9148), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1424z00_9154)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1171z00_8540)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1424z00_9154)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1172z00_8541)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1424z00_9154)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1173z00_8542)), BUNSPEC);
					{
						BgL_blockvz00_bglt BgL_auxz00_13408;

						{
							obj_t BgL_auxz00_13409;

							{	/* SawBbv/bbv-types.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_13410;

								BgL_tmpz00_13410 = ((BgL_objectz00_bglt) BgL_new1424z00_9154);
								BgL_auxz00_13409 = BGL_OBJECT_WIDENING(BgL_tmpz00_13410);
							}
							BgL_auxz00_13408 = ((BgL_blockvz00_bglt) BgL_auxz00_13409);
						}
						((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13408))->
								BgL_versionsz00) =
							((obj_t) ((obj_t) BgL_versions1174z00_8543)), BUNSPEC);
					}
					{
						BgL_blockvz00_bglt BgL_auxz00_13416;

						{
							obj_t BgL_auxz00_13417;

							{	/* SawBbv/bbv-types.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_13418;

								BgL_tmpz00_13418 = ((BgL_objectz00_bglt) BgL_new1424z00_9154);
								BgL_auxz00_13417 = BGL_OBJECT_WIDENING(BgL_tmpz00_13418);
							}
							BgL_auxz00_13416 = ((BgL_blockvz00_bglt) BgL_auxz00_13417);
						}
						((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13416))->
								BgL_genericz00) = ((obj_t) BgL_generic1175z00_8544), BUNSPEC);
					}
					{
						BgL_blockvz00_bglt BgL_auxz00_13423;

						{
							obj_t BgL_auxz00_13424;

							{	/* SawBbv/bbv-types.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_13425;

								BgL_tmpz00_13425 = ((BgL_objectz00_bglt) BgL_new1424z00_9154);
								BgL_auxz00_13424 = BGL_OBJECT_WIDENING(BgL_tmpz00_13425);
							}
							BgL_auxz00_13423 = ((BgL_blockvz00_bglt) BgL_auxz00_13424);
						}
						((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13423))->
								BgL_z52markz52) = ((long) BgL_z52mark1176z52_9153), BUNSPEC);
					}
					{
						BgL_blockvz00_bglt BgL_auxz00_13430;

						{
							obj_t BgL_auxz00_13431;

							{	/* SawBbv/bbv-types.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_13432;

								BgL_tmpz00_13432 = ((BgL_objectz00_bglt) BgL_new1424z00_9154);
								BgL_auxz00_13431 = BGL_OBJECT_WIDENING(BgL_tmpz00_13432);
							}
							BgL_auxz00_13430 = ((BgL_blockvz00_bglt) BgL_auxz00_13431);
						}
						((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13430))->BgL_mergez00) =
							((obj_t) BgL_merge1177z00_8546), BUNSPEC);
					}
					return BgL_new1424z00_9154;
				}
			}
		}

	}



/* &<@anonymous:2463> */
	obj_t BGl_z62zc3z04anonymousza32463ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8547)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			return BUNSPEC;
		}

	}



/* &lambda2462 */
	obj_t BGl_z62lambda2462z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8548,
		obj_t BgL_oz00_8549, obj_t BgL_vz00_8550)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockvz00_bglt BgL_auxz00_13437;

				{
					obj_t BgL_auxz00_13438;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_13439;

						BgL_tmpz00_13439 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8549));
						BgL_auxz00_13438 = BGL_OBJECT_WIDENING(BgL_tmpz00_13439);
					}
					BgL_auxz00_13437 = ((BgL_blockvz00_bglt) BgL_auxz00_13438);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13437))->BgL_mergez00) =
					((obj_t) BgL_vz00_8550), BUNSPEC);
			}
		}

	}



/* &lambda2461 */
	obj_t BGl_z62lambda2461z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8551,
		obj_t BgL_oz00_8552)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockvz00_bglt BgL_auxz00_13445;

				{
					obj_t BgL_auxz00_13446;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_13447;

						BgL_tmpz00_13447 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8552));
						BgL_auxz00_13446 = BGL_OBJECT_WIDENING(BgL_tmpz00_13447);
					}
					BgL_auxz00_13445 = ((BgL_blockvz00_bglt) BgL_auxz00_13446);
				}
				return (((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13445))->BgL_mergez00);
			}
		}

	}



/* &<@anonymous:2456> */
	obj_t BGl_z62zc3z04anonymousza32456ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8553)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			return BINT(-1L);
		}

	}



/* &lambda2455 */
	obj_t BGl_z62lambda2455z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8554,
		obj_t BgL_oz00_8555, obj_t BgL_vz00_8556)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{	/* SawBbv/bbv-types.scm 46 */
				long BgL_vz00_9163;

				BgL_vz00_9163 = (long) CINT(BgL_vz00_8556);
				{
					BgL_blockvz00_bglt BgL_auxz00_13455;

					{
						obj_t BgL_auxz00_13456;

						{	/* SawBbv/bbv-types.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_13457;

							BgL_tmpz00_13457 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8555));
							BgL_auxz00_13456 = BGL_OBJECT_WIDENING(BgL_tmpz00_13457);
						}
						BgL_auxz00_13455 = ((BgL_blockvz00_bglt) BgL_auxz00_13456);
					}
					return
						((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13455))->
							BgL_z52markz52) = ((long) BgL_vz00_9163), BUNSPEC);
		}}}

	}



/* &lambda2454 */
	obj_t BGl_z62lambda2454z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8557,
		obj_t BgL_oz00_8558)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{	/* SawBbv/bbv-types.scm 46 */
				long BgL_tmpz00_13463;

				{
					BgL_blockvz00_bglt BgL_auxz00_13464;

					{
						obj_t BgL_auxz00_13465;

						{	/* SawBbv/bbv-types.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_13466;

							BgL_tmpz00_13466 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8558));
							BgL_auxz00_13465 = BGL_OBJECT_WIDENING(BgL_tmpz00_13466);
						}
						BgL_auxz00_13464 = ((BgL_blockvz00_bglt) BgL_auxz00_13465);
					}
					BgL_tmpz00_13463 =
						(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13464))->BgL_z52markz52);
				}
				return BINT(BgL_tmpz00_13463);
			}
		}

	}



/* &<@anonymous:2449> */
	obj_t BGl_z62zc3z04anonymousza32449ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8559)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			return BUNSPEC;
		}

	}



/* &lambda2448 */
	obj_t BGl_z62lambda2448z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8560,
		obj_t BgL_oz00_8561, obj_t BgL_vz00_8562)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockvz00_bglt BgL_auxz00_13473;

				{
					obj_t BgL_auxz00_13474;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_13475;

						BgL_tmpz00_13475 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8561));
						BgL_auxz00_13474 = BGL_OBJECT_WIDENING(BgL_tmpz00_13475);
					}
					BgL_auxz00_13473 = ((BgL_blockvz00_bglt) BgL_auxz00_13474);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13473))->BgL_genericz00) =
					((obj_t) BgL_vz00_8562), BUNSPEC);
			}
		}

	}



/* &lambda2447 */
	obj_t BGl_z62lambda2447z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8563,
		obj_t BgL_oz00_8564)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockvz00_bglt BgL_auxz00_13481;

				{
					obj_t BgL_auxz00_13482;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_13483;

						BgL_tmpz00_13483 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8564));
						BgL_auxz00_13482 = BGL_OBJECT_WIDENING(BgL_tmpz00_13483);
					}
					BgL_auxz00_13481 = ((BgL_blockvz00_bglt) BgL_auxz00_13482);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13481))->BgL_genericz00);
			}
		}

	}



/* &<@anonymous:2442> */
	obj_t BGl_z62zc3z04anonymousza32442ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8565)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			return BNIL;
		}

	}



/* &lambda2441 */
	obj_t BGl_z62lambda2441z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8566,
		obj_t BgL_oz00_8567, obj_t BgL_vz00_8568)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockvz00_bglt BgL_auxz00_13489;

				{
					obj_t BgL_auxz00_13490;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_13491;

						BgL_tmpz00_13491 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8567));
						BgL_auxz00_13490 = BGL_OBJECT_WIDENING(BgL_tmpz00_13491);
					}
					BgL_auxz00_13489 = ((BgL_blockvz00_bglt) BgL_auxz00_13490);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13489))->BgL_versionsz00) =
					((obj_t) ((obj_t) BgL_vz00_8568)), BUNSPEC);
			}
		}

	}



/* &lambda2440 */
	obj_t BGl_z62lambda2440z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8569,
		obj_t BgL_oz00_8570)
	{
		{	/* SawBbv/bbv-types.scm 46 */
			{
				BgL_blockvz00_bglt BgL_auxz00_13498;

				{
					obj_t BgL_auxz00_13499;

					{	/* SawBbv/bbv-types.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_13500;

						BgL_tmpz00_13500 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8570));
						BgL_auxz00_13499 = BGL_OBJECT_WIDENING(BgL_tmpz00_13500);
					}
					BgL_auxz00_13498 = ((BgL_blockvz00_bglt) BgL_auxz00_13499);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_13498))->BgL_versionsz00);
			}
		}

	}



/* &lambda2372 */
	BgL_rtl_insz00_bglt BGl_z62lambda2372z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8571, obj_t BgL_o1168z00_8572)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{	/* SawBbv/bbv-types.scm 38 */
				long BgL_arg2373z00_9171;

				{	/* SawBbv/bbv-types.scm 38 */
					obj_t BgL_arg2374z00_9172;

					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_arg2375z00_9173;

						{	/* SawBbv/bbv-types.scm 38 */
							obj_t BgL_arg1815z00_9174;
							long BgL_arg1816z00_9175;

							BgL_arg1815z00_9174 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawBbv/bbv-types.scm 38 */
								long BgL_arg1817z00_9176;

								BgL_arg1817z00_9176 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_o1168z00_8572)));
								BgL_arg1816z00_9175 = (BgL_arg1817z00_9176 - OBJECT_TYPE);
							}
							BgL_arg2375z00_9173 =
								VECTOR_REF(BgL_arg1815z00_9174, BgL_arg1816z00_9175);
						}
						BgL_arg2374z00_9172 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2375z00_9173);
					}
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_tmpz00_13513;

						BgL_tmpz00_13513 = ((obj_t) BgL_arg2374z00_9172);
						BgL_arg2373z00_9171 = BGL_CLASS_NUM(BgL_tmpz00_13513);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) BgL_o1168z00_8572)), BgL_arg2373z00_9171);
			}
			{	/* SawBbv/bbv-types.scm 38 */
				BgL_objectz00_bglt BgL_tmpz00_13519;

				BgL_tmpz00_13519 =
					((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1168z00_8572));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13519, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1168z00_8572));
			return ((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1168z00_8572));
		}

	}



/* &<@anonymous:2371> */
	obj_t BGl_z62zc3z04anonymousza32371ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8573, obj_t BgL_new1167z00_8574)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_insz00_bglt BgL_auxz00_13527;

				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1167z00_8574))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1167z00_8574))))->BgL_z52spillz52) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1167z00_8574))))->BgL_destz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_rtl_funz00_bglt BgL_auxz00_13537;

					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_classz00_9178;

						BgL_classz00_9178 = BGl_rtl_funz00zzsaw_defsz00;
						{	/* SawBbv/bbv-types.scm 38 */
							obj_t BgL__ortest_1117z00_9179;

							BgL__ortest_1117z00_9179 = BGL_CLASS_NIL(BgL_classz00_9178);
							if (CBOOL(BgL__ortest_1117z00_9179))
								{	/* SawBbv/bbv-types.scm 38 */
									BgL_auxz00_13537 =
										((BgL_rtl_funz00_bglt) BgL__ortest_1117z00_9179);
								}
							else
								{	/* SawBbv/bbv-types.scm 38 */
									BgL_auxz00_13537 =
										((BgL_rtl_funz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9178));
								}
						}
					}
					((((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_new1167z00_8574))))->
							BgL_funz00) = ((BgL_rtl_funz00_bglt) BgL_auxz00_13537), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1167z00_8574))))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13550;

					{
						obj_t BgL_auxz00_13551;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13552;

							BgL_tmpz00_13552 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1167z00_8574));
							BgL_auxz00_13551 = BGL_OBJECT_WIDENING(BgL_tmpz00_13552);
						}
						BgL_auxz00_13550 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13551);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13550))->
							BgL_defz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13558;

					{
						obj_t BgL_auxz00_13559;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13560;

							BgL_tmpz00_13560 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1167z00_8574));
							BgL_auxz00_13559 = BGL_OBJECT_WIDENING(BgL_tmpz00_13560);
						}
						BgL_auxz00_13558 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13559);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13558))->
							BgL_outz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13566;

					{
						obj_t BgL_auxz00_13567;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13568;

							BgL_tmpz00_13568 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1167z00_8574));
							BgL_auxz00_13567 = BGL_OBJECT_WIDENING(BgL_tmpz00_13568);
						}
						BgL_auxz00_13566 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13567);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13566))->
							BgL_inz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_bbvzd2ctxzd2_bglt BgL_auxz00_13581;
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13574;

					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_classz00_9180;

						BgL_classz00_9180 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
						{	/* SawBbv/bbv-types.scm 38 */
							obj_t BgL__ortest_1117z00_9181;

							BgL__ortest_1117z00_9181 = BGL_CLASS_NIL(BgL_classz00_9180);
							if (CBOOL(BgL__ortest_1117z00_9181))
								{	/* SawBbv/bbv-types.scm 38 */
									BgL_auxz00_13581 =
										((BgL_bbvzd2ctxzd2_bglt) BgL__ortest_1117z00_9181);
								}
							else
								{	/* SawBbv/bbv-types.scm 38 */
									BgL_auxz00_13581 =
										((BgL_bbvzd2ctxzd2_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9180));
								}
						}
					}
					{
						obj_t BgL_auxz00_13575;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13576;

							BgL_tmpz00_13576 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1167z00_8574));
							BgL_auxz00_13575 = BGL_OBJECT_WIDENING(BgL_tmpz00_13576);
						}
						BgL_auxz00_13574 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13575);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13574))->
							BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_auxz00_13581), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13589;

					{
						obj_t BgL_auxz00_13590;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13591;

							BgL_tmpz00_13591 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1167z00_8574));
							BgL_auxz00_13590 = BGL_OBJECT_WIDENING(BgL_tmpz00_13591);
						}
						BgL_auxz00_13589 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13590);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13589))->
							BgL_z52hashz52) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_13527 = ((BgL_rtl_insz00_bglt) BgL_new1167z00_8574);
				return ((obj_t) BgL_auxz00_13527);
			}
		}

	}



/* &lambda2369 */
	BgL_rtl_insz00_bglt BGl_z62lambda2369z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8575, obj_t BgL_o1164z00_8576)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{	/* SawBbv/bbv-types.scm 38 */
				BgL_rtl_inszf2bbvzf2_bglt BgL_wide1166z00_9183;

				BgL_wide1166z00_9183 =
					((BgL_rtl_inszf2bbvzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_inszf2bbvzf2_bgl))));
				{	/* SawBbv/bbv-types.scm 38 */
					obj_t BgL_auxz00_13604;
					BgL_objectz00_bglt BgL_tmpz00_13600;

					BgL_auxz00_13604 = ((obj_t) BgL_wide1166z00_9183);
					BgL_tmpz00_13600 =
						((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1164z00_8576)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13600, BgL_auxz00_13604);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1164z00_8576)));
				{	/* SawBbv/bbv-types.scm 38 */
					long BgL_arg2370z00_9184;

					BgL_arg2370z00_9184 =
						BGL_CLASS_NUM(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_insz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_o1164z00_8576))),
						BgL_arg2370z00_9184);
				}
				return
					((BgL_rtl_insz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1164z00_8576)));
			}
		}

	}



/* &lambda2366 */
	BgL_rtl_insz00_bglt BGl_z62lambda2366z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8577, obj_t BgL_loc1154z00_8578, obj_t BgL_z52spill1155z52_8579,
		obj_t BgL_dest1156z00_8580, obj_t BgL_fun1157z00_8581,
		obj_t BgL_args1158z00_8582, obj_t BgL_def1159z00_8583,
		obj_t BgL_out1160z00_8584, obj_t BgL_in1161z00_8585,
		obj_t BgL_ctx1162z00_8586, obj_t BgL_z52hash1163z52_8587)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{	/* SawBbv/bbv-types.scm 38 */
				BgL_rtl_insz00_bglt BgL_new1417z00_9189;

				{	/* SawBbv/bbv-types.scm 38 */
					BgL_rtl_insz00_bglt BgL_tmp1415z00_9190;
					BgL_rtl_inszf2bbvzf2_bglt BgL_wide1416z00_9191;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_13618;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_rtl_insz00_bglt BgL_new1414z00_9192;

							BgL_new1414z00_9192 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawBbv/bbv-types.scm 38 */
								long BgL_arg2368z00_9193;

								BgL_arg2368z00_9193 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1414z00_9192),
									BgL_arg2368z00_9193);
							}
							{	/* SawBbv/bbv-types.scm 38 */
								BgL_objectz00_bglt BgL_tmpz00_13623;

								BgL_tmpz00_13623 = ((BgL_objectz00_bglt) BgL_new1414z00_9192);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13623, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1414z00_9192);
							BgL_auxz00_13618 = BgL_new1414z00_9192;
						}
						BgL_tmp1415z00_9190 = ((BgL_rtl_insz00_bglt) BgL_auxz00_13618);
					}
					BgL_wide1416z00_9191 =
						((BgL_rtl_inszf2bbvzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_inszf2bbvzf2_bgl))));
					{	/* SawBbv/bbv-types.scm 38 */
						obj_t BgL_auxz00_13631;
						BgL_objectz00_bglt BgL_tmpz00_13629;

						BgL_auxz00_13631 = ((obj_t) BgL_wide1416z00_9191);
						BgL_tmpz00_13629 = ((BgL_objectz00_bglt) BgL_tmp1415z00_9190);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13629, BgL_auxz00_13631);
					}
					((BgL_objectz00_bglt) BgL_tmp1415z00_9190);
					{	/* SawBbv/bbv-types.scm 38 */
						long BgL_arg2367z00_9194;

						BgL_arg2367z00_9194 =
							BGL_CLASS_NUM(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1415z00_9190), BgL_arg2367z00_9194);
					}
					BgL_new1417z00_9189 = ((BgL_rtl_insz00_bglt) BgL_tmp1415z00_9190);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1417z00_9189)))->BgL_locz00) =
					((obj_t) BgL_loc1154z00_8578), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1417z00_9189)))->BgL_z52spillz52) =
					((obj_t) ((obj_t) BgL_z52spill1155z52_8579)), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1417z00_9189)))->BgL_destz00) =
					((obj_t) BgL_dest1156z00_8580), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1417z00_9189)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_fun1157z00_8581)),
					BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1417z00_9189)))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1158z00_8582)), BUNSPEC);
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13652;

					{
						obj_t BgL_auxz00_13653;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13654;

							BgL_tmpz00_13654 = ((BgL_objectz00_bglt) BgL_new1417z00_9189);
							BgL_auxz00_13653 = BGL_OBJECT_WIDENING(BgL_tmpz00_13654);
						}
						BgL_auxz00_13652 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13653);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13652))->
							BgL_defz00) = ((obj_t) BgL_def1159z00_8583), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13659;

					{
						obj_t BgL_auxz00_13660;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13661;

							BgL_tmpz00_13661 = ((BgL_objectz00_bglt) BgL_new1417z00_9189);
							BgL_auxz00_13660 = BGL_OBJECT_WIDENING(BgL_tmpz00_13661);
						}
						BgL_auxz00_13659 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13660);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13659))->
							BgL_outz00) = ((obj_t) BgL_out1160z00_8584), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13666;

					{
						obj_t BgL_auxz00_13667;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13668;

							BgL_tmpz00_13668 = ((BgL_objectz00_bglt) BgL_new1417z00_9189);
							BgL_auxz00_13667 = BGL_OBJECT_WIDENING(BgL_tmpz00_13668);
						}
						BgL_auxz00_13666 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13667);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13666))->
							BgL_inz00) = ((obj_t) BgL_in1161z00_8585), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13673;

					{
						obj_t BgL_auxz00_13674;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13675;

							BgL_tmpz00_13675 = ((BgL_objectz00_bglt) BgL_new1417z00_9189);
							BgL_auxz00_13674 = BGL_OBJECT_WIDENING(BgL_tmpz00_13675);
						}
						BgL_auxz00_13673 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13674);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13673))->
							BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) ((BgL_bbvzd2ctxzd2_bglt)
								BgL_ctx1162z00_8586)), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13681;

					{
						obj_t BgL_auxz00_13682;

						{	/* SawBbv/bbv-types.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_13683;

							BgL_tmpz00_13683 = ((BgL_objectz00_bglt) BgL_new1417z00_9189);
							BgL_auxz00_13682 = BGL_OBJECT_WIDENING(BgL_tmpz00_13683);
						}
						BgL_auxz00_13681 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13682);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13681))->
							BgL_z52hashz52) = ((obj_t) BgL_z52hash1163z52_8587), BUNSPEC);
				}
				return BgL_new1417z00_9189;
			}
		}

	}



/* &<@anonymous:2415> */
	obj_t BGl_z62zc3z04anonymousza32415ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8588)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2414 */
	obj_t BGl_z62lambda2414z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8589,
		obj_t BgL_oz00_8590, obj_t BgL_vz00_8591)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13689;

				{
					obj_t BgL_auxz00_13690;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13691;

						BgL_tmpz00_13691 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8590));
						BgL_auxz00_13690 = BGL_OBJECT_WIDENING(BgL_tmpz00_13691);
					}
					BgL_auxz00_13689 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13690);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13689))->
						BgL_z52hashz52) = ((obj_t) BgL_vz00_8591), BUNSPEC);
			}
		}

	}



/* &lambda2413 */
	obj_t BGl_z62lambda2413z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8592,
		obj_t BgL_oz00_8593)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13697;

				{
					obj_t BgL_auxz00_13698;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13699;

						BgL_tmpz00_13699 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8593));
						BgL_auxz00_13698 = BGL_OBJECT_WIDENING(BgL_tmpz00_13699);
					}
					BgL_auxz00_13697 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13698);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13697))->
					BgL_z52hashz52);
			}
		}

	}



/* &<@anonymous:2404> */
	obj_t BGl_z62zc3z04anonymousza32404ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8594)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_bbvzd2ctxzd2_bglt BgL_auxz00_13705;

				{	/* SawBbv/bbv-types.scm 42 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1420z00_9197;

					{	/* SawBbv/bbv-types.scm 42 */
						BgL_bbvzd2ctxzd2_bglt BgL_new1419z00_9198;

						BgL_new1419z00_9198 =
							((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2ctxzd2_bgl))));
						{	/* SawBbv/bbv-types.scm 42 */
							long BgL_arg2407z00_9199;

							BgL_arg2407z00_9199 =
								BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1419z00_9198),
								BgL_arg2407z00_9199);
						}
						BgL_new1420z00_9197 = BgL_new1419z00_9198;
					}
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1420z00_9197))->BgL_idz00) =
						((long) -1L), BUNSPEC);
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1420z00_9197))->
							BgL_entriesz00) = ((obj_t) BNIL), BUNSPEC);
					{	/* SawBbv/bbv-types.scm 42 */
						obj_t BgL_fun2405z00_9200;

						BgL_fun2405z00_9200 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_PROCEDURE_CALL1(BgL_fun2405z00_9200,
							((obj_t) BgL_new1420z00_9197));
					}
					BgL_auxz00_13705 = BgL_new1420z00_9197;
				}
				return ((obj_t) BgL_auxz00_13705);
			}
		}

	}



/* &lambda2403 */
	obj_t BGl_z62lambda2403z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8595,
		obj_t BgL_oz00_8596, obj_t BgL_vz00_8597)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13719;

				{
					obj_t BgL_auxz00_13720;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13721;

						BgL_tmpz00_13721 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8596));
						BgL_auxz00_13720 = BGL_OBJECT_WIDENING(BgL_tmpz00_13721);
					}
					BgL_auxz00_13719 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13720);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13719))->
						BgL_ctxz00) =
					((BgL_bbvzd2ctxzd2_bglt) ((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_8597)),
					BUNSPEC);
			}
		}

	}



/* &lambda2402 */
	BgL_bbvzd2ctxzd2_bglt BGl_z62lambda2402z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8598, obj_t BgL_oz00_8599)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13728;

				{
					obj_t BgL_auxz00_13729;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13730;

						BgL_tmpz00_13730 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8599));
						BgL_auxz00_13729 = BGL_OBJECT_WIDENING(BgL_tmpz00_13730);
					}
					BgL_auxz00_13728 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13729);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13728))->BgL_ctxz00);
			}
		}

	}



/* &<@anonymous:2396> */
	obj_t BGl_z62zc3z04anonymousza32396ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8600)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			return BUNSPEC;
		}

	}



/* &lambda2395 */
	obj_t BGl_z62lambda2395z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8601,
		obj_t BgL_oz00_8602, obj_t BgL_vz00_8603)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13736;

				{
					obj_t BgL_auxz00_13737;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13738;

						BgL_tmpz00_13738 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8602));
						BgL_auxz00_13737 = BGL_OBJECT_WIDENING(BgL_tmpz00_13738);
					}
					BgL_auxz00_13736 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13737);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13736))->
						BgL_inz00) = ((obj_t) BgL_vz00_8603), BUNSPEC);
			}
		}

	}



/* &lambda2394 */
	obj_t BGl_z62lambda2394z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8604,
		obj_t BgL_oz00_8605)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13744;

				{
					obj_t BgL_auxz00_13745;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13746;

						BgL_tmpz00_13746 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8605));
						BgL_auxz00_13745 = BGL_OBJECT_WIDENING(BgL_tmpz00_13746);
					}
					BgL_auxz00_13744 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13745);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13744))->BgL_inz00);
			}
		}

	}



/* &<@anonymous:2389> */
	obj_t BGl_z62zc3z04anonymousza32389ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8606)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			return BUNSPEC;
		}

	}



/* &lambda2388 */
	obj_t BGl_z62lambda2388z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8607,
		obj_t BgL_oz00_8608, obj_t BgL_vz00_8609)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13752;

				{
					obj_t BgL_auxz00_13753;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13754;

						BgL_tmpz00_13754 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8608));
						BgL_auxz00_13753 = BGL_OBJECT_WIDENING(BgL_tmpz00_13754);
					}
					BgL_auxz00_13752 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13753);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13752))->
						BgL_outz00) = ((obj_t) BgL_vz00_8609), BUNSPEC);
			}
		}

	}



/* &lambda2387 */
	obj_t BGl_z62lambda2387z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8610,
		obj_t BgL_oz00_8611)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13760;

				{
					obj_t BgL_auxz00_13761;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13762;

						BgL_tmpz00_13762 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8611));
						BgL_auxz00_13761 = BGL_OBJECT_WIDENING(BgL_tmpz00_13762);
					}
					BgL_auxz00_13760 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13761);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13760))->BgL_outz00);
			}
		}

	}



/* &<@anonymous:2382> */
	obj_t BGl_z62zc3z04anonymousza32382ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8612)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			return BUNSPEC;
		}

	}



/* &lambda2381 */
	obj_t BGl_z62lambda2381z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8613,
		obj_t BgL_oz00_8614, obj_t BgL_vz00_8615)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13768;

				{
					obj_t BgL_auxz00_13769;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13770;

						BgL_tmpz00_13770 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8614));
						BgL_auxz00_13769 = BGL_OBJECT_WIDENING(BgL_tmpz00_13770);
					}
					BgL_auxz00_13768 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13769);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13768))->
						BgL_defz00) = ((obj_t) BgL_vz00_8615), BUNSPEC);
			}
		}

	}



/* &lambda2380 */
	obj_t BGl_z62lambda2380z62zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8616,
		obj_t BgL_oz00_8617)
	{
		{	/* SawBbv/bbv-types.scm 38 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_13776;

				{
					obj_t BgL_auxz00_13777;

					{	/* SawBbv/bbv-types.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_13778;

						BgL_tmpz00_13778 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8617));
						BgL_auxz00_13777 = BGL_OBJECT_WIDENING(BgL_tmpz00_13778);
					}
					BgL_auxz00_13776 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_13777);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_13776))->BgL_defz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_proc3792z00zzsaw_bbvzd2typeszd2, BFALSE,
				BGl_string3793z00zzsaw_bbvzd2typeszd2);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_proc3794z00zzsaw_bbvzd2typeszd2, BFALSE,
				BGl_string3795z00zzsaw_bbvzd2typeszd2);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_blockzd2predszd2updatez12zd2envzc0zzsaw_bbvzd2typeszd2,
				BGl_proc3796z00zzsaw_bbvzd2typeszd2, BGl_blockz00zzsaw_defsz00,
				BGl_string3797z00zzsaw_bbvzd2typeszd2);
		}

	}



/* &block-preds-update!1936 */
	obj_t BGl_z62blockzd2predszd2updatez121936z70zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8621, obj_t BgL_bz00_8622, obj_t BgL_valz00_8623)
	{
		{	/* SawBbv/bbv-types.scm 1338 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_8622)))->BgL_predsz00) = ((obj_t)
					((obj_t) BgL_valz00_8623)), BUNSPEC);
		}

	}



/* &bbv-equal?1884 */
	obj_t BGl_z62bbvzd2equalzf31884z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8624,
		obj_t BgL_xz00_8625, obj_t BgL_yz00_8626)
	{
		{	/* SawBbv/bbv-types.scm 1138 */
			return BBOOL((BgL_xz00_8625 == BgL_yz00_8626));
		}

	}



/* &bbv-hash1831 */
	obj_t BGl_z62bbvzd2hash1831zb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8627,
		obj_t BgL_oz00_8628)
	{
		{	/* SawBbv/bbv-types.scm 935 */
			return BINT(2345L);
		}

	}



/* bbv-hash */
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(obj_t
		BgL_oz00_188)
	{
		{	/* SawBbv/bbv-types.scm 935 */
			if (BGL_OBJECTP(BgL_oz00_188))
				{	/* SawBbv/bbv-types.scm 935 */
					obj_t BgL_method1833z00_3485;

					{	/* SawBbv/bbv-types.scm 935 */
						obj_t BgL_res3632z00_6599;

						{	/* SawBbv/bbv-types.scm 935 */
							long BgL_objzd2classzd2numz00_6570;

							BgL_objzd2classzd2numz00_6570 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_188));
							{	/* SawBbv/bbv-types.scm 935 */
								obj_t BgL_arg1811z00_6571;

								BgL_arg1811z00_6571 =
									PROCEDURE_REF(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
									(int) (1L));
								{	/* SawBbv/bbv-types.scm 935 */
									int BgL_offsetz00_6574;

									BgL_offsetz00_6574 = (int) (BgL_objzd2classzd2numz00_6570);
									{	/* SawBbv/bbv-types.scm 935 */
										long BgL_offsetz00_6575;

										BgL_offsetz00_6575 =
											((long) (BgL_offsetz00_6574) - OBJECT_TYPE);
										{	/* SawBbv/bbv-types.scm 935 */
											long BgL_modz00_6576;

											BgL_modz00_6576 =
												(BgL_offsetz00_6575 >> (int) ((long) ((int) (4L))));
											{	/* SawBbv/bbv-types.scm 935 */
												long BgL_restz00_6578;

												BgL_restz00_6578 =
													(BgL_offsetz00_6575 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* SawBbv/bbv-types.scm 935 */

													{	/* SawBbv/bbv-types.scm 935 */
														obj_t BgL_bucketz00_6580;

														BgL_bucketz00_6580 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_6571), BgL_modz00_6576);
														BgL_res3632z00_6599 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_6580), BgL_restz00_6578);
						}}}}}}}}
						BgL_method1833z00_3485 = BgL_res3632z00_6599;
					}
					return BGL_PROCEDURE_CALL1(BgL_method1833z00_3485, BgL_oz00_188);
				}
			else
				{	/* SawBbv/bbv-types.scm 935 */
					obj_t BgL_fun2699z00_3486;

					BgL_fun2699z00_3486 =
						PROCEDURE_REF(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
						(int) (0L));
					return BGL_PROCEDURE_CALL1(BgL_fun2699z00_3486, BgL_oz00_188);
				}
		}

	}



/* &bbv-hash */
	obj_t BGl_z62bbvzd2hashzb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8629,
		obj_t BgL_oz00_8630)
	{
		{	/* SawBbv/bbv-types.scm 935 */
			return BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(BgL_oz00_8630);
		}

	}



/* bbv-equal? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(obj_t
		BgL_xz00_214, obj_t BgL_yz00_215)
	{
		{	/* SawBbv/bbv-types.scm 1138 */
			if (BGL_OBJECTP(BgL_xz00_214))
				{	/* SawBbv/bbv-types.scm 1138 */
					obj_t BgL_method1885z00_3488;

					{	/* SawBbv/bbv-types.scm 1138 */
						obj_t BgL_res3638z00_6631;

						{	/* SawBbv/bbv-types.scm 1138 */
							long BgL_objzd2classzd2numz00_6602;

							BgL_objzd2classzd2numz00_6602 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_xz00_214));
							{	/* SawBbv/bbv-types.scm 1138 */
								obj_t BgL_arg1811z00_6603;

								BgL_arg1811z00_6603 =
									PROCEDURE_REF(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
									(int) (1L));
								{	/* SawBbv/bbv-types.scm 1138 */
									int BgL_offsetz00_6606;

									BgL_offsetz00_6606 = (int) (BgL_objzd2classzd2numz00_6602);
									{	/* SawBbv/bbv-types.scm 1138 */
										long BgL_offsetz00_6607;

										BgL_offsetz00_6607 =
											((long) (BgL_offsetz00_6606) - OBJECT_TYPE);
										{	/* SawBbv/bbv-types.scm 1138 */
											long BgL_modz00_6608;

											BgL_modz00_6608 =
												(BgL_offsetz00_6607 >> (int) ((long) ((int) (4L))));
											{	/* SawBbv/bbv-types.scm 1138 */
												long BgL_restz00_6610;

												BgL_restz00_6610 =
													(BgL_offsetz00_6607 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* SawBbv/bbv-types.scm 1138 */

													{	/* SawBbv/bbv-types.scm 1138 */
														obj_t BgL_bucketz00_6612;

														BgL_bucketz00_6612 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_6603), BgL_modz00_6608);
														BgL_res3638z00_6631 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_6612), BgL_restz00_6610);
						}}}}}}}}
						BgL_method1885z00_3488 = BgL_res3638z00_6631;
					}
					return
						CBOOL(BGL_PROCEDURE_CALL2(BgL_method1885z00_3488, BgL_xz00_214,
							BgL_yz00_215));
				}
			else
				{	/* SawBbv/bbv-types.scm 1138 */
					obj_t BgL_fun2701z00_3489;

					BgL_fun2701z00_3489 =
						PROCEDURE_REF(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
						(int) (0L));
					return
						CBOOL(BGL_PROCEDURE_CALL2(BgL_fun2701z00_3489, BgL_xz00_214,
							BgL_yz00_215));
				}
		}

	}



/* &bbv-equal? */
	obj_t BGl_z62bbvzd2equalzf3z43zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8631,
		obj_t BgL_xz00_8632, obj_t BgL_yz00_8633)
	{
		{	/* SawBbv/bbv-types.scm 1138 */
			return
				BBOOL(BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(BgL_xz00_8632,
					BgL_yz00_8633));
		}

	}



/* block-preds-update! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt
		BgL_bz00_266, obj_t BgL_valz00_267)
	{
		{	/* SawBbv/bbv-types.scm 1338 */
			{	/* SawBbv/bbv-types.scm 1338 */
				obj_t BgL_method1937z00_3490;

				{	/* SawBbv/bbv-types.scm 1338 */
					obj_t BgL_res3644z00_6663;

					{	/* SawBbv/bbv-types.scm 1338 */
						long BgL_objzd2classzd2numz00_6634;

						BgL_objzd2classzd2numz00_6634 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_266));
						{	/* SawBbv/bbv-types.scm 1338 */
							obj_t BgL_arg1811z00_6635;

							BgL_arg1811z00_6635 =
								PROCEDURE_REF
								(BGl_blockzd2predszd2updatez12zd2envzc0zzsaw_bbvzd2typeszd2,
								(int) (1L));
							{	/* SawBbv/bbv-types.scm 1338 */
								int BgL_offsetz00_6638;

								BgL_offsetz00_6638 = (int) (BgL_objzd2classzd2numz00_6634);
								{	/* SawBbv/bbv-types.scm 1338 */
									long BgL_offsetz00_6639;

									BgL_offsetz00_6639 =
										((long) (BgL_offsetz00_6638) - OBJECT_TYPE);
									{	/* SawBbv/bbv-types.scm 1338 */
										long BgL_modz00_6640;

										BgL_modz00_6640 =
											(BgL_offsetz00_6639 >> (int) ((long) ((int) (4L))));
										{	/* SawBbv/bbv-types.scm 1338 */
											long BgL_restz00_6642;

											BgL_restz00_6642 =
												(BgL_offsetz00_6639 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawBbv/bbv-types.scm 1338 */

												{	/* SawBbv/bbv-types.scm 1338 */
													obj_t BgL_bucketz00_6644;

													BgL_bucketz00_6644 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6635), BgL_modz00_6640);
													BgL_res3644z00_6663 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6644), BgL_restz00_6642);
					}}}}}}}}
					BgL_method1937z00_3490 = BgL_res3644z00_6663;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1937z00_3490,
					((obj_t) BgL_bz00_266), BgL_valz00_267);
			}
		}

	}



/* &block-preds-update! */
	obj_t BGl_z62blockzd2predszd2updatez12z70zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8634, obj_t BgL_bz00_8635, obj_t BgL_valz00_8636)
	{
		{	/* SawBbv/bbv-types.scm 1338 */
			return
				BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(
				((BgL_blockz00_bglt) BgL_bz00_8635), BgL_valz00_8636);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2printzd2envz00zz__objectz00,
				BGl_blockVz00zzsaw_bbvzd2typeszd2, BGl_proc3798z00zzsaw_bbvzd2typeszd2,
				BGl_string3799z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2printzd2envz00zz__objectz00,
				BGl_blockSz00zzsaw_bbvzd2typeszd2, BGl_proc3800z00zzsaw_bbvzd2typeszd2,
				BGl_string3799z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2,
				BGl_proc3801z00zzsaw_bbvzd2typeszd2,
				BGl_string3802z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2,
				BGl_proc3803z00zzsaw_bbvzd2typeszd2,
				BGl_string3802z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_blockz00zzsaw_defsz00,
				BGl_proc3804z00zzsaw_bbvzd2typeszd2,
				BGl_string3802z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_blockSz00zzsaw_bbvzd2typeszd2,
				BGl_proc3805z00zzsaw_bbvzd2typeszd2,
				BGl_string3802z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2,
				BGl_proc3806z00zzsaw_bbvzd2typeszd2,
				BGl_string3802z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00,
				BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2,
				BGl_proc3807z00zzsaw_bbvzd2typeszd2,
				BGl_string3808z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00, BGl_blockVz00zzsaw_bbvzd2typeszd2,
				BGl_proc3809z00zzsaw_bbvzd2typeszd2,
				BGl_string3808z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00, BGl_blockSz00zzsaw_bbvzd2typeszd2,
				BGl_proc3810z00zzsaw_bbvzd2typeszd2,
				BGl_string3808z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2, BGl_typez00zztype_typez00,
				BGl_proc3811z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2, BGl_atomz00zzast_nodez00,
				BGl_proc3813z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_blockSz00zzsaw_bbvzd2typeszd2, BGl_proc3814z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2,
				BGl_proc3815z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_regz00zzsaw_defsz00, BGl_proc3816z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_funz00zzsaw_defsz00, BGl_proc3817z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_returnz00zzsaw_defsz00, BGl_proc3818z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_loadiz00zzsaw_defsz00, BGl_proc3819z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_loadgz00zzsaw_defsz00, BGl_proc3820z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_loadfunz00zzsaw_defsz00, BGl_proc3821z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_globalrefz00zzsaw_defsz00, BGl_proc3822z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc3823z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_vallocz00zzsaw_defsz00, BGl_proc3824z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_vrefz00zzsaw_defsz00, BGl_proc3825z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_vlengthz00zzsaw_defsz00, BGl_proc3826z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_instanceofz00zzsaw_defsz00, BGl_proc3827z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_storegz00zzsaw_defsz00, BGl_proc3828z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_setfieldz00zzsaw_defsz00, BGl_proc3829z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_vsetz00zzsaw_defsz00, BGl_proc3830z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_newz00zzsaw_defsz00, BGl_proc3831z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_callz00zzsaw_defsz00, BGl_proc3832z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_lightfuncallz00zzsaw_defsz00,
				BGl_proc3833z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_pragmaz00zzsaw_defsz00, BGl_proc3834z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_castz00zzsaw_defsz00, BGl_proc3835z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
				BGl_rtl_cast_nullz00zzsaw_defsz00, BGl_proc3836z00zzsaw_bbvzd2typeszd2,
				BGl_string3812z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_atomz00zzast_nodez00, BGl_proc3837z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_blockSz00zzsaw_bbvzd2typeszd2, BGl_proc3839z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2,
				BGl_proc3840z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_regz00zzsaw_defsz00, BGl_proc3841z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_funz00zzsaw_defsz00, BGl_proc3842z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_selectz00zzsaw_defsz00, BGl_proc3843z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_switchz00zzsaw_defsz00, BGl_proc3844z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_loadiz00zzsaw_defsz00, BGl_proc3845z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_loadgz00zzsaw_defsz00, BGl_proc3846z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_loadfunz00zzsaw_defsz00, BGl_proc3847z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_globalrefz00zzsaw_defsz00, BGl_proc3848z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc3849z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_vallocz00zzsaw_defsz00, BGl_proc3850z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_vrefz00zzsaw_defsz00, BGl_proc3851z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_vlengthz00zzsaw_defsz00, BGl_proc3852z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_instanceofz00zzsaw_defsz00, BGl_proc3853z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_storegz00zzsaw_defsz00, BGl_proc3854z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_setfieldz00zzsaw_defsz00, BGl_proc3855z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_vsetz00zzsaw_defsz00, BGl_proc3856z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_newz00zzsaw_defsz00, BGl_proc3857z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_callz00zzsaw_defsz00, BGl_proc3858z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_lightfuncallz00zzsaw_defsz00,
				BGl_proc3859z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_pragmaz00zzsaw_defsz00, BGl_proc3860z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_castz00zzsaw_defsz00, BGl_proc3861z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
				BGl_rtl_cast_nullz00zzsaw_defsz00, BGl_proc3862z00zzsaw_bbvzd2typeszd2,
				BGl_string3838z00zzsaw_bbvzd2typeszd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_blockzd2predszd2updatez12zd2envzc0zzsaw_bbvzd2typeszd2,
				BGl_blockSz00zzsaw_bbvzd2typeszd2, BGl_proc3863z00zzsaw_bbvzd2typeszd2,
				BGl_string3864z00zzsaw_bbvzd2typeszd2);
		}

	}



/* &block-preds-update!-1939 */
	obj_t BGl_z62blockzd2predszd2updatez12zd21939za2zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8702, obj_t BgL_bz00_8703, obj_t BgL_npredsz00_8704)
	{
		{	/* SawBbv/bbv-types.scm 1347 */
			((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_8703))))->BgL_predsz00) = ((obj_t)
					((obj_t) BgL_npredsz00_8704)), BUNSPEC);
			{
				long BgL_auxz00_13979;
				BgL_blocksz00_bglt BgL_auxz00_13972;

				{	/* SawBbv/bbv-types.scm 1356 */
					long BgL_arg3416z00_9214;
					long BgL_arg3422z00_9215;

					BgL_arg3416z00_9214 = bgl_list_length(((obj_t) BgL_npredsz00_8704));
					{	/* SawBbv/bbv-types.scm 1356 */
						bool_t BgL_test4420z00_13982;

						{	/* SawBbv/bbv-types.scm 1356 */
							obj_t BgL_arg3429z00_9216;

							{
								BgL_blocksz00_bglt BgL_auxz00_13983;

								{
									obj_t BgL_auxz00_13984;

									{	/* SawBbv/bbv-types.scm 1356 */
										BgL_objectz00_bglt BgL_tmpz00_13985;

										BgL_tmpz00_13985 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt) BgL_bz00_8703));
										BgL_auxz00_13984 = BGL_OBJECT_WIDENING(BgL_tmpz00_13985);
									}
									BgL_auxz00_13983 = ((BgL_blocksz00_bglt) BgL_auxz00_13984);
								}
								BgL_arg3429z00_9216 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13983))->
									BgL_creatorz00);
							}
							BgL_test4420z00_13982 =
								(BgL_arg3429z00_9216 == CNST_TABLE_REF(47));
						}
						if (BgL_test4420z00_13982)
							{	/* SawBbv/bbv-types.scm 1356 */
								BgL_arg3422z00_9215 = 1L;
							}
						else
							{	/* SawBbv/bbv-types.scm 1356 */
								BgL_arg3422z00_9215 = 0L;
							}
					}
					BgL_auxz00_13979 = (BgL_arg3416z00_9214 + BgL_arg3422z00_9215);
				}
				{
					obj_t BgL_auxz00_13973;

					{	/* SawBbv/bbv-types.scm 1356 */
						BgL_objectz00_bglt BgL_tmpz00_13974;

						BgL_tmpz00_13974 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_8703));
						BgL_auxz00_13973 = BGL_OBJECT_WIDENING(BgL_tmpz00_13974);
					}
					BgL_auxz00_13972 = ((BgL_blocksz00_bglt) BgL_auxz00_13973);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_13972))->BgL_gccntz00) =
					((long) BgL_auxz00_13979), BUNSPEC);
		}}

	}



/* &bbv-equal?-rtl_cast_1935 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_cast_1935z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8705, obj_t BgL_xz00_8706, obj_t BgL_yz00_8707)
	{
		{	/* SawBbv/bbv-types.scm 1329 */
			{	/* SawBbv/bbv-types.scm 1330 */
				bool_t BgL_tmpz00_13995;

				{	/* SawBbv/bbv-types.scm 1330 */
					bool_t BgL_test4421z00_13996;

					{	/* SawBbv/bbv-types.scm 1330 */
						obj_t BgL_classz00_9218;

						BgL_classz00_9218 = BGl_rtl_cast_nullz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8707))
							{	/* SawBbv/bbv-types.scm 1330 */
								BgL_objectz00_bglt BgL_arg1807z00_9219;

								BgL_arg1807z00_9219 = (BgL_objectz00_bglt) (BgL_yz00_8707);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1330 */
										long BgL_idxz00_9220;

										BgL_idxz00_9220 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9219);
										BgL_test4421z00_13996 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9220 + 2L)) == BgL_classz00_9218);
									}
								else
									{	/* SawBbv/bbv-types.scm 1330 */
										bool_t BgL_res3669z00_9223;

										{	/* SawBbv/bbv-types.scm 1330 */
											obj_t BgL_oclassz00_9224;

											{	/* SawBbv/bbv-types.scm 1330 */
												obj_t BgL_arg1815z00_9225;
												long BgL_arg1816z00_9226;

												BgL_arg1815z00_9225 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1330 */
													long BgL_arg1817z00_9227;

													BgL_arg1817z00_9227 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9219);
													BgL_arg1816z00_9226 =
														(BgL_arg1817z00_9227 - OBJECT_TYPE);
												}
												BgL_oclassz00_9224 =
													VECTOR_REF(BgL_arg1815z00_9225, BgL_arg1816z00_9226);
											}
											{	/* SawBbv/bbv-types.scm 1330 */
												bool_t BgL__ortest_1115z00_9228;

												BgL__ortest_1115z00_9228 =
													(BgL_classz00_9218 == BgL_oclassz00_9224);
												if (BgL__ortest_1115z00_9228)
													{	/* SawBbv/bbv-types.scm 1330 */
														BgL_res3669z00_9223 = BgL__ortest_1115z00_9228;
													}
												else
													{	/* SawBbv/bbv-types.scm 1330 */
														long BgL_odepthz00_9229;

														{	/* SawBbv/bbv-types.scm 1330 */
															obj_t BgL_arg1804z00_9230;

															BgL_arg1804z00_9230 = (BgL_oclassz00_9224);
															BgL_odepthz00_9229 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9230);
														}
														if ((2L < BgL_odepthz00_9229))
															{	/* SawBbv/bbv-types.scm 1330 */
																obj_t BgL_arg1802z00_9231;

																{	/* SawBbv/bbv-types.scm 1330 */
																	obj_t BgL_arg1803z00_9232;

																	BgL_arg1803z00_9232 = (BgL_oclassz00_9224);
																	BgL_arg1802z00_9231 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9232,
																		2L);
																}
																BgL_res3669z00_9223 =
																	(BgL_arg1802z00_9231 == BgL_classz00_9218);
															}
														else
															{	/* SawBbv/bbv-types.scm 1330 */
																BgL_res3669z00_9223 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4421z00_13996 = BgL_res3669z00_9223;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1330 */
								BgL_test4421z00_13996 = ((bool_t) 0);
							}
					}
					if (BgL_test4421z00_13996)
						{	/* SawBbv/bbv-types.scm 1331 */
							BgL_typez00_bglt BgL_arg3410z00_9233;
							BgL_typez00_bglt BgL_arg3413z00_9234;

							BgL_arg3410z00_9233 =
								(((BgL_rtl_cast_nullz00_bglt) COBJECT(
										((BgL_rtl_cast_nullz00_bglt) BgL_xz00_8706)))->BgL_typez00);
							BgL_arg3413z00_9234 =
								(((BgL_rtl_cast_nullz00_bglt) COBJECT(
										((BgL_rtl_cast_nullz00_bglt) BgL_yz00_8707)))->BgL_typez00);
							BgL_tmpz00_13995 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3410z00_9233), ((obj_t) BgL_arg3413z00_9234));
						}
					else
						{	/* SawBbv/bbv-types.scm 1330 */
							BgL_tmpz00_13995 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_13995);
			}
		}

	}



/* &bbv-equal?-rtl_cast1933 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_cast1933z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8708, obj_t BgL_xz00_8709, obj_t BgL_yz00_8710)
	{
		{	/* SawBbv/bbv-types.scm 1321 */
			{	/* SawBbv/bbv-types.scm 1322 */
				bool_t BgL_tmpz00_14027;

				{	/* SawBbv/bbv-types.scm 1322 */
					bool_t BgL_test4426z00_14028;

					{	/* SawBbv/bbv-types.scm 1322 */
						obj_t BgL_classz00_9236;

						BgL_classz00_9236 = BGl_rtl_castz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8710))
							{	/* SawBbv/bbv-types.scm 1322 */
								BgL_objectz00_bglt BgL_arg1807z00_9237;

								BgL_arg1807z00_9237 = (BgL_objectz00_bglt) (BgL_yz00_8710);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1322 */
										long BgL_idxz00_9238;

										BgL_idxz00_9238 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9237);
										BgL_test4426z00_14028 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9238 + 2L)) == BgL_classz00_9236);
									}
								else
									{	/* SawBbv/bbv-types.scm 1322 */
										bool_t BgL_res3668z00_9241;

										{	/* SawBbv/bbv-types.scm 1322 */
											obj_t BgL_oclassz00_9242;

											{	/* SawBbv/bbv-types.scm 1322 */
												obj_t BgL_arg1815z00_9243;
												long BgL_arg1816z00_9244;

												BgL_arg1815z00_9243 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1322 */
													long BgL_arg1817z00_9245;

													BgL_arg1817z00_9245 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9237);
													BgL_arg1816z00_9244 =
														(BgL_arg1817z00_9245 - OBJECT_TYPE);
												}
												BgL_oclassz00_9242 =
													VECTOR_REF(BgL_arg1815z00_9243, BgL_arg1816z00_9244);
											}
											{	/* SawBbv/bbv-types.scm 1322 */
												bool_t BgL__ortest_1115z00_9246;

												BgL__ortest_1115z00_9246 =
													(BgL_classz00_9236 == BgL_oclassz00_9242);
												if (BgL__ortest_1115z00_9246)
													{	/* SawBbv/bbv-types.scm 1322 */
														BgL_res3668z00_9241 = BgL__ortest_1115z00_9246;
													}
												else
													{	/* SawBbv/bbv-types.scm 1322 */
														long BgL_odepthz00_9247;

														{	/* SawBbv/bbv-types.scm 1322 */
															obj_t BgL_arg1804z00_9248;

															BgL_arg1804z00_9248 = (BgL_oclassz00_9242);
															BgL_odepthz00_9247 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9248);
														}
														if ((2L < BgL_odepthz00_9247))
															{	/* SawBbv/bbv-types.scm 1322 */
																obj_t BgL_arg1802z00_9249;

																{	/* SawBbv/bbv-types.scm 1322 */
																	obj_t BgL_arg1803z00_9250;

																	BgL_arg1803z00_9250 = (BgL_oclassz00_9242);
																	BgL_arg1802z00_9249 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9250,
																		2L);
																}
																BgL_res3668z00_9241 =
																	(BgL_arg1802z00_9249 == BgL_classz00_9236);
															}
														else
															{	/* SawBbv/bbv-types.scm 1322 */
																BgL_res3668z00_9241 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4426z00_14028 = BgL_res3668z00_9241;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1322 */
								BgL_test4426z00_14028 = ((bool_t) 0);
							}
					}
					if (BgL_test4426z00_14028)
						{	/* SawBbv/bbv-types.scm 1323 */
							bool_t BgL_test4431z00_14051;

							{	/* SawBbv/bbv-types.scm 1323 */
								BgL_typez00_bglt BgL_arg3405z00_9251;
								BgL_typez00_bglt BgL_arg3406z00_9252;

								BgL_arg3405z00_9251 =
									(((BgL_rtl_castz00_bglt) COBJECT(
											((BgL_rtl_castz00_bglt) BgL_xz00_8709)))->BgL_totypez00);
								BgL_arg3406z00_9252 =
									(((BgL_rtl_castz00_bglt) COBJECT(
											((BgL_rtl_castz00_bglt) BgL_yz00_8710)))->BgL_totypez00);
								BgL_test4431z00_14051 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3405z00_9251), ((obj_t) BgL_arg3406z00_9252));
							}
							if (BgL_test4431z00_14051)
								{	/* SawBbv/bbv-types.scm 1324 */
									BgL_typez00_bglt BgL_arg3403z00_9253;
									BgL_typez00_bglt BgL_arg3404z00_9254;

									BgL_arg3403z00_9253 =
										(((BgL_rtl_castz00_bglt) COBJECT(
												((BgL_rtl_castz00_bglt) BgL_xz00_8709)))->
										BgL_fromtypez00);
									BgL_arg3404z00_9254 =
										(((BgL_rtl_castz00_bglt) COBJECT(((BgL_rtl_castz00_bglt)
													BgL_yz00_8710)))->BgL_fromtypez00);
									BgL_tmpz00_14027 =
										BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
											BgL_arg3403z00_9253), ((obj_t) BgL_arg3404z00_9254));
								}
							else
								{	/* SawBbv/bbv-types.scm 1323 */
									BgL_tmpz00_14027 = ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1322 */
							BgL_tmpz00_14027 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14027);
			}
		}

	}



/* &bbv-equal?-rtl_pragm1931 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_pragm1931z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8711, obj_t BgL_xz00_8712, obj_t BgL_yz00_8713)
	{
		{	/* SawBbv/bbv-types.scm 1314 */
			{	/* SawBbv/bbv-types.scm 1315 */
				bool_t BgL_tmpz00_14067;

				{	/* SawBbv/bbv-types.scm 1315 */
					bool_t BgL_test4432z00_14068;

					{	/* SawBbv/bbv-types.scm 1315 */
						obj_t BgL_classz00_9256;

						BgL_classz00_9256 = BGl_rtl_pragmaz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8713))
							{	/* SawBbv/bbv-types.scm 1315 */
								BgL_objectz00_bglt BgL_arg1807z00_9257;

								BgL_arg1807z00_9257 = (BgL_objectz00_bglt) (BgL_yz00_8713);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1315 */
										long BgL_idxz00_9258;

										BgL_idxz00_9258 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9257);
										BgL_test4432z00_14068 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9258 + 2L)) == BgL_classz00_9256);
									}
								else
									{	/* SawBbv/bbv-types.scm 1315 */
										bool_t BgL_res3667z00_9261;

										{	/* SawBbv/bbv-types.scm 1315 */
											obj_t BgL_oclassz00_9262;

											{	/* SawBbv/bbv-types.scm 1315 */
												obj_t BgL_arg1815z00_9263;
												long BgL_arg1816z00_9264;

												BgL_arg1815z00_9263 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1315 */
													long BgL_arg1817z00_9265;

													BgL_arg1817z00_9265 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9257);
													BgL_arg1816z00_9264 =
														(BgL_arg1817z00_9265 - OBJECT_TYPE);
												}
												BgL_oclassz00_9262 =
													VECTOR_REF(BgL_arg1815z00_9263, BgL_arg1816z00_9264);
											}
											{	/* SawBbv/bbv-types.scm 1315 */
												bool_t BgL__ortest_1115z00_9266;

												BgL__ortest_1115z00_9266 =
													(BgL_classz00_9256 == BgL_oclassz00_9262);
												if (BgL__ortest_1115z00_9266)
													{	/* SawBbv/bbv-types.scm 1315 */
														BgL_res3667z00_9261 = BgL__ortest_1115z00_9266;
													}
												else
													{	/* SawBbv/bbv-types.scm 1315 */
														long BgL_odepthz00_9267;

														{	/* SawBbv/bbv-types.scm 1315 */
															obj_t BgL_arg1804z00_9268;

															BgL_arg1804z00_9268 = (BgL_oclassz00_9262);
															BgL_odepthz00_9267 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9268);
														}
														if ((2L < BgL_odepthz00_9267))
															{	/* SawBbv/bbv-types.scm 1315 */
																obj_t BgL_arg1802z00_9269;

																{	/* SawBbv/bbv-types.scm 1315 */
																	obj_t BgL_arg1803z00_9270;

																	BgL_arg1803z00_9270 = (BgL_oclassz00_9262);
																	BgL_arg1802z00_9269 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9270,
																		2L);
																}
																BgL_res3667z00_9261 =
																	(BgL_arg1802z00_9269 == BgL_classz00_9256);
															}
														else
															{	/* SawBbv/bbv-types.scm 1315 */
																BgL_res3667z00_9261 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4432z00_14068 = BgL_res3667z00_9261;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1315 */
								BgL_test4432z00_14068 = ((bool_t) 0);
							}
					}
					if (BgL_test4432z00_14068)
						{	/* SawBbv/bbv-types.scm 1316 */
							obj_t BgL_arg3400z00_9271;
							obj_t BgL_arg3401z00_9272;

							BgL_arg3400z00_9271 =
								(((BgL_rtl_pragmaz00_bglt) COBJECT(
										((BgL_rtl_pragmaz00_bglt) BgL_xz00_8712)))->BgL_formatz00);
							BgL_arg3401z00_9272 =
								(((BgL_rtl_pragmaz00_bglt) COBJECT(
										((BgL_rtl_pragmaz00_bglt) BgL_yz00_8713)))->BgL_formatz00);
							{	/* SawBbv/bbv-types.scm 1316 */
								long BgL_l1z00_9273;

								BgL_l1z00_9273 = STRING_LENGTH(BgL_arg3400z00_9271);
								if ((BgL_l1z00_9273 == STRING_LENGTH(BgL_arg3401z00_9272)))
									{	/* SawBbv/bbv-types.scm 1316 */
										int BgL_arg1282z00_9274;

										{	/* SawBbv/bbv-types.scm 1316 */
											char *BgL_auxz00_14101;
											char *BgL_tmpz00_14099;

											BgL_auxz00_14101 = BSTRING_TO_STRING(BgL_arg3401z00_9272);
											BgL_tmpz00_14099 = BSTRING_TO_STRING(BgL_arg3400z00_9271);
											BgL_arg1282z00_9274 =
												memcmp(BgL_tmpz00_14099, BgL_auxz00_14101,
												BgL_l1z00_9273);
										}
										BgL_tmpz00_14067 = ((long) (BgL_arg1282z00_9274) == 0L);
									}
								else
									{	/* SawBbv/bbv-types.scm 1316 */
										BgL_tmpz00_14067 = ((bool_t) 0);
									}
							}
						}
					else
						{	/* SawBbv/bbv-types.scm 1315 */
							BgL_tmpz00_14067 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14067);
			}
		}

	}



/* &bbv-equal?-rtl_light1929 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_light1929z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8714, obj_t BgL_xz00_8715, obj_t BgL_yz00_8716)
	{
		{	/* SawBbv/bbv-types.scm 1308 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &bbv-equal?-rtl_call1927 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_call1927z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8717, obj_t BgL_xz00_8718, obj_t BgL_yz00_8719)
	{
		{	/* SawBbv/bbv-types.scm 1301 */
			{	/* SawBbv/bbv-types.scm 1302 */
				bool_t BgL_tmpz00_14108;

				{	/* SawBbv/bbv-types.scm 1302 */
					bool_t BgL_test4438z00_14109;

					{	/* SawBbv/bbv-types.scm 1302 */
						obj_t BgL_classz00_9277;

						BgL_classz00_9277 = BGl_rtl_callz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8719))
							{	/* SawBbv/bbv-types.scm 1302 */
								BgL_objectz00_bglt BgL_arg1807z00_9278;

								BgL_arg1807z00_9278 = (BgL_objectz00_bglt) (BgL_yz00_8719);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1302 */
										long BgL_idxz00_9279;

										BgL_idxz00_9279 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9278);
										BgL_test4438z00_14109 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9279 + 2L)) == BgL_classz00_9277);
									}
								else
									{	/* SawBbv/bbv-types.scm 1302 */
										bool_t BgL_res3666z00_9282;

										{	/* SawBbv/bbv-types.scm 1302 */
											obj_t BgL_oclassz00_9283;

											{	/* SawBbv/bbv-types.scm 1302 */
												obj_t BgL_arg1815z00_9284;
												long BgL_arg1816z00_9285;

												BgL_arg1815z00_9284 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1302 */
													long BgL_arg1817z00_9286;

													BgL_arg1817z00_9286 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9278);
													BgL_arg1816z00_9285 =
														(BgL_arg1817z00_9286 - OBJECT_TYPE);
												}
												BgL_oclassz00_9283 =
													VECTOR_REF(BgL_arg1815z00_9284, BgL_arg1816z00_9285);
											}
											{	/* SawBbv/bbv-types.scm 1302 */
												bool_t BgL__ortest_1115z00_9287;

												BgL__ortest_1115z00_9287 =
													(BgL_classz00_9277 == BgL_oclassz00_9283);
												if (BgL__ortest_1115z00_9287)
													{	/* SawBbv/bbv-types.scm 1302 */
														BgL_res3666z00_9282 = BgL__ortest_1115z00_9287;
													}
												else
													{	/* SawBbv/bbv-types.scm 1302 */
														long BgL_odepthz00_9288;

														{	/* SawBbv/bbv-types.scm 1302 */
															obj_t BgL_arg1804z00_9289;

															BgL_arg1804z00_9289 = (BgL_oclassz00_9283);
															BgL_odepthz00_9288 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9289);
														}
														if ((2L < BgL_odepthz00_9288))
															{	/* SawBbv/bbv-types.scm 1302 */
																obj_t BgL_arg1802z00_9290;

																{	/* SawBbv/bbv-types.scm 1302 */
																	obj_t BgL_arg1803z00_9291;

																	BgL_arg1803z00_9291 = (BgL_oclassz00_9283);
																	BgL_arg1802z00_9290 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9291,
																		2L);
																}
																BgL_res3666z00_9282 =
																	(BgL_arg1802z00_9290 == BgL_classz00_9277);
															}
														else
															{	/* SawBbv/bbv-types.scm 1302 */
																BgL_res3666z00_9282 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4438z00_14109 = BgL_res3666z00_9282;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1302 */
								BgL_test4438z00_14109 = ((bool_t) 0);
							}
					}
					if (BgL_test4438z00_14109)
						{	/* SawBbv/bbv-types.scm 1303 */
							BgL_globalz00_bglt BgL_arg3397z00_9292;
							BgL_globalz00_bglt BgL_arg3398z00_9293;

							BgL_arg3397z00_9292 =
								(((BgL_rtl_callz00_bglt) COBJECT(
										((BgL_rtl_callz00_bglt) BgL_xz00_8718)))->BgL_varz00);
							BgL_arg3398z00_9293 =
								(((BgL_rtl_callz00_bglt) COBJECT(
										((BgL_rtl_callz00_bglt) BgL_yz00_8719)))->BgL_varz00);
							BgL_tmpz00_14108 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3397z00_9292), ((obj_t) BgL_arg3398z00_9293));
						}
					else
						{	/* SawBbv/bbv-types.scm 1302 */
							BgL_tmpz00_14108 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14108);
			}
		}

	}



/* &bbv-equal?-rtl_new1925 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_new1925z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8720, obj_t BgL_xz00_8721, obj_t BgL_yz00_8722)
	{
		{	/* SawBbv/bbv-types.scm 1293 */
			{	/* SawBbv/bbv-types.scm 1294 */
				bool_t BgL_test4443z00_14140;

				{	/* SawBbv/bbv-types.scm 1294 */
					obj_t BgL_objz00_9295;

					BgL_objz00_9295 = BGl_rtl_newz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_objz00_9295))
						{	/* SawBbv/bbv-types.scm 1294 */
							BgL_objectz00_bglt BgL_arg1809z00_9296;
							long BgL_arg1810z00_9297;

							BgL_arg1809z00_9296 = (BgL_objectz00_bglt) (BgL_objz00_9295);
							{	/* SawBbv/bbv-types.scm 1294 */
								obj_t BgL_tmpz00_14144;

								BgL_tmpz00_14144 = ((obj_t) BgL_yz00_8722);
								BgL_arg1810z00_9297 = BGL_CLASS_DEPTH(BgL_tmpz00_14144);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 1294 */
									long BgL_idxz00_9298;

									BgL_idxz00_9298 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_9296);
									{	/* SawBbv/bbv-types.scm 1294 */
										obj_t BgL_arg1800z00_9299;

										BgL_arg1800z00_9299 =
											VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9298 + BgL_arg1810z00_9297));
										BgL_test4443z00_14140 =
											(BgL_arg1800z00_9299 == ((obj_t) BgL_yz00_8722));
								}}
							else
								{	/* SawBbv/bbv-types.scm 1294 */
									bool_t BgL_res3665z00_9301;

									{	/* SawBbv/bbv-types.scm 1294 */
										obj_t BgL_oclassz00_9302;

										{	/* SawBbv/bbv-types.scm 1294 */
											obj_t BgL_arg1815z00_9303;
											long BgL_arg1816z00_9304;

											BgL_arg1815z00_9303 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 1294 */
												long BgL_arg1817z00_9305;

												BgL_arg1817z00_9305 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_9296);
												BgL_arg1816z00_9304 =
													(BgL_arg1817z00_9305 - OBJECT_TYPE);
											}
											BgL_oclassz00_9302 =
												VECTOR_REF(BgL_arg1815z00_9303, BgL_arg1816z00_9304);
										}
										{	/* SawBbv/bbv-types.scm 1294 */
											bool_t BgL__ortest_1115z00_9306;

											BgL__ortest_1115z00_9306 =
												(((obj_t) BgL_yz00_8722) == BgL_oclassz00_9302);
											if (BgL__ortest_1115z00_9306)
												{	/* SawBbv/bbv-types.scm 1294 */
													BgL_res3665z00_9301 = BgL__ortest_1115z00_9306;
												}
											else
												{	/* SawBbv/bbv-types.scm 1294 */
													long BgL_odepthz00_9307;

													{	/* SawBbv/bbv-types.scm 1294 */
														obj_t BgL_arg1804z00_9308;

														BgL_arg1804z00_9308 = (BgL_oclassz00_9302);
														BgL_odepthz00_9307 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9308);
													}
													if ((BgL_arg1810z00_9297 < BgL_odepthz00_9307))
														{	/* SawBbv/bbv-types.scm 1294 */
															obj_t BgL_arg1802z00_9309;

															{	/* SawBbv/bbv-types.scm 1294 */
																obj_t BgL_arg1803z00_9310;

																BgL_arg1803z00_9310 = (BgL_oclassz00_9302);
																BgL_arg1802z00_9309 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9310,
																	BgL_arg1810z00_9297);
															}
															BgL_res3665z00_9301 =
																(BgL_arg1802z00_9309 ==
																((obj_t) BgL_yz00_8722));
														}
													else
														{	/* SawBbv/bbv-types.scm 1294 */
															BgL_res3665z00_9301 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4443z00_14140 = BgL_res3665z00_9301;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1294 */
							BgL_test4443z00_14140 = ((bool_t) 0);
						}
				}
				if (BgL_test4443z00_14140)
					{	/* SawBbv/bbv-types.scm 1294 */
						if (
							(((obj_t)
									(((BgL_rtl_newz00_bglt) COBJECT(
												((BgL_rtl_newz00_bglt) BgL_xz00_8721)))->
										BgL_typez00)) ==
								((obj_t) (((BgL_rtl_newz00_bglt) COBJECT(((BgL_rtl_newz00_bglt)
													BgL_yz00_8722)))->BgL_typez00))))
							{	/* SawBbv/bbv-types.scm 1296 */
								obj_t BgL_arg3388z00_9311;
								BgL_typez00_bglt BgL_arg3391z00_9312;

								BgL_arg3388z00_9311 =
									(((BgL_rtl_newz00_bglt) COBJECT(
											((BgL_rtl_newz00_bglt) BgL_xz00_8721)))->BgL_constrz00);
								BgL_arg3391z00_9312 =
									(((BgL_rtl_newz00_bglt) COBJECT(
											((BgL_rtl_newz00_bglt) BgL_yz00_8722)))->BgL_typez00);
								{	/* SawBbv/bbv-types.scm 1296 */
									obj_t BgL_list3392z00_9313;

									{	/* SawBbv/bbv-types.scm 1296 */
										obj_t BgL_arg3393z00_9314;

										BgL_arg3393z00_9314 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg3391z00_9312), BNIL);
										BgL_list3392z00_9313 =
											MAKE_YOUNG_PAIR(BgL_arg3388z00_9311, BgL_arg3393z00_9314);
									}
									return
										BGl_everyz00zz__r4_pairs_and_lists_6_3z00
										(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
										BgL_list3392z00_9313);
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 1295 */
								return BFALSE;
							}
					}
				else
					{	/* SawBbv/bbv-types.scm 1294 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-equal?-rtl_vset1923 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_vset1923z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8723, obj_t BgL_xz00_8724, obj_t BgL_yz00_8725)
	{
		{	/* SawBbv/bbv-types.scm 1285 */
			{	/* SawBbv/bbv-types.scm 1286 */
				bool_t BgL_tmpz00_14185;

				{	/* SawBbv/bbv-types.scm 1286 */
					bool_t BgL_test4449z00_14186;

					{	/* SawBbv/bbv-types.scm 1286 */
						obj_t BgL_classz00_9316;

						BgL_classz00_9316 = BGl_rtl_vsetz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8725))
							{	/* SawBbv/bbv-types.scm 1286 */
								BgL_objectz00_bglt BgL_arg1807z00_9317;

								BgL_arg1807z00_9317 = (BgL_objectz00_bglt) (BgL_yz00_8725);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1286 */
										long BgL_idxz00_9318;

										BgL_idxz00_9318 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9317);
										BgL_test4449z00_14186 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9318 + 3L)) == BgL_classz00_9316);
									}
								else
									{	/* SawBbv/bbv-types.scm 1286 */
										bool_t BgL_res3664z00_9321;

										{	/* SawBbv/bbv-types.scm 1286 */
											obj_t BgL_oclassz00_9322;

											{	/* SawBbv/bbv-types.scm 1286 */
												obj_t BgL_arg1815z00_9323;
												long BgL_arg1816z00_9324;

												BgL_arg1815z00_9323 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1286 */
													long BgL_arg1817z00_9325;

													BgL_arg1817z00_9325 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9317);
													BgL_arg1816z00_9324 =
														(BgL_arg1817z00_9325 - OBJECT_TYPE);
												}
												BgL_oclassz00_9322 =
													VECTOR_REF(BgL_arg1815z00_9323, BgL_arg1816z00_9324);
											}
											{	/* SawBbv/bbv-types.scm 1286 */
												bool_t BgL__ortest_1115z00_9326;

												BgL__ortest_1115z00_9326 =
													(BgL_classz00_9316 == BgL_oclassz00_9322);
												if (BgL__ortest_1115z00_9326)
													{	/* SawBbv/bbv-types.scm 1286 */
														BgL_res3664z00_9321 = BgL__ortest_1115z00_9326;
													}
												else
													{	/* SawBbv/bbv-types.scm 1286 */
														long BgL_odepthz00_9327;

														{	/* SawBbv/bbv-types.scm 1286 */
															obj_t BgL_arg1804z00_9328;

															BgL_arg1804z00_9328 = (BgL_oclassz00_9322);
															BgL_odepthz00_9327 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9328);
														}
														if ((3L < BgL_odepthz00_9327))
															{	/* SawBbv/bbv-types.scm 1286 */
																obj_t BgL_arg1802z00_9329;

																{	/* SawBbv/bbv-types.scm 1286 */
																	obj_t BgL_arg1803z00_9330;

																	BgL_arg1803z00_9330 = (BgL_oclassz00_9322);
																	BgL_arg1802z00_9329 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9330,
																		3L);
																}
																BgL_res3664z00_9321 =
																	(BgL_arg1802z00_9329 == BgL_classz00_9316);
															}
														else
															{	/* SawBbv/bbv-types.scm 1286 */
																BgL_res3664z00_9321 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4449z00_14186 = BgL_res3664z00_9321;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1286 */
								BgL_test4449z00_14186 = ((bool_t) 0);
							}
					}
					if (BgL_test4449z00_14186)
						{	/* SawBbv/bbv-types.scm 1287 */
							bool_t BgL_test4454z00_14209;

							{	/* SawBbv/bbv-types.scm 1287 */
								BgL_typez00_bglt BgL_arg3382z00_9331;
								BgL_typez00_bglt BgL_arg3383z00_9332;

								BgL_arg3382z00_9331 =
									(((BgL_rtl_vsetz00_bglt) COBJECT(
											((BgL_rtl_vsetz00_bglt) BgL_xz00_8724)))->BgL_typez00);
								BgL_arg3383z00_9332 =
									(((BgL_rtl_vsetz00_bglt) COBJECT(
											((BgL_rtl_vsetz00_bglt) BgL_yz00_8725)))->BgL_typez00);
								BgL_test4454z00_14209 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3382z00_9331), ((obj_t) BgL_arg3383z00_9332));
							}
							if (BgL_test4454z00_14209)
								{	/* SawBbv/bbv-types.scm 1288 */
									BgL_typez00_bglt BgL_arg3379z00_9333;
									BgL_typez00_bglt BgL_arg3381z00_9334;

									BgL_arg3379z00_9333 =
										(((BgL_rtl_vsetz00_bglt) COBJECT(
												((BgL_rtl_vsetz00_bglt) BgL_xz00_8724)))->BgL_vtypez00);
									BgL_arg3381z00_9334 =
										(((BgL_rtl_vsetz00_bglt) COBJECT(
												((BgL_rtl_vsetz00_bglt) BgL_yz00_8725)))->BgL_vtypez00);
									BgL_tmpz00_14185 =
										BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
										((obj_t) BgL_arg3379z00_9333),
										((obj_t) BgL_arg3381z00_9334));
								}
							else
								{	/* SawBbv/bbv-types.scm 1287 */
									BgL_tmpz00_14185 = ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1286 */
							BgL_tmpz00_14185 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14185);
			}
		}

	}



/* &bbv-equal?-rtl_setfi1921 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_setfi1921z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8726, obj_t BgL_xz00_8727, obj_t BgL_yz00_8728)
	{
		{	/* SawBbv/bbv-types.scm 1276 */
			{	/* SawBbv/bbv-types.scm 1277 */
				bool_t BgL_tmpz00_14225;

				{	/* SawBbv/bbv-types.scm 1277 */
					bool_t BgL_test4455z00_14226;

					{	/* SawBbv/bbv-types.scm 1277 */
						obj_t BgL_classz00_9336;

						BgL_classz00_9336 = BGl_rtl_setfieldz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8728))
							{	/* SawBbv/bbv-types.scm 1277 */
								BgL_objectz00_bglt BgL_arg1807z00_9337;

								BgL_arg1807z00_9337 = (BgL_objectz00_bglt) (BgL_yz00_8728);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1277 */
										long BgL_idxz00_9338;

										BgL_idxz00_9338 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9337);
										BgL_test4455z00_14226 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9338 + 3L)) == BgL_classz00_9336);
									}
								else
									{	/* SawBbv/bbv-types.scm 1277 */
										bool_t BgL_res3663z00_9341;

										{	/* SawBbv/bbv-types.scm 1277 */
											obj_t BgL_oclassz00_9342;

											{	/* SawBbv/bbv-types.scm 1277 */
												obj_t BgL_arg1815z00_9343;
												long BgL_arg1816z00_9344;

												BgL_arg1815z00_9343 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1277 */
													long BgL_arg1817z00_9345;

													BgL_arg1817z00_9345 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9337);
													BgL_arg1816z00_9344 =
														(BgL_arg1817z00_9345 - OBJECT_TYPE);
												}
												BgL_oclassz00_9342 =
													VECTOR_REF(BgL_arg1815z00_9343, BgL_arg1816z00_9344);
											}
											{	/* SawBbv/bbv-types.scm 1277 */
												bool_t BgL__ortest_1115z00_9346;

												BgL__ortest_1115z00_9346 =
													(BgL_classz00_9336 == BgL_oclassz00_9342);
												if (BgL__ortest_1115z00_9346)
													{	/* SawBbv/bbv-types.scm 1277 */
														BgL_res3663z00_9341 = BgL__ortest_1115z00_9346;
													}
												else
													{	/* SawBbv/bbv-types.scm 1277 */
														long BgL_odepthz00_9347;

														{	/* SawBbv/bbv-types.scm 1277 */
															obj_t BgL_arg1804z00_9348;

															BgL_arg1804z00_9348 = (BgL_oclassz00_9342);
															BgL_odepthz00_9347 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9348);
														}
														if ((3L < BgL_odepthz00_9347))
															{	/* SawBbv/bbv-types.scm 1277 */
																obj_t BgL_arg1802z00_9349;

																{	/* SawBbv/bbv-types.scm 1277 */
																	obj_t BgL_arg1803z00_9350;

																	BgL_arg1803z00_9350 = (BgL_oclassz00_9342);
																	BgL_arg1802z00_9349 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9350,
																		3L);
																}
																BgL_res3663z00_9341 =
																	(BgL_arg1802z00_9349 == BgL_classz00_9336);
															}
														else
															{	/* SawBbv/bbv-types.scm 1277 */
																BgL_res3663z00_9341 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4455z00_14226 = BgL_res3663z00_9341;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1277 */
								BgL_test4455z00_14226 = ((bool_t) 0);
							}
					}
					if (BgL_test4455z00_14226)
						{	/* SawBbv/bbv-types.scm 1278 */
							bool_t BgL_test4460z00_14249;

							{	/* SawBbv/bbv-types.scm 1278 */
								obj_t BgL_arg3373z00_9351;
								obj_t BgL_arg3375z00_9352;

								BgL_arg3373z00_9351 =
									(((BgL_rtl_setfieldz00_bglt) COBJECT(
											((BgL_rtl_setfieldz00_bglt) BgL_xz00_8727)))->
									BgL_namez00);
								BgL_arg3375z00_9352 =
									(((BgL_rtl_setfieldz00_bglt)
										COBJECT(((BgL_rtl_setfieldz00_bglt) BgL_yz00_8728)))->
									BgL_namez00);
								{	/* SawBbv/bbv-types.scm 1278 */
									long BgL_l1z00_9353;

									BgL_l1z00_9353 = STRING_LENGTH(BgL_arg3373z00_9351);
									if ((BgL_l1z00_9353 == STRING_LENGTH(BgL_arg3375z00_9352)))
										{	/* SawBbv/bbv-types.scm 1278 */
											int BgL_arg1282z00_9354;

											{	/* SawBbv/bbv-types.scm 1278 */
												char *BgL_auxz00_14260;
												char *BgL_tmpz00_14258;

												BgL_auxz00_14260 =
													BSTRING_TO_STRING(BgL_arg3375z00_9352);
												BgL_tmpz00_14258 =
													BSTRING_TO_STRING(BgL_arg3373z00_9351);
												BgL_arg1282z00_9354 =
													memcmp(BgL_tmpz00_14258, BgL_auxz00_14260,
													BgL_l1z00_9353);
											}
											BgL_test4460z00_14249 =
												((long) (BgL_arg1282z00_9354) == 0L);
										}
									else
										{	/* SawBbv/bbv-types.scm 1278 */
											BgL_test4460z00_14249 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test4460z00_14249)
								{	/* SawBbv/bbv-types.scm 1279 */
									bool_t BgL_test4462z00_14265;

									{	/* SawBbv/bbv-types.scm 1279 */
										BgL_typez00_bglt BgL_arg3371z00_9355;
										BgL_typez00_bglt BgL_arg3372z00_9356;

										BgL_arg3371z00_9355 =
											(((BgL_rtl_setfieldz00_bglt) COBJECT(
													((BgL_rtl_setfieldz00_bglt) BgL_xz00_8727)))->
											BgL_objtypez00);
										BgL_arg3372z00_9356 =
											(((BgL_rtl_setfieldz00_bglt)
												COBJECT(((BgL_rtl_setfieldz00_bglt) BgL_yz00_8728)))->
											BgL_objtypez00);
										BgL_test4462z00_14265 =
											BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
												BgL_arg3371z00_9355), ((obj_t) BgL_arg3372z00_9356));
									}
									if (BgL_test4462z00_14265)
										{	/* SawBbv/bbv-types.scm 1280 */
											BgL_typez00_bglt BgL_arg3369z00_9357;
											BgL_typez00_bglt BgL_arg3370z00_9358;

											BgL_arg3369z00_9357 =
												(((BgL_rtl_setfieldz00_bglt) COBJECT(
														((BgL_rtl_setfieldz00_bglt) BgL_xz00_8727)))->
												BgL_typez00);
											BgL_arg3370z00_9358 =
												(((BgL_rtl_setfieldz00_bglt)
													COBJECT(((BgL_rtl_setfieldz00_bglt) BgL_yz00_8728)))->
												BgL_typez00);
											BgL_tmpz00_14225 =
												BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
													BgL_arg3369z00_9357), ((obj_t) BgL_arg3370z00_9358));
										}
									else
										{	/* SawBbv/bbv-types.scm 1279 */
											BgL_tmpz00_14225 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 1278 */
									BgL_tmpz00_14225 = ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1277 */
							BgL_tmpz00_14225 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14225);
			}
		}

	}



/* &bbv-equal?-rtl_store1919 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_store1919z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8729, obj_t BgL_xz00_8730, obj_t BgL_yz00_8731)
	{
		{	/* SawBbv/bbv-types.scm 1269 */
			{	/* SawBbv/bbv-types.scm 1270 */
				bool_t BgL_tmpz00_14281;

				{	/* SawBbv/bbv-types.scm 1270 */
					bool_t BgL_test4463z00_14282;

					{	/* SawBbv/bbv-types.scm 1270 */
						obj_t BgL_classz00_9360;

						BgL_classz00_9360 = BGl_rtl_storegz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8731))
							{	/* SawBbv/bbv-types.scm 1270 */
								BgL_objectz00_bglt BgL_arg1807z00_9361;

								BgL_arg1807z00_9361 = (BgL_objectz00_bglt) (BgL_yz00_8731);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1270 */
										long BgL_idxz00_9362;

										BgL_idxz00_9362 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9361);
										BgL_test4463z00_14282 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9362 + 3L)) == BgL_classz00_9360);
									}
								else
									{	/* SawBbv/bbv-types.scm 1270 */
										bool_t BgL_res3662z00_9365;

										{	/* SawBbv/bbv-types.scm 1270 */
											obj_t BgL_oclassz00_9366;

											{	/* SawBbv/bbv-types.scm 1270 */
												obj_t BgL_arg1815z00_9367;
												long BgL_arg1816z00_9368;

												BgL_arg1815z00_9367 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1270 */
													long BgL_arg1817z00_9369;

													BgL_arg1817z00_9369 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9361);
													BgL_arg1816z00_9368 =
														(BgL_arg1817z00_9369 - OBJECT_TYPE);
												}
												BgL_oclassz00_9366 =
													VECTOR_REF(BgL_arg1815z00_9367, BgL_arg1816z00_9368);
											}
											{	/* SawBbv/bbv-types.scm 1270 */
												bool_t BgL__ortest_1115z00_9370;

												BgL__ortest_1115z00_9370 =
													(BgL_classz00_9360 == BgL_oclassz00_9366);
												if (BgL__ortest_1115z00_9370)
													{	/* SawBbv/bbv-types.scm 1270 */
														BgL_res3662z00_9365 = BgL__ortest_1115z00_9370;
													}
												else
													{	/* SawBbv/bbv-types.scm 1270 */
														long BgL_odepthz00_9371;

														{	/* SawBbv/bbv-types.scm 1270 */
															obj_t BgL_arg1804z00_9372;

															BgL_arg1804z00_9372 = (BgL_oclassz00_9366);
															BgL_odepthz00_9371 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9372);
														}
														if ((3L < BgL_odepthz00_9371))
															{	/* SawBbv/bbv-types.scm 1270 */
																obj_t BgL_arg1802z00_9373;

																{	/* SawBbv/bbv-types.scm 1270 */
																	obj_t BgL_arg1803z00_9374;

																	BgL_arg1803z00_9374 = (BgL_oclassz00_9366);
																	BgL_arg1802z00_9373 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9374,
																		3L);
																}
																BgL_res3662z00_9365 =
																	(BgL_arg1802z00_9373 == BgL_classz00_9360);
															}
														else
															{	/* SawBbv/bbv-types.scm 1270 */
																BgL_res3662z00_9365 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4463z00_14282 = BgL_res3662z00_9365;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1270 */
								BgL_test4463z00_14282 = ((bool_t) 0);
							}
					}
					if (BgL_test4463z00_14282)
						{	/* SawBbv/bbv-types.scm 1271 */
							BgL_globalz00_bglt BgL_arg3364z00_9375;
							BgL_globalz00_bglt BgL_arg3367z00_9376;

							BgL_arg3364z00_9375 =
								(((BgL_rtl_storegz00_bglt) COBJECT(
										((BgL_rtl_storegz00_bglt) BgL_xz00_8730)))->BgL_varz00);
							BgL_arg3367z00_9376 =
								(((BgL_rtl_storegz00_bglt) COBJECT(
										((BgL_rtl_storegz00_bglt) BgL_yz00_8731)))->BgL_varz00);
							BgL_tmpz00_14281 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3364z00_9375), ((obj_t) BgL_arg3367z00_9376));
						}
					else
						{	/* SawBbv/bbv-types.scm 1270 */
							BgL_tmpz00_14281 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14281);
			}
		}

	}



/* &bbv-equal?-rtl_insta1917 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_insta1917z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8732, obj_t BgL_xz00_8733, obj_t BgL_yz00_8734)
	{
		{	/* SawBbv/bbv-types.scm 1262 */
			{	/* SawBbv/bbv-types.scm 1263 */
				bool_t BgL_tmpz00_14313;

				{	/* SawBbv/bbv-types.scm 1263 */
					bool_t BgL_test4468z00_14314;

					{	/* SawBbv/bbv-types.scm 1263 */
						obj_t BgL_classz00_9378;

						BgL_classz00_9378 = BGl_rtl_instanceofz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8734))
							{	/* SawBbv/bbv-types.scm 1263 */
								BgL_objectz00_bglt BgL_arg1807z00_9379;

								BgL_arg1807z00_9379 = (BgL_objectz00_bglt) (BgL_yz00_8734);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1263 */
										long BgL_idxz00_9380;

										BgL_idxz00_9380 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9379);
										BgL_test4468z00_14314 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9380 + 3L)) == BgL_classz00_9378);
									}
								else
									{	/* SawBbv/bbv-types.scm 1263 */
										bool_t BgL_res3661z00_9383;

										{	/* SawBbv/bbv-types.scm 1263 */
											obj_t BgL_oclassz00_9384;

											{	/* SawBbv/bbv-types.scm 1263 */
												obj_t BgL_arg1815z00_9385;
												long BgL_arg1816z00_9386;

												BgL_arg1815z00_9385 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1263 */
													long BgL_arg1817z00_9387;

													BgL_arg1817z00_9387 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9379);
													BgL_arg1816z00_9386 =
														(BgL_arg1817z00_9387 - OBJECT_TYPE);
												}
												BgL_oclassz00_9384 =
													VECTOR_REF(BgL_arg1815z00_9385, BgL_arg1816z00_9386);
											}
											{	/* SawBbv/bbv-types.scm 1263 */
												bool_t BgL__ortest_1115z00_9388;

												BgL__ortest_1115z00_9388 =
													(BgL_classz00_9378 == BgL_oclassz00_9384);
												if (BgL__ortest_1115z00_9388)
													{	/* SawBbv/bbv-types.scm 1263 */
														BgL_res3661z00_9383 = BgL__ortest_1115z00_9388;
													}
												else
													{	/* SawBbv/bbv-types.scm 1263 */
														long BgL_odepthz00_9389;

														{	/* SawBbv/bbv-types.scm 1263 */
															obj_t BgL_arg1804z00_9390;

															BgL_arg1804z00_9390 = (BgL_oclassz00_9384);
															BgL_odepthz00_9389 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9390);
														}
														if ((3L < BgL_odepthz00_9389))
															{	/* SawBbv/bbv-types.scm 1263 */
																obj_t BgL_arg1802z00_9391;

																{	/* SawBbv/bbv-types.scm 1263 */
																	obj_t BgL_arg1803z00_9392;

																	BgL_arg1803z00_9392 = (BgL_oclassz00_9384);
																	BgL_arg1802z00_9391 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9392,
																		3L);
																}
																BgL_res3661z00_9383 =
																	(BgL_arg1802z00_9391 == BgL_classz00_9378);
															}
														else
															{	/* SawBbv/bbv-types.scm 1263 */
																BgL_res3661z00_9383 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4468z00_14314 = BgL_res3661z00_9383;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1263 */
								BgL_test4468z00_14314 = ((bool_t) 0);
							}
					}
					if (BgL_test4468z00_14314)
						{	/* SawBbv/bbv-types.scm 1264 */
							BgL_typez00_bglt BgL_arg3361z00_9393;
							BgL_typez00_bglt BgL_arg3362z00_9394;

							BgL_arg3361z00_9393 =
								(((BgL_rtl_instanceofz00_bglt) COBJECT(
										((BgL_rtl_instanceofz00_bglt) BgL_xz00_8733)))->
								BgL_typez00);
							BgL_arg3362z00_9394 =
								(((BgL_rtl_instanceofz00_bglt)
									COBJECT(((BgL_rtl_instanceofz00_bglt) BgL_yz00_8734)))->
								BgL_typez00);
							BgL_tmpz00_14313 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
									BgL_arg3361z00_9393), ((obj_t) BgL_arg3362z00_9394));
						}
					else
						{	/* SawBbv/bbv-types.scm 1263 */
							BgL_tmpz00_14313 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14313);
			}
		}

	}



/* &bbv-equal?-rtl_vleng1915 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_vleng1915z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8735, obj_t BgL_xz00_8736, obj_t BgL_yz00_8737)
	{
		{	/* SawBbv/bbv-types.scm 1254 */
			{	/* SawBbv/bbv-types.scm 1255 */
				bool_t BgL_tmpz00_14345;

				{	/* SawBbv/bbv-types.scm 1255 */
					bool_t BgL_test4473z00_14346;

					{	/* SawBbv/bbv-types.scm 1255 */
						obj_t BgL_classz00_9396;

						BgL_classz00_9396 = BGl_rtl_vlengthz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8737))
							{	/* SawBbv/bbv-types.scm 1255 */
								BgL_objectz00_bglt BgL_arg1807z00_9397;

								BgL_arg1807z00_9397 = (BgL_objectz00_bglt) (BgL_yz00_8737);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1255 */
										long BgL_idxz00_9398;

										BgL_idxz00_9398 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9397);
										BgL_test4473z00_14346 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9398 + 3L)) == BgL_classz00_9396);
									}
								else
									{	/* SawBbv/bbv-types.scm 1255 */
										bool_t BgL_res3660z00_9401;

										{	/* SawBbv/bbv-types.scm 1255 */
											obj_t BgL_oclassz00_9402;

											{	/* SawBbv/bbv-types.scm 1255 */
												obj_t BgL_arg1815z00_9403;
												long BgL_arg1816z00_9404;

												BgL_arg1815z00_9403 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1255 */
													long BgL_arg1817z00_9405;

													BgL_arg1817z00_9405 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9397);
													BgL_arg1816z00_9404 =
														(BgL_arg1817z00_9405 - OBJECT_TYPE);
												}
												BgL_oclassz00_9402 =
													VECTOR_REF(BgL_arg1815z00_9403, BgL_arg1816z00_9404);
											}
											{	/* SawBbv/bbv-types.scm 1255 */
												bool_t BgL__ortest_1115z00_9406;

												BgL__ortest_1115z00_9406 =
													(BgL_classz00_9396 == BgL_oclassz00_9402);
												if (BgL__ortest_1115z00_9406)
													{	/* SawBbv/bbv-types.scm 1255 */
														BgL_res3660z00_9401 = BgL__ortest_1115z00_9406;
													}
												else
													{	/* SawBbv/bbv-types.scm 1255 */
														long BgL_odepthz00_9407;

														{	/* SawBbv/bbv-types.scm 1255 */
															obj_t BgL_arg1804z00_9408;

															BgL_arg1804z00_9408 = (BgL_oclassz00_9402);
															BgL_odepthz00_9407 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9408);
														}
														if ((3L < BgL_odepthz00_9407))
															{	/* SawBbv/bbv-types.scm 1255 */
																obj_t BgL_arg1802z00_9409;

																{	/* SawBbv/bbv-types.scm 1255 */
																	obj_t BgL_arg1803z00_9410;

																	BgL_arg1803z00_9410 = (BgL_oclassz00_9402);
																	BgL_arg1802z00_9409 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9410,
																		3L);
																}
																BgL_res3660z00_9401 =
																	(BgL_arg1802z00_9409 == BgL_classz00_9396);
															}
														else
															{	/* SawBbv/bbv-types.scm 1255 */
																BgL_res3660z00_9401 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4473z00_14346 = BgL_res3660z00_9401;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1255 */
								BgL_test4473z00_14346 = ((bool_t) 0);
							}
					}
					if (BgL_test4473z00_14346)
						{	/* SawBbv/bbv-types.scm 1255 */
							{	/* SawBbv/bbv-types.scm 1256 */
								BgL_typez00_bglt BgL_arg3356z00_9411;
								BgL_typez00_bglt BgL_arg3357z00_9412;

								BgL_arg3356z00_9411 =
									(((BgL_rtl_vlengthz00_bglt) COBJECT(
											((BgL_rtl_vlengthz00_bglt) BgL_xz00_8736)))->BgL_typez00);
								BgL_arg3357z00_9412 =
									(((BgL_rtl_vlengthz00_bglt) COBJECT(
											((BgL_rtl_vlengthz00_bglt) BgL_yz00_8737)))->BgL_typez00);
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3356z00_9411), ((obj_t) BgL_arg3357z00_9412));
							}
							{	/* SawBbv/bbv-types.scm 1257 */
								BgL_typez00_bglt BgL_arg3358z00_9413;
								BgL_typez00_bglt BgL_arg3359z00_9414;

								BgL_arg3358z00_9413 =
									(((BgL_rtl_vlengthz00_bglt) COBJECT(
											((BgL_rtl_vlengthz00_bglt) BgL_xz00_8736)))->
									BgL_vtypez00);
								BgL_arg3359z00_9414 =
									(((BgL_rtl_vlengthz00_bglt) COBJECT(((BgL_rtl_vlengthz00_bglt)
												BgL_yz00_8737)))->BgL_vtypez00);
								BgL_tmpz00_14345 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
										BgL_arg3358z00_9413), ((obj_t) BgL_arg3359z00_9414));
							}
						}
					else
						{	/* SawBbv/bbv-types.scm 1255 */
							BgL_tmpz00_14345 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14345);
			}
		}

	}



/* &bbv-equal?-rtl_vref1913 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_vref1913z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8738, obj_t BgL_xz00_8739, obj_t BgL_yz00_8740)
	{
		{	/* SawBbv/bbv-types.scm 1246 */
			{	/* SawBbv/bbv-types.scm 1247 */
				bool_t BgL_tmpz00_14384;

				{	/* SawBbv/bbv-types.scm 1247 */
					bool_t BgL_test4478z00_14385;

					{	/* SawBbv/bbv-types.scm 1247 */
						obj_t BgL_classz00_9416;

						BgL_classz00_9416 = BGl_rtl_vrefz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8740))
							{	/* SawBbv/bbv-types.scm 1247 */
								BgL_objectz00_bglt BgL_arg1807z00_9417;

								BgL_arg1807z00_9417 = (BgL_objectz00_bglt) (BgL_yz00_8740);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1247 */
										long BgL_idxz00_9418;

										BgL_idxz00_9418 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9417);
										BgL_test4478z00_14385 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9418 + 3L)) == BgL_classz00_9416);
									}
								else
									{	/* SawBbv/bbv-types.scm 1247 */
										bool_t BgL_res3659z00_9421;

										{	/* SawBbv/bbv-types.scm 1247 */
											obj_t BgL_oclassz00_9422;

											{	/* SawBbv/bbv-types.scm 1247 */
												obj_t BgL_arg1815z00_9423;
												long BgL_arg1816z00_9424;

												BgL_arg1815z00_9423 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1247 */
													long BgL_arg1817z00_9425;

													BgL_arg1817z00_9425 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9417);
													BgL_arg1816z00_9424 =
														(BgL_arg1817z00_9425 - OBJECT_TYPE);
												}
												BgL_oclassz00_9422 =
													VECTOR_REF(BgL_arg1815z00_9423, BgL_arg1816z00_9424);
											}
											{	/* SawBbv/bbv-types.scm 1247 */
												bool_t BgL__ortest_1115z00_9426;

												BgL__ortest_1115z00_9426 =
													(BgL_classz00_9416 == BgL_oclassz00_9422);
												if (BgL__ortest_1115z00_9426)
													{	/* SawBbv/bbv-types.scm 1247 */
														BgL_res3659z00_9421 = BgL__ortest_1115z00_9426;
													}
												else
													{	/* SawBbv/bbv-types.scm 1247 */
														long BgL_odepthz00_9427;

														{	/* SawBbv/bbv-types.scm 1247 */
															obj_t BgL_arg1804z00_9428;

															BgL_arg1804z00_9428 = (BgL_oclassz00_9422);
															BgL_odepthz00_9427 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9428);
														}
														if ((3L < BgL_odepthz00_9427))
															{	/* SawBbv/bbv-types.scm 1247 */
																obj_t BgL_arg1802z00_9429;

																{	/* SawBbv/bbv-types.scm 1247 */
																	obj_t BgL_arg1803z00_9430;

																	BgL_arg1803z00_9430 = (BgL_oclassz00_9422);
																	BgL_arg1802z00_9429 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9430,
																		3L);
																}
																BgL_res3659z00_9421 =
																	(BgL_arg1802z00_9429 == BgL_classz00_9416);
															}
														else
															{	/* SawBbv/bbv-types.scm 1247 */
																BgL_res3659z00_9421 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4478z00_14385 = BgL_res3659z00_9421;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1247 */
								BgL_test4478z00_14385 = ((bool_t) 0);
							}
					}
					if (BgL_test4478z00_14385)
						{	/* SawBbv/bbv-types.scm 1247 */
							{	/* SawBbv/bbv-types.scm 1248 */
								BgL_typez00_bglt BgL_arg3339z00_9431;
								BgL_typez00_bglt BgL_arg3349z00_9432;

								BgL_arg3339z00_9431 =
									(((BgL_rtl_vrefz00_bglt) COBJECT(
											((BgL_rtl_vrefz00_bglt) BgL_xz00_8739)))->BgL_typez00);
								BgL_arg3349z00_9432 =
									(((BgL_rtl_vrefz00_bglt) COBJECT(
											((BgL_rtl_vrefz00_bglt) BgL_yz00_8740)))->BgL_typez00);
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3339z00_9431), ((obj_t) BgL_arg3349z00_9432));
							}
							{	/* SawBbv/bbv-types.scm 1249 */
								BgL_typez00_bglt BgL_arg3350z00_9433;
								BgL_typez00_bglt BgL_arg3352z00_9434;

								BgL_arg3350z00_9433 =
									(((BgL_rtl_vrefz00_bglt) COBJECT(
											((BgL_rtl_vrefz00_bglt) BgL_xz00_8739)))->BgL_vtypez00);
								BgL_arg3352z00_9434 =
									(((BgL_rtl_vrefz00_bglt) COBJECT(
											((BgL_rtl_vrefz00_bglt) BgL_yz00_8740)))->BgL_vtypez00);
								BgL_tmpz00_14384 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3350z00_9433), ((obj_t) BgL_arg3352z00_9434));
							}
						}
					else
						{	/* SawBbv/bbv-types.scm 1247 */
							BgL_tmpz00_14384 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14384);
			}
		}

	}



/* &bbv-equal?-rtl_vallo1911 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_vallo1911z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8741, obj_t BgL_xz00_8742, obj_t BgL_yz00_8743)
	{
		{	/* SawBbv/bbv-types.scm 1238 */
			{	/* SawBbv/bbv-types.scm 1239 */
				bool_t BgL_tmpz00_14423;

				{	/* SawBbv/bbv-types.scm 1239 */
					bool_t BgL_test4483z00_14424;

					{	/* SawBbv/bbv-types.scm 1239 */
						obj_t BgL_classz00_9436;

						BgL_classz00_9436 = BGl_rtl_vallocz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8743))
							{	/* SawBbv/bbv-types.scm 1239 */
								BgL_objectz00_bglt BgL_arg1807z00_9437;

								BgL_arg1807z00_9437 = (BgL_objectz00_bglt) (BgL_yz00_8743);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1239 */
										long BgL_idxz00_9438;

										BgL_idxz00_9438 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9437);
										BgL_test4483z00_14424 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9438 + 3L)) == BgL_classz00_9436);
									}
								else
									{	/* SawBbv/bbv-types.scm 1239 */
										bool_t BgL_res3658z00_9441;

										{	/* SawBbv/bbv-types.scm 1239 */
											obj_t BgL_oclassz00_9442;

											{	/* SawBbv/bbv-types.scm 1239 */
												obj_t BgL_arg1815z00_9443;
												long BgL_arg1816z00_9444;

												BgL_arg1815z00_9443 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1239 */
													long BgL_arg1817z00_9445;

													BgL_arg1817z00_9445 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9437);
													BgL_arg1816z00_9444 =
														(BgL_arg1817z00_9445 - OBJECT_TYPE);
												}
												BgL_oclassz00_9442 =
													VECTOR_REF(BgL_arg1815z00_9443, BgL_arg1816z00_9444);
											}
											{	/* SawBbv/bbv-types.scm 1239 */
												bool_t BgL__ortest_1115z00_9446;

												BgL__ortest_1115z00_9446 =
													(BgL_classz00_9436 == BgL_oclassz00_9442);
												if (BgL__ortest_1115z00_9446)
													{	/* SawBbv/bbv-types.scm 1239 */
														BgL_res3658z00_9441 = BgL__ortest_1115z00_9446;
													}
												else
													{	/* SawBbv/bbv-types.scm 1239 */
														long BgL_odepthz00_9447;

														{	/* SawBbv/bbv-types.scm 1239 */
															obj_t BgL_arg1804z00_9448;

															BgL_arg1804z00_9448 = (BgL_oclassz00_9442);
															BgL_odepthz00_9447 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9448);
														}
														if ((3L < BgL_odepthz00_9447))
															{	/* SawBbv/bbv-types.scm 1239 */
																obj_t BgL_arg1802z00_9449;

																{	/* SawBbv/bbv-types.scm 1239 */
																	obj_t BgL_arg1803z00_9450;

																	BgL_arg1803z00_9450 = (BgL_oclassz00_9442);
																	BgL_arg1802z00_9449 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9450,
																		3L);
																}
																BgL_res3658z00_9441 =
																	(BgL_arg1802z00_9449 == BgL_classz00_9436);
															}
														else
															{	/* SawBbv/bbv-types.scm 1239 */
																BgL_res3658z00_9441 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4483z00_14424 = BgL_res3658z00_9441;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1239 */
								BgL_test4483z00_14424 = ((bool_t) 0);
							}
					}
					if (BgL_test4483z00_14424)
						{	/* SawBbv/bbv-types.scm 1239 */
							{	/* SawBbv/bbv-types.scm 1240 */
								BgL_typez00_bglt BgL_arg3323z00_9451;
								BgL_typez00_bglt BgL_arg3326z00_9452;

								BgL_arg3323z00_9451 =
									(((BgL_rtl_vallocz00_bglt) COBJECT(
											((BgL_rtl_vallocz00_bglt) BgL_xz00_8742)))->BgL_typez00);
								BgL_arg3326z00_9452 =
									(((BgL_rtl_vallocz00_bglt) COBJECT(
											((BgL_rtl_vallocz00_bglt) BgL_yz00_8743)))->BgL_typez00);
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3323z00_9451), ((obj_t) BgL_arg3326z00_9452));
							}
							{	/* SawBbv/bbv-types.scm 1241 */
								BgL_typez00_bglt BgL_arg3327z00_9453;
								BgL_typez00_bglt BgL_arg3337z00_9454;

								BgL_arg3327z00_9453 =
									(((BgL_rtl_vallocz00_bglt) COBJECT(
											((BgL_rtl_vallocz00_bglt) BgL_xz00_8742)))->BgL_vtypez00);
								BgL_arg3337z00_9454 =
									(((BgL_rtl_vallocz00_bglt) COBJECT(
											((BgL_rtl_vallocz00_bglt) BgL_yz00_8743)))->BgL_vtypez00);
								BgL_tmpz00_14423 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3327z00_9453), ((obj_t) BgL_arg3337z00_9454));
							}
						}
					else
						{	/* SawBbv/bbv-types.scm 1239 */
							BgL_tmpz00_14423 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14423);
			}
		}

	}



/* &bbv-equal?-rtl_getfi1909 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_getfi1909z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8744, obj_t BgL_xz00_8745, obj_t BgL_yz00_8746)
	{
		{	/* SawBbv/bbv-types.scm 1229 */
			{	/* SawBbv/bbv-types.scm 1230 */
				bool_t BgL_tmpz00_14462;

				{	/* SawBbv/bbv-types.scm 1230 */
					bool_t BgL_test4488z00_14463;

					{	/* SawBbv/bbv-types.scm 1230 */
						obj_t BgL_classz00_9456;

						BgL_classz00_9456 = BGl_rtl_getfieldz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8746))
							{	/* SawBbv/bbv-types.scm 1230 */
								BgL_objectz00_bglt BgL_arg1807z00_9457;

								BgL_arg1807z00_9457 = (BgL_objectz00_bglt) (BgL_yz00_8746);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1230 */
										long BgL_idxz00_9458;

										BgL_idxz00_9458 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9457);
										BgL_test4488z00_14463 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9458 + 3L)) == BgL_classz00_9456);
									}
								else
									{	/* SawBbv/bbv-types.scm 1230 */
										bool_t BgL_res3657z00_9461;

										{	/* SawBbv/bbv-types.scm 1230 */
											obj_t BgL_oclassz00_9462;

											{	/* SawBbv/bbv-types.scm 1230 */
												obj_t BgL_arg1815z00_9463;
												long BgL_arg1816z00_9464;

												BgL_arg1815z00_9463 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1230 */
													long BgL_arg1817z00_9465;

													BgL_arg1817z00_9465 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9457);
													BgL_arg1816z00_9464 =
														(BgL_arg1817z00_9465 - OBJECT_TYPE);
												}
												BgL_oclassz00_9462 =
													VECTOR_REF(BgL_arg1815z00_9463, BgL_arg1816z00_9464);
											}
											{	/* SawBbv/bbv-types.scm 1230 */
												bool_t BgL__ortest_1115z00_9466;

												BgL__ortest_1115z00_9466 =
													(BgL_classz00_9456 == BgL_oclassz00_9462);
												if (BgL__ortest_1115z00_9466)
													{	/* SawBbv/bbv-types.scm 1230 */
														BgL_res3657z00_9461 = BgL__ortest_1115z00_9466;
													}
												else
													{	/* SawBbv/bbv-types.scm 1230 */
														long BgL_odepthz00_9467;

														{	/* SawBbv/bbv-types.scm 1230 */
															obj_t BgL_arg1804z00_9468;

															BgL_arg1804z00_9468 = (BgL_oclassz00_9462);
															BgL_odepthz00_9467 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9468);
														}
														if ((3L < BgL_odepthz00_9467))
															{	/* SawBbv/bbv-types.scm 1230 */
																obj_t BgL_arg1802z00_9469;

																{	/* SawBbv/bbv-types.scm 1230 */
																	obj_t BgL_arg1803z00_9470;

																	BgL_arg1803z00_9470 = (BgL_oclassz00_9462);
																	BgL_arg1802z00_9469 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9470,
																		3L);
																}
																BgL_res3657z00_9461 =
																	(BgL_arg1802z00_9469 == BgL_classz00_9456);
															}
														else
															{	/* SawBbv/bbv-types.scm 1230 */
																BgL_res3657z00_9461 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4488z00_14463 = BgL_res3657z00_9461;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1230 */
								BgL_test4488z00_14463 = ((bool_t) 0);
							}
					}
					if (BgL_test4488z00_14463)
						{	/* SawBbv/bbv-types.scm 1231 */
							bool_t BgL_test4493z00_14486;

							{	/* SawBbv/bbv-types.scm 1231 */
								obj_t BgL_arg3318z00_9471;
								obj_t BgL_arg3321z00_9472;

								BgL_arg3318z00_9471 =
									(((BgL_rtl_getfieldz00_bglt) COBJECT(
											((BgL_rtl_getfieldz00_bglt) BgL_xz00_8745)))->
									BgL_namez00);
								BgL_arg3321z00_9472 =
									(((BgL_rtl_getfieldz00_bglt)
										COBJECT(((BgL_rtl_getfieldz00_bglt) BgL_yz00_8746)))->
									BgL_namez00);
								{	/* SawBbv/bbv-types.scm 1231 */
									long BgL_l1z00_9473;

									BgL_l1z00_9473 = STRING_LENGTH(BgL_arg3318z00_9471);
									if ((BgL_l1z00_9473 == STRING_LENGTH(BgL_arg3321z00_9472)))
										{	/* SawBbv/bbv-types.scm 1231 */
											int BgL_arg1282z00_9474;

											{	/* SawBbv/bbv-types.scm 1231 */
												char *BgL_auxz00_14497;
												char *BgL_tmpz00_14495;

												BgL_auxz00_14497 =
													BSTRING_TO_STRING(BgL_arg3321z00_9472);
												BgL_tmpz00_14495 =
													BSTRING_TO_STRING(BgL_arg3318z00_9471);
												BgL_arg1282z00_9474 =
													memcmp(BgL_tmpz00_14495, BgL_auxz00_14497,
													BgL_l1z00_9473);
											}
											BgL_test4493z00_14486 =
												((long) (BgL_arg1282z00_9474) == 0L);
										}
									else
										{	/* SawBbv/bbv-types.scm 1231 */
											BgL_test4493z00_14486 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test4493z00_14486)
								{	/* SawBbv/bbv-types.scm 1232 */
									bool_t BgL_test4495z00_14502;

									{	/* SawBbv/bbv-types.scm 1232 */
										BgL_typez00_bglt BgL_arg3316z00_9475;
										BgL_typez00_bglt BgL_arg3317z00_9476;

										BgL_arg3316z00_9475 =
											(((BgL_rtl_getfieldz00_bglt) COBJECT(
													((BgL_rtl_getfieldz00_bglt) BgL_xz00_8745)))->
											BgL_objtypez00);
										BgL_arg3317z00_9476 =
											(((BgL_rtl_getfieldz00_bglt)
												COBJECT(((BgL_rtl_getfieldz00_bglt) BgL_yz00_8746)))->
											BgL_objtypez00);
										BgL_test4495z00_14502 =
											BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
												BgL_arg3316z00_9475), ((obj_t) BgL_arg3317z00_9476));
									}
									if (BgL_test4495z00_14502)
										{	/* SawBbv/bbv-types.scm 1233 */
											BgL_typez00_bglt BgL_arg3314z00_9477;
											BgL_typez00_bglt BgL_arg3315z00_9478;

											BgL_arg3314z00_9477 =
												(((BgL_rtl_getfieldz00_bglt) COBJECT(
														((BgL_rtl_getfieldz00_bglt) BgL_xz00_8745)))->
												BgL_typez00);
											BgL_arg3315z00_9478 =
												(((BgL_rtl_getfieldz00_bglt)
													COBJECT(((BgL_rtl_getfieldz00_bglt) BgL_yz00_8746)))->
												BgL_typez00);
											BgL_tmpz00_14462 =
												BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
													BgL_arg3314z00_9477), ((obj_t) BgL_arg3315z00_9478));
										}
									else
										{	/* SawBbv/bbv-types.scm 1232 */
											BgL_tmpz00_14462 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 1231 */
									BgL_tmpz00_14462 = ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1230 */
							BgL_tmpz00_14462 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14462);
			}
		}

	}



/* &bbv-equal?-rtl_globa1907 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_globa1907z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8747, obj_t BgL_xz00_8748, obj_t BgL_yz00_8749)
	{
		{	/* SawBbv/bbv-types.scm 1222 */
			{	/* SawBbv/bbv-types.scm 1223 */
				bool_t BgL_tmpz00_14518;

				{	/* SawBbv/bbv-types.scm 1223 */
					bool_t BgL_test4496z00_14519;

					{	/* SawBbv/bbv-types.scm 1223 */
						obj_t BgL_classz00_9480;

						BgL_classz00_9480 = BGl_rtl_globalrefz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8749))
							{	/* SawBbv/bbv-types.scm 1223 */
								BgL_objectz00_bglt BgL_arg1807z00_9481;

								BgL_arg1807z00_9481 = (BgL_objectz00_bglt) (BgL_yz00_8749);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1223 */
										long BgL_idxz00_9482;

										BgL_idxz00_9482 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9481);
										BgL_test4496z00_14519 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9482 + 3L)) == BgL_classz00_9480);
									}
								else
									{	/* SawBbv/bbv-types.scm 1223 */
										bool_t BgL_res3656z00_9485;

										{	/* SawBbv/bbv-types.scm 1223 */
											obj_t BgL_oclassz00_9486;

											{	/* SawBbv/bbv-types.scm 1223 */
												obj_t BgL_arg1815z00_9487;
												long BgL_arg1816z00_9488;

												BgL_arg1815z00_9487 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1223 */
													long BgL_arg1817z00_9489;

													BgL_arg1817z00_9489 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9481);
													BgL_arg1816z00_9488 =
														(BgL_arg1817z00_9489 - OBJECT_TYPE);
												}
												BgL_oclassz00_9486 =
													VECTOR_REF(BgL_arg1815z00_9487, BgL_arg1816z00_9488);
											}
											{	/* SawBbv/bbv-types.scm 1223 */
												bool_t BgL__ortest_1115z00_9490;

												BgL__ortest_1115z00_9490 =
													(BgL_classz00_9480 == BgL_oclassz00_9486);
												if (BgL__ortest_1115z00_9490)
													{	/* SawBbv/bbv-types.scm 1223 */
														BgL_res3656z00_9485 = BgL__ortest_1115z00_9490;
													}
												else
													{	/* SawBbv/bbv-types.scm 1223 */
														long BgL_odepthz00_9491;

														{	/* SawBbv/bbv-types.scm 1223 */
															obj_t BgL_arg1804z00_9492;

															BgL_arg1804z00_9492 = (BgL_oclassz00_9486);
															BgL_odepthz00_9491 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9492);
														}
														if ((3L < BgL_odepthz00_9491))
															{	/* SawBbv/bbv-types.scm 1223 */
																obj_t BgL_arg1802z00_9493;

																{	/* SawBbv/bbv-types.scm 1223 */
																	obj_t BgL_arg1803z00_9494;

																	BgL_arg1803z00_9494 = (BgL_oclassz00_9486);
																	BgL_arg1802z00_9493 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9494,
																		3L);
																}
																BgL_res3656z00_9485 =
																	(BgL_arg1802z00_9493 == BgL_classz00_9480);
															}
														else
															{	/* SawBbv/bbv-types.scm 1223 */
																BgL_res3656z00_9485 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4496z00_14519 = BgL_res3656z00_9485;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1223 */
								BgL_test4496z00_14519 = ((bool_t) 0);
							}
					}
					if (BgL_test4496z00_14519)
						{	/* SawBbv/bbv-types.scm 1224 */
							BgL_globalz00_bglt BgL_arg3311z00_9495;
							BgL_globalz00_bglt BgL_arg3312z00_9496;

							BgL_arg3311z00_9495 =
								(((BgL_rtl_globalrefz00_bglt) COBJECT(
										((BgL_rtl_globalrefz00_bglt) BgL_xz00_8748)))->BgL_varz00);
							BgL_arg3312z00_9496 =
								(((BgL_rtl_globalrefz00_bglt) COBJECT(
										((BgL_rtl_globalrefz00_bglt) BgL_yz00_8749)))->BgL_varz00);
							BgL_tmpz00_14518 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3311z00_9495), ((obj_t) BgL_arg3312z00_9496));
						}
					else
						{	/* SawBbv/bbv-types.scm 1223 */
							BgL_tmpz00_14518 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14518);
			}
		}

	}



/* &bbv-equal?-rtl_loadf1905 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_loadf1905z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8750, obj_t BgL_xz00_8751, obj_t BgL_yz00_8752)
	{
		{	/* SawBbv/bbv-types.scm 1215 */
			{	/* SawBbv/bbv-types.scm 1216 */
				bool_t BgL_tmpz00_14550;

				{	/* SawBbv/bbv-types.scm 1216 */
					bool_t BgL_test4501z00_14551;

					{	/* SawBbv/bbv-types.scm 1216 */
						obj_t BgL_classz00_9498;

						BgL_classz00_9498 = BGl_rtl_loadfunz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8752))
							{	/* SawBbv/bbv-types.scm 1216 */
								BgL_objectz00_bglt BgL_arg1807z00_9499;

								BgL_arg1807z00_9499 = (BgL_objectz00_bglt) (BgL_yz00_8752);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1216 */
										long BgL_idxz00_9500;

										BgL_idxz00_9500 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9499);
										BgL_test4501z00_14551 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9500 + 3L)) == BgL_classz00_9498);
									}
								else
									{	/* SawBbv/bbv-types.scm 1216 */
										bool_t BgL_res3655z00_9503;

										{	/* SawBbv/bbv-types.scm 1216 */
											obj_t BgL_oclassz00_9504;

											{	/* SawBbv/bbv-types.scm 1216 */
												obj_t BgL_arg1815z00_9505;
												long BgL_arg1816z00_9506;

												BgL_arg1815z00_9505 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1216 */
													long BgL_arg1817z00_9507;

													BgL_arg1817z00_9507 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9499);
													BgL_arg1816z00_9506 =
														(BgL_arg1817z00_9507 - OBJECT_TYPE);
												}
												BgL_oclassz00_9504 =
													VECTOR_REF(BgL_arg1815z00_9505, BgL_arg1816z00_9506);
											}
											{	/* SawBbv/bbv-types.scm 1216 */
												bool_t BgL__ortest_1115z00_9508;

												BgL__ortest_1115z00_9508 =
													(BgL_classz00_9498 == BgL_oclassz00_9504);
												if (BgL__ortest_1115z00_9508)
													{	/* SawBbv/bbv-types.scm 1216 */
														BgL_res3655z00_9503 = BgL__ortest_1115z00_9508;
													}
												else
													{	/* SawBbv/bbv-types.scm 1216 */
														long BgL_odepthz00_9509;

														{	/* SawBbv/bbv-types.scm 1216 */
															obj_t BgL_arg1804z00_9510;

															BgL_arg1804z00_9510 = (BgL_oclassz00_9504);
															BgL_odepthz00_9509 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9510);
														}
														if ((3L < BgL_odepthz00_9509))
															{	/* SawBbv/bbv-types.scm 1216 */
																obj_t BgL_arg1802z00_9511;

																{	/* SawBbv/bbv-types.scm 1216 */
																	obj_t BgL_arg1803z00_9512;

																	BgL_arg1803z00_9512 = (BgL_oclassz00_9504);
																	BgL_arg1802z00_9511 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9512,
																		3L);
																}
																BgL_res3655z00_9503 =
																	(BgL_arg1802z00_9511 == BgL_classz00_9498);
															}
														else
															{	/* SawBbv/bbv-types.scm 1216 */
																BgL_res3655z00_9503 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4501z00_14551 = BgL_res3655z00_9503;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1216 */
								BgL_test4501z00_14551 = ((bool_t) 0);
							}
					}
					if (BgL_test4501z00_14551)
						{	/* SawBbv/bbv-types.scm 1217 */
							BgL_globalz00_bglt BgL_arg3308z00_9513;
							BgL_globalz00_bglt BgL_arg3309z00_9514;

							BgL_arg3308z00_9513 =
								(((BgL_rtl_loadfunz00_bglt) COBJECT(
										((BgL_rtl_loadfunz00_bglt) BgL_xz00_8751)))->BgL_varz00);
							BgL_arg3309z00_9514 =
								(((BgL_rtl_loadfunz00_bglt) COBJECT(
										((BgL_rtl_loadfunz00_bglt) BgL_yz00_8752)))->BgL_varz00);
							BgL_tmpz00_14550 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3308z00_9513), ((obj_t) BgL_arg3309z00_9514));
						}
					else
						{	/* SawBbv/bbv-types.scm 1216 */
							BgL_tmpz00_14550 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14550);
			}
		}

	}



/* &bbv-equal?-rtl_loadg1903 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_loadg1903z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8753, obj_t BgL_xz00_8754, obj_t BgL_yz00_8755)
	{
		{	/* SawBbv/bbv-types.scm 1208 */
			{	/* SawBbv/bbv-types.scm 1209 */
				bool_t BgL_tmpz00_14582;

				{	/* SawBbv/bbv-types.scm 1209 */
					bool_t BgL_test4506z00_14583;

					{	/* SawBbv/bbv-types.scm 1209 */
						obj_t BgL_classz00_9516;

						BgL_classz00_9516 = BGl_rtl_loadgz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8755))
							{	/* SawBbv/bbv-types.scm 1209 */
								BgL_objectz00_bglt BgL_arg1807z00_9517;

								BgL_arg1807z00_9517 = (BgL_objectz00_bglt) (BgL_yz00_8755);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1209 */
										long BgL_idxz00_9518;

										BgL_idxz00_9518 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9517);
										BgL_test4506z00_14583 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9518 + 3L)) == BgL_classz00_9516);
									}
								else
									{	/* SawBbv/bbv-types.scm 1209 */
										bool_t BgL_res3654z00_9521;

										{	/* SawBbv/bbv-types.scm 1209 */
											obj_t BgL_oclassz00_9522;

											{	/* SawBbv/bbv-types.scm 1209 */
												obj_t BgL_arg1815z00_9523;
												long BgL_arg1816z00_9524;

												BgL_arg1815z00_9523 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1209 */
													long BgL_arg1817z00_9525;

													BgL_arg1817z00_9525 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9517);
													BgL_arg1816z00_9524 =
														(BgL_arg1817z00_9525 - OBJECT_TYPE);
												}
												BgL_oclassz00_9522 =
													VECTOR_REF(BgL_arg1815z00_9523, BgL_arg1816z00_9524);
											}
											{	/* SawBbv/bbv-types.scm 1209 */
												bool_t BgL__ortest_1115z00_9526;

												BgL__ortest_1115z00_9526 =
													(BgL_classz00_9516 == BgL_oclassz00_9522);
												if (BgL__ortest_1115z00_9526)
													{	/* SawBbv/bbv-types.scm 1209 */
														BgL_res3654z00_9521 = BgL__ortest_1115z00_9526;
													}
												else
													{	/* SawBbv/bbv-types.scm 1209 */
														long BgL_odepthz00_9527;

														{	/* SawBbv/bbv-types.scm 1209 */
															obj_t BgL_arg1804z00_9528;

															BgL_arg1804z00_9528 = (BgL_oclassz00_9522);
															BgL_odepthz00_9527 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9528);
														}
														if ((3L < BgL_odepthz00_9527))
															{	/* SawBbv/bbv-types.scm 1209 */
																obj_t BgL_arg1802z00_9529;

																{	/* SawBbv/bbv-types.scm 1209 */
																	obj_t BgL_arg1803z00_9530;

																	BgL_arg1803z00_9530 = (BgL_oclassz00_9522);
																	BgL_arg1802z00_9529 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9530,
																		3L);
																}
																BgL_res3654z00_9521 =
																	(BgL_arg1802z00_9529 == BgL_classz00_9516);
															}
														else
															{	/* SawBbv/bbv-types.scm 1209 */
																BgL_res3654z00_9521 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4506z00_14583 = BgL_res3654z00_9521;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1209 */
								BgL_test4506z00_14583 = ((bool_t) 0);
							}
					}
					if (BgL_test4506z00_14583)
						{	/* SawBbv/bbv-types.scm 1210 */
							BgL_globalz00_bglt BgL_arg3305z00_9531;
							BgL_globalz00_bglt BgL_arg3306z00_9532;

							BgL_arg3305z00_9531 =
								(((BgL_rtl_loadgz00_bglt) COBJECT(
										((BgL_rtl_loadgz00_bglt) BgL_xz00_8754)))->BgL_varz00);
							BgL_arg3306z00_9532 =
								(((BgL_rtl_loadgz00_bglt) COBJECT(
										((BgL_rtl_loadgz00_bglt) BgL_yz00_8755)))->BgL_varz00);
							BgL_tmpz00_14582 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3305z00_9531), ((obj_t) BgL_arg3306z00_9532));
						}
					else
						{	/* SawBbv/bbv-types.scm 1209 */
							BgL_tmpz00_14582 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14582);
			}
		}

	}



/* &bbv-equal?-rtl_loadi1901 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_loadi1901z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8756, obj_t BgL_xz00_8757, obj_t BgL_yz00_8758)
	{
		{	/* SawBbv/bbv-types.scm 1201 */
			{	/* SawBbv/bbv-types.scm 1202 */
				bool_t BgL_tmpz00_14614;

				{	/* SawBbv/bbv-types.scm 1202 */
					bool_t BgL_test4511z00_14615;

					{	/* SawBbv/bbv-types.scm 1202 */
						obj_t BgL_classz00_9534;

						BgL_classz00_9534 = BGl_rtl_loadiz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8758))
							{	/* SawBbv/bbv-types.scm 1202 */
								BgL_objectz00_bglt BgL_arg1807z00_9535;

								BgL_arg1807z00_9535 = (BgL_objectz00_bglt) (BgL_yz00_8758);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1202 */
										long BgL_idxz00_9536;

										BgL_idxz00_9536 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9535);
										BgL_test4511z00_14615 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9536 + 3L)) == BgL_classz00_9534);
									}
								else
									{	/* SawBbv/bbv-types.scm 1202 */
										bool_t BgL_res3653z00_9539;

										{	/* SawBbv/bbv-types.scm 1202 */
											obj_t BgL_oclassz00_9540;

											{	/* SawBbv/bbv-types.scm 1202 */
												obj_t BgL_arg1815z00_9541;
												long BgL_arg1816z00_9542;

												BgL_arg1815z00_9541 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1202 */
													long BgL_arg1817z00_9543;

													BgL_arg1817z00_9543 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9535);
													BgL_arg1816z00_9542 =
														(BgL_arg1817z00_9543 - OBJECT_TYPE);
												}
												BgL_oclassz00_9540 =
													VECTOR_REF(BgL_arg1815z00_9541, BgL_arg1816z00_9542);
											}
											{	/* SawBbv/bbv-types.scm 1202 */
												bool_t BgL__ortest_1115z00_9544;

												BgL__ortest_1115z00_9544 =
													(BgL_classz00_9534 == BgL_oclassz00_9540);
												if (BgL__ortest_1115z00_9544)
													{	/* SawBbv/bbv-types.scm 1202 */
														BgL_res3653z00_9539 = BgL__ortest_1115z00_9544;
													}
												else
													{	/* SawBbv/bbv-types.scm 1202 */
														long BgL_odepthz00_9545;

														{	/* SawBbv/bbv-types.scm 1202 */
															obj_t BgL_arg1804z00_9546;

															BgL_arg1804z00_9546 = (BgL_oclassz00_9540);
															BgL_odepthz00_9545 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9546);
														}
														if ((3L < BgL_odepthz00_9545))
															{	/* SawBbv/bbv-types.scm 1202 */
																obj_t BgL_arg1802z00_9547;

																{	/* SawBbv/bbv-types.scm 1202 */
																	obj_t BgL_arg1803z00_9548;

																	BgL_arg1803z00_9548 = (BgL_oclassz00_9540);
																	BgL_arg1802z00_9547 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9548,
																		3L);
																}
																BgL_res3653z00_9539 =
																	(BgL_arg1802z00_9547 == BgL_classz00_9534);
															}
														else
															{	/* SawBbv/bbv-types.scm 1202 */
																BgL_res3653z00_9539 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4511z00_14615 = BgL_res3653z00_9539;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1202 */
								BgL_test4511z00_14615 = ((bool_t) 0);
							}
					}
					if (BgL_test4511z00_14615)
						{	/* SawBbv/bbv-types.scm 1203 */
							BgL_atomz00_bglt BgL_arg3301z00_9549;
							BgL_atomz00_bglt BgL_arg3302z00_9550;

							BgL_arg3301z00_9549 =
								(((BgL_rtl_loadiz00_bglt) COBJECT(
										((BgL_rtl_loadiz00_bglt) BgL_xz00_8757)))->BgL_constantz00);
							BgL_arg3302z00_9550 =
								(((BgL_rtl_loadiz00_bglt) COBJECT(
										((BgL_rtl_loadiz00_bglt) BgL_yz00_8758)))->BgL_constantz00);
							BgL_tmpz00_14614 =
								BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3301z00_9549), ((obj_t) BgL_arg3302z00_9550));
						}
					else
						{	/* SawBbv/bbv-types.scm 1202 */
							BgL_tmpz00_14614 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14614);
			}
		}

	}



/* &bbv-equal?-rtl_switc1899 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_switc1899z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8759, obj_t BgL_xz00_8760, obj_t BgL_yz00_8761)
	{
		{	/* SawBbv/bbv-types.scm 1193 */
			{	/* SawBbv/bbv-types.scm 1194 */
				bool_t BgL_test4516z00_14646;

				{	/* SawBbv/bbv-types.scm 1194 */
					obj_t BgL_classz00_9552;

					BgL_classz00_9552 = BGl_rtl_switchz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_yz00_8761))
						{	/* SawBbv/bbv-types.scm 1194 */
							BgL_objectz00_bglt BgL_arg1807z00_9553;

							BgL_arg1807z00_9553 = (BgL_objectz00_bglt) (BgL_yz00_8761);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 1194 */
									long BgL_idxz00_9554;

									BgL_idxz00_9554 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9553);
									BgL_test4516z00_14646 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9554 + 4L)) == BgL_classz00_9552);
								}
							else
								{	/* SawBbv/bbv-types.scm 1194 */
									bool_t BgL_res3652z00_9557;

									{	/* SawBbv/bbv-types.scm 1194 */
										obj_t BgL_oclassz00_9558;

										{	/* SawBbv/bbv-types.scm 1194 */
											obj_t BgL_arg1815z00_9559;
											long BgL_arg1816z00_9560;

											BgL_arg1815z00_9559 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 1194 */
												long BgL_arg1817z00_9561;

												BgL_arg1817z00_9561 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9553);
												BgL_arg1816z00_9560 =
													(BgL_arg1817z00_9561 - OBJECT_TYPE);
											}
											BgL_oclassz00_9558 =
												VECTOR_REF(BgL_arg1815z00_9559, BgL_arg1816z00_9560);
										}
										{	/* SawBbv/bbv-types.scm 1194 */
											bool_t BgL__ortest_1115z00_9562;

											BgL__ortest_1115z00_9562 =
												(BgL_classz00_9552 == BgL_oclassz00_9558);
											if (BgL__ortest_1115z00_9562)
												{	/* SawBbv/bbv-types.scm 1194 */
													BgL_res3652z00_9557 = BgL__ortest_1115z00_9562;
												}
											else
												{	/* SawBbv/bbv-types.scm 1194 */
													long BgL_odepthz00_9563;

													{	/* SawBbv/bbv-types.scm 1194 */
														obj_t BgL_arg1804z00_9564;

														BgL_arg1804z00_9564 = (BgL_oclassz00_9558);
														BgL_odepthz00_9563 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9564);
													}
													if ((4L < BgL_odepthz00_9563))
														{	/* SawBbv/bbv-types.scm 1194 */
															obj_t BgL_arg1802z00_9565;

															{	/* SawBbv/bbv-types.scm 1194 */
																obj_t BgL_arg1803z00_9566;

																BgL_arg1803z00_9566 = (BgL_oclassz00_9558);
																BgL_arg1802z00_9565 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9566,
																	4L);
															}
															BgL_res3652z00_9557 =
																(BgL_arg1802z00_9565 == BgL_classz00_9552);
														}
													else
														{	/* SawBbv/bbv-types.scm 1194 */
															BgL_res3652z00_9557 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4516z00_14646 = BgL_res3652z00_9557;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1194 */
							BgL_test4516z00_14646 = ((bool_t) 0);
						}
				}
				if (BgL_test4516z00_14646)
					{	/* SawBbv/bbv-types.scm 1194 */
						if (
							(bgl_list_length(
									(((BgL_rtl_switchz00_bglt) COBJECT(
												((BgL_rtl_switchz00_bglt) BgL_xz00_8760)))->
										BgL_labelsz00)) ==
								bgl_list_length((((BgL_rtl_switchz00_bglt)
											COBJECT(((BgL_rtl_switchz00_bglt) BgL_yz00_8761)))->
										BgL_labelsz00))))
							{	/* SawBbv/bbv-types.scm 1196 */
								obj_t BgL_arg3287z00_9567;
								obj_t BgL_arg3288z00_9568;

								BgL_arg3287z00_9567 =
									(((BgL_rtl_switchz00_bglt) COBJECT(
											((BgL_rtl_switchz00_bglt) BgL_xz00_8760)))->
									BgL_labelsz00);
								BgL_arg3288z00_9568 =
									(((BgL_rtl_switchz00_bglt) COBJECT(((BgL_rtl_switchz00_bglt)
												BgL_yz00_8761)))->BgL_labelsz00);
								{	/* SawBbv/bbv-types.scm 1196 */
									obj_t BgL_list3289z00_9569;

									{	/* SawBbv/bbv-types.scm 1196 */
										obj_t BgL_arg3290z00_9570;

										BgL_arg3290z00_9570 =
											MAKE_YOUNG_PAIR(BgL_arg3288z00_9568, BNIL);
										BgL_list3289z00_9569 =
											MAKE_YOUNG_PAIR(BgL_arg3287z00_9567, BgL_arg3290z00_9570);
									}
									return
										BGl_everyz00zz__r4_pairs_and_lists_6_3z00
										(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
										BgL_list3289z00_9569);
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 1195 */
								return BFALSE;
							}
					}
				else
					{	/* SawBbv/bbv-types.scm 1194 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-equal?-rtl_selec1897 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_selec1897z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8762, obj_t BgL_xz00_8763, obj_t BgL_yz00_8764)
	{
		{	/* SawBbv/bbv-types.scm 1184 */
			{	/* SawBbv/bbv-types.scm 1185 */
				bool_t BgL_test4522z00_14684;

				{	/* SawBbv/bbv-types.scm 1185 */
					obj_t BgL_classz00_9572;

					BgL_classz00_9572 = BGl_rtl_selectz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_yz00_8764))
						{	/* SawBbv/bbv-types.scm 1185 */
							BgL_objectz00_bglt BgL_arg1807z00_9573;

							BgL_arg1807z00_9573 = (BgL_objectz00_bglt) (BgL_yz00_8764);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 1185 */
									long BgL_idxz00_9574;

									BgL_idxz00_9574 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9573);
									BgL_test4522z00_14684 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9574 + 3L)) == BgL_classz00_9572);
								}
							else
								{	/* SawBbv/bbv-types.scm 1185 */
									bool_t BgL_res3651z00_9577;

									{	/* SawBbv/bbv-types.scm 1185 */
										obj_t BgL_oclassz00_9578;

										{	/* SawBbv/bbv-types.scm 1185 */
											obj_t BgL_arg1815z00_9579;
											long BgL_arg1816z00_9580;

											BgL_arg1815z00_9579 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 1185 */
												long BgL_arg1817z00_9581;

												BgL_arg1817z00_9581 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9573);
												BgL_arg1816z00_9580 =
													(BgL_arg1817z00_9581 - OBJECT_TYPE);
											}
											BgL_oclassz00_9578 =
												VECTOR_REF(BgL_arg1815z00_9579, BgL_arg1816z00_9580);
										}
										{	/* SawBbv/bbv-types.scm 1185 */
											bool_t BgL__ortest_1115z00_9582;

											BgL__ortest_1115z00_9582 =
												(BgL_classz00_9572 == BgL_oclassz00_9578);
											if (BgL__ortest_1115z00_9582)
												{	/* SawBbv/bbv-types.scm 1185 */
													BgL_res3651z00_9577 = BgL__ortest_1115z00_9582;
												}
											else
												{	/* SawBbv/bbv-types.scm 1185 */
													long BgL_odepthz00_9583;

													{	/* SawBbv/bbv-types.scm 1185 */
														obj_t BgL_arg1804z00_9584;

														BgL_arg1804z00_9584 = (BgL_oclassz00_9578);
														BgL_odepthz00_9583 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9584);
													}
													if ((3L < BgL_odepthz00_9583))
														{	/* SawBbv/bbv-types.scm 1185 */
															obj_t BgL_arg1802z00_9585;

															{	/* SawBbv/bbv-types.scm 1185 */
																obj_t BgL_arg1803z00_9586;

																BgL_arg1803z00_9586 = (BgL_oclassz00_9578);
																BgL_arg1802z00_9585 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9586,
																	3L);
															}
															BgL_res3651z00_9577 =
																(BgL_arg1802z00_9585 == BgL_classz00_9572);
														}
													else
														{	/* SawBbv/bbv-types.scm 1185 */
															BgL_res3651z00_9577 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4522z00_14684 = BgL_res3651z00_9577;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1185 */
							BgL_test4522z00_14684 = ((bool_t) 0);
						}
				}
				if (BgL_test4522z00_14684)
					{	/* SawBbv/bbv-types.scm 1185 */
						if (
							(((obj_t)
									(((BgL_rtl_selectz00_bglt) COBJECT(
												((BgL_rtl_selectz00_bglt) BgL_xz00_8763)))->
										BgL_typez00)) ==
								((obj_t) (((BgL_rtl_selectz00_bglt)
											COBJECT(((BgL_rtl_selectz00_bglt) BgL_xz00_8763)))->
										BgL_typez00))))
							{	/* SawBbv/bbv-types.scm 1186 */
								if (
									(bgl_list_length(
											(((BgL_rtl_selectz00_bglt) COBJECT(
														((BgL_rtl_selectz00_bglt) BgL_xz00_8763)))->
												BgL_patternsz00)) ==
										bgl_list_length((((BgL_rtl_selectz00_bglt)
													COBJECT(((BgL_rtl_selectz00_bglt) BgL_yz00_8764)))->
												BgL_patternsz00))))
									{	/* SawBbv/bbv-types.scm 1188 */
										obj_t BgL_arg3268z00_9587;
										obj_t BgL_arg3269z00_9588;

										BgL_arg3268z00_9587 =
											(((BgL_rtl_selectz00_bglt) COBJECT(
													((BgL_rtl_selectz00_bglt) BgL_xz00_8763)))->
											BgL_patternsz00);
										BgL_arg3269z00_9588 =
											(((BgL_rtl_selectz00_bglt)
												COBJECT(((BgL_rtl_selectz00_bglt) BgL_yz00_8764)))->
											BgL_patternsz00);
										{	/* SawBbv/bbv-types.scm 1188 */
											obj_t BgL_list3270z00_9589;

											{	/* SawBbv/bbv-types.scm 1188 */
												obj_t BgL_arg3271z00_9590;

												BgL_arg3271z00_9590 =
													MAKE_YOUNG_PAIR(BgL_arg3269z00_9588, BNIL);
												BgL_list3270z00_9589 =
													MAKE_YOUNG_PAIR(BgL_arg3268z00_9587,
													BgL_arg3271z00_9590);
											}
											return
												BGl_everyz00zz__r4_pairs_and_lists_6_3z00
												(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
												BgL_list3270z00_9589);
										}
									}
								else
									{	/* SawBbv/bbv-types.scm 1187 */
										return BFALSE;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1186 */
								return BFALSE;
							}
					}
				else
					{	/* SawBbv/bbv-types.scm 1185 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-equal?-rtl_fun1895 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_fun1895z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8765, obj_t BgL_xz00_8766, obj_t BgL_yz00_8767)
	{
		{	/* SawBbv/bbv-types.scm 1178 */
			{	/* SawBbv/bbv-types.scm 1179 */
				bool_t BgL_tmpz00_14730;

				{	/* SawBbv/bbv-types.scm 1179 */
					obj_t BgL_arg3264z00_9592;
					obj_t BgL_arg3266z00_9593;

					{	/* SawBbv/bbv-types.scm 1179 */
						obj_t BgL_arg1815z00_9594;
						long BgL_arg1816z00_9595;

						BgL_arg1815z00_9594 = (BGl_za2classesza2z00zz__objectz00);
						{	/* SawBbv/bbv-types.scm 1179 */
							long BgL_arg1817z00_9596;

							BgL_arg1817z00_9596 =
								BGL_OBJECT_CLASS_NUM(
								((BgL_objectz00_bglt) ((BgL_rtl_funz00_bglt) BgL_xz00_8766)));
							BgL_arg1816z00_9595 = (BgL_arg1817z00_9596 - OBJECT_TYPE);
						}
						BgL_arg3264z00_9592 =
							VECTOR_REF(BgL_arg1815z00_9594, BgL_arg1816z00_9595);
					}
					{	/* SawBbv/bbv-types.scm 1179 */
						obj_t BgL_arg1815z00_9597;
						long BgL_arg1816z00_9598;

						BgL_arg1815z00_9597 = (BGl_za2classesza2z00zz__objectz00);
						{	/* SawBbv/bbv-types.scm 1179 */
							long BgL_arg1817z00_9599;

							BgL_arg1817z00_9599 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_yz00_8767));
							BgL_arg1816z00_9598 = (BgL_arg1817z00_9599 - OBJECT_TYPE);
						}
						BgL_arg3266z00_9593 =
							VECTOR_REF(BgL_arg1815z00_9597, BgL_arg1816z00_9598);
					}
					BgL_tmpz00_14730 = (BgL_arg3264z00_9592 == BgL_arg3266z00_9593);
				}
				return BBOOL(BgL_tmpz00_14730);
			}
		}

	}



/* &bbv-equal?-rtl_reg1893 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_reg1893z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8768, obj_t BgL_xz00_8769, obj_t BgL_yz00_8770)
	{
		{	/* SawBbv/bbv-types.scm 1169 */
			{	/* SawBbv/bbv-types.scm 1170 */
				bool_t BgL_tmpz00_14744;

				{	/* SawBbv/bbv-types.scm 1170 */
					bool_t BgL_test4529z00_14745;

					{	/* SawBbv/bbv-types.scm 1170 */
						obj_t BgL_classz00_9601;

						BgL_classz00_9601 = BGl_rtl_regz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_yz00_8770))
							{	/* SawBbv/bbv-types.scm 1170 */
								BgL_objectz00_bglt BgL_arg1807z00_9602;

								BgL_arg1807z00_9602 = (BgL_objectz00_bglt) (BgL_yz00_8770);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1170 */
										long BgL_idxz00_9603;

										BgL_idxz00_9603 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9602);
										BgL_test4529z00_14745 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9603 + 1L)) == BgL_classz00_9601);
									}
								else
									{	/* SawBbv/bbv-types.scm 1170 */
										bool_t BgL_res3650z00_9606;

										{	/* SawBbv/bbv-types.scm 1170 */
											obj_t BgL_oclassz00_9607;

											{	/* SawBbv/bbv-types.scm 1170 */
												obj_t BgL_arg1815z00_9608;
												long BgL_arg1816z00_9609;

												BgL_arg1815z00_9608 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1170 */
													long BgL_arg1817z00_9610;

													BgL_arg1817z00_9610 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9602);
													BgL_arg1816z00_9609 =
														(BgL_arg1817z00_9610 - OBJECT_TYPE);
												}
												BgL_oclassz00_9607 =
													VECTOR_REF(BgL_arg1815z00_9608, BgL_arg1816z00_9609);
											}
											{	/* SawBbv/bbv-types.scm 1170 */
												bool_t BgL__ortest_1115z00_9611;

												BgL__ortest_1115z00_9611 =
													(BgL_classz00_9601 == BgL_oclassz00_9607);
												if (BgL__ortest_1115z00_9611)
													{	/* SawBbv/bbv-types.scm 1170 */
														BgL_res3650z00_9606 = BgL__ortest_1115z00_9611;
													}
												else
													{	/* SawBbv/bbv-types.scm 1170 */
														long BgL_odepthz00_9612;

														{	/* SawBbv/bbv-types.scm 1170 */
															obj_t BgL_arg1804z00_9613;

															BgL_arg1804z00_9613 = (BgL_oclassz00_9607);
															BgL_odepthz00_9612 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9613);
														}
														if ((1L < BgL_odepthz00_9612))
															{	/* SawBbv/bbv-types.scm 1170 */
																obj_t BgL_arg1802z00_9614;

																{	/* SawBbv/bbv-types.scm 1170 */
																	obj_t BgL_arg1803z00_9615;

																	BgL_arg1803z00_9615 = (BgL_oclassz00_9607);
																	BgL_arg1802z00_9614 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9615,
																		1L);
																}
																BgL_res3650z00_9606 =
																	(BgL_arg1802z00_9614 == BgL_classz00_9601);
															}
														else
															{	/* SawBbv/bbv-types.scm 1170 */
																BgL_res3650z00_9606 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4529z00_14745 = BgL_res3650z00_9606;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1170 */
								BgL_test4529z00_14745 = ((bool_t) 0);
							}
					}
					if (BgL_test4529z00_14745)
						{	/* SawBbv/bbv-types.scm 1171 */
							bool_t BgL_test4534z00_14768;

							{	/* SawBbv/bbv-types.scm 1171 */
								BgL_typez00_bglt BgL_arg3259z00_9616;
								BgL_typez00_bglt BgL_arg3263z00_9617;

								BgL_arg3259z00_9616 =
									(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt) BgL_xz00_8769)))->BgL_typez00);
								BgL_arg3263z00_9617 =
									(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt) BgL_yz00_8770)))->BgL_typez00);
								BgL_test4534z00_14768 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(
									((obj_t) BgL_arg3259z00_9616), ((obj_t) BgL_arg3263z00_9617));
							}
							if (BgL_test4534z00_14768)
								{	/* SawBbv/bbv-types.scm 1172 */
									bool_t BgL_test4535z00_14776;

									{	/* SawBbv/bbv-types.scm 1172 */
										obj_t BgL_arg3255z00_9618;
										obj_t BgL_arg3256z00_9619;

										BgL_arg3255z00_9618 =
											(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_xz00_8769)))->BgL_varz00);
										BgL_arg3256z00_9619 =
											(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_yz00_8770)))->BgL_varz00);
										BgL_test4535z00_14776 =
											BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2
											(BgL_arg3255z00_9618, BgL_arg3256z00_9619);
									}
									if (BgL_test4535z00_14776)
										{	/* SawBbv/bbv-types.scm 1173 */
											obj_t BgL_arg3252z00_9620;
											obj_t BgL_arg3254z00_9621;

											BgL_arg3252z00_9620 =
												(((BgL_rtl_regz00_bglt) COBJECT(
														((BgL_rtl_regz00_bglt) BgL_xz00_8769)))->
												BgL_namez00);
											BgL_arg3254z00_9621 =
												(((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
															BgL_yz00_8770)))->BgL_namez00);
											BgL_tmpz00_14744 =
												BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2
												(BgL_arg3252z00_9620, BgL_arg3254z00_9621);
										}
									else
										{	/* SawBbv/bbv-types.scm 1172 */
											BgL_tmpz00_14744 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 1171 */
									BgL_tmpz00_14744 = ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1170 */
							BgL_tmpz00_14744 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14744);
			}
		}

	}



/* &bbv-equal?-rtl_ins/b1891 */
	obj_t BGl_z62bbvzd2equalzf3zd2rtl_inszf2b1891z63zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8771, obj_t BgL_xz00_8772, obj_t BgL_yz00_8773)
	{
		{	/* SawBbv/bbv-types.scm 1159 */
			{	/* SawBbv/bbv-types.scm 1160 */
				bool_t BgL_tmpz00_14788;

				{	/* SawBbv/bbv-types.scm 1160 */
					bool_t BgL_test4536z00_14789;

					{	/* SawBbv/bbv-types.scm 1160 */
						obj_t BgL_classz00_9623;

						BgL_classz00_9623 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
						if (BGL_OBJECTP(BgL_yz00_8773))
							{	/* SawBbv/bbv-types.scm 1160 */
								BgL_objectz00_bglt BgL_arg1807z00_9624;

								BgL_arg1807z00_9624 = (BgL_objectz00_bglt) (BgL_yz00_8773);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1160 */
										long BgL_idxz00_9625;

										BgL_idxz00_9625 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9624);
										BgL_test4536z00_14789 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9625 + 2L)) == BgL_classz00_9623);
									}
								else
									{	/* SawBbv/bbv-types.scm 1160 */
										bool_t BgL_res3649z00_9628;

										{	/* SawBbv/bbv-types.scm 1160 */
											obj_t BgL_oclassz00_9629;

											{	/* SawBbv/bbv-types.scm 1160 */
												obj_t BgL_arg1815z00_9630;
												long BgL_arg1816z00_9631;

												BgL_arg1815z00_9630 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1160 */
													long BgL_arg1817z00_9632;

													BgL_arg1817z00_9632 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9624);
													BgL_arg1816z00_9631 =
														(BgL_arg1817z00_9632 - OBJECT_TYPE);
												}
												BgL_oclassz00_9629 =
													VECTOR_REF(BgL_arg1815z00_9630, BgL_arg1816z00_9631);
											}
											{	/* SawBbv/bbv-types.scm 1160 */
												bool_t BgL__ortest_1115z00_9633;

												BgL__ortest_1115z00_9633 =
													(BgL_classz00_9623 == BgL_oclassz00_9629);
												if (BgL__ortest_1115z00_9633)
													{	/* SawBbv/bbv-types.scm 1160 */
														BgL_res3649z00_9628 = BgL__ortest_1115z00_9633;
													}
												else
													{	/* SawBbv/bbv-types.scm 1160 */
														long BgL_odepthz00_9634;

														{	/* SawBbv/bbv-types.scm 1160 */
															obj_t BgL_arg1804z00_9635;

															BgL_arg1804z00_9635 = (BgL_oclassz00_9629);
															BgL_odepthz00_9634 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9635);
														}
														if ((2L < BgL_odepthz00_9634))
															{	/* SawBbv/bbv-types.scm 1160 */
																obj_t BgL_arg1802z00_9636;

																{	/* SawBbv/bbv-types.scm 1160 */
																	obj_t BgL_arg1803z00_9637;

																	BgL_arg1803z00_9637 = (BgL_oclassz00_9629);
																	BgL_arg1802z00_9636 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9637,
																		2L);
																}
																BgL_res3649z00_9628 =
																	(BgL_arg1802z00_9636 == BgL_classz00_9623);
															}
														else
															{	/* SawBbv/bbv-types.scm 1160 */
																BgL_res3649z00_9628 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4536z00_14789 = BgL_res3649z00_9628;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1160 */
								BgL_test4536z00_14789 = ((bool_t) 0);
							}
					}
					if (BgL_test4536z00_14789)
						{	/* SawBbv/bbv-types.scm 1161 */
							bool_t BgL_test4541z00_14812;

							{	/* SawBbv/bbv-types.scm 1161 */
								obj_t BgL_arg3250z00_9638;
								obj_t BgL_arg3251z00_9639;

								BgL_arg3250z00_9638 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt)
												((BgL_rtl_insz00_bglt) BgL_xz00_8772))))->BgL_destz00);
								BgL_arg3251z00_9639 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_yz00_8773)))->BgL_destz00);
								BgL_test4541z00_14812 =
									BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(BgL_arg3250z00_9638,
									BgL_arg3251z00_9639);
							}
							if (BgL_test4541z00_14812)
								{	/* SawBbv/bbv-types.scm 1161 */
									if (
										(bgl_list_length(
												(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_xz00_8772))))->
													BgL_argsz00)) ==
											bgl_list_length((((BgL_rtl_insz00_bglt)
														COBJECT(((BgL_rtl_insz00_bglt) BgL_yz00_8773)))->
													BgL_argsz00))))
										{	/* SawBbv/bbv-types.scm 1163 */
											obj_t BgL__andtest_1399z00_9640;

											{	/* SawBbv/bbv-types.scm 1163 */
												obj_t BgL_arg3239z00_9641;
												obj_t BgL_arg3240z00_9642;

												BgL_arg3239z00_9641 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_xz00_8772))))->
													BgL_argsz00);
												BgL_arg3240z00_9642 =
													(((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
																BgL_yz00_8773)))->BgL_argsz00);
												{	/* SawBbv/bbv-types.scm 1163 */
													obj_t BgL_list3241z00_9643;

													{	/* SawBbv/bbv-types.scm 1163 */
														obj_t BgL_arg3242z00_9644;

														BgL_arg3242z00_9644 =
															MAKE_YOUNG_PAIR(BgL_arg3240z00_9642, BNIL);
														BgL_list3241z00_9643 =
															MAKE_YOUNG_PAIR(BgL_arg3239z00_9641,
															BgL_arg3242z00_9644);
													}
													BgL__andtest_1399z00_9640 =
														BGl_everyz00zz__r4_pairs_and_lists_6_3z00
														(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
														BgL_list3241z00_9643);
												}
											}
											if (CBOOL(BgL__andtest_1399z00_9640))
												{	/* SawBbv/bbv-types.scm 1164 */
													BgL_rtl_funz00_bglt BgL_arg3236z00_9645;
													BgL_rtl_funz00_bglt BgL_arg3237z00_9646;

													BgL_arg3236z00_9645 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_xz00_8772))))->
														BgL_funz00);
													BgL_arg3237z00_9646 =
														(((BgL_rtl_insz00_bglt)
															COBJECT(((BgL_rtl_insz00_bglt) BgL_yz00_8773)))->
														BgL_funz00);
													BgL_tmpz00_14788 =
														BGl_bbvzd2equalzf3z21zzsaw_bbvzd2typeszd2(((obj_t)
															BgL_arg3236z00_9645),
														((obj_t) BgL_arg3237z00_9646));
												}
											else
												{	/* SawBbv/bbv-types.scm 1163 */
													BgL_tmpz00_14788 = ((bool_t) 0);
												}
										}
									else
										{	/* SawBbv/bbv-types.scm 1162 */
											BgL_tmpz00_14788 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-types.scm 1161 */
									BgL_tmpz00_14788 = ((bool_t) 0);
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1160 */
							BgL_tmpz00_14788 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14788);
			}
		}

	}



/* &bbv-equal?-blockS1889 */
	obj_t BGl_z62bbvzd2equalzf3zd2blockS1889z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8774, obj_t BgL_xz00_8775, obj_t BgL_yz00_8776)
	{
		{	/* SawBbv/bbv-types.scm 1151 */
			{	/* SawBbv/bbv-types.scm 1152 */
				bool_t BgL_test4544z00_14847;

				{	/* SawBbv/bbv-types.scm 1152 */
					obj_t BgL_classz00_9648;

					BgL_classz00_9648 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
					if (BGL_OBJECTP(BgL_yz00_8776))
						{	/* SawBbv/bbv-types.scm 1152 */
							BgL_objectz00_bglt BgL_arg1807z00_9649;

							BgL_arg1807z00_9649 = (BgL_objectz00_bglt) (BgL_yz00_8776);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 1152 */
									long BgL_idxz00_9650;

									BgL_idxz00_9650 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9649);
									BgL_test4544z00_14847 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9650 + 2L)) == BgL_classz00_9648);
								}
							else
								{	/* SawBbv/bbv-types.scm 1152 */
									bool_t BgL_res3648z00_9653;

									{	/* SawBbv/bbv-types.scm 1152 */
										obj_t BgL_oclassz00_9654;

										{	/* SawBbv/bbv-types.scm 1152 */
											obj_t BgL_arg1815z00_9655;
											long BgL_arg1816z00_9656;

											BgL_arg1815z00_9655 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 1152 */
												long BgL_arg1817z00_9657;

												BgL_arg1817z00_9657 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9649);
												BgL_arg1816z00_9656 =
													(BgL_arg1817z00_9657 - OBJECT_TYPE);
											}
											BgL_oclassz00_9654 =
												VECTOR_REF(BgL_arg1815z00_9655, BgL_arg1816z00_9656);
										}
										{	/* SawBbv/bbv-types.scm 1152 */
											bool_t BgL__ortest_1115z00_9658;

											BgL__ortest_1115z00_9658 =
												(BgL_classz00_9648 == BgL_oclassz00_9654);
											if (BgL__ortest_1115z00_9658)
												{	/* SawBbv/bbv-types.scm 1152 */
													BgL_res3648z00_9653 = BgL__ortest_1115z00_9658;
												}
											else
												{	/* SawBbv/bbv-types.scm 1152 */
													long BgL_odepthz00_9659;

													{	/* SawBbv/bbv-types.scm 1152 */
														obj_t BgL_arg1804z00_9660;

														BgL_arg1804z00_9660 = (BgL_oclassz00_9654);
														BgL_odepthz00_9659 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9660);
													}
													if ((2L < BgL_odepthz00_9659))
														{	/* SawBbv/bbv-types.scm 1152 */
															obj_t BgL_arg1802z00_9661;

															{	/* SawBbv/bbv-types.scm 1152 */
																obj_t BgL_arg1803z00_9662;

																BgL_arg1803z00_9662 = (BgL_oclassz00_9654);
																BgL_arg1802z00_9661 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9662,
																	2L);
															}
															BgL_res3648z00_9653 =
																(BgL_arg1802z00_9661 == BgL_classz00_9648);
														}
													else
														{	/* SawBbv/bbv-types.scm 1152 */
															BgL_res3648z00_9653 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4544z00_14847 = BgL_res3648z00_9653;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 1152 */
							BgL_test4544z00_14847 = ((bool_t) 0);
						}
				}
				if (BgL_test4544z00_14847)
					{	/* SawBbv/bbv-types.scm 1152 */
						if (
							(bgl_list_length(
									(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt)
													((BgL_blockz00_bglt) BgL_xz00_8775))))->
										BgL_firstz00)) ==
								bgl_list_length((((BgL_blockz00_bglt)
											COBJECT(((BgL_blockz00_bglt) BgL_yz00_8776)))->
										BgL_firstz00))))
							{	/* SawBbv/bbv-types.scm 1154 */
								obj_t BgL_arg3223z00_9663;
								obj_t BgL_arg3226z00_9664;

								BgL_arg3223z00_9663 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_xz00_8775))))->BgL_firstz00);
								BgL_arg3226z00_9664 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_yz00_8776)))->BgL_firstz00);
								{	/* SawBbv/bbv-types.scm 1154 */
									obj_t BgL_list3227z00_9665;

									{	/* SawBbv/bbv-types.scm 1154 */
										obj_t BgL_arg3229z00_9666;

										BgL_arg3229z00_9666 =
											MAKE_YOUNG_PAIR(BgL_arg3226z00_9664, BNIL);
										BgL_list3227z00_9665 =
											MAKE_YOUNG_PAIR(BgL_arg3223z00_9663, BgL_arg3229z00_9666);
									}
									return
										BGl_everyz00zz__r4_pairs_and_lists_6_3z00
										(BGl_bbvzd2equalzf3zd2envzf3zzsaw_bbvzd2typeszd2,
										BgL_list3227z00_9665);
								}
							}
						else
							{	/* SawBbv/bbv-types.scm 1153 */
								return BFALSE;
							}
					}
				else
					{	/* SawBbv/bbv-types.scm 1152 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-equal?-atom1887 */
	obj_t BGl_z62bbvzd2equalzf3zd2atom1887z91zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8777, obj_t BgL_xz00_8778, obj_t BgL_yz00_8779)
	{
		{	/* SawBbv/bbv-types.scm 1144 */
			{	/* SawBbv/bbv-types.scm 1145 */
				bool_t BgL_tmpz00_14887;

				{	/* SawBbv/bbv-types.scm 1145 */
					bool_t BgL_test4550z00_14888;

					{	/* SawBbv/bbv-types.scm 1145 */
						obj_t BgL_classz00_9668;

						BgL_classz00_9668 = BGl_atomz00zzast_nodez00;
						if (BGL_OBJECTP(BgL_yz00_8779))
							{	/* SawBbv/bbv-types.scm 1145 */
								BgL_objectz00_bglt BgL_arg1807z00_9669;

								BgL_arg1807z00_9669 = (BgL_objectz00_bglt) (BgL_yz00_8779);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-types.scm 1145 */
										long BgL_idxz00_9670;

										BgL_idxz00_9670 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9669);
										BgL_test4550z00_14888 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9670 + 2L)) == BgL_classz00_9668);
									}
								else
									{	/* SawBbv/bbv-types.scm 1145 */
										bool_t BgL_res3647z00_9673;

										{	/* SawBbv/bbv-types.scm 1145 */
											obj_t BgL_oclassz00_9674;

											{	/* SawBbv/bbv-types.scm 1145 */
												obj_t BgL_arg1815z00_9675;
												long BgL_arg1816z00_9676;

												BgL_arg1815z00_9675 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-types.scm 1145 */
													long BgL_arg1817z00_9677;

													BgL_arg1817z00_9677 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9669);
													BgL_arg1816z00_9676 =
														(BgL_arg1817z00_9677 - OBJECT_TYPE);
												}
												BgL_oclassz00_9674 =
													VECTOR_REF(BgL_arg1815z00_9675, BgL_arg1816z00_9676);
											}
											{	/* SawBbv/bbv-types.scm 1145 */
												bool_t BgL__ortest_1115z00_9678;

												BgL__ortest_1115z00_9678 =
													(BgL_classz00_9668 == BgL_oclassz00_9674);
												if (BgL__ortest_1115z00_9678)
													{	/* SawBbv/bbv-types.scm 1145 */
														BgL_res3647z00_9673 = BgL__ortest_1115z00_9678;
													}
												else
													{	/* SawBbv/bbv-types.scm 1145 */
														long BgL_odepthz00_9679;

														{	/* SawBbv/bbv-types.scm 1145 */
															obj_t BgL_arg1804z00_9680;

															BgL_arg1804z00_9680 = (BgL_oclassz00_9674);
															BgL_odepthz00_9679 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9680);
														}
														if ((2L < BgL_odepthz00_9679))
															{	/* SawBbv/bbv-types.scm 1145 */
																obj_t BgL_arg1802z00_9681;

																{	/* SawBbv/bbv-types.scm 1145 */
																	obj_t BgL_arg1803z00_9682;

																	BgL_arg1803z00_9682 = (BgL_oclassz00_9674);
																	BgL_arg1802z00_9681 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9682,
																		2L);
																}
																BgL_res3647z00_9673 =
																	(BgL_arg1802z00_9681 == BgL_classz00_9668);
															}
														else
															{	/* SawBbv/bbv-types.scm 1145 */
																BgL_res3647z00_9673 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test4550z00_14888 = BgL_res3647z00_9673;
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 1145 */
								BgL_test4550z00_14888 = ((bool_t) 0);
							}
					}
					if (BgL_test4550z00_14888)
						{	/* SawBbv/bbv-types.scm 1145 */
							BgL_tmpz00_14887 =
								BGl_equalzf3zf3zz__r4_equivalence_6_2z00(
								(((BgL_atomz00_bglt) COBJECT(
											((BgL_atomz00_bglt) BgL_xz00_8778)))->BgL_valuez00),
								(((BgL_atomz00_bglt) COBJECT(
											((BgL_atomz00_bglt) BgL_yz00_8779)))->BgL_valuez00));
						}
					else
						{	/* SawBbv/bbv-types.scm 1145 */
							BgL_tmpz00_14887 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14887);
			}
		}

	}



/* &bbv-hash-rtl_cast_nu1883 */
	obj_t BGl_z62bbvzd2hashzd2rtl_cast_nu1883z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8780, obj_t BgL_oz00_8781)
	{
		{	/* SawBbv/bbv-types.scm 1132 */
			{

				{	/* SawBbv/bbv-types.scm 1133 */
					obj_t BgL_arg3204z00_9686;
					obj_t BgL_arg3205z00_9687;

					{	/* SawBbv/bbv-types.scm 1132 */
						obj_t BgL_nextzd2method1882zd2_9685;

						BgL_nextzd2method1882zd2_9685 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_cast_nullz00_bglt) BgL_oz00_8781)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_cast_nullz00zzsaw_defsz00);
						BgL_arg3204z00_9686 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1882zd2_9685,
							((obj_t) ((BgL_rtl_cast_nullz00_bglt) BgL_oz00_8781)));
					}
					{	/* SawBbv/bbv-types.scm 1133 */
						BgL_typez00_bglt BgL_arg3207z00_9688;

						BgL_arg3207z00_9688 =
							(((BgL_rtl_cast_nullz00_bglt) COBJECT(
									((BgL_rtl_cast_nullz00_bglt) BgL_oz00_8781)))->BgL_typez00);
						BgL_arg3205z00_9687 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3207z00_9688));
					}
					return
						BINT(
						((long) CINT(BgL_arg3204z00_9686) ^
							(long) CINT(BgL_arg3205z00_9687)));
		}}}

	}



/* &bbv-hash-rtl_cast1881 */
	obj_t BGl_z62bbvzd2hashzd2rtl_cast1881z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8782, obj_t BgL_oz00_8783)
	{
		{	/* SawBbv/bbv-types.scm 1125 */
			{

				{	/* SawBbv/bbv-types.scm 1126 */
					obj_t BgL_arg3192z00_9692;
					long BgL_arg3195z00_9693;

					{	/* SawBbv/bbv-types.scm 1125 */
						obj_t BgL_nextzd2method1880zd2_9691;

						BgL_nextzd2method1880zd2_9691 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_castz00_bglt) BgL_oz00_8783)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_castz00zzsaw_defsz00);
						BgL_arg3192z00_9692 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1880zd2_9691,
							((obj_t) ((BgL_rtl_castz00_bglt) BgL_oz00_8783)));
					}
					{	/* SawBbv/bbv-types.scm 1127 */
						obj_t BgL_arg3199z00_9694;
						obj_t BgL_arg3200z00_9695;

						{	/* SawBbv/bbv-types.scm 1127 */
							BgL_typez00_bglt BgL_arg3201z00_9696;

							BgL_arg3201z00_9696 =
								(((BgL_rtl_castz00_bglt) COBJECT(
										((BgL_rtl_castz00_bglt) BgL_oz00_8783)))->BgL_fromtypez00);
							BgL_arg3199z00_9694 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3201z00_9696));
						}
						{	/* SawBbv/bbv-types.scm 1127 */
							BgL_typez00_bglt BgL_arg3203z00_9697;

							BgL_arg3203z00_9697 =
								(((BgL_rtl_castz00_bglt) COBJECT(
										((BgL_rtl_castz00_bglt) BgL_oz00_8783)))->BgL_totypez00);
							BgL_arg3200z00_9695 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3203z00_9697));
						}
						BgL_arg3195z00_9693 =
							(
							(long) CINT(BgL_arg3199z00_9694) ^
							(long) CINT(BgL_arg3200z00_9695));
					}
					return BINT(((long) CINT(BgL_arg3192z00_9692) ^ BgL_arg3195z00_9693));
		}}}

	}



/* &bbv-hash-rtl_pragma1879 */
	obj_t BGl_z62bbvzd2hashzd2rtl_pragma1879z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8784, obj_t BgL_oz00_8785)
	{
		{	/* SawBbv/bbv-types.scm 1118 */
			{

				{	/* SawBbv/bbv-types.scm 1119 */
					obj_t BgL_arg3187z00_9701;
					obj_t BgL_arg3189z00_9702;

					{	/* SawBbv/bbv-types.scm 1118 */
						obj_t BgL_nextzd2method1878zd2_9700;

						BgL_nextzd2method1878zd2_9700 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_pragmaz00_bglt) BgL_oz00_8785)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_pragmaz00zzsaw_defsz00);
						BgL_arg3187z00_9701 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1878zd2_9700,
							((obj_t) ((BgL_rtl_pragmaz00_bglt) BgL_oz00_8785)));
					}
					BgL_arg3189z00_9702 =
						BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
						(((BgL_rtl_pragmaz00_bglt) COBJECT(
									((BgL_rtl_pragmaz00_bglt) BgL_oz00_8785)))->BgL_formatz00));
					return
						BINT(
						((long) CINT(BgL_arg3187z00_9701) ^
							(long) CINT(BgL_arg3189z00_9702)));
		}}}

	}



/* &bbv-hash-rtl_lightfu1877 */
	obj_t BGl_z62bbvzd2hashzd2rtl_lightfu1877z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8786, obj_t BgL_oz00_8787)
	{
		{	/* SawBbv/bbv-types.scm 1108 */
			{

				{	/* SawBbv/bbv-types.scm 1109 */
					obj_t BgL_arg3158z00_9706;
					long BgL_arg3160z00_9707;

					{	/* SawBbv/bbv-types.scm 1108 */
						obj_t BgL_nextzd2method1876zd2_9705;

						BgL_nextzd2method1876zd2_9705 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8787)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_lightfuncallz00zzsaw_defsz00);
						BgL_arg3158z00_9706 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1876zd2_9705,
							((obj_t) ((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8787)));
					}
					{	/* SawBbv/bbv-types.scm 1111 */
						obj_t BgL_arg3161z00_9708;
						long BgL_arg3162z00_9709;

						BgL_arg3161z00_9708 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
										((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8787)))->
								BgL_namez00));
						{	/* SawBbv/bbv-types.scm 1112 */
							obj_t BgL_arg3171z00_9710;
							obj_t BgL_arg3172z00_9711;

							BgL_arg3171z00_9710 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
											((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8787)))->
									BgL_rettypez00));
							{	/* SawBbv/bbv-types.scm 1113 */
								obj_t BgL_arg3175z00_9712;

								{	/* SawBbv/bbv-types.scm 1113 */
									obj_t BgL_l1759z00_9713;

									BgL_l1759z00_9713 =
										(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
												((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8787)))->
										BgL_funsz00);
									if (NULLP(BgL_l1759z00_9713))
										{	/* SawBbv/bbv-types.scm 1113 */
											BgL_arg3175z00_9712 = BNIL;
										}
									else
										{	/* SawBbv/bbv-types.scm 1113 */
											obj_t BgL_head1761z00_9714;

											BgL_head1761z00_9714 =
												MAKE_YOUNG_PAIR(BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
												(CAR(BgL_l1759z00_9713)), BNIL);
											{	/* SawBbv/bbv-types.scm 1113 */
												obj_t BgL_g1764z00_9715;

												BgL_g1764z00_9715 = CDR(BgL_l1759z00_9713);
												{
													obj_t BgL_l1759z00_9717;
													obj_t BgL_tail1762z00_9718;

													BgL_l1759z00_9717 = BgL_g1764z00_9715;
													BgL_tail1762z00_9718 = BgL_head1761z00_9714;
												BgL_zc3z04anonymousza33177ze3z87_9716:
													if (NULLP(BgL_l1759z00_9717))
														{	/* SawBbv/bbv-types.scm 1113 */
															BgL_arg3175z00_9712 = BgL_head1761z00_9714;
														}
													else
														{	/* SawBbv/bbv-types.scm 1113 */
															obj_t BgL_newtail1763z00_9719;

															{	/* SawBbv/bbv-types.scm 1113 */
																obj_t BgL_arg3181z00_9720;

																{	/* SawBbv/bbv-types.scm 1113 */
																	obj_t BgL_arg3182z00_9721;

																	BgL_arg3182z00_9721 =
																		CAR(((obj_t) BgL_l1759z00_9717));
																	BgL_arg3181z00_9720 =
																		BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
																		(BgL_arg3182z00_9721);
																}
																BgL_newtail1763z00_9719 =
																	MAKE_YOUNG_PAIR(BgL_arg3181z00_9720, BNIL);
															}
															SET_CDR(BgL_tail1762z00_9718,
																BgL_newtail1763z00_9719);
															{	/* SawBbv/bbv-types.scm 1113 */
																obj_t BgL_arg3180z00_9722;

																BgL_arg3180z00_9722 =
																	CDR(((obj_t) BgL_l1759z00_9717));
																{
																	obj_t BgL_tail1762z00_15006;
																	obj_t BgL_l1759z00_15005;

																	BgL_l1759z00_15005 = BgL_arg3180z00_9722;
																	BgL_tail1762z00_15006 =
																		BgL_newtail1763z00_9719;
																	BgL_tail1762z00_9718 = BgL_tail1762z00_15006;
																	BgL_l1759z00_9717 = BgL_l1759z00_15005;
																	goto BgL_zc3z04anonymousza33177ze3z87_9716;
																}
															}
														}
												}
											}
										}
								}
								BgL_arg3172z00_9711 =
									BGl_bitzd2xorza2z70zzsaw_bbvzd2typeszd2(BgL_arg3175z00_9712);
							}
							BgL_arg3162z00_9709 =
								(
								(long) CINT(BgL_arg3171z00_9710) ^
								(long) CINT(BgL_arg3172z00_9711));
						}
						BgL_arg3160z00_9707 =
							((long) CINT(BgL_arg3161z00_9708) ^ BgL_arg3162z00_9709);
					}
					return BINT(((long) CINT(BgL_arg3158z00_9706) ^ BgL_arg3160z00_9707));
		}}}

	}



/* &bbv-hash-rtl_call1875 */
	obj_t BGl_z62bbvzd2hashzd2rtl_call1875z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8788, obj_t BgL_oz00_8789)
	{
		{	/* SawBbv/bbv-types.scm 1101 */
			{

				{	/* SawBbv/bbv-types.scm 1103 */
					obj_t BgL_arg3155z00_9726;
					obj_t BgL_arg3156z00_9727;

					{	/* SawBbv/bbv-types.scm 1101 */
						obj_t BgL_nextzd2method1874zd2_9725;

						BgL_nextzd2method1874zd2_9725 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_callz00_bglt) BgL_oz00_8789)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_callz00zzsaw_defsz00);
						BgL_arg3155z00_9726 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1874zd2_9725,
							((obj_t) ((BgL_rtl_callz00_bglt) BgL_oz00_8789)));
					}
					{	/* SawBbv/bbv-types.scm 1103 */
						BgL_globalz00_bglt BgL_arg3157z00_9728;

						BgL_arg3157z00_9728 =
							(((BgL_rtl_callz00_bglt) COBJECT(
									((BgL_rtl_callz00_bglt) BgL_oz00_8789)))->BgL_varz00);
						BgL_arg3156z00_9727 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3157z00_9728));
					}
					return
						BINT(
						((long) CINT(BgL_arg3155z00_9726) ^
							(long) CINT(BgL_arg3156z00_9727)));
		}}}

	}



/* &bbv-hash-rtl_new1873 */
	obj_t BGl_z62bbvzd2hashzd2rtl_new1873z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8790, obj_t BgL_oz00_8791)
	{
		{	/* SawBbv/bbv-types.scm 1092 */
			{

				{	/* SawBbv/bbv-types.scm 1093 */
					obj_t BgL_arg3133z00_9732;
					long BgL_arg3135z00_9733;

					{	/* SawBbv/bbv-types.scm 1092 */
						obj_t BgL_nextzd2method1872zd2_9731;

						BgL_nextzd2method1872zd2_9731 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_newz00_bglt) BgL_oz00_8791)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_newz00zzsaw_defsz00);
						BgL_arg3133z00_9732 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1872zd2_9731,
							((obj_t) ((BgL_rtl_newz00_bglt) BgL_oz00_8791)));
					}
					{	/* SawBbv/bbv-types.scm 1095 */
						obj_t BgL_arg3136z00_9734;
						obj_t BgL_arg3137z00_9735;

						{	/* SawBbv/bbv-types.scm 1095 */
							BgL_typez00_bglt BgL_arg3139z00_9736;

							BgL_arg3139z00_9736 =
								(((BgL_rtl_newz00_bglt) COBJECT(
										((BgL_rtl_newz00_bglt) BgL_oz00_8791)))->BgL_typez00);
							BgL_arg3136z00_9734 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3139z00_9736));
						}
						{	/* SawBbv/bbv-types.scm 1096 */
							obj_t BgL_arg3140z00_9737;

							{	/* SawBbv/bbv-types.scm 1096 */
								obj_t BgL_l1753z00_9738;

								BgL_l1753z00_9738 =
									(((BgL_rtl_newz00_bglt) COBJECT(
											((BgL_rtl_newz00_bglt) BgL_oz00_8791)))->BgL_constrz00);
								if (NULLP(BgL_l1753z00_9738))
									{	/* SawBbv/bbv-types.scm 1096 */
										BgL_arg3140z00_9737 = BNIL;
									}
								else
									{	/* SawBbv/bbv-types.scm 1096 */
										obj_t BgL_head1755z00_9739;

										BgL_head1755z00_9739 =
											MAKE_YOUNG_PAIR(BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(CAR
												(BgL_l1753z00_9738)), BNIL);
										{	/* SawBbv/bbv-types.scm 1096 */
											obj_t BgL_g1758z00_9740;

											BgL_g1758z00_9740 = CDR(BgL_l1753z00_9738);
											{
												obj_t BgL_l1753z00_9742;
												obj_t BgL_tail1756z00_9743;

												BgL_l1753z00_9742 = BgL_g1758z00_9740;
												BgL_tail1756z00_9743 = BgL_head1755z00_9739;
											BgL_zc3z04anonymousza33142ze3z87_9741:
												if (NULLP(BgL_l1753z00_9742))
													{	/* SawBbv/bbv-types.scm 1096 */
														BgL_arg3140z00_9737 = BgL_head1755z00_9739;
													}
												else
													{	/* SawBbv/bbv-types.scm 1096 */
														obj_t BgL_newtail1757z00_9744;

														{	/* SawBbv/bbv-types.scm 1096 */
															obj_t BgL_arg3145z00_9745;

															{	/* SawBbv/bbv-types.scm 1096 */
																obj_t BgL_arg3146z00_9746;

																BgL_arg3146z00_9746 =
																	CAR(((obj_t) BgL_l1753z00_9742));
																BgL_arg3145z00_9745 =
																	BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
																	(BgL_arg3146z00_9746);
															}
															BgL_newtail1757z00_9744 =
																MAKE_YOUNG_PAIR(BgL_arg3145z00_9745, BNIL);
														}
														SET_CDR(BgL_tail1756z00_9743,
															BgL_newtail1757z00_9744);
														{	/* SawBbv/bbv-types.scm 1096 */
															obj_t BgL_arg3144z00_9747;

															BgL_arg3144z00_9747 =
																CDR(((obj_t) BgL_l1753z00_9742));
															{
																obj_t BgL_tail1756z00_15064;
																obj_t BgL_l1753z00_15063;

																BgL_l1753z00_15063 = BgL_arg3144z00_9747;
																BgL_tail1756z00_15064 = BgL_newtail1757z00_9744;
																BgL_tail1756z00_9743 = BgL_tail1756z00_15064;
																BgL_l1753z00_9742 = BgL_l1753z00_15063;
																goto BgL_zc3z04anonymousza33142ze3z87_9741;
															}
														}
													}
											}
										}
									}
							}
							BgL_arg3137z00_9735 =
								BGl_bitzd2xorza2z70zzsaw_bbvzd2typeszd2(BgL_arg3140z00_9737);
						}
						BgL_arg3135z00_9733 =
							(
							(long) CINT(BgL_arg3136z00_9734) ^
							(long) CINT(BgL_arg3137z00_9735));
					}
					return BINT(((long) CINT(BgL_arg3133z00_9732) ^ BgL_arg3135z00_9733));
		}}}

	}



/* &bbv-hash-rtl_vset1871 */
	obj_t BGl_z62bbvzd2hashzd2rtl_vset1871z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8792, obj_t BgL_oz00_8793)
	{
		{	/* SawBbv/bbv-types.scm 1085 */
			{

				{	/* SawBbv/bbv-types.scm 1086 */
					obj_t BgL_arg3126z00_9751;
					long BgL_arg3127z00_9752;

					{	/* SawBbv/bbv-types.scm 1085 */
						obj_t BgL_nextzd2method1870zd2_9750;

						BgL_nextzd2method1870zd2_9750 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_vsetz00_bglt) BgL_oz00_8793)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_vsetz00zzsaw_defsz00);
						BgL_arg3126z00_9751 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1870zd2_9750,
							((obj_t) ((BgL_rtl_vsetz00_bglt) BgL_oz00_8793)));
					}
					{	/* SawBbv/bbv-types.scm 1087 */
						obj_t BgL_arg3128z00_9753;
						obj_t BgL_arg3129z00_9754;

						{	/* SawBbv/bbv-types.scm 1087 */
							BgL_typez00_bglt BgL_arg3131z00_9755;

							BgL_arg3131z00_9755 =
								(((BgL_rtl_vsetz00_bglt) COBJECT(
										((BgL_rtl_vsetz00_bglt) BgL_oz00_8793)))->BgL_typez00);
							BgL_arg3128z00_9753 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3131z00_9755));
						}
						{	/* SawBbv/bbv-types.scm 1087 */
							BgL_typez00_bglt BgL_arg3132z00_9756;

							BgL_arg3132z00_9756 =
								(((BgL_rtl_vsetz00_bglt) COBJECT(
										((BgL_rtl_vsetz00_bglt) BgL_oz00_8793)))->BgL_vtypez00);
							BgL_arg3129z00_9754 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3132z00_9756));
						}
						BgL_arg3127z00_9752 =
							(
							(long) CINT(BgL_arg3128z00_9753) ^
							(long) CINT(BgL_arg3129z00_9754));
					}
					return BINT(((long) CINT(BgL_arg3126z00_9751) ^ BgL_arg3127z00_9752));
		}}}

	}



/* &bbv-hash-rtl_setfiel1869 */
	obj_t BGl_z62bbvzd2hashzd2rtl_setfiel1869z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8794, obj_t BgL_oz00_8795)
	{
		{	/* SawBbv/bbv-types.scm 1076 */
			{

				{	/* SawBbv/bbv-types.scm 1077 */
					obj_t BgL_arg3106z00_9760;
					long BgL_arg3114z00_9761;

					{	/* SawBbv/bbv-types.scm 1076 */
						obj_t BgL_nextzd2method1868zd2_9759;

						BgL_nextzd2method1868zd2_9759 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_setfieldz00_bglt) BgL_oz00_8795)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_setfieldz00zzsaw_defsz00);
						BgL_arg3106z00_9760 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1868zd2_9759,
							((obj_t) ((BgL_rtl_setfieldz00_bglt) BgL_oz00_8795)));
					}
					{	/* SawBbv/bbv-types.scm 1079 */
						obj_t BgL_arg3115z00_9762;
						long BgL_arg3116z00_9763;

						BgL_arg3115z00_9762 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							(((BgL_rtl_setfieldz00_bglt) COBJECT(
										((BgL_rtl_setfieldz00_bglt) BgL_oz00_8795)))->BgL_namez00));
						{	/* SawBbv/bbv-types.scm 1080 */
							obj_t BgL_arg3121z00_9764;
							obj_t BgL_arg3122z00_9765;

							{	/* SawBbv/bbv-types.scm 1080 */
								BgL_typez00_bglt BgL_arg3124z00_9766;

								BgL_arg3124z00_9766 =
									(((BgL_rtl_setfieldz00_bglt) COBJECT(
											((BgL_rtl_setfieldz00_bglt) BgL_oz00_8795)))->
									BgL_objtypez00);
								BgL_arg3121z00_9764 =
									BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(((obj_t)
										BgL_arg3124z00_9766));
							}
							{	/* SawBbv/bbv-types.scm 1080 */
								BgL_typez00_bglt BgL_arg3125z00_9767;

								BgL_arg3125z00_9767 =
									(((BgL_rtl_setfieldz00_bglt) COBJECT(
											((BgL_rtl_setfieldz00_bglt) BgL_oz00_8795)))->
									BgL_typez00);
								BgL_arg3122z00_9765 =
									BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(((obj_t)
										BgL_arg3125z00_9767));
							}
							BgL_arg3116z00_9763 =
								(
								(long) CINT(BgL_arg3121z00_9764) ^
								(long) CINT(BgL_arg3122z00_9765));
						}
						BgL_arg3114z00_9761 =
							((long) CINT(BgL_arg3115z00_9762) ^ BgL_arg3116z00_9763);
					}
					return BINT(((long) CINT(BgL_arg3106z00_9760) ^ BgL_arg3114z00_9761));
		}}}

	}



/* &bbv-hash-rtl_storeg1867 */
	obj_t BGl_z62bbvzd2hashzd2rtl_storeg1867z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8796, obj_t BgL_oz00_8797)
	{
		{	/* SawBbv/bbv-types.scm 1067 */
			{

				{	/* SawBbv/bbv-types.scm 1068 */
					obj_t BgL_arg3092z00_9771;
					long BgL_arg3093z00_9772;

					{	/* SawBbv/bbv-types.scm 1067 */
						obj_t BgL_nextzd2method1866zd2_9770;

						BgL_nextzd2method1866zd2_9770 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_storegz00_bglt) BgL_oz00_8797)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_storegz00zzsaw_defsz00);
						BgL_arg3092z00_9771 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1866zd2_9770,
							((obj_t) ((BgL_rtl_storegz00_bglt) BgL_oz00_8797)));
					}
					{	/* SawBbv/bbv-types.scm 1070 */
						BgL_globalz00_bglt BgL_i1391z00_9773;

						BgL_i1391z00_9773 =
							(((BgL_rtl_storegz00_bglt) COBJECT(
									((BgL_rtl_storegz00_bglt) BgL_oz00_8797)))->BgL_varz00);
						{	/* SawBbv/bbv-types.scm 1071 */
							obj_t BgL_arg3094z00_9774;
							obj_t BgL_arg3099z00_9775;

							BgL_arg3094z00_9774 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								(((BgL_globalz00_bglt) COBJECT(BgL_i1391z00_9773))->
									BgL_modulez00));
							{	/* SawBbv/bbv-types.scm 1071 */
								obj_t BgL_arg3105z00_9776;

								BgL_arg3105z00_9776 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_i1391z00_9773)))->BgL_idz00);
								BgL_arg3099z00_9775 =
									BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(BgL_arg3105z00_9776);
							}
							BgL_arg3093z00_9772 =
								(
								(long) CINT(BgL_arg3094z00_9774) ^
								(long) CINT(BgL_arg3099z00_9775));
					}}
					return BINT(((long) CINT(BgL_arg3092z00_9771) ^ BgL_arg3093z00_9772));
		}}}

	}



/* &bbv-hash-rtl_instanc1865 */
	obj_t BGl_z62bbvzd2hashzd2rtl_instanc1865z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8798, obj_t BgL_oz00_8799)
	{
		{	/* SawBbv/bbv-types.scm 1060 */
			{

				{	/* SawBbv/bbv-types.scm 1061 */
					obj_t BgL_arg3087z00_9780;
					obj_t BgL_arg3088z00_9781;

					{	/* SawBbv/bbv-types.scm 1060 */
						obj_t BgL_nextzd2method1864zd2_9779;

						BgL_nextzd2method1864zd2_9779 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_instanceofz00_bglt) BgL_oz00_8799)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_instanceofz00zzsaw_defsz00);
						BgL_arg3087z00_9780 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1864zd2_9779,
							((obj_t) ((BgL_rtl_instanceofz00_bglt) BgL_oz00_8799)));
					}
					{	/* SawBbv/bbv-types.scm 1062 */
						BgL_typez00_bglt BgL_arg3091z00_9782;

						BgL_arg3091z00_9782 =
							(((BgL_rtl_instanceofz00_bglt) COBJECT(
									((BgL_rtl_instanceofz00_bglt) BgL_oz00_8799)))->BgL_typez00);
						BgL_arg3088z00_9781 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3091z00_9782));
					}
					return
						BINT(
						((long) CINT(BgL_arg3087z00_9780) ^
							(long) CINT(BgL_arg3088z00_9781)));
		}}}

	}



/* &bbv-hash-rtl_vlength1863 */
	obj_t BGl_z62bbvzd2hashzd2rtl_vlength1863z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8800, obj_t BgL_oz00_8801)
	{
		{	/* SawBbv/bbv-types.scm 1053 */
			{

				{	/* SawBbv/bbv-types.scm 1054 */
					obj_t BgL_arg3081z00_9786;
					long BgL_arg3082z00_9787;

					{	/* SawBbv/bbv-types.scm 1053 */
						obj_t BgL_nextzd2method1862zd2_9785;

						BgL_nextzd2method1862zd2_9785 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_vlengthz00_bglt) BgL_oz00_8801)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_vlengthz00zzsaw_defsz00);
						BgL_arg3081z00_9786 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1862zd2_9785,
							((obj_t) ((BgL_rtl_vlengthz00_bglt) BgL_oz00_8801)));
					}
					{	/* SawBbv/bbv-types.scm 1055 */
						obj_t BgL_arg3083z00_9788;
						obj_t BgL_arg3084z00_9789;

						{	/* SawBbv/bbv-types.scm 1055 */
							BgL_typez00_bglt BgL_arg3085z00_9790;

							BgL_arg3085z00_9790 =
								(((BgL_rtl_vlengthz00_bglt) COBJECT(
										((BgL_rtl_vlengthz00_bglt) BgL_oz00_8801)))->BgL_typez00);
							BgL_arg3083z00_9788 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3085z00_9790));
						}
						{	/* SawBbv/bbv-types.scm 1055 */
							BgL_typez00_bglt BgL_arg3086z00_9791;

							BgL_arg3086z00_9791 =
								(((BgL_rtl_vlengthz00_bglt) COBJECT(
										((BgL_rtl_vlengthz00_bglt) BgL_oz00_8801)))->BgL_vtypez00);
							BgL_arg3084z00_9789 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3086z00_9791));
						}
						BgL_arg3082z00_9787 =
							(
							(long) CINT(BgL_arg3083z00_9788) ^
							(long) CINT(BgL_arg3084z00_9789));
					}
					return BINT(((long) CINT(BgL_arg3081z00_9786) ^ BgL_arg3082z00_9787));
		}}}

	}



/* &bbv-hash-rtl_vref1861 */
	obj_t BGl_z62bbvzd2hashzd2rtl_vref1861z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8802, obj_t BgL_oz00_8803)
	{
		{	/* SawBbv/bbv-types.scm 1046 */
			{

				{	/* SawBbv/bbv-types.scm 1047 */
					obj_t BgL_arg3073z00_9795;
					long BgL_arg3075z00_9796;

					{	/* SawBbv/bbv-types.scm 1046 */
						obj_t BgL_nextzd2method1860zd2_9794;

						BgL_nextzd2method1860zd2_9794 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_vrefz00_bglt) BgL_oz00_8803)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_vrefz00zzsaw_defsz00);
						BgL_arg3073z00_9795 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1860zd2_9794,
							((obj_t) ((BgL_rtl_vrefz00_bglt) BgL_oz00_8803)));
					}
					{	/* SawBbv/bbv-types.scm 1048 */
						obj_t BgL_arg3077z00_9797;
						obj_t BgL_arg3078z00_9798;

						{	/* SawBbv/bbv-types.scm 1048 */
							BgL_typez00_bglt BgL_arg3079z00_9799;

							BgL_arg3079z00_9799 =
								(((BgL_rtl_vrefz00_bglt) COBJECT(
										((BgL_rtl_vrefz00_bglt) BgL_oz00_8803)))->BgL_typez00);
							BgL_arg3077z00_9797 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3079z00_9799));
						}
						{	/* SawBbv/bbv-types.scm 1048 */
							BgL_typez00_bglt BgL_arg3080z00_9800;

							BgL_arg3080z00_9800 =
								(((BgL_rtl_vrefz00_bglt) COBJECT(
										((BgL_rtl_vrefz00_bglt) BgL_oz00_8803)))->BgL_vtypez00);
							BgL_arg3078z00_9798 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3080z00_9800));
						}
						BgL_arg3075z00_9796 =
							(
							(long) CINT(BgL_arg3077z00_9797) ^
							(long) CINT(BgL_arg3078z00_9798));
					}
					return BINT(((long) CINT(BgL_arg3073z00_9795) ^ BgL_arg3075z00_9796));
		}}}

	}



/* &bbv-hash-rtl_valloc1859 */
	obj_t BGl_z62bbvzd2hashzd2rtl_valloc1859z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8804, obj_t BgL_oz00_8805)
	{
		{	/* SawBbv/bbv-types.scm 1039 */
			{

				{	/* SawBbv/bbv-types.scm 1040 */
					obj_t BgL_arg3062z00_9804;
					long BgL_arg3066z00_9805;

					{	/* SawBbv/bbv-types.scm 1039 */
						obj_t BgL_nextzd2method1858zd2_9803;

						BgL_nextzd2method1858zd2_9803 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_vallocz00_bglt) BgL_oz00_8805)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_vallocz00zzsaw_defsz00);
						BgL_arg3062z00_9804 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1858zd2_9803,
							((obj_t) ((BgL_rtl_vallocz00_bglt) BgL_oz00_8805)));
					}
					{	/* SawBbv/bbv-types.scm 1041 */
						obj_t BgL_arg3067z00_9806;
						obj_t BgL_arg3070z00_9807;

						{	/* SawBbv/bbv-types.scm 1041 */
							BgL_typez00_bglt BgL_arg3071z00_9808;

							BgL_arg3071z00_9808 =
								(((BgL_rtl_vallocz00_bglt) COBJECT(
										((BgL_rtl_vallocz00_bglt) BgL_oz00_8805)))->BgL_typez00);
							BgL_arg3067z00_9806 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3071z00_9808));
						}
						{	/* SawBbv/bbv-types.scm 1041 */
							BgL_typez00_bglt BgL_arg3072z00_9809;

							BgL_arg3072z00_9809 =
								(((BgL_rtl_vallocz00_bglt) COBJECT(
										((BgL_rtl_vallocz00_bglt) BgL_oz00_8805)))->BgL_vtypez00);
							BgL_arg3070z00_9807 =
								BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
								((obj_t) BgL_arg3072z00_9809));
						}
						BgL_arg3066z00_9805 =
							(
							(long) CINT(BgL_arg3067z00_9806) ^
							(long) CINT(BgL_arg3070z00_9807));
					}
					return BINT(((long) CINT(BgL_arg3062z00_9804) ^ BgL_arg3066z00_9805));
		}}}

	}



/* &bbv-hash-rtl_getfiel1857 */
	obj_t BGl_z62bbvzd2hashzd2rtl_getfiel1857z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8806, obj_t BgL_oz00_8807)
	{
		{	/* SawBbv/bbv-types.scm 1030 */
			{

				{	/* SawBbv/bbv-types.scm 1031 */
					obj_t BgL_arg3043z00_9813;
					long BgL_arg3044z00_9814;

					{	/* SawBbv/bbv-types.scm 1030 */
						obj_t BgL_nextzd2method1856zd2_9812;

						BgL_nextzd2method1856zd2_9812 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_getfieldz00_bglt) BgL_oz00_8807)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_getfieldz00zzsaw_defsz00);
						BgL_arg3043z00_9813 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1856zd2_9812,
							((obj_t) ((BgL_rtl_getfieldz00_bglt) BgL_oz00_8807)));
					}
					{	/* SawBbv/bbv-types.scm 1033 */
						obj_t BgL_arg3049z00_9815;
						long BgL_arg3050z00_9816;

						BgL_arg3049z00_9815 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							(((BgL_rtl_getfieldz00_bglt) COBJECT(
										((BgL_rtl_getfieldz00_bglt) BgL_oz00_8807)))->BgL_namez00));
						{	/* SawBbv/bbv-types.scm 1034 */
							obj_t BgL_arg3055z00_9817;
							obj_t BgL_arg3058z00_9818;

							{	/* SawBbv/bbv-types.scm 1034 */
								BgL_typez00_bglt BgL_arg3059z00_9819;

								BgL_arg3059z00_9819 =
									(((BgL_rtl_getfieldz00_bglt) COBJECT(
											((BgL_rtl_getfieldz00_bglt) BgL_oz00_8807)))->
									BgL_objtypez00);
								BgL_arg3055z00_9817 =
									BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(((obj_t)
										BgL_arg3059z00_9819));
							}
							{	/* SawBbv/bbv-types.scm 1034 */
								BgL_typez00_bglt BgL_arg3061z00_9820;

								BgL_arg3061z00_9820 =
									(((BgL_rtl_getfieldz00_bglt) COBJECT(
											((BgL_rtl_getfieldz00_bglt) BgL_oz00_8807)))->
									BgL_typez00);
								BgL_arg3058z00_9818 =
									BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(((obj_t)
										BgL_arg3061z00_9820));
							}
							BgL_arg3050z00_9816 =
								(
								(long) CINT(BgL_arg3055z00_9817) ^
								(long) CINT(BgL_arg3058z00_9818));
						}
						BgL_arg3044z00_9814 =
							((long) CINT(BgL_arg3049z00_9815) ^ BgL_arg3050z00_9816);
					}
					return BINT(((long) CINT(BgL_arg3043z00_9813) ^ BgL_arg3044z00_9814));
		}}}

	}



/* &bbv-hash-rtl_globalr1855 */
	obj_t BGl_z62bbvzd2hashzd2rtl_globalr1855z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8808, obj_t BgL_oz00_8809)
	{
		{	/* SawBbv/bbv-types.scm 1023 */
			{

				{	/* SawBbv/bbv-types.scm 1025 */
					obj_t BgL_arg3032z00_9824;
					obj_t BgL_arg3037z00_9825;

					{	/* SawBbv/bbv-types.scm 1023 */
						obj_t BgL_nextzd2method1854zd2_9823;

						BgL_nextzd2method1854zd2_9823 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_globalrefz00_bglt) BgL_oz00_8809)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_globalrefz00zzsaw_defsz00);
						BgL_arg3032z00_9824 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1854zd2_9823,
							((obj_t) ((BgL_rtl_globalrefz00_bglt) BgL_oz00_8809)));
					}
					{	/* SawBbv/bbv-types.scm 1025 */
						BgL_globalz00_bglt BgL_arg3038z00_9826;

						BgL_arg3038z00_9826 =
							(((BgL_rtl_globalrefz00_bglt) COBJECT(
									((BgL_rtl_globalrefz00_bglt) BgL_oz00_8809)))->BgL_varz00);
						BgL_arg3037z00_9825 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3038z00_9826));
					}
					return
						BINT(
						((long) CINT(BgL_arg3032z00_9824) ^
							(long) CINT(BgL_arg3037z00_9825)));
		}}}

	}



/* &bbv-hash-rtl_loadfun1853 */
	obj_t BGl_z62bbvzd2hashzd2rtl_loadfun1853z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8810, obj_t BgL_oz00_8811)
	{
		{	/* SawBbv/bbv-types.scm 1016 */
			{

				{	/* SawBbv/bbv-types.scm 1018 */
					obj_t BgL_arg3029z00_9830;
					obj_t BgL_arg3030z00_9831;

					{	/* SawBbv/bbv-types.scm 1016 */
						obj_t BgL_nextzd2method1852zd2_9829;

						BgL_nextzd2method1852zd2_9829 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_loadfunz00_bglt) BgL_oz00_8811)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_loadfunz00zzsaw_defsz00);
						BgL_arg3029z00_9830 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1852zd2_9829,
							((obj_t) ((BgL_rtl_loadfunz00_bglt) BgL_oz00_8811)));
					}
					{	/* SawBbv/bbv-types.scm 1018 */
						BgL_globalz00_bglt BgL_arg3031z00_9832;

						BgL_arg3031z00_9832 =
							(((BgL_rtl_loadfunz00_bglt) COBJECT(
									((BgL_rtl_loadfunz00_bglt) BgL_oz00_8811)))->BgL_varz00);
						BgL_arg3030z00_9831 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3031z00_9832));
					}
					return
						BINT(
						((long) CINT(BgL_arg3029z00_9830) ^
							(long) CINT(BgL_arg3030z00_9831)));
		}}}

	}



/* &bbv-hash-rtl_loadg1851 */
	obj_t BGl_z62bbvzd2hashzd2rtl_loadg1851z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8812, obj_t BgL_oz00_8813)
	{
		{	/* SawBbv/bbv-types.scm 1009 */
			{

				{	/* SawBbv/bbv-types.scm 1011 */
					obj_t BgL_arg3025z00_9836;
					obj_t BgL_arg3026z00_9837;

					{	/* SawBbv/bbv-types.scm 1009 */
						obj_t BgL_nextzd2method1850zd2_9835;

						BgL_nextzd2method1850zd2_9835 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_loadgz00_bglt) BgL_oz00_8813)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_loadgz00zzsaw_defsz00);
						BgL_arg3025z00_9836 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1850zd2_9835,
							((obj_t) ((BgL_rtl_loadgz00_bglt) BgL_oz00_8813)));
					}
					{	/* SawBbv/bbv-types.scm 1011 */
						BgL_globalz00_bglt BgL_arg3027z00_9838;

						BgL_arg3027z00_9838 =
							(((BgL_rtl_loadgz00_bglt) COBJECT(
									((BgL_rtl_loadgz00_bglt) BgL_oz00_8813)))->BgL_varz00);
						BgL_arg3026z00_9837 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3027z00_9838));
					}
					return
						BINT(
						((long) CINT(BgL_arg3025z00_9836) ^
							(long) CINT(BgL_arg3026z00_9837)));
		}}}

	}



/* &bbv-hash-rtl_loadi1849 */
	obj_t BGl_z62bbvzd2hashzd2rtl_loadi1849z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8814, obj_t BgL_oz00_8815)
	{
		{	/* SawBbv/bbv-types.scm 1002 */
			{

				{	/* SawBbv/bbv-types.scm 1004 */
					obj_t BgL_arg3021z00_9842;
					obj_t BgL_arg3023z00_9843;

					{	/* SawBbv/bbv-types.scm 1002 */
						obj_t BgL_nextzd2method1848zd2_9841;

						BgL_nextzd2method1848zd2_9841 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_loadiz00_bglt) BgL_oz00_8815)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_loadiz00zzsaw_defsz00);
						BgL_arg3021z00_9842 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1848zd2_9841,
							((obj_t) ((BgL_rtl_loadiz00_bglt) BgL_oz00_8815)));
					}
					{	/* SawBbv/bbv-types.scm 1004 */
						BgL_atomz00_bglt BgL_arg3024z00_9844;

						BgL_arg3024z00_9844 =
							(((BgL_rtl_loadiz00_bglt) COBJECT(
									((BgL_rtl_loadiz00_bglt) BgL_oz00_8815)))->BgL_constantz00);
						BgL_arg3023z00_9843 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3024z00_9844));
					}
					return
						BINT(
						((long) CINT(BgL_arg3021z00_9842) ^
							(long) CINT(BgL_arg3023z00_9843)));
		}}}

	}



/* &bbv-hash-rtl_return1847 */
	obj_t BGl_z62bbvzd2hashzd2rtl_return1847z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8816, obj_t BgL_oz00_8817)
	{
		{	/* SawBbv/bbv-types.scm 995 */
			{

				{	/* SawBbv/bbv-types.scm 997 */
					obj_t BgL_arg3018z00_9848;
					obj_t BgL_arg3019z00_9849;

					{	/* SawBbv/bbv-types.scm 995 */
						obj_t BgL_nextzd2method1846zd2_9847;

						BgL_nextzd2method1846zd2_9847 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_returnz00_bglt) BgL_oz00_8817)),
							BGl_bbvzd2hashzd2envz00zzsaw_bbvzd2typeszd2,
							BGl_rtl_returnz00zzsaw_defsz00);
						BgL_arg3018z00_9848 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1846zd2_9847,
							((obj_t) ((BgL_rtl_returnz00_bglt) BgL_oz00_8817)));
					}
					{	/* SawBbv/bbv-types.scm 997 */
						BgL_typez00_bglt BgL_arg3020z00_9850;

						BgL_arg3020z00_9850 =
							(((BgL_rtl_returnz00_bglt) COBJECT(
									((BgL_rtl_returnz00_bglt) BgL_oz00_8817)))->BgL_typez00);
						BgL_arg3019z00_9849 =
							BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
							((obj_t) BgL_arg3020z00_9850));
					}
					return
						BINT(
						((long) CINT(BgL_arg3018z00_9848) ^
							(long) CINT(BgL_arg3019z00_9849)));
		}}}

	}



/* &bbv-hash-rtl_fun1845 */
	obj_t BGl_z62bbvzd2hashzd2rtl_fun1845z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8818, obj_t BgL_oz00_8819)
	{
		{	/* SawBbv/bbv-types.scm 989 */
			{	/* SawBbv/bbv-types.scm 990 */
				obj_t BgL_arg3016z00_9852;

				{	/* SawBbv/bbv-types.scm 990 */
					obj_t BgL_arg3017z00_9853;

					{	/* SawBbv/bbv-types.scm 990 */
						obj_t BgL_arg1815z00_9854;
						long BgL_arg1816z00_9855;

						BgL_arg1815z00_9854 = (BGl_za2classesza2z00zz__objectz00);
						{	/* SawBbv/bbv-types.scm 990 */
							long BgL_arg1817z00_9856;

							BgL_arg1817z00_9856 =
								BGL_OBJECT_CLASS_NUM(
								((BgL_objectz00_bglt) ((BgL_rtl_funz00_bglt) BgL_oz00_8819)));
							BgL_arg1816z00_9855 = (BgL_arg1817z00_9856 - OBJECT_TYPE);
						}
						BgL_arg3017z00_9853 =
							VECTOR_REF(BgL_arg1815z00_9854, BgL_arg1816z00_9855);
					}
					BgL_arg3016z00_9852 =
						BGl_classzd2namezd2zz__objectz00(BgL_arg3017z00_9853);
				}
				return BINT(bgl_symbol_hash_number(BgL_arg3016z00_9852));
			}
		}

	}



/* &bbv-hash-rtl_reg1843 */
	obj_t BGl_z62bbvzd2hashzd2rtl_reg1843z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8820, obj_t BgL_oz00_8821)
	{
		{	/* SawBbv/bbv-types.scm 981 */
			{	/* SawBbv/bbv-types.scm 983 */
				obj_t BgL_arg3009z00_9858;
				long BgL_arg3010z00_9859;

				{	/* SawBbv/bbv-types.scm 983 */
					BgL_typez00_bglt BgL_arg3011z00_9860;

					BgL_arg3011z00_9860 =
						(((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_oz00_8821)))->BgL_typez00);
					BgL_arg3009z00_9858 =
						BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
						((obj_t) BgL_arg3011z00_9860));
				}
				BgL_arg3010z00_9859 =
					(bgl_symbol_hash_number(
						(((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt) BgL_oz00_8821)))->BgL_namez00)) ^
					bgl_symbol_hash_number(
						(((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt) BgL_oz00_8821)))->BgL_keyz00)));
				return BINT(((long) CINT(BgL_arg3009z00_9858) ^ BgL_arg3010z00_9859));
		}}

	}



/* &bbv-hash-rtl_ins/bbv1841 */
	obj_t BGl_z62bbvzd2hashzd2rtl_inszf2bbv1841z90zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8822, obj_t BgL_oz00_8823)
	{
		{	/* SawBbv/bbv-types.scm 967 */
			{	/* SawBbv/bbv-types.scm 969 */
				bool_t BgL_test4559z00_15367;

				{	/* SawBbv/bbv-types.scm 969 */
					obj_t BgL_tmpz00_15368;

					{
						BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15369;

						{
							obj_t BgL_auxz00_15370;

							{	/* SawBbv/bbv-types.scm 969 */
								BgL_objectz00_bglt BgL_tmpz00_15371;

								BgL_tmpz00_15371 =
									((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8823));
								BgL_auxz00_15370 = BGL_OBJECT_WIDENING(BgL_tmpz00_15371);
							}
							BgL_auxz00_15369 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15370);
						}
						BgL_tmpz00_15368 =
							(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15369))->
							BgL_z52hashz52);
					}
					BgL_test4559z00_15367 = CBOOL(BgL_tmpz00_15368);
				}
				if (BgL_test4559z00_15367)
					{	/* SawBbv/bbv-types.scm 969 */
						BFALSE;
					}
				else
					{
						obj_t BgL_auxz00_15385;
						BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15378;

						{	/* SawBbv/bbv-types.scm 972 */
							obj_t BgL_arg2988z00_9862;
							long BgL_arg2989z00_9863;

							{	/* SawBbv/bbv-types.scm 972 */
								obj_t BgL_arg2990z00_9864;

								BgL_arg2990z00_9864 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt)
												((BgL_rtl_insz00_bglt) BgL_oz00_8823))))->BgL_destz00);
								BgL_arg2988z00_9862 =
									BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(BgL_arg2990z00_9864);
							}
							{	/* SawBbv/bbv-types.scm 974 */
								obj_t BgL_arg2991z00_9865;
								obj_t BgL_arg2992z00_9866;

								{	/* SawBbv/bbv-types.scm 974 */
									obj_t BgL_arg2993z00_9867;

									{	/* SawBbv/bbv-types.scm 974 */
										obj_t BgL_l1747z00_9868;

										BgL_l1747z00_9868 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt)
														((BgL_rtl_insz00_bglt) BgL_oz00_8823))))->
											BgL_argsz00);
										if (NULLP(BgL_l1747z00_9868))
											{	/* SawBbv/bbv-types.scm 974 */
												BgL_arg2993z00_9867 = BNIL;
											}
										else
											{	/* SawBbv/bbv-types.scm 974 */
												obj_t BgL_head1749z00_9869;

												{	/* SawBbv/bbv-types.scm 974 */
													obj_t BgL_arg3002z00_9870;

													{	/* SawBbv/bbv-types.scm 974 */
														obj_t BgL_arg3003z00_9871;

														BgL_arg3003z00_9871 = CAR(BgL_l1747z00_9868);
														BgL_arg3002z00_9870 =
															BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
															(BgL_arg3003z00_9871);
													}
													BgL_head1749z00_9869 =
														MAKE_YOUNG_PAIR(BgL_arg3002z00_9870, BNIL);
												}
												{	/* SawBbv/bbv-types.scm 974 */
													obj_t BgL_g1752z00_9872;

													BgL_g1752z00_9872 = CDR(BgL_l1747z00_9868);
													{
														obj_t BgL_l1747z00_9874;
														obj_t BgL_tail1750z00_9875;

														BgL_l1747z00_9874 = BgL_g1752z00_9872;
														BgL_tail1750z00_9875 = BgL_head1749z00_9869;
													BgL_zc3z04anonymousza32995ze3z87_9873:
														if (NULLP(BgL_l1747z00_9874))
															{	/* SawBbv/bbv-types.scm 974 */
																BgL_arg2993z00_9867 = BgL_head1749z00_9869;
															}
														else
															{	/* SawBbv/bbv-types.scm 974 */
																obj_t BgL_newtail1751z00_9876;

																{	/* SawBbv/bbv-types.scm 974 */
																	obj_t BgL_arg3000z00_9877;

																	{	/* SawBbv/bbv-types.scm 974 */
																		obj_t BgL_arg3001z00_9878;

																		BgL_arg3001z00_9878 =
																			CAR(((obj_t) BgL_l1747z00_9874));
																		BgL_arg3000z00_9877 =
																			BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
																			(BgL_arg3001z00_9878);
																	}
																	BgL_newtail1751z00_9876 =
																		MAKE_YOUNG_PAIR(BgL_arg3000z00_9877, BNIL);
																}
																SET_CDR(BgL_tail1750z00_9875,
																	BgL_newtail1751z00_9876);
																{	/* SawBbv/bbv-types.scm 974 */
																	obj_t BgL_arg2999z00_9879;

																	BgL_arg2999z00_9879 =
																		CDR(((obj_t) BgL_l1747z00_9874));
																	{
																		obj_t BgL_tail1750z00_15409;
																		obj_t BgL_l1747z00_15408;

																		BgL_l1747z00_15408 = BgL_arg2999z00_9879;
																		BgL_tail1750z00_15409 =
																			BgL_newtail1751z00_9876;
																		BgL_tail1750z00_9875 =
																			BgL_tail1750z00_15409;
																		BgL_l1747z00_9874 = BgL_l1747z00_15408;
																		goto BgL_zc3z04anonymousza32995ze3z87_9873;
																	}
																}
															}
													}
												}
											}
									}
									BgL_arg2991z00_9865 =
										BGl_bitzd2xorza2z70zzsaw_bbvzd2typeszd2
										(BgL_arg2993z00_9867);
								}
								{	/* SawBbv/bbv-types.scm 975 */
									BgL_rtl_funz00_bglt BgL_arg3008z00_9880;

									BgL_arg3008z00_9880 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt)
													((BgL_rtl_insz00_bglt) BgL_oz00_8823))))->BgL_funz00);
									BgL_arg2992z00_9866 =
										BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
										((obj_t) BgL_arg3008z00_9880));
								}
								BgL_arg2989z00_9863 =
									(
									(long) CINT(BgL_arg2991z00_9865) ^
									(long) CINT(BgL_arg2992z00_9866));
							}
							BgL_auxz00_15385 =
								BINT(((long) CINT(BgL_arg2988z00_9862) ^ BgL_arg2989z00_9863));
						}
						{
							obj_t BgL_auxz00_15379;

							{	/* SawBbv/bbv-types.scm 970 */
								BgL_objectz00_bglt BgL_tmpz00_15380;

								BgL_tmpz00_15380 =
									((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8823));
								BgL_auxz00_15379 = BGL_OBJECT_WIDENING(BgL_tmpz00_15380);
							}
							BgL_auxz00_15378 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15379);
						}
						((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15378))->
								BgL_z52hashz52) = ((obj_t) BgL_auxz00_15385), BUNSPEC);
			}}
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15423;

				{
					obj_t BgL_auxz00_15424;

					{	/* SawBbv/bbv-types.scm 976 */
						BgL_objectz00_bglt BgL_tmpz00_15425;

						BgL_tmpz00_15425 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_8823));
						BgL_auxz00_15424 = BGL_OBJECT_WIDENING(BgL_tmpz00_15425);
					}
					BgL_auxz00_15423 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15424);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15423))->
					BgL_z52hashz52);
			}
		}

	}



/* &bbv-hash-blockS1839 */
	obj_t BGl_z62bbvzd2hashzd2blockS1839z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8824, obj_t BgL_oz00_8825)
	{
		{	/* SawBbv/bbv-types.scm 955 */
			{
				obj_t BgL_insz00_9883;

				{	/* SawBbv/bbv-types.scm 961 */
					bool_t BgL_test4563z00_15431;

					{	/* SawBbv/bbv-types.scm 961 */
						obj_t BgL_tmpz00_15432;

						{
							BgL_blocksz00_bglt BgL_auxz00_15433;

							{
								obj_t BgL_auxz00_15434;

								{	/* SawBbv/bbv-types.scm 961 */
									BgL_objectz00_bglt BgL_tmpz00_15435;

									BgL_tmpz00_15435 =
										((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8825));
									BgL_auxz00_15434 = BGL_OBJECT_WIDENING(BgL_tmpz00_15435);
								}
								BgL_auxz00_15433 = ((BgL_blocksz00_bglt) BgL_auxz00_15434);
							}
							BgL_tmpz00_15432 =
								(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15433))->
								BgL_z52hashz52);
						}
						BgL_test4563z00_15431 = CBOOL(BgL_tmpz00_15432);
					}
					if (BgL_test4563z00_15431)
						{	/* SawBbv/bbv-types.scm 961 */
							BFALSE;
						}
					else
						{
							obj_t BgL_auxz00_15449;
							BgL_blocksz00_bglt BgL_auxz00_15442;

							{	/* SawBbv/bbv-types.scm 961 */
								obj_t BgL_arg2972z00_9897;

								BgL_arg2972z00_9897 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_8825))))->BgL_firstz00);
								BgL_insz00_9883 = BgL_arg2972z00_9897;
								if (NULLP(BgL_insz00_9883))
									{	/* SawBbv/bbv-types.scm 958 */
										BgL_auxz00_15449 = BINT(0L);
									}
								else
									{	/* SawBbv/bbv-types.scm 958 */
										obj_t BgL_arg2975z00_9884;

										{	/* SawBbv/bbv-types.scm 958 */
											obj_t BgL_l1741z00_9885;

											BgL_l1741z00_9885 = BgL_insz00_9883;
											{	/* SawBbv/bbv-types.scm 958 */
												obj_t BgL_head1743z00_9886;

												{	/* SawBbv/bbv-types.scm 958 */
													obj_t BgL_arg2985z00_9887;

													{	/* SawBbv/bbv-types.scm 958 */
														obj_t BgL_arg2986z00_9888;

														BgL_arg2986z00_9888 =
															CAR(((obj_t) BgL_l1741z00_9885));
														BgL_arg2985z00_9887 =
															BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
															(BgL_arg2986z00_9888);
													}
													BgL_head1743z00_9886 =
														MAKE_YOUNG_PAIR(BgL_arg2985z00_9887, BNIL);
												}
												{	/* SawBbv/bbv-types.scm 958 */
													obj_t BgL_g1746z00_9889;

													BgL_g1746z00_9889 = CDR(((obj_t) BgL_l1741z00_9885));
													{
														obj_t BgL_l1741z00_9891;
														obj_t BgL_tail1744z00_9892;

														BgL_l1741z00_9891 = BgL_g1746z00_9889;
														BgL_tail1744z00_9892 = BgL_head1743z00_9886;
													BgL_zc3z04anonymousza32977ze3z87_9890:
														if (NULLP(BgL_l1741z00_9891))
															{	/* SawBbv/bbv-types.scm 958 */
																BgL_arg2975z00_9884 = BgL_head1743z00_9886;
															}
														else
															{	/* SawBbv/bbv-types.scm 958 */
																obj_t BgL_newtail1745z00_9893;

																{	/* SawBbv/bbv-types.scm 958 */
																	obj_t BgL_arg2980z00_9894;

																	{	/* SawBbv/bbv-types.scm 958 */
																		obj_t BgL_arg2981z00_9895;

																		BgL_arg2981z00_9895 =
																			CAR(((obj_t) BgL_l1741z00_9891));
																		BgL_arg2980z00_9894 =
																			BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
																			(BgL_arg2981z00_9895);
																	}
																	BgL_newtail1745z00_9893 =
																		MAKE_YOUNG_PAIR(BgL_arg2980z00_9894, BNIL);
																}
																SET_CDR(BgL_tail1744z00_9892,
																	BgL_newtail1745z00_9893);
																{	/* SawBbv/bbv-types.scm 958 */
																	obj_t BgL_arg2979z00_9896;

																	BgL_arg2979z00_9896 =
																		CDR(((obj_t) BgL_l1741z00_9891));
																	{
																		obj_t BgL_tail1744z00_15472;
																		obj_t BgL_l1741z00_15471;

																		BgL_l1741z00_15471 = BgL_arg2979z00_9896;
																		BgL_tail1744z00_15472 =
																			BgL_newtail1745z00_9893;
																		BgL_tail1744z00_9892 =
																			BgL_tail1744z00_15472;
																		BgL_l1741z00_9891 = BgL_l1741z00_15471;
																		goto BgL_zc3z04anonymousza32977ze3z87_9890;
																	}
																}
															}
													}
												}
											}
										}
										BgL_auxz00_15449 =
											BGl_bitzd2xorza2z70zzsaw_bbvzd2typeszd2
											(BgL_arg2975z00_9884);
									}
							}
							{
								obj_t BgL_auxz00_15443;

								{	/* SawBbv/bbv-types.scm 961 */
									BgL_objectz00_bglt BgL_tmpz00_15444;

									BgL_tmpz00_15444 =
										((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8825));
									BgL_auxz00_15443 = BGL_OBJECT_WIDENING(BgL_tmpz00_15444);
								}
								BgL_auxz00_15442 = ((BgL_blocksz00_bglt) BgL_auxz00_15443);
							}
							((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15442))->
									BgL_z52hashz52) = ((obj_t) BgL_auxz00_15449), BUNSPEC);
						}
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_15475;

					{
						obj_t BgL_auxz00_15476;

						{	/* SawBbv/bbv-types.scm 962 */
							BgL_objectz00_bglt BgL_tmpz00_15477;

							BgL_tmpz00_15477 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8825));
							BgL_auxz00_15476 = BGL_OBJECT_WIDENING(BgL_tmpz00_15477);
						}
						BgL_auxz00_15475 = ((BgL_blocksz00_bglt) BgL_auxz00_15476);
					}
					return
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15475))->BgL_z52hashz52);
				}
			}
		}

	}



/* &bbv-hash-atom1837 */
	obj_t BGl_z62bbvzd2hashzd2atom1837z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8826, obj_t BgL_oz00_8827)
	{
		{	/* SawBbv/bbv-types.scm 948 */
			{	/* SawBbv/bbv-types.scm 950 */
				obj_t BgL_arg2967z00_9899;

				BgL_arg2967z00_9899 =
					(((BgL_atomz00_bglt) COBJECT(
							((BgL_atomz00_bglt) BgL_oz00_8827)))->BgL_valuez00);
				return BINT(BGl_getzd2hashnumberzd2zz__hashz00(BgL_arg2967z00_9899));
			}
		}

	}



/* &bbv-hash-type1835 */
	obj_t BGl_z62bbvzd2hashzd2type1835z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8828, obj_t BgL_oz00_8829)
	{
		{	/* SawBbv/bbv-types.scm 941 */
			{	/* SawBbv/bbv-types.scm 943 */
				obj_t BgL_arg2966z00_9901;

				BgL_arg2966z00_9901 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_8829)))->BgL_idz00);
				return BINT(bgl_symbol_hash_number(BgL_arg2966z00_9901));
			}
		}

	}



/* &dump-blockS1830 */
	obj_t BGl_z62dumpzd2blockS1830zb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8830,
		obj_t BgL_oz00_8831, obj_t BgL_pz00_8832, obj_t BgL_mz00_8833)
	{
		{	/* SawBbv/bbv-types.scm 677 */
			{	/* SawBbv/bbv-types.scm 689 */
				obj_t BgL_tmpz00_15491;

				BgL_tmpz00_15491 = ((obj_t) BgL_pz00_8832);
				bgl_display_string(BGl_string3865z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15491);
			}
			{	/* SawBbv/bbv-types.scm 689 */
				int BgL_arg2903z00_9903;

				BgL_arg2903z00_9903 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_oz00_8831))))->BgL_labelz00);
				bgl_display_obj(BINT(BgL_arg2903z00_9903), BgL_pz00_8832);
			}
			{	/* SawBbv/bbv-types.scm 689 */
				obj_t BgL_tmpz00_15499;

				BgL_tmpz00_15499 = ((obj_t) BgL_pz00_8832);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15499);
			}
			{	/* SawBbv/bbv-types.scm 690 */
				long BgL_arg2904z00_9904;

				BgL_arg2904z00_9904 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
					(int) (BgL_arg2904z00_9904));
			}
			{	/* SawBbv/bbv-types.scm 691 */
				obj_t BgL_tmpz00_15506;

				BgL_tmpz00_15506 = ((obj_t) BgL_pz00_8832);
				bgl_display_string(BGl_string3866z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15506);
			}
			{	/* SawBbv/bbv-types.scm 691 */
				int BgL_arg2905z00_9905;

				{	/* SawBbv/bbv-types.scm 691 */
					BgL_blockz00_bglt BgL_arg2906z00_9906;

					{
						BgL_blocksz00_bglt BgL_auxz00_15509;

						{
							obj_t BgL_auxz00_15510;

							{	/* SawBbv/bbv-types.scm 691 */
								BgL_objectz00_bglt BgL_tmpz00_15511;

								BgL_tmpz00_15511 =
									((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8831));
								BgL_auxz00_15510 = BGL_OBJECT_WIDENING(BgL_tmpz00_15511);
							}
							BgL_auxz00_15509 = ((BgL_blocksz00_bglt) BgL_auxz00_15510);
						}
						BgL_arg2906z00_9906 =
							(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15509))->BgL_parentz00);
					}
					BgL_arg2905z00_9905 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_arg2906z00_9906)))->BgL_labelz00);
				}
				bgl_display_obj(BINT(BgL_arg2905z00_9905), BgL_pz00_8832);
			}
			{	/* SawBbv/bbv-types.scm 691 */
				obj_t BgL_tmpz00_15521;

				BgL_tmpz00_15521 = ((obj_t) BgL_pz00_8832);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15521);
			}
			{	/* SawBbv/bbv-types.scm 692 */
				long BgL_arg2907z00_9907;

				BgL_arg2907z00_9907 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
					(int) (BgL_arg2907z00_9907));
			}
			{	/* SawBbv/bbv-types.scm 693 */
				obj_t BgL_tmpz00_15528;

				BgL_tmpz00_15528 = ((obj_t) BgL_pz00_8832);
				bgl_display_string(BGl_string3867z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15528);
			}
			{	/* SawBbv/bbv-types.scm 693 */
				obj_t BgL_arg2908z00_9908;

				{	/* SawBbv/bbv-types.scm 693 */
					BgL_blockz00_bglt BgL_i1330z00_9909;

					{
						BgL_blocksz00_bglt BgL_auxz00_15531;

						{
							obj_t BgL_auxz00_15532;

							{	/* SawBbv/bbv-types.scm 693 */
								BgL_objectz00_bglt BgL_tmpz00_15533;

								BgL_tmpz00_15533 =
									((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8831));
								BgL_auxz00_15532 = BGL_OBJECT_WIDENING(BgL_tmpz00_15533);
							}
							BgL_auxz00_15531 = ((BgL_blocksz00_bglt) BgL_auxz00_15532);
						}
						BgL_i1330z00_9909 =
							(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15531))->BgL_parentz00);
					}
					{
						BgL_blockvz00_bglt BgL_auxz00_15539;

						{
							obj_t BgL_auxz00_15540;

							{	/* SawBbv/bbv-types.scm 693 */
								BgL_objectz00_bglt BgL_tmpz00_15541;

								BgL_tmpz00_15541 = ((BgL_objectz00_bglt) BgL_i1330z00_9909);
								BgL_auxz00_15540 = BGL_OBJECT_WIDENING(BgL_tmpz00_15541);
							}
							BgL_auxz00_15539 = ((BgL_blockvz00_bglt) BgL_auxz00_15540);
						}
						BgL_arg2908z00_9908 =
							(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_15539))->BgL_mergez00);
				}}
				bgl_display_obj(BgL_arg2908z00_9908, BgL_pz00_8832);
			}
			{	/* SawBbv/bbv-types.scm 693 */
				obj_t BgL_tmpz00_15547;

				BgL_tmpz00_15547 = ((obj_t) BgL_pz00_8832);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15547);
			}
			{	/* SawBbv/bbv-types.scm 694 */
				long BgL_arg2912z00_9910;

				BgL_arg2912z00_9910 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
					(int) (BgL_arg2912z00_9910));
			}
			{	/* SawBbv/bbv-types.scm 695 */
				obj_t BgL_tmpz00_15554;

				BgL_tmpz00_15554 = ((obj_t) BgL_pz00_8832);
				bgl_display_string(BGl_string3868z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15554);
			}
			{	/* SawBbv/bbv-types.scm 695 */
				obj_t BgL_arg2913z00_9911;

				{	/* SawBbv/bbv-types.scm 695 */
					obj_t BgL_l1720z00_9912;

					BgL_l1720z00_9912 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_oz00_8831))))->BgL_predsz00);
					if (NULLP(BgL_l1720z00_9912))
						{	/* SawBbv/bbv-types.scm 695 */
							BgL_arg2913z00_9911 = BNIL;
						}
					else
						{	/* SawBbv/bbv-types.scm 695 */
							obj_t BgL_head1722z00_9913;

							BgL_head1722z00_9913 =
								MAKE_YOUNG_PAIR(BGl_lblze71ze7zzsaw_bbvzd2typeszd2(CAR
									(BgL_l1720z00_9912)), BNIL);
							{	/* SawBbv/bbv-types.scm 695 */
								obj_t BgL_g1725z00_9914;

								BgL_g1725z00_9914 = CDR(BgL_l1720z00_9912);
								{
									obj_t BgL_l1720z00_9916;
									obj_t BgL_tail1723z00_9917;

									BgL_l1720z00_9916 = BgL_g1725z00_9914;
									BgL_tail1723z00_9917 = BgL_head1722z00_9913;
								BgL_zc3z04anonymousza32915ze3z87_9915:
									if (NULLP(BgL_l1720z00_9916))
										{	/* SawBbv/bbv-types.scm 695 */
											BgL_arg2913z00_9911 = BgL_head1722z00_9913;
										}
									else
										{	/* SawBbv/bbv-types.scm 695 */
											obj_t BgL_newtail1724z00_9918;

											{	/* SawBbv/bbv-types.scm 695 */
												obj_t BgL_arg2918z00_9919;

												{	/* SawBbv/bbv-types.scm 695 */
													obj_t BgL_arg2919z00_9920;

													BgL_arg2919z00_9920 =
														CAR(((obj_t) BgL_l1720z00_9916));
													BgL_arg2918z00_9919 =
														BGl_lblze71ze7zzsaw_bbvzd2typeszd2
														(BgL_arg2919z00_9920);
												}
												BgL_newtail1724z00_9918 =
													MAKE_YOUNG_PAIR(BgL_arg2918z00_9919, BNIL);
											}
											SET_CDR(BgL_tail1723z00_9917, BgL_newtail1724z00_9918);
											{	/* SawBbv/bbv-types.scm 695 */
												obj_t BgL_arg2917z00_9921;

												BgL_arg2917z00_9921 = CDR(((obj_t) BgL_l1720z00_9916));
												{
													obj_t BgL_tail1723z00_15576;
													obj_t BgL_l1720z00_15575;

													BgL_l1720z00_15575 = BgL_arg2917z00_9921;
													BgL_tail1723z00_15576 = BgL_newtail1724z00_9918;
													BgL_tail1723z00_9917 = BgL_tail1723z00_15576;
													BgL_l1720z00_9916 = BgL_l1720z00_15575;
													goto BgL_zc3z04anonymousza32915ze3z87_9915;
												}
											}
										}
								}
							}
						}
				}
				bgl_display_obj(BgL_arg2913z00_9911, BgL_pz00_8832);
			}
			{	/* SawBbv/bbv-types.scm 695 */
				obj_t BgL_tmpz00_15578;

				BgL_tmpz00_15578 = ((obj_t) BgL_pz00_8832);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15578);
			}
			{	/* SawBbv/bbv-types.scm 696 */
				long BgL_arg2924z00_9922;

				BgL_arg2924z00_9922 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
					(int) (BgL_arg2924z00_9922));
			}
			{	/* SawBbv/bbv-types.scm 697 */
				obj_t BgL_tmpz00_15585;

				BgL_tmpz00_15585 = ((obj_t) BgL_pz00_8832);
				bgl_display_string(BGl_string3869z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15585);
			}
			{	/* SawBbv/bbv-types.scm 697 */
				obj_t BgL_arg2925z00_9923;

				{	/* SawBbv/bbv-types.scm 697 */
					obj_t BgL_l1727z00_9924;

					BgL_l1727z00_9924 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_oz00_8831))))->BgL_succsz00);
					if (NULLP(BgL_l1727z00_9924))
						{	/* SawBbv/bbv-types.scm 697 */
							BgL_arg2925z00_9923 = BNIL;
						}
					else
						{	/* SawBbv/bbv-types.scm 697 */
							obj_t BgL_head1729z00_9925;

							BgL_head1729z00_9925 =
								MAKE_YOUNG_PAIR(BGl_lblze71ze7zzsaw_bbvzd2typeszd2(CAR
									(BgL_l1727z00_9924)), BNIL);
							{	/* SawBbv/bbv-types.scm 697 */
								obj_t BgL_g1732z00_9926;

								BgL_g1732z00_9926 = CDR(BgL_l1727z00_9924);
								{
									obj_t BgL_l1727z00_9928;
									obj_t BgL_tail1730z00_9929;

									BgL_l1727z00_9928 = BgL_g1732z00_9926;
									BgL_tail1730z00_9929 = BgL_head1729z00_9925;
								BgL_zc3z04anonymousza32927ze3z87_9927:
									if (NULLP(BgL_l1727z00_9928))
										{	/* SawBbv/bbv-types.scm 697 */
											BgL_arg2925z00_9923 = BgL_head1729z00_9925;
										}
									else
										{	/* SawBbv/bbv-types.scm 697 */
											obj_t BgL_newtail1731z00_9930;

											{	/* SawBbv/bbv-types.scm 697 */
												obj_t BgL_arg2930z00_9931;

												{	/* SawBbv/bbv-types.scm 697 */
													obj_t BgL_arg2931z00_9932;

													BgL_arg2931z00_9932 =
														CAR(((obj_t) BgL_l1727z00_9928));
													BgL_arg2930z00_9931 =
														BGl_lblze71ze7zzsaw_bbvzd2typeszd2
														(BgL_arg2931z00_9932);
												}
												BgL_newtail1731z00_9930 =
													MAKE_YOUNG_PAIR(BgL_arg2930z00_9931, BNIL);
											}
											SET_CDR(BgL_tail1730z00_9929, BgL_newtail1731z00_9930);
											{	/* SawBbv/bbv-types.scm 697 */
												obj_t BgL_arg2929z00_9933;

												BgL_arg2929z00_9933 = CDR(((obj_t) BgL_l1727z00_9928));
												{
													obj_t BgL_tail1730z00_15607;
													obj_t BgL_l1727z00_15606;

													BgL_l1727z00_15606 = BgL_arg2929z00_9933;
													BgL_tail1730z00_15607 = BgL_newtail1731z00_9930;
													BgL_tail1730z00_9929 = BgL_tail1730z00_15607;
													BgL_l1727z00_9928 = BgL_l1727z00_15606;
													goto BgL_zc3z04anonymousza32927ze3z87_9927;
												}
											}
										}
								}
							}
						}
				}
				bgl_display_obj(BgL_arg2925z00_9923, BgL_pz00_8832);
			}
			{	/* SawBbv/bbv-types.scm 697 */
				obj_t BgL_tmpz00_15609;

				BgL_tmpz00_15609 = ((obj_t) BgL_pz00_8832);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15609);
			}
			{	/* SawBbv/bbv-types.scm 698 */
				long BgL_arg2934z00_9934;

				BgL_arg2934z00_9934 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
					(int) (BgL_arg2934z00_9934));
			}
			{	/* SawBbv/bbv-types.scm 699 */
				obj_t BgL_tmpz00_15616;

				BgL_tmpz00_15616 = ((obj_t) BgL_pz00_8832);
				bgl_display_string(BGl_string3870z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15616);
			}
			{	/* SawBbv/bbv-types.scm 699 */
				obj_t BgL_arg2935z00_9935;

				{	/* SawBbv/bbv-types.scm 699 */
					BgL_bbvzd2ctxzd2_bglt BgL_arg2936z00_9936;

					{
						BgL_blocksz00_bglt BgL_auxz00_15619;

						{
							obj_t BgL_auxz00_15620;

							{	/* SawBbv/bbv-types.scm 699 */
								BgL_objectz00_bglt BgL_tmpz00_15621;

								BgL_tmpz00_15621 =
									((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8831));
								BgL_auxz00_15620 = BGL_OBJECT_WIDENING(BgL_tmpz00_15621);
							}
							BgL_auxz00_15619 = ((BgL_blocksz00_bglt) BgL_auxz00_15620);
						}
						BgL_arg2936z00_9936 =
							(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15619))->BgL_ctxz00);
					}
					BgL_arg2935z00_9935 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2936z00_9936));
				}
				bgl_display_obj(BgL_arg2935z00_9935, BgL_pz00_8832);
			}
			{	/* SawBbv/bbv-types.scm 699 */
				obj_t BgL_tmpz00_15630;

				BgL_tmpz00_15630 = ((obj_t) BgL_pz00_8832);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15630);
			}
			if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
				{	/* SawBbv/bbv-types.scm 700 */
					{	/* SawBbv/bbv-types.scm 701 */
						long BgL_arg2940z00_9937;

						BgL_arg2940z00_9937 = ((long) CINT(BgL_mz00_8833) + 1L);
						BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
							(int) (BgL_arg2940z00_9937));
					}
					{	/* SawBbv/bbv-types.scm 702 */
						obj_t BgL_tmpz00_15639;

						BgL_tmpz00_15639 = ((obj_t) BgL_pz00_8832);
						bgl_display_string(BGl_string3871z00zzsaw_bbvzd2typeszd2,
							BgL_tmpz00_15639);
					}
					{	/* SawBbv/bbv-types.scm 702 */
						long BgL_arg2941z00_9938;

						{
							BgL_blocksz00_bglt BgL_auxz00_15642;

							{
								obj_t BgL_auxz00_15643;

								{	/* SawBbv/bbv-types.scm 702 */
									BgL_objectz00_bglt BgL_tmpz00_15644;

									BgL_tmpz00_15644 =
										((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8831));
									BgL_auxz00_15643 = BGL_OBJECT_WIDENING(BgL_tmpz00_15644);
								}
								BgL_auxz00_15642 = ((BgL_blocksz00_bglt) BgL_auxz00_15643);
							}
							BgL_arg2941z00_9938 =
								(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15642))->
								BgL_gccntz00);
						}
						bgl_display_obj(BINT(BgL_arg2941z00_9938), BgL_pz00_8832);
					}
					{	/* SawBbv/bbv-types.scm 702 */
						obj_t BgL_tmpz00_15652;

						BgL_tmpz00_15652 = ((obj_t) BgL_pz00_8832);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_15652);
					}
					{	/* SawBbv/bbv-types.scm 703 */
						long BgL_arg2942z00_9939;

						BgL_arg2942z00_9939 = ((long) CINT(BgL_mz00_8833) + 1L);
						BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
							(int) (BgL_arg2942z00_9939));
					}
					{	/* SawBbv/bbv-types.scm 704 */
						obj_t BgL_tmpz00_15659;

						BgL_tmpz00_15659 = ((obj_t) BgL_pz00_8832);
						bgl_display_string(BGl_string3872z00zzsaw_bbvzd2typeszd2,
							BgL_tmpz00_15659);
					}
					{	/* SawBbv/bbv-types.scm 704 */
						bool_t BgL_arg2943z00_9940;

						{
							BgL_blocksz00_bglt BgL_auxz00_15662;

							{
								obj_t BgL_auxz00_15663;

								{	/* SawBbv/bbv-types.scm 704 */
									BgL_objectz00_bglt BgL_tmpz00_15664;

									BgL_tmpz00_15664 =
										((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8831));
									BgL_auxz00_15663 = BGL_OBJECT_WIDENING(BgL_tmpz00_15664);
								}
								BgL_auxz00_15662 = ((BgL_blocksz00_bglt) BgL_auxz00_15663);
							}
							BgL_arg2943z00_9940 =
								(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_15662))->
								BgL_asleepz00);
						}
						bgl_display_obj(BBOOL(BgL_arg2943z00_9940), BgL_pz00_8832);
					}
					{	/* SawBbv/bbv-types.scm 704 */
						obj_t BgL_tmpz00_15672;

						BgL_tmpz00_15672 = ((obj_t) BgL_pz00_8832);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_15672);
				}}
			else
				{	/* SawBbv/bbv-types.scm 700 */
					BFALSE;
				}
			{	/* SawBbv/bbv-types.scm 705 */
				long BgL_arg2954z00_9941;

				BgL_arg2954z00_9941 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8832,
					(int) (BgL_arg2954z00_9941));
			}
			{	/* SawBbv/bbv-types.scm 706 */
				obj_t BgL_arg2955z00_9942;
				long BgL_arg2956z00_9943;

				BgL_arg2955z00_9942 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_oz00_8831))))->BgL_firstz00);
				BgL_arg2956z00_9943 = ((long) CINT(BgL_mz00_8833) + 1L);
				BGl_dumpza2za2zzsaw_defsz00(BgL_arg2955z00_9942, BgL_pz00_8832,
					(int) (BgL_arg2956z00_9943));
			}
			{	/* SawBbv/bbv-types.scm 707 */
				obj_t BgL_tmpz00_15686;

				BgL_tmpz00_15686 = ((obj_t) BgL_pz00_8832);
				return
					bgl_display_string(BGl_string3873z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15686);
			}
		}

	}



/* lbl~1 */
	obj_t BGl_lblze71ze7zzsaw_bbvzd2typeszd2(obj_t BgL_nz00_3849)
	{
		{	/* SawBbv/bbv-types.scm 680 */
			{	/* SawBbv/bbv-types.scm 680 */
				bool_t BgL_test4571z00_15689;

				{	/* SawBbv/bbv-types.scm 680 */
					obj_t BgL_classz00_6791;

					BgL_classz00_6791 = BGl_blockz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_nz00_3849))
						{	/* SawBbv/bbv-types.scm 680 */
							BgL_objectz00_bglt BgL_arg1807z00_6793;

							BgL_arg1807z00_6793 = (BgL_objectz00_bglt) (BgL_nz00_3849);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 680 */
									long BgL_idxz00_6799;

									BgL_idxz00_6799 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6793);
									BgL_test4571z00_15689 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6799 + 1L)) == BgL_classz00_6791);
								}
							else
								{	/* SawBbv/bbv-types.scm 680 */
									bool_t BgL_res3646z00_6824;

									{	/* SawBbv/bbv-types.scm 680 */
										obj_t BgL_oclassz00_6807;

										{	/* SawBbv/bbv-types.scm 680 */
											obj_t BgL_arg1815z00_6815;
											long BgL_arg1816z00_6816;

											BgL_arg1815z00_6815 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 680 */
												long BgL_arg1817z00_6817;

												BgL_arg1817z00_6817 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6793);
												BgL_arg1816z00_6816 =
													(BgL_arg1817z00_6817 - OBJECT_TYPE);
											}
											BgL_oclassz00_6807 =
												VECTOR_REF(BgL_arg1815z00_6815, BgL_arg1816z00_6816);
										}
										{	/* SawBbv/bbv-types.scm 680 */
											bool_t BgL__ortest_1115z00_6808;

											BgL__ortest_1115z00_6808 =
												(BgL_classz00_6791 == BgL_oclassz00_6807);
											if (BgL__ortest_1115z00_6808)
												{	/* SawBbv/bbv-types.scm 680 */
													BgL_res3646z00_6824 = BgL__ortest_1115z00_6808;
												}
											else
												{	/* SawBbv/bbv-types.scm 680 */
													long BgL_odepthz00_6809;

													{	/* SawBbv/bbv-types.scm 680 */
														obj_t BgL_arg1804z00_6810;

														BgL_arg1804z00_6810 = (BgL_oclassz00_6807);
														BgL_odepthz00_6809 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6810);
													}
													if ((1L < BgL_odepthz00_6809))
														{	/* SawBbv/bbv-types.scm 680 */
															obj_t BgL_arg1802z00_6812;

															{	/* SawBbv/bbv-types.scm 680 */
																obj_t BgL_arg1803z00_6813;

																BgL_arg1803z00_6813 = (BgL_oclassz00_6807);
																BgL_arg1802z00_6812 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6813,
																	1L);
															}
															BgL_res3646z00_6824 =
																(BgL_arg1802z00_6812 == BgL_classz00_6791);
														}
													else
														{	/* SawBbv/bbv-types.scm 680 */
															BgL_res3646z00_6824 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4571z00_15689 = BgL_res3646z00_6824;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 680 */
							BgL_test4571z00_15689 = ((bool_t) 0);
						}
				}
				if (BgL_test4571z00_15689)
					{	/* SawBbv/bbv-types.scm 680 */
						return
							BINT(
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_nz00_3849)))->BgL_labelz00));
					}
				else
					{	/* SawBbv/bbv-types.scm 680 */
						BGL_TAIL return bgl_typeof(BgL_nz00_3849);
					}
			}
		}

	}



/* &dump-blockV1828 */
	obj_t BGl_z62dumpzd2blockV1828zb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8834,
		obj_t BgL_oz00_8835, obj_t BgL_pz00_8836, obj_t BgL_mz00_8837)
	{
		{	/* SawBbv/bbv-types.scm 657 */
			{	/* SawBbv/bbv-types.scm 663 */
				obj_t BgL_tmpz00_15716;

				BgL_tmpz00_15716 = ((obj_t) BgL_pz00_8836);
				bgl_display_string(BGl_string3874z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15716);
			}
			{	/* SawBbv/bbv-types.scm 663 */
				int BgL_arg2871z00_9945;

				BgL_arg2871z00_9945 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_oz00_8835))))->BgL_labelz00);
				bgl_display_obj(BINT(BgL_arg2871z00_9945), BgL_pz00_8836);
			}
			{	/* SawBbv/bbv-types.scm 663 */
				obj_t BgL_tmpz00_15724;

				BgL_tmpz00_15724 = ((obj_t) BgL_pz00_8836);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15724);
			}
			{	/* SawBbv/bbv-types.scm 664 */
				long BgL_arg2872z00_9946;

				BgL_arg2872z00_9946 = ((long) CINT(BgL_mz00_8837) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8836,
					(int) (BgL_arg2872z00_9946));
			}
			{	/* SawBbv/bbv-types.scm 665 */
				obj_t BgL_tmpz00_15731;

				BgL_tmpz00_15731 = ((obj_t) BgL_pz00_8836);
				bgl_display_string(BGl_string3867z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15731);
			}
			{	/* SawBbv/bbv-types.scm 665 */
				obj_t BgL_arg2873z00_9947;

				{
					BgL_blockvz00_bglt BgL_auxz00_15734;

					{
						obj_t BgL_auxz00_15735;

						{	/* SawBbv/bbv-types.scm 665 */
							BgL_objectz00_bglt BgL_tmpz00_15736;

							BgL_tmpz00_15736 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_8835));
							BgL_auxz00_15735 = BGL_OBJECT_WIDENING(BgL_tmpz00_15736);
						}
						BgL_auxz00_15734 = ((BgL_blockvz00_bglt) BgL_auxz00_15735);
					}
					BgL_arg2873z00_9947 =
						(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_15734))->BgL_mergez00);
				}
				bgl_display_obj(BgL_arg2873z00_9947, BgL_pz00_8836);
			}
			{	/* SawBbv/bbv-types.scm 665 */
				obj_t BgL_tmpz00_15743;

				BgL_tmpz00_15743 = ((obj_t) BgL_pz00_8836);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15743);
			}
			{	/* SawBbv/bbv-types.scm 666 */
				long BgL_arg2874z00_9948;

				BgL_arg2874z00_9948 = ((long) CINT(BgL_mz00_8837) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8836,
					(int) (BgL_arg2874z00_9948));
			}
			{	/* SawBbv/bbv-types.scm 667 */
				obj_t BgL_tmpz00_15750;

				BgL_tmpz00_15750 = ((obj_t) BgL_pz00_8836);
				bgl_display_string(BGl_string3868z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15750);
			}
			{	/* SawBbv/bbv-types.scm 667 */
				obj_t BgL_arg2875z00_9949;

				{	/* SawBbv/bbv-types.scm 667 */
					obj_t BgL_l1703z00_9950;

					BgL_l1703z00_9950 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_oz00_8835))))->BgL_predsz00);
					if (NULLP(BgL_l1703z00_9950))
						{	/* SawBbv/bbv-types.scm 667 */
							BgL_arg2875z00_9949 = BNIL;
						}
					else
						{	/* SawBbv/bbv-types.scm 667 */
							obj_t BgL_head1705z00_9951;

							BgL_head1705z00_9951 =
								MAKE_YOUNG_PAIR(BGl_lblze70ze7zzsaw_bbvzd2typeszd2(CAR
									(BgL_l1703z00_9950)), BNIL);
							{	/* SawBbv/bbv-types.scm 667 */
								obj_t BgL_g1708z00_9952;

								BgL_g1708z00_9952 = CDR(BgL_l1703z00_9950);
								{
									obj_t BgL_l1703z00_9954;
									obj_t BgL_tail1706z00_9955;

									BgL_l1703z00_9954 = BgL_g1708z00_9952;
									BgL_tail1706z00_9955 = BgL_head1705z00_9951;
								BgL_zc3z04anonymousza32877ze3z87_9953:
									if (NULLP(BgL_l1703z00_9954))
										{	/* SawBbv/bbv-types.scm 667 */
											BgL_arg2875z00_9949 = BgL_head1705z00_9951;
										}
									else
										{	/* SawBbv/bbv-types.scm 667 */
											obj_t BgL_newtail1707z00_9956;

											{	/* SawBbv/bbv-types.scm 667 */
												obj_t BgL_arg2880z00_9957;

												{	/* SawBbv/bbv-types.scm 667 */
													obj_t BgL_arg2881z00_9958;

													BgL_arg2881z00_9958 =
														CAR(((obj_t) BgL_l1703z00_9954));
													BgL_arg2880z00_9957 =
														BGl_lblze70ze7zzsaw_bbvzd2typeszd2
														(BgL_arg2881z00_9958);
												}
												BgL_newtail1707z00_9956 =
													MAKE_YOUNG_PAIR(BgL_arg2880z00_9957, BNIL);
											}
											SET_CDR(BgL_tail1706z00_9955, BgL_newtail1707z00_9956);
											{	/* SawBbv/bbv-types.scm 667 */
												obj_t BgL_arg2879z00_9959;

												BgL_arg2879z00_9959 = CDR(((obj_t) BgL_l1703z00_9954));
												{
													obj_t BgL_tail1706z00_15772;
													obj_t BgL_l1703z00_15771;

													BgL_l1703z00_15771 = BgL_arg2879z00_9959;
													BgL_tail1706z00_15772 = BgL_newtail1707z00_9956;
													BgL_tail1706z00_9955 = BgL_tail1706z00_15772;
													BgL_l1703z00_9954 = BgL_l1703z00_15771;
													goto BgL_zc3z04anonymousza32877ze3z87_9953;
												}
											}
										}
								}
							}
						}
				}
				bgl_display_obj(BgL_arg2875z00_9949, BgL_pz00_8836);
			}
			{	/* SawBbv/bbv-types.scm 667 */
				obj_t BgL_tmpz00_15774;

				BgL_tmpz00_15774 = ((obj_t) BgL_pz00_8836);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15774);
			}
			{	/* SawBbv/bbv-types.scm 668 */
				long BgL_arg2887z00_9960;

				BgL_arg2887z00_9960 = ((long) CINT(BgL_mz00_8837) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8836,
					(int) (BgL_arg2887z00_9960));
			}
			{	/* SawBbv/bbv-types.scm 669 */
				obj_t BgL_tmpz00_15781;

				BgL_tmpz00_15781 = ((obj_t) BgL_pz00_8836);
				bgl_display_string(BGl_string3869z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15781);
			}
			{	/* SawBbv/bbv-types.scm 669 */
				obj_t BgL_arg2888z00_9961;

				{	/* SawBbv/bbv-types.scm 669 */
					obj_t BgL_l1710z00_9962;

					BgL_l1710z00_9962 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_oz00_8835))))->BgL_succsz00);
					if (NULLP(BgL_l1710z00_9962))
						{	/* SawBbv/bbv-types.scm 669 */
							BgL_arg2888z00_9961 = BNIL;
						}
					else
						{	/* SawBbv/bbv-types.scm 669 */
							obj_t BgL_head1712z00_9963;

							BgL_head1712z00_9963 =
								MAKE_YOUNG_PAIR(BGl_lblze70ze7zzsaw_bbvzd2typeszd2(CAR
									(BgL_l1710z00_9962)), BNIL);
							{	/* SawBbv/bbv-types.scm 669 */
								obj_t BgL_g1715z00_9964;

								BgL_g1715z00_9964 = CDR(BgL_l1710z00_9962);
								{
									obj_t BgL_l1710z00_9966;
									obj_t BgL_tail1713z00_9967;

									BgL_l1710z00_9966 = BgL_g1715z00_9964;
									BgL_tail1713z00_9967 = BgL_head1712z00_9963;
								BgL_zc3z04anonymousza32890ze3z87_9965:
									if (NULLP(BgL_l1710z00_9966))
										{	/* SawBbv/bbv-types.scm 669 */
											BgL_arg2888z00_9961 = BgL_head1712z00_9963;
										}
									else
										{	/* SawBbv/bbv-types.scm 669 */
											obj_t BgL_newtail1714z00_9968;

											{	/* SawBbv/bbv-types.scm 669 */
												obj_t BgL_arg2893z00_9969;

												{	/* SawBbv/bbv-types.scm 669 */
													obj_t BgL_arg2894z00_9970;

													BgL_arg2894z00_9970 =
														CAR(((obj_t) BgL_l1710z00_9966));
													BgL_arg2893z00_9969 =
														BGl_lblze70ze7zzsaw_bbvzd2typeszd2
														(BgL_arg2894z00_9970);
												}
												BgL_newtail1714z00_9968 =
													MAKE_YOUNG_PAIR(BgL_arg2893z00_9969, BNIL);
											}
											SET_CDR(BgL_tail1713z00_9967, BgL_newtail1714z00_9968);
											{	/* SawBbv/bbv-types.scm 669 */
												obj_t BgL_arg2892z00_9971;

												BgL_arg2892z00_9971 = CDR(((obj_t) BgL_l1710z00_9966));
												{
													obj_t BgL_tail1713z00_15803;
													obj_t BgL_l1710z00_15802;

													BgL_l1710z00_15802 = BgL_arg2892z00_9971;
													BgL_tail1713z00_15803 = BgL_newtail1714z00_9968;
													BgL_tail1713z00_9967 = BgL_tail1713z00_15803;
													BgL_l1710z00_9966 = BgL_l1710z00_15802;
													goto BgL_zc3z04anonymousza32890ze3z87_9965;
												}
											}
										}
								}
							}
						}
				}
				bgl_display_obj(BgL_arg2888z00_9961, BgL_pz00_8836);
			}
			{	/* SawBbv/bbv-types.scm 669 */
				obj_t BgL_tmpz00_15805;

				BgL_tmpz00_15805 = ((obj_t) BgL_pz00_8836);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15805);
			}
			{	/* SawBbv/bbv-types.scm 670 */
				long BgL_arg2897z00_9972;

				BgL_arg2897z00_9972 = ((long) CINT(BgL_mz00_8837) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8836,
					(int) (BgL_arg2897z00_9972));
			}
			{	/* SawBbv/bbv-types.scm 671 */
				obj_t BgL_arg2898z00_9973;
				long BgL_arg2899z00_9974;

				BgL_arg2898z00_9973 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_oz00_8835))))->BgL_firstz00);
				BgL_arg2899z00_9974 = ((long) CINT(BgL_mz00_8837) + 1L);
				BGl_dumpza2za2zzsaw_defsz00(BgL_arg2898z00_9973, BgL_pz00_8836,
					(int) (BgL_arg2899z00_9974));
			}
			{	/* SawBbv/bbv-types.scm 672 */
				obj_t BgL_tmpz00_15819;

				BgL_tmpz00_15819 = ((obj_t) BgL_pz00_8836);
				return
					bgl_display_string(BGl_string3873z00zzsaw_bbvzd2typeszd2,
					BgL_tmpz00_15819);
			}
		}

	}



/* lbl~0 */
	obj_t BGl_lblze70ze7zzsaw_bbvzd2typeszd2(obj_t BgL_nz00_3775)
	{
		{	/* SawBbv/bbv-types.scm 660 */
			{	/* SawBbv/bbv-types.scm 660 */
				bool_t BgL_test4580z00_15822;

				{	/* SawBbv/bbv-types.scm 660 */
					obj_t BgL_classz00_6726;

					BgL_classz00_6726 = BGl_blockz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_nz00_3775))
						{	/* SawBbv/bbv-types.scm 660 */
							BgL_objectz00_bglt BgL_arg1807z00_6728;

							BgL_arg1807z00_6728 = (BgL_objectz00_bglt) (BgL_nz00_3775);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-types.scm 660 */
									long BgL_idxz00_6734;

									BgL_idxz00_6734 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6728);
									BgL_test4580z00_15822 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6734 + 1L)) == BgL_classz00_6726);
								}
							else
								{	/* SawBbv/bbv-types.scm 660 */
									bool_t BgL_res3645z00_6759;

									{	/* SawBbv/bbv-types.scm 660 */
										obj_t BgL_oclassz00_6742;

										{	/* SawBbv/bbv-types.scm 660 */
											obj_t BgL_arg1815z00_6750;
											long BgL_arg1816z00_6751;

											BgL_arg1815z00_6750 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-types.scm 660 */
												long BgL_arg1817z00_6752;

												BgL_arg1817z00_6752 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6728);
												BgL_arg1816z00_6751 =
													(BgL_arg1817z00_6752 - OBJECT_TYPE);
											}
											BgL_oclassz00_6742 =
												VECTOR_REF(BgL_arg1815z00_6750, BgL_arg1816z00_6751);
										}
										{	/* SawBbv/bbv-types.scm 660 */
											bool_t BgL__ortest_1115z00_6743;

											BgL__ortest_1115z00_6743 =
												(BgL_classz00_6726 == BgL_oclassz00_6742);
											if (BgL__ortest_1115z00_6743)
												{	/* SawBbv/bbv-types.scm 660 */
													BgL_res3645z00_6759 = BgL__ortest_1115z00_6743;
												}
											else
												{	/* SawBbv/bbv-types.scm 660 */
													long BgL_odepthz00_6744;

													{	/* SawBbv/bbv-types.scm 660 */
														obj_t BgL_arg1804z00_6745;

														BgL_arg1804z00_6745 = (BgL_oclassz00_6742);
														BgL_odepthz00_6744 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6745);
													}
													if ((1L < BgL_odepthz00_6744))
														{	/* SawBbv/bbv-types.scm 660 */
															obj_t BgL_arg1802z00_6747;

															{	/* SawBbv/bbv-types.scm 660 */
																obj_t BgL_arg1803z00_6748;

																BgL_arg1803z00_6748 = (BgL_oclassz00_6742);
																BgL_arg1802z00_6747 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6748,
																	1L);
															}
															BgL_res3645z00_6759 =
																(BgL_arg1802z00_6747 == BgL_classz00_6726);
														}
													else
														{	/* SawBbv/bbv-types.scm 660 */
															BgL_res3645z00_6759 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test4580z00_15822 = BgL_res3645z00_6759;
								}
						}
					else
						{	/* SawBbv/bbv-types.scm 660 */
							BgL_test4580z00_15822 = ((bool_t) 0);
						}
				}
				if (BgL_test4580z00_15822)
					{	/* SawBbv/bbv-types.scm 660 */
						return
							BINT(
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_nz00_3775)))->BgL_labelz00));
					}
				else
					{	/* SawBbv/bbv-types.scm 660 */
						BGL_TAIL return bgl_typeof(BgL_nz00_3775);
					}
			}
		}

	}



/* &dump-rtl_ins/bbv1826 */
	obj_t BGl_z62dumpzd2rtl_inszf2bbv1826z42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8838, obj_t BgL_oz00_8839, obj_t BgL_pz00_8840,
		obj_t BgL_mz00_8841)
	{
		{	/* SawBbv/bbv-types.scm 632 */
			{	/* SawBbv/bbv-types.scm 636 */
				obj_t BgL_zc3z04anonymousza32790ze3z87_9976;

				BgL_zc3z04anonymousza32790ze3z87_9976 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza32790ze3ze5zzsaw_bbvzd2typeszd2, (int) (0L),
					(int) (4L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32790ze3z87_9976, (int) (0L),
					BgL_pz00_8840);
				PROCEDURE_SET(BgL_zc3z04anonymousza32790ze3z87_9976, (int) (1L),
					((obj_t) ((BgL_rtl_insz00_bglt) BgL_oz00_8839)));
				PROCEDURE_SET(BgL_zc3z04anonymousza32790ze3z87_9976, (int) (2L),
					BgL_mz00_8841);
				PROCEDURE_SET(BgL_zc3z04anonymousza32790ze3z87_9976, (int) (3L),
					((obj_t) ((BgL_rtl_insz00_bglt) BgL_oz00_8839)));
				return
					BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(BgL_pz00_8840,
					BgL_zc3z04anonymousza32790ze3z87_9976);
			}
		}

	}



/* &<@anonymous:2790> */
	obj_t BGl_z62zc3z04anonymousza32790ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8842)
	{
		{	/* SawBbv/bbv-types.scm 635 */
			{	/* SawBbv/bbv-types.scm 636 */
				obj_t BgL_pz00_8843;
				BgL_rtl_insz00_bglt BgL_i1327z00_8844;
				obj_t BgL_mz00_8845;
				BgL_rtl_insz00_bglt BgL_oz00_8846;

				BgL_pz00_8843 = PROCEDURE_REF(BgL_envz00_8842, (int) (0L));
				BgL_i1327z00_8844 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_8842, (int) (1L)));
				BgL_mz00_8845 = PROCEDURE_REF(BgL_envz00_8842, (int) (2L));
				BgL_oz00_8846 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_8842, (int) (3L)));
				{	/* SawBbv/bbv-types.scm 636 */
					obj_t BgL_tmpz00_15875;

					BgL_tmpz00_15875 = ((obj_t) BgL_pz00_8843);
					bgl_display_string(BGl_string3875z00zzsaw_bbvzd2typeszd2,
						BgL_tmpz00_15875);
				}
				if (CBOOL(
						(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_i1327z00_8844)))->BgL_destz00)))
					{	/* SawBbv/bbv-types.scm 637 */
						{	/* SawBbv/bbv-types.scm 638 */
							obj_t BgL_tmpz00_15882;

							BgL_tmpz00_15882 = ((obj_t) BgL_pz00_8843);
							bgl_display_string(BGl_string3876z00zzsaw_bbvzd2typeszd2,
								BgL_tmpz00_15882);
						}
						{	/* SawBbv/bbv-types.scm 639 */
							obj_t BgL_arg2793z00_9977;

							BgL_arg2793z00_9977 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_i1327z00_8844)))->BgL_destz00);
							BGl_dumpz00zzsaw_defsz00(BgL_arg2793z00_9977, BgL_pz00_8843,
								CINT(BgL_mz00_8845));
						}
						{	/* SawBbv/bbv-types.scm 640 */
							obj_t BgL_tmpz00_15889;

							BgL_tmpz00_15889 = ((obj_t) BgL_pz00_8843);
							bgl_display_string(BGl_string3877z00zzsaw_bbvzd2typeszd2,
								BgL_tmpz00_15889);
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 637 */
						BFALSE;
					}
				BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(
					((BgL_rtl_insz00_bglt) BgL_oz00_8846), BgL_pz00_8843, BgL_mz00_8845);
				if (CBOOL(
						(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_i1327z00_8844)))->BgL_destz00)))
					{	/* SawBbv/bbv-types.scm 643 */
						obj_t BgL_tmpz00_15898;

						BgL_tmpz00_15898 = ((obj_t) BgL_pz00_8843);
						bgl_display_string(BGl_string3878z00zzsaw_bbvzd2typeszd2,
							BgL_tmpz00_15898);
					}
				else
					{	/* SawBbv/bbv-types.scm 642 */
						BFALSE;
					}
				{	/* SawBbv/bbv-types.scm 644 */
					obj_t BgL_tmpz00_15901;

					BgL_tmpz00_15901 = ((obj_t) BgL_pz00_8843);
					bgl_display_string(BGl_string3879z00zzsaw_bbvzd2typeszd2,
						BgL_tmpz00_15901);
				}
				{	/* SawBbv/bbv-types.scm 645 */
					BgL_bbvzd2ctxzd2_bglt BgL_arg2799z00_9978;
					obj_t BgL_arg2800z00_9979;

					{
						BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15904;

						{
							obj_t BgL_auxz00_15905;

							{	/* SawBbv/bbv-types.scm 645 */
								BgL_objectz00_bglt BgL_tmpz00_15906;

								BgL_tmpz00_15906 = ((BgL_objectz00_bglt) BgL_i1327z00_8844);
								BgL_auxz00_15905 = BGL_OBJECT_WIDENING(BgL_tmpz00_15906);
							}
							BgL_auxz00_15904 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15905);
						}
						BgL_arg2799z00_9978 =
							(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15904))->
							BgL_ctxz00);
					}
					{
						BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15911;

						{
							obj_t BgL_auxz00_15912;

							{	/* SawBbv/bbv-types.scm 645 */
								BgL_objectz00_bglt BgL_tmpz00_15913;

								BgL_tmpz00_15913 = ((BgL_objectz00_bglt) BgL_i1327z00_8844);
								BgL_auxz00_15912 = BGL_OBJECT_WIDENING(BgL_tmpz00_15913);
							}
							BgL_auxz00_15911 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15912);
						}
						BgL_arg2800z00_9979 =
							(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15911))->
							BgL_inz00);
					}
					BGl_dumpzd2ctxzd2zzsaw_bbvzd2typeszd2(BgL_arg2799z00_9978,
						BgL_arg2800z00_9979, BgL_pz00_8843);
				}
				{	/* SawBbv/bbv-types.scm 646 */
					obj_t BgL_tmpz00_15919;

					BgL_tmpz00_15919 = ((obj_t) BgL_pz00_8843);
					bgl_display_string(BGl_string3880z00zzsaw_bbvzd2typeszd2,
						BgL_tmpz00_15919);
				}
				if (
					((long) CINT(BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2) >= 2L))
					{	/* SawBbv/bbv-types.scm 647 */
						{	/* SawBbv/bbv-types.scm 648 */
							obj_t BgL_arg2804z00_9980;

							{	/* SawBbv/bbv-types.scm 648 */
								obj_t BgL_tmpz00_15925;

								BgL_tmpz00_15925 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg2804z00_9980 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_15925);
							}
							bgl_display_string(BGl_string3881z00zzsaw_bbvzd2typeszd2,
								BgL_arg2804z00_9980);
						}
						{	/* SawBbv/bbv-types.scm 649 */
							obj_t BgL_arg2805z00_9981;

							{	/* SawBbv/bbv-types.scm 649 */
								obj_t BgL_l1683z00_9982;

								{	/* SawBbv/bbv-types.scm 649 */
									obj_t BgL_arg2821z00_9983;

									{
										BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15929;

										{
											obj_t BgL_auxz00_15930;

											{	/* SawBbv/bbv-types.scm 649 */
												BgL_objectz00_bglt BgL_tmpz00_15931;

												BgL_tmpz00_15931 =
													((BgL_objectz00_bglt) BgL_i1327z00_8844);
												BgL_auxz00_15930 =
													BGL_OBJECT_WIDENING(BgL_tmpz00_15931);
											}
											BgL_auxz00_15929 =
												((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15930);
										}
										BgL_arg2821z00_9983 =
											(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15929))->
											BgL_defz00);
									}
									BgL_l1683z00_9982 =
										BGl_regsetzd2ze3listz31zzsaw_regsetz00(
										((BgL_regsetz00_bglt) BgL_arg2821z00_9983));
								}
								if (NULLP(BgL_l1683z00_9982))
									{	/* SawBbv/bbv-types.scm 649 */
										BgL_arg2805z00_9981 = BNIL;
									}
								else
									{	/* SawBbv/bbv-types.scm 649 */
										obj_t BgL_head1685z00_9984;

										BgL_head1685z00_9984 =
											MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
												(BgL_l1683z00_9982)), BNIL);
										{	/* SawBbv/bbv-types.scm 649 */
											obj_t BgL_g1688z00_9985;

											BgL_g1688z00_9985 = CDR(BgL_l1683z00_9982);
											{
												obj_t BgL_l1683z00_9987;
												obj_t BgL_tail1686z00_9988;

												BgL_l1683z00_9987 = BgL_g1688z00_9985;
												BgL_tail1686z00_9988 = BgL_head1685z00_9984;
											BgL_zc3z04anonymousza32810ze3z87_9986:
												if (NULLP(BgL_l1683z00_9987))
													{	/* SawBbv/bbv-types.scm 649 */
														BgL_arg2805z00_9981 = BgL_head1685z00_9984;
													}
												else
													{	/* SawBbv/bbv-types.scm 649 */
														obj_t BgL_newtail1687z00_9989;

														{	/* SawBbv/bbv-types.scm 649 */
															obj_t BgL_arg2815z00_9990;

															{	/* SawBbv/bbv-types.scm 649 */
																obj_t BgL_arg2816z00_9991;

																BgL_arg2816z00_9991 =
																	CAR(((obj_t) BgL_l1683z00_9987));
																BgL_arg2815z00_9990 =
																	BGl_shapez00zztools_shapez00
																	(BgL_arg2816z00_9991);
															}
															BgL_newtail1687z00_9989 =
																MAKE_YOUNG_PAIR(BgL_arg2815z00_9990, BNIL);
														}
														SET_CDR(BgL_tail1686z00_9988,
															BgL_newtail1687z00_9989);
														{	/* SawBbv/bbv-types.scm 649 */
															obj_t BgL_arg2812z00_9992;

															BgL_arg2812z00_9992 =
																CDR(((obj_t) BgL_l1683z00_9987));
															{
																obj_t BgL_tail1686z00_15954;
																obj_t BgL_l1683z00_15953;

																BgL_l1683z00_15953 = BgL_arg2812z00_9992;
																BgL_tail1686z00_15954 = BgL_newtail1687z00_9989;
																BgL_tail1686z00_9988 = BgL_tail1686z00_15954;
																BgL_l1683z00_9987 = BgL_l1683z00_15953;
																goto BgL_zc3z04anonymousza32810ze3z87_9986;
															}
														}
													}
											}
										}
									}
							}
							{	/* SawBbv/bbv-types.scm 649 */
								obj_t BgL_list2806z00_9993;

								{	/* SawBbv/bbv-types.scm 649 */
									obj_t BgL_arg2808z00_9994;

									BgL_arg2808z00_9994 =
										MAKE_YOUNG_PAIR(BgL_arg2805z00_9981, BNIL);
									BgL_list2806z00_9993 =
										MAKE_YOUNG_PAIR(BGl_string3882z00zzsaw_bbvzd2typeszd2,
										BgL_arg2808z00_9994);
								}
								BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2806z00_9993);
							}
						}
						{	/* SawBbv/bbv-types.scm 650 */
							obj_t BgL_arg2823z00_9995;

							{	/* SawBbv/bbv-types.scm 650 */
								obj_t BgL_l1689z00_9996;

								{	/* SawBbv/bbv-types.scm 650 */
									obj_t BgL_arg2835z00_9997;

									{
										BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15958;

										{
											obj_t BgL_auxz00_15959;

											{	/* SawBbv/bbv-types.scm 650 */
												BgL_objectz00_bglt BgL_tmpz00_15960;

												BgL_tmpz00_15960 =
													((BgL_objectz00_bglt) BgL_i1327z00_8844);
												BgL_auxz00_15959 =
													BGL_OBJECT_WIDENING(BgL_tmpz00_15960);
											}
											BgL_auxz00_15958 =
												((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15959);
										}
										BgL_arg2835z00_9997 =
											(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15958))->
											BgL_inz00);
									}
									BgL_l1689z00_9996 =
										BGl_regsetzd2ze3listz31zzsaw_regsetz00(
										((BgL_regsetz00_bglt) BgL_arg2835z00_9997));
								}
								if (NULLP(BgL_l1689z00_9996))
									{	/* SawBbv/bbv-types.scm 650 */
										BgL_arg2823z00_9995 = BNIL;
									}
								else
									{	/* SawBbv/bbv-types.scm 650 */
										obj_t BgL_head1691z00_9998;

										BgL_head1691z00_9998 =
											MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
												(BgL_l1689z00_9996)), BNIL);
										{	/* SawBbv/bbv-types.scm 650 */
											obj_t BgL_g1694z00_9999;

											BgL_g1694z00_9999 = CDR(BgL_l1689z00_9996);
											{
												obj_t BgL_l1689z00_10001;
												obj_t BgL_tail1692z00_10002;

												BgL_l1689z00_10001 = BgL_g1694z00_9999;
												BgL_tail1692z00_10002 = BgL_head1691z00_9998;
											BgL_zc3z04anonymousza32828ze3z87_10000:
												if (NULLP(BgL_l1689z00_10001))
													{	/* SawBbv/bbv-types.scm 650 */
														BgL_arg2823z00_9995 = BgL_head1691z00_9998;
													}
												else
													{	/* SawBbv/bbv-types.scm 650 */
														obj_t BgL_newtail1693z00_10003;

														{	/* SawBbv/bbv-types.scm 650 */
															obj_t BgL_arg2831z00_10004;

															{	/* SawBbv/bbv-types.scm 650 */
																obj_t BgL_arg2832z00_10005;

																BgL_arg2832z00_10005 =
																	CAR(((obj_t) BgL_l1689z00_10001));
																BgL_arg2831z00_10004 =
																	BGl_shapez00zztools_shapez00
																	(BgL_arg2832z00_10005);
															}
															BgL_newtail1693z00_10003 =
																MAKE_YOUNG_PAIR(BgL_arg2831z00_10004, BNIL);
														}
														SET_CDR(BgL_tail1692z00_10002,
															BgL_newtail1693z00_10003);
														{	/* SawBbv/bbv-types.scm 650 */
															obj_t BgL_arg2830z00_10006;

															BgL_arg2830z00_10006 =
																CDR(((obj_t) BgL_l1689z00_10001));
															{
																obj_t BgL_tail1692z00_15983;
																obj_t BgL_l1689z00_15982;

																BgL_l1689z00_15982 = BgL_arg2830z00_10006;
																BgL_tail1692z00_15983 =
																	BgL_newtail1693z00_10003;
																BgL_tail1692z00_10002 = BgL_tail1692z00_15983;
																BgL_l1689z00_10001 = BgL_l1689z00_15982;
																goto BgL_zc3z04anonymousza32828ze3z87_10000;
															}
														}
													}
											}
										}
									}
							}
							{	/* SawBbv/bbv-types.scm 650 */
								obj_t BgL_list2824z00_10007;

								{	/* SawBbv/bbv-types.scm 650 */
									obj_t BgL_arg2826z00_10008;

									BgL_arg2826z00_10008 =
										MAKE_YOUNG_PAIR(BgL_arg2823z00_9995, BNIL);
									BgL_list2824z00_10007 =
										MAKE_YOUNG_PAIR(BGl_string3883z00zzsaw_bbvzd2typeszd2,
										BgL_arg2826z00_10008);
								}
								BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2824z00_10007);
							}
						}
						{	/* SawBbv/bbv-types.scm 651 */
							obj_t BgL_arg2836z00_10009;

							{	/* SawBbv/bbv-types.scm 651 */
								obj_t BgL_l1695z00_10010;

								{	/* SawBbv/bbv-types.scm 651 */
									obj_t BgL_arg2858z00_10011;

									{
										BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_15987;

										{
											obj_t BgL_auxz00_15988;

											{	/* SawBbv/bbv-types.scm 651 */
												BgL_objectz00_bglt BgL_tmpz00_15989;

												BgL_tmpz00_15989 =
													((BgL_objectz00_bglt) BgL_i1327z00_8844);
												BgL_auxz00_15988 =
													BGL_OBJECT_WIDENING(BgL_tmpz00_15989);
											}
											BgL_auxz00_15987 =
												((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_15988);
										}
										BgL_arg2858z00_10011 =
											(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_15987))->
											BgL_outz00);
									}
									BgL_l1695z00_10010 =
										BGl_regsetzd2ze3listz31zzsaw_regsetz00(
										((BgL_regsetz00_bglt) BgL_arg2858z00_10011));
								}
								if (NULLP(BgL_l1695z00_10010))
									{	/* SawBbv/bbv-types.scm 651 */
										BgL_arg2836z00_10009 = BNIL;
									}
								else
									{	/* SawBbv/bbv-types.scm 651 */
										obj_t BgL_head1697z00_10012;

										BgL_head1697z00_10012 =
											MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
												(BgL_l1695z00_10010)), BNIL);
										{	/* SawBbv/bbv-types.scm 651 */
											obj_t BgL_g1700z00_10013;

											BgL_g1700z00_10013 = CDR(BgL_l1695z00_10010);
											{
												obj_t BgL_l1695z00_10015;
												obj_t BgL_tail1698z00_10016;

												BgL_l1695z00_10015 = BgL_g1700z00_10013;
												BgL_tail1698z00_10016 = BgL_head1697z00_10012;
											BgL_zc3z04anonymousza32840ze3z87_10014:
												if (NULLP(BgL_l1695z00_10015))
													{	/* SawBbv/bbv-types.scm 651 */
														BgL_arg2836z00_10009 = BgL_head1697z00_10012;
													}
												else
													{	/* SawBbv/bbv-types.scm 651 */
														obj_t BgL_newtail1699z00_10017;

														{	/* SawBbv/bbv-types.scm 651 */
															obj_t BgL_arg2846z00_10018;

															{	/* SawBbv/bbv-types.scm 651 */
																obj_t BgL_arg2847z00_10019;

																BgL_arg2847z00_10019 =
																	CAR(((obj_t) BgL_l1695z00_10015));
																BgL_arg2846z00_10018 =
																	BGl_shapez00zztools_shapez00
																	(BgL_arg2847z00_10019);
															}
															BgL_newtail1699z00_10017 =
																MAKE_YOUNG_PAIR(BgL_arg2846z00_10018, BNIL);
														}
														SET_CDR(BgL_tail1698z00_10016,
															BgL_newtail1699z00_10017);
														{	/* SawBbv/bbv-types.scm 651 */
															obj_t BgL_arg2844z00_10020;

															BgL_arg2844z00_10020 =
																CDR(((obj_t) BgL_l1695z00_10015));
															{
																obj_t BgL_tail1698z00_16012;
																obj_t BgL_l1695z00_16011;

																BgL_l1695z00_16011 = BgL_arg2844z00_10020;
																BgL_tail1698z00_16012 =
																	BgL_newtail1699z00_10017;
																BgL_tail1698z00_10016 = BgL_tail1698z00_16012;
																BgL_l1695z00_10015 = BgL_l1695z00_16011;
																goto BgL_zc3z04anonymousza32840ze3z87_10014;
															}
														}
													}
											}
										}
									}
							}
							{	/* SawBbv/bbv-types.scm 651 */
								obj_t BgL_list2837z00_10021;

								{	/* SawBbv/bbv-types.scm 651 */
									obj_t BgL_arg2838z00_10022;

									BgL_arg2838z00_10022 =
										MAKE_YOUNG_PAIR(BgL_arg2836z00_10009, BNIL);
									BgL_list2837z00_10021 =
										MAKE_YOUNG_PAIR(BGl_string3884z00zzsaw_bbvzd2typeszd2,
										BgL_arg2838z00_10022);
								}
								BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2837z00_10021);
							}
						}
						{	/* SawBbv/bbv-types.scm 652 */
							obj_t BgL_arg2859z00_10023;

							{	/* SawBbv/bbv-types.scm 652 */
								BgL_rtl_funz00_bglt BgL_arg2870z00_10024;

								BgL_arg2870z00_10024 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_i1327z00_8844)))->BgL_funz00);
								BgL_arg2859z00_10023 =
									bgl_typeof(((obj_t) BgL_arg2870z00_10024));
							}
							{	/* SawBbv/bbv-types.scm 652 */
								obj_t BgL_list2860z00_10025;

								{	/* SawBbv/bbv-types.scm 652 */
									obj_t BgL_arg2864z00_10026;

									BgL_arg2864z00_10026 =
										MAKE_YOUNG_PAIR(BgL_arg2859z00_10023, BNIL);
									BgL_list2860z00_10025 =
										MAKE_YOUNG_PAIR(BGl_string3879z00zzsaw_bbvzd2typeszd2,
										BgL_arg2864z00_10026);
								}
								return
									BGl_displayza2za2zz__r4_output_6_10_3z00
									(BgL_list2860z00_10025);
							}
						}
					}
				else
					{	/* SawBbv/bbv-types.scm 647 */
						return BFALSE;
					}
			}
		}

	}



/* &shape-bbv-ctx1824 */
	obj_t BGl_z62shapezd2bbvzd2ctx1824z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8847, obj_t BgL_oz00_8848)
	{
		{	/* SawBbv/bbv-types.scm 612 */
			{	/* SawBbv/bbv-types.scm 614 */
				obj_t BgL_v1682z00_10028;

				BgL_v1682z00_10028 = create_vector(2L);
				{	/* SawBbv/bbv-types.scm 614 */
					long BgL_arg2776z00_10029;

					BgL_arg2776z00_10029 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_8848)))->BgL_idz00);
					VECTOR_SET(BgL_v1682z00_10028, 0L, BINT(BgL_arg2776z00_10029));
				}
				{	/* SawBbv/bbv-types.scm 614 */
					obj_t BgL_arg2777z00_10030;

					{	/* SawBbv/bbv-types.scm 614 */
						obj_t BgL_l1676z00_10031;

						BgL_l1676z00_10031 =
							(((BgL_bbvzd2ctxzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_8848)))->BgL_entriesz00);
						if (NULLP(BgL_l1676z00_10031))
							{	/* SawBbv/bbv-types.scm 614 */
								BgL_arg2777z00_10030 = BNIL;
							}
						else
							{	/* SawBbv/bbv-types.scm 614 */
								obj_t BgL_head1678z00_10032;

								BgL_head1678z00_10032 =
									MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
										(BgL_l1676z00_10031)), BNIL);
								{	/* SawBbv/bbv-types.scm 614 */
									obj_t BgL_g1681z00_10033;

									BgL_g1681z00_10033 = CDR(BgL_l1676z00_10031);
									{
										obj_t BgL_l1676z00_10035;
										obj_t BgL_tail1679z00_10036;

										BgL_l1676z00_10035 = BgL_g1681z00_10033;
										BgL_tail1679z00_10036 = BgL_head1678z00_10032;
									BgL_zc3z04anonymousza32779ze3z87_10034:
										if (NULLP(BgL_l1676z00_10035))
											{	/* SawBbv/bbv-types.scm 614 */
												BgL_arg2777z00_10030 = BgL_head1678z00_10032;
											}
										else
											{	/* SawBbv/bbv-types.scm 614 */
												obj_t BgL_newtail1680z00_10037;

												{	/* SawBbv/bbv-types.scm 614 */
													obj_t BgL_arg2783z00_10038;

													{	/* SawBbv/bbv-types.scm 614 */
														obj_t BgL_arg2784z00_10039;

														BgL_arg2784z00_10039 =
															CAR(((obj_t) BgL_l1676z00_10035));
														BgL_arg2783z00_10038 =
															BGl_shapez00zztools_shapez00
															(BgL_arg2784z00_10039);
													}
													BgL_newtail1680z00_10037 =
														MAKE_YOUNG_PAIR(BgL_arg2783z00_10038, BNIL);
												}
												SET_CDR(BgL_tail1679z00_10036,
													BgL_newtail1680z00_10037);
												{	/* SawBbv/bbv-types.scm 614 */
													obj_t BgL_arg2781z00_10040;

													BgL_arg2781z00_10040 =
														CDR(((obj_t) BgL_l1676z00_10035));
													{
														obj_t BgL_tail1679z00_16046;
														obj_t BgL_l1676z00_16045;

														BgL_l1676z00_16045 = BgL_arg2781z00_10040;
														BgL_tail1679z00_16046 = BgL_newtail1680z00_10037;
														BgL_tail1679z00_10036 = BgL_tail1679z00_16046;
														BgL_l1676z00_10035 = BgL_l1676z00_16045;
														goto BgL_zc3z04anonymousza32779ze3z87_10034;
													}
												}
											}
									}
								}
							}
					}
					VECTOR_SET(BgL_v1682z00_10028, 1L, BgL_arg2777z00_10030);
				}
				return BgL_v1682z00_10028;
			}
		}

	}



/* &shape-blockS1822 */
	obj_t BGl_z62shapezd2blockS1822zb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8849,
		obj_t BgL_oz00_8850)
	{
		{	/* SawBbv/bbv-types.scm 604 */
			{	/* SawBbv/bbv-types.scm 607 */
				obj_t BgL_zc3z04anonymousza32774ze3z87_10042;

				BgL_zc3z04anonymousza32774ze3z87_10042 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza32774ze3ze5zzsaw_bbvzd2typeszd2, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32774ze3z87_10042, (int) (0L),
					((obj_t) ((BgL_blockz00_bglt) BgL_oz00_8850)));
				return
					BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza32774ze3z87_10042);
			}
		}

	}



/* &<@anonymous:2774> */
	obj_t BGl_z62zc3z04anonymousza32774ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8851, obj_t BgL_pz00_8853)
	{
		{	/* SawBbv/bbv-types.scm 606 */
			{	/* SawBbv/bbv-types.scm 607 */
				BgL_blockz00_bglt BgL_oz00_8852;

				BgL_oz00_8852 =
					((BgL_blockz00_bglt) PROCEDURE_REF(BgL_envz00_8851, (int) (0L)));
				return
					BGl_dumpz00zzsaw_defsz00(
					((obj_t) BgL_oz00_8852), BgL_pz00_8853, (int) (0L));
		}}

	}



/* &shape-block1820 */
	obj_t BGl_z62shapezd2block1820zb0zzsaw_bbvzd2typeszd2(obj_t BgL_envz00_8854,
		obj_t BgL_oz00_8855)
	{
		{	/* SawBbv/bbv-types.scm 596 */
			{	/* SawBbv/bbv-types.scm 599 */
				obj_t BgL_zc3z04anonymousza32772ze3z87_10044;

				BgL_zc3z04anonymousza32772ze3z87_10044 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza32772ze3ze5zzsaw_bbvzd2typeszd2, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32772ze3z87_10044, (int) (0L),
					((obj_t) ((BgL_blockz00_bglt) BgL_oz00_8855)));
				return
					BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza32772ze3z87_10044);
			}
		}

	}



/* &<@anonymous:2772> */
	obj_t BGl_z62zc3z04anonymousza32772ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8856, obj_t BgL_pz00_8858)
	{
		{	/* SawBbv/bbv-types.scm 598 */
			{	/* SawBbv/bbv-types.scm 599 */
				BgL_blockz00_bglt BgL_oz00_8857;

				BgL_oz00_8857 =
					((BgL_blockz00_bglt) PROCEDURE_REF(BgL_envz00_8856, (int) (0L)));
				return
					BGl_dumpz00zzsaw_defsz00(
					((obj_t) BgL_oz00_8857), BgL_pz00_8858, (int) (0L));
		}}

	}



/* &shape-rtl_ins/bbv1818 */
	obj_t BGl_z62shapezd2rtl_inszf2bbv1818z42zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8859, obj_t BgL_oz00_8860)
	{
		{	/* SawBbv/bbv-types.scm 588 */
			{	/* SawBbv/bbv-types.scm 591 */
				obj_t BgL_zc3z04anonymousza32767ze3z87_10046;

				BgL_zc3z04anonymousza32767ze3z87_10046 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza32767ze3ze5zzsaw_bbvzd2typeszd2, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32767ze3z87_10046, (int) (0L),
					((obj_t) ((BgL_rtl_insz00_bglt) BgL_oz00_8860)));
				return
					BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza32767ze3z87_10046);
			}
		}

	}



/* &<@anonymous:2767> */
	obj_t BGl_z62zc3z04anonymousza32767ze3ze5zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8861, obj_t BgL_pz00_8863)
	{
		{	/* SawBbv/bbv-types.scm 590 */
			{	/* SawBbv/bbv-types.scm 591 */
				BgL_rtl_insz00_bglt BgL_oz00_8862;

				BgL_oz00_8862 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_8861, (int) (0L)));
				return
					BGl_dumpz00zzsaw_defsz00(
					((obj_t) BgL_oz00_8862), BgL_pz00_8863, (int) (0L));
		}}

	}



/* &shape-bbv-ctxentry1782 */
	obj_t BGl_z62shapezd2bbvzd2ctxentry1782z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8864, obj_t BgL_ez00_8865)
	{
		{	/* SawBbv/bbv-types.scm 181 */
			{	/* SawBbv/bbv-types.scm 183 */
				bool_t BgL_test4596z00_16090;

				if (
					(bgl_list_length(
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
								BgL_typesz00)) == 1L))
					{	/* SawBbv/bbv-types.scm 184 */
						bool_t BgL_test4598z00_16096;

						{	/* SawBbv/bbv-types.scm 184 */
							obj_t BgL_arg2761z00_10048;

							BgL_arg2761z00_10048 =
								CAR(
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
									BgL_typesz00));
							BgL_test4598z00_16096 =
								(BgL_arg2761z00_10048 == BGl_za2objza2z00zztype_cachez00);
						}
						if (BgL_test4598z00_16096)
							{	/* SawBbv/bbv-types.scm 184 */
								if (
									((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
											BgL_valuez00) == CNST_TABLE_REF(2)))
									{	/* SawBbv/bbv-types.scm 185 */
										BgL_test4596z00_16090 =
											NULLP(
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
												BgL_aliasesz00));
									}
								else
									{	/* SawBbv/bbv-types.scm 185 */
										BgL_test4596z00_16090 = ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-types.scm 184 */
								BgL_test4596z00_16090 = ((bool_t) 0);
							}
					}
				else
					{	/* SawBbv/bbv-types.scm 183 */
						BgL_test4596z00_16090 = ((bool_t) 0);
					}
				if (BgL_test4596z00_16090)
					{	/* SawBbv/bbv-types.scm 187 */
						BgL_rtl_regz00_bglt BgL_arg2719z00_10049;

						BgL_arg2719z00_10049 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->BgL_regz00);
						return BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2719z00_10049));
					}
				else
					{	/* SawBbv/bbv-types.scm 188 */
						obj_t BgL_v1657z00_10050;

						BgL_v1657z00_10050 = create_vector(4L);
						{	/* SawBbv/bbv-types.scm 188 */
							obj_t BgL_arg2721z00_10051;

							{	/* SawBbv/bbv-types.scm 188 */
								BgL_rtl_regz00_bglt BgL_arg2722z00_10052;

								BgL_arg2722z00_10052 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
									BgL_regz00);
								BgL_arg2721z00_10051 =
									BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2722z00_10052));
							}
							VECTOR_SET(BgL_v1657z00_10050, 0L, BgL_arg2721z00_10051);
						}
						{	/* SawBbv/bbv-types.scm 190 */
							obj_t BgL_arg2723z00_10053;

							{	/* SawBbv/bbv-types.scm 190 */
								obj_t BgL_arg2724z00_10054;

								if (
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
										BgL_polarityz00))
									{	/* SawBbv/bbv-types.scm 191 */
										obj_t BgL_l1641z00_10055;

										BgL_l1641z00_10055 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
											BgL_typesz00);
										{	/* SawBbv/bbv-types.scm 191 */
											obj_t BgL_head1643z00_10056;

											BgL_head1643z00_10056 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1641z00_10058;
												obj_t BgL_tail1644z00_10059;

												BgL_l1641z00_10058 = BgL_l1641z00_10055;
												BgL_tail1644z00_10059 = BgL_head1643z00_10056;
											BgL_zc3z04anonymousza32728ze3z87_10057:
												if (NULLP(BgL_l1641z00_10058))
													{	/* SawBbv/bbv-types.scm 191 */
														BgL_arg2724z00_10054 = CDR(BgL_head1643z00_10056);
													}
												else
													{	/* SawBbv/bbv-types.scm 191 */
														obj_t BgL_newtail1645z00_10060;

														{	/* SawBbv/bbv-types.scm 191 */
															obj_t BgL_arg2733z00_10061;

															{	/* SawBbv/bbv-types.scm 191 */
																obj_t BgL_tz00_10062;

																BgL_tz00_10062 =
																	CAR(((obj_t) BgL_l1641z00_10058));
																{	/* SawBbv/bbv-types.scm 191 */
																	obj_t BgL_arg2734z00_10063;
																	long BgL_arg2736z00_10064;

																	BgL_arg2734z00_10063 =
																		BGl_shapez00zztools_shapez00
																		(BgL_tz00_10062);
																	BgL_arg2736z00_10064 =
																		(((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																					BgL_ez00_8865)))->BgL_countz00);
																	{	/* SawBbv/bbv-types.scm 191 */
																		obj_t BgL_list2737z00_10065;

																		{	/* SawBbv/bbv-types.scm 191 */
																			obj_t BgL_arg2738z00_10066;

																			BgL_arg2738z00_10066 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg2736z00_10064), BNIL);
																			BgL_list2737z00_10065 =
																				MAKE_YOUNG_PAIR(BgL_arg2734z00_10063,
																				BgL_arg2738z00_10066);
																		}
																		BgL_arg2733z00_10061 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string3885z00zzsaw_bbvzd2typeszd2,
																			BgL_list2737z00_10065);
															}}}
															BgL_newtail1645z00_10060 =
																MAKE_YOUNG_PAIR(BgL_arg2733z00_10061, BNIL);
														}
														SET_CDR(BgL_tail1644z00_10059,
															BgL_newtail1645z00_10060);
														{	/* SawBbv/bbv-types.scm 191 */
															obj_t BgL_arg2731z00_10067;

															BgL_arg2731z00_10067 =
																CDR(((obj_t) BgL_l1641z00_10058));
															{
																obj_t BgL_tail1644z00_16142;
																obj_t BgL_l1641z00_16141;

																BgL_l1641z00_16141 = BgL_arg2731z00_10067;
																BgL_tail1644z00_16142 =
																	BgL_newtail1645z00_10060;
																BgL_tail1644z00_10059 = BgL_tail1644z00_16142;
																BgL_l1641z00_10058 = BgL_l1641z00_16141;
																goto BgL_zc3z04anonymousza32728ze3z87_10057;
															}
														}
													}
											}
										}
									}
								else
									{	/* SawBbv/bbv-types.scm 192 */
										obj_t BgL_l1646z00_10068;

										BgL_l1646z00_10068 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
											BgL_typesz00);
										{	/* SawBbv/bbv-types.scm 192 */
											obj_t BgL_head1648z00_10069;

											BgL_head1648z00_10069 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1646z00_10071;
												obj_t BgL_tail1649z00_10072;

												BgL_l1646z00_10071 = BgL_l1646z00_10068;
												BgL_tail1649z00_10072 = BgL_head1648z00_10069;
											BgL_zc3z04anonymousza32740ze3z87_10070:
												if (NULLP(BgL_l1646z00_10071))
													{	/* SawBbv/bbv-types.scm 192 */
														BgL_arg2724z00_10054 = CDR(BgL_head1648z00_10069);
													}
												else
													{	/* SawBbv/bbv-types.scm 192 */
														obj_t BgL_newtail1650z00_10073;

														{	/* SawBbv/bbv-types.scm 192 */
															obj_t BgL_arg2743z00_10074;

															{	/* SawBbv/bbv-types.scm 192 */
																obj_t BgL_tz00_10075;

																BgL_tz00_10075 =
																	CAR(((obj_t) BgL_l1646z00_10071));
																{	/* SawBbv/bbv-types.scm 192 */
																	obj_t BgL_arg2744z00_10076;

																	BgL_arg2744z00_10076 =
																		BGl_shapez00zztools_shapez00
																		(BgL_tz00_10075);
																	{	/* SawBbv/bbv-types.scm 192 */
																		obj_t BgL_list2745z00_10077;

																		BgL_list2745z00_10077 =
																			MAKE_YOUNG_PAIR(BgL_arg2744z00_10076,
																			BNIL);
																		BgL_arg2743z00_10074 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string3886z00zzsaw_bbvzd2typeszd2,
																			BgL_list2745z00_10077);
																	}
																}
															}
															BgL_newtail1650z00_10073 =
																MAKE_YOUNG_PAIR(BgL_arg2743z00_10074, BNIL);
														}
														SET_CDR(BgL_tail1649z00_10072,
															BgL_newtail1650z00_10073);
														{	/* SawBbv/bbv-types.scm 192 */
															obj_t BgL_arg2742z00_10078;

															BgL_arg2742z00_10078 =
																CDR(((obj_t) BgL_l1646z00_10071));
															{
																obj_t BgL_tail1649z00_16159;
																obj_t BgL_l1646z00_16158;

																BgL_l1646z00_16158 = BgL_arg2742z00_10078;
																BgL_tail1649z00_16159 =
																	BgL_newtail1650z00_10073;
																BgL_tail1649z00_10072 = BgL_tail1649z00_16159;
																BgL_l1646z00_10071 = BgL_l1646z00_16158;
																goto BgL_zc3z04anonymousza32740ze3z87_10070;
															}
														}
													}
											}
										}
									}
								{	/* SawBbv/bbv-types.scm 189 */
									obj_t BgL_list2725z00_10079;

									BgL_list2725z00_10079 =
										MAKE_YOUNG_PAIR(BgL_arg2724z00_10054, BNIL);
									BgL_arg2723z00_10053 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string3887z00zzsaw_bbvzd2typeszd2,
										BgL_list2725z00_10079);
								}
							}
							VECTOR_SET(BgL_v1657z00_10050, 1L, BgL_arg2723z00_10053);
						}
						{	/* SawBbv/bbv-types.scm 193 */
							obj_t BgL_arg2746z00_10080;

							{	/* SawBbv/bbv-types.scm 193 */
								obj_t BgL_arg2747z00_10081;

								BgL_arg2747z00_10081 =
									BGl_shapez00zztools_shapez00(
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
										BgL_valuez00));
								{	/* SawBbv/bbv-types.scm 193 */
									obj_t BgL_list2748z00_10082;

									BgL_list2748z00_10082 =
										MAKE_YOUNG_PAIR(BgL_arg2747z00_10081, BNIL);
									BgL_arg2746z00_10080 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string3888z00zzsaw_bbvzd2typeszd2,
										BgL_list2748z00_10082);
								}
							}
							VECTOR_SET(BgL_v1657z00_10050, 2L, BgL_arg2746z00_10080);
						}
						{	/* SawBbv/bbv-types.scm 194 */
							obj_t BgL_arg2750z00_10083;

							{	/* SawBbv/bbv-types.scm 194 */
								obj_t BgL_l1651z00_10084;

								BgL_l1651z00_10084 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_8865)))->
									BgL_aliasesz00);
								if (NULLP(BgL_l1651z00_10084))
									{	/* SawBbv/bbv-types.scm 194 */
										BgL_arg2750z00_10083 = BNIL;
									}
								else
									{	/* SawBbv/bbv-types.scm 194 */
										obj_t BgL_head1653z00_10085;

										BgL_head1653z00_10085 =
											MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
												(BgL_l1651z00_10084)), BNIL);
										{	/* SawBbv/bbv-types.scm 194 */
											obj_t BgL_g1656z00_10086;

											BgL_g1656z00_10086 = CDR(BgL_l1651z00_10084);
											{
												obj_t BgL_l1651z00_10088;
												obj_t BgL_tail1654z00_10089;

												BgL_l1651z00_10088 = BgL_g1656z00_10086;
												BgL_tail1654z00_10089 = BgL_head1653z00_10085;
											BgL_zc3z04anonymousza32752ze3z87_10087:
												if (NULLP(BgL_l1651z00_10088))
													{	/* SawBbv/bbv-types.scm 194 */
														BgL_arg2750z00_10083 = BgL_head1653z00_10085;
													}
												else
													{	/* SawBbv/bbv-types.scm 194 */
														obj_t BgL_newtail1655z00_10090;

														{	/* SawBbv/bbv-types.scm 194 */
															obj_t BgL_arg2755z00_10091;

															{	/* SawBbv/bbv-types.scm 194 */
																obj_t BgL_arg2756z00_10092;

																BgL_arg2756z00_10092 =
																	CAR(((obj_t) BgL_l1651z00_10088));
																BgL_arg2755z00_10091 =
																	BGl_shapez00zztools_shapez00
																	(BgL_arg2756z00_10092);
															}
															BgL_newtail1655z00_10090 =
																MAKE_YOUNG_PAIR(BgL_arg2755z00_10091, BNIL);
														}
														SET_CDR(BgL_tail1654z00_10089,
															BgL_newtail1655z00_10090);
														{	/* SawBbv/bbv-types.scm 194 */
															obj_t BgL_arg2754z00_10093;

															BgL_arg2754z00_10093 =
																CDR(((obj_t) BgL_l1651z00_10088));
															{
																obj_t BgL_tail1654z00_16187;
																obj_t BgL_l1651z00_16186;

																BgL_l1651z00_16186 = BgL_arg2754z00_10093;
																BgL_tail1654z00_16187 =
																	BgL_newtail1655z00_10090;
																BgL_tail1654z00_10089 = BgL_tail1654z00_16187;
																BgL_l1651z00_10088 = BgL_l1651z00_16186;
																goto BgL_zc3z04anonymousza32752ze3z87_10087;
															}
														}
													}
											}
										}
									}
							}
							VECTOR_SET(BgL_v1657z00_10050, 3L, BgL_arg2750z00_10083);
						}
						return BgL_v1657z00_10050;
					}
			}
		}

	}



/* &object-print-blockS1780 */
	obj_t BGl_z62objectzd2printzd2blockS1780z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8866, obj_t BgL_oz00_8867, obj_t BgL_pz00_8868,
		obj_t BgL_procz00_8869)
	{
		{	/* SawBbv/bbv-types.scm 155 */
			return
				BGl_dumpz00zzsaw_defsz00(
				((obj_t)
					((BgL_blockz00_bglt) BgL_oz00_8867)), BgL_pz00_8868, (int) (0L));
		}

	}



/* &object-print-blockV1778 */
	obj_t BGl_z62objectzd2printzd2blockV1778z62zzsaw_bbvzd2typeszd2(obj_t
		BgL_envz00_8870, obj_t BgL_oz00_8871, obj_t BgL_pz00_8872,
		obj_t BgL_procz00_8873)
	{
		{	/* SawBbv/bbv-types.scm 149 */
			return
				BGl_dumpz00zzsaw_defsz00(
				((obj_t)
					((BgL_blockz00_bglt) BgL_oz00_8871)), BgL_pz00_8872, (int) (0L));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2typeszd2(void)
	{
		{	/* SawBbv/bbv-types.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2gczd2(137439381L,
				BSTRING_TO_STRING(BGl_string3889z00zzsaw_bbvzd2typeszd2));
		}

	}

#ifdef __cplusplus
}
#endif
